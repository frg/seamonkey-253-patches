# HG changeset patch
# User Frank-Rainer Grahl <frgrahl@gmx.net>
# Date 1582379143 -3600
# Parent  e6c93fa37819f38b24fb715758d4ed5e0e84eedc
Bug 1617376 - Port Bug 1397387 "No longer able to edit bookmark item after closing New Bookmark or New Bookmark Folder dialog by [x] button" to SeaMonkey. r=IanN a=IanN

diff --git a/suite/components/places/PlacesUIUtils.jsm b/suite/components/places/PlacesUIUtils.jsm
--- a/suite/components/places/PlacesUIUtils.jsm
+++ b/suite/components/places/PlacesUIUtils.jsm
@@ -607,31 +607,55 @@ var PlacesUIUtils = {
    *        Describes the item to be edited/added in the dialog.
    *        See documentation at the top of bookmarkProperties.js
    * @param aWindow
    *        Owner window for the new dialog.
    *
    * @see documentation at the top of bookmarkProperties.js
    * @return true if any transaction has been performed, false otherwise.
    */
-  showBookmarkDialog:
-  function PUIU_showBookmarkDialog(aInfo, aParentWindow) {
+  showBookmarkDialog(aInfo, aParentWindow) {
     // Preserve size attributes differently based on the fact the dialog has
     // a folder picker or not, since it needs more horizontal space than the
     // other controls.
     let hasFolderPicker = !("hiddenRows" in aInfo) ||
                           !aInfo.hiddenRows.includes("folderPicker");
     // Use a different chrome url to persist different sizes.
     let dialogURL = hasFolderPicker ?
                     "chrome://communicator/content/places/bookmarkProperties2.xul" :
                     "chrome://communicator/content/places/bookmarkProperties.xul";
 
     let features = "centerscreen,chrome,modal,resizable=yes";
+
+    let topUndoEntry;
+    let batchBlockingDeferred;
+
+    if (this.useAsyncTransactions) {
+      // Set the transaction manager into batching mode.
+      topUndoEntry = PlacesTransactions.topUndoEntry;
+      batchBlockingDeferred = PromiseUtils.defer();
+      PlacesTransactions.batch(async () => {
+        await batchBlockingDeferred.promise;
+      });
+    }
+
     aParentWindow.openDialog(dialogURL, "", features, aInfo);
-    return ("performed" in aInfo && aInfo.performed);
+
+    let performed = ("performed" in aInfo && aInfo.performed);
+
+    if (this.useAsyncTransactions) {
+      batchBlockingDeferred.resolve();
+
+      if (!performed &&
+          topUndoEntry != PlacesTransactions.topUndoEntry) {
+        PlacesTransactions.undo().catch(Cu.reportError);
+      }
+    }
+
+    return performed;
   },
 
   _getTopBrowserWin: function PUIU__getTopBrowserWin() {
     return RecentWindow.getMostRecentBrowserWindow();
   },
 
   /**
    * set and fetch a favicon. Can only be used from the parent process.
diff --git a/suite/components/places/content/bookmarkProperties.js b/suite/components/places/content/bookmarkProperties.js
--- a/suite/components/places/content/bookmarkProperties.js
+++ b/suite/components/places/content/bookmarkProperties.js
@@ -56,18 +56,16 @@
  * window.arguments[0].performed is set to true if any transaction has
  * been performed by the dialog.
  */
 
 /* import-globals-from editBookmarkOverlay.js */
 
 ChromeUtils.defineModuleGetter(this, "PrivateBrowsingUtils",
   "resource://gre/modules/PrivateBrowsingUtils.jsm");
-ChromeUtils.defineModuleGetter(this, "PromiseUtils",
-  "resource://gre/modules/PromiseUtils.jsm");
 
 const BOOKMARK_ITEM = 0;
 const BOOKMARK_FOLDER = 1;
 const LIVEMARK_CONTAINER = 2;
 
 const ACTION_EDIT = 0;
 const ACTION_ADD = 1;
 
@@ -372,42 +370,30 @@ var BookmarkPropertiesPanel = {
 
   // Hack for implementing batched-Undo around the editBookmarkOverlay
   // instant-apply code. For all the details see the comment above beginBatch
   // in browser-places.js
   _batchBlockingDeferred: null,
   _beginBatch() {
     if (this._batching)
       return;
-    if (PlacesUIUtils.useAsyncTransactions) {
-      this._topUndoEntry = PlacesTransactions.topUndoEntry;
-      this._batchBlockingDeferred = PromiseUtils.defer();
-      PlacesTransactions.batch(async () => {
-        await this._batchBlockingDeferred.promise;
-      });
-    } else {
+    if (!PlacesUIUtils.useAsyncTransactions) {
       PlacesUtils.transactionManager.beginBatch(null);
     }
     this._batching = true;
   },
 
   _endBatch() {
     if (!this._batching)
-      return false;
+      return;
 
-    if (PlacesUIUtils.useAsyncTransactions) {
-      this._batchBlockingDeferred.resolve();
-      this._batchBlockingDeferred = null;
-    } else {
+    if (!PlacesUIUtils.useAsyncTransactions) {
       PlacesUtils.transactionManager.endBatch(false);
     }
     this._batching = false;
-    let changed = this._topUndoEntry != PlacesTransactions.topUndoEntry;
-    delete this._topUndoEntry;
-    return changed;
   },
 
   // nsISupports
   QueryInterface: function BPP_QueryInterface(aIID) {
     if (aIID.equals(Ci.nsIDOMEventListener) ||
         aIID.equals(Ci.nsISupports))
       return this;
 
@@ -441,22 +427,18 @@ var BookmarkPropertiesPanel = {
     window.arguments[0].performed = true;
   },
 
   onDialogCancel() {
     // The order here is important! We have to uninit the panel first, otherwise
     // changes done as part of Undo may change the panel contents and by
     // that force it to commit more transactions.
     gEditItemOverlay.uninitPanel(true);
-    let changed = this._endBatch();
-    if (PlacesUIUtils.useAsyncTransactions) {
-      if (changed) {
-        PlacesTransactions.undo().catch(Cu.reportError);
-      }
-    } else {
+    this._endBatch();
+    if (!PlacesUIUtils.useAsyncTransactions) {
       PlacesUtils.transactionManager.undoTransaction();
     }
     window.arguments[0].performed = false;
   },
 
   /**
    * This method checks to see if the input fields are in a valid state.
    *
