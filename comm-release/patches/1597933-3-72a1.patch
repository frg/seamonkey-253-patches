# HG changeset patch
# User Magnus Melin <mkmelin+mozilla@iki.fi>
# Date 1574325959 -7200
# Node ID 9b0f8cb7ffc12c4317f40a7e27fd223f951f8690
# Parent  976749558fd901c468a32aad9ed85e1ebc5ff9f6
Bug 1597933 - don't pass string constants to determine OAuth refresh token or not. r=Fallen

diff --git a/mailnews/base/util/OAuth2.jsm b/mailnews/base/util/OAuth2.jsm
--- a/mailnews/base/util/OAuth2.jsm
+++ b/mailnews/base/util/OAuth2.jsm
@@ -22,19 +22,16 @@ function OAuth2(aBaseURI, aScope, aAppKe
     this.consumerKey = aAppKey;
     this.consumerSecret = aAppSecret;
     this.scope = aScope;
     this.extraAuthParams = [];
 
     this.log = Log4Moz.getConfiguredLogger("TBOAuth");
 }
 
-OAuth2.CODE_AUTHORIZATION = "authorization_code";
-OAuth2.CODE_REFRESH = "refresh_token";
-
 OAuth2.prototype = {
     consumerKey: null,
     consumerSecret: null,
     completionURI: "http://localhost",
     requestWindowURI: "chrome://messenger/content/browserRequest.xul",
     requestWindowFeatures: "chrome,private,centerscreen,width=980,height=750",
     requestWindowTitle: "",
     scope: null,
@@ -46,17 +43,17 @@ OAuth2.prototype = {
     connect: function connect(aSuccess, aFailure, aWithUI, aRefresh) {
 
         this.connectSuccessCallback = aSuccess;
         this.connectFailureCallback = aFailure;
 
         if (!aRefresh && this.accessToken) {
             aSuccess();
         } else if (this.refreshToken) {
-            this.requestAccessToken(this.refreshToken, OAuth2.CODE_REFRESH);
+            this.requestAccessToken(this.refreshToken, true);
         } else {
             if (!aWithUI) {
                 aFailure('{ "error": "auth_noui" }');
                 return;
             }
             if (gConnecting[this.authURI]) {
                 aFailure("Window already open");
                 return;
@@ -158,46 +155,54 @@ OAuth2.prototype = {
         delete this._browserRequest;
     },
 
     // @see RFC 6749 section 4.1.2: Authorization Response
     onAuthorizationReceived(aURL) {
       this.log.info("OAuth2 authorization received: url=" + aURL);
       let params = new URLSearchParams(aURL.split("?", 2)[1]);
       if (params.has("code")) {
-        this.requestAccessToken(params.get("code"), OAuth2.CODE_AUTHORIZATION);
+        this.requestAccessToken(params.get("code"), false);
       } else {
         this.onAuthorizationFailed(null, aURL);
       }
     },
 
     onAuthorizationFailed: function(aError, aData) {
         this.connectFailureCallback(aData);
     },
 
-    requestAccessToken: function requestAccessToken(aCode, aType) {
-        let params = [
-            ["client_id", this.consumerKey],
-            ["client_secret", this.consumerSecret],
-            ["grant_type", aType],
-        ];
+    /**
+     * Request a new access token, or refresh an existing one.
+     * @param {string} aCode - The token issued to the client.
+     * @param {boolean} aRefresh - Whether it's a refresh of a token or not.
+     */
+    requestAccessToken(aCode, aRefresh) {
+      // @see RFC 6749 section 4.1.3. Access Token Request
+      // @see RFC 6749 section 6. Refreshing an Access Token
+      let params = [
+        ["client_id", this.consumerKey],
+        ["client_secret", this.consumerSecret],
+      ];
 
-        if (aType == OAuth2.CODE_AUTHORIZATION) {
-            params.push(["code", aCode]);
-            params.push(["redirect_uri", this.completionURI]);
-        } else if (aType == OAuth2.CODE_REFRESH) {
-            params.push(["refresh_token", aCode]);
-        }
+      if (aRefresh) {
+        params.push(["grant_type", "refresh_token"]);
+        params.push(["refresh_token", aCode]);
+      } else {
+        params.push(["grant_type", "authorization_code"]);
+        params.push(["code", aCode]);
+        params.push(["redirect_uri", this.completionURI]);
+      }
 
-        let options = {
-          postData: params,
-          onLoad: this.onAccessTokenReceived.bind(this),
-          onError: this.onAccessTokenFailed.bind(this)
-        }
-        httpRequest(this.tokenURI, options);
+      let options = {
+        postData: params,
+        onLoad: this.onAccessTokenReceived.bind(this),
+        onError: this.onAccessTokenFailed.bind(this)
+      }
+      httpRequest(this.tokenURI, options);
     },
 
     onAccessTokenFailed: function onAccessTokenFailed(aError, aData) {
         if (aError != "offline") {
             this.refreshToken = null;
         }
         this.connectFailureCallback(aData);
     },
