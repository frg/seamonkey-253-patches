# HG changeset patch
# User Neil Rashbrook <neil@parkwaycc.co.uk>
# Date 1539331260 -7200
# Node ID 8aebb2ed9307360384bb2cc5c35bde65e7535ac7
# Parent  002021479e51bd40f4efedf6bf320f34153fb159
Bug 1497513 - make some filter methods scriptable, replace string with ACString for 'headers' parameter. r=jorgk

diff --git a/mailnews/base/search/public/nsIMsgFilter.idl b/mailnews/base/search/public/nsIMsgFilter.idl
--- a/mailnews/base/search/public/nsIMsgFilter.idl
+++ b/mailnews/base/search/public/nsIMsgFilter.idl
@@ -77,23 +77,19 @@ interface nsIMsgFilter : nsISupports {
     void appendTerm(in nsIMsgSearchTerm term);
 
     nsIMsgSearchTerm createTerm();
 
     attribute nsIMutableArray searchTerms;
 
     attribute nsIMsgSearchScopeTerm scope;
 
-    // Marking noscript because "headers" is actually a null-separated
-    // list of headers, which is not scriptable.
-    [noscript] void MatchHdr(in nsIMsgDBHdr msgHdr, in nsIMsgFolder folder,
-                  in nsIMsgDatabase db,
-                  in string headers,
-                  //                  [array, size_is(headerSize)] in string headers,
-                  in unsigned long headerSize, out boolean result);
+    boolean MatchHdr(in nsIMsgDBHdr msgHdr, in nsIMsgFolder folder,
+                     in nsIMsgDatabase db,
+                     in ACString headers);  // null-separated list of headers
 
 
     /*
      * Report that Rule was matched and executed when filter logging is enabled.
      *
      * @param aFilterAction  The filter rule that was invoked.
      * @param aHeader        The header information of the message acted on by
      *                       the filter.
diff --git a/mailnews/base/search/public/nsIMsgFilterList.idl b/mailnews/base/search/public/nsIMsgFilterList.idl
--- a/mailnews/base/search/public/nsIMsgFilterList.idl
+++ b/mailnews/base/search/public/nsIMsgFilterList.idl
@@ -64,27 +64,21 @@ interface nsIMsgFilterList : nsISupports
 
     void parseCondition(in nsIMsgFilter aFilter, in string condition);
     // this is temporary so that we can save the filterlist to disk
     // without knowing where the filters were read from initially
     // (such as the filter list dialog)
     attribute nsIFile defaultFile;
     void saveToDefaultFile();
 
-
-    // marking noscript because headers is a null-separated list
-    // of strings, which is not scriptable
-    [noscript]
     void applyFiltersToHdr(in nsMsgFilterTypeType filterType,
                            in nsIMsgDBHdr msgHdr,
                            in nsIMsgFolder folder,
                            in nsIMsgDatabase db,
-                           in string headers,
-                           //[array, size_is(headerSize)] in string headers,
-                           in unsigned long headerSize,
+                           in ACString headers,  // null-separated list of headers
                            in nsIMsgFilterHitNotify listener,
                            in nsIMsgWindow msgWindow);
 
     // IO routines, used by filter object filing code.
     void writeIntAttr(in nsMsgFilterFileAttribValue attrib, in long value, in nsIOutputStream stream);
     void writeStrAttr(in nsMsgFilterFileAttribValue attrib, in string value, in nsIOutputStream stream);
     void writeWstrAttr(in nsMsgFilterFileAttribValue attrib, in wstring value, in nsIOutputStream stream);
     void writeBoolAttr(in nsMsgFilterFileAttribValue attrib, in boolean value, in nsIOutputStream stream);
diff --git a/mailnews/base/search/public/nsIMsgSearchTerm.idl b/mailnews/base/search/public/nsIMsgSearchTerm.idl
--- a/mailnews/base/search/public/nsIMsgSearchTerm.idl
+++ b/mailnews/base/search/public/nsIMsgSearchTerm.idl
@@ -88,30 +88,28 @@ interface nsIMsgSearchTerm : nsISupports
      * Test if the arbitrary header specified by this search term
      * matches the corresponding header in the passed in message.
      *
      * @param aScopeTerm scope of search
      * @param aLength length of message
      * @param aCharset The charset to apply to un-labeled non-UTF-8 data.
      * @param aCharsetOverride If true, aCharset is used instead of any
      *                         charset labeling other than UTF-8.
-     *
-     * N.B. This is noscript because headers is a null-separated list of
-     * strings, which is not scriptable.
+     * @param aMsg The nsIMsgDBHdr of the message
+     * @param aDb The message database containing aMsg
+     * @param aHeaders A null-separated list of message headers.
+     * @param aForFilters Whether this is a filter or a search operation.
      */
-    [noscript]
     boolean matchArbitraryHeader(in nsIMsgSearchScopeTerm aScopeTerm,
                                  in unsigned long aLength,
                                  in string aCharset,
                                  in boolean aCharsetOverride,
                                  in nsIMsgDBHdr aMsg,
                                  in nsIMsgDatabase aDb,
-                                 //[array, size_is(headerLength)] in string headers,
-                                 in string aHeaders,
-                                 in unsigned long aHeaderLength,
+                                 in ACString aHeaders,
                                  in boolean aForFilters);
 
     /**
      * Compares value.str with nsIMsgHdr::GetProperty(hdrProperty).
      * @param msg   msg to match db hdr property of.
      *
      * @returns     true if msg matches property, false otherwise.
      */
diff --git a/mailnews/base/search/public/nsMsgSearchBoolExpression.h b/mailnews/base/search/public/nsMsgSearchBoolExpression.h
--- a/mailnews/base/search/public/nsMsgSearchBoolExpression.h
+++ b/mailnews/base/search/public/nsMsgSearchBoolExpression.h
@@ -60,18 +60,17 @@ public:
   static nsMsgSearchBoolExpression * AddSearchTerm (nsMsgSearchBoolExpression * aOrigExpr, nsIMsgSearchTerm * aNewTerm, char * aEncodingStr); // IMAP/NNTP
     static nsMsgSearchBoolExpression * AddExpressionTree(nsMsgSearchBoolExpression * aOrigExpr, nsMsgSearchBoolExpression * aExpression, bool aBoolOp);
 
     // parses the expression tree and all
     // expressions underneath this node to
     // determine if the end result is true or false.
   bool OfflineEvaluate(nsIMsgDBHdr *msgToMatch,
           const char *defaultCharset, nsIMsgSearchScopeTerm *scope,
-          nsIMsgDatabase *db, const char *headers, uint32_t headerSize,
-          bool Filtering);
+          nsIMsgDatabase *db, const nsACString& headers, bool Filtering);
 
     // assuming the expression is for online
     // searches, determine the length of the
     // resulting IMAP/NNTP encoding string
   int32_t CalcEncodeStrSize();
 
     // fills pre-allocated
     // memory in buffer with
diff --git a/mailnews/base/search/src/nsMsgFilter.cpp b/mailnews/base/search/src/nsMsgFilter.cpp
--- a/mailnews/base/search/src/nsMsgFilter.cpp
+++ b/mailnews/base/search/src/nsMsgFilter.cpp
@@ -688,26 +688,26 @@ NS_IMETHODIMP nsMsgFilter::LogRuleHitFai
                                           nsresult aRcode,
                                           const char *aErrMsg)
 {
   return nsMsgFilter::LogRuleHitGeneric(aFilterAction, aMsgHdr, aRcode, aErrMsg);
 }
 
 NS_IMETHODIMP
 nsMsgFilter::MatchHdr(nsIMsgDBHdr *msgHdr, nsIMsgFolder *folder,
-                      nsIMsgDatabase *db, const char *headers,
-                      uint32_t headersSize, bool *pResult)
+                      nsIMsgDatabase *db, const nsACString& headers,
+                      bool *pResult)
 {
   NS_ENSURE_ARG_POINTER(folder);
   NS_ENSURE_ARG_POINTER(msgHdr);
   // use offlineMail because
   nsCString folderCharset;
   folder->GetCharset(folderCharset);
   nsresult rv = nsMsgSearchOfflineMail::MatchTermsForFilter(msgHdr, m_termList,
-                  folderCharset.get(),  m_scope,  db,  headers,  headersSize, &m_expressionTree, pResult);
+                  folderCharset.get(), m_scope, db, headers, &m_expressionTree, pResult);
   return rv;
 }
 
 NS_IMETHODIMP
 nsMsgFilter::SetFilterList(nsIMsgFilterList *filterList)
 {
   // doesn't hold a ref.
   m_filterList = filterList;
diff --git a/mailnews/base/search/src/nsMsgFilterList.cpp b/mailnews/base/search/src/nsMsgFilterList.cpp
--- a/mailnews/base/search/src/nsMsgFilterList.cpp
+++ b/mailnews/base/search/src/nsMsgFilterList.cpp
@@ -268,18 +268,17 @@ nsMsgFilterList::GetLogStream(nsIOutputS
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsMsgFilterList::ApplyFiltersToHdr(nsMsgFilterTypeType filterType,
                                    nsIMsgDBHdr *msgHdr,
                                    nsIMsgFolder *folder,
                                    nsIMsgDatabase *db,
-                                   const char*headers,
-                                   uint32_t headersSize,
+                                   const nsACString& headers,
                                    nsIMsgFilterHitNotify *listener,
                                    nsIMsgWindow *msgWindow)
 {
   nsCOMPtr<nsIMsgFilter> filter;
   uint32_t filterCount = 0;
   nsresult rv = GetFilterCount(&filterCount);
   NS_ENSURE_SUCCESS(rv, rv);
 
@@ -299,17 +298,17 @@ nsMsgFilterList::ApplyFiltersToHdr(nsMsg
 
       filter->GetFilterType(&curFilterType);
       if (curFilterType & filterType)
       {
         nsresult matchTermStatus = NS_OK;
         bool result;
 
         filter->SetScope(scope);
-        matchTermStatus = filter->MatchHdr(msgHdr, folder, db, headers, headersSize, &result);
+        matchTermStatus = filter->MatchHdr(msgHdr, folder, db, headers, &result);
         filter->SetScope(nullptr);
         if (NS_SUCCEEDED(matchTermStatus) && result && listener)
         {
           bool applyMore = true;
 
           rv = listener->ApplyFilterHit(filter, msgWindow, &applyMore);
           if (NS_FAILED(rv) || !applyMore)
             break;
diff --git a/mailnews/base/search/src/nsMsgFilterService.cpp b/mailnews/base/search/src/nsMsgFilterService.cpp
--- a/mailnews/base/search/src/nsMsgFilterService.cpp
+++ b/mailnews/base/search/src/nsMsgFilterService.cpp
@@ -1055,17 +1055,17 @@ nsresult nsMsgApplyFiltersToMessages::Ru
     OnNewSearch();
 
     for (int32_t i = 0; i < m_msgHdrList.Count(); i++)
     {
       nsCOMPtr<nsIMsgDBHdr> msgHdr = m_msgHdrList[i];
       CONTINUE_IF_FALSE(msgHdr, "null msgHdr");
 
       bool matched;
-      rv = m_curFilter->MatchHdr(msgHdr, m_curFolder, m_curFolderDB, nullptr, 0, &matched);
+      rv = m_curFilter->MatchHdr(msgHdr, m_curFolder, m_curFolderDB, EmptyCString(), &matched);
       if (NS_SUCCEEDED(rv) && matched)
       {
         // In order to work with nsMsgFilterAfterTheFact::ApplyFilter we initialize
         // nsMsgFilterAfterTheFact's information with a search hit now for the message
         // that we're filtering.
         OnSearchHit(msgHdr, m_curFolder);
       }
     }
diff --git a/mailnews/base/search/src/nsMsgLocalSearch.cpp b/mailnews/base/search/src/nsMsgLocalSearch.cpp
--- a/mailnews/base/search/src/nsMsgLocalSearch.cpp
+++ b/mailnews/base/search/src/nsMsgLocalSearch.cpp
@@ -124,47 +124,47 @@ nsMsgSearchBoolExpression::leftToRightAd
          delete tempExpr;    // clean up memory allocation in case of failure
     }
     return this;   // in case we failed to create a new expression, return self
 }
 
 
 // returns true or false depending on what the current expression evaluates to.
 bool nsMsgSearchBoolExpression::OfflineEvaluate(nsIMsgDBHdr *msgToMatch, const char *defaultCharset,
-  nsIMsgSearchScopeTerm *scope, nsIMsgDatabase *db, const char *headers,
-  uint32_t headerSize, bool Filtering)
+  nsIMsgSearchScopeTerm *scope, nsIMsgDatabase *db, const nsACString& headers,
+  bool Filtering)
 {
     bool result = true;    // always default to false positives
     bool isAnd;
 
     if (m_term) // do we contain just a search term?
     {
       nsMsgSearchOfflineMail::ProcessSearchTerm(msgToMatch, m_term,
-        defaultCharset, scope, db, headers, headerSize, Filtering, &result);
+        defaultCharset, scope, db, headers, Filtering, &result);
       return result;
     }
 
     // otherwise we must recursively determine the value of our sub expressions
 
     isAnd = (m_boolOp == nsMsgSearchBooleanOp::BooleanAND);
 
     if (m_leftChild)
     {
         result = m_leftChild->OfflineEvaluate(msgToMatch, defaultCharset,
-          scope, db, headers, headerSize, Filtering);
+          scope, db, headers, Filtering);
         if ( (result && !isAnd) || (!result && isAnd))
           return result;
     }
 
     // If we got this far, either there was no leftChild (which is impossible)
     // or we got (FALSE and OR) or (TRUE and AND) from the first result. That
     // means the outcome depends entirely on the rightChild.
     if (m_rightChild)
         result = m_rightChild->OfflineEvaluate(msgToMatch, defaultCharset,
-          scope, db, headers, headerSize, Filtering);
+          scope, db, headers, Filtering);
 
     return result;
 }
 
 // ### Maybe we can get rid of these because of our use of nsString???
 // constants used for online searching with IMAP/NNTP encoded search terms.
 // the + 1 is to account for null terminators we add at each stage of assembling the expression...
 const int sizeOfORTerm = 6+1;  // 6 bytes if we are combining two sub expressions with an OR term
@@ -303,36 +303,35 @@ nsresult nsMsgSearchOfflineMail::OpenSum
 
 
 nsresult
 nsMsgSearchOfflineMail::MatchTermsForFilter(nsIMsgDBHdr *msgToMatch,
                                             nsIArray *termList,
                                             const char *defaultCharset,
                                             nsIMsgSearchScopeTerm * scope,
                                             nsIMsgDatabase * db,
-                                            const char * headers,
-                                            uint32_t headerSize,
+                                            const nsACString& headers,
                                             nsMsgSearchBoolExpression ** aExpressionTree,
                                             bool *pResult)
 {
-    return MatchTerms(msgToMatch, termList, defaultCharset, scope, db, headers, headerSize, true, aExpressionTree, pResult);
+    return MatchTerms(msgToMatch, termList, defaultCharset, scope, db, headers, true, aExpressionTree, pResult);
 }
 
 // static method which matches a header against a list of search terms.
 nsresult
 nsMsgSearchOfflineMail::MatchTermsForSearch(nsIMsgDBHdr *msgToMatch,
                                             nsIArray* termList,
                                             const char *defaultCharset,
                                             nsIMsgSearchScopeTerm *scope,
                                             nsIMsgDatabase *db,
                                             nsMsgSearchBoolExpression ** aExpressionTree,
                                             bool *pResult)
 {
 
-    return MatchTerms(msgToMatch, termList, defaultCharset, scope, db, nullptr, 0, false, aExpressionTree, pResult);
+    return MatchTerms(msgToMatch, termList, defaultCharset, scope, db, EmptyCString(), false, aExpressionTree, pResult);
 }
 
 nsresult nsMsgSearchOfflineMail::ConstructExpressionTree(nsIArray *termList,
                                             uint32_t termCount,
                                             uint32_t &aStartPosInList,
                                             nsMsgSearchBoolExpression ** aExpressionTree)
 {
   nsMsgSearchBoolExpression * finalExpression = *aExpressionTree;
@@ -388,18 +387,17 @@ nsresult nsMsgSearchOfflineMail::Constru
   return NS_OK;
 }
 
 nsresult nsMsgSearchOfflineMail::ProcessSearchTerm(nsIMsgDBHdr *msgToMatch,
                                             nsIMsgSearchTerm * aTerm,
                                             const char *defaultCharset,
                                             nsIMsgSearchScopeTerm * scope,
                                             nsIMsgDatabase * db,
-                                            const char * headers,
-                                            uint32_t headerSize,
+                                            const nsACString& headers,
                                             bool Filtering,
                                             bool *pResult)
 {
     nsresult err = NS_OK;
     nsCString  recipients;
     nsCString  ccList;
     nsCString  matchString;
     nsCString  msgCharset;
@@ -638,36 +636,34 @@ nsresult nsMsgSearchOfflineMail::Process
               attrib < nsMsgSearchAttrib::kNumMsgSearchAttributes)
           {
             uint32_t lineCount;
             msgToMatch->GetLineCount(&lineCount);
             uint64_t messageOffset;
             msgToMatch->GetMessageOffset(&messageOffset);
             err = aTerm->MatchArbitraryHeader(scope, lineCount,charset,
                                               charsetOverride, msgToMatch, db,
-                                              headers, headerSize, Filtering,
-                                              &result);
+                                              headers, Filtering, &result);
           }
           else {
             err = NS_ERROR_INVALID_ARG; // ### was SearchError_InvalidAttribute
             result = false;
           }
     }
 
     *pResult = result;
     return err;
 }
 
 nsresult nsMsgSearchOfflineMail::MatchTerms(nsIMsgDBHdr *msgToMatch,
                                             nsIArray *termList,
                                             const char *defaultCharset,
                                             nsIMsgSearchScopeTerm * scope,
                                             nsIMsgDatabase * db,
-                                            const char * headers,
-                                            uint32_t headerSize,
+                                            const nsACString& headers,
                                             bool Filtering,
                                             nsMsgSearchBoolExpression ** aExpressionTree,
                                             bool *pResult)
 {
   NS_ENSURE_ARG(aExpressionTree);
   nsresult err;
 
   if (!*aExpressionTree)
@@ -678,17 +674,17 @@ nsresult nsMsgSearchOfflineMail::MatchTe
     err = ConstructExpressionTree(termList, count, initialPos, aExpressionTree);
     if (NS_FAILED(err))
       return err;
   }
 
   // evaluate the expression tree and return the result
   *pResult = (*aExpressionTree)
     ?  (*aExpressionTree)->OfflineEvaluate(msgToMatch,
-                 defaultCharset, scope, db, headers, headerSize, Filtering)
+                 defaultCharset, scope, db, headers, Filtering)
     :true;  // vacuously true...
 
   return NS_OK;
 }
 
 nsresult nsMsgSearchOfflineMail::Search(bool *aDone)
 {
   nsresult err = NS_OK;
diff --git a/mailnews/base/search/src/nsMsgLocalSearch.h b/mailnews/base/search/src/nsMsgLocalSearch.h
--- a/mailnews/base/search/src/nsMsgLocalSearch.h
+++ b/mailnews/base/search/src/nsMsgLocalSearch.h
@@ -35,18 +35,17 @@ public:
   NS_IMETHOD Abort () override;
   NS_IMETHOD AddResultElement (nsIMsgDBHdr *) override;
 
   static nsresult  MatchTermsForFilter(nsIMsgDBHdr * msgToMatch,
                                          nsIArray *termList,
                                          const char *defaultCharset,
                                          nsIMsgSearchScopeTerm *scope,
                                          nsIMsgDatabase * db,
-                                         const char * headers,
-                                         uint32_t headerSize,
+                                         const nsACString& headers,
                                          nsMsgSearchBoolExpression ** aExpressionTree,
                      bool *pResult);
 
   static nsresult MatchTermsForSearch(nsIMsgDBHdr * msgTomatch,
                                       nsIArray *termList,
                                       const char *defaultCharset,
                                       nsIMsgSearchScopeTerm *scope,
                                       nsIMsgDatabase *db,
@@ -55,29 +54,27 @@ public:
 
   virtual nsresult OpenSummaryFile ();
 
      static nsresult ProcessSearchTerm(nsIMsgDBHdr *msgToMatch,
                                nsIMsgSearchTerm * aTerm,
                                const char *defaultCharset,
                                nsIMsgSearchScopeTerm * scope,
                                nsIMsgDatabase * db,
-                               const char * headers,
-                               uint32_t headerSize,
+                               const nsACString& headers,
                                bool Filtering,
                  bool *pResult);
 protected:
   virtual ~nsMsgSearchOfflineMail();
   static nsresult MatchTerms(nsIMsgDBHdr *msgToMatch,
                                 nsIArray *termList,
                                 const char *defaultCharset,
                                 nsIMsgSearchScopeTerm *scope,
                                 nsIMsgDatabase * db,
-                                const char * headers,
-                                uint32_t headerSize,
+                                const nsACString& headers,
                                 bool ForFilters,
                                 nsMsgSearchBoolExpression ** aExpressionTree,
                 bool *pResult);
 
     static nsresult ConstructExpressionTree(nsIArray *termList,
                                       uint32_t termCount,
                                       uint32_t &aStartPosInList,
                                       nsMsgSearchBoolExpression ** aExpressionTree);
diff --git a/mailnews/base/search/src/nsMsgSearchTerm.cpp b/mailnews/base/search/src/nsMsgSearchTerm.cpp
--- a/mailnews/base/search/src/nsMsgSearchTerm.cpp
+++ b/mailnews/base/search/src/nsMsgSearchTerm.cpp
@@ -743,18 +743,17 @@ nsresult nsMsgSearchTerm::DeStreamNew (c
 // Looks in the MessageDB for the user specified arbitrary header, if it finds the header, it then looks for a match against
 // the value for the header.
 nsresult nsMsgSearchTerm::MatchArbitraryHeader (nsIMsgSearchScopeTerm *scope,
                                                 uint32_t length /* in lines*/,
                                                 const char *charset,
                                                 bool charsetOverride,
                                                 nsIMsgDBHdr *msg,
                                                 nsIMsgDatabase* db,
-                                                const char * headers,
-                                                uint32_t headersSize,
+                                                const nsACString& headers,
                                                 bool ForFiltering,
                                                 bool *pResult)
 {
   NS_ENSURE_ARG_POINTER(pResult);
 
   *pResult = false;
   nsresult rv = NS_OK;
   bool matchExpected = m_operator == nsMsgSearchOp::Contains ||
@@ -765,19 +764,20 @@ nsresult nsMsgSearchTerm::MatchArbitrary
   bool result = !matchExpected;
 
   nsCString dbHdrValue;
   msg->GetStringProperty(m_arbitraryHeader.get(), getter_Copies(dbHdrValue));
   if (!dbHdrValue.IsEmpty())
     // match value with the other info.
     return MatchRfc2047String(dbHdrValue, charset, charsetOverride, pResult);
 
-  nsMsgBodyHandler * bodyHandler =
-    new nsMsgBodyHandler (scope, length, msg, db, headers, headersSize,
-                          ForFiltering);
+  nsMsgBodyHandler * bodyHandler = new nsMsgBodyHandler(scope, length, msg, db,
+                                                        headers.BeginReading(),
+                                                        headers.Length(),
+                                                        ForFiltering);
   NS_ENSURE_TRUE(bodyHandler, NS_ERROR_OUT_OF_MEMORY);
 
   bodyHandler->SetStripHeaders (false);
 
   nsCString headerFullValue; // contains matched header value accumulated over multiple lines.
   nsAutoCString buf;
   nsAutoCString curMsgHeader;
   bool searchingHeaders = true;
diff --git a/mailnews/imap/src/nsImapMailFolder.cpp b/mailnews/imap/src/nsImapMailFolder.cpp
--- a/mailnews/imap/src/nsImapMailFolder.cpp
+++ b/mailnews/imap/src/nsImapMailFolder.cpp
@@ -3141,17 +3141,17 @@ nsresult nsImapMailFolder::NormalEndHead
 
       if (NS_SUCCEEDED(rv) && headers && !m_msgMovedByFilter &&
           !m_filterListRequiresBody)
       {
         if (m_filterList)
         {
           GetMoveCoalescer();  // not sure why we're doing this here.
           m_filterList->ApplyFiltersToHdr(nsMsgFilterType::InboxRule, newMsgHdr,
-                                          this, mDatabase, headers, headersSize,
+                                          this, mDatabase, nsDependentCString(headers, headersSize),
                                           this, msgWindow);
           NotifyFolderEvent(mFiltersAppliedAtom);
         }
       }
     }
   }
   // here we need to tweak flags from uid state..
   if (mDatabase && (!m_msgMovedByFilter || ShowDeletedMessages()))
@@ -4671,17 +4671,17 @@ nsImapMailFolder::NormalEndMsgWriteStrea
       {
         nsresult rv;
         nsCOMPtr<nsIMsgMailNewsUrl> msgUrl;
         msgUrl = do_QueryInterface(imapUrl, &rv);
         if (msgUrl && NS_SUCCEEDED(rv))
           msgUrl->GetMsgWindow(getter_AddRefs(msgWindow));
       }
       m_filterList->ApplyFiltersToHdr(nsMsgFilterType::InboxRule, newMsgHdr,
-                                      this, mDatabase, nullptr, 0, this,
+                                      this, mDatabase, EmptyCString(), this,
                                       msgWindow);
       NotifyFolderEvent(mFiltersAppliedAtom);
     }
     // Process filter plugins and other items normally done at the end of
     // HeaderFetchCompleted.
     bool pendingMoves = m_moveCoalescer && m_moveCoalescer->HasPendingMoves();
     PlaybackCoalescedOperations();
 
diff --git a/mailnews/local/src/nsParseMailbox.cpp b/mailnews/local/src/nsParseMailbox.cpp
--- a/mailnews/local/src/nsParseMailbox.cpp
+++ b/mailnews/local/src/nsParseMailbox.cpp
@@ -1939,22 +1939,22 @@ void nsParseNewMailState::ApplyFilters(b
                                           getter_AddRefs(downloadFolder));
       if (downloadFolder)
         downloadFolder->GetURI(m_inboxUri);
       char * headers = m_headers.GetBuffer();
       uint32_t headersSize = m_headers.GetBufferPos();
       if (m_filterList)
         (void) m_filterList->
           ApplyFiltersToHdr(nsMsgFilterType::InboxRule, msgHdr, downloadFolder,
-                            m_mailDB, headers, headersSize, this, msgWindow);
+                            m_mailDB, nsDependentCString(headers, headersSize), this, msgWindow);
       if (!m_msgMovedByFilter && m_deferredToServerFilterList)
       {
         (void) m_deferredToServerFilterList->
           ApplyFiltersToHdr(nsMsgFilterType::InboxRule, msgHdr, downloadFolder,
-                            m_mailDB, headers, headersSize, this, msgWindow);
+                            m_mailDB, nsDependentCString(headers, headersSize), this, msgWindow);
       }
     }
   }
   if (pMoved)
     *pMoved = m_msgMovedByFilter;
 }
 
 NS_IMETHODIMP nsParseNewMailState::ApplyFilterHit(nsIMsgFilter *filter, nsIMsgWindow *msgWindow, bool *applyMore)
diff --git a/mailnews/news/src/nsNNTPNewsgroupList.cpp b/mailnews/news/src/nsNNTPNewsgroupList.cpp
--- a/mailnews/news/src/nsNNTPNewsgroupList.cpp
+++ b/mailnews/news/src/nsNNTPNewsgroupList.cpp
@@ -1174,24 +1174,22 @@ nsNNTPNewsgroupList::CallFilters()
     }
 
     // The per-newsgroup filters should go first. If something stops filter
     // execution, then users should be able to override the global filters in
     // the per-newsgroup filters.
     if (filterCount)
     {
       rv = m_filterList->ApplyFiltersToHdr(nsMsgFilterType::NewsRule,
-          m_newMsgHdr, folder, m_newsDB, fullHeaders.get(),
-          fullHeaders.Length(), this, m_msgWindow);
+          m_newMsgHdr, folder, m_newsDB, fullHeaders, this, m_msgWindow);
     }
     if (serverFilterCount)
     {
       rv = m_serverFilterList->ApplyFiltersToHdr(nsMsgFilterType::NewsRule,
-          m_newMsgHdr, folder, m_newsDB, fullHeaders.get(),
-          fullHeaders.Length(), this, m_msgWindow);
+          m_newMsgHdr, folder, m_newsDB, fullHeaders, this, m_msgWindow);
     }
 
     NS_ENSURE_SUCCESS(rv,rv);
 
     if (m_addHdrToDB)
     {
       m_newsDB->AddNewHdrToDB(m_newMsgHdr, true);
       if (notifier)
