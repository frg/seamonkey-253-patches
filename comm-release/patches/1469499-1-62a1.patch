# HG changeset patch
# User Geoff Lankow <geoff@darktrojan.net>
# Date 1529885690 -43200
# Node ID 2135caba7fd1f4f9051c9469b68f98eac3c7bd82
# Parent  c8b052527829a63534feb07e0d43a2408d6a1b94
Bug 1469499 - Port bug 1444329: Replace use of nsIScriptableUnicodeConverter::convertFromByteArray. r=philipp
Backed out changeset 5365296a9690 (bug 1469499)

MozReview-Commit-ID: 3HjV5B89Bqj

diff --git a/calendar/base/modules/utils/calProviderUtils.jsm b/calendar/base/modules/utils/calProviderUtils.jsm
--- a/calendar/base/modules/utils/calProviderUtils.jsm
+++ b/calendar/base/modules/utils/calProviderUtils.jsm
@@ -4,16 +4,17 @@
 
 ChromeUtils.import("resource:///modules/mailServices.js");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 ChromeUtils.import("resource://gre/modules/Preferences.jsm");
 ChromeUtils.import("resource:///modules/iteratorUtils.jsm");
 
 XPCOMUtils.defineLazyModuleGetter(this, "cal", "resource://calendar/modules/calUtils.jsm", "cal");
+XPCOMUtils.defineLazyModuleGetter(this, "Deprecated", "resource://gre/modules/Deprecated.jsm");
 
 /*
  * Helpers and base class for calendar providers
  */
 
 // NOTE: This module should not be loaded directly, it is available when
 // including calUtils.jsm under the cal.provider namespace.
 
@@ -100,22 +101,23 @@ var calprovider = {
      * Convert a byte array to a string
      *
      * @param {octet[]} aResult         The bytes to convert
      * @param {Number} aResultLength    The number of bytes
      * @param {String} aCharset         The character set of the bytes, defaults to utf-8
      * @param {Boolean} aThrow          If true, the function will raise an exception on error
      * @return {?String}                The string result, or null on error
      */
-    convertByteArray: function(aResult, aResultLength, aCharset, aThrow) {
+    convertByteArray: function(aResult, aResultLength, aCharset="utf-8", aThrow) {
+        Deprecated.warning(
+            "The cal.provider.convertByteArray() function has been superseded by the use of TextDecoder.",
+            "https://bugzilla.mozilla.org/show_bug.cgi?id=1469499"
+        );
         try {
-            let resultConverter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"]
-                                            .createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
-            resultConverter.charset = aCharset || "UTF-8";
-            return resultConverter.convertFromByteArray(aResult, aResultLength);
+            return new TextDecoder(aCharset).decode(aResult);
         } catch (e) {
             if (aThrow) {
                 throw e;
             }
         }
         return null;
     },
 
diff --git a/calendar/base/src/calIcsParser.js b/calendar/base/src/calIcsParser.js
--- a/calendar/base/src/calIcsParser.js
+++ b/calendar/base/src/calIcsParser.js
@@ -1,15 +1,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
+ChromeUtils.import("resource://gre/modules/NetUtil.jsm");
 
 function calIcsParser() {
     this.wrappedJSObject = this;
     this.mItems = [];
     this.mParentlessItems = [];
     this.mComponents = [];
     this.mProperties = [];
 }
@@ -162,42 +163,17 @@ calIcsParser.prototype = {
         }
     },
 
     parseFromStream: function(aStream, aTzProvider, aAsyncParsing) {
         // Read in the string. Note that it isn't a real string at this point,
         // because likely, the file is utf8. The multibyte chars show up as multiple
         // 'chars' in this string. So call it an array of octets for now.
 
-        let octetArray = [];
-        let binaryIS = Components.classes["@mozilla.org/binaryinputstream;1"]
-                                 .createInstance(Components.interfaces.nsIBinaryInputStream);
-        binaryIS.setInputStream(aStream);
-        octetArray = binaryIS.readByteArray(binaryIS.available());
-
-        // Some other apps (most notably, sunbird 0.2) happily splits an UTF8
-        // character between the octets, and adds a newline and space between them,
-        // for ICS folding. Unfold manually before parsing the file as utf8.This is
-        // UTF8 safe, because octets with the first bit 0 are always one-octet
-        // characters. So the space or the newline never can be part of a multi-byte
-        // char.
-        for (let i = octetArray.length - 2; i >= 0; i--) {
-            if (octetArray[i] == "\n" && octetArray[i + 1] == " ") {
-                octetArray = octetArray.splice(i, 2);
-            }
-        }
-
-        // Interpret the byte-array as a UTF8-string, and convert into a
-        // javascript string.
-        let unicodeConverter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"]
-                                         .createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
-        // ICS files are always UTF8
-        unicodeConverter.charset = "UTF-8";
-        let stringData = unicodeConverter.convertFromByteArray(octetArray, octetArray.length);
-
+        let stringData = NetUtil.readInputStreamToString(aStream, aStream.available(), { charset: "utf-8" });
         this.parseString(stringData, aTzProvider, aAsyncParsing);
     },
 
     getItems: function(aCount) {
         aCount.value = this.mItems.length;
         return this.mItems.concat([]);
     },
 
diff --git a/calendar/providers/caldav/calDavCalendar.js b/calendar/providers/caldav/calDavCalendar.js
--- a/calendar/providers/caldav/calDavCalendar.js
+++ b/calendar/providers/caldav/calDavCalendar.js
@@ -663,17 +663,17 @@ calDavCalendar.prototype = {
                 let request = aLoader.request.QueryInterface(Components.interfaces.nsIHttpChannel);
                 let listenerStatus = Components.results.NS_OK;
                 let listenerDetail = parentItem;
                 let responseStatus;
                 try {
                     responseStatus = request.responseStatus;
 
                     if (self.verboseLogging()) {
-                        let str = cal.provider.convertByteArray(aResult, aResultLength);
+                        let str = new TextDecoder().decode(aResult);
                         cal.LOG("CalDAV: recv: " + (str || ""));
                     }
                 } catch (ex) {
                     listenerStatus = ex.result;
                     listenerDetail = "Request Failed: " + ex.message;
                     cal.LOG("CalDAV: Request error during add: " + ex);
                 }
 
@@ -782,17 +782,17 @@ calDavCalendar.prototype = {
                 let request = aLoader.request.QueryInterface(Components.interfaces.nsIHttpChannel);
                 let listenerStatus = Components.results.NS_OK;
                 let listenerDetail = aNewItem;
                 let responseStatus;
                 try {
                     responseStatus = request.responseStatus;
 
                     if (self.verboseLogging()) {
-                        let str = cal.provider.convertByteArray(aResult, aResultLength);
+                        let str = new TextDecoder().decode(aResult);
                         cal.LOG("CalDAV: recv: " + (str || ""));
                     }
                 } catch (ex) {
                     listenerStatus = ex.result;
                     listenerDetail = "Request Failed: " + ex.message;
                     cal.LOG("CalDAV: Request error during add: " + ex);
                 }
 
@@ -910,17 +910,17 @@ calDavCalendar.prototype = {
                 let request = aLoader.request.QueryInterface(Components.interfaces.nsIHttpChannel);
                 let listenerStatus = Components.results.NS_OK;
                 let listenerDetail = aItem;
                 let responseStatus;
                 try {
                     responseStatus = request.responseStatus;
 
                     if (self.verboseLogging()) {
-                        let str = cal.provider.convertByteArray(aResult, aResultLength);
+                        let str = new TextDecoder().decode(aResult);
                         cal.LOG("CalDAV: recv: " + (str || ""));
                     }
                 } catch (ex) {
                     listenerStatus = ex.result;
                     listenerDetail = "Request Failed: " + ex.message;
                     cal.LOG("CalDAV: Request error during delete: " + ex);
                 }
 
@@ -980,17 +980,17 @@ calDavCalendar.prototype = {
                 let request = aLoader.request.QueryInterface(Components.interfaces.nsIHttpChannel);
                 let listenerStatus = Components.results.NS_OK;
                 let listenerDetail = aItem;
                 let responseStatus;
                 try {
                     responseStatus = request.responseStatus;
 
                     if (self.verboseLogging()) {
-                        let str = cal.provider.convertByteArray(aResult, aResultLength);
+                        let str = new TextDecoder().decode(aResult);
                         cal.LOG("CalDAV: recv: " + (str || ""));
                     }
                 } catch (ex) {
                     listenerStatus = ex.result;
                     listenerDetail = "Request Failed: " + ex.message;
                     cal.LOG("CalDAV: Request error during add: " + ex);
                 }
 
@@ -1411,17 +1411,17 @@ calDavCalendar.prototype = {
                 return;
             } else if (request.responseStatus == 207 && self.mDisabled) {
                 // Looks like the calendar is there again, check its resource
                 // type first.
                 self.setupAuthentication(aChangeLogListener);
                 return;
             }
 
-            let str = cal.provider.convertByteArray(aResult, aResultLength);
+            let str = new TextDecoder().decode(aResult);
             if (!str) {
                 cal.LOG("CalDAV: Failed to get ctag from server for calendar " +
                         self.name);
             } else if (self.verboseLogging()) {
                 cal.LOG("CalDAV: recv: " + str);
             }
 
             let multistatus;
@@ -1794,17 +1794,17 @@ calDavCalendar.prototype = {
             // we only really need the authrealm for Digest auth
             // since only Digest is going to time out on us
             if (self.mAuthScheme == "Digest") {
                 let realmChop = wwwauth.split("realm=\"")[1];
                 self.mAuthRealm = realmChop.split("\", ")[0];
                 cal.LOG("CalDAV: realm " + self.mAuthRealm);
             }
 
-            let str = cal.provider.convertByteArray(aResult, aResultLength);
+            let str = new TextDecoder().decode(aResult);
             if (!str || request.responseStatus == 404) {
                 // No response, or the calendar no longer exists.
                 cal.LOG("CalDAV: Failed to determine resource type for" +
                         self.name);
                 self.completeCheckServerInfo(aChangeLogListener,
                                              Components.interfaces.calIErrors.DAV_NOT_DAV);
                 return;
             } else if (self.verboseLogging()) {
@@ -1963,17 +1963,17 @@ calDavCalendar.prototype = {
             try {
                 dav = request.getResponseHeader("DAV");
                 if (self.verboseLogging()) {
                     cal.LOG("CalDAV: DAV header: " + dav);
                 }
             } catch (ex) {
                 cal.LOG("CalDAV: Error getting DAV header for " + self.name +
                         ", status " + request.responseStatus +
-                        ", data: " + cal.provider.convertByteArray(aResult, aResultLength));
+                        ", data: " + new TextDecoder().decode(aResult));
             }
             // Google does not yet support OPTIONS but does support scheduling
             // so we'll spoof the DAV header until Google gets fixed
             if (self.calendarUri.host == "www.google.com") {
                 dav = "calendar-schedule";
                 // Google also reports an inbox URL distinct from the calendar
                 // URL but a) doesn't use it and b) 405s on etag queries to it
                 self.mShouldPollInbox = false;
@@ -2059,17 +2059,17 @@ calDavCalendar.prototype = {
             if (request.responseStatus != 207) {
                 cal.LOG("CalDAV: Unexpected status " + request.responseStatus +
                     " while querying principal namespace for " + self.name);
                 self.completeCheckServerInfo(aChangeLogListener,
                                              Components.results.NS_ERROR_FAILURE);
                 return;
             }
 
-            let str = cal.provider.convertByteArray(aResult, aResultLength);
+            let str = new TextDecoder().decode(aResult);
             if (!str) {
                 cal.LOG("CalDAV: Failed to propstat principal namespace for " + self.name);
                 self.completeCheckServerInfo(aChangeLogListener,
                                              Components.results.NS_ERROR_FAILURE);
                 return;
             } else if (self.verboseLogging()) {
                 cal.LOG("CalDAV: recv: " + str);
             }
@@ -2176,17 +2176,17 @@ calDavCalendar.prototype = {
 
         if (this.verboseLogging()) {
             cal.LOG("CalDAV: send: " + queryMethod + " " + requestUri.spec + "\n" + queryXml);
         }
 
         let streamListener = {};
         streamListener.onStreamComplete = function(aLoader, aContext, aStatus, aResultLength, aResult) {
             let request = aLoader.request.QueryInterface(Components.interfaces.nsIHttpChannel);
-            let str = cal.provider.convertByteArray(aResult, aResultLength);
+            let str = new TextDecoder().decode(aResult);
             if (!str) {
                 cal.LOG("CalDAV: Failed to report principals namespace for " + self.name);
                 doesntSupportScheduling();
                 return;
             } else if (self.verboseLogging()) {
                 cal.LOG("CalDAV: recv: " + str);
             }
 
@@ -2447,17 +2447,17 @@ calDavCalendar.prototype = {
                     ",Recipient=" + mailto_aCalId + "): " + fbQuery);
         }
 
         let streamListener = {};
 
         streamListener.onStreamComplete = function(aLoader, aContext, aStatus,
                                                    aResultLength, aResult) {
             let request = aLoader.request.QueryInterface(Components.interfaces.nsIHttpChannel);
-            let str = cal.provider.convertByteArray(aResult, aResultLength);
+            let str = new TextDecoder().decode(aResult);
             if (!str) {
                 cal.LOG("CalDAV: Failed to parse freebusy response from " + self.name);
             } else if (self.verboseLogging()) {
                 cal.LOG("CalDAV: recv: " + str);
             }
 
             if (request.responseStatus == 200) {
                 let periodsToReturn = [];
@@ -2806,17 +2806,22 @@ calDavCalendar.prototype = {
                                 self.name);
                     }
 
                     if (status != 200) {
                         cal.LOG("CalDAV: Sending iTIP failed with status " + status +
                                 " for " + self.name);
                     }
 
-                    let str = cal.provider.convertByteArray(aResult, aResultLength, "UTF-8", false);
+                    let str;
+                    try {
+                        str = new TextDecoder().decode(aResult);
+                    } catch (e) {
+                        str = null;
+                    }
                     if (str) {
                         if (self.verboseLogging()) {
                             cal.LOG("CalDAV: recv: " + str);
                         }
                     } else {
                         cal.LOG("CalDAV: Failed to parse iTIP response for" +
                                 self.name);
                     }
diff --git a/calendar/providers/gdata/modules/gdataRequest.jsm b/calendar/providers/gdata/modules/gdataRequest.jsm
--- a/calendar/providers/gdata/modules/gdataRequest.jsm
+++ b/calendar/providers/gdata/modules/gdataRequest.jsm
@@ -347,17 +347,17 @@ calGoogleRequest.prototype = {
         if (!aResult || !Components.isSuccessCode(aStatus)) {
             this.fail(aStatus, aResult);
             return;
         }
 
         let httpChannel = aLoader.request.QueryInterface(Components.interfaces.nsIHttpChannel);
 
         // Convert the stream, falling back to utf-8 in case its not given.
-        let result = cal.provider.convertByteArray(aResult, aResultLength, httpChannel.contentCharset);
+        let result = new TextDecoder(httpChannel.contentCharset).decode(aResult);
         if (result === null) {
             this.fail(Components.results.NS_ERROR_FAILURE,
                       "Could not convert bytestream to Unicode: " + e);
             return;
         }
 
         let objData;
         try {
diff --git a/calendar/providers/ics/calICSCalendar.js b/calendar/providers/ics/calICSCalendar.js
--- a/calendar/providers/ics/calICSCalendar.js
+++ b/calendar/providers/ics/calICSCalendar.js
@@ -231,23 +231,19 @@ calICSCalendar.prototype = {
             this.mObserver.onLoad(this);
             this.unlock();
             return;
         }
 
         // This conversion is needed, because the stream only knows about
         // byte arrays, not about strings or encodings. The array of bytes
         // need to be interpreted as utf8 and put into a javascript string.
-        let unicodeConverter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"]
-                                         .createInstance(Components.interfaces.nsIScriptableUnicodeConverter);
-        // ics files are always utf8
-        unicodeConverter.charset = "UTF-8";
         let str;
         try {
-            str = unicodeConverter.convertFromByteArray(result, result.length);
+            str = new TextDecoder().decode(result);
         } catch (e) {
             this.mObserver.onError(this.superCalendar, calIErrors.CAL_UTF8_DECODING_FAILED, e.toString());
             this.mObserver.onError(this.superCalendar, calIErrors.READ_FAILED, "");
             this.unlock();
             return;
         }
 
         this.createMemoryCalendar();
@@ -999,24 +995,19 @@ httpHooks.prototype = {
             // because there is a time in which we don't know the right
             // etag.
             // Try to do the best we can, by immediatly getting the etag.
 
             let etagListener = {};
             let self = this; // need to reference in callback
 
             etagListener.onStreamComplete = function(aLoader, aContext, aStatus, aResultLength, aResult) {
-                let resultConverter = Components.classes["@mozilla.org/intl/scriptableunicodeconverter"]
-                                                .createInstance(Components
-                                                .interfaces.nsIScriptableUnicodeConverter);
-                resultConverter.charset = "UTF-8";
-
                 let multistatus;
                 try {
-                    let str = resultConverter.convertFromByteArray(aResult, aResultLength);
+                    let str = new TextDecoder().decode(aResult);
                     multistatus = cal.xml.parseString(str);
                 } catch (ex) {
                     cal.LOG("[calICSCalendar] Failed to fetch channel etag");
                 }
 
                 self.mEtag = icsXPathFirst(multistatus, "/D:propfind/D:response/D:propstat/D:prop/D:getetag");
                 aRespFunc();
             };
