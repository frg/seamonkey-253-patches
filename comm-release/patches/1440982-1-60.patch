# HG changeset patch
# User Philipp Kewisch <mozilla@kewis.ch>
# Date 1519506604 -3600
#      Sa Feb 24 22:10:04 2018 +0100
# Node ID 3e4b5f228311cf6cf2d94a63a5e5e14f1f1b5768
# Parent  2d8bbefeb737260fac780f8ac8ac23b28ad1a130
Bug 1440982 - Move calIteratorUtils to cal.iterate namespace - manual changes. r=MakeMyDay

MozReview-Commit-ID: SVOlLq7xjo

diff --git a/calendar/.eslintrc.js b/calendar/.eslintrc.js
--- a/calendar/.eslintrc.js
+++ b/calendar/.eslintrc.js
@@ -477,16 +477,17 @@ module.exports = {
         // The following rules will not be enabled currently, but are kept here for
         // easier updates in the future.
         "no-else-return": 0,
     },
     "overrides": [{
         files: [
             "base/modules/calAuthUtils.jsm",
             "base/modules/calEmailUtils.jsm",
+            "base/modules/calIteratorUtils.jsm",
             "base/modules/calItipUtils.jsm",
             "base/modules/calUnifinderUtils.jsm",
             "base/modules/calL10NUtils.jsm",
             "base/modules/calProviderUtils.jsm",
         ],
         rules: {
             "require-jsdoc": [2, { require: { ClassDeclaration: true } }],
 
diff --git a/calendar/base/content/dialogs/calendar-dialog-utils.js b/calendar/base/content/dialogs/calendar-dialog-utils.js
--- a/calendar/base/content/dialogs/calendar-dialog-utils.js
+++ b/calendar/base/content/dialogs/calendar-dialog-utils.js
@@ -5,20 +5,20 @@
 /* exported gInTab, gMainWindow, gTabmail, intializeTabOrWindowVariables,
  *          dispose, setDialogId, loadReminders, saveReminder,
  *          commonUpdateReminder, updateLink, rearrangeAttendees,
  *          adaptScheduleAgent
  */
 
 ChromeUtils.import("resource://gre/modules/PluralForm.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
+
 ChromeUtils.import("resource:///modules/iteratorUtils.jsm");
 
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
 ChromeUtils.import("resource://calendar/modules/calRecurrenceUtils.jsm");
 
 // Variables related to whether we are in a tab or a window dialog.
 var gInTab = false;
 var gMainWindow = null;
 var gTabmail = null;
 
 /**
diff --git a/calendar/base/content/dialogs/calendar-event-dialog-reminder.js b/calendar/base/content/dialogs/calendar-event-dialog-reminder.js
--- a/calendar/base/content/dialogs/calendar-event-dialog-reminder.js
+++ b/calendar/base/content/dialogs/calendar-event-dialog-reminder.js
@@ -1,21 +1,21 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* exported onLoad, onReminderSelected, updateReminder, onNewReminder,
  *          onRemoveReminder, onAccept, onCancel
  */
 
-ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
 ChromeUtils.import("resource://gre/modules/PluralForm.jsm");
 ChromeUtils.import("resource://gre/modules/Preferences.jsm");
 
+ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
+
 var allowedActionsMap = {};
 
 /**
  * Sets up the reminder dialog.
  */
 function onLoad() {
     let calendar = window.arguments[0].calendar;
 
diff --git a/calendar/base/modules/calItemUtils.jsm b/calendar/base/modules/calItemUtils.jsm
--- a/calendar/base/modules/calItemUtils.jsm
+++ b/calendar/base/modules/calItemUtils.jsm
@@ -450,17 +450,17 @@ var calitem = {
                 comps.push(normalizeComponent(subcomp));
             }
             comps = comps.sort();
 
             return comp.componentType + props.join("\r\n") + comps.join("\r\n");
         }
 
         function normalizeProperty(prop) {
-            let params = [...cal.ical.paramIterator(prop)]
+            let params = [...cal.iterate.icalParameter(prop)]
                 .filter(([k, v]) => !(prop.propertyName in ignoreParams) ||
                        !(k in ignoreParams[prop.propertyName]))
                 .map(([k, v]) => k + "=" + v)
                 .sort();
 
             return prop.propertyName + ";" +
                    params.join(";") + ":" +
                    prop.valueAsIcalString;
diff --git a/calendar/base/modules/calIteratorUtils.jsm b/calendar/base/modules/calIteratorUtils.jsm
--- a/calendar/base/modules/calIteratorUtils.jsm
+++ b/calendar/base/modules/calIteratorUtils.jsm
@@ -1,185 +1,182 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
+ChromeUtils.import("resource://gre/modules/Preferences.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
-ChromeUtils.import("resource://gre/modules/Preferences.jsm");
+ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
-this.EXPORTED_SYMBOLS = ["cal"]; // even though it's defined in calUtils.jsm, import needs this
+XPCOMUtils.defineLazyModuleGetter(this, "cal", "resource://calendar/modules/calUtils.jsm", "cal");
+
+this.EXPORTED_SYMBOLS = ["caliterate"]; /* exported caliterate */
 
-/**
- * Iterates an array of items, i.e. the passed item including all
- * overridden instances of a recurring series.
- *
- * @param items array of items
- */
-cal.itemIterator = function* (items) {
-    for (let item of items) {
-        yield item;
-        let rec = item.recurrenceInfo;
-        if (rec) {
-            for (let exid of rec.getExceptionIds({})) {
-                yield rec.getExceptionFor(exid);
+var caliterate = {
+    /**
+     * Iterates an array of items, i.e. the passed item including all
+     * overridden instances of a recurring series.
+     *
+     * @param {calIItemBase[]} items        array of items to iterate
+     * @yields {calIItemBase}
+     */
+    items: function* (items) {
+        for (let item of items) {
+            yield item;
+            let rec = item.recurrenceInfo;
+            if (rec) {
+                for (let exid of rec.getExceptionIds({})) {
+                    yield rec.getExceptionFor(exid);
+                }
             }
         }
-    }
-};
-
-/**
- * Runs the body() function once for each item in the iterator using the event
- * queue to make sure other actions could run inbetween. When all iterations are
- * done (and also when cal.forEach.BREAK is returned), calls the completed()
- * function if passed.
- *
- * If you would like to break or continue inside the body(), return either
- *     cal.forEach.BREAK or cal.forEach.CONTINUE
- *
- * Note since the event queue is used, this function will return immediately,
- * before the iteration is complete. If you need to run actions after the real
- * for each loop, use the optional completed() function.
- *
- * @param iter          The Iterator or the plain Object to go through in this
- *                      loop.
- * @param body          The function called for each iteration. Its parameter is
- *                          the single item from the iterator.
- * @param completed     [optional] The function called after the loop completes.
- */
-cal.forEach = function(iterable, body, completed) {
-    // This should be a const one day, lets keep it a pref for now though until we
-    // find a sane value.
-    let LATENCY = Preferences.get("calendar.threading.latency", 250);
+    },
 
-    if (typeof iterable == "object" && !iterable[Symbol.iterator]) {
-        iterable = Object.entries(iterable);
-    }
-
-    let ourIter = iterable[Symbol.iterator]();
-    let currentThread = Services.tm.currentThread;
+    /**
+     * Runs the body() function once for each item in the iterator using the event queue to make
+     * sure other actions could run inbetween. When all iterations are done (and also when
+     * cal.iterate.forEach.BREAK is returned), calls the completed() function if passed.
+     *
+     * If you would like to break or continue inside the body(), return either
+     * cal.iterate.forEach.BREAK or cal.iterate.forEach.CONTINUE
+     *
+     * Note since the event queue is used, this function will return immediately, before the
+     * iteration is complete. If you need to run actions after the real for each loop, use the
+     * optional completed() function.
+     *
+     * @param {Iterable} iterable       The Iterator or the plain Object to go through in this loop.
+     * @param {Function} body           The function called for each iteration. Its parameter is the
+     *                                    single item from the iterator.
+     * @param {?Function} completed     [optional] The function called after the loop completes.
+     */
+    forEach: (() => {
+        // eslint-disable-next-line require-jsdoc
+        function forEach(iterable, body, completed=null) {
+            // This should be a const one day, lets keep it a pref for now though until we
+            // find a sane value.
+            let LATENCY = Preferences.get("calendar.threading.latency", 250);
 
-    // This is our dispatcher, it will be used for the iterations
-    let dispatcher = {
-        run: function() {
-            let startTime = (new Date()).getTime();
-            while (((new Date()).getTime() - startTime) < LATENCY) {
-                let next = ourIter.next();
-                let done = next.done;
-
-                if (!done) {
-                    let rc = body(next.value);
-                    if (rc == cal.forEach.BREAK) {
-                        done = true;
-                    }
-                }
-
-                if (done) {
-                    if (completed) {
-                        completed();
-                    }
-                    return;
-                }
+            if (typeof iterable == "object" && !iterable[Symbol.iterator]) {
+                iterable = Object.entries(iterable);
             }
 
-            currentThread.dispatch(this, currentThread.DISPATCH_NORMAL);
-        }
-    };
+            let ourIter = iterable[Symbol.iterator]();
+            let currentThread = Services.tm.currentThread;
 
-    currentThread.dispatch(dispatcher, currentThread.DISPATCH_NORMAL);
-};
+            // This is our dispatcher, it will be used for the iterations
+            let dispatcher = {
+                run: function() {
+                    let startTime = (new Date()).getTime();
+                    while (((new Date()).getTime() - startTime) < LATENCY) {
+                        let next = ourIter.next();
+                        let done = next.done;
+
+                        if (!done) {
+                            let rc = body(next.value);
+                            if (rc == cal.forEach.BREAK) {
+                                done = true;
+                            }
+                        }
 
-cal.forEach.CONTINUE = 1;
-cal.forEach.BREAK = 2;
+                        if (done) {
+                            if (completed) {
+                                completed();
+                            }
+                            return;
+                        }
+                    }
 
-/**
- * "ical" namespace. Used for all iterators (and possibly other functions) that
- * are related to libical.
- */
-cal.ical = {
+                    currentThread.dispatch(this, currentThread.DISPATCH_NORMAL);
+                }
+            };
+
+            currentThread.dispatch(dispatcher, currentThread.DISPATCH_NORMAL);
+        }
+        forEach.CONTINUE = 1;
+        forEach.BREAK = 2;
+
+        return forEach;
+    })(),
+
     /**
      *  Yields all subcomponents in all calendars in the passed component.
-     *  - If the passed component is an XROOT (contains multiple calendars),
-     *    then go through all VCALENDARs in it and get their subcomponents.
-     *  - If the passed component is a VCALENDAR, iterate through its direct
-     *    subcomponents.
-     *  - Otherwise assume the passed component is the item itself and yield
-     *    only the passed component.
+     *  - If the passed component is an XROOT (contains multiple calendars), then go through all
+     *    VCALENDARs in it and get their subcomponents.
+     *  - If the passed component is a VCALENDAR, iterate through its direct subcomponents.
+     *  - Otherwise assume the passed component is the item itself and yield only the passed
+     *    component.
      *
      * This iterator can only be used in a for..of block:
      *   for (let component of cal.ical.calendarComponentIterator(aComp)) { ... }
      *
-     *  @param aComponent       The component to iterate given the above rules.
-     *  @param aCompType        The type of item to iterate.
-     *  @return                 The iterator that yields all items.
+     *  @param {calIIcalComponent} aComponent   The component to iterate given the above rules.
+     *  @param {String} aCompType               The type of item to iterate.
+     *  @yields {calIIcalComponent}             The iterator that yields all items.
      */
-    calendarComponentIterator: function* (aComponent, aCompType) {
-        let compType = (aCompType || "ANY");
+    icalComponent: function* (aComponent, aCompType="ANY") {
         if (aComponent && aComponent.componentType == "VCALENDAR") {
-            yield* cal.ical.subcomponentIterator(aComponent, compType);
+            yield* cal.ical.subcomponentIterator(aComponent, aCompType);
         } else if (aComponent && aComponent.componentType == "XROOT") {
             for (let calComp of cal.ical.subcomponentIterator(aComponent, "VCALENDAR")) {
-                yield* cal.ical.subcomponentIterator(calComp, compType);
+                yield* cal.ical.subcomponentIterator(calComp, aCompType);
             }
-        } else if (aComponent && (compType == "ANY" || compType == aComponent.componentType)) {
+        } else if (aComponent && (aCompType == "ANY" || aCompType == aComponent.componentType)) {
             yield aComponent;
         }
     },
 
     /**
-     * Use to iterate through all subcomponents of a calIIcalComponent. This
-     * iterators depth is 1, this means no sub-sub-components will be iterated.
+     * Use to iterate through all subcomponents of a calIIcalComponent. This iterators depth is 1,
+     * this means no sub-sub-components will be iterated.
      *
      * This iterator can only be used in a for() block:
-     *   for (let component in cal.ical.subcomponentIterator(aComp)) { ... }
+     *   for (let component of cal.ical.subcomponentIterator(aComp)) { ... }
      *
-     * @param aComponent        The component who's subcomponents to iterate.
-     * @param aSubcomp          (optional) the specific subcomponent to
-     *                            enumerate. If not given, "ANY" will be used.
-     * @return                  An iterator object to iterate the properties.
+     * @param {calIIcalComponent} aComponent    The component who's subcomponents to iterate.
+     * @param {?String} aSubcomp                (optional) the specific subcomponent to enumerate.
+     *                                            If not given, "ANY" will be used.
+     * @yields {calIIcalComponent}              An iterator object to iterate the properties.
      */
-    subcomponentIterator: function* (aComponent, aSubcomp) {
-        let subcompName = (aSubcomp || "ANY");
-        for (let subcomp = aComponent.getFirstSubcomponent(subcompName);
+    icalSubcomponent: function* (aComponent, aSubcomp="ANY") {
+        for (let subcomp = aComponent.getFirstSubcomponent(aSubcomp);
              subcomp;
-             subcomp = aComponent.getNextSubcomponent(subcompName)) {
+             subcomp = aComponent.getNextSubcomponent(aSubcomp)) {
             yield subcomp;
         }
     },
 
     /**
      * Use to iterate through all properties of a calIIcalComponent.
      * This iterator can only be used in a for() block:
-     *   for (let property in cal.ical.propertyIterator(aComp)) { ... }
+     *   for (let property of cal.ical.propertyIterator(aComp)) { ... }
      *
-     * @param aComponent        The component to iterate.
-     * @param aProperty         (optional) the specific property to enumerate.
-     *                            If not given, "ANY" will be used.
-     * @return                  An iterator object to iterate the properties.
+     * @param {calIIcalComponent} aComponent    The component to iterate.
+     * @param {?String} aProperty               (optional) the specific property to enumerate.
+     *                                            If not given, "ANY" will be used.
+     * @yields {calIIcalProperty}               An iterator object to iterate the properties.
      */
-    propertyIterator: function* (aComponent, aProperty) {
-        let propertyName = (aProperty || "ANY");
-        for (let prop = aComponent.getFirstProperty(propertyName);
+    icalProperty: function* (aComponent, aProperty="ANY") {
+        for (let prop = aComponent.getFirstProperty(aProperty);
              prop;
-             prop = aComponent.getNextProperty(propertyName)) {
+             prop = aComponent.getNextProperty(aProperty)) {
             yield prop;
         }
     },
 
     /**
      * Use to iterate through all parameters of a calIIcalProperty.
      * This iterator behaves similar to the object iterator. Possible uses:
      *   for (let paramName in cal.ical.paramIterator(prop)) { ... }
      * or:
      *   for (let [paramName, paramValue] of cal.ical.paramIterator(prop)) { ... }
      *
-     * @param aProperty         The property to iterate.
-     * @return                  An iterator object to iterate the properties.
+     * @param {calIIcalProperty} aProperty         The property to iterate.
+     * @yields {[String, String]}                  An iterator object to iterate the properties.
      */
-    paramIterator: function* (aProperty) {
+    icalParameter: function* (aProperty) {
         let paramSet = new Set();
         for (let paramName = aProperty.getFirstParameterName();
              paramName;
              paramName = aProperty.getNextParameterName()) {
             // Workaround to avoid infinite loop when the property
             // contains duplicate parameters (bug 875739 for libical)
             if (!paramSet.has(paramName)) {
                 yield [paramName, aProperty.getParameter(paramName)];
diff --git a/calendar/base/modules/calItipUtils.jsm b/calendar/base/modules/calItipUtils.jsm
--- a/calendar/base/modules/calItipUtils.jsm
+++ b/calendar/base/modules/calItipUtils.jsm
@@ -1,17 +1,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
+ChromeUtils.import("resource://gre/modules/Preferences.jsm");
+ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
+
 ChromeUtils.import("resource:///modules/mailServices.js");
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
-ChromeUtils.import("resource://gre/modules/Preferences.jsm");
-ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 /**
  * Scheduling and iTIP helper code
  */
 this.EXPORTED_SYMBOLS = ["calitip"]; /* exported calitip */
 var calitip = {
     /**
      * Gets the sequence/revision number, either of the passed item or the last received one of an
diff --git a/calendar/base/modules/calUtils.jsm b/calendar/base/modules/calUtils.jsm
--- a/calendar/base/modules/calUtils.jsm
+++ b/calendar/base/modules/calUtils.jsm
@@ -361,16 +361,17 @@ XPCOMUtils.defineLazyModuleGetter(cal, "
 XPCOMUtils.defineLazyModuleGetter(cal, "alarms", "resource://calendar/modules/calAlarmUtils.jsm", "calalarms");
 XPCOMUtils.defineLazyModuleGetter(cal, "async", "resource://calendar/modules/calAsyncUtils.jsm", "calasync");
 XPCOMUtils.defineLazyModuleGetter(cal, "auth", "resource://calendar/modules/calAuthUtils.jsm", "calauth");
 XPCOMUtils.defineLazyModuleGetter(cal, "category", "resource://calendar/modules/calCategoryUtils.jsm", "calcategory");
 XPCOMUtils.defineLazyModuleGetter(cal, "data", "resource://calendar/modules/calDataUtils.jsm", "caldata");
 XPCOMUtils.defineLazyModuleGetter(cal, "dtz", "resource://calendar/modules/calDateTimeUtils.jsm", "caldtz");
 XPCOMUtils.defineLazyModuleGetter(cal, "email", "resource://calendar/modules/calEmailUtils.jsm", "calemail");
 XPCOMUtils.defineLazyModuleGetter(cal, "item", "resource://calendar/modules/calItemUtils.jsm", "calitem");
+XPCOMUtils.defineLazyModuleGetter(cal, "iterate", "resource://calendar/modules/calIteratorUtils.jsm", "caliterate");
 XPCOMUtils.defineLazyModuleGetter(cal, "itip", "resource://calendar/modules/calItipUtils.jsm", "calitip");
 XPCOMUtils.defineLazyModuleGetter(cal, "l10n", "resource://calendar/modules/calL10NUtils.jsm", "call10n");
 XPCOMUtils.defineLazyModuleGetter(cal, "print", "resource://calendar/modules/calPrintUtils.jsm", "calprint");
 XPCOMUtils.defineLazyModuleGetter(cal, "provider", "resource://calendar/modules/calProviderUtils.jsm", "calprovider");
 XPCOMUtils.defineLazyModuleGetter(cal, "unifinder", "resource://calendar/modules/calUnifinderUtils.jsm", "calunifinder");
 XPCOMUtils.defineLazyModuleGetter(cal, "view", "resource://calendar/modules/calViewUtils.jsm", "calview");
 XPCOMUtils.defineLazyModuleGetter(cal, "window", "resource://calendar/modules/calWindowUtils.jsm", "calwindow");
 XPCOMUtils.defineLazyModuleGetter(cal, "xml", "resource://calendar/modules/calXMLUtils.jsm", "calxml");
diff --git a/calendar/base/modules/calUtilsCompat.jsm b/calendar/base/modules/calUtilsCompat.jsm
--- a/calendar/base/modules/calUtilsCompat.jsm
+++ b/calendar/base/modules/calUtilsCompat.jsm
@@ -73,16 +73,26 @@ var migrations = {
         isEventCalendar: "isEventCalendar",
         isTaskCalendar: "isTaskCalendar",
         isEvent: "isEvent",
         isToDo: "isToDo",
         checkIfInRange: "checkIfInRange",
         setItemProperty: "setItemProperty",
         getEventDefaultTransparency: "getEventDefaultTransparency"
     },
+    iterate: {
+        itemIterator: "items",
+        forEach: "forEach",
+        ical: {
+            calendarComponentIterator: "items",
+            subcomponentIterator: "icalSubcomponent",
+            propertyIterator: "icalProperty",
+            paramIterator: "icalParameter"
+        }
+    },
     itip: {
         getPublishLikeItemCopy: "getPublishLikeItemCopy",
         isInvitation: "isInvitation",
         isOpenInvitation: "isOpenInvitation",
         resolveDelegation: "resolveDelegation",
         getInvitedAttendee: "getInvitedAttendee",
         getAttendeesBySender: "getAttendeesBySender"
     },
@@ -119,34 +129,35 @@ var migrations = {
     window: {
         openCalendarWizard: "openCalendarWizard",
         openCalendarProperties: "openCalendarProperties",
         calPrint: "openPrintDialog",
         getCalendarWindow: "getCalendarWindow"
     }
 };
 
+
 /**
  * Generate a forward function on the given global, for the namespace from the
  * migrations data.
  *
  * @param global        The global object to inject on.
  * @param namespace     The new namespace in the cal object.
  * @param from          The function/property name being migrated from
  * @param to            The function/property name being migrated to
  */
-function generateForward(global, namespace, from, to) {
+function generateForward(global, namespace, from, to, targetGlobal=null) {
     // Protect from footguns
     if (typeof global[from] != "undefined") {
         throw new Error(from + " is already defined on the cal. namespace!");
     }
 
     global[from] = function(...args) {
         let suffix = "";
-        let target = global[namespace][to];
+        let target = (targetGlobal || global)[namespace][to];
         if (typeof target == "function") {
             target = target(...args);
             suffix = "()";
         }
 
         Deprecated.warning(`calUtils' cal.${from}() has changed to cal.${namespace}.${to}${suffix}`,
                            "https://bugzilla.mozilla.org/show_bug.cgi?id=905097",
                            Components.stack.caller);
@@ -158,17 +169,24 @@ function generateForward(global, namespa
 /**
  * Inject the backwards compatibility functions using above migration data
  *
  * @param global        The global object to inject on.
  */
 function injectCalUtilsCompat(global) {
     for (let [namespace, nsdata] of Object.entries(migrations)) {
         for (let [from, to] of Object.entries(nsdata)) {
-            generateForward(global, namespace, from, to);
+            if (typeof to == "object") {
+                global[from] = {};
+                for (let [frominner, toinner] of Object.entries(to)) {
+                    generateForward(global[from], namespace, frominner, toinner, global);
+                }
+            } else {
+                generateForward(global, namespace, from, to);
+            }
         }
     }
 
     // calGetString is special, as the argument order and kind has changed as well
     global.calGetString = function(aBundleName, aStringName, aParams, aComponent="calendar") {
         Deprecated.warning("calUtils' cal.calGetString() has changed to cal.l10n.get*String()" +
                            " and the parameter order has changed",
                            "https://bugzilla.mozilla.org/show_bug.cgi?id=905097",
diff --git a/calendar/base/src/calAttachment.js b/calendar/base/src/calAttachment.js
--- a/calendar/base/src/calAttachment.js
+++ b/calendar/base/src/calAttachment.js
@@ -1,17 +1,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
+ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
+
 //
 // calAttachment.js
 //
 function calAttachment() {
     this.wrappedJSObject = this;
     this.mProperties = new cal.data.PropertyMap();
 }
 
diff --git a/calendar/base/src/calAttendee.js b/calendar/base/src/calAttendee.js
--- a/calendar/base/src/calAttendee.js
+++ b/calendar/base/src/calAttendee.js
@@ -1,16 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
 
 function calAttendee() {
     this.wrappedJSObject = this;
     this.mProperties = new cal.data.PropertyMap();
 }
 
 var calAttendeeClassID = Components.ID("{5c8dcaa3-170c-4a73-8142-d531156f664d}");
 var calAttendeeInterfaces = [Components.interfaces.calIAttendee];
diff --git a/calendar/base/src/calIcsParser.js b/calendar/base/src/calIcsParser.js
--- a/calendar/base/src/calIcsParser.js
+++ b/calendar/base/src/calIcsParser.js
@@ -1,15 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
+ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
+
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
-ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 function calIcsParser() {
     this.wrappedJSObject = this;
     this.mItems = [];
     this.mParentlessItems = [];
     this.mComponents = [];
     this.mProperties = [];
 }
diff --git a/calendar/base/src/calIcsSerializer.js b/calendar/base/src/calIcsSerializer.js
--- a/calendar/base/src/calIcsSerializer.js
+++ b/calendar/base/src/calIcsSerializer.js
@@ -1,14 +1,13 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
 
 function calIcsSerializer() {
     this.wrappedJSObject = this;
     this.mItems = [];
     this.mProperties = [];
     this.mComponents = [];
 }
 var calIcsSerializerClassID = Components.ID("{207a6682-8ff1-4203-9160-729ec28c8766}");
diff --git a/calendar/base/src/calItemBase.js b/calendar/base/src/calItemBase.js
--- a/calendar/base/src/calItemBase.js
+++ b/calendar/base/src/calItemBase.js
@@ -1,15 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
+ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
+
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
-ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 /**
  * calItemBase prototype definition
  *
  * @implements calIItemBase
  * @constructor
  */
 function calItemBase() {
diff --git a/calendar/base/src/calItipItem.js b/calendar/base/src/calItipItem.js
--- a/calendar/base/src/calItipItem.js
+++ b/calendar/base/src/calItipItem.js
@@ -1,16 +1,16 @@
 /* -*- Mode: javascript; tab-width: 20; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
+ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
+
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
-ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 /**
  * Constructor of calItipItem object
  */
 function calItipItem() {
     this.wrappedJSObject = this;
     this.mCurrentItemIndex = 0;
 }
diff --git a/calendar/base/src/calRelation.js b/calendar/base/src/calRelation.js
--- a/calendar/base/src/calRelation.js
+++ b/calendar/base/src/calRelation.js
@@ -1,15 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
+ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
+
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 /**
  * calRelation prototype definition
  *
  * @implements calIRelation
  * @constructor
  */
 function calRelation() {
diff --git a/calendar/base/src/calTimezoneService.js b/calendar/base/src/calTimezoneService.js
--- a/calendar/base/src/calTimezoneService.js
+++ b/calendar/base/src/calTimezoneService.js
@@ -1,20 +1,20 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 ChromeUtils.import("resource://gre/modules/AddonManager.jsm");
+ChromeUtils.import("resource://gre/modules/NetUtil.jsm");
+ChromeUtils.import("resource://gre/modules/Preferences.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
-ChromeUtils.import("resource://gre/modules/Preferences.jsm");
+
+ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
 ChromeUtils.import("resource://calendar/modules/ical.js");
-ChromeUtils.import("resource://gre/modules/NetUtil.jsm");
 
 function calStringEnumerator(stringArray) {
     this.mIndex = 0;
     this.mStringArray = stringArray;
 }
 calStringEnumerator.prototype = {
     // nsIUTF8StringEnumerator:
     hasMore: function() {
diff --git a/calendar/providers/caldav/calDavCalendar.js b/calendar/providers/caldav/calDavCalendar.js
--- a/calendar/providers/caldav/calDavCalendar.js
+++ b/calendar/providers/caldav/calDavCalendar.js
@@ -5,17 +5,16 @@
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/Timer.jsm");
 ChromeUtils.import("resource://gre/modules/Preferences.jsm");
 
 ChromeUtils.import("resource:///modules/OAuth2.jsm");
 
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
 
 //
 // calDavCalendar.js
 //
 
 var xmlHeader = '<?xml version="1.0" encoding="UTF-8"?>\n';
 
 var davNS = "DAV:";
diff --git a/calendar/providers/memory/calMemoryCalendar.js b/calendar/providers/memory/calMemoryCalendar.js
--- a/calendar/providers/memory/calMemoryCalendar.js
+++ b/calendar/providers/memory/calMemoryCalendar.js
@@ -1,17 +1,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
+ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
-ChromeUtils.import("resource://gre/modules/Services.jsm");
 
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
 
 //
 // calMemoryCalendar.js
 //
 
 var cICL = Components.interfaces.calIChangeLog;
 
 function calMemoryCalendar() {
diff --git a/calendar/providers/wcap/calWcapCalendarItems.js b/calendar/providers/wcap/calWcapCalendarItems.js
--- a/calendar/providers/wcap/calWcapCalendarItems.js
+++ b/calendar/providers/wcap/calWcapCalendarItems.js
@@ -1,15 +1,14 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
 
 calWcapCalendar.prototype.encodeAttendee = function(att) {
     if (LOG_LEVEL > 2) {
         log("attendee.icalProperty.icalString=" + att.icalProperty.icalString, this);
     }
     function encodeAttr(val, attr, params) {
         if (val && val.length > 0) {
             if (params.length > 0) {
diff --git a/calendar/providers/wcap/calWcapSession.js b/calendar/providers/wcap/calWcapSession.js
--- a/calendar/providers/wcap/calWcapSession.js
+++ b/calendar/providers/wcap/calWcapSession.js
@@ -1,19 +1,19 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* exported getWcapSessionFor */
 
-ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
+ChromeUtils.import("resource://gre/modules/Preferences.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
-ChromeUtils.import("resource://gre/modules/Preferences.jsm");
+
+ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
 
 function calWcapTimezone(tzProvider, tzid_, component_) {
     this.wrappedJSObject = this;
     this.provider = tzProvider;
     this.icalComponent = component_;
     this.tzid = tzid_;
     this.isUTC = false;
     this.isFloating = false;
diff --git a/calendar/providers/wcap/calWcapUtils.js b/calendar/providers/wcap/calWcapUtils.js
--- a/calendar/providers/wcap/calWcapUtils.js
+++ b/calendar/providers/wcap/calWcapUtils.js
@@ -1,20 +1,20 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* exported getCalendarSearchService, getDomParser, isParent, filterXmlNodes,
  *          getIcalUTC, getDatetimeFromIcalProp, getWcapString
  */
 
-ChromeUtils.import("resource://calendar/modules/calIteratorUtils.jsm");
+ChromeUtils.import("resource://gre/modules/Preferences.jsm");
+ChromeUtils.import("resource://gre/modules/Services.jsm");
+
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
-ChromeUtils.import("resource://gre/modules/Services.jsm");
-ChromeUtils.import("resource://gre/modules/Preferences.jsm");
 
 var g_bShutdown = false;
 
 function initLogging() {
     initLogging.mLogTimezone = cal.dtz.defaultTimezone;
     if (initLogging.mLogFilestream) {
         try {
             initLogging.mLogFilestream.close();
