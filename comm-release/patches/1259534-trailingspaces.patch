# HG changeset patch
# User Jorg K <jorgk@jorgk.com>
# Date 1516131263 -3600
# Node ID a73bf68de26cb71ee54d91a002da3a9e628e9b23
# Parent  e88f09f8bff8add0b6efb80af6df6a4223002f79
Bug 1259534 - remove trailing spaces from nsMsgBodyHandler.cpp so patch can be applied. rs=white-space-only a=jorgk

diff --git a/mailnews/base/search/src/nsMsgBodyHandler.cpp b/mailnews/base/search/src/nsMsgBodyHandler.cpp
--- a/mailnews/base/search/src/nsMsgBodyHandler.cpp
+++ b/mailnews/base/search/src/nsMsgBodyHandler.cpp
@@ -26,25 +26,25 @@ nsMsgBodyHandler::nsMsgBodyHandler (nsIM
   uint32_t flags;
   m_lineCountInBodyLines = NS_SUCCEEDED(msg->GetFlags(&flags)) ?
     !(flags & nsMsgMessageFlags::Offline) : true;
   // account for added x-mozilla-status lines, and envelope line.
   if (!m_lineCountInBodyLines)
     m_numLocalLines += 3;
   m_msgHdr = msg;
   m_db = db;
-  
+
   // the following are variables used when the body handler is handling stuff from filters....through this constructor, that is not the
   // case so we set them to NULL.
   m_headers = NULL;
   m_headersSize = 0;
   m_Filtering = false; // make sure we set this before we call initialize...
-  
+
   Initialize();  // common initialization stuff
-  OpenLocalFolder();      
+  OpenLocalFolder();
 }
 
 nsMsgBodyHandler::nsMsgBodyHandler(nsIMsgSearchScopeTerm * scope,
                                    uint32_t numLines,
                                    nsIMsgDBHdr* msg, nsIMsgDatabase* db,
                                    const char * headers, uint32_t headersSize,
                                    bool Filtering)
 {
@@ -55,19 +55,19 @@ nsMsgBodyHandler::nsMsgBodyHandler(nsIMs
     !(flags & nsMsgMessageFlags::Offline) : true;
   // account for added x-mozilla-status lines, and envelope line.
   if (!m_lineCountInBodyLines)
     m_numLocalLines += 3;
   m_msgHdr = msg;
   m_db = db;
   m_headersSize = headersSize;
   m_Filtering = Filtering;
-  
+
   Initialize();
-  
+
   if (m_Filtering)
     m_headers = headers;
   else
     OpenLocalFolder();  // if nothing else applies, then we must be a POP folder file
 }
 
 void nsMsgBodyHandler::Initialize()
 // common initialization code regardless of what body type we are handling...
@@ -103,17 +103,17 @@ int32_t nsMsgBodyHandler::GetNextLine (n
       // 3 cases: Offline IMAP, POP, or we are dealing with a news message....
       // Offline cases should be same as local mail cases, since we're going
       // to store offline messages in berkeley format folders.
       if (m_db)
       {
          length = GetNextLocalLine (nextLine); // (2) POP
       }
     }
-    
+
     if (length < 0)
       break; // eof in
 
     outLength = ApplyTransformations(nextLine, length, eatThisLine, buf);
   }
 
   if (outLength < 0)
     return -1; // eof out
@@ -144,34 +144,34 @@ int32_t nsMsgBodyHandler::GetNextFilterL
 {
   // m_nextHdr always points to the next header in the list....the list is NULL terminated...
   uint32_t numBytesCopied = 0;
   if (m_headersSize > 0)
   {
     // #mscott. Ugly hack! filter headers list have CRs & LFs inside the NULL delimited list of header
     // strings. It is possible to have: To NULL CR LF From. We want to skip over these CR/LFs if they start
     // at the beginning of what we think is another header.
-    
+
     while (m_headersSize > 0 && (m_headers[0] == '\r' || m_headers[0] == '\n' || m_headers[0] == ' ' || m_headers[0] == '\0'))
     {
       m_headers++;  // skip over these chars...
       m_headersSize--;
     }
-    
+
     if (m_headersSize > 0)
     {
       numBytesCopied = strlen(m_headers) + 1 ;
       buf.Assign(m_headers);
-      m_headers += numBytesCopied;  
-      // be careful...m_headersSize is unsigned. Don't let it go negative or we overflow to 2^32....*yikes*  
+      m_headers += numBytesCopied;
+      // be careful...m_headersSize is unsigned. Don't let it go negative or we overflow to 2^32....*yikes*
       if (m_headersSize < numBytesCopied)
         m_headersSize = 0;
       else
         m_headersSize -= numBytesCopied;  // update # bytes we have read from the headers list
-      
+
       return (int32_t) numBytesCopied;
     }
   }
   else if (m_headersSize == 0) {
     buf.Truncate();
   }
   return -1;
 }
@@ -192,23 +192,23 @@ int32_t nsMsgBodyHandler::GetNextLocalLi
     if (m_fileLineStream)
     {
       bool more = false;
       nsresult rv = m_fileLineStream->ReadLine(buf, &more);
       if (NS_SUCCEEDED(rv))
         return buf.Length();
     }
   }
-  
+
   return -1;
 }
 
 /**
  * This method applies a sequence of transformations to the line.
- * 
+ *
  * It applies the following sequences in order
  * * Removes headers if the searcher doesn't want them
  *   (sets m_pastHeaders)
  * * Determines the current MIME type.
  *   (via SniffPossibleMIMEHeader)
  * * Strips any HTML if the searcher doesn't want it
  * * Strips non-text parts
  * * Decodes any base64 part
@@ -223,38 +223,38 @@ int32_t nsMsgBodyHandler::GetNextLocalLi
  *                            redundant version of line).
  * @return            the length of the line after applying transformations
  */
 int32_t nsMsgBodyHandler::ApplyTransformations (const nsCString &line, int32_t length,
                                                 bool &eatThisLine, nsCString &buf)
 {
   int32_t newLength = length;
   eatThisLine = false;
-  
+
   if (!m_pastHeaders)  // line is a line from the message headers
   {
     if (m_stripHeaders)
       eatThisLine = true;
 
     // We have already grabbed all worthwhile information from the headers,
     // so there is no need to keep track of the current lines
     buf.Assign(line);
-   
+
     SniffPossibleMIMEHeader(buf);
-    
+
     m_pastHeaders = buf.IsEmpty() || buf.First() == '\r' ||
       buf.First() == '\n';
 
     return length;
   }
 
   // Check to see if this is the boundary string
   if (m_isMultipart && StringBeginsWith(line, boundary))
   {
-    if (m_base64part && m_partIsText) 
+    if (m_base64part && m_partIsText)
     {
       Base64Decode(buf);
       // Work on the parsed string
       if (!buf.Length())
       {
         NS_WARNING("Trying to transform an empty buffer");
         eatThisLine = true;
       }
@@ -274,67 +274,67 @@ int32_t nsMsgBodyHandler::ApplyTransform
     // Reset all assumed headers
     m_base64part = false;
     m_pastHeaders = false;
     m_partIsHtml = false;
     m_partIsText = true;
 
     return buf.Length();
   }
- 
+
   if (!m_partIsText)
   {
     // Ignore non-text parts
     buf.Truncate();
     eatThisLine = true;
     return 0;
   }
 
   if (m_base64part)
   {
     // We need to keep track of all lines to parse base64encoded...
     buf.Append(line.get());
     eatThisLine = true;
     return buf.Length();
   }
-    
+
   // ... but there's no point if we're not parsing base64.
   buf.Assign(line);
   if (m_stripHtml && m_partIsHtml)
   {
     StripHtml (buf);
     newLength = buf.Length();
   }
-  
+
   return newLength;
 }
 
 void nsMsgBodyHandler::StripHtml (nsCString &pBufInOut)
 {
   char *pBuf = (char*) PR_Malloc (pBufInOut.Length() + 1);
   if (pBuf)
   {
     char *pWalk = pBuf;
-    
+
     char *pWalkInOut = (char *) pBufInOut.get();
     bool inTag = false;
     while (*pWalkInOut) // throw away everything inside < >
     {
       if (!inTag)
         if (*pWalkInOut == '<')
           inTag = true;
         else
           *pWalk++ = *pWalkInOut;
         else
           if (*pWalkInOut == '>')
             inTag = false;
       pWalkInOut++;
     }
     *pWalk = 0; // null terminator
-    
+
     pBufInOut.Adopt(pBuf);
   }
 }
 
 /**
  * Determines the MIME type, if present, from the current line.
  *
  * m_partIsHtml, m_isMultipart, m_partIsText, m_base64part, and boundary are
@@ -420,11 +420,11 @@ void nsMsgBodyHandler::Base64Decode (nsC
   while (offset != -1) {
     pBufInOut.Replace(offset, 1, ' ');
     offset = pBufInOut.FindChar('\n', offset);
   }
   offset = pBufInOut.FindChar('\r');
   while (offset != -1) {
     pBufInOut.Replace(offset, 1, ' ');
     offset = pBufInOut.FindChar('\r', offset);
-  } 
+  }
 }
 
