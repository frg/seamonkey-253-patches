# HG changeset patch
# User Jorg K <jorgk@jorgk.com>
# Date 1505507478 -7200
#      Fri Sep 15 22:31:18 2017 +0200
# Node ID 2660f4337132dfcb6d9d4ccefd365f4353eff28c
# Parent  45b7534bdc5653c2c186ef4c211d324741e164b3
Bug 1340972 - Part 18: Replace more error-prone Addref()/Release() with using RefPtr in mailnews/. r=aceman

diff --git a/mailnews/addrbook/src/nsAddrDatabase.cpp b/mailnews/addrbook/src/nsAddrDatabase.cpp
--- a/mailnews/addrbook/src/nsAddrDatabase.cpp
+++ b/mailnews/addrbook/src/nsAddrDatabase.cpp
@@ -590,24 +590,24 @@ NS_IMETHODIMP nsAddrDatabase::CloseMDB(b
 // a database without having a listener!
 // This is evil in the com world, but there are times we need to delete the file.
 NS_IMETHODIMP nsAddrDatabase::ForceClosed()
 {
     nsresult    err = NS_OK;
     nsCOMPtr<nsIAddrDatabase> aDb(do_QueryInterface(this, &err));
 
     // make sure someone has a reference so object won't get deleted out from under us.
-    AddRef();
+    NS_ADDREF_THIS();
     NotifyAnnouncerGoingAway();
     // OK, remove from cache first and close the store.
     RemoveFromCache(this);
 
     err = CloseMDB(false);    // since we're about to delete it, no need to commit.
     NS_IF_RELEASE(m_mdbStore);
-    Release();
+    NS_RELEASE_THIS();
     return err;
 }
 
 NS_IMETHODIMP nsAddrDatabase::Commit(uint32_t commitType)
 {
   nsresult err = NS_OK;
   nsIMdbThumb *commitThumb = nullptr;
 
diff --git a/mailnews/base/src/nsMsgFolderCompactor.cpp b/mailnews/base/src/nsMsgFolderCompactor.cpp
--- a/mailnews/base/src/nsMsgFolderCompactor.cpp
+++ b/mailnews/base/src/nsMsgFolderCompactor.cpp
@@ -458,26 +458,25 @@ nsresult nsFolderCompactState::StartComp
   // TODO: test whether sorting the messages (m_keyArray) by messageOffset
   // would improve performance on large files (less seeks).
   // The m_keyArray is in the order as stored in DB and on IMAP or News
   // the messages stored on the mbox file are not necessarily in the same order.
   if (m_size > 0)
   {
     nsCOMPtr<nsIURI> notUsed;
     ShowCompactingStatusMsg();
-    AddRef();
+    NS_ADDREF_THIS();
     rv = m_messageService->CopyMessages(m_size, m_keyArray->m_keys.Elements(),
                                         m_folder, this,
                                         false, nullptr, m_window,
                                         getter_AddRefs(notUsed));
   }
   else
   { // no messages to copy with
     FinishCompact();
-//    Release(); // we don't "own" ourselves yet.
   }
   return rv;
 }
 
 nsresult
 nsFolderCompactState::FinishCompact()
 {
   NS_ENSURE_TRUE(m_folder, NS_ERROR_NOT_INITIALIZED);
@@ -769,17 +768,17 @@ nsFolderCompactState::OnStopRequest(nsIR
     {
       // in case we're not getting an error, we still need to pretend we did get an error,
       // because the compact did not successfully complete.
       m_folder->NotifyCompactCompleted();
       CleanupTempFilesAfterError();
       ReleaseFolderLock();
     }
   }
-  Release(); // kill self
+  NS_RELEASE_THIS(); // kill self
   return status;
 }
 
 NS_IMETHODIMP
 nsFolderCompactState::OnDataAvailable(nsIRequest *request, nsISupports *ctxt,
                                       nsIInputStream *inStr,
                                       uint64_t sourceOffset, uint32_t count)
 {
@@ -1137,25 +1136,25 @@ nsOfflineStoreCompactState::OnStopReques
   rv = CopyNextMessage(done);
   if (done)
   {
     m_db->Commit(nsMsgDBCommitType::kCompressCommit);
     msgHdr = nullptr;
     // no more to copy finish it up
     ReleaseFolderLock();
     FinishCompact();
-    Release(); // kill self
+    NS_RELEASE_THIS(); // kill self
   }
 
 done:
   if (NS_FAILED(rv)) {
     m_status = rv; // set the status to rv so the destructor can remove the
                    // temp folder and database
     ReleaseFolderLock();
-    Release(); // kill self
+    NS_RELEASE_THIS(); // kill self
     return rv;
   }
   return rv;
 }
 
 
 nsresult
 nsOfflineStoreCompactState::FinishCompact()
@@ -1296,17 +1295,17 @@ nsFolderCompactState::EndCopy(nsISupport
   return NS_OK;
 }
 
 nsresult nsOfflineStoreCompactState::StartCompacting()
 {
   nsresult rv = NS_OK;
   if (m_size > 0 && m_curIndex == 0)
   {
-    AddRef(); // we own ourselves, until we're done, anyway.
+    NS_ADDREF_THIS(); // we own ourselves, until we're done, anyway.
     ShowCompactingStatusMsg();
     bool done = false;
     rv = CopyNextMessage(done);
     if (!done)
       return rv;
   }
   ReleaseFolderLock();
   FinishCompact();
diff --git a/mailnews/base/util/Services.cpp b/mailnews/base/util/Services.cpp
--- a/mailnews/base/util/Services.cpp
+++ b/mailnews/base/util/Services.cpp
@@ -38,22 +38,22 @@ public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIOBSERVER
 
   static void EnsureInitialized();
 private:
   ~ShutdownObserver() {}
 
   void ShutdownServices();
-  static ShutdownObserver *sShutdownObserver;
+  static RefPtr<ShutdownObserver> sShutdownObserver;
   static bool sShuttingDown;
 };
 
 bool ShutdownObserver::sShuttingDown = false;
-ShutdownObserver *ShutdownObserver::sShutdownObserver = nullptr;
+RefPtr<ShutdownObserver> ShutdownObserver::sShutdownObserver = nullptr;
 }
 
 #define MOZ_SERVICE(NAME, TYPE, CONTRACT_ID) \
   static TYPE *g##NAME = nullptr; \
   already_AddRefed<TYPE> Get##NAME() \
   { \
     ShutdownObserver::EnsureInitialized(); \
     if (!g##NAME) \
@@ -79,28 +79,26 @@ NS_IMETHODIMP ShutdownObserver::Observe(
 }
 
 void ShutdownObserver::EnsureInitialized()
 {
   MOZ_ASSERT(!sShuttingDown, "It is illegal to use this code after shutdown!");
   if (!sShutdownObserver)
   {
     sShutdownObserver = new ShutdownObserver;
-    sShutdownObserver->AddRef();
     nsCOMPtr<nsIObserverService> obs(mozilla::services::GetObserverService());
     MOZ_ASSERT(obs, "This should never be null");
     obs->AddObserver(sShutdownObserver, "xpcom-shutdown-threads", false);
   }
 }
 
 void ShutdownObserver::ShutdownServices()
 {
   sShuttingDown = true;
   MOZ_ASSERT(sShutdownObserver, "Shutting down twice?");
-  sShutdownObserver->Release();
   sShutdownObserver = nullptr;
 #define MOZ_SERVICE(NAME, TYPE, CONTRACT_ID) NS_IF_RELEASE(g##NAME);
 #include "mozilla/mailnews/ServiceList.h"
 #undef MOZ_SERVICE
 }
 
 } // namespace services
 } // namespace mozilla
diff --git a/mailnews/db/msgdb/src/nsDBFolderInfo.cpp b/mailnews/db/msgdb/src/nsDBFolderInfo.cpp
--- a/mailnews/db/msgdb/src/nsDBFolderInfo.cpp
+++ b/mailnews/db/msgdb/src/nsDBFolderInfo.cpp
@@ -206,17 +206,16 @@ nsDBFolderInfo::nsDBFolderInfo(nsMsgData
     }
   }
 
   m_mdb = mdb;
   if (mdb)
   {
     nsresult err;
 
-    //		mdb->AddRef();
     err = m_mdb->GetStore()->StringToToken(mdb->GetEnv(), kDBFolderInfoScope, &m_rowScopeToken);
     if (NS_SUCCEEDED(err))
     {
       err = m_mdb->GetStore()->StringToToken(mdb->GetEnv(), kDBFolderInfoTableKind, &m_tableKindToken);
       if (NS_SUCCEEDED(err))
       {
         gDBFolderInfoOID.mOid_Scope = m_rowScopeToken;
         gDBFolderInfoOID.mOid_Id = 1;
diff --git a/mailnews/db/msgdb/src/nsMsgDatabase.cpp b/mailnews/db/msgdb/src/nsMsgDatabase.cpp
--- a/mailnews/db/msgdb/src/nsMsgDatabase.cpp
+++ b/mailnews/db/msgdb/src/nsMsgDatabase.cpp
@@ -1444,17 +1444,17 @@ nsresult nsMsgDatabase::CloseMDB(bool co
 // force the database to close - this'll flush out anybody holding onto
 // a database without having a listener!
 // This is evil in the com world, but there are times we need to delete the file.
 NS_IMETHODIMP nsMsgDatabase::ForceClosed()
 {
   nsresult  err = NS_OK;
 
   // make sure someone has a reference so object won't get deleted out from under us.
-  AddRef();
+  NS_ADDREF_THIS();
   NotifyAnnouncerGoingAway();
   // make sure dbFolderInfo isn't holding onto mork stuff because mork db is going away
   if (m_dbFolderInfo)
     m_dbFolderInfo->ReleaseExternalReferences();
   m_dbFolderInfo = nullptr;
 
   err = CloseMDB(true);  // Backup DB will try to recover info, so commit
   ClearCachedObjects(true);
@@ -1473,17 +1473,17 @@ if (m_mdbAllMsgHeadersTable)
   {
     m_mdbStore->Release();
     m_mdbStore = nullptr;
   }
 
   // better not be any listeners, because we're going away.
   NS_ASSERTION(m_ChangeListeners.IsEmpty(), "shouldn't have any listeners left");
 
-  Release();
+  NS_RELEASE_THIS();
   return err;
 }
 
 NS_IMETHODIMP nsMsgDatabase::GetDBFolderInfo(nsIDBFolderInfo  **result)
 {
   if (!m_dbFolderInfo)
   {
     NS_ERROR("db must be corrupt");
@@ -4787,23 +4787,23 @@ nsresult nsMsgDatabase::AddNewThread(nsM
     threadKey = kTableKeyForThreadOne;
 
   nsresult err = msgHdr->GetSubject(getter_Copies(subject));
 
   err = CreateNewThread(threadKey, subject.get(), &threadHdr);
   msgHdr->SetThreadId(threadKey);
   if (threadHdr)
   {
-    threadHdr->AddRef();
+    NS_ADDREF(threadHdr);
     // err = msgHdr->GetSubject(subject);
     // threadHdr->SetThreadKey(msgHdr->m_messageKey);
     // threadHdr->SetSubject(subject.get());
     // need to add the thread table to the db.
     AddToThread(msgHdr, threadHdr, nullptr, false);
-    threadHdr->Release();
+    NS_RELEASE(threadHdr);
   }
   return err;
 }
 
 nsresult nsMsgDatabase::GetBoolPref(const char *prefName, bool *result)
 {
   bool prefValue = false;
   nsresult rv;
diff --git a/mailnews/db/msgdb/src/nsMsgHdr.cpp b/mailnews/db/msgdb/src/nsMsgHdr.cpp
--- a/mailnews/db/msgdb/src/nsMsgHdr.cpp
+++ b/mailnews/db/msgdb/src/nsMsgHdr.cpp
@@ -24,17 +24,17 @@ NS_IMPL_ISUPPORTS(nsMsgHdr, nsIMsgDBHdr)
 
 nsMsgHdr::nsMsgHdr(nsMsgDatabase *db, nsIMdbRow *dbRow)
 {
   m_mdb = db;
   Init();
   m_mdbRow = dbRow;
   if(m_mdb)
   {
-    m_mdb->AddRef();
+    NS_ADDREF(m_mdb);  // Released in DTOR.
     mdbOid outOid;
     if (dbRow && NS_SUCCEEDED(dbRow->GetOid(m_mdb->GetEnv(), &outOid)))
     {
       m_messageKey = outOid.mOid_Id;
       m_mdb->AddHdrToUseCache((nsIMsgDBHdr *) this, m_messageKey);
     }
   }
 }
diff --git a/mailnews/import/becky/src/nsBeckyStringBundle.cpp b/mailnews/import/becky/src/nsBeckyStringBundle.cpp
--- a/mailnews/import/becky/src/nsBeckyStringBundle.cpp
+++ b/mailnews/import/becky/src/nsBeckyStringBundle.cpp
@@ -65,12 +65,10 @@ nsBeckyStringBundle::FormatStringFromNam
                                        params,
                                        length,
                                        _retval);
 }
 
 void
 nsBeckyStringBundle::Cleanup(void)
 {
-  if (mBundle)
-    mBundle->Release();
-  mBundle = nullptr;
+  NS_IF_RELEASE(mBundle);
 }
diff --git a/mailnews/local/src/nsPop3IncomingServer.cpp b/mailnews/local/src/nsPop3IncomingServer.cpp
--- a/mailnews/local/src/nsPop3IncomingServer.cpp
+++ b/mailnews/local/src/nsPop3IncomingServer.cpp
@@ -485,19 +485,17 @@ nsPop3IncomingServer::VerifyLogon(nsIUrl
 }
 
 NS_IMETHODIMP nsPop3IncomingServer::DownloadMailFromServers(nsIPop3IncomingServer** aServers,
                               uint32_t aCount,
                               nsIMsgWindow *aMsgWindow,
                               nsIMsgFolder *aFolder,
                               nsIUrlListener *aUrlListener)
 {
-  nsPop3GetMailChainer *getMailChainer = new nsPop3GetMailChainer;
-  NS_ENSURE_TRUE(getMailChainer, NS_ERROR_OUT_OF_MEMORY);
-  getMailChainer->AddRef(); // this object owns itself and releases when done.
+  RefPtr<nsPop3GetMailChainer> getMailChainer = new nsPop3GetMailChainer;
   return getMailChainer->GetNewMailForServers(aServers, aCount, aMsgWindow, aFolder, aUrlListener);
 }
 
 NS_IMETHODIMP nsPop3IncomingServer::GetNewMail(nsIMsgWindow *aMsgWindow,
                                                nsIUrlListener *aUrlListener,
                                                nsIMsgFolder *aInbox,
                                                nsIURI **aResult)
 {
@@ -530,19 +528,17 @@ nsPop3IncomingServer::GetNewMessages(nsI
 
   if (deferredToAccount.IsEmpty())
   {
     aFolder->GetServer(getter_AddRefs(server));
     GetDeferredServers(server, deferredServers);
   }
   if (deferredToAccount.IsEmpty() && !deferredServers.IsEmpty())
   {
-    nsPop3GetMailChainer *getMailChainer = new nsPop3GetMailChainer;
-    NS_ENSURE_TRUE(getMailChainer, NS_ERROR_OUT_OF_MEMORY);
-    getMailChainer->AddRef(); // this object owns itself and releases when done.
+    RefPtr<nsPop3GetMailChainer> getMailChainer = new nsPop3GetMailChainer;
     deferredServers.InsertElementAt(0, this);
     return getMailChainer->GetNewMailForServers(deferredServers.Elements(),
           deferredServers.Length(), aMsgWindow, inbox, aUrlListener);
   }
   if (m_runningProtocol)
     return NS_MSG_FOLDER_BUSY;
 
   return pop3Service->GetNewMail(aMsgWindow, aUrlListener, inbox, this, getter_AddRefs(url));
@@ -734,11 +730,10 @@ nsresult nsPop3GetMailChainer::RunNextGe
           nsCOMPtr<nsIPop3Service> pop3Service = do_GetService(kCPop3ServiceCID, &rv);
           NS_ENSURE_SUCCESS(rv,rv);
           return pop3Service->GetNewMail(m_downloadingMsgWindow, this, m_folderToDownloadTo, popServer, getter_AddRefs(url));
         }
       }
     }
   }
   rv = m_listener ? m_listener->OnStopRunningUrl(nullptr, NS_OK) : NS_OK;
-  Release(); // release ref to ourself.
   return rv;
 }
