# HG changeset patch
# User Ian Neal <iann_cvs@blueyonder.co.uk>
# Date 1613685107 0
# Parent  ffd913445eef97426b85f723156f7178a277837f
Bug 1628671 - When I try to open email Compose from command line, 'from=' option is ignored. r=frg a=frg

Port to SeaMonkey the relevant parts of the following bugs:
* Bug 394216 - Search for correct identity when draft identity key does not match From: address.
* Bug 882104 - Implement command line option to read message body from file.
* Bug 1266916 - Obey paragraph mode in new message invoked from mailto: URL.
* Bug 1310738 - Improve user experience for email creation on command line.
* Bug 1336531 - Don't put <br> before signatures in paragraph mode.
* Bug 1482058 - fix 'edit as new message' not working when identity email is null.

diff --git a/suite/locales/en-US/chrome/mailnews/compose/composeMsgs.properties b/suite/locales/en-US/chrome/mailnews/compose/composeMsgs.properties
--- a/suite/locales/en-US/chrome/mailnews/compose/composeMsgs.properties
+++ b/suite/locales/en-US/chrome/mailnews/compose/composeMsgs.properties
@@ -151,19 +151,29 @@ partAttachmentSafeName=Attached Message 
 ## String used by the Initialization Error dialog
 initErrorDlogTitle=Message Compose
 initErrorDlgMessage=An error occurred while creating a message compose window. Please try again.
 
 ## String used if the file to attach does not exist when passed as
 ## a command line argument
 errorFileAttachTitle=File Attach
 
-## LOCALIZATION NOTE (errorFileAttachMessage): %1$S will be replaced by the non-existent file name. Do not translate
+## LOCALIZATION NOTE (errorFileAttachMessage): %1$S will be replaced by the non-existent file name.
 errorFileAttachMessage=The file %1$S does not exist so could not be attached to the message.
 
+## String used if a file to serve as message body does not exist or cannot be
+## loaded when passed as a command line argument
+errorFileMessageTitle=Message File
+
+## LOCALIZATION NOTE (errorFileMessageMessage): %1$S will be replaced by the non-existent file name.
+errorFileMessageMessage=The file %1$S does not exist and could not be used as message body.
+
+## LOCALIZATION NOTE (errorLoadFileMessageMessage): %1$S will be replaced by the name of the file that can't be loaded.
+errorLoadFileMessageMessage=The file %1$S could not be loaded as message body.
+
 ## Strings used by the Save as Draft/Template dialog
 SaveDialogTitle=Save Message
 
 ## LOCALIZATION NOTE (SaveDialogMsg): %1$S is the folder name, %2$S is the host name
 SaveDialogMsg=Your message has been saved to the folder %1$S under %2$S.
 CheckMsg=Do not show me this dialog box again.
 
 ## Strings used by the prompt when Quitting while in progress
@@ -281,8 +291,12 @@ blockedAllowResource=Unblock %S
 ## Unblocking one/several file(s) will include it/them in your message.
 blockedContentMessage=%S has blocked a file from loading into this message. Unblocking the file will include it in your sent message.;%S has blocked some files from loading into this message. Unblocking a file will include it in your sent message.
 
 ## LOCALIZATION NOTE (blockedContentPrefLabel, blockedContentPrefAccesskey):
 ## Same content as (blockedContentPrefLabel, blockedContentPrefAccesskey)
 ## in mail directory. SeaMonkey does only use Options and not Preferences.
 blockedContentPrefLabel=Options
 blockedContentPrefAccesskey=O
+
+## Identity matching warning notification bar.
+## LOCALIZATION NOTE(identityWarning): %S will be replaced with the identity name.
+identityWarning=A unique identity matching the From address was not found. The message will be sent using the current From field and settings from identity %S.
diff --git a/suite/mailnews/components/compose/content/MsgComposeCommands.js b/suite/mailnews/components/compose/content/MsgComposeCommands.js
--- a/suite/mailnews/components/compose/content/MsgComposeCommands.js
+++ b/suite/mailnews/components/compose/content/MsgComposeCommands.js
@@ -7,16 +7,18 @@ ChromeUtils.import("resource://gre/modul
 ChromeUtils.import("resource://gre/modules/AppConstants.jsm");
 ChromeUtils.import("resource://gre/modules/PluralForm.jsm");
 ChromeUtils.import("resource://gre/modules/InlineSpellChecker.jsm");
 ChromeUtils.import("resource:///modules/folderUtils.jsm");
 ChromeUtils.import("resource:///modules/iteratorUtils.jsm");
 ChromeUtils.import("resource:///modules/mailServices.js");
 ChromeUtils.import("resource:///modules/MailUtils.js");
 
+ChromeUtils.defineModuleGetter(this, "OS", "resource://gre/modules/osfile.jsm");
+
 /**
  * interfaces
  */
 var nsIMsgCompDeliverMode = Ci.nsIMsgCompDeliverMode;
 var nsIMsgCompSendFormat = Ci.nsIMsgCompSendFormat;
 var nsIMsgCompConvertible = Ci.nsIMsgCompConvertible;
 var nsIMsgCompType = Ci.nsIMsgCompType;
 var nsIMsgCompFormat = Ci.nsIMsgCompFormat;
@@ -69,16 +71,17 @@ var gLogComposePerformance;
 
 var gMsgIdentityElement;
 var gMsgAddressingWidgetElement;
 var gMsgSubjectElement;
 var gMsgAttachmentElement;
 var gMsgHeadersToolbarElement;
 var gComposeType;
 var gFormatToolbarHidden = false;
+var gBodyFromArgs;
 
 // i18n globals
 var gCharsetConvertManager;
 
 var gLastWindowToHaveFocus;
 var gReceiptOptionChanged;
 var gDSNOptionChanged;
 var gAttachVCardOptionChanged;
@@ -170,16 +173,18 @@ var stateListener = {
     this.useParagraph = gMsgCompose.composeHTML &&
                         Services.prefs.getBoolPref("mail.compose.default_to_paragraph");
     this.editor = GetCurrentEditor();
     this.paragraphState = document.getElementById("cmd_paragraphState");
 
     // Look at the compose types which require action (nsIMsgComposeParams.idl):
     switch (gComposeType) {
 
+      case Ci.nsIMsgCompType.MailToUrl:
+        gBodyFromArgs = true;
       case Ci.nsIMsgCompType.New:
       case Ci.nsIMsgCompType.NewsPost:
       case Ci.nsIMsgCompType.ForwardAsAttachment:
         this.NotifyComposeBodyReadyNew();
         break;
 
       case Ci.nsIMsgCompType.Reply:
       case Ci.nsIMsgCompType.ReplyAll:
@@ -219,22 +224,39 @@ var stateListener = {
     }
 
     if (gMsgCompose.composeHTML)
       loadHTMLMsgPrefs();
     AdjustFocus();
   },
 
   NotifyComposeBodyReadyNew: function() {
+    let insertParagraph = this.useParagraph;
+
+    let mailDoc = document.getElementById("content-frame").contentDocument;
+    let mailBody = mailDoc.querySelector("body");
+    if (insertParagraph && gBodyFromArgs) {
+      // Check for "empty" body before allowing paragraph to be inserted.
+      // Non-empty bodies in a new message can occur when clicking on a
+      // mailto link or when using the command line option -compose.
+      // An "empty" body can be one of these two cases:
+      // 1) <br> and nothing follows (no next sibling)
+      // 2) <div/pre class="moz-signature">
+      // Note that <br><div/pre class="moz-signature"> doesn't happen in
+      // paragraph mode.
+      let firstChild = mailBody.firstChild;
+      if ((firstChild.nodeName != "BR" || firstChild.nextSibling) &&
+          !isSignature(firstChild))
+        insertParagraph = false;
+    }
+
     // Control insertion of line breaks.
-    if (this.useParagraph) {
+    if (insertParagraph) {
       this.editor.enableUndo(false);
 
-      let mailDoc = document.getElementById("content-frame").contentDocument;
-      let mailBody = mailDoc.querySelector("body");
       this.editor.selection.collapse(mailBody, 0);
       let pElement = this.editor.createElementWithDefaults("p");
       let brElement = this.editor.createElementWithDefaults("br");
       pElement.appendChild(brElement);
       this.editor.insertElementAtSelection(pElement, false);
 
       this.paragraphState.setAttribute("state", "p");
 
@@ -1143,16 +1165,17 @@ function onPasteOrDrop(e) {
     });
   }
 }
 
 function ComposeStartup(aParams)
 {
   var params = null; // New way to pass parameters to the compose window as a nsIMsgComposeParameters object
   var args = null;   // old way, parameters are passed as a string
+  gBodyFromArgs = false;
 
   if (aParams)
     params = aParams;
   else if (window.arguments && window.arguments[0]) {
     try {
       if (window.arguments[0] instanceof Ci.nsIMsgComposeParams)
         params = window.arguments[0];
       else
@@ -1185,26 +1208,37 @@ function ComposeStartup(aParams)
     // using a object of type nsMsgComposeParams instead of a string.
     params = Cc["@mozilla.org/messengercompose/composeparams;1"]
                .createInstance(Ci.nsIMsgComposeParams);
     params.composeFields = Cc["@mozilla.org/messengercompose/composefields;1"]
                              .createInstance(Ci.nsIMsgCompFields);
 
     if (args) { //Convert old fashion arguments into params
       var composeFields = params.composeFields;
-      if (args.bodyislink == "true")
+      if (args.bodyislink && args.bodyislink == "true")
         params.bodyIsLink = true;
       if (args.type)
         params.type = args.type;
-      if (args.format)
-        params.format = args.format;
+      if (args.format) {
+        // Only use valid values.
+        if (args.format == Ci.nsIMsgCompFormat.PlainText ||
+            args.format == Ci.nsIMsgCompFormat.HTML ||
+            args.format == Ci.nsIMsgCompFormat.OppositeOfDefault)
+          params.format = args.format;
+        else if (args.format.toLowerCase().trim() == "html")
+          params.format = Ci.nsIMsgCompFormat.HTML;
+        else if (args.format.toLowerCase().trim() == "text")
+          params.format = Ci.nsIMsgCompFormat.PlainText;
+      }
       if (args.originalMsgURI)
         params.originalMsgURI = args.originalMsgURI;
       if (args.preselectid)
         params.identity = getIdentityForKey(args.preselectid);
+      if (args.from)
+        composeFields.from = args.from;
       if (args.to)
         composeFields.to = args.to;
       if (args.cc)
         composeFields.cc = args.cc;
       if (args.bcc)
         composeFields.bcc = args.bcc;
       if (args.newsgroups)
         composeFields.newsgroups = args.newsgroups;
@@ -1236,36 +1270,147 @@ function ComposeStartup(aParams)
             attachment.url = uri.spec;
             composeFields.addAttachment(attachment);
           }
           else
           {
             let title = sComposeMsgsBundle.getString("errorFileAttachTitle");
             let msg = sComposeMsgsBundle.getFormattedString("errorFileAttachMessage",
                                                             [attachmentStr]);
-            Services.prompt.alert(window, title, msg);
+            Services.prompt.alert(null, title, msg);
           }
         }
       }
       if (args.newshost)
         composeFields.newshost = args.newshost;
-      if (args.body)
-         composeFields.body = args.body;
+      if (args.message) {
+        let msgFile = Cc["@mozilla.org/file/local;1"]
+                        .createInstance(Ci.nsIFile);
+        if (OS.Path.dirname(args.message) == ".") {
+          let workingDir = Services.dirsvc.get("CurWorkD", Ci.nsIFile);
+          args.message = OS.Path.join(workingDir.path, OS.Path.basename(args.message));
+        }
+        msgFile.initWithPath(args.message);
+
+        if (!msgFile.exists()) {
+          let title = sComposeMsgsBundle.getString("errorFileMessageTitle");
+          let msg = sComposeMsgsBundle.getFormattedString("errorFileMessageMessage",
+                                                          [args.message]);
+          Services.prompt.alert(null, title, msg);
+        } else {
+          let data = "";
+          let fstream = null;
+          let cstream = null;
+
+          try {
+            fstream = Cc["@mozilla.org/network/file-input-stream;1"]
+                        .createInstance(Ci.nsIFileInputStream);
+            cstream = Cc["@mozilla.org/intl/converter-input-stream;1"]
+                        .createInstance(Ci.nsIConverterInputStream);
+            fstream.init(msgFile, -1, 0, 0); // Open file in default/read-only mode.
+            cstream.init(fstream, "UTF-8", 0, 0);
+
+            let str = {};
+            let read = 0;
+
+            do {
+              // Read as much as we can and put it in str.value.
+              read = cstream.readString(0xffffffff, str);
+              data += str.value;
+            } while (read != 0);
+          } catch (e) {
+            let title = sComposeMsgsBundle.getString("errorFileMessageTitle");
+            let msg = sComposeMsgsBundle.getFormattedString("errorLoadFileMessageMessage",
+                                                            [args.message]);
+            Services.prompt.alert(null, title, msg);
+
+          } finally {
+            if (cstream)
+              cstream.close();
+            if (fstream)
+              fstream.close();
+          }
+
+          if (data) {
+            let pos = data.search(/\S/); // Find first non-whitespace character.
+
+            if (params.format != Ci.nsIMsgCompFormat.PlainText &&
+                (args.message.endsWith(".htm") ||
+                 args.message.endsWith(".html") ||
+                 data.substr(pos, 14).toLowerCase() == "<!doctype html" ||
+                 data.substr(pos, 5).toLowerCase() == "<html")) {
+              // We replace line breaks because otherwise they'll be converted
+              // to <br> in nsMsgCompose::BuildBodyMessageAndSignature().
+              // Don't do the conversion if the user asked explicitly for plain
+              // text.
+              data = data.replace(/\r?\n/g, " ");
+            }
+            gBodyFromArgs = true;
+            composeFields.body = data;
+          }
+        }
+      } else if (args.body) {
+        gBodyFromArgs = true;
+        composeFields.body = args.body;
+      }
     }
   }
 
   gComposeType = params.type;
 
+  // Detect correct identity when missing or mismatched.
   // An identity with no email is likely not valid.
-  if (!params.identity || !params.identity.email) {
-    // no pre selected identity, so use the default account
-    var identities = MailServices.accounts.defaultAccount.identities;
-    if (identities.length == 0)
-      identities = MailServices.accounts.allIdentities;
-    params.identity = identities.queryElementAt(0, Ci.nsIMsgIdentity);
+  // When editing a draft, 'params.identity' is pre-populated with the identity
+  // that created the draft or the identity owning the draft folder for a
+  // "foreign", draft, see ComposeMessage() in mailCommands.js. We don't want
+  // the latter, so use the creator identity which could be null.
+  if (gComposeType == Ci.nsIMsgCompType.Draft) {
+    let creatorKey = params.composeFields.creatorIdentityKey;
+    params.identity = creatorKey ? getIdentityForKey(creatorKey) : null;
+  }
+  let from = [];
+  if (params.composeFields.from)
+    from = MailServices.headerParser
+                       .parseEncodedHeader(params.composeFields.from, null);
+  from = (from.length && from[0] && from[0].email) ?
+    from[0].email.toLowerCase().trim() : null;
+  if (!params.identity || !params.identity.email ||
+      (from && !emailSimilar(from, params.identity.email))) {
+    let identities = MailServices.accounts.allIdentities;
+    let suitableCount = 0;
+
+    // Search for a matching identity.
+    if (from) {
+      for (let ident of fixIterator(identities, Ci.nsIMsgIdentity)) {
+        if (ident.email && from == ident.email.toLowerCase()) {
+          if (suitableCount == 0)
+            params.identity = ident;
+          suitableCount++;
+          if (suitableCount > 1)
+            break; // No need to find more, it's already not unique.
+        }
+      }
+    }
+
+    if (!params.identity || !params.identity.email) {
+      // No preset identity and no match, so use the default account.
+      let identity = MailServices.accounts.defaultAccount.defaultIdentity;
+      if (!identity) {
+        let identities = MailServices.accounts.allIdentities;
+        if (identities.length > 0)
+          identity = identities.queryElementAt(0, Ci.nsIMsgIdentity);
+      }
+      params.identity = identity;
+    }
+
+    // Warn if no or more than one match was found.
+    // But don't warn for +suffix additions (a+b@c.com).
+    if (from && (suitableCount > 1 ||
+        (suitableCount == 0 && !emailSimilar(from, params.identity.email))))
+      gComposeNotificationBar.setIdentityWarning(params.identity.identityName);
   }
 
   identityList.selectedItem =
     identityList.getElementsByAttribute("identitykey", params.identity.key)[0];
   if (params.composeFields.from)
     identityList.value = MailServices.headerParser.parseDecodedHeader(params.composeFields.from)[0].toString();
   LoadIdentity(true);
 
@@ -1380,16 +1525,31 @@ function ComposeStartup(aParams)
   gAutoSaveInterval = Services.prefs.getBoolPref("mail.compose.autosave")
     ? Services.prefs.getIntPref("mail.compose.autosaveinterval") * 60000
     : 0;
 
   if (gAutoSaveInterval)
     gAutoSaveTimeout = setTimeout(AutoSave, gAutoSaveInterval);
 }
 
+function splitEmailAddress(aEmail) {
+  let at = aEmail.lastIndexOf("@");
+  return (at != -1) ? [aEmail.slice(0, at), aEmail.slice(at + 1)]
+                    : [aEmail, ""];
+}
+
+// Emails are equal ignoring +suffixes (email+suffix@example.com).
+function emailSimilar(a, b) {
+  if (!a || !b)
+    return a == b;
+  a = splitEmailAddress(a.toLowerCase());
+  b = splitEmailAddress(b.toLowerCase());
+  return a[1] == b[1] && a[0].split("+", 1)[0] == b[0].split("+", 1)[0];
+}
+
 // The new, nice, simple way of getting notified when a new editor has been created
 var gMsgEditorCreationObserver =
 {
   observe: function(aSubject, aTopic, aData)
   {
     if (aTopic == "obs_documentCreated")
     {
       var editor = GetCurrentEditor();
@@ -2862,16 +3022,18 @@ function LoadIdentity(startup)
 
           try {
             gMsgCompose.identity = gCurrentIdentity;
           } catch (ex) { dump("### Cannot change the identity: " + ex + "\n");}
 
           var event = document.createEvent('Events');
           event.initEvent('compose-from-changed', false, true);
           document.getElementById("msgcomposeWindow").dispatchEvent(event);
+
+          gComposeNotificationBar.clearIdentityWarning();
         }
 
       if (!startup) {
           if (Services.prefs.getBoolPref("mail.autoComplete.highlightNonMatches"))
             document.getElementById('addressCol2#1').highlightNonMatches = true;
 
           // Only do this if we aren't starting up...
           // It gets done as part of startup already.
@@ -3494,18 +3656,42 @@ var gComposeNotificationBar = {
                           .setAttribute("label", msg);
     }
   },
 
   isShowingBlockedContentNotification: function() {
     return !!this.notificationBar.getNotificationWithValue("blockedContent");
   },
 
+  clearBlockedContentNotification: function() {
+    this.notificationBar.removeNotification(
+      this.notificationBar.getNotificationWithValue("blockedContent"));
+  },
+
   clearNotifications: function(aValue) {
     this.notificationBar.removeAllNotifications(true);
+  },
+
+  setIdentityWarning: function(aIdentityName) {
+    if (!this.notificationBar.getNotificationWithValue("identityWarning")) {
+      let text = sComposeMsgsBundle.getString("identityWarning").split("%S");
+      let label = new DocumentFragment();
+      label.appendChild(document.createTextNode(text[0]));
+      label.appendChild(document.createElement("b"));
+      label.lastChild.appendChild(document.createTextNode(aIdentityName));
+      label.appendChild(document.createTextNode(text[1]));
+      this.notificationBar.appendNotification(label, "identityWarning", null,
+        this.notificationBar.PRIORITY_WARNING_HIGH, null);
+    }
+  },
+
+  clearIdentityWarning: function() {
+    let idWarning = this.notificationBar.getNotificationWithValue("identityWarning");
+    if (idWarning)
+      this.notificationBar.removeNotification(idWarning);
   }
 };
 
 /**
  * Populate the menuitems of what blocked content to unblock.
  */
 function onBlockedContentOptionsShowing(aEvent) {
   let urls = aEvent.target.value ? aEvent.target.value.split(" ") : [];
@@ -3545,17 +3731,17 @@ function onUnblockResource(aURL, aNode) 
   } finally {
     // Remove it from the list on success and failure.
     let urls = aNode.value.split(" ");
     for (let i = 0; i < urls.length; i++) {
       if (urls[i] == aURL) {
         urls.splice(i, 1);
         aNode.value = urls.join(" ");
         if (urls.length == 0) {
-          gComposeNotificationBar.clearNotifications();
+          gComposeNotificationBar.clearBlockedContentNotification();
         }
         break;
       }
     }
   }
 }
 
 /**
