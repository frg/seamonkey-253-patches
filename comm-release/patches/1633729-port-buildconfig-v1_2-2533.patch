# HG changeset patch
# User Ian Neal <iann_cvs@blueyonder.co.uk>
# Date 1587991936 -3600
# Parent  d7dc34f309ff827ce84b7ba7116b1c727b688aa6
Bug 1633729 - Update about:buildconfig to display comm as well as mozilla source information - port and adapt. r=frg a=frg

diff --git a/build/source_repos.py b/build/source_repos.py
--- a/build/source_repos.py
+++ b/build/source_repos.py
@@ -1,28 +1,49 @@
 #  This Source Code Form is subject to the terms of the Mozilla Public
 #  License, v. 2.0. If a copy of the MPL was not distributed with this
 #  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import print_function, unicode_literals
 
 import sys
 import os
+from datetime import datetime
 
 import buildconfig
 
-sourcestamp_tmpl = """{buildid}
-{comm_repo}/rev/{comm_rev}
-{gecko_repo}/rev/{gecko_rev}
-"""
+def get_gecko_repo_info():
+    repo = buildconfig.substs.get('MOZ_GECKO_SOURCE_REPO', None)
+    rev = buildconfig.substs.get('MOZ_GECKO_SOURCE_CHANGESET', None)
+    if not repo:
+        repo = buildconfig.substs.get('MOZ_SOURCE_REPO', None)
+        rev = buildconfig.substs.get('MOZ_SOURCE_CHANGESET', None)
+
+    return repo, rev
+
+
+def get_comm_repo_info():
+    repo = buildconfig.substs.get('MOZ_COMM_SOURCE_REPO', None)
+    rev = buildconfig.substs.get('MOZ_COMM_SOURCE_CHANGESET', None)
+
+    return repo, rev
+
+
+def get_source_url(repo, rev):
+    source_url = ''
+    if "git" in repo:
+        source_url = '%s/tree/%s' % (repo, rev)
+    else:
+        source_url = '%s/rev/%s' % (repo, rev)
+
+    return source_url
 
 
 def gen_platformini(output, platform_ini):
-    gecko_repo = buildconfig.substs.get('MOZ_GECKO_SOURCE_REPO', '')
-    gecko_rev = buildconfig.substs.get('MOZ_GECKO_SOURCE_CHANGESET', '')
+    gecko_repo, gecko_rev = get_gecko_repo_info()
 
     with open(platform_ini, 'r') as fp:
         data = fp.readlines()
 
     for i in range(len(data)):
         if data[i].startswith('SourceRepository='):
             data[i] = 'SourceRepository=%s\n' % gecko_repo
         elif data[i].startswith('SourceStamp='):
@@ -30,54 +51,67 @@ def gen_platformini(output, platform_ini
 
     with open(platform_ini, 'w') as fp:
         fp.writelines(data)
 
     output.write('platform.ini updated.\n')
 
 
 def gen_sourcestamp(output):
-    data = dict(buildid=os.environ.get('MOZ_BUILD_DATE', 'unknown'),
-        gecko_repo=buildconfig.substs.get('MOZ_GECKO_SOURCE_REPO', None),
-        gecko_rev=buildconfig.substs.get('MOZ_GECKO_SOURCE_CHANGESET', None),
-        comm_repo=buildconfig.substs.get('MOZ_COMM_SOURCE_REPO', None),
-        comm_rev=buildconfig.substs.get('MOZ_COMM_SOURCE_CHANGESET', None))
+    buildid = os.environ.get('MOZ_BUILD_DATE')
+    if buildid and len(buildid) != 14:
+        print('Ignoring invalid MOZ_BUILD_DATE: %s' % buildid, file=sys.stderr)
+        buildid = None
+    if not buildid:
+        buildid = datetime.now().strftime('%Y%m%d%H%M%S')
+    output.write('{}\n'.format(buildid))
 
-    output.write(sourcestamp_tmpl.format(**data))
+    gecko_repo, gecko_rev = get_gecko_repo_info()
+    comm_repo, comm_rev  = get_comm_repo_info()
+    if gecko_repo:
+        output.write('{}\n'.format(get_source_url(gecko_repo, gecko_rev)))
+
+    if comm_repo:
+        output.write('{}\n'.format(get_source_url(comm_repo, comm_rev)))
 
 
 def source_repo_header(output):
     """
     Appends the Gecko source repository information to source-repo.h
     This information should be set in buildconfig.substs by moz.configure
     """
-    gecko_repo = buildconfig.substs.get('MOZ_GECKO_SOURCE_REPO', None)
-    gecko_rev = buildconfig.substs.get('MOZ_GECKO_SOURCE_CHANGESET', None)
-    comm_repo = buildconfig.substs.get('MOZ_COMM_SOURCE_REPO', None)
-    comm_rev = buildconfig.substs.get('MOZ_COMM_SOURCE_CHANGESET', None)
+    gecko_repo, gecko_rev = get_gecko_repo_info()
+    comm_repo, comm_rev  = get_comm_repo_info()
 
     if None in [gecko_repo, gecko_rev, comm_repo, comm_rev]:
         Exception("Source information not found in buildconfig."
                   "Try setting GECKO_HEAD_REPOSITORY and GECKO_HEAD_REV"
                   "as well as MOZ_SOURCE_REPO and MOZ_SOURCE_CHANGESET"
                   "environment variables and running mach configure again.")
 
     output.write('#define MOZ_GECKO_SOURCE_STAMP {}\n'.format(gecko_rev))
-    output.write('#define MOZ_COMM_SOURCE_STAMP {}\n'.format(comm_rev))
-    output.write('#define MOZ_SOURCE_STAMP {}\n'.format(comm_rev))
+    if comm_rev:
+        output.write('#define MOZ_COMM_SOURCE_STAMP {}\n'.format(comm_rev))
+        output.write('#define MOZ_SOURCE_STAMP {}\n'.format(comm_rev))
+    else:
+        output.write('#define MOZ_SOURCE_STAMP {}\n'.format(gecko_rev))
 
-    if buildconfig.substs.get('MOZ_INCLUDE_SOURCE_INFO'):
-        gecko_source_url = '%s/rev/%s' % (gecko_repo, gecko_rev)
-        comm_source_url = '%s/rev/%s' % (comm_repo, comm_rev)
+    if buildconfig.substs.get('MOZ_INCLUDE_SOURCE_INFO') and gecko_repo:
+        gecko_source_url = get_source_url(gecko_repo, gecko_rev)
         output.write('#define MOZ_GECKO_SOURCE_REPO {}\n'.format(gecko_repo))
         output.write('#define MOZ_GECKO_SOURCE_URL {}\n'.format(gecko_source_url))
-        output.write('#define MOZ_COMM_SOURCE_REPO {}\n'.format(comm_repo))
-        output.write('#define MOZ_COMM_SOURCE_URL {}\n'.format(comm_source_url))
-        output.write('#define MOZ_SOURCE_REPO {}\n'.format(comm_repo))
-        output.write('#define MOZ_SOURCE_URL {}\n'.format(comm_source_url))
+        if comm_repo:
+            comm_source_url = get_source_url(comm_repo, comm_rev)
+            output.write('#define MOZ_COMM_SOURCE_REPO {}\n'.format(comm_repo))
+            output.write('#define MOZ_COMM_SOURCE_URL {}\n'.format(comm_source_url))
+            output.write('#define MOZ_SOURCE_REPO {}\n'.format(comm_repo))
+            output.write('#define MOZ_SOURCE_URL {}\n'.format(comm_source_url))
+        else:
+            output.write('#define MOZ_SOURCE_REPO {}\n'.format(gecko_repo))
+            output.write('#define MOZ_SOURCE_URL {}\n'.format(gecko_source_url))
 
 
 def main(args):
     if args:
         func = globals().get(args[0])
         if func:
             return func(sys.stdout, *args[1:])
 
diff --git a/mail/configure.in b/mail/configure.in
--- a/mail/configure.in
+++ b/mail/configure.in
@@ -10,16 +10,17 @@ AC_SUBST(MOZ_GECKO_SOURCE_CHANGESET)
 AC_SUBST(MOZ_COMM_SOURCE_REPO)
 AC_SUBST(MOZ_COMM_SOURCE_CHANGESET)
 
 dnl Things we need to carry from confvars.sh
 AC_DEFINE(MOZ_THUNDERBIRD)
 AC_SUBST(MOZ_THUNDERBIRD)
 AC_SUBST(MOZ_COMPOSER)
 AC_SUBST(THUNDERBIRD_VERSION)
+AC_SUBST(THUNDERBIRD_VERSION_DISPLAY)
 AC_DEFINE(MOZ_SEPARATE_MANIFEST_FOR_THEME_OVERRIDES)
 AC_SUBST(MOZ_BUNDLED_FONTS)
 
 dnl Optional parts of the build.
 AC_SUBST(MOZ_MORK)
 if test "$MOZ_MORK"; then
   AC_DEFINE(MOZ_MORK)
 fi
diff --git a/mail/confvars.sh b/mail/confvars.sh
--- a/mail/confvars.sh
+++ b/mail/confvars.sh
@@ -24,16 +24,17 @@ fi
 
 MOZ_MORK=1
 
 MOZ_APP_VERSION_TXT=${_topsrcdir}/$MOZ_BUILD_APP/config/version.txt
 MOZ_APP_VERSION=`cat $MOZ_APP_VERSION_TXT`
 MOZ_APP_VERSION_DISPLAY_TXT=${_topsrcdir}/$MOZ_BUILD_APP/config/version_display.txt
 MOZ_APP_VERSION_DISPLAY=`cat $MOZ_APP_VERSION_DISPLAY_TXT`
 THUNDERBIRD_VERSION=$MOZ_APP_VERSION
+THUNDERBIRD_VERSION_DISPLAY=$MOZ_APP_VERSION_DISPLAY
 
 MOZ_BRANDING_DIRECTORY=mail/branding/nightly
 MOZ_OFFICIAL_BRANDING_DIRECTORY=other-licenses/branding/thunderbird
 MOZ_APP_ID={3550f703-e582-4d05-9a08-453d09bdfdc6}
 # This should usually be the same as the value MAR_CHANNEL_ID.
 # If more than one ID is needed, then you should use a comma separated list
 # of values.
 ACCEPTED_MAR_CHANNEL_IDS=thunderbird-comm-release
diff --git a/suite/Makefile.in b/suite/Makefile.in
--- a/suite/Makefile.in
+++ b/suite/Makefile.in
@@ -8,18 +8,15 @@ include $(topsrcdir)/config/rules.mk
 ifdef MAKENSISU
 # For Windows build the uninstaller during the application build since the
 # uninstaller is included with the application for mar file generation.
 libs::
 	$(MAKE) -C installer/windows uninstaller
 endif
 
 
-# As a fallout from bug 1247162, the sourcestamp in application.ini and
+# As fallout from bug 1247162, the sourcestamp in application.ini and
 # platform.ini are the same, which isn't a problem for Firefox, but
 # it's not right for anything else. So we correct platform.ini here.
-
-MOZ_REV=$(shell hg -R "$(MOZILLA_SRCDIR)" parent --template="{node}" 2>/dev/null)
+libs:: $(DIST)/bin/platform.ini
+	$(PYTHON) $(topsrcdir)/build/source_repos.py gen_platformini \
+		$(DIST)/bin/platform.ini
 
-libs:: $(DIST)/bin/platform.ini
-	sed -e "s/^\(SourceStamp=\).*/\1$(MOZ_REV)/" $(DIST)/bin/platform.ini \
-		> $(DIST)/bin/platform.ini~
-	mv -f $(DIST)/bin/platform.ini~ $(DIST)/bin/platform.ini
diff --git a/suite/app.mozbuild b/suite/app.mozbuild
--- a/suite/app.mozbuild
+++ b/suite/app.mozbuild
@@ -1,13 +1,15 @@
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+GENERATED_FILES['source-repo.h'].script = '../build/source_repos.py:source_repo_header'
+
 include('/mailnews/mailnews.mozbuild')
 
 include('/toolkit/toolkit.mozbuild')
 
 if CONFIG['MOZ_EXTENSIONS']:
     DIRS += ['/extensions']
 
 if CONFIG['MOZ_IRC']:
diff --git a/mail/base/content/buildconfig.css b/suite/base/content/buildconfig.css
copy from mail/base/content/buildconfig.css
copy to suite/base/content/buildconfig.css
--- a/mail/base/content/buildconfig.css
+++ b/suite/base/content/buildconfig.css
@@ -1,13 +1,13 @@
 /* # This Source Code Form is subject to the terms of the Mozilla Public */
 /* # License, v. 2.0. If a copy of the MPL was not distributed with this */
 /* # file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-@import url("chrome://global/skin/in-content/info-pages.css");
+@import url("chrome://global/skin/about.css");
 
 body {
   align-items: initial;
 }
 
 th { text-align: start; }
 h2 { margin-top: 1.5em; }
 th, td { vertical-align: top; }
diff --git a/mail/base/content/buildconfig.html b/suite/base/content/buildconfig.html
copy from mail/base/content/buildconfig.html
copy to suite/base/content/buildconfig.html
--- a/mail/base/content/buildconfig.html
+++ b/suite/base/content/buildconfig.html
@@ -7,21 +7,21 @@
 #include @TOPOBJDIR@/source-repo.h
 #include @TOPOBJDIR@/buildid.h
 <html>
 <head>
   <meta http-equiv="Content-Security-Policy" content="default-src 'none'; style-src chrome:" />
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width; user-scalable=false;">
   <title>Build Configuration</title>
-  <link rel="stylesheet" href="chrome://messenger/content/buildconfig.css" type="text/css">
+  <link rel="stylesheet" href="chrome://communicator/content/buildconfig.css" type="text/css">
 </head>
 <body class="aboutPageWideContainer">
 <h1>Build Configuration</h1>
-<h2>@MOZ_APP_DISPLAYNAME@ @THUNDERBIRD_VERSION_DISPLAY@ - @MOZ_BUILDID@</h2>
+<h2>@MOZ_APP_DISPLAYNAME@ @SEAMONKEY_VERSION_DISPLAY@ - @MOZ_BUILDID@</h2>
 <table>
   <tbody>
 #ifdef MOZ_COMM_SOURCE_URL
   <tr>
     <th>
       @MOZ_APP_DISPLAYNAME@ source
     </th>
   </tr>
@@ -42,18 +42,18 @@
       <a href="@MOZ_GECKO_SOURCE_URL@">@MOZ_GECKO_SOURCE_URL@</a>
     </td>
   </tr>
 #endif
   </tbody>
 </table>
 
 <p>The latest information on building @MOZ_APP_DISPLAYNAME@ can be found at
-  <a href="@THUNDERBIRD_DEVELOPER_WWW@">
-    @THUNDERBIRD_DEVELOPER_WWW@</a>.
+  <a href="@SEAMONKEY_DEVELOPER_WWW@">
+    @SEAMONKEY_DEVELOPER_WWW@</a>.
 </p>
 
 <h2>Build platform</h2>
 <table>
   <tbody>
   <tr>
     <th>Target</th>
   </tr>
diff --git a/suite/base/jar.mn b/suite/base/jar.mn
--- a/suite/base/jar.mn
+++ b/suite/base/jar.mn
@@ -1,14 +1,16 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.comm.jar:
 
 comm.jar:
 % content communicator %content/communicator/ contentaccessible=yes
+% override chrome://global/content/buildconfig.css chrome://communicator/content/buildconfig.css
+% override chrome://global/content/buildconfig.html chrome://communicator/content/buildconfig.html
 % override chrome://global/content/license.html chrome://communicator/content/license.html
 % override chrome://global/content/netError.xhtml chrome://communicator/content/certError.xhtml
 % overlay chrome://global/content/viewSource.xul chrome://communicator/content/viewSourceOverlay.xul
 % overlay chrome://global/content/viewPartialSource.xul chrome://communicator/content/viewSourceOverlay.xul
 % overlay chrome://editor/content/EdAdvancedEdit.xul chrome://communicator/content/helpEditorOverlay.xul
 % overlay chrome://editor/content/EdImageProps.xul chrome://communicator/content/helpEditorOverlay.xul
 % overlay chrome://editor/content/EditorPublish.xul chrome://communicator/content/helpEditorOverlay.xul
 % overlay chrome://editor/content/EditorPublishProgress.xul chrome://communicator/content/helpEditorOverlay.xul
@@ -52,16 +54,18 @@ comm.jar:
    content/communicator/aboutLife.xhtml                             (content/aboutLife.xhtml)
    content/communicator/aboutPrivateBrowsing.css                    (content/aboutPrivateBrowsing.css)
    content/communicator/aboutPrivateBrowsing.js                     (content/aboutPrivateBrowsing.js)
    content/communicator/aboutPrivateBrowsing.xul                    (content/aboutPrivateBrowsing.xul)
    content/communicator/askViewZoom.js                              (content/askViewZoom.js)
    content/communicator/askViewZoom.xul                             (content/askViewZoom.xul)
    content/communicator/blockedSite.js                              (content/blockedSite.js)
    content/communicator/blockedSite.xhtml                           (content/blockedSite.xhtml)
+   content/communicator/buildconfig.css                             (content/buildconfig.css)
+*  content/communicator/buildconfig.html                            (content/buildconfig.html)
    content/communicator/certError.css                               (content/certError.css)
    content/communicator/certError.js                                (content/certError.js)
    content/communicator/certError.xhtml                             (content/certError.xhtml)
    content/communicator/certError.xml                               (content/certError.xml)
    content/communicator/charsetOverlay.xul                          (content/charsetOverlay.xul)
    content/communicator/communicator.css                            (content/communicator.css)
    content/communicator/contentAreaClick.js                         (content/contentAreaClick.js)
    content/communicator/contentAreaContextOverlay.xul               (content/contentAreaContextOverlay.xul)
diff --git a/suite/base/moz.build b/suite/base/moz.build
--- a/suite/base/moz.build
+++ b/suite/base/moz.build
@@ -10,8 +10,33 @@ JAR_MANIFESTS += ['jar.mn']
 
 # Use suite/base/content/overrides/app-license.html as input when generating
 # chrome://content/communicator/license.html to override
 # chrome://global/content/license.html (about:license)
 DEFINES['APP_LICENSE_BLOCK'] = '%s/content/overrides/app-license.html' % SRCDIR
 
 for var in ('MOZ_APP_NAME', 'MOZ_MACBUNDLE_NAME'):
     DEFINES[var] = CONFIG[var]
+
+# For customized buildconfig
+DEFINES['TOPOBJDIR'] = TOPOBJDIR
+
+DEFINES['SEAMONKEY_DEVELOPER_WWW'] = "https://www.seamonkey-project.org/dev/"
+
+for var in ('CC', 'CC_VERSION', 'CXX', 'RUSTC', 'RUSTC_VERSION'):
+    if CONFIG[var]:
+        DEFINES[var] = CONFIG[var]
+
+for var in ('MOZ_CONFIGURE_OPTIONS', 'target', 'MOZ_APP_DISPLAYNAME', 'SEAMONKEY_VERSION_DISPLAY',):
+    DEFINES[var] = CONFIG[var]
+
+DEFINES['CFLAGS'] = ' '.join(CONFIG['OS_CFLAGS'])
+
+rustflags = CONFIG['RUSTFLAGS']
+if not rustflags:
+    rustflags = []
+DEFINES['RUSTFLAGS'] = ' '.join(rustflags)
+
+cxx_flags = []
+for var in ('OS_CPPFLAGS', 'OS_CXXFLAGS', 'DEBUG', 'OPTIMIZE', 'FRAMEPTR'):
+    cxx_flags += COMPILE_FLAGS[var] or []
+
+DEFINES['CXXFLAGS'] = ' '.join(cxx_flags)
diff --git a/suite/configure.in b/suite/configure.in
--- a/suite/configure.in
+++ b/suite/configure.in
@@ -1,20 +1,27 @@
 dnl -*- Mode: Autoconf; tab-width: 2; indent-tabs-mode: nil; -*-
 dnl vi: set tabstop=2 shiftwidth=2 expandtab:
 dnl This Source Code Form is subject to the terms of the Mozilla Public
 dnl License, v. 2.0. If a copy of the MPL was not distributed with this
 dnl file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+dnl Things we might need for setting source repos
+AC_SUBST(MOZ_GECKO_SOURCE_REPO)
+AC_SUBST(MOZ_GECKO_SOURCE_CHANGESET)
+AC_SUBST(MOZ_COMM_SOURCE_REPO)
+AC_SUBST(MOZ_COMM_SOURCE_CHANGESET)
+
 dnl Things we need to carry from confvars.sh
 AC_DEFINE(MOZ_SUITE)
 AC_SUBST(MOZ_SUITE)
 AC_SUBST(MOZ_COMPOSER)
 AC_SUBST(MOZ_PKG_VERSION)
 AC_SUBST(SEAMONKEY_VERSION)
+AC_SUBST(SEAMONKEY_VERSION_DISPLAY)
 AC_DEFINE(MOZ_SEPARATE_MANIFEST_FOR_THEME_OVERRIDES)
 AC_SUBST(MOZ_BUNDLED_FONTS)
 
 dnl Optional parts of the build.
 AC_SUBST(MOZ_MORK)
 if test "$MOZ_MORK"; then
   AC_DEFINE(MOZ_MORK)
 fi
diff --git a/suite/confvars.sh b/suite/confvars.sh
--- a/suite/confvars.sh
+++ b/suite/confvars.sh
@@ -25,16 +25,17 @@ MOZ_SERVICES_FXACCOUNTS=1
 
 MOZ_APP_VERSION_TXT=${_topsrcdir}/$MOZ_BUILD_APP/config/version.txt
 MOZ_APP_VERSION=`cat $MOZ_APP_VERSION_TXT`
 MOZ_APP_VERSION_DISPLAY_TXT=${_topsrcdir}/$MOZ_BUILD_APP/config/version_display.txt
 MOZ_APP_VERSION_DISPLAY=`cat $MOZ_APP_VERSION_DISPLAY_TXT`
 MOZ_PKG_VERSION_TXT=${_topsrcdir}/$MOZ_BUILD_APP/config/version_package.txt
 MOZ_PKG_VERSION=`cat $MOZ_PKG_VERSION_TXT`
 SEAMONKEY_VERSION=$MOZ_APP_VERSION
+SEAMONKEY_VERSION_DISPLAY=$MOZ_APP_VERSION_DISPLAY
 
 MOZ_APP_ID={92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}
 MOZ_PROFILE_MIGRATOR=1
 MOZ_SEPARATE_MANIFEST_FOR_THEME_OVERRIDES=1
 
 if test "$OS_ARCH" = "WINNT" -o \
         "$OS_ARCH" = "Linux"; then
   MOZ_BUNDLED_FONTS=1
diff --git a/suite/installer/Makefile.in b/suite/installer/Makefile.in
--- a/suite/installer/Makefile.in
+++ b/suite/installer/Makefile.in
@@ -221,13 +221,11 @@ endif
 package-compare:: $(MOZ_PKG_MANIFEST)
 ifdef MOZ_PKG_MANIFEST_P
 	cd $(DIST); find $(PKGCOMP_FIND_OPTS) $(FINDPATH) -type f | sort > bin-list.txt
 	grep "^$(BINPATH)" $(MOZ_PKG_MANIFEST) | sed -e 's/^\///' | sort > $(DIST)/pack-list.txt
 	-diff -u $(DIST)/pack-list.txt $(DIST)/bin-list.txt
 	rm -f $(DIST)/pack-list.txt $(DIST)/bin-list.txt
 endif
 
-# The comm-* source stamp is already there.
-PLATFORM_SOURCE_STAMP = $(firstword $(shell hg -R "$(MOZILLA_SRCDIR)" parent --template="{node|short}\n" 2>/dev/null))
-PLATFORM_SOURCE_REPO = $(shell hg -R "$(MOZILLA_SRCDIR)" showconfig paths.default 2>/dev/null | sed -e "s/^ssh:/https:/")
+# Overwrite the one made by Firefox with our own.
 make-sourcestamp-file::
-	@echo "$(PLATFORM_SOURCE_REPO)/rev/$(PLATFORM_SOURCE_STAMP)" >> $(MOZ_SOURCESTAMP_FILE)
+	$(PYTHON) $(topsrcdir)/build/source_repos.py gen_sourcestamp > $(MOZ_SOURCESTAMP_FILE)
