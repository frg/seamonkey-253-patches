# HG changeset patch
# User Jorg K <jorgk@jorgk.com>
# Date 1505324195 -7200
#      Wed Sep 13 19:36:35 2017 +0200
# Node ID fc81f4f432e341ca0dbbab4ed6d58bbe754d86ba
# Parent  ef2ec3ad49d7d0303f75d1628d0e11f3dfb93f73
Bug 1340972 - Part 10: Replace error-prone Addref()/Release() with using RefPtr in mailnews/imap. r=aceman

diff --git a/mailnews/imap/src/nsImapProtocol.cpp b/mailnews/imap/src/nsImapProtocol.cpp
--- a/mailnews/imap/src/nsImapProtocol.cpp
+++ b/mailnews/imap/src/nsImapProtocol.cpp
@@ -401,17 +401,16 @@ nsImapProtocol::nsImapProtocol() : nsMsg
   m_ignoreExpunges = false;
   m_prefAuthMethods = kCapabilityUndefined;
   m_failedAuthMethods = 0;
   m_currentAuthMethod = kCapabilityUndefined;
   m_socketType = nsMsgSocketType::trySTARTTLS;
   m_connectionStatus = NS_OK;
   m_safeToCloseConnection = false;
   m_hostSessionList = nullptr;
-  m_flagState = nullptr;
   m_fetchBodyIdList = nullptr;
   m_isGmailServer = false;
   m_fetchingWholeMessage = false;
 
   nsCOMPtr<nsIPrefBranch> prefBranch(do_GetService(NS_PREFSERVICE_CONTRACTID));
   NS_ASSERTION(prefBranch, "FAILED to create the preference service");
 
   // read in the accept languages preference
@@ -537,17 +536,16 @@ nsImapProtocol::Initialize(nsIImapHostSe
   m_flagState = new nsImapFlagAndUidState(kImapFlagAndUidStateSize);
   if (!m_flagState)
     return NS_ERROR_OUT_OF_MEMORY;
 
   aServer->GetUseIdle(&m_useIdle);
   aServer->GetForceSelect(m_forceSelectValue);
   aServer->GetUseCondStore(&m_useCondStore);
   aServer->GetUseCompressDeflate(&m_useCompressDeflate);
-  NS_ADDREF(m_flagState);
 
   m_hostSessionList = aHostSessionList; // no ref count...host session list has life time > connection
   m_parser.SetHostSessionList(aHostSessionList);
   m_parser.SetFlagState(m_flagState);
 
   // Initialize the empty mime part string on the main thread.
   rv = IMAPGetStringBundle(getter_AddRefs(m_bundle));
   NS_ENSURE_SUCCESS(rv, rv);
@@ -569,18 +567,16 @@ nsImapProtocol::Initialize(nsIImapHostSe
   }
   return NS_OK;
 }
 
 nsImapProtocol::~nsImapProtocol()
 {
   PR_Free(m_fetchBodyIdList);
 
-  NS_IF_RELEASE(m_flagState);
-
   PR_Free(m_dataOutputBuf);
   delete m_inputStreamBuffer;
 
   // **** We must be out of the thread main loop function
   NS_ASSERTION(!m_imapThreadIsRunning, "Oops, thread is still running.\n");
 }
 
 const nsCString&
@@ -4218,17 +4214,17 @@ void nsImapProtocol::ProcessMailboxUpdat
       return;
   }
 
   bool entered_waitForBodyIdsMonitor = false;
 
   uint32_t *msgIdList = nullptr;
   uint32_t msgCount = 0;
 
-  nsImapMailboxSpec *new_spec = GetServerStateParser().CreateCurrentMailboxSpec();
+  RefPtr<nsImapMailboxSpec> new_spec = GetServerStateParser().CreateCurrentMailboxSpec();
   if (new_spec && GetServerStateParser().LastCommandSuccessful())
   {
     nsImapAction imapAction;
     nsresult res = m_runningUrl->GetImapAction(&imapAction);
     if (NS_SUCCEEDED(res) && imapAction == nsIImapUrl::nsImapExpungeFolder)
       new_spec->mBoxFlags |= kJustExpunged;
     m_waitForBodyIdsMonitor.Enter();
     entered_waitForBodyIdsMonitor = true;
@@ -4243,18 +4239,16 @@ void nsImapProtocol::ProcessMailboxUpdat
       MOZ_ASSERT((m_stringIndex == IMAP_EMPTY_STRING_INDEX) || (m_stringIndex == IMAP_HEADERS_STRING_INDEX));
       m_progressCurrentNumber[m_stringIndex] = 0;
       m_runningUrl->SetMoreHeadersToDownload(more);
       // We're going to be re-running this url if there are more headers.
       if (more)
         m_runningUrl->SetRerunningUrl(true);
     }
   }
-  else if (!new_spec)
-    HandleMemoryFailure();
 
   if (GetServerStateParser().LastCommandSuccessful())
   {
     if (entered_waitForBodyIdsMonitor)
       m_waitForBodyIdsMonitor.Exit();
 
     if (msgIdList && !DeathSignalReceived() && GetServerStateParser().LastCommandSuccessful())
     {
@@ -4284,18 +4278,16 @@ void nsImapProtocol::ProcessMailboxUpdat
       m_progressCurrentNumber[m_stringIndex] = 0;
       m_progressExpectedNumber = msgCount;
       FolderMsgDump(msgIdList, msgCount, kEveryThingRFC822Peek);
       m_runningUrl->SetStoreResultsOffline(wasStoringOffline);
     }
   }
   if (!GetServerStateParser().LastCommandSuccessful())
     GetServerStateParser().ResetFlagInfo();
-
-  NS_IF_RELEASE(new_spec);
 }
 
 void nsImapProtocol::FolderHeaderDump(uint32_t *msgUids, uint32_t msgCount)
 {
   FolderMsgDump(msgUids, msgCount, kHeadersRFC822andUid);
 }
 
 void nsImapProtocol::FolderMsgDump(uint32_t *msgUids, uint32_t msgCount, nsIMAPeFetchFields fields)
@@ -4939,25 +4931,23 @@ nsImapProtocol::DiscoverMailboxSpec(nsIm
       m_specialXListMailboxes.Put(mailboxName, adoptedBoxSpec->mBoxFlags);
       // Remember hierarchy delimiter in case this is the first time we've
       // connected to the server and we need it to be correct for the two-level
       // XLIST we send (INBOX is guaranteed to be in the first response).
       if (adoptedBoxSpec->mBoxFlags & kImapInbox)
         m_runningUrl->SetOnlineSubDirSeparator(adoptedBoxSpec->mHierarchySeparator);
 
     }
-    NS_IF_RELEASE(adoptedBoxSpec);
     break;
   case kListingForFolderFlags:
     {
       // store mailbox flags from LIST for use by LSUB
       nsCString mailboxName(adoptedBoxSpec->mAllocatedPathName);
       m_standardListMailboxes.Put(mailboxName, adoptedBoxSpec->mBoxFlags);
     }
-    NS_IF_RELEASE(adoptedBoxSpec);
     break;
   case kListingForCreate:
   case kNoOperationInProgress:
   case kDiscoverTrashFolderInProgress:
   case kListingForInfoAndDiscovery:
     {
       // standard mailbox specs are stored in m_standardListMailboxes
       // because LSUB does necessarily return all mailbox flags.
@@ -5071,41 +5061,35 @@ nsImapProtocol::DiscoverMailboxSpec(nsIm
               nsIMAPMailboxInfo *mb = new nsIMAPMailboxInfo(adoptedBoxSpec->mAllocatedPathName, adoptedBoxSpec->mHierarchySeparator);
               m_listedMailboxList.AppendElement(mb);
             }
             SetMailboxDiscoveryStatus(eContinue);
           }
         }
       }
       }
-      NS_IF_RELEASE( adoptedBoxSpec);
-      break;
-    case kDiscoverBaseFolderInProgress:
       break;
     case kDeleteSubFoldersInProgress:
       {
         NS_ASSERTION(m_deletableChildren, "Oops .. null m_deletableChildren\n");
         m_deletableChildren->AppendElement(ToNewCString(adoptedBoxSpec->mAllocatedPathName));
-        NS_IF_RELEASE(adoptedBoxSpec);
       }
       break;
     case kListingForInfoOnly:
       {
         //UpdateProgressWindowForUpgrade(adoptedBoxSpec->allocatedPathName);
         ProgressEventFunctionUsingNameWithString("imapDiscoveringMailbox",
           adoptedBoxSpec->mAllocatedPathName.get());
         nsIMAPMailboxInfo *mb = new nsIMAPMailboxInfo(adoptedBoxSpec->mAllocatedPathName,
                                                       adoptedBoxSpec->mHierarchySeparator);
         m_listedMailboxList.AppendElement(mb);
-        NS_IF_RELEASE(adoptedBoxSpec);
       }
       break;
     case kDiscoveringNamespacesOnly:
       {
-        NS_IF_RELEASE(adoptedBoxSpec);
       }
       break;
     default:
       NS_ASSERTION (false, "we aren't supposed to be here");
       break;
   }
 }
 
@@ -6745,20 +6729,19 @@ void nsImapProtocol::OnStatusForFolder(c
   command.Append("\" (UIDNEXT MESSAGES UNSEEN RECENT)" CRLF);
 
   nsresult rv = SendData(command.get());
   if (NS_SUCCEEDED(rv))
       ParseIMAPandCheckForNewMail();
 
   if (GetServerStateParser().LastCommandSuccessful())
   {
-    nsImapMailboxSpec *new_spec = GetServerStateParser().CreateCurrentMailboxSpec(mailboxName);
+    RefPtr<nsImapMailboxSpec> new_spec = GetServerStateParser().CreateCurrentMailboxSpec(mailboxName);
     if (new_spec && m_imapMailFolderSink)
       m_imapMailFolderSink->UpdateImapMailboxStatus(this, new_spec);
-    NS_IF_RELEASE(new_spec);
   }
 }
 
 
 void nsImapProtocol::OnListFolder(const char * aSourceMailbox, bool aBool)
 {
   List(aSourceMailbox, aBool);
 }
@@ -7275,53 +7258,47 @@ void nsImapProtocol::DiscoverAllAndSubsc
         inboxNameWithDelim.Append(ns->GetDelimiter());
 
         if (!gHideUnusedNamespaces && *prefix &&
         PL_strcasecmp(prefix, inboxNameWithDelim.get())) /* only do it for
         non-empty namespace prefixes */
         {
           // Explicitly discover each Namespace, just so they're
           // there in the subscribe UI
-          nsImapMailboxSpec *boxSpec = new nsImapMailboxSpec;
-          if (boxSpec)
+          RefPtr<nsImapMailboxSpec> boxSpec = new nsImapMailboxSpec;
+          boxSpec->mFolderSelected = false;
+          boxSpec->mHostName.Assign(GetImapHostName());
+          boxSpec->mConnection = this;
+          boxSpec->mFlagState = nullptr;
+          boxSpec->mDiscoveredFromLsub = true;
+          boxSpec->mOnlineVerified = true;
+          boxSpec->mBoxFlags = kNoselect;
+          boxSpec->mHierarchySeparator = ns->GetDelimiter();
+
+          m_runningUrl->AllocateCanonicalPath(ns->GetPrefix(), ns->GetDelimiter(),
+                                              getter_Copies(boxSpec->mAllocatedPathName));
+          boxSpec->mNamespaceForFolder = ns;
+          boxSpec->mBoxFlags |= kNameSpace;
+
+          switch (ns->GetType())
           {
-            NS_ADDREF(boxSpec);
-            boxSpec->mFolderSelected = false;
-            boxSpec->mHostName.Assign(GetImapHostName());
-            boxSpec->mConnection = this;
-            boxSpec->mFlagState = nullptr;
-            boxSpec->mDiscoveredFromLsub = true;
-            boxSpec->mOnlineVerified = true;
-            boxSpec->mBoxFlags = kNoselect;
-            boxSpec->mHierarchySeparator = ns->GetDelimiter();
-
-            m_runningUrl->AllocateCanonicalPath(ns->GetPrefix(), ns->GetDelimiter(),
-                                                getter_Copies(boxSpec->mAllocatedPathName));
-            boxSpec->mNamespaceForFolder = ns;
-            boxSpec->mBoxFlags |= kNameSpace;
-
-            switch (ns->GetType())
-            {
-            case kPersonalNamespace:
-              boxSpec->mBoxFlags |= kPersonalMailbox;
-              break;
-            case kPublicNamespace:
-              boxSpec->mBoxFlags |= kPublicMailbox;
-              break;
-            case kOtherUsersNamespace:
-              boxSpec->mBoxFlags |= kOtherUsersMailbox;
-              break;
-            default:	// (kUnknownNamespace)
-              break;
-            }
-
-            DiscoverMailboxSpec(boxSpec);
+          case kPersonalNamespace:
+            boxSpec->mBoxFlags |= kPersonalMailbox;
+            break;
+          case kPublicNamespace:
+            boxSpec->mBoxFlags |= kPublicMailbox;
+            break;
+          case kOtherUsersNamespace:
+            boxSpec->mBoxFlags |= kOtherUsersMailbox;
+            break;
+          default:	// (kUnknownNamespace)
+            break;
           }
-          else
-            HandleMemoryFailure();
+
+          DiscoverMailboxSpec(boxSpec);
         }
 
         nsAutoCString allPattern(prefix);
         allPattern += '*';
 
         nsAutoCString topLevelPattern(prefix);
         topLevelPattern += '%';
 
@@ -7416,54 +7393,48 @@ void nsImapProtocol::DiscoverMailboxList
         // dmb - we should get this from a per-host preference,
         // I'd say. But for now, just make it true;
         if (!gHideUnusedNamespaces && *prefix &&
                     PL_strcasecmp(prefix, inboxNameWithDelim.get()))  // only do it for
                     // non-empty namespace prefixes, and for non-INBOX prefix
         {
           // Explicitly discover each Namespace, so that we can
                     // create subfolders of them,
-          nsImapMailboxSpec *boxSpec = new nsImapMailboxSpec;
-          if (boxSpec)
+          RefPtr<nsImapMailboxSpec> boxSpec = new nsImapMailboxSpec;
+          boxSpec->mFolderSelected = false;
+          boxSpec->mHostName = GetImapHostName();
+          boxSpec->mConnection = this;
+          boxSpec->mFlagState = nullptr;
+          boxSpec->mDiscoveredFromLsub = true;
+          boxSpec->mOnlineVerified = true;
+          boxSpec->mBoxFlags = kNoselect;
+          boxSpec->mHierarchySeparator = ns->GetDelimiter();
+          // Until |AllocateCanonicalPath()| gets updated:
+          m_runningUrl->AllocateCanonicalPath(
+                          ns->GetPrefix(), ns->GetDelimiter(),
+                          getter_Copies(boxSpec->mAllocatedPathName));
+          boxSpec->mNamespaceForFolder = ns;
+          boxSpec->mBoxFlags |= kNameSpace;
+
+          switch (ns->GetType())
           {
-            NS_ADDREF(boxSpec);
-            boxSpec->mFolderSelected = false;
-            boxSpec->mHostName = GetImapHostName();
-            boxSpec->mConnection = this;
-            boxSpec->mFlagState = nullptr;
-            boxSpec->mDiscoveredFromLsub = true;
-            boxSpec->mOnlineVerified = true;
-            boxSpec->mBoxFlags = kNoselect;
-            boxSpec->mHierarchySeparator = ns->GetDelimiter();
-            // Until |AllocateCanonicalPath()| gets updated:
-            m_runningUrl->AllocateCanonicalPath(
-                            ns->GetPrefix(), ns->GetDelimiter(),
-                            getter_Copies(boxSpec->mAllocatedPathName));
-            boxSpec->mNamespaceForFolder = ns;
-            boxSpec->mBoxFlags |= kNameSpace;
-
-            switch (ns->GetType())
-            {
-            case kPersonalNamespace:
-              boxSpec->mBoxFlags |= kPersonalMailbox;
-              break;
-            case kPublicNamespace:
-              boxSpec->mBoxFlags |= kPublicMailbox;
-              break;
-            case kOtherUsersNamespace:
-              boxSpec->mBoxFlags |= kOtherUsersMailbox;
-              break;
-            default:  // (kUnknownNamespace)
-              break;
-            }
-
-            DiscoverMailboxSpec(boxSpec);
+          case kPersonalNamespace:
+            boxSpec->mBoxFlags |= kPersonalMailbox;
+            break;
+          case kPublicNamespace:
+            boxSpec->mBoxFlags |= kPublicMailbox;
+            break;
+          case kOtherUsersNamespace:
+            boxSpec->mBoxFlags |= kOtherUsersMailbox;
+            break;
+          default:  // (kUnknownNamespace)
+            break;
           }
-          else
-            HandleMemoryFailure();
+
+          DiscoverMailboxSpec(boxSpec);
         }
 
         // now do the folders within this namespace
         nsCString pattern;
         nsCString pattern2;
         if (usingSubscription)
         {
           pattern.Append(prefix);
@@ -9551,21 +9522,19 @@ nsresult nsImapMockChannel::ReadFromMemC
     if (!bytesAvailable)
       return NS_ERROR_FAILURE;
 
     nsCOMPtr<nsIInputStreamPump> pump;
     rv = NS_NewInputStreamPump(getter_AddRefs(pump), in);
     NS_ENSURE_SUCCESS(rv, rv);
 
     // if we are going to read from the cache, then create a mock stream listener class and use it
-    nsImapCacheStreamListener * cacheListener = new nsImapCacheStreamListener();
-    NS_ADDREF(cacheListener);
+    RefPtr<nsImapCacheStreamListener> cacheListener = new nsImapCacheStreamListener();
     cacheListener->Init(m_channelListener, this);
     rv = pump->AsyncRead(cacheListener, m_channelContext);
-    NS_RELEASE(cacheListener);
 
     if (NS_SUCCEEDED(rv)) // ONLY if we succeeded in actually starting the read should we return
     {
       mCacheRequest = pump;
       nsCOMPtr<nsIImapUrl> imapUrl = do_QueryInterface(m_url);
       // if the msg is unread, we should mark it read on the server. This lets
       // the code running this url we're loading from the cache, if it cares.
       imapUrl->SetMsgLoadingFromCache(true);
@@ -9691,30 +9660,27 @@ bool nsImapMockChannel::ReadFromLocalCac
       int64_t offset;
       rv = folder->GetOfflineFileStream(msgKey, &offset, &size, getter_AddRefs(fileStream));
       // get the file channel from the folder, somehow (through the message or
       // folder sink?) We also need to set the transfer offset to the message offset
       if (fileStream && NS_SUCCEEDED(rv))
       {
         // dougt - This may break the ablity to "cancel" a read from offline mail reading.
         // fileChannel->SetLoadGroup(m_loadGroup);
-        nsImapCacheStreamListener * cacheListener = new nsImapCacheStreamListener();
-        NS_ADDREF(cacheListener);
+        RefPtr<nsImapCacheStreamListener> cacheListener = new nsImapCacheStreamListener();
         cacheListener->Init(m_channelListener, this);
 
         // create a stream pump that will async read the specified amount of data.
         // XXX make offset/size 64-bit ints
         nsCOMPtr<nsIInputStreamPump> pump;
         rv = NS_NewInputStreamPump(getter_AddRefs(pump), fileStream,
                                    offset, (int64_t) size);
         if (NS_SUCCEEDED(rv))
           rv = pump->AsyncRead(cacheListener, m_channelContext);
 
-        NS_RELEASE(cacheListener);
-
         if (NS_SUCCEEDED(rv)) // ONLY if we succeeded in actually starting the read should we return
         {
           // if the msg is unread, we should mark it read on the server. This lets
           // the code running this url we're loading from the cache, if it cares.
           imapUrl->SetMsgLoadingFromCache(true);
           return true;
         }
       } // if we got an offline file transport
diff --git a/mailnews/imap/src/nsImapProtocol.h b/mailnews/imap/src/nsImapProtocol.h
--- a/mailnews/imap/src/nsImapProtocol.h
+++ b/mailnews/imap/src/nsImapProtocol.h
@@ -460,17 +460,17 @@ private:
   bool m_threadShouldDie;
 
   // use to prevent re-entering TellThreadToDie.
   bool m_inThreadShouldDie;
   // if the UI thread has signaled the IMAP thread to die, and the
   // connection has timed out, this will be set to FALSE.
   bool m_safeToCloseConnection;
 
-  nsImapFlagAndUidState  *m_flagState;
+  RefPtr<nsImapFlagAndUidState> m_flagState;
   nsMsgBiffState        m_currentBiffState;
   // manage the IMAP server command tags
   // 11 = enough memory for the decimal representation of MAX_UINT + trailing nul
   char m_currentServerCommandTag[11];
   uint32_t m_currentServerCommandTagNumber;
   void IncrementCommandTagNumber();
   const char *GetServerCommandTag();
 
@@ -656,18 +656,18 @@ private:
   bool m_retryUrlOnError;
   bool m_preferPlainText;
   nsCString m_forceSelectValue;
   bool m_forceSelect;
 
   int32_t m_uidValidity; // stored uid validity for the selected folder.
 
   enum EMailboxHierarchyNameState {
-    kNoOperationInProgress,
-      kDiscoverBaseFolderInProgress,
+      kNoOperationInProgress,
+      // kDiscoverBaseFolderInProgress, - Unused. Keeping for historical reasons.
       kDiscoverTrashFolderInProgress,
       kDeleteSubFoldersInProgress,
       kListingForInfoOnly,
       kListingForInfoAndDiscovery,
       kDiscoveringNamespacesOnly,
       kXListing,
       kListingForFolderFlags,
       kListingForCreate
diff --git a/mailnews/imap/src/nsImapServerResponseParser.cpp b/mailnews/imap/src/nsImapServerResponseParser.cpp
--- a/mailnews/imap/src/nsImapServerResponseParser.cpp
+++ b/mailnews/imap/src/nsImapServerResponseParser.cpp
@@ -65,18 +65,16 @@ nsImapServerResponseParser::~nsImapServe
   PR_Free( fCurrentCommandTag );
   delete fSearchResults;
   PR_Free( fFolderAdminUrl );
   PR_Free( fNetscapeServerVersionString );
   PR_Free( fXSenderInfo );
   PR_Free( fLastAlert );
   PR_Free( fSelectedMailboxName );
   PR_Free(fAuthChallenge);
-
-  NS_IF_RELEASE (fHostSessionList);
 }
 
 bool nsImapServerResponseParser::LastCommandSuccessful()
 {
   return (!CommandFailed() &&
     !fServerConnection.DeathSignalReceived() &&
     nsIMAPGenericParser::LastCommandSuccessful());
 }
@@ -786,109 +784,95 @@ void nsImapServerResponseParser::mailbox
 /*
  mailbox_list    ::= "(" #("\Marked" / "\Noinferiors" /
                               "\Noselect" / "\Unmarked" / flag_extension) ")"
                               SPACE (<"> QUOTED_CHAR <"> / nil) SPACE mailbox
 */
 
 void nsImapServerResponseParser::mailbox_list(bool discoveredFromLsub)
 {
-  nsImapMailboxSpec *boxSpec = new nsImapMailboxSpec;
-  NS_ADDREF(boxSpec);
-  bool needsToFreeBoxSpec = true;
-  if (!boxSpec)
-    HandleMemoryFailure();
-  else
-  {
-    boxSpec->mFolderSelected = false;
-    boxSpec->mBoxFlags = kNoFlags;
-    boxSpec->mAllocatedPathName.Truncate();
-    boxSpec->mHostName.Truncate();
-    boxSpec->mConnection = &fServerConnection;
-    boxSpec->mFlagState = nullptr;
-    boxSpec->mDiscoveredFromLsub = discoveredFromLsub;
-    boxSpec->mOnlineVerified = true;
-    boxSpec->mBoxFlags &= ~kNameSpace;
+  RefPtr<nsImapMailboxSpec> boxSpec = new nsImapMailboxSpec;
+  boxSpec->mFolderSelected = false;
+  boxSpec->mBoxFlags = kNoFlags;
+  boxSpec->mAllocatedPathName.Truncate();
+  boxSpec->mHostName.Truncate();
+  boxSpec->mConnection = &fServerConnection;
+  boxSpec->mFlagState = nullptr;
+  boxSpec->mDiscoveredFromLsub = discoveredFromLsub;
+  boxSpec->mOnlineVerified = true;
+  boxSpec->mBoxFlags &= ~kNameSpace;
 
-    bool endOfFlags = false;
-    fNextToken++;// eat the first "("
-    do {
-      if (!PL_strncasecmp(fNextToken, "\\Marked", 7))
-        boxSpec->mBoxFlags |= kMarked;
-      else if (!PL_strncasecmp(fNextToken, "\\Unmarked", 9))
-        boxSpec->mBoxFlags |= kUnmarked;
-      else if (!PL_strncasecmp(fNextToken, "\\Noinferiors", 12))
-      {
-        boxSpec->mBoxFlags |= kNoinferiors;
-        // RFC 5258 \Noinferiors implies \HasNoChildren
-        if (fCapabilityFlag & kHasListExtendedCapability)
-          boxSpec->mBoxFlags |= kHasNoChildren;
-      }
-      else if (!PL_strncasecmp(fNextToken, "\\Noselect", 9))
-        boxSpec->mBoxFlags |= kNoselect;
-      else if (!PL_strncasecmp(fNextToken, "\\Drafts", 7))
-        boxSpec->mBoxFlags |= kImapDrafts;
-      else if (!PL_strncasecmp(fNextToken, "\\Trash", 6))
-        boxSpec->mBoxFlags |= kImapXListTrash;
-      else if (!PL_strncasecmp(fNextToken, "\\Sent", 5))
-        boxSpec->mBoxFlags |= kImapSent;
-      else if (!PL_strncasecmp(fNextToken, "\\Spam", 5) ||
-               !PL_strncasecmp(fNextToken, "\\Junk", 5))
-        boxSpec->mBoxFlags |= kImapSpam;
-      else if (!PL_strncasecmp(fNextToken, "\\Archive", 8))
-        boxSpec->mBoxFlags |= kImapArchive;
-      else if (!PL_strncasecmp(fNextToken, "\\All", 4) ||
-               !PL_strncasecmp(fNextToken, "\\AllMail", 8))
-        boxSpec->mBoxFlags |= kImapAllMail;
-      else if (!PL_strncasecmp(fNextToken, "\\Inbox", 6))
-        boxSpec->mBoxFlags |= kImapInbox;
-      else if (!PL_strncasecmp(fNextToken, "\\NonExistent", 11))
-      {
-        boxSpec->mBoxFlags |= kNonExistent;
-        // RFC 5258 \NonExistent implies \Noselect
-        boxSpec->mBoxFlags |= kNoselect;
-      }
-      else if (!PL_strncasecmp(fNextToken, "\\Subscribed", 10))
-        boxSpec->mBoxFlags |= kSubscribed;
-      else if (!PL_strncasecmp(fNextToken, "\\Remote", 6))
-        boxSpec->mBoxFlags |= kRemote;
-      else if (!PL_strncasecmp(fNextToken, "\\HasChildren", 11))
-        boxSpec->mBoxFlags |= kHasChildren;
-      else if (!PL_strncasecmp(fNextToken, "\\HasNoChildren", 13))
+  bool endOfFlags = false;
+  fNextToken++;// eat the first "("
+  do {
+    if (!PL_strncasecmp(fNextToken, "\\Marked", 7))
+      boxSpec->mBoxFlags |= kMarked;
+    else if (!PL_strncasecmp(fNextToken, "\\Unmarked", 9))
+      boxSpec->mBoxFlags |= kUnmarked;
+    else if (!PL_strncasecmp(fNextToken, "\\Noinferiors", 12))
+    {
+      boxSpec->mBoxFlags |= kNoinferiors;
+      // RFC 5258 \Noinferiors implies \HasNoChildren
+      if (fCapabilityFlag & kHasListExtendedCapability)
         boxSpec->mBoxFlags |= kHasNoChildren;
-      // we ignore flag other extensions
-
-      endOfFlags = *(fNextToken + strlen(fNextToken) - 1) == ')';
-      AdvanceToNextToken();
-    } while (!endOfFlags && ContinueParse());
-
-    if (ContinueParse())
+    }
+    else if (!PL_strncasecmp(fNextToken, "\\Noselect", 9))
+      boxSpec->mBoxFlags |= kNoselect;
+    else if (!PL_strncasecmp(fNextToken, "\\Drafts", 7))
+      boxSpec->mBoxFlags |= kImapDrafts;
+    else if (!PL_strncasecmp(fNextToken, "\\Trash", 6))
+      boxSpec->mBoxFlags |= kImapXListTrash;
+    else if (!PL_strncasecmp(fNextToken, "\\Sent", 5))
+      boxSpec->mBoxFlags |= kImapSent;
+    else if (!PL_strncasecmp(fNextToken, "\\Spam", 5) ||
+             !PL_strncasecmp(fNextToken, "\\Junk", 5))
+      boxSpec->mBoxFlags |= kImapSpam;
+    else if (!PL_strncasecmp(fNextToken, "\\Archive", 8))
+      boxSpec->mBoxFlags |= kImapArchive;
+    else if (!PL_strncasecmp(fNextToken, "\\All", 4) ||
+             !PL_strncasecmp(fNextToken, "\\AllMail", 8))
+      boxSpec->mBoxFlags |= kImapAllMail;
+    else if (!PL_strncasecmp(fNextToken, "\\Inbox", 6))
+      boxSpec->mBoxFlags |= kImapInbox;
+    else if (!PL_strncasecmp(fNextToken, "\\NonExistent", 11))
     {
-      if (*fNextToken == '"')
-      {
-        fNextToken++;
-        if (*fNextToken == '\\') // handle escaped char
-          boxSpec->mHierarchySeparator = *(fNextToken + 1);
-        else
-          boxSpec->mHierarchySeparator = *fNextToken;
-      }
-      else	// likely NIL.  Discovered late in 4.02 that we do not handle literals here (e.g. {10} <10 chars>), although this is almost impossibly unlikely
-        boxSpec->mHierarchySeparator = kOnlineHierarchySeparatorNil;
-      AdvanceToNextToken();
-      if (ContinueParse())
-      {
-        // nsImapProtocol::DiscoverMailboxSpec() eventually frees the
-        // boxSpec
-        needsToFreeBoxSpec = false;
-        mailbox(boxSpec);
-      }
+      boxSpec->mBoxFlags |= kNonExistent;
+      // RFC 5258 \NonExistent implies \Noselect
+      boxSpec->mBoxFlags |= kNoselect;
     }
+    else if (!PL_strncasecmp(fNextToken, "\\Subscribed", 10))
+      boxSpec->mBoxFlags |= kSubscribed;
+    else if (!PL_strncasecmp(fNextToken, "\\Remote", 6))
+      boxSpec->mBoxFlags |= kRemote;
+    else if (!PL_strncasecmp(fNextToken, "\\HasChildren", 11))
+      boxSpec->mBoxFlags |= kHasChildren;
+    else if (!PL_strncasecmp(fNextToken, "\\HasNoChildren", 13))
+      boxSpec->mBoxFlags |= kHasNoChildren;
+    // we ignore flag other extensions
+
+    endOfFlags = *(fNextToken + strlen(fNextToken) - 1) == ')';
+    AdvanceToNextToken();
+  } while (!endOfFlags && ContinueParse());
+
+  if (ContinueParse())
+  {
+    if (*fNextToken == '"')
+    {
+      fNextToken++;
+      if (*fNextToken == '\\') // handle escaped char
+        boxSpec->mHierarchySeparator = *(fNextToken + 1);
+      else
+        boxSpec->mHierarchySeparator = *fNextToken;
+    }
+    else	// likely NIL.  Discovered late in 4.02 that we do not handle literals here (e.g. {10} <10 chars>), although this is almost impossibly unlikely
+      boxSpec->mHierarchySeparator = kOnlineHierarchySeparatorNil;
+    AdvanceToNextToken();
+    if (ContinueParse())
+      mailbox(boxSpec);
   }
-  if (needsToFreeBoxSpec)
-    NS_RELEASE(boxSpec);
 }
 
 /* mailbox         ::= "INBOX" / astring
 */
 void nsImapServerResponseParser::mailbox(nsImapMailboxSpec *boxSpec)
 {
   char *boxname = nullptr;
   const char *serverKey = fServerConnection.GetImapServerKey();
@@ -3234,25 +3218,20 @@ bool nsImapServerResponseParser::IsNumer
       return false;
     }
   }
 
   return true;
 }
 
 
-nsImapMailboxSpec *nsImapServerResponseParser::CreateCurrentMailboxSpec(const char *mailboxName /* = nullptr */)
+already_AddRefed<nsImapMailboxSpec>
+nsImapServerResponseParser::CreateCurrentMailboxSpec(const char *mailboxName /* = nullptr */)
 {
-  nsImapMailboxSpec *returnSpec = new nsImapMailboxSpec;
-  if (!returnSpec)
-  {
-    HandleMemoryFailure();
-    return  nullptr;
-  }
-  NS_ADDREF(returnSpec);
+  RefPtr<nsImapMailboxSpec> returnSpec = new nsImapMailboxSpec;
   const char *mailboxNameToConvert = (mailboxName) ? mailboxName : fSelectedMailboxName;
   if (mailboxNameToConvert)
   {
     const char *serverKey = fServerConnection.GetImapServerKey();
     nsIMAPNamespace *ns = nullptr;
     if (serverKey && fHostSessionList)
       fHostSessionList->GetNamespaceForMailboxForHost(serverKey, mailboxNameToConvert, ns);	// for
       // delimiter
@@ -3287,17 +3266,17 @@ nsImapMailboxSpec *nsImapServerResponseP
   else
     returnSpec->mHostName.Truncate();
 
   if (fFlagState)
     returnSpec->mFlagState = fFlagState; //copies flag state
   else
     returnSpec->mFlagState = nullptr;
 
-  return returnSpec;
+  return returnSpec.forget();
 
 }
 // Reset the flag state.
 void nsImapServerResponseParser::ResetFlagInfo()
 {
   if (fFlagState)
     fFlagState->Reset();
 }
@@ -3311,19 +3290,17 @@ bool nsImapServerResponseParser::GetLast
 void nsImapServerResponseParser::ClearLastFetchChunkReceived()
 {
   fLastChunk = false;
 }
 
 void nsImapServerResponseParser::SetHostSessionList(nsIImapHostSessionList*
                                                aHostSessionList)
 {
-    NS_IF_RELEASE (fHostSessionList);
     fHostSessionList = aHostSessionList;
-    NS_IF_ADDREF (fHostSessionList);
 }
 
 void nsImapServerResponseParser::SetSyntaxError(bool error, const char *msg)
 {
   nsIMAPGenericParser::SetSyntaxError(error, msg);
   if (error)
   {
     if (!fCurrentLine)
diff --git a/mailnews/imap/src/nsImapServerResponseParser.h b/mailnews/imap/src/nsImapServerResponseParser.h
--- a/mailnews/imap/src/nsImapServerResponseParser.h
+++ b/mailnews/imap/src/nsImapServerResponseParser.h
@@ -72,17 +72,17 @@ public:
   uint32_t   SizeOfMostRecentMessage();
   void       SetTotalDownloadSize(int32_t newSize) { fTotalDownloadSize = newSize; }
 
   nsImapSearchResultIterator *CreateSearchResultIterator();
   void ResetSearchResultSequence() {fSearchResults->ResetSequence();}
 
   // create a struct mailbox_spec from our info, used in
   // libmsg c interface
-  nsImapMailboxSpec *CreateCurrentMailboxSpec(const char *mailboxName = nullptr);
+  already_AddRefed<nsImapMailboxSpec> CreateCurrentMailboxSpec(const char *mailboxName = nullptr);
 
   // Resets the flags state.
   void ResetFlagInfo();
 
   // set this to false if you don't want to alert the user to server
   // error messages
   void SetReportingErrors(bool reportThem) { fReportingErrors=reportThem;}
   bool GetReportingErrors() { return fReportingErrors; }
@@ -256,13 +256,13 @@ private:
   bool fLastChunk;
 
   // points to the current body shell, if any
   RefPtr<nsIMAPBodyShell> m_shell;
 
   // The connection object
   nsImapProtocol &fServerConnection;
 
-  nsIImapHostSessionList *fHostSessionList;
+  RefPtr<nsIImapHostSessionList> fHostSessionList;
   nsTArray<nsMsgKey> fCopyResponseKeyArray;
 };
 
 #endif
diff --git a/mailnews/imap/src/nsImapService.cpp b/mailnews/imap/src/nsImapService.cpp
--- a/mailnews/imap/src/nsImapService.cpp
+++ b/mailnews/imap/src/nsImapService.cpp
@@ -3317,23 +3317,21 @@ NS_IMETHODIMP nsImapService::PlaybackAll
       return goOnline->ProcessNextOperation();
   }
   return NS_ERROR_OUT_OF_MEMORY;
 }
 
 NS_IMETHODIMP nsImapService::DownloadAllOffineImapFolders(nsIMsgWindow *aMsgWindow,
                                                           nsIUrlListener *aListener)
 {
-  nsImapOfflineDownloader *downloadForOffline = new nsImapOfflineDownloader(aMsgWindow, aListener);
+  RefPtr<nsImapOfflineDownloader> downloadForOffline = new nsImapOfflineDownloader(aMsgWindow, aListener);
   if (downloadForOffline)
   {
     // hold reference to this so it won't get deleted out from under itself.
-    NS_ADDREF(downloadForOffline);
     nsresult rv = downloadForOffline->ProcessNextOperation();
-    NS_RELEASE(downloadForOffline);
     return rv;
   }
   return NS_ERROR_OUT_OF_MEMORY;
 }
 
 NS_IMETHODIMP nsImapService::GetCacheStorage(nsICacheStorage **result)
 {
   nsresult rv = NS_OK;
