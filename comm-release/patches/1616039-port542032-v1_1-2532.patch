# HG changeset patch
# User Ian Neal <iann_cvs@blueyonder.co.uk>
# Date 1581955601 0
# Parent  8fc6fff54e32341578b8ca9663f1020f234660e7
Bug 1616039 - Port |Bug 542032 - Don't look up prefs on every tab & window close| and |Bug 758138 - Current pref value is read before registering the observer| to SeaMonkey. r=frg a=frg

diff --git a/suite/components/sessionstore/nsSessionStore.js b/suite/components/sessionstore/nsSessionStore.js
--- a/suite/components/sessionstore/nsSessionStore.js
+++ b/suite/components/sessionstore/nsSessionStore.js
@@ -196,18 +196,20 @@ SessionStoreService.prototype = {
   /**
    * Initialize the component
    */
   initService: function() {
     OBSERVING.forEach(function(aTopic) {
       Services.obs.addObserver(this, aTopic, true);
     }, this);
 
-    // observe prefs changes so we can modify stored data to match
+    this._max_tabs_undo = this._prefBranch.getIntPref("sessionstore.max_tabs_undo");
     this._prefBranch.addObserver("sessionstore.max_tabs_undo", this, true);
+
+    this._max_windows_undo = this._prefBranch.getIntPref("sessionstore.max_windows_undo");
     this._prefBranch.addObserver("sessionstore.max_windows_undo", this, true);
 
     // this pref is only read at startup, so no need to observe it
     this._sessionhistory_max_entries =
       this._prefBranch.getIntPref("sessionhistory.max_entries");
 
     this._maxConcurrentTabRestores =
       this._prefBranch.getIntPref("sessionstore.max_concurrent_tabs");
@@ -428,21 +430,23 @@ SessionStoreService.prototype = {
       else if (this._loadState == STATE_RUNNING)
         this.saveState(true);
       break;
     case "nsPref:changed": // catch pref changes
       switch (aData) {
       // if the user decreases the max number of closed tabs they want
       // preserved update our internal states to match that max
       case "sessionstore.max_tabs_undo":
+        this._max_tabs_undo = this._prefBranch.getIntPref("sessionstore.max_tabs_undo");
         for (let ix in this._windows) {
-          this._windows[ix]._closedTabs.splice(this._prefBranch.getIntPref("sessionstore.max_tabs_undo"), this._windows[ix]._closedTabs.length);
+          this._windows[ix]._closedTabs.splice(this._max_tabs_undo, this._windows[ix]._closedTabs.length);
         }
         break;
       case "sessionstore.max_windows_undo":
+        this._max_windows_undo = this._prefBranch.getIntPref("sessionstore.max_windows_undo");
         this._capClosedWindows();
         break;
       case "sessionstore.interval":
         this._interval = this._prefBranch.getIntPref("sessionstore.interval");
         // reset timer and save
         if (this._saveTimer) {
           this._saveTimer.cancel();
           this._saveTimer = null;
@@ -795,33 +799,32 @@ SessionStoreService.prototype = {
    */
   onTabClose: function sss_onTabClose(aWindow, aTab) {
     // notify the tabbrowser that the tab state will be retrieved for the last time
     // (so that extension authors can easily set data on soon-to-be-closed tabs)
     var event = aWindow.document.createEvent("Events");
     event.initEvent("SSTabClosing", true, false);
     aTab.dispatchEvent(event);
 
-    var maxTabsUndo = this._prefBranch.getIntPref("sessionstore.max_tabs_undo");
     // don't update our internal state if we don't have to
-    if (maxTabsUndo == 0) {
+    if (this._max_tabs_undo == 0) {
       return;
     }
 
     // make sure that the tab related data is up-to-date
     var tabState = this._collectTabData(aTab);
     this._updateTextAndScrollDataForTab(aWindow, aTab.linkedBrowser, tabState);
 
     // store closed-tab data for undo
     if (this._shouldSaveTabState(tabState)) {
       aTab.tabData = { state: tabState };
       var closedTabs = this._windows[aWindow.__SSi]._closedTabs;
       closedTabs.unshift(aTab.tabData);
-      if (closedTabs.length > maxTabsUndo)
-        closedTabs.length = maxTabsUndo;
+      if (closedTabs.length > this._max_tabs_undo)
+        closedTabs.length = this._max_tabs_undo;
     };
   },
 
   /**
    * When a tab loads, save state.
    * @param aWindow
    *        Window reference
    * @param aBrowser
@@ -1284,17 +1287,17 @@ SessionStoreService.prototype = {
 
       // If there's a window already open that we can restore into, use that
       if (canUseWindow) {
         // Since we're not overwriting existing tabs, we want to merge _closedTabs,
         // putting existing ones first. Then make sure we're respecting the max pref.
         if (winState._closedTabs && winState._closedTabs.length) {
           let curWinState = this._windows[windowToUse.__SSi];
           curWinState._closedTabs = curWinState._closedTabs.concat(winState._closedTabs);
-          curWinState._closedTabs.splice(this._prefBranch.getIntPref("sessionstore.max_tabs_undo"), curWinState._closedTabs.length);
+          curWinState._closedTabs.splice(this._max_tabs_undo, curWinState._closedTabs.length);
         }
 
         // Restore into that window - pretend it's a followup since we'll already
         // have a focused window.
         //XXXzpao This is going to merge extData together (taking what was in
         //        winState over what is in the window already. The hack we have
         //        in _preWindowToRestoreInto will prevent most (all?) Panorama
         //        weirdness but we will still merge other extData.
@@ -3892,27 +3895,26 @@ SessionStoreService.prototype = {
   },
 
   /**
    * Resize this._closedWindows to the value of the pref, except in the case
    * where we don't have any non-popup windows on Windows and Linux. Then we must
    * resize such that we have at least one non-popup window.
    */
   _capClosedWindows : function sss_capClosedWindows() {
-    let maxWindowsUndo = this._prefBranch.getIntPref("sessionstore.max_windows_undo");
-    if (this._closedWindows.length <= maxWindowsUndo)
+    if (this._closedWindows.length <= this._max_windows_undo)
       return;
-    let spliceTo = maxWindowsUndo;
+    let spliceTo = this._max_windows_undo;
     if (AppConstants.platform != "macosx") {
       let normalWindowIndex = 0;
       // try to find a non-popup window in this._closedWindows
       while (normalWindowIndex < this._closedWindows.length &&
              this._closedWindows[normalWindowIndex].isPopup)
         normalWindowIndex++;
-      if (normalWindowIndex >= maxWindowsUndo)
+      if (normalWindowIndex >= this._max_windows_undo)
         spliceTo = normalWindowIndex + 1;
     }
 
     this._closedWindows.splice(spliceTo, this._closedWindows.length);
   },
 
   /**
    * Reset state to prepare for a new session state to be restored.
