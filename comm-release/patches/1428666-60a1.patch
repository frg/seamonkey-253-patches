# HG changeset patch
# User Gene Smith <gds@chartertn.net>
# Date 1515732618 18000
#      Thu Jan 11 23:50:18 2018 -0500
# Node ID a554d6a6752db5ae72d637347b77caa335630907
# Parent  a60a1d5eef0583badeac0476d1a566aa17b7f80c
Bug 1428666 - Follow-up to bug 1335982: Correct trash folder problems with certain imap servers. r=jorgk

diff --git a/mailnews/imap/src/nsImapProtocol.cpp b/mailnews/imap/src/nsImapProtocol.cpp
--- a/mailnews/imap/src/nsImapProtocol.cpp
+++ b/mailnews/imap/src/nsImapProtocol.cpp
@@ -792,19 +792,19 @@ nsresult nsImapProtocol::SetupWithUrl(ns
     (void) imapServer->GetShuttingDown(&shuttingDown);
     if (!shuttingDown)
       (void) imapServer->GetUseIdle(&m_useIdle);
     else
       m_useIdle = false;
     imapServer->GetFetchByChunks(&m_fetchByChunks);
     imapServer->GetSendID(&m_sendID);
 
-    nsAutoString trashFolderName;
-    if (NS_SUCCEEDED(imapServer->GetTrashFolderName(trashFolderName)))
-      CopyUTF16toMUTF7(trashFolderName, m_trashFolderName);
+    nsAutoString trashFolderPath;
+    if (NS_SUCCEEDED(imapServer->GetTrashFolderName(trashFolderPath)))
+      CopyUTF16toMUTF7(trashFolderPath, m_trashFolderPath);
 
     nsCOMPtr<nsIPrefBranch> prefBranch(do_GetService(NS_PREFSERVICE_CONTRACTID));
     if (prefBranch)
     {
       bool preferPlainText;
       prefBranch->GetBoolPref("mailnews.display.prefer_plaintext", &preferPlainText);
       // If the pref has changed since the last time we ran a url,
       // clear the shell cache for this host.
@@ -5120,37 +5120,32 @@ nsImapProtocol::DiscoverMailboxSpec(nsIm
           {
             m_hostSessionList->GetOnlineTrashFolderExistsForHost(
                                   GetImapServerKey(), onlineTrashFolderExists);
           }
         }
 
         // Don't set the Trash flag if not using the Trash model
         if (GetDeleteIsMoveToTrash() && !onlineTrashFolderExists &&
-            adoptedBoxSpec->mAllocatedPathName.Find(m_trashFolderName, /* ignoreCase = */ true) != -1)
+            adoptedBoxSpec->mAllocatedPathName.Find(m_trashFolderPath, /* ignoreCase = */ true) != -1)
         {
           bool trashExists = false;
-          nsCString trashMatch(CreatePossibleTrashName(nsPrefix));
-          nsCString serverTrashName;
-          m_runningUrl->AllocateCanonicalPath(trashMatch.get(),
-                                              ns->GetDelimiter(),
-                                              getter_Copies(serverTrashName));
-          if (StringBeginsWith(serverTrashName,
+          if (StringBeginsWith(m_trashFolderPath,
                                NS_LITERAL_CSTRING("INBOX/"),
                                nsCaseInsensitiveCStringComparator()))
           {
             nsAutoCString pathName(adoptedBoxSpec->mAllocatedPathName.get() + 6);
             trashExists =
               StringBeginsWith(adoptedBoxSpec->mAllocatedPathName,
-                               serverTrashName,
+                               m_trashFolderPath,
                                nsCaseInsensitiveCStringComparator()) && /* "INBOX/" */
-              pathName.Equals(Substring(serverTrashName, 6), nsCaseInsensitiveCStringComparator());
+              pathName.Equals(Substring(m_trashFolderPath, 6), nsCaseInsensitiveCStringComparator());
           }
           else
-            trashExists = adoptedBoxSpec->mAllocatedPathName.Equals(serverTrashName, nsCaseInsensitiveCStringComparator());
+            trashExists = adoptedBoxSpec->mAllocatedPathName.Equals(m_trashFolderPath, nsCaseInsensitiveCStringComparator());
 
           if (m_hostSessionList)
             m_hostSessionList->SetOnlineTrashFolderExistsForHost(GetImapServerKey(), trashExists);
 
           if (trashExists)
             adoptedBoxSpec->mBoxFlags |= kImapTrash;
         }
       }
@@ -7668,30 +7663,28 @@ void nsImapProtocol::MailboxDiscoveryFin
     bool usingSubscription = false;
     m_hostSessionList->GetOnlineTrashFolderExistsForHost(GetImapServerKey(), trashFolderExists);
     m_hostSessionList->GetHostIsUsingSubscription(GetImapServerKey(),usingSubscription);
     if (!trashFolderExists && GetDeleteIsMoveToTrash() && usingSubscription)
     {
       // maybe we're not subscribed to the Trash folder
       if (personalDir)
       {
-        nsCString originalTrashName(CreatePossibleTrashName(personalDir));
         m_hierarchyNameState = kDiscoverTrashFolderInProgress;
-        List(originalTrashName.get(), true);
+        List(m_trashFolderPath.get(), true);
         m_hierarchyNameState = kNoOperationInProgress;
       }
     }
 
     // There is no Trash folder (either LIST'd or LSUB'd), and we're using the
     // Delete-is-move-to-Trash model, and there is a personal namespace
     if (!trashFolderExists && GetDeleteIsMoveToTrash() && ns)
     {
-      nsCString trashName(CreatePossibleTrashName(ns->GetPrefix()));
       nsCString onlineTrashName;
-      m_runningUrl->AllocateServerPath(trashName.get(), ns->GetDelimiter(),
+      m_runningUrl->AllocateServerPath(m_trashFolderPath.get(), ns->GetDelimiter(),
                                        getter_Copies(onlineTrashName));
 
       GetServerStateParser().SetReportingErrors(false);
       bool created = CreateMailboxRespectingSubscriptions(onlineTrashName.get());
       GetServerStateParser().SetReportingErrors(true);
 
       // force discovery of new trash folder.
       if (created)
@@ -7824,23 +7817,16 @@ void nsImapProtocol::RenameMailbox(const
   command += escapedNewName;
   command += "\"" CRLF;
 
   nsresult rv = SendData(command.get());
   if (NS_SUCCEEDED(rv))
     ParseIMAPandCheckForNewMail();
 }
 
-nsCString nsImapProtocol::CreatePossibleTrashName(const char *prefix)
-{
-  nsCString returnTrash(prefix);
-  returnTrash += m_trashFolderName;
-  return returnTrash;
-}
-
 bool nsImapProtocol::GetListSubscribedIsBrokenOnServer()
 {
   // This is a workaround for an issue with LIST(SUBSCRIBED) crashing older versions of Zimbra
   if (GetServerStateParser().GetServerID().Find("\"NAME\" \"Zimbra\"", /* ignoreCase = */ true) != kNotFound) {
     nsCString serverID(GetServerStateParser().GetServerID());
     int start = serverID.Find("\"VERSION\" \"", /* ignoreCase = */ true) + 11;
     int length = serverID.Find("\" ", true, start);
     const nsDependentCSubstring serverVersionSubstring = Substring(serverID, start, length);
diff --git a/mailnews/imap/src/nsImapProtocol.h b/mailnews/imap/src/nsImapProtocol.h
--- a/mailnews/imap/src/nsImapProtocol.h
+++ b/mailnews/imap/src/nsImapProtocol.h
@@ -335,17 +335,17 @@ private:
   nsCString             m_userName;
   nsCString             m_serverKey;
   nsCString             m_realHostName;
   char                  *m_dataOutputBuf;
   RefPtr<nsMsgLineStreamBuffer> m_inputStreamBuffer;
   uint32_t              m_allocatedSize; // allocated size
   uint32_t        m_totalDataSize; // total data size
   uint32_t        m_curReadIndex;  // current read index
-  nsCString       m_trashFolderName;
+  nsCString       m_trashFolderPath;
 
   // Output stream for writing commands to the socket
   nsCOMPtr<nsISocketTransport>  m_transport;
 
   nsCOMPtr<nsIAsyncInputStream>   m_channelInputStream;
   nsCOMPtr<nsIAsyncOutputStream>  m_channelOutputStream;
   nsCOMPtr<nsIImapMockChannel>    m_mockChannel;   // this is the channel we should forward to people
   uint32_t m_bytesToChannel;
@@ -540,17 +540,16 @@ private:
   void FolderNotCreated(const char *mailboxName);
   // notify the fe that a folder was deleted
   void FolderRenamed(const char *oldName,
     const char *newName);
 
   bool FolderIsSelected(const char *mailboxName);
 
   bool    MailboxIsNoSelectMailbox(const char *mailboxName);
-  nsCString CreatePossibleTrashName(const char *prefix);
   bool FolderNeedsACLInitialized(const char *folderName);
   void DiscoverMailboxList();
   void DiscoverAllAndSubscribedBoxes();
   void MailboxDiscoveryFinished();
   void NthLevelChildList(const char *onlineMailboxPrefix, int32_t depth);
   // LIST SUBSCRIBED command (from RFC 5258) crashes some servers. so we need to
   // identify those servers
   bool GetListSubscribedIsBrokenOnServer();
