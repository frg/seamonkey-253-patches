# HG changeset patch
# User Jorg K <jorgk@jorgk.com>
# Date 1502575365 -7200
# Node ID f305e8b4e790388b0a891c5bcba942a5086f90c8
# Parent  98fe58b72500fa59565023188d144b5dc2daa6bc
Bug 1341211 - Replace nsIFilePicker::Show() with nsIFilePicker::Open() in the address book manager. rs=bustage-fix

diff --git a/mailnews/addrbook/src/nsAbManager.cpp b/mailnews/addrbook/src/nsAbManager.cpp
--- a/mailnews/addrbook/src/nsAbManager.cpp
+++ b/mailnews/addrbook/src/nsAbManager.cpp
@@ -12,17 +12,16 @@
 #include "nsMsgI18N.h"
 #include "nsIStringBundle.h"
 #include "nsMsgUtils.h"
 #include "nsAppDirectoryServiceDefs.h"
 #include "plstr.h"
 #include "prmem.h"
 #include "nsIServiceManager.h"
 #include "mozIDOMWindow.h"
-#include "nsIFilePicker.h"
 #include "plbase64.h"
 #include "nsIWindowWatcher.h"
 #include "nsDirectoryServiceUtils.h"
 #include "nsVCard.h"
 #include "nsVCardObj.h"
 #include "nsIAbLDAPAttributeMap.h"
 #include "nsICommandLine.h"
 #include "nsIFile.h"
@@ -519,16 +518,26 @@ enum ADDRESSBOOK_EXPORT_FILE_TYPE
  CSV_EXPORT_TYPE      = 0,
  CSV_EXPORT_TYPE_UTF8 = 1,
  TAB_EXPORT_TYPE      = 2,
  TAB_EXPORT_TYPE_UTF8 = 3,
  VCF_EXPORT_TYPE      = 4,
  LDIF_EXPORT_TYPE     = 5,
 };
 
+NS_IMPL_ISUPPORTS(nsAbManager::nsFilePickerShownCallback,
+                  nsIFilePickerShownCallback)
+nsAbManager::nsFilePickerShownCallback::nsFilePickerShownCallback(
+  nsAbManager* aAbManager, nsIFilePicker* aFilePicker, nsIAbDirectory *aDirectory)
+  : mFilePicker(aFilePicker)
+  , mAbManager(aAbManager)
+  , mDirectory(aDirectory)
+{
+}
+
 NS_IMETHODIMP nsAbManager::ExportAddressBook(mozIDOMWindowProxy *aParentWin, nsIAbDirectory *aDirectory)
 {
   NS_ENSURE_ARG_POINTER(aParentWin);
 
   nsresult rv;
   nsCOMPtr<nsIFilePicker> filePicker = do_CreateInstance("@mozilla.org/filepicker;1", &rv);
   NS_ENSURE_SUCCESS(rv, rv);
 
@@ -580,40 +589,46 @@ NS_IMETHODIMP nsAbManager::ExportAddress
   rv = filePicker->AppendFilter(filterString, NS_LITERAL_STRING("*.vcf"));
   NS_ENSURE_SUCCESS(rv, rv);
 
   rv = bundle->GetStringFromName("LDIFFiles", filterString);
   NS_ENSURE_SUCCESS(rv, rv);
   rv = filePicker->AppendFilter(filterString, NS_LITERAL_STRING("*.ldi; *.ldif"));
   NS_ENSURE_SUCCESS(rv, rv);
 
-  int16_t dialogResult;
-  filePicker->Show(&dialogResult);
+  nsCOMPtr<nsIFilePickerShownCallback> callback =
+    new nsAbManager::nsFilePickerShownCallback(this, filePicker, aDirectory);
+  return filePicker->Open(callback);
+}
 
-  if (dialogResult == nsIFilePicker::returnCancel)
-    return rv;
+NS_IMETHODIMP
+nsAbManager::nsFilePickerShownCallback::Done(int16_t aResult)
+{
+  nsresult rv;
+  if (aResult == nsIFilePicker::returnCancel)
+    return NS_OK;
 
   nsCOMPtr<nsIFile> localFile;
-  rv = filePicker->GetFile(getter_AddRefs(localFile));
+  rv = mFilePicker->GetFile(getter_AddRefs(localFile));
   NS_ENSURE_SUCCESS(rv, rv);
 
-  if (dialogResult == nsIFilePicker::returnReplace) {
+  if (aResult == nsIFilePicker::returnReplace) {
     // be extra safe and only delete when the file is really a file
     bool isFile;
     rv = localFile->IsFile(&isFile);
     if (NS_SUCCEEDED(rv) && isFile) {
       rv = localFile->Remove(false /* recursive delete */);
       NS_ENSURE_SUCCESS(rv, rv);
     }
   }
 
   // The type of export is determined by the drop-down in
   // the file picker dialog.
   int32_t exportType;
-  rv = filePicker->GetFilterIndex(&exportType);
+  rv = mFilePicker->GetFilterIndex(&exportType);
   NS_ENSURE_SUCCESS(rv,rv);
 
   nsAutoString fileName;
   rv = localFile->GetLeafName(fileName);
   NS_ENSURE_SUCCESS(rv, rv);
 
   switch ( exportType )
   {
@@ -622,55 +637,57 @@ NS_IMETHODIMP nsAbManager::ExportAddress
       // If filename does not have the correct ext, add one.
       if ((MsgFind(fileName, LDIF_FILE_EXTENSION, true, fileName.Length() - strlen(LDIF_FILE_EXTENSION)) == -1) &&
           (MsgFind(fileName, LDIF_FILE_EXTENSION2, true, fileName.Length() - strlen(LDIF_FILE_EXTENSION2)) == -1)) {
 
         // Add the extension and build a new localFile.
         fileName.AppendLiteral(LDIF_FILE_EXTENSION2);
         localFile->SetLeafName(fileName);
       }
-      rv = ExportDirectoryToLDIF(aDirectory, localFile);
+      rv = mAbManager->ExportDirectoryToLDIF(mDirectory, localFile);
       break;
 
     case CSV_EXPORT_TYPE: // csv
     case CSV_EXPORT_TYPE_UTF8:
       // If filename does not have the correct ext, add one.
       if (MsgFind(fileName, CSV_FILE_EXTENSION, true, fileName.Length() - strlen(CSV_FILE_EXTENSION)) == -1) {
 
         // Add the extension and build a new localFile.
         fileName.AppendLiteral(CSV_FILE_EXTENSION);
         localFile->SetLeafName(fileName);
       }
-      rv = ExportDirectoryToDelimitedText(aDirectory, CSV_DELIM, CSV_DELIM_LEN, localFile,
+      rv = mAbManager->ExportDirectoryToDelimitedText(
+                                          mDirectory, CSV_DELIM, CSV_DELIM_LEN, localFile,
                                           exportType==CSV_EXPORT_TYPE_UTF8);
       break;
 
     case TAB_EXPORT_TYPE: // tab & text
     case TAB_EXPORT_TYPE_UTF8:
       // If filename does not have the correct ext, add one.
       if ((MsgFind(fileName, TXT_FILE_EXTENSION, true, fileName.Length() - strlen(TXT_FILE_EXTENSION)) == -1) &&
           (MsgFind(fileName, TAB_FILE_EXTENSION, true, fileName.Length() - strlen(TAB_FILE_EXTENSION)) == -1)) {
 
         // Add the extension and build a new localFile.
         fileName.AppendLiteral(TXT_FILE_EXTENSION);
         localFile->SetLeafName(fileName);
       }
-      rv = ExportDirectoryToDelimitedText(aDirectory, TAB_DELIM, TAB_DELIM_LEN, localFile,
+      rv = mAbManager->ExportDirectoryToDelimitedText(
+                                          mDirectory, TAB_DELIM, TAB_DELIM_LEN, localFile,
                                           exportType==TAB_EXPORT_TYPE_UTF8);
       break;
 
     case VCF_EXPORT_TYPE: // vCard
       // If filename does not have the correct ext, add one.
       if (MsgFind(fileName, VCF_FILE_EXTENSION, true, fileName.Length() - strlen(VCF_FILE_EXTENSION)) == -1) {
 
         // Add the extension and build a new localFile.
         fileName.AppendLiteral(VCF_FILE_EXTENSION);
         localFile->SetLeafName(fileName);
       }
-      rv = ExportDirectoryToVCard(aDirectory, localFile);
+      rv = mAbManager->ExportDirectoryToVCard(mDirectory, localFile);
       break;
   };
 
   return rv;
 }
 
 nsresult
 nsAbManager::ExportDirectoryToDelimitedText(nsIAbDirectory *aDirectory,
diff --git a/mailnews/addrbook/src/nsAbManager.h b/mailnews/addrbook/src/nsAbManager.h
--- a/mailnews/addrbook/src/nsAbManager.h
+++ b/mailnews/addrbook/src/nsAbManager.h
@@ -9,16 +9,17 @@
 #include "nsIAbManager.h"
 #include "nsTObserverArray.h"
 #include "nsCOMPtr.h"
 #include "nsICommandLineHandler.h"
 #include "nsIObserver.h"
 #include "nsInterfaceHashtable.h"
 #include "nsIAbDirFactoryService.h"
 #include "nsIAbDirectory.h"
+#include "nsIFilePicker.h"
 
 class nsIAbLDAPAttributeMap;
 
 class nsAbManager : public nsIAbManager,
                     public nsICommandLineHandler,
                     public nsIObserver
 {
 
@@ -58,14 +59,34 @@ private:
     int operator==(nsIAbListener* aListener) const {
       return mListener == aListener;
     }
     int operator==(const abListener &aListener) const {
       return mListener == aListener.mListener;
     }
   };
 
+  class nsFilePickerShownCallback
+    : public nsIFilePickerShownCallback
+  {
+    virtual ~nsFilePickerShownCallback()
+    { }
+
+  public:
+    nsFilePickerShownCallback(nsAbManager* aInput,
+                              nsIFilePicker* aFilePicker,
+                              nsIAbDirectory *aDirectory);
+    NS_DECL_ISUPPORTS
+
+    NS_IMETHOD Done(int16_t aResult) override;
+
+  private:
+    nsCOMPtr<nsIFilePicker> mFilePicker;
+    RefPtr<nsAbManager> mAbManager;
+    RefPtr<nsIAbDirectory> mDirectory;
+  };
+
   nsTObserverArray<abListener> mListeners;
   nsCOMPtr<nsIAbDirectory> mCacheTopLevelAb;
   nsInterfaceHashtable<nsCStringHashKey, nsIAbDirectory> mAbStore;
 };
 
 #endif
