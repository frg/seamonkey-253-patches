# HG changeset patch
# User Philipp Kewisch <mozilla@kewis.ch>
# Date 1517693106 -3600
#      Sa Feb 03 22:25:06 2018 +0100
# Node ID e2c686325a54c7ac2d0546085784392d2cea47e0
# Parent  8f2d87d28965471bb4f99bf2e4b17606c1e0e080
Bug 1436490 - Move more data related functions into calDataUtils.jsm  - manual changes. r=MakeMyDay

MozReview-Commit-ID: 90LDD5i8MTq

diff --git a/calendar/base/modules/calDataUtils.jsm b/calendar/base/modules/calDataUtils.jsm
--- a/calendar/base/modules/calDataUtils.jsm
+++ b/calendar/base/modules/calDataUtils.jsm
@@ -89,10 +89,146 @@ class ObserverSet extends ListenerSet {
         }
         super.add(item);
     }
 }
 
 var caldata = {
     ListenerSet: ListenerSet,
     ObserverSet: ObserverSet,
-    PropertyMap: PropertyMap
+    PropertyMap: PropertyMap,
+
+    /**
+     * Use the binary search algorithm to search for an item in an array.
+     * function.
+     *
+     * The comptor function may look as follows for calIDateTime objects.
+     *     function comptor(a, b) {
+     *         return a.compare(b);
+     *     }
+     * If no comptor is specified, the default greater-than comptor will be used.
+     *
+     * @param itemArray             The array to search.
+     * @param newItem               The item to search in the array.
+     * @param comptor               A comparation function that can compare two items.
+     * @return                      The index of the new item.
+     */
+    binarySearch: function(itemArray, newItem, comptor) {
+        function binarySearchInternal(low, high) {
+            // Are we done yet?
+            if (low == high) {
+                return low + (comptor(newItem, itemArray[low]) < 0 ? 0 : 1);
+            }
+
+            let mid = Math.floor(low + ((high - low) / 2));
+            let cmp = comptor(newItem, itemArray[mid]);
+            if (cmp > 0) {
+                return binarySearchInternal(mid + 1, high);
+            } else if (cmp < 0) {
+                return binarySearchInternal(low, mid);
+            } else {
+                return mid;
+            }
+        }
+
+        if (itemArray.length < 1) {
+            return -1;
+        }
+        if (!comptor) {
+            comptor = function(a, b) {
+                return (a > b) - (a < b);
+            };
+        }
+        return binarySearchInternal(0, itemArray.length - 1);
+    },
+
+    /**
+     * Insert a new node underneath the given parentNode, using binary search. See binarySearch
+     * for a note on how the comptor works.
+     *
+     * @param parentNode           The parent node underneath the new node should be inserted.
+     * @param inserNode            The node to insert
+     * @param aItem                The calendar item to add a widget for.
+     * @param comptor              A comparison function that can compare two items (not DOM Nodes!)
+     * @param discardDuplicates    Use the comptor function to check if the item in
+     *                               question is already in the array. If so, the
+     *                               new item is not inserted.
+     * @param itemAccessor         [optional] A function that receives a DOM node and returns the associated item
+     *                               If null, this function will be used: function(n) n.item
+     */
+    binaryInsertNode: function(parentNode, insertNode, aItem, comptor, discardDuplicates, itemAccessor) {
+        let accessor = itemAccessor || caldata.binaryInsertNodeDefaultAccessor;
+
+        // Get the index of the node before which the inserNode will be inserted
+        let newIndex = caldata.binarySearch(Array.from(parentNode.childNodes, accessor), aItem, comptor);
+
+        if (newIndex < 0) {
+            parentNode.appendChild(insertNode);
+            newIndex = 0;
+        } else if (!discardDuplicates ||
+            comptor(accessor(parentNode.childNodes[Math.min(newIndex, parentNode.childNodes.length - 1)]), aItem) >= 0) {
+            // Only add the node if duplicates should not be discarded, or if
+            // they should and the childNode[newIndex] == node.
+            let node = parentNode.childNodes[newIndex];
+            parentNode.insertBefore(insertNode, node);
+        }
+        return newIndex;
+    },
+    binaryInsertNodeDefaultAccessor: n => n.item,
+
+    /**
+     * Insert an item into the given array, using binary search. See binarySearch
+     * for a note on how the comptor works.
+     *
+     * @param itemArray             The array to insert into.
+     * @param item                  The item to insert into the array.
+     * @param comptor               A comparation function that can compare two items.
+     * @param discardDuplicates     Use the comptor function to check if the item in
+     *                                question is already in the array. If so, the
+     *                                new item is not inserted.
+     * @return                      The index of the new item.
+     */
+    binaryInsert: function(itemArray, item, comptor, discardDuplicates) {
+        let newIndex = caldata.binarySearch(itemArray, item, comptor);
+
+        if (newIndex < 0) {
+            itemArray.push(item);
+            newIndex = 0;
+        } else if (!discardDuplicates ||
+                    comptor(itemArray[Math.min(newIndex, itemArray.length - 1)], item) != 0) {
+            // Only add the item if duplicates should not be discarded, or if
+            // they should and itemArray[newIndex] != item.
+            itemArray.splice(newIndex, 0, item);
+        }
+        return newIndex;
+    },
+
+    /**
+     * Generic object comparer
+     * Use to compare two objects which are not of type calIItemBase, in order
+     * to avoid the js-wrapping issues mentioned above.
+     *
+     * @param aObject        first object to be compared
+     * @param aOtherObject   second object to be compared
+     * @param aIID           IID to use in comparison, undefined/null defaults to nsISupports
+     */
+    compareObjects: function(aObject, aOtherObject, aIID) {
+        // xxx todo: seems to work fine e.g. for WCAP, but I still mistrust this trickery...
+        //           Anybody knows an official API that could be used for this purpose?
+        //           For what reason do clients need to pass aIID since
+        //           every XPCOM object has to implement nsISupports?
+        //           XPCOM (like COM, like UNO, ...) defines that QueryInterface *only* needs to return
+        //           the very same pointer for nsISupports during its lifetime.
+        if (!aIID) {
+            aIID = Components.interfaces.nsISupports;
+        }
+        let sip1 = Components.classes["@mozilla.org/supports-interface-pointer;1"]
+                             .createInstance(Components.interfaces.nsISupportsInterfacePointer);
+        sip1.data = aObject;
+        sip1.dataIID = aIID;
+
+        let sip2 = Components.classes["@mozilla.org/supports-interface-pointer;1"]
+                             .createInstance(Components.interfaces.nsISupportsInterfacePointer);
+        sip2.data = aOtherObject;
+        sip2.dataIID = aIID;
+        return sip1.data == sip2.data;
+    }
 };
diff --git a/calendar/base/modules/calUtilsCompat.jsm b/calendar/base/modules/calUtilsCompat.jsm
--- a/calendar/base/modules/calUtilsCompat.jsm
+++ b/calendar/base/modules/calUtilsCompat.jsm
@@ -18,16 +18,22 @@ this.EXPORTED_SYMBOLS = ["injectCalUtils
  */
 var migrations = {
     acl: {
         isCalendarWritable: "isCalendarWritable",
         userCanAddItemsToCalendar: "userCanAddItemsToCalendar",
         userCanDeleteItemsFromCalendar: "userCanDeleteItemsFromCalendar",
         userCanModifyItem: "userCanModifyItem"
     },
+    data: {
+        binarySearch: "binarySearch",
+        binaryInsertNode: "binaryInsertNode",
+        binaryInsert: "binaryInsert",
+        compareObjects: "compareObjects"
+    },
     dtz: {
         now: "now",
         ensureDateTime: "ensureDateTime",
         getRecentTimezones: "getRecentTimezones",
         saveRecentTimezone: "saveRecentTimezone",
         getDefaultStartDate: "getDefaultStartDate",
         setDefaultStartEndHour: "setDefaultStartEndHour",
         calGetStartDateProp: "startDateProp",
diff --git a/calendar/base/src/calUtils.js b/calendar/base/src/calUtils.js
--- a/calendar/base/src/calUtils.js
+++ b/calendar/base/src/calUtils.js
@@ -4,22 +4,21 @@
 
 /* This file contains commonly used functions in a centralized place so that
  * various components (and other js scopes) don't need to replicate them. Note
  * that loading this file twice in the same scope will throw errors.
  */
 
 /* exported getCalendarDirectory, attendeeMatchesAddresses,
  *          calRadioGroupSelectItem, getPrefCategoriesArray,
- *          setPrefCategoriesFromArray, calTryWrappedJSObject, compareArrays,
- *          compareObjects, LOG, WARN, ERROR, showError,
- *          getContrastingTextColor, sendMailTo, applyAttributeToMenuChildren,
- *          isPropertyValueSame, getParentNodeOrThis,
- *          getParentNodeOrThisByAttribute, calIterateEmailIdentities,
- *          binaryInsert, getCompositeCalendar
+ *          setPrefCategoriesFromArray, calTryWrappedJSObject, LOG, WARN,
+ *          ERROR, showError, getContrastingTextColor, sendMailTo,
+ *          applyAttributeToMenuChildren, isPropertyValueSame,
+ *          getParentNodeOrThis, getParentNodeOrThisByAttribute,
+ *          calIterateEmailIdentities, getCompositeCalendar
  */
 
 ChromeUtils.import("resource:///modules/mailServices.js");
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/Preferences.jsm");
 ChromeUtils.import("resource://gre/modules/AppConstants.jsm");
 
@@ -207,69 +206,16 @@ function getUUID() {
  */
 function calTryWrappedJSObject(obj) {
     if (obj && obj.wrappedJSObject) {
         obj = obj.wrappedJSObject;
     }
     return obj;
 }
 
-/**
- * Generic object comparer
- * Use to compare two objects which are not of type calIItemBase, in order
- * to avoid the js-wrapping issues mentioned above.
- *
- * @param aObject        first object to be compared
- * @param aOtherObject   second object to be compared
- * @param aIID           IID to use in comparison, undefined/null defaults to nsISupports
- */
-function compareObjects(aObject, aOtherObject, aIID) {
-    // xxx todo: seems to work fine e.g. for WCAP, but I still mistrust this trickery...
-    //           Anybody knows an official API that could be used for this purpose?
-    //           For what reason do clients need to pass aIID since
-    //           every XPCOM object has to implement nsISupports?
-    //           XPCOM (like COM, like UNO, ...) defines that QueryInterface *only* needs to return
-    //           the very same pointer for nsISupports during its lifetime.
-    if (!aIID) {
-        aIID = Components.interfaces.nsISupports;
-    }
-    let sip1 = Components.classes["@mozilla.org/supports-interface-pointer;1"]
-                         .createInstance(Components.interfaces.nsISupportsInterfacePointer);
-    sip1.data = aObject;
-    sip1.dataIID = aIID;
-
-    let sip2 = Components.classes["@mozilla.org/supports-interface-pointer;1"]
-                         .createInstance(Components.interfaces.nsISupportsInterfacePointer);
-    sip2.data = aOtherObject;
-    sip2.dataIID = aIID;
-    return sip1.data == sip2.data;
-}
-
-/**
- * Compare two arrays using the passed function.
- */
-function compareArrays(aOne, aTwo, compareFunc) {
-    if (!aOne && !aTwo) {
-        return true;
-    }
-    if (!aOne || !aTwo) {
-        return false;
-    }
-    let len = aOne.length;
-    if (len != aTwo.length) {
-        return false;
-    }
-    for (let i = 0; i < len; ++i) {
-        if (!compareFunc(aOne[i], aTwo[i])) {
-            return false;
-        }
-    }
-    return true;
-}
-
 
 /**
  * Helper used in the following log functions to actually log the message.
  * Should not be used outside of this file.
  */
 function _log(message, flag) {
     let frame = Components.stack.caller.caller;
     let filename = frame.filename ? frame.filename.split(" -> ").pop() : null;
@@ -536,113 +482,8 @@ function calIterateEmailIdentities(func)
         for (let j = 0; j < identities.length; ++j) {
             let identity = identities.queryElementAt(j, Components.interfaces.nsIMsgIdentity);
             if (!func(identity, account)) {
                 break;
             }
         }
     }
 }
-
-/**
- * Use the binary search algorithm to search for an item in an array.
- * function.
- *
- * The comptor function may look as follows for calIDateTime objects.
- *     function comptor(a, b) {
- *         return a.compare(b);
- *     }
- * If no comptor is specified, the default greater-than comptor will be used.
- *
- * @param itemArray             The array to search.
- * @param newItem               The item to search in the array.
- * @param comptor               A comparation function that can compare two items.
- * @return                      The index of the new item.
- */
-function binarySearch(itemArray, newItem, comptor) {
-    function binarySearchInternal(low, high) {
-        // Are we done yet?
-        if (low == high) {
-            return low + (comptor(newItem, itemArray[low]) < 0 ? 0 : 1);
-        }
-
-        let mid = Math.floor(low + ((high - low) / 2));
-        let cmp = comptor(newItem, itemArray[mid]);
-        if (cmp > 0) {
-            return binarySearchInternal(mid + 1, high);
-        } else if (cmp < 0) {
-            return binarySearchInternal(low, mid);
-        } else {
-            return mid;
-        }
-    }
-
-    if (itemArray.length < 1) {
-        return -1;
-    }
-    if (!comptor) {
-        comptor = function(a, b) {
-            return (a > b) - (a < b);
-        };
-    }
-    return binarySearchInternal(0, itemArray.length - 1);
-}
-
-/**
- * Insert a new node underneath the given parentNode, using binary search. See binarySearch
- * for a note on how the comptor works.
- *
- * @param parentNode           The parent node underneath the new node should be inserted.
- * @param inserNode            The node to insert
- * @param aItem                The calendar item to add a widget for.
- * @param comptor              A comparison function that can compare two items (not DOM Nodes!)
- * @param discardDuplicates    Use the comptor function to check if the item in
- *                               question is already in the array. If so, the
- *                               new item is not inserted.
- * @param itemAccessor         [optional] A function that receives a DOM node and returns the associated item
- *                               If null, this function will be used: function(n) n.item
- */
-function binaryInsertNode(parentNode, insertNode, aItem, comptor, discardDuplicates, itemAccessor) {
-    let accessor = itemAccessor || binaryInsertNode.defaultAccessor;
-
-    // Get the index of the node before which the inserNode will be inserted
-    let newIndex = binarySearch(Array.from(parentNode.childNodes, accessor), aItem, comptor);
-
-    if (newIndex < 0) {
-        parentNode.appendChild(insertNode);
-        newIndex = 0;
-    } else if (!discardDuplicates ||
-        comptor(accessor(parentNode.childNodes[Math.min(newIndex, parentNode.childNodes.length - 1)]), aItem) >= 0) {
-        // Only add the node if duplicates should not be discarded, or if
-        // they should and the childNode[newIndex] == node.
-        let node = parentNode.childNodes[newIndex];
-        parentNode.insertBefore(insertNode, node);
-    }
-    return newIndex;
-}
-binaryInsertNode.defaultAccessor = n => n.item;
-
-/**
- * Insert an item into the given array, using binary search. See binarySearch
- * for a note on how the comptor works.
- *
- * @param itemArray             The array to insert into.
- * @param item                  The item to insert into the array.
- * @param comptor               A comparation function that can compare two items.
- * @param discardDuplicates     Use the comptor function to check if the item in
- *                                question is already in the array. If so, the
- *                                new item is not inserted.
- * @return                      The index of the new item.
- */
-function binaryInsert(itemArray, item, comptor, discardDuplicates) {
-    let newIndex = binarySearch(itemArray, item, comptor);
-
-    if (newIndex < 0) {
-        itemArray.push(item);
-        newIndex = 0;
-    } else if (!discardDuplicates ||
-                comptor(itemArray[Math.min(newIndex, itemArray.length - 1)], item) != 0) {
-        // Only add the item if duplicates should not be discarded, or if
-        // they should and itemArray[newIndex] != item.
-        itemArray.splice(newIndex, 0, item);
-    }
-    return newIndex;
-}
