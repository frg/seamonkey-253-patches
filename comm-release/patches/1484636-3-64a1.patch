# HG changeset patch
# User Geoff Lankow <geoff@darktrojan.net>
# Date 1538099486 -43200
# Node ID d796a9fbbe9a6f8d8da2b3a98b7f8af56a9edbf4
# Parent  7c36162143ad927c9f59c09ba4099b811162e4dc
Bug 1484636 - Improve reliability of testEventDialog.js; rs=bustage-fix DONTBUILD

diff --git a/calendar/test/mozmill/eventDialog/testEventDialog.js b/calendar/test/mozmill/eventDialog/testEventDialog.js
--- a/calendar/test/mozmill/eventDialog/testEventDialog.js
+++ b/calendar/test/mozmill/eventDialog/testEventDialog.js
@@ -6,17 +6,17 @@ var RELATIVE_ROOT = "../shared-modules";
 var MODULE_REQUIRES = ["calendar-utils", "window-helpers"];
 
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://calendar/modules/calUtils.jsm");
 
 var plan_for_modal_dialog, wait_for_modal_dialog;
 var helpersForController, invokeEventDialog, createCalendar, deleteCalendars;
 var handleAddingAttachment, handleOccurrencePrompt;
-var goToDate, setData;
+var goToDate, setData, lookupEventBox;
 var CALENDARNAME, TIMEOUT_MODAL_DIALOG;
 
 var eventTitle = "Event";
 var eventLocation = "Location";
 var eventDescription = "Event Description";
 var eventAttendee = "foo@bar.com";
 var eventUrl = "http://mozilla.org";
 
@@ -28,16 +28,17 @@ function setupModule(module) {
         helpersForController,
         invokeEventDialog,
         createCalendar,
         deleteCalendars,
         handleAddingAttachment,
         handleOccurrencePrompt,
         goToDate,
         setData,
+        lookupEventBox,
         CALENDARNAME,
         TIMEOUT_MODAL_DIALOG
     } = collector.getModule("calendar-utils"));
     collector.getModule("calendar-utils").setupModule();
     Object.assign(module, helpersForController(controller));
 
     createCalendar(controller, CALENDARNAME);
 }
@@ -153,43 +154,41 @@ function testEventDialog() {
             id("attachment-link")/[0]/{"value":"mozilla.org"}
         `));
 
         // save
         event.click(eventid("button-saveandclose"));
     });
 
     // catch and dismiss alarm
-    controller.waitFor(() => mozmill.utils.getWindows("Calendar:AlarmWindow").length > 0);
-    let alarm = new mozmill.controller.MozMillController(mozmill.utils.getWindows("Calendar:AlarmWindow")[0]);
-    let { lookup: alarmlookup } = helpersForController(alarm);
-
-    // dismiss all button, label in .dtd file, bug #504635
-    alarm.waitThenClick(alarmlookup('/id("calendar-alarm-dialog")/id("alarm-actionbar")/[1]'));
-    controller.waitFor(() => mozmill.utils.getWindows("Calendar:AlarmWindow").length == 0);
+    plan_for_modal_dialog("Calendar:AlarmWindow", alarm => {
+        let { lookup: alarmlookup } = helpersForController(alarm);
+        alarm.waitThenClick(alarmlookup('/id("calendar-alarm-dialog")/id("alarm-actionbar")/[1]'));
+    });
+    wait_for_modal_dialog("Calendar:AlarmWindow");
 
     // verify event and alarm icon visible every day of the month and check tooltip
     // 1st January is Thursday so there's three days to check in the first row
     controller.assertNode(lookup(
         eventBox.replace("rowNumber", "0").replace("columnNumber", "4")
     ));
     checkIcon(eventBox, "0", "4");
-    checkTooltip(monthView, "0", "4", "1", startTime, endTime);
+    checkTooltip(monthView, 0, 4, 1, startTime, endTime);
 
     controller.assertNode(lookup(
         eventBox.replace("rowNumber", "0").replace("columnNumber", "5")
     ));
     checkIcon(eventBox, "0", "5");
-    checkTooltip(monthView, "0", "5", "2", startTime, endTime);
+    checkTooltip(monthView, 0, 5, 2, startTime, endTime);
 
     controller.assertNode(lookup(
         eventBox.replace("rowNumber", "0").replace("columnNumber", "6")
     ));
     checkIcon(eventBox, "0", "6");
-    checkTooltip(monthView, "0", "6", "3", startTime, endTime);
+    checkTooltip(monthView, 0, 6, 3, startTime, endTime);
 
     // 31st of January is Saturday so there's four more full rows to check
     let date = 4;
     for (let row = 1; row < 5; row++) {
         for (let col = 0; col < 7; col++) {
             controller.assertNode(lookup(
                 eventBox.replace("rowNumber", row).replace("columnNumber", col)
             ));
@@ -273,41 +272,38 @@ function checkIcon(eventBox, row, col) {
         anon({"align": "center"})/anon({"class":"alarm-icons-box"})/
         anon({"class": "reminder-icon"})
     `).replace("rowNumber", row).replace("columnNumber", col));
 
     controller.assertJS(icon.getNode().getAttribute("value") == "DISPLAY");
 }
 
 function checkTooltip(monthView, row, col, date, startTime, endTime) {
-    controller.mouseOver(lookup(`
-        ${monthView}/anon({"anonid":"mainbox"})/anon({"anonid":"monthgrid"})/
-        anon({"anonid":"monthgridrows"})/[${row}]/[${col}]/
-        {"tooltip":"itemTooltip","calendar":"${CALENDARNAME.toLowerCase()}"}
-    `));
+    let item = lookupEventBox(
+        "month", null, row + 1, col + 1, null,
+        `/{"tooltip":"itemTooltip","calendar":"${CALENDARNAME.toLowerCase()}"}`
+    );
+
+    let toolTip = '/id("messengerWindow")/id("calendar-popupset")/id("itemTooltip")';
+    let toolTipNode = lookup(toolTip).getNode();
+    toolTipNode.ownerGlobal.onMouseOverItem({ currentTarget: item.getNode() });
 
     // check title
-    let eventName = lookup(`
-        /id("messengerWindow")/id("calendar-popupset")/id("itemTooltip")/
-        {"class":"tooltipBox"}/{"class":"tooltipHeaderGrid"}/[1]/[0]/[1]
-    `);
-    controller.waitFor(() => eventName.getNode().textContent == eventTitle);
+    let toolTipGrid = toolTip + '/{"class":"tooltipBox"}/{"class":"tooltipHeaderGrid"}/';
+    let eventName = lookup(`${toolTipGrid}/[1]/[0]/[1]`);
+    controller.assert(() => eventName.getNode().textContent == eventTitle);
 
     // check date and time
-    let dateTime = lookup(`
-        /id("messengerWindow")/id("calendar-popupset")/id("itemTooltip")/
-        {"class":"tooltipBox"}/{"class":"tooltipHeaderGrid"}/[1]/[2]/[1]
-    `);
+    let dateTime = lookup(`${toolTipGrid}/[1]/[2]/[1]`);
 
     let formatter = new Services.intl.DateTimeFormat(undefined, { dateStyle: "full" });
     let startDate = formatter.format(new Date(2009, 0, date));
 
-    controller.waitFor(() => {
+    controller.assert(() => {
         let text = dateTime.getNode().textContent;
-        dump(`${text}\n`);
         return text.includes(`${startDate} ${startTime} – `);
     });
 
     // This could be on the next day if it is 00:00.
     controller.assert(() => dateTime.getNode().textContent.endsWith(endTime));
 }
 
 function teardownTest(module) {
