# HG changeset patch
# User Gene Smith <gds@chartertn.net>
# Date 1523595972 14400
# Node ID 316b36c77becf89be55fb67c81419ff8812ef51d
# Parent  481ec40196e3e0385c32ede5ebbaf6e6e4971dbb
Bug 1430480 - Fix missing message parts when messages copied manually or filtered. r=jorgk
Preset the nsMsgMessageFlags::Offline message flag when a copying from
a file to offline storage or when a copying between folders of the same server.
Otherwise, don't preset the nsMsgMessageFlag::Offline.

diff --git a/mailnews/imap/src/nsImapMailFolder.cpp b/mailnews/imap/src/nsImapMailFolder.cpp
--- a/mailnews/imap/src/nsImapMailFolder.cpp
+++ b/mailnews/imap/src/nsImapMailFolder.cpp
@@ -3876,17 +3876,18 @@ nsImapMailFolder::ReplayOfflineMoveCopy(
                   if (fakeDestHdr)
                     messages->AppendElement(fakeDestHdr);
                   break;
                 }
               }
             }
           }
         }
-        destImapFolder->SetPendingAttributes(messages, isMove);
+        // 3rd parameter: Set offline flag.
+        destImapFolder->SetPendingAttributes(messages, isMove, true);
       }
     }
     // if we can't get the dst folder db, we should still try to playback
     // the offline move/copy.
   }
 
   nsCOMPtr<nsIImapService> imapService = do_GetService(NS_IMAPSERVICE_CONTRACTID, &rv);
   NS_ENSURE_SUCCESS(rv, rv);
@@ -7427,17 +7428,18 @@ nsresult nsImapMailFolder::CopyMessagesO
 
   if (isMove)
     srcFolder->NotifyFolderEvent(NS_SUCCEEDED(rv) ?
                                  mDeleteOrMoveMsgCompletedAtom :
                                  mDeleteOrMoveMsgFailedAtom);
   return rv;
 }
 
-void nsImapMailFolder::SetPendingAttributes(nsIArray* messages, bool aIsMove)
+void nsImapMailFolder::SetPendingAttributes(nsIArray* messages, bool aIsMove,
+                                            bool aSetOffline)
 {
 
   GetDatabase();
   if (!mDatabase)
     return;
 
   uint32_t supportedUserFlags;
   GetSupportedUserFlags(&supportedUserFlags);
@@ -7528,18 +7530,22 @@ void nsImapMailFolder::SetPendingAttribu
       msgDBHdr->GetOfflineMessageSize(&messageSize);
       msgDBHdr->GetStringProperty("storeToken", getter_Copies(storeToken));
       if (messageSize)
       {
         mDatabase->SetUint32AttributeOnPendingHdr(msgDBHdr, "offlineMsgSize",
                                                   messageSize);
         mDatabase->SetUint64AttributeOnPendingHdr(msgDBHdr, "msgOffset",
                                                   messageOffset);
-        mDatabase->SetUint32AttributeOnPendingHdr(msgDBHdr, "flags",
-                                                  nsMsgMessageFlags::Offline);
+        // Not always setting "flags" attribute to nsMsgMessageFlags::Offline
+        // here because it can cause missing parts (inline or attachments)
+        // when messages are moved or copied manually or by filter action.
+        if (aSetOffline)
+          mDatabase->SetUint32AttributeOnPendingHdr(msgDBHdr, "flags",
+                                                    nsMsgMessageFlags::Offline);
         mDatabase->SetAttributeOnPendingHdr(msgDBHdr, "storeToken",
                                             storeToken.get());
       }
       nsMsgPriorityValue priority;
       msgDBHdr->GetPriority(&priority);
       if(priority != 0)
       {
         nsAutoCString priorityStr;
@@ -7650,17 +7656,18 @@ nsImapMailFolder::CopyMessages(nsIMsgFol
     NS_ENSURE_SUCCESS(rv, rv);
 
     if (WeAreOffline())
       return CopyMessagesOffline(srcFolder, sortedMsgs, isMove, msgWindow, listener);
 
     nsCOMPtr<nsIImapService> imapService = do_GetService(NS_IMAPSERVICE_CONTRACTID, &rv);
     NS_ENSURE_SUCCESS(rv,rv);
 
-    SetPendingAttributes(sortedMsgs, isMove);
+    // 3rd parameter: Do not set offline flag.
+    SetPendingAttributes(sortedMsgs, isMove, false);
 
     // if the folders aren't on the same server, do a stream base copy
     if (!sameServer)
     {
       rv = CopyMessagesWithStream(srcFolder, sortedMsgs, isMove, true, msgWindow, listener, allowUndo);
       goto done;
     }
 
@@ -8097,17 +8104,17 @@ nsImapMailFolder::CopyFileMessage(nsIFil
           messageId.AppendInt((int32_t) key);
           // Perhaps we have the message offline, but even if we do it is
           // not valid, since the only time we do a file copy for an
           // existing message is when we are changing the message.
           // So set the offline size to 0 to force SetPendingAttributes to
           // clear the offline message flag.
           msgToReplace->SetOfflineMessageSize(0);
           messages->AppendElement(msgToReplace);
-          SetPendingAttributes(messages, false);
+          SetPendingAttributes(messages, false, false);
         }
     }
 
     bool isMove = (msgToReplace ? true : false);
     rv = InitCopyState(srcSupport, messages, isMove, isDraftOrTemplate,
                        false, aNewMsgFlags, aNewMsgKeywords, listener,
                        msgWindow, false);
     if (NS_FAILED(rv))
@@ -8437,17 +8444,19 @@ nsImapMailFolder::CopyFileToOfflineStore
       if (msgStore)
         msgStore->FinishNewMessage(offlineStore, fakeHdr);
     }
 
     nsCOMPtr<nsIMutableArray> messages(do_CreateInstance(NS_ARRAY_CONTRACTID, &rv));
     NS_ENSURE_SUCCESS(rv, rv);
     messages->AppendElement(fakeHdr);
 
-    SetPendingAttributes(messages, false);
+    // We are copying from a file to offline store so set offline flag.
+    SetPendingAttributes(messages, false, true);
+
     // Gloda needs this notification to index the fake message.
     nsCOMPtr<nsIMsgFolderNotificationService>
       notifier(do_GetService(NS_MSGNOTIFICATIONSERVICE_CONTRACTID));
     if (notifier)
       notifier->NotifyMsgsClassified(messages, false, false);
     inputStream->Close();
     inputStream = nullptr;
     delete inputStreamBuffer;
diff --git a/mailnews/imap/src/nsImapMailFolder.h b/mailnews/imap/src/nsImapMailFolder.h
--- a/mailnews/imap/src/nsImapMailFolder.h
+++ b/mailnews/imap/src/nsImapMailFolder.h
@@ -430,17 +430,17 @@ protected:
   // offline-ish methods
   nsresult GetClearedOriginalOp(nsIMsgOfflineImapOperation *op, nsIMsgOfflineImapOperation **originalOp, nsIMsgDatabase **originalDB);
   nsresult GetOriginalOp(nsIMsgOfflineImapOperation *op, nsIMsgOfflineImapOperation **originalOp, nsIMsgDatabase **originalDB);
   nsresult CopyMessagesOffline(nsIMsgFolder* srcFolder,
                                 nsIArray* messages,
                                 bool isMove,
                                 nsIMsgWindow *msgWindow,
                                 nsIMsgCopyServiceListener* listener);
-  void SetPendingAttributes(nsIArray* messages, bool aIsMove);
+  void SetPendingAttributes(nsIArray* messages, bool aIsMove, bool aSetOffline);
 
   nsresult CopyOfflineMsgBody(nsIMsgFolder *srcFolder, nsIMsgDBHdr *destHdr,
                               nsIMsgDBHdr *origHdr, nsIInputStream *inputStream,
                               nsIOutputStream *outputStream);
 
   void GetTrashFolderName(nsAString &aFolderName);
   bool ShowPreviewText();
 
