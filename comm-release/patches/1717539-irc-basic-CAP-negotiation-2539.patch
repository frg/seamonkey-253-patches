# HG changeset patch
# User Ascrod <32915892+Ascrod@users.noreply.github.com>
# Date 1739830827 0
# Parent  4a3f8c87174f219327b226d9cee9db96001befbc
Bug 1717539 - Implement basic CAP negotiation. r=IanN a=IanN

Implement basic CAP negotiation.
IRCv3: Implement cap values and multiline replies.
IRCv3: Explicitly request multi-prefix capability.
IRCv3: Handle ACKing and NAKing multiple caps at once.
Fix /supports for new caps array format.
Always store values for caps.
Fix building the CAP REQ command for caps with values.
End CAP negotation after receiving CAP NAK.
Make caps and values less annoying to work with.
Clear cap values on disconnect.
Clear CAP values on connect instead of on disconnect.
Always propogate an event for CAP LS. Tag #53.
Show CAP messages on relevant network view.

diff --git a/suite/extensions/irc/js/lib/irc.js b/suite/extensions/irc/js/lib/irc.js
--- a/suite/extensions/irc/js/lib/irc.js
+++ b/suite/extensions/irc/js/lib/irc.js
@@ -6,16 +6,37 @@
 
 const JSIRC_ERR_NO_SOCKET = "JSIRCE:NS";
 const JSIRC_ERR_EXHAUSTED = "JSIRCE:E";
 const JSIRC_ERR_CANCELLED = "JSIRCE:C";
 const JSIRC_ERR_NO_SECURE = "JSIRCE:NO_SECURE";
 const JSIRC_ERR_OFFLINE = "JSIRCE:OFFLINE";
 const JSIRC_ERR_PAC_LOADING = "JSIRCE:PAC_LOADING";
 
+const JSIRCV3_SUPPORTED_CAPS = [
+  //"account-notify",
+  //"account-tag",
+  //"away-notify",
+  //"batch",
+  //"cap-notify",
+  //"chghost",
+  //"echo-message",
+  //"extended-join",
+  //"invite-notify",
+  //"labeled-response",
+  //"message-tags",
+  //"metadata",
+  //"monitor",
+  "multi-prefix",
+  //"sasl",
+  //"server-time",
+  //"tls",
+  //"userhost-in-name",
+];
+
 function userIsMe(user) {
   switch (user.TYPE) {
     case "IRCUser":
       return user == user.parent.me;
       break;
 
     case "IRCChanUser":
       return user.__proto__ == user.parent.parent.me;
@@ -474,16 +495,17 @@ function CIRCServer(parent, hostname, po
   s.usersStable = true;
   s.supports = null;
   s.channelTypes = null;
   s.channelModes = null;
   s.channelCount = -1;
   s.userModes = null;
   s.maxLineLength = 400;
   s.caps = new Object();
+  s.capvals = new Object();
 
   parent.servers[s.canonicalName] = s;
   if ("onInit" in s) {
     s.onInit();
   }
   return s;
 }
 
@@ -708,16 +730,23 @@ CIRCServer.prototype.onSocketConnection 
   }
 };
 
 /*
  * What to do when the client connects to it's primary server
  */
 CIRCServer.prototype.onConnect = function serv_onconnect(e) {
   this.parent.primServ = e.server;
+
+  this.sendData("CAP LS 302\n");
+  this.pendingCapNegotiation = true;
+
+  this.caps = new Object();
+  this.capvals = new Object();
+
   this.login(
     this.parent.INITIAL_NICK,
     this.parent.INITIAL_NAME,
     this.parent.INITIAL_DESC
   );
   return true;
 };
 
@@ -1299,16 +1328,18 @@ CIRCServer.prototype.onTopic = function 
 
   return true;
 };
 
 /* Successful login */
 CIRCServer.prototype.on001 = function serv_001(e) {
   this.parent.connectAttempt = 0;
   this.parent.connectCandidate = 0;
+  //Mark capability negotiation as finished, if we haven't already.
+  delete this.parent.pendingCapNegotiation;
   this.parent.state = NET_ONLINE;
   // nextHost is incremented after picking a server. Push it back here.
   this.parent.nextHost--;
 
   /* servers won't send a nick change notification if user was forced
    * to change nick while logging in (eg. nick already in use.)  We need
    * to verify here that what the server thinks our name is, matches what
    * we think it is.  If not, the server wins.
@@ -1839,43 +1870,106 @@ CIRCServer.prototype.onCap = function my
 
   if (e.params[2] == "LS") {
     /* We're getting a list of all server capabilities. Set them all to
      * null (if they don't exist) to indicate we don't know if they're
      * enabled or not (but this will evaluate to false which matches that
      * capabilities are only enabled on request).
      */
     var caps = e.params[3].split(/\s+/);
+    var multiline = e.params[3] == "*";
+    if (multiline) {
+      caps = e.params[4].split(/\s+/);
+    }
+
     for (var i = 0; i < caps.length; i++) {
-      var cap = caps[i].replace(/^-/, "").trim();
+      var [cap, value] = caps[i].split(/=(.+)/);
+      cap = cap.replace(/^-/, "").trim();
       if (!(cap in this.caps)) {
         this.caps[cap] = null;
       }
+      if (value) {
+        this.capvals[cap] = value;
+      }
+    }
+
+    // Don't do anything until the end of the response.
+    if (multiline) {
+      return true;
+    }
+
+    //Only request capabilities we support if we are connecting.
+    if (this.pendingCapNegotiation) {
+      var caps_req = JSIRCV3_SUPPORTED_CAPS.filter(i => i in this.caps);
+      if (caps_req.length > 0) {
+        caps_req = caps_req.join(" ");
+        e.server.sendData("CAP REQ :" + caps_req + "\n");
+      } else {
+        e.server.sendData("CAP END\n");
+        delete this.pendingCapNegotiation;
+      }
     }
   } else if (e.params[2] == "LIST") {
     /* Received list of enabled capabilities. Just use this as a sanity
      * check. */
     var caps = e.params[3].trim().split(/\s+/);
+    var multiline = e.params[3] == "*";
+    if (multiline) {
+      caps = e.params[4].trim().split(/\s+/);
+    }
+
     for (var i = 0; i < caps.length; i++) {
       this.caps[caps[i]] = true;
     }
+
+    // Don't do anything until the end of the response.
+    if (multiline) {
+      return true;
+    }
   } else if (e.params[2] == "ACK") {
     /* One or more capability changes have been successfully applied. An enabled
      * capability is just "cap" whilst a disabled capability is "-cap".
      */
     var caps = e.params[3].trim().split(/\s+/);
+    e.capsOn = new Array();
+    e.capsOff = new Array();
     for (var i = 0; i < caps.length; i++) {
-      var cap = caps[i];
-      e.cap = cap.replace(/^-/, "").trim();
-      e.capEnabled = cap[0] != "-";
-      this.caps[e.cap] = e.capEnabled;
+      var cap = caps[i].replace(/^-/, "").trim();
+      var enabled = caps[i][0] != "-";
+      if (enabled) {
+        e.capsOn.push(cap);
+      } else {
+        e.capsOff.push(cap);
+      }
+      this.caps[cap] = enabled;
+    }
+
+    if (this.pendingCapNegotiation) {
+      e.server.sendData("CAP END\n");
+      delete this.pendingCapNegotiation;
+
+      //Don't show the raw message while connecting.
+      return true;
     }
   } else if (e.params[2] == "NAK") {
     // A capability change has failed.
-    e.cap = e.params[3].replace(/^-/, "").trim();
+    var caps = e.params[3].trim().split(/\s+/);
+    e.caps = new Array();
+    for (var i = 0; i < caps.length; i++) {
+      var cap = caps[i].replace(/^-/, "").trim();
+      e.caps.push(cap);
+    }
+
+    if (this.pendingCapNegotiation) {
+      e.server.sendData("CAP END\n");
+      delete this.pendingCapNegotiation;
+
+      //Don't show the raw message while connecting.
+      return true;
+    }
   } else {
     dd("Unknown CAP reply " + e.params[2]);
   }
 
   e.destObject = this.parent;
   e.set = "network";
 };
 
@@ -2247,17 +2341,17 @@ CIRCServer.prototype.onNotice = CIRCServ
     } else {
       e.set = "user";
       e.replyTo = e.user; /* send replies to the user who sent the message */
     }
 
     /* The capability identify-msg adds a + or - in front the message to
      * indicate their network registration status.
      */
-    if (this.caps["identify-msg"]) {
+    if ("identify-msg" in this.caps && this.caps["identify-msg"]) {
       e.identifyMsg = false;
       var flag = e.params[2].substring(0, 1);
       if (flag == "+") {
         e.identifyMsg = true;
         e.params[2] = e.params[2].substring(1);
       } else if (flag == "-") {
         e.params[2] = e.params[2].substring(1);
       } else {
diff --git a/suite/extensions/irc/xul/content/handlers.js b/suite/extensions/irc/xul/content/handlers.js
--- a/suite/extensions/irc/xul/content/handlers.js
+++ b/suite/extensions/irc/xul/content/handlers.js
@@ -2248,43 +2248,53 @@ CIRCNetwork.prototype.onWallops = functi
 CIRCNetwork.prototype.on421 = function my_421(e) {
   this.display(getMsg(MSG_IRC_421, e.decodeParam(2)), MT_ERROR);
   return true;
 };
 
 /* cap reply */
 CIRCNetwork.prototype.onCap = function my_cap(e) {
   if (e.params[2] == "LS") {
-    var listCaps = new Array();
-    for (var cap in e.server.caps) {
-      listCaps.push(cap);
-    }
-    if (listCaps.length > 0) {
-      listCaps.sort();
-      display(getMsg(MSG_SUPPORTS_CAPS, listCaps.join(MSG_COMMASP)));
+    // Don't show the raw message until we've registered.
+    if (this.state == NET_ONLINE) {
+      var listCaps = new Array();
+      for (var cap in e.server.caps) {
+        var value = e.server.capvals[cap];
+        if (value) {
+          cap += "=" + value;
+        }
+        listCaps.push(cap);
+      }
+      if (listCaps.length > 0) {
+        listCaps.sort();
+        this.display(getMsg(MSG_SUPPORTS_CAPS, listCaps.join(MSG_COMMASP)));
+      }
     }
   } else if (e.params[2] == "LIST") {
     var listCapsEnabled = new Array();
     for (var cap in e.server.caps) {
       if (e.server.caps[cap]) {
         listCapsEnabled.push(cap);
       }
     }
     if (listCapsEnabled.length > 0) {
       listCapsEnabled.sort();
-      display(getMsg(MSG_SUPPORTS_CAPSON, listCapsEnabled.join(MSG_COMMASP)));
+      this.display(
+        getMsg(MSG_SUPPORTS_CAPSON, listCapsEnabled.join(MSG_COMMASP))
+      );
     }
   } else if (e.params[2] == "ACK") {
-    if (e.capEnabled) {
-      display(getMsg(MSG_CAPS_ON, e.cap));
-    } else {
-      display(getMsg(MSG_CAPS_OFF, e.cap));
+    if (e.capsOn.length) {
+      this.display(getMsg(MSG_CAPS_ON, e.capsOn.join(", ")));
+    }
+    if (e.capsOff.length) {
+      this.display(getMsg(MSG_CAPS_OFF, e.capsOff.join(", ")));
     }
   } else if (e.params[2] == "NAK") {
-    display(getMsg(MSG_CAPS_ERROR, e.cap));
+    this.display(getMsg(MSG_CAPS_ERROR, e.caps.join(", ")));
   }
   return true;
 };
 
 CIRCNetwork.prototype.reclaimName = function my_reclaimname() {
   var network = this;
 
   function callback() {
