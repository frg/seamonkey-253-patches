# HG changeset patch
# User Ian Neal <iann_cvs@blueyonder.co.uk>
# Date 1631438940 -3600
# Parent  6bc23228ce11207ea76e5fb0aeee96793ddf6385
Bug 1730391 - Add removeBrowser helper for tabbrowser. r=frg a=frg

diff --git a/suite/browser/tabbrowser.xml b/suite/browser/tabbrowser.xml
--- a/suite/browser/tabbrowser.xml
+++ b/suite/browser/tabbrowser.xml
@@ -1973,16 +1973,35 @@
             }
             this.tabContainer._handleNewTab(t);
 
             return t;
           ]]>
         </body>
       </method>
 
+      <method name="removeBrowser">
+        <parameter name="aBrowser"/>
+        <body>
+          <![CDATA[
+            let panel = this.getNotificationBox(aBrowser);
+            panel.destroy();
+            aBrowser.destroy();
+
+            // The pagehide event that this removal triggers is safe
+            // because the browser is no longer current at this point.
+            panel.remove();
+
+            // Fix up the selected panel.
+            panel = this.getNotificationBox(this.selectedTab.linkedBrowser);
+            this.mTabBox.selectedPanel = panel;
+          ]]>
+        </body>
+      </method>
+
       <method name="removeTab">
         <parameter name="aTab"/>
         <parameter name="aParams"/>
         <body>
           <![CDATA[
             this.mLastRelatedIndex = 0;
 
             if (!aParams) {
@@ -2016,17 +2035,17 @@
             // Dispatch a notification.
             // We dispatch it before any teardown so that event listeners can
             // inspect the tab that's about to close.
             aTab.dispatchEvent(new UIEvent("TabClose",
               { bubbles: true, cancelable: false, view: window,
                 detail: !!aParams.disableUndo }));
             var tabData = aTab.tabData || {};
             tabData.pos = this.getTabIndex(aTab);
-            tabData.panel = oldBrowser.parentNode.id;
+            tabData.panel = this.getNotificationBox(oldBrowser).id;
             tabData.title = oldBrowser.contentDocument.title ||
                             this.getTitleForURI(oldBrowser.currentURI) ||
                             this.mStringBundle.getString("tabs.untitled");
 
             var index = this.getTabIndex(aTab);
 
             // Remove SSL listener
             oldBrowser.webProgress.removeProgressListener(oldBrowser.securityUI);
@@ -2099,24 +2118,19 @@
                           !Services.prefs.getBoolPref("browser.tabs.cache_popups");
             if (maxUndoDepth <= 0 || aParams.disableUndo || inOnLoad || isPopup || this.isBrowserEmpty(oldBrowser)) {
               // Undo is disabled/tab is blank.  Kill the browser for real.
               // Because of the way XBL works (fields just set JS
               // properties on the element) and the code we have in place
               // to preserve the JS objects for any elements that have
               // JS properties set on them, the browser element won't be
               // destroyed until the document goes away.  So we force a
-              // cleanup ourselves.
-              oldBrowser.parentNode.destroy();
-              oldBrowser.destroy();
-              oldBrowser.parentNode.remove();
-
-              // Fix up the selected panel in the case the removed
-              // browser was to the left of the current browser
-              this.mTabBox.selectedPanel = this.selectedTab.linkedBrowser.parentNode;
+              // cleanup ourselves. Also fix up the selected panel in the case
+              // the removed browser was to the left of the current browser.
+              this.removeBrowser(oldBrowser);
               return;
             }
 
             // preserve a pointer to the browser for undoing the close
             // 1. save a copy of the session history (oldSH)
             // 2. hook up a new history
             // 3. add the last history entry from the old history the new
             //    history so we'll be able to go back from about:blank
@@ -2148,45 +2162,33 @@
             // about:blank is light
             oldBrowser.loadURI("about:blank");
 
             // remove overflow from the undo stack
             if (this.savedBrowsers.length > maxUndoDepth) {
               tabData = this.savedBrowsers.pop();
               var deadBrowser = tabData.browserData.browser;
               delete tabData.browserData;
-              deadBrowser.parentNode.destroy();
-              deadBrowser.destroy();
-
-              // The pagehide event that this removal triggers is safe
-              // because the browser is no longer current at this point
-              deadBrowser.parentNode.remove();
-              this.mTabBox.selectedPanel = this.selectedTab.linkedBrowser.parentNode;
+              this.removeBrowser(deadBrowser);
             }
           ]]>
         </body>
       </method>
 
       <method name="forgetSavedBrowser">
         <parameter name="aIndex"/>
         <body>
           <![CDATA[
             if (aIndex >= this.savedBrowsers.length || aIndex < 0)
               return false;
 
             var tabData = this.savedBrowsers.splice(aIndex, 1)[0];
             var deadBrowser = tabData.browserData.browser;
             delete tabData.browserData;
-            deadBrowser.parentNode.destroy();
-            deadBrowser.destroy();
-
-            // The pagehide event that this removal triggers is safe
-            // because the browser is no longer current at this point
-            deadBrowser.parentNode.remove();
-            this.mTabBox.selectedPanel = this.selectedTab.linkedBrowser.parentNode;
+            this.removeBrowser(deadBrowser);
             return true;
           ]]>
         </body>
       </method>
 
       <method name="reloadAllTabs">
         <body>
           <![CDATA[
@@ -2591,16 +2593,17 @@
         </body>
       </method>
 
       <method name="moveTabTo">
         <parameter name="aTab"/>
         <parameter name="aIndex"/>
         <body>
           <![CDATA[
+            let oldPosition;
             // for compatibility with extensions
             if (typeof(aTab) == "number") {
               oldPosition = aTab;
               aTab = this.tabs[oldPosition];
             } else {
               oldPosition = this.getTabIndex(aTab);
             }
 
@@ -3060,23 +3063,17 @@
                 return;
             }
 
             // Wipe out savedBrowsers since history is gone
             while (this.savedBrowsers.length > maxUndoDepth) {
               var tabData = this.savedBrowsers.pop();
               var deadBrowser = tabData.browserData.browser;
               delete tabData.browserData;
-              deadBrowser.parentNode.destroy();
-              deadBrowser.destroy();
-
-              // The pagehide event that this removal triggers is safe
-              // because the browser is no longer current at this point
-              deadBrowser.parentNode.remove();
-              this.mTabBox.selectedPanel = this.selectedTab.linkedBrowser.parentNode;
+              this.removeBrowser(deadBrowser);
             }
           ]]>
         </body>
       </method>
 
       <field name="_fastFind">null</field>
       <property name="fastFind" readonly="true">
         <getter>
