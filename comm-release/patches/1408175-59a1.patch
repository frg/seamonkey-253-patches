# HG changeset patch
# User Jorg K <jorgk@jorgk.com>
# Date 1512512437 -3600
# Node ID 0810e7646b069ccc267d1a6ea47c1b83c8e8dca2
# Parent  744c662ee65f7b26f73382503ad0fe1a5ce7b93e
Bug 1408175 - pass nsCString to MsgStripQuotedPrintable() instead of a raw char pointer. r=bz

diff --git a/mailnews/base/search/src/nsMsgSearchTerm.cpp b/mailnews/base/search/src/nsMsgSearchTerm.cpp
--- a/mailnews/base/search/src/nsMsgSearchTerm.cpp
+++ b/mailnews/base/search/src/nsMsgSearchTerm.cpp
@@ -964,23 +964,21 @@ nsresult nsMsgSearchTerm::MatchBody (nsI
   {
     if (bodyHan->GetNextLine(buf, charset) >= 0)
     {
       bool softLineBreak = false;
       // Do in-place decoding of quoted printable
       if (isQuotedPrintable)
       {
         softLineBreak = StringEndsWith(buf, NS_LITERAL_CSTRING("="));
-        MsgStripQuotedPrintable ((unsigned char*)buf.get());
-        // in case the string shrunk, reset the length. If soft line break,
-        // chop off the last char as well.
-        size_t bufLength = strlen(buf.get());
+        MsgStripQuotedPrintable(buf);
+        // If soft line break, chop off the last char as well.
+        size_t bufLength = buf.Length();
         if ((bufLength > 0) && softLineBreak)
-          --bufLength;
-        buf.SetLength(bufLength);
+          buf.SetLength(bufLength - 1);
       }
       compare.Append(buf);
       // If this line ends with a soft line break, loop around
       // and get the next line before looking for the search string.
       // This assumes the message can't end on a QP soft-line break.
       // That seems like a pretty safe assumption.
       if (softLineBreak)
         continue;
diff --git a/mailnews/base/util/nsMsgDBFolder.cpp b/mailnews/base/util/nsMsgDBFolder.cpp
--- a/mailnews/base/util/nsMsgDBFolder.cpp
+++ b/mailnews/base/util/nsMsgDBFolder.cpp
@@ -5646,19 +5646,17 @@ void nsMsgDBFolder::decodeMsgSnippet(con
     if (aIsComplete)
       base64Len -= base64Len % 4;
     char *decodedBody = PL_Base64Decode(aMsgSnippet.get(), base64Len, nullptr);
     if (decodedBody)
       aMsgSnippet.Adopt(decodedBody);
   }
   else if (MsgLowerCaseEqualsLiteral(aEncodingType, ENCODING_QUOTED_PRINTABLE))
   {
-    // giant hack - decode in place, and truncate string.
-    MsgStripQuotedPrintable((unsigned char *) aMsgSnippet.get());
-    aMsgSnippet.SetLength(strlen(aMsgSnippet.get()));
+    MsgStripQuotedPrintable(aMsgSnippet);
   }
 }
 
 /**
  * stripQuotesFromMsgSnippet - Reduces quoted reply text including the citation (Scott wrote:) from
  *                             the message snippet to " ... ". Assumes the snippet has been decoded and converted to
  *                             plain text.
  * @param aMsgSnippet in/out argument. The string to strip quotes from.
diff --git a/mailnews/base/util/nsMsgUtils.cpp b/mailnews/base/util/nsMsgUtils.cpp
--- a/mailnews/base/util/nsMsgUtils.cpp
+++ b/mailnews/base/util/nsMsgUtils.cpp
@@ -1474,23 +1474,25 @@ nsresult MsgGetLocalFileFromURI(const ns
   nsCOMPtr<nsIFile> argFile;
   rv = argFileURL->GetFile(getter_AddRefs(argFile));
   NS_ENSURE_SUCCESS(rv, rv);
 
   argFile.forget(aFile);
   return NS_OK;
 }
 
-NS_MSG_BASE void MsgStripQuotedPrintable (unsigned char *src)
+NS_MSG_BASE void MsgStripQuotedPrintable (nsCString& aSrc)
 {
   // decode quoted printable text in place
 
-  if (!*src)
+  if (aSrc.IsEmpty())
     return;
-  unsigned char *dest = src;
+
+  char *src = aSrc.BeginWriting();
+  char *dest = src;
   int srcIdx = 0, destIdx = 0;
 
   while (src[srcIdx] != 0)
   {
     // Decode sequence of '=XY' into a character with code XY.
     if (src[srcIdx] == '=')
     {
       if (MsgIsHex((const char*)src + srcIdx + 1, 2)) {
@@ -1521,16 +1523,17 @@ NS_MSG_BASE void MsgStripQuotedPrintable
         continue;
       }
     }
     else
       dest[destIdx++] = src[srcIdx++];
   }
 
   dest[destIdx] = src[srcIdx]; // null terminate
+  aSrc.SetLength(destIdx);
 }
 
 NS_MSG_BASE nsresult MsgEscapeString(const nsACString &aStr,
                                      uint32_t aType, nsACString &aResult)
 {
   nsresult rv;
   nsCOMPtr<nsINetUtil> nu = do_GetService(NS_NETUTIL_CONTRACTID, &rv);
   NS_ENSURE_SUCCESS(rv, rv);
diff --git a/mailnews/base/util/nsMsgUtils.h b/mailnews/base/util/nsMsgUtils.h
--- a/mailnews/base/util/nsMsgUtils.h
+++ b/mailnews/base/util/nsMsgUtils.h
@@ -194,17 +194,17 @@ NS_MSG_BASE nsresult MsgNewSafeBufferedF
 // and returns false if the keyword isn't present
 NS_MSG_BASE bool MsgFindKeyword(const nsCString &keyword, nsCString &keywords, int32_t *aStartOfKeyword, int32_t *aLength);
 
 NS_MSG_BASE bool MsgHostDomainIsTrusted(nsCString &host, nsCString &trustedMailDomains);
 
 // gets an nsIFile from a UTF-8 file:// path
 NS_MSG_BASE nsresult MsgGetLocalFileFromURI(const nsACString &aUTF8Path, nsIFile **aFile);
 
-NS_MSG_BASE void MsgStripQuotedPrintable (unsigned char *src);
+NS_MSG_BASE void MsgStripQuotedPrintable (nsCString& aSrc);
 
 /*
  * Utility function copied from nsReadableUtils
  */
 NS_MSG_BASE bool MsgIsUTF8(const nsACString& aString);
 
 /*
  * Utility functions that call functions from nsINetUtil
