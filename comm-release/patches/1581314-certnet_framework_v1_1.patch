# HG changeset patch
# User Ian Neal <iann_cvs@blueyonder.co.uk>
# Date 1623162001 14400
# Parent  add8d9ee7f5bec254dc6bc45eda36f7c1d391f7e
Bug 1581314 - Introduce a framework for expanding certerror code to handle neterrors. r=frg a=frg

diff --git a/suite/base/content/certError.js b/suite/base/content/certError.js
--- a/suite/base/content/certError.js
+++ b/suite/base/content/certError.js
@@ -19,16 +19,19 @@
 document.getElementById("technicalContentHeading")
         .addEventListener("click", function() { toggle("technicalContent"); });
 
 document.getElementById("expertContentHeading")
         .addEventListener("click", function() { toggle("expertContent"); });
 
 let gSearchParams;
 
+// Set to true on init if the error code is nssBadCert.
+let gIsCertError;
+
 initPage();
 
 function getErrorCode() {
   return gSearchParams.get("e");
 }
 
 function getCSSClass() {
   return gSearchParams.get("s");
@@ -36,41 +39,81 @@ function getCSSClass() {
 
 function getDescription() {
   return gSearchParams.get("d");
 }
 
 function initPage() {
   gSearchParams = new URLSearchParams(document.documentURI.split("?")[1]);
 
-  var intro = document.getElementById("introContentP1");
-  var node = document.evaluate('//text()[string()="#1"]', intro, null,
-                               XPathResult.ANY_UNORDERED_NODE_TYPE,
-                               null).singleNodeValue;
-  if (node)
-    node.textContent = location.host;
+  let err = getErrorCode();
+  gIsCertError = (err == "nssBadCert");
+
+  let pageTitle = document.getElementById("ept_" + err);
+  if (pageTitle) {
+    document.title = pageTitle.textContent;
+  }
+
+  // If it's an unknown error or there's no title or description defined,
+  // get the generic message.
+  let errTitle = document.getElementById("et_" + err);
+  let errDesc  = document.getElementById("ed_" + err);
+  if (!errTitle || !errDesc) {
+    errTitle = document.getElementById("et_generic");
+    errDesc  = document.getElementById("ed_generic");
+  }
+
+  let title = document.getElementById("errorTitleText");
+  if (title) {
+    title.innerHTML = errTitle.innerHTML;
+  }
 
-  switch (getCSSClass()) {
-  case "expertBadCert":
+  let sd = document.getElementById("errorShortDescText");
+  if (sd) {
+    if (gIsCertError) {
+      sd.innerHTML = errDesc.innerHTML;
+    } else {
+      sd.textContent = getDescription();
+    }
+  }
+
+  let xd = document.getElementById("errorShortDescExtra");
+  if (xd) {
+    let errExtra = document.getElementById("ex_" + err);
+    if (gIsCertError && errExtra) {
+      xd.innerHTML = errExtra.innerHTML;
+    } else {
+      xd.remove();
+    }
+  }
+
+  // Remove undisplayed errors to avoid bug 39098.
+  let errContainer = document.getElementById("errorContainer");
+  errContainer.remove();
+
+  if (gIsCertError) {
+    for (let host of document.querySelectorAll(".hostname")) {
+      host.textContent = location.host;
+    }
+  }
+
+  let className = getCSSClass();
+  if (className == "expertBadCert") {
     toggle("technicalContent");
     toggle("expertContent");
-    // fall through
-
-  default:
-    document.getElementById("badStsCertExplanation").remove();
-    if (window == window.top)
-      break;
-    // else fall though
+  }
 
   // Disallow overrides if this is a Strict-Transport-Security
   // host and the cert is bad (STS Spec section 7.3);
   // or if the cert error is in a frame (bug 633691).
-  case "badStsCert":
-    document.getElementById("expertContent").remove();
-    break;
+  if (className == "badStsCert" || window != top) {
+    document.getElementById("expertContent").setAttribute("hidden", "true");
+  }
+  if (className == "badStsCert") {
+    document.getElementById("badStsCertExplanation").removeAttribute("hidden");
   }
 
   addDomainErrorLinks();
 }
 
 /* In the case of SSL error pages about domain mismatch, see if
    we can hyperlink the user to the correct site.  We don't want
    to do this generically since it allows MitM attacks to redirect
diff --git a/suite/base/content/certError.xhtml b/suite/base/content/certError.xhtml
--- a/suite/base/content/certError.xhtml
+++ b/suite/base/content/certError.xhtml
@@ -17,44 +17,63 @@
   %certerrorDTD;
 ]>
 
 <!-- This Source Code Form is subject to the terms of the Mozilla Public
    - License, v. 2.0. If a copy of the MPL was not distributed with this
    - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
 <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
-    <title>&certerror.pagetitle;</title>
+    <title>&loadError.label;</title>
     <link rel="stylesheet" href="chrome://communicator/content/certError.css" type="text/css" media="all" />
     <link rel="stylesheet" href="chrome://communicator/skin/certError.css" type="text/css" media="all" />
     <link rel="icon" type="image/png" id="favicon" href="chrome://global/skin/icons/warning-16.png"/>
   </head>
 
   <body dir="&locale.dir;">
 
+    <!-- ERROR ITEM CONTAINER (removed during loading to avoid bug 39098) -->
+    <div id="errorContainer" style="display: none;">
+      <div id="errorPageTitlesContainer">
+        <span id="ept_nssBadCert">&certerror.pagetitle;</span>
+      </div>
+      <div id="errorTitlesContainer">
+        <h1 id="et_generic">&generic.title;</h1>
+        <h1 id="et_nssBadCert">&certerror.longpagetitle;</h1>
+      </div>
+      <div id="errorDescriptionsContainer">
+        <div id="ed_generic">&generic.longDesc;</div>
+        <div id="ed_nssBadCert">&certerror.introPara1a;</div>
+        <div id="ex_nssBadCert">&certerror.introPara2;</div>
+      </div>
+    </div>
+
     <!-- PAGE CONTAINER (for styling purposes only) -->
     <div id="errorPageContainer">
 
       <!-- Error Title -->
       <div id="errorTitle">
-        <h1 id="errorTitleText">&certerror.longpagetitle;</h1>
+        <h1 id="errorTitleText"/>
       </div>
 
       <!-- LONG CONTENT (the section most likely to require scrolling) -->
       <div id="errorLongContent">
-        <div id="introContent">
-          <p id="introContentP1">&certerror.introPara1;</p>
-          <p>&certerror.introPara2;</p>
+
+        <!-- Short Description -->
+        <div id="errorShortDesc">
+          <p id="errorShortDescText"/>
+          <p id="errorShortDescExtra"/>
         </div>
 
         <div id="whatShouldIDoContent">
           <h2>&certerror.whatShouldIDo.heading;</h2>
           <div id="whatShouldIDoContentText">
             <p>&certerror.whatShouldIDo.content;</p>
-            <p id="badStsCertExplanation">&certerror.whatShouldIDo.badStsCertExplanation;</p>
+            <p id="badStsCertExplanation"
+               hidden="true">&certerror.whatShouldIDo.badStsCertExplanation;</p>
             <span id="getMeOutOfHereButton"
                   class="button"
                   label="&certerror.getMeOutOfHere.label;"/>
           </div>
         </div>
 
         <!-- The following sections can be unhidden by default by setting the
              "browser.xul.error_pages.expert_bad_cert" pref to true -->
diff --git a/suite/locales/en-US/chrome/common/certError.dtd b/suite/locales/en-US/chrome/common/certError.dtd
--- a/suite/locales/en-US/chrome/common/certError.dtd
+++ b/suite/locales/en-US/chrome/common/certError.dtd
@@ -4,21 +4,22 @@
 
 <!-- These strings are used by SeaMonkey's custom about:certerror page,
 a replacement for the standard security certificate errors produced
 by NSS/PSM via netError.xhtml. -->
 
 <!ENTITY certerror.pagetitle  "Untrusted Connection">
 <!ENTITY certerror.longpagetitle "This Connection is Untrusted">
 
-<!-- Localization note (certerror.introPara1) - The string "#1" will
-be replaced at runtime with the name of the server to which the user
+<!-- Localization note (certerror.introPara1a) - The text content of the span
+tag will be replaced at runtime with the name of the server to which the user
 was trying to connect. -->
-<!ENTITY certerror.introPara1 "You have asked &brandShortName; to connect
-securely to <b>#1</b>, but we can't confirm that your connection is secure.">
+<!ENTITY certerror.introPara1a "You have asked &brandShortName; to connect
+securely to <span class='hostname'/>, but we can't confirm that your connection
+is secure.">
 <!ENTITY certerror.introPara2 "Normally, when you try to connect securely,
 websites will present trusted identification to prove that you are
 going to the right place. However, this website's identity can't be verified.">
 
 <!ENTITY certerror.whatShouldIDo.heading "What Should I Do?">
 <!ENTITY certerror.whatShouldIDo.content "If you usually connect to
 this website without problems, this error could mean that someone is
 trying to impersonate the website, and you shouldn't continue.">
diff --git a/suite/themes/classic/communicator/certError.css b/suite/themes/classic/communicator/certError.css
--- a/suite/themes/classic/communicator/certError.css
+++ b/suite/themes/classic/communicator/certError.css
@@ -19,16 +19,20 @@ h1 {
   border-bottom: 1px solid ThreeDLightShadow;
   font-size: 160%;
 }
 
 h2 {
   font-size: 130%;
 }
 
+span.hostname {
+  font-weight: bolder;
+}
+
 #errorPageContainer {
   position: relative;
   min-width: 13em;
   max-width: 52em;
   margin: 4em auto;
   border: 1px solid #FFBD09;
   border-radius: 10px;
   padding: 3em;
diff --git a/suite/themes/modern/communicator/certError.css b/suite/themes/modern/communicator/certError.css
--- a/suite/themes/modern/communicator/certError.css
+++ b/suite/themes/modern/communicator/certError.css
@@ -19,16 +19,20 @@ h1 {
   border-bottom: 1px solid #7A8490;
   font-size: 160%;
 }
 
 h2 {
   font-size: 130%;
 }
 
+span.hostname {
+  font-weight: bolder;
+}
+
 #errorPageContainer {
   position: relative;
   min-width: 13em;
   max-width: 52em;
   margin: 4em auto;
   border: 1px solid #E8DB99;
   border-radius: 10px;
   padding: 3em;
