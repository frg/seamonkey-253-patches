# HG changeset patch
# User Sean Lee <selee@mozilla.com>
# Date 1508300460 -28800
# Node ID c79eed2950589848897acb2bb45f1144f6cd7fd1
# Parent  ab15ff8d1a482dd4e7f3cb070fb095afe697705f
Bug 1409306 - Check the existence of the profile's cc-exp-month and cc-exp-year in `findCreditCardSelectOption`. r=lchang

MozReview-Commit-ID: 5gBLJcWXkGT

diff --git a/browser/extensions/formautofill/FormAutofillUtils.jsm b/browser/extensions/formautofill/FormAutofillUtils.jsm
--- a/browser/extensions/formautofill/FormAutofillUtils.jsm
+++ b/browser/extensions/formautofill/FormAutofillUtils.jsm
@@ -382,45 +382,54 @@ this.FormAutofillUtils = {
         break;
       }
     }
 
     return null;
   },
 
   findCreditCardSelectOption(selectEl, creditCard, fieldName) {
-    let oneDigitMonth = creditCard["cc-exp-month"].toString();
-    let twoDigitsMonth = oneDigitMonth.padStart(2, "0");
-    let fourDigitsYear = creditCard["cc-exp-year"].toString();
-    let twoDigitsYear = fourDigitsYear.substr(2, 2);
+    let oneDigitMonth = creditCard["cc-exp-month"] ? creditCard["cc-exp-month"].toString() : null;
+    let twoDigitsMonth = oneDigitMonth ? oneDigitMonth.padStart(2, "0") : null;
+    let fourDigitsYear = creditCard["cc-exp-year"] ? creditCard["cc-exp-year"].toString() : null;
+    let twoDigitsYear = fourDigitsYear ? fourDigitsYear.substr(2, 2) : null;
     let options = Array.from(selectEl.options);
 
     switch (fieldName) {
       case "cc-exp-month": {
+        if (!oneDigitMonth) {
+          return null;
+        }
         for (let option of options) {
           if ([option.text, option.label, option.value].some(s => {
             let result = /[1-9]\d*/.exec(s);
             return result && result[0] == oneDigitMonth;
           })) {
             return option;
           }
         }
         break;
       }
       case "cc-exp-year": {
+        if (!fourDigitsYear) {
+          return null;
+        }
         for (let option of options) {
           if ([option.text, option.label, option.value].some(
             s => s == twoDigitsYear || s == fourDigitsYear
           )) {
             return option;
           }
         }
         break;
       }
       case "cc-exp": {
+        if (!oneDigitMonth || !fourDigitsYear) {
+          return null;
+        }
         let patterns = [
           oneDigitMonth + "/" + twoDigitsYear,    // 8/22
           oneDigitMonth + "/" + fourDigitsYear,   // 8/2022
           twoDigitsMonth + "/" + twoDigitsYear,   // 08/22
           twoDigitsMonth + "/" + fourDigitsYear,  // 08/2022
           oneDigitMonth + "-" + twoDigitsYear,    // 8-22
           oneDigitMonth + "-" + fourDigitsYear,   // 8-2022
           twoDigitsMonth + "-" + twoDigitsYear,   // 08-22
diff --git a/browser/extensions/formautofill/test/unit/test_getAdaptedProfiles.js b/browser/extensions/formautofill/test/unit/test_getAdaptedProfiles.js
--- a/browser/extensions/formautofill/test/unit/test_getAdaptedProfiles.js
+++ b/browser/extensions/formautofill/test/unit/test_getAdaptedProfiles.js
@@ -653,16 +653,92 @@ const TESTCASES = [
     document: `<form><select autocomplete="cc-exp">
                  <option value="1703">1703</option>
                  <option value="2501" id="selected-cc-exp">2501</option>
                </select></form>`,
     profileData: [Object.assign({}, DEFAULT_CREDITCARD_RECORD)],
     expectedResult: [DEFAULT_CREDITCARD_RECORD],
     expectedOptionElements: [{"cc-exp": "selected-cc-exp"}],
   },
+  {
+    description: "Fill a cc-exp without cc-exp-month value in the profile",
+    document: `<form><select autocomplete="cc-exp">
+                 <option value="03/17">03/17</option>
+                 <option value="01/25">01/25</option>
+               </select></form>`,
+    profileData: [Object.assign({}, {
+      "guid": "123",
+      "cc-exp-year": 2025,
+    })],
+    expectedResult: [{
+      "guid": "123",
+      "cc-exp-year": 2025,
+    }],
+    expectedOptionElements: [],
+  },
+  {
+    description: "Fill a cc-exp without cc-exp-year value in the profile",
+    document: `<form><select autocomplete="cc-exp">
+                 <option value="03/17">03/17</option>
+                 <option value="01/25">01/25</option>
+               </select></form>`,
+    profileData: [Object.assign({}, {
+      "guid": "123",
+      "cc-exp-month": 1,
+    })],
+    expectedResult: [{
+      "guid": "123",
+      "cc-exp-month": 1,
+    }],
+    expectedOptionElements: [],
+  },
+  {
+    description: "Fill a cc-exp* without cc-exp-month value in the profile",
+    document: `<form>
+               <select autocomplete="cc-exp-month">
+                 <option value="03">03</option>
+                 <option value="01">01</option>
+               </select>
+               <select autocomplete="cc-exp-year">
+                 <option value="17">2017</option>
+                 <option value="25">2025</option>
+               </select>
+               </form>`,
+    profileData: [Object.assign({}, {
+      "guid": "123",
+      "cc-exp-year": 2025,
+    })],
+    expectedResult: [{
+      "guid": "123",
+      "cc-exp-year": 2025,
+    }],
+    expectedOptionElements: [],
+  },
+  {
+    description: "Fill a cc-exp* without cc-exp-year value in the profile",
+    document: `<form>
+               <select autocomplete="cc-exp-month">
+                 <option value="03">03</option>
+                 <option value="01">01</option>
+               </select>
+               <select autocomplete="cc-exp-year">
+                 <option value="17">2017</option>
+                 <option value="25">2025</option>
+               </select>
+               </form>`,
+    profileData: [Object.assign({}, {
+      "guid": "123",
+      "cc-exp-month": 1,
+    })],
+    expectedResult: [{
+      "guid": "123",
+      "cc-exp-month": 1,
+    }],
+    expectedOptionElements: [],
+  },
 ];
 
 for (let testcase of TESTCASES) {
   add_task(async function() {
     info("Starting testcase: " + testcase.description);
 
     let doc = MockDocument.createTestDocument("http://localhost:8080/test/",
                                               testcase.document);
