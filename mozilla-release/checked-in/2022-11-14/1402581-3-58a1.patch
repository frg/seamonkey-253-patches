# HG changeset patch
# User Tom Tung <shes050117@gmail.com>
# Date 1507195840 -28800
#      Thu Oct 05 17:30:40 2017 +0800
# Node ID dfc03e908c89f76c073acd7e46419890cfd87c6b
# Parent  909611ecd8422da6fde0505a41ea5622dca4c486
Bug 1402581 - P3: Allow failures happen when restoring or wiping padding file. r=bkelly

diff --git a/dom/cache/FileUtils.cpp b/dom/cache/FileUtils.cpp
--- a/dom/cache/FileUtils.cpp
+++ b/dom/cache/FileUtils.cpp
@@ -863,36 +863,45 @@ LockedDirectoryPaddingFinalizeWrite(nsIF
   rv = file->RenameTo(nullptr, NS_LITERAL_STRING(PADDING_FILE_NAME));
   if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
 
   return rv;
 }
 
 // static
 nsresult
-LockedDirectoryPaddingRestore(nsIFile* aBaseDir, mozIStorageConnection* aConn)
+LockedDirectoryPaddingRestore(nsIFile* aBaseDir, mozIStorageConnection* aConn,
+                              bool aMustRestore, int64_t* aPaddingSizeOut)
 {
   MOZ_DIAGNOSTIC_ASSERT(aBaseDir);
   MOZ_DIAGNOSTIC_ASSERT(aConn);
+  MOZ_DIAGNOSTIC_ASSERT(aPaddingSizeOut);
 
   // The content of padding file is untrusted, so remove it here.
   nsresult rv = LockedDirectoryPaddingDeleteFile(aBaseDir,
                                                  DirPaddingFile::FILE);
   if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
 
-  rv = LockedDirectoryPaddingDeleteFile(aBaseDir, DirPaddingFile::TMP_FILE);
-  if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
-
   int64_t paddingSize = 0;
   rv = db::FindOverallPaddingSize(aConn, &paddingSize);
   if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
 
   MOZ_DIAGNOSTIC_ASSERT(paddingSize >= 0);
+  *aPaddingSizeOut = paddingSize;
 
-  LockedDirectoryPaddingWrite(aBaseDir, DirPaddingFile::FILE, paddingSize);
+  rv = LockedDirectoryPaddingWrite(aBaseDir, DirPaddingFile::FILE, paddingSize);
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    // If we cannot write the correct padding size to file, just keep the
+    // temporary file and let the padding size to be recalculate in the next
+    // action
+    return aMustRestore ? rv : NS_OK;
+  }
+
+  rv = LockedDirectoryPaddingDeleteFile(aBaseDir, DirPaddingFile::TMP_FILE);
+  Unused << NS_WARN_IF(NS_FAILED(rv));
 
   return rv;
 }
 
 // static
 nsresult
 LockedDirectoryPaddingDeleteFile(nsIFile* aBaseDir,
                                  DirPaddingFile aPaddingFileType)
diff --git a/dom/cache/FileUtils.h b/dom/cache/FileUtils.h
--- a/dom/cache/FileUtils.h
+++ b/dom/cache/FileUtils.h
@@ -116,17 +116,18 @@ LockedUpdateDirectoryPaddingFile(nsIFile
 
 nsresult
 LockedDirectoryPaddingTemporaryWrite(nsIFile* aBaseDir, int64_t aPaddingSize);
 
 nsresult
 LockedDirectoryPaddingFinalizeWrite(nsIFile* aBaseDir);
 
 nsresult
-LockedDirectoryPaddingRestore(nsIFile* aBaseDir, mozIStorageConnection* aConn);
+LockedDirectoryPaddingRestore(nsIFile* aBaseDir, mozIStorageConnection* aConn,
+                              bool aMustRestore, int64_t* aPaddingSizeOut);
 
 nsresult
 LockedDirectoryPaddingDeleteFile(nsIFile* aBaseDir,
                                  DirPaddingFile aPaddingFileType);
 } // namespace cache
 } // namespace dom
 } // namespace mozilla
 
diff --git a/dom/cache/QuotaClient.cpp b/dom/cache/QuotaClient.cpp
--- a/dom/cache/QuotaClient.cpp
+++ b/dom/cache/QuotaClient.cpp
@@ -65,16 +65,50 @@ GetBodyUsage(nsIFile* aDir, const Atomic
     MOZ_DIAGNOSTIC_ASSERT(fileSize >= 0);
 
     aUsageInfo->AppendToFileUsage(fileSize);
   }
 
   return NS_OK;
 }
 
+static nsresult
+LockedGetPaddingSizeFromDB(nsIFile* aDir, const nsACString& aGroup,
+                           const nsACString& aOrigin, int64_t* aPaddingSizeOut)
+{
+  MOZ_DIAGNOSTIC_ASSERT(aDir);
+  MOZ_DIAGNOSTIC_ASSERT(aPaddingSizeOut);
+
+  *aPaddingSizeOut = 0;
+
+  nsCOMPtr<mozIStorageConnection> conn;
+  QuotaInfo quotaInfo;
+  quotaInfo.mGroup = aGroup;
+  quotaInfo.mOrigin = aOrigin;
+  nsresult rv = mozilla::dom::cache::
+                OpenDBConnection(quotaInfo, aDir, getter_AddRefs(conn));
+  if (rv == NS_ERROR_FILE_NOT_FOUND ||
+      rv == NS_ERROR_FILE_TARGET_DOES_NOT_EXIST) {
+    // Return NS_OK with size = 0 if both the db and padding file don't exist.
+    // There is no other way to get the overall padding size of an origin.
+    return NS_OK;
+  }
+  if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
+
+  int64_t paddingSize = 0;
+  rv = mozilla::dom::cache::
+       LockedDirectoryPaddingRestore(aDir, conn, /* aMustRestore */ false,
+                                     &paddingSize);
+  if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
+
+  *aPaddingSizeOut = paddingSize;
+
+  return rv;
+}
+
 class CacheQuotaClient final : public Client
 {
   static CacheQuotaClient* sInstance;
 
 public:
   CacheQuotaClient()
   : mDirPaddingFileMutex("DOMCacheQuotaClient.mDirPaddingFileMutex")
   {
@@ -138,28 +172,17 @@ public:
       // action fails, so restore the padding file.
       MutexAutoLock lock(mDirPaddingFileMutex);
 
       if (mozilla::dom::cache::
           DirectoryPaddingFileExists(dir, DirPaddingFile::TMP_FILE) ||
           NS_WARN_IF(NS_FAILED(mozilla::dom::cache::
                                LockedDirectoryPaddingGet(dir,
                                                          &paddingSize)))) {
-        nsCOMPtr<mozIStorageConnection> conn;
-        QuotaInfo quotaInfo;
-        quotaInfo.mGroup = aGroup;
-        quotaInfo.mOrigin = aOrigin;
-        rv = mozilla::dom::cache::
-             OpenDBConnection(quotaInfo, dir, getter_AddRefs(conn));
-        if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
-
-        rv = mozilla::dom::cache::LockedDirectoryPaddingRestore(dir, conn);
-        if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
-
-        rv = mozilla::dom::cache::LockedDirectoryPaddingGet(dir, &paddingSize);
+        rv = LockedGetPaddingSizeFromDB(dir, aGroup, aOrigin, &paddingSize);
         if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
       }
     }
 
     aUsageInfo->AppendToFileUsage(paddingSize);
 
     nsCOMPtr<nsISimpleEnumerator> entries;
     rv = dir->GetDirectoryEntries(getter_AddRefs(entries));
@@ -366,58 +389,69 @@ public:
   // static
   nsresult
   RestorePaddingFileInternal(nsIFile* aBaseDir, mozIStorageConnection* aConn)
   {
     MOZ_ASSERT(!NS_IsMainThread());
     MOZ_DIAGNOSTIC_ASSERT(aBaseDir);
     MOZ_DIAGNOSTIC_ASSERT(aConn);
 
+    int64_t dummyPaddingSize;
+
     MutexAutoLock lock(mDirPaddingFileMutex);
 
     nsresult rv =
-      mozilla::dom::cache::LockedDirectoryPaddingRestore(aBaseDir, aConn);
+      mozilla::dom::cache::
+      LockedDirectoryPaddingRestore(aBaseDir, aConn, /* aMustRestore */ true,
+                                    &dummyPaddingSize);
     Unused << NS_WARN_IF(NS_FAILED(rv));
 
     return rv;
   }
 
   // static
   nsresult
   WipePaddingFileInternal(const QuotaInfo& aQuotaInfo, nsIFile* aBaseDir)
   {
     MOZ_ASSERT(!NS_IsMainThread());
     MOZ_DIAGNOSTIC_ASSERT(aBaseDir);
 
     MutexAutoLock lock(mDirPaddingFileMutex);
 
-    // Remove temporary file if we have one.
-    nsresult rv =
-      mozilla::dom::cache::
-      LockedDirectoryPaddingDeleteFile(aBaseDir, DirPaddingFile::TMP_FILE);
-    if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
-
-    MOZ_DIAGNOSTIC_ASSERT(mozilla::dom::cache::
-                          DirectoryPaddingFileExists(aBaseDir,
-                                                     DirPaddingFile::FILE));
+    MOZ_ASSERT(mozilla::dom::cache::
+               DirectoryPaddingFileExists(aBaseDir, DirPaddingFile::FILE));
 
     int64_t paddingSize = 0;
-    rv = mozilla::dom::cache::LockedDirectoryPaddingGet(aBaseDir, &paddingSize);
-    if (NS_WARN_IF(NS_FAILED(rv))) {
-      // If read file fail, there is nothing we can do to recover the file.
+    bool temporaryPaddingFileExist =
+      mozilla::dom::cache::
+      DirectoryPaddingFileExists(aBaseDir, DirPaddingFile::TMP_FILE);
+
+    if (temporaryPaddingFileExist ||
+        NS_WARN_IF(NS_FAILED(
+          mozilla::dom::cache::
+          LockedDirectoryPaddingGet(aBaseDir, &paddingSize)))) {
+      // XXXtt: Maybe have a method in the QuotaManager to clean the usage under
+      // the quota client and the origin.
+      // There is nothing we can do to recover the file.
       NS_WARNING("Cannnot read padding size from file!");
       paddingSize = 0;
     }
 
     if (paddingSize > 0) {
       mozilla::dom::cache::DecreaseUsageForQuotaInfo(aQuotaInfo, paddingSize);
     }
 
+    nsresult rv =
+      mozilla::dom::cache::
+      LockedDirectoryPaddingDeleteFile(aBaseDir, DirPaddingFile::FILE);
+    if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
+
+    // Remove temporary file if we have one.
     rv = mozilla::dom::cache::
-         LockedDirectoryPaddingDeleteFile(aBaseDir, DirPaddingFile::FILE);
+         LockedDirectoryPaddingDeleteFile(aBaseDir, DirPaddingFile::TMP_FILE);
     if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
 
     rv = mozilla::dom::cache::LockedDirectoryPaddingInit(aBaseDir);
     Unused << NS_WARN_IF(NS_FAILED(rv));
 
     return rv;
   }
 
