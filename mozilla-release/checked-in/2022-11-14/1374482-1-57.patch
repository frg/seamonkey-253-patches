# HG changeset patch
# User Ting-Yu Lin <tlin@mozilla.com>
# Date 1508147921 -28800
#      Mon Oct 16 17:58:41 2017 +0800
# Node ID 0dbdb60379571829fc039843be5ae11635edc2fe
# Parent  5e1c2c0efd0b9f83cd505ff1708665e186eb39ac
Bug 1374482 - Part 1: Simplify focus changing tests for caret selection mode. r=mtseng, a=test-only

It uses _test_minimum_select_one_character() to ensure the carets appear,
but the complexity of that test is high. To make the test more stable, I
make it test select a word after changing the focus.

MozReview-Commit-ID: Jml82EpQxNm

diff --git a/layout/base/tests/marionette/test_accessiblecaret_selection_mode.py b/layout/base/tests/marionette/test_accessiblecaret_selection_mode.py
--- a/layout/base/tests/marionette/test_accessiblecaret_selection_mode.py
+++ b/layout/base/tests/marionette/test_accessiblecaret_selection_mode.py
@@ -315,34 +315,36 @@ class AccessibleCaretSelectionModeTestCa
                    el1_id=_contenteditable_id, el2_id=_content_id)
     @parameterized(_content_id + '_to_' + _input_id,
                    el1_id=_content_id, el2_id=_input_id)
     @parameterized(_content_id + '_to_' + _textarea_id,
                    el1_id=_content_id, el2_id=_textarea_id)
     @parameterized(_content_id + '_to_' + _contenteditable_id,
                    el1_id=_content_id, el2_id=_contenteditable_id)
     def test_long_press_changes_focus_from(self, el1_id, el2_id):
-        '''Test the focus could be changed from el1 to el2 by long press.
+        self.open_test_html(self._selection_html)
+        el1 = self.marionette.find_element(By.ID, el1_id)
+        el2 = self.marionette.find_element(By.ID, el2_id)
 
-        If the focus is changed to e2 successfully, the carets should appear and
-        could be dragged.
+        # Compute the content of the first word in el2.
+        sel = SelectionManager(el2)
+        original_content = sel.content
+        words = original_content.split()
+        target_content = words[0]
 
-        '''
-        # Goal: Tap to focus el1, and then select the first character on
-        # el2.
+        # Goal: Tap to focus el1, and then select the first word on el2.
 
         # We want to collect the location of the first word in el2 here
         # since self.word_location() has the side effect which would
         # change the focus.
-        self.open_test_html(self._selection_html)
-        el1 = self.marionette.find_element(By.ID, el1_id)
-        el2 = self.marionette.find_element(By.ID, el2_id)
         x, y = self.word_location(el2, 0)
+
         el1.tap()
-        self._test_minimum_select_one_character(el2, x=x, y=y)
+        self.long_press_on_location(el2, x, y)
+        self.assertEqual(target_content, sel.selected_content)
 
     @parameterized(_input_id, el_id=_input_id)
     @parameterized(_textarea_id, el_id=_textarea_id)
     @parameterized(_textarea_rtl_id, el_id=_textarea_rtl_id)
     @parameterized(_contenteditable_id, el_id=_contenteditable_id)
     def test_focus_not_changed_by_long_press_on_non_selectable(self, el_id):
         self.open_test_html(self._selection_html)
         el = self.marionette.find_element(By.ID, el_id)
