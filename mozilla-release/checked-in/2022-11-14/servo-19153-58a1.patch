# HG changeset patch
# User CYBAI <cyb.ai.815@gmail.com>
# Date 1510156357 21600
# Node ID 2c0284a0451aac2eeb376835cfbdf50fbbe005cb
# Parent  d757529a93495e47e80c5b0ecf4d30238412d0ff
servo: Merge #19153 - style: Move font-size-adjust outside of mako (from CYBAI:font-size-adjust-out-of-mako); r=emilio

This is a sub-PR of #19015
r? emilio

---
- [x] `./mach build -d` does not report any errors
- [x] `./mach test-tidy` does not report any errors
- [x] These changes fix #19151  (github issue number if applicable).
- [x] These changes do not require tests

Source-Repo: https://github.com/servo/servo
Source-Revision: 294a24af4850bc06971fe7e78bb680cc8eb02150

diff --git a/servo/components/style/properties/longhand/font.mako.rs b/servo/components/style/properties/longhand/font.mako.rs
--- a/servo/components/style/properties/longhand/font.mako.rs
+++ b/servo/components/style/properties/longhand/font.mako.rs
@@ -628,133 +628,24 @@ macro_rules! impl_gecko_keyword_conversi
                           "FontSize",
                           initial_value="computed::FontSize::medium()",
                           initial_specified_value="specified::FontSize::medium()",
                           animation_value_type="NonNegativeLength",
                           allow_quirks=True,
                           flags="APPLIES_TO_FIRST_LETTER APPLIES_TO_FIRST_LINE APPLIES_TO_PLACEHOLDER",
                           spec="https://drafts.csswg.org/css-fonts/#propdef-font-size")}
 
-<%helpers:longhand products="gecko" name="font-size-adjust"
-                   animation_value_type="longhands::font_size_adjust::computed_value::T"
-                   flags="APPLIES_TO_FIRST_LETTER APPLIES_TO_FIRST_LINE APPLIES_TO_PLACEHOLDER"
-                   spec="https://drafts.csswg.org/css-fonts/#propdef-font-size-adjust">
-    use properties::longhands::system_font::SystemFont;
-
-
-    #[derive(Clone, Copy, Debug, MallocSizeOf, PartialEq, ToCss)]
-    pub enum SpecifiedValue {
-        None,
-        Number(specified::Number),
-        System(SystemFont),
-    }
-
-    impl ToComputedValue for SpecifiedValue {
-        type ComputedValue = computed_value::T;
-
-        fn to_computed_value(&self, context: &Context) -> Self::ComputedValue {
-            match *self {
-                SpecifiedValue::None => computed_value::T::None,
-                SpecifiedValue::Number(ref n) => computed_value::T::Number(n.to_computed_value(context)),
-                SpecifiedValue::System(_) => {
-                    <%self:nongecko_unreachable>
-                        context.cached_system_font.as_ref().unwrap().font_size_adjust
-                    </%self:nongecko_unreachable>
-                }
-            }
-        }
-
-        fn from_computed_value(computed: &computed_value::T) -> Self {
-            match *computed {
-                computed_value::T::None => SpecifiedValue::None,
-                computed_value::T::Number(ref v) => SpecifiedValue::Number(specified::Number::from_computed_value(v)),
-            }
-        }
-    }
-
-    impl SpecifiedValue {
-        pub fn system_font(f: SystemFont) -> Self {
-            SpecifiedValue::System(f)
-        }
-        pub fn get_system(&self) -> Option<SystemFont> {
-            if let SpecifiedValue::System(s) = *self {
-                Some(s)
-            } else {
-                None
-            }
-        }
-    }
-
-    pub mod computed_value {
-        use values::CSSFloat;
-        use values::animated::{ToAnimatedValue, ToAnimatedZero};
-
-        #[derive(Animate, Clone, ComputeSquaredDistance, Copy, Debug, MallocSizeOf, PartialEq,
-                 ToCss)]
-        pub enum T {
-            #[animation(error)]
-            None,
-            Number(CSSFloat),
-        }
-
-        impl T {
-            pub fn from_gecko_adjust(gecko: f32) -> Self {
-                if gecko == -1.0 {
-                    T::None
-                } else {
-                    T::Number(gecko)
-                }
-            }
-        }
-
-        impl ToAnimatedZero for T {
-            #[inline]
-            fn to_animated_zero(&self) -> Result<Self, ()> { Err(()) }
-        }
-
-        impl ToAnimatedValue for T {
-            type AnimatedValue = Self;
-
-            #[inline]
-            fn to_animated_value(self) -> Self {
-                self
-            }
-
-            #[inline]
-            fn from_animated_value(animated: Self::AnimatedValue) -> Self {
-                match animated {
-                    T::Number(number) => T::Number(number.max(0.)),
-                    _ => animated
-                }
-            }
-        }
-    }
-
-    #[inline]
-    pub fn get_initial_value() -> computed_value::T {
-        computed_value::T::None
-    }
-
-    #[inline]
-    pub fn get_initial_specified_value() -> SpecifiedValue {
-        SpecifiedValue::None
-    }
-
-    /// none | <number>
-    pub fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
-                         -> Result<SpecifiedValue, ParseError<'i>> {
-        use values::specified::Number;
-
-        if input.try(|input| input.expect_ident_matching("none")).is_ok() {
-            return Ok(SpecifiedValue::None);
-        }
-
-        Ok(SpecifiedValue::Number(Number::parse_non_negative(context, input)?))
-    }
-</%helpers:longhand>
+${helpers.predefined_type("font-size-adjust",
+                          "FontSizeAdjust",
+                          products="gecko",
+                          initial_value="computed::FontSizeAdjust::none()",
+                          initial_specified_value="specified::FontSizeAdjust::none()",
+                          animation_value_type="ComputedValue",
+                          flags="APPLIES_TO_FIRST_LETTER APPLIES_TO_FIRST_LINE APPLIES_TO_PLACEHOLDER",
+                          spec="https://drafts.csswg.org/css-fonts/#propdef-font-size-adjust")}
 
 <%helpers:longhand products="gecko" name="font-synthesis" animation_value_type="discrete"
                    flags="APPLIES_TO_FIRST_LETTER APPLIES_TO_FIRST_LINE APPLIES_TO_PLACEHOLDER"
                    spec="https://drafts.csswg.org/css-fonts/#propdef-font-synthesis">
     use std::fmt;
     use style_traits::ToCss;
 
     pub mod computed_value {
diff --git a/servo/components/style/values/computed/font.rs b/servo/components/style/values/computed/font.rs
--- a/servo/components/style/values/computed/font.rs
+++ b/servo/components/style/values/computed/font.rs
@@ -2,17 +2,18 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Computed values for font properties
 
 use app_units::Au;
 use std::fmt;
 use style_traits::ToCss;
-use values::animated::ToAnimatedValue;
+use values::CSSFloat;
+use values::animated::{ToAnimatedValue, ToAnimatedZero};
 use values::computed::{Context, NonNegativeLength, ToComputedValue};
 use values::specified::font as specified;
 use values::specified::length::{FontBaseSize, NoCalcLength};
 
 pub use values::computed::Length as MozScriptMinSize;
 pub use values::specified::font::XTextZoom;
 
 /// As of CSS Fonts Module Level 3, only the following values are
@@ -207,16 +208,68 @@ impl ToAnimatedValue for FontSize {
     fn from_animated_value(animated: Self::AnimatedValue) -> Self {
         FontSize {
             size: animated.clamp(),
             keyword_info: None,
         }
     }
 }
 
+#[derive(Animate, Clone, ComputeSquaredDistance, Copy, Debug, MallocSizeOf, PartialEq, ToCss)]
+/// Preserve the readability of text when font fallback occurs
+pub enum FontSizeAdjust {
+    #[animation(error)]
+    /// None variant
+    None,
+    /// Number variant
+    Number(CSSFloat),
+}
+
+impl FontSizeAdjust {
+    #[inline]
+    /// Default value of font-size-adjust
+    pub fn none() -> Self {
+        FontSizeAdjust::None
+    }
+
+    /// Get font-size-adjust with float number
+    pub fn from_gecko_adjust(gecko: f32) -> Self {
+        if gecko == -1.0 {
+            FontSizeAdjust::None
+        } else {
+            FontSizeAdjust::Number(gecko)
+        }
+    }
+}
+
+impl ToAnimatedZero for FontSizeAdjust {
+    #[inline]
+    // FIXME(emilio): why?
+    fn to_animated_zero(&self) -> Result<Self, ()> {
+        Err(())
+    }
+}
+
+impl ToAnimatedValue for FontSizeAdjust {
+    type AnimatedValue = Self;
+
+    #[inline]
+    fn to_animated_value(self) -> Self {
+        self
+    }
+
+    #[inline]
+    fn from_animated_value(animated: Self::AnimatedValue) -> Self {
+        match animated {
+            FontSizeAdjust::Number(number) => FontSizeAdjust::Number(number.max(0.)),
+            _ => animated
+        }
+    }
+}
+
 impl ToComputedValue for specified::MozScriptMinSize {
     type ComputedValue = MozScriptMinSize;
 
     fn to_computed_value(&self, cx: &Context) -> MozScriptMinSize {
         // this value is used in the computation of font-size, so
         // we use the parent size
         let base_size = FontBaseSize::InheritedStyle;
         match self.0 {
diff --git a/servo/components/style/values/computed/mod.rs b/servo/components/style/values/computed/mod.rs
--- a/servo/components/style/values/computed/mod.rs
+++ b/servo/components/style/values/computed/mod.rs
@@ -31,17 +31,17 @@ use super::specified;
 pub use app_units::Au;
 pub use properties::animated_properties::TransitionProperty;
 #[cfg(feature = "gecko")]
 pub use self::align::{AlignItems, AlignJustifyContent, AlignJustifySelf, JustifyItems};
 pub use self::angle::Angle;
 pub use self::background::{BackgroundSize, BackgroundRepeat};
 pub use self::border::{BorderImageSlice, BorderImageWidth, BorderImageSideWidth};
 pub use self::border::{BorderRadius, BorderCornerRadius, BorderSpacing};
-pub use self::font::{FontSize, FontWeight, MozScriptLevel, MozScriptMinSize, XTextZoom};
+pub use self::font::{FontSize, FontSizeAdjust, FontWeight, MozScriptLevel, MozScriptMinSize, XTextZoom};
 pub use self::box_::{AnimationIterationCount, AnimationName, ScrollSnapType, VerticalAlign};
 pub use self::color::{Color, ColorPropertyValue, RGBAColor};
 pub use self::effects::{BoxShadow, Filter, SimpleShadow};
 pub use self::flex::FlexBasis;
 pub use self::image::{Gradient, GradientItem, Image, ImageLayer, LineDirection, MozImageRect};
 #[cfg(feature = "gecko")]
 pub use self::gecko::ScrollSnapPoint;
 pub use self::rect::LengthOrNumberRect;
diff --git a/servo/components/style/values/specified/font.rs b/servo/components/style/values/specified/font.rs
--- a/servo/components/style/values/specified/font.rs
+++ b/servo/components/style/values/specified/font.rs
@@ -8,17 +8,17 @@
 use Atom;
 use app_units::Au;
 use cssparser::{Parser, Token};
 use parser::{Parse, ParserContext};
 use properties::longhands::system_font::SystemFont;
 use std::fmt;
 use style_traits::{ToCss, StyleParseErrorKind, ParseError};
 use values::computed::{font as computed, Context, Length, NonNegativeLength, ToComputedValue};
-use values::specified::{AllowQuirks, LengthOrPercentage, NoCalcLength};
+use values::specified::{AllowQuirks, LengthOrPercentage, NoCalcLength, Number};
 use values::specified::length::{AU_PER_PT, AU_PER_PX, FontBaseSize};
 
 const DEFAULT_SCRIPT_MIN_SIZE_PT: u32 = 8;
 
 #[derive(Clone, Copy, Debug, Eq, MallocSizeOf, PartialEq, ToCss)]
 /// A specified font-weight value
 pub enum FontWeight {
     /// Normal variant
@@ -146,16 +146,86 @@ impl ToCss for FontSize {
 }
 
 impl From<LengthOrPercentage> for FontSize {
     fn from(other: LengthOrPercentage) -> Self {
         FontSize::Length(other)
     }
 }
 
+#[derive(Clone, Copy, Debug, MallocSizeOf, PartialEq, ToCss)]
+/// Preserve the readability of text when font fallback occurs
+pub enum FontSizeAdjust {
+    /// None variant
+    None,
+    /// Number variant
+    Number(Number),
+    /// system font
+    System(SystemFont),
+}
+
+impl FontSizeAdjust {
+    #[inline]
+    /// Default value of font-size-adjust
+    pub fn none() -> Self {
+        FontSizeAdjust::None
+    }
+
+    /// Get font-size-adjust with SystemFont
+    pub fn system_font(f: SystemFont) -> Self {
+        FontSizeAdjust::System(f)
+    }
+
+    /// Get SystemFont variant
+    pub fn get_system(&self) -> Option<SystemFont> {
+        if let FontSizeAdjust::System(s) = *self {
+            Some(s)
+        } else {
+            None
+        }
+    }
+}
+
+impl ToComputedValue for FontSizeAdjust {
+    type ComputedValue = computed::FontSizeAdjust;
+
+    fn to_computed_value(&self, context: &Context) -> Self::ComputedValue {
+        match *self {
+            FontSizeAdjust::None => computed::FontSizeAdjust::None,
+            FontSizeAdjust::Number(ref n) => computed::FontSizeAdjust::Number(n.to_computed_value(context)),
+            FontSizeAdjust::System(_) => {
+                #[cfg(feature = "gecko")] {
+                    context.cached_system_font.as_ref().unwrap().font_size_adjust
+                }
+                #[cfg(feature = "servo")] {
+                    unreachable!()
+                }
+            }
+        }
+    }
+
+    fn from_computed_value(computed: &computed::FontSizeAdjust) -> Self {
+        match *computed {
+            computed::FontSizeAdjust::None => FontSizeAdjust::None,
+            computed::FontSizeAdjust::Number(ref v) => FontSizeAdjust::Number(Number::from_computed_value(v)),
+        }
+    }
+}
+
+impl Parse for FontSizeAdjust {
+    /// none | <number>
+    fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>) -> Result<FontSizeAdjust, ParseError<'i>> {
+        if input.try(|input| input.expect_ident_matching("none")).is_ok() {
+            return Ok(FontSizeAdjust::None);
+        }
+
+        Ok(FontSizeAdjust::Number(Number::parse_non_negative(context, input)?))
+    }
+}
+
 /// CSS font keywords
 #[derive(Animate, ComputeSquaredDistance, MallocSizeOf, ToAnimatedValue, ToAnimatedZero)]
 #[derive(Clone, Copy, Debug, PartialEq)]
 #[allow(missing_docs)]
 pub enum KeywordSize {
     XXSmall = 1, // This is to enable the NonZero optimization
                  // which simplifies the representation of Option<KeywordSize>
                  // in bindgen
diff --git a/servo/components/style/values/specified/mod.rs b/servo/components/style/values/specified/mod.rs
--- a/servo/components/style/values/specified/mod.rs
+++ b/servo/components/style/values/specified/mod.rs
@@ -25,17 +25,17 @@ use values::specified::calc::CalcNode;
 
 pub use properties::animated_properties::TransitionProperty;
 pub use self::angle::Angle;
 #[cfg(feature = "gecko")]
 pub use self::align::{AlignItems, AlignJustifyContent, AlignJustifySelf, JustifyItems};
 pub use self::background::{BackgroundRepeat, BackgroundSize};
 pub use self::border::{BorderCornerRadius, BorderImageSlice, BorderImageWidth};
 pub use self::border::{BorderImageSideWidth, BorderRadius, BorderSideWidth, BorderSpacing};
-pub use self::font::{FontSize, FontWeight, MozScriptLevel, MozScriptMinSize, XTextZoom};
+pub use self::font::{FontSize, FontSizeAdjust, FontWeight, MozScriptLevel, MozScriptMinSize, XTextZoom};
 pub use self::box_::{AnimationIterationCount, AnimationName, ScrollSnapType, VerticalAlign};
 pub use self::color::{Color, ColorPropertyValue, RGBAColor};
 pub use self::effects::{BoxShadow, Filter, SimpleShadow};
 pub use self::flex::FlexBasis;
 #[cfg(feature = "gecko")]
 pub use self::gecko::ScrollSnapPoint;
 pub use self::image::{ColorStop, EndingShape as GradientEndingShape, Gradient};
 pub use self::image::{GradientItem, GradientKind, Image, ImageLayer, MozImageRect};

