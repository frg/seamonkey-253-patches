# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1506458203 14400
#      Tue Sep 26 16:36:43 2017 -0400
# Node ID 35460a8966c7e32c45407cb9a2a333d20b120875
# Parent  47c4a2097b273836df104b1de05a5767eba02116
Bug 1403325 - Use a single --sandbox-read-whitelist argument in mochitest and reftest, r=Alex_Gaynor

The main bug this fixes is that on reftest, the objdir needs to be added to the
whitelist on Windows. However, this only happens when running on Linux for some
reason.

Changing the --work-path and --obj-path arguments to --sandbox-read-whitelist
was more of a drive-by cleanup than anything necessary.

MozReview-Commit-ID: Dq8ZLETMzeM

diff --git a/layout/tools/reftest/mach_commands.py b/layout/tools/reftest/mach_commands.py
--- a/layout/tools/reftest/mach_commands.py
+++ b/layout/tools/reftest/mach_commands.py
@@ -71,18 +71,17 @@ class ReftestRunner(MozbuildObject):
             "reftest": (self.topsrcdir, "layout", "reftests", "reftest.list"),
             "crashtest": (self.topsrcdir, "testing", "crashtest", "crashtests.list"),
             "jstestbrowser": (self.topobjdir, "dist", "test-stage", "jsreftest", "tests",
                               "jstests.list")
         }
 
         args.extraProfileFiles.append(os.path.join(self.topobjdir, "dist", "plugins"))
         args.symbolsPath = os.path.join(self.topobjdir, "crashreporter-symbols")
-        args.workPath = self.topsrcdir
-        args.objPath = self.topobjdir
+        args.sandboxReadWhitelist.extend([self.topsrcdir, self.topobjdir])
 
         if not args.tests:
             args.tests = [os.path.join(*default_manifest[args.suite])]
 
         if args.suite == "jstestbrowser":
             args.extraProfileFiles.append(os.path.join(self.topobjdir, "dist",
                                                        "test-stage", "jsreftest",
                                                        "tests", "user.js"))
diff --git a/layout/tools/reftest/reftestcommandline.py b/layout/tools/reftest/reftestcommandline.py
--- a/layout/tools/reftest/reftestcommandline.py
+++ b/layout/tools/reftest/reftestcommandline.py
@@ -234,25 +234,21 @@ class ReftestArgumentsParser(argparse.Ar
                           help="The maximum number of attempts to try and recover from a "
                                "crash before aborting the test run [default 4].")
 
         self.add_argument("tests",
                           metavar="TEST_PATH",
                           nargs="*",
                           help="Path to test file, manifest file, or directory containing tests")
 
-        self.add_argument("--work-path",
-                          action="store",
-                          dest="workPath",
-                          help="Path to the base dir of all source files.")
-
-        self.add_argument("--obj-path",
-                          action="store",
-                          dest="objPath",
-                          help="Path to the base dir of all object files.")
+        self.add_argument("--sandbox-read-whitelist",
+                          action="append",
+                          dest="sandboxReadWhitelist",
+                          default=[],
+                          help="Path to add to the sandbox whitelist.")
 
         self.add_argument("--verify",
                           action="store_true",
                           default=False,
                           help="Test verification mode.")
 
         self.add_argument("--verify-max-time",
                           type=int,
diff --git a/layout/tools/reftest/runreftest.py b/layout/tools/reftest/runreftest.py
--- a/layout/tools/reftest/runreftest.py
+++ b/layout/tools/reftest/runreftest.py
@@ -323,28 +323,17 @@ class RefTest(object):
             prefs['browser.tabs.remote.autostart'] = True
             prefs['extensions.e10sBlocksEnabling'] = False
 
         # Bug 1262954: For winXP + e10s disable acceleration
         if platform.system() in ("Windows", "Microsoft") and \
            '5.1' in platform.version() and options.e10s:
             prefs['layers.acceleration.disabled'] = True
 
-        sandbox_whitelist_paths = [SCRIPT_DIRECTORY]
-        try:
-            if options.workPath:
-                sandbox_whitelist_paths.append(options.workPath)
-        except AttributeError:
-            pass
-        if platform.system() == "Linux":
-            try:
-                if options.objPath:
-                    sandbox_whitelist_paths.append(options.objPath)
-            except AttributeError:
-                pass
+        sandbox_whitelist_paths = [SCRIPT_DIRECTORY] + options.sandboxReadWhitelist
         if (platform.system() == "Linux" or
             platform.system() in ("Windows", "Microsoft")):
             # Trailing slashes are needed to indicate directories on Linux and Windows
             sandbox_whitelist_paths = map(lambda p: os.path.join(p, ""),
                                           sandbox_whitelist_paths)
 
         # Bug 1300355: Disable canvas cache for win7 as it uses
         # too much memory and causes OOMs.
diff --git a/layout/tools/reftest/selftest/conftest.py b/layout/tools/reftest/selftest/conftest.py
--- a/layout/tools/reftest/selftest/conftest.py
+++ b/layout/tools/reftest/selftest/conftest.py
@@ -41,26 +41,24 @@ def runtests(setup_test_harness, binary,
         'suite': 'reftest',
         'specialPowersExtensionPath': os.path.join(harness_root, 'specialpowers'),
     })
 
     if not os.path.isdir(build.bindir):
         package_root = os.path.dirname(harness_root)
         options.update({
             'extraProfileFiles': [os.path.join(package_root, 'bin', 'plugins')],
-            'objPath': os.environ['PYTHON_TEST_TMP'],
             'reftestExtensionPath': os.path.join(harness_root, 'reftest'),
+            'sandboxReadWhitelist': [here, os.environ['PYTHON_TEST_TMP']],
             'utilityPath': os.path.join(package_root, 'bin'),
-            'workPath': here,
         })
     else:
         options.update({
             'extraProfileFiles': [os.path.join(build.topobjdir, 'dist', 'plugins')],
-            'objPath': build.topobjdir,
-            'workPath': build.topsrcdir,
+            'sandboxReadWhitelist': [build.topobjdir, build.topsrcdir],
         })
 
     def normalize(test):
         if os.path.isabs(test):
             return test
         return os.path.join(here, 'files', test)
 
     def inner(*tests, **opts):
diff --git a/testing/mochitest/mochitest_options.py b/testing/mochitest/mochitest_options.py
--- a/testing/mochitest/mochitest_options.py
+++ b/testing/mochitest/mochitest_options.py
@@ -591,26 +591,21 @@ class MochitestArguments(ArgumentContain
           "help": "Port for websocket/process bridge. Default 8191.",
           }],
         [["--failure-pattern-file"],
          {"default": None,
           "dest": "failure_pattern_file",
           "help": "File describes all failure patterns of the tests.",
           "suppress": True,
           }],
-        [["--work-path"],
-         {"default": None,
-          "dest": "workPath",
-          "help": "Path to the base dir of all source files.",
-          "suppress": True,
-          }],
-        [["--obj-path"],
-         {"default": None,
-          "dest": "objPath",
-          "help": "Path to the base dir of all object files.",
+        [["--sandbox-read-whitelist"],
+         {"default": [],
+          "dest": "sandboxReadWhitelist",
+          "action": "append",
+          "help": "Path to add to the sandbox whitelist.",
           "suppress": True,
           }],
         [["--verify"],
          {"action": "store_true",
           "default": False,
           "help": "Test verification mode.",
           "suppress": True,
           }],
diff --git a/testing/mochitest/runtests.py b/testing/mochitest/runtests.py
--- a/testing/mochitest/runtests.py
+++ b/testing/mochitest/runtests.py
@@ -1853,27 +1853,17 @@ toolbar#nav-bar {
 
         prefs.update(self.extraPrefs(options.extraPrefs))
 
         # Bug 1262954: For windows XP + e10s disable acceleration
         if platform.system() in ("Windows", "Microsoft") and \
            '5.1' in platform.version() and options.e10s:
             prefs['layers.acceleration.disabled'] = True
 
-        sandbox_whitelist_paths = [SCRIPT_DIR]
-        try:
-            if options.workPath:
-                sandbox_whitelist_paths.append(options.workPath)
-        except AttributeError:
-            pass
-        try:
-            if options.objPath:
-                sandbox_whitelist_paths.append(options.objPath)
-        except AttributeError:
-            pass
+        sandbox_whitelist_paths = [SCRIPT_DIR] + options.sandboxReadWhitelist
         if (platform.system() == "Linux" or
             platform.system() in ("Windows", "Microsoft")):
             # Trailing slashes are needed to indicate directories on Linux and Windows
             sandbox_whitelist_paths = map(lambda p: os.path.join(p, ""),
                                           sandbox_whitelist_paths)
 
         # interpolate preferences
         interpolation = {
diff --git a/testing/mozharness/configs/unittests/linux_unittest.py b/testing/mozharness/configs/unittests/linux_unittest.py
--- a/testing/mozharness/configs/unittests/linux_unittest.py
+++ b/testing/mozharness/configs/unittests/linux_unittest.py
@@ -113,17 +113,17 @@ config = {
                 "--setpref=webgl.force-enabled=true",
                 "--quiet",
                 "--log-raw=%(raw_log_file)s",
                 "--log-errorsummary=%(error_summary_file)s",
                 "--use-test-media-devices",
                 "--screenshot-on-fail",
                 "--cleanup-crashes",
                 "--marionette-startup-timeout=180",
-                "--work-path=%(abs_work_dir)s",
+                "--sandbox-read-whitelist=%(abs_work_dir)s",
             ],
             "run_filename": "runtests.py",
             "testsdir": "mochitest"
         },
         "mozbase": {
             "options": [
                 "-b",
                 "%(binary_path)s"
@@ -145,17 +145,17 @@ config = {
             "options": [
                 "--appname=%(binary_path)s",
                 "--utility-path=tests/bin",
                 "--extra-profile-file=tests/bin/plugins",
                 "--symbols-path=%(symbols_path)s",
                 "--log-raw=%(raw_log_file)s",
                 "--log-errorsummary=%(error_summary_file)s",
                 "--cleanup-crashes",
-                "--work-path=%(abs_work_dir)s",
+                "--sandbox-read-whitelist=%(abs_work_dir)s",
             ],
             "run_filename": "runreftest.py",
             "testsdir": "reftest"
         },
         "xpcshell": {
             "options": [
                 "--symbols-path=%(symbols_path)s",
                 "--test-plugin-path=%(test_plugin_path)s",
diff --git a/testing/mozharness/configs/unittests/mac_unittest.py b/testing/mozharness/configs/unittests/mac_unittest.py
--- a/testing/mozharness/configs/unittests/mac_unittest.py
+++ b/testing/mozharness/configs/unittests/mac_unittest.py
@@ -81,17 +81,17 @@ config = {
                 "--extra-profile-file=tests/bin/plugins",
                 "--symbols-path=%(symbols_path)s",
                 "--certificate-path=tests/certs",
                 "--quiet",
                 "--log-raw=%(raw_log_file)s",
                 "--log-errorsummary=%(error_summary_file)s",
                 "--screenshot-on-fail",
                 "--cleanup-crashes",
-                "--work-path=%(abs_work_dir)s",
+                "--sandbox-read-whitelist=%(abs_work_dir)s",
             ],
             "run_filename": "runtests.py",
             "testsdir": "mochitest"
         },
         "mozbase": {
             "options": [
                 "-b",
                 "%(binary_path)s"
@@ -113,17 +113,17 @@ config = {
             "options": [
                 "--appname=%(binary_path)s",
                 "--utility-path=tests/bin",
                 "--extra-profile-file=tests/bin/plugins",
                 "--symbols-path=%(symbols_path)s",
                 "--log-raw=%(raw_log_file)s",
                 "--log-errorsummary=%(error_summary_file)s",
                 "--cleanup-crashes",
-                "--work-path=%(abs_work_dir)s",
+                "--sandbox-read-whitelist=%(abs_work_dir)s",
             ],
             "run_filename": "runreftest.py",
             "testsdir": "reftest"
         },
         "xpcshell": {
             "options": [
                 "--symbols-path=%(symbols_path)s",
                 "--test-plugin-path=%(test_plugin_path)s",
