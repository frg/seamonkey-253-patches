# HG changeset patch
# User Moritz Birghan <mbirghan@mozilla.com>
# Date 1563801373 0
# Node ID 3d2e4a55d5b0c375d0bee5c9214fe8e4f9f6bea6
# Parent  2a8905780aafe7adab7c04aa8eae740195fd2a66
Bug 1559520 - Removes nsIX509CertDB::DeveloperImportedRoot r=keeler

Differential Revision: https://phabricator.services.mozilla.com/D38052

diff --git a/security/apps/AppTrustDomain.cpp b/security/apps/AppTrustDomain.cpp
--- a/security/apps/AppTrustDomain.cpp
+++ b/security/apps/AppTrustDomain.cpp
@@ -7,18 +7,16 @@
 #include "AppTrustDomain.h"
 
 #include "MainThreadUtils.h"
 #include "certdb.h"
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/Casting.h"
 #include "mozilla/Preferences.h"
 #include "nsComponentManagerUtils.h"
-#include "nsIFile.h"
-#include "nsIFileStreams.h"
 #include "nsIX509CertDB.h"
 #include "nsNSSCertificate.h"
 #include "nsNetUtil.h"
 #include "mozpkix/pkixnss.h"
 #include "prerror.h"
 
 // Generated by gen_cert_header.py, which gets called by the build system.
 #include "xpcshell.inc"
@@ -28,25 +26,18 @@
 #include "addons-stage.inc"
 // Privileged Package Certificates
 #include "privileged-package-root.inc"
 
 using namespace mozilla::pkix;
 
 extern mozilla::LazyLogModule gPIPNSSLog;
 
-static char kDevImportedDER[] =
-  "network.http.signed-packages.developer-root";
-
 namespace mozilla { namespace psm {
 
-StaticMutex AppTrustDomain::sMutex;
-UniquePtr<unsigned char[]> AppTrustDomain::sDevImportedDERData;
-unsigned int AppTrustDomain::sDevImportedDERLen = 0;
-
 AppTrustDomain::AppTrustDomain(UniqueCERTCertList& certChain, void* pinArg)
   : mCertChain(certChain)
   , mPinArg(pinArg)
 {
 }
 
 nsresult
 AppTrustDomain::SetTrustedRoot(AppTrustedRoot trustedRoot)
@@ -73,60 +64,16 @@ AppTrustDomain::SetTrustedRoot(AppTruste
       trustedDER.len = mozilla::ArrayLength(addonsStageRoot);
       break;
 
     case nsIX509CertDB::PrivilegedPackageRoot:
       trustedDER.data = const_cast<uint8_t*>(privilegedPackageRoot);
       trustedDER.len = mozilla::ArrayLength(privilegedPackageRoot);
       break;
 
-    case nsIX509CertDB::DeveloperImportedRoot: {
-      StaticMutexAutoLock lock(sMutex);
-      if (!sDevImportedDERData) {
-        MOZ_ASSERT(!NS_IsMainThread());
-        nsCOMPtr<nsIFile> file(do_CreateInstance("@mozilla.org/file/local;1"));
-        if (!file) {
-          return NS_ERROR_FAILURE;
-        }
-        nsAutoCString path;
-        Preferences::GetCString(kDevImportedDER, path);
-        nsresult rv = file->InitWithNativePath(path);
-        if (NS_FAILED(rv)) {
-          return rv;
-        }
-
-        nsCOMPtr<nsIInputStream> inputStream;
-        rv = NS_NewLocalFileInputStream(getter_AddRefs(inputStream), file, -1,
-                                        -1, nsIFileInputStream::CLOSE_ON_EOF);
-        if (NS_FAILED(rv)) {
-          return rv;
-        }
-
-        uint64_t length;
-        rv = inputStream->Available(&length);
-        if (NS_FAILED(rv)) {
-          return rv;
-        }
-
-        auto data = MakeUnique<char[]>(length);
-        rv = inputStream->Read(data.get(), length, &sDevImportedDERLen);
-        if (NS_FAILED(rv)) {
-          return rv;
-        }
-
-        MOZ_ASSERT(length == sDevImportedDERLen);
-        sDevImportedDERData.reset(
-          BitwiseCast<unsigned char*, char*>(data.release()));
-      }
-
-      trustedDER.data = sDevImportedDERData.get();
-      trustedDER.len = sDevImportedDERLen;
-      break;
-    }
-
     default:
       return NS_ERROR_INVALID_ARG;
   }
 
   mTrustedRoot.reset(CERT_NewTempCertificate(CERT_GetDefaultCertDB(),
                                              &trustedDER, nullptr, false, true));
   if (!mTrustedRoot) {
     return mozilla::psm::GetXPCOMFromNSSError(PR_GetError());
diff --git a/security/apps/AppTrustDomain.h b/security/apps/AppTrustDomain.h
--- a/security/apps/AppTrustDomain.h
+++ b/security/apps/AppTrustDomain.h
@@ -3,18 +3,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef AppTrustDomain_h
 #define AppTrustDomain_h
 
 #include "mozpkix/pkixtypes.h"
-#include "mozilla/StaticMutex.h"
-#include "mozilla/UniquePtr.h"
 #include "nsDebug.h"
 #include "nsIX509CertDB.h"
 #include "ScopedNSSTypes.h"
 
 namespace mozilla { namespace psm {
 
 class AppTrustDomain final : public mozilla::pkix::TrustDomain
 {
@@ -75,17 +73,13 @@ public:
                            /*out*/ uint8_t* digestBuf,
                            size_t digestBufLen) override;
 
 private:
   /*out*/ UniqueCERTCertList& mCertChain;
   void* mPinArg; // non-owning!
   UniqueCERTCertificate mTrustedRoot;
   UniqueCERTCertificate mAddonsIntermediate;
-
-  static StaticMutex sMutex;
-  static UniquePtr<unsigned char[]> sDevImportedDERData;
-  static unsigned int sDevImportedDERLen;
 };
 
 } } // namespace mozilla::psm
 
 #endif // AppTrustDomain_h
diff --git a/security/manager/ssl/nsIX509CertDB.idl b/security/manager/ssl/nsIX509CertDB.idl
--- a/security/manager/ssl/nsIX509CertDB.idl
+++ b/security/manager/ssl/nsIX509CertDB.idl
@@ -247,24 +247,16 @@ interface nsIX509CertDB : nsISupports {
   // 2 used to be AppMarketplaceProdReviewersRoot.
   // 3 used to be AppMarketplaceDevPublicRoot.
   // 4 used to be AppMarketplaceDevReviewersRoot.
   // 5 used to be AppMarketplaceStageRoot.
   const AppTrustedRoot AppXPCShellRoot = 6;
   const AppTrustedRoot AddonsPublicRoot = 7;
   const AppTrustedRoot AddonsStageRoot = 8;
   const AppTrustedRoot PrivilegedPackageRoot = 9;
-  /*
-   * If DeveloperImportedRoot is set as trusted root, a CA from local file
-   * system will be imported. Only used when preference
-   * "network.http.packaged-apps-developer-mode" is set.
-   * The path of the CA is specified by preference
-   * "network.http.packaged-apps-developer-trusted-root".
-   */
-  const AppTrustedRoot DeveloperImportedRoot = 10;
   [must_use]
   void openSignedAppFileAsync(in AppTrustedRoot trustedRoot,
                               in nsIFile aJarFile,
                               in nsIOpenSignedAppFileCallback callback);
 
   /**
    *  Verifies the signature on a directory representing an unpacked signed
    *  JAR file. To be considered valid, there must be exactly one signature
