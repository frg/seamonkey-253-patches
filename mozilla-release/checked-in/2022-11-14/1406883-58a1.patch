# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1507543726 -7200
# Node ID f430beef5228ce7fe416da5a55c8abe6a6682b55
# Parent  79831d8ee2f68f042d800b34ac8972d3a69dbd0b
Bug 1406883: Make the constructing of MutableHandleValue explicit in CoerceInPlace calls; r=luke, r=jonco

MozReview-Commit-ID: 5Xfc8W9TR6v

diff --git a/js/src/wasm/WasmBuiltins.cpp b/js/src/wasm/WasmBuiltins.cpp
--- a/js/src/wasm/WasmBuiltins.cpp
+++ b/js/src/wasm/WasmBuiltins.cpp
@@ -287,38 +287,44 @@ WasmReportOutOfBounds()
 static void
 WasmReportUnalignedAccess()
 {
     JSContext* cx = TlsContext.get();
     JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr, JSMSG_WASM_UNALIGNED_ACCESS);
 }
 
 static int32_t
-CoerceInPlace_ToInt32(MutableHandleValue val)
+CoerceInPlace_ToInt32(Value* rawVal)
 {
     JSContext* cx = TlsContext.get();
 
     int32_t i32;
-    if (!ToInt32(cx, val, &i32))
+    RootedValue val(cx, *rawVal);
+    if (!ToInt32(cx, val, &i32)) {
+        *rawVal = PoisonedObjectValue(0x42);
         return false;
-    val.set(Int32Value(i32));
+    }
 
+    *rawVal = Int32Value(i32);
     return true;
 }
 
 static int32_t
-CoerceInPlace_ToNumber(MutableHandleValue val)
+CoerceInPlace_ToNumber(Value* rawVal)
 {
     JSContext* cx = TlsContext.get();
 
     double dbl;
-    if (!ToNumber(cx, val, &dbl))
+    RootedValue val(cx, *rawVal);
+    if (!ToNumber(cx, val, &dbl)) {
+        *rawVal = PoisonedObjectValue(0x42);
         return false;
-    val.set(DoubleValue(dbl));
+    }
 
+    *rawVal = DoubleValue(dbl);
     return true;
 }
 
 static int64_t
 DivI64(uint32_t x_hi, uint32_t x_lo, uint32_t y_hi, uint32_t y_lo)
 {
     int64_t x = ((uint64_t)x_hi << 32) + x_lo;
     int64_t y = ((uint64_t)y_hi << 32) + y_lo;
diff --git a/js/src/wasm/WasmStubs.cpp b/js/src/wasm/WasmStubs.cpp
--- a/js/src/wasm/WasmStubs.cpp
+++ b/js/src/wasm/WasmStubs.cpp
@@ -769,16 +769,25 @@ GenerateImportJitExit(MacroAssembler& ma
     masm.loadBaselineOrIonNoArgCheck(callee, callee, nullptr);
 
     Label rejoinBeforeCall;
     masm.bind(&rejoinBeforeCall);
 
     AssertStackAlignment(masm, JitStackAlignment, sizeOfRetAddr);
     masm.callJitNoProfiler(callee);
 
+    // Note that there might be a GC thing in the JSReturnOperand now.
+    // In all the code paths from here:
+    // - either the value is unboxed because it was a primitive and we don't
+    //   need to worry about rooting anymore.
+    // - or the value needs to be rooted, but nothing can cause a GC between
+    //   here and CoerceInPlace, which roots before coercing to a primitive.
+    //   In particular, this is true because wasm::InInterruptibleCode will
+    //   return false when PC is in the jit exit.
+
     // The JIT callee clobbers all registers, including WasmTlsReg and
     // FramePointer, so restore those here. During this sequence of
     // instructions, FP can't be trusted by the profiling frame iterator.
     offsets->untrustedFPStart = masm.currentOffset();
     AssertStackAlignment(masm, JitStackAlignment, sizeOfRetAddr);
 
     masm.loadWasmTlsRegFromFrame();
     masm.moveStackPtrTo(FramePointer);
