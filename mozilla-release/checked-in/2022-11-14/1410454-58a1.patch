# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1508524307 14400
#      Fri Oct 20 14:31:47 2017 -0400
# Node ID a550fea9a6cd8b479aba174826b19b401161ad8d
# Parent  c2d42102cac6648192b79dee7a9224a5d1bf07ce
Bug 1410454 - Fix ImportError regression in mozboot, r=nalexander

This fixes a regression from bug 1408365. Ideally these imports would
be defined at the top of the file, then the py2 linter would have caught
them. Even more ideally, mozboot would have some tests that hit these
code paths.

MozReview-Commit-ID: BWvIwAdUBF4

diff --git a/python/mozboot/mozboot/archlinux.py b/python/mozboot/mozboot/archlinux.py
--- a/python/mozboot/mozboot/archlinux.py
+++ b/python/mozboot/mozboot/archlinux.py
@@ -116,22 +116,22 @@ class ArchlinuxBootstrapper(StyloInstall
             print('Failed to install all packages.  The Android developer '
                   'toolchain requires 32 bit binaries be enabled (see '
                   'https://wiki.archlinux.org/index.php/Android).  You may need to '
                   'manually enable the multilib repository following the instructions '
                   'at https://wiki.archlinux.org/index.php/Multilib.')
             raise e
 
         # 2. Android pieces.
-        import android
+        from mozboot import android
         android.ensure_android('linux', artifact_mode=artifact_mode,
                                no_interactive=self.no_interactive)
 
     def suggest_mobile_android_mozconfig(self, artifact_mode=False):
-        import android
+        from mozboot import android
         android.suggest_mozconfig('linux', artifact_mode=artifact_mode)
 
     def suggest_mobile_android_artifact_mode_mozconfig(self):
         self.suggest_mobile_android_mozconfig(artifact_mode=True)
 
     def _update_package_manager(self):
         self.pacman_update
 
diff --git a/python/mozboot/mozboot/centosfedora.py b/python/mozboot/mozboot/centosfedora.py
--- a/python/mozboot/mozboot/centosfedora.py
+++ b/python/mozboot/mozboot/centosfedora.py
@@ -123,21 +123,21 @@ class CentOSFedoraBootstrapper(StyloInst
                 yasm = 'http://pkgs.repoforge.org/yasm/yasm-1.1.0-1.el6.rf.x86_64.rpm'
 
             self.run_as_root(['rpm', '-ivh', yasm])
 
     def ensure_mobile_android_packages(self, artifact_mode=False):
         # Install Android specific packages.
         self.dnf_install(*self.mobile_android_packages)
 
-        import android
+        from mozboot import android
         android.ensure_android('linux', artifact_mode=artifact_mode,
                                no_interactive=self.no_interactive)
 
     def suggest_mobile_android_mozconfig(self, artifact_mode=False):
-        import android
+        from mozboot import android
         android.suggest_mozconfig('linux', artifact_mode=artifact_mode)
 
     def suggest_mobile_android_artifact_mode_mozconfig(self):
         self.suggest_mobile_android_mozconfig(artifact_mode=True)
 
     def upgrade_mercurial(self, current):
         self.dnf_update('mercurial')
diff --git a/python/mozboot/mozboot/debian.py b/python/mozboot/mozboot/debian.py
--- a/python/mozboot/mozboot/debian.py
+++ b/python/mozboot/mozboot/debian.py
@@ -124,22 +124,22 @@ class DebianBootstrapper(StyloInstall, B
         # "Troubleshooting Ubuntu" at
         # http://developer.android.com/sdk/installing/index.html?pkg=tools.
         self.run_as_root(['dpkg', '--add-architecture', 'i386'])
         # After adding a new arch, the list of packages has to be updated
         self.apt_update()
         self.apt_install(*self.mobile_android_packages)
 
         # 2. Android pieces.
-        import android
+        from mozboot import android
         android.ensure_android('linux', artifact_mode=artifact_mode,
                                no_interactive=self.no_interactive)
 
     def suggest_mobile_android_mozconfig(self, artifact_mode=False):
-        import android
+        from mozboot import android
         android.suggest_mozconfig('linux', artifact_mode=artifact_mode)
 
     def suggest_mobile_android_artifact_mode_mozconfig(self):
         self.suggest_mobile_android_mozconfig(artifact_mode=True)
 
     def _update_package_manager(self):
         self.apt_update()
 
diff --git a/python/mozboot/mozboot/gentoo.py b/python/mozboot/mozboot/gentoo.py
--- a/python/mozboot/mozboot/gentoo.py
+++ b/python/mozboot/mozboot/gentoo.py
@@ -56,22 +56,22 @@ class GentooBootstrapper(StyloInstall, B
 
         # Install the Oracle JDK. We explicitly prompt the user to accept the
         # changes because this command might need to modify the portage
         # configuration files and doing so without user supervision is dangerous
         self.run_as_root(['emerge', '--noreplace', '--quiet',
                           '--autounmask-continue', '--ask',
                           '=dev-java/oracle-jdk-bin-1.8.0.144'])
 
-        import android
+        from mozboot import android
         android.ensure_android('linux', artifact_mode=artifact_mode,
                                no_interactive=self.no_interactive)
 
     def suggest_mobile_android_mozconfig(self, artifact_mode=False):
-        import android
+        from mozboot import android
         android.suggest_mozconfig('linux', artifact_mode=artifact_mode)
 
     def suggest_mobile_android_artifact_mode_mozconfig(self):
         self.suggest_mobile_android_mozconfig(artifact_mode=True)
 
     def _update_package_manager(self):
         self.run_as_root(['emerge', '--sync'])
 
diff --git a/python/mozboot/mozboot/linux_common.py b/python/mozboot/mozboot/linux_common.py
--- a/python/mozboot/mozboot/linux_common.py
+++ b/python/mozboot/mozboot/linux_common.py
@@ -9,10 +9,10 @@
 from __future__ import absolute_import
 
 
 class StyloInstall(object):
     def __init__(self, **kwargs):
         pass
 
     def ensure_stylo_packages(self, state_dir, checkout_root):
-        import stylo
+        from mozboot import stylo
         self.install_tooltool_clang_package(state_dir, checkout_root, stylo.LINUX)
diff --git a/python/mozboot/mozboot/mozillabuild.py b/python/mozboot/mozboot/mozillabuild.py
--- a/python/mozboot/mozboot/mozillabuild.py
+++ b/python/mozboot/mozboot/mozillabuild.py
@@ -40,17 +40,17 @@ class MozillaBuildBootstrapper(BaseBoots
 
     def install_mobile_android_packages(self):
         pass
 
     def install_mobile_android_artifact_mode_packages(self):
         pass
 
     def ensure_stylo_packages(self, state_dir, checkout_root):
-        import stylo
+        from mozboot import stylo
         self.install_tooltool_clang_package(state_dir, checkout_root, stylo.WINDOWS)
 
     def _update_package_manager(self):
         pass
 
     def run(self, command):
         subprocess.check_call(command, stdin=sys.stdin)
 
diff --git a/python/mozboot/mozboot/osx.py b/python/mozboot/mozboot/osx.py
--- a/python/mozboot/mozboot/osx.py
+++ b/python/mozboot/mozboot/osx.py
@@ -353,22 +353,22 @@ class OSXBootstrapper(BaseBootstrapper):
         if installed:
             print(JAVA_LICENSE_NOTICE)  # We accepted a license agreement for the user.
 
         is_64bits = sys.maxsize > 2**32
         if not is_64bits:
             raise Exception('You need a 64-bit version of Mac OS X to build Firefox for Android.')
 
         # 2. Android pieces.
-        import android
+        from mozboot import android
         android.ensure_android('macosx', artifact_mode=artifact_mode,
                                no_interactive=self.no_interactive)
 
     def suggest_homebrew_mobile_android_mozconfig(self, artifact_mode=False):
-        import android
+        from mozboot import android
         android.suggest_mozconfig('macosx', artifact_mode=artifact_mode)
 
     def _ensure_macports_packages(self, packages):
         self.port = self.which('port')
         assert self.port is not None
 
         installed = set(self.check_output([self.port, 'installed']).split())
 
@@ -418,22 +418,22 @@ class OSXBootstrapper(BaseBootstrapper):
                             'Please visit http://www.java.com/en/download/mac_download.jsp '
                             'to get the latest version.')
 
         is_64bits = sys.maxsize > 2**32
         if not is_64bits:
             raise Exception('You need a 64-bit version of Mac OS X to build Firefox for Android.')
 
         # 2. Android pieces.
-        import android
+        from mozboot import android
         android.ensure_android('macosx', artifact_mode=artifact_mode,
                                no_interactive=self.no_interactive)
 
     def suggest_macports_mobile_android_mozconfig(self, artifact_mode=False):
-        import android
+        from mozboot import android
         android.suggest_mozconfig('macosx', artifact_mode=artifact_mode)
 
     def ensure_package_manager(self):
         '''
         Search package mgr in sys.path, if none is found, prompt the user to install one.
         If only one is found, use that one. If both are found, prompt the user to choose
         one.
         '''
diff --git a/python/mozboot/mozboot/windows.py b/python/mozboot/mozboot/windows.py
--- a/python/mozboot/mozboot/windows.py
+++ b/python/mozboot/mozboot/windows.py
@@ -68,17 +68,17 @@ class WindowsBootstrapper(BaseBootstrapp
 
     def install_mobile_android_packages(self):
         raise NotImplementedError('We do not support building Android on Windows. Sorry!')
 
     def install_mobile_android_artifact_mode_packages(self):
         raise NotImplementedError('We do not support building Android on Windows. Sorry!')
 
     def ensure_stylo_packages(self, state_dir, checkout_root):
-        import stylo
+        from mozboot import stylo
         self.install_tooltool_clang_package(state_dir, checkout_root, stylo.WINDOWS)
 
     def _update_package_manager(self):
         self.pacman_update()
 
     def run(self, command):
         subprocess.check_call(command, stdin=sys.stdin)
 
