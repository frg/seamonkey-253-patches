# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1507726471 -7200
#      Wed Oct 11 14:54:31 2017 +0200
# Node ID 817d7ec24cb64275dd3c31ed29307c85407d8acb
# Parent  4fae57b0c64e0e9511f58c8188bdd3e37e7f5624
Bug 1406957 part 2 - Rewrite this-creation to be simpler and more consistent. r=tcampbell

diff --git a/js/src/jit/BaselineJIT.cpp b/js/src/jit/BaselineJIT.cpp
--- a/js/src/jit/BaselineJIT.cpp
+++ b/js/src/jit/BaselineJIT.cpp
@@ -365,29 +365,20 @@ jit::CanEnterBaselineAtBranch(JSContext*
    return CanEnterBaselineJIT(cx, script, fp);
 }
 
 MethodStatus
 jit::CanEnterBaselineMethod(JSContext* cx, RunState& state)
 {
     if (state.isInvoke()) {
         InvokeState& invoke = *state.asInvoke();
-
         if (invoke.args().length() > BASELINE_MAX_ARGS_LENGTH) {
             JitSpew(JitSpew_BaselineAbort, "Too many arguments (%u)", invoke.args().length());
             return Method_CantCompile;
         }
-
-        if (!state.maybeCreateThisForConstructor(cx)) {
-            if (cx->isThrowingOutOfMemory()) {
-                cx->recoverFromOutOfMemory();
-                return Method_Skipped;
-            }
-            return Method_Error;
-        }
     } else {
         if (state.asExecute()->isDebuggerEval()) {
             JitSpew(JitSpew_BaselineAbort, "debugger frame");
             return Method_CantCompile;
         }
     }
 
     RootedScript script(cx, state.script());
diff --git a/js/src/jit/Ion.cpp b/js/src/jit/Ion.cpp
--- a/js/src/jit/Ion.cpp
+++ b/js/src/jit/Ion.cpp
@@ -2510,39 +2510,28 @@ jit::CanEnter(JSContext* cx, RunState& s
             return Method_CantCompile;
         }
 
         if (TooManyFormalArguments(invoke.args().callee().as<JSFunction>().nargs())) {
             TrackAndSpewIonAbort(cx, script, "too many args");
             ForbidCompilation(cx, script);
             return Method_CantCompile;
         }
-
-        if (!state.maybeCreateThisForConstructor(cx)) {
-            if (cx->isThrowingOutOfMemory()) {
-                cx->recoverFromOutOfMemory();
-                return Method_Skipped;
-            }
-            return Method_Error;
-        }
     }
 
     // If --ion-eager is used, compile with Baseline first, so that we
     // can directly enter IonMonkey.
     if (JitOptions.eagerCompilation && !script->hasBaselineScript()) {
         MethodStatus status = CanEnterBaselineMethod(cx, state);
         if (status != Method_Compiled)
             return status;
     }
 
-    // Skip if the script is being compiled off thread or can't be
-    // Ion-compiled (again). MaybeCreateThisForConstructor could have
-    // started an Ion compilation or marked the script as uncompilable.
-    if (script->isIonCompilingOffThread() || !script->canIonCompile())
-        return Method_Skipped;
+    MOZ_ASSERT(!script->isIonCompilingOffThread());
+    MOZ_ASSERT(script->canIonCompile());
 
     // Attempt compilation. Returns Method_Compiled if already compiled.
     MethodStatus status = Compile(cx, script, nullptr, nullptr);
     if (status != Method_Compiled) {
         if (status == Method_CantCompile)
             ForbidCompilation(cx, script);
         return status;
     }
diff --git a/js/src/vm/Interpreter.cpp b/js/src/vm/Interpreter.cpp
--- a/js/src/vm/Interpreter.cpp
+++ b/js/src/vm/Interpreter.cpp
@@ -326,39 +326,45 @@ js::ValueToCallable(JSContext* cx, Handl
     if (v.isObject() && v.toObject().isCallable()) {
         return &v.toObject();
     }
 
     ReportIsNotFunction(cx, v, numToSkip, construct);
     return nullptr;
 }
 
-bool
-RunState::maybeCreateThisForConstructor(JSContext* cx)
-{
-    if (isInvoke()) {
-        InvokeState& invoke = *asInvoke();
-        if (invoke.constructing() && invoke.args().thisv().isPrimitive()) {
-            RootedObject callee(cx, &invoke.args().callee());
-            if (callee->isBoundFunction()) {
-                invoke.args().setThis(MagicValue(JS_UNINITIALIZED_LEXICAL));
-            } else if (script()->isDerivedClassConstructor()) {
-                MOZ_ASSERT(callee->as<JSFunction>().isClassConstructor());
-                invoke.args().setThis(MagicValue(JS_UNINITIALIZED_LEXICAL));
-            } else {
-                MOZ_ASSERT(invoke.args().thisv().isMagic(JS_IS_CONSTRUCTING));
-                RootedObject newTarget(cx, &invoke.args().newTarget().toObject());
-                NewObjectKind newKind = invoke.createSingleton() ? SingletonObject : GenericObject;
-                JSObject* obj = CreateThisForFunction(cx, callee, newTarget, newKind);
-                if (!obj)
-                    return false;
-                invoke.args().setThis(ObjectValue(*obj));
-            }
-        }
+static bool
+MaybeCreateThisForConstructor(JSContext* cx, JSScript* calleeScript, const CallArgs& args,
+                              bool createSingleton)
+{
+    if (args.thisv().isObject())
+        return true;
+
+    RootedObject callee(cx, &args.callee());
+    if (callee->isBoundFunction()) {
+        args.setThis(MagicValue(JS_UNINITIALIZED_LEXICAL));
+        return true;
     }
+
+    if (calleeScript->isDerivedClassConstructor()) {
+        MOZ_ASSERT(callee->as<JSFunction>().isClassConstructor());
+        args.setThis(MagicValue(JS_UNINITIALIZED_LEXICAL));
+        return true;
+    }
+
+    MOZ_ASSERT(args.thisv().isMagic(JS_IS_CONSTRUCTING));
+
+    RootedObject newTarget(cx, &args.newTarget().toObject());
+    NewObjectKind newKind = createSingleton ? SingletonObject : GenericObject;
+
+    JSObject* obj = CreateThisForFunction(cx, callee, newTarget, newKind);
+    if (!obj)
+        return false;
+
+    args.setThis(ObjectValue(*obj));
     return true;
 }
 
 static MOZ_NEVER_INLINE bool
 Interpret(JSContext* cx, RunState& state);
 
 InterpreterFrame*
 InvokeState::pushInterpreterFrame(JSContext* cx)
@@ -495,21 +501,25 @@ js::InternalCallOrConstruct(JSContext* c
     if (!JSFunction::getOrCreateScript(cx, fun))
         return false;
 
     /* Run function until JSOP_RETRVAL, JSOP_RETURN or error. */
     InvokeState state(cx, args, construct);
 
     // Check to see if createSingleton flag should be set for this frame.
     if (construct) {
+        bool createSingleton = false;
         jsbytecode* pc;
         if (JSScript* script = cx->currentScript(&pc)) {
             if (ObjectGroup::useSingletonForNewObject(cx, script, pc))
-                state.setCreateSingleton();
+                createSingleton = true;
         }
+
+        if (!MaybeCreateThisForConstructor(cx, state.script(), args, createSingleton))
+            return false;
     }
 
     bool ok = RunScript(cx, state);
 
     MOZ_ASSERT_IF(ok && construct, args.rval().isObject());
     return ok;
 }
 
@@ -3128,26 +3138,29 @@ CASE(JSOP_FUNCALL)
 
     {
         MOZ_ASSERT(maybeFun);
         ReservedRooted<JSFunction*> fun(&rootFunction0, maybeFun);
         ReservedRooted<JSScript*> funScript(&rootScript0, JSFunction::getOrCreateScript(cx, fun));
         if (!funScript)
             goto error;
 
-        bool createSingleton = ObjectGroup::useSingletonForNewObject(cx, script, REGS.pc);
+        bool createSingleton = false;
+        if (construct) {
+            createSingleton = ObjectGroup::useSingletonForNewObject(cx, script, REGS.pc);
+
+            if (!MaybeCreateThisForConstructor(cx, funScript, args, createSingleton))
+                goto error;
+        }
 
         TypeMonitorCall(cx, args, construct);
 
         {
             InvokeState state(cx, args, construct);
 
-            if (createSingleton)
-                state.setCreateSingleton();
-
             if (!createSingleton && jit::IsIonEnabled(cx)) {
                 jit::MethodStatus status = jit::CanEnter(cx, state);
                 if (status == jit::Method_Error)
                     goto error;
                 if (status == jit::Method_Compiled) {
                     jit::JitExecStatus exec = jit::IonCannon(cx, state);
                     interpReturnOK = !IsErrorStatus(exec);
                     if (interpReturnOK)
diff --git a/js/src/vm/Interpreter.h b/js/src/vm/Interpreter.h
--- a/js/src/vm/Interpreter.h
+++ b/js/src/vm/Interpreter.h
@@ -228,18 +228,16 @@ class RunState
         return (InvokeState*)this;
     }
 
     JS::HandleScript script() const { return script_; }
 
     virtual InterpreterFrame* pushInterpreterFrame(JSContext* cx) = 0;
     virtual void setReturnValue(const Value& v) = 0;
 
-    bool maybeCreateThisForConstructor(JSContext* cx);
-
   private:
     RunState(const RunState& other) = delete;
     RunState(const ExecuteState& other) = delete;
     RunState(const InvokeState& other) = delete;
     void operator=(const RunState& other) = delete;
 };
 
 // Eval or global script.
@@ -276,29 +274,24 @@ class ExecuteState : public RunState
     }
 };
 
 // Data to invoke a function.
 class InvokeState final : public RunState
 {
     const CallArgs& args_;
     MaybeConstruct construct_;
-    bool createSingleton_;
 
   public:
     InvokeState(JSContext* cx, const CallArgs& args, MaybeConstruct construct)
       : RunState(cx, Invoke, args.callee().as<JSFunction>().nonLazyScript()),
         args_(args),
-        construct_(construct),
-        createSingleton_(false)
+        construct_(construct)
     { }
 
-    bool createSingleton() const { return createSingleton_; }
-    void setCreateSingleton() { createSingleton_ = true; }
-
     bool constructing() const { return construct_; }
     const CallArgs& args() const { return args_; }
 
     virtual InterpreterFrame* pushInterpreterFrame(JSContext* cx);
 
     virtual void setReturnValue(const Value& v) {
         args_.rval().set(v);
     }
diff --git a/js/src/vm/Stack.cpp b/js/src/vm/Stack.cpp
--- a/js/src/vm/Stack.cpp
+++ b/js/src/vm/Stack.cpp
@@ -238,35 +238,18 @@ InterpreterFrame::prologue(JSContext* cx
     // At this point, we've yet to push any environments. Check that they
     // match the enclosing scope.
     AssertScopeMatchesEnvironment(script->enclosingScope(), environmentChain());
 
     MOZ_ASSERT(isFunctionFrame());
     if (callee().needsFunctionEnvironmentObjects() && !initFunctionEnvironmentObjects(cx))
         return false;
 
-    if (isConstructing()) {
-        if (callee().isBoundFunction()) {
-            thisArgument() = MagicValue(JS_UNINITIALIZED_LEXICAL);
-        } else if (script->isDerivedClassConstructor()) {
-            MOZ_ASSERT(callee().isClassConstructor());
-            thisArgument() = MagicValue(JS_UNINITIALIZED_LEXICAL);
-        } else if (thisArgument().isObject()) {
-            // Nothing to do. Correctly set.
-        } else {
-            MOZ_ASSERT(thisArgument().isMagic(JS_IS_CONSTRUCTING));
-            RootedObject callee(cx, &this->callee());
-            RootedObject newTarget(cx, &this->newTarget().toObject());
-            JSObject* obj = CreateThisForFunction(cx, callee, newTarget,
-                                                  createSingleton() ? SingletonObject : GenericObject);
-            if (!obj)
-                return false;
-            thisArgument() = ObjectValue(*obj);
-        }
-    }
+    MOZ_ASSERT_IF(isConstructing(),
+                  thisArgument().isObject() || thisArgument().isMagic(JS_UNINITIALIZED_LEXICAL));
 
     return probes::EnterScript(cx, script, script->functionNonDelazifying(), this);
 }
 
 void
 InterpreterFrame::epilogue(JSContext* cx, jsbytecode* pc)
 {
     RootedScript script(cx, this->script());
