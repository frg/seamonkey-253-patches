# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1506500424 -7200
# Node ID e7403204c8fa1353a4bdb4431362895781112eb8
# Parent  253162b29c3a0e202dd85aed9407ac54e4e60ef3
Bug 1389913 - IDB should not propagate the error events to self.onerror, r=bevis

diff --git a/dom/indexedDB/IndexedDatabaseManager.cpp b/dom/indexedDB/IndexedDatabaseManager.cpp
--- a/dom/indexedDB/IndexedDatabaseManager.cpp
+++ b/dom/indexedDB/IndexedDatabaseManager.cpp
@@ -134,16 +134,17 @@ const int32_t kDefaultMaxSerializedMsgSi
 
 #define IDB_PREF_BRANCH_ROOT "dom.indexedDB."
 
 const char kTestingPref[] = IDB_PREF_BRANCH_ROOT "testing";
 const char kPrefExperimental[] = IDB_PREF_BRANCH_ROOT "experimental";
 const char kPrefFileHandle[] = "dom.fileHandle.enabled";
 const char kDataThresholdPref[] = IDB_PREF_BRANCH_ROOT "dataThreshold";
 const char kPrefMaxSerilizedMsgSize[] = IDB_PREF_BRANCH_ROOT "maxSerializedMsgSize";
+const char kPrefErrorEventToSelfError[] = IDB_PREF_BRANCH_ROOT "errorEventToSelfError";
 
 #define IDB_PREF_LOGGING_BRANCH_ROOT IDB_PREF_BRANCH_ROOT "logging."
 
 const char kPrefLoggingEnabled[] = IDB_PREF_LOGGING_BRANCH_ROOT "enabled";
 const char kPrefLoggingDetails[] = IDB_PREF_LOGGING_BRANCH_ROOT "details";
 
 #if defined(DEBUG) || defined(MOZ_GECKO_PROFILER)
 const char kPrefLoggingProfiler[] =
@@ -155,16 +156,17 @@ const char kPrefLoggingProfiler[] =
 
 StaticRefPtr<IndexedDatabaseManager> gDBManager;
 
 Atomic<bool> gInitialized(false);
 Atomic<bool> gClosed(false);
 Atomic<bool> gTestingMode(false);
 Atomic<bool> gExperimentalFeaturesEnabled(false);
 Atomic<bool> gFileHandleEnabled(false);
+Atomic<bool> gPrefErrorEventToSelfError(false);
 Atomic<int32_t> gDataThresholdBytes(0);
 Atomic<int32_t> gMaxSerializedMsgSize(0);
 
 class DeleteFilesRunnable final
   : public nsIRunnable
   , public OpenDirectoryListener
 {
   typedef mozilla::dom::quota::DirectoryLock DirectoryLock;
@@ -364,16 +366,19 @@ IndexedDatabaseManager::Init()
                                        kTestingPref,
                                        &gTestingMode);
   Preferences::RegisterCallbackAndCall(AtomicBoolPrefChangedCallback,
                                        kPrefExperimental,
                                        &gExperimentalFeaturesEnabled);
   Preferences::RegisterCallbackAndCall(AtomicBoolPrefChangedCallback,
                                        kPrefFileHandle,
                                        &gFileHandleEnabled);
+  Preferences::RegisterCallbackAndCall(AtomicBoolPrefChangedCallback,
+                                       kPrefErrorEventToSelfError,
+                                       &gPrefErrorEventToSelfError);
 
   // By default IndexedDB uses SQLite with PRAGMA synchronous = NORMAL. This
   // guarantees (unlike synchronous = OFF) atomicity and consistency, but not
   // necessarily durability in situations such as power loss. This preference
   // allows enabling PRAGMA synchronous = FULL on SQLite, which does guarantee
   // durability, but with an extra fsync() and the corresponding performance
   // hit.
   sFullSynchronousMode = Preferences::GetBool("dom.indexedDB.fullSynchronous");
@@ -436,16 +441,19 @@ IndexedDatabaseManager::Destroy()
                                   kTestingPref,
                                   &gTestingMode);
   Preferences::UnregisterCallback(AtomicBoolPrefChangedCallback,
                                   kPrefExperimental,
                                   &gExperimentalFeaturesEnabled);
   Preferences::UnregisterCallback(AtomicBoolPrefChangedCallback,
                                   kPrefFileHandle,
                                   &gFileHandleEnabled);
+  Preferences::UnregisterCallback(AtomicBoolPrefChangedCallback,
+                                  kPrefErrorEventToSelfError,
+                                  &gPrefErrorEventToSelfError);
 
   Preferences::UnregisterCallback(LoggingModePrefChangedCallback,
                                   kPrefLoggingDetails);
 #ifdef MOZ_GECKO_PROFILER
   Preferences::UnregisterCallback(LoggingModePrefChangedCallback,
                                   kPrefLoggingProfiler);
 #endif
   Preferences::UnregisterCallback(LoggingModePrefChangedCallback,
@@ -463,16 +471,20 @@ IndexedDatabaseManager::Destroy()
 // static
 nsresult
 IndexedDatabaseManager::CommonPostHandleEvent(EventChainPostVisitor& aVisitor,
                                               IDBFactory* aFactory)
 {
   MOZ_ASSERT(aVisitor.mDOMEvent);
   MOZ_ASSERT(aFactory);
 
+  if (!gPrefErrorEventToSelfError) {
+    return NS_OK;
+  }
+
   if (aVisitor.mEventStatus == nsEventStatus_eConsumeNoDefault) {
     return NS_OK;
   }
 
   Event* internalEvent = aVisitor.mDOMEvent->InternalDOMEvent();
   MOZ_ASSERT(internalEvent);
 
   if (!internalEvent->IsTrusted()) {
diff --git a/dom/indexedDB/test/event_propagation_iframe.html b/dom/indexedDB/test/event_propagation_iframe.html
--- a/dom/indexedDB/test/event_propagation_iframe.html
+++ b/dom/indexedDB/test/event_propagation_iframe.html
@@ -21,73 +21,59 @@
     }
 
     function errorHandler(event) {
       ok(false, "indexedDB error, code " + event.target.error.name);
       finishTest();
     }
 
     function finishTest() {
-      // Let window.onerror have a chance to fire
-      setTimeout(function() {
-        setTimeout(function() {
-          testGenerator.return();
-          ok(windowErrorCount == 1, "Good window.onerror count");
-          window.parent.postMessage("SimpleTest.finish();", "*");
-        }, 0);
-      }, 0);
+      testGenerator.return();
+      window.parent.postMessage("SimpleTest.finish();", "*");
     }
 
     const eventChain = [
       "IDBRequest",
       "IDBTransaction",
       "IDBDatabase"
     ];
 
     let captureCount = 0;
     let bubbleCount = 0;
     let atTargetCount = 0;
-    let windowErrorCount = 0;
-
-    window.onerror = function(event) {
-      ok(!windowErrorCount++, "Correct number of window.onerror events");
-      setTimeout(function() { testGenerator.next(); }, 0);
-    };
 
     function errorEventCounter(event) {
       ok(event.type == "error", "Got an error event");
       ok(event.target instanceof window[eventChain[0]],
          "Correct event.target");
 
       let constructor;
       if (event.eventPhase == event.AT_TARGET) {
         atTargetCount++;
         constructor = eventChain[0];
       }
       else if (event.eventPhase == event.CAPTURING_PHASE) {
         constructor = eventChain[eventChain.length - 1 - captureCount++];
       }
       else if (event.eventPhase == event.BUBBLING_PHASE) {
         constructor = eventChain[++bubbleCount];
-        if (windowErrorCount && bubbleCount == eventChain.length - 1) {
+        if (bubbleCount == eventChain.length - 1) {
           event.preventDefault();
         }
       }
       ok(event.currentTarget instanceof window[constructor],
          "Correct event.currentTarget");
 
       if (bubbleCount == eventChain.length - 1) {
         ok(bubbleCount == captureCount,
            "Got same number of calls for both phases");
         ok(atTargetCount == 1, "Got one atTarget event");
 
         captureCount = bubbleCount = atTargetCount = 0;
-        if (windowErrorCount) {
-          finishTest();
-        }
+        finishTest();
       }
     }
 
     function* testSteps() {
       window.parent.SpecialPowers.addPermission("indexedDB", true, document);
 
       let request = indexedDB.open(window.location.pathname, 1);
       request.onerror = errorHandler;
diff --git a/dom/indexedDB/test/unit/test_transaction_error.js b/dom/indexedDB/test/unit/test_transaction_error.js
--- a/dom/indexedDB/test/unit/test_transaction_error.js
+++ b/dom/indexedDB/test/unit/test_transaction_error.js
@@ -83,33 +83,16 @@ function* testSteps() {
   transaction = db.transaction(objectStoreName, "readwrite");
   transaction.onerror = grabEventAndContinueHandler;
   transaction.onabort = grabEventAndContinueHandler;
 
   objectStore = transaction.objectStore(objectStoreName);
 
   info("Adding duplicate entry without preventDefault()");
 
-  if ("SimpleTest" in this) {
-    /* global SimpleTest */
-    SimpleTest.expectUncaughtException();
-  } else if ("DedicatedWorkerGlobalScope" in self &&
-             self instanceof DedicatedWorkerGlobalScope) {
-    let oldErrorFunction = self.onerror;
-    self.onerror = function(message, file, line) {
-      self.onerror = oldErrorFunction;
-      oldErrorFunction = null;
-
-      is(message,
-        "ConstraintError",
-        "Got expected ConstraintError on DedicatedWorkerGlobalScope");
-      return true;
-    };
-  }
-
   request = objectStore.add(data, dataKey);
   request.onsuccess = unexpectedSuccessHandler;
   request.onerror = grabEventAndContinueHandler;
   event = yield undefined;
 
   is(event.type, "error", "Got an error event");
   is(event.target, request, "Error event targeted request");
   is(event.currentTarget, request, "Got request error first");
