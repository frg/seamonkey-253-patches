# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1508419976 -3600
# Node ID 65e2ad9a6e302ac46df7fc9b0dd2ed54cd6878c1
# Parent  a3e747cce4f21b760d005c9879f0dd7ccf48d129
Bug 1410069 - Mention Bugzilla component in contribution advice. r=whimboo

DONTBUILD

MozReview-Commit-ID: F600Wp413Y2

diff --git a/testing/geckodriver/CONTRIBUTING.md b/testing/geckodriver/CONTRIBUTING.md
--- a/testing/geckodriver/CONTRIBUTING.md
+++ b/testing/geckodriver/CONTRIBUTING.md
@@ -62,45 +62,50 @@ release patches for older versions.  It 
 the tip-of-tree geckodriver binary, or failing this, the latest
 release when verifying the problem.  Similarly, as noted in the
 [README], geckodriver is only compatible with the current release
 channel versions of Firefox, and it consequently does not help
 to report bugs that affect outdated and unsupported Firefoxen.
 Please always try to verify the issue in the latest Firefox Nightly
 before you file your bug.
 
+Once we are satisfied the issue raised is of sufficiently actionable
+character, we will continue with triaging it and file a bug where it
+is appropriate.  Bugs specific to geckodriver will be filed in the
+[`Testing :: geckodriver`] component in Bugzilla.
+
 [mailing list]: #communication
 [trace-level log]: doc/TraceLogs.md
 [GitHub issue tracker]: https://github.com/mozilla/geckodriver/issues
 [README]: ./README.md
+[`Testing :: geckodriver`]: https://bugzilla.mozilla.org/buglist.cgi?component=geckodriver
 
 
 Writing code
 ============
 
 Because there are many moving parts involved remote controlling
 a web browser, it can be challenging to a new contributor to know
 where to start.  Please don’t hesitate to [ask questions]!
 
 The canonical source code repository of geckodriver is now
 [mozilla-central].  We continue to use the [GitHub issue tracker] as
 a triage ground before actual, actionable bugs and tasks are filed
-in [Bugzilla].  We also have a curated set of [good first bugs]
-you may consider attempting first.
+in the [`Testing :: geckodriver`] component in Bugzilla.  We also
+have a curated set of [good first bugs] you may consider attempting first.
 
 The purpose of this guide _is not_ to make sure you have a basic
 development environment set up.  For that there is plentiful
 documentation, such as the [Developer Guide] to get you rolling.
 Once you do, we can get started working up your first patch!
 Remember to [reach out to us] at any point if you have questions.
 
 [ask questions]: #communication
 [reach out to us]: #communication
 [mozilla-central]: https://searchfox.org/mozilla-central/source/testing/geckodriver/
-[Bugzilla]: https://bugzilla.mozilla.org/buglist.cgi?cmdtype=runnamed&namedcmd=geckodriver&list_id=13825733
 [good first bugs]: https://www.joshmatthews.net/bugsahoy/?automation=1&rust=1
 [Developer Guide]: https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide
 
 
 Building geckodriver
 --------------------
 
 geckodriver is written in [Rust], a systems programming language

