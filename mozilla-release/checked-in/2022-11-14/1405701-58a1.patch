# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1507396594 -7200
# Node ID 8367a42358e2c6f8f89185ec02a1c0337d8709a9
# Parent  0b666c61212fdb9faac3c63729fd4b980d363606
Bug 1405701 - Fix new.target in Baseline OSR eval frames. r=tcampbell

diff --git a/js/src/jit-test/tests/baseline/eval-newtarget-osr.js b/js/src/jit-test/tests/baseline/eval-newtarget-osr.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/baseline/eval-newtarget-osr.js
@@ -0,0 +1,5 @@
+function f(expected) {
+    eval("for (var i = 0; i < 30; i++) assertEq(new.target, expected)");
+}
+new f(f);
+f(undefined);
diff --git a/js/src/jit/BaselineJIT.cpp b/js/src/jit/BaselineJIT.cpp
--- a/js/src/jit/BaselineJIT.cpp
+++ b/js/src/jit/BaselineJIT.cpp
@@ -219,52 +219,43 @@ jit::EnterBaselineAtBranch(JSContext* cx
 
     // Skip debug breakpoint/trap handler, the interpreter already handled it
     // for the current op.
     if (fp->isDebuggee()) {
         MOZ_RELEASE_ASSERT(baseline->hasDebugInstrumentation());
         data.jitcode += MacroAssembler::ToggledCallSize(data.jitcode);
     }
 
+    // Note: keep this in sync with SetEnterJitData.
+
     data.osrFrame = fp;
     data.osrNumStackValues = fp->script()->nfixed() + cx->interpreterRegs().stackDepth();
 
-    AutoValueVector vals(cx);
-    RootedValue thisv(cx);
+    RootedValue newTarget(cx);
 
     if (fp->isFunctionFrame()) {
         data.constructing = fp->isConstructing();
         data.numActualArgs = fp->numActualArgs();
         data.maxArgc = Max(fp->numActualArgs(), fp->numFormalArgs()) + 1; // +1 = include |this|
         data.maxArgv = fp->argv() - 1; // -1 = include |this|
         data.envChain = nullptr;
         data.calleeToken = CalleeToToken(&fp->callee(), data.constructing);
     } else {
-        thisv.setUndefined();
         data.constructing = false;
         data.numActualArgs = 0;
-        data.maxArgc = 1;
-        data.maxArgv = thisv.address();
+        data.maxArgc = 0;
+        data.maxArgv = nullptr;
         data.envChain = fp->environmentChain();
 
         data.calleeToken = CalleeToToken(fp->script());
 
         if (fp->isEvalFrame()) {
-            if (!vals.reserve(2))
-                return JitExec_Aborted;
-
-            vals.infallibleAppend(thisv);
-
-            if (fp->script()->isDirectEvalInFunction())
-                vals.infallibleAppend(fp->newTarget());
-            else
-                vals.infallibleAppend(NullValue());
-
-            data.maxArgc = 2;
-            data.maxArgv = vals.begin();
+            newTarget = fp->newTarget();
+            data.maxArgc = 1;
+            data.maxArgv = newTarget.address();
         }
     }
 
     TraceLoggerThread* logger = TraceLoggerForCurrentThread(cx);
     TraceLogStopEvent(logger, TraceLogger_Interpreter);
     TraceLogStartEvent(logger, TraceLogger_Baseline);
 
     JitExecStatus status = EnterBaseline(cx, data);
diff --git a/js/src/jit/Ion.cpp b/js/src/jit/Ion.cpp
--- a/js/src/jit/Ion.cpp
+++ b/js/src/jit/Ion.cpp
@@ -2818,27 +2818,29 @@ EnterIon(JSContext* cx, EnterJitData& da
 }
 
 bool
 jit::SetEnterJitData(JSContext* cx, EnterJitData& data, RunState& state,
                      MutableHandle<GCVector<Value>> vals)
 {
     data.osrFrame = nullptr;
 
+    // Note: keep this in sync with EnterBaselineAtBranch.
+
     if (state.isInvoke()) {
         const CallArgs& args = state.asInvoke()->args();
         unsigned numFormals = state.script()->functionNonDelazifying()->nargs();
         data.constructing = state.asInvoke()->constructing();
         data.numActualArgs = args.length();
         data.maxArgc = Max(args.length(), numFormals) + 1;
         data.envChain = nullptr;
         data.calleeToken = CalleeToToken(&args.callee().as<JSFunction>(), data.constructing);
 
         if (data.numActualArgs >= numFormals) {
-            data.maxArgv = args.base() + 1;
+            data.maxArgv = args.array() - 1; // -1 to include |this|
         } else {
             MOZ_ASSERT(vals.empty());
             unsigned numPushedArgs = Max(args.length(), numFormals);
             if (!vals.reserve(numPushedArgs + 1 + data.constructing))
                 return false;
 
             // Append |this| and any provided arguments.
             for (size_t i = 1; i < args.length() + 2; ++i)
