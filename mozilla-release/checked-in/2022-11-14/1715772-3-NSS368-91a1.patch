# HG changeset patch
# User Benjamin Beurdouche <bbeurdouche@mozilla.com>
# Date 1626026546 0
#      Sun Jul 11 18:02:26 2021 +0000
# Node ID 877be9f91d38f4166c9a7363cd176f387456916b
# Parent  9ca3637f94644d379f2276ccfab6bc16737b9a36
Bug 1715772 - land NSS NSS_3_68_RTM UPGRADE_NSS_RELEASE, r=beurdouche

Differential Revision: https://phabricator.services.mozilla.com/D119577

diff --git a/security/nss/TAG-INFO b/security/nss/TAG-INFO
--- a/security/nss/TAG-INFO
+++ b/security/nss/TAG-INFO
@@ -1,1 +1,1 @@
-NSS_3_68_BETA1
\ No newline at end of file
+NSS_3_68_RTM
\ No newline at end of file
diff --git a/security/nss/coreconf/coreconf.dep b/security/nss/coreconf/coreconf.dep
--- a/security/nss/coreconf/coreconf.dep
+++ b/security/nss/coreconf/coreconf.dep
@@ -5,8 +5,9 @@
 
 /*
  * A dummy header file that is a dependency for all the object files.
  * Used to force a full recompilation of NSS in Mozilla's Tinderbox
  * depend builds.  See comments in rules.mk.
  */
 
 #error "Do not include this header file."
+
diff --git a/security/nss/gtests/ssl_gtest/tls_connect.cc b/security/nss/gtests/ssl_gtest/tls_connect.cc
--- a/security/nss/gtests/ssl_gtest/tls_connect.cc
+++ b/security/nss/gtests/ssl_gtest/tls_connect.cc
@@ -243,48 +243,48 @@ void TlsConnectTestBase::Init() {
 void TlsConnectTestBase::ResetAntiReplay(PRTime window) {
   SSLAntiReplayContext* p_anti_replay = nullptr;
   EXPECT_EQ(SECSuccess,
             SSL_CreateAntiReplayContext(now_, window, 1, 3, &p_anti_replay));
   EXPECT_NE(nullptr, p_anti_replay);
   anti_replay_.reset(p_anti_replay);
 }
 
-void TlsConnectTestBase::MakeEcKeyParams(SECItem* params, SSLNamedGroup group) {
+ScopedSECItem TlsConnectTestBase::MakeEcKeyParams(SSLNamedGroup group) {
   auto groupDef = ssl_LookupNamedGroup(group);
-  ASSERT_NE(nullptr, groupDef);
+  EXPECT_NE(nullptr, groupDef);
 
   auto oidData = SECOID_FindOIDByTag(groupDef->oidTag);
-  ASSERT_NE(nullptr, oidData);
-  ASSERT_NE(nullptr,
-            SECITEM_AllocItem(nullptr, params, (2 + oidData->oid.len)));
+  EXPECT_NE(nullptr, oidData);
+  ScopedSECItem params(
+      SECITEM_AllocItem(nullptr, nullptr, (2 + oidData->oid.len)));
+  EXPECT_TRUE(!!params);
   params->data[0] = SEC_ASN1_OBJECT_ID;
   params->data[1] = oidData->oid.len;
   memcpy(params->data + 2, oidData->oid.data, oidData->oid.len);
+  return params;
 }
 
 void TlsConnectTestBase::GenerateEchConfig(
     HpkeKemId kem_id, const std::vector<HpkeSymmetricSuite>& cipher_suites,
     const std::string& public_name, uint16_t max_name_len, DataBuffer& record,
     ScopedSECKEYPublicKey& pubKey, ScopedSECKEYPrivateKey& privKey) {
   bool gen_keys = !pubKey && !privKey;
-  SECKEYECParams ecParams = {siBuffer, NULL, 0};
-  MakeEcKeyParams(&ecParams, ssl_grp_ec_curve25519);
 
   SECKEYPublicKey* pub = nullptr;
   SECKEYPrivateKey* priv = nullptr;
 
   if (gen_keys) {
-    priv = SECKEY_CreateECPrivateKey(&ecParams, &pub, nullptr);
+    ScopedSECItem ecParams = MakeEcKeyParams(ssl_grp_ec_curve25519);
+    priv = SECKEY_CreateECPrivateKey(ecParams.get(), &pub, nullptr);
   } else {
     priv = privKey.get();
     pub = pubKey.get();
   }
   ASSERT_NE(nullptr, priv);
-  SECITEM_FreeItem(&ecParams, PR_FALSE);
   PRUint8 encoded[1024];
   unsigned int encoded_len = 0;
   SECStatus rv = SSL_EncodeEchConfigId(
       77, public_name.c_str(), max_name_len, kem_id, pub, cipher_suites.data(),
       cipher_suites.size(), encoded, &encoded_len, sizeof(encoded));
   EXPECT_EQ(SECSuccess, rv);
   EXPECT_GT(encoded_len, 0U);
 
diff --git a/security/nss/gtests/ssl_gtest/tls_connect.h b/security/nss/gtests/ssl_gtest/tls_connect.h
--- a/security/nss/gtests/ssl_gtest/tls_connect.h
+++ b/security/nss/gtests/ssl_gtest/tls_connect.h
@@ -141,17 +141,17 @@ class TlsConnectTestBase : public ::test
   void AdvanceTime(PRTime time_shift);
 
   void ResetAntiReplay(PRTime window);
   void RolloverAntiReplay();
 
   void SaveAlgorithmPolicy();
   void RestoreAlgorithmPolicy();
 
-  static void MakeEcKeyParams(SECItem* params, SSLNamedGroup group);
+  static ScopedSECItem MakeEcKeyParams(SSLNamedGroup group);
   static void GenerateEchConfig(
       HpkeKemId kem_id, const std::vector<HpkeSymmetricSuite>& cipher_suites,
       const std::string& public_name, uint16_t max_name_len, DataBuffer& record,
       ScopedSECKEYPublicKey& pubKey, ScopedSECKEYPrivateKey& privKey);
   void SetupEch(std::shared_ptr<TlsAgent>& client,
                 std::shared_ptr<TlsAgent>& server,
                 HpkeKemId kem_id = HpkeDhKemX25519Sha256,
                 bool expect_ech = true, bool set_client_config = true,
diff --git a/security/nss/gtests/ssl_gtest/tls_ech_unittest.cc b/security/nss/gtests/ssl_gtest/tls_ech_unittest.cc
--- a/security/nss/gtests/ssl_gtest/tls_ech_unittest.cc
+++ b/security/nss/gtests/ssl_gtest/tls_ech_unittest.cc
@@ -177,24 +177,22 @@ class TlsConnectStreamTls13Ech : public 
                                       echconfig.len()));
   }
 
   void ValidatePublicNames(const std::vector<std::string>& names,
                            SECStatus expected) {
     static const std::vector<HpkeSymmetricSuite> kSuites = {
         {HpkeKdfHkdfSha256, HpkeAeadAes128Gcm}};
 
-    SECKEYECParams ecParams = {siBuffer, NULL, 0};
-    MakeEcKeyParams(&ecParams, ssl_grp_ec_curve25519);
-
+    ScopedSECItem ecParams = MakeEcKeyParams(ssl_grp_ec_curve25519);
     ScopedSECKEYPublicKey pub;
     ScopedSECKEYPrivateKey priv;
     SECKEYPublicKey* pub_p = nullptr;
     SECKEYPrivateKey* priv_p =
-        SECKEY_CreateECPrivateKey(&ecParams, &pub_p, nullptr);
+        SECKEY_CreateECPrivateKey(ecParams.get(), &pub_p, nullptr);
     pub.reset(pub_p);
     priv.reset(priv_p);
     ASSERT_TRUE(!!pub);
     ASSERT_TRUE(!!priv);
 
     EnsureTlsSetup();
 
     DataBuffer cfg;
diff --git a/security/nss/lib/nss/nss.h b/security/nss/lib/nss/nss.h
--- a/security/nss/lib/nss/nss.h
+++ b/security/nss/lib/nss/nss.h
@@ -17,22 +17,22 @@
 
 /*
  * NSS's major version, minor version, patch level, build number, and whether
  * this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define NSS_VERSION "3.68" _NSS_CUSTOMIZED " Beta"
+#define NSS_VERSION "3.68" _NSS_CUSTOMIZED
 #define NSS_VMAJOR 3
 #define NSS_VMINOR 68
 #define NSS_VPATCH 0
 #define NSS_VBUILD 0
-#define NSS_BETA PR_TRUE
+#define NSS_BETA PR_FALSE
 
 #ifndef RC_INVOKED
 
 #include "seccomon.h"
 
 typedef struct NSSInitParametersStr NSSInitParameters;
 
 /*
diff --git a/security/nss/lib/softoken/softkver.h b/security/nss/lib/softoken/softkver.h
--- a/security/nss/lib/softoken/softkver.h
+++ b/security/nss/lib/softoken/softkver.h
@@ -12,16 +12,16 @@
 
 /*
  * Softoken's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define SOFTOKEN_VERSION "3.68" SOFTOKEN_ECC_STRING " Beta"
+#define SOFTOKEN_VERSION "3.68" SOFTOKEN_ECC_STRING
 #define SOFTOKEN_VMAJOR 3
 #define SOFTOKEN_VMINOR 68
 #define SOFTOKEN_VPATCH 0
 #define SOFTOKEN_VBUILD 0
-#define SOFTOKEN_BETA PR_TRUE
+#define SOFTOKEN_BETA PR_FALSE
 
 #endif /* _SOFTKVER_H_ */
diff --git a/security/nss/lib/util/nssutil.h b/security/nss/lib/util/nssutil.h
--- a/security/nss/lib/util/nssutil.h
+++ b/security/nss/lib/util/nssutil.h
@@ -14,22 +14,22 @@
 
 /*
  * NSS utilities's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <Beta>]"
  */
-#define NSSUTIL_VERSION "3.68 Beta"
+#define NSSUTIL_VERSION "3.68"
 #define NSSUTIL_VMAJOR 3
 #define NSSUTIL_VMINOR 68
 #define NSSUTIL_VPATCH 0
 #define NSSUTIL_VBUILD 0
-#define NSSUTIL_BETA PR_TRUE
+#define NSSUTIL_BETA PR_FALSE
 
 SEC_BEGIN_PROTOS
 
 /*
  * Returns a const string of the UTIL library version.
  */
 extern const char *NSSUTIL_GetVersion(void);
 
