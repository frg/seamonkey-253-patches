# HG changeset patch
# User Jeff Muizelaar <jmuizelaar@mozilla.com>
# Date 1507823168 14400
# Node ID 319d79157004e37e168599bcebcfb73b69679262
# Parent  47b429d054b0e63e88053de65b43bf7d094ac1c4
Bug 1250461. Don't reject profiles with negative colorant tristiumlus value on macOS. r=mstange

This is basically just this: https://bugs.chromium.org/p/chromium/issues/detail?id=562951
The original profile that caused us to add this check has bigger problems than negative
colorant tristiumlus values. We should reject it using a better metric. In the mean time
let's not reject these things on macOS.

diff --git a/gfx/qcms/iccread.c b/gfx/qcms/iccread.c
--- a/gfx/qcms/iccread.c
+++ b/gfx/qcms/iccread.c
@@ -290,27 +290,16 @@ qcms_bool qcms_profile_is_bogus(qcms_pro
        gX = s15Fixed16Number_to_float(profile->greenColorant.X);
        gY = s15Fixed16Number_to_float(profile->greenColorant.Y);
        gZ = s15Fixed16Number_to_float(profile->greenColorant.Z);
 
        bX = s15Fixed16Number_to_float(profile->blueColorant.X);
        bY = s15Fixed16Number_to_float(profile->blueColorant.Y);
        bZ = s15Fixed16Number_to_float(profile->blueColorant.Z);
 
-       // Check if any of the XYZ values are negative (see mozilla bug 498245)
-       // CIEXYZ tristimulus values cannot be negative according to the spec.
-       negative =
-	       (rX < 0) || (rY < 0) || (rZ < 0) ||
-	       (gX < 0) || (gY < 0) || (gZ < 0) ||
-	       (bX < 0) || (bY < 0) || (bZ < 0);
-
-       if (negative)
-	       return true;
-
-
        // Sum the values; they should add up to something close to white
        sum[0] = rX + gX + bX;
        sum[1] = rY + gY + bY;
        sum[2] = rZ + gZ + bZ;
 
        // Build our target vector (see mozilla bug 460629)
        target[0] = 0.96420f;
        target[1] = 1.00000f;
@@ -326,16 +315,38 @@ qcms_bool qcms_profile_is_bogus(qcms_pro
 
        // Compare with our tolerance
        for (i = 0; i < 3; ++i) {
            if (!(((sum[i] - tolerance[i]) <= target[i]) &&
                  ((sum[i] + tolerance[i]) >= target[i])))
                return true;
        }
 
+#ifndef __APPLE__
+       // Check if any of the XYZ values are negative (see mozilla bug 498245)
+       // CIEXYZ tristimulus values cannot be negative according to the spec.
+
+       negative =
+	       (rX < 0) || (rY < 0) || (rZ < 0) ||
+	       (gX < 0) || (gY < 0) || (gZ < 0) ||
+	       (bX < 0) || (bY < 0) || (bZ < 0);
+
+#else
+       // Chromatic adaption to D50 can result in negative XYZ, but the white
+       // point D50 tolerance test has passed. Accept negative values herein.
+       // See https://bugzilla.mozilla.org/show_bug.cgi?id=498245#c18 onwards
+       // for discussion about whether profile XYZ can or cannot be negative,
+       // per the spec. Also the https://bugzil.la/450923 user report.
+
+       // FIXME: allow this relaxation on all ports?
+       negative = false;
+#endif
+       if (negative)
+	       return true; // bogus
+
        // All Good
        return false;
 }
 
 #define TAG_bXYZ 0x6258595a
 #define TAG_gXYZ 0x6758595a
 #define TAG_rXYZ 0x7258595a
 #define TAG_rTRC 0x72545243
