# HG changeset patch
# User Luke Wagner <luke@mozilla.com>
# Date 1506096325 18000
#      Fri Sep 22 11:05:25 2017 -0500
# Node ID 30f8a995d4c1949e8763ddf52e2c971e1e6ba1e8
# Parent  9efe1c62bc06fa524d42ded491ddbd4363a4075b
Bug 1329019 - Baldr: Split CallSiteAndTarget into CallSite and CallSiteTarget (r=lth)

MozReview-Commit-ID: 8pBMZHnHcIf

diff --git a/js/src/jit/shared/Assembler-shared.h b/js/src/jit/shared/Assembler-shared.h
--- a/js/src/jit/shared/Assembler-shared.h
+++ b/js/src/jit/shared/Assembler-shared.h
@@ -841,17 +841,18 @@ typedef Vector<TrapFarJump, 0, SystemAll
 
 } // namespace wasm
 
 namespace jit {
 
 // The base class of all Assemblers for all archs.
 class AssemblerShared
 {
-    wasm::CallSiteAndTargetVector callSites_;
+    wasm::CallSiteVector callSites_;
+    wasm::CallSiteTargetVector callSiteTargets_;
     wasm::CallFarJumpVector callFarJumps_;
     wasm::TrapSiteVector trapSites_;
     wasm::TrapFarJumpVector trapFarJumps_;
     wasm::MemoryAccessVector memoryAccesses_;
     wasm::SymbolicAccessVector symbolicAccesses_;
 
   protected:
     Vector<CodeLabel, 0, SystemAllocPolicy> codeLabels_;
@@ -877,41 +878,58 @@ class AssemblerShared
         return !enoughMemory_;
     }
 
     bool embedsNurseryPointers() const {
         return embedsNurseryPointers_;
     }
 
     template <typename... Args>
-    void append(const wasm::CallSiteDesc& desc, CodeOffset retAddr, Args&&... args)
-    {
-        wasm::CallSite cs(desc, retAddr.offset());
-        enoughMemory_ &= callSites_.emplaceBack(cs, mozilla::Forward<Args>(args)...);
+    void append(const wasm::CallSiteDesc& desc, CodeOffset retAddr, Args&&... args) {
+        enoughMemory_ &= callSites_.emplaceBack(desc, retAddr.offset());
+        enoughMemory_ &= callSiteTargets_.emplaceBack(mozilla::Forward<Args>(args)...);
+    }
+    const wasm::CallSiteVector& callSites() const {
+        return callSites_;
     }
-    wasm::CallSiteAndTargetVector& callSites() { return callSites_; }
+    const wasm::CallSiteTargetVector& callSiteTargets() const {
+        return callSiteTargets_;
+    }
+    wasm::CallSiteVector&& extractCallSites() {
+        return Move(callSites_);
+    }
 
     void append(wasm::CallFarJump jmp) {
         enoughMemory_ &= callFarJumps_.append(jmp);
     }
-    const wasm::CallFarJumpVector& callFarJumps() const { return callFarJumps_; }
+    const wasm::CallFarJumpVector& callFarJumps() const {
+        return callFarJumps_;
+    }
 
     void append(wasm::TrapSite trapSite) {
         enoughMemory_ &= trapSites_.append(trapSite);
     }
-    const wasm::TrapSiteVector& trapSites() const { return trapSites_; }
+    const wasm::TrapSiteVector& trapSites() const {
+        return trapSites_;
+    }
     void clearTrapSites() { trapSites_.clear(); }
 
     void append(wasm::TrapFarJump jmp) {
         enoughMemory_ &= trapFarJumps_.append(jmp);
     }
-    const wasm::TrapFarJumpVector& trapFarJumps() const { return trapFarJumps_; }
+    const wasm::TrapFarJumpVector& trapFarJumps() const {
+        return trapFarJumps_;
+    }
 
-    void append(wasm::MemoryAccess access) { enoughMemory_ &= memoryAccesses_.append(access); }
-    wasm::MemoryAccessVector&& extractMemoryAccesses() { return Move(memoryAccesses_); }
+    void append(wasm::MemoryAccess access) {
+        enoughMemory_ &= memoryAccesses_.append(access);
+    }
+    wasm::MemoryAccessVector&& extractMemoryAccesses() {
+        return Move(memoryAccesses_);
+    }
 
     void append(const wasm::MemoryAccessDesc& access, size_t codeOffset, size_t framePushed) {
         if (access.hasTrap()) {
             // If a memory access is trapping (wasm, SIMD.js, Atomics), create a
             // TrapSite now which will generate a trap out-of-line path at the end
             // of the function which will *then* append a MemoryAccess.
             wasm::TrapDesc trap(access.trapOffset(), wasm::Trap::OutOfBounds, framePushed,
                                 wasm::TrapSite::MemoryAccess);
@@ -946,16 +964,19 @@ class AssemblerShared
     // Merge this assembler with the other one, invalidating it, by shifting all
     // offsets by a delta.
     bool asmMergeWith(size_t delta, const AssemblerShared& other) {
         size_t i = callSites_.length();
         enoughMemory_ &= callSites_.appendAll(other.callSites_);
         for (; i < callSites_.length(); i++)
             callSites_[i].offsetReturnAddressBy(delta);
 
+        enoughMemory_ &= callSiteTargets_.appendAll(other.callSiteTargets_);
+        // Nothing to offset.
+
         MOZ_ASSERT(other.trapSites_.empty(), "should have been cleared by wasmEmitTrapOutOfLineCode");
 
         i = callFarJumps_.length();
         enoughMemory_ &= callFarJumps_.appendAll(other.callFarJumps_);
         for (; i < callFarJumps_.length(); i++)
             callFarJumps_[i].offsetBy(delta);
 
         i = trapFarJumps_.length();
diff --git a/js/src/wasm/WasmGenerator.cpp b/js/src/wasm/WasmGenerator.cpp
--- a/js/src/wasm/WasmGenerator.cpp
+++ b/js/src/wasm/WasmGenerator.cpp
@@ -284,71 +284,73 @@ ModuleGenerator::patchCallSites()
 
     OffsetMap existingCallFarJumps;
     if (!existingCallFarJumps.init())
         return false;
 
     EnumeratedArray<Trap, Trap::Limit, Maybe<uint32_t>> existingTrapFarJumps;
 
     for (; lastPatchedCallsite_ < masm_.callSites().length(); lastPatchedCallsite_++) {
-        const CallSiteAndTarget& cs = masm_.callSites()[lastPatchedCallsite_];
-        uint32_t callerOffset = cs.returnAddressOffset();
+        const CallSite& callSite = masm_.callSites()[lastPatchedCallsite_];
+        const CallSiteTarget& target = masm_.callSiteTargets()[lastPatchedCallsite_];
+
+        uint32_t callerOffset = callSite.returnAddressOffset();
         MOZ_RELEASE_ASSERT(callerOffset < INT32_MAX);
 
-        switch (cs.kind()) {
+        switch (callSite.kind()) {
           case CallSiteDesc::Dynamic:
           case CallSiteDesc::Symbolic:
             break;
           case CallSiteDesc::Func: {
-            if (funcIsCompiled(cs.funcIndex())) {
-                uint32_t calleeOffset = funcCodeRange(cs.funcIndex()).funcNormalEntry();
+            if (funcIsCompiled(target.funcIndex())) {
+                uint32_t calleeOffset = funcCodeRange(target.funcIndex()).funcNormalEntry();
                 MOZ_RELEASE_ASSERT(calleeOffset < INT32_MAX);
 
                 if (uint32_t(abs(int32_t(calleeOffset) - int32_t(callerOffset))) < JumpRange()) {
                     masm_.patchCall(callerOffset, calleeOffset);
                     break;
                 }
             }
 
-            OffsetMap::AddPtr p = existingCallFarJumps.lookupForAdd(cs.funcIndex());
+            OffsetMap::AddPtr p = existingCallFarJumps.lookupForAdd(target.funcIndex());
             if (!p) {
                 Offsets offsets;
                 offsets.begin = masm_.currentOffset();
-                masm_.append(CallFarJump(cs.funcIndex(), masm_.farJumpWithPatch()));
+                masm_.append(CallFarJump(target.funcIndex(), masm_.farJumpWithPatch()));
                 offsets.end = masm_.currentOffset();
                 if (masm_.oom())
                     return false;
 
                 if (!metadataTier_->codeRanges.emplaceBack(CodeRange::FarJumpIsland, offsets))
                     return false;
-                if (!existingCallFarJumps.add(p, cs.funcIndex(), offsets.begin))
+                if (!existingCallFarJumps.add(p, target.funcIndex(), offsets.begin))
                     return false;
             }
 
             masm_.patchCall(callerOffset, p->value());
             break;
           }
           case CallSiteDesc::TrapExit: {
-            if (!existingTrapFarJumps[cs.trap()]) {
+            if (!existingTrapFarJumps[target.trap()]) {
                 // See MacroAssembler::wasmEmitTrapOutOfLineCode for why we must
                 // reload the TLS register on this path.
                 Offsets offsets;
                 offsets.begin = masm_.currentOffset();
                 masm_.loadPtr(Address(FramePointer, offsetof(Frame, tls)), WasmTlsReg);
-                masm_.append(TrapFarJump(cs.trap(), masm_.farJumpWithPatch()));
+                masm_.append(TrapFarJump(target.trap(), masm_.farJumpWithPatch()));
                 offsets.end = masm_.currentOffset();
                 if (masm_.oom())
                     return false;
 
                 if (!metadataTier_->codeRanges.emplaceBack(CodeRange::FarJumpIsland, offsets))
                     return false;
-                existingTrapFarJumps[cs.trap()] = Some(offsets.begin);
+                existingTrapFarJumps[target.trap()] = Some(offsets.begin);
             }
 
-            masm_.patchCall(callerOffset, *existingTrapFarJumps[cs.trap()]);
+            masm_.patchCall(callerOffset, *existingTrapFarJumps[target.trap()]);
             break;
           }
           case CallSiteDesc::Breakpoint:
           case CallSiteDesc::EnterFrame:
           case CallSiteDesc::LeaveFrame: {
             Uint32Vector& jumps = metadataTier_->debugTrapFarJumpOffsets;
             if (jumps.empty() ||
                 uint32_t(abs(int32_t(jumps.back()) - int32_t(callerOffset))) >= JumpRange())
@@ -1108,21 +1110,16 @@ ModuleGenerator::generateBytecodeHash(co
     sha1Sum.update(bytecode.begin(), bytecode.length());
     sha1Sum.finish(hash);
     memcpy(metadata_->debugHash, hash, sizeof(ModuleHash));
 }
 
 bool
 ModuleGenerator::finishMetadata(const ShareableBytes& bytecode)
 {
-    // Convert the CallSiteAndTargetVector (needed during generation) to a
-    // CallSiteVector (what is stored in the Module).
-    if (!metadataTier_->callSites.appendAll(masm_.callSites()))
-        return false;
-
     // The MacroAssembler has accumulated all the memory accesses during codegen.
     metadataTier_->memoryAccesses = masm_.extractMemoryAccesses();
 
     // Copy over data from the ModuleEnvironment.
     metadata_->memoryUsage = env_->memoryUsage;
     metadata_->minMemoryLength = env_->minMemoryLength;
     metadata_->maxMemoryLength = env_->maxMemoryLength;
     metadata_->tables = Move(env_->tables);
@@ -1143,16 +1140,17 @@ ModuleGenerator::finishMetadata(const Sh
                 return false;
             metadata_->debugFuncReturnTypes[i] = env_->funcSigs[i]->ret();
         }
         metadataTier_->debugFuncToCodeRange = Move(funcToCodeRange_);
     }
 
     // These Vectors can get large and the excess capacity can be significant,
     // so realloc them down to size.
+    metadataTier_->callSites = masm_.extractCallSites();
     metadataTier_->memoryAccesses.podResizeToFit();
     metadataTier_->codeRanges.podResizeToFit();
     metadataTier_->callSites.podResizeToFit();
     metadataTier_->debugTrapFarJumpOffsets.podResizeToFit();
     metadataTier_->debugFuncToCodeRange.podResizeToFit();
 
     // For asm.js, the tables vector is over-allocated (to avoid resize during
     // parallel copilation). Shrink it back down to fit.
diff --git a/js/src/wasm/WasmTypes.h b/js/src/wasm/WasmTypes.h
--- a/js/src/wasm/WasmTypes.h
+++ b/js/src/wasm/WasmTypes.h
@@ -1151,43 +1151,63 @@ class CallSite : public CallSiteDesc
 
     void setReturnAddressOffset(uint32_t r) { returnAddressOffset_ = r; }
     void offsetReturnAddressBy(int32_t o) { returnAddressOffset_ += o; }
     uint32_t returnAddressOffset() const { return returnAddressOffset_; }
 };
 
 WASM_DECLARE_POD_VECTOR(CallSite, CallSiteVector)
 
-class CallSiteAndTarget : public CallSite
+// A CallSiteTarget describes the callee of a CallSite, either a function or a
+// trap exit. Although checked in debug builds, a CallSiteTarget doesn't
+// officially know whether it targets a function or trap, relying on the Kind of
+// the CallSite to discriminate.
+
+class CallSiteTarget
 {
-    uint32_t index_;
+    uint32_t packed_;
+#ifdef DEBUG
+    enum Kind { None, FuncIndex, TrapExit } kind_;
+#endif
 
   public:
-    explicit CallSiteAndTarget(CallSite cs)
-      : CallSite(cs)
-    {
-        MOZ_ASSERT(cs.kind() != Func);
-    }
-    CallSiteAndTarget(CallSite cs, uint32_t funcIndex)
-      : CallSite(cs), index_(funcIndex)
-    {
-        MOZ_ASSERT(cs.kind() == Func);
-    }
-    CallSiteAndTarget(CallSite cs, Trap trap)
-      : CallSite(cs),
-        index_(uint32_t(trap))
-    {
-        MOZ_ASSERT(cs.kind() == TrapExit);
+    explicit CallSiteTarget()
+      : packed_(UINT32_MAX)
+#ifdef DEBUG
+      , kind_(None)
+#endif
+    {}
+
+    explicit CallSiteTarget(uint32_t funcIndex)
+      : packed_(funcIndex)
+#ifdef DEBUG
+      , kind_(FuncIndex)
+#endif
+    {}
+
+    explicit CallSiteTarget(Trap trap)
+      : packed_(uint32_t(trap))
+#ifdef DEBUG
+      , kind_(TrapExit)
+#endif
+    {}
+
+    uint32_t funcIndex() const {
+        MOZ_ASSERT(kind_ == FuncIndex);
+        return packed_;
     }
 
-    uint32_t funcIndex() const { MOZ_ASSERT(kind() == Func); return index_; }
-    Trap trap() const { MOZ_ASSERT(kind() == TrapExit); return Trap(index_); }
+    Trap trap() const {
+        MOZ_ASSERT(kind_ == TrapExit);
+        MOZ_ASSERT(packed_ < uint32_t(Trap::Limit));
+        return Trap(packed_);
+    }
 };
 
-typedef Vector<CallSiteAndTarget, 0, SystemAllocPolicy> CallSiteAndTargetVector;
+typedef Vector<CallSiteTarget, 0, SystemAllocPolicy> CallSiteTargetVector;
 
 // A wasm::SymbolicAddress represents a pointer to a well-known function that is
 // embedded in wasm code. Since wasm code is serialized and later deserialized
 // into a different address space, symbolic addresses must be used for *all*
 // pointers into the address space. The MacroAssembler records a list of all
 // SymbolicAddresses and the offsets of their use in the code for later patching
 // during static linking.
 
