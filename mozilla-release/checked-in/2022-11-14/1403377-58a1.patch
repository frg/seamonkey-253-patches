# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1506548975 25200
# Node ID e003f2efac27d627c3ac1395d1de6993f2ad61d3
# Parent  6c3e954ff71df5ca05a9f26bdd7710a6e9ad91c2
Bug 1403377 - Destroy hash table iterator before modifying hash table. r=smaug

nsDOMAttributeMap::BlastSubtreeToPieces() uses a hash table iterator
to get an arbitrary element from a hash table. This marks the hash
table as being in use. It then modifies the hash table before the
iterator is destroyed, triggering an assertion. The fix is to restrict
the scope of the iterator so that it is destroyed before the hashtable
is modified.

The existing code is safe because the iterator is never used after the
hash table has been modified.

MozReview-Commit-ID: DUjqewvwEe7

diff --git a/dom/base/crashtests/1403377.html b/dom/base/crashtests/1403377.html
new file mode 100644
--- /dev/null
+++ b/dom/base/crashtests/1403377.html
@@ -0,0 +1,18 @@
+<script>
+function fzfn(e){
+  let nc=window.frames[0].document.body.childElementCount;
+  let o=window.frames[0].document.body.childNodes[1%nc];
+  document.getElementById('b').appendChild(o.parentNode.removeChild(o));
+}
+window.onload=function(){
+  document.body.onerror=fzfn;
+  document.getElementById('c').addEventListener('DOMNodeRemoved', fzfn, true);
+  document.getElementById('c').attributes[0].name=='id';
+  window.frames[0].document.body.appendChild(document.getElementById('c'));
+  document.getElementById('a').innerHTML=document.createElement('multicol').outerHTML;
+}
+</script>
+<iframe></iframe>
+<script id='a'></script>
+<script id='b'></script>
+<script id='c'></script>
diff --git a/dom/base/crashtests/crashtests.list b/dom/base/crashtests/crashtests.list
--- a/dom/base/crashtests/crashtests.list
+++ b/dom/base/crashtests/crashtests.list
@@ -220,9 +220,10 @@ skip-if(stylo&&isDebugBuild&&winWidget) 
 HTTP(..) load xhr_abortinprogress.html
 load xhr_empty_datauri.html
 load xhr_html_nullresponse.html
 load 1383478.html
 load 1383780.html
 pref(clipboard.autocopy,true) load 1385272-1.html
 load 1393806.html
 load 1400701.html
+load 1403377.html
 pref(layout.css.resizeobserver.enabled,true) load 1555786.html
\ No newline at end of file
diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -7725,25 +7725,29 @@ nsIDocument::GetCompatMode(nsString& aCo
 
 void
 nsDOMAttributeMap::BlastSubtreeToPieces(nsINode *aNode)
 {
   if (aNode->IsElement()) {
     Element *element = aNode->AsElement();
     const nsDOMAttributeMap *map = element->GetAttributeMap();
     if (map) {
-      // This non-standard style of iteration is presumably used because some
-      // of the code in the loop body can trigger element removal, which
-      // invalidates the iterator.
       while (true) {
-        auto iter = map->mAttributeCache.ConstIter();
-        if (iter.Done()) {
-          break;
+        nsCOMPtr<nsIAttribute> attr;
+        {
+          // Use an iterator to get an arbitrary attribute from the
+          // cache. The iterator must be destroyed before any other
+          // operations on mAttributeCache, to avoid hash table
+          // assertions.
+          auto iter = map->mAttributeCache.ConstIter();
+          if (iter.Done()) {
+            break;
+          }
+          attr = iter.UserData();
         }
-        nsCOMPtr<nsIAttribute> attr = iter.UserData();
         NS_ASSERTION(attr.get(),
                      "non-nsIAttribute somehow made it into the hashmap?!");
 
         BlastSubtreeToPieces(attr);
 
         DebugOnly<nsresult> rv =
           element->UnsetAttr(attr->NodeInfo()->NamespaceID(),
                              attr->NodeInfo()->NameAtom(),
