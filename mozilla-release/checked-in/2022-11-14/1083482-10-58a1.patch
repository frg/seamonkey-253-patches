# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1509633497 -3600
#      Thu Nov 02 15:38:17 2017 +0100
# Node ID 3f850c136ee2db525daa6833fa84b69ef7ceb7a2
# Parent  95123b3ddfaed6fea5e0908a0b675c7ea6c10820
Bug 1083482 part 10 - Rename ResumeKind::CLOSE to ResumeKind::RETURN. r=anba

diff --git a/js/src/builtin/Generator.js b/js/src/builtin/Generator.js
--- a/js/src/builtin/Generator.js
+++ b/js/src/builtin/Generator.js
@@ -57,17 +57,17 @@ function GeneratorReturn(val) {
             return { value: val, done: true };
 
         if (GeneratorIsRunning(this))
             ThrowTypeError(JSMSG_NESTING_GENERATOR);
     }
 
     try {
         var rval = { value: val, done: true };
-        return resumeGenerator(this, rval, "close");
+        return resumeGenerator(this, rval, "return");
     } catch (e) {
         if (!GeneratorObjectIsClosed(this))
             GeneratorSetClosed(this);
         throw e;
     }
 }
 
 function InterpretGeneratorResume(gen, val, kind) {
@@ -75,11 +75,11 @@ function InterpretGeneratorResume(gen, v
     // the resumeGenerator/JSOP_RESUME also has to run in the interpreter. The
     // forceInterpreter() call below compiles to a bytecode op that prevents us
     // from JITing this script.
     forceInterpreter();
     if (kind === "next")
        return resumeGenerator(gen, val, "next");
     if (kind === "throw")
        return resumeGenerator(gen, val, "throw");
-    assert(kind === "close", "Invalid resume kind");
-    return resumeGenerator(gen, val, "close");
+    assert(kind === "return", "Invalid resume kind");
+    return resumeGenerator(gen, val, "return");
 }
diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -9328,17 +9328,17 @@ BytecodeEmitter::emitSelfHostedCallFunct
 
     checkTypeSet(callOp);
     return true;
 }
 
 bool
 BytecodeEmitter::emitSelfHostedResumeGenerator(ParseNode* pn)
 {
-    // Syntax: resumeGenerator(gen, value, 'next'|'throw'|'close')
+    // Syntax: resumeGenerator(gen, value, 'next'|'throw'|'return')
     if (pn->pn_count != 4) {
         reportError(pn, JSMSG_MORE_ARGS_NEEDED, "resumeGenerator", "1", "s");
         return false;
     }
 
     ParseNode* funNode = pn->pn_head;  // The resumeGenerator node.
 
     ParseNode* genNode = funNode->pn_next;
diff --git a/js/src/jit/BaselineCompiler.cpp b/js/src/jit/BaselineCompiler.cpp
--- a/js/src/jit/BaselineCompiler.cpp
+++ b/js/src/jit/BaselineCompiler.cpp
@@ -4703,17 +4703,17 @@ BaselineCompiler::emit_JSOP_FINALYIELDRV
 typedef bool (*InterpretResumeFn)(JSContext*, HandleObject, HandleValue, HandlePropertyName,
                                   MutableHandleValue);
 static const VMFunction InterpretResumeInfo =
     FunctionInfo<InterpretResumeFn>(jit::InterpretResume, "InterpretResume");
 
 typedef bool (*GeneratorThrowFn)(JSContext*, BaselineFrame*, Handle<GeneratorObject*>,
                                  HandleValue, uint32_t);
 static const VMFunction GeneratorThrowInfo =
-    FunctionInfo<GeneratorThrowFn>(jit::GeneratorThrowOrClose, "GeneratorThrowOrClose", TailCall);
+    FunctionInfo<GeneratorThrowFn>(jit::GeneratorThrowOrReturn, "GeneratorThrowOrReturn", TailCall);
 
 bool
 BaselineCompiler::emit_JSOP_RESUME()
 {
     GeneratorObject::ResumeKind resumeKind = GeneratorObject::getResumeKind(pc);
 
     frame.syncStack(0);
     masm.checkStackAlignment();
@@ -4886,17 +4886,17 @@ BaselineCompiler::emit_JSOP_RESUME()
                         scratch2);
         masm.loadPtr(BaseIndex(scratch1, scratch2, ScaleFromElemWidth(sizeof(uintptr_t))), scratch1);
 
         // Mark as running and jump to the generator's JIT code.
         masm.storeValue(Int32Value(GeneratorObject::YIELD_AND_AWAIT_INDEX_RUNNING),
                         Address(genObj, GeneratorObject::offsetOfYieldAndAwaitIndexSlot()));
         masm.jump(scratch1);
     } else {
-        MOZ_ASSERT(resumeKind == GeneratorObject::THROW || resumeKind == GeneratorObject::CLOSE);
+        MOZ_ASSERT(resumeKind == GeneratorObject::THROW || resumeKind == GeneratorObject::RETURN);
 
         // Update the frame's frameSize field.
         masm.computeEffectiveAddress(Address(BaselineFrameReg, BaselineFrame::FramePointerOffset),
                                      scratch2);
         masm.movePtr(scratch2, scratch1);
         masm.subStackPtrFrom(scratch2);
         masm.store32(scratch2, Address(BaselineFrameReg, BaselineFrame::reverseOffsetOfFrameSize()));
         masm.loadBaselineFramePtr(BaselineFrameReg, scratch2);
@@ -4912,17 +4912,17 @@ BaselineCompiler::emit_JSOP_RESUME()
             return false;
 
         // Create the frame descriptor.
         masm.subStackPtrFrom(scratch1);
         masm.makeFrameDescriptor(scratch1, JitFrame_BaselineJS, ExitFrameLayout::Size());
 
         // Push the frame descriptor and a dummy return address (it doesn't
         // matter what we push here, frame iterators will use the frame pc
-        // set in jit::GeneratorThrowOrClose).
+        // set in jit::GeneratorThrowOrReturn).
         masm.push(scratch1);
 
         // On ARM64, the callee will push the return address.
 #ifndef JS_CODEGEN_ARM64
         masm.push(ImmWord(0));
 #endif
         masm.jump(code);
     }
@@ -4931,18 +4931,18 @@ BaselineCompiler::emit_JSOP_RESUME()
     masm.bind(&interpret);
 
     prepareVMCall();
     if (resumeKind == GeneratorObject::NEXT) {
         pushArg(ImmGCPtr(cx->names().next));
     } else if (resumeKind == GeneratorObject::THROW) {
         pushArg(ImmGCPtr(cx->names().throw_));
     } else {
-        MOZ_ASSERT(resumeKind == GeneratorObject::CLOSE);
-        pushArg(ImmGCPtr(cx->names().close));
+        MOZ_ASSERT(resumeKind == GeneratorObject::RETURN);
+        pushArg(ImmGCPtr(cx->names().return_));
     }
 
     masm.loadValue(frame.addressOfStackValue(frame.peek(-1)), retVal);
     pushArg(retVal);
     pushArg(genObj);
 
     if (!callVM(InterpretResumeInfo))
         return false;
diff --git a/js/src/jit/VMFunctions.cpp b/js/src/jit/VMFunctions.cpp
--- a/js/src/jit/VMFunctions.cpp
+++ b/js/src/jit/VMFunctions.cpp
@@ -929,28 +929,28 @@ DebugAfterYield(JSContext* cx, BaselineF
     // The BaselineFrame has just been constructed by JSOP_RESUME in the
     // caller. We need to set its debuggee flag as necessary.
     if (frame->script()->isDebuggee())
         frame->setIsDebuggee();
     return true;
 }
 
 bool
-GeneratorThrowOrClose(JSContext* cx, BaselineFrame* frame, Handle<GeneratorObject*> genObj,
-                      HandleValue arg, uint32_t resumeKind)
+GeneratorThrowOrReturn(JSContext* cx, BaselineFrame* frame, Handle<GeneratorObject*> genObj,
+                       HandleValue arg, uint32_t resumeKind)
 {
     // Set the frame's pc to the current resume pc, so that frame iterators
     // work. This function always returns false, so we're guaranteed to enter
     // the exception handler where we will clear the pc.
     JSScript* script = frame->script();
     uint32_t offset = script->yieldAndAwaitOffsets()[genObj->yieldAndAwaitIndex()];
     frame->setOverridePc(script->offsetToPC(offset));
 
     MOZ_ALWAYS_TRUE(DebugAfterYield(cx, frame));
-    MOZ_ALWAYS_FALSE(js::GeneratorThrowOrClose(cx, frame, genObj, arg, resumeKind));
+    MOZ_ALWAYS_FALSE(js::GeneratorThrowOrReturn(cx, frame, genObj, arg, resumeKind));
     return false;
 }
 
 bool
 CheckGlobalOrEvalDeclarationConflicts(JSContext* cx, BaselineFrame* frame)
 {
     RootedScript script(cx, frame->script());
     RootedObject envChain(cx, frame->environmentChain());
diff --git a/js/src/jit/VMFunctions.h b/js/src/jit/VMFunctions.h
--- a/js/src/jit/VMFunctions.h
+++ b/js/src/jit/VMFunctions.h
@@ -726,18 +726,18 @@ NormalSuspend(JSContext* cx, HandleObjec
 MOZ_MUST_USE bool
 FinalSuspend(JSContext* cx, HandleObject obj, BaselineFrame* frame, jsbytecode* pc);
 MOZ_MUST_USE bool
 InterpretResume(JSContext* cx, HandleObject obj, HandleValue val, HandlePropertyName kind,
                 MutableHandleValue rval);
 MOZ_MUST_USE bool
 DebugAfterYield(JSContext* cx, BaselineFrame* frame);
 MOZ_MUST_USE bool
-GeneratorThrowOrClose(JSContext* cx, BaselineFrame* frame, Handle<GeneratorObject*> genObj,
-                      HandleValue arg, uint32_t resumeKind);
+GeneratorThrowOrReturn(JSContext* cx, BaselineFrame* frame, Handle<GeneratorObject*> genObj,
+                       HandleValue arg, uint32_t resumeKind);
 
 MOZ_MUST_USE bool
 GlobalNameConflictsCheckFromIon(JSContext* cx, HandleScript script);
 MOZ_MUST_USE bool
 CheckGlobalOrEvalDeclarationConflicts(JSContext* cx, BaselineFrame* frame);
 MOZ_MUST_USE bool
 InitFunctionEnvironmentObjects(JSContext* cx, BaselineFrame* frame);
 
diff --git a/js/src/vm/CommonPropertyNames.h b/js/src/vm/CommonPropertyNames.h
--- a/js/src/vm/CommonPropertyNames.h
+++ b/js/src/vm/CommonPropertyNames.h
@@ -61,17 +61,16 @@
     macro(callee, callee, "callee") \
     macro(caller, caller, "caller") \
     macro(callFunction, callFunction, "callFunction") \
     macro(cancel, cancel, "cancel") \
     macro(case, case_, "case") \
     macro(caseFirst, caseFirst, "caseFirst") \
     macro(catch, catch_, "catch") \
     macro(class, class_, "class") \
-    macro(close, close, "close") \
     macro(Collator, Collator, "Collator") \
     macro(collections, collections, "collections") \
     macro(columnNumber, columnNumber, "columnNumber") \
     macro(comma, comma, ",") \
     macro(compare, compare, "compare") \
     macro(configurable, configurable, "configurable") \
     macro(const, const_, "const") \
     macro(construct, construct, "construct") \
diff --git a/js/src/vm/GeneratorObject.cpp b/js/src/vm/GeneratorObject.cpp
--- a/js/src/vm/GeneratorObject.cpp
+++ b/js/src/vm/GeneratorObject.cpp
@@ -108,24 +108,24 @@ js::SetGeneratorClosed(JSContext* cx, Ab
 
     // Get the generator object stored on the scope chain and close it.
     Shape* shape = callObj.lookup(cx, cx->names().dotGenerator);
     GeneratorObject& genObj = callObj.getSlot(shape->slot()).toObject().as<GeneratorObject>();
     genObj.setClosed();
 }
 
 bool
-js::GeneratorThrowOrClose(JSContext* cx, AbstractFramePtr frame, Handle<GeneratorObject*> genObj,
-                          HandleValue arg, uint32_t resumeKind)
+js::GeneratorThrowOrReturn(JSContext* cx, AbstractFramePtr frame, Handle<GeneratorObject*> genObj,
+                           HandleValue arg, uint32_t resumeKind)
 {
     if (resumeKind == GeneratorObject::THROW) {
         cx->setPendingException(arg);
         genObj->setRunning();
     } else {
-        MOZ_ASSERT(resumeKind == GeneratorObject::CLOSE);
+        MOZ_ASSERT(resumeKind == GeneratorObject::RETURN);
 
         MOZ_ASSERT(arg.isObject());
         frame.setReturnValue(arg);
 
         cx->setPendingException(MagicValue(JS_GENERATOR_CLOSING));
         genObj->setClosing();
     }
     return false;
@@ -169,18 +169,18 @@ GeneratorObject::resume(JSContext* cx, I
     activation.regs().sp[-1] = arg;
 
     switch (resumeKind) {
       case NEXT:
         genObj->setRunning();
         return true;
 
       case THROW:
-      case CLOSE:
-        return GeneratorThrowOrClose(cx, activation.regs().fp(), genObj, arg, resumeKind);
+      case RETURN:
+        return GeneratorThrowOrReturn(cx, activation.regs().fp(), genObj, arg, resumeKind);
 
       default:
         MOZ_CRASH("bad resumeKind");
     }
 }
 
 const Class GeneratorObject::class_ = {
     "Generator",
diff --git a/js/src/vm/GeneratorObject.h b/js/src/vm/GeneratorObject.h
--- a/js/src/vm/GeneratorObject.h
+++ b/js/src/vm/GeneratorObject.h
@@ -29,39 +29,39 @@ class GeneratorObject : public NativeObj
         ENV_CHAIN_SLOT,
         ARGS_OBJ_SLOT,
         EXPRESSION_STACK_SLOT,
         YIELD_AND_AWAIT_INDEX_SLOT,
         NEWTARGET_SLOT,
         RESERVED_SLOTS
     };
 
-    enum ResumeKind { NEXT, THROW, CLOSE };
+    enum ResumeKind { NEXT, THROW, RETURN };
 
     static const Class class_;
 
   private:
     static bool suspend(JSContext* cx, HandleObject obj, AbstractFramePtr frame, jsbytecode* pc,
                         Value* vp, unsigned nvalues);
 
   public:
     static inline ResumeKind getResumeKind(jsbytecode* pc) {
         MOZ_ASSERT(*pc == JSOP_RESUME);
         unsigned arg = GET_UINT16(pc);
-        MOZ_ASSERT(arg <= CLOSE);
+        MOZ_ASSERT(arg <= RETURN);
         return static_cast<ResumeKind>(arg);
     }
 
     static inline ResumeKind getResumeKind(JSContext* cx, JSAtom* atom) {
         if (atom == cx->names().next)
             return NEXT;
         if (atom == cx->names().throw_)
             return THROW;
-        MOZ_ASSERT(atom == cx->names().close);
-        return CLOSE;
+        MOZ_ASSERT(atom == cx->names().return_);
+        return RETURN;
     }
 
     static JSObject* create(JSContext* cx, AbstractFramePtr frame);
 
     static bool resume(JSContext* cx, InterpreterActivation& activation,
                        HandleObject obj, HandleValue arg, ResumeKind resumeKind);
 
     static bool initialSuspend(JSContext* cx, HandleObject obj, AbstractFramePtr frame, jsbytecode* pc) {
@@ -206,18 +206,18 @@ class GeneratorObject : public NativeObj
     static size_t offsetOfExpressionStackSlot() {
         return getFixedSlotOffset(EXPRESSION_STACK_SLOT);
     }
     static size_t offsetOfNewTargetSlot() {
         return getFixedSlotOffset(NEWTARGET_SLOT);
     }
 };
 
-bool GeneratorThrowOrClose(JSContext* cx, AbstractFramePtr frame, Handle<GeneratorObject*> obj,
-                           HandleValue val, uint32_t resumeKind);
+bool GeneratorThrowOrReturn(JSContext* cx, AbstractFramePtr frame, Handle<GeneratorObject*> obj,
+                            HandleValue val, uint32_t resumeKind);
 void SetGeneratorClosed(JSContext* cx, AbstractFramePtr frame);
 
 MOZ_MUST_USE bool
 CheckGeneratorResumptionValue(JSContext* cx, HandleValue v);
 
 } // namespace js
 
 #endif /* vm_GeneratorObject_h */
