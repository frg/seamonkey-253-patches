# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1504956040 -3600
# Node ID dcadd5c098510d8db6c2da5cff3aa81ae273fc74
# Parent  9e08e31ec9b679bfa9f3295354727ed52bd58bd9
Bug 1396866 - Restore window state on maximizing, minimizing, and fullscreening. r=automatedtester

When maximizing the window we must restore it from iconified state or
exit fullscreen first.  Likewise for minimizing the window, we must
exit fullscreen.  For fullscreening the window we need to also restore
the window.

MozReview-Commit-ID: AOQX2cV2C75

diff --git a/testing/marionette/driver.js b/testing/marionette/driver.js
--- a/testing/marionette/driver.js
+++ b/testing/marionette/driver.js
@@ -1477,32 +1477,16 @@ GeckoDriver.prototype.getWindowRect = fu
 GeckoDriver.prototype.setWindowRect = async function(cmd, resp) {
   assert.firefox();
   const win = assert.window(this.getCurrentWindow());
   assert.noUserPrompt(this.dialog);
 
   let {x, y, width, height} = cmd.parameters;
   let origRect = this.curBrowser.rect;
 
-  // Exit fullscreen and wait for window to resize.
-  async function exitFullscreen() {
-    return new Promise(resolve => {
-      win.addEventListener("sizemodechange", whenIdle(win, resolve), {once: true});
-      win.fullScreen = false;
-    });
-  }
-
-  // Restore window and wait for the window state to change.
-  async function restoreWindow() {
-    return new Promise(resolve => {
-      win.addEventListener("sizemodechange", whenIdle(win, resolve), {once: true});
-      win.restore();
-    });
-  }
-
   // Synchronous resize to |width| and |height| dimensions.
   async function resizeWindow(width, height) {
     return new Promise(resolve => {
       win.addEventListener("resize", whenIdle(win, resolve), {once: true});
       win.resizeTo(width, height);
     });
   }
 
@@ -1528,23 +1512,23 @@ GeckoDriver.prototype.setWindowRect = as
           (win.screenX != origRect.x || win.screenY != origRect.y)) {
         resolve();
       } else {
         reject();
       }
     });
   }
 
-  switch (win.windowState) {
-    case win.STATE_FULLSCREEN:
-      await exitFullscreen();
+  switch (WindowState.from(win.windowState)) {
+    case WindowState.Fullscreen:
+      await exitFullscreen(win);
       break;
 
-    case win.STATE_MINIMIZED:
-      await restoreWindow();
+    case WindowState.Minimized:
+      await restoreWindow(win);
       break;
   }
 
   if (height != null && width != null) {
     assert.positiveInteger(height);
     assert.positiveInteger(width);
 
     if (win.outerWidth != width || win.outerHeight != height) {
@@ -3012,18 +2996,21 @@ GeckoDriver.prototype.setScreenOrientati
  * @throws {UnexpectedAlertOpenError}
  *     A modal dialog is open, blocking this operation.
  */
 GeckoDriver.prototype.minimizeWindow = async function(cmd, resp) {
   assert.firefox();
   const win = assert.window(this.getCurrentWindow());
   assert.noUserPrompt(this.dialog);
 
-  let state = WindowState.from(win.windowState);
-  if (state != WindowState.Minimized) {
+  if (WindowState.from(win.windowState) == WindowState.Fullscreen) {
+    await exitFullscreen(win);
+  }
+
+  if (WindowState.from(win.windowState) != WindowState.Minimized) {
     await new Promise(resolve => {
       win.addEventListener("sizemodechange", whenIdle(win, resolve), {once: true});
       win.minimize();
     });
   }
 
   return this.curBrowser.rect;
 };
@@ -3046,16 +3033,26 @@ GeckoDriver.prototype.minimizeWindow = a
  * @throws {UnexpectedAlertOpenError}
  *     A modal dialog is open, blocking this operation.
  */
 GeckoDriver.prototype.maximizeWindow = async function(cmd, resp) {
   assert.firefox();
   const win = assert.window(this.getCurrentWindow());
   assert.noUserPrompt(this.dialog);
 
+  switch (WindowState.from(win.windowState)) {
+    case WindowState.Fullscreen:
+      await exitFullscreen(win);
+      break;
+
+    case WindowState.Minimized:
+      await restoreWindow(win);
+      break;
+  }
+
   const origSize = {
     outerWidth: win.outerWidth,
     outerHeight: win.outerHeight,
   };
 
   // Wait for the window size to change.
   async function windowSizeChange(from) {
     return wait.until((resolve, reject) => {
@@ -3067,18 +3064,17 @@ GeckoDriver.prototype.maximizeWindow = a
           curSize.outerHeight != origSize.outerHeight) {
         resolve();
       } else {
         reject();
       }
     });
   }
 
-  let state = WindowState.from(win.windowState);
-  if (state != WindowState.Maximized) {
+  if (WindowState.from(win.windowState) != win.Maximized) {
     await new TimedPromise(resolve => {
       win.addEventListener("sizemodechange", resolve, {once: true});
       win.maximize();
     }, {throws: null});
 
     // Transitioning into a window state is asynchronous on Linux,
     // and we cannot rely on sizemodechange to accurately tell us when
     // the transition has completed.
@@ -3123,18 +3119,21 @@ GeckoDriver.prototype.maximizeWindow = a
  * @throws {UnexpectedAlertOpenError}
  *     A modal dialog is open, blocking this operation.
  */
 GeckoDriver.prototype.fullscreenWindow = async function(cmd, resp) {
   assert.firefox();
   const win = assert.window(this.getCurrentWindow());
   assert.noUserPrompt(this.dialog);
 
-  let state = WindowState.from(win.windowState);
-  if (state != WindowState.Fullscreen) {
+  if (WindowState.from(win.windowState) == WindowState.Minimized) {
+    await restoreWindow(win);
+  }
+
+  if (WindowState.from(win.windowState) != WindowState.Fullscreen) {
     await new Promise(resolve => {
       win.addEventListener("sizemodechange", resolve, {once: true});
       win.fullScreen = true;
     });
   }
 
   return this.curBrowser.rect;
 };
@@ -3696,16 +3695,42 @@ function copy(obj) {
 
 function getOuterWindowId(win) {
   return win.QueryInterface(Ci.nsIInterfaceRequestor)
       .getInterface(Ci.nsIDOMWindowUtils)
       .outerWindowID;
 }
 
 /**
+ * Exit fullscreen and wait for <var>window</var> to resize.
+ *
+ * @param {ChromeWindow} window
+ *     Window to exit fullscreen.
+ */
+async function exitFullscreen(window) {
+  return new Promise(resolve => {
+    window.addEventListener("sizemodechange", whenIdle(window, resolve), {once: true});
+    window.fullScreen = false;
+  });
+}
+
+/**
+ * Restore window and wait for the window state to change.
+ *
+ * @param {ChromeWindow} window
+ *     Window to restore.
+ */
+async function restoreWindow(window) {
+  return new Promise(resolve => {
+    window.addEventListener("sizemodechange", whenIdle(window, resolve), {once: true});
+    window.restore();
+  });
+}
+
+/**
  * Throttle <var>callback</var> until the main thread is idle and
  * <var>window</var> has performed an animation frame.
  *
  * @param {ChromeWindow} window
  *     Window to request the animation frame from.
  * @param {function()} callback
  *     Called when done.
  *
