# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1512078771 18000
#      Thu Nov 30 16:52:51 2017 -0500
# Node ID 3f370672ab4a662b577f2f3dfc4f2f12b0d8d077
# Parent  7c538732ab667ca4c28b37f9f97e0b651db9c16d
Bug 1421768 - Fix overeager clang-tidy behaviour causing silent breakage in searchfox. r=me

This is an import of the change from https://github.com/mozsearch/mozsearch/pull/66
plus a correction in the README file. The change was reviewed in github.

MozReview-Commit-ID: A7gINlBubZ4

diff --git a/build/clang-plugin/mozsearch-plugin/MozsearchIndexer.cpp b/build/clang-plugin/mozsearch-plugin/MozsearchIndexer.cpp
--- a/build/clang-plugin/mozsearch-plugin/MozsearchIndexer.cpp
+++ b/build/clang-plugin/mozsearch-plugin/MozsearchIndexer.cpp
@@ -495,60 +495,60 @@ public:
   bool TraverseEnumDecl(EnumDecl *D) {
     AutoSetContext Asc(this, D);
     return Super::TraverseEnumDecl(D);
   }
   bool TraverseRecordDecl(RecordDecl *D) {
     AutoSetContext Asc(this, D);
     return Super::TraverseRecordDecl(D);
   }
-  bool TraverseCxxRecordDecl(CXXRecordDecl *D) {
+  bool TraverseCXXRecordDecl(CXXRecordDecl *D) {
     AutoSetContext Asc(this, D);
     return Super::TraverseCXXRecordDecl(D);
   }
   bool TraverseFunctionDecl(FunctionDecl *D) {
     AutoSetContext Asc(this, D);
     const FunctionDecl *Def;
     // (See the larger AutoTemplateContext comment for more information.) If a
     // method on a templated class is declared out-of-line, we need to analyze
     // the definition inside the scope of the template or else we won't properly
     // handle member access on the templated type.
     if (TemplateStack && D->isDefined(Def) && Def && D != Def) {
       TraverseFunctionDecl(const_cast<FunctionDecl *>(Def));
     }
     return Super::TraverseFunctionDecl(D);
   }
-  bool TraverseCxxMethodDecl(CXXMethodDecl *D) {
+  bool TraverseCXXMethodDecl(CXXMethodDecl *D) {
     AutoSetContext Asc(this, D);
     const FunctionDecl *Def;
     // See TraverseFunctionDecl.
     if (TemplateStack && D->isDefined(Def) && Def && D != Def) {
       TraverseFunctionDecl(const_cast<FunctionDecl *>(Def));
     }
     return Super::TraverseCXXMethodDecl(D);
   }
-  bool TraverseCxxConstructorDecl(CXXConstructorDecl *D) {
+  bool TraverseCXXConstructorDecl(CXXConstructorDecl *D) {
     AutoSetContext Asc(this, D);
     const FunctionDecl *Def;
     // See TraverseFunctionDecl.
     if (TemplateStack && D->isDefined(Def) && Def && D != Def) {
       TraverseFunctionDecl(const_cast<FunctionDecl *>(Def));
     }
     return Super::TraverseCXXConstructorDecl(D);
   }
-  bool TraverseCxxConversionDecl(CXXConversionDecl *D) {
+  bool TraverseCXXConversionDecl(CXXConversionDecl *D) {
     AutoSetContext Asc(this, D);
     const FunctionDecl *Def;
     // See TraverseFunctionDecl.
     if (TemplateStack && D->isDefined(Def) && Def && D != Def) {
       TraverseFunctionDecl(const_cast<FunctionDecl *>(Def));
     }
     return Super::TraverseCXXConversionDecl(D);
   }
-  bool TraverseCxxDestructorDecl(CXXDestructorDecl *D) {
+  bool TraverseCXXDestructorDecl(CXXDestructorDecl *D) {
     AutoSetContext Asc(this, D);
     const FunctionDecl *Def;
     // See TraverseFunctionDecl.
     if (TemplateStack && D->isDefined(Def) && Def && D != Def) {
       TraverseFunctionDecl(const_cast<FunctionDecl *>(Def));
     }
     return Super::TraverseCXXDestructorDecl(D);
   }
@@ -1138,17 +1138,17 @@ public:
     }
 
     visitIdentifier(Kind, PrettyKind, getQualifiedName(D), Loc, Symbols,
                     getContext(D), Flags, PeekRange);
 
     return true;
   }
 
-  bool VisitCxxConstructExpr(CXXConstructExpr *E) {
+  bool VisitCXXConstructExpr(CXXConstructExpr *E) {
     SourceLocation Loc = E->getLocStart();
     normalizeLocation(&Loc);
     if (!isInterestingLocation(Loc)) {
       return true;
     }
 
     FunctionDecl *Ctor = E->getConstructor();
     if (Ctor->isTemplateInstantiation()) {
@@ -1308,17 +1308,17 @@ public:
       std::string Mangled = getMangledName(CurMangleContext, Decl);
       visitIdentifier("use", "enum", getQualifiedName(Decl), Loc, Mangled,
                       getContext(Loc));
     }
 
     return true;
   }
 
-  bool VisitCxxConstructorDecl(CXXConstructorDecl *D) {
+  bool VisitCXXConstructorDecl(CXXConstructorDecl *D) {
     if (!isInterestingLocation(D->getLocation())) {
       return true;
     }
 
     for (CXXConstructorDecl::init_const_iterator It = D->init_begin();
          It != D->init_end(); ++It) {
       const CXXCtorInitializer *Ci = *It;
       if (!Ci->getMember() || !Ci->isWritten()) {
@@ -1351,17 +1351,17 @@ public:
     if (FieldDecl *Field = dyn_cast<FieldDecl>(Decl)) {
       std::string Mangled = getMangledName(CurMangleContext, Field);
       visitIdentifier("use", "field", getQualifiedName(Field), Loc, Mangled,
                       getContext(Loc));
     }
     return true;
   }
 
-  bool VisitCxxDependentScopeMemberExpr(CXXDependentScopeMemberExpr *E) {
+  bool VisitCXXDependentScopeMemberExpr(CXXDependentScopeMemberExpr *E) {
     SourceLocation Loc = E->getMemberLoc();
     normalizeLocation(&Loc);
     if (!isInterestingLocation(Loc)) {
       return true;
     }
 
     if (TemplateStack) {
       TemplateStack->visitDependent(Loc);
diff --git a/build/clang-plugin/mozsearch-plugin/README b/build/clang-plugin/mozsearch-plugin/README
--- a/build/clang-plugin/mozsearch-plugin/README
+++ b/build/clang-plugin/mozsearch-plugin/README
@@ -1,12 +1,12 @@
 This clang plugin code generates a JSON file for each compiler input
 file. The JSON file contains information about the C++ symbols that
 are referenced by the input file. The data is eventually consumed by
-Searchfox. See https://github.com/bill-mccloskey/mozsearch for more
+Searchfox. See https://github.com/mozsearch/mozsearch for more
 information.
 
 This plugin is enabled with the --enable-clang-plugin and
 --enable-mozsearch-plugin mozconfig options. The output of the plugin
 is stored in $OBJDIR/mozsearch_index.
 
 This code is not a checker, unlike other parts of the Mozilla clang
 plugin. It cannot be used with clang-tidy.
