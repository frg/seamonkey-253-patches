# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1508791604 -3600
# Node ID 135d220e21783e0f4d892687594aedd672ba13da
# Parent  65cec6da9ec7e103f0314cd1fd8f07fea371a7c0
Bug 1411026 - Provide String type conversion for ErrorStatus. r=whimboo

This allows us to construct an ErrorStatus variant from a String.
This is useful if the remote end implementation uses the string
codes for transport.

The new From<String> trait for ErrorStatus is a reverse lookup of
ErrorStatus::error_code.

It currently handles two cases of Selenium errors, which are "element
not visible" and "invalid element coordinates".  Both these have
been deprecated in the WebDriver standard and we need to figure
out a deprecation strategy for them.

MozReview-Commit-ID: 48MAVNQoiKy

diff --git a/testing/webdriver/src/error.rs b/testing/webdriver/src/error.rs
--- a/testing/webdriver/src/error.rs
+++ b/testing/webdriver/src/error.rs
@@ -136,18 +136,18 @@ pub enum ErrorStatus {
 
     UnknownPath,
 
     /// Indicates that a [command] that should have executed properly is not
     /// currently supported.
     UnsupportedOperation,
 }
 
-
 impl ErrorStatus {
+    /// Returns the string serialisation of the error type.
     pub fn error_code(&self) -> &'static str {
         use self::ErrorStatus::*;
         match *self {
             ElementClickIntercepted => "element click intercepted",
             ElementNotInteractable => "element not interactable",
             ElementNotSelectable => "element not selectable",
             InsecureCertificate => "insecure certificate",
             InvalidArgument => "invalid argument",
@@ -173,16 +173,17 @@ impl ErrorStatus {
             UnknownCommand |
             UnknownError => "unknown error",
             UnknownMethod => "unknown method",
             UnknownPath => "unknown command",
             UnsupportedOperation => "unsupported operation",
         }
     }
 
+    /// Returns the correct HTTP status code associated with the error type.
     pub fn http_status(&self) -> StatusCode {
         use self::ErrorStatus::*;
         use self::StatusCode::*;
         match *self {
             ElementClickIntercepted => BadRequest,
             ElementNotInteractable => BadRequest,
             ElementNotSelectable => BadRequest,
             InsecureCertificate => BadRequest,
@@ -210,16 +211,52 @@ impl ErrorStatus {
             UnknownError => InternalServerError,
             UnknownMethod => MethodNotAllowed,
             UnknownPath => NotFound,
             UnsupportedOperation => InternalServerError,
         }
     }
 }
 
+/// Deserialises error type from string.
+impl From<String> for ErrorStatus {
+    fn from(s: String) -> ErrorStatus {
+        use self::ErrorStatus::*;
+        match &*s {
+            "element click intercepted" => ElementClickIntercepted,
+            "element not interactable" | "element not visible" => ElementNotInteractable,
+            "element not selectable" => ElementNotSelectable,
+            "insecure certificate" => InsecureCertificate,
+            "invalid argument" => InvalidArgument,
+            "invalid cookie domain" => InvalidCookieDomain,
+            "invalid coordinates" | "invalid element coordinates" => InvalidCoordinates,
+            "invalid element state" => InvalidElementState,
+            "invalid selector" => InvalidSelector,
+            "invalid session id" => InvalidSessionId,
+            "javascript error" => JavascriptError,
+            "move target out of bounds" => MoveTargetOutOfBounds,
+            "no such alert" => NoSuchAlert,
+            "no such element" => NoSuchElement,
+            "no such frame" => NoSuchFrame,
+            "no such window" => NoSuchWindow,
+            "script timeout" => ScriptTimeout,
+            "session not created" => SessionNotCreated,
+            "stale element reference" => StaleElementReference,
+            "timeout" => Timeout,
+            "unable to capture screen" => UnableToCaptureScreen,
+            "unable to set cookie" => UnableToSetCookie,
+            "unexpected alert open" => UnexpectedAlertOpen,
+            "unknown command" => UnknownCommand,
+            "unknown error" => UnknownError,
+            "unsupported operation" => UnsupportedOperation,
+            _ => UnknownError,
+        }
+    }
+}
+
 pub type WebDriverResult<T> = Result<T, WebDriverError>;
 
 #[derive(Debug)]
 pub struct WebDriverError {
     pub error: ErrorStatus,
     pub message: Cow<'static, str>,
     pub stack: Cow<'static, str>,
     pub delete_session: bool,

