# HG changeset patch
# User Paul Bone <pbone@mozilla.com>
# Date 1510726119 -39600
# Node ID 6d2e96805365cd287128980ddbc3a17b52c64393
# Parent  decea01e1549ba0bf9388c937d3793d7b71f0a70
Bug 1417315 - Calculate lazy_capacity perf value before resizing the nursery r=sfink

diff --git a/js/src/gc/Nursery.cpp b/js/src/gc/Nursery.cpp
--- a/js/src/gc/Nursery.cpp
+++ b/js/src/gc/Nursery.cpp
@@ -551,19 +551,18 @@ js::Nursery::renderProfileJSON(JSONPrint
 
     json.property("reason", JS::gcreason::ExplainReason(previousGC.reason));
     json.property("bytes_tenured", previousGC.tenuredBytes);
     json.property("bytes_used", previousGC.nurseryUsedBytes);
     json.property("cur_capacity", previousGC.nurseryCapacity);
     const size_t newCapacity = spaceToEnd(maxChunkCount());
     if (newCapacity != previousGC.nurseryCapacity)
         json.property("new_capacity", newCapacity);
-    const size_t lazyCapacity = spaceToEnd(allocatedChunkCount());
-    if (lazyCapacity != previousGC.nurseryCapacity)
-        json.property("lazy_capacity", lazyCapacity);
+    if (previousGC.nurseryLazyCapacity != previousGC.nurseryCapacity)
+        json.property("lazy_capacity", previousGC.nurseryLazyCapacity);
     if (!timeInChunkAlloc_.IsZero())
         json.property("chunk_alloc_us", timeInChunkAlloc_, json.MICROSECONDS);
 
     json.beginObjectProperty("phase_times");
 
 #define EXTRACT_NAME(name, text) #name,
     static const char* names[] = {
 FOR_EACH_NURSERY_PROFILE_TIME(EXTRACT_NAME)
@@ -677,16 +676,17 @@ js::Nursery::collect(JS::gcreason::Reaso
 
     TenureCountCache tenureCounts;
     previousGC.reason = JS::gcreason::NO_REASON;
     if (!isEmpty()) {
         doCollection(reason, tenureCounts);
     } else {
         previousGC.nurseryUsedBytes = 0;
         previousGC.nurseryCapacity = spaceToEnd(maxChunkCount());
+        previousGC.nurseryLazyCapacity = spaceToEnd(allocatedChunkCount());
         previousGC.tenuredBytes = 0;
     }
 
     // Resize the nursery.
     maybeResizeNursery(reason);
 
     // If we are promoting the nursery, or exhausted the store buffer with
     // pointers to nursery things, which will force a collection well before
@@ -864,16 +864,17 @@ js::Nursery::doCollection(JS::gcreason::
 #ifdef JS_GC_ZEAL
     if (rt->hasZealMode(ZealMode::CheckHashTablesOnMinorGC))
         CheckHashTablesAfterMovingGC(rt);
 #endif
     endProfile(ProfileKey::CheckHashTables);
 
     previousGC.reason = reason;
     previousGC.nurseryCapacity = initialNurseryCapacity;
+    previousGC.nurseryLazyCapacity = spaceToEnd(allocatedChunkCount());
     previousGC.nurseryUsedBytes = initialNurseryUsedBytes;
     previousGC.tenuredBytes = mover.tenuredSize;
 }
 
 void
 js::Nursery::FreeMallocedBuffersTask::transferBuffersToFree(MallocedBuffersSet& buffersToFree,
                                                             const AutoLockHelperThreadState& lock)
 {
diff --git a/js/src/gc/Nursery.h b/js/src/gc/Nursery.h
--- a/js/src/gc/Nursery.h
+++ b/js/src/gc/Nursery.h
@@ -376,16 +376,17 @@ class Nursery
 
     /*
      * This data is initialised only if the nursery is enabled and after at
      * least one call to Nursery::collect()
      */
     struct {
         JS::gcreason::Reason reason;
         size_t nurseryCapacity;
+        size_t nurseryLazyCapacity;
         size_t nurseryUsedBytes;
         size_t tenuredBytes;
     } previousGC;
 
     /*
      * Calculate the promotion rate of the most recent minor GC.
      * The valid_for_tenuring parameter is used to return whether this
      * promotion rate is accurate enough (the nursery was full enough) to be
