# HG changeset patch
# User Cameron McCormack <cam@mcc.id.au>
# Date 1508486207 18000
# Node ID 34c19b292f03597da37a5b0f652046fce22718b4
# Parent  4d671c1104c0e21824814cc389e97219dac655ca
servo: Merge #18936 - style: Keep track of document state dependencies (from heycam:document-state); r=emilio

Servo half of https://bugzilla.mozilla.org/show_bug.cgi?id=1390694, reviewed there by Emilio.

Source-Repo: https://github.com/servo/servo
Source-Revision: 44eeb1999b2bf1729ff8ae41438c687f6380e33d

diff --git a/servo/components/style/element_state.rs b/servo/components/style/element_state.rs
--- a/servo/components/style/element_state.rs
+++ b/servo/components/style/element_state.rs
@@ -139,15 +139,16 @@ bitflags! {
     }
 }
 
 bitflags! {
     /// Event-based document states.
     ///
     /// NB: Is important for this to remain in sync with Gecko's
     /// dom/base/nsIDocument.h.
+    #[derive(MallocSizeOf)]
     pub flags DocumentState: u64 {
         /// RTL locale: specific to the XUL localedir attribute
         const NS_DOCUMENT_STATE_RTL_LOCALE = 1 << 0,
         /// Window activation status
         const NS_DOCUMENT_STATE_WINDOW_INACTIVE = 1 << 1,
     }
 }
diff --git a/servo/components/style/gecko/selector_parser.rs b/servo/components/style/gecko/selector_parser.rs
--- a/servo/components/style/gecko/selector_parser.rs
+++ b/servo/components/style/gecko/selector_parser.rs
@@ -1,16 +1,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Gecko-specific bits for selector-parsing.
 
 use cssparser::{BasicParseError, BasicParseErrorKind, Parser, ToCss, Token, CowRcStr, SourceLocation};
-use element_state::ElementState;
+use element_state::{self, DocumentState, ElementState};
 use gecko_bindings::structs::CSSPseudoClassType;
 use gecko_bindings::structs::RawServoSelectorList;
 use gecko_bindings::sugar::ownership::{HasBoxFFI, HasFFI, HasSimpleFFI};
 use selector_parser::{SelectorParser, PseudoElementCascadeType};
 use selectors::SelectorList;
 use selectors::parser::{Selector, SelectorMethods, SelectorParseErrorKind};
 use selectors::visitor::SelectorVisitor;
 use std::fmt;
@@ -190,16 +190,25 @@ impl NonTSPseudoClass {
                     $(NonTSPseudoClass::$k_name(..) => flag!($k_state),)*
                     NonTSPseudoClass::MozAny(..) => ElementState::empty(),
                 }
             }
         }
         apply_non_ts_list!(pseudo_class_state)
     }
 
+    /// Get the document state flag associated with a pseudo-class, if any.
+    pub fn document_state_flag(&self) -> DocumentState {
+        match *self {
+            NonTSPseudoClass::MozLocaleDir(..) => element_state::NS_DOCUMENT_STATE_RTL_LOCALE,
+            NonTSPseudoClass::MozWindowInactive => element_state::NS_DOCUMENT_STATE_WINDOW_INACTIVE,
+            _ => DocumentState::empty(),
+        }
+    }
+
     /// Returns true if the given pseudoclass should trigger style sharing cache
     /// revalidation.
     pub fn needs_cache_revalidation(&self) -> bool {
         self.state_flag().is_empty() &&
         !matches!(*self,
                   // :-moz-any is handled by the revalidation visitor walking
                   // the things inside it; it does not need to cause
                   // revalidation on its own.
@@ -428,23 +437,16 @@ impl SelectorImpl {
     #[inline]
     pub fn each_eagerly_cascaded_pseudo_element<F>(mut fun: F)
         where F: FnMut(PseudoElement),
     {
         for pseudo in &EAGER_PSEUDOS {
             fun(pseudo.clone())
         }
     }
-
-
-    /// Returns the relevant state flag for a given non-tree-structural
-    /// pseudo-class.
-    pub fn pseudo_class_state_flag(pc: &NonTSPseudoClass) -> ElementState {
-        pc.state_flag()
-    }
 }
 
 fn utf16_to_ascii_lowercase(unit: u16) -> u16 {
     match unit {
         65...90 => unit + 32, // A-Z => a-z
         _ => unit
     }
 }
diff --git a/servo/components/style/servo/selector_parser.rs b/servo/components/style/servo/selector_parser.rs
--- a/servo/components/style/servo/selector_parser.rs
+++ b/servo/components/style/servo/selector_parser.rs
@@ -5,17 +5,17 @@
 #![deny(missing_docs)]
 
 //! Servo's selector parser.
 
 use {Atom, Prefix, Namespace, LocalName, CaseSensitivityExt};
 use attr::{AttrIdentifier, AttrValue};
 use cssparser::{Parser as CssParser, ToCss, serialize_identifier, CowRcStr, SourceLocation};
 use dom::{OpaqueNode, TElement, TNode};
-use element_state::ElementState;
+use element_state::{DocumentState, ElementState};
 use fnv::FnvHashMap;
 use invalidation::element::element_wrapper::ElementSnapshot;
 use properties::ComputedValues;
 use properties::PropertyFlags;
 use properties::longhands::display::computed_value as display;
 use selector_parser::{AttrValue as SelectorAttrValue, PseudoElementCascadeType, SelectorParser};
 use selectors::attr::{AttrSelectorOperation, NamespaceConstraint, CaseSensitivity};
 use selectors::parser::{SelectorMethods, SelectorParseErrorKind};
@@ -348,16 +348,21 @@ impl NonTSPseudoClass {
             Lang(_) |
             Link |
             Visited |
             ServoNonZeroBorder |
             ServoCaseSensitiveTypeAttr(_) => ElementState::empty(),
         }
     }
 
+    /// Get the document state flag associated with a pseudo-class, if any.
+    pub fn document_state_flag(&self) -> DocumentState {
+        DocumentState::empty()
+    }
+
     /// Returns true if the given pseudoclass should trigger style sharing cache revalidation.
     pub fn needs_cache_revalidation(&self) -> bool {
         self.state_flag().is_empty()
     }
 
     /// Returns true if the evaluation of the pseudo-class depends on the
     /// element's attributes.
     pub fn is_attr_based(&self) -> bool {
@@ -556,22 +561,16 @@ impl SelectorImpl {
     #[inline]
     pub fn each_eagerly_cascaded_pseudo_element<F>(mut fun: F)
         where F: FnMut(PseudoElement),
     {
         for i in 0..EAGER_PSEUDO_COUNT {
             fun(PseudoElement::from_eager_index(i));
         }
     }
-
-    /// Returns the pseudo-class state flag for selector matching.
-    #[inline]
-    pub fn pseudo_class_state_flag(pc: &NonTSPseudoClass) -> ElementState {
-        pc.state_flag()
-    }
 }
 
 /// A map from elements to snapshots for the Servo style back-end.
 #[derive(Debug)]
 pub struct SnapshotMap(FnvHashMap<OpaqueNode, ServoElementSnapshot>);
 
 impl SnapshotMap {
     /// Create a new empty `SnapshotMap`.
diff --git a/servo/components/style/stylist.rs b/servo/components/style/stylist.rs
--- a/servo/components/style/stylist.rs
+++ b/servo/components/style/stylist.rs
@@ -3,17 +3,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Selector matching.
 
 use {Atom, LocalName, Namespace};
 use applicable_declarations::{ApplicableDeclarationBlock, ApplicableDeclarationList};
 use context::{CascadeInputs, QuirksMode};
 use dom::TElement;
-use element_state::ElementState;
+use element_state::{DocumentState, ElementState};
 use font_metrics::FontMetricsProvider;
 #[cfg(feature = "gecko")]
 use gecko_bindings::structs::{nsIAtom, ServoStyleSetSizes, StyleRuleInclusion};
 use hashglobe::FailedAllocationError;
 use invalidation::element::invalidation_map::InvalidationMap;
 use invalidation::media_queries::{EffectiveMediaQueryResults, ToMediaListKey};
 #[cfg(feature = "gecko")]
 use malloc_size_of::{MallocShallowSizeOf, MallocSizeOf, MallocSizeOfOps};
@@ -619,30 +619,32 @@ impl Stylist {
                 .iter_origins()
                 .any(|(d, _)| {
                     d.attribute_dependencies
                         .might_contain_hash(local_name.get_hash())
                 })
         }
     }
 
-    /// Returns whether the given ElementState bit might be relied upon by a
-    /// selector of some rule in the stylist.
-    pub fn might_have_state_dependency(&self, state: ElementState) -> bool {
-        self.has_state_dependency(state)
-    }
-
     /// Returns whether the given ElementState bit is relied upon by a selector
     /// of some rule in the stylist.
     pub fn has_state_dependency(&self, state: ElementState) -> bool {
         self.cascade_data
             .iter_origins()
             .any(|(d, _)| d.state_dependencies.intersects(state))
     }
 
+    /// Returns whether the given DocumentState bit is relied upon by a selector
+    /// of some rule in the stylist.
+    pub fn has_document_state_dependency(&self, state: DocumentState) -> bool {
+        self.cascade_data
+            .iter_origins()
+            .any(|(d, _)| d.document_state_dependencies.intersects(state))
+    }
+
     /// Computes the style for a given "precomputed" pseudo-element, taking the
     /// universal rules and applying them.
     ///
     /// If `inherit_all` is true, then all properties are inherited from the
     /// parent; otherwise, non-inherited properties are reset to their initial
     /// values. The flow constructor uses this flag when constructing anonymous
     /// flows.
     pub fn precomputed_values_for_pseudo(
@@ -1708,16 +1710,18 @@ struct StylistSelectorVisitor<'a> {
     /// selectors.
     mapped_ids: &'a mut NonCountingBloomFilter,
     /// The filter with the local names of attributes there are selectors for.
     attribute_dependencies: &'a mut NonCountingBloomFilter,
     /// Whether there's any attribute selector for the [style] attribute.
     style_attribute_dependency: &'a mut bool,
     /// All the states selectors in the page reference.
     state_dependencies: &'a mut ElementState,
+    /// All the document states selectors in the page reference.
+    document_state_dependencies: &'a mut DocumentState,
 }
 
 fn component_needs_revalidation(
     c: &Component<SelectorImpl>,
     passed_rightmost_selector: bool,
 ) -> bool {
     match *c {
         Component::ID(_) => {
@@ -1795,16 +1799,17 @@ impl<'a> SelectorVisitor for StylistSele
     fn visit_simple_selector(&mut self, s: &Component<SelectorImpl>) -> bool {
         self.needs_revalidation =
             self.needs_revalidation ||
             component_needs_revalidation(s, self.passed_rightmost_selector);
 
         match *s {
             Component::NonTSPseudoClass(ref p) => {
                 self.state_dependencies.insert(p.state_flag());
+                self.document_state_dependencies.insert(p.document_state_flag());
             }
             Component::ID(ref id) if !self.passed_rightmost_selector => {
                 // We want to stop storing mapped ids as soon as we've moved off
                 // the rightmost ComplexSelector that is not a pseudo-element.
                 //
                 // That can be detected by a visit_complex_selector call with a
                 // combinator other than None and PseudoElement.
                 //
@@ -1863,16 +1868,21 @@ struct CascadeData {
     /// returning `true` for `"style"` just due to a hash collision.)
     style_attribute_dependency: bool,
 
     /// The element state bits that are relied on by selectors.  Like
     /// `attribute_dependencies`, this is used to avoid taking element snapshots
     /// when an irrelevant element state bit changes.
     state_dependencies: ElementState,
 
+    /// The document state bits that are relied on by selectors.  This is used
+    /// to tell whether we need to restyle the entire document when a document
+    /// state bit changes.
+    document_state_dependencies: DocumentState,
+
     /// The ids that appear in the rightmost complex selector of selectors (and
     /// hence in our selector maps).  Used to determine when sharing styles is
     /// safe: we disallow style sharing for elements whose id matches this
     /// filter, and hence might be in one of our selector maps.
     #[cfg_attr(feature = "servo", ignore_malloc_size_of = "just an array")]
     mapped_ids: NonCountingBloomFilter,
 
     /// Selectors that require explicit cache revalidation (i.e. which depend
@@ -1904,16 +1914,17 @@ impl CascadeData {
             element_map: SelectorMap::new(),
             pseudos_map: PerPseudoElementMap::default(),
             animations: Default::default(),
             extra_data: ExtraStyleData::default(),
             invalidation_map: InvalidationMap::new(),
             attribute_dependencies: NonCountingBloomFilter::new(),
             style_attribute_dependency: false,
             state_dependencies: ElementState::empty(),
+            document_state_dependencies: DocumentState::empty(),
             mapped_ids: NonCountingBloomFilter::new(),
             selectors_for_cache_revalidation: SelectorMap::new(),
             effective_media_query_results: EffectiveMediaQueryResults::new(),
             rules_source_order: 0,
             num_selectors: 0,
             num_declarations: 0,
         }
     }
@@ -2068,16 +2079,17 @@ impl CascadeData {
                             self.invalidation_map
                                 .note_selector(selector, quirks_mode)?;
                             let mut visitor = StylistSelectorVisitor {
                                 needs_revalidation: false,
                                 passed_rightmost_selector: false,
                                 attribute_dependencies: &mut self.attribute_dependencies,
                                 style_attribute_dependency: &mut self.style_attribute_dependency,
                                 state_dependencies: &mut self.state_dependencies,
+                                document_state_dependencies: &mut self.document_state_dependencies,
                                 mapped_ids: &mut self.mapped_ids,
                             };
 
                             selector.visit(&mut visitor);
 
                             if visitor.needs_revalidation {
                                 self.selectors_for_cache_revalidation.insert(
                                     RevalidationSelectorAndHashes::new(selector.clone(), hashes),
@@ -2256,16 +2268,17 @@ impl CascadeData {
 
     fn clear(&mut self) {
         self.clear_cascade_data();
         self.effective_media_query_results.clear();
         self.invalidation_map.clear();
         self.attribute_dependencies.clear();
         self.style_attribute_dependency = false;
         self.state_dependencies = ElementState::empty();
+        self.document_state_dependencies = DocumentState::empty();
         self.mapped_ids.clear();
         self.selectors_for_cache_revalidation.clear();
     }
 
     /// Measures heap usage.
     #[cfg(feature = "gecko")]
     pub fn add_size_of(&self, ops: &mut MallocSizeOfOps, sizes: &mut ServoStyleSetSizes) {
         sizes.mElementAndPseudosMaps += self.element_map.size_of(ops);
@@ -2364,19 +2377,21 @@ impl Rule {
 }
 
 /// A function to be able to test the revalidation stuff.
 pub fn needs_revalidation_for_testing(s: &Selector<SelectorImpl>) -> bool {
     let mut attribute_dependencies = NonCountingBloomFilter::new();
     let mut mapped_ids = NonCountingBloomFilter::new();
     let mut style_attribute_dependency = false;
     let mut state_dependencies = ElementState::empty();
+    let mut document_state_dependencies = DocumentState::empty();
     let mut visitor = StylistSelectorVisitor {
         needs_revalidation: false,
         passed_rightmost_selector: false,
         attribute_dependencies: &mut attribute_dependencies,
         style_attribute_dependency: &mut style_attribute_dependency,
         state_dependencies: &mut state_dependencies,
+        document_state_dependencies: &mut document_state_dependencies,
         mapped_ids: &mut mapped_ids,
     };
     s.visit(&mut visitor);
     visitor.needs_revalidation
 }
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -16,17 +16,17 @@ use std::iter;
 use std::mem;
 use std::ptr;
 use style::applicable_declarations::ApplicableDeclarationBlock;
 use style::context::{CascadeInputs, QuirksMode, SharedStyleContext, StyleContext};
 use style::context::ThreadLocalStyleContext;
 use style::data::{ElementStyles, self};
 use style::dom::{ShowSubtreeData, TElement, TNode};
 use style::driver;
-use style::element_state::ElementState;
+use style::element_state::{DocumentState, ElementState};
 use style::error_reporting::{NullReporter, ParseErrorReporter};
 use style::font_metrics::{FontMetricsProvider, get_metrics_provider_for_product};
 use style::gecko::data::{GeckoStyleSheet, PerDocumentStyleData, PerDocumentStyleDataImpl};
 use style::gecko::global_style_data::{GLOBAL_STYLE_DATA, GlobalStyleData, STYLE_THREAD_POOL};
 use style::gecko::restyle_damage::GeckoRestyleDamage;
 use style::gecko::selector_parser::PseudoElement;
 use style::gecko::traversal::RecalcStyleOnly;
 use style::gecko::wrapper::GeckoElement;
@@ -4155,30 +4155,41 @@ pub extern "C" fn Servo_StyleSet_HasStat
     element: RawGeckoElementBorrowed,
     state: u64,
 ) -> bool {
     let element = GeckoElement(element);
 
     let state = ElementState::from_bits_truncate(state);
     let data = PerDocumentStyleData::from_ffi(raw_data).borrow();
 
-    let mut has_dep = data.stylist.might_have_state_dependency(state);
+    let mut has_dep = data.stylist.has_state_dependency(state);
     if !has_dep {
         // TODO(emilio): Consider optimizing this storing attribute
         // dependencies from UA sheets separately, so we could optimize
         // the above lookup if cut_off_inheritance is true.
         element.each_xbl_stylist(|stylist| {
-            has_dep = has_dep || stylist.might_have_state_dependency(state);
+            has_dep = has_dep || stylist.has_state_dependency(state);
         });
     }
 
     has_dep
 }
 
 #[no_mangle]
+pub extern "C" fn Servo_StyleSet_HasDocumentStateDependency(
+    raw_data: RawServoStyleSetBorrowed,
+    state: u64,
+) -> bool {
+    let state = DocumentState::from_bits_truncate(state);
+    let data = PerDocumentStyleData::from_ffi(raw_data).borrow();
+
+    data.stylist.has_document_state_dependency(state)
+}
+
+#[no_mangle]
 pub extern "C" fn Servo_GetCustomPropertyValue(
     computed_values: ServoStyleContextBorrowed,
     name: *const nsAString,
     value: *mut nsAString,
 ) -> bool {
     let custom_properties = match computed_values.custom_properties() {
         Some(p) => p,
         None => return false,
