# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1508913953 -7200
#      Wed Oct 25 08:45:53 2017 +0200
# Node ID bf20f2a1fa2bf6bb4ae2f782377fbc58994862ca
# Parent  e4a6b90fc736dfbcf60c5c6f8cd6915e6f4a6e44
Bug 1411257 - No MOZ_CRASH if BackgroundChild::GetOrCreateForCurrentThread() fails - part 8 - WebAuthn API, r=asuth

diff --git a/dom/webauthn/WebAuthnManager.cpp b/dom/webauthn/WebAuthnManager.cpp
--- a/dom/webauthn/WebAuthnManager.cpp
+++ b/dom/webauthn/WebAuthnManager.cpp
@@ -231,41 +231,42 @@ WebAuthnManager::~WebAuthnManager()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   if (mTransaction.isSome()) {
     RejectTransaction(NS_ERROR_ABORT);
   }
 }
 
-void
-WebAuthnManager::GetOrCreateBackgroundActor()
+bool
+WebAuthnManager::MaybeCreateBackgroundActor()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   if (mChild) {
-    return;
+    return true;
   }
 
   PBackgroundChild* actor = BackgroundChild::GetOrCreateForCurrentThread();
   if (NS_WARN_IF(!actor)) {
-    MOZ_CRASH("Failed to create a PBackgroundChild actor!");
+    return false;
   }
 
   RefPtr<WebAuthnTransactionChild> mgr(new WebAuthnTransactionChild());
   PWebAuthnTransactionChild* constructedMgr =
     actor->SendPWebAuthnTransactionConstructor(mgr);
 
   if (NS_WARN_IF(!constructedMgr)) {
-    MOZ_CRASH("Failed to create a PBackgroundChild actor!");
-    return;
+    return false;
   }
 
   MOZ_ASSERT(constructedMgr == mgr);
   mChild = mgr.forget();
+
+  return true;
 }
 
 //static
 WebAuthnManager*
 WebAuthnManager::GetOrCreate()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
@@ -453,16 +454,21 @@ WebAuthnManager::MakeCredential(nsPIDOMW
   for (const auto& s: aOptions.mExcludeCredentials) {
     WebAuthnScopedCredentialDescriptor c;
     CryptoBuffer cb;
     cb.Assign(s.mId);
     c.id() = cb;
     excludeList.AppendElement(c);
   }
 
+  if (!MaybeCreateBackgroundActor()) {
+    promise->MaybeReject(NS_ERROR_DOM_NOT_ALLOWED_ERR);
+    return promise.forget();
+  }
+
   // TODO: Add extension list building
   nsTArray<WebAuthnExtension> extensions;
 
   WebAuthnTransactionInfo info(rpIdHash,
                                clientDataHash,
                                adjustedTimeout,
                                excludeList,
                                extensions);
@@ -470,20 +476,17 @@ WebAuthnManager::MakeCredential(nsPIDOMW
   ListenForVisibilityEvents(aParent, this);
 
   MOZ_ASSERT(mTransaction.isNothing());
   mTransaction = Some(WebAuthnTransaction(aParent,
                                           promise,
                                           Move(info),
                                           Move(clientDataJSON)));
 
-  GetOrCreateBackgroundActor();
-  if (mChild) {
-    mChild->SendRequestRegister(mTransaction.ref().mInfo);
-  }
+  mChild->SendRequestRegister(mTransaction.ref().mInfo);
 
   return promise.forget();
 }
 
 already_AddRefed<Promise>
 WebAuthnManager::GetAssertion(nsPIDOMWindowInner* aParent,
                               const PublicKeyCredentialRequestOptions& aOptions)
 {
@@ -596,16 +599,21 @@ WebAuthnManager::GetAssertion(nsPIDOMWin
   for (const auto& s: aOptions.mAllowCredentials) {
     WebAuthnScopedCredentialDescriptor c;
     CryptoBuffer cb;
     cb.Assign(s.mId);
     c.id() = cb;
     allowList.AppendElement(c);
   }
 
+  if (!MaybeCreateBackgroundActor()) {
+    promise->MaybeReject(NS_ERROR_DOM_NOT_ALLOWED_ERR);
+    return promise.forget();
+  }
+
   // TODO: Add extension list building
   // If extensions was specified, process any extensions supported by this
   // client platform, to produce the extension data that needs to be sent to the
   // authenticator. If an error is encountered while processing an extension,
   // skip that extension and do not produce any extension data for it. Call the
   // result of this processing clientExtensions.
   nsTArray<WebAuthnExtension> extensions;
 
@@ -618,20 +626,17 @@ WebAuthnManager::GetAssertion(nsPIDOMWin
   ListenForVisibilityEvents(aParent, this);
 
   MOZ_ASSERT(mTransaction.isNothing());
   mTransaction = Some(WebAuthnTransaction(aParent,
                                           promise,
                                           Move(info),
                                           Move(clientDataJSON)));
 
-  GetOrCreateBackgroundActor();
-  if (mChild) {
-    mChild->SendRequestSign(mTransaction.ref().mInfo);
-  }
+  mChild->SendRequestSign(mTransaction.ref().mInfo);
 
   return promise.forget();
 }
 
 already_AddRefed<Promise>
 WebAuthnManager::Store(nsPIDOMWindowInner* aParent,
                        const Credential& aCredential)
 {
diff --git a/dom/webauthn/WebAuthnManager.h b/dom/webauthn/WebAuthnManager.h
--- a/dom/webauthn/WebAuthnManager.h
+++ b/dom/webauthn/WebAuthnManager.h
@@ -128,17 +128,17 @@ private:
   // Rejects the current transaction and calls ClearTransaction().
   void RejectTransaction(const nsresult& aError);
   // Cancels the current transaction (by sending a Cancel message to the
   // parent) and rejects it by calling RejectTransaction().
   void CancelTransaction(const nsresult& aError);
 
   typedef MozPromise<nsresult, nsresult, false> BackgroundActorPromise;
 
-  void GetOrCreateBackgroundActor();
+  bool MaybeCreateBackgroundActor();
 
   // IPC Channel for the current transaction.
   RefPtr<WebAuthnTransactionChild> mChild;
 
   // The current transaction, if any.
   Maybe<WebAuthnTransaction> mTransaction;
 };
 
