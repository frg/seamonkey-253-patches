# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1505895151 -28800
#      Wed Sep 20 16:12:31 2017 +0800
# Node ID 6726a06cf08013df27a074e90434c33428bf1b90
# Parent  384e0cd8466a0f634e21054dee36c9835e770b79
Bug 1401471. P1 - make StreamAction a struct so we can associate data with each action in the future. r=gerald

MozReview-Commit-ID: A0ZpunJgNYm

diff --git a/dom/media/MediaCache.cpp b/dom/media/MediaCache.cpp
--- a/dom/media/MediaCache.cpp
+++ b/dom/media/MediaCache.cpp
@@ -1126,23 +1126,33 @@ MediaCache::PredictNextUseForIncomingDat
   }
   if (bytesAhead <= 0)
     return TimeDuration(0);
   int64_t millisecondsAhead = bytesAhead*1000/aStream->mPlaybackBytesPerSecond;
   return TimeDuration::FromMilliseconds(
       std::min<int64_t>(millisecondsAhead, INT32_MAX));
 }
 
-enum StreamAction { NONE, SEEK, SEEK_AND_RESUME, RESUME, SUSPEND };
-
 void
 MediaCache::Update()
 {
   NS_ASSERTION(NS_IsMainThread(), "Only call on main thread");
 
+  struct StreamAction
+  {
+    enum
+    {
+      NONE,
+      SEEK,
+      SEEK_AND_RESUME,
+      RESUME,
+      SUSPEND
+    } mTag = NONE;
+  };
+
   // The action to use for each stream. We store these so we can make
   // decisions while holding the cache lock but implement those decisions
   // without holding the cache lock, since we need to call out to
   // stream, decoder and element code.
   AutoTArray<StreamAction,10> actions;
 
   {
     ReentrantMonitorAutoEnter mon(mReentrantMonitor);
@@ -1253,17 +1263,17 @@ MediaCache::Update()
         latestNextUse = PredictNextUse(now, reusableBlock);
       }
     }
 
     int32_t resumeThreshold = Preferences::GetInt("media.cache_resume_threshold", 10);
     int32_t readaheadLimit = Preferences::GetInt("media.cache_readahead_limit", 30);
 
     for (uint32_t i = 0; i < mStreams.Length(); ++i) {
-      actions.AppendElement(NONE);
+      actions.AppendElement(StreamAction{});
 
       MediaCacheStream* stream = mStreams[i];
       if (stream->mClosed) {
         LOG("Stream %p closed", stream);
         continue;
       }
 
       // Figure out where we should be reading from. It's the first
@@ -1391,25 +1401,27 @@ MediaCache::Update()
         // We need to seek now.
         NS_ASSERTION(stream->mIsTransportSeekable || desiredOffset == 0,
                      "Trying to seek in a non-seekable stream!");
         // Round seek offset down to the start of the block. This is essential
         // because we don't want to think we have part of a block already
         // in mPartialBlockBuffer.
         stream->mChannelOffset =
           OffsetToBlockIndexUnchecked(desiredOffset) * BLOCK_SIZE;
-        actions[i] = stream->mCacheSuspended ? SEEK_AND_RESUME : SEEK;
+        actions[i].mTag = stream->mCacheSuspended
+                            ? StreamAction::SEEK_AND_RESUME
+                            : StreamAction::SEEK;
         // mChannelOffset is updated to a new position. We don't want data from
         // the old channel to be written to the wrong position. 0 is a sentinel
         // value which will not match any ID passed to NotifyDataReceived().
         stream->mLoadID = 0;
       } else if (enableReading && stream->mCacheSuspended) {
-        actions[i] = RESUME;
+        actions[i].mTag = StreamAction::RESUME;
       } else if (!enableReading && !stream->mCacheSuspended) {
-        actions[i] = SUSPEND;
+        actions[i].mTag = StreamAction::SUSPEND;
       }
     }
 #ifdef DEBUG
     mInUpdate = false;
 #endif
   }
 
   // Update the channel state without holding our cache lock. While we're
@@ -1420,57 +1432,60 @@ MediaCache::Update()
   // the action, which can only be written by the main thread (i.e., this
   // thread), so we don't have races here.
 
   // First, update the mCacheSuspended/mCacheEnded flags so that they're all correct
   // when we fire our CacheClient commands below. Those commands can rely on these flags
   // being set correctly for all streams.
   for (uint32_t i = 0; i < mStreams.Length(); ++i) {
     MediaCacheStream* stream = mStreams[i];
-    switch (actions[i]) {
-    case SEEK:
-	case SEEK_AND_RESUME:
-      stream->mCacheSuspended = false;
-      stream->mChannelEnded = false;
-      break;
-    case RESUME:
-      stream->mCacheSuspended = false;
-      break;
-    case SUSPEND:
-      stream->mCacheSuspended = true;
-      break;
-    default:
-      break;
+    switch (actions[i].mTag) {
+      case StreamAction::SEEK:
+      case StreamAction::SEEK_AND_RESUME:
+        stream->mCacheSuspended = false;
+        stream->mChannelEnded = false;
+        break;
+      case StreamAction::RESUME:
+        stream->mCacheSuspended = false;
+        break;
+      case StreamAction::SUSPEND:
+        stream->mCacheSuspended = true;
+        break;
+      default:
+        break;
     }
   }
 
   for (uint32_t i = 0; i < mStreams.Length(); ++i) {
     MediaCacheStream* stream = mStreams[i];
     nsresult rv;
-    switch (actions[i]) {
-    case SEEK:
-	case SEEK_AND_RESUME:
-      LOG("Stream %p CacheSeek to %" PRId64 " (resume=%d)", stream,
-          stream->mChannelOffset, actions[i] == SEEK_AND_RESUME);
-      rv = stream->mClient->CacheClientSeek(stream->mChannelOffset,
-                                            actions[i] == SEEK_AND_RESUME);
-      break;
-    case RESUME:
-      LOG("Stream %p Resumed", stream);
-      rv = stream->mClient->CacheClientResume();
-      QueueSuspendedStatusUpdate(stream->mResourceID);
-      break;
-    case SUSPEND:
-      LOG("Stream %p Suspended", stream);
-      rv = stream->mClient->CacheClientSuspend();
-      QueueSuspendedStatusUpdate(stream->mResourceID);
-      break;
-    default:
-      rv = NS_OK;
-      break;
+    switch (actions[i].mTag) {
+      case StreamAction::SEEK:
+      case StreamAction::SEEK_AND_RESUME:
+        LOG("Stream %p CacheSeek to %" PRId64 " (resume=%d)",
+            stream,
+            stream->mChannelOffset,
+            actions[i].mTag == StreamAction::SEEK_AND_RESUME);
+        rv = stream->mClient->CacheClientSeek(stream->mChannelOffset,
+                                              actions[i].mTag ==
+                                                StreamAction::SEEK_AND_RESUME);
+        break;
+      case StreamAction::RESUME:
+        LOG("Stream %p Resumed", stream);
+        rv = stream->mClient->CacheClientResume();
+        QueueSuspendedStatusUpdate(stream->mResourceID);
+        break;
+      case StreamAction::SUSPEND:
+        LOG("Stream %p Suspended", stream);
+        rv = stream->mClient->CacheClientSuspend();
+        QueueSuspendedStatusUpdate(stream->mResourceID);
+        break;
+      default:
+        rv = NS_OK;
+        break;
     }
 
     if (NS_FAILED(rv)) {
       // Close the streams that failed due to error. This will cause all
       // client Read and Seek operations on those streams to fail. Blocked
       // Reads will also be woken up.
       stream->mClient->Close();
     }
