# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1507409167 -39600
# Node ID d16958d3126b9dea70ee9849779fd4afc41ff0ac
# Parent  d9f54aa4a46108e55e6de6a7f3bdf7d74a67aeff
Bug 1406281 - Restyle CallbackAndVarCacheOrder.cpp. r=erahm.

This is a mixture of clang-format and manual restyling.

MozReview-Commit-ID: ApLqaQ3KZ5S

diff --git a/modules/libpref/test/gtest/CallbackAndVarCacheOrder.cpp b/modules/libpref/test/gtest/CallbackAndVarCacheOrder.cpp
--- a/modules/libpref/test/gtest/CallbackAndVarCacheOrder.cpp
+++ b/modules/libpref/test/gtest/CallbackAndVarCacheOrder.cpp
@@ -1,152 +1,165 @@
-/* -*-  Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2; -*- */
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 // Test that var caches are updated before callbacks.
 
 #include "gtest/gtest.h"
 #include "Preferences.h"
 
 namespace mozilla {
 
 template<typename T, typename U>
-struct Closure {
-  U* location;
-  T expected;
-  bool called;
+struct Closure
+{
+  U* mLocation;
+  T mExpected;
+  bool mCalled;
 };
 
 template<typename T, typename U>
-void VarChanged(const char* aPrefName, void* aData)
+void
+VarChanged(const char* aPrefName, void* aData)
 {
   auto closure = static_cast<Closure<T, U>*>(aData);
-  ASSERT_EQ(*closure->location, closure->expected);
-  ASSERT_FALSE(closure->called);
-  closure->called = true;
+  ASSERT_EQ(*closure->mLocation, closure->mExpected);
+  ASSERT_FALSE(closure->mCalled);
+  closure->mCalled = true;
 }
 
-void SetFunc(const char* prefName, bool value)
+void
+SetFunc(const char* aPrefName, bool aValue)
 {
-  nsresult rv = Preferences::SetBool(prefName, value);
+  nsresult rv = Preferences::SetBool(aPrefName, aValue);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 }
 
-void SetFunc(const char* prefName, int32_t value)
+void
+SetFunc(const char* aPrefName, int32_t aValue)
 {
-  nsresult rv = Preferences::SetInt(prefName, value);
+  nsresult rv = Preferences::SetInt(aPrefName, aValue);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 }
 
-void SetFunc(const char* prefName, uint32_t value)
+void
+SetFunc(const char* aPrefName, uint32_t aValue)
 {
-  nsresult rv = Preferences::SetUint(prefName, value);
+  nsresult rv = Preferences::SetUint(aPrefName, aValue);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 }
 
-void SetFunc(const char* prefName, float value)
+void
+SetFunc(const char* aPrefName, float aValue)
 {
-  nsresult rv = Preferences::SetFloat(prefName, value);
+  nsresult rv = Preferences::SetFloat(aPrefName, aValue);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 }
 
-void AddVarCacheFunc(bool* var, const char* prefName)
+void
+AddVarCacheFunc(bool* aVar, const char* aPrefName)
 {
-  nsresult rv = Preferences::AddBoolVarCache(var, prefName);
+  nsresult rv = Preferences::AddBoolVarCache(aVar, aPrefName);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 }
 
-void AddVarCacheFunc(int32_t* var, const char* prefName)
+void
+AddVarCacheFunc(int32_t* aVar, const char* aPrefName)
 {
-  nsresult rv = Preferences::AddIntVarCache(var, prefName);
+  nsresult rv = Preferences::AddIntVarCache(aVar, aPrefName);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 }
 
-void AddVarCacheFunc(uint32_t* var, const char* prefName)
+void
+AddVarCacheFunc(uint32_t* aVar, const char* aPrefName)
 {
-  nsresult rv = Preferences::AddUintVarCache(var, prefName);
+  nsresult rv = Preferences::AddUintVarCache(aVar, aPrefName);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 }
 
-void AddVarCacheFunc(Atomic<uint32_t,Relaxed>* var,
-                     const char* prefName)
+void
+AddVarCacheFunc(Atomic<uint32_t, Relaxed>* aVar, const char* aPrefName)
 {
-  nsresult rv = Preferences::AddAtomicUintVarCache(var, prefName);
+  nsresult rv = Preferences::AddAtomicUintVarCache(aVar, aPrefName);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 }
 
-void AddVarCacheFunc(float* var, const char* prefName)
+void
+AddVarCacheFunc(float* aVar, const char* aPrefName)
 {
-  nsresult rv = Preferences::AddFloatVarCache(var, prefName);
+  nsresult rv = Preferences::AddFloatVarCache(aVar, aPrefName);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 }
 
 template<typename T, typename U = T>
-void RunTest(const char* prefName1, const char* prefName2,
-             T value1, T value2)
+void
+RunTest(const char* aPrefName1, const char* aPrefName2, T aValue1, T aValue2)
 {
   static U var1, var2;
   static Closure<T, U> closure1, closure2;
   nsresult rv;
 
-  ASSERT_STRNE(prefName1, prefName2);
-  ASSERT_NE(value1, value2);
+  ASSERT_STRNE(aPrefName1, aPrefName2);
+  ASSERT_NE(aValue1, aValue2);
 
-  //
   // Call Add*VarCache first.
-  //
-  SetFunc(prefName1, value1);
+
+  SetFunc(aPrefName1, aValue1);
 
-  AddVarCacheFunc(&var1, prefName1);
-  ASSERT_EQ(var1, value1);
+  AddVarCacheFunc(&var1, aPrefName1);
+  ASSERT_EQ(var1, aValue1);
 
-  closure1 = { &var1, value2 };
-  rv = Preferences::RegisterCallback(VarChanged<T, U>, prefName1, &closure1);
+  closure1 = { &var1, aValue2 };
+  rv = Preferences::RegisterCallback(VarChanged<T, U>, aPrefName1, &closure1);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 
-  ASSERT_FALSE(closure1.called);
-  SetFunc(prefName1, value2);
-  ASSERT_EQ(var1, value2);
-  ASSERT_TRUE(closure1.called);
+  ASSERT_FALSE(closure1.mCalled);
+  SetFunc(aPrefName1, aValue2);
+  ASSERT_EQ(var1, aValue2);
+  ASSERT_TRUE(closure1.mCalled);
 
-  //
   // Call RegisterCallback first.
-  //
-  SetFunc(prefName2, value1);
+
+  SetFunc(aPrefName2, aValue1);
 
-  closure2 = { &var2, value2 };
-  rv = Preferences::RegisterCallback(VarChanged<T, U>, prefName2, &closure2);
+  closure2 = { &var2, aValue2 };
+  rv = Preferences::RegisterCallback(VarChanged<T, U>, aPrefName2, &closure2);
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 
-  AddVarCacheFunc(&var2, prefName2);
-  ASSERT_EQ(var2, value1);
+  AddVarCacheFunc(&var2, aPrefName2);
+  ASSERT_EQ(var2, aValue1);
 
-  ASSERT_FALSE(closure2.called);
-  SetFunc(prefName2, value2);
-  ASSERT_EQ(var2, value2);
-  ASSERT_TRUE(closure2.called);
+  ASSERT_FALSE(closure2.mCalled);
+  SetFunc(aPrefName2, aValue2);
+  ASSERT_EQ(var2, aValue2);
+  ASSERT_TRUE(closure2.mCalled);
 }
 
-TEST(CallbackAndVarCacheOrder, Bool) {
+TEST(CallbackAndVarCacheOrder, Bool)
+{
   RunTest<bool>("test_pref.bool.1", "test_pref.bool.2", false, true);
 }
 
-TEST(CallbackAndVarCacheOrder, Int) {
+TEST(CallbackAndVarCacheOrder, Int)
+{
   RunTest<int32_t>("test_pref.int.1", "test_pref.int.2", -2, 3);
 }
 
-TEST(CallbackAndVarCacheOrder, Uint) {
+TEST(CallbackAndVarCacheOrder, Uint)
+{
   RunTest<uint32_t>("test_pref.uint.1", "test_pref.uint.2", 4u, 5u);
 }
 
-TEST(CallbackAndVarCacheOrder, AtomicUint) {
-  RunTest<uint32_t,Atomic<uint32_t,Relaxed>>("test_pref.atomic_uint.1",
-                                             "test_pref.atomic_uint.2",
-                                             6u, 7u);
+TEST(CallbackAndVarCacheOrder, AtomicUint)
+{
+  RunTest<uint32_t, Atomic<uint32_t, Relaxed>>(
+    "test_pref.atomic_uint.1", "test_pref.atomic_uint.2", 6u, 7u);
 }
 
-TEST(CallbackAndVarCacheOrder, Float) {
+TEST(CallbackAndVarCacheOrder, Float)
+{
   RunTest<float>("test_pref.float.1", "test_pref.float.2", -8.0f, 9.0f);
 }
 
 } // namespace mozilla
