# HG changeset patch
# User Daniel Holbert <dholbert@cs.stanford.edu>
# Date 1515804588 28800
# Node ID 0a4e3d4de37415d0fec3f5bfb644ffa3da7eb659
# Parent  7fa13d656caf79fbe78340e01a0a63f943e50db8
Bug 1430312: Use StaticRefPtr instead of raw pointer for nsImageFrame::gIconLoad. r=tnikkel

MozReview-Commit-ID: HvkQEaZS1OG

diff --git a/layout/generic/nsImageFrame.cpp b/layout/generic/nsImageFrame.cpp
--- a/layout/generic/nsImageFrame.cpp
+++ b/layout/generic/nsImageFrame.cpp
@@ -96,17 +96,17 @@ using namespace mozilla::layers;
 
 //we must add hooks soon
 #define IMAGE_EDITOR_CHECK 1
 
 // Default alignment value (so we can tell an unset value from a set value)
 #define ALIGN_UNSET uint8_t(-1)
 
 // static icon information
-nsImageFrame::IconLoad* nsImageFrame::gIconLoad = nullptr;
+StaticRefPtr<nsImageFrame::IconLoad> nsImageFrame::gIconLoad;
 
 // cached IO service for loading icons
 nsIIOService* nsImageFrame::sIOService;
 
 // test if the width and height are fixed, looking at the style data
 // This is used by nsImageFrame::ShouldCreateImageFrameFor and should
 // not be used for layout decisions.
 static bool HaveSpecifiedSize(const nsStylePosition* aStylePosition)
@@ -2354,17 +2354,16 @@ nsImageFrame::GetLoadGroup(nsPresContext
 nsresult nsImageFrame::LoadIcons(nsPresContext *aPresContext)
 {
   NS_ASSERTION(!gIconLoad, "called LoadIcons twice");
 
   NS_NAMED_LITERAL_STRING(loadingSrc,"resource://gre-resources/loading-image.png");
   NS_NAMED_LITERAL_STRING(brokenSrc,"resource://gre-resources/broken-image.png");
 
   gIconLoad = new IconLoad();
-  NS_ADDREF(gIconLoad);
 
   nsresult rv;
   // create a loader and load the images
   rv = LoadIcon(loadingSrc,
                 aPresContext,
                 getter_AddRefs(gIconLoad->mLoadingImage));
   if (NS_FAILED(rv)) {
     return rv;
diff --git a/layout/generic/nsImageFrame.h b/layout/generic/nsImageFrame.h
--- a/layout/generic/nsImageFrame.h
+++ b/layout/generic/nsImageFrame.h
@@ -13,16 +13,17 @@
 #include "nsIObserver.h"
 
 #include "imgINotificationObserver.h"
 
 #include "nsDisplayList.h"
 #include "imgIContainer.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/DebugOnly.h"
+#include "mozilla/StaticPtr.h"
 #include "nsIReflowCallback.h"
 #include "nsTObserverArray.h"
 
 class nsFontMetrics;
 class nsImageMap;
 class nsIURI;
 class nsILoadGroup;
 class nsDisplayImage;
@@ -128,17 +129,17 @@ public:
 
   virtual LogicalSides GetLogicalSkipSides(const ReflowInput* aReflowInput = nullptr) const override;
 
   nsresult GetIntrinsicImageSize(nsSize& aSize);
 
   static void ReleaseGlobals() {
     if (gIconLoad) {
       gIconLoad->Shutdown();
-      NS_RELEASE(gIconLoad);
+      gIconLoad = nullptr;
     }
     NS_IF_RELEASE(sIOService);
   }
 
   nsresult Notify(imgIRequest *aRequest, int32_t aType, const nsIntRect* aData);
 
   /**
    * Function to test whether aContent, which has aStyleContext as its style,
@@ -391,17 +392,18 @@ private:
     RefPtr<imgRequestProxy> mLoadingImage;
     RefPtr<imgRequestProxy> mBrokenImage;
     bool             mPrefForceInlineAltText;
     bool             mPrefShowPlaceholders;
     bool             mPrefShowLoadingPlaceholder;
   };
 
 public:
-  static IconLoad* gIconLoad; // singleton pattern: one LoadIcons instance is used
+  // singleton pattern: one LoadIcons instance is used
+  static mozilla::StaticRefPtr<IconLoad> gIconLoad;
 
   friend class nsDisplayImage;
 };
 
 /**
  * Note that nsDisplayImage does not receive events. However, an image element
  * is replaced content so its background will be z-adjacent to the
  * image itself, and hence receive events just as if the image itself
