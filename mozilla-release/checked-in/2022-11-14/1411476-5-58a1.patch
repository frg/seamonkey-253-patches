# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1508911059 -28800
#      Wed Oct 25 13:57:39 2017 +0800
# Node ID 3a04b939c55a65748adf3e648852e94bb3245cce
# Parent  d859d89cd4390a4edc4fca8218310efb66c27ad4
Bug 1411476. P5 - dump debug info for ChannelMediaResource. r=gerald

MozReview-Commit-ID: GjdtrlNb948

diff --git a/dom/media/BaseMediaResource.h b/dom/media/BaseMediaResource.h
--- a/dom/media/BaseMediaResource.h
+++ b/dom/media/BaseMediaResource.h
@@ -114,16 +114,18 @@ public:
     return 0;
   }
 
   virtual size_t SizeOfIncludingThis(MallocSizeOf aMallocSizeOf) const
   {
     return aMallocSizeOf(this) + SizeOfExcludingThis(aMallocSizeOf);
   }
 
+  virtual nsCString GetDebugInfo() { return nsCString(); }
+
 protected:
   BaseMediaResource(MediaResourceCallback* aCallback,
                     nsIChannel* aChannel,
                     nsIURI* aURI)
     : mCallback(aCallback)
     , mChannel(aChannel)
     , mURI(aURI)
     , mLoadInBackground(false)
@@ -154,9 +156,9 @@ protected:
   // aLoadInBackground = true, i.e. when the document load event is not
   // blocked by this resource, and all channel loads will be in the
   // background.
   bool mLoadInBackground;
 };
 
 } // namespace mozilla
 
-#endif // BaseMediaResource_h
\ No newline at end of file
+#endif // BaseMediaResource_h
diff --git a/dom/media/ChannelMediaDecoder.cpp b/dom/media/ChannelMediaDecoder.cpp
--- a/dom/media/ChannelMediaDecoder.cpp
+++ b/dom/media/ChannelMediaDecoder.cpp
@@ -611,12 +611,22 @@ ChannelMediaDecoder::MetadataLoaded(
   UniquePtr<MetadataTags> aTags,
   MediaDecoderEventVisibility aEventVisibility)
 {
   MediaDecoder::MetadataLoaded(Move(aInfo), Move(aTags), aEventVisibility);
   // Set mode to PLAYBACK after reading metadata.
   mResource->SetReadMode(MediaCacheStream::MODE_PLAYBACK);
 }
 
+nsCString
+ChannelMediaDecoder::GetDebugInfo()
+{
+  auto&& str = MediaDecoder::GetDebugInfo();
+  if (mResource) {
+    AppendStringIfNotEmpty(str, mResource->GetDebugInfo());
+  }
+  return str;
+}
+
 } // namespace mozilla
 
 // avoid redefined macro in unified build
 #undef LOG
diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -69,16 +69,18 @@ protected:
                       UniquePtr<MetadataTags> aTags,
                       MediaDecoderEventVisibility aEventVisibility) override;
 
   RefPtr<ResourceCallback> mResourceCallback;
   RefPtr<BaseMediaResource> mResource;
 
   explicit ChannelMediaDecoder(MediaDecoderInit& aInit);
 
+  nsCString GetDebugInfo() override;
+
 public:
 
   // Create a decoder for the given aType. Returns null if we were unable
   // to create the decoder, for example because the requested MIME type in
   // the init struct was unsupported.
   static already_AddRefed<ChannelMediaDecoder> Create(
     MediaDecoderInit& aInit,
     DecoderDoctorDiagnostics* aDiagnostics);
diff --git a/dom/media/ChannelMediaResource.cpp b/dom/media/ChannelMediaResource.cpp
--- a/dom/media/ChannelMediaResource.cpp
+++ b/dom/media/ChannelMediaResource.cpp
@@ -965,16 +965,23 @@ ChannelMediaResource::GetLength()
 }
 
 int64_t
 ChannelMediaResource::GetOffset() const
 {
   return mCacheStream.GetOffset();
 }
 
+nsCString
+ChannelMediaResource::GetDebugInfo()
+{
+  return NS_LITERAL_CSTRING("ChannelMediaResource: ") +
+         mCacheStream.GetDebugInfo();
+}
+
 // ChannelSuspendAgent
 
 bool
 ChannelSuspendAgent::Suspend()
 {
   MOZ_ASSERT(NS_IsMainThread());
   SuspendInternal();
   return (++mSuspendCount == 1);
diff --git a/dom/media/ChannelMediaResource.h b/dom/media/ChannelMediaResource.h
--- a/dom/media/ChannelMediaResource.h
+++ b/dom/media/ChannelMediaResource.h
@@ -155,16 +155,18 @@ public:
 
     return size;
   }
 
   size_t SizeOfIncludingThis(MallocSizeOf aMallocSizeOf) const override {
     return aMallocSizeOf(this) + SizeOfExcludingThis(aMallocSizeOf);
   }
 
+  nsCString GetDebugInfo() override;
+
   class Listener final
     : public nsIStreamListener
     , public nsIInterfaceRequestor
     , public nsIChannelEventSink
     , public nsIThreadRetargetableStreamListener
   {
     ~Listener() {}
   public:
diff --git a/dom/media/MediaCache.cpp b/dom/media/MediaCache.cpp
--- a/dom/media/MediaCache.cpp
+++ b/dom/media/MediaCache.cpp
@@ -20,16 +20,17 @@
 #include "mozilla/Services.h"
 #include "mozilla/StaticPtr.h"
 #include "mozilla/SystemGroup.h"
 #include "mozilla/Telemetry.h"
 #include "nsContentUtils.h"
 #include "nsIObserverService.h"
 #include "nsIPrincipal.h"
 #include "nsISeekableStream.h"
+#include "nsPrintfCString.h"
 #include "nsThreadUtils.h"
 #include "prio.h"
 #include <algorithm>
 
 namespace mozilla {
 
 #undef LOG
 #undef LOGI
@@ -2715,13 +2716,26 @@ nsresult MediaCacheStream::GetCachedRang
     aRanges += MediaByteRange(startOffset, endOffset);
     startOffset = GetNextCachedDataInternal(endOffset);
     NS_ASSERTION(startOffset == -1 || startOffset > endOffset,
       "Must have advanced to start of next range, or hit end of stream");
   }
   return NS_OK;
 }
 
+nsCString
+MediaCacheStream::GetDebugInfo()
+{
+  ReentrantMonitorAutoEnter mon(mMediaCache->GetReentrantMonitor());
+  return nsPrintfCString("mStreamLength=%" PRId64 " mChannelOffset=%" PRId64
+                         " mCacheSuspended=%d mChannelEnded=%d mLoadID=%u",
+                         mStreamLength,
+                         mChannelOffset,
+                         mCacheSuspended,
+                         mChannelEnded,
+                         mLoadID);
+}
+
 } // namespace mozilla
 
 // avoid redefined macro in unified build
 #undef LOG
 #undef LOGI
diff --git a/dom/media/MediaCache.h b/dom/media/MediaCache.h
--- a/dom/media/MediaCache.h
+++ b/dom/media/MediaCache.h
@@ -356,16 +356,18 @@ public:
   void ThrottleReadahead(bool bThrottle);
 
   size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const;
 
   // Update mPrincipal for all streams of the same resource given that data has
   // been received from aPrincipal
   void UpdatePrincipal(nsIPrincipal* aPrincipal);
 
+  nsCString GetDebugInfo();
+
 private:
   friend class MediaCache;
 
   /**
    * A doubly-linked list of blocks. Add/Remove/Get methods are all
    * constant time. We declare this here so that a stream can contain a
    * BlockList of its read-ahead blocks. Blocks are referred to by index
    * into the MediaCache::mIndex array.
diff --git a/dom/media/MediaDecoder.h b/dom/media/MediaDecoder.h
--- a/dom/media/MediaDecoder.h
+++ b/dom/media/MediaDecoder.h
@@ -469,25 +469,25 @@ protected:
   RefPtr<MediaFormatReader> mReader;
 
   // Amount of buffered data ahead of current time required to consider that
   // the next frame is available.
   // An arbitrary value of 250ms is used.
   static constexpr auto DEFAULT_NEXT_FRAME_AVAILABLE_BUFFERED =
     media::TimeUnit::FromMicroseconds(250000);
 
+  virtual nsCString GetDebugInfo();
+
 private:
   // Ensures our media resource has been pinned.
   virtual void PinForSeek() = 0;
 
   // Ensures our media resource has been unpinned.
   virtual void UnpinForSeek() = 0;
 
-  nsCString GetDebugInfo();
-
   // Called when the owner's activity changed.
   void NotifyCompositor();
 
   void OnPlaybackErrorEvent(const MediaResult& aError);
 
   void OnDecoderDoctorEvent(DecoderDoctorEvent aEvent);
 
   void OnMediaNotSeekable()
