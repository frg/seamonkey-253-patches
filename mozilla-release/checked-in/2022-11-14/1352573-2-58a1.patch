# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1501762371 -36000
# Node ID db05998a8a032109be3afe6066518cdf903c13e0
# Parent  b3dcf2f88c249ed32ebb59c60c9b57ed3bffd635
Bug 1352573 (part 2) - Remove NPN_PluginThreadAsyncCall() and related machinery. r=bsmedberg.
* * *
[mq]: rm-checkGCRace

diff --git a/dom/plugins/base/nsNPAPIPlugin.cpp b/dom/plugins/base/nsNPAPIPlugin.cpp
--- a/dom/plugins/base/nsNPAPIPlugin.cpp
+++ b/dom/plugins/base/nsNPAPIPlugin.cpp
@@ -141,67 +141,49 @@ static NPNetscapeFuncs sBrowserFuncs = {
   _removeproperty,
   _hasproperty,
   _hasmethod,
   _releasevariantvalue,
   _setexception,
   _pushpopupsenabledstate,
   _poppopupsenabledstate,
   _enumerate,
-  _pluginthreadasynccall,
+  nullptr, // pluginthreadasynccall, not used
   _construct,
   _getvalueforurl,
   _setvalueforurl,
   nullptr, //NPN GetAuthenticationInfo, not supported
   _scheduletimer,
   _unscheduletimer,
   _popupcontextmenu,
   _convertpoint,
   nullptr, // handleevent, unimplemented
   nullptr, // unfocusinstance, unimplemented
   _urlredirectresponse,
   _initasyncsurface,
   _finalizeasyncsurface,
   _setcurrentasyncsurface
 };
 
-static Mutex *sPluginThreadAsyncCallLock = nullptr;
-static PRCList sPendingAsyncCalls = PR_INIT_STATIC_CLIST(&sPendingAsyncCalls);
-
 // POST/GET stream type
 enum eNPPStreamTypeInternal {
   eNPPStreamTypeInternal_Get,
   eNPPStreamTypeInternal_Post
 };
 
 void NS_NotifyBeginPluginCall(NSPluginCallReentry aReentryState)
 {
   nsNPAPIPluginInstance::BeginPluginCall(aReentryState);
 }
 
 void NS_NotifyPluginCall(NSPluginCallReentry aReentryState)
 {
   nsNPAPIPluginInstance::EndPluginCall(aReentryState);
 }
 
-static void CheckClassInitialized()
-{
-  static bool initialized = false;
-
-  if (initialized)
-    return;
-
-  if (!sPluginThreadAsyncCallLock)
-    sPluginThreadAsyncCallLock = new Mutex("nsNPAPIPlugin.sPluginThreadAsyncCallLock");
-
-  initialized = true;
-
-  NPN_PLUGIN_LOG(PLUGIN_LOG_NORMAL,("NPN callbacks initialized\n"));
-}
-
 nsNPAPIPlugin::nsNPAPIPlugin()
 {
   memset((void*)&mPluginFuncs, 0, sizeof(mPluginFuncs));
   mPluginFuncs.size = sizeof(mPluginFuncs);
   mPluginFuncs.version = (NP_VERSION_MAJOR << 8) | NP_VERSION_MINOR;
 
   mLibrary = nullptr;
 }
@@ -251,18 +233,16 @@ nsNPAPIPlugin::CreatePlugin(nsPluginTag 
 {
   AUTO_PROFILER_LABEL("nsNPAPIPlugin::CreatePlugin", OTHER);
   *aResult = nullptr;
 
   if (!aPluginTag) {
     return NS_ERROR_FAILURE;
   }
 
-  CheckClassInitialized();
-
   RefPtr<nsNPAPIPlugin> plugin = new nsNPAPIPlugin();
 
   PluginLibrary* pluginLib = GetNewPluginLibrary(aPluginTag);
   if (!pluginLib) {
     return NS_ERROR_FAILURE;
   }
 
 #if defined(XP_MACOSX)
@@ -435,47 +415,16 @@ MakeNewNPAPIStreamInternal(NPP npp, cons
 #if defined(MOZ_MEMORY) && defined(XP_WIN)
 extern "C" size_t malloc_usable_size(const void *ptr);
 #endif
 
 namespace {
 
 static char *gNPPException;
 
-class nsPluginThreadRunnable : public Runnable,
-                               public PRCList
-{
-public:
-  nsPluginThreadRunnable(NPP instance, PluginThreadCallback func,
-                         void *userData);
-  ~nsPluginThreadRunnable() override;
-
-  NS_IMETHOD Run() override;
-
-  bool IsForInstance(NPP instance)
-  {
-    return (mInstance == instance);
-  }
-
-  void Invalidate()
-  {
-    mFunc = nullptr;
-  }
-
-  bool IsValid()
-  {
-    return (mFunc != nullptr);
-  }
-
-private:
-  NPP mInstance;
-  PluginThreadCallback mFunc;
-  void *mUserData;
-};
-
 static nsIDocument *
 GetDocumentFromNPP(NPP npp)
 {
   NS_ENSURE_TRUE(npp, nullptr);
 
   nsNPAPIPluginInstance *inst = (nsNPAPIPluginInstance *)npp->ndata;
   NS_ENSURE_TRUE(inst, nullptr);
 
@@ -531,128 +480,16 @@ NPPExceptionAutoHolder::NPPExceptionAuto
 
 NPPExceptionAutoHolder::~NPPExceptionAutoHolder()
 {
   NS_ASSERTION(!gNPPException, "NPP exception not properly cleared!");
 
   gNPPException = mOldException;
 }
 
-nsPluginThreadRunnable::nsPluginThreadRunnable(NPP instance,
-                                               PluginThreadCallback func,
-                                               void *userData)
-  : Runnable("nsPluginThreadRunnable"),
-    mInstance(instance),
-    mFunc(func),
-    mUserData(userData)
-{
-  if (!sPluginThreadAsyncCallLock) {
-    // Failed to create lock, not much we can do here then...
-    mFunc = nullptr;
-
-    return;
-  }
-
-  PR_INIT_CLIST(this);
-
-  {
-    MutexAutoLock lock(*sPluginThreadAsyncCallLock);
-
-    nsNPAPIPluginInstance *inst = (nsNPAPIPluginInstance *)instance->ndata;
-    if (!inst || !inst->IsRunning()) {
-      // The plugin was stopped, ignore this async call.
-      mFunc = nullptr;
-
-      return;
-    }
-
-    PR_APPEND_LINK(this, &sPendingAsyncCalls);
-  }
-}
-
-nsPluginThreadRunnable::~nsPluginThreadRunnable()
-{
-  if (!sPluginThreadAsyncCallLock) {
-    return;
-  }
-
-  {
-    MutexAutoLock lock(*sPluginThreadAsyncCallLock);
-
-    PR_REMOVE_LINK(this);
-  }
-}
-
-NS_IMETHODIMP
-nsPluginThreadRunnable::Run()
-{
-  if (mFunc) {
-    PluginDestructionGuard guard(mInstance);
-
-    NS_TRY_SAFE_CALL_VOID(mFunc(mUserData), nullptr,
-                          NS_PLUGIN_CALL_SAFE_TO_REENTER_GECKO);
-  }
-
-  return NS_OK;
-}
-
-void
-OnPluginDestroy(NPP instance)
-{
-  if (!sPluginThreadAsyncCallLock) {
-    return;
-  }
-
-  {
-    MutexAutoLock lock(*sPluginThreadAsyncCallLock);
-
-    if (PR_CLIST_IS_EMPTY(&sPendingAsyncCalls)) {
-      return;
-    }
-
-    nsPluginThreadRunnable *r =
-      (nsPluginThreadRunnable *)PR_LIST_HEAD(&sPendingAsyncCalls);
-
-    do {
-      if (r->IsForInstance(instance)) {
-        r->Invalidate();
-      }
-
-      r = (nsPluginThreadRunnable *)PR_NEXT_LINK(r);
-    } while (r != &sPendingAsyncCalls);
-  }
-}
-
-void
-OnShutdown()
-{
-  NS_ASSERTION(PR_CLIST_IS_EMPTY(&sPendingAsyncCalls),
-               "Pending async plugin call list not cleaned up!");
-
-  if (sPluginThreadAsyncCallLock) {
-    delete sPluginThreadAsyncCallLock;
-
-    sPluginThreadAsyncCallLock = nullptr;
-  }
-}
-
-AsyncCallbackAutoLock::AsyncCallbackAutoLock()
-{
-  if (sPluginThreadAsyncCallLock) {
-    sPluginThreadAsyncCallLock->Lock();
-  }
-}
-
-AsyncCallbackAutoLock::~AsyncCallbackAutoLock()
-{
-  if (sPluginThreadAsyncCallLock) {
-    sPluginThreadAsyncCallLock->Unlock();
-  }
-}
-
 NPP NPPStack::sCurrentNPP = nullptr;
 
 const char *
 PeekException()
 {
   return gNPPException;
 }
 
@@ -1987,32 +1824,16 @@ void
   }
   nsNPAPIPluginInstance *inst = npp ? (nsNPAPIPluginInstance *)npp->ndata : nullptr;
   if (!inst)
     return;
 
   inst->PopPopupsEnabledState();
 }
 
-void
-_pluginthreadasynccall(NPP instance, PluginThreadCallback func, void *userData)
-{
-  if (NS_IsMainThread()) {
-    NPN_PLUGIN_LOG(PLUGIN_LOG_NOISY,("NPN_pluginthreadasynccall called from the main thread\n"));
-  } else {
-    NPN_PLUGIN_LOG(PLUGIN_LOG_NOISY,("NPN_pluginthreadasynccall called from a non main thread\n"));
-  }
-  RefPtr<nsPluginThreadRunnable> evt =
-    new nsPluginThreadRunnable(instance, func, userData);
-
-  if (evt && evt->IsValid()) {
-    NS_DispatchToMainThread(evt);
-  }
-}
-
 NPError
 _getvalueforurl(NPP instance, NPNURLVariable variable, const char *url,
                 char **value, uint32_t *len)
 {
   if (!NS_IsMainThread()) {
     NPN_PLUGIN_LOG(PLUGIN_LOG_ALWAYS,("NPN_getvalueforurl called from the wrong thread\n"));
     return NPERR_GENERIC_ERROR;
   }
diff --git a/dom/plugins/base/nsNPAPIPlugin.h b/dom/plugins/base/nsNPAPIPlugin.h
--- a/dom/plugins/base/nsNPAPIPlugin.h
+++ b/dom/plugins/base/nsNPAPIPlugin.h
@@ -214,22 +214,16 @@ void
 _setexception(NPObject* npobj, const NPUTF8 *message);
 
 void
 _pushpopupsenabledstate(NPP npp, NPBool enabled);
 
 void
 _poppopupsenabledstate(NPP npp);
 
-typedef void(*PluginThreadCallback)(void *);
-
-void
-_pluginthreadasynccall(NPP instance, PluginThreadCallback func,
-                       void *userData);
-
 NPError
 _getvalueforurl(NPP instance, NPNURLVariable variable, const char *url,
                 char **value, uint32_t *len);
 
 NPError
 _setvalueforurl(NPP instance, NPNURLVariable variable, const char *url,
                 const char *value, uint32_t len);
 
@@ -322,32 +316,16 @@ void
 } /* namespace mozilla */
 
 const char *
 PeekException();
 
 void
 PopException();
 
-void
-OnPluginDestroy(NPP instance);
-
-void
-OnShutdown();
-
-/**
- * within a lexical scope, locks and unlocks the mutex used to
- * serialize modifications to plugin async callback state.
- */
-struct MOZ_STACK_CLASS AsyncCallbackAutoLock
-{
-  AsyncCallbackAutoLock();
-  ~AsyncCallbackAutoLock();
-};
-
 class NPPStack
 {
 public:
   static NPP Peek()
   {
     return sCurrentNPP;
   }
 
diff --git a/dom/plugins/base/nsNPAPIPluginInstance.cpp b/dom/plugins/base/nsNPAPIPluginInstance.cpp
--- a/dom/plugins/base/nsNPAPIPluginInstance.cpp
+++ b/dom/plugins/base/nsNPAPIPluginInstance.cpp
@@ -159,25 +159,18 @@ nsresult nsNPAPIPluginInstance::Stop()
     UnscheduleTimer(mTimers[i - 1]->id);
 
   // If there's code from this plugin instance on the stack, delay the
   // destroy.
   if (PluginDestructionGuard::DelayDestroy(this)) {
     return NS_OK;
   }
 
-  // Make sure we lock while we're writing to mRunning after we've
-  // started as other threads might be checking that inside a lock.
-  {
-    AsyncCallbackAutoLock lock;
-    mRunning = DESTROYING;
-    mStopTime = TimeStamp::Now();
-  }
-
-  OnPluginDestroy(&mNPP);
+  mRunning = DESTROYING;
+  mStopTime = TimeStamp::Now();
 
   // clean up open streams
   while (mStreamListeners.Length() > 0) {
     RefPtr<nsNPAPIPluginStreamListener> currentListener(mStreamListeners[0]);
     currentListener->CleanUpStream(NPRES_USER_BREAK);
     mStreamListeners.RemoveElement(currentListener);
   }
 
diff --git a/dom/plugins/base/nsPluginHost.cpp b/dom/plugins/base/nsPluginHost.cpp
--- a/dom/plugins/base/nsPluginHost.cpp
+++ b/dom/plugins/base/nsPluginHost.cpp
@@ -3417,17 +3417,16 @@ void nsPluginHost::CreateWidget(nsPlugin
   aOwner->CallSetWindow();
 }
 
 NS_IMETHODIMP nsPluginHost::Observe(nsISupports *aSubject,
                                     const char *aTopic,
                                     const char16_t *someData)
 {
   if (!strcmp(NS_XPCOM_SHUTDOWN_OBSERVER_ID, aTopic)) {
-    OnShutdown();
     UnloadPlugins();
     sInst->Release();
   }
   if (!strcmp(NS_PREFBRANCH_PREFCHANGE_TOPIC_ID, aTopic)) {
     mPluginsDisabled = Preferences::GetBool("plugin.disable", false);
     // Unload or load plugins as needed
     if (mPluginsDisabled) {
       UnloadPlugins();
diff --git a/dom/plugins/ipc/ChildAsyncCall.cpp b/dom/plugins/ipc/ChildAsyncCall.cpp
deleted file mode 100644
--- a/dom/plugins/ipc/ChildAsyncCall.cpp
+++ /dev/null
@@ -1,54 +0,0 @@
-/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
- * vim: sw=2 ts=8 et :
- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#include "ChildAsyncCall.h"
-#include "PluginInstanceChild.h"
-
-namespace mozilla {
-namespace plugins {
-
-ChildAsyncCall::ChildAsyncCall(PluginInstanceChild* instance,
-                               PluginThreadCallback aFunc,
-                               void* aUserData)
-  : CancelableRunnable("plugins::ChildAsyncCall")
-  , mInstance(instance)
-  , mFunc(aFunc)
-  , mData(aUserData)
-{
-}
-
-nsresult
-ChildAsyncCall::Cancel()
-{
-  mInstance = nullptr;
-  mFunc = nullptr;
-  mData = nullptr;
-  return NS_OK;
-}
-
-void
-ChildAsyncCall::RemoveFromAsyncList()
-{
-  if (mInstance) {
-    MutexAutoLock lock(mInstance->mAsyncCallMutex);
-    mInstance->mPendingAsyncCalls.RemoveElement(this);
-  }
-}
-
-NS_IMETHODIMP
-ChildAsyncCall::Run()
-{
-  RemoveFromAsyncList();
-
-  if (mFunc)
-    mFunc(mData);
-
-  return NS_OK;
-}
-
-} // namespace plugins
-} // namespace mozilla
diff --git a/dom/plugins/ipc/ChildAsyncCall.h b/dom/plugins/ipc/ChildAsyncCall.h
deleted file mode 100644
--- a/dom/plugins/ipc/ChildAsyncCall.h
+++ /dev/null
@@ -1,41 +0,0 @@
-/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
- * vim: sw=2 ts=8 et :
- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#ifndef mozilla_plugins_ChildAsyncCall_h
-#define mozilla_plugins_ChildAsyncCall_h
-
-#include "PluginMessageUtils.h"
-#include "nsThreadUtils.h"
-
-namespace mozilla {
-namespace plugins {
-
-typedef void (*PluginThreadCallback)(void*);
-
-class PluginInstanceChild;
-
-class ChildAsyncCall : public CancelableRunnable
-{
-public:
-  ChildAsyncCall(PluginInstanceChild* instance,
-                 PluginThreadCallback aFunc, void* aUserData);
-
-  NS_IMETHOD Run() override;
-  nsresult Cancel() override;
-
-protected:
-  PluginInstanceChild* mInstance;
-  PluginThreadCallback mFunc;
-  void* mData;
-
-  void RemoveFromAsyncList();
-};
-
-} // namespace plugins
-} // namespace mozilla
-
-#endif // mozilla_plugins_ChildAsyncCall_h
diff --git a/dom/plugins/ipc/PluginInstanceChild.cpp b/dom/plugins/ipc/PluginInstanceChild.cpp
--- a/dom/plugins/ipc/PluginInstanceChild.cpp
+++ b/dom/plugins/ipc/PluginInstanceChild.cpp
@@ -140,17 +140,16 @@ PluginInstanceChild::PluginInstanceChild
     , mPluginWindowHWND(0)
     , mPluginWndProc(0)
     , mPluginParentHWND(0)
     , mCachedWinlessPluginHWND(0)
     , mWinlessPopupSurrogateHWND(0)
     , mWinlessThrottleOldWndProc(0)
     , mWinlessHiddenMsgHWND(0)
 #endif // OS_WIN
-    , mAsyncCallMutex("PluginInstanceChild::mAsyncCallMutex")
 #if defined(MOZ_WIDGET_COCOA)
 #if defined(__i386__)
     , mEventModel(NPEventModelCarbon)
 #endif
     , mShColorSpace(nullptr)
     , mShContext(nullptr)
     , mCGLayer(nullptr)
     , mCurrentEvent(nullptr)
@@ -3823,35 +3822,16 @@ PluginInstanceChild::UnscheduleTimer(uin
 {
     if (0 == id)
         return;
 
     mTimers.RemoveElement(id, ChildTimer::IDComparator());
 }
 
 void
-PluginInstanceChild::AsyncCall(PluginThreadCallback aFunc, void* aUserData)
-{
-    RefPtr<ChildAsyncCall> task = new ChildAsyncCall(this, aFunc, aUserData);
-    PostChildAsyncCall(task.forget());
-}
-
-void
-PluginInstanceChild::PostChildAsyncCall(already_AddRefed<ChildAsyncCall> aTask)
-{
-    RefPtr<ChildAsyncCall> task = aTask;
-
-    {
-        MutexAutoLock lock(mAsyncCallMutex);
-        mPendingAsyncCalls.AppendElement(task);
-    }
-    ProcessChild::message_loop()->PostTask(task.forget());
-}
-
-void
 PluginInstanceChild::SwapSurfaces()
 {
     RefPtr<gfxASurface> tmpsurf = mCurrentSurface;
 #ifdef XP_WIN
     PPluginSurfaceChild* tmpactor = mCurrentSurfaceActor;
 #endif
 
     mCurrentSurface = mBackSurface;
@@ -4060,23 +4040,16 @@ PluginInstanceChild::Destroy()
     UnhookWinlessFlashThrottle();
     DestroyPluginWindow();
 
     for (uint32_t i = 0; i < mPendingFlashThrottleMsgs.Length(); ++i) {
         mPendingFlashThrottleMsgs[i]->Cancel();
     }
     mPendingFlashThrottleMsgs.Clear();
 #endif
-
-    // Pending async calls are discarded, not delivered. This matches the
-    // in-process behavior.
-    for (uint32_t i = 0; i < mPendingAsyncCalls.Length(); ++i)
-        mPendingAsyncCalls[i]->Cancel();
-
-    mPendingAsyncCalls.Clear();
 }
 
 mozilla::ipc::IPCResult
 PluginInstanceChild::AnswerNPP_Destroy(NPError* aResult)
 {
     PLUGIN_LOG_DEBUG_METHOD;
     AssertPluginThread();
     *aResult = NPERR_NO_ERROR;
diff --git a/dom/plugins/ipc/PluginInstanceChild.h b/dom/plugins/ipc/PluginInstanceChild.h
--- a/dom/plugins/ipc/PluginInstanceChild.h
+++ b/dom/plugins/ipc/PluginInstanceChild.h
@@ -21,17 +21,16 @@
 #include "mozilla/gfx/QuartzSupport.h"
 #include "base/timer.h"
 
 #endif
 
 #include "npfunctions.h"
 #include "nsAutoPtr.h"
 #include "nsTArray.h"
-#include "ChildAsyncCall.h"
 #include "ChildTimer.h"
 #include "nsRect.h"
 #include "nsTHashtable.h"
 #include "mozilla/PaintTracker.h"
 #include "mozilla/gfx/Types.h"
 
 #include <map>
 
@@ -233,20 +232,16 @@ public:
 
 #ifdef MOZ_WIDGET_COCOA
     void Invalidate();
 #endif // definied(MOZ_WIDGET_COCOA)
 
     uint32_t ScheduleTimer(uint32_t interval, bool repeat, TimerFunc func);
     void UnscheduleTimer(uint32_t id);
 
-    void AsyncCall(PluginThreadCallback aFunc, void* aUserData);
-    // This function is a more general version of AsyncCall
-    void PostChildAsyncCall(already_AddRefed<ChildAsyncCall> aTask);
-
     int GetQuirks();
 
     void NPN_URLRedirectResponse(void* notifyData, NPBool allow);
 
 
     NPError NPN_InitAsyncSurface(NPSize *size, NPImageFormat format,
                                  void *initData, NPAsyncSurface *surface);
     NPError NPN_FinalizeAsyncSurface(NPAsyncSurface *surface);
@@ -436,23 +431,19 @@ private:
     int mNestedEventLevelDepth;
     HWND mCachedWinlessPluginHWND;
     HWND mWinlessPopupSurrogateHWND;
     nsIntPoint mPluginSize;
     WNDPROC mWinlessThrottleOldWndProc;
     HWND mWinlessHiddenMsgHWND;
 #endif
 
-    friend class ChildAsyncCall;
-
 #if defined(OS_WIN)
     nsTArray<FlashThrottleMsg*> mPendingFlashThrottleMsgs;
 #endif
-    Mutex mAsyncCallMutex;
-    nsTArray<ChildAsyncCall*> mPendingAsyncCalls;
     nsTArray<nsAutoPtr<ChildTimer> > mTimers;
 
     /**
      * During destruction we enumerate all remaining scriptable objects and
      * invalidate/delete them. Enumeration can re-enter, so maintain a
      * hash separate from PluginModuleChild.mObjectMap.
      */
     nsAutoPtr< nsTHashtable<DeletingObjectEntry> > mDeletingHash;
diff --git a/dom/plugins/ipc/PluginModuleChild.cpp b/dom/plugins/ipc/PluginModuleChild.cpp
--- a/dom/plugins/ipc/PluginModuleChild.cpp
+++ b/dom/plugins/ipc/PluginModuleChild.cpp
@@ -920,20 +920,16 @@ static void
 _setexception(NPObject* npobj, const NPUTF8 *message);
 
 static void
 _pushpopupsenabledstate(NPP aNPP, NPBool enabled);
 
 static void
 _poppopupsenabledstate(NPP aNPP);
 
-static void
-_pluginthreadasynccall(NPP instance, PluginThreadCallback func,
-                       void *userData);
-
 static NPError
 _getvalueforurl(NPP npp, NPNURLVariable variable, const char *url,
                 char **value, uint32_t *len);
 
 static NPError
 _setvalueforurl(NPP npp, NPNURLVariable variable, const char *url,
                 const char *value, uint32_t len);
 
@@ -1011,17 +1007,17 @@ const NPNetscapeFuncs PluginModuleChild:
     mozilla::plugins::child::_removeproperty,
     mozilla::plugins::child::_hasproperty,
     mozilla::plugins::child::_hasmethod,
     mozilla::plugins::child::_releasevariantvalue,
     mozilla::plugins::child::_setexception,
     mozilla::plugins::child::_pushpopupsenabledstate,
     mozilla::plugins::child::_poppopupsenabledstate,
     mozilla::plugins::child::_enumerate,
-    mozilla::plugins::child::_pluginthreadasynccall,
+    nullptr, // pluginthreadasynccall, not used
     mozilla::plugins::child::_construct,
     mozilla::plugins::child::_getvalueforurl,
     mozilla::plugins::child::_setvalueforurl,
     nullptr, //NPN GetAuthenticationInfo, not supported
     mozilla::plugins::child::_scheduletimer,
     mozilla::plugins::child::_unscheduletimer,
     mozilla::plugins::child::_popupcontextmenu,
     mozilla::plugins::child::_convertpoint,
@@ -1544,28 +1540,16 @@ void
 _poppopupsenabledstate(NPP aNPP)
 {
     PLUGIN_LOG_DEBUG_FUNCTION;
     ENSURE_PLUGIN_THREAD_VOID();
 
     InstCast(aNPP)->CallNPN_PopPopupsEnabledState();
 }
 
-void
-_pluginthreadasynccall(NPP aNPP,
-                       PluginThreadCallback aFunc,
-                       void* aUserData)
-{
-    PLUGIN_LOG_DEBUG_FUNCTION;
-    if (!aFunc)
-        return;
-
-    InstCast(aNPP)->AsyncCall(aFunc, aUserData);
-}
-
 NPError
 _getvalueforurl(NPP npp, NPNURLVariable variable, const char *url,
                 char **value, uint32_t *len)
 {
     PLUGIN_LOG_DEBUG_FUNCTION;
     AssertPluginThread();
 
     if (!url)
diff --git a/dom/plugins/ipc/moz.build b/dom/plugins/ipc/moz.build
--- a/dom/plugins/ipc/moz.build
+++ b/dom/plugins/ipc/moz.build
@@ -10,17 +10,16 @@ if CONFIG['MOZ_WIDGET_TOOLKIT'] == 'coco
 EXPORTS.mozilla += [
     'PluginLibrary.h',
 ]
 
 EXPORTS.mozilla.plugins += [
     'AStream.h',
     'BrowserStreamChild.h',
     'BrowserStreamParent.h',
-    'ChildAsyncCall.h',
     'ChildTimer.h',
     'NPEventAndroid.h',
     'NPEventOSX.h',
     'NPEventUnix.h',
     'NPEventWindows.h',
     'PluginBridge.h',
     'PluginInstanceChild.h',
     'PluginInstanceParent.h',
@@ -59,17 +58,16 @@ if CONFIG['OS_ARCH'] == 'WINNT':
 if CONFIG['MOZ_WIDGET_TOOLKIT'] == 'cocoa':
     EXPORTS.mozilla.plugins += [
         'PluginInterposeOSX.h',
     ]
 
 UNIFIED_SOURCES += [
     'BrowserStreamChild.cpp',
     'BrowserStreamParent.cpp',
-    'ChildAsyncCall.cpp',
     'ChildTimer.cpp',
     'PluginBackgroundDestroyer.cpp',
     'PluginInstanceParent.cpp',
     'PluginMessageUtils.cpp',
     'PluginModuleParent.cpp',
     'PluginProcessChild.cpp',
     'PluginProcessParent.cpp',
     'PluginQuirks.cpp',
diff --git a/dom/plugins/test/testplugin/nptest.cpp b/dom/plugins/test/testplugin/nptest.cpp
--- a/dom/plugins/test/testplugin/nptest.cpp
+++ b/dom/plugins/test/testplugin/nptest.cpp
@@ -89,19 +89,16 @@ IntentionalCrash()
 
 //
 // static data
 //
 
 static NPNetscapeFuncs* sBrowserFuncs = nullptr;
 static NPClass sNPClass;
 
-void
-asyncCallback(void* cookie);
-
 //
 // identifiers
 //
 
 typedef bool (* ScriptableFunction)
   (NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 
 static bool npnEvaluateTest(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
@@ -136,18 +133,16 @@ static bool streamTest(NPObject* npobj, 
 static bool postFileToURLTest(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool setPluginWantsAllStreams(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool crashPlugin(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool crashOnDestroy(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool getObjectValue(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool getJavaCodebase(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool checkObjectValue(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool enableFPExceptions(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
-static bool asyncCallbackTest(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
-static bool checkGCRace(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool hangPlugin(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool stallPlugin(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool getClipboardText(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool callOnDestroy(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool reinitWidget(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool crashPluginInNestedLoop(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool triggerXError(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
 static bool destroySharedGfxStuff(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result);
@@ -210,18 +205,16 @@ static const NPUTF8* sPluginMethodIdenti
   "postFileToURLTest",
   "setPluginWantsAllStreams",
   "crash",
   "crashOnDestroy",
   "getObjectValue",
   "getJavaCodebase",
   "checkObjectValue",
   "enableFPExceptions",
-  "asyncCallbackTest",
-  "checkGCRace",
   "hang",
   "stall",
   "getClipboardText",
   "callOnDestroy",
   "reinitWidget",
   "crashInNestedLoop",
   "triggerXError",
   "destroySharedGfxStuff",
@@ -285,18 +278,16 @@ static const ScriptableFunction sPluginM
   postFileToURLTest,
   setPluginWantsAllStreams,
   crashPlugin,
   crashOnDestroy,
   getObjectValue,
   getJavaCodebase,
   checkObjectValue,
   enableFPExceptions,
-  asyncCallbackTest,
-  checkGCRace,
   hangPlugin,
   stallPlugin,
   getClipboardText,
   callOnDestroy,
   reinitWidget,
   crashPluginInNestedLoop,
   triggerXError,
   destroySharedGfxStuff,
@@ -1901,22 +1892,16 @@ NPN_SetValueForURL(NPP instance, NPNURLV
 
 NPError
 NPN_GetValueForURL(NPP instance, NPNURLVariable variable, const char *url, char **value, uint32_t *len)
 {
   return sBrowserFuncs->getvalueforurl(instance, variable, url, value, len);
 }
 
 void
-NPN_PluginThreadAsyncCall(NPP plugin, void (*func)(void*), void* userdata)
-{
-  return sBrowserFuncs->pluginthreadasynccall(plugin, func, userdata);
-}
-
-void
 NPN_URLRedirectResponse(NPP instance, void* notifyData, NPBool allow)
 {
   return sBrowserFuncs->urlredirectresponse(instance, notifyData, allow);
 }
 
 NPError
 NPN_InitAsyncSurface(NPP instance, NPSize *size, NPImageFormat format,
                      void *initData, NPAsyncSurface *surface)
@@ -3009,181 +2994,16 @@ timerTest(NPObject* npobj, const NPVaria
   id->timerTestResult = true;
   timerEvent event = timerEvents[currentTimerEventCount];
 
   id->timerID[event.timerIdSchedule] = NPN_ScheduleTimer(npp, event.timerInterval, event.timerRepeat, timerCallback);
 
   return id->timerID[event.timerIdSchedule] != 0;
 }
 
-#ifdef XP_WIN
-void
-ThreadProc(void* cookie)
-#else
-void*
-ThreadProc(void* cookie)
-#endif
-{
-  NPObject* npobj = (NPObject*)cookie;
-  NPP npp = static_cast<TestNPObject*>(npobj)->npp;
-  InstanceData* id = static_cast<InstanceData*>(npp->pdata);
-  id->asyncTestPhase = 1;
-  NPN_PluginThreadAsyncCall(npp, asyncCallback, (void*)npobj);
-#ifndef XP_WIN
-  return nullptr;
-#endif
-}
-
-void
-asyncCallback(void* cookie)
-{
-  NPObject* npobj = (NPObject*)cookie;
-  NPP npp = static_cast<TestNPObject*>(npobj)->npp;
-  InstanceData* id = static_cast<InstanceData*>(npp->pdata);
-
-  switch (id->asyncTestPhase) {
-    // async callback triggered from same thread
-    case 0:
-#ifdef XP_WIN
-      if (_beginthread(ThreadProc, 0, (void*)npobj) == -1)
-        id->asyncCallbackResult = false;
-#else
-      pthread_t tid;
-      if (pthread_create(&tid, 0, ThreadProc, (void*)npobj))
-        id->asyncCallbackResult = false;
-#endif
-      break;
-
-    // async callback triggered from different thread
-    default:
-      NPObject* windowObject;
-      NPN_GetValue(npp, NPNVWindowNPObject, &windowObject);
-      if (!windowObject)
-        return;
-      NPVariant arg, rval;
-      BOOLEAN_TO_NPVARIANT(id->asyncCallbackResult, arg);
-      NPN_Invoke(npp, windowObject, NPN_GetStringIdentifier(id->asyncTestScriptCallback.c_str()), &arg, 1, &rval);
-      NPN_ReleaseVariantValue(&arg);
-      NPN_ReleaseObject(windowObject);
-      break;
-  }
-}
-
-static bool
-asyncCallbackTest(NPObject* npobj, const NPVariant* args, uint32_t argCount, NPVariant* result)
-{
-  NPP npp = static_cast<TestNPObject*>(npobj)->npp;
-  InstanceData* id = static_cast<InstanceData*>(npp->pdata);
-
-  if (argCount < 1 || !NPVARIANT_IS_STRING(args[0]))
-    return false;
-  const NPString* argstr = &NPVARIANT_TO_STRING(args[0]);
-  id->asyncTestScriptCallback = argstr->UTF8Characters;
-
-  id->asyncTestPhase = 0;
-  id->asyncCallbackResult = true;
-  NPN_PluginThreadAsyncCall(npp, asyncCallback, (void*)npobj);
-
-  return true;
-}
-
-static bool
-GCRaceInvoke(NPObject*, NPIdentifier, const NPVariant*, uint32_t, NPVariant*)
-{
-  return false;
-}
-
-static bool
-GCRaceInvokeDefault(NPObject* o, const NPVariant* args, uint32_t argCount,
-		    NPVariant* result)
-{
-  if (1 != argCount || !NPVARIANT_IS_INT32(args[0]) ||
-      35 != NPVARIANT_TO_INT32(args[0]))
-    return false;
-
-  return true;
-}
-
-static const NPClass kGCRaceClass = {
-  NP_CLASS_STRUCT_VERSION,
-  nullptr,
-  nullptr,
-  nullptr,
-  nullptr,
-  GCRaceInvoke,
-  GCRaceInvokeDefault,
-  nullptr,
-  nullptr,
-  nullptr,
-  nullptr,
-  nullptr,
-  nullptr
-};
-
-struct GCRaceData
-{
-  GCRaceData(NPP npp, NPObject* callback, NPObject* localFunc)
-    : npp_(npp)
-    , callback_(callback)
-    , localFunc_(localFunc)
-  {
-    NPN_RetainObject(callback_);
-    NPN_RetainObject(localFunc_);
-  }
-
-  ~GCRaceData()
-  {
-    NPN_ReleaseObject(callback_);
-    NPN_ReleaseObject(localFunc_);
-  }
-
-  NPP npp_;
-  NPObject* callback_;
-  NPObject* localFunc_;
-};
-
-static void
-FinishGCRace(void* closure)
-{
-  GCRaceData* rd = static_cast<GCRaceData*>(closure);
-
-  XPSleep(5);
-
-  NPVariant arg;
-  OBJECT_TO_NPVARIANT(rd->localFunc_, arg);
-
-  NPVariant result;
-  bool ok = NPN_InvokeDefault(rd->npp_, rd->callback_, &arg, 1, &result);
-  if (!ok)
-    return;
-
-  NPN_ReleaseVariantValue(&result);
-  delete rd;
-}
-
-bool
-checkGCRace(NPObject* npobj, const NPVariant* args, uint32_t argCount,
-	    NPVariant* result)
-{
-  if (1 != argCount || !NPVARIANT_IS_OBJECT(args[0]))
-    return false;
-
-  NPP npp = static_cast<TestNPObject*>(npobj)->npp;
-
-  NPObject* localFunc =
-    NPN_CreateObject(npp, const_cast<NPClass*>(&kGCRaceClass));
-
-  GCRaceData* rd =
-    new GCRaceData(npp, NPVARIANT_TO_OBJECT(args[0]), localFunc);
-  NPN_PluginThreadAsyncCall(npp, FinishGCRace, rd);
-
-  OBJECT_TO_NPVARIANT(localFunc, *result);
-  return true;
-}
-
 bool
 hangPlugin(NPObject* npobj, const NPVariant* args, uint32_t argCount,
            NPVariant* result)
 {
   mozilla::NoteIntentionalCrash("plugin");
 
   bool busyHang = false;
   if ((argCount == 1) && NPVARIANT_IS_BOOLEAN(args[0])) {
diff --git a/dom/plugins/test/testplugin/nptest.h b/dom/plugins/test/testplugin/nptest.h
--- a/dom/plugins/test/testplugin/nptest.h
+++ b/dom/plugins/test/testplugin/nptest.h
@@ -104,39 +104,36 @@ typedef struct InstanceData {
   bool lastReportedPrivateModeState;
   bool hasWidget;
   bool npnNewStream;
   bool throwOnNextInvoke;
   bool runScriptOnPaint;
   bool dontTouchElement;
   uint32_t timerID[2];
   bool timerTestResult;
-  bool asyncCallbackResult;
   bool invalidateDuringPaint;
   bool slowPaint;
   bool playingAudio;
   bool audioMuted;
   int32_t winX;
   int32_t winY;
   int32_t lastMouseX;
   int32_t lastMouseY;
   int32_t widthAtLastPaint;
   int32_t paintCount;
   int32_t writeCount;
   int32_t writeReadyCount;
-  int32_t asyncTestPhase;
   TestFunction testFunction;
   TestFunction functionToFail;
   NPError failureCode;
   NPObject* callOnDestroy;
   PostMode postMode;
   std::string testUrl;
   std::string frame;
   std::string timerTestScriptCallback;
-  std::string asyncTestScriptCallback;
   std::ostringstream err;
   uint16_t streamMode;
   int32_t streamChunkSize;
   int32_t streamBufSize;
   int32_t fileBufSize;
   TestRange* testrange;
   void* streamBuf;
   void* fileBuf;
