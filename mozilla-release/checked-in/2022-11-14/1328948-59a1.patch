# HG changeset patch
# User Yoshi Huang <allstars.chh@mozilla.com>
# Date 1511495430 -28800
# Node ID 163e415d22df1847d49320a83bacbf166adef7cb
# Parent  47b6517cc20a22a83c03c9248c68a45dc026e1a0
Bug 1328948 - add is(), as() to Cell. r=jonco

diff --git a/js/src/gc/Cell.h b/js/src/gc/Cell.h
--- a/js/src/gc/Cell.h
+++ b/js/src/gc/Cell.h
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef gc_Cell_h
 #define gc_Cell_h
 
 #include "gc/GCEnum.h"
 #include "gc/Heap.h"
 #include "js/GCAnnotations.h"
+#include "js/TraceKind.h"
 
 namespace JS {
 
 namespace shadow {
 struct Zone;
 } /* namespace shadow */
 
 enum class TraceKind;
@@ -70,16 +71,33 @@ struct Cell
     // The StoreBuffer used to record incoming pointers from the tenured heap.
     // This will return nullptr for a tenured cell.
     inline StoreBuffer* storeBuffer() const;
 
     inline JS::TraceKind getTraceKind() const;
 
     static MOZ_ALWAYS_INLINE bool needWriteBarrierPre(JS::Zone* zone);
 
+    template <class T>
+    inline bool is() const {
+        return getTraceKind() == JS::MapTypeToTraceKind<T>::kind;
+    }
+
+    template<class T>
+    inline T* as() {
+        MOZ_ASSERT(is<T>());
+        return static_cast<T*>(this);
+    }
+
+    template <class T>
+    inline const T* as() const {
+        MOZ_ASSERT(is<T>());
+        return static_cast<const T*>(this);
+    }
+
 #ifdef DEBUG
     inline bool isAligned() const;
     void dump(GenericPrinter& out) const;
     void dump() const;
 #endif
 
   protected:
     uintptr_t address() const;
diff --git a/js/src/gc/Verifier.cpp b/js/src/gc/Verifier.cpp
--- a/js/src/gc/Verifier.cpp
+++ b/js/src/gc/Verifier.cpp
@@ -684,19 +684,19 @@ CheckGrayMarkingTracer::checkCell(Cell* 
     if (parent->isMarkedBlack() && cell->isMarkedGray()) {
         failures++;
 
         fprintf(stderr, "Found black to gray edge to ");
         dumpCellInfo(cell);
         fprintf(stderr, "\n");
         dumpCellPath();
 
-        if (cell->getTraceKind() == JS::TraceKind::Object) {
+        if (cell->is<JSObject>()) {
             fprintf(stderr, "\n");
-            DumpObject(static_cast<JSObject*>(cell), stderr);
+            DumpObject(cell->as<JSObject>(), stderr);
         }
     }
 }
 
 bool
 CheckGrayMarkingTracer::check(AutoLockForExclusiveAccess& lock)
 {
     if (!traceHeap(lock))
diff --git a/js/src/jsgc.cpp b/js/src/jsgc.cpp
--- a/js/src/jsgc.cpp
+++ b/js/src/jsgc.cpp
@@ -8285,17 +8285,17 @@ JS::AssertGCThingMustBeTenured(JSObject*
                (!IsNurseryAllocable(obj->asTenured().getAllocKind()) ||
                 obj->getClass()->hasFinalize()));
 }
 
 JS_FRIEND_API(void)
 JS::AssertGCThingIsNotAnObjectSubclass(Cell* cell)
 {
     MOZ_ASSERT(cell);
-    MOZ_ASSERT(cell->getTraceKind() != JS::TraceKind::Object);
+    MOZ_ASSERT(!cell->is<JSObject>());
 }
 
 JS_FRIEND_API(void)
 js::gc::AssertGCThingHasType(js::gc::Cell* cell, JS::TraceKind kind)
 {
     MOZ_ASSERT(IsCellPointerValid(cell));
     if (!cell)
         MOZ_ASSERT(kind == JS::TraceKind::Null);
diff --git a/js/src/vm/Debugger.cpp b/js/src/vm/Debugger.cpp
--- a/js/src/vm/Debugger.cpp
+++ b/js/src/vm/Debugger.cpp
@@ -5208,37 +5208,37 @@ GetScriptReferentCell(JSObject* obj)
     return static_cast<gc::Cell*>(obj->as<NativeObject>().getPrivate());
 }
 
 static inline DebuggerScriptReferent
 GetScriptReferent(JSObject* obj)
 {
     MOZ_ASSERT(obj->getClass() == &DebuggerScript_class);
     if (gc::Cell* cell = GetScriptReferentCell(obj)) {
-        if (cell->getTraceKind() == JS::TraceKind::Script)
-            return AsVariant(static_cast<JSScript*>(cell));
-        MOZ_ASSERT(cell->getTraceKind() == JS::TraceKind::Object);
+        if (cell->is<JSScript>())
+            return AsVariant(cell->as<JSScript>());
+        MOZ_ASSERT(cell->is<JSObject>());
         return AsVariant(&static_cast<NativeObject*>(cell)->as<WasmInstanceObject>());
     }
     return AsVariant(static_cast<JSScript*>(nullptr));
 }
 
 void
 DebuggerScript_trace(JSTracer* trc, JSObject* obj)
 {
     /* This comes from a private pointer, so no barrier needed. */
     gc::Cell* cell = GetScriptReferentCell(obj);
     if (cell) {
-        if (cell->getTraceKind() == JS::TraceKind::Script) {
-            JSScript* script = static_cast<JSScript*>(cell);
+        if (cell->is<JSScript>()) {
+            JSScript* script = cell->as<JSScript>();
             TraceManuallyBarrieredCrossCompartmentEdge(trc, obj, &script,
                                                        "Debugger.Script script referent");
             obj->as<NativeObject>().setPrivateUnbarriered(script);
         } else {
-            JSObject* wasm = static_cast<JSObject*>(cell);
+            JSObject* wasm = cell->as<JSObject>();
             TraceManuallyBarrieredCrossCompartmentEdge(trc, obj, &wasm,
                                                        "Debugger.Script wasm referent");
             MOZ_ASSERT(wasm->is<WasmInstanceObject>());
             obj->as<NativeObject>().setPrivateUnbarriered(wasm);
         }
     }
 }
 
