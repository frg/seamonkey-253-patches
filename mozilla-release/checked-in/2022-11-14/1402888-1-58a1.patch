# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1506403287 -7200
#      Tue Sep 26 07:21:27 2017 +0200
# Node ID d434da3de9ea5e9b30238ccffde7faf43d51c427
# Parent  6d1c602d39f977d110e9394ee522cb7be8ec529f
 Bug 1402888 - Remove nsIStreamTransportService::createInputStream startOffset and readLimit params, r=smaug

diff --git a/dom/fetch/FetchStream.cpp b/dom/fetch/FetchStream.cpp
--- a/dom/fetch/FetchStream.cpp
+++ b/dom/fetch/FetchStream.cpp
@@ -219,18 +219,16 @@ FetchStream::RequestDataCallback(JSConte
         do_GetService(kStreamTransportServiceCID, &rv);
       if (NS_WARN_IF(NS_FAILED(rv))) {
         stream->ErrorPropagation(aCx, aStream, rv);
         return;
       }
 
       nsCOMPtr<nsITransport> transport;
       rv = sts->CreateInputTransport(stream->mOriginalInputStream,
-                                     /* aStartOffset */ 0,
-                                     /* aReadLimit */ -1,
                                      /* aCloseWhenDone */ true,
                                      getter_AddRefs(transport));
       if (NS_WARN_IF(NS_FAILED(rv))) {
         stream->ErrorPropagation(aCx, aStream, rv);
         return;
       }
 
       nsCOMPtr<nsIInputStream> wrapper;
diff --git a/dom/file/FileReader.cpp b/dom/file/FileReader.cpp
--- a/dom/file/FileReader.cpp
+++ b/dom/file/FileReader.cpp
@@ -433,18 +433,16 @@ FileReader::ReadFileContent(Blob& aBlob,
       do_GetService(kStreamTransportServiceCID, &rv);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       aRv.Throw(rv);
       return;
     }
 
     nsCOMPtr<nsITransport> transport;
     aRv = sts->CreateInputTransport(stream,
-                                    /* aStartOffset */ 0,
-                                    /* aReadLimit */ -1,
                                     /* aCloseWhenDone */ true,
                                     getter_AddRefs(transport));
     if (NS_WARN_IF(aRv.Failed())) {
       return;
     }
 
     nsCOMPtr<nsIInputStream> wrapper;
     aRv = transport->OpenInputStream(/* aFlags */ 0,
diff --git a/dom/file/ipc/IPCBlobInputStream.cpp b/dom/file/ipc/IPCBlobInputStream.cpp
--- a/dom/file/ipc/IPCBlobInputStream.cpp
+++ b/dom/file/ipc/IPCBlobInputStream.cpp
@@ -647,18 +647,16 @@ IPCBlobInputStream::EnsureAsyncRemoteStr
     nsCOMPtr<nsIStreamTransportService> sts =
       do_GetService(kStreamTransportServiceCID, &rv);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
     }
 
     nsCOMPtr<nsITransport> transport;
     rv = sts->CreateInputTransport(mRemoteStream,
-                                   /* aStartOffset */ 0,
-                                   /* aReadLimit */ -1,
                                    /* aCloseWhenDone */ true,
                                    getter_AddRefs(transport));
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
     }
 
     nsCOMPtr<nsIInputStream> wrapper;
     rv = transport->OpenInputStream(/* aFlags */ 0,
diff --git a/netwerk/base/nsIStreamTransportService.idl b/netwerk/base/nsIStreamTransportService.idl
--- a/netwerk/base/nsIStreamTransportService.idl
+++ b/netwerk/base/nsIStreamTransportService.idl
@@ -20,32 +20,23 @@ interface nsIInputAvailableCallback;
 interface nsIStreamTransportService : nsISupports
 {
     /**
      * CreateInputTransport
      *
      * @param aStream
      *        The input stream that will be read on a background thread.
      *        This stream must implement "blocking" stream semantics.
-     * @param aStartOffset
-     *        The input stream will be read starting from this offset.  Pass
-     *        -1 to read from the current stream offset.  NOTE: this parameter
-     *        is ignored if the stream does not support nsISeekableStream.
-     * @param aReadLimit
-     *        This parameter limits the number of bytes that will be read from
-     *        the input stream.  Pass -1 to read everything.
      * @param aCloseWhenDone
      *        Specify this flag to have the input stream closed once its
      *        contents have been completely read.
      *
      * @return nsITransport instance.
      */
     nsITransport createInputTransport(in nsIInputStream aStream,
-                                      in long long aStartOffset,
-                                      in long long aReadLimit,
                                       in boolean aCloseWhenDone);
 
     void InputAvailable(in nsIInputStream aStream,
                         in nsIInputAvailableCallback aCallback);
     /**
      * CreateOutputTransport
      *
      * @param aStream
diff --git a/netwerk/base/nsInputStreamPump.cpp b/netwerk/base/nsInputStreamPump.cpp
--- a/netwerk/base/nsInputStreamPump.cpp
+++ b/netwerk/base/nsInputStreamPump.cpp
@@ -13,16 +13,17 @@
 #include "nsThreadUtils.h"
 #include "nsCOMPtr.h"
 #include "mozilla/Logging.h"
 #include "GeckoProfiler.h"
 #include "nsIStreamListener.h"
 #include "nsILoadGroup.h"
 #include "nsNetCID.h"
 #include "nsStreamUtils.h"
+#include "SlicedInputStream.h"
 #include <algorithm>
 
 static NS_DEFINE_CID(kStreamTransportServiceCID, NS_STREAMTRANSPORTSERVICE_CID);
 
 //
 // MOZ_LOG=nsStreamPump:5
 //
 static mozilla::LazyLogModule gStreamPumpLog("nsStreamPump");
@@ -326,45 +327,37 @@ nsInputStreamPump::AsyncRead(nsIStreamLi
                                   "main thread only.");
 
     //
     // OK, we need to use the stream transport service if
     //
     // (1) the stream is blocking
     // (2) the stream does not support nsIAsyncInputStream
     //
+    if (mStreamOffset != UINT64_MAX || mStreamLength != UINT64_MAX) {
+        mStream = new SlicedInputStream(mStream, mStreamOffset, mStreamLength);
+        mStreamOffset = 0;
+    }
 
     bool nonBlocking;
     nsresult rv = mStream->IsNonBlocking(&nonBlocking);
     if (NS_FAILED(rv)) return rv;
 
     if (nonBlocking) {
         mAsyncStream = do_QueryInterface(mStream);
-        //
-        // if the stream supports nsIAsyncInputStream, and if we need to seek
-        // to a starting offset, then we must do so here.  in the non-async
-        // stream case, the stream transport service will take care of seeking
-        // for us.
-        //
-        if (mAsyncStream && (mStreamOffset != UINT64_MAX)) {
-            nsCOMPtr<nsISeekableStream> seekable = do_QueryInterface(mStream);
-            if (seekable)
-                seekable->Seek(nsISeekableStream::NS_SEEK_SET, mStreamOffset);
-        }
     }
 
     if (!mAsyncStream) {
         // ok, let's use the stream transport service to read this stream.
         nsCOMPtr<nsIStreamTransportService> sts =
             do_GetService(kStreamTransportServiceCID, &rv);
         if (NS_FAILED(rv)) return rv;
 
         nsCOMPtr<nsITransport> transport;
-        rv = sts->CreateInputTransport(mStream, mStreamOffset, mStreamLength,
-                                       mCloseWhenDone, getter_AddRefs(transport));
+        rv = sts->CreateInputTransport(mStream, mCloseWhenDone, getter_AddRefs(transport));
         if (NS_FAILED(rv)) return rv;
 
         nsCOMPtr<nsIInputStream> wrapper;
         rv = transport->OpenInputStream(0, mSegSize, mSegCount, getter_AddRefs(wrapper));
         if (NS_FAILED(rv)) return rv;
 
         mAsyncStream = do_QueryInterface(wrapper, &rv);
         if (NS_FAILED(rv)) return rv;
diff --git a/netwerk/base/nsStreamTransportService.cpp b/netwerk/base/nsStreamTransportService.cpp
--- a/netwerk/base/nsStreamTransportService.cpp
+++ b/netwerk/base/nsStreamTransportService.cpp
@@ -34,43 +34,37 @@ class nsInputStreamTransport : public ns
                              , public nsIInputStream
 {
 public:
     NS_DECL_THREADSAFE_ISUPPORTS
     NS_DECL_NSITRANSPORT
     NS_DECL_NSIINPUTSTREAM
 
     nsInputStreamTransport(nsIInputStream *source,
-                           uint64_t offset,
-                           uint64_t limit,
                            bool closeWhenDone)
         : mSource(source)
-        , mOffset(offset)
-        , mLimit(limit)
+        , mOffset(0)
         , mCloseWhenDone(closeWhenDone)
-        , mFirstTime(true)
         , mInProgress(false)
     {
     }
 
 private:
     virtual ~nsInputStreamTransport()
     {
     }
 
     nsCOMPtr<nsIAsyncInputStream>   mPipeIn;
 
     // while the copy is active, these members may only be accessed from the
     // nsIInputStream implementation.
     nsCOMPtr<nsITransportEventSink> mEventSink;
     nsCOMPtr<nsIInputStream>        mSource;
     int64_t                         mOffset;
-    int64_t                         mLimit;
     bool                            mCloseWhenDone;
-    bool                            mFirstTime;
 
     // this variable serves as a lock to prevent the state of the transport
     // from being modified once the copy is in progress.
     bool                            mInProgress;
 };
 
 NS_IMPL_ISUPPORTS(nsInputStreamTransport,
                   nsITransport,
@@ -155,63 +149,36 @@ nsInputStreamTransport::SetEventSink(nsI
 
 NS_IMETHODIMP
 nsInputStreamTransport::Close()
 {
     if (mCloseWhenDone)
         mSource->Close();
 
     // make additional reads return early...
-    mOffset = mLimit = 0;
+    mOffset = 0;
     return NS_OK;
 }
 
 NS_IMETHODIMP
 nsInputStreamTransport::Available(uint64_t *result)
 {
     return NS_ERROR_NOT_IMPLEMENTED;
 }
 
 NS_IMETHODIMP
 nsInputStreamTransport::Read(char *buf, uint32_t count, uint32_t *result)
 {
-    if (mFirstTime) {
-        mFirstTime = false;
-        if (mOffset != 0) {
-            // read from current position if offset equal to max
-            if (mOffset != -1) {
-                nsCOMPtr<nsISeekableStream> seekable = do_QueryInterface(mSource);
-                if (seekable)
-                    seekable->Seek(nsISeekableStream::NS_SEEK_SET, mOffset);
-            }
-            // reset offset to zero so we can use it to enforce limit
-            mOffset = 0;
-        }
-    }
-
-    // limit amount read
-    uint64_t max = count;
-    if (mLimit != -1) {
-        max = mLimit - mOffset;
-        if (max == 0) {
-            *result = 0;
-            return NS_OK;
-        }
-    }
-
-    if (count > max)
-        count = static_cast<uint32_t>(max);
-
     nsresult rv = mSource->Read(buf, count, result);
 
     if (NS_SUCCEEDED(rv)) {
         mOffset += *result;
         if (mEventSink)
             mEventSink->OnTransportStatus(this, NS_NET_STATUS_READING, mOffset,
-                                          mLimit);
+                                          -1);
     }
     return rv;
 }
 
 NS_IMETHODIMP
 nsInputStreamTransport::ReadSegments(nsWriteSegmentFun writer, void *closure,
                                      uint32_t count, uint32_t *result)
 {
@@ -522,23 +489,21 @@ nsStreamTransportService::IsOnCurrentThr
         pool = mPool;
     }
     NS_ENSURE_TRUE(pool, NS_ERROR_NOT_INITIALIZED);
     return pool->IsOnCurrentThread(result);
 }
 
 NS_IMETHODIMP
 nsStreamTransportService::CreateInputTransport(nsIInputStream *stream,
-                                               int64_t offset,
-                                               int64_t limit,
                                                bool closeWhenDone,
                                                nsITransport **result)
 {
     nsInputStreamTransport *trans =
-        new nsInputStreamTransport(stream, offset, limit, closeWhenDone);
+        new nsInputStreamTransport(stream, closeWhenDone);
     if (!trans)
         return NS_ERROR_OUT_OF_MEMORY;
     NS_ADDREF(*result = trans);
     return NS_OK;
 }
 
 NS_IMETHODIMP
 nsStreamTransportService::CreateOutputTransport(nsIOutputStream *stream,
diff --git a/netwerk/protocol/http/nsHttpChannel.cpp b/netwerk/protocol/http/nsHttpChannel.cpp
--- a/netwerk/protocol/http/nsHttpChannel.cpp
+++ b/netwerk/protocol/http/nsHttpChannel.cpp
@@ -4788,18 +4788,17 @@ nsHttpChannel::OpenCacheInputStream(nsIC
     // background threads.
 
     nsCOMPtr<nsITransport> transport;
     nsCOMPtr<nsIInputStream> wrapper;
 
     nsCOMPtr<nsIStreamTransportService> sts(services::GetStreamTransportService());
     rv = sts ? NS_OK : NS_ERROR_NOT_AVAILABLE;
     if (NS_SUCCEEDED(rv)) {
-        rv = sts->CreateInputTransport(stream, int64_t(-1), int64_t(-1),
-                                        true, getter_AddRefs(transport));
+        rv = sts->CreateInputTransport(stream, true, getter_AddRefs(transport));
     }
     if (NS_SUCCEEDED(rv)) {
         rv = transport->OpenInputStream(0, 0, 0, getter_AddRefs(wrapper));
     }
     if (NS_SUCCEEDED(rv)) {
         LOG(("Opened cache input stream [channel=%p, wrapper=%p, "
               "transport=%p, stream=%p]", this, wrapper.get(),
               transport.get(), stream.get()));
diff --git a/toolkit/modules/ZipUtils.jsm b/toolkit/modules/ZipUtils.jsm
--- a/toolkit/modules/ZipUtils.jsm
+++ b/toolkit/modules/ZipUtils.jsm
@@ -31,17 +31,17 @@ const EXTRACTION_BUFFER               = 
  *         The open OS.File instance to write to.
  */
 function saveStreamAsync(aPath, aStream, aFile) {
   return new Promise((resolve, reject) => {
 
     // Read the input stream on a background thread
     let sts = Cc["@mozilla.org/network/stream-transport-service;1"].
               getService(Ci.nsIStreamTransportService);
-    let transport = sts.createInputTransport(aStream, -1, -1, true);
+    let transport = sts.createInputTransport(aStream, true);
     let input = transport.openInputStream(0, 0, 0)
                          .QueryInterface(Ci.nsIAsyncInputStream);
     let source = Cc["@mozilla.org/binaryinputstream;1"].
                  createInstance(Ci.nsIBinaryInputStream);
     source.setInputStream(input);
 
 
     function readFailed(error) {
