# HG changeset patch
# User Brian Birtles <birtles@gmail.com>
# Date 1506915347 -32400
# Node ID 717c80a8bc24730442062bda4678e11261c67fea
# Parent  fb45747c4118730a7d3234583d7282837deb4928
Bug 1404774 - Defer throwing errors from parsing keyframe easing until after reading off all properties; r=hiro

As required by the recent spec change:

  https://github.com/w3c/web-animations/commit/d696468777c12a13bc7c46aa1a72c358e8da15fc

MozReview-Commit-ID: Ev6kUk1uLAY

diff --git a/dom/animation/KeyframeUtils.cpp b/dom/animation/KeyframeUtils.cpp
--- a/dom/animation/KeyframeUtils.cpp
+++ b/dom/animation/KeyframeUtils.cpp
@@ -630,16 +630,17 @@ GetKeyframeListFromKeyframeSequence(JSCo
 static bool
 ConvertKeyframeSequence(JSContext* aCx,
                         nsIDocument* aDocument,
                         JS::ForOfIterator& aIterator,
                         nsTArray<Keyframe>& aResult)
 {
   JS::Rooted<JS::Value> value(aCx);
   nsCSSParser parser(aDocument->CSSLoader());
+  ErrorResult parseEasingResult;
 
   for (;;) {
     bool done;
     if (!aIterator.next(&value, &done)) {
       return false;
     }
     if (done) {
       break;
@@ -667,35 +668,40 @@ ConvertKeyframeSequence(JSContext* aCx,
     if (!keyframeDict.mOffset.IsNull()) {
       keyframe->mOffset.emplace(keyframeDict.mOffset.Value());
     }
 
     if (keyframeDict.mComposite.WasPassed()) {
       keyframe->mComposite.emplace(keyframeDict.mComposite.Value());
     }
 
-    ErrorResult rv;
-    keyframe->mTimingFunction =
-      TimingParams::ParseEasing(keyframeDict.mEasing, aDocument, rv);
-    if (rv.MaybeSetPendingException(aCx)) {
-      return false;
-    }
-
     // Look for additional property-values pairs on the object.
     nsTArray<PropertyValuesPair> propertyValuePairs;
     if (value.isObject()) {
       JS::Rooted<JSObject*> object(aCx, &value.toObject());
       if (!GetPropertyValuesPairs(aCx, object,
                                   ListAllowance::eDisallow,
                                   aDocument->GetStyleBackendType(),
                                   propertyValuePairs)) {
         return false;
       }
     }
 
+    if (!parseEasingResult.Failed()) {
+      keyframe->mTimingFunction =
+        TimingParams::ParseEasing(keyframeDict.mEasing,
+                                  aDocument,
+                                  parseEasingResult);
+      // Even if the above fails, we still need to continue reading off all the
+      // properties since checking the validity of easing should be treated as
+      // a separate step that happens *after* all the other processing in this
+      // loop since (since it is never likely to be handled by WebIDL unlike the
+      // rest of this loop).
+    }
+
     for (PropertyValuesPair& pair : propertyValuePairs) {
       MOZ_ASSERT(pair.mValues.Length() == 1);
 
       Maybe<PropertyValuePair> valuePair =
         MakePropertyValuePair(pair.mProperty, pair.mValues[0], parser,
                               aDocument);
       if (!valuePair) {
         continue;
@@ -712,16 +718,21 @@ ConvertKeyframeSequence(JSContext* aCx,
       if (nsCSSProps::IsShorthand(pair.mProperty) &&
           keyframeDict.mSimulateComputeValuesFailure) {
         MarkAsComputeValuesFailureKey(keyframe->mPropertyValues.LastElement());
       }
 #endif
     }
   }
 
+  // Throw any errors we encountered while parsing 'easing' properties.
+  if (parseEasingResult.MaybeSetPendingException(aCx)) {
+    return false;
+  }
+
   return true;
 }
 
 /**
  * Reads the property-values pairs from the specified JS object.
  *
  * @param aObject The JS object to look at.
  * @param aAllowLists If eAllow, values will be converted to
@@ -1382,32 +1393,35 @@ GetKeyframeListFromPropertyIndexedKeyfra
     return;
   }
 
   Maybe<dom::CompositeOperation> composite;
   if (keyframeDict.mComposite.WasPassed()) {
     composite.emplace(keyframeDict.mComposite.Value());
   }
 
-  Maybe<ComputedTimingFunction> easing =
-    TimingParams::ParseEasing(keyframeDict.mEasing, aDocument, aRv);
-  if (aRv.Failed()) {
-    return;
-  }
-
   // Get all the property--value-list pairs off the object.
   JS::Rooted<JSObject*> object(aCx, &aValue.toObject());
   nsTArray<PropertyValuesPair> propertyValuesPairs;
   if (!GetPropertyValuesPairs(aCx, object, ListAllowance::eAllow,
                               aDocument->GetStyleBackendType(),
                               propertyValuesPairs)) {
     aRv.Throw(NS_ERROR_FAILURE);
     return;
   }
 
+  // Parse the easing property. We need to do this after reading off the
+  // property values since this is conceptually a separate step that happens
+  // after the WebIDL-like processing.
+  Maybe<ComputedTimingFunction> easing =
+    TimingParams::ParseEasing(keyframeDict.mEasing, aDocument, aRv);
+  if (aRv.Failed()) {
+    return;
+  }
+
   // Create a set of keyframes for each property.
   nsCSSParser parser(aDocument->CSSLoader());
   nsClassHashtable<nsFloatHashKey, Keyframe> processedKeyframes;
   for (const PropertyValuesPair& pair : propertyValuesPairs) {
     size_t count = pair.mValues.Length();
     if (count == 0) {
       // No animation values for this property.
       continue;
