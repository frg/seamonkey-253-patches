# HG changeset patch
# User David Parks <daparks@mozilla.com>
# Date 1617737291 0
#      Tue Apr 06 19:28:11 2021 +0000
# Node ID 4dba093cbb340ff05e8c97560afd3f6a766a96b4
# Parent  799839b3e43372f262426ac4a468d4a78408e299
Bug 1682030 - Remove NPAPI plugin support from accessible.  r=eeejay

Removes NPAPI plugin support from accessible/, as part of removing all of NPAPI plugin support.  This was the only reason for HTMLWin32ObjectAccessible was needed, so we get rid of that, too.

Differential Revision: https://phabricator.services.mozilla.com/D107143

diff --git a/accessible/base/nsAccessibilityService.cpp b/accessible/base/nsAccessibilityService.cpp
--- a/accessible/base/nsAccessibilityService.cpp
+++ b/accessible/base/nsAccessibilityService.cpp
@@ -40,17 +40,16 @@
 
 #ifdef MOZ_ACCESSIBILITY_ATK
 #include "AtkSocketAccessible.h"
 #endif
 
 #ifdef XP_WIN
 #include "mozilla/a11y/Compatibility.h"
 #include "mozilla/dom/ContentChild.h"
-#include "HTMLWin32ObjectAccessible.h"
 #include "mozilla/StaticPtr.h"
 #endif
 
 #ifdef A11Y_LOG
 #include "Logging.h"
 #endif
 
 #include "nsExceptionHandler.h"
diff --git a/accessible/ipc/DocAccessibleParent.cpp b/accessible/ipc/DocAccessibleParent.cpp
--- a/accessible/ipc/DocAccessibleParent.cpp
+++ b/accessible/ipc/DocAccessibleParent.cpp
@@ -698,48 +698,16 @@ DocAccessibleParent::SetEmulatedWindowHa
 {
   if (!aWindowHandle && mEmulatedWindowHandle && IsTopLevel()) {
     ::DestroyWindow(mEmulatedWindowHandle);
   }
   mEmulatedWindowHandle = aWindowHandle;
 }
 
 mozilla::ipc::IPCResult
-DocAccessibleParent::RecvGetWindowedPluginIAccessible(
-      const WindowsHandle& aHwnd, IAccessibleHolder* aPluginCOMProxy)
-{
-#if defined(MOZ_CONTENT_SANDBOX)
-  // We don't actually want the accessible object for aHwnd, but rather the
-  // one that belongs to its child (see HTMLWin32ObjectAccessible).
-  HWND childWnd = ::GetWindow(reinterpret_cast<HWND>(aHwnd), GW_CHILD);
-  if (!childWnd) {
-    // We're seeing this in the wild - the plugin is windowed but we no longer
-    // have a window.
-    return IPC_OK();
-  }
-
-  IAccessible* rawAccPlugin = nullptr;
-  HRESULT hr = ::AccessibleObjectFromWindow(childWnd, OBJID_WINDOW,
-                                            IID_IAccessible,
-                                            (void**)&rawAccPlugin);
-  if (FAILED(hr)) {
-    // This might happen if the plugin doesn't handle WM_GETOBJECT properly.
-    // We should not consider that a failure.
-    return IPC_OK();
-  }
-
-  aPluginCOMProxy->Set(IAccessibleHolder::COMPtrType(rawAccPlugin));
-
-  return IPC_OK();
-#else
-  return IPC_FAIL(this, "Message unsupported in this build configuration");
-#endif
-}
-
-mozilla::ipc::IPCResult
 DocAccessibleParent::RecvFocusEvent(const uint64_t& aID,
                                     const LayoutDeviceIntRect& aCaretRect)
 {
   if (mShutdown) {
     return IPC_OK();
   }
 
   ProxyAccessible* proxy = GetAccessible(aID);
diff --git a/accessible/ipc/DocAccessibleParent.h b/accessible/ipc/DocAccessibleParent.h
--- a/accessible/ipc/DocAccessibleParent.h
+++ b/accessible/ipc/DocAccessibleParent.h
@@ -184,19 +184,16 @@ public:
   { return const_cast<DocAccessibleParent*>(this)->ChildDocAt(aIdx); }
   DocAccessibleParent* ChildDocAt(size_t aIdx)
     { return LiveDocs().Get(mChildDocs[aIdx]); }
 
 #if defined(XP_WIN)
   void MaybeInitWindowEmulation();
   void SendParentCOMProxy();
 
-  virtual mozilla::ipc::IPCResult RecvGetWindowedPluginIAccessible(
-      const WindowsHandle& aHwnd, IAccessibleHolder* aPluginCOMProxy) override;
-
   /**
    * Set emulated native window handle for a document.
    * @param aWindowHandle emulated native window handle
    */
   void SetEmulatedWindowHandle(HWND aWindowHandle);
   HWND GetEmulatedWindowHandle() const { return mEmulatedWindowHandle; }
 #endif
 
diff --git a/accessible/ipc/win/PDocAccessible.ipdl b/accessible/ipc/win/PDocAccessible.ipdl
--- a/accessible/ipc/win/PDocAccessible.ipdl
+++ b/accessible/ipc/win/PDocAccessible.ipdl
@@ -64,19 +64,16 @@ parent:
   async FocusEvent(uint64_t aID, LayoutDeviceIntRect aCaretRect);
 
   /*
    * Tell the parent document to bind the existing document as a new child
    * document.
    */
   async BindChildDoc(PDocAccessible aChildDoc, uint64_t aID);
 
-  sync GetWindowedPluginIAccessible(WindowsHandle aHwnd)
-    returns (IAccessibleHolder aPluginCOMProxy);
-
 child:
   /**
    * We use IDispatchHolder instead of IAccessibleHolder for the following two
    * methods because of sandboxing. IAccessible::get_accParent returns the parent
    * as an IDispatch. COM is not smart enough to understand that IAccessible is
    * derived from IDispatch, so during marshaling it attempts to QueryInterface
    * on the parent's proxy for IDispatch. This will fail with E_ACCESSDENIED
    * thanks to the sandbox. We can avoid this entirely by just giving content
diff --git a/accessible/tests/mochitest/common.js b/accessible/tests/mochitest/common.js
--- a/accessible/tests/mochitest/common.js
+++ b/accessible/tests/mochitest/common.js
@@ -847,31 +847,16 @@ function getMainChromeWindow(aWindow) {
   return aWindow.QueryInterface(Ci.nsIInterfaceRequestor)
                 .getInterface(Ci.nsIWebNavigation)
                 .QueryInterface(Ci.nsIDocShellTreeItem)
                 .rootTreeItem
                 .QueryInterface(Ci.nsIInterfaceRequestor)
                 .getInterface(Ci.nsIDOMWindow);
 }
 
-/** Sets the test plugin(s) initially expected enabled state.
- * It will automatically be reset to it's previous value after the test
- * ends.
- * @param aNewEnabledState [in] the enabled state, e.g. SpecialPowers.Ci.nsIPluginTag.STATE_ENABLED
- * @param aPluginName [in, optional] The name of the plugin, defaults to "Test Plug-in"
- */
-function setTestPluginEnabledState(aNewEnabledState, aPluginName) {
-  var plugin = getTestPluginTag(aPluginName);
-  var oldEnabledState = plugin.enabledState;
-  plugin.enabledState = aNewEnabledState;
-  SimpleTest.registerCleanupFunction(function() {
-    getTestPluginTag(aPluginName).enabledState = oldEnabledState;
-  });
-}
-
 // //////////////////////////////////////////////////////////////////////////////
 // Private
 // //////////////////////////////////////////////////////////////////////////////
 
 // //////////////////////////////////////////////////////////////////////////////
 // Accessible general
 
 function getNodePrettyName(aNode) {
@@ -895,31 +880,16 @@ function getObjAddress(aObj) {
   var exp = /native\s*@\s*(0x[a-f0-9]+)/g;
   var match = exp.exec(aObj.toString());
   if (match)
     return match[1];
 
   return aObj.toString();
 }
 
-function getTestPluginTag(aPluginName) {
-  var ph = SpecialPowers.Cc["@mozilla.org/plugin/host;1"]
-                        .getService(SpecialPowers.Ci.nsIPluginHost);
-  var tags = ph.getPluginTags();
-  var name = aPluginName || "Test Plug-in";
-  for (var tag of tags) {
-    if (tag.name == name) {
-      return tag;
-    }
-  }
-
-  ok(false, "Could not find plugin tag with plugin name '" + name + "'");
-  return null;
-}
-
 function normalizeAccTreeObj(aObj) {
   var key = Object.keys(aObj)[0];
   var roleName = "ROLE_" + key;
   if (roleName in nsIAccessibleRole) {
     return {
       role: nsIAccessibleRole[roleName],
       children: aObj[key]
     };
diff --git a/accessible/windows/msaa/HTMLWin32ObjectAccessible.cpp b/accessible/windows/msaa/HTMLWin32ObjectAccessible.cpp
deleted file mode 100644
--- a/accessible/windows/msaa/HTMLWin32ObjectAccessible.cpp
+++ /dev/null
@@ -1,109 +0,0 @@
-/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#include "HTMLWin32ObjectAccessible.h"
-
-#include "Role.h"
-#include "States.h"
-
-using namespace mozilla::a11y;
-
-////////////////////////////////////////////////////////////////////////////////
-// HTMLWin32ObjectOwnerAccessible
-////////////////////////////////////////////////////////////////////////////////
-
-HTMLWin32ObjectOwnerAccessible::
-  HTMLWin32ObjectOwnerAccessible(nsIContent* aContent,
-                                 DocAccessible* aDoc, void* aHwnd) :
-  AccessibleWrap(aContent, aDoc), mHwnd(aHwnd)
-{
-  mStateFlags |= eNoKidsFromDOM;
-
-  // Our only child is a HTMLWin32ObjectAccessible object.
-  if (mHwnd) {
-    mNativeAccessible = new HTMLWin32ObjectAccessible(mHwnd, aDoc);
-    AppendChild(mNativeAccessible);
-  }
-}
-
-////////////////////////////////////////////////////////////////////////////////
-// HTMLWin32ObjectOwnerAccessible: Accessible implementation
-
-void
-HTMLWin32ObjectOwnerAccessible::Shutdown()
-{
-  AccessibleWrap::Shutdown();
-  mNativeAccessible = nullptr;
-}
-
-role
-HTMLWin32ObjectOwnerAccessible::NativeRole()
-{
-  return roles::EMBEDDED_OBJECT;
-}
-
-bool
-HTMLWin32ObjectOwnerAccessible::NativelyUnavailable() const
-{
-  // XXX: No HWND means this is windowless plugin which is not accessible in
-  // the meantime.
-  return !mHwnd;
-}
-
-////////////////////////////////////////////////////////////////////////////////
-// HTMLWin32ObjectAccessible
-////////////////////////////////////////////////////////////////////////////////
-
-HTMLWin32ObjectAccessible::HTMLWin32ObjectAccessible(void* aHwnd,
-                                                     DocAccessible* aDoc) :
-  DummyAccessible(aDoc)
-{
-  mHwnd = aHwnd;
-  if (mHwnd) {
-#if defined(MOZ_CONTENT_SANDBOX)
-    if (XRE_IsContentProcess()) {
-      DocAccessibleChild* ipcDoc = aDoc->IPCDoc();
-      MOZ_ASSERT(ipcDoc);
-      if (!ipcDoc) {
-        return;
-      }
-
-      IAccessibleHolder proxyHolder;
-      if (!ipcDoc->SendGetWindowedPluginIAccessible(
-              reinterpret_cast<uintptr_t>(mHwnd), &proxyHolder)) {
-        return;
-      }
-
-      mCOMProxy.reset(proxyHolder.Release());
-      return;
-    }
-#endif
-
-    // The plugin is not windowless. In this situation we use
-    // use its inner child owned by the plugin so that we don't get
-    // in an infinite loop, where the WM_GETOBJECT's get forwarded
-    // back to us and create another HTMLWin32ObjectAccessible
-    mHwnd = ::GetWindow((HWND)aHwnd, GW_CHILD);
-  }
-}
-
-void
-HTMLWin32ObjectAccessible::GetNativeInterface(void** aNativeAccessible)
-{
-#if defined(MOZ_CONTENT_SANDBOX)
-  if (XRE_IsContentProcess()) {
-    RefPtr<IAccessible> addRefed = mCOMProxy.get();
-    addRefed.forget(aNativeAccessible);
-    return;
-  }
-#endif
-
-  if (mHwnd) {
-    ::AccessibleObjectFromWindow(static_cast<HWND>(mHwnd),
-                                 OBJID_WINDOW, IID_IAccessible,
-                                 aNativeAccessible);
-  }
-}
-
diff --git a/accessible/windows/msaa/HTMLWin32ObjectAccessible.h b/accessible/windows/msaa/HTMLWin32ObjectAccessible.h
deleted file mode 100644
--- a/accessible/windows/msaa/HTMLWin32ObjectAccessible.h
+++ /dev/null
@@ -1,70 +0,0 @@
-/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#ifndef mozilla_a11y_HTMLWin32ObjectAccessible_h_
-#define mozilla_a11y_HTMLWin32ObjectAccessible_h_
-
-#include "BaseAccessibles.h"
-
-#if defined(MOZ_CONTENT_SANDBOX)
-#include "mozilla/mscom/Ptr.h"
-#endif
-
-struct IAccessible;
-
-namespace mozilla {
-namespace a11y {
-
-class HTMLWin32ObjectOwnerAccessible : public AccessibleWrap
-{
-public:
-  // This will own the HTMLWin32ObjectAccessible. We create this where the
-  // <object> or <embed> exists in the tree, so that get_accNextSibling() etc.
-  // will still point to Gecko accessible sibling content. This is necessary
-  // because the native plugin accessible doesn't know where it exists in the
-  // Mozilla tree, and returns null for previous and next sibling. This would
-  // have the effect of cutting off all content after the plugin.
-  HTMLWin32ObjectOwnerAccessible(nsIContent* aContent,
-                                 DocAccessible* aDoc, void* aHwnd);
-  virtual ~HTMLWin32ObjectOwnerAccessible() {}
-
-  // Accessible
-  virtual void Shutdown();
-  virtual mozilla::a11y::role NativeRole();
-  virtual bool NativelyUnavailable() const;
-
-protected:
-  void* mHwnd;
-  RefPtr<Accessible> mNativeAccessible;
-};
-
-/**
-  * This class is used only internally, we never! send out an IAccessible linked
-  *   back to this object. This class is used to represent a plugin object when
-  *   referenced as a child or sibling of another Accessible node. We need only
-  *   a limited portion of the Accessible interface implemented here. The
-  *   in depth accessible information will be returned by the actual IAccessible
-  *   object returned by us in Accessible::NewAccessible() that gets the IAccessible
-  *   from the windows system from the window handle.
-  */
-class HTMLWin32ObjectAccessible : public DummyAccessible
-{
-public:
-  HTMLWin32ObjectAccessible(void* aHwnd, DocAccessible* aDoc);
-  virtual ~HTMLWin32ObjectAccessible() {}
-
-  virtual void GetNativeInterface(void** aNativeAccessible) override;
-
-protected:
-  void* mHwnd;
-#if defined(MOZ_CONTENT_SANDBOX)
-  mscom::ProxyUniquePtr<IAccessible> mCOMProxy;
-#endif
-};
-
-} // namespace a11y
-} // namespace mozilla
-
-#endif
diff --git a/accessible/windows/msaa/moz.build b/accessible/windows/msaa/moz.build
--- a/accessible/windows/msaa/moz.build
+++ b/accessible/windows/msaa/moz.build
@@ -22,17 +22,16 @@ UNIFIED_SOURCES += [
     'AccessibleWrap.cpp',
     'ApplicationAccessibleWrap.cpp',
     'ARIAGridAccessibleWrap.cpp',
     'Compatibility.cpp',
     'DocAccessibleWrap.cpp',
     'EnumVariant.cpp',
     'GeckoCustom.cpp',
     'HTMLTableAccessibleWrap.cpp',
-    'HTMLWin32ObjectAccessible.cpp',
     'HyperTextAccessibleWrap.cpp',
     'ImageAccessibleWrap.cpp',
     'IUnknownImpl.cpp',
     'MsaaIdGenerator.cpp',
     'nsWinUtils.cpp',
     'Platform.cpp',
     'RootAccessibleWrap.cpp',
     'TextLeafAccessibleWrap.cpp',
