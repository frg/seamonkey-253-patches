# HG changeset patch
# User Sylvestre Ledru <sledru@mozilla.com>
# Date 1507230199 -7200
# Node ID a701f7c8334f814bf16c0b2eadc74b51d81d90be
# Parent  144a1efbfe7420bcbaec753fb262bbec7f471c3f
Bug 1406140 - build-clang.py: flake8 consistency r=andi

MozReview-Commit-ID: 6w1QesvVyyc

diff --git a/build/build-clang/build-clang.py b/build/build-clang/build-clang.py
--- a/build/build-clang/build-clang.py
+++ b/build/build-clang/build-clang.py
@@ -5,17 +5,16 @@
 
 import os
 import os.path
 import shutil
 import subprocess
 import platform
 import json
 import argparse
-import tempfile
 import glob
 import errno
 import re
 from contextlib import contextmanager
 import sys
 import which
 from distutils.dir_util import copy_tree
 
@@ -78,16 +77,17 @@ def updated_env(env):
 
 
 def build_tar_package(tar, name, base, directory):
     name = os.path.realpath(name)
     # On Windows, we have to convert this into an msys path so that tar can
     # understand it.
     if is_windows():
         name = name.replace('\\', '/')
+
         def f(match):
             return '/' + match.group(1).lower()
         name = re.sub(r'^([A-Za-z]):', f, name)
     run_in(base, [tar,
                   "-c",
                   "-%s" % ("J" if ".xz" in name else "j"),
                   "-f",
                   name, directory])
@@ -190,17 +190,17 @@ def build_one_stage(cc, cxx, asm, ld, ar
                   "-DCMAKE_SHARED_LINKER_FLAGS=%s" % ' '.join(ld[1:]),
                   "-DCMAKE_BUILD_TYPE=%s" % build_type,
                   "-DLLVM_TARGETS_TO_BUILD=X86;ARM",
                   "-DLLVM_ENABLE_ASSERTIONS=%s" % ("ON" if assertions else "OFF"),
                   "-DPYTHON_EXECUTABLE=%s" % slashify_path(python_path),
                   "-DCMAKE_INSTALL_PREFIX=%s" % inst_dir,
                   "-DLLVM_TOOL_LIBCXX_BUILD=%s" % ("ON" if build_libcxx else "OFF"),
                   "-DLIBCXX_LIBCPPABI_VERSION=\"\"",
-                  src_dir];
+                  src_dir]
     if is_windows():
         cmake_args.insert(-1, "-DLLVM_EXPORT_SYMBOLS_FOR_PLUGINS=ON")
         cmake_args.insert(-1, "-DLLVM_USE_CRT_RELEASE=MT")
     if ranlib is not None:
         cmake_args += ["-DCMAKE_RANLIB=%s" % slashify_path(ranlib)]
     if libtool is not None:
         cmake_args += ["-DCMAKE_LIBTOOL=%s" % slashify_path(libtool)]
     if osx_cross_compile:
@@ -221,16 +221,17 @@ def build_one_stage(cc, cxx, asm, ld, ar
 
     if is_linux():
         install_libgcc(gcc_dir, inst_dir)
     # For some reasons the import library clang.lib of clang.exe is not
     # installed, so we copy it by ourselves.
     if is_windows():
         install_import_library(build_dir, inst_dir)
 
+
 # Return the absolute path of a build tool.  We first look to see if the
 # variable is defined in the config file, and if so we make sure it's an
 # absolute path to an existing tool, otherwise we look for a program in
 # $PATH named "key".
 #
 # This expects the name of the key in the config file to match the name of
 # the tool in the default toolchain on the system (for example, "ld" on Unix
 # and "link" on Windows).
@@ -272,17 +273,17 @@ def get_tool(config, key):
 #       run-clang-tidy.py
 def prune_final_dir_for_clang_tidy(final_dir):
     # Make sure we only have what we expect.
     dirs = ("bin", "include", "lib", "libexec", "msbuild-bin", "share", "tools")
     for f in glob.glob("%s/*" % final_dir):
         if os.path.basename(f) not in dirs:
             raise Exception("Found unknown file %s in the final directory" % f)
         if not os.path.isdir(f):
-            raise Exception("Expected %s to be a directory" %f)
+            raise Exception("Expected %s to be a directory" % f)
 
     # In bin/, only keep clang-tidy and clang-apply-replacements. The last one
     # is used to auto-fix some of the issues detected by clang-tidy.
     re_clang_tidy = re.compile(
         r"^clang-(tidy|apply-replacements)(\.exe)?$", re.I)
     for f in glob.glob("%s/bin/*" % final_dir):
         if re_clang_tidy.search(os.path.basename(f)) is None:
             delete(f)
@@ -462,17 +463,17 @@ if __name__ == "__main__":
                 (source_dir + "/libcxx",
                  llvm_source_dir + "/projects/libcxx"),
                 (source_dir + "/libcxxabi",
                  llvm_source_dir + "/projects/libcxxabi")]
     for l in symlinks:
         # On Windows, we have to re-copy the whole directory every time.
         if not is_windows() and os.path.islink(l[1]):
             continue
-        delete(l[1]);
+        delete(l[1])
         if os.path.exists(l[0]):
             symlink(l[0], l[1])
 
     if build_clang_tidy:
         import_clang_tidy(llvm_source_dir)
 
     if not os.path.exists(build_dir):
         os.makedirs(build_dir)
@@ -495,18 +496,18 @@ if __name__ == "__main__":
     elif is_linux():
         extra_cflags = ["-static-libgcc"]
         extra_cxxflags = ["-static-libgcc", "-static-libstdc++"]
         extra_cflags2 = ["-fPIC"]
         extra_cxxflags2 = ["-fPIC", "-static-libstdc++"]
         extra_asmflags = []
         extra_ldflags = []
 
-        if os.environ.has_key('LD_LIBRARY_PATH'):
-            os.environ['LD_LIBRARY_PATH'] = '%s/lib64/:%s' % (gcc_dir, os.environ['LD_LIBRARY_PATH']);
+        if 'LD_LIBRARY_PATH' in os.environ:
+            os.environ['LD_LIBRARY_PATH'] = '%s/lib64/:%s' % (gcc_dir, os.environ['LD_LIBRARY_PATH'])
         else:
             os.environ['LD_LIBRARY_PATH'] = '%s/lib64/' % gcc_dir
     elif is_windows():
         extra_cflags = []
         extra_cxxflags = []
         # clang-cl would like to figure out what it's supposed to be emulating
         # by looking at an MSVC install, but we don't really have that here.
         # Force things on.
@@ -518,17 +519,17 @@ if __name__ == "__main__":
     if osx_cross_compile:
         # undo the damage done in the is_linux() block above, and also simulate
         # the is_darwin() block above.
         extra_cflags = []
         extra_cxxflags = ["-stdlib=libc++"]
         extra_cxxflags2 = ["-stdlib=libc++"]
 
         extra_flags = ["-target", "x86_64-apple-darwin11", "-mlinker-version=137",
-                       "-B", "%s/bin" %  os.getenv("CROSS_CCTOOLS_PATH"),
+                       "-B", "%s/bin" % os.getenv("CROSS_CCTOOLS_PATH"),
                        "-isysroot", os.getenv("CROSS_SYSROOT"),
                        # technically the sysroot flag there should be enough to deduce this,
                        # but clang needs some help to figure this out.
                        "-I%s/usr/include" % os.getenv("CROSS_SYSROOT"),
                        "-iframework", "%s/System/Library/Frameworks" % os.getenv("CROSS_SYSROOT")]
         extra_cflags += extra_flags
         extra_cxxflags += extra_flags
         extra_cflags2 += extra_flags
