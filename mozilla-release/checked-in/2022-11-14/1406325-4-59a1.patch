# HG changeset patch
# User John Dai <jdai@mozilla.com>
# Date 1510658580 -28800
#      Tue Nov 14 19:23:00 2017 +0800
# Node ID af93cbd1ebfd1a67ba13b1beace7bda6b7be09a3
# Parent  722f9a0b836127d4b643445fce8774238c2e6560
Bug 1406325 - Part 4: Use mType for LookupCustomElementDefinition and also removing parts of v0. f=echen, r=smaug

diff --git a/dom/base/CustomElementRegistry.cpp b/dom/base/CustomElementRegistry.cpp
--- a/dom/base/CustomElementRegistry.cpp
+++ b/dom/base/CustomElementRegistry.cpp
@@ -33,26 +33,24 @@ CustomElementCallback::Call()
       // this now in order to enqueue the connected callback. This is a spec
       // bug (w3c bug 27437).
       mOwnerData->mCreatedCallbackInvoked = true;
 
       // If ELEMENT is connected, enqueue connected callback for ELEMENT.
       nsIDocument* document = mThisObject->GetComposedDoc();
       if (document) {
         NodeInfo* ni = mThisObject->NodeInfo();
-        nsDependentAtomString extType(mOwnerData->mType);
-
         // We need to do this because at this point, CustomElementDefinition is
         // not set to CustomElementData yet, so EnqueueLifecycleCallback will
         // fail to find the CE definition for this custom element.
         // This will go away eventually since there is no created callback in v1.
         CustomElementDefinition* definition =
           nsContentUtils::LookupCustomElementDefinition(document,
             ni->LocalName(), ni->NamespaceID(),
-            extType.IsEmpty() ? nullptr : &extType);
+            mOwnerData->GetCustomElementType());
 
         nsContentUtils::EnqueueLifecycleCallback(
           nsIDocument::eConnected, mThisObject, nullptr, nullptr, definition);
       }
 
       static_cast<LifecycleCreatedCallback *>(mCallback.get())->Call(mThisObject, rv);
       mOwnerData->mElementIsBeingCreated = false;
       break;
@@ -127,20 +125,20 @@ CustomElementConstructor::Construct(cons
 // CustomElementData
 
 CustomElementData::CustomElementData(nsIAtom* aType)
   : CustomElementData(aType, CustomElementData::State::eUndefined)
 {
 }
 
 CustomElementData::CustomElementData(nsIAtom* aType, State aState)
-  : mType(aType)
-  , mElementIsBeingCreated(false)
+  : mElementIsBeingCreated(false)
   , mCreatedCallbackInvoked(true)
   , mState(aState)
+  , mType(aType)
 {
 }
 
 void
 CustomElementData::SetCustomElementDefinition(CustomElementDefinition* aDefinition)
 {
   MOZ_ASSERT(mState == State::eCustom);
   MOZ_ASSERT(!mCustomElementDefinition);
@@ -153,16 +151,22 @@ CustomElementDefinition*
 CustomElementData::GetCustomElementDefinition()
 {
   MOZ_ASSERT(mCustomElementDefinition ? mState == State::eCustom
                                       : mState != State::eCustom);
 
   return mCustomElementDefinition;
 }
 
+nsIAtom*
+CustomElementData::GetCustomElementType()
+{
+  return mType;
+}
+
 void
 CustomElementData::Traverse(nsCycleCollectionTraversalCallback& aCb) const
 {
   for (uint32_t i = 0; i < mReactionQueue.Length(); i++) {
     if (mReactionQueue[i]) {
       mReactionQueue[i]->Traverse(aCb);
     }
   }
@@ -264,22 +268,20 @@ CustomElementRegistry::CustomElementRegi
 
 CustomElementRegistry::~CustomElementRegistry()
 {
   mozilla::DropJSObjects(this);
 }
 
 CustomElementDefinition*
 CustomElementRegistry::LookupCustomElementDefinition(const nsAString& aLocalName,
-                                                     const nsAString* aIs) const
+                                                     nsIAtom* aTypeAtom) const
 {
   nsCOMPtr<nsIAtom> localNameAtom = NS_Atomize(aLocalName);
-  nsCOMPtr<nsIAtom> typeAtom = aIs ? NS_Atomize(*aIs) : localNameAtom;
-
-  CustomElementDefinition* data = mCustomDefinitions.GetWeak(typeAtom);
+  CustomElementDefinition* data = mCustomDefinitions.GetWeak(aTypeAtom);
   if (data && data->mLocalName == localNameAtom) {
     return data;
   }
 
   return nullptr;
 }
 
 CustomElementDefinition*
@@ -317,60 +319,16 @@ CustomElementRegistry::RegisterUnresolve
   }
 
   nsTArray<nsWeakPtr>* unresolved = mCandidatesMap.LookupOrAdd(typeName);
   nsWeakPtr* elem = unresolved->AppendElement();
   *elem = do_GetWeakReference(aElement);
   aElement->AddStates(NS_EVENT_STATE_UNRESOLVED);
 }
 
-void
-CustomElementRegistry::SetupCustomElement(Element* aElement,
-                                          const nsAString* aTypeExtension)
-{
-  nsCOMPtr<nsIAtom> tagAtom = aElement->NodeInfo()->NameAtom();
-  nsCOMPtr<nsIAtom> typeAtom = aTypeExtension ?
-    NS_Atomize(*aTypeExtension) : tagAtom;
-
-  if (aTypeExtension && !aElement->HasAttr(kNameSpaceID_None, nsGkAtoms::is)) {
-    // Custom element setup in the parser happens after the "is"
-    // attribute is added.
-    aElement->SetAttr(kNameSpaceID_None, nsGkAtoms::is, *aTypeExtension, true);
-  }
-
-  // SetupCustomElement() should be called with an element that don't have
-  // CustomElementData setup, if not we will hit the assertion in
-  // SetCustomElementData().
-  aElement->SetCustomElementData(new CustomElementData(typeAtom));
-
-  CustomElementDefinition* definition = LookupCustomElementDefinition(
-    aElement->NodeInfo()->LocalName(), aTypeExtension);
-
-  if (!definition) {
-    // The type extension doesn't exist in the registry,
-    // thus we don't need to enqueue callback or adjust
-    // the "is" attribute, but it is possibly an upgrade candidate.
-    RegisterUnresolvedElement(aElement, typeAtom);
-    return;
-  }
-
-  if (definition->mLocalName != tagAtom) {
-    // The element doesn't match the local name for the
-    // definition, thus the element isn't a custom element
-    // and we don't need to do anything more.
-    return;
-  }
-
-  // Enqueuing the created callback will set the CustomElementData on the
-  // element, causing prototype swizzling to occur in Element::WrapObject.
-  // We make it synchronously for createElement/createElementNS in order to
-  // pass tests. It'll be removed when we deprecate custom elements v0.
-  SyncInvokeReactions(nsIDocument::eCreated, aElement, definition);
-}
-
 /* static */ UniquePtr<CustomElementCallback>
 CustomElementRegistry::CreateCustomElementCallback(
   nsIDocument::ElementCallbackType aType, Element* aCustomElement,
   LifecycleCallbackArgs* aArgs,
   LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
   CustomElementDefinition* aDefinition)
 {
   MOZ_ASSERT(aDefinition, "CustomElementDefinition should not be null");
diff --git a/dom/base/CustomElementRegistry.h b/dom/base/CustomElementRegistry.h
--- a/dom/base/CustomElementRegistry.h
+++ b/dom/base/CustomElementRegistry.h
@@ -109,19 +109,17 @@ struct CustomElementData
   enum class State {
     eUndefined,
     eFailed,
     eCustom
   };
 
   explicit CustomElementData(nsIAtom* aType);
   CustomElementData(nsIAtom* aType, State aState);
-  // Custom element type, for <button is="x-button"> or <x-button>
-  // this would be x-button.
-  nsCOMPtr<nsIAtom> mType;
+
   // Element is being created flag as described in the custom elements spec.
   bool mElementIsBeingCreated;
   // Flag to determine if the created callback has been invoked, thus it
   // determines if other callbacks can be enqueued.
   bool mCreatedCallbackInvoked;
   // Custom element state as described in the custom element spec.
   State mState;
   // custom element reaction queue as described in the custom element spec.
@@ -129,23 +127,27 @@ struct CustomElementData
   // 2) it’s adopted into a new document, 3) its attributes are changed,
   // appended, removed, or replaced.
   // There are 3 reactions in reaction queue when doing upgrade operation,
   // e.g., create an element, insert a node.
   AutoTArray<UniquePtr<CustomElementReaction>, 3> mReactionQueue;
 
   void SetCustomElementDefinition(CustomElementDefinition* aDefinition);
   CustomElementDefinition* GetCustomElementDefinition();
+  nsIAtom* GetCustomElementType();
 
   void Traverse(nsCycleCollectionTraversalCallback& aCb) const;
   void Unlink();
 
 private:
   virtual ~CustomElementData() {}
 
+  // Custom element type, for <button is="x-button"> or <x-button>
+  // this would be x-button.
+  nsCOMPtr<nsIAtom> mType;
   RefPtr<CustomElementDefinition> mCustomElementDefinition;
 };
 
 #define ALEADY_CONSTRUCTED_MARKER nullptr
 
 // The required information for a custom element as defined in:
 // https://html.spec.whatwg.org/multipage/scripting.html#custom-element-definition
 struct CustomElementDefinition
@@ -364,28 +366,21 @@ public:
 
   explicit CustomElementRegistry(nsPIDOMWindowInner* aWindow);
 
   /**
    * Looking up a custom element definition.
    * https://html.spec.whatwg.org/#look-up-a-custom-element-definition
    */
   CustomElementDefinition* LookupCustomElementDefinition(
-    const nsAString& aLocalName, const nsAString* aIs = nullptr) const;
+    const nsAString& aLocalName, nsIAtom* aTypeAtom) const;
 
   CustomElementDefinition* LookupCustomElementDefinition(
     JSContext* aCx, JSObject *aConstructor) const;
 
-  /**
-   * Enqueue created callback or register upgrade candidate for
-   * newly created custom elements, possibly extending an existing type.
-   * ex. <x-button>, <button is="x-button> (type extension)
-   */
-  void SetupCustomElement(Element* aElement, const nsAString* aTypeExtension);
-
   static void EnqueueLifecycleCallback(nsIDocument::ElementCallbackType aType,
                                        Element* aCustomElement,
                                        LifecycleCallbackArgs* aArgs,
                                        LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
                                        CustomElementDefinition* aDefinition);
 
   void GetCustomPrototype(nsIAtom* aAtom,
                           JS::MutableHandle<JSObject*> aPrototype);
diff --git a/dom/base/Element.cpp b/dom/base/Element.cpp
--- a/dom/base/Element.cpp
+++ b/dom/base/Element.cpp
@@ -522,17 +522,17 @@ Element::WrapObject(JSContext *aCx, JS::
   JS::Rooted<JSObject*> customProto(aCx);
 
   if (!givenProto) {
     // Custom element prototype swizzling.
     CustomElementData* data = GetCustomElementData();
     if (data) {
       // If this is a registered custom element then fix the prototype.
       nsContentUtils::GetCustomPrototype(OwnerDoc(), NodeInfo()->NamespaceID(),
-                                         data->mType, &customProto);
+                                         data->GetCustomElementType(), &customProto);
       if (customProto &&
           NodePrincipal()->SubsumesConsideringDomain(nsContentUtils::ObjectPrincipal(customProto))) {
         // Just go ahead and create with the right proto up front.  Set
         // customProto to null to flag that we don't need to do any post-facto
         // proto fixups here.
         givenProto = customProto;
         customProto = nullptr;
       }
@@ -2810,17 +2810,17 @@ Element::SetAttrAndNotify(int32_t aNames
       binding->AttributeChanged(aName, aNamespaceID, false, aNotify);
     }
   }
 
   if (CustomElementRegistry::IsCustomElementEnabled()) {
     if (CustomElementData* data = GetCustomElementData()) {
       if (CustomElementDefinition* definition =
             nsContentUtils::GetElementDefinitionIfObservingAttr(this,
-                                                                data->mType,
+                                                                data->GetCustomElementType(),
                                                                 aName)) {
         MOZ_ASSERT(data->mState == CustomElementData::State::eCustom,
                    "AttributeChanged callback should fire only if "
                    "custom element state is custom");
         nsCOMPtr<nsIAtom> oldValueAtom;
         if (oldValue) {
           oldValueAtom = oldValue->GetAsAtom();
         } else {
@@ -3117,17 +3117,17 @@ Element::UnsetAttr(int32_t aNameSpaceID,
       binding->AttributeChanged(aName, aNameSpaceID, true, aNotify);
     }
   }
 
   if (CustomElementRegistry::IsCustomElementEnabled()) {
     if (CustomElementData* data = GetCustomElementData()) {
       if (CustomElementDefinition* definition =
             nsContentUtils::GetElementDefinitionIfObservingAttr(this,
-                                                                data->mType,
+                                                                data->GetCustomElementType(),
                                                                 aName)) {
         MOZ_ASSERT(data->mState == CustomElementData::State::eCustom,
                    "AttributeChanged callback should fire only if "
                    "custom element state is custom");
         nsAutoString ns;
         nsContentUtils::NameSpaceManager()->GetNameSpaceURI(aNameSpaceID, ns);
 
         nsCOMPtr<nsIAtom> oldValueAtom = oldValue.GetAsAtom();
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -10123,17 +10123,17 @@ nsContentUtils::HttpsStateIsModern(nsIDo
 
   return false;
 }
 
 /* static */ CustomElementDefinition*
 nsContentUtils::LookupCustomElementDefinition(nsIDocument* aDoc,
                                               const nsAString& aLocalName,
                                               uint32_t aNameSpaceID,
-                                              const nsAString* aIs)
+                                              nsIAtom* aTypeAtom)
 {
   MOZ_ASSERT(aDoc);
 
   if (aNameSpaceID != kNameSpaceID_XHTML ||
       !aDoc->GetDocShell()) {
     return nullptr;
   }
 
@@ -10142,47 +10142,17 @@ nsContentUtils::LookupCustomElementDefin
     return nullptr;
   }
 
   RefPtr<CustomElementRegistry> registry(window->CustomElements());
   if (!registry) {
     return nullptr;
   }
 
-  return registry->LookupCustomElementDefinition(aLocalName, aIs);
-}
-
-/* static */ void
-nsContentUtils::SetupCustomElement(Element* aElement,
-                                   const nsAString* aTypeExtension)
-{
-  MOZ_ASSERT(aElement);
-
-  nsCOMPtr<nsIDocument> doc = aElement->OwnerDoc();
-
-  if (!doc) {
-    return;
-  }
-
-  if (aElement->GetNameSpaceID() != kNameSpaceID_XHTML ||
-      !doc->GetDocShell()) {
-    return;
-  }
-
-  nsCOMPtr<nsPIDOMWindowInner> window(doc->GetInnerWindow());
-  if (!window) {
-    return;
-  }
-
-  RefPtr<CustomElementRegistry> registry(window->CustomElements());
-  if (!registry) {
-    return;
-  }
-
-  return registry->SetupCustomElement(aElement, aTypeExtension);
+  return registry->LookupCustomElementDefinition(aLocalName, aTypeAtom);
 }
 
 /* static */ CustomElementDefinition*
 nsContentUtils::GetElementDefinitionIfObservingAttr(Element* aCustomElement,
                                                     nsIAtom* aExtensionType,
                                                     nsIAtom* aAttrName)
 {
   CustomElementDefinition* definition =
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -3024,20 +3024,18 @@ public:
   /**
    * Looking up a custom element definition.
    * https://html.spec.whatwg.org/#look-up-a-custom-element-definition
    */
   static mozilla::dom::CustomElementDefinition*
     LookupCustomElementDefinition(nsIDocument* aDoc,
                                   const nsAString& aLocalName,
                                   uint32_t aNameSpaceID,
-                                  const nsAString* aIs = nullptr);
-
-  static void SetupCustomElement(Element* aElement,
-                                 const nsAString* aTypeExtension = nullptr);
+                                  nsIAtom* aTypeAtom);
+
 
   static mozilla::dom::CustomElementDefinition*
   GetElementDefinitionIfObservingAttr(Element* aCustomElement,
                                       nsIAtom* aExtensionType,
                                       nsIAtom* aAttrName);
 
   static void SyncInvokeReactions(nsIDocument::ElementCallbackType aType,
                                   Element* aCustomElement,
diff --git a/dom/base/nsNodeUtils.cpp b/dom/base/nsNodeUtils.cpp
--- a/dom/base/nsNodeUtils.cpp
+++ b/dom/base/nsNodeUtils.cpp
@@ -480,17 +480,17 @@ nsNodeUtils::CloneAndAdopt(nsINode *aNod
 
       if (data || !extension.IsEmpty()) {
         nsCOMPtr<nsIAtom> typeAtom = extension.IsEmpty() ? tagAtom : NS_Atomize(extension);
         cloneElem->SetCustomElementData(new CustomElementData(typeAtom));
         CustomElementDefinition* definition =
           nsContentUtils::LookupCustomElementDefinition(nodeInfo->GetDocument(),
                                                         nodeInfo->LocalName(),
                                                         nodeInfo->NamespaceID(),
-                                                        extension.IsEmpty() ? nullptr : &extension);
+                                                        typeAtom);
         if (definition) {
           nsContentUtils::EnqueueUpgradeReaction(cloneElem, definition);
         }
       }
     }
 
     if (aParent) {
       // If we're cloning we need to insert the cloned children into the cloned
diff --git a/dom/html/nsHTMLContentSink.cpp b/dom/html/nsHTMLContentSink.cpp
--- a/dom/html/nsHTMLContentSink.cpp
+++ b/dom/html/nsHTMLContentSink.cpp
@@ -255,33 +255,35 @@ NS_NewHTMLElement(Element** aResult, alr
                   FromParser aFromParser, const nsAString* aIs,
                   mozilla::dom::CustomElementDefinition* aDefinition)
 {
   *aResult = nullptr;
 
   RefPtr<mozilla::dom::NodeInfo> nodeInfo = aNodeInfo;
 
   nsIAtom *name = nodeInfo->NameAtom();
+  nsCOMPtr<nsIAtom> tagAtom = nodeInfo->NameAtom();
+  nsCOMPtr<nsIAtom> typeAtom = aIs ? NS_Atomize(*aIs) : tagAtom;
 
   NS_ASSERTION(nodeInfo->NamespaceEquals(kNameSpaceID_XHTML),
                "Trying to HTML elements that don't have the XHTML namespace");
 
   int32_t tag = nsHTMLTags::CaseSensitiveAtomTagToId(name);
 
   // https://dom.spec.whatwg.org/#concept-create-element
   // We only handle the "synchronous custom elements flag is set" now.
   // For the unset case (e.g. cloning a node), see bug 1319342 for that.
   // Step 4.
   CustomElementDefinition* definition = aDefinition;
   if (!definition && CustomElementRegistry::IsCustomElementEnabled()) {
     definition =
       nsContentUtils::LookupCustomElementDefinition(nodeInfo->GetDocument(),
                                                     nodeInfo->LocalName(),
                                                     nodeInfo->NamespaceID(),
-                                                    aIs);
+                                                    typeAtom);
   }
 
   // It might be a problem that parser synchronously calls constructor, so filed
   // bug 1378079 to figure out what we should do for parser case.
   if (definition) {
     /*
      * Synchronous custom elements flag is determined by 3 places in spec,
      * 1) create an element for a token, the flag is determined by
@@ -314,18 +316,16 @@ NS_NewHTMLElement(Element** aResult, alr
     JSContext* cx = aes.cx();
     ErrorResult rv;
 
     // Step 5.
     if (definition->IsCustomBuiltIn()) {
       // SetupCustomElement() should be called with an element that don't have
       // CustomElementData setup, if not we will hit the assertion in
       // SetCustomElementData().
-      nsCOMPtr<nsIAtom> tagAtom = nodeInfo->NameAtom();
-      nsCOMPtr<nsIAtom> typeAtom = aIs ? NS_Atomize(*aIs) : tagAtom;
       // Built-in element
       *aResult = CreateHTMLElement(tag, nodeInfo.forget(), aFromParser).take();
       (*aResult)->SetCustomElementData(new CustomElementData(typeAtom));
       if (synchronousCustomElements) {
         CustomElementRegistry::Upgrade(*aResult, definition, rv);
         if (rv.MaybeSetPendingException(cx)) {
           aes.ReportException();
         }
@@ -365,17 +365,17 @@ NS_NewHTMLElement(Element** aResult, alr
   }
 
   if (!*aResult) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
 
   if (CustomElementRegistry::IsCustomElementEnabled() &&
       (isCustomElementName || aIs)) {
-    nsContentUtils::SetupCustomElement(*aResult, aIs);
+    (*aResult)->SetCustomElementData(new CustomElementData(typeAtom));
   }
 
   return NS_OK;
 }
 
 already_AddRefed<nsGenericHTMLElement>
 CreateHTMLElement(uint32_t aNodeType,
                   already_AddRefed<mozilla::dom::NodeInfo>&& aNodeInfo,
diff --git a/parser/html/nsHtml5TreeOperation.cpp b/parser/html/nsHtml5TreeOperation.cpp
--- a/parser/html/nsHtml5TreeOperation.cpp
+++ b/parser/html/nsHtml5TreeOperation.cpp
@@ -425,19 +425,22 @@ nsHtml5TreeOperation::CreateHTMLElement(
       nsHtml5String is = aAttributes->getValue(nsHtml5AttributeName::ATTR_IS);
       if (is) {
         is.ToString(isValue);
       }
     }
 
     isCustomElement = (aCreator == NS_NewCustomElement || !isValue.IsEmpty());
     if (isCustomElement && aFromParser != dom::FROM_PARSER_FRAGMENT) {
+      nsCOMPtr<nsIAtom> tagAtom = nodeInfo->NameAtom();
+      nsCOMPtr<nsIAtom> typeAtom =
+        isValue.IsEmpty() ? tagAtom : NS_Atomize(isValue);
+
       definition = nsContentUtils::LookupCustomElementDefinition(document,
-        nodeInfo->LocalName(), nodeInfo->NamespaceID(),
-        (isValue.IsEmpty() ? nullptr : &isValue));
+        nodeInfo->LocalName(), nodeInfo->NamespaceID(), typeAtom);
 
       if (definition) {
         willExecuteScript = true;
       }
     }
   }
 
   if (willExecuteScript) { // This will cause custom element constructors to run
