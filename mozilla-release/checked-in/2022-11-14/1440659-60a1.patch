# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1519396344 0
#      Fri Feb 23 14:32:24 2018 +0000
# Node ID 78db99b1350b06b24a4db39500a2307bfd31d486
# Parent  a35f1b78fd2ac5743dc8242f13b137a2d590b490
Bug 1440659 - Update OTS library to latest upstream code (currently at f7238234966c466c5e3d11ee822e9760be0c527a, release 6.1.1 + recent changes from trunk). r=fredw

diff --git a/gfx/ots/README.mozilla b/gfx/ots/README.mozilla
--- a/gfx/ots/README.mozilla
+++ b/gfx/ots/README.mozilla
@@ -1,12 +1,12 @@
 This is the Sanitiser for OpenType project, from http://code.google.com/p/ots/.
 
 Our reference repository is https://github.com/khaledhosny/ots/.
 
-Current revision: f87b4556191e4132ef5c47365762eb88ace97fc3 (6.0.0)
+Current revision: f7238234966c466c5e3d11ee822e9760be0c527a (6.1.1)
 
 Upstream files included: LICENSE, src/, include/, tests/*.cc
 
 Additional files: README.mozilla, src/moz.build
 
 Additional patch: ots-visibility.patch (bug 711079).
 Additional patch: ots-lz4.patch
diff --git a/gfx/ots/include/opentype-sanitiser.h b/gfx/ots/include/opentype-sanitiser.h
--- a/gfx/ots/include/opentype-sanitiser.h
+++ b/gfx/ots/include/opentype-sanitiser.h
@@ -30,23 +30,27 @@
 typedef signed char int8_t;
 typedef unsigned char uint8_t;
 typedef short int16_t;
 typedef unsigned short uint16_t;
 typedef int int32_t;
 typedef unsigned int uint32_t;
 typedef __int64 int64_t;
 typedef unsigned __int64 uint64_t;
-#define ntohl(x) _byteswap_ulong (x)
-#define ntohs(x) _byteswap_ushort (x)
-#define htonl(x) _byteswap_ulong (x)
-#define htons(x) _byteswap_ushort (x)
+#define ots_ntohl(x) _byteswap_ulong (x)
+#define ots_ntohs(x) _byteswap_ushort (x)
+#define ots_htonl(x) _byteswap_ulong (x)
+#define ots_htons(x) _byteswap_ushort (x)
 #else
 #include <arpa/inet.h>
 #include <stdint.h>
+#define ots_ntohl(x) ntohl (x)
+#define ots_ntohs(x) ntohs (x)
+#define ots_htonl(x) htonl (x)
+#define ots_htons(x) htons (x)
 #endif
 
 #include <sys/types.h>
 
 #include <algorithm>
 #include <cassert>
 #include <cstddef>
 #include <cstring>
@@ -75,36 +79,36 @@ class OTSStream {
     const size_t orig_length = length;
     size_t offset = 0;
 
     size_t chksum_offset = Tell() & 3;
     if (chksum_offset) {
       const size_t l = std::min(length, static_cast<size_t>(4) - chksum_offset);
       uint32_t tmp = 0;
       std::memcpy(reinterpret_cast<uint8_t *>(&tmp) + chksum_offset, data, l);
-      chksum_ += ntohl(tmp);
+      chksum_ += ots_ntohl(tmp);
       length -= l;
       offset += l;
     }
 
     while (length >= 4) {
       uint32_t tmp;
       std::memcpy(&tmp, reinterpret_cast<const uint8_t *>(data) + offset,
         sizeof(uint32_t));
-      chksum_ += ntohl(tmp);
+      chksum_ += ots_ntohl(tmp);
       length -= 4;
       offset += 4;
     }
 
     if (length) {
       if (length > 4) return false;  // not reached
       uint32_t tmp = 0;
       std::memcpy(&tmp,
                   reinterpret_cast<const uint8_t*>(data) + offset, length);
-      chksum_ += ntohl(tmp);
+      chksum_ += ots_ntohl(tmp);
     }
 
     return WriteRaw(data, orig_length);
   }
 
   virtual bool Seek(off_t position) = 0;
   virtual off_t Tell() const = 0;
 
@@ -122,37 +126,37 @@ class OTSStream {
     return true;
   }
 
   bool WriteU8(uint8_t v) {
     return Write(&v, sizeof(v));
   }
 
   bool WriteU16(uint16_t v) {
-    v = htons(v);
+    v = ots_htons(v);
     return Write(&v, sizeof(v));
   }
 
   bool WriteS16(int16_t v) {
-    v = htons(v);
+    v = ots_htons(v);
     return Write(&v, sizeof(v));
   }
 
   bool WriteU24(uint32_t v) {
-    v = htonl(v);
+    v = ots_htonl(v);
     return Write(reinterpret_cast<uint8_t*>(&v)+1, 3);
   }
 
   bool WriteU32(uint32_t v) {
-    v = htonl(v);
+    v = ots_htonl(v);
     return Write(&v, sizeof(v));
   }
 
   bool WriteS32(int32_t v) {
-    v = htonl(v);
+    v = ots_htonl(v);
     return Write(&v, sizeof(v));
   }
 
   bool WriteR64(uint64_t v) {
     return Write(&v, sizeof(v));
   }
 
   void ResetChecksum() {
diff --git a/gfx/ots/src/cff.cc b/gfx/ots/src/cff.cc
--- a/gfx/ots/src/cff.cc
+++ b/gfx/ots/src/cff.cc
@@ -375,17 +375,17 @@ bool ParsePrivateDictData(
     operands.pop_back();
 
     switch (op) {
       // hints
       case 6:  // BlueValues
       case 7:  // OtherBlues
       case 8:  // FamilyBlues
       case 9:  // FamilyOtherBlues
-        if (operands.empty() || (operands.size() % 2) != 0) {
+        if ((operands.size() % 2) != 0) {
           return OTS_FAILURE();
         }
         break;
 
       // array
       case (12U << 8) + 12:  // StemSnapH (delta)
       case (12U << 8) + 13:  // StemSnapV (delta)
         if (operands.empty()) {
diff --git a/gfx/ots/src/cmap.cc b/gfx/ots/src/cmap.cc
--- a/gfx/ots/src/cmap.cc
+++ b/gfx/ots/src/cmap.cc
@@ -233,17 +233,17 @@ bool OpenTypeCMAP::ParseFormat4(int plat
                                          ranges[i].id_range_offset +
                                          range_delta * 2;
         // We need to be able to access a 16-bit value from this offset
         if (glyph_id_offset + 1 >= length) {
           return Error("bad glyph id offset (%d > %ld)", glyph_id_offset, length);
         }
         uint16_t glyph;
         std::memcpy(&glyph, data + glyph_id_offset, 2);
-        glyph = ntohs(glyph);
+        glyph = ots_ntohs(glyph);
         if (glyph >= num_glyphs) {
           return Error("Range glyph reference too high (%d > %d)", glyph, num_glyphs - 1);
         }
       }
     }
   }
 
   // We accept the table.
@@ -766,19 +766,20 @@ bool OpenTypeCMAP::Parse(const uint8_t *
         }
       } else if ((subtable_headers[i].encoding == 3) &&
                  (subtable_headers[i].format == 4)) {
         // parse and output the 0-3-4 table as 0-3-4 table.
         if (!ParseFormat4(0, 3, data + subtable_headers[i].offset,
                       subtable_headers[i].length, num_glyphs)) {
           return Error("Failed to parse format 4 cmap subtable %d", i);
         }
-      } else if ((subtable_headers[i].encoding == 3) &&
+      } else if ((subtable_headers[i].encoding == 3 ||
+                  subtable_headers[i].encoding == 4) &&
                  (subtable_headers[i].format == 12)) {
-        // parse and output the 0-3-12 table as 3-10-12 table.
+        // parse and output the 0-3-12 or 0-4-12 tables as 3-10-12 table.
         if (!Parse31012(data + subtable_headers[i].offset,
                         subtable_headers[i].length, num_glyphs)) {
           return Error("Failed to parse format 12 cmap subtable %d", i);
         }
       } else if ((subtable_headers[i].encoding == 5) &&
                  (subtable_headers[i].format == 14)) {
         if (!Parse0514(data + subtable_headers[i].offset,
                        subtable_headers[i].length, num_glyphs)) {
diff --git a/gfx/ots/src/gdef.cc b/gfx/ots/src/gdef.cc
--- a/gfx/ots/src/gdef.cc
+++ b/gfx/ots/src/gdef.cc
@@ -231,53 +231,53 @@ bool OpenTypeGDEF::Parse(const uint8_t *
   uint32_t version = 0;
   if (!table.ReadU32(&version)) {
     return Error("Incomplete table");
   }
   if (version < 0x00010000 || version == 0x00010001) {
     return Error("Bad version");
   }
 
+  bool version_2 = false;
   if (version >= 0x00010002) {
-    this->version_2 = true;
+    version_2 = true;
   }
 
   uint16_t offset_glyph_class_def = 0;
   uint16_t offset_attach_list = 0;
   uint16_t offset_lig_caret_list = 0;
   uint16_t offset_mark_attach_class_def = 0;
   if (!table.ReadU16(&offset_glyph_class_def) ||
       !table.ReadU16(&offset_attach_list) ||
       !table.ReadU16(&offset_lig_caret_list) ||
       !table.ReadU16(&offset_mark_attach_class_def)) {
     return Error("Incomplete table");
   }
   uint16_t offset_mark_glyph_sets_def = 0;
-  if (this->version_2) {
+  if (version_2) {
     if (!table.ReadU16(&offset_mark_glyph_sets_def)) {
       return Error("Incomplete table");
     }
   }
 
   unsigned gdef_header_end = 4 + 4 * 2;
-  if (this->version_2)
+  if (version_2)
     gdef_header_end += 2;
 
   // Parse subtables
   if (offset_glyph_class_def) {
     if (offset_glyph_class_def >= length ||
         offset_glyph_class_def < gdef_header_end) {
       return Error("Invalid offset to glyph classes");
     }
     if (!ots::ParseClassDefTable(GetFont(), data + offset_glyph_class_def,
                                  length - offset_glyph_class_def,
                                  this->m_num_glyphs, kMaxGlyphClassDefValue)) {
       return Error("Invalid glyph classes");
     }
-    this->has_glyph_class_def = true;
   }
 
   if (offset_attach_list) {
     if (offset_attach_list >= length ||
         offset_attach_list < gdef_header_end) {
       return Error("Invalid offset to attachment list");
     }
     if (!ParseAttachListTable(data + offset_attach_list,
@@ -303,29 +303,27 @@ bool OpenTypeGDEF::Parse(const uint8_t *
       return Error("Invalid offset to mark attachment list");
     }
     if (!ots::ParseClassDefTable(GetFont(),
                                  data + offset_mark_attach_class_def,
                                  length - offset_mark_attach_class_def,
                                  this->m_num_glyphs, kMaxClassDefValue)) {
       return Error("Invalid mark attachment list");
     }
-    this->has_mark_attachment_class_def = true;
   }
 
   if (offset_mark_glyph_sets_def) {
     if (offset_mark_glyph_sets_def >= length ||
         offset_mark_glyph_sets_def < gdef_header_end) {
       return Error("invalid offset to mark glyph sets");
     }
     if (!ParseMarkGlyphSetsDefTable(data + offset_mark_glyph_sets_def,
                                     length - offset_mark_glyph_sets_def)) {
       return Error("Invalid mark glyph sets");
     }
-    this->has_mark_glyph_sets_def = true;
   }
   this->m_data = data;
   this->m_length = length;
   return true;
 }
 
 bool OpenTypeGDEF::Serialize(OTSStream *out) {
   if (!out->Write(this->m_data, this->m_length)) {
diff --git a/gfx/ots/src/gdef.h b/gfx/ots/src/gdef.h
--- a/gfx/ots/src/gdef.h
+++ b/gfx/ots/src/gdef.h
@@ -8,33 +8,25 @@
 #include "ots.h"
 
 namespace ots {
 
 class OpenTypeGDEF : public Table {
  public:
   explicit OpenTypeGDEF(Font *font, uint32_t tag)
       : Table(font, tag, tag),
-        version_2(false),
-        has_glyph_class_def(false),
-        has_mark_attachment_class_def(false),
-        has_mark_glyph_sets_def(false),
         num_mark_glyph_sets(0),
         m_data(NULL),
         m_length(0),
         m_num_glyphs(0) {
   }
 
   bool Parse(const uint8_t *data, size_t length);
   bool Serialize(OTSStream *out);
 
-  bool version_2;
-  bool has_glyph_class_def;
-  bool has_mark_attachment_class_def;
-  bool has_mark_glyph_sets_def;
   uint16_t num_mark_glyph_sets;
 
  private:
   bool ParseAttachListTable(const uint8_t *data, size_t length);
   bool ParseLigCaretListTable(const uint8_t *data, size_t length);
   bool ParseMarkGlyphSetsDefTable(const uint8_t *data, size_t length);
 
   const uint8_t *m_data;
diff --git a/gfx/ots/src/glyf.cc b/gfx/ots/src/glyf.cc
--- a/gfx/ots/src/glyf.cc
+++ b/gfx/ots/src/glyf.cc
@@ -33,16 +33,26 @@ bool OpenTypeGLYF::ParseFlagsForSimpleGl
   }
 
   if (flag & (1u << 2)) {  // y-Short
     ++delta;
   } else if (!(flag & (1u << 5))) {
     delta += 2;
   }
 
+  /* MS and Apple specs say this bit is reserved and must be set to zero, but
+   * Apple spec then contradicts itself and says it should be set on the first
+   * contour flag for simple glyphs with overlapping contours:
+   * https://developer.apple.com/fonts/TrueType-Reference-Manual/RM06/Chap6AATIntro.html
+   * (“Overlapping contours” section) */
+  if (flag & (1u << 6) && *flag_index != 0) {
+    return Error("Bad glyph flag (%d), "
+                 "bit 6 must be set to zero for flag %d", flag, *flag_index);
+  }
+
   if (flag & (1u << 3)) {  // repeat
     if (*flag_index + 1 >= num_flags) {
       return Error("Count too high (%d + 1 >= %d)", *flag_index, num_flags);
     }
     uint8_t repeat = 0;
     if (!glyph.ReadU8(&repeat)) {
       return Error("Can't read repeat value");
     }
@@ -52,18 +62,18 @@ bool OpenTypeGLYF::ParseFlagsForSimpleGl
     delta += (delta * repeat);
 
     *flag_index += repeat;
     if (*flag_index >= num_flags) {
       return Error("Count too high (%d >= %d)", *flag_index, num_flags);
     }
   }
 
-  if ((flag & (1u << 6)) || (flag & (1u << 7))) {  // reserved flags
-    return Error("Bad glyph flag value (%d), reserved flags must be set to zero", flag);
+  if (flag & (1u << 7)) {  // reserved flag
+    return Error("Bad glyph flag (%d), reserved bit 7 must be set to zero", flag);
   }
 
   *coordinates_length += delta;
   if (glyph.length() < *coordinates_length) {
     return Error("Glyph coordinates length bigger than glyph length (%d > %d)",
                  *coordinates_length, glyph.length());
   }
 
diff --git a/gfx/ots/src/layout.cc b/gfx/ots/src/layout.cc
--- a/gfx/ots/src/layout.cc
+++ b/gfx/ots/src/layout.cc
@@ -17,20 +17,16 @@
 namespace {
 
 // The 'DFLT' tag of script table.
 const uint32_t kScriptTableTagDflt = 0x44464c54;
 // The value which represents there is no required feature index.
 const uint16_t kNoRequiredFeatureIndexDefined = 0xFFFF;
 // The lookup flag bit which indicates existence of MarkFilteringSet.
 const uint16_t kUseMarkFilteringSetBit = 0x0010;
-// The lookup flags which require GDEF table.
-const uint16_t kGdefRequiredFlags = 0x0002 | 0x0004 | 0x0008;
-// The mask for MarkAttachmentType.
-const uint16_t kMarkAttachmentTypeMask = 0xFF00;
 // The maximum type number of format for device tables.
 const uint16_t kMaxDeltaFormatType = 3;
 // The maximum number of class value.
 const uint16_t kMaxClassDefValue = 0xFFFF;
 
 struct ScriptRecord {
   uint32_t tag;
   uint16_t offset;
@@ -189,40 +185,17 @@ bool ParseLookupTable(ots::Font *font, c
       !subtable.ReadU16(&subtable_count)) {
     return OTS_FAILURE_MSG("Failed to read lookup table header");
   }
 
   if (lookup_type == 0 || lookup_type > parser->num_types) {
     return OTS_FAILURE_MSG("Bad lookup type %d", lookup_type);
   }
 
-  ots::OpenTypeGDEF *gdef = static_cast<ots::OpenTypeGDEF*>(
-      font->GetTypedTable(OTS_TAG_GDEF));
-
-  // Check lookup flags.
-  if ((lookup_flag & kGdefRequiredFlags) &&
-      (!gdef || !gdef->has_glyph_class_def)) {
-    return OTS_FAILURE_MSG("Lookup flags require GDEF table, "
-                           "but none was found: %d", lookup_flag);
-  }
-  if ((lookup_flag & kMarkAttachmentTypeMask) &&
-      (!gdef || !gdef->has_mark_attachment_class_def)) {
-    return OTS_FAILURE_MSG("Lookup flags ask for mark attachment, "
-                           "but there is no GDEF table or it has no "
-                           "mark attachment classes: %d", lookup_flag);
-  }
-  bool use_mark_filtering_set = false;
-  if (lookup_flag & kUseMarkFilteringSetBit) {
-    if (!gdef || !gdef->has_mark_glyph_sets_def) {
-      return OTS_FAILURE_MSG("Lookup flags ask for mark filtering, "
-                             "but there is no GDEF table or it has no "
-                             "mark filtering sets: %d", lookup_flag);
-    }
-    use_mark_filtering_set = true;
-  }
+  bool use_mark_filtering_set = lookup_flag & kUseMarkFilteringSetBit;
 
   std::vector<uint16_t> subtables;
   subtables.reserve(subtable_count);
   // If the |kUseMarkFilteringSetBit| of |lookup_flag| is set,
   // extra 2 bytes will follow after subtable offset array.
   const unsigned lookup_table_end = 2 * static_cast<unsigned>(subtable_count) +
       (use_mark_filtering_set ? 8 : 6);
   if (lookup_table_end > std::numeric_limits<uint16_t>::max()) {
@@ -243,18 +216,22 @@ bool ParseLookupTable(ots::Font *font, c
     return OTS_FAILURE_MSG("Bad subtable size %ld", subtables.size());
   }
 
   if (use_mark_filtering_set) {
     uint16_t mark_filtering_set = 0;
     if (!subtable.ReadU16(&mark_filtering_set)) {
       return OTS_FAILURE_MSG("Failed to read mark filtering set");
     }
-    if (gdef->num_mark_glyph_sets == 0 ||
-        mark_filtering_set >= gdef->num_mark_glyph_sets) {
+
+    ots::OpenTypeGDEF *gdef = static_cast<ots::OpenTypeGDEF*>(
+        font->GetTypedTable(OTS_TAG_GDEF));
+
+    if (gdef && (gdef->num_mark_glyph_sets == 0 ||
+        mark_filtering_set >= gdef->num_mark_glyph_sets)) {
       return OTS_FAILURE_MSG("Bad mark filtering set %d", mark_filtering_set);
     }
   }
 
   // Parse lookup subtables for this lookup type.
   for (unsigned i = 0; i < subtable_count; ++i) {
     if (!parser->Parse(font, data + subtables[i], length - subtables[i],
                        lookup_type)) {
diff --git a/gfx/ots/src/ots.cc b/gfx/ots/src/ots.cc
--- a/gfx/ots/src/ots.cc
+++ b/gfx/ots/src/ots.cc
@@ -12,17 +12,17 @@
 #include <cstring>
 #include <limits>
 #include <map>
 #include <vector>
 
 #include "woff2/decode.h"
 
 // The OpenType Font File
-// http://www.microsoft.com/typography/otspec/cmap.htm
+// http://www.microsoft.com/typography/otspec/otff.htm
 
 #include "cff.h"
 #include "cmap.h"
 #include "cvt.h"
 #include "fpgm.h"
 #include "gasp.h"
 #include "gdef.h"
 #include "glyf.h"
@@ -681,17 +681,17 @@ bool ProcessGeneric(ots::FontFile *heade
   for (const auto &table_entry : tables) {
     if (!font->GetTable(table_entry.tag)) {
       if (!font->ParseTable(table_entry, data, arena)) {
         return OTS_FAILURE_MSG_TAG("Failed to parse table", table_entry.tag);
       }
     }
   }
 
-  if (font->GetTable(OTS_TAG_CFF)) {
+  if (font->GetTable(OTS_TAG_CFF) || font->GetTable(OTS_TAG('C', 'F', 'F', '2'))) {
     // font with PostScript glyph
     if (font->version != OTS_TAG('O','T','T','O')) {
       return OTS_FAILURE_MSG_HDR("wrong font version for PostScript glyph data");
     }
     if (font->GetTable(OTS_TAG_GLYF) || font->GetTable(OTS_TAG_LOCA)) {
       // mixing outline formats is not recommended
       return OTS_FAILURE_MSG_HDR("font contains both PS and TT glyphs");
     }
diff --git a/gfx/ots/src/ots.h b/gfx/ots/src/ots.h
--- a/gfx/ots/src/ots.h
+++ b/gfx/ots/src/ots.h
@@ -107,17 +107,17 @@ class Buffer {
     return true;
   }
 
   bool ReadU16(uint16_t *value) {
     if (offset_ + 2 > length_) {
       return OTS_FAILURE();
     }
     std::memcpy(value, buffer_ + offset_, sizeof(uint16_t));
-    *value = ntohs(*value);
+    *value = ots_ntohs(*value);
     offset_ += 2;
     return true;
   }
 
   bool ReadS16(int16_t *value) {
     return ReadU16(reinterpret_cast<uint16_t*>(value));
   }
 
@@ -132,17 +132,17 @@ class Buffer {
     return true;
   }
 
   bool ReadU32(uint32_t *value) {
     if (offset_ + 4 > length_) {
       return OTS_FAILURE();
     }
     std::memcpy(value, buffer_ + offset_, sizeof(uint32_t));
-    *value = ntohl(*value);
+    *value = ots_ntohl(*value);
     offset_ += 4;
     return true;
   }
 
   bool ReadS32(int32_t *value) {
     return ReadU32(reinterpret_cast<uint32_t*>(value));
   }
 
diff --git a/gfx/ots/tests/layout_common_table_test.cc b/gfx/ots/tests/layout_common_table_test.cc
--- a/gfx/ots/tests/layout_common_table_test.cc
+++ b/gfx/ots/tests/layout_common_table_test.cc
@@ -528,17 +528,17 @@ TEST_F(LookupListTableTest, TesBadLookup
   EXPECT_FALSE(Parse());
 }
 
 TEST_F(LookupListTableTest, TesBadLookupFlag) {
   BuildFakeLookupListTable(&out, 1, 1);
   // Set IgnoreBaseGlyphs(0x0002) to the lookup flag of LookupTable[0].
   out.Seek(6);
   out.WriteU16(0x0002);
-  EXPECT_FALSE(Parse());
+  EXPECT_TRUE(Parse());
 }
 
 TEST_F(LookupListTableTest, TesBadSubtableCount) {
   BuildFakeLookupListTable(&out, 1, 1);
   // Set too large sutable count of LookupTable[0].
   out.Seek(8);
   out.WriteU16(2);
   EXPECT_FALSE(Parse());
