# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1505243657 -3600
#      Tue Sep 12 20:14:17 2017 +0100
# Node ID 24eb5afe1b98194c355f0437fd0de6c89009cf95
# Parent  05e0e3d22e4d3de13264fc92c91f5884c1b1a257
Bug 1403500, part 4 - Document and do some renaming for SVGAnimationElement's TargetReference. r=longsonr

MozReview-Commit-ID: 6vcMQAoJ9em

diff --git a/dom/svg/SVGAnimationElement.h b/dom/svg/SVGAnimationElement.h
--- a/dom/svg/SVGAnimationElement.h
+++ b/dom/svg/SVGAnimationElement.h
@@ -96,36 +96,45 @@ public:
 
  protected:
   // nsSVGElement overrides
 
   void UpdateHrefTarget(nsIContent* aNodeForContext,
                         const nsAString& aHrefStr);
   void AnimationTargetChanged();
 
-  class TargetReference : public mozilla::dom::IDTracker {
+  /**
+   * Helper that provides a reference to the element with the ID that is
+   * referenced by the animation element's 'href' attribute, if any, and that
+   * will notify the animation element if the element that that ID identifies
+   * changes to a different element (or none).  (If the 'href' attribute is not
+   * specified, then the animation target is the parent element and this helper
+   * is not used.)
+   */
+  class HrefTargetTracker final : public IDTracker {
   public:
-    explicit TargetReference(SVGAnimationElement* aAnimationElement) :
-      mAnimationElement(aAnimationElement) {}
+    explicit HrefTargetTracker(SVGAnimationElement* aAnimationElement)
+      : mAnimationElement(aAnimationElement)
+    {}
   protected:
     // We need to be notified when target changes, in order to request a
     // sample (which will clear animation effects from old target and apply
     // them to the new target) and update any event registrations.
     virtual void ElementChanged(Element* aFrom, Element* aTo) override {
       IDTracker::ElementChanged(aFrom, aTo);
       mAnimationElement->AnimationTargetChanged();
     }
 
     // We need to override IsPersistent to get persistent tracking (beyond the
     // first time the target changes)
     virtual bool IsPersistent() override { return true; }
   private:
     SVGAnimationElement* const mAnimationElement;
   };
 
-  TargetReference      mHrefTarget;
+  HrefTargetTracker    mHrefTarget;
   nsSMILTimedElement   mTimedElement;
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // mozilla_dom_SVGAnimationElement_h
