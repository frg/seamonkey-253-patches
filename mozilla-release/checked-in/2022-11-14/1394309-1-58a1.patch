# HG changeset patch
# User Ethan Lin <ethlin@mozilla.com>
# Date 1507710451 -28800
#      Wed Oct 11 16:27:31 2017 +0800
# Node ID 5be898720bb7dc93164b693b04db3433cfe0d4e2
# Parent  4ee13d258ff1946b06faae69422d9f7716fedd80
Bug 1394309 - Part1. Add clip rect to border render so we don't need to fall back for clipping. r=kats

MozReview-Commit-ID: 9KyYzMRYncV

diff --git a/layout/painting/nsCSSRendering.cpp b/layout/painting/nsCSSRendering.cpp
--- a/layout/painting/nsCSSRendering.cpp
+++ b/layout/painting/nsCSSRendering.cpp
@@ -868,17 +868,18 @@ ConstructBorderRenderer(nsPresContext* a
                              dirtyRect,
                              joinedBorderAreaPx,
                              borderStyles,
                              borderWidths,
                              bgRadii,
                              borderColors,
                              aStyleBorder.mBorderColors.get(),
                              bgColor,
-                             !aForFrame->BackfaceIsHidden());
+                             !aForFrame->BackfaceIsHidden(),
+                             *aNeedsClip ? Some(NSRectToRect(aBorderArea, oneDevPixel)) : Nothing());
 }
 
 
 DrawResult
 nsCSSRendering::PaintBorderWithStyleBorder(nsPresContext* aPresContext,
                                            gfxContext& aRenderingContext,
                                            nsIFrame* aForFrame,
                                            const nsRect& aDirtyRect,
@@ -1008,19 +1009,16 @@ nsCSSRendering::CreateBorderRendererWith
                                                    aStyleContext,
                                                    aDrawTarget,
                                                    aForFrame,
                                                    aDirtyRect,
                                                    aBorderArea,
                                                    aStyleBorder,
                                                    aSkipSides,
                                                    &needsClip);
-  if (needsClip) {
-    return Nothing();
-  }
   return Some(br);
 }
 
 static nsRect
 GetOutlineInnerRect(nsIFrame* aFrame)
 {
   nsRect* savedOutlineInnerRect =
     aFrame->GetProperty(nsIFrame::OutlineInnerRectProperty());
@@ -1148,17 +1146,18 @@ nsCSSRendering::CreateBorderRendererForO
                          dirtyRect,
                          oRect,
                          outlineStyles,
                          outlineWidths,
                          outlineRadii,
                          outlineColors,
                          nullptr,
                          bgColor,
-                         !aForFrame->BackfaceIsHidden());
+                         !aForFrame->BackfaceIsHidden(),
+                         Nothing());
 
   return Some(br);
 }
 
 void
 nsCSSRendering::PaintOutline(nsPresContext* aPresContext,
                              gfxContext& aRenderingContext,
                              nsIFrame* aForFrame,
@@ -1224,17 +1223,18 @@ nsCSSRendering::PaintFocus(nsPresContext
                          focusRect,
                          focusRect,
                          focusStyles,
                          focusWidths,
                          focusRadii,
                          focusColors,
                          nullptr,
                          NS_RGB(255, 0, 0),
-                         true);
+                         true,
+                         Nothing());
   br.DrawBorders();
 
   PrintAsStringNewline();
 }
 
 // Thebes Border Rendering Code End
 //----------------------------------------------------------------------
 
diff --git a/layout/painting/nsCSSRenderingBorders.cpp b/layout/painting/nsCSSRenderingBorders.cpp
--- a/layout/painting/nsCSSRenderingBorders.cpp
+++ b/layout/painting/nsCSSRenderingBorders.cpp
@@ -172,25 +172,27 @@ nsCSSBorderRenderer::nsCSSBorderRenderer
                                          const Rect& aDirtyRect,
                                          Rect& aOuterRect,
                                          const uint8_t* aBorderStyles,
                                          const Float* aBorderWidths,
                                          RectCornerRadii& aBorderRadii,
                                          const nscolor* aBorderColors,
                                          const nsBorderColors* aCompositeColors,
                                          nscolor aBackgroundColor,
-                                         bool aBackfaceIsVisible)
+                                         bool aBackfaceIsVisible,
+                                         const Maybe<Rect>& aClipRect)
   : mPresContext(aPresContext),
     mDocument(aDocument),
     mDrawTarget(aDrawTarget),
     mDirtyRect(aDirtyRect),
     mOuterRect(aOuterRect),
     mBorderRadii(aBorderRadii),
     mBackgroundColor(aBackgroundColor),
-    mBackfaceIsVisible(aBackfaceIsVisible)
+    mBackfaceIsVisible(aBackfaceIsVisible),
+    mLocalClip(aClipRect)
 {
   PodCopy(mBorderStyles, aBorderStyles, 4);
   PodCopy(mBorderWidths, aBorderWidths, 4);
   PodCopy(mBorderColors, aBorderColors, 4);
   NS_FOR_CSS_SIDES(side) {
     if (aCompositeColors && !(*aCompositeColors)[side].IsEmpty()) {
       mCompositeColors[side] = &(*aCompositeColors)[side];
     } else {
@@ -3611,23 +3613,35 @@ nsCSSBorderRenderer::CreateWebRenderComm
   NS_FOR_CSS_SIDES(i) {
     side[i] = wr::ToBorderSide(ToDeviceColor(mBorderColors[i]), mBorderStyles[i]);
   }
 
   wr::BorderRadius borderRadius = wr::ToBorderRadius(LayerSize(mBorderRadii[0].width, mBorderRadii[0].height),
                                                      LayerSize(mBorderRadii[1].width, mBorderRadii[1].height),
                                                      LayerSize(mBorderRadii[3].width, mBorderRadii[3].height),
                                                      LayerSize(mBorderRadii[2].width, mBorderRadii[2].height));
+
+  if (mLocalClip) {
+    LayoutDeviceRect clip = LayoutDeviceRect::FromUnknownRect(mLocalClip.value());
+    wr::LayoutRect clipRect = aSc.ToRelativeLayoutRect(clip);
+    wr::WrClipId clipId = aBuilder.DefineClip(clipRect);
+    aBuilder.PushClip(clipId);
+  }
+
   Range<const wr::BorderSide> wrsides(side, 4);
   aBuilder.PushBorder(transformedRect,
                       transformedRect,
                       mBackfaceIsVisible,
                       wr::ToBorderWidths(mBorderWidths[0], mBorderWidths[1], mBorderWidths[2], mBorderWidths[3]),
                       wrsides,
                       borderRadius);
+
+  if (mLocalClip) {
+    aBuilder.PopClip();
+  }
 }
 
 /* static */Maybe<nsCSSBorderImageRenderer>
 nsCSSBorderImageRenderer::CreateBorderImageRenderer(nsPresContext* aPresContext,
                                                     nsIFrame* aForFrame,
                                                     const nsRect& aBorderArea,
                                                     const nsStyleBorder& aStyleBorder,
                                                     const nsRect& aDirtyRect,
diff --git a/layout/painting/nsCSSRenderingBorders.h b/layout/painting/nsCSSRenderingBorders.h
--- a/layout/painting/nsCSSRenderingBorders.h
+++ b/layout/painting/nsCSSRenderingBorders.h
@@ -96,17 +96,18 @@ public:
                       const Rect& aDirtyRect,
                       Rect& aOuterRect,
                       const uint8_t* aBorderStyles,
                       const Float* aBorderWidths,
                       RectCornerRadii& aBorderRadii,
                       const nscolor* aBorderColors,
                       const nsBorderColors* aCompositeColors,
                       nscolor aBackgroundColor,
-                      bool aBackfaceIsVisible);
+                      bool aBackfaceIsVisible,
+                      const mozilla::Maybe<Rect>& aClipRect);
 
   // draw the entire border
   void DrawBorders();
 
   bool CanCreateWebRenderCommands();
   void CreateWebRenderCommands(mozilla::wr::DisplayListBuilder& aBuilder,
                                mozilla::wr::IpcResourceUpdateQueue& aResources,
                                const mozilla::layers::StackingContextHelper& aSc);
@@ -160,16 +161,17 @@ private:
 
   // calculated values
   bool mAllBordersSameStyle;
   bool mAllBordersSameWidth;
   bool mOneUnitBorder;
   bool mNoBorderRadius;
   bool mAvoidStroke;
   bool mBackfaceIsVisible;
+  mozilla::Maybe<Rect> mLocalClip;
 
   // For all the sides in the bitmask, would they be rendered
   // in an identical color and style?
   bool AreBorderSideFinalStylesSame(uint8_t aSides);
 
   // For the given style, is the given corner a solid color?
   bool IsSolidCornerStyle(uint8_t aStyle, mozilla::Corner aCorner);
 
