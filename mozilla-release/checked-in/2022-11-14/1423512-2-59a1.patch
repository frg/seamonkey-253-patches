# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1512521762 -32400
# Node ID 0c9996186048d2ad338c153861fb1c8aa80ce181
# Parent  7f9cfe0621baddba8754e10de4d110d575995735
Bug 1423512 - Remove infallible allocator exception for xpcom glue code. r=erahm

Back when mozalloc was a separate library, the xpcom glue code could not
use the infallible allocator API. But since bug 868814, that's not the
case anymore, so we can safely include mozalloc.h when XPCOM_GLUE is
set.

diff --git a/config/gcc-stl-wrapper.template.h b/config/gcc-stl-wrapper.template.h
--- a/config/gcc-stl-wrapper.template.h
+++ b/config/gcc-stl-wrapper.template.h
@@ -48,20 +48,18 @@
 #  define MOZ_INCLUDE_MOZALLOC_H_FROM_${HEADER}
 #endif
 
 #pragma GCC visibility push(default)
 #include_next <${HEADER}>
 #pragma GCC visibility pop
 
 #ifdef MOZ_INCLUDE_MOZALLOC_H_FROM_${HEADER}
-// See if we're in code that can use mozalloc.  NB: this duplicates
-// code in nscore.h because nscore.h pulls in prtypes.h, and chromium
-// can't build with that being included before base/basictypes.h.
-#  if !defined(XPCOM_GLUE) && !defined(NS_NO_XPCOM) && !defined(MOZ_NO_MOZALLOC)
+// See if we're in code that can use mozalloc.
+#  if !defined(NS_NO_XPCOM) && !defined(MOZ_NO_MOZALLOC)
 #    include "mozilla/mozalloc.h"
 #  else
 #    error "STL code can only be used with infallible ::operator new()"
 #  endif
 #endif
 
 // gcc calls a __throw_*() function from bits/functexcept.h when it
 // wants to "throw an exception".  functexcept exists nominally to
diff --git a/config/msvc-stl-wrapper.template.h b/config/msvc-stl-wrapper.template.h
--- a/config/msvc-stl-wrapper.template.h
+++ b/config/msvc-stl-wrapper.template.h
@@ -57,19 +57,17 @@
 #include_next <${HEADER}>
 #else
 #include <${HEADER_PATH}>
 #endif
 
 #pragma warning( pop )
 
 #ifdef MOZ_INCLUDE_MOZALLOC_H_FROM_${HEADER}
-// See if we're in code that can use mozalloc.  NB: this duplicates
-// code in nscore.h because nscore.h pulls in prtypes.h, and chromium
-// can't build with that being included before base/basictypes.h.
-#  if !defined(XPCOM_GLUE) && !defined(NS_NO_XPCOM) && !defined(MOZ_NO_MOZALLOC)
+// See if we're in code that can use mozalloc.
+#  if !defined(NS_NO_XPCOM) && !defined(MOZ_NO_MOZALLOC)
 #    include "mozilla/mozalloc.h"
 #  else
 #    error "STL code can only be used with infallible ::operator new()"
 #  endif
 #endif
 
 #endif  // if mozilla_${HEADER}_h
diff --git a/xpcom/base/nscore.h b/xpcom/base/nscore.h
--- a/xpcom/base/nscore.h
+++ b/xpcom/base/nscore.h
@@ -11,17 +11,17 @@
  * Make sure that we have the proper platform specific
  * c++ definitions needed by nscore.h
  */
 #ifndef _XPCOM_CONFIG_H_
 #include "xpcom-config.h"
 #endif
 
 /* Definitions of functions and operators that allocate memory. */
-#if !defined(XPCOM_GLUE) && !defined(NS_NO_XPCOM) && !defined(MOZ_NO_MOZALLOC)
+#if !defined(NS_NO_XPCOM) && !defined(MOZ_NO_MOZALLOC)
 #  include "mozilla/mozalloc.h"
 #endif
 
 /**
  * Incorporate the integer data types which XPCOM uses.
  */
 #include <stddef.h>
 #include <stdint.h>
