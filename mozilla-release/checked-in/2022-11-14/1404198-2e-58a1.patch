# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1508134411 25200
#      Sun Oct 15 23:13:31 2017 -0700
# Node ID 474d24f4545fb096b91a6eafec3b76e528ef1092
# Parent  c92f58659591b5e746c156b653ee327b111bd9df
Bug 1404198: Part 2e - Switch to NS_NewTimer* in layout. r=njn

MozReview-Commit-ID: 2WxyAXpJ3Fg

diff --git a/layout/base/AccessibleCaretEventHub.cpp b/layout/base/AccessibleCaretEventHub.cpp
--- a/layout/base/AccessibleCaretEventHub.cpp
+++ b/layout/base/AccessibleCaretEventHub.cpp
@@ -435,20 +435,20 @@ AccessibleCaretEventHub::Init()
   }
 
   docShell->AddWeakReflowObserver(this);
   docShell->AddWeakScrollObserver(this);
 
   mDocShell = static_cast<nsDocShell*>(docShell);
 
   if (sUseLongTapInjector) {
-    mLongTapInjectorTimer = do_CreateInstance("@mozilla.org/timer;1");
+    mLongTapInjectorTimer = NS_NewTimer();
   }
 
-  mScrollEndInjectorTimer = do_CreateInstance("@mozilla.org/timer;1");
+  mScrollEndInjectorTimer = NS_NewTimer();
 
   mManager = MakeUnique<AccessibleCaretManager>(mPresShell);
 
   mInitialized = true;
 }
 
 void
 AccessibleCaretEventHub::Terminate()
diff --git a/layout/base/PresShell.cpp b/layout/base/PresShell.cpp
--- a/layout/base/PresShell.cpp
+++ b/layout/base/PresShell.cpp
@@ -1803,17 +1803,17 @@ PresShell::Initialize(nscoord aWidth, ns
   if (!mPresContext->IsPaginated()) {
     // Kick off a one-shot timer based off our pref value.  When this timer
     // fires, if painting is still locked down, then we will go ahead and
     // trigger a full invalidate and allow painting to proceed normally.
     mPaintingSuppressed = true;
     // Don't suppress painting if the document isn't loading.
     nsIDocument::ReadyState readyState = mDocument->GetReadyStateEnum();
     if (readyState != nsIDocument::READYSTATE_COMPLETE) {
-      mPaintSuppressionTimer = do_CreateInstance("@mozilla.org/timer;1");
+      mPaintSuppressionTimer = NS_NewTimer();
     }
     if (!mPaintSuppressionTimer) {
       mPaintingSuppressed = false;
     } else {
       // Initialize the timer.
 
       // Default to PAINTLOCK_EVENT_DELAY if we can't get the pref value.
       int32_t delay =
@@ -1959,17 +1959,17 @@ PresShell::ResizeReflowIgnoreOverride(ns
       }
     }
   }
 
   if (!mIsDestroying && !mResizeEvent.IsPending() &&
       !mAsyncResizeTimerIsActive) {
     if (mInResize) {
       if (!mAsyncResizeEventTimer) {
-        mAsyncResizeEventTimer = do_CreateInstance("@mozilla.org/timer;1");
+        mAsyncResizeEventTimer = NS_NewTimer();
       }
       if (mAsyncResizeEventTimer) {
         mAsyncResizeTimerIsActive = true;
         mAsyncResizeEventTimer->SetTarget(
             mDocument->EventTargetFor(TaskCategory::Other));
         mAsyncResizeEventTimer->InitWithNamedFuncCallback(AsyncResizeEventCallback,
                                                           this, 15,
                                                           nsITimer::TYPE_ONE_SHOT,
@@ -3668,24 +3668,23 @@ PresShell::ScheduleViewManagerFlush(Pain
           // The passed-in PresShell is always alive here. Because if PresShell
           // died, mDelayedPaintTimer->Cancel() would be called during the
           // destruction and this callback would never be invoked.
           auto self = static_cast<PresShell*>(aClosure);
           self->SetNextPaintCompressed();
           self->ScheduleViewManagerFlush();
       };
 
-      mDelayedPaintTimer = do_CreateInstance(NS_TIMER_CONTRACTID);
-      mDelayedPaintTimer->SetTarget(
-          mDocument->EventTargetFor(TaskCategory::Other));
-      mDelayedPaintTimer->InitWithNamedFuncCallback(PaintTimerCallBack,
-                                                    this,
-                                                    kPaintDelayPeriod,
-                                                    nsITimer::TYPE_ONE_SHOT,
-                                                    "PaintTimerCallBack");
+      NS_NewTimerWithFuncCallback(getter_AddRefs(mDelayedPaintTimer),
+                                  PaintTimerCallBack,
+                                  this,
+                                  kPaintDelayPeriod,
+                                  nsITimer::TYPE_ONE_SHOT,
+                                  "PaintTimerCallBack",
+                                  mDocument->EventTargetFor(TaskCategory::Other));
     }
     return;
   }
 
   nsPresContext* presContext = GetPresContext();
   if (presContext) {
     presContext->RefreshDriver()->ScheduleViewManagerFlush();
   }
@@ -8538,26 +8537,23 @@ PresShell::sReflowContinueCallback(nsITi
 
 bool
 PresShell::ScheduleReflowOffTimer()
 {
   NS_PRECONDITION(!mObservingLayoutFlushes, "Shouldn't get here");
   ASSERT_REFLOW_SCHEDULED_STATE();
 
   if (!mReflowContinueTimer) {
-    mReflowContinueTimer = do_CreateInstance("@mozilla.org/timer;1");
-    mReflowContinueTimer->SetTarget(
+    nsresult rv = NS_NewTimerWithFuncCallback(
+        getter_AddRefs(mReflowContinueTimer),
+        sReflowContinueCallback, this, 30,
+        nsITimer::TYPE_ONE_SHOT,
+        "sReflowContinueCallback",
         mDocument->EventTargetFor(TaskCategory::Other));
-    if (!mReflowContinueTimer ||
-        NS_FAILED(mReflowContinueTimer->
-                    InitWithNamedFuncCallback(sReflowContinueCallback, this, 30,
-                                              nsITimer::TYPE_ONE_SHOT,
-                                              "sReflowContinueCallback"))) {
-      return false;
-    }
+    return NS_SUCCEEDED(rv);
   }
   return true;
 }
 
 bool
 PresShell::DoReflow(nsIFrame* target, bool aInterruptible)
 {
   gfxTextPerfMetrics* tp = mPresContext->GetTextPerfMetrics();
diff --git a/layout/base/nsCaret.cpp b/layout/base/nsCaret.cpp
--- a/layout/base/nsCaret.cpp
+++ b/layout/base/nsCaret.cpp
@@ -631,26 +631,26 @@ void nsCaret::ResetBlinking()
     // If the rate hasn't changed, then there is nothing to do.
     return;
   }
   mBlinkRate = blinkRate;
 
   if (mBlinkTimer) {
     mBlinkTimer->Cancel();
   } else {
-    nsresult  err;
-    mBlinkTimer = do_CreateInstance("@mozilla.org/timer;1", &err);
-    if (NS_FAILED(err)) {
-      return;
+    nsIEventTarget* target = nullptr;
+    if (nsCOMPtr<nsIPresShell> presShell = do_QueryReferent(mPresShell)) {
+      if (nsCOMPtr<nsIDocument> doc = presShell->GetDocument()) {
+        target = doc->EventTargetFor(TaskCategory::Other);
+      }
     }
 
-    if (nsCOMPtr<nsIPresShell> presShell = do_QueryReferent(mPresShell)) {
-      if (nsCOMPtr<nsIDocument> doc = presShell->GetDocument()) {
-        mBlinkTimer->SetTarget(doc->EventTargetFor(TaskCategory::Other));
-      }
+    mBlinkTimer = NS_NewTimer(target);
+    if (!mBlinkTimer) {
+      return;
     }
   }
 
   if (blinkRate > 0) {
     mBlinkCount = Preferences::GetInt("ui.caretBlinkCount", -1);
     mBlinkTimer->InitWithNamedFuncCallback(CaretBlinkCallback, this, blinkRate,
                                            nsITimer::TYPE_REPEATING_SLACK,
                                            "nsCaret::CaretBlinkCallback_timer");
diff --git a/layout/base/nsPresContext.cpp b/layout/base/nsPresContext.cpp
--- a/layout/base/nsPresContext.cpp
+++ b/layout/base/nsPresContext.cpp
@@ -2702,28 +2702,23 @@ nsPresContext::HasCachedStyleData()
   return styleSet->HasCachedStyleData();
 }
 
 already_AddRefed<nsITimer>
 nsPresContext::CreateTimer(nsTimerCallbackFunc aCallback,
                            const char* aName,
                            uint32_t aDelay)
 {
-  nsCOMPtr<nsITimer> timer = do_CreateInstance("@mozilla.org/timer;1");
-  timer->SetTarget(Document()->EventTargetFor(TaskCategory::Other));
-  if (timer) {
-    nsresult rv = timer->InitWithNamedFuncCallback(aCallback, this, aDelay,
-                                                   nsITimer::TYPE_ONE_SHOT,
-                                                   aName);
-    if (NS_SUCCEEDED(rv)) {
-      return timer.forget();
-    }
-  }
-
-  return nullptr;
+  nsCOMPtr<nsITimer> timer;
+  NS_NewTimerWithFuncCallback(getter_AddRefs(timer),
+                              aCallback, this, aDelay,
+                              nsITimer::TYPE_ONE_SHOT,
+                              aName,
+                              Document()->EventTargetFor(TaskCategory::Other));
+  return timer.forget();
 }
 
 static bool sGotInterruptEnv = false;
 enum InterruptMode {
   ModeRandom,
   ModeCounter,
   ModeEvent
 };
@@ -3103,31 +3098,30 @@ void
 nsRootPresContext::EnsureEventualDidPaintEvent(uint64_t aTransactionId)
 {
   for (NotifyDidPaintTimer& t : mNotifyDidPaintTimers) {
     if (t.mTransactionId == aTransactionId) {
       return;
     }
   }
 
-  nsCOMPtr<nsITimer> timer = do_CreateInstance("@mozilla.org/timer;1");
-  timer->SetTarget(Document()->EventTargetFor(TaskCategory::Other));
-  if (timer) {
-    RefPtr<nsRootPresContext> self = this;
-    nsresult rv = timer->InitWithCallback(
-      NewNamedTimerCallback([self, aTransactionId](){
-        nsAutoScriptBlocker blockScripts;
-        self->NotifyDidPaintForSubtree(aTransactionId);
-    }, "NotifyDidPaintForSubtree"), 100, nsITimer::TYPE_ONE_SHOT);
-
-    if (NS_SUCCEEDED(rv)) {
-      NotifyDidPaintTimer* t = mNotifyDidPaintTimers.AppendElement();
-      t->mTransactionId = aTransactionId;
-      t->mTimer = timer;
-    }
+  nsCOMPtr<nsITimer> timer;
+  RefPtr<nsRootPresContext> self = this;
+  nsresult rv = NS_NewTimerWithCallback(
+    getter_AddRefs(timer),
+    NewNamedTimerCallback([self, aTransactionId](){
+      nsAutoScriptBlocker blockScripts;
+      self->NotifyDidPaintForSubtree(aTransactionId);
+     }, "NotifyDidPaintForSubtree"), 100, nsITimer::TYPE_ONE_SHOT,
+    Document()->EventTargetFor(TaskCategory::Other));
+
+  if (NS_SUCCEEDED(rv)) {
+    NotifyDidPaintTimer* t = mNotifyDidPaintTimers.AppendElement();
+    t->mTransactionId = aTransactionId;
+    t->mTimer = timer;
   }
 }
 
 void
 nsRootPresContext::CancelDidPaintTimers(uint64_t aTransactionId)
 {
   uint32_t i = 0;
   while (i < mNotifyDidPaintTimers.Length()) {
diff --git a/layout/base/nsRefreshDriver.cpp b/layout/base/nsRefreshDriver.cpp
--- a/layout/base/nsRefreshDriver.cpp
+++ b/layout/base/nsRefreshDriver.cpp
@@ -349,17 +349,17 @@ class SimpleTimerBasedRefreshDriverTimer
 {
 public:
   /*
    * aRate -- the delay, in milliseconds, requested between timer firings
    */
   explicit SimpleTimerBasedRefreshDriverTimer(double aRate)
   {
     SetRate(aRate);
-    mTimer = do_CreateInstance(NS_TIMER_CONTRACTID);
+    mTimer = NS_NewTimer();
   }
 
   virtual ~SimpleTimerBasedRefreshDriverTimer() override
   {
     StopTimer();
   }
 
   // will take effect at next timer tick
diff --git a/layout/generic/ScrollbarActivity.cpp b/layout/generic/ScrollbarActivity.cpp
--- a/layout/generic/ScrollbarActivity.cpp
+++ b/layout/generic/ScrollbarActivity.cpp
@@ -424,17 +424,17 @@ ScrollbarActivity::SetIsFading(bool aNew
 
 void
 ScrollbarActivity::StartFadeBeginTimer()
 {
   if (GetForceAlwaysVisiblePref()) {
     return;
   }
   if (!mFadeBeginTimer) {
-    mFadeBeginTimer = do_CreateInstance("@mozilla.org/timer;1");
+    mFadeBeginTimer = NS_NewTimer();
   }
   mFadeBeginTimer->InitWithNamedFuncCallback(
     FadeBeginTimerFired, this, mScrollbarFadeBeginDelay,
     nsITimer::TYPE_ONE_SHOT, "ScrollbarActivity::FadeBeginTimerFired");
 }
 
 void
 ScrollbarActivity::CancelFadeBeginTimer()
diff --git a/layout/generic/nsGfxScrollFrame.cpp b/layout/generic/nsGfxScrollFrame.cpp
--- a/layout/generic/nsGfxScrollFrame.cpp
+++ b/layout/generic/nsGfxScrollFrame.cpp
@@ -2597,17 +2597,17 @@ void ScrollFrameHelper::TriggerDisplayPo
   }
 
   if (!gfxPrefs::APZDisplayPortExpiryTime()) {
     // a zero time disables the expiry
     return;
   }
 
   if (!mDisplayPortExpiryTimer) {
-    mDisplayPortExpiryTimer = do_CreateInstance("@mozilla.org/timer;1");
+    mDisplayPortExpiryTimer = NS_NewTimer();
   }
   ResetDisplayPortExpiryTimer();
 }
 
 void ScrollFrameHelper::ScrollVisual()
 {
   // Mark this frame as having been scrolled. If this is the root
   // scroll frame of a content document, then IsAlwaysActive()
@@ -2717,23 +2717,21 @@ ScrollFrameHelper::ScrollActivityCallbac
   self->mOuter->PresShell()->SynthesizeMouseMove(true);
 }
 
 
 void
 ScrollFrameHelper::ScheduleSyntheticMouseMove()
 {
   if (!mScrollActivityTimer) {
-    mScrollActivityTimer = do_CreateInstance("@mozilla.org/timer;1");
+    mScrollActivityTimer = NS_NewTimer(
+      mOuter->PresContext()->Document()->EventTargetFor(TaskCategory::Other));
     if (!mScrollActivityTimer) {
       return;
     }
-
-    mScrollActivityTimer->SetTarget(
-      mOuter->PresContext()->Document()->EventTargetFor(TaskCategory::Other));
   }
 
   mScrollActivityTimer->InitWithNamedFuncCallback(
     ScrollActivityCallback,
     this,
     100,
     nsITimer::TYPE_ONE_SHOT,
     "ScrollFrameHelper::ScheduleSyntheticMouseMove");
diff --git a/layout/printing/nsPagePrintTimer.cpp b/layout/printing/nsPagePrintTimer.cpp
--- a/layout/printing/nsPagePrintTimer.cpp
+++ b/layout/printing/nsPagePrintTimer.cpp
@@ -18,54 +18,42 @@ nsPagePrintTimer::~nsPagePrintTimer()
 {
   // This matches the IncrementDestroyRefCount call in the constructor.
   mDocViewerPrint->DecrementDestroyRefCount();
 }
 
 nsresult
 nsPagePrintTimer::StartTimer(bool aUseDelay)
 {
-  nsresult result;
-  mTimer = do_CreateInstance("@mozilla.org/timer;1", &result);
-  if (NS_FAILED(result)) {
-    NS_WARNING("unable to start the timer");
-  } else {
-    uint32_t delay = 0;
-    if (aUseDelay) {
-      if (mFiringCount < 10) {
-        // Longer delay for the few first pages.
-        delay = mDelay + ((10 - mFiringCount) * 100);
-      } else {
-        delay = mDelay;
-      }
+  uint32_t delay = 0;
+  if (aUseDelay) {
+    if (mFiringCount < 10) {
+      // Longer delay for the few first pages.
+      delay = mDelay + ((10 - mFiringCount) * 100);
+    } else {
+      delay = mDelay;
     }
-    mTimer->SetTarget(mDocument->EventTargetFor(TaskCategory::Other));
-    mTimer->InitWithCallback(this, delay, nsITimer::TYPE_ONE_SHOT);
   }
-  return result;
+  return NS_NewTimerWithCallback(getter_AddRefs(mTimer),
+                                 this, delay, nsITimer::TYPE_ONE_SHOT,
+                                 mDocument->EventTargetFor(TaskCategory::Other));
 }
 
 nsresult
 nsPagePrintTimer::StartWatchDogTimer()
 {
-  nsresult result;
   if (mWatchDogTimer) {
     mWatchDogTimer->Cancel();
   }
-  mWatchDogTimer = do_CreateInstance("@mozilla.org/timer;1", &result);
-  if (NS_FAILED(result)) {
-    NS_WARNING("unable to start the timer");
-  } else {
-    // Instead of just doing one timer for a long period do multiple so we
-    // can check if the user cancelled the printing.
-    mWatchDogTimer->SetTarget(mDocument->EventTargetFor(TaskCategory::Other));
-    mWatchDogTimer->InitWithCallback(this, WATCH_DOG_INTERVAL,
-                                     nsITimer::TYPE_ONE_SHOT);
-  }
-  return result;
+  // Instead of just doing one timer for a long period do multiple so we
+  // can check if the user cancelled the printing.
+  return NS_NewTimerWithCallback(getter_AddRefs(mWatchDogTimer),
+                                 this, WATCH_DOG_INTERVAL,
+                                 nsITimer::TYPE_ONE_SHOT,
+                                 mDocument->EventTargetFor(TaskCategory::Other));
 }
 
 void
 nsPagePrintTimer::StopWatchDogTimer()
 {
   if (mWatchDogTimer) {
     mWatchDogTimer->Cancel();
     mWatchDogTimer = nullptr;
@@ -168,21 +156,19 @@ nsPagePrintTimer::Notify(nsITimer *timer
   }
   return NS_OK;
 }
 
 
 void
 nsPagePrintTimer::WaitForRemotePrint()
 {
-  nsresult result;
-  mWaitingForRemotePrint = do_CreateInstance("@mozilla.org/timer;1", &result);
-  if (NS_FAILED(result)) {
+  mWaitingForRemotePrint = NS_NewTimer();
+  if (!mWaitingForRemotePrint) {
     NS_WARNING("Failed to wait for remote print, we might time-out.");
-    mWaitingForRemotePrint = nullptr;
   }
 }
 
 void
 nsPagePrintTimer::RemotePrintFinished()
 {
   if (!mWaitingForRemotePrint) {
     return;
diff --git a/layout/style/nsFontFaceLoader.cpp b/layout/style/nsFontFaceLoader.cpp
--- a/layout/style/nsFontFaceLoader.cpp
+++ b/layout/style/nsFontFaceLoader.cpp
@@ -83,26 +83,23 @@ nsFontFaceLoader::StartedLoading(nsIStre
   if (fontDisplay == NS_FONT_DISPLAY_AUTO ||
       fontDisplay == NS_FONT_DISPLAY_BLOCK) {
     loadTimeout = GetFallbackDelay();
   } else {
     loadTimeout = GetShortFallbackDelay();
   }
 
   if (loadTimeout > 0) {
-    mLoadTimer = do_CreateInstance("@mozilla.org/timer;1");
-    if (mLoadTimer) {
-      mLoadTimer->SetTarget(
-        mFontFaceSet->Document()->EventTargetFor(TaskCategory::Other));
-      mLoadTimer->InitWithNamedFuncCallback(LoadTimerCallback,
-                                            static_cast<void*>(this),
-                                            loadTimeout,
-                                            nsITimer::TYPE_ONE_SHOT,
-                                            "LoadTimerCallback");
-    }
+    NS_NewTimerWithFuncCallback(getter_AddRefs(mLoadTimer),
+                                LoadTimerCallback,
+                                static_cast<void*>(this),
+                                loadTimeout,
+                                nsITimer::TYPE_ONE_SHOT,
+                                "LoadTimerCallback",
+                                mFontFaceSet->Document()->EventTargetFor(TaskCategory::Other));
   } else {
     mUserFontEntry->mFontDataLoadingState = gfxUserFontEntry::LOADING_SLOWLY;
   }
   mStreamLoader = aStreamLoader;
 }
 
 /* static */ void
 nsFontFaceLoader::LoadTimerCallback(nsITimer* aTimer, void* aClosure)
diff --git a/layout/xul/nsListBoxBodyFrame.cpp b/layout/xul/nsListBoxBodyFrame.cpp
--- a/layout/xul/nsListBoxBodyFrame.cpp
+++ b/layout/xul/nsListBoxBodyFrame.cpp
@@ -117,30 +117,29 @@ nsListScrollSmoother::Start()
     NS_ASSERTION(self->mOuter, "mOuter is null, see bug #68365");
     if (self->mOuter) {
       // actually do some work.
       self->mOuter->InternalPositionChangedCallback();
     }
   };
 
   Stop();
-  mRepeatTimer = do_CreateInstance("@mozilla.org/timer;1");
-  nsIContent* content = nullptr;
+  nsIEventTarget* target = nullptr;
   if (mOuter) {
-    content = mOuter->GetContent();
+    if (nsIContent* content = mOuter->GetContent()) {
+      target = content->OwnerDoc()->EventTargetFor(TaskCategory::Other);
+    }
   }
-  if (content) {
-    mRepeatTimer->SetTarget(
-        content->OwnerDoc()->EventTargetFor(TaskCategory::Other));
-  }
-  mRepeatTimer->InitWithNamedFuncCallback(scrollSmootherCallback,
-                                          this,
-                                          SMOOTH_INTERVAL,
-                                          nsITimer::TYPE_ONE_SHOT,
-                                          "scrollSmootherCallback");
+  NS_NewTimerWithFuncCallback(getter_AddRefs(mRepeatTimer),
+                              scrollSmootherCallback,
+                              this,
+                              SMOOTH_INTERVAL,
+                              nsITimer::TYPE_ONE_SHOT,
+                              "scrollSmootherCallback",
+                              target);
 }
 
 void
 nsListScrollSmoother::Stop()
 {
   if ( mRepeatTimer ) {
     mRepeatTimer->Cancel();
     mRepeatTimer = nullptr;
diff --git a/layout/xul/nsMenuFrame.cpp b/layout/xul/nsMenuFrame.cpp
--- a/layout/xul/nsMenuFrame.cpp
+++ b/layout/xul/nsMenuFrame.cpp
@@ -524,20 +524,19 @@ nsMenuFrame::HandleEvent(nsPresContext* 
     // A timer is used so that it doesn't open if the user moves the mouse quickly
     // past the menu. This conditional check ensures that only menus have this
     // behaviour
     if (!IsDisabled() && IsMenu() && !IsOpen() && !mOpenTimer && !menuParent->IsMenuBar()) {
       int32_t menuDelay =
         LookAndFeel::GetInt(LookAndFeel::eIntID_SubmenuDelay, 300); // ms
 
       // We're a menu, we're built, we're closed, and no timer has been kicked off.
-      mOpenTimer = do_CreateInstance("@mozilla.org/timer;1");
-      mOpenTimer->SetTarget(
-          mContent->OwnerDoc()->EventTargetFor(TaskCategory::Other));
-      mOpenTimer->InitWithCallback(mTimerMediator, menuDelay, nsITimer::TYPE_ONE_SHOT);
+      NS_NewTimerWithCallback(getter_AddRefs(mOpenTimer),
+                              mTimerMediator, menuDelay, nsITimer::TYPE_ONE_SHOT,
+                              mContent->OwnerDoc()->EventTargetFor(TaskCategory::Other));
     }
   }
 
   return NS_OK;
 }
 
 void
 nsMenuFrame::ToggleMenuState()
@@ -1240,20 +1239,19 @@ nsMenuFrame::StartBlinking(WidgetGUIEven
 
   nsMenuParent* menuParent = GetMenuParent();
   if (menuParent) {
     // Make this menu ignore events from now on.
     menuParent->LockMenuUntilClosed(true);
   }
 
   // Set up a timer to blink back on.
-  mBlinkTimer = do_CreateInstance("@mozilla.org/timer;1");
-  mBlinkTimer->SetTarget(
-      mContent->OwnerDoc()->EventTargetFor(TaskCategory::Other));
-  mBlinkTimer->InitWithCallback(mTimerMediator, kBlinkDelay, nsITimer::TYPE_ONE_SHOT);
+  NS_NewTimerWithCallback(getter_AddRefs(mBlinkTimer),
+                          mTimerMediator, kBlinkDelay, nsITimer::TYPE_ONE_SHOT,
+                          mContent->OwnerDoc()->EventTargetFor(TaskCategory::Other));
   mBlinkState = 1;
 }
 
 void
 nsMenuFrame::StopBlinking()
 {
   mBlinkState = 0;
   if (mBlinkTimer) {
diff --git a/layout/xul/nsRepeatService.cpp b/layout/xul/nsRepeatService.cpp
--- a/layout/xul/nsRepeatService.cpp
+++ b/layout/xul/nsRepeatService.cpp
@@ -50,21 +50,20 @@ nsRepeatService::Start(Callback aCallbac
                        uint32_t aInitialDelay)
 {
   NS_PRECONDITION(aCallback != nullptr, "null ptr");
 
   mCallback = aCallback;
   mCallbackData = aCallbackData;
   mCallbackName = aCallbackName;
 
-  nsresult rv;
-  mRepeatTimer = do_CreateInstance("@mozilla.org/timer;1", &rv);
+  mRepeatTimer = NS_NewTimer(
+    aDocument->EventTargetFor(TaskCategory::Other));
 
-  if (NS_SUCCEEDED(rv))  {
-    mRepeatTimer->SetTarget(aDocument->EventTargetFor(TaskCategory::Other));
+  if (mRepeatTimer)  {
     InitTimerCallback(aInitialDelay);
   }
 }
 
 void nsRepeatService::Stop(Callback aCallback, void* aCallbackData)
 {
   if (mCallback != aCallback || mCallbackData != aCallbackData)
     return;
diff --git a/layout/xul/nsXULPopupManager.cpp b/layout/xul/nsXULPopupManager.cpp
--- a/layout/xul/nsXULPopupManager.cpp
+++ b/layout/xul/nsXULPopupManager.cpp
@@ -1239,28 +1239,29 @@ nsXULPopupManager::HidePopupAfterDelay(n
   // Don't close up immediately.
   // Kick off a close timer.
   KillMenuTimer();
 
   int32_t menuDelay =
     LookAndFeel::GetInt(LookAndFeel::eIntID_SubmenuDelay, 300); // ms
 
   // Kick off the timer.
-  mCloseTimer = do_CreateInstance("@mozilla.org/timer;1");
-  nsIContent* content = aPopup->GetContent();
-  if (content) {
-    mCloseTimer->SetTarget(
-        content->OwnerDoc()->EventTargetFor(TaskCategory::Other));
+  nsIEventTarget* target = nullptr;
+  if (nsIContent* content = aPopup->GetContent()) {
+    target = content->OwnerDoc()->EventTargetFor(TaskCategory::Other);
   }
-  mCloseTimer->InitWithNamedFuncCallback([](nsITimer* aTimer, void* aClosure) {
-    nsXULPopupManager* pm = nsXULPopupManager::GetInstance();
-    if (pm) {
-      pm->KillMenuTimer();
-    }
-  }, nullptr, menuDelay, nsITimer::TYPE_ONE_SHOT, "KillMenuTimer");
+  NS_NewTimerWithFuncCallback(
+    getter_AddRefs(mCloseTimer),
+    [](nsITimer* aTimer, void* aClosure) {
+      nsXULPopupManager* pm = nsXULPopupManager::GetInstance();
+      if (pm) {
+        pm->KillMenuTimer();
+      }
+    }, nullptr, menuDelay, nsITimer::TYPE_ONE_SHOT, "KillMenuTimer",
+    target);
 
   // the popup will call PopupDestroyed if it is destroyed, which checks if it
   // is set to mTimerMenu, so it should be safe to keep a reference to it
   mTimerMenu = aPopup;
 }
 
 void
 nsXULPopupManager::HidePopupsInList(const nsTArray<nsMenuPopupFrame *> &aFrames)
diff --git a/layout/xul/nsXULTooltipListener.cpp b/layout/xul/nsXULTooltipListener.cpp
--- a/layout/xul/nsXULTooltipListener.cpp
+++ b/layout/xul/nsXULTooltipListener.cpp
@@ -190,31 +190,28 @@ nsXULTooltipListener::MouseMove(nsIDOMEv
           mSourceNode = nullptr;
           return;
         }
 
         targetContent = targetContent->GetParent();
       }
     }
 
-    mTooltipTimer = do_CreateInstance("@mozilla.org/timer;1");
-    mTooltipTimer->SetTarget(
+    mTargetNode = do_GetWeakReference(eventTarget);
+    if (mTargetNode) {
+      nsresult rv = NS_NewTimerWithFuncCallback(
+        getter_AddRefs(mTooltipTimer),
+        sTooltipCallback, this,
+        LookAndFeel::GetInt(LookAndFeel::eIntID_TooltipDelay, 500),
+        nsITimer::TYPE_ONE_SHOT,
+        "sTooltipCallback",
         sourceContent->OwnerDoc()->EventTargetFor(TaskCategory::Other));
-    if (mTooltipTimer) {
-      mTargetNode = do_GetWeakReference(eventTarget);
-      if (mTargetNode) {
-        nsresult rv =
-          mTooltipTimer->InitWithNamedFuncCallback(sTooltipCallback, this,
-            LookAndFeel::GetInt(LookAndFeel::eIntID_TooltipDelay, 500),
-            nsITimer::TYPE_ONE_SHOT,
-            "sTooltipCallback");
-        if (NS_FAILED(rv)) {
-          mTargetNode = nullptr;
-          mSourceNode = nullptr;
-        }
+      if (NS_FAILED(rv)) {
+        mTargetNode = nullptr;
+        mSourceNode = nullptr;
       }
     }
     return;
   }
 
 #ifdef MOZ_XUL
   if (mIsSourceTree)
     return;
diff --git a/layout/xul/tree/nsTreeBodyFrame.cpp b/layout/xul/tree/nsTreeBodyFrame.cpp
--- a/layout/xul/tree/nsTreeBodyFrame.cpp
+++ b/layout/xul/tree/nsTreeBodyFrame.cpp
@@ -8,16 +8,17 @@
 #include "mozilla/ContentEvents.h"
 #include "mozilla/DebugOnly.h"
 #include "mozilla/EventDispatcher.h"
 #include "mozilla/gfx/2D.h"
 #include "mozilla/gfx/PathHelpers.h"
 #include "mozilla/Likely.h"
 #include "mozilla/MathAlgorithms.h"
 #include "mozilla/MouseEvents.h"
+#include "mozilla/ResultExtensions.h"
 #include "mozilla/TextEditRules.h"
 
 #include "gfxUtils.h"
 #include "nsAlgorithm.h"
 #include "nsCOMPtr.h"
 #include "nsFontMetrics.h"
 #include "nsPresContext.h"
 #include "nsNameSpaceManager.h"
@@ -1805,26 +1806,22 @@ nsTreeBodyFrame::CreateTimer(const LookA
   // Get the delay from the look and feel service.
   int32_t delay = LookAndFeel::GetInt(aID, 0);
 
   nsCOMPtr<nsITimer> timer;
 
   // Create a new timer only if the delay is greater than zero.
   // Zero value means that this feature is completely disabled.
   if (delay > 0) {
-    timer = do_CreateInstance("@mozilla.org/timer;1");
-    if (timer) {
-      timer->SetTarget(
-          mContent->OwnerDoc()->EventTargetFor(TaskCategory::Other));
-      timer->InitWithNamedFuncCallback(aFunc, this, delay, aType, aName);
-    }
+    MOZ_TRY_VAR(timer, NS_NewTimerWithFuncCallback(
+        aFunc, this, delay, aType, aName,
+        mContent->OwnerDoc()->EventTargetFor(TaskCategory::Other)));
   }
 
-  NS_IF_ADDREF(*aTimer = timer);
-
+  timer.forget(aTimer);
   return NS_OK;
 }
 
 nsresult
 nsTreeBodyFrame::RowCountChanged(int32_t aIndex, int32_t aCount)
 {
   if (aCount == 0 || !mView)
     return NS_OK; // Nothing to do.
diff --git a/layout/xul/tree/nsTreeSelection.cpp b/layout/xul/tree/nsTreeSelection.cpp
--- a/layout/xul/tree/nsTreeSelection.cpp
+++ b/layout/xul/tree/nsTreeSelection.cpp
@@ -325,25 +325,25 @@ NS_IMETHODIMP nsTreeSelection::TimedSele
     return rv;
 
   if (aMsec != -1) {
     mSuppressed = suppressSelect;
     if (!mSuppressed) {
       if (mSelectTimer)
         mSelectTimer->Cancel();
 
-      mSelectTimer = do_CreateInstance("@mozilla.org/timer;1");
-      nsCOMPtr<nsIContent> content = GetContent();
-      if (content) {
-        mSelectTimer->SetTarget(
-            content->OwnerDoc()->EventTargetFor(TaskCategory::Other));
+      nsIEventTarget* target = nullptr;
+      if (nsCOMPtr<nsIContent> content = GetContent()) {
+        target = content->OwnerDoc()->EventTargetFor(TaskCategory::Other);
       }
-      mSelectTimer->InitWithNamedFuncCallback(SelectCallback, this, aMsec,
-                                              nsITimer::TYPE_ONE_SHOT,
-                                              "nsTreeSelection::SelectCallback");
+      NS_NewTimerWithFuncCallback(getter_AddRefs(mSelectTimer),
+                                  SelectCallback, this, aMsec,
+                                  nsITimer::TYPE_ONE_SHOT,
+                                  "nsTreeSelection::SelectCallback",
+                                  target);
     }
   }
 
   return NS_OK;
 }
 
 NS_IMETHODIMP nsTreeSelection::Select(int32_t aIndex)
 {
