# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1508839361 -7200
#      Tue Oct 24 12:02:41 2017 +0200
# Node ID fcaf644bee467d65b25d416ef7f470a4857eb281
# Parent  65a67b54eec22d2500662ec228732fe5f3c1843e
Bug 1408333 Get rid of nsIIPCBackgroundChildCreateCallback - part 15 - ServiceWorkers, r=asuth

diff --git a/dom/workers/ServiceWorkerJob.cpp b/dom/workers/ServiceWorkerJob.cpp
--- a/dom/workers/ServiceWorkerJob.cpp
+++ b/dom/workers/ServiceWorkerJob.cpp
@@ -101,21 +101,16 @@ ServiceWorkerJob::Start(Callback* aFinal
   // We may have to wait for the PBackground actor to be initialized
   // before proceeding.  We should always be able to get a ServiceWorkerManager,
   // however, since Start() should not be called during shutdown.
   RefPtr<ServiceWorkerManager> swm = ServiceWorkerManager::GetInstance();
   if (!swm) {
     // browser shutdown
     return;
   }
-  if (!swm->HasBackgroundActor()) {
-    // waiting to initialize
-    swm->AppendPendingOperation(runnable);
-    return;
-  }
 
   // Otherwise start asynchronously.  We should never run a job synchronously.
   MOZ_ALWAYS_TRUE(NS_SUCCEEDED(
     NS_DispatchToMainThread(runnable.forget())));
 }
 
 void
 ServiceWorkerJob::Cancel()
diff --git a/dom/workers/ServiceWorkerManager.cpp b/dom/workers/ServiceWorkerManager.cpp
--- a/dom/workers/ServiceWorkerManager.cpp
+++ b/dom/workers/ServiceWorkerManager.cpp
@@ -246,17 +246,16 @@ private:
 // ServiceWorkerManager //
 //////////////////////////
 
 NS_IMPL_ADDREF(ServiceWorkerManager)
 NS_IMPL_RELEASE(ServiceWorkerManager)
 
 NS_INTERFACE_MAP_BEGIN(ServiceWorkerManager)
   NS_INTERFACE_MAP_ENTRY(nsIServiceWorkerManager)
-  NS_INTERFACE_MAP_ENTRY(nsIIPCBackgroundChildCreateCallback)
   NS_INTERFACE_MAP_ENTRY(nsIObserver)
   NS_INTERFACE_MAP_ENTRY_AMBIGUOUS(nsISupports, nsIServiceWorkerManager)
 NS_INTERFACE_MAP_END
 
 ServiceWorkerManager::ServiceWorkerManager()
   : mActor(nullptr)
   , mShuttingDown(false)
 {
@@ -292,21 +291,30 @@ ServiceWorkerManager::Init(ServiceWorker
       MOZ_ASSERT(NS_SUCCEEDED(rv));
       rv = obs->AddObserver(this, PURGE_DOMAIN_DATA, false /* ownsWeak */);
       MOZ_ASSERT(NS_SUCCEEDED(rv));
       rv = obs->AddObserver(this, CLEAR_ORIGIN_DATA, false /* ownsWeak */);
       MOZ_ASSERT(NS_SUCCEEDED(rv));
     }
   }
 
-  if (!BackgroundChild::GetOrCreateForCurrentThread(this)) {
-    // Make sure to do this last as our failure cleanup expects Init() to have
-    // executed.
-    ActorFailed();
-  }
+  PBackgroundChild* actorChild = BackgroundChild::GetOrCreateForCurrentThread();
+  if (NS_WARN_IF(!actorChild)) {
+    MaybeStartShutdown();
+    return;
+  }
+
+  PServiceWorkerManagerChild* actor =
+    actorChild->SendPServiceWorkerManagerConstructor();
+  if (!actor) {
+    MaybeStartShutdown();
+    return;
+  }
+
+  mActor = static_cast<ServiceWorkerManagerChild*>(actor);
 }
 
 void
 ServiceWorkerManager::MaybeStartShutdown()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   if (mShuttingDown) {
@@ -335,18 +343,16 @@ ServiceWorkerManager::MaybeStartShutdown
 
     if (XRE_IsParentProcess()) {
       obs->RemoveObserver(this, PURGE_SESSION_HISTORY);
       obs->RemoveObserver(this, PURGE_DOMAIN_DATA);
       obs->RemoveObserver(this, CLEAR_ORIGIN_DATA);
     }
   }
 
-  mPendingOperations.Clear();
-
   if (!mActor) {
     return;
   }
 
   mActor->ManagerShuttingDown();
 
   RefPtr<TeardownRunnable> runnable = new TeardownRunnable(mActor);
   nsresult rv = NS_DispatchToMainThread(runnable);
@@ -429,130 +435,16 @@ public:
 private:
   ~PropagateSoftUpdateRunnable()
   {}
 
   const OriginAttributes mOriginAttributes;
   const nsString mScope;
 };
 
-class PropagateUnregisterRunnable final : public Runnable
-{
-public:
-  PropagateUnregisterRunnable(nsIPrincipal* aPrincipal,
-                              nsIServiceWorkerUnregisterCallback* aCallback,
-                              const nsAString& aScope)
-    : Runnable("dom::workers::PropagateUnregisterRunnable")
-    , mPrincipal(aPrincipal)
-    , mCallback(aCallback)
-    , mScope(aScope)
-  {
-    MOZ_ASSERT(aPrincipal);
-  }
-
-  NS_IMETHOD Run() override
-  {
-    AssertIsOnMainThread();
-
-    RefPtr<ServiceWorkerManager> swm = ServiceWorkerManager::GetInstance();
-    if (swm) {
-      swm->PropagateUnregister(mPrincipal, mCallback, mScope);
-    }
-
-    return NS_OK;
-  }
-
-private:
-  ~PropagateUnregisterRunnable()
-  {}
-
-  nsCOMPtr<nsIPrincipal> mPrincipal;
-  nsCOMPtr<nsIServiceWorkerUnregisterCallback> mCallback;
-  const nsString mScope;
-};
-
-class RemoveRunnable final : public Runnable
-{
-public:
-  explicit RemoveRunnable(const nsACString& aHost)
-    : Runnable("dom::workers::RemoveRunnable")
-  {
-  }
-
-  NS_IMETHOD Run() override
-  {
-    AssertIsOnMainThread();
-
-    RefPtr<ServiceWorkerManager> swm = ServiceWorkerManager::GetInstance();
-    if (swm) {
-      swm->Remove(mHost);
-    }
-
-    return NS_OK;
-  }
-
-private:
-  ~RemoveRunnable()
-  {}
-
-  const nsCString mHost;
-};
-
-class PropagateRemoveRunnable final : public Runnable
-{
-public:
-  explicit PropagateRemoveRunnable(const nsACString& aHost)
-    : Runnable("dom::workers::PropagateRemoveRunnable")
-  {
-  }
-
-  NS_IMETHOD Run() override
-  {
-    AssertIsOnMainThread();
-
-    RefPtr<ServiceWorkerManager> swm = ServiceWorkerManager::GetInstance();
-    if (swm) {
-      swm->PropagateRemove(mHost);
-    }
-
-    return NS_OK;
-  }
-
-private:
-  ~PropagateRemoveRunnable()
-  {}
-
-  const nsCString mHost;
-};
-
-class PropagateRemoveAllRunnable final : public Runnable
-{
-public:
-  PropagateRemoveAllRunnable()
-    : Runnable("dom::workers::PropagateRemoveAllRunnable")
-  {
-  }
-
-  NS_IMETHOD Run() override
-  {
-    AssertIsOnMainThread();
-
-    RefPtr<ServiceWorkerManager> swm = ServiceWorkerManager::GetInstance();
-    if (swm) {
-      swm->PropagateRemoveAll();
-    }
-
-    return NS_OK;
-  }
-
-private:
-  ~PropagateRemoveAllRunnable()
-  {}
-};
-
 class PromiseResolverCallback final : public ServiceWorkerUpdateFinishCallback
 {
 public:
   PromiseResolverCallback(ServiceWorkerUpdateFinishCallback* aCallback,
                           GenericPromise::Private* aPromise)
     : mCallback(aCallback)
     , mPromise(aPromise)
   {
@@ -960,27 +852,16 @@ ServiceWorkerManager::Register(mozIDOMWi
 
   AssertIsOnMainThread();
   Telemetry::Accumulate(Telemetry::SERVICE_WORKER_REGISTRATIONS, 1);
 
   promise.forget(aPromise);
   return NS_OK;
 }
 
-void
-ServiceWorkerManager::AppendPendingOperation(nsIRunnable* aRunnable)
-{
-  MOZ_ASSERT(!mActor);
-  MOZ_ASSERT(aRunnable);
-
-  if (!mShuttingDown) {
-    mPendingOperations.AppendElement(aRunnable);
-  }
-}
-
 /*
  * Implements the async aspects of the getRegistrations algorithm.
  */
 class GetRegistrationsRunnable final : public Runnable
 {
   nsCOMPtr<nsPIDOMWindowInner> mWindow;
   RefPtr<Promise> mPromise;
 public:
@@ -2096,71 +1977,27 @@ ServiceWorkerManager::LoadRegistrations(
   AssertIsOnMainThread();
 
   for (uint32_t i = 0, len = aRegistrations.Length(); i < len; ++i) {
     LoadRegistration(aRegistrations[i]);
   }
 }
 
 void
-ServiceWorkerManager::ActorFailed()
-{
-  MOZ_DIAGNOSTIC_ASSERT(!mActor);
-  MaybeStartShutdown();
-}
-
-void
-ServiceWorkerManager::ActorCreated(mozilla::ipc::PBackgroundChild* aActor)
-{
-  MOZ_ASSERT(aActor);
-  MOZ_ASSERT(!mActor);
-
-  if (mShuttingDown) {
-    MOZ_DIAGNOSTIC_ASSERT(mPendingOperations.IsEmpty());
-    return;
-  }
-
-  PServiceWorkerManagerChild* actor =
-    aActor->SendPServiceWorkerManagerConstructor();
-  if (!actor) {
-    ActorFailed();
-    return;
-  }
-
-  mActor = static_cast<ServiceWorkerManagerChild*>(actor);
-
-  // Flush the pending requests.
-  for (uint32_t i = 0, len = mPendingOperations.Length(); i < len; ++i) {
-    MOZ_ASSERT(mPendingOperations[i]);
-    nsresult rv = NS_DispatchToCurrentThread(mPendingOperations[i].forget());
-    if (NS_FAILED(rv)) {
-      NS_WARNING("Failed to dispatch a runnable.");
-    }
-  }
-
-  mPendingOperations.Clear();
-}
-
-void
 ServiceWorkerManager::StoreRegistration(
                                    nsIPrincipal* aPrincipal,
                                    ServiceWorkerRegistrationInfo* aRegistration)
 {
   MOZ_ASSERT(aPrincipal);
   MOZ_ASSERT(aRegistration);
 
   if (mShuttingDown) {
     return;
   }
 
-  MOZ_DIAGNOSTIC_ASSERT(mActor);
-  if (!mActor) {
-    return;
-  }
-
   ServiceWorkerRegistrationData data;
   nsresult rv = PopulateRegistrationData(aPrincipal, aRegistration, data);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return;
   }
 
   PrincipalInfo principalInfo;
   if (NS_WARN_IF(NS_FAILED(PrincipalToPrincipalInfo(aPrincipal,
@@ -3011,23 +2848,16 @@ ServiceWorkerManager::SoftUpdate(const O
                                  const nsACString& aScope)
 {
   AssertIsOnMainThread();
 
   if (mShuttingDown) {
     return;
   }
 
-  if (!mActor) {
-    RefPtr<Runnable> runnable =
-      new SoftUpdateRunnable(aOriginAttributes, aScope, false, nullptr);
-    AppendPendingOperation(runnable);
-    return;
-  }
-
   RefPtr<GenericPromise::Private> promise =
     new GenericPromise::Private(__func__);
 
   RefPtr<CancelableRunnable> successRunnable =
     new SoftUpdateRunnable(aOriginAttributes, aScope, true, promise);
 
   RefPtr<CancelableRunnable> failureRunnable =
     new ResolvePromiseRunnable(promise);
@@ -3152,24 +2982,16 @@ ServiceWorkerManager::SoftUpdateInternal
 
 void
 ServiceWorkerManager::Update(nsIPrincipal* aPrincipal,
                              const nsACString& aScope,
                              ServiceWorkerUpdateFinishCallback* aCallback)
 {
   AssertIsOnMainThread();
 
-  if (!mActor) {
-    RefPtr<Runnable> runnable =
-      new UpdateRunnable(aPrincipal, aScope, aCallback,
-                         UpdateRunnable::ePostpone, nullptr);
-    AppendPendingOperation(runnable);
-    return;
-  }
-
   RefPtr<GenericPromise::Private> promise =
     new GenericPromise::Private(__func__);
 
   RefPtr<CancelableRunnable> successRunnable =
     new UpdateRunnable(aPrincipal, aScope, aCallback,
                        UpdateRunnable::eSuccess, promise);
 
   RefPtr<CancelableRunnable> failureRunnable =
@@ -3735,24 +3557,16 @@ ServiceWorkerManager::RemoveAndPropagate
   return NS_OK;
 }
 
 void
 ServiceWorkerManager::Remove(const nsACString& aHost)
 {
   AssertIsOnMainThread();
 
-  // We need to postpone this operation in case we don't have an actor because
-  // this is needed by the ForceUnregister.
-  if (!mActor) {
-    RefPtr<nsIRunnable> runnable = new RemoveRunnable(aHost);
-    AppendPendingOperation(runnable);
-    return;
-  }
-
   for (auto it1 = mRegistrationInfos.Iter(); !it1.Done(); it1.Next()) {
     ServiceWorkerManager::RegistrationDataPerPrincipal* data = it1.UserData();
     for (auto it2 = data->mInfos.Iter(); !it2.Done(); it2.Next()) {
       ServiceWorkerRegistrationInfo* reg = it2.UserData();
       nsCOMPtr<nsIURI> scopeURI;
       nsresult rv = NS_NewURI(getter_AddRefs(scopeURI), it2.Key(),
                               nullptr, nullptr);
       // This way subdomains are also cleared.
@@ -3762,23 +3576,16 @@ ServiceWorkerManager::Remove(const nsACS
     }
   }
 }
 
 void
 ServiceWorkerManager::PropagateRemove(const nsACString& aHost)
 {
   AssertIsOnMainThread();
-
-  if (!mActor) {
-    RefPtr<nsIRunnable> runnable = new PropagateRemoveRunnable(aHost);
-    AppendPendingOperation(runnable);
-    return;
-  }
-
   mActor->SendPropagateRemove(nsCString(aHost));
 }
 
 void
 ServiceWorkerManager::RemoveAll()
 {
   AssertIsOnMainThread();
 
@@ -3791,23 +3598,16 @@ ServiceWorkerManager::RemoveAll()
   }
 }
 
 void
 ServiceWorkerManager::PropagateRemoveAll()
 {
   AssertIsOnMainThread();
   MOZ_ASSERT(XRE_IsParentProcess());
-
-  if (!mActor) {
-    RefPtr<nsIRunnable> runnable = new PropagateRemoveAllRunnable();
-    AppendPendingOperation(runnable);
-    return;
-  }
-
   mActor->SendPropagateRemoveAll();
 }
 
 void
 ServiceWorkerManager::RemoveAllRegistrations(OriginAttributesPattern* aPattern)
 {
   AssertIsOnMainThread();
 
@@ -4035,42 +3835,27 @@ ServiceWorkerManager::PropagateSoftUpdat
   return NS_OK;
 }
 
 void
 ServiceWorkerManager::PropagateSoftUpdate(const OriginAttributes& aOriginAttributes,
                                           const nsAString& aScope)
 {
   AssertIsOnMainThread();
-
-  if (!mActor) {
-    RefPtr<nsIRunnable> runnable =
-      new PropagateSoftUpdateRunnable(aOriginAttributes, aScope);
-    AppendPendingOperation(runnable);
-    return;
-  }
-
   mActor->SendPropagateSoftUpdate(aOriginAttributes, nsString(aScope));
 }
 
 NS_IMETHODIMP
 ServiceWorkerManager::PropagateUnregister(nsIPrincipal* aPrincipal,
                                           nsIServiceWorkerUnregisterCallback* aCallback,
                                           const nsAString& aScope)
 {
   AssertIsOnMainThread();
   MOZ_ASSERT(aPrincipal);
 
-  if (!mActor) {
-    RefPtr<nsIRunnable> runnable =
-      new PropagateUnregisterRunnable(aPrincipal, aCallback, aScope);
-    AppendPendingOperation(runnable);
-    return NS_OK;
-  }
-
   PrincipalInfo principalInfo;
   if (NS_WARN_IF(NS_FAILED(PrincipalToPrincipalInfo(aPrincipal,
                                                     &principalInfo)))) {
     return NS_ERROR_FAILURE;
   }
 
   mActor->SendPropagateUnregister(principalInfo, nsString(aScope));
 
diff --git a/dom/workers/ServiceWorkerManager.h b/dom/workers/ServiceWorkerManager.h
--- a/dom/workers/ServiceWorkerManager.h
+++ b/dom/workers/ServiceWorkerManager.h
@@ -23,17 +23,16 @@
 #include "mozilla/dom/Promise.h"
 #include "mozilla/dom/ServiceWorkerCommon.h"
 #include "mozilla/dom/ServiceWorkerRegistrar.h"
 #include "mozilla/dom/ServiceWorkerRegistrarTypes.h"
 #include "mozilla/dom/workers/ServiceWorkerRegistrationInfo.h"
 #include "mozilla/ipc/BackgroundUtils.h"
 #include "nsClassHashtable.h"
 #include "nsDataHashtable.h"
-#include "nsIIPCBackgroundChildCreateCallback.h"
 #include "nsRefPtrHashtable.h"
 #include "nsTArrayForwardDeclare.h"
 #include "nsTObserverArray.h"
 
 class nsIConsoleReportCollector;
 
 namespace mozilla {
 
@@ -78,32 +77,30 @@ public:
 
 /*
  * The ServiceWorkerManager is a per-process global that deals with the
  * installation, querying and event dispatch of ServiceWorkers for all the
  * origins in the process.
  */
 class ServiceWorkerManager final
   : public nsIServiceWorkerManager
-  , public nsIIPCBackgroundChildCreateCallback
   , public nsIObserver
 {
   friend class GetReadyPromiseRunnable;
   friend class GetRegistrationsRunnable;
   friend class GetRegistrationRunnable;
   friend class ServiceWorkerJob;
   friend class ServiceWorkerRegistrationInfo;
   friend class ServiceWorkerUnregisterJob;
   friend class ServiceWorkerUpdateJob;
   friend class UpdateTimerCallback;
 
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSISERVICEWORKERMANAGER
-  NS_DECL_NSIIPCBACKGROUNDCHILDCREATECALLBACK
   NS_DECL_NSIOBSERVER
 
   struct RegistrationDataPerPrincipal;
   nsClassHashtable<nsCStringHashKey, RegistrationDataPerPrincipal> mRegistrationInfos;
 
   nsTObserverArray<ServiceWorkerRegistrationListener*> mServiceWorkerRegistrationListeners;
 
   nsRefPtrHashtable<nsISupportsHashKey, ServiceWorkerRegistrationInfo> mControlledDocuments;
@@ -458,36 +455,27 @@ private:
     PendingReadyPromise(nsIURI* aURI, Promise* aPromise)
       : mURI(aURI), mPromise(aPromise)
     {}
 
     nsCOMPtr<nsIURI> mURI;
     RefPtr<Promise> mPromise;
   };
 
-  void AppendPendingOperation(nsIRunnable* aRunnable);
-
-  bool HasBackgroundActor() const
-  {
-    return !!mActor;
-  }
-
   nsClassHashtable<nsISupportsHashKey, PendingReadyPromise> mPendingReadyPromises;
 
   void
   MaybeRemoveRegistration(ServiceWorkerRegistrationInfo* aRegistration);
 
   // Removes all service worker registrations that matches the given pattern.
   void
   RemoveAllRegistrations(OriginAttributesPattern* aPattern);
 
   RefPtr<ServiceWorkerManagerChild> mActor;
 
-  nsTArray<nsCOMPtr<nsIRunnable>> mPendingOperations;
-
   bool mShuttingDown;
 
   nsTArray<nsCOMPtr<nsIServiceWorkerManagerListener>> mListeners;
 
   void
   NotifyListenersOnRegister(nsIServiceWorkerRegistrationInfo* aRegistration);
 
   void
