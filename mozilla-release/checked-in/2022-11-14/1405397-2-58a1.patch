# HG changeset patch
# User Timothy Nikkel <tnikkel@gmail.com>
# Date 1507845981 18000
# Node ID d6fdc1d3b07044a6cd84adcc433d3f6a943dca20
# Parent  ad6b024e3773df11e20091b656f8be61861fa278
Bug 1405397. Part 2. ScrollFrameHelper::BuildDisplayList should only have one way to determine if we are using a displayport/building a async scrollable layer. r=mstange

The variable usingDisplayPort was determining if we applied a clip to the displayport (and one other thing), and otherwise mWillBuildScrollableLayer was used to determine those type of things.

When https://hg.mozilla.org/mozilla-central/rev/4c8b85e80aeb of bug 1364295 landed it actually changed behaviour even though it was only supposed to simplify code. Before that changeset mWillBuildScrollableLayer was always set to false if we weren't painting to the window because we never considered whether a displayport was set. After that changeset we actually looked to see if a displayport was set and set mWillBuildScrollableLayer to true if we had a displayport even when we weren't painting to the window.

So we would have usingDisplayPort == false, and mWillBuildScrollableLayer == true. We fix that be getting rid of usingDisplayPort and using everywhere.

This means that after this patch and bug 1364295 we will build display lists for event handling with mWillBuildScrollableLayer == true where we had it false before. So another patch we could make is to make all uses of mWillBuildScrollableLayer also check if we are painting to the window.

The decision to expand the dirty rect to the displayport is still restricted to when we are painting to the window and happens in DecideScrollableLayer, so we don't regress bug 745936.

diff --git a/layout/generic/nsGfxScrollFrame.cpp b/layout/generic/nsGfxScrollFrame.cpp
--- a/layout/generic/nsGfxScrollFrame.cpp
+++ b/layout/generic/nsGfxScrollFrame.cpp
@@ -3296,19 +3296,16 @@ ScrollFrameHelper::BuildDisplayList(nsDi
   nsRect dirtyRect = aBuilder->GetDirtyRect();
   if (!ignoringThisScrollFrame) {
     dirtyRect = dirtyRect.Intersect(mScrollPort);
   }
 
   Unused << DecideScrollableLayer(aBuilder, &dirtyRect,
               /* aSetBase = */ !mIsRoot);
 
-  bool usingDisplayPort = aBuilder->IsPaintingToWindow() &&
-    nsLayoutUtils::HasDisplayPort(mOuter->GetContent());
-
   if (aBuilder->IsForFrameVisibility()) {
     // We expand the dirty rect to catch frames just outside of the scroll port.
     // We use the dirty rect instead of the whole scroll port to prevent
     // too much expansion in the presence of very large (bigger than the
     // viewport) scroll ports.
     dirtyRect = ExpandRectToNearlyVisible(dirtyRect);
   }
 
@@ -3329,17 +3326,17 @@ ScrollFrameHelper::BuildDisplayList(nsDi
   if (ignoringThisScrollFrame) {
     // Root scrollframes have FrameMetrics and clipping on their container
     // layers, so don't apply clipping again.
     mAddClipRectToLayer = false;
 
     // If we are a root scroll frame that has a display port we want to add
     // scrollbars, they will be children of the scrollable layer, but they get
     // adjusted by the APZC automatically.
-    bool addScrollBars = mIsRoot && usingDisplayPort && aBuilder->IsPaintingToWindow();
+    bool addScrollBars = mIsRoot && mWillBuildScrollableLayer && aBuilder->IsPaintingToWindow();
 
     if (addScrollBars) {
       // Add classic scrollbars.
       AppendScrollPartsTo(aBuilder, aLists, createLayersForScrollbars, false);
     }
 
     {
       nsDisplayListBuilder::AutoCurrentActiveScrolledRootSetter asrSetter(aBuilder);
@@ -3517,17 +3514,17 @@ ScrollFrameHelper::BuildDisplayList(nsDi
     {
       // Clip our contents to the unsnapped scrolled rect. This makes sure that
       // we don't have display items over the subpixel seam at the edge of the
       // scrolled area.
       DisplayListClipState::AutoSaveRestore scrolledRectClipState(aBuilder);
       nsRect scrolledRectClip =
         GetUnsnappedScrolledRectInternal(mScrolledFrame->GetScrollableOverflowRect(),
                                          mScrollPort.Size()) + mScrolledFrame->GetPosition();
-      if (usingDisplayPort) {
+      if (mWillBuildScrollableLayer) {
         // Clip the contents to the display port.
         // The dirty rect already acts kind of like a clip, in that
         // FrameLayerBuilder intersects item bounds and opaque regions with
         // it, but it doesn't have the consistent snapping behavior of a
         // true clip.
         // For a case where this makes a difference, imagine the following
         // scenario: The display port has an edge that falls on a fractional
         // layer pixel, and there's an opaque display item that covers the
