# HG changeset patch
# User Yoshi Huang <allstars.chh@mozilla.com>
# Date 1505990538 -28800
#      Thu Sep 21 18:42:18 2017 +0800
# Node ID 9180e59a44f78fca784b8fc6dbadccf2bc300feb
# Parent  4294469d07c3a3df3f6059c094684843a7f7f8c0
Bug 1399399 - Move internal classes out of RootingAPI.h. r=jonco

diff --git a/js/public/RootingAPI.h b/js/public/RootingAPI.h
--- a/js/public/RootingAPI.h
+++ b/js/public/RootingAPI.h
@@ -136,16 +136,22 @@ template <typename T> struct IsHeapConst
     template <> struct IsHeapConstructibleType<T> { static constexpr bool value = true; };
 FOR_EACH_PUBLIC_GC_POINTER_TYPE(DECLARE_IS_HEAP_CONSTRUCTIBLE_TYPE)
 FOR_EACH_PUBLIC_TAGGED_GC_POINTER_TYPE(DECLARE_IS_HEAP_CONSTRUCTIBLE_TYPE)
 #undef DECLARE_IS_HEAP_CONSTRUCTIBLE_TYPE
 
 template <typename T, typename Wrapper>
 class PersistentRootedBase : public MutableWrappedPtrOperations<T, Wrapper> {};
 
+template <typename T>
+class FakeRooted;
+
+template <typename T>
+class FakeMutableHandle;
+
 namespace gc {
 struct Cell;
 template<typename T>
 struct PersistentRootedMarker;
 } /* namespace gc */
 
 // Important: Return a reference so passing a Rooted<T>, etc. to
 // something that takes a |const T&| is not a GC hazard.
@@ -1038,74 +1044,16 @@ class RootedBase<JSObject*, Container> :
 template <typename Container>
 class HandleBase<JSObject*, Container> : public WrappedPtrOperations<JSObject*, Container>
 {
   public:
     template <class U>
     JS::Handle<U*> as() const;
 };
 
-/** Interface substitute for Rooted<T> which does not root the variable's memory. */
-template <typename T>
-class MOZ_RAII FakeRooted : public RootedBase<T, FakeRooted<T>>
-{
-  public:
-    using ElementType = T;
-
-    template <typename CX>
-    explicit FakeRooted(CX* cx) : ptr(JS::GCPolicy<T>::initial()) {}
-
-    template <typename CX>
-    FakeRooted(CX* cx, T initial) : ptr(initial) {}
-
-    DECLARE_POINTER_CONSTREF_OPS(T);
-    DECLARE_POINTER_ASSIGN_OPS(FakeRooted, T);
-    DECLARE_NONPOINTER_ACCESSOR_METHODS(ptr);
-    DECLARE_NONPOINTER_MUTABLE_ACCESSOR_METHODS(ptr);
-
-  private:
-    T ptr;
-
-    void set(const T& value) {
-        ptr = value;
-    }
-
-    FakeRooted(const FakeRooted&) = delete;
-};
-
-/** Interface substitute for MutableHandle<T> which is not required to point to rooted memory. */
-template <typename T>
-class FakeMutableHandle : public js::MutableHandleBase<T, FakeMutableHandle<T>>
-{
-  public:
-    using ElementType = T;
-
-    MOZ_IMPLICIT FakeMutableHandle(T* t) {
-        ptr = t;
-    }
-
-    MOZ_IMPLICIT FakeMutableHandle(FakeRooted<T>* root) {
-        ptr = root->address();
-    }
-
-    void set(const T& v) {
-        *ptr = v;
-    }
-
-    DECLARE_POINTER_CONSTREF_OPS(T);
-    DECLARE_NONPOINTER_ACCESSOR_METHODS(*ptr);
-    DECLARE_NONPOINTER_MUTABLE_ACCESSOR_METHODS(*ptr);
-
-  private:
-    FakeMutableHandle() {}
-    DELETE_ASSIGNMENT_OPS(FakeMutableHandle, T);
-
-    T* ptr;
-};
-
 /**
  * Types for a variable that either should or shouldn't be rooted, depending on
  * the template parameter allowGC. Used for implementing functions that can
  * operate on either rooted or unrooted data.
  *
  * The toHandle() and toMutableHandle() functions are for calling functions
  * which require handle types and are only called in the CanGC case. These
  * allow the calling code to type check.
@@ -1135,37 +1083,16 @@ template <typename T> class MaybeRooted<
     }
 
     template <typename T2>
     static inline JS::Handle<T2*> downcastHandle(HandleType v) {
         return v.template as<T2>();
     }
 };
 
-template <typename T> class MaybeRooted<T, NoGC>
-{
-  public:
-    typedef const T& HandleType;
-    typedef FakeRooted<T> RootType;
-    typedef FakeMutableHandle<T> MutableHandleType;
-
-    static JS::Handle<T> toHandle(HandleType v) {
-        MOZ_CRASH("Bad conversion");
-    }
-
-    static JS::MutableHandle<T> toMutableHandle(MutableHandleType v) {
-        MOZ_CRASH("Bad conversion");
-    }
-
-    template <typename T2>
-    static inline T2* downcastHandle(HandleType v) {
-        return &v->template as<T2>();
-    }
-};
-
 } /* namespace js */
 
 namespace JS {
 
 template <typename T> template <typename S>
 inline
 Handle<T>::Handle(const Rooted<S>& root,
                   typename mozilla::EnableIf<mozilla::IsConvertible<S, T>::value, int>::Type dummy)
@@ -1661,11 +1588,9 @@ operator==(const T& a, std::nullptr_t b)
 
 template <typename T>
 typename mozilla::EnableIf<js::detail::DefineComparisonOps<T>::value &&
                            mozilla::IsPointer<typename T::ElementType>::value, bool>::Type
 operator!=(const T& a, std::nullptr_t b) {
     return !(a == b);
 }
 
-#undef DELETE_ASSIGNMENT_OPS
-
 #endif  /* js_RootingAPI_h */
diff --git a/js/src/gc/Rooting.h b/js/src/gc/Rooting.h
--- a/js/src/gc/Rooting.h
+++ b/js/src/gc/Rooting.h
@@ -76,11 +76,90 @@ typedef JS::Rooted<DebuggerFrame*>      
 typedef JS::Rooted<DebuggerObject*>         RootedDebuggerObject;
 typedef JS::Rooted<Scope*>                  RootedScope;
 
 typedef JS::GCVector<JSFunction*>   FunctionVector;
 typedef JS::GCVector<PropertyName*> PropertyNameVector;
 typedef JS::GCVector<Shape*>        ShapeVector;
 typedef JS::GCVector<JSString*>     StringVector;
 
+/** Interface substitute for Rooted<T> which does not root the variable's memory. */
+template <typename T>
+class MOZ_RAII FakeRooted : public RootedBase<T, FakeRooted<T>>
+{
+  public:
+    using ElementType = T;
+
+    template <typename CX>
+    explicit FakeRooted(CX* cx) : ptr(JS::GCPolicy<T>::initial()) {}
+
+    template <typename CX>
+    FakeRooted(CX* cx, T initial) : ptr(initial) {}
+
+    DECLARE_POINTER_CONSTREF_OPS(T);
+    DECLARE_POINTER_ASSIGN_OPS(FakeRooted, T);
+    DECLARE_NONPOINTER_ACCESSOR_METHODS(ptr);
+    DECLARE_NONPOINTER_MUTABLE_ACCESSOR_METHODS(ptr);
+
+  private:
+    T ptr;
+
+    void set(const T& value) {
+        ptr = value;
+    }
+
+    FakeRooted(const FakeRooted&) = delete;
+};
+
+/** Interface substitute for MutableHandle<T> which is not required to point to rooted memory. */
+template <typename T>
+class FakeMutableHandle : public js::MutableHandleBase<T, FakeMutableHandle<T>>
+{
+  public:
+    using ElementType = T;
+
+    MOZ_IMPLICIT FakeMutableHandle(T* t) {
+        ptr = t;
+    }
+
+    MOZ_IMPLICIT FakeMutableHandle(FakeRooted<T>* root) {
+        ptr = root->address();
+    }
+
+    void set(const T& v) {
+        *ptr = v;
+    }
+
+    DECLARE_POINTER_CONSTREF_OPS(T);
+    DECLARE_NONPOINTER_ACCESSOR_METHODS(*ptr);
+    DECLARE_NONPOINTER_MUTABLE_ACCESSOR_METHODS(*ptr);
+
+  private:
+    FakeMutableHandle() {}
+    DELETE_ASSIGNMENT_OPS(FakeMutableHandle, T);
+
+    T* ptr;
+};
+
+template <typename T> class MaybeRooted<T, NoGC>
+{
+  public:
+    typedef const T& HandleType;
+    typedef FakeRooted<T> RootType;
+    typedef FakeMutableHandle<T> MutableHandleType;
+
+    static JS::Handle<T> toHandle(HandleType v) {
+        MOZ_CRASH("Bad conversion");
+    }
+
+    static JS::MutableHandle<T> toMutableHandle(MutableHandleType v) {
+        MOZ_CRASH("Bad conversion");
+    }
+
+    template <typename T2>
+    static inline T2* downcastHandle(HandleType v) {
+        return &v->template as<T2>();
+    }
+};
+
 } /* namespace js */
 
 #endif /* gc_Rooting_h */
