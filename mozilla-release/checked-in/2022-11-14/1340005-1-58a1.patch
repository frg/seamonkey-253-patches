# HG changeset patch
# User Boris Chiou <boris.chiou@gmail.com>
# Date 1509131092 -7200
# Node ID 86ef7d8d75e43fa1da734e462573466ed2643e98
# Parent  744d71fc9a45a1f4a827cad66aa3d7d6cc16cb14
Bug 1340005 - Part 1: Implement AnimationValue::Opacity. r=birtles

We will use Servo backend on the compositor, so implement this for opacity.

MozReview-Commit-ID: BeWR2nBSbjb

diff --git a/gfx/layers/AnimationHelper.cpp b/gfx/layers/AnimationHelper.cpp
--- a/gfx/layers/AnimationHelper.cpp
+++ b/gfx/layers/AnimationHelper.cpp
@@ -458,32 +458,33 @@ CreateCSSValueList(const InfallibleTArra
   }
   if (aFunctions.Length() == 0) {
     result = new nsCSSValueList();
     result->mValue.SetNoneValue();
   }
   return new nsCSSValueSharedList(result.forget());
 }
 
-static StyleAnimationValue
-ToStyleAnimationValue(const Animatable& aAnimatable)
+static AnimationValue
+ToAnimationValue(const Animatable& aAnimatable)
 {
-  StyleAnimationValue result;
+  AnimationValue result;
 
   switch (aAnimatable.type()) {
     case Animatable::Tnull_t:
       break;
     case Animatable::TArrayOfTransformFunction: {
-      const InfallibleTArray<TransformFunction>& transforms =
-        aAnimatable.get_ArrayOfTransformFunction();
-      result.SetTransformValue(CreateCSSValueList(transforms));
+        const InfallibleTArray<TransformFunction>& transforms =
+          aAnimatable.get_ArrayOfTransformFunction();
+        result.mGecko.SetTransformValue(CreateCSSValueList(transforms));
+      }
       break;
-    }
     case Animatable::Tfloat:
-      result.SetFloatValue(aAnimatable.get_float());
+      result = AnimationValue::Opacity(StyleBackendType::Gecko,
+                                       aAnimatable.get_float());
       break;
     default:
       MOZ_ASSERT_UNREACHABLE("Unsupported type");
   }
 
   return result;
 }
 
@@ -504,29 +505,29 @@ AnimationHelper::SetAnimations(Animation
       case dom::FillMode::Backwards:
         animation.fillMode() = static_cast<uint8_t>(dom::FillMode::Both);
         break;
       default:
         break;
     }
 
     if (animation.baseStyle().type() != Animatable::Tnull_t) {
-      aBaseAnimationStyle = ToStyleAnimationValue(animation.baseStyle());
+      aBaseAnimationStyle = ToAnimationValue(animation.baseStyle()).mGecko;
     }
 
     AnimData* data = aAnimData.AppendElement();
     InfallibleTArray<Maybe<ComputedTimingFunction>>& functions =
       data->mFunctions;
     InfallibleTArray<StyleAnimationValue>& startValues = data->mStartValues;
     InfallibleTArray<StyleAnimationValue>& endValues = data->mEndValues;
 
     const InfallibleTArray<AnimationSegment>& segments = animation.segments();
     for (const AnimationSegment& segment : segments) {
-      startValues.AppendElement(ToStyleAnimationValue(segment.startState()));
-      endValues.AppendElement(ToStyleAnimationValue(segment.endState()));
+      startValues.AppendElement(ToAnimationValue(segment.startState()).mGecko);
+      endValues.AppendElement(ToAnimationValue(segment.endState()).mGecko);
 
       TimingFunction tf = segment.sampleFn();
       Maybe<ComputedTimingFunction> ctf =
         AnimationUtils::TimingFunctionToComputedTimingFunction(tf);
       functions.AppendElement(ctf);
     }
   }
 }
diff --git a/layout/style/ServoBindingList.h b/layout/style/ServoBindingList.h
--- a/layout/style/ServoBindingList.h
+++ b/layout/style/ServoBindingList.h
@@ -358,16 +358,19 @@ SERVO_BINDING_FUNC(Servo_AnimationValue_
                    nsCSSPropertyID property,
                    nsAString* buffer)
 SERVO_BINDING_FUNC(Servo_Shorthand_AnimationValues_Serialize, void,
                    nsCSSPropertyID shorthand_property,
                    RawGeckoServoAnimationValueListBorrowed values,
                    nsAString* buffer)
 SERVO_BINDING_FUNC(Servo_AnimationValue_GetOpacity, float,
                    RawServoAnimationValueBorrowed value)
+SERVO_BINDING_FUNC(Servo_AnimationValue_Opacity,
+                   RawServoAnimationValueStrong,
+                   float)
 SERVO_BINDING_FUNC(Servo_AnimationValue_GetTransform, void,
                    RawServoAnimationValueBorrowed value,
                    RefPtr<nsCSSValueSharedList>* list)
 SERVO_BINDING_FUNC(Servo_AnimationValue_DeepEqual, bool,
                    RawServoAnimationValueBorrowed,
                    RawServoAnimationValueBorrowed)
 SERVO_BINDING_FUNC(Servo_AnimationValue_Uncompute,
                    RawServoDeclarationBlockStrong,
diff --git a/layout/style/StyleAnimationValue.cpp b/layout/style/StyleAnimationValue.cpp
--- a/layout/style/StyleAnimationValue.cpp
+++ b/layout/style/StyleAnimationValue.cpp
@@ -5488,8 +5488,26 @@ AnimationValue::FromString(nsCSSProperty
   if (!StyleAnimationValue::ComputeValue(aProperty, aElement,
                                          styleContext->AsGecko(),
                                          aValue, false /* |aUseSVGMode| */,
                                          result.mGecko)) {
     MOZ_ASSERT(result.IsNull());
   }
   return result;
 }
+
+/* static */ AnimationValue
+AnimationValue::Opacity(StyleBackendType aBackendType, float aOpacity)
+{
+  AnimationValue result;
+
+  switch (aBackendType) {
+    case StyleBackendType::Servo:
+      result.mServo = Servo_AnimationValue_Opacity(aOpacity).Consume();
+      break;
+    case StyleBackendType::Gecko:
+      result.mGecko.SetFloatValue(aOpacity);
+      break;
+    default:
+      MOZ_ASSERT_UNREACHABLE("Unsupported style backend");
+  }
+  return result;
+}
diff --git a/layout/style/StyleAnimationValue.h b/layout/style/StyleAnimationValue.h
--- a/layout/style/StyleAnimationValue.h
+++ b/layout/style/StyleAnimationValue.h
@@ -33,16 +33,17 @@ namespace css {
 class StyleRule;
 } // namespace css
 
 namespace dom {
 class Element;
 } // namespace dom
 
 enum class CSSPseudoElementType : uint8_t;
+enum class StyleBackendType : uint8_t;
 struct PropertyStyleAnimationValuePair;
 
 /**
  * Utility class to handle animated style values
  */
 class StyleAnimationValue {
 public:
   // Mathematical methods
@@ -630,16 +631,19 @@ struct AnimationValue
 
   // Create an AnimaitonValue from a string. This method flushes style, so we
   // should use this carefully. Now, it is only used by
   // nsDOMWindowUtils::ComputeAnimationDistance.
   static AnimationValue FromString(nsCSSPropertyID aProperty,
                                    const nsAString& aValue,
                                    dom::Element* aElement);
 
+  // Create an AnimationValue from an opacity value.
+  static AnimationValue Opacity(StyleBackendType aBackendType, float aOpacity);
+
   // mGecko and mServo are mutually exclusive: only one or the other should
   // ever be set.
   // FIXME: After obsoleting StyleAnimationValue, we should remove mGecko, and
   // make AnimationValue a wrapper of RawServoAnimationValue to hide these
   // FFIs.
   StyleAnimationValue mGecko;
   RefPtr<RawServoAnimationValue> mServo;
 };
