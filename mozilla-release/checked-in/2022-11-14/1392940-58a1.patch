# HG changeset patch
# User Ray Lin <ralin@mozilla.com>
# Date 1504516687 -28800
# Node ID 3c25526c75878fafc2ead1741e3957fa32258a91
# Parent  abf6841928b0c423db2ed240e44661585c17a2c9
Bug 1392940 - Add expiration date parser to detect cc-exp family fields. r=MattN,seanlee

MozReview-Commit-ID: 3HB93QAdCXw

diff --git a/browser/extensions/formautofill/FormAutofillHeuristics.jsm b/browser/extensions/formautofill/FormAutofillHeuristics.jsm
--- a/browser/extensions/formautofill/FormAutofillHeuristics.jsm
+++ b/browser/extensions/formautofill/FormAutofillHeuristics.jsm
@@ -55,17 +55,17 @@ class FieldScanner {
   /**
    * Move the parsingIndex to the next elements. Any elements behind this index
    * means the parsing tasks are finished.
    *
    * @param {number} index
    *        The latest index of elements waiting for parsing.
    */
   set parsingIndex(index) {
-    if (index > this.fieldDetails.length) {
+    if (index > this._elements.length) {
       throw new Error("The parsing index is out of range.");
     }
     this._parsingIndex = index;
   }
 
   /**
    * Retrieve the field detail by the index. If the field detail is not ready,
    * the elements will be traversed until matching the index.
@@ -282,16 +282,80 @@ var LabelUtils = {
 
 /**
  * Returns the autocomplete information of fields according to heuristics.
  */
 this.FormAutofillHeuristics = {
   RULES: null,
 
   /**
+   * Try to find a contiguous sub-array within an array.
+   *
+   * @param {Array} array
+   * @param {Array} subArray
+   *
+   * @returns {boolean}
+   *          Return whether subArray was found within the array or not.
+   */
+  _matchContiguousSubArray(array, subArray) {
+    return array.some((elm, i) => subArray.every((sElem, j) => sElem == array[i + j]));
+  },
+
+  /**
+   * Try to find the field that is look like a month select.
+   *
+   * @param {DOMElement} element
+   * @returns {boolean}
+   *          Return true if we observe the trait of month select in
+   *          the current element.
+   */
+  _isExpirationMonthLikely(element) {
+    if (!(element instanceof Ci.nsIDOMHTMLSelectElement)) {
+      return false;
+    }
+
+    const options = [...element.options];
+    const desiredValues = Array(12).fill(1).map((v, i) => v + i);
+
+    // The number of month options shouldn't be less than 12 or larger than 13
+    // including the default option.
+    if (options.length < 12 || options.length > 13) {
+      return false;
+    }
+
+    return this._matchContiguousSubArray(options.map(e => +e.value), desiredValues) ||
+           this._matchContiguousSubArray(options.map(e => +e.label), desiredValues);
+  },
+
+
+  /**
+   * Try to find the field that is look like a year select.
+   *
+   * @param {DOMElement} element
+   * @returns {boolean}
+   *          Return true if we observe the trait of year select in
+   *          the current element.
+   */
+  _isExpirationYearLikely(element) {
+    if (!(element instanceof Ci.nsIDOMHTMLSelectElement)) {
+      return false;
+    }
+
+    const options = [...element.options];
+    // A normal expiration year select should contain at least the last three years
+    // in the list.
+    const curYear = new Date().getFullYear();
+    const desiredValues = Array(3).fill(0).map((v, i) => v + curYear + i);
+
+    return this._matchContiguousSubArray(options.map(e => +e.value), desiredValues) ||
+           this._matchContiguousSubArray(options.map(e => +e.label), desiredValues);
+  },
+
+
+  /**
    * Try to match the telephone related fields to the grammar
    * list to see if there is any valid telephone set and correct their
    * field names.
    *
    * @param {FieldScanner} fieldScanner
    *        The current parsing status for all elements
    * @returns {boolean}
    *          Return true if there is any field can be recognized in the parser,
@@ -386,16 +450,82 @@ this.FormAutofillHeuristics = {
       fieldScanner.parsingIndex++;
       parsedFields = true;
     }
 
     return parsedFields;
   },
 
   /**
+   * Try to look for expiration date fields and revise the field names if needed.
+   *
+   * @param {FieldScanner} fieldScanner
+   *        The current parsing status for all elements
+   * @returns {boolean}
+   *          Return true if there is any field can be recognized in the parser,
+   *          otherwise false.
+   */
+  _parseCreditCardExpirationDateFields(fieldScanner) {
+    if (fieldScanner.parsingFinished) {
+      return false;
+    }
+
+    const savedIndex = fieldScanner.parsingIndex;
+    const monthAndYearFieldNames = ["cc-exp-month", "cc-exp-year"];
+    const detail = fieldScanner.getFieldDetailByIndex(fieldScanner.parsingIndex);
+    const element = detail.elementWeakRef.get();
+
+    // Skip the uninteresting fields
+    if (!detail || !["cc-exp", ...monthAndYearFieldNames].includes(detail.fieldName)) {
+      return false;
+    }
+
+    // If the input type is a month picker, then assume it's cc-exp.
+    if (element.type == "month") {
+      fieldScanner.updateFieldName(fieldScanner.parsingIndex, "cc-exp");
+      fieldScanner.parsingIndex++;
+
+      return true;
+    }
+
+    // Don't process the fields if expiration month and expiration year are already
+    // matched by regex in correct order.
+    if (fieldScanner.getFieldDetailByIndex(fieldScanner.parsingIndex++).fieldName == "cc-exp-month" &&
+        !fieldScanner.parsingFinished &&
+        fieldScanner.getFieldDetailByIndex(fieldScanner.parsingIndex++).fieldName == "cc-exp-year") {
+      return true;
+    }
+    fieldScanner.parsingIndex = savedIndex;
+
+    // Determine the field name by checking if the fields are month select and year select
+    // likely.
+    if (this._isExpirationMonthLikely(element)) {
+      fieldScanner.updateFieldName(fieldScanner.parsingIndex, "cc-exp-month");
+      fieldScanner.parsingIndex++;
+      const nextDetail = fieldScanner.getFieldDetailByIndex(fieldScanner.parsingIndex);
+      const nextElement = nextDetail.elementWeakRef.get();
+      if (this._isExpirationYearLikely(nextElement) && !fieldScanner.parsingFinished) {
+        fieldScanner.updateFieldName(fieldScanner.parsingIndex, "cc-exp-year");
+        fieldScanner.parsingIndex++;
+
+        return true;
+      }
+    }
+    fieldScanner.parsingIndex = savedIndex;
+
+    // If no possible regular expiration fields are detected in current parsing window
+    // fallback to "cc-exp" as there's no such case that cc-exp-month or cc-exp-year
+    // presents alone.
+    fieldScanner.updateFieldName(fieldScanner.parsingIndex, "cc-exp");
+    fieldScanner.parsingIndex++;
+
+    return true;
+  },
+
+  /**
    * This function should provide all field details of a form. The details
    * contain the autocomplete info (e.g. fieldName, section, etc).
    *
    * `allowDuplicates` is used for the xpcshell-test purpose currently because
    * the heuristics should be verified that some duplicated elements still can
    * be predicted correctly.
    *
    * @param {HTMLFormElement} form
@@ -410,20 +540,21 @@ this.FormAutofillHeuristics = {
     if (form.elements.length <= 0) {
       return [];
     }
 
     let fieldScanner = new FieldScanner(form.elements);
     while (!fieldScanner.parsingFinished) {
       let parsedPhoneFields = this._parsePhoneFields(fieldScanner);
       let parsedAddressFields = this._parseAddressFields(fieldScanner);
+      let parsedExpirationDateFields = this._parseCreditCardExpirationDateFields(fieldScanner);
 
       // If there is no any field parsed, the parsing cursor can be moved
       // forward to the next one.
-      if (!parsedPhoneFields && !parsedAddressFields) {
+      if (!parsedPhoneFields && !parsedAddressFields && !parsedExpirationDateFields) {
         fieldScanner.parsingIndex++;
       }
     }
 
     LabelUtils.clearLabelMap();
 
     if (allowDuplicates) {
       return fieldScanner.fieldDetails;
diff --git a/browser/extensions/formautofill/FormAutofillUtils.jsm b/browser/extensions/formautofill/FormAutofillUtils.jsm
--- a/browser/extensions/formautofill/FormAutofillUtils.jsm
+++ b/browser/extensions/formautofill/FormAutofillUtils.jsm
@@ -171,17 +171,17 @@ this.FormAutofillUtils = {
       });
     });
   },
 
   autofillFieldSelector(doc) {
     return doc.querySelectorAll("input, select");
   },
 
-  ALLOWED_TYPES: ["text", "email", "tel", "number"],
+  ALLOWED_TYPES: ["text", "email", "tel", "number", "month"],
   isFieldEligibleForAutofill(element) {
     let tagName = element.tagName;
     if (tagName == "INPUT") {
       // `element.type` can be recognized as `text`, if it's missing or invalid.
       if (!this.ALLOWED_TYPES.includes(element.type)) {
         return false;
       }
     } else if (tagName != "SELECT") {
diff --git a/browser/extensions/formautofill/test/fixtures/heuristics_cc_exp.html b/browser/extensions/formautofill/test/fixtures/heuristics_cc_exp.html
new file mode 100644
--- /dev/null
+++ b/browser/extensions/formautofill/test/fixtures/heuristics_cc_exp.html
@@ -0,0 +1,63 @@
+<!DOCTYPE html>
+<html>
+<head>
+  <meta charset="utf-8">
+  <title>Heuristics cc-exp field test page</title>
+</head>
+<body>
+  <h1>Heuristics cc-exp field test page</h1>
+  <form id="form">
+    <p><label>Name: <input id="cc-name" autocomplete="cc-name"></label></p>
+    <p><label>Card Number: <input id="cc-number" autocomplete="cc-number"></label></p>
+    <p><label>Expiration month: <input id="cc-exp-month" autocomplete="cc-exp-month"></label></p>
+    <p><label>Expiration year: <input id="cc-exp-year" autocomplete="cc-exp-year"></label></p>
+    <p><label>CSC: <input id="cc-csc" autocomplete="cc-csc"></label></p>
+  </form>
+  <form id="formB">
+    <p><label>Card Number: <input id="cc-number" autocomplete="cc-number"></label></p>
+    <p><label>Expiration Date: <input autocomplete="cc-exp"></label></p>
+  </form>
+  <form id="formC">
+    <p><label>Card Number: <input id="cc-number" autocomplete="cc-number"></label></p>
+    <p><label>Expiration Date: <input type="text"></label></p>
+  </form>
+  <form id="formD">
+    <p><label>Card Number: <input id="cc-number" autocomplete="cc-number"></label></p>
+    <p>
+      <label>Exp:
+        <select>
+          <option value="1"></option>
+          <option value="2"></option>
+          <option value="3"></option>
+          <option value="4"></option>
+          <option value="5"></option>
+          <option value="6"></option>
+          <option value="7"></option>
+          <option value="8"></option>
+          <option value="9"></option>
+          <option value="10"></option>
+          <option value="11"></option>
+          <option value="12"></option>
+        </select>
+      </label>
+    </p>
+    <p>
+      <label>Exp:
+        <select>
+          <option value="2015"></option>
+          <option value="2016"></option>
+          <option value="2017"></option>
+          <option value="2018"></option>
+          <option value="2019"></option>
+          <option value="2020"></option>
+          <option value="2021"></option>
+          <option value="2022"></option>
+          <option value="2023"></option>
+          <option value="2024"></option>
+          <option value="2025"></option>
+        </select>
+      </label>
+    </p>
+  </form>
+</body>
+</html>
diff --git a/browser/extensions/formautofill/test/unit/head.js b/browser/extensions/formautofill/test/unit/head.js
--- a/browser/extensions/formautofill/test/unit/head.js
+++ b/browser/extensions/formautofill/test/unit/head.js
@@ -95,16 +95,17 @@ function runHeuristicsTest(patterns, fix
         }
       }
 
       Assert.equal(forms.length, testPattern.expectedResult.length, "Expected form count.");
 
       forms.forEach((form, formIndex) => {
         let formInfo = FormAutofillHeuristics.getFormInfo(form);
         info("FieldName Prediction Results: " + formInfo.map(i => i.fieldName));
+        info("FieldName Expected Results:   " + testPattern.expectedResult[formIndex].map(i => i.fieldName));
         Assert.equal(formInfo.length, testPattern.expectedResult[formIndex].length, "Expected field count.");
         formInfo.forEach((field, fieldIndex) => {
           let expectedField = testPattern.expectedResult[formIndex][fieldIndex];
           delete field._reason;
           expectedField.elementWeakRef = field.elementWeakRef;
           Assert.deepEqual(field, expectedField);
         });
       });
diff --git a/browser/extensions/formautofill/test/unit/heuristics/test_cc_exp.js b/browser/extensions/formautofill/test/unit/heuristics/test_cc_exp.js
new file mode 100644
--- /dev/null
+++ b/browser/extensions/formautofill/test/unit/heuristics/test_cc_exp.js
@@ -0,0 +1,32 @@
+/* global runHeuristicsTest */
+
+"use strict";
+
+runHeuristicsTest([
+  {
+    fixturePath: "heuristics_cc_exp.html",
+    expectedResult: [
+      [
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-name"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-csc"},
+      ],
+      [
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
+      ],
+      [
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
+      ],
+      [
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
+      ],
+    ],
+  },
+], "../../fixtures/");
+
diff --git a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_CDW.js b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_CDW.js
--- a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_CDW.js
+++ b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_CDW.js
@@ -39,19 +39,17 @@ runHeuristicsTest([
         {"section": "", "addressType": "", "contactType": "", "fieldName": "address-level1"}, // state
         {"section": "", "addressType": "", "contactType": "", "fieldName": "postal-code"},
 
         // FIXME: bug 1392932 - misdetect ZIP ext string
         {"section": "", "addressType": "", "contactType": "", "fieldName": "tel-extension"},
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-type"},
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"}, // ac-off
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
-
-        // FIXME: bug 1392940 - the below element can not match to "cc-exp-year" regexp directly.
-//      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
 
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-csc"},
       ],
       [],
     ],
   }, {
     fixturePath: "Checkout_Logon.html",
     expectedResult: [
diff --git a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_CostCo.js b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_CostCo.js
--- a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_CostCo.js
+++ b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_CostCo.js
@@ -44,19 +44,17 @@ runHeuristicsTest([
     ],
   }, {
     fixturePath: "Payment.html",
     expectedResult: [
       [
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-type"},
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"}, // ac-off
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
-
-        // FIXME: bug 1392940 - the below element can not match to "cc-exp-year" regexp directly.
-//      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
 
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-csc"}, // ac-off
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-name"}, // ac-off
       ],
       [
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"}, // ac-off
       ],
       [],
diff --git a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_QVC.js b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_QVC.js
--- a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_QVC.js
+++ b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_QVC.js
@@ -12,20 +12,20 @@ runHeuristicsTest([
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "bday-month"}, // select
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "bday-day"}, // select
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "bday-year"},
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-type"},
 
         // FIXME: bug 1392947 - this is a compound cc-exp field rather than the
         // separated ones below. the birthday fields are misdetected as
         // cc-exp-year and cc-exp-month.
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
+//      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
-        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
-        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
-        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
 
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-csc"},
       ],
       [
         {"section": "", "addressType": "", "contactType": "", "fieldName": "email"},
       ],
     ],
   }, {
@@ -37,20 +37,20 @@ runHeuristicsTest([
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "bday-month"}, // select
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "bday-day"}, // select
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "bday-year"}, // select
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-type"}, // select
 
         // FIXME: bug 1392947 - this is a compound cc-exp field rather than the
         // separated ones below. the birthday fields are misdetected as
         // cc-exp-year and cc-exp-month.
-//      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"}, // select
-        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"}, // select
+//      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"}, // ac-off
-        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
+//      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
 
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-csc"},
       ],
       [
         {"section": "", "addressType": "", "contactType": "", "fieldName": "email"},
       ],
     ],
   }, {
diff --git a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_Sears.js b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_Sears.js
--- a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_Sears.js
+++ b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_Sears.js
@@ -79,17 +79,17 @@ runHeuristicsTest([
         // as cc-number.
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
 
         // FIXME: bug 1392934 - this should be detected as address-level1 since
         // it's for Driver's license or state identification.
         {"section": "", "addressType": "", "contactType": "", "fieldName": "address-level1"},
 
         // FIXME: bug 1392947 - this is for birthday actually.
-        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "bday-month"},
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "bday-day"},
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "bday-year"},
       ],
       [
         {"section": "", "addressType": "", "contactType": "", "fieldName": "email"},
       ],
     ],
diff --git a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_Staples.js b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_Staples.js
--- a/browser/extensions/formautofill/test/unit/heuristics/third_party/test_Staples.js
+++ b/browser/extensions/formautofill/test/unit/heuristics/third_party/test_Staples.js
@@ -29,32 +29,24 @@ runHeuristicsTest([
         {"section": "", "addressType": "", "contactType": "", "fieldName": "organization"},
       ],
     ],
   }, {
     fixturePath: "PaymentBilling.html",
     expectedResult: [
       [
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
-
-        // FIXME: bug 1392940 - Since any credit card fields should be
-        // recognized no matter it's autocomplete="off" or not. This field
-        // "cc-exp-month" should be fixed as "cc-exp".
-        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-csc"},
       ],
     ],
   }, {
     fixturePath: "PaymentBilling_ac_on.html",
     expectedResult: [
       [
         {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
-
-        // FIXME: bug 1392940 - Since any credit card fields should be
-        // recognized no matter it's autocomplete="off" or not. This field
-        // "cc-exp-month" should be fixed as "cc-exp".
-        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
+        {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
 //      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-csc"},
       ],
     ],
   },
 ], "../../../fixtures/third_party/Staples/");
 
diff --git a/browser/extensions/formautofill/test/unit/xpcshell.ini b/browser/extensions/formautofill/test/unit/xpcshell.ini
--- a/browser/extensions/formautofill/test/unit/xpcshell.ini
+++ b/browser/extensions/formautofill/test/unit/xpcshell.ini
@@ -1,15 +1,16 @@
 [DEFAULT]
 firefox-appdir = browser
 head = head.js
 support-files =
   ../fixtures/**
 
 [heuristics/test_basic.js]
+[heuristics/test_cc_exp.js]
 [heuristics/third_party/test_BestBuy.js]
 [heuristics/third_party/test_CDW.js]
 [heuristics/third_party/test_CostCo.js]
 [heuristics/third_party/test_HomeDepot.js]
 [heuristics/third_party/test_Macys.js]
 [heuristics/third_party/test_NewEgg.js]
 [heuristics/third_party/test_OfficeDepot.js]
 [heuristics/third_party/test_QVC.js]
