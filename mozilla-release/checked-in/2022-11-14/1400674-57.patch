# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1507886664 -28800
#      Fri Oct 13 17:24:24 2017 +0800
# Node ID 413220444b15681a7f5af66514b4e66e9b434005
# Parent  de5806b3580db8b1d00ebf4103e5b4b901da037f
Bug 1400674 - Fix the algorithm of filling audio gaps with silence. r=jya, a=ritu

MozReview-Commit-ID: Hjsboq6PdlN

diff --git a/dom/media/mediasink/AudioSink.cpp b/dom/media/mediasink/AudioSink.cpp
--- a/dom/media/mediasink/AudioSink.cpp
+++ b/dom/media/mediasink/AudioSink.cpp
@@ -419,38 +419,31 @@ AudioSink::NotifyAudioNeeded()
     if (missingFrames.value() > AUDIO_FUZZ_FRAMES) {
       // The next audio packet begins some time after the end of the last packet
       // we pushed to the audio hardware. We must push silence into the audio
       // hardware so that the next audio packet begins playback at the correct
       // time.
       missingFrames = std::min<int64_t>(INT32_MAX, missingFrames.value());
       mFramesParsed += missingFrames.value();
 
-      // We need to calculate how many frames are missing at the output rate.
-      missingFrames =
-        SaferMultDiv(missingFrames.value(), mOutputRate, data->mRate);
-      if (!missingFrames.isValid()) {
-        NS_WARNING("Int overflow in AudioSink");
-        mErrored = true;
-        return;
+      RefPtr<AudioData> silenceData;
+      AlignedAudioBuffer silenceBuffer(missingFrames.value() * data->mChannels);
+       if (!silenceBuffer) {
+         NS_WARNING("OOM in AudioSink");
+         mErrored = true;
+         return;
+       }
+      if (mConverter->InputConfig() != mConverter->OutputConfig()) {
+        AlignedAudioBuffer convertedData =
+          mConverter->Process(AudioSampleBuffer(Move(silenceBuffer))).Forget();
+        silenceData = CreateAudioFromBuffer(Move(convertedData), data);
+      } else {
+        silenceData = CreateAudioFromBuffer(Move(silenceBuffer), data);
       }
-
-      // We need to insert silence, first use drained frames if any.
-      missingFrames -= DrainConverter(missingFrames.value());
-      // Insert silence if still needed.
-      if (missingFrames.value()) {
-        AlignedAudioBuffer silenceData(missingFrames.value() * mOutputChannels);
-        if (!silenceData) {
-          NS_WARNING("OOM in AudioSink");
-          mErrored = true;
-          return;
-        }
-        RefPtr<AudioData> silence = CreateAudioFromBuffer(Move(silenceData), data);
-        PushProcessedAudio(silence);
-      }
+      PushProcessedAudio(silenceData);
     }
 
     mLastEndTime = data->GetEndTime();
     mFramesParsed += data->mFrames;
 
     if (mConverter->InputConfig() != mConverter->OutputConfig()) {
       // We must ensure that the size in the buffer contains exactly the number
       // of frames, in case one of the audio producer over allocated the buffer.
@@ -481,17 +474,17 @@ AudioSink::PushProcessedAudio(AudioData*
   }
   mProcessedQueue.Push(aData);
   mProcessedQueueLength += FramesToUsecs(aData->mFrames, mOutputRate).value();
   return aData->mFrames;
 }
 
 already_AddRefed<AudioData>
 AudioSink::CreateAudioFromBuffer(AlignedAudioBuffer&& aBuffer,
-                                            AudioData* aReference)
+                                 AudioData* aReference)
 {
   uint32_t frames = aBuffer.Length() / mOutputChannels;
   if (!frames) {
     return nullptr;
   }
   auto duration = FramesToTimeUnit(frames, mOutputRate);
   if (!duration.IsValid()) {
     NS_WARNING("Int overflow in AudioSink");
