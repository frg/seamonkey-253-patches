# HG changeset patch
# User Cervantes Yu <cyu@mozilla.com>
# Date 1507001354 -28800
# Node ID e484df87b3ee915f29b0f37f47e045f7e1f092cd
# Parent  ec0e48a2c6eef8915f6f52df63fb18ea4334ba56
Bug 1400294 - Add 'noShell' attribute to nsIProcess to use CreateProcess() for launching a process that doesn't require shell service on Windows. r=froydnj,r=aklotz,r=dmajor

This adds an attribute 'noShell' to nsIProcess that is used to launch a process
using CreateProcess() if we are sure we are launching an executable and don't
require the extra work of the Windows shell service.

MozReview-Commit-ID: 7p0iHCZK1uX

diff --git a/toolkit/components/crashes/CrashService.js b/toolkit/components/crashes/CrashService.js
--- a/toolkit/components/crashes/CrashService.js
+++ b/toolkit/components/crashes/CrashService.js
@@ -43,16 +43,18 @@ function runMinidumpAnalyzer(minidumpPat
 
       exe.append(exeName);
 
       let args = [ minidumpPath ];
       let process = Cc["@mozilla.org/process/util;1"]
                       .createInstance(Ci.nsIProcess);
       process.init(exe);
       process.startHidden = true;
+      process.noShell = true;
+
       process.runAsync(args, args.length, (subject, topic, data) => {
         switch (topic) {
           case "process-finished":
             gRunningProcesses.delete(process);
             resolve();
             break;
           default:
             reject(new Error("Unexpected topic received " + topic));
diff --git a/toolkit/components/telemetry/TelemetrySend.jsm b/toolkit/components/telemetry/TelemetrySend.jsm
--- a/toolkit/components/telemetry/TelemetrySend.jsm
+++ b/toolkit/components/telemetry/TelemetrySend.jsm
@@ -1282,11 +1282,12 @@ var TelemetrySendImpl = {
 
     let exe = Services.dirsvc.get("GreBinD", Ci.nsIFile);
     exe.append(exeName);
 
     let process = Cc["@mozilla.org/process/util;1"]
                   .createInstance(Ci.nsIProcess);
     process.init(exe);
     process.startHidden = true;
+    process.noShell = true;
     process.run(/* blocking */ false, [url, pingPath], 2);
   },
 };
diff --git a/xpcom/threads/nsIProcess.idl b/xpcom/threads/nsIProcess.idl
--- a/xpcom/threads/nsIProcess.idl
+++ b/xpcom/threads/nsIProcess.idl
@@ -77,16 +77,22 @@ interface nsIProcess : nsISupports
   /**
    * When set to true the process will not open a new window when started and
    * will run hidden from the user. This currently affects only the Windows
    * platform.
    */
   attribute boolean startHidden;
 
   /**
+   * When set to true the process will be launched directly without using the
+   * shell. This currently affects only the Windows platform.
+   */
+  attribute boolean noShell;
+
+  /**
    * The process identifier of the currently running process. This will only
    * be available after the process has started and may not be available on
    * some platforms.
    */
   readonly attribute unsigned long pid;
 
   /**
    * The exit value of the process. This is only valid after the process has
diff --git a/xpcom/threads/nsProcess.h b/xpcom/threads/nsProcess.h
--- a/xpcom/threads/nsProcess.h
+++ b/xpcom/threads/nsProcess.h
@@ -58,16 +58,17 @@ private:
   nsresult RunProcess(bool aBlocking, char** aArgs, nsIObserver* aObserver,
                       bool aHoldWeak, bool aArgsUTF8);
 
   PRThread* mThread;
   mozilla::Mutex mLock;
   bool mShutdown;
   bool mBlocking;
   bool mStartHidden;
+  bool mNoShell;
 
   nsCOMPtr<nsIFile> mExecutable;
   nsString mTargetPath;
   int32_t mPid;
   nsCOMPtr<nsIObserver> mObserver;
   nsWeakPtr mWeakObserver;
 
   // These members are modified by multiple threads, any accesses should be
diff --git a/xpcom/threads/nsProcessCommon.cpp b/xpcom/threads/nsProcessCommon.cpp
--- a/xpcom/threads/nsProcessCommon.cpp
+++ b/xpcom/threads/nsProcessCommon.cpp
@@ -28,16 +28,17 @@
 #include "GeckoProfiler.h"
 
 #include <stdlib.h>
 
 #if defined(PROCESSMODEL_WINAPI)
 #include "nsString.h"
 #include "nsLiteralString.h"
 #include "nsReadableUtils.h"
+#include "mozilla/UniquePtrExtensions.h"
 #else
 #ifdef XP_MACOSX
 #include <crt_externs.h>
 #include <spawn.h>
 #endif
 #ifdef XP_UNIX
 #ifndef XP_MACOSX
 #include "base/process_util.h"
@@ -72,16 +73,17 @@ NS_IMPL_ISUPPORTS(nsProcess, nsIProcess,
 
 //Constructor
 nsProcess::nsProcess()
   : mThread(nullptr)
   , mLock("nsProcess.mLock")
   , mShutdown(false)
   , mBlocking(false)
   , mStartHidden(false)
+  , mNoShell(false)
   , mPid(-1)
   , mObserver(nullptr)
   , mWeakObserver(nullptr)
   , mExitValue(-1)
 #if !defined(XP_UNIX)
   , mProcess(nullptr)
 #endif
 {
@@ -460,56 +462,88 @@ nsProcess::RunProcess(bool aBlocking, ch
     }
   }
 
   mExitValue = -1;
   mPid = -1;
 
 #if defined(PROCESSMODEL_WINAPI)
   BOOL retVal;
-  wchar_t* cmdLine = nullptr;
+  UniqueFreePtr<wchar_t> cmdLine;
 
   // |aMyArgv| is null-terminated and always starts with the program path. If
   // the second slot is non-null then arguments are being passed.
-  if (aMyArgv[1] && assembleCmdLine(aMyArgv + 1, &cmdLine,
-                                    aArgsUTF8 ? CP_UTF8 : CP_ACP) == -1) {
-    return NS_ERROR_FILE_EXECUTION_FAILED;
-  }
+  if (aMyArgv[1] || mNoShell) {
+    // Pass the executable path as argv[0] to the launched program when calling
+    // CreateProcess().
+    char** argv = mNoShell ? aMyArgv : aMyArgv + 1;
 
-  /* The SEE_MASK_NO_CONSOLE flag is important to prevent console windows
-   * from appearing. This makes behavior the same on all platforms. The flag
-   * will not have any effect on non-console applications.
-   */
+    wchar_t* assembledCmdLine = nullptr;
+    if (assembleCmdLine(argv, &assembledCmdLine,
+                        aArgsUTF8 ? CP_UTF8 : CP_ACP) == -1) {
+      return NS_ERROR_FILE_EXECUTION_FAILED;
+    }
+    cmdLine.reset(assembledCmdLine);
+  }
 
   // The program name in aMyArgv[0] is always UTF-8
   NS_ConvertUTF8toUTF16 wideFile(aMyArgv[0]);
 
-  SHELLEXECUTEINFOW sinfo;
-  memset(&sinfo, 0, sizeof(SHELLEXECUTEINFOW));
-  sinfo.cbSize = sizeof(SHELLEXECUTEINFOW);
-  sinfo.hwnd   = nullptr;
-  sinfo.lpFile = wideFile.get();
-  sinfo.nShow  = mStartHidden ? SW_HIDE : SW_SHOWNORMAL;
-  sinfo.fMask  = SEE_MASK_FLAG_DDEWAIT |
-                 SEE_MASK_NO_CONSOLE |
-                 SEE_MASK_NOCLOSEPROCESS;
+  if (mNoShell) {
+    STARTUPINFO startupInfo;
+    ZeroMemory(&startupInfo, sizeof(startupInfo));
+    startupInfo.cb = sizeof(startupInfo);
+    startupInfo.dwFlags = STARTF_USESHOWWINDOW;
+    startupInfo.wShowWindow = mStartHidden ? SW_HIDE : SW_SHOWNORMAL;
+
+    PROCESS_INFORMATION processInfo;
+    retVal = CreateProcess(/* lpApplicationName = */ wideFile.get(),
+                           /* lpCommandLine */ cmdLine.get(),
+                           /* lpProcessAttributes = */ NULL,
+                           /* lpThreadAttributes = */ NULL,
+                           /* bInheritHandles = */ FALSE,
+                           /* dwCreationFlags = */ 0,
+                           /* lpEnvironment = */ NULL,
+                           /* lpCurrentDirectory = */ NULL,
+                           /* lpStartupInfo = */ &startupInfo,
+                           /* lpProcessInformation */ &processInfo);
+
+    if (!retVal) {
+      return NS_ERROR_FILE_EXECUTION_FAILED;
+    }
+
+    CloseHandle(processInfo.hThread);
 
-  if (cmdLine) {
-    sinfo.lpParameters = cmdLine;
-  }
+    mProcess = processInfo.hProcess;
+  } else {
+    SHELLEXECUTEINFOW sinfo;
+    memset(&sinfo, 0, sizeof(SHELLEXECUTEINFOW));
+    sinfo.cbSize = sizeof(SHELLEXECUTEINFOW);
+    sinfo.hwnd   = nullptr;
+    sinfo.lpFile = wideFile.get();
+    sinfo.nShow  = mStartHidden ? SW_HIDE : SW_SHOWNORMAL;
 
-  retVal = ShellExecuteExW(&sinfo);
-  if (!retVal) {
-    return NS_ERROR_FILE_EXECUTION_FAILED;
-  }
+    /* The SEE_MASK_NO_CONSOLE flag is important to prevent console windows
+     * from appearing. This makes behavior the same on all platforms. The flag
+     * will not have any effect on non-console applications.
+     */
+    sinfo.fMask  = SEE_MASK_FLAG_DDEWAIT |
+                   SEE_MASK_NO_CONSOLE |
+                   SEE_MASK_NOCLOSEPROCESS;
 
-  mProcess = sinfo.hProcess;
+    if (cmdLine) {
+      sinfo.lpParameters = cmdLine.get();
+    }
 
-  if (cmdLine) {
-    free(cmdLine);
+    retVal = ShellExecuteExW(&sinfo);
+    if (!retVal) {
+      return NS_ERROR_FILE_EXECUTION_FAILED;
+    }
+
+    mProcess = sinfo.hProcess;
   }
 
   mPid = GetProcessId(mProcess);
 #elif defined(XP_MACOSX)
   // Initialize spawn attributes.
   posix_spawnattr_t spawnattr;
   if (posix_spawnattr_init(&spawnattr) != 0) {
     return NS_ERROR_FAILURE;
@@ -612,16 +646,30 @@ nsProcess::GetStartHidden(bool* aStartHi
 NS_IMETHODIMP
 nsProcess::SetStartHidden(bool aStartHidden)
 {
   mStartHidden = aStartHidden;
   return NS_OK;
 }
 
 NS_IMETHODIMP
+nsProcess::GetNoShell(bool* aNoShell)
+{
+  *aNoShell = mNoShell;
+  return NS_OK;
+}
+
+NS_IMETHODIMP
+nsProcess::SetNoShell(bool aNoShell)
+{
+  mNoShell = aNoShell;
+  return NS_OK;
+}
+
+NS_IMETHODIMP
 nsProcess::GetPid(uint32_t* aPid)
 {
   if (!mThread) {
     return NS_ERROR_FAILURE;
   }
   if (mPid < 0) {
     return NS_ERROR_NOT_IMPLEMENTED;
   }
