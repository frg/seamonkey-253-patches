# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1506117779 -7200
#      Sat Sep 23 00:02:59 2017 +0200
# Node ID 6b96a0fab002660434dc96f3ff8cf086cac1f4ac
# Parent  4f6aadcaebb4ee6b2a0dddf189eda363b30cf5b2
Bug 1400936 - Only tear down the servo data in SetXBLInsertionParent if the parent actually changed. r=bholley, a=sledru

This is the reason similar assertion failures can't be reproduced with elements
and stuff like fieldset and form validity changes.

nsBindingManager::ContentRemoved calls SetXBLInsertionParent, which clears all
the Servo data from the subtree eagerly, which is a waste when the actual
binding parent is the same (null).

MozReview-Commit-ID: A5wLKfD4OTL
Signed-off-by: Emilio Cobos Alvarez <emilio@crisal.io>

diff --git a/dom/base/FragmentOrElement.cpp b/dom/base/FragmentOrElement.cpp
--- a/dom/base/FragmentOrElement.cpp
+++ b/dom/base/FragmentOrElement.cpp
@@ -1236,30 +1236,33 @@ FragmentOrElement::GetExistingDestInsert
     return &slots->mDestInsertionPoints;
   }
   return nullptr;
 }
 
 void
 FragmentOrElement::SetXBLInsertionParent(nsIContent* aContent)
 {
+  nsCOMPtr<nsIContent> oldInsertionParent = nullptr;
   if (aContent) {
     nsExtendedDOMSlots* slots = ExtendedDOMSlots();
     SetFlags(NODE_MAY_BE_IN_BINDING_MNGR);
+    oldInsertionParent = slots->mXBLInsertionParent.forget();
     slots->mXBLInsertionParent = aContent;
   } else {
-    nsExtendedDOMSlots* slots = GetExistingExtendedDOMSlots();
-    if (slots) {
+    if (nsExtendedDOMSlots* slots = GetExistingExtendedDOMSlots()) {
+      oldInsertionParent = slots->mXBLInsertionParent.forget();
       slots->mXBLInsertionParent = nullptr;
     }
   }
 
   // We just changed the flattened tree, so any Servo style data is now invalid.
   // We rely on nsXBLService::LoadBindings to re-traverse the subtree afterwards.
-  if (IsStyledByServo() && IsElement() && AsElement()->HasServoData()) {
+  if (oldInsertionParent != aContent &&
+      IsStyledByServo() && IsElement() && AsElement()->HasServoData()) {
     ServoRestyleManager::ClearServoDataFromSubtree(AsElement());
   }
 }
 
 nsresult
 FragmentOrElement::InsertChildAt(nsIContent* aKid,
                                 uint32_t aIndex,
                                 bool aNotify)
