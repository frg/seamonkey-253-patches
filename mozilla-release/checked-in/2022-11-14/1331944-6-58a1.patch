# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1509383429 14400
# Node ID 3741bc7ff417fc5e749e40b2baae51857eb8cf9d
# Parent  3a29cc4edf473bf5da434f8e7f0c5af72d80aef8
Bug 1331944 - Part 6. Expand SharedSurfacesChild to support sharing ImageContainers directly. r=jrmuizel

diff --git a/gfx/layers/ipc/SharedSurfacesChild.cpp b/gfx/layers/ipc/SharedSurfacesChild.cpp
--- a/gfx/layers/ipc/SharedSurfacesChild.cpp
+++ b/gfx/layers/ipc/SharedSurfacesChild.cpp
@@ -150,16 +150,47 @@ SharedSurfacesChild::Share(SourceSurface
   aId = data->Id();
   manager->SendAddSharedSurface(aId,
                                 SurfaceDescriptorShared(aSurface->GetSize(),
                                                         aSurface->Stride(),
                                                         format, handle));
   return NS_OK;
 }
 
+/* static */ nsresult
+SharedSurfacesChild::Share(ImageContainer* aContainer,
+                           wr::ExternalImageId& aId,
+                           uint32_t& aGeneration)
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  MOZ_ASSERT(aContainer);
+
+  if (aContainer->IsAsync()) {
+    return NS_ERROR_NOT_IMPLEMENTED;
+  }
+
+  AutoTArray<ImageContainer::OwningImage,4> images;
+  aContainer->GetCurrentImages(&images, &aGeneration);
+  if (images.IsEmpty()) {
+    return NS_ERROR_NOT_AVAILABLE;
+  }
+
+  RefPtr<gfx::SourceSurface> surface = images[0].mImage->GetAsSourceSurface();
+  if (!surface) {
+    return NS_ERROR_NOT_IMPLEMENTED;
+  }
+
+  if (surface->GetType() != SurfaceType::DATA_SHARED) {
+    return NS_ERROR_NOT_IMPLEMENTED;
+  }
+
+  auto sharedSurface = static_cast<SourceSurfaceSharedData*>(surface.get());
+  return Share(sharedSurface, aId);
+}
+
 /* static */ void
 SharedSurfacesChild::Unshare(const wr::ExternalImageId& aId)
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   CompositorManagerChild* manager = CompositorManagerChild::GetInstance();
   if (MOZ_UNLIKELY(!manager || !manager->CanSend())) {
     return;
diff --git a/gfx/layers/ipc/SharedSurfacesChild.h b/gfx/layers/ipc/SharedSurfacesChild.h
--- a/gfx/layers/ipc/SharedSurfacesChild.h
+++ b/gfx/layers/ipc/SharedSurfacesChild.h
@@ -21,16 +21,17 @@ class SourceSurfaceSharedData;
 namespace layers {
 
 class CompositorManagerChild;
 
 class SharedSurfacesChild final
 {
 public:
   static nsresult Share(gfx::SourceSurfaceSharedData* aSurface, wr::ExternalImageId& aId);
+  static nsresult Share(ImageContainer* aContainer, wr::ExternalImageId& aId, uint32_t& aGeneration);
 
 private:
   SharedSurfacesChild() = delete;
   ~SharedSurfacesChild() = delete;
 
   class SharedUserData;
 
   static void Unshare(const wr::ExternalImageId& aId);

