# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1509027273 14400
# Node ID dab6d6b8d11e6379a4c4e2ecfcb6b54b6b872060
# Parent  b08989095f153b9e86c6ab11f66afbb308377862
Bug 1412012 - [mozdebug] Add a basic test for get_default_debugger_name, r=jmaher

MozReview-Commit-ID: CwkXKJWd55M

diff --git a/testing/mozbase/moz.build b/testing/mozbase/moz.build
--- a/testing/mozbase/moz.build
+++ b/testing/mozbase/moz.build
@@ -2,16 +2,17 @@
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 PYTHON_UNITTEST_MANIFESTS += [
     'manifestparser/tests/manifest.ini',
     'mozcrash/tests/manifest.ini',
+    'mozdebug/tests/manifest.ini',
     'mozfile/tests/manifest.ini',
     'mozhttpd/tests/manifest.ini',
     'mozinfo/tests/manifest.ini',
     'mozinstall/tests/manifest.ini',
     'mozlog/tests/manifest.ini',
     'moznetwork/tests/manifest.ini',
     'mozprocess/tests/manifest.ini',
     'mozprofile/tests/manifest.ini',
diff --git a/testing/mozbase/mozdebug/tests/fake_debuggers/cgdb/cgdb b/testing/mozbase/mozdebug/tests/fake_debuggers/cgdb/cgdb
new file mode 100644
diff --git a/testing/mozbase/mozdebug/tests/fake_debuggers/devenv/devenv.exe b/testing/mozbase/mozdebug/tests/fake_debuggers/devenv/devenv.exe
new file mode 100644
diff --git a/testing/mozbase/mozdebug/tests/fake_debuggers/gdb/gdb b/testing/mozbase/mozdebug/tests/fake_debuggers/gdb/gdb
new file mode 100644
diff --git a/testing/mozbase/mozdebug/tests/fake_debuggers/lldb/lldb b/testing/mozbase/mozdebug/tests/fake_debuggers/lldb/lldb
new file mode 100644
diff --git a/testing/mozbase/mozdebug/tests/fake_debuggers/wdexpress/wdexpress.exe b/testing/mozbase/mozdebug/tests/fake_debuggers/wdexpress/wdexpress.exe
new file mode 100644
diff --git a/testing/mozbase/mozdebug/tests/manifest.ini b/testing/mozbase/mozdebug/tests/manifest.ini
new file mode 100644
--- /dev/null
+++ b/testing/mozbase/mozdebug/tests/manifest.ini
@@ -0,0 +1,4 @@
+[DEFAULT]
+subsuite = mozbase, os == "linux"
+
+[test.py]
diff --git a/testing/mozbase/mozdebug/tests/test.py b/testing/mozbase/mozdebug/tests/test.py
new file mode 100644
--- /dev/null
+++ b/testing/mozbase/mozdebug/tests/test.py
@@ -0,0 +1,65 @@
+#!/usr/bin/env python
+
+from __future__ import absolute_import, print_function
+
+import os
+
+import mozunit
+import pytest
+from mozdebug.mozdebug import (
+    _DEBUGGER_PRIORITIES,
+    DebuggerSearch,
+    get_default_debugger_name,
+)
+
+
+here = os.path.abspath(os.path.dirname(__file__))
+
+
+@pytest.fixture
+def set_debuggers(monkeypatch):
+    debugger_dir = os.path.join(here, 'fake_debuggers')
+
+    def _set_debuggers(*debuggers):
+        dirs = []
+        for d in debuggers:
+            if d.endswith('.exe'):
+                d = d[:-len('.exe')]
+            dirs.append(os.path.join(debugger_dir, d))
+        monkeypatch.setenv('PATH', os.pathsep.join(dirs))
+
+    return _set_debuggers
+
+
+@pytest.mark.parametrize('os_name', ['android', 'linux', 'mac', 'win', 'unknown'])
+def test_default_debugger_name(os_name, set_debuggers, monkeypatch):
+    import mozinfo
+    import sys
+
+    def update_os_name(*args, **kwargs):
+        mozinfo.info['os'] = os_name
+
+    monkeypatch.setattr(mozinfo, 'find_and_update_from_json', update_os_name)
+
+    if sys.platform == 'win32':
+        # This is used so distutils.spawn.find_executable doesn't add '.exe'
+        # suffixes to all our dummy binaries on Windows.
+        monkeypatch.setattr(sys, 'platform', 'linux')
+
+    debuggers = _DEBUGGER_PRIORITIES[os_name][:]
+    debuggers.reverse()
+    first = True
+    while len(debuggers) > 0:
+        set_debuggers(*debuggers)
+
+        if first:
+            assert get_default_debugger_name() == debuggers[-1]
+            first = False
+        else:
+            assert get_default_debugger_name() is None
+            assert get_default_debugger_name(DebuggerSearch.KeepLooking) == debuggers[-1]
+        debuggers = debuggers[:-1]
+
+
+if __name__ == '__main__':
+    mozunit.main()
