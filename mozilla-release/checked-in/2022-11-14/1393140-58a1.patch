# HG changeset patch
# User Ehsan Akhgari <ehsan@mozilla.com>
# Date 1503511214 14400
#      Wed Aug 23 14:00:14 2017 -0400
# Node ID ee49f5bb7b204499b646da1b9e0106e18cbe7749
# Parent  4d402f6a87b9e247ff4456f8e272f8463a7d2d74
Bug 1393140 - Rewrite EditorBase::FindBetterInsertionPoint() to not use nsINode::GetChildAt() in the fast path used in Firefox; r=masayuki

diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -2422,22 +2422,40 @@ EditorBase::FindBetterInsertionPoint(nsC
       aNode = node->GetFirstChild();
       aOffset = 0;
       return;
     }
 
     // In some other cases, aNode is the anonymous DIV, and offset points to the
     // terminating mozBR.  In that case, we'll adjust aInOutNode and
     // aInOutOffset to the preceding text node, if any.
-    if (offset > 0 && node->GetChildAt(offset - 1) &&
-        node->GetChildAt(offset - 1)->IsNodeOfType(nsINode::eTEXT)) {
-      NS_ENSURE_TRUE_VOID(node->Length() <= INT32_MAX);
-      aNode = node->GetChildAt(offset - 1);
-      aOffset = static_cast<int32_t>(aNode->Length());
-      return;
+    if (offset) {
+      if (offset == static_cast<int32_t>(node->GetChildCount())) {
+        // If offset points to the last child, use a fast path that avoids calling
+        // GetChildAt() which may perform a linear search.
+        nsIContent* child = node->GetLastChild();
+        while (child) {
+          if (child->IsNodeOfType(nsINode::eTEXT)) {
+            NS_ENSURE_TRUE_VOID(node->Length() <= INT32_MAX);
+            aNode = child;
+            aOffset = static_cast<int32_t>(aNode->Length());
+            return;
+          }
+          child = child->GetPreviousSibling();
+        }
+      } else {
+        // Fall back to a slow path that uses GetChildAt().
+        nsIContent* child = node->GetChildAt(offset - 1);
+        if (child && child->IsNodeOfType(nsINode::eTEXT)) {
+          NS_ENSURE_TRUE_VOID(node->Length() <= INT32_MAX);
+          aNode = child;
+          aOffset = static_cast<int32_t>(aNode->Length());
+          return;
+        }
+      }
     }
   }
 
   // Sometimes, aNode is the mozBR element itself.  In that case, we'll adjust
   // the insertion point to the previous text node, if one exists, or to the
   // parent anonymous DIV.
   if (TextEditUtils::IsMozBR(node) && !offset) {
     if (node->GetPreviousSibling() &&
