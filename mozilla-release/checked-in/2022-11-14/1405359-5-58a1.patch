# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1508885244 14400
# Node ID edc52ff233c42294140b0e22381f7f37cd30c345
# Parent  a0b898dfb8a207554d01e56d1d16c2c5bb4e8ad1
Bug 1405359 - Add some logging code in ScrollingLayersHelper. r=jrmuizel

MozReview-Commit-ID: Lb4UeUxO5HL

diff --git a/gfx/layers/wr/ScrollingLayersHelper.cpp b/gfx/layers/wr/ScrollingLayersHelper.cpp
--- a/gfx/layers/wr/ScrollingLayersHelper.cpp
+++ b/gfx/layers/wr/ScrollingLayersHelper.cpp
@@ -8,16 +8,19 @@
 
 #include "DisplayItemClipChain.h"
 #include "FrameMetrics.h"
 #include "mozilla/layers/StackingContextHelper.h"
 #include "mozilla/webrender/WebRenderAPI.h"
 #include "nsDisplayList.h"
 #include "UnitTransforms.h"
 
+#define SLH_LOG(...)
+//#define SLH_LOG(...) printf_stderr("SLH: " __VA_ARGS__)
+
 namespace mozilla {
 namespace layers {
 
 ScrollingLayersHelper::ScrollingLayersHelper()
   : mBuilder(nullptr)
 {
 }
 
@@ -51,22 +54,25 @@ ScrollingLayersHelper::EndList()
   mItemClipStack.back().Unapply(mBuilder);
   mItemClipStack.pop_back();
 }
 
 void
 ScrollingLayersHelper::BeginItem(nsDisplayItem* aItem,
                                  const StackingContextHelper& aStackingContext)
 {
+  SLH_LOG("processing item %p\n", aItem);
+
   ItemClips clips(aItem->GetActiveScrolledRoot(), aItem->GetClipChain());
   MOZ_ASSERT(!mItemClipStack.empty());
   if (clips.HasSameInputs(mItemClipStack.back())) {
     // Early-exit because if the clips are the same then we don't need to do
     // do the work of popping the old stuff and then pushing it right back on
     // for the new item.
+    SLH_LOG("early-exit for %p\n", aItem);
     return;
   }
   mItemClipStack.back().Unapply(mBuilder);
   mItemClipStack.pop_back();
 
   int32_t auPerDevPixel = aItem->Frame()->PresContext()->AppUnitsPerDevPixel();
 
   // There are two ASR chains here that we need to be fully defined. One is the
@@ -126,16 +132,18 @@ ScrollingLayersHelper::BeginItem(nsDispl
     if (!clipId) {
       clipId = mBuilder->TopmostClipId();
     }
     clips.mClipAndScroll = Some(std::make_pair(scrollId, clipId));
   }
 
   clips.Apply(mBuilder);
   mItemClipStack.push_back(clips);
+
+  SLH_LOG("done setup for %p\n", aItem);
 }
 
 std::pair<Maybe<FrameMetrics::ViewID>, Maybe<wr::WrClipId>>
 ScrollingLayersHelper::DefineClipChain(nsDisplayItem* aItem,
                                        const ActiveScrolledRoot* aAsr,
                                        const DisplayItemClipChain* aChain,
                                        int32_t aAppUnitsPerDevPixel,
                                        const StackingContextHelper& aStackingContext)
