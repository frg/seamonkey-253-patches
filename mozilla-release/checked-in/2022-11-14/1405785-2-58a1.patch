# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1505466094 -3600
#      Fri Sep 15 10:01:34 2017 +0100
# Node ID 0f1d6c8dc9929899d86ecd2f2d0d40662c83175b
# Parent  025a1eed7be8121a1b6ba4572dfda53059685705
Bug 1405785, part 2 - Document and rename nsSVGRenderingObserver::DoUpdate to OnRenderingChange. r=longsonr

MozReview-Commit-ID: 76ImwCiBeX6

diff --git a/dom/canvas/CanvasRenderingContext2D.cpp b/dom/canvas/CanvasRenderingContext2D.cpp
--- a/dom/canvas/CanvasRenderingContext2D.cpp
+++ b/dom/canvas/CanvasRenderingContext2D.cpp
@@ -992,17 +992,17 @@ public:
   CanvasFilterChainObserver(nsTArray<nsStyleFilter>& aFilters,
                             Element* aCanvasElement,
                             CanvasRenderingContext2D* aContext)
     : nsSVGFilterChainObserver(aFilters, aCanvasElement)
     , mContext(aContext)
   {
   }
 
-  virtual void DoUpdate() override
+  virtual void OnRenderingChange() override
   {
     if (!mContext) {
       MOZ_CRASH("GFX: This should never be called without a context");
     }
     // Refresh the cached FilterDescription in mContext->CurrentState().filter.
     // If this filter is not at the top of the state stack, we'll refresh the
     // wrong filter, but that's ok, because we'll refresh the right filter
     // when we pop the state stack in CanvasRenderingContext2D::Restore().
diff --git a/image/VectorImage.cpp b/image/VectorImage.cpp
--- a/image/VectorImage.cpp
+++ b/image/VectorImage.cpp
@@ -84,17 +84,17 @@ protected:
     StopObserving();
   }
 
   virtual Element* GetTarget() override
   {
     return mDocWrapper->GetRootSVGElem();
   }
 
-  virtual void DoUpdate() override
+  virtual void OnRenderingChange() override
   {
     Element* elem = GetTarget();
     MOZ_ASSERT(elem, "missing root SVG node");
 
     if (mHonoringInvalidations && !mDocWrapper->ShouldIgnoreInvalidation()) {
       nsIFrame* frame = elem->GetPrimaryFrame();
       if (!frame || frame->PresShell()->IsDestroying()) {
         // We're being destroyed. Bail out.
diff --git a/layout/svg/SVGObserverUtils.cpp b/layout/svg/SVGObserverUtils.cpp
--- a/layout/svg/SVGObserverUtils.cpp
+++ b/layout/svg/SVGObserverUtils.cpp
@@ -96,17 +96,17 @@ nsSVGRenderingObserver::GetReferencedFra
   }
   return nullptr;
 }
 
 void
 nsSVGRenderingObserver::InvalidateViaReferencedElement()
 {
   mInObserverList = false;
-  DoUpdate();
+  OnRenderingChange();
 }
 
 void
 nsSVGRenderingObserver::NotifyEvictedFromRenderingObserverList()
 {
   mInObserverList = false; // We've been removed from rendering-obs. list.
   StopObserving();            // Remove ourselves from mutation-obs. list.
 }
@@ -127,42 +127,42 @@ nsSVGRenderingObserver::AttributeChanged
   // anything for changes to SMIL element children, for example. Maybe it's not
   // worth having logic to optimize for that, but in most cases it could be a
   // small check?
   //
   // XXXjwatt: do we really want to blindly break the link between our
   // observers and ourselves for all attribute changes? For non-ID changes
   // surely that is unnecessary.
 
-  DoUpdate();
+  OnRenderingChange();
 }
 
 void
 nsSVGRenderingObserver::ContentAppended(nsIDocument* aDocument,
                                         nsIContent* aContainer,
                                         nsIContent* aFirstNewContent)
 {
-  DoUpdate();
+  OnRenderingChange();
 }
 
 void
 nsSVGRenderingObserver::ContentInserted(nsIDocument* aDocument,
                                         nsIContent* aContainer,
                                         nsIContent* aChild)
 {
-  DoUpdate();
+  OnRenderingChange();
 }
 
 void
 nsSVGRenderingObserver::ContentRemoved(nsIDocument* aDocument,
                                        nsIContent* aContainer,
                                        nsIContent* aChild,
                                        nsIContent* aPreviousSibling)
 {
-  DoUpdate();
+  OnRenderingChange();
 }
 
 /**
  * Note that in the current setup there are two separate observer lists.
  *
  * In nsSVGIDRenderingObserver's ctor, the new object adds itself to the
  * mutation observer list maintained by the referenced element. In this way the
  * nsSVGIDRenderingObserver is notified if there are any attribute or content
@@ -188,17 +188,17 @@ nsSVGIDRenderingObserver::nsSVGIDRenderi
 }
 
 nsSVGIDRenderingObserver::~nsSVGIDRenderingObserver()
 {
   StopObserving();
 }
 
 void
-nsSVGIDRenderingObserver::DoUpdate()
+nsSVGIDRenderingObserver::OnRenderingChange()
 {
   if (mObservedElementTracker.get() && mInObserverList) {
     SVGObserverUtils::RemoveRenderingObserver(mObservedElementTracker.get(), this);
     mInObserverList = false;
   }
 }
 
 void
@@ -216,19 +216,19 @@ nsSVGFrameReferenceFromProperty::Get()
     Detach();
   }
   return mFrame;
 }
 
 NS_IMPL_ISUPPORTS(nsSVGRenderingObserverProperty, nsIMutationObserver)
 
 void
-nsSVGRenderingObserverProperty::DoUpdate()
+nsSVGRenderingObserverProperty::OnRenderingChange()
 {
-  nsSVGIDRenderingObserver::DoUpdate();
+  nsSVGIDRenderingObserver::OnRenderingChange();
 
   nsIFrame* frame = mFrameReference.Get();
 
   if (frame && frame->HasAllStateBits(NS_FRAME_SVG_LAYOUT)) {
     // Changes should propagate out to things that might be observing
     // the referencing frame or its ancestors.
     nsLayoutUtils::PostRestyleEvent(
       frame->GetContent()->AsElement(), nsRestyleHint(0),
@@ -259,19 +259,19 @@ NS_INTERFACE_MAP_END
 nsSVGFilterFrame *
 nsSVGFilterReference::GetFilterFrame()
 {
   return static_cast<nsSVGFilterFrame*>(
     GetReferencedFrame(LayoutFrameType::SVGFilter, nullptr));
 }
 
 void
-nsSVGFilterReference::DoUpdate()
+nsSVGFilterReference::OnRenderingChange()
 {
-  nsSVGIDRenderingObserver::DoUpdate();
+  nsSVGIDRenderingObserver::OnRenderingChange();
 
   if (mFilterChainObserver) {
     mFilterChainObserver->Invalidate();
   }
 }
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(nsSVGFilterChainObserver)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(nsSVGFilterChainObserver)
@@ -332,46 +332,46 @@ nsSVGFilterChainObserver::IsInObserverLi
   for (uint32_t i = 0; i < mReferences.Length(); i++) {
     if (!mReferences[i]->IsInObserverList())
       return false;
   }
   return true;
 }
 
 void
-nsSVGFilterProperty::DoUpdate()
+nsSVGFilterProperty::OnRenderingChange()
 {
   nsIFrame* frame = mFrameReference.Get();
   if (!frame)
     return;
 
   // Repaint asynchronously in case the filter frame is being torn down
   nsChangeHint changeHint =
     nsChangeHint(nsChangeHint_RepaintFrame);
 
-  // Since we don't call nsSVGRenderingObserverProperty::DoUpdate, we have to
-  // add this bit ourselves.
+  // Since we don't call nsSVGRenderingObserverProperty::
+  // OnRenderingChange, we have to add this bit ourselves.
   if (frame->HasAllStateBits(NS_FRAME_SVG_LAYOUT)) {
     // Changes should propagate out to things that might be observing
     // the referencing frame or its ancestors.
     changeHint |= nsChangeHint_InvalidateRenderingObservers;
   }
 
   // Don't need to request UpdateOverflow if we're being reflowed.
   if (!(frame->GetStateBits() & NS_FRAME_IN_REFLOW)) {
     changeHint |= nsChangeHint_UpdateOverflow;
   }
   frame->PresContext()->RestyleManager()->PostRestyleEvent(
     frame->GetContent()->AsElement(), nsRestyleHint(0), changeHint);
 }
 
 void
-nsSVGMarkerProperty::DoUpdate()
+nsSVGMarkerProperty::OnRenderingChange()
 {
-  nsSVGRenderingObserverProperty::DoUpdate();
+  nsSVGRenderingObserverProperty::OnRenderingChange();
 
   nsIFrame* frame = mFrameReference.Get();
   if (!frame)
     return;
 
   NS_ASSERTION(frame->IsFrameOfType(nsIFrame::eSVG), "SVG frame expected");
 
   // Don't need to request ReflowFrame if we're being reflowed.
@@ -437,19 +437,19 @@ nsSVGMaskProperty::ResolveImage(uint32_t
 bool
 nsSVGTextPathProperty::TargetIsValid()
 {
   Element* target = GetTarget();
   return target && target->IsSVGElement(nsGkAtoms::path);
 }
 
 void
-nsSVGTextPathProperty::DoUpdate()
+nsSVGTextPathProperty::OnRenderingChange()
 {
-  nsSVGRenderingObserverProperty::DoUpdate();
+  nsSVGRenderingObserverProperty::OnRenderingChange();
 
   nsIFrame* frame = mFrameReference.Get();
   if (!frame)
     return;
 
   NS_ASSERTION(frame->IsFrameOfType(nsIFrame::eSVG) ||
                nsSVGUtils::IsInSVGTextSubtree(frame),
                "SVG frame expected");
@@ -483,19 +483,19 @@ InvalidateAllContinuations(nsIFrame* aFr
 {
   for (nsIFrame* f = aFrame; f;
        f = nsLayoutUtils::GetNextContinuationOrIBSplitSibling(f)) {
     f->InvalidateFrame();
   }
 }
 
 void
-nsSVGPaintingProperty::DoUpdate()
+nsSVGPaintingProperty::OnRenderingChange()
 {
-  nsSVGRenderingObserverProperty::DoUpdate();
+  nsSVGRenderingObserverProperty::OnRenderingChange();
 
   nsIFrame* frame = mFrameReference.Get();
   if (!frame)
     return;
 
   if (frame->GetStateBits() & NS_FRAME_SVG_LAYOUT) {
     frame->InvalidateFrameSubtree();
   } else {
@@ -754,17 +754,18 @@ SVGObserverUtils::UpdateEffects(nsIFrame
   aFrame->DeleteProperty(MarkerBeginProperty());
   aFrame->DeleteProperty(MarkerMiddleProperty());
   aFrame->DeleteProperty(MarkerEndProperty());
   aFrame->DeleteProperty(FillProperty());
   aFrame->DeleteProperty(StrokeProperty());
   aFrame->DeleteProperty(BackgroundImageProperty());
 
   // Ensure that the filter is repainted correctly
-  // We can't do that in DoUpdate as the referenced frame may not be valid
+  // We can't do that in OnRenderingChange as the referenced frame may
+  // not be valid
   GetOrCreateFilterProperty(aFrame);
 
   if (aFrame->IsSVGGeometryFrame() &&
       static_cast<SVGGeometryElement*>(aFrame->GetContent())->IsMarkable()) {
     // Set marker properties here to avoid reference loops
     nsCOMPtr<nsIURI> markerURL =
       GetMarkerURI(aFrame, &nsStyleSVG::mMarkerStart);
     GetMarkerProperty(markerURL, aFrame, MarkerBeginProperty());
diff --git a/layout/svg/SVGObserverUtils.h b/layout/svg/SVGObserverUtils.h
--- a/layout/svg/SVGObserverUtils.h
+++ b/layout/svg/SVGObserverUtils.h
@@ -33,19 +33,19 @@ class nsSVGMaskFrame;
 class nsSVGFilterChainObserver;
 
 /*
  * This interface allows us to be notified when a piece of SVG content is
  * re-rendered.
  *
  * Concrete implementations of this interface need to implement
  * "GetTarget()" to specify the piece of SVG content that they'd like to
- * monitor, and they need to implement "DoUpdate" to specify how we'll react
- * when that content gets re-rendered. They also need to implement a
- * constructor and destructor, which should call StartObserving and
+ * monitor, and they need to implement "OnRenderingChange" to specify how
+ * we'll react when that content gets re-rendered. They also need to implement
+ * a constructor and destructor, which should call StartObserving and
  * StopObserving, respectively.
  */
 class nsSVGRenderingObserver : public nsStubMutationObserver
 {
 
 protected:
   virtual ~nsSVGRenderingObserver()
     {}
@@ -78,22 +78,32 @@ public:
    */
   nsIFrame* GetReferencedFrame(mozilla::LayoutFrameType aFrameType, bool* aOK);
 
   Element* GetReferencedElement();
 
   virtual bool ObservesReflow() { return true; }
 
 protected:
-  // Non-virtual protected methods
   void StartObserving();
   void StopObserving();
 
-  // Virtual protected methods
-  virtual void DoUpdate() = 0; // called when the referenced resource changes.
+  /**
+   * Called whenever the rendering of the observed element may have changed.
+   *
+   * More specifically, this method is called whenever DOM mutation occurs in
+   * the observed element's subtree, or whenever
+   * SVGObserverUtils::InvalidateRenderingObservers or
+   * SVGObserverUtils::InvalidateDirectRenderingObservers is called for the
+   * observed element's frame.
+   *
+   * Subclasses should override this method to handle rendering changes
+   * appropriately.
+   */
+  virtual void OnRenderingChange() = 0;
 
   // This is an internally-used version of GetReferencedElement that doesn't
   // forcibly add us as an observer. (whereas GetReferencedElement does)
   virtual Element* GetTarget() = 0;
 
   // Whether we're in our referenced element's observer list at this time.
   bool mInObserverList;
 };
@@ -117,18 +127,17 @@ public:
 
   nsSVGIDRenderingObserver(nsIURI* aURI, nsIContent* aObservingContent,
                          bool aReferenceImage);
   virtual ~nsSVGIDRenderingObserver();
 
 protected:
   Element* GetTarget() override { return mObservedElementTracker.get(); }
 
-  // This is called when the referenced resource changes.
-  virtual void DoUpdate() override;
+  void OnRenderingChange() override;
 
   /**
    * Helper that provides a reference to the element with the ID that our
    * observer wants to observe, and that will invalidate our observer if the
    * element that that ID identifies changes to a different element (or none).
    */
   class ElementTracker final : public IDTracker
   {
@@ -136,17 +145,17 @@ protected:
     explicit ElementTracker(nsSVGIDRenderingObserver* aOwningObserver)
       : mOwningObserver(aOwningObserver)
     {}
   protected:
     virtual void ElementChanged(Element* aFrom, Element* aTo) override {
       mOwningObserver->StopObserving(); // stop observing the old element
       IDTracker::ElementChanged(aFrom, aTo);
       mOwningObserver->StartObserving(); // start observing the new element
-      mOwningObserver->DoUpdate();
+      mOwningObserver->OnRenderingChange();
     }
     /**
      * Override IsPersistent because we want to keep tracking the element
      * for the ID even when it changes.
      */
     virtual bool IsPersistent() override { return true; }
   private:
     nsSVGIDRenderingObserver* mOwningObserver;
@@ -190,17 +199,17 @@ public:
                                  bool aReferenceImage)
     : nsSVGIDRenderingObserver(aURI, aFrame->GetContent(), aReferenceImage)
     , mFrameReference(aFrame)
   {}
 
 protected:
   virtual ~nsSVGRenderingObserverProperty() {}
 
-  virtual void DoUpdate() override;
+  virtual void OnRenderingChange() override;
 
   nsSVGFrameReferenceFromProperty mFrameReference;
 };
 
 /**
  * In a filter chain, there can be multiple SVG reference filters.
  * e.g. filter: url(#svg-filter-1) blur(10px) url(#svg-filter-2);
  *
@@ -233,23 +242,23 @@ public:
    */
   nsSVGFilterFrame *GetFilterFrame();
 
   // nsISupports
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_CLASS_AMBIGUOUS(nsSVGFilterReference, nsSVGIDRenderingObserver)
 
   // nsISVGFilterReference
-  virtual void Invalidate() override { DoUpdate(); };
+  virtual void Invalidate() override { OnRenderingChange(); };
 
 protected:
   virtual ~nsSVGFilterReference() {}
 
   // nsSVGIDRenderingObserver
-  virtual void DoUpdate() override;
+  virtual void OnRenderingChange() override;
 
 private:
   nsSVGFilterChainObserver* mFilterChainObserver;
 };
 
 /**
  * This class manages a list of nsSVGFilterReferences, which represent SVG
  * reference filters in a filter chain.
@@ -264,26 +273,26 @@ class nsSVGFilterChainObserver : public 
 {
 public:
   nsSVGFilterChainObserver(const nsTArray<nsStyleFilter>& aFilters,
                            nsIContent* aFilteredElement,
                            nsIFrame* aFiltedFrame = nullptr);
 
   bool ReferencesValidResources();
   bool IsInObserverLists() const;
-  void Invalidate() { DoUpdate(); }
+  void Invalidate() { OnRenderingChange(); }
 
   // nsISupports
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_CLASS(nsSVGFilterChainObserver)
 
 protected:
   virtual ~nsSVGFilterChainObserver();
 
-  virtual void DoUpdate() = 0;
+  virtual void OnRenderingChange() = 0;
 
 private:
 
   void DetachReferences()
   {
     for (uint32_t i = 0; i < mReferences.Length(); i++) {
       mReferences[i]->DetachFromChainObserver();
     }
@@ -300,42 +309,42 @@ public:
     : nsSVGFilterChainObserver(aFilters, aFilteredFrame->GetContent(),
                                aFilteredFrame)
     , mFrameReference(aFilteredFrame)
   {}
 
   void DetachFromFrame() { mFrameReference.Detach(); }
 
 protected:
-  virtual void DoUpdate() override;
+  virtual void OnRenderingChange() override;
 
   nsSVGFrameReferenceFromProperty mFrameReference;
 };
 
 class nsSVGMarkerProperty final: public nsSVGRenderingObserverProperty
 {
 public:
   nsSVGMarkerProperty(nsIURI* aURI, nsIFrame* aFrame, bool aReferenceImage)
     : nsSVGRenderingObserverProperty(aURI, aFrame, aReferenceImage) {}
 
 protected:
-  virtual void DoUpdate() override;
+  virtual void OnRenderingChange() override;
 };
 
 class nsSVGTextPathProperty final : public nsSVGRenderingObserverProperty
 {
 public:
   nsSVGTextPathProperty(nsIURI* aURI, nsIFrame* aFrame, bool aReferenceImage)
     : nsSVGRenderingObserverProperty(aURI, aFrame, aReferenceImage)
     , mValid(true) {}
 
   virtual bool ObservesReflow() override { return false; }
 
 protected:
-  virtual void DoUpdate() override;
+  virtual void OnRenderingChange() override;
 
 private:
   /**
    * Returns true if the target of the textPath is the frame of a 'path' element.
    */
   bool TargetIsValid();
 
   bool mValid;
@@ -343,17 +352,17 @@ private:
 
 class nsSVGPaintingProperty final : public nsSVGRenderingObserverProperty
 {
 public:
   nsSVGPaintingProperty(nsIURI* aURI, nsIFrame* aFrame, bool aReferenceImage)
     : nsSVGRenderingObserverProperty(aURI, aFrame, aReferenceImage) {}
 
 protected:
-  virtual void DoUpdate() override;
+  virtual void OnRenderingChange() override;
 };
 
 class nsSVGMaskProperty final : public nsISupports
 {
 public:
   explicit nsSVGMaskProperty(nsIFrame* aFrame);
 
   // nsISupports
