# HG changeset patch
# User Ryan Hunt <rhunt@eqrion.net>
# Date 1507833119 14400
# Node ID 4770f57dadb853186ec74399f22f5fe38f8363bd
# Parent  2bdf755657a4a452303bbfd9f66b0a32e846d1f7
Block the main thread to wait for async paints to complete instead of for IPC resuming. (bug 1406960, r=dvander)

MozReview-Commit-ID: KTd25x2epkC

diff --git a/gfx/layers/ipc/CompositorBridgeChild.cpp b/gfx/layers/ipc/CompositorBridgeChild.cpp
--- a/gfx/layers/ipc/CompositorBridgeChild.cpp
+++ b/gfx/layers/ipc/CompositorBridgeChild.cpp
@@ -88,17 +88,17 @@ CompositorBridgeChild::CompositorBridgeC
   , mFwdTransactionId(0)
   , mDeviceResetSequenceNumber(0)
   , mMessageLoop(MessageLoop::current())
   , mProcessToken(0)
   , mSectionAllocator(nullptr)
   , mPaintLock("CompositorBridgeChild.mPaintLock")
   , mOutstandingAsyncPaints(0)
   , mOutstandingAsyncEndTransaction(false)
-  , mIsWaitingForPaint(false)
+  , mIsDelayingForAsyncPaints(false)
 {
   MOZ_ASSERT(NS_IsMainThread());
 }
 
 CompositorBridgeChild::~CompositorBridgeChild()
 {
   if (mCanSend) {
     gfxCriticalError() << "CompositorBridgeChild was not deinitialized";
@@ -1134,17 +1134,17 @@ CompositorBridgeChild::GetNextPipelineId
 }
 
 void
 CompositorBridgeChild::FlushAsyncPaints()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   MonitorAutoLock lock(mPaintLock);
-  while (mIsWaitingForPaint) {
+  while (mOutstandingAsyncPaints > 0 || mOutstandingAsyncEndTransaction) {
     lock.Wait();
   }
 
   // It's now safe to free any TextureClients that were used during painting.
   mTextureClientsForAsyncPaint.Clear();
 }
 
 void
@@ -1152,17 +1152,17 @@ CompositorBridgeChild::NotifyBeginAsyncP
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   MonitorAutoLock lock(mPaintLock);
 
   // We must not be waiting for paints to complete yet. This would imply we
   // started a new paint without waiting for a previous one, which could lead to
   // incorrect rendering or IPDL deadlocks.
-  MOZ_ASSERT(!mIsWaitingForPaint);
+  MOZ_ASSERT(!mIsDelayingForAsyncPaints);
 
   mOutstandingAsyncPaints++;
 
   // Mark texture clients that they are being used for async painting, and
   // make sure we hold them alive on the main thread.
   aState->mTextureClient->AddPaintThreadRef();
   mTextureClientsForAsyncPaint.AppendElement(aState->mTextureClient);
   if (aState->mTextureClientOnWhite) {
@@ -1221,57 +1221,58 @@ CompositorBridgeChild::NotifyFinishedAsy
   // Since this should happen after ALL paints are done and
   // at the end of a transaction, this should always be true.
   MOZ_RELEASE_ASSERT(mOutstandingAsyncPaints == 0);
   MOZ_ASSERT(mOutstandingAsyncEndTransaction);
 
   mOutstandingAsyncEndTransaction = false;
 
   // It's possible that we painted so fast that the main thread never reached
-  // the code that starts delaying messages. If so, mIsWaitingForPaint will be
+  // the code that starts delaying messages. If so, mIsDelayingForAsyncPaints will be
   // false, and we can safely return.
-  if (mIsWaitingForPaint) {
+  if (mIsDelayingForAsyncPaints) {
     ResumeIPCAfterAsyncPaint();
+  }
 
-    // Notify the main thread in case it's blocking. We do this unconditionally
-    // to avoid deadlocking.
-    lock.Notify();
-  }
+  // Notify the main thread in case it's blocking. We do this unconditionally
+  // to avoid deadlocking.
+  lock.Notify();
 }
 
 void
 CompositorBridgeChild::ResumeIPCAfterAsyncPaint()
 {
   // Note: the caller is responsible for holding the lock.
   mPaintLock.AssertCurrentThreadOwns();
   MOZ_ASSERT(PaintThread::IsOnPaintThread());
   MOZ_ASSERT(mOutstandingAsyncPaints == 0);
-  MOZ_ASSERT(mIsWaitingForPaint);
+  MOZ_ASSERT(!mOutstandingAsyncEndTransaction);
+  MOZ_ASSERT(mIsDelayingForAsyncPaints);
 
-  mIsWaitingForPaint = false;
+  mIsDelayingForAsyncPaints = false;
 
   // It's also possible that the channel has shut down already.
   if (!mCanSend || mActorDestroyed) {
     return;
   }
 
   GetIPCChannel()->StopPostponingSends();
 }
 
 void
 CompositorBridgeChild::PostponeMessagesIfAsyncPainting()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   MonitorAutoLock lock(mPaintLock);
 
-  MOZ_ASSERT(!mIsWaitingForPaint);
+  MOZ_ASSERT(!mIsDelayingForAsyncPaints);
 
   // We need to wait for async paints and the async end transaction as
   // it will do texture synchronization
   if (mOutstandingAsyncPaints > 0 || mOutstandingAsyncEndTransaction) {
-    mIsWaitingForPaint = true;
+    mIsDelayingForAsyncPaints = true;
     GetIPCChannel()->BeginPostponingSends();
   }
 }
 
 } // namespace layers
 } // namespace mozilla
diff --git a/gfx/layers/ipc/CompositorBridgeChild.h b/gfx/layers/ipc/CompositorBridgeChild.h
--- a/gfx/layers/ipc/CompositorBridgeChild.h
+++ b/gfx/layers/ipc/CompositorBridgeChild.h
@@ -379,15 +379,15 @@ private:
   size_t mOutstandingAsyncPaints;
 
   // Whether we are waiting for an async paint end transaction
   bool mOutstandingAsyncEndTransaction;
 
   // True if this CompositorBridge is currently delaying its messages until the
   // paint thread completes. This is R/W on both the main and paint threads, and
   // must be accessed within the paint lock.
-  bool mIsWaitingForPaint;
+  bool mIsDelayingForAsyncPaints;
 };
 
 } // namespace layers
 } // namespace mozilla
 
 #endif // mozilla_layers_CompositorBrigedChild_h
