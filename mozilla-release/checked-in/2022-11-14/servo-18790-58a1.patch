# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1507608255 18000
#      Mon Oct 09 23:04:15 2017 -0500
# Node ID 1849927185c693582bc21c3a04ca50d0e5cd906f
# Parent  b3e54403561fd222420c4e7743b8b9f15c8a7957
servo: Merge #18790 - Support :scope pseudo-class (from upsuper:scope); r=emilio

This fixes [bug 1406817](https://bugzilla.mozilla.org/show_bug.cgi?id=1406817).

Source-Repo: https://github.com/servo/servo
Source-Revision: bbb2a3cde9f4c7fc9debd5784fce2b07966101ff

diff --git a/servo/components/selectors/builder.rs b/servo/components/selectors/builder.rs
--- a/servo/components/selectors/builder.rs
+++ b/servo/components/selectors/builder.rs
@@ -279,17 +279,17 @@ fn complex_selector_specificity<Impl>(mu
             }
             Component::Class(..) |
             Component::AttributeInNoNamespace { .. } |
             Component::AttributeInNoNamespaceExists { .. } |
             Component::AttributeOther(..) |
 
             Component::FirstChild | Component::LastChild |
             Component::OnlyChild | Component::Root |
-            Component::Empty |
+            Component::Empty | Component::Scope |
             Component::NthChild(..) |
             Component::NthLastChild(..) |
             Component::NthOfType(..) |
             Component::NthLastOfType(..) |
             Component::FirstOfType | Component::LastOfType |
             Component::OnlyOfType |
             Component::NonTSPseudoClass(..) => {
                 specificity.class_like_selectors += 1
diff --git a/servo/components/selectors/context.rs b/servo/components/selectors/context.rs
--- a/servo/components/selectors/context.rs
+++ b/servo/components/selectors/context.rs
@@ -1,15 +1,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 use attr::CaseSensitivity;
 use bloom::BloomFilter;
 use nth_index_cache::NthIndexCache;
+use tree::OpaqueElement;
 
 /// What kind of selector matching mode we should use.
 ///
 /// There are two modes of selector matching. The difference is only noticeable
 /// in presence of pseudo-elements.
 #[derive(Clone, Copy, Debug, PartialEq)]
 pub enum MatchingMode {
     /// Don't ignore any pseudo-element selectors.
@@ -83,16 +84,29 @@ pub struct MatchingContext<'a> {
     /// Input that controls how matching for links is handled.
     pub visited_handling: VisitedHandlingMode,
     /// Output that records whether we encountered a "relevant link" while
     /// matching _any_ selector for this element. (This differs from
     /// `RelevantLinkStatus` which tracks the status for the _current_ selector
     /// only.)
     pub relevant_link_found: bool,
 
+    /// The element which is going to match :scope pseudo-class. It can be
+    /// either one :scope element, or the scoping element.
+    ///
+    /// Note that, although in theory there can be multiple :scope elements,
+    /// in current specs, at most one is specified, and when there is one,
+    /// scoping element is not relevant anymore, so we use a single field for
+    /// them.
+    ///
+    /// When this is None, :scope will match the root element.
+    ///
+    /// See https://drafts.csswg.org/selectors-4/#scope-pseudo
+    pub scope_element: Option<OpaqueElement>,
+
     quirks_mode: QuirksMode,
     classes_and_ids_case_sensitivity: CaseSensitivity,
 }
 
 impl<'a> MatchingContext<'a> {
     /// Constructs a new `MatchingContext`.
     pub fn new(
         matching_mode: MatchingMode,
@@ -120,16 +134,17 @@ impl<'a> MatchingContext<'a> {
         Self {
             matching_mode,
             bloom_filter,
             visited_handling,
             nth_index_cache,
             quirks_mode,
             relevant_link_found: false,
             classes_and_ids_case_sensitivity: quirks_mode.classes_and_ids_case_sensitivity(),
+            scope_element: None,
         }
     }
 
     /// The quirks mode of the document.
     #[inline]
     pub fn quirks_mode(&self) -> QuirksMode {
         self.quirks_mode
     }
diff --git a/servo/components/selectors/matching.rs b/servo/components/selectors/matching.rs
--- a/servo/components/selectors/matching.rs
+++ b/servo/components/selectors/matching.rs
@@ -719,16 +719,22 @@ fn matches_simple_selector<E, F>(
         }
         Component::Root => {
             element.is_root()
         }
         Component::Empty => {
             flags_setter(element, HAS_EMPTY_SELECTOR);
             element.is_empty()
         }
+        Component::Scope => {
+            match context.shared.scope_element {
+                Some(ref scope_element) => element.opaque() == *scope_element,
+                None => element.is_root(),
+            }
+        }
         Component::NthChild(a, b) => {
             matches_generic_nth_child(element, context, a, b, false, false, flags_setter)
         }
         Component::NthLastChild(a, b) => {
             matches_generic_nth_child(element, context, a, b, false, true, flags_setter)
         }
         Component::NthOfType(a, b) => {
             matches_generic_nth_child(element, context, a, b, true, false, flags_setter)
diff --git a/servo/components/selectors/parser.rs b/servo/components/selectors/parser.rs
--- a/servo/components/selectors/parser.rs
+++ b/servo/components/selectors/parser.rs
@@ -668,16 +668,17 @@ pub enum Component<Impl: SelectorImpl> {
     // Note: if/when we upgrade this to CSS4, which supports combinators, we
     // need to think about how this should interact with visit_complex_selector,
     // and what the consumers of those APIs should do about the presence of
     // combinators in negation.
     Negation(Box<[Component<Impl>]>),
     FirstChild, LastChild, OnlyChild,
     Root,
     Empty,
+    Scope,
     NthChild(i32, i32),
     NthLastChild(i32, i32),
     NthOfType(i32, i32),
     NthLastOfType(i32, i32),
     FirstOfType,
     LastOfType,
     OnlyOfType,
     NonTSPseudoClass(Impl::NonTSPseudoClass),
@@ -964,16 +965,17 @@ impl<Impl: SelectorImpl> ToCss for Compo
                 dest.write_str(")")
             }
 
             FirstChild => dest.write_str(":first-child"),
             LastChild => dest.write_str(":last-child"),
             OnlyChild => dest.write_str(":only-child"),
             Root => dest.write_str(":root"),
             Empty => dest.write_str(":empty"),
+            Scope => dest.write_str(":scope"),
             FirstOfType => dest.write_str(":first-of-type"),
             LastOfType => dest.write_str(":last-of-type"),
             OnlyOfType => dest.write_str(":only-of-type"),
             NthChild(a, b) | NthLastChild(a, b) | NthOfType(a, b) | NthLastOfType(a, b) => {
                 match *self {
                     NthChild(_, _) => dest.write_str(":nth-child(")?,
                     NthLastChild(_, _) => dest.write_str(":nth-last-child(")?,
                     NthOfType(_, _) => dest.write_str(":nth-of-type(")?,
@@ -1694,16 +1696,17 @@ fn parse_simple_pseudo_class<'i, P, E, I
     where P: Parser<'i, Impl=Impl, Error=E>, Impl: SelectorImpl
 {
     (match_ignore_ascii_case! { &name,
         "first-child" => Ok(Component::FirstChild),
         "last-child"  => Ok(Component::LastChild),
         "only-child"  => Ok(Component::OnlyChild),
         "root" => Ok(Component::Root),
         "empty" => Ok(Component::Empty),
+        "scope" => Ok(Component::Scope),
         "first-of-type" => Ok(Component::FirstOfType),
         "last-of-type"  => Ok(Component::LastOfType),
         "only-of-type"  => Ok(Component::OnlyOfType),
         _ => Err(())
     }).or_else(|()| {
         P::parse_non_ts_pseudo_class(parser, name)
             .map(Component::NonTSPseudoClass)
     })
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -1525,16 +1525,17 @@ pub unsafe extern "C" fn Servo_SelectorL
 ) -> bool {
     let element = GeckoElement(element);
     let mut context = MatchingContext::new(
         MatchingMode::Normal,
         None,
         None,
         element.owner_document_quirks_mode(),
     );
+    context.scope_element = Some(element.opaque());
 
     selectors::matching::matches_selector_list(selectors, &element, &mut context)
 }
 
 #[no_mangle]
 pub extern "C" fn Servo_ImportRule_GetHref(rule: RawServoImportRuleBorrowed, result: *mut nsAString) {
     read_locked_arc(rule, |rule: &ImportRule| {
         write!(unsafe { &mut *result }, "{}", rule.url.as_str()).unwrap();
