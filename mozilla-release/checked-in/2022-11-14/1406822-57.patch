# HG changeset patch
# User Aaron Klotz <aklotz@mozilla.com>
# Date 1508274087 21600
#      Tue Oct 17 15:01:27 2017 -0600
# Node ID c19fd45614748b503df767817f0ad2b7a197ee31
# Parent  15e157c5a6ea164cff4e5e2a84758ed614ee6107
Bug 1406822 - Implement a11y::HandlerProvider::GetEffectiveOutParamIid. r=Jamie, a=ritu

MozReview-Commit-ID: BKH3ooMQvN1

diff --git a/accessible/ipc/win/HandlerProvider.cpp b/accessible/ipc/win/HandlerProvider.cpp
--- a/accessible/ipc/win/HandlerProvider.cpp
+++ b/accessible/ipc/win/HandlerProvider.cpp
@@ -4,16 +4,18 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #define INITGUID
 
 #include "mozilla/a11y/HandlerProvider.h"
 
 #include "Accessible2_3.h"
+#include "AccessibleTable.h"
+#include "AccessibleTable2.h"
 #include "HandlerData.h"
 #include "HandlerData_i.c"
 #include "mozilla/Assertions.h"
 #include "mozilla/a11y/AccessibleWrap.h"
 #include "mozilla/dom/ContentChild.h"
 #include "mozilla/Move.h"
 #include "mozilla/mscom/AgileReference.h"
 #include "mozilla/mscom/FastMarshaler.h"
@@ -209,16 +211,28 @@ HandlerProvider::MarshalAs(REFIID aIid)
       aIid == IID_IAccessible2_3) {
     // This should always be the newest IA2 interface ID
     return NEWEST_IA2_IID;
   }
   // Otherwise we juse return the identity.
   return aIid;
 }
 
+REFIID
+HandlerProvider::GetEffectiveOutParamIid(REFIID aCallIid,
+                                         ULONG aCallMethod)
+{
+  if (aCallIid == IID_IAccessibleTable || aCallIid == IID_IAccessibleTable2) {
+    return IID_IAccessible2_3;
+  }
+
+  MOZ_ASSERT(false);
+  return IID_IUnknown;
+}
+
 HRESULT
 HandlerProvider::NewInstance(REFIID aIid,
                              mscom::InterceptorTargetPtr<IUnknown> aTarget,
                              NotNull<mscom::IHandlerProvider**> aOutNewPayload)
 {
   RefPtr<IHandlerProvider> newPayload(new HandlerProvider(aIid, Move(aTarget)));
   newPayload.forget(aOutNewPayload.get());
   return S_OK;
diff --git a/accessible/ipc/win/HandlerProvider.h b/accessible/ipc/win/HandlerProvider.h
--- a/accessible/ipc/win/HandlerProvider.h
+++ b/accessible/ipc/win/HandlerProvider.h
@@ -2,17 +2,17 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_a11y_HandlerProvider_h
 #define mozilla_a11y_HandlerProvider_h
 
-#include "handler/AccessibleHandler.h"
+#include "mozilla/a11y/AccessibleHandler.h"
 #include "mozilla/AlreadyAddRefed.h"
 #include "mozilla/Atomics.h"
 #include "mozilla/mscom/IHandlerProvider.h"
 #include "mozilla/mscom/Ptr.h"
 #include "mozilla/mscom/StructStream.h"
 #include "mozilla/Mutex.h"
 #include "mozilla/UniquePtr.h"
 
@@ -39,16 +39,18 @@ public:
   STDMETHODIMP_(ULONG) AddRef() override;
   STDMETHODIMP_(ULONG) Release() override;
 
   // IHandlerProvider
   STDMETHODIMP GetHandler(NotNull<CLSID*> aHandlerClsid) override;
   STDMETHODIMP GetHandlerPayloadSize(NotNull<DWORD*> aOutPayloadSize) override;
   STDMETHODIMP WriteHandlerPayload(NotNull<IStream*> aStream) override;
   STDMETHODIMP_(REFIID) MarshalAs(REFIID aIid) override;
+  STDMETHODIMP_(REFIID) GetEffectiveOutParamIid(REFIID aCallIid,
+                                                ULONG aCallMethod) override;
   STDMETHODIMP NewInstance(REFIID aIid,
                            mscom::InterceptorTargetPtr<IUnknown> aTarget,
                            NotNull<mscom::IHandlerProvider**> aOutNewPayload) override;
 
   // IGeckoBackChannel
   STDMETHODIMP put_HandlerControl(long aPid, IHandlerControl* aCtrl) override;
   STDMETHODIMP Refresh(IA2Data* aOutData) override;
 
