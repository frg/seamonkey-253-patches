# HG changeset patch
# User Henrik Skupin <mail@hskupin.info>
# Date 1510910678 -3600
# Node ID 4eb0dd155ef28ef6a3b55d91cfa175d0ae919b03
# Parent  7d2caf1a977799a6935cd8c3ef6d78e0f32c8445
Bug 1418227 - Indicate that sock instance is private. r=automatedtester, a=test-only

The sock instance in TcpTransport should not be modified by code
outside of the class. As such indicate it least as private.

MozReview-Commit-ID: FGgZHYj7EdV

diff --git a/testing/marionette/client/marionette_driver/transport.py b/testing/marionette/client/marionette_driver/transport.py
--- a/testing/marionette/client/marionette_driver/transport.py
+++ b/testing/marionette/client/marionette_driver/transport.py
@@ -101,31 +101,31 @@ class TcpTransport(object):
         self.addr = addr
         self.port = port
         self._socket_timeout = socket_timeout
 
         self.protocol = self.min_protocol_level
         self.application_type = None
         self.last_id = 0
         self.expected_response = None
-        self.sock = None
+        self._sock = None
 
     @property
     def socket_timeout(self):
-        if self.sock:
-            return self.sock.gettimeout()
+        if self._sock:
+            return self._sock.gettimeout()
 
         return self._socket_timeout
 
     @socket_timeout.setter
     def socket_timeout(self, value):
         self._socket_timeout = value
 
-        if self.sock:
-            self.sock.settimeout(value)
+        if self._sock:
+            self._sock.settimeout(value)
 
     def _unmarshal(self, packet):
         msg = None
 
         # protocol 3 and above
         if self.protocol >= 3:
             typ = int(packet[1])
             if typ == Command.TYPE:
@@ -143,17 +143,17 @@ class TcpTransport(object):
             the raw packet.
         """
         now = time.time()
         data = ""
         bytes_to_recv = 10
 
         while self.socket_timeout is None or (time.time() - now < self.socket_timeout):
             try:
-                chunk = self.sock.recv(bytes_to_recv)
+                chunk = self._sock.recv(bytes_to_recv)
                 data += chunk
             except socket.timeout:
                 pass
             else:
                 if not chunk:
                     raise socket.error("No data received over socket")
 
             sep = data.find(":")
@@ -182,28 +182,28 @@ class TcpTransport(object):
 
     def connect(self):
         """Connect to the server and process the hello message we expect
         to receive in response.
 
         Returns a tuple of the protocol level and the application type.
         """
         try:
-            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
-            self.sock.settimeout(self.socket_timeout)
+            self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
+            self._sock.settimeout(self.socket_timeout)
 
-            self.sock.connect((self.addr, self.port))
+            self._sock.connect((self.addr, self.port))
         except Exception:
-            # Unset self.sock so that the next attempt to send will cause
+            # Unset so that the next attempt to send will cause
             # another connection attempt.
-            self.sock = None
+            self._sock = None
             raise
 
         try:
-            with SocketTimeout(self.sock, 60.0):
+            with SocketTimeout(self._sock, 60.0):
                 # first packet is always a JSON Object
                 # which we can use to tell which protocol level we are at
                 raw = self.receive(unmarshal=False)
         except socket.timeout:
             msg = "Connection attempt failed because no data has been received over the socket: {}"
             exc, val, tb = sys.exc_info()
 
             raise exc, msg.format(val), tb
@@ -223,30 +223,30 @@ class TcpTransport(object):
         self.protocol = protocol
 
         return (self.protocol, self.application_type)
 
     def send(self, obj):
         """Send message to the remote server.  Allowed input is a
         ``Message`` instance or a JSON serialisable object.
         """
-        if not self.sock:
+        if not self._sock:
             self.connect()
 
         if isinstance(obj, Message):
             data = obj.to_msg()
             if isinstance(obj, Command):
                 self.expected_response = obj
         else:
             data = json.dumps(obj)
         payload = "{0}:{1}".format(len(data), data)
 
         totalsent = 0
         while totalsent < len(payload):
-            sent = self.sock.send(payload[totalsent:])
+            sent = self._sock.send(payload[totalsent:])
             if sent == 0:
                 raise IOError("Socket error after sending {0} of {1} bytes"
                               .format(totalsent, len(payload)))
             else:
                 totalsent += sent
 
     def respond(self, obj):
         """Send a response to a command.  This can be an arbitrary JSON
@@ -273,23 +273,23 @@ class TcpTransport(object):
     def close(self):
         """Close the socket.
 
         First forces the socket to not send data anymore, and then explicitly
         close it to free up its resources.
 
         See: https://docs.python.org/2/howto/sockets.html#disconnecting
         """
-        if self.sock:
+        if self._sock:
             try:
-                self.sock.shutdown(socket.SHUT_RDWR)
+                self._sock.shutdown(socket.SHUT_RDWR)
             except IOError as exc:
                 # If the socket is already closed, don't care about:
                 #   Errno  57: Socket not connected
                 #   Errno 107: Transport endpoint is not connected
                 if exc.errno not in (57, 107):
                     raise
 
-            self.sock.close()
-            self.sock = None
+            self._sock.close()
+            self._sock = None
 
     def __del__(self):
         self.close()
