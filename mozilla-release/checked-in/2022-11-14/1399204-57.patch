# HG changeset patch
# User Ting-Yu Lin <tlin@mozilla.com>
# Date 1507797708 -28800
#      Thu Oct 12 16:41:48 2017 +0800
# Node ID 3098eb13218908aed87917d26d50975a5496aedb
# Parent  519fa27aa3a712dd2bc22f92dd9b53568764b36f
Bug 1399204 - Update test_long_press_to_select_when_partial_visible_word_is_selected(). r=mtseng, a=test-only

This test fails to test what it designed to test because the <input> is not
narrow enough to let 'b's overflow.

I added an <input> that have a fixed size to ensure 'B's overflows, and
remove the first long press on the first word.

MozReview-Commit-ID: 83zBj4yBXVk

diff --git a/layout/base/tests/marionette/test_accessiblecaret_selection_mode.py b/layout/base/tests/marionette/test_accessiblecaret_selection_mode.py
--- a/layout/base/tests/marionette/test_accessiblecaret_selection_mode.py
+++ b/layout/base/tests/marionette/test_accessiblecaret_selection_mode.py
@@ -23,16 +23,17 @@ def skip_if_not_rotatable(target):
     return wrapper
 
 
 class AccessibleCaretSelectionModeTestCase(MarionetteTestCase):
     '''Test cases for AccessibleCaret under selection mode.'''
     # Element IDs.
     _input_id = 'input'
     _input_padding_id = 'input-padding'
+    _input_size_id = 'input-size'
     _textarea_id = 'textarea'
     _textarea2_id = 'textarea2'
     _textarea_one_line_id = 'textarea-one-line'
     _textarea_rtl_id = 'textarea-rtl'
     _contenteditable_id = 'contenteditable'
     _contenteditable2_id = 'contenteditable2'
     _content_id = 'content'
     _content2_id = 'content2'
@@ -564,46 +565,39 @@ class AccessibleCaretSelectionModeTestCa
         )
 
         # If AccessibleCaretEventHub is initialized successfully, select a word
         # should work.
         self._test_long_press_to_select_a_word(content)
 
     def test_long_press_to_select_when_partial_visible_word_is_selected(self):
         self.open_test_html(self._selection_html)
-        el = self.marionette.find_element(By.ID, self._input_id)
+        el = self.marionette.find_element(By.ID, self._input_size_id)
         sel = SelectionManager(el)
 
-        # To successfully select the second word while the first word is being
-        # selected, use sufficient spaces between 'a' and 'b' to avoid the
-        # second caret covers on the second word.
-        original_content = 'aaaaaaaa          bbbbbbbb'
-        el.clear()
-        el.send_keys(original_content)
+        original_content = sel.content
         words = original_content.split()
 
-        # We cannot use self.long_press_on_word() directly since it has will
-        # change the cursor position which affects this test. We have to store
-        # the position of word 0 and word 1 before long-pressing to select the
-        # word.
+        # We cannot use self.long_press_on_word() for the second long press
+        # on the first word because it has side effect that changes the
+        # cursor position. We need to save the location of the first word to
+        # be used later.
         word0_x, word0_y = self.word_location(el, 0)
-        word1_x, word1_y = self.word_location(el, 1)
 
+        # Long press on the second word.
+        self.long_press_on_word(el, 1)
+        self.assertEqual(words[1], sel.selected_content)
+
+        # Long press on the first word.
         self.long_press_on_location(el, word0_x, word0_y)
         self.assertEqual(words[0], sel.selected_content)
 
-        self.long_press_on_location(el, word1_x, word1_y)
-        self.assertEqual(words[1], sel.selected_content)
-
-        self.long_press_on_location(el, word0_x, word0_y)
-        self.assertEqual(words[0], sel.selected_content)
-
-        # If the second carets is visible, it can be dragged to the position of
-        # the first caret. After that, selection will contain only the first
-        # character.
+        # If the second caret is visible, it can be dragged to the position
+        # of the first caret. After that, selection will contain only the
+        # first character.
         (caret1_x, caret1_y), (caret2_x, caret2_y) = sel.carets_location()
         self.actions.flick(el, caret2_x, caret2_y, caret1_x, caret1_y).perform()
         self.assertEqual(words[0][0], sel.selected_content)
 
     @parameterized(_input_id, el_id=_input_id)
     @parameterized(_input_padding_id, el_id=_input_padding_id)
     @parameterized(_textarea_one_line_id, el_id=_textarea_one_line_id)
     @parameterized(_contenteditable_id, el_id=_contenteditable_id)
diff --git a/testing/marionette/harness/marionette_harness/www/test_carets_selection.html b/testing/marionette/harness/marionette_harness/www/test_carets_selection.html
--- a/testing/marionette/harness/marionette_harness/www/test_carets_selection.html
+++ b/testing/marionette/harness/marionette_harness/www/test_carets_selection.html
@@ -15,16 +15,20 @@
       overflow: auto;
     }
     </style>
   </head>
   <body>
     <div>
       <input id="input" value="ABC DEF GHI">
       <input id="input-padding" style="padding: 1em;" value="ABC DEF GHI">
+
+      <!-- To successfully select 'B's when 'A' is selected, use sufficient
+           spaces between 'A's and 'B's to avoid the second caret covers 'B's. -->
+      <input size="16" id="input-size" value="AAAAAAAA          BBBBBBBB">
     </div>
     <br>
     <div>
       <textarea id="textarea" rows="4" cols="8">ABC DEF GHI JKL MNO PQR</textarea>
       <textarea id="textarea-one-line" rows="4" cols="12">ABC DEF GHI</textarea>
     </div>
     <br>
     <div><textarea dir="rtl" id="textarea-rtl" rows="8" cols="8">موزيلا فيرفكس موزيلا فيرفكس</textarea></div>
