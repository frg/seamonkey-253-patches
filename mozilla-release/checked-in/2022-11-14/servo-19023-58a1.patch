# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1509013059 18000
# Node ID b99354d99d8b1320416c9ad0959f5d829584e12e
# Parent  a5f80b7594f691866a543f9b1047fbb637ec6044
servo: Merge #19023 - style: Remove PresentationalHintsSynthesizer (from emilio:remove-pres-hints-trait); r=KiChjang

This is not really an useful abstraction, and I never knew how to spell it.

Source-Repo: https://github.com/servo/servo
Source-Revision: b2867c0afa8bc728c13ddfd56678170dfba98967

diff --git a/servo/components/style/dom.rs b/servo/components/style/dom.rs
--- a/servo/components/style/dom.rs
+++ b/servo/components/style/dom.rs
@@ -291,37 +291,26 @@ fn fmt_subtree<F, N: TNode>(f: &mut fmt:
             writeln!(f, "")?;
             fmt_subtree(f, stringify, kid, indent + 1)?;
         }
     }
 
     Ok(())
 }
 
-/// A trait used to synthesize presentational hints for HTML element attributes.
-pub trait PresentationalHintsSynthesizer {
-    /// Generate the proper applicable declarations due to presentational hints,
-    /// and insert them into `hints`.
-    fn synthesize_presentational_hints_for_legacy_attributes<V>(&self,
-                                                                visited_handling: VisitedHandlingMode,
-                                                                hints: &mut V)
-        where V: Push<ApplicableDeclarationBlock>;
-}
-
 /// The element trait, the main abstraction the style crate acts over.
 pub trait TElement
     : Eq
     + PartialEq
     + Debug
     + Hash
     + Sized
     + Copy
     + Clone
     + SelectorsElement<Impl = SelectorImpl>
-    + PresentationalHintsSynthesizer
 {
     /// The concrete node type.
     type ConcreteNode: TNode<ConcreteElement = Self>;
 
     /// A concrete children iterator type in order to iterate over the `Node`s.
     ///
     /// TODO(emilio): We should eventually replace this with the `impl Trait`
     /// syntax.
@@ -821,16 +810,26 @@ pub trait TElement
         &self,
         override_lang: Option<Option<AttrValue>>,
         value: &PseudoClassStringArg
     ) -> bool;
 
     /// Returns whether this element is the main body element of the HTML
     /// document it is on.
     fn is_html_document_body_element(&self) -> bool;
+
+    /// Generate the proper applicable declarations due to presentational hints,
+    /// and insert them into `hints`.
+    fn synthesize_presentational_hints_for_legacy_attributes<V>(
+        &self,
+        visited_handling: VisitedHandlingMode,
+        hints: &mut V,
+    )
+    where
+        V: Push<ApplicableDeclarationBlock>;
 }
 
 /// TNode and TElement aren't Send because we want to be careful and explicit
 /// about our parallel traversal. However, there are certain situations
 /// (including but not limited to the traversal) where we need to send DOM
 /// objects to other threads.
 ///
 /// That's the reason why `SendNode` exists.
diff --git a/servo/components/style/gecko/wrapper.rs b/servo/components/style/gecko/wrapper.rs
--- a/servo/components/style/gecko/wrapper.rs
+++ b/servo/components/style/gecko/wrapper.rs
@@ -15,18 +15,17 @@
 //! the separation between the style system implementation and everything else.
 
 use CaseSensitivityExt;
 use app_units::Au;
 use applicable_declarations::ApplicableDeclarationBlock;
 use atomic_refcell::{AtomicRefCell, AtomicRef, AtomicRefMut};
 use context::{QuirksMode, SharedStyleContext, PostAnimationTasks, UpdateAnimationsTasks};
 use data::ElementData;
-use dom::{LayoutIterator, NodeInfo, TElement, TNode};
-use dom::{OpaqueNode, PresentationalHintsSynthesizer};
+use dom::{LayoutIterator, NodeInfo, OpaqueNode, TElement, TNode};
 use element_state::{ElementState, DocumentState, NS_DOCUMENT_STATE_WINDOW_INACTIVE};
 use error_reporting::ParseErrorReporter;
 use font_metrics::{FontMetrics, FontMetricsProvider, FontMetricsQueryResult};
 use gecko::data::PerDocumentStyleData;
 use gecko::global_style_data::GLOBAL_STYLE_DATA;
 use gecko::selector_parser::{SelectorImpl, NonTSPseudoClass, PseudoElement};
 use gecko::snapshot_helpers;
 use gecko_bindings::bindings;
@@ -1550,33 +1549,17 @@ impl<'le> TElement for GeckoElement<'le>
         }
 
         if !self.is_html_element() {
             return false;
         }
 
         unsafe { bindings::Gecko_IsDocumentBody(self.0) }
     }
-}
 
-impl<'le> PartialEq for GeckoElement<'le> {
-    fn eq(&self, other: &Self) -> bool {
-        self.0 as *const _ == other.0 as *const _
-    }
-}
-
-impl<'le> Eq for GeckoElement<'le> {}
-
-impl<'le> Hash for GeckoElement<'le> {
-    fn hash<H: Hasher>(&self, state: &mut H) {
-        (self.0 as *const _).hash(state);
-    }
-}
-
-impl<'le> PresentationalHintsSynthesizer for GeckoElement<'le> {
     fn synthesize_presentational_hints_for_legacy_attributes<V>(
         &self,
         visited_handling: VisitedHandlingMode,
         hints: &mut V
     )
     where
         V: Push<ApplicableDeclarationBlock>,
     {
@@ -1714,16 +1697,30 @@ impl<'le> PresentationalHintsSynthesizer
         if ns == structs::kNameSpaceID_MathML as i32 {
             if self.get_local_name().as_ptr() == atom!("math").as_ptr() {
                 hints.push(MATHML_LANG_RULE.clone());
             }
         }
     }
 }
 
+impl<'le> PartialEq for GeckoElement<'le> {
+    fn eq(&self, other: &Self) -> bool {
+        self.0 as *const _ == other.0 as *const _
+    }
+}
+
+impl<'le> Eq for GeckoElement<'le> {}
+
+impl<'le> Hash for GeckoElement<'le> {
+    fn hash<H: Hasher>(&self, state: &mut H) {
+        (self.0 as *const _).hash(state);
+    }
+}
+
 impl<'le> ::selectors::Element for GeckoElement<'le> {
     type Impl = SelectorImpl;
 
     fn opaque(&self) -> OpaqueElement {
         OpaqueElement::new(self.0)
     }
 
     fn parent_element(&self) -> Option<Self> {
