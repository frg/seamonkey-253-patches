# HG changeset patch
# User Kearwood "Kip" Gilbert <kgilbert@mozilla.com>
# Date 1503957014 25200
#      Mon Aug 28 14:50:14 2017 -0700
# Node ID 83203c2de90ab514f56ab533e840990eca866bc8
# Parent  c1ae8d693919b8212890c1e14b226cd154e61d9c
Bug 1400407 - Part 3: Remove IsMirror concept, as it is no longer used by WebVR,r=jgilbert
- This patch is the same as one from Bug 1382104 (Remove IsMirror concept
  in favor of checking forwarder).
- It is safe to uplift this patch without the rest of Bug 1382104 as long
  as the remaining Bug 1381084 is also uplifted.

MozReview-Commit-ID: 21YZObeSUa3

diff --git a/dom/canvas/WebGLContext.cpp b/dom/canvas/WebGLContext.cpp
--- a/dom/canvas/WebGLContext.cpp
+++ b/dom/canvas/WebGLContext.cpp
@@ -105,17 +105,16 @@ WebGLContextOptions::WebGLContextOptions
 }
 
 WebGLContext::WebGLContext()
     : WebGLContextUnchecked(nullptr)
     , mMaxPerfWarnings(gfxPrefs::WebGLMaxPerfWarnings())
     , mNumPerfWarnings(0)
     , mMaxAcceptableFBStatusInvals(gfxPrefs::WebGLMaxAcceptableFBStatusInvals())
     , mDataAllocGLCallCount(0)
-    , mLayerIsMirror(false)
     , mBypassShaderValidation(false)
     , mEmptyTFO(0)
     , mContextLossHandler(this)
     , mNeedsFakeNoAlpha(false)
     , mNeedsFakeNoDepth(false)
     , mNeedsFakeNoStencil(false)
     , mNeedsEmulatedLoneDepthStencil(false)
     , mAllowFBInvalidation(gfxPrefs::WebGLFBInvalidation())
@@ -1284,17 +1283,16 @@ WebGLContext::UpdateLastUseIndex()
     // should never happen with 64-bit; trying to handle this would be riskier than
     // not handling it as the handler code would never get exercised.
     if (!sIndex.isValid())
         MOZ_CRASH("Can't believe it's been 2^64 transactions already!");
     mLastUseIndex = sIndex.value();
 }
 
 static uint8_t gWebGLLayerUserData;
-static uint8_t gWebGLMirrorLayerUserData;
 
 class WebGLContextUserData : public LayerUserData
 {
 public:
     explicit WebGLContextUserData(HTMLCanvasElement* canvas)
         : mCanvas(canvas)
     {}
 
@@ -1320,64 +1318,59 @@ public:
 
 private:
     RefPtr<HTMLCanvasElement> mCanvas;
 };
 
 already_AddRefed<layers::Layer>
 WebGLContext::GetCanvasLayer(nsDisplayListBuilder* builder,
                              Layer* oldLayer,
-                             LayerManager* manager,
-                             bool aMirror /*= false*/)
+                             LayerManager* manager)
 {
     if (!mResetLayer && oldLayer &&
-        oldLayer->HasUserData(aMirror ? &gWebGLMirrorLayerUserData : &gWebGLLayerUserData)) {
+        oldLayer->HasUserData(&gWebGLLayerUserData))
+    {
         RefPtr<layers::Layer> ret = oldLayer;
         return ret.forget();
     }
 
     RefPtr<CanvasLayer> canvasLayer = manager->CreateCanvasLayer();
     if (!canvasLayer) {
         NS_WARNING("CreateCanvasLayer returned null!");
         return nullptr;
     }
 
     WebGLContextUserData* userData = nullptr;
-    if (builder->IsPaintingToWindow() && mCanvasElement && !aMirror) {
+    if (builder->IsPaintingToWindow() && mCanvasElement) {
         userData = new WebGLContextUserData(mCanvasElement);
     }
 
-    canvasLayer->SetUserData(aMirror ? &gWebGLMirrorLayerUserData : &gWebGLLayerUserData, userData);
+    canvasLayer->SetUserData(&gWebGLLayerUserData, userData);
 
     CanvasRenderer* canvasRenderer = canvasLayer->CreateOrGetCanvasRenderer();
-    if (!InitializeCanvasRenderer(builder, canvasRenderer, aMirror))
+    if (!InitializeCanvasRenderer(builder, canvasRenderer))
       return nullptr;
 
     uint32_t flags = gl->Caps().alpha ? 0 : Layer::CONTENT_OPAQUE;
     canvasLayer->SetContentFlags(flags);
 
     mResetLayer = false;
-    // We only wish to update mLayerIsMirror when a new layer is returned.
-    // If a cached layer is returned above, aMirror is not changing since
-    // the last cached layer was created and mLayerIsMirror is still valid.
-    mLayerIsMirror = aMirror;
 
     return canvasLayer.forget();
 }
 
 bool
 WebGLContext::InitializeCanvasRenderer(nsDisplayListBuilder* aBuilder,
-                                       CanvasRenderer* aRenderer,
-                                       bool aMirror)
+                                       CanvasRenderer* aRenderer)
 {
     if (IsContextLost())
         return false;
 
     CanvasInitializeData data;
-    if (aBuilder->IsPaintingToWindow() && mCanvasElement && !aMirror) {
+    if (aBuilder->IsPaintingToWindow() && mCanvasElement) {
         // Make the layer tell us whenever a transaction finishes (including
         // the current transaction), so we can clear our invalidation state and
         // start invalidating again. We need to do this for the layer that is
         // being painted to a window (there shouldn't be more than one at a time,
         // and if there is, flushing the invalidation state more often than
         // necessary is harmless).
 
         // The layer will be destroyed when we tear down the presentation
@@ -1390,17 +1383,16 @@ WebGLContext::InitializeCanvasRenderer(n
         data.mDidTransCallback = WebGLContextUserData::DidTransactionCallback;
         data.mDidTransCallbackData = this;
     }
 
     data.mGLContext = gl;
     data.mSize = nsIntSize(mWidth, mHeight);
     data.mHasAlpha = gl->Caps().alpha;
     data.mIsGLAlphaPremult = IsPremultAlpha() || !data.mHasAlpha;
-    data.mIsMirror = aMirror;
 
     aRenderer->Initialize(data);
     aRenderer->SetDirty();
     return true;
 }
 
 layers::LayersBackend
 WebGLContext::GetCompositorBackendType() const
@@ -2349,17 +2341,17 @@ WebGLContext::GetVRFrame()
       return nullptr;
   }
 
   RefPtr<SharedSurfaceTextureClient> sharedSurface = screen->Front();
   if (!sharedSurface) {
       return nullptr;
   }
 
-  return sharedSurface.forget();
+    return sharedSurface.forget();
 }
 
 ////////////////////////////////////////////////////////////////////////////////
 
 static inline size_t
 SizeOfViewElem(const dom::ArrayBufferView& view)
 {
     const auto& elemType = view.Type();
diff --git a/dom/canvas/WebGLContext.h b/dom/canvas/WebGLContext.h
--- a/dom/canvas/WebGLContext.h
+++ b/dom/canvas/WebGLContext.h
@@ -1411,17 +1411,16 @@ protected:
 
     CheckedUint32 mGeneration;
 
     WebGLContextOptions mOptions;
 
     bool mInvalidated;
     bool mCapturedFrameInvalidated;
     bool mResetLayer;
-    bool mLayerIsMirror;
     bool mOptionsFrozen;
     bool mDisableExtensions;
     bool mIsMesa;
     bool mLoseContextOnMemoryPressure;
     bool mCanLoseContextInForeground;
     bool mRestoreWhenVisible;
     bool mShouldPresent;
     bool mBackbufferNeedsClear;
diff --git a/gfx/layers/CanvasRenderer.h b/gfx/layers/CanvasRenderer.h
--- a/gfx/layers/CanvasRenderer.h
+++ b/gfx/layers/CanvasRenderer.h
@@ -37,17 +37,16 @@ struct CanvasInitializeData {
     , mPreTransCallback(nullptr)
     , mPreTransCallbackData(nullptr)
     , mDidTransCallback(nullptr)
     , mDidTransCallbackData(nullptr)
     , mFrontbufferGLTex(0)
     , mSize(0,0)
     , mHasAlpha(false)
     , mIsGLAlphaPremult(true)
-    , mIsMirror(false)
   { }
 
   // One of these three must be specified for Canvas2D, but never more than one
   PersistentBufferProvider* mBufferProvider; // A BufferProvider for the Canvas contents
   mozilla::gl::GLContext* mGLContext; // or this, for GL.
   AsyncCanvasRenderer* mRenderer; // or this, for OffscreenCanvas
 
   typedef void (* TransactionCallback)(void* closureData);
@@ -62,20 +61,16 @@ struct CanvasInitializeData {
   // The size of the canvas content
   gfx::IntSize mSize;
 
   // Whether the canvas drawingbuffer has an alpha channel.
   bool mHasAlpha;
 
   // Whether mGLContext contains data that is alpha-premultiplied.
   bool mIsGLAlphaPremult;
-
-  // Whether the canvas front buffer is already being rendered somewhere else.
-  // When true, do not swap buffers or Morph() to another factory on mGLContext
-  bool mIsMirror;
 };
 
 // Based class which used for canvas rendering. There are many derived classes for
 // different purposes. such as:
 //
 // CopyableCanvasRenderer provides a utility which readback canvas content to a
 // SourceSurface. BasicCanvasLayer uses CopyableCanvasRenderer.
 //
diff --git a/gfx/layers/CopyableCanvasRenderer.cpp b/gfx/layers/CopyableCanvasRenderer.cpp
--- a/gfx/layers/CopyableCanvasRenderer.cpp
+++ b/gfx/layers/CopyableCanvasRenderer.cpp
@@ -35,17 +35,16 @@ using namespace mozilla::gl;
 
 CopyableCanvasRenderer::CopyableCanvasRenderer()
   : mGLContext(nullptr)
   , mBufferProvider(nullptr)
   , mGLFrontbuffer(nullptr)
   , mAsyncRenderer(nullptr)
   , mIsAlphaPremultiplied(true)
   , mOriginPos(gl::OriginPos::TopLeft)
-  , mIsMirror(false)
   , mOpaque(true)
   , mCachedTempSurface(nullptr)
 {
   MOZ_COUNT_CTOR(CopyableCanvasRenderer);
 }
 
 CopyableCanvasRenderer::~CopyableCanvasRenderer()
 {
@@ -57,17 +56,16 @@ void
 CopyableCanvasRenderer::Initialize(const CanvasInitializeData& aData)
 {
   CanvasRenderer::Initialize(aData);
 
   if (aData.mGLContext) {
     mGLContext = aData.mGLContext;
     mIsAlphaPremultiplied = aData.mIsGLAlphaPremult;
     mOriginPos = gl::OriginPos::BottomLeft;
-    mIsMirror = aData.mIsMirror;
 
     MOZ_ASSERT(mGLContext->IsOffscreen(), "canvas gl context isn't offscreen");
 
     if (aData.mFrontbufferGLTex) {
       gfx::IntSize size(aData.mSize.width, aData.mSize.height);
       mGLFrontbuffer = SharedSurface_Basic::Wrap(aData.mGLContext, size, aData.mHasAlpha,
                                                  aData.mFrontbufferGLTex);
       mBufferProvider = aData.mBufferProvider;
diff --git a/gfx/layers/CopyableCanvasRenderer.h b/gfx/layers/CopyableCanvasRenderer.h
--- a/gfx/layers/CopyableCanvasRenderer.h
+++ b/gfx/layers/CopyableCanvasRenderer.h
@@ -58,17 +58,16 @@ public:
 protected:
   RefPtr<gl::GLContext> mGLContext;
   RefPtr<PersistentBufferProvider> mBufferProvider;
   UniquePtr<gl::SharedSurface> mGLFrontbuffer;
   RefPtr<AsyncCanvasRenderer> mAsyncRenderer;
 
   bool mIsAlphaPremultiplied;
   gl::OriginPos mOriginPos;
-  bool mIsMirror;
 
   bool mOpaque;
 
   RefPtr<gfx::DataSourceSurface> mCachedTempSurface;
 
   gfx::DataSourceSurface* GetTempSurface(const gfx::IntSize& aSize,
                                          const gfx::SurfaceFormat aFormat);
 };
diff --git a/gfx/layers/ShareableCanvasRenderer.cpp b/gfx/layers/ShareableCanvasRenderer.cpp
--- a/gfx/layers/ShareableCanvasRenderer.cpp
+++ b/gfx/layers/ShareableCanvasRenderer.cpp
@@ -58,17 +58,17 @@ ShareableCanvasRenderer::Initialize(cons
   mFlags = TextureFlags::ORIGIN_BOTTOM_LEFT;
   if (!aData.mIsGLAlphaPremult) {
     mFlags |= TextureFlags::NON_PREMULTIPLIED;
   }
 
   UniquePtr<gl::SurfaceFactory> factory =
     gl::GLScreenBuffer::CreateFactory(mGLContext, caps, forwarder, mFlags);
 
-  if (mGLFrontbuffer || aData.mIsMirror) {
+  if (mGLFrontbuffer) {
     // We're using a source other than the one in the default screen.
     // (SkiaGL)
     mFactory = Move(factory);
     if (!mFactory) {
       // Absolutely must have a factory here, so create a basic one
       mFactory = MakeUnique<gl::SurfaceFactory_Basic>(mGLContext, caps, mFlags);
     }
   } else {
diff --git a/gfx/layers/client/CanvasClient.cpp b/gfx/layers/client/CanvasClient.cpp
--- a/gfx/layers/client/CanvasClient.cpp
+++ b/gfx/layers/client/CanvasClient.cpp
@@ -406,21 +406,16 @@ CanvasClientSharedSurface::UpdateRendere
   RefPtr<TextureClient> newFront;
 
   if (canvasRenderer && canvasRenderer->mGLFrontbuffer) {
     mShSurfClient = CloneSurface(canvasRenderer->mGLFrontbuffer.get(), canvasRenderer->mFactory.get());
     if (!mShSurfClient) {
       gfxCriticalError() << "Invalid canvas front buffer";
       return;
     }
-  } else if (canvasRenderer && canvasRenderer->mIsMirror) {
-    mShSurfClient = CloneSurface(gl->Screen()->Front()->Surf(), canvasRenderer->mFactory.get());
-    if (!mShSurfClient) {
-      return;
-    }
   } else {
     mShSurfClient = gl->Screen()->Front();
     if (mShSurfClient && mShSurfClient->GetAllocator() &&
         mShSurfClient->GetAllocator() != GetForwarder()->GetTextureForwarder()) {
       mShSurfClient = CloneSurface(mShSurfClient->Surf(), gl->Screen()->Factory());
     }
     if (!mShSurfClient) {
       return;
