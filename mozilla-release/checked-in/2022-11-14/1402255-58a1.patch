# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1506309051 -36000
#      Mon Sep 25 13:10:51 2017 +1000
# Node ID 3f384ef139c4217981891b23cdcce8352cab0a0c
# Parent  c050879215e1533040c33f73462c82cbb49d5267
Bug 1402255 - Change nsIBaseWindow::title from wstring to AString. r=janv.

Because nsAString is nicer to work with than char16_t*. The patch relatedly
changes nsIEmbeddingSiteWindow::title and nsIWindowMediator::updateWindowTitle
as well.

diff --git a/docshell/base/nsDocShell.cpp b/docshell/base/nsDocShell.cpp
--- a/docshell/base/nsDocShell.cpp
+++ b/docshell/base/nsDocShell.cpp
@@ -6613,26 +6613,24 @@ nsDocShell::SetFocus()
 NS_IMETHODIMP
 nsDocShell::GetMainWidget(nsIWidget** aMainWidget)
 {
   // We don't create our own widget, so simply return the parent one.
   return GetParentWidget(aMainWidget);
 }
 
 NS_IMETHODIMP
-nsDocShell::GetTitle(char16_t** aTitle)
-{
-  NS_ENSURE_ARG_POINTER(aTitle);
-
-  *aTitle = ToNewUnicode(mTitle);
-  return NS_OK;
-}
-
-NS_IMETHODIMP
-nsDocShell::SetTitle(const char16_t* aTitle)
+nsDocShell::GetTitle(nsAString& aTitle)
+{
+  aTitle = mTitle;
+  return NS_OK;
+}
+
+NS_IMETHODIMP
+nsDocShell::SetTitle(const nsAString& aTitle)
 {
   // Store local title
   mTitle = aTitle;
 
   nsCOMPtr<nsIDocShellTreeItem> parent;
   GetSameTypeParent(getter_AddRefs(parent));
 
   // When title is set on the top object it should then be passed to the
diff --git a/docshell/base/nsDocShellTreeOwner.cpp b/docshell/base/nsDocShellTreeOwner.cpp
--- a/docshell/base/nsDocShellTreeOwner.cpp
+++ b/docshell/base/nsDocShellTreeOwner.cpp
@@ -714,27 +714,27 @@ nsDocShellTreeOwner::SetFocus()
   nsCOMPtr<nsIEmbeddingSiteWindow> ownerWin = GetOwnerWin();
   if (ownerWin) {
     return ownerWin->SetFocus();
   }
   return NS_ERROR_NULL_POINTER;
 }
 
 NS_IMETHODIMP
-nsDocShellTreeOwner::GetTitle(char16_t** aTitle)
+nsDocShellTreeOwner::GetTitle(nsAString& aTitle)
 {
   nsCOMPtr<nsIEmbeddingSiteWindow> ownerWin = GetOwnerWin();
   if (ownerWin) {
     return ownerWin->GetTitle(aTitle);
   }
   return NS_ERROR_NULL_POINTER;
 }
 
 NS_IMETHODIMP
-nsDocShellTreeOwner::SetTitle(const char16_t* aTitle)
+nsDocShellTreeOwner::SetTitle(const nsAString& aTitle)
 {
   nsCOMPtr<nsIEmbeddingSiteWindow> ownerWin = GetOwnerWin();
   if (ownerWin) {
     return ownerWin->SetTitle(aTitle);
   }
   return NS_ERROR_NULL_POINTER;
 }
 
diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -7301,17 +7301,17 @@ nsDocument::DoNotifyPossibleTitleChange(
 
   nsCOMPtr<nsIPresShell> shell = GetShell();
   if (shell) {
     nsCOMPtr<nsISupports> container =
       shell->GetPresContext()->GetContainerWeak();
     if (container) {
       nsCOMPtr<nsIBaseWindow> docShellWin = do_QueryInterface(container);
       if (docShellWin) {
-        docShellWin->SetTitle(title.get());
+        docShellWin->SetTitle(title);
       }
     }
   }
 
   // Fire a DOM event for the title change.
   nsContentUtils::DispatchChromeEvent(this, static_cast<nsIDocument*>(this),
                                       NS_LITERAL_STRING("DOMTitleChanged"),
                                       true, true);
diff --git a/dom/ipc/TabChild.cpp b/dom/ipc/TabChild.cpp
--- a/dom/ipc/TabChild.cpp
+++ b/dom/ipc/TabChild.cpp
@@ -942,25 +942,25 @@ TabChild::GetVisibility(bool* aVisibilit
 NS_IMETHODIMP
 TabChild::SetVisibility(bool aVisibility)
 {
   // should the platform support this? Bug 666365
   return NS_OK;
 }
 
 NS_IMETHODIMP
-TabChild::GetTitle(char16_t** aTitle)
+TabChild::GetTitle(nsAString& aTitle)
 {
   NS_WARNING("TabChild::GetTitle not supported in TabChild");
 
   return NS_ERROR_NOT_IMPLEMENTED;
 }
 
 NS_IMETHODIMP
-TabChild::SetTitle(const char16_t* aTitle)
+TabChild::SetTitle(const nsAString& aTitle)
 {
   // JavaScript sends the "DOMTitleChanged" event to the parent
   // via the message manager.
   return NS_OK;
 }
 
 NS_IMETHODIMP
 TabChild::GetSiteWindow(void** aSiteWindow)
diff --git a/toolkit/components/browser/nsIEmbeddingSiteWindow.idl b/toolkit/components/browser/nsIEmbeddingSiteWindow.idl
--- a/toolkit/components/browser/nsIEmbeddingSiteWindow.idl
+++ b/toolkit/components/browser/nsIEmbeddingSiteWindow.idl
@@ -129,17 +129,17 @@ interface nsIEmbeddingSiteWindow : nsISu
     /**
      * Visibility of the window.
      */
     attribute boolean visibility;
 
     /**
      * Title of the window.
      */
-    attribute wstring title;
+    attribute AString title;
 
     /**
      * Native window for the site's window. The implementor should copy the
      * native window object into the address supplied by the caller. The
      * type of the native window that the address refers to is  platform
      * and OS specific as follows:
      *
      * <ul>
diff --git a/toolkit/components/browser/nsWebBrowser.cpp b/toolkit/components/browser/nsWebBrowser.cpp
--- a/toolkit/components/browser/nsWebBrowser.cpp
+++ b/toolkit/components/browser/nsWebBrowser.cpp
@@ -1587,28 +1587,27 @@ nsWebBrowser::SetFocus()
   nsCOMPtr<nsPIDOMWindowOuter> window = GetWindow();
   NS_ENSURE_TRUE(window, NS_ERROR_FAILURE);
 
   nsCOMPtr<nsIFocusManager> fm = do_GetService(FOCUSMANAGER_CONTRACTID);
   return fm ? fm->SetFocusedWindow(window) : NS_OK;
 }
 
 NS_IMETHODIMP
-nsWebBrowser::GetTitle(char16_t** aTitle)
+nsWebBrowser::GetTitle(nsAString& aTitle)
 {
-  NS_ENSURE_ARG_POINTER(aTitle);
   NS_ENSURE_STATE(mDocShell);
 
   NS_ENSURE_SUCCESS(mDocShellAsWin->GetTitle(aTitle), NS_ERROR_FAILURE);
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
-nsWebBrowser::SetTitle(const char16_t* aTitle)
+nsWebBrowser::SetTitle(const nsAString& aTitle)
 {
   NS_ENSURE_STATE(mDocShell);
 
   NS_ENSURE_SUCCESS(mDocShellAsWin->SetTitle(aTitle), NS_ERROR_FAILURE);
 
   return NS_OK;
 }
 
diff --git a/toolkit/xre/nsNativeAppSupportWin.cpp b/toolkit/xre/nsNativeAppSupportWin.cpp
--- a/toolkit/xre/nsNativeAppSupportWin.cpp
+++ b/toolkit/xre/nsNativeAppSupportWin.cpp
@@ -988,17 +988,17 @@ nsNativeAppSupportWin::HandleDDENotifica
                         if ( !baseWindow ) {
                             break;
                         }
                         // And from the base window we can get the title.
                         nsString title;
                         if(!baseWindow) {
                             break;
                         }
-                        baseWindow->GetTitle(getter_Copies(title));
+                        baseWindow->GetTitle(title);
                         // Escape any double-quotes in the title.
                         escapeQuotes( title );
 
                         // Use a string buffer for the output data, first
                         // save a quote.
                         nsAutoCString   outpt( NS_LITERAL_CSTRING("\"") );
                         // Now copy the URL converting the Unicode string
                         // to a single-byte ASCII string
diff --git a/widget/nsIBaseWindow.idl b/widget/nsIBaseWindow.idl
--- a/widget/nsIBaseWindow.idl
+++ b/widget/nsIBaseWindow.idl
@@ -221,10 +221,10 @@ interface nsIBaseWindow : nsISupports
 	/**
 	* Give the window focus.
 	*/
 	void setFocus();
 
 	/*
 	Title of the window.
 	*/
-	attribute wstring title;
+	attribute AString title;
 };
diff --git a/xpfe/appshell/nsAppShellService.cpp b/xpfe/appshell/nsAppShellService.cpp
--- a/xpfe/appshell/nsAppShellService.cpp
+++ b/xpfe/appshell/nsAppShellService.cpp
@@ -375,22 +375,22 @@ WebBrowserChrome2Stub::GetVisibility(boo
 }
 NS_IMETHODIMP
 WebBrowserChrome2Stub::SetVisibility(bool aVisibility)
 {
   return NS_ERROR_NOT_IMPLEMENTED;
 }
 
 NS_IMETHODIMP
-WebBrowserChrome2Stub::GetTitle(char16_t** aTitle)
+WebBrowserChrome2Stub::GetTitle(nsAString& aTitle)
 {
   return NS_ERROR_NOT_IMPLEMENTED;
 }
 NS_IMETHODIMP
-WebBrowserChrome2Stub::SetTitle(const char16_t* aTitle)
+WebBrowserChrome2Stub::SetTitle(const nsAString& aTitle)
 {
   return NS_ERROR_NOT_IMPLEMENTED;
 }
 
 NS_IMETHODIMP
 WebBrowserChrome2Stub::GetSiteWindow(void** aSiteWindow)
 {
   return NS_ERROR_NOT_IMPLEMENTED;
diff --git a/xpfe/appshell/nsChromeTreeOwner.cpp b/xpfe/appshell/nsChromeTreeOwner.cpp
--- a/xpfe/appshell/nsChromeTreeOwner.cpp
+++ b/xpfe/appshell/nsChromeTreeOwner.cpp
@@ -425,23 +425,23 @@ NS_IMETHODIMP nsChromeTreeOwner::GetMain
 }
 
 NS_IMETHODIMP nsChromeTreeOwner::SetFocus()
 {
    NS_ENSURE_STATE(mXULWindow);
    return mXULWindow->SetFocus();
 }
 
-NS_IMETHODIMP nsChromeTreeOwner::GetTitle(char16_t** aTitle)
+NS_IMETHODIMP nsChromeTreeOwner::GetTitle(nsAString& aTitle)
 {
    NS_ENSURE_STATE(mXULWindow);
    return mXULWindow->GetTitle(aTitle);
 }
 
-NS_IMETHODIMP nsChromeTreeOwner::SetTitle(const char16_t* aTitle)
+NS_IMETHODIMP nsChromeTreeOwner::SetTitle(const nsAString& aTitle)
 {
    NS_ENSURE_STATE(mXULWindow);
    return mXULWindow->SetTitle(aTitle);
 }
 
 //*****************************************************************************
 // nsChromeTreeOwner::nsIWebProgressListener
 //*****************************************************************************
diff --git a/xpfe/appshell/nsContentTreeOwner.cpp b/xpfe/appshell/nsContentTreeOwner.cpp
--- a/xpfe/appshell/nsContentTreeOwner.cpp
+++ b/xpfe/appshell/nsContentTreeOwner.cpp
@@ -709,25 +709,24 @@ NS_IMETHODIMP nsContentTreeOwner::GetMai
 }
 
 NS_IMETHODIMP nsContentTreeOwner::SetFocus()
 {
    NS_ENSURE_STATE(mXULWindow);
    return mXULWindow->SetFocus();
 }
 
-NS_IMETHODIMP nsContentTreeOwner::GetTitle(char16_t** aTitle)
+NS_IMETHODIMP nsContentTreeOwner::GetTitle(nsAString& aTitle)
 {
-   NS_ENSURE_ARG_POINTER(aTitle);
    NS_ENSURE_STATE(mXULWindow);
 
    return mXULWindow->GetTitle(aTitle);
 }
 
-NS_IMETHODIMP nsContentTreeOwner::SetTitle(const char16_t* aTitle)
+NS_IMETHODIMP nsContentTreeOwner::SetTitle(const nsAString& aTitle)
 {
    // We only allow the title to be set from the primary content shell
   if(!mPrimary || !mContentTitleSetting)
     return NS_OK;
 
   NS_ENSURE_STATE(mXULWindow);
 
   nsAutoString   title;
@@ -806,17 +805,17 @@ NS_IMETHODIMP nsContentTreeOwner::SetTit
       }
     }
     nsIDocument* document = docShellElement->OwnerDoc();
     ErrorResult rv;
     document->SetTitle(title, rv);
     return rv.StealNSResult();
   }
 
-  return mXULWindow->SetTitle(title.get());
+  return mXULWindow->SetTitle(title);
 }
 
 //*****************************************************************************
 // nsContentTreeOwner: nsIWindowProvider
 //*****************************************************************************
 NS_IMETHODIMP
 nsContentTreeOwner::ProvideWindow(mozIDOMWindowProxy* aParent,
                                   uint32_t aChromeFlags,
@@ -1146,23 +1145,23 @@ nsSiteWindow::GetVisibility(bool *aVisib
 
 NS_IMETHODIMP
 nsSiteWindow::SetVisibility(bool aVisibility)
 {
   return mAggregator->SetVisibility(aVisibility);
 }
 
 NS_IMETHODIMP
-nsSiteWindow::GetTitle(char16_t * *aTitle)
+nsSiteWindow::GetTitle(nsAString& aTitle)
 {
   return mAggregator->GetTitle(aTitle);
 }
 
 NS_IMETHODIMP
-nsSiteWindow::SetTitle(const char16_t * aTitle)
+nsSiteWindow::SetTitle(const nsAString& aTitle)
 {
   return mAggregator->SetTitle(aTitle);
 }
 
 NS_IMETHODIMP
 nsSiteWindow::GetSiteWindow(void **aSiteWindow)
 {
   return mAggregator->GetParentNativeWindow(aSiteWindow);
diff --git a/xpfe/appshell/nsIWindowMediator.idl b/xpfe/appshell/nsIWindowMediator.idl
--- a/xpfe/appshell/nsIWindowMediator.idl
+++ b/xpfe/appshell/nsIWindowMediator.idl
@@ -104,17 +104,17 @@ interface nsIWindowMediator: nsISupports
   [noscript] void updateWindowTimeStamp(in nsIXULWindow aWindow);
 
   /** Call this method when a window's title changes. Listeners (see
     * addListener) will be notified through their onWindowTitleChange method.
     * @param aWindow the window whose title has changed
     * @param inTitle the window's new title
     */
   [noscript] void updateWindowTitle(in nsIXULWindow aWindow,
-                                    in wstring inTitle );
+                                    in AString inTitle);
 
   /* z-ordering: */
 
   const unsigned long zLevelTop    = 1;
   const unsigned long zLevelBottom = 2;
   const unsigned long zLevelBelow  = 3; // below some window
 
   /** A window wants to be moved in z-order. Calculate whether and how
diff --git a/xpfe/appshell/nsIWindowMediatorListener.idl b/xpfe/appshell/nsIWindowMediatorListener.idl
--- a/xpfe/appshell/nsIWindowMediatorListener.idl
+++ b/xpfe/appshell/nsIWindowMediatorListener.idl
@@ -5,15 +5,14 @@
 
 #include "nsISupports.idl"
 
 interface nsIXULWindow;
 
 [scriptable, uuid(2F276982-0D60-4377-A595-D350BA516395)]
 interface nsIWindowMediatorListener : nsISupports
 {
-    void onWindowTitleChange(in nsIXULWindow window,
-                             in wstring newTitle);
+    void onWindowTitleChange(in nsIXULWindow window, in AString newTitle);
 
     void onOpenWindow(in nsIXULWindow window);
     void onCloseWindow(in nsIXULWindow window);
 };
 
diff --git a/xpfe/appshell/nsWindowMediator.cpp b/xpfe/appshell/nsWindowMediator.cpp
--- a/xpfe/appshell/nsWindowMediator.cpp
+++ b/xpfe/appshell/nsWindowMediator.cpp
@@ -391,17 +391,17 @@ nsWindowMediator::UpdateWindowTimeStamp(
     info->mTimeStamp = ++mTimeStamp;
     return NS_OK;
   }
   return NS_ERROR_FAILURE;
 }
 
 NS_IMETHODIMP
 nsWindowMediator::UpdateWindowTitle(nsIXULWindow* inWindow,
-                                    const char16_t* inTitle)
+                                    const nsAString& inTitle)
 {
   MOZ_RELEASE_ASSERT(NS_IsMainThread());
   NS_ENSURE_STATE(mReady);
   if (GetInfoFor(inWindow)) {
     ListenerArray::ForwardIterator iter(mListeners);
     while (iter.HasMore()) {
       iter.GetNext()->OnWindowTitleChange(inWindow, inTitle);
     }
diff --git a/xpfe/appshell/nsXULWindow.cpp b/xpfe/appshell/nsXULWindow.cpp
--- a/xpfe/appshell/nsXULWindow.cpp
+++ b/xpfe/appshell/nsXULWindow.cpp
@@ -892,27 +892,23 @@ NS_IMETHODIMP nsXULWindow::GetMainWidget
 
 NS_IMETHODIMP nsXULWindow::SetFocus()
 {
   //XXX First Check In
   NS_ASSERTION(false, "Not Yet Implemented");
   return NS_OK;
 }
 
-NS_IMETHODIMP nsXULWindow::GetTitle(char16_t** aTitle)
+NS_IMETHODIMP nsXULWindow::GetTitle(nsAString& aTitle)
 {
-  NS_ENSURE_ARG_POINTER(aTitle);
-
-  *aTitle = ToNewUnicode(mTitle);
-  if (!*aTitle)
-    return NS_ERROR_OUT_OF_MEMORY;
+  aTitle = mTitle;
   return NS_OK;
 }
 
-NS_IMETHODIMP nsXULWindow::SetTitle(const char16_t* aTitle)
+NS_IMETHODIMP nsXULWindow::SetTitle(const nsAString& aTitle)
 {
   NS_ENSURE_STATE(mWindow);
   mTitle.Assign(aTitle);
   mTitle.StripCRLF();
   NS_ENSURE_SUCCESS(mWindow->SetTitle(mTitle), NS_ERROR_FAILURE);
 
   // Tell the window mediator that a title has changed
   nsCOMPtr<nsIWindowMediator> windowMediator(do_GetService(NS_WINDOWMEDIATOR_CONTRACTID));
diff --git a/xpfe/components/windowds/nsWindowDataSource.cpp b/xpfe/components/windowds/nsWindowDataSource.cpp
--- a/xpfe/components/windowds/nsWindowDataSource.cpp
+++ b/xpfe/components/windowds/nsWindowDataSource.cpp
@@ -116,33 +116,34 @@ NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(
 NS_INTERFACE_MAP_END
 
 // nsIWindowMediatorListener implementation
 // handle notifications from the window mediator and reflect them into
 // RDF
 
 NS_IMETHODIMP
 nsWindowDataSource::OnWindowTitleChange(nsIXULWindow *window,
-                                        const char16_t *newTitle)
+                                        const nsAString& newTitle)
 {
     nsresult rv;
 
     nsCOMPtr<nsIRDFResource> windowResource;
     mWindowResources.Get(window, getter_AddRefs(windowResource));
 
     // oops, make sure this window is in the hashtable!
     if (!windowResource) {
         OnOpenWindow(window);
         mWindowResources.Get(window, getter_AddRefs(windowResource));
     }
 
     NS_ENSURE_TRUE(windowResource, NS_ERROR_UNEXPECTED);
 
     nsCOMPtr<nsIRDFLiteral> newTitleLiteral;
-    rv = gRDFService->GetLiteral(newTitle, getter_AddRefs(newTitleLiteral));
+    rv = gRDFService->GetLiteral(PromiseFlatString(newTitle).get(),
+                                 getter_AddRefs(newTitleLiteral));
     NS_ENSURE_SUCCESS(rv, rv);
 
     // get the old title
     nsCOMPtr<nsIRDFNode> oldTitleNode;
     rv = GetTarget(windowResource, kNC_Name, true,
                    getter_AddRefs(oldTitleNode));
 
     // assert the change
