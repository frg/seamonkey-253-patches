# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1507737545 18000
#      Wed Oct 11 10:59:05 2017 -0500
# Node ID 7290aaeb80740ceced351d10507b3bfdf0807aea
# Parent  50ab411874dbf64333f6f12c9a32d8120f1cb2ca
servo: Merge #18826 - style: Fix invalidation collection for XBL when quirks modes don't match (from emilio:invalidation-xbl-stuff); r=heycam

Source-Repo: https://github.com/servo/servo
Source-Revision: 105c5d2a0692c6e09320c5d967b43f879c3cd00d

diff --git a/servo/components/style/invalidation/element/invalidator.rs b/servo/components/style/invalidation/element/invalidator.rs
--- a/servo/components/style/invalidation/element/invalidator.rs
+++ b/servo/components/style/invalidation/element/invalidator.rs
@@ -1,17 +1,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! The struct that takes care of encapsulating all the logic on where and how
 //! element styles need to be invalidated.
 
 use Atom;
-use context::{SharedStyleContext, StackLimitChecker};
+use context::{QuirksMode, SharedStyleContext, StackLimitChecker};
 use data::ElementData;
 use dom::{TElement, TNode};
 use element_state::{ElementState, IN_VISITED_OR_UNVISITED_STATE};
 use invalidation::element::element_wrapper::{ElementSnapshot, ElementWrapper};
 use invalidation::element::invalidation_map::*;
 use invalidation::element::restyle_hints::*;
 use selector_map::SelectorMap;
 use selector_parser::{SelectorImpl, Snapshot};
@@ -257,17 +257,17 @@ impl<'a, 'b: 'a, E> TreeStyleInvalidator
         let invalidated_self = {
             let mut collector = InvalidationCollector {
                 wrapper,
                 lookup_element,
                 nth_index_cache: self.nth_index_cache.as_mut().map(|c| &mut **c),
                 state_changes,
                 element: self.element,
                 snapshot: &snapshot,
-                shared_context: self.shared_context,
+                quirks_mode: self.shared_context.quirks_mode(),
                 removed_id: id_removed.as_ref(),
                 added_id: id_added.as_ref(),
                 classes_removed: &classes_removed,
                 classes_added: &classes_added,
                 descendant_invalidations: &mut descendant_invalidations,
                 sibling_invalidations: &mut sibling_invalidations,
                 invalidates_self: false,
             };
@@ -277,16 +277,20 @@ impl<'a, 'b: 'a, E> TreeStyleInvalidator
             });
 
             // TODO(emilio): Consider storing dependencies from the UA sheet in
             // a different map. If we do that, we can skip the stuff on the
             // shared stylist iff cut_off_inheritance is true, and we can look
             // just at that map.
             let _cut_off_inheritance =
                 self.element.each_xbl_stylist(|stylist| {
+                    // FIXME(emilio): Replace with assert / remove when we
+                    // figure out what to do with the quirks mode mismatches
+                    // (that is, when bug 1406875 is properly fixed).
+                    collector.quirks_mode = stylist.quirks_mode();
                     stylist.each_invalidation_map(|invalidation_map| {
                         collector.collect_dependencies_in_invalidation_map(invalidation_map);
                     });
                 });
 
             collector.invalidates_self
         };
 
@@ -818,17 +822,17 @@ impl<'a, 'b: 'a, E> TreeStyleInvalidator
 
 struct InvalidationCollector<'a, 'b: 'a, E>
     where E: TElement,
 {
     element: E,
     wrapper: ElementWrapper<'b, E>,
     nth_index_cache: Option<&'a mut NthIndexCache>,
     snapshot: &'a Snapshot,
-    shared_context: &'a SharedStyleContext<'b>,
+    quirks_mode: QuirksMode,
     lookup_element: E,
     removed_id: Option<&'a Atom>,
     added_id: Option<&'a Atom>,
     classes_removed: &'a SmallVec<[Atom; 8]>,
     classes_added: &'a SmallVec<[Atom; 8]>,
     state_changes: ElementState,
     descendant_invalidations: &'a mut InvalidationVector,
     sibling_invalidations: &'a mut InvalidationVector,
@@ -837,17 +841,17 @@ struct InvalidationCollector<'a, 'b: 'a,
 
 impl<'a, 'b: 'a, E> InvalidationCollector<'a, 'b, E>
     where E: TElement,
 {
     fn collect_dependencies_in_invalidation_map(
         &mut self,
         map: &InvalidationMap,
     ) {
-        let quirks_mode = self.shared_context.quirks_mode();
+        let quirks_mode = self.quirks_mode;
         let removed_id = self.removed_id;
         if let Some(ref id) = removed_id {
             if let Some(deps) = map.id_to_selector.get(id, quirks_mode) {
                 for dep in deps {
                     self.scan_dependency(dep, VisitedDependent::No);
                 }
             }
         }
@@ -890,34 +894,34 @@ impl<'a, 'b: 'a, E> InvalidationCollecto
     }
 
     fn collect_dependencies_in_map(
         &mut self,
         map: &SelectorMap<Dependency>,
     ) {
         map.lookup_with_additional(
             self.lookup_element,
-            self.shared_context.quirks_mode(),
+            self.quirks_mode,
             self.removed_id,
             self.classes_removed,
             &mut |dependency| {
                 self.scan_dependency(dependency, VisitedDependent::No);
                 true
             },
         );
     }
 
     fn collect_state_dependencies(
         &mut self,
         map: &SelectorMap<StateDependency>,
         state_changes: ElementState,
     ) {
         map.lookup_with_additional(
             self.lookup_element,
-            self.shared_context.quirks_mode(),
+            self.quirks_mode,
             self.removed_id,
             self.classes_removed,
             &mut |dependency| {
                 if !dependency.state.intersects(state_changes) {
                     return true;
                 }
                 let visited_dependent =
                     if dependency.state.intersects(IN_VISITED_OR_UNVISITED_STATE) {
@@ -940,17 +944,17 @@ impl<'a, 'b: 'a, E> InvalidationCollecto
         relevant_link_found: &mut bool,
     ) -> bool {
         let (matches_now, relevant_link_found_now) = {
             let mut context = MatchingContext::new_for_visited(
                 MatchingMode::Normal,
                 None,
                 self.nth_index_cache.as_mut().map(|c| &mut **c),
                 visited_handling_mode,
-                self.shared_context.quirks_mode(),
+                self.quirks_mode,
             );
 
             let matches_now = matches_selector(
                 &dependency.selector,
                 dependency.selector_offset,
                 None,
                 &self.element,
                 &mut context,
@@ -961,17 +965,17 @@ impl<'a, 'b: 'a, E> InvalidationCollecto
         };
 
         let (matched_then, relevant_link_found_then) = {
             let mut context = MatchingContext::new_for_visited(
                 MatchingMode::Normal,
                 None,
                 self.nth_index_cache.as_mut().map(|c| &mut **c),
                 visited_handling_mode,
-                self.shared_context.quirks_mode(),
+                self.quirks_mode,
             );
 
             let matched_then = matches_selector(
                 &dependency.selector,
                 dependency.selector_offset,
                 None,
                 &self.wrapper,
                 &mut context,
