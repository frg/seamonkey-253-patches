# HG changeset patch
# User Luke Chang <lchang@mozilla.com>
# Date 1507537357 -28800
# Node ID c05495fc2b5144f1580f123bbf2d412f24172952
# Parent  2930911e71b906280eae6f29bf19422896c97446
Bug 1400760 - [Form Autofill] Don't popup credit card dropdown if only cc-number field is identified and without @autocomplete. r=seanlee

MozReview-Commit-ID: JGoRLx2WEBG

diff --git a/browser/extensions/formautofill/FormAutofillHandler.jsm b/browser/extensions/formautofill/FormAutofillHandler.jsm
--- a/browser/extensions/formautofill/FormAutofillHandler.jsm
+++ b/browser/extensions/formautofill/FormAutofillHandler.jsm
@@ -147,33 +147,56 @@ FormAutofillHandler.prototype = {
 
     if (this.address.fieldDetails.length < FormAutofillUtils.AUTOFILL_FIELDS_THRESHOLD) {
       log.debug("Ignoring address related fields since it has only",
                 this.address.fieldDetails.length,
                 "field(s)");
       this.address.fieldDetails = [];
     }
 
-    if (!this.creditCard.fieldDetails.some(i => i.fieldName == "cc-number")) {
-      log.debug("Ignoring credit card related fields since it's without credit card number field");
+    if (!this._isValidCreditCardForm(this.creditCard.fieldDetails)) {
+      log.debug("Invalid credit card form");
       this.creditCard.fieldDetails = [];
     }
+
     let validDetails = Array.of(...(this.address.fieldDetails),
                                 ...(this.creditCard.fieldDetails));
     for (let detail of validDetails) {
       let input = detail.elementWeakRef.get();
       if (!input) {
         continue;
       }
       input.addEventListener("input", this);
     }
 
     return validDetails;
   },
 
+  _isValidCreditCardForm(fieldDetails) {
+    let ccNumberReason = "";
+    let hasCCNumber = false;
+    let hasExpiryDate = false;
+
+    for (let detail of fieldDetails) {
+      switch (detail.fieldName) {
+        case "cc-number":
+          hasCCNumber = true;
+          ccNumberReason = detail._reason;
+          break;
+        case "cc-exp":
+        case "cc-exp-month":
+        case "cc-exp-year":
+          hasExpiryDate = true;
+          break;
+      }
+    }
+
+    return hasCCNumber && (ccNumberReason == "autocomplete" || hasExpiryDate);
+  },
+
   getFieldDetailByName(fieldName) {
     return this.fieldDetails.find(detail => detail.fieldName == fieldName);
   },
 
   getFieldDetailByElement(element) {
     return this.fieldDetails.find(
       detail => detail.elementWeakRef.get() == element
     );
diff --git a/browser/extensions/formautofill/test/unit/test_collectFormFields.js b/browser/extensions/formautofill/test/unit/test_collectFormFields.js
--- a/browser/extensions/formautofill/test/unit/test_collectFormFields.js
+++ b/browser/extensions/formautofill/test/unit/test_collectFormFields.js
@@ -154,29 +154,104 @@ const TESTCASES = [
     validFieldDetails: [
       {"section": "", "addressType": "shipping", "contactType": "", "fieldName": "given-name"},
       {"section": "", "addressType": "shipping", "contactType": "", "fieldName": "family-name"},
       {"section": "", "addressType": "shipping", "contactType": "", "fieldName": "street-address"},
       {"section": "", "addressType": "shipping", "contactType": "", "fieldName": "cc-number"},
     ],
   },
   {
-    description: "It's an invalid address and credit form.",
+    description: "An invalid address form due to less than 3 fields.",
     document: `<form>
                <input id="given-name" autocomplete="shipping given-name">
                <input autocomplete="shipping address-level2">
+               </form>`,
+    addressFieldDetails: [],
+    creditCardFieldDetails: [],
+    validFieldDetails: [],
+  },
+  {
+    description: "An invalid credit card form due to omitted cc-number.",
+    document: `<form>
                <input id="cc-name" autocomplete="cc-name">
                <input id="cc-exp-month" autocomplete="cc-exp-month">
                <input id="cc-exp-year" autocomplete="cc-exp-year">
                </form>`,
     addressFieldDetails: [],
     creditCardFieldDetails: [],
     validFieldDetails: [],
   },
   {
+    description: "An invalid credit card form due to non-autocomplete-attr cc-number and omitted cc-exp-*.",
+    document: `<form>
+               <input id="cc-name" autocomplete="cc-name">
+               <input id="cc-number" name="card-number">
+               </form>`,
+    addressFieldDetails: [],
+    creditCardFieldDetails: [],
+    validFieldDetails: [],
+  },
+  {
+    description: "A valid credit card form with autocomplete-attr cc-number only.",
+    document: `<form>
+               <input id="cc-number" autocomplete="cc-number">
+               </form>`,
+    addressFieldDetails: [],
+    creditCardFieldDetails: [
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+    ],
+    validFieldDetails: [
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+    ],
+  },
+  {
+    description: "A valid credit card form with non-autocomplete-attr cc-number and cc-exp.",
+    document: `<form>
+               <input id="cc-number" name="card-number">
+               <input id="cc-exp" autocomplete="cc-exp">
+               </form>`,
+    addressFieldDetails: [],
+    creditCardFieldDetails: [
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
+    ],
+    validFieldDetails: [
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp"},
+    ],
+    ids: [
+      "cc-number",
+      "cc-exp",
+    ],
+  },
+  {
+    description: "A valid credit card form with non-autocomplete-attr cc-number and cc-exp-month/cc-exp-year.",
+    document: `<form>
+               <input id="cc-number" name="card-number">
+               <input id="cc-exp-month" autocomplete="cc-exp-month">
+               <input id="cc-exp-year" autocomplete="cc-exp-year">
+               </form>`,
+    addressFieldDetails: [],
+    creditCardFieldDetails: [
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
+    ],
+    validFieldDetails: [
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-number"},
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-month"},
+      {"section": "", "addressType": "", "contactType": "", "fieldName": "cc-exp-year"},
+    ],
+    ids: [
+      "cc-number",
+      "cc-exp-month",
+      "cc-exp-year",
+    ],
+  },
+  {
     description: "Three sets of adjacent phone number fields",
     document: `<form>
                  <input id="shippingAC" name="phone" maxlength="3">
                  <input id="shippingPrefix" name="phone" maxlength="3">
                  <input id="shippingSuffix" name="phone" maxlength="4">
                  <input id="shippingTelExt" name="extension">
 
                  <input id="billingAC" name="phone" maxlength="3">
