# HG changeset patch
# User Gijs Kruitbosch <gijskruitbosch@gmail.com>
# Date 1572734104 0
#      Sat Nov 02 22:35:04 2019 +0000
# Node ID 16d21676bcb9d75a488d25ac67f536331899b959
# Parent  49c8aa085e212a2bc38f07bc42c9508ea35d9f1a
Bug 1545123 - move reading pluginreg and scanning for plugins to a background thread, r=handyman,mconley

Finally, let's move the actual IO away from the main thread.

This means there are now 3 ways of looking for plugins:
1. looking for changes from ReloadPlugins. This runs the PluginFinder runnable
   on the main thread.
2. loading plugins from LoadPlugins. This will:
   a) first check prefs and report the flash plugin based on that information,
      if the prefs indicate it exists (using the callback provided by
      nsPluginHost).
   b) then hopefully dispatch to a background thread, where it will read
      pluginreg.dat, scan the appropriate folders on disk, and see if
      anything changed. Once done, it sets mFinishedFinding to true and
      re-dispatches itself to the main thread.
   c) then on the main thread, it reports any changes to nsPluginHost.
3. if dispatching in 2(b) fails, we will run steps (b) and (c) on the main
   thread.

Note: if ReloadPlugins is called, we intiially do (1), but if we find
changes, we clear out the set of known plugins and then run LoadPlugins
again (meaning we go through 2 (or 3 if 2(b) fails)). This is how
reloading plugins worked prior to my changes and I've attempted not to
change it.

In order for this to work, there are some other changes in this commit:

- the sandbox prefs are being read "early" and cached for flash vs
  "everything else". We can't access prefs on non-main threads without
  using StaticPrefs, which doesn't seem worth it here.
- some of the plugin tag classes are moved to threadsafe refcounting.
  This is a bit unfortunate, but because they're instantiated on a non-
  mainthread, and then later used on the main thread, despite the
  fact that the architecture means nothing tries to touch them from
  more than one thread at once, without threadsafe refcounting we hit
  asserts in debug mode if we add references to them back on the main thread.
- we add shutdown blocking for pluginfinding. We don't really want to
  be halfway through finding plugins and then trying to shut them down,
  or re-instantiating plugins after they've been unloaded.
- we keep a reference to the "pending" pluginfinder instance while
  doing lookups away from the main thread (ie (2)), to avoid re-entrancy or
  trying to write to pluginreg while we're reading it somewhere else,
  etc. If there's an attempt to do more plugin finding while this is
  ongoing, we flip mDoReloadOnceFindingFinished and do a reload once
  our initial lookups are complete.

Depends on D48331

Differential Revision: https://phabricator.services.mozilla.com/D48332

diff --git a/dom/plugins/base/PluginFinder.cpp b/dom/plugins/base/PluginFinder.cpp
--- a/dom/plugins/base/PluginFinder.cpp
+++ b/dom/plugins/base/PluginFinder.cpp
@@ -10,16 +10,17 @@
 
 #include "nsIFile.h"
 #include "nsISimpleEnumerator.h"
 #include "nsXULAppAPI.h"
 #include "nsIXULRuntime.h"
 #if defined(XP_MACOSX)
 #  include "nsILocalFileMac.h"
 #endif
+#include "nsIWritablePropertyBag2.h"
 
 #include "mozilla/ClearOnShutdown.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/Result.h"
 #include "mozilla/ResultExtensions.h"
 
 #include "nscore.h"
 #include "prio.h"
@@ -111,36 +112,79 @@ nsPluginTag* PluginFinder::FirstPluginWi
   for (nsPluginTag* tag = mPlugins; tag; tag = tag->mNext) {
     if (tag->mFullPath.Equals(path)) {
       return tag;
     }
   }
   return nullptr;
 }
 
-NS_IMPL_ISUPPORTS(PluginFinder, nsIRunnable)
+NS_IMPL_ISUPPORTS(PluginFinder, nsIRunnable, nsIAsyncShutdownBlocker)
+
+// nsIAsyncShutdownBlocker
+nsresult PluginFinder::GetName(nsAString& aName) {
+  aName.AssignLiteral("PluginFinder: finding plugins to load");
+  return NS_OK;
+}
+
+NS_IMETHODIMP PluginFinder::BlockShutdown(
+    nsIAsyncShutdownClient* aBarrierClient) {
+  // Finding plugins isn't interruptable, but we can attempt to prevent loading
+  // the plugins in nsPluginHost.
+  mShutdown = true;
+  return NS_OK;
+}
+
+NS_IMETHODIMP PluginFinder::GetState(nsIPropertyBag** aBagOut) {
+  MOZ_ASSERT(aBagOut);
+  nsCOMPtr<nsIWritablePropertyBag2> propertyBag =
+      do_CreateInstance("@mozilla.org/hash-property-bag;1");
+
+  if (NS_WARN_IF(!propertyBag)) {
+    return NS_ERROR_OUT_OF_MEMORY;
+  }
+  propertyBag->SetPropertyAsBool(NS_LITERAL_STRING("Finding"),
+                                 !mFinishedFinding);
+  propertyBag->SetPropertyAsBool(NS_LITERAL_STRING("CreatingList"),
+                                 mCreateList);
+  propertyBag->SetPropertyAsBool(NS_LITERAL_STRING("HavePlugins"), !!mPlugins);
+  propertyBag.forget(aBagOut);
+  return NS_OK;
+}
 
 PluginFinder::PluginFinder()
     : mCreateList(false),
       mPluginsChanged(false),
-      mFinishedFinding(false) {}
+      mFinishedFinding(false),
+      mCalledOnMainthread(false),  // overwritten from Run()
+      mShutdown(false) {}
 
 nsresult PluginFinder::DoFullSearch(const FoundPluginCallback& aCallback) {
   MOZ_ASSERT(XRE_IsParentProcess() && NS_IsMainThread());
   mCreateList = true;
   mFoundPluginCallback = std::move(aCallback);
 
   nsresult rv = EnsurePluginReg();
   // We pass this back to the caller, and ignore all other errors.
   if (rv == NS_ERROR_NOT_AVAILABLE) {
     return rv;
   }
 
   // Figure out plugin directories:
   MOZ_TRY(DeterminePluginDirs());
+  // If we're only interested in flash, provide an initial update from
+  // cache. We'll do a scan async, away from the main thread, and if
+  // there are updates then we'll pass them in.
+  // Don't do a blocklist check until we're done scanning,
+  // as the version might change anyway.
+  nsTArray<mozilla::Pair<bool, RefPtr<nsPluginTag>>> arr;
+  mFoundPluginCallback(!!mPlugins, mPlugins, arr);
+  // We've passed ownership of the flash plugin to the host, so make sure
+  // we don't accidentally try to use it when we leave the mainthread.
+  mPlugins = nullptr;
   return NS_OK;
 }
 
 nsresult PluginFinder::HavePluginsChanged(
     const PluginChangeCallback& aCallback) {
   MOZ_ASSERT(XRE_IsParentProcess() && NS_IsMainThread());
   mChangeCallback = std::move(aCallback);
 
@@ -152,37 +196,48 @@ nsresult PluginFinder::HavePluginsChange
 
   // Figure out plugin directories:
   MOZ_TRY(DeterminePluginDirs());
   return NS_OK;
 }
 
 nsresult PluginFinder::Run() {
   if (!mFinishedFinding) {
-    MOZ_TRY(FindPlugins());
+    mCalledOnMainthread = NS_IsMainThread();
     mFinishedFinding = true;
-  }
-  if (mPluginsChanged) {
-    WritePluginInfo(mPlugins, mInvalidPlugins);
+    FindPlugins();  // Will call WritePluginInfo()
+    if (!mCalledOnMainthread) {
+      return NS_DispatchToMainThread(this);
+    }
   }
-  // mPluginsChanged is false if all we have is flash and it hasn't changed.
-  // The host won't load plugins unless mPluginsChanged is true, so ensure it
-  // is.
-  if (mCreateList) {
-    mFoundPluginCallback(mPluginsChanged || !!mPlugins, mPlugins,
-                         mPluginBlocklistRequests);
-  } else {
-    mChangeCallback(mPluginsChanged);
+  
+  MOZ_ASSERT(NS_IsMainThread());
+  if (!mShutdown) {
+    if (mCreateList) {
+      mFoundPluginCallback(mPluginsChanged, mPlugins, mPluginBlocklistRequests);
+    } else {
+      mChangeCallback(mPluginsChanged);
+    }
   }
 
   // The host will adopt these plugin tag instances when given them,
   // so we should delete our references.
   mPlugins = nullptr;  // Don't iterate over these as the host needs them.
   NS_ITERATIVE_UNREF_LIST(RefPtr<nsInvalidPluginTag>, mInvalidPlugins, mNext);
   NS_ITERATIVE_UNREF_LIST(RefPtr<nsPluginTag>, mCachedPlugins, mNext);
+
+  // We need to tell the host we're done. We're on the main thread,
+  // but we could still be in the process of creating the pluginhost,
+  // if we were called synchronously. In that case, the host is
+  // responsible for cleaning us up, as we can't tell it to do so.
+  // So if we were called *async*, tell the host we're done:
+  if (mCreateList && !mCalledOnMainthread) {
+    RefPtr<nsPluginHost> host = nsPluginHost::GetInst();
+    host->FindingFinished();
+  }
   return NS_OK;
 }
 
 namespace {
 
 int64_t GetPluginLastModifiedTime(const nsCOMPtr<nsIFile>& localfile) {
   PRTime fileModTime = 0;
 
@@ -371,17 +426,18 @@ nsresult PluginFinder::ScanPluginsDirect
 
   return NS_OK;
 }
 
 // if mCreateList is false we will just scan for plugins
 // and see if any changes have been made to the plugins.
 // This is needed in ReloadPlugins to prevent possible recursive reloads
 nsresult PluginFinder::FindPlugins() {
-  // Read cached plugins info.
+  // Read cached plugins info. We ignore failures, as we can proceed
+  // without a pluginreg file.
   ReadPluginInfo();
 
   for (nsIFile* pluginDir : mPluginDirs) {
     if (!pluginDir) {
       continue;
     }
     // Ensure we have a directory; if this isn't a directory, try the parent.
     // We do this because in some cases items in this list of supposed plugin
@@ -547,16 +603,20 @@ nsresult PluginFinder::FindPlugins() {
   return rv;
 }
 
 /* static */ nsresult PluginFinder::EnsurePluginReg() {
   if (sPluginRegFile) {
     return NS_OK;
   }
 
+  if (!NS_IsMainThread()) {
+    return NS_ERROR_FAILURE;
+  }
+
   nsresult rv;
   nsCOMPtr<nsIProperties> directoryService(
       do_GetService(NS_DIRECTORY_SERVICE_CONTRACTID, &rv));
   if (NS_FAILED(rv)) return rv;
 
   nsCOMPtr<nsIFile> pluginRegFile;
   directoryService->Get(NS_APP_USER_PROFILE_50_DIR, NS_GET_IID(nsIFile),
                         getter_AddRefs(pluginRegFile));
@@ -624,20 +684,24 @@ nsresult PluginFinder::DeterminePluginDi
   }
 #endif /* XP_WIN */
   return NS_OK;
 }
 
 nsresult PluginFinder::ReadPluginInfo() {
   MOZ_ASSERT(XRE_IsParentProcess());
 
-  return ReadPluginInfoFromDisk();
-}
+  // Non-flash-only (for tests that use other plugins) and updates to the list
+  // sadly still use the main thread - but assert about the flash startup case:
+  if (mCreateList) {
+    MOZ_ASSERT(!NS_IsMainThread());
+  } else {
+    MOZ_ASSERT(NS_IsMainThread());
+  }
 
-nsresult PluginFinder::ReadPluginInfoFromDisk() {
   nsresult rv = EnsurePluginReg();
   if (NS_FAILED(rv)) {
     return rv;
   }
 
   // Cloning ensures we don't have a stat cache and get an
   // accurate filesize.
   nsCOMPtr<nsIFile> pluginReg;
diff --git a/dom/plugins/base/PluginFinder.h b/dom/plugins/base/PluginFinder.h
--- a/dom/plugins/base/PluginFinder.h
+++ b/dom/plugins/base/PluginFinder.h
@@ -7,29 +7,29 @@
 #define PluginFinder_h_
 
 #include "nsIRunnable.h"
 #include "mozilla/RefPtr.h"
 #include "nsCOMPtr.h"
 #include "nsString.h"
 #include "nsTArray.h"
 #include "nsPluginTags.h"
-#include "nsIRunnable.h"
+#include "nsIAsyncShutdown.h"
 
 class nsIFile;
 class nsPluginHost;
 
 class nsInvalidPluginTag : public nsISupports {
   virtual ~nsInvalidPluginTag();
 
  public:
   explicit nsInvalidPluginTag(const char* aFullPath,
                               int64_t aLastModifiedTime = 0);
 
-  NS_DECL_ISUPPORTS
+  NS_DECL_THREADSAFE_ISUPPORTS
 
   nsCString mFullPath;
   int64_t mLastModifiedTime;
   bool mSeen;
 
   RefPtr<nsInvalidPluginTag> mPrev;
   RefPtr<nsInvalidPluginTag> mNext;
 };
@@ -41,24 +41,25 @@ static inline bool UnloadPluginsASAP() {
 /**
  * Class responsible for discovering plugins on disk, in part based
  * on cached information stored in pluginreg.dat and in the prefs.
  *
  * This runnable is designed to be used as a one-shot. It's created
  * when the pluginhost wants to find plugins, and the next time it
  * wants to do so, it should create a new one.
  */
-class PluginFinder final : public nsIRunnable {
+class PluginFinder final : public nsIRunnable, public nsIAsyncShutdownBlocker {
   ~PluginFinder() = default;
 
  public:
   explicit PluginFinder();
 
-  NS_DECL_ISUPPORTS
+  NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIRUNNABLE
+  NS_DECL_NSIASYNCSHUTDOWNBLOCKER
 
   typedef std::function<void(
       bool /* aPluginsChanged */, RefPtr<nsPluginTag> /* aNewPlugins */,
       nsTArray<mozilla::Pair<
           bool, RefPtr<nsPluginTag>>>&) /* aBlocklistRequestPairs */>
       FoundPluginCallback;
   typedef std::function<void(bool /* aPluginsChanged */)> PluginChangeCallback;
 
@@ -109,11 +110,13 @@ class PluginFinder final : public nsIRun
 
   FoundPluginCallback mFoundPluginCallback;
   PluginChangeCallback mChangeCallback;
   RefPtr<nsPluginHost> mHost;
 
   bool mCreateList;
   bool mPluginsChanged;
   bool mFinishedFinding;
+  bool mCalledOnMainthread;
+  bool mShutdown;
 };
 
 #endif
diff --git a/dom/plugins/base/nsPluginHost.cpp b/dom/plugins/base/nsPluginHost.cpp
--- a/dom/plugins/base/nsPluginHost.cpp
+++ b/dom/plugins/base/nsPluginHost.cpp
@@ -11,16 +11,17 @@
 #include <cstdlib>
 #include <stdio.h>
 #include "nsExceptionHandler.h"
 #include "nsNPAPIPlugin.h"
 #include "nsNPAPIPluginStreamListener.h"
 #include "nsNPAPIPluginInstance.h"
 #include "nsPluginInstanceOwner.h"
 #include "nsObjectLoadingContent.h"
+#include "nsIEventTarget.h"
 #include "nsIHTTPHeaderListener.h"
 #include "nsIHttpHeaderVisitor.h"
 #include "nsIObserverService.h"
 #include "nsIHttpProtocolHandler.h"
 #include "nsIHttpChannel.h"
 #include "nsIUploadChannel.h"
 #include "nsIByteRangeRequest.h"
 #include "nsIStreamListener.h"
@@ -155,16 +156,18 @@ IsTypeInList(const nsCString& aMimeType,
 
   return FindInReadable(commaSeparated, start, end);
 }
 
 nsPluginHost::nsPluginHost()
   : mPluginsLoaded(false)
   , mOverrideInternalTypes(false)
   , mPluginsDisabled(false)
+  , mDoReloadOnceFindingFinished(false)
+  , mAddedFinderShutdownBlocker(false)
   , mPluginEpoch(0)
 {
   // check to see if pref is set at startup to let plugins take over in
   // full page mode for certain image mime types that we handle internally
   mOverrideInternalTypes =
     Preferences::GetBool("plugin.override_internal_types", false);
 
   mPluginsDisabled = Preferences::GetBool("plugin.disable", false);
@@ -271,24 +274,35 @@ nsresult nsPluginHost::ReloadPlugins()
     // called. See Bug 1337058 for more info.
     return NS_ERROR_PLUGINS_PLUGINSNOTCHANGED;
   }
   // this will create the initial plugin list out of cache
   // if it was not created yet
   if (!mPluginsLoaded)
     return LoadPlugins();
 
+  // We're already in the process of finding more plugins. Do it again once
+  // done (because maybe things have changed since we started looking).
+  if (mPendingFinder) {
+    mDoReloadOnceFindingFinished = true;
+    return NS_ERROR_PLUGINS_PLUGINSNOTCHANGED;
+  }
+
   // we are re-scanning plugins. New plugins may have been added, also some
   // plugins may have been removed, so we should probably shut everything down
   // but don't touch running (active and not stopped) plugins
 
   // check if plugins changed, no need to do anything else
   // if no changes to plugins have been made
-  // false instructs not to touch the plugin list, just to
-  // look for possible changes
+  // We're doing this on the main thread, and we checked mPendingFinder
+  // above, so we don't need to do anything else to ensure we don't re-enter.
+  // Future work may make this asynchronous and do the finding away from
+  // the mainthread for the reload case, too, (in which case we should use
+  // mPendingFinder instead of a local copy, and update PluginFinder) but at
+  // the moment this is less important than the initial finding on startup.
   RefPtr<PluginFinder> pf = new PluginFinder();
   bool pluginschanged;
   MOZ_TRY(pf->HavePluginsChanged([&pluginschanged](bool aPluginsChanged) {
     pluginschanged = aPluginsChanged;
   }));
   pf->Run();
 
   // if no changed detected, return an appropriate error code
@@ -1767,79 +1781,152 @@ WatchRegKey(uint32_t aRoot, nsCOMPtr<nsI
   if (NS_FAILED(rv)) {
     aKey = nullptr;
     return;
   }
   aKey->StartWatching(true);
 }
 #endif
 
+already_AddRefed<nsIAsyncShutdownClient> GetProfileChangeTeardownPhase() {
+  nsCOMPtr<nsIAsyncShutdownService> asyncShutdownSvc =
+      services::GetAsyncShutdown();
+  MOZ_ASSERT(asyncShutdownSvc);
+  if (NS_WARN_IF(!asyncShutdownSvc)) {
+    return nullptr;
+  }
+
+  nsCOMPtr<nsIAsyncShutdownClient> shutdownPhase;
+  DebugOnly<nsresult> rv =
+      asyncShutdownSvc->GetProfileChangeTeardown(getter_AddRefs(shutdownPhase));
+  MOZ_ASSERT(NS_SUCCEEDED(rv));
+  return shutdownPhase.forget();
+}
+
 nsresult nsPluginHost::LoadPlugins()
 {
   // This should only be run in the parent process. On plugin list change, we'll
   // update observers in the content process as part of SetPluginsInContent
   if (XRE_IsContentProcess()) {
     return NS_OK;
   }
   // do not do anything if it is already done
   // use ReloadPlugins() to enforce loading
   if (mPluginsLoaded)
     return NS_OK;
 
+  // Uh oh, someone's forcing us to load plugins, but we're already in the
+  // process of doing so. Schedule a reload for when we're done:
+  if (mPendingFinder) {
+    mDoReloadOnceFindingFinished = true;
+    return NS_OK;
+  }
+
   if (mPluginsDisabled)
     return NS_OK;
 
 #ifdef XP_WIN
   WatchRegKey(nsIWindowsRegKey::ROOT_KEY_LOCAL_MACHINE, mRegKeyHKLM);
   WatchRegKey(nsIWindowsRegKey::ROOT_KEY_CURRENT_USER, mRegKeyHKCU);
 #endif
 
-  // This is a runnable. In the future, we'll do some of this work async, on a
-  // different thread.
-  RefPtr<PluginFinder> pf = new PluginFinder();
+  // This is a runnable. The main part of its work will be done away from the
+  // main thread.
+  mPendingFinder = new PluginFinder();
+  mDoReloadOnceFindingFinished = false;
+  mAddedFinderShutdownBlocker = false;
   RefPtr<nsPluginHost> self = this;
-  nsresult rv = pf->DoFullSearch(
+  // Note that if we're in flash only mode, which is the default, then the
+  // callback will be called twice. Once for flash (or nothing, if we're not
+  // (yet) aware of flash being present), and then again after we've actually
+  // looked for it on disk.
+  nsresult rv = mPendingFinder->DoFullSearch(
       [self](
           bool aPluginsChanged, RefPtr<nsPluginTag> aPlugins,
           const nsTArray<Pair<bool, RefPtr<nsPluginTag>>>& aBlocklistRequests) {
         MOZ_ASSERT(NS_IsMainThread(),
                    "Callback should only be called on the main thread.");
         self->mPluginsLoaded = true;
         if (aPluginsChanged) {
           self->ClearNonRunningPlugins();
           while (aPlugins) {
             RefPtr<nsPluginTag> pluginTag = aPlugins;
             aPlugins = aPlugins->mNext;
             self->AddPluginTag(pluginTag);
           }
-          self->IncrementChromeEpoch();
+          self->SendPluginsToContent();
         }
 
         if (aPluginsChanged) {
           nsCOMPtr<nsIObserverService> obsService =
               mozilla::services::GetObserverService();
           if (obsService) {
             obsService->NotifyObservers(nullptr, "plugins-list-updated",
                                         nullptr);
           }
         }
       });
   // Deal with the profile not being ready yet by returning NS_OK - we'll get
   // the data later.
   if (NS_FAILED(rv)) {
+    mPendingFinder = nullptr;
     if (rv == NS_ERROR_NOT_AVAILABLE) {
       return NS_OK;
     }
     return rv;
   }
-  pf->Run();
+  bool dispatched = false;
+
+  // If we're only looking for flash (the default), try to do so away from
+  // the main thread. Note that in this case, the callback may have been called
+  // already, from the cached plugin info.
+  // First add the shutdown blocker, to avoid the potential for race
+  // conditions.
+  nsCOMPtr<nsIAsyncShutdownClient> shutdownPhase =
+      GetProfileChangeTeardownPhase();
+  if (shutdownPhase) {
+    rv =
+        shutdownPhase->AddBlocker(mPendingFinder, NS_LITERAL_STRING(__FILE__),
+                                  __LINE__, NS_LITERAL_STRING(""));
+    mAddedFinderShutdownBlocker = NS_SUCCEEDED(rv);
+  }
+
+  nsCOMPtr<nsIEventTarget> target =
+      do_GetService(NS_STREAMTRANSPORTSERVICE_CONTRACTID, &rv);
+  if (NS_SUCCEEDED(rv)) {
+    rv = target->Dispatch(mPendingFinder, nsIEventTarget::DISPATCH_NORMAL);
+    dispatched = NS_SUCCEEDED(rv);
+  }
+  // If we somehow failed to dispatch, remove the shutdown blocker.
+  if (mAddedFinderShutdownBlocker && !dispatched) {
+    shutdownPhase->RemoveBlocker(mPendingFinder);
+    mAddedFinderShutdownBlocker = false;
+  }
+  if (!dispatched) {
+    mPendingFinder->Run();
+    // We're running synchronously, so just remove the pending finder now.
+    mPendingFinder = nullptr;
+  }
 
   return NS_OK;
 }
 
+void nsPluginHost::FindingFinished() {
+  if (mAddedFinderShutdownBlocker) {
+    nsCOMPtr<nsIAsyncShutdownClient> shutdownPhase =
+        GetProfileChangeTeardownPhase();
+    shutdownPhase->RemoveBlocker(mPendingFinder);
+    mAddedFinderShutdownBlocker = false;
+  }
+  mPendingFinder = nullptr;
+  if (mDoReloadOnceFindingFinished) {
+    Unused << ReloadPlugins();
+  }
+}
+
 nsresult
 nsPluginHost::SetPluginsInContent(uint32_t aPluginEpoch,
                                   nsTArray<mozilla::plugins::PluginTag>& aPlugins,
                                   nsTArray<mozilla::plugins::FakePluginTag>& aFakePlugins)
 {
   MOZ_ASSERT(XRE_IsContentProcess());
 
   nsTArray<PluginTag> plugins;
diff --git a/dom/plugins/base/nsPluginHost.h b/dom/plugins/base/nsPluginHost.h
--- a/dom/plugins/base/nsPluginHost.h
+++ b/dom/plugins/base/nsPluginHost.h
@@ -51,24 +51,27 @@ class nsNPAPIPluginStreamListener;
 class nsIPluginInstanceOwner;
 class nsIInputStream;
 class nsIStreamListener;
 #ifndef npapi_h_
 struct _NPP;
 typedef _NPP* NPP;
 #endif
 
+class PluginFinder;
+
 class nsPluginHost final : public nsIPluginHost,
                            public nsIObserver,
                            public nsITimerCallback,
                            public nsSupportsWeakReference,
                            public nsINamed
 {
   friend class nsPluginTag;
   friend class nsFakePluginTag;
+  friend class PluginFinder;
   virtual ~nsPluginHost();
 
 public:
   nsPluginHost();
 
   static already_AddRefed<nsPluginHost> GetInst();
 
   NS_DECL_ISUPPORTS
@@ -308,28 +311,36 @@ private:
   uint32_t ChromeEpochForContent();
   void SetChromeEpochForContent(uint32_t aEpoch);
 
   void UpdateInMemoryPluginInfo(nsPluginTag* aPluginTag);
 
   void ClearNonRunningPlugins();
   nsresult ActuallyReloadPlugins();
 
+  // Callback into the host from PluginFinder once it's done.
+  void FindingFinished();
+
   RefPtr<nsPluginTag> mPlugins;
 
   nsTArray< RefPtr<nsFakePluginTag> > mFakePlugins;
 
   bool mPluginsLoaded;
 
   // set by pref plugin.override_internal_types
   bool mOverrideInternalTypes;
 
   // set by pref plugin.disable
   bool mPluginsDisabled;
 
+  // Only one plugin finding operation should be run at a time.
+  RefPtr<PluginFinder> mPendingFinder;
+  bool mDoReloadOnceFindingFinished;
+  bool mAddedFinderShutdownBlocker;
+
   // Any instances in this array will have valid plugin objects via GetPlugin().
   // When removing an instance it might not die - be sure to null out it's plugin.
   nsTArray< RefPtr<nsNPAPIPluginInstance> > mInstances;
 
   // An nsIFile for the pluginreg.dat file in the profile.
 #ifdef XP_WIN
   // In order to reload plugins when they change, we watch the registry via
   // this object.
diff --git a/dom/plugins/base/nsPluginTags.cpp b/dom/plugins/base/nsPluginTags.cpp
--- a/dom/plugins/base/nsPluginTags.cpp
+++ b/dom/plugins/base/nsPluginTags.cpp
@@ -31,16 +31,28 @@ using namespace mozilla;
 // no longer used                   0x0002    // reuse only if regenerating pluginreg.dat
 #define NS_PLUGIN_FLAG_FROMCACHE    0x0004    // this plugintag info was loaded from cache
 // no longer used                   0x0008    // reuse only if regenerating pluginreg.dat
 #define NS_PLUGIN_FLAG_CLICKTOPLAY  0x0020    // this is a click-to-play plugin
 
 static const char kPrefDefaultEnabledState[] = "plugin.default.state";
 static const char kPrefDefaultEnabledStateXpi[] = "plugin.defaultXpi.state";
 
+// The defaults here will be read from prefs and overwritten
+#if defined(MOZ_SANDBOX)
+#  if defined(XP_WIN) || defined(XP_MACOSX)
+static int32_t sFlashSandboxLevel = 0;
+static int32_t sDefaultSandboxLevel = 0;
+#  endif
+#  if defined(XP_MACOSX)
+static bool sEnableSandboxLogging = false;
+#  endif
+#endif /* MOZ_SANDBOX */
+static bool sInitializedSandboxingInfo = false;
+
 // check comma delimited extensions
 static bool ExtensionInList(const nsCString& aExtensionList,
                             const nsACString& aExtension)
 {
   nsCCharSeparatedTokenizer extensions(aExtensionList, ',');
   while (extensions.hasMoreTokens()) {
     const nsACString& extension = extensions.nextToken();
     if (extension.Equals(aExtension, nsCaseInsensitiveCStringComparator())) {
@@ -389,33 +401,23 @@ void nsPluginTag::InitMime(const char* c
       mExtensions.AppendElement(nsCString());
     }
   }
 }
 
 void
 nsPluginTag::InitSandboxLevel()
 {
-#if defined(XP_WIN) && defined(MOZ_SANDBOX)
-  nsAutoCString sandboxPref("dom.ipc.plugins.sandbox-level.");
-  sandboxPref.Append(GetNiceFileName());
-  if (NS_FAILED(Preferences::GetInt(sandboxPref.get(), &mSandboxLevel))) {
-    mSandboxLevel = Preferences::GetInt("dom.ipc.plugins.sandbox-level.default"
-);
-  }
-
-#if defined(_AMD64_)
-  // As level 2 is now the default NPAPI sandbox level for 64-bit flash, we
-  // don't want to allow a lower setting. This should be changed if the
-  // firefox.js pref file is changed.
-  if (mIsFlashPlugin && mSandboxLevel < 2) {
-    mSandboxLevel = 2;
-  }
-#endif
-#endif
+  MOZ_ASSERT(sInitializedSandboxingInfo,
+             "Should have initialized global sandboxing info");
+#if defined(MOZ_SANDBOX)
+#  if defined(XP_WIN)
+  mSandboxLevel = mIsFlashPlugin ? sFlashSandboxLevel : sDefaultSandboxLevel;
+#  endif /* defined(XP_WIN) */
+#endif   /* defined(MOZ_SANDBOX) */
 }
 
 #if !defined(XP_WIN) && !defined(XP_MACOSX)
 static void
 ConvertToUTF8(nsCString& aString)
 {
   Unused << UTF_8_ENCODING->DecodeWithoutBOMHandling(aString, aString);
 }
diff --git a/dom/plugins/base/nsPluginTags.cpp.1616404.later b/dom/plugins/base/nsPluginTags.cpp.1616404.later
deleted file mode 100644
--- a/dom/plugins/base/nsPluginTags.cpp.1616404.later
+++ /dev/null
@@ -1,23 +0,0 @@
---- nsPluginTags.cpp
-+++ nsPluginTags.cpp
-@@ -615,18 +615,18 @@ void nsPluginTag::TryUnloadPlugin(bool i
- #elif defined(XP_MACOSX) && defined(MOZ_SANDBOX)
-   int legacyOSMinorMax = Preferences::GetInt(
-       "dom.ipc.plugins.sandbox-level.flash.max-legacy-os-minor", 10);
-   const char* levelPref = "dom.ipc.plugins.sandbox-level.flash";
- 
-   if (PR_GetEnv("MOZ_DISABLE_NPAPI_SANDBOX")) {
-     // Flash sandbox disabled
-     sFlashSandboxLevel = 0;
--  } else if (nsCocoaFeatures::OSXVersionMajor() == 10 &&
--             nsCocoaFeatures::OSXVersionMinor() <= legacyOSMinorMax) {
-+  } else if (nsCocoaFeatures::macOSVersionMajor() == 10 &&
-+             nsCocoaFeatures::macOSVersionMinor() <= legacyOSMinorMax) {
-     const char* legacyLevelPref = "dom.ipc.plugins.sandbox-level.flash.legacy";
-     int32_t compatLevel = Preferences::GetInt(legacyLevelPref, 0);
-     int32_t level = Preferences::GetInt(levelPref, 0);
-     sFlashSandboxLevel = std::min(compatLevel, level);
-   } else {
-     sFlashSandboxLevel = Preferences::GetInt(levelPref, 0);
-   }
-   sFlashSandboxLevel = ClampFlashSandboxLevel(sFlashSandboxLevel);
diff --git a/dom/plugins/base/nsPluginTags.h b/dom/plugins/base/nsPluginTags.h
--- a/dom/plugins/base/nsPluginTags.h
+++ b/dom/plugins/base/nsPluginTags.h
@@ -90,17 +90,17 @@ NS_DEFINE_STATIC_IID_ACCESSOR(nsIInterna
 
 // A linked-list of plugin information that is used for instantiating plugins
 // and reflecting plugin information into JavaScript.
 class nsPluginTag final : public nsIInternalPluginTag
 {
 public:
   NS_DECLARE_STATIC_IID_ACCESSOR(NS_PLUGINTAG_IID)
 
-  NS_DECL_ISUPPORTS
+  NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIPLUGINTAG
 
   // These must match the STATE_* values in nsIPluginTag.idl
   enum PluginState {
     ePluginState_Disabled = 0,
     ePluginState_Clicktoplay = 1,
     ePluginState_Enabled = 2,
     ePluginState_MaxValue = 3,
@@ -131,16 +131,18 @@ public:
               bool aIsFlashPlugin,
               bool aSupportsAsyncRender,
               int64_t aLastModifiedTime,
               int32_t aSandboxLevel,
               uint16_t aBlocklistState);
 
   void TryUnloadPlugin(bool inShutdown);
 
+  static void EnsureSandboxInformation();
+
   // plugin is enabled and not blocklisted
   bool IsActive();
 
   bool IsEnabled() override;
   void SetEnabled(bool enabled);
   bool IsClicktoplay();
   bool IsBlocklisted();
 
