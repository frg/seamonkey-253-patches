# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1507730627 -7200
# Node ID df8f9a0ccc91adf71e23280b5eefd7d3b6064b04
# Parent  4a1b1bdfb4ff807b030a6499b537af71cf39ca39
Bug 1406879: Skip wasm frames when when enabling profiler and setting profiling FP; r=jandem

MozReview-Commit-ID: EIjjda2AorV

diff --git a/js/src/jit-test/tests/wasm/regress/enable-profiling-in-import.js b/js/src/jit-test/tests/wasm/regress/enable-profiling-in-import.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/wasm/regress/enable-profiling-in-import.js
@@ -0,0 +1,9 @@
+var module = new WebAssembly.Module(wasmTextToBinary(`
+    (module
+        (import "global" "func")
+        (func (export "f")
+         call 0
+        )
+    )
+`));
+new WebAssembly.Instance(module, { global: { func: enableGeckoProfiling } }).exports.f();
diff --git a/js/src/vm/GeckoProfiler.cpp b/js/src/vm/GeckoProfiler.cpp
--- a/js/src/vm/GeckoProfiler.cpp
+++ b/js/src/vm/GeckoProfiler.cpp
@@ -64,24 +64,33 @@ GeckoProfilerRuntime::setEventMarker(voi
 
 // Get a pointer to the top-most profiling frame, given the exit frame pointer.
 static void*
 GetTopProfilingJitFrame(Activation* act)
 {
     if (!act || !act->isJit())
         return nullptr;
 
-    // For null exitFrame, there is no previous exit frame, just return.
-    uint8_t* jsExitFP = act->asJit()->jsExitFP();
-    if (!jsExitFP)
+    jit::JitActivation* jitActivation = act->asJit();
+
+    // If there is no exit frame set, just return.
+    if (!jitActivation->hasExitFP())
         return nullptr;
 
-    jit::JSJitProfilingFrameIterator iter(jsExitFP);
-    MOZ_ASSERT(!iter.done());
-    return iter.fp();
+    // Skip wasm frames that might be in the way.
+    JitFrameIter iter(jitActivation);
+    while (!iter.done() && iter.isWasm())
+        ++iter;
+
+    if (!iter.isJSJit())
+        return nullptr;
+
+    jit::JSJitProfilingFrameIterator jitIter(iter.asJSJit().fp());
+    MOZ_ASSERT(!jitIter.done());
+    return jitIter.fp();
 }
 
 void
 GeckoProfilerRuntime::enable(bool enabled)
 {
 #ifdef DEBUG
     // All cooperating contexts must have profile stacks installed before the
     // profiler can be enabled. Cooperating threads created while the profiler
