# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1516137054 21600
# Node ID e21126825c41be2a70bb6759e2dbdc7190621608
# Parent  295823981cc0d9c1382ba9fce799344c6c753045
Bug 1437955 - Part 1: Split ParseNodeKind::Semi into ExpressionStatement and EmptyStatement. r=Waldo.

diff --git a/js/src/builtin/ReflectParse.cpp b/js/src/builtin/ReflectParse.cpp
--- a/js/src/builtin/ReflectParse.cpp
+++ b/js/src/builtin/ReflectParse.cpp
@@ -2375,24 +2375,27 @@ ASTSerializer::statement(ParseNode* pn, 
       case ParseNodeKind::Import:
         return importDeclaration(pn, dst);
 
       case ParseNodeKind::Export:
       case ParseNodeKind::ExportDefault:
       case ParseNodeKind::ExportFrom:
         return exportDeclaration(pn, dst);
 
-      case ParseNodeKind::Semi:
-        if (pn->pn_kid) {
-            RootedValue expr(cx);
-            return expression(pn->pn_kid, &expr) &&
-                   builder.expressionStatement(expr, &pn->pn_pos, dst);
-        }
+      case ParseNodeKind::Nop:
+      case ParseNodeKind::EmptyStatement:
         return builder.emptyStatement(&pn->pn_pos, dst);
 
+      case ParseNodeKind::ExpressionStatement:
+      {
+        RootedValue expr(cx);
+        return expression(pn->pn_kid, &expr) &&
+            builder.expressionStatement(expr, &pn->pn_pos, dst);
+      }
+
       case ParseNodeKind::LexicalScope:
         pn = pn->pn_expr;
         if (!pn->isKind(ParseNodeKind::StatementList))
             return statement(pn, dst);
         MOZ_FALLTHROUGH;
 
       case ParseNodeKind::StatementList:
         return blockStatement(pn, dst);
@@ -2554,19 +2557,16 @@ ASTSerializer::statement(ParseNode* pn, 
             if (!classMethod(next, &prop))
                 return false;
             methods.infallibleAppend(prop);
         }
 
         return builder.classMethods(methods, dst);
       }
 
-      case ParseNodeKind::Nop:
-        return builder.emptyStatement(&pn->pn_pos, dst);
-
       default:
         LOCAL_NOT_REACHED("unexpected statement type");
     }
 }
 
 bool
 ASTSerializer::classMethod(ParseNode* pn, MutableHandleValue dst)
 {
@@ -2783,17 +2783,17 @@ ASTSerializer::generatorExpression(Parse
                     return false;
             }
             next = next->pn_kid2;
         } else {
             break;
         }
     }
 
-    LOCAL_ASSERT(next->isKind(ParseNodeKind::Semi) &&
+    LOCAL_ASSERT(next->isKind(ParseNodeKind::ExpressionStatement) &&
                  next->pn_kid->isKind(ParseNodeKind::Yield) &&
                  next->pn_kid->pn_kid);
 
     RootedValue body(cx);
 
     return expression(next->pn_kid->pn_kid, &body) &&
            builder.generatorExpression(body, blocks, filter, isLegacy, &pn->pn_pos, dst);
 }
diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -3098,16 +3098,17 @@ BytecodeEmitter::checkSideEffects(ParseN
     if (!CheckRecursionLimit(cx))
         return false;
 
  restart:
 
     switch (pn->getKind()) {
       // Trivial cases with no side effects.
       case ParseNodeKind::Nop:
+      case ParseNodeKind::EmptyStatement:
       case ParseNodeKind::String:
       case ParseNodeKind::TemplateString:
       case ParseNodeKind::RegExp:
       case ParseNodeKind::True:
       case ParseNodeKind::False:
       case ParseNodeKind::Null:
       case ParseNodeKind::RawUndefined:
       case ParseNodeKind::Elision:
@@ -3231,22 +3232,19 @@ BytecodeEmitter::checkSideEffects(ParseN
       // Deletion of a non-Reference expression has side effects only through
       // evaluating the expression.
       case ParseNodeKind::DeleteExpr: {
         MOZ_ASSERT(pn->isArity(PN_UNARY));
         ParseNode* expr = pn->pn_kid;
         return checkSideEffects(expr, answer);
       }
 
-      case ParseNodeKind::Semi:
+      case ParseNodeKind::ExpressionStatement:
         MOZ_ASSERT(pn->isArity(PN_UNARY));
-        if (ParseNode* expr = pn->pn_kid)
-            return checkSideEffects(expr, answer);
-        *answer = false;
-        return true;
+        return checkSideEffects(pn->pn_kid, answer);
 
       // Binary cases with obvious side effects.
       case ParseNodeKind::Assign:
       case ParseNodeKind::AddAssign:
       case ParseNodeKind::SubAssign:
       case ParseNodeKind::BitOrAssign:
       case ParseNodeKind::BitXorAssign:
       case ParseNodeKind::BitAndAssign:
@@ -9044,23 +9042,19 @@ BytecodeEmitter::emitStatementList(Parse
     for (ParseNode* pn2 = pn->pn_head; pn2; pn2 = pn2->pn_next) {
         if (!emitTree(pn2))
             return false;
     }
     return true;
 }
 
 bool
-BytecodeEmitter::emitStatement(ParseNode* pn)
-{
-    MOZ_ASSERT(pn->isKind(ParseNodeKind::Semi));
-
-    ParseNode* pn2 = pn->pn_kid;
-    if (!pn2)
-        return true;
+BytecodeEmitter::emitExpressionStatement(ParseNode* pn)
+{
+    MOZ_ASSERT(pn->isKind(ParseNodeKind::ExpressionStatement));
 
     if (!updateSourceCoordNotes(pn->pn_pos.begin))
         return false;
 
     /*
      * Top-level or called-from-a-native JS_Execute/EvaluateScript,
      * debugger, and eval frames may need the value of the ultimate
      * expression statement as the script's result, despite the fact
@@ -9072,18 +9066,19 @@ BytecodeEmitter::emitStatement(ParseNode
     bool wantval = false;
     bool useful = false;
     if (sc->isFunctionBox())
         MOZ_ASSERT(!script->noScriptRval());
     else
         useful = wantval = !script->noScriptRval();
 
     /* Don't eliminate expressions with side effects. */
+    ParseNode* expr = pn->pn_kid;
     if (!useful) {
-        if (!checkSideEffects(pn2, &useful))
+        if (!checkSideEffects(expr, &useful))
             return false;
 
         /*
          * Don't eliminate apparently useless expressions if they are labeled
          * expression statements. The startOffset() test catches the case
          * where we are nesting in emitTree for a labeled compound statement.
          */
         if (innermostNestableControl &&
@@ -9092,18 +9087,18 @@ BytecodeEmitter::emitStatement(ParseNode
         {
             useful = true;
         }
     }
 
     if (useful) {
         JSOp op = wantval ? JSOP_SETRVAL : JSOP_POP;
         ValueUsage valueUsage = wantval ? ValueUsage::WantValue : ValueUsage::IgnoreValue;
-        MOZ_ASSERT_IF(pn2->isKind(ParseNodeKind::Assign), pn2->isOp(JSOP_NOP));
-        if (!emitTree(pn2, valueUsage))
+        MOZ_ASSERT_IF(expr->isKind(ParseNodeKind::Assign), expr->isOp(JSOP_NOP));
+        if (!emitTree(expr, valueUsage))
             return false;
         if (!emit1(op))
             return false;
     } else if (pn->isDirectivePrologueMember()) {
         // Don't complain about directive prologue members; just don't emit
         // their code.
     } else {
         if (JSAtom* atom = pn->isStringExprStatement()) {
@@ -9119,21 +9114,21 @@ BytecodeEmitter::emitStatement(ParseNode
             } else if (atom == cx->names().useAsm) {
                 if (sc->isFunctionBox()) {
                     if (IsAsmJSModule(sc->asFunctionBox()->function()))
                         directive = js_useAsm_str;
                 }
             }
 
             if (directive) {
-                if (!reportExtraWarning(pn2, JSMSG_CONTRARY_NONDIRECTIVE, directive))
+                if (!reportExtraWarning(expr, JSMSG_CONTRARY_NONDIRECTIVE, directive))
                     return false;
             }
         } else {
-            if (!reportExtraWarning(pn2, JSMSG_USELESS_EXPR))
+            if (!reportExtraWarning(expr, JSMSG_USELESS_EXPR))
                 return false;
         }
     }
 
     return true;
 }
 
 bool
@@ -11070,18 +11065,21 @@ BytecodeEmitter::emitTree(ParseNode* pn,
             return false;
         break;
 
       case ParseNodeKind::StatementList:
         if (!emitStatementList(pn))
             return false;
         break;
 
-      case ParseNodeKind::Semi:
-        if (!emitStatement(pn))
+      case ParseNodeKind::EmptyStatement:
+        break;
+
+      case ParseNodeKind::ExpressionStatement:
+        if (!emitExpressionStatement(pn))
             return false;
         break;
 
       case ParseNodeKind::Label:
         if (!emitLabeledStatement(&pn->as<LabeledStatement>()))
             return false;
         break;
 
diff --git a/js/src/frontend/BytecodeEmitter.h b/js/src/frontend/BytecodeEmitter.h
--- a/js/src/frontend/BytecodeEmitter.h
+++ b/js/src/frontend/BytecodeEmitter.h
@@ -785,17 +785,17 @@ struct MOZ_STACK_CLASS BytecodeEmitter
     MOZ_MUST_USE bool emitInitializer(ParseNode* initializer, ParseNode* pattern);
     MOZ_MUST_USE bool emitInitializerInBranch(ParseNode* initializer, ParseNode* pattern);
 
     MOZ_MUST_USE bool emitCallSiteObject(ParseNode* pn);
     MOZ_MUST_USE bool emitTemplateString(ParseNode* pn);
     MOZ_MUST_USE bool emitAssignment(ParseNode* lhs, ParseNodeKind pnk, ParseNode* rhs);
 
     MOZ_MUST_USE bool emitReturn(ParseNode* pn);
-    MOZ_MUST_USE bool emitStatement(ParseNode* pn);
+    MOZ_MUST_USE bool emitExpressionStatement(ParseNode* pn);
     MOZ_MUST_USE bool emitStatementList(ParseNode* pn);
 
     MOZ_MUST_USE bool emitDeleteName(ParseNode* pn);
     MOZ_MUST_USE bool emitDeleteProperty(ParseNode* pn);
     MOZ_MUST_USE bool emitDeleteElement(ParseNode* pn);
     MOZ_MUST_USE bool emitDeleteExpression(ParseNode* pn);
 
     // |op| must be JSOP_TYPEOF or JSOP_TYPEOFEXPR.
diff --git a/js/src/frontend/FoldConstants.cpp b/js/src/frontend/FoldConstants.cpp
--- a/js/src/frontend/FoldConstants.cpp
+++ b/js/src/frontend/FoldConstants.cpp
@@ -98,23 +98,24 @@ ContainsHoistedDeclaration(JSContext* cx
         return true;
 
       case ParseNodeKind::Module:
         *result = false;
         return true;
 
       // Statements with no sub-components at all.
       case ParseNodeKind::Nop: // induced by function f() {} function f() {}
+      case ParseNodeKind::EmptyStatement:
       case ParseNodeKind::Debugger:
         MOZ_ASSERT(node->isArity(PN_NULLARY));
         *result = false;
         return true;
 
       // Statements containing only an expression have no declarations.
-      case ParseNodeKind::Semi:
+      case ParseNodeKind::ExpressionStatement:
       case ParseNodeKind::Throw:
       case ParseNodeKind::Return:
         MOZ_ASSERT(node->isArity(PN_UNARY));
         *result = false;
         return true;
 
       // These two aren't statements in the spec, but we sometimes insert them
       // in statement lists anyway.
@@ -1635,16 +1636,17 @@ Fold(JSContext* cx, ParseNode** pnp, Par
 {
     if (!CheckRecursionLimit(cx))
         return false;
 
     ParseNode* pn = *pnp;
 
     switch (pn->getKind()) {
       case ParseNodeKind::Nop:
+      case ParseNodeKind::EmptyStatement:
       case ParseNodeKind::RegExp:
       case ParseNodeKind::String:
       case ParseNodeKind::True:
       case ParseNodeKind::False:
       case ParseNodeKind::Null:
       case ParseNodeKind::RawUndefined:
       case ParseNodeKind::Elision:
       case ParseNodeKind::Number:
@@ -1699,31 +1701,31 @@ Fold(JSContext* cx, ParseNode** pnp, Par
         return FoldUnaryArithmetic(cx, pn, parser, inGenexpLambda);
 
       case ParseNodeKind::PreIncrement:
       case ParseNodeKind::PostIncrement:
       case ParseNodeKind::PreDecrement:
       case ParseNodeKind::PostDecrement:
         return FoldIncrementDecrement(cx, pn, parser, inGenexpLambda);
 
+      case ParseNodeKind::ExpressionStatement:
       case ParseNodeKind::Throw:
       case ParseNodeKind::ArrayPush:
       case ParseNodeKind::MutateProto:
       case ParseNodeKind::ComputedName:
       case ParseNodeKind::Spread:
       case ParseNodeKind::Export:
       case ParseNodeKind::Void:
         MOZ_ASSERT(pn->isArity(PN_UNARY));
         return Fold(cx, &pn->pn_kid, parser, inGenexpLambda);
 
       case ParseNodeKind::ExportDefault:
         MOZ_ASSERT(pn->isArity(PN_BINARY));
         return Fold(cx, &pn->pn_left, parser, inGenexpLambda);
 
-      case ParseNodeKind::Semi:
       case ParseNodeKind::This:
         MOZ_ASSERT(pn->isArity(PN_UNARY));
         if (ParseNode*& expr = pn->pn_kid)
             return Fold(cx, &expr, parser, inGenexpLambda);
         return true;
 
       case ParseNodeKind::Pipeline:
         return true;
diff --git a/js/src/frontend/FullParseHandler.h b/js/src/frontend/FullParseHandler.h
--- a/js/src/frontend/FullParseHandler.h
+++ b/js/src/frontend/FullParseHandler.h
@@ -504,17 +504,17 @@ class FullParseHandler
 
     ParseNode* newSetThis(ParseNode* thisName, ParseNode* val) {
         MOZ_ASSERT(thisName->getOp() == JSOP_GETNAME);
         thisName->setOp(JSOP_SETNAME);
         return newBinary(ParseNodeKind::SetThis, thisName, val);
     }
 
     ParseNode* newEmptyStatement(const TokenPos& pos) {
-        return new_<UnaryNode>(ParseNodeKind::Semi, pos, nullptr);
+        return new_<NullaryNode>(ParseNodeKind::EmptyStatement, pos);
     }
 
     ParseNode* newImportDeclaration(ParseNode* importSpecSet,
                                     ParseNode* moduleSpec, const TokenPos& pos)
     {
         ParseNode* pn = new_<BinaryNode>(ParseNodeKind::Import, JSOP_NOP, pos,
                                          importSpecSet, moduleSpec);
         if (!pn)
@@ -550,17 +550,18 @@ class FullParseHandler
     }
 
     ParseNode* newExportBatchSpec(const TokenPos& pos) {
         return new_<NullaryNode>(ParseNodeKind::ExportBatchSpec, JSOP_NOP, pos);
     }
 
     ParseNode* newExprStatement(ParseNode* expr, uint32_t end) {
         MOZ_ASSERT(expr->pn_pos.end <= end);
-        return new_<UnaryNode>(ParseNodeKind::Semi, TokenPos(expr->pn_pos.begin, end), expr);
+        return new_<UnaryNode>(ParseNodeKind::ExpressionStatement,
+                               TokenPos(expr->pn_pos.begin, end), expr);
     }
 
     ParseNode* newIfStatement(uint32_t begin, ParseNode* cond, ParseNode* thenBranch,
                               ParseNode* elseBranch)
     {
         ParseNode* pn = new_<TernaryNode>(ParseNodeKind::If, cond, thenBranch, elseBranch);
         if (!pn)
             return null();
@@ -796,17 +797,17 @@ class FullParseHandler
     }
 
     bool isStatementPermittedAfterReturnStatement(ParseNode *node) {
         ParseNodeKind kind = node->getKind();
         return kind == ParseNodeKind::Function ||
                kind == ParseNodeKind::Var ||
                kind == ParseNodeKind::Break ||
                kind == ParseNodeKind::Throw ||
-               (kind == ParseNodeKind::Semi && !node->pn_kid);
+               kind == ParseNodeKind::EmptyStatement;
     }
 
     bool isSuperBase(ParseNode* node) {
         return node->isKind(ParseNodeKind::SuperBase);
     }
 
     inline MOZ_MUST_USE bool finishInitializerAssignment(ParseNode* pn, ParseNode* init);
 
diff --git a/js/src/frontend/NameFunctions.cpp b/js/src/frontend/NameFunctions.cpp
--- a/js/src/frontend/NameFunctions.cpp
+++ b/js/src/frontend/NameFunctions.cpp
@@ -384,16 +384,17 @@ class NameResolver
         auto initialParents = nparents;
         parents[initialParents] = cur;
         nparents++;
 
         switch (cur->getKind()) {
           // Nodes with no children that might require name resolution need no
           // further work.
           case ParseNodeKind::Nop:
+          case ParseNodeKind::EmptyStatement:
           case ParseNodeKind::String:
           case ParseNodeKind::TemplateString:
           case ParseNodeKind::RegExp:
           case ParseNodeKind::True:
           case ParseNodeKind::False:
           case ParseNodeKind::Null:
           case ParseNodeKind::RawUndefined:
           case ParseNodeKind::Elision:
@@ -417,16 +418,17 @@ class NameResolver
 
           case ParseNodeKind::NewTarget:
             MOZ_ASSERT(cur->isArity(PN_BINARY));
             MOZ_ASSERT(cur->pn_left->isKind(ParseNodeKind::PosHolder));
             MOZ_ASSERT(cur->pn_right->isKind(ParseNodeKind::PosHolder));
             break;
 
           // Nodes with a single non-null child requiring name resolution.
+          case ParseNodeKind::ExpressionStatement:
           case ParseNodeKind::TypeOfExpr:
           case ParseNodeKind::Void:
           case ParseNodeKind::Not:
           case ParseNodeKind::BitNot:
           case ParseNodeKind::Throw:
           case ParseNodeKind::DeleteName:
           case ParseNodeKind::DeleteProp:
           case ParseNodeKind::DeleteElem:
@@ -443,17 +445,16 @@ class NameResolver
           case ParseNodeKind::MutateProto:
           case ParseNodeKind::Export:
             MOZ_ASSERT(cur->isArity(PN_UNARY));
             if (!resolve(cur->pn_kid, prefix))
                 return false;
             break;
 
           // Nodes with a single nullable child.
-          case ParseNodeKind::Semi:
           case ParseNodeKind::This:
             MOZ_ASSERT(cur->isArity(PN_UNARY));
             if (ParseNode* expr = cur->pn_kid) {
                 if (!resolve(expr, prefix))
                     return false;
             }
             break;
 
diff --git a/js/src/frontend/ParseNode.cpp b/js/src/frontend/ParseNode.cpp
--- a/js/src/frontend/ParseNode.cpp
+++ b/js/src/frontend/ParseNode.cpp
@@ -179,16 +179,17 @@ PushUnaryNodeChild(ParseNode* node, Node
  */
 static PushResult
 PushNodeChildren(ParseNode* pn, NodeStack* stack)
 {
     switch (pn->getKind()) {
       // Trivial nodes that refer to no nodes, are referred to by nothing
       // but their parents, are never used, and are never a definition.
       case ParseNodeKind::Nop:
+      case ParseNodeKind::EmptyStatement:
       case ParseNodeKind::String:
       case ParseNodeKind::TemplateString:
       case ParseNodeKind::RegExp:
       case ParseNodeKind::True:
       case ParseNodeKind::False:
       case ParseNodeKind::Null:
       case ParseNodeKind::RawUndefined:
       case ParseNodeKind::Elision:
@@ -199,16 +200,17 @@ PushNodeChildren(ParseNode* pn, NodeStac
       case ParseNodeKind::Debugger:
       case ParseNodeKind::ExportBatchSpec:
       case ParseNodeKind::ObjectPropertyName:
       case ParseNodeKind::PosHolder:
         MOZ_ASSERT(pn->isArity(PN_NULLARY));
         return PushResult::Recyclable;
 
       // Nodes with a single non-null child.
+      case ParseNodeKind::ExpressionStatement:
       case ParseNodeKind::TypeOfName:
       case ParseNodeKind::TypeOfExpr:
       case ParseNodeKind::Void:
       case ParseNodeKind::Not:
       case ParseNodeKind::BitNot:
       case ParseNodeKind::Throw:
       case ParseNodeKind::DeleteName:
       case ParseNodeKind::DeleteProp:
@@ -224,18 +226,17 @@ PushNodeChildren(ParseNode* pn, NodeStac
       case ParseNodeKind::ArrayPush:
       case ParseNodeKind::Spread:
       case ParseNodeKind::MutateProto:
       case ParseNodeKind::Export:
       case ParseNodeKind::SuperBase:
         return PushUnaryNodeChild(pn, stack);
 
       // Nodes with a single nullable child.
-      case ParseNodeKind::This:
-      case ParseNodeKind::Semi: {
+      case ParseNodeKind::This: {
         MOZ_ASSERT(pn->isArity(PN_UNARY));
         if (pn->pn_kid)
             stack->push(pn->pn_kid);
         return PushResult::Recyclable;
       }
 
       // Binary nodes with two non-null children.
 
diff --git a/js/src/frontend/ParseNode.h b/js/src/frontend/ParseNode.h
--- a/js/src/frontend/ParseNode.h
+++ b/js/src/frontend/ParseNode.h
@@ -18,17 +18,18 @@ namespace frontend {
 
 class ParseContext;
 class FullParseHandler;
 class FunctionBox;
 class ObjectBox;
 
 #define FOR_EACH_PARSE_NODE_KIND(F) \
     F(Nop) \
-    F(Semi) \
+    F(EmptyStatement) \
+    F(ExpressionStatement) \
     F(Comma) \
     F(Conditional) \
     F(Colon) \
     F(Shorthand) \
     F(Pos) \
     F(Neg) \
     F(PreIncrement) \
     F(PostIncrement) \
@@ -289,20 +290,21 @@ IsTypeofKind(ParseNodeKind kind)
  *                                     pn_used: true
  *                                     pn_atom: variable name
  *                                     pn_lexdef: def node
  *                                   each assignment node has
  *                                     pn_left: Name with pn_used true and
  *                                              pn_lexdef (NOT pn_expr) set
  *                                     pn_right: initializer
  * Return   unary       pn_kid: return expr or null
- * Semi     unary       pn_kid: expr or null statement
- *                          pn_prologue: true if Directive Prologue member
- *                              in original source, not introduced via
- *                              constant folding or other tree rewriting
+ * ExpressionStatement unary    pn_kid: expr
+ *                              pn_prologue: true if Directive Prologue member
+ *                                  in original source, not introduced via
+ *                                  constant folding or other tree rewriting
+ * EmptyStatement nullary      (no fields)
  * Label    name        pn_atom: label, pn_expr: labeled statement
  * Import   binary      pn_left: ImportSpecList import specifiers
  *                          pn_right: String module specifier
  * Export   unary       pn_kid: declaration expression
  * ExportFrom binary   pn_left: ExportSpecList export specifiers
  *                          pn_right: String module specifier
  * ExportDefault unary pn_kid: export default declaration or expression
  *
@@ -673,20 +675,20 @@ class ParseNode
      * a directive prologue.
      *
      * Note that a Directive Prologue can contain statements that cannot
      * themselves be directives (string literals that include escape sequences
      * or escaped newlines, say). This member function returns true for such
      * nodes; we use it to determine the extent of the prologue.
      */
     JSAtom* isStringExprStatement() const {
-        if (getKind() == ParseNodeKind::Semi) {
+        if (getKind() == ParseNodeKind::ExpressionStatement) {
             MOZ_ASSERT(pn_arity == PN_UNARY);
             ParseNode* kid = pn_kid;
-            if (kid && kid->getKind() == ParseNodeKind::String && !kid->pn_parens)
+            if (kid->getKind() == ParseNodeKind::String && !kid->pn_parens)
                 return kid->pn_atom;
         }
         return nullptr;
     }
 
     /* True if pn is a parsenode representing a literal constant. */
     bool isLiteral() const {
         return isKind(ParseNodeKind::Number) ||
diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -581,23 +581,23 @@ static inline ParseNode*
 ComparisonRight(ParseNode* pn)
 {
     return BinaryOpRight(pn);
 }
 
 static inline bool
 IsExpressionStatement(ParseNode* pn)
 {
-    return pn->isKind(ParseNodeKind::Semi);
+    return pn->isKind(ParseNodeKind::ExpressionStatement);
 }
 
 static inline ParseNode*
 ExpressionStatementExpr(ParseNode* pn)
 {
-    MOZ_ASSERT(pn->isKind(ParseNodeKind::Semi));
+    MOZ_ASSERT(pn->isKind(ParseNodeKind::ExpressionStatement));
     return UnaryKid(pn);
 }
 
 static inline PropertyName*
 LoopControlMaybeLabel(ParseNode* pn)
 {
     MOZ_ASSERT(pn->isKind(ParseNodeKind::Break) || pn->isKind(ParseNodeKind::Continue));
     MOZ_ASSERT(pn->isArity(PN_NULLARY));
@@ -727,26 +727,25 @@ static inline bool
 IsIgnoredDirectiveName(JSContext* cx, JSAtom* atom)
 {
     return atom != cx->names().useStrict;
 }
 
 static inline bool
 IsIgnoredDirective(JSContext* cx, ParseNode* pn)
 {
-    return pn->isKind(ParseNodeKind::Semi) &&
-           UnaryKid(pn) &&
+    return pn->isKind(ParseNodeKind::ExpressionStatement) &&
            UnaryKid(pn)->isKind(ParseNodeKind::String) &&
            IsIgnoredDirectiveName(cx, UnaryKid(pn)->pn_atom);
 }
 
 static inline bool
 IsEmptyStatement(ParseNode* pn)
 {
-    return pn->isKind(ParseNodeKind::Semi) && !UnaryKid(pn);
+    return pn->isKind(ParseNodeKind::EmptyStatement);
 }
 
 static inline ParseNode*
 SkipEmptyStatements(ParseNode* pn)
 {
     while (pn && IsEmptyStatement(pn))
         pn = pn->pn_next;
     return pn;
@@ -3787,17 +3786,17 @@ ArgFail(FunctionValidator& f, PropertyNa
 
 static bool
 CheckArgumentType(FunctionValidator& f, ParseNode* stmt, PropertyName* name, Type* type)
 {
     if (!stmt || !IsExpressionStatement(stmt))
         return ArgFail(f, name, stmt ? stmt : f.fn());
 
     ParseNode* initNode = ExpressionStatementExpr(stmt);
-    if (!initNode || !initNode->isKind(ParseNodeKind::Assign))
+    if (!initNode->isKind(ParseNodeKind::Assign))
         return ArgFail(f, name, stmt);
 
     ParseNode* argNode = BinaryLeft(initNode);
     ParseNode* coercionNode = BinaryRight(initNode);
 
     if (!IsUseOfName(argNode, name))
         return ArgFail(f, name, stmt);
 
@@ -6459,21 +6458,18 @@ CheckAsExprStatement(FunctionValidator& 
     }
 
     return true;
 }
 
 static bool
 CheckExprStatement(FunctionValidator& f, ParseNode* exprStmt)
 {
-    MOZ_ASSERT(exprStmt->isKind(ParseNodeKind::Semi));
-    ParseNode* expr = UnaryKid(exprStmt);
-    if (!expr)
-        return true;
-    return CheckAsExprStatement(f, expr);
+    MOZ_ASSERT(exprStmt->isKind(ParseNodeKind::ExpressionStatement));
+    return CheckAsExprStatement(f, UnaryKid(exprStmt));
 }
 
 static bool
 CheckLoopConditionOnEntry(FunctionValidator& f, ParseNode* cond)
 {
     uint32_t maybeLit;
     if (IsLiteralInt(f.m(), cond, &maybeLit) && maybeLit)
         return true;
@@ -7048,28 +7044,29 @@ CheckBreakOrContinue(FunctionValidator& 
 
 static bool
 CheckStatement(FunctionValidator& f, ParseNode* stmt)
 {
     if (!CheckRecursionLimitDontReport(f.cx()))
         return f.m().failOverRecursed();
 
     switch (stmt->getKind()) {
-      case ParseNodeKind::Semi:          return CheckExprStatement(f, stmt);
-      case ParseNodeKind::While:         return CheckWhile(f, stmt);
-      case ParseNodeKind::For:           return CheckFor(f, stmt);
-      case ParseNodeKind::DoWhile:       return CheckDoWhile(f, stmt);
-      case ParseNodeKind::Label:         return CheckLabel(f, stmt);
-      case ParseNodeKind::If:            return CheckIf(f, stmt);
-      case ParseNodeKind::Switch:        return CheckSwitch(f, stmt);
-      case ParseNodeKind::Return:        return CheckReturn(f, stmt);
-      case ParseNodeKind::StatementList: return CheckStatementList(f, stmt);
-      case ParseNodeKind::Break:         return CheckBreakOrContinue(f, true, stmt);
-      case ParseNodeKind::Continue:      return CheckBreakOrContinue(f, false, stmt);
-      case ParseNodeKind::LexicalScope:  return CheckLexicalScope(f, stmt);
+      case ParseNodeKind::EmptyStatement:       return true;
+      case ParseNodeKind::ExpressionStatement:  return CheckExprStatement(f, stmt);
+      case ParseNodeKind::While:                return CheckWhile(f, stmt);
+      case ParseNodeKind::For:                  return CheckFor(f, stmt);
+      case ParseNodeKind::DoWhile:              return CheckDoWhile(f, stmt);
+      case ParseNodeKind::Label:                return CheckLabel(f, stmt);
+      case ParseNodeKind::If:                   return CheckIf(f, stmt);
+      case ParseNodeKind::Switch:               return CheckSwitch(f, stmt);
+      case ParseNodeKind::Return:               return CheckReturn(f, stmt);
+      case ParseNodeKind::StatementList:        return CheckStatementList(f, stmt);
+      case ParseNodeKind::Break:                return CheckBreakOrContinue(f, true, stmt);
+      case ParseNodeKind::Continue:             return CheckBreakOrContinue(f, false, stmt);
+      case ParseNodeKind::LexicalScope:         return CheckLexicalScope(f, stmt);
       default:;
     }
 
     return f.fail(stmt, "unexpected statement kind");
 }
 
 static bool
 ParseFunction(ModuleValidator& m, ParseNode** fnOut, unsigned* line)
