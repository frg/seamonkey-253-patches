# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1507375005 18000
#      Sat Oct 07 06:16:45 2017 -0500
# Node ID 6b8ce7a7fa1ef9f47c6c152d257518b10d05a603
# Parent  500e7a46f241d6b75c371f2a4a969de41d7e159a
servo: Merge #18774 -  stylo: Add a mechanism to restrict media-features to UA and chrome sheets (from emilio:ua-sheets-mq); r=xidorn

Source-Repo: https://github.com/servo/servo
Source-Revision: 72834c3482b7450d73e6fe8835766044676db4b5

diff --git a/servo/components/style/gecko/media_queries.rs b/servo/components/style/gecko/media_queries.rs
--- a/servo/components/style/gecko/media_queries.rs
+++ b/servo/components/style/gecko/media_queries.rs
@@ -28,16 +28,17 @@ use servo_arc::Arc;
 use std::cell::RefCell;
 use std::fmt::{self, Write};
 use std::sync::atomic::{AtomicBool, AtomicIsize, AtomicUsize, Ordering};
 use str::starts_with_ignore_ascii_case;
 use string_cache::Atom;
 use style_traits::{CSSPixel, DevicePixel};
 use style_traits::{ToCss, ParseError, StyleParseError};
 use style_traits::viewport::ViewportConstraints;
+use stylesheets::Origin;
 use values::{CSSFloat, CustomIdent, serialize_dimension};
 use values::computed::{self, ToComputedValue};
 use values::specified::Length;
 
 /// The `Device` in Gecko wraps a pres context, has a default values computed,
 /// and contains all the viewport rule state.
 pub struct Device {
     /// NB: The pres context lifetime is tied to the styleset, who owns the
@@ -590,18 +591,20 @@ impl Expression {
         }
     }
 
     /// Parse a media expression of the form:
     ///
     /// ```
     /// (media-feature: media-value)
     /// ```
-    pub fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
-                         -> Result<Self, ParseError<'i>> {
+    pub fn parse<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<Self, ParseError<'i>> {
         input.expect_parenthesis_block().map_err(|err|
             match err {
                 BasicParseError::UnexpectedToken(t) => StyleParseError::ExpectedIdentifier(t),
                 _ => StyleParseError::UnspecifiedError,
             }
         )?;
 
         input.parse_nested_block(|input| {
@@ -612,16 +615,22 @@ impl Expression {
                 let ident = input.expect_ident().map_err(|err|
                     match err {
                         BasicParseError::UnexpectedToken(t) => StyleParseError::ExpectedIdentifier(t),
                         _ => StyleParseError::UnspecifiedError,
                     }
                 )?;
 
                 let mut flags = 0;
+
+                if context.in_chrome_stylesheet() ||
+                    context.stylesheet_origin == Origin::UserAgent {
+                    flags |= nsMediaFeature_RequirementFlags::eUserAgentAndChromeOnly as u8;
+                }
+
                 let result = {
                     let mut feature_name = &**ident;
 
                     if unsafe { structs::StylePrefs_sWebkitPrefixedAliasesEnabled } &&
                        starts_with_ignore_ascii_case(feature_name, "-webkit-") {
                         feature_name = &feature_name[8..];
                         flags |= nsMediaFeature_RequirementFlags::eHasWebkitPrefix as u8;
                         if unsafe { structs::StylePrefs_sWebkitDevicePixelRatioEnabled } {
diff --git a/servo/components/style/parser.rs b/servo/components/style/parser.rs
--- a/servo/components/style/parser.rs
+++ b/servo/components/style/parser.rs
@@ -125,16 +125,21 @@ impl<'a> ParserContext<'a> {
         where R: ParseErrorReporter
     {
         let location = SourceLocation {
             line: location.line,
             column: location.column,
         };
         context.error_reporter.report_error(self.url_data, location, error)
     }
+
+    /// Returns whether this is a chrome stylesheets.
+    pub fn in_chrome_stylesheet(&self) -> bool {
+        self.url_data.is_chrome()
+    }
 }
 
 // XXXManishearth Replace all specified value parse impls with impls of this
 // trait. This will make it easy to write more generic values in the future.
 /// A trait to abstract parsing of a specified value given a `ParserContext` and
 /// CSS input.
 pub trait Parse : Sized {
     /// Parse a value of this type.
