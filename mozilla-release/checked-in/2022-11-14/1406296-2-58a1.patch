# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1507271662 -39600
#      Fri Oct 06 17:34:22 2017 +1100
# Node ID 6cccd545109faa8e70703d9fc68de03976d5f142
# Parent  1d53019fbe9648f25ff50c8ece77bc27f79f8a77
Bug 1406296 (part 2) - Remove the profiler's "layersdump" feature. r=mstange.

Because it just doesn't control any behaviour within the profiler, and it just
duplicates gfxPrefs::LayersDumpTexture().

With this gone, PROFILER_FEATURE_ACTIVE can also be removed.

diff --git a/browser/components/extensions/schemas/geckoProfiler.json b/browser/components/extensions/schemas/geckoProfiler.json
--- a/browser/components/extensions/schemas/geckoProfiler.json
+++ b/browser/components/extensions/schemas/geckoProfiler.json
@@ -21,17 +21,16 @@
     "types": [
       {
         "id": "ProfilerFeature",
         "type": "string",
         "enum": [
           "gpu",
           "java",
           "js",
-          "layersdump",
           "leaf",
           "mainthreadio",
           "memory",
           "privacy",
           "restyle",
           "stackwalk",
           "tasktracer",
           "threads"
diff --git a/gfx/layers/client/TextureClient.cpp b/gfx/layers/client/TextureClient.cpp
--- a/gfx/layers/client/TextureClient.cpp
+++ b/gfx/layers/client/TextureClient.cpp
@@ -1394,18 +1394,17 @@ TextureClient::PrintInfo(std::stringstre
 {
   aStream << aPrefix;
   aStream << nsPrintfCString("TextureClient (0x%p)", this).get();
   AppendToString(aStream, GetSize(), " [size=", "]");
   AppendToString(aStream, GetFormat(), " [format=", "]");
   AppendToString(aStream, mFlags, " [flags=", "]");
 
 #ifdef MOZ_DUMP_PAINTING
-  if (gfxPrefs::LayersDumpTexture() ||
-      PROFILER_FEATURE_ACTIVE(ProfilerFeature::LayersDump)) {
+  if (gfxPrefs::LayersDumpTexture()) {
     nsAutoCString pfx(aPrefix);
     pfx += "  ";
 
     aStream << "\n" << pfx.get() << "Surface: ";
     RefPtr<gfx::DataSourceSurface> dSurf = GetAsSurface();
     if (dSurf) {
       aStream << gfxUtils::GetAsLZ4Base64Str(dSurf).get();
     }
diff --git a/gfx/layers/composite/LayerManagerComposite.cpp b/gfx/layers/composite/LayerManagerComposite.cpp
--- a/gfx/layers/composite/LayerManagerComposite.cpp
+++ b/gfx/layers/composite/LayerManagerComposite.cpp
@@ -861,20 +861,16 @@ LayerManagerComposite::Render(const nsIn
   bool haveLayerEffects = (invertVal || grayscaleVal || contrastVal != 0.0);
 
   // Set LayerScope begin/end frame
   LayerScopeAutoFrame frame(PR_Now());
 
   // Dump to console
   if (gfxPrefs::LayersDump()) {
     this->Dump(/* aSorted= */true);
-  } else if (PROFILER_FEATURE_ACTIVE(ProfilerFeature::LayersDump)) {
-    std::stringstream ss;
-    Dump(ss);
-    PROFILER_TRACING("log", ss.str().c_str(), TRACING_EVENT);
   }
 
   // Dump to LayerScope Viewer
   if (LayerScope::CheckSendable()) {
     // Create a LayersPacket, dump Layers into it and transfer the
     // packet('s ownership) to LayerScope.
     auto packet = MakeUnique<layerscope::Packet>();
     layerscope::LayersPacket* layersPacket = packet->mutable_layers();
diff --git a/gfx/layers/composite/TextureHost.cpp b/gfx/layers/composite/TextureHost.cpp
--- a/gfx/layers/composite/TextureHost.cpp
+++ b/gfx/layers/composite/TextureHost.cpp
@@ -449,18 +449,17 @@ TextureHost::PrintInfo(std::stringstream
   //       GetSize() and GetFormat() on it.
   if (Lock()) {
     AppendToString(aStream, GetSize(), " [size=", "]");
     AppendToString(aStream, GetFormat(), " [format=", "]");
     Unlock();
   }
   AppendToString(aStream, mFlags, " [flags=", "]");
 #ifdef MOZ_DUMP_PAINTING
-  if (gfxPrefs::LayersDumpTexture() ||
-      PROFILER_FEATURE_ACTIVE(ProfilerFeature::LayersDump)) {
+  if (gfxPrefs::LayersDumpTexture()) {
     nsAutoCString pfx(aPrefix);
     pfx += "  ";
 
     aStream << "\n" << pfx.get() << "Surface: ";
     RefPtr<gfx::DataSourceSurface> dSurf = GetAsSurface();
     if (dSurf) {
       aStream << gfxUtils::GetAsLZ4Base64Str(dSurf).get();
     }
diff --git a/gfx/layers/composite/TiledContentHost.cpp b/gfx/layers/composite/TiledContentHost.cpp
--- a/gfx/layers/composite/TiledContentHost.cpp
+++ b/gfx/layers/composite/TiledContentHost.cpp
@@ -622,18 +622,17 @@ TiledContentHost::RenderLayerBuffer(Tile
 
 void
 TiledContentHost::PrintInfo(std::stringstream& aStream, const char* aPrefix)
 {
   aStream << aPrefix;
   aStream << nsPrintfCString("TiledContentHost (0x%p)", this).get();
 
 #if defined(MOZ_DUMP_PAINTING)
-  if (gfxPrefs::LayersDumpTexture() ||
-      PROFILER_FEATURE_ACTIVE(ProfilerFeature::LayersDump)) {
+  if (gfxPrefs::LayersDumpTexture()) {
     nsAutoCString pfx(aPrefix);
     pfx += "  ";
 
     Dump(aStream, pfx.get(), false);
   }
 #endif
 }
 
diff --git a/tools/profiler/public/GeckoProfiler.h b/tools/profiler/public/GeckoProfiler.h
--- a/tools/profiler/public/GeckoProfiler.h
+++ b/tools/profiler/public/GeckoProfiler.h
@@ -32,18 +32,16 @@
 #define AUTO_PROFILER_THREAD_SLEEP
 #define AUTO_PROFILER_THREAD_WAKE
 
 #define PROFILER_JS_INTERRUPT_CALLBACK()
 
 #define PROFILER_SET_JS_CONTEXT(cx)
 #define PROFILER_CLEAR_JS_CONTEXT()
 
-#define PROFILER_FEATURE_ACTIVE(feature) false
-
 #define AUTO_PROFILER_LABEL(label, category)
 #define AUTO_PROFILER_LABEL_DYNAMIC(label, category, dynamicStr)
 
 #define PROFILER_ADD_MARKER(markerName)
 
 #define PROFILER_TRACING(category, markerName, kind)
 #define AUTO_PROFILER_TRACING(category, markerName)
 
@@ -101,43 +99,40 @@ class TimeStamp;
   macro(0, "gpu", GPU) \
   \
   /* Profile Java code (Android only). */ \
   macro(1, "java", Java) \
   \
   /* Get the JS engine to emit pseudostack entries in prologues/epilogues */ \
   macro(2, "js", JS) \
   \
-  /* Dump the layer tree with the textures. */ \
-  macro(3, "layersdump", LayersDump) \
-  \
   /* Include the C++ leaf node if not stackwalking. */ \
   /* The DevTools profiler doesn't want the native addresses. */ \
-  macro(4, "leaf", Leaf) \
+  macro(3, "leaf", Leaf) \
   \
   /* Add main thread I/O to the profile. */ \
-  macro(5, "mainthreadio", MainThreadIO) \
+  macro(4, "mainthreadio", MainThreadIO) \
   \
   /* Add memory measurements (e.g. RSS). */ \
-  macro(6, "memory", Memory) \
+  macro(5, "memory", Memory) \
   \
   /* Do not include user-identifiable information. */ \
-  macro(7, "privacy", Privacy) \
+  macro(6, "privacy", Privacy) \
   \
   /* Restyle profiling. */ \
-  macro(8, "restyle", Restyle) \
+  macro(7, "restyle", Restyle) \
   \
   /* Walk the C++ stack. Not available on all platforms. */ \
-  macro(9, "stackwalk", StackWalk) \
+  macro(8, "stackwalk", StackWalk) \
   \
   /* Start profiling with feature TaskTracer. */ \
-  macro(10, "tasktracer", TaskTracer) \
+  macro(9, "tasktracer", TaskTracer) \
   \
   /* Profile the registered secondary threads. */ \
-  macro(11, "threads", Threads)
+  macro(10, "threads", Threads)
 
 struct ProfilerFeature
 {
   #define DECLARE(n_, str_, Name_) \
     static const uint32_t Name_ = (1u << n_); \
     static bool Has##Name_(uint32_t aFeatures) { return aFeatures & Name_; } \
     static void Set##Name_(uint32_t& aFeatures) { aFeatures |= Name_; } \
     static void Clear##Name_(uint32_t& aFeatures) { aFeatures &= ~Name_; }
@@ -281,17 +276,16 @@ bool profiler_thread_is_sleeping();
 // profiler_start(). The result is the same whether the profiler is active or
 // not.
 uint32_t profiler_get_available_features();
 
 // Check if a profiler feature (specified via the ProfilerFeature type) is
 // active. Returns false if the profiler is inactive. Note: the return value
 // can become immediately out-of-date, much like the return value of
 // profiler_is_active().
-#define PROFILER_FEATURE_ACTIVE(feature) profiler_feature_active(feature)
 bool profiler_feature_active(uint32_t aFeature);
 
 // Get the params used to start the profiler. Returns 0 and an empty vector
 // (via outparams) if the profile is inactive. It's possible that the features
 // returned may be slightly different to those requested due to required
 // adjustments.
 void profiler_get_start_params(int* aEntrySize, double* aInterval,
                                uint32_t* aFeatures,
