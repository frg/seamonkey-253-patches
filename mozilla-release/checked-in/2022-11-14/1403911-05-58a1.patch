# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1507039867 -7200
#      Tue Oct 03 16:11:07 2017 +0200
# Node ID 3b00d68c8ef9d7eeb723289518a3e0380efad07a
# Parent  694b0eb2bf8775ee1d2cd52c9be5ae9b163c63b6
Bug 1403911 - Part 5: Don't overwrite call arguments when converting arguments in string methods. r=jandem

diff --git a/js/src/jsstr.cpp b/js/src/jsstr.cpp
--- a/js/src/jsstr.cpp
+++ b/js/src/jsstr.cpp
@@ -70,26 +70,25 @@ using mozilla::IsSame;
 using mozilla::Move;
 using mozilla::PodCopy;
 using mozilla::PodEqual;
 using mozilla::RangedPtr;
 
 using JS::AutoCheckCannotGC;
 
 static JSLinearString*
-ArgToRootedString(JSContext* cx, const CallArgs& args, unsigned argno)
+ArgToLinearString(JSContext* cx, const CallArgs& args, unsigned argno)
 {
     if (argno >= args.length())
         return cx->names().undefined;
 
     JSString* str = ToString<CanGC>(cx, args[argno]);
     if (!str)
         return nullptr;
 
-    args[argno].setString(str);
     return str->ensureLinear(cx);
 }
 
 template <typename CharT> struct MaximumInlineLength;
 
 template<> struct MaximumInlineLength<Latin1Char> {
     static constexpr size_t value = JSFatInlineString::MAX_LENGTH_LATIN1;
 };
@@ -319,17 +318,17 @@ Escape(JSContext* cx, const CharT* chars
     return true;
 }
 
 static bool
 str_escape(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
-    JSLinearString* str = ArgToRootedString(cx, args, 0);
+    RootedLinearString str(cx, ArgToLinearString(cx, args, 0));
     if (!str)
         return false;
 
     InlineCharBuffer<Latin1Char> newChars;
     uint32_t newLength = 0;  // initialize to silence GCC warning
     if (str->hasLatin1Chars()) {
         AutoCheckCannotGC nogc;
         if (!Escape(cx, str->latin1Chars(nogc), str->length(), newChars, &newLength))
@@ -449,17 +448,17 @@ Unescape(StringBuffer& sb, const mozilla
 // ES2018 draft rev f83aa38282c2a60c6916ebc410bfdf105a0f6a54
 // B.2.1.2 unescape ( string )
 static bool
 str_unescape(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
     // Step 1.
-    RootedLinearString str(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString str(cx, ArgToLinearString(cx, args, 0));
     if (!str)
         return false;
 
     // Step 3.
     StringBuffer sb(cx);
     if (str->hasTwoByteChars() && !sb.ensureTwoByteChars())
         return false;
 
@@ -1439,17 +1438,17 @@ js::str_normalize(JSContext* cx, unsigne
     };
 
     NormalizationForm form;
     if (!args.hasDefined(0)) {
         // Step 3.
         form = NFC;
     } else {
         // Step 4.
-        JSLinearString* formStr = ArgToRootedString(cx, args, 0);
+        JSLinearString* formStr = ArgToLinearString(cx, args, 0);
         if (!formStr)
             return false;
 
         // Step 5.
         if (EqualStrings(formStr, cx->names().NFC)) {
             form = NFC;
         } else if (EqualStrings(formStr, cx->names().NFD)) {
             form = NFD;
@@ -2089,17 +2088,17 @@ js::str_includes(JSContext* cx, unsigned
     if (!str)
         return false;
 
     // Steps 3-4.
     if (!ReportErrorIfFirstArgIsRegExp(cx, args))
         return false;
 
     // Step 5.
-    RootedLinearString searchStr(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString searchStr(cx, ArgToLinearString(cx, args, 0));
     if (!searchStr)
         return false;
 
     // Step 6.
     uint32_t pos = 0;
     if (args.hasDefined(1)) {
         if (args[1].isInt32()) {
             int i = args[1].toInt32();
@@ -2134,17 +2133,17 @@ js::str_indexOf(JSContext* cx, unsigned 
     CallArgs args = CallArgsFromVp(argc, vp);
 
     // Steps 1, 2, and 3
     RootedString str(cx, ToStringForStringFunction(cx, args.thisv()));
     if (!str)
         return false;
 
     // Steps 4 and 5
-    RootedLinearString searchStr(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString searchStr(cx, ArgToLinearString(cx, args, 0));
     if (!searchStr)
         return false;
 
     // Steps 6 and 7
     uint32_t pos = 0;
     if (args.hasDefined(1)) {
         if (args[1].isInt32()) {
             int i = args[1].toInt32();
@@ -2216,17 +2215,17 @@ js::str_lastIndexOf(JSContext* cx, unsig
     CallArgs args = CallArgsFromVp(argc, vp);
 
     // Steps 1-2.
     RootedString str(cx, ToStringForStringFunction(cx, args.thisv()));
     if (!str)
         return false;
 
     // Step 3.
-    RootedLinearString searchStr(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString searchStr(cx, ArgToLinearString(cx, args, 0));
     if (!searchStr)
         return false;
 
     // Step 6.
     size_t len = str->length();
 
     // Step 8.
     size_t searchLen = searchStr->length();
@@ -2330,17 +2329,17 @@ js::str_startsWith(JSContext* cx, unsign
     if (!str)
         return false;
 
     // Steps 3-4.
     if (!ReportErrorIfFirstArgIsRegExp(cx, args))
         return false;
 
     // Step 5.
-    RootedLinearString searchStr(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString searchStr(cx, ArgToLinearString(cx, args, 0));
     if (!searchStr)
         return false;
 
     // Step 6.
     uint32_t pos = 0;
     if (args.hasDefined(1)) {
         if (args[1].isInt32()) {
             int i = args[1].toInt32();
@@ -2389,17 +2388,17 @@ js::str_endsWith(JSContext* cx, unsigned
     if (!str)
         return false;
 
     // Steps 3-4.
     if (!ReportErrorIfFirstArgIsRegExp(cx, args))
         return false;
 
     // Step 5.
-    RootedLinearString searchStr(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString searchStr(cx, ArgToLinearString(cx, args, 0));
     if (!searchStr)
         return false;
 
     // Step 6.
     uint32_t textLen = str->length();
 
     // Step 7.
     uint32_t pos = textLen;
@@ -4478,50 +4477,50 @@ Decode(JSContext* cx, HandleLinearString
     MOZ_ASSERT(res == Decode_Success);
     return TransferBufferToString(sb, rval);
 }
 
 static bool
 str_decodeURI(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    RootedLinearString str(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString str(cx, ArgToLinearString(cx, args, 0));
     if (!str)
         return false;
 
     return Decode(cx, str, js_isUriReservedPlusPound, args.rval());
 }
 
 static bool
 str_decodeURI_Component(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    RootedLinearString str(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString str(cx, ArgToLinearString(cx, args, 0));
     if (!str)
         return false;
 
     return Decode(cx, str, nullptr, args.rval());
 }
 
 static bool
 str_encodeURI(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    RootedLinearString str(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString str(cx, ArgToLinearString(cx, args, 0));
     if (!str)
         return false;
 
     return Encode(cx, str, js_isUriUnescaped, js_isUriReservedPlusPound, args.rval());
 }
 
 static bool
 str_encodeURI_Component(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    RootedLinearString str(cx, ArgToRootedString(cx, args, 0));
+    RootedLinearString str(cx, ArgToLinearString(cx, args, 0));
     if (!str)
         return false;
 
     return Encode(cx, str, js_isUriUnescaped, nullptr, args.rval());
 }
 
 bool
 js::EncodeURI(JSContext* cx, StringBuffer& sb, const char* chars, size_t length)
