# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1516389579 18000
#      Fri Jan 19 14:19:39 2018 -0500
# Node ID 04e629ae62cd669b173a171f66a6da188684cb8a
# Parent  d800fda1d607ad5f532f5956eb5045a22adc1a41
Bug 1431774 part 6.  de-COM the image loading content forceReload API.  r=mystor

MozReview-Commit-ID: 4n5BFQcHopU

diff --git a/dom/base/nsIImageLoadingContent.idl b/dom/base/nsIImageLoadingContent.idl
--- a/dom/base/nsIImageLoadingContent.idl
+++ b/dom/base/nsIImageLoadingContent.idl
@@ -160,24 +160,16 @@ interface nsIImageLoadingContent : imgIN
    *
    * @see imgILoader::loadImageWithChannel
    *
    * @throws NS_ERROR_NULL_POINTER if aChannel is null
    */
   [noscript] nsIStreamListener loadImageWithChannel(in nsIChannel aChannel);
 
   /**
-   * forceReload forces reloading of the image pointed to by currentURI
-   *
-   * @param aNotify [optional] request should notify, defaults to true
-   * @throws NS_ERROR_NOT_AVAILABLE if there is no current URI to reload
-   */
-  [noscript, optional_argc] void forceReload([optional] in boolean aNotify /* = true */);
-
-  /**
    * Enables/disables image state forcing. When |aForce| is PR_TRUE, we force
    * nsImageLoadingContent::ImageState() to return |aState|. Call again with |aForce|
    * as PR_FALSE to revert ImageState() to its original behaviour.
    */
   [noscript] void forceImageState(in boolean aForce, in unsigned long long aState);
 
   /**
    * The intrinsic size and width of this content. May differ from actual image
diff --git a/dom/base/nsImageLoadingContent.cpp b/dom/base/nsImageLoadingContent.cpp
--- a/dom/base/nsImageLoadingContent.cpp
+++ b/dom/base/nsImageLoadingContent.cpp
@@ -794,56 +794,38 @@ nsImageLoadingContent::LoadImageWithChan
     aChannel->GetURI(getter_AddRefs(mCurrentURI));
 
   FireEvent(NS_LITERAL_STRING("error"));
   FireEvent(NS_LITERAL_STRING("loadend"));
   return rv;
 }
 
 void
-nsImageLoadingContent::ForceReload(const mozilla::dom::Optional<bool>& aNotify,
-                                   mozilla::ErrorResult& aError)
+nsImageLoadingContent::ForceReload(bool aNotify, ErrorResult& aError)
 {
   nsCOMPtr<nsIURI> currentURI;
   GetCurrentURI(getter_AddRefs(currentURI));
   if (!currentURI) {
     aError.Throw(NS_ERROR_NOT_AVAILABLE);
     return;
   }
 
-  // defaults to true
-  bool notify = !aNotify.WasPassed() || aNotify.Value();
-
   // We keep this flag around along with the old URI even for failed requests
   // without a live request object
   ImageLoadType loadType = \
     (mCurrentRequestFlags & REQUEST_IS_IMAGESET) ? eImageLoadType_Imageset
                                                  : eImageLoadType_Normal;
-  nsresult rv = LoadImage(currentURI, true, notify, loadType, true, nullptr,
+  nsresult rv = LoadImage(currentURI, true, aNotify, loadType, true, nullptr,
                           nsIRequest::VALIDATE_ALWAYS);
   if (NS_FAILED(rv)) {
     aError.Throw(rv);
   }
 }
 
 NS_IMETHODIMP
-nsImageLoadingContent::ForceReload(bool aNotify /* = true */,
-                                   uint8_t aArgc)
-{
-  mozilla::dom::Optional<bool> notify;
-  if (aArgc >= 1) {
-    notify.Construct() = aNotify;
-  }
-
-  ErrorResult result;
-  ForceReload(notify, result);
-  return result.StealNSResult();
-}
-
-NS_IMETHODIMP
 nsImageLoadingContent::BlockOnload(imgIRequest* aRequest)
 {
   if (aRequest == mCurrentRequest) {
     NS_ASSERTION(!(mCurrentRequestFlags & REQUEST_BLOCKS_ONLOAD),
                  "Double BlockOnload!?");
     mCurrentRequestFlags |= REQUEST_BLOCKS_ONLOAD;
   } else if (aRequest == mPendingRequest) {
     NS_ASSERTION(!(mPendingRequestFlags & REQUEST_BLOCKS_ONLOAD),
diff --git a/dom/base/nsImageLoadingContent.h b/dom/base/nsImageLoadingContent.h
--- a/dom/base/nsImageLoadingContent.h
+++ b/dom/base/nsImageLoadingContent.h
@@ -70,23 +70,17 @@ public:
   }
   void AddObserver(imgINotificationObserver* aObserver);
   void RemoveObserver(imgINotificationObserver* aObserver);
   already_AddRefed<imgIRequest>
     GetRequest(int32_t aRequestType, mozilla::ErrorResult& aError);
   int32_t
     GetRequestType(imgIRequest* aRequest, mozilla::ErrorResult& aError);
   already_AddRefed<nsIURI> GetCurrentURI(mozilla::ErrorResult& aError);
-  void ForceReload(const mozilla::dom::Optional<bool>& aNotify,
-                   mozilla::ErrorResult& aError);
-
-  // XPCOM [optional] syntax helper
-  nsresult ForceReload(bool aNotify = true) {
-    return ForceReload(aNotify, 1);
-  }
+  void ForceReload(bool aNotify, mozilla::ErrorResult& aError);
 
 protected:
   enum ImageLoadType {
     // Most normal image loads
     eImageLoadType_Normal,
     // From a <img srcset> or <picture> context. Affects type given to content
     // policy.
     eImageLoadType_Imageset
diff --git a/dom/html/HTMLImageElement.cpp b/dom/html/HTMLImageElement.cpp
--- a/dom/html/HTMLImageElement.cpp
+++ b/dom/html/HTMLImageElement.cpp
@@ -486,17 +486,18 @@ HTMLImageElement::AfterMaybeChangeAttr(i
     if (InResponsiveMode()) {
       // per spec, full selection runs when this changes, even though
       // it doesn't directly affect the source selection
       QueueImageLoadTask(true);
     } else if (OwnerDoc()->ShouldLoadImages()) {
       // Bug 1076583 - We still use the older synchronous algorithm in
       // non-responsive mode. Force a new load of the image with the
       // new cross origin policy
-      ForceReload(aNotify);
+      IgnoredErrorResult error;
+      ForceReload(aNotify, error);
     }
   }
 }
 
 nsresult
 HTMLImageElement::GetEventTargetParent(EventChainPreVisitor& aVisitor)
 {
   // We handle image element with attribute ismap in its corresponding frame
diff --git a/dom/webidl/HTMLImageElement.webidl b/dom/webidl/HTMLImageElement.webidl
--- a/dom/webidl/HTMLImageElement.webidl
+++ b/dom/webidl/HTMLImageElement.webidl
@@ -105,15 +105,21 @@ interface MozImageLoadingContent {
   [ChromeOnly]
   void removeObserver(imgINotificationObserver aObserver);
   [ChromeOnly,Throws]
   imgIRequest? getRequest(long aRequestType);
   [ChromeOnly,Throws]
   long getRequestType(imgIRequest aRequest);
   [ChromeOnly,Throws]
   readonly attribute URI? currentURI;
+  /**
+   * forceReload forces reloading of the image pointed to by currentURI
+   *
+   * @param aNotify request should notify
+   * @throws NS_ERROR_NOT_AVAILABLE if there is no current URI to reload
+   */
   [ChromeOnly,Throws]
-  void forceReload(optional boolean aNotify);
+  void forceReload(optional boolean aNotify = true);
   [ChromeOnly]
   void forceImageState(boolean aForce, unsigned long long aState);
 };
 
 HTMLImageElement implements MozImageLoadingContent;
