# HG changeset patch
# User sotaro <sotaro.ikeda.g@gmail.com>
# Date 1506036069 -32400
# Node ID f4e02a5eb8f07ce46c75d141a68b69c46efe0c14
# Parent  ceabaab23ea0ca1b14ce9f9332daddc8df50d1bf
Bug 1387920 - Drop all ImageContainers' ImageClients during disabling WebRender r=nical

diff --git a/gfx/layers/ImageContainer.cpp b/gfx/layers/ImageContainer.cpp
--- a/gfx/layers/ImageContainer.cpp
+++ b/gfx/layers/ImageContainer.cpp
@@ -132,16 +132,26 @@ ImageContainer::GetImageClient()
 {
   RecursiveMutexAutoLock mon(mRecursiveMutex);
   EnsureImageClient();
   RefPtr<ImageClient> imageClient = mImageClient;
   return imageClient.forget();
 }
 
 void
+ImageContainer::DropImageClient()
+{
+  RecursiveMutexAutoLock mon(mRecursiveMutex);
+  if (mImageClient) {
+    mImageClient->ClearCachedResources();
+    mImageClient = nullptr;
+  }
+}
+
+void
 ImageContainer::EnsureImageClient()
 {
   // If we're not forcing a new ImageClient, then we can skip this if we don't have an existing
   // ImageClient, or if the existing one belongs to an IPC actor that is still open.
   if (!mIsAsync) {
     return;
   }
   if (mImageClient && mImageClient->GetForwarder()->GetLayersIPCActor()->IPCOpen()) {
diff --git a/gfx/layers/ImageContainer.h b/gfx/layers/ImageContainer.h
--- a/gfx/layers/ImageContainer.h
+++ b/gfx/layers/ImageContainer.h
@@ -612,16 +612,18 @@ public:
    */
   already_AddRefed<ImageClient> GetImageClient();
 
   /**
    * Main thread only.
    */
   static ProducerID AllocateProducerID();
 
+  void DropImageClient();
+
 private:
   typedef mozilla::RecursiveMutex RecursiveMutex;
 
   // Private destructor, to discourage deletion outside of Release():
   ~ImageContainer();
 
   void SetCurrentImageInternal(const nsTArray<NonOwningImage>& aImages);
 
diff --git a/gfx/layers/ipc/ImageBridgeChild.cpp b/gfx/layers/ipc/ImageBridgeChild.cpp
--- a/gfx/layers/ipc/ImageBridgeChild.cpp
+++ b/gfx/layers/ipc/ImageBridgeChild.cpp
@@ -716,17 +716,34 @@ MessageLoop * ImageBridgeChild::GetMessa
 {
   return sImageBridgeChildThread ? sImageBridgeChildThread->message_loop() : nullptr;
 }
 
 /* static */ void
 ImageBridgeChild::IdentifyCompositorTextureHost(const TextureFactoryIdentifier& aIdentifier)
 {
   if (RefPtr<ImageBridgeChild> child = GetSingleton()) {
-    child->IdentifyTextureHost(aIdentifier);
+    child->UpdateTextureFactoryIdentifier(aIdentifier);
+  }
+}
+
+void
+ImageBridgeChild::UpdateTextureFactoryIdentifier(const TextureFactoryIdentifier& aIdentifier)
+{
+  bool disablingWebRender = GetCompositorBackendType() == LayersBackend::LAYERS_WR &&
+                            aIdentifier.mParentBackend != LayersBackend::LAYERS_WR;
+  IdentifyTextureHost(aIdentifier);
+  if (disablingWebRender) {
+    // ImageHost is incompatible between WebRender enabled and WebRender disabled.
+    // Then drop all ImageContainers' ImageClients during disabling WebRender.
+    MutexAutoLock lock(mContainerMapLock);
+    for (auto iter = mImageContainers.Iter(); !iter.Done(); iter.Next()) {
+      ImageContainer* container = iter.Data();
+      container->DropImageClient();
+    }
   }
 }
 
 RefPtr<ImageClient>
 ImageBridgeChild::CreateImageClient(CompositableType aType,
                                     ImageContainer* aImageContainer)
 {
   if (InImageBridgeChildThread()) {
diff --git a/gfx/layers/ipc/ImageBridgeChild.h b/gfx/layers/ipc/ImageBridgeChild.h
--- a/gfx/layers/ipc/ImageBridgeChild.h
+++ b/gfx/layers/ipc/ImageBridgeChild.h
@@ -245,16 +245,18 @@ private:
   void FlushAllImagesSync(
     SynchronousTask* aTask,
     ImageClient* aClient,
     ImageContainer* aContainer);
 
   void ProxyAllocShmemNow(SynchronousTask* aTask, AllocShmemParams* aParams);
   void ProxyDeallocShmemNow(SynchronousTask* aTask, Shmem* aShmem, bool* aResult);
 
+  void UpdateTextureFactoryIdentifier(const TextureFactoryIdentifier& aIdentifier);
+
 public:
   // CompositableForwarder
 
   virtual void Connect(CompositableClient* aCompositable,
                        ImageContainer* aImageContainer) override;
 
   virtual bool UsesImageBridge() const override { return true; }
 
