# HG changeset patch
# User Henrik Skupin <mail@hskupin.info>
# Date 1504187000 -7200
# Node ID f8673bffc09a1398be1094c28f383745398c2b3b
# Parent  b77eae409e174dbcb8eb6e84db2f88a4fb4ab456
Bug 1388251 - Cancel connection attempts if process is not running. r=jgraham

If the browser process is not running it doesn't make sense to try to
connect to it for another 60s. Instead error out immediately.

MozReview-Commit-ID: 64DTZfEfzQj

diff --git a/testing/geckodriver/src/marionette.rs b/testing/geckodriver/src/marionette.rs
--- a/testing/geckodriver/src/marionette.rs
+++ b/testing/geckodriver/src/marionette.rs
@@ -423,17 +423,17 @@ impl MarionetteHandler {
         logging::init(&self.current_log_level);
 
         let port = self.settings.port.unwrap_or(try!(get_free_port()));
         if !self.settings.connect_existing {
             try!(self.start_browser(port, options));
         }
 
         let mut connection = MarionetteConnection::new(port, session_id.clone());
-        try!(connection.connect());
+        try!(connection.connect(&mut self.browser));
         self.connection = Mutex::new(Some(connection));
 
         Ok(capabilities)
     }
 
     fn start_browser(&mut self, port: u16, mut options: FirefoxOptions) -> WebDriverResult<()> {
         let binary = try!(options.binary
             .ok_or(WebDriverError::new(ErrorStatus::SessionNotCreated,
@@ -1319,23 +1319,38 @@ impl MarionetteConnection {
     pub fn new(port: u16, session_id: Option<String>) -> MarionetteConnection {
         MarionetteConnection {
             port: port,
             stream: None,
             session: MarionetteSession::new(session_id)
         }
     }
 
-    pub fn connect(&mut self) -> WebDriverResult<()> {
+    pub fn connect(&mut self, browser: &mut Option<FirefoxRunner>) -> WebDriverResult<()> {
         let timeout = 60 * 1000;  // ms
         let poll_interval = 100;  // ms
         let poll_attempts = timeout / poll_interval;
         let mut poll_attempt = 0;
 
         loop {
+            // If the process is gone, immediately abort the connection attempts
+            if let &mut Some(ref mut runner) = browser {
+                let status = runner.status();
+                if status.is_err() || status.as_ref().map(|x| *x).unwrap_or(None) != None {
+                    return Err(WebDriverError::new(
+                        ErrorStatus::UnknownError,
+                        format!("Process unexpectedly closed with status: {}", status
+                            .ok()
+                            .and_then(|x| x)
+                            .and_then(|x| x.code())
+                            .map(|x| x.to_string())
+                            .unwrap_or("{unknown}".into()))));
+                }
+            }
+
             match TcpStream::connect(&(DEFAULT_HOST, self.port)) {
                 Ok(stream) => {
                     self.stream = Some(stream);
                     break
                 },
                 Err(e) => {
                     trace!("  connection attempt {}/{}", poll_attempt, poll_attempts);
                     if poll_attempt <= poll_attempts {

