# HG changeset patch
# User Gabriele Svelto <gsvelto@mozilla.com>
# Date 1507641067 -7200
#      Tue Oct 10 15:11:07 2017 +0200
# Node ID 7d871d84a6a2949233711317587c8ba19965555b
# Parent  6b89ee5fc61806e1deb72d2e1c85a6a3d981a2e7
Bug 1402519 - Remove MOZ_CRASHREPORTER directives from gfx; r=jrmuizel

MozReview-Commit-ID: 7S8AJ18cCx0

diff --git a/gfx/gl/GfxTexturesReporter.cpp b/gfx/gl/GfxTexturesReporter.cpp
--- a/gfx/gl/GfxTexturesReporter.cpp
+++ b/gfx/gl/GfxTexturesReporter.cpp
@@ -1,23 +1,20 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
 /* vim: set ts=8 sts=4 et sw=4 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include <string>
 #include <sstream>
+#include "nsExceptionHandler.h"
 #include "GfxTexturesReporter.h"
 #include "gfxPrefs.h"
 
-#ifdef MOZ_CRASHREPORTER
-#include "nsExceptionHandler.h"
-#endif
-
 using namespace mozilla;
 using namespace mozilla::gl;
 
 NS_IMPL_ISUPPORTS(GfxTexturesReporter, nsIMemoryReporter)
 
 Atomic<size_t> GfxTexturesReporter::sAmount(0);
 Atomic<size_t> GfxTexturesReporter::sPeakAmount(0);
 Atomic<size_t> GfxTexturesReporter::sTileWasteAmount(0);
@@ -71,12 +68,10 @@ GfxTexturesReporter::UpdateAmount(Memory
         if (sAmount > sPeakAmount) {
             sPeakAmount.exchange(sAmount);
             if (gfxPrefs::GfxLoggingPeakTextureUsageEnabled()) {
                 printf_stderr("Peak texture usage: %s\n", FormatBytes(sPeakAmount).c_str());
             }
         }
     }
 
-#ifdef MOZ_CRASHREPORTER
     CrashReporter::AnnotateTexturesSize(sAmount);
-#endif
 }
diff --git a/gfx/ipc/GPUChild.cpp b/gfx/ipc/GPUChild.cpp
--- a/gfx/ipc/GPUChild.cpp
+++ b/gfx/ipc/GPUChild.cpp
@@ -138,22 +138,21 @@ GPUChild::RecvGraphicsError(const nsCStr
     lf->UpdateStringsVector(message.str());
   }
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
 GPUChild::RecvInitCrashReporter(Shmem&& aShmem, const NativeThreadId& aThreadId)
 {
-#ifdef MOZ_CRASHREPORTER
   mCrashReporter = MakeUnique<ipc::CrashReporterHost>(
     GeckoProcessType_GPU,
     aShmem,
     aThreadId);
-#endif
+
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
 GPUChild::RecvNotifyUiObservers(const nsCString& aTopic)
 {
   nsCOMPtr<nsIObserverService> obsSvc = mozilla::services::GetObserverService();
   MOZ_ASSERT(obsSvc);
@@ -246,22 +245,20 @@ GPUChild::RecvFinishMemoryReport(const u
   }
   return IPC_OK();
 }
 
 void
 GPUChild::ActorDestroy(ActorDestroyReason aWhy)
 {
   if (aWhy == AbnormalShutdown) {
-#ifdef MOZ_CRASHREPORTER
     if (mCrashReporter) {
       mCrashReporter->GenerateCrashReport(OtherPid());
       mCrashReporter = nullptr;
     }
-#endif
 
     Telemetry::Accumulate(Telemetry::SUBPROCESS_ABNORMAL_ABORT,
         nsDependentCString(XRE_ChildProcessTypeToString(GeckoProcessType_GPU)), 1);
 
     // Notify the Telemetry environment so that we can refresh and do a subsession split
     if (nsCOMPtr<nsIObserverService> obsvc = services::GetObserverService()) {
       obsvc->NotifyObservers(nullptr, "compositor:process-aborted", nullptr);
     }
diff --git a/gfx/ipc/GPUParent.cpp b/gfx/ipc/GPUParent.cpp
--- a/gfx/ipc/GPUParent.cpp
+++ b/gfx/ipc/GPUParent.cpp
@@ -93,20 +93,18 @@ GPUParent::Init(base::ProcessId aParentP
 
   nsDebugImpl::SetMultiprocessMode("GPU");
 
   // This must be sent before any IPDL message, which may hit sentinel
   // errors due to parent and content processes having different
   // versions.
   GetIPCChannel()->SendBuildID();
 
-#ifdef MOZ_CRASHREPORTER
   // Init crash reporter support.
   CrashReporterClient::InitSingleton(this);
-#endif
 
   // Ensure gfxPrefs are initialized.
   gfxPrefs::GetSingleton();
   gfxConfig::Init();
   gfxVars::Initialize();
   gfxPlatform::InitNullMetadata();
   // Ensure our Factory is initialised, mainly for gfx logging to work.
   gfxPlatform::InitMoz2DLogging();
@@ -437,16 +435,14 @@ GPUParent::ActorDestroy(ActorDestroyReas
   Factory::ShutDown();
 #if defined(XP_WIN)
   DeviceManagerDx::Shutdown();
 #endif
   LayerTreeOwnerTracker::Shutdown();
   gfxVars::Shutdown();
   gfxConfig::Shutdown();
   gfxPrefs::DestroySingleton();
-#ifdef MOZ_CRASHREPORTER
   CrashReporterClient::DestroySingleton();
-#endif
   XRE_ShutdownChildProcess();
 }
 
 } // namespace gfx
 } // namespace mozilla
diff --git a/gfx/ipc/GPUProcessManager.cpp b/gfx/ipc/GPUProcessManager.cpp
--- a/gfx/ipc/GPUProcessManager.cpp
+++ b/gfx/ipc/GPUProcessManager.cpp
@@ -34,22 +34,19 @@
 #include "VRManagerChild.h"
 #include "VRManagerParent.h"
 #include "VsyncBridgeChild.h"
 #include "VsyncIOThreadHolder.h"
 #include "VsyncSource.h"
 #include "mozilla/dom/VideoDecoderManagerChild.h"
 #include "mozilla/dom/VideoDecoderManagerParent.h"
 #include "MediaPrefs.h"
+#include "nsExceptionHandler.h"
 #include "nsPrintfCString.h"
 
-#ifdef MOZ_CRASHREPORTER
-# include "nsExceptionHandler.h"
-#endif
-
 #if defined(MOZ_WIDGET_ANDROID)
 #include "mozilla/widget/AndroidUiThread.h"
 #include "mozilla/layers/UiCompositorControllerChild.h"
 #endif // defined(MOZ_WIDGET_ANDROID)
 
 namespace mozilla {
 namespace gfx {
 
@@ -362,25 +359,23 @@ GPUProcessManager::OnProcessLaunchComple
   if (NS_FAILED(rv)) {
     DisableGPUProcess("Failed to create PVsyncBridge endpoints");
     return;
   }
 
   mVsyncBridge = VsyncBridgeChild::Create(mVsyncIOThread, mProcessToken, Move(vsyncChild));
   mGPUChild->SendInitVsyncBridge(Move(vsyncParent));
 
-#ifdef MOZ_CRASHREPORTER
   CrashReporter::AnnotateCrashReport(
     NS_LITERAL_CSTRING("GPUProcessStatus"),
     NS_LITERAL_CSTRING("Running"));
 
   CrashReporter::AnnotateCrashReport(
     NS_LITERAL_CSTRING("GPUProcessLaunchCount"),
     nsPrintfCString("%d", mNumProcessAttempts));
-#endif
 }
 
 static bool
 ShouldLimitDeviceResets(uint32_t count, int32_t deltaMilliseconds)
 {
   // We decide to limit by comparing the amount of resets that have happened
   // and time since the last reset to two prefs.
   int32_t timeLimit = gfxPrefs::DeviceResetThresholdMilliseconds();
@@ -707,21 +702,19 @@ GPUProcessManager::DestroyProcess()
   mProcessToken = 0;
   mProcess = nullptr;
   mGPUChild = nullptr;
   if (mVsyncBridge) {
     mVsyncBridge->Close();
     mVsyncBridge = nullptr;
   }
 
-#ifdef MOZ_CRASHREPORTER
   CrashReporter::AnnotateCrashReport(
     NS_LITERAL_CSTRING("GPUProcessStatus"),
     NS_LITERAL_CSTRING("Destroyed"));
-#endif
 }
 
 already_AddRefed<CompositorSession>
 GPUProcessManager::CreateTopLevelCompositor(nsBaseWidget* aWidget,
                                             LayerManager* aLayerManager,
                                             CSSToLayoutDeviceScale aScale,
                                             const CompositorOptions& aOptions,
                                             bool aUseExternalSurfaceSize,
diff --git a/gfx/src/DriverCrashGuard.cpp b/gfx/src/DriverCrashGuard.cpp
--- a/gfx/src/DriverCrashGuard.cpp
+++ b/gfx/src/DriverCrashGuard.cpp
@@ -4,19 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #include "DriverCrashGuard.h"
 #include "gfxEnv.h"
 #include "gfxPrefs.h"
 #include "gfxConfig.h"
 #include "nsAppDirectoryServiceDefs.h"
 #include "nsDirectoryServiceUtils.h"
-#ifdef MOZ_CRASHREPORTER
 #include "nsExceptionHandler.h"
-#endif
 #include "nsServiceManagerUtils.h"
 #include "nsString.h"
 #include "nsXULAppAPI.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/Telemetry.h"
 #include "mozilla/Services.h"
 #include "mozilla/gfx/Logging.h"
 #include "mozilla/dom/ContentChild.h"
@@ -156,21 +154,19 @@ DriverCrashGuard::~DriverCrashGuard()
     // proceed to mark the status as okay.
     if (GetStatus() != DriverInitStatus::Crashed) {
       SetStatus(DriverInitStatus::Okay);
     }
   } else {
     dom::ContentChild::GetSingleton()->SendEndDriverCrashGuard(uint32_t(mType));
   }
 
-#ifdef MOZ_CRASHREPORTER
   // Remove the crash report annotation.
   CrashReporter::AnnotateCrashReport(NS_LITERAL_CSTRING("GraphicsStartupTest"),
                                      NS_LITERAL_CSTRING(""));
-#endif
 }
 
 bool
 DriverCrashGuard::Crashed()
 {
   InitializeIfNeeded();
 
   // Note, we read mCrashDetected instead of GetStatus(), since in child
@@ -199,25 +195,23 @@ DriverCrashGuard::GetGuardFile()
   return file;
 }
 
 void
 DriverCrashGuard::ActivateGuard()
 {
   mGuardActivated = true;
 
-#ifdef MOZ_CRASHREPORTER
   // Anotate crash reports only if we're a real guard. Otherwise, we could
   // attribute a random parent process crash to a graphics problem in a child
   // process.
   if (mMode != Mode::Proxy) {
     CrashReporter::AnnotateCrashReport(NS_LITERAL_CSTRING("GraphicsStartupTest"),
                                        NS_LITERAL_CSTRING("1"));
   }
-#endif
 
   // If we're in the content process, the rest of the guarding is handled
   // in the parent.
   if (XRE_IsContentProcess()) {
     return;
   }
 
   SetStatus(DriverInitStatus::Attempting);
diff --git a/gfx/src/gfxCrashReporterUtils.cpp b/gfx/src/gfxCrashReporterUtils.cpp
--- a/gfx/src/gfxCrashReporterUtils.cpp
+++ b/gfx/src/gfxCrashReporterUtils.cpp
@@ -1,22 +1,15 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "gfxCrashReporterUtils.h"
-
-#if defined(MOZ_CRASHREPORTER)
-#define MOZ_GFXFEATUREREPORTER 1
-#endif
-
-#ifdef MOZ_GFXFEATUREREPORTER
-#include "gfxCrashReporterUtils.h"
 #include <string.h>                     // for strcmp
 #include "mozilla/Assertions.h"         // for MOZ_ASSERT_HELPER2
 #include "mozilla/Services.h"           // for GetObserverService
 #include "mozilla/StaticMutex.h"
 #include "mozilla/SystemGroup.h"        // for SystemGroup
 #include "mozilla/mozalloc.h"           // for operator new, etc
 #include "mozilla/RefPtr.h"             // for RefPtr
 #include "MainThreadUtils.h"            // for NS_IsMainThread
@@ -132,18 +125,8 @@ ScopedGfxFeatureReporter::AppNote(const 
     CrashReporter::AppendAppNotesToCrashReport(aMessage);
   } else {
     nsCOMPtr<nsIRunnable> r = new AppendAppNotesRunnable(aMessage);
     SystemGroup::Dispatch(TaskCategory::Other, r.forget());
   }
 }
   
 } // end namespace mozilla
-
-#else
-
-namespace mozilla {
-void ScopedGfxFeatureReporter::WriteAppNote(char) {}
-void ScopedGfxFeatureReporter::AppNote(const nsACString&) {}
-  
-} // namespace mozilla
-
-#endif
diff --git a/gfx/tests/gtest/TestGfxPrefs.cpp b/gfx/tests/gtest/TestGfxPrefs.cpp
--- a/gfx/tests/gtest/TestGfxPrefs.cpp
+++ b/gfx/tests/gtest/TestGfxPrefs.cpp
@@ -76,17 +76,16 @@ TEST(GfxPrefs, Set) {
   // Once float, default -1
   ASSERT_TRUE(gfxPrefs::APZMaxVelocity() == -1.0f);
   gfxPrefs::SetAPZMaxVelocity(1.75f);
   ASSERT_TRUE(gfxPrefs::APZMaxVelocity() == 1.75f);
   gfxPrefs::SetAPZMaxVelocity(-1.0f);
   ASSERT_TRUE(gfxPrefs::APZMaxVelocity() == -1.0f);
 }
 
-#ifdef MOZ_CRASHREPORTER
 // Randomly test the function we use in nsExceptionHandler.cpp here:
 extern bool SimpleNoCLibDtoA(double aValue, char* aBuffer, int aBufferLength);
 TEST(GfxPrefs, StringUtility)
 {
   char testBuffer[64];
   double testVal[] = {13.4,
                       3324243.42,
                       0.332424342,
@@ -99,9 +98,8 @@ TEST(GfxPrefs, StringUtility)
   }
 
   // We do not like negative numbers (random limitation)
   ASSERT_FALSE(SimpleNoCLibDtoA(-864.0, testBuffer, sizeof(testBuffer)));
 
   // It won't fit into 32:
   ASSERT_FALSE(SimpleNoCLibDtoA(testVal[4], testBuffer, sizeof(testBuffer)/2));
 }
-#endif
diff --git a/gfx/thebes/DeviceManagerDx.cpp b/gfx/thebes/DeviceManagerDx.cpp
--- a/gfx/thebes/DeviceManagerDx.cpp
+++ b/gfx/thebes/DeviceManagerDx.cpp
@@ -16,26 +16,23 @@
 #include "mozilla/gfx/GraphicsMessages.h"
 #include "mozilla/gfx/Logging.h"
 #include "mozilla/gfx/gfxVars.h"
 #include "mozilla/layers/CompositorBridgeChild.h"
 #include "mozilla/layers/CompositorThread.h"
 #include "mozilla/layers/DeviceAttachmentsD3D11.h"
 #include "mozilla/layers/MLGDeviceD3D11.h"
 #include "mozilla/layers/PaintThread.h"
+#include "nsExceptionHandler.h"
 #include "nsIGfxInfo.h"
+#include "nsPrintfCString.h"
 #include "nsString.h"
 #include <d3d11.h>
 #include <ddraw.h>
 
-#ifdef MOZ_CRASHREPORTER
-#include "nsExceptionHandler.h"
-#include "nsPrintfCString.h"
-#endif
-
 namespace mozilla {
 namespace gfx {
 
 using namespace mozilla::widget;
 using namespace mozilla::layers;
 
 StaticAutoPtr<DeviceManagerDx> DeviceManagerDx::sInstance;
 
@@ -767,22 +764,20 @@ DeviceManagerDx::MaybeResetAndReacquireD
   if (!HasDeviceReset(&resetReason)) {
     return false;
   }
 
   if (resetReason != DeviceResetReason::FORCED_RESET) {
     Telemetry::Accumulate(Telemetry::DEVICE_RESET_REASON, uint32_t(resetReason));
   }
 
-#ifdef MOZ_CRASHREPORTER
   nsPrintfCString reasonString("%d", int(resetReason));
   CrashReporter::AnnotateCrashReport(
     NS_LITERAL_CSTRING("DeviceResetReason"),
     reasonString);
-#endif
 
   bool createCompositorDevice = !!mCompositorDevice;
   bool createContentDevice = !!mContentDevice;
 
   ResetDevices();
 
   if (createCompositorDevice && !CreateCompositorDevices()) {
     // Just stop, don't try anything more
diff --git a/gfx/thebes/gfxPlatform.cpp b/gfx/thebes/gfxPlatform.cpp
--- a/gfx/thebes/gfxPlatform.cpp
+++ b/gfx/thebes/gfxPlatform.cpp
@@ -62,26 +62,24 @@
 #include "gfxImageSurface.h"
 #include "nsUnicodeProperties.h"
 #include "harfbuzz/hb.h"
 #include "gfxGraphiteShaper.h"
 #include "gfx2DGlue.h"
 #include "gfxGradientCache.h"
 #include "gfxUtils.h" // for NextPowerOfTwo
 
+#include "nsExceptionHandler.h"
 #include "nsUnicodeRange.h"
 #include "nsServiceManagerUtils.h"
 #include "nsTArray.h"
 #include "nsIObserverService.h"
 #include "nsIScreenManager.h"
 #include "FrameMetrics.h"
 #include "MainThreadUtils.h"
-#ifdef MOZ_CRASHREPORTER
-#include "nsExceptionHandler.h"
-#endif
 
 #include "nsWeakReference.h"
 
 #include "cairo.h"
 #include "qcms.h"
 
 #include "imgITools.h"
 
@@ -291,22 +289,19 @@ void CrashStatsLogForwarder::UpdateCrash
     logAnnotation = "|[X";
     break;
   }
 
   for (auto& it : mBuffer) {
     message << logAnnotation << Get<0>(it) << "]" << Get<1>(it) << " (t=" << Get<2>(it) << ") ";
   }
 
-#ifdef MOZ_CRASHREPORTER
   nsCString reportString(message.str().c_str());
   nsresult annotated = CrashReporter::AnnotateCrashReport(mCrashCriticalKey, reportString);
-#else
-  nsresult annotated = NS_ERROR_NOT_IMPLEMENTED;
-#endif
+
   if (annotated != NS_OK) {
     printf("Crash Annotation %s: %s",
            mCrashCriticalKey.get(), message.str().c_str());
   }
 }
 
 class LogForwarderEvent : public Runnable
 {
