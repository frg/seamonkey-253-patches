# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1508432778 -7200
#      Thu Oct 19 19:06:18 2017 +0200
# Node ID 58c1a09d1a65d3e1563dc8c479c02f1bdc67c2b0
# Parent  4a4e5c4a2bc081ddea5818d50cd4227f2bc25a82
Bug 1407936 - Cleaning up QuotaManagar data only when ServiceWorkers are correctly unregistered. r=bkelly, a=ritu

diff --git a/browser/base/content/sanitize.js b/browser/base/content/sanitize.js
--- a/browser/base/content/sanitize.js
+++ b/browser/base/content/sanitize.js
@@ -289,25 +289,38 @@ Sanitizer.prototype = {
         ChromeUtils.import("resource:///modules/offlineAppCache.jsm");
         // This doesn't wait for the cleanup to be complete.
         OfflineAppCacheHelper.clear();
 
         // LocalStorage
         Services.obs.notifyObservers(null, "extension:purge-localStorage");
 
         // ServiceWorkers
+        let promises = [];
         let serviceWorkers = serviceWorkerManager.getAllRegistrations();
         for (let i = 0; i < serviceWorkers.length; i++) {
           let sw = serviceWorkers.queryElementAt(i, Ci.nsIServiceWorkerRegistrationInfo);
-          let host = sw.principal.URI.host;
-          serviceWorkerManager.removeAndPropagate(host);
+
+          promises.push(new Promise(resolve => {
+            let unregisterCallback = {
+              unregisterSucceeded: () => { resolve(true); },
+              // We don't care about failures.
+              unregisterFailed: () => { resolve(true); },
+              QueryInterface: XPCOMUtils.generateQI(
+                [Ci.nsIServiceWorkerUnregisterCallback])
+            };
+
+            serviceWorkerManager.propagateUnregister(sw.principal, unregisterCallback, sw.scope);
+          }));
         }
 
+        await Promise.all(promises);
+
         // QuotaManager
-        let promises = [];
+        promises = [];
         await new Promise(resolve => {
           quotaManagerService.getUsage(request => {
             if (request.resultCode != Cr.NS_OK) {
               // We are probably shutting down. We don't want to propagate the
               // error, rejecting the promise.
               resolve();
               return;
             }
diff --git a/dom/interfaces/base/nsIServiceWorkerManager.idl b/dom/interfaces/base/nsIServiceWorkerManager.idl
--- a/dom/interfaces/base/nsIServiceWorkerManager.idl
+++ b/dom/interfaces/base/nsIServiceWorkerManager.idl
@@ -181,26 +181,23 @@ interface nsIServiceWorkerManager : nsIS
    * - Unregister jobs will be queued for all registrations.
    *   This eventually results in the registration being deleted from disk too.
    */
   void removeAndPropagate(in AUTF8String aHost);
 
   // Testing
   DOMString getScopeForUrl(in nsIPrincipal aPrincipal, in DOMString aPath);
 
-  // Note: This is meant to be used only by about:serviceworkers.
   // It returns an array of nsIServiceWorkerRegistrationInfos.
   nsIArray getAllRegistrations();
 
-  // Note: This is meant to be used only by about:serviceworkers.
   // It calls softUpdate() for each child process.
   [implicit_jscontext] void propagateSoftUpdate(in jsval aOriginAttributes,
                                                 in DOMString aScope);
 
-  // Note: This is meant to be used only by about:serviceworkers.
   // It calls unregister() in each child process. The callback is used to
   // inform when unregister() is completed on the current process.
   void propagateUnregister(in nsIPrincipal aPrincipal,
                            in nsIServiceWorkerUnregisterCallback aCallback,
                            in DOMString aScope);
 
   void sendNotificationClickEvent(in ACString aOriginSuffix,
                                   in ACString scope,
