# HG changeset patch
# User Ryan Hunt <rhunt@eqrion.net>
# Date 1510260261 18000
#      Thu Nov 09 15:44:21 2017 -0500
# Node ID 713c3225f14883b71e0d6febda505e56889f8234
# Parent  885688ba4f3de24995eb8a80df58ce1f7b65e04d
Don't do PaintThebes after PaintOffMainThread. (bug 1399692 part 10, r=dvander)

MozReview-Commit-ID: J0IOzqIGRtz

diff --git a/gfx/layers/client/ClientPaintedLayer.cpp b/gfx/layers/client/ClientPaintedLayer.cpp
--- a/gfx/layers/client/ClientPaintedLayer.cpp
+++ b/gfx/layers/client/ClientPaintedLayer.cpp
@@ -199,31 +199,34 @@ ClientPaintedLayer::PaintThebes(nsTArray
  *    ClientLayerManager will force a flushed paint and block the main thread
  *    if we have another transaction. Thus we have a gap between when the main
  *    thread records, the paint thread paints, and we block the main thread
  *    from trying to paint again. The underlying API however is NOT thread safe.
  *  4) We have both "sync" and "async" OMTP. Sync OMTP means we paint on the main thread
  *     but block the main thread while the paint thread paints. Async OMTP doesn't block
  *     the main thread. Sync OMTP is only meant to be used as a debugging tool.
  */
-bool
+void
 ClientPaintedLayer::PaintOffMainThread()
 {
   uint32_t flags = GetPaintFlags();
 
   PaintState state = mContentClient->BeginPaint(this, flags | ContentClient::PAINT_ASYNC);
   bool didUpdate = false;
 
   if (state.mBufferState) {
     PaintThread::Get()->PrepareBuffer(state.mBufferState);
     didUpdate = true;
   }
 
   if (!UpdatePaintRegion(state)) {
-    return false;
+    if (didUpdate) {
+      ClientManager()->SetQueuedAsyncPaints();
+    }
+    return;
   }
 
   RotatedBuffer::DrawIterator iter;
 
   // Debug Protip: Change to BorrowDrawTargetForPainting if using sync OMTP.
   while (RefPtr<CapturedPaintState> captureState =
           mContentClient->BorrowDrawTargetForRecording(state, &iter))
   {
@@ -267,32 +270,31 @@ ClientPaintedLayer::PaintOffMainThread()
 
   PaintThread::Get()->EndLayer();
   mContentClient->EndPaint(nullptr);
 
   if (didUpdate) {
     UpdateContentClient(state);
     ClientManager()->SetQueuedAsyncPaints();
   }
-  return true;
+  return;
 }
 
 void
 ClientPaintedLayer::RenderLayerWithReadback(ReadbackProcessor *aReadback)
 {
   RenderMaskLayers(this);
 
   if (!EnsureContentClient()) {
     return;
   }
 
   if (CanRecordLayer(aReadback)) {
-    if (PaintOffMainThread()) {
-      return;
-    }
+    PaintOffMainThread();
+    return;
   }
 
   nsTArray<ReadbackProcessor::Update> readbackUpdates;
   nsIntRegion readbackRegion;
   if (aReadback && UsedForReadback()) {
     aReadback->GetPaintedLayerUpdates(this, &readbackUpdates);
   }
 
diff --git a/gfx/layers/client/ClientPaintedLayer.h b/gfx/layers/client/ClientPaintedLayer.h
--- a/gfx/layers/client/ClientPaintedLayer.h
+++ b/gfx/layers/client/ClientPaintedLayer.h
@@ -116,17 +116,17 @@ protected:
   void PaintThebes(nsTArray<ReadbackProcessor::Update>* aReadbackUpdates);
   void RecordThebes();
   bool CanRecordLayer(ReadbackProcessor* aReadback);
   bool HasMaskLayers();
   bool EnsureContentClient();
   uint32_t GetPaintFlags();
   void UpdateContentClient(PaintState& aState);
   bool UpdatePaintRegion(PaintState& aState);
-  bool PaintOffMainThread();
+  void PaintOffMainThread();
 
   virtual void PrintInfo(std::stringstream& aStream, const char* aPrefix) override;
 
   void DestroyBackBuffer()
   {
     mContentClient = nullptr;
   }
 
