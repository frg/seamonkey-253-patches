# HG changeset patch
# User Jessica Jong <jjong@mozilla.com>
# Date 1506322020 14400
# Node ID 05e1f74416aa38f0a30fd818ff855abd9d1d5696
# Parent  2f35524622294b5be53067471e092cb1ce531894
Bug 1392970 - Part 2: Get CustomElementDefinition from CustomElementData when possible. r=smaug

MozReview-Commit-ID: L6DCtGXBesX

diff --git a/dom/base/CustomElementRegistry.cpp b/dom/base/CustomElementRegistry.cpp
--- a/dom/base/CustomElementRegistry.cpp
+++ b/dom/base/CustomElementRegistry.cpp
@@ -33,18 +33,30 @@ CustomElementCallback::Call()
       // this now in order to enqueue the attached callback. This is a spec
       // bug (w3c bug 27437).
       mOwnerData->mCreatedCallbackInvoked = true;
 
       // If ELEMENT is in a document and this document has a browsing context,
       // enqueue attached callback for ELEMENT.
       nsIDocument* document = mThisObject->GetComposedDoc();
       if (document && document->GetDocShell()) {
+        NodeInfo* ni = mThisObject->NodeInfo();
+        nsDependentAtomString extType(mOwnerData->mType);
+
+        // We need to do this because at this point, CustomElementDefinition is
+        // not set to CustomElementData yet, so EnqueueLifecycleCallback will
+        // fail to find the CE definition for this custom element.
+        // This will go away eventually since there is no created callback in v1.
+        CustomElementDefinition* definition =
+          nsContentUtils::LookupCustomElementDefinition(document,
+            ni->LocalName(), ni->NamespaceID(),
+            extType.IsEmpty() ? nullptr : &extType);
+
         nsContentUtils::EnqueueLifecycleCallback(
-          document, nsIDocument::eAttached, mThisObject);
+          document, nsIDocument::eAttached, mThisObject, nullptr, definition);
       }
 
       static_cast<LifecycleCreatedCallback *>(mCallback.get())->Call(mThisObject, rv);
       mOwnerData->mElementIsBeingCreated = false;
       break;
     }
     case nsIDocument::eAttached:
       static_cast<LifecycleAttachedCallback *>(mCallback.get())->Call(mThisObject, rv);
@@ -311,61 +323,45 @@ CustomElementRegistry::SetupCustomElemen
   SyncInvokeReactions(nsIDocument::eCreated, aElement, definition);
 }
 
 UniquePtr<CustomElementCallback>
 CustomElementRegistry::CreateCustomElementCallback(
   nsIDocument::ElementCallbackType aType, Element* aCustomElement,
   LifecycleCallbackArgs* aArgs, CustomElementDefinition* aDefinition)
 {
+  MOZ_ASSERT(aDefinition, "CustomElementDefinition should not be null");
+
   RefPtr<CustomElementData> elementData = aCustomElement->GetCustomElementData();
   MOZ_ASSERT(elementData, "CustomElementData should exist");
 
-  // Let DEFINITION be ELEMENT's definition
-  CustomElementDefinition* definition = aDefinition;
-  if (!definition) {
-    mozilla::dom::NodeInfo* info = aCustomElement->NodeInfo();
-
-    // Make sure we get the correct definition in case the element
-    // is a extended custom element e.g. <button is="x-button">.
-    nsCOMPtr<nsIAtom> typeAtom = elementData ?
-      elementData->mType.get() : info->NameAtom();
-
-    definition = mCustomDefinitions.GetWeak(typeAtom);
-    if (!definition || definition->mLocalName != info->NameAtom()) {
-      // Trying to enqueue a callback for an element that is not
-      // a custom element. We are done, nothing to do.
-      return nullptr;
-    }
-  }
-
   // Let CALLBACK be the callback associated with the key NAME in CALLBACKS.
   CallbackFunction* func = nullptr;
   switch (aType) {
     case nsIDocument::eCreated:
-      if (definition->mCallbacks->mCreatedCallback.WasPassed()) {
-        func = definition->mCallbacks->mCreatedCallback.Value();
+      if (aDefinition->mCallbacks->mCreatedCallback.WasPassed()) {
+        func = aDefinition->mCallbacks->mCreatedCallback.Value();
       }
       break;
 
     case nsIDocument::eAttached:
-      if (definition->mCallbacks->mAttachedCallback.WasPassed()) {
-        func = definition->mCallbacks->mAttachedCallback.Value();
+      if (aDefinition->mCallbacks->mAttachedCallback.WasPassed()) {
+        func = aDefinition->mCallbacks->mAttachedCallback.Value();
       }
       break;
 
     case nsIDocument::eDetached:
-      if (definition->mCallbacks->mDetachedCallback.WasPassed()) {
-        func = definition->mCallbacks->mDetachedCallback.Value();
+      if (aDefinition->mCallbacks->mDetachedCallback.WasPassed()) {
+        func = aDefinition->mCallbacks->mDetachedCallback.Value();
       }
       break;
 
     case nsIDocument::eAttributeChanged:
-      if (definition->mCallbacks->mAttributeChangedCallback.WasPassed()) {
-        func = definition->mCallbacks->mAttributeChangedCallback.Value();
+      if (aDefinition->mCallbacks->mAttributeChangedCallback.WasPassed()) {
+        func = aDefinition->mCallbacks->mAttributeChangedCallback.Value();
       }
       break;
   }
 
   // If there is no such callback, stop.
   if (!func) {
     return nullptr;
   }
@@ -411,31 +407,21 @@ CustomElementRegistry::SyncInvokeReactio
 }
 
 void
 CustomElementRegistry::EnqueueLifecycleCallback(nsIDocument::ElementCallbackType aType,
                                                 Element* aCustomElement,
                                                 LifecycleCallbackArgs* aArgs,
                                                 CustomElementDefinition* aDefinition)
 {
-  RefPtr<CustomElementData> elementData = aCustomElement->GetCustomElementData();
-  MOZ_ASSERT(elementData, "CustomElementData should exist");
-
-  // Let DEFINITION be ELEMENT's definition
   CustomElementDefinition* definition = aDefinition;
   if (!definition) {
-    mozilla::dom::NodeInfo* info = aCustomElement->NodeInfo();
-
-    // Make sure we get the correct definition in case the element
-    // is a extended custom element e.g. <button is="x-button">.
-    nsCOMPtr<nsIAtom> typeAtom = elementData ?
-      elementData->mType.get() : info->NameAtom();
-
-    definition = mCustomDefinitions.GetWeak(typeAtom);
-    if (!definition || definition->mLocalName != info->NameAtom()) {
+    definition = aCustomElement->GetCustomElementDefinition();
+    if (!definition ||
+        definition->mLocalName != aCustomElement->NodeInfo()->NameAtom()) {
       return;
     }
   }
 
   auto callback =
     CreateCustomElementCallback(aType, aCustomElement, aArgs, definition);
   if (!callback) {
     return;
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -10184,23 +10184,18 @@ nsContentUtils::SetupCustomElement(Eleme
   return registry->SetupCustomElement(aElement, aTypeExtension);
 }
 
 /* static */ CustomElementDefinition*
 nsContentUtils::GetElementDefinitionIfObservingAttr(Element* aCustomElement,
                                                     nsIAtom* aExtensionType,
                                                     nsIAtom* aAttrName)
 {
-  nsString extType = nsDependentAtomString(aExtensionType);
-  NodeInfo *ni = aCustomElement->NodeInfo();
-
   CustomElementDefinition* definition =
-    LookupCustomElementDefinition(aCustomElement->OwnerDoc(), ni->LocalName(),
-                                  ni->NamespaceID(),
-                                  extType.IsEmpty() ? nullptr : &extType);
+    aCustomElement->GetCustomElementDefinition();
 
   // Custom element not defined yet or attribute is not in the observed
   // attribute list.
   if (!definition || !definition->IsInObservedAttributeList(aAttrName)) {
     return nullptr;
   }
 
   return definition;
