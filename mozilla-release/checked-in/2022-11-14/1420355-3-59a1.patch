# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1511823958 -32400
#      Tue Nov 28 08:05:58 2017 +0900
# Node ID a19bce0cd06cf37d1732717ebfc0b9a5fae7fe84
# Parent  78deb725276424238d66660f70e40de723d02f65
Bug 1420355 - Remove fallbacks when DMD is initializing. r=njn

As of bug 1420353, DMD's replace_* functions can't be called before
replace_init places them in the malloc function table, which only
happens after DMD::Init has run, meaning DMD is always initialized
by the time any of its replace_* function can be called.

diff --git a/memory/replace/dmd/DMD.cpp b/memory/replace/dmd/DMD.cpp
--- a/memory/replace/dmd/DMD.cpp
+++ b/memory/replace/dmd/DMD.cpp
@@ -86,19 +86,16 @@ StatusMsg(const char* aFmt, ...)
 #ifndef DISALLOW_COPY_AND_ASSIGN
 #define DISALLOW_COPY_AND_ASSIGN(T) \
   T(const T&);                      \
   void operator=(const T&)
 #endif
 
 static malloc_table_t gMallocTable;
 
-// Whether DMD finished initializing.
-static bool gIsDMDInitialized = false;
-
 // This provides infallible allocations (they abort on OOM).  We use it for all
 // of DMD's own allocations, which fall into the following three cases.
 //
 // - Direct allocations (the easy case).
 //
 // - Indirect allocations in js::{Vector,HashSet,HashMap} -- this class serves
 //   as their AllocPolicy.
 //
@@ -1249,24 +1246,16 @@ static void Init(malloc_table_t* aMalloc
 } // namespace dmd
 } // namespace mozilla
 
 static void*
 replace_malloc(size_t aSize)
 {
   using namespace mozilla::dmd;
 
-  if (!gIsDMDInitialized) {
-    // DMD hasn't started up, either because it wasn't enabled by the user, or
-    // we're still in Init() and something has indirectly called malloc.  Do a
-    // vanilla malloc.  (In the latter case, if it fails we'll crash.  But
-    // OOM is highly unlikely so early on.)
-    return gMallocTable.malloc(aSize);
-  }
-
   Thread* t = Thread::Fetch();
   if (t->InterceptsAreBlocked()) {
     // Intercepts are blocked, which means this must be a call to malloc
     // triggered indirectly by DMD (e.g. via MozStackWalk).  Be infallible.
     return InfallibleAllocPolicy::malloc_(aSize);
   }
 
   // This must be a call to malloc from outside DMD.  Intercept it.
@@ -1275,39 +1264,31 @@ replace_malloc(size_t aSize)
   return ptr;
 }
 
 static void*
 replace_calloc(size_t aCount, size_t aSize)
 {
   using namespace mozilla::dmd;
 
-  if (!gIsDMDInitialized) {
-    return gMallocTable.calloc(aCount, aSize);
-  }
-
   Thread* t = Thread::Fetch();
   if (t->InterceptsAreBlocked()) {
     return InfallibleAllocPolicy::calloc_(aCount * aSize);
   }
 
   void* ptr = gMallocTable.calloc(aCount, aSize);
   AllocCallback(ptr, aCount * aSize, t);
   return ptr;
 }
 
 static void*
 replace_realloc(void* aOldPtr, size_t aSize)
 {
   using namespace mozilla::dmd;
 
-  if (!gIsDMDInitialized) {
-    return gMallocTable.realloc(aOldPtr, aSize);
-  }
-
   Thread* t = Thread::Fetch();
   if (t->InterceptsAreBlocked()) {
     return InfallibleAllocPolicy::realloc_(aOldPtr, aSize);
   }
 
   // If |aOldPtr| is nullptr, the call is equivalent to |malloc(aSize)|.
   if (!aOldPtr) {
     return replace_malloc(aSize);
@@ -1335,40 +1316,31 @@ replace_realloc(void* aOldPtr, size_t aS
   return ptr;
 }
 
 static void*
 replace_memalign(size_t aAlignment, size_t aSize)
 {
   using namespace mozilla::dmd;
 
-  if (!gIsDMDInitialized) {
-    return gMallocTable.memalign(aAlignment, aSize);
-  }
-
   Thread* t = Thread::Fetch();
   if (t->InterceptsAreBlocked()) {
     return InfallibleAllocPolicy::memalign_(aAlignment, aSize);
   }
 
   void* ptr = gMallocTable.memalign(aAlignment, aSize);
   AllocCallback(ptr, aSize, t);
   return ptr;
 }
 
 static void
 replace_free(void* aPtr)
 {
   using namespace mozilla::dmd;
 
-  if (!gIsDMDInitialized) {
-    gMallocTable.free(aPtr);
-    return;
-  }
-
   Thread* t = Thread::Fetch();
   if (t->InterceptsAreBlocked()) {
     return InfallibleAllocPolicy::free_(aPtr);
   }
 
   // Do the actual free after updating the table.  Otherwise, another thread
   // could call malloc and get the freed block and update the table, and then
   // our update here would remove the newly-malloc'd block.
@@ -1612,17 +1584,16 @@ Init(malloc_table_t* aMallocTable)
     // Create this even if the mode isn't Cumulative (albeit with a small
     // size), in case the mode is changed later on (as is done by SmokeDMD.cpp,
     // for example).
     gDeadBlockTable = InfallibleAllocPolicy::new_<DeadBlockTable>();
     size_t tableSize = gOptions->IsCumulativeMode() ? 8192 : 4;
     MOZ_ALWAYS_TRUE(gDeadBlockTable->init(tableSize));
   }
 
-  gIsDMDInitialized = true;
 }
 
 //---------------------------------------------------------------------------
 // Block reporting and unreporting
 //---------------------------------------------------------------------------
 
 static void
 ReportHelper(const void* aPtr, bool aReportedOnAlloc)
