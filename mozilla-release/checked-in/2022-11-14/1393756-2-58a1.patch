# HG changeset patch
# User steveck-chung <schung@mozilla.com>
# Date 1505967135 -28800
# Node ID 53670ed7793d9ecf06821403aeabbd9c41cf209a
# Parent  3a4470d9b1a65bc1da8c08fad756e66818734ce7
Bug 1393756 - Part 2: Add mochitest for credit card doorhanger saving with masterpassword. r=lchang

MozReview-Commit-ID: 1qw24KnrTcF

diff --git a/browser/extensions/formautofill/FormAutofillParent.jsm b/browser/extensions/formautofill/FormAutofillParent.jsm
--- a/browser/extensions/formautofill/FormAutofillParent.jsm
+++ b/browser/extensions/formautofill/FormAutofillParent.jsm
@@ -441,18 +441,25 @@ FormAutofillParent.prototype = {
       return;
     }
 
     if (state == "disable") {
       Services.prefs.setBoolPref("extensions.formautofill.creditCards.enabled", false);
       return;
     }
 
-    await this.profileStorage.creditCards.normalizeCCNumberFields(creditCard.record);
-    this.profileStorage.creditCards.add(creditCard.record);
+    try {
+      await this.profileStorage.creditCards.normalizeCCNumberFields(creditCard.record);
+      this.profileStorage.creditCards.add(creditCard.record);
+    } catch (e) {
+      if (e.result != Cr.NS_ERROR_ABORT) {
+        throw e;
+      }
+      log.warn("User canceled master password entry");
+    }
   },
 
   _onFormSubmit(data, target) {
     let {profile: {address, creditCard}, timeStartedFillingMS} = data;
 
     if (address) {
       this._onAddressSubmit(address, target, timeStartedFillingMS);
     }
diff --git a/browser/extensions/formautofill/test/browser/browser_creditCard_doorhanger.js b/browser/extensions/formautofill/test/browser/browser_creditCard_doorhanger.js
--- a/browser/extensions/formautofill/test/browser/browser_creditCard_doorhanger.js
+++ b/browser/extensions/formautofill/test/browser/browser_creditCard_doorhanger.js
@@ -51,17 +51,17 @@ add_task(async function test_submit_cred
 
       await promiseShown;
       await clickDoorhangerButton(MAIN_BUTTON);
       await TestUtils.topicObserved("formautofill-storage-changed");
     }
   );
 
   let creditCards = await getCreditCards();
-  is(creditCards.length, 1, "1 address in storage");
+  is(creditCards.length, 1, "1 credit card in storage");
   is(creditCards[0]["cc-name"], "User 1", "Verify the name field");
 });
 
 add_task(async function test_submit_creditCard_never_save() {
   await BrowserTestUtils.withNewTab({gBrowser, url: CREDITCARD_FORM_URL},
     async function(browser) {
       let promiseShown = BrowserTestUtils.waitForEvent(PopupNotifications.panel,
                                                        "popupshown");
@@ -83,12 +83,83 @@ add_task(async function test_submit_cred
       await promiseShown;
       await clickDoorhangerButton(MENU_BUTTON, 0);
     }
   );
 
   await sleep(1000);
   let creditCards = await getCreditCards();
   let creditCardPref = SpecialPowers.getBoolPref(ENABLED_AUTOFILL_CREDITCARDS_PREF);
-  is(creditCards.length, 1, "Still 1 address in storage");
+  is(creditCards.length, 1, "Still 1 credit card in storage");
   is(creditCardPref, false, "Credit card is disabled");
   SpecialPowers.clearUserPref(ENABLED_AUTOFILL_CREDITCARDS_PREF);
 });
+
+add_task(async function test_submit_creditCard_saved_with_mp_enabled() {
+  LoginTestUtils.masterPassword.enable();
+  // Login with the masterPassword in LoginTestUtils.
+  let masterPasswordDialogShown = waitForMasterPasswordDialog(true);
+  await BrowserTestUtils.withNewTab({gBrowser, url: CREDITCARD_FORM_URL},
+    async function(browser) {
+      let promiseShown = BrowserTestUtils.waitForEvent(PopupNotifications.panel,
+                                                       "popupshown");
+      await ContentTask.spawn(browser, null, async function() {
+        let form = content.document.getElementById("form");
+        let name = form.querySelector("#cc-name");
+        name.focus();
+        await new Promise(resolve => setTimeout(resolve, 1000));
+        name.setUserInput("User 0");
+
+        let number = form.querySelector("#cc-number");
+        number.setUserInput("1234123412341234");
+
+        // Wait 1000ms before submission to make sure the input value applied
+        await new Promise(resolve => setTimeout(resolve, 1000));
+        form.querySelector("input[type=submit]").click();
+      });
+
+      await promiseShown;
+      await clickDoorhangerButton(MAIN_BUTTON);
+      await masterPasswordDialogShown;
+      await TestUtils.topicObserved("formautofill-storage-changed");
+    }
+  );
+
+  let creditCards = await getCreditCards();
+  is(creditCards.length, 2, "2 credit cards in storage");
+  is(creditCards[1]["cc-name"], "User 0", "Verify the name field");
+  is(creditCards[1]["cc-number"], "************1234", "Verify the card number field");
+  LoginTestUtils.masterPassword.disable();
+});
+
+add_task(async function test_submit_creditCard_saved_with_mp_enabled_but_canceled() {
+  LoginTestUtils.masterPassword.enable();
+  let masterPasswordDialogShown = waitForMasterPasswordDialog();
+  await BrowserTestUtils.withNewTab({gBrowser, url: CREDITCARD_FORM_URL},
+    async function(browser) {
+      let promiseShown = BrowserTestUtils.waitForEvent(PopupNotifications.panel,
+                                                       "popupshown");
+      await ContentTask.spawn(browser, null, async function() {
+        let form = content.document.getElementById("form");
+        let name = form.querySelector("#cc-name");
+        name.focus();
+        await new Promise(resolve => setTimeout(resolve, 1000));
+        name.setUserInput("User 2");
+
+        let number = form.querySelector("#cc-number");
+        number.setUserInput("5678567856785678");
+
+        // Wait 1000ms before submission to make sure the input value applied
+        await new Promise(resolve => setTimeout(resolve, 1000));
+        form.querySelector("input[type=submit]").click();
+      });
+
+      await promiseShown;
+      await clickDoorhangerButton(MAIN_BUTTON);
+      await masterPasswordDialogShown;
+    }
+  );
+
+  await sleep(1000);
+  let creditCards = await getCreditCards();
+  is(creditCards.length, 2, "Still 2 credit cards in storage");
+  LoginTestUtils.masterPassword.disable();
+});
diff --git a/browser/extensions/formautofill/test/browser/browser_manageCreditCardsDialog.js b/browser/extensions/formautofill/test/browser/browser_manageCreditCardsDialog.js
--- a/browser/extensions/formautofill/test/browser/browser_manageCreditCardsDialog.js
+++ b/browser/extensions/formautofill/test/browser/browser_manageCreditCardsDialog.js
@@ -1,12 +1,10 @@
 "use strict";
 
-ChromeUtils.import("resource://testing-common/LoginTestUtils.jsm", this);
-
 const TEST_SELECTORS = {
   selRecords: "#credit-cards",
   btnRemove: "#remove",
   btnShowHideCreditCards: "#show-hide-credit-cards",
   btnAdd: "#add",
   btnEdit: "#edit",
 };
 
diff --git a/browser/extensions/formautofill/test/browser/head.js b/browser/extensions/formautofill/test/browser/head.js
--- a/browser/extensions/formautofill/test/browser/head.js
+++ b/browser/extensions/formautofill/test/browser/head.js
@@ -4,16 +4,18 @@
             FTU_PREF, ENABLED_AUTOFILL_ADDRESSES_PREF, AUTOFILL_CREDITCARDS_AVAILABLE_PREF, ENABLED_AUTOFILL_CREDITCARDS_PREF,
             SYNC_USERNAME_PREF, SYNC_ADDRESSES_PREF,
             sleep, expectPopupOpen, openPopupOn, expectPopupClose, closePopup, clickDoorhangerButton,
             getAddresses, saveAddress, removeAddresses, saveCreditCard,
             getDisplayedPopupItems, getDoorhangerCheckbox, waitForMasterPasswordDialog */
 
 "use strict";
 
+ChromeUtils.import("resource://testing-common/LoginTestUtils.jsm", this);
+
 const MANAGE_ADDRESSES_DIALOG_URL = "chrome://formautofill/content/manageAddresses.xhtml";
 const MANAGE_CREDIT_CARDS_DIALOG_URL = "chrome://formautofill/content/manageCreditCards.xhtml";
 const EDIT_ADDRESS_DIALOG_URL = "chrome://formautofill/content/editAddress.xhtml";
 const EDIT_CREDIT_CARD_DIALOG_URL = "chrome://formautofill/content/editCreditCard.xhtml";
 const BASE_URL = "http://mochi.test:8888/browser/browser/extensions/formautofill/test/browser/";
 const FORM_URL = "http://mochi.test:8888/browser/browser/extensions/formautofill/test/browser/autocomplete_basic.html";
 const CREDITCARD_FORM_URL =
   "http://mochi.test:8888/browser/browser/extensions/formautofill/test/browser/autocomplete_creditcard_basic.html";
@@ -222,23 +224,30 @@ async function clickDoorhangerButton(but
   }
   await popuphidden;
 }
 
 function getDoorhangerCheckbox() {
   return getNotification().checkbox;
 }
 
-// Wait for master password dialog and cancel to close it.
-function waitForMasterPasswordDialog() {
+
+// Wait for the master password dialog to popup and enter the password to log in
+// if "login" is "true" or dismiss it directly if otherwise.
+function waitForMasterPasswordDialog(login = false) {
   let dialogShown = TestUtils.topicObserved("common-dialog-loaded");
-  return dialogShown.then(function([subject]) {
+  return dialogShown.then(([subject]) => {
     let dialog = subject.Dialog;
     is(dialog.args.title, "Password Required", "Master password dialog shown");
-    dialog.ui.button1.click();
+    if (login) {
+      dialog.ui.password1Textbox.value = LoginTestUtils.masterPassword.masterPassword;
+      dialog.ui.button0.click();
+    } else {
+      dialog.ui.button1.click();
+    }
   });
 }
 
 registerCleanupFunction(async function() {
   let addresses = await getAddresses();
   if (addresses.length) {
     await removeAddresses(addresses.map(address => address.guid));
   }
