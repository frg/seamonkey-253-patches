# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1511535531 -3600
# Node ID 4e5334ede43ddd34afb64b49c1cc395f2e6f333c
# Parent  2618e9ce283ae2d4d360a581daad00d99ab66e65
Bug 1420400 - Optimize/self-host Reflect.get. r=anba

diff --git a/js/src/builtin/Reflect.cpp b/js/src/builtin/Reflect.cpp
--- a/js/src/builtin/Reflect.cpp
+++ b/js/src/builtin/Reflect.cpp
@@ -41,40 +41,16 @@ Reflect_deleteProperty(JSContext* cx, un
     // Step 4.
     ObjectOpResult result;
     if (!DeleteProperty(cx, target, key, result))
         return false;
     args.rval().setBoolean(bool(result));
     return true;
 }
 
-/* ES6 26.1.6 Reflect.get(target, propertyKey [, receiver]) */
-static bool
-Reflect_get(JSContext* cx, unsigned argc, Value* vp)
-{
-    CallArgs args = CallArgsFromVp(argc, vp);
-
-    // Step 1.
-    RootedObject obj(cx, NonNullObjectArg(cx, "`target`", "Reflect.get", args.get(0)));
-    if (!obj)
-        return false;
-
-    // Steps 2-3.
-    RootedValue propertyKey(cx, args.get(1));
-    RootedId key(cx);
-    if (!ToPropertyKey(cx, propertyKey, &key))
-        return false;
-
-    // Step 4.
-    RootedValue receiver(cx, args.length() > 2 ? args[2] : args.get(0));
-
-    // Step 5.
-    return GetProperty(cx, obj, receiver, key, args.rval());
-}
-
 /* ES6 26.1.8 Reflect.getPrototypeOf(target) */
 bool
 js::Reflect_getPrototypeOf(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
     // Step 1.
     RootedObject target(cx, NonNullObjectArg(cx, "`target`", "Reflect.getPrototypeOf",
@@ -205,17 +181,17 @@ Reflect_setPrototypeOf(JSContext* cx, un
     return true;
 }
 
 static const JSFunctionSpec methods[] = {
     JS_SELF_HOSTED_FN("apply", "Reflect_apply", 3, 0),
     JS_SELF_HOSTED_FN("construct", "Reflect_construct", 2, 0),
     JS_SELF_HOSTED_FN("defineProperty", "Reflect_defineProperty", 3, 0),
     JS_FN("deleteProperty", Reflect_deleteProperty, 2, 0),
-    JS_FN("get", Reflect_get, 2, 0),
+    JS_SELF_HOSTED_FN("get", "Reflect_get", 2, 0),
     JS_SELF_HOSTED_FN("getOwnPropertyDescriptor", "Reflect_getOwnPropertyDescriptor", 2, 0),
     JS_INLINABLE_FN("getPrototypeOf", Reflect_getPrototypeOf, 1, 0, ReflectGetPrototypeOf),
     JS_SELF_HOSTED_FN("has", "Reflect_has", 2, 0),
     JS_FN("isExtensible", Reflect_isExtensible, 1, 0),
     JS_FN("ownKeys", Reflect_ownKeys, 1, 0),
     JS_FN("preventExtensions", Reflect_preventExtensions, 1, 0),
     JS_FN("set", Reflect_set, 3, 0),
     JS_FN("setPrototypeOf", Reflect_setPrototypeOf, 2, 0),
diff --git a/js/src/builtin/Reflect.js b/js/src/builtin/Reflect.js
--- a/js/src/builtin/Reflect.js
+++ b/js/src/builtin/Reflect.js
@@ -122,15 +122,35 @@ function Reflect_getOwnPropertyDescripto
     // The other steps are identical to Object.getOwnPropertyDescriptor().
     return ObjectGetOwnPropertyDescriptor(target, propertyKey);
 }
 
 // ES2017 draft rev a785b0832b071f505a694e1946182adeab84c972
 // 26.1.8 Reflect.has ( target, propertyKey )
 function Reflect_has(target, propertyKey) {
     // Step 1.
-    if (!IsObject(target))
+    if (!IsObject(target)) {
         ThrowTypeError(JSMSG_NOT_NONNULL_OBJECT_ARG, "`target`", "Reflect.has",
                        ToSource(target));
+    }
 
     // Steps 2-3 are identical to the runtime semantics of the "in" operator.
     return propertyKey in target;
 }
+
+// ES2018 draft rev 0525bb33861c7f4e9850f8a222c89642947c4b9c
+// 26.1.5 Reflect.get ( target, propertyKey [ , receiver ] )
+function Reflect_get(target, propertyKey/*, receiver*/) {
+    // Step 1.
+    if (!IsObject(target)) {
+        ThrowTypeError(JSMSG_NOT_NONNULL_OBJECT_ARG, "`target`", "Reflect.get",
+                       ToSource(target));
+    }
+
+    // Step 3 (reordered).
+    if (arguments.length > 2) {
+        // Steps 2, 4.
+        return getPropertySuper(target, propertyKey, arguments[2]);
+    }
+
+    // Steps 2, 4.
+    return target[propertyKey];
+}
diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -9091,16 +9091,42 @@ BytecodeEmitter::emitSelfHostedHasOwn(Pa
     ParseNode* objNode = idNode->pn_next;
     if (!emitTree(objNode))
         return false;
 
     return emit1(JSOP_HASOWN);
 }
 
 bool
+BytecodeEmitter::emitSelfHostedGetPropertySuper(ParseNode* pn)
+{
+    if (pn->pn_count != 4) {
+        reportError(pn, JSMSG_MORE_ARGS_NEEDED, "getPropertySuper", "3", "");
+        return false;
+    }
+
+    ParseNode* funNode = pn->pn_head;  // The getPropertySuper node.
+
+    ParseNode* objNode = funNode->pn_next;
+    ParseNode* idNode = objNode->pn_next;
+    ParseNode* receiverNode = idNode->pn_next;
+
+    if (!emitTree(idNode))
+        return false;
+
+    if (!emitTree(receiverNode))
+        return false;
+
+    if (!emitTree(objNode))
+        return false;
+
+    return emitElemOpBase(JSOP_GETELEM_SUPER);
+}
+
+bool
 BytecodeEmitter::isRestParameter(ParseNode* pn)
 {
     if (!sc->isFunctionBox())
         return false;
 
     FunctionBox* funbox = sc->asFunctionBox();
     RootedFunction fun(cx, funbox->function());
     if (!funbox->hasRest())
@@ -9285,16 +9311,18 @@ BytecodeEmitter::emitCallOrNew(ParseNode
         if (pn2->name() == cx->names().forceInterpreter)
             return emitSelfHostedForceInterpreter(pn);
         if (pn2->name() == cx->names().allowContentIter)
             return emitSelfHostedAllowContentIter(pn);
         if (pn2->name() == cx->names().defineDataPropertyIntrinsic && pn->pn_count == 4)
             return emitSelfHostedDefineDataProperty(pn);
         if (pn2->name() == cx->names().hasOwn)
             return emitSelfHostedHasOwn(pn);
+        if (pn2->name() == cx->names().getPropertySuper)
+            return emitSelfHostedGetPropertySuper(pn);
         // Fall through
     }
 
     if (!emitCallee(pn2, pn, spread, &callop))
         return false;
 
     bool isNewOp = pn->getOp() == JSOP_NEW || pn->getOp() == JSOP_SPREADNEW ||
                    pn->getOp() == JSOP_SUPERCALL || pn->getOp() == JSOP_SPREADSUPERCALL;
diff --git a/js/src/frontend/BytecodeEmitter.h b/js/src/frontend/BytecodeEmitter.h
--- a/js/src/frontend/BytecodeEmitter.h
+++ b/js/src/frontend/BytecodeEmitter.h
@@ -816,16 +816,17 @@ struct MOZ_STACK_CLASS BytecodeEmitter
     bool isRestParameter(ParseNode* pn);
 
     MOZ_MUST_USE bool emitCallOrNew(ParseNode* pn, ValueUsage valueUsage = ValueUsage::WantValue);
     MOZ_MUST_USE bool emitSelfHostedCallFunction(ParseNode* pn);
     MOZ_MUST_USE bool emitSelfHostedResumeGenerator(ParseNode* pn);
     MOZ_MUST_USE bool emitSelfHostedForceInterpreter(ParseNode* pn);
     MOZ_MUST_USE bool emitSelfHostedAllowContentIter(ParseNode* pn);
     MOZ_MUST_USE bool emitSelfHostedDefineDataProperty(ParseNode* pn);
+    MOZ_MUST_USE bool emitSelfHostedGetPropertySuper(ParseNode* pn);
     MOZ_MUST_USE bool emitSelfHostedHasOwn(ParseNode* pn);
 
     MOZ_MUST_USE bool emitDo(ParseNode* pn);
     MOZ_MUST_USE bool emitWhile(ParseNode* pn);
 
     MOZ_MUST_USE bool emitFor(ParseNode* pn, EmitterScope* headLexicalEmitterScope = nullptr);
     MOZ_MUST_USE bool emitCStyleFor(ParseNode* pn, EmitterScope* headLexicalEmitterScope);
     MOZ_MUST_USE bool emitForIn(ParseNode* pn, EmitterScope* headLexicalEmitterScope);
diff --git a/js/src/vm/CommonPropertyNames.h b/js/src/vm/CommonPropertyNames.h
--- a/js/src/vm/CommonPropertyNames.h
+++ b/js/src/vm/CommonPropertyNames.h
@@ -160,16 +160,17 @@
     macro(GeneratorReturn, GeneratorReturn, "GeneratorReturn") \
     macro(GeneratorThrow, GeneratorThrow, "GeneratorThrow") \
     macro(get, get, "get") \
     macro(getInternals, getInternals, "getInternals") \
     macro(getOwnPropertyDescriptor, getOwnPropertyDescriptor, "getOwnPropertyDescriptor") \
     macro(getOwnPropertyNames, getOwnPropertyNames, "getOwnPropertyNames") \
     macro(getPrefix, getPrefix, "get ") \
     macro(getPropertyDescriptor, getPropertyDescriptor, "getPropertyDescriptor") \
+    macro(getPropertySuper, getPropertySuper, "getPropertySuper") \
     macro(getPrototypeOf, getPrototypeOf, "getPrototypeOf") \
     macro(global, global, "global") \
     macro(globalThis, globalThis, "globalThis") \
     macro(group, group, "group") \
     macro(Handle, Handle, "Handle") \
     macro(has, has, "has") \
     macro(hasOwn, hasOwn, "hasOwn") \
     macro(hasOwnProperty, hasOwnProperty, "hasOwnProperty") \
