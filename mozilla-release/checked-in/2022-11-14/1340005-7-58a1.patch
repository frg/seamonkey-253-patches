# HG changeset patch
# User Boris Chiou <boris.chiou@gmail.com>
# Date 1509131607 -7200
# Node ID 3b54675c1eee8416b0a8da02b4f1025ade265159
# Parent  4fba90c91929b39ed2929c78bae3c4e74e87ee57
Bug 1340005 - Part 7: Switch compositor animations to Servo backend for desktop. r=birtles,hiro,nical

We want to always use Servo animation backend on the compositor.
However, Android doesn't support Stylo now, so add a defined flag for it.

MozReview-Commit-ID: 63MnTBnq6yv

diff --git a/gfx/config/gfxVars.h b/gfx/config/gfxVars.h
--- a/gfx/config/gfxVars.h
+++ b/gfx/config/gfxVars.h
@@ -40,16 +40,25 @@ class gfxVarReceiver;
   _(WebRenderDebugFlags,        int32_t,          0)                    \
   _(ScreenDepth,                int32_t,          0)                    \
   _(GREDirectory,               nsCString,        nsCString())          \
   _(UseOMTP,                    bool,             false)                \
   _(AllowD3D11KeyedMutex,       bool,             false)                \
 
   /* Add new entries above this line. */
 
+// Define the default animation backend on the compositor. Now we don't use
+// stylo on the compositor only on Android, and this is a fixed flag. If
+// we want to update this flag, please add a new gfxVars for it.
+#if defined(ANDROID)
+  #define USE_STYLO_ON_COMPOSITOR false
+#else
+  #define USE_STYLO_ON_COMPOSITOR true
+#endif
+
 // Some graphics settings are computed on the UI process and must be
 // communicated to content and GPU processes. gfxVars helps facilitate
 // this. Its function is similar to gfxPrefs, except rather than hold
 // user preferences, it holds dynamically computed values.
 //
 // Each variable in GFX_VARS_LIST exposes the following static methods:
 //
 //    const DataType& CxxName();
diff --git a/gfx/layers/AnimationHelper.cpp b/gfx/layers/AnimationHelper.cpp
--- a/gfx/layers/AnimationHelper.cpp
+++ b/gfx/layers/AnimationHelper.cpp
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "AnimationHelper.h"
 #include "mozilla/ComputedTimingFunction.h" // for ComputedTimingFunction
 #include "mozilla/dom/AnimationEffectReadOnlyBinding.h" // for dom::FillMode
 #include "mozilla/dom/KeyframeEffectBinding.h" // for dom::IterationComposite
 #include "mozilla/dom/KeyframeEffectReadOnly.h" // for dom::KeyFrameEffectReadOnly
+#include "mozilla/gfx/gfxVars.h"        // for USE_STYLO_ON_COMPOSITOR
 #include "mozilla/layers/CompositorThread.h" // for CompositorThreadHolder
 #include "mozilla/layers/LayerAnimationUtils.h" // for TimingFunctionToComputedTimingFunction
 #include "mozilla/StyleAnimationValue.h" // for StyleAnimationValue, etc
 #include "nsDeviceContext.h"            // for AppUnitsPerCSSPixel
 #include "nsDisplayList.h"              // for nsDisplayTransform, etc
 
 namespace mozilla {
 namespace layers {
@@ -271,19 +272,17 @@ AnimationHelper::SampleAnimationForEachN
     animSegment.mFromValue = animData.mStartValues[segmentIndex];
     animSegment.mToValue = animData.mEndValues[segmentIndex];
     animSegment.mFromComposite =
       static_cast<dom::CompositeOperation>(segment->startComposite());
     animSegment.mToComposite =
       static_cast<dom::CompositeOperation>(segment->endComposite());
 
     // interpolate the property
-    bool isServo = animSegment.mFromValue.mServo ||
-                   animSegment.mToValue.mServo;
-    if (isServo) {
+    if (USE_STYLO_ON_COMPOSITOR) {
       dom::IterationCompositeOperation iterCompositeOperation =
           static_cast<dom::IterationCompositeOperation>(
             animation.iterationComposite());
 
       aAnimationValue.mServo =
         Servo_ComposeAnimationSegment(
           &animSegment,
           aAnimationValue.mServo,
@@ -473,32 +472,34 @@ CreateCSSValueList(const InfallibleTArra
     result->mValue.SetNoneValue();
   }
   return new nsCSSValueSharedList(result.forget());
 }
 
 static AnimationValue
 ToAnimationValue(const Animatable& aAnimatable)
 {
+  StyleBackendType backend = USE_STYLO_ON_COMPOSITOR
+                             ? StyleBackendType::Servo
+                             : StyleBackendType::Gecko;
   AnimationValue result;
 
   switch (aAnimatable.type()) {
     case Animatable::Tnull_t:
       break;
     case Animatable::TArrayOfTransformFunction: {
         const InfallibleTArray<TransformFunction>& transforms =
           aAnimatable.get_ArrayOfTransformFunction();
         RefPtr<nsCSSValueSharedList> list(CreateCSSValueList(transforms));
         MOZ_ASSERT(list, "Transform list should be non null");
-        result = AnimationValue::Transform(StyleBackendType::Gecko, *list);
+        result = AnimationValue::Transform(backend, *list);
       }
       break;
     case Animatable::Tfloat:
-      result = AnimationValue::Opacity(StyleBackendType::Gecko,
-                                       aAnimatable.get_float());
+      result = AnimationValue::Opacity(backend, aAnimatable.get_float());
       break;
     default:
       MOZ_ASSERT_UNREACHABLE("Unsupported type");
   }
 
   return result;
 }
 
