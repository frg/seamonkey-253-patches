# HG changeset patch
# User Eric Rahm <erahm@mozilla.com>
# Date 1517439806 28800
#      Wed Jan 31 15:03:26 2018 -0800
# Node ID 15e683c39a3afe200395eb7358e2af0df89da8e4
# Parent  58bc9f41c4d3dc27f0cad251a74145a5845e5c87
Bug 1434689 - Part 1: Add moz_temporary_class annotation. r=mystor

This adds a `moz_temporary_class` annotation that can be used to indicate
a class is intended to only be used as a temporary.

diff --git a/build/clang-plugin/CustomTypeAnnotation.cpp b/build/clang-plugin/CustomTypeAnnotation.cpp
--- a/build/clang-plugin/CustomTypeAnnotation.cpp
+++ b/build/clang-plugin/CustomTypeAnnotation.cpp
@@ -9,16 +9,18 @@ CustomTypeAnnotation StackClass =
     CustomTypeAnnotation("moz_stack_class", "stack");
 CustomTypeAnnotation GlobalClass =
     CustomTypeAnnotation("moz_global_class", "global");
 CustomTypeAnnotation NonHeapClass =
     CustomTypeAnnotation("moz_nonheap_class", "non-heap");
 CustomTypeAnnotation HeapClass = CustomTypeAnnotation("moz_heap_class", "heap");
 CustomTypeAnnotation NonTemporaryClass =
     CustomTypeAnnotation("moz_non_temporary_class", "non-temporary");
+CustomTypeAnnotation TemporaryClass =
+    CustomTypeAnnotation("moz_temporary_class", "temporary");
 
 void CustomTypeAnnotation::dumpAnnotationReason(BaseCheck &Check, QualType T,
                                                 SourceLocation Loc) {
   const char *Inherits =
       "%1 is a %0 type because it inherits from a %0 type %2";
   const char *Member = "%1 is a %0 type because member %2 is a %0 type %3";
   const char *Array = "%1 is a %0 type because it is an array of %0 type %2";
   const char *Templ =
diff --git a/build/clang-plugin/CustomTypeAnnotation.h b/build/clang-plugin/CustomTypeAnnotation.h
--- a/build/clang-plugin/CustomTypeAnnotation.h
+++ b/build/clang-plugin/CustomTypeAnnotation.h
@@ -63,10 +63,11 @@ protected:
   virtual std::string getImplicitReason(const TagDecl *D) const { return ""; }
 };
 
 extern CustomTypeAnnotation StackClass;
 extern CustomTypeAnnotation GlobalClass;
 extern CustomTypeAnnotation NonHeapClass;
 extern CustomTypeAnnotation HeapClass;
 extern CustomTypeAnnotation NonTemporaryClass;
+extern CustomTypeAnnotation TemporaryClass;
 
 #endif
diff --git a/build/clang-plugin/ScopeChecker.cpp b/build/clang-plugin/ScopeChecker.cpp
--- a/build/clang-plugin/ScopeChecker.cpp
+++ b/build/clang-plugin/ScopeChecker.cpp
@@ -118,44 +118,48 @@ void ScopeChecker::check(const MatchFind
   }
 
   // Error messages for incorrect allocations.
   const char *Stack = "variable of type %0 only valid on the stack";
   const char *Global = "variable of type %0 only valid as global";
   const char *Heap = "variable of type %0 only valid on the heap";
   const char *NonHeap = "variable of type %0 is not valid on the heap";
   const char *NonTemporary = "variable of type %0 is not valid in a temporary";
+  const char *Temporary = "variable of type %0 is only valid as a temporary";
 
   const char *StackNote =
       "value incorrectly allocated in an automatic variable";
   const char *GlobalNote = "value incorrectly allocated in a global variable";
   const char *HeapNote = "value incorrectly allocated on the heap";
   const char *TemporaryNote = "value incorrectly allocated in a temporary";
 
   // Report errors depending on the annotations on the input types.
   switch (Variety) {
   case AV_None:
     return;
 
   case AV_Global:
     StackClass.reportErrorIfPresent(*this, T, Loc, Stack, GlobalNote);
     HeapClass.reportErrorIfPresent(*this, T, Loc, Heap, GlobalNote);
+    TemporaryClass.reportErrorIfPresent(*this, T, Loc, Temporary, GlobalNote);
     break;
 
   case AV_Automatic:
     GlobalClass.reportErrorIfPresent(*this, T, Loc, Global, StackNote);
     HeapClass.reportErrorIfPresent(*this, T, Loc, Heap, StackNote);
+    TemporaryClass.reportErrorIfPresent(*this, T, Loc, Temporary, StackNote);
     break;
 
   case AV_Temporary:
     GlobalClass.reportErrorIfPresent(*this, T, Loc, Global, TemporaryNote);
     HeapClass.reportErrorIfPresent(*this, T, Loc, Heap, TemporaryNote);
     NonTemporaryClass.reportErrorIfPresent(*this, T, Loc, NonTemporary,
                                            TemporaryNote);
     break;
 
   case AV_Heap:
     GlobalClass.reportErrorIfPresent(*this, T, Loc, Global, HeapNote);
     StackClass.reportErrorIfPresent(*this, T, Loc, Stack, HeapNote);
     NonHeapClass.reportErrorIfPresent(*this, T, Loc, NonHeap, HeapNote);
+    TemporaryClass.reportErrorIfPresent(*this, T, Loc, Temporary, HeapNote);
     break;
   }
 }
diff --git a/build/clang-plugin/tests/TestTemporaryClass.cpp b/build/clang-plugin/tests/TestTemporaryClass.cpp
new file mode 100644
--- /dev/null
+++ b/build/clang-plugin/tests/TestTemporaryClass.cpp
@@ -0,0 +1,72 @@
+#define MOZ_TEMPORARY_CLASS __attribute__((annotate("moz_temporary_class")))
+#define MOZ_IMPLICIT __attribute__((annotate("moz_implicit")))
+
+#include <stddef.h>
+
+struct MOZ_TEMPORARY_CLASS Temporary {
+  int i;
+  Temporary() {}
+  MOZ_IMPLICIT Temporary(int a) {}
+  Temporary(int a, int b) {}
+  void *operator new(size_t x) throw() { return 0; }
+  void *operator new(size_t blah, char *buffer) { return buffer; }
+};
+
+template <class T>
+struct MOZ_TEMPORARY_CLASS TemplateClass {
+  T i;
+};
+
+void gobble(void *) { }
+
+void gobbleref(const Temporary&) { }
+
+template <class T>
+void gobbleanyref(const T&) { }
+
+void misuseNonTemporaryClass(int len) {
+  // All of these should error.
+  Temporary invalid; // expected-error {{variable of type 'Temporary' is only valid as a temporary}} expected-note {{value incorrectly allocated in an automatic variable}}
+  Temporary alsoInvalid[2]; // expected-error {{variable of type 'Temporary [2]' is only valid as a temporary}} expected-note {{value incorrectly allocated in an automatic variable}} expected-note {{'Temporary [2]' is a temporary type because it is an array of temporary type 'Temporary'}}
+  static Temporary invalidStatic; // expected-error {{variable of type 'Temporary' is only valid as a temporary}} expected-note {{value incorrectly allocated in a global variable}}
+  static Temporary alsoInvalidStatic[2]; // expected-error {{variable of type 'Temporary [2]' is only valid as a temporary}} expected-note {{value incorrectly allocated in a global variable}} expected-note {{'Temporary [2]' is a temporary type because it is an array of temporary type 'Temporary'}}
+
+  gobble(&invalid);
+  gobble(&invalidStatic);
+  gobble(&alsoInvalid[0]);
+
+  // All of these should be fine.
+  gobbleref(Temporary());
+  gobbleref(Temporary(10, 20));
+  gobbleref(Temporary(10));
+  gobbleref(10);
+  gobbleanyref(TemplateClass<int>());
+
+  // All of these should error.
+  gobble(new Temporary); // expected-error {{variable of type 'Temporary' is only valid as a temporary}} expected-note {{value incorrectly allocated on the heap}}
+  gobble(new Temporary[10]); // expected-error {{variable of type 'Temporary' is only valid as a temporary}} expected-note {{value incorrectly allocated on the heap}}
+  gobble(new TemplateClass<int>); // expected-error {{variable of type 'TemplateClass<int>' is only valid as a temporary}} expected-note {{value incorrectly allocated on the heap}}
+  gobble(len <= 5 ? &invalid : new Temporary); // expected-error {{variable of type 'Temporary' is only valid as a temporary}} expected-note {{value incorrectly allocated on the heap}}
+
+  // Placement new is odd, but okay.
+  char buffer[sizeof(Temporary)];
+  gobble(new (buffer) Temporary);
+}
+
+void defaultArg(const Temporary& arg = Temporary()) { // expected-error {{variable of type 'Temporary' is only valid as a temporary}} expected-note {{value incorrectly allocated in an automatic variable}}
+}
+
+// Can't be a global, this should error.
+Temporary invalidStatic; // expected-error {{variable of type 'Temporary' is only valid as a temporary}} expected-note {{value incorrectly allocated in a global variable}}
+
+struct RandomClass {
+  Temporary nonstaticMember; // This is okay if RandomClass is only used as a temporary.
+  static Temporary staticMember; // expected-error {{variable of type 'Temporary' is only valid as a temporary}} expected-note {{value incorrectly allocated in a global variable}}
+};
+
+struct BadInherit : Temporary {};
+
+void useStuffWrongly() {
+  gobbleanyref(BadInherit());
+  gobbleanyref(RandomClass());
+}
diff --git a/build/clang-plugin/tests/moz.build b/build/clang-plugin/tests/moz.build
--- a/build/clang-plugin/tests/moz.build
+++ b/build/clang-plugin/tests/moz.build
@@ -38,16 +38,17 @@ SOURCES += [
     'TestNonTemporaryClass.cpp',
     'TestNoRefcountedInsideLambdas.cpp',
     'TestOverrideBaseCall.cpp',
     'TestOverrideBaseCallAnnotation.cpp',
     'TestParamTraitsEnum.cpp',
     'TestRefCountedCopyConstructor.cpp',
     'TestSprintfLiteral.cpp',
     'TestStackClass.cpp',
+    'TestTemporaryClass.cpp',
     'TestTrivialCtorDtor.cpp',
 ]
 
 DisableStlWrapping()
 NoVisibilityFlags()
 
 # Build without any warning flags, and with clang verify flag for a
 # syntax-only build (no codegen), without a limit on the number of errors.
