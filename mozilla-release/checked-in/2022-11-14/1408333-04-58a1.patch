# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1508839359 -7200
#      Tue Oct 24 12:02:39 2017 +0200
# Node ID 3c13a8e409773d3241fe3b9c199a8f578da7c04e
# Parent  c0dc9d4848566024f276a4cd96dd2ae776f946d5
Bug 1408333 Get rid of nsIIPCBackgroundChildCreateCallback - part 4 - MutableBlobStorage, r=asuth

diff --git a/dom/file/MutableBlobStorage.cpp b/dom/file/MutableBlobStorage.cpp
--- a/dom/file/MutableBlobStorage.cpp
+++ b/dom/file/MutableBlobStorage.cpp
@@ -545,49 +545,28 @@ MutableBlobStorage::ShouldBeTemporarySto
   }
 
   return bufferSize.value() >= mMaxMemory;
 }
 
 void
 MutableBlobStorage::MaybeCreateTemporaryFile()
 {
+  MOZ_ASSERT(NS_IsMainThread());
+
   mStorageState = eWaitingForTemporaryFile;
 
-  mozilla::ipc::PBackgroundChild* actor =
-    mozilla::ipc::BackgroundChild::GetForCurrentThread();
-  if (actor) {
-    ActorCreated(actor);
-  } else {
-    mozilla::ipc::BackgroundChild::GetOrCreateForCurrentThread(this);
-  }
-}
-
-void
-MutableBlobStorage::ActorFailed()
-{
-  MOZ_CRASH("Failed to create a PBackgroundChild actor!");
-}
-
-void
-MutableBlobStorage::ActorCreated(PBackgroundChild* aActor)
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  MOZ_ASSERT(mStorageState == eWaitingForTemporaryFile ||
-             mStorageState == eClosed);
-  MOZ_ASSERT_IF(mPendingCallback, mStorageState == eClosed);
-
-  // If the object has been already closed and we don't need to execute a
-  // callback, we have nothing else to do.
-  if (mStorageState == eClosed && !mPendingCallback) {
-    return;
+  mozilla::ipc::PBackgroundChild* actorChild =
+    mozilla::ipc::BackgroundChild::GetOrCreateForCurrentThread();
+  if (NS_WARN_IF(!actorChild)) {
+    MOZ_CRASH("Failed to create a PBackgroundChild actor!");
   }
 
   mActor = new TemporaryIPCBlobChild(this);
-  aActor->SendPTemporaryIPCBlobConstructor(mActor);
+  actorChild->SendPTemporaryIPCBlobConstructor(mActor);
 
   // We need manually to increase the reference for this actor because the
   // IPC allocator method is not triggered. The Release() is called by IPDL
   // when the actor is deleted.
   mActor.get()->AddRef();
 
   // The actor will call us when the FileDescriptor is received.
 }
@@ -691,12 +670,10 @@ MutableBlobStorage::DispatchToIOThread(a
 
 size_t
 MutableBlobStorage::SizeOfCurrentMemoryBuffer() const
 {
   MOZ_ASSERT(NS_IsMainThread());
   return mStorageState < eInTemporaryFile ? mDataLen : 0;
 }
 
-NS_IMPL_ISUPPORTS(MutableBlobStorage, nsIIPCBackgroundChildCreateCallback)
-
 } // dom namespace
 } // mozilla namespace
diff --git a/dom/file/MutableBlobStorage.h b/dom/file/MutableBlobStorage.h
--- a/dom/file/MutableBlobStorage.h
+++ b/dom/file/MutableBlobStorage.h
@@ -3,17 +3,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_MutableBlobStorage_h
 #define mozilla_dom_MutableBlobStorage_h
 
 #include "mozilla/RefPtr.h"
-#include "nsIIPCBackgroundChildCreateCallback.h"
 #include "prio.h"
 
 class nsIEventTarget;
 class nsIRunnable;
 
 namespace mozilla {
 
 class TaskQueue;
@@ -32,21 +31,20 @@ public:
   NS_INLINE_DECL_PURE_VIRTUAL_REFCOUNTING
 
   virtual void BlobStoreCompleted(MutableBlobStorage* aBlobStorage,
                                   Blob* aBlob,
                                   nsresult aRv) = 0;
 };
 
 // This class is main-thread only.
-class MutableBlobStorage final : public nsIIPCBackgroundChildCreateCallback
+class MutableBlobStorage final
 {
 public:
-  NS_DECL_NSIIPCBACKGROUNDCHILDCREATECALLBACK
-  NS_DECL_THREADSAFE_ISUPPORTS
+  NS_INLINE_DECL_THREADSAFE_REFCOUNTING(MutableBlobStorage)
 
   enum MutableBlobStorageType
   {
     eOnlyInMemory,
     eCouldBeInTemporaryFile,
   };
 
   explicit MutableBlobStorage(MutableBlobStorageType aType,
