# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1509383429 14400
# Node ID 12ac8625bf4ca24334ef7cdd68a95d92239a159c
# Parent  c5be64943e914644c53d0f0a7fd61aac542b6e8b
Bug 1331944 - Part 4. Add RenderSharedSurfaceTextureHost wrapper to integrate with external images. r=jrmuizel

diff --git a/gfx/webrender_bindings/RenderSharedSurfaceTextureHost.cpp b/gfx/webrender_bindings/RenderSharedSurfaceTextureHost.cpp
new file mode 100644
--- /dev/null
+++ b/gfx/webrender_bindings/RenderSharedSurfaceTextureHost.cpp
@@ -0,0 +1,53 @@
+/* -*- Mode: C++; tab-width: 20; indent-tabs-mode: nil; c-basic-offset: 2 -*-
+ * This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "RenderSharedSurfaceTextureHost.h"
+
+#include "mozilla/gfx/Logging.h"
+#include "mozilla/layers/ImageDataSerializer.h"
+#include "mozilla/layers/SourceSurfaceSharedData.h"
+
+namespace mozilla {
+namespace wr {
+
+RenderSharedSurfaceTextureHost::RenderSharedSurfaceTextureHost(gfx::SourceSurfaceSharedDataWrapper* aSurface)
+  : mSurface(aSurface)
+  , mLocked(false)
+{
+  MOZ_COUNT_CTOR_INHERITED(RenderSharedSurfaceTextureHost, RenderTextureHost);
+  MOZ_ASSERT(aSurface);
+}
+
+RenderSharedSurfaceTextureHost::~RenderSharedSurfaceTextureHost()
+{
+  MOZ_COUNT_DTOR_INHERITED(RenderSharedSurfaceTextureHost, RenderTextureHost);
+}
+
+wr::WrExternalImage
+RenderSharedSurfaceTextureHost::Lock(uint8_t aChannelIndex, gl::GLContext* aGL)
+{
+  if (!mLocked) {
+    if (NS_WARN_IF(!mSurface->Map(gfx::DataSourceSurface::MapType::READ_WRITE,
+                                  &mMap))) {
+      return RawDataToWrExternalImage(nullptr, 0);
+    }
+    mLocked = true;
+  }
+
+  return RawDataToWrExternalImage(mMap.mData,
+                                  mMap.mStride * mSurface->GetSize().height);
+}
+
+void
+RenderSharedSurfaceTextureHost::Unlock()
+{
+  if (mLocked) {
+    mSurface->Unmap();
+    mLocked = false;
+  }
+}
+
+} // namespace wr
+} // namespace mozilla
diff --git a/gfx/webrender_bindings/RenderSharedSurfaceTextureHost.h b/gfx/webrender_bindings/RenderSharedSurfaceTextureHost.h
new file mode 100644
--- /dev/null
+++ b/gfx/webrender_bindings/RenderSharedSurfaceTextureHost.h
@@ -0,0 +1,42 @@
+/* -*- Mode: C++; tab-width: 20; indent-tabs-mode: nil; c-basic-offset: 2 -*-
+ * This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef MOZILLA_GFX_RENDERSHAREDSURFACETEXTUREHOST_H
+#define MOZILLA_GFX_RENDERSHAREDSURFACETEXTUREHOST_H
+
+#include "RenderTextureHost.h"
+
+namespace mozilla {
+namespace gfx {
+class SourceSurfaceSharedDataWrapper;
+}
+
+namespace wr {
+
+/**
+ * This class allows for surfaces managed by SharedSurfacesParent to be inserted
+ * into the render texture cache by wrapping an existing surface wrapper. These
+ * surfaces are backed by BGRA/X shared memory buffers.
+ */
+class RenderSharedSurfaceTextureHost final : public RenderTextureHost
+{
+public:
+  explicit RenderSharedSurfaceTextureHost(gfx::SourceSurfaceSharedDataWrapper* aSurface);
+
+  wr::WrExternalImage Lock(uint8_t aChannelIndex, gl::GLContext* aGL) override;
+  void Unlock() override;
+
+private:
+  ~RenderSharedSurfaceTextureHost() override;
+
+  RefPtr<gfx::SourceSurfaceSharedDataWrapper> mSurface;
+  gfx::DataSourceSurface::MappedSurface mMap;
+  bool mLocked;
+};
+
+} // namespace wr
+} // namespace mozilla
+
+#endif // MOZILLA_GFX_RENDERSHAREDSURFACETEXTUREHOST_H
diff --git a/gfx/webrender_bindings/moz.build b/gfx/webrender_bindings/moz.build
--- a/gfx/webrender_bindings/moz.build
+++ b/gfx/webrender_bindings/moz.build
@@ -5,29 +5,31 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 with Files('**'):
     BUG_COMPONENT = ('Core', 'Graphics: WebRender')
 
 EXPORTS.mozilla.webrender += [
     'RenderBufferTextureHost.h',
     'RendererOGL.h',
+    'RenderSharedSurfaceTextureHost.h',
     'RenderTextureHost.h',
     'RenderTextureHostOGL.h',
     'RenderThread.h',
     'webrender_ffi.h',
     'webrender_ffi_generated.h',
     'WebRenderAPI.h',
     'WebRenderTypes.h',
 ]
 
 UNIFIED_SOURCES += [
     'Moz2DImageRenderer.cpp',
     'RenderBufferTextureHost.cpp',
     'RendererOGL.cpp',
+    'RenderSharedSurfaceTextureHost.cpp',
     'RenderTextureHost.cpp',
     'RenderTextureHostOGL.cpp',
     'RenderThread.cpp',
     'WebRenderAPI.cpp',
 ]
 
 if CONFIG['MOZ_WIDGET_TOOLKIT'] == 'cocoa':
     EXPORTS.mozilla.webrender += [

