# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1506977395 14400
#      Mon Oct 02 16:49:55 2017 -0400
# Node ID 2369eb303658c724e18641134c725ecb2f8b6cf6
# Parent  f936f29ab4156804612bcc40068ee3c6e9bd5379
Bug 1403176 - Make WebRenderLayerManager reject pending scroll offset updates. r=mstange

Currently some callers attempt to set a "pending scroll offset update"
on the layer tree, which basically allows it to send a scroll offset
update to the compositor in an empty transaction, without doing a full
paint. However, WebRenderLayerManager doesn't really support empty
transactions yet, so we want to reject attempts to do this for now. This
will force the callers to schedule a full transaction instead of an
empty transaction.

MozReview-Commit-ID: 1bBlj59W5HH

diff --git a/gfx/layers/Layers.h b/gfx/layers/Layers.h
--- a/gfx/layers/Layers.h
+++ b/gfx/layers/Layers.h
@@ -798,18 +798,18 @@ private:
   FramesTimingRecording mRecording;
 
 public:
   /*
    * Methods to store/get/clear a "pending scroll info update" object on a
    * per-scrollid basis. This is used for empty transactions that push over
    * scroll position updates to the APZ code.
    */
-  bool SetPendingScrollUpdateForNextTransaction(FrameMetrics::ViewID aScrollId,
-                                                const ScrollUpdateInfo& aUpdateInfo);
+  virtual bool SetPendingScrollUpdateForNextTransaction(FrameMetrics::ViewID aScrollId,
+                                                        const ScrollUpdateInfo& aUpdateInfo);
   Maybe<ScrollUpdateInfo> GetPendingScrollInfoUpdate(FrameMetrics::ViewID aScrollId);
   void ClearPendingScrollInfoUpdate();
 private:
   std::map<FrameMetrics::ViewID,ScrollUpdateInfo> mPendingScrollUpdates;
 
   // Display items are only valid during this transaction.
   // At the end of the transaction, we have to go and clear out
   // DisplayItemLayer's and null their display item. See comment
diff --git a/gfx/layers/wr/WebRenderLayerManager.cpp b/gfx/layers/wr/WebRenderLayerManager.cpp
--- a/gfx/layers/wr/WebRenderLayerManager.cpp
+++ b/gfx/layers/wr/WebRenderLayerManager.cpp
@@ -1280,10 +1280,19 @@ WebRenderLayerManager::CreateBorderLayer
 }
 
 already_AddRefed<DisplayItemLayer>
 WebRenderLayerManager::CreateDisplayItemLayer()
 {
   return MakeAndAddRef<WebRenderDisplayItemLayer>(this);
 }
 
+bool
+WebRenderLayerManager::SetPendingScrollUpdateForNextTransaction(FrameMetrics::ViewID aScrollId,
+                                                                const ScrollUpdateInfo& aUpdateInfo)
+{
+  // If we ever support changing the scroll position in an "empty transactions"
+  // properly in WR we can fill this in. Covered by bug 1382259.
+  return false;
+}
+
 } // namespace layers
 } // namespace mozilla
diff --git a/gfx/layers/wr/WebRenderLayerManager.h b/gfx/layers/wr/WebRenderLayerManager.h
--- a/gfx/layers/wr/WebRenderLayerManager.h
+++ b/gfx/layers/wr/WebRenderLayerManager.h
@@ -244,16 +244,19 @@ public:
     }
     RefPtr<T> res = static_cast<T*>(data.get());
     return res.forget();
   }
 
   bool ShouldNotifyInvalidation() const { return mShouldNotifyInvalidation; }
   void SetNotifyInvalidation(bool aShouldNotifyInvalidation) { mShouldNotifyInvalidation = aShouldNotifyInvalidation; }
 
+  bool SetPendingScrollUpdateForNextTransaction(FrameMetrics::ViewID aScrollId,
+                                                const ScrollUpdateInfo& aUpdateInfo) override;
+
 private:
   /**
    * Take a snapshot of the parent context, and copy
    * it into mTarget.
    */
   void MakeSnapshotIfRequired(LayoutDeviceIntSize aSize);
 
   void ClearLayer(Layer* aLayer);
