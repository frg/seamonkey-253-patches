# HG changeset patch
# User Karl Tomlinson <karlt+@karlt.net>
# Date 1507517329 -46800
#      Mon Oct 09 15:48:49 2017 +1300
# Node ID 6ffe631f903855c8537ad400b5ab22aad301ba9b
# Parent  803d2e044e6f0d8829ff1b52d4582c6ad041b3e1
bug 1404220 assign zero to AUDIO_FORMAT_SILENCE enumerator r=padenot

I don't know whether or not another zero initial AudioChunk member value
makes initialization more efficient, but zero for silence is more intuitive
for humans.

MozReview-Commit-ID: JEYv65btMul

diff --git a/dom/media/AudioSampleFormat.h b/dom/media/AudioSampleFormat.h
--- a/dom/media/AudioSampleFormat.h
+++ b/dom/media/AudioSampleFormat.h
@@ -15,22 +15,22 @@ namespace mozilla {
  * Audio formats supported in MediaStreams and media elements.
  *
  * Only one of these is supported by AudioStream, and that is determined
  * at compile time (roughly, FLOAT32 on desktops, S16 on mobile). Media decoders
  * produce that format only; queued AudioData always uses that format.
  */
 enum AudioSampleFormat
 {
+  // Silence: format will be chosen later
+  AUDIO_FORMAT_SILENCE,
   // Native-endian signed 16-bit audio samples
   AUDIO_FORMAT_S16,
   // Signed 32-bit float samples
   AUDIO_FORMAT_FLOAT32,
-  // Silence: format will be chosen later
-  AUDIO_FORMAT_SILENCE,
   // The format used for output by AudioStream.
 #ifdef MOZ_SAMPLE_TYPE_S16
   AUDIO_OUTPUT_FORMAT = AUDIO_FORMAT_S16
 #else
   AUDIO_OUTPUT_FORMAT = AUDIO_FORMAT_FLOAT32
 #endif
 };
 
@@ -241,18 +241,18 @@ ScaleAudioSamples(short* aBuffer, int aC
     aBuffer[i] = short((int32_t(aBuffer[i]) * volume) >> 16);
   }
 }
 
 inline const void*
 AddAudioSampleOffset(const void* aBase, AudioSampleFormat aFormat,
                      int32_t aOffset)
 {
-  static_assert(AUDIO_FORMAT_S16 == 0, "Bad constant");
-  static_assert(AUDIO_FORMAT_FLOAT32 == 1, "Bad constant");
+  static_assert(AUDIO_FORMAT_S16 == 1, "Bad constant");
+  static_assert(AUDIO_FORMAT_FLOAT32 == 2, "Bad constant");
   MOZ_ASSERT(aFormat == AUDIO_FORMAT_S16 || aFormat == AUDIO_FORMAT_FLOAT32);
 
-  return static_cast<const uint8_t*>(aBase) + (aFormat + 1)*2*aOffset;
+  return static_cast<const uint8_t*>(aBase) + aFormat*2*aOffset;
 }
 
 } // namespace mozilla
 
 #endif /* MOZILLA_AUDIOSAMPLEFORMAT_H_ */
