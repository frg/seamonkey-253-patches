# HG changeset patch
# User John Dai <jdai@mozilla.com>
# Date 1507796700 14400
# Node ID b47e0b4d65a9daf651dce59e38896856d062dfaf
# Parent  bd60e1d4b8b42ef41052cc2a38face3b31dfb730
Bug 1406297 - Fix Document.createElement must report an exception. r=smaug

diff --git a/dom/base/CustomElementRegistry.cpp b/dom/base/CustomElementRegistry.cpp
--- a/dom/base/CustomElementRegistry.cpp
+++ b/dom/base/CustomElementRegistry.cpp
@@ -108,23 +108,21 @@ CustomElementConstructor::Construct(cons
   if (!cx) {
     MOZ_ASSERT(aRv.Failed());
     return nullptr;
   }
 
   JS::Rooted<JSObject*> result(cx);
   JS::Rooted<JS::Value> constructor(cx, JS::ObjectValue(*mCallback));
   if (!JS::Construct(cx, constructor, JS::HandleValueArray::empty(), &result)) {
-    aRv.Throw(NS_ERROR_DOM_INVALID_STATE_ERR);
     return nullptr;
   }
 
   RefPtr<Element> element;
   if (NS_FAILED(UNWRAP_OBJECT(Element, &result, element))) {
-    aRv.Throw(NS_ERROR_DOM_INVALID_STATE_ERR);
     return nullptr;
   }
 
   return element.forget();
 }
 //-----------------------------------------------------
 // CustomElementData
 
@@ -900,17 +898,17 @@ DoUpgrade(Element* aElement,
   // Rethrow the exception since it might actually throw the exception from the
   // upgrade steps back out to the caller of document.createElement.
   RefPtr<Element> constructResult =
     aConstructor->Construct("Custom Element Upgrade", aRv);
   if (aRv.Failed()) {
     return;
   }
 
-  if (constructResult.get() != aElement) {
+  if (!constructResult || constructResult.get() != aElement) {
     aRv.Throw(NS_ERROR_DOM_INVALID_STATE_ERR);
     return;
   }
 }
 
 } // anonymous namespace
 
 // https://html.spec.whatwg.org/multipage/scripting.html#upgrades
diff --git a/dom/html/nsHTMLContentSink.cpp b/dom/html/nsHTMLContentSink.cpp
--- a/dom/html/nsHTMLContentSink.cpp
+++ b/dom/html/nsHTMLContentSink.cpp
@@ -221,28 +221,33 @@ public:
   };
 
   Node* mStack;
   int32_t mStackSize;
   int32_t mStackPos;
 };
 
 static void
-DoCustomElementCreate(Element** aElement, nsIDocument* aDoc,
+DoCustomElementCreate(Element** aElement, nsIDocument* aDoc, nsIAtom* aLocalName,
                       CustomElementConstructor* aConstructor, ErrorResult& aRv)
 {
   RefPtr<Element> element =
     aConstructor->Construct("Custom Element Create", aRv);
-  if (aRv.Failed() || !element->IsHTMLElement()) {
+  if (aRv.Failed()) {
+    return;
+  }
+
+  if (!element || !element->IsHTMLElement()) {
     aRv.ThrowTypeError<MSG_THIS_DOES_NOT_IMPLEMENT_INTERFACE>(NS_LITERAL_STRING("HTMLElement"));
     return;
   }
 
   if (aDoc != element->OwnerDoc() || element->GetParentNode() ||
-      element->HasChildren() || element->GetAttrCount()) {
+      element->HasChildren() || element->GetAttrCount() ||
+      element->NodeInfo()->NameAtom() != aLocalName) {
     aRv.Throw(NS_ERROR_DOM_NOT_SUPPORTED_ERR);
     return;
   }
 
   element.forget(aElement);
 }
 
 nsresult
@@ -304,29 +309,30 @@ NS_NewHTMLElement(Element** aResult, alr
       // SetCustomElementData().
       nsCOMPtr<nsIAtom> tagAtom = nodeInfo->NameAtom();
       nsCOMPtr<nsIAtom> typeAtom = aIs ? NS_Atomize(*aIs) : tagAtom;
       // Built-in element
       *aResult = CreateHTMLElement(tag, nodeInfo.forget(), aFromParser).take();
       (*aResult)->SetCustomElementData(new CustomElementData(typeAtom));
       if (synchronousCustomElements) {
         CustomElementRegistry::Upgrade(*aResult, definition, rv);
+        if (rv.MaybeSetPendingException(cx)) {
+          aes.ReportException();
+        }
       } else {
         nsContentUtils::EnqueueUpgradeReaction(*aResult, definition);
       }
 
-      if (rv.MaybeSetPendingException(cx)) {
-        aes.ReportException();
-      }
       return NS_OK;
     }
 
     // Step 6.1.
     if (synchronousCustomElements) {
       DoCustomElementCreate(aResult, nodeInfo->GetDocument(),
+                            nodeInfo->NameAtom(),
                             definition->mConstructor, rv);
       if (rv.MaybeSetPendingException(cx)) {
         NS_IF_ADDREF(*aResult = NS_NewHTMLUnknownElement(nodeInfo.forget(), aFromParser));
       }
       return NS_OK;
     }
 
     // Step 6.2.
