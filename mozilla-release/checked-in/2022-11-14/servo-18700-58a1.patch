# HG changeset patch
# User Ting-Yu Lin <tlin@mozilla.com>
# Date 1506916544 18000
#      Sun Oct 01 22:55:44 2017 -0500
# Node ID 7bd101e8bb6ee9fc29b96fffb972b747ff0f4813
# Parent  4ac8b57c8db69f294b2b2367e533c460bd5d08e9
servo: Merge #18700 - style: Refactor style shape source (from aethanyc:refactor-style-shape-source); r=heycam

This was reviewed in https://bugzilla.mozilla.org/show_bug.cgi?id=1404243

Source-Repo: https://github.com/servo/servo
Source-Revision: e554d1c9d425767477f0a8d814be630d31cd2499

diff --git a/servo/components/style/gecko/conversions.rs b/servo/components/style/gecko/conversions.rs
--- a/servo/components/style/gecko/conversions.rs
+++ b/servo/components/style/gecko/conversions.rs
@@ -612,23 +612,24 @@ pub mod basic_shape {
         ReferenceBox: From<StyleGeometryBox>,
     {
         fn from(other: &'a StyleShapeSource) -> Self {
             match other.mType {
                 StyleShapeSourceType::None => ShapeSource::None,
                 StyleShapeSourceType::Box => ShapeSource::Box(other.mReferenceBox.into()),
                 StyleShapeSourceType::URL => {
                     unsafe {
-                        let other_url = &(**other.__bindgen_anon_1.mURL.as_ref());
+                        let shape_image = &*other.mShapeImage.mPtr;
+                        let other_url = &(**shape_image.__bindgen_anon_1.mURLValue.as_ref());
                         let url = SpecifiedUrl::from_url_value_data(&other_url._base).unwrap();
                         ShapeSource::Url(url)
                     }
                 },
                 StyleShapeSourceType::Shape => {
-                    let other_shape = unsafe { &(**other.__bindgen_anon_1.mBasicShape.as_ref()) };
+                    let other_shape = unsafe { &*other.mBasicShape.mPtr };
                     let shape = other_shape.into();
                     let reference_box = if other.mReferenceBox == StyleGeometryBox::NoBox {
                         None
                     } else {
                         Some(other.mReferenceBox.into())
                     };
                     ShapeSource::Shape(shape, reference_box)
                 }
diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -4943,27 +4943,23 @@ fn static_assert() {
                 }
             }
             ShapeSource::None => {} // don't change the type
             ShapeSource::Box(reference) => {
                 ${ident}.mReferenceBox = reference.into();
                 ${ident}.mType = StyleShapeSourceType::Box;
             }
             ShapeSource::Shape(servo_shape, maybe_box) => {
-                ${ident}.mReferenceBox = maybe_box.map(Into::into)
-                                                   .unwrap_or(StyleGeometryBox::NoBox);
-                ${ident}.mType = StyleShapeSourceType::Shape;
-
-                fn init_shape(${ident}: &mut StyleShapeSource, ty: StyleBasicShapeType) -> &mut StyleBasicShape {
+                fn init_shape(${ident}: &mut StyleShapeSource, basic_shape_type: StyleBasicShapeType)
+                              -> &mut StyleBasicShape {
                     unsafe {
-                        // We have to be very careful to avoid a copy here!
-                        let ref mut union = ${ident}.__bindgen_anon_1;
-                        let shape: &mut *mut StyleBasicShape = union.mBasicShape.as_mut();
-                        *shape = Gecko_NewBasicShape(ty);
-                        &mut **shape
+                        // Create StyleBasicShape in StyleShapeSource. mReferenceBox and mType
+                        // will be set manually later.
+                        Gecko_NewBasicShape(${ident}, basic_shape_type);
+                        &mut *${ident}.mBasicShape.mPtr
                     }
                 }
                 match servo_shape {
                     BasicShape::Inset(inset) => {
                         let shape = init_shape(${ident}, StyleBasicShapeType::Inset);
                         unsafe { shape.mCoordinates.set_len(4) };
 
                         // set_len() can't call constructors, so the coordinates
@@ -5015,16 +5011,20 @@ fn static_assert() {
                         }
                         shape.mFillRule = if poly.fill == FillRule::EvenOdd {
                             StyleFillRule::Evenodd
                         } else {
                             StyleFillRule::Nonzero
                         };
                     }
                 }
+
+                ${ident}.mReferenceBox = maybe_box.map(Into::into)
+                                                  .unwrap_or(StyleGeometryBox::NoBox);
+                ${ident}.mType = StyleShapeSourceType::Shape;
             }
         }
 
     }
 
     pub fn clone_${ident}(&self) -> longhands::${ident}::computed_value::T {
         (&self.gecko.${gecko_ffi_name}).into()
     }
