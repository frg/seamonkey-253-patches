# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1509470294 14400
# Node ID 3bc72af522e8beb8d5e04943a20ef383d998643d
# Parent  33efecc679a7cabcfb90704da3641cec8e21d1a3
Bug 1407752 - Avoid taking the webrender fallback path for empty borders. r=jrmuizel

MozReview-Commit-ID: BTSljrUuKcH

diff --git a/layout/forms/nsButtonFrameRenderer.cpp b/layout/forms/nsButtonFrameRenderer.cpp
--- a/layout/forms/nsButtonFrameRenderer.cpp
+++ b/layout/forms/nsButtonFrameRenderer.cpp
@@ -248,17 +248,20 @@ nsDisplayButtonBoxShadowOuter::CreateWeb
   }
   return true;
 }
 
 class nsDisplayButtonBorder : public nsDisplayItem {
 public:
   nsDisplayButtonBorder(nsDisplayListBuilder* aBuilder,
                                   nsButtonFrameRenderer* aRenderer)
-    : nsDisplayItem(aBuilder, aRenderer->GetFrame()), mBFR(aRenderer) {
+    : nsDisplayItem(aBuilder, aRenderer->GetFrame())
+    , mBFR(aRenderer)
+    , mBorderIsEmpty(false)
+  {
     MOZ_COUNT_CTOR(nsDisplayButtonBorder);
   }
 #ifdef NS_BUILD_REFCNT_LOGGING
   virtual ~nsDisplayButtonBorder() {
     MOZ_COUNT_DTOR(nsDisplayButtonBorder);
   }
 #endif
   virtual bool MustPaintOnContentSide() const override { return true; }
@@ -286,16 +289,17 @@ public:
                                        mozilla::wr::IpcResourceUpdateQueue& aResources,
                                        const StackingContextHelper& aSc,
                                        mozilla::layers::WebRenderLayerManager* aManager,
                                        nsDisplayListBuilder* aDisplayListBuilder) override;
   NS_DISPLAY_DECL_NAME("ButtonBorderBackground", TYPE_BUTTON_BORDER_BACKGROUND)
 private:
   nsButtonFrameRenderer* mBFR;
   Maybe<nsCSSBorderRenderer> mBorderRenderer;
+  bool mBorderIsEmpty;
 };
 
 nsDisplayItemGeometry*
 nsDisplayButtonBorder::AllocateGeometry(nsDisplayListBuilder* aBuilder)
 {
   return new nsDisplayItemGenericImageGeometry(this, aBuilder);
 }
 
@@ -312,25 +316,30 @@ nsDisplayButtonBorder::GetLayerState(nsD
 
     nsPoint offset = ToReferenceFrame();
     if (!nsDisplayBoxShadowInner::CanCreateWebRenderCommands(aBuilder,
                                                              mFrame,
                                                              offset)) {
       return LAYER_NONE;
     }
 
+    mBorderIsEmpty = false;
     Maybe<nsCSSBorderRenderer> br =
     nsCSSRendering::CreateBorderRenderer(mFrame->PresContext(),
                                          nullptr,
                                          mFrame,
                                          nsRect(),
                                          nsRect(offset, mFrame->GetSize()),
                                          mFrame->StyleContext(),
-                                         mFrame->GetSkipSides());
+                                         mFrame->GetSkipSides(),
+                                         &mBorderIsEmpty);
     if (!br) {
+      if (mBorderIsEmpty) {
+        return LAYER_ACTIVE;
+      }
       return LAYER_NONE;
     }
 
     if (!br->CanCreateWebRenderCommands()) {
       return LAYER_NONE;
     }
 
     mBorderRenderer = br;
@@ -355,17 +364,21 @@ nsDisplayButtonBorder::CreateWebRenderCo
                                                mozilla::layers::WebRenderLayerManager* aManager,
                                                nsDisplayListBuilder* aDisplayListBuilder)
 {
   ContainerLayerParameters parameter;
   if (GetLayerState(aDisplayListBuilder, aManager, parameter) != LAYER_ACTIVE) {
     return false;
   }
 
-  MOZ_ASSERT(mBorderRenderer);
+  if (!mBorderRenderer) {
+    // empty border, nothing to do
+    MOZ_ASSERT(mBorderIsEmpty);
+    return true;
+  }
 
   // This is really a combination of paint box shadow inner +
   // paint border.
   nsRect buttonRect = nsRect(ToReferenceFrame(), mFrame->GetSize());
   bool snap;
   nsRegion visible = GetBounds(aDisplayListBuilder, &snap);
   nsDisplayBoxShadowInner::CreateInsetBoxShadowWebRenderCommands(aBuilder,
                                                                  aSc,
diff --git a/layout/painting/nsCSSRendering.cpp b/layout/painting/nsCSSRendering.cpp
--- a/layout/painting/nsCSSRendering.cpp
+++ b/layout/painting/nsCSSRendering.cpp
@@ -661,40 +661,42 @@ nsCSSRendering::PaintBorder(nsPresContex
 
 Maybe<nsCSSBorderRenderer>
 nsCSSRendering::CreateBorderRenderer(nsPresContext* aPresContext,
                                      DrawTarget* aDrawTarget,
                                      nsIFrame* aForFrame,
                                      const nsRect& aDirtyRect,
                                      const nsRect& aBorderArea,
                                      nsStyleContext* aStyleContext,
-                                     Sides aSkipSides)
+                                     Sides aSkipSides,
+                                     bool* aOutBorderIsEmpty)
 {
   nsStyleContext *styleIfVisited = aStyleContext->GetStyleIfVisited();
   const nsStyleBorder *styleBorder = aStyleContext->StyleBorder();
   // Don't check RelevantLinkVisited here, since we want to take the
   // same amount of time whether or not it's true.
   if (!styleIfVisited) {
     return CreateBorderRendererWithStyleBorder(aPresContext, aDrawTarget,
                                                aForFrame, aDirtyRect,
                                                aBorderArea, *styleBorder,
-                                               aStyleContext, aSkipSides);
+                                               aStyleContext, aSkipSides,
+                                               aOutBorderIsEmpty);
   }
 
   nsStyleBorder newStyleBorder(*styleBorder);
 
   NS_FOR_CSS_SIDES(side) {
     nscolor color = aStyleContext->
       GetVisitedDependentColor(nsStyleBorder::BorderColorFieldFor(side));
     newStyleBorder.mBorderColor[side] = StyleComplexColor::FromColor(color);
   }
   return CreateBorderRendererWithStyleBorder(aPresContext, aDrawTarget,
                                              aForFrame, aDirtyRect, aBorderArea,
                                              newStyleBorder, aStyleContext,
-                                             aSkipSides);
+                                             aSkipSides, aOutBorderIsEmpty);
 }
 
 
 bool
 nsCSSRendering::CreateWebRenderCommandsForBorder(nsDisplayItem* aItem,
                                                  nsIFrame* aForFrame,
                                                  const nsRect& aBorderArea,
                                                  mozilla::wr::DisplayListBuilder& aBuilder,
@@ -976,17 +978,18 @@ nsCSSRendering::PaintBorderWithStyleBord
 Maybe<nsCSSBorderRenderer>
 nsCSSRendering::CreateBorderRendererWithStyleBorder(nsPresContext* aPresContext,
                                                     DrawTarget* aDrawTarget,
                                                     nsIFrame* aForFrame,
                                                     const nsRect& aDirtyRect,
                                                     const nsRect& aBorderArea,
                                                     const nsStyleBorder& aStyleBorder,
                                                     nsStyleContext* aStyleContext,
-                                                    Sides aSkipSides)
+                                                    Sides aSkipSides,
+                                                    bool* aOutBorderIsEmpty)
 {
   const nsStyleDisplay* displayData = aStyleContext->StyleDisplay();
   if (displayData->mAppearance) {
     nsITheme *theme = aPresContext->GetTheme();
     if (theme &&
         theme->ThemeSupportsWidget(aPresContext, aForFrame,
                                    displayData->mAppearance)) {
       return Nothing();
@@ -996,16 +999,19 @@ nsCSSRendering::CreateBorderRendererWith
   if (aStyleBorder.mBorderImageSource.GetType() != eStyleImageType_Null) {
     return Nothing();
   }
 
   nsMargin border = aStyleBorder.GetComputedBorder();
   if (0 == border.left && 0 == border.right &&
       0 == border.top  && 0 == border.bottom) {
     // Empty border area
+    if (aOutBorderIsEmpty) {
+      *aOutBorderIsEmpty = true;
+    }
     return Nothing();
   }
 
   bool needsClip = false;
   nsCSSBorderRenderer br = ConstructBorderRenderer(aPresContext,
                                                    aStyleContext,
                                                    aDrawTarget,
                                                    aForFrame,
diff --git a/layout/painting/nsCSSRendering.h b/layout/painting/nsCSSRendering.h
--- a/layout/painting/nsCSSRendering.h
+++ b/layout/painting/nsCSSRendering.h
@@ -187,27 +187,29 @@ struct nsCSSRendering {
 
   static mozilla::Maybe<nsCSSBorderRenderer>
   CreateBorderRenderer(nsPresContext* aPresContext,
                        DrawTarget* aDrawTarget,
                        nsIFrame* aForFrame,
                        const nsRect& aDirtyRect,
                        const nsRect& aBorderArea,
                        nsStyleContext* aStyleContext,
-                       Sides aSkipSides = Sides());
+                       Sides aSkipSides = Sides(),
+                       bool* aOutBorderIsEmpty = nullptr);
 
   static mozilla::Maybe<nsCSSBorderRenderer>
   CreateBorderRendererWithStyleBorder(nsPresContext* aPresContext,
                                       DrawTarget* aDrawTarget,
                                       nsIFrame* aForFrame,
                                       const nsRect& aDirtyRect,
                                       const nsRect& aBorderArea,
                                       const nsStyleBorder& aBorderStyle,
                                       nsStyleContext* aStyleContext,
-                                      Sides aSkipSides = Sides());
+                                      Sides aSkipSides = Sides(),
+                                      bool* aOutBorderIsEmpty = nullptr);
 
   static mozilla::Maybe<nsCSSBorderRenderer>
   CreateBorderRendererForOutline(nsPresContext* aPresContext,
                                  gfxContext* aRenderingContext,
                                  nsIFrame* aForFrame,
                                  const nsRect& aDirtyRect,
                                  const nsRect& aBorderArea,
                                  nsStyleContext* aStyleContext);
