# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1508839360 -7200
#      Tue Oct 24 12:02:40 2017 +0200
# Node ID aae5ad233d21eca2290a9dcb959c5800428abd20
# Parent  bd43320ad761572fc3a52f9e9b1fda36fc743285
Bug 1408333 Get rid of nsIIPCBackgroundChildCreateCallback - part 8 - U2F, r=asuth

diff --git a/dom/u2f/U2FManager.cpp b/dom/u2f/U2FManager.cpp
--- a/dom/u2f/U2FManager.cpp
+++ b/dom/u2f/U2FManager.cpp
@@ -28,18 +28,17 @@ namespace dom {
 
 namespace {
 StaticRefPtr<U2FManager> gU2FManager;
 static mozilla::LazyLogModule gU2FManagerLog("u2fmanager");
 }
 
 NS_NAMED_LITERAL_STRING(kVisibilityChange, "visibilitychange");
 
-NS_IMPL_ISUPPORTS(U2FManager, nsIIPCBackgroundChildCreateCallback,
-                  nsIDOMEventListener);
+NS_IMPL_ISUPPORTS(U2FManager, nsIDOMEventListener);
 
 /***********************************************************************
  * Utility Functions
  **********************************************************************/
 
 static void
 ListenForVisibilityEvents(nsPIDOMWindowInner* aParent,
                           U2FManager* aListener)
@@ -138,35 +137,40 @@ U2FManager::~U2FManager()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   if (mTransaction.isSome()) {
     RejectTransaction(NS_ERROR_ABORT);
   }
 }
 
-RefPtr<U2FManager::BackgroundActorPromise>
+void
 U2FManager::GetOrCreateBackgroundActor()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
-  PBackgroundChild *actor = BackgroundChild::GetForCurrentThread();
-  RefPtr<U2FManager::BackgroundActorPromise> promise =
-    mPBackgroundCreationPromise.Ensure(__func__);
+  if (mChild) {
+    return;
+  }
 
-  if (actor) {
-    ActorCreated(actor);
-  } else {
-    bool ok = BackgroundChild::GetOrCreateForCurrentThread(this);
-    if (NS_WARN_IF(!ok)) {
-      ActorFailed();
-    }
+  PBackgroundChild* actorChild = BackgroundChild::GetOrCreateForCurrentThread();
+  if (NS_WARN_IF(!actorChild)) {
+    MOZ_CRASH("Failed to create a PBackgroundChild actor!");
   }
 
-  return promise;
+  RefPtr<U2FTransactionChild> mgr(new U2FTransactionChild());
+  PWebAuthnTransactionChild* constructedMgr =
+    actorChild->SendPWebAuthnTransactionConstructor(mgr);
+
+  if (NS_WARN_IF(!constructedMgr)) {
+    return;
+  }
+
+  MOZ_ASSERT(constructedMgr == mgr);
+  mChild = mgr.forget();
 }
 
 //static
 U2FManager*
 U2FManager::GetOrCreate()
 {
   MOZ_ASSERT(NS_IsMainThread());
   if (gU2FManager) {
@@ -254,42 +258,37 @@ U2FManager::Register(nsPIDOMWindowInner*
   }
 
   CryptoBuffer rpIdHash, clientDataHash;
   if (NS_FAILED(BuildTransactionHashes(aRpId, aClientDataJSON,
                                        rpIdHash, clientDataHash))) {
     return U2FPromise::CreateAndReject(ErrorCode::OTHER_ERROR, __func__).forget();
   }
 
-  RefPtr<MozPromise<nsresult, nsresult, false>> p = GetOrCreateBackgroundActor();
-  p->Then(GetMainThreadSerialEventTarget(), __func__,
-          []() {
-            U2FManager* mgr = U2FManager::Get();
-            if (mgr && mgr->mChild && mgr->mTransaction.isSome()) {
-              mgr->mChild->SendRequestRegister(mgr->mTransaction.ref().mInfo);
-            }
-          },
-          []() {
-            // This case can't actually happen, we'll have crashed if the child
-            // failed to create.
-          });
-
   ListenForVisibilityEvents(aParent, this);
 
   // Always blank for U2F
   nsTArray<WebAuthnExtension> extensions;
 
   WebAuthnTransactionInfo info(rpIdHash,
                                clientDataHash,
                                aTimeoutMillis,
                                aExcludeList,
                                extensions);
 
   MOZ_ASSERT(mTransaction.isNothing());
+
   mTransaction = Some(U2FTransaction(aParent, Move(info), aClientDataJSON));
+
+  GetOrCreateBackgroundActor();
+
+  if (mChild) {
+    mChild->SendRequestRegister(mTransaction.ref().mInfo);
+  }
+
   return mTransaction.ref().mPromise.Ensure(__func__);
 }
 
 already_AddRefed<U2FPromise>
 U2FManager::Sign(nsPIDOMWindowInner* aParent,
                  const nsCString& aRpId,
                  const nsCString& aClientDataJSON,
                  const uint32_t& aTimeoutMillis,
@@ -303,42 +302,36 @@ U2FManager::Sign(nsPIDOMWindowInner* aPa
   }
 
   CryptoBuffer rpIdHash, clientDataHash;
   if (NS_FAILED(BuildTransactionHashes(aRpId, aClientDataJSON,
                                        rpIdHash, clientDataHash))) {
     return U2FPromise::CreateAndReject(ErrorCode::OTHER_ERROR, __func__).forget();
   }
 
-  RefPtr<MozPromise<nsresult, nsresult, false>> p = GetOrCreateBackgroundActor();
-  p->Then(GetMainThreadSerialEventTarget(), __func__,
-          []() {
-            U2FManager* mgr = U2FManager::Get();
-            if (mgr && mgr->mChild && mgr->mTransaction.isSome()) {
-              mgr->mChild->SendRequestSign(mgr->mTransaction.ref().mInfo);
-            }
-          },
-          []() {
-            // This case can't actually happen, we'll have crashed if the child
-            // failed to create.
-          });
-
   ListenForVisibilityEvents(aParent, this);
 
   // Always blank for U2F
   nsTArray<WebAuthnExtension> extensions;
 
   WebAuthnTransactionInfo info(rpIdHash,
                                clientDataHash,
                                aTimeoutMillis,
                                aAllowList,
                                extensions);
 
   MOZ_ASSERT(mTransaction.isNothing());
   mTransaction = Some(U2FTransaction(aParent, Move(info), aClientDataJSON));
+
+  GetOrCreateBackgroundActor();
+
+  if (mChild) {
+    mChild->SendRequestSign(mTransaction.ref().mInfo);
+  }
+
   return mTransaction.ref().mPromise.Ensure(__func__);
 }
 
 void
 U2FManager::FinishRegister(nsTArray<uint8_t>& aRegBuffer)
 {
   MOZ_ASSERT(NS_IsMainThread());
 
@@ -479,44 +472,15 @@ U2FManager::HandleEvent(nsIDOMEvent* aEv
 
     CancelTransaction(NS_ERROR_ABORT);
   }
 
   return NS_OK;
 }
 
 void
-U2FManager::ActorCreated(PBackgroundChild* aActor)
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  MOZ_ASSERT(aActor);
-
-  if (mChild) {
-    return;
-  }
-
-  RefPtr<U2FTransactionChild> mgr(new U2FTransactionChild());
-  PWebAuthnTransactionChild* constructedMgr =
-    aActor->SendPWebAuthnTransactionConstructor(mgr);
-
-  if (NS_WARN_IF(!constructedMgr)) {
-    ActorFailed();
-    return;
-  }
-  MOZ_ASSERT(constructedMgr == mgr);
-  mChild = mgr.forget();
-  mPBackgroundCreationPromise.Resolve(NS_OK, __func__);
-}
-
-void
 U2FManager::ActorDestroyed()
 {
   mChild = nullptr;
 }
 
-void
-U2FManager::ActorFailed()
-{
-  MOZ_CRASH("We shouldn't be here!");
-}
-
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/u2f/U2FManager.h b/dom/u2f/U2FManager.h
--- a/dom/u2f/U2FManager.h
+++ b/dom/u2f/U2FManager.h
@@ -7,17 +7,16 @@
 #ifndef mozilla_dom_U2FManager_h
 #define mozilla_dom_U2FManager_h
 
 #include "U2FAuthenticator.h"
 #include "mozilla/MozPromise.h"
 #include "mozilla/dom/Event.h"
 #include "mozilla/dom/PWebAuthnTransaction.h"
 #include "nsIDOMEventListener.h"
-#include "nsIIPCBackgroundChildCreateCallback.h"
 
 /*
  * Content process manager for the U2F protocol. Created on calls to the
  * U2F DOM object, this manager handles establishing IPC channels
  * for U2F transactions, as well as keeping track of MozPromise objects
  * representing transactions in flight.
  *
  * The U2F spec (http://fidoalliance.org/specs/fido-u2f-v1.1-id-20160915.zip)
@@ -73,23 +72,21 @@ public:
   // Holds the parameters of the current transaction, as we need them both
   // before the transaction request is sent, and on successful return.
   WebAuthnTransactionInfo mInfo;
 
   // Client data used to assemble reply objects.
   nsCString mClientData;
 };
 
-class U2FManager final : public nsIIPCBackgroundChildCreateCallback
-                       , public nsIDOMEventListener
+class U2FManager final : public nsIDOMEventListener
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIDOMEVENTLISTENER
-  NS_DECL_NSIIPCBACKGROUNDCHILDCREATECALLBACK
 
   static U2FManager* GetOrCreate();
   static U2FManager* Get();
 
   already_AddRefed<U2FPromise> Register(nsPIDOMWindowInner* aParent,
               const nsCString& aRpId,
               const nsCString& aClientDataJSON,
               const uint32_t& aTimeoutMillis,
@@ -129,24 +126,21 @@ private:
   // Rejects the current transaction and calls ClearTransaction().
   void RejectTransaction(const nsresult& aError);
   // Cancels the current transaction (by sending a Cancel message to the
   // parent) and rejects it by calling RejectTransaction().
   void CancelTransaction(const nsresult& aError);
 
   typedef MozPromise<nsresult, nsresult, false> BackgroundActorPromise;
 
-  RefPtr<BackgroundActorPromise> GetOrCreateBackgroundActor();
+  void GetOrCreateBackgroundActor();
 
   // IPC Channel for the current transaction.
   RefPtr<U2FTransactionChild> mChild;
 
   // The current transaction, if any.
   Maybe<U2FTransaction> mTransaction;
-
-  // Promise for dealing with PBackground Actor creation.
-  MozPromiseHolder<BackgroundActorPromise> mPBackgroundCreationPromise;
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // mozilla_dom_U2FManager_h
