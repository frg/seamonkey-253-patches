# HG changeset patch
# User Stone Shih <sshih@mozilla.com>
# Date 1505879876 -28800
# Node ID 5e0a1d82ef670c537b7b5c60ba0f2606a9f04ef7
# Parent  054bb5a4c7ed4d41955b13aa617dae774c33d0e4
Bug 1404273 - [Pointer Event] Coalescing mousemove for different pointers. r=smaug.

MozReview-Commit-ID: HfjRiUlBy3L

diff --git a/dom/ipc/CoalescedMouseData.h b/dom/ipc/CoalescedMouseData.h
--- a/dom/ipc/CoalescedMouseData.h
+++ b/dom/ipc/CoalescedMouseData.h
@@ -12,16 +12,26 @@
 #include "nsRefreshDriver.h"
 
 namespace mozilla {
 namespace dom {
 
 class CoalescedMouseData final : public CoalescedInputData<WidgetMouseEvent>
 {
 public:
+  CoalescedMouseData()
+  {
+    MOZ_COUNT_CTOR(mozilla::dom::CoalescedMouseData);
+  }
+
+  ~CoalescedMouseData()
+  {
+    MOZ_COUNT_DTOR(mozilla::dom::CoalescedMouseData);
+  }
+
   void Coalesce(const WidgetMouseEvent& aEvent,
                 const ScrollableLayerGuid& aGuid,
                 const uint64_t& aInputBlockId);
 
   bool CanCoalesce(const WidgetMouseEvent& aEvent,
                    const ScrollableLayerGuid& aGuid,
                    const uint64_t& aInputBlockId);
 };
diff --git a/dom/ipc/TabChild.cpp b/dom/ipc/TabChild.cpp
--- a/dom/ipc/TabChild.cpp
+++ b/dom/ipc/TabChild.cpp
@@ -1595,47 +1595,59 @@ TabChild::RecvMouseEvent(const nsString&
                                          nsIDOMMouseEvent::MOZ_SOURCE_UNKNOWN,
                                          0 /* Use the default value here. */);
   return IPC_OK();
 }
 
 void
 TabChild::MaybeDispatchCoalescedMouseMoveEvents()
 {
-  if (!mCoalesceMouseMoveEvents || mCoalescedMouseData.IsEmpty()) {
+  if (!mCoalesceMouseMoveEvents) {
     return;
   }
-  const WidgetMouseEvent* event = mCoalescedMouseData.GetCoalescedEvent();
-  MOZ_ASSERT(event);
-  // Dispatch the coalesced mousemove event. Using RecvRealMouseButtonEvent to
-  // bypass the coalesce handling in RecvRealMouseMoveEvent.
-  RecvRealMouseButtonEvent(*event,
-                           mCoalescedMouseData.GetScrollableLayerGuid(),
-                           mCoalescedMouseData.GetInputBlockId());
+  for (auto iter = mCoalescedMouseData.Iter(); !iter.Done(); iter.Next()) {
+    CoalescedMouseData* data = iter.UserData();
+    if (!data || data->IsEmpty()) {
+      continue;
+    }
+    const WidgetMouseEvent* event = data->GetCoalescedEvent();
+    MOZ_ASSERT(event);
+    // Dispatch the coalesced mousemove event. Using RecvRealMouseButtonEvent to
+    // bypass the coalesce handling in RecvRealMouseMoveEvent.
+    RecvRealMouseButtonEvent(*event,
+                             data->GetScrollableLayerGuid(),
+                             data->GetInputBlockId());
+    data->Reset();
+  }
   if (mCoalescedMouseEventFlusher) {
-    mCoalescedMouseData.Reset();
     mCoalescedMouseEventFlusher->RemoveObserver();
   }
 }
 
 mozilla::ipc::IPCResult
 TabChild::RecvRealMouseMoveEvent(const WidgetMouseEvent& aEvent,
                                  const ScrollableLayerGuid& aGuid,
                                  const uint64_t& aInputBlockId)
 {
   if (mCoalesceMouseMoveEvents && mCoalescedMouseEventFlusher) {
-    if (mCoalescedMouseData.CanCoalesce(aEvent, aGuid, aInputBlockId)) {
-      mCoalescedMouseData.Coalesce(aEvent, aGuid, aInputBlockId);
+    CoalescedMouseData* data = nullptr;
+    mCoalescedMouseData.Get(aEvent.pointerId, &data);
+    if (!data) {
+      data = new CoalescedMouseData();
+      mCoalescedMouseData.Put(aEvent.pointerId, data);
+    }
+    if (data->CanCoalesce(aEvent, aGuid, aInputBlockId)) {
+      data->Coalesce(aEvent, aGuid, aInputBlockId);
       mCoalescedMouseEventFlusher->StartObserver();
       return IPC_OK();
     }
     // Can't coalesce current mousemove event. Dispatch the coalesced mousemove
     // event and coalesce the current one.
     MaybeDispatchCoalescedMouseMoveEvents();
-    mCoalescedMouseData.Coalesce(aEvent, aGuid, aInputBlockId);
+    data->Coalesce(aEvent, aGuid, aInputBlockId);
     mCoalescedMouseEventFlusher->StartObserver();
   } else if (!RecvRealMouseButtonEvent(aEvent, aGuid, aInputBlockId)) {
     return IPC_FAIL_NO_REASON(this);
   }
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
@@ -1669,16 +1681,21 @@ mozilla::ipc::IPCResult
 TabChild::RecvRealMouseButtonEvent(const WidgetMouseEvent& aEvent,
                                    const ScrollableLayerGuid& aGuid,
                                    const uint64_t& aInputBlockId)
 {
   if (aEvent.mMessage != eMouseMove) {
     // Flush the coalesced mousemove event before dispatching other mouse
     // events.
     MaybeDispatchCoalescedMouseMoveEvents();
+    if (aEvent.mMessage == eMouseEnterIntoWidget) {
+      mCoalescedMouseData.Put(aEvent.pointerId, new CoalescedMouseData());
+    } else if (aEvent.mMessage == eMouseExitFromWidget) {
+      mCoalescedMouseData.Remove(aEvent.pointerId);
+    }
   }
   // Mouse events like eMouseEnterIntoWidget, that are created in the parent
   // process EventStateManager code, have an input block id which they get from
   // the InputAPZContext in the parent process stack. However, they did not
   // actually go through the APZ code and so their mHandledByAPZ flag is false.
   // Since thos events didn't go through APZ, we don't need to send
   // notifications for them.
   bool pendingLayerization = false;
diff --git a/dom/ipc/TabChild.h b/dom/ipc/TabChild.h
--- a/dom/ipc/TabChild.h
+++ b/dom/ipc/TabChild.h
@@ -904,17 +904,17 @@ private:
   // be skipped to not flood child process.
   mozilla::TimeStamp mRepeatedKeyEventTime;
 
   // Similar to mRepeatedKeyEventTime, store the end time (from parent process)
   // of handling the last repeated wheel event so that in case event handling
   // takes time, some repeated events can be skipped to not flood child process.
   mozilla::TimeStamp mLastWheelProcessedTimeFromParent;
   mozilla::TimeDuration mLastWheelProcessingDuration;
-  CoalescedMouseData mCoalescedMouseData;
+  nsClassHashtable<nsUint32HashKey, CoalescedMouseData> mCoalescedMouseData;
   CoalescedWheelData mCoalescedWheelData;
   RefPtr<CoalescedMouseMoveFlusher> mCoalescedMouseEventFlusher;
 
   RefPtr<layers::IAPZCTreeManager> mApzcTreeManager;
 
   // The most recently seen layer observer epoch in RecvSetDocShellIsActive.
   uint64_t mLayerObserverEpoch;
 
