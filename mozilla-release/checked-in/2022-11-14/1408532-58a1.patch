# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1509709758 14400
#      Fri Nov 03 07:49:18 2017 -0400
# Node ID 05676eb12f345f998cbd7e3fc0266b80f6ead6c0
# Parent  5c76b349ce097efe7ceab7c5d1c17ca12c075c85
Bug 1408532 - Ensure ImageBridgeParent cleans up old actors if the process ID is reused. r=dvander

diff --git a/gfx/layers/ipc/ImageBridgeParent.cpp b/gfx/layers/ipc/ImageBridgeParent.cpp
--- a/gfx/layers/ipc/ImageBridgeParent.cpp
+++ b/gfx/layers/ipc/ImageBridgeParent.cpp
@@ -64,36 +64,37 @@ ImageBridgeParent::Setup()
 ImageBridgeParent::ImageBridgeParent(MessageLoop* aLoop,
                                      ProcessId aChildProcessId)
   : mMessageLoop(aLoop)
   , mClosed(false)
   , mCompositorThreadHolder(CompositorThreadHolder::GetSingleton())
 {
   MOZ_ASSERT(NS_IsMainThread());
 
-  // creates the map only if it has not been created already, so it is safe
-  // with several bridges
-  {
-    MonitorAutoLock lock(*sImageBridgesLock);
-    sImageBridges[aChildProcessId] = this;
-  }
   SetOtherProcessId(aChildProcessId);
 }
 
 ImageBridgeParent::~ImageBridgeParent()
 {
 }
 
 /* static */ ImageBridgeParent*
 ImageBridgeParent::CreateSameProcess()
 {
+  base::ProcessId pid = base::GetCurrentProcId();
   RefPtr<ImageBridgeParent> parent =
-    new ImageBridgeParent(CompositorThreadHolder::Loop(), base::GetCurrentProcId());
+    new ImageBridgeParent(CompositorThreadHolder::Loop(), pid);
   parent->mSelfRef = parent;
 
+  {
+    MonitorAutoLock lock(*sImageBridgesLock);
+    MOZ_RELEASE_ASSERT(sImageBridges.count(pid) == 0);
+    sImageBridges[pid] = parent;
+  }
+
   sImageBridgeParentSingleton = parent;
   return parent;
 }
 
 /* static */ bool
 ImageBridgeParent::CreateForGPUProcess(Endpoint<PImageBridgeParent>&& aEndpoint)
 {
   MOZ_ASSERT(XRE_GetProcessType() == GeckoProcessType_GPU);
@@ -240,16 +241,38 @@ ImageBridgeParent::CreateForContent(Endp
 }
 
 void
 ImageBridgeParent::Bind(Endpoint<PImageBridgeParent>&& aEndpoint)
 {
   if (!aEndpoint.Bind(this))
     return;
   mSelfRef = this;
+
+  // If the child process ID was reused by the OS before the ImageBridgeParent
+  // object was destroyed, we need to clean it up first.
+  RefPtr<ImageBridgeParent> oldActor;
+  {
+    MonitorAutoLock lock(*sImageBridgesLock);
+    ImageBridgeMap::const_iterator i = sImageBridges.find(OtherPid());
+    if (i != sImageBridges.end()) {
+      oldActor = i->second;
+    }
+  }
+
+  // We can't hold the lock during Close because it erases itself from the map.
+  if (oldActor) {
+    MOZ_RELEASE_ASSERT(!oldActor->mClosed);
+    oldActor->Close();
+  }
+
+  {
+    MonitorAutoLock lock(*sImageBridgesLock);
+    sImageBridges[OtherPid()] = this;
+  }
 }
 
 mozilla::ipc::IPCResult ImageBridgeParent::RecvWillClose()
 {
   // If there is any texture still alive we have to force it to deallocate the
   // device data (GL textures, etc.) now because shortly after SenStop() returns
   // on the child side the widget will be destroyed along with it's associated
   // GL context.
