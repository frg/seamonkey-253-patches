# HG changeset patch
# User Marco Castelluccio <mcastelluccio@mozilla.com>
# Date 1512779914 -3600
#      Sat Dec 09 01:38:34 2017 +0100
# Node ID bb87be1f7492c21b9d98e4844b7538febfa91f63
# Parent  53ed0617f39c775cebfad9cec7a903943f773e26
Bug 1424227 - Support merging multiple input info files into a single output info file. r=chmanchester

diff --git a/python/mozbuild/mozbuild/codecoverage/lcov_rewriter.py b/python/mozbuild/mozbuild/codecoverage/lcov_rewriter.py
--- a/python/mozbuild/mozbuild/codecoverage/lcov_rewriter.py
+++ b/python/mozbuild/mozbuild/codecoverage/lcov_rewriter.py
@@ -240,51 +240,53 @@ class LcovFile(object):
       'BRDA': 3,
       'BRF': 0,
       'BRH': 0,
       'DA': 2,
       'LH': 0,
       'LF': 0,
     }
 
-    def __init__(self, lcov_fh):
-        self.lcov_fh = lcov_fh
+    def __init__(self, lcov_paths):
+        self.lcov_paths = lcov_paths
 
     def iterate_records(self, rewrite_source=None):
         current_source_file = None
         current_preprocessed = False
         current_lines = []
-        for line in self.lcov_fh:
-            line = line.rstrip()
-            if not line:
-                continue
+        for lcov_path in self.lcov_paths:
+            with open(lcov_path) as lcov_fh:
+                for line in lcov_fh:
+                    line = line.rstrip()
+                    if not line:
+                        continue
 
-            if line == 'end_of_record':
-                # We skip records that we couldn't rewrite, that is records for which
-                # rewrite_url returns None.
-                if current_source_file != None:
-                    yield (current_source_file, current_preprocessed, current_lines)
-                current_source_file = None
-                current_preprocessed = False
-                current_lines = []
-                continue
+                    if line == 'end_of_record':
+                        # We skip records that we couldn't rewrite, that is records for which
+                        # rewrite_url returns None.
+                        if current_source_file != None:
+                            yield (current_source_file, current_preprocessed, current_lines)
+                        current_source_file = None
+                        current_preprocessed = False
+                        current_lines = []
+                        continue
 
-            colon = line.find(':')
-            prefix = line[:colon]
+                    colon = line.find(':')
+                    prefix = line[:colon]
 
-            if prefix == 'SF':
-                sf = line[(colon + 1):]
-                res = rewrite_source(sf) if rewrite_source is not None else (sf, False)
-                if res is None:
-                    current_lines.append(line)
-                else:
-                    current_source_file, current_preprocessed = res
-                    current_lines.append('SF:' + current_source_file)
-            else:
-                current_lines.append(line)
+                    if prefix == 'SF':
+                        sf = line[(colon + 1):]
+                        res = rewrite_source(sf) if rewrite_source is not None else (sf, False)
+                        if res is None:
+                            current_lines.append(line)
+                        else:
+                            current_source_file, current_preprocessed = res
+                            current_lines.append('SF:' + current_source_file)
+                    else:
+                        current_lines.append(line)
 
     def parse_record(self, record_content):
         self.current_record = LcovRecord()
 
         for line in record_content:
             colon = line.find(':')
 
             prefix = line[:colon]
@@ -659,20 +661,17 @@ class UrlFinder(object):
 class LcovFileRewriter(object):
     # Class for partial parses of LCOV format and rewriting to resolve urls
     # and preprocessed file lines.
     def __init__(self, appdir, gredir, extra_chrome_manifests):
         self.topobjdir = buildconfig.topobjdir
         self.url_finder = UrlFinder(appdir, gredir, extra_chrome_manifests)
         self.pp_rewriter = RecordRewriter()
 
-    def rewrite_file(self, in_path, output_suffix):
-        in_path = os.path.abspath(in_path)
-        out_path = in_path + output_suffix
-
+    def rewrite_files(self, in_paths, output_file, output_suffix):
         unknowns = set()
         found_valid = False
 
         def rewrite_source(url):
             try:
                 res = self.url_finder.rewrite_url(url)
                 if res is None:
                     return None
@@ -689,19 +688,27 @@ class LcovFileRewriter(object):
                 obj_path = os.path.join(self.topobjdir, objdir_file)
                 with open(obj_path) as fh:
                     self.pp_rewriter.populate_pp_info(fh, source_file)
 
             found_valid = True
 
             return source_file, preprocessed
 
-        with open(in_path) as fh, open(out_path, 'w+') as out_fh:
-            lcov_file = LcovFile(fh)
-            lcov_file.print_file(out_fh, rewrite_source, self.pp_rewriter.rewrite_record)
+        in_paths = [os.path.abspath(in_path) for in_path in in_paths]
+
+        if output_file:
+            lcov_file = LcovFile(in_paths)
+            with open(output_file, 'w+') as out_fh:
+                lcov_file.print_file(out_fh, rewrite_source, self.pp_rewriter.rewrite_record)
+        else:
+            for in_path in in_paths:
+                lcov_file = LcovFile([in_path])
+                with open(in_path + output_suffix, 'w+') as out_fh:
+                    lcov_file.print_file(out_fh, rewrite_source, self.pp_rewriter.rewrite_record)
 
         if not found_valid:
             print("WARNING: No valid records found in %s" % in_path)
             return
 
 
 def main():
     parser = ArgumentParser(description="Given a set of gcov .info files produced "
@@ -715,16 +722,18 @@ def main():
     parser.add_argument("--gre-dir", default="dist/bin/",
                         help="Prefix of the gre dir in use. This is used to map "
                              "urls starting with resource://gre. It may differ by "
                              "app, but defaults to the valid value for firefox.")
     parser.add_argument("--output-suffix", default=".out",
                         help="The suffix to append to output files.")
     parser.add_argument("--extra-chrome-manifests", nargs='+',
                         help="Paths to files containing extra chrome registration.")
+    parser.add_argument("--output-file", default="",
+                        help="The output file where the results are merged. Leave empty to make the rewriter not merge files.")
     parser.add_argument("files", nargs='+',
                         help="The set of files to process.")
 
     args = parser.parse_args()
     if not args.extra_chrome_manifests:
         extra_path = os.path.join(buildconfig.topobjdir, '_tests',
                                   'extra.manifest')
         if os.path.isfile(extra_path):
@@ -735,13 +744,12 @@ def main():
 
     files = []
     for f in args.files:
         if os.path.isdir(f):
             files += [os.path.join(f, e) for e in os.listdir(f)]
         else:
             files.append(f)
 
-    for f in files:
-        rewriter.rewrite_file(f, args.output_suffix)
+    rewriter.rewrite_files(files, args.output_file, args.output_suffix)
 
 if __name__ == '__main__':
     main()
