# HG changeset patch
# User Edouard Oger <eoger@fastmail.com>
# Date 1510775622 18000
# Node ID 55ebc5117e6ae26a7acd1dcf3a431021e6598c5b
# Parent  7a693ada871ff5e7dab0b8a17d477c8726b30b35
Bug 1416842 - Allow fetch to reject with nsresult in chrome code. r=bkelly

MozReview-Commit-ID: FD2NUJZtAhT

diff --git a/dom/cache/TypeUtils.cpp b/dom/cache/TypeUtils.cpp
--- a/dom/cache/TypeUtils.cpp
+++ b/dom/cache/TypeUtils.cpp
@@ -252,17 +252,18 @@ TypeUtils::ToCacheQueryParams(CacheQuery
     aOut.cacheName() = NS_LITERAL_STRING("");
   }
 }
 
 already_AddRefed<Response>
 TypeUtils::ToResponse(const CacheResponse& aIn)
 {
   if (aIn.type() == ResponseType::Error) {
-    RefPtr<InternalResponse> error = InternalResponse::NetworkError();
+    // We don't bother tracking the internal error code for cached responses...
+    RefPtr<InternalResponse> error = InternalResponse::NetworkError(NS_ERROR_FAILURE);
     RefPtr<Response> r = new Response(GetGlobalObject(), error, nullptr);
     return r.forget();
   }
 
   RefPtr<InternalResponse> ir = new InternalResponse(aIn.status(),
                                                        aIn.statusText());
   ir->SetURLList(aIn.urlList());
 
diff --git a/dom/fetch/Fetch.cpp b/dom/fetch/Fetch.cpp
--- a/dom/fetch/Fetch.cpp
+++ b/dom/fetch/Fetch.cpp
@@ -254,26 +254,28 @@ private:
 };
 
 class MainThreadFetchResolver final : public FetchDriverObserver
 {
   RefPtr<Promise> mPromise;
   RefPtr<Response> mResponse;
   RefPtr<FetchObserver> mFetchObserver;
   RefPtr<AbortSignal> mSignal;
+  const bool mMozErrors;
 
   nsCOMPtr<nsILoadGroup> mLoadGroup;
 
   NS_DECL_OWNINGTHREAD
 public:
   MainThreadFetchResolver(Promise* aPromise, FetchObserver* aObserver,
-                          AbortSignal* aSignal)
+                          AbortSignal* aSignal, bool aMozErrors)
     : mPromise(aPromise)
     , mFetchObserver(aObserver)
     , mSignal(aSignal)
+    , mMozErrors(aMozErrors)
   {}
 
   void
   OnResponseAvailableInternal(InternalResponse* aResponse) override;
 
   void SetLoadGroup(nsILoadGroup* aLoadGroup)
   {
     mLoadGroup = aLoadGroup;
@@ -435,18 +437,19 @@ FetchRequest(nsIGlobalObject* aGlobal, c
       }
       nsresult rv = NS_NewLoadGroup(getter_AddRefs(loadGroup), principal);
       if (NS_WARN_IF(NS_FAILED(rv))) {
         aRv.Throw(rv);
         return nullptr;
       }
     }
 
+    bool mozErrors = aInit.mMozErrors.WasPassed() ? aInit.mMozErrors.Value() : false;
     RefPtr<MainThreadFetchResolver> resolver =
-      new MainThreadFetchResolver(p, observer, signal);
+      new MainThreadFetchResolver(p, observer, signal, mozErrors);
     RefPtr<FetchDriver> fetch =
       new FetchDriver(r, principal, loadGroup,
                       aGlobal->EventTargetFor(TaskCategory::Other), isTrackingFetch);
     fetch->SetDocument(doc);
     resolver->SetLoadGroup(loadGroup);
     aRv = fetch->Fetch(signal, resolver);
     if (NS_WARN_IF(aRv.Failed())) {
       return nullptr;
@@ -489,16 +492,21 @@ MainThreadFetchResolver::OnResponseAvail
     nsCOMPtr<nsIGlobalObject> go = mPromise->GetParentObject();
     mResponse = new Response(go, aResponse, mSignal);
     mPromise->MaybeResolve(mResponse);
   } else {
     if (mFetchObserver) {
       mFetchObserver->SetState(FetchState::Errored);
     }
 
+    if (mMozErrors) {
+      mPromise->MaybeReject(aResponse->GetErrorCode());
+      return;
+    }
+
     ErrorResult result;
     result.ThrowTypeError<MSG_FETCH_FAILED>();
     mPromise->MaybeReject(result);
   }
 }
 
 bool
 MainThreadFetchResolver::NeedOnDataAvailable()
diff --git a/dom/fetch/FetchDriver.cpp b/dom/fetch/FetchDriver.cpp
--- a/dom/fetch/FetchDriver.cpp
+++ b/dom/fetch/FetchDriver.cpp
@@ -123,18 +123,19 @@ FetchDriver::Fetch(AbortSignal* aSignal,
     if (aSignal->Aborted()) {
       Abort();
       return NS_OK;
     }
 
     Follow(aSignal);
   }
 
-  if (NS_FAILED(HttpFetch())) {
-    FailWithNetworkError();
+  rv = HttpFetch();
+  if (NS_FAILED(rv)) {
+    FailWithNetworkError(rv);
   }
 
   // Any failure is handled by FailWithNetworkError notifying the aObserver.
   return NS_OK;
 }
 
 // This function implements the "HTTP Fetch" algorithm from the Fetch spec.
 // Functionality is often split between here, the CORS listener proxy and the
@@ -457,20 +458,20 @@ FetchDriver::BeginAndGetFilteredResponse
     mResponseAvailableCalled = true;
   #endif
   }
 
   return filteredResponse.forget();
 }
 
 void
-FetchDriver::FailWithNetworkError()
+FetchDriver::FailWithNetworkError(nsresult rv)
 {
   workers::AssertIsOnMainThread();
-  RefPtr<InternalResponse> error = InternalResponse::NetworkError();
+  RefPtr<InternalResponse> error = InternalResponse::NetworkError(rv);
   if (mObserver) {
     mObserver->OnResponseAvailable(error);
 #ifdef DEBUG
     mResponseAvailableCalled = true;
 #endif
     mObserver->OnResponseEnd(FetchDriverObserver::eByNetworking);
     mObserver = nullptr;
   }
@@ -491,17 +492,17 @@ FetchDriver::OnStartRequest(nsIRequest* 
   if (!mChannel) {
     MOZ_ASSERT(!mObserver);
     return NS_BINDING_ABORTED;
   }
 
   nsresult rv;
   aRequest->GetStatus(&rv);
   if (NS_FAILED(rv)) {
-    FailWithNetworkError();
+    FailWithNetworkError(rv);
     return rv;
   }
 
   // We should only get to the following code once.
   MOZ_ASSERT(!mPipeOutputStream);
   MOZ_ASSERT(mObserver);
 
   mNeedToObserveOnDataAvailable = mObserver->NeedOnDataAvailable();
@@ -521,17 +522,17 @@ FetchDriver::OnStartRequest(nsIRequest* 
 
   if (httpChannel) {
     uint32_t responseStatus;
     rv = httpChannel->GetResponseStatus(&responseStatus);
     MOZ_ASSERT(NS_SUCCEEDED(rv));
 
     if (mozilla::net::nsHttpChannel::IsRedirectStatus(responseStatus)) {
       if (mRequest->GetRedirectMode() == RequestRedirect::Error) {
-        FailWithNetworkError();
+        FailWithNetworkError(NS_BINDING_ABORTED);
         return NS_BINDING_FAILED;
       }
       if (mRequest->GetRedirectMode() == RequestRedirect::Manual) {
         foundOpaqueRedirect = true;
       }
     }
 
     nsAutoCString statusText;
@@ -592,36 +593,36 @@ FetchDriver::OnStartRequest(nsIRequest* 
   nsCOMPtr<nsIInputStream> pipeInputStream;
   rv = NS_NewPipe(getter_AddRefs(pipeInputStream),
                   getter_AddRefs(mPipeOutputStream),
                   0, /* default segment size */
                   UINT32_MAX /* infinite pipe */,
                   true /* non-blocking input, otherwise you deadlock */,
                   false /* blocking output, since the pipe is 'in'finite */ );
   if (NS_WARN_IF(NS_FAILED(rv))) {
-    FailWithNetworkError();
+    FailWithNetworkError(rv);
     // Cancel request.
     return rv;
   }
   response->SetBody(pipeInputStream, contentLength);
 
   response->InitChannelInfo(channel);
 
   nsCOMPtr<nsIURI> channelURI;
   rv = channel->GetURI(getter_AddRefs(channelURI));
   if (NS_WARN_IF(NS_FAILED(rv))) {
-    FailWithNetworkError();
+    FailWithNetworkError(rv);
     // Cancel request.
     return rv;
   }
 
   nsCOMPtr<nsILoadInfo> loadInfo;
   rv = channel->GetLoadInfo(getter_AddRefs(loadInfo));
   if (NS_WARN_IF(NS_FAILED(rv))) {
-    FailWithNetworkError();
+    FailWithNetworkError(rv);
     return rv;
   }
 
   // Propagate any tainting from the channel back to our response here.  This
   // step is not reflected in the spec because the spec is written such that
   // FetchEvent.respondWith() just passes the already-tainted Response back to
   // the outer fetch().  In gecko, however, we serialize the Response through
   // the channel and must regenerate the tainting from the channel in the
@@ -629,17 +630,17 @@ FetchDriver::OnStartRequest(nsIRequest* 
   mRequest->MaybeIncreaseResponseTainting(loadInfo->GetTainting());
 
   // Resolves fetch() promise which may trigger code running in a worker.  Make
   // sure the Response is fully initialized before calling this.
   mResponse = BeginAndGetFilteredResponse(response, foundOpaqueRedirect);
   if (NS_WARN_IF(!mResponse)) {
     // Fail to generate a paddingInfo for opaque response.
     MOZ_DIAGNOSTIC_ASSERT(mResponse->Type() == ResponseType::Opaque);
-    FailWithNetworkError();
+    FailWithNetworkError(NS_ERROR_UNEXPECTED);
     return rv;
   }
 
   // From "Main Fetch" step 19: SRI-part1.
   if (ShouldCheckSRI(mRequest, mResponse) && mSRIMetadata.IsEmpty()) {
     nsIConsoleReportCollector* reporter = nullptr;
     if (mObserver) {
       reporter = mObserver->GetReporter();
@@ -657,17 +658,17 @@ FetchDriver::OnStartRequest(nsIRequest* 
                                                 reporter);
 
     // Do not retarget off main thread when using SRI API.
     return NS_OK;
   }
 
   nsCOMPtr<nsIEventTarget> sts = do_GetService(NS_STREAMTRANSPORTSERVICE_CONTRACTID, &rv);
   if (NS_WARN_IF(NS_FAILED(rv))) {
-    FailWithNetworkError();
+    FailWithNetworkError(rv);
     // Cancel request.
     return rv;
   }
 
   // Try to retarget off main thread.
   if (nsCOMPtr<nsIThreadRetargetableRequest> rr = do_QueryInterface(aRequest)) {
     Unused << NS_WARN_IF(NS_FAILED(rr->RetargetDeliveryTo(sts)));
   }
@@ -833,17 +834,17 @@ FetchDriver::OnStopRequest(nsIRequest* a
       if (mDocument && mDocument->GetDocumentURI()) {
         mDocument->GetDocumentURI()->GetAsciiSpec(sourceUri);
       } else if (!mWorkerScript.IsEmpty()) {
         sourceUri.Assign(mWorkerScript);
       }
       nsresult rv = mSRIDataVerifier->Verify(mSRIMetadata, channel, sourceUri,
                                              reporter);
       if (NS_FAILED(rv)) {
-        FailWithNetworkError();
+        FailWithNetworkError(rv);
         // Cancel request.
         return rv;
       }
     }
 
     if (mPipeOutputStream) {
       mPipeOutputStream->Close();
     }
diff --git a/dom/fetch/FetchDriver.h b/dom/fetch/FetchDriver.h
--- a/dom/fetch/FetchDriver.h
+++ b/dom/fetch/FetchDriver.h
@@ -158,17 +158,17 @@ private:
 
   nsresult HttpFetch();
   // Returns the filtered response sent to the observer.
   already_AddRefed<InternalResponse>
   BeginAndGetFilteredResponse(InternalResponse* aResponse,
                               bool aFoundOpaqueRedirect);
   // Utility since not all cases need to do any post processing of the filtered
   // response.
-  void FailWithNetworkError();
+  void FailWithNetworkError(nsresult rv);
 
   void SetRequestHeaders(nsIHttpChannel* aChannel) const;
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // mozilla_dom_FetchDriver_h
diff --git a/dom/fetch/FetchTypes.ipdlh b/dom/fetch/FetchTypes.ipdlh
--- a/dom/fetch/FetchTypes.ipdlh
+++ b/dom/fetch/FetchTypes.ipdlh
@@ -49,13 +49,14 @@ struct IPCInternalResponse
   uint32_t status;
   nsCString statusText;
   HeadersEntry[] headers;
   HeadersGuardEnum headersGuard;
   IPCChannelInfo channelInfo;
   OptionalPrincipalInfo principalInfo;
   OptionalIPCStream body;
   int64_t bodySize;
+  nsresult errorCode;
 };
 
 
 } // namespace ipc
 } // namespace mozilla
diff --git a/dom/fetch/InternalResponse.cpp b/dom/fetch/InternalResponse.cpp
--- a/dom/fetch/InternalResponse.cpp
+++ b/dom/fetch/InternalResponse.cpp
@@ -30,23 +30,24 @@ InternalResponse::InternalResponse(uint1
                                    const nsACString& aStatusText,
                                    RequestCredentials aCredentialsMode)
   : mType(ResponseType::Default)
   , mStatus(aStatus)
   , mStatusText(aStatusText)
   , mHeaders(new InternalHeaders(HeadersGuardEnum::Response))
   , mBodySize(UNKNOWN_BODY_SIZE)
   , mPaddingSize(UNKNOWN_PADDING_SIZE)
+  , mErrorCode(NS_OK)
   , mCredentialsMode(aCredentialsMode) {}
 
 already_AddRefed<InternalResponse>
 InternalResponse::FromIPC(const IPCInternalResponse& aIPCResponse)
 {
   if (aIPCResponse.type() == ResponseType::Error) {
-    return InternalResponse::NetworkError();
+    return InternalResponse::NetworkError(aIPCResponse.errorCode());
   }
 
   RefPtr<InternalResponse> response =
     new InternalResponse(aIPCResponse.status(),
                          aIPCResponse.statusText());
 
   response->SetURLList(aIPCResponse.urlList());
 
diff --git a/dom/fetch/InternalResponse.h b/dom/fetch/InternalResponse.h
--- a/dom/fetch/InternalResponse.h
+++ b/dom/fetch/InternalResponse.h
@@ -51,23 +51,25 @@ public:
   {
     eCloneInputStream,
     eDontCloneInputStream,
   };
 
   already_AddRefed<InternalResponse> Clone(CloneType eCloneType);
 
   static already_AddRefed<InternalResponse>
-  NetworkError()
+  NetworkError(nsresult aRv)
   {
+    MOZ_DIAGNOSTIC_ASSERT(NS_FAILED(aRv));
     RefPtr<InternalResponse> response = new InternalResponse(0, EmptyCString());
     ErrorResult result;
     response->Headers()->SetGuard(HeadersGuardEnum::Immutable, result);
     MOZ_ASSERT(!result.Failed());
     response->mType = ResponseType::Error;
+    response->mErrorCode = aRv;
     return response.forget();
   }
 
   already_AddRefed<InternalResponse>
   OpaqueResponse();
 
   already_AddRefed<InternalResponse>
   OpaqueRedirectResponse();
@@ -279,16 +281,22 @@ public:
   }
 
   bool
   IsRedirected() const
   {
     return mURLList.Length() > 1;
   }
 
+  nsresult
+  GetErrorCode() const
+  {
+    return mErrorCode;
+  }
+
   // Takes ownership of the principal info.
   void
   SetPrincipalInfo(UniquePtr<mozilla::ipc::PrincipalInfo> aPrincipalInfo);
 
   LoadTainting
   GetTainting() const;
 
   already_AddRefed<InternalResponse>
@@ -315,16 +323,17 @@ private:
   const nsCString mStatusText;
   RefPtr<InternalHeaders> mHeaders;
   nsCOMPtr<nsIInputStream> mBody;
   int64_t mBodySize;
   // It's used to passed to the CacheResponse to generate padding size. Once, we
   // generate the padding size for resposne, we don't need it anymore.
   Maybe<uint32_t> mPaddingInfo;
   int64_t mPaddingSize;
+  nsresult mErrorCode;
 
   RequestCredentials mCredentialsMode;
 
 public:
   static const int64_t UNKNOWN_BODY_SIZE = -1;
   static const int64_t UNKNOWN_PADDING_SIZE = -1;
 private:
   ChannelInfo mChannelInfo;
diff --git a/dom/fetch/Response.cpp b/dom/fetch/Response.cpp
--- a/dom/fetch/Response.cpp
+++ b/dom/fetch/Response.cpp
@@ -82,17 +82,17 @@ Response::~Response()
 {
   mozilla::DropJSObjects(this);
 }
 
 /* static */ already_AddRefed<Response>
 Response::Error(const GlobalObject& aGlobal)
 {
   nsCOMPtr<nsIGlobalObject> global = do_QueryInterface(aGlobal.GetAsSupports());
-  RefPtr<InternalResponse> error = InternalResponse::NetworkError();
+  RefPtr<InternalResponse> error = InternalResponse::NetworkError(NS_ERROR_FAILURE);
   RefPtr<Response> r = new Response(global, error, nullptr);
   return r.forget();
 }
 
 /* static */ already_AddRefed<Response>
 Response::Redirect(const GlobalObject& aGlobal, const nsAString& aUrl,
                    uint16_t aStatus, ErrorResult& aRv)
 {
diff --git a/dom/tests/mochitest/fetch/test_fetch_basic.js b/dom/tests/mochitest/fetch/test_fetch_basic.js
--- a/dom/tests/mochitest/fetch/test_fetch_basic.js
+++ b/dom/tests/mochitest/fetch/test_fetch_basic.js
@@ -89,16 +89,26 @@ function testNonGetBlobURL() {
         ok(e instanceof TypeError, "Blob URL with non-GET request should get a TypeError");
       });
     })
   ).then(function() {
     URL.revokeObjectURL(url);
   });
 }
 
+function testMozErrors() {
+  // mozErrors shouldn't be available to content and be ignored.
+  return fetch("http://localhost:4/should/fail", { mozErrors: true }).then(res => {
+    ok(false, "Request should not succeed");
+  }).catch(err => {
+    ok(err instanceof TypeError);
+  });
+}
+
 function runTest() {
   return Promise.resolve()
     .then(testAboutURL)
     .then(testDataURL)
     .then(testSameOriginBlobURL)
     .then(testNonGetBlobURL)
+    .then(testMozErrors)
     // Put more promise based tests here.
 }
diff --git a/dom/tests/unit/test_Fetch.js b/dom/tests/unit/test_Fetch.js
--- a/dom/tests/unit/test_Fetch.js
+++ b/dom/tests/unit/test_Fetch.js
@@ -205,16 +205,28 @@ add_test(function test_getTestFailedConn
     do_throw("Request should not succeed");
   }).catch(err => {
     Assert.equal(true, err instanceof TypeError);
     do_test_finished();
     run_next_test();
   });
 });
 
+add_test(function test_mozError() {
+  do_test_pending();
+  // try a server that's not there
+  fetch("http://localhost:4/should/fail", { mozErrors: true }).then(response => {
+    do_throw("Request should not succeed");
+  }).catch(err => {
+    Assert.equal(err.result, Cr.NS_ERROR_CONNECTION_REFUSED);
+    do_test_finished();
+    run_next_test();
+  });
+});
+
 // test POSTing some JSON data
 add_test(function test_PostJSONData() {
   do_test_pending();
 
   let testData = createTestData("/postJSONData");
   testData.request.body = "{\"foo\": \"bar\"}";
 
   server.registerPathHandler(testData.testPath, function(aRequest, aResponse) {
diff --git a/dom/webidl/Request.webidl b/dom/webidl/Request.webidl
--- a/dom/webidl/Request.webidl
+++ b/dom/webidl/Request.webidl
@@ -46,16 +46,21 @@ dictionary RequestInit {
   USVString referrer;
   ReferrerPolicy referrerPolicy;
   RequestMode mode;
   RequestCredentials credentials;
   RequestCache cache;
   RequestRedirect redirect;
   DOMString integrity;
 
+  // If a main-thread fetch() promise rejects, the error passed will be a
+  // nsresult code.
+  [ChromeOnly]
+  boolean mozErrors;
+
   AbortSignal? signal;
 
   [Func="FetchObserver::IsEnabled"]
   ObserverCallback observe;
 };
 
 // Gecko currently does not ship RequestContext, so please don't use it in IDL
 // that is exposed to script.
