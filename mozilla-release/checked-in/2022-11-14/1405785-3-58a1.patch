# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1505483667 -3600
#      Fri Sep 15 14:54:27 2017 +0100
# Node ID 9602570c97b40a66f35156a1101ffb9296f7470a
# Parent  c5e8b8f250285e9822064cab193b039962f67cef
Bug 1405785, part 3 - Document and rename nsSVGRenderingObserver::InvalidateViaReferencedElement to OnNonDOMMutationRenderingChange. r=longsonr

MozReview-Commit-ID: APN6iFoFDbm

diff --git a/layout/svg/SVGObserverUtils.cpp b/layout/svg/SVGObserverUtils.cpp
--- a/layout/svg/SVGObserverUtils.cpp
+++ b/layout/svg/SVGObserverUtils.cpp
@@ -93,17 +93,17 @@ nsSVGRenderingObserver::GetReferencedFra
     if (aOK) {
       *aOK = false;
     }
   }
   return nullptr;
 }
 
 void
-nsSVGRenderingObserver::InvalidateViaReferencedElement()
+nsSVGRenderingObserver::OnNonDOMMutationRenderingChange()
 {
   mInObserverList = false;
   OnRenderingChange();
 }
 
 void
 nsSVGRenderingObserver::NotifyEvictedFromRenderingObserverList()
 {
@@ -796,17 +796,17 @@ nsSVGRenderingObserverList::InvalidateAl
   AutoTArray<nsSVGRenderingObserver*,10> observers;
 
   for (auto it = mObservers.Iter(); !it.Done(); it.Next()) {
     observers.AppendElement(it.Get()->GetKey());
   }
   mObservers.Clear();
 
   for (uint32_t i = 0; i < observers.Length(); ++i) {
-    observers[i]->InvalidateViaReferencedElement();
+    observers[i]->OnNonDOMMutationRenderingChange();
   }
 }
 
 void
 nsSVGRenderingObserverList::InvalidateAllForReflow()
 {
   if (mObservers.Count() == 0)
     return;
@@ -817,17 +817,17 @@ nsSVGRenderingObserverList::InvalidateAl
     nsSVGRenderingObserver* obs = it.Get()->GetKey();
     if (obs->ObservesReflow()) {
       observers.AppendElement(obs);
       it.Remove();
     }
   }
 
   for (uint32_t i = 0; i < observers.Length(); ++i) {
-    observers[i]->InvalidateViaReferencedElement();
+    observers[i]->OnNonDOMMutationRenderingChange();
   }
 }
 
 void
 nsSVGRenderingObserverList::RemoveAll()
 {
   AutoTArray<nsSVGRenderingObserver*,10> observers;
 
diff --git a/layout/svg/SVGObserverUtils.h b/layout/svg/SVGObserverUtils.h
--- a/layout/svg/SVGObserverUtils.h
+++ b/layout/svg/SVGObserverUtils.h
@@ -57,17 +57,25 @@ public:
     {}
 
   // nsIMutationObserver
   NS_DECL_NSIMUTATIONOBSERVER_ATTRIBUTECHANGED
   NS_DECL_NSIMUTATIONOBSERVER_CONTENTAPPENDED
   NS_DECL_NSIMUTATIONOBSERVER_CONTENTINSERTED
   NS_DECL_NSIMUTATIONOBSERVER_CONTENTREMOVED
 
-  void InvalidateViaReferencedElement();
+  /**
+   * Called when non-DOM-mutation changes to the observed element should likely
+   * cause the rendering of our observer to change.  This includes changes to
+   * CSS computed values, but also changes to rendering observers that the
+   * observed element itself may have (for example, when we're being used to
+   * observe an SVG pattern, and an element in that pattern references and
+   * observes a gradient that has changed).
+   */
+  void OnNonDOMMutationRenderingChange();
 
   // When a nsSVGRenderingObserver list gets forcibly cleared, it uses this
   // callback to notify every observer that's cleared from it, so they can
   // react.
   void NotifyEvictedFromRenderingObserverList();
 
   bool IsInObserverList() const { return mInObserverList; }
 
@@ -381,17 +389,17 @@ private:
   nsIFrame* mFrame;
 };
 
 /**
  * A manager for one-shot nsSVGRenderingObserver tracking.
  * nsSVGRenderingObservers can be added or removed. They are not strongly
  * referenced so an observer must be removed before it dies.
  * When InvalidateAll is called, all outstanding references get
- * InvalidateViaReferencedElement()
+ * OnNonDOMMutationRenderingChange()
  * called on them and the list is cleared. The intent is that
  * the observer will force repainting of whatever part of the document
  * is needed, and then at paint time the observer will do a clean lookup
  * of the referenced element and [re-]add itself to the element's observer list.
  *
  * InvalidateAll must be called before this object is destroyed, i.e.
  * before the referenced frame is destroyed. This should normally happen
  * via nsSVGContainerFrame::RemoveFrame, since only frames in the frame
