# HG changeset patch
# User Blake Kaplan <mrbkap@gmail.com>
# Date 1504914634 25200
#      Fri Sep 08 16:50:34 2017 -0700
# Node ID 785910c8bee719b14fd14a2fe64b260429449b3a
# Parent  f965317299df23f2a69f3ab51c2f502eca04b44b
Bug 1398393 - Set Servo thread state on cooperative threads. r=billm

By doing this we avoid triggering assertions in the Servo code that ensure
we have registered the thread with Servo and set the proper state on it.

MozReview-Commit-ID: K6qHrYoQDLm

diff --git a/layout/style/ServoBindingList.h b/layout/style/ServoBindingList.h
--- a/layout/style/ServoBindingList.h
+++ b/layout/style/ServoBindingList.h
@@ -544,16 +544,18 @@ SERVO_BINDING_FUNC(Servo_ComputedValues_
 // to RawServoStyleRule.
 SERVO_BINDING_FUNC(Servo_ComputedValues_GetStyleRuleList, void,
                    ServoStyleContextBorrowed values,
                    RawGeckoServoStyleRuleListBorrowedMut rules)
 
 // Initialize Servo components. Should be called exactly once at startup.
 SERVO_BINDING_FUNC(Servo_Initialize, void,
                    RawGeckoURLExtraData* dummy_url_data)
+// Initialize Servo on a cooperative Quantum DOM thread.
+SERVO_BINDING_FUNC(Servo_InitializeCooperativeThread, void);
 // Shut down Servo components. Should be called exactly once at shutdown.
 SERVO_BINDING_FUNC(Servo_Shutdown, void)
 
 // Restyle and change hints.
 SERVO_BINDING_FUNC(Servo_NoteExplicitHints, void, RawGeckoElementBorrowed element,
                    nsRestyleHint restyle_hint, nsChangeHint change_hint)
 // We'd like to return `nsChangeHint` here, but bindgen bitfield enums don't
 // work as return values with the Linux 32-bit ABI at the moment because
diff --git a/xpcom/threads/CooperativeThreadPool.cpp b/xpcom/threads/CooperativeThreadPool.cpp
--- a/xpcom/threads/CooperativeThreadPool.cpp
+++ b/xpcom/threads/CooperativeThreadPool.cpp
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "CooperativeThreadPool.h"
 
 #include "base/message_loop.h"
 #include "mozilla/IOInterposer.h"
+#include "mozilla/ServoBindings.h"
 #include "nsError.h"
 #include "nsThreadUtils.h"
 
 using namespace mozilla;
 
 static bool gCooperativeSchedulingEnabled;
 MOZ_THREAD_LOCAL(CooperativeThreadPool::CooperativeThread*) CooperativeThreadPool::sTlsCurrentThread;
 
diff --git a/xpcom/threads/Scheduler.cpp b/xpcom/threads/Scheduler.cpp
--- a/xpcom/threads/Scheduler.cpp
+++ b/xpcom/threads/Scheduler.cpp
@@ -648,16 +648,17 @@ SchedulerImpl::ThreadController::OnStart
   xpc::CreateCooperativeContext();
 
   JSContext* cx = dom::danger::GetJSContext();
   mScheduler->SetJSContext(aIndex, cx);
   if (sPrefPreemption) {
     JS_AddInterruptCallback(cx, SchedulerImpl::InterruptCallback);
   }
   js::SetCooperativeYieldCallback(cx, SchedulerImpl::YieldCallback);
+  Servo_InitializeCooperativeThread();
 }
 
 void
 SchedulerImpl::ThreadController::OnStopThread(size_t aIndex)
 {
   xpc::DestroyCooperativeContext();
 
   NS_UnsetMainThread();
