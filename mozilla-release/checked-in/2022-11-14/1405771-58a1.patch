# HG changeset patch
# User Ehsan Akhgari <ehsan@mozilla.com>
# Date 1507243067 14400
# Node ID afebcd6c64248230294eb61894436e621fbc1a15
# Parent  e2ea6cc3085a18ed63d9a3fd4267b515a5a07e94
Bug 1405771 - Run the selection listeners after Range mutation observers have finished running to make sure no stale Ranges are observable from the listeners; r=smaug

diff --git a/dom/base/crashtests/1405771.html b/dom/base/crashtests/1405771.html
new file mode 100644
--- /dev/null
+++ b/dom/base/crashtests/1405771.html
@@ -0,0 +1,20 @@
+<html>
+<head>
+<script></script>
+<!-- a -->
+<script>
+window.onload=function(){
+  let s=window.getSelection();
+  let r=document.createRange();
+  r.selectNode(document.getElementById('b'));
+  s.addRange(r);
+  try{window.getSelection().modify('extend','forward','word')}catch(e){}
+  let o=document.getElementById('a');
+  o.parentNode.replaceChild(document.createElement('col'), o);
+}
+</script>
+>
+<template id='a' contenteditable='true'></template>
+<header id='b'></header>
+<!-- a -->
+</html>
\ No newline at end of file
diff --git a/dom/base/crashtests/crashtests.list b/dom/base/crashtests/crashtests.list
--- a/dom/base/crashtests/crashtests.list
+++ b/dom/base/crashtests/crashtests.list
@@ -223,9 +223,10 @@ load xhr_empty_datauri.html
 load xhr_html_nullresponse.html
 load 1383478.html
 load 1383780.html
 pref(clipboard.autocopy,true) load 1385272-1.html
 load 1393806.html
 load 1396466.html
 load 1400701.html
 load 1403377.html
+load 1405771.html
 pref(layout.css.resizeobserver.enabled,true) load 1555786.html
\ No newline at end of file
diff --git a/dom/base/nsRange.cpp b/dom/base/nsRange.cpp
--- a/dom/base/nsRange.cpp
+++ b/dom/base/nsRange.cpp
@@ -944,16 +944,33 @@ nsRange::IntersectsNode(nsINode& aNode, 
 
   // Step 2.
   if (disconnected) {
     result = false;
   }
   return result;
 }
 
+void
+nsRange::NotifySelectionListenersAfterRangeSet()
+{
+  if (mSelection) {
+    // Our internal code should not move focus with using this instance while
+    // it's calling Selection::NotifySelectionListeners() which may move focus
+    // or calls selection listeners.  So, let's set mCalledByJS to false here
+    // since non-*JS() methods don't set it to false.
+    AutoCalledByJSRestore calledByJSRestorer(*this);
+    mCalledByJS = false;
+    // Be aware, this range may be modified or stop being a range for selection
+    // after this call.  Additionally, the selection instance may have gone.
+    RefPtr<Selection> selection = mSelection;
+    selection->NotifySelectionListeners(calledByJSRestorer.SavedValue());
+  }
+}
+
 /******************************************************
  * Private helper routines
  ******************************************************/
 
 // It's important that all setting of the range start/end points
 // go through this function, which will do all the right voodoo
 // for content notification of range ownership.
 // Calling DoSetRange with either parent argument null will collapse
@@ -1027,27 +1044,23 @@ nsRange::DoSetRange(const RawRangeBounda
   }
 
   // This needs to be the last thing this function does, other than notifying
   // selection listeners. See comment in ParentChainChanged.
   mRoot = aRoot;
 
   // Notify any selection listeners. This has to occur last because otherwise the world
   // could be observed by a selection listener while the range was in an invalid state.
+  // So we run it off of a script runner to ensure it runs after the mutation observers
+  // have finished running.
   if (mSelection) {
-    // Our internal code should not move focus with using this instance while
-    // it's calling Selection::NotifySelectionListeners() which may move focus
-    // or calls selection listeners.  So, let's set mCalledByJS to false here
-    // since non-*JS() methods don't set it to false.
-    AutoCalledByJSRestore calledByJSRestorer(*this);
-    mCalledByJS = false;
-    // Be aware, this range may be modified or stop being a range for selection
-    // after this call.  Additionally, the selection instance may have gone.
-    RefPtr<Selection> selection = mSelection;
-    selection->NotifySelectionListeners(calledByJSRestorer.SavedValue());
+    nsContentUtils::AddScriptRunner(NewRunnableMethod(
+                                    "NotifySelectionListenersAfterRangeSet",
+                                    this,
+                                    &nsRange::NotifySelectionListenersAfterRangeSet));
   }
 }
 
 static int32_t
 IndexOf(nsINode* aChild)
 {
   nsINode* parent = aChild->GetParentNode();
 
diff --git a/dom/base/nsRange.h b/dom/base/nsRange.h
--- a/dom/base/nsRange.h
+++ b/dom/base/nsRange.h
@@ -422,16 +422,21 @@ public:
    * Otherwise, |this| will be modified so that it ends before the first
    * -moz-user-select:none node and additional ranges may also be created.
    * If all nodes in the range are -moz-user-select:none then aOutRanges
    * will be empty.
    * @param aOutRanges the resulting set of ranges
    */
   void ExcludeNonSelectableNodes(nsTArray<RefPtr<nsRange>>* aOutRanges);
 
+  /**
+   * Notify the selection listeners after a range has been modified.
+   */
+  void NotifySelectionListenersAfterRangeSet();
+
   typedef nsTHashtable<nsPtrHashKey<nsRange> > RangeHashTable;
 protected:
 
   void RegisterCommonAncestor(nsINode* aNode);
   void UnregisterCommonAncestor(nsINode* aNode, bool aIsUnlinking);
   nsINode* IsValidBoundary(nsINode* aNode) const
   {
     return ComputeRootNode(aNode, mMaySpanAnonymousSubtrees);
