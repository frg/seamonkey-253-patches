# HG changeset patch
# User John Dai <jdai@mozilla.com>
# Date 1510658700 -28800
#      Tue Nov 14 19:25:00 2017 +0800
# Node ID 067aabd5d35d4d6ff68dd6d7d3472ea04b38a925
# Parent  66c6a5a87c83907195925fd91af1d9bfd894261e
Bug 1406325 - Part 5: Implement try to upgrade. f=echen, r=smaug

diff --git a/dom/base/CustomElementRegistry.cpp b/dom/base/CustomElementRegistry.cpp
--- a/dom/base/CustomElementRegistry.cpp
+++ b/dom/base/CustomElementRegistry.cpp
@@ -319,16 +319,34 @@ CustomElementRegistry::RegisterUnresolve
   }
 
   nsTArray<nsWeakPtr>* unresolved = mCandidatesMap.LookupOrAdd(typeName);
   nsWeakPtr* elem = unresolved->AppendElement();
   *elem = do_GetWeakReference(aElement);
   aElement->AddStates(NS_EVENT_STATE_UNRESOLVED);
 }
 
+void
+CustomElementRegistry::UnregisterUnresolvedElement(Element* aElement,
+                                                   nsIAtom* aTypeName)
+{
+  nsTArray<nsWeakPtr>* candidates;
+  if (mCandidatesMap.Get(aTypeName, &candidates)) {
+    MOZ_ASSERT(candidates);
+    // We don't need to iterate the candidates array and remove the element from
+    // the array for performance reason. It'll be handled by bug 1396620.
+    for (size_t i = 0; i < candidates->Length(); ++i) {
+      nsCOMPtr<Element> elem = do_QueryReferent(candidates->ElementAt(i));
+      if (elem && elem.get() == aElement) {
+        candidates->RemoveElementAt(i);
+      }
+    }
+  }
+}
+
 /* static */ UniquePtr<CustomElementCallback>
 CustomElementRegistry::CreateCustomElementCallback(
   nsIDocument::ElementCallbackType aType, Element* aCustomElement,
   LifecycleCallbackArgs* aArgs,
   LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
   CustomElementDefinition* aDefinition)
 {
   MOZ_ASSERT(aDefinition, "CustomElementDefinition should not be null");
diff --git a/dom/base/CustomElementRegistry.h b/dom/base/CustomElementRegistry.h
--- a/dom/base/CustomElementRegistry.h
+++ b/dom/base/CustomElementRegistry.h
@@ -390,36 +390,42 @@ public:
                            CustomElementDefinition* aDefinition);
 
   /**
    * Upgrade an element.
    * https://html.spec.whatwg.org/multipage/scripting.html#upgrades
    */
   static void Upgrade(Element* aElement, CustomElementDefinition* aDefinition, ErrorResult& aRv);
 
-private:
-  ~CustomElementRegistry();
-
-  static UniquePtr<CustomElementCallback> CreateCustomElementCallback(
-    nsIDocument::ElementCallbackType aType, Element* aCustomElement,
-    LifecycleCallbackArgs* aArgs,
-    LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
-    CustomElementDefinition* aDefinition);
-
   /**
    * Registers an unresolved custom element that is a candidate for
    * upgrade when the definition is registered via registerElement.
    * |aTypeName| is the name of the custom element type, if it is not
    * provided, then element name is used. |aTypeName| should be provided
    * when registering a custom element that extends an existing
    * element. e.g. <button is="x-button">.
    */
   void RegisterUnresolvedElement(Element* aElement,
                                  nsIAtom* aTypeName = nullptr);
 
+  /**
+   * Unregister an unresolved custom element that is a candidate for
+   * upgrade when a custom element is removed from tree.
+   */
+  void UnregisterUnresolvedElement(Element* aElement,
+                                   nsIAtom* aTypeName = nullptr);
+private:
+  ~CustomElementRegistry();
+
+  static UniquePtr<CustomElementCallback> CreateCustomElementCallback(
+    nsIDocument::ElementCallbackType aType, Element* aCustomElement,
+    LifecycleCallbackArgs* aArgs,
+    LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
+    CustomElementDefinition* aDefinition);
+
   void UpgradeCandidates(nsIAtom* aKey,
                          CustomElementDefinition* aDefinition,
                          ErrorResult& aRv);
 
   typedef nsRefPtrHashtable<nsISupportsHashKey, CustomElementDefinition>
     DefinitionMap;
   typedef nsClassHashtable<nsISupportsHashKey, nsTArray<nsWeakPtr>>
     CandidateMap;
diff --git a/dom/base/Element.cpp b/dom/base/Element.cpp
--- a/dom/base/Element.cpp
+++ b/dom/base/Element.cpp
@@ -1802,18 +1802,23 @@ Element::BindToTree(nsIDocument* aDocume
     // update our subtree pointer.
     SetSubtreeRootPointer(aParent->SubtreeRoot());
   }
 
   if (CustomElementRegistry::IsCustomElementEnabled() && IsInComposedDoc()) {
     // Connected callback must be enqueued whenever a custom element becomes
     // connected.
     CustomElementData* data = GetCustomElementData();
-    if (data && data->mState == CustomElementData::State::eCustom) {
-      nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eConnected, this);
+    if (data) {
+      if (data->mState == CustomElementData::State::eCustom) {
+        nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eConnected, this);
+      } else {
+        // Step 7.7.2.2 https://dom.spec.whatwg.org/#concept-node-insert
+        nsContentUtils::TryToUpgradeElement(this);
+      }
     }
   }
 
   // Propagate scoped style sheet tracking bit.
   if (mParent->IsContent()) {
     nsIContent* parent;
     ShadowRoot* shadowRootParent = ShadowRoot::FromNode(mParent);
     if (shadowRootParent) {
@@ -2131,19 +2136,26 @@ Element::UnbindFromTree(bool aDeep, bool
     }
 
     document->ClearBoxObjectFor(this);
 
      // Disconnected must be enqueued whenever a connected custom element becomes
      // disconnected.
     if (CustomElementRegistry::IsCustomElementEnabled()) {
       CustomElementData* data  = GetCustomElementData();
-      if (data && data->mState == CustomElementData::State::eCustom) {
-        nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eDisconnected,
-                                                 this);
+      if (data) {
+        if (data->mState == CustomElementData::State::eCustom) {
+          nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eDisconnected,
+                                                   this);
+        } else {
+          // Remove an unresolved custom element that is a candidate for
+          // upgrade when a custom element is disconnected.
+          // We will make sure it's shadow-including tree order in bug 1326028.
+          nsContentUtils::UnregisterUnresolvedElement(this);
+        }
       }
     }
   }
 
   // This has to be here, rather than in nsGenericHTMLElement::UnbindFromTree,
   //  because it has to happen after unsetting the parent pointer, but before
   //  recursively unbinding the kids.
   if (IsHTMLElement()) {
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -235,16 +235,17 @@
 #endif
 
 extern "C" int MOZ_XMLTranslateEntity(const char* ptr, const char* end,
                                       const char** next, char16_t* result);
 extern "C" int MOZ_XMLCheckQName(const char* ptr, const char* end,
                                  int ns_aware, const char** colon);
 
 class imgLoader;
+class nsIAtom;
 
 using namespace mozilla::dom;
 using namespace mozilla::ipc;
 using namespace mozilla::gfx;
 using namespace mozilla::layers;
 using namespace mozilla::widget;
 using namespace mozilla;
 
@@ -10181,16 +10182,37 @@ nsContentUtils::HttpsStateIsModern(nsIDo
     if (isTrustworthyOrigin) {
       return true;
     }
   }
 
   return false;
 }
 
+/* static */ void
+nsContentUtils::TryToUpgradeElement(Element* aElement)
+{
+  NodeInfo* nodeInfo = aElement->NodeInfo();
+  nsCOMPtr<nsIAtom> typeAtom =
+    aElement->GetCustomElementData()->GetCustomElementType();
+  CustomElementDefinition* definition =
+    nsContentUtils::LookupCustomElementDefinition(nodeInfo->GetDocument(),
+                                                  nodeInfo->LocalName(),
+                                                  nodeInfo->NamespaceID(),
+                                                  typeAtom);
+  if (definition) {
+    nsContentUtils::EnqueueUpgradeReaction(aElement, definition);
+  } else {
+    // Add an unresolved custom element that is a candidate for
+    // upgrade when a custom element is connected to the document.
+    // We will make sure it's shadow-including tree order in bug 1326028.
+    nsContentUtils::RegisterUnresolvedElement(aElement, typeAtom);
+  }
+}
+
 /* static */ CustomElementDefinition*
 nsContentUtils::LookupCustomElementDefinition(nsIDocument* aDoc,
                                               const nsAString& aLocalName,
                                               uint32_t aNameSpaceID,
                                               nsIAtom* aTypeAtom)
 {
   MOZ_ASSERT(aDoc);
 
@@ -10207,16 +10229,56 @@ nsContentUtils::LookupCustomElementDefin
   RefPtr<CustomElementRegistry> registry(window->CustomElements());
   if (!registry) {
     return nullptr;
   }
 
   return registry->LookupCustomElementDefinition(aLocalName, aTypeAtom);
 }
 
+/* static */ void
+nsContentUtils::RegisterUnresolvedElement(Element* aElement, nsIAtom* aTypeName)
+{
+  MOZ_ASSERT(aElement);
+
+  nsIDocument* doc = aElement->OwnerDoc();
+  nsPIDOMWindowInner* window(doc->GetInnerWindow());
+  if (!window) {
+    return;
+  }
+
+  RefPtr<CustomElementRegistry> registry(window->CustomElements());
+  if (!registry) {
+    return;
+  }
+
+  registry->RegisterUnresolvedElement(aElement, aTypeName);
+}
+
+/* static */ void
+nsContentUtils::UnregisterUnresolvedElement(Element* aElement)
+{
+  MOZ_ASSERT(aElement);
+
+  nsCOMPtr<nsIAtom> typeAtom =
+    aElement->GetCustomElementData()->GetCustomElementType();
+  nsIDocument* doc = aElement->OwnerDoc();
+  nsPIDOMWindowInner* window(doc->GetInnerWindow());
+  if (!window) {
+    return;
+  }
+
+  RefPtr<CustomElementRegistry> registry(window->CustomElements());
+  if (!registry) {
+    return;
+  }
+
+  registry->UnregisterUnresolvedElement(aElement, typeAtom);
+}
+
 /* static */ CustomElementDefinition*
 nsContentUtils::GetElementDefinitionIfObservingAttr(Element* aCustomElement,
                                                     nsIAtom* aExtensionType,
                                                     nsIAtom* aAttrName)
 {
   CustomElementDefinition* definition =
     aCustomElement->GetCustomElementDefinition();
 
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -3027,25 +3027,33 @@ public:
    * Returns true if the "HTTPS state" of the document should be "modern". See:
    *
    * https://html.spec.whatwg.org/#concept-document-https-state
    * https://fetch.spec.whatwg.org/#concept-response-https-state
    */
   static bool HttpsStateIsModern(nsIDocument* aDocument);
 
   /**
+   * Try to upgrade an element.
+   * https://html.spec.whatwg.org/multipage/custom-elements.html#concept-try-upgrade
+   */
+  static void TryToUpgradeElement(Element* aElement);
+
+  /**
    * Looking up a custom element definition.
    * https://html.spec.whatwg.org/#look-up-a-custom-element-definition
    */
   static mozilla::dom::CustomElementDefinition*
     LookupCustomElementDefinition(nsIDocument* aDoc,
                                   const nsAString& aLocalName,
                                   uint32_t aNameSpaceID,
                                   nsIAtom* aTypeAtom);
 
+  static void RegisterUnresolvedElement(Element* aElement, nsIAtom* aTypeName);
+  static void UnregisterUnresolvedElement(Element* aElement);
 
   static mozilla::dom::CustomElementDefinition*
   GetElementDefinitionIfObservingAttr(Element* aCustomElement,
                                       nsIAtom* aExtensionType,
                                       nsIAtom* aAttrName);
 
   static void SyncInvokeReactions(nsIDocument::ElementCallbackType aType,
                                   Element* aCustomElement,
diff --git a/dom/tests/mochitest/webcomponents/upgrade_tests.js b/dom/tests/mochitest/webcomponents/upgrade_tests.js
--- a/dom/tests/mochitest/webcomponents/upgrade_tests.js
+++ b/dom/tests/mochitest/webcomponents/upgrade_tests.js
@@ -3,30 +3,39 @@ function test_upgrade(f, msg) {
   test_with_new_window(function(testWindow, testMsg) {
     let elementParser = testWindow.document.querySelector('unresolved-element');
     f(testWindow, elementParser, testMsg);
   }, msg + ' (HTML parser)');
 
   // Run upgrading test on an element created by document.createElement.
   test_with_new_window(function(testWindow, testMsg) {
     let element = testWindow.document.createElement('unresolved-element');
+    testWindow.document.documentElement.appendChild(element);
     f(testWindow, element, testMsg);
   }, msg + ' (document.createElement)');
 }
 
 // Test cases
 
 test_upgrade(function(testWindow, testElement, msg) {
   class MyCustomElement extends testWindow.HTMLElement {};
   testWindow.customElements.define('unresolved-element', MyCustomElement);
   SimpleTest.is(Object.getPrototypeOf(Cu.waiveXrays(testElement)),
                 MyCustomElement.prototype, msg);
 }, 'Custom element must be upgraded if there is a matching definition');
 
 test_upgrade(function(testWindow, testElement, msg) {
+  testElement.remove();
+  class MyCustomElement extends testWindow.HTMLElement {};
+  testWindow.customElements.define('unresolved-element', MyCustomElement);
+  SimpleTest.is(Object.getPrototypeOf(testElement),
+                testWindow.HTMLElement.prototype, msg);
+}, 'Custom element must not be upgraded if it has been removed from tree');
+
+test_upgrade(function(testWindow, testElement, msg) {
   let exceptionToThrow = {name: 'exception thrown by a custom constructor'};
   class ThrowCustomElement extends testWindow.HTMLElement {
     constructor() {
       super();
       if (exceptionToThrow) {
         throw exceptionToThrow;
       }
     }
