# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1508311060 18000
# Node ID 4d48f13d3d7f6a0921f3df09235812c858728782
# Parent  23c0e487584eddf7bb432258d48ff0c5a96eed0e
servo: Merge #18932 - Rename `size_of_is_0!` as `malloc_size_of_is_0!` (from nnethercote:rename-size_of_is_0); r=emilio

The new name makes it clearer that it comes from the `malloc_size_of`
crate.

<!-- Please describe your changes on the following line: -->

---
<!-- Thank you for contributing to Servo! Please replace each `[ ]` by `[X]` when the step is complete, and replace `__` with appropriate data: -->
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [ ] These changes fix #__ (github issue number if applicable).

<!-- Either: -->
- [ ] There are tests for these changes OR
- [X] These changes do not require tests because it's a trivial name change.

<!-- Also, please make sure that "Allow edits from maintainers" checkbox is checked, so that we can help you if you get stuck somewhere along the way.-->

<!-- Pull requests that do not address these steps are welcome, but they will require additional verification as part of the review process. -->

Source-Repo: https://github.com/servo/servo
Source-Revision: 421baa854ea40c7b1a3d1e75acac14da04a3fbcc

diff --git a/servo/components/malloc_size_of/lib.rs b/servo/components/malloc_size_of/lib.rs
--- a/servo/components/malloc_size_of/lib.rs
+++ b/servo/components/malloc_size_of/lib.rs
@@ -410,17 +410,17 @@ impl MallocSizeOf for smallbitvec::Small
 impl<T: MallocSizeOf, U> MallocSizeOf for euclid::TypedSize2D<T, U> {
     fn size_of(&self, ops: &mut MallocSizeOfOps) -> usize {
         self.width.size_of(ops) + self.height.size_of(ops)
     }
 }
 
 /// For use on types where size_of() returns 0.
 #[macro_export]
-macro_rules! size_of_is_0(
+macro_rules! malloc_size_of_is_0(
     ($($ty:ty),+) => (
         $(
             impl $crate::MallocSizeOf for $ty {
                 #[inline(always)]
                 fn size_of(&self, _: &mut $crate::MallocSizeOfOps) -> usize {
                     0
                 }
             }
@@ -433,19 +433,19 @@ macro_rules! size_of_is_0(
             fn size_of(&self, _: &mut $crate::MallocSizeOfOps) -> usize {
                 0
             }
         }
         )+
     );
 );
 
-size_of_is_0!(bool, char, str);
-size_of_is_0!(u8, u16, u32, u64, usize);
-size_of_is_0!(i8, i16, i32, i64, isize);
-size_of_is_0!(f32, f64);
+malloc_size_of_is_0!(bool, char, str);
+malloc_size_of_is_0!(u8, u16, u32, u64, usize);
+malloc_size_of_is_0!(i8, i16, i32, i64, isize);
+malloc_size_of_is_0!(f32, f64);
 
-size_of_is_0!(Range<u8>, Range<u16>, Range<u32>, Range<u64>, Range<usize>);
-size_of_is_0!(Range<i8>, Range<i16>, Range<i32>, Range<i64>, Range<isize>);
-size_of_is_0!(Range<f32>, Range<f64>);
+malloc_size_of_is_0!(Range<u8>, Range<u16>, Range<u32>, Range<u64>, Range<usize>);
+malloc_size_of_is_0!(Range<i8>, Range<i16>, Range<i32>, Range<i64>, Range<isize>);
+malloc_size_of_is_0!(Range<f32>, Range<f64>);
 
-size_of_is_0!(app_units::Au);
-size_of_is_0!(cssparser::RGBA, cssparser::TokenSerializationType);
+malloc_size_of_is_0!(app_units::Au);
+malloc_size_of_is_0!(cssparser::RGBA, cssparser::TokenSerializationType);
diff --git a/servo/components/style/gecko_string_cache/mod.rs b/servo/components/style/gecko_string_cache/mod.rs
--- a/servo/components/style/gecko_string_cache/mod.rs
+++ b/servo/components/style/gecko_string_cache/mod.rs
@@ -383,9 +383,9 @@ impl From<*mut nsIAtom> for Atom {
             if !ret.is_static() {
                 Gecko_AddRefAtom(ptr);
             }
             ret
         }
     }
 }
 
-size_of_is_0!(Atom);
+malloc_size_of_is_0!(Atom);
diff --git a/servo/components/style/properties/longhand/color.mako.rs b/servo/components/style/properties/longhand/color.mako.rs
--- a/servo/components/style/properties/longhand/color.mako.rs
+++ b/servo/components/style/properties/longhand/color.mako.rs
@@ -67,17 +67,17 @@ pub mod system_colors {
     use std::fmt;
     use style_traits::ToCss;
     use values::computed::{Context, ToComputedValue};
 
     pub type SystemColor = LookAndFeel_ColorID;
 
     // It's hard to implement MallocSizeOf for LookAndFeel_ColorID because it
     // is a bindgen type. So we implement it on the typedef instead.
-    size_of_is_0!(SystemColor);
+    malloc_size_of_is_0!(SystemColor);
 
     impl ToCss for SystemColor {
         fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
             let s = match *self {
                 % for color in system_colors + extra_colors:
                     LookAndFeel_ColorID::eColorID_${to_rust_ident(color)} => "${color}",
                 % endfor
                 LookAndFeel_ColorID::eColorID_LAST_COLOR => unreachable!(),
