# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1506587489 -32400
# Node ID 322eba7fa97aa0cd3595242cefbe45be8c626ce1
# Parent  5a9b49e80fff07d791185be5a153caaa01efaa73
Bug 1403026 - Make IMEStateManager release all string buffer of its static members at XPCOM shutdown r=smaug

IMEStateManager has a static instance of InputContext which has some nsString
instances.  Then, their members will be released after XPCOM shutdown and
that is detected as unexpected destruction.

Therefore, IMEStateManager should release them by itself at XPCOM shutdown.
Using nsTSubstring::SetCapacity(0) is the simplest way to release only the
string buffers.

MozReview-Commit-ID: LMrQxQF9xPn

diff --git a/dom/events/IMEStateManager.cpp b/dom/events/IMEStateManager.cpp
--- a/dom/events/IMEStateManager.cpp
+++ b/dom/events/IMEStateManager.cpp
@@ -188,16 +188,19 @@ IMEStateManager::Shutdown()
 {
   MOZ_LOG(sISMLog, LogLevel::Info,
     ("Shutdown(), sTextCompositions=0x%p, sTextCompositions->Length()=%zu",
      sTextCompositions, sTextCompositions ? sTextCompositions->Length() : 0));
 
   MOZ_ASSERT(!sTextCompositions || !sTextCompositions->Length());
   delete sTextCompositions;
   sTextCompositions = nullptr;
+  // All string instances in the global space need to be empty after XPCOM
+  // shutdown.
+  sActiveChildInputContext.ShutDown();
 }
 
 // static
 void
 IMEStateManager::OnTabParentDestroying(TabParent* aTabParent)
 {
   if (sFocusedIMETabParent == aTabParent) {
     NotifyIMEOfBlurForChildProcess();
diff --git a/widget/IMEData.h b/widget/IMEData.h
--- a/widget/IMEData.h
+++ b/widget/IMEData.h
@@ -274,16 +274,28 @@ struct InputContext final
 {
   InputContext()
     : mOrigin(XRE_IsParentProcess() ? ORIGIN_MAIN : ORIGIN_CONTENT)
     , mMayBeIMEUnaware(false)
     , mInPrivateBrowsing(false)
   {
   }
 
+  // If InputContext instance is a static variable, any heap allocated stuff
+  // of its members need to be deleted at XPCOM shutdown.  Otherwise, it's
+  // detected as memory leak.
+  void ShutDown()
+  {
+    // The buffer for nsString will be released with a call of SetCapacity(0).
+    // Truncate() isn't enough because it just sets length to 0.
+    mHTMLInputType.SetCapacity(0);
+    mHTMLInputInputmode.SetCapacity(0);
+    mActionHint.SetCapacity(0);
+  }
+
   bool IsPasswordEditor() const
   {
     return mHTMLInputType.LowerCaseEqualsLiteral("password");
   }
 
   IMEState mIMEState;
 
   /* The type of the input if the input is a html input field */

