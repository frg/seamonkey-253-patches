# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1508943087 -32400
#      Wed Oct 25 23:51:27 2017 +0900
# Node ID 5faabed7438613b088c5d5020b93b63cafd3dc7b
# Parent  0aacbe13228d9217d591ac89f4533eafd27b9225
Bug 1409155 - IMEHandler should not associate IMC with any window if active IME is ATOK 2010 or earlier with x64 build on Win8 or later. r=m_kato, a=ritu

ATOK started to be TIP of TSF since 2011.  Older than it, i.e., ATOK 2010 and
earlier have a lot of problems even for daily use.  Perhaps, the reason is
Win 8 has a lot of changes around IMM-IME support and TSF, and ATOK 2010 is
released earlier than Win 8.

ATOK 2006 crashes while converting a word with candidate window.
ATOK 2007 doesn't paint and resize suggest window and candidate window
correctly (showing white window or too big window).
ATOK 2008 and ATOK 2009 crash when user just opens their open state.
ATOK 2010 isn't installable newly on Win 7 or later, but we have a lot of
crash reports.

Note that ATOK 2006 is the first version supporting Win XP x64.  So, ATOK 2005
must not support x64 apps.

Unfortunately, we cannot block loading DLLs of them.  Therefore, IMEHandler
should disassociate IMC from active window when user changes active keyboard
layout to the legacy ATOK and not associate IME with any window when
SetInputContext() is called even with "enabled".  Additionally, when user
changes active keyboard layout from the legacy ATOK to one of the other
keyboard layouts, IMEHandler should associate IMC with current window for
new active IME.

MozReview-Commit-ID: RVYwmYxzO7

diff --git a/widget/windows/IMMHandler.cpp b/widget/windows/IMMHandler.cpp
--- a/widget/windows/IMMHandler.cpp
+++ b/widget/windows/IMMHandler.cpp
@@ -11,16 +11,17 @@
 #include "nsWindowDefs.h"
 #include "WinUtils.h"
 #include "KeyboardLayout.h"
 #include <algorithm>
 
 #include "mozilla/CheckedInt.h"
 #include "mozilla/MiscEvents.h"
 #include "mozilla/TextEvents.h"
+#include "mozilla/WindowsVersion.h"
 
 #ifndef IME_PROP_ACCEPT_WIDE_VKEY
 #define IME_PROP_ACCEPT_WIDE_VKEY 0x20
 #endif
 
 //-------------------------------------------------------------------------
 //
 // from http://download.microsoft.com/download/6/0/9/60908e9e-d2c1-47db-98f6-216af76a235f/msime.h
@@ -220,16 +221,64 @@ WritingMode IMMHandler::sWritingModeOfCo
 nsString IMMHandler::sIMEName;
 UINT IMMHandler::sCodePage = 0;
 DWORD IMMHandler::sIMEProperty = 0;
 DWORD IMMHandler::sIMEUIProperty = 0;
 bool IMMHandler::sAssumeVerticalWritingModeNotSupported = false;
 bool IMMHandler::sHasFocus = false;
 bool IMMHandler::sNativeCaretIsCreatedForPlugin = false;
 
+#define IMPL_IS_IME_ACTIVE(aReadableName, aActualName)                         \
+bool                                                                           \
+IMMHandler::Is ## aReadableName ## Active()                                    \
+{                                                                              \
+  return sIMEName.Equals(aActualName);                                         \
+}                                                                              \
+
+IMPL_IS_IME_ACTIVE(ATOK2006, u"ATOK 2006")
+IMPL_IS_IME_ACTIVE(ATOK2007, u"ATOK 2007")
+IMPL_IS_IME_ACTIVE(ATOK2008, u"ATOK 2008")
+IMPL_IS_IME_ACTIVE(ATOK2009, u"ATOK 2009")
+IMPL_IS_IME_ACTIVE(ATOK2010, u"ATOK 2010")
+// NOTE: Even on Windows for en-US, the name of Google Japanese Input is
+//       written in Japanese.
+IMPL_IS_IME_ACTIVE(GoogleJapaneseInput,
+                   u"Google \x65E5\x672C\x8A9E\x5165\x529B "
+                   u"IMM32 \x30E2\x30B8\x30E5\x30FC\x30EB")
+IMPL_IS_IME_ACTIVE(Japanist2003, u"Japanist 2003")
+
+#undef IMPL_IS_IME_ACTIVE
+
+// static
+bool
+IMMHandler::IsActiveIMEInBlockList()
+{
+  if (sIMEName.IsEmpty()) {
+    return false;
+  }
+#ifdef _WIN64
+  // ATOK started to be TIP of TSF since 2011.  Older than it, i.e., ATOK 2010
+  // and earlier have a lot of problems even for daily use.  Perhaps, the
+  // reason is Win 8 has a lot of changes around IMM-IME support and TSF,
+  // and ATOK 2010 is released earlier than Win 8.
+  // ATOK 2006 crashes while converting a word with candidate window.
+  // ATOK 2007 doesn't paint and resize suggest window and candidate window
+  // correctly (showing white window or too big window).
+  // ATOK 2008 and ATOK 2009 crash when user just opens their open state.
+  // ATOK 2010 isn't installable newly on Win 7 or later, but we have a lot of
+  // crash reports.
+  if (IsWin8OrLater() &&
+      (IsATOK2006Active() || IsATOK2007Active() || IsATOK2008Active() ||
+       IsATOK2009Active() || IsATOK2010Active())) {
+    return true;
+  }
+#endif // #ifdef _WIN64
+  return false;
+}
+
 // static
 void
 IMMHandler::EnsureHandlerInstance()
 {
   if (!gIMMHandler) {
     gIMMHandler = new IMMHandler();
   }
 }
@@ -286,33 +335,16 @@ IMMHandler::IsTopLevelWindowOfCompositio
     return false;
   }
   HWND wnd = gIMMHandler->mComposingWindow->GetWindowHandle();
   return WinUtils::GetTopLevelHWND(wnd, true) == aWindow->GetWindowHandle();
 }
 
 // static
 bool
-IMMHandler::IsJapanist2003Active()
-{
-  return sIMEName.EqualsLiteral("Japanist 2003");
-}
-
-// static
-bool
-IMMHandler::IsGoogleJapaneseInputActive()
-{
-  // NOTE: Even on Windows for en-US, the name of Google Japanese Input is
-  //       written in Japanese.
-  return sIMEName.Equals(u"Google \x65E5\x672C\x8A9E\x5165\x529B "
-                         u"IMM32 \x30E2\x30B8\x30E5\x30FC\x30EB");
-}
-
-// static
-bool
 IMMHandler::ShouldDrawCompositionStringOurselves()
 {
   // If current IME has special UI or its composition window should not
   // positioned to caret position, we should now draw composition string
   // ourselves.
   return !(sIMEProperty & IME_PROP_SPECIAL_UI) &&
           (sIMEProperty & IME_PROP_AT_CARET);
 }
diff --git a/widget/windows/IMMHandler.h b/widget/windows/IMMHandler.h
--- a/widget/windows/IMMHandler.h
+++ b/widget/windows/IMMHandler.h
@@ -152,26 +152,44 @@ public:
   // Returns NS_SUCCESS_EVENT_CONSUMED if the mouse button event is consumed by
   // IME.  Otherwise, NS_OK.
   static nsresult OnMouseButtonEvent(nsWindow* aWindow,
                                      const IMENotification& aIMENotification);
   static void SetCandidateWindow(nsWindow* aWindow, CANDIDATEFORM* aForm);
   static void DefaultProcOfPluginEvent(nsWindow* aWindow,
                                        const NPEvent* aEvent);
 
+#define DECL_IS_IME_ACTIVE(aReadableName)                                      \
+  static bool Is ## aReadableName ## Active();                                 \
+
+  // Japanese IMEs
+  DECL_IS_IME_ACTIVE(ATOK2006)
+  DECL_IS_IME_ACTIVE(ATOK2007)
+  DECL_IS_IME_ACTIVE(ATOK2008)
+  DECL_IS_IME_ACTIVE(ATOK2009)
+  DECL_IS_IME_ACTIVE(ATOK2010)
+  DECL_IS_IME_ACTIVE(GoogleJapaneseInput)
+  DECL_IS_IME_ACTIVE(Japanist2003)
+
+#undef DECL_IS_IME_ACTIVE
+
+  /**
+   * IsActiveIMEInBlockList() returns true if we know active keyboard layout's
+   * IME has some crash bugs or something which make some damage to us.  When
+   * this returns true, IMC shouldn't be associated with any windows.
+   */
+  static bool IsActiveIMEInBlockList();
+
 protected:
   static void EnsureHandlerInstance();
 
   static bool IsComposingOnOurEditor();
   static bool IsComposingOnPlugin();
   static bool IsComposingWindow(nsWindow* aWindow);
 
-  static bool IsJapanist2003Active();
-  static bool IsGoogleJapaneseInputActive();
-
   static bool ShouldDrawCompositionStringOurselves();
   static bool IsVerticalWritingSupported();
   // aWindow can be nullptr if it's called without receiving WM_INPUTLANGCHANGE.
   static void InitKeyboardLayout(nsWindow* aWindow, HKL aKeyboardLayout);
   static UINT GetKeyboardCodePage();
 
   /**
    * Checks whether the window is top level window of the composing window.
diff --git a/widget/windows/WinIMEHandler.cpp b/widget/windows/WinIMEHandler.cpp
--- a/widget/windows/WinIMEHandler.cpp
+++ b/widget/windows/WinIMEHandler.cpp
@@ -38,16 +38,17 @@ namespace widget {
 
 /******************************************************************************
  * IMEHandler
  ******************************************************************************/
 
 nsWindow* IMEHandler::sFocusedWindow = nullptr;
 InputContextAction::Cause IMEHandler::sLastContextActionCause =
   InputContextAction::CAUSE_UNKNOWN;
+bool IMEHandler::sForceDisableCurrentIMM_IME = false;
 bool IMEHandler::sPluginHasFocus = false;
 
 #ifdef NS_ENABLE_TSF
 bool IMEHandler::sIsInTSFMode = false;
 bool IMEHandler::sIsIMMEnabled = true;
 bool IMEHandler::sAssociateIMCOnlyWhenIMM_IMEActive = false;
 decltype(SetInputScopes)* IMEHandler::sSetInputScopes = nullptr;
 #endif // #ifdef NS_ENABLE_TSF
@@ -77,16 +78,18 @@ IMEHandler::Initialize()
                            &module)) {
       sSetInputScopes = reinterpret_cast<decltype(SetInputScopes)*>(
         GetProcAddress(module, "SetInputScopes"));
     }
   }
 #endif // #ifdef NS_ENABLE_TSF
 
   IMMHandler::Initialize();
+
+  sForceDisableCurrentIMM_IME = IMMHandler::IsActiveIMEInBlockList();
 }
 
 // static
 void
 IMEHandler::Terminate()
 {
 #ifdef NS_ENABLE_TSF
   if (sIsInTSFMode) {
@@ -180,18 +183,33 @@ IMEHandler::ProcessMessage(nsWindow* aWi
     // IME isn't implemented with IMM, IMMHandler shouldn't handle any
     // messages.
     if (!IsIMMActive()) {
       return false;
     }
   }
 #endif // #ifdef NS_ENABLE_TSF
 
-  return IMMHandler::ProcessMessage(aWindow, aMessage, aWParam, aLParam,
-                                    aResult);
+  bool keepGoing =
+    IMMHandler::ProcessMessage(aWindow, aMessage, aWParam, aLParam, aResult);
+
+  // If user changes active IME to an IME which is listed in our block list,
+  // we should disassociate IMC from the window for preventing the IME to work
+  // and crash.
+  if (aMessage == WM_INPUTLANGCHANGE) {
+    bool disableIME = IMMHandler::IsActiveIMEInBlockList();
+    if (disableIME != sForceDisableCurrentIMM_IME) {
+      bool enable =
+        !disableIME && WinUtils::IsIMEEnabled(aWindow->InputContextRef());
+      AssociateIMEContext(aWindow, enable);
+      sForceDisableCurrentIMM_IME = disableIME;
+    }
+  }
+
+  return keepGoing;
 }
 
 #ifdef NS_ENABLE_TSF
 // static
 bool
 IMEHandler::IsIMMActive()
 {
   return TSFTextStore::IsIMM_IMEActive();
@@ -416,17 +434,18 @@ IMEHandler::OnDestroyWindow(nsWindow* aW
   AssociateIMEContext(aWindow, true);
 }
 
 #ifdef NS_ENABLE_TSF
 // static
 bool
 IMEHandler::NeedsToAssociateIMC()
 {
-  return !sAssociateIMCOnlyWhenIMM_IMEActive || !IsIMMActive();
+  return !sForceDisableCurrentIMM_IME &&
+         (!sAssociateIMCOnlyWhenIMM_IMEActive || !IsIMMActive());
 }
 #endif // #ifdef NS_ENABLE_TSF
 
 // static
 void
 IMEHandler::SetInputContext(nsWindow* aWindow,
                             InputContext& aInputContext,
                             const InputContextAction& aAction)
diff --git a/widget/windows/WinIMEHandler.h b/widget/windows/WinIMEHandler.h
--- a/widget/windows/WinIMEHandler.h
+++ b/widget/windows/WinIMEHandler.h
@@ -141,16 +141,17 @@ public:
    */
   static bool CurrentKeyboardLayoutHasIME();
 #endif // #ifdef DEBUG
 
 private:
   static nsWindow* sFocusedWindow;
   static InputContextAction::Cause sLastContextActionCause;
 
+  static bool sForceDisableCurrentIMM_IME;
   static bool sPluginHasFocus;
 
 #ifdef NS_ENABLE_TSF
   static decltype(SetInputScopes)* sSetInputScopes;
   static void SetInputScopeForIMM32(nsWindow* aWindow,
                                     const nsAString& aHTMLInputType,
                                     const nsAString& aHTMLInputInputmode);
   static bool sIsInTSFMode;
diff --git a/widget/windows/nsWindowBase.h b/widget/windows/nsWindowBase.h
--- a/widget/windows/nsWindowBase.h
+++ b/widget/windows/nsWindowBase.h
@@ -103,16 +103,18 @@ public:
 
   /*
    * WM_APPCOMMAND common handler.
    * Sends events via NativeKey::HandleAppCommandMessage().
    */
   virtual bool HandleAppCommandMsg(const MSG& aAppCommandMsg,
                                    LRESULT *aRetValue);
 
+  const InputContext& InputContextRef() const { return mInputContext; }
+
 protected:
   virtual int32_t LogToPhys(double aValue) = 0;
   void ChangedDPI();
 
   static bool InitTouchInjection();
   bool InjectTouchPoint(uint32_t aId, LayoutDeviceIntPoint& aPoint,
                         POINTER_FLAGS aFlags, uint32_t aPressure = 1024,
                         uint32_t aOrientation = 90);
