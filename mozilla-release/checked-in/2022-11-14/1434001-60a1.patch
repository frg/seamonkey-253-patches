# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1517260095 -3600
#      Mon Jan 29 22:08:15 2018 +0100
# Node ID 14767d51b28eddfde2116d848a1aff0d265e0177
# Parent  238042a0fc6d51b1035291a6d558e138c28d8c15
Bug 1434001: Handle lambdas in CanRunScriptChecker. r=mystor

Such a shame that the functionDecl() matcher doesn't handle them. I didn't find
a cleaner way to handle them, but I'm a 100% noob with AST matchers, so there
may be a more elegant way to do this.

MozReview-Commit-ID: 3HJQdFpN4hy

diff --git a/build/clang-plugin/CanRunScriptChecker.cpp b/build/clang-plugin/CanRunScriptChecker.cpp
--- a/build/clang-plugin/CanRunScriptChecker.cpp
+++ b/build/clang-plugin/CanRunScriptChecker.cpp
@@ -24,17 +24,17 @@ void CanRunScriptChecker::registerMatche
 
   auto OptionalInvalidExplicitArg = anyOf(
       // We want to find any argument which is invalid.
       hasAnyArgument(InvalidArg),
 
       // This makes this matcher optional.
       anything());
 
-  // Please not that the hasCanRunScriptAnnotation() matchers are not present
+  // Please note that the hasCanRunScriptAnnotation() matchers are not present
   // directly in the cxxMemberCallExpr, callExpr and constructExpr matchers
   // because we check that the corresponding functions can run script later in
   // the checker code.
   AstMatcher->addMatcher(
       expr(
           anyOf(
               // We want to match a method call expression,
               cxxMemberCallExpr(
@@ -88,18 +88,24 @@ private:
   /// This method recursively adds all the methods overriden by the given
   /// paremeter.
   void addAllOverriddenMethodsRecursively(const CXXMethodDecl *Method);
 
   std::unordered_set<const FunctionDecl *> &CanRunScriptFuncs;
 };
 
 void FuncSetCallback::run(const MatchFinder::MatchResult &Result) {
-  const FunctionDecl *Func =
-      Result.Nodes.getNodeAs<FunctionDecl>("canRunScriptFunction");
+  const FunctionDecl *Func;
+  if (auto *Lambda = Result.Nodes.getNodeAs<LambdaExpr>("lambda")) {
+    Func = Lambda->getCallOperator();
+    if (!Func || !hasCustomAnnotation(Func, "moz_can_run_script"))
+      return;
+  } else {
+    Func = Result.Nodes.getNodeAs<FunctionDecl>("canRunScriptFunction");
+  }
 
   CanRunScriptFuncs.insert(Func);
 
   // If this is a method, we check the methods it overrides.
   if (auto *Method = dyn_cast<CXXMethodDecl>(Func)) {
     addAllOverriddenMethodsRecursively(Method);
   }
 }
@@ -127,17 +133,19 @@ void CanRunScriptChecker::buildFuncSet(A
   MatchFinder Finder;
   // We create the callback which will be called when we find a function with
   // a MOZ_CAN_RUN_SCRIPT annotation.
   FuncSetCallback Callback(CanRunScriptFuncs);
   // We add the matcher to the finder, linking it to our callback.
   Finder.addMatcher(
       functionDecl(hasCanRunScriptAnnotation()).bind("canRunScriptFunction"),
       &Callback);
-
+  Finder.addMatcher(
+      lambdaExpr().bind("lambda"),
+      &Callback);
   // We start the analysis, given the ASTContext our main checker is in.
   Finder.matchAST(*Context);
 }
 
 void CanRunScriptChecker::check(const MatchFinder::MatchResult &Result) {
 
   // If the set of functions which can run script is not yet built, then build
   // it.
@@ -209,15 +217,18 @@ void CanRunScriptChecker::check(const Ma
   if (InvalidArg) {
     diag(CallRange.getBegin(), ErrorInvalidArg, DiagnosticIDs::Error)
         << CallRange;
   }
 
   // If the parent function is not marked as MOZ_CAN_RUN_SCRIPT, we emit an
   // error and a not indicating it.
   if (ParentFunction) {
+    assert(!hasCustomAnnotation(ParentFunction, "moz_can_run_script") &&
+           "Matcher missed something");
+
     diag(CallRange.getBegin(), ErrorNonCanRunScriptParent, DiagnosticIDs::Error)
         << CallRange;
 
     diag(ParentFunction->getLocation(), NoteNonCanRunScriptParent,
          DiagnosticIDs::Note);
   }
 }
diff --git a/build/clang-plugin/tests/TestCanRunScript.cpp b/build/clang-plugin/tests/TestCanRunScript.cpp
--- a/build/clang-plugin/tests/TestCanRunScript.cpp
+++ b/build/clang-plugin/tests/TestCanRunScript.cpp
@@ -32,16 +32,29 @@ struct RefCountedBase {
     test2(this);
   }
 
   virtual void method_test3() {
     test();
   }
 };
 
+MOZ_CAN_RUN_SCRIPT void testLambda() {
+  auto doIt = []() MOZ_CAN_RUN_SCRIPT {
+    test();
+  };
+
+  auto doItWrong = []() { // expected-note {{parent function declared here}}
+    test(); // expected-error {{functions marked as MOZ_CAN_RUN_SCRIPT can only be called from functions also marked as MOZ_CAN_RUN_SCRIPT}}
+  };
+
+  doIt();
+  doItWrong();
+}
+
 void test2_parent() { // expected-note {{parent function declared here}}
   test2(new RefCountedBase); // expected-error {{arguments must all be strong refs or parent parameters when calling a function marked as MOZ_CAN_RUN_SCRIPT (including the implicit object argument)}} \
                              // expected-error {{functions marked as MOZ_CAN_RUN_SCRIPT can only be called from functions also marked as MOZ_CAN_RUN_SCRIPT}}
 }
 
 MOZ_CAN_RUN_SCRIPT void test2_parent2() {
   test2(new RefCountedBase); // expected-error {{arguments must all be strong refs or parent parameters when calling a function marked as MOZ_CAN_RUN_SCRIPT (including the implicit object argument)}}
 }
