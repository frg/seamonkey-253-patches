# HG changeset patch
# User Andreas Farre <farre@mozilla.com>
# Date 1506602541 -7200
#      Thu Sep 28 14:42:21 2017 +0200
# Node ID 1126bd3a92f5a0fa36218877ba9212dda9c326fc
# Parent  f5ef8bdecc686caf3f4875f407fef782812f3666
Bug 1403586 - Don't disble budget throttling due to gUM. r=smaug,padenot

diff --git a/dom/base/TimeoutManager.cpp b/dom/base/TimeoutManager.cpp
--- a/dom/base/TimeoutManager.cpp
+++ b/dom/base/TimeoutManager.cpp
@@ -1229,17 +1229,16 @@ ThrottleTimeoutsCallback::Notify(nsITime
 
 }
 
 bool
 TimeoutManager::BudgetThrottlingEnabled(bool aIsBackground) const
 {
   // A window can be throttled using budget if
   // * It isn't active
-  // * If it isn't using user media
   // * If it isn't using WebRTC
   // * If it hasn't got open WebSockets
   // * If it hasn't got active IndexedDB databases
 
   // Note that we allow both foreground and background to be
   // considered for budget throttling. What determines if they are if
   // budget throttling is enabled is the max budget.
   if ((aIsBackground ? gBackgroundThrottlingMaxBudget
@@ -1251,21 +1250,16 @@ TimeoutManager::BudgetThrottlingEnabled(
     return false;
   }
 
   // Check if there are any active IndexedDB databases
   if (mWindow.AsInner()->HasActiveIndexedDBDatabases()) {
     return false;
   }
 
-  // Check if we have active GetUserMedia
-  if (mWindow.AsInner()->HasActiveUserMedia()) {
-    return false;
-  }
-
   // Check if we have active PeerConnection
   if (mWindow.AsInner()->HasActivePeerConnections()) {
     return false;
   }
 
   if (mWindow.AsInner()->HasOpenWebSockets()) {
     return false;
   }
diff --git a/dom/base/nsGlobalWindow.cpp b/dom/base/nsGlobalWindow.cpp
--- a/dom/base/nsGlobalWindow.cpp
+++ b/dom/base/nsGlobalWindow.cpp
@@ -1016,18 +1016,17 @@ nsPIDOMWindow<T>::nsPIDOMWindow(nsPIDOMW
   mDesktopModeViewport(false), mIsRootOuterWindow(false), mInnerWindow(nullptr),
   mOuterWindow(aOuterWindow),
   // Make sure no actual window ends up with mWindowID == 0
   mWindowID(NextWindowID()), mHasNotifiedGlobalCreated(false),
   mMarkedCCGeneration(0), mServiceWorkersTestingEnabled(false),
   mLargeAllocStatus(LargeAllocStatus::NONE),
   mHasTriedToCacheTopInnerWindow(false),
   mNumOfIndexedDBDatabases(0),
-  mNumOfOpenWebSockets(0),
-  mNumOfActiveUserMedia(0)
+  mNumOfOpenWebSockets(0)
 {
   if (aOuterWindow) {
     mTimeoutManager =
       MakeUnique<mozilla::dom::TimeoutManager>(*nsGlobalWindow::Cast(AsInner()));
   }
 }
 
 template<class T>
@@ -4350,17 +4349,16 @@ nsPIDOMWindowInner::SyncStateFromParentW
 void
 nsGlobalWindow::UpdateTopInnerWindow()
 {
   if (!IsInnerWindow() || AsInner()->IsTopInnerWindow()) {
     return;
   }
 
   AsInner()->UpdateWebSocketCount(-(int32_t)mNumOfOpenWebSockets);
-  AsInner()->UpdateUserMediaCount(-(int32_t)mNumOfActiveUserMedia);
 }
 
 void
 nsPIDOMWindowInner::AddPeerConnection()
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(IsInnerWindow());
   mTopInnerWindow ? mTopInnerWindow->mActivePeerConnections++
@@ -4512,42 +4510,16 @@ nsPIDOMWindowInner::HasOpenWebSockets() 
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   return mNumOfOpenWebSockets ||
          (mTopInnerWindow && mTopInnerWindow->mNumOfOpenWebSockets);
 }
 
 void
-nsPIDOMWindowInner::UpdateUserMediaCount(int32_t aDelta)
-{
-  MOZ_ASSERT(NS_IsMainThread());
-
-  if (aDelta == 0) {
-    return;
-  }
-
-  if (mTopInnerWindow && !IsTopInnerWindow()) {
-    mTopInnerWindow->UpdateUserMediaCount(aDelta);
-  }
-
-  MOZ_DIAGNOSTIC_ASSERT(
-    aDelta > 0 || ((aDelta + mNumOfActiveUserMedia) < mNumOfActiveUserMedia));
-
-  mNumOfActiveUserMedia += aDelta;
-}
-
-bool
-nsPIDOMWindowInner::HasActiveUserMedia() const
-{
-  return (mTopInnerWindow ? mTopInnerWindow->mNumOfActiveUserMedia
-                          : mNumOfActiveUserMedia) > 0;
-}
-
-void
 nsPIDOMWindowOuter::MaybeActiveMediaComponents()
 {
   if (IsInnerWindow()) {
     return mOuterWindow->MaybeActiveMediaComponents();
   }
 
   if (mMediaSuspend != nsISuspendedTypes::SUSPENDED_BLOCK) {
     return;
diff --git a/dom/base/nsPIDOMWindow.h b/dom/base/nsPIDOMWindow.h
--- a/dom/base/nsPIDOMWindow.h
+++ b/dom/base/nsPIDOMWindow.h
@@ -743,19 +743,16 @@ protected:
   // could be null and we don't want it to be set multiple times.
   bool mHasTriedToCacheTopInnerWindow;
 
   // The number of active IndexedDB databases. Inner window only.
   uint32_t mNumOfIndexedDBDatabases;
 
   // The number of open WebSockets. Inner window only.
   uint32_t mNumOfOpenWebSockets;
-
-  // The number of active user media. Inner window only.
-  uint32_t mNumOfActiveUserMedia;
 };
 
 #define NS_PIDOMWINDOWINNER_IID \
 { 0x775dabc9, 0x8f43, 0x4277, \
   { 0x9a, 0xdb, 0xf1, 0x99, 0x0d, 0x77, 0xcf, 0xfb } }
 
 #define NS_PIDOMWINDOWOUTER_IID \
   { 0x769693d4, 0xb009, 0x4fe2, \
@@ -934,22 +931,16 @@ public:
 
   // Increase/Decrease the number of open WebSockets.
   void UpdateWebSocketCount(int32_t aDelta);
 
   // Return true if there are any open WebSockets that could block
   // timeout-throttling.
   bool HasOpenWebSockets() const;
 
-  // Increase/Decrease the number of active user media.
-  void UpdateUserMediaCount(int32_t aDelta);
-
-  // Return true if there are any currently ongoing user media.
-  bool HasActiveUserMedia() const;
-
 protected:
   void CreatePerformanceObjectIfNeeded();
 };
 
 NS_DEFINE_STATIC_IID_ACCESSOR(nsPIDOMWindowInner, NS_PIDOMWINDOWINNER_IID)
 
 // NB: It's very very important that these two classes have identical vtables
 // and memory layout!
diff --git a/dom/media/MediaManager.cpp b/dom/media/MediaManager.cpp
--- a/dom/media/MediaManager.cpp
+++ b/dom/media/MediaManager.cpp
@@ -2966,38 +2966,31 @@ MediaManager::AddWindowID(uint64_t aWind
   // when this window is closed or navigated away from.
   // This is safe since we're on main-thread, and the windowlist can only
   // be invalidated from the main-thread (see OnNavigation)
   if (IsWindowStillActive(aWindowId)) {
     MOZ_ASSERT(false, "Window already added");
     return;
   }
 
-  auto* window = nsGlobalWindow::GetInnerWindowWithId(aWindowId);
-  if (window) {
-    window->AsInner()->UpdateUserMediaCount(1);
-  }
-
   GetActiveWindows()->Put(aWindowId, aListener);
 }
 
 void
 MediaManager::RemoveWindowID(uint64_t aWindowId)
 {
   mActiveWindows.Remove(aWindowId);
 
   // get outer windowID
   auto* window = nsGlobalWindow::GetInnerWindowWithId(aWindowId);
   if (!window) {
     LOG(("No inner window for %" PRIu64, aWindowId));
     return;
   }
 
-  window->AsInner()->UpdateUserMediaCount(-1);
-
   nsPIDOMWindowOuter* outer = window->AsInner()->GetOuterWindow();
   if (!outer) {
     LOG(("No outer window for inner %" PRIu64, aWindowId));
     return;
   }
 
   uint64_t outerID = outer->WindowID();
 
