# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1506441409 -7200
#      Tue Sep 26 17:56:49 2017 +0200
# Node ID ddb02772078ec30849fa8299953d465643416d49
# Parent  79dd8db13cab210386e8f3c547952bcbaf7ee3f7
Bug 1401889 - support alt+click to collapse all nodes in markup view;r=pbro

MozReview-Commit-ID: AWadQb6Tn2e

diff --git a/devtools/client/inspector/markup/markup.js b/devtools/client/inspector/markup/markup.js
--- a/devtools/client/inspector/markup/markup.js
+++ b/devtools/client/inspector/markup/markup.js
@@ -1190,16 +1190,29 @@ MarkupView.prototype = {
   /**
    * Collapse the node's children.
    */
   collapseNode: function (node) {
     let container = this.getContainer(node);
     container.setExpanded(false);
   },
 
+  _collapseAll: function (container) {
+    container.setExpanded(false);
+    let children = container.getChildContainers() || [];
+    children.forEach(child => this._collapseAll(child));
+  },
+
+  /**
+   * Collapse the entire tree beneath a node.
+   */
+  collapseAll: function (node) {
+    this._collapseAll(this.getContainer(node));
+  },
+
   /**
    * Returns either the innerHTML or the outerHTML for a remote node.
    *
    * @param  {NodeFront} node
    *         The NodeFront to get the outerHTML / innerHTML for.
    * @param  {Boolean} isOuter
    *         If true, makes the function return the outerHTML,
    *         otherwise the innerHTML.
@@ -1429,32 +1442,34 @@ MarkupView.prototype = {
         }
       });
 
       this.emit("begin-editing");
     });
   },
 
   /**
-   * Mark the given node expanded.
+   * Expand or collapse the given node.
    *
    * @param  {NodeFront} node
-   *         The NodeFront to mark as expanded.
+   *         The NodeFront to update.
    * @param  {Boolean} expanded
-   *         Whether the expand or collapse.
-   * @param  {Boolean} expandDescendants
-   *         Whether to expand all descendants too
+   *         Whether the node should be expanded/collapsed.
+   * @param  {Boolean} applyToDescendants
+   *         Whether all descendants should also be expanded/collapsed
    */
-  setNodeExpanded: function (node, expanded, expandDescendants) {
+  setNodeExpanded: function (node, expanded, applyToDescendants) {
     if (expanded) {
-      if (expandDescendants) {
+      if (applyToDescendants) {
         this.expandAll(node);
       } else {
         this.expandNode(node);
       }
+    } else if (applyToDescendants) {
+      this.collapseAll(node);
     } else {
       this.collapseNode(node);
     }
   },
 
   /**
    * Mark the given node selected, and update the inspector.selection
    * object's NodeFront to keep consistent state between UI and selection.
diff --git a/devtools/client/inspector/markup/test/browser_markup_toggle_03.js b/devtools/client/inspector/markup/test/browser_markup_toggle_03.js
--- a/devtools/client/inspector/markup/test/browser_markup_toggle_03.js
+++ b/devtools/client/inspector/markup/test/browser_markup_toggle_03.js
@@ -1,35 +1,53 @@
 /* vim: set ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
 // Test toggling (expand/collapse) elements by alt-clicking on twisties, which
-// should expand all the descendants
+// should expand/collapse all the descendants
 
 const TEST_URL = URL_ROOT + "doc_markup_toggle.html";
 
 add_task(function* () {
   let {inspector} = yield openInspectorForURL(TEST_URL);
 
   info("Getting the container for the UL parent element");
   let container = yield getContainerForSelector("ul", inspector);
 
-  info("Alt-clicking on the UL parent expander, and waiting for children");
+  info("Alt-clicking on collapsed expander should expand all children");
   let onUpdated = inspector.once("inspector-updated");
   EventUtils.synthesizeMouseAtCenter(container.expander, {altKey: true},
     inspector.markup.doc.defaultView);
   yield onUpdated;
   yield waitForMultipleChildrenUpdates(inspector);
 
   info("Checking that all nodes exist and are expanded");
-  let nodeList = yield inspector.walker.querySelectorAll(
-    inspector.walker.rootNode, "ul, li, span, em");
-  let nodeFronts = yield nodeList.items();
+  let nodeFronts = yield getNodeFronts(inspector);
   for (let nodeFront of nodeFronts) {
     let nodeContainer = getContainerForNodeFront(nodeFront, inspector);
     ok(nodeContainer, "Container for node " + nodeFront.tagName + " exists");
     ok(nodeContainer.expanded,
       "Container for node " + nodeFront.tagName + " is expanded");
   }
+
+  info("Alt-clicking on expanded expander should collapse all children");
+  EventUtils.synthesizeMouseAtCenter(container.expander, {altKey: true},
+    inspector.markup.doc.defaultView);
+  yield waitForMultipleChildrenUpdates(inspector);
+  // No need to wait for inspector-updated here since we are not retrieving new nodes.
+
+  info("Checking that all nodes are collapsed");
+  nodeFronts = yield getNodeFronts(inspector);
+  for (let nodeFront of nodeFronts) {
+    let nodeContainer = getContainerForNodeFront(nodeFront, inspector);
+    ok(!nodeContainer.expanded,
+      "Container for node " + nodeFront.tagName + " is collapsed");
+  }
 });
+
+function* getNodeFronts(inspector) {
+  let nodeList = yield inspector.walker.querySelectorAll(
+    inspector.walker.rootNode, "ul, li, span, em");
+  return nodeList.items();
+}
