# HG changeset patch
# User David Parks <daparks@mozilla.com>
# Date 1605715030 0
# Node ID e38cc205b04ccf7d48ae98d20659caa1375e4abe
# Parent  b716d9071a9484fb3b8e515eace8a02dd2edce4d
Bug 1675349: Update tests under browser/components for the removal of plugins. r=jmathies,mixedpuppy

UPDATED
-------

browser/components/extensions/test/browser/browser_ext_browsingData_pluginData.js
Test plugindata part of the browsingData extension's use of nsIClearDataService.
Have test attempt to clear non-existent data.  Test that browsingData APIs do not cause exceptions.

REMOVED
-------
browser/components/enterprisepolicies/tests/browser/browser_policy_disable_flash_plugin.js
Bug 1429169
Flash ctp/disabled by enterprise policy.

Differential Revision: https://phabricator.services.mozilla.com/D95907

diff --git a/browser/components/extensions/test/browser/browser_ext_browsingData_pluginData.js b/browser/components/extensions/test/browser/browser_ext_browsingData_pluginData.js
--- a/browser/components/extensions/test/browser/browser_ext_browsingData_pluginData.js
+++ b/browser/components/extensions/test/browser/browser_ext_browsingData_pluginData.js
@@ -1,66 +1,21 @@
 /* -*- Mode: indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set sts=2 sw=2 et tw=80: */
 "use strict";
 
-XPCOMUtils.defineLazyServiceGetter(this, "pluginHost",
-                                   "@mozilla.org/plugin/host;1",
-                                   "nsIPluginHost");
-
-// Returns the chrome side nsIPluginTag for the test plugin.
-function getTestPlugin() {
-  let tags = pluginHost.getPluginTags();
-  let plugin = tags.find(tag => tag.name == "Test Plug-in");
-  if (!plugin) {
-    ok(false, "Unable to find plugin");
-  }
-  return plugin;
-}
-
 const TEST_ROOT = getRootDirectory(gTestPath).replace("chrome://mochitests/content/", "http://127.0.0.1:8888/");
 const TEST_URL = TEST_ROOT + "file_clearplugindata.html";
 const REFERENCE_DATE = Date.now();
-const PLUGIN_TAG = getTestPlugin();
-
-/* Due to layout being async, "PluginBindAttached" may trigger later. This
-   returns a Promise that resolves once we've forced a layout flush, which
-   triggers the PluginBindAttached event to fire. This trick only works if
-   there is some sort of plugin in the page.
- */
-function promiseUpdatePluginBindings(browser) {
-  return ContentTask.spawn(browser, {}, async function() {
-    let doc = content.document;
-    let elems = doc.getElementsByTagName("embed");
-    if (elems && elems.length > 0) {
-      elems[0].clientTop; // eslint-disable-line no-unused-expressions
-    }
-  });
-}
-
-function stored(needles) {
-  let something = pluginHost.siteHasData(PLUGIN_TAG, null);
-  if (!needles) {
-    return something;
-  }
-
-  if (!something) {
-    return false;
-  }
-
-  if (needles.every(value => pluginHost.siteHasData(PLUGIN_TAG, value))) {
-    return true;
-  }
-
-  return false;
-}
 
 add_task(async function testPluginData() {
   function background() {
     browser.test.onMessage.addListener(async(msg, options) => {
+      // Plugins are disabled.  We are simply testing that these async APIs
+      // complete without exceptions.
       if (msg == "removePluginData") {
         await browser.browsingData.removePluginData(options);
       } else {
         await browser.browsingData.remove(options, {pluginData: true});
       }
       browser.test.sendMessage("pluginDataRemoved");
     });
   }
@@ -68,65 +23,52 @@ add_task(async function testPluginData()
   let extension = ExtensionTestUtils.loadExtension({
     background,
     manifest: {
       permissions: ["browsingData"],
     },
   });
 
   async function testRemovalMethod(method) {
+    // NB: Since pplugins are disabled, there is never any data to clear.
+    // We are really testing that these operations are no-ops.
+
     // Clear plugin data with no since value.
 
     // Load page to set data for the plugin.
     let tab = await BrowserTestUtils.openNewForegroundTab(gBrowser, TEST_URL);
-    await promiseUpdatePluginBindings(gBrowser.selectedBrowser);
-
-    ok(stored(["foo.com", "bar.com", "baz.com", "qux.com"]),
-       "Data stored for sites");
 
     extension.sendMessage(method, {});
     await extension.awaitMessage("pluginDataRemoved");
 
-    ok(!stored(null), "All data cleared");
     await BrowserTestUtils.removeTab(tab);
 
     // Clear history with recent since value.
 
     // Load page to set data for the plugin.
     tab = await BrowserTestUtils.openNewForegroundTab(gBrowser, TEST_URL);
-    await promiseUpdatePluginBindings(gBrowser.selectedBrowser);
-
-    ok(stored(["foo.com", "bar.com", "baz.com", "qux.com"]),
-       "Data stored for sites");
 
     extension.sendMessage(method, {since: REFERENCE_DATE - 20000});
     await extension.awaitMessage("pluginDataRemoved");
 
-    ok(stored(["bar.com", "qux.com"]), "Data stored for sites");
-    ok(!stored(["foo.com"]), "Data cleared for foo.com");
-    ok(!stored(["baz.com"]), "Data cleared for baz.com");
     await BrowserTestUtils.removeTab(tab);
 
     // Clear history with old since value.
 
     // Load page to set data for the plugin.
     tab = await BrowserTestUtils.openNewForegroundTab(gBrowser, TEST_URL);
-    await promiseUpdatePluginBindings(gBrowser.selectedBrowser);
-
-    ok(stored(["foo.com", "bar.com", "baz.com", "qux.com"]),
-       "Data stored for sites");
 
     extension.sendMessage(method, {since: REFERENCE_DATE - 1000000});
     await extension.awaitMessage("pluginDataRemoved");
 
-    ok(!stored(null), "All data cleared");
     await BrowserTestUtils.removeTab(tab);
   }
 
-  PLUGIN_TAG.enabledState = Ci.nsIPluginTag.STATE_ENABLED;
-
+  waitForExplicitFinish();
   await extension.startup();
 
   await testRemovalMethod("removePluginData");
   await testRemovalMethod("remove");
 
   await extension.unload();
+  ok(true, "should get to the end without throwing an exception");
+  finish();
 });
diff --git a/browser/components/extensions/test/browser/file_clearplugindata.html b/browser/components/extensions/test/browser/file_clearplugindata.html
--- a/browser/components/extensions/test/browser/file_clearplugindata.html
+++ b/browser/components/extensions/test/browser/file_clearplugindata.html
@@ -1,30 +1,14 @@
 <!--
   Any copyright is dedicated to the Public Domain.
   http://creativecommons.org/publicdomain/zero/1.0/
 -->
 <html>
   <head>
-    <title>Plugin Clear Site Data sanitize test</title>
+    <title>Plugin Clear Site Data disabled test</title>
 
     <embed id="plugin1" type="application/x-test" width="200" height="200"></embed>
 
   </head>
 
   <body></body>
-
-  <script type="application/javascript">
-    "use strict";
-
-    // Make sure clearing by timerange is supported.
-    let p = document.getElementById("plugin1");
-    p.setSitesWithDataCapabilities(true);
-
-    p.setSitesWithData(
-      "foo.com:0:5," +
-      "bar.com:0:100," +
-      "baz.com:1:5," +
-      "qux.com:1:100"
-    );
-  </script>
-
 </html>
