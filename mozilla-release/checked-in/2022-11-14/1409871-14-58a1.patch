# HG changeset patch
# User Ryan Hunt <rhunt@eqrion.net>
# Date 1508350256 14400
#      Wed Oct 18 14:10:56 2017 -0400
# Node ID 91419a724237c70ce33d155fd4f87b4c7ca3074f
# Parent  fadcb17b622fd4b6c2fb701e8e3c4a5d437d26f6
Cleanup rotated buffer declarations and add some documentation. (bug 1409871 part 14, r=nical)

This organizes the rotated buffer declarations and adds some documentation.

MozReview-Commit-ID: 5r24RiUMM6o

diff --git a/gfx/layers/RotatedBuffer.h b/gfx/layers/RotatedBuffer.h
--- a/gfx/layers/RotatedBuffer.h
+++ b/gfx/layers/RotatedBuffer.h
@@ -76,38 +76,30 @@ public:
     : mBufferRect(aBufferRect)
     , mBufferRotation(aBufferRotation)
     , mDidSelfCopy(false)
   { }
   RotatedBuffer()
     : mDidSelfCopy(false)
   { }
 
-  struct DrawIterator {
-    friend class RotatedBuffer;
-    DrawIterator()
-      : mCount(0)
-    {}
-
-    nsIntRegion mDrawRegion;
-
-  private:
-    uint32_t mCount;
-  };
-
   /*
    * Which buffer should be drawn to/read from.
    */
   enum ContextSource {
     BUFFER_BLACK, // The normal buffer, or buffer with black background when using component alpha.
     BUFFER_WHITE, // The buffer with white background, only valid with component alpha.
     BUFFER_BOTH // The combined black/white buffers, only valid for writing operations, not reading.
   };
-  // It is the callers repsonsibility to ensure aTarget is flushed after calling
-  // this method.
+
+  /**
+   * Draws the contents of this rotated buffer into the specified draw target.
+   * It is the callers repsonsibility to ensure aTarget is flushed after calling
+   * this method.
+   */
   void DrawBufferWithRotation(gfx::DrawTarget* aTarget, ContextSource aSource,
                               float aOpacity = 1.0,
                               gfx::CompositionOp aOperator = gfx::CompositionOp::OP_OVER,
                               gfx::SourceSurface* aMask = nullptr,
                               const gfx::Matrix* aMaskTransform = nullptr) const;
 
   /**
    * Complete the drawing operation. The region to draw must have been
@@ -116,57 +108,38 @@ public:
    */
   void DrawTo(PaintedLayer* aLayer,
               gfx::DrawTarget* aTarget,
               float aOpacity,
               gfx::CompositionOp aOp,
               gfx::SourceSurface* aMask,
               const gfx::Matrix* aMaskTransform);
 
+  /**
+   * Update the specified region of this rotated buffer with the contents
+   * of a source rotated buffer.
+   */
   void UpdateDestinationFrom(const RotatedBuffer& aSource,
                              const nsIntRegion& aUpdateRegion);
 
   /**
-   * Adjusts the buffer to be centered on the destination buffer rect,
-   * and ready to draw the specified bounds. Returns whether a new buffer
-   * needs to be created.
+   * A draw iterator is used to keep track of which quadrant of a rotated
+   * buffer and region of that quadrant is being updated.
    */
-  bool AdjustTo(const gfx::IntRect& aDestBufferRect,
-                const gfx::IntRect& aDrawBounds,
-                bool aCanHaveRotation,
-                bool aCanDrawRotated);
-
-  /**
-   * |BufferRect()| is the rect of device pixels that this
-   * RotatedBuffer covers.  That is what DrawBufferWithRotation()
-   * will paint when it's called.
-   */
-  const gfx::IntRect& BufferRect() const { return mBufferRect; }
-  const gfx::IntPoint& BufferRotation() const { return mBufferRotation; }
+  struct DrawIterator {
+    friend class RotatedBuffer;
+    DrawIterator()
+      : mCount(0)
+    {}
 
-  void SetBufferRect(const gfx::IntRect& aBufferRect) {
-    mBufferRect = aBufferRect;
-  }
-  void SetBufferRotation(const gfx::IntPoint& aBufferRotation) {
-    mBufferRotation = aBufferRotation;
-  }
-
-  bool DidSelfCopy() const { return mDidSelfCopy; }
-  void ClearDidSelfCopy() { mDidSelfCopy = false; }
+    nsIntRegion mDrawRegion;
 
-  virtual gfx::SurfaceFormat GetFormat() const = 0;
-
-  virtual bool HaveBuffer() const = 0;
-  virtual bool HaveBufferOnWhite() const = 0;
-
-  virtual bool IsLocked() = 0;
-  virtual bool Lock(OpenMode aMode) = 0;
-  virtual void Unlock() = 0;
-
-  virtual already_AddRefed<gfx::SourceSurface> GetSourceSurface(ContextSource aSource) const = 0;
+  private:
+    uint32_t mCount;
+  };
 
   /**
    * Get a draw target at the specified resolution for updating |aBounds|,
    * which must be contained within a single quadrant.
    *
    * The result should only be held temporarily by the caller (it will be kept
    * alive by this). Once used it should be returned using ReturnDrawTarget.
    * BorrowDrawTargetForQuadrantUpdate may not be called more than once without
@@ -179,16 +152,73 @@ public:
    */
   gfx::DrawTarget*
   BorrowDrawTargetForQuadrantUpdate(const gfx::IntRect& aBounds,
                                     ContextSource aSource,
                                     DrawIterator* aIter,
                                     bool aSetTransform = true,
                                     gfx::Matrix* aOutTransform = nullptr);
 
+  /**
+   * Adjusts the buffer to be centered on the destination buffer rect,
+   * and ready to draw the specified bounds. Returns whether a new buffer
+   * needs to be created.
+   */
+  bool AdjustTo(const gfx::IntRect& aDestBufferRect,
+                const gfx::IntRect& aDrawBounds,
+                bool aCanHaveRotation,
+                bool aCanDrawRotated);
+
+  /**
+   * |BufferRect()| is the rect of device pixels that this
+   * RotatedBuffer covers.  That is what DrawBufferWithRotation()
+   * will paint when it's called.
+   */
+  const gfx::IntRect& BufferRect() const { return mBufferRect; }
+  const gfx::IntPoint& BufferRotation() const { return mBufferRotation; }
+
+  /**
+   * Overrides the current buffer rect with the specified rect.
+   * Do not do this unless you know what you're doing.
+   */
+  void SetBufferRect(const gfx::IntRect& aBufferRect) {
+    mBufferRect = aBufferRect;
+  }
+
+  /**
+   * Overrides the current buffer rotation with the specified point.
+   * Do not do this unless you know what you're doing.
+   */
+  void SetBufferRotation(const gfx::IntPoint& aBufferRotation) {
+    mBufferRotation = aBufferRotation;
+  }
+
+  /**
+   * Returns whether this buffer did a self copy when adjusting to
+   * a new buffer rect. This is only used externally for syncing
+   * rotated buffers.
+   */
+  bool DidSelfCopy() const { return mDidSelfCopy; }
+
+  /**
+   * Clears the self copy flag.
+   */
+  void ClearDidSelfCopy() { mDidSelfCopy = false; }
+
+  virtual bool IsLocked() = 0;
+  virtual bool Lock(OpenMode aMode) = 0;
+  virtual void Unlock() = 0;
+
+  virtual bool HaveBuffer() const = 0;
+  virtual bool HaveBufferOnWhite() const = 0;
+
+  virtual gfx::SurfaceFormat GetFormat() const = 0;
+
+  virtual already_AddRefed<gfx::SourceSurface> GetSourceSurface(ContextSource aSource) const = 0;
+
   virtual gfx::DrawTarget* GetDTBuffer() const = 0;
   virtual gfx::DrawTarget* GetDTBufferOnWhite() const = 0;
 
 protected:
   virtual ~RotatedBuffer() {}
 
   enum XSide {
     LEFT, RIGHT
@@ -208,31 +238,33 @@ protected:
   void DrawBufferQuadrant(gfx::DrawTarget* aTarget, XSide aXSide, YSide aYSide,
                           ContextSource aSource,
                           float aOpacity,
                           gfx::CompositionOp aOperator,
                           gfx::SourceSurface* aMask,
                           const gfx::Matrix* aMaskTransform) const;
 
   /** The area of the PaintedLayer that is covered by the buffer as a whole */
-  gfx::IntRect             mBufferRect;
+  gfx::IntRect  mBufferRect;
   /**
    * The x and y rotation of the buffer. Conceptually the buffer
    * has its origin translated to mBufferRect.TopLeft() - mBufferRotation,
    * is tiled to fill the plane, and the result is clipped to mBufferRect.
    * So the pixel at mBufferRotation within the buffer is what gets painted at
    * mBufferRect.TopLeft().
    * This is "rotation" in the sense of rotating items in a linear buffer,
    * where items falling off the end of the buffer are returned to the
    * buffer at the other end, not 2D rotation!
    */
-  gfx::IntPoint            mBufferRotation;
-  // When this is true it means that all pixels have moved inside the buffer.
-  // It's not possible to sync with another buffer without a full copy.
-  bool                  mDidSelfCopy;
+  gfx::IntPoint mBufferRotation;
+  /**
+   * When this is true it means that all pixels have moved inside the buffer.
+   * It's not possible to sync with another buffer without a full copy.
+   */
+  bool          mDidSelfCopy;
 };
 
 class RemoteRotatedBuffer : public RotatedBuffer
 {
 public:
   RemoteRotatedBuffer(TextureClient* aClient, TextureClient* aClientOnWhite,
                       const gfx::IntRect& aBufferRect,
                       const gfx::IntPoint& aBufferRotation)
@@ -240,32 +272,31 @@ public:
     , mClient(aClient)
     , mClientOnWhite(aClientOnWhite)
   { }
 
   virtual bool IsLocked() override;
   virtual bool Lock(OpenMode aMode) override;
   virtual void Unlock() override;
 
-  void SyncWithObject(SyncObjectClient* aSyncObject);
-  void Clear();
+  virtual bool HaveBuffer() const override { return !!mClient; }
+  virtual bool HaveBufferOnWhite() const override { return !!mClientOnWhite; }
+
+  virtual gfx::SurfaceFormat GetFormat() const override;
+
+  virtual already_AddRefed<gfx::SourceSurface> GetSourceSurface(ContextSource aSource) const override;
+
+  virtual gfx::DrawTarget* GetDTBuffer() const override;
+  virtual gfx::DrawTarget* GetDTBufferOnWhite() const override;
 
   TextureClient* GetClient() const { return mClient; }
   TextureClient* GetClientOnWhite() const { return mClientOnWhite; }
 
-  virtual gfx::SurfaceFormat GetFormat() const override;
-
-  virtual bool HaveBuffer() const override { return !!mClient; }
-  virtual bool HaveBufferOnWhite() const override { return !!mClientOnWhite; }
-
-  virtual already_AddRefed<gfx::SourceSurface> GetSourceSurface(ContextSource aSource) const override;
-
-protected:
-  virtual gfx::DrawTarget* GetDTBuffer() const override;
-  virtual gfx::DrawTarget* GetDTBufferOnWhite() const override;
+  void SyncWithObject(SyncObjectClient* aSyncObject);
+  void Clear();
 
 private:
   RefPtr<TextureClient> mClient;
   RefPtr<TextureClient> mClientOnWhite;
 
   RefPtr<gfx::DrawTarget> mTarget;
   RefPtr<gfx::DrawTarget> mTargetOnWhite;
 };
@@ -280,24 +311,23 @@ public:
     , mTarget(aTarget)
     , mTargetOnWhite(aTargetOnWhite)
   { }
 
   virtual bool IsLocked() override { return false; }
   virtual bool Lock(OpenMode aMode) override { return true; }
   virtual void Unlock() override {}
 
-  virtual gfx::SurfaceFormat GetFormat() const override;
-
   virtual bool HaveBuffer() const override { return !!mTarget; }
   virtual bool HaveBufferOnWhite() const override { return !!mTargetOnWhite; }
 
+  virtual gfx::SurfaceFormat GetFormat() const override;
+
   virtual already_AddRefed<gfx::SourceSurface> GetSourceSurface(ContextSource aSource) const override;
 
-protected:
   virtual gfx::DrawTarget* GetDTBuffer() const override;
   virtual gfx::DrawTarget* GetDTBufferOnWhite() const override;
 
 private:
   RefPtr<gfx::DrawTarget> mTarget;
   RefPtr<gfx::DrawTarget> mTargetOnWhite;
 };
 
@@ -318,17 +348,16 @@ public:
 
   virtual already_AddRefed<gfx::SourceSurface> GetSourceSurface(ContextSource aSource) const;
 
   virtual gfx::SurfaceFormat GetFormat() const override;
 
   virtual bool HaveBuffer() const { return !!mSource; }
   virtual bool HaveBufferOnWhite() const { return !!mSourceOnWhite; }
 
-protected:
   virtual gfx::DrawTarget* GetDTBuffer() const { return nullptr; }
   virtual gfx::DrawTarget* GetDTBufferOnWhite() const { return nullptr; }
 
 private:
   RefPtr<gfx::SourceSurface> mSource;
   RefPtr<gfx::SourceSurface> mSourceOnWhite;
 };
 
