# HG changeset patch
# User Cameron McCormack <cam@mcc.id.au>
# Date 1508401835 -28800
# Node ID 5970cb3d3673853c29176dde16377699a2463772
# Parent  422f688f96061b5a918623a142d1cedb48924704
Bug 1408311 - Part 4: Fix bug when serializing sanitized style rules. r=xidorn

MozReview-Commit-ID: LBfmRsYSJND

diff --git a/dom/base/nsTreeSanitizer.cpp b/dom/base/nsTreeSanitizer.cpp
--- a/dom/base/nsTreeSanitizer.cpp
+++ b/dom/base/nsTreeSanitizer.cpp
@@ -1071,23 +1071,19 @@ nsTreeSanitizer::MustPrune(int32_t aName
   }
   if (nsGkAtoms::style == aLocal) {
     return true;
   }
   return false;
 }
 
 bool
-nsTreeSanitizer::SanitizeStyleDeclaration(DeclarationBlock* aDeclaration,
-                                          nsAutoString& aRuleText)
+nsTreeSanitizer::SanitizeStyleDeclaration(DeclarationBlock* aDeclaration)
 {
-  bool didSanitize =
-    aDeclaration->RemovePropertyByID(eCSSProperty__moz_binding);
-  aDeclaration->ToString(aRuleText);
-  return didSanitize;
+  return aDeclaration->RemovePropertyByID(eCSSProperty__moz_binding);
 }
 
 bool
 nsTreeSanitizer::SanitizeStyleSheet(const nsAString& aOriginal,
                                     nsAString& aSanitized,
                                     nsIDocument* aDocument,
                                     nsIURI* aBaseURI)
 {
@@ -1156,22 +1152,21 @@ nsTreeSanitizer::SanitizeStyleSheet(cons
         break;
       }
       case mozilla::css::Rule::STYLE_RULE: {
         // For style rules, we will just look for and remove the
         // -moz-binding properties.
         auto styleRule = static_cast<BindingStyleRule*>(rule);
         DeclarationBlock* styleDecl = styleRule->GetDeclarationBlock();
         MOZ_ASSERT(styleDecl);
+        if (SanitizeStyleDeclaration(styleDecl)) {
+          didSanitize = true;
+        }
         nsAutoString decl;
-        bool sanitized = SanitizeStyleDeclaration(styleDecl, decl);
-        didSanitize = sanitized || didSanitize;
-        if (!sanitized) {
-          styleRule->GetCssText(decl);
-        }
+        styleRule->GetCssText(decl);
         aSanitized.Append(decl);
       }
     }
   }
   return didSanitize;
 }
 
 void
@@ -1205,18 +1200,19 @@ nsTreeSanitizer::SanitizeAttributes(mozi
           // Pass the CSS Loader object to the parser, to allow parser error
           // reports to include the outer window ID.
           nsCSSParser parser(document->CSSLoader());
           decl = parser.ParseStyleAttribute(value, document->GetDocumentURI(),
                                             aElement->GetBaseURIForStyleAttr(),
                                             document->NodePrincipal());
         }
         if (decl) {
-          nsAutoString cleanValue;
-          if (SanitizeStyleDeclaration(decl, cleanValue)) {
+          if (SanitizeStyleDeclaration(decl)) {
+            nsAutoString cleanValue;
+            decl->ToString(cleanValue);
             aElement->SetAttr(kNameSpaceID_None,
                               nsGkAtoms::style,
                               cleanValue,
                               false);
           }
         }
         continue;
       }
diff --git a/dom/base/nsTreeSanitizer.h b/dom/base/nsTreeSanitizer.h
--- a/dom/base/nsTreeSanitizer.h
+++ b/dom/base/nsTreeSanitizer.h
@@ -146,25 +146,22 @@ class MOZ_STACK_CLASS nsTreeSanitizer {
      * @return true if the attribute was removed and false otherwise
      */
     bool SanitizeURL(mozilla::dom::Element* aElement,
                        int32_t aNamespace,
                        nsIAtom* aLocalName);
 
     /**
      * Checks a style rule for the presence of the 'binding' CSS property and
-     * removes that property from the rule and reserializes in case the
-     * property was found.
+     * removes that property from the rule.
      *
      * @param aDeclaration The style declaration to check
-     * @param aRuleText the serialized mutated rule if the method returns true
      * @return true if the rule was modified and false otherwise
      */
-    bool SanitizeStyleDeclaration(mozilla::DeclarationBlock* aDeclaration,
-                                  nsAutoString& aRuleText);
+    bool SanitizeStyleDeclaration(mozilla::DeclarationBlock* aDeclaration);
 
     /**
      * Parses a style sheet and reserializes it with the 'binding' property
      * removed if it was present.
      *
      * @param aOrigin the original style sheet source
      * @param aSanitized the reserialization without 'binding'; only valid if
      *                   this method return true
diff --git a/dom/base/test/test_bug650776.html b/dom/base/test/test_bug650776.html
--- a/dom/base/test/test_bug650776.html
+++ b/dom/base/test/test_bug650776.html
@@ -24,18 +24,18 @@ is(s.sanitize("foo", 0), "<html><head></
 // Scripts get removed
 is(s.sanitize("<script>\u003c/script>", 0), "<html><head></head><body></body></html>", "Wrong sanitizer result 2");
 // Event handlers get removed
 is(s.sanitize("<a onclick='boom()'></a>", 0), "<html><head></head><body><a></a></body></html>", "Wrong sanitizer result 3");
 // By default, styles are removed
 is(s.sanitize("<style>p { color: red; }</style><p style='background-color: blue;'></p>", 0), "<html><head></head><body><p></p></body></html>", "Wrong sanitizer result 4");
 // Can allow styles
 is(s.sanitize("<style>p { color: red; }</style><p style='background-color: blue;'></p>", u.SanitizerAllowStyle), '<html><head><style>p { color: red; }</style></head><body><p style="background-color: blue;"></p></body></html>', "Wrong sanitizer result 5");
-// -moz-binding gets dropped when styles allowed; however, reconstructing the p { ... } part seems broken!
-todo_is(s.sanitize("<style>p { color: red; -moz-binding: url(foo); }</style><p style='background-color: blue; -moz-binding: url(foo);'></p>", u.SanitizerAllowStyle), '<html><head><style>p { color: red; }</style></head><body><p style="background-color: blue;"></p></body></html>', "Wrong sanitizer result 6");
+// -moz-binding gets dropped when styles allowed
+is(s.sanitize("<style>p { color: red; -moz-binding: url(foo); }</style><p style='background-color: blue; -moz-binding: url(foo);'></p>", u.SanitizerAllowStyle), '<html><head><style>p { color: red; }</style></head><body><p style="background-color: blue;"></p></body></html>', "Wrong sanitizer result 6");
 // Various cid: embeds only cases
 is(s.sanitize("<img src='foo.html'>", u.SanitizerCidEmbedsOnly), '<html><head></head><body><img></body></html>', "Wrong sanitizer result 7");
 is(s.sanitize("<img src='cid:foo'>", u.SanitizerCidEmbedsOnly), '<html><head></head><body><img src="cid:foo"></body></html>', "Wrong sanitizer result 8");
 is(s.sanitize("<img src='data:image/png,'>", u.SanitizerCidEmbedsOnly), '<html><head></head><body><img></body></html>', "Wrong sanitizer result 9");
 is(s.sanitize("<img src='http://mochi.test/'>", u.SanitizerCidEmbedsOnly), '<html><head></head><body><img></body></html>', "Wrong sanitizer result 10");
 is(s.sanitize("<a href='http://mochi.test/'></a>", u.SanitizerCidEmbedsOnly), '<html><head></head><body><a href="http://mochi.test/"></a></body></html>', "Wrong sanitizer result 11");
 is(s.sanitize("<body background='http://mochi.test/'>", u.SanitizerCidEmbedsOnly), '<html><head></head><body></body></html>', "Wrong sanitizer result 12");
 is(s.sanitize("<body background='cid:foo'>", u.SanitizerCidEmbedsOnly), '<html><head></head><body background="cid:foo"></body></html>', "Wrong sanitizer result 13");
