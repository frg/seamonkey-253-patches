# HG changeset patch
# User Brian Hackett <bhackett1024@gmail.com>
# Date 1506431046 25200
# Node ID 2cc6a5ffe72d0afa5a0a536f0f2803f761fc7131
# Parent  5367c5e216883a873ce2aa870e865500c2a8f99c
Bug 1402580 - Support yield callback used when entering the system zone group in the JS shell, r=jandem.

diff --git a/js/src/gc/ZoneGroup.cpp b/js/src/gc/ZoneGroup.cpp
--- a/js/src/gc/ZoneGroup.cpp
+++ b/js/src/gc/ZoneGroup.cpp
@@ -60,17 +60,17 @@ ZoneGroup::~ZoneGroup()
 }
 
 void
 ZoneGroup::enter(JSContext* cx)
 {
     if (ownerContext().context() == cx) {
         MOZ_ASSERT(enterCount);
     } else {
-        if (useExclusiveLocking) {
+        if (useExclusiveLocking()) {
             MOZ_ASSERT(!usedByHelperThread());
             while (ownerContext().context() != nullptr) {
                 cx->yieldToEmbedding();
             }
         }
         MOZ_RELEASE_ASSERT(ownerContext().context() == nullptr);
         MOZ_ASSERT(enterCount == 0);
         ownerContext_ = CooperatingContext(cx);
diff --git a/js/src/gc/ZoneGroup.h b/js/src/gc/ZoneGroup.h
--- a/js/src/gc/ZoneGroup.h
+++ b/js/src/gc/ZoneGroup.h
@@ -36,17 +36,17 @@ class ZoneGroup
     // The context with exclusive access to this zone group.
     UnprotectedData<CooperatingContext> ownerContext_;
 
     // The number of times the context has entered this zone group.
     UnprotectedData<size_t> enterCount;
 
     // If this flag is true, then we may need to block before entering this zone
     // group. Blocking happens using JSContext::yieldToEmbedding.
-    UnprotectedData<bool> useExclusiveLocking;
+    UnprotectedData<bool> useExclusiveLocking_;
 
   public:
     CooperatingContext& ownerContext() { return ownerContext_.ref(); }
     void* addressOfOwnerContext() { return &ownerContext_.ref().cx; }
 
     void enter(JSContext* cx);
     void leave();
     bool canEnterWithoutYielding(JSContext* cx);
@@ -96,18 +96,19 @@ class ZoneGroup
     bool init();
 
     inline Nursery& nursery();
     inline gc::StoreBuffer& storeBuffer();
 
     inline bool isCollecting();
     inline bool isGCScheduled();
 
-    // See the useExclusiveLocking field above.
-    void setUseExclusiveLocking() { useExclusiveLocking = true; }
+    // See the useExclusiveLocking_ field above.
+    void setUseExclusiveLocking() { useExclusiveLocking_ = true; }
+    bool useExclusiveLocking() { return useExclusiveLocking_; }
 
     // Delete an empty zone after its contents have been merged.
     void deleteEmptyZone(Zone* zone);
 
 #ifdef DEBUG
   private:
     // The number of possible bailing places encounters before forcefully bailing
     // in that place. Zero means inactive.
diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -1458,16 +1458,19 @@ ConvertTranscodeResultToJSException(JSCo
 
       case JS::TranscodeResult_Throw:
         MOZ_ASSERT(cx->isExceptionPending());
         return false;
     }
 }
 
 static bool
+CooperativeThreadMayYield(JSContext* cx);
+
+static bool
 Evaluate(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
     if (args.length() < 1 || args.length() > 2) {
         JS_ReportErrorNumberASCII(cx, my_GetErrorMessage, nullptr,
                                   args.length() < 1 ? JSSMSG_NOT_ENOUGH_ARGS : JSSMSG_TOO_MANY_ARGS,
                                   "evaluate");
@@ -1548,24 +1551,35 @@ Evaluate(JSContext* cx, unsigned argc, V
         if (!JS_GetProperty(cx, opts, "zoneGroup", &v))
             return false;
         if (!v.isUndefined()) {
             if (global != JS_GetGlobalForObject(cx, &args.callee())) {
                 JS_ReportErrorASCII(cx, "zoneGroup and global cannot both be specified.");
                 return false;
             }
 
-            // Find all eligible globals to execute in: any global in another
-            // zone group which has not been entered by a cooperative thread.
+            // Find all eligible globals to execute in.
             JS::AutoObjectVector eligibleGlobals(cx);
             for (CompartmentsIter c(cx->runtime(), SkipAtoms); !c.done(); c.next()) {
-                if (!c->zone()->group()->ownerContext().context() &&
-                    c->maybeGlobal() &&
-                    !cx->runtime()->isSelfHostingGlobal(c->maybeGlobal()))
-                {
+                // Compartments without globals and the self hosting global may
+                // not be entered.
+                if (!c->maybeGlobal() || cx->runtime()->isSelfHostingGlobal(c->maybeGlobal()))
+                    continue;
+
+                // Globals in zone groups which are not in use by a cooperative
+                // thread may be entered.
+                if (!c->zone()->group()->ownerContext().context()) {
+                    if (!eligibleGlobals.append(c->maybeGlobal()))
+                        return false;
+                }
+
+                // Globals in zone groups which use exclusive locking may be
+                // entered, in which case this thread will yield until the zone
+                // group is available.
+                if (c->zone()->group()->useExclusiveLocking() && CooperativeThreadMayYield(cx)) {
                     if (!eligibleGlobals.append(c->maybeGlobal()))
                         return false;
                 }
             }
 
             if (eligibleGlobals.empty()) {
                 JS_ReportErrorASCII(cx, "zoneGroup can only be used if another"
                                     " cooperative thread has called cooperativeYield(true).");
@@ -3346,47 +3360,59 @@ CooperativeYield()
     // is another thread to do so.
     if (cooperationState->numThreads) {
         uint64_t count = cooperationState->yieldCount;
         cooperationState->cvar.wait(lock, [&] { return cooperationState->yieldCount != count; });
     }
 }
 
 static bool
-CooperativeYieldThread(JSContext* cx, unsigned argc, Value* vp)
-{
-    CallArgs args = CallArgsFromVp(argc, vp);
-
-    if (!cx->runtime()->gc.canChangeActiveContext(cx)) {
-        JS_ReportErrorASCII(cx, "Cooperating multithreading context switches are not currently allowed");
-        return false;
-    }
-
-    if (GetShellContext(cx)->isWorker) {
-        JS_ReportErrorASCII(cx, "Worker threads cannot yield");
-        return false;
-    }
-
-    if (cooperationState->singleThreaded) {
-        JS_ReportErrorASCII(cx, "Yielding is not allowed while single threaded");
-        return false;
-    }
+CooperativeThreadMayYield(JSContext* cx)
+{
+    if (!cx->runtime()->gc.canChangeActiveContext(cx))
+        return false;
+
+    if (GetShellContext(cx)->isWorker)
+        return false;
+
+    if (cooperationState->singleThreaded)
+        return false;
 
     // To avoid contention issues between threads, yields are not allowed while
     // a thread has access to zone groups other than its original one, i.e. if
     // the thread is inside an evaluate() call with a different zone group.
     // This is not a limit which the browser has, but is necessary in the
     // shell: the shell can have arbitrary interleavings between cooperative
     // threads, whereas the browser has more control over which threads are
     // running at different times.
     for (ZoneGroupsIter group(cx->runtime()); !group.done(); group.next()) {
-        if (group->ownerContext().context() == cx && group != cx->zone()->group()) {
-            JS_ReportErrorASCII(cx, "Yielding is not allowed while owning multiple zone groups");
-            return false;
-        }
+        if (group->ownerContext().context() == cx && group != cx->zone()->group())
+            return false;
+    }
+
+    return true;
+}
+
+static void
+CooperativeYieldCallback(JSContext* cx)
+{
+    MOZ_ASSERT(CooperativeThreadMayYield(cx));
+    CooperativeBeginWait(cx);
+    CooperativeYield();
+    CooperativeEndWait(cx);
+}
+
+static bool
+CooperativeYieldThread(JSContext* cx, unsigned argc, Value* vp)
+{
+    CallArgs args = CallArgsFromVp(argc, vp);
+
+    if (!CooperativeThreadMayYield(cx)) {
+        JS_ReportErrorASCII(cx, "Yielding is not currently allowed");
+        return false;
     }
 
     {
         Maybe<JS::AutoRelinquishZoneGroups> artzg;
         if ((args.length() > 0) && ToBoolean(args[0]))
             artzg.emplace(cx);
 
         CooperativeBeginWait(cx);
@@ -3476,16 +3502,18 @@ WorkerMain(void* arg)
     MOZ_ASSERT(!!input->parentRuntime != !!input->siblingContext);
 
     JSContext* cx = input->parentRuntime
          ? JS_NewContext(8L * 1024L * 1024L, 2L * 1024L * 1024L, input->parentRuntime)
          : JS_NewCooperativeContext(input->siblingContext);
     if (!cx)
         return;
 
+    SetCooperativeYieldCallback(cx, CooperativeYieldCallback);
+
     UniquePtr<ShellContext> sc = MakeUnique<ShellContext>(cx);
     if (!sc)
         return;
 
     auto guard = mozilla::MakeScopeExit([&] {
         if (cx)
             JS_DestroyContext(cx);
         if (input->siblingContext) {
@@ -8814,16 +8842,17 @@ main(int argc, char** argv, char** envp)
 #endif
 
     js::SetPreserveWrapperCallback(cx, DummyPreserveWrapperCallback);
 
     cooperationState = js_new<CooperationState>();
     JS::SetSingleThreadedExecutionCallbacks(cx,
                                             CooperativeBeginSingleThreadedExecution,
                                             CooperativeEndSingleThreadedExecution);
+    SetCooperativeYieldCallback(cx, CooperativeYieldCallback);
 
     result = Shell(cx, &op, envp);
 
 #ifdef DEBUG
     if (OOM_printAllocationCount)
         printf("OOM max count: %" PRIu64 "\n", js::oom::counter);
 #endif
 
