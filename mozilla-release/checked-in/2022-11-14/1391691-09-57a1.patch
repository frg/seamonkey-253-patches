# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1503077502 -3600
# Node ID ef813a1a750fd9b7d2fc1b61548d3b8853cd9015
# Parent  23c470ddb8348c9134024cefd42a776d5a6ae407
Bug 1391691 - Make WebDriver:MaximizeWindow idempotent. r=automatedtester

MozReview-Commit-ID: EJ0VQOTWysg

diff --git a/testing/marionette/driver.js b/testing/marionette/driver.js
--- a/testing/marionette/driver.js
+++ b/testing/marionette/driver.js
@@ -3050,17 +3050,19 @@ GeckoDriver.prototype.minimizeWindow = a
     });
   }
 
   return this.curBrowser.rect;
 };
 
 /**
  * Synchronously maximizes the user agent window as if the user pressed
- * the maximize button, or restores it if it is already maximized.
+ * the maximize button.
+ *
+ * No action is taken if the window is already maximized.
  *
  * Not supported on Fennec.
  *
  * @return {Object.<string, number>}
  *     Window rect.
  *
  * @throws {UnsupportedOperationError}
  *     Not available for current application.
@@ -3090,58 +3092,54 @@ GeckoDriver.prototype.maximizeWindow = a
           curSize.outerHeight != origSize.outerHeight) {
         resolve();
       } else {
         reject();
       }
     });
   }
 
-  let modeChangeEv;
-  await new TimedPromise(resolve => {
-    modeChangeEv = resolve;
-    win.addEventListener("sizemodechange", modeChangeEv, {once: true});
-
-    if (win.windowState == win.STATE_MAXIMIZED) {
-      win.restore();
-    } else {
+  let state = WindowState.from(win.windowState);
+  if (state != WindowState.Maximized) {
+    await new TimedPromise(resolve => {
+      win.addEventListener("sizemodechange", resolve, {once: true});
       win.maximize();
-    }
-  }, {throws: null});
-  win.removeEventListener("sizemodechange", modeChangeEv);
-
-  // Transitioning into a window state is asynchronous on Linux, and we
-  // cannot rely on sizemodechange to accurately tell us when the
-  // transition has completed.
-  //
-  // To counter for this we wait for the window size to change, which
-  // it usually will.  On platforms where the transition is synchronous,
-  // the wait will have the cost of one iteration because the size will
-  // have changed as part of the transition.  Where the platform
-  // is asynchronous, the cost may be greater as we have to poll
-  // continuously until we see a change, but it ensures conformity in
-  // behaviour.
-  //
-  // Certain window managers, however, do not have a concept of maximised
-  // windows and here sizemodechange may never fire.  Indeed, if the
-  // window covers the maximum available screen real estate, the window
-  // size may also not change.  In this circumstance, which admittedly
-  // is a somewhat bizarre edge case, we assume that the timeout of
-  // waiting for sizemodechange to fire is sufficient to give the window
-  // enough time to transition itself into whatever form or shape the
-  // WM is programmed to give it.
-  await windowSizeChange();
+    }, {throws: null});
+
+    // Transitioning into a window state is asynchronous on Linux,
+    // and we cannot rely on sizemodechange to accurately tell us when
+    // the transition has completed.
+    //
+    // To counter for this we wait for the window size to change, which
+    // it usually will.  On platforms where the transition is synchronous,
+    // the wait will have the cost of one iteration because the size
+    // will have changed as part of the transition.  Where the platform is
+    // asynchronous, the cost may be greater as we have to poll
+    // continuously until we see a change, but it ensures conformity in
+    // behaviour.
+    //
+    // Certain window managers, however, do not have a concept of
+    // maximised windows and here sizemodechange may never fire.  Indeed,
+    // if the window covers the maximum available screen real estate,
+    // the window size may also not change.  In this circumstance,
+    // which admittedly is a somewhat bizarre edge case, we assume that
+    // the timeout of waiting for sizemodechange to fire is sufficient
+    // to give the window enough time to transition itself into whatever
+    // form or shape the WM is programmed to give it.
+    await windowSizeChange();
+  }
 
   return this.curBrowser.rect;
 };
 
 /**
  * Synchronously sets the user agent window to full screen as if the user
- * had done "View > Enter Full Screen", or restores it if it is already
- * in full screen.
+ * had done "View > Enter Full Screen".
+ *
+ * No action is taken if the window is already in full screen mode.
  *
  * Not supported on Fennec.
  *
  * @return {Map.<string, number>}
  *     Window rect.
  *
  * @throws {UnsupportedOperationError}
  *     Not available for current application.
diff --git a/testing/marionette/harness/marionette_harness/tests/unit/test_window_maximize.py b/testing/marionette/harness/marionette_harness/tests/unit/test_window_maximize.py
--- a/testing/marionette/harness/marionette_harness/tests/unit/test_window_maximize.py
+++ b/testing/marionette/harness/marionette_harness/tests/unit/test_window_maximize.py
@@ -40,20 +40,16 @@ class TestWindowMaximize(MarionetteTestC
                 "current width {current} should be greater than or equal to max width {max}"
                 .format(delta=delta, current=actual["width"], max=self.max["width"] - delta))
         self.assertGreaterEqual(
             actual["height"], self.max["height"] - delta,
             msg="Window height is not within {delta} px of availHeight: "
                 "current height {current} should be greater than or equal to max height {max}"
                 .format(delta=delta, current=actual["height"], max=self.max["height"] - delta))
 
-    def assert_window_restored(self, actual):
-        self.assertEqual(self.original_size["width"], actual["width"])
-        self.assertEqual(self.original_size["height"], actual["height"])
-
     def assert_window_rect(self, rect):
         self.assertIn("width", rect)
         self.assertIn("height", rect)
         self.assertIn("x", rect)
         self.assertIn("y", rect)
         self.assertIsInstance(rect["width"], int)
         self.assertIsInstance(rect["height"], int)
         self.assertIsInstance(rect["x"], int)
@@ -61,24 +57,26 @@ class TestWindowMaximize(MarionetteTestC
 
     def test_maximize(self):
         maximize_resp = self.marionette.maximize_window()
         self.assert_window_rect(maximize_resp)
         window_rect_resp = self.marionette.window_rect
         self.assertEqual(maximize_resp, window_rect_resp)
         self.assert_window_maximized(maximize_resp)
 
-    def test_maximize_twice_restores(self):
+    def test_maximize_twice_is_idempotent(self):
         maximized = self.marionette.maximize_window()
         self.assert_window_maximized(maximized)
 
-        restored = self.marionette.maximize_window()
-        self.assert_window_restored(restored)
+        still_maximized = self.marionette.maximize_window()
+        self.assert_window_maximized(still_maximized)
 
     def test_stress(self):
         for i in range(1, 25):
             expect_maximized = bool(i % 2)
 
-            rect = self.marionette.maximize_window()
             if expect_maximized:
+                rect = self.marionette.maximize_window()
                 self.assert_window_maximized(rect)
             else:
-                self.assert_window_restored(rect)
+                rect = self.marionette.set_window_rect(width=800, height=600)
+                self.assertEqual(800, rect["width"])
+                self.assertEqual(600, rect["height"])
