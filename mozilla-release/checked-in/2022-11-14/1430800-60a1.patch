# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1516278055 28800
# Node ID 1e5c359b9fe38d29b65fb6c872505c124895a9f2
# Parent  f41272d9655e8d204e05b29c5f08d85aee190f7f
Bug 1430800: Optimize String.raw by avoiding rest-parameter array allocation. r=evilpie

diff --git a/js/src/builtin/String.js b/js/src/builtin/String.js
--- a/js/src/builtin/String.js
+++ b/js/src/builtin/String.js
@@ -787,67 +787,63 @@ function String_toLocaleUpperCase() {
 
     if (requestedLocale === undefined)
         requestedLocale = DefaultLocale();
 
     // Steps 7-16.
     return intl_toLocaleUpperCase(string, requestedLocale);
 }
 
-/* ES6 Draft May 22, 2014 21.1.2.4 */
-function String_static_raw(callSite, ...substitutions) {
-    // Step 1 (implicit).
-    // Step 2.
-    var numberOfSubstitutions = substitutions.length;
+// ES2018 draft rev 8fadde42cf6a9879b4ab0cb6142b31c4ee501667
+// 21.1.2.4 String.raw ( template, ...substitutions )
+function String_static_raw(callSite/*, ...substitutions*/) {
+    // Steps 1-2 (not applicable).
 
-    // Steps 3-4.
+    // Step 3.
     var cooked = ToObject(callSite);
 
-    // Steps 5-7.
+    // Step 4.
     var raw = ToObject(cooked.raw);
 
-    // Steps 8-10.
+    // Step 5.
     var literalSegments = ToLength(raw.length);
 
-    // Step 11.
-    if (literalSegments <= 0)
+    // Step 6.
+    if (literalSegments === 0)
         return "";
 
-    // Step 12.
-    var resultString = "";
-
-    // Step 13.
-    var nextIndex = 0;
+    // Special case for |String.raw `<literal>`| callers to avoid falling into
+    // the loop code below.
+    if (literalSegments === 1)
+        return ToString(raw[0]);
 
-    // Step 14.
-    while (true) {
-        // Steps a-d.
-        var nextSeg = ToString(raw[nextIndex]);
-
-        // Step e.
-        resultString = resultString + nextSeg;
+    // Steps 7-9 were reordered to use the arguments object instead of a rest
+    // parameter, because the former is currently more optimized.
+    //
+    // String.raw intersperses the substitution elements between the literal
+    // segments, i.e. a substitution is added iff there are still pending
+    // literal segments. Furthermore by moving the access to |raw[0]| outside
+    // of the loop, we can use |nextIndex| to index into both, the |raw| array
+    // and the arguments object.
 
-        // Step f.
-        if (nextIndex + 1 === literalSegments)
-            // Step f.i.
-            return resultString;
+    // Steps 7 (implicit) and 9.a-c.
+    var resultString = ToString(raw[0]);
 
-        // Steps g-j.
-        var nextSub;
-        if (nextIndex < numberOfSubstitutions)
-            nextSub = ToString(substitutions[nextIndex]);
-        else
-            nextSub = "";
+    // Steps 8-9, 9.d, and 9.i.
+    for (var nextIndex = 1; nextIndex < literalSegments; nextIndex++) {
+        // Steps 9.e-h.
+        if (nextIndex < arguments.length)
+            resultString += ToString(arguments[nextIndex]);
 
-        // Step k.
-        resultString = resultString + nextSub;
+        // Steps 9.a-c.
+        resultString += ToString(raw[nextIndex]);
+    }
 
-        // Step l.
-        nextIndex++;
-    }
+    // Step 9.d.i.
+    return resultString;
 }
 
 /**
  * Compare String str1 against String str2, using the locale and collation
  * options provided.
  *
  * Mozilla proprietary.
  * Spec: https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/String#String_generic_methods
diff --git a/js/src/jsstr.cpp b/js/src/jsstr.cpp
--- a/js/src/jsstr.cpp
+++ b/js/src/jsstr.cpp
@@ -3864,17 +3864,17 @@ js::str_fromCodePoint(JSContext* cx, uns
     args.rval().setString(str);
     return true;
 }
 
 static const JSFunctionSpec string_static_methods[] = {
     JS_INLINABLE_FN("fromCharCode", js::str_fromCharCode, 1, 0, StringFromCharCode),
     JS_INLINABLE_FN("fromCodePoint", js::str_fromCodePoint, 1, 0, StringFromCodePoint),
 
-    JS_SELF_HOSTED_FN("raw",             "String_static_raw",           2,0),
+    JS_SELF_HOSTED_FN("raw",             "String_static_raw",           1,0),
     JS_SELF_HOSTED_FN("substring",       "String_static_substring",     3,0),
     JS_SELF_HOSTED_FN("substr",          "String_static_substr",        3,0),
     JS_SELF_HOSTED_FN("slice",           "String_static_slice",         3,0),
 
     JS_SELF_HOSTED_FN("match",           "String_generic_match",        2,0),
     JS_SELF_HOSTED_FN("replace",         "String_generic_replace",      3,0),
     JS_SELF_HOSTED_FN("search",          "String_generic_search",       2,0),
     JS_SELF_HOSTED_FN("split",           "String_generic_split",        3,0),
