# HG changeset patch
# User Josh Matthews <josh@joshmatthews.net>
# Date 1507047928 18000
#      Tue Oct 03 11:25:28 2017 -0500
# Node ID ccd48f2cb1eb49321295252aef94d7d482c2f101
# Parent  c5cde6a43056b898c67d1504900881405e4600f7
servo: Merge #18414 - Reduce the size of AnimationValue code (from jdm:animdecl-codesize); r=SimonSapin

According to bloaty, these commits shave off 25k from code in the animated_properties module.

---
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors

Source-Repo: https://github.com/servo/servo
Source-Revision: eff768679d3aa94d0c6256f18f823f67f89a54ef

diff --git a/servo/components/style/properties/helpers/animated_properties.mako.rs b/servo/components/style/properties/helpers/animated_properties.mako.rs
--- a/servo/components/style/properties/helpers/animated_properties.mako.rs
+++ b/servo/components/style/properties/helpers/animated_properties.mako.rs
@@ -402,17 +402,17 @@ impl AnimationValue {
     pub fn from_declaration(
         decl: &PropertyDeclaration,
         context: &mut Context,
         extra_custom_properties: Option<<&Arc<::custom_properties::CustomPropertiesMap>>,
         initial: &ComputedValues
     ) -> Option<Self> {
         use properties::LonghandId;
 
-        match *decl {
+        let animatable = match *decl {
             % for prop in data.longhands:
             % if prop.animatable:
             PropertyDeclaration::${prop.camel_case}(ref val) => {
                 context.for_non_inherited_property =
                     % if prop.style_struct.inherited:
                         None;
                     % else:
                         Some(LonghandId::${prop.camel_case});
@@ -422,84 +422,84 @@ impl AnimationValue {
                     longhands::system_font::resolve_system_font(sf, context);
                 }
             % endif
             % if prop.boxed:
             let computed = (**val).to_computed_value(context);
             % else:
             let computed = val.to_computed_value(context);
             % endif
-            Some(AnimationValue::${prop.camel_case}(
+            AnimationValue::${prop.camel_case}(
             % if prop.is_animatable_with_computed_value:
                 computed
             % else:
                 computed.to_animated_value()
             % endif
-            ))
+            )
             },
             % endif
             % endfor
             PropertyDeclaration::CSSWideKeyword(id, keyword) => {
                 match id {
                     // We put all the animatable properties first in the hopes
                     // that it might increase match locality.
                     % for prop in data.longhands:
                     % if prop.animatable:
                     LonghandId::${prop.camel_case} => {
-                        let computed = match keyword {
+                        let style_struct = match keyword {
                             % if not prop.style_struct.inherited:
                                 CSSWideKeyword::Unset |
                             % endif
                             CSSWideKeyword::Initial => {
-                                let initial_struct = initial.get_${prop.style_struct.name_lower}();
-                                initial_struct.clone_${prop.ident}()
+                                initial.get_${prop.style_struct.name_lower}()
                             },
                             % if prop.style_struct.inherited:
                                 CSSWideKeyword::Unset |
                             % endif
                             CSSWideKeyword::Inherit => {
-                                let inherit_struct = context.builder
-                                                            .get_parent_${prop.style_struct.name_lower}();
-                                inherit_struct.clone_${prop.ident}()
+                                context.builder
+                                       .get_parent_${prop.style_struct.name_lower}()
                             },
                         };
+                        let computed = style_struct.clone_${prop.ident}();
                         % if not prop.is_animatable_with_computed_value:
                         let computed = computed.to_animated_value();
                         % endif
-                        Some(AnimationValue::${prop.camel_case}(computed))
+                        AnimationValue::${prop.camel_case}(computed)
                     },
                     % endif
                     % endfor
                     % for prop in data.longhands:
                     % if not prop.animatable:
-                    LonghandId::${prop.camel_case} => None,
+                    LonghandId::${prop.camel_case} => return None,
                     % endif
                     % endfor
                 }
             },
             PropertyDeclaration::WithVariables(id, ref unparsed) => {
                 let substituted = {
                     let custom_properties =
                         extra_custom_properties.or_else(|| context.style().custom_properties());
 
                     unparsed.substitute_variables(
                         id,
                         custom_properties,
                         context.quirks_mode
                     )
                 };
-                AnimationValue::from_declaration(
+                return AnimationValue::from_declaration(
                     &substituted,
                     context,
                     extra_custom_properties,
                     initial,
                 )
             },
-            _ => None // non animatable properties will get included because of shorthands. ignore.
-        }
+            _ => return None // non animatable properties will get included because of shorthands. ignore.
+        };
+        Some(animatable)
     }
 
     /// Get an AnimationValue for an AnimatableLonghand from a given computed values.
     pub fn from_computed_values(
         property: &LonghandId,
         computed_values: &ComputedValues
     ) -> Option<Self> {
         Some(match *property {
@@ -519,66 +519,77 @@ impl AnimationValue {
             }
             % endif
             % endfor
             _ => return None,
         })
     }
 }
 
+fn animate_discrete<T: Clone>(this: &T, other: &T, procedure: Procedure) -> Result<T, ()> {
+    if let Procedure::Interpolate { progress } = procedure {
+        Ok(if progress < 0.5 { this.clone() } else { other.clone() })
+    } else {
+        Err(())
+    }
+}
+
 impl Animate for AnimationValue {
     fn animate(&self, other: &Self, procedure: Procedure) -> Result<Self, ()> {
-        match (self, other) {
+        let value = match (self, other) {
             % for prop in data.longhands:
             % if prop.animatable:
             % if prop.animation_value_type != "discrete":
             (
                 &AnimationValue::${prop.camel_case}(ref this),
                 &AnimationValue::${prop.camel_case}(ref other),
             ) => {
-                Ok(AnimationValue::${prop.camel_case}(
+                AnimationValue::${prop.camel_case}(
                     this.animate(other, procedure)?,
-                ))
+                )
             },
             % else:
             (
                 &AnimationValue::${prop.camel_case}(ref this),
                 &AnimationValue::${prop.camel_case}(ref other),
             ) => {
-                if let Procedure::Interpolate { progress } = procedure {
-                    Ok(AnimationValue::${prop.camel_case}(
-                        if progress < 0.5 { this.clone() } else { other.clone() },
-                    ))
-                } else {
-                    Err(())
-                }
+                AnimationValue::${prop.camel_case}(
+                    animate_discrete(this, other, procedure)?
+                )
             },
             % endif
             % endif
             % endfor
             _ => {
                 panic!("Unexpected AnimationValue::animate call, got: {:?}, {:?}", self, other);
             }
-        }
+        };
+        Ok(value)
     }
 }
 
 impl ComputeSquaredDistance for AnimationValue {
     fn compute_squared_distance(&self, other: &Self) -> Result<SquaredDistance, ()> {
+        match *self {
+            % for i, prop in enumerate([p for p in data.longhands if p.animatable and p.animation_value_type == "discrete"]):
+            % if i > 0:
+            |
+            % endif
+            AnimationValue::${prop.camel_case}(..)
+            % endfor
+            => return Err(()),
+            _ => (),
+        }
         match (self, other) {
             % for prop in data.longhands:
             % if prop.animatable:
             % if prop.animation_value_type != "discrete":
             (&AnimationValue::${prop.camel_case}(ref this), &AnimationValue::${prop.camel_case}(ref other)) => {
                 this.compute_squared_distance(other)
             },
-            % else:
-            (&AnimationValue::${prop.camel_case}(_), &AnimationValue::${prop.camel_case}(_)) => {
-                Err(())
-            },
             % endif
             % endif
             % endfor
             _ => {
                 panic!(
                     "computed values should be of the same property, got: {:?}, {:?}",
                     self,
                     other
