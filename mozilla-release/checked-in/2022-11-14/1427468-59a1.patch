# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1514703029 -32400
#      Sun Dec 31 15:50:29 2017 +0900
# Node ID aa60e61cdc9581821fc5d6a1246cdd170b2e52ab
# Parent  1a49d75b29d3dfadf29989363e0f80a8e07bb375
Bug 1427468 - Allow to run mach python without a virtualenv. r=nalexander

Sometimes, one just wants to run a one-off script with access to all
(or most) the libraries available like mozbuild, etc. but without
the weight of the whole virtualenv, which implies having an objdir
setup, etc.

One of my use cases is to run our preprocessor before the objdir is even
setup, and I'd rather not have one automatically created.

diff --git a/python/mach_commands.py b/python/mach_commands.py
--- a/python/mach_commands.py
+++ b/python/mach_commands.py
@@ -2,16 +2,17 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import argparse
 import logging
 import os
+import sys
 import tempfile
 
 from concurrent.futures import (
     ThreadPoolExecutor,
     as_completed,
     thread,
 )
 
@@ -30,28 +31,39 @@ from mach.decorators import (
     Command,
 )
 
 
 @CommandProvider
 class MachCommands(MachCommandBase):
     @Command('python', category='devenv',
              description='Run Python.')
+    @CommandArgument('--no-virtualenv', action='store_true',
+                     help='Do not set up a virtualenv')
     @CommandArgument('args', nargs=argparse.REMAINDER)
-    def python(self, args):
+    def python(self, no_virtualenv, args):
         # Avoid logging the command
         self.log_manager.terminal_handler.setLevel(logging.CRITICAL)
 
-        self._activate_virtualenv()
+        # Note: subprocess requires native strings in os.environ on Windows.
+        append_env = {
+            b'PYTHONDONTWRITEBYTECODE': str('1'),
+        }
 
-        return self.run_process([self.virtualenv_manager.python_path] + args,
+        if no_virtualenv:
+            python_path = sys.executable
+            append_env[b'PYTHONPATH'] = os.pathsep.join(sys.path)
+        else:
+            self._activate_virtualenv()
+            python_path = self.virtualenv_manager.python_path
+
+        return self.run_process([python_path] + args,
                                 pass_thru=True,  # Allow user to run Python interactively.
                                 ensure_exit_code=False,  # Don't throw on non-zero exit code.
-                                # Note: subprocess requires native strings in os.environ on Windows
-                                append_env={b'PYTHONDONTWRITEBYTECODE': str('1')})
+                                append_env=append_env)
 
     @Command('python-test', category='testing',
              description='Run Python unit tests with an appropriate test runner.')
     @CommandArgument('-v', '--verbose',
                      default=False,
                      action='store_true',
                      help='Verbose output.')
     @CommandArgument('--stop',
