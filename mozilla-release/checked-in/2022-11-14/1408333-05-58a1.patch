# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1508839360 -7200
#      Tue Oct 24 12:02:40 2017 +0200
# Node ID b64ffa1024fc39d52f0963ac172a9687534821db
# Parent  3c13a8e409773d3241fe3b9c199a8f578da7c04e
Bug 1408333 Get rid of nsIIPCBackgroundChildCreateCallback - part 5 - IPCBlob, r=asuth

diff --git a/dom/file/ipc/IPCBlobInputStreamThread.cpp b/dom/file/ipc/IPCBlobInputStreamThread.cpp
--- a/dom/file/ipc/IPCBlobInputStreamThread.cpp
+++ b/dom/file/ipc/IPCBlobInputStreamThread.cpp
@@ -6,17 +6,16 @@
 
 #include "IPCBlobInputStreamThread.h"
 
 #include "mozilla/StaticMutex.h"
 #include "mozilla/SystemGroup.h"
 #include "mozilla/TaskCategory.h"
 #include "mozilla/ipc/BackgroundChild.h"
 #include "mozilla/ipc/PBackgroundChild.h"
-#include "nsIIPCBackgroundChildCreateCallback.h"
 #include "nsXPCOMPrivate.h"
 
 namespace mozilla {
 
 using namespace ipc;
 
 namespace dom {
 
@@ -37,65 +36,54 @@ public:
      mozilla::StaticMutexAutoLock lock(gIPCBlobThreadMutex);
      MOZ_ASSERT(gIPCBlobThread);
      gIPCBlobThread->InitializeOnMainThread();
      return NS_OK;
   }
 };
 
 class MigrateActorRunnable final : public Runnable
-                                 , public nsIIPCBackgroundChildCreateCallback
 {
 public:
-  NS_DECL_ISUPPORTS_INHERITED
-
   explicit MigrateActorRunnable(IPCBlobInputStreamChild* aActor)
     : Runnable("dom::MigrateActorRunnable")
     , mActor(aActor)
   {
     MOZ_ASSERT(mActor);
   }
 
   NS_IMETHOD
   Run() override
   {
-    BackgroundChild::GetOrCreateForCurrentThread(this);
-    return NS_OK;
-  }
-
-  void
-  ActorFailed() override
-  {
-    // We cannot continue. We are probably shutting down.
-  }
-
-  void
-  ActorCreated(mozilla::ipc::PBackgroundChild* aActor) override
-  {
     MOZ_ASSERT(mActor->State() == IPCBlobInputStreamChild::eInactiveMigrating);
 
-    if (aActor->SendPIPCBlobInputStreamConstructor(mActor, mActor->ID(),
-                                                   mActor->Size())) {
+    PBackgroundChild* actorChild =
+      BackgroundChild::GetOrCreateForCurrentThread();
+    if (!actorChild) {
+      return NS_OK;
+    }
+
+    if (actorChild->SendPIPCBlobInputStreamConstructor(mActor, mActor->ID(),
+                                                       mActor->Size())) {
       // We need manually to increase the reference for this actor because the
       // IPC allocator method is not triggered. The Release() is called by IPDL
       // when the actor is deleted.
       mActor.get()->AddRef();
       mActor->Migrated();
     }
+
+    return NS_OK;
   }
 
 private:
   ~MigrateActorRunnable() = default;
 
   RefPtr<IPCBlobInputStreamChild> mActor;
 };
 
-NS_IMPL_ISUPPORTS_INHERITED(MigrateActorRunnable, Runnable,
-                  nsIIPCBackgroundChildCreateCallback)
-
 } // anonymous
 
 NS_IMPL_ISUPPORTS(IPCBlobInputStreamThread, nsIObserver, nsIEventTarget)
 
 /* static */ bool
 IPCBlobInputStreamThread::IsOnFileEventTarget(nsIEventTarget* aEventTarget)
 {
   MOZ_ASSERT(aEventTarget);
