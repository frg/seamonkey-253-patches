# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1517948087 0
# Node ID d45bbc02ad0ddab649f1c7387c6942a1a19381a6
# Parent  cfffd33853e366d801506c39703889df46ce9350
Bug 499025 - Don't open an error pop-up if the user cancels printing. r=bobowen

This fixes PrintTargetWindows::BeginPrinting to detect when the
user cancels and have it return NS_ERROR_ABORT in that case.

The rest of the changes are simply making sure that the various
call points up the call stack don't print a warning message if
NS_ERROR_ABORT is returned up from
PrintTargetWindows::BeginPrinting.

MozReview-Commit-ID: 6xZ5SPje6TT

diff --git a/gfx/src/nsDeviceContext.cpp b/gfx/src/nsDeviceContext.cpp
--- a/gfx/src/nsDeviceContext.cpp
+++ b/gfx/src/nsDeviceContext.cpp
@@ -536,16 +536,20 @@ nsDeviceContext::BeginDocument(const nsA
     if (NS_SUCCEEDED(rv)) {
         if (mDeviceContextSpec) {
            rv = mDeviceContextSpec->BeginDocument(aTitle, aPrintToFileName,
                                                   aStartPage, aEndPage);
         }
         mIsCurrentlyPrintingDoc = true;
     }
 
+    // Warn about any failure (except user cancelling):
+    NS_WARNING_ASSERTION(NS_SUCCEEDED(rv) || rv == NS_ERROR_ABORT,
+                         "nsDeviceContext::BeginDocument failed");
+
     return rv;
 }
 
 
 nsresult
 nsDeviceContext::EndDocument(void)
 {
     MOZ_ASSERT(mIsCurrentlyPrintingDoc,
diff --git a/gfx/thebes/PrintTargetWindows.cpp b/gfx/thebes/PrintTargetWindows.cpp
--- a/gfx/thebes/PrintTargetWindows.cpp
+++ b/gfx/thebes/PrintTargetWindows.cpp
@@ -71,18 +71,31 @@ PrintTargetWindows::BeginPrinting(const 
 
   nsString docName(aPrintToFileName);
   docinfo.cbSize = sizeof(docinfo);
   docinfo.lpszDocName = titleStr.Length() > 0 ? titleStr.get() : L"Mozilla Document";
   docinfo.lpszOutput = docName.Length() > 0 ? docName.get() : nullptr;
   docinfo.lpszDatatype = nullptr;
   docinfo.fwType = 0;
 
-  ::StartDocW(mDC, &docinfo);
-
+  // If the user selected Microsoft Print to PDF or XPS Document Printer, then
+  // the following StartDoc call will put up a dialog window to prompt the
+  // user to provide the name and location of the file to be saved.  A zero or
+  // negative return value indicates failure.  In that case we want to check
+  // whether that is because the user hit Cancel, since we want to treat that
+  // specially to avoid notifying the user that the print "failed" in that
+  // case.
+  // XXX We should perhaps introduce a new NS_ERROR_USER_CANCELLED errer.
+  int result = ::StartDocW(mDC, &docinfo);
+  if (result <= 0) {
+    if (::GetLastError() == ERROR_CANCELLED) {
+      return NS_ERROR_ABORT;
+    }
+    return NS_ERROR_FAILURE;
+  }
   return NS_OK;
 }
 
 nsresult
 PrintTargetWindows::EndPrinting()
 {
   int result = ::EndDoc(mDC);
   return (result <= 0) ? NS_ERROR_FAILURE : NS_OK;
diff --git a/layout/printing/ipc/RemotePrintJobParent.cpp b/layout/printing/ipc/RemotePrintJobParent.cpp
--- a/layout/printing/ipc/RemotePrintJobParent.cpp
+++ b/layout/printing/ipc/RemotePrintJobParent.cpp
@@ -79,17 +79,19 @@ RemotePrintJobParent::InitializePrintDev
   mPrintDeviceContext = new nsDeviceContext();
   rv = mPrintDeviceContext->InitForPrinting(deviceContextSpec);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   rv = mPrintDeviceContext->BeginDocument(aDocumentTitle, aPrintToFile,
                                           aStartPage, aEndPage);
-  if (NS_WARN_IF(NS_FAILED(rv))) {
+  if (NS_FAILED(rv)) {
+    NS_WARNING_ASSERTION(rv == NS_ERROR_ABORT,
+                         "Failed to initialize print device");
     return rv;
   }
 
   return NS_OK;
 }
 
 nsresult
 RemotePrintJobParent::PrepareNextPageFD(FileDescriptor* aFd)
diff --git a/layout/printing/nsPrintEngine.cpp b/layout/printing/nsPrintEngine.cpp
--- a/layout/printing/nsPrintEngine.cpp
+++ b/layout/printing/nsPrintEngine.cpp
@@ -1868,17 +1868,21 @@ nsPrintEngine::SetupToPrintContent()
     if (seqFrame) {
       seqFrame->StartPrint(printData->mPrintObject->mPresContext,
                            printData->mPrintSettings, docTitleStr, docURLStr);
     }
   }
 
   PR_PL(("****************** Begin Document ************************\n"));
 
-  NS_ENSURE_SUCCESS(rv, rv);
+  if (NS_FAILED(rv)) {
+    NS_WARNING_ASSERTION(rv == NS_ERROR_ABORT,
+                         "Failed to begin document for printing");
+    return rv;
+  }
 
   // This will print the docshell document
   // when it completes asynchronously in the DonePrintingPages method
   // it will check to see if there are more docshells to be printed and
   // then PrintDocContent will be called again.
 
   if (mIsDoingPrinting) {
     PrintDocContent(printData->mPrintObject, rv); // ignore return value
@@ -1975,17 +1979,18 @@ nsPrintEngine::AfterNetworkPrint(bool aH
   if (mIsDoingPrinting) {
     rv = DocumentReadyForPrinting();
   } else {
     rv = FinishPrintPreview();
   }
 
   /* cleaup on failure + notify user */
   if (aHandleError && NS_FAILED(rv)) {
-    NS_WARNING("nsPrintEngine::AfterNetworkPrint failed");
+    NS_WARNING_ASSERTION(rv == NS_ERROR_ABORT,
+                         "nsPrintJob::AfterNetworkPrint failed");
     CleanupOnFailure(rv, !mIsDoingPrinting);
   }
 
   return rv;
 }
 
 ////////////////////////////////////////////////////////////////////////////////
 // nsIWebProgressListener
