# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1508491133 18000
# Node ID 6764b27645f0a168b61e3f2aa2477e56801ea107
# Parent  28848fec0e26b2ed4aea7717c9ee3e754f821b75
servo: Merge #18962 - Support matching for ::-moz-tree-* pseudo-elements (from upsuper:tree-pseudos); r=emilio

This is the Servo side change of [bug 1397644](https://bugzilla.mozilla.org/show_bug.cgi?id=1397644).

Source-Repo: https://github.com/servo/servo
Source-Revision: b1e6f05ae455748f6091ddf81c1c0488e09546a1

diff --git a/servo/components/selectors/context.rs b/servo/components/selectors/context.rs
--- a/servo/components/selectors/context.rs
+++ b/servo/components/selectors/context.rs
@@ -104,16 +104,20 @@ where
     /// When this is None, :scope will match the root element.
     ///
     /// See https://drafts.csswg.org/selectors-4/#scope-pseudo
     pub scope_element: Option<OpaqueElement>,
 
     /// The current nesting level of selectors that we're matching.
     pub nesting_level: usize,
 
+    /// An optional hook function for checking whether a pseudo-element
+    /// should match when matching_mode is ForStatelessPseudoElement.
+    pub pseudo_element_matching_fn: Option<&'a Fn(&Impl::PseudoElement) -> bool>,
+
     quirks_mode: QuirksMode,
     classes_and_ids_case_sensitivity: CaseSensitivity,
     _impl: ::std::marker::PhantomData<Impl>,
 }
 
 impl<'a, Impl> MatchingContext<'a, Impl>
 where
     Impl: SelectorImpl,
@@ -147,16 +151,17 @@ where
             bloom_filter,
             visited_handling,
             nth_index_cache,
             quirks_mode,
             relevant_link_found: false,
             classes_and_ids_case_sensitivity: quirks_mode.classes_and_ids_case_sensitivity(),
             scope_element: None,
             nesting_level: 0,
+            pseudo_element_matching_fn: None,
             _impl: ::std::marker::PhantomData,
         }
     }
 
     /// The quirks mode of the document.
     #[inline]
     pub fn quirks_mode(&self) -> QuirksMode {
         self.quirks_mode
diff --git a/servo/components/selectors/matching.rs b/servo/components/selectors/matching.rs
--- a/servo/components/selectors/matching.rs
+++ b/servo/components/selectors/matching.rs
@@ -382,19 +382,30 @@ where
     E: Element,
     F: FnMut(&E, ElementSelectorFlags),
 {
     // If this is the special pseudo-element mode, consume the ::pseudo-element
     // before proceeding, since the caller has already handled that part.
     if context.nesting_level == 0 &&
         context.matching_mode == MatchingMode::ForStatelessPseudoElement {
         // Consume the pseudo.
-        let pseudo = iter.next().unwrap();
-        debug_assert!(matches!(*pseudo, Component::PseudoElement(..)),
-                      "Used MatchingMode::ForStatelessPseudoElement in a non-pseudo selector");
+        match *iter.next().unwrap() {
+            Component::PseudoElement(ref pseudo) => {
+                if let Some(ref f) = context.pseudo_element_matching_fn {
+                    if !f(pseudo) {
+                        return false;
+                    }
+                }
+            }
+            _ => {
+                debug_assert!(false,
+                              "Used MatchingMode::ForStatelessPseudoElement \
+                               in a non-pseudo selector");
+            }
+        }
 
         // The only other parser-allowed Component in this sequence is a state
         // class. We just don't match in that case.
         if let Some(s) = iter.next() {
             debug_assert!(matches!(*s, Component::NonTSPseudoClass(..)),
                           "Someone messed up pseudo-element parsing");
             return false;
         }
diff --git a/servo/components/style/gecko/pseudo_element.rs b/servo/components/style/gecko/pseudo_element.rs
--- a/servo/components/style/gecko/pseudo_element.rs
+++ b/servo/components/style/gecko/pseudo_element.rs
@@ -41,17 +41,17 @@ impl PseudoElement {
     ///
     /// We resolve the others lazily, see `Servo_ResolvePseudoStyle`.
     pub fn cascade_type(&self) -> PseudoElementCascadeType {
         if self.is_eager() {
             debug_assert!(!self.is_anon_box());
             return PseudoElementCascadeType::Eager
         }
 
-        if self.is_anon_box() {
+        if self.is_precomputed() {
             return PseudoElementCascadeType::Precomputed
         }
 
         PseudoElementCascadeType::Lazy
     }
 
     /// Whether the pseudo-element should inherit from the default computed
     /// values instead of from the parent element.
@@ -132,17 +132,17 @@ impl PseudoElement {
     #[inline]
     pub fn skip_item_based_display_fixup(&self) -> bool {
         (self.flags() & structs::CSS_PSEUDO_ELEMENT_IS_FLEX_OR_GRID_ITEM) == 0
     }
 
     /// Whether this pseudo-element is precomputed.
     #[inline]
     pub fn is_precomputed(&self) -> bool {
-        self.is_anon_box()
+        self.is_anon_box() && !self.is_tree_pseudo_element()
     }
 
     /// Covert non-canonical pseudo-element to canonical one, and keep a
     /// canonical one as it is.
     pub fn canonical(&self) -> PseudoElement {
         match *self {
             PseudoElement::MozPlaceholder => PseudoElement::Placeholder,
             _ => self.clone(),
diff --git a/servo/components/style/gecko/pseudo_element_definition.mako.rs b/servo/components/style/gecko/pseudo_element_definition.mako.rs
--- a/servo/components/style/gecko/pseudo_element_definition.mako.rs
+++ b/servo/components/style/gecko/pseudo_element_definition.mako.rs
@@ -3,17 +3,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /// Gecko's pseudo-element definition.
 #[derive(Clone, Debug, Eq, Hash, PartialEq)]
 pub enum PseudoElement {
     % for pseudo in PSEUDOS:
         /// ${pseudo.value}
         % if pseudo.is_tree_pseudo_element():
-        ${pseudo.capitalized()}(Box<[String]>),
+        ${pseudo.capitalized()}(Box<[Atom]>),
         % else:
         ${pseudo.capitalized()},
         % endif
     % endfor
 }
 
 /// Important: If you change this, you should also update Gecko's
 /// nsCSSPseudoElements::IsEagerlyCascadedInServo.
@@ -22,16 +22,22 @@ pub enum PseudoElement {
 <% SIMPLE_PSEUDOS = [pseudo for pseudo in PSEUDOS if not pseudo.is_tree_pseudo_element()] %>
 
 /// The number of eager pseudo-elements.
 pub const EAGER_PSEUDO_COUNT: usize = ${len(EAGER_PSEUDOS)};
 
 /// The number of non-functional pseudo-elements.
 pub const SIMPLE_PSEUDO_COUNT: usize = ${len(SIMPLE_PSEUDOS)};
 
+/// The number of tree pseudo-elements.
+pub const TREE_PSEUDO_COUNT: usize = ${len(TREE_PSEUDOS)};
+
+/// The number of all pseudo-elements.
+pub const PSEUDO_COUNT: usize = ${len(PSEUDOS)};
+
 /// The list of eager pseudos.
 pub const EAGER_PSEUDOS: [PseudoElement; EAGER_PSEUDO_COUNT] = [
     % for eager_pseudo_name in EAGER_PSEUDOS:
     PseudoElement::${eager_pseudo_name},
     % endfor
 ];
 
 <%def name="pseudo_element_variant(pseudo, tree_arg='..')">\
@@ -44,34 +50,32 @@ impl PseudoElement {
     pub fn atom(&self) -> Atom {
         match *self {
             % for pseudo in PSEUDOS:
                 ${pseudo_element_variant(pseudo)} => atom!("${pseudo.value}"),
             % endfor
         }
     }
 
-    /// Returns an index if the pseudo-element is a simple (non-functional)
-    /// pseudo.
+    /// Returns an index of the pseudo-element.
     #[inline]
-    pub fn simple_index(&self) -> Option<usize> {
+    pub fn index(&self) -> usize {
         match *self {
-            % for i, pseudo in enumerate(SIMPLE_PSEUDOS):
-            ${pseudo_element_variant(pseudo)} => Some(${i}),
+            % for i, pseudo in enumerate(PSEUDOS):
+            ${pseudo_element_variant(pseudo)} => ${i},
             % endfor
-            _ => None,
         }
     }
 
     /// Returns an array of `None` values.
     ///
     /// FIXME(emilio): Integer generics can't come soon enough.
-    pub fn simple_pseudo_none_array<T>() -> [Option<T>; SIMPLE_PSEUDO_COUNT] {
+    pub fn pseudo_none_array<T>() -> [Option<T>; PSEUDO_COUNT] {
         [
-            ${",\n".join(["None" for pseudo in SIMPLE_PSEUDOS])}
+            ${",\n            ".join(["None" for pseudo in PSEUDOS])}
         ]
     }
 
     /// Whether this pseudo-element is an anonymous box.
     #[inline]
     pub fn is_anon_box(&self) -> bool {
         match *self {
             % for pseudo in PSEUDOS:
@@ -85,16 +89,27 @@ impl PseudoElement {
 
     /// Whether this pseudo-element is eagerly-cascaded.
     #[inline]
     pub fn is_eager(&self) -> bool {
         matches!(*self,
                  ${" | ".join(map(lambda name: "PseudoElement::{}".format(name), EAGER_PSEUDOS))})
     }
 
+    /// Whether this pseudo-element is tree pseudo-element.
+    #[inline]
+    pub fn is_tree_pseudo_element(&self) -> bool {
+        match *self {
+            % for pseudo in TREE_PSEUDOS:
+            ${pseudo_element_variant(pseudo)} => true,
+            % endfor
+            _ => false,
+        }
+    }
+
     /// Gets the flags associated to this pseudo-element, or 0 if it's an
     /// anonymous box.
     pub fn flags(&self) -> u32 {
         match *self {
             % for pseudo in PSEUDOS:
                 ${pseudo_element_variant(pseudo)} =>
                 % if pseudo.is_tree_pseudo_element():
                     0,
@@ -127,31 +142,42 @@ impl PseudoElement {
     pub fn pseudo_type(&self) -> CSSPseudoElementType {
         use gecko_bindings::structs::CSSPseudoElementType_InheritingAnonBox;
 
         match *self {
             % for pseudo in PSEUDOS:
                 % if not pseudo.is_anon_box():
                     PseudoElement::${pseudo.capitalized()} => CSSPseudoElementType::${pseudo.original_ident},
                 % elif pseudo.is_tree_pseudo_element():
-                    PseudoElement::${pseudo.capitalized()}(..) => CSSPseudoElementType_InheritingAnonBox,
+                    PseudoElement::${pseudo.capitalized()}(..) => CSSPseudoElementType::XULTree,
                 % elif pseudo.is_inheriting_anon_box():
                     PseudoElement::${pseudo.capitalized()} => CSSPseudoElementType_InheritingAnonBox,
                 % else:
                     PseudoElement::${pseudo.capitalized()} => CSSPseudoElementType::NonInheritingAnonBox,
                 % endif
             % endfor
         }
     }
 
     /// Get a PseudoInfo for a pseudo
     pub fn pseudo_info(&self) -> (*mut structs::nsIAtom, CSSPseudoElementType) {
         (self.atom().as_ptr(), self.pseudo_type())
     }
 
+    /// Get the argument list of a tree pseudo-element.
+    #[inline]
+    pub fn tree_pseudo_args(&self) -> Option<<&[Atom]> {
+        match *self {
+            % for pseudo in TREE_PSEUDOS:
+            PseudoElement::${pseudo.capitalized()}(ref args) => Some(args),
+            % endfor
+            _ => None,
+        }
+    }
+
     /// Construct a pseudo-element from an `Atom`.
     #[inline]
     pub fn from_atom(atom: &Atom) -> Option<Self> {
         % for pseudo in PSEUDOS:
             % if pseudo.is_tree_pseudo_element():
                 // We cannot generate ${pseudo_element_variant(pseudo)} from just an atom.
             % else:
                 if atom == &atom!("${pseudo.value}") {
@@ -172,16 +198,29 @@ impl PseudoElement {
                 if atom == &atom!("${pseudo.value}") {
                     return Some(${pseudo_element_variant(pseudo)});
                 }
             % endif
         % endfor
         None
     }
 
+    /// Construct a tree pseudo-element from atom and args.
+    #[inline]
+    pub fn from_tree_pseudo_atom(atom: &Atom, args: Box<[Atom]>) -> Option<Self> {
+        % for pseudo in PSEUDOS:
+            % if pseudo.is_tree_pseudo_element():
+                if atom == &atom!("${pseudo.value}") {
+                    return Some(PseudoElement::${pseudo.capitalized()}(args));
+                }
+            % endif
+        % endfor
+        None
+    }
+
     /// Constructs an atom from a string of text, and whether we're in a
     /// user-agent stylesheet.
     ///
     /// If we're not in a user-agent stylesheet, we will never parse anonymous
     /// box pseudo-elements.
     ///
     /// Returns `None` if the pseudo-element is not recognised.
     #[inline]
@@ -202,17 +241,17 @@ impl PseudoElement {
         None
     }
 
     /// Constructs a tree pseudo-element from the given name and arguments.
     /// "name" must start with "-moz-tree-".
     ///
     /// Returns `None` if the pseudo-element is not recognized.
     #[inline]
-    pub fn tree_pseudo_element(name: &str, args: Box<[String]>) -> Option<Self> {
+    pub fn tree_pseudo_element(name: &str, args: Box<[Atom]>) -> Option<Self> {
         use std::ascii::AsciiExt;
         debug_assert!(name.starts_with("-moz-tree-"));
         let tree_part = &name[10..];
         % for pseudo in TREE_PSEUDOS:
             if tree_part.eq_ignore_ascii_case("${pseudo.value[11:]}") {
                 return Some(${pseudo_element_variant(pseudo, "args")});
             }
         % endfor
@@ -223,26 +262,25 @@ impl PseudoElement {
 impl ToCss for PseudoElement {
     fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
         dest.write_char(':')?;
         match *self {
             % for pseudo in PSEUDOS:
                 ${pseudo_element_variant(pseudo)} => dest.write_str("${pseudo.value}")?,
             % endfor
         }
-        match *self {
-            ${" |\n            ".join("PseudoElement::{}(ref args)".format(pseudo.capitalized())
-                                      for pseudo in TREE_PSEUDOS)} => {
+        if let Some(args) = self.tree_pseudo_args() {
+            if !args.is_empty() {
                 dest.write_char('(')?;
                 let mut iter = args.iter();
                 if let Some(first) = iter.next() {
-                    serialize_identifier(first, dest)?;
+                    serialize_identifier(&first.to_string(), dest)?;
                     for item in iter {
                         dest.write_str(", ")?;
-                        serialize_identifier(item, dest)?;
+                        serialize_identifier(&item.to_string(), dest)?;
                     }
                 }
-                dest.write_char(')')
+                dest.write_char(')')?;
             }
-            _ => Ok(()),
         }
+        Ok(())
     }
 }
diff --git a/servo/components/style/gecko/selector_parser.rs b/servo/components/style/gecko/selector_parser.rs
--- a/servo/components/style/gecko/selector_parser.rs
+++ b/servo/components/style/gecko/selector_parser.rs
@@ -12,17 +12,17 @@ use gecko_bindings::sugar::ownership::{H
 use selector_parser::{SelectorParser, PseudoElementCascadeType};
 use selectors::SelectorList;
 use selectors::parser::{Selector, SelectorMethods, SelectorParseErrorKind};
 use selectors::visitor::SelectorVisitor;
 use std::fmt;
 use string_cache::{Atom, Namespace, WeakAtom, WeakNamespace};
 use style_traits::{ParseError, StyleParseErrorKind};
 
-pub use gecko::pseudo_element::{PseudoElement, EAGER_PSEUDOS, EAGER_PSEUDO_COUNT, SIMPLE_PSEUDO_COUNT};
+pub use gecko::pseudo_element::{PseudoElement, EAGER_PSEUDOS, EAGER_PSEUDO_COUNT, PSEUDO_COUNT};
 pub use gecko::snapshot::SnapshotMap;
 
 bitflags! {
     // See NonTSPseudoClass::is_enabled_in()
     flags NonTSPseudoClassFlag: u8 {
         const PSEUDO_CLASS_ENABLED_IN_UA_SHEETS = 1 << 0,
         const PSEUDO_CLASS_ENABLED_IN_CHROME = 1 << 1,
         const PSEUDO_CLASS_ENABLED_IN_UA_SHEETS_AND_CHROME =
@@ -383,30 +383,37 @@ impl<'a, 'i> ::selectors::Parser<'i> for
         } else {
             Err(parser.new_custom_error(SelectorParseErrorKind::UnsupportedPseudoClassOrElement(name)))
         }
     }
 
     fn parse_pseudo_element(&self, location: SourceLocation, name: CowRcStr<'i>)
                             -> Result<PseudoElement, ParseError<'i>> {
         PseudoElement::from_slice(&name, self.in_user_agent_stylesheet())
+            .or_else(|| {
+                if name.starts_with("-moz-tree-") {
+                    PseudoElement::tree_pseudo_element(&name, Box::new([]))
+                } else {
+                    None
+                }
+            })
             .ok_or(location.new_custom_error(SelectorParseErrorKind::UnsupportedPseudoClassOrElement(name.clone())))
     }
 
     fn parse_functional_pseudo_element<'t>(&self, name: CowRcStr<'i>,
                                            parser: &mut Parser<'i, 't>)
                                            -> Result<PseudoElement, ParseError<'i>> {
         if name.starts_with("-moz-tree-") {
             // Tree pseudo-elements can have zero or more arguments,
             // separated by either comma or space.
             let mut args = Vec::new();
             loop {
                 let location = parser.current_source_location();
                 match parser.next() {
-                    Ok(&Token::Ident(ref ident)) => args.push(ident.as_ref().to_owned()),
+                    Ok(&Token::Ident(ref ident)) => args.push(Atom::from(ident.as_ref())),
                     Ok(&Token::Comma) => {},
                     Ok(t) => return Err(location.new_unexpected_token_error(t.clone())),
                     Err(BasicParseError { kind: BasicParseErrorKind::EndOfInput, .. }) => break,
                     _ => unreachable!("Parser::next() shouldn't return any other error"),
                 }
             }
             let args = args.into_boxed_slice();
             if let Some(pseudo) = PseudoElement::tree_pseudo_element(&name, args) {
diff --git a/servo/components/style/selector_parser.rs b/servo/components/style/selector_parser.rs
--- a/servo/components/style/selector_parser.rs
+++ b/servo/components/style/selector_parser.rs
@@ -97,26 +97,26 @@ pub enum PseudoElementCascadeType {
     /// an optimisation since they are private pseudo-elements (like
     /// `::-servo-details-content`).
     ///
     /// This pseudo-elements are resolved on the fly using *only* global rules
     /// (rules of the form `*|*`), and applying them to the parent style.
     Precomputed,
 }
 
-/// A per-functional-pseudo map, from a given pseudo to a `T`.
+/// A per-pseudo map, from a given pseudo to a `T`.
 #[derive(MallocSizeOf)]
 pub struct PerPseudoElementMap<T> {
-    entries: [Option<T>; SIMPLE_PSEUDO_COUNT],
+    entries: [Option<T>; PSEUDO_COUNT],
 }
 
 impl<T> Default for PerPseudoElementMap<T> {
     fn default() -> Self {
         Self {
-            entries: PseudoElement::simple_pseudo_none_array(),
+            entries: PseudoElement::pseudo_none_array(),
         }
     }
 }
 
 impl<T> Debug for PerPseudoElementMap<T>
 where
     T: Debug,
 {
@@ -132,56 +132,44 @@ where
         }
         f.write_str("]")
     }
 }
 
 impl<T> PerPseudoElementMap<T> {
     /// Get an entry in the map.
     pub fn get(&self, pseudo: &PseudoElement) -> Option<&T> {
-        let index = match pseudo.simple_index() {
-            Some(i) => i,
-            None => return None,
-        };
-        self.entries[index].as_ref()
+        self.entries[pseudo.index()].as_ref()
     }
 
     /// Clear this enumerated array.
     pub fn clear(&mut self) {
         *self = Self::default();
     }
 
     /// Set an entry value.
     ///
     /// Returns an error if the element is not a simple pseudo.
-    pub fn set(&mut self, pseudo: &PseudoElement, value: T) -> Result<(), ()> {
-        let index = match pseudo.simple_index() {
-            Some(i) => i,
-            None => return Err(()),
-        };
-        self.entries[index] = Some(value);
-        Ok(())
+    pub fn set(&mut self, pseudo: &PseudoElement, value: T) {
+        self.entries[pseudo.index()] = Some(value);
     }
 
     /// Get an entry for `pseudo`, or create it with calling `f`.
     pub fn get_or_insert_with<F>(
         &mut self,
         pseudo: &PseudoElement,
         f: F,
-    ) -> Result<&mut T, ()>
+    ) -> &mut T
     where
         F: FnOnce() -> T,
     {
-        let index = match pseudo.simple_index() {
-            Some(i) => i,
-            None => return Err(()),
-        };
+        let index = pseudo.index();
         if self.entries[index].is_none() {
             self.entries[index] = Some(f());
         }
-        Ok(self.entries[index].as_mut().unwrap())
+        self.entries[index].as_mut().unwrap()
     }
 
     /// Get an iterator for the entries.
     pub fn iter(&self) -> ::std::slice::Iter<Option<T>> {
         self.entries.iter()
     }
 }
diff --git a/servo/components/style/servo/selector_parser.rs b/servo/components/style/servo/selector_parser.rs
--- a/servo/components/style/servo/selector_parser.rs
+++ b/servo/components/style/servo/selector_parser.rs
@@ -57,19 +57,18 @@ pub enum PseudoElement {
     ServoAnonymousTable,
     ServoAnonymousTableRow,
     ServoAnonymousTableCell,
     ServoAnonymousBlock,
     ServoInlineBlockWrapper,
     ServoInlineAbsolute,
 }
 
-/// The count of simple (non-functional) pseudo-elements (that is, all
-/// pseudo-elements for now).
-pub const SIMPLE_PSEUDO_COUNT: usize = PseudoElement::ServoInlineAbsolute as usize + 1;
+/// The count of all pseudo-elements.
+pub const PSEUDO_COUNT: usize = PseudoElement::ServoInlineAbsolute as usize + 1;
 
 impl ::selectors::parser::PseudoElement for PseudoElement {
     type Impl = SelectorImpl;
 
     fn supports_pseudo_class(&self, _: &NonTSPseudoClass) -> bool {
         false
     }
 }
@@ -105,22 +104,22 @@ impl PseudoElement {
     #[inline]
     pub fn eager_index(&self) -> usize {
         debug_assert!(self.is_eager());
         self.clone() as usize
     }
 
     /// An index for this pseudo-element to be indexed in an enumerated array.
     #[inline]
-    pub fn simple_index(&self) -> Option<usize> {
-        Some(self.clone() as usize)
+    pub fn index(&self) -> usize {
+        self.clone() as usize
     }
 
-    /// An array of `None`, one per simple pseudo-element.
-    pub fn simple_pseudo_none_array<T>() -> [Option<T>; SIMPLE_PSEUDO_COUNT] {
+    /// An array of `None`, one per pseudo-element.
+    pub fn pseudo_none_array<T>() -> [Option<T>; PSEUDO_COUNT] {
         Default::default()
     }
 
     /// Creates a pseudo-element from an eager index.
     #[inline]
     pub fn from_eager_index(i: usize) -> Self {
         assert!(i < EAGER_PSEUDO_COUNT);
         let result: PseudoElement = unsafe { mem::transmute(i) };
diff --git a/servo/components/style/stylist.rs b/servo/components/style/stylist.rs
--- a/servo/components/style/stylist.rs
+++ b/servo/components/style/stylist.rs
@@ -819,23 +819,31 @@ impl Stylist {
     pub fn lazily_compute_pseudo_element_style<E>(
         &self,
         guards: &StylesheetGuards,
         element: &E,
         pseudo: &PseudoElement,
         rule_inclusion: RuleInclusion,
         parent_style: &ComputedValues,
         is_probe: bool,
-        font_metrics: &FontMetricsProvider
+        font_metrics: &FontMetricsProvider,
+        matching_fn: Option<&Fn(&PseudoElement) -> bool>,
     ) -> Option<Arc<ComputedValues>>
     where
         E: TElement,
     {
         let cascade_inputs =
-            self.lazy_pseudo_rules(guards, element, pseudo, is_probe, rule_inclusion);
+            self.lazy_pseudo_rules(
+                guards,
+                element,
+                pseudo,
+                is_probe,
+                rule_inclusion,
+                matching_fn
+            );
         self.compute_pseudo_element_style_with_inputs(
             &cascade_inputs,
             pseudo,
             guards,
             parent_style,
             font_metrics,
         )
     }
@@ -979,17 +987,18 @@ impl Stylist {
     /// See the documentation on lazy pseudo-elements in
     /// docs/components/style.md
     pub fn lazy_pseudo_rules<E>(
         &self,
         guards: &StylesheetGuards,
         element: &E,
         pseudo: &PseudoElement,
         is_probe: bool,
-        rule_inclusion: RuleInclusion
+        rule_inclusion: RuleInclusion,
+        matching_fn: Option<&Fn(&PseudoElement) -> bool>,
     ) -> CascadeInputs
     where
         E: TElement
     {
         let pseudo = pseudo.canonical();
         debug_assert!(pseudo.is_lazy());
 
         // Apply the selector flags. We should be in sequential mode
@@ -1026,16 +1035,17 @@ impl Stylist {
         let mut declarations = ApplicableDeclarationList::new();
         let mut matching_context =
             MatchingContext::new(
                 MatchingMode::ForStatelessPseudoElement,
                 None,
                 None,
                 self.quirks_mode,
             );
+        matching_context.pseudo_element_matching_fn = matching_fn;
 
         self.push_applicable_declarations(
             element,
             Some(&pseudo),
             None,
             None,
             AnimationRules(None, None),
             rule_inclusion,
@@ -1062,16 +1072,17 @@ impl Stylist {
             let mut matching_context =
                 MatchingContext::new_for_visited(
                     MatchingMode::ForStatelessPseudoElement,
                     None,
                     None,
                     VisitedHandlingMode::RelevantLinkVisited,
                     self.quirks_mode,
                 );
+            matching_context.pseudo_element_matching_fn = matching_fn;
 
             self.push_applicable_declarations(
                 element,
                 Some(&pseudo),
                 None,
                 None,
                 AnimationRules(None, None),
                 rule_inclusion,
@@ -1289,16 +1300,17 @@ impl Stylist {
                 // NOTE(emilio): This is needed because the XBL stylist may
                 // think it has a different quirks mode than the document.
                 let mut matching_context = MatchingContext::new(
                     context.matching_mode,
                     context.bloom_filter,
                     context.nth_index_cache.as_mut().map(|s| &mut **s),
                     stylist.quirks_mode,
                 );
+                matching_context.pseudo_element_matching_fn = context.pseudo_element_matching_fn;
 
                 map.get_all_matching_rules(
                     element,
                     &rule_hash_target,
                     applicable_declarations,
                     &mut matching_context,
                     stylist.quirks_mode,
                     flags_setter,
@@ -2004,43 +2016,36 @@ impl CascadeData {
                     let style_rule = locked.read_with(&guard);
                     self.num_declarations +=
                         style_rule.block.read_with(&guard).len();
                     for selector in &style_rule.selectors.0 {
                         self.num_selectors += 1;
 
                         let map = match selector.pseudo_element() {
                             Some(pseudo) if pseudo.is_precomputed() => {
-                                if !selector.is_universal() ||
-                                   !matches!(origin, Origin::UserAgent) {
-                                    // ::-moz-tree selectors may appear in
-                                    // non-UA sheets (even though they never
-                                    // match).
-                                    continue;
-                                }
+                                debug_assert!(selector.is_universal());
+                                debug_assert!(matches!(origin, Origin::UserAgent));
 
                                 precomputed_pseudo_element_decls
                                     .as_mut()
                                     .expect("Expected precomputed declarations for the UA level")
                                     .get_or_insert_with(&pseudo.canonical(), Vec::new)
-                                    .expect("Unexpected tree pseudo-element?")
                                     .push(ApplicableDeclarationBlock::new(
                                         StyleSource::Style(locked.clone()),
                                         self.rules_source_order,
                                         CascadeLevel::UANormal,
                                         selector.specificity()
                                     ));
 
                                 continue;
                             }
                             None => &mut self.element_map,
                             Some(pseudo) => {
                                 self.pseudos_map
                                     .get_or_insert_with(&pseudo.canonical(), || Box::new(SelectorMap::new()))
-                                    .expect("Unexpected tree pseudo-element?")
                             }
                         };
 
                         let hashes =
                             AncestorHashes::new(&selector, quirks_mode);
 
                         let rule = Rule::new(
                             selector.clone(),
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -80,16 +80,17 @@ use style::gecko_bindings::bindings::nsT
 use style::gecko_bindings::bindings::nsTimingFunctionBorrowedMut;
 use style::gecko_bindings::structs;
 use style::gecko_bindings::structs::{CSSPseudoElementType, CompositeOperation};
 use style::gecko_bindings::structs::{Loader, LoaderReusableStyleSheets};
 use style::gecko_bindings::structs::{RawServoStyleRule, ServoStyleContextStrong, RustString};
 use style::gecko_bindings::structs::{ServoStyleSheet, SheetParsingMode, nsIAtom, nsCSSPropertyID};
 use style::gecko_bindings::structs::{nsCSSFontFaceRule, nsCSSCounterStyleRule};
 use style::gecko_bindings::structs::{nsRestyleHint, nsChangeHint, PropertyValuePair};
+use style::gecko_bindings::structs::AtomArray;
 use style::gecko_bindings::structs::IterationCompositeOperation;
 use style::gecko_bindings::structs::MallocSizeOf as GeckoMallocSizeOf;
 use style::gecko_bindings::structs::OriginFlags;
 use style::gecko_bindings::structs::OriginFlags_Author;
 use style::gecko_bindings::structs::OriginFlags_User;
 use style::gecko_bindings::structs::OriginFlags_UserAgent;
 use style::gecko_bindings::structs::RawGeckoGfxMatrix4x4;
 use style::gecko_bindings::structs::RawGeckoPresContextOwned;
@@ -122,17 +123,17 @@ use style::properties::{SKIP_ROOT_AND_IT
 use style::properties::PROHIBIT_DISPLAY_CONTENTS;
 use style::properties::animated_properties::AnimationValue;
 use style::properties::animated_properties::compare_property_priority;
 use style::properties::parse_one_declaration_into;
 use style::rule_cache::RuleCacheConditions;
 use style::rule_tree::{CascadeLevel, StrongRuleNode, StyleSource};
 use style::selector_parser::{PseudoElementCascadeType, SelectorImpl};
 use style::shared_lock::{SharedRwLockReadGuard, StylesheetGuards, ToCssWithGuard, Locked};
-use style::string_cache::Atom;
+use style::string_cache::{Atom, WeakAtom};
 use style::style_adjuster::StyleAdjuster;
 use style::stylesheets::{CssRule, CssRules, CssRuleType, CssRulesHelpers, DocumentRule};
 use style::stylesheets::{FontFeatureValuesRule, ImportRule, KeyframesRule, MediaRule};
 use style::stylesheets::{NamespaceRule, Origin, OriginSet, PageRule, StyleRule};
 use style::stylesheets::{StylesheetContents, SupportsRule};
 use style::stylesheets::StylesheetLoader as StyleStylesheetLoader;
 use style::stylesheets::keyframes_rule::{Keyframe, KeyframeSelector, KeyframesStepValue};
 use style::stylesheets::supports_rule::parse_condition_or_declaration;
@@ -1981,27 +1982,88 @@ pub extern "C" fn Servo_ResolvePseudoSty
         &guard,
         element,
         &pseudo,
         RuleInclusion::All,
         &data.styles,
         inherited_style,
         &*doc_data,
         is_probe,
+        /* matching_func = */ None,
     );
 
     match style {
         Some(s) => s.into(),
         None => {
             debug_assert!(is_probe);
             Strong::null()
         }
     }
 }
 
+fn debug_atom_array(atoms: &AtomArray) -> String {
+    let mut result = String::from("[");
+    for atom in atoms.iter() {
+        if atom.mRawPtr.is_null() {
+            result += "(null), ";
+        } else {
+            let atom = unsafe { WeakAtom::new(atom.mRawPtr) };
+            write!(result, "{}, ", atom).unwrap();
+        }
+    }
+    result.push(']');
+    result
+}
+
+#[no_mangle]
+pub extern "C" fn Servo_ComputedValues_ResolveXULTreePseudoStyle(
+    element: RawGeckoElementBorrowed,
+    pseudo_tag: *mut nsIAtom,
+    inherited_style: ServoStyleContextBorrowed,
+    input_word: *const AtomArray,
+    raw_data: RawServoStyleSetBorrowed
+) -> ServoStyleContextStrong {
+    let element = GeckoElement(element);
+    let data = element.borrow_data()
+        .expect("Calling ResolveXULTreePseudoStyle on unstyled element?");
+
+    let pseudo = unsafe {
+        Atom::with(pseudo_tag, |atom| {
+            PseudoElement::from_tree_pseudo_atom(atom, Box::new([]))
+        }).expect("ResolveXULTreePseudoStyle with a non-tree pseudo?")
+    };
+    let input_word = unsafe { input_word.as_ref().unwrap() };
+
+    let doc_data = PerDocumentStyleData::from_ffi(raw_data).borrow();
+
+    debug!("ResolveXULTreePseudoStyle: {:?} {:?} {}",
+           element, pseudo, debug_atom_array(input_word));
+
+    let matching_fn = |pseudo: &PseudoElement| {
+        let args = pseudo.tree_pseudo_args().expect("Not a tree pseudo-element?");
+        args.iter().all(|atom| {
+            input_word.iter().any(|item| atom.as_ptr() == item.mRawPtr)
+        })
+    };
+
+    let global_style_data = &*GLOBAL_STYLE_DATA;
+    let guard = global_style_data.shared_lock.read();
+    get_pseudo_style(
+        &guard,
+        element,
+        &pseudo,
+        RuleInclusion::All,
+        &data.styles,
+        Some(inherited_style),
+        &*doc_data,
+        /* is_probe = */ false,
+        Some(&matching_fn),
+    ).unwrap().into()
+}
+
 #[no_mangle]
 pub extern "C" fn Servo_SetExplicitStyle(element: RawGeckoElementBorrowed,
                                          style: ServoStyleContextBorrowed)
 {
     let element = GeckoElement(element);
     debug!("Servo_SetExplicitStyle: {:?}", element);
     // We only support this API for initial styling. There's no reason it couldn't
     // work for other things, we just haven't had a reason to do so.
@@ -2035,16 +2097,17 @@ fn get_pseudo_style(
     guard: &SharedRwLockReadGuard,
     element: GeckoElement,
     pseudo: &PseudoElement,
     rule_inclusion: RuleInclusion,
     styles: &ElementStyles,
     inherited_styles: Option<&ComputedValues>,
     doc_data: &PerDocumentStyleDataImpl,
     is_probe: bool,
+    matching_func: Option<&Fn(&PseudoElement) -> bool>,
 ) -> Option<Arc<ComputedValues>> {
     let style = match pseudo.cascade_type() {
         PseudoElementCascadeType::Eager => {
             match *pseudo {
                 PseudoElement::FirstLetter => {
                     styles.pseudos.get(&pseudo).and_then(|pseudo_styles| {
                         // inherited_styles can be None when doing lazy resolution
                         // (e.g. for computed style) or when probing.  In that case
@@ -2108,16 +2171,17 @@ fn get_pseudo_style(
                 .lazily_compute_pseudo_element_style(
                     &guards,
                     &element,
                     &pseudo,
                     rule_inclusion,
                     base,
                     is_probe,
                     &metrics,
+                    matching_func,
                 )
         },
     };
 
     if is_probe {
         return style;
     }
 
@@ -3339,16 +3403,17 @@ pub extern "C" fn Servo_ResolveStyleLazi
                     &guard,
                     element,
                     pseudo,
                     rule_inclusion,
                     styles,
                     /* inherited_styles = */ None,
                     &*data,
                     is_probe,
+                    /* matching_func = */ None,
                 )
             }
             None => Some(styles.primary().clone()),
         }
     };
 
     let is_before_or_after = pseudo.as_ref().map_or(false, |p| p.is_before_or_after());
 
