# HG changeset patch
# User Scott Wu <scottcwwu@gmail.com>
# Date 1505299839 -28800
# Node ID 7f7d9a165b2fd988d4c8153352e0301dc9f364c5
# Parent  e3141484615b2c6ed67ff2648610bb7281b09db4
Bug 1399382 - Add a pref to hide credit card autofill feature, r=lchang

MozReview-Commit-ID: 2GIOrLBmFKR

diff --git a/browser/app/profile/firefox.js b/browser/app/profile/firefox.js
--- a/browser/app/profile/firefox.js
+++ b/browser/app/profile/firefox.js
@@ -1580,16 +1580,21 @@ pref("browser.crashReports.unsubmittedCh
 #ifdef NIGHTLY_BUILD
 pref("extensions.formautofill.available", "on");
 #elif MOZ_UPDATE_CHANNEL == release
 pref("extensions.formautofill.available", "staged-rollout");
 #else
 pref("extensions.formautofill.available", "detect");
 #endif
 pref("extensions.formautofill.addresses.enabled", true);
+#ifdef NIGHTLY_BUILD
+pref("extensions.formautofill.creditCards.available", true);
+#else
+pref("extensions.formautofill.creditCards.available", false);
+#endif
 pref("extensions.formautofill.creditCards.enabled", true);
 pref("extensions.formautofill.firstTimeUse", true);
 pref("extensions.formautofill.heuristics.enabled", true);
 pref("extensions.formautofill.loglevel", "Warn");
 
 // Whether or not to restore a session with lazy-browser tabs.
 pref("browser.sessionstore.restore_tabs_lazily", true);
 
diff --git a/browser/extensions/formautofill/FormAutofillParent.jsm b/browser/extensions/formautofill/FormAutofillParent.jsm
--- a/browser/extensions/formautofill/FormAutofillParent.jsm
+++ b/browser/extensions/formautofill/FormAutofillParent.jsm
@@ -81,27 +81,31 @@ FormAutofillParent.prototype = {
   /**
    * Initializes ProfileStorage and registers the message handler.
    */
   async init() {
     Services.obs.addObserver(this, "advanced-pane-loaded");
     Services.ppmm.addMessageListener("FormAutofill:InitStorage", this);
     Services.ppmm.addMessageListener("FormAutofill:GetRecords", this);
     Services.ppmm.addMessageListener("FormAutofill:SaveAddress", this);
-    Services.ppmm.addMessageListener("FormAutofill:SaveCreditCard", this);
     Services.ppmm.addMessageListener("FormAutofill:RemoveAddresses", this);
-    Services.ppmm.addMessageListener("FormAutofill:RemoveCreditCards", this);
     Services.ppmm.addMessageListener("FormAutofill:OpenPreferences", this);
-    Services.ppmm.addMessageListener("FormAutofill:GetDecryptedString", this);
     Services.mm.addMessageListener("FormAutofill:OnFormSubmit", this);
 
     // Observing the pref and storage changes
     Services.prefs.addObserver(ENABLED_AUTOFILL_ADDRESSES_PREF, this);
-    Services.prefs.addObserver(ENABLED_AUTOFILL_CREDITCARDS_PREF, this);
     Services.obs.addObserver(this, "formautofill-storage-changed");
+
+    // Only listen to credit card related messages if it is available
+    if (FormAutofillUtils.isAutofillCreditCardsAvailable) {
+      Services.ppmm.addMessageListener("FormAutofill:SaveCreditCard", this);
+      Services.ppmm.addMessageListener("FormAutofill:RemoveCreditCards", this);
+      Services.ppmm.addMessageListener("FormAutofill:GetDecryptedString", this);
+      Services.prefs.addObserver(ENABLED_AUTOFILL_CREDITCARDS_PREF, this);
+    }
   },
 
   observe(subject, topic, data) {
     log.debug("observe:", topic, "with data:", data);
     switch (topic) {
       case "advanced-pane-loaded": {
         let useOldOrganization = Services.prefs.getBoolPref("browser.preferences.useOldOrganization",
                                                             false);
@@ -247,22 +251,26 @@ FormAutofillParent.prototype = {
    * @private
    */
   _uninit() {
     this.profileStorage._saveImmediately();
 
     Services.ppmm.removeMessageListener("FormAutofill:InitStorage", this);
     Services.ppmm.removeMessageListener("FormAutofill:GetRecords", this);
     Services.ppmm.removeMessageListener("FormAutofill:SaveAddress", this);
-    Services.ppmm.removeMessageListener("FormAutofill:SaveCreditCard", this);
     Services.ppmm.removeMessageListener("FormAutofill:RemoveAddresses", this);
-    Services.ppmm.removeMessageListener("FormAutofill:RemoveCreditCards", this);
     Services.obs.removeObserver(this, "advanced-pane-loaded");
     Services.prefs.removeObserver(ENABLED_AUTOFILL_ADDRESSES_PREF, this);
-    Services.prefs.removeObserver(ENABLED_AUTOFILL_CREDITCARDS_PREF, this);
+
+    if (FormAutofillUtils.isAutofillCreditCardsAvailable) {
+      Services.ppmm.removeMessageListener("FormAutofill:SaveCreditCard", this);
+      Services.ppmm.removeMessageListener("FormAutofill:RemoveCreditCards", this);
+      Services.ppmm.removeMessageListener("FormAutofill:GetDecryptedString", this);
+      Services.prefs.removeObserver(ENABLED_AUTOFILL_CREDITCARDS_PREF, this);
+    }
   },
 
   /**
    * Get the records from profile store and return results back to content
    * process. It will decrypt the credit card number and append
    * "cc-number-decrypted" to each record if MasterPassword isn't set.
    *
    * @private
diff --git a/browser/extensions/formautofill/FormAutofillPreferences.jsm b/browser/extensions/formautofill/FormAutofillPreferences.jsm
--- a/browser/extensions/formautofill/FormAutofillPreferences.jsm
+++ b/browser/extensions/formautofill/FormAutofillPreferences.jsm
@@ -56,93 +56,108 @@ FormAutofillPreferences.prototype = {
   },
 
   /**
    * Create Form Autofill preference group
    *
    * @param  {XULDocument} document
    */
   createPreferenceGroup(document) {
+    let learnMoreURL = Services.urlFormatter.formatURLPref("app.support.baseURL") + "autofill-card-address";
     let formAutofillGroup;
     let addressAutofill = document.createElementNS(XUL_NS, "hbox");
     let addressAutofillCheckboxGroup = document.createElementNS(XUL_NS, "description");
     let addressAutofillCheckbox = document.createElementNS(XUL_NS, "checkbox");
     let addressAutofillLearnMore = document.createElementNS(XUL_NS, "label");
     let savedAddressesBtn = document.createElementNS(XUL_NS, "button");
-    let creditCardAutofill = document.createElementNS(XUL_NS, "hbox");
-    let creditCardAutofillCheckboxGroup = document.createElementNS(XUL_NS, "description");
-    let creditCardAutofillCheckbox = document.createElementNS(XUL_NS, "checkbox");
-    let creditCardAutofillLearnMore = document.createElementNS(XUL_NS, "label");
-    let savedCreditCardsBtn = document.createElementNS(XUL_NS, "button");
 
     if (this.useOldOrganization) {
       let caption = document.createElementNS(XUL_NS, "caption");
       let captionLabel = document.createElementNS(XUL_NS, "label");
 
       formAutofillGroup = document.createElementNS(XUL_NS, "groupbox");
       formAutofillGroup.hidden = document.location.href != "about:preferences#privacy";
       // Use .setAttribute because HTMLElement.dataset is not available on XUL elements
       formAutofillGroup.setAttribute("data-category", "panePrivacy");
       formAutofillGroup.appendChild(caption);
       caption.appendChild(captionLabel);
       captionLabel.textContent = this.bundle.GetStringFromName("preferenceGroupTitle");
     } else {
       formAutofillGroup = document.createElementNS(XUL_NS, "vbox");
       savedAddressesBtn.className = "accessory-button";
-      savedCreditCardsBtn.className = "accessory-button";
     }
 
-    this.refs = {
-      formAutofillGroup,
-      addressAutofillCheckbox,
-      savedAddressesBtn,
-      creditCardAutofillCheckbox,
-      savedCreditCardsBtn,
-    };
-
     formAutofillGroup.id = "formAutofillGroup";
     addressAutofill.id = "addressAutofill";
     addressAutofillLearnMore.id = "addressAutofillLearnMore";
     addressAutofillLearnMore.className = "learnMore text-link";
-    creditCardAutofill.id = "creditCardAutofill";
-    creditCardAutofillLearnMore.id = "creditCardAutofillLearnMore";
-    creditCardAutofillLearnMore.className = "learnMore text-link";
 
     addressAutofillLearnMore.setAttribute("value", this.bundle.GetStringFromName("learnMore"));
     addressAutofillCheckbox.setAttribute("label", this.bundle.GetStringFromName("enableAddressAutofill"));
     savedAddressesBtn.setAttribute("label", this.bundle.GetStringFromName("savedAddresses"));
-    creditCardAutofillLearnMore.setAttribute("value", this.bundle.GetStringFromName("learnMore"));
-    creditCardAutofillCheckbox.setAttribute("label", this.bundle.GetStringFromName("enableCreditCardAutofill"));
-    savedCreditCardsBtn.setAttribute("label", this.bundle.GetStringFromName("savedCreditCards"));
 
-    let learnMoreURL = Services.urlFormatter.formatURLPref("app.support.baseURL") + "autofill-card-address";
     addressAutofillLearnMore.setAttribute("href", learnMoreURL);
-    creditCardAutofillLearnMore.setAttribute("href", learnMoreURL);
 
     // Manually set the checked state
     if (FormAutofillUtils.isAutofillAddressesEnabled) {
       addressAutofillCheckbox.setAttribute("checked", true);
     }
-    if (FormAutofillUtils.isAutofillCreditCardsEnabled) {
-      creditCardAutofillCheckbox.setAttribute("checked", true);
-    }
 
     addressAutofillCheckboxGroup.flex = 1;
     creditCardAutofillCheckboxGroup.flex = 1;
 
     formAutofillGroup.appendChild(addressAutofill);
     addressAutofill.appendChild(addressAutofillCheckboxGroup);
     addressAutofillCheckboxGroup.appendChild(addressAutofillCheckbox);
     addressAutofillCheckboxGroup.appendChild(addressAutofillLearnMore);
     addressAutofill.appendChild(savedAddressesBtn);
-    formAutofillGroup.appendChild(creditCardAutofill);
-    creditCardAutofill.appendChild(creditCardAutofillCheckboxGroup);
-    creditCardAutofillCheckboxGroup.appendChild(creditCardAutofillCheckbox);
-    creditCardAutofillCheckboxGroup.appendChild(creditCardAutofillLearnMore);
-    creditCardAutofill.appendChild(savedCreditCardsBtn);
+
+    this.refs = {
+      formAutofillGroup,
+      addressAutofillCheckbox,
+      savedAddressesBtn,
+    };
+
+    if (FormAutofillUtils.isAutofillCreditCardsAvailable) {
+      let creditCardAutofill = document.createElementNS(XUL_NS, "hbox");
+      let creditCardAutofillCheckboxGroup = document.createElementNS(XUL_NS, "description");
+      let creditCardAutofillCheckbox = document.createElementNS(XUL_NS, "checkbox");
+      let creditCardAutofillLearnMore = document.createElementNS(XUL_NS, "label");
+      let savedCreditCardsBtn = document.createElementNS(XUL_NS, "button");
+
+      if (!this.useOldOrganization) {
+        savedCreditCardsBtn.className = "accessory-button";
+      }
+      creditCardAutofillLearnMore.className = "learnMore text-link";
+
+      creditCardAutofill.id = "creditCardAutofill";
+      creditCardAutofillLearnMore.id = "creditCardAutofillLearnMore";
+
+      creditCardAutofillLearnMore.setAttribute("value", this.bundle.GetStringFromName("learnMore"));
+      creditCardAutofillCheckbox.setAttribute("label", this.bundle.GetStringFromName("enableCreditCardAutofill"));
+      savedCreditCardsBtn.setAttribute("label", this.bundle.GetStringFromName("savedCreditCards"));
+
+      creditCardAutofillLearnMore.setAttribute("href", learnMoreURL);
+
+      // Manually set the checked state
+      if (FormAutofillUtils.isAutofillCreditCardsEnabled) {
+        creditCardAutofillCheckbox.setAttribute("checked", true);
+      }
+
+      creditCardAutofillCheckboxGroup.flex = 1;
+
+      formAutofillGroup.appendChild(creditCardAutofill);
+      creditCardAutofill.appendChild(creditCardAutofillCheckboxGroup);
+      creditCardAutofillCheckboxGroup.appendChild(creditCardAutofillCheckbox);
+      creditCardAutofillCheckboxGroup.appendChild(creditCardAutofillLearnMore);
+      creditCardAutofill.appendChild(savedCreditCardsBtn);
+
+      this.refs.creditCardAutofillCheckbox = creditCardAutofillCheckbox;
+      this.refs.savedCreditCardsBtn = savedCreditCardsBtn;
+    }
   },
 
   /**
    * Handle events
    *
    * @param  {DOMEvent} event
    */
   handleEvent(event) {
diff --git a/browser/extensions/formautofill/FormAutofillUtils.jsm b/browser/extensions/formautofill/FormAutofillUtils.jsm
--- a/browser/extensions/formautofill/FormAutofillUtils.jsm
+++ b/browser/extensions/formautofill/FormAutofillUtils.jsm
@@ -12,24 +12,26 @@ const ADDRESS_REFERENCES = "chrome://for
 //       bug 1370193.
 const ALTERNATIVE_COUNTRY_NAMES = {
   "US": ["US", "United States of America", "United States", "America", "U.S.", "USA", "U.S.A.", "U.S.A"],
 };
 
 const ADDRESSES_COLLECTION_NAME = "addresses";
 const CREDITCARDS_COLLECTION_NAME = "creditCards";
 const ENABLED_AUTOFILL_ADDRESSES_PREF = "extensions.formautofill.addresses.enabled";
+const AUTOFILL_CREDITCARDS_AVAILABLE_PREF = "extensions.formautofill.creditCards.available";
 const ENABLED_AUTOFILL_CREDITCARDS_PREF = "extensions.formautofill.creditCards.enabled";
 
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 
 this.FormAutofillUtils = {
   get AUTOFILL_FIELDS_THRESHOLD() { return 3; },
   get isAutofillEnabled() { return this.isAutofillAddressesEnabled || this.isAutofillCreditCardsEnabled; },
+  get isAutofillCreditCardsEnabled() { return this.isAutofillCreditCardsAvailable && this._isAutofillCreditCardsEnabled; },
 
   ADDRESSES_COLLECTION_NAME,
   CREDITCARDS_COLLECTION_NAME,
   ENABLED_AUTOFILL_ADDRESSES_PREF,
   ENABLED_AUTOFILL_CREDITCARDS_PREF,
 
   _fieldNameInfo: {
     "name": "name",
@@ -517,9 +519,11 @@ this.FormAutofillUtils.defineLazyLogGett
 
 XPCOMUtils.defineLazyGetter(FormAutofillUtils, "stringBundle", function() {
   return Services.strings.createBundle("chrome://formautofill/locale/formautofill.properties");
 });
 
 XPCOMUtils.defineLazyPreferenceGetter(this.FormAutofillUtils,
                                       "isAutofillAddressesEnabled", ENABLED_AUTOFILL_ADDRESSES_PREF);
 XPCOMUtils.defineLazyPreferenceGetter(this.FormAutofillUtils,
-                                      "isAutofillCreditCardsEnabled", ENABLED_AUTOFILL_CREDITCARDS_PREF);
+                                      "isAutofillCreditCardsAvailable", AUTOFILL_CREDITCARDS_AVAILABLE_PREF);
+XPCOMUtils.defineLazyPreferenceGetter(this.FormAutofillUtils,
+                                      "_isAutofillCreditCardsEnabled", ENABLED_AUTOFILL_CREDITCARDS_PREF);
diff --git a/browser/extensions/formautofill/test/browser/browser_privacyPreferences.js b/browser/extensions/formautofill/test/browser/browser_privacyPreferences.js
--- a/browser/extensions/formautofill/test/browser/browser_privacyPreferences.js
+++ b/browser/extensions/formautofill/test/browser/browser_privacyPreferences.js
@@ -71,8 +71,22 @@ add_task(async function test_autofillDis
         "Form Autofill group should be visible");
       is(content.document.querySelector(selectors.addressAutofillCheckbox).checked, false,
         "Checkbox should be unchecked when Autofill Addresses is disabled");
       is(content.document.querySelector(selectors.creditCardAutofillCheckbox).checked, false,
         "Checkbox should be unchecked when Autofill Credit Cards is disabled");
     });
   });
 });
+
+add_task(async function test_creditCardNotAvailable() {
+  SpecialPowers.pushPrefEnv({set: [[AUTOFILL_CREDITCARDS_AVAILABLE_PREF, false]]});
+  let finalPrefPaneLoaded = TestUtils.topicObserved("sync-pane-loaded", () => true);
+  await BrowserTestUtils.withNewTab({gBrowser, url: PAGE_PRIVACY}, async function(browser) {
+    await finalPrefPaneLoaded;
+    await ContentTask.spawn(browser, SELECTORS, (selectors) => {
+      is(content.document.querySelector(selectors.group).hidden, false,
+        "Form Autofill group should be visible");
+      ok(!content.document.querySelector(selectors.creditCardAutofillCheckbox),
+        "Autofill credit cards checkbox should not exist");
+    });
+  });
+});
diff --git a/browser/extensions/formautofill/test/browser/head.js b/browser/extensions/formautofill/test/browser/head.js
--- a/browser/extensions/formautofill/test/browser/head.js
+++ b/browser/extensions/formautofill/test/browser/head.js
@@ -1,28 +1,30 @@
 /* exported MANAGE_ADDRESSES_DIALOG_URL, MANAGE_CREDIT_CARDS_DIALOG_URL, EDIT_ADDRESS_DIALOG_URL, EDIT_CREDIT_CARD_DIALOG_URL,
             BASE_URL, TEST_ADDRESS_1, TEST_ADDRESS_2, TEST_ADDRESS_3, TEST_ADDRESS_4, TEST_ADDRESS_5,
             TEST_CREDIT_CARD_1, TEST_CREDIT_CARD_2, TEST_CREDIT_CARD_3, FORM_URL, CREDITCARD_FORM_URL,
-            FTU_PREF, ENABLED_AUTOFILL_ADDRESSES_PREF, ENABLED_AUTOFILL_CREDITCARDS_PREF, SYNC_USERNAME_PREF, SYNC_ADDRESSES_PREF,
+            FTU_PREF, ENABLED_AUTOFILL_ADDRESSES_PREF, AUTOFILL_CREDITCARDS_AVAILABLE_PREF, ENABLED_AUTOFILL_CREDITCARDS_PREF,
+            SYNC_USERNAME_PREF, SYNC_ADDRESSES_PREF,
             sleep, expectPopupOpen, openPopupOn, expectPopupClose, closePopup, clickDoorhangerButton,
             getAddresses, saveAddress, removeAddresses, saveCreditCard,
             getDisplayedPopupItems, getDoorhangerCheckbox, waitForMasterPasswordDialog */
 
 "use strict";
 
 const MANAGE_ADDRESSES_DIALOG_URL = "chrome://formautofill/content/manageAddresses.xhtml";
 const MANAGE_CREDIT_CARDS_DIALOG_URL = "chrome://formautofill/content/manageCreditCards.xhtml";
 const EDIT_ADDRESS_DIALOG_URL = "chrome://formautofill/content/editAddress.xhtml";
 const EDIT_CREDIT_CARD_DIALOG_URL = "chrome://formautofill/content/editCreditCard.xhtml";
 const BASE_URL = "http://mochi.test:8888/browser/browser/extensions/formautofill/test/browser/";
 const FORM_URL = "http://mochi.test:8888/browser/browser/extensions/formautofill/test/browser/autocomplete_basic.html";
 const CREDITCARD_FORM_URL =
   "http://mochi.test:8888/browser/browser/extensions/formautofill/test/browser/autocomplete_creditcard_basic.html";
 const FTU_PREF = "extensions.formautofill.firstTimeUse";
 const ENABLED_AUTOFILL_ADDRESSES_PREF = "extensions.formautofill.addresses.enabled";
+const AUTOFILL_CREDITCARDS_AVAILABLE_PREF = "extensions.formautofill.creditCards.available";
 const ENABLED_AUTOFILL_CREDITCARDS_PREF = "extensions.formautofill.creditCards.enabled";
 const SYNC_USERNAME_PREF = "services.sync.username";
 const SYNC_ADDRESSES_PREF = "services.sync.engine.addresses";
 
 const TEST_ADDRESS_1 = {
   "given-name": "John",
   "additional-name": "R.",
   "family-name": "Smith",
