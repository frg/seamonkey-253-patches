# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1510003266 28800
#      Mon Nov 06 13:21:06 2017 -0800
# Node ID 14062282d28b457fa6eb08b4c5e84eb77b2a46ec
# Parent  eff3594e8666620320948d2df37fb7b41a2c6d3a
Bug 1424394 - Change ErrorReporter::reportErrorNoOffset to take a va_list of parameters, not direct varargs.  r=Yoric

diff --git a/js/src/frontend/ErrorReporter.h b/js/src/frontend/ErrorReporter.h
--- a/js/src/frontend/ErrorReporter.h
+++ b/js/src/frontend/ErrorReporter.h
@@ -1,21 +1,42 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
+ * vim: set ts=8 sts=4 et sw=4 tw=99:
+ * This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
 #ifndef frontend_ErrorReporter_h
 #define frontend_ErrorReporter_h
 
+#include <stdarg.h> // for va_list
+#include <stddef.h> // for size_t
+#include <stdint.h> // for uint32_t
+
+#include "jsapi.h" // for JS::ReadOnlyCompileOptions
+
 namespace js {
 namespace frontend {
 
 class ErrorReporter
 {
   public:
-    virtual const ReadOnlyCompileOptions& options() const = 0;
+    virtual const JS::ReadOnlyCompileOptions& options() const = 0;
     virtual void lineNumAndColumnIndex(size_t offset, uint32_t* line, uint32_t* column) const = 0;
     virtual size_t offset() const = 0;
     virtual bool hasTokenizationStarted() const = 0;
-    virtual void reportErrorNoOffset(unsigned errorNumber, ...) = 0;
+    virtual void reportErrorNoOffsetVA(unsigned errorNumber, va_list args) = 0;
     virtual const char* getFilename() const = 0;
+
+    void reportErrorNoOffset(unsigned errorNumber, ...) {
+        va_list args;
+        va_start(args, errorNumber);
+
+        reportErrorNoOffsetVA(errorNumber, args);
+
+        va_end(args);
+    }
 };
 
 } // namespace frontend
 } // namespace js
 
 #endif // frontend_ErrorReporter_h
diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -853,27 +853,22 @@ TokenStream::reportError(unsigned errorN
     ErrorMetadata metadata;
     if (computeErrorMetadata(&metadata, currentToken().pos.begin))
         ReportCompileError(cx, Move(metadata), nullptr, JSREPORT_ERROR, errorNumber, args);
 
     va_end(args);
 }
 
 void
-TokenStreamAnyChars::reportErrorNoOffset(unsigned errorNumber, ...)
+TokenStreamAnyChars::reportErrorNoOffsetVA(unsigned errorNumber, va_list args)
 {
-    va_list args;
-    va_start(args, errorNumber);
-
     ErrorMetadata metadata;
     computeErrorMetadataNoOffset(&metadata);
 
     ReportCompileError(cx, Move(metadata), nullptr, JSREPORT_ERROR, errorNumber, args);
-
-    va_end(args);
 }
 
 bool
 TokenStream::warning(unsigned errorNumber, ...)
 {
     va_list args;
     va_start(args, errorNumber);
 
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -580,17 +580,17 @@ class TokenStreamAnyChars: public ErrorR
                                      unsigned flags, unsigned errorNumber, va_list args);
 
     // Compute error metadata for an error at no offset.
     void computeErrorMetadataNoOffset(ErrorMetadata* err);
 
     virtual const char* getFilename() const override { return filename; }
     virtual bool hasTokenizationStarted() const override;
     virtual void lineNumAndColumnIndex(size_t offset, uint32_t* line, uint32_t* column) const override;
-    virtual void reportErrorNoOffset(unsigned errorNumber, ...) override;
+    virtual void reportErrorNoOffsetVA(unsigned errorNumber, va_list args) override;
     virtual size_t offset() const override;
 
   protected:
     // Options used for parsing/tokenizing.
     const ReadOnlyCompileOptions& options_;
 
     Token               tokens[ntokens];    // circular token buffer
     unsigned            cursor;             // index of last parsed token
