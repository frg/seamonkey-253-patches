# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1510086976 18000
# Node ID 2a5a80d284b5ef958298cf515867ffbe420478c0
# Parent  0326d3a6fcaded52acd3c4525bfb1e51d7689563
Bug 1414292.  Update to HTML spec changes for cross-origin object property enumerability.  r=peterv

Updates to https://github.com/whatwg/html/pull/3186

Includes the changes from https://github.com/w3c/web-platform-tests/pull/8045

MozReview-Commit-ID: 5vEo1QGPufE

diff --git a/js/xpconnect/wrappers/FilteringWrapper.cpp b/js/xpconnect/wrappers/FilteringWrapper.cpp
--- a/js/xpconnect/wrappers/FilteringWrapper.cpp
+++ b/js/xpconnect/wrappers/FilteringWrapper.cpp
@@ -218,19 +218,22 @@ CrossOriginXrayWrapper::getPropertyDescr
         // from the ones we add ourselves here.
         MOZ_ASSERT(!JSID_IS_SYMBOL(id),
                    "What's this symbol-named property that appeared on a "
                    "Window or Location instance?");
 
         // All properties on cross-origin DOM objects are |own|.
         desc.object().set(wrapper);
 
-        // All properties on cross-origin DOM objects are non-enumerable and
-        // "configurable". Any value attributes are read-only.
-        desc.attributesRef() &= ~JSPROP_ENUMERATE;
+        // All properties on cross-origin DOM objects are "configurable". Any
+        // value attributes are read-only.  Indexed properties are enumerable,
+        // but nothing else is.
+        if (!JSID_IS_INT(id)) {
+            desc.attributesRef() &= ~JSPROP_ENUMERATE;
+        }
         desc.attributesRef() &= ~JSPROP_PERMANENT;
         if (!desc.getter() && !desc.setter())
             desc.attributesRef() |= JSPROP_READONLY;
     } else if (IsCrossOriginWhitelistedSymbol(cx, id)) {
         // Spec says to return PropertyDescriptor {
         //   [[Value]]: undefined, [[Writable]]: false, [[Enumerable]]: false,
         //   [[Configurable]]: true
         // }.
diff --git a/testing/web-platform/meta/html/browsers/origin/cross-origin-objects/cross-origin-objects.html.ini b/testing/web-platform/meta/html/browsers/origin/cross-origin-objects/cross-origin-objects.html.ini
deleted file mode 100644
--- a/testing/web-platform/meta/html/browsers/origin/cross-origin-objects/cross-origin-objects.html.ini
+++ /dev/null
@@ -1,10 +0,0 @@
-[cross-origin-objects.html]
-  type: testharness
-  [[[GetOwnProperty\]\] - Property descriptors for cross-origin properties should be set up correctly]
-    expected: FAIL
-
-  [Can only enumerate safelisted properties]
-    expected: FAIL
-
-  [[[OwnPropertyKeys\]\] should return all properties from cross-origin objects]
-    expected: FAIL
diff --git a/testing/web-platform/tests/html/browsers/origin/cross-origin-objects/cross-origin-objects.html b/testing/web-platform/tests/html/browsers/origin/cross-origin-objects/cross-origin-objects.html
--- a/testing/web-platform/tests/html/browsers/origin/cross-origin-objects/cross-origin-objects.html
+++ b/testing/web-platform/tests/html/browsers/origin/cross-origin-objects/cross-origin-objects.html
@@ -179,27 +179,30 @@ addTest(function() {
 addTest(function() {
   assert_true(isObject(Object.getOwnPropertyDescriptor(C, 'close')), "C.close is |own|");
   assert_true(isObject(Object.getOwnPropertyDescriptor(C, 'top')), "C.top is |own|");
   assert_true(isObject(Object.getOwnPropertyDescriptor(C.location, 'href')), "C.location.href is |own|");
   assert_true(isObject(Object.getOwnPropertyDescriptor(C.location, 'replace')), "C.location.replace is |own|");
 }, "[[GetOwnProperty]] - Properties on cross-origin objects should be reported |own|");
 
 function checkPropertyDescriptor(desc, propName, expectWritable) {
-  var isSymbol = (typeof(propName) == "symbol");
+  const isSymbol = typeof(propName) === "symbol";
+  const isArrayIndexPropertyName = !isSymbol && !isNaN(parseInt(propName, 10));
   propName = String(propName);
   assert_true(isObject(desc), "property descriptor for " + propName + " should exist");
   assert_equals(desc.configurable, true, "property descriptor for " + propName + " should be configurable");
-  if (isSymbol) {
-    assert_equals(desc.enumerable, false, "symbol-property descriptor for " + propName + " should not be enumerable");
-    assert_true("value" in desc,
-                "property descriptor for " + propName + " should be a value descriptor");
-    assert_equals(desc.value, undefined,
+  if (!isArrayIndexPropertyName) {
+    assert_equals(desc.enumerable, false, "property descriptor for " + propName + " should not be enumerable");
+    if(isSymbol) {
+      assert_true("value" in desc,
+                  "property descriptor for " + propName + " should be a value descriptor");
+      assert_equals(desc.value, undefined,
                   "symbol-named cross-origin visible prop " + propName +
                   " should come back as undefined");
+    }
   } else {
     assert_equals(desc.enumerable, true, "property descriptor for " + propName + " should be enumerable");
   }
   if ('value' in desc)
     assert_equals(desc.writable, expectWritable, "property descriptor for " + propName + " should have writable: " + expectWritable);
   else
     assert_equals(typeof desc.set != 'undefined', expectWritable,
                   "property descriptor for " + propName + " should " + (expectWritable ? "" : "not ") + "have setter");
@@ -260,44 +263,42 @@ addTest(function() {
 /*
  * EnumerateObjectProperties (backed by [[OwnPropertyKeys]])
  */
 
 addTest(function() {
   let i = 0;
   for (var prop in C) {
     i++;
-    assert_true(whitelistedWindowPropNames.includes(prop), prop + " is not safelisted for a cross-origin Window");
+    assert_true(whitelistedWindowIndices.includes(prop), prop + " is not safelisted for a cross-origin Window");
   }
-  assert_equals(i, whitelistedWindowPropNames.length, "Enumerate all safelisted cross-origin Window properties");
+  assert_equals(i, whitelistedWindowIndices.length, "Enumerate all enumerable safelisted cross-origin Window properties");
   i = 0;
   for (var prop in C.location) {
     i++;
-    assert_true(whitelistedLocationPropNames.includes(prop), prop + " is not safelisted for a cross-origin Location");
   }
-  assert_equals(i, whitelistedLocationPropNames.length, "Enumerate all safelisted cross-origin Location properties");
-}, "Can only enumerate safelisted properties");
+  assert_equals(i, 0, "There's nothing to enumerate for cross-origin Location properties");
+}, "Can only enumerate safelisted enumerable properties");
 
 /*
  * [[OwnPropertyKeys]]
  */
 
 addTest(function() {
   assert_array_equals(Object.getOwnPropertyNames(C).sort(),
                       whitelistedWindowPropNames,
                       "Object.getOwnPropertyNames() gives the right answer for cross-origin Window");
   assert_array_equals(Object.keys(C).sort(),
-                      whitelistedWindowPropNames,
+                      whitelistedWindowIndices,
                       "Object.keys() gives the right answer for cross-origin Window");
   assert_array_equals(Object.getOwnPropertyNames(C.location).sort(),
                       whitelistedLocationPropNames,
                       "Object.getOwnPropertyNames() gives the right answer for cross-origin Location");
-  assert_array_equals(Object.keys(C.location).sort(),
-                      whitelistedLocationPropNames,
-                      "Object.keys() gives the right answer for cross-origin Location");
+  assert_equals(Object.keys(C.location).length, 0,
+                "Object.keys() gives the right answer for cross-origin Location");
 }, "[[OwnPropertyKeys]] should return all properties from cross-origin objects");
 
 addTest(function() {
   assert_array_equals(Object.getOwnPropertySymbols(C), whitelistedSymbols,
     "Object.getOwnPropertySymbols() should return the three symbol-named properties that are exposed on a cross-origin Window");
   assert_array_equals(Object.getOwnPropertySymbols(C.location),
                       whitelistedSymbols,
     "Object.getOwnPropertySymbols() should return the three symbol-named properties that are exposed on a cross-origin Location");
