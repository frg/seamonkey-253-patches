# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1508874359 14400
# Node ID 75e3a4777a7f7aeba2cea8bae4ff64c67712f578
# Parent  abfe8cd47e93bd208a38004223ea49238d1b4f99
Bug 1409446 - Modify the extra-clip flag to instead track more useful information. r=ethlin,mstange

Instead of just keeping a count of how many "extra clips" (aka
out-of-band clips) we have pushed, track more complex information for
each clip. In particular, track the display item's normal clip chain, as
well as the clip id of the extra clip that was pushed. This will be
needed to override clip cache information in the next patch.

MozReview-Commit-ID: AWKDTkelhyL

diff --git a/gfx/webrender_bindings/WebRenderAPI.cpp b/gfx/webrender_bindings/WebRenderAPI.cpp
--- a/gfx/webrender_bindings/WebRenderAPI.cpp
+++ b/gfx/webrender_bindings/WebRenderAPI.cpp
@@ -630,17 +630,16 @@ WebRenderAPI::RunOnRenderThread(UniquePt
 {
   auto event = reinterpret_cast<uintptr_t>(aEvent.release());
   wr_api_send_external_event(mDocHandle, event);
 }
 
 DisplayListBuilder::DisplayListBuilder(PipelineId aId,
                                        const wr::LayoutSize& aContentSize,
                                        size_t aCapacity)
-  : mExtraClipCount(0)
 {
   MOZ_COUNT_CTOR(DisplayListBuilder);
   mWrState = wr_state_new(aId, aContentSize, aCapacity);
 }
 
 DisplayListBuilder::~DisplayListBuilder()
 {
   MOZ_COUNT_DTOR(DisplayListBuilder);
@@ -719,40 +718,57 @@ DisplayListBuilder::DefineClip(const May
       aAncestorClipId ? Stringify(aAncestorClipId.ref().id).c_str() : "(nil)",
       Stringify(aClipRect).c_str(), aMask,
       aMask ? Stringify(aMask->rect).c_str() : "none",
       aComplex ? aComplex->Length() : 0);
   return wr::WrClipId { clip_id };
 }
 
 void
-DisplayListBuilder::PushClip(const wr::WrClipId& aClipId, bool aExtra)
+DisplayListBuilder::PushClip(const wr::WrClipId& aClipId,
+                             const DisplayItemClipChain* aParent)
 {
   wr_dp_push_clip(mWrState, aClipId.id);
   WRDL_LOG("PushClip id=%" PRIu64 "\n", mWrState, aClipId.id);
-  if (!aExtra) {
+  if (!aParent) {
     mClipStack.push_back(wr::ScrollOrClipId(aClipId));
   } else {
-    mExtraClipCount++;
+    auto it = mCacheOverride.insert({ aParent, std::vector<wr::WrClipId>() });
+    it.first->second.push_back(aClipId);
+    WRDL_LOG("Pushing override %p -> %" PRIu64 "\n", mWrState, aParent, aClipId.id);
   }
 }
 
 void
-DisplayListBuilder::PopClip(bool aExtra)
+DisplayListBuilder::PopClip(const DisplayItemClipChain* aParent)
 {
   WRDL_LOG("PopClip\n", mWrState);
-  if (!aExtra) {
+  if (!aParent) {
     MOZ_ASSERT(mClipStack.back().is<wr::WrClipId>());
     mClipStack.pop_back();
   } else {
-    mExtraClipCount--;
+    auto it = mCacheOverride.find(aParent);
+    MOZ_ASSERT(it != mCacheOverride.end());
+    MOZ_ASSERT(!(it->second.empty()));
+    WRDL_LOG("Popping override %p -> %" PRIu64 "\n", mWrState, aParent, it->second.back().id);
+    it->second.pop_back();
+    if (it->second.empty()) {
+      mCacheOverride.erase(it);
+    }
   }
   wr_dp_pop_clip(mWrState);
 }
 
+Maybe<wr::WrClipId>
+DisplayListBuilder::GetCacheOverride(const DisplayItemClipChain* aParent)
+{
+  auto it = mCacheOverride.find(aParent);
+  return it == mCacheOverride.end() ? Nothing() : Some(it->second.back());
+}
+
 wr::WrStickyId
 DisplayListBuilder::DefineStickyFrame(const wr::LayoutRect& aContentRect,
                                       const wr::StickySideConstraint* aTop,
                                       const wr::StickySideConstraint* aRight,
                                       const wr::StickySideConstraint* aBottom,
                                       const wr::StickySideConstraint* aLeft)
 {
   uint64_t id = wr_dp_define_sticky_frame(mWrState, aContentRect, aTop,
diff --git a/gfx/webrender_bindings/WebRenderAPI.h b/gfx/webrender_bindings/WebRenderAPI.h
--- a/gfx/webrender_bindings/WebRenderAPI.h
+++ b/gfx/webrender_bindings/WebRenderAPI.h
@@ -17,16 +17,18 @@
 #include "mozilla/webrender/webrender_ffi.h"
 #include "mozilla/webrender/WebRenderTypes.h"
 #include "FrameMetrics.h"
 #include "GLTypes.h"
 #include "Units.h"
 
 namespace mozilla {
 
+struct DisplayItemClipChain;
+
 namespace widget {
 class CompositorWidget;
 }
 
 namespace layers {
 class CompositorBridgeParentBase;
 class WebRenderBridgeParent;
 }
@@ -229,18 +231,19 @@ public:
                            bool aIsBackfaceVisible);
   void PopStackingContext();
 
   wr::WrClipId DefineClip(const Maybe<layers::FrameMetrics::ViewID>& aAncestorScrollId,
                           const Maybe<wr::WrClipId>& aAncestorClipId,
                           const wr::LayoutRect& aClipRect,
                           const nsTArray<wr::ComplexClipRegion>* aComplex = nullptr,
                           const wr::WrImageMask* aMask = nullptr);
-  void PushClip(const wr::WrClipId& aClipId, bool aExtra = false);
-  void PopClip(bool aExtra = false);
+  void PushClip(const wr::WrClipId& aClipId, const DisplayItemClipChain* aParent = nullptr);
+  void PopClip(const DisplayItemClipChain* aParent = nullptr);
+  Maybe<wr::WrClipId> GetCacheOverride(const DisplayItemClipChain* aParent);
 
   wr::WrStickyId DefineStickyFrame(const wr::LayoutRect& aContentRect,
                                    const wr::StickySideConstraint* aTop,
                                    const wr::StickySideConstraint* aRight,
                                    const wr::StickySideConstraint* aBottom,
                                    const wr::StickySideConstraint* aLeft);
   void PushStickyFrame(const wr::WrStickyId& aStickyId);
   void PopStickyFrame();
@@ -402,33 +405,45 @@ public:
   Maybe<wr::WrClipId> TopmostClipId();
   // Same as TopmostClipId() but for scroll layers.
   layers::FrameMetrics::ViewID TopmostScrollId();
 
   // Try to avoid using this when possible.
   wr::WrState* Raw() { return mWrState; }
 
   // Return true if the current clip stack has any extra clip.
-  bool HasExtraClip() { return mExtraClipCount > 0; }
+  bool HasExtraClip() { return !mCacheOverride.empty(); }
 
 protected:
   wr::WrState* mWrState;
 
   // Track the stack of clip ids and scroll layer ids that have been pushed
   // (by PushClip and PushScrollLayer/PushClipAndScrollInfo, respectively) and
   // haven't yet been popped.
   std::vector<wr::ScrollOrClipId> mClipStack;
 
   // Track each scroll id that we encountered. We use this structure to
   // ensure that we don't define a particular scroll layer multiple times,
   // as that results in undefined behaviour in WR.
   std::unordered_set<layers::FrameMetrics::ViewID> mScrollIdsDefined;
 
-  // The number of extra clips that are in the stack.
-  uint32_t mExtraClipCount;
+  // A map that holds the cache overrides creates by "out of band" clips, i.e.
+  // clips that are generated by display items but that ScrollingLayersHelper
+  // doesn't know about. These are called "cache overrides" because while we're
+  // inside one of these clips, the WR clip stack is different from what
+  // ScrollingLayersHelper thinks it actually is (because of the out-of-band
+  // clip that was pushed onto the stack) and so ScrollingLayersHelper cannot
+  // use its clip cache as-is. Instead, any time ScrollingLayersHelper wants
+  // to define a new clip as a child of clip X, it should first check the
+  // cache overrides to see if there is an out-of-band clip Y that is already a
+  // child of X, and then define its clip as a child of Y instead. This map
+  // stores X -> ClipId of Y, which allows ScrollingLayersHelper to do the
+  // necessary lookup. Note that there theoretically might be multiple
+  // different "Y" clips which is why we need a vector.
+  std::unordered_map<const DisplayItemClipChain*, std::vector<wr::WrClipId>> mCacheOverride;
 
   friend class WebRenderAPI;
 };
 
 Maybe<wr::ImageFormat>
 SurfaceFormatToImageFormat(gfx::SurfaceFormat aFormat);
 
 } // namespace wr
diff --git a/layout/forms/nsButtonFrameRenderer.cpp b/layout/forms/nsButtonFrameRenderer.cpp
--- a/layout/forms/nsButtonFrameRenderer.cpp
+++ b/layout/forms/nsButtonFrameRenderer.cpp
@@ -378,17 +378,17 @@ nsDisplayButtonBorder::CreateWebRenderCo
   nsRect buttonRect = nsRect(ToReferenceFrame(), mFrame->GetSize());
   bool snap;
   nsRegion visible = GetBounds(aDisplayListBuilder, &snap);
   nsDisplayBoxShadowInner::CreateInsetBoxShadowWebRenderCommands(aBuilder,
                                                                  aSc,
                                                                  visible,
                                                                  mFrame,
                                                                  buttonRect);
-  mBorderRenderer->CreateWebRenderCommands(aBuilder, aResources, aSc);
+  mBorderRenderer->CreateWebRenderCommands(this, aBuilder, aResources, aSc);
 
   return true;
 }
 
 void
 nsDisplayButtonBorder::ComputeInvalidationRegion(nsDisplayListBuilder* aBuilder,
                                                  const nsDisplayItemGeometry* aGeometry,
                                                  nsRegion *aInvalidRegion) const
@@ -546,17 +546,17 @@ nsDisplayButtonForeground::CreateWebRend
                                                    mozilla::layers::WebRenderLayerManager* aManager,
                                                    nsDisplayListBuilder* aDisplayListBuilder)
 {
   ContainerLayerParameters parameter;
   if (GetLayerState(aDisplayListBuilder, aManager, parameter) != LAYER_ACTIVE) {
     return false;
   }
 
-  mBorderRenderer->CreateWebRenderCommands(aBuilder, aResources, aSc);
+  mBorderRenderer->CreateWebRenderCommands(this, aBuilder, aResources, aSc);
   return true;
 }
 
 nsresult
 nsButtonFrameRenderer::DisplayButton(nsDisplayListBuilder* aBuilder,
                                      nsDisplayList* aBackground,
                                      nsDisplayList* aForeground)
 {
diff --git a/layout/generic/nsColumnSetFrame.cpp b/layout/generic/nsColumnSetFrame.cpp
--- a/layout/generic/nsColumnSetFrame.cpp
+++ b/layout/generic/nsColumnSetFrame.cpp
@@ -122,17 +122,17 @@ nsDisplayColumnRule::CreateWebRenderComm
 
   for (auto iter = mBorderRenderers.begin(); iter != mBorderRenderers.end(); iter++) {
     if (!iter->CanCreateWebRenderCommands()) {
       return false;
     }
   }
 
   for (auto iter = mBorderRenderers.begin(); iter != mBorderRenderers.end(); iter++) {
-    iter->CreateWebRenderCommands(aBuilder, aResources, aSc);
+    iter->CreateWebRenderCommands(this, aBuilder, aResources, aSc);
   }
 
   return true;
 }
 
 /**
  * Tracking issues:
  *
diff --git a/layout/painting/nsCSSRendering.cpp b/layout/painting/nsCSSRendering.cpp
--- a/layout/painting/nsCSSRendering.cpp
+++ b/layout/painting/nsCSSRendering.cpp
@@ -713,17 +713,17 @@ nsCSSRendering::CreateWebRenderCommandsF
                                            aBorderArea,
                                            aForFrame->StyleContext(),
                                            aForFrame->GetSkipSides());
 
     if (br) {
       if (!br->CanCreateWebRenderCommands()) {
         return false;
       }
-      br->CreateWebRenderCommands(aBuilder, aResources, aSc);
+      br->CreateWebRenderCommands(aItem, aBuilder, aResources, aSc);
       return true;
     }
   }
 
   // Next try to draw an image border
   const nsStyleBorder *styleBorder = aForFrame->StyleContext()->StyleBorder();
   const nsStyleImage* image = &styleBorder->mBorderImageSource;
 
diff --git a/layout/painting/nsCSSRenderingBorders.cpp b/layout/painting/nsCSSRenderingBorders.cpp
--- a/layout/painting/nsCSSRenderingBorders.cpp
+++ b/layout/painting/nsCSSRenderingBorders.cpp
@@ -3598,17 +3598,18 @@ nsCSSBorderRenderer::CanCreateWebRenderC
       return false;
     }
   }
 
   return true;
 }
 
 void
-nsCSSBorderRenderer::CreateWebRenderCommands(wr::DisplayListBuilder& aBuilder,
+nsCSSBorderRenderer::CreateWebRenderCommands(nsDisplayItem* aItem,
+                                             wr::DisplayListBuilder& aBuilder,
                                              wr::IpcResourceUpdateQueue& aResources,
                                              const layers::StackingContextHelper& aSc)
 {
   LayoutDeviceRect outerRect = LayoutDeviceRect::FromUnknownRect(mOuterRect);
   wr::LayoutRect transformedRect = aSc.ToRelativeLayoutRect(outerRect);
   wr::BorderSide side[4];
   NS_FOR_CSS_SIDES(i) {
     side[i] = wr::ToBorderSide(ToDeviceColor(mBorderColors[i]), mBorderStyles[i]);
@@ -3618,29 +3619,29 @@ nsCSSBorderRenderer::CreateWebRenderComm
                                                      LayoutDeviceSize::FromUnknownSize(mBorderRadii[1]),
                                                      LayoutDeviceSize::FromUnknownSize(mBorderRadii[3]),
                                                      LayoutDeviceSize::FromUnknownSize(mBorderRadii[2]));
 
   if (mLocalClip) {
     LayoutDeviceRect clip = LayoutDeviceRect::FromUnknownRect(mLocalClip.value());
     wr::LayoutRect clipRect = aSc.ToRelativeLayoutRect(clip);
     wr::WrClipId clipId = aBuilder.DefineClip(Nothing(), Nothing(), clipRect);
-    aBuilder.PushClip(clipId, true);
+    aBuilder.PushClip(clipId, aItem->GetClipChain());
   }
 
   Range<const wr::BorderSide> wrsides(side, 4);
   aBuilder.PushBorder(transformedRect,
                       transformedRect,
                       mBackfaceIsVisible,
                       wr::ToBorderWidths(mBorderWidths[0], mBorderWidths[1], mBorderWidths[2], mBorderWidths[3]),
                       wrsides,
                       borderRadius);
 
   if (mLocalClip) {
-    aBuilder.PopClip(true);
+    aBuilder.PopClip(aItem->GetClipChain());
   }
 }
 
 /* static */Maybe<nsCSSBorderImageRenderer>
 nsCSSBorderImageRenderer::CreateBorderImageRenderer(nsPresContext* aPresContext,
                                                     nsIFrame* aForFrame,
                                                     const nsRect& aBorderArea,
                                                     const nsStyleBorder& aStyleBorder,
diff --git a/layout/painting/nsCSSRenderingBorders.h b/layout/painting/nsCSSRenderingBorders.h
--- a/layout/painting/nsCSSRenderingBorders.h
+++ b/layout/painting/nsCSSRenderingBorders.h
@@ -103,17 +103,18 @@ public:
                       nscolor aBackgroundColor,
                       bool aBackfaceIsVisible,
                       const mozilla::Maybe<Rect>& aClipRect);
 
   // draw the entire border
   void DrawBorders();
 
   bool CanCreateWebRenderCommands();
-  void CreateWebRenderCommands(mozilla::wr::DisplayListBuilder& aBuilder,
+  void CreateWebRenderCommands(nsDisplayItem* aItem,
+                               mozilla::wr::DisplayListBuilder& aBuilder,
                                mozilla::wr::IpcResourceUpdateQueue& aResources,
                                const mozilla::layers::StackingContextHelper& aSc);
 
   // utility function used for background painting as well as borders
   static void ComputeInnerRadii(const RectCornerRadii& aRadii,
                                 const Float* aBorderSizes,
                                 RectCornerRadii* aInnerRadiiRet);
 
diff --git a/layout/painting/nsDisplayList.cpp b/layout/painting/nsDisplayList.cpp
--- a/layout/painting/nsDisplayList.cpp
+++ b/layout/painting/nsDisplayList.cpp
@@ -4434,17 +4434,17 @@ nsDisplayOutline::CreateWebRenderCommand
                                           mozilla::layers::WebRenderLayerManager* aManager,
                                           nsDisplayListBuilder* aDisplayListBuilder)
 {
   ContainerLayerParameters parameter;
   if (GetLayerState(aDisplayListBuilder, aManager, parameter) != LAYER_ACTIVE) {
     return false;
   }
 
-  mBorderRenderer->CreateWebRenderCommands(aBuilder, aResources, aSc);
+  mBorderRenderer->CreateWebRenderCommands(this, aBuilder, aResources, aSc);
   return true;
 }
 
 bool
 nsDisplayOutline::IsInvisibleInRect(const nsRect& aRect) const
 {
   const nsStyleOutline* outline = mFrame->StyleOutline();
   nsRect borderBox(ToReferenceFrame(), mFrame->GetSize());
@@ -9046,23 +9046,23 @@ nsDisplayMask::CreateWebRenderCommands(m
                                                                             aSc, aDisplayListBuilder,
                                                                             bounds);
   if (mask) {
     wr::WrClipId clipId = aBuilder.DefineClip(Nothing(), Nothing(),
         aSc.ToRelativeLayoutRect(bounds), nullptr, mask.ptr());
     // Don't record this clip push in aBuilder's internal clip stack, because
     // otherwise any nested ScrollingLayersHelper instances that are created
     // will get confused about which clips are pushed.
-    aBuilder.PushClip(clipId, /*aMask*/ true);
+    aBuilder.PushClip(clipId, GetClipChain());
   }
 
   nsDisplaySVGEffects::CreateWebRenderCommands(aBuilder, aResources, aSc, aManager, aDisplayListBuilder);
 
   if (mask) {
-    aBuilder.PopClip(/*aMask*/ true);
+    aBuilder.PopClip(GetClipChain());
   }
 
   return true;
 }
 
 #ifdef MOZ_DUMP_PAINTING
 void
 nsDisplayMask::PrintEffects(nsACString& aTo)
