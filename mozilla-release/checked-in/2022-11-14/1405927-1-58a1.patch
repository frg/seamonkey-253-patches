# HG changeset patch
# User Alexis Beingessner <a.beingessner@gmail.com>
# Date 1507139391 14400
# Node ID e41db689a6627bc7f6b4641697a8cc82cace496c
# Parent  8a0513c39545528b9cb88436550ca8993500f92a
Bug 1405927 - Remove TextLayer support from nsDisplayText. r=mattwoodrow

MozReview-Commit-ID: J1BDkXZdvnc

diff --git a/layout/generic/nsTextFrame.cpp b/layout/generic/nsTextFrame.cpp
--- a/layout/generic/nsTextFrame.cpp
+++ b/layout/generic/nsTextFrame.cpp
@@ -4974,22 +4974,16 @@ public:
   }
   virtual void HitTest(nsDisplayListBuilder* aBuilder, const nsRect& aRect,
                        HitTestState* aState,
                        nsTArray<nsIFrame*> *aOutFrames) override {
     if (nsRect(ToReferenceFrame(), mFrame->GetSize()).Intersects(aRect)) {
       aOutFrames->AppendElement(mFrame);
     }
   }
-  virtual LayerState GetLayerState(nsDisplayListBuilder* aBuilder,
-                                   LayerManager* aManager,
-                                   const ContainerLayerParameters& aParameters) override;
-  virtual already_AddRefed<Layer> BuildLayer(nsDisplayListBuilder* aBuilder,
-                                             LayerManager* aManager,
-                                             const ContainerLayerParameters& aContainerParameters) override;
   virtual bool CreateWebRenderCommands(mozilla::wr::DisplayListBuilder& aBuilder,
                                        mozilla::wr::IpcResourceUpdateQueue& aResources,
                                        const StackingContextHelper& aSc,
                                        WebRenderLayerManager* aManager,
                                        nsDisplayListBuilder* aDisplayListBuilder) override;
   virtual void Paint(nsDisplayListBuilder* aBuilder,
                      gfxContext* aCtx) override;
   NS_DISPLAY_DECL_NAME("Text", TYPE_TEXT)
@@ -5131,56 +5125,16 @@ nsDisplayText::nsDisplayText(nsDisplayLi
 {
   MOZ_COUNT_CTOR(nsDisplayText);
   mIsFrameSelected = aIsSelected;
   mBounds = mFrame->GetVisualOverflowRectRelativeToSelf() + ToReferenceFrame();
     // Bug 748228
   mBounds.Inflate(mFrame->PresContext()->AppUnitsPerDevPixel());
 }
 
-LayerState
-nsDisplayText::GetLayerState(nsDisplayListBuilder* aBuilder,
-                             LayerManager* aManager,
-                             const ContainerLayerParameters& aParameters)
-{
-
-  // Are we doing text layers or webrender?
-  if (!(gfxPrefs::LayersAllowTextLayers() &&
-      CanUseAdvancedLayer(aBuilder->GetWidgetLayerManager()))) {
-    return mozilla::LAYER_NONE;
-  }
-
-  // If we haven't yet, compute the layout/style of the text by running
-  // the painting algorithm with a TextDrawTarget (doesn't actually paint).
-  if (!mTextDrawer) {
-    mTextDrawer = new TextDrawTarget();
-    RefPtr<gfxContext> captureCtx = gfxContext::CreateOrNull(mTextDrawer);
-
-    // TODO: Paint() checks mDisableSubpixelAA, we should too.
-    RenderToContext(captureCtx, aBuilder, true);
-  }
-
-  if (!mTextDrawer->CanSerializeFonts()) {
-    return mozilla::LAYER_NONE;
-  }
-
-  // If we're using the webrender backend, then we're good to go!
-  if (aManager->GetBackendType() == layers::LayersBackend::LAYERS_WR) {
-    return mozilla::LAYER_ACTIVE;
-  }
-
-  // If we're using the TextLayer backend, then we need to make sure
-  // the input is plain enough for it to handle
-  if (!mTextDrawer->ContentsAreSimple()) {
-    return mozilla::LAYER_NONE;
-  }
-
-  return mozilla::LAYER_ACTIVE;
-}
-
 void
 nsDisplayText::Paint(nsDisplayListBuilder* aBuilder,
                      gfxContext* aCtx) {
   AUTO_PROFILER_LABEL("nsDisplayText::Paint", GRAPHICS);
 
   DrawTargetAutoDisableSubpixelAntialiasing disable(aCtx->GetDrawTarget(),
                                                     mDisableSubpixelAA);
   RenderToContext(aCtx, aBuilder);
@@ -5188,88 +5142,39 @@ nsDisplayText::Paint(nsDisplayListBuilde
 
 bool
 nsDisplayText::CreateWebRenderCommands(mozilla::wr::DisplayListBuilder& aBuilder,
                                        mozilla::wr::IpcResourceUpdateQueue& aResources,
                                        const StackingContextHelper& aSc,
                                        WebRenderLayerManager* aManager,
                                        nsDisplayListBuilder* aDisplayListBuilder)
 {
-  ContainerLayerParameters parameter;
-  if (GetLayerState(aDisplayListBuilder, aManager, parameter) != LAYER_ACTIVE) {
+  if (!gfxPrefs::LayersAllowTextLayers()) {
     return false;
   }
 
   if (mBounds.IsEmpty()) {
     return true;
   }
 
-  mTextDrawer->CreateWebRenderCommands(aBuilder, aSc, aManager, this, mBounds);
+  RefPtr<TextDrawTarget> textDrawer = new TextDrawTarget();
+  RefPtr<gfxContext> captureCtx = gfxContext::CreateOrNull(textDrawer);
+
+  // TODO: Paint() checks mDisableSubpixelAA, we should too.
+  RenderToContext(captureCtx, aDisplayListBuilder, true);
+
+  if (!textDrawer->CanSerializeFonts()) {
+    return false;
+  }
+
+  textDrawer->CreateWebRenderCommands(aBuilder, aSc, aManager, this, mBounds);
 
   return true;
 }
 
-already_AddRefed<layers::Layer>
-nsDisplayText::BuildLayer(nsDisplayListBuilder* aBuilder,
-                          LayerManager* aManager,
-                          const ContainerLayerParameters& aContainerParameters)
-{
-  // If we're using webrender, we want layerless rendering, so emit a dummy.
-  // See CreateWebRenderCommands for actual drawing code.
-  if (aManager->GetBackendType() == layers::LayersBackend::LAYERS_WR) {
-    return BuildDisplayItemLayer(aBuilder, aManager, aContainerParameters);
-  }
-
-  // We should have all the glyphs recorded now, build
-  // the TextLayer.
-  RefPtr<layers::TextLayer> layer = static_cast<layers::TextLayer*>
-    (aManager->GetLayerBuilder()->GetLeafLayerFor(aBuilder, this));
-  if (!layer) {
-    layer = aManager->CreateTextLayer();
-  }
-
-  // GetLayerState has guaranteed to us that we have exactly one font
-  // so this will be overwritten by the time we use it.
-  ScaledFont* font = nullptr;
-
-  nsTArray<GlyphArray> allGlyphs;
-  size_t totalLength = 0;
-  for (auto& part : mTextDrawer->GetParts()) {
-    totalLength += part.text.Length();
-  }
-  allGlyphs.SetCapacity(totalLength);
-
-  for (auto& part : mTextDrawer->GetParts()) {
-    for (const mozilla::layout::TextRunFragment& text : part.text) {
-      if (!font) {
-        font = text.font;
-      }
-
-      GlyphArray* glyphs = allGlyphs.AppendElement();
-      glyphs->glyphs() = text.glyphs;
-      glyphs->color() = text.color;
-    }
-  }
-
-  MOZ_ASSERT(font);
-
-  layer->SetGlyphs(Move(allGlyphs));
-  layer->SetScaledFont(font);
-
-  auto A2D = mFrame->PresContext()->AppUnitsPerDevPixel();
-  bool dummy;
-  const LayoutDeviceIntRect destBounds =
-          LayoutDeviceIntRect::FromAppUnitsToOutside(GetBounds(aBuilder, &dummy), A2D);
-  layer->SetBounds(IntRect(destBounds.x, destBounds.y, destBounds.width, destBounds.height));
-
-  layer->SetBaseTransform(gfx::Matrix4x4::Translation(aContainerParameters.mOffset.x,
-                                                      aContainerParameters.mOffset.y, 0));
-  return layer.forget();
-}
-
 void
 nsDisplayText::RenderToContext(gfxContext* aCtx, nsDisplayListBuilder* aBuilder, bool aIsRecording)
 {
   nsTextFrame* f = static_cast<nsTextFrame*>(mFrame);
 
   // Add 1 pixel of dirty area around mVisibleRect to allow us to paint
   // antialiased pixels beyond the measured text extents.
   // This is temporary until we do this in the actual calculation of text extents.
