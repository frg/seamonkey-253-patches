# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1505907021 -7200
# Node ID bb40b2b9b866d49cfe13c01b1b38a64fcdbb783e
# Parent  43b328d14287281b8d3c4ef0f095e560e9b6e377
Bug 1401508 - Skip creating GetCapabilitiesExecutor for default Promises in Promise.all/race. r=till

diff --git a/js/src/builtin/Promise.cpp b/js/src/builtin/Promise.cpp
--- a/js/src/builtin/Promise.cpp
+++ b/js/src/builtin/Promise.cpp
@@ -466,39 +466,39 @@ PromiseHasAnyFlag(PromiseObject& promise
     return promise.getFixedSlot(PromiseSlot_Flags).toInt32() & flag;
 }
 
 static bool ResolvePromiseFunction(JSContext* cx, unsigned argc, Value* vp);
 static bool RejectPromiseFunction(JSContext* cx, unsigned argc, Value* vp);
 
 // ES2016, 25.4.1.3.
 static MOZ_MUST_USE bool
-CreateResolvingFunctions(JSContext* cx, HandleValue promise,
-                         MutableHandleValue resolveVal,
-                         MutableHandleValue rejectVal)
+CreateResolvingFunctions(JSContext* cx, HandleObject promise,
+                         MutableHandleObject resolveFn,
+                         MutableHandleObject rejectFn)
 {
     RootedAtom funName(cx, cx->names().empty);
     RootedFunction resolve(cx, NewNativeFunction(cx, ResolvePromiseFunction, 1, funName,
                                                  gc::AllocKind::FUNCTION_EXTENDED, GenericObject));
     if (!resolve)
         return false;
 
     RootedFunction reject(cx, NewNativeFunction(cx, RejectPromiseFunction, 1, funName,
                                                  gc::AllocKind::FUNCTION_EXTENDED, GenericObject));
     if (!reject)
         return false;
 
-    resolve->setExtendedSlot(ResolveFunctionSlot_Promise, promise);
+    resolve->setExtendedSlot(ResolveFunctionSlot_Promise, ObjectValue(*promise));
     resolve->setExtendedSlot(ResolveFunctionSlot_RejectFunction, ObjectValue(*reject));
 
-    reject->setExtendedSlot(RejectFunctionSlot_Promise, promise);
+    reject->setExtendedSlot(RejectFunctionSlot_Promise, ObjectValue(*promise));
     reject->setExtendedSlot(RejectFunctionSlot_ResolveFunction, ObjectValue(*resolve));
 
-    resolveVal.setObject(*resolve);
-    rejectVal.setObject(*reject);
+    resolveFn.set(resolve);
+    rejectFn.set(reject);
 
     return true;
 }
 
 static void ClearResolutionFunctionSlots(JSFunction* resolutionFun);
 static MOZ_MUST_USE bool RejectMaybeWrappedPromise(JSContext *cx, HandleObject promiseObj,
                                                    HandleValue reason);
 
@@ -855,16 +855,42 @@ CreatePromiseObjectWithoutResolutionFunc
     if (!promise)
         return nullptr;
 
     AddPromiseFlags(*promise, PROMISE_FLAG_DEFAULT_RESOLVE_FUNCTION |
                     PROMISE_FLAG_DEFAULT_REJECT_FUNCTION);
     return promise;
 }
 
+static MOZ_MUST_USE PromiseObject*
+CreatePromiseWithDefaultResolutionFunctions(JSContext* cx, MutableHandleObject resolve,
+                                            MutableHandleObject reject)
+{
+    // ES2016, 25.4.3.1., as if called with GetCapabilitiesExecutor as the
+    // executor argument.
+
+    // Steps 1-2 (Not applicable).
+
+    // Steps 3-7.
+    Rooted<PromiseObject*> promise(cx, CreatePromiseObjectInternal(cx));
+    if (!promise)
+        return nullptr;
+
+    // Step 8.
+    if (!CreateResolvingFunctions(cx, promise, resolve, reject))
+        return nullptr;
+
+    promise->setFixedSlot(PromiseSlot_RejectFunction, ObjectValue(*reject));
+
+    // Steps 9-10 (Not applicable).
+
+    // Step 11.
+    return promise;
+}
+
 // ES2016, 25.4.1.5.
 static MOZ_MUST_USE bool
 NewPromiseCapability(JSContext* cx, HandleObject C, MutableHandleObject promise,
                      MutableHandleObject resolve, MutableHandleObject reject,
                      bool canOmitResolutionFunctions)
 {
     RootedValue cVal(cx, ObjectValue(*C));
 
@@ -878,18 +904,25 @@ NewPromiseCapability(JSContext* cx, Hand
     // resolve/reject functions won't ever escape to content, we can skip
     // creating and calling the executor function and instead return a Promise
     // marked as having default resolve/reject functions.
     //
     // This can't be used in Promise.all and Promise.race because we have to
     // pass the reject (and resolve, in the race case) function to thenables
     // in the list passed to all/race, which (potentially) means exposing them
     // to content.
-    if (canOmitResolutionFunctions && IsNativeFunction(cVal, PromiseConstructor)) {
-        promise.set(CreatePromiseObjectWithoutResolutionFunctions(cx));
+    //
+    // For Promise.all and Promise.race we can only optimize away the creation
+    // of the GetCapabilitiesExecutor function, and directly allocate the
+    // result promise instead of invoking the Promise constructor.
+    if (IsNativeFunction(cVal, PromiseConstructor)) {
+        if (canOmitResolutionFunctions)
+            promise.set(CreatePromiseObjectWithoutResolutionFunctions(cx));
+        else
+            promise.set(CreatePromiseWithDefaultResolutionFunctions(cx, resolve, reject));
         if (!promise)
             return false;
         return true;
     }
 
     // Step 3 (omitted).
 
     // Step 4.
@@ -1250,42 +1283,43 @@ PromiseResolveThenableJob(JSContext* cx,
     CallArgs args = CallArgsFromVp(argc, vp);
 
     RootedFunction job(cx, &args.callee().as<JSFunction>());
     RootedValue then(cx, job->getExtendedSlot(ThenableJobSlot_Handler));
     MOZ_ASSERT(!IsWrapper(&then.toObject()));
     RootedNativeObject jobArgs(cx, &job->getExtendedSlot(ThenableJobSlot_JobData)
                                     .toObject().as<NativeObject>());
 
-    RootedValue promise(cx, jobArgs->getDenseElement(ThenableJobDataIndex_Promise));
+    RootedObject promise(cx, &jobArgs->getDenseElement(ThenableJobDataIndex_Promise).toObject());
     RootedValue thenable(cx, jobArgs->getDenseElement(ThenableJobDataIndex_Thenable));
 
     // Step 1.
-    RootedValue resolveVal(cx);
-    RootedValue rejectVal(cx);
-    if (!CreateResolvingFunctions(cx, promise, &resolveVal, &rejectVal))
+    RootedObject resolveFn(cx);
+    RootedObject rejectFn(cx);
+    if (!CreateResolvingFunctions(cx, promise, &resolveFn, &rejectFn))
         return false;
 
     // Step 2.
     FixedInvokeArgs<2> args2(cx);
-    args2[0].set(resolveVal);
-    args2[1].set(rejectVal);
+    args2[0].setObject(*resolveFn);
+    args2[1].setObject(*rejectFn);
 
     RootedValue rval(cx);
 
     // In difference to the usual pattern, we return immediately on success.
     if (Call(cx, then, thenable, args2, &rval))
         return true;
 
     if (!MaybeGetAndClearException(cx, &rval))
         return false;
 
     FixedInvokeArgs<1> rejectArgs(cx);
     rejectArgs[0].set(rval);
 
+    RootedValue rejectVal(cx, ObjectValue(*rejectFn));
     return Call(cx, rejectVal, UndefinedHandleValue, rejectArgs, &rval);
 }
 
 /**
  * Tells the embedding to enqueue a Promise resolve thenable job, based on
  * three parameters:
  * promiseToResolve_ - The promise to resolve, obviously.
  * thenable_ - The thenable to resolve the Promise with.
@@ -1580,64 +1614,64 @@ PromiseObject::create(JSContext* cx, Han
 
 
     // Steps 3-7.
     Rooted<PromiseObject*> promise(cx, CreatePromiseObjectInternal(cx, usedProto, needsWrapping,
                                                                    false));
     if (!promise)
         return nullptr;
 
-    RootedValue promiseVal(cx, ObjectValue(*promise));
-    if (needsWrapping && !cx->compartment()->wrap(cx, &promiseVal))
+    RootedObject promiseObj(cx, promise);
+    if (needsWrapping && !cx->compartment()->wrap(cx, &promiseObj))
         return nullptr;
 
     // Step 8.
     // The resolving functions are created in the compartment active when the
     // (maybe wrapped) Promise constructor was called. They contain checks and
     // can unwrap the Promise if required.
-    RootedValue resolveVal(cx);
-    RootedValue rejectVal(cx);
-    if (!CreateResolvingFunctions(cx, promiseVal, &resolveVal, &rejectVal))
+    RootedObject resolveFn(cx);
+    RootedObject rejectFn(cx);
+    if (!CreateResolvingFunctions(cx, promiseObj, &resolveFn, &rejectFn))
         return nullptr;
 
     // Need to wrap the resolution functions before storing them on the Promise.
     if (needsWrapping) {
         AutoCompartment ac(cx, promise);
-        RootedValue wrappedRejectVal(cx, rejectVal);
-        if (!cx->compartment()->wrap(cx, &wrappedRejectVal))
+        RootedObject wrappedRejectFn(cx, rejectFn);
+        if (!cx->compartment()->wrap(cx, &wrappedRejectFn))
             return nullptr;
-        promise->setFixedSlot(PromiseSlot_RejectFunction, wrappedRejectVal);
+        promise->setFixedSlot(PromiseSlot_RejectFunction, ObjectValue(*wrappedRejectFn));
     } else {
-        promise->setFixedSlot(PromiseSlot_RejectFunction, rejectVal);
+        promise->setFixedSlot(PromiseSlot_RejectFunction, ObjectValue(*rejectFn));
     }
 
     // Step 9.
     bool success;
     {
         FixedInvokeArgs<2> args(cx);
 
-        args[0].set(resolveVal);
-        args[1].set(rejectVal);
+        args[0].setObject(*resolveFn);
+        args[1].setObject(*rejectFn);
 
         RootedValue calleeOrRval(cx, ObjectValue(*executor));
         success = Call(cx, calleeOrRval, UndefinedHandleValue, args, &calleeOrRval);
     }
 
     // Step 10.
     if (!success) {
         RootedValue exceptionVal(cx);
         if (!MaybeGetAndClearException(cx, &exceptionVal))
             return nullptr;
 
         FixedInvokeArgs<1> args(cx);
 
         args[0].set(exceptionVal);
 
-        // |rejectVal| is unused after this, so we can safely write to it.
-        if (!Call(cx, rejectVal, UndefinedHandleValue, args, &rejectVal))
+        RootedValue calleeOrRval(cx, ObjectValue(*rejectFn));
+        if (!Call(cx, calleeOrRval, UndefinedHandleValue, args, &calleeOrRval))
             return nullptr;
     }
 
     // Let the Debugger know about this Promise.
     JS::dbg::onNewPromise(cx, promise);
 
     // Step 11.
     return promise;
@@ -1993,17 +2027,17 @@ PerformPromiseAll(JSContext *cx, JS::For
                 return false;
         }
 
         // Step i.
         // Sadly, because someone could have overridden
         // "resolve" on the canonical Promise constructor.
         RootedValue nextPromise(cx);
         RootedValue staticResolve(cx);
-        if (!GetProperty(cx, CVal, cx->names().resolve, &staticResolve))
+        if (!GetProperty(cx, C, CVal, cx->names().resolve, &staticResolve))
             return false;
 
         FixedInvokeArgs<1> resolveArgs(cx);
         resolveArgs[0].set(nextValue);
         if (!Call(cx, staticResolve, CVal, resolveArgs, &nextPromise))
             return false;
 
         // Step j.
@@ -2202,17 +2236,17 @@ PerformPromiseRace(JSContext *cx, JS::Fo
             return true;
         }
 
         // Step h.
         // Sadly, because someone could have overridden
         // "resolve" on the canonical Promise constructor.
         RootedValue nextPromise(cx);
         RootedValue staticResolve(cx);
-        if (!GetProperty(cx, CVal, cx->names().resolve, &staticResolve))
+        if (!GetProperty(cx, C, CVal, cx->names().resolve, &staticResolve))
             return false;
 
         FixedInvokeArgs<1> resolveArgs(cx);
         resolveArgs[0].set(nextValue);
         if (!Call(cx, staticResolve, CVal, resolveArgs, &nextPromise))
             return false;
 
         // Step i.
@@ -3057,25 +3091,25 @@ PerformPromiseThenWithReaction(JSContext
  * the call to |promise.then| would use the original |Promise| constructor to
  * create the resulting promise, this function skips the call to |promise.then|
  * and thus creating a new promise that would not be observable by content.
  */
 static MOZ_MUST_USE bool
 BlockOnPromise(JSContext* cx, HandleValue promiseVal, HandleObject blockedPromise_,
                HandleValue onFulfilled, HandleValue onRejected)
 {
-    RootedValue thenVal(cx);
-    if (!GetProperty(cx, promiseVal, cx->names().then, &thenVal))
+    RootedObject promiseObj(cx, ToObject(cx, promiseVal));
+    if (!promiseObj)
         return false;
 
-    RootedObject promiseObj(cx);
-    if (promiseVal.isObject())
-        promiseObj = &promiseVal.toObject();
-
-    if (promiseObj && promiseObj->is<PromiseObject>() && IsNativeFunction(thenVal, Promise_then)) {
+    RootedValue thenVal(cx);
+    if (!GetProperty(cx, promiseObj, promiseVal, cx->names().then, &thenVal))
+        return false;
+
+    if (promiseObj->is<PromiseObject>() && IsNativeFunction(thenVal, Promise_then)) {
         // |promise| is an unwrapped Promise, and |then| is the original
         // |Promise.prototype.then|, inline it here.
         // 25.4.5.3., step 3.
         RootedObject PromiseCtor(cx);
         if (!GetBuiltinConstructor(cx, JSProto_Promise, &PromiseCtor))
             return false;
 
         RootedObject C(cx, SpeciesConstructor(cx, promiseObj, JSProto_Promise, IsPromiseSpecies));
@@ -3115,17 +3149,17 @@ BlockOnPromise(JSContext* cx, HandleValu
             return false;
     }
 
     // In case the value to depend on isn't an object at all, there's nothing
     // more to do here: we can only add reactions to Promise objects
     // (potentially after unwrapping them), and non-object values can't be
     // Promise objects. This can happen if Promise.all is called on an object
     // with a `resolve` method that returns primitives.
-    if (!promiseObj)
+    if (!promiseVal.isObject())
         return true;
 
     // The object created by the |promise.then| call or the inlined version
     // of it above is visible to content (either because |promise.then| was
     // overridden by content and could leak it, or because a constructor
     // other than the original value of |Promise| was used to create it).
     // To have both that object and |blockedPromise| show up as dependent
     // promises in the debugger, add a dummy reaction to the list of reject
