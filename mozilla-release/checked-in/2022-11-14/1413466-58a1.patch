# HG changeset patch
# User Bevis Tseng <btseng@mozilla.com>
# Date 1509520703 -28800
# Node ID 15f1ca2cd69383674b0005998f226232e829f654
# Parent  2a2a58f090e899746a24f934eaa0e25ee5910f05
Bug 1413466 - Fire events after iterating ServiceWorkerManager::mControlledDocuments is done. r=bkelly

It's possible to modify mControlledDocuments from oncontrollerchange handler.
We should fire events after iteration to prevent updating this hashtable while reading it.

diff --git a/dom/workers/ServiceWorkerManager.cpp b/dom/workers/ServiceWorkerManager.cpp
--- a/dom/workers/ServiceWorkerManager.cpp
+++ b/dom/workers/ServiceWorkerManager.cpp
@@ -3363,26 +3363,34 @@ ServiceWorkerManager::SetSkipWaitingFlag
     registration->TryToActivateAsync();
   }
 }
 
 void
 ServiceWorkerManager::FireControllerChange(ServiceWorkerRegistrationInfo* aRegistration)
 {
   AssertIsOnMainThread();
+
+  AutoTArray<nsCOMPtr<nsIDocument>, 16> documents;
   for (auto iter = mControlledDocuments.Iter(); !iter.Done(); iter.Next()) {
     if (iter.UserData() != aRegistration) {
       continue;
     }
 
     nsCOMPtr<nsIDocument> doc = do_QueryInterface(iter.Key());
     if (NS_WARN_IF(!doc)) {
       continue;
     }
 
+    documents.AppendElement(doc);
+  }
+
+  // Fire event after iterating mControlledDocuments is done to prevent
+  // modification by reentering from the event handlers during iteration.
+  for (auto& doc : documents) {
     FireControllerChangeOnDocument(doc);
   }
 }
 
 already_AddRefed<ServiceWorkerRegistrationInfo>
 ServiceWorkerManager::GetRegistration(nsIPrincipal* aPrincipal,
                                       const nsACString& aScope) const
 {
