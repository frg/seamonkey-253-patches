# HG changeset patch
# User Boris Chiou <boris.chiou@gmail.com>
# Date 1509131190 -7200
# Node ID 143169e5a42dac7c6a2a24b68494886b44ec748c
# Parent  576f9e9d18c85c59d23c699eacf087c94e88196b
Bug 1340005 - Part 2: Implement AnimationValue::Transform. r=birtles

MozReview-Commit-ID: BDKcpDIM9nb

diff --git a/gfx/layers/AnimationHelper.cpp b/gfx/layers/AnimationHelper.cpp
--- a/gfx/layers/AnimationHelper.cpp
+++ b/gfx/layers/AnimationHelper.cpp
@@ -469,17 +469,19 @@ ToAnimationValue(const Animatable& aAnim
   AnimationValue result;
 
   switch (aAnimatable.type()) {
     case Animatable::Tnull_t:
       break;
     case Animatable::TArrayOfTransformFunction: {
         const InfallibleTArray<TransformFunction>& transforms =
           aAnimatable.get_ArrayOfTransformFunction();
-        result.mGecko.SetTransformValue(CreateCSSValueList(transforms));
+        RefPtr<nsCSSValueSharedList> list(CreateCSSValueList(transforms));
+        MOZ_ASSERT(list, "Transform list should be non null");
+        result = AnimationValue::Transform(StyleBackendType::Gecko, *list);
       }
       break;
     case Animatable::Tfloat:
       result = AnimationValue::Opacity(StyleBackendType::Gecko,
                                        aAnimatable.get_float());
       break;
     default:
       MOZ_ASSERT_UNREACHABLE("Unsupported type");
diff --git a/layout/style/ServoBindingList.h b/layout/style/ServoBindingList.h
--- a/layout/style/ServoBindingList.h
+++ b/layout/style/ServoBindingList.h
@@ -364,16 +364,19 @@ SERVO_BINDING_FUNC(Servo_Shorthand_Anima
 SERVO_BINDING_FUNC(Servo_AnimationValue_GetOpacity, float,
                    RawServoAnimationValueBorrowed value)
 SERVO_BINDING_FUNC(Servo_AnimationValue_Opacity,
                    RawServoAnimationValueStrong,
                    float)
 SERVO_BINDING_FUNC(Servo_AnimationValue_GetTransform, void,
                    RawServoAnimationValueBorrowed value,
                    RefPtr<nsCSSValueSharedList>* list)
+SERVO_BINDING_FUNC(Servo_AnimationValue_Transform,
+                   RawServoAnimationValueStrong,
+                   const nsCSSValueSharedList& list)
 SERVO_BINDING_FUNC(Servo_AnimationValue_DeepEqual, bool,
                    RawServoAnimationValueBorrowed,
                    RawServoAnimationValueBorrowed)
 SERVO_BINDING_FUNC(Servo_AnimationValue_Uncompute,
                    RawServoDeclarationBlockStrong,
                    RawServoAnimationValueBorrowed value)
 SERVO_BINDING_FUNC(Servo_AnimationValue_Compute,
                    RawServoAnimationValueStrong,
diff --git a/layout/style/StyleAnimationValue.cpp b/layout/style/StyleAnimationValue.cpp
--- a/layout/style/StyleAnimationValue.cpp
+++ b/layout/style/StyleAnimationValue.cpp
@@ -5506,8 +5506,27 @@ AnimationValue::Opacity(StyleBackendType
     case StyleBackendType::Gecko:
       result.mGecko.SetFloatValue(aOpacity);
       break;
     default:
       MOZ_ASSERT_UNREACHABLE("Unsupported style backend");
   }
   return result;
 }
+
+/* static */ AnimationValue
+AnimationValue::Transform(StyleBackendType aBackendType,
+                          nsCSSValueSharedList& aList)
+{
+  AnimationValue result;
+
+  switch (aBackendType) {
+    case StyleBackendType::Servo:
+      result.mServo = Servo_AnimationValue_Transform(aList).Consume();
+      break;
+    case StyleBackendType::Gecko:
+      result.mGecko.SetTransformValue(&aList);
+      break;
+    default:
+      MOZ_ASSERT_UNREACHABLE("Unsupported style backend");
+  }
+  return result;
+}
diff --git a/layout/style/StyleAnimationValue.h b/layout/style/StyleAnimationValue.h
--- a/layout/style/StyleAnimationValue.h
+++ b/layout/style/StyleAnimationValue.h
@@ -633,16 +633,19 @@ struct AnimationValue
   // should use this carefully. Now, it is only used by
   // nsDOMWindowUtils::ComputeAnimationDistance.
   static AnimationValue FromString(nsCSSPropertyID aProperty,
                                    const nsAString& aValue,
                                    dom::Element* aElement);
 
   // Create an AnimationValue from an opacity value.
   static AnimationValue Opacity(StyleBackendType aBackendType, float aOpacity);
+  // Create an AnimationValue from a transform list.
+  static AnimationValue Transform(StyleBackendType aBackendType,
+                                  nsCSSValueSharedList& aList);
 
   // mGecko and mServo are mutually exclusive: only one or the other should
   // ever be set.
   // FIXME: After obsoleting StyleAnimationValue, we should remove mGecko, and
   // make AnimationValue a wrapper of RawServoAnimationValue to hide these
   // FFIs.
   StyleAnimationValue mGecko;
   RefPtr<RawServoAnimationValue> mServo;
