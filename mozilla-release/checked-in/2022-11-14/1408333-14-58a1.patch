# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1508839361 -7200
#      Tue Oct 24 12:02:41 2017 +0200
# Node ID d6e09df5f05e752d3e0e6954c7877ed787ce95cf
# Parent  b165046a4e5ec8cc3936c2b28d669b70c3790963
Bug 1408333 Get rid of nsIIPCBackgroundChildCreateCallback - part 14 - QuotaManager, r=asuth

diff --git a/dom/quota/QuotaManagerService.cpp b/dom/quota/QuotaManagerService.cpp
--- a/dom/quota/QuotaManagerService.cpp
+++ b/dom/quota/QuotaManagerService.cpp
@@ -12,17 +12,16 @@
 #include "mozilla/Hal.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/Unused.h"
 #include "mozilla/ipc/BackgroundChild.h"
 #include "mozilla/ipc/BackgroundParent.h"
 #include "mozilla/ipc/BackgroundUtils.h"
 #include "mozilla/ipc/PBackgroundChild.h"
 #include "nsIIdleService.h"
-#include "nsIIPCBackgroundChildCreateCallback.h"
 #include "nsIObserverService.h"
 #include "nsIScriptSecurityManager.h"
 #include "nsXULAppAPI.h"
 #include "QuotaManager.h"
 #include "QuotaRequests.h"
 
 #define PROFILE_BEFORE_CHANGE_QM_OBSERVER_ID "profile-before-change-qm"
 
@@ -91,38 +90,16 @@ public:
   { }
 
 private:
   NS_DECL_NSIRUNNABLE
 };
 
 } // namespace
 
-class QuotaManagerService::BackgroundCreateCallback final
-  : public nsIIPCBackgroundChildCreateCallback
-{
-  RefPtr<QuotaManagerService> mService;
-
-public:
-  explicit
-  BackgroundCreateCallback(QuotaManagerService* aService)
-    : mService(aService)
-  {
-    MOZ_ASSERT(aService);
-  }
-
-  NS_DECL_ISUPPORTS
-
-private:
-  ~BackgroundCreateCallback()
-  { }
-
-  NS_DECL_NSIIPCBACKGROUNDCHILDCREATECALLBACK
-};
-
 class QuotaManagerService::PendingRequestInfo
 {
 protected:
   RefPtr<RequestBase> mRequest;
 
 public:
   explicit PendingRequestInfo(RequestBase* aRequest)
     : mRequest(aRequest)
@@ -347,107 +324,46 @@ nsresult
 QuotaManagerService::InitiateRequest(nsAutoPtr<PendingRequestInfo>& aInfo)
 {
   // Nothing can be done here if we have previously failed to create a
   // background actor.
   if (mBackgroundActorFailed) {
     return NS_ERROR_FAILURE;
   }
 
-  if (!mBackgroundActor && mPendingRequests.IsEmpty()) {
-    if (PBackgroundChild* actor = BackgroundChild::GetForCurrentThread()) {
-      BackgroundActorCreated(actor);
-    } else {
-      // We need to start the sequence to create a background actor for this
-      // thread.
-      RefPtr<BackgroundCreateCallback> cb = new BackgroundCreateCallback(this);
-      if (NS_WARN_IF(!BackgroundChild::GetOrCreateForCurrentThread(cb))) {
-        return NS_ERROR_FAILURE;
-      }
+  if (!mBackgroundActor) {
+    PBackgroundChild* backgroundActor =
+      BackgroundChild::GetOrCreateForCurrentThread();
+    if (NS_WARN_IF(!backgroundActor)) {
+      mBackgroundActorFailed = true;
+      return NS_ERROR_FAILURE;
+    }
+
+    {
+      QuotaChild* actor = new QuotaChild(this);
+
+      mBackgroundActor =
+        static_cast<QuotaChild*>(backgroundActor->SendPQuotaConstructor(actor));
     }
   }
 
+  if (!mBackgroundActor) {
+    mBackgroundActorFailed = true;
+    return NS_ERROR_FAILURE;
+  }
+
   // If we already have a background actor then we can start this request now.
-  if (mBackgroundActor) {
-    nsresult rv = aInfo->InitiateRequest(mBackgroundActor);
-    if (NS_WARN_IF(NS_FAILED(rv))) {
-      return rv;
-    }
-  } else {
-    mPendingRequests.AppendElement(aInfo.forget());
+  nsresult rv = aInfo->InitiateRequest(mBackgroundActor);
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
   }
 
   return NS_OK;
 }
 
-nsresult
-QuotaManagerService::BackgroundActorCreated(PBackgroundChild* aBackgroundActor)
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  MOZ_ASSERT(aBackgroundActor);
-  MOZ_ASSERT(!mBackgroundActor);
-  MOZ_ASSERT(!mBackgroundActorFailed);
-
-  {
-    QuotaChild* actor = new QuotaChild(this);
-
-    mBackgroundActor =
-      static_cast<QuotaChild*>(aBackgroundActor->SendPQuotaConstructor(actor));
-  }
-
-  if (NS_WARN_IF(!mBackgroundActor)) {
-    BackgroundActorFailed();
-    return NS_ERROR_FAILURE;
-  }
-
-  nsresult rv = NS_OK;
-
-  for (uint32_t index = 0, count = mPendingRequests.Length();
-       index < count;
-       index++) {
-    nsAutoPtr<PendingRequestInfo> info(mPendingRequests[index].forget());
-
-    nsresult rv2 = info->InitiateRequest(mBackgroundActor);
-
-    // Warn for every failure, but just return the first failure if there are
-    // multiple failures.
-    if (NS_WARN_IF(NS_FAILED(rv2)) && NS_SUCCEEDED(rv)) {
-      rv = rv2;
-    }
-  }
-
-  mPendingRequests.Clear();
-
-  return rv;
-}
-
-void
-QuotaManagerService::BackgroundActorFailed()
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  MOZ_ASSERT(!mPendingRequests.IsEmpty());
-  MOZ_ASSERT(!mBackgroundActor);
-  MOZ_ASSERT(!mBackgroundActorFailed);
-
-  mBackgroundActorFailed = true;
-
-  for (uint32_t index = 0, count = mPendingRequests.Length();
-       index < count;
-       index++) {
-    nsAutoPtr<PendingRequestInfo> info(mPendingRequests[index].forget());
-
-    RequestBase* request = info->GetRequest();
-    if (request) {
-      request->SetError(NS_ERROR_FAILURE);
-    }
-  }
-
-  mPendingRequests.Clear();
-}
-
 void
 QuotaManagerService::PerformIdleMaintenance()
 {
   using namespace mozilla::hal;
 
   MOZ_ASSERT(XRE_IsParentProcess());
   MOZ_ASSERT(NS_IsMainThread());
 
@@ -855,46 +771,16 @@ AbortOperationsRunnable::Run()
     return NS_OK;
   }
 
   quotaManager->AbortOperationsForProcess(mContentParentId);
 
   return NS_OK;
 }
 
-NS_IMPL_ISUPPORTS(QuotaManagerService::BackgroundCreateCallback,
-                  nsIIPCBackgroundChildCreateCallback)
-
-void
-QuotaManagerService::
-BackgroundCreateCallback::ActorCreated(PBackgroundChild* aActor)
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  MOZ_ASSERT(aActor);
-  MOZ_ASSERT(mService);
-
-  RefPtr<QuotaManagerService> service;
-  mService.swap(service);
-
-  service->BackgroundActorCreated(aActor);
-}
-
-void
-QuotaManagerService::
-BackgroundCreateCallback::ActorFailed()
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  MOZ_ASSERT(mService);
-
-  RefPtr<QuotaManagerService> service;
-  mService.swap(service);
-
-  service->BackgroundActorFailed();
-}
-
 nsresult
 QuotaManagerService::
 UsageRequestInfo::InitiateRequest(QuotaChild* aActor)
 {
   MOZ_ASSERT(aActor);
 
   auto request = static_cast<UsageRequest*>(mRequest.get());
 
diff --git a/dom/quota/QuotaManagerService.h b/dom/quota/QuotaManagerService.h
--- a/dom/quota/QuotaManagerService.h
+++ b/dom/quota/QuotaManagerService.h
@@ -38,18 +38,16 @@ class QuotaManagerService final
   class BackgroundCreateCallback;
   class PendingRequestInfo;
   class UsageRequestInfo;
   class RequestInfo;
   class IdleMaintenanceInfo;
 
   nsCOMPtr<nsIEventTarget> mBackgroundThread;
 
-  nsTArray<nsAutoPtr<PendingRequestInfo>> mPendingRequests;
-
   QuotaChild* mBackgroundActor;
 
   bool mBackgroundActorFailed;
   bool mIdleObserverRegistered;
 
 public:
   // Returns a non-owning reference.
   static QuotaManagerService*
