# HG changeset patch
# User Lee Salzman <lsalzman@mozilla.com>
# Date 1510017666 18000
# Node ID 8e450204ab2ea252f370020a936c92cc9a90dc7f
# Parent  92c65fe334a59e1d82cd0aaef2b86d44b6c6cf42
Bug 1403198 - support WR font descriptors on Mac. r=jrmuizel

MozReview-Commit-ID: BqN51yY88oJ

diff --git a/Cargo.lock b/Cargo.lock
--- a/Cargo.lock
+++ b/Cargo.lock
@@ -1776,16 +1776,18 @@ dependencies = [
  "time",
 ]
 
 [[package]]
 name = "webrender_bindings"
 version = "0.1.0"
 dependencies = [
  "app_units",
+ "core-foundation",
+ "core-graphics",
  "dwrote",
  "euclid",
  "gleam",
  "log 0.3.9",
  "rayon",
  "thread_profiler",
  "webrender",
  "webrender_api",
diff --git a/gfx/2d/NativeFontResourceMac.cpp b/gfx/2d/NativeFontResourceMac.cpp
--- a/gfx/2d/NativeFontResourceMac.cpp
+++ b/gfx/2d/NativeFontResourceMac.cpp
@@ -48,15 +48,15 @@ NativeFontResourceMac::Create(uint8_t *a
   return fontResource.forget();
 }
 
 already_AddRefed<UnscaledFont>
 NativeFontResourceMac::CreateUnscaledFont(uint32_t aIndex,
                                           const uint8_t* aInstanceData,
                                           uint32_t aInstanceDataLength)
 {
-  RefPtr<UnscaledFont> unscaledFont = new UnscaledFontMac(mFontRef);
+  RefPtr<UnscaledFont> unscaledFont = new UnscaledFontMac(mFontRef, true);
 
   return unscaledFont.forget();
 }
 
 } // gfx
 } // mozilla
diff --git a/gfx/2d/ScaledFontMac.cpp b/gfx/2d/ScaledFontMac.cpp
--- a/gfx/2d/ScaledFontMac.cpp
+++ b/gfx/2d/ScaledFontMac.cpp
@@ -317,16 +317,41 @@ UnscaledFontMac::GetFontFileData(FontFil
     memcpy(&buf.data[checkSumAdjustmentOffset], &fontChecksum, sizeof(fontChecksum));
 
     // we always use an index of 0
     aDataCallback(buf.data, buf.offset, 0, aBaton);
 
     return true;
 }
 
+bool
+UnscaledFontMac::GetWRFontDescriptor(WRFontDescriptorOutput aCb, void* aBaton)
+{
+  if (mIsDataFont) {
+    return false;
+  }
+
+  CFStringRef psname = CGFontCopyPostScriptName(mFont);
+  if (!psname) {
+    return false;
+  }
+
+  char buf[256];
+  const char* cstr = CFStringGetCStringPtr(psname, kCFStringEncodingUTF8);
+  if (!cstr) {
+    if (!CFStringGetCString(psname, buf, sizeof(buf), kCFStringEncodingUTF8)) {
+      return false;
+    }
+    cstr = buf;
+  }
+
+  aCb(reinterpret_cast<const uint8_t*>(cstr), strlen(cstr), 0, aBaton);
+  return true;
+}
+
 static void
 CollectVariationsFromDictionary(const void* aKey, const void* aValue, void* aContext)
 {
   auto keyPtr = static_cast<const CFTypeRef>(aKey);
   auto valuePtr = static_cast<const CFTypeRef>(aValue);
   auto outVariations = static_cast<std::vector<FontVariation>*>(aContext);
   if (CFGetTypeID(keyPtr) == CFNumberGetTypeID() &&
       CFGetTypeID(valuePtr) == CFNumberGetTypeID()) {
diff --git a/gfx/2d/UnscaledFontMac.h b/gfx/2d/UnscaledFontMac.h
--- a/gfx/2d/UnscaledFontMac.h
+++ b/gfx/2d/UnscaledFontMac.h
@@ -18,18 +18,19 @@
 
 namespace mozilla {
 namespace gfx {
 
 class UnscaledFontMac final : public UnscaledFont
 {
 public:
   MOZ_DECLARE_REFCOUNTED_VIRTUAL_TYPENAME(UnscaledFontMac, override)
-  explicit UnscaledFontMac(CGFontRef aFont)
+  explicit UnscaledFontMac(CGFontRef aFont, bool aIsDataFont = false)
     : mFont(aFont)
+    , mIsDataFont(aIsDataFont)
   {
     CFRetain(mFont);
   }
   ~UnscaledFontMac()
   {
     CFRelease(mFont);
   }
 
@@ -46,17 +47,20 @@ public:
                      const FontVariation* aVariations,
                      uint32_t aNumVariations) override;
 
   static CGFontRef
     CreateCGFontWithVariations(CGFontRef aFont,
                                uint32_t aVariationCount,
                                const FontVariation* aVariations);
 
+  bool GetWRFontDescriptor(WRFontDescriptorOutput aCb, void* aBaton) override;
+
 private:
   CGFontRef mFont;
+  bool mIsDataFont;
 };
 
 } // namespace gfx
 } // namespace mozilla
 
 #endif /* MOZILLA_GFX_UNSCALEDFONTMAC_H_ */
 
diff --git a/gfx/thebes/gfxMacPlatformFontList.mm b/gfx/thebes/gfxMacPlatformFontList.mm
--- a/gfx/thebes/gfxMacPlatformFontList.mm
+++ b/gfx/thebes/gfxMacPlatformFontList.mm
@@ -265,17 +265,17 @@ gfxFont*
 MacOSFontEntry::CreateFontInstance(const gfxFontStyle *aFontStyle, bool aNeedsBold)
 {
     RefPtr<UnscaledFontMac> unscaledFont(mUnscaledFont);
     if (!unscaledFont) {
         CGFontRef baseFont = GetFontRef();
         if (!baseFont) {
             return nullptr;
         }
-        unscaledFont = new UnscaledFontMac(baseFont);
+        unscaledFont = new UnscaledFontMac(baseFont, mIsDataUserFont);
         mUnscaledFont = unscaledFont;
     }
 
     return new gfxMacFont(unscaledFont, this, aFontStyle, aNeedsBold);
 }
 
 bool
 MacOSFontEntry::HasVariations()
diff --git a/gfx/webrender_bindings/Cargo.toml b/gfx/webrender_bindings/Cargo.toml
--- a/gfx/webrender_bindings/Cargo.toml
+++ b/gfx/webrender_bindings/Cargo.toml
@@ -16,8 +16,12 @@ log = "0.3"
 [dependencies.webrender]
 path = "../webrender"
 version = "0.53.1"
 default-features = false
 
 [target.'cfg(target_os = "windows")'.dependencies]
 dwrote = "0.4"
 
+[target.'cfg(target_os = "macos")'.dependencies]
+core-foundation = "0.4"
+core-graphics = "0.9"
+
diff --git a/gfx/webrender_bindings/src/bindings.rs b/gfx/webrender_bindings/src/bindings.rs
--- a/gfx/webrender_bindings/src/bindings.rs
+++ b/gfx/webrender_bindings/src/bindings.rs
@@ -20,16 +20,21 @@ use moz2d_renderer::Moz2dImageRenderer;
 use app_units::Au;
 use rayon;
 use euclid::SideOffsets2D;
 use log::{set_logger, shutdown_logger, LogLevelFilter, Log, LogLevel, LogMetadata, LogRecord};
 
 #[cfg(target_os = "windows")]
 use dwrote::{FontDescriptor, FontWeight, FontStretch, FontStyle};
 
+#[cfg(target_os = "macos")]
+use core_foundation::string::CFString;
+#[cfg(target_os = "macos")]
+use core_graphics::font::CGFont;
+
 extern crate webrender_api;
 
 /// cbindgen:field-names=[mNamespace, mHandle]
 type WrExternalImageBufferType = ExternalImageType;
 
 /// cbindgen:field-names=[mHandle]
 /// cbindgen:derive-lt=true
 /// cbindgen:derive-lte=true
@@ -998,16 +1003,27 @@ fn read_font_descriptor(
     FontDescriptor {
         family_name: String::from_utf16(&wchars).unwrap(),
         weight: FontWeight::from_u32(index & 0xffff),
         stretch: FontStretch::from_u32((index >> 16) & 0xff),
         style: FontStyle::from_u32((index >> 24) & 0xff),
     }
 }
 
+#[cfg(target_os = "macos")]
+fn read_font_descriptor(
+    bytes: &mut WrVecU8,
+    _index: u32
+) -> NativeFontHandle {
+    let chars = bytes.flush_into_vec();
+    let name = String::from_utf8(chars).unwrap();
+    let font = CGFont::from_name(&CFString::new(&*name)).unwrap();
+    NativeFontHandle(font)
+}
+
 #[cfg(not(any(target_os = "macos", target_os = "windows")))]
 fn read_font_descriptor(
     bytes: &mut WrVecU8,
     index: u32
 ) -> NativeFontHandle {
     let cstr = CString::new(bytes.flush_into_vec()).unwrap();
     NativeFontHandle {
         pathname: String::from(cstr.to_str().unwrap()),
diff --git a/gfx/webrender_bindings/src/lib.rs b/gfx/webrender_bindings/src/lib.rs
--- a/gfx/webrender_bindings/src/lib.rs
+++ b/gfx/webrender_bindings/src/lib.rs
@@ -11,11 +11,16 @@ extern crate app_units;
 extern crate gleam;
 extern crate rayon;
 extern crate thread_profiler;
 extern crate log;
 
 #[cfg(target_os = "windows")]
 extern crate dwrote;
 
+#[cfg(target_os = "macos")]
+extern crate core_foundation;
+#[cfg(target_os = "macos")]
+extern crate core_graphics;
+
 #[allow(non_snake_case)]
 pub mod bindings;
 pub mod moz2d_renderer;
