# HG changeset patch
# User Tom Prince <mozilla@hocat.ca>
# Date 1514415452 25200
#      Wed Dec 27 15:57:32 2017 -0700
# Node ID 06f53c37de7a20fe90665212ba6236285b22a1ae
# Parent  fd74b7278ba568e209ba83d38d325e9174a7f29c
Bug 1427213: Clean up docstrings in mozpack.path; r=glandium

MozReview-Commit-ID: 91cm9XJROy0

diff --git a/python/mozbuild/mozpack/path.py b/python/mozbuild/mozpack/path.py
--- a/python/mozbuild/mozpack/path.py
+++ b/python/mozbuild/mozpack/path.py
@@ -1,29 +1,29 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+'''
+Like :py:mod:`os.path`, with a reduced set of functions, and with normalized path
+separators (always use forward slashes).
+Also contains a few additional utilities not found in :py:mod:`os.path`.
+'''
+
 from __future__ import absolute_import
 
 import posixpath
 import os
 import re
 
-'''
-Like os.path, with a reduced set of functions, and with normalized path
-separators (always use forward slashes).
-Also contains a few additional utilities not found in os.path.
-'''
-
 
 def normsep(path):
     '''
     Normalize path separators, by using forward slashes instead of whatever
-    os.sep is.
+    :py:const:`os.sep` is.
     '''
     if os.sep != '/':
         path = path.replace(os.sep, '/')
     if os.altsep and os.altsep != '/':
         path = path.replace(os.altsep, '/')
     return path
 
 
@@ -62,68 +62,78 @@ def basename(path):
 
 def splitext(path):
     return posixpath.splitext(normsep(path))
 
 
 def split(path):
     '''
     Return the normalized path as a list of its components.
-        split('foo/bar/baz') returns ['foo', 'bar', 'baz']
+
+        ``split('foo/bar/baz')`` returns ``['foo', 'bar', 'baz']``
     '''
     return normsep(path).split('/')
 
 
 def basedir(path, bases):
     '''
-    Given a list of directories (bases), return which one contains the given
+    Given a list of directories (`bases`), return which one contains the given
     path. If several matches are found, the deepest base directory is returned.
-        basedir('foo/bar/baz', ['foo', 'baz', 'foo/bar']) returns 'foo/bar'
-        ('foo' and 'foo/bar' both match, but 'foo/bar' is the deepest match)
+
+        ``basedir('foo/bar/baz', ['foo', 'baz', 'foo/bar'])`` returns ``'foo/bar'``
+        (`'foo'` and `'foo/bar'` both match, but `'foo/bar'` is the deepest match)
     '''
     path = normsep(path)
     bases = [normsep(b) for b in bases]
     if path in bases:
         return path
     for b in sorted(bases, reverse=True):
         if b == '' or path.startswith(b + '/'):
             return b
 
 
 re_cache = {}
 
+
 def match(path, pattern):
     '''
     Return whether the given path matches the given pattern.
     An asterisk can be used to match any string, including the null string, in
     one part of the path:
-        'foo' matches '*', 'f*' or 'fo*o'
+
+        ``foo`` matches ``*``, ``f*`` or ``fo*o``
+
     However, an asterisk matching a subdirectory may not match the null string:
-        'foo/bar' does *not* match 'foo/*/bar'
+
+        ``foo/bar`` does *not* match ``foo/*/bar``
+
     If the pattern matches one of the ancestor directories of the path, the
     patch is considered matching:
-        'foo/bar' matches 'foo'
+
+        ``foo/bar`` matches ``foo``
+
     Two adjacent asterisks can be used to match files and zero or more
     directories and subdirectories.
-        'foo/bar' matches 'foo/**/bar', or '**/bar'
+
+        ``foo/bar`` matches ``foo/**/bar``, or ``**/bar``
     '''
     if not pattern:
         return True
     if pattern not in re_cache:
         p = re.escape(pattern)
         p = re.sub(r'(^|\\\/)\\\*\\\*\\\/', r'\1(?:.+/)?', p)
         p = re.sub(r'(^|\\\/)\\\*\\\*$', r'(?:\1.+)?', p)
         p = p.replace(r'\*', '[^/]*') + '(?:/.*)?$'
         re_cache[pattern] = re.compile(p)
     return re_cache[pattern].match(path) is not None
 
 
 def rebase(oldbase, base, relativepath):
     '''
-    Return relativepath relative to base instead of oldbase.
+    Return `relativepath` relative to `base` instead of `oldbase`.
     '''
     if base == oldbase:
         return relativepath
     if len(base) < len(oldbase):
         assert basedir(oldbase, [base]) == base
         relbase = relpath(oldbase, base)
         result = join(relbase, relativepath)
     else:
diff --git a/tools/lint/flake8.yml b/tools/lint/flake8.yml
--- a/tools/lint/flake8.yml
+++ b/tools/lint/flake8.yml
@@ -6,16 +6,17 @@ flake8:
         - build/*.py
         - configure.py
         - config/check_macroassembler_style.py
         - config/mozunit.py
         - layout/tools/reftest
         - python/mach
         - python/mach_commands.py
         - python/mozboot
+        - python/mozbuild/mozpack/path.py
         - python/mozlint
         - python/mozterm
         - python/mozversioncontrol
         - security/manager
         - taskcluster
         - testing/firefox-ui
         - testing/mach_commands.py
         - testing/marionette/client
