# HG changeset patch
# User Ryan Hunt <rhunt@eqrion.net>
# Date 1508352414 14400
#      Wed Oct 18 14:46:54 2017 -0400
# Node ID 8881c53c3c4170ae32cadb90e03a46c1fbf679b7
# Parent  33e42c8b68ffbd82815fffbdbe493fd638adffa1
Fix PrepareDrawTargetForPainting (bug 1409871 part 16, r=dvander)

CapturedPaintState::mTarget is actually a dual draw target to the black and
white draw targets, so this code will have the white draw target cleared black
and then white. This isn't incorrect, it's just wasteful.

MozReview-Commit-ID: ItgiSmegPK6

diff --git a/gfx/layers/PaintThread.cpp b/gfx/layers/PaintThread.cpp
--- a/gfx/layers/PaintThread.cpp
+++ b/gfx/layers/PaintThread.cpp
@@ -29,17 +29,17 @@ StaticRefPtr<nsIThread> PaintThread::sTh
 PlatformThreadId PaintThread::sThreadId;
 
 // RAII make sure we clean up and restore our draw targets
 // when we paint async.
 struct MOZ_STACK_CLASS AutoCapturedPaintSetup
 {
   AutoCapturedPaintSetup(CapturedPaintState* aState, CompositorBridgeChild* aBridge)
   : mState(aState)
-  , mTarget(aState->mTarget)
+  , mTarget(aState->mTargetDual)
   , mRestorePermitsSubpixelAA(mTarget->GetPermitSubpixelAA())
   , mOldTransform(mTarget->GetTransform())
   , mBridge(aBridge)
   {
     mTarget->SetTransform(aState->mCapture->GetTransform());
     mTarget->SetPermitSubpixelAA(aState->mCapture->GetPermitSubpixelAA());
   }
 
@@ -212,17 +212,17 @@ PaintThread::PaintContents(CapturedPaint
 void
 PaintThread::AsyncPaintContents(CompositorBridgeChild* aBridge,
                                 CapturedPaintState* aState,
                                 PrepDrawTargetForPaintingCallback aCallback)
 {
   MOZ_ASSERT(IsOnPaintThread());
   MOZ_ASSERT(aState);
 
-  DrawTarget* target = aState->mTarget;
+  DrawTarget* target = aState->mTargetDual;
   DrawTargetCapture* capture = aState->mCapture;
 
   AutoCapturedPaintSetup setup(aState, aBridge);
 
   if (!aCallback(aState)) {
     return;
   }
 
diff --git a/gfx/layers/PaintThread.h b/gfx/layers/PaintThread.h
--- a/gfx/layers/PaintThread.h
+++ b/gfx/layers/PaintThread.h
@@ -23,33 +23,36 @@ class DrawTargetCapture;
 namespace layers {
 
 // Holds the key parts from a RotatedBuffer::PaintState
 // required to draw the captured paint state
 class CapturedPaintState {
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(CapturedPaintState)
 public:
   CapturedPaintState(nsIntRegion& aRegionToDraw,
+                     gfx::DrawTarget* aTargetDual,
                      gfx::DrawTarget* aTarget,
                      gfx::DrawTarget* aTargetOnWhite,
                      const gfx::Matrix& aTargetTransform,
                      SurfaceMode aSurfaceMode,
                      gfxContentType aContentType)
   : mRegionToDraw(aRegionToDraw)
+  , mTargetDual(aTargetDual)
   , mTarget(aTarget)
   , mTargetOnWhite(aTargetOnWhite)
   , mTargetTransform(aTargetTransform)
   , mSurfaceMode(aSurfaceMode)
   , mContentType(aContentType)
   {}
 
   nsIntRegion mRegionToDraw;
   RefPtr<TextureClient> mTextureClient;
   RefPtr<TextureClient> mTextureClientOnWhite;
   RefPtr<gfx::DrawTargetCapture> mCapture;
+  RefPtr<gfx::DrawTarget> mTargetDual;
   RefPtr<gfx::DrawTarget> mTarget;
   RefPtr<gfx::DrawTarget> mTargetOnWhite;
   gfx::Matrix mTargetTransform;
   SurfaceMode mSurfaceMode;
   gfxContentType mContentType;
 
 protected:
   virtual ~CapturedPaintState() {}
diff --git a/gfx/layers/client/ClientPaintedLayer.cpp b/gfx/layers/client/ClientPaintedLayer.cpp
--- a/gfx/layers/client/ClientPaintedLayer.cpp
+++ b/gfx/layers/client/ClientPaintedLayer.cpp
@@ -220,17 +220,17 @@ ClientPaintedLayer::PaintOffMainThread()
 
   bool didUpdate = false;
   RotatedBuffer::DrawIterator iter;
 
   // Debug Protip: Change to BorrowDrawTargetForPainting if using sync OMTP.
   while (RefPtr<CapturedPaintState> captureState =
           mContentClient->BorrowDrawTargetForRecording(state, &iter))
   {
-    DrawTarget* target = captureState->mTarget;
+    DrawTarget* target = captureState->mTargetDual;
     if (!target || !target->IsValid()) {
       if (target) {
         mContentClient->ReturnDrawTargetToBuffer(target);
       }
       continue;
     }
 
     RefPtr<DrawTargetCapture> captureDT =
diff --git a/gfx/layers/client/ContentClient.cpp b/gfx/layers/client/ContentClient.cpp
--- a/gfx/layers/client/ContentClient.cpp
+++ b/gfx/layers/client/ContentClient.cpp
@@ -249,17 +249,17 @@ ContentClient::BorrowDrawTargetForPainti
   if (!capturedState) {
     return nullptr;
   }
 
   if (!ContentClient::PrepareDrawTargetForPainting(capturedState)) {
     return nullptr;
   }
 
-  return capturedState->mTarget;
+  return capturedState->mTargetDual;
 }
 
 RefPtr<CapturedPaintState>
 ContentClient::BorrowDrawTargetForRecording(ContentClient::PaintState& aPaintState,
                                             RotatedBuffer::DrawIterator* aIter,
                                             bool aSetTransform)
 {
   if (aPaintState.mMode == SurfaceMode::SURFACE_NONE || !mBuffer) {
@@ -277,16 +277,17 @@ ContentClient::BorrowDrawTargetForRecord
   }
 
   nsIntRegion regionToDraw =
     ExpandDrawRegion(aPaintState, aIter, result->GetBackendType());
 
   RefPtr<CapturedPaintState> state =
     new CapturedPaintState(regionToDraw,
                            result,
+                           mBuffer->GetDTBuffer(),
                            mBuffer->GetDTBufferOnWhite(),
                            transform,
                            aPaintState.mMode,
                            aPaintState.mContentType);
   return state;
 }
 
 void
