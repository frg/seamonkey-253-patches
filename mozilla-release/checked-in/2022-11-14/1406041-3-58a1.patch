# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1507741317 -7200
# Node ID 1863f88528cd8a28af29083583f79b63199644d4
# Parent  4fc4e67d5ffc10a68669c442edb244677b6eb51e
Bug 1406041: Add a fast-path for wasm code segment lookup; r=luke

MozReview-Commit-ID: L6LXvOOaNKs

diff --git a/js/src/jit-test/tests/basic/testTypedArrayInit.js b/js/src/jit-test/tests/basic/testTypedArrayInit.js
--- a/js/src/jit-test/tests/basic/testTypedArrayInit.js
+++ b/js/src/jit-test/tests/basic/testTypedArrayInit.js
@@ -6,19 +6,17 @@ function f() {
                      Uint8ClampedArray,
                      Int16Array,
                      Uint16Array,
                      Int32Array,
                      Uint32Array,
                      Float32Array,
                      Float64Array ])
   {
-    for (var len of [ 3, 30, 300, 3000 ]) {
-    // TODO: disabled before follow-up in bug 1406041
-    //for (var len of [ 3, 30, 300, 3000, 30000 ]) {
+    for (var len of [ 3, 30, 300, 3000, 30000 ]) {
       var arr = new ctor(len);
       for (var i = 0; i < arr.length; i++)
         assertEq(arr[i], 0, "index " + i + " of " + ctor.name + " len " + len);
     }
   }
 }
 
 f();
diff --git a/js/src/jit/arm/Simulator-arm.cpp b/js/src/jit/arm/Simulator-arm.cpp
--- a/js/src/jit/arm/Simulator-arm.cpp
+++ b/js/src/jit/arm/Simulator-arm.cpp
@@ -1560,16 +1560,19 @@ Simulator::startWasmInterrupt(JitActivat
 // The signal handler only redirects the PC to the interrupt stub when the PC is
 // in function code. However, this guard is racy for the ARM simulator since the
 // signal handler samples PC in the middle of simulating an instruction and thus
 // the current PC may have advanced once since the signal handler's guard. So we
 // re-check here.
 void
 Simulator::handleWasmInterrupt()
 {
+    if (!wasm::CodeExists)
+        return;
+
     uint8_t* pc = (uint8_t*)get_pc();
     uint8_t* fp = (uint8_t*)get_register(r11);
 
     const wasm::CodeSegment* cs = nullptr;
     if (!wasm::InInterruptibleCode(cx_, pc, &cs))
         return;
 
     // fp can be null during the prologue/epilogue of the entry function.
@@ -1584,16 +1587,18 @@ Simulator::handleWasmInterrupt()
 // WasmArrayRawBuffer comment). The guard pages catch out-of-bounds accesses
 // using a signal handler that redirects PC to a stub that safely reports an
 // error. However, if the handler is hit by the simulator, the PC is in C++ code
 // and cannot be redirected. Therefore, we must avoid hitting the handler by
 // redirecting in the simulator before the real handler would have been hit.
 bool
 Simulator::handleWasmFault(int32_t addr, unsigned numBytes)
 {
+    if (!wasm::CodeExists)
+        return false;
     if (!cx_->activation() || !cx_->activation()->isJit())
         return false;
     JitActivation* act = cx_->activation()->asJit();
 
     void* pc = reinterpret_cast<void*>(get_pc());
     uint8_t* fp = reinterpret_cast<uint8_t*>(get_register(r11));
 
     const wasm::CodeSegment* segment = wasm::LookupCodeSegment(pc);
diff --git a/js/src/jit/mips32/Simulator-mips32.cpp b/js/src/jit/mips32/Simulator-mips32.cpp
--- a/js/src/jit/mips32/Simulator-mips32.cpp
+++ b/js/src/jit/mips32/Simulator-mips32.cpp
@@ -1632,16 +1632,19 @@ Simulator::startInterrupt(JitActivation*
 // The signal handler only redirects the PC to the interrupt stub when the PC is
 // in function code. However, this guard is racy for the simulator since the
 // signal handler samples PC in the middle of simulating an instruction and thus
 // the current PC may have advanced once since the signal handler's guard. So we
 // re-check here.
 void
 Simulator::handleWasmInterrupt()
 {
+    if (!wasm::CodeExists)
+        return;
+
     void* pc = (void*)get_pc();
     void* fp = (void*)getRegister(Register::fp);
 
     JitActivation* activation = TlsContext.get()->activation()->asJit();
     const wasm::CodeSegment* segment = wasm::LookupCodeSegment(pc);
     if (!segment || !segment->containsCodePC(pc))
         return;
 
@@ -1658,16 +1661,19 @@ Simulator::handleWasmInterrupt()
 // WasmArrayRawBuffer comment). The guard pages catch out-of-bounds accesses
 // using a signal handler that redirects PC to a stub that safely reports an
 // error. However, if the handler is hit by the simulator, the PC is in C++ code
 // and cannot be redirected. Therefore, we must avoid hitting the handler by
 // redirecting in the simulator before the real handler would have been hit.
 bool
 Simulator::handleWasmFault(int32_t addr, unsigned numBytes)
 {
+    if (!wasm::CodeExists)
+        return false;
+
     JSContext* cx = TlsContext.get();
     if (!cx->activation() || !cx->activation()->isJit())
         return false;
     JitActivation* act = cx->activation()->asJit();
 
     void* pc = reinterpret_cast<void*>(get_pc());
     uint8_t* fp = reinterpret_cast<uint8_t*>(getRegister(Register::fp));
 
diff --git a/js/src/wasm/WasmProcess.cpp b/js/src/wasm/WasmProcess.cpp
--- a/js/src/wasm/WasmProcess.cpp
+++ b/js/src/wasm/WasmProcess.cpp
@@ -33,16 +33,18 @@ using mozilla::BinarySearchIf;
 // Whenever a new CodeSegment is ready to use, it has to be registered so that
 // we can have fast lookups from pc to CodeSegments in numerous places. Since
 // wasm compilation may be tiered, and the second tier doesn't have access to
 // any JSContext/JSCompartment/etc lying around, we have to use a process-wide
 // map instead.
 
 typedef Vector<const CodeSegment*, 0, SystemAllocPolicy> CodeSegmentVector;
 
+Atomic<bool> wasm::CodeExists(false);
+
 class ProcessCodeSegmentMap
 {
     // Since writes (insertions or removals) can happen on any background
     // thread at the same time, we need a lock here.
 
     Mutex mutatorsMutex_;
 
     CodeSegmentVector segments1_;
@@ -129,16 +131,18 @@ class ProcessCodeSegmentMap
 
         size_t index;
         MOZ_ALWAYS_FALSE(BinarySearchIf(*mutableCodeSegments_, 0, mutableCodeSegments_->length(),
                                         CodeSegmentPC(cs->base()), &index));
 
         if (!mutableCodeSegments_->insert(mutableCodeSegments_->begin() + index, cs))
             return false;
 
+        CodeExists = true;
+
         swapAndWait();
 
 #ifdef DEBUG
         size_t otherIndex;
         MOZ_ALWAYS_FALSE(BinarySearchIf(*mutableCodeSegments_, 0, mutableCodeSegments_->length(),
                                         CodeSegmentPC(cs->base()), &otherIndex));
         MOZ_ASSERT(index == otherIndex);
 #endif
@@ -158,16 +162,19 @@ class ProcessCodeSegmentMap
         LockGuard<Mutex> lock(mutatorsMutex_);
 
         size_t index;
         MOZ_ALWAYS_TRUE(BinarySearchIf(*mutableCodeSegments_, 0, mutableCodeSegments_->length(),
                                        CodeSegmentPC(cs->base()), &index));
 
         mutableCodeSegments_->erase(mutableCodeSegments_->begin() + index);
 
+        if (!mutableCodeSegments_->length())
+            CodeExists = false;
+
         swapAndWait();
 
 #ifdef DEBUG
         size_t otherIndex;
         MOZ_ALWAYS_TRUE(BinarySearchIf(*mutableCodeSegments_, 0, mutableCodeSegments_->length(),
                                        CodeSegmentPC(cs->base()), &otherIndex));
         MOZ_ASSERT(index == otherIndex);
 #endif
diff --git a/js/src/wasm/WasmProcess.h b/js/src/wasm/WasmProcess.h
--- a/js/src/wasm/WasmProcess.h
+++ b/js/src/wasm/WasmProcess.h
@@ -14,32 +14,39 @@
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
 
 #ifndef wasm_process_h
 #define wasm_process_h
 
+#include "mozilla/Atomics.h"
+
 namespace js {
 namespace wasm {
 
 class CodeSegment;
 class Code;
 
 // These methods return the wasm::CodeSegment (resp. wasm::Code) containing
 // the given pc, if any exist in the process. These methods do not take a lock,
 // and thus are safe to use in a profiling or async interrupt context.
 
 const CodeSegment*
 LookupCodeSegment(const void* pc);
 
 const Code*
 LookupCode(const void* pc);
 
+// A bool member that can be used as a very fast lookup to know if there is any
+// code segment at all.
+
+extern mozilla::Atomic<bool> CodeExists;
+
 // These methods allow to (un)register CodeSegments so they can be looked up
 // via pc in the methods described above.
 
 bool
 RegisterCodeSegment(const CodeSegment* cs);
 
 void
 UnregisterCodeSegment(const CodeSegment* cs);
