# HG changeset patch
# User Adam Gashlin <agashlin@mozilla.com>
# Date 1508449194 25200
#      Thu Oct 19 14:39:54 2017 -0700
# Node ID f5219bf056ec6d15f9b59e8c3848db873439cdca
# Parent  856de727bd02b04e30ae43e8902fb13c5b72bb4b
Bug 1401400 - Part 2. Dump all threads for "hang" type crashes. r=gsvelto

MozReview-Commit-ID: 9A3lmZ0kWFY

diff --git a/toolkit/components/crashes/CrashService.js b/toolkit/components/crashes/CrashService.js
--- a/toolkit/components/crashes/CrashService.js
+++ b/toolkit/components/crashes/CrashService.js
@@ -18,21 +18,23 @@ var gQuitting = false;
 // Tracks all the running instances of the minidump-analyzer
 var gRunningProcesses = new Set();
 
 /**
  * Run the minidump analyzer tool to gather stack traces from the minidump. The
  * stack traces will be stored in the .extra file under the StackTraces= entry.
  *
  * @param minidumpPath {string} The path to the minidump file
+ * @param allThreads {bool} Gather stack traces for all threads, not just the
+ *                   crashing thread.
  *
  * @returns {Promise} A promise that gets resolved once minidump analysis has
  *          finished.
  */
-function runMinidumpAnalyzer(minidumpPath) {
+function runMinidumpAnalyzer(minidumpPath, allThreads) {
   return new Promise((resolve, reject) => {
     try {
       const binSuffix = AppConstants.platform === "win" ? ".exe" : "";
       const exeName = "minidump-analyzer" + binSuffix;
 
       let exe = Services.dirsvc.get("GreBinD", Ci.nsIFile);
 
       if (AppConstants.platform === "macosx") {
@@ -45,16 +47,20 @@ function runMinidumpAnalyzer(minidumpPat
 
       let args = [ minidumpPath ];
       let process = Cc["@mozilla.org/process/util;1"]
                       .createInstance(Ci.nsIProcess);
       process.init(exe);
       process.startHidden = true;
       process.noShell = true;
 
+      if (allThreads) {
+        args.unshift("--full");
+      }
+
       process.runAsync(args, args.length, (subject, topic, data) => {
         switch (topic) {
           case "process-finished":
             gRunningProcesses.delete(process);
             resolve();
             break;
           default:
             reject(new Error("Unexpected topic received " + topic));
@@ -157,38 +163,41 @@ CrashService.prototype = Object.freeze({
       break;
     case Ci.nsICrashService.PROCESS_TYPE_GPU:
       processType = Services.crashmanager.PROCESS_TYPE_GPU;
       break;
     default:
       throw new Error("Unrecognized PROCESS_TYPE: " + processType);
     }
 
+    let allThreads = false;
+
     switch (crashType) {
     case Ci.nsICrashService.CRASH_TYPE_CRASH:
       crashType = Services.crashmanager.CRASH_TYPE_CRASH;
       break;
     case Ci.nsICrashService.CRASH_TYPE_HANG:
       crashType = Services.crashmanager.CRASH_TYPE_HANG;
+      allThreads = true;
       break;
     default:
       throw new Error("Unrecognized CRASH_TYPE: " + crashType);
     }
 
     let cr = Cc["@mozilla.org/toolkit/crash-reporter;1"]
                .getService(Components.interfaces.nsICrashReporter);
     let minidumpPath = cr.getMinidumpForID(id).path;
     let extraPath = cr.getExtraFileForID(id).path;
     let metadata = {};
     let hash = null;
 
     if (!gQuitting) {
       // Minidump analysis can take a long time, don't start it if the browser
       // is already quitting.
-      await runMinidumpAnalyzer(minidumpPath);
+      await runMinidumpAnalyzer(minidumpPath, allThreads);
     }
 
     metadata = await processExtraFile(extraPath);
     hash = await computeMinidumpHash(minidumpPath);
 
     if (hash) {
       metadata.MinidumpSha256Hash = hash;
     }
