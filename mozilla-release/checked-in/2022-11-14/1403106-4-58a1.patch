# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1507116764 -7200
# Node ID d7561fda2d1322d7331a98b2814fdfda56a58e8a
# Parent  11b9b1201f8cd786df00c674dbbb2ab588d7faa8
Bug 1403106 - Fix console launchpad for ObjectClient; r=Honza

This removes the fixtures for ObjectClient since it's now in its
own file, and add one for the DebuggerClient, because it's still
using non-web dependency (e.g. `Cu`).

MozReview-Commit-ID: 6OJM2HPwvOY

diff --git a/devtools/client/webconsole/new-console-output/test/fixtures/DebuggerClient.js b/devtools/client/webconsole/new-console-output/test/fixtures/DebuggerClient.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/webconsole/new-console-output/test/fixtures/DebuggerClient.js
@@ -0,0 +1,88 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+"use strict";
+
+// Objects and functions were cherry-picked from devtools/shared/client/main.js, in
+// order to make the console launchpad Worker.
+
+// mock, we only need DebuggerClient.requester
+const DebuggerClient = function (transport) {};
+
+/**
+ * A declarative helper for defining methods that send requests to the server.
+ *
+ * @param packetSkeleton
+ *        The form of the packet to send. Can specify fields to be filled from
+ *        the parameters by using the |arg| function.
+ * @param before
+ *        The function to call before sending the packet. Is passed the packet,
+ *        and the return value is used as the new packet. The |this| context is
+ *        the instance of the client object we are defining a method for.
+ * @param after
+ *        The function to call after the response is received. It is passed the
+ *        response, and the return value is considered the new response that
+ *        will be passed to the callback. The |this| context is the instance of
+ *        the client object we are defining a method for.
+ * @return Request
+ *         The `Request` object that is a Promise object and resolves once
+ *         we receive the response. (See request method for more details)
+ */
+DebuggerClient.requester = function (packetSkeleton, config = {}) {
+  let { before, after } = config;
+  return function (...args) {
+    let outgoingPacket = {
+      to: packetSkeleton.to || this.actor
+    };
+
+    let maxPosition = -1;
+    for (let k of Object.keys(packetSkeleton)) {
+      if (packetSkeleton[k] instanceof DebuggerClient.Argument) {
+        let { position } = packetSkeleton[k];
+        outgoingPacket[k] = packetSkeleton[k].getArgument(args);
+        maxPosition = Math.max(position, maxPosition);
+      } else {
+        outgoingPacket[k] = packetSkeleton[k];
+      }
+    }
+
+    if (before) {
+      outgoingPacket = before.call(this, outgoingPacket);
+    }
+
+    return this.request(outgoingPacket, (response) => {
+      if (after) {
+        let { from } = response;
+        response = after.call(this, response);
+        if (!response.from) {
+          response.from = from;
+        }
+      }
+
+      // The callback is always the last parameter.
+      let thisCallback = args[maxPosition + 1];
+      if (thisCallback) {
+        thisCallback(response);
+      }
+      return response;
+    }, "DebuggerClient.requester request callback");
+  };
+};
+
+function arg(pos) {
+  return new DebuggerClient.Argument(pos);
+}
+
+DebuggerClient.Argument = function (position) {
+  this.position = position;
+};
+
+DebuggerClient.Argument.prototype.getArgument = function (params) {
+  if (!(this.position in params)) {
+    throw new Error("Bad index into params: " + this.position);
+  }
+  return params[this.position];
+};
+
+module.exports = { arg, DebuggerClient };
diff --git a/devtools/client/webconsole/new-console-output/test/fixtures/ObjectClient.js b/devtools/client/webconsole/new-console-output/test/fixtures/ObjectClient.js
deleted file mode 100644
--- a/devtools/client/webconsole/new-console-output/test/fixtures/ObjectClient.js
+++ /dev/null
@@ -1,21 +0,0 @@
-/* Any copyright is dedicated to the Public Domain.
-   http://creativecommons.org/publicdomain/zero/1.0/ */
-
-"use strict";
-
-class ObjectClient {
-  constructor(client, grip) {
-    this._grip = grip;
-    this._client = client;
-    this.request = this._client.request;
-  }
-
-  getPrototypeAndProperties() {
-    return this._client.request({
-      to: this._grip.actor,
-      type: "prototypeAndProperties"
-    });
-  }
-}
-
-module.exports = { ObjectClient };
diff --git a/devtools/client/webconsole/webpack.config.js b/devtools/client/webconsole/webpack.config.js
--- a/devtools/client/webconsole/webpack.config.js
+++ b/devtools/client/webconsole/webpack.config.js
@@ -21,27 +21,33 @@ let webpackConfig = {
 
   module: {
     rules: [
       {
         test: /\.(png|svg)$/,
         loader: "file-loader?name=[path][name].[ext]",
       },
       {
-        /*
-         * The version of webpack used in the launchpad seems to have trouble
-         * with the require("raw!${file}") that we use for the properties
-         * file in l10.js.
-         * This loader goes through the whole code and remove the "raw!" prefix
-         * so the raw-loader declared in devtools-launchpad config can load
-         * those files.
-         */
-        test: /\.js/,
-        loader: "rewrite-raw",
-      },
+        test: /\.js$/,
+        loaders: [
+          /*
+          * The version of webpack used in the launchpad seems to have trouble
+          * with the require("raw!${file}") that we use for the properties
+          * file in l10.js.
+          * This loader goes through the whole code and remove the "raw!" prefix
+          * so the raw-loader declared in devtools-launchpad config can load
+          * those files.
+          */
+          "rewrite-raw",
+          // Replace all references to this.browserRequire() by require()
+          "rewrite-browser-require",
+          // Replace all references to loader.lazyRequire() by require()
+          "rewrite-lazy-require",
+        ],
+      }
     ]
   },
 
   resolveLoader: {
     modules: [
       path.resolve("./node_modules"),
       path.resolve("../shared/webpack"),
     ]
@@ -115,17 +121,17 @@ webpackConfig.resolve = {
     "devtools/client/shared/keycodes": path.join(__dirname, "../../client/shared/keycodes"),
     "devtools/client/shared/zoom-keys": "devtools-modules/src/zoom-keys",
     "devtools/client/shared/curl": path.join(__dirname, "../../client/shared/curl"),
     "devtools/client/shared/scroll": path.join(__dirname, "../../client/shared/scroll"),
 
     "devtools/shared/fronts/timeline": path.join(__dirname, "../../shared/shims/fronts/timeline"),
     "devtools/shared/defer": path.join(__dirname, "../../shared/defer"),
     "devtools/shared/old-event-emitter": "devtools-modules/src/utils/event-emitter",
-    "devtools/shared/client/object-client": path.join(__dirname, "new-console-output/test/fixtures/ObjectClient"),
+    "devtools/shared/client/debugger-client": path.join(__dirname, "new-console-output/test/fixtures/DebuggerClient"),
     "devtools/shared/platform/clipboard": path.join(__dirname, "../../shared/platform/content/clipboard"),
     "devtools/shared/platform/stack": path.join(__dirname, "../../shared/platform/content/stack"),
 
     "devtools/client/netmonitor/src/utils/request-utils": path.join(__dirname, "../netmonitor/src/utils/request-utils"),
     "devtools/client/netmonitor/src/components/tabbox-panel": path.join(__dirname, "../netmonitor/src/components/tabbox-panel"),
     "devtools/client/netmonitor/src/connector/firefox-data-provider": path.join(__dirname, "../netmonitor/src/connector/firefox-data-provider"),
     "devtools/client/netmonitor/src/constants": path.join(__dirname, "../netmonitor/src/constants"),
   }
