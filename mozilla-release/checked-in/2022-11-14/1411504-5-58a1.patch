# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1508810423 -28800
#      Tue Oct 24 10:00:23 2017 +0800
# Node ID efd62ef842c129d85e70a159be5d0eea94807cdb
# Parent  96076914ca966a0f2820e00901ba9c92391eaa04
Bug 1411504. P5 - handle CacheClientNotifySuspendedStatusChanged/QueueSuspendedStatusUpdate off the main thread. r=gerald

MozReview-Commit-ID: 7Wc1tvd3S6x

diff --git a/dom/media/ChannelMediaResource.cpp b/dom/media/ChannelMediaResource.cpp
--- a/dom/media/ChannelMediaResource.cpp
+++ b/dom/media/ChannelMediaResource.cpp
@@ -847,18 +847,21 @@ ChannelMediaResource::UpdatePrincipal()
     secMan->GetChannelResultPrincipal(mChannel, getter_AddRefs(principal));
     mCacheStream.UpdatePrincipal(principal);
   }
 }
 
 void
 ChannelMediaResource::CacheClientNotifySuspendedStatusChanged()
 {
-  NS_ASSERTION(NS_IsMainThread(), "Don't call on non-main thread");
-  mCallback->NotifySuspendedStatusChanged(IsSuspendedByCache());
+  mCallback->AbstractMainThread()->Dispatch(NewRunnableMethod<bool>(
+    "MediaResourceCallback::NotifySuspendedStatusChanged",
+    mCallback.get(),
+    &MediaResourceCallback::NotifySuspendedStatusChanged,
+    IsSuspendedByCache()));
 }
 
 nsresult
 ChannelMediaResource::Seek(int64_t aOffset, bool aResume)
 {
   MOZ_ASSERT(NS_IsMainThread());
   LOG("Seek requested for aOffset [%" PRId64 "]", aOffset);
 
diff --git a/dom/media/MediaCache.cpp b/dom/media/MediaCache.cpp
--- a/dom/media/MediaCache.cpp
+++ b/dom/media/MediaCache.cpp
@@ -1567,17 +1567,17 @@ MediaCache::QueueUpdate()
   // don't leak a runnable in that case.
   nsCOMPtr<nsIRunnable> event = new UpdateEvent(this);
   SystemGroup::Dispatch(TaskCategory::Other, event.forget());
 }
 
 void
 MediaCache::QueueSuspendedStatusUpdate(int64_t aResourceID)
 {
-  NS_ASSERTION(NS_IsMainThread(), "Only call on main thread");
+  mReentrantMonitor.AssertCurrentThreadIn();
   if (!mSuspendedStatusToNotify.Contains(aResourceID)) {
     mSuspendedStatusToNotify.AppendElement(aResourceID);
   }
 }
 
 #ifdef DEBUG_VERIFY_CACHE
 void
 MediaCache::Verify()
@@ -2184,22 +2184,23 @@ void
 MediaCacheStream::Close()
 {
   NS_ASSERTION(NS_IsMainThread(), "Only call on main thread");
 
   if (!mMediaCache || mClosed) {
     return;
   }
 
+  ReentrantMonitorAutoEnter mon(mMediaCache->GetReentrantMonitor());
+
   // Closing a stream will change the return value of
   // MediaCacheStream::AreAllStreamsForResourceSuspended as well as
   // ChannelMediaResource::IsSuspendedByCache. Let's notify it.
   mMediaCache->QueueSuspendedStatusUpdate(mResourceID);
 
-  ReentrantMonitorAutoEnter mon(mMediaCache->GetReentrantMonitor());
   mClosed = true;
   mMediaCache->ReleaseStreamBlocks(this);
   // Wake up any blocked readers
   mon.NotifyAll();
 
   // Queue an Update since we may have created more free space. Don't do
   // it from CloseInternal since that gets called by Update() itself
   // sometimes, and we try to not to queue updates from Update().
