# HG changeset patch
# User Kyle Machulis <kyle@nonpolynomial.com>
# Date 1550671925 0
# Node ID bd5fbc5d0466775347935f0fd74858d5fb157299
# Parent  92d3b1649b377b0093a11c634420e7cf242dbb3f
Bug 1529164 - Remove FakePlugin frame loading; r=nika

Remove the ability for fake plugins to create frames. Fake plugins
aren't used anymore, so we can simplify nsFrameLoader a bit by
removing some of the related checks.

Differential Revision: https://phabricator.services.mozilla.com/D20430

diff --git a/dom/base/nsFrameLoader.cpp b/dom/base/nsFrameLoader.cpp
--- a/dom/base/nsFrameLoader.cpp
+++ b/dom/base/nsFrameLoader.cpp
@@ -156,23 +156,22 @@ NS_IMPL_CYCLE_COLLECTING_RELEASE(nsFrame
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(nsFrameLoader)
   NS_INTERFACE_MAP_ENTRY(nsIFrameLoader)
   NS_WRAPPERCACHE_INTERFACE_MAP_ENTRY
   NS_INTERFACE_MAP_ENTRY_AMBIGUOUS(nsISupports, nsIFrameLoader)
   NS_INTERFACE_MAP_ENTRY(nsIWebBrowserPersistable)
 NS_INTERFACE_MAP_END
 
 nsFrameLoader::nsFrameLoader(Element* aOwner, nsPIDOMWindowOuter* aOpener,
-                             bool aNetworkCreated, int32_t aJSPluginID)
+                             bool aNetworkCreated)
   : mOwnerContent(aOwner)
   , mDetachedSubdocFrame(nullptr)
   , mOpener(aOpener)
   , mRemoteBrowser(nullptr)
   , mChildID(0)
-  , mJSPluginID(aJSPluginID)
   , mEventMode(EVENT_MODE_NORMAL_DISPATCH)
   , mBrowserChangingProcessBlockers(nullptr)
   , mIsPrerendered(false)
   , mDepthTooGreat(false)
   , mIsTopLevelContent(false)
   , mDestroyCalled(false)
   , mNeedsAsyncDestroy(false)
   , mInSwap(false)
@@ -194,18 +193,17 @@ nsFrameLoader::~nsFrameLoader()
 {
   if (mMessageManager) {
     mMessageManager->Disconnect();
   }
   MOZ_RELEASE_ASSERT(mDestroyCalled);
 }
 
 nsFrameLoader*
-nsFrameLoader::Create(Element* aOwner, nsPIDOMWindowOuter* aOpener, bool aNetworkCreated,
-                      int32_t aJSPluginId)
+nsFrameLoader::Create(Element* aOwner, nsPIDOMWindowOuter* aOpener, bool aNetworkCreated)
 {
   NS_ENSURE_TRUE(aOwner, nullptr);
   nsIDocument* doc = aOwner->OwnerDoc();
 
   // We never create nsFrameLoaders for elements in resource documents.
   //
   // We never create nsFrameLoaders for elements in data documents, unless the
   // document is a static document.
@@ -225,17 +223,17 @@ nsFrameLoader::Create(Element* aOwner, n
   // since for a static document we know aOwner will end up in a document and
   // the nsFrameLoader will be used for its docShell.)
   //
   NS_ENSURE_TRUE(!doc->IsResourceDoc() &&
                  ((!doc->IsLoadedAsData() && aOwner->IsInComposedDoc()) ||
                   doc->IsStaticDocument()),
                  nullptr);
 
-  return new nsFrameLoader(aOwner, aOpener, aNetworkCreated, aJSPluginId);
+  return new nsFrameLoader(aOwner, aOpener, aNetworkCreated);
 }
 
 void
 nsFrameLoader::LoadFrame(ErrorResult& aRv)
 {
   nsresult rv = LoadFrame();
   if (NS_FAILED(rv)) {
     aRv.Throw(rv);
@@ -341,24 +339,18 @@ nsFrameLoader::LoadURI(nsIURI* aURI, nsI
 {
   if (!aURI)
     return NS_ERROR_INVALID_POINTER;
   NS_ENSURE_STATE(!mDestroyCalled && mOwnerContent);
 
   nsCOMPtr<nsIDocument> doc = mOwnerContent->OwnerDoc();
 
   nsresult rv;
-  // If IsForJSPlugin() returns true then we want to allow the load. We're just
-  // loading the source for the implementation of the JS plugin from a URI
-  // that's under our control. We will already have done the security checks for
-  // loading the plugin content itself in the object/embed loading code.
-  if (!IsForJSPlugin()) {
-    rv = CheckURILoad(aURI, aTriggeringPrincipal);
-    NS_ENSURE_SUCCESS(rv, rv);
-  }
+  rv = CheckURILoad(aURI, aTriggeringPrincipal);
+  NS_ENSURE_SUCCESS(rv, rv);
 
   mURIToLoad = aURI;
   mTriggeringPrincipal = aTriggeringPrincipal;
   rv = doc->InitializeFrameLoader(this);
   if (NS_FAILED(rv)) {
     mURIToLoad = nullptr;
     mTriggeringPrincipal = nullptr;
   }
@@ -2419,20 +2411,16 @@ nsFrameLoader::OwnerIsIsolatedMozBrowser
   }
 
   return false;
 }
 
 bool
 nsFrameLoader::ShouldUseRemoteProcess()
 {
-  if (IsForJSPlugin()) {
-    return true;
-  }
-
   if (PR_GetEnv("MOZ_DISABLE_OOP_TABS") ||
       Preferences::GetBool("dom.ipc.tabs.disabled", false)) {
     return false;
   }
 
   // Don't try to launch nested children if we don't have OMTC.
   // They won't render!
   if (XRE_IsContentProcess() &&
@@ -3142,17 +3130,17 @@ nsFrameLoader::TryRemoteBrowser()
       openingTab->Manager() &&
       openingTab->Manager()->IsContentParent()) {
     openerContentParent = openingTab->Manager()->AsContentParent();
   }
 
   // <iframe mozbrowser> gets to skip these checks.
   // iframes for JS plugins also get to skip these checks. We control the URL that gets
   // loaded, but the load is triggered from the document containing the plugin.
-  if (!OwnerIsMozBrowserFrame() && !IsForJSPlugin()) {
+  if (!OwnerIsMozBrowserFrame()) {
     if (parentDocShell->ItemType() != nsIDocShellTreeItem::typeChrome) {
       // Allow about:addon an exception to this rule so it can load remote
       // extension options pages.
       //
       // Note that the new frame's message manager will not be a child of the
       // chrome window message manager, and, the values of window.top and
       // window.parent will be different than they would be for a non-remote
       // frame.
@@ -3993,21 +3981,16 @@ nsFrameLoader::MaybeUpdatePrimaryTabPare
     }
   }
 }
 
 nsresult
 nsFrameLoader::GetNewTabContext(MutableTabContext* aTabContext,
                                 nsIURI* aURI)
 {
-  if (IsForJSPlugin()) {
-    return aTabContext->SetTabContextForJSPluginFrame(mJSPluginID) ? NS_OK :
-           NS_ERROR_FAILURE;
-  }
-
   OriginAttributes attrs;
   attrs.mInIsolatedMozBrowser = OwnerIsIsolatedMozBrowserFrame();
   nsresult rv;
 
   attrs.mAppId = nsIScriptSecurityManager::NO_APP_ID;
 
   // set the userContextId on the attrs before we pass them into
   // the tab context
diff --git a/dom/base/nsFrameLoader.h b/dom/base/nsFrameLoader.h
--- a/dom/base/nsFrameLoader.h
+++ b/dom/base/nsFrameLoader.h
@@ -78,18 +78,17 @@ class nsFrameLoader final : public nsIFr
   friend class RequestGroupedHistoryNavigationHelper;
   typedef mozilla::dom::PBrowserParent PBrowserParent;
   typedef mozilla::dom::TabParent TabParent;
   typedef mozilla::layout::RenderFrameParent RenderFrameParent;
 
 public:
   static nsFrameLoader* Create(mozilla::dom::Element* aOwner,
                                nsPIDOMWindowOuter* aOpener,
-                               bool aNetworkCreated,
-                               int32_t aJSPluginID = nsFakePluginTag::NOT_JSPLUGIN);
+                               bool aNetworkCreated);
 
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS_AMBIGUOUS(nsFrameLoader, nsIFrameLoader)
   NS_DECL_NSIFRAMELOADER
   NS_DECL_NSIMUTATIONOBSERVER_ATTRIBUTECHANGED
   NS_DECL_NSIWEBBROWSERPERSISTABLE
   nsresult CheckForRecursiveLoad(nsIURI* aURI);
   nsresult ReallyStartLoading();
@@ -345,34 +344,28 @@ public:
   RefPtr<nsFrameMessageManager> mMessageManager;
   nsCOMPtr<nsIInProcessContentFrameMessageManager> mChildMessageManager;
 
   virtual JSObject* WrapObject(JSContext* cx, JS::Handle<JSObject*> aGivenProto) override;
 
 private:
   nsFrameLoader(mozilla::dom::Element* aOwner,
                 nsPIDOMWindowOuter* aOpener,
-                bool aNetworkCreated,
-                int32_t aJSPluginID);
+                bool aNetworkCreated);
   ~nsFrameLoader();
 
   void SetOwnerContent(mozilla::dom::Element* aContent);
 
   bool ShouldUseRemoteProcess();
 
   /**
    * Return true if the frame is a remote frame. Return false otherwise
    */
   bool IsRemoteFrame();
 
-  bool IsForJSPlugin()
-  {
-    return mJSPluginID != nsFakePluginTag::NOT_JSPLUGIN;
-  }
-
   /**
    * Is this a frame loader for an isolated <iframe mozbrowser>?
    *
    * By default, mozbrowser frames are isolated.  Isolation can be disabled by
    * setting the frame's noisolation attribute.  Disabling isolation is
    * only allowed if the containing document is chrome.
    */
   bool OwnerIsIsolatedMozBrowserFrame();
@@ -468,18 +461,16 @@ private:
   nsCOMPtr<nsIDocument> mContainerDocWhileDetached;
 
   // An opener window which should be used when the docshell is created.
   nsCOMPtr<nsPIDOMWindowOuter> mOpener;
 
   TabParent* mRemoteBrowser;
   uint64_t mChildID;
 
-  int32_t mJSPluginID;
-
   // See nsIFrameLoader.idl. EVENT_MODE_NORMAL_DISPATCH automatically
   // forwards some input events to out-of-process content.
   uint32_t mEventMode;
 
   // Holds the last known size of the frame.
   mozilla::ScreenIntSize mLazySize;
 
   nsCOMPtr<nsIPartialSHistory> mPartialSHistory;
diff --git a/dom/base/nsObjectLoadingContent.cpp b/dom/base/nsObjectLoadingContent.cpp
--- a/dom/base/nsObjectLoadingContent.cpp
+++ b/dom/base/nsObjectLoadingContent.cpp
@@ -557,17 +557,17 @@ void
 nsObjectLoadingContent::SetupFrameLoader(int32_t aJSPluginId)
 {
   nsCOMPtr<nsIContent> thisContent =
     do_QueryInterface(static_cast<nsIImageLoadingContent*>(this));
   NS_ASSERTION(thisContent, "must be a content");
 
   mFrameLoader = nsFrameLoader::Create(thisContent->AsElement(),
                                        /* aOpener = */ nullptr,
-                                       mNetworkCreated, aJSPluginId);
+                                       mNetworkCreated);
   if (!mFrameLoader) {
     NS_NOTREACHED("nsFrameLoader::Create failed");
   }
 }
 
 // Helper to spawn the frameloader and return a pointer to its docshell.
 already_AddRefed<nsIDocShell>
 nsObjectLoadingContent::SetupDocShell(nsIURI* aRecursionCheckURI)
@@ -2231,107 +2231,16 @@ nsObjectLoadingContent::LoadObject(bool 
 
         // We'll handle this below
         doSpawnPlugin = true;
       } else {
         rv = AsyncStartPluginInstance();
       }
     }
     break;
-    case eType_FakePlugin:
-    {
-      if (mChannel) {
-        /// XXX(johns): Ideally we'd have some way to pass the channel to the
-        ///             fake plugin handler, but for now handlers will need to
-        ///             request element.srcURI themselves if they want it
-        LOG(("OBJLC [%p]: Closing unused channel for fake plugin type", this));
-        CloseChannel();
-      }
-
-      /// XXX(johns) Bug FIXME - We need to cleanup the various plugintag
-      ///            classes to be more sane and avoid this dance
-      nsCOMPtr<nsIPluginTag> basetag =
-        nsContentUtils::PluginTagForType(mContentType, false);
-      nsCOMPtr<nsIFakePluginTag> tag = do_QueryInterface(basetag);
-
-      uint32_t id;
-      if (NS_FAILED(tag->GetId(&id))) {
-        rv = NS_ERROR_FAILURE;
-        break;
-      }
-
-      MOZ_ASSERT(id <= PR_INT32_MAX,
-                 "Something went wrong, nsPluginHost::RegisterFakePlugin shouldn't have "
-                 "given out this id.");
-
-      SetupFrameLoader(int32_t(id));
-      if (!mFrameLoader) {
-        rv = NS_ERROR_FAILURE;
-        break;
-      }
-
-
-      nsString sandboxScript;
-      tag->GetSandboxScript(sandboxScript);
-      if (!sandboxScript.IsEmpty()) {
-        // Create a sandbox.
-        AutoJSAPI jsapi;
-        jsapi.Init();
-        JS::Rooted<JSObject*> sandbox(jsapi.cx());
-        rv = nsContentUtils::XPConnect()->
-          CreateSandbox(jsapi.cx(), nsContentUtils::GetSystemPrincipal(),
-                        sandbox.address());
-        if (NS_FAILED(rv)) {
-          break;
-        }
-
-        AutoEntryScript aes(sandbox, "JS plugin sandbox code");
-
-        JS::Rooted<JS::Value> element(aes.cx());
-        if (!ToJSValue(aes.cx(), thisContent, &element)) {
-          rv = NS_ERROR_FAILURE;
-          break;
-        }
-
-        if (!JS_DefineProperty(aes.cx(), sandbox, "pluginElement", element, JSPROP_ENUMERATE)) {
-          rv = NS_ERROR_FAILURE;
-          break;
-        }
-
-        JS::Rooted<JS::Value> rval(aes.cx());
-        // If the eval'ed code throws we won't load and do fallback instead.
-        rv = nsContentUtils::XPConnect()->EvalInSandboxObject(sandboxScript, nullptr, aes.cx(), sandbox, &rval);
-        if (NS_FAILED(rv)) {
-          break;
-        }
-      }
-
-      nsCOMPtr<nsIURI> handlerURI;
-      if (tag) {
-        tag->GetHandlerURI(getter_AddRefs(handlerURI));
-      }
-
-      if (!handlerURI) {
-        NS_NOTREACHED("Selected type is not a proper fake plugin handler");
-        rv = NS_ERROR_FAILURE;
-        break;
-      }
-
-      nsCString spec;
-      handlerURI->GetSpec(spec);
-      LOG(("OBJLC [%p]: Loading fake plugin handler (%s)", this, spec.get()));
-
-      rv = mFrameLoader->LoadURI(handlerURI);
-      if (NS_FAILED(rv)) {
-        LOG(("OBJLC [%p]: LoadURI() failed for fake handler", this));
-        mFrameLoader->Destroy();
-        mFrameLoader = nullptr;
-      }
-    }
-    break;
     case eType_Document:
     {
       if (!mChannel) {
         // We could mFrameLoader->LoadURI(mURI), but UpdateObjectParameters
         // requires documents have a channel, so this is not a valid state.
         NS_NOTREACHED("Attempting to load a document without a channel");
         rv = NS_ERROR_FAILURE;
         break;
@@ -2373,16 +2282,19 @@ nsObjectLoadingContent::LoadObject(bool 
       if (NS_FAILED(rv)) {
         LOG(("OBJLC [%p]: OpenChannel returned failure (%" PRIu32 ")",
              this, static_cast<uint32_t>(rv)));
       }
     break;
     case eType_Null:
       // Handled below, silence compiler warnings
     break;
+    case eType_FakePlugin:
+      // We're now in the process of removing FakePlugin. See bug 1529133.
+      MOZ_CRASH("Shouldn't reach here! This means there's a fakeplugin trying to be loaded.");
   }
 
   //
   // Loaded, handle notifications and fallback
   //
   if (NS_FAILED(rv)) {
     // If we failed in the loading hunk above, switch to fallback
     LOG(("OBJLC [%p]: Loading failed, switching to fallback", this));
