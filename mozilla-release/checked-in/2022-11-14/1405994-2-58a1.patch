# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1507637086 -7200
# Node ID fd1fe0931730ae57be8aee0969183be0121e2857
# Parent  7be4708b3e58c4f4111c2e5e38f9e4d0852a0790
Bug 1405994 part 2 - Use arguments rectifier when entering JIT code instead of a Vector. r=nbp

diff --git a/js/src/jit/BaselineJIT.cpp b/js/src/jit/BaselineJIT.cpp
--- a/js/src/jit/BaselineJIT.cpp
+++ b/js/src/jit/BaselineJIT.cpp
@@ -190,19 +190,17 @@ EnterBaseline(JSContext* cx, EnterJitDat
 JitExecStatus
 jit::EnterBaselineMethod(JSContext* cx, RunState& state)
 {
     BaselineScript* baseline = state.script()->baselineScript();
 
     EnterJitData data(cx);
     data.jitcode = baseline->method()->raw();
 
-    Rooted<GCVector<Value>> vals(cx, GCVector<Value>(cx));
-    if (!SetEnterJitData(cx, data, state, &vals))
-        return JitExec_Error;
+    SetEnterJitData(cx, data, state);
 
     JitExecStatus status = EnterBaseline(cx, data);
     if (status != JitExec_Ok)
         return status;
 
     state.setReturnValue(data.result);
     return JitExec_Ok;
 }
diff --git a/js/src/jit/Ion.cpp b/js/src/jit/Ion.cpp
--- a/js/src/jit/Ion.cpp
+++ b/js/src/jit/Ion.cpp
@@ -2821,94 +2821,63 @@ EnterIon(JSContext* cx, EnterJitData& da
 
     // Release temporary buffer used for OSR into Ion.
     cx->freeOsrTempData();
 
     MOZ_ASSERT_IF(data.result.isMagic(), data.result.isMagic(JS_ION_ERROR));
     return data.result.isMagic() ? JitExec_Error : JitExec_Ok;
 }
 
-bool
-jit::SetEnterJitData(JSContext* cx, EnterJitData& data, RunState& state,
-                     MutableHandle<GCVector<Value>> vals)
+void
+jit::SetEnterJitData(JSContext* cx, EnterJitData& data, RunState& state)
 {
     data.osrFrame = nullptr;
 
     // Note: keep this in sync with EnterBaselineAtBranch.
 
     if (state.isInvoke()) {
         const CallArgs& args = state.asInvoke()->args();
         unsigned numFormals = state.script()->functionNonDelazifying()->nargs();
         data.constructing = state.asInvoke()->constructing();
         data.numActualArgs = args.length();
-        data.maxArgc = Max(args.length(), numFormals) + 1;
+        data.maxArgc = args.length() + 1;
+        data.maxArgv = args.array() - 1; // -1 to include |this|
         data.envChain = nullptr;
         data.calleeToken = CalleeToToken(&args.callee().as<JSFunction>(), data.constructing);
-
-        if (data.numActualArgs >= numFormals) {
-            data.maxArgv = args.array() - 1; // -1 to include |this|
-        } else {
-            MOZ_ASSERT(vals.empty());
-            unsigned numPushedArgs = Max(args.length(), numFormals);
-            if (!vals.reserve(numPushedArgs + 1 + data.constructing))
-                return false;
-
-            // Append |this| and any provided arguments.
-            for (size_t i = 1; i < args.length() + 2; ++i)
-                vals.infallibleAppend(args.base()[i]);
-
-            // Pad missing arguments with |undefined|.
-            while (vals.length() < numFormals + 1)
-                vals.infallibleAppend(UndefinedValue());
-
-            if (data.constructing)
-                vals.infallibleAppend(args.newTarget());
-
-            MOZ_ASSERT(vals.length() >= numFormals + 1 + data.constructing);
-            data.maxArgv = vals.begin();
-        }
+        if (numFormals > data.numActualArgs)
+            data.jitcode = cx->runtime()->jitRuntime()->getArgumentsRectifier()->raw();
     } else {
         data.constructing = false;
         data.numActualArgs = 0;
-        data.maxArgc = 0;
-        data.maxArgv = nullptr;
         data.envChain = state.asExecute()->environmentChain();
-
         data.calleeToken = CalleeToToken(state.script());
 
-        if (state.script()->isForEval() && state.script()->isDirectEvalInFunction()) {
-            // Push newTarget onto the stack.
-            if (!vals.reserve(1))
-                return false;
-
-            data.maxArgc = 1;
-            data.maxArgv = vals.begin();
+        if (state.script()->isDirectEvalInFunction()) {
             if (state.asExecute()->newTarget().isNull()) {
                 ScriptFrameIter iter(cx);
-                vals.infallibleAppend(iter.newTarget());
-            } else {
-                vals.infallibleAppend(state.asExecute()->newTarget());
+                state.asExecute()->setNewTarget(iter.newTarget());
             }
+            data.maxArgc = 1;
+            data.maxArgv = state.asExecute()->addressOfNewTarget();
+        } else {
+            data.maxArgc = 0;
+            data.maxArgv = nullptr;
         }
     }
-
-    return true;
 }
 
 JitExecStatus
 jit::IonCannon(JSContext* cx, RunState& state)
 {
     IonScript* ion = state.script()->ionScript();
 
     EnterJitData data(cx);
     data.jitcode = ion->method()->raw();
 
-    Rooted<GCVector<Value>> vals(cx, GCVector<Value>(cx));
-    if (!SetEnterJitData(cx, data, state, &vals))
-        return JitExec_Error;
+    SetEnterJitData(cx, data, state);
 
     JitExecStatus status = EnterIon(cx, data);
 
     if (status == JitExec_Ok)
         state.setReturnValue(data.result);
 
     return status;
 }
diff --git a/js/src/jit/Ion.h b/js/src/jit/Ion.h
--- a/js/src/jit/Ion.h
+++ b/js/src/jit/Ion.h
@@ -121,18 +121,17 @@ enum JitExecStatus
 static inline bool
 IsErrorStatus(JitExecStatus status)
 {
     return status == JitExec_Error || status == JitExec_Aborted;
 }
 
 struct EnterJitData;
 
-MOZ_MUST_USE bool SetEnterJitData(JSContext* cx, EnterJitData& data, RunState& state,
-                                  MutableHandle<GCVector<Value>> vals);
+void SetEnterJitData(JSContext* cx, EnterJitData& data, RunState& state);
 
 JitExecStatus IonCannon(JSContext* cx, RunState& state);
 
 // Used to enter Ion from C++ natives like Array.map. Called from FastInvokeGuard.
 JitExecStatus FastInvoke(JSContext* cx, HandleFunction fun, CallArgs& args);
 
 // Walk the stack and invalidate active Ion frames for the invalid scripts.
 void Invalidate(TypeZone& types, FreeOp* fop,
diff --git a/js/src/vm/Interpreter.h b/js/src/vm/Interpreter.h
--- a/js/src/vm/Interpreter.h
+++ b/js/src/vm/Interpreter.h
@@ -256,17 +256,20 @@ class ExecuteState : public RunState
                  JSObject& envChain, AbstractFramePtr evalInFrame, Value* result)
       : RunState(cx, Execute, script),
         newTargetValue_(cx, newTargetValue),
         envChain_(cx, &envChain),
         evalInFrame_(evalInFrame),
         result_(result)
     { }
 
-    Value newTarget() { return newTargetValue_; }
+    Value newTarget() const { return newTargetValue_; }
+    void setNewTarget(const Value& v) { newTargetValue_ = v; }
+    Value* addressOfNewTarget() { return newTargetValue_.address(); }
+
     JSObject* environmentChain() const { return envChain_; }
     bool isDebuggerEval() const { return !!evalInFrame_; }
 
     virtual InterpreterFrame* pushInterpreterFrame(JSContext* cx);
 
     virtual void setReturnValue(const Value& v) {
         if (result_)
             *result_ = v;
