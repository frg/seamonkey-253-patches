# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1508419131 -7200
# Node ID aec42777a66d07d430f3c7aef65e53e065e5b5a4
# Parent  bc6264ebae0cd3783311c503beed696877185715
Bug 1392961 - Add preference to make VP9 the preferred video codec. r=jesup

Behaviour is controlled through the media.navigator.video.vp9_preferred preference.

MozReview-Commit-ID: J06ArFYNmTk

diff --git a/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp b/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp
--- a/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp
+++ b/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp
@@ -828,17 +828,18 @@ class CompareCodecPriority {
 
 class ConfigureCodec {
   public:
     explicit ConfigureCodec(nsCOMPtr<nsIPrefBranch>& branch) :
       mHardwareH264Enabled(false),
       mHardwareH264Supported(false),
       mSoftwareH264Enabled(false),
       mH264Enabled(false),
-      mVP9Enabled(false),
+      mVP9Enabled(true),
+      mVP9Preferred(false),
       mH264Level(13), // minimum suggested for WebRTC spec
       mH264MaxBr(0), // Unlimited
       mH264MaxMbps(0), // Unlimited
       mVP8MaxFs(0),
       mVP8MaxFr(0),
       mUseTmmbr(false),
       mUseRemb(false),
       mUseAudioFec(false),
@@ -854,16 +855,19 @@ class ConfigureCodec {
 
       branch->GetIntPref("media.navigator.video.h264.max_br", &mH264MaxBr);
 
       branch->GetIntPref("media.navigator.video.h264.max_mbps", &mH264MaxMbps);
 
       branch->GetBoolPref("media.peerconnection.video.vp9_enabled",
           &mVP9Enabled);
 
+      branch->GetBoolPref("media.peerconnection.video.vp9_preferred",
+          &mVP9Preferred);
+
       branch->GetIntPref("media.navigator.video.max_fs", &mVP8MaxFs);
       if (mVP8MaxFs <= 0) {
         mVP8MaxFs = 12288; // We must specify something other than 0
       }
 
       branch->GetIntPref("media.navigator.video.max_fr", &mVP8MaxFr);
       if (mVP8MaxFr <= 0) {
         mVP8MaxFr = 60; // We must specify something other than 0
@@ -925,19 +929,24 @@ class ConfigureCodec {
               if (mHardwareH264Supported) {
                 videoCodec.mStronglyPreferred = true;
               }
             } else if (videoCodec.mName == "red") {
               videoCodec.mEnabled = mRedUlpfecEnabled;
             } else if (videoCodec.mName == "ulpfec") {
               videoCodec.mEnabled = mRedUlpfecEnabled;
             } else if (videoCodec.mName == "VP8" || videoCodec.mName == "VP9") {
-              if (videoCodec.mName == "VP9" && !mVP9Enabled) {
-                videoCodec.mEnabled = false;
-                break;
+              if (videoCodec.mName == "VP9") {
+                if (!mVP9Enabled) {
+                  videoCodec.mEnabled = false;
+                  break;
+                }
+                if (mVP9Preferred) {
+                  videoCodec.mStronglyPreferred = true;
+                }
               }
               videoCodec.mConstraints.maxFs = mVP8MaxFs;
               videoCodec.mConstraints.maxFps = mVP8MaxFr;
             }
 
             if (mUseTmmbr) {
               videoCodec.EnableTmmbr();
             }
@@ -954,16 +963,17 @@ class ConfigureCodec {
     }
 
   private:
     bool mHardwareH264Enabled;
     bool mHardwareH264Supported;
     bool mSoftwareH264Enabled;
     bool mH264Enabled;
     bool mVP9Enabled;
+    bool mVP9Preferred;
     int32_t mH264Level;
     int32_t mH264MaxBr;
     int32_t mH264MaxMbps;
     int32_t mVP8MaxFs;
     int32_t mVP8MaxFr;
     bool mUseTmmbr;
     bool mUseRemb;
     bool mUseAudioFec;
diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -482,16 +482,17 @@ pref("media.peerconnection.enabled", tru
 pref("media.peerconnection.video.enabled", true);
 pref("media.navigator.video.max_fs", 12288); // Enough for 2048x1536
 pref("media.navigator.video.max_fr", 60);
 pref("media.navigator.video.h264.level", 31); // 0x42E01f - level 3.1
 pref("media.navigator.video.h264.max_br", 0);
 pref("media.navigator.video.h264.max_mbps", 0);
 pref("media.peerconnection.video.h264_enabled", false);
 pref("media.peerconnection.video.vp9_enabled", true);
+pref("media.peerconnection.video.vp9_preferred", false);
 pref("media.getusermedia.aec", 1);
 pref("media.getusermedia.browser.enabled", false);
 pref("media.getusermedia.channels", 0);
 // Desktop is typically VGA capture or more; and qm_select will not drop resolution
 // below 1/2 in each dimension (or so), so QVGA (320x200) is the lowest here usually.
 pref("media.peerconnection.video.min_bitrate", 0);
 pref("media.peerconnection.video.start_bitrate", 0);
 pref("media.peerconnection.video.max_bitrate", 0);
