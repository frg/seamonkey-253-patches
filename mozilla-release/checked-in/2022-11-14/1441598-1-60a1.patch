# HG changeset patch
# User Bob Owen <bobowencode@gmail.com>
# Date 1520238134 0
# Node ID b47e9371891215b7f5bb5e7ec26485f51b4e477c
# Parent  41c067edfb0f2ce36b4b2b834129daa590b20f9e
Bug 1441598: Don't try and send messages to PrintProgressDialogChild when printing is complete. r=jwatt

diff --git a/layout/printing/nsPrintEngine.cpp b/layout/printing/nsPrintEngine.cpp
--- a/layout/printing/nsPrintEngine.cpp
+++ b/layout/printing/nsPrintEngine.cpp
@@ -3503,19 +3503,22 @@ nsPrintEngine::StartPagePrintTimer(const
 
   return mPagePrintTimer->Start(aPO.get());
 }
 
 /*=============== nsIObserver Interface ======================*/
 NS_IMETHODIMP
 nsPrintEngine::Observe(nsISupports *aSubject, const char *aTopic, const char16_t *aData)
 {
-  nsresult rv = NS_ERROR_FAILURE;
-
-  rv = InitPrintDocConstruction(true);
+  // Only process a null topic which means the progress dialog is open.
+  if (aTopic) {
+    return NS_OK;
+  }
+
+  nsresult rv = InitPrintDocConstruction(true);
   if (!mIsDoingPrinting && mPrtPreview) {
     RefPtr<nsPrintData> printDataOfPrintPreview = mPrtPreview;
     printDataOfPrintPreview->OnEndPrinting();
   }
 
   return rv;
 
 }
diff --git a/toolkit/components/printing/content/printUtils.js b/toolkit/components/printing/content/printUtils.js
--- a/toolkit/components/printing/content/printUtils.js
+++ b/toolkit/components/printing/content/printUtils.js
@@ -484,16 +484,21 @@ var PrintUtils = {
     }
     return printSettings;
   },
 
   // This observer is called once the progress dialog has been "opened"
   _obsPP:
   {
     observe(aSubject, aTopic, aData) {
+      // Only process a null topic which means the progress dialog is open.
+      if (aTopic) {
+        return;
+      }
+
       // delay the print preview to show the content of the progress dialog
       setTimeout(function() { PrintUtils.enterPrintPreview(); }, 0);
     },
 
     QueryInterface(iid) {
       if (iid.equals(Ci.nsIObserver) ||
           iid.equals(Ci.nsISupportsWeakReference) ||
           iid.equals(Ci.nsISupports))
diff --git a/toolkit/components/printingui/ipc/PrintProgressDialogParent.cpp b/toolkit/components/printingui/ipc/PrintProgressDialogParent.cpp
--- a/toolkit/components/printingui/ipc/PrintProgressDialogParent.cpp
+++ b/toolkit/components/printingui/ipc/PrintProgressDialogParent.cpp
@@ -94,18 +94,23 @@ PrintProgressDialogParent::Recv__delete_
 }
 
 // nsIObserver
 NS_IMETHODIMP
 PrintProgressDialogParent::Observe(nsISupports *aSubject, const char *aTopic,
                                    const char16_t *aData)
 {
   if (mActive) {
-    if (aTopic && !strcmp(aTopic, "cancelled")) {
-      Unused << SendCancelledCurrentJob();
+    if (aTopic) {
+      if (!strcmp(aTopic, "cancelled")) {
+        Unused << SendCancelledCurrentJob();
+      } else if (!strcmp(aTopic, "completed")) {
+        // Once printing is complete don't send any messages to the child.
+        mActive = false;
+      }
     } else {
       Unused << SendDialogOpened();
     }
   } else {
     NS_WARNING("The print progress dialog finished opening, but communications "
                "with the child have been closed.");
   }
 
diff --git a/toolkit/components/printingui/unixshared/nsPrintProgress.cpp b/toolkit/components/printingui/unixshared/nsPrintProgress.cpp
--- a/toolkit/components/printingui/unixshared/nsPrintProgress.cpp
+++ b/toolkit/components/printingui/unixshared/nsPrintProgress.cpp
@@ -174,32 +174,48 @@ NS_IMETHODIMP nsPrintProgress::DoneIniti
   if (m_observer) {
     m_observer->Observe(nullptr, nullptr, nullptr);
   }
   return NS_OK;
 }
 
 NS_IMETHODIMP nsPrintProgress::OnStateChange(nsIWebProgress *aWebProgress, nsIRequest *aRequest, uint32_t aStateFlags, nsresult aStatus)
 {
+  if (XRE_IsE10sParentProcess() &&
+      aStateFlags & nsIWebProgressListener::STATE_STOP) {
+    // If we're in an e10s parent, m_observer is a PrintProgressDialogParent,
+    // so we let it know that printing has completed, because it might mean that
+    // its PrintProgressDialogChild has already been deleted.
+    m_observer->Observe(nullptr, "completed", nullptr);
+  }
+
   m_pendingStateFlags = aStateFlags;
   m_pendingStateValue = aStatus;
 
   uint32_t count = m_listenerList.Count();
   for (uint32_t i = count - 1; i < count; i --)
   {
     nsCOMPtr<nsIWebProgressListener> progressListener = m_listenerList.SafeObjectAt(i);
     if (progressListener)
       progressListener->OnStateChange(aWebProgress, aRequest, aStateFlags, aStatus);
   }
 
   return NS_OK;
 }
 
 NS_IMETHODIMP nsPrintProgress::OnProgressChange(nsIWebProgress *aWebProgress, nsIRequest *aRequest, int32_t aCurSelfProgress, int32_t aMaxSelfProgress, int32_t aCurTotalProgress, int32_t aMaxTotalProgress)
 {
+  if (XRE_IsE10sParentProcess() && aCurSelfProgress &&
+      aCurSelfProgress >= aMaxSelfProgress) {
+    // If we're in an e10s parent, m_observer is a PrintProgressDialogParent,
+    // so we let it know that printing has completed, because it might mean that
+    // its PrintProgressDialogChild has already been deleted.
+    m_observer->Observe(nullptr, "completed", nullptr);
+  }
+
   uint32_t count = m_listenerList.Count();
   for (uint32_t i = count - 1; i < count; i --)
   {
     nsCOMPtr<nsIWebProgressListener> progressListener = m_listenerList.SafeObjectAt(i);
     if (progressListener)
       progressListener->OnProgressChange(aWebProgress, aRequest, aCurSelfProgress, aMaxSelfProgress, aCurTotalProgress, aMaxTotalProgress);
   }
 
diff --git a/toolkit/components/printingui/win/nsPrintProgress.cpp b/toolkit/components/printingui/win/nsPrintProgress.cpp
--- a/toolkit/components/printingui/win/nsPrintProgress.cpp
+++ b/toolkit/components/printingui/win/nsPrintProgress.cpp
@@ -203,32 +203,48 @@ NS_IMETHODIMP nsPrintProgress::DoneIniti
   if (m_observer) {
     m_observer->Observe(nullptr, nullptr, nullptr);
   }
   return NS_OK;
 }
 
 NS_IMETHODIMP nsPrintProgress::OnStateChange(nsIWebProgress *aWebProgress, nsIRequest *aRequest, uint32_t aStateFlags, nsresult aStatus)
 {
+  if (XRE_IsE10sParentProcess() &&
+      aStateFlags & nsIWebProgressListener::STATE_STOP) {
+    // If we're in an e10s parent, m_observer is a PrintProgressDialogParent,
+    // so we let it know that printing has completed, because it might mean that
+    // its PrintProgressDialogChild has already been deleted.
+    m_observer->Observe(nullptr, "completed", nullptr);
+  }
+
   m_pendingStateFlags = aStateFlags;
   m_pendingStateValue = aStatus;
 
   uint32_t count = m_listenerList.Count();
   for (uint32_t i = count - 1; i < count; i --)
   {
     nsCOMPtr<nsIWebProgressListener> progressListener = m_listenerList.SafeObjectAt(i);
     if (progressListener)
       progressListener->OnStateChange(aWebProgress, aRequest, aStateFlags, aStatus);
   }
 
   return NS_OK;
 }
 
 NS_IMETHODIMP nsPrintProgress::OnProgressChange(nsIWebProgress *aWebProgress, nsIRequest *aRequest, int32_t aCurSelfProgress, int32_t aMaxSelfProgress, int32_t aCurTotalProgress, int32_t aMaxTotalProgress)
 {
+  if (XRE_IsE10sParentProcess() && aCurSelfProgress &&
+      aCurSelfProgress >= aMaxSelfProgress) {
+    // If we're in an e10s parent, m_observer is a PrintProgressDialogParent,
+    // so we let it know that printing has completed, because it might mean that
+    // its PrintProgressDialogChild has already been deleted.
+    m_observer->Observe(nullptr, "completed", nullptr);
+  }
+
   uint32_t count = m_listenerList.Count();
   for (uint32_t i = count - 1; i < count; i --)
   {
     nsCOMPtr<nsIWebProgressListener> progressListener = m_listenerList.SafeObjectAt(i);
     if (progressListener)
       progressListener->OnProgressChange(aWebProgress, aRequest, aCurSelfProgress, aMaxSelfProgress, aCurTotalProgress, aMaxTotalProgress);
   }
 
