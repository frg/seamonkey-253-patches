# HG changeset patch
# User John Dai <jdai@mozilla.com>
# Date 1506919320 14400
# Node ID 0ad5ea4a3f1cda04e949cdae7e2c22f54d44d476
# Parent  5cd94556ebf5743d8d3e8e00fa499ada7cc724fa
Bug 1121994 - Implement adopted callback for custom elements. r=smaug

diff --git a/dom/base/CustomElementRegistry.cpp b/dom/base/CustomElementRegistry.cpp
--- a/dom/base/CustomElementRegistry.cpp
+++ b/dom/base/CustomElementRegistry.cpp
@@ -45,29 +45,33 @@ CustomElementCallback::Call()
         // fail to find the CE definition for this custom element.
         // This will go away eventually since there is no created callback in v1.
         CustomElementDefinition* definition =
           nsContentUtils::LookupCustomElementDefinition(document,
             ni->LocalName(), ni->NamespaceID(),
             extType.IsEmpty() ? nullptr : &extType);
 
         nsContentUtils::EnqueueLifecycleCallback(
-          nsIDocument::eConnected, mThisObject, nullptr, definition);
+          nsIDocument::eConnected, mThisObject, nullptr, nullptr, definition);
       }
 
       static_cast<LifecycleCreatedCallback *>(mCallback.get())->Call(mThisObject, rv);
       mOwnerData->mElementIsBeingCreated = false;
       break;
     }
     case nsIDocument::eConnected:
       static_cast<LifecycleConnectedCallback *>(mCallback.get())->Call(mThisObject, rv);
       break;
     case nsIDocument::eDisconnected:
       static_cast<LifecycleDisconnectedCallback *>(mCallback.get())->Call(mThisObject, rv);
       break;
+    case nsIDocument::eAdopted:
+      static_cast<LifecycleAdoptedCallback *>(mCallback.get())->Call(mThisObject,
+        mAdoptedCallbackArgs.mOldDocument, mAdoptedCallbackArgs.mNewDocument, rv);
+      break;
     case nsIDocument::eAttributeChanged:
       static_cast<LifecycleAttributeChangedCallback *>(mCallback.get())->Call(mThisObject,
         mArgs.name, mArgs.oldValue, mArgs.newValue, mArgs.namespaceURI, rv);
       break;
   }
 }
 
 void
@@ -320,17 +324,19 @@ CustomElementRegistry::SetupCustomElemen
   // We make it synchronously for createElement/createElementNS in order to
   // pass tests. It'll be removed when we deprecate custom elements v0.
   SyncInvokeReactions(nsIDocument::eCreated, aElement, definition);
 }
 
 /* static */ UniquePtr<CustomElementCallback>
 CustomElementRegistry::CreateCustomElementCallback(
   nsIDocument::ElementCallbackType aType, Element* aCustomElement,
-  LifecycleCallbackArgs* aArgs, CustomElementDefinition* aDefinition)
+  LifecycleCallbackArgs* aArgs,
+  LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
+  CustomElementDefinition* aDefinition)
 {
   MOZ_ASSERT(aDefinition, "CustomElementDefinition should not be null");
 
   RefPtr<CustomElementData> elementData = aCustomElement->GetCustomElementData();
   MOZ_ASSERT(elementData, "CustomElementData should exist");
 
   // Let CALLBACK be the callback associated with the key NAME in CALLBACKS.
   CallbackFunction* func = nullptr;
@@ -348,16 +354,22 @@ CustomElementRegistry::CreateCustomEleme
       break;
 
     case nsIDocument::eDisconnected:
       if (aDefinition->mCallbacks->mDisconnectedCallback.WasPassed()) {
         func = aDefinition->mCallbacks->mDisconnectedCallback.Value();
       }
       break;
 
+    case nsIDocument::eAdopted:
+      if (aDefinition->mCallbacks->mAdoptedCallback.WasPassed()) {
+        func = aDefinition->mCallbacks->mAdoptedCallback.Value();
+      }
+      break;
+
     case nsIDocument::eAttributeChanged:
       if (aDefinition->mCallbacks->mAttributeChangedCallback.WasPassed()) {
         func = aDefinition->mCallbacks->mAttributeChangedCallback.Value();
       }
       break;
   }
 
   // If there is no such callback, stop.
@@ -376,26 +388,29 @@ CustomElementRegistry::CreateCustomEleme
   // Add CALLBACK to ELEMENT's callback queue.
   auto callback =
     MakeUnique<CustomElementCallback>(aCustomElement, aType, func, elementData);
 
   if (aArgs) {
     callback->SetArgs(*aArgs);
   }
 
+  if (aAdoptedCallbackArgs) {
+    callback->SetAdoptedCallbackArgs(*aAdoptedCallbackArgs);
+  }
   return Move(callback);
 }
 
 void
 CustomElementRegistry::SyncInvokeReactions(nsIDocument::ElementCallbackType aType,
                                            Element* aCustomElement,
                                            CustomElementDefinition* aDefinition)
 {
   auto callback = CreateCustomElementCallback(aType, aCustomElement, nullptr,
-                                              aDefinition);
+                                              nullptr, aDefinition);
   if (!callback) {
     return;
   }
 
   UniquePtr<CustomElementReaction> reaction(Move(
     MakeUnique<CustomElementCallbackReaction>(aDefinition,
                                               Move(callback))));
 
@@ -404,29 +419,31 @@ CustomElementRegistry::SyncInvokeReactio
 
   nsContentUtils::AddScriptRunner(runnable);
 }
 
 /* static */ void
 CustomElementRegistry::EnqueueLifecycleCallback(nsIDocument::ElementCallbackType aType,
                                                 Element* aCustomElement,
                                                 LifecycleCallbackArgs* aArgs,
+                                                LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
                                                 CustomElementDefinition* aDefinition)
 {
   CustomElementDefinition* definition = aDefinition;
   if (!definition) {
     definition = aCustomElement->GetCustomElementDefinition();
     if (!definition ||
         definition->mLocalName != aCustomElement->NodeInfo()->NameAtom()) {
       return;
     }
   }
 
   auto callback =
-    CreateCustomElementCallback(aType, aCustomElement, aArgs, definition);
+    CreateCustomElementCallback(aType, aCustomElement, aArgs,
+                                aAdoptedCallbackArgs, definition);
   if (!callback) {
     return;
   }
 
   DocGroup* docGroup = aCustomElement->OwnerDoc()->GetDocGroup();
   if (!docGroup) {
     return;
   }
@@ -932,25 +949,25 @@ CustomElementRegistry::Upgrade(Element* 
         LifecycleCallbackArgs args = {
           nsDependentAtomString(attrName),
           VoidString(),
           (attrValue.IsEmpty() ? VoidString() : attrValue),
           (namespaceURI.IsEmpty() ? VoidString() : namespaceURI)
         };
         nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eAttributeChanged,
                                                  aElement,
-                                                 &args, aDefinition);
+                                                 &args, nullptr, aDefinition);
       }
     }
   }
 
   // Step 4.
   if (aElement->IsInComposedDoc()) {
     nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eConnected, aElement,
-                                             nullptr, aDefinition);
+      nullptr, nullptr, aDefinition);
   }
 
   // Step 5.
   AutoConstructionStackEntry acs(aDefinition->mConstructionStack,
                                  nsGenericHTMLElement::FromContent(aElement));
 
   // Step 6 and step 7.
   DoUpgrade(aElement, aDefinition->mConstructor, aRv);
@@ -964,17 +981,18 @@ CustomElementRegistry::Upgrade(Element* 
   // Step 8.
   data->mState = CustomElementData::State::eCustom;
 
   // Step 9.
   aElement->SetCustomElementDefinition(aDefinition);
 
   // This is for old spec.
   nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eCreated,
-                                           aElement, nullptr, aDefinition);
+                                           aElement, nullptr,
+                                           nullptr, aDefinition);
 }
 
 //-----------------------------------------------------
 // CustomElementReactionsStack
 
 void
 CustomElementReactionsStack::CreateAndPushElementQueue()
 {
@@ -1147,16 +1165,21 @@ NS_IMPL_CYCLE_COLLECTION_TRAVERSE_BEGIN(
     cb.NoteXPCOMChild(callbacks->mConnectedCallback.Value());
   }
 
   if (callbacks->mDisconnectedCallback.WasPassed()) {
     NS_CYCLE_COLLECTION_NOTE_EDGE_NAME(cb, "mCallbacks->mDisconnectedCallback");
     cb.NoteXPCOMChild(callbacks->mDisconnectedCallback.Value());
   }
 
+  if (callbacks->mAdoptedCallback.WasPassed()) {
+    NS_CYCLE_COLLECTION_NOTE_EDGE_NAME(cb, "mCallbacks->mAdoptedCallback");
+    cb.NoteXPCOMChild(callbacks->mAdoptedCallback.Value());
+  }
+
   NS_CYCLE_COLLECTION_NOTE_EDGE_NAME(cb, "mConstructor");
   cb.NoteXPCOMChild(tmp->mConstructor);
 NS_IMPL_CYCLE_COLLECTION_TRAVERSE_END
 
 NS_IMPL_CYCLE_COLLECTION_TRACE_BEGIN(CustomElementDefinition)
   NS_IMPL_CYCLE_COLLECTION_TRACE_JS_MEMBER_CALLBACK(mPrototype)
 NS_IMPL_CYCLE_COLLECTION_TRACE_END
 
diff --git a/dom/base/CustomElementRegistry.h b/dom/base/CustomElementRegistry.h
--- a/dom/base/CustomElementRegistry.h
+++ b/dom/base/CustomElementRegistry.h
@@ -35,41 +35,55 @@ class Promise;
 struct LifecycleCallbackArgs
 {
   nsString name;
   nsString oldValue;
   nsString newValue;
   nsString namespaceURI;
 };
 
+struct LifecycleAdoptedCallbackArgs
+{
+  nsCOMPtr<nsIDocument> mOldDocument;
+  nsCOMPtr<nsIDocument> mNewDocument;
+};
+
 class CustomElementCallback
 {
 public:
   CustomElementCallback(Element* aThisObject,
                         nsIDocument::ElementCallbackType aCallbackType,
                         CallbackFunction* aCallback,
                         CustomElementData* aOwnerData);
   void Traverse(nsCycleCollectionTraversalCallback& aCb) const;
   void Call();
   void SetArgs(LifecycleCallbackArgs& aArgs)
   {
     MOZ_ASSERT(mType == nsIDocument::eAttributeChanged,
                "Arguments are only used by attribute changed callback.");
     mArgs = aArgs;
   }
 
+  void SetAdoptedCallbackArgs(LifecycleAdoptedCallbackArgs& aAdoptedCallbackArgs)
+  {
+    MOZ_ASSERT(mType == nsIDocument::eAdopted,
+      "Arguments are only used by adopted callback.");
+    mAdoptedCallbackArgs = aAdoptedCallbackArgs;
+  }
+
 private:
   // The this value to use for invocation of the callback.
   RefPtr<Element> mThisObject;
   RefPtr<CallbackFunction> mCallback;
   // The type of callback (eCreated, eAttached, etc.)
   nsIDocument::ElementCallbackType mType;
   // Arguments to be passed to the callback,
   // used by the attribute changed callback.
   LifecycleCallbackArgs mArgs;
+  LifecycleAdoptedCallbackArgs mAdoptedCallbackArgs;
   // CustomElementData that contains this callback in the
   // callback queue.
   CustomElementData* mOwnerData;
 };
 
 class CustomElementConstructor final : public CallbackFunction
 {
 public:
@@ -369,16 +383,17 @@ public:
    * newly created custom elements, possibly extending an existing type.
    * ex. <x-button>, <button is="x-button> (type extension)
    */
   void SetupCustomElement(Element* aElement, const nsAString* aTypeExtension);
 
   static void EnqueueLifecycleCallback(nsIDocument::ElementCallbackType aType,
                                        Element* aCustomElement,
                                        LifecycleCallbackArgs* aArgs,
+                                       LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
                                        CustomElementDefinition* aDefinition);
 
   void GetCustomPrototype(nsIAtom* aAtom,
                           JS::MutableHandle<JSObject*> aPrototype);
 
   void SyncInvokeReactions(nsIDocument::ElementCallbackType aType,
                            Element* aCustomElement,
                            CustomElementDefinition* aDefinition);
@@ -389,17 +404,19 @@ public:
    */
   static void Upgrade(Element* aElement, CustomElementDefinition* aDefinition, ErrorResult& aRv);
 
 private:
   ~CustomElementRegistry();
 
   static UniquePtr<CustomElementCallback> CreateCustomElementCallback(
     nsIDocument::ElementCallbackType aType, Element* aCustomElement,
-    LifecycleCallbackArgs* aArgs, CustomElementDefinition* aDefinition);
+    LifecycleCallbackArgs* aArgs,
+    LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
+    CustomElementDefinition* aDefinition);
 
   /**
    * Registers an unresolved custom element that is a candidate for
    * upgrade when the definition is registered via registerElement.
    * |aTypeName| is the name of the custom element type, if it is not
    * provided, then element name is used. |aTypeName| should be provided
    * when registering a custom element that extends an existing
    * element. e.g. <button is="x-button">.
diff --git a/dom/base/Element.cpp b/dom/base/Element.cpp
--- a/dom/base/Element.cpp
+++ b/dom/base/Element.cpp
@@ -2729,18 +2729,18 @@ Element::SetAttrAndNotify(int32_t aNames
         LifecycleCallbackArgs args = {
           nsDependentAtomString(aName),
           aModType == nsIDOMMutationEvent::ADDITION ?
             VoidString() : nsDependentAtomString(oldValueAtom),
           nsDependentAtomString(newValueAtom),
           (ns.IsEmpty() ? VoidString() : ns)
         };
 
-        nsContentUtils::EnqueueLifecycleCallback(
-          nsIDocument::eAttributeChanged, this, &args, definition);
+        nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eAttributeChanged,
+          this, &args, nullptr, definition);
       }
     }
   }
 
   if (aCallAfterSetAttr) {
     rv = AfterSetAttr(aNamespaceID, aName, &valueForAfterSetAttr, oldValue,
                       aNotify);
     NS_ENSURE_SUCCESS(rv, rv);
@@ -3024,18 +3024,18 @@ Element::UnsetAttr(int32_t aNameSpaceID,
         nsCOMPtr<nsIAtom> oldValueAtom = oldValue.GetAsAtom();
         LifecycleCallbackArgs args = {
           nsDependentAtomString(aName),
           nsDependentAtomString(oldValueAtom),
           VoidString(),
           (ns.IsEmpty() ? VoidString() : ns)
         };
 
-        nsContentUtils::EnqueueLifecycleCallback(
-          nsIDocument::eAttributeChanged, this, &args, definition);
+        nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eAttributeChanged,
+          this, &args, nullptr, definition);
       }
     }
   }
 
   rv = AfterSetAttr(aNameSpaceID, aName, nullptr, &oldValue, aNotify);
   NS_ENSURE_SUCCESS(rv, rv);
 
   UpdateState(aNotify);
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -10243,24 +10243,26 @@ nsContentUtils::EnqueueUpgradeReaction(E
     doc->GetDocGroup()->CustomElementReactionsStack();
   stack->EnqueueUpgradeReaction(aElement, aDefinition);
 }
 
 /* static */ void
 nsContentUtils::EnqueueLifecycleCallback(nsIDocument::ElementCallbackType aType,
                                          Element* aCustomElement,
                                          LifecycleCallbackArgs* aArgs,
+                                         LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
                                          CustomElementDefinition* aDefinition)
 {
   // No DocGroup means no custom element reactions stack.
   if (!aCustomElement->OwnerDoc()->GetDocGroup()) {
     return;
   }
 
   CustomElementRegistry::EnqueueLifecycleCallback(aType, aCustomElement, aArgs,
+                                                  aAdoptedCallbackArgs,
                                                   aDefinition);
 }
 
 /* static */ void
 nsContentUtils::GetCustomPrototype(nsIDocument* aDoc,
                                    int32_t aNamespaceID,
                                    nsIAtom* aAtom,
                                    JS::MutableHandle<JSObject*> aPrototype)
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -132,16 +132,17 @@ class HTMLEditor;
 namespace dom {
 struct CustomElementDefinition;
 class DocumentFragment;
 class Element;
 class EventTarget;
 class IPCDataTransfer;
 class IPCDataTransferItem;
 struct LifecycleCallbackArgs;
+struct LifecycleAdoptedCallbackArgs;
 class NodeInfo;
 class nsIContentChild;
 class nsIContentParent;
 class TabChild;
 class Selection;
 class TabParent;
 } // namespace dom
 
@@ -3039,16 +3040,17 @@ public:
                                   mozilla::dom::CustomElementDefinition* aDefinition);
 
   static void EnqueueUpgradeReaction(Element* aElement,
                                      mozilla::dom::CustomElementDefinition* aDefinition);
 
   static void EnqueueLifecycleCallback(nsIDocument::ElementCallbackType aType,
                                        Element* aCustomElement,
                                        mozilla::dom::LifecycleCallbackArgs* aArgs = nullptr,
+                                       mozilla::dom::LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs = nullptr,
                                        mozilla::dom::CustomElementDefinition* aDefinition = nullptr);
 
   static void GetCustomPrototype(nsIDocument* aDoc,
                                  int32_t aNamespaceID,
                                  nsIAtom* aAtom,
                                  JS::MutableHandle<JSObject*> prototype);
 
   static bool AttemptLargeAllocationLoad(nsIHttpChannel* aChannel);
diff --git a/dom/base/nsIDocument.h b/dom/base/nsIDocument.h
--- a/dom/base/nsIDocument.h
+++ b/dom/base/nsIDocument.h
@@ -2819,16 +2819,17 @@ public:
   {
     return GetRootElement();
   }
 
   enum ElementCallbackType {
     eCreated,
     eConnected,
     eDisconnected,
+    eAdopted,
     eAttributeChanged
   };
 
   nsIDocument* GetTopLevelContentDocument();
 
   virtual void
     RegisterElement(JSContext* aCx, const nsAString& aName,
                     const mozilla::dom::ElementRegistrationOptions& aOptions,
diff --git a/dom/base/nsNodeUtils.cpp b/dom/base/nsNodeUtils.cpp
--- a/dom/base/nsNodeUtils.cpp
+++ b/dom/base/nsNodeUtils.cpp
@@ -513,16 +513,33 @@ nsNodeUtils::CloneAndAdopt(nsINode *aNod
 
     aNode->mNodeInfo.swap(newNodeInfo);
     if (elem) {
       elem->NodeInfoChanged(oldDoc);
     }
 
     nsIDocument* newDoc = aNode->OwnerDoc();
     if (newDoc) {
+      if (CustomElementRegistry::IsCustomElementEnabled()) {
+        // Adopted callback must be enqueued whenever a node’s
+        // shadow-including inclusive descendants that is custom.
+        Element* element = aNode->IsElement() ? aNode->AsElement() : nullptr;
+        if (element) {
+          RefPtr<CustomElementData> data = element->GetCustomElementData();
+          if (data && data->mState == CustomElementData::State::eCustom) {
+            LifecycleAdoptedCallbackArgs args = {
+              oldDoc,
+              newDoc
+            };
+            nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eAdopted,
+                                                     element, nullptr, &args);
+          }
+        }
+      }
+
       // XXX what if oldDoc is null, we don't know if this should be
       // registered or not! Can that really happen?
       if (wasRegistered) {
         newDoc->RegisterActivityObserver(aNode->AsElement());
       }
 
       if (nsPIDOMWindowInner* window = newDoc->GetInnerWindow()) {
         EventListenerManager* elm = aNode->GetExistingListenerManager();
diff --git a/dom/webidl/WebComponents.webidl b/dom/webidl/WebComponents.webidl
--- a/dom/webidl/WebComponents.webidl
+++ b/dom/webidl/WebComponents.webidl
@@ -8,24 +8,27 @@
  *
  * Copyright © 2012 W3C® (MIT, ERCIM, Keio), All Rights Reserved. W3C
  * liability, trademark and document use rules apply.
  */
 
 callback LifecycleCreatedCallback = void();
 callback LifecycleConnectedCallback = void();
 callback LifecycleDisconnectedCallback = void();
+callback LifecycleAdoptedCallback = void(Document? oldDocument,
+                                         Document? newDocment);
 callback LifecycleAttributeChangedCallback = void(DOMString attrName,
                                                   DOMString? oldValue,
                                                   DOMString? newValue,
                                                   DOMString? namespaceURI);
 
 dictionary LifecycleCallbacks {
   LifecycleCreatedCallback? createdCallback;
   LifecycleConnectedCallback? connectedCallback;
   LifecycleDisconnectedCallback? disconnectedCallback;
+  LifecycleAdoptedCallback? adoptedCallback;
   LifecycleAttributeChangedCallback? attributeChangedCallback;
 };
 
 dictionary ElementRegistrationOptions {
   object? prototype = null;
   DOMString? extends = null;
 };
diff --git a/testing/web-platform/meta/custom-elements/adopted-callback.html.ini b/testing/web-platform/meta/custom-elements/adopted-callback.html.ini
--- a/testing/web-platform/meta/custom-elements/adopted-callback.html.ini
+++ b/testing/web-platform/meta/custom-elements/adopted-callback.html.ini
@@ -1,166 +1,82 @@
 [adopted-callback.html]
   type: testharness
-  [Inserting a custom element into the document of the template elements must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving a custom element from the owner document into the document of the template elements must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Inserting an ancestor of custom element into the document of the template elements must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving an ancestor of custom element from the owner document into the document of the template elements must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
   [Inserting a custom element into a shadow tree in the document of the template elements must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting the shadow host of a custom element into the document of the template elements must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Moving the shadow host of a custom element from the owner document into the document of the template elements must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting a custom element into a detached shadow tree that belongs to the document of the template elements must enqueue and invoke adoptedCallback]
     expected: FAIL
 
-  [Inserting a custom element into a new document must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving a custom element from the owner document into a new document must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Inserting an ancestor of custom element into a new document must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving an ancestor of custom element from the owner document into a new document must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
   [Inserting a custom element into a shadow tree in a new document must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting the shadow host of a custom element into a new document must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Moving the shadow host of a custom element from the owner document into a new document must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting a custom element into a detached shadow tree that belongs to a new document must enqueue and invoke adoptedCallback]
     expected: FAIL
 
-  [Inserting a custom element into a cloned document must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving a custom element from the owner document into a cloned document must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Inserting an ancestor of custom element into a cloned document must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving an ancestor of custom element from the owner document into a cloned document must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
   [Inserting a custom element into a shadow tree in a cloned document must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting the shadow host of a custom element into a cloned document must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Moving the shadow host of a custom element from the owner document into a cloned document must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting a custom element into a detached shadow tree that belongs to a cloned document must enqueue and invoke adoptedCallback]
     expected: FAIL
 
-  [Inserting a custom element into a document created by createHTMLDocument must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving a custom element from the owner document into a document created by createHTMLDocument must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Inserting an ancestor of custom element into a document created by createHTMLDocument must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving an ancestor of custom element from the owner document into a document created by createHTMLDocument must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
   [Inserting a custom element into a shadow tree in a document created by createHTMLDocument must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting the shadow host of a custom element into a document created by createHTMLDocument must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Moving the shadow host of a custom element from the owner document into a document created by createHTMLDocument must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting a custom element into a detached shadow tree that belongs to a document created by createHTMLDocument must enqueue and invoke adoptedCallback]
     expected: FAIL
 
-  [Inserting a custom element into an HTML document created by createDocument must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving a custom element from the owner document into an HTML document created by createDocument must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Inserting an ancestor of custom element into an HTML document created by createDocument must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving an ancestor of custom element from the owner document into an HTML document created by createDocument must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
   [Inserting a custom element into a shadow tree in an HTML document created by createDocument must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting the shadow host of a custom element into an HTML document created by createDocument must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Moving the shadow host of a custom element from the owner document into an HTML document created by createDocument must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting a custom element into a detached shadow tree that belongs to an HTML document created by createDocument must enqueue and invoke adoptedCallback]
     expected: FAIL
 
-  [Inserting a custom element into the document of an iframe must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving a custom element from the owner document into the document of an iframe must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Inserting an ancestor of custom element into the document of an iframe must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving an ancestor of custom element from the owner document into the document of an iframe must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
   [Inserting a custom element into a shadow tree in the document of an iframe must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting the shadow host of a custom element into the document of an iframe must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Moving the shadow host of a custom element from the owner document into the document of an iframe must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting a custom element into a detached shadow tree that belongs to the document of an iframe must enqueue and invoke adoptedCallback]
     expected: FAIL
 
-  [Inserting a custom element into an HTML document fetched by XHR must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving a custom element from the owner document into an HTML document fetched by XHR must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Inserting an ancestor of custom element into an HTML document fetched by XHR must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
-  [Moving an ancestor of custom element from the owner document into an HTML document fetched by XHR must enqueue and invoke adoptedCallback]
-    expected: FAIL
-
   [Inserting a custom element into a shadow tree in an HTML document fetched by XHR must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Inserting the shadow host of a custom element into an HTML document fetched by XHR must enqueue and invoke adoptedCallback]
     expected: FAIL
 
   [Moving the shadow host of a custom element from the owner document into an HTML document fetched by XHR must enqueue and invoke adoptedCallback]
     expected: FAIL
diff --git a/testing/web-platform/meta/custom-elements/custom-element-reaction-queue.html.ini b/testing/web-platform/meta/custom-elements/custom-element-reaction-queue.html.ini
deleted file mode 100644
--- a/testing/web-platform/meta/custom-elements/custom-element-reaction-queue.html.ini
+++ /dev/null
@@ -1,5 +0,0 @@
-[custom-element-reaction-queue.html]
-  type: testharness
-  [Mutating another custom element inside adopted callback must invoke all pending callbacks on the mutated element]
-    expected: FAIL
-
diff --git a/testing/web-platform/meta/custom-elements/reactions/ChildNode.html.ini b/testing/web-platform/meta/custom-elements/reactions/ChildNode.html.ini
deleted file mode 100644
--- a/testing/web-platform/meta/custom-elements/reactions/ChildNode.html.ini
+++ /dev/null
@@ -1,10 +0,0 @@
-[ChildNode.html]
-  type: testharness
-  [before on ChildNode must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
-
-  [after on ChildNode must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
-
-  [replaceWith on ChildNode must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
diff --git a/testing/web-platform/meta/custom-elements/reactions/Document.html.ini b/testing/web-platform/meta/custom-elements/reactions/Document.html.ini
--- a/testing/web-platform/meta/custom-elements/reactions/Document.html.ini
+++ b/testing/web-platform/meta/custom-elements/reactions/Document.html.ini
@@ -1,16 +1,13 @@
 [Document.html]
   type: testharness
   [importNode on Document must construct a new custom element when importing a custom element]
     expected: FAIL
 
-  [adoptNode on Document must enqueue an adopted reaction when importing a custom element]
-    expected: FAIL
-
   [importNode on Document must construct a new custom element when importing a custom element from a template]
     expected: FAIL
 
   [execCommand on Document must enqueue a disconnected reaction when deleting a custom element from a contenteditable element]
     expected: FAIL
 
   [body on Document must enqueue disconnectedCallback when removing a custom element]
     expected: FAIL
diff --git a/testing/web-platform/meta/custom-elements/reactions/Element.html.ini b/testing/web-platform/meta/custom-elements/reactions/Element.html.ini
--- a/testing/web-platform/meta/custom-elements/reactions/Element.html.ini
+++ b/testing/web-platform/meta/custom-elements/reactions/Element.html.ini
@@ -14,19 +14,16 @@
 
   [removeAttributeNode on Element must not enqueue an attributeChanged reaction when removing an attribute that does not exist]
     expected: FAIL
 
   [undefined must enqueue a connected reaction]
     expected: FAIL
 
   [undefined must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL 
-
-  [insertAdjacentElement on Element must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
     expected: FAIL
 
   [innerHTML on Element must enqueue a connected reaction for a newly constructed custom element]
     expected: FAIL
 
   [innerHTML on Element must enqueue a attributeChanged reaction for a newly constructed custom element]
     expected: FAIL
 
diff --git a/testing/web-platform/meta/custom-elements/reactions/Node.html.ini b/testing/web-platform/meta/custom-elements/reactions/Node.html.ini
--- a/testing/web-platform/meta/custom-elements/reactions/Node.html.ini
+++ b/testing/web-platform/meta/custom-elements/reactions/Node.html.ini
@@ -3,17 +3,8 @@
   [cloneNode on Node must enqueue an attributeChanged reaction when cloning an element with an observed attribute]
     expected: FAIL
 
   [cloneNode on Node must not enqueue an attributeChanged reaction when cloning an element with an unobserved attribute]
     expected: FAIL
 
   [cloneNode on Node must enqueue an attributeChanged reaction when cloning an element only for observed attributes]
     expected: FAIL
-
-  [insertBefore on ChildNode must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
-
-  [appendChild on ChildNode must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
-
-  [replaceChild on ChildNode must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
diff --git a/testing/web-platform/meta/custom-elements/reactions/ParentNode.html.ini b/testing/web-platform/meta/custom-elements/reactions/ParentNode.html.ini
deleted file mode 100644
--- a/testing/web-platform/meta/custom-elements/reactions/ParentNode.html.ini
+++ /dev/null
@@ -1,8 +0,0 @@
-[ParentNode.html]
-  type: testharness
-  [prepend on ParentNode must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
-
-  [append on ParentNode must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
-
diff --git a/testing/web-platform/meta/custom-elements/reactions/Range.html.ini b/testing/web-platform/meta/custom-elements/reactions/Range.html.ini
--- a/testing/web-platform/meta/custom-elements/reactions/Range.html.ini
+++ b/testing/web-platform/meta/custom-elements/reactions/Range.html.ini
@@ -4,17 +4,11 @@
     expected: FAIL
 
   [cloneContents on Range must not enqueue an attributeChanged reaction when cloning an element with an unobserved attribute]
     expected: FAIL
 
   [cloneContents on Range must enqueue an attributeChanged reaction when cloning an element only for observed attributes]
     expected: FAIL
 
-  [insertNode on Range must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
-
-  [surroundContents on Range must enqueue a disconnected reaction, an adopted reaction, and a connected reaction when the custom element was in another document]
-    expected: FAIL
-
   [createContextualFragment on Range must construct a custom element]
     expected: FAIL
 
