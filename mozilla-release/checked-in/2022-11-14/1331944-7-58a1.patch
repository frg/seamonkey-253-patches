# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1509383429 14400
# Node ID 8bb3b0587f93502631c566fb4edeec9b8a13a584
# Parent  3741bc7ff417fc5e749e40b2baae51857eb8cf9d
Bug 1331944 - Part 7. Handle shared surfaces in WebRenderBridgeParent::AddExternalImage. r=jrmuizel

diff --git a/gfx/layers/wr/WebRenderBridgeParent.cpp b/gfx/layers/wr/WebRenderBridgeParent.cpp
--- a/gfx/layers/wr/WebRenderBridgeParent.cpp
+++ b/gfx/layers/wr/WebRenderBridgeParent.cpp
@@ -19,16 +19,17 @@
 #include "mozilla/layers/APZCTreeManager.h"
 #include "mozilla/layers/Compositor.h"
 #include "mozilla/layers/CompositorBridgeParent.h"
 #include "mozilla/layers/CompositorThread.h"
 #include "mozilla/layers/CompositorVsyncScheduler.h"
 #include "mozilla/layers/ImageBridgeParent.h"
 #include "mozilla/layers/ImageDataSerializer.h"
 #include "mozilla/layers/IpcResourceUpdateQueue.h"
+#include "mozilla/layers/SharedSurfacesParent.h"
 #include "mozilla/layers/TextureHost.h"
 #include "mozilla/layers/AsyncImagePipelineManager.h"
 #include "mozilla/layers/WebRenderImageHost.h"
 #include "mozilla/layers/WebRenderTextureHost.h"
 #include "mozilla/TimeStamp.h"
 #include "mozilla/Unused.h"
 #include "mozilla/webrender/RenderThread.h"
 #include "mozilla/widget/CompositorWidget.h"
@@ -353,37 +354,51 @@ bool
 WebRenderBridgeParent::AddExternalImage(wr::ExternalImageId aExtId, wr::ImageKey aKey,
                                         wr::ResourceUpdateQueue& aResources)
 {
   Range<wr::ImageKey> keys(&aKey, 1);
   // Check if key is obsoleted.
   if (keys[0].mNamespace != mIdNamespace) {
     return true;
   }
-  MOZ_ASSERT(mExternalImageIds.Get(wr::AsUint64(aExtId)).get());
 
-  RefPtr<WebRenderImageHost> host = mExternalImageIds.Get(wr::AsUint64(aExtId));
-  if (!host) {
-    NS_ERROR("CompositableHost does not exist");
-    return false;
-  }
-  if (!gfxEnv::EnableWebRenderRecording()) {
-    TextureHost* texture = host->GetAsTextureHostForComposite();
-    if (!texture) {
-      NS_ERROR("TextureHost does not exist");
+  RefPtr<DataSourceSurface> dSurf = SharedSurfacesParent::Acquire(aExtId);
+  if (dSurf) {
+    if (!gfxEnv::EnableWebRenderRecording()) {
+      wr::ImageDescriptor descriptor(dSurf->GetSize(), dSurf->Stride(),
+                                     dSurf->GetFormat());
+      aResources.AddExternalImage(aKey, descriptor, aExtId,
+                                  wr::WrExternalImageBufferType::ExternalBuffer,
+                                  0);
+      return true;
+    }
+  } else {
+    MOZ_ASSERT(mExternalImageIds.Get(wr::AsUint64(aExtId)).get());
+
+    RefPtr<WebRenderImageHost> host = mExternalImageIds.Get(wr::AsUint64(aExtId));
+    if (!host) {
+      NS_ERROR("CompositableHost does not exist");
       return false;
     }
-    WebRenderTextureHost* wrTexture = texture->AsWebRenderTextureHost();
-    if (wrTexture) {
-      wrTexture->PushResourceUpdates(aResources, TextureHost::ADD_IMAGE, keys,
-                                     wrTexture->GetExternalImageKey());
-      return true;
+    if (!gfxEnv::EnableWebRenderRecording()) {
+      TextureHost* texture = host->GetAsTextureHostForComposite();
+      if (!texture) {
+        NS_ERROR("TextureHost does not exist");
+        return false;
+      }
+      WebRenderTextureHost* wrTexture = texture->AsWebRenderTextureHost();
+      if (wrTexture) {
+        wrTexture->PushResourceUpdates(aResources, TextureHost::ADD_IMAGE, keys,
+                                       wrTexture->GetExternalImageKey());
+        return true;
+      }
     }
+    dSurf = host->GetAsSurface();
   }
-  RefPtr<DataSourceSurface> dSurf = host->GetAsSurface();
+
   if (!dSurf) {
     NS_ERROR("TextureHost does not return DataSourceSurface");
     return false;
   }
 
   DataSourceSurface::MappedSurface map;
   if (!dSurf->Map(gfx::DataSourceSurface::MapType::READ, &map)) {
     NS_ERROR("DataSourceSurface failed to map");
@@ -839,16 +854,21 @@ WebRenderBridgeParent::RecvAddExternalIm
 }
 
 mozilla::ipc::IPCResult
 WebRenderBridgeParent::RecvRemoveExternalImageId(const ExternalImageId& aImageId)
 {
   if (mDestroyed) {
     return IPC_OK();
   }
+
+  if (SharedSurfacesParent::Release(aImageId)) {
+    return IPC_OK();
+  }
+
   WebRenderImageHost* wrHost = mExternalImageIds.Get(wr::AsUint64(aImageId)).get();
   if (!wrHost) {
     return IPC_OK();
   }
 
   wrHost->ClearWrBridge();
   mExternalImageIds.Remove(wr::AsUint64(aImageId));
 

