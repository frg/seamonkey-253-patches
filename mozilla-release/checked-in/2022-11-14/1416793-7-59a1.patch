# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1510675304 28800
#      Tue Nov 14 08:01:44 2017 -0800
# Node ID 96fdf13f21180ba7e77b2ed95cc93b9e22cc721b
# Parent  87b2687468e682ab0c1e78eca3fd291f67846c6f
Bug 1416793 - Part 7: Inline FunctionContextFlags into FunctionBox. r=jandem

diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -477,17 +477,23 @@ FunctionBox::FunctionBox(JSContext* cx, 
     wasEmitted(false),
     declaredArguments(false),
     usesArguments(false),
     usesApply(false),
     usesThis(false),
     usesReturn(false),
     hasRest_(false),
     isExprBody_(false),
-    funCxFlags()
+    hasExtensibleScope_(false),
+    argumentsHasLocalBinding_(false),
+    definitelyNeedsArgsObj_(false),
+    needsHomeObject_(false),
+    isDerivedClassConstructor_(false),
+    hasThisBinding_(false),
+    hasInnerFunctions_(false)
 {
     // Functions created at parse time may be set singleton after parsing and
     // baked into JIT code, so they must be allocated tenured. They are held by
     // the JSScript so cannot be collected during a minor GC anyway.
     MOZ_ASSERT(fun->isTenured());
 }
 
 void
diff --git a/js/src/frontend/SharedContext.h b/js/src/frontend/SharedContext.h
--- a/js/src/frontend/SharedContext.h
+++ b/js/src/frontend/SharedContext.h
@@ -56,86 +56,16 @@ StatementKindIsLoop(StatementKind kind)
 }
 
 static inline bool
 StatementKindIsUnlabeledBreakTarget(StatementKind kind)
 {
     return StatementKindIsLoop(kind) || kind == StatementKind::Switch;
 }
 
-class FunctionContextFlags
-{
-    // This class's data is all private and so only visible to these friends.
-    friend class FunctionBox;
-
-    // This function does something that can extend the set of bindings in its
-    // call objects --- it does a direct eval in non-strict code, or includes a
-    // function statement (as opposed to a function definition).
-    //
-    // This flag is *not* inherited by enclosed or enclosing functions; it
-    // applies only to the function in whose flags it appears.
-    //
-    bool hasExtensibleScope:1;
-
-    // Technically, every function has a binding named 'arguments'. Internally,
-    // this binding is only added when 'arguments' is mentioned by the function
-    // body. This flag indicates whether 'arguments' has been bound either
-    // through implicit use:
-    //   function f() { return arguments }
-    // or explicit redeclaration:
-    //   function f() { var arguments; return arguments }
-    //
-    // Note 1: overwritten arguments (function() { arguments = 3 }) will cause
-    // this flag to be set but otherwise require no special handling:
-    // 'arguments' is just a local variable and uses of 'arguments' will just
-    // read the local's current slot which may have been assigned. The only
-    // special semantics is that the initial value of 'arguments' is the
-    // arguments object (not undefined, like normal locals).
-    //
-    // Note 2: if 'arguments' is bound as a formal parameter, there will be an
-    // 'arguments' in Bindings, but, as the "LOCAL" in the name indicates, this
-    // flag will not be set. This is because, as a formal, 'arguments' will
-    // have no special semantics: the initial value is unconditionally the
-    // actual argument (or undefined if nactual < nformal).
-    //
-    bool argumentsHasLocalBinding:1;
-
-    // In many cases where 'arguments' has a local binding (as described above)
-    // we do not need to actually create an arguments object in the function
-    // prologue: instead we can analyze how 'arguments' is used (using the
-    // simple dataflow analysis in analyzeSSA) to determine that uses of
-    // 'arguments' can just read from the stack frame directly. However, the
-    // dataflow analysis only looks at how JSOP_ARGUMENTS is used, so it will
-    // be unsound in several cases. The frontend filters out such cases by
-    // setting this flag which eagerly sets script->needsArgsObj to true.
-    //
-    bool definitelyNeedsArgsObj:1;
-
-    bool needsHomeObject:1;
-    bool isDerivedClassConstructor:1;
-
-    // Whether this function has a .this binding. If true, we need to emit
-    // JSOP_FUNCTIONTHIS in the prologue to initialize it.
-    bool hasThisBinding:1;
-
-    // Whether this function has nested functions.
-    bool hasInnerFunctions:1;
-
-  public:
-    FunctionContextFlags()
-     :  hasExtensibleScope(false),
-        argumentsHasLocalBinding(false),
-        definitelyNeedsArgsObj(false),
-        needsHomeObject(false),
-        isDerivedClassConstructor(false),
-        hasThisBinding(false),
-        hasInnerFunctions(false)
-    { }
-};
-
 // List of directives that may be encountered in a Directive Prologue (ES5 15.1).
 class Directives
 {
     bool strict_;
     bool asmJS_;
 
   public:
     explicit Directives(bool strict) : strict_(strict), asmJS_(false) {}
@@ -408,17 +338,68 @@ class FunctionBox : public ObjectBox, pu
     bool            usesApply:1;            /* contains an f.apply() call */
     bool            usesThis:1;             /* contains 'this' */
     bool            usesReturn:1;           /* contains a 'return' statement */
     bool            hasRest_:1;             /* has rest parameter */
     bool            isExprBody_:1;          /* arrow function with expression
                                              * body or expression closure:
                                              * function(x) x*x */
 
-    FunctionContextFlags funCxFlags;
+    // This function does something that can extend the set of bindings in its
+    // call objects --- it does a direct eval in non-strict code, or includes a
+    // function statement (as opposed to a function definition).
+    //
+    // This flag is *not* inherited by enclosed or enclosing functions; it
+    // applies only to the function in whose flags it appears.
+    //
+    bool hasExtensibleScope_:1;
+
+    // Technically, every function has a binding named 'arguments'. Internally,
+    // this binding is only added when 'arguments' is mentioned by the function
+    // body. This flag indicates whether 'arguments' has been bound either
+    // through implicit use:
+    //   function f() { return arguments }
+    // or explicit redeclaration:
+    //   function f() { var arguments; return arguments }
+    //
+    // Note 1: overwritten arguments (function() { arguments = 3 }) will cause
+    // this flag to be set but otherwise require no special handling:
+    // 'arguments' is just a local variable and uses of 'arguments' will just
+    // read the local's current slot which may have been assigned. The only
+    // special semantics is that the initial value of 'arguments' is the
+    // arguments object (not undefined, like normal locals).
+    //
+    // Note 2: if 'arguments' is bound as a formal parameter, there will be an
+    // 'arguments' in Bindings, but, as the "LOCAL" in the name indicates, this
+    // flag will not be set. This is because, as a formal, 'arguments' will
+    // have no special semantics: the initial value is unconditionally the
+    // actual argument (or undefined if nactual < nformal).
+    //
+    bool argumentsHasLocalBinding_:1;
+
+    // In many cases where 'arguments' has a local binding (as described above)
+    // we do not need to actually create an arguments object in the function
+    // prologue: instead we can analyze how 'arguments' is used (using the
+    // simple dataflow analysis in analyzeSSA) to determine that uses of
+    // 'arguments' can just read from the stack frame directly. However, the
+    // dataflow analysis only looks at how JSOP_ARGUMENTS is used, so it will
+    // be unsound in several cases. The frontend filters out such cases by
+    // setting this flag which eagerly sets script->needsArgsObj to true.
+    //
+    bool definitelyNeedsArgsObj_:1;
+
+    bool needsHomeObject_:1;
+    bool isDerivedClassConstructor_:1;
+
+    // Whether this function has a .this binding. If true, we need to emit
+    // JSOP_FUNCTIONTHIS in the prologue to initialize it.
+    bool hasThisBinding_:1;
+
+    // Whether this function has nested functions.
+    bool hasInnerFunctions_:1;
 
     FunctionBox(JSContext* cx, ObjectBox* traceListHead, JSFunction* fun,
                 uint32_t toStringStart, Directives directives, bool extraWarnings,
                 GeneratorKind generatorKind, FunctionAsyncKind asyncKind);
 
     MutableHandle<LexicalScope::Data*> namedLambdaBindings() {
         MOZ_ASSERT(context->keepAtoms);
         return MutableHandle<LexicalScope::Data*>::fromMarkedLocation(&namedLambdaBindings_);
@@ -499,34 +480,34 @@ class FunctionBox : public ObjectBox, pu
         hasRest_ = true;
     }
 
     bool isExprBody() const { return isExprBody_; }
     void setIsExprBody() {
         isExprBody_ = true;
     }
 
-    bool hasExtensibleScope()        const { return funCxFlags.hasExtensibleScope; }
-    bool hasThisBinding()            const { return funCxFlags.hasThisBinding; }
-    bool argumentsHasLocalBinding()  const { return funCxFlags.argumentsHasLocalBinding; }
-    bool definitelyNeedsArgsObj()    const { return funCxFlags.definitelyNeedsArgsObj; }
-    bool needsHomeObject()           const { return funCxFlags.needsHomeObject; }
-    bool isDerivedClassConstructor() const { return funCxFlags.isDerivedClassConstructor; }
-    bool hasInnerFunctions()         const { return funCxFlags.hasInnerFunctions; }
+    bool hasExtensibleScope()        const { return hasExtensibleScope_; }
+    bool hasThisBinding()            const { return hasThisBinding_; }
+    bool argumentsHasLocalBinding()  const { return argumentsHasLocalBinding_; }
+    bool definitelyNeedsArgsObj()    const { return definitelyNeedsArgsObj_; }
+    bool needsHomeObject()           const { return needsHomeObject_; }
+    bool isDerivedClassConstructor() const { return isDerivedClassConstructor_; }
+    bool hasInnerFunctions()         const { return hasInnerFunctions_; }
 
-    void setHasExtensibleScope()           { funCxFlags.hasExtensibleScope       = true; }
-    void setHasThisBinding()               { funCxFlags.hasThisBinding           = true; }
-    void setArgumentsHasLocalBinding()     { funCxFlags.argumentsHasLocalBinding = true; }
-    void setDefinitelyNeedsArgsObj()       { MOZ_ASSERT(funCxFlags.argumentsHasLocalBinding);
-                                             funCxFlags.definitelyNeedsArgsObj   = true; }
+    void setHasExtensibleScope()           { hasExtensibleScope_       = true; }
+    void setHasThisBinding()               { hasThisBinding_           = true; }
+    void setArgumentsHasLocalBinding()     { argumentsHasLocalBinding_ = true; }
+    void setDefinitelyNeedsArgsObj()       { MOZ_ASSERT(argumentsHasLocalBinding_);
+                                             definitelyNeedsArgsObj_   = true; }
     void setNeedsHomeObject()              { MOZ_ASSERT(function()->allowSuperProperty());
-                                             funCxFlags.needsHomeObject          = true; }
+                                             needsHomeObject_          = true; }
     void setDerivedClassConstructor()      { MOZ_ASSERT(function()->isClassConstructor());
-                                             funCxFlags.isDerivedClassConstructor = true; }
-    void setHasInnerFunctions()            { funCxFlags.hasInnerFunctions         = true; }
+                                             isDerivedClassConstructor_ = true; }
+    void setHasInnerFunctions()            { hasInnerFunctions_         = true; }
 
     bool hasSimpleParameterList() const {
         return !hasRest() && !hasParameterExprs && !hasDestructuringArgs;
     }
 
     bool hasMappedArgsObj() const {
         return !strict() && hasSimpleParameterList();
     }
diff --git a/js/src/jsscript.h b/js/src/jsscript.h
--- a/js/src/jsscript.h
+++ b/js/src/jsscript.h
@@ -1024,17 +1024,17 @@ class JSScript : public js::gc::TenuredC
     // True if the script has a non-syntactic scope on its dynamic scope chain.
     // That is, there are objects about which we know nothing between the
     // outermost syntactic scope and the global.
     bool hasNonSyntacticScope_:1;
 
     // see Parser::selfHostingMode.
     bool selfHosted_:1;
 
-    // See FunctionContextFlags.
+    // See FunctionBox.
     bool bindingsAccessedDynamically_:1;
     bool funHasExtensibleScope_:1;
 
     // True if any formalIsAliased(i).
     bool funHasAnyAliasedFormal_:1;
 
     // Have warned about uses of undefined properties in this script.
     bool warnedAboutUndefinedProp_:1;
