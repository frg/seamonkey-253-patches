# HG changeset patch
# User Edgar Chen <top12345tw@gmail.com>
# Date 1516172113 -28800
# Node ID 53d7ccf627faa0f95c65dbe599aba1c53f600c15
# Parent  4ab7f4f081a6a9bee010e6a4ffb9d9f3341e79be
Bug 1430951 - Avoid element name atomizing to improve performance of LookupCustomElementDefinition; r=smaug

Since we are dealing with the element (nodeInfo->LocalName() and NameAtom() are the same value),
we could use nodeInfo->NameAtom() instead.

MozReview-Commit-ID: 4vIBDEM1Nwv

diff --git a/dom/base/CustomElementRegistry.cpp b/dom/base/CustomElementRegistry.cpp
--- a/dom/base/CustomElementRegistry.cpp
+++ b/dom/base/CustomElementRegistry.cpp
@@ -234,22 +234,21 @@ CustomElementRegistry::CustomElementRegi
 }
 
 CustomElementRegistry::~CustomElementRegistry()
 {
   mozilla::DropJSObjects(this);
 }
 
 CustomElementDefinition*
-CustomElementRegistry::LookupCustomElementDefinition(const nsAString& aLocalName,
+CustomElementRegistry::LookupCustomElementDefinition(nsIAtom* aNameAtom,
                                                      nsIAtom* aTypeAtom) const
 {
-  nsCOMPtr<nsIAtom> localNameAtom = NS_Atomize(aLocalName);
   CustomElementDefinition* data = mCustomDefinitions.GetWeak(aTypeAtom);
-  if (data && data->mLocalName == localNameAtom) {
+  if (data && data->mLocalName == aNameAtom) {
     return data;
   }
 
   return nullptr;
 }
 
 CustomElementDefinition*
 CustomElementRegistry::LookupCustomElementDefinition(JSContext* aCx,
diff --git a/dom/base/CustomElementRegistry.h b/dom/base/CustomElementRegistry.h
--- a/dom/base/CustomElementRegistry.h
+++ b/dom/base/CustomElementRegistry.h
@@ -413,17 +413,17 @@ public:
 
   explicit CustomElementRegistry(nsPIDOMWindowInner* aWindow);
 
   /**
    * Looking up a custom element definition.
    * https://html.spec.whatwg.org/#look-up-a-custom-element-definition
    */
   CustomElementDefinition* LookupCustomElementDefinition(
-    const nsAString& aLocalName, nsIAtom* aTypeAtom) const;
+    nsIAtom* aNameAtom, nsIAtom* aTypeAtom) const;
 
   CustomElementDefinition* LookupCustomElementDefinition(
     JSContext* aCx, JSObject *aConstructor) const;
 
   static void EnqueueLifecycleCallback(nsIDocument::ElementCallbackType aType,
                                        Element* aCustomElement,
                                        LifecycleCallbackArgs* aArgs,
                                        LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -10117,19 +10117,21 @@ nsContentUtils::HttpsStateIsModern(nsIDo
 }
 
 /* static */ void
 nsContentUtils::TryToUpgradeElement(Element* aElement)
 {
   NodeInfo* nodeInfo = aElement->NodeInfo();
   nsCOMPtr<nsIAtom> typeAtom =
     aElement->GetCustomElementData()->GetCustomElementType();
+
+  MOZ_ASSERT(nodeInfo->NameAtom()->Equals(nodeInfo->LocalName()));
   CustomElementDefinition* definition =
     nsContentUtils::LookupCustomElementDefinition(nodeInfo->GetDocument(),
-                                                  nodeInfo->LocalName(),
+                                                  nodeInfo->NameAtom(),
                                                   nodeInfo->NamespaceID(),
                                                   typeAtom);
   if (definition) {
     nsContentUtils::EnqueueUpgradeReaction(aElement, definition);
   } else {
     // Add an unresolved custom element that is a candidate for
     // upgrade when a custom element is connected to the document.
     // We will make sure it's shadow-including tree order in bug 1326028.
@@ -10203,19 +10205,20 @@ nsContentUtils::NewXULOrHTMLElement(Elem
 
   // https://dom.spec.whatwg.org/#concept-create-element
   // We only handle the "synchronous custom elements flag is set" now.
   // For the unset case (e.g. cloning a node), see bug 1319342 for that.
   // Step 4.
   CustomElementDefinition* definition = aDefinition;
   if (CustomElementRegistry::IsCustomElementEnabled() && isCustomElement &&
       !definition) {
+    MOZ_ASSERT(nodeInfo->NameAtom()->Equals(nodeInfo->LocalName()));
     definition =
       nsContentUtils::LookupCustomElementDefinition(nodeInfo->GetDocument(),
-                                                    nodeInfo->LocalName(),
+                                                    nodeInfo->NameAtom(),
                                                     nodeInfo->NamespaceID(),
                                                     typeAtom);
   }
 
   // It might be a problem that parser synchronously calls constructor, so filed
   // bug 1378079 to figure out what we should do for parser case.
   if (definition) {
     /*
@@ -10317,17 +10320,17 @@ nsContentUtils::NewXULOrHTMLElement(Elem
     (*aResult)->SetCustomElementData(new CustomElementData(typeAtom));
   }
 
   return NS_OK;
 }
 
 /* static */ CustomElementDefinition*
 nsContentUtils::LookupCustomElementDefinition(nsIDocument* aDoc,
-                                              const nsAString& aLocalName,
+                                              nsIAtom* aNameAtom,
                                               uint32_t aNameSpaceID,
                                               nsIAtom* aTypeAtom)
 {
   MOZ_ASSERT(aDoc);
 
   if ((aNameSpaceID != kNameSpaceID_XUL &&
        aNameSpaceID != kNameSpaceID_XHTML) ||
       !aDoc->GetDocShell()) {
@@ -10339,17 +10342,17 @@ nsContentUtils::LookupCustomElementDefin
     return nullptr;
   }
 
   RefPtr<CustomElementRegistry> registry(window->CustomElements());
   if (!registry) {
     return nullptr;
   }
 
-  return registry->LookupCustomElementDefinition(aLocalName, aTypeAtom);
+  return registry->LookupCustomElementDefinition(aNameAtom, aTypeAtom);
 }
 
 /* static */ void
 nsContentUtils::RegisterUnresolvedElement(Element* aElement, nsIAtom* aTypeName)
 {
   MOZ_ASSERT(aElement);
 
   nsIDocument* doc = aElement->OwnerDoc();
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -3037,17 +3037,17 @@ public:
                                       mozilla::dom::CustomElementDefinition* aDefinition);
 
   /**
    * Looking up a custom element definition.
    * https://html.spec.whatwg.org/#look-up-a-custom-element-definition
    */
   static mozilla::dom::CustomElementDefinition*
     LookupCustomElementDefinition(nsIDocument* aDoc,
-                                  const nsAString& aLocalName,
+                                  nsIAtom* aNameAtom,
                                   uint32_t aNameSpaceID,
                                   nsIAtom* aTypeAtom);
 
   static void RegisterUnresolvedElement(Element* aElement, nsIAtom* aTypeName);
   static void UnregisterUnresolvedElement(Element* aElement);
 
   static mozilla::dom::CustomElementDefinition*
   GetElementDefinitionIfObservingAttr(Element* aCustomElement,
diff --git a/dom/base/nsNodeUtils.cpp b/dom/base/nsNodeUtils.cpp
--- a/dom/base/nsNodeUtils.cpp
+++ b/dom/base/nsNodeUtils.cpp
@@ -482,19 +482,21 @@ nsNodeUtils::CloneAndAdopt(nsINode *aNod
           !extension.IsEmpty()) {
         // The typeAtom can be determined by extension, because we only need to
         // consider two cases: 1) Original node is a autonomous custom element
         // which has CustomElementData. 2) Original node is a built-in custom
         // element or normal element, but it has `is` attribute when it is being
         // cloned.
         nsCOMPtr<nsIAtom> typeAtom = extension.IsEmpty() ? tagAtom : NS_Atomize(extension);
         cloneElem->SetCustomElementData(new CustomElementData(typeAtom));
+
+        MOZ_ASSERT(nodeInfo->NameAtom()->Equals(nodeInfo->LocalName()));
         CustomElementDefinition* definition =
           nsContentUtils::LookupCustomElementDefinition(nodeInfo->GetDocument(),
-                                                        nodeInfo->LocalName(),
+                                                        nodeInfo->NameAtom(),
                                                         nodeInfo->NamespaceID(),
                                                         typeAtom);
         if (definition) {
           nsContentUtils::EnqueueUpgradeReaction(cloneElem, definition);
         }
       }
     }
 
diff --git a/parser/html/nsHtml5TreeOperation.cpp b/parser/html/nsHtml5TreeOperation.cpp
--- a/parser/html/nsHtml5TreeOperation.cpp
+++ b/parser/html/nsHtml5TreeOperation.cpp
@@ -408,18 +408,19 @@ nsHtml5TreeOperation::CreateHTMLElement(
     }
 
     isCustomElement = (aCreator == NS_NewCustomElement || isAtom);
     if (isCustomElement && aFromParser != dom::FROM_PARSER_FRAGMENT) {
       nsCOMPtr<nsIAtom> tagAtom = nodeInfo->NameAtom();
       nsCOMPtr<nsIAtom> typeAtom =
         (aCreator == NS_NewCustomElement) ? tagAtom : isAtom;
 
+      MOZ_ASSERT(nodeInfo->NameAtom()->Equals(nodeInfo->LocalName()));
       definition = nsContentUtils::LookupCustomElementDefinition(document,
-        nodeInfo->LocalName(), nodeInfo->NamespaceID(), typeAtom);
+        nodeInfo->NameAtom(), nodeInfo->NamespaceID(), typeAtom);
 
       if (definition) {
         willExecuteScript = true;
       }
     }
   }
 
   if (willExecuteScript) { // This will cause custom element constructors to run
