# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1508851493 14400
#      Tue Oct 24 09:24:53 2017 -0400
# Node ID f16311e84f0d54ef8d2597ebefb2b9fe9b6c3a0a
# Parent  48844408c4041dd7194be50ae6cc9913b7502b48
Bug 1410634 - Part 1: Call channel IsFromCache() during OnStartRequest() to determine if update result came from http cache or network. r=tt, a=ritu

diff --git a/dom/workers/ServiceWorkerScriptCache.cpp b/dom/workers/ServiceWorkerScriptCache.cpp
--- a/dom/workers/ServiceWorkerScriptCache.cpp
+++ b/dom/workers/ServiceWorkerScriptCache.cpp
@@ -95,22 +95,23 @@ public:
   NS_DECL_NSISTREAMLOADEROBSERVER
   NS_DECL_NSIREQUESTOBSERVER
 
   CompareNetwork(CompareManager* aManager,
                  ServiceWorkerRegistrationInfo* aRegistration,
                  bool aIsMainScript)
     : mManager(aManager)
     , mRegistration(aRegistration)
-    , mIsMainScript(aIsMainScript)
     , mInternalHeaders(new InternalHeaders())
     , mLoadFlags(nsIChannel::LOAD_BYPASS_SERVICE_WORKER)
     , mState(WaitingForInitialization)
     , mNetworkResult(NS_OK)
     , mCacheResult(NS_OK)
+    , mIsMainScript(aIsMainScript)
+    , mIsFromCache(false)
   {
     MOZ_ASSERT(aManager);
     AssertIsOnMainThread();
   }
 
   nsresult
   Initialize(nsIPrincipal* aPrincipal,
              const nsAString& aURL,
@@ -175,18 +176,16 @@ private:
 
   nsresult
   SetPrincipalInfo(nsIChannel* aChannel);
 
   RefPtr<CompareManager> mManager;
   RefPtr<CompareCache> mCC;
   RefPtr<ServiceWorkerRegistrationInfo> mRegistration;
 
-  bool mIsMainScript;
-
   nsCOMPtr<nsIChannel> mChannel;
   nsString mBuffer;
   nsString mURL;
   ChannelInfo mChannelInfo;
   RefPtr<InternalHeaders> mInternalHeaders;
   UniquePtr<PrincipalInfo> mPrincipalInfo;
 
   nsCString mMaxScope;
@@ -197,16 +196,19 @@ private:
     WaitingForBothFinished,
     WaitingForNetworkFinished,
     WaitingForCacheFinished,
     Finished
   } mState;
 
   nsresult mNetworkResult;
   nsresult mCacheResult;
+
+  const bool mIsMainScript;
+  bool mIsFromCache;
 };
 
 NS_IMPL_ISUPPORTS(CompareNetwork, nsIStreamLoaderObserver,
                   nsIRequestObserver)
 
 // This class gets a cached Response from the CacheStorage and then it calls
 // CacheFinish() in the CompareNetwork.
 class CompareCache final : public PromiseNativeHandler
@@ -858,16 +860,22 @@ CompareNetwork::OnStartRequest(nsIReques
   mChannelInfo.InitFromChannel(mChannel);
 
   nsresult rv = SetPrincipalInfo(mChannel);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   mInternalHeaders->FillResponseHeaders(mChannel);
+
+  nsCOMPtr<nsICacheInfoChannel> cacheChannel(do_QueryInterface(channel));
+  if (cacheChannel) {
+    cacheChannel->IsFromCache(&mIsFromCache);
+  }
+
   return NS_OK;
 }
 
 nsresult
 CompareNetwork::SetPrincipalInfo(nsIChannel* aChannel)
 {
   nsIScriptSecurityManager* ssm = nsContentUtils::GetSecurityManager();
   if (!ssm) {
@@ -954,25 +962,19 @@ CompareNetwork::OnStreamComplete(nsIStre
   }
 
   // Note: we explicitly don't check for the return value here, because the
   // absence of the header is not an error condition.
   Unused << httpChannel->GetResponseHeader(
       NS_LITERAL_CSTRING("Service-Worker-Allowed"),
       mMaxScope);
 
-  bool isFromCache = false;
-  nsCOMPtr<nsICacheInfoChannel> cacheChannel(do_QueryInterface(httpChannel));
-  if (cacheChannel) {
-    cacheChannel->IsFromCache(&isFromCache);
-  }
-
   // [9.2 Update]4.13, If response's cache state is not "local",
   // set registration's last update check time to the current time
-  if (!isFromCache) {
+  if (!mIsFromCache) {
     mRegistration->RefreshLastUpdateCheckTime();
   }
 
   nsAutoCString mimeType;
   nsresult rv2 = httpChannel->GetContentType(mimeType);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     // We should only end up here if !mResponseHead in the channel.  If headers
     // were received but no content type was specified, we'll be given
