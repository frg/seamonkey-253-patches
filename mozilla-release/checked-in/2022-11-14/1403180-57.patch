# HG changeset patch
# User James Teh <jteh@mozilla.com>
# Date 1508737044 -36000
#      Mon Oct 23 15:37:24 2017 +1000
# Node ID c81df7f7b71fb5056b32eba3d9eff4136bc5719a
# Parent  5ce993c17a4332d2605f4338e432eaf21cafb6a2
Bug 1403180 - Fix StripHandlerFromOBJREF for VT_DISPATCH on Windows 7. r=aklotz, a=ritu

StripHandlerFromOBJREF shortens the OBJREF by sizeof(CLSID), so it needs to seek the stream back after tweaking the OBJREF.
Previously, this was done using a relative seek.
Unfortunately, for some reason I can't fathom on Windows 7, this doesn't work when marshaling for VT_DISPATCH.
The Seek call succeeds, but either does nothing or sets the stream position to a garbage value.
Instead, we now use an absolute seek, which seems to behave.
This was breaking IAccessible::accNavigate and AccessibleChildren on Windows 7.
MozReview-Commit-ID: FEH93oiyP5R

diff --git a/ipc/mscom/Objref.cpp b/ipc/mscom/Objref.cpp
--- a/ipc/mscom/Objref.cpp
+++ b/ipc/mscom/Objref.cpp
@@ -275,19 +275,24 @@ StripHandlerFromOBJREF(NotNull<IStream*>
   // The difference between a OBJREF_STANDARD and an OBJREF_HANDLER is
   // sizeof(CLSID), so we'll zero out the remaining bytes.
   CLSID zeroClsid = {0};
   hr = aStream->Write(&zeroClsid, sizeof(CLSID), &bytesWritten);
   if (FAILED(hr) || bytesWritten != sizeof(CLSID)) {
     return false;
   }
 
-  // Back up to just before the zeros we just wrote
-  seekTo.QuadPart = -static_cast<int64_t>(sizeof(CLSID));
-  return SUCCEEDED(aStream->Seek(seekTo, STREAM_SEEK_CUR, nullptr));
+  // Back up to the end of the tweaked OBJREF.
+  // There are now sizeof(CLSID) less bytes.
+  // Bug 1403180: Using -sizeof(CLSID) with a relative seek sometimes
+  // doesn't work on Windows 7.
+  // It succeeds, but doesn't seek the stream for some unknown reason.
+  // Use an absolute seek instead.
+  seekTo.QuadPart = aEndPos - sizeof(CLSID);
+  return SUCCEEDED(aStream->Seek(seekTo, STREAM_SEEK_SET, nullptr));
 }
 
 uint32_t
 GetOBJREFSize(NotNull<IStream*> aStream)
 {
   // Make a clone so that we don't manipulate aStream's seek pointer
   RefPtr<IStream> cloned;
   HRESULT hr = aStream->Clone(getter_AddRefs(cloned));
