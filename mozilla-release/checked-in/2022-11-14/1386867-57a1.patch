# HG changeset patch
# User Tim Taubert <ttaubert@mozilla.com>
# Date 1504097820 -7200
# Node ID a972e0fb330b7229f86b26dfc75279b44a41c25b
# Parent  4c7b2b9deadc46a1720be3fbf163fce54fc7d697
Bug 1386867 - Fix quantum_pageload_google talos regression r=mystor

diff --git a/browser/components/sessionstore/SessionStorage.jsm b/browser/components/sessionstore/SessionStorage.jsm
--- a/browser/components/sessionstore/SessionStorage.jsm
+++ b/browser/components/sessionstore/SessionStorage.jsm
@@ -23,26 +23,24 @@ function getPrincipalForFrame(docShell, 
   let ssm = Services.scriptSecurityManager;
   let uri = frame.document.documentURIObject;
   return ssm.getDocShellCodebasePrincipal(uri, docShell);
 }
 
 this.SessionStorage = Object.freeze({
   /**
    * Updates all sessionStorage "super cookies"
-   * @param docShell
-   *        That tab's docshell (containing the sessionStorage)
-   * @param frameTree
-   *        The docShell's FrameTree instance.
+   * @param content
+   *        A tab's global, i.e. the root frame we want to collect for.
    * @return Returns a nested object that will have hosts as keys and per-origin
    *         session storage data as strings. For example:
    *         {"https://example.com^userContextId=1": {"key": "value", "my_number": "123"}}
    */
-  collect(docShell, frameTree) {
-    return SessionStorageInternal.collect(docShell, frameTree);
+  collect(content) {
+    return SessionStorageInternal.collect(content);
   },
 
   /**
    * Restores all sessionStorage "super cookies".
    * @param aDocShell
    *        A tab's docshell (containing the sessionStorage)
    * @param aStorageData
    *        A nested object with storage data to be restored that has hosts as
diff --git a/browser/components/sessionstore/content/content-sessionStore.js b/browser/components/sessionstore/content/content-sessionStore.js
--- a/browser/components/sessionstore/content/content-sessionStore.js
+++ b/browser/components/sessionstore/content/content-sessionStore.js
@@ -47,34 +47,16 @@ const DOM_STORAGE_LIMIT_PREF = "browser.
 const TIMEOUT_DISABLED_PREF = "browser.sessionstore.debug.no_auto_updates";
 
 const PREF_INTERVAL = "browser.sessionstore.interval";
 
 const kNoIndex = Number.MAX_SAFE_INTEGER;
 const kLastIndex = Number.MAX_SAFE_INTEGER - 1;
 
 /**
- * Returns a lazy function that will evaluate the given
- * function |fn| only once and cache its return value.
- */
-function createLazy(fn) {
-  let cached = false;
-  let cachedValue = null;
-
-  return function lazy() {
-    if (!cached) {
-      cachedValue = fn();
-      cached = true;
-    }
-
-    return cachedValue;
-  };
-}
-
-/**
  * A function that will recursively call |cb| to collected data for all
  * non-dynamic frames in the current frame/docShell tree.
  */
 function mapFrameTree(callback) {
   return (function map(frame, cb) {
     // Collect data for the current frame.
     let obj = cb(frame) || {};
     let children = [];
@@ -587,20 +569,19 @@ var DocShellCapabilitiesListener = {
  * message to the parent process containing up-to-date sessionStorage data.
  *
  * Causes a SessionStore:update message to be sent that contains the current
  * DOMSessionStorage contents. The data is a nested object using host names
  * as keys and per-host DOMSessionStorage data as values.
  */
 var SessionStorageListener = {
   init() {
-    let filter = ssu.createDynamicFrameEventFilter(this);
-    addEventListener("MozSessionStorageChanged", filter, true);
     Services.obs.addObserver(this, "browser:purge-domain-data");
     StateChangeNotifier.addObserver(this);
+    this.resetEventListener();
   },
 
   uninit() {
     Services.obs.removeObserver(this, "browser:purge-domain-data");
   },
 
   observe() {
     // Collect data on the next tick so that any other observer
@@ -614,31 +595,48 @@ var SessionStorageListener = {
   // changes we have to send over the entire sessions storage data, we just
   // reset these changes.
   _changes: undefined,
 
   resetChanges() {
     this._changes = undefined;
   },
 
+  // The event listener waiting for MozSessionStorageChanged events.
+  _listener: null,
+
+  resetEventListener() {
+    if (!this._listener) {
+      this._listener = ssu.createDynamicFrameEventFilter(this);
+      addEventListener("MozSessionStorageChanged", this._listener, true);
+    }
+  },
+
+  removeEventListener() {
+    removeEventListener("MozSessionStorageChanged", this._listener, true);
+    this._listener = null;
+  },
+
   handleEvent(event) {
     if (!docShell) {
       return;
     }
 
     // How much data does DOMSessionStorage contain?
     let usage = content.QueryInterface(Ci.nsIInterfaceRequestor)
                        .getInterface(Ci.nsIDOMWindowUtils)
                        .getStorageUsage(event.storageArea);
     Services.telemetry.getHistogramById("FX_SESSION_RESTORE_DOM_STORAGE_SIZE_ESTIMATE_CHARS").add(usage);
 
     // Don't store any data if we exceed the limit. Wipe any data we previously
     // collected so that we don't confuse websites with partial state.
     if (usage > Services.prefs.getIntPref(DOM_STORAGE_LIMIT_PREF)) {
       MessageQueue.push("storage", () => null);
+      this.removeEventListener();
+      this.resetChanges();
       return;
     }
 
     let {url, key, newValue} = event;
     let uri = Services.io.newURI(url);
     let domain = uri.prePath;
     if (!this._changes) {
       this._changes = {};
@@ -670,16 +668,17 @@ var SessionStorageListener = {
     MessageQueue.push("storage", () => SessionStorage.collect(content));
   },
 
   onPageLoadCompleted() {
     this.collect();
   },
 
   onPageLoadStarted() {
+    this.resetEventListener();
     this.collect();
   }
 };
 
 /**
  * Listen for changes to the privacy status of the tab.
  * By definition, tabs start in non-private mode.
  *
@@ -838,17 +837,17 @@ var MessageQueue = {
    *
    * @param key (string)
    *        A unique identifier specific to the type of data this is passed.
    * @param fn (function)
    *        A function that returns the value that will be sent to the parent
    *        process.
    */
   push(key, fn) {
-    this._data.set(key, createLazy(fn));
+    this._data.set(key, fn);
 
     if (!this._timeout && !this._timeoutDisabled) {
       // Wait a little before sending the message to batch multiple changes.
       this._timeout = setTimeoutWithTarget(
         () => this.sendWhenIdle(), this.BATCH_DELAY_MS, tabEventTarget);
     }
   },
 
