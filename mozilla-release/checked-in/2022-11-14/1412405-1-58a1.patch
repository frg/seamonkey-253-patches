# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1509138500 14400
# Node ID 3eeb19b262ccfd4d4fbb496c03d9752e03c6ffab
# Parent  6f9a97d89dfd68a6c0f3ed29841535d5a5408c9d
Bug 1412405 - try harder to find a definition for isnanf in the custom linker; r=nalexander

The comment with the accompanying change explains things, but the short
version is that clang generates full calls to isnanf, which our
dlsym-based symbol lookup in the custom linker cannot handle correctly.
We therefore need to do extra work for isnanf to find the correct symbol.

diff --git a/mozglue/linker/ElfLoader.cpp b/mozglue/linker/ElfLoader.cpp
--- a/mozglue/linker/ElfLoader.cpp
+++ b/mozglue/linker/ElfLoader.cpp
@@ -326,16 +326,35 @@ SystemElf::~SystemElf()
   ElfLoader::Singleton.lastError = dlerror();
   ElfLoader::Singleton.Forget(this);
 }
 
 void *
 SystemElf::GetSymbolPtr(const char *symbol) const
 {
   void *sym = dlsym(dlhandle, symbol);
+  // Various bits of Gecko use isnanf, which gcc is happy to compile into
+  // inlined code using floating-point comparisons.  clang, on the other hand,
+  // does not use inline code and generates full calls to isnanf.
+  //
+  // libm.so on Android defines isnanf as weak.  dlsym always returns null for
+  // weak symbols.  Which means that we'll never be able to resolve the symbol
+  // that clang generates here.  However, said weak symbol for isnanf is just
+  // an alias for __isnanf, which is the real definition.  So if we're asked
+  // for isnanf and we can't find it, try looking for __isnanf instead.  The
+  // actual system linker uses alternate resolution interfaces and therefore
+  // does not encounter this issue.
+  //
+  // See also https://bugs.chromium.org/p/chromium/issues/detail?id=376828,
+  // from which this comment and this fix are adapted.
+  if (!sym &&
+      !strcmp(symbol, "isnanf") &&
+      !strcmp(GetName(), "libm.so")) {
+    sym = dlsym(dlhandle, "__isnanf");
+  }
   DEBUG_LOG("dlsym(%p [\"%s\"], \"%s\") = %p", dlhandle, GetPath(), symbol, sym);
   ElfLoader::Singleton.lastError = dlerror();
   return sym;
 }
 
 Mappable *
 SystemElf::GetMappable() const
 {

