# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1510823534 28800
#      Thu Nov 16 01:12:14 2017 -0800
# Node ID 5d31d1c239964a128acc3d88d2d6acd045617e7f
# Parent  d2b56dd2a7ddf516a392876caf611866b59f0bf0
Bug 1416793 - Part 1: Remove array and generator comprehension leftovers. r=jandem

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -474,18 +474,16 @@ class BytecodeEmitter::EmitterScope : pu
         noteIndex_(ScopeNote::NoScopeNoteIndex)
     { }
 
     void dump(BytecodeEmitter* bce);
 
     MOZ_MUST_USE bool enterLexical(BytecodeEmitter* bce, ScopeKind kind,
                                    Handle<LexicalScope::Data*> bindings);
     MOZ_MUST_USE bool enterNamedLambda(BytecodeEmitter* bce, FunctionBox* funbox);
-    MOZ_MUST_USE bool enterComprehensionFor(BytecodeEmitter* bce,
-                                            Handle<LexicalScope::Data*> bindings);
     MOZ_MUST_USE bool enterFunction(BytecodeEmitter* bce, FunctionBox* funbox);
     MOZ_MUST_USE bool enterFunctionExtraBodyVar(BytecodeEmitter* bce, FunctionBox* funbox);
     MOZ_MUST_USE bool enterParameterExpressionVar(BytecodeEmitter* bce);
     MOZ_MUST_USE bool enterGlobal(BytecodeEmitter* bce, GlobalSharedContext* globalsc);
     MOZ_MUST_USE bool enterEval(BytecodeEmitter* bce, EvalSharedContext* evalsc);
     MOZ_MUST_USE bool enterModule(BytecodeEmitter* module, ModuleSharedContext* modulesc);
     MOZ_MUST_USE bool enterWith(BytecodeEmitter* bce);
     MOZ_MUST_USE bool deadZoneFrameSlots(BytecodeEmitter* bce);
@@ -974,46 +972,16 @@ BytecodeEmitter::EmitterScope::enterName
     };
     if (!internScope(bce, createScope))
         return false;
 
     return checkEnvironmentChainLength(bce);
 }
 
 bool
-BytecodeEmitter::EmitterScope::enterComprehensionFor(BytecodeEmitter* bce,
-                                                     Handle<LexicalScope::Data*> bindings)
-{
-    if (!enterLexical(bce, ScopeKind::Lexical, bindings))
-        return false;
-
-    // For comprehensions, initialize all lexical names up front to undefined
-    // because they're now a dead feature and don't interact properly with
-    // TDZ.
-    auto nop = [](BytecodeEmitter*, const NameLocation&, bool) {
-        return true;
-    };
-
-    if (!bce->emit1(JSOP_UNDEFINED))
-        return false;
-
-    RootedAtom name(bce->cx);
-    for (BindingIter bi(*bindings, frameSlotStart(), /* isNamedLambda = */ false); bi; bi++) {
-        name = bi.name();
-        if (!bce->emitInitializeName(name, nop))
-            return false;
-    }
-
-    if (!bce->emit1(JSOP_POP))
-        return false;
-
-    return true;
-}
-
-bool
 BytecodeEmitter::EmitterScope::enterParameterExpressionVar(BytecodeEmitter* bce)
 {
     MOZ_ASSERT(this == bce->innermostEmitterScopeNoCheck());
 
     if (!ensureCache(bce))
         return false;
 
     // Parameter expressions var scopes have no pre-set bindings and are
@@ -3168,23 +3136,22 @@ BytecodeEmitter::checkSideEffects(ParseN
         return true;
 
       // Looking up or evaluating the associated name could throw.
       case ParseNodeKind::TypeOfName:
         MOZ_ASSERT(pn->isArity(PN_UNARY));
         *answer = true;
         return true;
 
-      // These unary cases have side effects on the enclosing object/array,
-      // sure.  But that's not the question this function answers: it's
-      // whether the operation may have a side effect on something *other* than
-      // the result of the overall operation in which it's embedded.  The
-      // answer to that is no, for an object literal having a mutated prototype
-      // and an array comprehension containing no other effectful operations
-      // only produce a value, without affecting anything else.
+      // This unary case has side effects on the enclosing object, sure.  But
+      // that's not the question this function answers: it's whether the
+      // operation may have a side effect on something *other* than the result
+      // of the overall operation in which it's embedded.  The answer to that
+      // is no, because an object literal having a mutated prototype only
+      // produces a value, without affecting anything else.
       case ParseNodeKind::MutateProto:
         MOZ_ASSERT(pn->isArity(PN_UNARY));
         return checkSideEffects(pn->pn_kid, answer);
 
       // Unary cases with obvious side effects.
       case ParseNodeKind::PreIncrement:
       case ParseNodeKind::PostIncrement:
       case ParseNodeKind::PreDecrement:
diff --git a/js/src/frontend/FullParseHandler.h b/js/src/frontend/FullParseHandler.h
--- a/js/src/frontend/FullParseHandler.h
+++ b/js/src/frontend/FullParseHandler.h
@@ -705,20 +705,16 @@ class FullParseHandler
         addList(newExpr, ctor);
         return newExpr;
     }
 
     ParseNode* newAssignment(ParseNodeKind kind, ParseNode* lhs, ParseNode* rhs) {
         return newBinary(kind, lhs, rhs);
     }
 
-    bool isUnparenthesizedCommaExpression(ParseNode* node) {
-        return node->isKind(ParseNodeKind::Comma) && !node->isInParens();
-    }
-
     bool isUnparenthesizedAssignment(Node node) {
         if (node->isKind(ParseNodeKind::Assign) && !node->isInParens()) {
             // ParseNodeKind::Assign is also (mis)used for things like
             // |var name = expr;|. But this method is only called on actual
             // expressions, so we can just assert the node's op is the one used
             // for plain assignment.
             MOZ_ASSERT(node->isOp(JSOP_NOP));
             return true;
diff --git a/js/src/frontend/ParseNode.h b/js/src/frontend/ParseNode.h
--- a/js/src/frontend/ParseNode.h
+++ b/js/src/frontend/ParseNode.h
@@ -633,17 +633,17 @@ class ParseNode
 #define PNX_FUNCDEFS    0x01            /* contains top-level function statements */
 #define PNX_ARRAYHOLESPREAD 0x02        /* one or more of
                                            1. array initialiser has holes
                                            2. array initializer has spread node */
 #define PNX_NONCONST    0x04            /* initialiser has non-constants */
 
     bool functionIsHoisted() const {
         MOZ_ASSERT(pn_arity == PN_CODE && getKind() == ParseNodeKind::Function);
-        MOZ_ASSERT(isOp(JSOP_LAMBDA) ||        // lambda, genexpr
+        MOZ_ASSERT(isOp(JSOP_LAMBDA) ||        // lambda
                    isOp(JSOP_LAMBDA_ARROW) ||  // arrow function
                    isOp(JSOP_DEFFUN) ||        // non-body-level function statement
                    isOp(JSOP_NOP) ||           // body-level function stmt in global code
                    isOp(JSOP_GETLOCAL) ||      // body-level function stmt in function code
                    isOp(JSOP_GETARG) ||        // body-level function redeclaring formal
                    isOp(JSOP_INITLEXICAL));    // block-level function stmt
         return !isOp(JSOP_LAMBDA) && !isOp(JSOP_LAMBDA_ARROW) && !isOp(JSOP_DEFFUN);
     }
diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -519,18 +519,17 @@ FunctionBox::initStandaloneFunction(Scop
 void
 FunctionBox::initWithEnclosingParseContext(ParseContext* enclosing, FunctionSyntaxKind kind)
 {
     SharedContext* sc = enclosing->sc();
     useAsm = sc->isFunctionBox() && sc->asFunctionBox()->useAsmOrInsideUseAsm();
 
     JSFunction* fun = function();
 
-    // Arrow functions and generator expression lambdas don't have
-    // their own `this` binding.
+    // Arrow functions don't have their own `this` binding.
     if (fun->isArrow()) {
         allowNewTarget_ = sc->allowNewTarget();
         allowSuperProperty_ = sc->allowSuperProperty();
         allowSuperCall_ = sc->allowSuperCall();
         needsThisTDZChecks_ = sc->needsThisTDZChecks();
         thisBinding_ = sc->thisBinding();
     } else {
         allowNewTarget_ = true;
diff --git a/js/src/frontend/SharedContext.h b/js/src/frontend/SharedContext.h
--- a/js/src/frontend/SharedContext.h
+++ b/js/src/frontend/SharedContext.h
@@ -202,19 +202,18 @@ class Directives
         return strict_ == rhs.strict_ && asmJS_ == rhs.asmJS_;
     }
     bool operator!=(const Directives& rhs) const {
         return !(*this == rhs);
     }
 };
 
 // The kind of this-binding for the current scope. Note that arrow functions
-// (and generator expression lambdas) have a lexical this-binding so their
-// ThisBinding is the same as the ThisBinding of their enclosing scope and can
-// be any value.
+// have a lexical this-binding so their ThisBinding is the same as the
+// ThisBinding of their enclosing scope and can be any value.
 enum class ThisBinding { Global, Function, Module };
 
 class GlobalSharedContext;
 class EvalSharedContext;
 class ModuleSharedContext;
 
 /*
  * The struct SharedContext is part of the current parser context (see
diff --git a/js/src/frontend/SyntaxParseHandler.h b/js/src/frontend/SyntaxParseHandler.h
--- a/js/src/frontend/SyntaxParseHandler.h
+++ b/js/src/frontend/SyntaxParseHandler.h
@@ -93,24 +93,16 @@ class SyntaxParseHandler
 
         // The directive prologue at the start of a FunctionBody or ScriptBody
         // is the longest sequence (possibly empty) of string literal
         // expression statements at the start of a function.  Thus we need this
         // to treat |"use strict";| as a possible Use Strict Directive and
         // |("use strict");| as a useless statement.
         NodeUnparenthesizedString,
 
-        // Legacy generator expressions of the form |(expr for (...))| and
-        // array comprehensions of the form |[expr for (...)]|) don't permit
-        // |expr| to be a comma expression.  Thus we need this to treat
-        // |(a(), b for (x in []))| as a syntax error and
-        // |((a(), b) for (x in []))| as a generator that calls |a| and then
-        // yields |b| each time it's resumed.
-        NodeUnparenthesizedCommaExpr,
-
         // Assignment expressions in condition contexts could be typos for
         // equality checks.  (Think |if (x = y)| versus |if (x == y)|.)  Thus
         // we need this to treat |if (x = y)| as a possible typo and
         // |if ((x = y))| as a deliberate assignment within a condition.
         //
         // (Technically this isn't needed, as these are *only* extraWarnings
         // warnings, and parsing with that option disables syntax parsing.  But
         // it seems best to be consistent, and perhaps the syntax parser will
@@ -404,41 +396,36 @@ class SyntaxParseHandler
         // pattern.  So we can just say any old thing here, because the only
         // time we'll be wrong is a case that syntax parsing has already
         // rejected.  Use NodeName so the SyntaxParseHandler
         // Parser::cloneLeftHandSide can assert it sees only this.
         return NodeName;
     }
 
     Node newCommaExpressionList(Node kid) {
-        return NodeUnparenthesizedCommaExpr;
+        return NodeGeneric;
     }
 
     void addList(Node list, Node kid) {
         MOZ_ASSERT(list == NodeGeneric ||
                    list == NodeUnparenthesizedArray ||
                    list == NodeUnparenthesizedObject ||
-                   list == NodeUnparenthesizedCommaExpr ||
                    list == NodeVarDeclaration ||
                    list == NodeLexicalDeclaration ||
                    list == NodeFunctionCall);
     }
 
     Node newNewExpression(uint32_t begin, Node ctor) {
         return NodeGeneric;
     }
 
     Node newAssignment(ParseNodeKind kind, Node lhs, Node rhs) {
         return kind == ParseNodeKind::Assign ? NodeUnparenthesizedAssignment : NodeGeneric;
     }
 
-    bool isUnparenthesizedCommaExpression(Node node) {
-        return node == NodeUnparenthesizedCommaExpr;
-    }
-
     bool isUnparenthesizedAssignment(Node node) {
         return node == NodeUnparenthesizedAssignment;
     }
 
     bool isUnparenthesizedUnaryExpression(Node node) {
         return node == NodeUnparenthesizedUnary;
     }
 
@@ -466,17 +453,16 @@ class SyntaxParseHandler
         if (node == NodeUnparenthesizedArray)
             return NodeParenthesizedArray;
         if (node == NodeUnparenthesizedObject)
             return NodeParenthesizedObject;
 
         // Other nodes need not be recognizable after parenthesization; convert
         // them to a generic node.
         if (node == NodeUnparenthesizedString ||
-            node == NodeUnparenthesizedCommaExpr ||
             node == NodeUnparenthesizedAssignment ||
             node == NodeUnparenthesizedUnary)
         {
             return NodeGeneric;
         }
 
         // Convert parenthesized |async| to a normal name node.
         if (node == NodePotentialAsyncKeyword)
diff --git a/js/src/jit-test/tests/debug/Environment-variables.js b/js/src/jit-test/tests/debug/Environment-variables.js
--- a/js/src/jit-test/tests/debug/Environment-variables.js
+++ b/js/src/jit-test/tests/debug/Environment-variables.js
@@ -6,17 +6,17 @@
 load(libdir + "asserts.js");
 
 var cases = [
     // global bindings and bindings on the global prototype chain
     "x = VAL; @@",
     "var x = VAL; @@",
     "Object.prototype.x = VAL; @@",
 
-    // let, catch, and comprehension bindings
+    // let and catch bindings
     "let x = VAL; @@",
     "{ let x = VAL; @@ }",
     "try { throw VAL; } catch (x) { @@ }",
     "try { throw VAL; } catch (x) { @@ }",
     "for (let x of [VAL]) { @@ }",
     "for each (let x in [VAL]) { @@ }",
     "switch (0) { default: let x = VAL; @@ }",
 
diff --git a/js/src/jit-test/tests/gc/bug-886630.js b/js/src/jit-test/tests/gc/bug-886630.js
--- a/js/src/jit-test/tests/gc/bug-886630.js
+++ b/js/src/jit-test/tests/gc/bug-886630.js
@@ -9,17 +9,17 @@ if (false) {
     print(x);
 }
 var tryRunning = tryRunningDirectly;
 function unlikelyToHang(code) {
     var codeL = code.replace(/\s/g, " ");
     return true && code.indexOf("infloop") == -1 && !(codeL.match(/const.*for/)) // can be an infinite loop: function() { const x = 1; for each(x in ({a1:1})) dumpln(3); }
     && !(codeL.match(/for.*const/)) // can be an infinite loop: for each(x in ...); const x;
     && !(codeL.match(/for.*in.*uneval/)) // can be slow to loop through the huge string uneval(this), for example
-    && !(codeL.match(/for.*for.*for/)) // nested for loops (including for..in, array comprehensions, etc) can take a while
+    && !(codeL.match(/for.*for.*for/)) // nested for loops (including for..in, etc) can take a while
     && !(codeL.match(/for.*for.*gc/))
 }
 function whatToTestSpidermonkeyTrunk(code) {
     var codeL = code.replace(/\s/g, " ");
     return {
         allowParse: true,
         allowExec: unlikelyToHang(code),
         allowIter: true,
diff --git a/js/src/js.msg b/js/src/js.msg
--- a/js/src/js.msg
+++ b/js/src/js.msg
@@ -176,17 +176,16 @@ MSG_DEF(JSMSG_OUT_OF_MEMORY,           0
 MSG_DEF(JSMSG_OVER_RECURSED,           0, JSEXN_INTERNALERR, "too much recursion")
 MSG_DEF(JSMSG_TOO_BIG_TO_ENCODE,       0, JSEXN_INTERNALERR, "data are to big to encode")
 MSG_DEF(JSMSG_TOO_DEEP,                1, JSEXN_INTERNALERR, "{0} nested too deeply")
 MSG_DEF(JSMSG_UNCAUGHT_EXCEPTION,      1, JSEXN_INTERNALERR, "uncaught exception: {0}")
 MSG_DEF(JSMSG_UNKNOWN_FORMAT,          1, JSEXN_INTERNALERR, "unknown bytecode format {0}")
 
 // Frontend
 MSG_DEF(JSMSG_ACCESSOR_WRONG_ARGS,     3, JSEXN_SYNTAXERR, "{0} functions must have {1} argument{2}")
-MSG_DEF(JSMSG_ARRAY_COMP_LEFTSIDE,     0, JSEXN_SYNTAXERR, "invalid array comprehension left-hand side")
 MSG_DEF(JSMSG_ARRAY_INIT_TOO_BIG,      0, JSEXN_INTERNALERR, "array initializer too large")
 MSG_DEF(JSMSG_AS_AFTER_IMPORT_STAR,    0, JSEXN_SYNTAXERR, "missing keyword 'as' after import *")
 MSG_DEF(JSMSG_AS_AFTER_RESERVED_WORD,  1, JSEXN_SYNTAXERR, "missing keyword 'as' after reserved word '{0}'")
 MSG_DEF(JSMSG_AWAIT_IN_DEFAULT,        0, JSEXN_SYNTAXERR, "await can't be used in default expression")
 MSG_DEF(JSMSG_AWAIT_OUTSIDE_ASYNC,     0, JSEXN_SYNTAXERR, "await is only valid in async functions and async generators")
 MSG_DEF(JSMSG_BAD_ARROW_ARGS,          0, JSEXN_SYNTAXERR, "invalid arrow-function arguments (parentheses around the arrow-function may help)")
 MSG_DEF(JSMSG_BAD_BINDING,             1, JSEXN_SYNTAXERR, "redefining {0} is deprecated")
 MSG_DEF(JSMSG_BAD_CONST_DECL,          0, JSEXN_SYNTAXERR, "missing = in const declaration")
@@ -195,31 +194,29 @@ MSG_DEF(JSMSG_BAD_DESTRUCT_ASS,        0
 MSG_DEF(JSMSG_BAD_DESTRUCT_TARGET,     0, JSEXN_SYNTAXERR, "invalid destructuring target")
 MSG_DEF(JSMSG_BAD_DESTRUCT_PARENS,     0, JSEXN_SYNTAXERR, "destructuring patterns in assignments can't be parenthesized")
 MSG_DEF(JSMSG_BAD_DESTRUCT_DECL,       0, JSEXN_SYNTAXERR, "missing = in destructuring declaration")
 MSG_DEF(JSMSG_BAD_DUP_ARGS,            0, JSEXN_SYNTAXERR, "duplicate argument names not allowed in this context")
 MSG_DEF(JSMSG_BAD_FOR_EACH_LOOP,       0, JSEXN_SYNTAXERR, "invalid for each loop")
 MSG_DEF(JSMSG_BAD_FOR_LEFTSIDE,        0, JSEXN_SYNTAXERR, "invalid for-in/of left-hand side")
 MSG_DEF(JSMSG_LEXICAL_DECL_DEFINES_LET,0, JSEXN_SYNTAXERR, "a lexical declaration can't define a 'let' binding")
 MSG_DEF(JSMSG_LET_STARTING_FOROF_LHS,  0, JSEXN_SYNTAXERR, "an expression X in 'for (X of Y)' must not start with 'let'")
-MSG_DEF(JSMSG_BAD_GENEXP_BODY,         1, JSEXN_SYNTAXERR, "illegal use of {0} in generator expression")
 MSG_DEF(JSMSG_BAD_INCOP_OPERAND,       0, JSEXN_SYNTAXERR, "invalid increment/decrement operand")
 MSG_DEF(JSMSG_BAD_METHOD_DEF,          0, JSEXN_SYNTAXERR, "bad method definition")
 MSG_DEF(JSMSG_BAD_OCTAL,               1, JSEXN_SYNTAXERR, "{0} is not a legal ECMA-262 octal constant")
 MSG_DEF(JSMSG_BAD_POW_LEFTSIDE,        0, JSEXN_SYNTAXERR, "unparenthesized unary expression can't appear on the left-hand side of '**'")
 MSG_DEF(JSMSG_BAD_PROP_ID,             0, JSEXN_SYNTAXERR, "invalid property id")
 MSG_DEF(JSMSG_BAD_RETURN_OR_YIELD,     1, JSEXN_SYNTAXERR, "{0} not in function")
 MSG_DEF(JSMSG_BAD_STRICT_ASSIGN,       1, JSEXN_SYNTAXERR, "'{0}' can't be defined or assigned to in strict mode code")
 MSG_DEF(JSMSG_BAD_STRICT_ASSIGN_ARGUMENTS, 0, JSEXN_SYNTAXERR, "'arguments' can't be defined or assigned to in strict mode code")
 MSG_DEF(JSMSG_BAD_STRICT_ASSIGN_EVAL,  0, JSEXN_SYNTAXERR, "'eval' can't be defined or assigned to in strict mode code")
 MSG_DEF(JSMSG_BAD_SWITCH,              0, JSEXN_SYNTAXERR, "invalid switch statement")
 MSG_DEF(JSMSG_BAD_SUPER,               0, JSEXN_SYNTAXERR, "invalid use of keyword 'super'")
 MSG_DEF(JSMSG_BAD_SUPERPROP,           1, JSEXN_SYNTAXERR, "use of super {0} accesses only valid within methods or eval code within methods")
 MSG_DEF(JSMSG_BAD_SUPERCALL,           0, JSEXN_SYNTAXERR, "super() is only valid in derived class constructors")
-MSG_DEF(JSMSG_BRACKET_AFTER_ARRAY_COMPREHENSION, 0, JSEXN_SYNTAXERR, "missing ] after array comprehension")
 MSG_DEF(JSMSG_BRACKET_AFTER_LIST,      0, JSEXN_SYNTAXERR, "missing ] after element list")
 MSG_DEF(JSMSG_BRACKET_IN_INDEX,        0, JSEXN_SYNTAXERR, "missing ] in index expression")
 MSG_DEF(JSMSG_BRACKET_OPENED,          2, JSEXN_NOTE, "[ opened at line {0}, column {1}")
 MSG_DEF(JSMSG_CATCH_AFTER_GENERAL,     0, JSEXN_SYNTAXERR, "catch after unconditional catch")
 MSG_DEF(JSMSG_CATCH_IDENTIFIER,        0, JSEXN_SYNTAXERR, "missing identifier in catch")
 MSG_DEF(JSMSG_CATCH_OR_FINALLY,        0, JSEXN_SYNTAXERR, "missing catch or finally after try")
 MSG_DEF(JSMSG_CATCH_WITHOUT_TRY,       0, JSEXN_SYNTAXERR, "catch without try")
 MSG_DEF(JSMSG_COLON_AFTER_CASE,        0, JSEXN_SYNTAXERR, "missing : after case label")
@@ -265,17 +262,16 @@ MSG_DEF(JSMSG_GARBAGE_AFTER_INPUT,     2
 MSG_DEF(JSMSG_IDSTART_AFTER_NUMBER,    0, JSEXN_SYNTAXERR, "identifier starts immediately after numeric literal")
 MSG_DEF(JSMSG_ILLEGAL_CHARACTER,       0, JSEXN_SYNTAXERR, "illegal character")
 MSG_DEF(JSMSG_IMPORT_DECL_AT_TOP_LEVEL, 0, JSEXN_SYNTAXERR, "import declarations may only appear at top level of a module")
 MSG_DEF(JSMSG_OF_AFTER_FOR_LOOP_DECL,  0, JSEXN_SYNTAXERR, "a declaration in the head of a for-of loop can't have an initializer")
 MSG_DEF(JSMSG_IN_AFTER_LEXICAL_FOR_DECL,0,JSEXN_SYNTAXERR, "a lexical declaration in the head of a for-in loop can't have an initializer")
 MSG_DEF(JSMSG_INVALID_FOR_IN_DECL_WITH_INIT,0,JSEXN_SYNTAXERR,"for-in loop head declarations may not have initializers")
 MSG_DEF(JSMSG_INVALID_ID,              1, JSEXN_SYNTAXERR, "{0} is an invalid identifier")
 MSG_DEF(JSMSG_LABEL_NOT_FOUND,         0, JSEXN_SYNTAXERR, "label not found")
-MSG_DEF(JSMSG_LET_COMP_BINDING,        0, JSEXN_SYNTAXERR, "'let' is not a valid name for a comprehension variable")
 MSG_DEF(JSMSG_LEXICAL_DECL_NOT_IN_BLOCK,   1, JSEXN_SYNTAXERR, "{0} declaration not directly within block")
 MSG_DEF(JSMSG_LEXICAL_DECL_LABEL,      1, JSEXN_SYNTAXERR, "{0} declarations cannot be labelled")
 MSG_DEF(JSMSG_GENERATOR_LABEL,         0, JSEXN_SYNTAXERR, "generator functions cannot be labelled")
 MSG_DEF(JSMSG_FUNCTION_LABEL,          0, JSEXN_SYNTAXERR, "functions cannot be labelled")
 MSG_DEF(JSMSG_SLOPPY_FUNCTION_LABEL,   0, JSEXN_SYNTAXERR, "functions can only be labelled inside blocks")
 MSG_DEF(JSMSG_LINE_BREAK_AFTER_THROW,  0, JSEXN_SYNTAXERR, "no line break is allowed between 'throw' and its expression")
 MSG_DEF(JSMSG_LINE_BREAK_BEFORE_ARROW, 0, JSEXN_SYNTAXERR, "no line break is allowed before '=>'")
 MSG_DEF(JSMSG_MALFORMED_ESCAPE,        1, JSEXN_SYNTAXERR, "malformed {0} character escape sequence")
diff --git a/js/src/jsast.tbl b/js/src/jsast.tbl
--- a/js/src/jsast.tbl
+++ b/js/src/jsast.tbl
@@ -29,18 +29,16 @@ ASTDEF(AST_NEW_EXPR,              "NewEx
 ASTDEF(AST_CALL_EXPR,             "CallExpression",                 "callExpression")
 ASTDEF(AST_MEMBER_EXPR,           "MemberExpression",               "memberExpression")
 ASTDEF(AST_FUNC_EXPR,             "FunctionExpression",             "functionExpression")
 ASTDEF(AST_ARROW_EXPR,            "ArrowFunctionExpression",        "arrowFunctionExpression")
 ASTDEF(AST_ARRAY_EXPR,            "ArrayExpression",                "arrayExpression")
 ASTDEF(AST_SPREAD_EXPR,           "SpreadExpression",               "spreadExpression")
 ASTDEF(AST_OBJECT_EXPR,           "ObjectExpression",               "objectExpression")
 ASTDEF(AST_THIS_EXPR,             "ThisExpression",                 "thisExpression")
-ASTDEF(AST_COMP_EXPR,             "ComprehensionExpression",        "comprehensionExpression")
-ASTDEF(AST_GENERATOR_EXPR,        "GeneratorExpression",            "generatorExpression")
 ASTDEF(AST_YIELD_EXPR,            "YieldExpression",                "yieldExpression")
 ASTDEF(AST_CLASS_EXPR,            "ClassExpression",                "classExpression")
 ASTDEF(AST_METAPROPERTY,          "MetaProperty",                   "metaProperty")
 ASTDEF(AST_SUPER,                 "Super",                          "super")
 
 ASTDEF(AST_EMPTY_STMT,            "EmptyStatement",                 "emptyStatement")
 ASTDEF(AST_BLOCK_STMT,            "BlockStatement",                 "blockStatement")
 ASTDEF(AST_EXPR_STMT,             "ExpressionStatement",            "expressionStatement")
@@ -63,18 +61,16 @@ ASTDEF(AST_LET_STMT,              "LetSt
 ASTDEF(AST_IMPORT_DECL,           "ImportDeclaration",              "importDeclaration")
 ASTDEF(AST_IMPORT_SPEC,           "ImportSpecifier",                "importSpecifier")
 ASTDEF(AST_EXPORT_DECL,           "ExportDeclaration",              "exportDeclaration")
 ASTDEF(AST_EXPORT_SPEC,           "ExportSpecifier",                "exportSpecifier")
 ASTDEF(AST_EXPORT_BATCH_SPEC,     "ExportBatchSpecifier",           "exportBatchSpecifier")
 
 ASTDEF(AST_CASE,                  "SwitchCase",                     "switchCase")
 ASTDEF(AST_CATCH,                 "CatchClause",                    "catchClause")
-ASTDEF(AST_COMP_BLOCK,            "ComprehensionBlock",             "comprehensionBlock")
-ASTDEF(AST_COMP_IF,               "ComprehensionIf",                "comprehensionIf")
 
 ASTDEF(AST_ARRAY_PATT,            "ArrayPattern",                   "arrayPattern")
 ASTDEF(AST_OBJECT_PATT,           "ObjectPattern",                  "objectPattern")
 ASTDEF(AST_PROP_PATT,             "Property",                       "propertyPattern")
 ASTDEF(AST_TEMPLATE_LITERAL,      "TemplateLiteral",                "templateLiteral")
 ASTDEF(AST_TAGGED_TEMPLATE,       "TaggedTemplate",                 "taggedTemplate")
 ASTDEF(AST_CALL_SITE_OBJ,         "CallSiteObject",                 "callSiteObject")
 ASTDEF(AST_COMPUTED_NAME,         "ComputedName",                   "computedName")
diff --git a/js/src/tests/non262/reflect-parse/PatternBuilders.js b/js/src/tests/non262/reflect-parse/PatternBuilders.js
--- a/js/src/tests/non262/reflect-parse/PatternBuilders.js
+++ b/js/src/tests/non262/reflect-parse/PatternBuilders.js
@@ -124,20 +124,16 @@ function defaultClause(stmts) {
     return Pattern({ type: "SwitchCase", test: null, consequent: stmts });
 }
 function catchClause(id, body) {
     return Pattern({ type: "CatchClause", param: id, body: body });
 }
 function tryStmt(body, handler, fin) {
     return Pattern({ type: "TryStatement", block: body, handler: handler, finalizer: fin });
 }
-function letStmt(head, body) {
-    return Pattern({ type: "LetStatement", head: head, body: body });
-}
-
 function superProp(id) {
     return dotExpr(Pattern({ type: "Super" }), id);
 }
 function superElem(id) {
     return memExpr(Pattern({ type: "Super" }), id);
 }
 
 function classStmt(id, heritage, body) {
@@ -251,48 +247,19 @@ function templateLit(elts) {
 }
 function taggedTemplate(tagPart, templatePart) {
     return Pattern({ type: "TaggedTemplate", callee: tagPart, arguments : templatePart });
 }
 function template(raw, cooked, ...args) {
     return Pattern([{ type: "CallSiteObject", raw: raw, cooked: cooked}, ...args]);
 }
 
-function compExpr(body, blocks, filter, style) {
-    if (style == "legacy" || !filter)
-        return Pattern({ type: "ComprehensionExpression", body, blocks, filter, style });
-    else
-        return Pattern({ type: "ComprehensionExpression", body, blocks: blocks.concat(compIf(filter)), filter: null, style });
-}
-function genExpr(body, blocks, filter, style) {
-    if (style == "legacy" || !filter)
-        return Pattern({ type: "GeneratorExpression", body, blocks, filter, style });
-    else
-        return Pattern({ type: "GeneratorExpression", body, blocks: blocks.concat(compIf(filter)), filter: null, style });
-}
-function graphExpr(idx, body) {
-    return Pattern({ type: "GraphExpression", index: idx, expression: body });
-}
-function idxExpr(idx) {
-    return Pattern({ type: "GraphIndexExpression", index: idx });
-}
-
-function compBlock(left, right) {
-    return Pattern({ type: "ComprehensionBlock", left: left, right: right, each: false, of: false });
-}
 function compEachBlock(left, right) {
     return Pattern({ type: "ComprehensionBlock", left: left, right: right, each: true, of: false });
 }
-function compOfBlock(left, right) {
-    return Pattern({ type: "ComprehensionBlock", left: left, right: right, each: false, of: true });
-}
-function compIf(test) {
-    return Pattern({ type: "ComprehensionIf", test: test });
-}
-
 function arrPatt(elts) {
     return Pattern({ type: "ArrayPattern", elements: elts });
 }
 function objPatt(elts) {
     return Pattern({ type: "ObjectPattern", properties: elts });
 }
 
 function assignElem(target, defaultExpr = null, targetIdent = typeof target == 'string' ? ident(target) : target) {
diff --git a/js/src/tests/non262/reflect-parse/alternateBuilder.js b/js/src/tests/non262/reflect-parse/alternateBuilder.js
--- a/js/src/tests/non262/reflect-parse/alternateBuilder.js
+++ b/js/src/tests/non262/reflect-parse/alternateBuilder.js
@@ -128,20 +128,16 @@ return {
         for (var i = 0; i < elts.length; i++) {
             if (!elts[i])
                 elts[i] = ["Empty"];
         }
         elts.unshift("TemplateLit", {});
         return elts;
     },
 
-    graphExpression: reject,
-    graphIndexExpression: reject,
-    comprehensionExpression: reject,
-    generatorExpression: reject,
     yieldExpression: reject,
 
     emptyStatement: () => ["EmptyStmt", {}],
     blockStatement: function(stmts) {
         stmts.unshift("BlockStmt", {});
         return stmts;
     },
     labeledStatement: function(lab, stmt) {
@@ -195,17 +191,16 @@ return {
         else
             stmts.unshift("DefaultCase", {});
         return stmts;
     },
     catchClause: function(param, body) {
         param[0] = "IdPatt";
         return ["CatchClause", {}, param, body];
     },
-    comprehensionBlock: reject,
 
     arrayPattern: reject,
     objectPattern: reject,
     propertyPattern: reject,
 };
 })();
 
 Pattern(["Program", {},
diff --git a/js/src/tests/non262/reflect-parse/classes.js b/js/src/tests/non262/reflect-parse/classes.js
--- a/js/src/tests/non262/reflect-parse/classes.js
+++ b/js/src/tests/non262/reflect-parse/classes.js
@@ -453,21 +453,16 @@ function testClasses() {
     assertClassError("class NAME { constructor() { super; } }", SyntaxError);
 
     /* SuperCall */
 
     // SuperCall is invalid outside derived class constructors.
     assertError("super()", SyntaxError);
     assertError("(function() { super(); })", SyntaxError);
 
-    // SuperCall is invalid in generator comprehensions, even inside derived
-    // class constructors
-    assertError("(super() for (x in y))", SyntaxError);
-    assertClassError("class NAME { constructor() { (super() for (x in y))", SyntaxError);
-
 
     // Even in class constructors
     assertClassError("class NAME { constructor() { super(); } }", SyntaxError);
 
     function superConstructor(args) {
         return classMethod(ident("constructor"),
                            methodFun("NAME", "method", false,
                                      [], [exprStmt(superCallExpr(args))]),
diff --git a/js/src/vm/EnvironmentObject.cpp b/js/src/vm/EnvironmentObject.cpp
--- a/js/src/vm/EnvironmentObject.cpp
+++ b/js/src/vm/EnvironmentObject.cpp
@@ -1922,18 +1922,17 @@ class DebugEnvironmentProxyHandler : pub
   public:
     static const char family;
     static const DebugEnvironmentProxyHandler singleton;
 
     constexpr DebugEnvironmentProxyHandler() : BaseProxyHandler(&family) {}
 
     static bool isFunctionEnvironmentWithThis(const JSObject& env)
     {
-        // All functions except arrows and generator expression lambdas should
-        // have their own this binding.
+        // All functions except arrows should have their own this binding.
         return isFunctionEnvironment(env) && !env.as<CallObject>().callee().hasLexicalThis();
     }
 
     bool getPrototypeIfOrdinary(JSContext* cx, HandleObject proxy, bool* isOrdinary,
                                 MutableHandleObject protop) const override
     {
         MOZ_CRASH("shouldn't be possible to access the prototype chain of a DebugEnvironmentProxyHandler");
     }
diff --git a/js/src/vm/EnvironmentObject.h b/js/src/vm/EnvironmentObject.h
--- a/js/src/vm/EnvironmentObject.h
+++ b/js/src/vm/EnvironmentObject.h
@@ -923,17 +923,17 @@ class DebugEnvironmentProxy : public Pro
     bool isForDeclarative() const;
 
     // Get a property by 'id', but returns sentinel values instead of throwing
     // on exceptional cases.
     static bool getMaybeSentinelValue(JSContext* cx, Handle<DebugEnvironmentProxy*> env,
                                       HandleId id, MutableHandleValue vp);
 
     // Returns true iff this is a function environment with its own this-binding
-    // (all functions except arrow functions and generator expression lambdas).
+    // (all functions except arrow functions).
     bool isFunctionEnvironmentWithThis();
 
     // Does this debug environment not have a real counterpart or was never
     // live (and thus does not have a synthesized EnvironmentObject or a
     // snapshot)?
     bool isOptimizedOut() const;
 };
 
diff --git a/js/src/vm/HelperThreads.cpp b/js/src/vm/HelperThreads.cpp
--- a/js/src/vm/HelperThreads.cpp
+++ b/js/src/vm/HelperThreads.cpp
@@ -696,17 +696,17 @@ EnsureParserCreatedClasses(JSContext* cx
 
     if (!EnsureConstructor(cx, global, JSProto_Array))
         return false; // needed by array literals
 
     if (!EnsureConstructor(cx, global, JSProto_RegExp))
         return false; // needed by regular expression literals
 
     if (!GlobalObject::initGenerators(cx, global))
-        return false; // needed by function*() {} and generator comprehensions
+        return false; // needed by function*() {}
 
     if (kind == ParseTaskKind::Module && !GlobalObject::ensureModulePrototypesCreated(cx, global))
         return false;
 
     return true;
 }
 
 class AutoClearUsedByHelperThread
