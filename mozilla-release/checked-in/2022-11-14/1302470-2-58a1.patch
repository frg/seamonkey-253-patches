# HG changeset patch
# User Brad Werth <bwerth@mozilla.com>
# Date 1492708912 25200
# Node ID 9dfcb2f9db7dcfe69ef348a9eb3cecaaf240c140
# Parent  87cfef435223a44e0066bacce54a52b67e115e32
Bug 1302470 Part 2: Branch IsRangeVisible to delegate to IsRangeRendered when range is in viewport. r=masayuki

MozReview-Commit-ID: LZyvt08I9vz

diff --git a/toolkit/components/typeaheadfind/nsTypeAheadFind.cpp b/toolkit/components/typeaheadfind/nsTypeAheadFind.cpp
--- a/toolkit/components/typeaheadfind/nsTypeAheadFind.cpp
+++ b/toolkit/components/typeaheadfind/nsTypeAheadFind.cpp
@@ -1211,32 +1211,32 @@ nsTypeAheadFind::IsRangeVisible(nsIPresS
   nsCOMPtr<nsIDOMNode> node;
   aRange->GetStartContainer(getter_AddRefs(node));
 
   nsCOMPtr<nsIContent> content(do_QueryInterface(node));
   if (!content)
     return false;
 
   nsIFrame *frame = content->GetPrimaryFrame();
-  if (!frame)
+  if (!frame) {
     return false;  // No frame! Not visible then.
+  }
 
-  if (!frame->StyleVisibility()->IsVisible())
+  if (!frame->StyleVisibility()->IsVisible()) {
     return false;
+  }
 
   // Detect if we are _inside_ a text control, or something else with its own
   // selection controller.
   if (aUsesIndependentSelection) {
     *aUsesIndependentSelection =
       (frame->GetStateBits() & NS_FRAME_INDEPENDENT_SELECTION);
   }
 
   // ---- We have a frame ----
-  if (!aMustBeInViewPort)
-    return true; //  Don't need it to be on screen, just in rendering tree
 
   // Get the next in flow frame that contains the range start
   int32_t startFrameOffset, endFrameOffset;
   uint32_t startRangeOffset;
   aRange->GetStartOffset(&startRangeOffset);
   while (true) {
     frame->GetOffsets(startFrameOffset, endFrameOffset);
     if (static_cast<int32_t>(startRangeOffset) < endFrameOffset) {
@@ -1262,37 +1262,48 @@ nsTypeAheadFind::IsRangeVisible(nsIPresS
 
   if (!aGetTopVisibleLeaf && !frame->GetRect().IsEmpty()) {
     rectVisibility =
       aPresShell->GetRectVisibility(frame,
                                     nsRect(nsPoint(0,0), frame->GetSize()),
                                     minDistance);
 
     if (rectVisibility != nsRectVisibility_kAboveViewport) {
-      return true;
+      // This is an early exit case, where we return true iff the range
+      // is actually rendered.
+      return IsRangeRendered(aPresShell, aPresContext, aRange);
     }
   }
 
-  // We know that the target range isn't usable because it's not in the
-  // view port. Move range forward to first visible point,
+  // Below this point, we know the range is not in the viewport.
+
+  if (!aMustBeInViewPort) {
+    // This is an early exit case because we don't care that that range
+    // is out of viewport, so we return that the range is "visible".
+    return true;
+  }
+
+  // The range isn't in the viewport, but we could scroll it into view.
+  // Move range forward to first visible point,
   // this speeds us up a lot in long documents
   nsCOMPtr<nsIFrameEnumerator> frameTraversal;
   nsCOMPtr<nsIFrameTraversal> trav(do_CreateInstance(kFrameTraversalCID));
   if (trav)
     trav->NewFrameTraversal(getter_AddRefs(frameTraversal),
                             aPresContext, frame,
                             eLeaf,
                             false, // aVisual
                             false, // aLockInScrollView
                             false, // aFollowOOFs
                             false  // aSkipPopupChecks
                             );
 
-  if (!frameTraversal)
+  if (!frameTraversal) {
     return false;
+  }
 
   while (rectVisibility == nsRectVisibility_kAboveViewport) {
     frameTraversal->Next();
     frame = frameTraversal->CurrentItem();
     if (!frame)
       return false;
 
     if (!frame->GetRect().IsEmpty()) {
