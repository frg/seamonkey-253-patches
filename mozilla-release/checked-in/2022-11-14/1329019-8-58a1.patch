# HG changeset patch
# User yuyin <yuyin-hf@loongson.cn>
# Date 1506588720 14400
# Node ID 238ad8ca8df0aaa72446a11594108607e1f7ca18
# Parent  f28b9affebf67360b52cdb5034e8e704ed39a8d8
Bug 1329019 - MIPS: Add some missing functions to fix build errors. r=luke

diff --git a/js/src/jit/mips-shared/Assembler-mips-shared.cpp b/js/src/jit/mips-shared/Assembler-mips-shared.cpp
--- a/js/src/jit/mips-shared/Assembler-mips-shared.cpp
+++ b/js/src/jit/mips-shared/Assembler-mips-shared.cpp
@@ -96,16 +96,37 @@ AssemblerMIPSShared::finish()
 }
 
 bool
 AssemblerMIPSShared::appendRawCode(const uint8_t* code, size_t numBytes)
 {
     return m_buffer.appendRawCode(code, numBytes);
 }
 
+bool
+AssemblerMIPSShared::reserve(size_t size)
+{
+    // This buffer uses fixed-size chunks so there's no point in reserving
+    // now vs. on-demand.
+    return !oom();
+}
+
+bool
+AssemblerMIPSShared::swapBuffer(wasm::Bytes& bytes)
+{
+    // For now, specialize to the one use case. As long as wasm::Bytes is a
+    // Vector, not a linked-list of chunks, there's not much we can do other
+    // than copy.
+    MOZ_ASSERT(bytes.empty());
+    if (!bytes.resize(bytesNeeded()))
+        return false;
+    m_buffer.executableCopy(bytes.begin());
+    return true;
+}
+
 uint32_t
 AssemblerMIPSShared::actualIndex(uint32_t idx_) const
 {
     return idx_;
 }
 
 uint8_t*
 AssemblerMIPSShared::PatchableJumpAddress(JitCode* code, uint32_t pe_)
@@ -122,25 +143,16 @@ AssemblerMIPSShared::copyJumpRelocationT
 
 void
 AssemblerMIPSShared::copyDataRelocationTable(uint8_t* dest)
 {
     if (dataRelocations_.length())
         memcpy(dest, dataRelocations_.buffer(), dataRelocations_.length());
 }
 
-void
-AssemblerMIPSShared::processCodeLabels(uint8_t* rawCode)
-{
-    for (size_t i = 0; i < codeLabels_.length(); i++) {
-        CodeLabel label = codeLabels_[i];
-        Bind(rawCode, *label.patchAt(), *label.target());
-    }
-}
-
 AssemblerMIPSShared::Condition
 AssemblerMIPSShared::InvertCondition(Condition cond)
 {
     switch (cond) {
       case Equal:
         return NotEqual;
       case NotEqual:
         return Equal;
diff --git a/js/src/jit/mips-shared/Assembler-mips-shared.h b/js/src/jit/mips-shared/Assembler-mips-shared.h
--- a/js/src/jit/mips-shared/Assembler-mips-shared.h
+++ b/js/src/jit/mips-shared/Assembler-mips-shared.h
@@ -948,16 +948,18 @@ class AssemblerMIPSShared : public Assem
         return StackPointer;
     }
 
   protected:
     bool isFinished;
   public:
     void finish();
     bool appendRawCode(const uint8_t* code, size_t numBytes);
+    bool reserve(size_t size);
+    bool swapBuffer(wasm::Bytes& bytes);
     void executableCopy(void* buffer, bool flushICache = true);
     void copyJumpRelocationTable(uint8_t* dest);
     void copyDataRelocationTable(uint8_t* dest);
 
     // Size of the instruction stream, in bytes.
     size_t size() const;
     // Size of the jump relocation table, in bytes.
     size_t jumpRelocationTableBytes() const;
@@ -1222,17 +1224,16 @@ class AssemblerMIPSShared : public Assem
                          FPConditionBit fcc = FCC0);
     BufferOffset as_movz(FloatFormat fmt, FloatRegister fd, FloatRegister fs, Register rt);
     BufferOffset as_movn(FloatFormat fmt, FloatRegister fd, FloatRegister fs, Register rt);
 
     // label operations
     void bind(Label* label, BufferOffset boff = BufferOffset());
     void bindLater(Label* label, wasm::TrapDesc target);
     virtual void bind(InstImm* inst, uintptr_t branch, uintptr_t target) = 0;
-    virtual void Bind(uint8_t* rawCode, CodeOffset label, CodeOffset target) = 0;
     void bind(CodeOffset* label) {
         label->bind(currentOffset());
     }
     void use(CodeOffset* label) {
         label->bind(currentOffset());
     }
     uint32_t currentOffset() {
         return nextOffset().getOffset();
@@ -1302,18 +1303,16 @@ class AssemblerMIPSShared : public Assem
 
     static uint8_t* NextInstruction(uint8_t* instruction, uint32_t* count = nullptr);
 
     static void ToggleToJmp(CodeLocationLabel inst_);
     static void ToggleToCmp(CodeLocationLabel inst_);
 
     static void UpdateLuiOriValue(Instruction* inst0, Instruction* inst1, uint32_t value);
 
-    void processCodeLabels(uint8_t* rawCode);
-
     bool bailed() {
         return m_buffer.bail();
     }
 
     void verifyHeapAccessDisassembly(uint32_t begin, uint32_t end,
                                      const Disassembler::HeapAccess& heapAccess)
     {
         // Implement this if we implement a disassembler.
diff --git a/js/src/jit/mips32/Assembler-mips32.cpp b/js/src/jit/mips32/Assembler-mips32.cpp
--- a/js/src/jit/mips32/Assembler-mips32.cpp
+++ b/js/src/jit/mips32/Assembler-mips32.cpp
@@ -424,16 +424,25 @@ Assembler::bind(RepatchLabel* label)
             addLongJump(BufferOffset(label->offset() + sizeof(void*)));
             Assembler::WriteLuiOriInstructions(&inst[1], &inst[2], ScratchRegister, dest.getOffset());
             inst[3] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         }
     }
     label->bind(dest.getOffset());
 }
 
+void
+Assembler::processCodeLabels(uint8_t* rawCode)
+{
+    for (size_t i = 0; i < codeLabels_.length(); i++) {
+        CodeLabel label = codeLabels_[i];
+        Bind(rawCode, *label.patchAt(), *label.target());
+    }
+}
+
 uint32_t
 Assembler::PatchWrite_NearCallSize()
 {
     return 4 * sizeof(uint32_t);
 }
 
 void
 Assembler::PatchWrite_NearCall(CodeLocationLabel start, CodeLocationLabel toCall)
diff --git a/js/src/jit/mips32/Assembler-mips32.h b/js/src/jit/mips32/Assembler-mips32.h
--- a/js/src/jit/mips32/Assembler-mips32.h
+++ b/js/src/jit/mips32/Assembler-mips32.h
@@ -152,16 +152,18 @@ class Assembler : public AssemblerMIPSSh
     }
 
   public:
     using AssemblerMIPSShared::bind;
 
     void bind(RepatchLabel* label);
     static void Bind(uint8_t* rawCode, CodeOffset label, CodeOffset target);
 
+    void processCodeLabels(uint8_t* rawCode);
+
     static void TraceJumpRelocations(JSTracer* trc, JitCode* code, CompactBufferReader& reader);
     static void TraceDataRelocations(JSTracer* trc, JitCode* code, CompactBufferReader& reader);
 
     void bind(InstImm* inst, uintptr_t branch, uintptr_t target);
 
     // Copy the assembly code to the given buffer, and perform any pending
     // relocations relying on the target address.
     void executableCopy(uint8_t* buffer, bool flushICache = true);
diff --git a/js/src/jit/mips64/Assembler-mips64.cpp b/js/src/jit/mips64/Assembler-mips64.cpp
--- a/js/src/jit/mips64/Assembler-mips64.cpp
+++ b/js/src/jit/mips64/Assembler-mips64.cpp
@@ -362,16 +362,25 @@ Assembler::bind(RepatchLabel* label)
             addLongJump(BufferOffset(label->offset() + sizeof(uint32_t)));
             Assembler::WriteLoad64Instructions(&inst[1], ScratchRegister, dest.getOffset());
             inst[5] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         }
     }
     label->bind(dest.getOffset());
 }
 
+void
+Assembler::processCodeLabels(uint8_t* rawCode)
+{
+    for (size_t i = 0; i < codeLabels_.length(); i++) {
+        CodeLabel label = codeLabels_[i];
+        Bind(rawCode, *label.patchAt(), *label.target());
+    }
+}
+
 uint32_t
 Assembler::PatchWrite_NearCallSize()
 {
     // Load an address needs 4 instructions, and a jump with a delay slot.
     return (4 + 2) * sizeof(uint32_t);
 }
 
 void
diff --git a/js/src/jit/mips64/Assembler-mips64.h b/js/src/jit/mips64/Assembler-mips64.h
--- a/js/src/jit/mips64/Assembler-mips64.h
+++ b/js/src/jit/mips64/Assembler-mips64.h
@@ -144,16 +144,18 @@ class Assembler : public AssemblerMIPSSh
 
     static uintptr_t GetPointer(uint8_t*);
 
     using AssemblerMIPSShared::bind;
 
     void bind(RepatchLabel* label);
     static void Bind(uint8_t* rawCode, CodeOffset label, CodeOffset target);
 
+    void processCodeLabels(uint8_t* rawCode);
+
     static void TraceJumpRelocations(JSTracer* trc, JitCode* code, CompactBufferReader& reader);
     static void TraceDataRelocations(JSTracer* trc, JitCode* code, CompactBufferReader& reader);
 
     void bind(InstImm* inst, uintptr_t branch, uintptr_t target);
 
     // Copy the assembly code to the given buffer, and perform any pending
     // relocations relying on the target address.
     void executableCopy(uint8_t* buffer, bool flushICache = true);
