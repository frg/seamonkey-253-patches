# HG changeset patch
# User Arthur Edelstein <arthuredelstein@gmail.com>
# Date 1506641820 14400
# Node ID 2e390724da6f4933ff63617eea12fb34dfb42f21
# Parent  ee49f5bb7b204499b646da1b9e0106e18cbe7749
Bug 1404169 - Fix ubsan runtime error for nsSMState enum. r=m_kato

Based on the patch by Jehan <jehan@girinstud.io>:
https://cgit.freedesktop.org/uchardet/uchardet/commit/?id=53f7ad0e0b0894f11c948ee75f74595ab1b61405
Original freedesktop bug:
https://bugs.freedesktop.org/show_bug.cgi?id=101032

diff --git a/extensions/universalchardet/src/base/nsCodingStateMachine.h b/extensions/universalchardet/src/base/nsCodingStateMachine.h
--- a/extensions/universalchardet/src/base/nsCodingStateMachine.h
+++ b/extensions/universalchardet/src/base/nsCodingStateMachine.h
@@ -4,21 +4,22 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #ifndef nsCodingStateMachine_h__
 #define nsCodingStateMachine_h__
 
 #include "mozilla/ArrayUtils.h"
 
 #include "nsPkgInt.h"
 
-typedef enum {
-   eStart = 0,
-   eError = 1,
-   eItsMe = 2
-} nsSMState;
+/* Apart from these 3 generic states, machine states are specific to
+ * each charset prober.
+ */
+#define eStart 0
+#define eError 1
+#define eItsMe 2
 
 #define GETCLASS(c) GETFROMPCK(((unsigned char)(c)), mModel->classTable)
 
 //state machine model
 typedef struct
 {
   nsPkgInt classTable;
   uint32_t classFactor;
@@ -28,37 +29,37 @@ typedef struct
   const size_t charLenTableLength;
 #endif
   const char* name;
 } SMModel;
 
 class nsCodingStateMachine {
 public:
   explicit nsCodingStateMachine(const SMModel* sm) : mModel(sm) { mCurrentState = eStart; }
-  nsSMState NextState(char c){
+  uint32_t NextState(char c){
     //for each byte we get its class , if it is first byte, we also get byte length
     uint32_t byteCls = GETCLASS(c);
     if (mCurrentState == eStart)
     {
       mCurrentBytePos = 0;
       MOZ_ASSERT(byteCls < mModel->charLenTableLength);
       mCurrentCharLen = mModel->charLenTable[byteCls];
     }
     //from byte's class and stateTable, we get its next state
-    mCurrentState=(nsSMState)GETFROMPCK(mCurrentState*(mModel->classFactor)+byteCls,
-                                       mModel->stateTable);
+    mCurrentState = GETFROMPCK(mCurrentState * mModel->classFactor + byteCls,
+                               mModel->stateTable);
     mCurrentBytePos++;
     return mCurrentState;
   }
   uint32_t  GetCurrentCharLen(void) {return mCurrentCharLen;}
   void      Reset(void) {mCurrentState = eStart;}
   const char * GetCodingStateMachine() {return mModel->name;}
 
 protected:
-  nsSMState mCurrentState;
+  uint32_t mCurrentState;
   uint32_t mCurrentCharLen;
   uint32_t mCurrentBytePos;
 
   const SMModel *mModel;
 };
 
 extern const SMModel UTF8SMModel;
 extern const SMModel Big5SMModel;
diff --git a/extensions/universalchardet/src/base/nsEUCJPProber.cpp b/extensions/universalchardet/src/base/nsEUCJPProber.cpp
--- a/extensions/universalchardet/src/base/nsEUCJPProber.cpp
+++ b/extensions/universalchardet/src/base/nsEUCJPProber.cpp
@@ -17,17 +17,17 @@ void  nsEUCJPProber::Reset(void)
   mState = eDetecting;
   mContextAnalyser.Reset();
   mDistributionAnalyser.Reset();
 }
 
 nsProbingState nsEUCJPProber::HandleData(const char* aBuf, uint32_t aLen)
 {
   NS_ASSERTION(aLen, "HandleData called with empty buffer");
-  nsSMState codingState;
+  uint32_t codingState;
 
   for (uint32_t i = 0; i < aLen; i++)
   {
     codingState = mCodingSM->NextState(aBuf[i]);
     if (codingState == eItsMe)
     {
       mState = eFoundIt;
       break;
diff --git a/extensions/universalchardet/src/base/nsEscCharsetProber.cpp b/extensions/universalchardet/src/base/nsEscCharsetProber.cpp
--- a/extensions/universalchardet/src/base/nsEscCharsetProber.cpp
+++ b/extensions/universalchardet/src/base/nsEscCharsetProber.cpp
@@ -22,17 +22,17 @@ void nsEscCharSetProber::Reset(void)
 {
   mState = eDetecting;
   mCodingSM->Reset();
   mDetectedCharset = nullptr;
 }
 
 nsProbingState nsEscCharSetProber::HandleData(const char* aBuf, uint32_t aLen)
 {
-  nsSMState codingState;
+  uint32_t codingState;
   uint32_t i;
 
   for ( i = 0; i < aLen && mState == eDetecting; i++)
   {
     codingState = mCodingSM->NextState(aBuf[i]);
     if (codingState == eItsMe)
     {
       mState = eFoundIt;
diff --git a/extensions/universalchardet/src/base/nsSJISProber.cpp b/extensions/universalchardet/src/base/nsSJISProber.cpp
--- a/extensions/universalchardet/src/base/nsSJISProber.cpp
+++ b/extensions/universalchardet/src/base/nsSJISProber.cpp
@@ -17,17 +17,17 @@ void  nsSJISProber::Reset(void)
   mState = eDetecting;
   mContextAnalyser.Reset();
   mDistributionAnalyser.Reset();
 }
 
 nsProbingState nsSJISProber::HandleData(const char* aBuf, uint32_t aLen)
 {
   NS_ASSERTION(aLen, "HandleData called with empty buffer");
-  nsSMState codingState;
+  uint32_t codingState;
 
   for (uint32_t i = 0; i < aLen; i++)
   {
     codingState = mCodingSM->NextState(aBuf[i]);
     if (codingState == eItsMe)
     {
       mState = eFoundIt;
       break;
diff --git a/extensions/universalchardet/src/base/nsUTF8Prober.cpp b/extensions/universalchardet/src/base/nsUTF8Prober.cpp
--- a/extensions/universalchardet/src/base/nsUTF8Prober.cpp
+++ b/extensions/universalchardet/src/base/nsUTF8Prober.cpp
@@ -9,17 +9,17 @@ void  nsUTF8Prober::Reset(void)
 {
   mCodingSM->Reset();
   mNumOfMBChar = 0;
   mState = eDetecting;
 }
 
 nsProbingState nsUTF8Prober::HandleData(const char* aBuf, uint32_t aLen)
 {
-  nsSMState codingState;
+  uint32_t codingState;
 
   for (uint32_t i = 0; i < aLen; i++)
   {
     codingState = mCodingSM->NextState(aBuf[i]);
     if (codingState == eItsMe)
     {
       mState = eFoundIt;
       break;

