# HG changeset patch
# User Edgar Chen <echen@mozilla.com>
# Date 1507909014 -28800
# Node ID 1d0330d6d96fcce081c477914219061efe601bb8
# Parent  8450fb4272c9c79cf398cd969f2e21d14859acf2
Bug 1396620 - Part 1: Remove created callback for custom elements; r=jdai,smaug

MozReview-Commit-ID: IDovq3OZwgy

diff --git a/dom/base/CustomElementRegistry.cpp b/dom/base/CustomElementRegistry.cpp
--- a/dom/base/CustomElementRegistry.cpp
+++ b/dom/base/CustomElementRegistry.cpp
@@ -18,48 +18,16 @@
 namespace mozilla {
 namespace dom {
 
 void
 CustomElementCallback::Call()
 {
   IgnoredErrorResult rv;
   switch (mType) {
-    case nsIDocument::eCreated:
-    {
-      // For the duration of this callback invocation, the element is being created
-      // flag must be set to true.
-      mOwnerData->mElementIsBeingCreated = true;
-
-      // The callback hasn't actually been invoked yet, but we need to flip
-      // this now in order to enqueue the connected callback. This is a spec
-      // bug (w3c bug 27437).
-      mOwnerData->mCreatedCallbackInvoked = true;
-
-      // If ELEMENT is connected, enqueue connected callback for ELEMENT.
-      nsIDocument* document = mThisObject->GetComposedDoc();
-      if (document) {
-        NodeInfo* ni = mThisObject->NodeInfo();
-        // We need to do this because at this point, CustomElementDefinition is
-        // not set to CustomElementData yet, so EnqueueLifecycleCallback will
-        // fail to find the CE definition for this custom element.
-        // This will go away eventually since there is no created callback in v1.
-        CustomElementDefinition* definition =
-          nsContentUtils::LookupCustomElementDefinition(document,
-            ni->LocalName(), ni->NamespaceID(),
-            mOwnerData->GetCustomElementType());
-
-        nsContentUtils::EnqueueLifecycleCallback(
-          nsIDocument::eConnected, mThisObject, nullptr, nullptr, definition);
-      }
-
-      static_cast<LifecycleCreatedCallback *>(mCallback.get())->Call(mThisObject, rv);
-      mOwnerData->mElementIsBeingCreated = false;
-      break;
-    }
     case nsIDocument::eConnected:
       static_cast<LifecycleConnectedCallback *>(mCallback.get())->Call(mThisObject, rv);
       break;
     case nsIDocument::eDisconnected:
       static_cast<LifecycleDisconnectedCallback *>(mCallback.get())->Call(mThisObject, rv);
       break;
     case nsIDocument::eAdopted:
       static_cast<LifecycleAdoptedCallback *>(mCallback.get())->Call(mThisObject,
@@ -79,22 +47,20 @@ CustomElementCallback::Traverse(nsCycleC
   aCb.NoteXPCOMChild(mThisObject);
 
   NS_CYCLE_COLLECTION_NOTE_EDGE_NAME(aCb, "mCallback");
   aCb.NoteXPCOMChild(mCallback);
 }
 
 CustomElementCallback::CustomElementCallback(Element* aThisObject,
                                              nsIDocument::ElementCallbackType aCallbackType,
-                                             mozilla::dom::CallbackFunction* aCallback,
-                                             CustomElementData* aOwnerData)
+                                             mozilla::dom::CallbackFunction* aCallback)
   : mThisObject(aThisObject),
     mCallback(aCallback),
-    mType(aCallbackType),
-    mOwnerData(aOwnerData)
+    mType(aCallbackType)
 {
 }
 //-----------------------------------------------------
 // CustomElementConstructor
 
 already_AddRefed<Element>
 CustomElementConstructor::Construct(const char* aExecutionReason,
                                     ErrorResult& aRv)
@@ -125,19 +91,17 @@ CustomElementConstructor::Construct(cons
 // CustomElementData
 
 CustomElementData::CustomElementData(nsIAtom* aType)
   : CustomElementData(aType, CustomElementData::State::eUndefined)
 {
 }
 
 CustomElementData::CustomElementData(nsIAtom* aType, State aState)
-  : mElementIsBeingCreated(false)
-  , mCreatedCallbackInvoked(true)
-  , mState(aState)
+  : mState(aState)
   , mType(aType)
 {
 }
 
 void
 CustomElementData::SetCustomElementDefinition(CustomElementDefinition* aDefinition)
 {
   MOZ_ASSERT(mState == State::eCustom);
@@ -345,29 +309,22 @@ CustomElementRegistry::UnregisterUnresol
 /* static */ UniquePtr<CustomElementCallback>
 CustomElementRegistry::CreateCustomElementCallback(
   nsIDocument::ElementCallbackType aType, Element* aCustomElement,
   LifecycleCallbackArgs* aArgs,
   LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
   CustomElementDefinition* aDefinition)
 {
   MOZ_ASSERT(aDefinition, "CustomElementDefinition should not be null");
-
-  RefPtr<CustomElementData> elementData = aCustomElement->GetCustomElementData();
-  MOZ_ASSERT(elementData, "CustomElementData should exist");
+  MOZ_ASSERT(aCustomElement->GetCustomElementData(),
+             "CustomElementData should exist");
 
   // Let CALLBACK be the callback associated with the key NAME in CALLBACKS.
   CallbackFunction* func = nullptr;
   switch (aType) {
-    case nsIDocument::eCreated:
-      if (aDefinition->mCallbacks->mCreatedCallback.WasPassed()) {
-        func = aDefinition->mCallbacks->mCreatedCallback.Value();
-      }
-      break;
-
     case nsIDocument::eConnected:
       if (aDefinition->mCallbacks->mConnectedCallback.WasPassed()) {
         func = aDefinition->mCallbacks->mConnectedCallback.Value();
       }
       break;
 
     case nsIDocument::eDisconnected:
       if (aDefinition->mCallbacks->mDisconnectedCallback.WasPassed()) {
@@ -388,58 +345,30 @@ CustomElementRegistry::CreateCustomEleme
       break;
   }
 
   // If there is no such callback, stop.
   if (!func) {
     return nullptr;
   }
 
-  if (aType == nsIDocument::eCreated) {
-    elementData->mCreatedCallbackInvoked = false;
-  } else if (!elementData->mCreatedCallbackInvoked) {
-    // Callbacks other than created callback must not be enqueued
-    // until after the created callback has been invoked.
-    return nullptr;
-  }
-
   // Add CALLBACK to ELEMENT's callback queue.
   auto callback =
-    MakeUnique<CustomElementCallback>(aCustomElement, aType, func, elementData);
+    MakeUnique<CustomElementCallback>(aCustomElement, aType, func);
 
   if (aArgs) {
     callback->SetArgs(*aArgs);
   }
 
   if (aAdoptedCallbackArgs) {
     callback->SetAdoptedCallbackArgs(*aAdoptedCallbackArgs);
   }
   return Move(callback);
 }
 
-void
-CustomElementRegistry::SyncInvokeReactions(nsIDocument::ElementCallbackType aType,
-                                           Element* aCustomElement,
-                                           CustomElementDefinition* aDefinition)
-{
-  auto callback = CreateCustomElementCallback(aType, aCustomElement, nullptr,
-                                              nullptr, aDefinition);
-  if (!callback) {
-    return;
-  }
-
-  UniquePtr<CustomElementReaction> reaction(Move(
-    MakeUnique<CustomElementCallbackReaction>(Move(callback))));
-
-  RefPtr<SyncInvokeReactionRunnable> runnable =
-    new SyncInvokeReactionRunnable(Move(reaction), aCustomElement);
-
-  nsContentUtils::AddScriptRunner(runnable);
-}
-
 /* static */ void
 CustomElementRegistry::EnqueueLifecycleCallback(nsIDocument::ElementCallbackType aType,
                                                 Element* aCustomElement,
                                                 LifecycleCallbackArgs* aArgs,
                                                 LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
                                                 CustomElementDefinition* aDefinition)
 {
   CustomElementDefinition* definition = aDefinition;
@@ -527,19 +456,17 @@ nsISupports* CustomElementRegistry::GetP
 {
   return mWindow;
 }
 
 static const char* kLifeCycleCallbackNames[] = {
   "connectedCallback",
   "disconnectedCallback",
   "adoptedCallback",
-  "attributeChangedCallback",
-  // The life cycle callbacks from v0 spec.
-  "createdCallback"
+  "attributeChangedCallback"
 };
 
 static void
 CheckLifeCycleCallbacks(JSContext* aCx,
                         JS::Handle<JSObject*> aConstructor,
                         ErrorResult& aRv)
 {
   for (size_t i = 0; i < ArrayLength(kLifeCycleCallbackNames); ++i) {
@@ -1004,21 +931,16 @@ CustomElementRegistry::Upgrade(Element* 
     return;
   }
 
   // Step 8.
   data->mState = CustomElementData::State::eCustom;
 
   // Step 9.
   aElement->SetCustomElementDefinition(aDefinition);
-
-  // This is for old spec.
-  nsContentUtils::EnqueueLifecycleCallback(nsIDocument::eCreated,
-                                           aElement, nullptr,
-                                           nullptr, aDefinition);
 }
 
 //-----------------------------------------------------
 // CustomElementReactionsStack
 
 void
 CustomElementReactionsStack::CreateAndPushElementQueue()
 {
@@ -1180,21 +1102,16 @@ NS_IMPL_CYCLE_COLLECTION_TRAVERSE_BEGIN(
   mozilla::dom::LifecycleCallbacks* callbacks = tmp->mCallbacks.get();
 
   if (callbacks->mAttributeChangedCallback.WasPassed()) {
     NS_CYCLE_COLLECTION_NOTE_EDGE_NAME(cb,
       "mCallbacks->mAttributeChangedCallback");
     cb.NoteXPCOMChild(callbacks->mAttributeChangedCallback.Value());
   }
 
-  if (callbacks->mCreatedCallback.WasPassed()) {
-    NS_CYCLE_COLLECTION_NOTE_EDGE_NAME(cb, "mCallbacks->mCreatedCallback");
-    cb.NoteXPCOMChild(callbacks->mCreatedCallback.Value());
-  }
-
   if (callbacks->mConnectedCallback.WasPassed()) {
     NS_CYCLE_COLLECTION_NOTE_EDGE_NAME(cb, "mCallbacks->mConnectedCallback");
     cb.NoteXPCOMChild(callbacks->mConnectedCallback.Value());
   }
 
   if (callbacks->mDisconnectedCallback.WasPassed()) {
     NS_CYCLE_COLLECTION_NOTE_EDGE_NAME(cb, "mCallbacks->mDisconnectedCallback");
     cb.NoteXPCOMChild(callbacks->mDisconnectedCallback.Value());
diff --git a/dom/base/CustomElementRegistry.h b/dom/base/CustomElementRegistry.h
--- a/dom/base/CustomElementRegistry.h
+++ b/dom/base/CustomElementRegistry.h
@@ -46,18 +46,17 @@ struct LifecycleAdoptedCallbackArgs
   nsCOMPtr<nsIDocument> mNewDocument;
 };
 
 class CustomElementCallback
 {
 public:
   CustomElementCallback(Element* aThisObject,
                         nsIDocument::ElementCallbackType aCallbackType,
-                        CallbackFunction* aCallback,
-                        CustomElementData* aOwnerData);
+                        CallbackFunction* aCallback);
   void Traverse(nsCycleCollectionTraversalCallback& aCb) const;
   void Call();
   void SetArgs(LifecycleCallbackArgs& aArgs)
   {
     MOZ_ASSERT(mType == nsIDocument::eAttributeChanged,
                "Arguments are only used by attribute changed callback.");
     mArgs = aArgs;
   }
@@ -74,19 +73,16 @@ private:
   RefPtr<Element> mThisObject;
   RefPtr<CallbackFunction> mCallback;
   // The type of callback (eCreated, eAttached, etc.)
   nsIDocument::ElementCallbackType mType;
   // Arguments to be passed to the callback,
   // used by the attribute changed callback.
   LifecycleCallbackArgs mArgs;
   LifecycleAdoptedCallbackArgs mAdoptedCallbackArgs;
-  // CustomElementData that contains this callback in the
-  // callback queue.
-  CustomElementData* mOwnerData;
 };
 
 class CustomElementConstructor final : public CallbackFunction
 {
 public:
   explicit CustomElementConstructor(CallbackFunction* aOther)
     : CallbackFunction(aOther)
   {
@@ -110,21 +106,16 @@ struct CustomElementData
     eUndefined,
     eFailed,
     eCustom
   };
 
   explicit CustomElementData(nsIAtom* aType);
   CustomElementData(nsIAtom* aType, State aState);
 
-  // Element is being created flag as described in the custom elements spec.
-  bool mElementIsBeingCreated;
-  // Flag to determine if the created callback has been invoked, thus it
-  // determines if other callbacks can be enqueued.
-  bool mCreatedCallbackInvoked;
   // Custom element state as described in the custom element spec.
   State mState;
   // custom element reaction queue as described in the custom element spec.
   // There is 1 reaction in reaction queue, when 1) it becomes disconnected,
   // 2) it’s adopted into a new document, 3) its attributes are changed,
   // appended, removed, or replaced.
   // There are 3 reactions in reaction queue when doing upgrade operation,
   // e.g., create an element, insert a node.
@@ -380,20 +371,16 @@ public:
                                        Element* aCustomElement,
                                        LifecycleCallbackArgs* aArgs,
                                        LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs,
                                        CustomElementDefinition* aDefinition);
 
   void GetCustomPrototype(nsIAtom* aAtom,
                           JS::MutableHandle<JSObject*> aPrototype);
 
-  void SyncInvokeReactions(nsIDocument::ElementCallbackType aType,
-                           Element* aCustomElement,
-                           CustomElementDefinition* aDefinition);
-
   /**
    * Upgrade an element.
    * https://html.spec.whatwg.org/multipage/scripting.html#upgrades
    */
   static void Upgrade(Element* aElement, CustomElementDefinition* aDefinition, ErrorResult& aRv);
 
   /**
    * Registers an unresolved custom element that is a candidate for
@@ -472,41 +459,16 @@ private:
       ~AutoSetRunningFlag() {
         mRegistry->mIsCustomDefinitionRunning = false;
       }
 
     private:
       CustomElementRegistry* mRegistry;
   };
 
-  class SyncInvokeReactionRunnable : public mozilla::Runnable {
-    public:
-      SyncInvokeReactionRunnable(
-        UniquePtr<CustomElementReaction> aReaction, Element* aCustomElement)
-        : Runnable(
-            "dom::CustomElementRegistry::SyncInvokeReactionRunnable")
-        , mReaction(Move(aReaction))
-        , mCustomElement(aCustomElement)
-      {
-      }
-
-      NS_IMETHOD Run() override
-      {
-        // It'll never throw exceptions, because all the exceptions are handled
-        // by Lifecycle*Callback::Call function.
-        ErrorResult rv;
-        mReaction->Invoke(mCustomElement, rv);
-        return NS_OK;
-      }
-
-    private:
-      UniquePtr<CustomElementReaction> mReaction;
-      Element* mCustomElement;
-  };
-
 public:
   nsISupports* GetParentObject() const;
 
   virtual JSObject* WrapObject(JSContext* aCx, JS::Handle<JSObject*> aGivenProto) override;
 
   void Define(const nsAString& aName, Function& aFunctionConstructor,
               const ElementDefinitionOptions& aOptions, ErrorResult& aRv);
 
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -10225,37 +10225,16 @@ nsContentUtils::GetElementDefinitionIfOb
   if (!definition || !definition->IsInObservedAttributeList(aAttrName)) {
     return nullptr;
   }
 
   return definition;
 }
 
 /* static */ void
-nsContentUtils::SyncInvokeReactions(nsIDocument::ElementCallbackType aType,
-                                    Element* aElement,
-                                    CustomElementDefinition* aDefinition)
-{
-  MOZ_ASSERT(aElement);
-
-  nsIDocument* doc = aElement->OwnerDoc();
-  nsPIDOMWindowInner* window(doc->GetInnerWindow());
-  if (!window) {
-    return;
-  }
-
-  RefPtr<CustomElementRegistry> registry(window->CustomElements());
-  if (!registry) {
-    return;
-  }
-
-  registry->SyncInvokeReactions(aType, aElement, aDefinition);
-}
-
-/* static */ void
 nsContentUtils::EnqueueUpgradeReaction(Element* aElement,
                                        CustomElementDefinition* aDefinition)
 {
   MOZ_ASSERT(aElement);
 
   nsIDocument* doc = aElement->OwnerDoc();
 
   // No DocGroup means no custom element reactions stack.
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -3040,20 +3040,16 @@ public:
   static void RegisterUnresolvedElement(Element* aElement, nsIAtom* aTypeName);
   static void UnregisterUnresolvedElement(Element* aElement);
 
   static mozilla::dom::CustomElementDefinition*
   GetElementDefinitionIfObservingAttr(Element* aCustomElement,
                                       nsIAtom* aExtensionType,
                                       nsIAtom* aAttrName);
 
-  static void SyncInvokeReactions(nsIDocument::ElementCallbackType aType,
-                                  Element* aCustomElement,
-                                  mozilla::dom::CustomElementDefinition* aDefinition);
-
   static void EnqueueUpgradeReaction(Element* aElement,
                                      mozilla::dom::CustomElementDefinition* aDefinition);
 
   static void EnqueueLifecycleCallback(nsIDocument::ElementCallbackType aType,
                                        Element* aCustomElement,
                                        mozilla::dom::LifecycleCallbackArgs* aArgs = nullptr,
                                        mozilla::dom::LifecycleAdoptedCallbackArgs* aAdoptedCallbackArgs = nullptr,
                                        mozilla::dom::CustomElementDefinition* aDefinition = nullptr);
diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -6310,19 +6310,16 @@ nsDocument::CustomElementConstructor(JSC
     }
 
     element->SetCustomElementData(
       new CustomElementData(definition->mType,
                             CustomElementData::State::eCustom));
 
     element->SetCustomElementDefinition(definition);
 
-    // It'll be removed when we deprecate custom elements v0.
-    nsContentUtils::SyncInvokeReactions(nsIDocument::eCreated, element,
-                                        definition);
     NS_ENSURE_TRUE(element, false);
   }
 
   // The prototype setup happens in Element::WrapObject().
   nsresult rv = nsContentUtils::WrapNative(aCx, element, element, args.rval());
   NS_ENSURE_SUCCESS(rv, true);
 
   return true;
diff --git a/dom/base/nsIDocument.h b/dom/base/nsIDocument.h
--- a/dom/base/nsIDocument.h
+++ b/dom/base/nsIDocument.h
@@ -2805,17 +2805,16 @@ public:
   // Skip GetContentType, because our NS_IMETHOD version above works fine here.
   // GetDoctype defined above
   Element* GetDocumentElement() const
   {
     return GetRootElement();
   }
 
   enum ElementCallbackType {
-    eCreated,
     eConnected,
     eDisconnected,
     eAdopted,
     eAttributeChanged
   };
 
   nsIDocument* GetTopLevelContentDocument();
 
diff --git a/dom/base/test/chrome/registerElement_ep.js b/dom/base/test/chrome/registerElement_ep.js
--- a/dom/base/test/chrome/registerElement_ep.js
+++ b/dom/base/test/chrome/registerElement_ep.js
@@ -1,8 +1,8 @@
 var proto = Object.create(HTMLElement.prototype);
 proto.magicNumber = 42;
-proto.createdCallback = function() {
+proto.connectedCallback = function() {
   finishTest(this.magicNumber === 42);
 };
 document.registerElement("x-foo", { prototype: proto });
 
-document.createElement("x-foo");
+document.firstChild.appendChild(document.createElement("x-foo"));
diff --git a/dom/base/test/chrome/test_registerElement_content.xul b/dom/base/test/chrome/test_registerElement_content.xul
--- a/dom/base/test/chrome/test_registerElement_content.xul
+++ b/dom/base/test/chrome/test_registerElement_content.xul
@@ -16,40 +16,37 @@ https://bugzilla.mozilla.org/show_bug.cg
      target="_blank">Mozilla Bug 1130028</a>
   <iframe onload="startTests()" id="frame" src="http://example.com/chrome/dom/base/test/chrome/frame_registerElement_content.html"></iframe>
   </body>
 
   <!-- test code goes here -->
   <script type="application/javascript"><![CDATA[
 
   /** Test for Bug 1130028 **/
-  SimpleTest.waitForExplicitFinish();
-
-  var createdCallbackCount = 0;
+  var connectedCallbackCount = 0;
 
-  // Callback should be called once by element created in chrome,
-  // and once by element created in content.
-  function createdCallbackCalled() {
-    createdCallbackCount++;
-    ok(true, "Created callback called, should be called twice in test.");
+  // Callback should be called only once by element created in content.
+  function connectedCallbackCalled() {
+    connectedCallbackCount++;
+    is(connectedCallbackCount, 1, "Connected callback called, should be called once in test.");
     is(this.magicNumber, 42, "Callback should be able to see the custom prototype.");
-    if (createdCallbackCount == 2) {
-      SimpleTest.finish();
-    }
   }
 
   function startTests() {
     var frame = $("frame");
 
     var c = frame.contentDocument.registerElement("x-foo");
     var elem = new c();
     is(elem.tagName, "X-FOO", "Constructor should create an x-foo element.");
 
     var proto = Object.create(frame.contentWindow.HTMLElement.prototype);
     proto.magicNumber = 42;
-    proto.createdCallback = createdCallbackCalled;
+    proto.connectedCallback = connectedCallbackCalled;
+
     frame.contentDocument.registerElement("x-bar", { prototype: proto });
+    is(connectedCallbackCount, 1, "Connected callback should be called by element created in content.");
 
-    frame.contentDocument.createElement("x-bar");
+    var element = frame.contentDocument.createElement("x-bar");
+    is(element.magicNumber, 42, "Should be able to see the custom prototype on created element.");
   }
 
   ]]></script>
 </window>
diff --git a/dom/base/test/chrome/test_registerElement_ep.xul b/dom/base/test/chrome/test_registerElement_ep.xul
--- a/dom/base/test/chrome/test_registerElement_ep.xul
+++ b/dom/base/test/chrome/test_registerElement_ep.xul
@@ -21,18 +21,18 @@ https://bugzilla.mozilla.org/show_bug.cg
   <script type="application/javascript"><![CDATA[
 
   ChromeUtils.import("resource://gre/modules/Services.jsm");
 
   /** Test for Bug 1130028 **/
   SimpleTest.waitForExplicitFinish();
 
   function finishTest(canSeePrototype) {
-    ok(true, "createdCallback called when reigsterElement was called with an extended principal.");
-    ok(canSeePrototype, "createdCallback should be able to see custom prototype.");
+    ok(true, "connectedCallback called when reigsterElement was called with an extended principal.");
+    ok(canSeePrototype, "connectedCallback should be able to see custom prototype.");
     SimpleTest.finish();
   }
 
   function startTests() {
     var frame = $("frame");
 
     // Create a sandbox with an extended principal then run a script that registers a custom element in the sandbox.
     var sandbox = Cu.Sandbox([frame.contentWindow], { sandboxPrototype: frame.contentWindow });
diff --git a/dom/tests/mochitest/webcomponents/mochitest.ini b/dom/tests/mochitest/webcomponents/mochitest.ini
--- a/dom/tests/mochitest/webcomponents/mochitest.ini
+++ b/dom/tests/mochitest/webcomponents/mochitest.ini
@@ -3,26 +3,23 @@ support-files =
   inert_style.css
   dummy_page.html
 
 [test_bug900724.html]
 [test_bug1017896.html]
 [test_bug1176757.html]
 [test_bug1276240.html]
 [test_content_element.html]
-[test_custom_element_adopt_callbacks.html]
 [test_custom_element_callback_innerhtml.html]
 skip-if = true # disabled - See bug 1390396
-[test_custom_element_clone_callbacks_extended.html]
 [test_custom_element_htmlconstructor.html]
 skip-if = os == 'android' # bug 1323645
 support-files =
   htmlconstructor_autonomous_tests.js
   htmlconstructor_builtin_tests.js
-[test_custom_element_import_node_created_callback.html]
 [test_custom_element_in_shadow.html]
 skip-if = true # disabled - See bug 1390396
 [test_custom_element_register_invalid_callbacks.html]
 [test_custom_element_throw_on_dynamic_markup_insertion.html]
 [test_custom_element_get.html]
 [test_custom_element_when_defined.html]
 [test_custom_element_upgrade.html]
 support-files =
diff --git a/dom/tests/mochitest/webcomponents/test_custom_element_adopt_callbacks.html b/dom/tests/mochitest/webcomponents/test_custom_element_adopt_callbacks.html
deleted file mode 100644
--- a/dom/tests/mochitest/webcomponents/test_custom_element_adopt_callbacks.html
+++ /dev/null
@@ -1,29 +0,0 @@
-<!DOCTYPE HTML>
-<html>
-<!--
-https://bugzilla.mozilla.org/show_bug.cgi?id=1081039
--->
-<head>
-  <title>Test callbacks for adopted custom elements.</title>
-  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
-  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
-</head>
-<body>
-<template id="template"><x-foo></x-foo></template>
-<a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=1081039">Bug 1081039</a>
-<script>
-
-var p = Object.create(HTMLElement.prototype);
-p.createdCallback = function() {
-  ok(false, "Created callback should not be called for adopted node.");
-};
-
-document.registerElement("x-foo", { prototype: p });
-
-var template = document.getElementById("template");
-var adoptedFoo = document.adoptNode(template.content.firstChild);
-is(adoptedFoo.nodeName, "X-FOO");
-
-</script>
-</body>
-</html>
diff --git a/dom/tests/mochitest/webcomponents/test_custom_element_clone_callbacks_extended.html b/dom/tests/mochitest/webcomponents/test_custom_element_clone_callbacks_extended.html
deleted file mode 100644
--- a/dom/tests/mochitest/webcomponents/test_custom_element_clone_callbacks_extended.html
+++ /dev/null
@@ -1,40 +0,0 @@
-<!DOCTYPE HTML>
-<html>
-<!--
-https://bugzilla.mozilla.org/show_bug.cgi?id=1081039
--->
-<head>
-  <title>Test callbacks for cloned extended custom elements.</title>
-  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
-  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
-</head>
-<body>
-<a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=1081039">Bug 1081039</a>
-<script>
-
-SimpleTest.waitForExplicitFinish();
-
-// Test to make sure created callback is called on clones created after
-// registering the custom element.
-
-var count = 0;
-var p = Object.create(HTMLButtonElement.prototype);
-p.createdCallback = function() { // should be called by createElement and cloneNode
-  is(this.__proto__, p, "Correct prototype should be set on custom elements.");
-
-  if (++count == 2) {
-    SimpleTest.finish();
-  }
-};
-
-document.registerElement("x-foo", { prototype: p, extends: "button" });
-var foo = document.createElement("button", {is: "x-foo"});
-is(foo.getAttribute("is"), "x-foo");
-
-var fooClone = foo.cloneNode(true);
-
-SimpleTest.waitForExplicitFinish();
-
-</script>
-</body>
-</html>
diff --git a/dom/tests/mochitest/webcomponents/test_custom_element_import_node_created_callback.html b/dom/tests/mochitest/webcomponents/test_custom_element_import_node_created_callback.html
deleted file mode 100644
--- a/dom/tests/mochitest/webcomponents/test_custom_element_import_node_created_callback.html
+++ /dev/null
@@ -1,34 +0,0 @@
-<!DOCTYPE HTML>
-<html>
-<!--
-https://bugzilla.mozilla.org/show_bug.cgi?id=1093680
--->
-<head>
-  <title>Test created callback order for imported custom element.</title>
-  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
-  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
-</head>
-<body>
-<template id="template"><x-foo><span></span></x-foo></template>
-<a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=1093680">Bug 1093680</a>
-<script>
-
-var fooProtoCreatedCallbackCalled = false;
-var fooProto = Object.create(HTMLElement.prototype);
-fooProto.createdCallback = function() {
-  ok(this.firstElementChild, "When the created callback is called, the element should already have a child because the callback should only be called after cloning all the contents.");
-  fooProtoCreatedCallbackCalled = true;
-};
-
-document.registerElement("x-foo", { prototype: fooProto });
-
-var template = document.getElementById("template");
-
-// Importing node will implicityly clone the conent in the main document.
-var adoptedFoo = document.importNode(template.content, true);
-
-ok(fooProtoCreatedCallbackCalled, "Created callback should be called after importing custom element into document");
-
-</script>
-</body>
-</html>
diff --git a/dom/tests/mochitest/webcomponents/test_custom_element_register_invalid_callbacks.html b/dom/tests/mochitest/webcomponents/test_custom_element_register_invalid_callbacks.html
--- a/dom/tests/mochitest/webcomponents/test_custom_element_register_invalid_callbacks.html
+++ b/dom/tests/mochitest/webcomponents/test_custom_element_register_invalid_callbacks.html
@@ -14,17 +14,16 @@ https://bugzilla.mozilla.org/show_bug.cg
 <script>
 
 // Use window from iframe to isolate the test.
 const testWindow = iframe.contentDocument.defaultView;
 
 // This is for backward compatibility.
 // We should do the same checks for the callbacks from v0 spec.
 [
-  'createdCallback',
   'attributeChangedCallback',
 ].forEach(callback => {
   var c = class {};
   var p = c.prototype;
 
   // Test getting callback throws exception.
   Object.defineProperty(p, callback, {
     get() {
diff --git a/dom/tests/mochitest/webcomponents/test_document_shared_registry.html b/dom/tests/mochitest/webcomponents/test_document_shared_registry.html
--- a/dom/tests/mochitest/webcomponents/test_document_shared_registry.html
+++ b/dom/tests/mochitest/webcomponents/test_document_shared_registry.html
@@ -9,47 +9,16 @@ https://bugzilla.mozilla.org/show_bug.cg
   <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
 </head>
 <body>
 <div id="container"></div>
 <a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=783129">Bug 783129</a>
 <script>
 var container = document.getElementById("container");
 
-function createdCallbackFromMainDoc() {
-  var createdCallbackCalled = false;
-  var assocDoc = document.implementation.createHTMLDocument();
-
-  var proto = Object.create(HTMLElement.prototype);
-  proto.createdCallback = function() {
-    is(createdCallbackCalled, false, "created callback should only be called once in this tests.");
-    createdCallbackCalled = true;
-    runNextTest();
-  };
-
-  assocDoc.registerElement("x-associated-doc-callback-elem", { prototype: proto });
-  document.createElement("x-associated-doc-callback-elem");
-}
-
-function createdCallbackFromDocHTMLNamespace() {
-  var createdCallbackCalled = false;
-  var assocDoc = document.implementation.createDocument("http://www.w3.org/1999/xhtml", "html", null);
-  var somediv = assocDoc.createElement("div");
-
-  var proto = Object.create(HTMLElement.prototype);
-  proto.createdCallback = function() {
-    is(createdCallbackCalled, false, "created callback should only be called once in this tests.");
-    createdCallbackCalled = true;
-    runNextTest();
-  };
-
-  assocDoc.registerElement("x-assoc-doc-with-ns-callback-elem", { prototype: proto });
-  document.createElement("x-assoc-doc-with-ns-callback-elem");
-}
-
 function registerNoRegistryDoc() {
   var assocDoc = document.implementation.createDocument(null, "html");
   try {
     assocDoc.registerElement("x-dummy", { prototype: Object.create(HTMLElement.prototype) });
     ok(false, "Registring element in document without registry should throw.");
   } catch (ex) {
     ok(true, "Registring element in document without registry should throw.");
   }
@@ -60,18 +29,16 @@ function registerNoRegistryDoc() {
 function runNextTest() {
   if (testFunctions.length > 0) {
     var nextTestFunction = testFunctions.shift();
     nextTestFunction();
   }
 }
 
 var testFunctions = [
-  createdCallbackFromMainDoc,
-  createdCallbackFromDocHTMLNamespace,
   registerNoRegistryDoc,
   SimpleTest.finish
 ];
 
 SimpleTest.waitForExplicitFinish();
 
 runNextTest();
 </script>
diff --git a/dom/webidl/WebComponents.webidl b/dom/webidl/WebComponents.webidl
--- a/dom/webidl/WebComponents.webidl
+++ b/dom/webidl/WebComponents.webidl
@@ -5,28 +5,26 @@
  *
  * The origin of this IDL file is
  * http://dvcs.w3.org/hg/webcomponents/raw-file/tip/spec/custom/index.html
  *
  * Copyright © 2012 W3C® (MIT, ERCIM, Keio), All Rights Reserved. W3C
  * liability, trademark and document use rules apply.
  */
 
-callback LifecycleCreatedCallback = void();
 callback LifecycleConnectedCallback = void();
 callback LifecycleDisconnectedCallback = void();
 callback LifecycleAdoptedCallback = void(Document? oldDocument,
                                          Document? newDocment);
 callback LifecycleAttributeChangedCallback = void(DOMString attrName,
                                                   DOMString? oldValue,
                                                   DOMString? newValue,
                                                   DOMString? namespaceURI);
 
 dictionary LifecycleCallbacks {
-  LifecycleCreatedCallback? createdCallback;
   LifecycleConnectedCallback? connectedCallback;
   LifecycleDisconnectedCallback? disconnectedCallback;
   LifecycleAdoptedCallback? adoptedCallback;
   LifecycleAttributeChangedCallback? attributeChangedCallback;
 };
 
 dictionary ElementRegistrationOptions {
   object? prototype = null;
diff --git a/js/xpconnect/tests/mochitest/test_bug1094930.html b/js/xpconnect/tests/mochitest/test_bug1094930.html
--- a/js/xpconnect/tests/mochitest/test_bug1094930.html
+++ b/js/xpconnect/tests/mochitest/test_bug1094930.html
@@ -11,19 +11,19 @@ https://bugzilla.mozilla.org/show_bug.cg
   <iframe id="ifr"></iframe>
 </head>
 <body>
 <a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=1094930">Mozilla Bug 1094930</a>
 <p id="display"></p>
 <script type="application/javascript">
   SimpleTest.waitForExplicitFinish();
   var proto = { 
-    createdCallback: function() {
-      ok(true, "createdCallback was called");
+    connectedCallback: function() {
+      ok(true, "connectedCallback was called");
       SimpleTest.finish()
     }
   };
 
   var f = document.registerElement.call(frames[0].document, "x-foo", { prototype: proto });
-  var inst = new f();
+  frames[0].document.firstChild.appendChild(new f());
 </script>
 </body>
 </html>
