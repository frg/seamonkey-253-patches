# HG changeset patch
# User John Dai <jdai@mozilla.com>
# Date 1507008840 14400
# Node ID 3bc98b219437ef4d30819a955796a37fb9ee23c0
# Parent  26b2bcc1e774c35bf5e9ffc28d7e495ee751686f
Bug 1319342 - Clone a node should enqueue an upgrade reaction. r=smaug

diff --git a/dom/base/nsNodeUtils.cpp b/dom/base/nsNodeUtils.cpp
--- a/dom/base/nsNodeUtils.cpp
+++ b/dom/base/nsNodeUtils.cpp
@@ -463,29 +463,47 @@ nsNodeUtils::CloneAndAdopt(nsINode *aNod
   nsCOMPtr<nsINode> clone;
   if (aClone) {
     nsresult rv = aNode->Clone(nodeInfo, getter_AddRefs(clone), aDeep);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       aError.Throw(rv);
       return nullptr;
     }
 
-    if (clone->IsElement()) {
+    if (CustomElementRegistry::IsCustomElementEnabled() && clone->IsElement()) {
       // The cloned node may be a custom element that may require
-      // enqueing created callback and prototype swizzling.
+      // enqueing upgrade reaction.
       Element* elem = clone->AsElement();
-      if (nsContentUtils::IsCustomElementName(nodeInfo->NameAtom())) {
-        nsContentUtils::SetupCustomElement(elem);
+      CustomElementDefinition* definition = nullptr;
+      nsCOMPtr<nsIAtom> tagAtom = nodeInfo->NameAtom();
+      if (nsContentUtils::IsCustomElementName(tagAtom)) {
+        definition =
+          nsContentUtils::LookupCustomElementDefinition(nodeInfo->GetDocument(),
+                                                        nodeInfo->LocalName(),
+                                                        nodeInfo->NamespaceID());
+        if (definition) {
+          elem->SetCustomElementData(new CustomElementData(tagAtom));
+          nsContentUtils::EnqueueUpgradeReaction(elem, definition);
+        }
       } else {
         // Check if node may be custom element by type extension.
         // ex. <button is="x-button">
         nsAutoString extension;
         if (elem->GetAttr(kNameSpaceID_None, nsGkAtoms::is, extension) &&
             !extension.IsEmpty()) {
-          nsContentUtils::SetupCustomElement(elem, &extension);
+          definition =
+            nsContentUtils::LookupCustomElementDefinition(nodeInfo->GetDocument(),
+                                                          nodeInfo->LocalName(),
+                                                          nodeInfo->NamespaceID(),
+                                                          &extension);
+          if (definition) {
+            nsCOMPtr<nsIAtom> typeAtom = NS_Atomize(extension);
+            elem->SetCustomElementData(new CustomElementData(typeAtom));
+            nsContentUtils::EnqueueUpgradeReaction(elem, definition);
+          }
         }
       }
     }
 
     if (aParent) {
       // If we're cloning we need to insert the cloned children into the cloned
       // parent.
       rv = aParent->AppendChildTo(static_cast<nsIContent*>(clone.get()),
diff --git a/dom/tests/mochitest/webcomponents/mochitest.ini b/dom/tests/mochitest/webcomponents/mochitest.ini
--- a/dom/tests/mochitest/webcomponents/mochitest.ini
+++ b/dom/tests/mochitest/webcomponents/mochitest.ini
@@ -7,17 +7,16 @@ support-files =
 [test_bug1017896.html]
 [test_bug1176757.html]
 skip-if = stylo # bug 1293844
 [test_bug1276240.html]
 [test_content_element.html]
 skip-if = stylo # bug 1293844
 [test_custom_element_adopt_callbacks.html]
 [test_custom_element_callback_innerhtml.html]
-[test_custom_element_clone_callbacks.html]
 [test_custom_element_clone_callbacks_extended.html]
 [test_custom_element_htmlconstructor.html]
 skip-if = os == 'android' # bug 1323645
 support-files =
   htmlconstructor_autonomous_tests.js
   htmlconstructor_builtin_tests.js
 [test_custom_element_import_node_created_callback.html]
 [test_custom_element_in_shadow.html]
diff --git a/dom/tests/mochitest/webcomponents/test_custom_element_clone_callbacks.html b/dom/tests/mochitest/webcomponents/test_custom_element_clone_callbacks.html
deleted file mode 100644
--- a/dom/tests/mochitest/webcomponents/test_custom_element_clone_callbacks.html
+++ /dev/null
@@ -1,54 +0,0 @@
-<!DOCTYPE HTML>
-<html>
-<!--
-https://bugzilla.mozilla.org/show_bug.cgi?id=1081039
--->
-<head>
-  <title>Test callbacks for cloned custom elements.</title>
-  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
-  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
-</head>
-<body>
-<a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=1081039">Bug 1081039</a>
-<script>
-
-SimpleTest.waitForExplicitFinish();
-
-// Test to make sure created callback is called on clones that are upgraded and clones
-// created after registering the custom element.
-
-var callbackCalledOnUpgrade = false;
-var callbackCalledOnClone = false;
-
-var foo = document.createElement("x-foo");
-var fooClone = foo.cloneNode(true);
-
-var p = Object.create(HTMLElement.prototype);
-p.createdCallback = function() {
-  is(this.__proto__, p, "Correct prototype should be set on custom elements.");
-
-  if (this == fooClone) {
-    // Callback called for the element created before registering the custom element.
-    // Should be called on element upgrade.
-    is(callbackCalledOnUpgrade, false, "Upgrade should only be called once per clone.");
-    callbackCalledOnUpgrade = true;
-  } else if (this != foo) {
-    // Callback called for the element created after registering the custom element.
-    is(callbackCalledOnClone, false, "Upgrade should only be called once per clone.");
-    callbackCalledOnClone = true;
-  }
-
-  if (callbackCalledOnUpgrade && callbackCalledOnClone) {
-    SimpleTest.finish();
-  }
-};
-
-document.registerElement("x-foo", { prototype: p });
-
-var anotherFooClone = foo.cloneNode(true);
-
-SimpleTest.waitForExplicitFinish();
-
-</script>
-</body>
-</html>
diff --git a/testing/web-platform/meta/custom-elements/reaction-timing.html.ini b/testing/web-platform/meta/custom-elements/reaction-timing.html.ini
deleted file mode 100644
--- a/testing/web-platform/meta/custom-elements/reaction-timing.html.ini
+++ /dev/null
@@ -1,5 +0,0 @@
-[reaction-timing.html]
-  type: testharness
-  [Calling Node.prototype.cloneNode(false) must push a new element queue to the processing stack]
-    expected: FAIL
-
diff --git a/testing/web-platform/meta/custom-elements/reactions/Document.html.ini b/testing/web-platform/meta/custom-elements/reactions/Document.html.ini
--- a/testing/web-platform/meta/custom-elements/reactions/Document.html.ini
+++ b/testing/web-platform/meta/custom-elements/reactions/Document.html.ini
@@ -1,16 +1,13 @@
 [Document.html]
   type: testharness
   [importNode on Document must construct a new custom element when importing a custom element]
     expected: FAIL
 
-  [importNode on Document must construct a new custom element when importing a custom element from a template]
-    expected: FAIL
-
   [execCommand on Document must enqueue a disconnected reaction when deleting a custom element from a contenteditable element]
     expected: FAIL
 
   [body on Document must enqueue disconnectedCallback when removing a custom element]
     expected: FAIL
 
   [open on Document must enqueue disconnectedCallback when removing a custom element]
     expected: FAIL
diff --git a/testing/web-platform/meta/custom-elements/reactions/Node.html.ini b/testing/web-platform/meta/custom-elements/reactions/Node.html.ini
deleted file mode 100644
--- a/testing/web-platform/meta/custom-elements/reactions/Node.html.ini
+++ /dev/null
@@ -1,10 +0,0 @@
-[Node.html]
-  type: testharness
-  [cloneNode on Node must enqueue an attributeChanged reaction when cloning an element with an observed attribute]
-    expected: FAIL
-
-  [cloneNode on Node must not enqueue an attributeChanged reaction when cloning an element with an unobserved attribute]
-    expected: FAIL
-
-  [cloneNode on Node must enqueue an attributeChanged reaction when cloning an element only for observed attributes]
-    expected: FAIL
diff --git a/testing/web-platform/meta/custom-elements/reactions/Range.html.ini b/testing/web-platform/meta/custom-elements/reactions/Range.html.ini
--- a/testing/web-platform/meta/custom-elements/reactions/Range.html.ini
+++ b/testing/web-platform/meta/custom-elements/reactions/Range.html.ini
@@ -1,14 +1,5 @@
 [Range.html]
   type: testharness
-  [cloneContents on Range must enqueue an attributeChanged reaction when cloning an element with an observed attribute]
-    expected: FAIL
-
-  [cloneContents on Range must not enqueue an attributeChanged reaction when cloning an element with an unobserved attribute]
-    expected: FAIL
-
-  [cloneContents on Range must enqueue an attributeChanged reaction when cloning an element only for observed attributes]
-    expected: FAIL
-
   [createContextualFragment on Range must construct a custom element]
     expected: FAIL
 
diff --git a/testing/web-platform/meta/custom-elements/upgrading/Node-cloneNode.html.ini b/testing/web-platform/meta/custom-elements/upgrading/Node-cloneNode.html.ini
deleted file mode 100644
--- a/testing/web-platform/meta/custom-elements/upgrading/Node-cloneNode.html.ini
+++ /dev/null
@@ -1,14 +0,0 @@
-[Node-cloneNode.html]
-  type: testharness
-  [Node.prototype.cloneNode(true) must set parentNode, previousSibling, and nextSibling before upgrading custom elements]
-    expected: FAIL
-
-  [HTMLElement constructor must throw an InvalidStateError when the top of the construction stack is marked AlreadyConstructed due to a custom element constructor constructing itself after super() call]
-    expected: FAIL
-
-  [HTMLElement constructor must throw an InvalidStateError when the top of the construction stack is marked AlreadyConstructed due to a custom element constructor constructing itself before super() call]
-    expected: FAIL
-
-  [Upgrading a custom element must throw InvalidStateError when the custom element's constructor returns another element]
-    expected: FAIL
-
