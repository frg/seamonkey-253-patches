# HG changeset patch
# User Mantaroh Yoshinaga <mantaroh@gmail.com>
# Date 1510731078 -32400
#      Wed Nov 15 16:31:18 2017 +0900
# Node ID b78cea997533d47a5b0ff5f719329c4ccd5514d0
# Parent  154ec3c19c88655ffdf1d1684d61de19f1d57bea
Bug 1409971 - Part 3. Set nsIPrintSettings::IsCancelled to true in order to cancel print job. r=mconley

In content process, we should set nsIPrintSettings::IsCancelled to true in order
to cancel the print job. nsPrintEngine use this flag for cancelling.

MozReview-Commit-ID: EqnNJOlIm5s

diff --git a/toolkit/components/printingui/ipc/PrintProgressDialogChild.cpp b/toolkit/components/printingui/ipc/PrintProgressDialogChild.cpp
--- a/toolkit/components/printingui/ipc/PrintProgressDialogChild.cpp
+++ b/toolkit/components/printingui/ipc/PrintProgressDialogChild.cpp
@@ -14,18 +14,20 @@ using mozilla::Unused;
 namespace mozilla {
 namespace embedding {
 
 NS_IMPL_ISUPPORTS(PrintProgressDialogChild,
                   nsIWebProgressListener,
                   nsIPrintProgressParams)
 
 PrintProgressDialogChild::PrintProgressDialogChild(
-  nsIObserver* aOpenObserver) :
-  mOpenObserver(aOpenObserver)
+  nsIObserver* aOpenObserver,
+  nsIPrintSettings* aPrintSettings) :
+  mOpenObserver(aOpenObserver),
+  mPrintSettings(aPrintSettings)
 {
 }
 
 PrintProgressDialogChild::~PrintProgressDialogChild()
 {
   // When the printing engine stops supplying information about printing
   // progress, it'll drop references to us and destroy us. We need to signal
   // the parent to decrement its refcount, as well as prevent it from attempting
@@ -41,16 +43,19 @@ PrintProgressDialogChild::RecvDialogOpen
   // nullptrs.
   mOpenObserver->Observe(nullptr, nullptr, nullptr);
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
 PrintProgressDialogChild::RecvCancelledCurrentJob()
 {
+  if (mPrintSettings) {
+    mPrintSettings->SetIsCancelled(true);
+  }
   return IPC_OK();
 }
 
 // nsIWebProgressListener
 
 NS_IMETHODIMP
 PrintProgressDialogChild::OnStateChange(nsIWebProgress* aProgress,
                                         nsIRequest* aRequest,
diff --git a/toolkit/components/printingui/ipc/PrintProgressDialogChild.h b/toolkit/components/printingui/ipc/PrintProgressDialogChild.h
--- a/toolkit/components/printingui/ipc/PrintProgressDialogChild.h
+++ b/toolkit/components/printingui/ipc/PrintProgressDialogChild.h
@@ -2,41 +2,44 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_embedding_PrintProgressDialogChild_h
 #define mozilla_embedding_PrintProgressDialogChild_h
 
 #include "mozilla/embedding/PPrintProgressDialogChild.h"
 #include "nsIPrintProgressParams.h"
+#include "nsIPrintSettings.h"
 #include "nsIWebProgressListener.h"
 
 class nsIObserver;
 
 namespace mozilla {
 namespace embedding {
 
 class PrintProgressDialogChild final : public PPrintProgressDialogChild,
                                        public nsIWebProgressListener,
                                        public nsIPrintProgressParams
 {
   NS_DECL_ISUPPORTS
   NS_DECL_NSIWEBPROGRESSLISTENER
   NS_DECL_NSIPRINTPROGRESSPARAMS
 
 public:
-  MOZ_IMPLICIT PrintProgressDialogChild(nsIObserver* aOpenObserver);
+    MOZ_IMPLICIT PrintProgressDialogChild(nsIObserver* aOpenObserver,
+                                          nsIPrintSettings* aPrintSettings);
 
   virtual mozilla::ipc::IPCResult RecvDialogOpened() override;
 
   virtual mozilla::ipc::IPCResult RecvCancelledCurrentJob() override;
 
 private:
   virtual ~PrintProgressDialogChild();
   nsCOMPtr<nsIObserver> mOpenObserver;
   nsString mDocTitle;
   nsString mDocURL;
+  nsCOMPtr<nsIPrintSettings> mPrintSettings;
 };
 
 } // namespace embedding
 } // namespace mozilla
 
 #endif
diff --git a/toolkit/components/printingui/ipc/nsPrintingProxy.cpp b/toolkit/components/printingui/ipc/nsPrintingProxy.cpp
--- a/toolkit/components/printingui/ipc/nsPrintingProxy.cpp
+++ b/toolkit/components/printingui/ipc/nsPrintingProxy.cpp
@@ -143,17 +143,17 @@ nsPrintingProxy::ShowProgress(mozIDOMWin
   nsCOMPtr<nsPIDOMWindowOuter> pwin = nsPIDOMWindowOuter::From(parent);
   NS_ENSURE_STATE(pwin);
   nsCOMPtr<nsIDocShell> docShell = pwin->GetDocShell();
   NS_ENSURE_STATE(docShell);
   nsCOMPtr<nsITabChild> tabchild = docShell->GetTabChild();
   TabChild* pBrowser = static_cast<TabChild*>(tabchild.get());
 
   RefPtr<PrintProgressDialogChild> dialogChild =
-    new PrintProgressDialogChild(openDialogObserver);
+    new PrintProgressDialogChild(openDialogObserver, printSettings);
 
   SendPPrintProgressDialogConstructor(dialogChild);
 
   // Get the RemotePrintJob if we have one available.
   RefPtr<mozilla::layout::RemotePrintJobChild> remotePrintJob;
   if (printSettings) {
     nsCOMPtr<nsIPrintSession> printSession;
     nsresult rv = printSettings->GetPrintSession(getter_AddRefs(printSession));
