# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1517335137 18000
# Node ID ecf18629444aa6514b36dbd137c83ba10f60b66f
# Parent  12343acacd5a27e24abc040735235a7c8bd0fa11
Bug 1389759 - Fix GPUProcessManager::EnsureGPUReady how did not guarantee the GPU process state is consistent. r=rhunt

GPUProcessManager::EnsureGPUReady promises that its state will be
consistent after returning. Either the GPU process is ready to be used,
or there is no GPU process at all. In the case it is attempted to
synchronously initialize the GPUChild with the device data and failed,
it broke that promise. This is because the GPU process was still setup,
but we weren't going to use it. This became a problem with the
CompositorManagerChild because it uses the process token as an
identifier, and it should have been reset to 0 in this case.

Now if GPUChild::EnsureGPUReady (the initialization step) fails, we
disable the GPU process entirely. This ensures our internal state is
consistent and the callers expectations are upheld.

diff --git a/gfx/ipc/GPUProcessManager.cpp b/gfx/ipc/GPUProcessManager.cpp
--- a/gfx/ipc/GPUProcessManager.cpp
+++ b/gfx/ipc/GPUProcessManager.cpp
@@ -199,18 +199,27 @@ GPUProcessManager::EnsureGPUReady()
     if (!mProcess->WaitForLaunch()) {
       // If this fails, we should have fired OnProcessLaunchComplete and
       // removed the process.
       MOZ_ASSERT(!mProcess && !mGPUChild);
       return false;
     }
   }
 
-  if (mGPUChild && mGPUChild->EnsureGPUReady()) {
-    return true;
+  if (mGPUChild) {
+    if (mGPUChild->EnsureGPUReady()) {
+      return true;
+    }
+
+    // If the initialization above fails, we likely have a GPU process teardown
+    // waiting in our message queue (or will soon). We need to ensure we don't
+    // restart it later because if we fail here, our callers assume they should
+    // fall back to a combined UI/GPU process. This also ensures our internal
+    // state is consistent (e.g. process token is reset).
+    DisableGPUProcess("Failed to initialize GPU process");
   }
 
   return false;
 }
 
 void
 GPUProcessManager::EnsureProtocolsReady()
 {
