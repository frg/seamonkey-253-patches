# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1507090878 18000
#      Tue Oct 03 23:21:18 2017 -0500
# Node ID 9fd0fbf3d2b5182a6c54c12a038ba22f9f1041d8
# Parent  045c54df4bd08744aa9bd68a462a50b2ede32a4c
servo: Merge #18736 - Remove nsTFixedString<T> (from nnethercote:bug-1403506); r=erahm,mystor

This is for https://bugzilla.mozilla.org/show_bug.cgi?id=1403506, which is r=erahm,mystor.

<!-- Thank you for contributing to Servo! Please replace each `[ ]` by `[X]` when the step is complete, and replace `__` with appropriate data: -->
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [ ] These changes fix https://bugzilla.mozilla.org/show_bug.cgi?id=1403506.

<!-- Either: -->
- [ ] There are tests for these changes OR
- [X] These changes do not require tests because they are tested in Gecko.

<!-- Also, please make sure that "Allow edits from maintainers" checkbox is checked, so that we can help you if you get stuck somewhere along the way.-->

<!-- Pull requests that do not address these steps are welcome, but they will require additional verification as part of the review process. -->

Source-Repo: https://github.com/servo/servo
Source-Revision: c0f404999c5fe40cba3a1d5d231a922e922a55b3

diff --git a/servo/components/style/gecko/rules.rs b/servo/components/style/gecko/rules.rs
--- a/servo/components/style/gecko/rules.rs
+++ b/servo/components/style/gecko/rules.rs
@@ -10,16 +10,17 @@ use computed_values::font_family::Family
 use counter_style;
 use cssparser::UnicodeRange;
 use font_face::{FontFaceRuleData, Source, FontDisplay, FontWeight};
 use gecko_bindings::bindings;
 use gecko_bindings::structs::{self, nsCSSFontFaceRule, nsCSSValue};
 use gecko_bindings::structs::{nsCSSCounterDesc, nsCSSCounterStyleRule};
 use gecko_bindings::sugar::ns_css_value::ToNsCssValue;
 use gecko_bindings::sugar::refptr::{RefPtr, UniqueRefPtr};
+use nsstring::nsString;
 use properties::longhands::font_language_override;
 use shared_lock::{ToCssWithGuard, SharedRwLockReadGuard};
 use std::{fmt, str};
 use values::generics::FontSettings;
 
 /// A @font-face rule
 pub type FontFaceRule = RefPtr<nsCSSFontFaceRule>;
 
@@ -196,17 +197,17 @@ impl From<FontFaceRuleData> for FontFace
         data.set_descriptors(&mut result.mDecl.mDescriptors);
         result.get()
     }
 }
 
 impl ToCssWithGuard for FontFaceRule {
     fn to_css<W>(&self, _guard: &SharedRwLockReadGuard, dest: &mut W) -> fmt::Result
     where W: fmt::Write {
-        ns_auto_string!(css_text);
+        let mut css_text = nsString::new();
         unsafe {
             bindings::Gecko_CSSFontFaceRule_GetCssText(self.get(), &mut *css_text);
         }
         write!(dest, "{}", css_text)
     }
 }
 
 /// A @counter-style rule
@@ -233,17 +234,17 @@ impl From<counter_style::CounterStyleRul
         data.set_descriptors(&mut result.mValues);
         result.get()
     }
 }
 
 impl ToCssWithGuard for CounterStyleRule {
     fn to_css<W>(&self, _guard: &SharedRwLockReadGuard, dest: &mut W) -> fmt::Result
     where W: fmt::Write {
-        ns_auto_string!(css_text);
+        let mut css_text = nsString::new();
         unsafe {
             bindings::Gecko_CSSCounterStyle_GetCssText(self.get(), &mut *css_text);
         }
         write!(dest, "{}", css_text)
     }
 }
 
 /// The type of nsCSSCounterStyleRule::mValues
diff --git a/servo/components/style/gecko_bindings/nsstring_vendor/src/lib.rs b/servo/components/style/gecko_bindings/nsstring_vendor/src/lib.rs
--- a/servo/components/style/gecko_bindings/nsstring_vendor/src/lib.rs
+++ b/servo/components/style/gecko_bindings/nsstring_vendor/src/lib.rs
@@ -17,22 +17,26 @@
 //!
 //! Use `ns[C]Str` (`nsDependent[C]String` in C++) as an intermediate between
 //! borrowed rust data structures (such as `&str` and `&[u16]`) and `&{mut,}
 //! nsA[C]String` (using `ns[C]Str::from(value)`). These conversions should not
 //! perform any allocations. This type is not safe to share with `C++` as a
 //! struct field, but passing the borrowed `&{mut,} nsA[C]String` over FFI is
 //! safe.
 //!
-//! Use `nsFixed[C]String` or `ns_auto_[c]string!` for dynamic stack allocated
-//! strings which are expected to hold short string values.
-//!
 //! Use `*{const,mut} nsA[C]String` (`{const,} nsA[C]String*` in C++) for
 //! function arguments passed across the rust/C++ language boundary.
 //!
+//! There is currently no Rust equivalent to nsAuto[C]String. Implementing a
+//! type that contains a pointer to an inline buffer is difficult in Rust due
+//! to its move semantics, which require that it be safe to move a value by
+//! copying its bits. If such a type is genuinely needed at some point,
+//! https://bugzilla.mozilla.org/show_bug.cgi?id=1403506#c6 has a sketch of how
+//! to emulate it via macros.
+//!
 //! # String Types
 //!
 //! ## `nsA[C]String`
 //!
 //! The core types in this module are `nsAString` and `nsACString`. These types
 //! are zero-sized as far as rust is concerned, and are safe to pass around
 //! behind both references (in rust code), and pointers (in C++ code). They
 //! represent a handle to a XPCOM string which holds either `u16` or `u8`
@@ -93,46 +97,16 @@
 //! When passing this type by reference, prefer passing a `&nsA[C]String` or
 //! `&mut nsA[C]String`. to passing this type.
 //!
 //! When passing this type across the language boundary, pass it as `*const
 //! nsA[C]String` for an immutable reference, or `*mut nsA[C]String` for a
 //! mutable reference. This struct may also be included in `#[repr(C)]` structs
 //! shared with C++.
 //!
-//! ## `nsFixed[C]String<'a>`
-//!
-//! This type is a string type with fixed backing storage. It is created with
-//! `nsFixed[C]String::new(buffer)`, passing a mutable reference to a buffer as
-//! the argument. This buffer will be used as backing storage whenever the
-//! resulting string will fit within it, falling back to heap allocations only
-//! when the string size exceeds that of the backing buffer.
-//!
-//! Like `ns[C]String`, this type dereferences to `nsA[C]String` which provides
-//! the methods for manipulating the type, and is not `#[repr(C)]`.
-//!
-//! When passing this type by reference, prefer passing a `&nsA[C]String` or
-//! `&mut nsA[C]String`. to passing this type.
-//!
-//! When passing this type across the language boundary, pass it as `*const
-//! nsA[C]String` for an immutable reference, or `*mut nsA[C]String` for a
-//! mutable reference. This struct may also be included in `#[repr(C)]`
-//! structs shared with C++, although `nsFixed[C]String` objects are uncommon
-//! as struct members.
-//!
-//! ## `ns_auto_[c]string!($name)`
-//!
-//! This is a helper macro which defines a fixed size, (currently 64 character),
-//! backing array on the stack, and defines a local variable with name `$name`
-//! which is a `nsFixed[C]String` using this buffer as its backing storage.
-//!
-//! Usage of this macro is similar to the C++ type `nsAuto[C]String`, but could
-//! not be implemented as a basic type due to the differences between rust and
-//! C++'s move semantics.
-//!
 //! ## `ns[C]StringRepr`
 //!
 //! This crate also provides the type `ns[C]StringRepr` which acts conceptually
 //! similar to an `ns[C]String`, however, it does not have a `Drop`
 //! implementation.
 //!
 //! If this type is dropped in rust, it will not free its backing storage. This
 //! can be useful when implementing FFI types which contain `ns[C]String` members
@@ -150,43 +124,43 @@ use std::borrow;
 use std::slice;
 use std::mem;
 use std::fmt;
 use std::cmp;
 use std::str;
 use std::u32;
 use std::os::raw::c_void;
 
-//////////////////////////////////
-// Internal Implemenation Flags //
-//////////////////////////////////
+///////////////////////////////////
+// Internal Implementation Flags //
+///////////////////////////////////
 
 mod data_flags {
     bitflags! {
         // While this has the same layout as u16, it cannot be passed
         // over FFI safely as a u16.
         #[repr(C)]
         pub flags DataFlags : u16 {
             const TERMINATED = 1 << 0, // IsTerminated returns true
             const VOIDED = 1 << 1, // IsVoid returns true
             const SHARED = 1 << 2, // mData points to a heap-allocated, shared buffer
             const OWNED = 1 << 3, // mData points to a heap-allocated, raw buffer
-            const FIXED = 1 << 4, // mData points to a fixed-size writable, dependent buffer
+            const INLINE = 1 << 4, // mData points to a writable, inline buffer
             const LITERAL = 1 << 5, // mData points to a string literal; TERMINATED will also be set
         }
     }
 }
 
 mod class_flags {
     bitflags! {
         // While this has the same layout as u16, it cannot be passed
         // over FFI safely as a u16.
         #[repr(C)]
         pub flags ClassFlags : u16 {
-            const FIXED = 1 << 0, // |this| is of type nsTFixedString
+            const INLINE = 1 << 0, // |this|'s buffer is inline
             const NULL_TERMINATED = 1 << 1, // |this| requires its buffer is null-terminated
         }
     }
 }
 
 use data_flags::DataFlags;
 use class_flags::ClassFlags;
 
@@ -196,17 +170,16 @@ use class_flags::ClassFlags;
 
 macro_rules! define_string_types {
     {
         char_t = $char_t: ty;
 
         AString = $AString: ident;
         String = $String: ident;
         Str = $Str: ident;
-        FixedString = $FixedString: ident;
 
         StringLike = $StringLike: ident;
         StringAdapter = $StringAdapter: ident;
 
         StringRepr = $StringRepr: ident;
 
         drop = $drop: ident;
         assign = $assign: ident, $fallible_assign: ident;
@@ -430,22 +403,16 @@ macro_rules! define_string_types {
         }
 
         impl<'a> cmp::PartialEq<$Str<'a>> for $AString {
             fn eq(&self, other: &$Str<'a>) -> bool {
                 self.eq(&**other)
             }
         }
 
-        impl<'a> cmp::PartialEq<$FixedString<'a>> for $AString {
-            fn eq(&self, other: &$FixedString<'a>) -> bool {
-                self.eq(&**other)
-            }
-        }
-
         #[repr(C)]
         pub struct $Str<'a> {
             hdr: $StringRepr,
             _marker: PhantomData<&'a [$char_t]>,
         }
 
         impl $Str<'static> {
             pub fn new() -> $Str<'static> {
@@ -701,110 +668,16 @@ macro_rules! define_string_types {
         }
 
         impl<'a> cmp::PartialEq<&'a str> for $String {
             fn eq(&self, other: &&'a str) -> bool {
                 $AString::eq(self, *other)
             }
         }
 
-        /// A nsFixed[C]String is a string which uses a fixed size mutable
-        /// backing buffer for storing strings which will fit within that
-        /// buffer, rather than using heap allocations.
-        #[repr(C)]
-        pub struct $FixedString<'a> {
-            base: $String,
-            capacity: u32,
-            buffer: *mut $char_t,
-            _marker: PhantomData<&'a mut [$char_t]>,
-        }
-
-        impl<'a> $FixedString<'a> {
-            pub fn new(buf: &'a mut [$char_t]) -> $FixedString<'a> {
-                let len = buf.len();
-                assert!(len < (u32::MAX as usize));
-                let buf_ptr = buf.as_mut_ptr();
-                $FixedString {
-                    base: $String {
-                        hdr: $StringRepr::new(class_flags::FIXED | class_flags::NULL_TERMINATED),
-                    },
-                    capacity: len as u32,
-                    buffer: buf_ptr,
-                    _marker: PhantomData,
-                }
-            }
-        }
-
-        impl<'a> Deref for $FixedString<'a> {
-            type Target = $AString;
-            fn deref(&self) -> &$AString {
-                &self.base
-            }
-        }
-
-        impl<'a> DerefMut for $FixedString<'a> {
-            fn deref_mut(&mut self) -> &mut $AString {
-                &mut self.base
-            }
-        }
-
-        impl<'a> AsRef<[$char_t]> for $FixedString<'a> {
-            fn as_ref(&self) -> &[$char_t] {
-                &self
-            }
-        }
-
-        impl<'a> fmt::Write for $FixedString<'a> {
-            fn write_str(&mut self, s: &str) -> Result<(), fmt::Error> {
-                $AString::write_str(self, s)
-            }
-        }
-
-        impl<'a> fmt::Display for $FixedString<'a> {
-            fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
-                <$AString as fmt::Display>::fmt(self, f)
-            }
-        }
-
-        impl<'a> fmt::Debug for $FixedString<'a> {
-            fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
-                <$AString as fmt::Debug>::fmt(self, f)
-            }
-        }
-
-        impl<'a> cmp::PartialEq for $FixedString<'a> {
-            fn eq(&self, other: &$FixedString<'a>) -> bool {
-                $AString::eq(self, other)
-            }
-        }
-
-        impl<'a> cmp::PartialEq<[$char_t]> for $FixedString<'a> {
-            fn eq(&self, other: &[$char_t]) -> bool {
-                $AString::eq(self, other)
-            }
-        }
-
-        impl<'a, 'b> cmp::PartialEq<&'b [$char_t]> for $FixedString<'a> {
-            fn eq(&self, other: &&'b [$char_t]) -> bool {
-                $AString::eq(self, *other)
-            }
-        }
-
-        impl<'a> cmp::PartialEq<str> for $FixedString<'a> {
-            fn eq(&self, other: &str) -> bool {
-                $AString::eq(self, other)
-            }
-        }
-
-        impl<'a, 'b> cmp::PartialEq<&'b str> for $FixedString<'a> {
-            fn eq(&self, other: &&'b str) -> bool {
-                $AString::eq(self, *other)
-            }
-        }
-
         /// An adapter type to allow for passing both types which coerce to
         /// &[$char_type], and &$AString to a function, while still performing
         /// optimized operations when passed the $AString.
         pub enum $StringAdapter<'a> {
             Borrowed($Str<'a>),
             Abstract(&'a $AString),
         }
 
@@ -864,22 +737,16 @@ macro_rules! define_string_types {
         }
 
         impl $StringLike for $String {
             fn adapt(&self) -> $StringAdapter {
                 $StringAdapter::Abstract(self)
             }
         }
 
-        impl<'a> $StringLike for $FixedString<'a> {
-            fn adapt(&self) -> $StringAdapter {
-                $StringAdapter::Abstract(self)
-            }
-        }
-
         impl $StringLike for [$char_t] {
             fn adapt(&self) -> $StringAdapter {
                 $StringAdapter::Borrowed($Str::from(self))
             }
         }
 
         impl $StringLike for Vec<$char_t> {
             fn adapt(&self) -> $StringAdapter {
@@ -900,17 +767,16 @@ macro_rules! define_string_types {
 ///////////////////////////////////////////
 
 define_string_types! {
     char_t = u8;
 
     AString = nsACString;
     String = nsCString;
     Str = nsCStr;
-    FixedString = nsFixedCString;
 
     StringLike = nsCStringLike;
     StringAdapter = nsCStringAdapter;
 
     StringRepr = nsCStringRepr;
 
     drop = Gecko_FinalizeCString;
     assign = Gecko_AssignCString, Gecko_FallibleAssignCString;
@@ -1024,35 +890,26 @@ impl nsCStringLike for String {
 }
 
 impl nsCStringLike for Box<str> {
     fn adapt(&self) -> nsCStringAdapter {
         nsCStringAdapter::Borrowed(nsCStr::from(&self[..]))
     }
 }
 
-#[macro_export]
-macro_rules! ns_auto_cstring {
-    ($name:ident) => {
-        let mut buf: [u8; 64] = [0; 64];
-        let mut $name = $crate::nsFixedCString::new(&mut buf);
-    }
-}
-
 ///////////////////////////////////////////
 // Bindings for nsString (u16 char type) //
 ///////////////////////////////////////////
 
 define_string_types! {
     char_t = u16;
 
     AString = nsAString;
     String = nsString;
     Str = nsStr;
-    FixedString = nsFixedString;
 
     StringLike = nsStringLike;
     StringAdapter = nsStringAdapter;
 
     StringRepr = nsStringRepr;
 
     drop = Gecko_FinalizeString;
     assign = Gecko_AssignString, Gecko_FallibleAssignString;
@@ -1124,24 +981,16 @@ impl fmt::Debug for nsAString {
 }
 
 impl cmp::PartialEq<str> for nsAString {
     fn eq(&self, other: &str) -> bool {
         other.encode_utf16().eq(self.iter().cloned())
     }
 }
 
-#[macro_export]
-macro_rules! ns_auto_string {
-    ($name:ident) => {
-        let mut buf: [u16; 64] = [0; 64];
-        let mut $name = $crate::nsFixedString::new(&mut buf);
-    }
-}
-
 #[cfg(not(feature = "gecko_debug"))]
 #[allow(non_snake_case)]
 unsafe fn Gecko_IncrementStringAdoptCount(_: *mut c_void) {}
 
 extern "C" {
     #[cfg(feature = "gecko_debug")]
     fn Gecko_IncrementStringAdoptCount(data: *mut c_void);
 
diff --git a/servo/components/style/lib.rs b/servo/components/style/lib.rs
--- a/servo/components/style/lib.rs
+++ b/servo/components/style/lib.rs
@@ -61,17 +61,16 @@ extern crate lazy_static;
 extern crate log;
 extern crate lru_cache;
 #[cfg(feature = "gecko")] #[macro_use] extern crate malloc_size_of;
 #[cfg(feature = "gecko")] #[macro_use] extern crate malloc_size_of_derive;
 #[allow(unused_extern_crates)]
 #[macro_use]
 extern crate matches;
 #[cfg(feature = "gecko")]
-#[macro_use]
 pub extern crate nsstring_vendor as nsstring;
 #[cfg(feature = "gecko")] extern crate num_cpus;
 extern crate num_integer;
 extern crate num_traits;
 extern crate ordered_float;
 extern crate owning_ref;
 extern crate parking_lot;
 extern crate pdqsort;
