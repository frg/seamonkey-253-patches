# HG changeset patch
# User Jed Davis <jld@mozilla.com>
# Date 1508975902 21600
#      Wed Oct 25 17:58:22 2017 -0600
# Node ID 7e53467676cac3f79f79c3f183d6b5cc58835aa2
# Parent  255a3f3f51aeb616d7f839d4034fd056997694be
Bug 1410241 - Don't call destructors on objects we use in the SIGSYS handler. r=gcp

MozReview-Commit-ID: LAgORUSvDh9

diff --git a/security/sandbox/linux/Sandbox.cpp b/security/sandbox/linux/Sandbox.cpp
--- a/security/sandbox/linux/Sandbox.cpp
+++ b/security/sandbox/linux/Sandbox.cpp
@@ -34,17 +34,16 @@
 #include <sys/prctl.h>
 #include <sys/ptrace.h>
 #include <sys/syscall.h>
 #include <sys/time.h>
 #include <unistd.h>
 
 #include "mozilla/Array.h"
 #include "mozilla/Atomics.h"
-#include "mozilla/Maybe.h"
 #include "mozilla/Range.h"
 #include "mozilla/SandboxInfo.h"
 #include "mozilla/Span.h"
 #include "mozilla/UniquePtr.h"
 #include "mozilla/Unused.h"
 #include "sandbox/linux/bpf_dsl/codegen.h"
 #include "sandbox/linux/bpf_dsl/dump_bpf.h"
 #include "sandbox/linux/bpf_dsl/policy.h"
@@ -83,17 +82,17 @@ int gSeccompTsyncBroadcastSignum = 0;
 
 namespace mozilla {
 
 static bool gSandboxCrashOnError = false;
 
 // This is initialized by SandboxSetCrashFunc().
 SandboxCrashFunc gSandboxCrashFunc;
 
-static Maybe<SandboxReporterClient> gSandboxReporterClient;
+static SandboxReporterClient* gSandboxReporterClient;
 static UniquePtr<SandboxChroot> gChrootHelper;
 static void (*gChromiumSigSysHandler)(int, siginfo_t*, void*);
 
 // Test whether a ucontext, interpreted as the state after a syscall,
 // indicates the given error.  See also sandbox::Syscall::PutValueInUcontext.
 static bool
 ContextIsError(const ucontext_t *aContext, int aError)
 {
@@ -456,17 +455,17 @@ ApplySandboxWithTSync(sock_fprog* aFilte
   }
 }
 
 // Common code for sandbox startup.
 static void
 SetCurrentProcessSandbox(UniquePtr<sandbox::bpf_dsl::Policy> aPolicy)
 {
   MOZ_ASSERT(gSandboxCrashFunc);
-  MOZ_RELEASE_ASSERT(gSandboxReporterClient.isSome());
+  MOZ_RELEASE_ASSERT(gSandboxReporterClient != nullptr);
 
   // Note: PolicyCompiler borrows the policy and registry for its
   // lifetime, but does not take ownership of them.
   sandbox::bpf_dsl::PolicyCompiler compiler(aPolicy.get(),
                                             sandbox::Trap::Registry());
   sandbox::CodeGen::Program program = compiler.Compile();
   if (SandboxInfo::Get().Test(SandboxInfo::kVerbose)) {
     sandbox::bpf_dsl::DumpBPF::PrintProgram(program);
@@ -679,26 +678,28 @@ SetContentProcessSandbox(int aBrokerFd, 
 {
   if (!SandboxInfo::Get().Test(SandboxInfo::kEnabledForContent)) {
     if (aBrokerFd >= 0) {
       close(aBrokerFd);
     }
     return false;
   }
 
-  gSandboxReporterClient.emplace(aFileProcess ? SandboxReport::ProcType::FILE
-                                              : SandboxReport::ProcType::CONTENT);
+  auto procType = aFileProcess
+    ? SandboxReport::ProcType::FILE
+    : SandboxReport::ProcType::CONTENT;
+  gSandboxReporterClient = new SandboxReporterClient(procType);
 
   // This needs to live until the process exits.
-  static Maybe<SandboxBrokerClient> sBroker;
+  static SandboxBrokerClient* sBroker;
   if (aBrokerFd >= 0) {
-    sBroker.emplace(aBrokerFd);
+    sBroker = new SandboxBrokerClient(aBrokerFd);
   }
 
-  SetCurrentProcessSandbox(GetContentSandboxPolicy(sBroker.ptrOr(nullptr),
+  SetCurrentProcessSandbox(GetContentSandboxPolicy(sBroker,
                                                    aSyscallWhitelist));
   return true;
 }
 #endif // MOZ_CONTENT_SANDBOX
 
 #ifdef MOZ_GMP_SANDBOX
 /**
  * Starts the seccomp sandbox for a media plugin process.  Should be
@@ -714,17 +715,18 @@ SetContentProcessSandbox(int aBrokerFd, 
 void
 SetMediaPluginSandbox(const char *aFilePath)
 {
   MOZ_RELEASE_ASSERT(aFilePath != nullptr);
   if (!SandboxInfo::Get().Test(SandboxInfo::kEnabledForMedia)) {
     return;
   }
 
-  gSandboxReporterClient.emplace(SandboxReport::ProcType::MEDIA_PLUGIN);
+  gSandboxReporterClient =
+    new SandboxReporterClient(SandboxReport::ProcType::MEDIA_PLUGIN);
 
   SandboxOpenedFile plugin(aFilePath);
   if (!plugin.IsOpen()) {
     SANDBOX_LOG_ERROR("failed to open plugin file %s: %s",
                       aFilePath, strerror(errno));
     MOZ_CRASH();
   }
 
