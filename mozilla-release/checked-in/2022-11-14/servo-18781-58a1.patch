# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1507460668 18000
# Node ID d5c376eb3d03125941c69875659e83a8ca29f301
# Parent  3ea1bc47a15b92a86dba680a38dedf245b52eb72
servo: Merge #18781 - style: Add a simple custom properties benchmark (from emilio:custom-props-bench); r=heycam

This is going to help the work in bug 1405411.

Source-Repo: https://github.com/servo/servo
Source-Revision: 47efcd5e52afd62dcd84ba350948039f67613e20

diff --git a/servo/components/style/custom_properties.rs b/servo/components/style/custom_properties.rs
--- a/servo/components/style/custom_properties.rs
+++ b/servo/components/style/custom_properties.rs
@@ -3,17 +3,16 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Support for [custom properties for cascading variables][custom].
 //!
 //! [custom]: https://drafts.csswg.org/css-variables/
 
 use Atom;
 use cssparser::{Delimiter, Parser, ParserInput, SourcePosition, Token, TokenSerializationType};
-use parser::ParserContext;
 use precomputed_hash::PrecomputedHash;
 use properties::{CSSWideKeyword, DeclaredValue};
 use selector_map::{PrecomputedHashSet, PrecomputedHashMap};
 use selectors::parser::SelectorParseErrorKind;
 use servo_arc::Arc;
 use std::ascii::AsciiExt;
 use std::borrow::{Borrow, Cow};
 use std::fmt;
@@ -236,25 +235,27 @@ impl ComputedValue {
     fn push_variable(&mut self, variable: &ComputedValue) {
         self.push(&variable.css, variable.first_token_type, variable.last_token_type)
     }
 }
 
 impl SpecifiedValue {
     /// Parse a custom property SpecifiedValue.
     pub fn parse<'i, 't>(
-        _context: &ParserContext,
         input: &mut Parser<'i, 't>,
     ) -> Result<Box<Self>, ParseError<'i>> {
         let mut references = PrecomputedHashSet::default();
-        let (first, css, last) = parse_self_contained_declaration_value(input, Some(&mut references))?;
+
+        let (first_token_type, css, last_token_type) =
+            parse_self_contained_declaration_value(input, Some(&mut references))?;
+
         Ok(Box::new(SpecifiedValue {
             css: css.into_owned(),
-            first_token_type: first,
-            last_token_type: last,
+            first_token_type,
+            last_token_type,
             references
         }))
     }
 }
 
 /// Parse the value of a non-custom property that contains `var()` references.
 pub fn parse_non_custom_with_var<'i, 't>
                                 (input: &mut Parser<'i, 't>)
diff --git a/servo/components/style/properties/properties.mako.rs b/servo/components/style/properties/properties.mako.rs
--- a/servo/components/style/properties/properties.mako.rs
+++ b/servo/components/style/properties/properties.mako.rs
@@ -1657,17 +1657,17 @@ impl PropertyDeclaration {
         let start = input.state();
         match id {
             PropertyId::Custom(property_name) => {
                 // FIXME: fully implement https://github.com/w3c/csswg-drafts/issues/774
                 // before adding skip_whitespace here.
                 // This probably affects some test results.
                 let value = match input.try(|i| CSSWideKeyword::parse(i)) {
                     Ok(keyword) => DeclaredValueOwned::CSSWideKeyword(keyword),
-                    Err(()) => match ::custom_properties::SpecifiedValue::parse(context, input) {
+                    Err(()) => match ::custom_properties::SpecifiedValue::parse(input) {
                         Ok(value) => DeclaredValueOwned::Value(value),
                         Err(e) => return Err(StyleParseErrorKind::new_invalid(name, e)),
                     }
                 };
                 declarations.push(PropertyDeclaration::Custom(property_name, value));
                 Ok(())
             }
             PropertyId::Longhand(id) => {
diff --git a/servo/components/style/thread_state.rs b/servo/components/style/thread_state.rs
--- a/servo/components/style/thread_state.rs
+++ b/servo/components/style/thread_state.rs
@@ -48,17 +48,19 @@ thread_types! {
 }
 
 thread_local!(static STATE: RefCell<Option<ThreadState>> = RefCell::new(None));
 
 /// Initializes the current thread state.
 pub fn initialize(x: ThreadState) {
     STATE.with(|ref k| {
         if let Some(ref s) = *k.borrow() {
-            panic!("Thread state already initialized as {:?}", s);
+            if x != *s {
+                panic!("Thread state already initialized as {:?}", s);
+            }
         }
         *k.borrow_mut() = Some(x);
     });
 }
 
 /// Initializes the current thread as a layout worker thread.
 pub fn initialize_layout_worker_thread() {
     initialize(LAYOUT | IN_WORKER);
diff --git a/servo/components/style/values/specified/image.rs b/servo/components/style/values/specified/image.rs
--- a/servo/components/style/values/specified/image.rs
+++ b/servo/components/style/values/specified/image.rs
@@ -904,28 +904,28 @@ impl Parse for ColorStop {
         Ok(ColorStop {
             color: RGBAColor::parse(context, input)?,
             position: input.try(|i| LengthOrPercentage::parse(context, i)).ok(),
         })
     }
 }
 
 impl Parse for PaintWorklet {
-    fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
+    fn parse<'i, 't>(
+        _context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<Self, ParseError<'i>> {
         input.expect_function_matching("paint")?;
         input.parse_nested_block(|input| {
             let name = Atom::from(&**input.expect_ident()?);
             let arguments = input.try(|input| {
                 input.expect_comma()?;
-                input.parse_comma_separated(|input| Ok(*SpecifiedValue::parse(context, input)?))
+                input.parse_comma_separated(|input| Ok(*SpecifiedValue::parse(input)?))
             }).unwrap_or(vec![]);
-            Ok(PaintWorklet {
-                name: name,
-                arguments: arguments,
-            })
+            Ok(PaintWorklet { name, arguments })
         })
     }
 }
 
 impl Parse for MozImageRect {
     fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
         input.try(|i| i.expect_function_matching("-moz-image-rect"))?;
         input.parse_nested_block(|i| {
diff --git a/servo/tests/unit/style/custom_properties.rs b/servo/tests/unit/style/custom_properties.rs
new file mode 100644
--- /dev/null
+++ b/servo/tests/unit/style/custom_properties.rs
@@ -0,0 +1,49 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+use cssparser::{Parser, ParserInput};
+use servo_arc::Arc;
+use style::custom_properties::{self, Name, SpecifiedValue, CustomPropertiesMap};
+use style::properties::DeclaredValue;
+use test::{self, Bencher};
+
+fn cascade(
+    name_and_value: &[(&str, &str)],
+    inherited: Option<&Arc<CustomPropertiesMap>>,
+) -> Option<Arc<CustomPropertiesMap>> {
+    let values = name_and_value.iter().map(|&(name, value)| {
+        let mut input = ParserInput::new(value);
+        let mut parser = Parser::new(&mut input);
+        (Name::from(name), SpecifiedValue::parse(&mut parser).unwrap())
+    }).collect::<Vec<_>>();
+
+    let mut custom_properties = None;
+    let mut seen = Default::default();
+    for &(ref name, ref val) in &values {
+        custom_properties::cascade(
+            &mut custom_properties,
+            inherited,
+            &mut seen,
+            name,
+            DeclaredValue::Value(val)
+        )
+    }
+
+    custom_properties::finish_cascade(custom_properties, inherited)
+}
+
+#[bench]
+fn cascade_custom_simple(b: &mut Bencher) {
+    b.iter(|| {
+        let parent = cascade(&[
+            ("foo", "10px"),
+            ("bar", "100px"),
+        ], None);
+
+        test::black_box(cascade(&[
+            ("baz", "calc(40em + 4px)"),
+            ("bazz", "calc(30em + 4px)"),
+        ], parent.as_ref()))
+    })
+}
diff --git a/servo/tests/unit/style/lib.rs b/servo/tests/unit/style/lib.rs
--- a/servo/tests/unit/style/lib.rs
+++ b/servo/tests/unit/style/lib.rs
@@ -19,16 +19,17 @@ extern crate servo_config;
 extern crate servo_url;
 #[macro_use] extern crate size_of_test;
 #[macro_use] extern crate style;
 extern crate style_traits;
 extern crate test;
 
 mod animated_properties;
 mod attr;
+mod custom_properties;
 mod keyframes;
 mod logical_geometry;
 mod media_queries;
 mod parsing;
 mod properties;
 mod rule_tree;
 mod size_of;
 #[path = "../stylo/specified_values.rs"]
