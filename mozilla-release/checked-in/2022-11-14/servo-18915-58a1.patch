# HG changeset patch
# User Cameron McCormack <cam@mcc.id.au>
# Date 1508220699 18000
# Node ID 0ae04a97c8a3e944b78d8e0d4640cd7358edb831
# Parent  e2ee6ae5e78d6b7daa72e62601f78e0a31835900
servo: Merge #18915 - style: Add FFI function for parsing IntersectionObserver rootMargin values (from heycam:observer); r=upsuper

Servo-side part of https://bugzilla.mozilla.org/show_bug.cgi?id=1408305, reviewed there by Xidorn.

Source-Repo: https://github.com/servo/servo
Source-Revision: ac74cd57a257fc95e0513524e196c91807c18bba

diff --git a/servo/components/style/gecko_bindings/sugar/ns_css_value.rs b/servo/components/style/gecko_bindings/sugar/ns_css_value.rs
--- a/servo/components/style/gecko_bindings/sugar/ns_css_value.rs
+++ b/servo/components/style/gecko_bindings/sugar/ns_css_value.rs
@@ -76,27 +76,37 @@ impl nsCSSValue {
         debug_assert!(!array.is_null());
         &*array
     }
 
     /// Sets LengthOrPercentage value to this nsCSSValue.
     pub unsafe fn set_lop(&mut self, lop: LengthOrPercentage) {
         match lop {
             LengthOrPercentage::Length(px) => {
-                bindings::Gecko_CSSValue_SetPixelLength(self, px.px())
+                self.set_px(px.px())
             }
             LengthOrPercentage::Percentage(pc) => {
-                bindings::Gecko_CSSValue_SetPercentage(self, pc.0)
+                self.set_percentage(pc.0)
             }
             LengthOrPercentage::Calc(calc) => {
                 bindings::Gecko_CSSValue_SetCalc(self, calc.into())
             }
         }
     }
 
+    /// Sets a px value to this nsCSSValue.
+    pub unsafe fn set_px(&mut self, px: f32) {
+        bindings::Gecko_CSSValue_SetPixelLength(self, px)
+    }
+
+    /// Sets a percentage value to this nsCSSValue.
+    pub unsafe fn set_percentage(&mut self, unit_value: f32) {
+        bindings::Gecko_CSSValue_SetPercentage(self, unit_value)
+    }
+
     /// Returns LengthOrPercentage value.
     pub unsafe fn get_lop(&self) -> LengthOrPercentage {
         use values::computed::Length;
         match self.mUnit {
             nsCSSUnit::eCSSUnit_Pixel => {
                 LengthOrPercentage::Length(Length::new(bindings::Gecko_CSSValue_GetNumber(self)))
             },
             nsCSSUnit::eCSSUnit_Percent => {
diff --git a/servo/components/style/values/specified/gecko.rs b/servo/components/style/values/specified/gecko.rs
--- a/servo/components/style/values/specified/gecko.rs
+++ b/servo/components/style/values/specified/gecko.rs
@@ -1,18 +1,23 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Specified types for legacy Gecko-only properties.
 
-use cssparser::Parser;
+use cssparser::{Parser, Token};
+use gecko_bindings::structs;
+use gecko_bindings::sugar::ns_css_value::ToNsCssValue;
 use parser::{Parse, ParserContext};
-use style_traits::ParseError;
+use style_traits::{ParseError, StyleParseErrorKind};
+use values::CSSFloat;
+use values::computed;
 use values::generics::gecko::ScrollSnapPoint as GenericScrollSnapPoint;
+use values::generics::rect::Rect;
 use values::specified::length::LengthOrPercentage;
 
 /// A specified type for scroll snap points.
 pub type ScrollSnapPoint = GenericScrollSnapPoint<LengthOrPercentage>;
 
 impl Parse for ScrollSnapPoint {
     fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
         if input.try(|i| i.expect_ident_matching("none")).is_ok() {
@@ -20,8 +25,70 @@ impl Parse for ScrollSnapPoint {
         }
         input.expect_function_matching("repeat")?;
         let length = input.parse_nested_block(|i| {
             LengthOrPercentage::parse_non_negative(context, i)
         })?;
         Ok(GenericScrollSnapPoint::Repeat(length))
     }
 }
+
+/// A component of an IntersectionObserverRootMargin.
+#[derive(Clone, Copy, Debug, PartialEq)]
+pub enum PixelOrPercentage {
+    /// An absolute length in pixels (px)
+    Px(CSSFloat),
+    /// A percentage (%)
+    Percentage(computed::Percentage),
+}
+
+impl Parse for PixelOrPercentage {
+    fn parse<'i, 't>(
+        _context: &ParserContext,
+        input: &mut Parser<'i, 't>
+    ) -> Result<Self, ParseError<'i>> {
+        let location = input.current_source_location();
+        let token = input.next()?;
+        let value = match *token {
+            Token::Dimension { value, ref unit, .. } => {
+                match_ignore_ascii_case! { unit,
+                    "px" => Ok(PixelOrPercentage::Px(value)),
+                    _ => Err(()),
+                }
+            }
+            Token::Percentage { unit_value, .. } => {
+                Ok(PixelOrPercentage::Percentage(
+                    computed::Percentage(unit_value)
+                ))
+            }
+            _ => Err(()),
+        };
+        value.map_err(|()| {
+            location.new_custom_error(StyleParseErrorKind::UnspecifiedError)
+        })
+    }
+}
+
+impl ToNsCssValue for PixelOrPercentage {
+    fn convert(self, nscssvalue: &mut structs::nsCSSValue) {
+        match self {
+            PixelOrPercentage::Px(px) => {
+                unsafe { nscssvalue.set_px(px); }
+            }
+            PixelOrPercentage::Percentage(pc) => {
+                unsafe { nscssvalue.set_percentage(pc.0); }
+            }
+        }
+    }
+}
+
+/// The value of an IntersectionObserver's rootMargin property.
+pub struct IntersectionObserverRootMargin(pub Rect<PixelOrPercentage>);
+
+impl Parse for IntersectionObserverRootMargin {
+    fn parse<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<Self, ParseError<'i>> {
+        let rect = Rect::parse_with(context, input, PixelOrPercentage::parse)?;
+        Ok(IntersectionObserverRootMargin(rect))
+    }
+}
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -108,17 +108,17 @@ use style::gecko_bindings::structs::nsSt
 use style::gecko_bindings::structs::nsTArray;
 use style::gecko_bindings::structs::nsresult;
 use style::gecko_bindings::sugar::ownership::{FFIArcHelpers, HasFFI, HasArcFFI};
 use style::gecko_bindings::sugar::ownership::{HasSimpleFFI, Strong};
 use style::gecko_bindings::sugar::refptr::RefPtr;
 use style::gecko_properties::style_structs;
 use style::invalidation::element::restyle_hints;
 use style::media_queries::{Device, MediaList, parse_media_query_list};
-use style::parser::{ParserContext, self};
+use style::parser::{Parse, ParserContext, self};
 use style::properties::{CascadeFlags, ComputedValues, DeclarationSource, Importance};
 use style::properties::{IS_FIELDSET_CONTENT, IS_LINK, IS_VISITED_LINK, LonghandIdSet};
 use style::properties::{LonghandId, PropertyDeclaration, PropertyDeclarationBlock, PropertyId};
 use style::properties::{PropertyDeclarationId, ShorthandId};
 use style::properties::{SKIP_ROOT_AND_ITEM_BASED_DISPLAY_FIXUP, SourcePropertyDeclaration, StyleBuilder};
 use style::properties::PROHIBIT_DISPLAY_CONTENTS;
 use style::properties::animated_properties::AnimationValue;
 use style::properties::animated_properties::compare_property_priority;
@@ -142,16 +142,17 @@ use style::timer::Timer;
 use style::traversal::DomTraversal;
 use style::traversal::resolve_style;
 use style::traversal_flags::{TraversalFlags, self};
 use style::values::{CustomIdent, KeyframesName};
 use style::values::animated::{Animate, Procedure, ToAnimatedZero};
 use style::values::computed::{Context, ToComputedValue};
 use style::values::distance::ComputeSquaredDistance;
 use style::values::specified;
+use style::values::specified::gecko::IntersectionObserverRootMargin;
 use style_traits::{PARSING_MODE_DEFAULT, ToCss};
 use super::error_reporter::ErrorReporter;
 use super::stylesheet_loader::StylesheetLoader;
 
 /*
  * For Gecko->Servo function calls, we need to redeclare the same signature that was declared in
  * the C header in Gecko. In order to catch accidental mismatches, we run rust-bindgen against
  * those signatures as well, giving us a second declaration of all the Servo_* functions in this
@@ -2340,16 +2341,17 @@ pub extern "C" fn Servo_ParseProperty(
 #[no_mangle]
 pub extern "C" fn Servo_ParseEasing(
     easing: *const nsAString,
     data: *mut URLExtraData,
     output: nsTimingFunctionBorrowedMut
 ) -> bool {
     use style::properties::longhands::transition_timing_function;
 
+    // FIXME Dummy URL data would work fine here.
     let url_data = unsafe { RefPtr::from_ptr_ref(&data) };
     let context = ParserContext::new(Origin::Author,
                                      url_data,
                                      Some(CssRuleType::Style),
                                      PARSING_MODE_DEFAULT,
                                      QuirksMode::NoQuirks);
     let easing = unsafe { (*easing).to_string() };
     let mut input = ParserInput::new(&easing);
@@ -4393,8 +4395,44 @@ pub extern "C" fn Servo_ComputeColor(
                     true
                 }
                 None => false,
             }
         }
         Err(_) => false,
     }
 }
+
+#[no_mangle]
+pub extern "C" fn Servo_ParseIntersectionObserverRootMargin(
+    value: *const nsAString,
+    result: *mut structs::nsCSSRect,
+) -> bool {
+    let value = unsafe { value.as_ref().unwrap().to_string() };
+    let result = unsafe { result.as_mut().unwrap() };
+
+    let mut input = ParserInput::new(&value);
+    let mut parser = Parser::new(&mut input);
+
+    let url_data = unsafe { dummy_url_data() };
+    let context = ParserContext::new(
+        Origin::Author,
+        url_data,
+        Some(CssRuleType::Style),
+        PARSING_MODE_DEFAULT,
+        QuirksMode::NoQuirks,
+    );
+
+    let margin = parser.parse_entirely(|p| {
+        IntersectionObserverRootMargin::parse(&context, p)
+    });
+    match margin {
+        Ok(margin) => {
+            let rect = margin.0;
+            result.mTop.set_from(rect.0);
+            result.mRight.set_from(rect.1);
+            result.mBottom.set_from(rect.2);
+            result.mLeft.set_from(rect.3);
+            true
+        }
+        Err(..) => false,
+    }
+}
