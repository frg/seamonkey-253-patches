# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1504570866 25200
# Node ID 8af47160570c05088128428bae1eb69bf48b0dc4
# Parent  c2e780fd85fc108d3b595b0afb84465487d35268
Bug 1369533: Return dead wrappers rather than null for dead CallbackObject values. r=bz

We don't have access to an appropriate context to create the dead wrapper in
when the callback is nuked, so instead, this patch creates a new dead wrapper
in the caller compartment each time the property is accessed. This is the same
behavior we'd get when trying to re-wrap a cross-compartment dead wrapper, so
it's consistent with the way we handle these situations elsewhere.

MozReview-Commit-ID: 3cMeR4z8EOe

diff --git a/dom/base/CustomElementRegistry.cpp b/dom/base/CustomElementRegistry.cpp
--- a/dom/base/CustomElementRegistry.cpp
+++ b/dom/base/CustomElementRegistry.cpp
@@ -590,16 +590,19 @@ CustomElementRegistry::Define(const nsAS
 
   AutoJSAPI jsapi;
   if (NS_WARN_IF(!jsapi.Init(mWindow))) {
     aRv.Throw(NS_ERROR_FAILURE);
     return;
   }
 
   JSContext *cx = jsapi.cx();
+  // Note: No calls that might run JS or trigger CC before this point, or
+  // there's a (vanishingly small) chance of our constructor being nulled
+  // before we access it.
   JS::Rooted<JSObject*> constructor(cx, aFunctionConstructor.CallableOrNull());
 
   /**
    * 1. If IsConstructor(constructor) is false, then throw a TypeError and abort
    *    these steps.
    */
   // For now, all wrappers are constructable if they are callable. So we need to
   // unwrap constructor to check it is really constructable.
@@ -875,17 +878,17 @@ CustomElementRegistry::Get(JSContext* aC
   nsCOMPtr<nsIAtom> nameAtom(NS_Atomize(aName));
   CustomElementDefinition* data = mCustomDefinitions.Get(nameAtom);
 
   if (!data) {
     aRetVal.setUndefined();
     return;
   }
 
-  aRetVal.setObjectOrNull(data->mConstructor->CallableOrNull());
+  aRetVal.setObject(*data->mConstructor->Callback(aCx));
 }
 
 already_AddRefed<Promise>
 CustomElementRegistry::WhenDefined(const nsAString& aName, ErrorResult& aRv)
 {
   nsCOMPtr<nsIGlobalObject> global = do_QueryInterface(mWindow);
   RefPtr<Promise> promise = Promise::Create(global, aRv);
 
diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -6502,16 +6502,19 @@ nsDocument::RegisterElement(JSContext* a
     // Only convert NAME to lowercase in HTML documents.
     nsAutoString lcName;
     IsHTMLDocument() ? nsContentUtils::ASCIIToLower(aOptions.mExtends, lcName)
                      : lcName.Assign(aOptions.mExtends);
 
     options.mExtends.Construct(lcName);
   }
 
+  // Note: No calls that might run JS or trigger CC after this, or there's a
+  // (vanishingly small) risk of our constructor being nulled before Define()
+  // can access it.
   RefPtr<Function> functionConstructor =
     new Function(aCx, wrappedConstructor, sgo);
 
   registry->Define(lcType, *functionConstructor, options, rv);
 
   aRetval.set(wrappedConstructor);
 }
 
diff --git a/dom/bindings/BindingUtils.h b/dom/bindings/BindingUtils.h
--- a/dom/bindings/BindingUtils.h
+++ b/dom/bindings/BindingUtils.h
@@ -1913,47 +1913,47 @@ GetOrCreateDOMReflectorNoWrap(JSContext*
                               JS::MutableHandle<JS::Value> rval)
 {
   return
     GetOrCreateDOMReflectorNoWrapHelper<T>::GetOrCreate(cx, value, rval);
 }
 
 template <class T>
 inline JSObject*
-GetCallbackFromCallbackObject(T* aObj)
+GetCallbackFromCallbackObject(JSContext* aCx, T* aObj)
 {
-  return aObj->CallbackOrNull();
+  return aObj->Callback(aCx);
 }
 
 // Helper for getting the callback JSObject* of a smart ptr around a
 // CallbackObject or a reference to a CallbackObject or something like
 // that.
 template <class T, bool isSmartPtr=IsSmartPtr<T>::value>
 struct GetCallbackFromCallbackObjectHelper
 {
-  static inline JSObject* Get(const T& aObj)
+  static inline JSObject* Get(JSContext* aCx, const T& aObj)
   {
-    return GetCallbackFromCallbackObject(aObj.get());
+    return GetCallbackFromCallbackObject(aCx, aObj.get());
   }
 };
 
 template <class T>
 struct GetCallbackFromCallbackObjectHelper<T, false>
 {
-  static inline JSObject* Get(T& aObj)
+  static inline JSObject* Get(JSContext* aCx, T& aObj)
   {
-    return GetCallbackFromCallbackObject(&aObj);
+    return GetCallbackFromCallbackObject(aCx, &aObj);
   }
 };
 
 template<class T>
 inline JSObject*
-GetCallbackFromCallbackObject(T& aObj)
+GetCallbackFromCallbackObject(JSContext* aCx, T& aObj)
 {
-  return GetCallbackFromCallbackObjectHelper<T>::Get(aObj);
+  return GetCallbackFromCallbackObjectHelper<T>::Get(aCx, aObj);
 }
 
 static inline bool
 AtomizeAndPinJSString(JSContext* cx, jsid& id, const char* chars)
 {
   if (JSString *str = ::JS_AtomizeAndPinString(cx, chars)) {
     id = INTERNED_STRING_TO_JSID(cx, str);
     return true;
diff --git a/dom/bindings/CallbackObject.cpp b/dom/bindings/CallbackObject.cpp
--- a/dom/bindings/CallbackObject.cpp
+++ b/dom/bindings/CallbackObject.cpp
@@ -111,16 +111,28 @@ CallbackObject::FinishSlowJSInitIfMoreTh
       mIncumbentJSGlobal = mIncumbentGlobal->GetGlobalJSObject();
     }
   } else {
     // We can just forget all our stuff.
     ClearJSReferences();
   }
 }
 
+JSObject*
+CallbackObject::Callback(JSContext* aCx)
+{
+  JSObject* callback = CallbackOrNull();
+  if (!callback) {
+    callback = JS_NewDeadWrapper(aCx);
+  }
+
+  MOZ_DIAGNOSTIC_ASSERT(callback);
+  return callback;
+}
+
 CallbackObject::CallSetup::CallSetup(CallbackObject* aCallback,
                                      ErrorResult& aRv,
                                      const char* aExecutionReason,
                                      ExceptionHandling aExceptionHandling,
                                      JSCompartment* aCompartment,
                                      bool aIsJSImplementedWebIDL)
   : mCx(nullptr)
   , mCompartment(aCompartment)
diff --git a/dom/bindings/CallbackObject.h b/dom/bindings/CallbackObject.h
--- a/dom/bindings/CallbackObject.h
+++ b/dom/bindings/CallbackObject.h
@@ -88,16 +88,20 @@ public:
   // argument can safely rely on the callback being non-null so long as it
   // doesn't trigger any scripts before it accesses it.
   JS::Handle<JSObject*> CallbackOrNull() const
   {
     mCallback.exposeToActiveJS();
     return CallbackPreserveColor();
   }
 
+  // Like CallbackOrNull(), but will return a new dead proxy object in the
+  // caller's compartment if the callback is null.
+  JSObject* Callback(JSContext* aCx);
+
   JSObject* GetCreationStack() const
   {
     return mCreationStack;
   }
 
   void MarkForCC()
   {
     mCallback.exposeToActiveJS();
@@ -589,22 +593,27 @@ public:
 
   // But nullptr can't use the above template, because it doesn't know which S
   // to select.  So we need a special overload for nullptr.
   void operator=(decltype(nullptr) arg)
   {
     this->get().operator=(arg);
   }
 
-  // Codegen relies on being able to do CallbackOrNull() on us.
+  // Codegen relies on being able to do CallbackOrNull() and Callback() on us.
   JS::Handle<JSObject*> CallbackOrNull() const
   {
     return this->get()->CallbackOrNull();
   }
 
+  JSObject* Callback(JSContext* aCx) const
+  {
+    return this->get()->Callback(aCx);
+  }
+
   ~RootedCallback()
   {
     // Ensure that our callback starts holding on to its own JS objects as
     // needed.  We really do need to check that things are initialized even when
     // T is OwningNonNull, because we might be running before the OwningNonNull
     // ever got assigned to!
     if (IsInitialized(this->get())) {
       this->get()->FinishSlowJSInitIfMoreThanOneOwner(mCx);
diff --git a/dom/bindings/Codegen.py b/dom/bindings/Codegen.py
--- a/dom/bindings/Codegen.py
+++ b/dom/bindings/Codegen.py
@@ -7004,17 +7004,17 @@ def getWrapTemplateForType(type, descrip
                 CGGeneric(setNull()),
                 CGGeneric(conversion)).define()
         return conversion, False
 
     if type.isCallback() or type.isCallbackInterface():
         # Callbacks can store null if we nuked the compartments their
         # objects lived in.
         wrapCode = setObjectOrNull(
-            "GetCallbackFromCallbackObject(%(result)s)",
+            "GetCallbackFromCallbackObject(cx, %(result)s)",
             wrapAsType=type)
         if type.nullable():
             wrapCode = (
                 "if (%(result)s) {\n" +
                 indent(wrapCode) +
                 "} else {\n" +
                 indent(setNull()) +
                 "}\n")
diff --git a/dom/bindings/ToJSValue.h b/dom/bindings/ToJSValue.h
--- a/dom/bindings/ToJSValue.h
+++ b/dom/bindings/ToJSValue.h
@@ -127,17 +127,17 @@ ToJSValue(JSContext* aCx,
 MOZ_MUST_USE inline bool
 ToJSValue(JSContext* aCx,
           CallbackObject& aArgument,
           JS::MutableHandle<JS::Value> aValue)
 {
   // Make sure we're called in a compartment
   MOZ_ASSERT(JS::CurrentGlobalOrNull(aCx));
 
-  aValue.setObjectOrNull(aArgument.CallbackOrNull());
+  aValue.setObjectOrNull(aArgument.Callback(aCx));
 
   return MaybeWrapValue(aCx, aValue);
 }
 
 // Accept objects that inherit from nsWrapperCache (e.g. most
 // DOM objects).
 template <class T>
 MOZ_MUST_USE
diff --git a/dom/events/DOMEventTargetHelper.cpp b/dom/events/DOMEventTargetHelper.cpp
--- a/dom/events/DOMEventTargetHelper.cpp
+++ b/dom/events/DOMEventTargetHelper.cpp
@@ -319,17 +319,17 @@ DOMEventTargetHelper::SetEventHandler(ns
 
 void
 DOMEventTargetHelper::GetEventHandler(nsIAtom* aType,
                                       JSContext* aCx,
                                       JS::Value* aValue)
 {
   EventHandlerNonNull* handler = GetEventHandler(aType, EmptyString());
   if (handler) {
-    *aValue = JS::ObjectOrNullValue(handler->CallableOrNull());
+    *aValue = JS::ObjectValue(*handler->Callback(aCx));
   } else {
     *aValue = JS::NullValue();
   }
 }
 
 nsresult
 DOMEventTargetHelper::GetEventTargetParent(EventChainPreVisitor& aVisitor)
 {
diff --git a/js/xpconnect/tests/unit/test_nuke_sandbox_event_listeners.js b/js/xpconnect/tests/unit/test_nuke_sandbox_event_listeners.js
--- a/js/xpconnect/tests/unit/test_nuke_sandbox_event_listeners.js
+++ b/js/xpconnect/tests/unit/test_nuke_sandbox_event_listeners.js
@@ -22,27 +22,34 @@ add_task(async function() {
                        .getInterface(Ci.nsIDocShell);
 
   docShell.createAboutBlankContentViewer(principal);
 
   let window = webnav.document.defaultView;
   let sandbox = Cu.Sandbox(window, {sandboxPrototype: window});
 
   function sandboxContent() {
+    window.onload = function SandboxOnLoad() {};
+
     window.addEventListener("FromTest", () => {
       window.dispatchEvent(new CustomEvent("FromSandbox"));
     }, true);
   }
 
   Cu.evalInSandbox(`(${sandboxContent})()`, sandbox);
 
 
   let fromTestPromise = promiseEvent(window, "FromTest");
   let fromSandboxPromise = promiseEvent(window, "FromSandbox");
 
+  equal(typeof window.onload, "function",
+        "window.onload should contain sandbox event listener");
+  equal(window.onload.name, "SandboxOnLoad",
+        "window.onload have the correct function name");
+
   info("Dispatch FromTest event");
   window.dispatchEvent(new window.CustomEvent("FromTest"));
 
   await fromTestPromise;
   info("Got event from test");
 
   await fromSandboxPromise;
   info("Got response from sandbox");
@@ -63,16 +70,18 @@ add_task(async function() {
   info("Got event from test");
 
 
   // Force cycle collection, which should cause our callback reference
   // to be dropped, and dredge up potential issues there.
   Cu.forceGC();
   Cu.forceCC();
 
+  ok(Cu.isDeadWrapper(window.onload),
+     "window.onload should contain a dead wrapper after sandbox is nuked");
 
   info("Dispatch FromTest event");
   fromTestPromise = promiseEvent(window, "FromTest");
   window.dispatchEvent(new window.CustomEvent("FromTest"));
   await fromTestPromise;
   info("Got event from test");
 
   let listeners = Services.els.getListenerInfoFor(window);
