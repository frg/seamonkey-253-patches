# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1509118897 14400
# Node ID d76b2163ba1ae8021d9ac555f670e88acb3a23c9
# Parent  8aeb487d69de5371f37e7167e479bfa2357db230
Bug 1373802 - Handle more clipping cases. r=mstange

We already support cases where we have scrolling clips applied to fixed
items. However if we had to build nested clips inside those items, then
those nested clips would not properly inherit from the scrolling clips.
This patch addresses that case.

MozReview-Commit-ID: CWp1x0EsyaP

diff --git a/gfx/layers/wr/ScrollingLayersHelper.cpp b/gfx/layers/wr/ScrollingLayersHelper.cpp
--- a/gfx/layers/wr/ScrollingLayersHelper.cpp
+++ b/gfx/layers/wr/ScrollingLayersHelper.cpp
@@ -111,16 +111,29 @@ ScrollingLayersHelper::BeginItem(nsDispl
   FrameMetrics::ViewID scrollId = aItem->GetActiveScrolledRoot()
       ? nsLayoutUtils::ViewIDForASR(aItem->GetActiveScrolledRoot())
       : FrameMetrics::NULL_SCROLL_ID;
   // If the leafmost ASR is not the same as the item's ASR then we are dealing
   // with a case where the item's clip chain is scrolled by something other than
   // the item's ASR. So for those cases we need to use the ClipAndScroll API.
   bool needClipAndScroll = (leafmostId != scrollId);
 
+  // The other scenario where we need to push a ClipAndScroll is when we are
+  // in a nested display item where the enclosing item pushed a ClipAndScroll,
+  // and our clip chain extends from that item's clip chain. To check this we
+  // want to make sure that (a) we are InsideClipAndScroll(), and (b) nothing
+  // else was pushed onto mBuilder's stack since that ClipAndScroll.
+  if (!needClipAndScroll &&
+      InsideClipAndScroll() &&
+      mBuilder->TopmostScrollId() == scrollId &&
+      !mBuilder->TopmostIsClip()) {
+    MOZ_ASSERT(mItemClipStack.back().mClipAndScroll->first == scrollId);
+    needClipAndScroll = true;
+  }
+
   // If we don't need a ClipAndScroll, ensure the item's ASR is at the top of
   // the scroll stack
   if (!needClipAndScroll && mBuilder->TopmostScrollId() != scrollId) {
     MOZ_ASSERT(leafmostId == scrollId); // because !needClipAndScroll
     clips.mScrollId = Some(scrollId);
   }
   // And ensure the leafmost clip, if scrolled by that ASR, is at the top of the
   // stack.
@@ -257,33 +270,49 @@ ScrollingLayersHelper::RecurseAndDefineC
     } else {
       // But if the ASRs are different, this is the outermost clip that's
       // still inside aAsr, and we need to make it a child of aAsr rather
       // than aChain->mParent.
       ancestorIds.second = Nothing();
     }
   } else {
     MOZ_ASSERT(!ancestorIds.second);
-    // If aChain->mASR is already the topmost scroll layer on the stack, but
-    // but there was another clip pushed *on top* of that ASR, then that clip
-    // shares the ASR, and we need to make our clip a child of that clip, which
-    // in turn will already be a descendant of the correct ASR.
-    // This covers the cases where e.g. the Gecko display list has nested items,
-    // and the clip chain on the nested item implicitly extends from the clip
-    // chain on the containing wrapper item. In this case the aChain->mParent
-    // pointer will be null for the nested item but the containing wrapper's
-    // clip will be on the stack already and we can pick it up from there.
-    // Another way of thinking about this is that if the clip chain were
-    // "fully completed" then aChain->mParent wouldn't be null but would point
-    // to the clip corresponding to mBuilder->TopmostClipId(), and we would
-    // have gone into the |aChain->mParent->mASR == aAsr| branch above.
     FrameMetrics::ViewID scrollId = aChain->mASR ? nsLayoutUtils::ViewIDForASR(aChain->mASR) : FrameMetrics::NULL_SCROLL_ID;
-    if (mBuilder->TopmostScrollId() == scrollId && mBuilder->TopmostIsClip()) {
-      ancestorIds.first = Nothing();
-      ancestorIds.second = mBuilder->TopmostClipId();
+    if (mBuilder->TopmostScrollId() == scrollId) {
+      if (mBuilder->TopmostIsClip()) {
+        // If aChain->mASR is already the topmost scroll layer on the stack, but
+        // but there was another clip pushed *on top* of that ASR, then that clip
+        // shares the ASR, and we need to make our clip a child of that clip, which
+        // in turn will already be a descendant of the correct ASR.
+        // This covers the cases where e.g. the Gecko display list has nested items,
+        // and the clip chain on the nested item implicitly extends from the clip
+        // chain on the containing wrapper item. In this case the aChain->mParent
+        // pointer will be null for the nested item but the containing wrapper's
+        // clip will be on the stack already and we can pick it up from there.
+        // Another way of thinking about this is that if the clip chain were
+        // "fully completed" then aChain->mParent wouldn't be null but would point
+        // to the clip corresponding to mBuilder->TopmostClipId(), and we would
+        // have gone into the |aChain->mParent->mASR == aAsr| branch above.
+        ancestorIds.first = Nothing();
+        ancestorIds.second = mBuilder->TopmostClipId();
+      } else if (InsideClipAndScroll()) {
+        // If aChain->mASR is already the topmost scroll layer on the stack, but
+        // it was pushed as part of a "clip and scroll" entry (i.e. because an
+        // item had a clip scrolled by a different ASR than the item itself),
+        // then we have need to propagate that behaviour as well. For example if
+        // the enclosing display item pushed a ClipAndScroll with (scrollid=S,
+        // clipid=C), then then clip we're defining here (call it D) needs to be
+        // defined as a child of C, and we'll need to push the ClipAndScroll
+        // (S, D) for this item. This hunk of code ensures that we define D
+        // as a child of C, and when we set the needClipAndScroll flag elsewhere
+        // in this file we make sure to set it for this scenario.
+        MOZ_ASSERT(mItemClipStack.back().mClipAndScroll->first == scrollId);
+        ancestorIds.first = Nothing();
+        ancestorIds.second = mItemClipStack.back().mClipAndScroll->second;
+      }
     }
   }
   // At most one of the ancestor pair should be defined here, and the one that
   // is defined will be the parent clip for the new clip that we're defining.
   MOZ_ASSERT(!(ancestorIds.first && ancestorIds.second));
 
   LayoutDeviceRect clip = LayoutDeviceRect::FromAppUnits(
       aChain->mClip.GetClipRect(), aAppUnitsPerDevPixel);
@@ -412,16 +441,22 @@ ScrollingLayersHelper::RecurseAndDefineA
   mBuilder->DefineScrollLayer(scrollId, ancestorIds.first, ancestorIds.second,
       aSc.ToRelativeLayoutRect(contentRect),
       aSc.ToRelativeLayoutRect(clipBounds));
 
   ids.first = Some(scrollId);
   return ids;
 }
 
+bool
+ScrollingLayersHelper::InsideClipAndScroll() const
+{
+  return !mItemClipStack.empty() && mItemClipStack.back().mClipAndScroll.isSome();
+}
+
 ScrollingLayersHelper::~ScrollingLayersHelper()
 {
   MOZ_ASSERT(!mBuilder);
   MOZ_ASSERT(mCache.empty());
   MOZ_ASSERT(mItemClipStack.empty());
 }
 
 ScrollingLayersHelper::ItemClips::ItemClips(const ActiveScrolledRoot* aAsr,
diff --git a/gfx/layers/wr/ScrollingLayersHelper.h b/gfx/layers/wr/ScrollingLayersHelper.h
--- a/gfx/layers/wr/ScrollingLayersHelper.h
+++ b/gfx/layers/wr/ScrollingLayersHelper.h
@@ -61,16 +61,18 @@ private:
 
   std::pair<Maybe<FrameMetrics::ViewID>, Maybe<wr::WrClipId>>
   RecurseAndDefineAsr(nsDisplayItem* aItem,
                       const ActiveScrolledRoot* aAsr,
                       const DisplayItemClipChain* aChain,
                       int32_t aAppUnitsPerDevPixel,
                       const StackingContextHelper& aSc);
 
+  bool InsideClipAndScroll() const;
+
   // Note: two DisplayItemClipChain* A and B might actually be "equal" (as per
   // DisplayItemClipChain::Equal(A, B)) even though they are not the same pointer
   // (A != B). In this hopefully-rare case, they will get separate entries
   // in this map when in fact we could collapse them. However, to collapse
   // them involves writing a custom hash function for the pointer type such that
   // A and B hash to the same things whenever DisplayItemClipChain::Equal(A, B)
   // is true, and that will incur a performance penalty for all the hashmap
   // operations, so is probably not worth it. With the current code we might
diff --git a/layout/reftests/async-scrolling/reftest.list b/layout/reftests/async-scrolling/reftest.list
--- a/layout/reftests/async-scrolling/reftest.list
+++ b/layout/reftests/async-scrolling/reftest.list
@@ -31,34 +31,34 @@ skip-if(!asyncPan) == sticky-pos-scrolla
 skip-if(!asyncPan) == fixed-pos-scrollable-1.html fixed-pos-scrollable-1-ref.html
 skip-if(!asyncPan) == culling-1.html culling-1-ref.html
 skip-if(!asyncPan) == position-fixed-iframe-1.html position-fixed-iframe-1-ref.html
 skip-if(!asyncPan) == position-fixed-iframe-2.html position-fixed-iframe-2-ref.html
 fuzzy-if(skiaContent,1,11300) skip-if(!asyncPan) == position-fixed-in-scroll-container.html position-fixed-in-scroll-container-ref.html
 skip-if(!asyncPan) == position-fixed-inside-sticky-1.html position-fixed-inside-sticky-1-ref.html
 skip-if(!asyncPan) == position-fixed-inside-sticky-2.html position-fixed-inside-sticky-2-ref.html
 fuzzy(1,60000) skip-if(!asyncPan) == group-opacity-surface-size-1.html group-opacity-surface-size-1-ref.html
-fuzzy-if(Android,1,197) skip-if(!asyncPan) == position-sticky-transformed.html position-sticky-transformed-ref.html # bug 1366295 for webrender
+fuzzy-if(Android,1,197) skip-if(!asyncPan) == position-sticky-transformed.html position-sticky-transformed-ref.html
 skip-if(!asyncPan) == offscreen-prerendered-active-opacity.html offscreen-prerendered-active-opacity-ref.html
 fuzzy-if(Android,6,4) fuzzy-if(skiaContent&&!Android,1,34) skip-if(!asyncPan) == offscreen-clipped-blendmode-1.html offscreen-clipped-blendmode-ref.html
 fuzzy-if(Android,6,4) skip-if(!asyncPan) == offscreen-clipped-blendmode-2.html offscreen-clipped-blendmode-ref.html
 fuzzy-if(Android,6,4) skip == offscreen-clipped-blendmode-3.html offscreen-clipped-blendmode-ref.html # bug 1251588 - wrong AGR on mix-blend-mode item
 fuzzy-if(Android,6,4) skip-if(!asyncPan) == offscreen-clipped-blendmode-4.html offscreen-clipped-blendmode-ref.html
 fuzzy-if(Android,7,4) skip-if(!asyncPan) == perspective-scrolling-1.html perspective-scrolling-1-ref.html
 fuzzy-if(Android,7,4) skip-if(!asyncPan) == perspective-scrolling-2.html perspective-scrolling-2-ref.html
 fuzzy-if(Android,7,4) fails-if(webrender) skip-if(!asyncPan) == perspective-scrolling-3.html perspective-scrolling-3-ref.html # bug 1361720 for webrender
 fuzzy-if(Android,7,4) skip-if(!asyncPan) == perspective-scrolling-4.html perspective-scrolling-4-ref.html
 pref(apz.disable_for_scroll_linked_effects,true) skip-if(!asyncPan) == disable-apz-for-sle-pages.html disable-apz-for-sle-pages-ref.html
 fuzzy-if(browserIsRemote&&d2d,1,19) skip-if(!asyncPan) == background-blend-mode-1.html background-blend-mode-1-ref.html
 skip-if(Android||!asyncPan) != opaque-fractional-displayport-1.html about:blank
 skip-if(Android||!asyncPan) != opaque-fractional-displayport-2.html about:blank
-fuzzy-if(Android,6,4) fails-if(webrender) skip-if(!asyncPan) == fixed-pos-scrolled-clip-1.html fixed-pos-scrolled-clip-1-ref.html   # bug 1373802 for webrender
-fuzzy-if(Android,6,8) fails-if(webrender) skip-if(!asyncPan) == fixed-pos-scrolled-clip-2.html fixed-pos-scrolled-clip-2-ref.html   # bug 1373802 for webrender
-fuzzy-if(Android,6,8) fails-if(webrender) skip-if(!asyncPan) == fixed-pos-scrolled-clip-3.html fixed-pos-scrolled-clip-3-ref.html   # bug 1373802 for webrender
-fuzzy-if(Android,6,8) fails-if(webrender) skip-if(!asyncPan) == fixed-pos-scrolled-clip-4.html fixed-pos-scrolled-clip-4-ref.html   # bug 1373802 for webrender
+fuzzy-if(Android,6,4) skip-if(!asyncPan) == fixed-pos-scrolled-clip-1.html fixed-pos-scrolled-clip-1-ref.html
+fuzzy-if(Android,6,8) fails-if(webrender) skip-if(!asyncPan) == fixed-pos-scrolled-clip-2.html fixed-pos-scrolled-clip-2-ref.html   # bug 1377187 for webrender
+fuzzy-if(Android,6,8) fails-if(webrender) skip-if(!asyncPan) == fixed-pos-scrolled-clip-3.html fixed-pos-scrolled-clip-3-ref.html   # bug 1377187 for webrender
+fuzzy-if(Android,6,8) skip-if(!asyncPan) == fixed-pos-scrolled-clip-4.html fixed-pos-scrolled-clip-4-ref.html
 fuzzy-if(Android,6,4) skip-if(!asyncPan) == position-sticky-scrolled-clip-1.html position-sticky-scrolled-clip-1-ref.html
 fuzzy-if(Android,6,4) skip == position-sticky-scrolled-clip-2.html position-sticky-scrolled-clip-2-ref.html # bug ?????? - incorrectly applying clip to sticky contents
 
 # for the following tests, we want to disable the low-precision buffer
 # as it will expand the displayport beyond what the test specifies in
 # its reftest-displayport attributes, and interfere with where we expect
 # checkerboarding to occur
 default-preferences pref(layers.low-precision-buffer,false)
