# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1513276031 18000
# Node ID 9e183521e2dec6ab4ece8a8a7cdbfa476c700feb
# Parent  a2eac135896cb2ea5272dd00f07ca4e1006599a2
Bug 1424300 P3 Add a mochitest verifying we can recover if the script cache is deleted. r=edenchuang

diff --git a/dom/workers/test/serviceworkers/mochitest.ini b/dom/workers/test/serviceworkers/mochitest.ini
--- a/dom/workers/test/serviceworkers/mochitest.ini
+++ b/dom/workers/test/serviceworkers/mochitest.ini
@@ -226,16 +226,17 @@ support-files =
   lazy_worker.js
   nofetch_handler_worker.js
   service_worker.js
   service_worker_client.html
   utils.js
   bug1290951_worker_main.sjs
   bug1290951_worker_imported.sjs
   sw_storage_not_allow.js
+  update_worker.sjs
 
 [test_bug1151916.html]
 [test_bug1240436.html]
 [test_bug1408734.html]
 [test_claim.html]
 [test_claim_oninstall.html]
 [test_controller.html]
 [test_cookie_fetch.html]
@@ -339,8 +340,9 @@ tags = openwindow
 [test_update_missing_imported_script.html]
 [test_workerUnregister.html]
 [test_workerUpdate.html]
 [test_workerupdatefoundevent.html]
 [test_xslt.html]
 [test_async_waituntil.html]
 [test_worker_reference_gc_timeout.html]
 [test_nofetch_handler.html]
+[test_bad_script_cache.html]
diff --git a/dom/workers/test/serviceworkers/test_bad_script_cache.html b/dom/workers/test/serviceworkers/test_bad_script_cache.html
new file mode 100644
--- /dev/null
+++ b/dom/workers/test/serviceworkers/test_bad_script_cache.html
@@ -0,0 +1,96 @@
+<!--
+  Any copyright is dedicated to the Public Domain.
+  http://creativecommons.org/publicdomain/zero/1.0/
+-->
+<!DOCTYPE HTML>
+<html>
+<head>
+  <title>Test updating a service worker with a bad script cache.</title>
+  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
+</head>
+<body>
+<script src='utils.js'></script>
+<script class="testbody" type="text/javascript">
+
+async function deleteCaches(cacheStorage) {
+  let keyList = await cacheStorage.keys();
+  let promiseList = [];
+  keyList.forEach(key => {
+    promiseList.push(cacheStorage.delete(key));
+  });
+  return await Promise.all(keyList);
+}
+
+function waitForUpdate(reg) {
+  return new Promise(resolve => {
+    reg.addEventListener('updatefound', resolve, { once: true });
+  });
+}
+
+async function runTest() {
+  let reg;
+  try {
+    const script = 'update_worker.sjs';
+    const scope = 'bad-script-cache';
+
+    reg = await navigator.serviceWorker.register(script, { scope: scope });
+    await waitForState(reg.installing, 'activated');
+
+    // Verify the service worker script cache has the worker script stored.
+    let chromeCaches = SpecialPowers.createChromeCache('chrome', window.origin);
+    let scriptURL = new URL(script, window.location.href);
+    let response = await chromeCaches.match(scriptURL.href);
+    is(response.url, scriptURL.href, 'worker script should be stored');
+
+    // Force delete the service worker script out from under the service worker.
+    // Note: Prefs are set to kill the SW thread immediately on idle.
+    await deleteCaches(chromeCaches);
+
+    // Verify the service script cache no longer knows about the worker script.
+    response = await chromeCaches.match(scriptURL.href);
+    is(response, undefined, 'worker script should not be stored');
+
+    // Force an update and wait for it to fire an update event.
+    reg.update();
+    await waitForUpdate(reg);
+    await waitForState(reg.installing, 'activated');
+
+    // Verify that the script cache knows about the worker script again.
+    response = await chromeCaches.match(scriptURL.href);
+    is(response.url, scriptURL.href, 'worker script should be stored');
+  } catch (e) {
+    ok(false, e);
+  }
+  if (reg) {
+    await reg.unregister();
+  }
+
+  // If this test is run on windows and the process shuts down immediately after, then
+  // we may fail to remove some of the Cache API body files.  This is because the GC
+  // runs late causing Cache API to cleanup after shutdown begins.  It seems something
+  // during shutdown scans these files and conflicts with removing the file on windows.
+  //
+  // To avoid this we perform an explict GC here to ensure that Cache API can cleanup
+  // earlier.
+  await new Promise(resolve => SpecialPowers.exactGC(resolve));
+
+  SimpleTest.finish();
+}
+
+SimpleTest.waitForExplicitFinish();
+SpecialPowers.pushPrefEnv({"set": [
+  // standard prefs
+  ["dom.serviceWorkers.exemptFromPerDomainMax", true],
+  ["dom.serviceWorkers.enabled", true],
+  ["dom.serviceWorkers.testing.enabled", true],
+  ["dom.caches.enabled", true],
+
+  // immediately kill the service worker thread when idle
+  ["dom.serviceWorkers.idle_timeout", 0],
+
+]}, runTest);
+</script>
+</pre>
+</body>
+</html>
diff --git a/dom/workers/test/serviceworkers/update_worker.sjs b/dom/workers/test/serviceworkers/update_worker.sjs
new file mode 100644
--- /dev/null
+++ b/dom/workers/test/serviceworkers/update_worker.sjs
@@ -0,0 +1,13 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+"use strict";
+
+function handleRequest(request, response) {
+  // This header is necessary for making this script able to be loaded.
+  response.setHeader("Content-Type", "application/javascript");
+
+  var body = '/* ' + Date.now() + ' */';
+  response.write(body);
+}
+
