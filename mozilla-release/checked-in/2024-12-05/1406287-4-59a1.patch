# HG changeset patch
# User Daisuke Akatsuka <dakatsuka@mozilla.com>
# Date 1510562650 -32400
# Node ID def7e03f6262bc5111112bd8b012a004f4e07195
# Parent  f164c477b464dbf20cc8cd1e22e01bf81606c699
Bug 1406287 - Part 4: Add test for time ticks. r=gl

MozReview-Commit-ID: EPpCACBfJOP

diff --git a/devtools/client/inspector/animation/test/browser.ini b/devtools/client/inspector/animation/test/browser.ini
--- a/devtools/client/inspector/animation/test/browser.ini
+++ b/devtools/client/inspector/animation/test/browser.ini
@@ -6,10 +6,11 @@ support-files =
   head.js
   !/devtools/client/framework/test/shared-head.js
   !/devtools/client/inspector/test/head.js
   !/devtools/client/inspector/test/shared-head.js
   !/devtools/client/shared/test/test-actor-registry.js
   !/devtools/client/shared/test/test-actor.js
 
 [browser_animation_animation_list_exists.js]
+[browser_animation_animation_list_time_tick.js]
 [browser_animation_empty_on_invalid_nodes.js]
 [browser_animation_inspector_exists.js]
diff --git a/devtools/client/inspector/animation/test/browser_animation_animation_list_exists.js b/devtools/client/inspector/animation/test/browser_animation_animation_list_exists.js
--- a/devtools/client/inspector/animation/test/browser_animation_animation_list_exists.js
+++ b/devtools/client/inspector/animation/test/browser_animation_animation_list_exists.js
@@ -1,15 +1,13 @@
 /* Any copyright is dedicated to the Public Domain.
    http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
-requestLongerTimeout(2);
-
 // Test that whether animations ui could be displayed
 
 add_task(async function () {
   await addTab(URL_ROOT + "doc_simple_animation.html");
 
   const { animationInspector, inspector, panel } = await openAnimationInspector();
 
   info("Checking animation list and items existence");
@@ -31,13 +29,12 @@ add_task(async function () {
   info("Checking list and items existence after select a element which has an animation");
   const animatedNode = await getNodeFront(".animated", inspector);
   await selectNodeAndWaitForAnimations(animatedNode, inspector);
   is(panel.querySelectorAll(".animation-list .animation-item").length, 1,
      "The number of animations displayed should be 1 for .animated element");
 
   // TODO: We need to add following tests after implement since this test has same role
   // of animationinspector/test/browser_animation_timeline_ui.js
-  // * header existance.
   // * name label in animation element existance.
   // * target node in animation element existance.
   // * summary graph in animation element existance.
 });
diff --git a/devtools/client/inspector/animation/test/browser_animation_animation_list_time_tick.js b/devtools/client/inspector/animation/test/browser_animation_animation_list_time_tick.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/animation/test/browser_animation_animation_list_time_tick.js
@@ -0,0 +1,80 @@
+/* Any copyright is dedicated to the Public Domain.
+   http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test for following time tick items.
+// * animation list header elements existence
+// * time tick item elements existence
+// * count and label of time tick elements changing by the sidebar width
+
+const TimeScale = require("devtools/client/inspector/animation/utils/timescale");
+const { findOptimalTimeInterval } =
+  require("devtools/client/inspector/animation/utils/utils");
+
+// Should be kept in sync with TIME_GRADUATION_MIN_SPACING in
+// AnimationTimeTickList component.
+const TIME_GRADUATION_MIN_SPACING = 40;
+
+add_task(async function () {
+  await addTab(URL_ROOT + "doc_simple_animation.html");
+  const { animationInspector, inspector, panel } = await openAnimationInspector();
+  const timeScale = new TimeScale(animationInspector.animations);
+
+  info("Checking animation list header element existence");
+  const listContainerEl = panel.querySelector(".animation-list-container");
+  const listHeaderEl = listContainerEl.querySelector(".devtools-toolbar");
+  ok(listHeaderEl, "The header element should be in animation list container element");
+
+  info("Checking time tick item elements existence");
+  assertTimelineTickItems(timeScale, listHeaderEl);
+  const timelineTickItemLength =
+    listHeaderEl.querySelectorAll(".animation-timeline-tick-item").length;
+
+  info("Checking timeline tick item elements after enlarge sidebar width");
+  await setSidebarWidth("100%", inspector);
+  assertTimelineTickItems(timeScale, listHeaderEl);
+  ok(timelineTickItemLength <
+    listHeaderEl.querySelectorAll(".animation-timeline-tick-item").length,
+     "The timeline tick item elements should increase");
+});
+
+/**
+ * Assert timeline tick item's position and label.
+ *
+ * @param {TimeScale} - timeScale
+ * @param {Element} - listHeaderEl which is header element
+ */
+function assertTimelineTickItems(timeScale, listHeaderEl) {
+  const animationTimelineTickListEl =
+    listHeaderEl.querySelector(".animation-timeline-tick-list");
+  ok(animationTimelineTickListEl,
+     "The animation timeline tick list element should be in header");
+
+  const width = animationTimelineTickListEl.offsetWidth;
+  const animationDuration = timeScale.getDuration();
+  const minTimeInterval = TIME_GRADUATION_MIN_SPACING * animationDuration / width;
+  const interval = findOptimalTimeInterval(minTimeInterval);
+  const expectedTickItem = Math.ceil(animationDuration / interval);
+
+  const timelineTickItemEls =
+    listHeaderEl.querySelectorAll(".animation-timeline-tick-item");
+  is(timelineTickItemEls.length, expectedTickItem,
+     "The expected number of timeline ticks were found");
+
+  info("Make sure graduations are evenly distributed and show the right times");
+  for (const [index, tickEl] of timelineTickItemEls.entries()) {
+    const left = parseFloat(tickEl.style.left);
+    const expectedPos = index * interval * 100 / animationDuration;
+    is(Math.round(left), Math.round(expectedPos),
+       `Graduation ${ index } is positioned correctly`);
+
+    // Note that the distancetoRelativeTime and formatTime functions are tested
+    // separately in xpcshell test test_timeScale.js, so we assume that they
+    // work here.
+    const formattedTime =
+      timeScale.formatTime(timeScale.distanceToRelativeTime(expectedPos, width));
+    is(tickEl.textContent, formattedTime,
+       `Graduation ${ index } has the right text content`);
+  }
+}
diff --git a/devtools/client/inspector/animation/test/head.js b/devtools/client/inspector/animation/test/head.js
--- a/devtools/client/inspector/animation/test/head.js
+++ b/devtools/client/inspector/animation/test/head.js
@@ -19,16 +19,17 @@ const ANIMATION_L10N =
 // Enable new animation inspector.
 Services.prefs.setBoolPref("devtools.new-animationinspector.enabled", true);
 
 // Auto clean-up when a test ends.
 // Clean-up all prefs that might have been changed during a test run
 // (safer here because if the test fails, then the pref is never reverted)
 registerCleanupFunction(() => {
   Services.prefs.clearUserPref("devtools.new-animationinspector.enabled");
+  Services.prefs.clearUserPref("devtools.toolsidebar-width.inspector");
 });
 
 /**
  * Open the toolbox, with the inspector tool visible and the animationinspector
  * sidebar selected.
  *
  * @return {Promise} that resolves when the inspector is ready.
  */
@@ -99,8 +100,23 @@ addTab = async function (url) {
  */
 const selectNodeAndWaitForAnimations = async function (data, inspector, reason = "test") {
   // We want to make sure the rest of the test waits for the animations to
   // be properly displayed (wait for all target DOM nodes to be previewed).
   const onUpdated = inspector.once("inspector-updated");
   await selectNode(data, inspector, reason);
   await onUpdated;
 };
+
+/**
+ * Set the sidebar width by given parameter.
+ *
+ * @param {String} width
+ *        Change sidebar width by given parameter.
+ * @param {InspectorPanel} inspector
+ *        The instance of InspectorPanel currently loaded in the toolbox
+ * @return {Promise} Resolves when the sidebar size changed.
+ */
+const setSidebarWidth = async function (width, inspector) {
+  const onUpdated = inspector.toolbox.once("inspector-sidebar-resized");
+  inspector._splitter.setState({ width });
+  await onUpdated;
+};
