# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1512503650 18000
# Node ID c0c7f12df75a4cd3c15f904606a017af8bcb9ba7
# Parent  70e780d3f9bd1791f08002d891c143fde7cc9740
Bug 1423328 Allow a caller to set a ClientSource controlled by a service worker using a ClientHandle. r=baku

diff --git a/dom/clients/manager/ClientHandle.cpp b/dom/clients/manager/ClientHandle.cpp
--- a/dom/clients/manager/ClientHandle.cpp
+++ b/dom/clients/manager/ClientHandle.cpp
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ClientHandle.h"
 
 #include "ClientHandleChild.h"
 #include "ClientHandleOpChild.h"
 #include "ClientManager.h"
 #include "mozilla/dom/PClientManagerChild.h"
+#include "mozilla/dom/ServiceWorkerDescriptor.h"
 
 namespace mozilla {
 namespace dom {
 
 ClientHandle::~ClientHandle()
 {
   Shutdown();
 }
@@ -96,10 +97,30 @@ ClientHandle::ExecutionReady(const Clien
 }
 
 const ClientInfo&
 ClientHandle::Info() const
 {
   return mClientInfo;
 }
 
+RefPtr<GenericPromise>
+ClientHandle::Control(const ServiceWorkerDescriptor& aServiceWorker)
+{
+  RefPtr<GenericPromise::Private> outerPromise =
+    new GenericPromise::Private(__func__);
+
+  RefPtr<ClientOpPromise> innerPromise =
+    StartOp(ClientControlledArgs(aServiceWorker.ToIPC()));
+
+  innerPromise->Then(mSerialEventTarget, __func__,
+    [outerPromise](const ClientOpResult& aResult) {
+      outerPromise->Resolve(true, __func__);
+    },
+    [outerPromise](const ClientOpResult& aResult) {
+      outerPromise->Reject(aResult.get_nsresult(), __func__);
+    });
+
+  return outerPromise.forget();
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/clients/manager/ClientHandle.h b/dom/clients/manager/ClientHandle.h
--- a/dom/clients/manager/ClientHandle.h
+++ b/dom/clients/manager/ClientHandle.h
@@ -4,29 +4,31 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #ifndef _mozilla_dom_ClientHandle_h
 #define _mozilla_dom_ClientHandle_h
 
 #include "mozilla/dom/ClientInfo.h"
 #include "mozilla/dom/ClientOpPromise.h"
 #include "mozilla/dom/ClientThing.h"
+#include "mozilla/MozPromise.h"
 
 #ifdef XP_WIN
 #undef PostMessage
 #endif
 
 namespace mozilla {
 
 namespace dom {
 
 class ClientManager;
 class ClientHandleChild;
 class ClientOpConstructorArgs;
 class PClientManagerChild;
+class ServiceWorkerDescriptor;
 
 // The ClientHandle allows code to take a simple ClientInfo struct and
 // convert it into a live actor-backed object attached to a particular
 // ClientSource somewhere in the browser.  If the ClientSource is
 // destroyed then the ClientHandle will simply begin to reject operations.
 // We do not currently provide a way to be notified when the ClientSource
 // is destroyed, but this could be added in the future.
 class ClientHandle final : public ClientThing<ClientHandleChild>
@@ -57,15 +59,21 @@ class ClientHandle final : public Client
 
   void
   Activate(PClientManagerChild* aActor);
 
 public:
   const ClientInfo&
   Info() const;
 
+  // Mark the ClientSource attached to this handle as controlled by the
+  // given service worker.  The promise will resolve true if the ClientSource
+  // is successfully marked or reject if the operation could not be completed.
+  RefPtr<GenericPromise>
+  Control(const ServiceWorkerDescriptor& aServiceWorker);
+
   NS_INLINE_DECL_REFCOUNTING(ClientHandle);
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // _mozilla_dom_ClientHandle_h
diff --git a/dom/clients/manager/ClientHandleOpParent.cpp b/dom/clients/manager/ClientHandleOpParent.cpp
--- a/dom/clients/manager/ClientHandleOpParent.cpp
+++ b/dom/clients/manager/ClientHandleOpParent.cpp
@@ -1,23 +1,54 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ClientHandleOpParent.h"
 
+#include "ClientHandleParent.h"
+#include "ClientSourceParent.h"
+
 namespace mozilla {
 namespace dom {
 
+ClientSourceParent*
+ClientHandleOpParent::GetSource() const
+{
+  auto handle = static_cast<ClientHandleParent*>(Manager());
+  return handle->GetSource();
+}
+
 void
 ClientHandleOpParent::ActorDestroy(ActorDestroyReason aReason)
 {
+  mPromiseRequestHolder.DisconnectIfExists();
 }
 
 void
 ClientHandleOpParent::Init(const ClientOpConstructorArgs& aArgs)
 {
+  ClientSourceParent* source = GetSource();
+  if (!source) {
+    Unused << PClientHandleOpParent::Send__delete__(this, NS_ERROR_DOM_ABORT_ERR);
+    return;
+  }
+
+  RefPtr<ClientOpPromise> p = source->StartOp(aArgs);
+
+  // Capturing 'this' is safe here because we disconnect the promise in
+  // ActorDestroy() which ensures neither lambda is called if the actor
+  // is destroyed before the source operation completes.
+  p->Then(GetCurrentThreadSerialEventTarget(), __func__,
+    [this] (const ClientOpResult& aResult) {
+      mPromiseRequestHolder.Complete();
+      Unused << PClientHandleOpParent::Send__delete__(this, aResult);
+    },
+    [this] (nsresult aRv) {
+      mPromiseRequestHolder.Complete();
+      Unused << PClientHandleOpParent::Send__delete__(this, aRv);
+    })->Track(mPromiseRequestHolder);
 }
 
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/clients/manager/ClientHandleOpParent.h b/dom/clients/manager/ClientHandleOpParent.h
--- a/dom/clients/manager/ClientHandleOpParent.h
+++ b/dom/clients/manager/ClientHandleOpParent.h
@@ -1,23 +1,31 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #ifndef _mozilla_dom_ClientHandleOpParent_h
 #define _mozilla_dom_ClientHandleOpParent_h
 
+#include "ClientOpPromise.h"
 #include "mozilla/dom/PClientHandleOpParent.h"
 
 namespace mozilla {
 namespace dom {
 
+class ClientSourceParent;
+
 class ClientHandleOpParent final : public PClientHandleOpParent
 {
+  MozPromiseRequestHolder<ClientOpPromise> mPromiseRequestHolder;
+
+  ClientSourceParent*
+  GetSource() const;
+
   // PClientHandleOpParent interface
   void
   ActorDestroy(ActorDestroyReason aReason) override;
 
 public:
   ClientHandleOpParent() = default;
   ~ClientHandleOpParent() = default;
 
diff --git a/dom/clients/manager/ClientIPCTypes.ipdlh b/dom/clients/manager/ClientIPCTypes.ipdlh
--- a/dom/clients/manager/ClientIPCTypes.ipdlh
+++ b/dom/clients/manager/ClientIPCTypes.ipdlh
@@ -51,22 +51,28 @@ union IPCClientState
 };
 
 struct ClientSourceExecutionReadyArgs
 {
   nsCString url;
   FrameType frameType;
 };
 
+struct ClientControlledArgs
+{
+  IPCServiceWorkerDescriptor serviceWorker;
+};
+
 struct ClientOpenWindowArgs
 {
 };
 
-struct ClientOpConstructorArgs
+union ClientOpConstructorArgs
 {
+  ClientControlledArgs;
 };
 
 struct ClientNavigateOpConstructorArgs
 {
 };
 
 union ClientOpResult
 {
diff --git a/dom/clients/manager/ClientSource.cpp b/dom/clients/manager/ClientSource.cpp
--- a/dom/clients/manager/ClientSource.cpp
+++ b/dom/clients/manager/ClientSource.cpp
@@ -252,16 +252,47 @@ ClientSource::Thaw()
 }
 
 const ClientInfo&
 ClientSource::Info() const
 {
   return mClientInfo;
 }
 
+void
+ClientSource::SetController(const ServiceWorkerDescriptor& aServiceWorker)
+{
+  NS_ASSERT_OWNINGTHREAD(ClientSource);
+
+  if (mController.isSome() && mController.ref() == aServiceWorker) {
+    return;
+  }
+
+  mController.reset();
+  mController.emplace(aServiceWorker);
+}
+
+RefPtr<ClientOpPromise>
+ClientSource::Control(const ClientControlledArgs& aArgs)
+{
+  NS_ASSERT_OWNINGTHREAD(ClientSource);
+
+  SetController(ServiceWorkerDescriptor(aArgs.serviceWorker()));
+
+  RefPtr<ClientOpPromise> ref =
+    ClientOpPromise::CreateAndResolve(NS_OK, __func__);
+  return ref.forget();
+}
+
+const Maybe<ServiceWorkerDescriptor>&
+ClientSource::GetController() const
+{
+  return mController;
+}
+
 nsISerialEventTarget*
 ClientSource::EventTarget() const
 {
   return mEventTarget;
 }
 
 void
 ClientSource::Traverse(nsCycleCollectionTraversalCallback& aCallback,
diff --git a/dom/clients/manager/ClientSource.h b/dom/clients/manager/ClientSource.h
--- a/dom/clients/manager/ClientSource.h
+++ b/dom/clients/manager/ClientSource.h
@@ -2,24 +2,29 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #ifndef _mozilla_dom_ClientSource_h
 #define _mozilla_dom_ClientSource_h
 
 #include "mozilla/dom/ClientInfo.h"
+#include "mozilla/dom/ClientOpPromise.h"
 #include "mozilla/dom/ClientThing.h"
+#include "mozilla/dom/ServiceWorkerDescriptor.h"
+#include "mozilla/Variant.h"
 
 class nsIDocShell;
+class nsISerialEventTarget;
 class nsPIDOMWindowInner;
 
 namespace mozilla {
 namespace dom {
 
+class ClientControlledArgs;
 class ClientManager;
 class ClientSourceChild;
 class ClientSourceConstructorArgs;
 class ClientSourceExecutionReadyArgs;
 class PClientManagerChild;
 
 namespace workers {
 class WorkerPrivate;
@@ -41,16 +46,17 @@ class ClientSource final : public Client
   nsCOMPtr<nsISerialEventTarget> mEventTarget;
 
   Variant<Nothing,
           RefPtr<nsPIDOMWindowInner>,
           nsCOMPtr<nsIDocShell>,
           mozilla::dom::workers::WorkerPrivate*> mOwner;
 
   ClientInfo mClientInfo;
+  Maybe<ServiceWorkerDescriptor> mController;
 
   void
   Shutdown();
 
   void
   ExecutionReady(const ClientSourceExecutionReadyArgs& aArgs);
 
   mozilla::dom::workers::WorkerPrivate*
@@ -86,16 +92,37 @@ public:
   Freeze();
 
   void
   Thaw();
 
   const ClientInfo&
   Info() const;
 
+  // Synchronously mark the ClientSource as controlled by the given service
+  // worker.  This can happen as a result of a remote operation or directly
+  // by local code.  For example, if a client's initial network load is
+  // intercepted by a controlling service worker then this should be called
+  // immediately.
+  //
+  // Note, there is no way to clear the controlling service worker because
+  // the specification does not allow that operation.
+  void
+  SetController(const ServiceWorkerDescriptor& aServiceWorker);
+
+  // Mark the ClientSource as controlled using the remote operation arguments.
+  // This will in turn call SetController().
+  RefPtr<ClientOpPromise>
+  Control(const ClientControlledArgs& aArgs);
+
+  // Get the ClientSource's current controlling service worker, if one has
+  // been set.
+  const Maybe<ServiceWorkerDescriptor>&
+  GetController() const;
+
   nsISerialEventTarget*
   EventTarget() const;
 
   void
   Traverse(nsCycleCollectionTraversalCallback& aCallback,
            const char* aName,
            uint32_t aFlags);
 };
diff --git a/dom/clients/manager/ClientSourceChild.cpp b/dom/clients/manager/ClientSourceChild.cpp
--- a/dom/clients/manager/ClientSourceChild.cpp
+++ b/dom/clients/manager/ClientSourceChild.cpp
@@ -1,18 +1,18 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ClientSourceChild.h"
 
+#include "ClientSource.h"
 #include "ClientSourceOpChild.h"
-#include "ClientThing.h"
 #include "mozilla/dom/ClientIPCTypes.h"
 #include "mozilla/Unused.h"
 
 namespace mozilla {
 namespace dom {
 
 using mozilla::ipc::IPCResult;
 
@@ -56,27 +56,33 @@ ClientSourceChild::ClientSourceChild(con
 {
 }
 
 void
 ClientSourceChild::SetOwner(ClientThing<ClientSourceChild>* aThing)
 {
   MOZ_DIAGNOSTIC_ASSERT(aThing);
   MOZ_DIAGNOSTIC_ASSERT(!mSource);
-  mSource = aThing;
+  mSource = static_cast<ClientSource*>(aThing);
 }
 
 void
 ClientSourceChild::RevokeOwner(ClientThing<ClientSourceChild>* aThing)
 {
   MOZ_DIAGNOSTIC_ASSERT(mSource);
-  MOZ_DIAGNOSTIC_ASSERT(mSource == aThing);
+  MOZ_DIAGNOSTIC_ASSERT(mSource == static_cast<ClientSource*>(aThing));
   mSource = nullptr;
 }
 
+ClientSource*
+ClientSourceChild::GetSource() const
+{
+  return mSource;
+}
+
 void
 ClientSourceChild::MaybeStartTeardown()
 {
   if (mTeardownStarted) {
     return;
   }
   mTeardownStarted = true;
   Unused << SendTeardown();
diff --git a/dom/clients/manager/ClientSourceChild.h b/dom/clients/manager/ClientSourceChild.h
--- a/dom/clients/manager/ClientSourceChild.h
+++ b/dom/clients/manager/ClientSourceChild.h
@@ -6,22 +6,23 @@
 #ifndef _mozilla_dom_ClientSourceChild_h
 #define _mozilla_dom_ClientSourceChild_h
 
 #include "mozilla/dom/PClientSourceChild.h"
 
 namespace mozilla {
 namespace dom {
 
+class ClientSource;
 class ClientSourceConstructorArgs;
 template <typename ActorType> class ClientThing;
 
 class ClientSourceChild final : public PClientSourceChild
 {
-  ClientThing<ClientSourceChild>* mSource;
+  ClientSource* mSource;
   bool mTeardownStarted;
 
   // PClientSourceChild interface
   void
   ActorDestroy(ActorDestroyReason aReason) override;
 
   PClientSourceOpChild*
   AllocPClientSourceOpChild(const ClientOpConstructorArgs& aArgs) override;
@@ -37,16 +38,19 @@ public:
   explicit ClientSourceChild(const ClientSourceConstructorArgs& aArgs);
 
   void
   SetOwner(ClientThing<ClientSourceChild>* aThing);
 
   void
   RevokeOwner(ClientThing<ClientSourceChild>* aThing);
 
+  ClientSource*
+  GetSource() const;
+
   void
   MaybeStartTeardown();
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // _mozilla_dom_ClientSourceChild_h
diff --git a/dom/clients/manager/ClientSourceOpChild.cpp b/dom/clients/manager/ClientSourceOpChild.cpp
--- a/dom/clients/manager/ClientSourceOpChild.cpp
+++ b/dom/clients/manager/ClientSourceOpChild.cpp
@@ -1,23 +1,91 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ClientSourceOpChild.h"
 
+#include "ClientSource.h"
+#include "ClientSourceChild.h"
+#include "mozilla/Unused.h"
+
 namespace mozilla {
 namespace dom {
 
+ClientSource*
+ClientSourceOpChild::GetSource() const
+{
+  auto actor = static_cast<ClientSourceChild*>(Manager());
+  return actor->GetSource();
+}
+
+template<typename Method, typename Args>
+void
+ClientSourceOpChild::DoSourceOp(Method aMethod, const Args& aArgs)
+{
+  RefPtr<ClientOpPromise> promise;
+  nsCOMPtr<nsISerialEventTarget> target;
+
+  // Some ClientSource operations can cause the ClientSource to be destroyed.
+  // This means we should reference the ClientSource pointer for the minimum
+  // possible to start the operation.  Use an extra block scope here to help
+  // enforce this and prevent accidental usage later in the method.
+  {
+    ClientSource* source = GetSource();
+    if (!source) {
+      Unused << PClientSourceOpChild::Send__delete__(this, NS_ERROR_DOM_ABORT_ERR);
+      return;
+    }
+
+    target = source->EventTarget();
+
+    // This may cause the ClientSource object to be destroyed.  Do not
+    // use the source variable after this call.
+    promise = (source->*aMethod)(aArgs);
+  }
+
+  // The ClientSource methods are required to always return a promise.  If
+  // they encounter an error they should just immediately resolve or reject
+  // the promise as appropriate.
+  MOZ_DIAGNOSTIC_ASSERT(promise);
+
+  // Capture 'this' is safe here because we disconnect the promise
+  // ActorDestroy() which ensures nethier lambda is called if the
+  // actor is destroyed before the source operation completes.
+  promise->Then(target, __func__,
+    [this, aArgs] (const mozilla::dom::ClientOpResult& aResult) {
+      mPromiseRequestHolder.Complete();
+      Unused << PClientSourceOpChild::Send__delete__(this, aResult);
+    },
+    [this] (nsresult aRv) {
+      mPromiseRequestHolder.Complete();
+      Unused << PClientSourceOpChild::Send__delete__(this, aRv);
+    })->Track(mPromiseRequestHolder);
+}
+
 void
 ClientSourceOpChild::ActorDestroy(ActorDestroyReason aReason)
 {
+  mPromiseRequestHolder.DisconnectIfExists();
 }
 
 void
 ClientSourceOpChild::Init(const ClientOpConstructorArgs& aArgs)
 {
+  switch (aArgs.type()) {
+    case ClientOpConstructorArgs::TClientControlledArgs:
+    {
+      DoSourceOp(&ClientSource::Control, aArgs.get_ClientControlledArgs());
+      break;
+    }
+    default:
+    {
+      MOZ_ASSERT_UNREACHABLE("unknown client operation!");
+      break;
+    }
+  }
 }
 
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/clients/manager/ClientSourceOpChild.h b/dom/clients/manager/ClientSourceOpChild.h
--- a/dom/clients/manager/ClientSourceOpChild.h
+++ b/dom/clients/manager/ClientSourceOpChild.h
@@ -7,18 +7,29 @@
 #define _mozilla_dom_ClientSourceOpChild_h
 
 #include "mozilla/dom/PClientSourceOpChild.h"
 #include "ClientOpPromise.h"
 
 namespace mozilla {
 namespace dom {
 
+class ClientSource;
+
 class ClientSourceOpChild final : public PClientSourceOpChild
 {
+  MozPromiseRequestHolder<ClientOpPromise> mPromiseRequestHolder;
+
+  ClientSource*
+  GetSource() const;
+
+  template <typename Method, typename Args>
+  void
+  DoSourceOp(Method aMethod, const Args& aArgs);
+
   // PClientSourceOpChild interface
   void
   ActorDestroy(ActorDestroyReason aReason) override;
 
 public:
   ClientSourceOpChild() = default;
   ~ClientSourceOpChild() = default;
 
diff --git a/dom/clients/manager/ClientSourceParent.cpp b/dom/clients/manager/ClientSourceParent.cpp
--- a/dom/clients/manager/ClientSourceParent.cpp
+++ b/dom/clients/manager/ClientSourceParent.cpp
@@ -221,10 +221,30 @@ ClientSourceParent::AttachHandle(ClientH
 void
 ClientSourceParent::DetachHandle(ClientHandleParent* aClientHandle)
 {
   MOZ_DIAGNOSTIC_ASSERT(aClientHandle);
   MOZ_ASSERT(mHandleList.Contains(aClientHandle));
   mHandleList.RemoveElement(aClientHandle);
 }
 
+RefPtr<ClientOpPromise>
+ClientSourceParent::StartOp(const ClientOpConstructorArgs& aArgs)
+{
+  RefPtr<ClientOpPromise::Private> promise =
+    new ClientOpPromise::Private(__func__);
+
+  // If we are being controlled, remember that data before propagating
+  // on to the ClientSource.
+  if (aArgs.type() == ClientOpConstructorArgs::TClientControlledArgs) {
+    mController.reset();
+    mController.emplace(aArgs.get_ClientControlledArgs().serviceWorker());
+  }
+
+  // Constructor failure will reject the promise via ActorDestroy().
+  ClientSourceOpParent* actor = new ClientSourceOpParent(aArgs, promise);
+  Unused << SendPClientSourceOpConstructor(actor, aArgs);
+
+  return promise.forget();
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/clients/manager/ClientSourceParent.h b/dom/clients/manager/ClientSourceParent.h
--- a/dom/clients/manager/ClientSourceParent.h
+++ b/dom/clients/manager/ClientSourceParent.h
@@ -2,27 +2,30 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #ifndef _mozilla_dom_ClientSourceParent_h
 #define _mozilla_dom_ClientSourceParent_h
 
 #include "ClientInfo.h"
+#include "ClientOpPromise.h"
 #include "mozilla/dom/PClientSourceParent.h"
+#include "mozilla/dom/ServiceWorkerDescriptor.h"
 
 namespace mozilla {
 namespace dom {
 
 class ClientHandleParent;
 class ClientManagerService;
 
 class ClientSourceParent final : public PClientSourceParent
 {
   ClientInfo mClientInfo;
+  Maybe<ServiceWorkerDescriptor> mController;
   RefPtr<ClientManagerService> mService;
   nsTArray<ClientHandleParent*> mHandleList;
   bool mExecutionReady;
   bool mFrozen;
 
   void
   KillInvalidChild();
 
@@ -61,14 +64,17 @@ public:
   bool
   IsFrozen() const;
 
   void
   AttachHandle(ClientHandleParent* aClientSource);
 
   void
   DetachHandle(ClientHandleParent* aClientSource);
+
+  RefPtr<ClientOpPromise>
+  StartOp(const ClientOpConstructorArgs& aArgs);
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // _mozilla_dom_ClientSourceParent_h
