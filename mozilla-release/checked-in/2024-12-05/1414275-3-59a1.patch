# HG changeset patch
# User Gabriel Luong <gabriel.luong@gmail.com>
# Date 1510585333 18000
# Node ID 5125625bd2c3d263f2dcfd9bb5b4b2b39efd54ea
# Parent  2d20a1572587c9c298606f33dc6f1e8c7c9bd8a6
Bug 1414275 - Part 3: Add unit test for the flexbox highlighter toggle in the rule view. r=pbro

diff --git a/devtools/client/inspector/rules/test/browser.ini b/devtools/client/inspector/rules/test/browser.ini
--- a/devtools/client/inspector/rules/test/browser.ini
+++ b/devtools/client/inspector/rules/test/browser.ini
@@ -153,16 +153,25 @@ skip-if = os == "mac" # Bug 1245996 : cl
 [browser_rules_edit-value-after-name_04.js]
 [browser_rules_editable-field-focus_01.js]
 [browser_rules_editable-field-focus_02.js]
 [browser_rules_eyedropper.js]
 [browser_rules_filtereditor-appears-on-swatch-click.js]
 [browser_rules_filtereditor-commit-on-ENTER.js]
 [browser_rules_filtereditor-revert-on-ESC.js]
 skip-if = (os == "win" && debug) # bug 963492: win.
+[browser_rules_flexbox-highlighter-on-mutation.js]
+[browser_rules_flexbox-highlighter-on-navigate.js]
+[browser_rules_flexbox-highlighter-on-reload.js]
+[browser_rules_flexbox-highlighter-restored-after-reload.js]
+[browser_rules_flexbox-toggle_01.js]
+[browser_rules_flexbox-toggle_01b.js]
+[browser_rules_flexbox-toggle_02.js]
+[browser_rules_flexbox-toggle_03.js]
+[browser_rules_flexbox-toggle_04.js]
 [browser_rules_grid-highlighter-on-mutation.js]
 [browser_rules_grid-highlighter-on-navigate.js]
 [browser_rules_grid-highlighter-on-reload.js]
 [browser_rules_grid-highlighter-restored-after-reload.js]
 [browser_rules_grid-toggle_01.js]
 [browser_rules_grid-toggle_01b.js]
 [browser_rules_grid-toggle_02.js]
 [browser_rules_grid-toggle_03.js]
diff --git a/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-on-mutation.js b/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-on-mutation.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-on-mutation.js
@@ -0,0 +1,39 @@
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Tests that the flexbox highlighter is hidden when the highlighted flexbox container is
+// removed from the page.
+
+const TEST_URI = `
+  <style type='text/css'>
+    #flex {
+      display: flex;
+    }
+  </style>
+  <div id="flex"></div>
+`;
+
+add_task(function* () {
+  yield addTab("data:text/html;charset=utf-8," + encodeURIComponent(TEST_URI));
+  let {inspector, view, testActor} = yield openRuleView();
+  let {highlighters} = view;
+
+  yield selectNode("#flex", inspector);
+  let container = getRuleViewProperty(view, "#flex", "display").valueSpan;
+  let flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Toggling ON the flexbox highlighter from the rule-view.");
+  let onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  flexboxToggle.click();
+  yield onHighlighterShown;
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+
+  info("Remove the #flex container in the content page.");
+  let onHighlighterHidden = highlighters.once("flexbox-highlighter-hidden");
+  testActor.eval(`document.querySelector("#flex").remove();`);
+  yield onHighlighterHidden;
+  ok(!highlighters.flexboxHighlighterShown, "Flexbox highlighter is hidden.");
+});
diff --git a/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-on-navigate.js b/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-on-navigate.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-on-navigate.js
@@ -0,0 +1,37 @@
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test that flexbox highlighter is hidden on page navigation.
+
+const TEST_URI = `
+  <style type='text/css'>
+    #flex {
+      display: flex;
+    }
+  </style>
+  <div id="flex"></div>
+`;
+
+const TEST_URI_2 = "data:text/html,<html><body>test</body></html>";
+
+add_task(function* () {
+  yield addTab("data:text/html;charset=utf-8," + encodeURIComponent(TEST_URI));
+  let {inspector, view} = yield openRuleView();
+  let {highlighters} = view;
+
+  yield selectNode("#flex", inspector);
+  let container = getRuleViewProperty(view, "#flex", "display").valueSpan;
+  let flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Toggling ON the flexbox highlighter from the rule-view.");
+  let onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  flexboxToggle.click();
+  yield onHighlighterShown;
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+
+  yield navigateTo(inspector, TEST_URI_2);
+  ok(!highlighters.flexboxHighlighterShown, "Flexbox highlighter is hidden.");
+});
diff --git a/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-on-reload.js b/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-on-reload.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-on-reload.js
@@ -0,0 +1,48 @@
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test that a flexbox highlighter after reloading the page.
+
+const TEST_URI = `
+  <style type='text/css'>
+    #flex {
+      display: flex;
+    }
+  </style>
+  <div id="flex"></div>
+`;
+
+add_task(function* () {
+  yield addTab("data:text/html;charset=utf-8," + encodeURIComponent(TEST_URI));
+
+  info("Check that the flexbox highlighter can be displayed.");
+  yield checkFlexboxHighlighter();
+
+  info("Close the toolbox before reloading the tab.");
+  let target = TargetFactory.forTab(gBrowser.selectedTab);
+  yield gDevTools.closeToolbox(target);
+
+  yield refreshTab(gBrowser.selectedTab);
+
+  info("Check that the flexbox highlighter can be displayed after reloading the page.");
+  yield checkFlexboxHighlighter();
+});
+
+function* checkFlexboxHighlighter() {
+  let {inspector, view} = yield openRuleView();
+  let {highlighters} = view;
+
+  yield selectNode("#flex", inspector);
+  let container = getRuleViewProperty(view, "#flex", "display").valueSpan;
+  let flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Toggling ON the flexbox highlighter from the rule-view.");
+  let onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  flexboxToggle.click();
+  yield onHighlighterShown;
+
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+}
diff --git a/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-restored-after-reload.js b/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-restored-after-reload.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/rules/test/browser_rules_flexbox-highlighter-restored-after-reload.js
@@ -0,0 +1,61 @@
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Tests that the flexbox highlighter is re-displayed after reloading a page.
+
+const TEST_URI = `
+  <style type='text/css'>
+    #flex {
+      display: flex;
+    }
+  </style>
+  <div id="flex"></div>
+`;
+
+const OTHER_URI = `
+  <style type='text/css'>
+    #grid {
+      display: grid;
+    }
+  </style>
+  <div id="grid"></div>
+`;
+
+add_task(function* () {
+  yield addTab("data:text/html;charset=utf-8," + encodeURIComponent(TEST_URI));
+
+  info("Check that the flexbox highlighter can be displayed.");
+  let {inspector, view} = yield openRuleView();
+  let {highlighters} = view;
+
+  yield selectNode("#flex", inspector);
+  let container = getRuleViewProperty(view, "#flex", "display").valueSpan;
+  let flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Toggling ON the flexbox highlighter from the rule-view.");
+  let onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  flexboxToggle.click();
+  yield onHighlighterShown;
+
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+
+  info("Reload the page, expect the highlighter to be displayed once again");
+  let onStateRestored = highlighters.once("flexbox-state-restored");
+  yield refreshTab(gBrowser.selectedTab);
+  let { restored } = yield onStateRestored;
+  ok(restored, "The highlighter state was restored");
+
+  info("Check that the flexbox highlighter can be displayed after reloading the page");
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+
+  info("Navigate to another URL, and check that the highlighter is hidden");
+  let otherUri = "data:text/html;charset=utf-8," + encodeURIComponent(OTHER_URI);
+  onStateRestored = highlighters.once("flexbox-state-restored");
+  yield navigateTo(inspector, otherUri);
+  ({ restored } = yield onStateRestored);
+  ok(!restored, "The highlighter state was not restored");
+  ok(!highlighters.flexboxHighlighterShown, "Flexbox highlighter is hidden.");
+});
diff --git a/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_01.js b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_01.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_01.js
@@ -0,0 +1,61 @@
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test toggling the flexbox highlighter in the rule view and the display of the
+// flexbox highlighter.
+
+const TEST_URI = `
+  <style type='text/css'>
+    #flex {
+      display: flex;
+    }
+  </style>
+  <div id="flex"></div>
+`;
+
+const HIGHLIGHTER_TYPE = "FlexboxHighlighter";
+
+add_task(function* () {
+  yield addTab("data:text/html;charset=utf-8," + encodeURIComponent(TEST_URI));
+  let {inspector, view} = yield openRuleView();
+  let {highlighters} = view;
+
+  yield selectNode("#flex", inspector);
+  let container = getRuleViewProperty(view, "#flex", "display").valueSpan;
+  let flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Checking the initial state of the flexbox toggle in the rule-view.");
+  ok(flexboxToggle, "Flexbox highlighter toggle is visible.");
+  ok(!flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle button is not active.");
+  ok(!highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "No flexbox highlighter exists in the rule-view.");
+  ok(!highlighters.flexboxHighlighterShown, "No flexbox highlighter is shown.");
+
+  info("Toggling ON the flexbox highlighter from the rule-view.");
+  let onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  flexboxToggle.click();
+  yield onHighlighterShown;
+
+  info("Checking the flexbox highlighter is created and toggle button is active in " +
+    "the rule-view.");
+  ok(flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle is active.");
+  ok(highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "Flexbox highlighter created in the rule-view.");
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+
+  info("Toggling OFF the flexbox highlighter from the rule-view.");
+  let onHighlighterHidden = highlighters.once("flexbox-highlighter-hidden");
+  flexboxToggle.click();
+  yield onHighlighterHidden;
+
+  info("Checking the flexbox highlighter is not shown and toggle button is not active " +
+    "in the rule-view.");
+  ok(!flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle button is not active.");
+  ok(!highlighters.flexboxHighlighterShown, "No flexbox highlighter is shown.");
+});
diff --git a/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_01b.js b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_01b.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_01b.js
@@ -0,0 +1,61 @@
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test toggling the flebox highlighter in the rule view and the display of the
+// flexbox highlighter.
+
+const TEST_URI = `
+  <style type='text/css'>
+    #flex {
+      display: inline-flex;
+    }
+  </style>
+  <div id="flex"></div>
+`;
+
+const HIGHLIGHTER_TYPE = "FlexboxHighlighter";
+
+add_task(function* () {
+  yield addTab("data:text/html;charset=utf-8," + encodeURIComponent(TEST_URI));
+  let {inspector, view} = yield openRuleView();
+  let {highlighters} = view;
+
+  yield selectNode("#flex", inspector);
+  let container = getRuleViewProperty(view, "#flex", "display").valueSpan;
+  let flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Checking the initial state of the flexbox toggle in the rule-view.");
+  ok(flexboxToggle, "Flexbox highlighter toggle is visible.");
+  ok(!flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle button is not active.");
+  ok(!highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "No flexbox highlighter exists in the rule-view.");
+  ok(!highlighters.flexboxHighlighterShown, "No flexbox highlighter is shown.");
+
+  info("Toggling ON the flexbox highlighter from the rule-view.");
+  let onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  flexboxToggle.click();
+  yield onHighlighterShown;
+
+  info("Checking the flexbox highlighter is created and toggle button is active in " +
+    "the rule-view.");
+  ok(flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle is active.");
+  ok(highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "Flexbox highlighter created in the rule-view.");
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+
+  info("Toggling OFF the flexbox highlighter from the rule-view.");
+  let onHighlighterHidden = highlighters.once("flexbox-highlighter-hidden");
+  flexboxToggle.click();
+  yield onHighlighterHidden;
+
+  info("Checking the flexbox highlighter is not shown and toggle button is not active " +
+    "in the rule-view.");
+  ok(!flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle button is not active.");
+  ok(!highlighters.flexboxHighlighterShown, "No flexbox highlighter is shown.");
+});
diff --git a/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_02.js b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_02.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_02.js
@@ -0,0 +1,74 @@
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test toggling the flexbox highlighter in the rule view from an overridden
+// 'display: flex' declaration.
+
+const TEST_URI = `
+  <style type='text/css'>
+    #flex {
+      display: flex;
+    }
+    div, ul {
+      display: flex;
+    }
+  </style>
+  <ul id="flex">
+    <li>1</li>
+    <li>2</li>
+  </ul>
+`;
+
+const HIGHLIGHTER_TYPE = "FlexboxHighlighter";
+
+add_task(function* () {
+  yield addTab("data:text/html;charset=utf-8," + encodeURIComponent(TEST_URI));
+  let {inspector, view} = yield openRuleView();
+  let {highlighters} = view;
+
+  yield selectNode("#flex", inspector);
+  let container = getRuleViewProperty(view, "#flex", "display").valueSpan;
+  let flexboxToggle = container.querySelector(".ruleview-flex");
+  let overriddenContainer = getRuleViewProperty(view, "div, ul", "display").valueSpan;
+  let overriddenFlexboxToggle = overriddenContainer.querySelector(".ruleview-flex");
+
+  info("Checking the initial state of the flexbox toggle in the rule-view.");
+  ok(flexboxToggle && overriddenFlexboxToggle,
+    "Flexbox highlighter toggles are visible.");
+  ok(!flexboxToggle.classList.contains("active") &&
+    !overriddenFlexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle buttons are not active.");
+  ok(!highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "No flexbox highlighter exists in the rule-view.");
+  ok(!highlighters.flexboxHighlighterShown, "No flexbox highlighter is shown.");
+
+  info("Toggling ON the flexbox highlighter from the overridden rule in the rule-view.");
+  let onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  overriddenFlexboxToggle.click();
+  yield onHighlighterShown;
+
+  info("Checking the flexbox highlighter is created and toggle buttons are active in " +
+    "the rule-view.");
+  ok(flexboxToggle.classList.contains("active") &&
+    overriddenFlexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle is active.");
+  ok(highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "Flexbox highlighter created in the rule-view.");
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+
+  info("Toggling off the flexbox highlighter from the normal flexbox declaration in  " +
+    "the rule-view.");
+  let onHighlighterHidden = highlighters.once("flexbox-highlighter-hidden");
+  flexboxToggle.click();
+  yield onHighlighterHidden;
+
+  info("Checking the flexbox highlighter is not shown and toggle buttons are not " +
+    "active in the rule-view.");
+  ok(!flexboxToggle.classList.contains("active") &&
+    !overriddenFlexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle buttons are not active.");
+  ok(!highlighters.flexboxHighlighterShown, "No flexbox highlighter is shown.");
+});
diff --git a/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_03.js b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_03.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_03.js
@@ -0,0 +1,91 @@
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test toggling the flexbox highlighter in the rule view with multiple flexboxes in the
+// page.
+
+const TEST_URI = `
+  <style type='text/css'>
+    .flex {
+      display: flex;
+    }
+  </style>
+  <div id="flex1" class="flex"></div>
+  <div id="flex2" class="flex"></div>
+`;
+
+const HIGHLIGHTER_TYPE = "FlexboxHighlighter";
+
+add_task(function* () {
+  yield addTab("data:text/html;charset=utf-8," + encodeURIComponent(TEST_URI));
+  let {inspector, view} = yield openRuleView();
+  let {highlighters} = view;
+
+  info("Selecting the first flexbox container.");
+  yield selectNode("#flex1", inspector);
+  let container = getRuleViewProperty(view, ".flex", "display").valueSpan;
+  let flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Checking the state of the flexbox toggle for the first flexbox container in " +
+    "the rule-view.");
+  ok(flexboxToggle, "flexbox highlighter toggle is visible.");
+  ok(!flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle button is not active.");
+  ok(!highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "No flexbox highlighter exists in the rule-view.");
+  ok(!highlighters.flexboxHighlighterShown, "No flexbox highlighter is shown.");
+
+  info("Toggling ON the flexbox highlighter for the first flexbox container from the " +
+    "rule-view.");
+  let onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  flexboxToggle.click();
+  yield onHighlighterShown;
+
+  info("Checking the flexbox highlighter is created and toggle button is active in " +
+    "the rule-view.");
+  ok(flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle is active.");
+  ok(highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "Flexbox highlighter created in the rule-view.");
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+
+  info("Selecting the second flexbox container.");
+  yield selectNode("#flex2", inspector);
+  let firstFlexboxHighterShown = highlighters.flexboxHighlighterShown;
+  container = getRuleViewProperty(view, ".flex", "display").valueSpan;
+  flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Checking the state of the CSS flexbox toggle for the second flexbox container " +
+    "in the rule-view.");
+  ok(flexboxToggle, "Flexbox highlighter toggle is visible.");
+  ok(!flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle button is not active.");
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is still shown.");
+
+  info("Toggling ON the flexbox highlighter for the second flexbox container from the " +
+    "rule-view.");
+  onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  flexboxToggle.click();
+  yield onHighlighterShown;
+
+  info("Checking the flexbox highlighter is created for the second flexbox container " +
+    "and toggle button is active in the rule-view.");
+  ok(flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle is active.");
+  ok(highlighters.flexboxHighlighterShown != firstFlexboxHighterShown,
+    "Flexbox highlighter for the second flexbox container is shown.");
+
+  info("Selecting the first flexbox container.");
+  yield selectNode("#flex1", inspector);
+  container = getRuleViewProperty(view, ".flex", "display").valueSpan;
+  flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Checking the state of the flexbox toggle for the first flexbox container in " +
+    "the rule-view.");
+  ok(flexboxToggle, "Flexbox highlighter toggle is visible.");
+  ok(!flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle button is not active.");
+});
diff --git a/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_04.js b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_04.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/rules/test/browser_rules_flexbox-toggle_04.js
@@ -0,0 +1,61 @@
+/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test toggling the flexbox highlighter in the rule view from a
+// 'display: flex!important' declaration.
+
+const TEST_URI = `
+  <style type='text/css'>
+    #flex {
+      display: flex !important;
+    }
+  </style>
+  <div id="flex"></div>
+`;
+
+const HIGHLIGHTER_TYPE = "FlexboxHighlighter";
+
+add_task(function* () {
+  yield addTab("data:text/html;charset=utf-8," + encodeURIComponent(TEST_URI));
+  let {inspector, view} = yield openRuleView();
+  let {highlighters} = view;
+
+  yield selectNode("#flex", inspector);
+  let container = getRuleViewProperty(view, "#flex", "display").valueSpan;
+  let flexboxToggle = container.querySelector(".ruleview-flex");
+
+  info("Checking the initial state of the flexbox toggle in the rule-view.");
+  ok(flexboxToggle, "Flexbox highlighter toggle is visible.");
+  ok(!flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle button is not active.");
+  ok(!highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "No flexbox highlighter exists in the rule-view.");
+  ok(!highlighters.flexboxHighlighterShown, "No flexbox highlighter is shown.");
+
+  info("Toggling ON the flexbox highlighter from the rule-view.");
+  let onHighlighterShown = highlighters.once("flexbox-highlighter-shown");
+  flexboxToggle.click();
+  yield onHighlighterShown;
+
+  info("Checking the flexbox highlighter is created and toggle button is active in " +
+    "the rule-view.");
+  ok(flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle is active.");
+  ok(highlighters.highlighters[HIGHLIGHTER_TYPE],
+    "Flexbox highlighter created in the rule-view.");
+  ok(highlighters.flexboxHighlighterShown, "Flexbox highlighter is shown.");
+
+  info("Toggling OFF the flexbox highlighter from the rule-view.");
+  let onHighlighterHidden = highlighters.once("flexbox-highlighter-hidden");
+  flexboxToggle.click();
+  yield onHighlighterHidden;
+
+  info("Checking the flexbox highlighter is not shown and toggle button is not active " +
+    "in the rule-view.");
+  ok(!flexboxToggle.classList.contains("active"),
+    "Flexbox highlighter toggle button is not active.");
+  ok(!highlighters.flexboxHighlighterShown, "No flexbox highlighter is shown.");
+});
diff --git a/devtools/client/inspector/rules/test/head.js b/devtools/client/inspector/rules/test/head.js
--- a/devtools/client/inspector/rules/test/head.js
+++ b/devtools/client/inspector/rules/test/head.js
@@ -18,20 +18,22 @@ var {getInplaceEditorForSpan: inplaceEdi
   require("devtools/client/shared/inplace-editor");
 
 const ROOT_TEST_DIR = getRootDirectory(gTestPath);
 const FRAME_SCRIPT_URL = ROOT_TEST_DIR + "doc_frame_script.js";
 
 const STYLE_INSPECTOR_L10N
       = new LocalizationHelper("devtools/shared/locales/styleinspector.properties");
 
+Services.prefs.setBoolPref("devtools.inspector.flexboxHighlighter.enabled", true);
 Services.prefs.setBoolPref("devtools.inspector.shapesHighlighter.enabled", true);
 
 registerCleanupFunction(() => {
   Services.prefs.clearUserPref("devtools.defaultColorUnit");
+  Services.prefs.clearUserPref("devtools.inspector.flexboxHighlighter.enabled");
   Services.prefs.clearUserPref("devtools.inspector.shapesHighlighter.enabled");
 });
 
 /**
  * The rule-view tests rely on a frame-script to be injected in the content test
  * page. So override the shared-head's addTab to load the frame script after the
  * tab was added.
  * FIXME: Refactor the rule-view tests to use the testActor instead of a frame
