# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1515089984 -3600
# Node ID 7a4e812ee5adc626ba19828f770d720f0b9db9cd
# Parent  b922efc65bbb8b02b8abc57a0fc135c3bdace741
Bug 1425574 - Fill the feature gap between Console.jsm and Console API - part 8 - maxLogLevel, r=smaug

diff --git a/dom/console/Console.cpp b/dom/console/Console.cpp
--- a/dom/console/Console.cpp
+++ b/dom/console/Console.cpp
@@ -661,19 +661,21 @@ private:
 
   RefPtr<ConsoleCallData> mCallData;
 };
 
 // This runnable calls ProfileMethod() on the console on the main-thread.
 class ConsoleProfileRunnable final : public ConsoleRunnable
 {
 public:
-  ConsoleProfileRunnable(Console* aConsole, const nsAString& aAction,
+  ConsoleProfileRunnable(Console* aConsole, Console::MethodName aName,
+                         const nsAString& aAction,
                          const Sequence<JS::Value>& aArguments)
     : ConsoleRunnable(aConsole)
+    , mName(aName)
     , mAction(aAction)
     , mArguments(aArguments)
   {
     MOZ_ASSERT(aConsole);
   }
 
 private:
   bool
@@ -744,23 +746,24 @@ private:
         return;
       }
 
       if (!arguments.AppendElement(value, fallible)) {
         return;
       }
     }
 
-    mConsole->ProfileMethodInternal(aCx, mAction, arguments);
+    mConsole->ProfileMethodInternal(aCx, mName, mAction, arguments);
   }
 
   virtual void
   ReleaseData() override
   {}
 
+  Console::MethodName mName;
   nsString mAction;
 
   // This is a reference of the sequence of arguments we receive from the DOM
   // bindings and it's rooted by them. It's only used on the owning thread in
   // PreDispatch().
   const Sequence<JS::Value>& mArguments;
 };
 
@@ -818,16 +821,17 @@ Console::Create(nsPIDOMWindowInner* aWin
 }
 
 Console::Console(nsPIDOMWindowInner* aWindow)
   : mWindow(aWindow)
   , mOuterID(0)
   , mInnerID(0)
   , mDumpToStdout(false)
   , mChromeInstance(false)
+  , mMaxLogLevel(ConsoleLogLevel::All)
   , mStatus(eUnknown)
   , mCreationTimeStamp(TimeStamp::Now())
 {
   if (mWindow) {
     MOZ_ASSERT(mWindow->IsInnerWindow());
     mInnerID = mWindow->WindowID();
 
     // Without outerwindow any console message coming from this object will not
@@ -1044,65 +1048,72 @@ Console::TimeStamp(const GlobalObject& a
   }
 
   Method(aGlobal, MethodTimeStamp, NS_LITERAL_STRING("timeStamp"), data);
 }
 
 /* static */ void
 Console::Profile(const GlobalObject& aGlobal, const Sequence<JS::Value>& aData)
 {
-  ProfileMethod(aGlobal, NS_LITERAL_STRING("profile"), aData);
+  ProfileMethod(aGlobal, MethodProfile, NS_LITERAL_STRING("profile"), aData);
 }
 
 /* static */ void
 Console::ProfileEnd(const GlobalObject& aGlobal,
                     const Sequence<JS::Value>& aData)
 {
-  ProfileMethod(aGlobal, NS_LITERAL_STRING("profileEnd"), aData);
+  ProfileMethod(aGlobal, MethodProfileEnd, NS_LITERAL_STRING("profileEnd"),
+                aData);
 }
 
 /* static */ void
-Console::ProfileMethod(const GlobalObject& aGlobal, const nsAString& aAction,
+Console::ProfileMethod(const GlobalObject& aGlobal, MethodName aName,
+                       const nsAString& aAction,
                        const Sequence<JS::Value>& aData)
 {
   RefPtr<Console> console = GetConsole(aGlobal);
   if (!console) {
     return;
   }
 
   JSContext* cx = aGlobal.Context();
-  console->ProfileMethodInternal(cx, aAction, aData);
+  console->ProfileMethodInternal(cx, aName, aAction, aData);
 }
 
 bool
 Console::IsEnabled(JSContext* aCx) const
 {
   // Console is always enabled if it is a custom Chrome-Only instance.
   if (mChromeInstance) {
     return true;
   }
 
   // Make all Console API no-op if DevTools aren't enabled.
   return nsContentUtils::DevToolsEnabled(aCx);
 }
 
 void
-Console::ProfileMethodInternal(JSContext* aCx, const nsAString& aAction,
+Console::ProfileMethodInternal(JSContext* aCx, MethodName aMethodName,
+                               const nsAString& aAction,
                                const Sequence<JS::Value>& aData)
 {
   if (!IsEnabled(aCx)) {
     return;
   }
 
+  if (!ShouldProceed(aMethodName)) {
+    return;
+  }
+
   MaybeExecuteDumpFunction(aCx, aAction, aData);
 
   if (!NS_IsMainThread()) {
     // Here we are in a worker thread.
     RefPtr<ConsoleProfileRunnable> runnable =
-      new ConsoleProfileRunnable(this, aAction, aData);
+      new ConsoleProfileRunnable(this, aMethodName, aAction, aData);
 
     runnable->Dispatch(aCx);
     return;
   }
 
   ClearException ce(aCx);
 
   RootedDictionary<ConsoleProfileEvent> event(aCx);
@@ -1223,16 +1234,20 @@ void
 Console::MethodInternal(JSContext* aCx, MethodName aMethodName,
                         const nsAString& aMethodString,
                         const Sequence<JS::Value>& aData)
 {
   if (!IsEnabled(aCx)) {
     return;
   }
 
+  if (!ShouldProceed(aMethodName)) {
+    return;
+  }
+
   AssertIsOnOwningThread();
 
   RefPtr<ConsoleCallData> callData(new ConsoleCallData());
 
   ClearException ce(aCx);
 
   if (NS_WARN_IF(!callData->Initialize(aCx, aMethodName, aMethodString,
                                        aData, this))) {
@@ -2663,10 +2678,79 @@ Console::ExecuteDumpFunction(const nsASt
   MOZ_LOG(nsContentUtils::DOMDumpLog(), LogLevel::Debug, ("%s", str.get()));
 #ifdef ANDROID
   __android_log_print(ANDROID_LOG_INFO, "Gecko", "%s", str.get());
 #endif
   fputs(str.get(), stdout);
   fflush(stdout);
 }
 
+bool
+Console::ShouldProceed(MethodName aName) const
+{
+  return WebIDLLogLevelToInteger(mMaxLogLevel) <=
+           InternalLogLevelToInteger(aName);
+}
+
+uint32_t
+Console::WebIDLLogLevelToInteger(ConsoleLogLevel aLevel) const
+{
+  switch (aLevel) {
+    case ConsoleLogLevel::All: return 0;
+    case ConsoleLogLevel::Debug: return 2;
+    case ConsoleLogLevel::Log: return 3;
+    case ConsoleLogLevel::Info: return 3;
+    case ConsoleLogLevel::Clear: return 3;
+    case ConsoleLogLevel::Trace: return 3;
+    case ConsoleLogLevel::TimeEnd: return 3;
+    case ConsoleLogLevel::Time: return 3;
+    case ConsoleLogLevel::Group: return 3;
+    case ConsoleLogLevel::GroupEnd: return 3;
+    case ConsoleLogLevel::Profile: return 3;
+    case ConsoleLogLevel::ProfileEnd: return 3;
+    case ConsoleLogLevel::Dir: return 3;
+    case ConsoleLogLevel::Dirxml: return 3;
+    case ConsoleLogLevel::Warn: return 4;
+    case ConsoleLogLevel::Error: return 5;
+    case ConsoleLogLevel::Off: return UINT32_MAX;
+    default:
+      MOZ_CRASH("ConsoleLogLevel is out of sync with the Console implementation!");
+      return 0;
+  }
+
+  return 0;
+}
+
+uint32_t
+Console::InternalLogLevelToInteger(MethodName aName) const
+{
+  switch (aName) {
+    case MethodLog: return 3;
+    case MethodInfo: return 3;
+    case MethodWarn: return 4;
+    case MethodError: return 5;
+    case MethodException: return 5;
+    case MethodDebug: return 2;
+    case MethodTable: return 3;
+    case MethodTrace: return 3;
+    case MethodDir: return 3;
+    case MethodDirxml: return 3;
+    case MethodGroup: return 3;
+    case MethodGroupCollapsed: return 3;
+    case MethodGroupEnd: return 3;
+    case MethodTime: return 3;
+    case MethodTimeEnd: return 3;
+    case MethodTimeStamp: return 3;
+    case MethodAssert: return 3;
+    case MethodCount: return 3;
+    case MethodClear: return 3;
+    case MethodProfile: return 3;
+    case MethodProfileEnd: return 3;
+    default:
+      MOZ_CRASH("MethodName is out of sync with the Console implementation!");
+      return 0;
+  }
+
+  return 0;
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/console/Console.h b/dom/console/Console.h
--- a/dom/console/Console.h
+++ b/dom/console/Console.h
@@ -2,18 +2,17 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_Console_h
 #define mozilla_dom_Console_h
 
-#include "mozilla/dom/BindingDeclarations.h"
-#include "mozilla/ErrorResult.h"
+#include "mozilla/dom/ConsoleBinding.h"
 #include "mozilla/JSObjectHolder.h"
 #include "mozilla/TimeStamp.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsDataHashtable.h"
 #include "nsHashKeys.h"
 #include "nsIObserver.h"
 #include "nsWeakReference.h"
 #include "nsDOMNavigationTiming.h"
@@ -28,19 +27,16 @@ namespace dom {
 
 class AnyCallback;
 class ConsoleCallData;
 class ConsoleInstance;
 class ConsoleInstanceDumpCallback;
 class ConsoleRunnable;
 class ConsoleCallDataRunnable;
 class ConsoleProfileRunnable;
-struct ConsoleInstanceOptions;
-struct ConsoleTimerError;
-struct ConsoleStackEntry;
 
 class Console final : public nsIObserver
                     , public nsSupportsWeakReference
 {
 public:
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS_AMBIGUOUS(Console, nsIObserver)
   NS_DECL_NSIOBSERVER
@@ -157,31 +153,34 @@ private:
     MethodGroup,
     MethodGroupCollapsed,
     MethodGroupEnd,
     MethodTime,
     MethodTimeEnd,
     MethodTimeStamp,
     MethodAssert,
     MethodCount,
-    MethodClear
+    MethodClear,
+    MethodProfile,
+    MethodProfileEnd,
   };
 
   static already_AddRefed<Console>
   GetConsole(const GlobalObject& aGlobal);
 
   static already_AddRefed<Console>
   GetConsoleInternal(const GlobalObject& aGlobal, ErrorResult &aRv);
 
   static void
-  ProfileMethod(const GlobalObject& aGlobal, const nsAString& aAction,
-                const Sequence<JS::Value>& aData);
+  ProfileMethod(const GlobalObject& aGlobal, MethodName aName,
+                const nsAString& aAction, const Sequence<JS::Value>& aData);
 
   void
-  ProfileMethodInternal(JSContext* aCx, const nsAString& aAction,
+  ProfileMethodInternal(JSContext* aCx, MethodName aName,
+                        const nsAString& aAction,
                         const Sequence<JS::Value>& aData);
 
   static void
   Method(const GlobalObject& aGlobal, MethodName aName,
          const nsAString& aString, const Sequence<JS::Value>& aData);
 
   void
   MethodInternal(JSContext* aCx, MethodName aName,
@@ -396,16 +395,25 @@ private:
   MaybeExecuteDumpFunctionForTrace(JSContext* aCx, nsIStackFrame* aStack);
 
   void
   ExecuteDumpFunction(const nsAString& aMessage);
 
   bool
   IsEnabled(JSContext* aCx) const;
 
+  bool
+  ShouldProceed(MethodName aName) const;
+
+  uint32_t
+  WebIDLLogLevelToInteger(ConsoleLogLevel aLevel) const;
+
+  uint32_t
+  InternalLogLevelToInteger(MethodName aName) const;
+
   // All these nsCOMPtr are touched on main thread only.
   nsCOMPtr<nsPIDOMWindowInner> mWindow;
   nsCOMPtr<nsIConsoleAPIStorage> mStorage;
   RefPtr<JSObjectHolder> mSandbox;
 
   // Touched on the owner thread.
   nsDataHashtable<nsStringHashKey, DOMHighResTimeStamp> mTimerRegistry;
   nsDataHashtable<nsStringHashKey, uint32_t> mCounterRegistry;
@@ -433,16 +441,17 @@ private:
 
   // Set only by ConsoleInstance:
   nsString mConsoleID;
   nsString mPassedInnerID;
   RefPtr<ConsoleInstanceDumpCallback> mDumpFunction;
   bool mDumpToStdout;
   nsString mDumpPrefix;
   bool mChromeInstance;
+  ConsoleLogLevel mMaxLogLevel;
 
   enum {
     eUnknown,
     eInitialized,
     eShuttingDown
   } mStatus;
 
   // This is used when Console is created and it's used only for JSM custom
diff --git a/dom/console/ConsoleInstance.cpp b/dom/console/ConsoleInstance.cpp
--- a/dom/console/ConsoleInstance.cpp
+++ b/dom/console/ConsoleInstance.cpp
@@ -31,16 +31,20 @@ ConsoleInstance::ConsoleInstance(const C
     // For historical reasons, ConsoleInstance prints messages on stdout.
     mConsole->mDumpToStdout = true;
   }
 
   mConsole->mDumpPrefix = aOptions.mPrefix;
 
   // Let's inform that this is a custom instance.
   mConsole->mChromeInstance = true;
+
+  if (aOptions.mMaxLogLevel.WasPassed()) {
+    mConsole->mMaxLogLevel = aOptions.mMaxLogLevel.Value();
+  }
 }
 
 ConsoleInstance::~ConsoleInstance()
 {}
 
 JSObject*
 ConsoleInstance::WrapObject(JSContext* aCx, JS::Handle<JSObject*> aGivenProto)
 {
@@ -106,23 +110,25 @@ ConsoleInstance::TimeStamp(JSContext* aC
 
   mConsole->MethodInternal(aCx, Console::MethodTimeStamp,
                            NS_LITERAL_STRING("timeStamp"), data);
 }
 
 void
 ConsoleInstance::Profile(JSContext* aCx, const Sequence<JS::Value>& aData)
 {
-  mConsole->ProfileMethodInternal(aCx, NS_LITERAL_STRING("profile"), aData);
+  mConsole->ProfileMethodInternal(aCx, Console::MethodProfile,
+                                  NS_LITERAL_STRING("profile"), aData);
 }
 
 void
 ConsoleInstance::ProfileEnd(JSContext* aCx, const Sequence<JS::Value>& aData)
 {
-  mConsole->ProfileMethodInternal(aCx, NS_LITERAL_STRING("profileEnd"), aData);
+  mConsole->ProfileMethodInternal(aCx, Console::MethodProfileEnd,
+                                  NS_LITERAL_STRING("profileEnd"), aData);
 }
 
 void
 ConsoleInstance::Assert(JSContext* aCx, bool aCondition,
                         const Sequence<JS::Value>& aData)
 {
   if (!aCondition) {
     mConsole->MethodInternal(aCx, Console::MethodAssert,
diff --git a/dom/webidl/Console.webidl b/dom/webidl/Console.webidl
--- a/dom/webidl/Console.webidl
+++ b/dom/webidl/Console.webidl
@@ -151,14 +151,20 @@ interface ConsoleInstance {
   void timeStamp(optional any data);
 
   void profile(any... data);
   void profileEnd(any... data);
 };
 
 callback ConsoleInstanceDumpCallback = void (DOMString message);
 
+enum ConsoleLogLevel {
+  "all", "debug", "log", "info", "clear", "trace", "timeEnd", "time", "group",
+  "groupEnd", "profile", "profileEnd", "dir", "dirxml", "warn", "error", "off"
+};
+
 dictionary ConsoleInstanceOptions {
   ConsoleInstanceDumpCallback dump;
   DOMString prefix = "";
   DOMString innerID = "";
   DOMString consoleID = "";
+  ConsoleLogLevel maxLogLevel;
 };
