# HG changeset patch
# User Natalia Kulatova <nkulatova@mozilla.com>
# Date 1675963514 0
#      Thu Feb 09 17:25:14 2023 +0000
# Node ID 48bc459e51d80c00f033180e30e0806b292af0fe
# Parent  47da5c3c1655a7399ec3acdc23f39bb437b78225
Bug 1808725 - land NSS NSS_3_88_RTM UPGRADE_NSS_RELEASE, r=nss-reviewers,jschanck

Differential Revision: https://phabricator.services.mozilla.com/D169333

diff --git a/security/nss/TAG-INFO b/security/nss/TAG-INFO
--- a/security/nss/TAG-INFO
+++ b/security/nss/TAG-INFO
@@ -1,1 +1,1 @@
-NSS_3_88_BETA1
\ No newline at end of file
+NSS_3_88_RTM
\ No newline at end of file
diff --git a/security/nss/coreconf/coreconf.dep b/security/nss/coreconf/coreconf.dep
--- a/security/nss/coreconf/coreconf.dep
+++ b/security/nss/coreconf/coreconf.dep
@@ -5,9 +5,8 @@
 
 /*
  * A dummy header file that is a dependency for all the object files.
  * Used to force a full recompilation of NSS in Mozilla's Tinderbox
  * depend builds.  See comments in rules.mk.
  */
 
 #error "Do not include this header file."
-
diff --git a/security/nss/doc/rst/releases/index.rst b/security/nss/doc/rst/releases/index.rst
--- a/security/nss/doc/rst/releases/index.rst
+++ b/security/nss/doc/rst/releases/index.rst
@@ -3,16 +3,17 @@
 Releases
 ========
 
 .. toctree::
    :maxdepth: 0
    :glob:
    :hidden:
 
+   nss_3_88.rst
    nss_3_87.rst
    nss_3_86.rst
    nss_3_85.rst
    nss_3_84.rst
    nss_3_83.rst
    nss_3_82.rst
    nss_3_81.rst
    nss_3_80.rst
@@ -41,29 +42,43 @@ Releases
    nss_3_68.rst
    nss_3_67.rst
    nss_3_66.rst
    nss_3_65.rst
    nss_3_64.rst
 
 .. note::
 
-   **NSS 3.87** is the latest version of NSS.
-   Complete release notes are available here: :ref:`mozilla_projects_nss_nss_3_87_release_notes`
+   **NSS 3.88** is the latest version of NSS.
+   Complete release notes are available here: :ref:`mozilla_projects_nss_nss_3_88_release_notes`
 
    **NSS 3.79.2** is the latest ESR version of NSS.
    Complete release notes are available here: :ref:`mozilla_projects_nss_nss_3_79_2_release_notes`
 
 
 .. container::
 
-   Changes in 3.87 included in this release:
+   Changes in 3.88 included in this release:
 
-   - Bug 1803226 - NULL password encoding incorrect.
-   - Bug 1804071 - Fix rng stub signature for fuzzing builds.
-   - Bug 1803595 - Updating the compiler parsing for build.
-   - Bug 1749030 - Modification of supported compilers.
-   - Bug 1774654 tstclnt crashes when accessing gnutls server without a user cert in the database.
-   - Bug 1751707 - Add configuration option to enable source-based coverage sanitizer.
-   - Bug 1751705 - Update ECCKiila generated files.
-   - Bug 1730353 - Add support for the LoongArch 64-bit architecture.
-   - Bug 1798823 - add checks for zero-length RSA modulus to avoid memory errors and failed assertions later.
-   - Bug 1798823 - Additional zero-length RSA modulus checks.
\ No newline at end of file
+   - Bug 1815870 - use a different treeherder symbol for each docker image build task.
+   - Bug 1815868 - pin an older version of the ubuntu:18.04 and 20.04 docker images 
+   - Bug 1810702 - remove nested table in rst doc
+   - Bug 1815246 - Export NSS_CMSSignerInfo_GetDigestAlgTag. 
+   - Bug 1812671 - build failure while implicitly casting SECStatus to PRUInt32. r=nss-reviewers,mt
+   - Bug 1212915 - Add check for ClientHello SID max length. This is tested by Bogo tests 
+   - Bug 1771100 - Added EarlyData ALPN test support to BoGo shim. 
+   - Bug 1790357 - ECH client - Discard resumption TLS < 1.3 Session(IDs|Tickets) if ECH configs are setup.
+   - Bug 1714245 - On HRR skip PSK incompatible with negotiated ciphersuites hash algorithm. 
+   - Bug 1789410 - ECH client: Send ech_required alert on server negotiating TLS 1.2. Fixed misleading Gtest, enabled corresponding BoGo test.
+   - Bug 1771100 - Added Bogo ECH rejection test support.
+   - Bug 1771100 - Added ECH 0Rtt support to BoGo shim. 
+   - Bug 1747957 - RSA OAEP Wycheproof JSON
+   - Bug 1747957 - RSA decrypt Wycheproof JSON
+   - Bug 1747957 - ECDSA Wycheproof JSON
+   - Bug 1747957 - ECDH Wycheproof JSON
+   - Bug 1747957 - PKCS#1v1.5 wycheproof json
+   - Bug 1747957 - Use X25519 wycheproof json
+   - Bug 1766767 - Move scripts to python3
+   - Bug 1809627 - Properly link FuzzingEngine for oss-fuzz.
+   - Bug 1805907 - Extending RSA-PSS bltest test coverage (Adding SHA-256 and SHA-384) 
+   - Bug 1804091 NSS needs to move off of DSA for integrity checks
+   - Bug 1805815 - Add initial testing with ACVP vector sets using acvp-rust
+   - Bug 1806369 - Don't clone libFuzzer, rely on clang instead
\ No newline at end of file
diff --git a/security/nss/doc/rst/releases/nss_3_88.rst b/security/nss/doc/rst/releases/nss_3_88.rst
new file mode 100644
--- /dev/null
+++ b/security/nss/doc/rst/releases/nss_3_88.rst
@@ -0,0 +1,82 @@
+.. _mozilla_projects_nss_nss_3_88_release_notes:
+
+NSS 3.88 release notes
+======================
+
+`Introduction <#introduction>`__
+--------------------------------
+
+.. container::
+
+   Network Security Services (NSS) 3.88 was released on *9 February 2023**.
+
+
+.. _distribution_information:
+
+`Distribution Information <#distribution_information>`__
+--------------------------------------------------------
+
+.. container::
+
+   The HG tag is NSS_3_88_RTM. NSS 3.88 requires NSPR 4.35 or newer.
+
+   NSS 3.88 source distributions are available on ftp.mozilla.org for secure HTTPS download:
+
+   -  Source tarballs:
+      https://ftp.mozilla.org/pub/mozilla.org/security/nss/releases/NSS_3_88_RTM/src/
+
+   Other releases are available :ref:`mozilla_projects_nss_releases`.
+
+.. _changes_in_nss_3.88:
+
+`Changes in NSS 3.88 <#changes_in_nss_3.88>`__
+----------------------------------------------------
+
+.. container::
+
+   - Bug 1815870 - use a different treeherder symbol for each docker image build task.
+   - Bug 1815868 - pin an older version of the ubuntu:18.04 and 20.04 docker images 
+   - Bug 1810702 - remove nested table in rst doc
+   - Bug 1815246 - Export NSS_CMSSignerInfo_GetDigestAlgTag. 
+   - Bug 1812671 - build failure while implicitly casting SECStatus to PRUInt32. r=nss-reviewers,mt
+   - Bug 1212915 - Add check for ClientHello SID max length. This is tested by Bogo tests 
+   - Bug 1771100 - Added EarlyData ALPN test support to BoGo shim. 
+   - Bug 1790357 - ECH client - Discard resumption TLS < 1.3 Session(IDs|Tickets) if ECH configs are setup.
+   - Bug 1714245 - On HRR skip PSK incompatible with negotiated ciphersuites hash algorithm. 
+   - Bug 1789410 - ECH client: Send ech_required alert on server negotiating TLS 1.2. Fixed misleading Gtest, enabled corresponding BoGo test.
+   - Bug 1771100 - Added Bogo ECH rejection test support.
+   - Bug 1771100 - Added ECH 0Rtt support to BoGo shim. 
+   - Bug 1747957 - RSA OAEP Wycheproof JSON
+   - Bug 1747957 - RSA decrypt Wycheproof JSON
+   - Bug 1747957 - ECDSA Wycheproof JSON
+   - Bug 1747957 - ECDH Wycheproof JSON
+   - Bug 1747957 - PKCS#1v1.5 wycheproof json
+   - Bug 1747957 - Use X25519 wycheproof json
+   - Bug 1766767 - Move scripts to python3
+   - Bug 1809627 - Properly link FuzzingEngine for oss-fuzz.
+   - Bug 1805907 - Extending RSA-PSS bltest test coverage (Adding SHA-256 and SHA-384) 
+   - Bug 1804091 NSS needs to move off of DSA for integrity checks
+   - Bug 1805815 - Add initial testing with ACVP vector sets using acvp-rust
+   - Bug 1806369 - Don't clone libFuzzer, rely on clang instead
+
+
+
+`Compatibility <#compatibility>`__
+----------------------------------
+
+.. container::
+
+   NSS 3.88 shared libraries are backwards-compatible with all older NSS 3.x shared
+   libraries. A program linked with older NSS 3.x shared libraries will work with
+   this new version of the shared libraries without recompiling or
+   relinking. Furthermore, applications that restrict their use of NSS APIs to the
+   functions listed in NSS Public Functions will remain compatible with future
+   versions of the NSS shared libraries.
+
+`Feedback <#feedback>`__
+------------------------
+
+.. container::
+
+   Bugs discovered should be reported by filing a bug report on
+   `bugzilla.mozilla.org <https://bugzilla.mozilla.org/enter_bug.cgi?product=NSS>`__ (product NSS).
diff --git a/security/nss/lib/nss/nss.h b/security/nss/lib/nss/nss.h
--- a/security/nss/lib/nss/nss.h
+++ b/security/nss/lib/nss/nss.h
@@ -17,22 +17,22 @@
 
 /*
  * NSS's major version, minor version, patch level, build number, and whether
  * this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define NSS_VERSION "3.88" _NSS_CUSTOMIZED " Beta"
+#define NSS_VERSION "3.88" _NSS_CUSTOMIZED
 #define NSS_VMAJOR 3
 #define NSS_VMINOR 88
 #define NSS_VPATCH 0
 #define NSS_VBUILD 0
-#define NSS_BETA PR_TRUE
+#define NSS_BETA PR_FALSE
 
 #ifndef RC_INVOKED
 
 #include "seccomon.h"
 
 typedef struct NSSInitParametersStr NSSInitParameters;
 
 /*
diff --git a/security/nss/lib/softoken/softkver.h b/security/nss/lib/softoken/softkver.h
--- a/security/nss/lib/softoken/softkver.h
+++ b/security/nss/lib/softoken/softkver.h
@@ -12,16 +12,16 @@
 
 /*
  * Softoken's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define SOFTOKEN_VERSION "3.88" SOFTOKEN_ECC_STRING " Beta"
+#define SOFTOKEN_VERSION "3.88" SOFTOKEN_ECC_STRING
 #define SOFTOKEN_VMAJOR 3
 #define SOFTOKEN_VMINOR 88
 #define SOFTOKEN_VPATCH 0
 #define SOFTOKEN_VBUILD 0
-#define SOFTOKEN_BETA PR_TRUE
+#define SOFTOKEN_BETA PR_FALSE
 
 #endif /* _SOFTKVER_H_ */
diff --git a/security/nss/lib/util/nssutil.h b/security/nss/lib/util/nssutil.h
--- a/security/nss/lib/util/nssutil.h
+++ b/security/nss/lib/util/nssutil.h
@@ -14,22 +14,22 @@
 
 /*
  * NSS utilities's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <Beta>]"
  */
-#define NSSUTIL_VERSION "3.88 Beta"
+#define NSSUTIL_VERSION "3.88"
 #define NSSUTIL_VMAJOR 3
 #define NSSUTIL_VMINOR 88
 #define NSSUTIL_VPATCH 0
 #define NSSUTIL_VBUILD 0
-#define NSSUTIL_BETA PR_TRUE
+#define NSSUTIL_BETA PR_FALSE
 
 SEC_BEGIN_PROTOS
 
 /*
  * Returns a const string of the UTIL library version.
  */
 extern const char *NSSUTIL_GetVersion(void);
 
