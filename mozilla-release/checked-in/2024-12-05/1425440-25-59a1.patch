# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1516108631 -3600
#      Tue Jan 16 14:17:11 2018 +0100
# Node ID 5fc568e52a68faaaf24be23198d5ab309500e622
# Parent  9ef23aa75c3d494ea56cd2ecf72c3b326acd7249
Bug 1425440 - Get rid of GetChildAt_Deprecated in nsGenericDOMDataNode, r=catalinb

diff --git a/dom/base/nsGenericDOMDataNode.cpp b/dom/base/nsGenericDOMDataNode.cpp
--- a/dom/base/nsGenericDOMDataNode.cpp
+++ b/dom/base/nsGenericDOMDataNode.cpp
@@ -771,69 +771,84 @@ nsGenericDOMDataNode::SplitText(uint32_t
   nsCOMPtr<nsIContent> newChild;
   nsresult rv = SplitData(aOffset, getter_AddRefs(newChild));
   if (NS_SUCCEEDED(rv)) {
     rv = CallQueryInterface(newChild, aReturn);
   }
   return rv;
 }
 
-/* static */ int32_t
-nsGenericDOMDataNode::FirstLogicallyAdjacentTextNode(nsIContent* aParent,
-                                                     int32_t aIndex)
+static nsIContent*
+FirstLogicallyAdjacentTextNode(nsIContent* aNode)
 {
-  while (aIndex-- > 0) {
-    nsIContent* sibling = aParent->GetChildAt_Deprecated(aIndex);
-    if (!sibling->IsNodeOfType(nsINode::eTEXT))
-      return aIndex + 1;
+  nsCOMPtr<nsIContent> parent = aNode->GetParent();
+
+  while (aNode) {
+    nsIContent* sibling = aNode->GetPreviousSibling();
+    if (!sibling || !sibling->IsNodeOfType(nsINode::eTEXT)) {
+      return aNode;
+    }
+    aNode = sibling;
   }
-  return 0;
+
+  return parent->GetFirstChild();
 }
 
-/* static */ int32_t
-nsGenericDOMDataNode::LastLogicallyAdjacentTextNode(nsIContent* aParent,
-                                                    int32_t aIndex,
-                                                    uint32_t aCount)
+static nsIContent*
+LastLogicallyAdjacentTextNode(nsIContent* aNode)
 {
-  while (++aIndex < int32_t(aCount)) {
-    nsIContent* sibling = aParent->GetChildAt_Deprecated(aIndex);
-    if (!sibling->IsNodeOfType(nsINode::eTEXT))
-      return aIndex - 1;
+  nsCOMPtr<nsIContent> parent = aNode->GetParent();
+
+  while (aNode) {
+    nsIContent* sibling = aNode->GetNextSibling();
+    if (!sibling) break;
+
+    if (!sibling->IsNodeOfType(nsINode::eTEXT)) {
+      return aNode;
+    }
+
+    aNode = sibling;
   }
-  return aCount - 1;
+
+  return parent->GetLastChild();
 }
 
 nsresult
 nsGenericDOMDataNode::GetWholeText(nsAString& aWholeText)
 {
   nsIContent* parent = GetParent();
 
   // Handle parent-less nodes
   if (!parent)
     return GetData(aWholeText);
 
   int32_t index = parent->IndexOf(this);
   NS_WARNING_ASSERTION(index >= 0,
                        "Trying to use .wholeText with an anonymous"
                        "text node child of a binding parent?");
   NS_ENSURE_TRUE(index >= 0, NS_ERROR_DOM_NOT_SUPPORTED_ERR);
-  int32_t first =
-    FirstLogicallyAdjacentTextNode(parent, index);
-  int32_t last =
-    LastLogicallyAdjacentTextNode(parent, index, parent->GetChildCount());
+  nsCOMPtr<nsIContent> first = FirstLogicallyAdjacentTextNode(this);
+  nsCOMPtr<nsIContent> last = LastLogicallyAdjacentTextNode(this);
 
   aWholeText.Truncate();
 
   nsCOMPtr<nsIDOMText> node;
   nsAutoString tmp;
-  do {
-    node = do_QueryInterface(parent->GetChildAt_Deprecated(first));
+
+  while (true) {
+    node = do_QueryInterface(first);
     node->GetData(tmp);
     aWholeText.Append(tmp);
-  } while (first++ < last);
+
+    if (first == last) {
+      break;
+    }
+
+    first = first->GetNextSibling();
+  }
 
   return NS_OK;
 }
 
 //----------------------------------------------------------------------
 
 // Implementation of the nsIContent interface text functions
 
diff --git a/dom/base/nsGenericDOMDataNode.h b/dom/base/nsGenericDOMDataNode.h
--- a/dom/base/nsGenericDOMDataNode.h
+++ b/dom/base/nsGenericDOMDataNode.h
@@ -229,23 +229,16 @@ protected:
 
     return parent && parent->IsElement() ? parent->AsElement() : nullptr;
   }
 
   nsresult SplitText(uint32_t aOffset, nsIDOMText** aReturn);
 
   nsresult GetWholeText(nsAString& aWholeText);
 
-  static int32_t FirstLogicallyAdjacentTextNode(nsIContent* aParent,
-                                                int32_t aIndex);
-
-  static int32_t LastLogicallyAdjacentTextNode(nsIContent* aParent,
-                                               int32_t aIndex,
-                                               uint32_t aCount);
-
   nsresult SetTextInternal(uint32_t aOffset, uint32_t aCount,
                            const char16_t* aBuffer, uint32_t aLength,
                            bool aNotify,
                            CharacterDataChangeInfo::Details* aDetails = nullptr);
 
   /**
    * Method to clone this node. This needs to be overriden by all derived
    * classes. If aCloneText is true the text content will be cloned too.
