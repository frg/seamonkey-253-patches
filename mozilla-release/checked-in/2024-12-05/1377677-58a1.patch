# HG changeset patch
# User Oriol Brufau <oriol-bugzilla@hotmail.com>
# Date 1508810107 -7200
# Node ID 15cf4e532ae8bbec4cd6af8230747a3468e3fe08
# Parent  c7662d351bb08b002a02dbac5a6914e57198c774
Bug 1377677 - Remember expanded objects when switching panel in JSON Viewer r=Honza

MozReview-Commit-ID: AB9SUV2FY8s

diff --git a/devtools/client/jsonview/components/JsonPanel.js b/devtools/client/jsonview/components/JsonPanel.js
--- a/devtools/client/jsonview/components/JsonPanel.js
+++ b/devtools/client/jsonview/components/JsonPanel.js
@@ -3,29 +3,26 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 define(function (require, exports, module) {
   const { DOM: dom, createFactory, createClass, PropTypes } = require("devtools/client/shared/vendor/react");
-  const TreeViewClass = require("devtools/client/shared/components/tree/TreeView");
-  const TreeView = createFactory(TreeViewClass);
+  const TreeView = createFactory(require("devtools/client/shared/components/tree/TreeView"));
 
   const { REPS, MODE } = require("devtools/client/shared/components/reps/reps");
   const { createFactories } = require("devtools/client/shared/react-utils");
   const { Rep } = REPS;
 
   const { SearchBox } = createFactories(require("./SearchBox"));
   const { Toolbar, ToolbarButton } = createFactories(require("./reps/Toolbar"));
 
   const { div } = dom;
-  const AUTO_EXPAND_MAX_SIZE = 100 * 1024;
-  const AUTO_EXPAND_MAX_LEVEL = 7;
 
   function isObject(value) {
     return Object(value) === value;
   }
 
   /**
    * This template represents the 'JSON' panel. The panel is
    * responsible for rendering an expandable tree that allows simple
@@ -37,17 +34,17 @@ define(function (require, exports, modul
     propTypes: {
       data: PropTypes.oneOfType([
         PropTypes.string,
         PropTypes.array,
         PropTypes.object,
         PropTypes.bool,
         PropTypes.number
       ]),
-      jsonTextLength: PropTypes.number,
+      expandedNodes: PropTypes.instanceOf(Set),
       searchFilter: PropTypes.string,
       actions: PropTypes.object,
     },
 
     getInitialState: function () {
       return {};
     },
 
@@ -91,33 +88,24 @@ define(function (require, exports, modul
     renderTree: function () {
       // Append custom column for displaying values. This column
       // Take all available horizontal space.
       let columns = [{
         id: "value",
         width: "100%"
       }];
 
-      // Expand the document by default if its size isn't bigger than 100KB.
-      let expandedNodes = new Set();
-      if (this.props.jsonTextLength <= AUTO_EXPAND_MAX_SIZE) {
-        expandedNodes = TreeViewClass.getExpandedNodes(
-          this.props.data,
-          {maxLevel: AUTO_EXPAND_MAX_LEVEL}
-        );
-      }
-
       // Render tree component.
       return TreeView({
         object: this.props.data,
         mode: MODE.TINY,
         onFilter: this.onFilter,
         columns: columns,
         renderValue: this.renderValue,
-        expandedNodes: expandedNodes,
+        expandedNodes: this.props.expandedNodes,
       });
     },
 
     render: function () {
       let content;
       let data = this.props.data;
 
       if (!isObject(data)) {
diff --git a/devtools/client/jsonview/components/MainTabbedArea.js b/devtools/client/jsonview/components/MainTabbedArea.js
--- a/devtools/client/jsonview/components/MainTabbedArea.js
+++ b/devtools/client/jsonview/components/MainTabbedArea.js
@@ -29,17 +29,18 @@ define(function (require, exports, modul
       headers: PropTypes.object,
       searchFilter: PropTypes.string,
       json: PropTypes.oneOfType([
         PropTypes.string,
         PropTypes.object,
         PropTypes.array,
         PropTypes.bool,
         PropTypes.number
-      ])
+      ]),
+      expandedNodes: PropTypes.instanceOf(Set),
     },
 
     getInitialState: function () {
       return {
         json: {},
         headers: {},
         jsonText: this.props.jsonText,
         tabActive: this.props.tabActive
@@ -55,17 +56,17 @@ define(function (require, exports, modul
         Tabs({
           tabActive: this.state.tabActive,
           onAfterChange: this.onTabChanged},
           TabPanel({
             className: "json",
             title: JSONView.Locale.$STR("jsonViewer.tab.JSON")},
             JsonPanel({
               data: this.props.json,
-              jsonTextLength: this.props.jsonText.length,
+              expandedNodes: this.props.expandedNodes,
               actions: this.props.actions,
               searchFilter: this.state.searchFilter
             })
           ),
           TabPanel({
             className: "rawdata",
             title: JSONView.Locale.$STR("jsonViewer.tab.RawData")},
             TextPanel({
diff --git a/devtools/client/jsonview/json-viewer.js b/devtools/client/jsonview/json-viewer.js
--- a/devtools/client/jsonview/json-viewer.js
+++ b/devtools/client/jsonview/json-viewer.js
@@ -5,18 +5,21 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 define(function (require, exports, module) {
   const { render } = require("devtools/client/shared/vendor/react-dom");
   const { createFactories } = require("devtools/client/shared/react-utils");
   const { MainTabbedArea } = createFactories(require("./components/MainTabbedArea"));
+  const TreeViewClass = require("devtools/client/shared/components/tree/TreeView");
 
   const json = document.getElementById("json");
+  const AUTO_EXPAND_MAX_SIZE = 100 * 1024;
+  const AUTO_EXPAND_MAX_LEVEL = 7;
 
   let prettyURL;
 
   // Application state object.
   let input = {
     jsonText: json.textContent,
     jsonPretty: null,
     headers: JSONView.headers,
@@ -30,16 +33,26 @@ define(function (require, exports, modul
   }
 
   try {
     input.json = JSON.parse(input.jsonText);
   } catch (err) {
     input.json = err;
   }
 
+  // Expand the document by default if its size isn't bigger than 100KB.
+  if (!(input.json instanceof Error) && input.jsonText.length <= AUTO_EXPAND_MAX_SIZE) {
+    input.expandedNodes = TreeViewClass.getExpandedNodes(
+      input.json,
+      {maxLevel: AUTO_EXPAND_MAX_LEVEL}
+    );
+  } else {
+    input.expandedNodes = new Set();
+  }
+
   json.remove();
 
   /**
    * Application actions/commands. This list implements all commands
    * available for the JSON viewer.
    */
   input.actions = {
     onCopyJson: function () {
diff --git a/devtools/client/jsonview/test/browser_jsonview_valid_json.js b/devtools/client/jsonview/test/browser_jsonview_valid_json.js
--- a/devtools/client/jsonview/test/browser_jsonview_valid_json.js
+++ b/devtools/client/jsonview/test/browser_jsonview_valid_json.js
@@ -26,13 +26,18 @@ add_task(function* () {
 
   // Clicking the value does not collapse it (so that it can be selected and copied).
   yield clickJsonNode(".jsonPanelBox .treeTable .treeValueCell");
   is(yield countRows(), 3, "There must still be three rows");
 
   // Clicking the label collapses the auto-expanded node.
   yield clickJsonNode(".jsonPanelBox .treeTable .treeLabel");
   is(yield countRows(), 1, "There must be one row");
+
+  // Collapsed nodes are preserved when switching panels.
+  yield selectJsonViewContentTab("headers");
+  yield selectJsonViewContentTab("json");
+  is(yield countRows(), 1, "There must still be one row");
 });
 
 function countRows() {
   return getElementCount(".jsonPanelBox .treeTable .treeRow");
 }
