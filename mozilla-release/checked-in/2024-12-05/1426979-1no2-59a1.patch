# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1515610769 18000
# Node ID 1e1991807b395b83e2e1e7b1eb67ce00c479da2e
# Parent  ee9e7021bce252bd2157576e04bbb92a00e43be3
Bug 1426979 P1 Ignore about:blank and about:srcdoc channels when determining if a window ClientSource is controlled or not. r=asuth

diff --git a/dom/base/nsGlobalWindow.cpp b/dom/base/nsGlobalWindow.cpp
--- a/dom/base/nsGlobalWindow.cpp
+++ b/dom/base/nsGlobalWindow.cpp
@@ -3447,18 +3447,42 @@ nsGlobalWindow::InnerSetNewDocument(JSCo
 
 nsresult
 nsGlobalWindow::EnsureClientSource()
 {
   MOZ_DIAGNOSTIC_ASSERT(mDoc);
 
   bool newClientSource = false;
 
+  // Get the load info for the document if we performed a load.  Be careful
+  // not to look at about:blank or about:srcdoc loads, though. They will have
+  // a channel and loadinfo, but their loadinfo will never be controlled.  This
+  // would in turn inadvertantly trigger the logic below to clear the inherited
+  // controller.
+  nsCOMPtr<nsILoadInfo> loadInfo;
   nsCOMPtr<nsIChannel> channel = mDoc->GetChannel();
-  nsCOMPtr<nsILoadInfo> loadInfo = channel ? channel->GetLoadInfo() : nullptr;
+  if (channel) {
+    nsCOMPtr<nsIURI> uri;
+    Unused << channel->GetURI(getter_AddRefs(uri));
+
+    bool ignoreLoadInfo = false;
+
+    // Note, this is mostly copied from NS_IsAboutBlank().  Its duplicated
+    // here so we can efficiently check about:srcdoc as well.
+    bool isAbout = false;
+    if (NS_SUCCEEDED(uri->SchemeIs("about", &isAbout)) && isAbout) {
+      nsCString spec = uri->GetSpecOrDefault();
+      ignoreLoadInfo = spec.EqualsLiteral("about:blank") ||
+                       spec.EqualsLiteral("about:srcdoc");
+    }
+
+    if (!ignoreLoadInfo) {
+      loadInfo = channel->GetLoadInfo();
+    }
+  }
 
   // Take the initial client source from the docshell immediately.  Even if we
   // don't end up using it here we should consume it.
   UniquePtr<ClientSource> initialClientSource;
   nsIDocShell* docshell = GetDocShell();
   if (docshell) {
     initialClientSource = docshell->TakeInitialClientSource();
   }
