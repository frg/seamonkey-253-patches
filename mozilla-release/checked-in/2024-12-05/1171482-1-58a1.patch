# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1508337132 -7200
# Node ID 502b8b343daf93b437019b4e72b304f18271937e
# Parent  1755c4e37905f93eaf68fbe1b2682a8b61079de5
Bug 1171482 - add DAMP test for inspector mutations;r=ochameau

MozReview-Commit-ID: 7ItEe4O4jyB

diff --git a/testing/talos/talos/tests/devtools/addon/content/damp.html b/testing/talos/talos/tests/devtools/addon/content/damp.html
--- a/testing/talos/talos/tests/devtools/addon/content/damp.html
+++ b/testing/talos/talos/tests/devtools/addon/content/damp.html
@@ -26,16 +26,17 @@ var defaultConfig = {
     "complicated.styleeditor": true,
     "complicated.performance": true,
     "complicated.netmonitor": true,
     "complicated.saveAndReadHeapSnapshot": true,
 
     "console.bulklog": true,
     "console.streamlog": true,
     "console.objectexpand": true,
+    "inspector.mutations": true,
   }
 };
 
 var testsInfo = {
   "cold.inspector": "Measure first open toolbox on inspector panel",
 
   "simple.webconsole": "Measure open/close toolbox on webconsole panel against simple document",
   "simple.inspector": "Measure open/close toolbox on inspector panel against simple document",
@@ -51,16 +52,17 @@ var testsInfo = {
   "complicated.styleeditor": "Measure open/close toolbox on style editor panel against complicated document",
   "complicated.performance": "Measure open/close toolbox on performance panel against complicated document",
   "complicated.netmonitor": "Measure open/close toolbox on network monitor panel against complicated document",
   "complicated.saveAndReadHeapSnapshot": "Measure open/close toolbox on memory panel and save/read heap snapshot against complicated document",
 
   "console.bulklog": "Measure time for a bunch of sync console.log statements to appear",
   "console.streamlog": "Measure rAF on page during a stream of console.log statements",
   "console.objectexpand": "Measure time to expand a large object and close the console",
+  "inspector.mutations": "Measure the time to perform childList mutations when inspector is enabled",
 };
 
 function updateConfig() {
   config = {subtests: []};
   for (var test in defaultConfig.subtests) {
     if ($("subtest-" + test).checked) { // eslint-disable-line no-undef
       config.subtests.push(test);
     }
diff --git a/testing/talos/talos/tests/devtools/addon/content/damp.js b/testing/talos/talos/tests/devtools/addon/content/damp.js
--- a/testing/talos/talos/tests/devtools/addon/content/damp.js
+++ b/testing/talos/talos/tests/devtools/addon/content/damp.js
@@ -315,16 +315,73 @@ Damp.prototype = {
       name: "console.objectexpand",
       value: performance.now() - start,
     });
 
     yield this.closeToolboxAndLog("console.objectexpanded");
     yield this.testTeardown();
   }),
 
+  /**
+   * Measure the time necesssary to perform successive childList mutations in the content
+   * page and update the markup-view accordingly.
+   */
+  _inspectorMutationsTest: Task.async(function* () {
+    let tab = yield this.testSetup(SIMPLE_URL);
+    let messageManager = tab.linkedBrowser.messageManager;
+    let {toolbox} = yield this.openToolbox("inspector");
+    let inspector = toolbox.getPanel("inspector");
+
+    // Test with n=LIMIT mutations, with t=DELAY ms between each one.
+    const LIMIT = 100;
+    const DELAY = 5;
+
+    messageManager.loadFrameScript("data:,(" + encodeURIComponent(
+      `function () {
+        const LIMIT = ${LIMIT};
+        addMessageListener("start-mutations-test", function () {
+          let addElement = function(index) {
+            if (index == LIMIT) {
+              // LIMIT was reached, stop adding elements.
+              return;
+            }
+            let div = content.document.createElement("div");
+            content.document.body.appendChild(div);
+            content.setTimeout(() => addElement(index + 1), ${DELAY});
+          };
+          addElement(0);
+        });
+      }`
+    ) + ")()", false);
+
+    let start = performance.now();
+
+    yield new Promise(resolve => {
+      let childListMutationsCounter = 0;
+      inspector.on("markupmutation", (evt, mutations) => {
+        let childListMutations = mutations.filter(m => m.type === "childList");
+        childListMutationsCounter += childListMutations.length;
+        if (childListMutationsCounter === LIMIT) {
+          // Wait until we received exactly n=LIMIT mutations in the markup view.
+          resolve();
+        }
+      });
+
+      messageManager.sendAsyncMessage("start-mutations-test");
+    });
+
+    this._results.push({
+      name: "inspector.mutations",
+      value: performance.now() - start
+    });
+
+    yield this.closeToolbox(null);
+    yield this.testTeardown();
+  }),
+
   takeCensus(label) {
     let start = performance.now();
 
     this._snapshot.takeCensus({
       breakdown: {
         by: "coarseType",
         objects: {
           by: "objectClass",
@@ -679,16 +736,17 @@ Damp.prototype = {
     Object.assign(tests, this._getToolLoadingTests(COMPLICATED_URL, "complicated", {
       expectedMessages: 7,
       expectedSources: 14,
     }));
 
     tests["console.bulklog"] = this._consoleBulkLoggingTest;
     tests["console.streamlog"] = this._consoleStreamLoggingTest;
     tests["console.objectexpand"] = this._consoleObjectExpansionTest;
+    tests["inspector.mutations"] = this._inspectorMutationsTest;
 
     // Filter tests via `./mach --subtests filter` command line argument
     let filter = Services.prefs.getCharPref("talos.subtests", "");
     if (filter) {
       for (let name in tests) {
         if (!name.includes(filter)) {
           delete tests[name];
         }
