# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1513871427 18000
# Node ID 950d56123c676ddb5f1630aba0e1fb78451eba97
# Parent  19767a5982fba2eeb379391356c26d38b966d266
Bug 1401359 Disable SharedWorker if in windows that cannot access storage. r=baku

diff --git a/dom/workers/RuntimeService.cpp b/dom/workers/RuntimeService.cpp
--- a/dom/workers/RuntimeService.cpp
+++ b/dom/workers/RuntimeService.cpp
@@ -2450,16 +2450,41 @@ RuntimeService::CreateSharedWorker(const
                                    const nsAString& aName,
                                    SharedWorker** aSharedWorker)
 {
   AssertIsOnMainThread();
 
   nsCOMPtr<nsPIDOMWindowInner> window = do_QueryInterface(aGlobal.GetAsSupports());
   MOZ_ASSERT(window);
 
+  // If the window is blocked from accessing storage, do not allow it
+  // to connect to a SharedWorker.  This would potentially allow it
+  // to communicate with other windows that do have storage access.
+  // Allow private browsing, however, as we handle that isolation
+  // via the principal.
+  auto storageAllowed = nsContentUtils::StorageAllowedForWindow(window);
+  if (storageAllowed != nsContentUtils::StorageAccess::eAllow &&
+      storageAllowed != nsContentUtils::StorageAccess::ePrivateBrowsing) {
+    return NS_ERROR_DOM_SECURITY_ERR;
+  }
+
+  // Assert that the principal private browsing state matches the
+  // StorageAccess value.
+#ifdef  MOZ_DIAGNOSTIC_ASSERT_ENABLED
+  if (storageAllowed == nsContentUtils::StorageAccess::ePrivateBrowsing) {
+    nsCOMPtr<nsIDocument> doc = window->GetExtantDoc();
+    nsCOMPtr<nsIPrincipal> principal = doc ? doc->NodePrincipal() : nullptr;
+    uint32_t privateBrowsingId = 0;
+    if (principal) {
+      MOZ_ALWAYS_SUCCEEDS(principal->GetPrivateBrowsingId(&privateBrowsingId));
+    }
+    MOZ_DIAGNOSTIC_ASSERT(privateBrowsingId != 0);
+  }
+#endif // MOZ_DIAGNOSTIC_ASSERT_ENABLED
+
   JSContext* cx = aGlobal.Context();
 
   WorkerLoadInfo loadInfo;
   nsresult rv = WorkerPrivate::GetLoadInfo(cx, window, nullptr, aScriptURL,
                                            false,
                                            WorkerPrivate::OverrideLoadGroup,
                                            WorkerTypeShared, &loadInfo);
   NS_ENSURE_SUCCESS(rv, rv);
diff --git a/dom/workers/test/mochitest.ini b/dom/workers/test/mochitest.ini
--- a/dom/workers/test/mochitest.ini
+++ b/dom/workers/test/mochitest.ini
@@ -51,16 +51,17 @@ support-files =
   onLine_worker_head.js
   promise_worker.js
   recursion_worker.js
   recursiveOnerror_worker.js
   redirect_to_foreign.sjs
   rvals_worker.js
   sharedWorker_console.js
   sharedWorker_sharedWorker.js
+  sharedWorker_thirdparty_frame.html
   simpleThread_worker.js
   suspend_window.html
   suspend_worker.js
   terminate_worker.js
   test_csp.html^headers^
   test_csp.js
   referrer_worker.html
   threadErrors_worker1.js
@@ -167,16 +168,17 @@ support-files =
 [test_promise.html]
 [test_promise_resolved_with_string.html]
 [test_recursion.html]
 [test_recursiveOnerror.html]
 [test_resolveWorker.html]
 [test_resolveWorker-assignment.html]
 [test_rvals.html]
 [test_sharedWorker.html]
+[test_sharedWorker_thirdparty.html]
 [test_simpleThread.html]
 [test_suspend.html]
 [test_terminate.html]
 [test_threadErrors.html]
 [test_threadTimeouts.html]
 [test_throwingOnerror.html]
 [test_timeoutTracing.html]
 [test_transferable.html]
diff --git a/dom/workers/test/sharedWorker_thirdparty_frame.html b/dom/workers/test/sharedWorker_thirdparty_frame.html
new file mode 100644
--- /dev/null
+++ b/dom/workers/test/sharedWorker_thirdparty_frame.html
@@ -0,0 +1,16 @@
+<!DOCTYPE HTML>
+<script>
+  let params = new URLSearchParams(document.location.search.substring(1));
+  let name = params.get('name');
+  try {
+    let worker = new SharedWorker('sharedWorker_sharedWorker.js',
+                                  { name: name });
+    worker.port.addEventListener('message', evt => {
+      parent.postMessage( { name: name, result: 'allowed' }, '*');
+    }, { once: true });
+    worker.port.start();
+    worker.port.postMessage('ping');
+  } catch(e) {
+    parent.postMessage({ name: name, result: 'blocked' }, '*');
+  }
+</script>
diff --git a/dom/workers/test/test_sharedWorker_thirdparty.html b/dom/workers/test/test_sharedWorker_thirdparty.html
new file mode 100644
--- /dev/null
+++ b/dom/workers/test/test_sharedWorker_thirdparty.html
@@ -0,0 +1,60 @@
+<!--
+  Any copyright is dedicated to the Public Domain.
+  http://creativecommons.org/publicdomain/zero/1.0/
+-->
+<!DOCTYPE HTML>
+<html>
+<head>
+  <title>Test for SharedWorker in 3rd Party Iframes</title>
+  <script src="/tests/SimpleTest/SimpleTest.js"> </script>
+  <script src="/tests/SimpleTest/SpawnTask.js"> </script>
+  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css">
+</head>
+<body>
+  <p id="display"></p>
+  <div id="content" style="display: none"></div>
+  <pre id="test">
+  <script class="testbody">
+
+  function testThirdPartyFrame(name) {
+    return new Promise(resolve => {
+      let frame = document.createElement('iframe');
+      frame.src =
+        'http://example.org/tests/dom/workers/test/sharedWorker_thirdparty_frame.html?name=' + name;
+      document.body.appendChild(frame);
+      window.addEventListener('message', function messageListener(evt) {
+        if (evt.data.name !== name) {
+          return;
+        }
+        frame.remove();
+        window.removeEventListener('message', messageListener);
+        resolve(evt.data.result);
+      });
+    });
+  }
+
+  const COOKIE_BEHAVIOR_ACCEPT        = 0;
+  const COOKIE_BEHAVIOR_REJECTFOREIGN = 1;
+
+  add_task(async function allowed() {
+    await SpecialPowers.pushPrefEnv({ set: [
+      ["network.cookie.cookieBehavior", COOKIE_BEHAVIOR_ACCEPT]
+    ]});
+    let result = await testThirdPartyFrame('allowed');
+    ok(result === 'allowed',
+       'SharedWorker should be allowed when 3rd party iframes can access storage');
+  });
+
+  add_task(async function blocked() {
+    await SpecialPowers.pushPrefEnv({ set: [
+      ["network.cookie.cookieBehavior", COOKIE_BEHAVIOR_REJECTFOREIGN]
+    ]});
+    let result = await testThirdPartyFrame('blocked');
+    ok(result === 'blocked',
+       'SharedWorker should not be allowed when 3rd party iframes are denied storage');
+  });
+
+  </script>
+  </pre>
+</body>
+</html>
