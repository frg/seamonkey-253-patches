# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1515416705 -3600
# Node ID 676de682404300c568123e6790f7b0e9fdd2a87d
# Parent  db6ea1f39d6e03f3d26b772f4663c7d1973e0396
Bug 1419771 - Introduce DOMPrefs, a thread-safe access to preferences for DOM - part 17 - Network Information enabled, r=asuth

diff --git a/dom/base/DOMPrefs.cpp b/dom/base/DOMPrefs.cpp
--- a/dom/base/DOMPrefs.cpp
+++ b/dom/base/DOMPrefs.cpp
@@ -45,16 +45,17 @@ PREF(ServiceWorkersEnabled, "dom.service
 PREF(ServiceWorkersTestingEnabled, "dom.serviceWorkers.testing.enabled")
 PREF(StorageManagerEnabled, "dom.storageManager.enabled")
 PREF(PromiseRejectionEventsEnabled, "dom.promise_rejection_events.enabled")
 PREF(PushEnabled, "dom.push.enabled")
 PREF(StreamsEnabled, "dom.streams.enabled")
 PREF(RequestContextEnabled, "dom.requestcontext.enabled")
 PREF(OffscreenCanvasEnabled, "gfx.offscreencanvas.enabled")
 PREF(WebkitBlinkDirectoryPickerEnabled, "dom.webkitBlink.dirPicker.enabled")
+PREF(NetworkInformationEnabled, "dom.netinfo.enabled")
 
 #undef PREF
 
 #define PREF_WEBIDL(name)                        \
   /* static */ bool                              \
   DOMPrefs::name(JSContext* aCx, JSObject* aObj) \
   {                                              \
     return DOMPrefs::name();                     \
@@ -67,13 +68,14 @@ PREF_WEBIDL(NotificationRIEnabled)
 PREF_WEBIDL(ServiceWorkersEnabled)
 PREF_WEBIDL(StorageManagerEnabled)
 PREF_WEBIDL(PromiseRejectionEventsEnabled)
 PREF_WEBIDL(PushEnabled)
 PREF_WEBIDL(StreamsEnabled)
 PREF_WEBIDL(RequestContextEnabled)
 PREF_WEBIDL(OffscreenCanvasEnabled)
 PREF_WEBIDL(WebkitBlinkDirectoryPickerEnabled)
+PREF_WEBIDL(NetworkInformationEnabled)
 
 #undef PREF_WEBIDL
 
 } // dom namespace
 } // mozilla namespace
diff --git a/dom/base/DOMPrefs.h b/dom/base/DOMPrefs.h
--- a/dom/base/DOMPrefs.h
+++ b/dom/base/DOMPrefs.h
@@ -74,14 +74,18 @@ public:
 
   // Returns true if the gfx.offscreencanvas.enabled pref is set.
   static bool OffscreenCanvasEnabled();
   static bool OffscreenCanvasEnabled(JSContext* aCx, JSObject* aObj);
 
   // Returns true if the dom.webkitBlink.dirPicker.enabled pref is set.
   static bool WebkitBlinkDirectoryPickerEnabled();
   static bool WebkitBlinkDirectoryPickerEnabled(JSContext* aCx, JSObject* aObj);
+
+  // Returns true if the dom.netinfo.enabled pref is set.
+  static bool NetworkInformationEnabled();
+  static bool NetworkInformationEnabled(JSContext* aCx, JSObject* aObj);
 };
 
 } // dom namespace
 } // mozilla namespace
 
 #endif // mozilla_dom_DOMPrefs_h
diff --git a/dom/network/Connection.cpp b/dom/network/Connection.cpp
--- a/dom/network/Connection.cpp
+++ b/dom/network/Connection.cpp
@@ -99,28 +99,16 @@ Connection::Update(ConnectionType aType,
   mDHCPGateway = aDHCPGateway;
 
   if (aNotify && previousType != aType &&
       !nsContentUtils::ShouldResistFingerprinting()) {
     DispatchTrustedEvent(CHANGE_EVENT_NAME);
   }
 }
 
-/* static */ bool
-Connection::IsEnabled(JSContext* aCx, JSObject* aObj)
-{
-  if (NS_IsMainThread()) {
-    return Preferences::GetBool("dom.netinfo.enabled");
-  }
-
-  WorkerPrivate* workerPrivate = GetWorkerPrivateFromContext(aCx);
-  MOZ_ASSERT(workerPrivate);
-  return workerPrivate->NetworkInformationEnabled();
-}
-
 /* static */ Connection*
 Connection::CreateForWindow(nsPIDOMWindowInner* aWindow)
 {
   MOZ_ASSERT(aWindow);
   return new ConnectionMainThread(aWindow);
 }
 
 /* static */ already_AddRefed<Connection>
diff --git a/dom/network/Connection.h b/dom/network/Connection.h
--- a/dom/network/Connection.h
+++ b/dom/network/Connection.h
@@ -32,18 +32,16 @@ class Connection : public DOMEventTarget
 {
 public:
   NS_DECL_ISUPPORTS_INHERITED
   NS_DECL_NSINETWORKPROPERTIES
   NS_DECL_OWNINGTHREAD
 
   NS_REALLY_FORWARD_NSIDOMEVENTTARGET(DOMEventTargetHelper)
 
-  static bool IsEnabled(JSContext* aCx, JSObject* aObj);
-
   static Connection*
   CreateForWindow(nsPIDOMWindowInner* aWindow);
 
   static already_AddRefed<Connection>
   CreateForWorker(workers::WorkerPrivate* aWorkerPrivate,
                   ErrorResult& aRv);
 
   void Shutdown();
diff --git a/dom/webidl/NetworkInformation.webidl b/dom/webidl/NetworkInformation.webidl
--- a/dom/webidl/NetworkInformation.webidl
+++ b/dom/webidl/NetworkInformation.webidl
@@ -13,14 +13,14 @@ enum ConnectionType {
     "bluetooth",
     "ethernet",
     "wifi",
     "other",
     "none",
     "unknown"
 };
 
-[Func="mozilla::dom::network::Connection::IsEnabled",
+[Func="mozilla::dom::DOMPrefs::NetworkInformationEnabled",
  Exposed=(Window,Worker)]
 interface NetworkInformation : EventTarget {
     readonly    attribute ConnectionType type;
                 attribute EventHandler   ontypechange;
 };
diff --git a/dom/webidl/WorkerNavigator.webidl b/dom/webidl/WorkerNavigator.webidl
--- a/dom/webidl/WorkerNavigator.webidl
+++ b/dom/webidl/WorkerNavigator.webidl
@@ -11,11 +11,11 @@ WorkerNavigator implements NavigatorID;
 WorkerNavigator implements NavigatorLanguage;
 WorkerNavigator implements NavigatorOnLine;
 WorkerNavigator implements NavigatorConcurrentHardware;
 WorkerNavigator implements NavigatorStorage;
 
 // http://wicg.github.io/netinfo/#extensions-to-the-navigator-interface
 [Exposed=(Worker)]
 partial interface WorkerNavigator {
-    [Func="mozilla::dom::network::Connection::IsEnabled", Throws]
+    [Func="mozilla::dom::DOMPrefs::NetworkInformationEnabled", Throws]
     readonly attribute NetworkInformation connection;
 };
diff --git a/dom/workers/WorkerPrefs.h b/dom/workers/WorkerPrefs.h
--- a/dom/workers/WorkerPrefs.h
+++ b/dom/workers/WorkerPrefs.h
@@ -16,17 +16,16 @@
 //     macro in Workers.h.
 //   * The name of the function that updates the new value of a pref.
 //
 //   WORKER_PREF("foo.bar", UpdaterFunction)
 //
 //   * First argument is the name of the pref.
 //   * The name of the function that updates the new value of a pref.
 
-WORKER_SIMPLE_PREF("dom.netinfo.enabled", NetworkInformationEnabled, NETWORKINFORMATION_ENABLED)
 WORKER_SIMPLE_PREF("dom.fetchObserver.enabled", FetchObserverEnabled, FETCHOBSERVER_ENABLED)
 WORKER_SIMPLE_PREF("privacy.resistFingerprinting", ResistFingerprintingEnabled, RESISTFINGERPRINTING_ENABLED)
 WORKER_SIMPLE_PREF("devtools.enabled", DevToolsEnabled, DEVTOOLS_ENABLED)
 WORKER_PREF("intl.accept_languages", PrefLanguagesChanged)
 WORKER_PREF("general.appname.override", AppNameOverrideChanged)
 WORKER_PREF("general.appversion.override", AppVersionOverrideChanged)
 WORKER_PREF("general.platform.override", PlatformOverrideChanged)
 #ifdef JS_GC_ZEAL
