# HG changeset patch
# User Daisuke Akatsuka <dakatsuka@mozilla.com>
# Date 1512524496 -32400
# Node ID 0b61da1ea3ab0825fea7c765231dbc94e9926964
# Parent  07e2805147ed57fb23706b09e882c5bd53302821
Bug 1422218 - Part 1: Get segments of property graph at keyframe offset explicit. r=pbro

MozReview-Commit-ID: 9PIngKGGRhX

diff --git a/devtools/client/animationinspector/components/keyframes.js b/devtools/client/animationinspector/components/keyframes.js
--- a/devtools/client/animationinspector/components/keyframes.js
+++ b/devtools/client/animationinspector/components/keyframes.js
@@ -3,17 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const {createNode, createSVGNode} =
   require("devtools/client/animationinspector/utils");
-const {ProgressGraphHelper, getPreferredKeyframesProgressThreshold} =
+const { ProgressGraphHelper, } =
   require("devtools/client/animationinspector/graph-helper.js");
 
 // Counter for linearGradient ID.
 let LINEAR_GRADIENT_ID_COUNTER = 0;
 
 /**
  * UI component responsible for displaying a list of keyframes.
  * Also, shows a graphical graph for the animation progress of one iteration.
@@ -59,18 +59,17 @@ Keyframes.prototype = {
     const minSegmentDuration =
       totalDuration / this.containerEl.clientWidth;
 
     // Create graph helper to render the animation property graph.
     const win = this.containerEl.ownerGlobal;
     const graphHelper =
       new ProgressGraphHelper(win, propertyName, animationType, keyframes, totalDuration);
 
-    renderPropertyGraph(graphEl, totalDuration, minSegmentDuration,
-                        getPreferredKeyframesProgressThreshold(keyframes), graphHelper);
+    renderPropertyGraph(graphEl, totalDuration, minSegmentDuration, graphHelper);
 
     // Destroy ProgressGraphHelper resources.
     graphHelper.destroy();
 
     // Set viewBox which includes invisible stroke width.
     // At first, calculate invisible stroke width from maximum width.
     // The reason why divide by 2 is that half of stroke width will be invisible
     // if we use 0 or 1 for y axis.
@@ -100,23 +99,20 @@ Keyframes.prototype = {
   }
 };
 
 /**
  * Render a graph representing the progress of the animation over one iteration.
  * @param {Element} parentEl - Parent element of this appended path element.
  * @param {Number} duration - Duration of one iteration.
  * @param {Number} minSegmentDuration - Minimum segment duration.
- * @param {Number} minProgressThreshold - Minimum progress threshold.
  * @param {ProgressGraphHelper} graphHelper - The object of ProgressGraphHalper.
  */
-function renderPropertyGraph(parentEl, duration, minSegmentDuration,
-                             minProgressThreshold, graphHelper) {
-  const segments = graphHelper.createPathSegments(0, duration, minSegmentDuration,
-                                                  minProgressThreshold);
+function renderPropertyGraph(parentEl, duration, minSegmentDuration, graphHelper) {
+  const segments = graphHelper.createPathSegments(duration, minSegmentDuration);
 
   const graphType = graphHelper.getGraphType();
   if (graphType !== "color") {
     graphHelper.appendShapePath(parentEl, segments, graphType);
     renderEasingHint(parentEl, segments, graphHelper);
     return;
   }
 
@@ -163,42 +159,27 @@ function renderPropertyGraph(parentEl, d
  */
 function renderEasingHint(parentEl, segments, helper) {
   const keyframes = helper.getKeyframes();
   const duration = helper.getDuration();
 
   // Split segments for each keyframe.
   for (let i = 0, indexOfSegments = 0; i < keyframes.length - 1; i++) {
     const startKeyframe = keyframes[i];
-    const startTime = startKeyframe.offset * duration;
     const endKeyframe = keyframes[i + 1];
     const endTime = endKeyframe.offset * duration;
 
     const keyframeSegments = [];
     for (; indexOfSegments < segments.length; indexOfSegments++) {
       const segment = segments[indexOfSegments];
-      if (segment.x < startTime) {
-        // If previous easings were linear, we need to increment the indexOfSegments.
-        continue;
-      }
-      if (segment.x > endTime) {
-        indexOfSegments -= 1;
+      keyframeSegments.push(segment);
+
+      if (segment.x === endTime) {
         break;
       }
-      keyframeSegments.push(segment);
-    }
-
-    // If keyframeSegments does not have segment which is at startTime,
-    // get and set the segment.
-    if (keyframeSegments[0].x !== startTime) {
-      keyframeSegments.unshift(helper.getSegment(startTime));
-    }
-    // Also, endTime.
-    if (keyframeSegments[keyframeSegments.length - 1].x !== endTime) {
-      keyframeSegments.push(helper.getSegment(endTime));
     }
 
     // Append easing hint as text and emphasis path.
     const gEl = createSVGNode({
       parent: parentEl,
       nodeType: "g"
     });
     createSVGNode({
diff --git a/devtools/client/animationinspector/graph-helper.js b/devtools/client/animationinspector/graph-helper.js
--- a/devtools/client/animationinspector/graph-helper.js
+++ b/devtools/client/animationinspector/graph-helper.js
@@ -243,29 +243,48 @@ ProgressGraphHelper.prototype = {
 
     return value => {
       return discreteValues.indexOf(value) / (discreteValues.length - 1);
     };
   },
 
   /**
    * Create the path segments from given parameters.
-   * @param {Number} startTime - Starting time of animation.
-   * @param {Number} endTime - Ending time of animation.
+   *
+   * @param {Number} duration - Duration of animation.
    * @param {Number} minSegmentDuration - Minimum segment duration.
    * @param {Number} minProgressThreshold - Minimum progress threshold.
    * @return {Array} path segments -
    *                 [{x: {Number} time, y: {Number} progress}, ...]
    */
-  createPathSegments: function (startTime, endTime,
-                                minSegmentDuration, minProgressThreshold) {
-    return !this.valueHelperFunction
-           ? createKeyframesPathSegments(endTime - startTime, this.devtoolsKeyframes)
-           : createPathSegments(startTime, endTime,
-                                minSegmentDuration, minProgressThreshold, this);
+  createPathSegments: function (duration, minSegmentDuration, minProgressThreshold) {
+    if (!this.valueHelperFunction) {
+      return createKeyframesPathSegments(duration, this.devtoolsKeyframes);
+    }
+
+    const segments = [];
+
+    for (let i = 0; i < this.devtoolsKeyframes.length - 1; i++) {
+      const startKeyframe = this.devtoolsKeyframes[i];
+      const endKeyframe = this.devtoolsKeyframes[i + 1];
+
+      let threshold = getPreferredProgressThreshold(startKeyframe.easing);
+      if (threshold !== DEFAULT_MIN_PROGRESS_THRESHOLD) {
+        // We should consider the keyframe's duration.
+        threshold *= (endKeyframe.offset - startKeyframe.offset);
+      }
+
+      const startTime = startKeyframe.offset * duration;
+      const endTime = endKeyframe.offset * duration;
+
+      segments.push(...createPathSegments(startTime, endTime,
+                                          minSegmentDuration, threshold, this));
+    }
+
+    return segments;
   },
 
   /**
    * Append path element as shape. Also, this method appends two segment
    * that are {start x, 0} and {end x, 0} to make shape.
    * @param {Element} parentEl - Parent element of this appended path element.
    * @param {Array} pathSegments - Path segments. Please see createPathSegments.
    * @param {String} cls - Class name.
