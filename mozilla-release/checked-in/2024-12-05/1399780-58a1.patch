# HG changeset patch
# User Thomas Nguyen <tnguyen@mozilla.com>
# Date 1508121693 -28800
# Node ID 5fdebd6d25a567eec64b5c0e54e6b3eb667ca9b1
# Parent  5283b44ff51e356a78cf4819d8f90c398c82c8dd
Bug 1399780 - Add a test that referrerpolicy attributes are honoured correctly in speculative loading r=ckerschb

MozReview-Commit-ID: 6wU7RMEghjx

diff --git a/dom/base/test/file_bug704320_preload_attr.html b/dom/base/test/file_bug704320_preload_attr.html
new file mode 100644
--- /dev/null
+++ b/dom/base/test/file_bug704320_preload_attr.html
@@ -0,0 +1,27 @@
+<!DOCTYPE HTML>
+<html>
+<!--
+Test whether the speculative parser should use the referrerpolicy attribute
+https://bugzilla.mozilla.org/show_bug.cgi?id=1399780
+-->
+<head>
+  <meta charset="utf-8">
+  <script type="text/javascript" src="file_bug704320_preload_common.js"></script>
+  <script language="javascript" type="text/javascript">
+    // interfere doc.write(meta referrer) to the down side preloads
+    document.write("<meta name='referrer' content='unsafe-url'>");
+  </script>
+
+  <link rel="stylesheet"
+        href="http://example.com/tests/dom/base/test/bug704320_counter.sjs?type=css"
+        onload="incrementLoad2('link', 2);"
+        referrerpolicy="origin"/>
+
+  <img src="http://example.com/tests/dom/base/test/bug704320_counter.sjs?type=img"
+       onload="incrementLoad2('img', 2);"
+       referrerpolicy="origin"/>
+
+</head>
+<body>
+</body>
+</html>
diff --git a/dom/base/test/mochitest.ini b/dom/base/test/mochitest.ini
--- a/dom/base/test/mochitest.ini
+++ b/dom/base/test/mochitest.ini
@@ -103,16 +103,17 @@ support-files =
   file_bug687859-16.js^headers^
   file_bug687859-bom.js
   file_bug687859-bom.js^headers^
   file_bug687859-charset.js
   file_bug687859-http.js
   file_bug687859-http.js^headers^
   file_bug687859-inherit.js
   file_bug692434.xml
+  file_bug704320_preload_attr.html
   file_bug704320_preload_common.js
   file_bug704320_preload_reuse.html
   file_bug704320_preload_noreuse.html
   file_bug704320_redirect.html
   file_bug707142_baseline.json
   file_bug707142_bom.json
   file_bug707142_utf-16.json
   file_bug708620-2.html
diff --git a/dom/base/test/test_bug704320_preload.html b/dom/base/test/test_bug704320_preload.html
--- a/dom/base/test/test_bug704320_preload.html
+++ b/dom/base/test/test_bug704320_preload.html
@@ -42,16 +42,27 @@ var tests = (function*() {
   // load the second test frame
   // it will call back into this function via postMessage when it finishes loading.
   // and continue beyond the yield.
   yield iframe.src = 'file_bug704320_preload_reuse.html';
 
   // check the second test
   yield checkResults(finalizePreloadReuse);
 
+  // reset the counter
+  yield resetCounter();
+
+  // load the third test frame
+  // it will call back into this function via postMessage when it finishes loading.
+  // and continue beyond the yield.
+  yield iframe.src = 'file_bug704320_preload_attr.html';
+
+  // check the third test
+  yield checkResults(finalizePreloadReferrerPolicyAttr);
+
   // complete.
   SimpleTest.finish();
 })();
 
 // Helper functions below.
 
 /**
  * This checks the first test: a test where the preloads should not
@@ -116,16 +127,44 @@ function finalizePreloadReuse(results) {
        "No loads for " + x + " should have a missing referrer.");
     is(results[x].referrers.indexOf('full'), -1,
        "No loads for " + x + " should have an 'full' referrer.")
   }
 
   advance();
 }
 
+/**
+ * This checks the third test: a test where preload requests of image, style
+ * should use referrerpolicy attribute and we expect the preloads should not
+ * be reused
+ */
+function finalizePreloadReferrerPolicyAttr(results) {
+  var expected = {'css': {'count': 1, 'referrers': ['origin']},
+                  'img': {'count': 1, 'referrers': ['origin']}};
+
+  for (var x in expected) {
+    ok(x in results, "some " + x + " loads required in results object.");
+
+    is(results[x].count, expected[x].count,
+       "Expected " + expected[x].count + " loads for " + x + " requests.");
+
+    // 'origin' is required
+    ok(results[x].referrers.indexOf('origin') >= 0,
+       "One load for " + x + " should have had 'origin' referrer.");
+
+    // no other values should be in the referrers.
+    is(results[x].referrers.indexOf('none'), -1,
+       "No loads for " + x + " should have a missing referrer.");
+    is(results[x].referrers.indexOf('full'), -1,
+       "No loads for " + x + " should have an 'full' referrer.")
+  }
+
+  advance();
+}
 
 /**
  * Grabs the results via XHR and passes to checker.
  */
 function checkResults(checker) {
   doXHR('/tests/dom/base/test/bug704320_counter.sjs?results',
         function(xhr) {
           checker(JSON.parse(xhr.responseText));

