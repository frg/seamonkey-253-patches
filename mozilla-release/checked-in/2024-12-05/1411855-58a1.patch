# HG changeset patch
# User Fred Lin <gasolin@gmail.com>
# Date 1509501918 -28800
# Node ID aadc92d4e79b299590726bc76b28928a4894b8e9
# Parent  63f3e51fcee8ae0bf4d78c1150d92866165bd016
Bug 1411855 - remove unused and no inline function in toolbar;r=rickychien

MozReview-Commit-ID: HXiTJSskRPr

diff --git a/devtools/client/netmonitor/src/components/Toolbar.js b/devtools/client/netmonitor/src/components/Toolbar.js
--- a/devtools/client/netmonitor/src/components/Toolbar.js
+++ b/devtools/client/netmonitor/src/components/Toolbar.js
@@ -7,20 +7,20 @@
 const Services = require("Services");
 const {
   Component,
   createFactory,
   DOM,
   PropTypes,
 } = require("devtools/client/shared/vendor/react");
 const { connect } = require("devtools/client/shared/vendor/react-redux");
+
 const Actions = require("../actions/index");
 const { FILTER_SEARCH_DELAY, FILTER_TAGS } = require("../constants");
 const {
-  getDisplayedRequestsSummary,
   getRecordingState,
   getRequestFilterTypes,
   getTypeFilteredRequests,
   isNetworkDetailsToggleButtonDisabled,
 } = require("../selectors/index");
 
 const { autocompleteProvider } = require("../utils/filter-autocomplete-provider");
 const { L10N } = require("../utils/l10n");
@@ -75,16 +75,17 @@ class Toolbar extends Component {
       browserCacheDisabled: PropTypes.bool.isRequired,
       toggleRequestFilterType: PropTypes.func.isRequired,
       filteredRequests: PropTypes.object.isRequired,
     };
   }
 
   constructor(props) {
     super(props);
+    this.autocompleteProvider = this.autocompleteProvider.bind(this);
     this.toggleRequestFilterType = this.toggleRequestFilterType.bind(this);
     this.updatePersistentLogsEnabled = this.updatePersistentLogsEnabled.bind(this);
     this.updateBrowserCacheDisabled = this.updateBrowserCacheDisabled.bind(this);
   }
 
   componentDidMount() {
     Services.prefs.addObserver(DEVTOOLS_ENABLE_PERSISTENT_LOG_PREF,
                                this.updatePersistentLogsEnabled);
@@ -111,42 +112,36 @@ class Toolbar extends Component {
       Services.prefs.getBoolPref(DEVTOOLS_ENABLE_PERSISTENT_LOG_PREF));
   }
 
   updateBrowserCacheDisabled() {
     this.props.disableBrowserCache(
       Services.prefs.getBoolPref(DEVTOOLS_DISABLE_CACHE_PREF));
   }
 
+  autocompleteProvider(filter) {
+    return autocompleteProvider(filter, this.props.filteredRequests);
+  }
+
   render() {
     let {
       toggleRecording,
       clearRequests,
       requestFilterTypes,
       setRequestFilterText,
       networkDetailsToggleDisabled,
       networkDetailsOpen,
       toggleNetworkDetails,
       togglePersistentLogs,
       persistentLogsEnabled,
       toggleBrowserCache,
       browserCacheDisabled,
-      filteredRequests,
       recording,
     } = this.props;
 
-    let toggleButtonClassName = [
-      "network-details-panel-toggle",
-      "devtools-button",
-    ];
-
-    if (!networkDetailsOpen) {
-      toggleButtonClassName.push("pane-collapsed");
-    }
-
     // Render list of filter-buttons.
     let buttons = requestFilterTypes.map(([type, checked]) => {
       let classList = ["devtools-button", `requests-list-filter-${type}-button`];
       checked && classList.push("checked");
 
       return (
         button({
           className: classList.join(" "),
@@ -158,28 +153,41 @@ class Toolbar extends Component {
         },
           TOOLBAR_FILTER_LABELS[type]
         )
       );
     });
 
     // Calculate class-list for toggle recording button. The button
     // has two states: pause/play.
-    let toggleButtonClassList = [
+    let toggleRecordingButtonClass = [
       "devtools-button",
       "requests-list-pause-button",
       recording ? "devtools-pause-icon" : "devtools-play-icon",
+    ].join(" ");
+
+    // Detail toggle button
+    let toggleDetailButtonClassList = [
+      "network-details-panel-toggle",
+      "devtools-button",
     ];
 
+    if (!networkDetailsOpen) {
+      toggleDetailButtonClassList.push("pane-collapsed");
+    }
+    let toggleDetailButtonClass = toggleDetailButtonClassList.join(" ");
+    let toggleDetailButtonTitle = networkDetailsOpen ? COLLAPSE_DETAILS_PANE :
+      EXPAND_DETAILS_PANE;
+
     // Render the entire toolbar.
     return (
       span({ className: "devtools-toolbar devtools-toolbar-container" },
         span({ className: "devtools-toolbar-group" },
           button({
-            className: toggleButtonClassList.join(" "),
+            className: toggleRecordingButtonClass,
             title: TOOLBAR_TOGGLE_RECORDING,
             onClick: toggleRecording,
           }),
           button({
             className: "devtools-button devtools-clear-icon requests-list-clear-button",
             title: TOOLBAR_CLEAR,
             onClick: clearRequests,
           }),
@@ -215,42 +223,40 @@ class Toolbar extends Component {
         ),
         span({ className: "devtools-toolbar-group" },
           SearchBox({
             delay: FILTER_SEARCH_DELAY,
             keyShortcut: SEARCH_KEY_SHORTCUT,
             placeholder: SEARCH_PLACE_HOLDER,
             type: "filter",
             onChange: setRequestFilterText,
-            autocompleteProvider: filter =>
-              autocompleteProvider(filter, filteredRequests),
+            autocompleteProvider: this.autocompleteProvider,
           }),
           button({
-            className: toggleButtonClassName.join(" "),
-            title: networkDetailsOpen ? COLLAPSE_DETAILS_PANE : EXPAND_DETAILS_PANE,
+            className: toggleDetailButtonClass,
+            title: toggleDetailButtonTitle,
             disabled: networkDetailsToggleDisabled,
             tabIndex: "0",
             onClick: toggleNetworkDetails,
           }),
         )
       )
     );
   }
 }
 
 module.exports = connect(
   (state) => ({
+    browserCacheDisabled: state.ui.browserCacheDisabled,
+    filteredRequests: getTypeFilteredRequests(state),
     networkDetailsToggleDisabled: isNetworkDetailsToggleButtonDisabled(state),
     networkDetailsOpen: state.ui.networkDetailsOpen,
     persistentLogsEnabled: state.ui.persistentLogsEnabled,
-    browserCacheDisabled: state.ui.browserCacheDisabled,
     recording: getRecordingState(state),
     requestFilterTypes: getRequestFilterTypes(state),
-    filteredRequests: getTypeFilteredRequests(state),
-    summary: getDisplayedRequestsSummary(state),
   }),
   (dispatch) => ({
     clearRequests: () => dispatch(Actions.clearRequests()),
     disableBrowserCache: (disabled) => dispatch(Actions.disableBrowserCache(disabled)),
     enablePersistentLogs: (enabled) => dispatch(Actions.enablePersistentLogs(enabled)),
     setRequestFilterText: (text) => dispatch(Actions.setRequestFilterText(text)),
     toggleBrowserCache: () => dispatch(Actions.toggleBrowserCache()),
     toggleNetworkDetails: () => dispatch(Actions.toggleNetworkDetails()),
