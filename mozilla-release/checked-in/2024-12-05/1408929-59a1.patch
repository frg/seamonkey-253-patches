# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1510917284 -3600
# Node ID 02a2365fd62b825721b3e4161e79f8a19df55b55
# Parent  215fcc76ed23ca7d5353af6abd41604f841f1d4a
Bug 1408929 - Rename and enable browser_webconsole_completion.js in the new console frontend; r=jdescottes.

MozReview-Commit-ID: ICu5aJViiW3

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -204,16 +204,17 @@ skip-if = true #       Bug 1403188
 [browser_jsterm_autocomplete_helpers.js]
 [browser_jsterm_autocomplete_in_chrome_tab.js]
 [browser_jsterm_autocomplete_in_debugger_stackframe.js]
 [browser_jsterm_autocomplete_inside_text.js]
 [browser_jsterm_autocomplete_nav_and_tab_key.js]
 [browser_jsterm_autocomplete_return_key_no_selection.js]
 [browser_jsterm_autocomplete_return_key.js]
 [browser_jsterm_autocomplete-properties-with-non-alphanumeric-names.js]
+[browser_jsterm_completion.js]
 [browser_jsterm_copy_command.js]
 [browser_jsterm_dollar.js]
 [browser_jsterm_history_persist.js]
 [browser_jsterm_inspect.js]
 [browser_jsterm_no_autocompletion_on_defined_variables.js]
 [browser_jsterm_no_input_and_tab_key_pressed.js]
 [browser_jsterm_no_input_change_and_tab_key_pressed.js]
 [browser_jsterm_popup_close_on_tab_switch.js]
@@ -230,18 +231,16 @@ tags = mcb
 [browser_webconsole_cd_iframe.js]
 [browser_webconsole_certificate_messages.js]
 [browser_webconsole_click_function_to_source.js]
 skip-if = true #       Bug 1406038
 [browser_webconsole_clickable_urls.js]
 [browser_webconsole_closing_after_completion.js]
 [browser_webconsole_closure_inspection.js]
 skip-if = true #       Bug 1405250
-[browser_webconsole_completion.js]
-skip-if = true # Bug 1408929
 [browser_webconsole_console_api_iframe.js]
 skip-if = true # Bug 1408930
 [browser_webconsole_console_dir.js]
 [browser_webconsole_console_dir_uninspectable.js]
 [browser_webconsole_console_group.js]
 [browser_webconsole_console_logging_workers_api.js]
 skip-if = true # Bug 1405252
 [browser_webconsole_console_table.js]
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_completion.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_jsterm_completion.js
rename from devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_completion.js
rename to devtools/client/webconsole/new-console-output/test/mochitest/browser_jsterm_completion.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_completion.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_jsterm_completion.js
@@ -4,103 +4,69 @@
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
 // Tests that code completion works properly.
 
 "use strict";
 
 const TEST_URI = "data:text/html;charset=utf8,<p>test code completion";
 
-var jsterm;
-
-add_task(function* () {
-  yield loadTab(TEST_URI);
-
-  let hud = yield openConsole();
-
-  jsterm = hud.jsterm;
+add_task(async function () {
+  let {jsterm} = await openNewTabAndConsole(TEST_URI);
   let input = jsterm.inputNode;
 
   // Test typing 'docu'.
-  input.value = "docu";
-  input.setSelectionRange(4, 4);
-  yield complete(jsterm.COMPLETE_HINT_ONLY);
-
+  await jstermSetValueAndComplete(jsterm, "docu");
   is(input.value, "docu", "'docu' completion (input.value)");
   is(jsterm.completeNode.value, "    ment", "'docu' completion (completeNode)");
 
   // Test typing 'docu' and press tab.
-  input.value = "docu";
-  input.setSelectionRange(4, 4);
-  yield complete(jsterm.COMPLETE_FORWARD);
-
+  await jstermSetValueAndComplete(jsterm, "docu", undefined, jsterm.COMPLETE_FORWARD);
   is(input.value, "document", "'docu' tab completion");
   is(input.selectionStart, 8, "start selection is alright");
   is(input.selectionEnd, 8, "end selection is alright");
   is(jsterm.completeNode.value.replace(/ /g, ""), "", "'docu' completed");
 
   // Test typing 'window.Ob' and press tab.  Just 'window.O' is
   // ambiguous: could be window.Object, window.Option, etc.
-  input.value = "window.Ob";
-  input.setSelectionRange(9, 9);
-  yield complete(jsterm.COMPLETE_FORWARD);
-
+  await jstermSetValueAndComplete(jsterm, "window.Ob", undefined, jsterm.COMPLETE_FORWARD);
   is(input.value, "window.Object", "'window.Ob' tab completion");
 
   // Test typing 'document.getElem'.
-  input.value = "document.getElem";
-  input.setSelectionRange(16, 16);
-  yield complete(jsterm.COMPLETE_FORWARD);
-
+  await jstermSetValueAndComplete(
+    jsterm, "document.getElem", undefined, jsterm.COMPLETE_FORWARD);
   is(input.value, "document.getElem", "'document.getElem' completion");
   is(jsterm.completeNode.value, "                entsByTagNameNS",
      "'document.getElem' completion");
 
   // Test pressing tab another time.
-  yield jsterm.complete(jsterm.COMPLETE_FORWARD);
-
+  await jsterm.complete(jsterm.COMPLETE_FORWARD);
   is(input.value, "document.getElem", "'document.getElem' completion");
   is(jsterm.completeNode.value, "                entsByTagName",
      "'document.getElem' another tab completion");
 
   // Test pressing shift_tab.
-  complete(jsterm.COMPLETE_BACKWARD);
-
+  await jstermComplete(jsterm, jsterm.COMPLETE_BACKWARD);
   is(input.value, "document.getElem", "'document.getElem' untab completion");
   is(jsterm.completeNode.value, "                entsByTagNameNS",
      "'document.getElem' completion");
 
   jsterm.clearOutput();
 
-  input.value = "docu";
-  yield complete(jsterm.COMPLETE_HINT_ONLY);
+  await jstermSetValueAndComplete(jsterm, "docu");
+  is(jsterm.completeNode.value, "    ment", "'docu' completion");
 
-  is(jsterm.completeNode.value, "    ment", "'docu' completion");
-  yield jsterm.execute();
+  await jsterm.execute();
   is(jsterm.completeNode.value, "", "clear completion on execute()");
 
   // Test multi-line completion works
-  input.value = "console.log('one');\nconsol";
-  yield complete(jsterm.COMPLETE_HINT_ONLY);
-
+  await jstermSetValueAndComplete(jsterm, "console.log('one');\nconsol");
   is(jsterm.completeNode.value, "                   \n      e",
      "multi-line completion");
 
   // Test non-object autocompletion.
-  input.value = "Object.name.sl";
-  yield complete(jsterm.COMPLETE_HINT_ONLY);
-
+  await jstermSetValueAndComplete(jsterm, "Object.name.sl");
   is(jsterm.completeNode.value, "              ice", "non-object completion");
 
   // Test string literal autocompletion.
-  input.value = "'Asimov'.sl";
-  yield complete(jsterm.COMPLETE_HINT_ONLY);
-
+  await jstermSetValueAndComplete(jsterm, "'Asimov'.sl");
   is(jsterm.completeNode.value, "           ice", "string literal completion");
-
-  jsterm = null;
 });
-
-function complete(type) {
-  let updated = jsterm.once("autocomplete-updated");
-  jsterm.complete(type);
-  return updated;
-}
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/head.js b/devtools/client/webconsole/new-console-output/test/mochitest/head.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/head.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/head.js
@@ -294,26 +294,50 @@ function hasFocus(node) {
 /**
  * Set the value of the JsTerm and its caret position, and fire a completion request.
  *
  * @param {JsTerm} jsterm
  * @param {String} value : The value to set the jsterm to.
  * @param {Integer} caretIndexOffset : A number that will be added to value.length
  *                  when setting the caret. A negative number will place the caret
  *                  in (end - offset) position. Default to 0 (caret set at the end)
+ * @param {Integer} completionType : One of the following jsterm property
+ *                   - COMPLETE_FORWARD
+ *                   - COMPLETE_BACKWARD
+ *                   - COMPLETE_HINT_ONLY
+ *                   - COMPLETE_PAGEUP
+ *                   - COMPLETE_PAGEDOWN
+ *                  Will default to COMPLETE_HINT_ONLY.
  * @returns {Promise} resolves when the jsterm is completed.
  */
-function jstermSetValueAndComplete(jsterm, value, caretIndexOffset = 0) {
+function jstermSetValueAndComplete(jsterm, value, caretIndexOffset = 0, completionType) {
   const {inputNode} = jsterm;
   inputNode.value = value;
   let index = value.length + caretIndexOffset;
   inputNode.setSelectionRange(index, index);
 
+  return jstermComplete(jsterm, completionType);
+}
+
+/**
+ * Fires a completion request on the jsterm with the specified completionType
+ *
+ * @param {JsTerm} jsterm
+ * @param {Integer} completionType : One of the following jsterm property
+ *                   - COMPLETE_FORWARD
+ *                   - COMPLETE_BACKWARD
+ *                   - COMPLETE_HINT_ONLY
+ *                   - COMPLETE_PAGEUP
+ *                   - COMPLETE_PAGEDOWN
+ *                  Will default to COMPLETE_HINT_ONLY.
+ * @returns {Promise} resolves when the jsterm is completed.
+ */
+function jstermComplete(jsterm, completionType = jsterm.COMPLETE_HINT_ONLY) {
   const updated = jsterm.once("autocomplete-updated");
-  jsterm.complete(jsterm.COMPLETE_HINT_ONLY);
+  jsterm.complete(completionType);
   return updated;
 }
 
 /**
  * Open the JavaScript debugger.
  *
  * @param object options
  *        Options for opening the debugger:
