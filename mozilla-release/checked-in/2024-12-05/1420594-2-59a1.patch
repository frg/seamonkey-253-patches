# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1512399092 18000
# Node ID a3e3a096629cff5e9575cd6794702df36d3798e5
# Parent  bdf31182d0ff46ff8c24750c9376816c38506386
Bug 1420594 P2 Eagerly shutdown ClientManagerService. r=baku

diff --git a/dom/clients/manager/ClientManagerService.cpp b/dom/clients/manager/ClientManagerService.cpp
--- a/dom/clients/manager/ClientManagerService.cpp
+++ b/dom/clients/manager/ClientManagerService.cpp
@@ -1,19 +1,23 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ClientManagerService.h"
 
+#include "ClientManagerParent.h"
 #include "ClientSourceParent.h"
 #include "mozilla/ipc/BackgroundParent.h"
 #include "mozilla/ipc/PBackgroundSharedTypes.h"
+#include "mozilla/ClearOnShutdown.h"
+#include "mozilla/SystemGroup.h"
+#include "nsIAsyncShutdown.h"
 
 namespace mozilla {
 namespace dom {
 
 using mozilla::ipc::AssertIsOnBackgroundThread;
 using mozilla::ipc::ContentPrincipalInfo;
 using mozilla::ipc::PrincipalInfo;
 
@@ -51,47 +55,175 @@ MatchPrincipalInfo(const PrincipalInfo& 
       break;
     }
   }
 
   // Clients (windows/workers) should never have an expanded principal type.
   MOZ_CRASH("unexpected principal type!");
 }
 
+class ClientShutdownBlocker final : public nsIAsyncShutdownBlocker
+{
+  RefPtr<GenericPromise::Private> mPromise;
+
+  ~ClientShutdownBlocker() = default;
+
+public:
+  explicit ClientShutdownBlocker(GenericPromise::Private* aPromise)
+    : mPromise(aPromise)
+  {
+    MOZ_DIAGNOSTIC_ASSERT(mPromise);
+  }
+
+  NS_IMETHOD
+  GetName(nsAString& aNameOut) override
+  {
+    aNameOut =
+      NS_LITERAL_STRING("ClientManagerService: start destroying IPC actors early");
+    return NS_OK;
+  }
+
+  NS_IMETHOD
+  BlockShutdown(nsIAsyncShutdownClient* aClient) override
+  {
+    mPromise->Resolve(true, __func__);
+    aClient->RemoveBlocker(this);
+    return NS_OK;
+  }
+
+  NS_IMETHOD
+  GetState(nsIPropertyBag**) override
+  {
+    return NS_OK;
+  }
+
+  NS_DECL_ISUPPORTS
+};
+
+NS_IMPL_ISUPPORTS(ClientShutdownBlocker, nsIAsyncShutdownBlocker)
+
+// Helper function the resolves a MozPromise when we detect that the browser
+// has begun to shutdown.
+RefPtr<GenericPromise>
+OnShutdown()
+{
+  RefPtr<GenericPromise::Private> ref = new GenericPromise::Private(__func__);
+
+  nsCOMPtr<nsIRunnable> r = NS_NewRunnableFunction("ClientManagerServer::OnShutdown",
+  [ref] () {
+    nsCOMPtr<nsIAsyncShutdownService> svc = services::GetAsyncShutdown();
+    if (!svc) {
+      ref->Resolve(true, __func__);
+      return;
+    }
+
+    nsCOMPtr<nsIAsyncShutdownClient> phase;
+    MOZ_ALWAYS_SUCCEEDS(svc->GetXpcomWillShutdown(getter_AddRefs(phase)));
+    if (!phase) {
+      ref->Resolve(true, __func__);
+      return;
+    }
+
+    nsCOMPtr<nsIAsyncShutdownBlocker> blocker = new ClientShutdownBlocker(ref);
+    nsresult rv =
+      phase->AddBlocker(blocker, NS_LITERAL_STRING(__FILE__), __LINE__,
+                        NS_LITERAL_STRING("ClientManagerService shutdown"));
+
+    if (NS_FAILED(rv)) {
+      ref->Resolve(true, __func__);
+      return;
+    }
+  });
+
+  MOZ_ALWAYS_SUCCEEDS(
+    SystemGroup::Dispatch(TaskCategory::Other, r.forget()));
+
+  return ref.forget();
+}
+
 } // anonymous namespace
 
 ClientManagerService::ClientManagerService()
+  : mShutdown(false)
 {
   AssertIsOnBackgroundThread();
+
+  // While the ClientManagerService will be gracefully terminated as windows
+  // and workers are naturally killed, this can cause us to do extra work
+  // relatively late in the shutdown process.  To avoid this we eagerly begin
+  // shutdown at the first sign it has begun.  Since we handle normal shutdown
+  // gracefully we don't really need to block anything here.  We just begin
+  // destroying our IPC actors immediately.
+  OnShutdown()->Then(GetCurrentThreadSerialEventTarget(), __func__,
+    [] () {
+      RefPtr<ClientManagerService> svc = ClientManagerService::GetInstance();
+      if (svc) {
+        svc->Shutdown();
+      }
+    });
 }
 
 ClientManagerService::~ClientManagerService()
 {
   AssertIsOnBackgroundThread();
   MOZ_DIAGNOSTIC_ASSERT(mSourceTable.Count() == 0);
   MOZ_DIAGNOSTIC_ASSERT(mManagerList.IsEmpty());
 
   MOZ_DIAGNOSTIC_ASSERT(sClientManagerServiceInstance == this);
   sClientManagerServiceInstance = nullptr;
 }
 
+void
+ClientManagerService::Shutdown()
+{
+  AssertIsOnBackgroundThread();
+
+  // If many ClientManagerService are created and destroyed quickly we can
+  // in theory get more than one shutdown listener calling us.
+  if (mShutdown) {
+    return;
+  }
+  mShutdown = true;
+
+  // Begin destroying our various manager actors which will in turn destroy
+  // all source, handle, and operation actors.
+  AutoTArray<ClientManagerParent*, 16> list(mManagerList);
+  for (auto actor : list) {
+    Unused << PClientManagerParent::Send__delete__(actor);
+  }
+}
+
 // static
 already_AddRefed<ClientManagerService>
 ClientManagerService::GetOrCreateInstance()
 {
   AssertIsOnBackgroundThread();
 
   if (!sClientManagerServiceInstance) {
     sClientManagerServiceInstance = new ClientManagerService();
   }
 
   RefPtr<ClientManagerService> ref(sClientManagerServiceInstance);
   return ref.forget();
 }
 
+// static
+already_AddRefed<ClientManagerService>
+ClientManagerService::GetInstance()
+{
+  AssertIsOnBackgroundThread();
+
+  if (!sClientManagerServiceInstance) {
+    return nullptr;
+  }
+
+  RefPtr<ClientManagerService> ref(sClientManagerServiceInstance);
+  return ref.forget();
+}
+
 bool
 ClientManagerService::AddSource(ClientSourceParent* aSource)
 {
   AssertIsOnBackgroundThread();
   MOZ_ASSERT(aSource);
   auto entry = mSourceTable.LookupForAdd(aSource->Info().Id());
   // Do not permit overwriting an existing ClientSource with the same
   // UUID.  This would allow a spoofed ClientParentSource actor to
@@ -136,16 +268,21 @@ ClientManagerService::FindSource(const n
 
 void
 ClientManagerService::AddManager(ClientManagerParent* aManager)
 {
   AssertIsOnBackgroundThread();
   MOZ_DIAGNOSTIC_ASSERT(aManager);
   MOZ_ASSERT(!mManagerList.Contains(aManager));
   mManagerList.AppendElement(aManager);
+
+  // If shutdown has already begun then immediately destroy the actor.
+  if (mShutdown) {
+    Unused << PClientManagerParent::Send__delete__(aManager);
+  }
 }
 
 void
 ClientManagerService::RemoveManager(ClientManagerParent* aManager)
 {
   AssertIsOnBackgroundThread();
   MOZ_DIAGNOSTIC_ASSERT(aManager);
   DebugOnly<bool> removed = mManagerList.RemoveElement(aManager);
diff --git a/dom/clients/manager/ClientManagerService.h b/dom/clients/manager/ClientManagerService.h
--- a/dom/clients/manager/ClientManagerService.h
+++ b/dom/clients/manager/ClientManagerService.h
@@ -2,16 +2,17 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #ifndef _mozilla_dom_ClientManagerService_h
 #define _mozilla_dom_ClientManagerService_h
 
 #include "mozilla/ipc/PBackgroundSharedTypes.h"
+#include "mozilla/MozPromise.h"
 #include "nsDataHashtable.h"
 
 namespace mozilla {
 
 namespace dom {
 
 class ClientManagerParent;
 class ClientSourceParent;
@@ -22,23 +23,32 @@ class ClientSourceParent;
 class ClientManagerService final
 {
   // Store the ClientSourceParent objects in a hash table.  We want to
   // optimize for insertion, removal, and lookup by UUID.
   nsDataHashtable<nsIDHashKey, ClientSourceParent*> mSourceTable;
 
   nsTArray<ClientManagerParent*> mManagerList;
 
+  bool mShutdown;
+
   ClientManagerService();
   ~ClientManagerService();
 
+  void
+  Shutdown();
+
 public:
   static already_AddRefed<ClientManagerService>
   GetOrCreateInstance();
 
+  // Returns nullptr if the service is not already created.
+  static already_AddRefed<ClientManagerService>
+  GetInstance();
+
   bool
   AddSource(ClientSourceParent* aSource);
 
   bool
   RemoveSource(ClientSourceParent* aSource);
 
   ClientSourceParent*
   FindSource(const nsID& aID,
