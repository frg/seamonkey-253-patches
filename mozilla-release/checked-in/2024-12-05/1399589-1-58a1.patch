# HG changeset patch
# User Alexandre Poirot <poirot.alex@gmail.com>
# Date 1507046195 -7200
# Node ID 7b2ded01bf04aacd4715b684a6b2e759352f9e28
# Parent  e3df6548ee9159243650d7d0faa8e8af98547680
Bug 1399589 - Register all spec and front modules lazily in specs/index.js. r=jdescottes

MozReview-Commit-ID: 3U2D0PWivm2

diff --git a/devtools/shared/specs/index.js b/devtools/shared/specs/index.js
--- a/devtools/shared/specs/index.js
+++ b/devtools/shared/specs/index.js
@@ -3,61 +3,231 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 // Registry indexing all specs and front modules
 //
 // All spec and front modules should be listed here
 // in order to be referenced by any other spec or front module.
-//
-// TODO: For now we only register dynamically loaded specs and fronts here.
-// See Bug 1399589 for supporting all specs and front modules.
 
-// Declare in which spec module and front module a set of types are defined
-const Types = [
+// Declare in which spec module and front module a set of types are defined.
+// This array should be sorted by `spec` attribute.
+const Types = exports.__TypesForTests = [
+  {
+    types: ["accessible", "accessiblewalker", "accessibility"],
+    spec: "devtools/shared/specs/accessibility",
+    front: "devtools/shared/fronts/accessibility",
+  },
+  {
+    types: ["actorActor", "actorRegistry"],
+    spec: "devtools/shared/specs/actor-registry",
+    front: "devtools/shared/fronts/actor-registry",
+  },
+  {
+    types: ["addons"],
+    spec: "devtools/shared/specs/addons",
+    front: "devtools/shared/fronts/addons",
+  },
+  {
+    types: ["animationplayer", "animations"],
+    spec: "devtools/shared/specs/animation",
+    front: "devtools/shared/fronts/animation",
+  },
+  /* breakpoint has old fashion client and no front */
+  {
+    types: ["breakpoint"],
+    spec: "devtools/shared/specs/breakpoint",
+    front: null,
+  },
+  {
+    types: ["function-call", "call-watcher"],
+    spec: "devtools/shared/specs/call-watcher",
+    front: "devtools/shared/fronts/call-watcher",
+  },
+  {
+    types: ["frame-snapshot", "canvas"],
+    spec: "devtools/shared/specs/canvas",
+    front: "devtools/shared/fronts/canvas",
+  },
+  {
+    types: ["cssProperties"],
+    spec: "devtools/shared/specs/css-properties",
+    front: "devtools/shared/fronts/css-properties",
+  },
   {
-    types: ["pagestyle", "domstylerule"],
-    spec: "devtools/shared/specs/styles",
-    front: "devtools/shared/fronts/styles",
+    types: ["cssUsage"],
+    spec: "devtools/shared/specs/csscoverage",
+    front: "devtools/shared/fronts/csscoverage",
+  },
+  {
+    types: ["device"],
+    spec: "devtools/shared/specs/device",
+    front: "devtools/shared/fronts/device",
+  },
+  {
+    types: ["emulation"],
+    spec: "devtools/shared/specs/emulation",
+    front: "devtools/shared/fronts/emulation",
+  },
+  /* environment has old fashion client and no front */
+  {
+    types: ["environment"],
+    spec: "devtools/shared/specs/environment",
+    front: null,
+  },
+  {
+    types: ["eventLoopLag"],
+    spec: "devtools/shared/specs/eventlooplag",
+    front: "devtools/shared/fronts/eventlooplag",
+  },
+  /* frame has old fashion client and no front */
+  {
+    types: ["frame"],
+    spec: "devtools/shared/specs/frame",
+    front: null,
+  },
+  {
+    types: ["framerate"],
+    spec: "devtools/shared/specs/framerate",
+    front: "devtools/shared/fronts/framerate",
+  },
+  {
+    types: ["gcli"],
+    spec: "devtools/shared/specs/gcli",
+    front: "devtools/shared/fronts/gcli",
+  },
+  /* heap snapshot has old fashion client and no front */
+  {
+    types: ["heapSnapshotFile"],
+    spec: "devtools/shared/specs/heap-snapshot-file",
+    front: null,
   },
   {
     types: ["highlighter", "customhighlighter"],
     spec: "devtools/shared/specs/highlighters",
     front: "devtools/shared/fronts/highlighters",
   },
   {
+    types: ["domnodelist", "domwalker", "inspector"],
+    spec: "devtools/shared/specs/inspector",
+    front: "devtools/shared/fronts/inspector",
+  },
+  {
     types: ["grid", "layout"],
     spec: "devtools/shared/specs/layout",
     front: "devtools/shared/fronts/layout",
   },
   {
+    types: ["memory"],
+    spec: "devtools/shared/specs/memory",
+    front: "devtools/shared/fronts/memory",
+  },
+  /* imageData isn't an actor but just a DictType */
+  {
+    types: ["imageData"],
+    spec: "devtools/shared/specs/node",
+    front: null,
+  },
+  {
+    types: ["domnode"],
+    spec: "devtools/shared/specs/node",
+    front: "devtools/shared/fronts/inspector",
+  },
+  {
+    types: ["performance"],
+    spec: "devtools/shared/specs/performance",
+    front: "devtools/shared/fronts/performance",
+  },
+  {
+    types: ["performance-recording"],
+    spec: "devtools/shared/specs/performance-recording",
+    front: "devtools/shared/fronts/performance-recording",
+  },
+  {
+    types: ["preference"],
+    spec: "devtools/shared/specs/preference",
+    front: "devtools/shared/fronts/preference",
+  },
+  {
+    types: ["promises"],
+    spec: "devtools/shared/specs/promises",
+    front: "devtools/shared/fronts/promises",
+  },
+  {
+    types: ["reflow"],
+    spec: "devtools/shared/specs/reflow",
+    front: "devtools/shared/fronts/reflow",
+  },
+  /* Script and source have old fashion client and no front */
+  {
+    types: ["context"],
+    spec: "devtools/shared/specs/script",
+    front: null,
+  },
+  {
+    types: ["source"],
+    spec: "devtools/shared/specs/source",
+    front: null,
+  },
+  {
+    types: ["cookies", "localStorage", "sessionStorage", "Cache", "indexedDB", "storage"],
+    spec: "devtools/shared/specs/storage",
+    front: "devtools/shared/fronts/storage",
+  },
+  /* longstring is special, it has a wrapper type. See its spec module */
+  {
     types: ["longstring"],
     spec: "devtools/shared/specs/string",
+    front: null
+  },
+  {
+    types: ["longstractor"],
+    spec: "devtools/shared/specs/string",
     front: "devtools/shared/fronts/string",
   },
   {
+    types: ["pagestyle", "domstylerule"],
+    spec: "devtools/shared/specs/styles",
+    front: "devtools/shared/fronts/styles",
+  },
+  {
     types: ["originalsource", "mediarule", "stylesheet", "stylesheets"],
     spec: "devtools/shared/specs/stylesheets",
     front: "devtools/shared/fronts/stylesheets",
   },
   {
-    types: ["imageData", "domnode"],
-    spec: "devtools/shared/specs/node",
-    front: "devtools/shared/fronts/inspector",
+    types: ["timeline"],
+    spec: "devtools/shared/specs/timeline",
+    front: "devtools/shared/fronts/timeline",
+  },
+  {
+    types: ["audionode", "webaudio"],
+    spec: "devtools/shared/specs/webaudio",
+    front: "devtools/shared/fronts/webaudio",
   },
   {
-    types: ["domwalker"],
-    spec: "devtools/shared/specs/inspector",
-    front: "devtools/shared/fronts/inspector",
+    types: ["webExtensionInspectedWindow"],
+    spec: "devtools/shared/specs/webextension-inspected-window",
+    front: "devtools/shared/fronts/webextension-inspected-window",
   },
   {
-    types: ["performance-recording"],
-    spec: "devtools/shared/specs/performance-recording",
-    front: "devtools/shared/fronts/performancec-recording",
+    types: ["webExtensionAddon"],
+    spec: "devtools/shared/specs/webextension-parent",
+    front: null,
+  },
+  {
+    types: ["gl-shader", "gl-program", "webgl"],
+    spec: "devtools/shared/specs/webgl",
+    front: "devtools/shared/fronts/webgl",
+  },
+  {
+    types: ["worker", "pushSubscription", "serviceWorkerRegistration", "serviceWorker"],
+    spec: "devtools/shared/specs/worker",
+    front: null,
   },
 ];
 
 const lazySpecs = new Map();
 const lazyFronts = new Map();
 
 // Convert the human readable `Types` list into efficient maps
 Types.forEach(item => {
diff --git a/devtools/shared/tests/unit/test_protocol_index.js b/devtools/shared/tests/unit/test_protocol_index.js
new file mode 100644
--- /dev/null
+++ b/devtools/shared/tests/unit/test_protocol_index.js
@@ -0,0 +1,51 @@
+/* Any copyright is dedicated to the Public Domain.
+   http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+const { lazyLoadFront } = require("devtools/shared/specs/index");
+const Types = require("devtools/shared/specs/index").__TypesForTests;
+const { getType } = require("devtools/shared/protocol").types;
+
+function run_test() {
+  test_index_is_alphabetically_sorted();
+  test_specs();
+  test_fronts();
+}
+
+// Check alphabetic order of specs defined in devtools/shared/specs/index.js,
+// in order to ease its maintenance and readability.
+function test_index_is_alphabetically_sorted() {
+  let lastSpec = "";
+  for (let type of Types) {
+    let spec = type.spec;
+    if (lastSpec && spec < lastSpec) {
+      ok(false, `Spec definition for "${spec}" should be before "${lastSpec}"`);
+    }
+    lastSpec = spec;
+  }
+  ok(true, "Specs index is alphabetically sorted");
+}
+
+function test_specs() {
+  for (let type of Types) {
+    for (let typeName of type.types) {
+      ok(getType(typeName), `${typeName} spec is defined`);
+    }
+  }
+  ok(true, "Specs are all accessible");
+}
+
+function test_fronts() {
+  for (let item of Types) {
+    if (!item.front) {
+      continue;
+    }
+    for (let typeName of item.types) {
+      lazyLoadFront(typeName);
+      let type = getType(typeName);
+      ok(type, `Front for ${typeName} has a spec`);
+      ok(type.frontClass, `${typeName} has a front correctly defined`);
+    }
+  }
+  ok(true, "Front are all accessible");
+}
diff --git a/devtools/shared/tests/unit/xpcshell.ini b/devtools/shared/tests/unit/xpcshell.ini
--- a/devtools/shared/tests/unit/xpcshell.ini
+++ b/devtools/shared/tests/unit/xpcshell.ini
@@ -36,8 +36,9 @@ run-if = nightly_build
 [test_prettifyCSS.js]
 [test_require_lazy.js]
 [test_require_raw.js]
 [test_require.js]
 [test_stack.js]
 [test_defer.js]
 [test_executeSoon.js]
 [test_wasm-source-map.js]
+[test_protocol_index.js]
