# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1511450509 -3600
# Node ID bac113bafbdfd44e3440b2ec44328810eb9889bb
# Parent  3bc4190076c7c9af39f1dd0e69b893750dcee08c
Bug 1408947 - enable browser_webconsole_reopen_closed_tab.js in the new console frontend;r=nchevobbe

MozReview-Commit-ID: KNcfZ4FqVET

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -49,17 +49,16 @@ support-files =
   test-bug-595934-malformedxml-external.html
   test-bug-595934-malformedxml-external.xml
   test-bug-595934-malformedxml.xhtml
   test-bug-595934-svg.xhtml
   test-bug-595934-workers.html
   test-bug-595934-workers.js
   test-bug-597136-external-script-errors.html
   test-bug-597136-external-script-errors.js
-  test-bug-597756-reopen-closed-tab.html
   test-bug-599725-response-headers.sjs
   test-bug-601177-log-levels.html
   test-bug-601177-log-levels.js
   test-bug-603750-websocket.html
   test-bug-603750-websocket.js
   test-cd-iframe-child.html
   test-cd-iframe-parent.html
   test-console-api-iframe.html
@@ -146,16 +145,17 @@ support-files =
   test-location-styleeditor-link.html
   test-mixedcontent-securityerrors.html
   test-mutation.html
   test-network-request.html
   test-network.html
   test-observe-http-ajax.html
   test-own-console.html
   test-property-provider.html
+  test-reopen-closed-tab.html
   test-repeated-messages.html
   test-result-format-as-string.html
   test-sourcemap-error-01.html
   test-sourcemap-error-01.js
   test-sourcemap-error-02.html
   test-sourcemap-error-02.js
   test-stacktrace-location-debugger-link.html
   test-trackingprotection-securityerrors.html
@@ -382,17 +382,16 @@ skip-if = true # Bug 1408946
 [browser_webconsole_property_provider.js]
 skip-if = true # Bug 1406841
 #old console skip-if = e10s # Bug 1042253 - webconsole tests disabled with e10s
 [browser_webconsole_prune_scroll.js]
 skip-if = true #       Bug 1404832
 [browser_webconsole_reflow.js]
 skip-if = true #       Bug 1406022
 [browser_webconsole_reopen_closed_tab.js]
-skip-if = true # Bug 1408947
 [browser_webconsole_repeat_different_objects.js]
 skip-if = true #       Bug 1401953
 [browser_webconsole_repeated_messages_accuracy.js]
 skip-if = true #       Bug 1403450
 [browser_webconsole_sandbox_update_after_navigation.js]
 skip-if = true #       Bug 1401942
 [browser_webconsole_script_errordoc_urls.js]
 skip-if = true #       Bug 1403454
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_reopen_closed_tab.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_reopen_closed_tab.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_reopen_closed_tab.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_reopen_closed_tab.js
@@ -1,72 +1,53 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
-// See Bug 597756.
+// See Bug 597756. Check that errors are still displayed in the console after reloading a
+// page.
 
 const TEST_URI = "http://example.com/browser/devtools/client/webconsole/" +
-                 "test/test-bug-597756-reopen-closed-tab.html";
+                 "new-console-output/test/mochitest/test-reopen-closed-tab.html";
+
+add_task(async function () {
+  // If we persist log, the test might be successful even if only the first error log is
+  // shown
+  pushPref("devtools.webconsole.persistlog", false);
+
+  info("Open console and refresh tab.");
+
+  expectUncaughtExceptionNoE10s();
+  let hud = await openNewTabAndConsole(TEST_URI);
+  hud.jsterm.clearOutput();
+
+  expectUncaughtExceptionNoE10s();
+  await refreshTab();
+  await waitForError(hud);
 
-var HUD;
+  // Close and reopen
+  await closeConsole();
+
+  expectUncaughtExceptionNoE10s();
+  gBrowser.removeCurrentTab();
+  hud = await openNewTabAndConsole(TEST_URI);
 
-add_task(function* () {
+  expectUncaughtExceptionNoE10s();
+  await refreshTab();
+  await waitForError(hud);
+});
+
+async function waitForError(hud) {
+  info("Wait for error message");
+  await waitFor(() => findMessage(hud, "fooBug597756_error", ".message.error"));
+  ok(true, "error message displayed");
+}
+
+function expectUncaughtExceptionNoE10s() {
   // On e10s, the exception is triggered in child process
   // and is ignored by test harness
   if (!Services.appinfo.browserTabsRemoteAutostart) {
     expectUncaughtException();
   }
-
-  let { browser } = yield loadTab(TEST_URI);
-  HUD = yield openConsole();
-
-  if (!Services.appinfo.browserTabsRemoteAutostart) {
-    expectUncaughtException();
-  }
-
-  yield reload(browser);
-
-  yield testMessages();
-
-  yield closeConsole();
-
-  // Close and reopen
-  gBrowser.removeCurrentTab();
-
-  if (!Services.appinfo.browserTabsRemoteAutostart) {
-    expectUncaughtException();
-  }
-
-  let tab = yield loadTab(TEST_URI);
-  HUD = yield openConsole();
-
-  if (!Services.appinfo.browserTabsRemoteAutostart) {
-    expectUncaughtException();
-  }
-
-  yield reload(tab.browser);
-
-  yield testMessages();
-
-  HUD = null;
-});
-
-function reload(browser) {
-  let loaded = loadBrowser(browser);
-  browser.reload();
-  return loaded;
 }
-
-function testMessages() {
-  return waitForMessages({
-    webconsole: HUD,
-    messages: [{
-      name: "error message displayed",
-      text: "fooBug597756_error",
-      category: CATEGORY_JS,
-      severity: SEVERITY_ERROR,
-    }],
-  });
-}
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/head.js b/devtools/client/webconsole/new-console-output/test/mochitest/head.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/head.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/head.js
@@ -389,16 +389,33 @@ async function openDebugger(options = {}
  */
 async function openConsole(tab) {
   let target = TargetFactory.forTab(tab || gBrowser.selectedTab);
   const toolbox = await gDevTools.showToolbox(target, "webconsole");
   return toolbox.getCurrentPanel().hud;
 }
 
 /**
+ * Close the Web Console for the given tab.
+ *
+ * @param nsIDOMElement [tab]
+ *        Optional tab element for which you want close the Web Console.
+ *        Defaults to current selected tab.
+ * @return object
+ *         A promise that is resolved once the web console is closed.
+ */
+async function closeConsole(tab = gBrowser.selectedTab) {
+  let target = TargetFactory.forTab(tab);
+  let toolbox = gDevTools.getToolbox(target);
+  if (toolbox) {
+    await toolbox.destroy();
+  }
+}
+
+/**
  * Fake clicking a link and return the URL we would have navigated to.
  * This function should be used to check external links since we can't access
  * network in tests.
  *
  * @param ElementNode element
  *        The <a> element we want to simulate click on.
  * @returns Promise
  *          A Promise that resolved when the link clik simulation occured.
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/test-bug-597756-reopen-closed-tab.html b/devtools/client/webconsole/new-console-output/test/mochitest/test-reopen-closed-tab.html
rename from devtools/client/webconsole/new-console-output/test/mochitest/test-bug-597756-reopen-closed-tab.html
rename to devtools/client/webconsole/new-console-output/test/mochitest/test-reopen-closed-tab.html
