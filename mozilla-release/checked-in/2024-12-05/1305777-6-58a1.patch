# HG changeset patch
# User J. Ryan Stinnett <jryans@gmail.com>
# Date 1506549203 18000
# Node ID e2ea4cf82cae3e761e94af85e32b2f8a9a2c48dd
# Parent  280ce67d4d802eccc327ce143d2083d481c70f96
Bug 1305777 - Clean up touch simulator after old RDM removal. r=ochameau

Only `simulator-core` is used by new RDM via the Emulation actor, so I've moved
this server side and renamed it to `touch-simulator`.

MozReview-Commit-ID: 5YgpYNDwBvw

diff --git a/devtools/server/actors/emulation.js b/devtools/server/actors/emulation.js
--- a/devtools/server/actors/emulation.js
+++ b/devtools/server/actors/emulation.js
@@ -2,17 +2,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const { Ci } = require("chrome");
 const protocol = require("devtools/shared/protocol");
 const { emulationSpec } = require("devtools/shared/specs/emulation");
-const { SimulatorCore } = require("devtools/shared/touch/simulator-core");
+const { TouchSimulator } = require("devtools/server/actors/emulation/touch-simulator");
 
 /**
  * This actor overrides various browser features to simulate different environments to
  * test how pages perform under various conditions.
  *
  * The design below, which saves the previous value of each property before setting, is
  * needed because it's possible to have multiple copies of this actor for a single page.
  * When some instance of this actor changes a property, we want it to be able to restore
@@ -23,27 +23,27 @@ const { SimulatorCore } = require("devto
  * "no override" for each of the properties.
  */
 let EmulationActor = protocol.ActorClassWithSpec(emulationSpec, {
 
   initialize(conn, tabActor) {
     protocol.Actor.prototype.initialize.call(this, conn);
     this.tabActor = tabActor;
     this.docShell = tabActor.docShell;
-    this.simulatorCore = new SimulatorCore(tabActor.chromeEventHandler);
+    this.touchSimulator = new TouchSimulator(tabActor.chromeEventHandler);
   },
 
   destroy() {
     this.clearDPPXOverride();
     this.clearNetworkThrottling();
     this.clearTouchEventsOverride();
     this.clearUserAgentOverride();
     this.tabActor = null;
     this.docShell = null;
-    this.simulatorCore = null;
+    this.touchSimulator = null;
     protocol.Actor.prototype.destroy.call(this);
   },
 
   /**
    * Retrieve the console actor for this tab.  This allows us to expose network throttling
    * as part of emulation settings, even though it's internally connected to the network
    * monitor, which for historical reasons is part of the console actor.
    */
@@ -181,19 +181,19 @@ let EmulationActor = protocol.ActorClass
       return false;
     }
     if (this._previousTouchEventsOverride === undefined) {
       this._previousTouchEventsOverride = this.getTouchEventsOverride();
     }
 
     // Start or stop the touch simulator depending on the override flag
     if (flag == Ci.nsIDocShell.TOUCHEVENTS_OVERRIDE_ENABLED) {
-      this.simulatorCore.start();
+      this.touchSimulator.start();
     } else {
-      this.simulatorCore.stop();
+      this.touchSimulator.stop();
     }
 
     this.docShell.touchEventsOverride = flag;
     return true;
   },
 
   getTouchEventsOverride() {
     return this.docShell.touchEventsOverride;
diff --git a/devtools/shared/touch/moz.build b/devtools/server/actors/emulation/moz.build
rename from devtools/shared/touch/moz.build
rename to devtools/server/actors/emulation/moz.build
--- a/devtools/shared/touch/moz.build
+++ b/devtools/server/actors/emulation/moz.build
@@ -1,11 +1,9 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 DevToolsModules(
-    'simulator-content.js',
-    'simulator-core.js',
-    'simulator.js',
+    'touch-simulator.js',
 )
diff --git a/devtools/shared/touch/simulator-core.js b/devtools/server/actors/emulation/touch-simulator.js
rename from devtools/shared/touch/simulator-core.js
rename to devtools/server/actors/emulation/touch-simulator.js
--- a/devtools/shared/touch/simulator-core.js
+++ b/devtools/server/actors/emulation/touch-simulator.js
@@ -19,24 +19,24 @@ var systemAppOrigin = (function () {
     // Fall back to default value
   }
   return systemOrigin;
 })();
 
 var threshold = Services.prefs.getIntPref("ui.dragThresholdX", 25);
 var delay = Services.prefs.getIntPref("ui.click_hold_context_menus.delay", 500);
 
-function SimulatorCore(simulatorTarget) {
+function TouchSimulator(simulatorTarget) {
   this.simulatorTarget = simulatorTarget;
 }
 
 /**
  * Simulate touch events for platforms where they aren't generally available.
  */
-SimulatorCore.prototype = {
+TouchSimulator.prototype = {
   events: [
     "mousedown",
     "mousemove",
     "mouseup",
     "touchstart",
     "touchend",
     "mouseenter",
     "mouseover",
@@ -349,9 +349,9 @@ SimulatorCore.prototype = {
         autoSize.value                        // width = device-width
     ) {
       return 0;
     }
     return 300;
   }
 };
 
-exports.SimulatorCore = SimulatorCore;
+exports.TouchSimulator = TouchSimulator;
diff --git a/devtools/server/actors/moz.build b/devtools/server/actors/moz.build
--- a/devtools/server/actors/moz.build
+++ b/devtools/server/actors/moz.build
@@ -1,15 +1,16 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 DIRS += [
+    'emulation',
     'highlighters',
     'utils',
     'webconsole',
 ]
 
 DevToolsModules(
     'accessibility.js',
     'actor-registry.js',
diff --git a/devtools/shared/moz.build b/devtools/shared/moz.build
--- a/devtools/shared/moz.build
+++ b/devtools/shared/moz.build
@@ -24,17 +24,16 @@ DIRS += [
     'platform',
     'pretty-fast',
     'qrcode',
     'security',
     'sourcemap',
     'sprintfjs',
     'shims',
     'specs',
-    'touch',
     'transport',
     'webconsole',
     'worker',
 ]
 
 # Only ship test helpers in local builds
 if not CONFIG['MOZILLA_OFFICIAL']:
     DIRS += ['test-helpers']
diff --git a/devtools/shared/touch/simulator-content.js b/devtools/shared/touch/simulator-content.js
deleted file mode 100644
--- a/devtools/shared/touch/simulator-content.js
+++ /dev/null
@@ -1,42 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
- /* globals addMessageListener, sendAsyncMessage, docShell */
-"use strict";
-
-const { require } = Cu.import("resource://devtools/shared/Loader.jsm", {});
-const { SimulatorCore } = require("devtools/shared/touch/simulator-core");
-
-/**
- * Launches SimulatorCore in the content window to simulate touch events
- * This frame script is managed by `simulator.js`.
- */
-
-var simulator = {
-  messages: [
-    "TouchEventSimulator:Start",
-    "TouchEventSimulator:Stop",
-  ],
-
-  init() {
-    this.simulatorCore = new SimulatorCore(docShell.chromeEventHandler);
-    this.messages.forEach(msgName => {
-      addMessageListener(msgName, this);
-    });
-  },
-
-  receiveMessage(msg) {
-    switch (msg.name) {
-      case "TouchEventSimulator:Start":
-        this.simulatorCore.start();
-        sendAsyncMessage("TouchEventSimulator:Started");
-        break;
-      case "TouchEventSimulator:Stop":
-        this.simulatorCore.stop();
-        sendAsyncMessage("TouchEventSimulator:Stopped");
-        break;
-    }
-  },
-};
-
-simulator.init();
diff --git a/devtools/shared/touch/simulator.js b/devtools/shared/touch/simulator.js
deleted file mode 100644
--- a/devtools/shared/touch/simulator.js
+++ /dev/null
@@ -1,77 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-"use strict";
-
-var promise = require("promise");
-var defer = require("devtools/shared/defer");
-var Services = require("Services");
-
-const FRAME_SCRIPT =
-  "resource://devtools/shared/touch/simulator-content.js";
-
-var trackedBrowsers = new WeakMap();
-var savedTouchEventsEnabled =
-  Services.prefs.getIntPref("dom.w3c_touch_events.enabled");
-
-/**
- * Simulate touch events for platforms where they aren't generally available.
- * Defers to the `simulator-content.js` frame script to perform the real work.
- */
-function TouchEventSimulator(browser) {
-  // Returns an already instantiated simulator for this browser
-  let simulator = trackedBrowsers.get(browser);
-  if (simulator) {
-    return simulator;
-  }
-
-  let mm = browser.frameLoader.messageManager;
-  mm.loadFrameScript(FRAME_SCRIPT, true);
-
-  simulator = {
-    enabled: false,
-
-    start() {
-      if (this.enabled) {
-        return promise.resolve({ isReloadNeeded: false });
-      }
-      this.enabled = true;
-
-      let deferred = defer();
-      let isReloadNeeded =
-        Services.prefs.getIntPref("dom.w3c_touch_events.enabled") != 1;
-      Services.prefs.setIntPref("dom.w3c_touch_events.enabled", 1);
-      let onStarted = () => {
-        mm.removeMessageListener("TouchEventSimulator:Started", onStarted);
-        deferred.resolve({ isReloadNeeded });
-      };
-      mm.addMessageListener("TouchEventSimulator:Started", onStarted);
-      mm.sendAsyncMessage("TouchEventSimulator:Start");
-      return deferred.promise;
-    },
-
-    stop() {
-      if (!this.enabled) {
-        return promise.resolve();
-      }
-      this.enabled = false;
-
-      let deferred = defer();
-      Services.prefs.setIntPref("dom.w3c_touch_events.enabled",
-                                savedTouchEventsEnabled);
-      let onStopped = () => {
-        mm.removeMessageListener("TouchEventSimulator:Stopped", onStopped);
-        deferred.resolve();
-      };
-      mm.addMessageListener("TouchEventSimulator:Stopped", onStopped);
-      mm.sendAsyncMessage("TouchEventSimulator:Stop");
-      return deferred.promise;
-    }
-  };
-
-  trackedBrowsers.set(browser, simulator);
-
-  return simulator;
-}
-
-exports.TouchEventSimulator = TouchEventSimulator;
