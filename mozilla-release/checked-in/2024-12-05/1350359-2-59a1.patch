# HG changeset patch
# User Eden Chuang <echuang@mozilla.com>
# Date 1512369555 -28800
# Node ID cada18145faf829364d5d87062394f801d47b119
# Parent  2a9f5af042ed9780dc4b8dd677702955c16e74ec
Bug 1350359 - Part 2: Fetch and save alterntative data to InternalResponse. r=bkelly

    Create a new class AlternativeDataStreamListener for alternative data and
    main data overlap loading.
    AlternativeDataStreamListener coorperates with FetchDriver to handle
    following situations
      1. There is no preferred alternative data type in InternalRequest
         Directly using FetchDriver to listen on the opened channel

      2. If preferred alternative data type exists in InternalRequest, but no
         saved data in cache.
         AlternativeDataStreamListener is constructed to listen on the channel,
         but its status would be set as FALLBACK and redirect callbacks to
         FetchDriver.

      3. If preferred alternative data type exists in InternalRequest, and the
         data also exists in the cache.
         AlternativeDataStreamListener is constructed to listen on the channel
         for loading the alternative data. And also open a channel listened by
         FetchDriver for loading the main data when AlternativeDataStreamListener::
         OnStartRequest is called.
         If the cacheEntryId is different between main data channel and
         alternative data channel, we will cancel the alternative data loading.

diff --git a/dom/fetch/FetchDriver.cpp b/dom/fetch/FetchDriver.cpp
--- a/dom/fetch/FetchDriver.cpp
+++ b/dom/fetch/FetchDriver.cpp
@@ -53,16 +53,271 @@ ShouldCheckSRI(const InternalRequest* co
   MOZ_DIAGNOSTIC_ASSERT(aResponse);
 
   return !aRequest->GetIntegrity().IsEmpty() &&
          aResponse->Type() != ResponseType::Error;
 }
 
 } // anonymous namespace
 
+//-----------------------------------------------------------------------------
+// AlternativeDataStreamListener
+//-----------------------------------------------------------------------------
+class AlternativeDataStreamListener final : public nsIStreamListener,
+                                            public nsIThreadRetargetableStreamListener
+{
+public:
+  NS_DECL_THREADSAFE_ISUPPORTS
+  NS_DECL_NSIREQUESTOBSERVER
+  NS_DECL_NSISTREAMLISTENER
+  NS_DECL_NSITHREADRETARGETABLESTREAMLISTENER
+
+  // The status of AlternativeDataStreamListener
+  // LOADING: is the initial status, loading the alternative data
+  // COMPLETED: Alternative data loading is completed
+  // CANCELED: Alternative data loading is canceled, this would make
+  //           AlternativeDataStreamListener ignore all channel callbacks
+  // FALLBACK: fallback the channel callbacks to FetchDriver
+  // Depends on different situaions, the status transition could be followings
+  // 1. LOADING->COMPLETED
+  //    This is the normal status transition for alternative data loading
+  //
+  // 2. LOADING->CANCELED
+  //    LOADING->COMPLETED->CANCELED
+  //    Alternative data loading could be canceled when cacheId from alternative
+  //    data channel does not match with from main data channel(The cacheID
+  //    checking is in FetchDriver::OnStartRequest).
+  //    Notice the alternative data loading could finish before the cacheID
+  //    checking, so the statust transition could be LOADING->COMPLETED->CANCELED
+  //
+  // 3. LOADING->FALLBACK
+  //    For the case that alternative data loading could not be initialized, i.e.
+  //    alternative data does not exist or no preferred alternative data type is
+  //    requested. Once the status becomes FALLBACK, AlternativeDataStreamListener
+  //    transits the channel callback request to FetchDriver, and the status
+  //    should not go back to LOADING, COMPLETED, or CANCELED anymore.
+  enum eStatus {
+    LOADING = 0,
+    COMPLETED,
+    CANCELED,
+    FALLBACK
+  };
+
+  AlternativeDataStreamListener(FetchDriver* aFetchDriver,
+                                nsIChannel* aChannel,
+                                const nsACString& aAlternativeDataType);
+  eStatus Status();
+  void Cancel();
+  uint64_t GetAlternativeDataCacheEntryId();
+  already_AddRefed<nsICacheInfoChannel> GetCacheInfoChannel();
+  already_AddRefed<nsIInputStream> GetAlternativeInputStream();
+
+private:
+  ~AlternativeDataStreamListener() = default;
+
+  // This creates a strong reference cycle with FetchDriver and its
+  // mAltDataListener. We need to clear at least one reference of them once the
+  // data loading finishes.
+  RefPtr<FetchDriver> mFetchDriver;
+  nsCString mAlternativeDataType;
+  nsCOMPtr<nsIInputStream> mPipeAlternativeInputStream;
+  nsCOMPtr<nsIOutputStream> mPipeAlternativeOutputStream;
+  uint64_t mAlternativeDataCacheEntryId;
+  nsCOMPtr<nsICacheInfoChannel> mCacheInfoChannel;
+  nsCOMPtr<nsIChannel> mChannel;
+  Atomic<eStatus> mStatus;
+};
+
+NS_IMPL_ISUPPORTS(AlternativeDataStreamListener,
+                  nsIStreamListener,
+                  nsIThreadRetargetableStreamListener)
+
+AlternativeDataStreamListener::AlternativeDataStreamListener(FetchDriver* aFetchDriver,
+                                                             nsIChannel* aChannel,
+                                                             const nsACString& aAlternativeDataType)
+  : mFetchDriver(aFetchDriver)
+  , mAlternativeDataType(aAlternativeDataType)
+  , mAlternativeDataCacheEntryId(0)
+  , mChannel(aChannel)
+  , mStatus(AlternativeDataStreamListener::LOADING)
+{
+  MOZ_DIAGNOSTIC_ASSERT(mFetchDriver);
+  MOZ_DIAGNOSTIC_ASSERT(mChannel);
+}
+
+AlternativeDataStreamListener::eStatus
+AlternativeDataStreamListener::Status()
+{
+  return mStatus;
+}
+
+void
+AlternativeDataStreamListener::Cancel()
+{
+  mAlternativeDataCacheEntryId = 0;
+  mCacheInfoChannel = nullptr;
+  mPipeAlternativeOutputStream = nullptr;
+  mPipeAlternativeInputStream = nullptr;
+  if (mChannel && mStatus != AlternativeDataStreamListener::FALLBACK) {
+    // if mStatus is fallback, we need to keep channel to forward request back to
+    // FetchDriver
+    mChannel->Cancel(NS_BINDING_ABORTED);
+    mChannel = nullptr;
+  }
+  mStatus = AlternativeDataStreamListener::CANCELED;
+}
+
+uint64_t
+AlternativeDataStreamListener::GetAlternativeDataCacheEntryId()
+{
+  return mAlternativeDataCacheEntryId;
+}
+
+already_AddRefed<nsIInputStream>
+AlternativeDataStreamListener::GetAlternativeInputStream()
+{
+  nsCOMPtr<nsIInputStream> inputStream = mPipeAlternativeInputStream;
+  return inputStream.forget();
+}
+
+already_AddRefed<nsICacheInfoChannel>
+AlternativeDataStreamListener::GetCacheInfoChannel()
+{
+  nsCOMPtr<nsICacheInfoChannel> channel = mCacheInfoChannel;
+  return channel.forget();
+}
+
+NS_IMETHODIMP
+AlternativeDataStreamListener::OnStartRequest(nsIRequest* aRequest,
+                                              nsISupports* aContext)
+{
+  workers::AssertIsOnMainThread();
+  MOZ_ASSERT(!mAlternativeDataType.IsEmpty());
+  // Checking the alternative data type is the same between we asked and the
+  // saved in the channel.
+  nsAutoCString alternativeDataType;
+  nsCOMPtr<nsICacheInfoChannel> cic = do_QueryInterface(aRequest);
+  mStatus = AlternativeDataStreamListener::LOADING;
+  if (cic &&
+      NS_SUCCEEDED(cic->GetAlternativeDataType(alternativeDataType)) &&
+      mAlternativeDataType.Equals(alternativeDataType) &&
+      NS_SUCCEEDED(cic->GetCacheEntryId(&mAlternativeDataCacheEntryId))) {
+
+    MOZ_DIAGNOSTIC_ASSERT(!mPipeAlternativeInputStream);
+    MOZ_DIAGNOSTIC_ASSERT(!mPipeAlternativeOutputStream);
+    nsresult rv = NS_NewPipe(
+      getter_AddRefs(mPipeAlternativeInputStream),
+      getter_AddRefs(mPipeAlternativeOutputStream),
+      0 /* default segment size */,
+      UINT32_MAX /* infinite pipe */,
+      true /* non-blocking input, otherwise you deadlock */,
+      false /* blocking output, since the pipe is 'in'finite */);
+
+    if (NS_FAILED(rv)) {
+      mFetchDriver->FailWithNetworkError(rv);
+      return rv;
+    }
+
+    MOZ_DIAGNOSTIC_ASSERT(!mCacheInfoChannel);
+    mCacheInfoChannel = cic;
+
+    // call FetchDriver::HttpFetch to load main body
+    MOZ_ASSERT(mFetchDriver);
+    return mFetchDriver->HttpFetch();
+
+  } else {
+    // Needn't load alternative data, since alternative data does not exist.
+    // Set status to FALLBACK to reuse the opened channel to load main body, then
+    // call FetchDriver::OnStartRequest to continue the work.
+    // Unfortunately can't change the stream listener to mFetchDriver, need to
+    // keep AlternativeDataStreamListener alive to redirect OnDataAvailable and
+    // OnStopRequest to mFetchDriver.
+    MOZ_ASSERT(alternativeDataType.IsEmpty());
+    mStatus = AlternativeDataStreamListener::FALLBACK;
+    mAlternativeDataCacheEntryId = 0;
+    MOZ_ASSERT(mFetchDriver);
+    return mFetchDriver->OnStartRequest(aRequest, aContext);
+  }
+  return NS_OK;
+}
+
+NS_IMETHODIMP
+AlternativeDataStreamListener::OnDataAvailable(nsIRequest* aRequest,
+                                               nsISupports* aContext,
+                                               nsIInputStream* aInputStream,
+                                               uint64_t aOffset,
+                                               uint32_t aCount)
+{
+  if (mStatus == AlternativeDataStreamListener::LOADING) {
+    MOZ_ASSERT(mPipeAlternativeOutputStream);
+    uint32_t read;
+    return aInputStream->ReadSegments(NS_CopySegmentToStream,
+                                      mPipeAlternativeOutputStream,
+                                      aCount, &read);
+  }
+  if (mStatus == AlternativeDataStreamListener::FALLBACK) {
+    MOZ_ASSERT(mFetchDriver);
+    return mFetchDriver->OnDataAvailable(aRequest, aContext, aInputStream, aOffset, aCount);
+  }
+  return NS_OK;
+}
+
+NS_IMETHODIMP
+AlternativeDataStreamListener::OnStopRequest(nsIRequest* aRequest,
+                                             nsISupports* aContext,
+                                             nsresult aStatusCode)
+{
+  workers::AssertIsOnMainThread();
+
+  // Alternative data loading is going to finish, breaking the reference cycle
+  // here by taking the ownership to a loacl variable.
+  RefPtr<FetchDriver> fetchDriver = mFetchDriver.forget();
+
+  if (mStatus == AlternativeDataStreamListener::CANCELED) {
+    // do nothing
+    return NS_OK;
+  }
+
+  if (mStatus == AlternativeDataStreamListener::FALLBACK) {
+    MOZ_ASSERT(fetchDriver);
+    return fetchDriver->OnStopRequest(aRequest, aContext, aStatusCode);
+  }
+
+  MOZ_DIAGNOSTIC_ASSERT(mStatus == AlternativeDataStreamListener::LOADING);
+
+  MOZ_ASSERT(!mAlternativeDataType.IsEmpty() &&
+             mPipeAlternativeOutputStream &&
+             mPipeAlternativeInputStream);
+
+  mPipeAlternativeOutputStream->Close();
+  mPipeAlternativeOutputStream = nullptr;
+
+  // Cleanup the states for alternative data if needed.
+  if (NS_FAILED(aStatusCode)) {
+    mAlternativeDataCacheEntryId = 0;
+    mCacheInfoChannel = nullptr;
+    mPipeAlternativeInputStream = nullptr;
+  }
+  mStatus = AlternativeDataStreamListener::COMPLETED;
+  // alternative data loading finish, call FetchDriver::FinishOnStopRequest to
+  // continue the final step for the case FetchDriver::OnStopRequest is called
+  // earlier than AlternativeDataStreamListener::OnStopRequest
+  MOZ_ASSERT(fetchDriver);
+  return fetchDriver->FinishOnStopRequest(this);
+}
+
+NS_IMETHODIMP
+AlternativeDataStreamListener::CheckListenerChain()
+{
+  return NS_OK;
+}
+//-----------------------------------------------------------------------------
+// FetchDriver
+//-----------------------------------------------------------------------------
+
 NS_IMPL_ISUPPORTS(FetchDriver,
                   nsIStreamListener, nsIChannelEventSink, nsIInterfaceRequestor,
                   nsIThreadRetargetableStreamListener)
 
 FetchDriver::FetchDriver(InternalRequest* aRequest, nsIPrincipal* aPrincipal,
                          nsILoadGroup* aLoadGroup, nsIEventTarget* aMainThreadEventTarget,
                          bool aIsTrackingFetch)
   : mPrincipal(aPrincipal)
@@ -123,35 +378,36 @@ FetchDriver::Fetch(AbortSignal* aSignal,
     if (aSignal->Aborted()) {
       Abort();
       return NS_OK;
     }
 
     Follow(aSignal);
   }
 
-  rv = HttpFetch();
+  rv = HttpFetch(mRequest->GetPreferredAlternativeDataType());
   if (NS_FAILED(rv)) {
     FailWithNetworkError(rv);
   }
 
   // Any failure is handled by FailWithNetworkError notifying the aObserver.
   return NS_OK;
 }
 
 // This function implements the "HTTP Fetch" algorithm from the Fetch spec.
 // Functionality is often split between here, the CORS listener proxy and the
 // Necko HTTP implementation.
 nsresult
-FetchDriver::HttpFetch()
+FetchDriver::HttpFetch(const nsACString& aPreferredAlternativeDataType)
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   // Step 1. "Let response be null."
   mResponse = nullptr;
+  mOnStopRequestCalled = false;
   nsresult rv;
 
   nsCOMPtr<nsIIOService> ios = do_GetIOService(&rv);
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsAutoCString url;
   mRequest->GetURL(url);
   nsCOMPtr<nsIURI> uri;
@@ -261,18 +517,16 @@ FetchDriver::HttpFetch()
                        mRequest->ContentPolicyType(),
                        mLoadGroup,
                        nullptr, /* aCallbacks */
                        loadFlags,
                        ios);
   }
   NS_ENSURE_SUCCESS(rv, rv);
 
-  mLoadGroup = nullptr;
-
   // Insert ourselves into the notification callbacks chain so we can set
   // headers on redirects.
 #ifdef DEBUG
   {
     nsCOMPtr<nsIInterfaceRequestor> notificationCallbacks;
     chan->GetNotificationCallbacks(getter_AddRefs(notificationCallbacks));
     MOZ_ASSERT(!notificationCallbacks);
   }
@@ -406,17 +660,33 @@ FetchDriver::HttpFetch()
 
   if (mIsTrackingFetch && nsContentUtils::IsLowerNetworkPriority()) {
     nsCOMPtr<nsISupportsPriority> p = do_QueryInterface(chan);
     if (p) {
       p->SetPriority(nsISupportsPriority::PRIORITY_LOWEST);
     }
   }
 
-  rv = chan->AsyncOpen2(this);
+  // if the preferred alternative data type in InternalRequest is not empty, set
+  // the data type on the created channel and also create a AlternativeDataStreamListener
+  // to be the stream listener of the channel.
+  if (!aPreferredAlternativeDataType.IsEmpty()) {
+    nsCOMPtr<nsICacheInfoChannel> cic = do_QueryInterface(chan);
+    if (cic) {
+      cic->PreferAlternativeDataType(aPreferredAlternativeDataType);
+      MOZ_ASSERT(!mAltDataListener);
+      mAltDataListener =
+        new AlternativeDataStreamListener(this, chan, aPreferredAlternativeDataType);
+      rv = chan->AsyncOpen2(mAltDataListener);
+    } else {
+      rv = chan->AsyncOpen2(this);
+    }
+  } else {
+    rv = chan->AsyncOpen2(this);
+  }
   NS_ENSURE_SUCCESS(rv, rv);
 
   // Step 4 onwards of "HTTP Fetch" is handled internally by Necko.
 
   mChannel = chan;
   return NS_OK;
 }
 already_AddRefed<InternalResponse>
@@ -579,16 +849,43 @@ FetchDriver::OnStartRequest(nsIRequest* 
       contentLenStr.AppendInt(contentLength);
       response->Headers()->Append(NS_LITERAL_CSTRING("Content-Length"),
                                   contentLenStr,
                                   result);
       MOZ_ASSERT(!result.Failed());
     }
   }
 
+  nsCOMPtr<nsICacheInfoChannel> cic = do_QueryInterface(aRequest);
+  response->SetCacheInfoChannel(cic);
+  uint64_t cacheEntryId = 0;
+  // Skip the case that mAltDataListener->Status() equals to FALLBACK, that means
+  // the opened channel for alternative data loading is reused for loading the
+  // main data. FALLBACK also means that cacheEntryId from mAltDataListener is
+  // invalid.
+  if (cic &&
+      mAltDataListener &&
+      mAltDataListener->Status() != AlternativeDataStreamListener::FALLBACK &&
+      NS_SUCCEEDED(cic->GetCacheEntryId(&cacheEntryId))) {
+    // Verify the cache ID is the same with from alternative data cache.
+    // If the cache ID is different, droping the alternative data loading,
+    // otherwise setup the response's alternative body and cacheInfoChannel.
+    if (cacheEntryId != mAltDataListener->GetAlternativeDataCacheEntryId()) {
+      mAltDataListener->Cancel();
+    } else {
+      // AlternativeDataStreamListener::OnStartRequest had already been called,
+      // the alternative data input stream and cacheInfo channel must be created.
+      nsCOMPtr<nsICacheInfoChannel> cacheInfo = mAltDataListener->GetCacheInfoChannel();
+      nsCOMPtr<nsIInputStream> altInputStream = mAltDataListener->GetAlternativeInputStream();
+      MOZ_ASSERT(altInputStream && cacheInfo);
+      response->SetAlternativeBody(altInputStream);
+      response->SetCacheInfoChannel(cacheInfo);
+    }
+  }
+
   // We open a pipe so that we can immediately set the pipe's read end as the
   // response's body. Setting the segment size to UINT32_MAX means that the
   // pipe has infinite space. The nsIChannel will continue to buffer data in
   // xpcom events even if we block on a fixed size pipe.  It might be possible
   // to suspend the channel and then resume when there is space available, but
   // for now use an infinite pipe to avoid blocking.
   nsCOMPtr<nsIInputStream> pipeInputStream;
   rv = NS_NewPipe(getter_AddRefs(pipeInputStream),
@@ -811,24 +1108,33 @@ FetchDriver::OnDataAvailable(nsIRequest*
 
 NS_IMETHODIMP
 FetchDriver::OnStopRequest(nsIRequest* aRequest,
                            nsISupports* aContext,
                            nsresult aStatusCode)
 {
   workers::AssertIsOnMainThread();
 
+  MOZ_DIAGNOSTIC_ASSERT(!mOnStopRequestCalled);
+  mOnStopRequestCalled = true;
+
+  // main data loading is going to finish, breaking the reference cycle.
+  RefPtr<AlternativeDataStreamListener> altDataListener = mAltDataListener.forget();
+
   // We need to check mObserver, which is nulled by FailWithNetworkError(),
   // because in the case of "error" redirect mode, aStatusCode may be NS_OK but
   // mResponse will definitely be null so we must not take the else branch.
   if (NS_FAILED(aStatusCode) || !mObserver) {
     nsCOMPtr<nsIAsyncOutputStream> outputStream = do_QueryInterface(mPipeOutputStream);
     if (outputStream) {
       outputStream->CloseWithStatus(NS_BINDING_FAILED);
     }
+    if (altDataListener) {
+      altDataListener->Cancel();
+    }
 
     // We proceed as usual here, since we've already created a successful response
     // from OnStartRequest.
   } else {
     MOZ_ASSERT(mResponse);
     MOZ_ASSERT(!mResponse->IsError());
 
     // From "Main Fetch" step 19: SRI-part3.
@@ -846,27 +1152,52 @@ FetchDriver::OnStopRequest(nsIRequest* a
       if (mDocument && mDocument->GetDocumentURI()) {
         mDocument->GetDocumentURI()->GetAsciiSpec(sourceUri);
       } else if (!mWorkerScript.IsEmpty()) {
         sourceUri.Assign(mWorkerScript);
       }
       nsresult rv = mSRIDataVerifier->Verify(mSRIMetadata, channel, sourceUri,
                                              reporter);
       if (NS_FAILED(rv)) {
+        if (altDataListener) {
+          altDataListener->Cancel();
+        }
         FailWithNetworkError(rv);
         // Cancel request.
         return rv;
       }
     }
 
     if (mPipeOutputStream) {
       mPipeOutputStream->Close();
     }
   }
 
+  return FinishOnStopRequest(altDataListener);
+}
+
+nsresult
+FetchDriver::FinishOnStopRequest(AlternativeDataStreamListener* aAltDataListener)
+{
+  workers::AssertIsOnMainThread();
+  // OnStopRequest is not called from channel, that means the main data loading
+  // does not finish yet. Reaching here since alternative data loading finishes.
+  if (!mOnStopRequestCalled) {
+    return NS_OK;
+  }
+
+  MOZ_DIAGNOSTIC_ASSERT(!mAltDataListener);
+  // Wait for alternative data loading finish if we needed it.
+  if (aAltDataListener &&
+      aAltDataListener->Status() == AlternativeDataStreamListener::LOADING) {
+    // For LOADING case, channel holds the reference of altDataListener, no need
+    // to restore it to mAltDataListener.
+    return NS_OK;
+  }
+
   if (mObserver) {
     // From "Main Fetch" step 19.1, 19.2: Process response.
     if (ShouldCheckSRI(mRequest, mResponse)) {
       MOZ_ASSERT(mResponse);
       mObserver->OnResponseAvailable(mResponse);
       #ifdef DEBUG
         mResponseAvailableCalled = true;
       #endif
diff --git a/dom/fetch/FetchDriver.h b/dom/fetch/FetchDriver.h
--- a/dom/fetch/FetchDriver.h
+++ b/dom/fetch/FetchDriver.h
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_FetchDriver_h
 #define mozilla_dom_FetchDriver_h
 
 #include "nsIChannelEventSink.h"
+#include "nsICacheInfoChannel.h"
 #include "nsIInterfaceRequestor.h"
 #include "nsIStreamListener.h"
 #include "nsIThreadRetargetableStreamListener.h"
 #include "mozilla/ConsoleReportCollector.h"
 #include "mozilla/dom/AbortSignal.h"
 #include "mozilla/dom/SRIMetadata.h"
 #include "mozilla/RefPtr.h"
 
@@ -83,16 +84,18 @@ protected:
 
   virtual void OnResponseAvailableInternal(InternalResponse* aResponse) = 0;
 
   nsCOMPtr<nsIConsoleReportCollector> mReporter;
 private:
   bool mGotResponseAvailable;
 };
 
+class AlternativeDataStreamListener;
+
 class FetchDriver final : public nsIStreamListener,
                           public nsIChannelEventSink,
                           public nsIInterfaceRequestor,
                           public nsIThreadRetargetableStreamListener,
                           public AbortFollower
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
@@ -141,34 +144,42 @@ private:
 
   // This is written once in OnStartRequest on the main thread and then
   // written/read in OnDataAvailable() on any thread.  Necko guarantees
   // that these do not overlap.
   bool mNeedToObserveOnDataAvailable;
 
   bool mIsTrackingFetch;
 
+  RefPtr<AlternativeDataStreamListener> mAltDataListener;
+  bool mOnStopRequestCalled;
+
 #ifdef DEBUG
   bool mResponseAvailableCalled;
   bool mFetchCalled;
 #endif
 
+  friend class AlternativeDataStreamListener;
+
   FetchDriver() = delete;
   FetchDriver(const FetchDriver&) = delete;
   FetchDriver& operator=(const FetchDriver&) = delete;
   ~FetchDriver();
 
-  nsresult HttpFetch();
+
+  nsresult HttpFetch(const nsACString& aPreferredAlternativeDataType = EmptyCString());
   // Returns the filtered response sent to the observer.
   already_AddRefed<InternalResponse>
   BeginAndGetFilteredResponse(InternalResponse* aResponse,
                               bool aFoundOpaqueRedirect);
   // Utility since not all cases need to do any post processing of the filtered
   // response.
   void FailWithNetworkError(nsresult rv);
 
   void SetRequestHeaders(nsIHttpChannel* aChannel) const;
+
+  nsresult FinishOnStopRequest(AlternativeDataStreamListener* aAltDataListener);
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // mozilla_dom_FetchDriver_h
diff --git a/dom/fetch/InternalResponse.h b/dom/fetch/InternalResponse.h
--- a/dom/fetch/InternalResponse.h
+++ b/dom/fetch/InternalResponse.h
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_InternalResponse_h
 #define mozilla_dom_InternalResponse_h
 
 #include "nsIInputStream.h"
+#include "nsICacheInfoChannel.h"
 #include "nsISupportsImpl.h"
 
 #include "mozilla/dom/InternalHeaders.h"
 #include "mozilla/dom/RequestBinding.h"
 #include "mozilla/dom/ResponseBinding.h"
 #include "mozilla/dom/ChannelInfo.h"
 #include "mozilla/UniquePtr.h"
 
@@ -246,16 +247,67 @@ public:
 
   int64_t
   GetPaddingSize();
 
   void
   SetPaddingSize(int64_t aPaddingSize);
 
   void
+  SetAlternativeBody(nsIInputStream* aAlternativeBody)
+  {
+    if (mWrappedResponse) {
+      return mWrappedResponse->SetAlternativeBody(aAlternativeBody);
+    }
+    // A request's body may not be reset once set.
+    MOZ_DIAGNOSTIC_ASSERT(!mAlternativeBody);
+
+    mAlternativeBody = aAlternativeBody;
+  }
+
+  already_AddRefed<nsIInputStream>
+  TakeAlternativeBody()
+  {
+    if (mWrappedResponse) {
+      return mWrappedResponse->TakeAlternativeBody();
+    }
+
+    if (!mAlternativeBody) {
+      return nullptr;
+    }
+
+    // cleanup the non-alternative body here.
+    // Once alternative data is used, the real body is no need anymore.
+    mBody = nullptr;
+    mBodySize = UNKNOWN_BODY_SIZE;
+    return mAlternativeBody.forget();
+  }
+
+  void
+  SetCacheInfoChannel(nsICacheInfoChannel* aCacheInfoChannel)
+  {
+    if (mWrappedResponse) {
+      return mWrappedResponse->SetCacheInfoChannel(aCacheInfoChannel);
+    }
+
+    mCacheInfoChannel = aCacheInfoChannel;
+  }
+
+  already_AddRefed<nsICacheInfoChannel>
+  GetCacheInfoChannel()
+  {
+    if (mWrappedResponse) {
+      return mWrappedResponse->GetCacheInfoChannel();
+    }
+
+    nsCOMPtr<nsICacheInfoChannel> ret = mCacheInfoChannel;
+    return ret.forget();
+  }
+
+  void
   InitChannelInfo(nsIChannel* aChannel)
   {
     mChannelInfo.InitFromChannel(aChannel);
   }
 
   void
   InitChannelInfo(const mozilla::ipc::IPCChannelInfo& aChannelInfo)
   {
@@ -324,18 +376,21 @@ private:
   RefPtr<InternalHeaders> mHeaders;
   nsCOMPtr<nsIInputStream> mBody;
   int64_t mBodySize;
   // It's used to passed to the CacheResponse to generate padding size. Once, we
   // generate the padding size for resposne, we don't need it anymore.
   Maybe<uint32_t> mPaddingInfo;
   int64_t mPaddingSize;
   nsresult mErrorCode;
+  RequestCredentials mCredentialsMode;
 
-  RequestCredentials mCredentialsMode;
+  // For alternative data such as JS Bytecode cached in the HTTP cache.
+  nsCOMPtr<nsIInputStream> mAlternativeBody;
+  nsCOMPtr<nsICacheInfoChannel> mCacheInfoChannel;
 
 public:
   static const int64_t UNKNOWN_BODY_SIZE = -1;
   static const int64_t UNKNOWN_PADDING_SIZE = -1;
 private:
   ChannelInfo mChannelInfo;
   UniquePtr<mozilla::ipc::PrincipalInfo> mPrincipalInfo;
 
