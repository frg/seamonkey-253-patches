# HG changeset patch
# User J. Ryan Stinnett <jryans@gmail.com>
# Date 1507943244 18000
# Node ID b8403c3cf71a2b40532f2769074fe4f2ea719ea7
# Parent  84b1c1ac0c07fd9fa95820e7d2c093a68f88377a
Bug 1407830 - Add diagnostics to RDM swap. r=ochameau

Add some (disabled by default) logging to the RDM swap process to speed up
future investigations.

MozReview-Commit-ID: ICuH7i5Nsq5

diff --git a/devtools/client/responsive.html/browser/swap.js b/devtools/client/responsive.html/browser/swap.js
--- a/devtools/client/responsive.html/browser/swap.js
+++ b/devtools/client/responsive.html/browser/swap.js
@@ -3,16 +3,20 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const { Ci } = require("chrome");
 const { Task } = require("devtools/shared/task");
 const { tunnelToInnerBrowser } = require("./tunnel");
 
+function debug(msg) {
+  // console.log(`RDM swap: ${msg}`);
+}
+
 /**
  * Swap page content from an existing tab into a new browser within a container
  * page.  Page state is preserved by using `swapFrameLoaders`, just like when
  * you move a tab to a new window.  This provides a seamless transition for the
  * user since the page is not reloaded.
  *
  * See /devtools/docs/responsive-design-mode.md for a high level overview of how
  * this is used in RDM.  The steps described there are copied into the code
@@ -78,16 +82,17 @@ function swapToInnerBrowser({ tab, conta
       // Hide the browser content temporarily while things move around to avoid displaying
       // strange intermediate states.
       tab.linkedBrowser.style.visibility = "hidden";
 
       // Freeze navigation temporarily to avoid "blinking" in the location bar.
       freezeNavigationState(tab);
 
       // 1. Create a temporary, hidden tab to load the tool UI.
+      debug("Add blank tool tab");
       let containerTab = addTabSilently("about:blank", {
         skipAnimation: true,
         forceNotRemote: true,
       });
       gBrowser.hideTab(containerTab);
       let containerBrowser = containerTab.linkedBrowser;
       // Even though we load the `containerURL` with `LOAD_FLAGS_BYPASS_HISTORY` below,
       // `SessionHistory.jsm` has a fallback path for tabs with no history which
@@ -98,16 +103,17 @@ function swapToInnerBrowser({ tab, conta
       // this only affects the container frame, not the content.)  A better fix would be
       // to just not load the `content-sessionStore.js` frame script at all in the
       // container tab, but it's loaded for all tab browsers, so this seems a bit harder
       // to achieve in a nice way.
       containerBrowser.messageManager.sendAsyncMessage("SessionStore:flush", {
         epoch: -1,
       });
       // Prevent the `containerURL` from ending up in the tab's history.
+      debug("Load container URL");
       containerBrowser.loadURIWithFlags(containerURL, {
         flags: Ci.nsIWebNavigation.LOAD_FLAGS_BYPASS_HISTORY,
       });
 
       // Copy tab listener state flags to container tab.  Each tab gets its own tab
       // listener and state flags which cache document loading progress.  The state flags
       // are checked when switching tabs to update the browser UI.  The later step of
       // `swapBrowsersAndCloseOther` will fold the state back into the main tab.
@@ -117,49 +123,56 @@ function swapToInnerBrowser({ tab, conta
       // 2. Mark the tool tab browser's docshell as active so the viewport frame
       //    is created eagerly and will be ready to swap.
       // This line is crucial when the tool UI is loaded into a background tab.
       // Without it, the viewport browser's frame is created lazily, leading to
       // a multi-second delay before it would be possible to `swapFrameLoaders`.
       // Even worse than the delay, there appears to be no obvious event fired
       // after the frame is set lazily, so it's unclear how to know that work
       // has finished.
+      debug("Set container docShell active");
       containerBrowser.docShellIsActive = true;
 
       // 3. Create the initial viewport inside the tool UI.
       // The calling application will use container page loaded into the tab to
       // do whatever it needs to create the inner browser.
+      debug("Yield to container tab loaded");
       yield tabLoaded(containerTab);
+      debug("Yield to get inner browser");
       innerBrowser = yield getInnerBrowser(containerBrowser);
       addXULBrowserDecorations(innerBrowser);
       if (innerBrowser.isRemoteBrowser != tab.linkedBrowser.isRemoteBrowser) {
         throw new Error("The inner browser's remoteness must match the " +
                         "original tab.");
       }
 
       // 4. Swap tab content from the regular browser tab to the browser within
       //    the viewport in the tool UI, preserving all state via
       //    `gBrowser._swapBrowserDocShells`.
       dispatchDevToolsBrowserSwap(tab.linkedBrowser, innerBrowser);
+      debug("Swap content to inner browser");
       gBrowser._swapBrowserDocShells(tab, innerBrowser);
 
       // 5. Force the original browser tab to be non-remote since the tool UI
       //    must be loaded in the parent process, and we're about to swap the
       //    tool UI into this tab.
+      debug("Flip original tab to remote false");
       gBrowser.updateBrowserRemoteness(tab.linkedBrowser, false);
 
       // 6. Swap the tool UI (with viewport showing the content) into the
       //    original browser tab and close the temporary tab used to load the
       //    tool via `swapBrowsersAndCloseOther`.
+      debug("Swap tool UI to original tab");
       swapBrowsersAndCloseOtherSilently(tab, containerTab);
 
       // 7. Start a tunnel from the tool tab's browser to the viewport browser
       //    so that some browser UI functions, like navigation, are connected to
       //    the content in the viewport, instead of the tool page.
       tunnel = tunnelToInnerBrowser(tab.linkedBrowser, innerBrowser);
+      debug("Yield to tunnel start");
       yield tunnel.start();
 
       // Swapping browsers disconnects the find bar UI from the browser.
       // If the find bar has been initialized, reconnect it.
       if (gBrowser.isFindBarInitialized(tab)) {
         let findBar = gBrowser.getFindBar(tab);
         findBar.browser = tab.linkedBrowser;
         if (!findBar.hidden) {
diff --git a/devtools/client/responsive.html/manager.js b/devtools/client/responsive.html/manager.js
--- a/devtools/client/responsive.html/manager.js
+++ b/devtools/client/responsive.html/manager.js
@@ -23,16 +23,20 @@ loader.lazyRequireGetter(this, "startup"
   "devtools/client/responsive.html/utils/window", true);
 loader.lazyRequireGetter(this, "message",
   "devtools/client/responsive.html/utils/message");
 loader.lazyRequireGetter(this, "getStr",
   "devtools/client/responsive.html/utils/l10n", true);
 loader.lazyRequireGetter(this, "EmulationFront",
   "devtools/shared/fronts/emulation", true);
 
+function debug(msg) {
+  // console.log(`RDM manager: ${msg}`);
+}
+
 /**
  * ResponsiveUIManager is the external API for the browser UI, etc. to use when
  * opening and closing the responsive UI.
  */
 const ResponsiveUIManager = exports.ResponsiveUIManager = {
   activeTabs: new Map(),
 
   /**
@@ -312,47 +316,57 @@ ResponsiveUI.prototype = {
   /**
    * Open RDM while preserving the state of the page.  We use `swapFrameLoaders`
    * to ensure all in-page state is preserved, just like when you move a tab to
    * a new window.
    *
    * For more details, see /devtools/docs/responsive-design-mode.md.
    */
   init: Task.async(function* () {
+    debug("Init start");
+
     let ui = this;
 
     // Watch for tab close and window close so we can clean up RDM synchronously
     this.tab.addEventListener("TabClose", this);
     this.browserWindow.addEventListener("unload", this);
 
     // Swap page content from the current tab into a viewport within RDM
+    debug("Create browser swapper");
     this.swap = swapToInnerBrowser({
       tab: this.tab,
       containerURL: TOOL_URL,
       getInnerBrowser: Task.async(function* (containerBrowser) {
         let toolWindow = ui.toolWindow = containerBrowser.contentWindow;
         toolWindow.addEventListener("message", ui);
+        debug("Yield to init from inner");
         yield message.request(toolWindow, "init");
         toolWindow.addInitialViewport("about:blank");
+        debug("Yield to browser mounted");
         yield message.wait(toolWindow, "browser-mounted");
         return ui.getViewportBrowser();
       })
     });
+    debug("Yield to swap start");
     yield this.swap.start();
 
     this.tab.addEventListener("BeforeTabRemotenessChange", this);
 
     // Notify the inner browser to start the frame script
+    debug("Yield to start frame script");
     yield message.request(this.toolWindow, "start-frame-script");
 
     // Get the protocol ready to speak with emulation actor
+    debug("Yield to RDP server connect");
     yield this.connectToServer();
 
     // Non-blocking message to tool UI to start any delayed init activities
     message.post(this.toolWindow, "post-init");
+
+    debug("Init done");
   }),
 
   /**
    * Close RDM and restore page content back into a regular tab.
    *
    * @param object
    *        Destroy options, which currently includes a `reason` string.
    * @return boolean
