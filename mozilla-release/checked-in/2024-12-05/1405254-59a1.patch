# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1511366022 -3600
# Node ID 827dbe8a6f8131c4e6f9794312a91b2a88c2f2e8
# Parent  ae95e7e22e2c1f2c6f9ea71f12cef2818bd80155
Bug 1405254 - Add console.count test cases in console mocha tests;r=bgrins.

This patch adds a few console.count stubs and check
that they all render as expected.

MozReview-Commit-ID: IejxMKnLTAz

diff --git a/devtools/client/webconsole/new-console-output/test/components/console-api-call.test.js b/devtools/client/webconsole/new-console-output/test/components/console-api-call.test.js
--- a/devtools/client/webconsole/new-console-output/test/components/console-api-call.test.js
+++ b/devtools/client/webconsole/new-console-output/test/components/console-api-call.test.js
@@ -120,20 +120,48 @@ describe("ConsoleAPICall component:", ()
       }));
 
       expect(wrapper.find(".timestamp").length).toBe(0);
     });
   });
 
   describe("console.count", () => {
     it("renders", () => {
-      const message = stubPreparedMessages.get("console.count('bar')");
-      const wrapper = render(ConsoleApiCall({ message, serviceContainer }));
+      const messages = [{
+        key: "console.count('bar')",
+        expectedBodyText: "bar: 1",
+      }, {
+        key: "console.count | default: 1",
+        expectedBodyText: "default: 1",
+      }, {
+        key: "console.count | default: 2",
+        expectedBodyText: "default: 2",
+      }, {
+        key: "console.count | test counter: 1",
+        expectedBodyText: "test counter: 1",
+      }, {
+        key: "console.count | test counter: 2",
+        expectedBodyText: "test counter: 2",
+      }, {
+        key: "console.count | default: 3",
+        expectedBodyText: "default: 3",
+      }, {
+        key: "console.count | default: 4",
+        expectedBodyText: "default: 4",
+      }, {
+        key: "console.count | test counter: 3",
+        expectedBodyText: "test counter: 3",
+      }];
 
-      expect(wrapper.find(".message-body").text()).toBe("bar: 1");
+      for (const {key, expectedBodyText} of messages) {
+        const message = stubPreparedMessages.get(key);
+        const wrapper = render(ConsoleApiCall({ message, serviceContainer }));
+
+        expect(wrapper.find(".message-body").text()).toBe(expectedBodyText);
+      }
     });
   });
 
   describe("console.assert", () => {
     it("renders", () => {
       const message = stubPreparedMessages.get(
         "console.assert(false, {message: 'foobar'})");
       const wrapper = render(ConsoleApiCall({ message, serviceContainer }));
diff --git a/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/stub-snippets.js b/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/stub-snippets.js
--- a/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/stub-snippets.js
+++ b/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/stub-snippets.js
@@ -130,16 +130,38 @@ console.groupCollapsed(
 console.groupEnd();
 `});
 
 consoleApi.set("console.dir({C, M, Y, K})", {
   keys: ["console.dir({C, M, Y, K})"],
   code: "console.dir({cyan: 'C', magenta: 'M', yellow: 'Y', black: 'K'});"
 });
 
+consoleApi.set("console.count", {
+  keys: [
+    "console.count | default: 1",
+    "console.count | default: 2",
+    "console.count | test counter: 1",
+    "console.count | test counter: 2",
+    "console.count | default: 3",
+    "console.count | clear",
+    "console.count | default: 4",
+    "console.count | test counter: 3",
+  ],
+  code: `
+    console.count();
+    console.count();
+    console.count("test counter");
+    console.count("test counter");
+    console.count();
+    console.clear();
+    console.count();
+    console.count("test counter");
+`});
+
 // CSS messages
 const cssMessage = new Map();
 
 cssMessage.set("Unknown property", `
 p {
   such-unknown-property: wow;
 }
 `);
diff --git a/devtools/client/webconsole/new-console-output/test/fixtures/stubs/consoleApi.js b/devtools/client/webconsole/new-console-output/test/fixtures/stubs/consoleApi.js
--- a/devtools/client/webconsole/new-console-output/test/fixtures/stubs/consoleApi.js
+++ b/devtools/client/webconsole/new-console-output/test/fixtures/stubs/consoleApi.js
@@ -1292,16 +1292,210 @@ stubPreparedMessages.set("console.dir({C
   },
   "groupId": null,
   "exceptionDocURL": null,
   "userProvidedStyles": [],
   "notes": null,
   "indent": 0
 }));
 
+stubPreparedMessages.set("console.count | default: 1", new ConsoleMessage({
+  "id": "1",
+  "allowRepeating": true,
+  "source": "console-api",
+  "timeStamp": 1511365913333,
+  "type": "log",
+  "helperType": null,
+  "level": "log",
+  "messageText": "default: 1",
+  "parameters": null,
+  "repeatId": "{\"frame\":{\"source\":\"http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html\",\"line\":2,\"column\":5},\"groupId\":null,\"indent\":0,\"level\":\"log\",\"messageText\":\"default: 1\",\"parameters\":null,\"source\":\"console-api\",\"type\":\"log\",\"userProvidedStyles\":[]}",
+  "stacktrace": null,
+  "frame": {
+    "source": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "line": 2,
+    "column": 5
+  },
+  "groupId": null,
+  "exceptionDocURL": null,
+  "userProvidedStyles": [],
+  "notes": null,
+  "indent": 0
+}));
+
+stubPreparedMessages.set("console.count | default: 2", new ConsoleMessage({
+  "id": "1",
+  "allowRepeating": true,
+  "source": "console-api",
+  "timeStamp": 1511365913334,
+  "type": "log",
+  "helperType": null,
+  "level": "log",
+  "messageText": "default: 2",
+  "parameters": null,
+  "repeatId": "{\"frame\":{\"source\":\"http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html\",\"line\":3,\"column\":5},\"groupId\":null,\"indent\":0,\"level\":\"log\",\"messageText\":\"default: 2\",\"parameters\":null,\"source\":\"console-api\",\"type\":\"log\",\"userProvidedStyles\":[]}",
+  "stacktrace": null,
+  "frame": {
+    "source": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "line": 3,
+    "column": 5
+  },
+  "groupId": null,
+  "exceptionDocURL": null,
+  "userProvidedStyles": [],
+  "notes": null,
+  "indent": 0
+}));
+
+stubPreparedMessages.set("console.count | test counter: 1", new ConsoleMessage({
+  "id": "1",
+  "allowRepeating": true,
+  "source": "console-api",
+  "timeStamp": 1511365913334,
+  "type": "log",
+  "helperType": null,
+  "level": "log",
+  "messageText": "test counter: 1",
+  "parameters": null,
+  "repeatId": "{\"frame\":{\"source\":\"http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html\",\"line\":4,\"column\":5},\"groupId\":null,\"indent\":0,\"level\":\"log\",\"messageText\":\"test counter: 1\",\"parameters\":null,\"source\":\"console-api\",\"type\":\"log\",\"userProvidedStyles\":[]}",
+  "stacktrace": null,
+  "frame": {
+    "source": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "line": 4,
+    "column": 5
+  },
+  "groupId": null,
+  "exceptionDocURL": null,
+  "userProvidedStyles": [],
+  "notes": null,
+  "indent": 0
+}));
+
+stubPreparedMessages.set("console.count | test counter: 2", new ConsoleMessage({
+  "id": "1",
+  "allowRepeating": true,
+  "source": "console-api",
+  "timeStamp": 1511365913334,
+  "type": "log",
+  "helperType": null,
+  "level": "log",
+  "messageText": "test counter: 2",
+  "parameters": null,
+  "repeatId": "{\"frame\":{\"source\":\"http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html\",\"line\":5,\"column\":5},\"groupId\":null,\"indent\":0,\"level\":\"log\",\"messageText\":\"test counter: 2\",\"parameters\":null,\"source\":\"console-api\",\"type\":\"log\",\"userProvidedStyles\":[]}",
+  "stacktrace": null,
+  "frame": {
+    "source": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "line": 5,
+    "column": 5
+  },
+  "groupId": null,
+  "exceptionDocURL": null,
+  "userProvidedStyles": [],
+  "notes": null,
+  "indent": 0
+}));
+
+stubPreparedMessages.set("console.count | default: 3", new ConsoleMessage({
+  "id": "1",
+  "allowRepeating": true,
+  "source": "console-api",
+  "timeStamp": 1511365913334,
+  "type": "log",
+  "helperType": null,
+  "level": "log",
+  "messageText": "default: 3",
+  "parameters": null,
+  "repeatId": "{\"frame\":{\"source\":\"http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html\",\"line\":6,\"column\":5},\"groupId\":null,\"indent\":0,\"level\":\"log\",\"messageText\":\"default: 3\",\"parameters\":null,\"source\":\"console-api\",\"type\":\"log\",\"userProvidedStyles\":[]}",
+  "stacktrace": null,
+  "frame": {
+    "source": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "line": 6,
+    "column": 5
+  },
+  "groupId": null,
+  "exceptionDocURL": null,
+  "userProvidedStyles": [],
+  "notes": null,
+  "indent": 0
+}));
+
+stubPreparedMessages.set("console.count | clear", new ConsoleMessage({
+  "id": "1",
+  "allowRepeating": true,
+  "source": "console-api",
+  "timeStamp": 1511365913334,
+  "type": "clear",
+  "helperType": null,
+  "level": "log",
+  "messageText": null,
+  "parameters": [
+    "Console was cleared."
+  ],
+  "repeatId": "{\"frame\":{\"source\":\"http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html\",\"line\":7,\"column\":5},\"groupId\":null,\"indent\":0,\"level\":\"log\",\"messageText\":null,\"parameters\":[\"Console was cleared.\"],\"source\":\"console-api\",\"type\":\"clear\",\"userProvidedStyles\":[]}",
+  "stacktrace": null,
+  "frame": {
+    "source": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "line": 7,
+    "column": 5
+  },
+  "groupId": null,
+  "exceptionDocURL": null,
+  "userProvidedStyles": [],
+  "notes": null,
+  "indent": 0
+}));
+
+stubPreparedMessages.set("console.count | default: 4", new ConsoleMessage({
+  "id": "1",
+  "allowRepeating": true,
+  "source": "console-api",
+  "timeStamp": 1511365913335,
+  "type": "log",
+  "helperType": null,
+  "level": "log",
+  "messageText": "default: 4",
+  "parameters": null,
+  "repeatId": "{\"frame\":{\"source\":\"http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html\",\"line\":8,\"column\":5},\"groupId\":null,\"indent\":0,\"level\":\"log\",\"messageText\":\"default: 4\",\"parameters\":null,\"source\":\"console-api\",\"type\":\"log\",\"userProvidedStyles\":[]}",
+  "stacktrace": null,
+  "frame": {
+    "source": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "line": 8,
+    "column": 5
+  },
+  "groupId": null,
+  "exceptionDocURL": null,
+  "userProvidedStyles": [],
+  "notes": null,
+  "indent": 0
+}));
+
+stubPreparedMessages.set("console.count | test counter: 3", new ConsoleMessage({
+  "id": "1",
+  "allowRepeating": true,
+  "source": "console-api",
+  "timeStamp": 1511365913335,
+  "type": "log",
+  "helperType": null,
+  "level": "log",
+  "messageText": "test counter: 3",
+  "parameters": null,
+  "repeatId": "{\"frame\":{\"source\":\"http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html\",\"line\":9,\"column\":5},\"groupId\":null,\"indent\":0,\"level\":\"log\",\"messageText\":\"test counter: 3\",\"parameters\":null,\"source\":\"console-api\",\"type\":\"log\",\"userProvidedStyles\":[]}",
+  "stacktrace": null,
+  "frame": {
+    "source": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "line": 9,
+    "column": 5
+  },
+  "groupId": null,
+  "exceptionDocURL": null,
+  "userProvidedStyles": [],
+  "notes": null,
+  "indent": 0
+}));
+
 stubPackets.set("console.log('foobar', 'test')", {
   "from": "server1.conn0.child1/consoleActor2",
   "type": "consoleAPICall",
   "message": {
     "addonId": "",
     "arguments": [
       "foobar",
       "test"
@@ -2519,12 +2713,223 @@ stubPackets.set("console.dir({C, M, Y, K
     "timeStamp": 1502884924899,
     "timer": null,
     "workerType": "none",
     "styles": [],
     "category": "webdev"
   }
 });
 
+stubPackets.set("console.count | default: 1", {
+  "from": "server1.conn0.child1/consoleActor2",
+  "type": "consoleAPICall",
+  "message": {
+    "addonId": "",
+    "arguments": [
+      "default"
+    ],
+    "columnNumber": 5,
+    "counter": {
+      "count": 1,
+      "label": "default"
+    },
+    "filename": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "functionName": "triggerPacket",
+    "groupName": "",
+    "level": "count",
+    "lineNumber": 2,
+    "private": false,
+    "timeStamp": 1511365913333,
+    "timer": null,
+    "workerType": "none",
+    "styles": [],
+    "category": "webdev"
+  }
+});
+
+stubPackets.set("console.count | default: 2", {
+  "from": "server1.conn0.child1/consoleActor2",
+  "type": "consoleAPICall",
+  "message": {
+    "addonId": "",
+    "arguments": [
+      "default"
+    ],
+    "columnNumber": 5,
+    "counter": {
+      "count": 2,
+      "label": "default"
+    },
+    "filename": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "functionName": "triggerPacket",
+    "groupName": "",
+    "level": "count",
+    "lineNumber": 3,
+    "private": false,
+    "timeStamp": 1511365913334,
+    "timer": null,
+    "workerType": "none",
+    "styles": [],
+    "category": "webdev"
+  }
+});
+
+stubPackets.set("console.count | test counter: 1", {
+  "from": "server1.conn0.child1/consoleActor2",
+  "type": "consoleAPICall",
+  "message": {
+    "addonId": "",
+    "arguments": [
+      "test counter"
+    ],
+    "columnNumber": 5,
+    "counter": {
+      "count": 1,
+      "label": "test counter"
+    },
+    "filename": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "functionName": "triggerPacket",
+    "groupName": "",
+    "level": "count",
+    "lineNumber": 4,
+    "private": false,
+    "timeStamp": 1511365913334,
+    "timer": null,
+    "workerType": "none",
+    "styles": [],
+    "category": "webdev"
+  }
+});
+
+stubPackets.set("console.count | test counter: 2", {
+  "from": "server1.conn0.child1/consoleActor2",
+  "type": "consoleAPICall",
+  "message": {
+    "addonId": "",
+    "arguments": [
+      "test counter"
+    ],
+    "columnNumber": 5,
+    "counter": {
+      "count": 2,
+      "label": "test counter"
+    },
+    "filename": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "functionName": "triggerPacket",
+    "groupName": "",
+    "level": "count",
+    "lineNumber": 5,
+    "private": false,
+    "timeStamp": 1511365913334,
+    "timer": null,
+    "workerType": "none",
+    "styles": [],
+    "category": "webdev"
+  }
+});
+
+stubPackets.set("console.count | default: 3", {
+  "from": "server1.conn0.child1/consoleActor2",
+  "type": "consoleAPICall",
+  "message": {
+    "addonId": "",
+    "arguments": [
+      "default"
+    ],
+    "columnNumber": 5,
+    "counter": {
+      "count": 3,
+      "label": "default"
+    },
+    "filename": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "functionName": "triggerPacket",
+    "groupName": "",
+    "level": "count",
+    "lineNumber": 6,
+    "private": false,
+    "timeStamp": 1511365913334,
+    "timer": null,
+    "workerType": "none",
+    "styles": [],
+    "category": "webdev"
+  }
+});
+
+stubPackets.set("console.count | clear", {
+  "from": "server1.conn0.child1/consoleActor2",
+  "type": "consoleAPICall",
+  "message": {
+    "addonId": "",
+    "arguments": [],
+    "columnNumber": 5,
+    "counter": null,
+    "filename": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "functionName": "triggerPacket",
+    "groupName": "",
+    "level": "clear",
+    "lineNumber": 7,
+    "private": false,
+    "timeStamp": 1511365913334,
+    "timer": null,
+    "workerType": "none",
+    "styles": [],
+    "category": "webdev"
+  }
+});
+
+stubPackets.set("console.count | default: 4", {
+  "from": "server1.conn0.child1/consoleActor2",
+  "type": "consoleAPICall",
+  "message": {
+    "addonId": "",
+    "arguments": [
+      "default"
+    ],
+    "columnNumber": 5,
+    "counter": {
+      "count": 4,
+      "label": "default"
+    },
+    "filename": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "functionName": "triggerPacket",
+    "groupName": "",
+    "level": "count",
+    "lineNumber": 8,
+    "private": false,
+    "timeStamp": 1511365913335,
+    "timer": null,
+    "workerType": "none",
+    "styles": [],
+    "category": "webdev"
+  }
+});
+
+stubPackets.set("console.count | test counter: 3", {
+  "from": "server1.conn0.child1/consoleActor2",
+  "type": "consoleAPICall",
+  "message": {
+    "addonId": "",
+    "arguments": [
+      "test counter"
+    ],
+    "columnNumber": 5,
+    "counter": {
+      "count": 3,
+      "label": "test counter"
+    },
+    "filename": "http://example.com/browser/devtools/client/webconsole/new-console-output/test/fixtures/stub-generators/test-console-api.html",
+    "functionName": "triggerPacket",
+    "groupName": "",
+    "level": "count",
+    "lineNumber": 9,
+    "private": false,
+    "timeStamp": 1511365913335,
+    "timer": null,
+    "workerType": "none",
+    "styles": [],
+    "category": "webdev"
+  }
+});
+
 module.exports = {
   stubPreparedMessages,
   stubPackets,
 };
