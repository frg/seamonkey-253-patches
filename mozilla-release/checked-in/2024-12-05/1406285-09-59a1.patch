# HG changeset patch
# User Daisuke Akatsuka <dakatsuka@mozilla.com>
# Date 1516248125 -32400
# Node ID d948ac6aff2cbfdffaa0dcfb1fbe4c3eaf6a6ac2
# Parent  ff3287f88c67509d5af249e4d9dd5e069b76b5cf
Bug 1406285 - Part 9: Implement endDelay component. r=gl

MozReview-Commit-ID: F62qpT8yh03

diff --git a/devtools/client/inspector/animation/components/graph/EndDelaySign.js b/devtools/client/inspector/animation/components/graph/EndDelaySign.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/animation/components/graph/EndDelaySign.js
@@ -0,0 +1,47 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+"use strict";
+
+const { PureComponent } = require("devtools/client/shared/vendor/react");
+const dom = require("devtools/client/shared/vendor/react-dom-factories");
+const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
+
+class EndDelaySign extends PureComponent {
+  static get propTypes() {
+    return {
+      animation: PropTypes.object.isRequired,
+      timeScale: PropTypes.object.isRequired,
+    };
+  }
+
+  render() {
+    const {
+      animation,
+      timeScale,
+    } = this.props;
+
+    const { state } = animation;
+    const startTime = (state.previousStartTime || 0) - timeScale.minStartTime;
+    const endTime = state.duration * state.iterationCount + state.delay;
+    const endDelay = state.endDelay < 0 ? state.endDelay : 0;
+    const offset = (startTime + endTime + endDelay) / timeScale.getDuration() * 100;
+    const width = Math.abs(state.endDelay) / timeScale.getDuration() * 100;
+
+    const endDelayClass = state.endDelay < 0 ? "negative" : "";
+    const fillClass = state.fill === "both" || state.fill === "forwards" ? "fill" : "";
+
+    return dom.div(
+      {
+        className: `animation-end-delay-sign ${ endDelayClass } ${ fillClass }`,
+        style: {
+          width: `${ width }%`,
+          left: `${ offset }%`,
+        },
+      }
+    );
+  }
+}
+
+module.exports = EndDelaySign;
diff --git a/devtools/client/inspector/animation/components/graph/SummaryGraph.js b/devtools/client/inspector/animation/components/graph/SummaryGraph.js
--- a/devtools/client/inspector/animation/components/graph/SummaryGraph.js
+++ b/devtools/client/inspector/animation/components/graph/SummaryGraph.js
@@ -4,16 +4,17 @@
 
 "use strict";
 
 const { createFactory, PureComponent } = require("devtools/client/shared/vendor/react");
 const dom = require("devtools/client/shared/vendor/react-dom-factories");
 const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
 
 const DelaySign = createFactory(require("./DelaySign"));
+const EndDelaySign = createFactory(require("./EndDelaySign"));
 const SummaryGraphPath = createFactory(require("./SummaryGraphPath"));
 
 class SummaryGraph extends PureComponent {
   static get propTypes() {
     return {
       animation: PropTypes.object.isRequired,
       simulateAnimation: PropTypes.func.isRequired,
       timeScale: PropTypes.object.isRequired,
@@ -41,14 +42,23 @@ class SummaryGraph extends PureComponent
       animation.state.delay ?
         DelaySign(
           {
             animation,
             timeScale,
           }
         )
       :
+      null,
+      animation.state.iterationCount && animation.state.endDelay ?
+        EndDelaySign(
+          {
+            animation,
+            timeScale,
+          }
+        )
+      :
       null
     );
   }
 }
 
 module.exports = SummaryGraph;
diff --git a/devtools/client/inspector/animation/components/graph/moz.build b/devtools/client/inspector/animation/components/graph/moz.build
--- a/devtools/client/inspector/animation/components/graph/moz.build
+++ b/devtools/client/inspector/animation/components/graph/moz.build
@@ -1,12 +1,13 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 DevToolsModules(
     'ComputedTimingPath.js',
     'DelaySign.js',
     'EffectTimingPath.js',
+    'EndDelaySign.js',
     'SummaryGraph.js',
     'SummaryGraphPath.js',
     'TimingPath.js'
 )
diff --git a/devtools/client/themes/animation.css b/devtools/client/themes/animation.css
--- a/devtools/client/themes/animation.css
+++ b/devtools/client/themes/animation.css
@@ -114,43 +114,54 @@
   transform: scale(1, -1);
   vector-effect: non-scaling-stroke;
 }
 
 .animation-effect-timing-path path.infinity:nth-child(n+2) {
   opacity: 0.3;
 }
 
-.animation-delay-sign {
+.animation-delay-sign,
+.animation-end-delay-sign {
   background-color: var(--theme-graphs-grey);
   height: 3px;
   position: absolute;
   top: calc(100% - 1.5px);
 }
 
-.animation-delay-sign::before {
+.animation-delay-sign::before,
+.animation-end-delay-sign::before {
   background-color: inherit;
   border-radius: 50%;
   content: "";
   height: 6px;
-  left: -3px;
   position: absolute;
   top: -1.5px;
   width: 6px;
 }
 
-.animation-delay-sign.fill {
+.animation-delay-sign.fill,
+.animation-end-delay-sign.fill {
   background-color: var(--effect-timing-graph-color);
 }
 
 .animation-delay-sign.negative::before {
   left: unset;
   right: -3px;
 }
 
+.animation-end-delay-sign::before {
+  right: -3px;
+}
+
+.animation-end-delay-sign.negative::before {
+  left: -3px;
+  right: unset;
+}
+
 /* No Animation Panel */
 .animation-error-message {
   overflow: auto;
 }
 
 .animation-error-message > p {
   white-space: pre;
 }
