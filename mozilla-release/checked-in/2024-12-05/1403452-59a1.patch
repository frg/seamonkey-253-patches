# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1510217128 -3600
# Node ID 3dac33fd437c414381b27dcda649613e27f9a949
# Parent  cda492e7395cf6033f3c659c29280220ac9dc049
Bug 1403452 - Enable browser_webconsole_block_mixedcontent_securityerrors in new console frontend; r=jdescottes.

MozReview-Commit-ID: KBd0K20pDeQ

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -219,18 +219,16 @@ skip-if = true #       Bug 1403188
 [browser_jsterm_no_input_change_and_tab_key_pressed.js]
 [browser_jsterm_popup_close_on_tab_switch.js]
 [browser_jsterm_popup.js]
 [browser_jsterm_selfxss.js]
 subsuite = clipboard
 [browser_netmonitor_shows_reqs_in_webconsole.js]
 [browser_webconsole_allow_mixedcontent_securityerrors.js]
 tags = mcb
-skip-if = true #       Bug 1403452
-# old console skip-if = (os == 'win' && bits == 64) # Bug 1390001
 [browser_webconsole_batching.js]
 [browser_webconsole_block_mixedcontent_securityerrors.js]
 tags = mcb
 skip-if = true #       Bug 1403899
 # old console skip-if = (os == 'win' && bits == 64) # Bug 1390001
 [browser_webconsole_cached_messages.js]
 [browser_webconsole_cd_iframe.js]
 skip-if = true #       Bug 1406030
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_allow_mixedcontent_securityerrors.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_allow_mixedcontent_securityerrors.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_allow_mixedcontent_securityerrors.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_allow_mixedcontent_securityerrors.js
@@ -9,61 +9,42 @@
 // are logged to the console and have the correct "Learn More"
 // url appended to them.
 // Bug 875456 - Log mixed content messages from the Mixed Content
 // Blocker to the Security Pane in the Web Console
 
 "use strict";
 
 const TEST_URI = "https://example.com/browser/devtools/client/webconsole/" +
-                 "test/test-mixedcontent-securityerrors.html";
-const LEARN_MORE_URI = "https://developer.mozilla.org/docs/Web/Security/" +
-                       "Mixed_content" + DOCS_GA_PARAMS;
+  "new-console-output/test/mochitest/test-mixedcontent-securityerrors.html";
+const LEARN_MORE_URI =
+  "https://developer.mozilla.org/docs/Web/Security/Mixed_content" + DOCS_GA_PARAMS;
 
-add_task(function* () {
-  yield pushPrefEnv();
+add_task(async function () {
+  await Promise.all([
+    pushPref("security.mixed_content.block_active_content", false),
+    pushPref("security.mixed_content.block_display_content", false),
+  ]);
 
-  yield loadTab(TEST_URI);
+  const hud = await openNewTabAndConsole(TEST_URI);
 
-  let hud = yield openConsole();
+  const activeContentText = "Loading mixed (insecure) active content " +
+    "\u201chttp://example.com/\u201d on a secure page";
+  const displayContentText = "Loading mixed (insecure) display content " +
+    "\u201chttp://example.com/tests/image/test/mochitest/blue.png\u201d on a secure page";
 
-  let results = yield waitForMessages({
-    webconsole: hud,
-    messages: [
-      {
-        name: "Logged mixed active content",
-        text: "Loading mixed (insecure) active content " +
-              "\u201chttp://example.com/\u201d on a secure page",
-        category: CATEGORY_SECURITY,
-        severity: SEVERITY_WARNING,
-        objects: true,
-      },
-      {
-        name: "Logged mixed passive content - image",
-        text: "Loading mixed (insecure) display content " +
-              "\u201chttp://example.com/tests/image/test/mochitest/blue.png\u201d " +
-              "on a secure page",
-        category: CATEGORY_SECURITY,
-        severity: SEVERITY_WARNING,
-        objects: true,
-      },
-    ],
-  });
+  const waitUntilWarningMessage = text =>
+    waitFor(() => findMessage(hud, text, ".message.warn"), undefined, 100);
+
+  const onMixedActiveContent = waitUntilWarningMessage(activeContentText);
+  const onMixedDisplayContent = waitUntilWarningMessage(displayContentText);
+
+  await onMixedDisplayContent;
+  ok(true, "Mixed display content warning message is visible");
 
-  yield testClickOpenNewTab(hud, results);
-});
+  const mixedActiveContentMessage = await onMixedActiveContent;
+  ok(true, "Mixed active content warning message is visible");
 
-function pushPrefEnv() {
-  let deferred = defer();
-  let options = {"set":
-      [["security.mixed_content.block_active_content", false],
-       ["security.mixed_content.block_display_content", false]
-  ]};
-  SpecialPowers.pushPrefEnv(options, deferred.resolve);
-  return deferred.promise;
-}
-
-function testClickOpenNewTab(hud, results) {
-  let warningNode = results[0].clickableElements[0];
-  ok(warningNode, "link element");
-  ok(warningNode.classList.contains("learn-more-link"), "link class name");
-  return simulateMessageLinkClick(warningNode, LEARN_MORE_URI);
-}
+  info("Clicking on the Learn More link");
+  const learnMoreLink = mixedActiveContentMessage.querySelector(".learn-more-link");
+  const url = await simulateLinkClick(learnMoreLink);
+  is(url, LEARN_MORE_URI, `Clicking the provided link opens ${url}`);
+});
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/head.js b/devtools/client/webconsole/new-console-output/test/mochitest/head.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/head.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/head.js
@@ -1,27 +1,28 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 /* import-globals-from ../../../../framework/test/shared-head.js */
-/* exported WCUL10n, openNewTabAndConsole, waitForMessages, waitFor, findMessage,
-   openContextMenu, hideContextMenu, loadDocument, hasFocus,
-   waitForNodeMutation, testOpenInDebugger, checkClickOnNode */
+/* eslint no-unused-vars: [2, {"vars": "local"}] */
 
 "use strict";
 
 // shared-head.js handles imports, constants, and utility functions
 // Load the shared-head file first.
 Services.scriptloader.loadSubScript(
   "chrome://mochitests/content/browser/devtools/client/framework/test/shared-head.js",
   this);
 
 var {HUDService} = require("devtools/client/webconsole/hudservice");
 var WCUL10n = require("devtools/client/webconsole/webconsole-l10n");
+const DOCS_GA_PARAMS = "?utm_source=mozilla" +
+                       "&utm_medium=firefox-console-errors" +
+                       "&utm_campaign=default";
 
 Services.prefs.setBoolPref("devtools.webconsole.new-frontend-enabled", true);
 registerCleanupFunction(function* () {
   Services.prefs.clearUserPref("devtools.webconsole.new-frontend-enabled");
   Services.prefs.clearUserPref("devtools.webconsole.ui.filterbar");
 
   // Reset all filter prefs between tests. First flushPrefEnv in case one of the
   // filter prefs has been pushed for the test
@@ -357,9 +358,33 @@ async function openDebugger(options = {}
  *        Defaults to current selected tab.
  * @return Promise
  *         A promise that is resolved with the console hud once the web console is open.
  */
 async function openConsole(tab) {
   let target = TargetFactory.forTab(tab || gBrowser.selectedTab);
   const toolbox = await gDevTools.showToolbox(target, "webconsole");
   return toolbox.getCurrentPanel().hud;
-};
+}
+
+/**
+ * Fake clicking a link and return the URL we would have navigated to.
+ * This function should be used to check external links since we can't access
+ * network in tests.
+ *
+ * @param ElementNode element
+ *        The <a> element we want to simulate click on.
+ * @returns Promise
+ *          A Promise that resolved when the link clik simulation occured.
+ */
+function simulateLinkClick(element) {
+  return new Promise((resolve) => {
+    // Override openUILinkIn to prevent navigating.
+    let oldOpenUILinkIn = window.openUILinkIn;
+    window.openUILinkIn = function (link) {
+      window.openUILinkIn = oldOpenUILinkIn;
+      resolve(link);
+    };
+
+    // Click on the link.
+    element.click();
+  });
+}
