# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1510563765 -3600
# Node ID d720dddd8e4e22f6470efb35f0b795e3dd899376
# Parent  0f35f0daecdecd71b199d574b1e6386a928e2c54
Bug 1403899 - Enable browser_webconsole_block_mixedcontent_securityerrors.js in new console frontend; r=Honza.

MozReview-Commit-ID: 5rCahlZBOGy

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -222,18 +222,16 @@ skip-if = true #       Bug 1403188
 [browser_jsterm_selfxss.js]
 subsuite = clipboard
 [browser_netmonitor_shows_reqs_in_webconsole.js]
 [browser_webconsole_allow_mixedcontent_securityerrors.js]
 tags = mcb
 [browser_webconsole_batching.js]
 [browser_webconsole_block_mixedcontent_securityerrors.js]
 tags = mcb
-skip-if = true #       Bug 1403899
-# old console skip-if = (os == 'win' && bits == 64) # Bug 1390001
 [browser_webconsole_cached_messages.js]
 [browser_webconsole_cd_iframe.js]
 skip-if = true #       Bug 1406030
 [browser_webconsole_certificate_messages.js]
 [browser_webconsole_charset.js]
 skip-if = true #       Bug 1404400
 [browser_webconsole_click_function_to_source.js]
 skip-if = true #       Bug 1406038
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_block_mixedcontent_securityerrors.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_block_mixedcontent_securityerrors.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_block_mixedcontent_securityerrors.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_block_mixedcontent_securityerrors.js
@@ -7,102 +7,82 @@
 // on it while the "block mixed content" settings are _on_.
 // It then checks that the blocked mixed content warning messages
 // are logged to the console and have the correct "Learn More"
 // url appended to them. After the first test finishes, it invokes
 // a second test that overrides the mixed content blocker settings
 // by clicking on the doorhanger shield and validates that the
 // appropriate messages are logged to console.
 // Bug 875456 - Log mixed content messages from the Mixed Content
-// Blocker to the Security Pane in the Web Console
+// Blocker to the Security Pane in the Web Console.
 
 "use strict";
 
 const TEST_URI = "https://example.com/browser/devtools/client/webconsole/" +
-                 "test/test-mixedcontent-securityerrors.html";
-const LEARN_MORE_URI = "https://developer.mozilla.org/docs/Web/Security/" +
-                       "Mixed_content" + DOCS_GA_PARAMS;
+  "new-console-output/test/mochitest/test-mixedcontent-securityerrors.html";
+const LEARN_MORE_URI =
+  "https://developer.mozilla.org/docs/Web/Security/Mixed_content" + DOCS_GA_PARAMS;
+
+const blockedActiveContentText = "Blocked loading mixed active content " +
+  "\u201chttp://example.com/\u201d";
+const blockedDisplayContentText = "Blocked loading mixed display content " +
+  "\u201chttp://example.com/tests/image/test/mochitest/blue.png\u201d";
+const activeContentText = "Loading mixed (insecure) active content " +
+  "\u201chttp://example.com/\u201d on a secure page";
+const displayContentText = "Loading mixed (insecure) display content " +
+  "\u201chttp://example.com/tests/image/test/mochitest/blue.png\u201d on a secure page";
 
-add_task(function* () {
-  yield pushPrefEnv();
+add_task(async function() {
+  await pushPrefEnv();
+
+  const hud = await openNewTabAndConsole(TEST_URI);
+
+  const waitForErrorMessage = text =>
+    waitFor(() => findMessage(hud, text, ".message.error"), undefined, 100);
 
-  let { browser } = yield loadTab(TEST_URI);
+  const onBlockedIframe = waitForErrorMessage(blockedActiveContentText);
+  const onBlockedImage = waitForErrorMessage(blockedDisplayContentText);
 
-  let hud = yield openConsole();
+  await onBlockedImage;
+  ok(true, "Blocked mixed display content error message is visible");
+
+  const blockedMixedActiveContentMessage = await onBlockedIframe;
+  ok(true, "Blocked mixed active content error message is visible");
 
-  let results = yield waitForMessages({
-    webconsole: hud,
-    messages: [
-      {
-        name: "Logged blocking mixed active content",
-        text: "Blocked loading mixed active content \u201chttp://example.com/\u201d",
-        category: CATEGORY_SECURITY,
-        severity: SEVERITY_ERROR,
-        objects: true,
-      },
-      {
-        name: "Logged blocking mixed passive content - image",
-        text: "Blocked loading mixed active content \u201chttp://example.com/\u201d",
-        category: CATEGORY_SECURITY,
-        severity: SEVERITY_ERROR,
-        objects: true,
-      },
-    ],
-  });
+  info("Clicking on the Learn More link");
+  let learnMoreLink = blockedMixedActiveContentMessage.querySelector(".learn-more-link");
+  let url = await simulateLinkClick(learnMoreLink);
+  is(url, LEARN_MORE_URI, `Clicking the provided link opens ${url}`);
+
+  info("Test disabling mixed content protection");
+
+  let {gIdentityHandler} = gBrowser.ownerGlobal;
+  ok(gIdentityHandler._identityBox.classList.contains("mixedActiveBlocked"),
+    "Mixed Active Content state appeared on identity box");
+  // Disabe mixed content protection.
+  gIdentityHandler.disableMixedContentProtection();
 
-  yield testClickOpenNewTab(hud, results[0]);
+  const waitForWarningMessage = text =>
+    waitFor(() => findMessage(hud, text, ".message.warn"), undefined, 100);
+
+  const onMixedActiveContent = waitForWarningMessage(activeContentText);
+  const onMixedDisplayContent = waitForWarningMessage(displayContentText);
 
-  let results2 = yield mixedContentOverrideTest2(hud, browser);
+  await onMixedDisplayContent;
+  ok(true, "Mixed display content warning message is visible");
 
-  yield testClickOpenNewTab(hud, results2[0]);
+  const mixedActiveContentMessage = await onMixedActiveContent;
+  ok(true, "Mixed active content warning message is visible");
+
+  info("Clicking on the Learn More link");
+  learnMoreLink = mixedActiveContentMessage.querySelector(".learn-more-link");
+  url = await simulateLinkClick(learnMoreLink);
+  is(url, LEARN_MORE_URI, `Clicking the provided link opens ${url}`);
 });
 
 function pushPrefEnv() {
-  let deferred = defer();
-  let options = {
-    "set": [
-      ["security.mixed_content.block_active_content", true],
-      ["security.mixed_content.block_display_content", true],
-    ]
-  };
-  SpecialPowers.pushPrefEnv(options, deferred.resolve);
-  return deferred.promise;
-}
-
-function mixedContentOverrideTest2(hud, browser) {
-  let deferred = defer();
-  let {gIdentityHandler} = browser.ownerGlobal;
-  ok(gIdentityHandler._identityBox.classList.contains("mixedActiveBlocked"),
-    "Mixed Active Content state appeared on identity box");
-  gIdentityHandler.disableMixedContentProtection();
+  const prefs = [
+    ["security.mixed_content.block_active_content", true],
+    ["security.mixed_content.block_display_content", true],
+  ];
 
-  waitForMessages({
-    webconsole: hud,
-    messages: [
-      {
-        name: "Logged blocking mixed active content",
-        text: "Loading mixed (insecure) active content " +
-              "\u201chttp://example.com/\u201d on a secure page",
-        category: CATEGORY_SECURITY,
-        severity: SEVERITY_WARNING,
-        objects: true,
-      },
-      {
-        name: "Logged blocking mixed passive content - image",
-        text: "Loading mixed (insecure) display content" +
-          " \u201chttp://example.com/tests/image/test/mochitest/blue.png\u201d" +
-          " on a secure page",
-        category: CATEGORY_SECURITY,
-        severity: SEVERITY_WARNING,
-        objects: true,
-      },
-    ],
-  }).then(msgs => deferred.resolve(msgs), console.error);
-
-  return deferred.promise;
+  return Promise.all(prefs.map(([pref, value]) => pushPref(pref, value)));
 }
-
-function testClickOpenNewTab(hud, match) {
-  let warningNode = match.clickableElements[0];
-  ok(warningNode, "link element");
-  ok(warningNode.classList.contains("learn-more-link"), "link class name");
-  return simulateMessageLinkClick(warningNode, LEARN_MORE_URI);
-}
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_block_mixedcontent_securityerrors.js.1424917.later b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_block_mixedcontent_securityerrors.js.1424917.later
deleted file mode 100644
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_block_mixedcontent_securityerrors.js.1424917.later
+++ /dev/null
@@ -1,17 +0,0 @@
---- browser_webconsole_block_mixedcontent_securityerrors.js
-+++ browser_webconsole_block_mixedcontent_securityerrors.js
-@@ -77,14 +77,12 @@ add_task(async function() {
-   url = await simulateLinkClick(learnMoreLink);
-   is(url, LEARN_MORE_URI, `Clicking the provided link opens ${url}`);
- });
- 
- function pushPrefEnv() {
-   const prefs = [
-     ["security.mixed_content.block_active_content", true],
-     ["security.mixed_content.block_display_content", true],
--    ["security.mixed_content.use_hsts", false],
--    ["security.mixed_content.send_hsts_priming", false],
-   ];
- 
-   return Promise.all(prefs.map(([pref, value]) => pushPref(pref, value)));
- }
