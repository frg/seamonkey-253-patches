# HG changeset patch
# User J. Ryan Stinnett <jryans@gmail.com>
# Date 1510246219 21600
# Node ID c2e7d45a29a26698b5fe5976a39a9e001a2d68e5
# Parent  6f6ca4fb0de95860fc358f2c3e72d939d2140581
Bug 1415502 - Remove RDM browser-swap docs. r=jryans

These low level docs are getting out of date and causing confusion.  Further,
they are of limited value at this stage anyway.

MozReview-Commit-ID: FSoNniNZjtj

diff --git a/devtools/client/responsive.html/browser/swap.js b/devtools/client/responsive.html/browser/swap.js
--- a/devtools/client/responsive.html/browser/swap.js
+++ b/devtools/client/responsive.html/browser/swap.js
@@ -17,19 +17,16 @@ function debug(msg) {
  * page.  Page state is preserved by using `swapFrameLoaders`, just like when
  * you move a tab to a new window.  This provides a seamless transition for the
  * user since the page is not reloaded.
  *
  * See /devtools/docs/responsive-design-mode.md for a high level overview of how
  * this is used in RDM.  The steps described there are copied into the code
  * below.
  *
- * For additional low level details about swapping browser content,
- * see /devtools/client/responsive.html/docs/browser-swap.md.
- *
  * @param tab
  *        A browser tab with content to be swapped.
  * @param containerURL
  *        URL to a page that holds an inner browser.
  * @param getInnerBrowser
  *        Function that returns a Promise to the inner browser within the
  *        container page.  It is called with the outer browser that loaded the
  *        container page.
diff --git a/devtools/client/responsive.html/browser/tunnel.js b/devtools/client/responsive.html/browser/tunnel.js
--- a/devtools/client/responsive.html/browser/tunnel.js
+++ b/devtools/client/responsive.html/browser/tunnel.js
@@ -15,18 +15,17 @@ const FRAME_LOADER = Symbol("devtools/re
 // Export for use in tests.
 exports.OUTER_FRAME_LOADER_SYMBOL = FRAME_LOADER;
 
 function debug(msg) {
   // console.log(msg);
 }
 
 /**
- * Properties swapped between browsers by browser.xml's `swapDocShells`.  See also the
- * list at /devtools/client/responsive.html/docs/browser-swap.md.
+ * Properties swapped between browsers by browser.xml's `swapDocShells`.
  */
 const SWAPPED_BROWSER_STATE = [
   "_remoteFinder",
   "_securityUI",
   "_documentURI",
   "_documentContentType",
   "_contentTitle",
   "_characterSet",
diff --git a/devtools/client/responsive.html/docs/browser-swap.md b/devtools/client/responsive.html/docs/browser-swap.md
deleted file mode 100644
--- a/devtools/client/responsive.html/docs/browser-swap.md
+++ /dev/null
@@ -1,144 +0,0 @@
-# Overview
-
-The RDM tool uses several forms of tab and browser swapping to integrate the
-tool UI cleanly into the browser UI.  The high level steps of this process are
-documented at `/devtools/docs/responsive-design-mode.md`.
-
-This document contains a random assortment of low level notes about the steps
-the browser goes through when swapping browsers between tabs.
-
-# Connections between Browsers and Tabs
-
-Link between tab and browser (`gBrowser._linkBrowserToTab`):
-
-```
-aTab.linkedBrowser = browser;
-gBrowser._tabForBrowser.set(browser, aTab);
-```
-
-# Swapping Browsers between Tabs
-
-## Legend
-
-* (R): remote browsers only
-* (!R): non-remote browsers only
-
-## Functions Called
-
-When you call `gBrowser.swapBrowsersAndCloseOther` to move tab content from a
-browser in one tab to a browser in another tab, here are all the code paths
-involved:
-
-* `gBrowser.swapBrowsersAndCloseOther`
-  * `gBrowser._beginRemoveTab`
-    * `gBrowser.tabContainer.updateVisibility`
-    * Emit `TabClose`
-    * `browser.webProgress.removeProgressListener`
-    * `filter.removeProgressListener`
-    * `listener.destroy`
-  * `gBrowser._swapBrowserDocShells`
-    * `ourBrowser.webProgress.removeProgressListener`
-    * `filter.removeProgressListener`
-    * `gBrowser._swapRegisteredOpenURIs`
-    * `ourBrowser.swapDocShells(aOtherBrowser)`
-      * Emit `SwapDocShells`
-      * `PopupNotifications._swapBrowserNotifications`
-      * `browser.detachFormFill` (!R)
-      * `browser.swapFrameLoaders`
-      * `browser.attachFormFill` (!R)
-      * `browser._remoteWebNavigationImpl.swapBrowser(browser)` (R)
-      * `browser._remoteWebProgressManager.swapBrowser(browser)` (R)
-      * `browser._remoteFinder.swapBrowser(browser)` (R)
-      * Emit `EndSwapDocShells`
-    * `gBrowser.mTabProgressListener`
-    * `filter.addProgressListener`
-    * `ourBrowser.webProgress.addProgressListener`
-  * `gBrowser._endRemoveTab`
-    * `gBrowser._blurTab`
-    * `gBrowser._tabFilters.delete`
-    * `gBrowser._tabListeners.delete`
-    * `gBrowser._outerWindowIDBrowserMap.delete`
-    * `browser.destroy`
-    * `gBrowser.tabContainer.removeChild`
-    * `gBrowser.tabContainer.adjustTabstrip`
-    * `gBrowser.tabContainer._setPositionalAttributes`
-    * `browser.parentNode.removeChild(browser)`
-    * `gBrowser._tabForBrowser.delete`
-    * `gBrowser.mPanelContainer.removeChild`
-  * `gBrowser.setTabTitle` / `gBrowser.setTabTitleLoading`
-    * `browser.currentURI.spec`
-    * `gBrowser._tabAttrModified`
-    * `gBrowser.updateTitlebar`
-  * `gBrowser.updateCurrentBrowser`
-    * `browser.docShellIsActive` (!R)
-    * `gBrowser.showTab`
-    * `gBrowser._appendStatusPanel`
-    * `gBrowser._callProgressListeners` with `onLocationChange`
-    * `gBrowser._callProgressListeners` with `onSecurityChange`
-    * `gBrowser._callProgressListeners` with `onUpdateCurrentBrowser`
-    * `gBrowser.updateTitlebar`
-    * `gBrowser._callProgressListeners` with `onStateChange`
-    * `gBrowser._setCloseKeyState`
-    * Emit `TabSelect`
-    * `gBrowser._tabAttrModified`
-    * `browser.getInPermitUnload`
-    * `gBrowser.tabContainer._setPositionalAttributes`
-  * `gBrowser._tabAttrModified`
-
-## Browser State
-
-When calling `gBrowser.swapBrowsersAndCloseOther`, the browser is not actually
-moved from one tab to the other.  Instead, various properties _on_ each of the
-browsers are swapped.
-
-Browser attributes `gBrowser.swapBrowsersAndCloseOther` transfers between
-browsers:
-
-* `usercontextid`
-
-Tab attributes `gBrowser.swapBrowsersAndCloseOther` transfers between tabs:
-
-* `usercontextid`
-* `muted`
-* `soundplaying`
-* `busy`
-
-Browser properties `gBrowser.swapBrowsersAndCloseOther` transfers between
-browsers:
-
-* `mIconURL`
-* `getFindBar(aOurTab)._findField.value`
-
-Browser properties `gBrowser._swapBrowserDocShells` transfers between browsers:
-
-* `outerWindowID` in `gBrowser._outerWindowIDBrowserMap`
-* `_outerWindowID` on the browser (R)
-* `docShellIsActive`
-* `permanentKey`
-* `registeredOpenURI`
-
-Browser properties `browser.swapDocShells` transfers between browsers:
-
-* `_docShell`
-* `_webBrowserFind`
-* `_contentWindow`
-* `_webNavigation`
-* `_remoteWebNavigation` (R)
-* `_remoteWebNavigationImpl` (R)
-* `_remoteWebProgressManager` (R)
-* `_remoteWebProgress` (R)
-* `_remoteFinder` (R)
-* `_securityUI` (R)
-* `_documentURI` (R)
-* `_documentContentType` (R)
-* `_contentTitle` (R)
-* `_characterSet` (R)
-* `_contentPrincipal` (R)
-* `_imageDocument` (R)
-* `_fullZoom` (R)
-* `_textZoom` (R)
-* `_isSyntheticDocument` (R)
-* `_innerWindowID` (R)
-* `_manifestURI` (R)
-
-`browser.swapFrameLoaders` swaps the actual page content.
