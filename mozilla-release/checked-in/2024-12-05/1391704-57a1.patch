# HG changeset patch
# User Florian Queze <florian@queze.net>
# Date 1504219351 -7200
# Node ID d64ad536c15846ee95664e6a8fae10ce064cf118
# Parent  b514a9f75e579924a283bdae815d13d0c78fffd9
Bug 1391704 - Avoid flickering while moving tabs across windows, r=mconley.

diff --git a/browser/base/content/browser.js b/browser/base/content/browser.js
--- a/browser/base/content/browser.js
+++ b/browser/base/content/browser.js
@@ -1336,16 +1336,35 @@ var gBrowserInit = {
         document.documentElement.setAttribute("darkwindowframe", "true");
       }
     }
 
     ToolbarIconColor.init();
 
     gRemoteControl.updateVisualCue(Marionette.running);
 
+    // If we are given a tab to swap in, take care of it before first paint to
+    // avoid an about:blank flash.
+    let tabToOpen = window.arguments && window.arguments[0];
+    if (tabToOpen instanceof XULElement) {
+      // Clear the reference to the tab from the arguments array.
+      window.arguments[0] = null;
+
+      // Stop the about:blank load
+      gBrowser.stop();
+      // make sure it has a docshell
+      gBrowser.docShell;
+
+      try {
+        gBrowser.swapBrowsersAndCloseOther(gBrowser.selectedTab, tabToOpen);
+      } catch (e) {
+        Cu.reportError(e);
+      }
+    }
+
     // Wait until chrome is painted before executing code not critical to making the window visible
     this._boundDelayedStartup = this._delayedStartup.bind(this);
     window.addEventListener("MozAfterPaint", this._boundDelayedStartup);
 
     this._loadHandled = true;
   },
 
   _cancelDelayedStartup() {
@@ -1391,16 +1410,18 @@ var gBrowserInit = {
     });
 
     gBrowser.addEventListener("PermissionStateChange", function() {
       gIdentityHandler.refreshIdentityBlock();
     });
 
     let uriToLoad = this._getUriToLoad();
     if (uriToLoad && uriToLoad != "about:blank") {
+      // We don't check if uriToLoad is a XULElement because this case has
+      // already been handled before first paint, and the argument cleared.
       if (uriToLoad instanceof Ci.nsIArray) {
         let count = uriToLoad.length;
         let specs = [];
         for (let i = 0; i < count; i++) {
           let urisstring = uriToLoad.queryElementAt(i, Ci.nsISupportsString);
           specs.push(urisstring.data);
         }
 
@@ -1408,49 +1429,16 @@ var gBrowserInit = {
         // so that we don't disrupt startup
         try {
           gBrowser.loadTabs(specs, {
             inBackground: false,
             replace: true,
             triggeringPrincipal: Services.scriptSecurityManager.getSystemPrincipal(),
           });
         } catch (e) {}
-      } else if (uriToLoad instanceof XULElement) {
-        // swap the given tab with the default about:blank tab and then close
-        // the original tab in the other window.
-        let tabToOpen = uriToLoad;
-
-        // If this tab was passed as a window argument, clear the
-        // reference to it from the arguments array.
-        if (window.arguments[0] == tabToOpen) {
-          window.arguments[0] = null;
-        }
-
-        // Stop the about:blank load
-        gBrowser.stop();
-        // make sure it has a docshell
-        gBrowser.docShell;
-
-        // We must set usercontextid before updateBrowserRemoteness()
-        // so that the newly created remote tab child has correct usercontextid
-        if (tabToOpen.hasAttribute("usercontextid")) {
-          let usercontextid = tabToOpen.getAttribute("usercontextid");
-          gBrowser.selectedBrowser.setAttribute("usercontextid", usercontextid);
-        }
-
-        try {
-          // Make sure selectedBrowser has the same remote settings as the one
-          // we are swapping in.
-          gBrowser.updateBrowserRemoteness(gBrowser.selectedBrowser,
-                                           tabToOpen.linkedBrowser.isRemoteBrowser,
-                                           { remoteType: tabToOpen.linkedBrowser.remoteType });
-          gBrowser.swapBrowsersAndCloseOther(gBrowser.selectedTab, tabToOpen);
-        } catch (e) {
-          Cu.reportError(e);
-        }
       } else if (window.arguments.length >= 3) {
         // window.arguments[2]: referrer (nsIURI | string)
         //                 [3]: postData (nsIInputStream)
         //                 [4]: allowThirdPartyFixup (bool)
         //                 [5]: referrerPolicy (int)
         //                 [6]: userContextId (int)
         //                 [7]: originPrincipal (nsIPrincipal)
         //                 [8]: triggeringPrincipal (nsIPrincipal)
diff --git a/browser/base/content/tabbrowser.xml b/browser/base/content/tabbrowser.xml
--- a/browser/base/content/tabbrowser.xml
+++ b/browser/base/content/tabbrowser.xml
@@ -3005,16 +3005,26 @@
                 // This call actually closes the window, unless the user
                 // cancels the operation.  We are finished here in both cases.
                 this._windowIsClosing = window.closeWindow(true, window.warnAboutClosingWindow);
                 return false;
               }
 
               newTab = true;
             }
+            aTab._endRemoveArgs = [closeWindow, newTab];
+
+            // swapBrowsersAndCloseOther will take care of closing the window without animation.
+            if (closeWindow && aAdoptedByTab) {
+              // Remove the tab's filter to avoid leaking.
+              if (aTab.linkedPanel) {
+                this._tabFilters.delete(aTab);
+              }
+              return true;
+            }
 
             // Mute audio immediately to improve perceived speed of tab closure.
             if (!aAdoptedByTab && aTab.hasAttribute("soundplaying")) {
               // Don't persist the muted state as this wasn't a user action.
               // This lets undo-close-tab return it to an unmuted state.
               aTab.linkedBrowser.mute(true);
             }
 
@@ -3067,17 +3077,16 @@
 
             // Remove this tab as the owner of any other tabs, since it's going away.
             for (let tab of this.tabs) {
               if ("owner" in tab && tab.owner == aTab)
                 // |tab| is a child of the tab we're removing, make it an orphan
                 tab.owner = null;
             }
 
-            aTab._endRemoveArgs = [closeWindow, newTab];
             return true;
           ]]>
         </body>
       </method>
 
       <method name="_endRemoveTab">
         <parameter name="aTab"/>
         <body>
@@ -3284,16 +3293,35 @@
             }
 
             // First, start teardown of the other browser.  Make sure to not
             // fire the beforeunload event in the process.  Close the other
             // window if this was its last tab.
             if (!remoteBrowser._beginRemoveTab(aOtherTab, aOurTab, true))
               return;
 
+            // If this is the last tab of the window, hide the window
+            // immediately without animation before the docshell swap, to avoid
+            // about:blank being painted.
+            let [closeWindow] = aOtherTab._endRemoveArgs;
+            if (closeWindow) {
+              let win = aOtherTab.ownerGlobal;
+              let dwu = win.QueryInterface(Ci.nsIInterfaceRequestor)
+                           .getInterface(Ci.nsIDOMWindowUtils);
+              dwu.suppressAnimation(true);
+              // Only suppressing window animations isn't enough to avoid
+              // an empty content area being painted.
+              let baseWin = win.QueryInterface(Ci.nsIInterfaceRequestor)
+                               .getInterface(Ci.nsIDocShell)
+                               .QueryInterface(Ci.nsIDocShellTreeItem)
+                               .treeOwner
+                               .QueryInterface(Ci.nsIBaseWindow);
+              baseWin.visibility = false;
+            }
+
             let modifiedAttrs = [];
             if (aOtherTab.hasAttribute("muted")) {
               aOurTab.setAttribute("muted", "true");
               aOurTab.muteReason = aOtherTab.muteReason;
               ourBrowser.mute();
               modifiedAttrs.push("muted");
             }
             if (aOtherTab.hasAttribute("soundplaying")) {
@@ -3350,17 +3378,21 @@
                 otherFindBar.findMode == otherFindBar.FIND_NORMAL) {
               let ourFindBar = this.getFindBar(aOurTab);
               ourFindBar._findField.value = otherFindBar._findField.value;
               if (!otherFindBar.hidden)
                 ourFindBar.onFindCommand();
             }
 
             // Finish tearing down the tab that's going away.
-            remoteBrowser._endRemoveTab(aOtherTab);
+            if (closeWindow) {
+              aOtherTab.ownerGlobal.close();
+            } else {
+              remoteBrowser._endRemoveTab(aOtherTab);
+            }
 
             this.setTabTitle(aOurTab);
 
             // If the tab was already selected (this happpens in the scenario
             // of replaceTabWithWindow), notify onLocationChange, etc.
             if (aOurTab.selected)
               this.updateCurrentBrowser(true);
 
@@ -3716,16 +3748,24 @@
           <![CDATA[
             if (this.tabs.length == 1)
               return null;
 
             var options = "chrome,dialog=no,all";
             for (var name in aOptions)
               options += "," + name + "=" + aOptions[name];
 
+            // Play the tab closing animation to give immediate feedback while
+            // waiting for the new window to appear.
+            // content area when the docshells are swapped.
+            if (this.animationsEnabled) {
+              aTab.style.maxWidth = ""; // ensure that fade-out transition happens
+              aTab.removeAttribute("fadein");
+            }
+
             // tell a new window to take the "dropped" tab
             return window.openDialog(getBrowserURL(), "_blank", options, aTab);
           ]]>
         </body>
       </method>
 
       <method name="moveTabTo">
         <parameter name="aTab"/>
@@ -3825,17 +3865,18 @@
           // Swap the dropped tab with a new one we create and then close
           // it in the other window (making it seem to have moved between
           // windows). We also ensure that the tab we create to swap into has
           // the same remote type and process as the one we're swapping in.
           // This makes sure we don't get a short-lived process for the new tab.
           let linkedBrowser = aTab.linkedBrowser;
           let params = { eventDetail: { adoptedTab: aTab },
                          preferredRemoteType: linkedBrowser.remoteType,
-                         sameProcessAsFrameLoader: linkedBrowser.frameLoader };
+                         sameProcessAsFrameLoader: linkedBrowser.frameLoader,
+                         skipAnimation: true };
           if (aTab.hasAttribute("usercontextid")) {
             // new tab must have the same usercontextid as the old one
             params.userContextId = aTab.getAttribute("usercontextid");
           }
           let newTab = this.addTab("about:blank", params);
           let newBrowser = this.getBrowserForTab(newTab);
 
           // Stop the about:blank load.
@@ -7483,17 +7524,17 @@
         if (this.tabbrowser.tabs.length == 1) {
           // resize _before_ move to ensure the window fits the new screen.  if
           // the window is too large for its screen, the window manager may do
           // automatic repositioning.
           window.resizeTo(winWidth, winHeight);
           window.moveTo(left, top);
           window.focus();
         } else {
-          let props = { screenX: left, screenY: top };
+          let props = { screenX: left, screenY: top, suppressanimation: 1 };
           if (AppConstants.platform != "win") {
             props.outerWidth = winWidth;
             props.outerHeight = winHeight;
           }
           this.tabbrowser.replaceTabWithWindow(draggedTab, props);
         }
         event.stopPropagation();
       ]]></handler>
diff --git a/browser/components/customizableui/content/panelUI.js b/browser/components/customizableui/content/panelUI.js
--- a/browser/components/customizableui/content/panelUI.js
+++ b/browser/components/customizableui/content/panelUI.js
@@ -375,78 +375,80 @@ const PanelUI = {
    * by the user.
    *
    * @param aCustomizing (optional) set to true if this was called while entering
    *        customization mode. If that's the case, we trust that customization
    *        mode will handle calling beginBatchUpdate and endBatchUpdate.
    *
    * @return a Promise that resolves once the panel is ready to roll.
    */
-  ensureReady(aCustomizing = false) {
-    if (this._readyPromise) {
-      return this._readyPromise;
+  async ensureReady(aCustomizing = false) {
+    if (this._isReady) {
+      return;
     }
+
+    await window.delayedStartupPromise;
     this._ensureEventListenersAdded();
     if (gPhotonStructure) {
       this.panel.hidden = false;
-      this._readyPromise = Promise.resolve();
       this._isReady = true;
-      return this._readyPromise;
+      return;
     }
-    this._readyPromise = (async () => {
-      if (!this._initialized) {
-        await new Promise(resolve => {
-          let delayedStartupObserver = (aSubject, aTopic, aData) => {
-            if (aSubject == window) {
-              Services.obs.removeObserver(delayedStartupObserver, "browser-delayed-startup-finished");
-              resolve();
-            }
-          };
-          Services.obs.addObserver(delayedStartupObserver, "browser-delayed-startup-finished");
-        });
-      }
+
+    if (!this._initialized) {
+      await new Promise(resolve => {
+        let delayedStartupObserver = (aSubject, aTopic, aData) => {
+          if (aSubject == window) {
+            Services.obs.removeObserver(delayedStartupObserver,
+                                        "browser-delayed-startup-finished");
+            resolve();
+          }
+        };
+        Services.obs.addObserver(delayedStartupObserver,
+                                 "browser-delayed-startup-finished");
+      });
+    }
 
-      this.contents.setAttributeNS("http://www.w3.org/XML/1998/namespace", "lang",
-                                   getLocale());
-      if (!this._scrollWidth) {
-        // In order to properly center the contents of the panel, while ensuring
-        // that we have enough space on either side to show a scrollbar, we have to
-        // do a bit of hackery. In particular, we calculate a new width for the
-        // scroller, based on the system scrollbar width.
-        this._scrollWidth =
-          (await ScrollbarSampler.getSystemScrollbarWidth()) + "px";
-        let cstyle = window.getComputedStyle(this.scroller);
-        let widthStr = cstyle.width;
-        // Get the calculated padding on the left and right sides of
-        // the scroller too. We'll use that in our final calculation so
-        // that if a scrollbar appears, we don't have the contents right
-        // up against the edge of the scroller.
-        let paddingLeft = cstyle.paddingLeft;
-        let paddingRight = cstyle.paddingRight;
-        let calcStr = [widthStr, this._scrollWidth,
-                       paddingLeft, paddingRight].join(" + ");
-        this.scroller.style.width = "calc(" + calcStr + ")";
+    this.contents.setAttributeNS("http://www.w3.org/XML/1998/namespace", "lang",
+                                 getLocale());
+    if (!this._scrollWidth) {
+      // In order to properly center the contents of the panel, while ensuring
+      // that we have enough space on either side to show a scrollbar, we have
+      // to do a bit of hackery. In particular, we calculate a new width for the
+      // scroller, based on the system scrollbar width.
+      this._scrollWidth =
+        (await ScrollbarSampler.getSystemScrollbarWidth()) + "px";
+      let cstyle = window.getComputedStyle(this.scroller);
+      let widthStr = cstyle.width;
+      // Get the calculated padding on the left and right sides of
+      // the scroller too. We'll use that in our final calculation so
+      // that if a scrollbar appears, we don't have the contents right
+      // up against the edge of the scroller.
+      let paddingLeft = cstyle.paddingLeft;
+      let paddingRight = cstyle.paddingRight;
+      let calcStr = [widthStr, this._scrollWidth,
+                     paddingLeft, paddingRight].join(" + ");
+      this.scroller.style.width = "calc(" + calcStr + ")";
+    }
+
+    if (aCustomizing) {
+      CustomizableUI.registerMenuPanel(this.contents,
+                                       CustomizableUI.AREA_PANEL);
+    } else {
+      this.beginBatchUpdate();
+      try {
+        CustomizableUI.registerMenuPanel(this.contents,
+                                         CustomizableUI.AREA_PANEL);
+      } finally {
+        this.endBatchUpdate();
       }
-
-      if (aCustomizing) {
-        CustomizableUI.registerMenuPanel(this.contents, CustomizableUI.AREA_PANEL);
-      } else {
-        this.beginBatchUpdate();
-        try {
-          CustomizableUI.registerMenuPanel(this.contents, CustomizableUI.AREA_PANEL);
-        } finally {
-          this.endBatchUpdate();
-        }
-      }
-      this._updateQuitTooltip();
-      this.panel.hidden = false;
-      this._isReady = true;
-    })().catch(Cu.reportError);
-
-    return this._readyPromise;
+    }
+    this._updateQuitTooltip();
+    this.panel.hidden = false;
+    this._isReady = true;
   },
 
   /**
    * Switch the panel to the main view if it's not already
    * in that view.
    */
   showMainView() {
     this._ensureEventListenersAdded();
diff --git a/devtools/client/responsive.html/test/browser/browser_exit_button.js b/devtools/client/responsive.html/test/browser/browser_exit_button.js
--- a/devtools/client/responsive.html/test/browser/browser_exit_button.js
+++ b/devtools/client/responsive.html/test/browser/browser_exit_button.js
@@ -21,18 +21,19 @@ add_task(function* () {
   let waitTabIsDetached = Promise.all([
     once(tab, "TabClose"),
     once(tab.linkedBrowser, "SwapDocShells")
   ]);
 
   // Detach the tab with RDM open.
   let newWindow = gBrowser.replaceTabWithWindow(tab);
 
-  // Waiting the tab is detached.
+  // Wait until the tab is detached and the new window is fully initialized.
   yield waitTabIsDetached;
+  yield newWindow.delayedStartupPromise;
 
   // Get the new tab instance.
   tab = newWindow.gBrowser.tabs[0];
 
   // Detaching a tab closes RDM.
   ok(!manager.isActiveForTab(tab),
     "Responsive Design Mode is not active for the tab");
 
diff --git a/dom/base/test/browser_bug1058164.js b/dom/base/test/browser_bug1058164.js
--- a/dom/base/test/browser_bug1058164.js
+++ b/dom/base/test/browser_bug1058164.js
@@ -64,17 +64,17 @@ add_task(async function test_swap_framel
   let firstBrowser = tab.linkedBrowser;
   await BrowserTestUtils.browserLoaded(firstBrowser);
 
   // Swap the browser out to a new window
   let newWindow = gBrowser.replaceTabWithWindow(tab);
 
   // We have to wait for the window to load so we can get the selected browser
   // to listen to.
-  await BrowserTestUtils.waitForEvent(newWindow, "load");
+  await BrowserTestUtils.waitForEvent(newWindow, "DOMContentLoaded");
   let newWindowBrowser = newWindow.gBrowser.selectedBrowser;
 
   // Wait for the expected pagehide and pageshow events on the initial browser
   await prepareForVisibilityEvents(newWindowBrowser, ["pagehide", "pageshow"]);
 
   // Now let's send the browser back to the original window
 
   // First, create a new, empty browser tab to replace the window with
