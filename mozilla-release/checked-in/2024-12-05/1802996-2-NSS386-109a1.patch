# HG changeset patch
# User John Schanck <jschanck@mozilla.com>
# Date 1670532712 0
#      Thu Dec 08 20:51:52 2022 +0000
# Node ID 282f7fc8b7a41a5e9632e882e3c21028d70b0881
# Parent  842a29cc67bb88dfd8a884090a90932a888b5b8e
Bug 1802996 - land NSS NSS_3_86_RTM UPGRADE_NSS_RELEASE, r=bbeurdouche

Differential Revision: https://phabricator.services.mozilla.com/D164263

diff --git a/security/nss/TAG-INFO b/security/nss/TAG-INFO
--- a/security/nss/TAG-INFO
+++ b/security/nss/TAG-INFO
@@ -1,1 +1,1 @@
-NSS_3_86_BETA1
\ No newline at end of file
+NSS_3_86_RTM
\ No newline at end of file
diff --git a/security/nss/coreconf/coreconf.dep b/security/nss/coreconf/coreconf.dep
--- a/security/nss/coreconf/coreconf.dep
+++ b/security/nss/coreconf/coreconf.dep
@@ -5,9 +5,8 @@
 
 /*
  * A dummy header file that is a dependency for all the object files.
  * Used to force a full recompilation of NSS in Mozilla's Tinderbox
  * depend builds.  See comments in rules.mk.
  */
 
 #error "Do not include this header file."
-
diff --git a/security/nss/doc/rst/releases/index.rst b/security/nss/doc/rst/releases/index.rst
--- a/security/nss/doc/rst/releases/index.rst
+++ b/security/nss/doc/rst/releases/index.rst
@@ -3,16 +3,18 @@
 Releases
 ========
 
 .. toctree::
    :maxdepth: 0
    :glob:
    :hidden:
 
+   nss_3_86.rst
+   nss_3_85.rst
    nss_3_84.rst
    nss_3_83.rst
    nss_3_82.rst
    nss_3_81.rst
    nss_3_80.rst
    nss_3_79_2.rst
    nss_3_79_1.rst
    nss_3_79.rst
@@ -38,21 +40,34 @@ Releases
    nss_3_68.rst
    nss_3_67.rst
    nss_3_66.rst
    nss_3_65.rst
    nss_3_64.rst
 
 .. note::
 
-   **NSS 3.84** is the latest version of NSS.
-   Complete release notes are available here: :ref:`mozilla_projects_nss_nss_3_84_release_notes`
+   **NSS 3.86** is the latest version of NSS.
+   Complete release notes are available here: :ref:`mozilla_projects_nss_nss_3_86_release_notes`
 
    **NSS 3.79.2** is the latest ESR version of NSS.
    Complete release notes are available here: :ref:`mozilla_projects_nss_nss_3_79_2_release_notes`
 
 
 .. container::
 
-   Changes in 3.84 included in this release:
+   Changes in 3.86 included in this release:
 
-   - Bug 1791699 - Bump minimum NSPR version to 4.35.
-   - Bug 1792103 - Add a flag to disable building libnssckbi.
+   - Bug 1803190 - conscious language removal in NSS.
+   - Bug 1794506 - Set nssckbi version number to 2.60.
+   - Bug 1803453 - Set CKA_NSS_SERVER_DISTRUST_AFTER and CKA_NSS_EMAIL_DISTRUST_AFTER for 3 TrustCor Root Certificates.
+   - Bug 1799038 - Remove Staat der Nederlanden EV Root CA from NSS.
+   - Bug 1797559 - Remove EC-ACC root cert from NSS.
+   - Bug 1794507 - Remove SwissSign Platinum CA - G2 from NSS.
+   - Bug 1794495 - Remove Network Solutions Certificate Authority.
+   - Bug 1802331 - compress docker image artifact with zstd.
+   - Bug 1799315 - Migrate nss from AWS to GCP.
+   - Bug 1800989 - Enable static builds in the CI.
+   - Bug 1765759 - Removing SAW docker from the NSS build system.
+   - Bug 1783231 - Initialising variables in the rsa blinding code.
+   - Bug 320582 - Implementation of the double-signing of the message for ECDSA.
+   - Bug 1783231 - Adding exponent blinding for RSA.
+
diff --git a/security/nss/doc/rst/releases/nss_3_86.rst b/security/nss/doc/rst/releases/nss_3_86.rst
new file mode 100644
--- /dev/null
+++ b/security/nss/doc/rst/releases/nss_3_86.rst
@@ -0,0 +1,72 @@
+.. _mozilla_projects_nss_nss_3_86_release_notes:
+
+NSS 3.86 release notes
+======================
+
+`Introduction <#introduction>`__
+--------------------------------
+
+.. container::
+
+   Network Security Services (NSS) 3.86 was released on **8 December 2022**.
+
+
+.. _distribution_information:
+
+`Distribution Information <#distribution_information>`__
+--------------------------------------------------------
+
+.. container::
+
+   The HG tag is NSS_3_86_RTM. NSS 3.86 requires NSPR 4.35 or newer.
+
+   NSS 3.86 source distributions are available on ftp.mozilla.org for secure HTTPS download:
+
+   -  Source tarballs:
+      https://ftp.mozilla.org/pub/mozilla.org/security/nss/releases/NSS_3_86_RTM/src/
+
+   Other releases are available :ref:`mozilla_projects_nss_releases`.
+
+.. _changes_in_nss_3.86:
+
+`Changes in NSS 3.86 <#changes_in_nss_3.86>`__
+----------------------------------------------------
+
+.. container::
+
+   - Bug 1803190 - conscious language removal in NSS.
+   - Bug 1794506 - Set nssckbi version number to 2.60.
+   - Bug 1803453 - Set CKA_NSS_SERVER_DISTRUST_AFTER and CKA_NSS_EMAIL_DISTRUST_AFTER for 3 TrustCor Root Certificates.
+   - Bug 1799038 - Remove Staat der Nederlanden EV Root CA from NSS.
+   - Bug 1797559 - Remove EC-ACC root cert from NSS.
+   - Bug 1794507 - Remove SwissSign Platinum CA - G2 from NSS.
+   - Bug 1794495 - Remove Network Solutions Certificate Authority.
+   - Bug 1802331 - compress docker image artifact with zstd.
+   - Bug 1799315 - Migrate nss from AWS to GCP.
+   - Bug 1800989 - Enable static builds in the CI.
+   - Bug 1765759 - Removing SAW docker from the NSS build system.
+   - Bug 1783231 - Initialising variables in the rsa blinding code.
+   - Bug 320582 - Implementation of the double-signing of the message for ECDSA.
+   - Bug 1783231 - Adding exponent blinding for RSA.
+
+
+
+`Compatibility <#compatibility>`__
+----------------------------------
+
+.. container::
+
+   NSS 3.86 shared libraries are backwards-compatible with all older NSS 3.x shared
+   libraries. A program linked with older NSS 3.x shared libraries will work with
+   this new version of the shared libraries without recompiling or
+   relinking. Furthermore, applications that restrict their use of NSS APIs to the
+   functions listed in NSS Public Functions will remain compatible with future
+   versions of the NSS shared libraries.
+
+`Feedback <#feedback>`__
+------------------------
+
+.. container::
+
+   Bugs discovered should be reported by filing a bug report on
+   `bugzilla.mozilla.org <https://bugzilla.mozilla.org/enter_bug.cgi?product=NSS>`__ (product NSS).
diff --git a/security/nss/lib/nss/nss.h b/security/nss/lib/nss/nss.h
--- a/security/nss/lib/nss/nss.h
+++ b/security/nss/lib/nss/nss.h
@@ -17,22 +17,22 @@
 
 /*
  * NSS's major version, minor version, patch level, build number, and whether
  * this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define NSS_VERSION "3.86" _NSS_CUSTOMIZED " Beta"
+#define NSS_VERSION "3.86" _NSS_CUSTOMIZED
 #define NSS_VMAJOR 3
 #define NSS_VMINOR 86
 #define NSS_VPATCH 0
 #define NSS_VBUILD 0
-#define NSS_BETA PR_TRUE
+#define NSS_BETA PR_FALSE
 
 #ifndef RC_INVOKED
 
 #include "seccomon.h"
 
 typedef struct NSSInitParametersStr NSSInitParameters;
 
 /*
diff --git a/security/nss/lib/softoken/softkver.h b/security/nss/lib/softoken/softkver.h
--- a/security/nss/lib/softoken/softkver.h
+++ b/security/nss/lib/softoken/softkver.h
@@ -12,16 +12,16 @@
 
 /*
  * Softoken's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define SOFTOKEN_VERSION "3.86" SOFTOKEN_ECC_STRING " Beta"
+#define SOFTOKEN_VERSION "3.86" SOFTOKEN_ECC_STRING
 #define SOFTOKEN_VMAJOR 3
 #define SOFTOKEN_VMINOR 86
 #define SOFTOKEN_VPATCH 0
 #define SOFTOKEN_VBUILD 0
-#define SOFTOKEN_BETA PR_TRUE
+#define SOFTOKEN_BETA PR_FALSE
 
 #endif /* _SOFTKVER_H_ */
diff --git a/security/nss/lib/util/nssutil.h b/security/nss/lib/util/nssutil.h
--- a/security/nss/lib/util/nssutil.h
+++ b/security/nss/lib/util/nssutil.h
@@ -14,22 +14,22 @@
 
 /*
  * NSS utilities's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <Beta>]"
  */
-#define NSSUTIL_VERSION "3.86 Beta"
+#define NSSUTIL_VERSION "3.86"
 #define NSSUTIL_VMAJOR 3
 #define NSSUTIL_VMINOR 86
 #define NSSUTIL_VPATCH 0
 #define NSSUTIL_VBUILD 0
-#define NSSUTIL_BETA PR_TRUE
+#define NSSUTIL_BETA PR_FALSE
 
 SEC_BEGIN_PROTOS
 
 /*
  * Returns a const string of the UTIL library version.
  */
 extern const char *NSSUTIL_GetVersion(void);
 
