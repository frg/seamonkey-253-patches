# HG changeset patch
# User Daisuke Akatsuka <dakatsuka@mozilla.com>
# Date 1509004523 -32400
# Node ID 623f9e2bd3164c9c6180563b8a2a3f8a894e5d53
# Parent  4d69b46288e48ba37d2d06b82403efb9a522ae30
Bug 1404801 - Part 1: Implement basic flow for animation list. r=gl

MozReview-Commit-ID: 88qdmj2oIoZ

diff --git a/devtools/client/inspector/animation/actions/animations.js b/devtools/client/inspector/animation/actions/animations.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/animation/actions/animations.js
@@ -0,0 +1,19 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+"use strict";
+
+const { UPDATE_ANIMATIONS } = require("./index");
+
+module.exports = {
+  /**
+   * Update the list of animation in the animation inspector.
+   */
+  updateAnimations(animations) {
+    return {
+      type: UPDATE_ANIMATIONS,
+      animations,
+    };
+  }
+};
diff --git a/devtools/client/inspector/animation/actions/index.js b/devtools/client/inspector/animation/actions/index.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/animation/actions/index.js
@@ -0,0 +1,12 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+"use strict";
+
+const { createEnum } = require("devtools/client/shared/enum");
+
+createEnum([
+  // Update the list of animation.
+  "UPDATE_ANIMATIONS",
+], module.exports);
diff --git a/devtools/client/inspector/animation/actions/moz.build b/devtools/client/inspector/animation/actions/moz.build
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/animation/actions/moz.build
@@ -0,0 +1,8 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+DevToolsModules(
+    'animations.js',
+    'index.js'
+)
diff --git a/devtools/client/inspector/animation/animation.js b/devtools/client/inspector/animation/animation.js
--- a/devtools/client/inspector/animation/animation.js
+++ b/devtools/client/inspector/animation/animation.js
@@ -1,21 +1,55 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
-const { createElement } = require("devtools/client/shared/vendor/react");
+const { AnimationsFront } = require("devtools/shared/fronts/animation");
+const { createElement, createFactory } = require("devtools/client/shared/vendor/react");
+const { Provider } = require("devtools/client/shared/vendor/react-redux");
 
-const App = require("./components/App");
+const App = createFactory(require("./components/App"));
+const { updateAnimations } = require("./actions/animations");
 
 class AnimationInspector {
-  constructor() {
+  constructor(inspector) {
+    this.inspector = inspector;
+    this.update = this.update.bind(this);
+
     this.init();
   }
 
   init() {
-    this.provider = createElement(App);
+    const target = this.inspector.target;
+    this.animationsFront = new AnimationsFront(target.client, target.form);
+
+    const provider = createElement(Provider, {
+      id: "newanimationinspector",
+      key: "newanimationinspector",
+      store: this.inspector.store
+    }, App());
+    this.provider = provider;
+
+    this.inspector.selection.on("new-node-front", this.update);
+    this.inspector.sidebar.on("newanimationinspector-selected", this.update);
+  }
+
+  destroy() {
+    this.inspector.selection.off("new-node-front", this.update);
+    this.inspector.sidebar.off("newanimationinspector-selected", this.update);
+
+    this.inspector = null;
+  }
+
+  async update() {
+    const selection = this.inspector.selection;
+    const animations =
+      selection.isConnected() && selection.isElementNode()
+      ? await this.animationsFront.getAnimationPlayersForNode(selection.nodeFront)
+      : [];
+
+    this.inspector.store.dispatch(updateAnimations(animations));
   }
 }
 
 module.exports = AnimationInspector;
diff --git a/devtools/client/inspector/animation/components/App.js b/devtools/client/inspector/animation/components/App.js
--- a/devtools/client/inspector/animation/components/App.js
+++ b/devtools/client/inspector/animation/components/App.js
@@ -1,19 +1,26 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
-const { DOM: dom, PureComponent } = require("devtools/client/shared/vendor/react");
+const { DOM: dom, PropTypes, PureComponent } = require("devtools/client/shared/vendor/react");
+const { connect } = require("devtools/client/shared/vendor/react-redux");
 
 class App extends PureComponent {
+  static get propTypes() {
+    return {
+      animations: PropTypes.arrayOf(PropTypes.object).isRequired,
+    };
+  }
+
   render() {
     return dom.div(
       {
         id: "animation-container"
       }
     );
   }
 }
 
-module.exports = App;
+module.exports = connect(state => state)(App);
diff --git a/devtools/client/inspector/animation/moz.build b/devtools/client/inspector/animation/moz.build
--- a/devtools/client/inspector/animation/moz.build
+++ b/devtools/client/inspector/animation/moz.build
@@ -1,13 +1,15 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 DIRS += [
-    'components'
+    'actions',
+    'components',
+    'reducers'
 ]
 
 BROWSER_CHROME_MANIFESTS += ['test/browser.ini']
 
 DevToolsModules(
     'animation.js'
 )
diff --git a/devtools/client/inspector/animation/reducers/animations.js b/devtools/client/inspector/animation/reducers/animations.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/animation/reducers/animations.js
@@ -0,0 +1,20 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+"use strict";
+
+const { UPDATE_ANIMATIONS } = require("../actions/index");
+
+const INITIAL_ANIMATIONS = [];
+
+const reducers = {
+  [UPDATE_ANIMATIONS](_, { animations }) {
+    return animations;
+  }
+};
+
+module.exports = function (animations = INITIAL_ANIMATIONS, action) {
+  const reducer = reducers[action.type];
+  return reducer ? reducer(animations, action) : animations;
+};
diff --git a/devtools/client/inspector/animation/reducers/moz.build b/devtools/client/inspector/animation/reducers/moz.build
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/animation/reducers/moz.build
@@ -0,0 +1,7 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+DevToolsModules(
+    'animations.js'
+)
diff --git a/devtools/client/inspector/inspector.js b/devtools/client/inspector/inspector.js
--- a/devtools/client/inspector/inspector.js
+++ b/devtools/client/inspector/inspector.js
@@ -717,17 +717,17 @@ Inspector.prototype = {
           {
             props: {
               id: animationId,
               title: animationTitle
             },
             panel: () => {
               const AnimationInspector =
                 this.browserRequire("devtools/client/inspector/animation/animation");
-              this.animationinspector = new AnimationInspector();
+              this.animationinspector = new AnimationInspector(this);
               return this.animationinspector.provider;
             }
           },
           defaultTab == animationId);
       } else {
         this.sidebar.addFrameTab(
           "animationinspector",
           animationTitle,
@@ -1160,16 +1160,20 @@ Inspector.prototype = {
     if (this.layoutview) {
       this.layoutview.destroy();
     }
 
     if (this.fontinspector) {
       this.fontinspector.destroy();
     }
 
+    if (this.animationinspector) {
+      this.animationinspector.destroy();
+    }
+
     let cssPropertiesDestroyer = this._cssPropertiesLoaded.then(({front}) => {
       if (front) {
         front.destroy();
       }
     });
 
     this.sidebar.off("select", this.onSidebarSelect);
     let sidebarDestroyer = this.sidebar.destroy();
diff --git a/devtools/client/inspector/reducers.js b/devtools/client/inspector/reducers.js
--- a/devtools/client/inspector/reducers.js
+++ b/devtools/client/inspector/reducers.js
@@ -2,16 +2,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 // This file exposes the Redux reducers of the box model, grid and grid highlighter
 // settings.
 
+exports.animations = require("devtools/client/inspector/animation/reducers/animations");
 exports.boxModel = require("devtools/client/inspector/boxmodel/reducers/box-model");
 exports.changes = require("devtools/client/inspector/changes/reducers/changes");
 exports.events = require("devtools/client/inspector/events/reducers/events");
 exports.extensionsSidebar = require("devtools/client/inspector/extensions/reducers/sidebar");
 exports.flexboxes = require("devtools/client/inspector/flexbox/reducers/flexboxes");
 exports.fontOptions = require("devtools/client/inspector/fonts/reducers/font-options");
 exports.fonts = require("devtools/client/inspector/fonts/reducers/fonts");
 exports.grids = require("devtools/client/inspector/grids/reducers/grids");
