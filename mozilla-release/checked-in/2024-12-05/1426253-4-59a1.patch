# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1513785199 18000
# Node ID 661c1ca56b09907f19299040eb7baa6209728997
# Parent  55791f68a19027ab685b1dbb0a6902f29a37f129
Bug 1426253 P4 Assert that ClientSource::SetController() is never called on a client in private browsing mode. r=asuth

diff --git a/dom/clients/manager/ClientInfo.cpp b/dom/clients/manager/ClientInfo.cpp
--- a/dom/clients/manager/ClientInfo.cpp
+++ b/dom/clients/manager/ClientInfo.cpp
@@ -6,16 +6,18 @@
 
 #include "ClientInfo.h"
 
 #include "mozilla/dom/ClientIPCTypes.h"
 
 namespace mozilla {
 namespace dom {
 
+using mozilla::ipc::PrincipalInfo;
+
 ClientInfo::ClientInfo(const nsID& aId,
                        ClientType aType,
                        const mozilla::ipc::PrincipalInfo& aPrincipalInfo,
                        const TimeStamp& aCreationTime)
   : mData(MakeUnique<IPCClientInfo>(aId, aType, aPrincipalInfo, aCreationTime,
                                     EmptyCString(),
                                     mozilla::dom::FrameType::None))
 {
@@ -105,10 +107,36 @@ ClientInfo::SetFrameType(mozilla::dom::F
 }
 
 const IPCClientInfo&
 ClientInfo::ToIPC() const
 {
   return *mData;
 }
 
+bool
+ClientInfo::IsPrivateBrowsing() const
+{
+  switch(PrincipalInfo().type()) {
+    case PrincipalInfo::TContentPrincipalInfo:
+    {
+      auto& p = PrincipalInfo().get_ContentPrincipalInfo();
+      return p.attrs().mPrivateBrowsingId != 0;
+    }
+    case PrincipalInfo::TSystemPrincipalInfo:
+    {
+      return false;
+    }
+    case PrincipalInfo::TNullPrincipalInfo:
+    {
+      auto& p = PrincipalInfo().get_NullPrincipalInfo();
+      return p.attrs().mPrivateBrowsingId != 0;
+    }
+    default:
+    {
+      // clients should never be expanded principals
+      MOZ_CRASH("unexpected principal type!");
+    }
+  }
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/clients/manager/ClientInfo.h b/dom/clients/manager/ClientInfo.h
--- a/dom/clients/manager/ClientInfo.h
+++ b/dom/clients/manager/ClientInfo.h
@@ -86,14 +86,18 @@ public:
   // Set the frame type for the global.  This should only happen once the
   // global has become execution ready.
   void
   SetFrameType(mozilla::dom::FrameType aFrameType);
 
   // Convert to the ipdl generated type.
   const IPCClientInfo&
   ToIPC() const;
+
+  // Determine if the client is in private browsing mode.
+  bool
+  IsPrivateBrowsing() const;
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // _mozilla_dom_ClientInfo_h
diff --git a/dom/clients/manager/ClientSource.cpp b/dom/clients/manager/ClientSource.cpp
--- a/dom/clients/manager/ClientSource.cpp
+++ b/dom/clients/manager/ClientSource.cpp
@@ -350,16 +350,21 @@ ClientSource::WorkerSyncPing(WorkerPriva
   GetActor()->SendWorkerSyncPing();
 }
 
 void
 ClientSource::SetController(const ServiceWorkerDescriptor& aServiceWorker)
 {
   NS_ASSERT_OWNINGTHREAD(ClientSource);
 
+  // A client in private browsing mode should never be controlled by
+  // a service worker.  The principal origin attributes should guarantee
+  // this invariant.
+  MOZ_DIAGNOSTIC_ASSERT(!mClientInfo.IsPrivateBrowsing());
+
   if (mController.isSome() && mController.ref() == aServiceWorker) {
     return;
   }
 
   mController.reset();
   mController.emplace(aServiceWorker);
 
   RefPtr<ServiceWorkerContainer> swc;
