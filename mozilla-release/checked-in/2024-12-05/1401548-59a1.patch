# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1511968290 -3600
# Node ID 897dda0b93d8e34d32bf607749414285cb912be9
# Parent  be75de14bbc7d1ede4889eb19473aa7f39cb5695
Bug 1401548 - rename and enable browser_bug_869003_inspect_cross_domain_object.js;r=nchevobbe

MozReview-Commit-ID: 6RnfOEsLQW1

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -80,18 +80,16 @@ support-files =
   test-bug-766001-console-log.js
   test-bug-766001-js-console-links.html
   test-bug-766001-js-errors.js
   test-bug-782653-css-errors-1.css
   test-bug-782653-css-errors-2.css
   test-bug-782653-css-errors.html
   test-bug-837351-security-errors.html
   test-bug-859170-longstring-hang.html
-  test-bug-869003-iframe.html
-  test-bug-869003-top-window.html
   test-bug-952277-highlight-nodes-in-vview.html
   test-iframe-child.html
   test-iframe-parent.html
   test-certificate-messages.html
   test-closure-optimized-out.html
   test-closures.html
   test-console-api-stackframe.html
   test-console-clear.html
@@ -127,16 +125,18 @@ support-files =
   test-filter.html
   test-for-of.html
   test-iframe-762593-insecure-form-action.html
   test-iframe-762593-insecure-frame.html
   test-iframe1.html
   test-iframe2.html
   test-iframe3.html
   test-image.png
+  test-inspect-cross-domain-objects-frame.html
+  test-inspect-cross-domain-objects-top.html
   test-jsterm-dollar.html
   test-location-debugger-link-console-log.js
   test-location-debugger-link-errors.js
   test-location-debugger-link.html
   test-location-styleeditor-link-1.css
   test-location-styleeditor-link-2.css
   test-location-styleeditor-link.html
   test-mixedcontent-securityerrors.html
@@ -314,17 +314,16 @@ skip-if = true #       Bug 1404371
 skip-if = true #       Bug 1405343
 [browser_webconsole_input_focus.js]
 [browser_webconsole_insecure_passwords_about_blank_web_console_warning.js]
 skip-if = true #       Bug 1404884
 [browser_webconsole_insecure_passwords_web_console_warning.js]
 skip-if = true #       Bug 1404888
 # old console skip-if = true # Bug 1110500 - mouse event failure in test
 [browser_webconsole_inspect_cross_domain_object.js]
-skip-if = true #       Bug 1401548
 [browser_webconsole_iterators_generators.js]
 skip-if = true #       Bug 1404849
 # old console skip-if = e10s # Bug 1042253 - webconsole tests disabled with e10s
 [browser_webconsole_js_input_expansion.js]
 [browser_webconsole_jsterm.js]
 skip-if = true #       Bug 1405352
 # old console skip-if = e10s # Bug 1042253 - webconsole e10s tests (Linux debug timeout)
 [browser_webconsole_jsterm_copy.js]
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_inspect_cross_domain_object.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_inspect_cross_domain_object.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_inspect_cross_domain_object.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_inspect_cross_domain_object.js
@@ -4,80 +4,72 @@
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
 // Check that users can inspect objects logged from cross-domain iframes -
 // bug 869003.
 
 "use strict";
 
 const TEST_URI = "http://example.com/browser/devtools/client/webconsole/" +
-                 "test/test-bug-869003-top-window.html";
+                 "new-console-output/test/mochitest/" +
+                 "test-inspect-cross-domain-objects-top.html";
 
-add_task(function* () {
-  // This test is slightly more involved: it opens the web console, then the
-  // variables view for a given object, it updates a property in the view and
-  // checks the result. We can get a timeout with debug builds on slower
-  // machines.
+add_task(async function () {
   requestLongerTimeout(2);
 
-  yield loadTab("data:text/html;charset=utf8,<p>hello");
-  let hud = yield openConsole();
+  let hud = await openNewTabAndConsole("data:text/html;charset=utf8,<p>hello");
+
+  info("Wait for the 'foobar' message to be logged by the frame");
+  let onMessage = waitForMessage(hud, "foobar");
+  BrowserTestUtils.loadURI(gBrowser.selectedBrowser, TEST_URI);
+  let {node} = await onMessage;
+
+  const objectInspectors = [...node.querySelectorAll(".tree")];
+  is(objectInspectors.length, 2, "There is the expected number of object inspectors");
+
+  const [oi1, oi2] = objectInspectors;
 
-  BrowserTestUtils.loadURI(gBrowser.selectedBrowser, TEST_URI);
+  info("Expanding the first object inspector");
+  await expandObjectInspector(oi1);
+
+  // The first object inspector now looks like:
+  // ▼ {…}
+  // |  bug: 869003
+  // |  hello: "world!"
+  // |  ▶︎ __proto__: Object { … }
+
+  let oi1Nodes = oi1.querySelectorAll(".node");
+  is(oi1Nodes.length, 4, "There is the expected number of nodes in the tree");
+  ok(oi1.textContent.includes("bug: 869003"), "Expected content");
+  ok(oi1.textContent.includes('hello: "world!"'), "Expected content");
 
-  let [result] = yield waitForMessages({
-    webconsole: hud,
-    messages: [{
-      name: "console.log message",
-      text: "foobar",
-      category: CATEGORY_WEBDEV,
-      severity: SEVERITY_LOG,
-      objects: true,
-    }],
+  info("Expanding the second object inspector");
+  await expandObjectInspector(oi2);
+
+  // The second object inspector now looks like:
+  // ▼ func()
+  // |  arguments: null
+  // |  bug: 869003
+  // |  caller: null
+  // |  hello: "world!"
+  // |  length: 1
+  // |  name: "func"
+  // |  ▶︎ prototype: Object { … }
+  // |  ▶︎ __proto__: function ()
+
+  let oi2Nodes = oi2.querySelectorAll(".node");
+  is(oi2Nodes.length, 9, "There is the expected number of nodes in the tree");
+  ok(oi2.textContent.includes("arguments: null"), "Expected content");
+  ok(oi2.textContent.includes("bug: 869003"), "Expected content");
+  ok(oi2.textContent.includes("caller: null"), "Expected content");
+  ok(oi2.textContent.includes('hello: "world!"'), "Expected content");
+  ok(oi2.textContent.includes("length: 1"), "Expected content");
+  ok(oi2.textContent.includes('name: "func"'), "Expected content");
+});
+
+function expandObjectInspector(oi) {
+  let onMutation = waitForNodeMutation(oi, {
+    childList: true
   });
 
-  let msg = [...result.matched][0];
-  ok(msg, "message element");
-
-  let body = msg.querySelector(".message-body");
-  ok(body, "message body");
-  ok(body.textContent.includes('{ hello: "world!",'), "message text check");
-  ok(body.textContent.includes('function func()'), "message text check");
-
-  yield testClickable(result.clickableElements[0], [
-    { name: "hello", value: "world!" },
-    { name: "bug", value: 869003 },
-  ], hud);
-  yield testClickable(result.clickableElements[1], [
-    { name: "hello", value: "world!" },
-    { name: "name", value: "func" },
-    { name: "length", value: 1 },
-  ], hud);
-});
-
-function* testClickable(clickable, props, hud) {
-  ok(clickable, "clickable object found");
-
-  executeSoon(() => {
-    EventUtils.synthesizeMouse(clickable, 2, 2, {}, hud.iframeWindow);
-  });
-
-  let aVar = yield hud.jsterm.once("variablesview-fetched");
-  ok(aVar, "variables view fetched");
-  ok(aVar._variablesView, "variables view object");
-
-  let [result] = yield findVariableViewProperties(aVar, props, { webconsole: hud });
-  let prop = result.matchedProp;
-  ok(prop, "matched the |" + props[0].name + "| property in the variables view");
-
-  // Check that property value updates work.
-  aVar = yield updateVariablesViewProperty({
-    property: prop,
-    field: "value",
-    string: "'omgtest'",
-    webconsole: hud,
-  });
-
-  info("onFetchAfterUpdate");
-
-  props[0].value = "omgtest";
-  yield findVariableViewProperties(aVar, props, { webconsole: hud });
+  oi.querySelector(".arrow").click();
+  return onMutation;
 }
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/test-bug-869003-iframe.html b/devtools/client/webconsole/new-console-output/test/mochitest/test-inspect-cross-domain-objects-frame.html
rename from devtools/client/webconsole/new-console-output/test/mochitest/test-bug-869003-iframe.html
rename to devtools/client/webconsole/new-console-output/test/mochitest/test-inspect-cross-domain-objects-frame.html
--- a/devtools/client/webconsole/new-console-output/test/mochitest/test-bug-869003-iframe.html
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/test-inspect-cross-domain-objects-frame.html
@@ -3,19 +3,19 @@
   <head>
     <meta charset="utf-8">
     <title>Web Console test for bug 869003</title>
     <!-- Any copyright is dedicated to the Public Domain.
        - http://creativecommons.org/publicdomain/zero/1.0/ -->
     <script type="text/javascript"><!--
       window.onload = function testConsoleLogging()
       {
-        var o = { hello: "world!", bug: 869003 };
-        var f = Object.assign(function func(arg){}, o);
-        console.log("foobar", o, f);
+        var obj1 = { hello: "world!", bug: 869003 };
+        var obj2 = Object.assign(function func(arg){}, obj1);
+        console.log("foobar", obj1, obj2);
       };
     // --></script>
   </head>
   <body>
     <p>Make sure users can inspect objects from cross-domain iframes.</p>
     <p>Iframe window.</p>
   </body>
 </html>
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/test-bug-869003-top-window.html b/devtools/client/webconsole/new-console-output/test/mochitest/test-inspect-cross-domain-objects-top.html
rename from devtools/client/webconsole/new-console-output/test/mochitest/test-bug-869003-top-window.html
rename to devtools/client/webconsole/new-console-output/test/mochitest/test-inspect-cross-domain-objects-top.html
--- a/devtools/client/webconsole/new-console-output/test/mochitest/test-bug-869003-top-window.html
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/test-inspect-cross-domain-objects-top.html
@@ -4,11 +4,11 @@
     <meta charset="utf-8">
     <title>Web Console test for bug 869003</title>
     <!-- Any copyright is dedicated to the Public Domain.
        - http://creativecommons.org/publicdomain/zero/1.0/ -->
   </head>
   <body>
     <p>Make sure users can inspect objects from cross-domain iframes.</p>
     <p>Top window.</p>
-    <iframe src="http://example.org/browser/devtools/client/webconsole/test/test-bug-869003-iframe.html"></iframe>
+    <iframe src="http://example.org/browser/devtools/client/webconsole/new-console-output/test/mochitest/test-inspect-cross-domain-objects-frame.html"></iframe>
   </body>
 </html>
