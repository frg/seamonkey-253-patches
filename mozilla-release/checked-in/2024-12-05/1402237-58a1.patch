# HG changeset patch
# User Brian Grinstead <bgrinstead@mozilla.com>
# Date 1506703817 25200
# Node ID 7b96d8f2636bf5ad1104e273335a3a18d38c6794
# Parent  a791d6a58dee2b998461dd20b57b91ceed39db2a
Bug 1402237 - Always scroll to bottom when the first messages are rendered;r=nchevobbe

MozReview-Commit-ID: B3YY3Tvwk5A

diff --git a/devtools/client/webconsole/new-console-output/components/console-output.js b/devtools/client/webconsole/new-console-output/components/console-output.js
--- a/devtools/client/webconsole/new-console-output/components/console-output.js
+++ b/devtools/client/webconsole/new-console-output/components/console-output.js
@@ -52,16 +52,20 @@ const ConsoleOutput = createClass({
       scrollToBottom(this.outputNode);
     }, 0);
     this.props.serviceContainer.attachRefToHud("outputScroller", this.outputNode);
   },
 
   componentWillUpdate(nextProps, nextState) {
     const outputNode = this.outputNode;
     if (!outputNode || !outputNode.lastChild) {
+      // Force a scroll to bottom when messages are added to an empty console.
+      // This makes the console stay pinned to the bottom if a batch of messages
+      // are added after a page refresh (Bug 1402237).
+      this.shouldScrollBottom = true;
       return;
     }
 
     const lastChild = outputNode.lastChild;
     const visibleMessagesDelta =
       nextProps.visibleMessages.length - this.props.visibleMessages.length;
     const messagesDelta =
       nextProps.messages.size - this.props.messages.size;
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_scroll.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_scroll.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_scroll.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_scroll.js
@@ -1,69 +1,79 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
-const TEST_URI = "data:text/html;charset=utf-8,<p>Web Console test for " +
-                 "scroll behavior";
+const TEST_URI =
+`data:text/html;charset=utf-8,<p>Web Console test for  scroll.</p>
+  <script>
+  for (let i = 0; i < 100; i++) {
+    console.log("init-" + i);
+  }
+  </script>
+`;
 add_task(async function () {
   const hud = await openNewTabAndConsole(TEST_URI);
   let {ui} = hud;
+  const outputContainer = ui.outputNode.querySelector(".webconsole-output");
 
-  info("Waiting for logged messages");
+  info("Console should be scrolled to bottom on initial load from page logs");
+  await waitFor(() => findMessage(hud, "init-99"));
+  ok(hasVerticalOverflow(outputContainer), "There is a vertical overflow");
+  ok(isScrolledToBottom(outputContainer), "The console is scrolled to the bottom");
 
-  let receievedMessages = waitForMessages({hud, messages: [{
-    text: "init-99"
-  }]});
-  ContentTask.spawn(gBrowser.selectedBrowser, {}, function () {
-    for (let i = 0; i < 100; i++) {
-      content.wrappedJSObject.console.log("init-" + i);
-    }
-  });
-  await receievedMessages;
+  await refreshTab();
 
-  const outputContainer = ui.outputNode.querySelector(".webconsole-output");
-  ok(outputContainer.scrollHeight > outputContainer.clientHeight,
-    "There is a vertical overflow");
+  info("Console should be scrolled to bottom after refresh from page logs");
+  await waitFor(() => findMessage(hud, "init-99"));
+  ok(hasVerticalOverflow(outputContainer), "There is a vertical overflow");
+  ok(isScrolledToBottom(outputContainer), "The console is scrolled to the bottom");
 
   info("Scroll up");
   outputContainer.scrollTop = 0;
 
   info("Add a message to check that the scroll isn't impacted");
-  receievedMessages = waitForMessages({hud, messages: [{
+  let receievedMessages = waitForMessages({hud, messages: [{
     text: "stay"
   }]});
   ContentTask.spawn(gBrowser.selectedBrowser, {}, function () {
     content.wrappedJSObject.console.log("stay");
   });
   await receievedMessages;
+  ok(hasVerticalOverflow(outputContainer), "There is a vertical overflow");
   is(outputContainer.scrollTop, 0, "The console stayed scrolled to the top");
 
   info("Evaluate a command to check that the console scrolls to the bottom");
   receievedMessages = waitForMessages({hud, messages: [{
     text: "42"
   }]});
   ui.jsterm.execute("21 + 21");
   await receievedMessages;
-  is(isScrolledToBottom(outputContainer), true, "The console is scrolled to the bottom");
+  ok(hasVerticalOverflow(outputContainer), "There is a vertical overflow");
+  ok(isScrolledToBottom(outputContainer), "The console is scrolled to the bottom");
 
   info("Add a message to check that the console do scroll since we're at the bottom");
   receievedMessages = waitForMessages({hud, messages: [{
     text: "scroll"
   }]});
   ContentTask.spawn(gBrowser.selectedBrowser, {}, function () {
     content.wrappedJSObject.console.log("scroll");
   });
   await receievedMessages;
-  is(isScrolledToBottom(outputContainer), true, "The console is scrolled to the bottom");
+  ok(hasVerticalOverflow(outputContainer), "There is a vertical overflow");
+  ok(isScrolledToBottom(outputContainer), "The console is scrolled to the bottom");
 });
 
+function hasVerticalOverflow(container) {
+  return container.scrollHeight > container.clientHeight;
+}
+
 function isScrolledToBottom(container) {
   if (!container.lastChild) {
     return true;
   }
   let lastNodeHeight = container.lastChild.clientHeight;
   return container.scrollTop + container.clientHeight >=
          container.scrollHeight - lastNodeHeight / 2;
 }
