# HG changeset patch
# User Tim Nguyen <ntim.bugs@gmail.com>
# Date 1510314927 0
# Node ID c05d4e399a3736387d52c731dc6d69ba2431b79e
# Parent  b57162c5ecfd0fce73aa588d7066ffe48e675adc
Bug 1416194 - Reduce Toolbar component updates in netmonitor. r=gasolin

MozReview-Commit-ID: Apwj73aeC0U

diff --git a/devtools/client/netmonitor/src/components/Toolbar.js b/devtools/client/netmonitor/src/components/Toolbar.js
--- a/devtools/client/netmonitor/src/components/Toolbar.js
+++ b/devtools/client/netmonitor/src/components/Toolbar.js
@@ -7,22 +7,22 @@
 const Services = require("Services");
 const {
   Component,
   createFactory,
   DOM,
   PropTypes,
 } = require("devtools/client/shared/vendor/react");
 const { connect } = require("devtools/client/shared/vendor/react-redux");
+const I = require("devtools/client/shared/vendor/immutable");
 
 const Actions = require("../actions/index");
 const { FILTER_SEARCH_DELAY, FILTER_TAGS } = require("../constants");
 const {
   getRecordingState,
-  getRequestFilterTypes,
   getTypeFilteredRequests,
   isNetworkDetailsToggleButtonDisabled,
 } = require("../selectors/index");
 
 const { autocompleteProvider } = require("../utils/filter-autocomplete-provider");
 const { L10N } = require("../utils/l10n");
 
 // Components
@@ -57,17 +57,17 @@ const DISABLE_CACHE_LABEL = L10N.getStr(
  * as well as set of filters for filtering the content.
  */
 class Toolbar extends Component {
   static get propTypes() {
     return {
       toggleRecording: PropTypes.func.isRequired,
       recording: PropTypes.bool.isRequired,
       clearRequests: PropTypes.func.isRequired,
-      requestFilterTypes: PropTypes.array.isRequired,
+      requestFilterTypes: PropTypes.object.isRequired,
       setRequestFilterText: PropTypes.func.isRequired,
       networkDetailsToggleDisabled: PropTypes.bool.isRequired,
       networkDetailsOpen: PropTypes.bool.isRequired,
       toggleNetworkDetails: PropTypes.func.isRequired,
       enablePersistentLogs: PropTypes.func.isRequired,
       togglePersistentLogs: PropTypes.func.isRequired,
       persistentLogsEnabled: PropTypes.bool.isRequired,
       disableBrowserCache: PropTypes.func.isRequired,
@@ -88,16 +88,28 @@ class Toolbar extends Component {
 
   componentDidMount() {
     Services.prefs.addObserver(DEVTOOLS_ENABLE_PERSISTENT_LOG_PREF,
                                this.updatePersistentLogsEnabled);
     Services.prefs.addObserver(DEVTOOLS_DISABLE_CACHE_PREF,
                                this.updateBrowserCacheDisabled);
   }
 
+  shouldComponentUpdate(nextProps) {
+    return this.props.networkDetailsOpen !== nextProps.networkDetailsOpen
+    || this.props.networkDetailsToggleDisabled !== nextProps.networkDetailsToggleDisabled
+    || this.props.persistentLogsEnabled !== nextProps.persistentLogsEnabled
+    || this.props.browserCacheDisabled !== nextProps.browserCacheDisabled
+    || this.props.recording !== nextProps.recording
+    || !I.is(this.props.requestFilterTypes, nextProps.requestFilterTypes)
+
+    // Filtered requests are useful only when searchbox is focused
+    || this.refs.searchbox && this.refs.searchbox.focused;
+  }
+
   componentWillUnmount() {
     Services.prefs.removeObserver(DEVTOOLS_ENABLE_PERSISTENT_LOG_PREF,
                                   this.updatePersistentLogsEnabled);
     Services.prefs.removeObserver(DEVTOOLS_DISABLE_CACHE_PREF,
                                   this.updateBrowserCacheDisabled);
   }
 
   toggleRequestFilterType(evt) {
@@ -133,17 +145,17 @@ class Toolbar extends Component {
       togglePersistentLogs,
       persistentLogsEnabled,
       toggleBrowserCache,
       browserCacheDisabled,
       recording,
     } = this.props;
 
     // Render list of filter-buttons.
-    let buttons = requestFilterTypes.map(([type, checked]) => {
+    let buttons = requestFilterTypes.entrySeq().toArray().map(([type, checked]) => {
       let classList = ["devtools-button", `requests-list-filter-${type}-button`];
       checked && classList.push("checked");
 
       return (
         button({
           className: classList.join(" "),
           key: type,
           onClick: this.toggleRequestFilterType,
@@ -222,16 +234,17 @@ class Toolbar extends Component {
           ),
         ),
         span({ className: "devtools-toolbar-group" },
           SearchBox({
             delay: FILTER_SEARCH_DELAY,
             keyShortcut: SEARCH_KEY_SHORTCUT,
             placeholder: SEARCH_PLACE_HOLDER,
             type: "filter",
+            ref: "searchbox",
             onChange: setRequestFilterText,
             autocompleteProvider: this.autocompleteProvider,
           }),
           button({
             className: toggleDetailButtonClass,
             title: toggleDetailButtonTitle,
             disabled: networkDetailsToggleDisabled,
             tabIndex: "0",
@@ -246,17 +259,17 @@ class Toolbar extends Component {
 module.exports = connect(
   (state) => ({
     browserCacheDisabled: state.ui.browserCacheDisabled,
     filteredRequests: getTypeFilteredRequests(state),
     networkDetailsToggleDisabled: isNetworkDetailsToggleButtonDisabled(state),
     networkDetailsOpen: state.ui.networkDetailsOpen,
     persistentLogsEnabled: state.ui.persistentLogsEnabled,
     recording: getRecordingState(state),
-    requestFilterTypes: getRequestFilterTypes(state),
+    requestFilterTypes: state.filters.requestFilterTypes,
   }),
   (dispatch) => ({
     clearRequests: () => dispatch(Actions.clearRequests()),
     disableBrowserCache: (disabled) => dispatch(Actions.disableBrowserCache(disabled)),
     enablePersistentLogs: (enabled) => dispatch(Actions.enablePersistentLogs(enabled)),
     setRequestFilterText: (text) => dispatch(Actions.setRequestFilterText(text)),
     toggleBrowserCache: () => dispatch(Actions.toggleBrowserCache()),
     toggleNetworkDetails: () => dispatch(Actions.toggleNetworkDetails()),
diff --git a/devtools/client/netmonitor/src/middleware/prefs.js b/devtools/client/netmonitor/src/middleware/prefs.js
--- a/devtools/client/netmonitor/src/middleware/prefs.js
+++ b/devtools/client/netmonitor/src/middleware/prefs.js
@@ -8,30 +8,30 @@ const Services = require("Services");
 const {
   ENABLE_REQUEST_FILTER_TYPE_ONLY,
   RESET_COLUMNS,
   TOGGLE_COLUMN,
   TOGGLE_REQUEST_FILTER_TYPE,
   ENABLE_PERSISTENT_LOGS,
   DISABLE_BROWSER_CACHE,
 } = require("../constants");
-const { getRequestFilterTypes } = require("../selectors/index");
 
 /**
   * Update the relevant prefs when:
   *   - a column has been toggled
   *   - a filter type has been set
   */
 function prefsMiddleware(store) {
   return next => action => {
     const res = next(action);
     switch (action.type) {
       case ENABLE_REQUEST_FILTER_TYPE_ONLY:
       case TOGGLE_REQUEST_FILTER_TYPE:
-        let filters = getRequestFilterTypes(store.getState())
+        let filters = store.getState().filters.requestFilterTypes
+          .entrySeq().toArray()
           .filter(([type, check]) => check)
           .map(([type, check]) => type);
         Services.prefs.setCharPref(
           "devtools.netmonitor.filters", JSON.stringify(filters));
         break;
       case ENABLE_PERSISTENT_LOGS:
         Services.prefs.setBoolPref(
           "devtools.netmonitor.persistlog", store.getState().ui.persistentLogsEnabled);
diff --git a/devtools/client/netmonitor/src/selectors/filters.js b/devtools/client/netmonitor/src/selectors/filters.js
deleted file mode 100644
--- a/devtools/client/netmonitor/src/selectors/filters.js
+++ /dev/null
@@ -1,13 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-"use strict";
-
-function getRequestFilterTypes(state) {
-  return state.filters.requestFilterTypes.entrySeq().toArray();
-}
-
-module.exports = {
-  getRequestFilterTypes,
-};
diff --git a/devtools/client/netmonitor/src/selectors/index.js b/devtools/client/netmonitor/src/selectors/index.js
--- a/devtools/client/netmonitor/src/selectors/index.js
+++ b/devtools/client/netmonitor/src/selectors/index.js
@@ -1,17 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
-const filters = require("./filters");
 const requests = require("./requests");
 const timingMarkers = require("./timing-markers");
 const ui = require("./ui");
 
 Object.assign(exports,
-  filters,
   requests,
   timingMarkers,
   ui
 );
diff --git a/devtools/client/netmonitor/src/selectors/moz.build b/devtools/client/netmonitor/src/selectors/moz.build
--- a/devtools/client/netmonitor/src/selectors/moz.build
+++ b/devtools/client/netmonitor/src/selectors/moz.build
@@ -1,11 +1,10 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 DevToolsModules(
-    'filters.js',
     'index.js',
     'requests.js',
     'timing-markers.js',
     'ui.js',
 )
diff --git a/devtools/client/netmonitor/test/browser_net_prefs-reload.js b/devtools/client/netmonitor/test/browser_net_prefs-reload.js
--- a/devtools/client/netmonitor/test/browser_net_prefs-reload.js
+++ b/devtools/client/netmonitor/test/browser_net_prefs-reload.js
@@ -4,18 +4,16 @@
 "use strict";
 
 /**
  * Tests if the prefs that should survive across tool reloads work.
  */
 
 add_task(function* () {
   let { monitor } = yield initNetMonitor(SIMPLE_URL);
-  let { getRequestFilterTypes } = monitor.panelWin
-    .windowRequire("devtools/client/netmonitor/src/selectors/index");
   let Actions = monitor.panelWin
     .windowRequire("devtools/client/netmonitor/src/actions/index");
   info("Starting test... ");
 
   // This test reopens the network monitor a bunch of times, for different
   // hosts (bottom, side, window). This seems to be slow on debug builds.
   requestLongerTimeout(3);
 
@@ -29,17 +27,18 @@ add_task(function* () {
   let getState = () => getStore().getState();
 
   let prefsToCheck = {
     filters: {
       // A custom new value to be used for the verified preference.
       newValue: ["html", "css"],
       // Getter used to retrieve the current value from the frontend, in order
       // to verify that the pref was applied properly.
-      validateValue: () => getRequestFilterTypes(getState())
+      validateValue: () => getState().filters.requestFilterTypes
+        .entrySeq().toArray()
         .filter(([type, check]) => check)
         .map(([type, check]) => type),
       // Predicate used to modify the frontend when setting the new pref value,
       // before trying to validate the changes.
       modifyFrontend: (value) => value.forEach(e =>
         getStore().dispatch(Actions.toggleRequestFilterType(e)))
     },
     networkDetailsWidth: {
