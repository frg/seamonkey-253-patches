# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1508836082 -7200
# Node ID dfc3f11935aa57dbb750007a59c1758933cb957e
# Parent  a6330864a975d71e8fa0b69c2e8d97ecc177e4ae
Bug 1404850 - Fix self-XSS protection in new console and enable test; r=Honza.

The self-XSS protection was broken since the switch to an HTML document. We
went from using the XUL <notificationbox> tag to a simple div.
But notificationbox has method that a simple div has not, and when trying
to display the warning message, there was an error in terminal.
This patch uses the NotificationBox React component and create it when needed.
It also rename and enable the test that ensures that XSS is working.

MozReview-Commit-ID: BhxIqf7gzAG

diff --git a/devtools/client/shared/components/NotificationBox.js b/devtools/client/shared/components/NotificationBox.js
--- a/devtools/client/shared/components/NotificationBox.js
+++ b/devtools/client/shared/components/NotificationBox.js
@@ -129,17 +129,17 @@ class NotificationBox extends Component 
     }
 
     let notifications = this.state.notifications.set(value, {
       label: label,
       value: value,
       image: image,
       priority: priority,
       type: type,
-      buttons: buttons,
+      buttons: Array.isArray(buttons) ? buttons : [],
       eventCallback: eventCallback,
     });
 
     // High priorities must be on top.
     notifications = notifications.sortBy((val, key) => {
       return -val.priority;
     });
 
@@ -147,17 +147,19 @@ class NotificationBox extends Component 
       notifications: notifications
     });
   }
 
   /**
    * Remove specific notification from the list.
    */
   removeNotification(notification) {
-    this.close(this.state.notifications.get(notification.value));
+    if (notification) {
+      this.close(this.state.notifications.get(notification.value));
+    }
   }
 
   /**
    * Returns an object that represents a notification. It can be
    * used to close it.
    */
   getNotificationWithValue(value) {
     let notification = this.state.notifications.get(value);
@@ -224,16 +226,17 @@ class NotificationBox extends Component 
   /**
    * Render a notification.
    */
   renderNotification(notification) {
     return (
       div({
         key: notification.value,
         className: "notification",
+        "data-key": notification.value,
         "data-type": notification.type},
         div({className: "notificationInner"},
           div({className: "details"},
             div({
               className: "messageImage",
               "data-type": notification.type}),
             span({className: "messageText"},
               notification.label
diff --git a/devtools/client/webconsole/jsterm.js b/devtools/client/webconsole/jsterm.js
--- a/devtools/client/webconsole/jsterm.js
+++ b/devtools/client/webconsole/jsterm.js
@@ -21,16 +21,18 @@ loader.lazyRequireGetter(this, "Autocomp
 loader.lazyRequireGetter(this, "ToolSidebar", "devtools/client/framework/sidebar", true);
 loader.lazyRequireGetter(this, "Messages", "devtools/client/webconsole/console-output", true);
 loader.lazyRequireGetter(this, "asyncStorage", "devtools/shared/async-storage");
 loader.lazyRequireGetter(this, "EnvironmentClient", "devtools/shared/client/environment-client");
 loader.lazyRequireGetter(this, "ObjectClient", "devtools/shared/client/object-client");
 loader.lazyImporter(this, "VariablesView", "resource://devtools/client/shared/widgets/VariablesView.jsm");
 loader.lazyImporter(this, "VariablesViewController", "resource://devtools/client/shared/widgets/VariablesViewController.jsm");
 loader.lazyRequireGetter(this, "gDevTools", "devtools/client/framework/devtools", true);
+loader.lazyRequireGetter(this, "NotificationBox", "devtools/client/shared/components/NotificationBox", true);
+loader.lazyRequireGetter(this, "PriorityLevels", "devtools/client/shared/components/NotificationBox", true);
 
 const l10n = require("devtools/client/webconsole/webconsole-l10n");
 
 // Constants used for defining the direction of JSTerm input history navigation.
 const HISTORY_BACK = -1;
 const HISTORY_FORWARD = 1;
 
 const XHTML_NS = "http://www.w3.org/1999/xhtml";
@@ -264,19 +266,18 @@ JSTerm.prototype = {
     this._updateCharSize();
 
     if (this.hud.isBrowserConsole &&
         !Services.prefs.getBoolPref("devtools.chrome.enabled")) {
       inputContainer.style.display = "none";
     } else {
       let okstring = l10n.getStr("selfxss.okstring");
       let msg = l10n.getFormatStr("selfxss.msg", [okstring]);
-      this._onPaste = WebConsoleUtils.pasteHandlerGen(
-        this.inputNode, doc.getElementById("webconsole-notificationbox"),
-        msg, okstring);
+      this._onPaste = WebConsoleUtils.pasteHandlerGen(this.inputNode,
+          this.getNotificationBox(), msg, okstring);
       this.inputNode.addEventListener("keypress", this._keyPress);
       this.inputNode.addEventListener("paste", this._onPaste);
       this.inputNode.addEventListener("drop", this._onPaste);
       this.inputNode.addEventListener("input", this._inputEventHandler);
       this.inputNode.addEventListener("keyup", this._inputEventHandler);
       this.inputNode.addEventListener("focus", this._focusEventHandler);
     }
 
@@ -1744,16 +1745,40 @@ JSTerm.prototype = {
     tempLabel.remove();
     // Calculate the width of the chevron placed at the beginning of the input
     // box. Remove 4 more pixels to accomodate the padding of the popup.
     this._chevronWidth = +doc.defaultView.getComputedStyle(this.inputNode)
                              .paddingLeft.replace(/[^0-9.]/g, "") - 4;
   },
 
   /**
+   * Build the notification box as soon as needed.
+   */
+  getNotificationBox: function () {
+    if (this._notificationBox) {
+      return this._notificationBox;
+    }
+
+    let box = this.hud.document.getElementById("webconsole-notificationbox");
+    if (box.tagName === "notificationbox") {
+      // Here we are in the old console frontend (e.g. browser console), so we
+      // can directly return the notificationbox.
+      return box;
+    }
+
+    let toolbox = gDevTools.getToolbox(this.hud.owner.target);
+
+    // Render NotificationBox and assign priority levels to it.
+    this._notificationBox = Object.assign(
+      toolbox.ReactDOM.render(toolbox.React.createElement(NotificationBox), box),
+      PriorityLevels);
+    return this._notificationBox;
+  },
+
+  /**
    * Destroy the sidebar.
    * @private
    */
   _sidebarDestroy: function () {
     if (this._variablesView) {
       this._variablesView.controller.releaseActors();
       this._variablesView = null;
     }
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -209,25 +209,23 @@ skip-if = true #       Bug 1403188
 [browser_jsterm_dollar.js]
 [browser_jsterm_history_persist.js]
 [browser_jsterm_inspect.js]
 [browser_jsterm_no_autocompletion_on_defined_variables.js]
 [browser_jsterm_no_input_and_tab_key_pressed.js]
 [browser_jsterm_no_input_change_and_tab_key_pressed.js]
 [browser_jsterm_popup_close_on_tab_switch.js]
 [browser_jsterm_popup.js]
+[browser_jsterm_selfxss.js]
+subsuite = clipboard
 [browser_netmonitor_shows_reqs_in_webconsole.js]
 [browser_webconsole_allow_mixedcontent_securityerrors.js]
 tags = mcb
 skip-if = true #       Bug 1403452
 # old console skip-if = (os == 'win' && bits == 64) # Bug 1390001
-[browser_webconsole_autocomplete_and_selfxss.js]
-subsuite = clipboard
-skip-if = true #       Bug 1404850
-# old console skip-if = (os == 'linux' && bits == 32 && debug) # bug 1328915, disable linux32 debug devtools for timeouts
 [browser_webconsole_autocomplete_crossdomain_iframe.js]
 skip-if = true # Bug 1408919
 [browser_webconsole_autocomplete_in_debugger_stackframe.js]
 skip-if = true # Bug 1408920
 [browser_webconsole_batching.js]
 [browser_webconsole_block_mixedcontent_securityerrors.js]
 tags = mcb
 skip-if = true #       Bug 1403899
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_autocomplete_and_selfxss.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_jsterm_selfxss.js
rename from devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_autocomplete_and_selfxss.js
rename to devtools/client/webconsole/new-console-output/test/mochitest/browser_jsterm_selfxss.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_autocomplete_and_selfxss.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_jsterm_selfxss.js
@@ -1,130 +1,100 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
-const TEST_URI = "data:text/html;charset=utf-8,<p>test for bug 642615";
-
-XPCOMUtils.defineLazyServiceGetter(this, "clipboardHelper",
-                                   "@mozilla.org/widget/clipboardhelper;1",
-                                   "nsIClipboardHelper");
-var WebConsoleUtils = require("devtools/client/webconsole/utils").Utils;
-
-add_task(function* () {
-  yield loadTab(TEST_URI);
-
-  let hud = yield openConsole();
+const TEST_URI = "data:text/html;charset=utf-8,<p>test for bug 642615 & 994134</p>";
 
-  yield consoleOpened(hud);
-});
-
-function consoleOpened(HUD) {
-  let deferred = defer();
+XPCOMUtils.defineLazyServiceGetter(
+  this,
+  "clipboardHelper",
+  "@mozilla.org/widget/clipboardhelper;1",
+  "nsIClipboardHelper"
+);
+const WebConsoleUtils = require("devtools/client/webconsole/utils").Utils;
+const stringToCopy = "foobazbarBug642615";
 
-  let jsterm = HUD.jsterm;
-  let stringToCopy = "foobazbarBug642615";
-
+add_task(async function () {
+  let {jsterm} = await openNewTabAndConsole(TEST_URI);
   jsterm.clearOutput();
-
   ok(!jsterm.completeNode.value, "no completeNode.value");
 
   jsterm.setInputValue("doc");
 
-  let completionValue;
-
-  // wait for key "u"
-  function onCompletionValue() {
-    completionValue = jsterm.completeNode.value;
+  info("wait for completion value after typing 'docu'");
+  let onAutocompleteUpdated = jsterm.once("autocomplete-updated");
+  EventUtils.synthesizeKey("u", {});
+  await onAutocompleteUpdated;
 
-    // Arguments: expected, setup, success, failure.
-    waitForClipboard(
-      stringToCopy,
-      function () {
-        clipboardHelper.copyString(stringToCopy);
-      },
-      onClipboardCopy,
-      finishTest);
-  }
+  const completionValue = jsterm.completeNode.value;
 
-  function onClipboardCopy() {
-    testSelfXss();
+  // Arguments: expected, setup.
+  await waitForClipboardPromise(() =>
+    clipboardHelper.copyString(stringToCopy), stringToCopy);
+
+  await testSelfXss(jsterm);
 
-    jsterm.setInputValue("docu");
-    info("wait for completion update after clipboard paste");
-    updateEditUIVisibility();
-    jsterm.once("autocomplete-updated", onClipboardPaste);
-    goDoCommand("cmd_paste");
-  }
+  jsterm.setInputValue("docu");
+  info("wait for completion update after clipboard paste");
+  updateEditUIVisibility();
+  onAutocompleteUpdated = jsterm.once("autocomplete-updated");
+  goDoCommand("cmd_paste");
+
+  await onAutocompleteUpdated;
+
+  ok(!jsterm.completeNode.value, "no completion value after paste");
 
-  // Self xss prevention tests (bug 994134)
-  function testSelfXss() {
-    info("Self-xss paste tests");
-    WebConsoleUtils.usageCount = 0;
-    is(WebConsoleUtils.usageCount, 0, "Test for usage count getter");
-    // Input some commands to check if usage counting is working
-    for (let i = 0; i <= 3; i++) {
-      jsterm.setInputValue(i);
-      jsterm.execute();
-    }
-    is(WebConsoleUtils.usageCount, 4, "Usage count incremented");
-    WebConsoleUtils.usageCount = 0;
-    updateEditUIVisibility();
+  info("wait for completion update after undo");
+  onAutocompleteUpdated = jsterm.once("autocomplete-updated");
+
+  goDoCommand("cmd_undo");
+
+  await onAutocompleteUpdated;
 
-    let oldVal = jsterm.getInputValue();
-    goDoCommand("cmd_paste");
-    let notificationbox = jsterm.hud.document.getElementById("webconsole-notificationbox");
-    let notification = notificationbox.getNotificationWithValue("selfxss-notification");
-    ok(notification, "Self-xss notification shown");
-    is(oldVal, jsterm.getInputValue(), "Paste blocked by self-xss prevention");
+  is(jsterm.completeNode.value, completionValue, "same completeNode.value after undo");
+
+  info("wait for completion update after clipboard paste (ctrl-v)");
+  onAutocompleteUpdated = jsterm.once("autocomplete-updated");
+
+  EventUtils.synthesizeKey("v", {accelKey: true});
 
-    // Allow pasting
-    jsterm.setInputValue("allow pasting");
-    let evt = document.createEvent("KeyboardEvent");
-    evt.initKeyEvent("keyup", true, true, window,
-                     0, 0, 0, 0,
-                     0, " ".charCodeAt(0));
-    jsterm.inputNode.dispatchEvent(evt);
-    jsterm.setInputValue("");
-    goDoCommand("cmd_paste");
-    isnot("", jsterm.getInputValue(), "Paste works");
+  await onAutocompleteUpdated;
+  ok(!jsterm.completeNode.value, "no completion value after paste (ctrl-v)");
+});
+
+// Self xss prevention tests (bug 994134)
+async function testSelfXss(jsterm) {
+  info("Self-xss paste tests");
+  WebConsoleUtils.usageCount = 0;
+  is(WebConsoleUtils.usageCount, 0, "Test for usage count getter");
+  // Input some commands to check if usage counting is working
+  for (let i = 0; i <= 3; i++) {
+    jsterm.setInputValue(i);
+    jsterm.execute();
   }
-  function onClipboardPaste() {
-    ok(!jsterm.completeNode.value, "no completion value after paste");
-
-    info("wait for completion update after undo");
-    jsterm.once("autocomplete-updated", onCompletionValueAfterUndo);
-
-    // Get out of the webconsole event loop.
-    executeSoon(() => {
-      goDoCommand("cmd_undo");
-    });
-  }
+  is(WebConsoleUtils.usageCount, 4, "Usage count incremented");
+  WebConsoleUtils.usageCount = 0;
+  updateEditUIVisibility();
 
-  function onCompletionValueAfterUndo() {
-    is(jsterm.completeNode.value, completionValue,
-       "same completeNode.value after undo");
+  let oldVal = jsterm.getInputValue();
+  goDoCommand("cmd_paste");
 
-    info("wait for completion update after clipboard paste (ctrl-v)");
-    jsterm.once("autocomplete-updated", () => {
-      ok(!jsterm.completeNode.value,
-         "no completion value after paste (ctrl-v)");
-
-      // using executeSoon() to get out of the webconsole event loop.
-      executeSoon(deferred.resolve);
-    });
+  let notificationbox = jsterm.hud.document.getElementById("webconsole-notificationbox");
+  let notification = notificationbox.querySelector(".notification");
+  is(notification.getAttribute("data-key"), "selfxss-notification",
+    "Self-xss notification shown");
+  is(oldVal, jsterm.getInputValue(), "Paste blocked by self-xss prevention");
 
-    // Get out of the webconsole event loop.
-    executeSoon(() => {
-      EventUtils.synthesizeKey("v", {accelKey: true});
-    });
-  }
-
-  info("wait for completion value after typing 'docu'");
-  jsterm.once("autocomplete-updated", onCompletionValue);
-
-  EventUtils.synthesizeKey("u", {});
-
-  return deferred.promise;
+  // Allow pasting
+  jsterm.setInputValue("allow pasting");
+  let evt = document.createEvent("KeyboardEvent");
+  evt.initKeyEvent("keyup", true, true, window,
+                    0, 0, 0, 0,
+                    0, " ".charCodeAt(0));
+  jsterm.inputNode.dispatchEvent(evt);
+  jsterm.setInputValue("");
+  goDoCommand("cmd_paste");
+  is(stringToCopy, jsterm.getInputValue(), "Paste works");
 }
diff --git a/devtools/client/webconsole/webconsole.html b/devtools/client/webconsole/webconsole.html
--- a/devtools/client/webconsole/webconsole.html
+++ b/devtools/client/webconsole/webconsole.html
@@ -7,33 +7,33 @@
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
     <link rel="stylesheet" href="chrome://devtools/skin/widgets.css"/>
     <link rel="stylesheet" href="resource://devtools/client/themes/light-theme.css"/>
     <link rel="stylesheet" href="chrome://devtools/skin/webconsole.css"/>
     <link rel="stylesheet" href="chrome://devtools/skin/components-frame.css"/>
     <link rel="stylesheet" href="resource://devtools/client/shared/components/reps/reps.css"/>
     <link rel="stylesheet" href="resource://devtools/client/shared/components/tabs/Tabs.css"/>
     <link rel="stylesheet" href="resource://devtools/client/shared/components/tabs/TabBar.css"/>
+    <link rel="stylesheet" href="resource://devtools/client/shared/components/NotificationBox.css"/>
     <link rel="stylesheet" href="chrome://devtools/content/netmonitor/src/assets/styles/netmonitor.css"/>
 
     <script src="chrome://devtools/content/shared/theme-switching.js"></script>
     <script type="application/javascript"
             src="resource://devtools/client/webconsole/new-console-output/main.js"></script>
   </head>
   <body class="theme-sidebar" role="application">
     <div id="app-wrapper" class="theme-body">
       <div id="output-container" role="document" aria-live="polite"></div>
       <div id="jsterm-wrapper">
-        <div id="webconsole-notificationbox">
-          <div class="jsterm-input-container" style="direction:ltr">
-            <div class="jsterm-stack-node">
-              <textarea class="jsterm-complete-node devtools-monospace"
-                       tabindex="-1"></textarea>
-              <textarea class="jsterm-input-node devtools-monospace"
-                       rows="1" tabindex="0"
-                       aria-autocomplete="list"></textarea>
-            </div>
+        <div id="webconsole-notificationbox"></div>
+        <div class="jsterm-input-container" style="direction:ltr">
+          <div class="jsterm-stack-node">
+            <textarea class="jsterm-complete-node devtools-monospace"
+                      tabindex="-1"></textarea>
+            <textarea class="jsterm-input-node devtools-monospace"
+                      rows="1" tabindex="0"
+                      aria-autocomplete="list"></textarea>
           </div>
         </div>
       </div>
     </div>
   </body>
 </html>
