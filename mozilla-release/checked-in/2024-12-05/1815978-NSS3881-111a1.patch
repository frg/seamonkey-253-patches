# HG changeset patch
# User John Schanck <jschanck@mozilla.com>
# Date 1675980586 0
#      Thu Feb 09 22:09:46 2023 +0000
# Node ID 560eb41fa7969bcd476f35a8953219a77aeaf9fd
# Parent  d5722560ec795436a52524f486a516f3e5e6fb7a
Bug 1815978 - land NSS NSS_3_88_1_RTM UPGRADE_NSS_RELEASE, r=keeler

2023-02-09  John M. Schanck  <jschanck@mozilla.com>

	* doc/rst/releases/nss_3_88_1.rst:
	Documentation: release notes for NSS 3.88.1
	[35df50f6b615] [NSS_3_88_1_RTM] <NSS_3_88_1_BRANCH>

	* lib/nss/nss.h, lib/softoken/softkver.h, lib/util/nssutil.h:
	Set version numbers to 3.88.1 final
	[3f59586e1b37] <NSS_3_88_1_BRANCH>

	* lib/pkcs12/p12d.c, lib/pkcs12/p12t.h, lib/pkcs12/p12tmpl.c:
	Bug 1804640 - improve handling of unknown PKCS#12 safe bag types.
	r=rrelyea

	[684586ec163a] <NSS_3_88_1_BRANCH>

2023-02-09  Anna Weine  <anna.weine@mozilla.com>

	* .hgtags:
	Added tag NSS_3_88_RTM for changeset da9f14b8cc9d
	[58d7a8a55aea] <NSS_3_88_BRANCH>

Differential Revision: https://phabricator.services.mozilla.com/D169387

diff --git a/security/nss/TAG-INFO b/security/nss/TAG-INFO
--- a/security/nss/TAG-INFO
+++ b/security/nss/TAG-INFO
@@ -1,1 +1,1 @@
-NSS_3_88_RTM
\ No newline at end of file
+NSS_3_88_1_RTM
\ No newline at end of file
diff --git a/security/nss/coreconf/coreconf.dep b/security/nss/coreconf/coreconf.dep
--- a/security/nss/coreconf/coreconf.dep
+++ b/security/nss/coreconf/coreconf.dep
@@ -5,8 +5,9 @@
 
 /*
  * A dummy header file that is a dependency for all the object files.
  * Used to force a full recompilation of NSS in Mozilla's Tinderbox
  * depend builds.  See comments in rules.mk.
  */
 
 #error "Do not include this header file."
+
diff --git a/security/nss/doc/rst/releases/nss_3_88_1.rst b/security/nss/doc/rst/releases/nss_3_88_1.rst
new file mode 100644
--- /dev/null
+++ b/security/nss/doc/rst/releases/nss_3_88_1.rst
@@ -0,0 +1,58 @@
+.. _mozilla_projects_nss_nss_3_88_1_release_notes:
+
+NSS 3.88.1 release notes
+======================
+
+`Introduction <#introduction>`__
+--------------------------------
+
+.. container::
+
+   Network Security Services (NSS) 3.88.1 was released on *9 February 2023**.
+
+
+.. _distribution_information:
+
+`Distribution Information <#distribution_information>`__
+--------------------------------------------------------
+
+.. container::
+
+   The HG tag is NSS_3_88_1_RTM. NSS 3.88.1 requires NSPR 4.35 or newer.
+
+   NSS 3.88.1 source distributions are available on ftp.mozilla.org for secure HTTPS download:
+
+   -  Source tarballs:
+      https://ftp.mozilla.org/pub/mozilla.org/security/nss/releases/NSS_3_88_1_RTM/src/
+
+   Other releases are available :ref:`mozilla_projects_nss_releases`.
+
+.. _changes_in_nss_3.88.1:
+
+`Changes in NSS 3.88.1 <#changes_in_nss_3.88.1>`__
+----------------------------------------------------
+
+.. container::
+
+   - Bug 1804640 - improve handling of unknown PKCS#12 safe bag types.
+
+
+`Compatibility <#compatibility>`__
+----------------------------------
+
+.. container::
+
+   NSS 3.88.1 shared libraries are backwards-compatible with all older NSS 3.x shared
+   libraries. A program linked with older NSS 3.x shared libraries will work with
+   this new version of the shared libraries without recompiling or
+   relinking. Furthermore, applications that restrict their use of NSS APIs to the
+   functions listed in NSS Public Functions will remain compatible with future
+   versions of the NSS shared libraries.
+
+`Feedback <#feedback>`__
+------------------------
+
+.. container::
+
+   Bugs discovered should be reported by filing a bug report on
+   `bugzilla.mozilla.org <https://bugzilla.mozilla.org/enter_bug.cgi?product=NSS>`__ (product NSS).
diff --git a/security/nss/lib/nss/nss.h b/security/nss/lib/nss/nss.h
--- a/security/nss/lib/nss/nss.h
+++ b/security/nss/lib/nss/nss.h
@@ -17,20 +17,20 @@
 
 /*
  * NSS's major version, minor version, patch level, build number, and whether
  * this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define NSS_VERSION "3.88" _NSS_CUSTOMIZED
+#define NSS_VERSION "3.88.1" _NSS_CUSTOMIZED
 #define NSS_VMAJOR 3
 #define NSS_VMINOR 88
-#define NSS_VPATCH 0
+#define NSS_VPATCH 1
 #define NSS_VBUILD 0
 #define NSS_BETA PR_FALSE
 
 #ifndef RC_INVOKED
 
 #include "seccomon.h"
 
 typedef struct NSSInitParametersStr NSSInitParameters;
diff --git a/security/nss/lib/pkcs12/p12d.c b/security/nss/lib/pkcs12/p12d.c
--- a/security/nss/lib/pkcs12/p12d.c
+++ b/security/nss/lib/pkcs12/p12d.c
@@ -332,41 +332,48 @@ sec_pkcs12_decoder_safe_bag_update(void 
                                    unsigned long len, int depth,
                                    SEC_ASN1EncodingPart data_kind)
 {
     sec_PKCS12SafeContentsContext *safeContentsCtx =
         (sec_PKCS12SafeContentsContext *)arg;
     SEC_PKCS12DecoderContext *p12dcx;
     SECStatus rv;
 
-    /* make sure that we are not skipping the current safeBag,
-     * and that there are no errors.  If so, just return rather
-     * than continuing to process.
-     */
-    if (!safeContentsCtx || !safeContentsCtx->p12dcx ||
-        safeContentsCtx->p12dcx->error || safeContentsCtx->skipCurrentSafeBag) {
+    if (!safeContentsCtx || !safeContentsCtx->p12dcx || !safeContentsCtx->currentSafeBagA1Dcx) {
         return;
     }
     p12dcx = safeContentsCtx->p12dcx;
 
+    /* make sure that there are no errors and we are not skipping the current safeBag */
+    if (p12dcx->error || safeContentsCtx->skipCurrentSafeBag) {
+        goto loser;
+    }
+
     rv = SEC_ASN1DecoderUpdate(safeContentsCtx->currentSafeBagA1Dcx, data, len);
     if (rv != SECSuccess) {
         p12dcx->errorValue = PORT_GetError();
+        p12dcx->error = PR_TRUE;
+        goto loser;
+    }
+
+    /* The update may have set safeContentsCtx->skipCurrentSafeBag, and we
+     * may not get another opportunity to clean up the decoder context.
+     */
+    if (safeContentsCtx->skipCurrentSafeBag) {
         goto loser;
     }
 
     return;
 
 loser:
-    /* set the error, and finish the decoder context.  because there
+    /* Finish the decoder context. Because there
      * is not a way of returning an error message, it may be worth
      * while to do a check higher up and finish any decoding contexts
      * that are still open.
      */
-    p12dcx->error = PR_TRUE;
     SEC_ASN1DecoderFinish(safeContentsCtx->currentSafeBagA1Dcx);
     safeContentsCtx->currentSafeBagA1Dcx = NULL;
     return;
 }
 
 /* notify function for decoding safeBags.  This function is
  * used to filter safeBag types which are not supported,
  * initiate the decoding of nested safe contents, and decode
diff --git a/security/nss/lib/pkcs12/p12t.h b/security/nss/lib/pkcs12/p12t.h
--- a/security/nss/lib/pkcs12/p12t.h
+++ b/security/nss/lib/pkcs12/p12t.h
@@ -68,16 +68,17 @@ struct sec_PKCS12SafeBagStr {
     /* Dependent upon the type of bag being used. */
     union {
         SECKEYPrivateKeyInfo *pkcs8KeyBag;
         SECKEYEncryptedPrivateKeyInfo *pkcs8ShroudedKeyBag;
         sec_PKCS12CertBag *certBag;
         sec_PKCS12CRLBag *crlBag;
         sec_PKCS12SecretBag *secretBag;
         sec_PKCS12SafeContents *safeContents;
+        SECItem *unknownBag;
     } safeBagContent;
 
     sec_PKCS12Attribute **attribs;
 
     /* used locally */
     SECOidData *bagTypeTag;
     PLArenaPool *arena;
     unsigned int nAttribs;
diff --git a/security/nss/lib/pkcs12/p12tmpl.c b/security/nss/lib/pkcs12/p12tmpl.c
--- a/security/nss/lib/pkcs12/p12tmpl.c
+++ b/security/nss/lib/pkcs12/p12tmpl.c
@@ -25,22 +25,22 @@ sec_pkcs12_choose_safe_bag_type(void *sr
     if (src_or_dest == NULL) {
         return NULL;
     }
 
     safeBag = (sec_PKCS12SafeBag *)src_or_dest;
 
     oiddata = SECOID_FindOID(&safeBag->safeBagType);
     if (oiddata == NULL) {
-        return SEC_ASN1_GET(SEC_AnyTemplate);
+        return SEC_ASN1_GET(SEC_PointerToAnyTemplate);
     }
 
     switch (oiddata->offset) {
         default:
-            theTemplate = SEC_ASN1_GET(SEC_AnyTemplate);
+            theTemplate = SEC_ASN1_GET(SEC_PointerToAnyTemplate);
             break;
         case SEC_OID_PKCS12_V1_KEY_BAG_ID:
             theTemplate = SEC_ASN1_GET(SECKEY_PointerToPrivateKeyInfoTemplate);
             break;
         case SEC_OID_PKCS12_V1_CERT_BAG_ID:
             theTemplate = sec_PKCS12PointerToCertBagTemplate;
             break;
         case SEC_OID_PKCS12_V1_CRL_BAG_ID:
diff --git a/security/nss/lib/softoken/softkver.h b/security/nss/lib/softoken/softkver.h
--- a/security/nss/lib/softoken/softkver.h
+++ b/security/nss/lib/softoken/softkver.h
@@ -12,16 +12,16 @@
 
 /*
  * Softoken's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define SOFTOKEN_VERSION "3.88" SOFTOKEN_ECC_STRING
+#define SOFTOKEN_VERSION "3.88.1" SOFTOKEN_ECC_STRING
 #define SOFTOKEN_VMAJOR 3
 #define SOFTOKEN_VMINOR 88
-#define SOFTOKEN_VPATCH 0
+#define SOFTOKEN_VPATCH 1
 #define SOFTOKEN_VBUILD 0
 #define SOFTOKEN_BETA PR_FALSE
 
 #endif /* _SOFTKVER_H_ */
diff --git a/security/nss/lib/util/nssutil.h b/security/nss/lib/util/nssutil.h
--- a/security/nss/lib/util/nssutil.h
+++ b/security/nss/lib/util/nssutil.h
@@ -14,20 +14,20 @@
 
 /*
  * NSS utilities's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <Beta>]"
  */
-#define NSSUTIL_VERSION "3.88"
+#define NSSUTIL_VERSION "3.88.1"
 #define NSSUTIL_VMAJOR 3
 #define NSSUTIL_VMINOR 88
-#define NSSUTIL_VPATCH 0
+#define NSSUTIL_VPATCH 1
 #define NSSUTIL_VBUILD 0
 #define NSSUTIL_BETA PR_FALSE
 
 SEC_BEGIN_PROTOS
 
 /*
  * Returns a const string of the UTIL library version.
  */
