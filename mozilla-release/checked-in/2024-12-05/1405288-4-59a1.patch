# HG changeset patch
# User Patrick Brosset <pbrosset@mozilla.com>
# Date 1508404857 -7200
# Node ID a13174ac2d9586ea4d7d0ea56d815c25e940f95a
# Parent  9301eb288270e9b326eba695552ca9366150f8f6
Bug 1405288 - Remove pickColorFromPage actor method check from then inspector r=ochameau

This method was added in bug 1262439 last year, and shipped with FF50.
We support all the way back to the latest ESR (52 now).
So let's remove the backward compat code for this method.

MozReview-Commit-ID: LUL7FFWWC5M

diff --git a/devtools/client/inspector/inspector.js b/devtools/client/inspector/inspector.js
--- a/devtools/client/inspector/inspector.js
+++ b/devtools/client/inspector/inspector.js
@@ -833,30 +833,28 @@ Inspector.prototype = {
    *
    * @return {Boolean} true if the eyedropper highlighter is supported by the current
    *         document.
    */
   supportsEyeDropper: Task.async(function* () {
     try {
       let hasSupportsHighlighters =
         yield this.target.actorHasMethod("inspector", "supportsHighlighters");
-      let hasPickColorFromPage =
-        yield this.target.actorHasMethod("inspector", "pickColorFromPage");
 
       let supportsHighlighters;
       if (hasSupportsHighlighters) {
         supportsHighlighters = yield this.inspector.supportsHighlighters();
       } else {
         // If the actor does not provide the supportsHighlighter method, fallback to
         // check if the selected node's document is a HTML document.
         let { nodeFront } = this.selection;
         supportsHighlighters = nodeFront && nodeFront.isInHTMLDocument;
       }
 
-      return supportsHighlighters && hasPickColorFromPage;
+      return supportsHighlighters;
     } catch (e) {
       console.error(e);
       return false;
     }
   }),
 
   setupToolbar: Task.async(function* () {
     this.teardownToolbar();
@@ -1683,18 +1681,17 @@ Inspector.prototype = {
                          .catch(console.error);
   },
 
   /**
    * Hide the eyedropper.
    * @return {Promise} resolves when the eyedropper is hidden.
    */
   hideEyeDropper: function () {
-    // The eyedropper button doesn't exist, most probably because the actor doesn't
-    // support the pickColorFromPage, or because the page isn't HTML.
+    // The eyedropper button doesn't exist, most probably  because the page isn't HTML.
     if (!this.eyeDropperButton) {
       return null;
     }
 
     this.eyeDropperButton.classList.remove("checked");
     this.stopEyeDropperListeners();
     return this.inspector.cancelPickColorFromPage()
                          .catch(console.error);
