# HG changeset patch
# User Jan Odvarko <odvarko@gmail.com>
# Date 1512660513 -3600
# Node ID 7f796e33aa5bd233d64e9d3eb466e62ca91af99e
# Parent  c9cafe87881ccf450bf5c22b25b068debbd725b6
Bug 1421335 - Right response info link disappears after request is expanded; r=nchevobbe

MozReview-Commit-ID: 5WEqPQaWGGt

diff --git a/devtools/client/netmonitor/src/connector/firefox-data-provider.js b/devtools/client/netmonitor/src/connector/firefox-data-provider.js
--- a/devtools/client/netmonitor/src/connector/firefox-data-provider.js
+++ b/devtools/client/netmonitor/src/connector/firefox-data-provider.js
@@ -336,17 +336,23 @@ class FirefoxDataProvider {
       case "responseContent":
         this.pushRequestToQueue(actor, {
           contentSize: networkInfo.response.bodySize,
           transferredSize: networkInfo.response.transferredSize,
           mimeType: networkInfo.response.content.mimeType,
         });
         break;
       case "eventTimings":
-        this.pushRequestToQueue(actor, { totalTime: networkInfo.totalTime });
+        // Total time doesn't have to be always set e.g. net provider enhancer
+        // in Console panel is using this method to fetch data when network log
+        // is expanded. So, make sure to not push undefined into the payload queue
+        // (it could overwrite an existing value).
+        if (typeof networkInfo.totalTime != "undefined") {
+          this.pushRequestToQueue(actor, { totalTime: networkInfo.totalTime });
+        }
         await this._requestData(actor, updateType);
         break;
     }
 
     // This available field helps knowing when/if updateType property is arrived
     // and can be requested via `requestData`
     this.pushRequestToQueue(actor, { [`${updateType}Available`]: true });
 
diff --git a/devtools/client/webconsole/new-console-output/reducers/messages.js b/devtools/client/webconsole/new-console-output/reducers/messages.js
--- a/devtools/client/webconsole/new-console-output/reducers/messages.js
+++ b/devtools/client/webconsole/new-console-output/reducers/messages.js
@@ -60,16 +60,17 @@ function addMessage(state, filtersState,
   const {
     messagesById,
     messagesUiById,
     groupsById,
     currentGroup,
     repeatById,
     visibleMessages,
     filteredMessagesCount,
+    networkMessagesUpdateById,
   } = state;
 
   if (newMessage.type === constants.MESSAGE_TYPE.NULL_MESSAGE) {
     // When the message has a NULL type, we don't add it.
     return state;
   }
 
   if (newMessage.type === constants.MESSAGE_TYPE.END_GROUP) {
@@ -131,16 +132,24 @@ function addMessage(state, filtersState,
   } else if (DEFAULT_FILTERS.includes(cause)) {
     newState.filteredMessagesCount = {
       ...filteredMessagesCount,
       global: filteredMessagesCount.global + 1,
       [cause]: filteredMessagesCount[cause] + 1
     };
   }
 
+  // Append received network-data also into networkMessagesUpdateById
+  // that is responsible for collecting (lazy loaded) HTTP payload data.
+  if (newMessage.source == "network") {
+    newState.networkMessagesUpdateById = Object.assign({}, networkMessagesUpdateById, {
+      [newMessage.actor]: newMessage
+    });
+  }
+
   return newState;
 }
 
 function messages(state = MessageState(), action, filtersState, prefsState) {
   const {
     messagesById,
     messagesUiById,
     messagesTableDataById,
diff --git a/devtools/client/webconsole/new-console-output/store.js b/devtools/client/webconsole/new-console-output/store.js
--- a/devtools/client/webconsole/new-console-output/store.js
+++ b/devtools/client/webconsole/new-console-output/store.js
@@ -193,35 +193,19 @@ function enableNetProvider(hud) {
       let newState = reducer(state, action);
 
       // If network message has been opened, fetch all HTTP details
       // from the backend. It can happen (especially in test) that
       // the message is opened before all network event updates are
       // received. The rest of updates will be handled below, see:
       // NETWORK_MESSAGE_UPDATE action handler.
       if (type == MESSAGE_OPEN) {
-        let message = getMessage(state, action.id);
+        let updates = getAllNetworkMessagesUpdateById(newState);
+        let message = updates[action.id];
         if (!message.openedOnce && message.source == "network") {
-          let updates = getAllNetworkMessagesUpdateById(newState);
-
-          // If there is no network request update received for this
-          // request-log, it's likely that it comes from cache.
-          // I.e. it's been executed before the console panel started
-          // listening from network events. Let fix that by updating
-          // the reducer now.
-          // Executing the reducer means that the `networkMessagesUpdateById`
-          // is updated (a actor key created). The key is needed for proper
-          // handling NETWORK_UPDATE_REQUEST event (in the same reducer).
-          if (!updates[action.id]) {
-            newState = reducer(newState, {
-              type: NETWORK_MESSAGE_UPDATE,
-              message: message,
-            });
-          }
-
           dataProvider.onNetworkEvent(null, message);
           message.updates.forEach(updateType => {
             dataProvider.onNetworkEventUpdate(null, {
               packet: { updateType: updateType },
               networkInfo: message,
             });
           });
         }
