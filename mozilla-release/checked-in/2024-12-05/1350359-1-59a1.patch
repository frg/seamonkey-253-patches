# HG changeset patch
# User Eden Chuang <echuang@mozilla.com>
# Date 1512369550 -28800
# Node ID 8a56d02d045536804ae3376daae792b61e17a934
# Parent  bb62e9843a7d2a9a6c687966e433e4e47e1d4aee
Bug 1350359 - Part 1: Set alternative data type from InterceptedChannel to InternalRequest. r=bkelly

diff --git a/dom/fetch/InternalRequest.cpp b/dom/fetch/InternalRequest.cpp
--- a/dom/fetch/InternalRequest.cpp
+++ b/dom/fetch/InternalRequest.cpp
@@ -46,16 +46,18 @@ InternalRequest::GetRequestConstructorCo
   copy->mContentPolicyType = mContentPolicyTypeOverridden ?
                              mContentPolicyType :
                              nsIContentPolicy::TYPE_FETCH;
   copy->mMode = mMode;
   copy->mCredentialsMode = mCredentialsMode;
   copy->mCacheMode = mCacheMode;
   copy->mRedirectMode = mRedirectMode;
   copy->mContentPolicyTypeOverridden = mContentPolicyTypeOverridden;
+
+  copy->mPreferredAlternativeDataType = mPreferredAlternativeDataType;
   return copy.forget();
 }
 
 already_AddRefed<InternalRequest>
 InternalRequest::Clone()
 {
   RefPtr<InternalRequest> clone = new InternalRequest(*this);
 
diff --git a/dom/fetch/InternalRequest.h b/dom/fetch/InternalRequest.h
--- a/dom/fetch/InternalRequest.h
+++ b/dom/fetch/InternalRequest.h
@@ -529,16 +529,28 @@ public:
   SetPrincipalInfo(UniquePtr<mozilla::ipc::PrincipalInfo> aPrincipalInfo);
 
   const UniquePtr<mozilla::ipc::PrincipalInfo>&
   GetPrincipalInfo() const
   {
     return mPrincipalInfo;
   }
 
+  const nsCString&
+  GetPreferredAlternativeDataType() const
+  {
+    return mPreferredAlternativeDataType;
+  }
+
+  void
+  SetPreferredAlternativeDataType(const nsACString& aDataType)
+  {
+    mPreferredAlternativeDataType = aDataType;
+  }
+
 private:
   // Does not copy mBodyStream.  Use fallible Clone() for complete copy.
   explicit InternalRequest(const InternalRequest& aOther);
 
   ~InternalRequest();
 
   static RequestContext
   MapContentPolicyTypeToRequestContext(nsContentPolicyType aContentPolicyType);
@@ -551,16 +563,18 @@ private:
 
   nsCString mMethod;
   // mURLList: a list of one or more fetch URLs
   nsTArray<nsCString> mURLList;
   RefPtr<InternalHeaders> mHeaders;
   nsCOMPtr<nsIInputStream> mBodyStream;
   int64_t mBodyLength;
 
+  nsCString mPreferredAlternativeDataType;
+
   nsContentPolicyType mContentPolicyType;
 
   // Empty string: no-referrer
   // "about:client": client (default)
   // URL: an URL
   nsString mReferrer;
   ReferrerPolicy mReferrerPolicy;
 
diff --git a/dom/workers/ServiceWorkerPrivate.cpp b/dom/workers/ServiceWorkerPrivate.cpp
--- a/dom/workers/ServiceWorkerPrivate.cpp
+++ b/dom/workers/ServiceWorkerPrivate.cpp
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ServiceWorkerPrivate.h"
 
 #include "ServiceWorkerManager.h"
 #include "ServiceWorkerWindowClient.h"
 #include "nsContentUtils.h"
+#include "nsICacheInfoChannel.h"
 #include "nsIHttpChannelInternal.h"
 #include "nsIHttpHeaderVisitor.h"
 #include "nsINamed.h"
 #include "nsINetworkInterceptController.h"
 #include "nsIPushErrorReporter.h"
 #include "nsISupportsImpl.h"
 #include "nsITimedChannel.h"
 #include "nsIUploadChannel2.h"
@@ -1596,16 +1597,28 @@ private:
                                                               mRequestRedirect,
                                                               mRequestCredentials,
                                                               NS_ConvertUTF8toUTF16(mReferrer),
                                                               mReferrerPolicy,
                                                               mContentPolicyType,
                                                               mIntegrity);
     internalReq->SetBody(mUploadStream, -1);
 
+    nsCOMPtr<nsIChannel> channel;
+    nsresult rv = mInterceptedChannel->GetChannel(getter_AddRefs(channel));
+    NS_ENSURE_SUCCESS(rv, false);
+
+    nsAutoCString alternativeDataType;
+    nsCOMPtr<nsICacheInfoChannel> cic = do_QueryInterface(channel);
+    if (cic &&
+        NS_SUCCEEDED(cic->GetPreferredAlternativeDataType(alternativeDataType)) &&
+        !alternativeDataType.IsEmpty()) {
+      internalReq->SetPreferredAlternativeDataType(alternativeDataType);
+    }
+
     nsCOMPtr<nsIGlobalObject> global = do_QueryInterface(globalObj.GetAsSupports());
     if (NS_WARN_IF(!global)) {
       return false;
     }
 
     // TODO This request object should be created with a AbortSignal object
     // which should be aborted if the loading is aborted. See bug 1394102.
     RefPtr<Request> request = new Request(global, internalReq, nullptr);
diff --git a/netwerk/base/nsICacheInfoChannel.idl b/netwerk/base/nsICacheInfoChannel.idl
--- a/netwerk/base/nsICacheInfoChannel.idl
+++ b/netwerk/base/nsICacheInfoChannel.idl
@@ -80,16 +80,23 @@ interface nsICacheInfoChannel : nsISuppo
    * Calling this method instructs the channel to serve the alternative data
    * if that was previously saved in the cache, otherwise it will serve the
    * real data.
    * Must be called before AsyncOpen.
    */
   void preferAlternativeDataType(in ACString type);
 
   /**
+   * Get the preferred alternative data type set by preferAlternativeDataType().
+   * This attribute stands for the desired data type instead of the type of the
+   * information retrieved from the network stack.
+   */
+  readonly attribute ACString preferredAlternativeDataType;
+
+  /**
    * Holds the type of the alternative data representation that the channel
    * is returning.
    * Is empty string if no alternative data representation was requested, or
    * if the requested representation wasn't found in the cache.
    * Can only be called during or after OnStartRequest.
    */
   readonly attribute ACString alternativeDataType;
 
diff --git a/netwerk/protocol/http/HttpChannelChild.cpp b/netwerk/protocol/http/HttpChannelChild.cpp
--- a/netwerk/protocol/http/HttpChannelChild.cpp
+++ b/netwerk/protocol/http/HttpChannelChild.cpp
@@ -3044,16 +3044,23 @@ NS_IMETHODIMP
 HttpChannelChild::PreferAlternativeDataType(const nsACString & aType)
 {
   ENSURE_CALLED_BEFORE_ASYNC_OPEN();
   mPreferredCachedAltDataType = aType;
   return NS_OK;
 }
 
 NS_IMETHODIMP
+HttpChannelChild::GetPreferredAlternativeDataType(nsACString & aType)
+{
+  aType = mPreferredCachedAltDataType;
+  return NS_OK;
+}
+
+NS_IMETHODIMP
 HttpChannelChild::GetAlternativeDataType(nsACString & aType)
 {
   // Must be called during or after OnStartRequest
   if (!mAfterOnStartRequestBegun) {
     return NS_ERROR_NOT_AVAILABLE;
   }
 
   aType = mAvailableCachedAltDataType;
diff --git a/netwerk/protocol/http/nsHttpChannel.cpp b/netwerk/protocol/http/nsHttpChannel.cpp
--- a/netwerk/protocol/http/nsHttpChannel.cpp
+++ b/netwerk/protocol/http/nsHttpChannel.cpp
@@ -7634,16 +7634,23 @@ NS_IMETHODIMP
 nsHttpChannel::PreferAlternativeDataType(const nsACString & aType)
 {
     ENSURE_CALLED_BEFORE_ASYNC_OPEN();
     mPreferredCachedAltDataType = aType;
     return NS_OK;
 }
 
 NS_IMETHODIMP
+nsHttpChannel::GetPreferredAlternativeDataType(nsACString & aType)
+{
+  aType = mPreferredCachedAltDataType;
+  return NS_OK;
+}
+
+NS_IMETHODIMP
 nsHttpChannel::GetAlternativeDataType(nsACString & aType)
 {
     // must be called during or after OnStartRequest
     if (!mAfterOnStartRequestBegun) {
         return NS_ERROR_NOT_AVAILABLE;
     }
     aType = mAvailableCachedAltDataType;
     return NS_OK;
