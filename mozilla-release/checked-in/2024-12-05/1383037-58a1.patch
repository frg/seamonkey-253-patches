# HG changeset patch
# User sole <spenades@mozilla.com>
# Date 1507024999 -3600
# Node ID 74d929bbd6099037869b77bec8c64456102188de
# Parent  be4e102292598c2c945a0520ad7c3558fd94f469
Bug 1383037 - Use only one debounce function in devtools. r=pbro

MozReview-Commit-ID: J9t90N1U2XY

diff --git a/devtools/client/inspector/rules/rules.js b/devtools/client/inspector/rules/rules.js
--- a/devtools/client/inspector/rules/rules.js
+++ b/devtools/client/inspector/rules/rules.js
@@ -25,17 +25,18 @@ const {
   VIEW_NODE_PROPERTY_TYPE,
   VIEW_NODE_VALUE_TYPE,
   VIEW_NODE_IMAGE_URL_TYPE,
   VIEW_NODE_LOCATION_TYPE,
   VIEW_NODE_SHAPE_POINT_TYPE,
 } = require("devtools/client/inspector/shared/node-types");
 const StyleInspectorMenu = require("devtools/client/inspector/shared/style-inspector-menu");
 const TooltipsOverlay = require("devtools/client/inspector/shared/tooltips-overlay");
-const {createChild, promiseWarn, debounce} = require("devtools/client/inspector/shared/utils");
+const {createChild, promiseWarn} = require("devtools/client/inspector/shared/utils");
+const {debounce} = require("devtools/shared/debounce");
 const EventEmitter = require("devtools/shared/old-event-emitter");
 const KeyShortcuts = require("devtools/client/shared/key-shortcuts");
 const clipboardHelper = require("devtools/shared/platform/clipboard");
 const AutocompletePopup = require("devtools/client/shared/autocomplete-popup");
 
 const HTML_NS = "http://www.w3.org/1999/xhtml";
 const PREF_UA_STYLES = "devtools.inspector.showUserAgentStyles";
 const PREF_DEFAULT_COLOR_UNIT = "devtools.defaultColorUnit";
diff --git a/devtools/client/inspector/shared/utils.js b/devtools/client/inspector/shared/utils.js
--- a/devtools/client/inspector/shared/utils.js
+++ b/devtools/client/inspector/shared/utils.js
@@ -95,46 +95,16 @@ function advanceValidate(keyCode, value,
     }
   }
   return false;
 }
 
 exports.advanceValidate = advanceValidate;
 
 /**
- * Create a debouncing function wrapper to only call the target function after a certain
- * amount of time has passed without it being called.
- *
- * @param {Function} func
- *         The function to debounce
- * @param {number} wait
- *         The wait period
- * @param {Object} scope
- *         The scope to use for func
- * @return {Function} The debounced function
- */
-function debounce(func, wait, scope) {
-  let timer = null;
-
-  return function () {
-    if (timer) {
-      clearTimeout(timer);
-    }
-
-    let args = arguments;
-    timer = setTimeout(function () {
-      timer = null;
-      func.apply(scope, args);
-    }, wait);
-  };
-}
-
-exports.debounce = debounce;
-
-/**
  * From underscore's `_.throttle`
  * http://underscorejs.org
  * (c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
  * Underscore may be freely distributed under the MIT license.
  *
  * Returns a function, that, when invoked, will only be triggered at most once during a
  * given window of time. The throttled function will run as much as it can, without ever
  * going more than once per wait duration.
diff --git a/devtools/shared/debounce.js b/devtools/shared/debounce.js
--- a/devtools/shared/debounce.js
+++ b/devtools/shared/debounce.js
@@ -1,39 +1,33 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 /**
- * From underscore's `_.debounce`
- * http://underscorejs.org
- * (c) 2009-2014 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
- * Underscore may be freely distributed under the MIT license.
+ * Create a debouncing function wrapper to only call the target function after a certain
+ * amount of time has passed without it being called.
  *
- * [and in turn extracted from the SDK's "lang/functional/concurrent.js"]
+ * @param {Function} func
+ *         The function to debounce
+ * @param {number} wait
+ *         The wait period
+ * @param {Object} scope
+ *         The scope to use for func
+ * @return {Function} The debounced function
  */
-exports.debounce = function (fn, wait) {
-  let timeout, args, context, timestamp, result;
+exports.debounce = function (func, wait, scope) {
+  let timer = null;
 
-  let later = function () {
-    let last = Date.now() - timestamp;
-    if (last < wait) {
-      timeout = setTimeout(later, wait - last);
-    } else {
-      timeout = null;
-      result = fn.apply(context, args);
-      context = args = null;
-    }
-  };
-
-  return function (...aArgs) {
-    context = this;
-    args = aArgs;
-    timestamp  = Date.now();
-    if (!timeout) {
-      timeout = setTimeout(later, wait);
+  return function () {
+    if (timer) {
+      clearTimeout(timer);
     }
 
-    return result;
+    let args = arguments;
+    timer = setTimeout(function () {
+      timer = null;
+      func.apply(scope, args);
+    }, wait);
   };
 };
