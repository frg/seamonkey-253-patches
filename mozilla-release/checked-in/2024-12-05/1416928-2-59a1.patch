# HG changeset patch
# User Luca Greco <lgreco@mozilla.com>
# Date 1510764863 -3600
# Node ID 3de9dc374632c47bfe05c610bfa40363fab1101c
# Parent  f3ffa82c873aae540eee73761f9cf397ca19844e
Bug 1416928 - Test content script debugging on the new debugger UI. r=jlast

MozReview-Commit-ID: Dt0eaKmp777

diff --git a/devtools/client/debugger/new/test/mochitest/browser.ini b/devtools/client/debugger/new/test/mochitest/browser.ini
--- a/devtools/client/debugger/new/test/mochitest/browser.ini
+++ b/devtools/client/debugger/new/test/mochitest/browser.ini
@@ -19,16 +19,17 @@ support-files =
   examples/sum/sum.js
   examples/sum/sum.min.js
   examples/sum/sum.min.js.map
   examples/reload/code_reload_1.js
   examples/reload/code_reload_2.js
   examples/reload/doc_reload.html
   examples/doc-async.html
   examples/doc-asm.html
+  examples/doc-content-script-sources.html
   examples/doc-scripts.html
   examples/doc-script-mutate.html
   examples/doc-script-switching.html
   examples/doc-exceptions.html
   examples/doc-iframes.html
   examples/doc-frames.html
   examples/doc-debugger-statements.html
   examples/doc-minified.html
@@ -69,16 +70,17 @@ support-files =
 [browser_dbg-breakpoints-cond.js]
 [browser_dbg-browser-content-toolbox.js]
 skip-if = !e10s # This test is only valid in e10s
 [browser_dbg-call-stack.js]
 [browser_dbg-scopes.js]
 [browser_dbg-chrome-create.js]
 [browser_dbg-chrome-debugging.js]
 [browser_dbg-console.js]
+[browser_dbg-content-script-sources.js]
 [browser_dbg-debugger-buttons.js]
 [browser_dbg-editor-gutter.js]
 [browser_dbg-editor-select.js]
 [browser_dbg-editor-highlight.js]
 [browser_dbg-expressions.js]
 [browser_dbg-expressions-error.js]
 [browser_dbg-iframes.js]
 [browser_dbg_keyboard_navigation.js]
diff --git a/devtools/client/debugger/new/test/mochitest/browser_dbg-content-script-sources.js b/devtools/client/debugger/new/test/mochitest/browser_dbg-content-script-sources.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/debugger/new/test/mochitest/browser_dbg-content-script-sources.js
@@ -0,0 +1,87 @@
+/* Any copyright is dedicated to the Public Domain.
+ * http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+/* global ExtensionTestUtils, closeTab, openToolboxForTab, assertDebugLine,
+          waitForSelectedSource */
+
+// Tests that the content scripts are listed in the source tree.
+
+async function selectContentScriptSources(dbg) {
+  await waitForSources(dbg, "content_script.js");
+
+  // Select a source.
+  await selectSource(dbg, "content_script.js");
+
+  ok(
+    findElementWithSelector(dbg, ".sources-list .focused"),
+    "Source is focused"
+  );
+}
+
+async function installAndStartExtension() {
+  function contentScript() {
+    console.log("content script loads");
+
+    // This listener prevents the source from being garbage collected
+    // and be missing from the scripts returned by `dbg.findScripts()`
+    // in `ThreadActor._discoverSources`.
+    window.onload = () => {};
+  }
+
+  let extension = ExtensionTestUtils.loadExtension({
+    manifest: {
+      "content_scripts": [
+        {
+          "js": ["content_script.js"],
+          "matches": ["http://example.com/*"],
+          "run_at": "document_start",
+        },
+      ],
+    },
+    files: {
+      "content_script.js": contentScript,
+    },
+  });
+
+  await extension.startup();
+
+  return extension;
+}
+
+add_task(async function () {
+  const extension = await installAndStartExtension();
+
+  let dbg = await initDebugger("doc-content-script-sources.html");
+  await selectContentScriptSources(dbg);
+  await closeTab(dbg, "content_script.js");
+
+  // Destroy the toolbox and repeat the test in a new toolbox
+  // and ensures that the content script is still listed.
+  await dbg.toolbox.destroy();
+  const toolbox = await openToolboxForTab(gBrowser.selectedTab, "jsdebugger");
+  dbg = createDebuggerContext(toolbox);
+  await selectContentScriptSources(dbg);
+
+  await addBreakpoint(dbg, "content_script.js", 2);
+
+  for (let i = 1; i < 3; i++) {
+    info(`Reloading tab (${i} time)`);
+    gBrowser.reloadTab(gBrowser.selectedTab);
+    await waitForPaused(dbg);
+    await waitForSources(dbg, "content_script.js");
+    await waitForSelectedSource(dbg, "content_script.js");
+    ok(
+      findElementWithSelector(dbg, ".sources-list .focused"),
+      "Source is focused"
+    );
+    assertPausedLocation(dbg);
+    assertDebugLine(dbg, 2);
+    await resume(dbg);
+  }
+
+  await closeTab(dbg, "content_script.js");
+
+  await extension.unload();
+});
diff --git a/devtools/client/debugger/new/test/mochitest/examples/doc-content-script-sources.html b/devtools/client/debugger/new/test/mochitest/examples/doc-content-script-sources.html
new file mode 100644
--- /dev/null
+++ b/devtools/client/debugger/new/test/mochitest/examples/doc-content-script-sources.html
@@ -0,0 +1,12 @@
+<!-- Any copyright is dedicated to the Public Domain.
+     http://creativecommons.org/publicdomain/zero/1.0/ -->
+<!DOCTYPE html>
+<html>
+  <head>
+    <meta charset="utf-8"/>
+    <title>Debugger test page</title>
+  </head>
+
+  <body>
+  </body>
+</html>
