# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1515172222 18000
# Node ID e7b92985e941f7f1b9b562fba75b0b764b1ce6a9
# Parent  9a14e1e8544c7bb1d640516454a4518e0311af5e
Bug 1425975 P14 Assert that storage is allowed when a ClientSource is both execution ready and controlled. r=asuth

diff --git a/dom/clients/manager/ClientSource.cpp b/dom/clients/manager/ClientSource.cpp
--- a/dom/clients/manager/ClientSource.cpp
+++ b/dom/clients/manager/ClientSource.cpp
@@ -201,16 +201,24 @@ ClientSource::WorkerExecutionReady(Worke
 {
   MOZ_DIAGNOSTIC_ASSERT(aWorkerPrivate);
   aWorkerPrivate->AssertIsOnWorkerThread();
 
   if (IsShutdown()) {
     return;
   }
 
+  // A client without access to storage should never be controlled by
+  // a service worker.  Check this here in case we were controlled before
+  // execution ready.  We can't reliably determine what our storage policy
+  // is before execution ready, unfortunately.
+  if (mController.isSome()) {
+    MOZ_DIAGNOSTIC_ASSERT(aWorkerPrivate->IsStorageAllowed());
+  }
+
   // Its safe to store the WorkerPrivate* here because the ClientSource
   // is explicitly destroyed by WorkerPrivate before exiting its run loop.
   MOZ_DIAGNOSTIC_ASSERT(mOwner.is<Nothing>());
   mOwner = AsVariant(aWorkerPrivate);
 
   ClientSourceExecutionReadyArgs args(
     aWorkerPrivate->GetLocationInfo().mHref,
     FrameType::None);
@@ -230,16 +238,25 @@ ClientSource::WindowExecutionReady(nsPID
     return NS_OK;
   }
 
   nsIDocument* doc = aInnerWindow->GetExtantDoc();
   if (NS_WARN_IF(!doc)) {
     return NS_ERROR_UNEXPECTED;
   }
 
+  // A client without access to storage should never be controlled by
+  // a service worker.  Check this here in case we were controlled before
+  // execution ready.  We can't reliably determine what our storage policy
+  // is before execution ready, unfortunately.
+  if (mController.isSome()) {
+    MOZ_DIAGNOSTIC_ASSERT(nsContentUtils::StorageAllowedForWindow(aInnerWindow) ==
+                          nsContentUtils::StorageAccess::eAllow);
+  }
+
   // Don't use nsAutoCString here since IPC requires a full nsCString anyway.
   nsCString spec;
 
   nsIURI* uri = doc->GetOriginalURI();
   if (uri) {
     nsresult rv = uri->GetSpec(spec);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
@@ -285,16 +302,20 @@ ClientSource::DocShellExecutionReady(nsI
     return NS_OK;
   }
 
   nsPIDOMWindowOuter* outer = aDocShell->GetWindow();
   if (NS_WARN_IF(!outer)) {
     return NS_ERROR_UNEXPECTED;
   }
 
+  // Note: We don't assert storage access for a controlled client.  If
+  // the about:blank actually gets used then WindowExecutionReady() will
+  // get called which asserts storage access.
+
   // TODO: dedupe this with WindowExecutionReady
   FrameType frameType = FrameType::Top_level;
   if (!outer->IsTopLevelWindow()) {
     frameType = FrameType::Nested;
   } else if(outer->HadOriginalOpener()) {
     frameType = FrameType::Auxiliary;
   }
 
@@ -355,16 +376,26 @@ ClientSource::SetController(const Servic
 {
   NS_ASSERT_OWNINGTHREAD(ClientSource);
 
   // A client in private browsing mode should never be controlled by
   // a service worker.  The principal origin attributes should guarantee
   // this invariant.
   MOZ_DIAGNOSTIC_ASSERT(!mClientInfo.IsPrivateBrowsing());
 
+  // A client without access to storage should never be controlled a
+  // a service worker.  If we are already execution ready with a real
+  // window or worker, then verify assert the storage policy is correct.
+  if (GetInnerWindow()) {
+    MOZ_DIAGNOSTIC_ASSERT(nsContentUtils::StorageAllowedForWindow(GetInnerWindow()) ==
+                          nsContentUtils::StorageAccess::eAllow);
+  } else if (GetWorkerPrivate()) {
+    MOZ_DIAGNOSTIC_ASSERT(GetWorkerPrivate()->IsStorageAllowed());
+  }
+
   if (mController.isSome() && mController.ref() == aServiceWorker) {
     return;
   }
 
   mController.reset();
   mController.emplace(aServiceWorker);
 
   RefPtr<ServiceWorkerContainer> swc;
