# HG changeset patch
# User Alexandre Poirot <poirot.alex@gmail.com>
# Date 1510916562 28800
# Node ID 18ecb8448c59dc32c2a04de63e005d3b7a0638f2
# Parent  a55e987bf91acd84cee1e3f1fc0efb993003c1fc
Bug 1415532 - Convert damp.js from Task.jsm to async/await. r=jdescottes

MozReview-Commit-ID: WXZ3Tv2BTI

diff --git a/testing/talos/talos/tests/devtools/addon/content/damp.js b/testing/talos/talos/tests/devtools/addon/content/damp.js
--- a/testing/talos/talos/tests/devtools/addon/content/damp.js
+++ b/testing/talos/talos/tests/devtools/addon/content/damp.js
@@ -1,10 +1,9 @@
 const { Services } = ChromeUtils.import("resource://gre/modules/Services.jsm");
-const { Task } = ChromeUtils.import("resource://gre/modules/Task.jsm", {});
 const { XPCOMUtils } = ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm", {});
 const env = Cc["@mozilla.org/process/environment;1"].getService(Ci.nsIEnvironment);
 
 XPCOMUtils.defineLazyGetter(this, "require", function() {
   let { require } =
     ChromeUtils.import("resource://devtools/shared/Loader.jsm", {});
   return require;
 });
@@ -108,27 +107,27 @@ Damp.prototype = {
 
     let stopRecordTimestamp = performance.now();
     return {
       toolbox,
       time: stopRecordTimestamp - startRecordTimestamp
     };
   },
 
-  closeToolbox: Task.async(function* () {
+  async closeToolbox() {
     let tab = getActiveTab(getMostRecentBrowserWindow());
     let target = TargetFactory.forTab(tab);
-    yield target.client.waitForRequestsToSettle();
+    await target.client.waitForRequestsToSettle();
     let startRecordTimestamp = performance.now();
-    yield gDevTools.closeToolbox(target);
+    await gDevTools.closeToolbox(target);
     let stopRecordTimestamp = performance.now();
     return {
       time: stopRecordTimestamp - startRecordTimestamp
     };
-  }),
+  },
 
   saveHeapSnapshot(label) {
     let tab = getActiveTab(getMostRecentBrowserWindow());
     let target = TargetFactory.forTab(tab);
     let toolbox = gDevTools.getToolbox(target);
     let panel = toolbox.getCurrentPanel();
     let memoryFront = panel.panelWin.gFront;
 
@@ -149,31 +148,31 @@ Damp.prototype = {
     let end = performance.now();
     this._results.push({
       name: label + ".readHeapSnapshot",
       value: end - start
     });
     return Promise.resolve();
   },
 
-  waitForNetworkRequests: Task.async(function* (label, toolbox) {
+  async waitForNetworkRequests(label, toolbox) {
     const start = performance.now();
-    yield this.waitForAllRequestsFinished();
+    await this.waitForAllRequestsFinished();
     const end = performance.now();
     this._results.push({
       name: label + ".requestsFinished.DAMP",
       value: end - start
     });
-  }),
+  },
 
-  _consoleBulkLoggingTest: Task.async(function* () {
+  async _consoleBulkLoggingTest() {
     let TOTAL_MESSAGES = 10;
-    let tab = yield this.testSetup(SIMPLE_URL);
+    let tab = await this.testSetup(SIMPLE_URL);
     let messageManager = tab.linkedBrowser.messageManager;
-    let {toolbox} = yield this.openToolbox("webconsole");
+    let {toolbox} = await this.openToolbox("webconsole");
     let webconsole = toolbox.getPanel("webconsole");
 
     // Resolve once the last message has been received.
     let allMessagesReceived = new Promise(resolve => {
       function receiveMessages(e, messages) {
         for (let m of messages) {
           if (m.node.textContent.includes("damp " + TOTAL_MESSAGES)) {
             webconsole.hud.ui.off("new-messages", receiveMessages);
@@ -196,36 +195,36 @@ Damp.prototype = {
         });
       }`
     ) + ")()", true);
 
     // Kick off the logging
     messageManager.sendAsyncMessage("do-logs");
 
     let start = performance.now();
-    yield allMessagesReceived;
+    await allMessagesReceived;
     let end = performance.now();
 
     this._results.push({
       name: "console.bulklog",
       value: end - start
     });
 
-    yield this.closeToolbox(null);
-    yield this.testTeardown();
-  }),
+    await this.closeToolbox(null);
+    await this.testTeardown();
+  },
 
   // Log a stream of console messages, 1 per rAF.  Then record the average
   // time per rAF.  The idea is that the console being slow can slow down
   // content (i.e. Bug 1237368).
-  _consoleStreamLoggingTest: Task.async(function* () {
+  async _consoleStreamLoggingTest() {
     let TOTAL_MESSAGES = 100;
-    let tab = yield this.testSetup(SIMPLE_URL);
+    let tab = await this.testSetup(SIMPLE_URL);
     let messageManager = tab.linkedBrowser.messageManager;
-    yield this.openToolbox("webconsole");
+    await this.openToolbox("webconsole");
 
     // Load a frame script using a data URI so we can do logs
     // from the page.  So this is running in content.
     messageManager.loadFrameScript("data:,(" + encodeURIComponent(
       `function () {
         let count = 0;
         let startTime = content.performance.now();
         function log() {
@@ -242,35 +241,35 @@ Damp.prototype = {
             let avgTime = (content.performance.now() - startTime) / ${TOTAL_MESSAGES};
             sendSyncMessage("done", Math.round(avgTime));
           }
         }
         log();
       }`
     ) + ")()", true);
 
-    let avgTime = yield new Promise(resolve => {
+    let avgTime = await new Promise(resolve => {
       messageManager.addMessageListener("done", (e) => {
         resolve(e.data);
       });
     });
 
     this._results.push({
       name: "console.streamlog",
       value: avgTime
     });
 
-    yield this.closeToolbox(null);
-    yield this.testTeardown();
-  }),
+    await this.closeToolbox(null);
+    await this.testTeardown();
+  },
 
-  _consoleObjectExpansionTest: Task.async(function* () {
-    let tab = yield this.testSetup(SIMPLE_URL);
+  async _consoleObjectExpansionTest() {
+    let tab = await this.testSetup(SIMPLE_URL);
     let messageManager = tab.linkedBrowser.messageManager;
-    let {toolbox} = yield this.openToolbox("webconsole");
+    let {toolbox} = await this.openToolbox("webconsole");
     let webconsole = toolbox.getPanel("webconsole");
 
     // Resolve once the first message is received.
     let onMessageReceived = new Promise(resolve => {
       function receiveMessages(e, messages) {
         for (let m of messages) {
           resolve(m);
         }
@@ -290,40 +289,40 @@ Damp.prototype = {
         });
       }`
     ) + ")()", true);
 
     // Kick off the logging
     messageManager.sendAsyncMessage("do-dir");
 
     let start = performance.now();
-    yield onMessageReceived;
+    await onMessageReceived;
     const tree = webconsole.hud.ui.outputNode.querySelector(".dir.message .tree");
     // The tree can be collapsed since the properties are fetched asynchronously.
     if (tree.querySelectorAll(".node").length === 1) {
       // If this is the case, we wait for the properties to be fetched and displayed.
-      yield new Promise(resolve => {
+      await new Promise(resolve => {
         const observer = new MutationObserver(mutations => {
           resolve(mutations);
           observer.disconnect();
         });
         observer.observe(tree, {
           childList: true
         });
       });
     }
 
     this._results.push({
       name: "console.objectexpand",
       value: performance.now() - start,
     });
 
-    yield this.closeToolboxAndLog("console.objectexpanded");
-    yield this.testTeardown();
-  }),
+    await this.closeToolboxAndLog("console.objectexpanded");
+    await this.testTeardown();
+  },
 
 async _consoleOpenWithCachedMessagesTest() {
   let TOTAL_MESSAGES = 100;
   let tab = await this.testSetup(SIMPLE_URL);
 
   // Load a frame script using a data URI so we can do logs
   // from the page.  So this is running in content.
   tab.linkedBrowser.messageManager.loadFrameScript("data:,(" + encodeURIComponent(`
@@ -339,20 +338,20 @@ async _consoleOpenWithCachedMessagesTest
   await this.closeToolbox(null);
   await this.testTeardown();
 },
 
   /**
    * Measure the time necesssary to perform successive childList mutations in the content
    * page and update the markup-view accordingly.
    */
-  _inspectorMutationsTest: Task.async(function* () {
-    let tab = yield this.testSetup(SIMPLE_URL);
+  async _inspectorMutationsTest() {
+    let tab = await this.testSetup(SIMPLE_URL);
     let messageManager = tab.linkedBrowser.messageManager;
-    let {toolbox} = yield this.openToolbox("inspector");
+    let {toolbox} = await this.openToolbox("inspector");
     let inspector = toolbox.getPanel("inspector");
 
     // Test with n=LIMIT mutations, with t=DELAY ms between each one.
     const LIMIT = 100;
     const DELAY = 5;
 
     messageManager.loadFrameScript("data:,(" + encodeURIComponent(
       `function () {
@@ -369,17 +368,17 @@ async _consoleOpenWithCachedMessagesTest
           };
           addElement(0);
         });
       }`
     ) + ")()", false);
 
     let start = performance.now();
 
-    yield new Promise(resolve => {
+    await new Promise(resolve => {
       let childListMutationsCounter = 0;
       inspector.on("markupmutation", (evt, mutations) => {
         let childListMutations = mutations.filter(m => m.type === "childList");
         childListMutationsCounter += childListMutations.length;
         if (childListMutationsCounter === LIMIT) {
           // Wait until we received exactly n=LIMIT mutations in the markup view.
           resolve();
         }
@@ -388,19 +387,19 @@ async _consoleOpenWithCachedMessagesTest
       messageManager.sendAsyncMessage("start-mutations-test");
     });
 
     this._results.push({
       name: "inspector.mutations",
       value: performance.now() - start
     });
 
-    yield this.closeToolbox(null);
-    yield this.testTeardown();
-  }),
+    await this.closeToolbox(null);
+    await this.testTeardown();
+  },
 
   takeCensus(label) {
     let start = performance.now();
 
     this._snapshot.takeCensus({
       breakdown: {
         by: "coarseType",
         objects: {
@@ -478,145 +477,145 @@ async _consoleOpenWithCachedMessagesTest
     await this.reloadPageAndLog("panelsInBackground");
 
     await this.closeToolbox();
     await this.testTeardown();
   },
 
   _getToolLoadingTests(url, label, { expectedMessages, expectedSources }) {
     let tests = {
-      inspector: Task.async(function* () {
-        yield this.testSetup(url);
-        let toolbox = yield this.openToolboxAndLog(label + ".inspector", "inspector");
+      async inspector() {
+        await this.testSetup(url);
+        let toolbox = await this.openToolboxAndLog(label + ".inspector", "inspector");
         let onReload = async function() {
           let inspector = toolbox.getPanel("inspector");
           // First wait for markup view to be loaded against the new root node
           await inspector.once("new-root");
           // Then wait for inspector to be updated
           await inspector.once("inspector-updated");
         };
-        yield this.reloadPageAndLog(label + ".inspector", onReload);
-        yield this.closeToolboxAndLog(label + ".inspector");
-        yield this.testTeardown();
-      }),
+        await this.reloadPageAndLog(label + ".inspector", onReload);
+        await this.closeToolboxAndLog(label + ".inspector");
+        await this.testTeardown();
+      },
 
-      webconsole: Task.async(function* () {
-        yield this.testSetup(url);
-        let toolbox = yield this.openToolboxAndLog(label + ".webconsole", "webconsole");
+      async webconsole() {
+        await this.testSetup(url);
+        let toolbox = await this.openToolboxAndLog(label + ".webconsole", "webconsole");
         let onReload = async function() {
           let webconsole = toolbox.getPanel("webconsole");
           await new Promise(done => {
             let messages = 0;
             let receiveMessages = () => {
               if (++messages == expectedMessages) {
                 webconsole.hud.ui.off("new-messages", receiveMessages);
                 done();
               }
             };
             webconsole.hud.ui.on("new-messages", receiveMessages);
           });
         };
-        yield this.reloadPageAndLog(label + ".webconsole", onReload);
-        yield this.closeToolboxAndLog(label + ".webconsole");
-        yield this.testTeardown();
-      }),
+        await this.reloadPageAndLog(label + ".webconsole", onReload);
+        await this.closeToolboxAndLog(label + ".webconsole");
+        await this.testTeardown();
+      },
 
-      debugger: Task.async(function* () {
-        yield this.testSetup(url);
+      async debugger() {
+        await this.testSetup(url);
         let onLoad = async function(toolbox, dbg) {
           await new Promise(done => {
             let { selectors, store } = dbg.panelWin.getGlobalsForTesting();
             let unsubscribe;
             function countSources() {
               const sources = selectors.getSources(store.getState());
               if (sources.size >= expectedSources) {
                 unsubscribe();
                 done();
               }
             }
             unsubscribe = store.subscribe(countSources);
             countSources();
           });
         };
-        let toolbox = yield this.openToolboxAndLog(label + ".jsdebugger", "jsdebugger", onLoad);
+        let toolbox = await this.openToolboxAndLog(label + ".jsdebugger", "jsdebugger", onLoad);
         let onReload = async function() {
           await new Promise(done => {
             let count = 0;
             let { client } = toolbox.target;
             let onSource = async (_, actor) => {
               if (++count >= expectedSources) {
                 client.removeListener("newSource", onSource);
                 done();
               }
             };
             client.addListener("newSource", onSource);
           });
         };
-        yield this.reloadPageAndLog(label + ".jsdebugger", onReload);
-        yield this.closeToolboxAndLog(label + ".jsdebugger");
-        yield this.testTeardown();
-      }),
+        await this.reloadPageAndLog(label + ".jsdebugger", onReload);
+        await this.closeToolboxAndLog(label + ".jsdebugger");
+        await this.testTeardown();
+      },
 
-      styleeditor: Task.async(function* () {
-        yield this.testSetup(url);
-        yield this.openToolboxAndLog(label + ".styleeditor", "styleeditor");
-        yield this.reloadPageAndLog(label + ".styleeditor");
-        yield this.closeToolboxAndLog(label + ".styleeditor");
-        yield this.testTeardown();
-      }),
+      async styleeditor() {
+        await this.testSetup(url);
+        await this.openToolboxAndLog(label + ".styleeditor", "styleeditor");
+        await this.reloadPageAndLog(label + ".styleeditor");
+        await this.closeToolboxAndLog(label + ".styleeditor");
+        await this.testTeardown();
+      },
 
-      performance: Task.async(function* () {
-        yield this.testSetup(url);
-        yield this.openToolboxAndLog(label + ".performance", "performance");
-        yield this.reloadPageAndLog(label + ".performance");
-        yield this.closeToolboxAndLog(label + ".performance");
-        yield this.testTeardown();
-      }),
+      async performance() {
+        await this.testSetup(url);
+        await this.openToolboxAndLog(label + ".performance", "performance");
+        await this.reloadPageAndLog(label + ".performance");
+        await this.closeToolboxAndLog(label + ".performance");
+        await this.testTeardown();
+      },
 
-      netmonitor: Task.async(function* () {
-        yield this.testSetup(url);
-        const toolbox = yield this.openToolboxAndLog(label + ".netmonitor", "netmonitor");
+      async netmonitor() {
+        await this.testSetup(url);
+        const toolbox = await this.openToolboxAndLog(label + ".netmonitor", "netmonitor");
         const requestsDone = this.waitForNetworkRequests(label + ".netmonitor", toolbox);
-        yield this.reloadPageAndLog(label + ".netmonitor");
-        yield requestsDone;
-        yield this.closeToolboxAndLog(label + ".netmonitor");
-        yield this.testTeardown();
-      }),
+        await this.reloadPageAndLog(label + ".netmonitor");
+        await requestsDone;
+        await this.closeToolboxAndLog(label + ".netmonitor");
+        await this.testTeardown();
+      },
 
-      saveAndReadHeapSnapshot: Task.async(function* () {
-        yield this.testSetup(url);
-        yield this.openToolboxAndLog(label + ".memory", "memory");
-        yield this.reloadPageAndLog(label + ".memory");
-        yield this.saveHeapSnapshot(label);
-        yield this.readHeapSnapshot(label);
-        yield this.takeCensus(label);
-        yield this.closeToolboxAndLog(label + ".memory");
-        yield this.testTeardown();
-      }),
+      async saveAndReadHeapSnapshot() {
+        await this.testSetup(url);
+        await this.openToolboxAndLog(label + ".memory", "memory");
+        await this.reloadPageAndLog(label + ".memory");
+        await this.saveHeapSnapshot(label);
+        await this.readHeapSnapshot(label);
+        await this.takeCensus(label);
+        await this.closeToolboxAndLog(label + ".memory");
+        await this.testTeardown();
+      },
     };
     // Prefix all tests with the page type (simple or complicated)
     for (let name in tests) {
       tests[label + "." + name] = tests[name];
       delete tests[name];
     }
     return tests;
   },
 
-  testSetup: Task.async(function* (url) {
-    let tab = yield this.addTab(url);
-    yield new Promise(resolve => {
+  async testSetup(url) {
+    let tab = await this.addTab(url);
+    await new Promise(resolve => {
       setTimeout(resolve, this._config.rest);
     });
     return tab;
-  }),
+  },
 
-  testTeardown: Task.async(function* (url) {
+  async testTeardown(url) {
     this.closeCurrentTab();
     this._nextCommand();
-  }),
+  },
 
   // Everything below here are common pieces needed for the test runner to function,
   // just copy and pasted from Tart with /s/TART/DAMP
 
   _win: undefined,
   _dampTab: undefined,
   _results: [],
   _config: {subtests: [], repeat: 1, rest: 100},
