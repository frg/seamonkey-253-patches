# HG changeset patch
# User John Schanck <jschanck@mozilla.com>
# Date 1678403254 0
#      Thu Mar 09 23:07:34 2023 +0000
# Node ID f7927075967fb5ec2580fa68c1590cf09064707a
# Parent  62ae85a85926944f45374c0ab437ad809accaa16
Bug 1815435 - land NSS NSS_3_89_RTM UPGRADE_NSS_RELEASE, r=keeler

Differential Revision: https://phabricator.services.mozilla.com/D172164

diff --git a/security/nss/TAG-INFO b/security/nss/TAG-INFO
--- a/security/nss/TAG-INFO
+++ b/security/nss/TAG-INFO
@@ -1,1 +1,1 @@
-NSS_3_89_BETA4
\ No newline at end of file
+NSS_3_89_RTM
\ No newline at end of file
diff --git a/security/nss/coreconf/coreconf.dep b/security/nss/coreconf/coreconf.dep
--- a/security/nss/coreconf/coreconf.dep
+++ b/security/nss/coreconf/coreconf.dep
@@ -5,8 +5,9 @@
 
 /*
  * A dummy header file that is a dependency for all the object files.
  * Used to force a full recompilation of NSS in Mozilla's Tinderbox
  * depend builds.  See comments in rules.mk.
  */
 
 #error "Do not include this header file."
+
diff --git a/security/nss/doc/rst/releases/index.rst b/security/nss/doc/rst/releases/index.rst
--- a/security/nss/doc/rst/releases/index.rst
+++ b/security/nss/doc/rst/releases/index.rst
@@ -3,16 +3,17 @@
 Releases
 ========
 
 .. toctree::
    :maxdepth: 0
    :glob:
    :hidden:
 
+   nss_3_89.rst
    nss_3_88_1.rst
    nss_3_88.rst
    nss_3_87_1.rst
    nss_3_87.rst
    nss_3_86.rst
    nss_3_85.rst
    nss_3_84.rst
    nss_3_83.rst
diff --git a/security/nss/doc/rst/releases/nss_3_89.rst b/security/nss/doc/rst/releases/nss_3_89.rst
new file mode 100644
--- /dev/null
+++ b/security/nss/doc/rst/releases/nss_3_89.rst
@@ -0,0 +1,83 @@
+.. _mozilla_projects_nss_nss_3_89_release_notes:
+
+NSS 3.89 release notes
+======================
+
+`Introduction <#introduction>`__
+--------------------------------
+
+.. container::
+
+   Network Security Services (NSS) 3.89 was released on *9 March 2023**.
+
+
+.. _distribution_information:
+
+`Distribution Information <#distribution_information>`__
+--------------------------------------------------------
+
+.. container::
+
+   The HG tag is NSS_3_89_RTM. NSS 3.89 requires NSPR 4.35 or newer.
+
+   NSS 3.89 source distributions are available on ftp.mozilla.org for secure HTTPS download:
+
+   -  Source tarballs:
+      https://ftp.mozilla.org/pub/mozilla.org/security/nss/releases/NSS_3_89_RTM/src/
+
+   Other releases are available :ref:`mozilla_projects_nss_releases`.
+
+.. _changes_in_nss_3.89:
+
+`Changes in NSS 3.89 <#changes_in_nss_3.89>`__
+----------------------------------------------------
+
+.. container::
+
+   - Bug 1820834 - revert freebl/softoken RSA_MIN_MODULUS_BITS increase.
+   - Bug 1820175 - PR_STATIC_ASSERT is cursed.
+   - Bug 1767883 - Need to add policy control to keys lengths for signatures.
+   - Bug 1820175 - Fix unreachable code warning in fuzz builds.
+   - Bug 1820175 - Fix various compiler warnings in NSS.
+   - Bug 1820175 - Enable various compiler warnings for clang builds.
+   - Bug 1815136 - set PORT error after sftk_HMACCmp failure.
+   - Bug 1767883 - Need to add policy control to keys lengths for signatures.
+   - Bug 1804662 - remove data length assertion in sec_PKCS7Decrypt.
+   - Bug 1804660 - Make high tag number assertion failure an error.
+   - Bug 1817513 - CKM_SHA384_KEY_DERIVATION correction maximum key length from 284 to 384.
+   - Bug 1815167 - Tolerate certificate_authorities xtn in ClientHello.
+   - Bug 1789436 - Fix build failure on Windows.
+   - Bug 1811337 - migrate Win 2012 tasks to Azure.
+   - Bug 1810702 - fix title length in doc.
+   - Bug 1570615 - Add interop tests for HRR and PSK to GREASE suite.
+   - Bug 1570615 - Add presence/absence tests for TLS GREASE.
+   - Bug 1804688 - Correct addition of GREASE value to ALPN xtn.
+   - Bug 1789436 - CH extension permutation.
+   - Bug 1570615 - TLS GREASE (RFC8701).
+   - Bug 1804640 - improve handling of unknown PKCS#12 safe bag types.
+   - Bug 1815870 - use a different treeherder symbol for each docker image build task.
+   - Bug 1815868 - pin an older version of the ubuntu:18.04 and 20.04 docker images.
+   - Bug 1810702 - remove nested table in rst doc.
+   - Bug 1815246 - Export NSS_CMSSignerInfo_GetDigestAlgTag.
+   - Bug 1812671 - build failure while implicitly casting SECStatus to PRUInt32.
+
+
+`Compatibility <#compatibility>`__
+----------------------------------
+
+.. container::
+
+   NSS 3.89 shared libraries are backwards-compatible with all older NSS 3.x shared
+   libraries. A program linked with older NSS 3.x shared libraries will work with
+   this new version of the shared libraries without recompiling or
+   relinking. Furthermore, applications that restrict their use of NSS APIs to the
+   functions listed in NSS Public Functions will remain compatible with future
+   versions of the NSS shared libraries.
+
+`Feedback <#feedback>`__
+------------------------
+
+.. container::
+
+   Bugs discovered should be reported by filing a bug report on
+   `bugzilla.mozilla.org <https://bugzilla.mozilla.org/enter_bug.cgi?product=NSS>`__ (product NSS).
diff --git a/security/nss/lib/nss/nss.h b/security/nss/lib/nss/nss.h
--- a/security/nss/lib/nss/nss.h
+++ b/security/nss/lib/nss/nss.h
@@ -17,22 +17,22 @@
 
 /*
  * NSS's major version, minor version, patch level, build number, and whether
  * this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define NSS_VERSION "3.89" _NSS_CUSTOMIZED " Beta"
+#define NSS_VERSION "3.89" _NSS_CUSTOMIZED
 #define NSS_VMAJOR 3
 #define NSS_VMINOR 89
 #define NSS_VPATCH 0
 #define NSS_VBUILD 0
-#define NSS_BETA PR_TRUE
+#define NSS_BETA PR_FALSE
 
 #ifndef RC_INVOKED
 
 #include "seccomon.h"
 
 typedef struct NSSInitParametersStr NSSInitParameters;
 
 /*
diff --git a/security/nss/lib/softoken/softkver.h b/security/nss/lib/softoken/softkver.h
--- a/security/nss/lib/softoken/softkver.h
+++ b/security/nss/lib/softoken/softkver.h
@@ -12,16 +12,16 @@
 
 /*
  * Softoken's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <ECC>][ <Beta>]"
  */
-#define SOFTOKEN_VERSION "3.89" SOFTOKEN_ECC_STRING " Beta"
+#define SOFTOKEN_VERSION "3.89" SOFTOKEN_ECC_STRING
 #define SOFTOKEN_VMAJOR 3
 #define SOFTOKEN_VMINOR 89
 #define SOFTOKEN_VPATCH 0
 #define SOFTOKEN_VBUILD 0
-#define SOFTOKEN_BETA PR_TRUE
+#define SOFTOKEN_BETA PR_FALSE
 
 #endif /* _SOFTKVER_H_ */
diff --git a/security/nss/lib/util/nssutil.h b/security/nss/lib/util/nssutil.h
--- a/security/nss/lib/util/nssutil.h
+++ b/security/nss/lib/util/nssutil.h
@@ -14,22 +14,22 @@
 
 /*
  * NSS utilities's major version, minor version, patch level, build number,
  * and whether this is a beta release.
  *
  * The format of the version string should be
  *     "<major version>.<minor version>[.<patch level>[.<build number>]][ <Beta>]"
  */
-#define NSSUTIL_VERSION "3.89 Beta"
+#define NSSUTIL_VERSION "3.89"
 #define NSSUTIL_VMAJOR 3
 #define NSSUTIL_VMINOR 89
 #define NSSUTIL_VPATCH 0
 #define NSSUTIL_VBUILD 0
-#define NSSUTIL_BETA PR_TRUE
+#define NSSUTIL_BETA PR_FALSE
 
 SEC_BEGIN_PROTOS
 
 /*
  * Returns a const string of the UTIL library version.
  */
 extern const char *NSSUTIL_GetVersion(void);
 
