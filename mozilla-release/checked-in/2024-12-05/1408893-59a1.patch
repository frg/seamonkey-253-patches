# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1515153179 -3600
# Node ID 253e9d9481f58e91e517626f171ba20ce38ce9d5
# Parent  f3e1072ceb9f351e581733e770395312ccc9db46
Bug 1408893 - Enable browser_webconsole_eval_in_debugger_stackframe2.js in the new console frontend; r=bgrins.

MozReview-Commit-ID: Ej7flr2u3Ya

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -267,17 +267,16 @@ subsuite = clipboard
 skip-if = (e10s && debug) || (e10s && os == 'win') # Bug 1221499 enabled these on windows
 [browser_webconsole_cspro.js]
 [browser_webconsole_document_focus.js]
 [browser_webconsole_duplicate_errors.js]
 skip-if = true #       Bug 1403907
 [browser_webconsole_errors_after_page_reload.js]
 [browser_webconsole_eval_in_debugger_stackframe.js]
 [browser_webconsole_eval_in_debugger_stackframe2.js]
-skip-if = true # Bug 1408893
 [browser_webconsole_execution_scope.js]
 [browser_webconsole_external_script_errors.js]
 [browser_webconsole_file_uri.js]
 skip-if = true #       Bug 1404382
 [browser_webconsole_filter_scroll.js]
 skip-if = true #       Bug 1404392
 [browser_webconsole_filters.js]
 [browser_webconsole_filters_persist.js]
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_eval_in_debugger_stackframe2.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_eval_in_debugger_stackframe2.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_eval_in_debugger_stackframe2.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_eval_in_debugger_stackframe2.js
@@ -5,67 +5,61 @@
 
 // Test to make sure that web console commands can fire while paused at a
 // breakpoint that was triggered from a JS call.  Relies on asynchronous js
 // evaluation over the protocol - see Bug 1088861.
 
 "use strict";
 
 const TEST_URI = "http://example.com/browser/devtools/client/webconsole/" +
-                 "test/test-eval-in-stackframe.html";
+                 "new-console-output/test/mochitest/test-eval-in-stackframe.html";
 
-// Force the old debugger UI since it's directly used (see Bug 1301705)
-Services.prefs.setBoolPref("devtools.debugger.new-debugger-frontend", false);
-registerCleanupFunction(function* () {
-  Services.prefs.clearUserPref("devtools.debugger.new-debugger-frontend");
-});
+add_task(async function () {
+  // Force the old debugger UI since it's directly used (see Bug 1301705).
+  await pushPref("devtools.debugger.new-debugger-frontend", false);
 
-add_task(function* () {
-  yield loadTab(TEST_URI);
-
-  info("open the web console");
-  let hud = yield openConsole();
-  let {jsterm} = hud;
+  info("open the console");
+  const hud = await openNewTabAndConsole(TEST_URI);
+  const {jsterm} = hud;
 
   info("open the debugger");
-  let {panelWin} = yield openDebugger();
-  let {DebuggerController} = panelWin;
-  let {activeThread} = DebuggerController;
+  let {panel} = await openDebugger();
+  let {activeThread} = panel.panelWin.DebuggerController;
+
+  const onFirstCallFramesAdded = activeThread.addOneTimeListener("framesadded");
+  // firstCall calls secondCall, which has a debugger statement, so we'll be paused.
+  const onFirstCallMessageReceived = waitForMessage(hud, "undefined");
 
-  let firstCall = defer();
-  let frameAdded = defer();
-  executeSoon(() => {
-    info("Executing firstCall");
-    activeThread.addOneTimeListener("framesadded", () => {
-      executeSoon(frameAdded.resolve);
-    });
-    jsterm.execute("firstCall()").then(firstCall.resolve);
+  const unresolvedSymbol = Symbol();
+  let firstCallEvaluationResult = unresolvedSymbol;
+  onFirstCallMessageReceived.then(message => {
+    firstCallEvaluationResult = message;
   });
+  jsterm.execute("firstCall()");
 
   info("Waiting for a frame to be added");
-  yield frameAdded.promise;
+  await onFirstCallFramesAdded;
+
+  info("frames added, select the console again");
+  await openConsole();
 
   info("Executing basic command while paused");
-  yield executeAndConfirm(jsterm, "1 + 2", "3");
+  let onMessageReceived = waitForMessage(hud, "3");
+  jsterm.execute("1 + 2");
+  let message = await onMessageReceived;
+  ok(message, "`1 + 2` was evaluated whith debugger paused");
 
   info("Executing command using scoped variables while paused");
-  yield executeAndConfirm(jsterm, "foo + foo2",
-                          '"globalFooBug783499foo2SecondCall"');
+  onMessageReceived = waitForMessage(hud, `"globalFooBug783499foo2SecondCall"`);
+  jsterm.execute("foo + foo2");
+  message = await onMessageReceived;
+  ok(message, "`foo + foo2` was evaluated as expected with debugger paused");
+
+  info("Checking the first command, which is the last to resolve since it paused");
+  ok(firstCallEvaluationResult === unresolvedSymbol, "firstCall was not evaluated yet");
 
   info("Resuming the thread");
   activeThread.resume();
 
-  info("Checking the first command, which is the last to resolve since it " +
-       "paused");
-  let node = yield firstCall.promise;
-  is(node.querySelector(".message-body").textContent,
-     "undefined",
-     "firstCall() returned correct value");
+  message = await onFirstCallMessageReceived;
+  ok(firstCallEvaluationResult !== unresolvedSymbol,
+    "firstCall() returned correct value");
 });
-
-function* executeAndConfirm(jsterm, input, output) {
-  info("Executing command `" + input + "`");
-
-  let node = yield jsterm.execute(input);
-
-  is(node.querySelector(".message-body").textContent, output,
-     "Expected result from call to " + input);
-}
