# HG changeset patch
# User Yura Zenevich <yura.zenevich@gmail.com>
# Date 1510608997 18000
# Node ID 207a13b699ac7b2a3eb58384dac109c3269552d5
# Parent  5fa423b95ce4c40101046f0b3d382b61a1293f3e
Bug 1416893 - Added getConsumers method to nsIAccessibilityService. r=surkov

MozReview-Commit-ID: EoBdYSxqEGz


diff --git a/accessible/base/nsAccessibilityService.cpp b/accessible/base/nsAccessibilityService.cpp
--- a/accessible/base/nsAccessibilityService.cpp
+++ b/accessible/base/nsAccessibilityService.cpp
@@ -1872,34 +1872,42 @@ nsAccessibilityService::UnsetConsumers(u
     return;
   }
 
   gConsumers &= ~aConsumers;
   NotifyOfConsumersChange();
 }
 
 void
+nsAccessibilityService::GetConsumers(nsAString& aString)
+{
+  const char16_t* kJSONFmt =
+    u"{ \"XPCOM\": %s, \"MainProcess\": %s, \"PlatformAPI\": %s }";
+  nsString json;
+  nsTextFormatter::ssprintf(json, kJSONFmt,
+    gConsumers & eXPCOM ? "true" : "false",
+    gConsumers & eMainProcess ? "true" : "false",
+    gConsumers & ePlatformAPI ? "true" : "false");
+  aString.Assign(json);
+}
+
+void
 nsAccessibilityService::NotifyOfConsumersChange()
 {
   nsCOMPtr<nsIObserverService> observerService =
     mozilla::services::GetObserverService();
 
   if (!observerService) {
     return;
   }
 
-  const char16_t* kJSONFmt =
-    u"{ \"XPCOM\": %s, \"MainProcess\": %s, \"PlatformAPI\": %s }";
-  nsString json;
-  nsTextFormatter::ssprintf(json, kJSONFmt,
-    gConsumers & eXPCOM ? "true" : "false",
-    gConsumers & eMainProcess ? "true" : "false",
-    gConsumers & ePlatformAPI ? "true" : "false");
+  nsAutoString consumers;
+  GetConsumers(consumers);
   observerService->NotifyObservers(
-    nullptr, "a11y-consumers-changed", json.get());
+    nullptr, "a11y-consumers-changed", consumers.get());
 }
 
 nsAccessibilityService*
 GetOrCreateAccService(uint32_t aNewConsumer)
 {
   if (!nsAccessibilityService::gAccessibilityService) {
     RefPtr<nsAccessibilityService> service = new nsAccessibilityService();
     if (!service->Init()) {
diff --git a/accessible/base/nsAccessibilityService.h b/accessible/base/nsAccessibilityService.h
--- a/accessible/base/nsAccessibilityService.h
+++ b/accessible/base/nsAccessibilityService.h
@@ -305,16 +305,21 @@ private:
                                 Accessible* aContext);
 
   /**
    * Notify observers about change of the accessibility service's consumers.
    */
   void NotifyOfConsumersChange();
 
   /**
+   * Get a JSON string representing the accessibility service consumers.
+   */
+  void GetConsumers(nsAString& aString);
+
+  /**
    * Set accessibility service consumers.
    */
   void SetConsumers(uint32_t aConsumers);
 
   /**
    * Unset accessibility service consumers.
    */
   void UnsetConsumers(uint32_t aConsumers);
diff --git a/accessible/interfaces/nsIAccessibilityService.idl b/accessible/interfaces/nsIAccessibilityService.idl
--- a/accessible/interfaces/nsIAccessibilityService.idl
+++ b/accessible/interfaces/nsIAccessibilityService.idl
@@ -11,17 +11,17 @@ interface nsIWeakReference;
 interface nsIPresShell;
 interface nsIAccessiblePivot;
 
 /**
  * An interface for in-process accessibility clients wishing to get an
  * nsIAccessible for a given DOM node.  More documentation at:
  *   http://www.mozilla.org/projects/ui/accessibility
  */
-[scriptable, builtinclass, uuid(9a6f80fe-25cc-405c-9f8f-25869bc9f94e)]
+[scriptable, builtinclass, uuid(2188e3a0-c88e-11e7-8f1a-0800200c9a66)]
 interface nsIAccessibilityService : nsISupports
 {
   /**
    * Return application accessible.
    */
   nsIAccessible getApplicationAccessible();
 
   /**
@@ -92,9 +92,15 @@ interface nsIAccessibilityService : nsIS
    * @see Logging.cpp for list of possible values.
    */
   void setLogging(in ACString aModules);
 
   /**
    * Return true if the given module is logged.
    */
   boolean isLogged(in AString aModule);
+
+  /**
+   * Get the current accessibility service consumers.
+   * @returns a JSON string representing the accessibility service consumers.
+   */
+  AString getConsumers();
 };
diff --git a/accessible/tests/browser/browser_shutdown_remote_no_reference.js b/accessible/tests/browser/browser_shutdown_remote_no_reference.js
--- a/accessible/tests/browser/browser_shutdown_remote_no_reference.js
+++ b/accessible/tests/browser/browser_shutdown_remote_no_reference.js
@@ -34,16 +34,20 @@ add_task(async function() {
     await Promise.all([parentA11yInit, contentA11yInit]);
     await parentConsumersChanged.then(data => Assert.deepEqual(data, {
       XPCOM: true, MainProcess: false, PlatformAPI: false
     }, "Accessibility service consumers in parent are correct."));
     await contentConsumersChanged.then(data => Assert.deepEqual(data, {
       XPCOM: false, MainProcess: true, PlatformAPI: false
     }, "Accessibility service consumers in content are correct."));
 
+    Assert.deepEqual(JSON.parse(accService.getConsumers()), {
+      XPCOM: true, MainProcess: false, PlatformAPI: false
+    }, "Accessibility service consumers in parent are correct.");
+
     info("Removing a service in parent and waiting for service to be shut " +
       "down in content");
     // Remove a11y service reference in the main process.
     let parentA11yShutdown = shutdownPromise();
     let contentA11yShutdown = shutdownPromise(browser);
     parentConsumersChanged = a11yConsumersChangedPromise();
     contentConsumersChanged =
       ContentTask.spawn(browser, {}, a11yConsumersChangedPromise);
diff --git a/accessible/tests/browser/browser_shutdown_remote_own_reference.js b/accessible/tests/browser/browser_shutdown_remote_own_reference.js
--- a/accessible/tests/browser/browser_shutdown_remote_own_reference.js
+++ b/accessible/tests/browser/browser_shutdown_remote_own_reference.js
@@ -42,16 +42,22 @@ add_task(async function() {
     // Add a new reference to the a11y service inside the content process.
     loadFrameScripts(browser, `let accService = Components.classes[
       '@mozilla.org/accessibilityService;1'].getService(
         Components.interfaces.nsIAccessibilityService);`);
     await contentConsumersChanged.then(data => Assert.deepEqual(data, {
       XPCOM: true, MainProcess: true, PlatformAPI: false
     }, "Accessibility service consumers in content are correct."));
 
+    const contentConsumers = await ContentTask.spawn(browser, {}, () =>
+      accService.getConsumers());
+    Assert.deepEqual(JSON.parse(contentConsumers), {
+      XPCOM: true, MainProcess: true, PlatformAPI: false
+    }, "Accessibility service consumers in parent are correct.");
+
     info("Shutting down a service in parent and making sure the one in " +
       "content stays alive");
     let contentCanShutdown = false;
     let parentA11yShutdown = shutdownPromise();
     contentConsumersChanged =
       ContentTask.spawn(browser, {}, a11yConsumersChangedPromise);
     // This promise will resolve only if contentCanShutdown flag is set to true.
     // If 'a11y-init-or-shutdown' event with '0' flag (in content) comes before
diff --git a/accessible/xpcom/xpcAccessibilityService.cpp b/accessible/xpcom/xpcAccessibilityService.cpp
--- a/accessible/xpcom/xpcAccessibilityService.cpp
+++ b/accessible/xpcom/xpcAccessibilityService.cpp
@@ -253,16 +253,28 @@ xpcAccessibilityService::IsLogged(const 
 
 #ifdef A11Y_LOG
   *aIsLogged = logging::IsEnabled(aModule);
 #endif
 
   return NS_OK;
 }
 
+NS_IMETHODIMP
+xpcAccessibilityService::GetConsumers(nsAString& aString)
+{
+  nsAccessibilityService* accService = GetAccService();
+  if (!accService) {
+    return NS_ERROR_SERVICE_NOT_AVAILABLE;
+  }
+
+  accService->GetConsumers(aString);
+  return NS_OK;
+}
+
 ////////////////////////////////////////////////////////////////////////////////
 // NS_GetAccessibilityService
 ////////////////////////////////////////////////////////////////////////////////
 
 nsresult
 NS_GetAccessibilityService(nsIAccessibilityService** aResult)
 {
   NS_ENSURE_TRUE(aResult, NS_ERROR_NULL_POINTER);

