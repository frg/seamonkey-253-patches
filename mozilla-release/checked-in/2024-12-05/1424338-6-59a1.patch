# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1512762403 18000
# Node ID 3d35790aaf573a84e737b336c815cde017cdb882
# Parent  cc506d7ead55ad04984013672d139b0067c82799
Bug 1424338 P6 Implement ClientHandle::PostMessage() r=baku

diff --git a/dom/clients/manager/ClientHandle.cpp b/dom/clients/manager/ClientHandle.cpp
--- a/dom/clients/manager/ClientHandle.cpp
+++ b/dom/clients/manager/ClientHandle.cpp
@@ -7,20 +7,23 @@
 #include "ClientHandle.h"
 
 #include "ClientHandleChild.h"
 #include "ClientHandleOpChild.h"
 #include "ClientManager.h"
 #include "ClientState.h"
 #include "mozilla/dom/PClientManagerChild.h"
 #include "mozilla/dom/ServiceWorkerDescriptor.h"
+#include "mozilla/dom/ipc/StructuredCloneData.h"
 
 namespace mozilla {
 namespace dom {
 
+using mozilla::dom::ipc::StructuredCloneData;
+
 ClientHandle::~ClientHandle()
 {
   Shutdown();
 }
 
 void
 ClientHandle::Shutdown()
 {
@@ -137,10 +140,45 @@ ClientHandle::Focus()
     }, [outerPromise](const ClientOpResult& aResult) {
       outerPromise->Reject(aResult.get_nsresult(), __func__);
     });
 
   RefPtr<ClientStatePromise> ref = outerPromise.get();
   return ref.forget();
 }
 
+RefPtr<GenericPromise>
+ClientHandle::PostMessage(StructuredCloneData& aData,
+                          const ServiceWorkerDescriptor& aSource)
+{
+  RefPtr<GenericPromise> ref;
+
+  if (IsShutdown()) {
+    ref = GenericPromise::CreateAndReject(NS_ERROR_DOM_INVALID_STATE_ERR, __func__);
+    return ref.forget();
+  }
+
+  ClientPostMessageArgs args;
+  args.serviceWorker() = aSource.ToIPC();
+
+  if (!aData.BuildClonedMessageDataForBackgroundChild(GetActor()->Manager()->Manager(),
+                                                      args.clonedData())) {
+    ref = GenericPromise::CreateAndReject(NS_ERROR_DOM_INVALID_STATE_ERR, __func__);
+    return ref.forget();
+  }
+
+  RefPtr<GenericPromise::Private> outerPromise =
+    new GenericPromise::Private(__func__);
+
+  RefPtr<ClientOpPromise> innerPromise = StartOp(args);
+  innerPromise->Then(mSerialEventTarget, __func__,
+    [outerPromise](const ClientOpResult& aResult) {
+      outerPromise->Resolve(true, __func__);
+    }, [outerPromise](const ClientOpResult& aResult) {
+      outerPromise->Reject(aResult.get_nsresult(), __func__);
+    });
+
+  ref = outerPromise.get();
+  return ref.forget();
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/clients/manager/ClientHandle.h b/dom/clients/manager/ClientHandle.h
--- a/dom/clients/manager/ClientHandle.h
+++ b/dom/clients/manager/ClientHandle.h
@@ -20,16 +20,20 @@ namespace mozilla {
 namespace dom {
 
 class ClientManager;
 class ClientHandleChild;
 class ClientOpConstructorArgs;
 class PClientManagerChild;
 class ServiceWorkerDescriptor;
 
+namespace ipc {
+class StructuredCloneData;
+}
+
 // The ClientHandle allows code to take a simple ClientInfo struct and
 // convert it into a live actor-backed object attached to a particular
 // ClientSource somewhere in the browser.  If the ClientSource is
 // destroyed then the ClientHandle will simply begin to reject operations.
 // We do not currently provide a way to be notified when the ClientSource
 // is destroyed, but this could be added in the future.
 class ClientHandle final : public ClientThing<ClientHandleChild>
 {
@@ -71,15 +75,25 @@ public:
   Control(const ServiceWorkerDescriptor& aServiceWorker);
 
   // Focus the Client if possible.  If successful the promise will resolve with
   // a new ClientState snapshot after focus has completed.  If focusing fails
   // for any reason then the promise will reject.
   RefPtr<ClientStatePromise>
   Focus();
 
+  // Send a postMessage() call to the target Client.  Currently this only
+  // supports sending from a ServiceWorker source and the MessageEvent is
+  // dispatched to the Client's navigator.serviceWorker event target.  The
+  // returned promise will resolve if the MessageEvent is dispatched or if
+  // it triggers an error handled in the Client's context.  Other errors
+  // will result in the promise rejecting.
+  RefPtr<GenericPromise>
+  PostMessage(ipc::StructuredCloneData& aData,
+              const ServiceWorkerDescriptor& aSource);
+
   NS_INLINE_DECL_REFCOUNTING(ClientHandle);
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // _mozilla_dom_ClientHandle_h
diff --git a/dom/clients/manager/ClientHandleOpParent.cpp b/dom/clients/manager/ClientHandleOpParent.cpp
--- a/dom/clients/manager/ClientHandleOpParent.cpp
+++ b/dom/clients/manager/ClientHandleOpParent.cpp
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ClientHandleOpParent.h"
 
 #include "ClientHandleParent.h"
 #include "ClientSourceParent.h"
+#include "mozilla/dom/PClientManagerParent.h"
 
 namespace mozilla {
 namespace dom {
 
 ClientSourceParent*
 ClientHandleOpParent::GetSource() const
 {
   auto handle = static_cast<ClientHandleParent*>(Manager());
@@ -29,17 +30,43 @@ void
 ClientHandleOpParent::Init(const ClientOpConstructorArgs& aArgs)
 {
   ClientSourceParent* source = GetSource();
   if (!source) {
     Unused << PClientHandleOpParent::Send__delete__(this, NS_ERROR_DOM_ABORT_ERR);
     return;
   }
 
-  RefPtr<ClientOpPromise> p = source->StartOp(aArgs);
+  RefPtr<ClientOpPromise> p;
+
+  // ClientPostMessageArgs can contain PBlob actors.  This means we
+  // can't just forward the args from one PBackground manager to
+  // another.  Instead, unpack the structured clone data and repack
+  // it into a new set of arguments.
+  if (aArgs.type() == ClientOpConstructorArgs::TClientPostMessageArgs) {
+    const ClientPostMessageArgs& orig = aArgs.get_ClientPostMessageArgs();
+
+    ClientPostMessageArgs rebuild;
+    rebuild.serviceWorker() = orig.serviceWorker();
+
+    StructuredCloneData data;
+    data.BorrowFromClonedMessageDataForBackgroundParent(orig.clonedData());
+    if (!data.BuildClonedMessageDataForBackgroundParent(source->Manager()->Manager(),
+                                                        rebuild.clonedData())) {
+      Unused << PClientHandleOpParent::Send__delete__(this, NS_ERROR_DOM_ABORT_ERR);
+      return;
+    }
+
+    p = source->StartOp(rebuild);
+  }
+
+  // Other argument types can just be forwarded straight through.
+  else {
+    p = source->StartOp(aArgs);
+  }
 
   // Capturing 'this' is safe here because we disconnect the promise in
   // ActorDestroy() which ensures neither lambda is called if the actor
   // is destroyed before the source operation completes.
   p->Then(GetCurrentThreadSerialEventTarget(), __func__,
     [this] (const ClientOpResult& aResult) {
       mPromiseRequestHolder.Complete();
       Unused << PClientHandleOpParent::Send__delete__(this, aResult);
diff --git a/dom/clients/manager/ClientIPCTypes.ipdlh b/dom/clients/manager/ClientIPCTypes.ipdlh
--- a/dom/clients/manager/ClientIPCTypes.ipdlh
+++ b/dom/clients/manager/ClientIPCTypes.ipdlh
@@ -79,16 +79,22 @@ struct ClientNavigateArgs
 };
 
 union ClientEndPoint
 {
   IPCClientInfo;
   IPCServiceWorkerDescriptor;
 };
 
+struct ClientPostMessageArgs
+{
+  ClonedMessageData clonedData;
+  IPCServiceWorkerDescriptor serviceWorker;
+};
+
 struct ClientMatchAllArgs
 {
   ClientEndPoint endpoint;
   ClientType type;
   bool includeUncontrolled;
 };
 
 struct ClientClaimArgs
@@ -109,16 +115,17 @@ struct ClientOpenWindowArgs
   nsCString baseURL;
 };
 
 union ClientOpConstructorArgs
 {
   ClientControlledArgs;
   ClientFocusArgs;
   ClientNavigateArgs;
+  ClientPostMessageArgs;
   ClientMatchAllArgs;
   ClientClaimArgs;
   ClientGetInfoAndStateArgs;
   ClientOpenWindowArgs;
 };
 
 struct ClientList
 {
diff --git a/dom/clients/manager/ClientSource.cpp b/dom/clients/manager/ClientSource.cpp
--- a/dom/clients/manager/ClientSource.cpp
+++ b/dom/clients/manager/ClientSource.cpp
@@ -7,26 +7,39 @@
 #include "ClientSource.h"
 
 #include "ClientManager.h"
 #include "ClientManagerChild.h"
 #include "ClientSourceChild.h"
 #include "ClientState.h"
 #include "ClientValidation.h"
 #include "mozilla/dom/ClientIPCTypes.h"
+#include "mozilla/dom/ipc/StructuredCloneData.h"
+#include "mozilla/dom/MessageEvent.h"
+#include "mozilla/dom/MessageEventBinding.h"
+#include "mozilla/dom/Navigator.h"
 #include "mozilla/dom/WorkerPrivate.h"
+#include "mozilla/dom/WorkerScope.h"
+#include "mozilla/dom/ServiceWorkerContainer.h"
+#include "mozilla/dom/workers/ServiceWorkerManager.h"
+#include "mozilla/dom/workers/bindings/ServiceWorker.h"
 #include "nsContentUtils.h"
 #include "nsIDocShell.h"
 #include "nsPIDOMWindow.h"
 
 namespace mozilla {
 namespace dom {
 
+using mozilla::dom::ipc::StructuredCloneData;
+using mozilla::dom::workers::ServiceWorkerInfo;
+using mozilla::dom::workers::ServiceWorkerManager;
+using mozilla::dom::workers::ServiceWorkerRegistrationInfo;
 using mozilla::dom::workers::WorkerPrivate;
 using mozilla::ipc::PrincipalInfo;
+using mozilla::ipc::PrincipalInfoToPrincipal;
 
 void
 ClientSource::Shutdown()
 {
   NS_ASSERT_OWNINGTHREAD(ClientSource);
   if (IsShutdown()) {
     return;
   }
@@ -407,16 +420,138 @@ ClientSource::Focus(const ClientFocusArg
     return ref.forget();
   }
 
   ref = ClientOpPromise::CreateAndResolve(state.ToIPC(), __func__);
   return ref.forget();
 }
 
 RefPtr<ClientOpPromise>
+ClientSource::PostMessage(const ClientPostMessageArgs& aArgs)
+{
+  NS_ASSERT_OWNINGTHREAD(ClientSource);
+  RefPtr<ClientOpPromise> ref;
+
+  ServiceWorkerDescriptor source(aArgs.serviceWorker());
+  const PrincipalInfo& principalInfo = source.PrincipalInfo();
+
+  StructuredCloneData clonedData;
+  clonedData.BorrowFromClonedMessageDataForBackgroundChild(aArgs.clonedData());
+
+  // Currently we only support firing these messages on window Clients.
+  // Once we expose ServiceWorkerContainer and the ServiceWorker on Worker
+  // threads then this will need to change.  See bug 1113522.
+  if (mClientInfo.Type() != ClientType::Window) {
+    ref = ClientOpPromise::CreateAndReject(NS_ERROR_NOT_IMPLEMENTED, __func__);
+    return ref.forget();
+  }
+
+  MOZ_ASSERT(NS_IsMainThread());
+
+  RefPtr<ServiceWorkerContainer> target;
+  nsCOMPtr<nsIGlobalObject> globalObject;
+
+  // We don't need to force the creation of the about:blank document
+  // here because there is no postMessage listener.  If a listener
+  // was registered then the document will already be created.
+  nsPIDOMWindowInner* window = GetInnerWindow();
+  if (window) {
+    globalObject = do_QueryInterface(window);
+    RefPtr<Navigator> navigator =
+      static_cast<Navigator*>(window->GetNavigator());
+    if (navigator) {
+      target = navigator->ServiceWorker();
+    }
+  }
+
+  if (NS_WARN_IF(!target)) {
+    ref = ClientOpPromise::CreateAndReject(NS_ERROR_DOM_INVALID_STATE_ERR,
+                                           __func__);
+    return ref.forget();
+  }
+
+  // If AutoJSAPI::Init() fails then either global is nullptr or not
+  // in a usable state.
+  AutoJSAPI jsapi;
+  if (!jsapi.Init(globalObject)) {
+    ref = ClientOpPromise::CreateAndResolve(NS_OK, __func__);
+    return ref.forget();
+  }
+
+  JSContext* cx = jsapi.cx();
+
+  ErrorResult result;
+  JS::Rooted<JS::Value> messageData(cx);
+  clonedData.Read(cx, &messageData, result);
+  if (result.MaybeSetPendingException(cx)) {
+    // We reported the error in the current window context.  Resolve
+    // promise instead of rejecting.
+    ref = ClientOpPromise::CreateAndResolve(NS_OK, __func__);
+    return ref.forget();
+  }
+
+  RootedDictionary<MessageEventInit> init(cx);
+
+  init.mData = messageData;
+  if (!clonedData.TakeTransferredPortsAsSequence(init.mPorts)) {
+    // Report the error in the current window context and resolve the
+    // promise instead of rejecting.
+    xpc::Throw(cx, NS_ERROR_OUT_OF_MEMORY);
+    ref = ClientOpPromise::CreateAndResolve(NS_OK, __func__);
+    return ref.forget();
+  }
+
+  nsresult rv = NS_OK;
+  nsCOMPtr<nsIPrincipal> principal =
+    PrincipalInfoToPrincipal(principalInfo, &rv);
+  if (NS_FAILED(rv) || !principal) {
+    ref = ClientOpPromise::CreateAndReject(NS_ERROR_FAILURE, __func__);
+    return ref.forget();
+  }
+
+  nsAutoCString origin;
+  rv = principal->GetOriginNoSuffix(origin);
+  if (NS_SUCCEEDED(rv)) {
+    CopyUTF8toUTF16(origin, init.mOrigin);
+  }
+
+  RefPtr<ServiceWorkerManager> swm = ServiceWorkerManager::GetInstance();
+  if (!swm) {
+    // Shutting down. Just don't deliver this message.
+    ref = ClientOpPromise::CreateAndReject(NS_ERROR_FAILURE, __func__);
+    return ref.forget();
+  }
+
+  RefPtr<ServiceWorkerRegistrationInfo> reg =
+    swm->GetRegistration(principal, source.Scope());
+  if (reg) {
+    RefPtr<ServiceWorkerInfo> serviceWorker = reg->GetByID(source.Id());
+    if (serviceWorker) {
+      init.mSource.SetValue().SetAsServiceWorker() =
+        serviceWorker->GetOrCreateInstance(GetInnerWindow());
+    }
+  }
+
+  RefPtr<MessageEvent> event =
+    MessageEvent::Constructor(target, NS_LITERAL_STRING("message"), init);
+  event->SetTrusted(true);
+
+  bool preventDefaultCalled = false;
+  rv = target->DispatchEvent(static_cast<dom::Event*>(event.get()),
+                             &preventDefaultCalled);
+  if (NS_FAILED(rv)) {
+    ref = ClientOpPromise::CreateAndReject(NS_ERROR_FAILURE, __func__);
+    return ref.forget();
+  }
+
+  ref = ClientOpPromise::CreateAndResolve(NS_OK, __func__);
+  return ref.forget();
+}
+
+RefPtr<ClientOpPromise>
 ClientSource::Claim(const ClientClaimArgs& aArgs)
 {
   SetController(ServiceWorkerDescriptor(aArgs.serviceWorker()));
 
   RefPtr<ClientOpPromise> ref =
     ClientOpPromise::CreateAndResolve(NS_OK, __func__);
 
   return ref.forget();
diff --git a/dom/clients/manager/ClientSource.h b/dom/clients/manager/ClientSource.h
--- a/dom/clients/manager/ClientSource.h
+++ b/dom/clients/manager/ClientSource.h
@@ -7,30 +7,38 @@
 #define _mozilla_dom_ClientSource_h
 
 #include "mozilla/dom/ClientInfo.h"
 #include "mozilla/dom/ClientOpPromise.h"
 #include "mozilla/dom/ClientThing.h"
 #include "mozilla/dom/ServiceWorkerDescriptor.h"
 #include "mozilla/Variant.h"
 
+#ifdef XP_WIN
+#undef PostMessage
+#endif
+
 class nsIDocShell;
 class nsISerialEventTarget;
 class nsPIDOMWindowInner;
 
 namespace mozilla {
 namespace dom {
 
 class ClientClaimArgs;
 class ClientControlledArgs;
 class ClientFocusArgs;
+class ClientGetInfoAndStateArgs;
 class ClientManager;
+class ClientPostMessageArgs;
 class ClientSourceChild;
 class ClientSourceConstructorArgs;
 class ClientSourceExecutionReadyArgs;
+class ClientState;
+class ClientWindowState;
 class PClientManagerChild;
 
 namespace workers {
 class WorkerPrivate;
 } // workers namespace
 
 // ClientSource is an RAII style class that is designed to be held via
 // a UniquePtr<>.  When created ClientSource will register the existence
@@ -132,16 +140,19 @@ public:
   // been set.
   const Maybe<ServiceWorkerDescriptor>&
   GetController() const;
 
   RefPtr<ClientOpPromise>
   Focus(const ClientFocusArgs& aArgs);
 
   RefPtr<ClientOpPromise>
+  PostMessage(const ClientPostMessageArgs& aArgs);
+
+  RefPtr<ClientOpPromise>
   Claim(const ClientClaimArgs& aArgs);
 
   RefPtr<ClientOpPromise>
   GetInfoAndState(const ClientGetInfoAndStateArgs& aArgs);
 
   nsresult
   SnapshotState(ClientState* aStateOut);
 
diff --git a/dom/clients/manager/ClientSourceOpChild.cpp b/dom/clients/manager/ClientSourceOpChild.cpp
--- a/dom/clients/manager/ClientSourceOpChild.cpp
+++ b/dom/clients/manager/ClientSourceOpChild.cpp
@@ -79,16 +79,21 @@ ClientSourceOpChild::Init(const ClientOp
       DoSourceOp(&ClientSource::Control, aArgs.get_ClientControlledArgs());
       break;
     }
     case ClientOpConstructorArgs::TClientFocusArgs:
     {
       DoSourceOp(&ClientSource::Focus, aArgs.get_ClientFocusArgs());
       break;
     }
+    case ClientOpConstructorArgs::TClientPostMessageArgs:
+    {
+      DoSourceOp(&ClientSource::PostMessage, aArgs.get_ClientPostMessageArgs());
+      break;
+    }
     case ClientOpConstructorArgs::TClientClaimArgs:
     {
       DoSourceOp(&ClientSource::Claim, aArgs.get_ClientClaimArgs());
       break;
     }
     case ClientOpConstructorArgs::TClientGetInfoAndStateArgs:
     {
       DoSourceOp(&ClientSource::GetInfoAndState,
