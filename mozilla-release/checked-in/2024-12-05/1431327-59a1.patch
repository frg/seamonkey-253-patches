# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1516178179 -3600
# Node ID 1a4f40cd0bea7855f76b2237819e07e89fd89d75
# Parent  333fd01b9a545f864286f3886c411b61515f096e
Bug 1431327 - Refactor setupStore helper function; r=bgrins.

Change the signature of setupStore to accept an option object as
its second parameter so it's more easy to remember what it does
when reading the consumer code.
Also, pass the wrapped actions object to setupStore so we do use
the same generator when adding messages later.
Add more group messages in the group test to make sure group are being
closed when a groupEnd message is passed in a messagesAdd batch.

MozReview-Commit-ID: CBN0r8nBaAr

diff --git a/devtools/client/webconsole/new-console-output/test/components/console-api-call.test.js b/devtools/client/webconsole/new-console-output/test/components/console-api-call.test.js
--- a/devtools/client/webconsole/new-console-output/test/components/console-api-call.test.js
+++ b/devtools/client/webconsole/new-console-output/test/components/console-api-call.test.js
@@ -265,17 +265,17 @@ describe("ConsoleAPICall component:", ()
       const secondElementStyle = elements.eq(1).prop("style");
       // Allowed styles are applied accordingly on the second element.
       expect(secondElementStyle.color).toBe(`red`);
       // Forbidden styles are not applied.
       expect(secondElementStyle.background).toBe(undefined);
     });
 
     it("toggle the group when the collapse button is clicked", () => {
-      const store = setupStore([]);
+      const store = setupStore();
       store.dispatch = sinon.spy();
       const message = stubPreparedMessages.get("console.group('bar')");
 
       let wrapper = mount(Provider({store},
         ConsoleApiCall({
           message,
           open: true,
           dispatch: store.dispatch,
diff --git a/devtools/client/webconsole/new-console-output/test/components/evaluation-result.test.js b/devtools/client/webconsole/new-console-output/test/components/evaluation-result.test.js
--- a/devtools/client/webconsole/new-console-output/test/components/evaluation-result.test.js
+++ b/devtools/client/webconsole/new-console-output/test/components/evaluation-result.test.js
@@ -60,17 +60,17 @@ describe("EvaluationResult component:", 
     const message = stubPreparedMessages.get("cd(document)");
     const wrapper = render(EvaluationResult({ message, serviceContainer }));
 
     expect(wrapper.find(".message-body").text())
       .toBe("Cannot cd() to the given window. Invalid argument.");
   });
 
   it("displays a [Learn more] link", () => {
-    const store = setupStore([]);
+    const store = setupStore();
 
     const message = stubPreparedMessages.get("asdf()");
 
     serviceContainer.openLink = sinon.spy();
     const wrapper = mount(Provider({store},
       EvaluationResult({message, serviceContainer})
     ));
 
diff --git a/devtools/client/webconsole/new-console-output/test/components/filter-bar.test.js b/devtools/client/webconsole/new-console-output/test/components/filter-bar.test.js
--- a/devtools/client/webconsole/new-console-output/test/components/filter-bar.test.js
+++ b/devtools/client/webconsole/new-console-output/test/components/filter-bar.test.js
@@ -26,17 +26,17 @@ const serviceContainer = require("devtoo
 const ServicesMock = require("Services");
 
 describe("FilterBar component:", () => {
   afterEach(() => {
     ServicesMock.prefs.testHelpers.clearPrefs();
   });
 
   it("initial render", () => {
-    const store = setupStore([]);
+    const store = setupStore();
 
     const wrapper = render(Provider({store}, FilterBar({ serviceContainer })));
     const toolbar = wrapper.find(
       ".devtools-toolbar.webconsole-filterbar-primary"
     );
 
     // Clear button
     const clearButton = toolbar.children().eq(0);
@@ -164,17 +164,17 @@ describe("FilterBar component:", () => {
     expect(toolbar.exists()).toBeTruthy();
 
     const message = toolbar.find(".filter-message-text");
     expect(message.text()).toBe("10 items hidden by filters");
     expect(message.prop("title")).toBe("error: 2, warn: 2, log: 2, info: 2, debug: 2");
   });
 
   it("does not display the number of hidden messages when there are no messages", () => {
-    const store = setupStore([]);
+    const store = setupStore();
     const wrapper = mount(Provider({store}, FilterBar({ serviceContainer })));
     const toolbar = wrapper.find(".webconsole-filterbar-filtered-messages");
     expect(toolbar.exists()).toBeFalsy();
   });
 
   it("does not display the number of hidden non-default filters (CSS, Network,…)", () => {
     const store = setupStore([
       "Unknown property ‘such-unknown-property’.  Declaration dropped.",
@@ -189,17 +189,17 @@ describe("FilterBar component:", () => {
     expect(filters[FILTERS.NET]).toBe(false);
     expect(filters[FILTERS.NETXHR]).toBe(false);
 
     const toolbar = wrapper.find(".webconsole-filterbar-filtered-messages");
     expect(toolbar.exists()).toBeFalsy();
   });
 
   it("displays filter bar when button is clicked", () => {
-    const store = setupStore([]);
+    const store = setupStore();
 
     expect(getAllUi(store.getState()).filterBarVisible).toBe(false);
     expect(ServicesMock.prefs.getBoolPref(PREFS.UI.FILTER_BAR), false);
 
     const wrapper = mount(Provider({store}, FilterBar({ serviceContainer })));
     wrapper.find(".devtools-filter-icon").simulate("click");
 
     expect(getAllUi(store.getState()).filterBarVisible).toBe(true);
@@ -231,37 +231,37 @@ describe("FilterBar component:", () => {
     ];
 
     secondaryBar.children().forEach((child, index) => {
       expect(child.html()).toEqual(shallow(buttons[index]).html());
     });
   });
 
   it("fires MESSAGES_CLEAR action when clear button is clicked", () => {
-    const store = setupStore([]);
+    const store = setupStore();
     store.dispatch = sinon.spy();
 
     const wrapper = mount(Provider({store}, FilterBar({ serviceContainer })));
     wrapper.find(".devtools-clear-icon").simulate("click");
     const call = store.dispatch.getCall(0);
     expect(call.args[0]).toEqual({
       type: MESSAGES_CLEAR
     });
   });
 
   it("sets filter text when text is typed", () => {
-    const store = setupStore([]);
+    const store = setupStore();
 
     const wrapper = mount(Provider({store}, FilterBar({ serviceContainer })));
     wrapper.find(".devtools-plaininput").simulate("input", { target: { value: "a" } });
     expect(store.getState().filters.text).toBe("a");
   });
 
   it("toggles persist logs when checkbox is clicked", () => {
-    const store = setupStore([]);
+    const store = setupStore();
 
     expect(getAllUi(store.getState()).persistLogs).toBe(false);
     expect(ServicesMock.prefs.getBoolPref(PREFS.UI.PERSIST), false);
 
     const wrapper = mount(Provider({store}, FilterBar({ serviceContainer })));
     wrapper.find(".filter-checkbox input").simulate("change");
 
     expect(getAllUi(store.getState()).persistLogs).toBe(true);
diff --git a/devtools/client/webconsole/new-console-output/test/components/page-error.test.js b/devtools/client/webconsole/new-console-output/test/components/page-error.test.js
--- a/devtools/client/webconsole/new-console-output/test/components/page-error.test.js
+++ b/devtools/client/webconsole/new-console-output/test/components/page-error.test.js
@@ -65,17 +65,17 @@ describe("PageError component:", () => {
     const message = stubPreparedMessages.get("TypeError longString message");
     const wrapper = render(PageError({ message, serviceContainer }));
 
     const text = wrapper.find(".message-body").text();
     expect(text.startsWith("Error: Long error Long error")).toBe(true);
   });
 
   it("displays a [Learn more] link", () => {
-    const store = setupStore([]);
+    const store = setupStore();
 
     const message = stubPreparedMessages.get("ReferenceError: asdf is not defined");
 
     serviceContainer.openLink = sinon.spy();
     const wrapper = mount(Provider({store},
       PageError({message, serviceContainer})
     ));
 
@@ -99,17 +99,17 @@ describe("PageError component:", () => {
     expect(wrapper.find(".theme-twisty.open").length).toBe(1);
 
     // There should be five stacktrace items.
     const frameLinks = wrapper.find(`.stack-trace span.frame-link`);
     expect(frameLinks.length).toBe(5);
   });
 
   it("toggle the stacktrace when the collapse button is clicked", () => {
-    const store = setupStore([]);
+    const store = setupStore();
     store.dispatch = sinon.spy();
     const message = stubPreparedMessages.get("ReferenceError: asdf is not defined");
 
     let wrapper = mount(Provider({store},
       PageError({
         message,
         open: true,
         dispatch: store.dispatch,
diff --git a/devtools/client/webconsole/new-console-output/test/helpers.js b/devtools/client/webconsole/new-console-output/test/helpers.js
--- a/devtools/client/webconsole/new-console-output/test/helpers.js
+++ b/devtools/client/webconsole/new-console-output/test/helpers.js
@@ -4,60 +4,64 @@
 "use strict";
 
 let ReactDOM = require("devtools/client/shared/vendor/react-dom");
 let React = require("devtools/client/shared/vendor/react");
 const dom = require("devtools/client/shared/vendor/react-dom-factories");
 const { createElement } = React;
 const TestUtils = ReactDOM.TestUtils;
 
-const actions = require("devtools/client/webconsole/new-console-output/actions/index");
+const reduxActions = require("devtools/client/webconsole/new-console-output/actions/index");
 const { configureStore } = require("devtools/client/webconsole/new-console-output/store");
 const { IdGenerator } = require("devtools/client/webconsole/new-console-output/utils/id-generator");
 const { stubPackets } = require("devtools/client/webconsole/new-console-output/test/fixtures/stubs/index");
 const {
   getAllMessagesById,
 } = require("devtools/client/webconsole/new-console-output/selectors/messages");
 
 /**
  * Prepare actions for use in testing.
  */
 function setupActions() {
   // Some actions use dependency injection. This helps them avoid using state in
   // a hard-to-test way. We need to inject stubbed versions of these dependencies.
-  const wrappedActions = Object.assign({}, actions);
+  const wrappedActions = Object.assign({}, reduxActions);
 
   const idGenerator = new IdGenerator();
   wrappedActions.messagesAdd = (packets) => {
-    return actions.messagesAdd(packets, idGenerator);
+    return reduxActions.messagesAdd(packets, idGenerator);
   };
 
   return {
-    ...actions,
-    messagesAdd: packets => actions.messagesAdd(packets, idGenerator)
+    ...reduxActions,
+    messagesAdd: packets => reduxActions.messagesAdd(packets, idGenerator)
   };
 }
 
 /**
  * Prepare the store for use in testing.
  */
-function setupStore(input = [], hud, options, wrappedActions) {
+function setupStore(input = [], {
+  storeOptions,
+  actions,
+  hud,
+} = {}) {
   if (!hud) {
     hud = {
       proxy: {
         releaseActor: () => {}
       }
     };
   }
-  const store = configureStore(hud, options);
+  const store = configureStore(hud, storeOptions);
 
   // Add the messages from the input commands to the store.
-  const messagesAdd = wrappedActions
-    ? wrappedActions.messagesAdd
-    : actions.messagesAdd;
+  const messagesAdd = actions
+    ? actions.messagesAdd
+    : reduxActions.messagesAdd;
   store.dispatch(messagesAdd(input.map(cmd => stubPackets.get(cmd))));
 
   return store;
 }
 
 function renderComponent(component, props) {
   const el = createElement(component, props, {});
   // By default, renderIntoDocument() won't work for stateless components, but
diff --git a/devtools/client/webconsole/new-console-output/test/store/filters.test.js b/devtools/client/webconsole/new-console-output/test/store/filters.test.js
--- a/devtools/client/webconsole/new-console-output/test/store/filters.test.js
+++ b/devtools/client/webconsole/new-console-output/test/store/filters.test.js
@@ -199,17 +199,17 @@ describe("Filtering", () => {
   describe("Combined filters", () => {
     // @TODO add test
     it("filters");
   });
 });
 
 describe("Clear filters", () => {
   it("clears all filters", () => {
-    const store = setupStore([]);
+    const store = setupStore();
 
     // Setup test case
     store.dispatch(actions.filterToggle(FILTERS.ERROR));
     store.dispatch(actions.filterToggle(FILTERS.CSS));
     store.dispatch(actions.filterToggle(FILTERS.NET));
     store.dispatch(actions.filterToggle(FILTERS.NETXHR));
     store.dispatch(actions.filterTextSet("foobar"));
 
@@ -261,17 +261,17 @@ describe("Clear filters", () => {
       [PREFS.FILTER.NETXHR]: false,
       [PREFS.FILTER.WARN]: true,
     });
   });
 });
 
 describe("Resets filters", () => {
   it("resets default filters value to their original one.", () => {
-    const store = setupStore([]);
+    const store = setupStore();
 
     // Setup test case
     store.dispatch(actions.filterToggle(FILTERS.ERROR));
     store.dispatch(actions.filterToggle(FILTERS.LOG));
     store.dispatch(actions.filterToggle(FILTERS.CSS));
     store.dispatch(actions.filterToggle(FILTERS.NET));
     store.dispatch(actions.filterToggle(FILTERS.NETXHR));
     store.dispatch(actions.filterTextSet("foobar"));
diff --git a/devtools/client/webconsole/new-console-output/test/store/messages.test.js b/devtools/client/webconsole/new-console-output/test/store/messages.test.js
--- a/devtools/client/webconsole/new-console-output/test/store/messages.test.js
+++ b/devtools/client/webconsole/new-console-output/test/store/messages.test.js
@@ -31,36 +31,36 @@ describe("Message reducer:", () => {
   let actions;
 
   before(() => {
     actions = setupActions();
   });
 
   describe("messagesById", () => {
     it("adds a message to an empty store", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const packet = stubPackets.get("console.log('foobar', 'test')");
       const message = stubPreparedMessages.get("console.log('foobar', 'test')");
       dispatch(actions.messagesAdd([packet]));
 
       expect(getFirstMessage(getState())).toEqual(message);
     });
 
     it("increments repeat on a repeating log message", () => {
       const key1 = "console.log('foobar', 'test')";
-      const { dispatch, getState } = setupStore([key1, key1]);
+      const { dispatch, getState } = setupStore([key1, key1], {actions});
 
       const packet = clonePacket(stubPackets.get(key1));
+      const packet2 = clonePacket(packet);
 
       // Repeat ID must be the same even if the timestamp is different.
       packet.message.timeStamp = 1;
-      dispatch(actions.messagesAdd([packet]));
-      packet.message.timeStamp = 2;
-      dispatch(actions.messagesAdd([packet]));
+      packet2.message.timeStamp = 2;
+      dispatch(actions.messagesAdd([packet, packet2]));
 
       const messages = getAllMessagesById(getState());
 
       expect(messages.size).toBe(1);
       const repeat = getAllRepeatById(getState());
       expect(repeat[getFirstMessage(getState()).id]).toBe(4);
     });
 
@@ -179,21 +179,22 @@ describe("Message reducer:", () => {
     it("cleans the repeatsById object when messages are pruned", () => {
       const { dispatch, getState } = setupStore(
         [
           "console.log('foobar', 'test')",
           "console.log('foobar', 'test')",
           "console.log(undefined)",
           "console.log(undefined)",
         ],
-        null, {
-          logLimit: 2
-        },
-        actions
-      );
+        {
+          actions,
+          storeOptions: {
+            logLimit: 2
+          }
+        });
 
       // Check that we have the expected data.
       let repeats = getAllRepeatById(getState());
       expect(Object.keys(repeats).length).toBe(2);
       const lastMessageId = getLastMessage(getState()).id;
 
       // This addition will prune the first message out of the store.
       let packet = stubPackets.get("console.log('foobar', 'test')");
@@ -210,17 +211,17 @@ describe("Message reducer:", () => {
       packet = stubPackets.get("console.log(undefined)");
       dispatch(actions.messagesAdd([packet]));
 
       // repeatById should now be empty.
       expect(getAllRepeatById(getState())).toEqual({});
     });
 
     it("properly limits number of messages", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const logLimit = 1000;
       const packet = clonePacket(stubPackets.get("console.log(undefined)"));
 
       for (let i = 1; i <= logLimit + 2; i++) {
         packet.message.arguments = [`message num ${i}`];
         dispatch(actions.messagesAdd([packet]));
       }
@@ -228,17 +229,17 @@ describe("Message reducer:", () => {
       const messages = getAllMessagesById(getState());
       expect(messages.size).toBe(logLimit);
       expect(getFirstMessage(getState()).parameters[0]).toBe(`message num 3`);
       expect(getLastMessage(getState()).parameters[0])
         .toBe(`message num ${logLimit + 2}`);
     });
 
     it("properly limits number of messages when there are nested groups", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const logLimit = 1000;
 
       const packet = clonePacket(stubPackets.get("console.log(undefined)"));
       const packetGroup = clonePacket(stubPackets.get("console.group('bar')"));
       const packetGroupEnd = clonePacket(stubPackets.get("console.groupEnd()"));
 
       packetGroup.message.arguments = [`group-1`];
@@ -274,17 +275,17 @@ describe("Message reducer:", () => {
 
       // The groups were cleaned up.
       const groups = getGroupsById(getState());
       expect(groups.size).toBe(0);
     });
 
     it("properly limits number of groups", () => {
       const logLimit = 100;
-      const { dispatch, getState } = setupStore([], null, {logLimit});
+      const { dispatch, getState } = setupStore([], {storeOptions: {logLimit}});
 
       const packet = clonePacket(stubPackets.get("console.log(undefined)"));
       const packetGroup = clonePacket(stubPackets.get("console.group('bar')"));
       const packetGroupEnd = clonePacket(stubPackets.get("console.groupEnd()"));
 
       for (let i = 0; i < logLimit + 2; i++) {
         dispatch(actions.messagesAdd([packetGroup]));
         packet.message.arguments = [`message-${i}-a`];
@@ -304,17 +305,17 @@ describe("Message reducer:", () => {
       expect(groups.size).toBe(logLimit);
 
       expect(messages.get(visibleMessages[1]).parameters[0]).toBe(`message-2-a`);
       expect(getLastMessage(getState()).parameters[0]).toBe(`message-${logLimit + 1}-b`);
     });
 
     it("properly limits number of collapsed groups", () => {
       const logLimit = 100;
-      const { dispatch, getState } = setupStore([], null, {logLimit});
+      const { dispatch, getState } = setupStore([], {storeOptions: {logLimit}});
 
       const packet = clonePacket(stubPackets.get("console.log(undefined)"));
       const packetGroupCollapsed = clonePacket(
         stubPackets.get("console.groupCollapsed('foo')"));
       const packetGroupEnd = clonePacket(stubPackets.get("console.groupEnd()"));
 
       for (let i = 0; i < logLimit + 2; i++) {
         packetGroupCollapsed.message.arguments = [`group-${i}`];
@@ -340,47 +341,47 @@ describe("Message reducer:", () => {
       const visibleMessages = getVisibleMessages(getState());
       expect(visibleMessages.length).toBe(logLimit);
       const lastVisibleMessageId = visibleMessages[visibleMessages.length - 1];
       expect(messages.get(lastVisibleMessageId).parameters[0])
         .toBe(`group-${logLimit + 1}`);
     });
 
     it("does not add null messages to the store", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const message = stubPackets.get("console.time('bar')");
       dispatch(actions.messagesAdd([message]));
 
       const messages = getAllMessagesById(getState());
       expect(messages.size).toBe(0);
     });
 
     it("adds console.table call with unsupported type as console.log", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const packet = stubPackets.get("console.table('bar')");
       dispatch(actions.messagesAdd([packet]));
 
       const tableMessage = getLastMessage(getState());
       expect(tableMessage.level).toEqual(MESSAGE_TYPE.LOG);
     });
 
     it("adds console.group messages to the store", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const message = stubPackets.get("console.group('bar')");
       dispatch(actions.messagesAdd([message]));
 
       const messages = getAllMessagesById(getState());
       expect(messages.size).toBe(1);
     });
 
     it("adds messages in console.group to the store", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const groupPacket = stubPackets.get("console.group('bar')");
       const groupEndPacket = stubPackets.get("console.groupEnd('bar')");
       const logPacket = stubPackets.get("console.log('foobar', 'test')");
 
       const packets = [
         groupPacket,
         logPacket,
@@ -411,64 +412,64 @@ describe("Message reducer:", () => {
 
       const messages = getAllMessagesById(getState());
       const visibleMessages = getVisibleMessages(getState());
       expect(messages.size).toBe(messageCount);
       expect(visibleMessages.length).toBe(messageCount);
     });
 
     it("sets groupId property as expected", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       dispatch(actions.messagesAdd([
         stubPackets.get("console.group('bar')"),
         stubPackets.get("console.log('foobar', 'test')")
       ]));
 
       const messages = getAllMessagesById(getState());
       expect(messages.size).toBe(2);
       expect(getLastMessage(getState()).groupId).toBe(getFirstMessage(getState()).id);
     });
 
     it("does not display console.groupEnd messages to the store", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const message = stubPackets.get("console.groupEnd('bar')");
       dispatch(actions.messagesAdd([message]));
 
       const messages = getAllMessagesById(getState());
       expect(messages.size).toBe(0);
     });
 
     it("filters out message added after a console.groupCollapsed message", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       dispatch(actions.messagesAdd([
         stubPackets.get("console.groupCollapsed('foo')"),
         stubPackets.get("console.log('foobar', 'test')"),
       ]));
 
       const messages = getVisibleMessages(getState());
       expect(messages.length).toBe(1);
     });
 
     it("adds console.dirxml call as console.log", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const packet = stubPackets.get("console.dirxml(window)");
       dispatch(actions.messagesAdd([packet]));
 
       const dirxmlMessage = getLastMessage(getState());
       expect(dirxmlMessage.level).toEqual(MESSAGE_TYPE.LOG);
     });
   });
 
   describe("expandedMessageIds", () => {
     it("opens console.trace messages when they are added", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const message = stubPackets.get("console.trace()");
       dispatch(actions.messagesAdd([message]));
 
       const expanded = getAllMessagesUiById(getState());
       expect(expanded.length).toBe(1);
       expect(expanded[0]).toBe(getFirstMessage(getState()).id);
     });
@@ -486,18 +487,20 @@ describe("Message reducer:", () => {
 
       const expanded = getAllMessagesUiById(getState());
       expect(expanded.length).toBe(0);
     });
 
     it("cleans the messages UI list when messages are pruned", () => {
       const { dispatch, getState } = setupStore(
         ["console.trace()", "console.log(undefined)", "console.trace()"],
-        null, {
-          logLimit: 3
+        {
+          storeOptions: {
+            logLimit: 3
+          }
         }
       );
 
       // Check that we have the expected data.
       let expanded = getAllMessagesUiById(getState());
       expect(expanded.length).toBe(2);
       expect(expanded[0]).toBe(getFirstMessage(getState()).id);
       const lastMessageId = getLastMessage(getState()).id;
@@ -519,28 +522,28 @@ describe("Message reducer:", () => {
       packet = stubPackets.get("console.log(undefined)");
       dispatch(actions.messagesAdd([packet]));
 
       // expandedMessageIds should now be empty.
       expect(getAllMessagesUiById(getState()).length).toBe(0);
     });
 
     it("opens console.group messages when they are added", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const message = stubPackets.get("console.group('bar')");
       dispatch(actions.messagesAdd([message]));
 
       const expanded = getAllMessagesUiById(getState());
       expect(expanded.length).toBe(1);
       expect(expanded[0]).toBe(getFirstMessage(getState()).id);
     });
 
     it("does not open console.groupCollapsed messages when they are added", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const message = stubPackets.get("console.groupCollapsed('foo')");
       dispatch(actions.messagesAdd([message]));
 
       const expanded = getAllMessagesUiById(getState());
       expect(expanded.length).toBe(0);
     });
 
@@ -581,33 +584,35 @@ describe("Message reducer:", () => {
 
       expanded = getAllMessagesUiById(getState());
       expect(expanded.length).toBe(0);
     });
   });
 
   describe("currentGroup", () => {
     it("sets the currentGroup when console.group message is added", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const packet = stubPackets.get("console.group('bar')");
       dispatch(actions.messagesAdd([packet]));
 
       const currentGroup = getCurrentGroup(getState());
       expect(currentGroup).toBe(getFirstMessage(getState()).id);
     });
 
     it("sets currentGroup to expected value when console.groupEnd is added", () => {
       const { dispatch, getState } = setupStore([
         "console.group('bar')",
-        "console.groupCollapsed('foo')"
+        "console.groupCollapsed('foo')",
+        "console.group('bar')",
+        "console.groupEnd('bar')",
       ]);
 
       let currentGroup = getCurrentGroup(getState());
-      expect(currentGroup).toBe(getLastMessage(getState()).id);
+      expect(currentGroup).toBe(getMessageAt(getState(), 1).id);
 
       const endFooPacket = stubPackets.get("console.groupEnd('foo')");
       dispatch(actions.messagesAdd([endFooPacket]));
       currentGroup = getCurrentGroup(getState());
       expect(currentGroup).toBe(getFirstMessage(getState()).id);
 
       const endBarPacket = stubPackets.get("console.groupEnd('bar')");
       dispatch(actions.messagesAdd([endBarPacket]));
@@ -624,17 +629,17 @@ describe("Message reducer:", () => {
 
       const currentGroup = getCurrentGroup(getState());
       expect(currentGroup).toBe(null);
     });
   });
 
   describe("groupsById", () => {
     it("adds the group with expected array when console.group message is added", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       const barPacket = stubPackets.get("console.group('bar')");
       dispatch(actions.messagesAdd([barPacket]));
 
       let groupsById = getGroupsById(getState());
       expect(groupsById.size).toBe(1);
       expect(groupsById.has(getFirstMessage(getState()).id)).toBe(true);
       expect(groupsById.get(getFirstMessage(getState()).id)).toEqual([]);
@@ -670,46 +675,67 @@ describe("Message reducer:", () => {
           "console.group('bar')",
           "console.group()",
           "console.groupEnd()",
           "console.groupEnd('bar')",
           "console.group('bar')",
           "console.groupEnd('bar')",
           "console.log('foobar', 'test')",
         ],
-        null, {
-          logLimit: 3
+        {
+          actions,
+          storeOptions: {
+            logLimit: 3
+          }
         }
       );
 
+      /*
+       * Here is the initial state of the console:
+       * ▶︎ bar
+       *   ▶︎ noLabel
+       * ▶︎ bar
+       * foobar test
+      */
+
       // Check that we have the expected data.
       let groupsById = getGroupsById(getState());
       expect(groupsById.size).toBe(3);
 
       // This addition will prune the first group (and its child group) out of the store.
+      /*
+       * ▶︎ bar
+       * foobar test
+       * undefined
+      */
       let packet = stubPackets.get("console.log(undefined)");
       dispatch(actions.messagesAdd([packet]));
 
       groupsById = getGroupsById(getState());
 
-      // There should be only the id of the last console.trace message.
+      // There should be only the id of the last console.group message.
       expect(groupsById.size).toBe(1);
 
       // This additions will prune the last group message out of the store.
+      /*
+       * foobar test
+       * undefined
+       * foobar test
+      */
       packet = stubPackets.get("console.log('foobar', 'test')");
       dispatch(actions.messagesAdd([packet]));
 
       // groupsById should now be empty.
       expect(getGroupsById(getState()).size).toBe(0);
     });
   });
 
   describe("networkMessagesUpdateById", () => {
     it("adds the network update message when network update action is called", () => {
-      const { dispatch, getState } = setupStore([]);
+      const { dispatch, getState } = setupStore();
 
       let packet = clonePacket(stubPackets.get("GET request"));
       let updatePacket = clonePacket(stubPackets.get("GET request update"));
 
       packet.actor = "message1";
       updatePacket.networkInfo.actor = "message1";
       dispatch(actions.messagesAdd([packet]));
       dispatch(
@@ -744,18 +770,20 @@ describe("Message reducer:", () => {
 
       dispatch(actions.messagesClear());
 
       networkUpdates = getAllNetworkMessagesUpdateById(getState());
       expect(Object.keys(networkUpdates).length).toBe(0);
     });
 
     it("cleans the networkMessagesUpdateById property when messages are pruned", () => {
-      const { dispatch, getState } = setupStore([], null, {
-        logLimit: 3
+      const { dispatch, getState } = setupStore([], {
+        storeOptions: {
+          logLimit: 3
+        }
       });
 
       // Add 3 network messages and their updates
       let packet = clonePacket(stubPackets.get("XHR GET request"));
       let updatePacket = clonePacket(stubPackets.get("XHR GET request update"));
       packet.actor = "message1";
       updatePacket.networkInfo.actor = "message1";
       dispatch(actions.messagesAdd([packet]));
@@ -831,18 +859,20 @@ describe("Message reducer:", () => {
       expect(table.get(getFirstMessage(getState()).id)).toBe(data);
 
       dispatch(actions.messagesClear());
 
       expect(getAllMessagesTableDataById(getState()).size).toBe(0);
     });
 
     it("cleans the messagesTableDataById property when messages are pruned", () => {
-      const { dispatch, getState } = setupStore([], null, {
-        logLimit: 2
+      const { dispatch, getState } = setupStore([], {
+        storeOptions: {
+          logLimit: 2
+        }
       });
 
       // Add 2 table message and their data.
       dispatch(actions.messagesAdd([stubPackets.get("console.table(['a', 'b', 'c'])")]));
       dispatch(actions.messagesAdd(
         [stubPackets.get("console.table(['red', 'green', 'blue']);")]));
 
       let messages = getAllMessagesById(getState());
@@ -870,18 +900,20 @@ describe("Message reducer:", () => {
     });
   });
 
   describe("messagesAdd", () => {
     it("still log repeated message over logLimit, but only repeated ones", () => {
       // Log two distinct messages
       const key1 = "console.log('foobar', 'test')";
       const key2 = "console.log(undefined)";
-      const { dispatch, getState } = setupStore([key1, key2], null, {
-        logLimit: 2
+      const { dispatch, getState } = setupStore([key1, key2], {
+        storeOptions: {
+          logLimit: 2
+        }
       });
 
       // Then repeat the last one two times and log the first one again
       const packet1 = clonePacket(stubPackets.get(key2));
       const packet2 = clonePacket(stubPackets.get(key2));
       const packet3 = clonePacket(stubPackets.get(key1));
 
       // Repeat ID must be the same even if the timestamp is different.
diff --git a/devtools/client/webconsole/new-console-output/test/store/network-messages.test.js b/devtools/client/webconsole/new-console-output/test/store/network-messages.test.js
--- a/devtools/client/webconsole/new-console-output/test/store/network-messages.test.js
+++ b/devtools/client/webconsole/new-console-output/test/store/network-messages.test.js
@@ -19,17 +19,17 @@ describe("Network message reducer:", () 
   let getState;
   let dispatch;
 
   before(() => {
     actions = setupActions();
   });
 
   beforeEach(() => {
-    const store = setupStore([]);
+    const store = setupStore();
 
     getState = store.getState;
     dispatch = store.dispatch;
 
     let packet = clonePacket(stubPackets.get("GET request"));
     let updatePacket = clonePacket(stubPackets.get("GET request update"));
 
     packet.actor = "message1";
diff --git a/devtools/client/webconsole/new-console-output/test/store/release-actors.test.js b/devtools/client/webconsole/new-console-output/test/store/release-actors.test.js
--- a/devtools/client/webconsole/new-console-output/test/store/release-actors.test.js
+++ b/devtools/client/webconsole/new-console-output/test/store/release-actors.test.js
@@ -19,22 +19,25 @@ describe("Release actor enhancer:", () =
     actions = setupActions();
   });
 
   describe("Client proxy", () => {
     it("releases backend actors when limit reached adding a single message", () => {
       const logLimit = 100;
       let releasedActors = [];
       const { dispatch, getState } = setupStore([], {
-        proxy: {
-          releaseActor: (actor) => {
-            releasedActors.push(actor);
+        storeOptions: {logLimit},
+        hud: {
+          proxy: {
+            releaseActor: (actor) => {
+              releasedActors.push(actor);
+            }
           }
         }
-      }, { logLimit });
+      });
 
       // Add a log message.
       dispatch(actions.messagesAdd([
         stubPackets.get("console.log('myarray', ['red', 'green', 'blue'])")]));
 
       const firstMessage = getFirstMessage(getState());
       const firstMessageActor = firstMessage.parameters[1].actor;
 
@@ -58,22 +61,25 @@ describe("Release actor enhancer:", () =
       expect(releasedActors).toInclude(secondMessageActor);
       expect(releasedActors).toInclude(thirdMessageActor);
     });
 
     it("releases backend actors when limit reached adding multiple messages", () => {
       const logLimit = 100;
       let releasedActors = [];
       const { dispatch, getState } = setupStore([], {
-        proxy: {
-          releaseActor: (actor) => {
-            releasedActors.push(actor);
+        storeOptions: {logLimit},
+        hud: {
+          proxy: {
+            releaseActor: (actor) => {
+              releasedActors.push(actor);
+            }
           }
         }
-      }, { logLimit });
+      });
 
       // Add a log message.
       dispatch(actions.messagesAdd([
         stubPackets.get("console.log('myarray', ['red', 'green', 'blue'])")]));
 
       const firstMessage = getFirstMessage(getState());
       const firstMessageActor = firstMessage.parameters[1].actor;
 
@@ -104,19 +110,21 @@ describe("Release actor enhancer:", () =
       expect(releasedActors).toInclude(firstMessageActor);
       expect(releasedActors).toInclude(secondMessageActor);
       expect(releasedActors).toInclude(thirdMessageActor);
     });
 
     it("properly releases backend actors after clear", () => {
       let releasedActors = [];
       const { dispatch, getState } = setupStore([], {
-        proxy: {
-          releaseActor: (actor) => {
-            releasedActors.push(actor);
+        hud: {
+          proxy: {
+            releaseActor: (actor) => {
+              releasedActors.push(actor);
+            }
           }
         }
       });
 
       // Add a log message.
       dispatch(actions.messagesAdd([
         stubPackets.get("console.log('myarray', ['red', 'green', 'blue'])")]));
 
