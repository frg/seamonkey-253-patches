# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1515743773 -3600
# Node ID cdae594745cdb4a77cae7bae4e662af3212f2a79
# Parent  4224d25a6518abc768e47360ed3a83b5fafe8919
Bug 1405340 - Enable browser_webconsole_hpkp_onvalid-headers.js in new console frontend;r=Honza.

MozReview-Commit-ID: 4bcZR2HrFOd

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -275,18 +275,16 @@ skip-if = (e10s && debug) || (e10s && os
 [browser_webconsole_file_uri.js]
 skip-if = true #       Bug 1404382
 [browser_webconsole_filter_scroll.js]
 [browser_webconsole_filters.js]
 [browser_webconsole_filters_persist.js]
 [browser_webconsole_highlighter_console_helper.js]
 [browser_webconsole_history_arrow_keys.js]
 [browser_webconsole_hpkp_invalid-headers.js]
-skip-if = true #       Bug 1405340
-# old console skip-if = (os == 'win' && bits == 64) # Bug 1390001
 [browser_webconsole_hsts_invalid-headers.js]
 skip-if = true #       Bug 1405341
 # old console skip-if = e10s # Bug 1042253 - webconsole e10s tests
 [browser_webconsole_iframe_wrong_hud.js]
 skip-if = true #       Bug 1404378
 [browser_webconsole_ineffective_iframe_sandbox_warning.js]
 skip-if = true #       Bug 1404883
 # old console skip-if = (os == 'win' && bits == 64) # Bug 1390001
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_hpkp_invalid-headers.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_hpkp_invalid-headers.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_hpkp_invalid-headers.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_hpkp_invalid-headers.js
@@ -1,126 +1,107 @@
-/* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
-/* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
-// Tests that errors about invalid HPKP security headers are logged to the web
-// console.
+// Tests that errors about invalid HPKP security headers are logged to the web console.
 
 "use strict";
 
-const TEST_URI = "data:text/html;charset=utf-8,Web Console HPKP invalid " +
-                 "header test";
+const TEST_URI = "data:text/html;charset=utf-8,Web Console HPKP invalid header test";
 const SJS_URL = "https://example.com/browser/devtools/client/webconsole/" +
-                "test/test_hpkp-invalid-headers.sjs";
-const LEARN_MORE_URI = "https://developer.mozilla.org/docs/Web/HTTP/" +
-                       "Public_Key_Pinning" + DOCS_GA_PARAMS;
-const NON_BUILTIN_ROOT_PREF = "security.cert_pinning.process_headers_from_" +
-                              "non_builtin_roots";
+                "new-console-output/test/mochitest/test_hpkp-invalid-headers.sjs";
+const LEARN_MORE_URI =
+  "https://developer.mozilla.org/docs/Web/HTTP/Public_Key_Pinning" + DOCS_GA_PARAMS;
+const NON_BUILTIN_ROOT_PREF =
+  "security.cert_pinning.process_headers_from_non_builtin_roots";
 
-add_task(function* () {
+add_task(async function () {
   registerCleanupFunction(() => {
     Services.prefs.clearUserPref(NON_BUILTIN_ROOT_PREF);
   });
 
-  yield loadTab(TEST_URI);
+  let hud = await openNewTabAndConsole(TEST_URI);
 
-  let hud = yield openConsole();
-
-  yield* checkForMessage({
+  await navigateAndCheckForWarningMessage({
     url: SJS_URL + "?badSyntax",
     name: "Could not parse header error displayed successfully",
     text: "Public-Key-Pins: The site specified a header that could not be " +
           "parsed successfully."
   }, hud);
 
-  yield* checkForMessage({
+  await navigateAndCheckForWarningMessage({
     url: SJS_URL + "?noMaxAge",
     name: "No max-age error displayed successfully",
     text: "Public-Key-Pins: The site specified a header that did not include " +
           "a \u2018max-age\u2019 directive."
   }, hud);
 
-  yield* checkForMessage({
+  await navigateAndCheckForWarningMessage({
     url: SJS_URL + "?invalidIncludeSubDomains",
     name: "Invalid includeSubDomains error displayed successfully",
     text: "Public-Key-Pins: The site specified a header that included an " +
           "invalid \u2018includeSubDomains\u2019 directive."
   }, hud);
 
-  yield* checkForMessage({
+  await navigateAndCheckForWarningMessage({
     url: SJS_URL + "?invalidMaxAge",
     name: "Invalid max-age error displayed successfully",
     text: "Public-Key-Pins: The site specified a header that included an " +
           "invalid \u2018max-age\u2019 directive."
   }, hud);
 
-  yield* checkForMessage({
+  await navigateAndCheckForWarningMessage({
     url: SJS_URL + "?multipleIncludeSubDomains",
     name: "Multiple includeSubDomains error displayed successfully",
     text: "Public-Key-Pins: The site specified a header that included " +
           "multiple \u2018includeSubDomains\u2019 directives."
   }, hud);
 
-  yield* checkForMessage({
+  await navigateAndCheckForWarningMessage({
     url: SJS_URL + "?multipleMaxAge",
     name: "Multiple max-age error displayed successfully",
     text: "Public-Key-Pins: The site specified a header that included " +
           "multiple \u2018max-age\u2019 directives."
   }, hud);
 
-  yield* checkForMessage({
+  await navigateAndCheckForWarningMessage({
     url: SJS_URL + "?multipleReportURIs",
     name: "Multiple report-uri error displayed successfully",
     text: "Public-Key-Pins: The site specified a header that included " +
           "multiple \u2018report-uri\u2019 directives."
   }, hud);
 
   // The root used for mochitests is not built-in, so set the relevant pref to
   // true to have the PKP implementation return more specific errors.
   Services.prefs.setBoolPref(NON_BUILTIN_ROOT_PREF, true);
 
-  yield* checkForMessage({
+  await navigateAndCheckForWarningMessage({
     url: SJS_URL + "?pinsetDoesNotMatch",
     name: "Non-matching pinset error displayed successfully",
     text: "Public-Key-Pins: The site specified a header that did not include " +
           "a matching pin."
   }, hud);
 
   Services.prefs.setBoolPref(NON_BUILTIN_ROOT_PREF, false);
 
-  yield* checkForMessage({
+  await navigateAndCheckForWarningMessage({
     url: SJS_URL + "?pinsetDoesNotMatch",
     name: "Non-built-in root error displayed successfully",
     text: "Public-Key-Pins: The certificate used by the site was not issued " +
           "by a certificate in the default root certificate store. To " +
           "prevent accidental breakage, the specified header was ignored."
   }, hud);
 });
 
-function* checkForMessage(curTest, hud) {
-  hud.jsterm.clearOutput();
-
-  BrowserTestUtils.loadURI(gBrowser.selectedBrowser, curTest.url);
+async function navigateAndCheckForWarningMessage({name, text, url}, hud) {
+  hud.jsterm.clearOutput(true);
 
-  let results = yield waitForMessages({
-    webconsole: hud,
-    messages: [
-      {
-        name: curTest.name,
-        text: curTest.text,
-        category: CATEGORY_SECURITY,
-        severity: SEVERITY_WARNING,
-        objects: true,
-      },
-    ],
-  });
+  const onMessage = waitForMessage(hud, text, ".message.warning");
+  BrowserTestUtils.loadURI(gBrowser.selectedBrowser, url);
+  const {node} = await onMessage;
+  ok(node, name);
 
-  yield testClickOpenNewTab(hud, results);
+  let learnMoreNode = node.querySelector(".learn-more-link");
+  ok(learnMoreNode, `There is a "Learn more" link`);
+  const navigatedLink = await simulateLinkClick(learnMoreNode);
+  is(navigatedLink, LEARN_MORE_URI,
+    "Click on the learn more link navigates the user to the expected url");
 }
-
-function testClickOpenNewTab(hud, results) {
-  let warningNode = results[0].clickableElements[0];
-  ok(warningNode, "link element");
-  ok(warningNode.classList.contains("learn-more-link"), "link class name");
-  return simulateMessageLinkClick(warningNode, LEARN_MORE_URI);
-}
