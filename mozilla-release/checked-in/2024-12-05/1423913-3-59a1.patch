# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1512751927 18000
# Node ID c71de7f98d08b1772f7dc54d56828c34f86ef16e
# Parent  89619781ee4b54e7694e003dd9fae8c959313df2
Bug 1423913 P3 Don't register more shutdown handle if we create more than one ClientManagerService instance. r=baku

diff --git a/dom/clients/manager/ClientManagerService.cpp b/dom/clients/manager/ClientManagerService.cpp
--- a/dom/clients/manager/ClientManagerService.cpp
+++ b/dom/clients/manager/ClientManagerService.cpp
@@ -19,16 +19,17 @@ namespace dom {
 
 using mozilla::ipc::AssertIsOnBackgroundThread;
 using mozilla::ipc::ContentPrincipalInfo;
 using mozilla::ipc::PrincipalInfo;
 
 namespace {
 
 ClientManagerService* sClientManagerServiceInstance = nullptr;
+bool sClientManagerServiceShutdownRegistered = false;
 
 bool
 MatchPrincipalInfo(const PrincipalInfo& aLeft, const PrincipalInfo& aRight)
 {
   if (aLeft.type() != aRight.type()) {
     return false;
   }
 
@@ -141,45 +142,56 @@ OnShutdown()
 
 } // anonymous namespace
 
 ClientManagerService::ClientManagerService()
   : mShutdown(false)
 {
   AssertIsOnBackgroundThread();
 
-  // While the ClientManagerService will be gracefully terminated as windows
-  // and workers are naturally killed, this can cause us to do extra work
-  // relatively late in the shutdown process.  To avoid this we eagerly begin
-  // shutdown at the first sign it has begun.  Since we handle normal shutdown
-  // gracefully we don't really need to block anything here.  We just begin
-  // destroying our IPC actors immediately.
-  OnShutdown()->Then(GetCurrentThreadSerialEventTarget(), __func__,
-    [] () {
-      RefPtr<ClientManagerService> svc = ClientManagerService::GetInstance();
-      if (svc) {
-        svc->Shutdown();
-      }
-    });
+  // Only register one shutdown handler at a time.  If a previous service
+  // instance did this, but shutdown has not come, then we can avoid
+  // doing it again.
+  if (!sClientManagerServiceShutdownRegistered) {
+    sClientManagerServiceShutdownRegistered = true;
+
+    // While the ClientManagerService will be gracefully terminated as windows
+    // and workers are naturally killed, this can cause us to do extra work
+    // relatively late in the shutdown process.  To avoid this we eagerly begin
+    // shutdown at the first sign it has begun.  Since we handle normal shutdown
+    // gracefully we don't really need to block anything here.  We just begin
+    // destroying our IPC actors immediately.
+    OnShutdown()->Then(GetCurrentThreadSerialEventTarget(), __func__,
+      [] () {
+        // Look up the latest service instance, if it exists.  This may
+        // be different from the instance that registered the shutdown
+        // handler.
+        RefPtr<ClientManagerService> svc = ClientManagerService::GetInstance();
+        if (svc) {
+          svc->Shutdown();
+        }
+      });
+  }
 }
 
 ClientManagerService::~ClientManagerService()
 {
   AssertIsOnBackgroundThread();
   MOZ_DIAGNOSTIC_ASSERT(mSourceTable.Count() == 0);
   MOZ_DIAGNOSTIC_ASSERT(mManagerList.IsEmpty());
 
   MOZ_DIAGNOSTIC_ASSERT(sClientManagerServiceInstance == this);
   sClientManagerServiceInstance = nullptr;
 }
 
 void
 ClientManagerService::Shutdown()
 {
   AssertIsOnBackgroundThread();
+  MOZ_DIAGNOSTIC_ASSERT(sClientManagerServiceShutdownRegistered);
 
   // If many ClientManagerService are created and destroyed quickly we can
   // in theory get more than one shutdown listener calling us.
   if (mShutdown) {
     return;
   }
   mShutdown = true;
 
