# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1515089984 -3600
# Node ID 9f44d89eda685123154dfb9a7ce5736a57b72429
# Parent  f6d3617953505b1b2780294bc8fa00eeff5fda32
Bug 1425574 - Fill the feature gap between Console.jsm and Console API - part 7 - Console active, r=smaug

diff --git a/dom/console/Console.cpp b/dom/console/Console.cpp
--- a/dom/console/Console.cpp
+++ b/dom/console/Console.cpp
@@ -817,16 +817,17 @@ Console::Create(nsPIDOMWindowInner* aWin
   return console.forget();
 }
 
 Console::Console(nsPIDOMWindowInner* aWindow)
   : mWindow(aWindow)
   , mOuterID(0)
   , mInnerID(0)
   , mDumpToStdout(false)
+  , mChromeInstance(false)
   , mStatus(eUnknown)
   , mCreationTimeStamp(TimeStamp::Now())
 {
   if (mWindow) {
     MOZ_ASSERT(mWindow->IsInnerWindow());
     mInnerID = mWindow->WindowID();
 
     // Without outerwindow any console message coming from this object will not
@@ -1066,22 +1067,33 @@ Console::ProfileMethod(const GlobalObjec
   if (!console) {
     return;
   }
 
   JSContext* cx = aGlobal.Context();
   console->ProfileMethodInternal(cx, aAction, aData);
 }
 
+bool
+Console::IsEnabled(JSContext* aCx) const
+{
+  // Console is always enabled if it is a custom Chrome-Only instance.
+  if (mChromeInstance) {
+    return true;
+  }
+
+  // Make all Console API no-op if DevTools aren't enabled.
+  return nsContentUtils::DevToolsEnabled(aCx);
+}
+
 void
 Console::ProfileMethodInternal(JSContext* aCx, const nsAString& aAction,
                                const Sequence<JS::Value>& aData)
 {
-  // Make all Console API no-op if DevTools aren't enabled.
-  if (!nsContentUtils::DevToolsEnabled(aCx)) {
+  if (!IsEnabled(aCx)) {
     return;
   }
 
   MaybeExecuteDumpFunction(aCx, aAction, aData);
 
   if (!NS_IsMainThread()) {
     // Here we are in a worker thread.
     RefPtr<ConsoleProfileRunnable> runnable =
@@ -1207,18 +1219,17 @@ Console::Method(const GlobalObject& aGlo
                           aData);
 }
 
 void
 Console::MethodInternal(JSContext* aCx, MethodName aMethodName,
                         const nsAString& aMethodString,
                         const Sequence<JS::Value>& aData)
 {
-  // Make all Console API no-op if DevTools aren't enabled.
-  if (!nsContentUtils::DevToolsEnabled(aCx)) {
+  if (!IsEnabled(aCx)) {
     return;
   }
 
   AssertIsOnOwningThread();
 
   RefPtr<ConsoleCallData> callData(new ConsoleCallData());
 
   ClearException ce(aCx);
diff --git a/dom/console/Console.h b/dom/console/Console.h
--- a/dom/console/Console.h
+++ b/dom/console/Console.h
@@ -393,16 +393,19 @@ private:
                            const Sequence<JS::Value>& aData);
 
   void
   MaybeExecuteDumpFunctionForTrace(JSContext* aCx, nsIStackFrame* aStack);
 
   void
   ExecuteDumpFunction(const nsAString& aMessage);
 
+  bool
+  IsEnabled(JSContext* aCx) const;
+
   // All these nsCOMPtr are touched on main thread only.
   nsCOMPtr<nsPIDOMWindowInner> mWindow;
   nsCOMPtr<nsIConsoleAPIStorage> mStorage;
   RefPtr<JSObjectHolder> mSandbox;
 
   // Touched on the owner thread.
   nsDataHashtable<nsStringHashKey, DOMHighResTimeStamp> mTimerRegistry;
   nsDataHashtable<nsStringHashKey, uint32_t> mCounterRegistry;
@@ -429,16 +432,17 @@ private:
   uint64_t mInnerID;
 
   // Set only by ConsoleInstance:
   nsString mConsoleID;
   nsString mPassedInnerID;
   RefPtr<ConsoleInstanceDumpCallback> mDumpFunction;
   bool mDumpToStdout;
   nsString mDumpPrefix;
+  bool mChromeInstance;
 
   enum {
     eUnknown,
     eInitialized,
     eShuttingDown
   } mStatus;
 
   // This is used when Console is created and it's used only for JSM custom
diff --git a/dom/console/ConsoleInstance.cpp b/dom/console/ConsoleInstance.cpp
--- a/dom/console/ConsoleInstance.cpp
+++ b/dom/console/ConsoleInstance.cpp
@@ -28,16 +28,19 @@ ConsoleInstance::ConsoleInstance(const C
   if (aOptions.mDump.WasPassed()) {
     mConsole->mDumpFunction = &aOptions.mDump.Value();
   } else {
     // For historical reasons, ConsoleInstance prints messages on stdout.
     mConsole->mDumpToStdout = true;
   }
 
   mConsole->mDumpPrefix = aOptions.mPrefix;
+
+  // Let's inform that this is a custom instance.
+  mConsole->mChromeInstance = true;
 }
 
 ConsoleInstance::~ConsoleInstance()
 {}
 
 JSObject*
 ConsoleInstance::WrapObject(JSContext* aCx, JS::Handle<JSObject*> aGivenProto)
 {
