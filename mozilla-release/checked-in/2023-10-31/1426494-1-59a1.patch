# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1513748664 -3600
# Node ID f28babf65d7277182a3ab72825e1a16641c0fee5
# Parent  bfb8a3f26f4e3856a025370998ff9242c75ab611
Bug 1426494: Devirtualize StyleScope::AsNode. r=smaug

MozReview-Commit-ID: 2nDEI5aIu46

diff --git a/dom/base/ShadowRoot.cpp b/dom/base/ShadowRoot.cpp
--- a/dom/base/ShadowRoot.cpp
+++ b/dom/base/ShadowRoot.cpp
@@ -51,16 +51,17 @@ NS_INTERFACE_MAP_END_INHERITING(Document
 
 NS_IMPL_ADDREF_INHERITED(ShadowRoot, DocumentFragment)
 NS_IMPL_RELEASE_INHERITED(ShadowRoot, DocumentFragment)
 
 ShadowRoot::ShadowRoot(Element* aElement, bool aClosed,
                        already_AddRefed<mozilla::dom::NodeInfo>&& aNodeInfo,
                        nsXBLPrototypeBinding* aProtoBinding)
   : DocumentFragment(aNodeInfo)
+  , StyleScope(*this)
   , mProtoBinding(aProtoBinding)
   , mInsertionPointChanged(false)
   , mIsComposedDocParticipant(false)
 {
   SetHost(aElement);
   mMode = aClosed ? ShadowRootMode::Closed : ShadowRootMode::Open;
 
   // Nodes in a shadow tree should never store a value
diff --git a/dom/base/ShadowRoot.h b/dom/base/ShadowRoot.h
--- a/dom/base/ShadowRoot.h
+++ b/dom/base/ShadowRoot.h
@@ -51,22 +51,16 @@ public:
   {
     return mMode;
   }
   bool IsClosed() const
   {
     return mMode == ShadowRootMode::Closed;
   }
 
-  // StyleScope.
-  nsINode& AsNode() final
-  {
-    return *this;
-  }
-
   // [deprecated] Shadow DOM v0
   void AddToIdTable(Element* aElement, nsIAtom* aId);
   void RemoveFromIdTable(Element* aElement, nsIAtom* aId);
   void InsertSheet(StyleSheet* aSheet, nsIContent* aLinkingContent);
   void RemoveSheet(StyleSheet* aSheet);
   bool ApplyAuthorStyles();
   void SetApplyAuthorStyles(bool aApplyAuthorStyles);
   StyleSheetList* StyleSheets()
diff --git a/dom/base/StyleScope.cpp b/dom/base/StyleScope.cpp
--- a/dom/base/StyleScope.cpp
+++ b/dom/base/StyleScope.cpp
@@ -5,16 +5,26 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "StyleScope.h"
 #include "mozilla/dom/StyleSheetList.h"
 
 namespace mozilla {
 namespace dom {
 
+StyleScope::StyleScope(mozilla::dom::ShadowRoot& aShadowRoot)
+  : mAsNode(aShadowRoot)
+  , mKind(Kind::ShadowRoot)
+{}
+
+StyleScope::StyleScope(nsIDocument& aDoc)
+  : mAsNode(aDoc)
+  , mKind(Kind::Document)
+{}
+
 StyleScope::~StyleScope()
 {
 }
 
 StyleSheetList&
 StyleScope::EnsureDOMStyleSheets()
 {
   if (!mDOMStyleSheets) {
diff --git a/dom/base/StyleScope.h b/dom/base/StyleScope.h
--- a/dom/base/StyleScope.h
+++ b/dom/base/StyleScope.h
@@ -22,22 +22,33 @@ class StyleSheetList;
  * A class meant to be shared by ShadowRoot and Document, that holds a list of
  * stylesheets.
  *
  * TODO(emilio, bug 1418159): In the future this should hold most of the
  * relevant style state, this should allow us to fix bug 548397.
  */
 class StyleScope
 {
+  enum class Kind {
+    Document,
+    ShadowRoot,
+  };
+
 public:
-  virtual nsINode& AsNode() = 0;
+  explicit StyleScope(nsIDocument&);
+  explicit StyleScope(mozilla::dom::ShadowRoot&);
+
+  nsINode& AsNode()
+  {
+    return mAsNode;
+  }
 
   const nsINode& AsNode() const
   {
-    return const_cast<StyleScope&>(*this).AsNode();
+    return mAsNode;
   }
 
   StyleSheet* SheetAt(size_t aIndex) const
   {
     return mStyleSheets.SafeElementAt(aIndex);
   }
 
   size_t SheetCount() const
@@ -67,15 +78,18 @@ public:
 
   StyleSheetList& EnsureDOMStyleSheets();
 
   ~StyleScope();
 
 protected:
   nsTArray<RefPtr<mozilla::StyleSheet>> mStyleSheets;
   RefPtr<mozilla::dom::StyleSheetList> mDOMStyleSheets;
+
+  nsINode& mAsNode;
+  const Kind mKind;
 };
 
 }
 
 }
 
 #endif
diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -1362,16 +1362,17 @@ struct nsIDocument::FrameRequest
 
 static already_AddRefed<mozilla::dom::NodeInfo> nullNodeInfo;
 
 // ==================================================================
 // =
 // ==================================================================
 nsIDocument::nsIDocument()
   : nsINode(nullNodeInfo),
+    StyleScope(*this),
     mReferrerPolicySet(false),
     mReferrerPolicy(mozilla::net::RP_Unset),
     mBlockAllMixedContent(false),
     mBlockAllMixedContentPreloads(false),
     mUpgradeInsecureRequests(false),
     mUpgradeInsecurePreloads(false),
     mCharacterSet(WINDOWS_1252_ENCODING),
     mCharacterSetSource(0),
diff --git a/dom/base/nsIDocument.h b/dom/base/nsIDocument.h
--- a/dom/base/nsIDocument.h
+++ b/dom/base/nsIDocument.h
@@ -1293,21 +1293,16 @@ public:
    * supplied by add-ons or by the app (Firefox OS or Firefox Mobile, for
    * example), since their sheets should override built-in sheets.
    *
    * TODO We can get rid of the whole concept of delayed loading if we fix
    * bug 77999.
    */
   virtual void EnsureOnDemandBuiltInUASheet(mozilla::StyleSheet* aSheet) = 0;
 
-  nsINode& AsNode() final
-  {
-    return *this;
-  }
-
   mozilla::dom::StyleSheetList* StyleSheets()
   {
     return &StyleScope::EnsureDOMStyleSheets();
   }
 
   /**
    * Insert a sheet at a particular spot in the stylesheet list (zero-based)
    * @param aSheet the sheet to insert
