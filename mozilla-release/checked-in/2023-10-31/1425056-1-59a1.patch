# HG changeset patch
# User Ryan Hunt <rhunt@eqrion.net>
# Date 1513787783 21600
# Node ID d50f97d2091afadebb552ab40670ea05349bfff4
# Parent  70ac3be15a7d634381558410669f9a7f0cdd2556
Lock access to PowCache in FilterNodeLighting (bug 1425056, r=mstange)

PowCache is not thread safe to the same filter node being drawn by multiple threads
at the same time. In this case I think it's easiest to just add a mutex for this
filter node.

MozReview-Commit-ID: 7pHuFYGxV4B

diff --git a/gfx/2d/FilterNodeSoftware.cpp b/gfx/2d/FilterNodeSoftware.cpp
--- a/gfx/2d/FilterNodeSoftware.cpp
+++ b/gfx/2d/FilterNodeSoftware.cpp
@@ -3278,17 +3278,18 @@ DistantLightSoftware::SetAttribute(uint3
 static inline Point3D Normalized(const Point3D &vec) {
   Point3D copy(vec);
   copy.Normalize();
   return copy;
 }
 
 template<typename LightType, typename LightingType>
 FilterNodeLightingSoftware<LightType, LightingType>::FilterNodeLightingSoftware(const char* aTypeName)
- : mSurfaceScale(0)
+ : mLock("FilterNodeLightingSoftware")
+ , mSurfaceScale(0)
 #if defined(MOZILLA_INTERNAL_API) && (defined(DEBUG) || defined(FORCE_BUILD_REFCNT_LOGGING))
  , mTypeName(aTypeName)
 #endif
 {}
 
 template<typename LightType, typename LightingType>
 int32_t
 FilterNodeLightingSoftware<LightType, LightingType>::InputIndex(uint32_t aInputEnumIndex)
@@ -3537,16 +3538,18 @@ FilterNodeLightingSoftware<LightType, Li
     return nullptr;
   }
 
   uint8_t* sourceData = DataAtOffset(input, sourceMap.GetMappedSurface(), offset);
   int32_t sourceStride = sourceMap.GetStride();
   uint8_t* targetData = targetMap.GetData();
   int32_t targetStride = targetMap.GetStride();
 
+  MutexAutoLock lock(mLock);
+
   uint32_t lightColor = ColorToBGRA(mColor);
   mLight.Prepare();
   mLighting.Prepare();
 
   for (int32_t y = 0; y < size.height; y++) {
     for (int32_t x = 0; x < size.width; x++) {
       int32_t sourceIndex = y * sourceStride + x;
       int32_t targetIndex = y * targetStride + 4 * x;
diff --git a/gfx/2d/FilterNodeSoftware.h b/gfx/2d/FilterNodeSoftware.h
--- a/gfx/2d/FilterNodeSoftware.h
+++ b/gfx/2d/FilterNodeSoftware.h
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef _MOZILLA_GFX_FILTERNODESOFTWARE_H_
 #define _MOZILLA_GFX_FILTERNODESOFTWARE_H_
 
 #include "Filters.h"
+#include "mozilla/Mutex.h"
 #include <vector>
 
 namespace mozilla {
 namespace gfx {
 
 class DataSourceSurface;
 class DrawTarget;
 struct DrawOptions;
@@ -704,16 +705,17 @@ protected:
   virtual void RequestFromInputsForRect(const IntRect &aRect) override;
 
 private:
   template<typename CoordType>
   already_AddRefed<DataSourceSurface> DoRender(const IntRect& aRect,
                                            CoordType aKernelUnitLengthX,
                                            CoordType aKernelUnitLengthY);
 
+  Mutex mLock;
   LightType mLight;
   LightingType mLighting;
   Float mSurfaceScale;
   Size mKernelUnitLength;
   Color mColor;
 #if defined(MOZILLA_INTERNAL_API) && (defined(DEBUG) || defined(FORCE_BUILD_REFCNT_LOGGING))
   const char* mTypeName;
 #endif
