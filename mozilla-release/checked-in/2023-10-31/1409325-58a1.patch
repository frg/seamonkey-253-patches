# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1508240461 -7200
#      Tue Oct 17 13:41:01 2017 +0200
# Node ID adc148ea3d36cf51b83c1aa943b86e997f2ce9d5
# Parent  f28b89abe86f906d1bef38df88eb3b034c905b96
Bug 1409325 - Update FileReader WebIDL File, r=smaug

diff --git a/dom/file/Blob.cpp b/dom/file/Blob.cpp
--- a/dom/file/Blob.cpp
+++ b/dom/file/Blob.cpp
@@ -184,21 +184,26 @@ void
 Blob::GetType(nsAString &aType)
 {
   mImpl->GetType(aType);
 }
 
 already_AddRefed<Blob>
 Blob::Slice(const Optional<int64_t>& aStart,
             const Optional<int64_t>& aEnd,
-            const nsAString& aContentType,
+            const Optional<nsAString>& aContentType,
             ErrorResult& aRv)
 {
+  nsAutoString contentType;
+  if (aContentType.WasPassed()) {
+    contentType = aContentType.Value();
+  }
+
   RefPtr<BlobImpl> impl =
-    mImpl->Slice(aStart, aEnd, aContentType, aRv);
+    mImpl->Slice(aStart, aEnd, contentType, aRv);
   if (aRv.Failed()) {
     return nullptr;
   }
 
   RefPtr<Blob> blob = Blob::Create(mParent, impl);
   return blob.forget();
 }
 
diff --git a/dom/file/Blob.h b/dom/file/Blob.h
--- a/dom/file/Blob.h
+++ b/dom/file/Blob.h
@@ -117,17 +117,17 @@ public:
                                JS::Handle<JSObject*> aGivenProto) override;
 
   uint64_t GetSize(ErrorResult& aRv);
 
   void GetType(nsAString& aType);
 
   already_AddRefed<Blob> Slice(const Optional<int64_t>& aStart,
                                const Optional<int64_t>& aEnd,
-                               const nsAString& aContentType,
+                               const Optional<nsAString>& aContentType,
                                ErrorResult& aRv);
 
   size_t GetAllocationSize() const;
 
 protected:
   // File constructor should never be used directly. Use Blob::Create instead.
   Blob(nsISupports* aParent, BlobImpl* aImpl);
   virtual ~Blob();
diff --git a/dom/file/FileReader.h b/dom/file/FileReader.h
--- a/dom/file/FileReader.h
+++ b/dom/file/FileReader.h
@@ -67,19 +67,24 @@ public:
   // WebIDL
   static already_AddRefed<FileReader>
   Constructor(const GlobalObject& aGlobal, ErrorResult& aRv);
   void ReadAsArrayBuffer(JSContext* aCx, Blob& aBlob, ErrorResult& aRv)
   {
     ReadFileContent(aBlob, EmptyString(), FILE_AS_ARRAYBUFFER, aRv);
   }
 
-  void ReadAsText(Blob& aBlob, const nsAString& aLabel, ErrorResult& aRv)
+  void ReadAsText(Blob& aBlob, const Optional<nsAString>& aLabel,
+                  ErrorResult& aRv)
   {
-    ReadFileContent(aBlob, aLabel, FILE_AS_TEXT, aRv);
+    if (aLabel.WasPassed()) {
+      ReadFileContent(aBlob, aLabel.Value(), FILE_AS_TEXT, aRv);
+    } else {
+      ReadFileContent(aBlob, EmptyString(), FILE_AS_TEXT, aRv);
+    }
   }
 
   void ReadAsDataURL(Blob& aBlob, ErrorResult& aRv)
   {
     ReadFileContent(aBlob, EmptyString(), FILE_AS_DATAURL, aRv);
   }
 
   void Abort();
diff --git a/dom/webidl/Blob.webidl b/dom/webidl/Blob.webidl
--- a/dom/webidl/Blob.webidl
+++ b/dom/webidl/Blob.webidl
@@ -22,17 +22,17 @@ interface Blob {
 
   readonly attribute DOMString type;
 
   //slice Blob into byte-ranged chunks
 
   [Throws]
   Blob slice([Clamp] optional long long start,
              [Clamp] optional long long end,
-             optional DOMString contentType = "");
+             optional DOMString contentType);
 };
 
 enum EndingTypes { "transparent", "native" };
 
 dictionary BlobPropertyBag {
   DOMString type = "";
   EndingTypes endings = "transparent";
 };
diff --git a/dom/webidl/FileReader.webidl b/dom/webidl/FileReader.webidl
--- a/dom/webidl/FileReader.webidl
+++ b/dom/webidl/FileReader.webidl
@@ -1,28 +1,30 @@
 /* -*- Mode: IDL; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/.
  *
  * The origin of this IDL file is
- * http://dev.w3.org/2006/webapi/FileAPI/#dfn-filereader
+ * https://w3c.github.io/FileAPI/#APIASynch
  *
  * Copyright © 2013 W3C® (MIT, ERCIM, Keio, Beihang), All Rights Reserved. W3C
  * liability, trademark and document use rules apply.
  */
 
 [Constructor,
  Exposed=(Window,Worker,System)]
 interface FileReader : EventTarget {
   // async read methods
   [Throws]
   void readAsArrayBuffer(Blob blob);
   [Throws]
-  void readAsText(Blob blob, optional DOMString label = "");
+  void readAsBinaryString(Blob filedata);
+  [Throws]
+  void readAsText(Blob blob, optional DOMString label);
   [Throws]
   void readAsDataURL(Blob blob);
 
   void abort();
 
   // states
   const unsigned short EMPTY = 0;
   const unsigned short LOADING = 1;
@@ -41,14 +43,8 @@ interface FileReader : EventTarget {
   // event handler attributes
   attribute EventHandler onloadstart;
   attribute EventHandler onprogress;
   attribute EventHandler onload;
   attribute EventHandler onabort;
   attribute EventHandler onerror;
   attribute EventHandler onloadend;
 };
-
-// Mozilla extensions.
-partial interface FileReader {
-  [Throws]
-  void readAsBinaryString(Blob filedata);
-};
