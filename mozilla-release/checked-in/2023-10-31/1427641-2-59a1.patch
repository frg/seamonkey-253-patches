# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1512571362 0
# Node ID 1070a455ef173c643fa398a15e0033ea491a0bdd
# Parent  f9033e71d7284bcaaf1b831e7b5b879be59f60ad
Bug 1427641 - patch 2 - Convert variation values into FreeType's data type, and apply them to the FT_Face. r=lsalzman

diff --git a/gfx/thebes/gfxFT2FontBase.cpp b/gfx/thebes/gfxFT2FontBase.cpp
--- a/gfx/thebes/gfxFT2FontBase.cpp
+++ b/gfx/thebes/gfxFT2FontBase.cpp
@@ -5,20 +5,22 @@
 
 #include "gfxFT2FontBase.h"
 #include "gfxFT2Utils.h"
 #include "harfbuzz/hb.h"
 #include "mozilla/Likely.h"
 #include "gfxFontConstants.h"
 #include "gfxFontUtils.h"
 #include <algorithm>
+#include <dlfcn.h>
 
 #include FT_TRUETYPE_TAGS_H
 #include FT_TRUETYPE_TABLES_H
 #include FT_ADVANCES_H
+#include FT_MULTIPLE_MASTERS_H
 
 #ifndef FT_FACE_FLAG_COLOR
 #define FT_FACE_FLAG_COLOR ( 1L << 14 )
 #endif
 
 using namespace mozilla::gfx;
 
 gfxFT2FontBase::gfxFT2FontBase(const RefPtr<UnscaledFontFreeType>& aUnscaledFont,
@@ -200,16 +202,47 @@ gfxFT2FontBase::InitMetrics()
         mMetrics.underlineOffset = -underlineSize;
         mMetrics.strikeoutOffset = 0.25 * emHeight;
         mMetrics.strikeoutSize = underlineSize;
 
         SanitizeMetrics(&mMetrics, false);
         return;
     }
 
+    // For variation fonts, figure out the variation coordinates to be applied
+    // for each axis, in freetype's order (which may not match the order of
+    // axes in mStyle.variationSettings, so we need to search by axis tag).
+    if (face->face_flags & FT_FACE_FLAG_MULTIPLE_MASTERS) {
+        typedef FT_UInt (*GetVarFunc)(FT_Face, FT_MM_Var**);
+        typedef FT_UInt (*SetCoordsFunc)(FT_Face, FT_UInt, FT_Fixed*);
+        static GetVarFunc getVar =
+            (GetVarFunc)dlsym(RTLD_DEFAULT, "FT_Get_MM_Var");
+        static SetCoordsFunc setCoords =
+            (SetCoordsFunc)dlsym(RTLD_DEFAULT, "FT_Set_Var_Design_Coordinates");
+        FT_MM_Var* ftVar;
+        if (getVar && setCoords && FT_Err_Ok == (*getVar)(face, &ftVar)) {
+            for (unsigned i = 0; i < ftVar->num_axis; ++i) {
+                mCoords.AppendElement(ftVar->axis[i].def);
+                for (const auto& v : mStyle.variationSettings) {
+                    if (ftVar->axis[i].tag == v.mTag) {
+                        FT_Fixed val = v.mValue * 0x10000;
+                        val = std::min(val, ftVar->axis[i].maximum);
+                        val = std::max(val, ftVar->axis[i].minimum);
+                        mCoords[i] = val;
+                        break;
+                    }
+                }
+            }
+            free(ftVar);
+            if (!mCoords.IsEmpty()) {
+                (*setCoords)(face, mCoords.Length(), mCoords.Elements());
+            }
+        }
+    }
+
     const FT_Size_Metrics& ftMetrics = face->size->metrics;
 
     mMetrics.maxAscent = FLOAT_FROM_26_6(ftMetrics.ascender);
     mMetrics.maxDescent = -FLOAT_FROM_26_6(ftMetrics.descender);
     mMetrics.maxAdvance = FLOAT_FROM_26_6(ftMetrics.max_advance);
     gfxFloat lineHeight = FLOAT_FROM_26_6(ftMetrics.height);
 
     gfxFloat emHeight;
diff --git a/gfx/thebes/gfxFT2FontBase.h b/gfx/thebes/gfxFT2FontBase.h
--- a/gfx/thebes/gfxFT2FontBase.h
+++ b/gfx/thebes/gfxFT2FontBase.h
@@ -44,12 +44,19 @@ private:
 
 protected:
     virtual const Metrics& GetHorizontalMetrics() override;
 
     uint32_t mSpaceGlyph;
     Metrics mMetrics;
     bool    mEmbolden;
 
+    // For variation/multiple-master fonts, this will be an array of the values
+    // for each axis, as specified by mStyle.variationSettings (or the font's
+    // default for axes not present in variationSettings). Values here are in
+    // freetype's 16.16 fixed-point format, and clamped to the valid min/max
+    // range reported by the face.
+    nsTArray<FT_Fixed> mCoords;
+
     mozilla::UniquePtr<nsDataHashtable<nsUint32HashKey,int32_t>> mGlyphWidths;
 };
 
 #endif /* GFX_FT2FONTBASE_H */

