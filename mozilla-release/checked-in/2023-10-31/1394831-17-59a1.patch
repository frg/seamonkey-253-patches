# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1510566758 -3600
#      Mon Nov 13 10:52:38 2017 +0100
# Node ID f1b13194212c9cc0150abc4fed231c4c4a84c2fd
# Parent  ce448d8a91b3b4014346e9142ddb23fe0e0606ab
Bug 1394831 part 17 - Factor out maybeConvertToOrGrowDictionaryForAdd. r=bhackett

diff --git a/js/src/vm/NativeObject.h b/js/src/vm/NativeObject.h
--- a/js/src/vm/NativeObject.h
+++ b/js/src/vm/NativeObject.h
@@ -849,16 +849,21 @@ class NativeObject : public ShapedObject
   private:
     static MOZ_ALWAYS_INLINE Shape* getChildDataProperty(JSContext* cx, HandleNativeObject obj,
                                                          HandleShape parent,
                                                          MutableHandle<StackShape> child);
     static MOZ_ALWAYS_INLINE Shape* getChildAccessorProperty(JSContext* cx, HandleNativeObject obj,
                                                              HandleShape parent,
                                                              MutableHandle<StackShape> child);
 
+    static MOZ_ALWAYS_INLINE bool
+    maybeConvertToOrGrowDictionaryForAdd(JSContext* cx, HandleNativeObject obj, HandleId id,
+                                         ShapeTable** table, ShapeTable::Entry** entry,
+                                         const AutoKeepShapeTables& keep);
+
   public:
     /* Add a property whose id is not yet in this scope. */
     static MOZ_ALWAYS_INLINE Shape* addDataProperty(JSContext* cx, HandleNativeObject obj, HandleId id,
                                                     uint32_t slot, unsigned attrs);
 
     static MOZ_ALWAYS_INLINE Shape* addAccessorProperty(JSContext* cx, HandleNativeObject obj, HandleId id,
                                                         JSGetterOp getter, JSSetterOp setter,
                                                         unsigned attrs);
diff --git a/js/src/vm/Shape.cpp b/js/src/vm/Shape.cpp
--- a/js/src/vm/Shape.cpp
+++ b/js/src/vm/Shape.cpp
@@ -494,45 +494,55 @@ class MOZ_RAII AutoCheckShapeConsistency
     ~AutoCheckShapeConsistency() {
         obj_->checkShapeConsistency();
     }
 #endif
 };
 
 } // namespace js
 
+/* static */ MOZ_ALWAYS_INLINE bool
+NativeObject::maybeConvertToOrGrowDictionaryForAdd(JSContext* cx, HandleNativeObject obj, HandleId id,
+                                                   ShapeTable** table, ShapeTable::Entry** entry,
+                                                   const AutoKeepShapeTables& keep)
+{
+    MOZ_ASSERT(!!*table == !!*entry);
+
+    // The code below deals with either converting obj to dictionary mode or
+    // growing an object that's already in dictionary mode.
+    if (!obj->inDictionaryMode()) {
+        if (!ShouldConvertToDictionary(obj))
+            return true;
+        if (!toDictionaryMode(cx, obj))
+            return false;
+        *table = obj->lastProperty()->maybeTable(keep);
+    } else {
+        if (!(*table)->needsToGrow())
+            return true;
+        if (!(*table)->grow(cx))
+            return false;
+    }
+
+    *entry = &(*table)->search<MaybeAdding::Adding>(id, keep);
+    MOZ_ASSERT(!(*entry)->shape());
+    return true;
+}
+
 /* static */ Shape*
 NativeObject::addAccessorPropertyInternal(JSContext* cx,
                                           HandleNativeObject obj, HandleId id,
                                           GetterOp getter, SetterOp setter, unsigned attrs,
                                           ShapeTable* table, ShapeTable::Entry* entry,
                                           const AutoKeepShapeTables& keep)
 {
     AutoCheckShapeConsistency check(obj);
     AutoRooterGetterSetter gsRoot(cx, attrs, &getter, &setter);
 
-    // The code below deals with either converting obj to dictionary mode or
-    // growing an object that's already in dictionary mode.
-    if (!obj->inDictionaryMode()) {
-        if (ShouldConvertToDictionary(obj)) {
-            if (!toDictionaryMode(cx, obj))
-                return nullptr;
-            table = obj->lastProperty()->maybeTable(keep);
-            entry = &table->search<MaybeAdding::Adding>(id, keep);
-        }
-    } else {
-        if (table->needsToGrow()) {
-            if (!table->grow(cx))
-                return nullptr;
-            entry = &table->search<MaybeAdding::Adding>(id, keep);
-            MOZ_ASSERT(!entry->shape());
-        }
-    }
-
-    MOZ_ASSERT(!!table == !!entry);
+    if (!maybeConvertToOrGrowDictionaryForAdd(cx, obj, id, &table, &entry, keep))
+        return nullptr;
 
     // Find or create a property tree node labeled by our arguments.
     RootedShape shape(cx);
     {
         RootedShape last(cx, obj->lastProperty());
         Rooted<UnownedBaseShape*> nbase(cx, GetBaseShapeForNewShape(cx, last, id));
         if (!nbase)
             return nullptr;
@@ -568,35 +578,18 @@ NativeObject::addDataPropertyInternal(JS
                                       const AutoKeepShapeTables& keep)
 {
     AutoCheckShapeConsistency check(obj);
 
     // The slot, if any, must be a reserved slot.
     MOZ_ASSERT(slot == SHAPE_INVALID_SLOT ||
                slot < JSCLASS_RESERVED_SLOTS(obj->getClass()));
 
-    // The code below deals with either converting obj to dictionary mode or
-    // growing an object that's already in dictionary mode.
-    if (!obj->inDictionaryMode()) {
-        if (ShouldConvertToDictionary(obj)) {
-            if (!toDictionaryMode(cx, obj))
-                return nullptr;
-            table = obj->lastProperty()->maybeTable(keep);
-            entry = &table->search<MaybeAdding::Adding>(id, keep);
-        }
-    } else {
-        if (table->needsToGrow()) {
-            if (!table->grow(cx))
-                return nullptr;
-            entry = &table->search<MaybeAdding::Adding>(id, keep);
-            MOZ_ASSERT(!entry->shape());
-        }
-    }
-
-    MOZ_ASSERT(!!table == !!entry);
+    if (!maybeConvertToOrGrowDictionaryForAdd(cx, obj, id, &table, &entry, keep))
+        return nullptr;
 
     // Find or create a property tree node labeled by our arguments.
     RootedShape shape(cx);
     {
         RootedShape last(cx, obj->lastProperty());
         Rooted<UnownedBaseShape*> nbase(cx, GetBaseShapeForNewShape(cx, last, id));
         if (!nbase)
             return nullptr;
