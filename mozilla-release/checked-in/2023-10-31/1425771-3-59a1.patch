# HG changeset patch
# User Hiroyuki Ikezoe <hikezoe@mozilla.com>
# Date 1513574366 -32400
# Node ID 4f15412de9e861ebca0cd7f63a99ffff7a204a59
# Parent  defd32520915be804c88754236b856656e180f03
Bug 1425771 - Tweak expected restyle count for the case where the animation begins at the current time. r=birtles

The expected restyle count depends both on animation state and micro task
handling.

If we have the conformant Promise handling and if the animation begins at the
current time, we will observe 1 less restyles than observed frames since we
skip the first restyle in the initial frame.  This represents correctly what
we do and what we *should* do.

If we don't have the conformant Promise handling and if the animation doesn't
begin at the current time, we will observe 1 more restyles than obsered frames
since the Promise in observeStyling (precisely the Promise is inside the
callback for requestAnimationFrame) is fulfilled once after a restyling process
followed by the requestAnimationFrame.

MozReview-Commit-ID: FLhSRx4y1V7

diff --git a/dom/animation/test/mozilla/file_restyles.html b/dom/animation/test/mozilla/file_restyles.html
--- a/dom/animation/test/mozilla/file_restyles.html
+++ b/dom/animation/test/mozilla/file_restyles.html
@@ -94,16 +94,45 @@ function waitForWheelEvent(aTarget) {
 // asynchronously (e.g. using play()) and then ended up running the next frame
 // at precisely the time the animation started (due to aligning with vsync
 // refresh rate) then we won't end up restyling in that frame.
 function startsRightNow(aAnimation) {
   return aAnimation.startTime === aAnimation.timeline.currentTime &&
          aAnimation.currentTime === 0;
 }
 
+function tweakExpectedRestyleCount(aAnimation, aExpectedRestyleCount) {
+  // Normally we expect one restyling for each requestAnimationFrame (as
+  // called by observeRestyling) PLUS one for the last frame becasue of bug
+  // 1193394.  However, we won't observe that initial restyling unless BOTH of
+  // the following two conditions hold:
+  //
+  // 1. We are running *before* restyling happens.
+  // 2. The animation actually needs a restyle because it started prior to
+  //    this frame.  Even if (1) is true, in some cases due to aligning with
+  //    the refresh driver, the animation fame in which the ready promise is
+  //    resolved happens to coincide perfectly with the start time of the
+  //    animation.  In this case no restyling is needed so we won't observe
+  //    an additional restyle.
+  if (hasConformantPromiseHandling) {
+    // If we have the conformant Promise handling and |aAnimation| begins at
+    // the current timeline time, we will not process restyling in the initial
+    // frame.
+    if (startsRightNow(aAnimation)) {
+      return aExpectedRestyleCount - 1;
+    }
+  } else if (!startsRightNow(aAnimation)) {
+    // If we don't have the conformant Promise handling and |aAnimation|
+    // doesn't begin at the current timeline time, we will see an additional
+    // restyling in the last frame.
+    return aExpectedRestyleCount + 1;
+  }
+  return aExpectedRestyleCount;
+}
+
 var omtaEnabled = isOMTAEnabled();
 
 var isAndroid = !!navigator.userAgent.includes("Android");
 var isServo = isStyledByServo();
 var offscreenThrottlingEnabled =
   SpecialPowers.getBoolPref('dom.animations.offscreen-throttling');
 var hasConformantPromiseHandling;
 
@@ -140,29 +169,19 @@ waitForAllPaints(() => {
 
   add_task(async function restyling_for_main_thread_animations() {
     var div = addDiv(null, { style: 'animation: background-color 100s' });
     var animation = div.getAnimations()[0];
 
     await animation.ready;
     ok(!SpecialPowers.wrap(animation).isRunningOnCompositor);
 
-    // Normally we expect one restyling for each requestAnimationFrame (as
-    // called by observeRestyling) PLUS one for the last frame becasue of bug
-    // 1193394.  However, we won't observe that initial restyling unless BOTH of
-    // the following two conditions hold:
-    //
-    // 1. We are running *before* restyling happens.
-    // 2. The animation actually needs a restyle because it started prior to
-    //    this frame.  Even if (1) is true, in some cases due to aligning with
-    //    the refresh driver, the animation fame in which the ready promise is
-    //    resolved happens to coincide perfectly with the start time of the
-    //    animation.  In this case no restyling is needed so we won't observe
-    //    an additional restyle.
-    const expectedRestyleCount = !startsRightNow(animation) ? 6 : 5;
+    // We need to tweak expected restyle count depending on animation state and
+    // micro task handling.
+    const expectedRestyleCount = tweakExpectedRestyleCount(animation, 5);
     var markers = await observeStyling(5);
     is(markers.length, expectedRestyleCount,
        'CSS animations running on the main-thread should update style ' +
        'on the main thread');
     await ensureElementRemoval(div);
   });
 
   add_task_if_omta_enabled(async function no_restyling_for_compositor_animations() {
@@ -1000,17 +1019,17 @@ waitForAllPaints(() => {
     }
 
     var div = addDiv(null, { style: 'transform: translateY(-400px);' });
     var animation =
       div.animate([{ visibility: 'visible' }], 100 * MS_PER_SEC);
 
     await animation.ready;
 
-    const expectedRestyleCount = !startsRightNow(animation) ? 6 : 5;
+    const expectedRestyleCount = tweakExpectedRestyleCount(animation, 5);
     var markers = await observeStyling(5);
 
     is(markers.length, expectedRestyleCount,
        'Discrete animation has has no keyframe whose offset is 0 or 1 in an ' +
        'out-of-view element should not be throttled');
     await ensureElementRemoval(div);
   });
 
@@ -1068,17 +1087,17 @@ waitForAllPaints(() => {
     var rect = addSVGElement(svg, 'rect', { x:      '-10',
                                             y:      '-10',
                                             width:  '10',
                                             height: '10',
                                             fill:   'red' });
     var animation = rect.animate({ fill: ['blue', 'lime'] }, 100 * MS_PER_SEC);
     await animation.ready;
 
-    const expectedRestyleCount = !startsRightNow(animation) ? 6 : 5;
+    const expectedRestyleCount = tweakExpectedRestyleCount(animation, 5);
     var markers = await observeStyling(5);
     is(markers.length, expectedRestyleCount,
        'CSS animations on an in-view svg element with post-transform should ' +
        'not be throttled.');
 
     await ensureElementRemoval(div);
   });
 
@@ -1129,17 +1148,17 @@ waitForAllPaints(() => {
     var targetDiv = addDiv(null,
                            { style: 'animation: background-color 100s;' +
                                     'transform: translate(-50px, -50px);' });
     scrollDiv.appendChild(targetDiv);
 
     var animation = targetDiv.getAnimations()[0];
     await animation.ready;
 
-    const expectedRestyleCount = !startsRightNow(animation) ? 6 : 5;
+    const expectedRestyleCount = tweakExpectedRestyleCount(animation, 5);
     var markers = await observeStyling(5);
     is(markers.length, expectedRestyleCount,
        'CSS animation on an in-view element with pre-transform should not ' +
        'be throttled.');
 
     await ensureElementRemoval(scrollDiv);
   });
 

