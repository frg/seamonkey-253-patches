# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1512938023 -3600
#      Sun Dec 10 21:33:43 2017 +0100
# Node ID c27eaf506c73c88f90d5b1966046b4e173d00825
# Parent  f9b7dc9f7e08bca6862b1706660b042ec1ccbe4b
Bug 1404997 - P20. Make MediaStreamListener::NotifyPull asynchronous. r=padenot

The operations is done in two ways:
1- Process all the MediaStreamListener at once, which returns a promise that will be resolved once the operation is completed.
2- As the Cubeb audio callback must be resolved immediately, the MSG will wait for all the promises to be resolved until it continues the operation of feeding the callback the necessary data.

This will allow to parallelize the stream's tracks' audio decoding.

MozReview-Commit-ID: EeoDvxnJyWV

diff --git a/dom/media/MediaStreamGraph.cpp b/dom/media/MediaStreamGraph.cpp
--- a/dom/media/MediaStreamGraph.cpp
+++ b/dom/media/MediaStreamGraph.cpp
@@ -1144,22 +1144,35 @@ MediaStreamGraphImpl::UpdateGraph(GraphT
   // been woken up right after having been to sleep.
   MOZ_ASSERT(aEndBlockingDecisions >= mStateComputedTime);
 
   UpdateStreamOrder();
 
   bool ensureNextIteration = false;
 
   // Grab pending stream input and compute blocking time
+  // TODO: Ensure that heap memory allocations isn't going to be a problem.
+  // Maybe modify code to use nsAutoTArray as out parameters.
+  nsTArray<RefPtr<SourceMediaStream::NotifyPullPromise>> promises;
   for (MediaStream* stream : mStreams) {
     if (SourceMediaStream* is = stream->AsSourceStream()) {
-      is->PullNewData(aEndBlockingDecisions, &ensureNextIteration);
+      promises.AppendElements(
+        is->PullNewData(aEndBlockingDecisions, &ensureNextIteration));
+    }
+  }
+
+  // Wait until all PullEnabled stream's listeners have completed.
+  if (!promises.IsEmpty()) {
+    AwaitAll(promises);
+  }
+
+  for (MediaStream* stream : mStreams) {
+    if (SourceMediaStream* is = stream->AsSourceStream()) {
       is->ExtractPendingInput();
     }
-
     if (stream->mFinished) {
       // The stream's not suspended, and since it's finished, underruns won't
       // stop it playing out. So there's no blocking other than what we impose
       // here.
       GraphTime endTime = stream->GetStreamTracks().GetAllTracksEnd() +
           stream->mTracksStartTime;
       if (endTime <= mStateComputedTime) {
         LOG(LogLevel::Verbose,
@@ -2689,53 +2702,58 @@ SourceMediaStream::SetPullEnabled(bool a
 {
   MutexAutoLock lock(mMutex);
   mPullEnabled = aEnabled;
   if (mPullEnabled && GraphImpl()) {
     GraphImpl()->EnsureNextIteration();
   }
 }
 
-void
+nsTArray<RefPtr<SourceMediaStream::NotifyPullPromise>>
 SourceMediaStream::PullNewData(StreamTime aDesiredUpToTime,
                                bool* aEnsureNextIteration)
 {
+  // 2 is the average number of listeners per SourceMediaStream.
+  nsTArray<RefPtr<SourceMediaStream::NotifyPullPromise>> promises(2);
   MutexAutoLock lock(mMutex);
-  if (mPullEnabled && !mFinished && !mListeners.IsEmpty()) {
-    // Compute how much stream time we'll need assuming we don't block
-    // the stream at all.
-    StreamTime t = GraphTimeToStreamTime(aDesiredUpToTime);
-    StreamTime current = mTracks.GetEnd();
-    LOG(LogLevel::Verbose,
-        ("Calling NotifyPull aStream=%p t=%f current end=%f",
-          this,
-          GraphImpl()->MediaTimeToSeconds(t),
-          GraphImpl()->MediaTimeToSeconds(current)));
-    if (t > current) {
-      *aEnsureNextIteration = true;
+  if (!mPullEnabled || mFinished) {
+    return promises;
+  }
+  // Compute how much stream time we'll need assuming we don't block
+  // the stream at all.
+  StreamTime t = GraphTimeToStreamTime(aDesiredUpToTime);
+  StreamTime current = mTracks.GetEnd();
+  LOG(LogLevel::Verbose,
+      ("Calling NotifyPull aStream=%p t=%f current end=%f",
+        this,
+        GraphImpl()->MediaTimeToSeconds(t),
+        GraphImpl()->MediaTimeToSeconds(current)));
+  if (t <= current) {
+    return promises;
+  }
+  *aEnsureNextIteration = true;
 #ifdef DEBUG
-      if (mListeners.Length() == 0) {
-        LOG(
-          LogLevel::Error,
-          ("No listeners in NotifyPull aStream=%p desired=%f current end=%f",
-            this,
-            GraphImpl()->MediaTimeToSeconds(t),
-            GraphImpl()->MediaTimeToSeconds(current)));
-        DumpTrackInfo();
-      }
+  if (mListeners.Length() == 0) {
+    LOG(
+      LogLevel::Error,
+      ("No listeners in NotifyPull aStream=%p desired=%f current end=%f",
+        this,
+        GraphImpl()->MediaTimeToSeconds(t),
+        GraphImpl()->MediaTimeToSeconds(current)));
+    DumpTrackInfo();
+  }
 #endif
-      for (uint32_t j = 0; j < mListeners.Length(); ++j) {
-        MediaStreamListener* l = mListeners[j];
-        {
-          MutexAutoUnlock unlock(mMutex);
-          l->NotifyPull(GraphImpl(), t);
-        }
-      }
+  for (uint32_t j = 0; j < mListeners.Length(); ++j) {
+    MediaStreamListener* l = mListeners[j];
+    {
+      MutexAutoUnlock unlock(mMutex);
+      promises.AppendElement(l->AsyncNotifyPull(GraphImpl(), t));
     }
   }
+  return promises;
 }
 
 void
 SourceMediaStream::ExtractPendingInput()
 {
   MutexAutoLock lock(mMutex);
 
   bool finished = mFinishPending;
diff --git a/dom/media/MediaStreamGraph.h b/dom/media/MediaStreamGraph.h
--- a/dom/media/MediaStreamGraph.h
+++ b/dom/media/MediaStreamGraph.h
@@ -7,16 +7,17 @@
 #define MOZILLA_MEDIASTREAMGRAPH_H_
 
 #include "AudioStream.h"
 #include "MainThreadUtils.h"
 #include "MediaStreamTypes.h"
 #include "StreamTracks.h"
 #include "VideoSegment.h"
 #include "mozilla/LinkedList.h"
+#include "mozilla/MozPromise.h"
 #include "mozilla/Mutex.h"
 #include "mozilla/TaskQueue.h"
 #include "nsAutoPtr.h"
 #include "nsAutoRef.h"
 #include "nsIRunnable.h"
 #include "nsTArray.h"
 #include <speex/speex_resampler.h>
 
@@ -697,17 +698,19 @@ public:
    * it is still possible for a NotifyPull to occur.
    */
   void SetPullEnabled(bool aEnabled);
 
   /**
    * Call all MediaStreamListeners to request new data via the NotifyPull API
    * (if enabled).
    */
-  void PullNewData(StreamTime aDesiredUpToTime, bool* aEnsureNextIteration);
+  typedef MozPromise<bool, bool, true /* is exclusive */ > NotifyPullPromise;
+  nsTArray<RefPtr<NotifyPullPromise>> PullNewData(StreamTime aDesiredUpToTime,
+                                                  bool* aEnsureNextIteration);
 
   /**
    * Extract any state updates pending in the stream, and apply them.
    */
   void ExtractPendingInput();
 
   /**
    * These add/remove DirectListeners, which allow bypassing the graph and any
diff --git a/dom/media/MediaStreamListener.h b/dom/media/MediaStreamListener.h
--- a/dom/media/MediaStreamListener.h
+++ b/dom/media/MediaStreamListener.h
@@ -34,17 +34,18 @@ class VideoSegment;
  * You should do something non-blocking and non-reentrant (e.g. dispatch an
  * event to some thread) and return.
  * The listener is not allowed to add/remove any listeners from the stream.
  *
  * When a listener is first attached, we guarantee to send a NotifyBlockingChanged
  * callback to notify of the initial blocking state. Also, if a listener is
  * attached to a stream that has already finished, we'll call NotifyFinished.
  */
-class MediaStreamListener {
+class MediaStreamListener
+{
 protected:
   // Protected destructor, to discourage deletion outside of Release():
   virtual ~MediaStreamListener() {}
 
 public:
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(MediaStreamListener)
 
   /**
@@ -55,16 +56,24 @@ public:
    * calls to add or remove MediaStreamListeners. It is not allowed to block
    * for any length of time.
    * aDesiredTime is the stream time we would like to get data up to. Data
    * beyond this point will not be played until NotifyPull runs again, so there's
    * not much point in providing it. Note that if the stream is blocked for
    * some reason, then data before aDesiredTime may not be played immediately.
    */
   virtual void NotifyPull(MediaStreamGraph* aGraph, StreamTime aDesiredTime) {}
+  virtual RefPtr<SourceMediaStream::NotifyPullPromise> AsyncNotifyPull(
+    MediaStreamGraph* aGraph,
+    StreamTime aDesiredTime)
+  {
+    NotifyPull(aGraph, aDesiredTime);
+    return SourceMediaStream::NotifyPullPromise::CreateAndResolve(true,
+                                                                  __func__);
+  }
 
   enum Blocking {
     BLOCKED,
     UNBLOCKED
   };
   /**
    * Notify that the blocking status of the stream changed. The initial state
    * is assumed to be BLOCKED.
