# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1512011233 -28800
# Node ID ef7f09a38f92f6be601349a4269ef0af3987921f
# Parent  52854bd23f9c4f5aedb3beeb24df8bf66f7a7753
Bug 1421875. P2 - move MediaDecoder::NotifyDataArrived() down the class hierarchy. r=bechen,gerald

For it is never used by ChannelMediaDecoder.

MozReview-Commit-ID: Jtvlj0iwTm7

diff --git a/dom/media/MediaDecoder.cpp b/dom/media/MediaDecoder.cpp
--- a/dom/media/MediaDecoder.cpp
+++ b/dom/media/MediaDecoder.cpp
@@ -1404,23 +1404,16 @@ MediaDecoder::NotifyReaderDataArrived()
 
   nsresult rv = mReader->OwnerThread()->Dispatch(
     NewRunnableMethod("MediaFormatReader::NotifyDataArrived",
                       mReader.get(),
                       &MediaFormatReader::NotifyDataArrived));
   MOZ_DIAGNOSTIC_ASSERT(NS_SUCCEEDED(rv));
 }
 
-void
-MediaDecoder::NotifyDataArrived()
-{
-  NotifyReaderDataArrived();
-  DownloadProgressed();
-}
-
 // Provide access to the state machine object
 MediaDecoderStateMachine*
 MediaDecoder::GetStateMachine() const
 {
   MOZ_ASSERT(NS_IsMainThread());
   return mDecoderStateMachine;
 }
 
diff --git a/dom/media/MediaDecoder.h b/dom/media/MediaDecoder.h
--- a/dom/media/MediaDecoder.h
+++ b/dom/media/MediaDecoder.h
@@ -174,20 +174,16 @@ public:
   virtual void RemoveOutputStream(MediaStream* aStream);
 
   // Return the duration of the video in seconds.
   virtual double GetDuration();
 
   // Return true if the stream is infinite.
   bool IsInfinite() const;
 
-  // Called as data arrives on the stream and is read into the cache.  Called
-  // on the main thread only.
-  void NotifyDataArrived();
-
   // Return true if we are currently seeking in the media resource.
   // Call on the main thread only.
   bool IsSeeking() const;
 
   // Return true if the decoder has reached the end of playback.
   bool IsEnded() const;
 
   // True if we are playing a MediaSource object.
diff --git a/dom/media/hls/HLSDecoder.cpp b/dom/media/hls/HLSDecoder.cpp
--- a/dom/media/hls/HLSDecoder.cpp
+++ b/dom/media/hls/HLSDecoder.cpp
@@ -235,9 +235,17 @@ HLSDecoder::Shutdown()
   }
   if (mJavaCallbacks) {
     HLSResourceCallbacksSupport::DisposeNative(mJavaCallbacks);
     mJavaCallbacks = nullptr;
   }
   MediaDecoder::Shutdown();
 }
 
+void
+HLSDecoder::NotifyDataArrived()
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  NotifyReaderDataArrived();
+  DownloadProgressed();
+}
+
 } // namespace mozilla
diff --git a/dom/media/hls/HLSDecoder.h b/dom/media/hls/HLSDecoder.h
--- a/dom/media/hls/HLSDecoder.h
+++ b/dom/media/hls/HLSDecoder.h
@@ -36,16 +36,19 @@ public:
 
   void AddSizeOfResources(ResourceSizes* aSizes) override;
   already_AddRefed<nsIPrincipal> GetCurrentPrincipal() override;
   bool IsTransportSeekable() override { return true; }
   void Suspend() override;
   void Resume() override;
   void Shutdown() override;
 
+  // Called as data arrives on the underlying HLS player. Main thread only.
+  void NotifyDataArrived();
+
 private:
   friend class HLSResourceCallbacksSupport;
 
   void PinForSeek() override {}
   void UnpinForSeek() override {}
 
   MediaDecoderStateMachine* CreateStateMachine();
 
diff --git a/dom/media/mediasource/MediaSourceDecoder.cpp b/dom/media/mediasource/MediaSourceDecoder.cpp
--- a/dom/media/mediasource/MediaSourceDecoder.cpp
+++ b/dom/media/mediasource/MediaSourceDecoder.cpp
@@ -359,16 +359,24 @@ MediaSourceDecoder::NotifyInitDataArrive
   MOZ_ASSERT(NS_IsMainThread());
   AbstractThread::AutoEnter context(AbstractMainThread());
 
   if (mDemuxer) {
     mDemuxer->NotifyInitDataArrived();
   }
 }
 
+void
+MediaSourceDecoder::NotifyDataArrived()
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  NotifyReaderDataArrived();
+  DownloadProgressed();
+}
+
 already_AddRefed<nsIPrincipal>
 MediaSourceDecoder::GetCurrentPrincipal()
 {
   MOZ_ASSERT(NS_IsMainThread());
   return do_AddRef(mPrincipal);
 }
 
 #undef MSE_DEBUG
diff --git a/dom/media/mediasource/MediaSourceDecoder.h b/dom/media/mediasource/MediaSourceDecoder.h
--- a/dom/media/mediasource/MediaSourceDecoder.h
+++ b/dom/media/mediasource/MediaSourceDecoder.h
@@ -63,16 +63,20 @@ public:
   void AddSizeOfResources(ResourceSizes* aSizes) override;
 
   MediaDecoderOwner::NextFrameStatus NextFrameBufferedStatus() override;
 
   bool IsMSE() const override { return true; }
 
   void NotifyInitDataArrived();
 
+  // Called as data appended to the source buffer or EOS is called on the media
+  // source. Main thread only.
+  void NotifyDataArrived();
+
 private:
   void PinForSeek() override {}
   void UnpinForSeek() override {}
   MediaDecoderStateMachine* CreateStateMachine();
   void DoSetMediaSourceDuration(double aDuration);
   media::TimeInterval ClampIntervalToEnd(const media::TimeInterval& aInterval);
   bool CanPlayThroughImpl() override;
   bool IsLiveStream() override final { return !mEnded; }
