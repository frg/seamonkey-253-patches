# HG changeset patch
# User Marco Castelluccio <mcastelluccio@mozilla.com>
# Date 1509395700 0
#      Mon Oct 30 20:35:00 2017 +0000
# Node ID 96371614d917585de513af03eaceb87fd0e2596b
# Parent  bf09fad10de6ffdfa72dfc216e84d6e834b533e3
Bug 1413013 - Remove unused nsITransactionList. r=masayuki

diff --git a/editor/txmgr/moz.build b/editor/txmgr/moz.build
--- a/editor/txmgr/moz.build
+++ b/editor/txmgr/moz.build
@@ -3,28 +3,26 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 TEST_DIRS += ['tests']
 
 XPIDL_SOURCES += [
     'nsITransaction.idl',
-    'nsITransactionList.idl',
     'nsITransactionListener.idl',
     'nsITransactionManager.idl',
 ]
 
 XPIDL_MODULE = 'txmgr'
 
 EXPORTS += [
     'nsTransactionManagerCID.h',
 ]
 
 UNIFIED_SOURCES += [
     'nsTransactionItem.cpp',
-    'nsTransactionList.cpp',
     'nsTransactionManager.cpp',
     'nsTransactionManagerFactory.cpp',
     'nsTransactionStack.cpp',
 ]
 
 FINAL_LIBRARY = 'xul'
diff --git a/editor/txmgr/nsITransactionList.idl b/editor/txmgr/nsITransactionList.idl
deleted file mode 100644
--- a/editor/txmgr/nsITransactionList.idl
+++ /dev/null
@@ -1,70 +0,0 @@
-/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#include "nsISupports.idl"
-
-interface nsITransaction;
-
-/*
- * The nsITransactionList interface.
- * <P>
- * The implementation for this interface is provided by the Transaction Manager.
- * This interface provides a mechanism for accessing the transactions on the
- * Undo or Redo stacks as well as any auto-aggregated children that a
- * transaction may have.
- */
-[scriptable, uuid(d007ceff-c978-486a-b697-384ca01997be)]
-
-interface nsITransactionList : nsISupports
-{
-  /**
-   * The number of transactions contained in this list.
-   */
-  readonly attribute long numItems;
-
-  /**
-   * itemIsBatch() returns true if the item at aIndex is a batch. Note that
-   * currently there is no requirement for a TransactionManager implementation
-   * to associate a toplevel nsITransaction with a batch so it is possible for
-   * itemIsBatch() to return true and getItem() to return null. However, you
-   * can still access the transactions contained in the batch with a call to
-   * getChildListForItem().
-   * @param aIndex The index of the item in the list.
-   */
-  boolean itemIsBatch(in long aIndex);
-
-  /**
-   * getItem() returns the transaction at the given index in the list. Note that
-   * a null can be returned here if the item is a batch. The transaction
-   * returned is AddRef'd so it is up to the caller to Release the transaction
-   * when it is done.
-   * @param aIndex The index of the item in the list.
-   */
-  nsITransaction getItem(in long aIndex);
-
-  /**
-   * getData() returns the data (of type nsISupports array) associated with
-   * the transaction list.
-   */
-  void getData(in long aIndex, [optional] out unsigned long aLength,
-               [array, size_is(aLength), retval] out nsISupports aData);
-
-  /**
-   * getNumChildrenForItem() returns the number of child (auto-aggreated)
-   * transactions the item at aIndex has.
-   * @param aIndex The index of the item in the list.
-   */
-  long getNumChildrenForItem(in long aIndex);
-
-  /**
-   * getChildListForItem() returns the list of children associated with the
-   * item at aIndex. Implementations may return null if there are no children,
-   * or an empty list. The list returned is AddRef'd so it is up to the caller
-   * to Release the transaction when it is done.
-   * @param aIndex The index of the item in the list.
-   */
-  nsITransactionList getChildListForItem(in long aIndex);
-};
-
diff --git a/editor/txmgr/nsITransactionManager.idl b/editor/txmgr/nsITransactionManager.idl
--- a/editor/txmgr/nsITransactionManager.idl
+++ b/editor/txmgr/nsITransactionManager.idl
@@ -1,16 +1,15 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsISupports.idl"
 #include "nsITransaction.idl"
-#include "nsITransactionList.idl"
 #include "nsITransactionListener.idl"
 
 %{ C++
 
 #define NS_TRANSACTIONMANAGER_CONTRACTID "@mozilla.org/transactionmanager;1"
 
 %} C++
 
@@ -63,17 +62,17 @@ interface nsITransactionManager : nsISup
 
   /**
    * Turns on the transaction manager's batch mode, forcing all transactions
    * executed by the transaction manager's doTransaction() method to be
    * aggregated together until EndBatch() is called.  This mode allows an
    * application to execute and group together several independent transactions
    * so they can be undone with a single call to undoTransaction().
    * @param aData An arbitrary nsISupports object that is associated with the
-   * batch. Can be retrieved from nsITransactionList.
+   * batch. Can be retrieved from the undo or redo stacks.
    */
   void beginBatch(in nsISupports aData);
 
   /**
    * Turns off the transaction manager's batch mode.
    * @param aAllowEmpty If true, a batch containing no children will be
    * pushed onto the undo stack. Otherwise, ending a batch with no
    * children will result in no transactions being pushed on the undo stack.
@@ -130,30 +129,16 @@ interface nsITransactionManager : nsISup
    * Returns an AddRef'd pointer to the transaction at the top of the
    * redo stack. Callers should be aware that this method could return
    * return a null in some implementations if there is a batch at the top
    * of the redo stack.
    */
   nsITransaction peekRedoStack();
 
   /**
-   * Returns the list of transactions on the undo stack. Note that the
-   * transaction at the top of the undo stack will actually be at the
-   * index 'n-1' in the list, where 'n' is the number of items in the list.
-   */
-  nsITransactionList getUndoList();
-
-  /**
-   * Returns the list of transactions on the redo stack. Note that the
-   * transaction at the top of the redo stack will actually be at the
-   * index 'n-1' in the list, where 'n' is the number of items in the list.
-   */
-  nsITransactionList getRedoList();
-
-  /**
    * Adds a listener to the transaction manager's notification list. Listeners
    * are notified whenever a transaction is done, undone, or redone.
    * <P>
    * The listener's AddRef() method is called.
    * @param aListener the lister to add.
    */
   void AddListener(in nsITransactionListener aListener);
 
diff --git a/editor/txmgr/nsTransactionList.cpp b/editor/txmgr/nsTransactionList.cpp
deleted file mode 100644
--- a/editor/txmgr/nsTransactionList.cpp
+++ /dev/null
@@ -1,174 +0,0 @@
-/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#include "mozilla/mozalloc.h"
-#include "nsCOMPtr.h"
-#include "nsDebug.h"
-#include "nsError.h"
-#include "nsID.h"
-#include "nsISupportsUtils.h"
-#include "nsITransactionManager.h"
-#include "nsTransactionItem.h"
-#include "nsTransactionList.h"
-#include "nsTransactionStack.h"
-#include "nscore.h"
-
-NS_IMPL_ISUPPORTS(nsTransactionList, nsITransactionList)
-
-nsTransactionList::nsTransactionList(nsITransactionManager *aTxnMgr, nsTransactionStack *aTxnStack)
-  : mTxnStack(aTxnStack)
-  , mTxnItem(nullptr)
-{
-  if (aTxnMgr)
-    mTxnMgr = do_GetWeakReference(aTxnMgr);
-}
-
-nsTransactionList::nsTransactionList(nsITransactionManager *aTxnMgr, nsTransactionItem *aTxnItem)
-  : mTxnStack(0)
-  , mTxnItem(aTxnItem)
-{
-  if (aTxnMgr)
-    mTxnMgr = do_GetWeakReference(aTxnMgr);
-}
-
-nsTransactionList::~nsTransactionList()
-{
-  mTxnStack = 0;
-  mTxnItem  = nullptr;
-}
-
-NS_IMETHODIMP nsTransactionList::GetNumItems(int32_t *aNumItems)
-{
-  NS_ENSURE_TRUE(aNumItems, NS_ERROR_NULL_POINTER);
-
-  *aNumItems = 0;
-
-  nsCOMPtr<nsITransactionManager> txMgr = do_QueryReferent(mTxnMgr);
-  NS_ENSURE_TRUE(txMgr, NS_ERROR_FAILURE);
-
-  if (mTxnStack) {
-    *aNumItems = mTxnStack->GetSize();
-  } else if (mTxnItem) {
-    return mTxnItem->GetNumberOfChildren(aNumItems);
-  }
-
-  return NS_OK;
-}
-
-NS_IMETHODIMP nsTransactionList::ItemIsBatch(int32_t aIndex, bool *aIsBatch)
-{
-  NS_ENSURE_TRUE(aIsBatch, NS_ERROR_NULL_POINTER);
-
-  *aIsBatch = false;
-
-  nsCOMPtr<nsITransactionManager> txMgr = do_QueryReferent(mTxnMgr);
-  NS_ENSURE_TRUE(txMgr, NS_ERROR_FAILURE);
-
-  RefPtr<nsTransactionItem> item;
-  if (mTxnStack) {
-    item = mTxnStack->GetItem(aIndex);
-  } else if (mTxnItem) {
-    nsresult rv = mTxnItem->GetChild(aIndex, getter_AddRefs(item));
-    NS_ENSURE_SUCCESS(rv, rv);
-  }
-  NS_ENSURE_TRUE(item, NS_ERROR_FAILURE);
-
-  return item->GetIsBatch(aIsBatch);
-}
-
-NS_IMETHODIMP nsTransactionList::GetData(int32_t aIndex,
-                                         uint32_t *aLength,
-                                         nsISupports ***aData)
-{
-  nsCOMPtr<nsITransactionManager> txMgr = do_QueryReferent(mTxnMgr);
-  NS_ENSURE_TRUE(txMgr, NS_ERROR_FAILURE);
-
-  RefPtr<nsTransactionItem> item;
-  if (mTxnStack) {
-    item = mTxnStack->GetItem(aIndex);
-  } else if (mTxnItem) {
-    nsresult rv = mTxnItem->GetChild(aIndex, getter_AddRefs(item));
-    NS_ENSURE_SUCCESS(rv, rv);
-  }
-
-  nsCOMArray<nsISupports>& data = item->GetData();
-  nsISupports** ret = static_cast<nsISupports**>(moz_xmalloc(data.Count() *
-    sizeof(nsISupports*)));
-
-  for (int32_t i = 0; i < data.Count(); i++) {
-    NS_ADDREF(ret[i] = data[i]);
-  }
-  *aLength = data.Count();
-  *aData = ret;
-  return NS_OK;
-}
-
-NS_IMETHODIMP nsTransactionList::GetItem(int32_t aIndex, nsITransaction **aItem)
-{
-  NS_ENSURE_TRUE(aItem, NS_ERROR_NULL_POINTER);
-
-  *aItem = 0;
-
-  nsCOMPtr<nsITransactionManager> txMgr = do_QueryReferent(mTxnMgr);
-  NS_ENSURE_TRUE(txMgr, NS_ERROR_FAILURE);
-
-  RefPtr<nsTransactionItem> item;
-  if (mTxnStack) {
-    item = mTxnStack->GetItem(aIndex);
-  } else if (mTxnItem) {
-    nsresult rv = mTxnItem->GetChild(aIndex, getter_AddRefs(item));
-    NS_ENSURE_SUCCESS(rv, rv);
-  }
-  NS_ENSURE_TRUE(item, NS_ERROR_FAILURE);
-
-  *aItem = item->GetTransaction().take();
-  return NS_OK;
-}
-
-NS_IMETHODIMP nsTransactionList::GetNumChildrenForItem(int32_t aIndex, int32_t *aNumChildren)
-{
-  NS_ENSURE_TRUE(aNumChildren, NS_ERROR_NULL_POINTER);
-
-  *aNumChildren = 0;
-
-  nsCOMPtr<nsITransactionManager> txMgr = do_QueryReferent(mTxnMgr);
-  NS_ENSURE_TRUE(txMgr, NS_ERROR_FAILURE);
-
-  RefPtr<nsTransactionItem> item;
-  if (mTxnStack) {
-    item = mTxnStack->GetItem(aIndex);
-  } else if (mTxnItem) {
-    nsresult rv = mTxnItem->GetChild(aIndex, getter_AddRefs(item));
-    NS_ENSURE_SUCCESS(rv, rv);
-  }
-  NS_ENSURE_TRUE(item, NS_ERROR_FAILURE);
-
-  return item->GetNumberOfChildren(aNumChildren);
-}
-
-NS_IMETHODIMP nsTransactionList::GetChildListForItem(int32_t aIndex, nsITransactionList **aTxnList)
-{
-  NS_ENSURE_TRUE(aTxnList, NS_ERROR_NULL_POINTER);
-
-  *aTxnList = 0;
-
-  nsCOMPtr<nsITransactionManager> txMgr = do_QueryReferent(mTxnMgr);
-  NS_ENSURE_TRUE(txMgr, NS_ERROR_FAILURE);
-
-  RefPtr<nsTransactionItem> item;
-  if (mTxnStack) {
-    item = mTxnStack->GetItem(aIndex);
-  } else if (mTxnItem) {
-    nsresult rv = mTxnItem->GetChild(aIndex, getter_AddRefs(item));
-    NS_ENSURE_SUCCESS(rv, rv);
-  }
-  NS_ENSURE_TRUE(item, NS_ERROR_FAILURE);
-
-  *aTxnList = (nsITransactionList *)new nsTransactionList(txMgr, item);
-  NS_ENSURE_TRUE(*aTxnList, NS_ERROR_OUT_OF_MEMORY);
-
-  NS_ADDREF(*aTxnList);
-  return NS_OK;
-}
diff --git a/editor/txmgr/nsTransactionList.h b/editor/txmgr/nsTransactionList.h
deleted file mode 100644
--- a/editor/txmgr/nsTransactionList.h
+++ /dev/null
@@ -1,46 +0,0 @@
-/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#ifndef nsTransactionList_h__
-#define nsTransactionList_h__
-
-#include "nsISupportsImpl.h"
-#include "nsITransactionList.h"
-#include "nsIWeakReferenceUtils.h"
-
-class nsITransaction;
-class nsITransactionManager;
-class nsTransactionItem;
-class nsTransactionStack;
-
-/** implementation of a transaction list object.
- *
- */
-class nsTransactionList : public nsITransactionList
-{
-private:
-
-  nsWeakPtr                   mTxnMgr;
-  nsTransactionStack         *mTxnStack;
-  RefPtr<nsTransactionItem> mTxnItem;
-
-protected:
-  virtual ~nsTransactionList();
-
-public:
-
-  nsTransactionList(nsITransactionManager *aTxnMgr, nsTransactionStack *aTxnStack);
-  nsTransactionList(nsITransactionManager *aTxnMgr, nsTransactionItem *aTxnItem);
-
-  /* Macro for AddRef(), Release(), and QueryInterface() */
-  NS_DECL_ISUPPORTS
-
-  /* nsITransactionManager method implementations. */
-  NS_DECL_NSITRANSACTIONLIST
-
-  /* nsTransactionList specific methods. */
-};
-
-#endif // nsTransactionList_h__
diff --git a/editor/txmgr/nsTransactionManager.cpp b/editor/txmgr/nsTransactionManager.cpp
--- a/editor/txmgr/nsTransactionManager.cpp
+++ b/editor/txmgr/nsTransactionManager.cpp
@@ -6,21 +6,19 @@
 #include "mozilla/Assertions.h"
 #include "mozilla/mozalloc.h"
 #include "nsCOMPtr.h"
 #include "nsDebug.h"
 #include "nsError.h"
 #include "nsISupportsBase.h"
 #include "nsISupportsUtils.h"
 #include "nsITransaction.h"
-#include "nsITransactionList.h"
 #include "nsITransactionListener.h"
 #include "nsIWeakReference.h"
 #include "nsTransactionItem.h"
-#include "nsTransactionList.h"
 #include "nsTransactionManager.h"
 #include "nsTransactionStack.h"
 
 nsTransactionManager::nsTransactionManager(int32_t aMaxTransactionCount)
   : mMaxTransactionCount(aMaxTransactionCount)
   , mDoStack(nsTransactionStack::FOR_UNDO)
   , mUndoStack(nsTransactionStack::FOR_UNDO)
   , mRedoStack(nsTransactionStack::FOR_REDO)
@@ -364,36 +362,16 @@ nsTransactionManager::PeekRedoStack()
 {
   RefPtr<nsTransactionItem> tx = mRedoStack.Peek();
   if (!tx) {
     return nullptr;
   }
   return tx->GetTransaction();
 }
 
-NS_IMETHODIMP
-nsTransactionManager::GetUndoList(nsITransactionList **aTransactionList)
-{
-  NS_ENSURE_TRUE(aTransactionList, NS_ERROR_NULL_POINTER);
-
-  *aTransactionList = (nsITransactionList *)new nsTransactionList(this, &mUndoStack);
-  NS_IF_ADDREF(*aTransactionList);
-  return (! *aTransactionList) ? NS_ERROR_OUT_OF_MEMORY : NS_OK;
-}
-
-NS_IMETHODIMP
-nsTransactionManager::GetRedoList(nsITransactionList **aTransactionList)
-{
-  NS_ENSURE_TRUE(aTransactionList, NS_ERROR_NULL_POINTER);
-
-  *aTransactionList = (nsITransactionList *)new nsTransactionList(this, &mRedoStack);
-  NS_IF_ADDREF(*aTransactionList);
-  return (! *aTransactionList) ? NS_ERROR_OUT_OF_MEMORY : NS_OK;
-}
-
 nsresult
 nsTransactionManager::BatchTopUndo()
 {
   if (mUndoStack.GetSize() < 2) {
     // Not enough transactions to merge into one batch.
     return NS_OK;
   }
 
