# HG changeset patch
# User Scott Wu <scottcwwu@gmail.com>
# Date 1515490384 -28800
# Node ID 2d6ef6b74bfa2f2e1de2aeddc96a25bab6fd6000
# Parent  95e406224ae1c11efe11c7f1e9466812e1a64711
Bug 1427510 - Use current year as expiration year in credit card test case to fix perma failure. r=lchang

MozReview-Commit-ID: IuDCc6UDfay

diff --git a/browser/extensions/formautofill/test/browser/browser.ini b/browser/extensions/formautofill/test/browser/browser.ini
--- a/browser/extensions/formautofill/test/browser/browser.ini
+++ b/browser/extensions/formautofill/test/browser/browser.ini
@@ -9,16 +9,15 @@ support-files =
 [browser_autocomplete_footer.js]
 [browser_autocomplete_marked_back_forward.js]
 [browser_autocomplete_marked_detached_tab.js]
 [browser_check_installed.js]
 [browser_creditCard_doorhanger.js]
 [browser_dropdown_layout.js]
 [browser_editAddressDialog.js]
 [browser_editCreditCardDialog.js]
-skip-if = true # Bug 1427510
 [browser_first_time_use_doorhanger.js]
 [browser_insecure_form.js]
 [browser_manageAddressesDialog.js]
 [browser_manageCreditCardsDialog.js]
 [browser_privacyPreferences.js]
 [browser_submission_in_private_mode.js]
 [browser_update_doorhanger.js]
diff --git a/browser/extensions/formautofill/test/browser/browser_creditCard_doorhanger.js b/browser/extensions/formautofill/test/browser/browser_creditCard_doorhanger.js
--- a/browser/extensions/formautofill/test/browser/browser_creditCard_doorhanger.js
+++ b/browser/extensions/formautofill/test/browser/browser_creditCard_doorhanger.js
@@ -132,17 +132,17 @@ add_task(async function test_submit_chan
         let name = form.querySelector("#cc-name");
 
         name.focus();
         await new Promise(resolve => setTimeout(resolve, 1000));
         name.setUserInput("");
 
         form.querySelector("#cc-number").setUserInput("1234567812345678");
         form.querySelector("#cc-exp-month").setUserInput("4");
-        form.querySelector("#cc-exp-year").setUserInput("2017");
+        form.querySelector("#cc-exp-year").setUserInput(new Date().getFullYear());
         // Wait 1000ms before submission to make sure the input value applied
         await new Promise(resolve => setTimeout(resolve, 1000));
         form.querySelector("input[type=submit]").click();
       });
 
       await promiseShown;
       await clickDoorhangerButton(MAIN_BUTTON);
     }
@@ -170,17 +170,17 @@ add_task(async function test_submit_dupl
       await ContentTask.spawn(browser, null, async function() {
         let form = content.document.getElementById("form");
         let name = form.querySelector("#cc-name");
         name.focus();
 
         name.setUserInput("John Doe");
         form.querySelector("#cc-number").setUserInput("1234567812345678");
         form.querySelector("#cc-exp-month").setUserInput("4");
-        form.querySelector("#cc-exp-year").setUserInput("2017");
+        form.querySelector("#cc-exp-year").setUserInput(new Date().getFullYear());
 
         // Wait 1000ms before submission to make sure the input value applied
         await new Promise(resolve => setTimeout(resolve, 1000));
         form.querySelector("input[type=submit]").click();
       });
 
       await sleep(1000);
       is(PopupNotifications.panel.state, "closed", "Doorhanger is hidden");
@@ -207,31 +207,31 @@ add_task(async function test_submit_unno
         let form = content.document.getElementById("form");
         let name = form.querySelector("#cc-name");
         name.focus();
 
         name.setUserInput("John Doe");
         form.querySelector("#cc-number").setUserInput("1234567812345678");
         form.querySelector("#cc-exp-month").setUserInput("4");
         // Set unnormalized year
-        form.querySelector("#cc-exp-year").setUserInput("17");
+        form.querySelector("#cc-exp-year").setUserInput(new Date().getFullYear().toString().substr(2, 2));
 
         // Wait 1000ms before submission to make sure the input value applied
         await new Promise(resolve => setTimeout(resolve, 1000));
         form.querySelector("input[type=submit]").click();
       });
 
       await sleep(1000);
       is(PopupNotifications.panel.state, "closed", "Doorhanger is hidden");
     }
   );
 
   creditCards = await getCreditCards();
   is(creditCards.length, 1, "Still 1 credit card in storage");
-  is(creditCards[0]["cc-exp-year"], "2017", "Verify the expiry year field");
+  is(creditCards[0]["cc-exp-year"], new Date().getFullYear(), "Verify the expiry year field");
   await removeAllRecords();
 });
 
 add_task(async function test_submit_creditCard_never_save() {
   await SpecialPowers.pushPrefEnv({
     "set": [
       [CREDITCARDS_USED_STATUS_PREF, 0],
     ],
diff --git a/browser/extensions/formautofill/test/browser/browser_editCreditCardDialog.js b/browser/extensions/formautofill/test/browser/browser_editCreditCardDialog.js
--- a/browser/extensions/formautofill/test/browser/browser_editCreditCardDialog.js
+++ b/browser/extensions/formautofill/test/browser/browser_editCreditCardDialog.js
@@ -57,16 +57,51 @@ add_task(async function test_saveCreditC
     if (fieldName === "cc-number") {
       fieldValue = "*".repeat(fieldValue.length - 4) + fieldValue.substr(-4);
     }
     is(creditCards[0][fieldName], fieldValue, "check " + fieldName);
   }
   ok(creditCards[0]["cc-number-encrypted"], "cc-number-encrypted exists");
 });
 
+add_task(async function test_saveCreditCardWithMaxYear() {
+  await new Promise(resolve => {
+    let win = window.openDialog(EDIT_CREDIT_CARD_DIALOG_URL);
+    win.addEventListener("load", () => {
+      win.addEventListener("unload", () => {
+        ok(true, "Edit credit card dialog is closed");
+        resolve();
+      }, {once: true});
+      EventUtils.synthesizeKey("VK_TAB", {}, win);
+      EventUtils.synthesizeKey(TEST_CREDIT_CARD_2["cc-number"], {}, win);
+      EventUtils.synthesizeKey("VK_TAB", {}, win);
+      EventUtils.synthesizeKey(TEST_CREDIT_CARD_2["cc-name"], {}, win);
+      EventUtils.synthesizeKey("VK_TAB", {}, win);
+      EventUtils.synthesizeKey(TEST_CREDIT_CARD_2["cc-exp-month"].toString(), {}, win);
+      EventUtils.synthesizeKey("VK_TAB", {}, win);
+      EventUtils.synthesizeKey(TEST_CREDIT_CARD_2["cc-exp-year"].toString(), {}, win);
+      EventUtils.synthesizeKey("VK_TAB", {}, win);
+      EventUtils.synthesizeKey("VK_TAB", {}, win);
+      info("saving credit card");
+      EventUtils.synthesizeKey("VK_RETURN", {}, win);
+    }, {once: true});
+  });
+  let creditCards = await getCreditCards();
+
+  is(creditCards.length, 2, "Two credit card is in storage");
+  for (let [fieldName, fieldValue] of Object.entries(TEST_CREDIT_CARD_2)) {
+    if (fieldName === "cc-number") {
+      fieldValue = "*".repeat(fieldValue.length - 4) + fieldValue.substr(-4);
+    }
+    is(creditCards[1][fieldName], fieldValue, "check " + fieldName);
+  }
+  ok(creditCards[1]["cc-number-encrypted"], "cc-number-encrypted exists");
+  await removeCreditCards([creditCards[1].guid]);
+});
+
 add_task(async function test_editCreditCard() {
   let creditCards = await getCreditCards();
   is(creditCards.length, 1, "only one credit card is in storage");
   await new Promise(resolve => {
     let win = window.openDialog(EDIT_CREDIT_CARD_DIALOG_URL, null, null, creditCards[0]);
     win.addEventListener("FormReady", () => {
       win.addEventListener("unload", () => {
         ok(true, "Edit credit card dialog is closed");
@@ -87,17 +122,17 @@ add_task(async function test_editCreditC
 
   creditCards = await getCreditCards();
   is(creditCards.length, 0, "Credit card storage is empty");
 });
 
 add_task(async function test_addInvalidCreditCard() {
   await new Promise(resolve => {
     let win = window.openDialog(EDIT_CREDIT_CARD_DIALOG_URL);
-    win.addEventListener("FormReady", () => {
+    win.addEventListener("load", () => {
       const unloadHandler = () => ok(false, "Edit credit card dialog shouldn't be closed");
       win.addEventListener("unload", unloadHandler);
 
       EventUtils.synthesizeKey("VK_TAB", {}, win);
       EventUtils.synthesizeKey("test", {}, win);
       win.document.querySelector("#save").click();
 
       is(win.document.querySelector("form").checkValidity(), false, "cc-number is invalid");
diff --git a/browser/extensions/formautofill/test/browser/head.js b/browser/extensions/formautofill/test/browser/head.js
--- a/browser/extensions/formautofill/test/browser/head.js
+++ b/browser/extensions/formautofill/test/browser/head.js
@@ -95,24 +95,24 @@ const TEST_ADDRESS_DE_1 = {
   tel: "+4930983333000",
   email: "timbl@w3.org",
 };
 
 const TEST_CREDIT_CARD_1 = {
   "cc-name": "John Doe",
   "cc-number": "1234567812345678",
   "cc-exp-month": 4,
-  "cc-exp-year": 2017,
+  "cc-exp-year": new Date().getFullYear(),
 };
 
 const TEST_CREDIT_CARD_2 = {
   "cc-name": "Timothy Berners-Lee",
   "cc-number": "1111222233334444",
   "cc-exp-month": 12,
-  "cc-exp-year": 2022,
+  "cc-exp-year": new Date().getFullYear() + 10,
 };
 
 const TEST_CREDIT_CARD_3 = {
   "cc-number": "9999888877776666",
   "cc-exp-month": 1,
   "cc-exp-year": 2000,
 };
 
