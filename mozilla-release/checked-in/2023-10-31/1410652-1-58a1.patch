# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1508862746 -3600
# Node ID bf841cb4ea9af88c797275517935f5497b9e7e63
# Parent  4248b3b589233a7e6f8442faf04a3d65cb0218e8
Bug 1410652 - Fix API docs of assert functions. r=whimboo

MozReview-Commit-ID: HktgnHoCGZV

diff --git a/testing/marionette/assert.js b/testing/marionette/assert.js
--- a/testing/marionette/assert.js
+++ b/testing/marionette/assert.js
@@ -74,47 +74,47 @@ assert.firefox = function(msg = "") {
  *     If current browser is not Fennec.
  */
 assert.fennec = function(msg = "") {
   msg = msg || "Only supported in Fennec";
   assert.that(isFennec, msg, UnsupportedOperationError)();
 };
 
 /**
- * Asserts that the current |context| is content.
+ * Asserts that the current <var>context</var> is content.
  *
  * @param {string} context
  *     Context to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {string}
- *     |context| is returned unaltered.
+ *     <var>context</var> is returned unaltered.
  *
  * @throws {UnsupportedOperationError}
- *     If |context| is not content.
+ *     If <var>context</var> is not content.
  */
 assert.content = function(context, msg = "") {
   msg = msg || "Only supported in content context";
   assert.that(c => c.toString() == "content", msg, UnsupportedOperationError)(context);
 };
 
 /**
  * Asserts that |win| is open.
  *
  * @param {ChromeWindow} win
  *     Chrome window to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {ChromeWindow}
- *     |win| is returned unaltered.
+ *     <var>win</var> is returned unaltered.
  *
  * @throws {NoSuchWindowError}
- *     If |win| has been closed.
+ *     If <var>win</var> has been closed.
  */
 assert.window = function(win, msg = "") {
   msg = msg || "Unable to locate window";
   return assert.that(w => w && !w.closed,
       msg,
       NoSuchWindowError)(win);
 };
 
@@ -154,231 +154,232 @@ assert.contentBrowser = function(context
  */
 assert.noUserPrompt = function(dialog, msg = "") {
   assert.that(d => d === null || typeof d == "undefined",
       msg,
       UnexpectedAlertOpenError)(dialog);
 };
 
 /**
- * Asserts that |obj| is defined.
+ * Asserts that <var>obj</var> is defined.
  *
  * @param {?} obj
  *     Value to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {?}
- *     |obj| is returned unaltered.
+ *     <var>obj</var> is returned unaltered.
  *
  * @throws {InvalidArgumentError}
- *     If |obj| is not defined.
+ *     If <var>obj</var> is not defined.
  */
 assert.defined = function(obj, msg = "") {
   msg = msg || pprint`Expected ${obj} to be defined`;
   return assert.that(o => typeof o != "undefined", msg)(obj);
 };
 
 /**
- * Asserts that |obj| is a finite number.
+ * Asserts that <var>obj</var> is a finite number.
  *
  * @param {?} obj
  *     Value to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {number}
- *     |obj| is returned unaltered.
+ *     <var>obj</var> is returned unaltered.
  *
  * @throws {InvalidArgumentError}
- *     If |obj| is not a number.
+ *     If <var>obj</var> is not a number.
  */
 assert.number = function(obj, msg = "") {
   msg = msg || pprint`Expected ${obj} to be finite number`;
   return assert.that(Number.isFinite, msg)(obj);
 };
 
 /**
- * Asserts that |obj| is callable.
+ * Asserts that <var>obj</var> is callable.
  *
  * @param {?} obj
  *     Value to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {Function}
- *     |obj| is returned unaltered.
+ *     <var>obj</var> is returned unaltered.
  *
  * @throws {InvalidArgumentError}
- *     If |obj| is not callable.
+ *     If <var>obj</var> is not callable.
  */
 assert.callable = function(obj, msg = "") {
   msg = msg || pprint`${obj} is not callable`;
   return assert.that(o => typeof o == "function", msg)(obj);
 };
 
 /**
- * Asserts that |obj| is an integer.
+ * Asserts that <var>obj</var> is an integer.
  *
  * @param {?} obj
  *     Value to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {number}
- *     |obj| is returned unaltered.
+ *     <var>obj</var> is returned unaltered.
  *
  * @throws {InvalidArgumentError}
- *     If |obj| is not an integer.
+ *     If <var>obj</var> is not an integer.
  */
 assert.integer = function(obj, msg = "") {
   msg = msg || pprint`Expected ${obj} to be an integer`;
   return assert.that(Number.isInteger, msg)(obj);
 };
 
 /**
- * Asserts that |obj| is a positive integer.
+ * Asserts that <var>obj</var> is a positive integer.
  *
  * @param {?} obj
  *     Value to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {number}
- *     |obj| is returned unaltered.
+ *     <var>obj</var> is returned unaltered.
  *
  * @throws {InvalidArgumentError}
- *     If |obj| is not a positive integer.
+ *     If <var>obj</var> is not a positive integer.
  */
 assert.positiveInteger = function(obj, msg = "") {
   assert.integer(obj, msg);
   msg = msg || pprint`Expected ${obj} to be >= 0`;
   return assert.that(n => n >= 0, msg)(obj);
 };
 
 /**
- * Asserts that |obj| is a boolean.
+ * Asserts that <var>obj</var> is a boolean.
  *
  * @param {?} obj
  *     Value to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {boolean}
- *     |obj| is returned unaltered.
+ *     <var>obj</var> is returned unaltered.
  *
  * @throws {InvalidArgumentError}
- *     If |obj| is not a boolean.
+ *     If <var>obj</var> is not a boolean.
  */
 assert.boolean = function(obj, msg = "") {
   msg = msg || pprint`Expected ${obj} to be boolean`;
   return assert.that(b => typeof b == "boolean", msg)(obj);
 };
 
 /**
- * Asserts that |obj| is a string.
+ * Asserts that <var>obj</var> is a string.
  *
  * @param {?} obj
  *     Value to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {string}
- *     |obj| is returned unaltered.
+ *     <var>obj</var> is returned unaltered.
  *
  * @throws {InvalidArgumentError}
- *     If |obj| is not a string.
+ *     If <var>obj</var> is not a string.
  */
 assert.string = function(obj, msg = "") {
   msg = msg || pprint`Expected ${obj} to be a string`;
   return assert.that(s => typeof s == "string", msg)(obj);
 };
 
 /**
- * Asserts that |obj| is an object.
+ * Asserts that <var>obj</var> is an object.
  *
  * @param {?} obj
  *     Value to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {Object}
- *     |obj| is returned unaltered.
+ *     obj| is returned unaltered.
  *
  * @throws {InvalidArgumentError}
- *     If |obj| is not an object.
+ *     If <var>obj</var> is not an object.
  */
 assert.object = function(obj, msg = "") {
   msg = msg || pprint`Expected ${obj} to be an object`;
   return assert.that(o => {
     // unable to use instanceof because LHS and RHS may come from
     // different globals
     let s = Object.prototype.toString.call(o);
     return s == "[object Object]" || s == "[object nsJSIID]";
   }, msg)(obj);
 };
 
 /**
- * Asserts that |prop| is in |obj|.
+ * Asserts that <var>prop</var> is in <var>obj</var>.
  *
  * @param {?} prop
- *     Own property to test if is in |obj|.
+ *     Own property to test if is in <var>obj</var>.
  * @param {?} obj
  *     Object.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {?}
- *     Value of |obj|'s own property |prop|.
+ *     Value of <var>obj</var>'s own property <var>prop</var>.
  *
  * @throws {InvalidArgumentError}
- *     If |prop| is not in |obj|, or |obj| is not an object.
+ *     If <var>prop</var> is not in <var>obj</var>, or <var>obj</var>
+ *     is not an object.
  */
 assert.in = function(prop, obj, msg = "") {
   assert.object(obj, msg);
   msg = msg || pprint`Expected ${prop} in ${obj}`;
   assert.that(p => obj.hasOwnProperty(p), msg)(prop);
   return obj[prop];
 };
 
 /**
- * Asserts that |obj| is an Array.
+ * Asserts that <var>obj</var> is an Array.
  *
  * @param {?} obj
  *     Value to test.
  * @param {string=} msg
  *     Custom error message.
  *
  * @return {Object}
- *     |obj| is returned unaltered.
+ *     <var>obj</var> is returned unaltered.
  *
  * @throws {InvalidArgumentError}
- *     If |obj| is not an Array.
+ *     If <var>obj</var> is not an Array.
  */
 assert.array = function(obj, msg = "") {
   msg = msg || pprint`Expected ${obj} to be an Array`;
   return assert.that(Array.isArray, msg)(obj);
 };
 
 /**
  * Returns a function that is used to assert the |predicate|.
  *
  * @param {function(?): boolean} predicate
  *     Evaluated on calling the return value of this function.  If its
- *     return value of the inner function is false, |error| is thrown
- *     with |message|.
+ *     return value of the inner function is false, <var>error</var>
+ *     is thrown with <var>message</var>.
  * @param {string=} message
  *     Custom error message.
  * @param {Error=} error
  *     Custom error type by its class.
  *
  * @return {function(?): ?}
- *     Function that takes and returns the passed in value unaltered, and
- *     which may throw |error| with |message| if |predicate| evaluates
- *     to false.
+ *     Function that takes and returns the passed in value unaltered,
+ *     and which may throw <var>error</var> with <var>message</var>
+ *     if <var>predicate</var> evaluates to false.
  */
 assert.that = function(
     predicate, message = "", error = InvalidArgumentError) {
   return obj => {
     if (!predicate(obj)) {
       throw new error(message);
     }
     return obj;
