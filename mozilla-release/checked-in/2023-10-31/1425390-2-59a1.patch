# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1513323941 -32400
# Node ID 99f9646ec458b63fc3ff09def2b29a1ad7ce35b3
# Parent  d4eb28e720783b754eaf11ea4d857afe145dbe80
Bug 1425390 - part 2: Make the constructor of CompositionTransaction take TextComposition to reduce its arguments r=m_kato

Most arguments of the constructor of CompositionTransaction are now stored by
EditorBase::mComposition.  So, making it take TextComposition reduces the
number of its arguments.

Note that this patch doesn't make it retrieve TextComposition with
EditorBase::GetComposition() for guaranteeing that the editor has non-nullptr
mComposition.

MozReview-Commit-ID: 3O5wL52UBUy

diff --git a/editor/libeditor/CompositionTransaction.cpp b/editor/libeditor/CompositionTransaction.cpp
--- a/editor/libeditor/CompositionTransaction.cpp
+++ b/editor/libeditor/CompositionTransaction.cpp
@@ -2,41 +2,39 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "CompositionTransaction.h"
 
 #include "mozilla/EditorBase.h"         // mEditorBase
 #include "mozilla/SelectionState.h"     // RangeUpdater
+#include "mozilla/TextComposition.h"    // TextComposition
 #include "mozilla/dom/Selection.h"      // local var
 #include "mozilla/dom/Text.h"           // mTextNode
 #include "nsAString.h"                  // params
 #include "nsDebug.h"                    // for NS_ASSERTION, etc
 #include "nsError.h"                    // for NS_SUCCEEDED, NS_FAILED, etc
 #include "nsIPresShell.h"               // nsISelectionController constants
 #include "nsRange.h"                    // local var
 #include "nsQueryObject.h"              // for do_QueryObject
 
 namespace mozilla {
 
 using namespace dom;
 
 CompositionTransaction::CompositionTransaction(
-                          Text& aTextNode,
-                          uint32_t aOffset,
-                          uint32_t aReplaceLength,
-                          TextRangeArray* aTextRangeArray,
+                          EditorBase& aEditorBase,
                           const nsAString& aStringToInsert,
-                          EditorBase& aEditorBase,
+                          const TextComposition& aTextComposition,
                           RangeUpdater* aRangeUpdater)
-  : mTextNode(&aTextNode)
-  , mOffset(aOffset)
-  , mReplaceLength(aReplaceLength)
-  , mRanges(aTextRangeArray)
+  : mTextNode(aTextComposition.GetContainerTextNode())
+  , mOffset(aTextComposition.XPOffsetInTextNode())
+  , mReplaceLength(aTextComposition.XPLengthInTextNode())
+  , mRanges(aTextComposition.GetRanges())
   , mStringToInsert(aStringToInsert)
   , mEditorBase(&aEditorBase)
   , mRangeUpdater(aRangeUpdater)
   , mFixed(false)
 {
   MOZ_ASSERT(mTextNode->TextLength() >= mOffset);
 }
 
diff --git a/editor/libeditor/CompositionTransaction.h b/editor/libeditor/CompositionTransaction.h
--- a/editor/libeditor/CompositionTransaction.h
+++ b/editor/libeditor/CompositionTransaction.h
@@ -13,16 +13,17 @@
 #define NS_IMETEXTTXN_IID \
   { 0xb391355d, 0x346c, 0x43d1, \
     { 0x85, 0xed, 0x9e, 0x65, 0xbe, 0xe7, 0x7e, 0x48 } }
 
 namespace mozilla {
 
 class EditorBase;
 class RangeUpdater;
+class TextComposition;
 class TextRangeArray;
 
 namespace dom {
 class Text;
 } // namespace dom
 
 /**
  * CompositionTransaction stores all edit for a composition, i.e.,
@@ -31,31 +32,27 @@ class Text;
  * ranges and commit or cancel the composition.
  */
 class CompositionTransaction final : public EditTransactionBase
 {
 public:
   NS_DECLARE_STATIC_IID_ACCESSOR(NS_IMETEXTTXN_IID)
 
   /**
-   * @param aTextNode           The start node of text content.
-   * @param aOffset             The location in aTextNode to do the insertion.
-   * @param aReplaceLength      The length of text to replace. 0 means not
-   *                            replacing existing text.
-   * @param aTextRangeArray     Clauses and/or caret information. This may be
-   *                            null.
-   * @param aString             The new text to insert.
-   * @param aEditorBase         Used to get and set the selection.
-   * @param aRangeUpdater       The range updater
+   * @param aEditorBase         The editor which has composition.
+   * @param aStringToInsert     The new composition string to insert.  This may
+   *                            be different from actual composition string.
+   *                            E.g., password editor can hide the character
+   *                            with a different character.
+   * @param aTextComposition    The composition.
+   * @param aRangeUpdater       The range updater.
    */
-  CompositionTransaction(dom::Text& aTextNode,
-                         uint32_t aOffset, uint32_t aReplaceLength,
-                         TextRangeArray* aTextRangeArray,
-                         const nsAString& aString,
-                         EditorBase& aEditorBase,
+  CompositionTransaction(EditorBase& aEditorBase,
+                         const nsAString& aStringToInsert,
+                         const TextComposition& aTextComposition,
                          RangeUpdater* aRangeUpdater);
 
   NS_DECL_CYCLE_COLLECTION_CLASS_INHERITED(CompositionTransaction,
                                            EditTransactionBase)
 
   NS_DECL_ISUPPORTS_INHERITED
 
   NS_DECL_EDITTRANSACTIONBASE
diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -4686,22 +4686,17 @@ already_AddRefed<CompositionTransaction>
 EditorBase::CreateTxnForComposition(const nsAString& aStringToInsert)
 {
   MOZ_ASSERT(mComposition);
   MOZ_ASSERT(mComposition->GetContainerTextNode());
   // During handling IME composition, mComposition must have been initialized.
   // TODO: We can simplify CompositionTransaction::Init() with TextComposition
   //       class.
   RefPtr<CompositionTransaction> transaction =
-    new CompositionTransaction(*mComposition->GetContainerTextNode(),
-                               mComposition->XPOffsetInTextNode(),
-                               mComposition->XPLengthInTextNode(),
-                               mComposition->GetRanges(),
-                               aStringToInsert,
-                               *this,
+    new CompositionTransaction(*this, aStringToInsert, *mComposition,
                                &mRangeUpdater);
   return transaction.forget();
 }
 
 already_AddRefed<AddStyleSheetTransaction>
 EditorBase::CreateTxnForAddStyleSheet(StyleSheet* aSheet)
 {
   RefPtr<AddStyleSheetTransaction> transaction =
