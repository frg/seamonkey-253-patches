# HG changeset patch
# User Matt Woodrow <mwoodrow@mozilla.com>
# Date 1510955963 -46800
# Node ID 935f5a2f8b11185a0b87f7d5e4db598fd16779c6
# Parent  f297360846109b7789f3e86a70c77f77bb168906
Bug 1417529 - Part 2: Propagate invalidations with a 3d context up to the root, since we don't have valid overflow areas for except for the post-transform overflow on the 3d context root. r=miko

diff --git a/layout/painting/RetainedDisplayListBuilder.cpp b/layout/painting/RetainedDisplayListBuilder.cpp
--- a/layout/painting/RetainedDisplayListBuilder.cpp
+++ b/layout/painting/RetainedDisplayListBuilder.cpp
@@ -631,16 +631,31 @@ RetainedDisplayListBuilder::ComputeRebui
     // and then we use MarkFrameForDisplayIfVisible to make sure the stacking
     // context itself gets built. We don't need to build items that intersect outside
     // of the stacking context, since we know the stacking context item exists in
     // the old list, so we can trivially merge without needing other items.
     nsRect overflow = f->GetVisualOverflowRectRelativeToSelf();
     nsIFrame* currentFrame = f;
 
     while (currentFrame != mBuilder.RootReferenceFrame()) {
+
+      // Preserve-3d frames don't have valid overflow areas, and they might
+      // have singular transforms (despite still being visible when combined
+      // with their ancestors). If we're at one, jump up to the root of the
+      // preserve-3d context and use the whole overflow area.
+      nsIFrame* last = currentFrame;
+      while (currentFrame->Extend3DContext() ||
+             currentFrame->Combines3DTransformWithAncestors()) {
+        last = currentFrame;
+        currentFrame = currentFrame->GetParent();
+      }
+      if (last != currentFrame) {
+        overflow = last->GetVisualOverflowRectRelativeToParent();
+      }
+
       // Convert 'overflow' into the coordinate space of the nearest stacking context
       // or display port ancestor and update 'currentFrame' to point to that frame.
       overflow = nsLayoutUtils::TransformFrameRectToAncestor(currentFrame, overflow, mBuilder.RootReferenceFrame(),
                                                              nullptr, nullptr,
                                                              /* aStopAtStackingContextAndDisplayPort = */ true,
                                                              &currentFrame);
       MOZ_ASSERT(currentFrame);
 

