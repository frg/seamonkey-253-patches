# HG changeset patch
# User Amy Chung <amchung@mozilla.com>
# Date 1507012631 -28800
#      Tue Oct 03 14:37:11 2017 +0800
# Node ID 64c4a8859b6cdaf04a1be21800473f0775b4fd30
# Parent  e13c15b8e6a12b555c5a594d50b5b3ffd0c86a79
Bug 1399590 - Modify the argument of nsICookiePermission::CanAccess for changing nsIURI to nsIPrincipal. r=jdm

diff --git a/caps/moz.build b/caps/moz.build
--- a/caps/moz.build
+++ b/caps/moz.build
@@ -52,16 +52,17 @@ UNIFIED_SOURCES += [
     'SystemPrincipal.cpp',
 ]
 
 LOCAL_INCLUDES += [
     '/docshell/base',
     '/dom/base',
     '/js/xpconnect/src',
     '/netwerk/base',
+    '/netwerk/cookie',
 ]
 
 if CONFIG['ENABLE_TESTS']:
     DIRS += ['tests/gtest']
 
 include('/ipc/chromium/chromium-config.mozbuild')
 
 FINAL_LIBRARY = 'xul'
diff --git a/dom/base/Navigator.cpp b/dom/base/Navigator.cpp
--- a/dom/base/Navigator.cpp
+++ b/dom/base/Navigator.cpp
@@ -579,17 +579,17 @@ Navigator::CookieEnabled()
   }
 
   nsCOMPtr<nsICookiePermission> permMgr =
     do_GetService(NS_COOKIEPERMISSION_CONTRACTID);
   NS_ENSURE_TRUE(permMgr, cookieEnabled);
 
   // Pass null for the channel, just like the cookie service does.
   nsCookieAccess access;
-  nsresult rv = permMgr->CanAccess(codebaseURI, nullptr, &access);
+  nsresult rv = permMgr->CanAccess(doc->NodePrincipal(), &access);
   NS_ENSURE_SUCCESS(rv, cookieEnabled);
 
   if (access != nsICookiePermission::ACCESS_DEFAULT) {
     cookieEnabled = access != nsICookiePermission::ACCESS_DENY;
   }
 
   return cookieEnabled;
 }
diff --git a/dom/browser-element/mochitest/browserElement_CookiesNotThirdParty.js b/dom/browser-element/mochitest/browserElement_CookiesNotThirdParty.js
--- a/dom/browser-element/mochitest/browserElement_CookiesNotThirdParty.js
+++ b/dom/browser-element/mochitest/browserElement_CookiesNotThirdParty.js
@@ -42,11 +42,10 @@ function runTest() {
   // alert('failure:'), as appropriate.  Finally, the page will
   // alert('finish');
   iframe.src = innerPage;
   document.body.appendChild(iframe);
 }
 
 // Disable third-party cookies for this test.
 addEventListener('testready', function() {
-  SpecialPowers.pushPrefEnv({'set': [['network.cookie.cookieBehavior', 1],
-                                     ['network.cookie.ipc.sync', true]]}, runTest);
+  SpecialPowers.pushPrefEnv({'set': [['network.cookie.cookieBehavior', 1]]}, runTest);
 });
diff --git a/extensions/cookie/nsCookiePermission.cpp b/extensions/cookie/nsCookiePermission.cpp
--- a/extensions/cookie/nsCookiePermission.cpp
+++ b/extensions/cookie/nsCookiePermission.cpp
@@ -134,36 +134,37 @@ nsCookiePermission::SetAccess(nsIURI    
   //       the permission codes used by nsIPermissionManager.
   //       this is nice because it avoids conversion code.
   //
   return mPermMgr->Add(aURI, kPermissionType, aAccess,
                        nsIPermissionManager::EXPIRE_NEVER, 0);
 }
 
 NS_IMETHODIMP
-nsCookiePermission::CanAccess(nsIURI         *aURI,
-                              nsIChannel     *aChannel,
+nsCookiePermission::CanAccess(nsIPrincipal   *aPrincipal,
                               nsCookieAccess *aResult)
 {
   // Check this protocol doesn't allow cookies
   bool hasFlags;
+  nsCOMPtr<nsIURI> uri;
+  aPrincipal->GetURI(getter_AddRefs(uri));
   nsresult rv =
-    NS_URIChainHasFlags(aURI, nsIProtocolHandler::URI_FORBIDS_COOKIE_ACCESS,
+    NS_URIChainHasFlags(uri, nsIProtocolHandler::URI_FORBIDS_COOKIE_ACCESS,
                         &hasFlags);
   if (NS_FAILED(rv) || hasFlags) {
     *aResult = ACCESS_DENY;
     return NS_OK;
   }
 
   // Lazily initialize ourselves
   if (!EnsureInitialized())
     return NS_ERROR_UNEXPECTED;
 
   // finally, check with permission manager...
-  rv = mPermMgr->TestPermission(aURI, kPermissionType, (uint32_t *) aResult);
+  rv = mPermMgr->TestPermissionFromPrincipal(aPrincipal, kPermissionType, (uint32_t *) aResult);
   if (NS_SUCCEEDED(rv)) {
     if (*aResult == nsICookiePermission::ACCESS_SESSION) {
       *aResult = nsICookiePermission::ACCESS_ALLOW;
     }
   }
 
   return rv;
 }
diff --git a/netwerk/cookie/CookieServiceChild.cpp b/netwerk/cookie/CookieServiceChild.cpp
--- a/netwerk/cookie/CookieServiceChild.cpp
+++ b/netwerk/cookie/CookieServiceChild.cpp
@@ -229,17 +229,17 @@ CookieServiceChild::PrefChanged(nsIPrefB
   bool boolval;
   if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kPrefThirdPartySession, &boolval)))
     mThirdPartySession = !!boolval;
 
   if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kPrefCookieIPCSync, &boolval)))
     mIPCSync = !!boolval;
 
   if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kCookieLeaveSecurityAlone, &boolval)))
-   mLeaveSecureAlone = !!boolval;
+    mLeaveSecureAlone = !!boolval;
 
   if (!mThirdPartyUtil && RequireThirdPartyCheck()) {
     mThirdPartyUtil = do_GetService(THIRDPARTYUTIL_CONTRACTID);
     NS_ASSERTION(mThirdPartyUtil, "require ThirdPartyUtil service");
   }
 }
 
 void
@@ -271,17 +271,18 @@ CookieServiceChild::GetCookieStringFromC
   int64_t currentTimeInUsec = PR_Now();
   int64_t currentTime = currentTimeInUsec / PR_USEC_PER_SEC;
 
   nsCOMPtr<nsICookiePermission> permissionService = do_GetService(NS_COOKIEPERMISSION_CONTRACTID);
   CookieStatus cookieStatus =
     nsCookieService::CheckPrefs(permissionService, mCookieBehavior,
                                 mThirdPartySession, aHostURI,
                                 aIsForeign, nullptr,
-                                CountCookiesFromHashTable(baseDomain, aOriginAttrs));
+                                CountCookiesFromHashTable(baseDomain, aOriginAttrs),
+                                aOriginAttrs);
 
   if (cookieStatus != STATUS_ACCEPTED && cookieStatus != STATUS_ACCEPT_SESSION) {
     return;
   }
 
   cookiesList->Sort(CompareCookiesForSending());
   for (uint32_t i = 0; i < cookiesList->Length(); i++) {
     nsCookie *cookie = cookiesList->ElementAt(i);
@@ -516,17 +517,18 @@ CookieServiceChild::SetCookieStringInter
     GetBaseDomain(mTLDService, aHostURI, baseDomain, requireHostMatch);
 
   nsCOMPtr<nsICookiePermission> permissionService = do_GetService(NS_COOKIEPERMISSION_CONTRACTID);
 
   CookieStatus cookieStatus =
     nsCookieService::CheckPrefs(permissionService, mCookieBehavior,
                                 mThirdPartySession, aHostURI,
                                 !!isForeign, aCookieString,
-                                CountCookiesFromHashTable(baseDomain, attrs));
+                                CountCookiesFromHashTable(baseDomain, attrs),
+                                attrs);
 
   if (cookieStatus != STATUS_ACCEPTED && cookieStatus != STATUS_ACCEPT_SESSION) {
     return NS_OK;
   }
 
   nsCString serverTimeString(aServerTime);
   int64_t serverTime = nsCookieService::ParseServerTime(serverTimeString);
   bool moreCookies;
diff --git a/netwerk/cookie/nsCookieService.cpp b/netwerk/cookie/nsCookieService.cpp
--- a/netwerk/cookie/nsCookieService.cpp
+++ b/netwerk/cookie/nsCookieService.cpp
@@ -2116,17 +2116,17 @@ nsCookieService::SetCookieStringInternal
 
   // check default prefs
   uint32_t priorCookieCount = 0;
   nsAutoCString hostFromURI;
   aHostURI->GetHost(hostFromURI);
   CountCookiesFromHost(hostFromURI, &priorCookieCount);
   CookieStatus cookieStatus = CheckPrefs(mPermissionService, mCookieBehavior,
                                          mThirdPartySession, aHostURI, aIsForeign,
-                                         aCookieHeader.get(), priorCookieCount);
+                                         aCookieHeader.get(), priorCookieCount, aOriginAttrs);
 
   // fire a notification if third party or if cookie was rejected
   // (but not if there was an error)
   switch (cookieStatus) {
   case STATUS_REJECTED:
     NotifyRejected(aHostURI);
     if (aIsForeign) {
       NotifyThirdParty(aHostURI, false, aChannel);
@@ -3264,17 +3264,17 @@ nsCookieService::GetCookiesForURI(nsIURI
     return;
   }
 
   // check default prefs
   uint32_t priorCookieCount = 0;
   CountCookiesFromHost(hostFromURI, &priorCookieCount);
   CookieStatus cookieStatus = CheckPrefs(mPermissionService, mCookieBehavior,
                                          mThirdPartySession, aHostURI, aIsForeign,
-                                         nullptr, priorCookieCount);
+                                         nullptr, priorCookieCount, aOriginAttrs);
 
   // for GetCookie(), we don't fire rejection notifications.
   switch (cookieStatus) {
   case STATUS_REJECTED:
   case STATUS_REJECTED_WITH_ERROR:
     return;
   default:
     break;
@@ -4123,40 +4123,49 @@ static inline bool IsSubdomainOf(const n
   if (a == b)
     return true;
   if (a.Length() > b.Length())
     return a[a.Length() - b.Length() - 1] == '.' && StringEndsWith(a, b);
   return false;
 }
 
 CookieStatus
-nsCookieService::CheckPrefs(nsICookiePermission *aPermissionService,
-                            uint8_t              aCookieBehavior,
-                            bool                 aThirdPartySession,
-                            nsIURI              *aHostURI,
-                            bool                 aIsForeign,
-                            const char          *aCookieHeader,
-                            const int            aNumOfCookies)
+nsCookieService::CheckPrefs(nsICookiePermission    *aPermissionService,
+                            uint8_t                 aCookieBehavior,
+                            bool                    aThirdPartySession,
+                            nsIURI                 *aHostURI,
+                            bool                    aIsForeign,
+                            const char             *aCookieHeader,
+                            const int               aNumOfCookies,
+                            const OriginAttributes &aOriginAttrs)
 {
   nsresult rv;
 
   // don't let ftp sites get/set cookies (could be a security issue)
   bool ftp;
   if (NS_SUCCEEDED(aHostURI->SchemeIs("ftp", &ftp)) && ftp) {
     COOKIE_LOGFAILURE(aCookieHeader ? SET_COOKIE : GET_COOKIE, aHostURI, aCookieHeader, "ftp sites cannot read cookies");
     return STATUS_REJECTED_WITH_ERROR;
   }
 
+  nsCOMPtr<nsIPrincipal> principal =
+      BasePrincipal::CreateCodebasePrincipal(aHostURI, aOriginAttrs);
+
+  if (!principal) {
+    COOKIE_LOGFAILURE(aCookieHeader ? SET_COOKIE : GET_COOKIE, aHostURI, aCookieHeader, "non-codebase principals cannot get/set cookies");
+    return STATUS_REJECTED_WITH_ERROR;
+  }
+
   // check the permission list first; if we find an entry, it overrides
   // default prefs. see bug 184059.
   if (aPermissionService) {
     nsCookieAccess access;
     // Not passing an nsIChannel here is probably OK; our implementation
     // doesn't do anything with it anyway.
-    rv = aPermissionService->CanAccess(aHostURI, nullptr, &access);
+    rv = aPermissionService->CanAccess(principal, &access);
 
     // if we found an entry, use it
     if (NS_SUCCEEDED(rv)) {
       switch (access) {
       case nsICookiePermission::ACCESS_DENY:
         COOKIE_LOGFAILURE(aCookieHeader ? SET_COOKIE : GET_COOKIE, aHostURI,
                           aCookieHeader, "cookies are blocked for this site");
         return STATUS_REJECTED;
diff --git a/netwerk/cookie/nsCookieService.h b/netwerk/cookie/nsCookieService.h
--- a/netwerk/cookie/nsCookieService.h
+++ b/netwerk/cookie/nsCookieService.h
@@ -226,17 +226,17 @@ class nsCookieService final : public nsI
    */
   static void AppClearDataObserverInit();
   static nsCString GetPathFromURI(nsIURI *aHostURI);
   static nsresult GetBaseDomain(nsIEffectiveTLDService *aTLDService, nsIURI *aHostURI, nsCString &aBaseDomain, bool &aRequireHostMatch);
   static nsresult GetBaseDomainFromHost(nsIEffectiveTLDService *aTLDService, const nsACString &aHost, nsCString &aBaseDomain);
   static bool DomainMatches(nsCookie* aCookie, const nsACString& aHost);
   static bool PathMatches(nsCookie* aCookie, const nsACString& aPath);
   static bool CanSetCookie(nsIURI *aHostURI, const nsCookieKey& aKey, nsCookieAttributes &aCookieAttributes, bool aRequireHostMatch, CookieStatus aStatus, nsDependentCString &aCookieHeader, int64_t aServerTime, bool aFromHttp, nsIChannel* aChannel, bool aLeaveSercureAlone, bool &aSetCookie);
-  static CookieStatus CheckPrefs(nsICookiePermission *aPermissionServices, uint8_t aCookieBehavior, bool aThirdPartyUtil, nsIURI *aHostURI, bool aIsForeign, const char *aCookieHeader, const int aNumOfCookies);
+  static CookieStatus CheckPrefs(nsICookiePermission *aPermissionServices, uint8_t aCookieBehavior, bool aThirdPartyUtil, nsIURI *aHostURI, bool aIsForeign, const char *aCookieHeader, const int aNumOfCookies, const OriginAttributes& aOriginAttrs);
   static int64_t ParseServerTime(const nsCString &aServerTime);
   void GetCookiesForURI(nsIURI *aHostURI, bool aIsForeign, bool aHttpBound, const OriginAttributes& aOriginAttrs, nsTArray<nsCookie*>& aCookieList);
 
   protected:
     virtual ~nsCookieService();
 
     void                          PrefChanged(nsIPrefBranch *aPrefBranch);
     void                          InitDBStates();
diff --git a/netwerk/cookie/nsICookiePermission.idl b/netwerk/cookie/nsICookiePermission.idl
--- a/netwerk/cookie/nsICookiePermission.idl
+++ b/netwerk/cookie/nsICookiePermission.idl
@@ -2,16 +2,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsISupports.idl"
 
 interface nsICookie2;
 interface nsIURI;
 interface nsIChannel;
+interface nsIPrincipal;
 
 typedef long nsCookieAccess;
 
 /**
  * An interface to test for cookie permissions
  */
 [scriptable, uuid(11ddd4ed-8f5b-40b3-b2a0-27c20ea1c88d)]
 interface nsICookiePermission : nsISupports
@@ -49,27 +50,24 @@ interface nsICookiePermission : nsISuppo
                  in nsCookieAccess aAccess);
 
   /**
    * canAccess
    *
    * this method is called to test whether or not the given URI/channel may
    * access the cookie database, either to set or get cookies.
    *
-   * @param aURI
-   *        the URI trying to access cookies
-   * @param aChannel
-   *        the channel corresponding to aURI
+   * @param aPrincipal
+   *        the principal trying to access cookies.
    *
    * @return one of the following nsCookieAccess values:
    *         ACCESS_DEFAULT, ACCESS_ALLOW, ACCESS_DENY, or
    *         ACCESS_ALLOW_FIRST_PARTY_ONLY
    */
-  nsCookieAccess canAccess(in nsIURI     aURI,
-                           in nsIChannel aChannel);
+  nsCookieAccess canAccess(in nsIPrincipal aPrincipal);
 
   /**
    * canSetCookie
    *
    * this method is called to test whether or not the given URI/channel may
    * set a specific cookie.  this method is always preceded by a call to
    * canAccess. it may modify the isSession and expiry attributes of the
    * cookie via the aIsSession and aExpiry parameters, in order to limit
