# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1508775316 14400
#      Mon Oct 23 12:15:16 2017 -0400
# Node ID aaf86ab8bea034ee41a172b494752facbc88a396
# Parent  8da2e30bf2f39e7b62254908fb98738b1393ba6a
Bug 1402044 - don't inline InputEventStatistics::Get(); r=erahm

Defining Get() in the declaration of InputEventStatistics implicitly
sticks an "inline" on the function, which is not what we want: inlining
it spreads around a lot of static initialization code.  Providing an
out-of-line definition is much better in terms of code size.

diff --git a/xpcom/threads/InputEventStatistics.cpp b/xpcom/threads/InputEventStatistics.cpp
--- a/xpcom/threads/InputEventStatistics.cpp
+++ b/xpcom/threads/InputEventStatistics.cpp
@@ -5,16 +5,27 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "InputEventStatistics.h"
 
 #include "nsRefreshDriver.h"
 
 namespace mozilla {
 
+/*static*/ InputEventStatistics&
+InputEventStatistics::Get()
+{
+  static UniquePtr<InputEventStatistics> sInstance;
+  if (!sInstance) {
+    sInstance = MakeUnique<InputEventStatistics>(ConstructorCookie());
+    ClearOnShutdown(&sInstance);
+  }
+  return *sInstance;
+}
+
 TimeDuration
 InputEventStatistics::TimeDurationCircularBuffer::GetMean()
 {
   return mTotal / (int64_t)mSize;
 }
 
 InputEventStatistics::InputEventStatistics(ConstructorCookie&&)
   : mEnable(false)
diff --git a/xpcom/threads/InputEventStatistics.h b/xpcom/threads/InputEventStatistics.h
--- a/xpcom/threads/InputEventStatistics.h
+++ b/xpcom/threads/InputEventStatistics.h
@@ -74,25 +74,17 @@ class InputEventStatistics
   struct ConstructorCookie {};
 
 public:
   explicit InputEventStatistics(ConstructorCookie&&);
   ~InputEventStatistics()
   {
   }
 
-  static InputEventStatistics& Get()
-  {
-    static UniquePtr<InputEventStatistics> sInstance;
-    if (!sInstance) {
-      sInstance = MakeUnique<InputEventStatistics>(ConstructorCookie());
-      ClearOnShutdown(&sInstance);
-    }
-    return *sInstance;
-  }
+  static InputEventStatistics& Get();
 
   void UpdateInputDuration(TimeDuration aDuration)
   {
     if (!mEnable) {
       return;
     }
     mLastInputDurations->Insert(aDuration);
   }
