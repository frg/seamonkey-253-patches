# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1513641627 21600
# Node ID 31c635f8f249de38c1378ac0b88ea091c13134f5
# Parent  bbf88a2d847e069077b3b4e43ffda2da86b924a3
servo: Merge #19601 - style: Don't adjust :visited styles (from emilio:adjust-visited); r=upsuper

As the comment says those are not interesting, and it matches what we do for
text and placeholders in Servo_ComputedValues_Inherit.

This fixes https://bugzilla.mozilla.org/show_bug.cgi?id=1425893

Source-Repo: https://github.com/servo/servo
Source-Revision: d6797db10054dd42a7e96ce2ca8198838b068c81

diff --git a/servo/components/style/style_adjuster.rs b/servo/components/style/style_adjuster.rs
--- a/servo/components/style/style_adjuster.rs
+++ b/servo/components/style/style_adjuster.rs
@@ -552,16 +552,25 @@ impl<'a, 'b: 'a> StyleAdjuster<'a, 'b> {
     /// When comparing to Gecko, this is similar to the work done by
     /// `nsStyleContext::ApplyStyleFixups`, plus some parts of
     /// `nsStyleSet::GetContext`.
     pub fn adjust(
         &mut self,
         layout_parent_style: &ComputedValues,
         flags: CascadeFlags,
     ) {
+        // Don't adjust visited styles, visited-dependent properties aren't
+        // affected by these adjustments and it'd be just wasted work anyway.
+        //
+        // It also doesn't make much sense to adjust them, since we don't
+        // cascade most properties anyway, and they wouldn't be looked up.
+        if flags.contains(CascadeFlags::VISITED_DEPENDENT_ONLY) {
+            return;
+        }
+
         self.adjust_for_visited(flags);
         #[cfg(feature = "gecko")]
         {
             self.adjust_for_prohibited_display_contents(flags);
             self.adjust_for_fieldset_content(layout_parent_style, flags);
         }
         self.adjust_for_top_layer();
         self.blockify_if_necessary(layout_parent_style, flags);
