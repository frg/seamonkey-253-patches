# HG changeset patch
# User Ting-Yu Lin <tlin@mozilla.com>
# Date 1510739736 -28800
# Node ID 25dfc9b677cb24c57124ef0f1bb2ad47eda11771
# Parent  4a0a6d7874bd93194f7d4769f562c57eba3c124c
Bug 1417376 Part 3 - Pass nsPoint parameters by const references instead of values. r=mats

It's good to save some copy constructor calls.

MozReview-Commit-ID: 6TveqwkOvc0

diff --git a/layout/base/nsLayoutUtils.cpp b/layout/base/nsLayoutUtils.cpp
--- a/layout/base/nsLayoutUtils.cpp
+++ b/layout/base/nsLayoutUtils.cpp
@@ -6115,17 +6115,17 @@ nsLayoutUtils::DrawString(const nsIFrame
     aFontMetrics.SetTextRunRTL(false);
     DrawUniDirString(aString, aLength, aPoint, aFontMetrics, *aContext);
   }
 }
 
 void
 nsLayoutUtils::DrawUniDirString(const char16_t* aString,
                                 uint32_t aLength,
-                                nsPoint aPoint,
+                                const nsPoint& aPoint,
                                 nsFontMetrics& aFontMetrics,
                                 gfxContext& aContext)
 {
   nscoord x = aPoint.x;
   nscoord y = aPoint.y;
 
   uint32_t maxChunkLength = GetMaxChunkLength(aFontMetrics);
   if (aLength <= maxChunkLength) {
diff --git a/layout/base/nsLayoutUtils.h b/layout/base/nsLayoutUtils.h
--- a/layout/base/nsLayoutUtils.h
+++ b/layout/base/nsLayoutUtils.h
@@ -1641,17 +1641,17 @@ public:
                                            const nsPoint& aFill,
                                            const nsSize& aRepeatSize);
 
   /**
    * Supports only LTR or RTL. Bidi (mixed direction) is not supported.
    */
   static void DrawUniDirString(const char16_t* aString,
                                uint32_t aLength,
-                               nsPoint aPoint,
+                               const nsPoint& aPoint,
                                nsFontMetrics& aFontMetrics,
                                gfxContext& aContext);
 
   /**
    * Helper function for drawing text-shadow. The callback's job
    * is to draw whatever needs to be blurred onto the given context.
    */
   typedef void (* TextShadowCallback)(gfxContext* aCtx,
diff --git a/layout/generic/BRFrame.cpp b/layout/generic/BRFrame.cpp
--- a/layout/generic/BRFrame.cpp
+++ b/layout/generic/BRFrame.cpp
@@ -27,17 +27,17 @@ namespace mozilla {
 
 class BRFrame final : public nsFrame
 {
 public:
   NS_DECL_FRAMEARENA_HELPERS(BRFrame)
 
   friend nsIFrame* ::NS_NewBRFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 
-  virtual ContentOffsets CalcContentOffsetsFromFramePoint(nsPoint aPoint) override;
+  ContentOffsets CalcContentOffsetsFromFramePoint(const nsPoint& aPoint) override;
 
   virtual FrameSearchResult PeekOffsetNoAmount(bool aForward, int32_t* aOffset) override;
   virtual FrameSearchResult
   PeekOffsetCharacter(bool aForward, int32_t* aOffset,
                       PeekOffsetCharacterOptions aOptions =
                         PeekOffsetCharacterOptions()) override;
   virtual FrameSearchResult PeekOffsetWord(bool aForward, bool aWordSelectEatSpace,
                               bool aIsKeyboardSelect, int32_t* aOffset,
@@ -212,17 +212,17 @@ BRFrame::GetPrefISize(gfxContext *aRende
 }
 
 nscoord
 BRFrame::GetLogicalBaseline(mozilla::WritingMode aWritingMode) const
 {
   return mAscent;
 }
 
-nsIFrame::ContentOffsets BRFrame::CalcContentOffsetsFromFramePoint(nsPoint aPoint)
+nsIFrame::ContentOffsets BRFrame::CalcContentOffsetsFromFramePoint(const nsPoint& aPoint)
 {
   ContentOffsets offsets;
   offsets.content = mContent->GetParent();
   if (offsets.content) {
     offsets.offset = offsets.content->IndexOf(mContent);
     offsets.secondaryOffset = offsets.offset;
     offsets.associate = CARET_ASSOCIATE_AFTER;
   }
diff --git a/layout/generic/nsFrame.cpp b/layout/generic/nsFrame.cpp
--- a/layout/generic/nsFrame.cpp
+++ b/layout/generic/nsFrame.cpp
@@ -449,19 +449,18 @@ nsIFrame::IsVisibleConsideringAncestors(
       frame = parent;
     }
   }
 
   return true;
 }
 
 void
-nsIFrame::FindCloserFrameForSelection(
-                                 nsPoint aPoint,
-                                 nsIFrame::FrameWithDistance* aCurrentBestFrame)
+nsIFrame::FindCloserFrameForSelection(const nsPoint& aPoint,
+                                      FrameWithDistance* aCurrentBestFrame)
 {
   if (nsLayoutUtils::PointIsCloserToRect(aPoint, mRect,
                                          aCurrentBestFrame->mXDistance,
                                          aCurrentBestFrame->mYDistance)) {
     aCurrentBestFrame->mFrame = this;
   }
 }
 
@@ -4737,17 +4736,18 @@ struct FrameTarget {
   }
   nsIFrame* frame;
   bool frameEdge;
   bool afterFrame;
   bool emptyBlock;
 };
 
 // See function implementation for information
-static FrameTarget GetSelectionClosestFrame(nsIFrame* aFrame, nsPoint aPoint,
+static FrameTarget GetSelectionClosestFrame(nsIFrame* aFrame,
+                                            const nsPoint& aPoint,
                                             uint32_t aFlags);
 
 static bool SelfIsSelectable(nsIFrame* aFrame, uint32_t aFlags)
 {
   if ((aFlags & nsIFrame::SKIP_HIDDEN) &&
       !aFrame->StyleVisibility()->IsVisible()) {
     return false;
   }
@@ -4769,17 +4769,17 @@ static bool SelectionDescendToKids(nsIFr
   return !aFrame->IsGeneratedContentFrame() &&
          style != StyleUserSelect::All  &&
          style != StyleUserSelect::None &&
          ((parent->GetStateBits() & NS_FRAME_INDEPENDENT_SELECTION) ||
           !(aFrame->GetStateBits() & NS_FRAME_INDEPENDENT_SELECTION));
 }
 
 static FrameTarget GetSelectionClosestFrameForChild(nsIFrame* aChild,
-                                                    nsPoint aPoint,
+                                                    const nsPoint& aPoint,
                                                     uint32_t aFlags)
 {
   nsIFrame* parent = aChild->GetParent();
   if (SelectionDescendToKids(aChild)) {
     nsPoint pt = aPoint - aChild->GetOffsetTo(parent);
     return GetSelectionClosestFrame(aChild, pt, aFlags);
   }
   return FrameTarget(aChild, false, false);
@@ -4819,17 +4819,17 @@ static FrameTarget DrillDownToSelectionF
   return FrameTarget(aFrame, true, aEndFrame);
 }
 
 // This method finds the closest valid FrameTarget on a given line; if there is
 // no valid FrameTarget on the line, it returns a null FrameTarget
 static FrameTarget GetSelectionClosestFrameForLine(
                       nsBlockFrame* aParent,
                       nsBlockFrame::LineIterator aLine,
-                      nsPoint aPoint,
+                      const nsPoint& aPoint,
                       uint32_t aFlags)
 {
   nsIFrame *frame = aLine->mFirstChild;
   // Account for end of lines (any iterator from the block is valid)
   if (aLine == aParent->LinesEnd())
     return DrillDownToSelectionFrame(aParent, true, aFlags);
   nsIFrame *closestFromIStart = nullptr, *closestFromIEnd = nullptr;
   nscoord closestIStart = aLine->IStart(), closestIEnd = aLine->IEnd();
@@ -4877,17 +4877,17 @@ static FrameTarget GetSelectionClosestFr
 }
 
 // This method is for the special handling we do for block frames; they're
 // special because they represent paragraphs and because they are organized
 // into lines, which have bounds that are not stored elsewhere in the
 // frame tree.  Returns a null FrameTarget for frames which are not
 // blocks or blocks with no lines except editable one.
 static FrameTarget GetSelectionClosestFrameForBlock(nsIFrame* aFrame,
-                                                    nsPoint aPoint,
+                                                    const nsPoint& aPoint,
                                                     uint32_t aFlags)
 {
   nsBlockFrame* bf = nsLayoutUtils::GetAsBlock(aFrame); // used only for QI
   if (!bf)
     return FrameTarget::Null();
 
   // This code searches for the correct line
   nsBlockFrame::LineIterator firstLine = bf->LinesBegin();
@@ -4962,17 +4962,18 @@ static FrameTarget GetSelectionClosestFr
 
 // GetSelectionClosestFrame is the helper function that calculates the closest
 // frame to the given point.
 // It doesn't completely account for offset styles, so needs to be used in
 // restricted environments.
 // Cannot handle overlapping frames correctly, so it should receive the output
 // of GetFrameForPoint
 // Guaranteed to return a valid FrameTarget
-static FrameTarget GetSelectionClosestFrame(nsIFrame* aFrame, nsPoint aPoint,
+static FrameTarget GetSelectionClosestFrame(nsIFrame* aFrame,
+                                            const nsPoint& aPoint,
                                             uint32_t aFlags)
 {
   {
     // Handle blocks; if the frame isn't a block, the method fails
     FrameTarget target = GetSelectionClosestFrameForBlock(aFrame, aPoint, aFlags);
     if (!target.IsNull())
       return target;
   }
@@ -4992,17 +4993,18 @@ static FrameTarget GetSelectionClosestFr
       if (nsSVGUtils::IsInSVGTextSubtree(closest.mFrame))
         return FrameTarget(closest.mFrame, false, false);
       return GetSelectionClosestFrameForChild(closest.mFrame, aPoint, aFlags);
     }
   }
   return FrameTarget(aFrame, false, false);
 }
 
-nsIFrame::ContentOffsets OffsetsForSingleFrame(nsIFrame* aFrame, nsPoint aPoint)
+nsIFrame::ContentOffsets OffsetsForSingleFrame(nsIFrame* aFrame,
+                                               const nsPoint& aPoint)
 {
   nsIFrame::ContentOffsets offsets;
   FrameContentRange range = GetRangeForFrame(aFrame);
   offsets.content = range.content;
   // If there are continuations (meaning it's not one rectangle), this is the
   // best this function can do
   if (aFrame->GetNextContinuation() || aFrame->GetPrevContinuation()) {
     offsets.offset = range.start;
@@ -5051,17 +5053,17 @@ static nsIFrame* AdjustFrameForSelection
     if (userSelect == StyleUserSelect::All ||
         frame->IsGeneratedContentFrame()) {
       adjustedFrame = frame;
     }
   }
   return adjustedFrame;
 }
 
-nsIFrame::ContentOffsets nsIFrame::GetContentOffsetsFromPoint(nsPoint aPoint,
+nsIFrame::ContentOffsets nsIFrame::GetContentOffsetsFromPoint(const nsPoint& aPoint,
                                                               uint32_t aFlags)
 {
   nsIFrame *adjustedFrame;
   if (aFlags & IGNORE_SELECTION_STYLE) {
     adjustedFrame = this;
   }
   else {
     // This section of code deals with special selection styles.  Note that
@@ -5132,17 +5134,17 @@ nsIFrame::ContentOffsets nsIFrame::GetCo
   return static_cast<nsFrame*>(closest.frame)->CalcContentOffsetsFromFramePoint(pt);
 
   // XXX should I add some kind of offset standardization?
   // consider <b>xxxxx</b><i>zzzzz</i>; should any click between the last
   // x and first z put the cursor in the same logical position in addition
   // to the same visual position?
 }
 
-nsIFrame::ContentOffsets nsFrame::CalcContentOffsetsFromFramePoint(nsPoint aPoint)
+nsIFrame::ContentOffsets nsFrame::CalcContentOffsetsFromFramePoint(const nsPoint& aPoint)
 {
   return OffsetsForSingleFrame(this, aPoint);
 }
 
 void
 nsIFrame::AssociateImage(const nsStyleImage& aImage, nsPresContext* aPresContext)
 {
   if (aImage.GetType() != eStyleImageType_Image) {
diff --git a/layout/generic/nsFrame.h b/layout/generic/nsFrame.h
--- a/layout/generic/nsFrame.h
+++ b/layout/generic/nsFrame.h
@@ -436,17 +436,17 @@ public:
   nsresult SelectByTypeAtPoint(nsPresContext* aPresContext,
                                const nsPoint& aPoint,
                                nsSelectionAmount aBeginAmountType,
                                nsSelectionAmount aEndAmountType,
                                uint32_t aSelectFlags);
 
   // Helper for GetContentAndOffsetsFromPoint; calculation of content offsets
   // in this function assumes there is no child frame that can be targeted.
-  virtual ContentOffsets CalcContentOffsetsFromFramePoint(nsPoint aPoint);
+  virtual ContentOffsets CalcContentOffsetsFromFramePoint(const nsPoint& aPoint);
 
   // Box layout methods
   nsSize GetXULPrefSize(nsBoxLayoutState& aBoxLayoutState) override;
   nsSize GetXULMinSize(nsBoxLayoutState& aBoxLayoutState) override;
   nsSize GetXULMaxSize(nsBoxLayoutState& aBoxLayoutState) override;
   nscoord GetXULFlex() override;
   nscoord GetXULBoxAscent(nsBoxLayoutState& aBoxLayoutState) override;
 
diff --git a/layout/generic/nsFrameSelection.cpp b/layout/generic/nsFrameSelection.cpp
--- a/layout/generic/nsFrameSelection.cpp
+++ b/layout/generic/nsFrameSelection.cpp
@@ -1236,17 +1236,17 @@ nsFrameSelection::HandleClick(nsIContent
     return TakeFocus(aNewFocus, aContentOffset, aContentEndOffset, aHint,
                      aContinueSelection, aMultipleSelection);
   }
 
   return NS_OK;
 }
 
 void
-nsFrameSelection::HandleDrag(nsIFrame *aFrame, nsPoint aPoint)
+nsFrameSelection::HandleDrag(nsIFrame* aFrame, const nsPoint& aPoint)
 {
   if (!aFrame || !mShell)
     return;
 
   nsresult result;
   nsIFrame *newFrame = 0;
   nsPoint   newPoint;
 
@@ -1305,18 +1305,18 @@ nsFrameSelection::HandleDrag(nsIFrame *a
     }
   }
 
   HandleClick(offsets.content, offsets.offset, offsets.offset,
               true, false, offsets.associate);
 }
 
 nsresult
-nsFrameSelection::StartAutoScrollTimer(nsIFrame *aFrame,
-                                       nsPoint   aPoint,
+nsFrameSelection::StartAutoScrollTimer(nsIFrame* aFrame,
+                                       const nsPoint& aPoint,
                                        uint32_t  aDelay)
 {
   int8_t index = GetIndexFromSelectionType(SelectionType::eNormal);
   if (!mDomSelections[index]) {
     return NS_ERROR_NULL_POINTER;
   }
 
   return mDomSelections[index]->StartAutoScrollTimer(aFrame, aPoint, aDelay);
diff --git a/layout/generic/nsFrameSelection.h b/layout/generic/nsFrameSelection.h
--- a/layout/generic/nsFrameSelection.h
+++ b/layout/generic/nsFrameSelection.h
@@ -232,17 +232,17 @@ public:
                        CaretAssociateHint aHint);
 
   /** HandleDrag extends the selection to contain the frame closest to aPoint.
    *  @param aPresContext is the context to use when figuring out what frame contains the point.
    *  @param aFrame is the parent of all frames to use when searching for the closest frame to the point.
    *  @param aPoint is relative to aFrame
    */
   /*unsafe*/
-  void HandleDrag(nsIFrame *aFrame, nsPoint aPoint);
+  void HandleDrag(nsIFrame* aFrame, const nsPoint& aPoint);
 
   /** HandleTableSelection will set selection to a table, cell, etc
    *   depending on information contained in aFlags
    *  @param aParentContent is the paretent of either a table or cell that user clicked or dragged the mouse in
    *  @param aContentOffset is the offset of the table or cell
    *  @param aTarget indicates what to select (defined in nsISelectionPrivate.idl/nsISelectionPrivate.h):
    *    TABLESELECTION_CELL      We should select a cell (content points to the cell)
    *    TABLESELECTION_ROW       We should select a row (content points to any cell in row)
@@ -315,18 +315,18 @@ public:
    *  not scrolled to the end of the document.
    *  @param aFrame is the outermost frame to use when searching for
    *  the closest frame for the point, i.e. the frame that is capturing
    *  the mouse
    *  @param aPoint is relative to aFrame.
    *  @param aDelay is the timer's interval.
    */
   /*unsafe*/
-  nsresult StartAutoScrollTimer(nsIFrame *aFrame,
-                                nsPoint aPoint,
+  nsresult StartAutoScrollTimer(nsIFrame* aFrame,
+                                const nsPoint& aPoint,
                                 uint32_t aDelay);
 
   /** StopAutoScrollTimer stops any active auto scroll timer.
    */
   void StopAutoScrollTimer();
 
   /** Lookup Selection
    *  returns in frame coordinates the selection beginning and ending with the type of selection given
diff --git a/layout/generic/nsGfxScrollFrame.cpp b/layout/generic/nsGfxScrollFrame.cpp
--- a/layout/generic/nsGfxScrollFrame.cpp
+++ b/layout/generic/nsGfxScrollFrame.cpp
@@ -1337,17 +1337,17 @@ ScrollFrameHelper::WantAsyncScroll() con
   // The check for scroll bars was added in bug 825692 to prevent layerization
   // of text inputs for performance reasons.
   bool isVAsyncScrollable = isVScrollable && (mVScrollbarBox || canScrollWithoutScrollbars);
   bool isHAsyncScrollable = isHScrollable && (mHScrollbarBox || canScrollWithoutScrollbars);
   return isVAsyncScrollable || isHAsyncScrollable;
 }
 
 static nsRect
-GetOnePixelRangeAroundPoint(nsPoint aPoint, bool aIsHorizontal)
+GetOnePixelRangeAroundPoint(const nsPoint& aPoint, bool aIsHorizontal)
 {
   nsRect allowedRange(aPoint, nsSize());
   nscoord halfPixel = nsPresContext::CSSPixelsToAppUnits(0.5f);
   if (aIsHorizontal) {
     allowedRange.x = aPoint.x - halfPixel;
     allowedRange.width = halfPixel*2 - 1;
   } else {
     allowedRange.y = aPoint.y - halfPixel;
diff --git a/layout/generic/nsIFrame.h b/layout/generic/nsIFrame.h
--- a/layout/generic/nsIFrame.h
+++ b/layout/generic/nsIFrame.h
@@ -1986,20 +1986,20 @@ public:
   };
   /**
    * This function calculates the content offsets for selection relative to
    * a point.  Note that this should generally only be callled on the event
    * frame associated with an event because this function does not account
    * for frame lists other than the primary one.
    * @param aPoint point relative to this frame
    */
-  ContentOffsets GetContentOffsetsFromPoint(nsPoint aPoint,
+  ContentOffsets GetContentOffsetsFromPoint(const nsPoint& aPoint,
                                             uint32_t aFlags = 0);
 
-  virtual ContentOffsets GetContentOffsetsFromPointExternal(nsPoint aPoint,
+  virtual ContentOffsets GetContentOffsetsFromPointExternal(const nsPoint& aPoint,
                                                             uint32_t aFlags = 0)
   { return GetContentOffsetsFromPoint(aPoint, aFlags); }
 
   /**
    * Ensure that aImage gets notifed when the underlying image request loads
    * or animates.
    */
   void AssociateImage(const nsStyleImage& aImage, nsPresContext* aPresContext);
@@ -3926,17 +3926,17 @@ public:
    * aPoint and this frame's rectangle, and with a pointer to this frame.
    * If aPoint is not closer, then aCurrentBestFrame is left unchanged.
    *
    * @param aPoint The point to check for its distance to this frame.
    * @param aCurrentBestFrame Pointer to a struct that will be updated with
    *   a pointer to this frame and its distance to aPoint, if this frame
    *   is indeed closer than the current distance in aCurrentBestFrame.
    */
-  virtual void FindCloserFrameForSelection(nsPoint aPoint,
+  virtual void FindCloserFrameForSelection(const nsPoint& aPoint,
                                            FrameWithDistance* aCurrentBestFrame);
 
   /**
    * Is this a flex item? (i.e. a non-abs-pos child of a flex container)
    */
   inline bool IsFlexItem() const;
   /**
    * Is this a flex or grid item? (i.e. a non-abs-pos child of a flex/grid container)
diff --git a/layout/generic/nsTextFrame.cpp b/layout/generic/nsTextFrame.cpp
--- a/layout/generic/nsTextFrame.cpp
+++ b/layout/generic/nsTextFrame.cpp
@@ -7451,29 +7451,29 @@ CountCharsFit(const gfxTextRun* aTextRun
       width = nextWidth;
     }
   }
   *aFitWidth = width;
   return last;
 }
 
 nsIFrame::ContentOffsets
-nsTextFrame::CalcContentOffsetsFromFramePoint(nsPoint aPoint)
+nsTextFrame::CalcContentOffsetsFromFramePoint(const nsPoint& aPoint)
 {
   return GetCharacterOffsetAtFramePointInternal(aPoint, true);
 }
 
 nsIFrame::ContentOffsets
 nsTextFrame::GetCharacterOffsetAtFramePoint(const nsPoint &aPoint)
 {
   return GetCharacterOffsetAtFramePointInternal(aPoint, false);
 }
 
 nsIFrame::ContentOffsets
-nsTextFrame::GetCharacterOffsetAtFramePointInternal(nsPoint aPoint,
+nsTextFrame::GetCharacterOffsetAtFramePointInternal(const nsPoint& aPoint,
                                                     bool aForInsertionPoint)
 {
   ContentOffsets offsets;
 
   gfxSkipCharsIterator iter = EnsureTextRun(nsTextFrame::eInflated);
   if (!mTextRun)
     return offsets;
 
diff --git a/layout/generic/nsTextFrame.h b/layout/generic/nsTextFrame.h
--- a/layout/generic/nsTextFrame.h
+++ b/layout/generic/nsTextFrame.h
@@ -167,17 +167,17 @@ public:
 #ifdef DEBUG_FRAME_DUMP
   void List(FILE* out = stderr,
             const char* aPrefix = "",
             uint32_t aFlags = 0) const override;
   nsresult GetFrameName(nsAString& aResult) const override;
   void ToCString(nsCString& aBuf, int32_t* aTotalContentLength) const;
 #endif
 
-  ContentOffsets CalcContentOffsetsFromFramePoint(nsPoint aPoint) override;
+  ContentOffsets CalcContentOffsetsFromFramePoint(const nsPoint& aPoint) override;
   ContentOffsets GetCharacterOffsetAtFramePoint(const nsPoint& aPoint);
 
   /**
    * This is called only on the primary text frame. It indicates that
    * the selection state of the given character range has changed.
    * Text in the range is unconditionally invalidated
    * (Selection::Repaint depends on this).
    * @param aSelected true if the selection has been added to the range,
@@ -871,17 +871,17 @@ protected:
    * the specified selection type from the font metrics.
    */
   static gfxFloat ComputeSelectionUnderlineHeight(
     nsPresContext* aPresContext,
     const gfxFont::Metrics& aFontMetrics,
     SelectionType aSelectionType);
 
   ContentOffsets GetCharacterOffsetAtFramePointInternal(
-    nsPoint aPoint,
+    const nsPoint& aPoint,
     bool aForInsertionPoint);
 
   void ClearFrameOffsetCache();
 
   bool HasAnyNoncollapsedCharacters() override;
 
   void ClearMetrics(ReflowOutput& aMetrics);
 
diff --git a/layout/painting/DisplayItemClip.cpp b/layout/painting/DisplayItemClip.cpp
--- a/layout/painting/DisplayItemClip.cpp
+++ b/layout/painting/DisplayItemClip.cpp
@@ -424,17 +424,17 @@ DisplayItemClip::ComputeRegionInClips(Di
     aCombined->Or(*aCombined, NonRoundedIntersection());
   } else {
     *aCombined = NonRoundedIntersection();
   }
   return true;
 }
 
 void
-DisplayItemClip::MoveBy(nsPoint aPoint)
+DisplayItemClip::MoveBy(const nsPoint& aPoint)
 {
   if (!mHaveClipRect)
     return;
   mClipRect += aPoint;
   for (uint32_t i = 0; i < mRoundedClipRects.Length(); ++i) {
     mRoundedClipRects[i].mRect += aPoint;
   }
 }
diff --git a/layout/painting/DisplayItemClip.h b/layout/painting/DisplayItemClip.h
--- a/layout/painting/DisplayItemClip.h
+++ b/layout/painting/DisplayItemClip.h
@@ -163,17 +163,17 @@ public:
 
   bool HasClip() const { return mHaveClipRect; }
   const nsRect& GetClipRect() const
   {
     NS_ASSERTION(HasClip(), "No clip rect!");
     return mClipRect;
   }
 
-  void MoveBy(nsPoint aPoint);
+  void MoveBy(const nsPoint& aPoint);
 
   nsCString ToString() const;
 
   /**
    * Find the largest N such that the first N rounded rects in 'this' are
    * equal to the first N rounded rects in aOther, and N <= aMax.
    */
   uint32_t GetCommonRoundedRectCount(const DisplayItemClip& aOther,
diff --git a/layout/svg/SVGTextFrame.cpp b/layout/svg/SVGTextFrame.cpp
--- a/layout/svg/SVGTextFrame.cpp
+++ b/layout/svg/SVGTextFrame.cpp
@@ -3418,19 +3418,18 @@ SVGTextFrame::HandleAttributeChangeInDes
     if (aNameSpaceID == kNameSpaceID_None &&
         IsGlyphPositioningAttribute(aAttribute)) {
       NotifyGlyphMetricsChange();
     }
   }
 }
 
 void
-SVGTextFrame::FindCloserFrameForSelection(
-                                 nsPoint aPoint,
-                                 nsIFrame::FrameWithDistance* aCurrentBestFrame)
+SVGTextFrame::FindCloserFrameForSelection(const nsPoint& aPoint,
+                                          FrameWithDistance* aCurrentBestFrame)
 {
   if (GetStateBits() & NS_FRAME_IS_NONDISPLAY) {
     return;
   }
 
   UpdateGlyphPositioning();
 
   nsPresContext* presContext = PresContext();
diff --git a/layout/svg/SVGTextFrame.h b/layout/svg/SVGTextFrame.h
--- a/layout/svg/SVGTextFrame.h
+++ b/layout/svg/SVGTextFrame.h
@@ -238,18 +238,18 @@ public:
   }
 #endif
 
   virtual void DidSetStyleContext(nsStyleContext* aOldStyleContext) override;
 
   /**
    * Finds the nsTextFrame for the closest rendered run to the specified point.
    */
-  virtual void FindCloserFrameForSelection(nsPoint aPoint,
-                                          FrameWithDistance* aCurrentBestFrame) override;
+  virtual void FindCloserFrameForSelection(const nsPoint& aPoint,
+                                           FrameWithDistance* aCurrentBestFrame) override;
 
 
 
   // nsSVGDisplayableFrame interface:
   virtual void NotifySVGChanged(uint32_t aFlags) override;
   virtual void PaintSVG(gfxContext& aContext,
                         const gfxMatrix& aTransform,
                         imgDrawingParams& aImgParams,
