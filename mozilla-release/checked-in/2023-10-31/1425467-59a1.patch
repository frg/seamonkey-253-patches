# HG changeset patch
# User Makoto Kato <m_kato@ga2.so-net.ne.jp>
# Date 1513354494 21600
# Node ID bd452595f41ac9bca168aba57ebfd21beafba52b
# Parent  9ad7d468a396bf465169d2fd9d5e3afdbfd9cfa8
Bug 1425467 - mInlineEditedCell should be Element. r=masayuki

mInlineEditedCell for inline table editor is still nsIDOMElement.  To avoid QI
and virtual call, we should replace it with Element.

MozReview-Commit-ID: 76yfQJxwm9d

diff --git a/editor/libeditor/HTMLAnonymousNodeEditor.cpp b/editor/libeditor/HTMLAnonymousNodeEditor.cpp
--- a/editor/libeditor/HTMLAnonymousNodeEditor.cpp
+++ b/editor/libeditor/HTMLAnonymousNodeEditor.cpp
@@ -358,36 +358,32 @@ HTMLEditor::CheckSelectionStateForAnonym
   if (mIsAbsolutelyPositioningEnabled) {
     // Absolute Positioning support is enabled, is the selection contained
     // in an absolutely positioned element ?
     rv =
       GetAbsolutelyPositionedSelectionContainer(getter_AddRefs(absPosElement));
     NS_ENSURE_SUCCESS(rv, rv);
   }
 
-  nsCOMPtr<nsIDOMElement> cellElement;
+  RefPtr<Element> cellElement;
   if (mIsObjectResizingEnabled || mIsInlineTableEditingEnabled) {
     // Resizing or Inline Table Editing is enabled, we need to check if the
     // selection is contained in a table cell
-    rv = GetElementOrParentByTagName(NS_LITERAL_STRING("td"),
-                                     nullptr,
-                                     getter_AddRefs(cellElement));
-    NS_ENSURE_SUCCESS(rv, rv);
+    cellElement = GetElementOrParentByTagName(NS_LITERAL_STRING("td"), nullptr);
   }
 
   if (mIsObjectResizingEnabled && cellElement) {
     // we are here because Resizing is enabled AND selection is contained in
     // a cell
 
     // get the enclosing table
     if (nsGkAtoms::img != focusTagAtom) {
       // the element container of the selection is not an image, so we'll show
       // the resizers around the table
-      nsCOMPtr<nsIDOMNode> tableNode = GetEnclosingTable(cellElement);
-      focusElement = do_QueryInterface(tableNode);
+      focusElement = do_QueryInterface(GetEnclosingTable(cellElement));
       focusTagAtom = nsGkAtoms::table;
     }
   }
 
   // we allow resizers only around images, tables, and absolutely positioned
   // elements. If we don't have image/table, let's look at the latter case.
   if (nsGkAtoms::img != focusTagAtom && nsGkAtoms::table != focusTagAtom) {
     focusElement = absPosElement;
@@ -455,17 +451,17 @@ HTMLEditor::CheckSelectionStateForAnonym
       nsresult rv = ShowGrabberOnElement(absPosElement);
       if (NS_WARN_IF(NS_FAILED(rv))) {
         return rv;
       }
     }
   }
 
   if (mIsInlineTableEditingEnabled && cellElement &&
-      IsModifiableNode(cellElement) && cellElement != hostNode) {
+      IsModifiableNode(cellElement) && cellElement != hostContent) {
     if (mInlineEditedCell) {
       nsresult rv = RefreshInlineTableEditingUI();
       if (NS_WARN_IF(NS_FAILED(rv))) {
         return rv;
       }
     } else {
       nsresult rv = ShowInlineTableEditingUI(cellElement);
       if (NS_WARN_IF(NS_FAILED(rv))) {
diff --git a/editor/libeditor/HTMLEditor.h b/editor/libeditor/HTMLEditor.h
--- a/editor/libeditor/HTMLEditor.h
+++ b/editor/libeditor/HTMLEditor.h
@@ -584,16 +584,19 @@ protected:
                            int32_t& aNewColCount);
 
   /**
    * Fallback method: Call this after using ClearSelection() and you
    * failed to set selection to some other content in the document.
    */
   nsresult SetSelectionAtDocumentStart(Selection* aSelection);
 
+  nsresult GetTableSize(Element* aTable,
+                        int32_t* aRowCount, int32_t* aColCount);
+
   // End of Table Editing utilities
 
   static Element* GetEnclosingTable(nsINode* aNode);
   static nsIDOMNode* GetEnclosingTable(nsIDOMNode* aNode);
 
   /**
    * Content-based query returns true if <aProperty aAttribute=aValue> effects
    * aNode.  If <aProperty aAttribute=aValue> contains aNode, but
@@ -1093,26 +1096,28 @@ protected:
   void AddPositioningOffset(int32_t& aX, int32_t& aY);
   void SnapToGrid(int32_t& newX, int32_t& newY);
   nsresult GrabberClicked();
   nsresult EndMoving();
   nsresult CheckPositionedElementBGandFG(nsIDOMElement* aElement,
                                          nsAString& aReturn);
 
   // inline table editing
-  nsCOMPtr<nsIDOMElement> mInlineEditedCell;
+  RefPtr<Element> mInlineEditedCell;
 
   ManualNACPtr mAddColumnBeforeButton;
   ManualNACPtr mRemoveColumnButton;
   ManualNACPtr mAddColumnAfterButton;
 
   ManualNACPtr mAddRowBeforeButton;
   ManualNACPtr mRemoveRowButton;
   ManualNACPtr mAddRowAfterButton;
 
+  nsresult ShowInlineTableEditingUI(Element* aCell);
+
   void AddMouseClickListener(Element* aElement);
   void RemoveMouseClickListener(Element* aElement);
 
   nsCOMPtr<nsILinkHandler> mLinkHandler;
 
   ParagraphSeparator mDefaultParagraphSeparator;
 
 public:
diff --git a/editor/libeditor/HTMLInlineTableEditor.cpp b/editor/libeditor/HTMLInlineTableEditor.cpp
--- a/editor/libeditor/HTMLInlineTableEditor.cpp
+++ b/editor/libeditor/HTMLInlineTableEditor.cpp
@@ -40,25 +40,29 @@ HTMLEditor::GetInlineTableEditingEnabled
 {
   *aIsEnabled = mIsInlineTableEditingEnabled;
   return NS_OK;
 }
 
 NS_IMETHODIMP
 HTMLEditor::ShowInlineTableEditingUI(nsIDOMElement* aCell)
 {
-  NS_ENSURE_ARG_POINTER(aCell);
+  nsCOMPtr<Element> cell = do_QueryInterface(aCell);
+  return ShowInlineTableEditingUI(cell);
+}
 
+nsresult
+HTMLEditor::ShowInlineTableEditingUI(Element* aCell)
+{
   // do nothing if aCell is not a table cell...
-  nsCOMPtr<Element> cell = do_QueryInterface(aCell);
-  if (!cell || !HTMLEditUtils::IsTableCell(cell)) {
+  if (!aCell || !HTMLEditUtils::IsTableCell(aCell)) {
     return NS_OK;
   }
  
-  if (NS_WARN_IF(!IsDescendantOfEditorRoot(cell))) {
+  if (NS_WARN_IF(!IsDescendantOfEditorRoot(aCell))) {
     return NS_ERROR_UNEXPECTED;
   }
 
   if (mInlineEditedCell) {
     NS_ERROR("call HideInlineTableEditingUI first");
     return NS_ERROR_UNEXPECTED;
   }
 
@@ -136,24 +140,23 @@ HTMLEditor::DoInlineTableEditingAction(n
     nsAutoString anonclass;
     nsresult rv =
       aElement->GetAttribute(NS_LITERAL_STRING("_moz_anonclass"), anonclass);
     NS_ENSURE_SUCCESS(rv, rv);
 
     if (!StringBeginsWith(anonclass, NS_LITERAL_STRING("mozTable")))
       return NS_OK;
 
-    nsCOMPtr<nsIDOMNode> tableNode = GetEnclosingTable(mInlineEditedCell);
-    nsCOMPtr<nsIDOMElement> tableElement = do_QueryInterface(tableNode);
+    RefPtr<Element> tableElement = GetEnclosingTable(mInlineEditedCell);
     int32_t rowCount, colCount;
     rv = GetTableSize(tableElement, &rowCount, &colCount);
     NS_ENSURE_SUCCESS(rv, rv);
 
     bool hideUI = false;
-    bool hideResizersWithInlineTableUI = (GetAsDOMNode(mResizedObject) == tableElement);
+    bool hideResizersWithInlineTableUI = (mResizedObject == tableElement);
 
     if (anonclass.EqualsLiteral("mozTableAddColumnBefore"))
       InsertTableColumn(1, false);
     else if (anonclass.EqualsLiteral("mozTableAddColumnAfter"))
       InsertTableColumn(1, true);
     else if (anonclass.EqualsLiteral("mozTableAddRowBefore"))
       InsertTableRow(1, false);
     else if (anonclass.EqualsLiteral("mozTableAddRowAfter"))
@@ -206,38 +209,37 @@ HTMLEditor::RemoveMouseClickListener(Ele
     evtTarget->RemoveEventListener(NS_LITERAL_STRING("click"),
                                    mEventListener, true);
   }
 }
 
 NS_IMETHODIMP
 HTMLEditor::RefreshInlineTableEditingUI()
 {
+  if (!mInlineEditedCell) {
+   return NS_OK;
+  }
+
   nsCOMPtr<nsIDOMHTMLElement> htmlElement = do_QueryInterface(mInlineEditedCell);
   if (!htmlElement) {
     return NS_ERROR_NULL_POINTER;
   }
 
   int32_t xCell, yCell, wCell, hCell;
-  nsCOMPtr<Element> element = do_QueryInterface(mInlineEditedCell);
-  if (NS_WARN_IF(!element)) {
-   return NS_ERROR_FAILURE;
-  }
-  GetElementOrigin(*element, xCell, yCell);
+  GetElementOrigin(*mInlineEditedCell, xCell, yCell);
 
   nsresult rv = htmlElement->GetOffsetWidth(&wCell);
   NS_ENSURE_SUCCESS(rv, rv);
   rv = htmlElement->GetOffsetHeight(&hCell);
   NS_ENSURE_SUCCESS(rv, rv);
 
   int32_t xHoriz = xCell + wCell/2;
   int32_t yVert  = yCell + hCell/2;
 
-  nsCOMPtr<nsIDOMNode> tableNode = GetEnclosingTable(mInlineEditedCell);
-  nsCOMPtr<nsIDOMElement> tableElement = do_QueryInterface(tableNode);
+  RefPtr<Element> tableElement = GetEnclosingTable(mInlineEditedCell);
   int32_t rowCount, colCount;
   rv = GetTableSize(tableElement, &rowCount, &colCount);
   NS_ENSURE_SUCCESS(rv, rv);
 
   SetAnonymousElementPosition(xHoriz-10, yCell-7,  mAddColumnBeforeButton);
 #ifdef DISABLE_TABLE_DELETION
   if (colCount== 1) {
     mRemoveColumnButton->SetAttr(kNameSpaceID_None, nsGkAtoms::_class,
diff --git a/editor/libeditor/HTMLTableEditor.cpp b/editor/libeditor/HTMLTableEditor.cpp
--- a/editor/libeditor/HTMLTableEditor.cpp
+++ b/editor/libeditor/HTMLTableEditor.cpp
@@ -2650,28 +2650,35 @@ HTMLEditor::GetNumberOfCellsInRow(nsIDOM
   return cellCount;
 }
 
 NS_IMETHODIMP
 HTMLEditor::GetTableSize(nsIDOMElement* aTable,
                          int32_t* aRowCount,
                          int32_t* aColCount)
 {
+  nsCOMPtr<Element> table = do_QueryInterface(aTable);
+  return GetTableSize(table, aRowCount, aColCount);
+}
+
+nsresult
+HTMLEditor::GetTableSize(Element* aTable,
+                         int32_t* aRowCount,
+                         int32_t* aColCount)
+{
   NS_ENSURE_ARG_POINTER(aRowCount);
   NS_ENSURE_ARG_POINTER(aColCount);
   *aRowCount = 0;
   *aColCount = 0;
-  nsCOMPtr<nsIDOMElement> table;
   // Get the selected talbe or the table enclosing the selection anchor
-  nsresult rv = GetElementOrParentByTagName(NS_LITERAL_STRING("table"), aTable,
-                                            getter_AddRefs(table));
-  NS_ENSURE_SUCCESS(rv, rv);
+  RefPtr<Element> table =
+    GetElementOrParentByTagName(NS_LITERAL_STRING("table"), aTable);
   NS_ENSURE_TRUE(table, NS_ERROR_FAILURE);
 
-  nsTableWrapperFrame* tableFrame = GetTableFrame(table.get());
+  nsTableWrapperFrame* tableFrame = do_QueryFrame(table->GetPrimaryFrame());
   NS_ENSURE_TRUE(tableFrame, NS_ERROR_FAILURE);
 
   *aRowCount = tableFrame->GetRowCount();
   *aColCount = tableFrame->GetColCount();
 
   return NS_OK;
 }
 
