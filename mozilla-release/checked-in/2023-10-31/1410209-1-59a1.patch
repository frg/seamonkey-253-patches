# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1509050603 25200
# Node ID 7b1a39e7a0366115ec410ae28917fd03c3f60e23
# Parent  5c9702e5821ac7c055634f833b9f17a627a2c3bf
Bug 1410209, part 1 - Rename OSFile's ErrorEvent to avoid name conflicts. r=froydnj

ErrorEvent is also the name of a DOM event class, which causes an
assertion in the XPCOM leak checking system with the third patch.

MozReview-Commit-ID: 3xTHondZr9n

diff --git a/toolkit/components/osfile/NativeOSFileInternals.cpp b/toolkit/components/osfile/NativeOSFileInternals.cpp
--- a/toolkit/components/osfile/NativeOSFileInternals.cpp
+++ b/toolkit/components/osfile/NativeOSFileInternals.cpp
@@ -427,39 +427,39 @@ Int32Result::GetCacheableResult(JSContex
   return NS_OK;
 }
 
 //////// Callback events
 
 /**
  * An event used to notify asynchronously of an error.
  */
-class ErrorEvent final : public Runnable {
+class OSFileErrorEvent final : public Runnable {
 public:
   /**
    * @param aOnSuccess The success callback.
    * @param aOnError The error callback.
    * @param aDiscardedResult The discarded result.
    * @param aOperation The name of the operation, used for error reporting.
    * @param aOSError The OS error of the operation, as returned by errno/
    * GetLastError().
    *
    * Note that we pass both the success callback and the error
    * callback, as well as the discarded result to ensure that they are
    * all released on the main thread, rather than on the IO thread
    * (which would hopefully segfault). Also, we pass the callbacks as
    * alread_AddRefed to ensure that we do not manipulate main-thread
    * only refcounters off the main thread.
    */
-  ErrorEvent(nsMainThreadPtrHandle<nsINativeOSFileSuccessCallback>& aOnSuccess,
-             nsMainThreadPtrHandle<nsINativeOSFileErrorCallback>& aOnError,
-             already_AddRefed<AbstractResult>& aDiscardedResult,
-             const nsACString& aOperation,
-             int32_t aOSError)
-    : Runnable("ErrorEvent")
+  OSFileErrorEvent(nsMainThreadPtrHandle<nsINativeOSFileSuccessCallback>& aOnSuccess,
+                   nsMainThreadPtrHandle<nsINativeOSFileErrorCallback>& aOnError,
+                   already_AddRefed<AbstractResult>& aDiscardedResult,
+                   const nsACString& aOperation,
+                   int32_t aOSError)
+    : Runnable("OSFileErrorEvent")
     , mOnSuccess(aOnSuccess)
     , mOnError(aOnError)
     , mDiscardedResult(aDiscardedResult)
     , mOSError(aOSError)
     , mOperation(aOperation)
   {
     MOZ_ASSERT(!NS_IsMainThread());
     }
@@ -561,39 +561,40 @@ public:
 
   /**
    * Fail, asynchronously.
    */
   void Fail(const nsACString& aOperation,
             already_AddRefed<AbstractResult>&& aDiscardedResult,
             int32_t aOSError = 0) {
     Resolve();
-    RefPtr<ErrorEvent> event = new ErrorEvent(mOnSuccess,
-                                                mOnError,
-                                                aDiscardedResult,
-                                                aOperation,
-                                                aOSError);
+
+    RefPtr<OSFileErrorEvent> event = new OSFileErrorEvent(mOnSuccess,
+                                                          mOnError,
+                                                          aDiscardedResult,
+                                                          aOperation,
+                                                          aOSError);
     nsresult rv = NS_DispatchToMainThread(event);
     if (NS_FAILED(rv)) {
       // Last ditch attempt to release on the main thread - some of
       // the members of event are not thread-safe, so letting the
       // pointer go out of scope would cause a crash.
-      NS_ReleaseOnMainThreadSystemGroup("AbstractDoEvent::ErrorEvent",
+      NS_ReleaseOnMainThreadSystemGroup("AbstractDoEvent::OSFileErrorEvent",
                                         event.forget());
     }
   }
 
   /**
    * Succeed, asynchronously.
    */
   void Succeed(already_AddRefed<nsINativeOSFileResult>&& aResult) {
     Resolve();
     RefPtr<SuccessEvent> event = new SuccessEvent(mOnSuccess,
-                                                    mOnError,
-                                                    aResult);
+                                                  mOnError,
+                                                  aResult);
     nsresult rv = NS_DispatchToMainThread(event);
     if (NS_FAILED(rv)) {
       // Last ditch attempt to release on the main thread - some of
       // the members of event are not thread-safe, so letting the
       // pointer go out of scope would cause a crash.
       NS_ReleaseOnMainThreadSystemGroup("AbstractDoEvent::SuccessEvent",
                                         event.forget());
     }
