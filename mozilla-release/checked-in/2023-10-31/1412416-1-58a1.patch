# HG changeset patch
# User Eric Rahm <erahm@mozilla.com>
# Date 1509135828 25200
#      Fri Oct 27 13:23:48 2017 -0700
# Node ID f3aea617d92e1995803f07903f7f8d4d24323bce
# Parent  22c44d79d6c32692c8708ba377038f2515eda4e7
Bug 1412416 - Part 1: Support arena duplication of wide strings. r=froydnj

This adds support for duplicating both raw `char*` strings and raw `char16_t*`
strings by making the character type generic.

diff --git a/xpcom/ds/ArenaAllocatorExtensions.h b/xpcom/ds/ArenaAllocatorExtensions.h
--- a/xpcom/ds/ArenaAllocatorExtensions.h
+++ b/xpcom/ds/ArenaAllocatorExtensions.h
@@ -27,21 +27,21 @@ T* DuplicateString(const T* aSrc, const 
 /**
  * Makes an arena allocated null-terminated copy of the source string. The
  * source string must be null-terminated.
  *
  * @param aSrc String to copy.
  * @param aArena The arena to allocate the string copy out of.
  * @return An arena allocated null-terminated string.
  */
-template<size_t ArenaSize, size_t Alignment>
-char* ArenaStrdup(const char* aStr,
-                  ArenaAllocator<ArenaSize, Alignment>& aArena)
+template<typename T, size_t ArenaSize, size_t Alignment>
+T* ArenaStrdup(const T* aStr,
+               ArenaAllocator<ArenaSize, Alignment>& aArena)
 {
-  return detail::DuplicateString(aStr, strlen(aStr), aArena);
+  return detail::DuplicateString(aStr, nsCharTraits<T>::length(aStr), aArena);
 }
 
 /**
  * Makes an arena allocated null-terminated copy of the source string.
  *
  * @param aSrc String to copy.
  * @param aArena The arena to allocate the string copy out of.
  * @return An arena allocated null-terminated string.
diff --git a/xpcom/tests/gtest/TestArenaAllocator.cpp b/xpcom/tests/gtest/TestArenaAllocator.cpp
--- a/xpcom/tests/gtest/TestArenaAllocator.cpp
+++ b/xpcom/tests/gtest/TestArenaAllocator.cpp
@@ -279,20 +279,25 @@ TEST(ArenaAllocator, Clear)
   prev_sz = sz;
   sz = a.SizeOfExcludingThis(TestSizeOf);
   EXPECT_GT(sz, prev_sz);
 }
 
 TEST(ArenaAllocator, Extensions)
 {
   ArenaAllocator<4096, 8> a;
-  const char* const kTestStr = "This is a test string.";
-  char* dup = mozilla::ArenaStrdup(kTestStr, a);
-  EXPECT_STREQ(dup, kTestStr);
 
+  // Test with raw strings.
+  const char* const kTestCStr = "This is a test string.";
+  char* c_dup = mozilla::ArenaStrdup(kTestCStr, a);
+  EXPECT_STREQ(c_dup, kTestCStr);
+
+  const char16_t* const kTestStr = u"This is a wide test string.";
+  char16_t* dup = mozilla::ArenaStrdup(kTestStr, a);
+  EXPECT_TRUE(nsString(dup).Equals(kTestStr));
   NS_NAMED_LITERAL_STRING(wideStr, "A wide string.");
   nsLiteralString::char_type* wide = mozilla::ArenaStrdup(wideStr, a);
   EXPECT_TRUE(wideStr.Equals(wide));
 
   NS_NAMED_LITERAL_CSTRING(cStr, "A c-string.");
   nsLiteralCString::char_type* cstr = mozilla::ArenaStrdup(cStr, a);
   EXPECT_TRUE(cStr.Equals(cstr));
 }
