# HG changeset patch
# User Stone Shih <sshih@mozilla.com>
# Date 1505731296 -28800
# Node ID f763151e6c59fd0e18c69a918a36d2e6741d5b0e
# Parent  4492e50711e7819650cfc427f2dc3f5bd5eeedad
Bug 1400792 - Fire pointercancel when starting a dnd operation. r=smaug.

MozReview-Commit-ID: 4UTXpPHNqJ7

diff --git a/dom/events/EventStateManager.cpp b/dom/events/EventStateManager.cpp
--- a/dom/events/EventStateManager.cpp
+++ b/dom/events/EventStateManager.cpp
@@ -1731,16 +1731,63 @@ EventStateManager::FillInEventFromGestur
   // the old event, adjusted for the fact that the widget might be
   // different
   aEvent->mRefPoint =
     mGestureDownPoint - aEvent->mWidget->WidgetToScreenOffset();
   aEvent->mModifiers = mGestureModifiers;
   aEvent->buttons = mGestureDownButtons;
 }
 
+void
+EventStateManager::MaybeFirePointerCancel(WidgetInputEvent* aEvent)
+{
+  nsCOMPtr<nsIPresShell> shell = mPresContext->GetPresShell();
+  AutoWeakFrame targetFrame = mCurrentTarget;
+
+  if (!PointerEventHandler::IsPointerEventEnabled() || !shell ||
+      !targetFrame) {
+    return;
+  }
+
+  nsCOMPtr<nsIContent> content;
+  targetFrame->GetContentForEvent(aEvent, getter_AddRefs(content));
+  if (!content) {
+    return;
+  }
+
+  nsEventStatus status = nsEventStatus_eIgnore;
+
+  if (WidgetMouseEvent* aMouseEvent = aEvent->AsMouseEvent()) {
+    WidgetPointerEvent event(*aMouseEvent);
+    PointerEventHandler::InitPointerEventFromMouse(&event,
+                                                   aMouseEvent,
+                                                   ePointerCancel);
+
+    event.convertToPointer = false;
+    shell->HandleEventWithTarget(&event, targetFrame, content, &status);
+  } else if (WidgetTouchEvent* aTouchEvent = aEvent->AsTouchEvent()) {
+    WidgetPointerEvent event(aTouchEvent->IsTrusted(), ePointerCancel,
+                             aTouchEvent->mWidget);
+
+    PointerEventHandler::InitPointerEventFromTouch(&event,
+                                                   aTouchEvent,
+                                                   aTouchEvent->mTouches[0],
+                                                   true);
+
+    event.convertToPointer = false;
+    shell->HandleEventWithTarget(&event, targetFrame, content, &status);
+  } else {
+    MOZ_ASSERT(false);
+  }
+
+  // HandleEventWithTarget clears out mCurrentTarget, which may be used in the
+  // caller GenerateDragGesture. We have to restore mCurrentTarget.
+  mCurrentTarget = targetFrame;
+}
+
 //
 // GenerateDragGesture
 //
 // If we're in the TRACKING state of the d&d gesture tracker, check the current position
 // of the mouse in relation to the old one. If we've moved a sufficient amount from
 // the mouse down, then fire off a drag gesture event.
 void
 EventStateManager::GenerateDragGesture(nsPresContext* aPresContext,
@@ -1882,16 +1929,17 @@ EventStateManager::GenerateDragGesture(n
                                          nullptr);
       }
 
       if (status != nsEventStatus_eConsumeNoDefault) {
         bool dragStarted = DoDefaultDragStart(aPresContext, event, dataTransfer,
                                               targetContent, selection);
         if (dragStarted) {
           sActiveESM = nullptr;
+          MaybeFirePointerCancel(aEvent);
           aEvent->StopPropagation();
         }
       }
 
       // Reset mCurretTargetContent to what it was
       mCurrentTargetContent = targetBeforeEvent;
     }
 
@@ -3228,20 +3276,22 @@ EventStateManager::PostHandleEvent(nsPre
   case ePointerCancel:
   case ePointerUp: {
     WidgetPointerEvent* pointerEvent = aEvent->AsPointerEvent();
     MOZ_ASSERT(pointerEvent);
     // Implicitly releasing capture for given pointer. ePointerLostCapture
     // should be send after ePointerUp or ePointerCancel.
     PointerEventHandler::ImplicitlyReleasePointerCapture(pointerEvent);
 
-    if (pointerEvent->inputSource == nsIDOMMouseEvent::MOZ_SOURCE_TOUCH) {
-      // After UP/Cancel Touch pointers become invalid so we can remove relevant
-      // helper from Table Mouse/Pen pointers are valid all the time (not only
-      // between down/up)
+    if (pointerEvent->mMessage == ePointerCancel ||
+        pointerEvent->inputSource == nsIDOMMouseEvent::MOZ_SOURCE_TOUCH) {
+      // After pointercancel, pointer becomes invalid so we can remove relevant
+      // helper from table. Regarding pointerup with non-hoverable device, the
+      // pointer also becomes invalid. Hoverable (mouse/pen) pointers are valid
+      // all the time (not only between down/up).
       GenerateMouseEnterExit(pointerEvent);
       mPointersEnterLeaveHelper.Remove(pointerEvent->pointerId);
     }
     break;
   }
   case eMouseUp:
     {
       ClearGlobalActiveContent(this);
diff --git a/dom/events/EventStateManager.h b/dom/events/EventStateManager.h
--- a/dom/events/EventStateManager.h
+++ b/dom/events/EventStateManager.h
@@ -907,16 +907,22 @@ protected:
 
   friend class mozilla::dom::TabParent;
   void BeginTrackingRemoteDragGesture(nsIContent* aContent);
   void StopTrackingDragGesture();
   void GenerateDragGesture(nsPresContext* aPresContext,
                            WidgetInputEvent* aEvent);
 
   /**
+   * When starting a dnd session, UA must fire a pointercancel event and stop
+   * firing the subsequent pointer events.
+   */
+  void MaybeFirePointerCancel(WidgetInputEvent* aEvent);
+
+  /**
    * Determine which node the drag should be targeted at.
    * This is either the node clicked when there is a selection, or, for HTML,
    * the element with a draggable property set to true.
    *
    * aSelectionTarget - target to check for selection
    * aDataTransfer - data transfer object that will contain the data to drag
    * aSelection - [out] set to the selection to be dragged
    * aTargetNode - [out] the draggable node, or null if there isn't one
diff --git a/dom/events/PointerEventHandler.cpp b/dom/events/PointerEventHandler.cpp
--- a/dom/events/PointerEventHandler.cpp
+++ b/dom/events/PointerEventHandler.cpp
@@ -372,16 +372,71 @@ PointerEventHandler::PostHandlePointerEv
   if (!pointerInfo->mActiveState) {
     return;
   }
   aMouseOrTouchEvent->PreventDefault(false);
   pointerInfo->mPreventMouseEventByContent = true;
 }
 
 /* static */ void
+PointerEventHandler::InitPointerEventFromMouse(
+                       WidgetPointerEvent* aPointerEvent,
+                       WidgetMouseEvent* aMouseEvent,
+                       EventMessage aMessage)
+{
+  MOZ_ASSERT(aPointerEvent);
+  MOZ_ASSERT(aMouseEvent);
+  aPointerEvent->pointerId = aMouseEvent->pointerId;
+  aPointerEvent->inputSource = aMouseEvent->inputSource;
+  aPointerEvent->mMessage = aMessage;
+  aPointerEvent->button = aMouseEvent->mMessage == eMouseMove ?
+                            WidgetMouseEvent::eNoButton : aMouseEvent->button;
+
+  aPointerEvent->buttons = aMouseEvent->buttons;
+  aPointerEvent->pressure = aPointerEvent->buttons ?
+                              aMouseEvent->pressure ?
+                                aMouseEvent->pressure : 0.5f :
+                              0.0f;
+}
+
+/* static */ void
+PointerEventHandler::InitPointerEventFromTouch(
+                       WidgetPointerEvent* aPointerEvent,
+                       WidgetTouchEvent* aTouchEvent,
+                       mozilla::dom::Touch* aTouch,
+                       bool aIsPrimary)
+{
+  MOZ_ASSERT(aPointerEvent);
+  MOZ_ASSERT(aTouchEvent);
+
+  int16_t button = aTouchEvent->mMessage == eTouchMove ?
+                     WidgetMouseEvent::eNoButton :
+                     WidgetMouseEvent::eLeftButton;
+
+  int16_t buttons = aTouchEvent->mMessage == eTouchEnd ?
+                      WidgetMouseEvent::eNoButtonFlag :
+                      WidgetMouseEvent::eLeftButtonFlag;
+
+  aPointerEvent->mIsPrimary = aIsPrimary;
+  aPointerEvent->pointerId = aTouch->Identifier();
+  aPointerEvent->mRefPoint = aTouch->mRefPoint;
+  aPointerEvent->mModifiers = aTouchEvent->mModifiers;
+  aPointerEvent->mWidth = aTouch->RadiusX(CallerType::System);
+  aPointerEvent->mHeight = aTouch->RadiusY(CallerType::System);
+  aPointerEvent->tiltX = aTouch->tiltX;
+  aPointerEvent->tiltY = aTouch->tiltY;
+  aPointerEvent->mTime = aTouchEvent->mTime;
+  aPointerEvent->mTimeStamp = aTouchEvent->mTimeStamp;
+  aPointerEvent->mFlags = aTouchEvent->mFlags;
+  aPointerEvent->button = button;
+  aPointerEvent->buttons = buttons;
+  aPointerEvent->inputSource = nsIDOMMouseEvent::MOZ_SOURCE_TOUCH;
+}
+
+/* static */ void
 PointerEventHandler::DispatchPointerFromMouseOrTouch(
                        PresShell* aShell,
                        nsIFrame* aFrame,
                        WidgetGUIEvent* aEvent,
                        bool aDontRetargetEvents,
                        nsEventStatus* aStatus,
                        nsIContent** aTargetContent)
 {
@@ -413,44 +468,33 @@ PointerEventHandler::DispatchPointerFrom
         mouseEvent->buttons & ~nsContentUtils::GetButtonsFlagForButton(button) ?
         ePointerMove : ePointerDown;
       break;
     default:
       return;
     }
 
     WidgetPointerEvent event(*mouseEvent);
-    event.pointerId = mouseEvent->pointerId;
-    event.inputSource = mouseEvent->inputSource;
-    event.mMessage = pointerMessage;
-    event.button = button;
-    event.buttons = mouseEvent->buttons;
-    event.pressure = event.buttons ?
-                     mouseEvent->pressure ? mouseEvent->pressure : 0.5f :
-                     0.0f;
+    InitPointerEventFromMouse(&event, mouseEvent, pointerMessage);
     event.convertToPointer = mouseEvent->convertToPointer = false;
     PreHandlePointerEventsPreventDefault(&event, aEvent);
     RefPtr<PresShell> shell(aShell);
     shell->HandleEvent(aFrame, &event, aDontRetargetEvents, aStatus,
                        aTargetContent);
     PostHandlePointerEventsPreventDefault(&event, aEvent);
   } else if (aEvent->mClass == eTouchEventClass) {
     WidgetTouchEvent* touchEvent = aEvent->AsTouchEvent();
-    int16_t button = WidgetMouseEvent::eLeftButton;
-    int16_t buttons = WidgetMouseEvent::eLeftButtonFlag;
     // loop over all touches and dispatch pointer events on each touch
     // copy the event
     switch (touchEvent->mMessage) {
     case eTouchMove:
       pointerMessage = ePointerMove;
-      button = WidgetMouseEvent::eNoButton;
       break;
     case eTouchEnd:
       pointerMessage = ePointerUp;
-      buttons = WidgetMouseEvent::eNoButtonFlag;
       break;
     case eTouchStart:
       pointerMessage = ePointerDown;
       break;
     case eTouchCancel:
     case eTouchPointerCancel:
       pointerMessage = ePointerCancel;
       break;
@@ -462,30 +506,18 @@ PointerEventHandler::DispatchPointerFrom
     for (uint32_t i = 0; i < touchEvent->mTouches.Length(); ++i) {
       Touch* touch = touchEvent->mTouches[i];
       if (!TouchManager::ShouldConvertTouchToPointer(touch, touchEvent)) {
         continue;
       }
 
       WidgetPointerEvent event(touchEvent->IsTrusted(), pointerMessage,
                                touchEvent->mWidget);
-      event.mIsPrimary = i == 0;
-      event.pointerId = touch->Identifier();
-      event.mRefPoint = touch->mRefPoint;
-      event.mModifiers = touchEvent->mModifiers;
-      event.mWidth = touch->RadiusX(CallerType::System);
-      event.mHeight = touch->RadiusY(CallerType::System);
-      event.tiltX = touch->tiltX;
-      event.tiltY = touch->tiltY;
-      event.mTime = touchEvent->mTime;
-      event.mTimeStamp = touchEvent->mTimeStamp;
-      event.mFlags = touchEvent->mFlags;
-      event.button = button;
-      event.buttons = buttons;
-      event.inputSource = nsIDOMMouseEvent::MOZ_SOURCE_TOUCH;
+
+      InitPointerEventFromTouch(&event, touchEvent, touch, i == 0);
       event.convertToPointer = touch->convertToPointer = false;
       PreHandlePointerEventsPreventDefault(&event, aEvent);
       shell->HandleEvent(aFrame, &event, aDontRetargetEvents, aStatus,
                          aTargetContent);
       PostHandlePointerEventsPreventDefault(&event, aEvent);
     }
   }
 }
diff --git a/dom/events/PointerEventHandler.h b/dom/events/PointerEventHandler.h
--- a/dom/events/PointerEventHandler.h
+++ b/dom/events/PointerEventHandler.h
@@ -138,16 +138,25 @@ public:
 
   static void DispatchPointerFromMouseOrTouch(PresShell* aShell,
                                               nsIFrame* aFrame,
                                               WidgetGUIEvent* aEvent,
                                               bool aDontRetargetEvents,
                                               nsEventStatus* aStatus,
                                               nsIContent** aTargetContent);
 
+  static void InitPointerEventFromMouse(WidgetPointerEvent* aPointerEvent,
+                                        WidgetMouseEvent* aMouseEvent,
+                                        EventMessage aMessage);
+
+  static void InitPointerEventFromTouch(WidgetPointerEvent* aPointerEvent,
+                                        WidgetTouchEvent* aTouchEvent,
+                                        mozilla::dom::Touch* aTouch,
+                                        bool aIsPrimary);
+
 private:
   // GetPointerType returns pointer type like mouse, pen or touch for pointer
   // event with pointerId. The return value must be one of
   // nsIDOMMouseEvent::MOZ_SOURCE_*
   static uint16_t GetPointerType(uint32_t aPointerId);
 
   // GetPointerPrimaryState returns state of attribute isPrimary for pointer
   // event with pointerId
