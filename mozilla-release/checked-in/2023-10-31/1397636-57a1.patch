# HG changeset patch
# User steveck-chung <schung@mozilla.com>
# Date 1504780647 -28800
# Node ID 45a470fd009745f4048e3ed39bc698b9f0784031
# Parent  04251662d53c3730ae89e690c73d2466e6cad337
Bug 1397636 - Collect information on how much time users spent on page with credit card forms. r=MattN

MozReview-Commit-ID: KlraQkAd0mW

diff --git a/browser/extensions/formautofill/FormAutofillHandler.jsm b/browser/extensions/formautofill/FormAutofillHandler.jsm
--- a/browser/extensions/formautofill/FormAutofillHandler.jsm
+++ b/browser/extensions/formautofill/FormAutofillHandler.jsm
@@ -777,16 +777,22 @@ FormAutofillHandler.prototype = {
       delete data.address;
     }
 
     if (data.creditCard && !this._isCreditCardRecordCreatable(data.creditCard.record)) {
       log.debug("No credit card record saving since card number is invalid");
       delete data.creditCard;
     }
 
+    // If both address and credit card exists, skip this metrics because it not a
+    // general case and each specific histogram might contains insufficient data set.
+    if (data.address && data.creditCard) {
+      this.timeStartedFillingMS = null;
+    }
+
     return data;
   },
 
   _normalizeAddress(address) {
     if (!address) {
       return;
     }
 
diff --git a/browser/extensions/formautofill/FormAutofillParent.jsm b/browser/extensions/formautofill/FormAutofillParent.jsm
--- a/browser/extensions/formautofill/FormAutofillParent.jsm
+++ b/browser/extensions/formautofill/FormAutofillParent.jsm
@@ -422,31 +422,36 @@ FormAutofillParent.prototype = {
         });
       } else {
         // We want to exclude the first time form filling.
         Services.telemetry.scalarAdd("formautofill.addresses.fill_type_manual", 1);
       }
     }
   },
 
-  async _onCreditCardSubmit(creditCard, target) {
+  async _onCreditCardSubmit(creditCard, target, timeStartedFillingMS) {
     // We'll show the credit card doorhanger if:
     //   - User applys autofill and changed
     //   - User fills form manually
     if (creditCard.guid &&
         Object.keys(creditCard.record).every(key => creditCard.untouchedFields.includes(key))) {
       // Add probe to record credit card autofill(without modification).
       Services.telemetry.scalarAdd("formautofill.creditCards.fill_type_autofill", 1);
+      this._recordFormFillingTime("creditCard", "autofill", timeStartedFillingMS);
       return;
     }
 
     // Add the probe to record credit card manual filling or autofill but modified case.
-    let ccScalar = creditCard.guid ? "formautofill.creditCards.fill_type_autofill_modified" :
-                                     "formautofill.creditCards.fill_type_manual";
-    Services.telemetry.scalarAdd(ccScalar, 1);
+    if (creditCard.guid) {
+      Services.telemetry.scalarAdd("formautofill.creditCards.fill_type_autofill_modified", 1);
+      this._recordFormFillingTime("creditCard", "autofill-update", timeStartedFillingMS);
+    } else {
+      Services.telemetry.scalarAdd("formautofill.creditCards.fill_type_manual", 1);
+      this._recordFormFillingTime("creditCard", "manual", timeStartedFillingMS);
+    }
 
     let state = await FormAutofillDoorhanger.show(target, "creditCard");
     if (state == "cancel") {
       return;
     }
 
     if (state == "disable") {
       Services.prefs.setBoolPref("extensions.formautofill.creditCards.enabled", false);
@@ -466,27 +471,30 @@ FormAutofillParent.prototype = {
 
   _onFormSubmit(data, target) {
     let {profile: {address, creditCard}, timeStartedFillingMS} = data;
 
     if (address) {
       this._onAddressSubmit(address, target, timeStartedFillingMS);
     }
     if (creditCard) {
-      this._onCreditCardSubmit(creditCard, target);
+      this._onCreditCardSubmit(creditCard, target, timeStartedFillingMS);
     }
   },
   /**
    * Set the probes for the filling time with specific filling type and form type.
    *
    * @private
    * @param  {string} formType
    *         3 type of form (address/creditcard/address-creditcard).
    * @param  {string} fillingType
    *         3 filling type (manual/autofill/autofill-update).
-   * @param  {int} startedFillingMS
-   *         Time that form started to filling in ms.
+   * @param  {int|null} startedFillingMS
+   *         Time that form started to filling in ms. Early return if start time is null.
    */
   _recordFormFillingTime(formType, fillingType, startedFillingMS) {
+    if (!startedFillingMS) {
+      return;
+    }
     let histogram = Services.telemetry.getKeyedHistogramById("FORM_FILLING_REQUIRED_TIME_MS");
     histogram.add(`${formType}-${fillingType}`, Date.now() - startedFillingMS);
   },
 };
