# HG changeset patch
# User Hiroyuki Ikezoe <hikezoe@mozilla.com>
# Date 1511153786 -32400
# Node ID 3ccf0877126db5ca2a2c29a9715be2963af04be2
# Parent  b7375395abda6842e394788c6b838e87014e87d4
Bug 1418059 - Stop eagerly CSS animations on the root of display:none subtree. r=birtles

Otherwise we do update keyframes data unnecessarily.

MozReview-Commit-ID: ys4BEF1kxX

diff --git a/layout/style/crashtests/1418059.html b/layout/style/crashtests/1418059.html
new file mode 100644
--- /dev/null
+++ b/layout/style/crashtests/1418059.html
@@ -0,0 +1,24 @@
+<!DOCTYPE html>
+<html class="reftest-wait">
+<style>
+@keyframes anim {
+  to { transform: rotate(360deg); }
+}
+.document-ready .animation::after {
+  display: none;
+}
+.animation::after {
+  content: "";
+  animation: anim 1s infinite;
+}
+</style>
+<div class="animation"></div>
+<script>
+window.addEventListener('load', () => {
+  document.documentElement.classList.add('document-ready');
+  requestAnimationFrame(() => {
+    document.documentElement.classList.remove('reftest-wait');
+  });
+});
+</script>
+</html>
diff --git a/layout/style/crashtests/crashtests.list b/layout/style/crashtests/crashtests.list
--- a/layout/style/crashtests/crashtests.list
+++ b/layout/style/crashtests/crashtests.list
@@ -258,9 +258,10 @@ load 1410226-1.html
 load 1410226-2.html
 load 1411143.html
 load 1411478.html
 load 1413288.html
 load 1413361.html
 load 1415663.html
 pref(dom.webcomponents.enabled,true) load 1415353.html
 load 1415021.html # This should have dom.webcomponents.enabled=true, but it leaks the world, see bug 1416296.
+load 1418059.html
 pref(layout.css.resizeobserver.enabled,true) load 1552911.html
diff --git a/layout/style/nsAnimationManager.cpp b/layout/style/nsAnimationManager.cpp
--- a/layout/style/nsAnimationManager.cpp
+++ b/layout/style/nsAnimationManager.cpp
@@ -1063,29 +1063,34 @@ nsAnimationManager::UpdateAnimations(
   const ServoStyleContext* aStyleContext)
 {
   MOZ_ASSERT(mPresContext->IsDynamic(),
              "Should not update animations for print or print preview");
   MOZ_ASSERT(aElement->IsInComposedDoc(),
              "Should not update animations that are not attached to the "
              "document tree");
 
-  if (!aStyleContext) {
+  const nsStyleDisplay* disp = aStyleContext
+    ? aStyleContext->ComputedData()->GetStyleDisplay()
+    : nullptr;
+
+  if (!disp || disp->mDisplay == StyleDisplay::None) {
     // If we are in a display:none subtree we will have no computed values.
-    // Since CSS animations should not run in display:none subtrees we should
-    // stop (actually, destroy) any animations on this element here.
+    // However, if we are on the root of display:none subtree, the computed
+    // values might not have been cleared yet.
+    // In either case, since CSS animations should not run in display:none
+    // subtrees we should stop (actually, destroy) any animations on this
+    // element here.
     StopAnimationsForElement(aElement, aPseudoType);
     return;
   }
 
   NonOwningAnimationTarget target(aElement, aPseudoType);
   ServoCSSAnimationBuilder builder(aStyleContext);
 
-  const nsStyleDisplay *disp =
-      aStyleContext->ComputedData()->GetStyleDisplay();
   DoUpdateAnimations(target, *disp, builder);
 }
 
 template<class BuilderType>
 void
 nsAnimationManager::DoUpdateAnimations(
   const NonOwningAnimationTarget& aTarget,
   const nsStyleDisplay& aStyleDisplay,
