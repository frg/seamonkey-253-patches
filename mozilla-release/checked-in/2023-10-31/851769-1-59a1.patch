# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1513309053 21600
# Node ID 9b216b26741aac8f9ce072840a58dd60b26cf56f
# Parent  968c938f8074bbba2cff02a1f03bb62c29b57311
Bug 851769 part 1 - Handle arrays with length <= 1 more efficiently in Array.prototype.reverse. r=anba

diff --git a/js/src/jsarray.cpp b/js/src/jsarray.cpp
--- a/js/src/jsarray.cpp
+++ b/js/src/jsarray.cpp
@@ -1521,18 +1521,20 @@ SetArrayElements(JSContext* cx, HandleOb
     }
 
     return true;
 }
 
 static DenseElementResult
 ArrayReverseDenseKernel(JSContext* cx, HandleNativeObject obj, uint32_t length)
 {
-    /* An empty array or an array with no elements is already reversed. */
-    if (length == 0 || obj->getDenseInitializedLength() == 0)
+    MOZ_ASSERT(length > 1);
+
+    // If there are no elements, we're done.
+    if (obj->getDenseInitializedLength() == 0)
         return DenseElementResult::Success;
 
     if (obj->denseElementsAreFrozen())
         return DenseElementResult::Incomplete;
 
     if (!IsPackedArray(obj)) {
         /*
          * It's actually surprisingly complicated to reverse an array due
@@ -1591,16 +1593,22 @@ js::array_reverse(JSContext* cx, unsigne
     if (!obj)
         return false;
 
     // Step 2.
     uint64_t len;
     if (!GetLengthProperty(cx, obj, &len))
         return false;
 
+    // An empty array or an array with length 1 is already reversed.
+    if (len <= 1) {
+        args.rval().setObject(*obj);
+        return true;
+    }
+
     if (IsPackedArrayOrNoExtraIndexedProperties(obj, len) && len <= UINT32_MAX) {
         DenseElementResult result =
             ArrayReverseDenseKernel(cx, obj.as<NativeObject>(), uint32_t(len));
         if (result != DenseElementResult::Incomplete) {
             /*
              * Per ECMA-262, don't update the length of the array, even if the new
              * array has trailing holes (and thus the original array began with
              * holes).
