# HG changeset patch
# User Chris Pearce <cpearce@mozilla.com>
# Date 1512358175 -46800
# Node ID 0fe153d0bc4a6253ad4de8834b6c5f73075e9d72
# Parent  71ffd73d99972e0fa282bfbf4a470a2392d2bc27
Bug 1422662 - Move BaseMediaResource::Close() into MediaResource class. r=jwwang

For the project to export Gecko's media stack and import it into Servo,
I need a way to shutdown an abstract MediaResource. So I'd like to move
BaseMediaResource::Close() up into MediaResource class.

MozReview-Commit-ID: 9JmxJPs02PN

diff --git a/dom/media/BaseMediaResource.h b/dom/media/BaseMediaResource.h
--- a/dom/media/BaseMediaResource.h
+++ b/dom/media/BaseMediaResource.h
@@ -28,21 +28,16 @@ public:
    * Create a resource, reading data from the channel. Call on main thread only.
    * The caller must follow up by calling resource->Open().
    */
   static already_AddRefed<BaseMediaResource> Create(
     MediaResourceCallback* aCallback,
     nsIChannel* aChannel,
     bool aIsPrivateBrowsing);
 
-  // Close the resource, stop any listeners, channels, etc.
-  // Cancels any currently blocking Read request and forces that request to
-  // return an error.
-  virtual nsresult Close() = 0;
-
   // Pass true to limit the amount of readahead data (specified by
   // "media.cache_readahead_limit") or false to read as much as the
   // cache size allows.
   virtual void ThrottleReadahead(bool bThrottle) {}
 
   // This is the client's estimate of the playback rate assuming
   // the media plays continuously. The cache can't guess this itself
   // because it doesn't know when the decoder was paused, buffering, etc.
diff --git a/dom/media/MediaResource.h b/dom/media/MediaResource.h
--- a/dom/media/MediaResource.h
+++ b/dom/media/MediaResource.h
@@ -54,16 +54,21 @@ public:
   // Our refcounting is threadsafe, and when our refcount drops to zero
   // we dispatch an event to the main thread to delete the MediaResource.
   // Note that this means it's safe for references to this object to be
   // released on a non main thread, but the destructor will always run on
   // the main thread.
   NS_METHOD_(MozExternalRefCountType) AddRef(void);
   NS_METHOD_(MozExternalRefCountType) Release(void);
 
+  // Close the resource, stop any listeners, channels, etc.
+  // Cancels any currently blocking Read request and forces that request to
+  // return an error.
+  virtual nsresult Close() { return NS_OK; }
+
   // These methods are called off the main thread.
   // Read up to aCount bytes from the stream. The read starts at
   // aOffset in the stream, seeking to that location initially if
   // it is not the current stream offset. The remaining arguments,
   // results and requirements are the same as per the Read method.
   virtual nsresult ReadAt(int64_t aOffset, char* aBuffer,
                           uint32_t aCount, uint32_t* aBytes) = 0;
   // Indicate whether caching data in advance of reads is worth it.
diff --git a/dom/media/mediasource/SourceBufferResource.h b/dom/media/mediasource/SourceBufferResource.h
--- a/dom/media/mediasource/SourceBufferResource.h
+++ b/dom/media/mediasource/SourceBufferResource.h
@@ -30,17 +30,17 @@ DDLoggedTypeDeclNameAndBase(SourceBuffer
 
 // SourceBufferResource is not thread safe.
 class SourceBufferResource final
   : public MediaResource
   , public DecoderDoctorLifeLogger<SourceBufferResource>
 {
 public:
   SourceBufferResource();
-  nsresult Close();
+  nsresult Close() override;
   nsresult ReadAt(int64_t aOffset,
                   char* aBuffer,
                   uint32_t aCount,
                   uint32_t* aBytes) override;
   // Memory-based and no locks, caching discouraged.
   bool ShouldCacheReads() override { return false; }
   void Pin() override { UNIMPLEMENTED(); }
   void Unpin() override { UNIMPLEMENTED(); }
