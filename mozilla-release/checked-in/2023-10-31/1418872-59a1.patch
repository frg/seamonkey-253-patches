# HG changeset patch
# User Cameron McCormack <cam@mcc.id.au>
# Date 1511146005 -28800
# Node ID caa8b67df526a3cc1761a2fafcbb4a36e4c3bdf1
# Parent  17b463b047d1c084be142d5393421226ec69e572
Bug 1418872 - Move ControlCharVisibilityDefault() out of nsCSSParser. r=TYLin

MozReview-Commit-ID: EkNq7lREGgA

diff --git a/layout/base/nsLayoutUtils.cpp b/layout/base/nsLayoutUtils.cpp
--- a/layout/base/nsLayoutUtils.cpp
+++ b/layout/base/nsLayoutUtils.cpp
@@ -124,16 +124,17 @@
 #include "SVGViewportElement.h"
 #include "DisplayItemClip.h"
 #include "mozilla/layers/WebRenderLayerManager.h"
 #include "prenv.h"
 #include "RetainedDisplayListBuilder.h"
 #include "TextDrawTarget.h"
 #include "nsDeckFrame.h"
 #include "nsIEffectiveTLDService.h" // for IsInStyloBlocklist
+#include "mozilla/StylePrefs.h"
 
 #ifdef MOZ_XUL
 #include "nsXULPopupManager.h"
 #endif
 
 #include "GeckoProfiler.h"
 #include "nsAnimationManager.h"
 #include "nsTransitionManager.h"
@@ -9986,8 +9987,16 @@ nsLayoutUtils::ComputeOffsetToUserSpace(
   // frame is painted in the right place.
   gfxPoint toUserSpaceGfx = nsSVGUtils::FrameSpaceInCSSPxToUserSpaceOffset(aFrame);
   nsPoint toUserSpace =
     nsPoint(nsPresContext::CSSPixelsToAppUnits(float(toUserSpaceGfx.x)),
             nsPresContext::CSSPixelsToAppUnits(float(toUserSpaceGfx.y)));
 
   return (offsetToBoundingBox - toUserSpace);
 }
+
+/* static */ uint8_t
+nsLayoutUtils::ControlCharVisibilityDefault()
+{
+  return StylePrefs::sControlCharVisibility
+    ? NS_STYLE_CONTROL_CHARACTER_VISIBILITY_VISIBLE
+    : NS_STYLE_CONTROL_CHARACTER_VISIBILITY_HIDDEN;
+}
diff --git a/layout/base/nsLayoutUtils.h b/layout/base/nsLayoutUtils.h
--- a/layout/base/nsLayoutUtils.h
+++ b/layout/base/nsLayoutUtils.h
@@ -3046,16 +3046,20 @@ public:
   static bool IsInvisibleBreak(nsINode* aNode, nsIFrame** aNextLineFrame = nullptr);
 
   static nsRect ComputeGeometryBox(nsIFrame* aFrame,
                                    StyleGeometryBox aGeometryBox);
 
   static nsPoint ComputeOffsetToUserSpace(nsDisplayListBuilder* aBuilder,
                                           nsIFrame* aFrame);
 
+  // Return the default value to be used for -moz-control-character-visibility,
+  // from preferences.
+  static uint8_t ControlCharVisibilityDefault();
+
 private:
   static uint32_t sFontSizeInflationEmPerLine;
   static uint32_t sFontSizeInflationMinTwips;
   static uint32_t sFontSizeInflationLineThreshold;
   static int32_t  sFontSizeInflationMappingIntercept;
   static uint32_t sFontSizeInflationMaxRatio;
   static bool sFontSizeInflationForceEnabled;
   static bool sFontSizeInflationDisabledInMasterProcess;
diff --git a/layout/style/nsCSSParser.cpp b/layout/style/nsCSSParser.cpp
--- a/layout/style/nsCSSParser.cpp
+++ b/layout/style/nsCSSParser.cpp
@@ -18221,17 +18221,8 @@ nsCSSParser::ParseFontFaceDescriptor(nsC
 
 bool
 nsCSSParser::IsValueValidForProperty(const nsCSSPropertyID aPropID,
                                      const nsAString&    aPropValue)
 {
   return static_cast<CSSParserImpl*>(mImpl)->
     IsValueValidForProperty(aPropID, aPropValue);
 }
-
-/* static */
-uint8_t
-nsCSSParser::ControlCharVisibilityDefault()
-{
-  return StylePrefs::sControlCharVisibility
-    ? NS_STYLE_CONTROL_CHARACTER_VISIBILITY_VISIBLE
-    : NS_STYLE_CONTROL_CHARACTER_VISIBILITY_HIDDEN;
-}
diff --git a/layout/style/nsCSSParser.h b/layout/style/nsCSSParser.h
--- a/layout/style/nsCSSParser.h
+++ b/layout/style/nsCSSParser.h
@@ -335,21 +335,16 @@ public:
                                nsIURI* aBaseURL,
                                nsIPrincipal* aSheetPrincipal,
                                nsCSSValue& aValue);
 
   // Check whether a given value can be applied to a property.
   bool IsValueValidForProperty(const nsCSSPropertyID aPropID,
                                const nsAString&    aPropValue);
 
-  // Return the default value to be used for -moz-control-character-visibility,
-  // from preferences (cached by our Startup(), so that both nsStyleText and
-  // nsRuleNode can have fast access to it).
-  static uint8_t ControlCharVisibilityDefault();
-
 protected:
   // This is a CSSParserImpl*, but if we expose that type name in this
   // header, we can't put the type definition (in nsCSSParser.cpp) in
   // the anonymous namespace.
   void* mImpl;
 };
 
 #endif /* nsCSSParser_h___ */
diff --git a/layout/style/nsRuleNode.cpp b/layout/style/nsRuleNode.cpp
--- a/layout/style/nsRuleNode.cpp
+++ b/layout/style/nsRuleNode.cpp
@@ -5110,17 +5110,17 @@ nsRuleNode::ComputeTextData(void* aStart
   }
 
   // -moz-control-character-visibility: enum, inherit, initial
   SetValue(*aRuleData->ValueForControlCharacterVisibility(),
            text->mControlCharacterVisibility,
            conditions,
            SETVAL_ENUMERATED | SETVAL_UNSET_INHERIT,
            parentText->mControlCharacterVisibility,
-           nsCSSParser::ControlCharVisibilityDefault());
+           nsLayoutUtils::ControlCharVisibilityDefault());
 
   COMPUTE_END_INHERITED(Text, text)
 }
 
 const void*
 nsRuleNode::ComputeTextResetData(void* aStartStruct,
                                  const nsRuleData* aRuleData,
                                  GeckoStyleContext* aContext,
diff --git a/layout/style/nsStyleStruct.cpp b/layout/style/nsStyleStruct.cpp
--- a/layout/style/nsStyleStruct.cpp
+++ b/layout/style/nsStyleStruct.cpp
@@ -4183,17 +4183,17 @@ nsStyleText::nsStyleText(const nsPresCon
   , mWhiteSpace(StyleWhiteSpace::Normal)
   , mWordBreak(NS_STYLE_WORDBREAK_NORMAL)
   , mOverflowWrap(NS_STYLE_OVERFLOWWRAP_NORMAL)
   , mHyphens(StyleHyphens::Manual)
   , mRubyAlign(NS_STYLE_RUBY_ALIGN_SPACE_AROUND)
   , mRubyPosition(NS_STYLE_RUBY_POSITION_OVER)
   , mTextSizeAdjust(NS_STYLE_TEXT_SIZE_ADJUST_AUTO)
   , mTextCombineUpright(NS_STYLE_TEXT_COMBINE_UPRIGHT_NONE)
-  , mControlCharacterVisibility(nsCSSParser::ControlCharVisibilityDefault())
+  , mControlCharacterVisibility(nsLayoutUtils::ControlCharVisibilityDefault())
   , mTextEmphasisStyle(NS_STYLE_TEXT_EMPHASIS_STYLE_NONE)
   , mTextRendering(NS_STYLE_TEXT_RENDERING_AUTO)
   , mTextEmphasisColor(StyleComplexColor::CurrentColor())
   , mWebkitTextFillColor(StyleComplexColor::CurrentColor())
   , mWebkitTextStrokeColor(StyleComplexColor::CurrentColor())
   , mTabSize(float(NS_STYLE_TABSIZE_INITIAL), eStyleUnit_Factor)
   , mWordSpacing(0, nsStyleCoord::CoordConstructor)
   , mLetterSpacing(eStyleUnit_Normal)
