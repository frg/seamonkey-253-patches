# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1508622625 -39600
# Node ID b9207815554a5be47a45c2cbe9c1f910ff89803f
# Parent  abf49ec6a2832ff912efb726ea83d4ba556113fd
Bug 1410294 (part 3) - Overhaul static atom macros. r=froydnj.

There are four things that must be provided for every static atom, two of which
have a macro:
- the atom pointer declaration (no macro);
- the atom pointer definition (no macro);
- the atom char buffer (NS_STATIC_ATOM_BUFFER);
- the StaticAtomSetup struct (NS_STATIC_ATOM_SETUP).

This patch introduces new macros for the first two things: NS_STATIC_ATOM_DECL
and NS_STATIC_ATOM_DEFN, and changes the arguments of the existing two macros
to make them easier to use (e.g. all the '##' concatenation now happens within
the macros).

One consequence of the change is that all static atoms must be within a class,
so the patch adds a couple of classes where necessary (DefaultAtoms, TSAtoms).

The patch also adds a big comment explaining how the macros are used, and what
their expansion looks like. This makes it a lot easier to understand how static
atoms work. Correspondingly, the patch removes some small comments scattered
around the macro use points.

MozReview-Commit-ID: wpRyrEOTHE

diff --git a/dom/base/nsGkAtoms.cpp b/dom/base/nsGkAtoms.cpp
--- a/dom/base/nsGkAtoms.cpp
+++ b/dom/base/nsGkAtoms.cpp
@@ -10,28 +10,27 @@
  * is loaded and they are destroyed when gklayout is unloaded.
  */
 
 #include "nsGkAtoms.h"
 #include "nsStaticAtom.h"
 
 using namespace mozilla;
 
-// define storage for all atoms
-#define GK_ATOM(name_, value_) nsIAtom* nsGkAtoms::name_;
+#define GK_ATOM(name_, value_) NS_STATIC_ATOM_DEFN(nsGkAtoms, name_)
 #include "nsGkAtomList.h"
 #undef GK_ATOM
 
-#define GK_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
+#define GK_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_, value_)
 #include "nsGkAtomList.h"
 #undef GK_ATOM
 
 static const nsStaticAtomSetup sGkAtomSetup[] = {
-#define GK_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(name_##_buffer, &nsGkAtoms::name_),
-#include "nsGkAtomList.h"
-#undef GK_ATOM
+  #define GK_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(nsGkAtoms, name_)
+  #include "nsGkAtomList.h"
+  #undef GK_ATOM
 };
 
 void nsGkAtoms::AddRefAtoms()
 {
   NS_RegisterStaticAtoms(sGkAtomSetup);
 }
 
diff --git a/dom/base/nsGkAtoms.h b/dom/base/nsGkAtoms.h
--- a/dom/base/nsGkAtoms.h
+++ b/dom/base/nsGkAtoms.h
@@ -1,35 +1,22 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-/*
- * This class wraps up the creation (and destruction) of the standard
- * set of atoms used by gklayout; the atoms are created when gklayout
- * is loaded and they are destroyed when gklayout is unloaded.
- */
-
 #ifndef nsGkAtoms_h___
 #define nsGkAtoms_h___
 
-class nsIAtom;
+#include "nsStaticAtom.h"
 
-class nsGkAtoms {
+class nsGkAtoms
+{
 public:
-
   static void AddRefAtoms();
 
-  /* Declare all atoms
-
-     The atom names and values are stored in nsGkAtomList.h and
-     are brought to you by the magic of C preprocessing
-
-     Add new atoms to nsGkAtomList and all support logic will be auto-generated
-   */
-#define GK_ATOM(_name, _value) static nsIAtom* _name;
-#include "nsGkAtomList.h"
-#undef GK_ATOM
+  #define GK_ATOM(_name, _value) NS_STATIC_ATOM_DECL(_name)
+  #include "nsGkAtomList.h"
+  #undef GK_ATOM
 };
 
 #endif /* nsGkAtoms_h___ */
diff --git a/editor/txtsvc/nsTextServicesDocument.cpp b/editor/txtsvc/nsTextServicesDocument.cpp
--- a/editor/txtsvc/nsTextServicesDocument.cpp
+++ b/editor/txtsvc/nsTextServicesDocument.cpp
@@ -65,49 +65,56 @@ public:
   nsINode* mNode;
   int32_t mNodeOffset;
   int32_t mStrOffset;
   int32_t mLength;
   bool    mIsInsertedText;
   bool    mIsValid;
 };
 
-#define TS_ATOM(name_, value_) nsIAtom* nsTextServicesDocument::name_ = 0;
-#include "nsTSAtomList.h" // IWYU pragma: keep
-#undef TS_ATOM
-
 nsTextServicesDocument::nsTextServicesDocument()
 {
   mSelStartIndex  = -1;
   mSelStartOffset = -1;
   mSelEndIndex    = -1;
   mSelEndOffset   = -1;
 
   mIteratorStatus = eIsDone;
 }
 
 nsTextServicesDocument::~nsTextServicesDocument()
 {
   ClearOffsetTable(&mOffsetTable);
 }
 
-#define TS_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
+class TSAtoms
+{
+public:
+  #define TS_ATOM(name_, value_) NS_STATIC_ATOM_DECL(name_)
+  #include "nsTSAtomList.h" // IWYU pragma: keep
+  #undef TS_ATOM
+};
+
+#define TS_ATOM(name_, value_) NS_STATIC_ATOM_DEFN(TSAtoms, name_)
 #include "nsTSAtomList.h" // IWYU pragma: keep
 #undef TS_ATOM
 
-/* static */
-void
+#define TS_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_, value_)
+#include "nsTSAtomList.h" // IWYU pragma: keep
+#undef TS_ATOM
+
+static const nsStaticAtomSetup sTSAtomSetup[] = {
+  #define TS_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(TSAtoms, name_)
+  #include "nsTSAtomList.h" // IWYU pragma: keep
+  #undef TS_ATOM
+};
+
+/* static */ void
 nsTextServicesDocument::RegisterAtoms()
 {
-  static const nsStaticAtomSetup sTSAtomSetup[] = {
-#define TS_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(name_##_buffer, &name_),
-#include "nsTSAtomList.h" // IWYU pragma: keep
-#undef TS_ATOM
-  };
-
   NS_RegisterStaticAtoms(sTSAtomSetup);
 }
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(nsTextServicesDocument)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(nsTextServicesDocument)
 
 NS_INTERFACE_MAP_BEGIN(nsTextServicesDocument)
   NS_INTERFACE_MAP_ENTRY(nsITextServicesDocument)
@@ -1961,42 +1968,42 @@ nsTextServicesDocument::IsBlockNode(nsIC
 {
   if (!aContent) {
     NS_ERROR("How did a null pointer get passed to IsBlockNode?");
     return false;
   }
 
   nsIAtom *atom = aContent->NodeInfo()->NameAtom();
 
-  return (sAAtom       != atom &&
-          sAddressAtom != atom &&
-          sBigAtom     != atom &&
-          sBAtom       != atom &&
-          sCiteAtom    != atom &&
-          sCodeAtom    != atom &&
-          sDfnAtom     != atom &&
-          sEmAtom      != atom &&
-          sFontAtom    != atom &&
-          sIAtom       != atom &&
-          sKbdAtom     != atom &&
-          sKeygenAtom  != atom &&
-          sNobrAtom    != atom &&
-          sSAtom       != atom &&
-          sSampAtom    != atom &&
-          sSmallAtom   != atom &&
-          sSpacerAtom  != atom &&
-          sSpanAtom    != atom &&
-          sStrikeAtom  != atom &&
-          sStrongAtom  != atom &&
-          sSubAtom     != atom &&
-          sSupAtom     != atom &&
-          sTtAtom      != atom &&
-          sUAtom       != atom &&
-          sVarAtom     != atom &&
-          sWbrAtom     != atom);
+  return (TSAtoms::sAAtom       != atom &&
+          TSAtoms::sAddressAtom != atom &&
+          TSAtoms::sBigAtom     != atom &&
+          TSAtoms::sBAtom       != atom &&
+          TSAtoms::sCiteAtom    != atom &&
+          TSAtoms::sCodeAtom    != atom &&
+          TSAtoms::sDfnAtom     != atom &&
+          TSAtoms::sEmAtom      != atom &&
+          TSAtoms::sFontAtom    != atom &&
+          TSAtoms::sIAtom       != atom &&
+          TSAtoms::sKbdAtom     != atom &&
+          TSAtoms::sKeygenAtom  != atom &&
+          TSAtoms::sNobrAtom    != atom &&
+          TSAtoms::sSAtom       != atom &&
+          TSAtoms::sSampAtom    != atom &&
+          TSAtoms::sSmallAtom   != atom &&
+          TSAtoms::sSpacerAtom  != atom &&
+          TSAtoms::sSpanAtom    != atom &&
+          TSAtoms::sStrikeAtom  != atom &&
+          TSAtoms::sStrongAtom  != atom &&
+          TSAtoms::sSubAtom     != atom &&
+          TSAtoms::sSupAtom     != atom &&
+          TSAtoms::sTtAtom      != atom &&
+          TSAtoms::sUAtom       != atom &&
+          TSAtoms::sVarAtom     != atom &&
+          TSAtoms::sWbrAtom     != atom);
 }
 
 bool
 nsTextServicesDocument::HasSameBlockNodeParent(nsIContent *aContent1, nsIContent *aContent2)
 {
   nsIContent* p1 = aContent1->GetParent();
   nsIContent* p2 = aContent2->GetParent();
 
diff --git a/editor/txtsvc/nsTextServicesDocument.h b/editor/txtsvc/nsTextServicesDocument.h
--- a/editor/txtsvc/nsTextServicesDocument.h
+++ b/editor/txtsvc/nsTextServicesDocument.h
@@ -30,20 +30,16 @@ class nsITextServicesFilter;
 
 /** implementation of a text services object.
  *
  */
 class nsTextServicesDocument final : public nsITextServicesDocument,
                                      public nsIEditActionListener
 {
 private:
-  #define TS_ATOM(name_, value_) static nsIAtom* name_;
-  #include "nsTSAtomList.h" // IWYU pragma: keep
-  #undef TS_ATOM
-
   typedef enum { eIsDone=0,        // No iterator (I), or iterator doesn't point to anything valid.
                  eValid,           // I points to first text node (TN) in current block (CB).
                  ePrev,            // No TN in CB, I points to first TN in prev block.
                  eNext             // No TN in CB, I points to first TN in next block.
   } TSDIteratorStatus;
 
   nsCOMPtr<nsIDOMDocument>        mDOMDocument;
   nsCOMPtr<nsISelectionController>mSelCon;
diff --git a/layout/style/moz.build b/layout/style/moz.build
--- a/layout/style/moz.build
+++ b/layout/style/moz.build
@@ -213,17 +213,16 @@ UNIFIED_SOURCES += [
     'nsAnimationManager.cpp',
     'nsComputedDOMStyle.cpp',
     'nsCSSAnonBoxes.cpp',
     'nsCSSDataBlock.cpp',
     'nsCSSKeywords.cpp',
     'nsCSSParser.cpp',
     'nsCSSProps.cpp',
     'nsCSSPseudoClasses.cpp',
-    'nsCSSPseudoElements.cpp',
     'nsCSSRuleProcessor.cpp',
     'nsCSSRules.cpp',
     'nsCSSScanner.cpp',
     'nsCSSValue.cpp',
     'nsDOMCSSAttrDeclaration.cpp',
     'nsDOMCSSDeclaration.cpp',
     'nsDOMCSSRect.cpp',
     'nsDOMCSSRGBColor.cpp',
@@ -272,19 +271,22 @@ UNIFIED_SOURCES += [
     'StreamLoader.cpp',
     'StyleAnimationValue.cpp',
     'StylePrefs.cpp',
     'StyleRule.cpp',
     'StyleSheet.cpp',
     'URLExtraData.cpp',
 ]
 
-# - nsLayoutStylesheetCache.cpp needs to be built separately because it uses
-# nsExceptionHandler.h, which includes windows.h.
 SOURCES += [
+    # Both nsCSSPseudoElements.cpp and nsCSSPseudoClasses.cpp defined a
+    # 'mozPlaceholder' static atom.
+    'nsCSSPseudoElements.cpp',
+    # nsLayoutStylesheetCache.cpp uses nsExceptionHandler.h, which includes
+    # windows.h.
     'nsLayoutStylesheetCache.cpp',
 ]
 
 include('/ipc/chromium/chromium-config.mozbuild')
 
 FINAL_LIBRARY = 'xul'
 
 LOCAL_INCLUDES += [
diff --git a/layout/style/nsCSSAnonBoxes.cpp b/layout/style/nsCSSAnonBoxes.cpp
--- a/layout/style/nsCSSAnonBoxes.cpp
+++ b/layout/style/nsCSSAnonBoxes.cpp
@@ -9,42 +9,40 @@
 #include "mozilla/ArrayUtils.h"
 
 #include "nsCSSAnonBoxes.h"
 #include "nsAtomListUtils.h"
 #include "nsStaticAtom.h"
 
 using namespace mozilla;
 
-// define storage for all atoms
 #define CSS_ANON_BOX(name_, value_) \
-  nsICSSAnonBoxPseudo* nsCSSAnonBoxes::name_;
+  NS_STATIC_ATOM_SUBCLASS_DEFN(nsICSSAnonBoxPseudo, nsCSSAnonBoxes, name_)
 #include "nsCSSAnonBoxList.h"
 #undef CSS_ANON_BOX
 
-#define CSS_ANON_BOX(name_, value_) \
-  NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
+#define CSS_ANON_BOX(name_, value_) NS_STATIC_ATOM_BUFFER(name_, value_)
 #include "nsCSSAnonBoxList.h"
 #undef CSS_ANON_BOX
 
 static const nsStaticAtomSetup sCSSAnonBoxAtomSetup[] = {
   // Put the non-inheriting anon boxes first, so we can index into them easily.
-#define CSS_ANON_BOX(name_, value_) /* nothing */
-#define CSS_NON_INHERITING_ANON_BOX(name_, value_) \
-  NS_STATIC_ATOM_SETUP(name_##_buffer, (nsIAtom**)&nsCSSAnonBoxes::name_),
-#include "nsCSSAnonBoxList.h"
-#undef CSS_NON_INHERITING_ANON_BOX
-#undef CSS_ANON_BOX
+  #define CSS_ANON_BOX(name_, value_) /* nothing */
+  #define CSS_NON_INHERITING_ANON_BOX(name_, value_) \
+    NS_STATIC_ATOM_SUBCLASS_SETUP(nsCSSAnonBoxes, name_)
+  #include "nsCSSAnonBoxList.h"
+  #undef CSS_NON_INHERITING_ANON_BOX
+  #undef CSS_ANON_BOX
 
-#define CSS_ANON_BOX(name_, value_) \
-  NS_STATIC_ATOM_SETUP(name_##_buffer, (nsIAtom**)&nsCSSAnonBoxes::name_),
-#define CSS_NON_INHERITING_ANON_BOX(name_, value_) /* nothing */
-#include "nsCSSAnonBoxList.h"
-#undef CSS_NON_INHERITING_ANON_BOX
-#undef CSS_ANON_BOX
+  #define CSS_ANON_BOX(name_, value_) \
+    NS_STATIC_ATOM_SUBCLASS_SETUP(nsCSSAnonBoxes, name_)
+  #define CSS_NON_INHERITING_ANON_BOX(name_, value_) /* nothing */
+  #include "nsCSSAnonBoxList.h"
+  #undef CSS_NON_INHERITING_ANON_BOX
+  #undef CSS_ANON_BOX
 };
 
 void nsCSSAnonBoxes::AddRefAtoms()
 {
   NS_RegisterStaticAtoms(sCSSAnonBoxAtomSetup);
 }
 
 bool nsCSSAnonBoxes::IsAnonBox(nsIAtom *aAtom)
diff --git a/layout/style/nsCSSAnonBoxes.h b/layout/style/nsCSSAnonBoxes.h
--- a/layout/style/nsCSSAnonBoxes.h
+++ b/layout/style/nsCSSAnonBoxes.h
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* atom list for CSS anonymous boxes */
 
 #ifndef nsCSSAnonBoxes_h___
 #define nsCSSAnonBoxes_h___
 
 #include "nsIAtom.h"
+#include "nsStaticAtom.h"
 
 // Empty class derived from nsIAtom so that function signatures can
 // require an atom from this atom list.
 class nsICSSAnonBoxPseudo : public nsIAtom {};
 
 class nsCSSAnonBoxes {
 public:
 
@@ -25,17 +26,18 @@ public:
   static bool IsTreePseudoElement(nsIAtom* aPseudo);
 #endif
   static bool IsNonElement(nsIAtom* aPseudo)
   {
     return aPseudo == mozText || aPseudo == oofPlaceholder ||
            aPseudo == firstLetterContinuation;
   }
 
-#define CSS_ANON_BOX(_name, _value) static nsICSSAnonBoxPseudo* _name;
+#define CSS_ANON_BOX(name_, value_) \
+  NS_STATIC_ATOM_SUBCLASS_DECL(nsICSSAnonBoxPseudo, name_)
 #include "nsCSSAnonBoxList.h"
 #undef CSS_ANON_BOX
 
   typedef uint8_t NonInheritingBase;
   enum class NonInheriting : NonInheritingBase {
 #define CSS_ANON_BOX(_name, _value) /* nothing */
 #define CSS_NON_INHERITING_ANON_BOX(_name, _value) _name,
 #include "nsCSSAnonBoxList.h"
diff --git a/layout/style/nsCSSPseudoClasses.cpp b/layout/style/nsCSSPseudoClasses.cpp
--- a/layout/style/nsCSSPseudoClasses.cpp
+++ b/layout/style/nsCSSPseudoClasses.cpp
@@ -14,81 +14,88 @@
 #include "mozilla/Preferences.h"
 #include "mozilla/dom/Element.h"
 #include "nsString.h"
 #include "nsAttrValueInlines.h"
 #include "nsIMozBrowserFrame.h"
 
 using namespace mozilla;
 
-// define storage for all atoms
-#define CSS_PSEUDO_CLASS(_name, _value, _flags, _pref) \
-  static nsIAtom* sPseudoClass_##_name;
-#include "nsCSSPseudoClassList.h"
-#undef CSS_PSEUDO_CLASS
-
-#define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
-  NS_STATIC_ATOM_BUFFER(name_##_pseudo_class_buffer, value_)
-#include "nsCSSPseudoClassList.h"
-#undef CSS_PSEUDO_CLASS
-
 #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
   static_assert(!((flags_) & CSS_PSEUDO_CLASS_ENABLED_IN_CHROME) || \
                 ((flags_) & CSS_PSEUDO_CLASS_ENABLED_IN_UA_SHEETS), \
                 "Pseudo-class '" #name_ "' is enabled in chrome, so it " \
                 "should also be enabled in UA sheets");
 #include "nsCSSPseudoClassList.h"
 #undef CSS_PSEUDO_CLASS
 
-// Array of nsStaticAtomSetup for each of the pseudo-classes.
-static const nsStaticAtomSetup sCSSPseudoClassAtomSetup[] = {
+class CSSPseudoClassAtoms
+{
+public:
+  #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
+    NS_STATIC_ATOM_DECL(name_)
+  #include "nsCSSPseudoClassList.h"
+  #undef CSS_PSEUDO_CLASS
+};
+
 #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
-  NS_STATIC_ATOM_SETUP(name_##_pseudo_class_buffer, &sPseudoClass_##name_),
+  NS_STATIC_ATOM_DEFN(CSSPseudoClassAtoms, name_)
 #include "nsCSSPseudoClassList.h"
 #undef CSS_PSEUDO_CLASS
+
+#define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
+  NS_STATIC_ATOM_BUFFER(name_, value_)
+#include "nsCSSPseudoClassList.h"
+#undef CSS_PSEUDO_CLASS
+
+static const nsStaticAtomSetup sCSSPseudoClassAtomSetup[] = {
+  #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
+    NS_STATIC_ATOM_SETUP(CSSPseudoClassAtoms, name_)
+  #include "nsCSSPseudoClassList.h"
+  #undef CSS_PSEUDO_CLASS
 };
 
 // Flags data for each of the pseudo-classes, which must be separate
 // from the previous array since there's no place for it in
 // nsStaticAtomSetup.
 /* static */ const uint32_t
 nsCSSPseudoClasses::kPseudoClassFlags[] = {
-#define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
-  flags_,
-#include "nsCSSPseudoClassList.h"
-#undef CSS_PSEUDO_CLASS
+  #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
+    flags_,
+  #include "nsCSSPseudoClassList.h"
+  #undef CSS_PSEUDO_CLASS
 };
 
 /* static */ bool
 nsCSSPseudoClasses::sPseudoClassEnabled[] = {
-// If the pseudo class has any "ENABLED_IN" flag set, it is disabled by
-// default. Note that, if a pseudo class has pref, whatever its default
-// value is, it'll later be changed in nsCSSPseudoClasses::AddRefAtoms()
-// If the pseudo class has "ENABLED_IN" flags but doesn't have a pref,
-// it is an internal pseudo class which is disabled elsewhere.
-#define IS_ENABLED_BY_DEFAULT(flags_) \
-  (!((flags_) & CSS_PSEUDO_CLASS_ENABLED_MASK))
-#define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
-  IS_ENABLED_BY_DEFAULT(flags_),
-#include "nsCSSPseudoClassList.h"
-#undef CSS_PSEUDO_CLASS
-#undef IS_ENABLED_BY_DEFAULT
+  // If the pseudo class has any "ENABLED_IN" flag set, it is disabled by
+  // default. Note that, if a pseudo class has pref, whatever its default
+  // value is, it'll later be changed in nsCSSPseudoClasses::AddRefAtoms()
+  // If the pseudo class has "ENABLED_IN" flags but doesn't have a pref,
+  // it is an internal pseudo class which is disabled elsewhere.
+  #define IS_ENABLED_BY_DEFAULT(flags_) \
+    (!((flags_) & CSS_PSEUDO_CLASS_ENABLED_MASK))
+  #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
+    IS_ENABLED_BY_DEFAULT(flags_),
+  #include "nsCSSPseudoClassList.h"
+  #undef CSS_PSEUDO_CLASS
+  #undef IS_ENABLED_BY_DEFAULT
 };
 
 void nsCSSPseudoClasses::AddRefAtoms()
 {
   NS_RegisterStaticAtoms(sCSSPseudoClassAtomSetup);
 
-#define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_)                        \
-  if (pref_[0]) {                                                             \
-    auto idx = static_cast<CSSPseudoElementTypeBase>(Type::name_);            \
-    Preferences::AddBoolVarCache(&sPseudoClassEnabled[idx], pref_);           \
-  }
-#include "nsCSSPseudoClassList.h"
-#undef CSS_PSEUDO_CLASS
+  #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_)                      \
+    if (pref_[0]) {                                                           \
+      auto idx = static_cast<CSSPseudoElementTypeBase>(Type::name_);          \
+      Preferences::AddBoolVarCache(&sPseudoClassEnabled[idx], pref_);         \
+    }
+  #include "nsCSSPseudoClassList.h"
+  #undef CSS_PSEUDO_CLASS
 }
 
 bool
 nsCSSPseudoClasses::HasStringArg(Type aType)
 {
   return aType == Type::lang ||
          aType == Type::mozSystemMetric ||
          aType == Type::mozLocaleDir ||
diff --git a/layout/style/nsCSSPseudoElements.cpp b/layout/style/nsCSSPseudoElements.cpp
--- a/layout/style/nsCSSPseudoElements.cpp
+++ b/layout/style/nsCSSPseudoElements.cpp
@@ -12,33 +12,32 @@
 
 #include "nsAtomListUtils.h"
 #include "nsStaticAtom.h"
 #include "nsCSSAnonBoxes.h"
 #include "nsDOMString.h"
 
 using namespace mozilla;
 
-// define storage for all atoms
 #define CSS_PSEUDO_ELEMENT(name_, value_, flags_) \
-  nsICSSPseudoElement* nsCSSPseudoElements::name_;
+  NS_STATIC_ATOM_BUFFER(name_, value_)
 #include "nsCSSPseudoElementList.h"
 #undef CSS_PSEUDO_ELEMENT
 
 #define CSS_PSEUDO_ELEMENT(name_, value_, flags_) \
-  NS_STATIC_ATOM_BUFFER(name_##_pseudo_element_buffer, value_)
+  NS_STATIC_ATOM_SUBCLASS_DEFN(nsICSSPseudoElement, nsCSSPseudoElements, name_)
 #include "nsCSSPseudoElementList.h"
 #undef CSS_PSEUDO_ELEMENT
 
 // Array of nsStaticAtomSetup for each of the pseudo-elements.
 static const nsStaticAtomSetup sCSSPseudoElementAtomSetup[] = {
-#define CSS_PSEUDO_ELEMENT(name_, value_, flags_) \
-  NS_STATIC_ATOM_SETUP(name_##_pseudo_element_buffer, (nsIAtom**)&nsCSSPseudoElements::name_),
-#include "nsCSSPseudoElementList.h"
-#undef CSS_PSEUDO_ELEMENT
+  #define CSS_PSEUDO_ELEMENT(name_, value_, flags_) \
+    NS_STATIC_ATOM_SUBCLASS_SETUP(nsCSSPseudoElements, name_)
+  #include "nsCSSPseudoElementList.h"
+  #undef CSS_PSEUDO_ELEMENT
 };
 
 // Flags data for each of the pseudo-elements, which must be separate
 // from the previous array since there's no place for it in
 // nsStaticAtomSetup.
 /* static */ const uint32_t
 nsCSSPseudoElements::kPseudoElementFlags[] = {
 #define CSS_PSEUDO_ELEMENT(name_, value_, flags_) \
diff --git a/layout/style/nsCSSPseudoElements.h b/layout/style/nsCSSPseudoElements.h
--- a/layout/style/nsCSSPseudoElements.h
+++ b/layout/style/nsCSSPseudoElements.h
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* atom list for CSS pseudo-elements */
 
 #ifndef nsCSSPseudoElements_h___
 #define nsCSSPseudoElements_h___
 
 #include "nsIAtom.h"
+#include "nsStaticAtom.h"
 #include "mozilla/CSSEnabledState.h"
 #include "mozilla/Compiler.h"
 
 // Is this pseudo-element a CSS2 pseudo-element that can be specified
 // with the single colon syntax (in addition to the double-colon syntax,
 // which can be used for all pseudo-elements)?
 //
 // Note: We also rely on this for IsEagerlyCascadedInServo.
@@ -50,17 +51,17 @@
 namespace mozilla {
 
 // The total count of CSSPseudoElement is less than 256,
 // so use uint8_t as its underlying type.
 typedef uint8_t CSSPseudoElementTypeBase;
 enum class CSSPseudoElementType : CSSPseudoElementTypeBase {
   // If the actual pseudo-elements stop being first here, change
   // GetPseudoType.
-#define CSS_PSEUDO_ELEMENT(_name, _value_, _flags) \
+#define CSS_PSEUDO_ELEMENT(_name, _value, _flags) \
   _name,
 #include "nsCSSPseudoElementList.h"
 #undef CSS_PSEUDO_ELEMENT
   Count,
   InheritingAnonBox = Count, // pseudo from nsCSSAnonBoxes,
                              // IsNonInheritingAnonBox false.
   NonInheritingAnonBox, // from nsCSSAnonBoxes, IsNonInheritingAnonBox true.
 #ifdef MOZ_XUL
@@ -93,17 +94,17 @@ public:
     return PseudoElementHasFlags(aType, CSS_PSEUDO_ELEMENT_IS_CSS2);
   }
 
 
   // This must match EAGER_PSEUDO_COUNT in Rust code.
   static const size_t kEagerPseudoCount = 4;
 
 #define CSS_PSEUDO_ELEMENT(_name, _value, _flags) \
-  static nsICSSPseudoElement* _name;
+  NS_STATIC_ATOM_SUBCLASS_DECL(nsICSSPseudoElement, _name)
 #include "nsCSSPseudoElementList.h"
 #undef CSS_PSEUDO_ELEMENT
 
   static Type GetPseudoType(nsIAtom* aAtom, EnabledState aEnabledState);
 
   // Get the atom for a given Type. aType must be < CSSPseudoElementType::Count.
   // This only ever returns static atoms, so it's fine to return a raw pointer.
   static nsIAtom* GetPseudoAtom(Type aType);
diff --git a/parser/htmlparser/nsHTMLTags.cpp b/parser/htmlparser/nsHTMLTags.cpp
--- a/parser/htmlparser/nsHTMLTags.cpp
+++ b/parser/htmlparser/nsHTMLTags.cpp
@@ -18,24 +18,20 @@ using namespace mozilla;
 #define HTML_TAG(_tag, _classname, _interfacename) (u"" #_tag),
 #define HTML_OTHER(_tag)
 const char16_t* const nsHTMLTags::sTagUnicodeTable[] = {
 #include "nsHTMLTagList.h"
 };
 #undef HTML_TAG
 #undef HTML_OTHER
 
-// static array of tag atoms
-nsIAtom* nsHTMLTags::sTagAtomTable[eHTMLTag_userdefined - 1];
-
 int32_t nsHTMLTags::gTableRefCount;
 PLHashTable* nsHTMLTags::gTagTable;
 PLHashTable* nsHTMLTags::gTagAtomTable;
 
-
 // char16_t* -> id hash
 static PLHashNumber
 HTMLTagsHashCodeUCPtr(const void *key)
 {
   return HashString(static_cast<const char16_t*>(key));
 }
 
 static int
@@ -51,39 +47,41 @@ HTMLTagsKeyCompareUCPtr(const void *key1
 static PLHashNumber
 HTMLTagsHashCodeAtom(const void *key)
 {
   return NS_PTR_TO_INT32(key) >> 2;
 }
 
 #define NS_HTMLTAG_NAME_MAX_LENGTH 10
 
-// static
-void
-nsHTMLTags::RegisterAtoms(void)
-{
-#define HTML_TAG(_tag, _classname, _interfacename) NS_STATIC_ATOM_BUFFER(Atombuffer_##_tag, #_tag)
+// This would use NS_STATIC_ATOM_DEFN if it wasn't an array.
+nsIAtom* nsHTMLTags::sTagAtomTable[eHTMLTag_userdefined - 1];
+
+#define HTML_TAG(_tag, _classname, _interfacename) \
+  NS_STATIC_ATOM_BUFFER(_tag, #_tag)
 #define HTML_OTHER(_tag)
 #include "nsHTMLTagList.h"
 #undef HTML_TAG
 #undef HTML_OTHER
 
-// static array of tag StaticAtom structs
-#define HTML_TAG(_tag, _classname, _interfacename) NS_STATIC_ATOM_SETUP(Atombuffer_##_tag, &nsHTMLTags::sTagAtomTable[eHTMLTag_##_tag - 1]),
-#define HTML_OTHER(_tag)
+/* static */ void
+nsHTMLTags::RegisterAtoms(void)
+{
+  // This would use NS_STATIC_ATOM_SETUP if it wasn't an array.
   static const nsStaticAtomSetup sTagAtomSetup[] = {
-#include "nsHTMLTagList.h"
+    #define HTML_TAG(_tag, _classname, _interfacename) \
+      { _tag##_buffer, &nsHTMLTags::sTagAtomTable[eHTMLTag_##_tag - 1] },
+    #define HTML_OTHER(_tag)
+    #include "nsHTMLTagList.h"
+    #undef HTML_TAG
+    #undef HTML_OTHER
   };
-#undef HTML_TAG
-#undef HTML_OTHER
 
-  // Fill in our static atom pointers
   NS_RegisterStaticAtoms(sTagAtomSetup);
 
-
 #if defined(DEBUG)
   {
     // let's verify that all names in the the table are lowercase...
     for (int32_t i = 0; i < NS_HTML_TAG_MAX; ++i) {
       nsAutoString temp1((char16_t*)sTagAtomSetup[i].mString);
       nsAutoString temp2((char16_t*)sTagAtomSetup[i].mString);
       ToLowerCase(temp1);
       NS_ASSERTION(temp1.Equals(temp2), "upper case char in table");
diff --git a/parser/htmlparser/nsHTMLTags.h b/parser/htmlparser/nsHTMLTags.h
--- a/parser/htmlparser/nsHTMLTags.h
+++ b/parser/htmlparser/nsHTMLTags.h
@@ -1,21 +1,20 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsHTMLTags_h___
 #define nsHTMLTags_h___
 
+#include "nsStaticAtom.h"
 #include "nsString.h"
 #include "plhash.h"
 
-class nsIAtom;
-
 /*
    Declare the enum list using the magic of preprocessing
    enum values are "eHTMLTag_foo" (where foo is the tag)
 
    To change the list of tags, see nsHTMLTagList.h
 
    These enum values are used as the index of array in various places.
    If we change the structure of the enum by adding entries to it or removing
@@ -71,16 +70,17 @@ public:
     return tag ? (nsHTMLTag)NS_PTR_TO_INT32(tag) : eHTMLTag_userdefined;
   }
 
 #ifdef DEBUG
   static void TestTagTable();
 #endif
 
 private:
+  // This would use NS_STATIC_ATOM_DECL if it wasn't an array.
   static nsIAtom* sTagAtomTable[eHTMLTag_userdefined - 1];
   static const char16_t* const sTagUnicodeTable[];
 
   static int32_t gTableRefCount;
   static PLHashTable* gTagTable;
   static PLHashTable* gTagAtomTable;
 };
 
diff --git a/rdf/base/nsRDFContentSink.cpp b/rdf/base/nsRDFContentSink.cpp
--- a/rdf/base/nsRDFContentSink.cpp
+++ b/rdf/base/nsRDFContentSink.cpp
@@ -121,19 +121,19 @@ public:
     static nsIRDFContainerUtils* gRDFContainerUtils;
     static nsIRDFResource* kRDF_type;
     static nsIRDFResource* kRDF_instanceOf; // XXX should be RDF:type
     static nsIRDFResource* kRDF_Alt;
     static nsIRDFResource* kRDF_Bag;
     static nsIRDFResource* kRDF_Seq;
     static nsIRDFResource* kRDF_nextVal;
 
-#define RDF_ATOM(name_, value_) static nsIAtom* name_;
-#include "nsRDFContentSinkAtomList.h"
-#undef RDF_ATOM
+    #define RDF_ATOM(name_, value_) NS_STATIC_ATOM_DECL(name_)
+    #include "nsRDFContentSinkAtomList.h"
+    #undef RDF_ATOM
 
     typedef struct ContainerInfo {
         nsIRDFResource**  mType;
         nsContainerTestFn mTestFn;
         nsMakeContainerFn mMakeFn;
     } ContainerInfo;
 
 protected:
@@ -230,28 +230,29 @@ nsIRDFResource* RDFContentSinkImpl::kRDF
 nsIRDFResource* RDFContentSinkImpl::kRDF_Bag;
 nsIRDFResource* RDFContentSinkImpl::kRDF_Seq;
 nsIRDFResource* RDFContentSinkImpl::kRDF_nextVal;
 
 mozilla::LazyLogModule RDFContentSinkImpl::gLog("nsRDFContentSink");
 
 ////////////////////////////////////////////////////////////////////////
 
-#define RDF_ATOM(name_, value_) nsIAtom* RDFContentSinkImpl::name_;
+#define RDF_ATOM(name_, value_) NS_STATIC_ATOM_DEFN(RDFContentSinkImpl, name_)
 #include "nsRDFContentSinkAtomList.h"
 #undef RDF_ATOM
 
-#define RDF_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
+#define RDF_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_, value_)
 #include "nsRDFContentSinkAtomList.h"
 #undef RDF_ATOM
 
 static const nsStaticAtomSetup sRDFContentSinkAtomSetup[] = {
-#define RDF_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(name_##_buffer, &RDFContentSinkImpl::name_),
-#include "nsRDFContentSinkAtomList.h"
-#undef RDF_ATOM
+  #define RDF_ATOM(name_, value_) \
+    NS_STATIC_ATOM_SETUP(RDFContentSinkImpl, name_)
+  #include "nsRDFContentSinkAtomList.h"
+  #undef RDF_ATOM
 };
 
 // static
 void
 nsRDFAtoms::RegisterAtoms()
 {
     NS_RegisterStaticAtoms(sRDFContentSinkAtomSetup);
 }
diff --git a/xpcom/ds/nsAtomTable.cpp b/xpcom/ds/nsAtomTable.cpp
--- a/xpcom/ds/nsAtomTable.cpp
+++ b/xpcom/ds/nsAtomTable.cpp
@@ -8,16 +8,17 @@
 #include "mozilla/Attributes.h"
 #include "mozilla/HashFunctions.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/Mutex.h"
 #include "mozilla/DebugOnly.h"
 #include "mozilla/Sprintf.h"
 #include "mozilla/Unused.h"
 
+#include "nsIAtom.h"
 #include "nsAtomTable.h"
 #include "nsStaticAtom.h"
 #include "nsString.h"
 #include "nsCRT.h"
 #include "PLDHashTable.h"
 #include "prenv.h"
 #include "nsThreadUtils.h"
 #include "nsDataHashtable.h"
@@ -497,35 +498,44 @@ static bool gStaticAtomTableSealed = fal
 // shrink it to 4096 entries.
 //
 // By choosing an initial length of 4096, we get an initial capacity of 8192.
 // That's the biggest initial capacity that will let us be > 25% full when the
 // first dynamic atom is removed (when the count is ~2700), thus avoiding any
 // shrinking.
 #define ATOM_HASHTABLE_INITIAL_LENGTH  4096
 
+class DefaultAtoms
+{
+public:
+  NS_STATIC_ATOM_DECL(empty)
+};
+
+NS_STATIC_ATOM_DEFN(DefaultAtoms, empty)
+
+NS_STATIC_ATOM_BUFFER(empty, "")
+
+static const nsStaticAtomSetup sDefaultAtomSetup[] = {
+  NS_STATIC_ATOM_SETUP(DefaultAtoms, empty)
+};
+
 void
 NS_InitAtomTable()
 {
   MOZ_ASSERT(!gAtomTable);
   gAtomTable = new PLDHashTable(&AtomTableOps, sizeof(AtomTableEntry),
                                 ATOM_HASHTABLE_INITIAL_LENGTH);
   gAtomTableLock = new Mutex("Atom Table Lock");
 
   // Bug 1340710 has caused us to generate an empty atom at arbitrary times
   // after startup.  If we end up creating one before nsGkAtoms::_empty is
   // registered, we get an assertion about transmuting a dynamic atom into a
   // static atom.  In order to avoid that, we register an empty string static
   // atom as soon as we initialize the atom table to guarantee that the empty
   // string atom will always be static.
-  NS_STATIC_ATOM_BUFFER(empty, "");
-  static nsIAtom* empty_atom = nullptr;
-  static const nsStaticAtomSetup sDefaultAtomSetup[] = {
-    NS_STATIC_ATOM_SETUP(empty, &empty_atom)
-  };
   NS_RegisterStaticAtoms(sDefaultAtomSetup);
 }
 
 void
 NS_ShutdownAtomTable()
 {
   delete gStaticAtomTable;
   gStaticAtomTable = nullptr;
diff --git a/xpcom/ds/nsStaticAtom.h b/xpcom/ds/nsStaticAtom.h
--- a/xpcom/ds/nsStaticAtom.h
+++ b/xpcom/ds/nsStaticAtom.h
@@ -2,41 +2,126 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsStaticAtom_h__
 #define nsStaticAtom_h__
 
-#include "nsIAtom.h"
-#include "nsStringBuffer.h"
+#include <stdint.h>
+
+class nsIAtom;
 
-#define NS_STATIC_ATOM_SETUP(buffer_name, atom_ptr) \
-  { buffer_name, atom_ptr }
+// The following macros are used to define static atoms, typically in
+// conjunction with a .h file that defines the names and values of the atoms.
+//
+// For example, the .h file might be called MyAtomList.h and look like this:
+//
+//   MY_ATOM(one, "one")
+//   MY_ATOM(two, "two")
+//   MY_ATOM(three, "three")
+//
+// The code defining the static atoms might look like this:
+//
+//   class MyAtoms {
+//   public:
+//     #define MY_ATOM(_name, _value) NS_STATIC_ATOM_DECL(_name)
+//     #include "MyAtomList.h"
+//     #undef MY_ATOM
+//   };
+//
+//   #define MY_ATOM(name_, value_) NS_STATIC_ATOM_DEFN(MyAtoms, name_)
+//   #include "MyAtomList.h"
+//   #undef MY_ATOM
+//
+//   #define MY_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_, value_)
+//   #include "MyAtomList.h"
+//   #undef MY_ATOM
+//
+//   static const nsStaticAtomSetup sMyAtomSetup[] = {
+//     #define MY_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(MyAtoms, name_)
+//     #include "MyAtomList.h"
+//     #undef MY_ATOM
+//   };
+//
+// The macros expand to the following:
+//
+//   class MyAtoms
+//   {
+//   public:
+//     static nsIAtom* one;
+//     static nsIAtom* two;
+//     static nsIAtom* three;
+//   };
+//
+//   nsIAtom* MyAtoms::one;
+//   nsIAtom* MyAtoms::two;
+//   nsIAtom* MyAtoms::three;
+//
+//   static const char16_t one_buffer[4] = u"one";     // plus a static_assert
+//   static const char16_t two_buffer[4] = u"two";     // plus a static_assert
+//   static const char16_t three_buffer[6] = u"three"; // plus a static_assert
+//
+//   static const nsStaticAtomSetup sMyAtomSetup[] = {
+//     { one_buffer, &MyAtoms::one },
+//     { two_buffer, &MyAtoms::two },
+//     { three_buffer, &MyAtoms::three },
+//   };
+//
+// When RegisterStaticAtoms(sMyAtomSetup) is called it iterates over
+// sMyAtomSetup[]. E.g. for the first atom it does roughly the following:
+// - MyAtoms::one = new nsIAtom(one_buffer)
+// - inserts MyAtoms::one into the atom table
 
-// Note that |str_data| is an 8-bit string, and so |sizeof(str_data)| is equal
+// The declaration of the pointer to the static atom, which must be within a
+// class.
+#define NS_STATIC_ATOM_DECL(name_) \
+  static nsIAtom* name_;
+
+// Like NS_STATIC_ATOM_DECL, but for sub-classes of nsIAtom.
+#define NS_STATIC_ATOM_SUBCLASS_DECL(type_, name_) \
+  static type_* name_;
+
+// The definition of the pointer to the static atom. Initially null, it is
+// set by RegisterStaticAtoms() to point to a heap-allocated nsIAtom.
+#define NS_STATIC_ATOM_DEFN(class_, name_) \
+  nsIAtom* class_::name_;
+
+// Like NS_STATIC_ATOM_DEFN, but for sub-classes of nsIAtom.
+#define NS_STATIC_ATOM_SUBCLASS_DEFN(type_, class_, name_) \
+  type_* class_::name_;
+
+// The buffer of 16-bit chars that constitute the static atom.
+//
+// Note that |value_| is an 8-bit string, and so |sizeof(value_)| is equal
 // to the number of chars (including the terminating '\0'). The |u""| prefix
-// converts |str_data| to a 16-bit string, which is assigned.
-#define NS_STATIC_ATOM_BUFFER(buffer_name, str_data) \
-  static const char16_t buffer_name[sizeof(str_data)] = u"" str_data; \
-  static_assert(sizeof(str_data[0]) == 1, "non-8-bit static atom literal");
+// converts |value_| to a 16-bit string, which is what is assigned.
+#define NS_STATIC_ATOM_BUFFER(name_, value_) \
+  static const char16_t name_##_buffer[sizeof(value_)] = u"" value_; \
+  static_assert(sizeof(value_[0]) == 1, "non-8-bit static atom literal");
 
-/**
- * Holds data used to initialize large number of atoms during startup. Use
- * the above macros to initialize these structs. They should never be accessed
- * directly other than from AtomTable.cpp.
- */
+// The StaticAtomSetup. Used only during start-up.
+#define NS_STATIC_ATOM_SETUP(class_, name_) \
+  { name_##_buffer, &class_::name_ },
+
+// Like NS_STATIC_ATOM_SUBCLASS, but for sub-classes of nsIAtom.
+#define NS_STATIC_ATOM_SUBCLASS_SETUP(class_, name_) \
+  { name_##_buffer, reinterpret_cast<nsIAtom**>(&class_::name_) },
+
+// Holds data used to initialize large number of atoms during startup. Use
+// NS_STATIC_ATOM_SETUP to initialize these structs. They should never be
+// accessed directly other than from nsAtomTable.cpp.
 struct nsStaticAtomSetup
 {
   const char16_t* const mString;
   nsIAtom** const mAtom;
 };
 
-// Register an array of static atoms with the atom table
+// Register an array of static atoms with the atom table.
 template<uint32_t N>
 void
 NS_RegisterStaticAtoms(const nsStaticAtomSetup (&aSetup)[N])
 {
   extern void RegisterStaticAtoms(const nsStaticAtomSetup* aSetup,
                                   uint32_t aCount);
   RegisterStaticAtoms(aSetup, N);
 }
diff --git a/xpcom/io/nsDirectoryService.cpp b/xpcom/io/nsDirectoryService.cpp
--- a/xpcom/io/nsDirectoryService.cpp
+++ b/xpcom/io/nsDirectoryService.cpp
@@ -100,28 +100,29 @@ nsDirectoryService::Create(nsISupports* 
 
   if (!gService) {
     return NS_ERROR_NOT_INITIALIZED;
   }
 
   return gService->QueryInterface(aIID, aResult);
 }
 
-#define DIR_ATOM(name_, value_) nsIAtom* nsDirectoryService::name_ = nullptr;
+#define DIR_ATOM(name_, value_) NS_STATIC_ATOM_DEFN(nsDirectoryService, name_)
 #include "nsDirectoryServiceAtomList.h"
 #undef DIR_ATOM
 
-#define DIR_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
+#define DIR_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_, value_)
 #include "nsDirectoryServiceAtomList.h"
 #undef DIR_ATOM
 
 static const nsStaticAtomSetup sDirectoryServiceAtomSetup[] = {
-#define DIR_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(name_##_buffer, &nsDirectoryService::name_),
-#include "nsDirectoryServiceAtomList.h"
-#undef DIR_ATOM
+  #define DIR_ATOM(name_, value_) \
+    NS_STATIC_ATOM_SETUP(nsDirectoryService, name_)
+  #include "nsDirectoryServiceAtomList.h"
+  #undef DIR_ATOM
 };
 
 NS_IMETHODIMP
 nsDirectoryService::Init()
 {
   NS_NOTREACHED("nsDirectoryService::Init() for internal use only!");
   return NS_OK;
 }
diff --git a/xpcom/io/nsDirectoryService.h b/xpcom/io/nsDirectoryService.h
--- a/xpcom/io/nsDirectoryService.h
+++ b/xpcom/io/nsDirectoryService.h
@@ -6,16 +6,17 @@
 
 #ifndef nsDirectoryService_h___
 #define nsDirectoryService_h___
 
 #include "nsIDirectoryService.h"
 #include "nsInterfaceHashtable.h"
 #include "nsIFile.h"
 #include "nsIAtom.h"
+#include "nsStaticAtom.h"
 #include "nsTArray.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/StaticPtr.h"
 
 #define NS_XPCOM_INIT_CURRENT_PROCESS_DIR       "MozBinD"   // Can be used to set NS_XPCOM_CURRENT_PROCESS_DIR
                                                             // CANNOT be used to GET a location
 #define NS_DIRECTORY_SERVICE_CID  {0xf00152d0,0xb40b,0x11d3,{0x8c, 0x9c, 0x00, 0x00, 0x64, 0x65, 0x73, 0x74}}
 
@@ -49,18 +50,14 @@ private:
   ~nsDirectoryService();
 
   nsresult GetCurrentProcessDirectory(nsIFile** aFile);
 
   nsInterfaceHashtable<nsCStringHashKey, nsIFile> mHashtable;
   nsTArray<nsCOMPtr<nsIDirectoryServiceProvider>> mProviders;
 
 public:
-
-#define DIR_ATOM(name_, value_) static nsIAtom* name_;
-#include "nsDirectoryServiceAtomList.h"
-#undef DIR_ATOM
-
+  #define DIR_ATOM(name_, value_) NS_STATIC_ATOM_DECL(name_)
+  #include "nsDirectoryServiceAtomList.h"
+  #undef DIR_ATOM
 };
 
-
 #endif
-
