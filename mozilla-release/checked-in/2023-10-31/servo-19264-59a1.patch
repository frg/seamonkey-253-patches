# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1510934585 21600
# Node ID 251110eaf3c43520f1ba7c5d452b4a2c099e6cc9
# Parent  a4342159331f32f9976652ba3f36c872536ebc04
servo: Merge #19264 - style: Remove unused argument in element_needs_traversal (from emilio:unused-arg); r=mbrubeck

Source-Repo: https://github.com/servo/servo
Source-Revision: bd6caef234f9a1c29cc01c907255a4f307fbc827

diff --git a/servo/components/style/traversal.rs b/servo/components/style/traversal.rs
--- a/servo/components/style/traversal.rs
+++ b/servo/components/style/traversal.rs
@@ -150,44 +150,41 @@ pub trait DomTraversal<E: TElement> : Sy
         // that there's something to traverse, and we don't need to do any
         // invalidation since we're not doing any restyling.
         if traversal_flags.contains(TraversalFlags::UnstyledOnly) {
             return PreTraverseToken(Some(root))
         }
 
         let mut data = root.mutate_data();
         let mut data = data.as_mut().map(|d| &mut **d);
-        let parent = root.traversal_parent();
-        let parent_data = parent.as_ref().and_then(|p| p.borrow_data());
 
         if let Some(ref mut data) = data {
             if !traversal_flags.for_animation_only() {
                 // Invalidate our style, and that of our siblings and
                 // descendants as needed.
                 //
                 // FIXME(emilio): an nth-index cache could be worth here, even
                 // if temporary?
                 let invalidation_result =
                     data.invalidate_style_if_needed(root, shared_context, None, None);
 
                 if invalidation_result.has_invalidated_siblings() {
-                    let actual_root =
-                        parent.expect("How in the world can you invalidate \
-                                       siblings without a parent?");
+                    let actual_root = root.traversal_parent()
+                        .expect("How in the world can you invalidate \
+                                 siblings without a parent?");
                     unsafe { actual_root.set_dirty_descendants() }
                     return PreTraverseToken(Some(actual_root));
                 }
             }
         }
 
         let should_traverse = Self::element_needs_traversal(
             root,
             traversal_flags,
             data.as_mut().map(|d| &**d),
-            parent_data.as_ref().map(|d| &**d)
         );
 
         // If we're not going to traverse at all, we may need to clear some state
         // off the root (which would normally be done at the end of recalc_style_at).
         if !should_traverse && data.is_some() {
             clear_state_after_traversing(root, data.unwrap(), traversal_flags);
         }
 
@@ -198,27 +195,23 @@ pub trait DomTraversal<E: TElement> : Sy
     /// never processes text nodes, but Servo overrides this to visit them for
     /// flow construction when necessary.
     fn text_node_needs_traversal(node: E::ConcreteNode, _parent_data: &ElementData) -> bool {
         debug_assert!(node.is_text_node());
         false
     }
 
     /// Returns true if traversal is needed for the given element and subtree.
-    ///
-    /// The caller passes |parent_data|, which is only null if there is no
-    /// parent.
     fn element_needs_traversal(
         el: E,
         traversal_flags: TraversalFlags,
         data: Option<&ElementData>,
-        parent_data: Option<&ElementData>,
     ) -> bool {
-        debug!("element_needs_traversal({:?}, {:?}, {:?}, {:?})",
-               el, traversal_flags, data, parent_data);
+        debug!("element_needs_traversal({:?}, {:?}, {:?})",
+               el, traversal_flags, data);
 
         if traversal_flags.contains(TraversalFlags::UnstyledOnly) {
             return data.map_or(true, |d| !d.has_styles()) || el.has_dirty_descendants();
         }
 
 
         // In case of animation-only traversal we need to traverse the element
         // if the element has animation only dirty descendants bit,
@@ -840,17 +833,17 @@ where
             child_data.invalidate_style_if_needed(
                 child,
                 &context.shared,
                 Some(&context.thread_local.stack_limit_checker),
                 Some(&mut context.thread_local.nth_index_cache)
             );
         }
 
-        if D::element_needs_traversal(child, flags, child_data.map(|d| &*d), Some(data)) {
+        if D::element_needs_traversal(child, flags, child_data.map(|d| &*d)) {
             note_child(child_node);
 
             // Set the dirty descendants bit on the parent as needed, so that we
             // can find elements during the post-traversal.
             //
             // Note that these bits may be cleared again at the bottom of
             // recalc_style_at if requested by the caller.
             if !is_initial_style {
