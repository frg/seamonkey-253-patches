# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1513114813 21600
# Node ID fd5e1a313cfb09ec2722cee935fefa7849841901
# Parent  0b879e80a064a3e7a127e1aaeb9a8cfe2d053273
Bug 1412289 - Add a shell function to enable NativeObject::checkShapeConsistency. r=bhackett

diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -3098,16 +3098,27 @@ HelperThreadCount(JSContext* cx, unsigne
     if (CanUseExtraThreads())
         args.rval().setInt32(HelperThreadState().threadCount);
     else
         args.rval().setInt32(0);
 #endif
     return true;
 }
 
+static bool
+EnableShapeConsistencyChecks(JSContext* cx, unsigned argc, Value* vp)
+{
+    CallArgs args = CallArgsFromVp(argc, vp);
+#ifdef DEBUG
+    NativeObject::enableShapeConsistencyChecks();
+#endif
+    args.rval().setUndefined();
+    return true;
+}
+
 #ifdef JS_TRACE_LOGGING
 static bool
 EnableTraceLogger(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     TraceLoggerThread* logger = TraceLoggerForCurrentThread(cx);
     if (!TraceLoggerEnable(logger, cx))
         return false;
@@ -5354,16 +5365,20 @@ gc::ZealModeHelpText),
 "detachArrayBuffer(buffer)",
 "  Detach the given ArrayBuffer object from its memory, i.e. as if it\n"
 "  had been transferred to a WebWorker."),
 
     JS_FN_HELP("helperThreadCount", HelperThreadCount, 0, 0,
 "helperThreadCount()",
 "  Returns the number of helper threads available for off-thread tasks."),
 
+    JS_FN_HELP("enableShapeConsistencyChecks", EnableShapeConsistencyChecks, 0, 0,
+"enableShapeConsistencyChecks()",
+"  Enable some slow Shape assertions.\n"),
+
 #ifdef JS_TRACE_LOGGING
     JS_FN_HELP("startTraceLogger", EnableTraceLogger, 0, 0,
 "startTraceLogger()",
 "  Start logging this thread.\n"),
 
     JS_FN_HELP("stopTraceLogger", DisableTraceLogger, 0, 0,
 "stopTraceLogger()",
 "  Stop logging this thread."),
diff --git a/js/src/jit-test/tests/basic/shape-checks.js b/js/src/jit-test/tests/basic/shape-checks.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/basic/shape-checks.js
@@ -0,0 +1,8 @@
+enableShapeConsistencyChecks();
+var o = {};
+for (var i = 0; i < 50; i++) {
+    o["x" + i] = i;
+}
+for (var i = 0; i < 50; i += 2) {
+    delete o["x" + i];
+}
diff --git a/js/src/vm/NativeObject.cpp b/js/src/vm/NativeObject.cpp
--- a/js/src/vm/NativeObject.cpp
+++ b/js/src/vm/NativeObject.cpp
@@ -120,27 +120,28 @@ ObjectElements::FreezeElements(JSContext
 
     MarkObjectGroupFlags(cx, obj, OBJECT_FLAG_FROZEN_ELEMENTS);
     obj->getElementsHeader()->freeze();
 
     return true;
 }
 
 #ifdef DEBUG
+static mozilla::Atomic<bool, mozilla::Relaxed> gShapeConsistencyChecksEnabled(false);
+
+/* static */ void
+js::NativeObject::enableShapeConsistencyChecks()
+{
+    gShapeConsistencyChecksEnabled = true;
+}
+
 void
 js::NativeObject::checkShapeConsistency()
 {
-    static int throttle = -1;
-    if (throttle < 0) {
-        if (const char* var = getenv("JS_CHECK_SHAPE_THROTTLE"))
-            throttle = atoi(var);
-        if (throttle < 0)
-            throttle = 0;
-    }
-    if (throttle == 0)
+    if (!gShapeConsistencyChecksEnabled)
         return;
 
     MOZ_ASSERT(isNative());
 
     Shape* shape = lastProperty();
     Shape* prev = nullptr;
 
     AutoCheckCannotGC nogc;
@@ -148,51 +149,55 @@ js::NativeObject::checkShapeConsistency(
         if (ShapeTable* table = shape->maybeTable(nogc)) {
             for (uint32_t fslot = table->freeList();
                  fslot != SHAPE_INVALID_SLOT;
                  fslot = getSlot(fslot).toPrivateUint32())
             {
                 MOZ_ASSERT(fslot < slotSpan());
             }
 
-            for (int n = throttle; --n >= 0 && shape->parent; shape = shape->parent) {
+            while (shape->parent) {
                 MOZ_ASSERT_IF(lastProperty() != shape, !shape->hasTable());
 
                 ShapeTable::Entry& entry = table->search<MaybeAdding::NotAdding>(shape->propid(),
                                                                                  nogc);
                 MOZ_ASSERT(entry.shape() == shape);
+                shape = shape->parent;
             }
         }
 
         shape = lastProperty();
-        for (int n = throttle; --n >= 0 && shape; shape = shape->parent) {
-            MOZ_ASSERT_IF(shape->slot() != SHAPE_INVALID_SLOT, shape->slot() < slotSpan());
+        while (shape) {
+            MOZ_ASSERT_IF(!shape->isEmptyShape() && shape->isDataProperty(),
+                          shape->slot() < slotSpan());
             if (!prev) {
                 MOZ_ASSERT(lastProperty() == shape);
                 MOZ_ASSERT(shape->listp == &shape_);
             } else {
                 MOZ_ASSERT(shape->listp == &prev->parent);
             }
             prev = shape;
+            shape = shape->parent;
         }
     } else {
-        for (int n = throttle; --n >= 0 && shape->parent; shape = shape->parent) {
+        while (shape->parent) {
             if (ShapeTable* table = shape->maybeTable(nogc)) {
                 MOZ_ASSERT(shape->parent);
                 for (Shape::Range<NoGC> r(shape); !r.empty(); r.popFront()) {
                     ShapeTable::Entry& entry =
                         table->search<MaybeAdding::NotAdding>(r.front().propid(), nogc);
                     MOZ_ASSERT(entry.shape() == &r.front());
                 }
             }
             if (prev) {
-                MOZ_ASSERT(prev->maybeSlot() >= shape->maybeSlot());
+                MOZ_ASSERT_IF(shape->isDataProperty(), prev->maybeSlot() >= shape->maybeSlot());
                 shape->kids.checkConsistency(prev);
             }
             prev = shape;
+            shape = shape->parent;
         }
     }
 }
 #endif
 
 void
 NativeObject::updateShapeAfterMovingGC()
 {
diff --git a/js/src/vm/NativeObject.h b/js/src/vm/NativeObject.h
--- a/js/src/vm/NativeObject.h
+++ b/js/src/vm/NativeObject.h
@@ -545,16 +545,20 @@ class NativeObject : public ShapedObject
 
     static inline JS::Result<NativeObject*, JS::OOM&>
     create(JSContext* cx, js::gc::AllocKind kind, js::gc::InitialHeap heap,
            js::HandleShape shape, js::HandleObjectGroup group);
 
     static inline JS::Result<NativeObject*, JS::OOM&>
     createWithTemplate(JSContext* cx, js::gc::InitialHeap heap, HandleObject templateObject);
 
+#ifdef DEBUG
+    static void enableShapeConsistencyChecks();
+#endif
+
   protected:
 #ifdef DEBUG
     friend class js::AutoCheckShapeConsistency;
     void checkShapeConsistency();
 #else
     void checkShapeConsistency() { }
 #endif
 
