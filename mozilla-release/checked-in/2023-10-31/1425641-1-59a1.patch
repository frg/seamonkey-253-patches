# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1512555164 0
# Node ID 35763c155792b11d019bb5e24eb766b235091bd1
# Parent  763ee2e567d7461ba85210b74c0c8c4e5b68ca74
Bug 1425641, part 1 - Collect nsPrintJob members and remove some unneeded methods. r=bobowen

MozReview-Commit-ID: 8Z64aE5f61j

diff --git a/layout/printing/nsPrintJob.cpp b/layout/printing/nsPrintJob.cpp
--- a/layout/printing/nsPrintJob.cpp
+++ b/layout/printing/nsPrintJob.cpp
@@ -205,33 +205,16 @@ public:
 protected:
   RefPtr<nsPrintJob>      mPrintJob;
   bool                    mSuppressed;
 };
 
 NS_IMPL_ISUPPORTS(nsPrintJob, nsIWebProgressListener,
                   nsISupportsWeakReference, nsIObserver)
 
-//---------------------------------------------------
-//-- nsPrintJob Class Impl
-//---------------------------------------------------
-nsPrintJob::nsPrintJob()
-  : mIsCreatingPrintPreview(false)
-  , mIsDoingPrinting(false)
-  , mIsDoingPrintPreview(false)
-  , mProgressDialogIsShown(false)
-  , mScreenDPI(115.0f)
-  , mPagePrintTimer(nullptr)
-  , mLoadCounter(0)
-  , mDidLoadDataForPrinting(false)
-  , mIsDestroying(false)
-  , mDisallowSelectionPrint(false)
-{
-}
-
 //-------------------------------------------------------
 nsPrintJob::~nsPrintJob()
 {
   Destroy(); // for insurance
   DisconnectPagePrintTimer();
 }
 
 //-------------------------------------------------------
@@ -401,17 +384,17 @@ nsPrintJob::CommonPrint(bool            
   // alive for the duration of this method, because our main owning reference
   // (on nsDocumentViewer) might be cleared during this function (if we cause
   // script to run and it cancels the print operation).
 
   nsresult rv = DoCommonPrint(aIsPrintPreview, aPrintSettings,
                               aWebProgressListener, aDoc);
   if (NS_FAILED(rv)) {
     if (aIsPrintPreview) {
-      SetIsCreatingPrintPreview(false);
+      mIsCreatingPrintPreview = false;
       SetIsPrintPreview(false);
     } else {
       SetIsPrinting(false);
     }
     if (mProgressDialogIsShown)
       CloseProgressDialog(aWebProgressListener);
     if (rv != NS_ERROR_ABORT && rv != NS_ERROR_OUT_OF_MEMORY) {
       FirePrintingErrorEvent(rv);
@@ -458,17 +441,17 @@ nsPrintJob::DoCommonPrint(bool          
 
   rv = CheckForPrinters(printData->mPrintSettings);
   NS_ENSURE_SUCCESS(rv, rv);
 
   printData->mPrintSettings->SetIsCancelled(false);
   printData->mPrintSettings->GetShrinkToFit(&printData->mShrinkToFit);
 
   if (aIsPrintPreview) {
-    SetIsCreatingPrintPreview(true);
+    mIsCreatingPrintPreview = true;
     SetIsPrintPreview(true);
     nsCOMPtr<nsIContentViewer> viewer =
       do_QueryInterface(mDocViewerPrint);
     if (viewer) {
       viewer->SetTextZoom(1.0f);
       viewer->SetFullZoom(1.0f);
       viewer->SetMinFontSize(0);
     }
@@ -543,17 +526,17 @@ nsPrintJob::DoCommonPrint(bool          
     BuildDocTree(printData->mPrintObject->mDocShell, &printData->mPrintDocList,
                  printData->mPrintObject);
   }
 
   // The nsAutoScriptBlocker above will now have been destroyed, which may
   // cause our print/print-preview operation to finish. In this case, we
   // should immediately return an error code so that the root caller knows
   // it shouldn't continue to do anything with this instance.
-  if (mIsDestroying || (aIsPrintPreview && !GetIsCreatingPrintPreview())) {
+  if (mIsDestroying || (aIsPrintPreview && !mIsCreatingPrintPreview)) {
     return NS_ERROR_FAILURE;
   }
 
   if (!aIsPrintPreview) {
     SetIsPrinting(true);
   }
 
   // XXX This isn't really correct...
@@ -1537,17 +1520,17 @@ nsPrintJob::CleanupOnFailure(nsresult aR
     mPagePrintTimer->Stop();
     DisconnectPagePrintTimer();
   }
 
   if (aIsPrinting) {
     SetIsPrinting(false);
   } else {
     SetIsPrintPreview(false);
-    SetIsCreatingPrintPreview(false);
+    mIsCreatingPrintPreview = false;
   }
 
   /* cleanup done, let's fire-up an error dialog to notify the user
    * what went wrong...
    *
    * When rv == NS_ERROR_ABORT, it means we want out of the
    * print job without displaying any error messages
    */
@@ -3448,21 +3431,21 @@ nsPrintJob::FinishPrintPreview()
     return rv;
   }
 
   rv = DocumentReadyForPrinting();
 
   // Note that this method may be called while the instance is being
   // initialized.  Some methods which initialize the instance (e.g.,
   // DoCommonPrint) may need to stop initializing and return error if
-  // this is called.  Therefore it's important to set IsCreatingPrintPreview
-  // state to false here.  If you need to remove this call of
-  // SetIsCreatingPrintPreview here, you need to keep them being able to
-  // check whether the owner stopped using this instance.
-  SetIsCreatingPrintPreview(false);
+  // this is called.  Therefore it's important to set mIsCreatingPrintPreview
+  // state to false here.  If you need to stop setting that here, you need to
+  // keep them being able to check whether the owner stopped using this
+  // instance.
+  mIsCreatingPrintPreview = false;
 
   // mPrt may be cleared during a call of nsPrintData::OnEndPrinting()
   // because that method invokes some arbitrary listeners.
   RefPtr<nsPrintData> printData = mPrt;
   if (NS_FAILED(rv)) {
     /* cleanup done, let's fire-up an error dialog to notify the user
      * what went wrong...
      */
diff --git a/layout/printing/nsPrintJob.h b/layout/printing/nsPrintJob.h
--- a/layout/printing/nsPrintJob.h
+++ b/layout/printing/nsPrintJob.h
@@ -36,16 +36,18 @@ class nsIPageSequenceFrame;
  * A print job may be instantiated either for printing to an actual physical
  * printer, or for creating a print preview.
  */
 class nsPrintJob final : public nsIObserver
                        , public nsIWebProgressListener
                        , public nsSupportsWeakReference
 {
 public:
+  nsPrintJob() = default;
+
   // nsISupports interface...
   NS_DECL_ISUPPORTS
 
   // nsIObserver
   NS_DECL_NSIOBSERVER
 
   NS_DECL_NSIWEBPROGRESSLISTENER
 
@@ -69,18 +71,16 @@ public:
 
   // This enum tells indicates what the default should be for the title
   // if the title from the document is null
   enum eDocTitleDefault {
     eDocTitleDefBlank,
     eDocTitleDefURLDoc
   };
 
-  nsPrintJob();
-
   void Destroy();
   void DestroyPrintingData();
 
   nsresult Initialize(nsIDocumentViewerPrint* aDocViewerPrint,
                       nsIDocShell*            aContainer,
                       nsIDocument*            aDocument,
                       float                   aScreenDPI);
 
@@ -184,20 +184,16 @@ public:
   {
     return mIsDoingPrinting;
   }
   void SetIsPrintPreview(bool aIsPrintPreview);
   bool GetIsPrintPreview()
   {
     return mIsDoingPrintPreview;
   }
-  void SetIsCreatingPrintPreview(bool aIsCreatingPrintPreview)
-  {
-    mIsCreatingPrintPreview = aIsCreatingPrintPreview;
-  }
   bool GetIsCreatingPrintPreview()
   {
     return mIsCreatingPrintPreview;
   }
 
   void SetDisallowSelectionPrint(bool aDisallowSelectionPrint)
   {
     mDisallowSelectionPrint = aDisallowSelectionPrint;
@@ -244,41 +240,41 @@ private:
   nsresult UpdateSelectionAndShrinkPrintObject(nsPrintObject* aPO,
                                                bool aDocumentIsTopLevel);
   nsresult InitPrintDocConstruction(bool aHandleError);
   void FirePrintPreviewUpdateEvent();
 
   void PageDone(nsresult aResult);
 
 
-  bool mIsCreatingPrintPreview;
-  bool mIsDoingPrinting;
-  bool mIsDoingPrintPreview;
-  bool mProgressDialogIsShown;
+  nsCOMPtr<nsIDocument> mDocument;
+  nsCOMPtr<nsIDocumentViewerPrint> mDocViewerPrint;
 
-  nsCOMPtr<nsIDocumentViewerPrint> mDocViewerPrint;
-  nsWeakPtr               mContainer;
-  float                   mScreenDPI;
+  nsWeakPtr mContainer;
+  WeakFrame mPageSeqFrame;
 
   // We are the primary owner of our nsPrintData member vars.  These vars
   // are refcounted so that functions (e.g. nsPrintData methods) can create
   // temporary owning references when they need to fire a callback that
   // could conceivably destroy this nsPrintJob owner object and all its
   // member-data.
   RefPtr<nsPrintData> mPrt;
 
-  nsPagePrintTimer*       mPagePrintTimer;
-  WeakFrame               mPageSeqFrame;
-
   // Print Preview
   RefPtr<nsPrintData> mPrtPreview;
   RefPtr<nsPrintData> mOldPrtPreview;
 
-  nsCOMPtr<nsIDocument>   mDocument;
+  nsPagePrintTimer* mPagePrintTimer = nullptr;
+
+  float mScreenDPI = 115.0f;
+  int32_t mLoadCounter = 0;
 
-  int32_t mLoadCounter;
-  bool mDidLoadDataForPrinting;
-  bool mIsDestroying;
-  bool mDisallowSelectionPrint;
+  bool mIsCreatingPrintPreview = false;
+  bool mIsDoingPrinting = false;
+  bool mIsDoingPrintPreview = false;
+  bool mProgressDialogIsShown = false;
+  bool mDidLoadDataForPrinting = false;
+  bool mIsDestroying = false;
+  bool mDisallowSelectionPrint = false;
 };
 
 #endif // nsPrintJob_h
 
