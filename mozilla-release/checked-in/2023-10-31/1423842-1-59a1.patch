# HG changeset patch
# User Byron Campen [:bwc] <docfaraday@gmail.com>
# Date 1513622005 21600
#      Mon Dec 18 12:33:25 2017 -0600
# Node ID 17a32b697729c8a2f622f074fe6420e6ceba56a2
# Parent  3b489b994f6d1f88c09a68c3032b19cf20b8d162
Bug 1423842 - Part 1: Test-case: onaddstream should fire after tracks are added, but before SRD resolves. r=jib

MozReview-Commit-ID: FOL1ueOR2xF

diff --git a/dom/media/tests/mochitest/test_peerConnection_transceivers.html b/dom/media/tests/mochitest/test_peerConnection_transceivers.html
--- a/dom/media/tests/mochitest/test_peerConnection_transceivers.html
+++ b/dom/media/tests/mochitest/test_peerConnection_transceivers.html
@@ -19,34 +19,54 @@
       is(e.name, exceptionName, description + " throws " + exceptionName);
     }
   };
 
   let stopTracks = (...streams) => {
     streams.forEach(stream => stream.getTracks().forEach(track => track.stop()));
   };
 
-  let setRemoteDescriptionReturnTrackEvents = async (pc, desc) => {
-    let trackEvents = [];
-    let listener = e => trackEvents.push(e);
-    pc.addEventListener("track", listener);
-    await pc.setRemoteDescription(desc);
-    pc.removeEventListener("track", listener);
+  let collectEvents = (target, name, check) => {
+    let events = [];
+    let handler = e => {
+      check(e);
+      events.push(e);
+    };
+
+    target.addEventListener(name, handler);
 
-    // basic sanity-check, simplifies testing elsewhere
-    for (let e of trackEvents) {
+    let finishCollecting = () => {
+      target.removeEventListener(name, handler);
+      return events;
+    };
+
+    return {finish: finishCollecting};
+  };
+
+  let collectTrackEvents = pc => {
+    let checkEvent = e => {
       ok(e.track, "Track is set on event");
       ok(e.receiver, "Receiver is set on event");
       ok(e.transceiver, "Transceiver is set on event");
       ok(e.streams, "Streams is set on event");
+      e.streams.forEach(stream => {
+        ok(stream.getTracks().includes(e.track),
+           "Each stream in event contains the track");
+      });
       is(e.receiver, e.transceiver.receiver, "Receiver belongs to transceiver");
       is(e.track, e.receiver.track, "Track belongs to receiver");
-    }
+    };
+
+    return collectEvents(pc, "track", checkEvent);
+  };
 
-    return trackEvents;
+  let setRemoteDescriptionReturnTrackEvents = async (pc, desc) => {
+    let trackEventCollector = collectTrackEvents(pc);
+    await pc.setRemoteDescription(desc);
+    return trackEventCollector.finish();
   };
 
   let offerAnswer = async (offerer, answerer) => {
     let offer = await offerer.createOffer();
     await answerer.setRemoteDescription(offer);
     await offerer.setLocalDescription(offer);
     let answer = await answerer.createAnswer();
     await offerer.setRemoteDescription(answer);
@@ -395,16 +415,17 @@
     hasProps(trackEvents,
       [
         {
           track: pc2.getTransceivers()[0].receiver.track,
           streams: [{id: stream.id}]
         }
       ]);
 
+
     hasProps(pc2.getTransceivers(),
       [
         {
           receiver: {track: {kind: "audio"}},
           sender: {track: null},
           direction: "recvonly",
           mid: "sdparta_0", // Firefox-specific
           currentDirection: null,
@@ -1023,16 +1044,81 @@
     is(2, countUnmuteVideo2.count, "Got 2 unmute events for pc2's video track");
 
     pc1.close();
     pc2.close();
     stopTracks(stream1);
     stopTracks(stream2);
   };
 
+  let checkOnAddStream = async () => {
+    let pc1 = new RTCPeerConnection();
+    let stream1 = await getUserMedia({audio: true, video: true});
+    let audio1 = stream1.getAudioTracks()[0];
+    pc1.addTrack(audio1, stream1);
+    let video1 = stream1.getVideoTracks()[0];
+    pc1.addTrack(video1, stream1);
+
+    let pc2 = new RTCPeerConnection();
+    let stream2 = await getUserMedia({audio: true, video: true});
+    let audio2 = stream2.getAudioTracks()[0];
+    pc2.addTrack(audio2, stream2);
+    let video2 = stream2.getVideoTracks()[0];
+    pc2.addTrack(video2, stream2);
+
+    let offer = await pc1.createOffer();
+
+    let trackEventCollector = collectTrackEvents(pc2);
+    let addstreamEventCollector = collectEvents(pc2, "addstream", e => {
+      hasProps(e, {stream: {id: stream1.id}});
+      is(e.stream.getAudioTracks().length, 1, "One audio track");
+      is(e.stream.getVideoTracks().length, 1, "One video track");
+    });
+
+    await pc2.setRemoteDescription(offer);
+
+    let addstreamEvents = addstreamEventCollector.finish();
+    is(addstreamEvents.length, 1, "Should have 1 addstream event");
+
+    let trackEvents = trackEventCollector.finish();
+
+    hasProps(trackEvents,
+      [
+        {streams: [addstreamEvents[0].stream]},
+        {streams: [addstreamEvents[0].stream]}
+      ]);
+
+    await pc1.setLocalDescription(offer);
+    let answer = await pc2.createAnswer();
+
+    trackEventCollector = collectTrackEvents(pc1);
+    addstreamEventCollector = collectEvents(pc1, "addstream", e => {
+      hasProps(e, {stream: {id: stream2.id}});
+      is(e.stream.getAudioTracks().length, 1, "One audio track");
+      is(e.stream.getVideoTracks().length, 1, "One video track");
+    });
+
+    await pc1.setRemoteDescription(answer);
+    addstreamEvents = addstreamEventCollector.finish();
+    is(addstreamEvents.length, 1, "Should have 1 addstream event");
+
+    trackEvents = trackEventCollector.finish();
+
+    hasProps(trackEvents,
+      [
+        {streams: [addstreamEvents[0].stream]},
+        {streams: [addstreamEvents[0].stream]}
+      ]);
+
+    pc1.close();
+    pc2.close();
+    stopTracks(stream1);
+    stopTracks(stream2);
+  };
+
   let checkStop = async () => {
     let pc1 = new RTCPeerConnection();
     let stream = await getUserMedia({audio: true});
     let track = stream.getAudioTracks()[0];
     pc1.addTrack(track, stream);
 
     let offer = await pc1.createOffer();
     await pc1.setLocalDescription(offer);
@@ -1936,16 +2022,17 @@
     await checkAddTransceiverNoTrackDoesntPair();
     await checkAddTransceiverWithTrackDoesntPair();
     await checkAddTransceiverThenReplaceTrackDoesntPair();
     await checkAddTransceiverThenAddTrackPairs();
     await checkAddTrackPairs();
     await checkReplaceTrackNullDoesntPreventPairing();
     await checkRemoveAndReadd();
     await checkMute();
+    await checkOnAddStream();
     await checkStop();
     await checkStopAfterCreateOffer();
     await checkStopAfterSetLocalOffer();
     await checkStopAfterSetRemoteOffer();
     await checkStopAfterCreateAnswer();
     await checkStopAfterSetLocalAnswer();
     await checkStopAfterClose();
     await checkLocalRollback();
