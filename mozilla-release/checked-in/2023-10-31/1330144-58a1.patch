# HG changeset patch
# User Neil Deakin <neil@mozilla.com>
# Date 1507129805 14400
#      Wed Oct 04 11:10:05 2017 -0400
# Node ID 0c6417e737ff1d6aa9f5ac4850876eb370a08ad6
# Parent  087cf7df007909e55075b55918f7168211e88c87
Bug 1330144, don't focus 'noinitialfocus' elements at all in dialogs, r=gijs

diff --git a/toolkit/content/tests/chrome/chrome.ini b/toolkit/content/tests/chrome/chrome.ini
--- a/toolkit/content/tests/chrome/chrome.ini
+++ b/toolkit/content/tests/chrome/chrome.ini
@@ -8,16 +8,17 @@ support-files =
   bug304188_window.xul
   bug331215_window.xul
   bug360437_window.xul
   bug366992_window.xul
   bug409624_window.xul
   bug429723_window.xul
   bug624329_window.xul
   dialog_dialogfocus.xul
+  dialog_dialogfocus2.xul
   file_about_networking_wsh.py
   file_autocomplete_with_composition.js
   findbar_entireword_window.xul
   findbar_events_window.xul
   findbar_window.xul
   frame_popup_anchor.xul
   frame_popupremoving_frame.xul
   frame_subframe_origin_subframe1.xul
diff --git a/toolkit/content/tests/chrome/dialog_dialogfocus2.xul b/toolkit/content/tests/chrome/dialog_dialogfocus2.xul
new file mode 100644
--- /dev/null
+++ b/toolkit/content/tests/chrome/dialog_dialogfocus2.xul
@@ -0,0 +1,6 @@
+<?xml-stylesheet href="chrome://global/skin" type="text/css"?>
+
+<dialog id="root" buttons="none" xmlns="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul">
+  <button id="nonbutton" noinitialfocus="true"/>
+</dialog>
+
diff --git a/toolkit/content/tests/chrome/test_dialogfocus.xul b/toolkit/content/tests/chrome/test_dialogfocus.xul
--- a/toolkit/content/tests/chrome/test_dialogfocus.xul
+++ b/toolkit/content/tests/chrome/test_dialogfocus.xul
@@ -21,22 +21,26 @@
 </body>
 
 <script>
 <![CDATA[
 
 SimpleTest.waitForExplicitFinish();
 SimpleTest.requestCompleteLog();
 
-var expected = [ "one", "_extra2", "tab", "one", "tabbutton2", "tabbutton", "two", "textbox-yes", "one" ];
+var expected = [ "one", "_extra2", "tab", "one", "tabbutton2", "tabbutton", "two", "textbox-yes", "one", "root" ];
 // non-Mac will always focus the default button if any of the dialog buttons
 // would be focused
 if (!navigator.platform.includes("Mac"))
   expected[1] = "_accept";
 
+let extraDialog = "data:application/vnd.mozilla.xul+xml,<dialog id='root' buttons='none' " +
+                  "xmlns='http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul'>" +
+                  "<button id='nonbutton' noinitialfocus='true'/></dialog>";
+
 var step = 0;
 var fullKeyboardAccess = false;
 
 function startTest()
 {
   var testButton = document.getElementById("test");
   synthesizeKey("VK_TAB", { });
   fullKeyboardAccess = (document.activeElement == testButton);
@@ -50,17 +54,18 @@ function runTest()
   info("runTest(), step = " + step + ", expected = " + expected[step - 1]);
   if (step > expected.length || (!fullKeyboardAccess && step == 2)) {
     info("finishing");
     SimpleTest.finish();
     return;
   }
 
   var expectedFocus = expected[step - 1];
-  var win = window.openDialog("dialog_dialogfocus.xul", "_new", "chrome,dialog", step);
+  let filename = expectedFocus == "root" ? "dialog_dialogfocus2.xul" : "dialog_dialogfocus.xul";
+  var win = window.openDialog(filename, "_new", "chrome,dialog", step);
 
   function checkDialogFocus(event)
   {
     info("checkDialogFocus()");
     // if full keyboard access is not on, just skip the tests
     var match = false;
     if (fullKeyboardAccess) {
       if (!(event.target instanceof Element)) {
@@ -78,24 +83,40 @@ function runTest()
       if (!match)
         return;
     }
     else {
       match = (win.document.activeElement == win.document.documentElement);
       info("match = " + match);
     }
 
-    win.removeEventListener("focus", checkDialogFocus, true);
+    win.removeEventListener("focus", checkDialogFocusEvent, true);
     ok(match, "focus step " + step);
 
     win.close();
     SimpleTest.waitForFocus(runTest, window);
   }
 
-  win.addEventListener("focus", checkDialogFocus, true);
+  function checkDialogFocusRoot(event) {
+    if (event.target == win) {
+      is(win.document.activeElement, win.document.documentElement, "No other focus but root");
+      win.close();
+      SimpleTest.waitForFocus(runTest, window);
+    }
+  }
+
+  function checkDialogFocusEvent(event) {
+    // Delay to have time for focus/blur to occur.
+    if (expectedFocus == "root") {
+      setTimeout(checkDialogFocusRoot, 0, event);
+    } else {
+      checkDialogFocus(event);
+    }
+  }
+  win.addEventListener("focus", checkDialogFocusEvent, true);
 }
 
 SimpleTest.waitForFocus(startTest, window);
 
 ]]>
 
 </script>
 
diff --git a/toolkit/content/widgets/dialog.xml b/toolkit/content/widgets/dialog.xml
--- a/toolkit/content/widgets/dialog.xml
+++ b/toolkit/content/widgets/dialog.xml
@@ -171,18 +171,22 @@
 
               var focusedElt = document.commandDispatcher.focusedElement;
               if (focusedElt) {
                 var initialFocusedElt = focusedElt;
                 while (focusedElt.localName == "tab" ||
                        focusedElt.getAttribute("noinitialfocus") == "true") {
                   document.commandDispatcher.advanceFocusIntoSubtree(focusedElt);
                   focusedElt = document.commandDispatcher.focusedElement;
-                  if (focusedElt == initialFocusedElt)
+                  if (focusedElt == initialFocusedElt) {
+                    if (focusedElt.getAttribute("noinitialfocus") == "true") {
+                      focusedElt.blur();
+                    }
                     break;
+                  }
                 }
 
                 if (initialFocusedElt.localName == "tab") {
                   if (focusedElt.hasAttribute("dlgtype")) {
                     // We don't want to focus on anonymous OK, Cancel, etc. buttons,
                     // so return focus to the tab itself
                     initialFocusedElt.focus();
                   }
