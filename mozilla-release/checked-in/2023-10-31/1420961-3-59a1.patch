# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1511952101 -3600
#      Wed Nov 29 11:41:41 2017 +0100
# Node ID bb14fe85b1a9486b533f91fd4c818bce51df2b99
# Parent  3a517f2169d7afb883aeca044fbf57fa70b22c2f
Bug 1420961 part 3 - Remove JSITER_ENUMERATE and flags arguments. r=evilpie

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -7389,21 +7389,19 @@ BytecodeEmitter::emitForIn(ParseNode* fo
         }
     }
 
     // Evaluate the expression being iterated.
     ParseNode* expr = forInHead->pn_kid3;
     if (!emitTreeInBranch(expr))                          // EXPR
         return false;
 
-    // Convert the value to the appropriate sort of iterator object for the
-    // loop variant (for-in, for-each-in, or destructuring for-in).
-    unsigned iflags = forInLoop->pn_iflags;
-    MOZ_ASSERT(0 == (iflags & ~JSITER_ENUMERATE));
-    if (!emit2(JSOP_ITER, AssertedCast<uint8_t>(iflags))) // ITER
+    MOZ_ASSERT(forInLoop->pn_iflags == 0);
+
+    if (!emit1(JSOP_ITER))                                // ITER
         return false;
 
     // For-in loops have both the iterator and the value on the stack. Push
     // undefined to balance the stack.
     if (!emit1(JSOP_UNDEFINED))                           // ITER ITERVAL
         return false;
 
     LoopControl loopInfo(this, StatementKind::ForInLoop);
@@ -7447,20 +7445,18 @@ BytecodeEmitter::emitForIn(ParseNode* fo
     }
 
     {
 #ifdef DEBUG
         auto loopDepth = this->stackDepth;
 #endif
         MOZ_ASSERT(loopDepth >= 2);
 
-        if (iflags == JSITER_ENUMERATE) {
-            if (!emit1(JSOP_ITERNEXT))                    // ITER ITERVAL
-                return false;
-        }
+        if (!emit1(JSOP_ITERNEXT))                        // ITER ITERVAL
+            return false;
 
         if (!emitInitializeForInOrOfTarget(forInHead))    // ITER ITERVAL
             return false;
 
         MOZ_ASSERT(this->stackDepth == loopDepth,
                    "iterator and iterval must be left on the stack");
     }
 
diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -6349,17 +6349,16 @@ Parser<ParseHandler, CharT>::forStatemen
         // |target| is the LeftHandSideExpression or declaration to which the
         // per-iteration value (an arbitrary value exposed by the iteration
         // protocol, or a string naming a property) is assigned.
         Node target = startNode;
 
         // Parse the rest of the for-in/of head.
         if (headKind == ParseNodeKind::ForIn) {
             stmt.refineForKind(StatementKind::ForInLoop);
-            iflags |= JSITER_ENUMERATE;
         } else {
             stmt.refineForKind(StatementKind::ForOfLoop);
         }
 
         // Parser::declaration consumed everything up to the closing ')'.  That
         // token follows an {Assignment,}Expression and so must be interpreted
         // as an operand to be consistent with normal expression tokenizing.
         MUST_MATCH_TOKEN_MOD(TokenKind::Rp, TokenStream::Operand, JSMSG_PAREN_AFTER_FOR_CTRL);
diff --git a/js/src/jit/BaselineIC.cpp b/js/src/jit/BaselineIC.cpp
--- a/js/src/jit/BaselineIC.cpp
+++ b/js/src/jit/BaselineIC.cpp
@@ -4117,17 +4117,16 @@ ICTableSwitch::fixupJumpTable(JSScript* 
 //
 // GetIterator_Fallback
 //
 
 static bool
 DoGetIteratorFallback(JSContext* cx, BaselineFrame* frame, ICGetIterator_Fallback* stub,
                       HandleValue value, MutableHandleValue res)
 {
-    jsbytecode* pc = stub->icEntry()->pc(frame->script());
     FallbackICSpew(cx, stub, "GetIterator");
 
     if (stub->state().maybeTransition())
         stub->discardStubs(cx);
 
     if (stub->state().canAttachStub()) {
         RootedScript script(cx, frame->script());
         jsbytecode* pc = stub->icEntry()->pc(script);
@@ -4141,18 +4140,17 @@ DoGetIteratorFallback(JSContext* cx, Bas
                                                         engine, script, stub, &attached);
             if (newStub)
                 JitSpew(JitSpew_BaselineIC, "  Attached CacheIR stub");
         }
         if (!attached)
             stub->state().trackNotAttached();
     }
 
-    uint8_t flags = GET_UINT8(pc);
-    JSObject* iterobj = ValueToIterator(cx, flags, value);
+    JSObject* iterobj = ValueToIterator(cx, value);
     if (!iterobj)
         return false;
 
     res.setObject(*iterobj);
     return true;
 }
 
 typedef bool (*DoGetIteratorFallbackFn)(JSContext*, BaselineFrame*, ICGetIterator_Fallback*,
diff --git a/js/src/jit/CacheIR.cpp b/js/src/jit/CacheIR.cpp
--- a/js/src/jit/CacheIR.cpp
+++ b/js/src/jit/CacheIR.cpp
@@ -4156,19 +4156,16 @@ GetIteratorIRGenerator::tryAttachStub()
     return false;
 }
 
 bool
 GetIteratorIRGenerator::tryAttachNativeIterator(ObjOperandId objId, HandleObject obj)
 {
     MOZ_ASSERT(JSOp(*pc_) == JSOP_ITER);
 
-    if (GET_UINT8(pc_) != JSITER_ENUMERATE)
-        return false;
-
     PropertyIteratorObject* iterobj = LookupInIteratorCache(cx_, obj);
     if (!iterobj)
         return false;
 
     MOZ_ASSERT(obj->isNative() || obj->is<UnboxedPlainObject>());
 
     // Guard on the receiver's shape/group.
     Maybe<ObjOperandId> expandoId;
diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -9290,19 +9290,16 @@ CodeGenerator::visitIteratorEnd(LIterato
     const Register temp1 = ToRegister(lir->temp1());
     const Register temp2 = ToRegister(lir->temp2());
     const Register temp3 = ToRegister(lir->temp3());
 
     OutOfLineCode* ool = oolCallVM(CloseIteratorFromIonInfo, lir, ArgList(obj), StoreNothing());
 
     LoadNativeIterator(masm, obj, temp1, ool->entry());
 
-    masm.branchTest32(Assembler::Zero, Address(temp1, offsetof(NativeIterator, flags)),
-                      Imm32(JSITER_ENUMERATE), ool->entry());
-
     // Clear active bit.
     masm.and32(Imm32(~JSITER_ACTIVE), Address(temp1, offsetof(NativeIterator, flags)));
 
     // Reset property cursor.
     masm.loadPtr(Address(temp1, offsetof(NativeIterator, props_array)), temp2);
     masm.storePtr(temp2, Address(temp1, offsetof(NativeIterator, props_cursor)));
 
     // Unlink from the iterator list.
diff --git a/js/src/jit/IonBuilder.cpp b/js/src/jit/IonBuilder.cpp
--- a/js/src/jit/IonBuilder.cpp
+++ b/js/src/jit/IonBuilder.cpp
@@ -2300,17 +2300,17 @@ IonBuilder::inspectOpcode(JSOp op)
 
       case JSOP_FRESHENLEXICALENV:
         return jsop_copylexicalenv(true);
 
       case JSOP_RECREATELEXICALENV:
         return jsop_copylexicalenv(false);
 
       case JSOP_ITER:
-        return jsop_iter(GET_INT8(pc));
+        return jsop_iter();
 
       case JSOP_MOREITER:
         return jsop_itermore();
 
       case JSOP_ISNOITER:
         return jsop_isnoiter();
 
       case JSOP_ENDITER:
@@ -12589,17 +12589,17 @@ IonBuilder::jsop_toid()
 
     current->add(ins);
     current->push(ins);
 
     return resumeAfter(ins);
 }
 
 AbortReasonOr<Ok>
-IonBuilder::jsop_iter(uint8_t flags)
+IonBuilder::jsop_iter()
 {
     MDefinition* obj = current->pop();
     MInstruction* ins = MGetIteratorCache::New(alloc(), obj);
 
     if (!outermostBuilder()->iterators_.append(ins))
         return abort(AbortReason::Alloc);
 
     current->add(ins);
diff --git a/js/src/jit/IonBuilder.h b/js/src/jit/IonBuilder.h
--- a/js/src/jit/IonBuilder.h
+++ b/js/src/jit/IonBuilder.h
@@ -572,17 +572,17 @@ class IonBuilder
     AbortReasonOr<Ok> jsop_copylexicalenv(bool copySlots);
     AbortReasonOr<Ok> jsop_functionthis();
     AbortReasonOr<Ok> jsop_globalthis();
     AbortReasonOr<Ok> jsop_typeof();
     AbortReasonOr<Ok> jsop_toasync();
     AbortReasonOr<Ok> jsop_toasyncgen();
     AbortReasonOr<Ok> jsop_toasynciter();
     AbortReasonOr<Ok> jsop_toid();
-    AbortReasonOr<Ok> jsop_iter(uint8_t flags);
+    AbortReasonOr<Ok> jsop_iter();
     AbortReasonOr<Ok> jsop_itermore();
     AbortReasonOr<Ok> jsop_isnoiter();
     AbortReasonOr<Ok> jsop_iterend();
     AbortReasonOr<Ok> jsop_iternext();
     AbortReasonOr<Ok> jsop_in();
     AbortReasonOr<Ok> jsop_hasown();
     AbortReasonOr<Ok> jsop_instanceof();
     AbortReasonOr<Ok> jsop_getaliasedvar(EnvironmentCoordinate ec);
diff --git a/js/src/jit/IonIC.cpp b/js/src/jit/IonIC.cpp
--- a/js/src/jit/IonIC.cpp
+++ b/js/src/jit/IonIC.cpp
@@ -406,34 +406,32 @@ IonBindNameIC::update(JSContext* cx, Han
     return holder;
 }
 
 /* static */ JSObject*
 IonGetIteratorIC::update(JSContext* cx, HandleScript outerScript, IonGetIteratorIC* ic,
                          HandleValue value)
 {
     IonScript* ionScript = outerScript->ionScript();
-    jsbytecode* pc = ic->pc();
 
     if (ic->state().maybeTransition())
         ic->discardStubs(cx->zone());
 
     if (ic->state().canAttachStub()) {
         bool attached = false;
         RootedScript script(cx, ic->script());
-        GetIteratorIRGenerator gen(cx, script, pc, ic->state().mode(), value);
+        GetIteratorIRGenerator gen(cx, script, ic->pc(), ic->state().mode(), value);
         if (gen.tryAttachStub())
             ic->attachCacheIRStub(cx, gen.writerRef(), gen.cacheKind(), ionScript, &attached);
 
         if (!attached)
             ic->state().trackNotAttached();
     }
 
-    uint8_t flags = GET_UINT8(pc);
-    return ValueToIterator(cx, flags, value);
+    return ValueToIterator(cx, value);
 }
 
 /* static */ bool
 IonHasOwnIC::update(JSContext* cx, HandleScript outerScript, IonHasOwnIC* ic,
                     HandleValue val, HandleValue idVal, int32_t* res)
 {
     IonScript* ionScript = outerScript->ionScript();
 
diff --git a/js/src/jsfriendapi.h b/js/src/jsfriendapi.h
--- a/js/src/jsfriendapi.h
+++ b/js/src/jsfriendapi.h
@@ -1005,19 +1005,18 @@ StringIsArrayIndex(JSLinearString* str, 
 JS_FRIEND_API(void)
 SetPreserveWrapperCallback(JSContext* cx, PreserveWrapperCallback callback);
 
 JS_FRIEND_API(bool)
 IsObjectInContextCompartment(JSObject* obj, const JSContext* cx);
 
 /*
  * NB: keep these in sync with the copy in builtin/SelfHostingDefines.h.
- * The first two are omitted because they shouldn't be used in new code.
  */
-#define JSITER_ENUMERATE  0x1   /* for-in compatible hidden default iterator */
+/* 0x1 is no longer used */
 /* 0x2 is no longer used */
 /* 0x4 is no longer used */
 #define JSITER_OWNONLY    0x8   /* iterate over obj's own properties only */
 #define JSITER_HIDDEN     0x10  /* also enumerate non-enumerable properties */
 #define JSITER_SYMBOLS    0x20  /* also include symbol property keys */
 #define JSITER_SYMBOLSONLY 0x40 /* exclude string property keys */
 #define JSITER_FORAWAITOF 0x80  /* for-await-of */
 
diff --git a/js/src/jsiter.cpp b/js/src/jsiter.cpp
--- a/js/src/jsiter.cpp
+++ b/js/src/jsiter.cpp
@@ -532,46 +532,42 @@ JS_FRIEND_API(bool)
 js::GetPropertyKeys(JSContext* cx, HandleObject obj, unsigned flags, AutoIdVector* props)
 {
     return Snapshot(cx, obj,
                     flags & (JSITER_OWNONLY | JSITER_HIDDEN | JSITER_SYMBOLS | JSITER_SYMBOLSONLY),
                     props);
 }
 
 static inline PropertyIteratorObject*
-NewPropertyIteratorObject(JSContext* cx, unsigned flags)
+NewPropertyIteratorObject(JSContext* cx)
 {
-    if (flags & JSITER_ENUMERATE) {
-        RootedObjectGroup group(cx, ObjectGroup::defaultNewGroup(cx, &PropertyIteratorObject::class_,
-                                                                 TaggedProto(nullptr)));
-        if (!group)
-            return nullptr;
+    RootedObjectGroup group(cx, ObjectGroup::defaultNewGroup(cx, &PropertyIteratorObject::class_,
+                                                             TaggedProto(nullptr)));
+    if (!group)
+        return nullptr;
 
-        const Class* clasp = &PropertyIteratorObject::class_;
-        RootedShape shape(cx, EmptyShape::getInitialShape(cx, clasp, TaggedProto(nullptr),
-                                                          ITERATOR_FINALIZE_KIND));
-        if (!shape)
-            return nullptr;
+    const Class* clasp = &PropertyIteratorObject::class_;
+    RootedShape shape(cx, EmptyShape::getInitialShape(cx, clasp, TaggedProto(nullptr),
+                                                      ITERATOR_FINALIZE_KIND));
+    if (!shape)
+        return nullptr;
 
-        JSObject* obj;
-        JS_TRY_VAR_OR_RETURN_NULL(cx, obj, NativeObject::create(cx, ITERATOR_FINALIZE_KIND,
-                                                                GetInitialHeap(GenericObject, clasp),
-                                                                shape, group));
+    JSObject* obj;
+    JS_TRY_VAR_OR_RETURN_NULL(cx, obj, NativeObject::create(cx, ITERATOR_FINALIZE_KIND,
+                                                            GetInitialHeap(GenericObject, clasp),
+                                                            shape, group));
 
-        PropertyIteratorObject* res = &obj->as<PropertyIteratorObject>();
+    PropertyIteratorObject* res = &obj->as<PropertyIteratorObject>();
 
-        // CodeGenerator::visitIteratorStartO assumes the iterator object is not
-        // inside the nursery when deciding whether a barrier is necessary.
-        MOZ_ASSERT(!js::gc::IsInsideNursery(res));
+    // CodeGenerator::visitIteratorStartO assumes the iterator object is not
+    // inside the nursery when deciding whether a barrier is necessary.
+    MOZ_ASSERT(!js::gc::IsInsideNursery(res));
 
-        MOZ_ASSERT(res->numFixedSlots() == JSObject::ITER_CLASS_NFIXED_SLOTS);
-        return res;
-    }
-
-    return NewBuiltinClassInstance<PropertyIteratorObject>(cx);
+    MOZ_ASSERT(res->numFixedSlots() == JSObject::ITER_CLASS_NFIXED_SLOTS);
+    return res;
 }
 
 NativeIterator*
 NativeIterator::allocateIterator(JSContext* cx, uint32_t numGuards, uint32_t plength)
 {
     JS_STATIC_ASSERT(sizeof(ReceiverGuard) == 2 * sizeof(void*));
 
     size_t extraLength = plength + numGuards * 2;
@@ -602,21 +598,21 @@ NativeIterator::allocateSentinel(JSConte
     PodZero(ni);
 
     ni->next_ = ni;
     ni->prev_ = ni;
     return ni;
 }
 
 inline void
-NativeIterator::init(JSObject* obj, JSObject* iterObj, unsigned flags, uint32_t numGuards, uint32_t key)
+NativeIterator::init(JSObject* obj, JSObject* iterObj, uint32_t numGuards, uint32_t key)
 {
     this->obj.init(obj);
     this->iterObj_ = iterObj;
-    this->flags = flags;
+    this->flags = 0;
     this->guard_array = (HeapReceiverGuard*) this->props_end;
     this->guard_length = numGuards;
     this->guard_key = key;
 }
 
 bool
 NativeIterator::initProperties(JSContext* cx, Handle<PropertyIteratorObject*> obj,
                                const AutoIdVector& props)
@@ -637,42 +633,39 @@ NativeIterator::initProperties(JSContext
 
     return true;
 }
 
 static inline void
 RegisterEnumerator(JSContext* cx, PropertyIteratorObject* iterobj, NativeIterator* ni)
 {
     /* Register non-escaping native enumerators (for-in) with the current context. */
-    if (ni->flags & JSITER_ENUMERATE) {
-        ni->link(cx->compartment()->enumerators);
+    ni->link(cx->compartment()->enumerators);
 
-        MOZ_ASSERT(!(ni->flags & JSITER_ACTIVE));
-        ni->flags |= JSITER_ACTIVE;
-    }
+    MOZ_ASSERT(!(ni->flags & JSITER_ACTIVE));
+    ni->flags |= JSITER_ACTIVE;
 }
 
 static inline PropertyIteratorObject*
-VectorToKeyIterator(JSContext* cx, HandleObject obj, unsigned flags, AutoIdVector& keys,
-                    uint32_t numGuards)
+VectorToKeyIterator(JSContext* cx, HandleObject obj, AutoIdVector& keys, uint32_t numGuards)
 {
     if (obj->isSingleton() && !JSObject::setIteratedSingleton(cx, obj))
         return nullptr;
     MarkObjectGroupFlags(cx, obj, OBJECT_FLAG_ITERATED);
 
-    Rooted<PropertyIteratorObject*> iterobj(cx, NewPropertyIteratorObject(cx, flags));
+    Rooted<PropertyIteratorObject*> iterobj(cx, NewPropertyIteratorObject(cx));
     if (!iterobj)
         return nullptr;
 
     NativeIterator* ni = NativeIterator::allocateIterator(cx, numGuards, keys.length());
     if (!ni)
         return nullptr;
 
     iterobj->setNativeIterator(ni);
-    ni->init(obj, iterobj, flags, numGuards, 0);
+    ni->init(obj, iterobj, numGuards, 0);
     if (!ni->initProperties(cx, iterobj, keys))
         return nullptr;
 
     if (numGuards) {
         // Fill in the guard array from scratch. Also recompute the guard key
         // as we might have reshaped the object (see for instance the
         // setIteratedSingleton call above) or GC might have moved shapes and
         // groups in memory.
@@ -694,37 +687,36 @@ VectorToKeyIterator(JSContext* cx, Handl
     }
 
     RegisterEnumerator(cx, iterobj, ni);
     return iterobj;
 }
 
 
 JSObject*
-js::EnumeratedIdVectorToIterator(JSContext* cx, HandleObject obj, unsigned flags,
-                                 AutoIdVector& props)
+js::EnumeratedIdVectorToIterator(JSContext* cx, HandleObject obj, AutoIdVector& props)
 {
-    return VectorToKeyIterator(cx, obj, flags, props, 0);
+    return VectorToKeyIterator(cx, obj, props, 0);
 }
 
 // Mainly used for .. in over null/undefined
 JSObject*
-js::NewEmptyPropertyIterator(JSContext* cx, unsigned flags)
+js::NewEmptyPropertyIterator(JSContext* cx)
 {
-    Rooted<PropertyIteratorObject*> iterobj(cx, NewPropertyIteratorObject(cx, flags));
+    Rooted<PropertyIteratorObject*> iterobj(cx, NewPropertyIteratorObject(cx));
     if (!iterobj)
         return nullptr;
 
     AutoIdVector keys(cx); // Empty
     NativeIterator* ni = NativeIterator::allocateIterator(cx, 0, keys.length());
     if (!ni)
         return nullptr;
 
     iterobj->setNativeIterator(ni);
-    ni->init(nullptr, iterobj, flags, 0, 0);
+    ni->init(nullptr, iterobj, 0, 0);
     if (!ni->initProperties(cx, iterobj, keys))
         return nullptr;
 
     RegisterEnumerator(cx, iterobj, ni);
     return iterobj;
 }
 
 /* static */ bool
@@ -761,20 +753,16 @@ CanCompareIterableObjectToCache(JSObject
 
 using ReceiverGuardVector = Vector<ReceiverGuard, 8>;
 
 static MOZ_ALWAYS_INLINE PropertyIteratorObject*
 LookupInIteratorCache(JSContext* cx, JSObject* obj, uint32_t* numGuards)
 {
     MOZ_ASSERT(*numGuards == 0);
 
-    // The iterator object for JSITER_ENUMERATE never escapes, so we don't
-    // care that the "proper" prototype is set.  This also lets us reuse an
-    // old, inactive iterator object.
-
     ReceiverGuardVector guards(cx);
     uint32_t key = 0;
     JSObject* pobj = obj;
     do {
         if (!CanCompareIterableObjectToCache(pobj))
             return nullptr;
 
         ReceiverGuard guard(pobj);
@@ -856,48 +844,39 @@ StoreInIteratorCache(JSContext* cx, JSOb
         ReportOutOfMemory(cx);
         return false;
     }
 
     return true;
 }
 
 JSObject*
-js::GetIterator(JSContext* cx, HandleObject obj, unsigned flags)
+js::GetIterator(JSContext* cx, HandleObject obj)
 {
     uint32_t numGuards = 0;
-    if (flags == JSITER_ENUMERATE) {
-        if (PropertyIteratorObject* iterobj = LookupInIteratorCache(cx, obj, &numGuards)) {
-            NativeIterator* ni = iterobj->getNativeIterator();
-            UpdateNativeIterator(ni, obj);
-            RegisterEnumerator(cx, iterobj, ni);
-            return iterobj;
-        }
+    if (PropertyIteratorObject* iterobj = LookupInIteratorCache(cx, obj, &numGuards)) {
+        NativeIterator* ni = iterobj->getNativeIterator();
+        UpdateNativeIterator(ni, obj);
+        RegisterEnumerator(cx, iterobj, ni);
+        return iterobj;
+    }
 
-        if (numGuards > 0 && !CanStoreInIteratorCache(cx, obj))
-            numGuards = 0;
-    }
+    if (numGuards > 0 && !CanStoreInIteratorCache(cx, obj))
+        numGuards = 0;
 
     MOZ_ASSERT(!obj->is<PropertyIteratorObject>());
 
-    // We should only call the enumerate trap for "for-in".
-    // Or when we call GetIterator from the Proxy [[Enumerate]] hook.
-    // JSITER_ENUMERATE is just an optimization and the same
-    // as flags == 0 otherwise.
-    if (flags == 0 || flags == JSITER_ENUMERATE) {
-        if (MOZ_UNLIKELY(obj->is<ProxyObject>()))
-            return Proxy::enumerate(cx, obj);
-    }
-
+    if (MOZ_UNLIKELY(obj->is<ProxyObject>()))
+        return Proxy::enumerate(cx, obj);
 
     AutoIdVector keys(cx);
-    if (!Snapshot(cx, obj, flags, &keys))
+    if (!Snapshot(cx, obj, 0, &keys))
         return nullptr;
 
-    JSObject* res = VectorToKeyIterator(cx, obj, flags, keys, numGuards);
+    JSObject* res = VectorToKeyIterator(cx, obj, keys, numGuards);
     if (!res)
         return nullptr;
 
     PropertyIteratorObject* iterobj = &res->as<PropertyIteratorObject>();
     assertSameCompartment(cx, iterobj);
 
     // Cache the iterator object.
     if (numGuards > 0) {
@@ -1116,57 +1095,55 @@ js::NewStringIteratorObject(JSContext* c
     RootedObject proto(cx, GlobalObject::getOrCreateStringIteratorPrototype(cx, cx->global()));
     if (!proto)
         return nullptr;
 
     return NewObjectWithGivenProto<StringIteratorObject>(cx, proto, newKind);
 }
 
 JSObject*
-js::ValueToIterator(JSContext* cx, unsigned flags, HandleValue vp)
+js::ValueToIterator(JSContext* cx, HandleValue vp)
 {
     RootedObject obj(cx);
     if (vp.isObject()) {
         /* Common case. */
         obj = &vp.toObject();
-    } else if ((flags & JSITER_ENUMERATE) && vp.isNullOrUndefined()) {
+    } else if (vp.isNullOrUndefined()) {
         /*
          * Enumerating over null and undefined gives an empty enumerator, so
          * that |for (var p in <null or undefined>) <loop>;| never executes
          * <loop>, per ES5 12.6.4.
          */
-        return NewEmptyPropertyIterator(cx, flags);
+        return NewEmptyPropertyIterator(cx);
     } else {
         obj = ToObject(cx, vp);
         if (!obj)
             return nullptr;
     }
 
-    return GetIterator(cx, obj, flags);
+    return GetIterator(cx, obj);
 }
 
 void
 js::CloseIterator(JSObject* obj)
 {
     if (obj->is<PropertyIteratorObject>()) {
         /* Remove enumerators from the active list, which is a stack. */
         NativeIterator* ni = obj->as<PropertyIteratorObject>().getNativeIterator();
 
-        if (ni->flags & JSITER_ENUMERATE) {
-            ni->unlink();
+        ni->unlink();
 
-            MOZ_ASSERT(ni->flags & JSITER_ACTIVE);
-            ni->flags &= ~JSITER_ACTIVE;
+        MOZ_ASSERT(ni->flags & JSITER_ACTIVE);
+        ni->flags &= ~JSITER_ACTIVE;
 
-            /*
-             * Reset the enumerator; it may still be in the cached iterators
-             * for this thread, and can be reused.
-             */
-            ni->props_cursor = ni->props_array;
-        }
+        /*
+         * Reset the enumerator; it may still be in the cached iterators
+         * for this thread, and can be reused.
+         */
+        ni->props_cursor = ni->props_array;
     }
 }
 
 bool
 js::IteratorCloseForException(JSContext* cx, HandleObject obj)
 {
     MOZ_ASSERT(cx->isExceptionPending());
 
@@ -1215,18 +1192,17 @@ js::IteratorCloseForException(JSContext*
     return true;
 }
 
 void
 js::UnwindIteratorForUncatchableException(JSContext* cx, JSObject* obj)
 {
     if (obj->is<PropertyIteratorObject>()) {
         NativeIterator* ni = obj->as<PropertyIteratorObject>().getNativeIterator();
-        if (ni->flags & JSITER_ENUMERATE)
-            ni->unlink();
+        ni->unlink();
     }
 }
 
 /*
  * Suppress enumeration of deleted properties. This function must be called
  * when a property is deleted and there might be active enumerators.
  *
  * We maintain a list of active non-escaping for-in enumerators. To suppress
diff --git a/js/src/jsiter.h b/js/src/jsiter.h
--- a/js/src/jsiter.h
+++ b/js/src/jsiter.h
@@ -80,35 +80,32 @@ struct NativeIterator
     }
 
     void incCursor() {
         props_cursor = props_cursor + 1;
     }
     void link(NativeIterator* other) {
         /* A NativeIterator cannot appear in the enumerator list twice. */
         MOZ_ASSERT(!next_ && !prev_);
-        MOZ_ASSERT(flags & JSITER_ENUMERATE);
 
         this->next_ = other;
         this->prev_ = other->prev_;
         other->prev_->next_ = this;
         other->prev_ = this;
     }
     void unlink() {
-        MOZ_ASSERT(flags & JSITER_ENUMERATE);
-
         next_->prev_ = prev_;
         prev_->next_ = next_;
         next_ = nullptr;
         prev_ = nullptr;
     }
 
     static NativeIterator* allocateSentinel(JSContext* maybecx);
     static NativeIterator* allocateIterator(JSContext* cx, uint32_t slength, uint32_t plength);
-    void init(JSObject* obj, JSObject* iterObj, unsigned flags, uint32_t slength, uint32_t key);
+    void init(JSObject* obj, JSObject* iterObj, uint32_t slength, uint32_t key);
     bool initProperties(JSContext* cx, Handle<PropertyIteratorObject*> obj,
                         const js::AutoIdVector& props);
 
     void trace(JSTracer* trc);
 
     static void destroy(NativeIterator* iter) {
         js_free(iter);
     }
@@ -149,39 +146,29 @@ class StringIteratorObject : public JSOb
   public:
     static const Class class_;
 };
 
 StringIteratorObject*
 NewStringIteratorObject(JSContext* cx, NewObjectKind newKind = GenericObject);
 
 JSObject*
-GetIterator(JSContext* cx, HandleObject obj, unsigned flags);
+GetIterator(JSContext* cx, HandleObject obj);
 
 PropertyIteratorObject*
 LookupInIteratorCache(JSContext* cx, HandleObject obj);
 
-/*
- * Creates either a key or value iterator, depending on flags. For a value
- * iterator, performs value-lookup to convert the given list of jsids.
- */
 JSObject*
-EnumeratedIdVectorToIterator(JSContext* cx, HandleObject obj, unsigned flags, AutoIdVector& props);
+EnumeratedIdVectorToIterator(JSContext* cx, HandleObject obj, AutoIdVector& props);
 
 JSObject*
-NewEmptyPropertyIterator(JSContext* cx, unsigned flags);
+NewEmptyPropertyIterator(JSContext* cx);
 
-/*
- * Convert the value stored in *vp to its iteration object. The flags should
- * contain JSITER_ENUMERATE if js::ValueToIterator is called when enumerating
- * for-in semantics are required, and when the caller can guarantee that the
- * iterator will never be exposed to scripts.
- */
 JSObject*
-ValueToIterator(JSContext* cx, unsigned flags, HandleValue vp);
+ValueToIterator(JSContext* cx, HandleValue vp);
 
 void
 CloseIterator(JSObject* obj);
 
 bool
 IteratorCloseForException(JSContext* cx, HandleObject obj);
 
 void
diff --git a/js/src/proxy/BaseProxyHandler.cpp b/js/src/proxy/BaseProxyHandler.cpp
--- a/js/src/proxy/BaseProxyHandler.cpp
+++ b/js/src/proxy/BaseProxyHandler.cpp
@@ -289,17 +289,17 @@ BaseProxyHandler::enumerate(JSContext* c
     assertEnteredPolicy(cx, proxy, JSID_VOID, ENUMERATE);
 
     // GetPropertyKeys will invoke getOwnEnumerablePropertyKeys along the proto
     // chain for us.
     AutoIdVector props(cx);
     if (!GetPropertyKeys(cx, proxy, 0, &props))
         return nullptr;
 
-    return EnumeratedIdVectorToIterator(cx, proxy, 0, props);
+    return EnumeratedIdVectorToIterator(cx, proxy, props);
 }
 
 bool
 BaseProxyHandler::call(JSContext* cx, HandleObject proxy, const CallArgs& args) const
 {
     MOZ_CRASH("callable proxies should implement call trap");
 }
 
diff --git a/js/src/proxy/CrossCompartmentWrapper.cpp b/js/src/proxy/CrossCompartmentWrapper.cpp
--- a/js/src/proxy/CrossCompartmentWrapper.cpp
+++ b/js/src/proxy/CrossCompartmentWrapper.cpp
@@ -255,18 +255,17 @@ CrossCompartmentWrapper::getOwnEnumerabl
 
 /*
  * We can reify non-escaping iterator objects instead of having to wrap them. This
  * allows fast iteration over objects across a compartment boundary.
  */
 static bool
 CanReify(HandleObject obj)
 {
-    return obj->is<PropertyIteratorObject>() &&
-           (obj->as<PropertyIteratorObject>().getNativeIterator()->flags & JSITER_ENUMERATE);
+    return obj->is<PropertyIteratorObject>();
 }
 
 struct AutoCloseIterator
 {
     AutoCloseIterator(JSContext* cx, PropertyIteratorObject* obj) : obj(cx, obj) {}
 
     ~AutoCloseIterator() {
         if (obj)
@@ -310,17 +309,17 @@ Reify(JSContext* cx, JSCompartment* orig
                     return nullptr;
                 keys.infallibleAppend(id);
             }
         }
 
         close.clear();
         CloseIterator(iterObj);
 
-        obj = EnumeratedIdVectorToIterator(cx, obj, ni->flags, keys);
+        obj = EnumeratedIdVectorToIterator(cx, obj, keys);
     }
     return obj;
 }
 
 JSObject*
 CrossCompartmentWrapper::enumerate(JSContext* cx, HandleObject wrapper) const
 {
     RootedObject res(cx);
diff --git a/js/src/proxy/Proxy.cpp b/js/src/proxy/Proxy.cpp
--- a/js/src/proxy/Proxy.cpp
+++ b/js/src/proxy/Proxy.cpp
@@ -461,36 +461,36 @@ Proxy::enumerate(JSContext* cx, HandleOb
         AutoIdVector props(cx);
         if (!Proxy::getOwnEnumerablePropertyKeys(cx, proxy, props))
             return nullptr;
 
         RootedObject proto(cx);
         if (!GetPrototype(cx, proxy, &proto))
             return nullptr;
         if (!proto)
-            return EnumeratedIdVectorToIterator(cx, proxy, 0, props);
+            return EnumeratedIdVectorToIterator(cx, proxy, props);
         assertSameCompartment(cx, proxy, proto);
 
         AutoIdVector protoProps(cx);
         if (!GetPropertyKeys(cx, proto, 0, &protoProps))
             return nullptr;
         if (!AppendUnique(cx, props, protoProps))
             return nullptr;
-        return EnumeratedIdVectorToIterator(cx, proxy, 0, props);
+        return EnumeratedIdVectorToIterator(cx, proxy, props);
     }
 
     AutoEnterPolicy policy(cx, handler, proxy, JSID_VOIDHANDLE,
                            BaseProxyHandler::ENUMERATE, true);
 
     // If the policy denies access but wants us to return true, we need
     // to hand a valid (empty) iterator object to the caller.
     if (!policy.allowed()) {
         if (!policy.returnValue())
             return nullptr;
-        return NewEmptyPropertyIterator(cx, 0);
+        return NewEmptyPropertyIterator(cx);
     }
     return handler->enumerate(cx, proxy);
 }
 
 bool
 Proxy::call(JSContext* cx, HandleObject proxy, const CallArgs& args)
 {
     if (!CheckRecursionLimit(cx))
diff --git a/js/src/proxy/Wrapper.cpp b/js/src/proxy/Wrapper.cpp
--- a/js/src/proxy/Wrapper.cpp
+++ b/js/src/proxy/Wrapper.cpp
@@ -81,17 +81,17 @@ ForwardingProxyHandler::delete_(JSContex
 }
 
 JSObject*
 ForwardingProxyHandler::enumerate(JSContext* cx, HandleObject proxy) const
 {
     assertEnteredPolicy(cx, proxy, JSID_VOID, ENUMERATE);
     MOZ_ASSERT(!hasPrototype()); // Should never be called if there's a prototype.
     RootedObject target(cx, proxy->as<ProxyObject>().target());
-    return GetIterator(cx, target, 0);
+    return GetIterator(cx, target);
 }
 
 bool
 ForwardingProxyHandler::getPrototype(JSContext* cx, HandleObject proxy,
                                      MutableHandleObject protop) const
 {
     RootedObject target(cx, proxy->as<ProxyObject>().target());
     return GetPrototype(cx, target, protop);
diff --git a/js/src/vm/Interpreter.cpp b/js/src/vm/Interpreter.cpp
--- a/js/src/vm/Interpreter.cpp
+++ b/js/src/vm/Interpreter.cpp
@@ -2298,20 +2298,18 @@ CASE(JSOP_HASOWN)
     REGS.sp--;
     REGS.sp[-1].setBoolean(found);
 }
 END_CASE(JSOP_HASOWN)
 
 CASE(JSOP_ITER)
 {
     MOZ_ASSERT(REGS.stackDepth() >= 1);
-    uint8_t flags = GET_UINT8(REGS.pc);
     HandleValue val = REGS.stackHandleAt(-1);
-    ReservedRooted<JSObject*> iter(&rootObject0);
-    iter.set(ValueToIterator(cx, flags, val));
+    JSObject* iter = ValueToIterator(cx, val);
     if (!iter)
         goto error;
     REGS.sp[-1].setObject(*iter);
 }
 END_CASE(JSOP_ITER)
 
 CASE(JSOP_MOREITER)
 {
diff --git a/js/src/vm/Opcodes.h b/js/src/vm/Opcodes.h
--- a/js/src/vm/Opcodes.h
+++ b/js/src/vm/Opcodes.h
@@ -693,25 +693,24 @@ 1234567890123456789012345678901234567890
      *   Category: Statements
      *   Type: Exception Handling
      *   Operands: uint16_t msgNumber
      *   Stack: =>
      */ \
     macro(JSOP_THROWMSG,   74, "throwmsg",    NULL,         3,  0,  0, JOF_UINT16) \
     \
     /*
-     * Sets up a for-in or for-each-in loop using the JSITER_* flag bits in
-     * this op's uint8_t immediate operand. It pops the top of stack value as
-     * 'val' and pushes 'iter' which is an iterator for 'val'.
+     * Sets up a for-in loop. It pops the top of stack value as 'val' and pushes
+     * 'iter' which is an iterator for 'val'.
      *   Category: Statements
      *   Type: For-In Statement
-     *   Operands: uint8_t flags
+     *   Operands:
      *   Stack: val => iter
      */ \
-    macro(JSOP_ITER,      75, "iter",       NULL,         2,  1,  1,  JOF_UINT8) \
+    macro(JSOP_ITER,      75, "iter",       NULL,         1,  1,  1,  JOF_BYTE) \
     /*
      * Pushes the next iterated value onto the stack. If no value is available,
      * MagicValue(JS_NO_ITER_VALUE) is pushed.
      *
      *   Category: Statements
      *   Type: For-In Statement
      *   Operands:
      *   Stack: iter => iter, val
