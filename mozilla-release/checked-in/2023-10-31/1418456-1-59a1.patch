# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1510948349 -3600
# Node ID 391937d9f0d7c9dfecec618747eef04ed379901f
# Parent  803d0f1bc3e3750017f3237794bc1e2958974d2c
Bug 1418456: Clear subtree element data when applying an XBL binding. r=heycam

We not only need to care about children getting inserted in the flat tree, but
also about children moving _out_ of the flat tree.

In particular, as of right now we may leave stale data on elements when they
disappear from the flattened tree.

We're lucky enough that in 99% of the situations we enter in[1] and that clears
all the stuff, including servo data. But my assertions for bug 1414999 caught
the template / observes case.

Thus, just clear the whole bound element subtree data, and restyle it in the
end, no need for StyleNewChildren. This matches what we do for shadow DOM
(though in the shadow DOM case we do it async in DestroyFramesForAndRestyle).

[1]: https://searchfox.org/mozilla-central/rev/9bab9dc5a9472e3c163ab279847d2249322c206e/dom/xbl/nsXBLBinding.cpp#368

MozReview-Commit-ID: 69A0aR0AFfU

diff --git a/dom/xbl/nsXBLService.cpp b/dom/xbl/nsXBLService.cpp
--- a/dom/xbl/nsXBLService.cpp
+++ b/dom/xbl/nsXBLService.cpp
@@ -49,16 +49,17 @@
 #ifdef MOZ_XUL
 #include "nsXULPrototypeCache.h"
 #endif
 #include "nsIDOMEventListener.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/EventListenerManager.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/ServoStyleSet.h"
+#include "mozilla/ServoRestyleManager.h"
 #include "mozilla/dom/Event.h"
 #include "mozilla/dom/Element.h"
 
 using namespace mozilla;
 using namespace mozilla::dom;
 
 #define NS_MAX_XBL_BINDING_RECURSION 20
 
@@ -378,38 +379,42 @@ nsXBLService::IsChromeOrResourceURI(nsIU
   if (NS_SUCCEEDED(aURI->SchemeIs("chrome", &isChrome)) &&
       NS_SUCCEEDED(aURI->SchemeIs("resource", &isResource)))
       return (isChrome || isResource);
   return false;
 }
 
 // RAII class to invoke StyleNewChildren for Elements in Servo-backed documents
 // on destruction.
-class MOZ_STACK_CLASS AutoStyleNewChildren
+class MOZ_STACK_CLASS AutoStyleElement
 {
 public:
-  explicit AutoStyleNewChildren(Element* aElement) : mElement(aElement) { MOZ_ASSERT(mElement); }
-  ~AutoStyleNewChildren()
+  explicit AutoStyleElement(Element* aElement)
+    : mElement(aElement)
+    , mHadData(aElement->HasServoData())
+  {
+    if (mHadData) {
+      ServoRestyleManager::ClearServoDataFromSubtree(mElement);
+    }
+  }
+  ~AutoStyleElement()
   {
     nsIPresShell* presShell = mElement->OwnerDoc()->GetShell();
-    if (!presShell || !presShell->DidInitialize()) {
+    if (!mHadData || !presShell || !presShell->DidInitialize()) {
       return;
     }
 
     if (ServoStyleSet* servoSet = presShell->StyleSet()->GetAsServo()) {
-      // Check MayTraverseFrom to handle programatic XBL consumers.
-      // See bug 1370793.
-      if (servoSet->MayTraverseFrom(mElement)) {
-        servoSet->StyleNewlyBoundElement(mElement);
-      }
+      servoSet->ReresolveStyleForBindings(mElement);
     }
   }
 
 private:
   Element* mElement;
+  bool mHadData;
 };
 
 // This function loads a particular XBL file and installs all of the bindings
 // onto the element.
 nsresult
 nsXBLService::LoadBindings(nsIContent* aContent, nsIURI* aURL,
                            nsIPrincipal* aOriginPrincipal,
                            nsXBLBinding** aBinding, bool* aResolveStyle)
@@ -432,33 +437,32 @@ nsXBLService::LoadBindings(nsIContent* a
 
   if (ok) {
     // Block an attempt to load a binding that has special wrapper
     // automation needs.
     return NS_OK;
   }
 
   // There are various places in this function where we shuffle content around
-  // the subtree and rebind things to and from insertion points. Once all that's
-  // done, we want to invoke StyleNewChildren to style any unstyled children
-  // that we may have after bindings have been removed and applied. This includes
-  // anonymous content created in this function, explicit children for which we
-  // defer styling until after XBL bindings are applied, and elements whose existing
-  // style was invalidated by a call to SetXBLInsertionParent.
+  // the subtree and rebind things to and from insertion points.
+  //
+  // Once all that's done, we want to invoke StyleNewSubtree to restyle the
+  // whole subtree with the new flattened tree that we may have after bindings
+  // have been removed and applied.
   //
-  // However, we skip this styling if aContent is not in the document, since we
-  // should keep such elements unstyled.  (There are some odd cases where we do
-  // apply bindings to elements not in the document.)
-  Maybe<AutoStyleNewChildren> styleNewChildren;
-  if (aContent->IsInComposedDoc()) {
-    styleNewChildren.emplace(aContent->AsElement());
+  // This includes anonymous content created in this function, explicit children
+  // for which we defer styling until after XBL bindings are applied, and
+  // elements whose existing style was invalidated by a call to
+  // SetXBLInsertionParent.
+  Maybe<AutoStyleElement> styleElement;
+  if (aContent->IsStyledByServo()) {
+    styleElement.emplace(aContent->AsElement());
   }
 
-  nsXBLBinding *binding = aContent->GetXBLBinding();
-  if (binding) {
+  if (nsXBLBinding* binding = aContent->GetXBLBinding()) {
     if (binding->MarkedForDeath()) {
       FlushStyleBindings(aContent);
       binding = nullptr;
     }
     else {
       // See if the URIs match.
       if (binding->PrototypeBinding()->CompareBindingURI(aURL))
         return NS_OK;
diff --git a/layout/base/nsCSSFrameConstructor.cpp b/layout/base/nsCSSFrameConstructor.cpp
--- a/layout/base/nsCSSFrameConstructor.cpp
+++ b/layout/base/nsCSSFrameConstructor.cpp
@@ -2611,27 +2611,22 @@ nsCSSFrameConstructor::ConstructDocEleme
     if (binding) {
       // For backwards compat, keep firing the root's constructor
       // after all of its kids' constructors.  So tell the binding
       // manager about it right now.
       mDocument->BindingManager()->AddToAttachedQueue(binding);
     }
 
     if (resolveStyle) {
-      if (styleContext->IsServo()) {
-        styleContext = mPresShell->StyleSet()->AsServo()->
-          ReresolveStyleForBindings(aDocElement);
-      } else {
-        // FIXME: Should this use ResolveStyleContext?  (The calls in
-        // this function are the only case in nsCSSFrameConstructor
-        // where we don't do so for the construction of a style context
-        // for an element.)
-        styleContext = mPresShell->StyleSet()->ResolveStyleFor(
-            aDocElement, nullptr, LazyComputeBehavior::Assert);
-      }
+      // FIXME: Should this use ResolveStyleContext?  (The calls in
+      // this function are the only case in nsCSSFrameConstructor
+      // where we don't do so for the construction of a style context
+      // for an element.)
+      styleContext = mPresShell->StyleSet()->ResolveStyleFor(
+          aDocElement, nullptr, LazyComputeBehavior::Assert);
       display = styleContext->StyleDisplay();
     }
   } else if (display->mBinding.ForceGet() && aDocElement->IsStyledByServo()) {
     // See the comment in AddFrameConstructionItemsInternal for why this is
     // needed.
     mPresShell->StyleSet()->AsServo()->StyleNewChildren(aDocElement);
   }
 
@@ -5931,18 +5926,18 @@ nsCSSFrameConstructor::AddFrameConstruct
       if (newPendingBinding->mBinding) {
         pendingBinding = newPendingBinding;
         // aState takes over owning newPendingBinding
         aState.AddPendingBinding(newPendingBinding.forget());
       }
 
       if (resolveStyle) {
         if (styleContext->IsServo()) {
-          styleContext = mPresShell->StyleSet()->AsServo()->
-            ReresolveStyleForBindings(aContent->AsElement());
+          styleContext =
+            mPresShell->StyleSet()->AsServo()->ResolveServoStyle(aContent->AsElement());
         } else {
           styleContext =
             ResolveStyleContext(styleContext->AsGecko()->GetParent(),
                                 aContent, &aState);
         }
 
         display = styleContext->StyleDisplay();
         aStyleContext = styleContext;
@@ -5952,16 +5947,18 @@ nsCSSFrameConstructor::AddFrameConstruct
     } else if (display->mBinding.ForceGet()) {
       if (aContent->IsStyledByServo()) {
         // Servo's should_traverse_children skips styling descendants of
         // elements with a -moz-binding value.  For -moz-binding URLs that can
         // be resolved, we will load the binding above, which will style the
         // children after they have been rearranged in the flattened tree.
         // If the URL couldn't be resolved, we still need to style the children,
         // so we do that here.
+        //
+        // FIXME(emilio): Again, should go away.
         mPresShell->StyleSet()->AsServo()->StyleNewChildren(aContent->AsElement());
       }
     }
   }
 
   const bool isGeneratedContent = !!(aFlags & ITEM_IS_GENERATED_CONTENT);
 
   // Pre-check for display "none" - if we find that, don't create
diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -344,29 +344,28 @@ ServoStyleSet::ResolveStyleFor(Element* 
     PreTraverseSync();
     return ResolveStyleLazilyInternal(
         aElement, CSSPseudoElementType::NotPseudo);
   }
 
   return ResolveServoStyle(aElement);
 }
 
-already_AddRefed<ServoStyleContext>
+void
 ServoStyleSet::ReresolveStyleForBindings(Element* aElement)
 {
-  // XXX: We should have a better way to restyle ourselves.
-  ServoRestyleManager::ClearServoDataFromSubtree(aElement);
+  MOZ_ASSERT(!aElement->HasServoData(), "Should've been cleared before!");
   StyleNewSubtree(aElement);
 
   // Servo's should_traverse_children() in traversal.rs skips
   // styling descendants of elements with a -moz-binding the
   // first time. Thus call StyleNewChildren() again.
+  //
+  // FIXME(emilio): This is just stupid, we should remove that now.
   StyleNewChildren(aElement);
-
-  return ResolveServoStyle(aElement);
 }
 
 const ServoElementSnapshotTable&
 ServoStyleSet::Snapshots()
 {
   return mPresContext->RestyleManager()->AsServo()->Snapshots();
 }
 
diff --git a/layout/style/ServoStyleSet.h b/layout/style/ServoStyleSet.h
--- a/layout/style/ServoStyleSet.h
+++ b/layout/style/ServoStyleSet.h
@@ -177,20 +177,20 @@ public:
   nsresult EndUpdate();
 
   already_AddRefed<ServoStyleContext>
   ResolveStyleFor(dom::Element* aElement,
                   ServoStyleContext* aParentContext,
                   LazyComputeBehavior aMayCompute);
 
   // Clear style data and resolve style for the given element and its
-  // subtree for changes to -moz-binding. It returns the new style
-  // context of the given element.
-  already_AddRefed<ServoStyleContext>
-  ReresolveStyleForBindings(Element* aElement);
+  // subtree for changes to -moz-binding.
+  //
+  // TODO(emilio): Remove.
+  void ReresolveStyleForBindings(Element* aElement);
 
   // Get a style context for a text node (which no rules will match).
   //
   // The returned style context will have nsCSSAnonBoxes::mozText as its pseudo.
   //
   // (Perhaps mozText should go away and we shouldn't even create style
   // contexts for such content nodes, when text-combine-upright is not
   // present.  However, not doing any rule matching for them is a first step.)
