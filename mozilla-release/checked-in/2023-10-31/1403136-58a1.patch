# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1506457600 -7200
# Node ID f0da5d64adedfa0658adf0d2c90d68e3a225a05c
# Parent  20c88d5336b8975acee83639b014d0234450d40d
Bug 1403136 - Clean up JSSetterOp code now that slotful setters are gone. r=evilpie

diff --git a/js/ipc/JavaScriptShared.cpp b/js/ipc/JavaScriptShared.cpp
--- a/js/ipc/JavaScriptShared.cpp
+++ b/js/ipc/JavaScriptShared.cpp
@@ -595,17 +595,17 @@ JavaScriptShared::fromDescriptor(JSConte
 bool
 UnknownPropertyStub(JSContext* cx, HandleObject obj, HandleId id, MutableHandleValue vp)
 {
     JS_ReportErrorASCII(cx, "getter could not be wrapped via CPOWs");
     return false;
 }
 
 bool
-UnknownStrictPropertyStub(JSContext* cx, HandleObject obj, HandleId id, MutableHandleValue vp,
+UnknownStrictPropertyStub(JSContext* cx, HandleObject obj, HandleId id, HandleValue v,
                           ObjectOpResult& result)
 {
     JS_ReportErrorASCII(cx, "setter could not be wrapped via CPOWs");
     return false;
 }
 
 bool
 JavaScriptShared::toDescriptor(JSContext* cx, const PPropertyDescriptor& in,
diff --git a/js/public/Class.h b/js/public/Class.h
--- a/js/public/Class.h
+++ b/js/public/Class.h
@@ -374,23 +374,21 @@ typedef bool
 
 /** Add a property named by id to obj. */
 typedef bool
 (* JSAddPropertyOp)(JSContext* cx, JS::HandleObject obj, JS::HandleId id, JS::HandleValue v);
 
 /**
  * Set a property named by id in obj, treating the assignment as strict
  * mode code if strict is true. Note the jsid id type -- id may be a string
- * (Unicode property identifier) or an int (element index). The *vp out
- * parameter, on success, is the new property value after the
- * set.
+ * (Unicode property identifier) or an int (element index).
  */
 typedef bool
 (* JSSetterOp)(JSContext* cx, JS::HandleObject obj, JS::HandleId id,
-               JS::MutableHandleValue vp, JS::ObjectOpResult& result);
+               JS::HandleValue v, JS::ObjectOpResult& result);
 
 /**
  * Delete a property named by id in obj.
  *
  * If an error occurred, return false as per normal JSAPI error practice.
  *
  * If no error occurred, but the deletion attempt wasn't allowed (perhaps
  * because the property was non-configurable), call result.fail() and
diff --git a/js/src/jsarray.cpp b/js/src/jsarray.cpp
--- a/js/src/jsarray.cpp
+++ b/js/src/jsarray.cpp
@@ -618,33 +618,33 @@ js::SetLengthProperty(JSContext* cx, Han
 static bool
 array_length_getter(JSContext* cx, HandleObject obj, HandleId id, MutableHandleValue vp)
 {
     vp.setNumber(obj->as<ArrayObject>().length());
     return true;
 }
 
 static bool
-array_length_setter(JSContext* cx, HandleObject obj, HandleId id, MutableHandleValue vp,
+array_length_setter(JSContext* cx, HandleObject obj, HandleId id, HandleValue v,
                     ObjectOpResult& result)
 {
     MOZ_ASSERT(id == NameToId(cx->names().length));
 
     if (!obj->is<ArrayObject>()) {
         // This array .length property was found on the prototype
         // chain. Ideally the setter should not have been called, but since
         // we're here, do an impression of SetPropertyByDefining.
-        return DefineDataProperty(cx, obj, id, vp, JSPROP_ENUMERATE, result);
+        return DefineDataProperty(cx, obj, id, v, JSPROP_ENUMERATE, result);
     }
 
     Rooted<ArrayObject*> arr(cx, &obj->as<ArrayObject>());
     MOZ_ASSERT(arr->lengthIsWritable(),
                "setter shouldn't be called if property is non-writable");
 
-    return ArraySetLength(cx, arr, id, JSPROP_PERMANENT, vp, result);
+    return ArraySetLength(cx, arr, id, JSPROP_PERMANENT, v, result);
 }
 
 struct ReverseIndexComparator
 {
     bool operator()(const uint32_t& a, const uint32_t& b, bool* lessOrEqualp) {
         MOZ_ASSERT(a != b, "how'd we get duplicate indexes?");
         *lessOrEqualp = b <= a;
         return true;
diff --git a/js/src/jscntxt.cpp b/js/src/jscntxt.cpp
--- a/js/src/jscntxt.cpp
+++ b/js/src/jscntxt.cpp
@@ -1328,17 +1328,16 @@ JSContext::JSContext(JSRuntime* runtime,
     enableAccessValidation(false),
     inUnsafeRegion(0),
     generationalDisabled(0),
     compactingDisabledCount(0),
     keepAtoms(0),
     suppressProfilerSampling(false),
     tempLifoAlloc_((size_t)TEMP_LIFO_ALLOC_PRIMARY_CHUNK_SIZE),
     debuggerMutations(0),
-    propertyRemovals(0),
     ionPcScriptCache(nullptr),
     throwing(false),
     overRecursed_(false),
     propagatingForcedReturn_(false),
     liveVolatileJitFrameIter_(nullptr),
     reportGranularity(JS_DEFAULT_JITREPORT_GRANULARITY),
     resolvingList(nullptr),
 #ifdef DEBUG
diff --git a/js/src/jscntxt.h b/js/src/jscntxt.h
--- a/js/src/jscntxt.h
+++ b/js/src/jscntxt.h
@@ -609,23 +609,16 @@ struct JSContext : public JS::RootingCon
   private:
     js::ThreadLocalData<js::LifoAlloc> tempLifoAlloc_;
   public:
     js::LifoAlloc& tempLifoAlloc() { return tempLifoAlloc_.ref(); }
     const js::LifoAlloc& tempLifoAlloc() const { return tempLifoAlloc_.ref(); }
 
     js::ThreadLocalData<uint32_t> debuggerMutations;
 
-    /*
-     * The propertyRemovals counter is incremented for every JSObject::clear,
-     * and for each JSObject::remove method call that frees a slot in the given
-     * object. See js_NativeGet and js_NativeSet in jsobj.cpp.
-     */
-    js::ThreadLocalData<uint32_t> propertyRemovals;
-
     // Cache for jit::GetPcScript().
     js::ThreadLocalData<js::jit::PcScriptCache*> ionPcScriptCache;
 
   private:
     /* Exception state -- the exception member is a GC root by definition. */
     js::ThreadLocalData<bool> throwing;            /* is there a pending exception? */
     js::ThreadLocalData<JS::PersistentRooted<JS::Value>> unwrappedException_; /* most-recently-thrown exception */
 
diff --git a/js/src/jscntxtinlines.h b/js/src/jscntxtinlines.h
--- a/js/src/jscntxtinlines.h
+++ b/js/src/jscntxtinlines.h
@@ -360,24 +360,24 @@ CallJSGetterOp(JSContext* cx, GetterOp o
     assertSameCompartment(cx, obj, id, vp);
     bool ok = op(cx, obj, id, vp);
     if (ok)
         assertSameCompartment(cx, vp);
     return ok;
 }
 
 MOZ_ALWAYS_INLINE bool
-CallJSSetterOp(JSContext* cx, SetterOp op, HandleObject obj, HandleId id, MutableHandleValue vp,
+CallJSSetterOp(JSContext* cx, SetterOp op, HandleObject obj, HandleId id, HandleValue v,
                ObjectOpResult& result)
 {
     if (!CheckRecursionLimit(cx))
         return false;
 
-    assertSameCompartment(cx, obj, id, vp);
-    return op(cx, obj, id, vp, result);
+    assertSameCompartment(cx, obj, id, v);
+    return op(cx, obj, id, v, result);
 }
 
 inline bool
 CallJSAddPropertyOp(JSContext* cx, JSAddPropertyOp op, HandleObject obj, HandleId id,
                     HandleValue v)
 {
     if (!CheckRecursionLimit(cx))
         return false;
diff --git a/js/src/proxy/BaseProxyHandler.cpp b/js/src/proxy/BaseProxyHandler.cpp
--- a/js/src/proxy/BaseProxyHandler.cpp
+++ b/js/src/proxy/BaseProxyHandler.cpp
@@ -190,20 +190,18 @@ js::SetPropertyIgnoringNamedGetter(JSCon
         // Steps 5.a-b.
         if (!ownDesc.writable())
             return result.fail(JSMSG_READ_ONLY);
         if (!receiver.isObject())
             return result.fail(JSMSG_SET_NON_OBJECT_RECEIVER);
         RootedObject receiverObj(cx, &receiver.toObject());
 
         // Nonstandard SpiderMonkey special case: setter ops.
-        if (SetterOp setter = ownDesc.setter()) {
-            RootedValue valCopy(cx, v);
-            return CallJSSetterOp(cx, setter, receiverObj, id, &valCopy, result);
-        }
+        if (SetterOp setter = ownDesc.setter())
+            return CallJSSetterOp(cx, setter, receiverObj, id, v, result);
 
         // Steps 5.c-d.
         Rooted<PropertyDescriptor> existingDescriptor(cx);
         if (!GetOwnPropertyDescriptor(cx, receiverObj, id, &existingDescriptor))
             return false;
 
         // Step 5.e.
         if (existingDescriptor.object()) {
diff --git a/js/src/vm/ArgumentsObject.cpp b/js/src/vm/ArgumentsObject.cpp
--- a/js/src/vm/ArgumentsObject.cpp
+++ b/js/src/vm/ArgumentsObject.cpp
@@ -472,17 +472,17 @@ MappedArgGetter(JSContext* cx, HandleObj
             else
                 vp.setObject(*callee);
         }
     }
     return true;
 }
 
 static bool
-MappedArgSetter(JSContext* cx, HandleObject obj, HandleId id, MutableHandleValue vp,
+MappedArgSetter(JSContext* cx, HandleObject obj, HandleId id, HandleValue v,
                 ObjectOpResult& result)
 {
     if (!obj->is<MappedArgumentsObject>())
         return result.succeed();
     Handle<MappedArgumentsObject*> argsobj = obj.as<MappedArgumentsObject>();
 
     Rooted<PropertyDescriptor> desc(cx);
     if (!GetOwnPropertyDescriptor(cx, argsobj, id, &desc))
@@ -495,36 +495,36 @@ MappedArgSetter(JSContext* cx, HandleObj
     RootedFunction callee(cx, &argsobj->callee());
     RootedScript script(cx, JSFunction::getOrCreateScript(cx, callee));
     if (!script)
         return false;
 
     if (JSID_IS_INT(id)) {
         unsigned arg = unsigned(JSID_TO_INT(id));
         if (arg < argsobj->initialLength() && !argsobj->isElementDeleted(arg)) {
-            argsobj->setElement(cx, arg, vp);
+            argsobj->setElement(cx, arg, v);
             if (arg < script->functionNonDelazifying()->nargs())
-                TypeScript::SetArgument(cx, script, arg, vp);
+                TypeScript::SetArgument(cx, script, arg, v);
             return result.succeed();
         }
     } else {
         MOZ_ASSERT(JSID_IS_ATOM(id, cx->names().length) || JSID_IS_ATOM(id, cx->names().callee));
     }
 
     /*
      * For simplicity we use delete/define to replace the property with a
      * simple data property. Note that we rely on ArgumentsObject::obj_delProperty
      * to set the corresponding override-bit.
      * Note also that we must define the property instead of setting it in case
      * the user has changed the prototype to an object that has a setter for
      * this id.
      */
     ObjectOpResult ignored;
     return NativeDeleteProperty(cx, argsobj, id, ignored) &&
-           NativeDefineDataProperty(cx, argsobj, id, vp, attrs, result);
+           NativeDefineDataProperty(cx, argsobj, id, v, attrs, result);
 }
 
 static bool
 DefineArgumentsIterator(JSContext* cx, Handle<ArgumentsObject*> argsobj)
 {
     RootedId iteratorId(cx, SYMBOL_TO_JSID(cx->wellKnownSymbols().iterator));
     HandlePropertyName shName = cx->names().ArrayValues;
     RootedAtom name(cx, cx->names().values);
@@ -722,17 +722,17 @@ UnmappedArgGetter(JSContext* cx, HandleO
         MOZ_ASSERT(JSID_IS_ATOM(id, cx->names().length));
         if (!argsobj.hasOverriddenLength())
             vp.setInt32(argsobj.initialLength());
     }
     return true;
 }
 
 static bool
-UnmappedArgSetter(JSContext* cx, HandleObject obj, HandleId id, MutableHandleValue vp,
+UnmappedArgSetter(JSContext* cx, HandleObject obj, HandleId id, HandleValue v,
                   ObjectOpResult& result)
 {
     if (!obj->is<UnmappedArgumentsObject>())
         return result.succeed();
     Handle<UnmappedArgumentsObject*> argsobj = obj.as<UnmappedArgumentsObject>();
 
     Rooted<PropertyDescriptor> desc(cx);
     if (!GetOwnPropertyDescriptor(cx, argsobj, id, &desc))
@@ -740,31 +740,31 @@ UnmappedArgSetter(JSContext* cx, HandleO
     MOZ_ASSERT(desc.object());
     unsigned attrs = desc.attributes();
     MOZ_ASSERT(!(attrs & JSPROP_READONLY));
     attrs &= (JSPROP_ENUMERATE | JSPROP_PERMANENT); /* only valid attributes */
 
     if (JSID_IS_INT(id)) {
         unsigned arg = unsigned(JSID_TO_INT(id));
         if (arg < argsobj->initialLength()) {
-            argsobj->setElement(cx, arg, vp);
+            argsobj->setElement(cx, arg, v);
             return result.succeed();
         }
     } else {
         MOZ_ASSERT(JSID_IS_ATOM(id, cx->names().length));
     }
 
     /*
      * For simplicity we use delete/define to replace the property with a
      * simple data property. Note that we rely on ArgumentsObject::obj_delProperty
      * to set the corresponding override-bit.
      */
     ObjectOpResult ignored;
     return NativeDeleteProperty(cx, argsobj, id, ignored) &&
-           NativeDefineDataProperty(cx, argsobj, id, vp, attrs, result);
+           NativeDefineDataProperty(cx, argsobj, id, v, attrs, result);
 }
 
 /* static */ bool
 UnmappedArgumentsObject::obj_resolve(JSContext* cx, HandleObject obj, HandleId id, bool* resolvedp)
 {
     Rooted<UnmappedArgumentsObject*> argsobj(cx, &obj->as<UnmappedArgumentsObject>());
 
     if (JSID_IS_SYMBOL(id) && JSID_TO_SYMBOL(id) == cx->wellKnownSymbols().iterator) {
diff --git a/js/src/vm/NativeObject.cpp b/js/src/vm/NativeObject.cpp
--- a/js/src/vm/NativeObject.cpp
+++ b/js/src/vm/NativeObject.cpp
@@ -2529,32 +2529,18 @@ NativeSetExistingDataProperty(JSContext*
         // Bizarre: shared (slotless) property that's writable but has no
         // JSSetterOp. JS code can't define such a property, but it can be done
         // through the JSAPI. Treat it as non-writable.
         return result.fail(JSMSG_GETTER_ONLY);
     }
 
     MOZ_ASSERT(!obj->is<WithEnvironmentObject>());  // See bug 1128681.
 
-    uint32_t sample = cx->propertyRemovals;
     RootedId id(cx, shape->propid());
-    RootedValue value(cx, v);
-    if (!CallJSSetterOp(cx, shape->setterOp(), obj, id, &value, result))
-        return false;
-
-    // Update any slot for the shape with the value produced by the setter,
-    // unless the setter deleted the shape.
-    if (shape->hasSlot() &&
-        (MOZ_LIKELY(cx->propertyRemovals == sample) ||
-         obj->contains(cx, shape)))
-    {
-        obj->setSlot(shape->slot(), value);
-    }
-
-    return true;  // result is populated by CallJSSetterOp above.
+    return CallJSSetterOp(cx, shape->setterOp(), obj, id, v, result);
 }
 
 /*
  * When a [[Set]] operation finds no existing property with the given id
  * or finds a writable data property on the prototype chain, we end up here.
  * Finish the [[Set]] by defining a new property on receiver.
  *
  * This implements ES6 draft rev 28, 9.1.9 [[Set]] steps 5.b-f, but it
@@ -2755,18 +2741,17 @@ SetExistingProperty(JSContext* cx, Handl
         // property causes the setter to be called, instead of shadowing,
         // unless the existing property is JSPROP_SHADOWABLE (see bug 552432).
         if (!shape->hasSlot() && !shape->hasShadowable()) {
             // Even weirder sub-special-case: inherited slotless data property
             // with default setter. Wut.
             if (shape->hasDefaultSetter())
                 return result.succeed();
 
-            RootedValue valCopy(cx, v);
-            return CallJSSetterOp(cx, shape->setterOp(), obj, id, &valCopy, result);
+            return CallJSSetterOp(cx, shape->setterOp(), obj, id, v, result);
         }
 
         // Shadow pobj[id] by defining a new data property receiver[id].
         // Delegate everything to SetPropertyByDefining.
         return SetPropertyByDefining(cx, id, v, receiver, result);
     }
 
     // Steps 6-11.
diff --git a/js/src/vm/Shape.cpp b/js/src/vm/Shape.cpp
--- a/js/src/vm/Shape.cpp
+++ b/js/src/vm/Shape.cpp
@@ -858,23 +858,18 @@ NativeObject::putProperty(JSContext* cx,
     }
 
     /*
      * Can't fail now, so free the previous incarnation's slot if the new shape
      * has no slot. But we do not need to free oldSlot (and must not, as trying
      * to will botch an assertion in JSObject::freeSlot) if the new last
      * property (shape here) has a slotSpan that does not cover it.
      */
-    if (hadSlot && !shape->hasSlot()) {
-        if (oldSlot < obj->slotSpan())
-            obj->freeSlot(cx, oldSlot);
-        /* Note: The optimization based on propertyRemovals is only relevant to the active thread. */
-        if (!cx->helperThread())
-            ++cx->propertyRemovals;
-    }
+    if (hadSlot && !shape->hasSlot() && oldSlot < obj->slotSpan())
+        obj->freeSlot(cx, oldSlot);
 
     obj->checkShapeConsistency();
 
     return shape;
 }
 
 /* static */ Shape*
 NativeObject::changeProperty(JSContext* cx, HandleNativeObject obj, HandleShape shape,
@@ -962,21 +957,18 @@ NativeObject::removeProperty(JSContext* 
             BaseShape* nbase = BaseShape::getUnowned(cx, base);
             if (!nbase)
                 return false;
             previous->base_ = nbase;
         }
     }
 
     /* If shape has a slot, free its slot number. */
-    if (shape->hasSlot()) {
+    if (shape->hasSlot())
         obj->freeSlot(cx, shape->slot());
-        if (!cx->helperThread())
-            ++cx->propertyRemovals;
-    }
 
     /*
      * A dictionary-mode object owns mutable, unique shapes on a non-circular
      * doubly linked list, hashed by lastProperty()->table. So we can edit the
      * list and hash in place.
      */
     if (obj->inDictionaryMode()) {
         ShapeTable* table = obj->lastProperty()->maybeTable(keep);
@@ -1045,18 +1037,16 @@ NativeObject::clear(JSContext* cx, Handl
     }
     MOZ_ASSERT(shape->isEmptyShape());
 
     if (obj->inDictionaryMode())
         shape->listp = &obj->shape_;
 
     JS_ALWAYS_TRUE(obj->setLastProperty(cx, shape));
 
-    if (!cx->helperThread())
-        ++cx->propertyRemovals;
     obj->checkShapeConsistency();
 }
 
 /* static */ bool
 NativeObject::rollbackProperties(JSContext* cx, HandleNativeObject obj, uint32_t slotSpan)
 {
     /*
      * Remove properties from this object until it has a matching slot span.
diff --git a/js/xpconnect/src/Sandbox.cpp b/js/xpconnect/src/Sandbox.cpp
--- a/js/xpconnect/src/Sandbox.cpp
+++ b/js/xpconnect/src/Sandbox.cpp
@@ -436,24 +436,24 @@ sandbox_moved(JSObject* obj, JSObject* o
     if (!sop)
         return 0;
 
     return static_cast<SandboxPrivate*>(sop)->ObjectMoved(obj, old);
 }
 
 static bool
 writeToProto_setProperty(JSContext* cx, JS::HandleObject obj, JS::HandleId id,
-                         JS::MutableHandleValue vp, JS::ObjectOpResult& result)
+                         JS::HandleValue v, JS::ObjectOpResult& result)
 {
     RootedObject proto(cx);
     if (!JS_GetPrototype(cx, obj, &proto))
         return false;
 
     RootedValue receiver(cx, ObjectValue(*proto));
-    return JS_ForwardSetPropertyTo(cx, proto, id, vp, receiver, result);
+    return JS_ForwardSetPropertyTo(cx, proto, id, v, receiver, result);
 }
 
 static bool
 writeToProto_getProperty(JSContext* cx, JS::HandleObject obj, JS::HandleId id,
                     JS::MutableHandleValue vp)
 {
     RootedObject proto(cx);
     if (!JS_GetPrototype(cx, obj, &proto))
