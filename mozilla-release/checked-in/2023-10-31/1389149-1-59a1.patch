# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1510763984 18000
# Node ID 1bfcc78675d00fece2cb38f8799226aff5743d2a
# Parent  adae9199c7f60cbf88de4391ab088196498a13c8
Bug 1389149 - Extract the logic from nsDisplayLayerEventRegions::AddFrame into a more reusable form. r=mstange

This introduces a enum bitset type that encapsulates some of the
interesting properties that frames have that make it interesting for
hit-testing in the compositor. This type is designed so it can be sent
directly to webrender and gotten back in the hit-test.

MozReview-Commit-ID: GCxV7ZaoJd1

diff --git a/gfx/src/CompositorHitTestInfo.h b/gfx/src/CompositorHitTestInfo.h
new file mode 100644
--- /dev/null
+++ b/gfx/src/CompositorHitTestInfo.h
@@ -0,0 +1,44 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef MOZILLA_GFX_COMPOSITORHITTESTINFO_H_
+#define MOZILLA_GFX_COMPOSITORHITTESTINFO_H_
+
+#include "mozilla/TypedEnumBits.h"
+
+namespace mozilla {
+namespace gfx {
+
+// This set of flags is used to figure out what information a frame has
+// that is relevant to hit-testing in the compositor. The flags are
+// intentionally set up so that if all of them are 0 the item is effectively
+// invisible to hit-testing, and no information for this frame needs to be
+// sent to the compositor.
+enum class CompositorHitTestInfo : uint8_t {
+  // Shortcut for checking that none of the flags are set
+  eInvisibleToHitTest = 0,
+
+  // The frame participates in hit-testing
+  eVisibleToHitTest = 1 << 0,
+  // The frame requires main-thread handling for events
+  eDispatchToContent = 1 << 1,
+
+  // The touch action flags are set up so that the default of
+  // touch-action:auto on an element leaves all the flags as 0.
+  eTouchActionPanXDisabled = 1 << 2,
+  eTouchActionPanYDisabled = 1 << 3,
+  eTouchActionPinchZoomDisabled = 1 << 4,
+  eTouchActionDoubleTapZoomDisabled = 1 << 5,
+  // Mask to check for all the touch-action flags at once
+  eTouchActionMask = (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5),
+};
+
+MOZ_MAKE_ENUM_CLASS_BITWISE_OPERATORS(CompositorHitTestInfo)
+
+} // namespace gfx
+} // namespace mozilla
+
+#endif /* MOZILLA_GFX_COMPOSITORHITTESTINFO_H_ */
diff --git a/gfx/src/moz.build b/gfx/src/moz.build
--- a/gfx/src/moz.build
+++ b/gfx/src/moz.build
@@ -42,16 +42,17 @@ EXPORTS += [
 ]
 
 EXPORTS.mozilla += [
     'AppUnits.h',
     'ArrayView.h',
 ]
 
 EXPORTS.mozilla.gfx += [
+    'CompositorHitTestInfo.h',
     'TiledRegion.h',
 ]
 
 if CONFIG['MOZ_X11']:
     EXPORTS.mozilla += ['X11Util.h']
     SOURCES += [
         'X11Util.cpp',
     ]
diff --git a/layout/generic/nsFrame.cpp b/layout/generic/nsFrame.cpp
--- a/layout/generic/nsFrame.cpp
+++ b/layout/generic/nsFrame.cpp
@@ -11151,16 +11151,83 @@ nsIFrame::AddSizeOfExcludingThisForTree(
   while (!iter.IsDone()) {
     for (const nsIFrame* f : iter.CurrentList()) {
       f->AddSizeOfExcludingThisForTree(aSizes);
     }
     iter.Next();
   }
 }
 
+CompositorHitTestInfo
+nsIFrame::GetCompositorHitTestInfo(nsDisplayListBuilder* aBuilder)
+{
+  CompositorHitTestInfo result = CompositorHitTestInfo::eInvisibleToHitTest;
+
+  if (aBuilder->IsInsidePointerEventsNoneDoc()) {
+    // Somewhere up the parent document chain is a subdocument with pointer-
+    // events:none set on it.
+    return result;
+  }
+  if (!GetParent()) {
+    MOZ_ASSERT(IsViewportFrame());
+    // Viewport frames are never event targets, other frames, like canvas frames,
+    // are the event targets for any regions viewport frames may cover.
+    return result;
+  }
+  uint8_t pointerEvents = StyleUserInterface()->GetEffectivePointerEvents(this);
+  if (pointerEvents == NS_STYLE_POINTER_EVENTS_NONE) {
+    return result;
+  }
+  if (!StyleVisibility()->IsVisible()) {
+    return result;
+  }
+
+  // Anything that didn't match the above conditions is visible to hit-testing.
+  result |= CompositorHitTestInfo::eVisibleToHitTest;
+
+  if (aBuilder->IsBuildingNonLayerizedScrollbar() ||
+      aBuilder->GetAncestorHasApzAwareEventHandler()) {
+    // Scrollbars may be painted into a layer below the actual layer they will
+    // scroll, and therefore wheel events may be dispatched to the outer frame
+    // instead of the intended scrollframe. To address this, we force a d-t-c
+    // region on scrollbar frames that won't be placed in their own layer. See
+    // bug 1213324 for details.
+    result |= CompositorHitTestInfo::eDispatchToContent;
+  }
+
+  nsIFrame* touchActionFrame = this;
+  if (nsIScrollableFrame* scrollFrame = nsLayoutUtils::GetScrollableFrameFor(this)) {
+    touchActionFrame = do_QueryFrame(scrollFrame);
+  }
+  uint32_t touchAction = nsLayoutUtils::GetTouchActionFromFrame(touchActionFrame);
+  // The CSS allows the syntax auto | none | [pan-x || pan-y] | manipulation
+  // so we can eliminate some combinations of things.
+  if (touchAction == NS_STYLE_TOUCH_ACTION_AUTO) {
+    // nothing to do
+  } else if (touchAction & NS_STYLE_TOUCH_ACTION_MANIPULATION) {
+    result |= CompositorHitTestInfo::eTouchActionDoubleTapZoomDisabled;
+  } else {
+    if (!(touchAction & NS_STYLE_TOUCH_ACTION_PAN_X)) {
+      result |= CompositorHitTestInfo::eTouchActionPanXDisabled;
+    }
+    if (!(touchAction & NS_STYLE_TOUCH_ACTION_PAN_Y)) {
+      result |= CompositorHitTestInfo::eTouchActionPanYDisabled;
+    }
+    if (touchAction & NS_STYLE_TOUCH_ACTION_NONE) {
+      result |= CompositorHitTestInfo::eTouchActionPinchZoomDisabled
+              | CompositorHitTestInfo::eTouchActionDoubleTapZoomDisabled;
+      // pan-x and pan-y disabled flags will already have been set above
+      MOZ_ASSERT(result & CompositorHitTestInfo::eTouchActionPanXDisabled);
+      MOZ_ASSERT(result & CompositorHitTestInfo::eTouchActionPanYDisabled);
+    }
+  }
+
+  return result;
+}
+
 // Box layout debugging
 #ifdef DEBUG_REFLOW
 int32_t gIndent2 = 0;
 
 void
 nsAdaptorAddIndents()
 {
     for(int32_t i=0; i < gIndent2; i++)
diff --git a/layout/generic/nsIFrame.h b/layout/generic/nsIFrame.h
--- a/layout/generic/nsIFrame.h
+++ b/layout/generic/nsIFrame.h
@@ -63,16 +63,17 @@
 #include "nsLayoutUtils.h"
 #include "nsQueryFrame.h"
 #include "nsString.h"
 #include "nsStyleContext.h"
 #include "nsStyleStruct.h"
 #include "Visibility.h"
 #include "nsChangeHint.h"
 #include "nsStyleContextInlines.h"
+#include "mozilla/gfx/CompositorHitTestInfo.h"
 #include "mozilla/gfx/MatrixFwd.h"
 #include "nsDisplayItemTypes.h"
 
 #ifdef ACCESSIBILITY
 #include "mozilla/a11y/AccTypes.h"
 #endif
 
 /**
@@ -4165,16 +4166,18 @@ public:
   void SetHasOverrideDirtyRegion(bool aHasDirtyRegion) { mHasOverrideDirtyRegion = aHasDirtyRegion; }
 
   bool MayHaveWillChangeBudget() { return mMayHaveWillChangeBudget; }
   void SetMayHaveWillChangeBudget(bool aHasBudget) { mMayHaveWillChangeBudget = aHasBudget; }
 
   bool BuiltBlendContainer() { return mBuiltBlendContainer; }
   void SetBuiltBlendContainer(bool aBuilt) { mBuiltBlendContainer = aBuilt; }
 
+  mozilla::gfx::CompositorHitTestInfo GetCompositorHitTestInfo(nsDisplayListBuilder* aBuilder);
+
 protected:
   static void DestroyAnonymousContent(nsPresContext* aPresContext,
                                       already_AddRefed<nsIContent>&& aContent);
 
   /**
    * Reparent this frame's view if it has one.
    */
   void ReparentFrameViewTo(nsViewManager* aViewManager,
diff --git a/layout/painting/nsDisplayList.cpp b/layout/painting/nsDisplayList.cpp
--- a/layout/painting/nsDisplayList.cpp
+++ b/layout/painting/nsDisplayList.cpp
@@ -4779,41 +4779,22 @@ nsDisplayEventReceiver::HitTest(nsDispla
 }
 
 void
 nsDisplayLayerEventRegions::AddFrame(nsDisplayListBuilder* aBuilder,
                                      nsIFrame* aFrame)
 {
   NS_ASSERTION(aBuilder->FindReferenceFrameFor(aFrame) == aBuilder->FindReferenceFrameFor(mFrame),
                "Reference frame mismatch");
-  if (aBuilder->IsInsidePointerEventsNoneDoc()) {
-    // Somewhere up the parent document chain is a subdocument with pointer-
-    // events:none set on it.
-    return;
-  }
-  if (!aFrame->GetParent()) {
-    MOZ_ASSERT(aFrame->IsViewportFrame());
-    // Viewport frames are never event targets, other frames, like canvas frames,
-    // are the event targets for any regions viewport frames may cover.
+  CompositorHitTestInfo hitInfo =
+      aFrame->GetCompositorHitTestInfo(aBuilder);
+  if (hitInfo == CompositorHitTestInfo::eInvisibleToHitTest) {
     return;
   }
 
-  uint8_t pointerEvents =
-    aFrame->StyleUserInterface()->GetEffectivePointerEvents(aFrame);
-  if (pointerEvents == NS_STYLE_POINTER_EVENTS_NONE) {
-    return;
-  }
-  bool simpleRegions = aFrame->HasAnyStateBits(NS_FRAME_SIMPLE_EVENT_REGIONS);
-  if (!simpleRegions) {
-    if (!aFrame->StyleVisibility()->IsVisible()) {
-      return;
-    }
-  }
-
-
   // XXX handle other pointerEvents values for SVG
 
   // XXX Do something clever here for the common case where the border box
   // is obviously entirely inside mHitRegion.
   nsRect borderBox;
 
   nsIScrollableFrame* scrollFrame = nsLayoutUtils::GetScrollableFrameFor(aFrame);
   if (scrollFrame) {
@@ -4833,16 +4814,21 @@ nsDisplayLayerEventRegions::AddFrame(nsD
   if (aFrame != mFrame &&
       aBuilder->IsRetainingDisplayList()) {
     aFrame->AddDisplayItem(this);
   }
 
   borderBox += aBuilder->ToReferenceFrame(aFrame);
 
   bool borderBoxHasRoundedCorners = false;
+
+  // use the NS_FRAME_SIMPLE_EVENT_REGIONS to avoid calling the slightly
+  // expensive HasNonZeroCorner function if we know from a previous run that
+  // the frame has zero corners.
+  bool simpleRegions = aFrame->HasAnyStateBits(NS_FRAME_SIMPLE_EVENT_REGIONS);
   if (!simpleRegions) {
     if (nsLayoutUtils::HasNonZeroCorner(aFrame->StyleBorder()->mBorderRadius)) {
       borderBoxHasRoundedCorners = true;
     } else {
       aFrame->AddStateBits(NS_FRAME_SIMPLE_EVENT_REGIONS);
     }
   }
 
@@ -4858,42 +4844,35 @@ nsDisplayLayerEventRegions::AddFrame(nsD
 
   if (borderBoxHasRoundedCorners ||
       (aFrame->GetStateBits() & NS_FRAME_SVG_LAYOUT)) {
     mMaybeHitRegion.Add(aFrame, borderBox);
   } else {
     mHitRegion.Add(aFrame, borderBox);
   }
 
-  if (aBuilder->IsBuildingNonLayerizedScrollbar() ||
-      aBuilder->GetAncestorHasApzAwareEventHandler())
-  {
-    // Scrollbars may be painted into a layer below the actual layer they will
-    // scroll, and therefore wheel events may be dispatched to the outer frame
-    // instead of the intended scrollframe. To address this, we force a d-t-c
-    // region on scrollbar frames that won't be placed in their own layer. See
-    // bug 1213324 for details.
+  if (hitInfo & CompositorHitTestInfo::eDispatchToContent) {
     mDispatchToContentHitRegion.Add(aFrame, borderBox);
   }
 
   // Touch action region
 
-  nsIFrame* touchActionFrame = aFrame;
-  if (scrollFrame) {
-    touchActionFrame = do_QueryFrame(scrollFrame);
-  }
-  uint32_t touchAction = nsLayoutUtils::GetTouchActionFromFrame(touchActionFrame);
-  if (touchAction != NS_STYLE_TOUCH_ACTION_AUTO) {
-    if (touchAction & NS_STYLE_TOUCH_ACTION_NONE) {
+  auto touchFlags = hitInfo & CompositorHitTestInfo::eTouchActionMask;
+  if (touchFlags) {
+    // something was disabled
+    if (touchFlags == CompositorHitTestInfo::eTouchActionMask) {
+      // everything was disabled, so touch-action:none
       mNoActionRegion.Add(aFrame, borderBox);
     } else {
-      if ((touchAction & NS_STYLE_TOUCH_ACTION_PAN_X)) {
+      if (!(hitInfo & CompositorHitTestInfo::eTouchActionPanXDisabled)) {
+        // pan-x is allowed
         mHorizontalPanRegion.Add(aFrame, borderBox);
       }
-      if ((touchAction & NS_STYLE_TOUCH_ACTION_PAN_Y)) {
+      if (!(hitInfo & CompositorHitTestInfo::eTouchActionPanYDisabled)) {
+        // pan-y is allowed
         mVerticalPanRegion.Add(aFrame, borderBox);
       }
     }
   }
 }
 
 static void
 RemoveFrameFromFrameRects(nsDisplayLayerEventRegions::FrameRects& aFrameRects, nsIFrame* aFrame)
