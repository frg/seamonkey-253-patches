# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1511766261 -28800
# Node ID 8316864557a24cb7c50ff18a6cd7d0939e87bf31
# Parent  dcfa8fccec4fa151658d2cd39b5669af6c5a6f34
Bug 1420819 - run MediaCache::Flush() off the main thread. r=bechen,gerald

MozReview-Commit-ID: 4QR1TvORZlz

diff --git a/dom/media/MediaCache.cpp b/dom/media/MediaCache.cpp
--- a/dom/media/MediaCache.cpp
+++ b/dom/media/MediaCache.cpp
@@ -422,16 +422,18 @@ protected:
   // Guess the duration until the next incoming data on aStream will be used
   TimeDuration PredictNextUseForIncomingData(AutoLock&,
                                              MediaCacheStream* aStream);
 
   // Truncate the file and index array if there are free blocks at the
   // end
   void Truncate();
 
+  void FlushInternal(AutoLock&);
+
   // There is at most one file-backed media cache.
   // It is owned by all MediaCacheStreams that use it.
   // This is a raw pointer set by GetMediaCache(), and reset by ~MediaCache(),
   // both on the main thread; and is not accessed anywhere else.
   static MediaCache* gMediaCache;
 
   // This member is main-thread only. It's used to allocate unique
   // resource IDs to streams.
@@ -690,33 +692,42 @@ MediaCacheStream::BlockList::NotifyBlock
   if (e2) {
     e2 = mEntries.PutEntry(aBlockIndex1);
     e2->mNextBlock = e2Next;
     e2->mPrevBlock = e2Prev;
   }
 }
 
 void
-MediaCache::Flush()
+MediaCache::FlushInternal(AutoLock& aLock)
 {
-  NS_ASSERTION(NS_IsMainThread(), "Only call on main thread");
-  AutoLock lock(mMonitor);
-
   for (uint32_t blockIndex = 0; blockIndex < mIndex.Length(); ++blockIndex) {
-    FreeBlock(lock, blockIndex);
+    FreeBlock(aLock, blockIndex);
   }
 
   // Truncate index array.
   Truncate();
   NS_ASSERTION(mIndex.Length() == 0, "Blocks leaked?");
   // Reset block cache to its pristine state.
   mBlockCache->Flush();
 }
 
 void
+MediaCache::Flush()
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  nsCOMPtr<nsIRunnable> r = NS_NewRunnableFunction(
+    "MediaCache::Flush", [self = RefPtr<MediaCache>(this)]() {
+      AutoLock lock(self->mMonitor);
+      self->FlushInternal(lock);
+    });
+  sThread->Dispatch(r.forget());
+}
+
+void
 MediaCache::CloseStreamsForPrivateBrowsing()
 {
   MOZ_ASSERT(NS_IsMainThread());
   for (MediaCacheStream* s : mStreams) {
     if (s->mIsPrivateBrowsing) {
       s->mClient->Close();
     }
   }
