# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1506321032 -36000
# Node ID 9c24fa59b0073d866fe44e6c969c5481d78a6793
# Parent  62cdcea5b8dd9adfb1e6ec16e3fa84bb4e81f99b
Bug 1401873 - Expose nsAtom in nsIAtom.h. r=froydnj.

This patch moves nsAtom's declaration to nsIAtom.h. In order to keep most of
nsAtom's members private the patch also does the following.

- It introduces a new class, nsAtomFriend, which encapsulates the functions
  that will need access to nsAtom's private members.

- It moves GCKind, GCAtomTable(), and GCAtomTableLocked() out of nsAtom.

- It removes the factory methods, and replaces their uses with direct
  constructor calls.

MozReview-Commit-ID: L8vfrHsR2cS

diff --git a/xpcom/ds/nsAtomTable.cpp b/xpcom/ds/nsAtomTable.cpp
--- a/xpcom/ds/nsAtomTable.cpp
+++ b/xpcom/ds/nsAtomTable.cpp
@@ -62,16 +62,42 @@ class CheckStaticAtomSizes
                   (offsetof(nsFakeStringBuffer<1>, mStringData) ==
                    sizeof(nsStringBuffer)),
                   "mocked-up strings' representations should be compatible");
   }
 };
 
 //----------------------------------------------------------------------
 
+enum class GCKind {
+  RegularOperation,
+  Shutdown,
+};
+
+// This class encapsulates the functions that need access to nsAtom's private
+// members.
+class nsAtomFriend
+{
+public:
+  static void RegisterStaticAtoms(const nsStaticAtom* aAtoms,
+                                  uint32_t aAtomCount);
+
+  static void AtomTableClearEntry(PLDHashTable* aTable,
+                                  PLDHashEntryHdr* aEntry);
+
+  static void GCAtomTableLocked(const MutexAutoLock& aProofOfLock,
+                                GCKind aKind);
+
+  static already_AddRefed<nsIAtom> Atomize(const nsACString& aUTF8String);
+  static already_AddRefed<nsIAtom> Atomize(const nsAString& aUTF16String);
+  static already_AddRefed<nsIAtom> AtomizeMainThread(const nsAString& aUTF16Str);
+};
+
+//----------------------------------------------------------------------
+
 // gUnusedAtomCount is incremented when an atom loses its last reference
 // (and thus turned into unused state), and decremented when an unused
 // atom gets a reference again. The atom table relies on this value to
 // schedule GC. This value can temporarily go below zero when multiple
 // threads are operating the same atom, so it has to be signed so that
 // we wouldn't use overflow value for comparison.
 // See Atom::DynamicAddRef and Atom::DynamicRelease.
 static Atomic<int32_t, ReleaseAcquire> gUnusedAtomCount(0);
@@ -104,129 +130,89 @@ public:
 
 private:
   nsStringBuffer* mBuffer;
 };
 
 UniquePtr<nsTArray<FakeBufferRefcountHelper>> gFakeBuffers;
 #endif
 
-class nsAtom final : public nsIAtom
+// This constructor is for dynamic atoms.
+nsAtom::nsAtom(const nsAString& aString, uint32_t aHash)
+  : mRefCnt(1)
 {
-public:
-  static already_AddRefed<nsAtom> CreateDynamic(const nsAString& aString,
-                                                uint32_t aHash)
-  {
-    // The refcount is appropriately initialized in the constructor.
-    return dont_AddRef(new nsAtom(aString, aHash));
-  }
-
-  static nsAtom* CreateStatic(nsStringBuffer* aStringBuffer, uint32_t aLength,
-                              uint32_t aHash)
-  {
-    return new nsAtom(aStringBuffer, aLength, aHash);
+  mLength = aString.Length();
+  SetKind(AtomKind::DynamicAtom);
+  RefPtr<nsStringBuffer> buf = nsStringBuffer::FromString(aString);
+  if (buf) {
+    mString = static_cast<char16_t*>(buf->Data());
+  } else {
+    const size_t size = (mLength + 1) * sizeof(char16_t);
+    buf = nsStringBuffer::Alloc(size);
+    if (MOZ_UNLIKELY(!buf)) {
+      // We OOM because atom allocations should be small and it's hard to
+      // handle them more gracefully in a constructor.
+      NS_ABORT_OOM(size);
+    }
+    mString = static_cast<char16_t*>(buf->Data());
+    CopyUnicodeTo(aString, 0, mString, mLength);
+    mString[mLength] = char16_t(0);
   }
 
-  static void GCAtomTable();
+  mHash = aHash;
+  MOZ_ASSERT(mHash == HashString(mString, mLength));
 
-  enum class GCKind {
-    RegularOperation,
-    Shutdown,
-  };
-
-  static void GCAtomTableLocked(const MutexAutoLock& aProofOfLock,
-                                GCKind aKind);
+  NS_ASSERTION(mString[mLength] == char16_t(0), "null terminated");
+  NS_ASSERTION(buf && buf->StorageSize() >= (mLength + 1) * sizeof(char16_t),
+               "enough storage");
+  NS_ASSERTION(Equals(aString), "correct data");
 
-private:
-  // This constructor is for dynamic atoms.
-  nsAtom(const nsAString& aString, uint32_t aHash)
-    : mRefCnt(1)
-  {
-    mLength = aString.Length();
-    SetKind(AtomKind::DynamicAtom);
-    RefPtr<nsStringBuffer> buf = nsStringBuffer::FromString(aString);
-    if (buf) {
-      mString = static_cast<char16_t*>(buf->Data());
-    } else {
-      const size_t size = (mLength + 1) * sizeof(char16_t);
-      buf = nsStringBuffer::Alloc(size);
-      if (MOZ_UNLIKELY(!buf)) {
-        // We OOM because atom allocations should be small and it's hard to
-        // handle them more gracefully in a constructor.
-        NS_ABORT_OOM(size);
-      }
-      mString = static_cast<char16_t*>(buf->Data());
-      CopyUnicodeTo(aString, 0, mString, mLength);
-      mString[mLength] = char16_t(0);
-    }
+  // Take ownership of buffer
+  mozilla::Unused << buf.forget();
+}
 
-    mHash = aHash;
-    MOZ_ASSERT(mHash == HashString(mString, mLength));
-
-    NS_ASSERTION(mString[mLength] == char16_t(0), "null terminated");
-    NS_ASSERTION(buf && buf->StorageSize() >= (mLength + 1) * sizeof(char16_t),
-                 "enough storage");
-    NS_ASSERTION(Equals(aString), "correct data");
-
-    // Take ownership of buffer
-    mozilla::Unused << buf.forget();
-  }
-
-  // This constructor is for static atoms.
-  nsAtom(nsStringBuffer* aStringBuffer, uint32_t aLength, uint32_t aHash)
-  {
-    mLength = aLength;
-    SetKind(AtomKind::StaticAtom);
-    mString = static_cast<char16_t*>(aStringBuffer->Data());
+// This constructor is for static atoms.
+nsAtom::nsAtom(nsStringBuffer* aStringBuffer, uint32_t aLength, uint32_t aHash)
+{
+  mLength = aLength;
+  SetKind(AtomKind::StaticAtom);
+  mString = static_cast<char16_t*>(aStringBuffer->Data());
 
 #if defined(NS_BUILD_REFCNT_LOGGING)
-    MOZ_ASSERT(NS_IsMainThread());
-    if (!gFakeBuffers) {
-      gFakeBuffers = MakeUnique<nsTArray<FakeBufferRefcountHelper>>();
-    }
-    gFakeBuffers->AppendElement(aStringBuffer);
+  MOZ_ASSERT(NS_IsMainThread());
+  if (!gFakeBuffers) {
+    gFakeBuffers = MakeUnique<nsTArray<FakeBufferRefcountHelper>>();
+  }
+  gFakeBuffers->AppendElement(aStringBuffer);
 #endif
 
-    // Technically we could currently avoid doing this addref by instead making
-    // the static atom buffers have an initial refcount of 2.
-    aStringBuffer->AddRef();
+  // Technically we could currently avoid doing this addref by instead making
+  // the static atom buffers have an initial refcount of 2.
+  aStringBuffer->AddRef();
 
-    mHash = aHash;
-    MOZ_ASSERT(mHash == HashString(mString, mLength));
-
-    MOZ_ASSERT(mString[mLength] == char16_t(0), "null terminated");
-    MOZ_ASSERT(aStringBuffer &&
-               aStringBuffer->StorageSize() == (mLength + 1) * sizeof(char16_t),
-               "correct storage");
-  }
+  mHash = aHash;
+  MOZ_ASSERT(mHash == HashString(mString, mLength));
 
-public:
-  // We don't need a virtual destructor because we always delete via an nsAtom*
-  // pointer (in AtomTableClearEntry() for static atoms, and in
-  // GCAtomTableLocked() for dynamic atoms), not an nsIAtom* pointer.
-  ~nsAtom()
-  {
-    if (IsDynamicAtom()) {
-      nsStringBuffer::FromData(mString)->Release();
-    } else {
-      MOZ_ASSERT(IsStaticAtom());
-    }
+  MOZ_ASSERT(mString[mLength] == char16_t(0), "null terminated");
+  MOZ_ASSERT(aStringBuffer &&
+             aStringBuffer->StorageSize() == (mLength + 1) * sizeof(char16_t),
+             "correct storage");
+}
+
+// We don't need a virtual destructor because we always delete via an nsAtom*
+// pointer (in AtomTableClearEntry() for static atoms, and in
+// GCAtomTableLocked() for dynamic atoms), not an nsIAtom* pointer.
+nsAtom::~nsAtom()
+{
+  if (IsDynamicAtom()) {
+    nsStringBuffer::FromData(mString)->Release();
+  } else {
+    MOZ_ASSERT(IsStaticAtom());
   }
-
-  NS_DECL_NSIATOM
-  NS_IMETHOD QueryInterface(REFNSIID aIID, void** aInstancePtr) final;
-  typedef mozilla::TrueType HasThreadSafeRefCnt;
-
-  MozExternalRefCountType DynamicAddRef();
-  MozExternalRefCountType DynamicRelease();
-
-protected:
-  ThreadSafeAutoRefCnt mRefCnt;
-  NS_DECL_OWNINGTHREAD
-};
+}
 
 NS_IMPL_QUERY_INTERFACE(nsAtom, nsIAtom);
 
 NS_IMETHODIMP
 nsAtom::ScriptableToString(nsAString& aBuf)
 {
   nsStringBuffer::FromData(mString)->ToString(mLength, aBuf);
   return NS_OK;
@@ -373,18 +359,18 @@ AtomTableMatchKey(const PLDHashEntryHdr*
       CompareUTF8toUTF16(nsDependentCSubstring(k->mUTF8String,
                                                k->mUTF8String + k->mLength),
                          nsDependentAtomString(he->mAtom)) == 0;
   }
 
   return he->mAtom->Equals(k->mUTF16String, k->mLength);
 }
 
-static void
-AtomTableClearEntry(PLDHashTable* aTable, PLDHashEntryHdr* aEntry)
+void
+nsAtomFriend::AtomTableClearEntry(PLDHashTable* aTable, PLDHashEntryHdr* aEntry)
 {
   auto entry = static_cast<AtomTableEntry*>(aEntry);
   nsAtom* atom = entry->mAtom;
   if (atom->IsStaticAtom()) {
     // This case -- when the entry being cleared holds a static atom -- only
     // occurs when gAtomTable is destroyed, whereupon all static atoms within it
     // must be explicitly deleted.
     delete atom;
@@ -396,37 +382,28 @@ AtomTableInitEntry(PLDHashEntryHdr* aEnt
 {
   static_cast<AtomTableEntry*>(aEntry)->mAtom = nullptr;
 }
 
 static const PLDHashTableOps AtomTableOps = {
   AtomTableGetHash,
   AtomTableMatchKey,
   PLDHashTable::MoveEntryStub,
-  AtomTableClearEntry,
+  nsAtomFriend::AtomTableClearEntry,
   AtomTableInitEntry
 };
 
 //----------------------------------------------------------------------
 
 #define RECENTLY_USED_MAIN_THREAD_ATOM_CACHE_SIZE 31
 static nsAtom*
   sRecentlyUsedMainThreadAtoms[RECENTLY_USED_MAIN_THREAD_ATOM_CACHE_SIZE] = {};
 
 void
-nsAtom::GCAtomTable()
-{
-  if (NS_IsMainThread()) {
-    MutexAutoLock lock(*gAtomTableLock);
-    GCAtomTableLocked(lock, GCKind::RegularOperation);
-  }
-}
-
-void
-nsAtom::GCAtomTableLocked(const MutexAutoLock& aProofOfLock, GCKind aKind)
+nsAtomFriend::GCAtomTableLocked(const MutexAutoLock& aProofOfLock, GCKind aKind)
 {
   MOZ_ASSERT(NS_IsMainThread());
   for (uint32_t i = 0; i < RECENTLY_USED_MAIN_THREAD_ATOM_CACHE_SIZE; ++i) {
     sRecentlyUsedMainThreadAtoms[i] = nullptr;
   }
 
   int32_t removedCount = 0; // Use a non-atomic temporary for cheaper increments.
   nsAutoCString nonZeroRefcountAtoms;
@@ -483,16 +460,25 @@ nsAtom::GCAtomTableLocked(const MutexAut
   // so we won't try to resurrect a zero refcount atom while trying to delete
   // it.
 
   MOZ_ASSERT_IF(aKind == GCKind::Shutdown, removedCount == gUnusedAtomCount);
 
   gUnusedAtomCount -= removedCount;
 }
 
+static void
+GCAtomTable()
+{
+  if (NS_IsMainThread()) {
+    MutexAutoLock lock(*gAtomTableLock);
+    nsAtomFriend::GCAtomTableLocked(lock, GCKind::RegularOperation);
+  }
+}
+
 MozExternalRefCountType
 nsAtom::DynamicAddRef()
 {
   MOZ_ASSERT(IsDynamicAtom());
   nsrefcnt count = ++mRefCnt;
   if (count == 1) {
     gUnusedAtomCount--;
   }
@@ -613,17 +599,17 @@ NS_ShutdownAtomTable()
   delete gStaticAtomTable;
   gStaticAtomTable = nullptr;
 
 #ifdef NS_FREE_PERMANENT_DATA
   // Do a final GC to satisfy leak checking. We skip this step in release
   // builds.
   {
     MutexAutoLock lock(*gAtomTableLock);
-    nsAtom::GCAtomTableLocked(lock, nsAtom::GCKind::Shutdown);
+    nsAtomFriend::GCAtomTableLocked(lock, GCKind::Shutdown);
   }
 #endif
 
   delete gAtomTable;
   gAtomTable = nullptr;
   delete gAtomTableLock;
   gAtomTableLock = nullptr;
 }
@@ -660,17 +646,18 @@ GetAtomHashEntry(const char16_t* aString
 {
   gAtomTableLock->AssertCurrentThreadOwns();
   AtomTableKey key(aString, aLength, aHashOut);
   // This is an infallible add.
   return static_cast<AtomTableEntry*>(gAtomTable->Add(&key));
 }
 
 void
-RegisterStaticAtoms(const nsStaticAtom* aAtoms, uint32_t aAtomCount)
+nsAtomFriend::RegisterStaticAtoms(const nsStaticAtom* aAtoms,
+                                  uint32_t aAtomCount)
 {
   MutexAutoLock lock(*gAtomTableLock);
 
   MOZ_RELEASE_ASSERT(!gStaticAtomTableSealed,
                      "Atom table has already been sealed!");
 
   if (!gStaticAtomTable) {
     gStaticAtomTable = new StaticAtomTable();
@@ -697,38 +684,44 @@ RegisterStaticAtoms(const nsStaticAtom* 
       // C++ here, not Smalltalk.
       if (!atom->IsStaticAtom()) {
         nsAutoCString name;
         atom->ToUTF8String(name);
         MOZ_CRASH_UNSAFE_PRINTF(
           "Static atom registration for %s should be pushed back", name.get());
       }
     } else {
-      atom = nsAtom::CreateStatic(stringBuffer, stringLen, hash);
+      atom = new nsAtom(stringBuffer, stringLen, hash);
       he->mAtom = atom;
     }
     *atomp = atom;
 
     if (!gStaticAtomTableSealed) {
       StaticAtomEntry* entry =
         gStaticAtomTable->PutEntry(nsDependentAtomString(atom));
       MOZ_ASSERT(atom->IsStaticAtom());
       entry->mAtom = atom;
     }
   }
 }
 
+void
+RegisterStaticAtoms(const nsStaticAtom* aAtoms, uint32_t aAtomCount)
+{
+  nsAtomFriend::RegisterStaticAtoms(aAtoms, aAtomCount);
+}
+
 already_AddRefed<nsIAtom>
 NS_Atomize(const char* aUTF8String)
 {
-  return NS_Atomize(nsDependentCString(aUTF8String));
+  return nsAtomFriend::Atomize(nsDependentCString(aUTF8String));
 }
 
 already_AddRefed<nsIAtom>
-NS_Atomize(const nsACString& aUTF8String)
+nsAtomFriend::Atomize(const nsACString& aUTF8String)
 {
   MutexAutoLock lock(*gAtomTableLock);
   uint32_t hash;
   AtomTableEntry* he = GetAtomHashEntry(aUTF8String.Data(),
                                         aUTF8String.Length(),
                                         &hash);
 
   if (he->mAtom) {
@@ -737,52 +730,64 @@ NS_Atomize(const nsACString& aUTF8String
     return atom.forget();
   }
 
   // This results in an extra addref/release of the nsStringBuffer.
   // Unfortunately there doesn't seem to be any APIs to avoid that.
   // Actually, now there is, sort of: ForgetSharedBuffer.
   nsString str;
   CopyUTF8toUTF16(aUTF8String, str);
-  RefPtr<nsAtom> atom = nsAtom::CreateDynamic(str, hash);
+  RefPtr<nsAtom> atom = dont_AddRef(new nsAtom(str, hash));
 
   he->mAtom = atom;
 
   return atom.forget();
 }
 
 already_AddRefed<nsIAtom>
-NS_Atomize(const char16_t* aUTF16String)
+NS_Atomize(const nsACString& aUTF8String)
 {
-  return NS_Atomize(nsDependentString(aUTF16String));
+  return nsAtomFriend::Atomize(aUTF8String);
 }
 
 already_AddRefed<nsIAtom>
-NS_Atomize(const nsAString& aUTF16String)
+NS_Atomize(const char16_t* aUTF16String)
+{
+  return nsAtomFriend::Atomize(nsDependentString(aUTF16String));
+}
+
+already_AddRefed<nsIAtom>
+nsAtomFriend::Atomize(const nsAString& aUTF16String)
 {
   MutexAutoLock lock(*gAtomTableLock);
   uint32_t hash;
   AtomTableEntry* he = GetAtomHashEntry(aUTF16String.Data(),
                                         aUTF16String.Length(),
                                         &hash);
 
   if (he->mAtom) {
     nsCOMPtr<nsIAtom> atom = he->mAtom;
 
     return atom.forget();
   }
 
-  RefPtr<nsAtom> atom = nsAtom::CreateDynamic(aUTF16String, hash);
+  RefPtr<nsAtom> atom = dont_AddRef(new nsAtom(aUTF16String, hash));
   he->mAtom = atom;
 
   return atom.forget();
 }
 
 already_AddRefed<nsIAtom>
-NS_AtomizeMainThread(const nsAString& aUTF16String)
+NS_Atomize(const nsAString& aUTF16String)
+{
+  return nsAtomFriend::Atomize(aUTF16String);
+}
+
+already_AddRefed<nsIAtom>
+nsAtomFriend::AtomizeMainThread(const nsAString& aUTF16String)
 {
   MOZ_ASSERT(NS_IsMainThread());
   nsCOMPtr<nsIAtom> retVal;
   uint32_t hash;
   AtomTableKey key(aUTF16String.Data(), aUTF16String.Length(), &hash);
   uint32_t index = hash % RECENTLY_USED_MAIN_THREAD_ATOM_CACHE_SIZE;
   nsAtom* atom = sRecentlyUsedMainThreadAtoms[index];
   if (atom) {
@@ -796,29 +801,35 @@ NS_AtomizeMainThread(const nsAString& aU
   }
 
   MutexAutoLock lock(*gAtomTableLock);
   AtomTableEntry* he = static_cast<AtomTableEntry*>(gAtomTable->Add(&key));
 
   if (he->mAtom) {
     retVal = he->mAtom;
   } else {
-    RefPtr<nsAtom> newAtom = nsAtom::CreateDynamic(aUTF16String, hash);
+    RefPtr<nsAtom> newAtom = dont_AddRef(new nsAtom(aUTF16String, hash));
     he->mAtom = newAtom;
     retVal = newAtom.forget();
   }
 
   sRecentlyUsedMainThreadAtoms[index] = he->mAtom;
   return retVal.forget();
 }
 
+already_AddRefed<nsIAtom>
+NS_AtomizeMainThread(const nsAString& aUTF16String)
+{
+  return nsAtomFriend::AtomizeMainThread(aUTF16String);
+}
+
 nsrefcnt
 NS_GetNumberOfAtoms(void)
 {
-  nsAtom::GCAtomTable(); // Trigger a GC so we return a deterministic result.
+  GCAtomTable(); // Trigger a GC so we return a deterministic result.
   MutexAutoLock lock(*gAtomTableLock);
   return gAtomTable->EntryCount();
 }
 
 int32_t
 NS_GetUnusedAtomCount(void)
 {
   return gUnusedAtomCount;
diff --git a/xpcom/ds/nsIAtom.h.1401873-2.later b/xpcom/ds/nsIAtom.h.1401873-2.later
new file mode 100644
--- /dev/null
+++ b/xpcom/ds/nsIAtom.h.1401873-2.later
@@ -0,0 +1,42 @@
+--- nsIAtom.h
++++ nsIAtom.h
+@@ -92,16 +92,39 @@ protected:
+ };
+ 
+ NS_DEFINE_STATIC_IID_ACCESSOR(nsIAtom, NS_IATOM_IID)
+ 
+ #define NS_DECL_NSIATOM \
+   NS_IMETHOD ToUTF8String(nsACString& _retval) override; \
+   NS_IMETHOD_(size_t) SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) override;
+ 
++class nsAtom final : public nsIAtom
++{
++public:
++  NS_DECL_NSIATOM
++  NS_IMETHOD QueryInterface(REFNSIID aIID, void** aInstancePtr) final;
++  typedef mozilla::TrueType HasThreadSafeRefCnt;
++
++private:
++  friend class nsIAtom;
++  friend class nsAtomFriend;
++
++  // Construction and destruction is done entirely by |friend|s.
++  nsAtom(const nsAString& aString, uint32_t aHash);
++  nsAtom(nsStringBuffer* aStringBuffer, uint32_t aLength, uint32_t aHash);
++  ~nsAtom();
++
++  MozExternalRefCountType DynamicAddRef();
++  MozExternalRefCountType DynamicRelease();
++
++  mozilla::ThreadSafeAutoRefCnt mRefCnt;
++  NS_DECL_OWNINGTHREAD
++};
++
+ // The four forms of NS_Atomize (for use with |nsCOMPtr<nsIAtom>|) return the
+ // atom for the string given. At any given time there will always be one atom
+ // representing a given string. Atoms are intended to make string comparison
+ // cheaper by simplifying it to pointer equality. A pointer to the atom that
+ // does not own a reference is not guaranteed to be valid.
+ 
+ // Find an atom that matches the given UTF-8 string. The string is assumed to
+ // be zero terminated. Never returns null.
diff --git a/xpcom/ds/nsIAtom.idl b/xpcom/ds/nsIAtom.idl
--- a/xpcom/ds/nsIAtom.idl
+++ b/xpcom/ds/nsIAtom.idl
@@ -117,16 +117,43 @@ protected:
    * nsStringBuffer::FromData() calls above are valid.
    */
   char16_t* mString;
 %}
 };
 
 
 %{C++
+class nsAtom final : public nsIAtom
+{
+public:
+  NS_DECL_NSIATOM
+  NS_IMETHOD QueryInterface(REFNSIID aIID, void** aInstancePtr) final;
+  typedef mozilla::TrueType HasThreadSafeRefCnt;
+
+private:
+  friend class nsIAtom;
+  friend class nsAtomFriend;
+
+  // Construction and destruction is done entirely by |friend|s.
+  nsAtom(const nsAString& aString, uint32_t aHash);
+  nsAtom(nsStringBuffer* aStringBuffer, uint32_t aLength, uint32_t aHash);
+  ~nsAtom();
+
+  MozExternalRefCountType DynamicAddRef();
+  MozExternalRefCountType DynamicRelease();
+
+  mozilla::ThreadSafeAutoRefCnt mRefCnt;
+  NS_DECL_OWNINGTHREAD
+};
+
+%}
+
+
+%{C++
 /*
  * The four forms of NS_Atomize (for use with |nsCOMPtr<nsIAtom>|) return the
  * atom for the string given. At any given time there will always be one atom
  * representing a given string. Atoms are intended to make string comparison
  * cheaper by simplifying it to pointer equality. A pointer to the atom that
  * does not own a reference is not guaranteed to be valid.
  */
 
