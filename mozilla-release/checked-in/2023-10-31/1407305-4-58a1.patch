# HG changeset patch
# User Ehsan Akhgari <ehsan@mozilla.com>
# Date 1507415810 14400
# Node ID e22de36807c6229b3710382f0094ffe5e31eaa26
# Parent  5540a409d0fff96cb01d43518b775b63bfc5b25c
Bug 1407305 - Part 4: Avoid using GetChildAt() in EditorBase::GetNextNode(); r=masayuki

diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -3305,39 +3305,41 @@ EditorBase::GetPriorNode(nsINode* aParen
 
   // restart the search from the non-editable node we just found
   return GetPriorNode(resultNode, aEditableNode, aNoBlockCrossing);
 }
 
 nsIContent*
 EditorBase::GetNextNode(nsINode* aParentNode,
                         int32_t aOffset,
+                        nsINode* aChildAtOffset,
                         bool aEditableNode,
                         bool aNoBlockCrossing)
 {
   MOZ_ASSERT(aParentNode);
 
   // if aParentNode is a text node, use its location instead
   if (aParentNode->NodeType() == nsIDOMNode::TEXT_NODE) {
     nsINode* parent = aParentNode->GetParentNode();
     NS_ENSURE_TRUE(parent, nullptr);
     aOffset = parent->IndexOf(aParentNode) + 1; // _after_ the text node
     aParentNode = parent;
   }
 
   // look at the child at 'aOffset'
-  nsIContent* child = aParentNode->GetChildAt(aOffset);
-  if (child) {
-    if (aNoBlockCrossing && IsBlockNode(child)) {
-      return child;
+  if (aChildAtOffset) {
+    if (aNoBlockCrossing && IsBlockNode(aChildAtOffset)) {
+      MOZ_ASSERT(aChildAtOffset->IsContent());
+      return aChildAtOffset->AsContent();
     }
 
-    nsIContent* resultNode = GetLeftmostChild(child, aNoBlockCrossing);
+    nsIContent* resultNode = GetLeftmostChild(aChildAtOffset, aNoBlockCrossing);
     if (!resultNode) {
-      return child;
+      MOZ_ASSERT(aChildAtOffset->IsContent());
+      return aChildAtOffset->AsContent();
     }
 
     if (!IsDescendantOfEditorRoot(resultNode)) {
       return nullptr;
     }
 
     if (!aEditableNode || IsEditable(resultNode)) {
       return resultNode;
@@ -4652,17 +4654,17 @@ EditorBase::CreateTxnForDeleteRange(nsRa
   }
 
   // we're either deleting a node or chardata, need to dig into the next/prev
   // node to find out
   nsCOMPtr<nsINode> selectedNode;
   if (aAction == ePrevious) {
     selectedNode = GetPriorNode(node, offset, child, true);
   } else if (aAction == eNext) {
-    selectedNode = GetNextNode(node, offset, true);
+    selectedNode = GetNextNode(node, offset, child, true);
   }
 
   while (selectedNode &&
          selectedNode->IsNodeOfType(nsINode::eDATA_NODE) &&
          !selectedNode->Length()) {
     // Can't delete an empty chardata node (bug 762183)
     if (aAction == ePrevious) {
       selectedNode = GetPriorNode(selectedNode, true);
diff --git a/editor/libeditor/EditorBase.h b/editor/libeditor/EditorBase.h
--- a/editor/libeditor/EditorBase.h
+++ b/editor/libeditor/EditorBase.h
@@ -726,16 +726,17 @@ public:
                           bool aEditableNode,
                           bool bNoBlockCrossing = false);
 
   /**
    * And another version that takes a {parent,offset} pair rather than a node.
    */
   nsIContent* GetNextNode(nsINode* aParentNode,
                           int32_t aOffset,
+                          nsINode* aChildAtOffset,
                           bool aEditableNode,
                           bool aNoBlockCrossing = false);
 
   /**
    * Helper for GetNextNode() and GetPriorNode().
    */
   nsIContent* FindNode(nsINode* aCurrentNode,
                        bool aGoForward,
diff --git a/editor/libeditor/HTMLEditRules.cpp b/editor/libeditor/HTMLEditRules.cpp
--- a/editor/libeditor/HTMLEditRules.cpp
+++ b/editor/libeditor/HTMLEditRules.cpp
@@ -849,28 +849,29 @@ HTMLEditRules::GetAlignment(bool* aMixed
 
   int32_t rootOffset = root->GetParentNode() ?
                        root->GetParentNode()->IndexOf(root) : -1;
 
   NS_ENSURE_STATE(selection->GetRangeAt(0) &&
                   selection->GetRangeAt(0)->GetStartContainer());
   OwningNonNull<nsINode> parent =
     *selection->GetRangeAt(0)->GetStartContainer();
+  nsIContent* child = selection->GetRangeAt(0)->GetChildAtStartOffset();
   int32_t offset = selection->GetRangeAt(0)->StartOffset();
 
   // Is the selection collapsed?
   nsCOMPtr<nsINode> nodeToExamine;
   if (selection->Collapsed() || parent->GetAsText()) {
     // If selection is collapsed, we want to look at 'parent' and its ancestors
     // for divs with alignment on them.  If we are in a text node, then that is
     // the node of interest.
     nodeToExamine = parent;
   } else if (parent->IsHTMLElement(nsGkAtoms::html) && offset == rootOffset) {
     // If we have selected the body, let's look at the first editable node
-    nodeToExamine = htmlEditor->GetNextNode(parent, offset, true);
+    nodeToExamine = htmlEditor->GetNextNode(parent, offset, child, true);
   } else {
     nsTArray<RefPtr<nsRange>> arrayOfRanges;
     GetPromotedRanges(selection, arrayOfRanges, EditAction::align);
 
     // Use these ranges to construct a list of nodes to act on.
     nsTArray<OwningNonNull<nsINode>> arrayOfNodes;
     nsresult rv = GetNodesForOperation(arrayOfRanges, arrayOfNodes,
                                        EditAction::align, TouchContent::no);
@@ -5046,18 +5047,19 @@ HTMLEditRules::CheckForEmptyBlock(nsINod
         }
         // Else just let selection percolate up.  We'll adjust it in
         // AfterEdit()
       }
     } else {
       if (aAction == nsIEditor::eNext || aAction == nsIEditor::eNextWord ||
           aAction == nsIEditor::eToEndOfLine) {
         // Move to the start of the next node, if any
-        nsCOMPtr<nsIContent> nextNode = htmlEditor->GetNextNode(blockParent,
-                                                                offset + 1, true);
+        nsINode* child = emptyBlock->GetNextSibling();
+        nsCOMPtr<nsIContent> nextNode =
+          htmlEditor->GetNextNode(blockParent, offset + 1, child, true);
         if (nextNode) {
           EditorDOMPoint pt = GetGoodSelPointForNode(*nextNode, aAction);
           nsresult rv = aSelection->Collapse(pt.node, pt.offset);
           NS_ENSURE_SUCCESS(rv, rv);
         } else {
           // Adjust selection to be right after it.
           nsresult rv = aSelection->Collapse(blockParent, offset + 1);
           NS_ENSURE_SUCCESS(rv, rv);
diff --git a/editor/libeditor/HTMLEditor.cpp b/editor/libeditor/HTMLEditor.cpp
--- a/editor/libeditor/HTMLEditor.cpp
+++ b/editor/libeditor/HTMLEditor.cpp
@@ -3945,17 +3945,18 @@ HTMLEditor::GetNextHTMLNode(nsIDOMNode* 
  * GetNextHTMLNode() is same as above but takes {parent,offset} instead of node.
  */
 nsIContent*
 HTMLEditor::GetNextHTMLNode(nsINode* aParent,
                             int32_t aOffset,
                             nsINode* aChildAtOffset,
                             bool aNoBlockCrossing)
 {
-  nsIContent* content = GetNextNode(aParent, aOffset, true, aNoBlockCrossing);
+  nsIContent* content = GetNextNode(aParent, aOffset, aChildAtOffset,
+                                    true, aNoBlockCrossing);
   if (content && !IsDescendantOfEditorRoot(content)) {
     return nullptr;
   }
   return content;
 }
 
 bool
 HTMLEditor::IsFirstEditableChild(nsINode* aNode)
