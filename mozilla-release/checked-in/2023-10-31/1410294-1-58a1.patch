# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1508468687 -39600
# Node ID 26304529e394dab1472f6d4cd7f6654f4a41b95f
# Parent  afa7a493594213a10f277ea4c3a083ddf5375830
Bug 1410294 (part 1) - Rename nsStaticAtom as nsStaticAtomSetup. r=froydnj.

Because it's the type we use to set up static atoms at startup, not the static
atom itself.

The patch accordingly renames some parameters, variables, and NS_STATIC_ATOM,
for consistency.

MozReview-Commit-ID: 1a0KvhYNNw2

diff --git a/dom/base/nsAtomListUtils.cpp b/dom/base/nsAtomListUtils.cpp
--- a/dom/base/nsAtomListUtils.cpp
+++ b/dom/base/nsAtomListUtils.cpp
@@ -9,18 +9,18 @@
  */
 
 #include "nsAtomListUtils.h"
 #include "nsIAtom.h"
 #include "nsStaticAtom.h"
 
 /* static */ bool
 nsAtomListUtils::IsMember(nsIAtom *aAtom,
-                          const nsStaticAtom* aInfo,
-                          uint32_t aInfoCount)
+                          const nsStaticAtomSetup* aSetup,
+                          uint32_t aCount)
 {
-    for (const nsStaticAtom *info = aInfo, *info_end = aInfo + aInfoCount;
-         info != info_end; ++info) {
-        if (aAtom == *(info->mAtom))
+    for (const nsStaticAtomSetup *setup = aSetup, *setup_end = aSetup + aCount;
+         setup != setup_end; ++setup) {
+        if (aAtom == *(setup->mAtom))
             return true;
     }
     return false;
 }
diff --git a/dom/base/nsAtomListUtils.h b/dom/base/nsAtomListUtils.h
--- a/dom/base/nsAtomListUtils.h
+++ b/dom/base/nsAtomListUtils.h
@@ -9,18 +9,18 @@
  */
 
 #ifndef nsAtomListUtils_h__
 #define nsAtomListUtils_h__
 
 #include <stdint.h>
 
 class nsIAtom;
-struct nsStaticAtom;
+struct nsStaticAtomSetup;
 
 class nsAtomListUtils {
 public:
-    static bool IsMember(nsIAtom *aAtom,
-                           const nsStaticAtom* aInfo,
-                           uint32_t aInfoCount);
+  static bool IsMember(nsIAtom *aAtom,
+                       const nsStaticAtomSetup* aSetup,
+                       uint32_t aCount);
 };
 
 #endif /* !defined(nsAtomListUtils_h__) */
diff --git a/dom/base/nsGkAtoms.cpp b/dom/base/nsGkAtoms.cpp
--- a/dom/base/nsGkAtoms.cpp
+++ b/dom/base/nsGkAtoms.cpp
@@ -19,19 +19,19 @@ using namespace mozilla;
 #define GK_ATOM(name_, value_) nsIAtom* nsGkAtoms::name_;
 #include "nsGkAtomList.h"
 #undef GK_ATOM
 
 #define GK_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
 #include "nsGkAtomList.h"
 #undef GK_ATOM
 
-static const nsStaticAtom GkAtoms_info[] = {
-#define GK_ATOM(name_, value_) NS_STATIC_ATOM(name_##_buffer, &nsGkAtoms::name_),
+static const nsStaticAtomSetup sGkAtomSetup[] = {
+#define GK_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(name_##_buffer, &nsGkAtoms::name_),
 #include "nsGkAtomList.h"
 #undef GK_ATOM
 };
 
 void nsGkAtoms::AddRefAtoms()
 {
-  NS_RegisterStaticAtoms(GkAtoms_info);
+  NS_RegisterStaticAtoms(sGkAtomSetup);
 }
 
diff --git a/editor/txtsvc/nsTextServicesDocument.cpp b/editor/txtsvc/nsTextServicesDocument.cpp
--- a/editor/txtsvc/nsTextServicesDocument.cpp
+++ b/editor/txtsvc/nsTextServicesDocument.cpp
@@ -27,17 +27,17 @@
 #include "nsIPlaintextEditor.h"         // for nsIPlaintextEditor
 #include "nsISelection.h"               // for nsISelection
 #include "nsISelectionController.h"     // for nsISelectionController, etc
 #include "nsISupportsBase.h"            // for nsISupports
 #include "nsISupportsUtils.h"           // for NS_IF_ADDREF, NS_ADDREF, etc
 #include "nsITextServicesFilter.h"      // for nsITextServicesFilter
 #include "nsIWordBreaker.h"             // for nsWordRange, nsIWordBreaker
 #include "nsRange.h"                    // for nsRange
-#include "nsStaticAtom.h"               // for NS_STATIC_ATOM, etc
+#include "nsStaticAtom.h"               // for NS_STATIC_ATOM_SETUP, etc
 #include "nsString.h"                   // for nsString, nsAutoString
 #include "nsTextServicesDocument.h"
 #include "nscore.h"                     // for nsresult, NS_IMETHODIMP, etc
 
 #define LOCK_DOC(doc)
 #define UNLOCK_DOC(doc)
 
 using namespace mozilla;
@@ -92,23 +92,23 @@ nsTextServicesDocument::~nsTextServicesD
 #define TS_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
 #include "nsTSAtomList.h" // IWYU pragma: keep
 #undef TS_ATOM
 
 /* static */
 void
 nsTextServicesDocument::RegisterAtoms()
 {
-  static const nsStaticAtom ts_atoms[] = {
-#define TS_ATOM(name_, value_) NS_STATIC_ATOM(name_##_buffer, &name_),
+  static const nsStaticAtomSetup sTSAtomSetup[] = {
+#define TS_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(name_##_buffer, &name_),
 #include "nsTSAtomList.h" // IWYU pragma: keep
 #undef TS_ATOM
   };
 
-  NS_RegisterStaticAtoms(ts_atoms);
+  NS_RegisterStaticAtoms(sTSAtomSetup);
 }
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(nsTextServicesDocument)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(nsTextServicesDocument)
 
 NS_INTERFACE_MAP_BEGIN(nsTextServicesDocument)
   NS_INTERFACE_MAP_ENTRY(nsITextServicesDocument)
   NS_INTERFACE_MAP_ENTRY(nsIEditActionListener)
diff --git a/layout/style/nsCSSAnonBoxes.cpp b/layout/style/nsCSSAnonBoxes.cpp
--- a/layout/style/nsCSSAnonBoxes.cpp
+++ b/layout/style/nsCSSAnonBoxes.cpp
@@ -20,67 +20,65 @@ using namespace mozilla;
 #include "nsCSSAnonBoxList.h"
 #undef CSS_ANON_BOX
 
 #define CSS_ANON_BOX(name_, value_) \
   NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
 #include "nsCSSAnonBoxList.h"
 #undef CSS_ANON_BOX
 
-static const nsStaticAtom CSSAnonBoxes_info[] = {
+static const nsStaticAtomSetup sCSSAnonBoxAtomSetup[] = {
   // Put the non-inheriting anon boxes first, so we can index into them easily.
 #define CSS_ANON_BOX(name_, value_) /* nothing */
 #define CSS_NON_INHERITING_ANON_BOX(name_, value_) \
-  NS_STATIC_ATOM(name_##_buffer, (nsIAtom**)&nsCSSAnonBoxes::name_),
+  NS_STATIC_ATOM_SETUP(name_##_buffer, (nsIAtom**)&nsCSSAnonBoxes::name_),
 #include "nsCSSAnonBoxList.h"
 #undef CSS_NON_INHERITING_ANON_BOX
 #undef CSS_ANON_BOX
 
 #define CSS_ANON_BOX(name_, value_) \
-  NS_STATIC_ATOM(name_##_buffer, (nsIAtom**)&nsCSSAnonBoxes::name_),
+  NS_STATIC_ATOM_SETUP(name_##_buffer, (nsIAtom**)&nsCSSAnonBoxes::name_),
 #define CSS_NON_INHERITING_ANON_BOX(name_, value_) /* nothing */
 #include "nsCSSAnonBoxList.h"
 #undef CSS_NON_INHERITING_ANON_BOX
 #undef CSS_ANON_BOX
 };
 
 void nsCSSAnonBoxes::AddRefAtoms()
 {
-  NS_RegisterStaticAtoms(CSSAnonBoxes_info);
+  NS_RegisterStaticAtoms(sCSSAnonBoxAtomSetup);
 }
 
 bool nsCSSAnonBoxes::IsAnonBox(nsIAtom *aAtom)
 {
-  return nsAtomListUtils::IsMember(aAtom, CSSAnonBoxes_info,
-                                   ArrayLength(CSSAnonBoxes_info));
+  return nsAtomListUtils::IsMember(aAtom, sCSSAnonBoxAtomSetup,
+                                   ArrayLength(sCSSAnonBoxAtomSetup));
 }
 
 #ifdef MOZ_XUL
 /* static */ bool
 nsCSSAnonBoxes::IsTreePseudoElement(nsIAtom* aPseudo)
 {
   MOZ_ASSERT(nsCSSAnonBoxes::IsAnonBox(aPseudo));
   return StringBeginsWith(nsDependentAtomString(aPseudo),
                           NS_LITERAL_STRING(":-moz-tree-"));
 }
 #endif
 
 /* static*/ nsCSSAnonBoxes::NonInheriting
 nsCSSAnonBoxes::NonInheritingTypeForPseudoTag(nsIAtom* aPseudo)
 {
   MOZ_ASSERT(IsNonInheritingAnonBox(aPseudo));
-  for (NonInheritingBase i = 0;
-       i < ArrayLength(CSSAnonBoxes_info);
-       ++i) {
-    if (*CSSAnonBoxes_info[i].mAtom == aPseudo) {
+  for (NonInheritingBase i = 0; i < ArrayLength(sCSSAnonBoxAtomSetup); ++i) {
+    if (*sCSSAnonBoxAtomSetup[i].mAtom == aPseudo) {
       return static_cast<NonInheriting>(i);
     }
   }
 
   MOZ_CRASH("Bogus pseudo passed to NonInheritingTypeForPseudoTag");
 }
 
 /* static */ nsIAtom*
 nsCSSAnonBoxes::GetNonInheritingPseudoAtom(NonInheriting aBoxType)
 {
   MOZ_ASSERT(aBoxType < NonInheriting::_Count);
-  return *CSSAnonBoxes_info[static_cast<NonInheritingBase>(aBoxType)].mAtom;
+  return *sCSSAnonBoxAtomSetup[static_cast<NonInheritingBase>(aBoxType)].mAtom;
 }
diff --git a/layout/style/nsCSSPseudoClasses.cpp b/layout/style/nsCSSPseudoClasses.cpp
--- a/layout/style/nsCSSPseudoClasses.cpp
+++ b/layout/style/nsCSSPseudoClasses.cpp
@@ -33,27 +33,27 @@ using namespace mozilla;
 #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
   static_assert(!((flags_) & CSS_PSEUDO_CLASS_ENABLED_IN_CHROME) || \
                 ((flags_) & CSS_PSEUDO_CLASS_ENABLED_IN_UA_SHEETS), \
                 "Pseudo-class '" #name_ "' is enabled in chrome, so it " \
                 "should also be enabled in UA sheets");
 #include "nsCSSPseudoClassList.h"
 #undef CSS_PSEUDO_CLASS
 
-// Array of nsStaticAtom for each of the pseudo-classes.
-static const nsStaticAtom CSSPseudoClasses_info[] = {
+// Array of nsStaticAtomSetup for each of the pseudo-classes.
+static const nsStaticAtomSetup sCSSPseudoClassAtomSetup[] = {
 #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
-  NS_STATIC_ATOM(name_##_pseudo_class_buffer, &sPseudoClass_##name_),
+  NS_STATIC_ATOM_SETUP(name_##_pseudo_class_buffer, &sPseudoClass_##name_),
 #include "nsCSSPseudoClassList.h"
 #undef CSS_PSEUDO_CLASS
 };
 
 // Flags data for each of the pseudo-classes, which must be separate
 // from the previous array since there's no place for it in
-// nsStaticAtom.
+// nsStaticAtomSetup.
 /* static */ const uint32_t
 nsCSSPseudoClasses::kPseudoClassFlags[] = {
 #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_) \
   flags_,
 #include "nsCSSPseudoClassList.h"
 #undef CSS_PSEUDO_CLASS
 };
 
@@ -70,17 +70,17 @@ nsCSSPseudoClasses::sPseudoClassEnabled[
   IS_ENABLED_BY_DEFAULT(flags_),
 #include "nsCSSPseudoClassList.h"
 #undef CSS_PSEUDO_CLASS
 #undef IS_ENABLED_BY_DEFAULT
 };
 
 void nsCSSPseudoClasses::AddRefAtoms()
 {
-  NS_RegisterStaticAtoms(CSSPseudoClasses_info);
+  NS_RegisterStaticAtoms(sCSSPseudoClassAtomSetup);
 
 #define CSS_PSEUDO_CLASS(name_, value_, flags_, pref_)                        \
   if (pref_[0]) {                                                             \
     auto idx = static_cast<CSSPseudoElementTypeBase>(Type::name_);            \
     Preferences::AddBoolVarCache(&sPseudoClassEnabled[idx], pref_);           \
   }
 #include "nsCSSPseudoClassList.h"
 #undef CSS_PSEUDO_CLASS
@@ -104,24 +104,24 @@ nsCSSPseudoClasses::HasNthPairArg(Type a
          aType == Type::nthLastOfType;
 }
 
 void
 nsCSSPseudoClasses::PseudoTypeToString(Type aType, nsAString& aString)
 {
   MOZ_ASSERT(aType < Type::Count, "Unexpected type");
   auto idx = static_cast<CSSPseudoClassTypeBase>(aType);
-  (*CSSPseudoClasses_info[idx].mAtom)->ToString(aString);
+  (*sCSSPseudoClassAtomSetup[idx].mAtom)->ToString(aString);
 }
 
 /* static */ CSSPseudoClassType
 nsCSSPseudoClasses::GetPseudoType(nsIAtom* aAtom, EnabledState aEnabledState)
 {
-  for (uint32_t i = 0; i < ArrayLength(CSSPseudoClasses_info); ++i) {
-    if (*CSSPseudoClasses_info[i].mAtom == aAtom) {
+  for (uint32_t i = 0; i < ArrayLength(sCSSPseudoClassAtomSetup); ++i) {
+    if (*sCSSPseudoClassAtomSetup[i].mAtom == aAtom) {
       Type type = Type(i);
       return IsEnabled(type, aEnabledState) ? type : Type::NotPseudo;
     }
   }
   return Type::NotPseudo;
 }
 
 /* static */ bool
diff --git a/layout/style/nsCSSPseudoElements.cpp b/layout/style/nsCSSPseudoElements.cpp
--- a/layout/style/nsCSSPseudoElements.cpp
+++ b/layout/style/nsCSSPseudoElements.cpp
@@ -23,44 +23,44 @@ using namespace mozilla;
 #include "nsCSSPseudoElementList.h"
 #undef CSS_PSEUDO_ELEMENT
 
 #define CSS_PSEUDO_ELEMENT(name_, value_, flags_) \
   NS_STATIC_ATOM_BUFFER(name_##_pseudo_element_buffer, value_)
 #include "nsCSSPseudoElementList.h"
 #undef CSS_PSEUDO_ELEMENT
 
-// Array of nsStaticAtom for each of the pseudo-elements.
-static const nsStaticAtom CSSPseudoElements_info[] = {
+// Array of nsStaticAtomSetup for each of the pseudo-elements.
+static const nsStaticAtomSetup sCSSPseudoElementAtomSetup[] = {
 #define CSS_PSEUDO_ELEMENT(name_, value_, flags_) \
-  NS_STATIC_ATOM(name_##_pseudo_element_buffer, (nsIAtom**)&nsCSSPseudoElements::name_),
+  NS_STATIC_ATOM_SETUP(name_##_pseudo_element_buffer, (nsIAtom**)&nsCSSPseudoElements::name_),
 #include "nsCSSPseudoElementList.h"
 #undef CSS_PSEUDO_ELEMENT
 };
 
 // Flags data for each of the pseudo-elements, which must be separate
 // from the previous array since there's no place for it in
-// nsStaticAtom.
+// nsStaticAtomSetup.
 /* static */ const uint32_t
 nsCSSPseudoElements::kPseudoElementFlags[] = {
 #define CSS_PSEUDO_ELEMENT(name_, value_, flags_) \
   flags_,
 #include "nsCSSPseudoElementList.h"
 #undef CSS_PSEUDO_ELEMENT
 };
 
 void nsCSSPseudoElements::AddRefAtoms()
 {
-  NS_RegisterStaticAtoms(CSSPseudoElements_info);
+  NS_RegisterStaticAtoms(sCSSPseudoElementAtomSetup);
 }
 
 bool nsCSSPseudoElements::IsPseudoElement(nsIAtom *aAtom)
 {
-  return nsAtomListUtils::IsMember(aAtom, CSSPseudoElements_info,
-                                   ArrayLength(CSSPseudoElements_info));
+  return nsAtomListUtils::IsMember(aAtom, sCSSPseudoElementAtomSetup,
+                                   ArrayLength(sCSSPseudoElementAtomSetup));
 }
 
 /* static */ bool
 nsCSSPseudoElements::IsCSS2PseudoElement(nsIAtom *aAtom)
 {
   // We don't implement this using PseudoElementHasFlags because callers
   // want to pass things that could be anon boxes.
   NS_ASSERTION(nsCSSPseudoElements::IsPseudoElement(aAtom) ||
@@ -77,19 +77,19 @@ nsCSSPseudoElements::IsCSS2PseudoElement
                "result doesn't match flags");
   return result;
 }
 
 /* static */ CSSPseudoElementType
 nsCSSPseudoElements::GetPseudoType(nsIAtom *aAtom, EnabledState aEnabledState)
 {
   for (CSSPseudoElementTypeBase i = 0;
-       i < ArrayLength(CSSPseudoElements_info);
+       i < ArrayLength(sCSSPseudoElementAtomSetup);
        ++i) {
-    if (*CSSPseudoElements_info[i].mAtom == aAtom) {
+    if (*sCSSPseudoElementAtomSetup[i].mAtom == aAtom) {
       auto type = static_cast<Type>(i);
       // ::moz-placeholder is an alias for ::placeholder
       if (type == CSSPseudoElementType::mozPlaceholder) {
         type = CSSPseudoElementType::placeholder;
       }
       return IsEnabled(type, aEnabledState) ? type : Type::NotPseudo;
     }
   }
@@ -110,17 +110,17 @@ nsCSSPseudoElements::GetPseudoType(nsIAt
 
   return Type::NotPseudo;
 }
 
 /* static */ nsIAtom*
 nsCSSPseudoElements::GetPseudoAtom(Type aType)
 {
   NS_ASSERTION(aType < Type::Count, "Unexpected type");
-  return *CSSPseudoElements_info[
+  return *sCSSPseudoElementAtomSetup[
     static_cast<CSSPseudoElementTypeBase>(aType)].mAtom;
 }
 
 /* static */ already_AddRefed<nsIAtom>
 nsCSSPseudoElements::GetPseudoAtom(const nsAString& aPseudoElement)
 {
   if (DOMStringIsNull(aPseudoElement) || aPseudoElement.IsEmpty() ||
       aPseudoElement.First() != char16_t(':')) {
diff --git a/parser/htmlparser/nsHTMLTags.cpp b/parser/htmlparser/nsHTMLTags.cpp
--- a/parser/htmlparser/nsHTMLTags.cpp
+++ b/parser/htmlparser/nsHTMLTags.cpp
@@ -62,43 +62,43 @@ nsHTMLTags::RegisterAtoms(void)
 {
 #define HTML_TAG(_tag, _classname, _interfacename) NS_STATIC_ATOM_BUFFER(Atombuffer_##_tag, #_tag)
 #define HTML_OTHER(_tag)
 #include "nsHTMLTagList.h"
 #undef HTML_TAG
 #undef HTML_OTHER
 
 // static array of tag StaticAtom structs
-#define HTML_TAG(_tag, _classname, _interfacename) NS_STATIC_ATOM(Atombuffer_##_tag, &nsHTMLTags::sTagAtomTable[eHTMLTag_##_tag - 1]),
+#define HTML_TAG(_tag, _classname, _interfacename) NS_STATIC_ATOM_SETUP(Atombuffer_##_tag, &nsHTMLTags::sTagAtomTable[eHTMLTag_##_tag - 1]),
 #define HTML_OTHER(_tag)
-  static const nsStaticAtom sTagAtoms_info[] = {
+  static const nsStaticAtomSetup sTagAtomSetup[] = {
 #include "nsHTMLTagList.h"
   };
 #undef HTML_TAG
 #undef HTML_OTHER
 
   // Fill in our static atom pointers
-  NS_RegisterStaticAtoms(sTagAtoms_info);
+  NS_RegisterStaticAtoms(sTagAtomSetup);
 
 
 #if defined(DEBUG)
   {
     // let's verify that all names in the the table are lowercase...
     for (int32_t i = 0; i < NS_HTML_TAG_MAX; ++i) {
-      nsAutoString temp1((char16_t*)sTagAtoms_info[i].mString);
-      nsAutoString temp2((char16_t*)sTagAtoms_info[i].mString);
+      nsAutoString temp1((char16_t*)sTagAtomSetup[i].mString);
+      nsAutoString temp2((char16_t*)sTagAtomSetup[i].mString);
       ToLowerCase(temp1);
       NS_ASSERTION(temp1.Equals(temp2), "upper case char in table");
     }
 
     // let's verify that all names in the unicode strings above are
     // correct.
     for (int32_t i = 0; i < NS_HTML_TAG_MAX; ++i) {
       nsAutoString temp1(sTagUnicodeTable[i]);
-      nsAutoString temp2((char16_t*)sTagAtoms_info[i].mString);
+      nsAutoString temp2((char16_t*)sTagAtomSetup[i].mString);
       NS_ASSERTION(temp1.Equals(temp2), "Bad unicode tag name!");
     }
 
     // let's verify that NS_HTMLTAG_NAME_MAX_LENGTH is correct
     uint32_t maxTagNameLength = 0;
     for (int32_t i = 0; i < NS_HTML_TAG_MAX; ++i) {
       uint32_t len = NS_strlen(sTagUnicodeTable[i]);
       maxTagNameLength = std::max(len, maxTagNameLength);
diff --git a/rdf/base/nsRDFContentSink.cpp b/rdf/base/nsRDFContentSink.cpp
--- a/rdf/base/nsRDFContentSink.cpp
+++ b/rdf/base/nsRDFContentSink.cpp
@@ -238,27 +238,27 @@ mozilla::LazyLogModule RDFContentSinkImp
 #define RDF_ATOM(name_, value_) nsIAtom* RDFContentSinkImpl::name_;
 #include "nsRDFContentSinkAtomList.h"
 #undef RDF_ATOM
 
 #define RDF_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
 #include "nsRDFContentSinkAtomList.h"
 #undef RDF_ATOM
 
-static const nsStaticAtom rdf_atoms[] = {
-#define RDF_ATOM(name_, value_) NS_STATIC_ATOM(name_##_buffer, &RDFContentSinkImpl::name_),
+static const nsStaticAtomSetup sRDFContentSinkAtomSetup[] = {
+#define RDF_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(name_##_buffer, &RDFContentSinkImpl::name_),
 #include "nsRDFContentSinkAtomList.h"
 #undef RDF_ATOM
 };
 
 // static
 void
 nsRDFAtoms::RegisterAtoms()
 {
-    NS_RegisterStaticAtoms(rdf_atoms);
+    NS_RegisterStaticAtoms(sRDFContentSinkAtomSetup);
 }
 
 RDFContentSinkImpl::RDFContentSinkImpl()
     : mText(nullptr),
       mTextLength(0),
       mTextSize(0),
       mState(eRDFContentSinkState_InProlog),
       mParseMode(eRDFContentSinkParseMode_Literal),
diff --git a/xpcom/ds/nsAtomTable.cpp b/xpcom/ds/nsAtomTable.cpp
--- a/xpcom/ds/nsAtomTable.cpp
+++ b/xpcom/ds/nsAtomTable.cpp
@@ -52,18 +52,18 @@ enum class GCKind {
   Shutdown,
 };
 
 // This class encapsulates the functions that need access to nsAtom's private
 // members.
 class nsAtomFriend
 {
 public:
-  static void RegisterStaticAtoms(const nsStaticAtom* aAtoms,
-                                  uint32_t aAtomCount);
+  static void RegisterStaticAtoms(const nsStaticAtomSetup* aSetup,
+                                  uint32_t aCount);
 
   static void AtomTableClearEntry(PLDHashTable* aTable,
                                   PLDHashEntryHdr* aEntry);
 
   static void GCAtomTableLocked(const MutexAutoLock& aProofOfLock,
                                 GCKind aKind);
 
   static already_AddRefed<nsIAtom> Atomize(const nsACString& aUTF8String);
@@ -513,20 +513,20 @@ NS_InitAtomTable()
   // Bug 1340710 has caused us to generate an empty atom at arbitrary times
   // after startup.  If we end up creating one before nsGkAtoms::_empty is
   // registered, we get an assertion about transmuting a dynamic atom into a
   // static atom.  In order to avoid that, we register an empty string static
   // atom as soon as we initialize the atom table to guarantee that the empty
   // string atom will always be static.
   NS_STATIC_ATOM_BUFFER(empty, "");
   static nsIAtom* empty_atom = nullptr;
-  static const nsStaticAtom default_atoms[] = {
-    NS_STATIC_ATOM(empty, &empty_atom)
+  static const nsStaticAtomSetup sDefaultAtomSetup[] = {
+    NS_STATIC_ATOM_SETUP(empty, &empty_atom)
   };
-  NS_RegisterStaticAtoms(default_atoms);
+  NS_RegisterStaticAtoms(sDefaultAtomSetup);
 }
 
 void
 NS_ShutdownAtomTable()
 {
   delete gStaticAtomTable;
   gStaticAtomTable = nullptr;
 
@@ -577,31 +577,31 @@ GetAtomHashEntry(const char16_t* aString
 {
   gAtomTableLock->AssertCurrentThreadOwns();
   AtomTableKey key(aString, aLength, aHashOut);
   // This is an infallible add.
   return static_cast<AtomTableEntry*>(gAtomTable->Add(&key));
 }
 
 void
-nsAtomFriend::RegisterStaticAtoms(const nsStaticAtom* aAtoms,
-                                  uint32_t aAtomCount)
+nsAtomFriend::RegisterStaticAtoms(const nsStaticAtomSetup* aSetup,
+                                  uint32_t aCount)
 {
   MutexAutoLock lock(*gAtomTableLock);
 
   MOZ_RELEASE_ASSERT(!gStaticAtomTableSealed,
                      "Atom table has already been sealed!");
 
   if (!gStaticAtomTable) {
     gStaticAtomTable = new StaticAtomTable();
   }
 
-  for (uint32_t i = 0; i < aAtomCount; ++i) {
-    const char16_t* string = aAtoms[i].mString;
-    nsIAtom** atomp = aAtoms[i].mAtom;
+  for (uint32_t i = 0; i < aCount; ++i) {
+    const char16_t* string = aSetup[i].mString;
+    nsIAtom** atomp = aSetup[i].mAtom;
 
     MOZ_ASSERT(nsCRT::IsAscii(string));
 
     uint32_t stringLen = NS_strlen(string);
 
     uint32_t hash;
     AtomTableEntry* he = GetAtomHashEntry(string, stringLen, &hash);
 
@@ -628,19 +628,19 @@ nsAtomFriend::RegisterStaticAtoms(const 
         gStaticAtomTable->PutEntry(nsDependentAtomString(atom));
       MOZ_ASSERT(atom->IsStaticAtom());
       entry->mAtom = atom;
     }
   }
 }
 
 void
-RegisterStaticAtoms(const nsStaticAtom* aAtoms, uint32_t aAtomCount)
+RegisterStaticAtoms(const nsStaticAtomSetup* aSetup, uint32_t aCount)
 {
-  nsAtomFriend::RegisterStaticAtoms(aAtoms, aAtomCount);
+  nsAtomFriend::RegisterStaticAtoms(aSetup, aCount);
 }
 
 already_AddRefed<nsIAtom>
 NS_Atomize(const char* aUTF8String)
 {
   return nsAtomFriend::Atomize(nsDependentCString(aUTF8String));
 }
 
diff --git a/xpcom/ds/nsStaticAtom.h b/xpcom/ds/nsStaticAtom.h
--- a/xpcom/ds/nsStaticAtom.h
+++ b/xpcom/ds/nsStaticAtom.h
@@ -5,39 +5,40 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsStaticAtom_h__
 #define nsStaticAtom_h__
 
 #include "nsIAtom.h"
 #include "nsStringBuffer.h"
 
-#define NS_STATIC_ATOM(buffer_name, atom_ptr) \
+#define NS_STATIC_ATOM_SETUP(buffer_name, atom_ptr) \
   { buffer_name, atom_ptr }
 
 // Note that |str_data| is an 8-bit string, and so |sizeof(str_data)| is equal
 // to the number of chars (including the terminating '\0'). The |u""| prefix
 // converts |str_data| to a 16-bit string, which is assigned.
 #define NS_STATIC_ATOM_BUFFER(buffer_name, str_data) \
   static const char16_t buffer_name[sizeof(str_data)] = u"" str_data; \
   static_assert(sizeof(str_data[0]) == 1, "non-8-bit static atom literal");
 
 /**
  * Holds data used to initialize large number of atoms during startup. Use
  * the above macros to initialize these structs. They should never be accessed
  * directly other than from AtomTable.cpp.
  */
-struct nsStaticAtom
+struct nsStaticAtomSetup
 {
   const char16_t* const mString;
   nsIAtom** const mAtom;
 };
 
 // Register an array of static atoms with the atom table
 template<uint32_t N>
 void
-NS_RegisterStaticAtoms(const nsStaticAtom (&aAtoms)[N])
+NS_RegisterStaticAtoms(const nsStaticAtomSetup (&aSetup)[N])
 {
-  extern void RegisterStaticAtoms(const nsStaticAtom*, uint32_t aAtomCount);
-  RegisterStaticAtoms(aAtoms, N);
+  extern void RegisterStaticAtoms(const nsStaticAtomSetup* aSetup,
+                                  uint32_t aCount);
+  RegisterStaticAtoms(aSetup, N);
 }
 
 #endif
diff --git a/xpcom/io/nsDirectoryService.cpp b/xpcom/io/nsDirectoryService.cpp
--- a/xpcom/io/nsDirectoryService.cpp
+++ b/xpcom/io/nsDirectoryService.cpp
@@ -108,18 +108,18 @@ nsDirectoryService::Create(nsISupports* 
 #define DIR_ATOM(name_, value_) nsIAtom* nsDirectoryService::name_ = nullptr;
 #include "nsDirectoryServiceAtomList.h"
 #undef DIR_ATOM
 
 #define DIR_ATOM(name_, value_) NS_STATIC_ATOM_BUFFER(name_##_buffer, value_)
 #include "nsDirectoryServiceAtomList.h"
 #undef DIR_ATOM
 
-static const nsStaticAtom directory_atoms[] = {
-#define DIR_ATOM(name_, value_) NS_STATIC_ATOM(name_##_buffer, &nsDirectoryService::name_),
+static const nsStaticAtomSetup sDirectoryServiceAtomSetup[] = {
+#define DIR_ATOM(name_, value_) NS_STATIC_ATOM_SETUP(name_##_buffer, &nsDirectoryService::name_),
 #include "nsDirectoryServiceAtomList.h"
 #undef DIR_ATOM
 };
 
 NS_IMETHODIMP
 nsDirectoryService::Init()
 {
   NS_NOTREACHED("nsDirectoryService::Init() for internal use only!");
@@ -129,17 +129,17 @@ nsDirectoryService::Init()
 void
 nsDirectoryService::RealInit()
 {
   NS_ASSERTION(!gService,
                "nsDirectoryService::RealInit Mustn't initialize twice!");
 
   gService = new nsDirectoryService();
 
-  NS_RegisterStaticAtoms(directory_atoms);
+  NS_RegisterStaticAtoms(sDirectoryServiceAtomSetup);
 
   // Let the list hold the only reference to the provider.
   nsAppFileLocationProvider* defaultProvider = new nsAppFileLocationProvider;
   gService->mProviders.AppendElement(defaultProvider);
 }
 
 nsDirectoryService::~nsDirectoryService()
 {
