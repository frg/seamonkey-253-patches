# HG changeset patch
# User Thom Chiovoloni <tchiovoloni@mozilla.com>
# Date 1507152380 14400
#      Wed Oct 04 17:26:20 2017 -0400
# Node ID bf80be6c221064ab3ea802e09768be223019443a
# Parent  522b12fe2c137579ff34b990140b69c7248f5a54
Bug 1405833 - Ensure SyncEngine uses CommonUtils.namedTimer properly. r=kitcambridge

MozReview-Commit-ID: 6YnhcSjKW9U

diff --git a/services/sync/modules/engines.js b/services/sync/modules/engines.js
--- a/services/sync/modules/engines.js
+++ b/services/sync/modules/engines.js
@@ -882,20 +882,22 @@ SyncEngine.prototype = {
   set toFetch(val) {
     // Coerce the array to a string for more efficient comparison.
     if (val + "" == this._toFetch) {
       return;
     }
     this._toFetch = val;
     Utils.namedTimer(function() {
       try {
-        Async.promiseSpinningly(Utils.jsonSave("toFetch/" + this.name, this, val));
+        Async.promiseSpinningly(Utils.jsonSave("toFetch/" + this.name, this, this._toFetch));
       } catch (error) {
         this._log.error("Failed to read JSON records to fetch", error);
       }
+      // Notify our tests that we finished writing the file.
+      Observers.notify("sync-testing:file-saved:toFetch", null, this.name);
     }, 0, this, "_toFetchDelay");
   },
 
   async loadToFetch() {
     // Initialize to empty if there's no file.
     this._toFetch = [];
     let toFetch = await Utils.jsonLoad("toFetch/" + this.name, this);
     if (toFetch) {
@@ -908,21 +910,25 @@ SyncEngine.prototype = {
   },
   set previousFailed(val) {
     // Coerce the array to a string for more efficient comparison.
     if (val + "" == this._previousFailed) {
       return;
     }
     this._previousFailed = val;
     Utils.namedTimer(function() {
-      Utils.jsonSave("failed/" + this.name, this, val).then(() => {
+      Utils.jsonSave("failed/" + this.name, this, this._previousFailed).then(() => {
         this._log.debug("Successfully wrote previousFailed.");
       })
       .catch((error) => {
         this._log.error("Failed to set previousFailed", error);
+      })
+      .then(() => {
+        // Notify our tests that we finished writing the file.
+        Observers.notify("sync-testing:file-saved:previousFailed", null, this.name);
       });
     }, 0, this, "_previousFailedDelay");
   },
 
   async loadPreviousFailed() {
     // Initialize to empty if there's no file
     this._previousFailed = [];
     let previousFailed = await Utils.jsonLoad("failed/" + this.name, this);
diff --git a/services/sync/tests/unit/test_syncengine.js b/services/sync/tests/unit/test_syncengine.js
--- a/services/sync/tests/unit/test_syncengine.js
+++ b/services/sync/tests/unit/test_syncengine.js
@@ -88,23 +88,39 @@ add_task(async function test_toFetch() {
   const filename = "weave/toFetch/steam.json";
   let engine = await makeSteamEngine();
   try {
     // Ensure pristine environment
     Assert.equal(engine.toFetch.length, 0);
 
     // Write file to disk
     let toFetch = [Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID()];
+    let wrotePromise = promiseOneObserver("sync-testing:file-saved:toFetch");
     engine.toFetch = toFetch;
     Assert.equal(engine.toFetch, toFetch);
     // toFetch is written asynchronously
-    await Async.promiseYield();
+    await wrotePromise;
     let fakefile = syncTesting.fakeFilesystem.fakeContents[filename];
     Assert.equal(fakefile, JSON.stringify(toFetch));
 
+    // Make sure it work for consecutive writes before the callback is executed.
+    toFetch = [Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID()];
+    let toFetch2 = [Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID()];
+    wrotePromise = promiseOneObserver("sync-testing:file-saved:toFetch");
+
+    engine.toFetch = toFetch;
+    Assert.equal(engine.toFetch, toFetch);
+
+    engine.toFetch = toFetch2;
+    Assert.equal(engine.toFetch, toFetch2);
+    // Note that do to the way CommonUtils.namedTimer works, we won't get a 2nd callback.
+    await wrotePromise;
+    fakefile = syncTesting.fakeFilesystem.fakeContents[filename];
+    Assert.equal(fakefile, JSON.stringify(toFetch2));
+
     // Read file from disk
     toFetch = [Utils.makeGUID(), Utils.makeGUID()];
     syncTesting.fakeFilesystem.fakeContents[filename] = JSON.stringify(toFetch);
     await engine.loadToFetch();
     Assert.equal(engine.toFetch.length, 2);
     Assert.equal(engine.toFetch[0], toFetch[0]);
     Assert.equal(engine.toFetch[1], toFetch[1]);
   } finally {
@@ -118,23 +134,39 @@ add_task(async function test_previousFai
   const filename = "weave/failed/steam.json";
   let engine = await makeSteamEngine();
   try {
     // Ensure pristine environment
     Assert.equal(engine.previousFailed.length, 0);
 
     // Write file to disk
     let previousFailed = [Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID()];
+    let wrotePromise = promiseOneObserver("sync-testing:file-saved:previousFailed");
     engine.previousFailed = previousFailed;
     Assert.equal(engine.previousFailed, previousFailed);
     // previousFailed is written asynchronously
-    await Async.promiseYield();
+    await wrotePromise;
     let fakefile = syncTesting.fakeFilesystem.fakeContents[filename];
     Assert.equal(fakefile, JSON.stringify(previousFailed));
 
+    // Make sure it work for consecutive writes before the callback is executed.
+    previousFailed = [Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID()];
+    let previousFailed2 = [Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID(), Utils.makeGUID()];
+    wrotePromise = promiseOneObserver("sync-testing:file-saved:previousFailed");
+
+    engine.previousFailed = previousFailed;
+    Assert.equal(engine.previousFailed, previousFailed);
+
+    engine.previousFailed = previousFailed2;
+    Assert.equal(engine.previousFailed, previousFailed2);
+    // Note that do to the way CommonUtils.namedTimer works, we're only notified once.
+    await wrotePromise;
+    fakefile = syncTesting.fakeFilesystem.fakeContents[filename];
+    Assert.equal(fakefile, JSON.stringify(previousFailed2));
+
     // Read file from disk
     previousFailed = [Utils.makeGUID(), Utils.makeGUID()];
     syncTesting.fakeFilesystem.fakeContents[filename] = JSON.stringify(previousFailed);
     await engine.loadPreviousFailed();
     Assert.equal(engine.previousFailed.length, 2);
     Assert.equal(engine.previousFailed[0], previousFailed[0]);
     Assert.equal(engine.previousFailed[1], previousFailed[1]);
   } finally {
