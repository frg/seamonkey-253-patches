# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1512725729 -28800
# Node ID 772fef647fd7750f8c87dfd4f55bbd85046a555a
# Parent  682dbde79981ede0e08580d6cd946c7102f12af2
Bug 1420608. P2 - fix the test timeout. r=alwu

See comment 50 for the cause. Since file_silentAudioTrack.html calls play() to
start playback immediately, it is possible that 'mozentervideosuspend' has been
fired before check_video_decoding_state() has a chance to register event handlers.
We call load() and play() to start playback from the beginning so we won't miss
any events.

MozReview-Commit-ID: 9sKygfIxEtS

diff --git a/toolkit/content/tests/browser/browser_resume_bkg_video_on_tab_hover.js b/toolkit/content/tests/browser/browser_resume_bkg_video_on_tab_hover.js
--- a/toolkit/content/tests/browser/browser_resume_bkg_video_on_tab_hover.js
+++ b/toolkit/content/tests/browser/browser_resume_bkg_video_on_tab_hover.js
@@ -1,16 +1,27 @@
 const PAGE = "https://example.com/browser/toolkit/content/tests/browser/file_silentAudioTrack.html";
 
-async function check_video_decoding_state(isSuspended) {
+async function check_video_decoding_state(args) {
   let video = content.document.getElementById("autoplay");
   if (!video) {
     ok(false, "Can't get the video element!");
   }
 
+  let isSuspended = args.suspend;
+  let reload = args.reload;
+
+  if (reload) {
+    // It is too late to register event handlers when playback is half
+    // way done. Let's start playback from the beginning so we won't
+    // miss any events.
+    video.load();
+    video.play();
+  }
+
   let state = isSuspended ? "suspended" : "resumed";
   let event = isSuspended ? "mozentervideosuspend" : "mozexitvideosuspend";
   return new Promise(resolve => {
     video.addEventListener(event, function() {
       ok(true, `Video decoding is ${state}.`);
       resolve();
     }, {once: true});
   });
@@ -41,23 +52,26 @@ function check_should_not_send_unselecte
   return new Promise(resolve => {
     browser.messageManager.addMessageListener("UnselectedTabHoverMsg:Disabled", function() {
       ok(true, "Should not send unselected tab hover msg, no one is listening for it.");
       resolve();
     });
   });
 }
 
-function get_video_decoding_suspend_promise(browser) {
-  return ContentTask.spawn(browser, true /* suspend */,
+function get_video_decoding_suspend_promise(browser, reload) {
+  let suspend = true;
+  return ContentTask.spawn(browser, { suspend, reload },
                            check_video_decoding_state);
 }
 
 function get_video_decoding_resume_promise(browser) {
-  return ContentTask.spawn(browser, false /* resume */,
+  let suspend = false;
+  let reload = false;
+  return ContentTask.spawn(browser, { suspend, reload },
                            check_video_decoding_state);
 }
 
 /**
  * Because of bug1029451, we can't receive "mouseover" event correctly when
  * we disable non-test mouse event. Therefore, we can't synthesize mouse event
  * to simulate cursor hovering, so we temporarily use a hacky way to resume and
  * suspend video decoding.
@@ -92,17 +106,17 @@ add_task(async function resume_and_suspe
   let browser = tab.linkedBrowser;
 
   info("- before loading media, we shoudn't send the tab hover msg for tab -");
   await check_should_not_send_unselected_tab_hover_msg(browser);
   browser.loadURI(PAGE);
   await BrowserTestUtils.browserLoaded(browser);
 
   info("- should suspend background video decoding -");
-  await get_video_decoding_suspend_promise(browser);
+  await get_video_decoding_suspend_promise(browser, true);
   await check_should_send_unselected_tab_hover_msg(browser);
 
   info("- when cursor is hovering over the tab, resuming the video decoding -");
   let promise = get_video_decoding_resume_promise(browser);
   await cursor_hover_over_tab_and_resume_video_decoding(browser);
   await promise;
   await check_should_send_unselected_tab_hover_msg(browser);
 
