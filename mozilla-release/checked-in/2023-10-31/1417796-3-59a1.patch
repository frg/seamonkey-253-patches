# HG changeset patch
# User Ting-Yu Lin <tlin@mozilla.com>
# Date 1510811380 -28800
#      Thu Nov 16 13:49:40 2017 +0800
# Node ID e2b7c18a8271ecddcfba419055211543b4926074
# Parent  1154dbd720f2c64bd1e4adaa574c68f2fe28d914
Bug 1417796 - Pass UpdateCaretsHintSet by const references instead of values. r=mtseng

Fix warning: the parameter 'aHints' is copied for each invocation but only
used as a const reference; consider making it a const reference

MozReview-Commit-ID: 6CyT6gxVgES

diff --git a/layout/base/AccessibleCaretManager.cpp b/layout/base/AccessibleCaretManager.cpp
--- a/layout/base/AccessibleCaretManager.cpp
+++ b/layout/base/AccessibleCaretManager.cpp
@@ -210,17 +210,17 @@ AccessibleCaretManager::HideCarets()
     AC_LOG("%s", __FUNCTION__);
     mFirstCaret->SetAppearance(Appearance::None);
     mSecondCaret->SetAppearance(Appearance::None);
     DispatchCaretStateChangedEvent(CaretChangedReason::Visibilitychange);
   }
 }
 
 void
-AccessibleCaretManager::UpdateCarets(UpdateCaretsHintSet aHint)
+AccessibleCaretManager::UpdateCarets(const UpdateCaretsHintSet& aHint)
 {
   FlushLayout();
   if (IsTerminated()) {
     return;
   }
 
   mLastUpdateCaretMode = GetCaretMode();
 
@@ -271,17 +271,17 @@ AccessibleCaretManager::IsCaretDisplayab
 bool
 AccessibleCaretManager::HasNonEmptyTextContent(nsINode* aNode) const
 {
   return nsContentUtils::HasNonEmptyTextContent(
            aNode, nsContentUtils::eRecurseIntoChildren);
 }
 
 void
-AccessibleCaretManager::UpdateCaretsForCursorMode(UpdateCaretsHintSet aHints)
+AccessibleCaretManager::UpdateCaretsForCursorMode(const UpdateCaretsHintSet& aHints)
 {
   AC_LOG("%s, selection: %p", __FUNCTION__, GetSelection());
 
   int32_t offset = 0;
   nsIFrame* frame = nullptr;
   if (!IsCaretDisplayableInCursorMode(&frame, &offset)) {
     HideCarets();
     return;
@@ -330,17 +330,17 @@ AccessibleCaretManager::UpdateCaretsForC
 
   if (!aHints.contains(UpdateCaretsHint::DispatchNoEvent) &&
       !mActiveCaret) {
     DispatchCaretStateChangedEvent(CaretChangedReason::Updateposition);
   }
 }
 
 void
-AccessibleCaretManager::UpdateCaretsForSelectionMode(UpdateCaretsHintSet aHints)
+AccessibleCaretManager::UpdateCaretsForSelectionMode(const UpdateCaretsHintSet& aHints)
 {
   AC_LOG("%s: selection: %p", __FUNCTION__, GetSelection());
 
   int32_t startOffset = 0;
   nsIFrame* startFrame =
     GetFrameForFirstRangeStartOrLastRangeEnd(eDirNext, &startOffset);
 
   int32_t endOffset = 0;
diff --git a/layout/base/AccessibleCaretManager.h b/layout/base/AccessibleCaretManager.h
--- a/layout/base/AccessibleCaretManager.h
+++ b/layout/base/AccessibleCaretManager.h
@@ -140,23 +140,23 @@ protected:
   using UpdateCaretsHintSet = mozilla::EnumSet<UpdateCaretsHint>;
 
   friend std::ostream& operator<<(std::ostream& aStream,
                                   const UpdateCaretsHint& aResult);
 
   // Update carets based on current selection status. This function will flush
   // layout, so caller must ensure the PresShell is still valid after calling
   // this method.
-  void UpdateCarets(UpdateCaretsHintSet aHints = UpdateCaretsHint::Default);
+  void UpdateCarets(const UpdateCaretsHintSet& aHints = UpdateCaretsHint::Default);
 
   // Force hiding all carets regardless of the current selection status.
   void HideCarets();
 
-  void UpdateCaretsForCursorMode(UpdateCaretsHintSet aHints);
-  void UpdateCaretsForSelectionMode(UpdateCaretsHintSet aHints);
+  void UpdateCaretsForCursorMode(const UpdateCaretsHintSet& aHints);
+  void UpdateCaretsForSelectionMode(const UpdateCaretsHintSet& aHints);
 
   // Provide haptic / touch feedback, primarily for select on longpress.
   void ProvideHapticFeedback();
 
   // Get the nearest enclosing focusable frame of aFrame.
   // @return focusable frame if there is any; nullptr otherwise.
   nsIFrame* GetFocusableFrame(nsIFrame* aFrame) const;
 
