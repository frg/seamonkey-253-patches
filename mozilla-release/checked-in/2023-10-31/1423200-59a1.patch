# HG changeset patch
# User Mike Conley <mconley@mozilla.com>
# Date 1516228568 28800
# Node ID 1765a455cc35fb3d3ccb7f2e3004bbacc7448105
# Parent  6b5e92aa71357eb1e1f6835ce81a49cc5a5a281b
Bug 1423200 - When setting up a new content viewer, if the previous PresShell was active, make the new one active too. r=mystor

It's possible to RenderLayers for a top-level content process DocShell without that DocShell being
active. When we do this, the PresShell for that DocShell becomes active, but the DocShell stays
inactive (to avoid accidentally playing paused video or clearing notifications in that DocShell).

If a DocShell is inactive but rendering its layers, it's possible for that DocShell to navigate.
When this occurs, a new PresShell can be created, which normally reads its active state off of
the DocShell. This means that the PresShell will become inactive even though the tab is supposed
to continue rendering its layers.

This patch checks for PresShell active state when setting up a new content viewer after a navigation,
and if the previous PresShell was active, makes the new PresShell active too.

MozReview-Commit-ID: KX9HvZJKqg2

diff --git a/docshell/base/nsDocShell.cpp b/docshell/base/nsDocShell.cpp
--- a/docshell/base/nsDocShell.cpp
+++ b/docshell/base/nsDocShell.cpp
@@ -9455,30 +9455,32 @@ nsDocShell::SetupNewViewer(nsIContentVie
                           NS_ERROR_FAILURE);
         NS_ENSURE_SUCCESS(oldCv->GetAuthorStyleDisabled(&styleDisabled),
                           NS_ERROR_FAILURE);
       }
     }
   }
 
   nscolor bgcolor = NS_RGBA(0, 0, 0, 0);
+  bool isActive = false;
   // Ensure that the content viewer is destroyed *after* the GC - bug 71515
   nsCOMPtr<nsIContentViewer> contentViewer = mContentViewer;
   if (contentViewer) {
     // Stop any activity that may be happening in the old document before
     // releasing it...
     contentViewer->Stop();
 
     // Try to extract the canvas background color from the old
     // presentation shell, so we can use it for the next document.
     nsCOMPtr<nsIPresShell> shell;
     contentViewer->GetPresShell(getter_AddRefs(shell));
 
     if (shell) {
       bgcolor = shell->GetCanvasBackground();
+      isActive = shell->IsActive();
     }
 
     contentViewer->Close(mSavingOldViewer ? mOSHE.get() : nullptr);
     aNewViewer->SetPreviousViewer(contentViewer);
   }
   if (mOSHE && (!mContentViewer || !mSavingOldViewer)) {
     // We don't plan to save a viewer in mOSHE; tell it to drop
     // any other state it's holding.
@@ -9527,16 +9529,19 @@ nsDocShell::SetupNewViewer(nsIContentVie
 
   // Stuff the bgcolor from the old pres shell into the new
   // pres shell. This improves page load continuity.
   nsCOMPtr<nsIPresShell> shell;
   mContentViewer->GetPresShell(getter_AddRefs(shell));
 
   if (shell) {
     shell->SetCanvasBackground(bgcolor);
+    if (isActive) {
+      shell->SetIsActive(isActive);
+    }
   }
 
   // XXX: It looks like the LayoutState gets restored again in Embed()
   //      right after the call to SetupNewViewer(...)
 
   // We don't show the mContentViewer yet, since we want to draw the old page
   // until we have enough of the new page to show.  Just return with the new
   // viewer still set to hidden.
