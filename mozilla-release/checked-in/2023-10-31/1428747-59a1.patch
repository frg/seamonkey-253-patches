# HG changeset patch
# User Olli Pettay <Olli.Pettay@helsinki.fi>
# Date 1515505302 -7200
# Node ID 9bcdad9d7f4372f12125a42b6f4a413566e0fe8e
# Parent  5f9d8851c52ae749ef422e7481c2c7483de3c2bd
Bug 1428747 - Make EditorBase::GetDOMEventTarget() to return EventTarget* and GetFocusedContent() return nsIContent* in order to optimize out some AddRef/Release calls, r=masayuki

diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -5345,44 +5345,44 @@ EditorBase::IsModifiableNode(nsIDOMNode*
 }
 
 bool
 EditorBase::IsModifiableNode(nsINode* aNode)
 {
   return true;
 }
 
-already_AddRefed<nsIContent>
+nsIContent*
 EditorBase::GetFocusedContent()
 {
-  nsCOMPtr<nsIDOMEventTarget> piTarget = GetDOMEventTarget();
+  nsIDOMEventTarget* piTarget = GetDOMEventTarget();
   if (!piTarget) {
     return nullptr;
   }
 
   nsFocusManager* fm = nsFocusManager::GetFocusManager();
   NS_ENSURE_TRUE(fm, nullptr);
 
   nsIContent* content = fm->GetFocusedContent();
   MOZ_ASSERT((content == piTarget) == SameCOMIdentity(content, piTarget));
 
-  return (content == piTarget) ?
-    piTarget.forget().downcast<nsIContent>() : nullptr;
+  return (content == piTarget) ? content : nullptr;
 }
 
 already_AddRefed<nsIContent>
 EditorBase::GetFocusedContentForIME()
 {
-  return GetFocusedContent();
+  nsCOMPtr<nsIContent> content = GetFocusedContent();
+  return content.forget();
 }
 
 bool
 EditorBase::IsActiveInDOMWindow()
 {
-  nsCOMPtr<nsIDOMEventTarget> piTarget = GetDOMEventTarget();
+  nsIDOMEventTarget* piTarget = GetDOMEventTarget();
   if (!piTarget) {
     return false;
   }
 
   nsFocusManager* fm = nsFocusManager::GetFocusManager();
   NS_ENSURE_TRUE(fm, false);
 
   nsCOMPtr<nsIDocument> document = GetDocument();
@@ -5404,17 +5404,17 @@ EditorBase::IsAcceptableInputEvent(Widge
   // If the event is trusted, the event should always cause input.
   if (NS_WARN_IF(!aGUIEvent)) {
     return false;
   }
 
   // If this is dispatched by using cordinates but this editor doesn't have
   // focus, we shouldn't handle it.
   if (aGUIEvent->IsUsingCoordinates()) {
-    nsCOMPtr<nsIContent> focusedContent = GetFocusedContent();
+    nsIContent* focusedContent = GetFocusedContent();
     if (!focusedContent) {
       return false;
     }
   }
 
   // If a composition event isn't dispatched via widget, we need to ignore them
   // since they cannot be managed by TextComposition. E.g., the event was
   // created by chrome JS.
diff --git a/editor/libeditor/EditorBase.h b/editor/libeditor/EditorBase.h
--- a/editor/libeditor/EditorBase.h
+++ b/editor/libeditor/EditorBase.h
@@ -1159,17 +1159,17 @@ public:
                                   Selection* aSelection,
                                   nsINode* previousSelectedNode,
                                   uint32_t previousSelectedOffset,
                                   nsINode* aStartContainer,
                                   uint32_t aStartOffset,
                                   nsINode* aEndContainer,
                                   uint32_t aEndOffset);
 
-  virtual already_AddRefed<dom::EventTarget> GetDOMEventTarget() = 0;
+  virtual dom::EventTarget* GetDOMEventTarget() = 0;
 
   /**
    * Fast non-refcounting editor root element accessor
    */
   Element* GetRoot() const { return mRootElement; }
 
   /**
    * Likewise, but gets the editor's root instead, which is different for HTML
@@ -1339,17 +1339,17 @@ public:
   /**
    * Get the input event target. This might return null.
    */
   virtual already_AddRefed<nsIContent> GetInputEventTargetContent() = 0;
 
   /**
    * Get the focused content, if we're focused.  Returns null otherwise.
    */
-  virtual already_AddRefed<nsIContent> GetFocusedContent();
+  virtual nsIContent* GetFocusedContent();
 
   /**
    * Get the focused content for the argument of some IMEStateManager's
    * methods.
    */
   virtual already_AddRefed<nsIContent> GetFocusedContentForIME();
 
   /**
diff --git a/editor/libeditor/EditorEventListener.cpp b/editor/libeditor/EditorEventListener.cpp
--- a/editor/libeditor/EditorEventListener.cpp
+++ b/editor/libeditor/EditorEventListener.cpp
@@ -139,17 +139,17 @@ EditorEventListener::Connect(EditorBase*
   return rv;
 }
 
 nsresult
 EditorEventListener::InstallToEditor()
 {
   NS_PRECONDITION(mEditorBase, "The caller must set mEditorBase");
 
-  nsCOMPtr<EventTarget> piTarget = mEditorBase->GetDOMEventTarget();
+  EventTarget* piTarget = mEditorBase->GetDOMEventTarget();
   NS_ENSURE_TRUE(piTarget, NS_ERROR_FAILURE);
 
   // register the event listeners with the listener manager
   EventListenerManager* elmP = piTarget->GetOrCreateListenerManager();
   NS_ENSURE_STATE(elmP);
 
 #ifdef HANDLE_NATIVE_TEXT_DIRECTION_SWITCH
   elmP->AddEventListenerByType(this,
@@ -1088,17 +1088,17 @@ EditorEventListener::Focus(InternalFocus
   // Spell check a textarea the first time that it is focused.
   SpellCheckIfNeeded();
   if (!editorBase) {
     // In e10s, this can cause us to flush notifications, which can destroy
     // the node we're about to focus.
     return NS_OK;
   }
 
-  nsCOMPtr<nsIDOMEventTarget> target = aFocusEvent->GetDOMEventTarget();
+  nsIDOMEventTarget* target = aFocusEvent->GetDOMEventTarget();
   nsCOMPtr<nsINode> node = do_QueryInterface(target);
   NS_ENSURE_TRUE(node, NS_ERROR_UNEXPECTED);
 
   // If the target is a document node but it's not editable, we should ignore
   // it because actual focused element's event is going to come.
   if (node->IsNodeOfType(nsINode::eDOCUMENT) &&
       !node->HasFlag(NODE_IS_EDITABLE)) {
     return NS_OK;
@@ -1216,17 +1216,17 @@ EditorEventListener::ShouldHandleNativeK
   // Only return true if the target of the event is a desendant of the active
   // editing host in order to match the similar decision made in
   // nsXBLWindowKeyHandler.
   // Note that IsAcceptableInputEvent doesn't check for the active editing
   // host for keyboard events, otherwise this check would have been
   // unnecessary.  IsAcceptableInputEvent currently makes a similar check for
   // mouse events.
 
-  nsCOMPtr<nsIDOMEventTarget> target = aKeyboardEvent->GetDOMEventTarget();
+  nsIDOMEventTarget* target = aKeyboardEvent->GetDOMEventTarget();
   nsCOMPtr<nsIContent> targetContent = do_QueryInterface(target);
   if (!targetContent) {
     return false;
   }
 
   RefPtr<EditorBase> editorBase(mEditorBase);
   HTMLEditor* htmlEditor = editorBase->AsHTMLEditor();
   if (!htmlEditor) {
diff --git a/editor/libeditor/HTMLAbsPositionEditor.cpp b/editor/libeditor/HTMLAbsPositionEditor.cpp
--- a/editor/libeditor/HTMLAbsPositionEditor.cpp
+++ b/editor/libeditor/HTMLAbsPositionEditor.cpp
@@ -402,17 +402,17 @@ nsresult
 HTMLEditor::GrabberClicked()
 {
   // add a mouse move listener to the editor
   nsresult rv = NS_OK;
   if (!mMouseMotionListenerP) {
     mMouseMotionListenerP = new ResizerMouseMotionListener(*this);
     if (!mMouseMotionListenerP) {return NS_ERROR_NULL_POINTER;}
 
-    nsCOMPtr<nsIDOMEventTarget> piTarget = GetDOMEventTarget();
+    nsIDOMEventTarget* piTarget = GetDOMEventTarget();
     NS_ENSURE_TRUE(piTarget, NS_ERROR_FAILURE);
 
     rv = piTarget->AddEventListener(NS_LITERAL_STRING("mousemove"),
                                      mMouseMotionListenerP,
                                      false, false);
     NS_ASSERTION(NS_SUCCEEDED(rv),
                  "failed to register mouse motion listener");
   }
diff --git a/editor/libeditor/HTMLEditor.cpp b/editor/libeditor/HTMLEditor.cpp
--- a/editor/libeditor/HTMLEditor.cpp
+++ b/editor/libeditor/HTMLEditor.cpp
@@ -4528,54 +4528,53 @@ HTMLEditor::GetReturnInParagraphCreatesN
 
 nsresult
 HTMLEditor::GetReturnInParagraphCreatesNewParagraph(bool* aCreatesNewParagraph)
 {
   *aCreatesNewParagraph = mCRInParagraphCreatesParagraph;
   return NS_OK;
 }
 
-already_AddRefed<nsIContent>
+nsIContent*
 HTMLEditor::GetFocusedContent()
 {
   nsFocusManager* fm = nsFocusManager::GetFocusManager();
   NS_ENSURE_TRUE(fm, nullptr);
 
   nsCOMPtr<nsIContent> focusedContent = fm->GetFocusedContent();
 
   nsCOMPtr<nsIDocument> document = GetDocument();
   if (NS_WARN_IF(!document)) {
     return nullptr;
   }
   bool inDesignMode = document->HasFlag(NODE_IS_EDITABLE);
   if (!focusedContent) {
     // in designMode, nobody gets focus in most cases.
     if (inDesignMode && OurWindowHasFocus()) {
-      nsCOMPtr<nsIContent> rootContent = document->GetRootElement();
-      return rootContent.forget();
+      return document->GetRootElement();
     }
     return nullptr;
   }
 
   if (inDesignMode) {
     return OurWindowHasFocus() &&
       nsContentUtils::ContentIsDescendantOf(focusedContent, document) ?
-        focusedContent.forget() : nullptr;
+        focusedContent.get() : nullptr;
   }
 
   // We're HTML editor for contenteditable
 
   // If the focused content isn't editable, or it has independent selection,
   // we don't have focus.
   if (!focusedContent->HasFlag(NODE_IS_EDITABLE) ||
       focusedContent->HasIndependentSelection()) {
     return nullptr;
   }
   // If our window is focused, we're focused.
-  return OurWindowHasFocus() ? focusedContent.forget() : nullptr;
+  return OurWindowHasFocus() ? focusedContent.get() : nullptr;
 }
 
 already_AddRefed<nsIContent>
 HTMLEditor::GetFocusedContentForIME()
 {
   nsCOMPtr<nsIContent> focusedContent = GetFocusedContent();
   if (!focusedContent) {
     return nullptr;
@@ -4651,25 +4650,25 @@ HTMLEditor::GetActiveEditingHost()
   // we're not active.
   if (!content->HasFlag(NODE_IS_EDITABLE) ||
       content->HasIndependentSelection()) {
     return nullptr;
   }
   return content->GetEditingHost();
 }
 
-already_AddRefed<EventTarget>
+EventTarget*
 HTMLEditor::GetDOMEventTarget()
 {
   // Don't use getDocument here, because we have no way of knowing
   // whether Init() was ever called.  So we need to get the document
   // ourselves, if it exists.
   MOZ_ASSERT(IsInitialized(), "The HTMLEditor has not been initialized yet");
   nsCOMPtr<mozilla::dom::EventTarget> target = GetDocument();
-  return target.forget();
+  return target;
 }
 
 bool
 HTMLEditor::ShouldReplaceRootElement()
 {
   if (!mRootElement) {
     // If we don't know what is our root element, we should find our root.
     return true;
diff --git a/editor/libeditor/HTMLEditor.h b/editor/libeditor/HTMLEditor.h
--- a/editor/libeditor/HTMLEditor.h
+++ b/editor/libeditor/HTMLEditor.h
@@ -104,20 +104,20 @@ public:
 
   // nsIEditor overrides
   NS_IMETHOD GetPreferredIMEState(widget::IMEState* aState) override;
 
   // TextEditor overrides
   NS_IMETHOD BeginningOfDocument() override;
   virtual nsresult HandleKeyPressEvent(
                      WidgetKeyboardEvent* aKeyboardEvent) override;
-  virtual already_AddRefed<nsIContent> GetFocusedContent() override;
+  virtual nsIContent* GetFocusedContent() override;
   virtual already_AddRefed<nsIContent> GetFocusedContentForIME() override;
   virtual bool IsActiveInDOMWindow() override;
-  virtual already_AddRefed<dom::EventTarget> GetDOMEventTarget() override;
+  virtual dom::EventTarget* GetDOMEventTarget() override;
   virtual Element* GetEditorRoot() override;
   virtual already_AddRefed<nsIContent> FindSelectionRoot(
                                          nsINode *aNode) override;
   virtual bool IsAcceptableInputEvent(WidgetGUIEvent* aGUIEvent) override;
   virtual already_AddRefed<nsIContent> GetInputEventTargetContent() override;
   using EditorBase::IsEditable;
   virtual nsresult RemoveAttributeOrEquivalent(
                      Element* aElement,
diff --git a/editor/libeditor/HTMLEditorObjectResizer.cpp b/editor/libeditor/HTMLEditorObjectResizer.cpp
--- a/editor/libeditor/HTMLEditorObjectResizer.cpp
+++ b/editor/libeditor/HTMLEditorObjectResizer.cpp
@@ -532,17 +532,17 @@ HTMLEditor::StartResizing(nsIDOMElement*
   // add a mouse move listener to the editor
   nsresult result = NS_OK;
   if (!mMouseMotionListenerP) {
     mMouseMotionListenerP = new ResizerMouseMotionListener(*this);
     if (!mMouseMotionListenerP) {
       return NS_ERROR_OUT_OF_MEMORY;
     }
 
-    nsCOMPtr<nsIDOMEventTarget> target = GetDOMEventTarget();
+    nsIDOMEventTarget* target = GetDOMEventTarget();
     NS_ENSURE_TRUE(target, NS_ERROR_FAILURE);
 
     result = target->AddEventListener(NS_LITERAL_STRING("mousemove"),
                                       mMouseMotionListenerP, true);
     NS_ASSERTION(NS_SUCCEEDED(result),
                  "failed to register mouse motion listener");
   }
   return result;
diff --git a/editor/libeditor/TextEditor.cpp b/editor/libeditor/TextEditor.cpp
--- a/editor/libeditor/TextEditor.cpp
+++ b/editor/libeditor/TextEditor.cpp
@@ -1717,21 +1717,20 @@ TextEditor::SelectEntireDocument(Selecti
     nsINode* parentNode = GetNodeLocation(childNode, &parentOffset);
 
     return aSelection->Extend(parentNode, parentOffset);
   }
 
   return NS_OK;
 }
 
-already_AddRefed<EventTarget>
+EventTarget*
 TextEditor::GetDOMEventTarget()
 {
-  nsCOMPtr<EventTarget> copy = mEventTarget;
-  return copy.forget();
+  return mEventTarget;
 }
 
 
 nsresult
 TextEditor::SetAttributeOrEquivalent(Element* aElement,
                                      nsIAtom* aAttribute,
                                      const nsAString& aValue,
                                      bool aSuppressTransaction)
diff --git a/editor/libeditor/TextEditor.h b/editor/libeditor/TextEditor.h
--- a/editor/libeditor/TextEditor.h
+++ b/editor/libeditor/TextEditor.h
@@ -124,17 +124,17 @@ public:
   /**
    * Make the given selection span the entire document.
    */
   virtual nsresult SelectEntireDocument(Selection* aSelection) override;
 
   virtual nsresult HandleKeyPressEvent(
                      WidgetKeyboardEvent* aKeyboardEvent) override;
 
-  virtual already_AddRefed<dom::EventTarget> GetDOMEventTarget() override;
+  virtual dom::EventTarget* GetDOMEventTarget() override;
 
   virtual nsresult BeginIMEComposition(WidgetCompositionEvent* aEvent) override;
   virtual nsresult UpdateIMEComposition(
                      WidgetCompositionEvent* aCompositionChangeEvet) override;
 
   virtual already_AddRefed<nsIContent> GetInputEventTargetContent() override;
 
   // Utility Routines, not part of public API
