# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1516457890 -3600
#      Sat Jan 20 15:18:10 2018 +0100
# Node ID 429a5e0d58c329c193b43bbf9a3fcdd3447f2e92
# Parent  63bc52d5d0b09f169a6bd5819008816786394a52
Bug 1431726 part 1 - Remove MaybeResolveConstructor, use GlobalObject::ensureConstructor instead. r=anba

diff --git a/js/src/jsobj.cpp b/js/src/jsobj.cpp
--- a/js/src/jsobj.cpp
+++ b/js/src/jsobj.cpp
@@ -2124,44 +2124,34 @@ JSObject::changeToSingleton(JSContext* c
                                                          obj->taggedProto());
     if (!group)
         return false;
 
     obj->group_ = group;
     return true;
 }
 
-static bool
-MaybeResolveConstructor(JSContext* cx, Handle<GlobalObject*> global, JSProtoKey key)
-{
-    if (global->isStandardClassResolved(key))
-        return true;
-    MOZ_ASSERT(!cx->helperThread());
-
-    return GlobalObject::resolveConstructor(cx, global, key);
-}
-
 bool
 js::GetBuiltinConstructor(JSContext* cx, JSProtoKey key, MutableHandleObject objp)
 {
     MOZ_ASSERT(key != JSProto_Null);
-    Rooted<GlobalObject*> global(cx, cx->global());
-    if (!MaybeResolveConstructor(cx, global, key))
+    Handle<GlobalObject*> global = cx->global();
+    if (!GlobalObject::ensureConstructor(cx, global, key))
         return false;
 
     objp.set(&global->getConstructor(key).toObject());
     return true;
 }
 
 bool
 js::GetBuiltinPrototype(JSContext* cx, JSProtoKey key, MutableHandleObject protop)
 {
     MOZ_ASSERT(key != JSProto_Null);
-    Rooted<GlobalObject*> global(cx, cx->global());
-    if (!MaybeResolveConstructor(cx, global, key))
+    Handle<GlobalObject*> global = cx->global();
+    if (!GlobalObject::ensureConstructor(cx, global, key))
         return false;
 
     protop.set(&global->getPrototype(key).toObject());
     return true;
 }
 
 bool
 js::IsStandardPrototype(JSObject* obj, JSProtoKey key)
diff --git a/js/src/vm/GlobalObject.cpp b/js/src/vm/GlobalObject.cpp
--- a/js/src/vm/GlobalObject.cpp
+++ b/js/src/vm/GlobalObject.cpp
@@ -112,28 +112,21 @@ GlobalObject::skipDeselectedConstructor(
       case JSProto_Atomics:
       case JSProto_SharedArrayBuffer:
         return !cx->compartment()->creationOptions().getSharedMemoryAndAtomicsEnabled();
       default:
         return false;
     }
 }
 
-/* static */ bool
-GlobalObject::ensureConstructor(JSContext* cx, Handle<GlobalObject*> global, JSProtoKey key)
-{
-    if (global->isStandardClassResolved(key))
-        return true;
-    return resolveConstructor(cx, global, key);
-}
-
 /* static*/ bool
 GlobalObject::resolveConstructor(JSContext* cx, Handle<GlobalObject*> global, JSProtoKey key)
 {
     MOZ_ASSERT(!global->isStandardClassResolved(key));
+    MOZ_ASSERT(!cx->helperThread());
 
     // Prohibit collection of allocation metadata. Metadata builders shouldn't
     // need to observe lazily-constructed prototype objects coming into
     // existence. And assertions start to fail when the builder itself attempts
     // an allocation that re-entrantly tries to create the same prototype.
     AutoSuppressAllocationMetadataBuilder suppressMetadata(cx);
 
     // Constructor resolution may execute self-hosted scripts. These
diff --git a/js/src/vm/GlobalObject.h b/js/src/vm/GlobalObject.h
--- a/js/src/vm/GlobalObject.h
+++ b/js/src/vm/GlobalObject.h
@@ -130,23 +130,31 @@ class GlobalObject : public NativeObject
         setSlot(EVAL, ObjectValue(*evalobj));
     }
 
     Value getConstructor(JSProtoKey key) const {
         MOZ_ASSERT(key <= JSProto_LIMIT);
         return getSlot(APPLICATION_SLOTS + key);
     }
     static bool skipDeselectedConstructor(JSContext* cx, JSProtoKey key);
-    static bool ensureConstructor(JSContext* cx, Handle<GlobalObject*> global, JSProtoKey key);
-    static bool resolveConstructor(JSContext* cx, Handle<GlobalObject*> global, JSProtoKey key);
     static bool initBuiltinConstructor(JSContext* cx, Handle<GlobalObject*> global,
                                        JSProtoKey key, HandleObject ctor, HandleObject proto);
 
     static bool maybeResolveGlobalThis(JSContext* cx, Handle<GlobalObject*> global, bool* resolved);
 
+  private:
+    static bool resolveConstructor(JSContext* cx, Handle<GlobalObject*> global, JSProtoKey key);
+
+  public:
+    static bool ensureConstructor(JSContext* cx, Handle<GlobalObject*> global, JSProtoKey key) {
+        if (global->isStandardClassResolved(key))
+            return true;
+        return resolveConstructor(cx, global, key);
+    }
+
     void setConstructor(JSProtoKey key, const Value& v) {
         MOZ_ASSERT(key <= JSProto_LIMIT);
         setSlot(APPLICATION_SLOTS + key, v);
     }
 
     Value getPrototype(JSProtoKey key) const {
         MOZ_ASSERT(key <= JSProto_LIMIT);
         return getSlot(APPLICATION_SLOTS + JSProto_LIMIT + key);
