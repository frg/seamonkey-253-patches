# HG changeset patch
# User Miko Mynttinen <mikokm@gmail.com>
# Date 1515502535 -3600
# Node ID ec9db833ef5b932c483b03f0e3b0cd6632f0f663
# Parent  ebbff374fab878932e571f0626b2fdf4739ceb32
Bug 1428906 - Implement Clone() and copy-constructors for nsDisplayTableBlendMode, nsDisplayTableBlendContainer, and nsDisplayTableFixedPosition r=mattwoodrow

MozReview-Commit-ID: Fr34xFp4n8

diff --git a/layout/painting/crashtests/1428906-1.html b/layout/painting/crashtests/1428906-1.html
new file mode 100644
--- /dev/null
+++ b/layout/painting/crashtests/1428906-1.html
@@ -0,0 +1,16 @@
+<html><head>
+<meta http-equiv="content-type" content="text/html; charset=UTF-8"><style>
+*,m{background:url()repeat center top fixed}
+#a{transform:scale(1)}
+</style>
+<script>
+function eh1(){
+  try{c=a.insertRow()}catch(e){}
+  try{c.appendChild(b)}catch(e){}
+}
+</script>
+</head><body><table id="a">
+<tbody><tr><d id="b">
+<video>
+<source onerror="eh1()">
+</video></d></tr></tbody></table></body></html>
diff --git a/layout/painting/crashtests/crashtests.list b/layout/painting/crashtests/crashtests.list
--- a/layout/painting/crashtests/crashtests.list
+++ b/layout/painting/crashtests/crashtests.list
@@ -2,8 +2,9 @@ load 1402183-1.html
 skip-if(!(stylo||styloVsGecko)||Android) load 1407470-1.html
 load 1413073-1.html
 load 1413073-2.html
 load 1405881-1.html
 load 1418177-1.html
 load 1418722-1.html
 load 1419917.html
 load 1425271-1.html
+load 1428906-1.html
diff --git a/layout/painting/nsDisplayList.h b/layout/painting/nsDisplayList.h
--- a/layout/painting/nsDisplayList.h
+++ b/layout/painting/nsDisplayList.h
@@ -5103,17 +5103,29 @@ class nsDisplayTableBlendMode : public n
 public:
   nsDisplayTableBlendMode(nsDisplayListBuilder* aBuilder, nsIFrame* aFrame,
                           nsDisplayList* aList, uint8_t aBlendMode,
                           const ActiveScrolledRoot* aActiveScrolledRoot,
                           uint32_t aIndex, nsIFrame* aAncestorFrame)
     : nsDisplayBlendMode(aBuilder, aFrame, aList, aBlendMode, aActiveScrolledRoot, aIndex)
     , mAncestorFrame(aAncestorFrame)
     , mTableType(GetTableTypeFromFrame(aAncestorFrame))
-  { }
+  {}
+
+  nsDisplayTableBlendMode(nsDisplayListBuilder* aBuilder,
+                          const nsDisplayTableBlendMode& aOther)
+    : nsDisplayBlendMode(aBuilder, aOther)
+    , mAncestorFrame(aOther.mAncestorFrame)
+    , mTableType(aOther.mTableType)
+  {}
+
+  virtual nsDisplayWrapList* Clone(nsDisplayListBuilder* aBuilder) const override
+  {
+    return new (aBuilder) nsDisplayTableBlendMode(aBuilder, *this);
+  }
 
   virtual nsIFrame* FrameForInvalidation() const override { return mAncestorFrame; }
 
   virtual uint32_t GetPerFrameKey() const override {
     return (mIndex << (TYPE_BITS + static_cast<uint8_t>(TableTypeBits::COUNT))) |
            (static_cast<uint8_t>(mTableType) << TYPE_BITS) |
            nsDisplayItem::GetPerFrameKey();
   }
@@ -5195,16 +5207,22 @@ protected:
 class nsDisplayTableBlendContainer : public nsDisplayBlendContainer
 {
 public:
   static nsDisplayTableBlendContainer*
   CreateForBackgroundBlendMode(nsDisplayListBuilder* aBuilder, nsIFrame* aFrame,
                                nsDisplayList* aList,
                                const ActiveScrolledRoot* aActiveScrolledRoot,
                                nsIFrame* aAncestorFrame);
+
+  virtual nsDisplayWrapList* Clone(nsDisplayListBuilder* aBuilder) const override
+  {
+    return new (aBuilder) nsDisplayTableBlendContainer(aBuilder, *this);
+  }
+
   virtual nsIFrame* FrameForInvalidation() const override { return mAncestorFrame; }
 
   virtual uint32_t GetPerFrameKey() const override {
     return (static_cast<uint8_t>(mTableType) << TYPE_BITS) |
            nsDisplayItem::GetPerFrameKey();
   }
 
   NS_DISPLAY_DECL_NAME("BlendContainer", TYPE_BLEND_CONTAINER)
@@ -5212,17 +5230,24 @@ public:
 protected:
   nsDisplayTableBlendContainer(nsDisplayListBuilder* aBuilder, nsIFrame* aFrame,
                                nsDisplayList* aList,
                                const ActiveScrolledRoot* aActiveScrolledRoot,
                                bool aIsForBackground, nsIFrame* aAncestorFrame)
     : nsDisplayBlendContainer(aBuilder, aFrame, aList, aActiveScrolledRoot, aIsForBackground)
     , mAncestorFrame(aAncestorFrame)
     , mTableType(GetTableTypeFromFrame(aAncestorFrame))
-  { }
+  {}
+
+  nsDisplayTableBlendContainer(nsDisplayListBuilder* aBuilder,
+                               const nsDisplayTableBlendContainer& aOther)
+    : nsDisplayBlendContainer(aBuilder, aOther)
+    , mAncestorFrame(aOther.mAncestorFrame)
+    , mTableType(aOther.mTableType)
+  {}
 
   nsIFrame* mAncestorFrame;
   TableType mTableType;
 };
 
 
 /**
  * nsDisplayOwnLayer constructor flags. If we nest this class inside
@@ -5461,31 +5486,32 @@ public:
                          nsDisplayList* aList,
                          const ActiveScrolledRoot* aActiveScrolledRoot);
   nsDisplayFixedPosition(nsDisplayListBuilder* aBuilder,
                          const nsDisplayFixedPosition& aOther)
     : nsDisplayOwnLayer(aBuilder, aOther)
     , mAnimatedGeometryRootForScrollMetadata(aOther.mAnimatedGeometryRootForScrollMetadata)
     , mIndex(aOther.mIndex)
     , mIsFixedBackground(aOther.mIsFixedBackground)
-  {}
+  {
+    MOZ_COUNT_CTOR(nsDisplayFixedPosition);
+  }
 
   static nsDisplayFixedPosition* CreateForFixedBackground(nsDisplayListBuilder* aBuilder,
                                                           nsIFrame* aFrame,
                                                           nsDisplayBackgroundImage* aImage,
                                                           uint32_t aIndex);
 
 
 #ifdef NS_BUILD_REFCNT_LOGGING
   virtual ~nsDisplayFixedPosition();
 #endif
 
   virtual nsDisplayWrapList* Clone(nsDisplayListBuilder* aBuilder) const override
   {
-    MOZ_COUNT_CTOR(nsDisplayFixedPosition);
     return new (aBuilder) nsDisplayFixedPosition(aBuilder, *this);
   }
 
   virtual already_AddRefed<Layer> BuildLayer(nsDisplayListBuilder* aBuilder,
                                              LayerManager* aManager,
                                              const ContainerLayerParameters& aContainerParameters) override;
   NS_DISPLAY_DECL_NAME("FixedPosition", TYPE_FIXED_POSITION)
   virtual LayerState GetLayerState(nsDisplayListBuilder* aBuilder,
@@ -5532,28 +5558,42 @@ class nsDisplayTableFixedPosition : publ
 {
 public:
   static nsDisplayTableFixedPosition* CreateForFixedBackground(nsDisplayListBuilder* aBuilder,
                                                                nsIFrame* aFrame,
                                                                nsDisplayBackgroundImage* aImage,
                                                                uint32_t aIndex,
                                                                nsIFrame* aAncestorFrame);
 
+  virtual nsDisplayWrapList* Clone(nsDisplayListBuilder* aBuilder) const override
+  {
+    return new (aBuilder) nsDisplayTableFixedPosition(aBuilder, *this);
+  }
+
   virtual nsIFrame* FrameForInvalidation() const override { return mAncestorFrame; }
 
   virtual uint32_t GetPerFrameKey() const override {
     return (mIndex << (TYPE_BITS + static_cast<uint8_t>(TableTypeBits::COUNT))) |
            (static_cast<uint8_t>(mTableType) << TYPE_BITS) |
            nsDisplayItem::GetPerFrameKey();
   }
 
   NS_DISPLAY_DECL_NAME("TableFixedPosition", TYPE_TABLE_FIXED_POSITION)
 protected:
   nsDisplayTableFixedPosition(nsDisplayListBuilder* aBuilder, nsIFrame* aFrame,
-                              nsDisplayList* aList, uint32_t aIndex, nsIFrame* aAncestorFrame);
+                              nsDisplayList* aList, uint32_t aIndex,
+                              nsIFrame* aAncestorFrame);
+
+  nsDisplayTableFixedPosition(nsDisplayListBuilder* aBuilder,
+                              const nsDisplayTableFixedPosition& aOther)
+    : nsDisplayFixedPosition(aBuilder, aOther)
+    , mAncestorFrame(aOther.mAncestorFrame)
+    , mTableType(aOther.mTableType)
+  {}
+
 
   nsIFrame* mAncestorFrame;
   TableType mTableType;
 };
 
 /**
  * This creates an empty scrollable layer. It has no child layers.
  * It is used to record the existence of a scrollable frame in the layer
