# HG changeset patch
# User Henrik Skupin <mail@hskupin.info>
# Date 1509571119 -3600
# Node ID a29ab4c986c761f64d5889f715390bd82e9137e7
# Parent  cd2a5b5cf752194818cee5f96f2ae5cdcbf3d564
Bug 1321516 - Switch to WebDriver conformant interactability checks. r=ato

This change will cause Marionette to use the webdriver conformant
clickElement method instead of the legacy one by default. It means
that additional checks will be performed to ensure that the element
to click onto is visible, and not obscured by other elements. Also
it will be scrolled into view in case it is located outside of the
current view port.

Even it is used by default, the behavior can be controlled with the
"moz:webdriverClick" capability. Setting to to 'false' when creating
a new session, will cause a fallback to legacy clickElement.

MozReview-Commit-ID: E560k62Q2J9

diff --git a/testing/marionette/harness/marionette_harness/tests/unit/test_capabilities.py b/testing/marionette/harness/marionette_harness/tests/unit/test_capabilities.py
--- a/testing/marionette/harness/marionette_harness/tests/unit/test_capabilities.py
+++ b/testing/marionette/harness/marionette_harness/tests/unit/test_capabilities.py
@@ -63,23 +63,23 @@ class TestCapabilities(MarionetteTestCas
             else:
                 current_profile = convert_path(self.marionette.instance.runner.profile.profile)
             self.assertEqual(convert_path(str(self.caps["moz:profile"])), current_profile)
             self.assertEqual(convert_path(str(self.marionette.profile)), current_profile)
 
         self.assertIn("moz:accessibilityChecks", self.caps)
         self.assertFalse(self.caps["moz:accessibilityChecks"])
         self.assertIn("moz:webdriverClick", self.caps)
-        self.assertEqual(self.caps["moz:webdriverClick"], False)
+        self.assertEqual(self.caps["moz:webdriverClick"], True)
 
-    def test_set_webdriver_click(self):
+    def test_disable_webdriver_click(self):
         self.marionette.delete_session()
-        self.marionette.start_session({"moz:webdriverClick": True})
+        self.marionette.start_session({"moz:webdriverClick": False})
         caps = self.marionette.session_capabilities
-        self.assertEqual(True, caps["moz:webdriverClick"])
+        self.assertEqual(False, caps["moz:webdriverClick"])
 
     def test_we_get_valid_uuid4_when_creating_a_session(self):
         self.assertNotIn("{", self.marionette.session_id,
                          "Session ID has {{}} in it: {}".format(
                              self.marionette.session_id))
 
 
 class TestCapabilityMatching(MarionetteTestCase):
diff --git a/testing/marionette/session.js b/testing/marionette/session.js
--- a/testing/marionette/session.js
+++ b/testing/marionette/session.js
@@ -361,17 +361,17 @@ session.Capabilities = class extends Map
       // features
       ["rotatable", appinfo.name == "B2G"],
 
       // proprietary
       ["moz:accessibilityChecks", false],
       ["moz:headless", Cc["@mozilla.org/gfx/info;1"].getService(Ci.nsIGfxInfo).isHeadless],
       ["moz:processID", Services.appinfo.processID],
       ["moz:profile", maybeProfile()],
-      ["moz:webdriverClick", false],
+      ["moz:webdriverClick", true],
     ]);
   }
 
   /**
    * @param {string} key
    *     Capability key.
    * @param {(string|number|boolean)} value
    *     JSON-safe capability value.
diff --git a/testing/marionette/test_session.js b/testing/marionette/test_session.js
--- a/testing/marionette/test_session.js
+++ b/testing/marionette/test_session.js
@@ -374,17 +374,17 @@ add_test(function test_Capabilities_ctor
   ok(caps.get("timeouts") instanceof session.Timeouts);
   ok(caps.get("proxy") instanceof session.Proxy);
 
   ok(caps.has("rotatable"));
 
   equal(false, caps.get("moz:accessibilityChecks"));
   ok(caps.has("moz:processID"));
   ok(caps.has("moz:profile"));
-  equal(false, caps.get("moz:webdriverClick"));
+  equal(true, caps.get("moz:webdriverClick"));
 
   run_next_test();
 });
 
 add_test(function test_Capabilities_toString() {
   equal("[object session.Capabilities]", new session.Capabilities().toString());
 
   run_next_test();
@@ -442,17 +442,17 @@ add_test(function test_Capabilities_from
   let proxyConfig = {proxyType: "manual"};
   caps = fromJSON({proxy: proxyConfig});
   equal("manual", caps.get("proxy").proxyType);
 
   let timeoutsConfig = {implicit: 123};
   caps = fromJSON({timeouts: timeoutsConfig});
   equal(123, caps.get("timeouts").implicit);
 
-  equal(false, caps.get("moz:webdriverClick"));
+  equal(true, caps.get("moz:webdriverClick"));
   caps = fromJSON({"moz:webdriverClick": true});
   equal(true, caps.get("moz:webdriverClick"));
   Assert.throws(() => fromJSON({"moz:webdriverClick": "foo"}));
   Assert.throws(() => fromJSON({"moz:webdriverClick": 1}));
 
   caps = fromJSON({"moz:accessibilityChecks": true});
   equal(true, caps.get("moz:accessibilityChecks"));
   caps = fromJSON({"moz:accessibilityChecks": false});

