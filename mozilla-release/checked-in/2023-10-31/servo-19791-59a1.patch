# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1516201789 21600
# Node ID 877584981d6bdacaa24b1860030c47e052ad1ea3
# Parent  51b2e0af7ec7ed59f9da12e5ffbdb6d7d5eb3a89
servo: Merge #19791 - style: Cleanup a bit the calc code (from emilio:calc-clean); r=jdm

I happened to be looking at it.

Source-Repo: https://github.com/servo/servo
Source-Revision: 8a740aa4d1e75aad9308e3eb805ed3e15078fb59

diff --git a/servo/components/style/values/computed/length.rs b/servo/components/style/values/computed/length.rs
--- a/servo/components/style/values/computed/length.rs
+++ b/servo/components/style/values/computed/length.rs
@@ -87,25 +87,22 @@ impl CalcLengthOrPercentage {
     /// Returns a new `CalcLengthOrPercentage`.
     #[inline]
     pub fn new(length: Length, percentage: Option<Percentage>) -> Self {
         Self::with_clamping_mode(length, percentage, AllowedNumericType::All)
     }
 
     /// Returns a new `CalcLengthOrPercentage` with a specific clamping mode.
     #[inline]
-    pub fn with_clamping_mode(length: Length,
-                              percentage: Option<Percentage>,
-                              clamping_mode: AllowedNumericType)
-                              -> Self {
-        Self {
-            clamping_mode: clamping_mode,
-            length: length,
-            percentage: percentage,
-        }
+    pub fn with_clamping_mode(
+        length: Length,
+        percentage: Option<Percentage>,
+        clamping_mode: AllowedNumericType,
+    ) -> Self {
+        Self { clamping_mode, length, percentage, }
     }
 
     /// Returns this `calc()` as a `<length>`.
     ///
     /// Panics in debug mode if a percentage is present in the expression.
     #[inline]
     pub fn length(&self) -> CSSPixelLength {
         debug_assert!(self.percentage.is_none());
@@ -131,18 +128,19 @@ impl CalcLengthOrPercentage {
     }
 
     /// Convert the computed value into used value.
     #[inline]
     pub fn to_used_value(&self, container_len: Option<Au>) -> Option<Au> {
         self.to_pixel_length(container_len).map(Au::from)
     }
 
-    /// If there are special rules for computing percentages in a value (e.g. the height property),
-    /// they apply whenever a calc() expression contains percentages.
+    /// If there are special rules for computing percentages in a value (e.g.
+    /// the height property), they apply whenever a calc() expression contains
+    /// percentages.
     pub fn to_pixel_length(&self, container_len: Option<Au>) -> Option<Length> {
         match (container_len, self.percentage) {
             (Some(len), Some(percent)) => {
                 let pixel = self.length.px() + len.scale_by(percent.0).to_f32_px();
                 Some(Length::new(self.clamping_mode.clamp(pixel)))
             },
             (_, None) => Some(self.length()),
             _ => None,
@@ -221,19 +219,25 @@ impl ToCss for CalcLengthOrPercentage {
         length.abs().to_css(dest)?;
 
         dest.write_str(")")
     }
 }
 
 impl specified::CalcLengthOrPercentage {
     /// Compute the value, zooming any absolute units by the zoom function.
-    fn to_computed_value_with_zoom<F>(&self, context: &Context, zoom_fn: F,
-                                      base_size: FontBaseSize) -> CalcLengthOrPercentage
-        where F: Fn(Length) -> Length {
+    fn to_computed_value_with_zoom<F>(
+        &self,
+        context: &Context,
+        zoom_fn: F,
+        base_size: FontBaseSize,
+    ) -> CalcLengthOrPercentage
+    where
+        F: Fn(Length) -> Length,
+    {
         use std::f32;
         let mut length = 0.;
 
         if let Some(absolute) = self.absolute {
             length += zoom_fn(absolute.to_computed_value(context)).px();
         }
 
         for val in &[self.vw.map(ViewportPercentageLength::Vw),
@@ -258,17 +262,21 @@ impl specified::CalcLengthOrPercentage {
         CalcLengthOrPercentage {
             clamping_mode: self.clamping_mode,
             length: Length::new(length.min(f32::MAX).max(f32::MIN)),
             percentage: self.percentage,
         }
     }
 
     /// Compute font-size or line-height taking into account text-zoom if necessary.
-    pub fn to_computed_value_zoomed(&self, context: &Context, base_size: FontBaseSize) -> CalcLengthOrPercentage {
+    pub fn to_computed_value_zoomed(
+        &self,
+        context: &Context,
+        base_size: FontBaseSize,
+    ) -> CalcLengthOrPercentage {
         self.to_computed_value_with_zoom(context, |abs| context.maybe_zoom_text(abs.into()).0, base_size)
     }
 
     /// Compute the value into pixel length as CSSFloat without context,
     /// so it returns Err(()) if there is any non-absolute unit.
     pub fn to_computed_pixel_length_without_context(&self) -> Result<CSSFloat, ()> {
         if self.vw.is_some() || self.vh.is_some() || self.vmin.is_some() || self.vmax.is_some() ||
            self.em.is_some() || self.ex.is_some() || self.ch.is_some() || self.rem.is_some() ||
diff --git a/servo/components/style/values/specified/calc.rs b/servo/components/style/values/specified/calc.rs
--- a/servo/components/style/values/specified/calc.rs
+++ b/servo/components/style/values/specified/calc.rs
@@ -59,19 +59,20 @@ pub enum CalcUnit {
     /// `<angle>`
     Angle,
     /// `<time>`
     Time,
 }
 
 /// A struct to hold a simplified `<length>` or `<percentage>` expression.
 ///
-/// In some cases, e.g. DOMMatrix, we support calc(), but reject all the relative lengths, and
-/// to_computed_pixel_length_without_context() handles this case. Therefore, if you want to add a
-/// new field, please make sure this function work properly.
+/// In some cases, e.g. DOMMatrix, we support calc(), but reject all the
+/// relative lengths, and to_computed_pixel_length_without_context() handles
+/// this case. Therefore, if you want to add a new field, please make sure this
+/// function work properly.
 #[derive(Clone, Copy, Debug, Default, MallocSizeOf, PartialEq)]
 #[allow(missing_docs)]
 pub struct CalcLengthOrPercentage {
     pub clamping_mode: AllowedNumericType,
     pub absolute: Option<AbsoluteLength>,
     pub vw: Option<CSSFloat>,
     pub vh: Option<CSSFloat>,
     pub vmin: Option<CSSFloat>,
