# HG changeset patch
# User Tom Ritter <tom@mozilla.com>
# Date 1508301451 18000
#      Tue Oct 17 23:37:31 2017 -0500
# Node ID 77edd9d741494c058a49d75737223a0fe947fdf6
# Parent  8317f91b5666c845767c1e604248903c43c6e363
Bug 1409619 Resolve signed/unsigned comparison warning by fixing function definitions to be signed in widget/windows/TSFTextStore r=jimm

The functions changed are given signed arguments (that are converted to unsigned).
Changing them to signed resolves the warnings and preserves the original values.

MozReview-Commit-ID: BxIAECFiuQR

diff --git a/widget/windows/TSFTextStore.cpp b/widget/windows/TSFTextStore.cpp
--- a/widget/windows/TSFTextStore.cpp
+++ b/widget/windows/TSFTextStore.cpp
@@ -4748,18 +4748,18 @@ TSFTextStore::RecordCompositionStartActi
   }
 
   return RecordCompositionStartAction(aComposition, start, length,
                                       aPreserveSelection);
 }
 
 HRESULT
 TSFTextStore::RecordCompositionStartAction(ITfCompositionView* aComposition,
-                                           ULONG aStart,
-                                           ULONG aLength,
+                                           LONG aStart,
+                                           LONG aLength,
                                            bool aPreserveSelection)
 {
   MOZ_LOG(sTextStoreLog, LogLevel::Debug,
     ("0x%p   TSFTextStore::RecordCompositionStartAction("
      "aComposition=0x%p, aStart=%d, aLength=%d, aPreserveSelection=%s), "
      "mComposition.mView=0x%p",
      this, aComposition, aStart, aLength, GetBoolName(aPreserveSelection),
      mComposition.mView.get()));
diff --git a/widget/windows/TSFTextStore.h b/widget/windows/TSFTextStore.h
--- a/widget/windows/TSFTextStore.h
+++ b/widget/windows/TSFTextStore.h
@@ -309,18 +309,18 @@ protected:
                               ITfRange* aNewRange);
 
   // Following methods record composing action(s) to mPendingActions.
   // They will be flushed FlushPendingActions().
   HRESULT  RecordCompositionStartAction(ITfCompositionView* aCompositionView,
                                         ITfRange* aRange,
                                         bool aPreserveSelection);
   HRESULT  RecordCompositionStartAction(ITfCompositionView* aComposition,
-                                        ULONG aStart,
-                                        ULONG aLength,
+                                        LONG aStart,
+                                        LONG aLength,
                                         bool aPreserveSelection);
   HRESULT  RecordCompositionUpdateAction();
   HRESULT  RecordCompositionEndAction();
 
   // DispatchEvent() dispatches the event and if it may not be handled
   // synchronously, this makes the instance not notify TSF of pending
   // notifications until next notification from content.
   void     DispatchEvent(WidgetGUIEvent& aEvent);
@@ -688,24 +688,24 @@ protected:
    * composition immediately before (e.g., see InsertTextAtSelectionInternal()).
    *
    * @param aStart              The inserted offset you expected.
    * @param aLength             The inserted text length you expected.
    * @return                    true if the last pending actions are
    *                            COMPOSITION_START and COMPOSITION_END and
    *                            aStart and aLength match their information.
    */
-  bool WasTextInsertedWithoutCompositionAt(ULONG aStart, ULONG aLength) const
+  bool WasTextInsertedWithoutCompositionAt(LONG aStart, LONG aLength) const
   {
     if (mPendingActions.Length() < 2) {
       return false;
     }
     const PendingAction& pendingLastAction = mPendingActions.LastElement();
     if (pendingLastAction.mType != PendingAction::COMPOSITION_END ||
-        pendingLastAction.mData.Length() != aLength) {
+        pendingLastAction.mData.Length() != ULONG(aLength)) {
       return false;
     }
     const PendingAction& pendingPreLastAction =
       mPendingActions[mPendingActions.Length() - 2];
     return pendingPreLastAction.mType == PendingAction::COMPOSITION_START &&
            pendingPreLastAction.mSelectionStart == aStart;
   }
 
