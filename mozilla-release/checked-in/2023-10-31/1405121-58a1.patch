# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1507024956 -7200
#      Tue Oct 03 12:02:36 2017 +0200
# Node ID c5c5b9eff4c09691af9e9b3b2f2b18a576c15775
# Parent  ee9115e30b1da31c0080e090b99a59c33a7d1a64
Bug 1405121 - Add fast path for native objects to TestIntegrityLevel. r=jandem

diff --git a/js/src/jsobj.cpp b/js/src/jsobj.cpp
--- a/js/src/jsobj.cpp
+++ b/js/src/jsobj.cpp
@@ -572,55 +572,117 @@ js::SetIntegrityLevel(JSContext* cx, Han
     if (level == IntegrityLevel::Frozen && obj->isNative()) {
         if (!ObjectElements::FreezeElements(cx, obj.as<NativeObject>()))
             return false;
     }
 
     return true;
 }
 
+static bool
+ResolveLazyProperties(JSContext* cx, HandleNativeObject obj)
+{
+    const Class* clasp = obj->getClass();
+    if (JSEnumerateOp enumerate = clasp->getEnumerate()) {
+        if (!enumerate(cx, obj))
+            return false;
+    }
+    if (clasp->getNewEnumerate() && clasp->getResolve()) {
+        AutoIdVector properties(cx);
+        if (!clasp->getNewEnumerate()(cx, obj, properties, /* enumerableOnly = */ false))
+            return false;
+
+        RootedId id(cx);
+        for (size_t i = 0; i < properties.length(); i++) {
+            id = properties[i];
+            bool found;
+            if (!HasOwnProperty(cx, obj, id, &found))
+                return false;
+        }
+    }
+    return true;
+}
+
 // ES6 draft rev33 (12 Feb 2015) 7.3.15
 bool
 js::TestIntegrityLevel(JSContext* cx, HandleObject obj, IntegrityLevel level, bool* result)
 {
     // Steps 3-6. (Steps 1-2 are redundant assertions.)
     bool status;
     if (!IsExtensible(cx, obj, &status))
         return false;
     if (status) {
         *result = false;
         return true;
     }
 
-    // Steps 7-8.
-    AutoIdVector props(cx);
-    if (!GetPropertyKeys(cx, obj, JSITER_HIDDEN | JSITER_OWNONLY | JSITER_SYMBOLS, &props))
-        return false;
-
-    // Step 9.
-    RootedId id(cx);
-    Rooted<PropertyDescriptor> desc(cx);
-    for (size_t i = 0, len = props.length(); i < len; i++) {
-        id = props[i];
-
-        // Steps 9.a-b.
-        if (!GetOwnPropertyDescriptor(cx, obj, id, &desc))
+    // Fast path for native objects.
+    if (obj->isNative()) {
+        HandleNativeObject nobj = obj.as<NativeObject>();
+
+        // Force lazy properties to be resolved.
+        if (!ResolveLazyProperties(cx, nobj))
             return false;
 
-        // Step 9.c.
-        if (!desc.object())
-            continue;
-
-        // Steps 9.c.i-ii.
-        if (desc.configurable() ||
-            (level == IntegrityLevel::Frozen && desc.isDataDescriptor() && desc.writable()))
+        // Typed array elements are non-configurable, writable properties, so
+        // if any elements are present, the typed array cannot be frozen.
+        if (nobj->is<TypedArrayObject>() && nobj->as<TypedArrayObject>().length() > 0 &&
+            level == IntegrityLevel::Frozen)
         {
             *result = false;
             return true;
         }
+
+        // Unless the frozen flag is set, dense elements are configurable.
+        if (nobj->getDenseInitializedLength() > 0 && !nobj->denseElementsAreFrozen()) {
+            *result = false;
+            return true;
+        }
+
+        // Steps 7-9.
+        for (Shape::Range<NoGC> r(nobj->lastProperty()); !r.empty(); r.popFront()) {
+            Shape* shape = &r.front();
+
+            // Steps 9.c.i-ii.
+            if (shape->configurable() ||
+                (level == IntegrityLevel::Frozen &&
+                 shape->isDataDescriptor() && shape->writable()))
+            {
+                *result = false;
+                return true;
+            }
+        }
+    } else {
+        // Steps 7-8.
+        AutoIdVector props(cx);
+        if (!GetPropertyKeys(cx, obj, JSITER_HIDDEN | JSITER_OWNONLY | JSITER_SYMBOLS, &props))
+            return false;
+
+        // Step 9.
+        RootedId id(cx);
+        Rooted<PropertyDescriptor> desc(cx);
+        for (size_t i = 0, len = props.length(); i < len; i++) {
+            id = props[i];
+
+            // Steps 9.a-b.
+            if (!GetOwnPropertyDescriptor(cx, obj, id, &desc))
+                return false;
+
+            // Step 9.c.
+            if (!desc.object())
+                continue;
+
+            // Steps 9.c.i-ii.
+            if (desc.configurable() ||
+                (level == IntegrityLevel::Frozen && desc.isDataDescriptor() && desc.writable()))
+            {
+                *result = false;
+                return true;
+            }
+        }
     }
 
     // Step 10.
     *result = true;
     return true;
 }
 
 
@@ -2721,36 +2783,18 @@ js::PreventExtensions(JSContext* cx, Han
 
     if (!obj->nonProxyIsExtensible())
         return result.succeed();
 
     if (!MaybeConvertUnboxedObjectToNative(cx, obj))
         return false;
 
     // Force lazy properties to be resolved.
-    if (obj->isNative()) {
-        const Class* clasp = obj->getClass();
-        if (JSEnumerateOp enumerate = clasp->getEnumerate()) {
-            if (!enumerate(cx, obj.as<NativeObject>()))
-                return false;
-        }
-        if (clasp->getNewEnumerate() && clasp->getResolve()) {
-            AutoIdVector properties(cx);
-            if (!clasp->getNewEnumerate()(cx, obj, properties, /* enumerableOnly = */ false))
-                return false;
-
-            RootedId id(cx);
-            for (size_t i = 0; i < properties.length(); i++) {
-                id = properties[i];
-                bool found;
-                if (!HasOwnProperty(cx, obj, id, &found))
-                    return false;
-            }
-        }
-    }
+    if (obj->isNative() && !ResolveLazyProperties(cx, obj.as<NativeObject>()))
+        return false;
 
     // Sparsify dense elements, to make sure no element can be added without a
     // call to isExtensible, at the cost of performance. If the object is being
     // frozen, the caller is responsible for freezing the elements (and all
     // other properties).
     if (obj->isNative() && level != IntegrityLevel::Frozen) {
         if (!NativeObject::sparsifyDenseElements(cx, obj.as<NativeObject>()))
             return false;
diff --git a/js/src/tests/non262/TypedArray/test-integrity-level-detached.js b/js/src/tests/non262/TypedArray/test-integrity-level-detached.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/TypedArray/test-integrity-level-detached.js
@@ -0,0 +1,104 @@
+const EMPTY = 0;
+const INLINE_STORAGE = 10;
+const NON_INLINE_STORAGE = 1024;
+
+class DetachedInt32Array extends Int32Array {
+    constructor(...args) {
+        super(...args);
+        detachArrayBuffer(this.buffer);
+    }
+}
+
+function throwsTypeError(fn) {
+    try {
+        fn();
+    } catch (e) {
+        assertEq(e instanceof TypeError, true);
+        return true;
+    }
+    return false;
+}
+
+// Non-standard: Accessing elements of detached array buffers should throw, but
+// this is currently not implemented.
+const ACCESS_ON_DETACHED_ARRAY_BUFFER_THROWS = (() => {
+    let ta = new DetachedInt32Array(10);
+    let throws = throwsTypeError(() => ta[0]);
+    // Ensure [[Get]] and [[GetOwnProperty]] return consistent results.
+    assertEq(throwsTypeError(() => Object.getOwnPropertyDescriptor(ta, 0)), throws);
+    return throws;
+})();
+
+function maybeThrowOnDetached(fn, returnValue) {
+    if (ACCESS_ON_DETACHED_ARRAY_BUFFER_THROWS) {
+        assertThrowsInstanceOf(fn, TypeError);
+        return returnValue;
+    }
+    return fn();
+}
+
+// Empty typed arrays can be sealed.
+{
+    let ta = new DetachedInt32Array(EMPTY);
+    Object.seal(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(Object.isSealed(ta), true);
+    assertEq(Object.isFrozen(ta), true);
+}
+
+// Non-empty typed arrays can be sealed, but calling TestIntegrityLevel will
+// throw on detached typed arrays.
+for (let length of [INLINE_STORAGE, NON_INLINE_STORAGE]) {
+    let ta = new DetachedInt32Array(length);
+    Object.seal(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(maybeThrowOnDetached(() => Object.isSealed(ta), true), true);
+    assertEq(maybeThrowOnDetached(() => Object.isFrozen(ta), true), true);
+}
+
+// Empty typed arrays can be frozen.
+{
+    let ta = new DetachedInt32Array(EMPTY);
+    Object.freeze(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(Object.isSealed(ta), true);
+    assertEq(Object.isFrozen(ta), true);
+}
+
+// Non-empty typed arrays cannot be frozen.
+for (let length of [INLINE_STORAGE, NON_INLINE_STORAGE]) {
+    let ta = new DetachedInt32Array(length);
+    maybeThrowOnDetached(() => Object.freeze(ta));
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(maybeThrowOnDetached(() => Object.isSealed(ta), true), true);
+    assertEq(maybeThrowOnDetached(() => Object.isFrozen(ta), true), true);
+}
+
+// Non-extensible empty typed arrays are sealed and frozen.
+{
+    let ta = new DetachedInt32Array(EMPTY);
+    Object.preventExtensions(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(Object.isSealed(ta), true);
+    assertEq(Object.isFrozen(ta), true);
+}
+
+// Calling TestIntegrityLevel will throw on detached typed arrays with non-zero
+// length.
+for (let length of [INLINE_STORAGE, NON_INLINE_STORAGE]) {
+    let ta = new DetachedInt32Array(length);
+    Object.preventExtensions(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(maybeThrowOnDetached(() => Object.isSealed(ta), true), true);
+    assertEq(maybeThrowOnDetached(() => Object.isFrozen(ta), true), true);
+}
+
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
diff --git a/js/src/tests/non262/TypedArray/test-integrity-level.js b/js/src/tests/non262/TypedArray/test-integrity-level.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/TypedArray/test-integrity-level.js
@@ -0,0 +1,67 @@
+const EMPTY = 0;
+const INLINE_STORAGE = 10;
+const NON_INLINE_STORAGE = 1024;
+
+// Empty typed arrays can be sealed.
+{
+    let ta = new Int32Array(EMPTY);
+    Object.seal(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(Object.isSealed(ta), true);
+    assertEq(Object.isFrozen(ta), true);
+}
+
+// Non-empty typed arrays can be sealed.
+for (let length of [INLINE_STORAGE, NON_INLINE_STORAGE]) {
+    let ta = new Int32Array(length);
+    Object.seal(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(Object.isSealed(ta), true);
+    assertEq(Object.isFrozen(ta), false);
+}
+
+// Empty typed arrays can be frozen.
+{
+    let ta = new Int32Array(EMPTY);
+    Object.freeze(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(Object.isSealed(ta), true);
+    assertEq(Object.isFrozen(ta), true);
+}
+
+// Non-empty typed arrays cannot be frozen.
+for (let length of [INLINE_STORAGE, NON_INLINE_STORAGE]) {
+    let ta = new Int32Array(length);
+    assertThrowsInstanceOf(() => Object.freeze(ta), TypeError);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(Object.isSealed(ta), true);
+    assertEq(Object.isFrozen(ta), false);
+}
+
+// Non-extensible empty typed arrays are sealed and frozen.
+{
+    let ta = new Int32Array(EMPTY);
+    Object.preventExtensions(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(Object.isSealed(ta), true);
+    assertEq(Object.isFrozen(ta), true);
+}
+
+// Non-extensible non-empty typed arrays are sealed, but aren't frozen.
+for (let length of [INLINE_STORAGE, NON_INLINE_STORAGE]) {
+    let ta = new Int32Array(length);
+    Object.preventExtensions(ta);
+
+    assertEq(Object.isExtensible(ta), false);
+    assertEq(Object.isSealed(ta), true);
+    assertEq(Object.isFrozen(ta), false);
+}
+
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
