# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1510525189 -39600
# Node ID ef86c22e6f1fecc38d80fd6144fcef4f8828982b
# Parent  b16e928014ff1f7e220bc453b0503d36ff56d1d6
Bug 1416613 - Make Preferences::InitStaticMembers() slightly faster. r=glandium

InitStaticMembers() is the hottest function within libpref, because it is
called from all the getters and setters. This patch tweaks it so the common
case is handled with a single comparison, instead of two. This is valid because
sPreferences is nulled at the same time that sShutdown is set.

The patch also tweaks some other conditions to take advantage of this fact.

MozReview-Commit-ID: C74fLWOw7bE

diff --git a/modules/libpref/Preferences.cpp b/modules/libpref/Preferences.cpp
--- a/modules/libpref/Preferences.cpp
+++ b/modules/libpref/Preferences.cpp
@@ -3189,16 +3189,17 @@ static const char kPrefFileHeader[] =
   " * To make a manual change to preferences, you can visit the URL "
   "about:config"
   NS_LINEBREAK
   " */"
   NS_LINEBREAK
   NS_LINEBREAK;
 // clang-format on
 
+// Note: if sShutdown is true, sPreferences will be nullptr.
 StaticRefPtr<Preferences> Preferences::sPreferences;
 bool Preferences::sShutdown = false;
 
 // This globally enables or disables OMT pref writing, both sync and async.
 static int32_t sAllowOMTPrefWrite = -1;
 
 // Write the preference data to a file.
 class PreferencesWriter final
@@ -3660,17 +3661,21 @@ Preferences::IsServiceAvailable()
   return !!sPreferences;
 }
 
 /* static */ bool
 Preferences::InitStaticMembers()
 {
   MOZ_ASSERT(NS_IsMainThread() || mozilla::ServoStyleSet::IsInServoTraversal());
 
-  if (!sShutdown && !sPreferences) {
+  if (MOZ_LIKELY(sPreferences)) {
+    return true;
+  }
+
+  if (!sShutdown) {
     MOZ_ASSERT(NS_IsMainThread());
     nsCOMPtr<nsIPrefService> prefService =
       do_GetService(NS_PREFSERVICE_CONTRACTID);
   }
 
   return sPreferences != nullptr;
 }
 
@@ -4811,17 +4816,18 @@ Preferences::AddWeakObserver(nsIObserver
   NS_ENSURE_TRUE(InitStaticMembers(), NS_ERROR_NOT_AVAILABLE);
   return sPreferences->mRootBranch->AddObserver(aPref, aObserver, true);
 }
 
 /* static */ nsresult
 Preferences::RemoveObserver(nsIObserver* aObserver, const char* aPref)
 {
   MOZ_ASSERT(aObserver);
-  if (!sPreferences && sShutdown) {
+  if (sShutdown) {
+    MOZ_ASSERT(!sPreferences);
     return NS_OK; // Observers have been released automatically.
   }
   NS_ENSURE_TRUE(sPreferences, NS_ERROR_NOT_AVAILABLE);
   return sPreferences->mRootBranch->RemoveObserver(aPref, aObserver);
 }
 
 /* static */ nsresult
 Preferences::AddStrongObservers(nsIObserver* aObserver, const char** aPrefs)
@@ -4844,17 +4850,18 @@ Preferences::AddWeakObservers(nsIObserve
   }
   return NS_OK;
 }
 
 /* static */ nsresult
 Preferences::RemoveObservers(nsIObserver* aObserver, const char** aPrefs)
 {
   MOZ_ASSERT(aObserver);
-  if (!sPreferences && sShutdown) {
+  if (sShutdown) {
+    MOZ_ASSERT(!sPreferences);
     return NS_OK; // Observers have been released automatically.
   }
   NS_ENSURE_TRUE(sPreferences, NS_ERROR_NOT_AVAILABLE);
 
   for (uint32_t i = 0; aPrefs[i]; i++) {
     nsresult rv = RemoveObserver(aObserver, aPrefs[i]);
     NS_ENSURE_SUCCESS(rv, rv);
   }
@@ -4907,17 +4914,18 @@ Preferences::RegisterCallbackAndCall(Pre
 
 /* static */ nsresult
 Preferences::UnregisterCallback(PrefChangedFunc aCallback,
                                 const char* aPref,
                                 void* aClosure,
                                 MatchKind aMatchKind)
 {
   MOZ_ASSERT(aCallback);
-  if (!sPreferences && sShutdown) {
+  if (sShutdown) {
+    MOZ_ASSERT(!sPreferences);
     return NS_OK; // Observers have been released automatically.
   }
   NS_ENSURE_TRUE(sPreferences, NS_ERROR_NOT_AVAILABLE);
 
   return PREF_UnregisterCallback(aPref, aCallback, aClosure, aMatchKind);
 }
 
 static void
