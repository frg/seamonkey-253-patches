# HG changeset patch
# User Cameron McCormack <cam@mcc.id.au>
# Date 1511450605 21600
# Node ID a853772ccf73f120e80d3443cafd6d9f0a58400f
# Parent  115c53e64ef7762a79b9a43e235c392dcb94dbf5
servo: Merge #19349 - style: Stop using nsMediaExpression_Range (from heycam:media-feature-range); r=emilio

Servo part of https://bugzilla.mozilla.org/show_bug.cgi?id=1420071, reviewed there by Emilio.

Source-Repo: https://github.com/servo/servo
Source-Revision: 7ac376eed62ae6a7bb16b1115e718f9c9aee3b00

diff --git a/servo/components/style/gecko/media_queries.rs b/servo/components/style/gecko/media_queries.rs
--- a/servo/components/style/gecko/media_queries.rs
+++ b/servo/components/style/gecko/media_queries.rs
@@ -9,18 +9,17 @@ use app_units::Au;
 use context::QuirksMode;
 use cssparser::{CssStringWriter, Parser, RGBA, Token, BasicParseErrorKind};
 use euclid::ScaleFactor;
 use euclid::Size2D;
 use gecko::values::{convert_nscolor_to_rgba, convert_rgba_to_nscolor};
 use gecko_bindings::bindings;
 use gecko_bindings::structs;
 use gecko_bindings::structs::{nsCSSKeyword, nsCSSProps_KTableEntry, nsCSSValue, nsCSSUnit};
-use gecko_bindings::structs::{nsMediaExpression_Range, nsMediaFeature};
-use gecko_bindings::structs::{nsMediaFeature_ValueType, nsMediaFeature_RangeType};
+use gecko_bindings::structs::{nsMediaFeature, nsMediaFeature_ValueType, nsMediaFeature_RangeType};
 use gecko_bindings::structs::{nsPresContext, RawGeckoPresContextOwned};
 use gecko_bindings::structs::nsIAtom;
 use media_queries::MediaType;
 use parser::{Parse, ParserContext};
 use properties::ComputedValues;
 use servo_arc::Arc;
 use std::fmt::{self, Write};
 use std::sync::atomic::{AtomicBool, AtomicIsize, AtomicUsize, Ordering};
@@ -212,38 +211,49 @@ impl Device {
         size.scale_by(self.pres_context().mEffectiveTextZoom)
     }
     /// Un-apply text zoom (see nsStyleFont::UnzoomText).
     pub fn unzoom_text(&self, size: Au) -> Au {
         size.scale_by(1. / self.pres_context().mEffectiveTextZoom)
     }
 }
 
+/// The kind of matching that should be performed on a media feature value.
+#[derive(Clone, Copy, Debug, Eq, PartialEq)]
+pub enum Range {
+    /// At least the specified value.
+    Min,
+    /// At most the specified value.
+    Max,
+    /// Exactly the specified value.
+    Equal,
+}
+
 /// A expression for gecko contains a reference to the media feature, the value
 /// the media query contained, and the range to evaluate.
 #[derive(Clone, Debug)]
 pub struct Expression {
     feature: &'static nsMediaFeature,
     value: Option<MediaExpressionValue>,
-    range: nsMediaExpression_Range
+    range: Range,
 }
 
 impl ToCss for Expression {
     fn to_css<W>(&self, dest: &mut W) -> fmt::Result
         where W: fmt::Write,
     {
         dest.write_str("(")?;
 
         if (self.feature.mReqFlags & structs::nsMediaFeature_RequirementFlags_eHasWebkitPrefix) != 0 {
             dest.write_str("-webkit-")?;
         }
         match self.range {
-            nsMediaExpression_Range::eMin => dest.write_str("min-")?,
-            nsMediaExpression_Range::eMax => dest.write_str("max-")?,
-            nsMediaExpression_Range::eEqual => {},
+            Range::Min => dest.write_str("min-")?,
+            Range::Max => dest.write_str("max-")?,
+            Range::Equal => {},
         }
 
         // NB: CssStringWriter not needed, feature names are under control.
         write!(dest, "{}", Atom::from(unsafe { *self.feature.mName }))?;
 
         if let Some(ref val) = self.value {
             dest.write_str(": ")?;
             val.to_css(dest, self)?;
@@ -548,17 +558,17 @@ fn parse_feature_value<'i, 't>(
     Ok(value)
 }
 
 impl Expression {
     /// Trivially construct a new expression.
     fn new(
         feature: &'static nsMediaFeature,
         value: Option<MediaExpressionValue>,
-        range: nsMediaExpression_Range,
+        range: Range,
     ) -> Self {
         Self { feature, value, range }
     }
 
     /// Parse a media expression of the form:
     ///
     /// ```
     /// (media-feature: media-value)
@@ -603,22 +613,22 @@ impl Expression {
                         flags |= structs::nsMediaFeature_RequirementFlags_eHasWebkitPrefix;
                         if unsafe { structs::StylePrefs_sWebkitDevicePixelRatioEnabled } {
                             flags |= structs::nsMediaFeature_RequirementFlags_eWebkitDevicePixelRatioPrefEnabled;
                         }
                     }
 
                     let range = if starts_with_ignore_ascii_case(feature_name, "min-") {
                         feature_name = &feature_name[4..];
-                        nsMediaExpression_Range::eMin
+                        Range::Min
                     } else if starts_with_ignore_ascii_case(feature_name, "max-") {
                         feature_name = &feature_name[4..];
-                        nsMediaExpression_Range::eMax
+                        Range::Max
                     } else {
-                        nsMediaExpression_Range::eEqual
+                        Range::Equal
                     };
 
                     let atom = Atom::from(feature_name);
                     match find_feature(|f| atom.as_ptr() == unsafe { *f.mName }) {
                         Some(f) => Ok((f, range)),
                         None => Err(()),
                     }
                 };
@@ -636,31 +646,31 @@ impl Expression {
                 }
 
                 if (feature.mReqFlags & !flags) != 0 {
                     return Err(location.new_custom_error(
                         StyleParseErrorKind::MediaQueryExpectedFeatureName(ident.clone())
                     ))
                 }
 
-                if range != nsMediaExpression_Range::eEqual &&
+                if range != Range::Equal &&
                    feature.mRangeType != nsMediaFeature_RangeType::eMinMaxAllowed {
                     return Err(location.new_custom_error(
                         StyleParseErrorKind::MediaQueryExpectedFeatureName(ident.clone())
                     ))
                 }
             }
 
             // If there's no colon, this is a media query of the form
             // '(<feature>)', that is, there's no value specified.
             //
             // Gecko doesn't allow ranged expressions without a value, so just
             // reject them here too.
             if input.try(|i| i.expect_colon()).is_err() {
-                if range != nsMediaExpression_Range::eEqual {
+                if range != Range::Equal {
                     return Err(input.new_custom_error(StyleParseErrorKind::RangedExpressionWithNoValue))
                 }
                 return Ok(Expression::new(feature, None, range));
             }
 
             let value = parse_feature_value(feature,
                                             feature.mValueType,
                                             context, input).map_err(|err|
@@ -692,17 +702,17 @@ impl Expression {
         &self,
         device: &Device,
         actual_value: &MediaExpressionValue,
         quirks_mode: QuirksMode,
     ) -> bool {
         use self::MediaExpressionValue::*;
         use std::cmp::Ordering;
 
-        debug_assert!(self.range == nsMediaExpression_Range::eEqual ||
+        debug_assert!(self.range == Range::Equal ||
                       self.feature.mRangeType == nsMediaFeature_RangeType::eMinMaxAllowed,
                       "Whoops, wrong range");
 
         // http://dev.w3.org/csswg/mediaqueries3/#units
         // em units are relative to the initial font-size.
         let required_value = match self.value {
             Some(ref v) => v,
             None => {
@@ -758,14 +768,14 @@ impl Expression {
             (&Enumerated(one), &Enumerated(other)) => {
                 debug_assert!(self.feature.mRangeType != nsMediaFeature_RangeType::eMinMaxAllowed);
                 return one == other;
             }
             _ => unreachable!(),
         };
 
         cmp == Ordering::Equal || match self.range {
-            nsMediaExpression_Range::eMin => cmp == Ordering::Less,
-            nsMediaExpression_Range::eEqual => false,
-            nsMediaExpression_Range::eMax => cmp == Ordering::Greater,
+            Range::Min => cmp == Ordering::Less,
+            Range::Equal => false,
+            Range::Max => cmp == Ordering::Greater,
         }
     }
 }
