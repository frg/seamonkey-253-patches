# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1514418083 -32400
#      Thu Dec 28 08:41:23 2017 +0900
# Node ID 69f9bc50da95f131f2bae64f97adf2f8f354d37a
# Parent  5fc9e32e0988f6aceb55bb45e3d06478178ac10f
Bug 1427150 - Update cubeb from upstream to 43e15fc. r=kamidphish

diff --git a/media/libcubeb/README_MOZILLA b/media/libcubeb/README_MOZILLA
--- a/media/libcubeb/README_MOZILLA
+++ b/media/libcubeb/README_MOZILLA
@@ -1,8 +1,8 @@
 The source from this directory was copied from the cubeb 
 git repository using the update.sh script.  The only changes
 made were those applied by update.sh and the addition of
 Makefile.in build files for the Mozilla build system.
 
 The cubeb git repository is: git://github.com/kinetiknz/cubeb.git
 
-The git commit ID used was e1e8337c43d2c99ba88e9b7fc2da82238c8fb4b4 (2017-12-21 21:11:57 +0200)
+The git commit ID used was 43e15fc92293fc842ecd8a109fbe039c4f3c0c13 (2017-12-28 09:32:11 +1000)
diff --git a/media/libcubeb/src/cubeb_pulse.c b/media/libcubeb/src/cubeb_pulse.c
--- a/media/libcubeb/src/cubeb_pulse.c
+++ b/media/libcubeb/src/cubeb_pulse.c
@@ -79,30 +79,41 @@
   X(pa_stream_writable_size)                    \
   X(pa_stream_peek)                             \
   X(pa_stream_drop)                             \
   X(pa_stream_get_buffer_attr)                  \
   X(pa_stream_get_device_name)                  \
   X(pa_context_set_subscribe_callback)          \
   X(pa_context_subscribe)                       \
   X(pa_mainloop_api_once)                       \
+  X(pa_get_library_version)                     \
 
 #define MAKE_TYPEDEF(x) static typeof(x) * cubeb_##x;
 LIBPULSE_API_VISIT(MAKE_TYPEDEF);
 #undef MAKE_TYPEDEF
 #endif
 
+#if PA_CHECK_VERSION(2, 0, 0)
+static int has_pulse_v2 = 0;
+#endif
+
 static struct cubeb_ops const pulse_ops;
 
+struct cubeb_default_sink_info {
+  pa_channel_map channel_map;
+  uint32_t sample_spec_rate;
+  pa_sink_flags_t flags;
+};
+
 struct cubeb {
   struct cubeb_ops const * ops;
   void * libpulse;
   pa_threaded_mainloop * mainloop;
   pa_context * context;
-  pa_sink_info * default_sink_info;
+  struct cubeb_default_sink_info * default_sink_info;
   char * context_name;
   int error;
   cubeb_device_collection_changed_callback collection_changed_callback;
   void * collection_changed_user_ptr;
   cubeb_strings * device_ids;
 };
 
 struct cubeb_stream {
@@ -148,18 +159,20 @@ intern_device_id(cubeb * ctx, char const
 
 static void
 sink_info_callback(pa_context * context, const pa_sink_info * info, int eol, void * u)
 {
   (void)context;
   cubeb * ctx = u;
   if (!eol) {
     free(ctx->default_sink_info);
-    ctx->default_sink_info = malloc(sizeof(pa_sink_info));
-    memcpy(ctx->default_sink_info, info, sizeof(pa_sink_info));
+    ctx->default_sink_info = malloc(sizeof(struct cubeb_default_sink_info));
+    memcpy(&ctx->default_sink_info->channel_map, &info->channel_map, sizeof(pa_channel_map));
+    ctx->default_sink_info->sample_spec_rate = info->sample_spec.rate;
+    ctx->default_sink_info->flags = info->flags;
   }
   WRAP(pa_threaded_mainloop_signal)(ctx->mainloop, 0);
 }
 
 static void
 server_info_callback(pa_context * context, const pa_server_info * info, void * u)
 {
   pa_operation * o;
@@ -617,16 +630,21 @@ pulse_init(cubeb ** context, char const 
       return CUBEB_ERROR;                       \
     }                                           \
   }
 
   LIBPULSE_API_VISIT(LOAD);
 #undef LOAD
 #endif
 
+#if PA_CHECK_VERSION(2, 0, 0)
+  const char* version = WRAP(pa_get_library_version)();
+  has_pulse_v2 = strtol(version, NULL, 10) >= 2;
+#endif
+
   ctx = calloc(1, sizeof(*ctx));
   assert(ctx);
 
   ctx->ops = &pulse_ops;
   ctx->libpulse = libpulse;
   if (cubeb_strings_init(&ctx->device_ids) != CUBEB_OK) {
     pulse_destroy(ctx);
     return CUBEB_ERROR;
@@ -684,17 +702,17 @@ static int
 pulse_get_preferred_sample_rate(cubeb * ctx, uint32_t * rate)
 {
   assert(ctx && rate);
   (void)ctx;
 
   if (!ctx->default_sink_info)
     return CUBEB_ERROR;
 
-  *rate = ctx->default_sink_info->sample_spec.rate;
+  *rate = ctx->default_sink_info->sample_spec_rate;
 
   return CUBEB_OK;
 }
 
 static int
 pulse_get_preferred_channel_layout(cubeb * ctx, cubeb_channel_layout * layout)
 {
   assert(ctx && layout);
@@ -1220,17 +1238,17 @@ pulse_ensure_dev_list_data_list_size (pu
   }
 }
 
 static cubeb_device_state
 pulse_get_state_from_sink_port(pa_sink_port_info * info)
 {
   if (info != NULL) {
 #if PA_CHECK_VERSION(2, 0, 0)
-    if (info->available == PA_PORT_AVAILABLE_NO)
+    if (has_pulse_v2 && info->available == PA_PORT_AVAILABLE_NO)
       return CUBEB_DEVICE_STATE_UNPLUGGED;
     else /*if (info->available == PA_PORT_AVAILABLE_YES) + UNKNOWN */
 #endif
       return CUBEB_DEVICE_STATE_ENABLED;
   }
 
   return CUBEB_DEVICE_STATE_ENABLED;
 }
@@ -1292,17 +1310,17 @@ pulse_sink_info_cb(pa_context * context,
   list_data->count += 1;
 }
 
 static cubeb_device_state
 pulse_get_state_from_source_port(pa_source_port_info * info)
 {
   if (info != NULL) {
 #if PA_CHECK_VERSION(2, 0, 0)
-    if (info->available == PA_PORT_AVAILABLE_NO)
+    if (has_pulse_v2 && info->available == PA_PORT_AVAILABLE_NO)
       return CUBEB_DEVICE_STATE_UNPLUGGED;
     else /*if (info->available == PA_PORT_AVAILABLE_YES) + UNKNOWN */
 #endif
       return CUBEB_DEVICE_STATE_ENABLED;
   }
 
   return CUBEB_DEVICE_STATE_ENABLED;
 }
