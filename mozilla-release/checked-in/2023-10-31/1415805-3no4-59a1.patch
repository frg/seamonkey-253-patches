# HG changeset patch
# User bechen@mozilla.com <bechen@mozilla.com>
# Date 1511338211 -28800
#      Wed Nov 22 16:10:11 2017 +0800
# Node ID 0affcb3cdc5486e7bfb63a75abea17b330a2c987
# Parent  a01834548568172611f154b8090bff7b0fe0f996
Bug 1415805 - region.scroll setter should not throw. r=smaug

MozReview-Commit-ID: FU9YBBeLT5B

diff --git a/dom/media/TextTrackRegion.cpp b/dom/media/TextTrackRegion.cpp
--- a/dom/media/TextTrackRegion.cpp
+++ b/dom/media/TextTrackRegion.cpp
@@ -1,16 +1,15 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 et tw=78: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/dom/TextTrackRegion.h"
-#include "mozilla/dom/VTTRegionBinding.h"
 
 namespace mozilla {
 namespace dom {
 
 NS_IMPL_CYCLE_COLLECTION_WRAPPERCACHE(TextTrackRegion, mParent)
 NS_IMPL_CYCLE_COLLECTING_ADDREF(TextTrackRegion)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(TextTrackRegion)
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(TextTrackRegion)
@@ -40,16 +39,17 @@ TextTrackRegion::Constructor(const Globa
 TextTrackRegion::TextTrackRegion(nsISupports* aGlobal)
   : mParent(aGlobal)
   , mWidth(100)
   , mLines(3)
   , mRegionAnchorX(0)
   , mRegionAnchorY(100)
   , mViewportAnchorX(0)
   , mViewportAnchorY(100)
+  , mScroll(ScrollSetting::_empty)
 {
 }
 
 void
 TextTrackRegion::CopyValues(TextTrackRegion& aRegion)
 {
   mId = aRegion.Id();
   mWidth = aRegion.Width();
diff --git a/dom/media/TextTrackRegion.h b/dom/media/TextTrackRegion.h
--- a/dom/media/TextTrackRegion.h
+++ b/dom/media/TextTrackRegion.h
@@ -7,16 +7,17 @@
 #ifndef mozilla_dom_TextTrackRegion_h
 #define mozilla_dom_TextTrackRegion_h
 
 #include "nsCycleCollectionParticipant.h"
 #include "nsString.h"
 #include "nsWrapperCache.h"
 #include "mozilla/ErrorResult.h"
 #include "mozilla/dom/TextTrack.h"
+#include "mozilla/dom/VTTRegionBinding.h"
 #include "mozilla/Preferences.h"
 
 namespace mozilla {
 namespace dom {
 
 class GlobalObject;
 class TextTrack;
 
@@ -111,29 +112,26 @@ public:
 
   void SetViewportAnchorY(double aVal, ErrorResult& aRv)
   {
     if (!InvalidValue(aVal, aRv)) {
       mViewportAnchorY = aVal;
     }
   }
 
-  void GetScroll(nsAString& aScroll) const
+  ScrollSetting Scroll() const
   {
-    aScroll = mScroll;
+    return mScroll;
   }
 
-  void SetScroll(const nsAString& aScroll, ErrorResult& aRv)
+  void SetScroll(const ScrollSetting& aScroll)
   {
-    if (!aScroll.EqualsLiteral("") && !aScroll.EqualsLiteral("up")) {
-      aRv.Throw(NS_ERROR_DOM_SYNTAX_ERR);
-      return;
+    if (aScroll == ScrollSetting::_empty || aScroll == ScrollSetting::Up) {
+      mScroll = aScroll;
     }
-
-    mScroll = aScroll;
   }
 
   void GetId(nsAString& aId) const
   {
     aId = mId;
   }
 
   void SetId(const nsAString& aId)
@@ -144,37 +142,33 @@ public:
   /** end WebIDL Methods. */
 
 
   // Helper to aid copying of a given TextTrackRegion's width, lines,
   // anchor, viewport and scroll values.
   void CopyValues(TextTrackRegion& aRegion);
 
   // -----helpers-------
-  const nsAString& Scroll() const
-  {
-    return mScroll;
-  }
   const nsAString& Id() const
   {
     return mId;
   }
 
 private:
   ~TextTrackRegion() {}
 
   nsCOMPtr<nsISupports> mParent;
   nsString mId;
   double mWidth;
   long mLines;
   double mRegionAnchorX;
   double mRegionAnchorY;
   double mViewportAnchorX;
   double mViewportAnchorY;
-  nsString mScroll;
+  ScrollSetting mScroll;
 
   // Helper to ensure new value is in the range: 0.0% - 100.0%; throws
   // an IndexSizeError otherwise.
   inline bool InvalidValue(double aValue, ErrorResult& aRv)
   {
     if(aValue < 0.0  || aValue > 100.0) {
       aRv.Throw(NS_ERROR_DOM_INDEX_SIZE_ERR);
       return true;
diff --git a/dom/webidl/VTTRegion.webidl b/dom/webidl/VTTRegion.webidl
--- a/dom/webidl/VTTRegion.webidl
+++ b/dom/webidl/VTTRegion.webidl
@@ -1,27 +1,32 @@
 /* -*- Mode: IDL; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/.
  *
  * The origin of this IDL file is
- *  http://dev.w3.org/html5/webvtt/#extension-of-the-texttrack-interface-for-region-support
+ * https://w3c.github.io/webvtt/#the-vttregion-interface
  */
 
+enum ScrollSetting {
+  "",
+  "up"
+};
+
 [Constructor, Pref="media.webvtt.regions.enabled"]
 interface VTTRegion {
            attribute DOMString id;
            [SetterThrows]
            attribute double width;
            [SetterThrows]
            attribute long lines;
            [SetterThrows]
            attribute double regionAnchorX;
            [SetterThrows]
            attribute double regionAnchorY;
            [SetterThrows]
            attribute double viewportAnchorX;
            [SetterThrows]
            attribute double viewportAnchorY;
-           [SetterThrows]
-           attribute DOMString scroll;
+
+           attribute ScrollSetting scroll;
 };
