# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1512349723 -39600
# Node ID 0f87d8c908c39aed95ce68bc021e168fc69ebca2
# Parent  05894fa0207009baf315dbf12d2f7e07df59a6eb
Bug 1422649 - Remove a bogus exception from the early pref access checking. r=billm

New content processes get prefs in three ways.

- They read them from greprefs.js, prefs.js and other such files.

- They get sent "early prefs" from the parent process via the command line
  (-intPrefs/-boolPrefs/-stringPrefs).

- They get sent "late prefs" from the parent process via IPC message.

(The latter two are necessary for communicating prefs that have been added or
modified in the parent process since the file reading occurred at startup.)

We have some machinery that detects if a late pref is accessed before the late
prefs are set, which is good. But it has a big exception in it; late pref
accesses that occur early via Add*VarCache() and RegisterCallbackAndCall() are
allowed.

This exception was added in bug 1341414. The description of that bug says "We
should change AddBoolVarCache so that it doesn't look at the pref in the
content process until prefs have been received from the parent." Unfortunately,
the patch in that bug added the exception to the checking without changing
Add*VarCache() in the suggested way!

This means it's possible for late prefs to be read early via VarCaches (or
RegisterCallbackAndCall()) when their values are incorrect, which is bad.

Changing Add*VarCache() to delay the reading as bug 1341414 originally
suggested seems difficult. A simpler fix is to just remove the exception in the
checking and extend the early prefs list as necessary. This patch does that,
lengthening the early prefs list from ~210 to ~300. Fortunately, most (all?) of
the added prefs are ints or bools rather than strings, so it doesn't increase
the size of the command line arguments for content processes by too much.

diff --git a/dom/ipc/ContentPrefs.cpp b/dom/ipc/ContentPrefs.cpp
--- a/dom/ipc/ContentPrefs.cpp
+++ b/dom/ipc/ContentPrefs.cpp
@@ -18,16 +18,17 @@
  *
  ******************************************************************************/
 
 const char* mozilla::dom::ContentPrefs::gEarlyPrefs[] = {
   "accessibility.monoaudio.enable",
   "accessibility.mouse_focuses_formcontrol",
   "accessibility.tabfocus_applies_to_xul",
   "app.update.channel",
+  "browser.autofocus",
   "browser.dom.window.dump.enabled",
   "browser.sessionhistory.max_entries",
   "browser.sessionhistory.max_total_viewers",
 #if defined(NIGHTLY_BUILD) || defined(DEBUG)
   "browser.startup.record",
 #endif
   "content.cors.disable",
   "content.cors.no_private_data",
@@ -40,33 +41,44 @@ const char* mozilla::dom::ContentPrefs::
   "content.sink.interactive_deflect_count",
   "content.sink.interactive_parse_time",
   "content.sink.interactive_time",
   "content.sink.pending_event_mode",
   "content.sink.perf_deflect_count",
   "content.sink.perf_parse_time",
   "device.storage.prompt.testing",
   "device.storage.writable.name",
+  "devtools.enabled",
   "dom.allow_XUL_XBL_for_file",
   "dom.allow_cut_copy",
+  "dom.animations-api.core.enabled",
+  "dom.animations-api.element-animate.enabled",
+  "dom.animations-api.pending-member.enabled",
   "dom.enable_frame_timing",
   "dom.enable_performance",
+  "dom.enable_performance_navigation_timing",
   "dom.enable_resource_timing",
   "dom.event.handling-user-input-time-limit",
   "dom.event.touch.coalescing.enabled",
   "dom.forms.autocomplete.formautofill",
+  "dom.forms.inputmode",
+  "dom.input.skip_cursor_move_for_same_value_set",
   "dom.ipc.processPriorityManager.backgroundGracePeriodMS",
   "dom.ipc.processPriorityManager.backgroundPerceivableGracePeriodMS",
   "dom.ipc.useNativeEventProcessing.content",
   "dom.max_chrome_script_run_time",
   "dom.max_ext_content_script_run_time",
   "dom.max_script_run_time",
   "dom.mozBrowserFramesEnabled",
   "dom.performance.enable_notify_performance_timing",
   "dom.performance.enable_user_timing_logging",
+  "dom.placeholder.show_on_focus",
+  "dom.requestIdleCallback.enabled",
+  "dom.script_loader.bytecode_cache.enabled",
+  "dom.script_loader.bytecode_cache.strategy",
   "dom.storage.testing",
   "dom.url.encode_decode_hash",
   "dom.url.getters_decode_hash",
   "dom.use_watchdog",
   "dom.vibrator.enabled",
   "dom.vibrator.max_vibrate_list_len",
   "dom.vibrator.max_vibrate_ms",
   "dom.webcomponents.customelements.enabled",
@@ -74,28 +86,30 @@ const char* mozilla::dom::ContentPrefs::
   "focusmanager.testmode",
   "font.size.inflation.disabledInMasterProcess",
   "font.size.inflation.emPerLine",
   "font.size.inflation.forceEnabled",
   "font.size.inflation.lineThreshold",
   "font.size.inflation.mappingIntercept",
   "font.size.inflation.maxRatio",
   "font.size.inflation.minTwips",
+  "font.size.systemFontScale",
   "full-screen-api.allow-trusted-requests-only",
   "full-screen-api.enabled",
   "full-screen-api.unprefix.enabled",
 #ifdef FUZZING
   "fuzzing.enabled",
 #endif
   "gfx.font_rendering.opentype_svg.enabled",
   "hangmonitor.timeout",
   "html5.flushtimer.initialdelay",
   "html5.flushtimer.subsequentdelay",
   "html5.offmainthread",
   "intl.charset.fallback.tld",
+  "intl.charset.fallback.utf8_for_file",
   "intl.ime.hack.on_ime_unaware_apps.fire_key_events_for_composition",
   "javascript.enabled",
   "javascript.options.array_prototype_values",
   "javascript.options.asmjs",
   "javascript.options.asyncstack",
   "javascript.options.baselinejit",
   "javascript.options.baselinejit.threshold",
   "javascript.options.baselinejit.unsafe_eager_compilation",
@@ -119,54 +133,120 @@ const char* mozilla::dom::ContentPrefs::
   "javascript.options.throw_on_debuggee_would_run",
   "javascript.options.unboxed_objects",
   "javascript.options.wasm",
   "javascript.options.wasm_baselinejit",
   "javascript.options.wasm_ionjit",
   "javascript.options.werror",
   "javascript.use_us_english_locale",
   "jsloader.shareGlobal",
+  "layout.css.all-shorthand.enabled",
+  "layout.css.background-blend-mode.enabled",
+  "layout.css.box-decoration-break.enabled",
+  "layout.css.color-adjust.enabled",
+  "layout.css.column-span.enabled",
+  "layout.css.contain.enabled",
+  "layout.css.control-characters.visible",
+  "layout.css.expensive-style-struct-assertions.enabled",
+  "layout.css.float-logical-values.enabled",
+  "layout.css.font-display.enabled",
+  "layout.css.font-variations.enabled",
+  "layout.css.frames-timing.enabled",
+  "layout.css.getBoxQuads.enabled",
+  "layout.css.grid-template-subgrid-value.enabled",
+  "layout.css.grid.enabled",
+  "layout.css.image-orientation.enabled",
+  "layout.css.initial-letter.enabled",
+  "layout.css.isolation.enabled",
+  "layout.css.mix-blend-mode.enabled",
+  "layout.css.moz-document.content.enabled",
+  "layout.css.osx-font-smoothing.enabled",
+  "layout.css.overflow-clip-box.enabled",
+  "layout.css.overscroll-behavior.enabled",
+  "layout.css.prefixes.animations",
+  "layout.css.prefixes.border-image",
+  "layout.css.prefixes.box-sizing",
+  "layout.css.prefixes.device-pixel-ratio-webkit",
+  "layout.css.prefixes.font-features",
+  "layout.css.prefixes.gradients",
+  "layout.css.prefixes.transforms",
+  "layout.css.prefixes.transitions",
+  "layout.css.prefixes.webkit",
+  "layout.css.scope-pseudo.enabled",
+  "layout.css.scoped-style.enabled",
+  "layout.css.scroll-behavior.property-enabled",
+  "layout.css.scroll-snap.enabled",
 #ifdef MOZ_STYLO
   "layout.css.servo.chrome.enabled",
+  "layout.css.servo.enabled",
+#endif
+  "layout.css.shape-outside.enabled",
+#ifdef MOZ_STYLO
   "layout.css.stylo-blocklist.blocked_domains",
   "layout.css.stylo-blocklist.enabled",
 #endif
+  "layout.css.text-align-unsafe-value.enabled",
+  "layout.css.text-combine-upright-digits.enabled",
+  "layout.css.text-combine-upright.enabled",
+  "layout.css.text-justify.enabled",
+  "layout.css.touch_action.enabled",
+  "layout.css.visited_links_enabled",
   "layout.idle_period.required_quiescent_frames",
   "layout.idle_period.time_limit",
   "layout.interruptible-reflow.enabled",
   "mathml.disabled",
+  "media.audio-max-decode-error",
+  "media.cache_readahead_limit",
+  "media.cache_resume_threshold",
+  "media.cache_size",
   "media.clearkey.persistent-license.enabled",
   "media.cubeb.backend",
   "media.cubeb.sandbox",
   "media.cubeb_latency_msg_frames",
   "media.cubeb_latency_playback_ms",
   "media.decoder-doctor.wmf-disabled-is-failure",
   "media.decoder.recycle.enabled",
+  "media.decoder.skip-to-next-key-frame.enabled",
   "media.dormant-on-pause-timeout-ms",
   "media.eme.audio.blank",
+  "media.eme.chromium-api.video-shmems",
   "media.eme.enabled",
   "media.eme.video.blank",
   "media.ffmpeg.enabled",
+  "media.ffmpeg.low-latency.enabled",
   "media.ffvpx.enabled",
   "media.ffvpx.low-latency.enabled",
   "media.flac.enabled",
   "media.forcestereo.enabled",
   "media.gmp.decoder.enabled",
   "media.gmp.insecure.allow",
   "media.gpu-process-decoder",
+  "media.hls.enabled",
   "media.libavcodec.allow-obsolete",
+  "media.memory_cache_max_size",
+  "media.memory_caches_combined_limit_kb",
+  "media.memory_caches_combined_limit_pc_sysmem",
+  "media.mp4.enabled",
+  "media.navigator.mediadatadecoder_enabled",
   "media.ogg.enabled",
   "media.ogg.flac.enabled",
+  "media.playback.warnings-as-errors",
+  "media.playback.warnings-as-errors.stagefright-vs-rust",
   "media.resampling.enabled",
+  "media.resume-bkgnd-video-on-tabhover",
   "media.ruin-av-sync.enabled",
+  "media.rust.mp4parser",
   "media.rust.test_mode",
+  "media.seamless-looping",
   "media.suspend-bkgnd-video.delay-ms",
   "media.suspend-bkgnd-video.enabled",
   "media.use-blank-decoder",
+  "media.video-max-decode-error",
   "media.video_stats.enabled",
+  "media.videocontrols.lock-video-orientation",
   "media.volume_scale",
   "media.webspeech.recognition.enable",
   "media.webspeech.recognition.force_enable",
   "media.webspeech.synth.force_global_queue",
   "media.webspeech.test.enable",
   "media.webspeech.test.fake_fsm_events",
   "media.webspeech.test.fake_recognition_service",
   "media.wmf.allow-unsupported-resolutions",
@@ -180,40 +260,56 @@ const char* mozilla::dom::ContentPrefs::
   "network.buffer.cache.count",
   "network.buffer.cache.size",
   "network.captive-portal-service.enabled",
   "network.cookie.cookieBehavior",
   "network.cookie.lifetimePolicy",
   "network.dns.disablePrefetch",
   "network.dns.disablePrefetchFromHTTPS",
   "network.file.disable_unc_paths",
+  "network.http.tailing.enabled",
   "network.jar.block-remote-files",
   "network.loadinfo.skip_type_assertion",
   "network.notify.changed",
   "network.offline-mirrors-connectivity",
   "network.protocol-handler.external.jar",
   "network.proxy.type",
   "network.security.ports.banned",
   "network.security.ports.banned.override",
   "network.standard-url.enable-rust",
   "network.standard-url.max-length",
+  "network.standard-url.punycode-host",
   "network.sts.max_time_for_events_between_two_polls",
   "network.sts.max_time_for_pr_close_during_shutdown",
   "network.tcp.keepalive.enabled",
   "network.tcp.keepalive.idle_time",
   "network.tcp.keepalive.probe_count",
   "network.tcp.keepalive.retry_interval",
   "network.tcp.sendbuffer",
   "nglayout.debug.invalidation",
   "privacy.donottrackheader.enabled",
   "privacy.firstparty.isolate",
   "privacy.firstparty.isolate.restrict_opener_access",
   "privacy.resistFingerprinting",
+  "privacy.resistFingerprinting.target_video_res",
+  "privacy.resistFingerprinting.video_dropped_ratio",
+  "privacy.resistFingerprinting.video_frames_per_sec",
+  "privacy.trackingprotection.lower_network_priority",
+  "privacy.window.maxInnerHeight",
+  "privacy.window.maxInnerWidth",
+  "security.csp.enable",
+  "security.data_uri.block_toplevel_data_uri_navigations",
   "security.data_uri.unique_opaque_origin",
   "security.fileuri.strict_origin_policy",
+  "security.mixed_content.block_active_content",
+  "security.mixed_content.block_display_content",
+  "security.mixed_content.block_object_subrequest",
+  "security.mixed_content.hsts_priming_cache_timeout",
+  "security.mixed_content.send_hsts_priming",
+  "security.mixed_content.use_hsts",
   "security.sandbox.content.level",
   "security.sandbox.content.tempDirSuffix",
   "security.sandbox.logging.enabled",
   "security.sandbox.mac.track.violations",
   "security.sandbox.windows.log.stackTraceDepth",
   "svg.disabled",
   "svg.display-lists.hit-testing.enabled",
   "svg.display-lists.painting.enabled",
@@ -232,17 +328,20 @@ const char* mozilla::dom::ContentPrefs::
   "toolkit.telemetry.log.level",
   "toolkit.telemetry.minSubsessionLength",
   "toolkit.telemetry.scheduler.idleTickInterval",
   "toolkit.telemetry.scheduler.tickInterval",
   "toolkit.telemetry.unified",
   "ui.key.menuAccessKeyFocuses",
   "ui.popup.disable_autohide",
   "ui.use_activity_cursor",
-  "view_source.editor.external"};
+  "view_source.editor.external",
+  "zoom.maxPercent",
+  "zoom.minPercent"
+};
 
 const char** mozilla::dom::ContentPrefs::GetEarlyPrefs(size_t* aCount)
 {
   *aCount = ArrayLength(ContentPrefs::gEarlyPrefs);
   return gEarlyPrefs;
 }
 
 const char* mozilla::dom::ContentPrefs::GetEarlyPref(size_t aIndex)
diff --git a/modules/libpref/Preferences.cpp b/modules/libpref/Preferences.cpp
--- a/modules/libpref/Preferences.cpp
+++ b/modules/libpref/Preferences.cpp
@@ -763,55 +763,35 @@ static bool
 IsEarlyPref(const char* aPrefName)
 {
   size_t prefsLen;
   size_t found;
   const char** list = mozilla::dom::ContentPrefs::GetEarlyPrefs(&prefsLen);
   return BinarySearchIf(list, 0, prefsLen, StringComparator(aPrefName), &found);
 }
 
-static bool gInstallingCallback = false;
-
-class AutoInstallingCallback
-{
-public:
-  AutoInstallingCallback() { gInstallingCallback = true; }
-  ~AutoInstallingCallback() { gInstallingCallback = false; }
-};
-
-#define AUTO_INSTALLING_CALLBACK() AutoInstallingCallback installingRAII
-
-#else // DEBUG
-
-#define AUTO_INSTALLING_CALLBACK()
-
 #endif // DEBUG
 
 static Pref*
 pref_HashTableLookup(const char* aPrefName)
 {
   MOZ_ASSERT(NS_IsMainThread() || mozilla::ServoStyleSet::IsInServoTraversal());
 
 #ifdef DEBUG
   if (!XRE_IsParentProcess()) {
     if (gPhase == ContentProcessPhase::eNoPrefsSet) {
       MOZ_CRASH_UNSAFE_PRINTF(
         "accessing pref %s before early prefs are set", aPrefName);
     }
 
-    if (gPhase == ContentProcessPhase::eEarlyPrefsSet && !gInstallingCallback &&
+    if (gPhase == ContentProcessPhase::eEarlyPrefsSet &&
         !IsEarlyPref(aPrefName)) {
       // If you hit this crash, you have an early access of a non-early pref.
       // Consider moving the access later or add the pref to the whitelist of
       // early prefs in ContentPrefs.cpp and get review from a DOM peer.
-      //
-      // Note that accesses of non-early prefs that happen while
-      // installing a callback (e.g. VarCache) are considered acceptable. These
-      // accesses will fail, but once the proper pref value is set the callback
-      // will be immediately called, so things should work out.
       MOZ_CRASH_UNSAFE_PRINTF(
         "accessing non-early pref %s before late prefs are set", aPrefName);
     }
   }
 #endif
 
   return static_cast<Pref*>(gHashTable->Search(aPrefName));
 }
@@ -3473,35 +3453,37 @@ NS_INTERFACE_MAP_BEGIN(Preferences)
   NS_INTERFACE_MAP_ENTRY(nsISupportsWeakReference)
 NS_INTERFACE_MAP_END
 
 /* static */ void
 Preferences::SetEarlyPreferences(const nsTArray<dom::Pref>* aDomPrefs)
 {
   MOZ_ASSERT(!XRE_IsParentProcess());
 
+  gEarlyDomPrefs = new InfallibleTArray<dom::Pref>(mozilla::Move(*aDomPrefs));
+
 #ifdef DEBUG
   MOZ_ASSERT(gPhase == ContentProcessPhase::eNoPrefsSet);
   gPhase = ContentProcessPhase::eEarlyPrefsSet;
 #endif
-  gEarlyDomPrefs = new InfallibleTArray<dom::Pref>(mozilla::Move(*aDomPrefs));
 }
 
 /* static */ void
 Preferences::SetLatePreferences(const nsTArray<dom::Pref>* aDomPrefs)
 {
   MOZ_ASSERT(!XRE_IsParentProcess());
 
+  for (unsigned int i = 0; i < aDomPrefs->Length(); i++) {
+    Preferences::SetPreference(aDomPrefs->ElementAt(i));
+  }
+
 #ifdef DEBUG
   MOZ_ASSERT(gPhase == ContentProcessPhase::eEarlyPrefsSet);
   gPhase = ContentProcessPhase::eEarlyAndLatePrefsSet;
 #endif
-  for (unsigned int i = 0; i < aDomPrefs->Length(); i++) {
-    Preferences::SetPreference(aDomPrefs->ElementAt(i));
-  }
 }
 
 /* static */ void
 Preferences::InitializeUserPrefs()
 {
   MOZ_ASSERT(XRE_IsParentProcess());
   MOZ_ASSERT(!sPreferences->mCurrentFile, "Should only initialize prefs once");
 
@@ -4793,17 +4775,16 @@ Preferences::RegisterCallback(PrefChange
 Preferences::RegisterCallbackAndCall(PrefChangedFunc aCallback,
                                      const char* aPref,
                                      void* aClosure,
                                      MatchKind aMatchKind)
 {
   MOZ_ASSERT(aCallback);
   nsresult rv = RegisterCallback(aCallback, aPref, aClosure, aMatchKind);
   if (NS_SUCCEEDED(rv)) {
-    AUTO_INSTALLING_CALLBACK();
     (*aCallback)(aPref, aClosure);
   }
   return rv;
 }
 
 /* static */ nsresult
 Preferences::UnregisterCallback(PrefChangedFunc aCallback,
                                 const char* aPrefNode,
@@ -4863,20 +4844,17 @@ BoolVarChanged(const char* aPref, void* 
 
 /* static */ nsresult
 Preferences::AddBoolVarCache(bool* aCache, const char* aPref, bool aDefault)
 {
   NS_ASSERTION(aCache, "aCache must not be NULL");
 #ifdef DEBUG
   AssertNotAlreadyCached("bool", aPref, aCache);
 #endif
-  {
-    AUTO_INSTALLING_CALLBACK();
-    *aCache = GetBool(aPref, aDefault);
-  }
+  *aCache = GetBool(aPref, aDefault);
   CacheData* data = new CacheData();
   data->mCacheLocation = aCache;
   data->mDefaultValueBool = aDefault;
   CacheDataAppendElement(data);
   Preferences::RegisterCallback(BoolVarChanged,
                                 aPref,
                                 data,
                                 Preferences::ExactMatch,
@@ -4898,20 +4876,17 @@ template<MemoryOrdering Order>
 Preferences::AddAtomicBoolVarCache(Atomic<bool, Order>* aCache,
                                    const char* aPref,
                                    bool aDefault)
 {
   NS_ASSERTION(aCache, "aCache must not be NULL");
 #ifdef DEBUG
   AssertNotAlreadyCached("bool", aPref, aCache);
 #endif
-  {
-    AUTO_INSTALLING_CALLBACK();
-    *aCache = Preferences::GetBool(aPref, aDefault);
-  }
+  *aCache = Preferences::GetBool(aPref, aDefault);
   CacheData* data = new CacheData();
   data->mCacheLocation = aCache;
   data->mDefaultValueBool = aDefault;
   CacheDataAppendElement(data);
   Preferences::RegisterCallback(AtomicBoolVarChanged<Order>,
                                 aPref,
                                 data,
                                 Preferences::ExactMatch,
@@ -4931,20 +4906,17 @@ IntVarChanged(const char* aPref, void* a
 Preferences::AddIntVarCache(int32_t* aCache,
                             const char* aPref,
                             int32_t aDefault)
 {
   NS_ASSERTION(aCache, "aCache must not be NULL");
 #ifdef DEBUG
   AssertNotAlreadyCached("int", aPref, aCache);
 #endif
-  {
-    AUTO_INSTALLING_CALLBACK();
-    *aCache = Preferences::GetInt(aPref, aDefault);
-  }
+  *aCache = Preferences::GetInt(aPref, aDefault);
   CacheData* data = new CacheData();
   data->mCacheLocation = aCache;
   data->mDefaultValueInt = aDefault;
   CacheDataAppendElement(data);
   Preferences::RegisterCallback(
     IntVarChanged, aPref, data, Preferences::ExactMatch, /* isPriority */ true);
   return NS_OK;
 }
@@ -4963,20 +4935,17 @@ template<MemoryOrdering Order>
 Preferences::AddAtomicIntVarCache(Atomic<int32_t, Order>* aCache,
                                   const char* aPref,
                                   int32_t aDefault)
 {
   NS_ASSERTION(aCache, "aCache must not be NULL");
 #ifdef DEBUG
   AssertNotAlreadyCached("int", aPref, aCache);
 #endif
-  {
-    AUTO_INSTALLING_CALLBACK();
-    *aCache = Preferences::GetInt(aPref, aDefault);
-  }
+  *aCache = Preferences::GetInt(aPref, aDefault);
   CacheData* data = new CacheData();
   data->mCacheLocation = aCache;
   data->mDefaultValueUint = aDefault;
   CacheDataAppendElement(data);
   Preferences::RegisterCallback(AtomicIntVarChanged<Order>,
                                 aPref,
                                 data,
                                 Preferences::ExactMatch,
@@ -4996,20 +4965,17 @@ UintVarChanged(const char* aPref, void* 
 Preferences::AddUintVarCache(uint32_t* aCache,
                              const char* aPref,
                              uint32_t aDefault)
 {
   NS_ASSERTION(aCache, "aCache must not be NULL");
 #ifdef DEBUG
   AssertNotAlreadyCached("uint", aPref, aCache);
 #endif
-  {
-    AUTO_INSTALLING_CALLBACK();
-    *aCache = Preferences::GetUint(aPref, aDefault);
-  }
+  *aCache = Preferences::GetUint(aPref, aDefault);
   CacheData* data = new CacheData();
   data->mCacheLocation = aCache;
   data->mDefaultValueUint = aDefault;
   CacheDataAppendElement(data);
   Preferences::RegisterCallback(UintVarChanged,
                                 aPref,
                                 data,
                                 Preferences::ExactMatch,
@@ -5031,20 +4997,17 @@ template<MemoryOrdering Order>
 Preferences::AddAtomicUintVarCache(Atomic<uint32_t, Order>* aCache,
                                    const char* aPref,
                                    uint32_t aDefault)
 {
   NS_ASSERTION(aCache, "aCache must not be NULL");
 #ifdef DEBUG
   AssertNotAlreadyCached("uint", aPref, aCache);
 #endif
-  {
-    AUTO_INSTALLING_CALLBACK();
-    *aCache = Preferences::GetUint(aPref, aDefault);
-  }
+  *aCache = Preferences::GetUint(aPref, aDefault);
   CacheData* data = new CacheData();
   data->mCacheLocation = aCache;
   data->mDefaultValueUint = aDefault;
   CacheDataAppendElement(data);
   Preferences::RegisterCallback(AtomicUintVarChanged<Order>,
                                 aPref,
                                 data,
                                 Preferences::ExactMatch,
@@ -5078,20 +5041,17 @@ FloatVarChanged(const char* aPref, void*
 
 /* static */ nsresult
 Preferences::AddFloatVarCache(float* aCache, const char* aPref, float aDefault)
 {
   NS_ASSERTION(aCache, "aCache must not be NULL");
 #ifdef DEBUG
   AssertNotAlreadyCached("float", aPref, aCache);
 #endif
-  {
-    AUTO_INSTALLING_CALLBACK();
-    *aCache = Preferences::GetFloat(aPref, aDefault);
-  }
+  *aCache = Preferences::GetFloat(aPref, aDefault);
   CacheData* data = new CacheData();
   data->mCacheLocation = aCache;
   data->mDefaultValueFloat = aDefault;
   CacheDataAppendElement(data);
   Preferences::RegisterCallback(FloatVarChanged,
                                 aPref,
                                 data,
                                 Preferences::ExactMatch,
