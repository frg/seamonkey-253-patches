# HG changeset patch
# User Ehsan Akhgari <ehsan@mozilla.com>
# Date 1507412991 14400
# Node ID 8c6f9b6b56ebe5bfd1eb7d6345e9af2e94cc914e
# Parent  1080c01c249577f1fbf5d4b27080dbf8242e637e
Bug 1407305 - Part 3: Avoid using GetChildAt() in EditorBase::GetPriorNode(); r=masayuki

diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -3270,34 +3270,35 @@ EditorBase::GetLengthOfDOMNode(nsIDOMNod
   NS_ENSURE_TRUE(node, NS_ERROR_NULL_POINTER);
   aCount = node->Length();
   return NS_OK;
 }
 
 nsIContent*
 EditorBase::GetPriorNode(nsINode* aParentNode,
                          int32_t aOffset,
+                         nsINode* aChildAtOffset,
                          bool aEditableNode,
                          bool aNoBlockCrossing)
 {
   MOZ_ASSERT(aParentNode);
 
   // If we are at the beginning of the node, or it is a text node, then just
   // look before it.
   if (!aOffset || aParentNode->NodeType() == nsIDOMNode::TEXT_NODE) {
     if (aNoBlockCrossing && IsBlockNode(aParentNode)) {
       // If we aren't allowed to cross blocks, don't look before this block.
       return nullptr;
     }
     return GetPriorNode(aParentNode, aEditableNode, aNoBlockCrossing);
   }
 
   // else look before the child at 'aOffset'
-  if (nsIContent* child = aParentNode->GetChildAt(aOffset)) {
-    return GetPriorNode(child, aEditableNode, aNoBlockCrossing);
+  if (aChildAtOffset) {
+    return GetPriorNode(aChildAtOffset, aEditableNode, aNoBlockCrossing);
   }
 
   // unless there isn't one, in which case we are at the end of the node
   // and want the deep-right child.
   nsIContent* resultNode = GetRightmostChild(aParentNode, aNoBlockCrossing);
   if (!resultNode || !aEditableNode || IsEditable(resultNode)) {
     return resultNode;
   }
@@ -4536,16 +4537,17 @@ EditorBase::CreateTxnForDeleteRange(nsRa
   MOZ_ASSERT(aAction != eNone);
 
   // get the node and offset of the insertion point
   nsCOMPtr<nsINode> node = aRangeToDelete->GetStartContainer();
   if (NS_WARN_IF(!node)) {
     return nullptr;
   }
 
+  nsIContent* child = aRangeToDelete->GetChildAtStartOffset();
   int32_t offset = aRangeToDelete->StartOffset();
 
   // determine if the insertion point is at the beginning, middle, or end of
   // the node
 
   uint32_t count = node->Length();
 
   bool isFirst = !offset;
@@ -4648,17 +4650,17 @@ EditorBase::CreateTxnForDeleteRange(nsRa
     node.forget(aRemovingNode);
     return deleteTextTransaction.forget();
   }
 
   // we're either deleting a node or chardata, need to dig into the next/prev
   // node to find out
   nsCOMPtr<nsINode> selectedNode;
   if (aAction == ePrevious) {
-    selectedNode = GetPriorNode(node, offset, true);
+    selectedNode = GetPriorNode(node, offset, child, true);
   } else if (aAction == eNext) {
     selectedNode = GetNextNode(node, offset, true);
   }
 
   while (selectedNode &&
          selectedNode->IsNodeOfType(nsINode::eDATA_NODE) &&
          !selectedNode->Length()) {
     // Can't delete an empty chardata node (bug 762183)
diff --git a/editor/libeditor/EditorBase.h b/editor/libeditor/EditorBase.h
--- a/editor/libeditor/EditorBase.h
+++ b/editor/libeditor/EditorBase.h
@@ -703,16 +703,17 @@ public:
   nsIContent* GetPriorNode(nsINode* aCurrentNode, bool aEditableNode,
                            bool aNoBlockCrossing = false);
 
   /**
    * And another version that takes a {parent,offset} pair rather than a node.
    */
   nsIContent* GetPriorNode(nsINode* aParentNode,
                            int32_t aOffset,
+                           nsINode* aChildAtOffset,
                            bool aEditableNode,
                            bool aNoBlockCrossing = false);
 
 
   /**
    * Get the node immediately after to aCurrentNode.
    * @param aCurrentNode   the node from which we start the search
    * @param aEditableNode  if true, only return an editable node
diff --git a/editor/libeditor/HTMLEditRules.cpp b/editor/libeditor/HTMLEditRules.cpp
--- a/editor/libeditor/HTMLEditRules.cpp
+++ b/editor/libeditor/HTMLEditRules.cpp
@@ -5063,16 +5063,17 @@ HTMLEditRules::CheckForEmptyBlock(nsINod
           NS_ENSURE_SUCCESS(rv, rv);
         }
       } else if (aAction == nsIEditor::ePrevious ||
                  aAction == nsIEditor::ePreviousWord ||
                  aAction == nsIEditor::eToBeginningOfLine) {
         // Move to the end of the previous node
         nsCOMPtr<nsIContent> priorNode = htmlEditor->GetPriorNode(blockParent,
                                                                   offset,
+                                                                  emptyBlock,
                                                                   true);
         if (priorNode) {
           EditorDOMPoint pt = GetGoodSelPointForNode(*priorNode, aAction);
           nsresult rv = aSelection->Collapse(pt.node, pt.offset);
           NS_ENSURE_SUCCESS(rv, rv);
         } else {
           nsresult rv = aSelection->Collapse(blockParent, offset + 1);
           NS_ENSURE_SUCCESS(rv, rv);
diff --git a/editor/libeditor/HTMLEditor.cpp b/editor/libeditor/HTMLEditor.cpp
--- a/editor/libeditor/HTMLEditor.cpp
+++ b/editor/libeditor/HTMLEditor.cpp
@@ -3900,17 +3900,17 @@ HTMLEditor::GetPriorHTMLNode(nsINode* aP
                              bool aNoBlockCrossing)
 {
   MOZ_ASSERT(aParent);
 
   if (!GetActiveEditingHost()) {
     return nullptr;
   }
 
-  return GetPriorNode(aParent, aOffset, true, aNoBlockCrossing);
+  return GetPriorNode(aParent, aOffset, aChildAtOffset, true, aNoBlockCrossing);
 }
 
 /**
  * GetNextHTMLNode() returns the next editable leaf node, if there is
  * one within the <body>.
  */
 nsIContent*
 HTMLEditor::GetNextHTMLNode(nsINode* aNode,
