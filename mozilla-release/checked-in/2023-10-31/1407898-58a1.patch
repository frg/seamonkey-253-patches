# HG changeset patch
# User Brian Birtles <birtles@gmail.com>
# Date 1508913290 -32400
#      Wed Oct 25 15:34:50 2017 +0900
# Node ID b23ba929796f0d808250560baa8bf5ca28899b1d
# Parent  79cf35ffeed3cf9cbf3f30f5c477adf1778b05a5
Bug 1407898 - Check if presContext is null before dereferencing in GetComputedKeyframeValues; r=hiro

There are no reliable steps to make this happen but it appears to have happened
at least twice during fuzzing. As a result, it doesn't matter too much what the
behavior here is as long as we don't crash.

MozReview-Commit-ID: 4gdiBL2wngU

diff --git a/dom/animation/KeyframeUtils.cpp b/dom/animation/KeyframeUtils.cpp
--- a/dom/animation/KeyframeUtils.cpp
+++ b/dom/animation/KeyframeUtils.cpp
@@ -486,18 +486,26 @@ KeyframeUtils::DistributeKeyframes(nsTAr
 template<typename StyleType>
 /* static */ nsTArray<AnimationProperty>
 KeyframeUtils::GetAnimationPropertiesFromKeyframes(
   const nsTArray<Keyframe>& aKeyframes,
   dom::Element* aElement,
   StyleType* aStyle,
   dom::CompositeOperation aEffectComposite)
 {
+  nsTArray<AnimationProperty> result;
+
   const nsTArray<ComputedKeyframeValues> computedValues =
     GetComputedKeyframeValues(aKeyframes, aElement, aStyle);
+  if (computedValues.IsEmpty()) {
+    // In rare cases GetComputedKeyframeValues might fail and return an empty
+    // array, in which case we likewise return an empty array from here.
+    return result;
+  }
+
   MOZ_ASSERT(aKeyframes.Length() == computedValues.Length(),
              "Array length mismatch");
 
   nsTArray<KeyframeValueEntry> entries(aKeyframes.Length());
 
   const size_t len = aKeyframes.Length();
   for (size_t i = 0; i < len; ++i) {
     const Keyframe& frame = aKeyframes[i];
@@ -509,17 +517,16 @@ KeyframeUtils::GetAnimationPropertiesFro
       entry->mProperty = value.mProperty;
       entry->mValue = value.mValue;
       entry->mTimingFunction = frame.mTimingFunction;
       entry->mComposite =
         frame.mComposite ? frame.mComposite.value() : aEffectComposite;
     }
   }
 
-  nsTArray<AnimationProperty> result;
   BuildSegmentsFromValueEntries(entries, result);
   return result;
 }
 
 /* static */ bool
 KeyframeUtils::IsAnimatableProperty(nsCSSPropertyID aProperty,
                                     StyleBackendType aBackend)
 {
@@ -1099,21 +1106,30 @@ GetComputedKeyframeValues(const nsTArray
 static nsTArray<ComputedKeyframeValues>
 GetComputedKeyframeValues(const nsTArray<Keyframe>& aKeyframes,
                           dom::Element* aElement,
                           const ServoStyleContext* aStyleContext)
 {
   MOZ_ASSERT(aElement);
   MOZ_ASSERT(aElement->IsStyledByServo());
 
-  nsPresContext* presContext = nsContentUtils::GetContextForContent(aElement);
-  MOZ_ASSERT(presContext);
+  nsTArray<ComputedKeyframeValues> result;
 
-  return presContext->StyleSet()->AsServo()
+  nsPresContext* presContext = nsContentUtils::GetContextForContent(aElement);
+  if (!presContext) {
+    // This has been reported to happen with some combinations of content
+    // (particularly involving resize events and layout flushes? See bug 1407898
+    // and bug 1408420) but no reproducible steps have been found.
+    // For now we just return an empty array.
+    return result;
+  }
+
+  result = presContext->StyleSet()->AsServo()
     ->GetComputedKeyframeValuesFor(aKeyframes, aElement, aStyleContext);
+  return result;
 }
 
 static void
 AppendInitialSegment(AnimationProperty* aAnimationProperty,
                      const KeyframeValueEntry& aFirstEntry)
 {
   AnimationPropertySegment* segment =
     aAnimationProperty->mSegments.AppendElement();
