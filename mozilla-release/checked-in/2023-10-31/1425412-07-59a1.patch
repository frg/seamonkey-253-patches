# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1513341443 -32400
# Node ID 1af2984c6a7c7967feca04fb4b1e7b13f1f79f88
# Parent  87eaf5720935ac37b7f0a9c5b99b9cdca63916b0
Bug 1425412 - part 7: Create SplitNodeTransaction::Create() and remove EditorBase::CreateTxnForSplitNode() r=m_kato

SplitNodeTransaction::Create() just hides what it does.  For making its caller
clearer, let's create a factory method, SplitNodeTransaction::Create().

MozReview-Commit-ID: KDiC8dDrLuQ

diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -1569,22 +1569,24 @@ EditorBase::SplitNode(const EditorRawDOM
       //     may be a data node like text node.  However, nobody implements this
       //     method actually.  So, we should get rid of this in a follow up bug.
       listener->WillSplitNode(aStartOfRightNode.GetContainerAsDOMNode(),
                               aStartOfRightNode.Offset());
     }
   }
 
   RefPtr<SplitNodeTransaction> transaction =
-    CreateTxnForSplitNode(aStartOfRightNode);
+    SplitNodeTransaction::Create(*this, aStartOfRightNode);
   aError = DoTransaction(transaction);
 
   nsCOMPtr<nsIContent> newNode = transaction->GetNewNode();
   NS_WARNING_ASSERTION(newNode, "Failed to create a new left node");
 
+  // XXX Some other transactions manage range updater by themselves.
+  //     Why doesn't SplitNodeTransaction do it?
   mRangeUpdater.SelAdjSplitNode(*aStartOfRightNode.GetContainerAsContent(),
                                 newNode);
 
   {
     AutoActionListenerArray listeners(mActionListeners);
     for (auto& listener : listeners) {
       listener->DidSplitNode(aStartOfRightNode.GetContainerAsDOMNode(),
                              GetAsDOMNode(newNode));
@@ -3035,25 +3037,16 @@ EditorBase::DeleteText(nsGenericDOMDataN
           static_cast<nsIDOMCharacterData*>(GetAsDOMNode(&aCharData)), aOffset,
           aLength, rv);
     }
   }
 
   return rv;
 }
 
-already_AddRefed<SplitNodeTransaction>
-EditorBase::CreateTxnForSplitNode(const EditorRawDOMPoint& aStartOfRightNode)
-{
-  MOZ_ASSERT(aStartOfRightNode.IsSetAndValid());
-  RefPtr<SplitNodeTransaction> transaction =
-    new SplitNodeTransaction(*this, aStartOfRightNode);
-  return transaction.forget();
-}
-
 already_AddRefed<JoinNodeTransaction>
 EditorBase::CreateTxnForJoinNode(nsINode& aLeftNode,
                                  nsINode& aRightNode)
 {
   RefPtr<JoinNodeTransaction> joinNodeTransaction =
     new JoinNodeTransaction(*this, aLeftNode, aRightNode);
   // If it's not editable, the transaction shouldn't be recorded since it
   // should never be undone/redone.
diff --git a/editor/libeditor/EditorBase.h b/editor/libeditor/EditorBase.h
--- a/editor/libeditor/EditorBase.h
+++ b/editor/libeditor/EditorBase.h
@@ -585,31 +585,16 @@ protected:
    * Create a transaction for removing a style sheet.
    */
   already_AddRefed<mozilla::RemoveStyleSheetTransaction>
     CreateTxnForRemoveStyleSheet(StyleSheet* aSheet);
 
   nsresult DeleteText(nsGenericDOMDataNode& aElement,
                       uint32_t aOffset, uint32_t aLength);
 
-  /**
-   * CreateTxnForSplitNode() creates a transaction to create a new node
-   * (left node) identical to an existing node (right node), and split the
-   * contents between the same point in both nodes.
-   *
-   * @param aStartOfRightNode   The point to split.  Its container will be
-   *                            the right node, i.e., become the new node's
-   *                            next sibling.  And the point will be start
-   *                            of the right node.
-   * @return                    The new transaction to split the container of
-   *                            aStartOfRightNode.
-   */
-  already_AddRefed<SplitNodeTransaction>
-    CreateTxnForSplitNode(const EditorRawDOMPoint& aStartOfRightNode);
-
   already_AddRefed<JoinNodeTransaction>
     CreateTxnForJoinNode(nsINode& aLeftNode, nsINode& aRightNode);
 
   /**
    * This method first deletes the selection, if it's not collapsed.  Then if
    * the selection lies in a CharacterData node, it splits it.  If the
    * selection is at this point collapsed in a CharacterData node, it's
    * adjusted to be collapsed right before or after the node instead (which is
diff --git a/editor/libeditor/SplitNodeTransaction.cpp b/editor/libeditor/SplitNodeTransaction.cpp
--- a/editor/libeditor/SplitNodeTransaction.cpp
+++ b/editor/libeditor/SplitNodeTransaction.cpp
@@ -12,16 +12,26 @@
 #include "nsDebug.h"                    // for NS_ASSERTION, etc.
 #include "nsError.h"                    // for NS_ERROR_NOT_INITIALIZED, etc.
 #include "nsIContent.h"                 // for nsIContent
 
 namespace mozilla {
 
 using namespace dom;
 
+// static
+already_AddRefed<SplitNodeTransaction>
+SplitNodeTransaction::Create(EditorBase& aEditorBase,
+                             const EditorRawDOMPoint& aStartOfRightNode)
+{
+  RefPtr<SplitNodeTransaction> transaction =
+    new SplitNodeTransaction(aEditorBase, aStartOfRightNode);
+  return transaction.forget();
+}
+
 SplitNodeTransaction::SplitNodeTransaction(
                         EditorBase& aEditorBase,
                         const EditorRawDOMPoint& aStartOfRightNode)
   : mEditorBase(&aEditorBase)
   , mStartOfRightNode(aStartOfRightNode)
 {
   MOZ_DIAGNOSTIC_ASSERT(aStartOfRightNode.IsSet());
   MOZ_DIAGNOSTIC_ASSERT(aStartOfRightNode.GetContainerAsContent());
diff --git a/editor/libeditor/SplitNodeTransaction.h b/editor/libeditor/SplitNodeTransaction.h
--- a/editor/libeditor/SplitNodeTransaction.h
+++ b/editor/libeditor/SplitNodeTransaction.h
@@ -21,26 +21,35 @@ namespace mozilla {
 class EditorBase;
 
 /**
  * A transaction that splits a node into two identical nodes, with the children
  * divided between the new nodes.
  */
 class SplitNodeTransaction final : public EditTransactionBase
 {
+private:
+  SplitNodeTransaction(EditorBase& aEditorBase,
+                       const EditorRawDOMPoint& aStartOfRightNode);
+
 public:
   /**
+   * Creates a transaction to create a new node (left node) identical to an
+   * existing node (right node), and split the contents between the same point
+   * in both nodes.
+   *
    * @param aEditorBase         The provider of core editing operations.
    * @param aStartOfRightNode   The point to split.  Its container will be
    *                            the right node, i.e., become the new node's
    *                            next sibling.  And the point will be start
    *                            of the right node.
    */
-  SplitNodeTransaction(EditorBase& aEditorBase,
-                       const EditorRawDOMPoint& aStartOfRightNode);
+  static already_AddRefed<SplitNodeTransaction>
+  Create(EditorBase& aEditorBase,
+         const EditorRawDOMPoint& aStartOfRightNode);
 
   NS_DECL_ISUPPORTS_INHERITED
   NS_DECL_CYCLE_COLLECTION_CLASS_INHERITED(SplitNodeTransaction,
                                            EditTransactionBase)
 
   NS_DECL_EDITTRANSACTIONBASE
 
   NS_IMETHOD RedoTransaction() override;
