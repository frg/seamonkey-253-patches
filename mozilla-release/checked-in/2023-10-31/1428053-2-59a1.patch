# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1515147332 -3600
# Node ID 07467c83fe96babaca416268fc6ae2128d55cf98
# Parent  99ceac76c3b940ea781cffdaf5e1a50ad2364fc3
Bug 1428053: Fix ShadowRoot::ContentAppended. r=jessica

MozReview-Commit-ID: iUhaP8VVIO

diff --git a/dom/base/ShadowRoot.cpp b/dom/base/ShadowRoot.cpp
--- a/dom/base/ShadowRoot.cpp
+++ b/dom/base/ShadowRoot.cpp
@@ -523,17 +523,17 @@ ShadowRoot::AttributeChanged(nsIDocument
 void
 ShadowRoot::ContentAppended(nsIDocument* aDocument,
                             nsIContent* aContainer,
                             nsIContent* aFirstNewContent)
 {
   for (nsIContent* content = aFirstNewContent;
        content;
        content = content->GetNextSibling()) {
-    ContentInserted(aDocument, aContainer, aFirstNewContent);
+    ContentInserted(aDocument, aContainer, content);
   }
 }
 
 void
 ShadowRoot::ContentInserted(nsIDocument* aDocument,
                             nsIContent* aContainer,
                             nsIContent* aChild)
 {
diff --git a/dom/base/crashtests/1428053.html b/dom/base/crashtests/1428053.html
new file mode 100644
--- /dev/null
+++ b/dom/base/crashtests/1428053.html
@@ -0,0 +1,23 @@
+<script>
+  try { o1 = document.createElement("h1") } catch (e) {}
+  try { o2 = document.createElement("slot") } catch (e) {}
+  try { o3 = document.createElement("o") } catch (e) {}
+  try { document.documentElement.appendChild(o1) } catch (e) {}
+  try { document.documentElement.appendChild(o3) } catch (e) {}
+  try { o4 = o1.attachShadow({mode: "open"}) } catch (e) {}
+  try { o5 = new Range() } catch (e) {}
+  try { o6 = new MutationObserver(function(arg_0, arg_1) {
+      try { o4.append("", o2, "") } catch (e) {};
+  }) } catch (e) {}
+  try { o5.selectNode(o3); } catch (e) {}
+  try { customElements.define("custom-element_0", class extends HTMLElement {
+      connectedCallback() {
+          try { this.offsetHeight } catch (e) {}
+      }
+  }) } catch (e) {}
+  try { o8 = document.createElement("custom-element_0") } catch (e) {}
+  try { document.documentElement.appendChild(o8) } catch (e) {}
+  try { o6.observe(document, { "childList": [""] }); } catch (e) {}
+  try { document.replaceChild(o5.endContainer, document.documentElement); } catch (e) {}
+  try { o1.insertAdjacentHTML("afterBegin", "<</#"); } catch (e) {}
+</script>
diff --git a/dom/base/crashtests/crashtests.list b/dom/base/crashtests/crashtests.list
--- a/dom/base/crashtests/crashtests.list
+++ b/dom/base/crashtests/crashtests.list
@@ -233,9 +233,10 @@ load 1400701.html
 load 1403377.html
 load 1405771.html
 load 1406109-1.html
 pref(dom.webcomponents.enabled,true) load 1324463.html
 pref(dom.webcomponents.customelements.enabled,true) load 1413815.html
 load 1411473.html
 pref(dom.webcomponents.enabled,false) load 1422931.html
 pref(dom.webcomponents.enabled,true) load 1419799.html
+pref(dom.webcomponents.enabled,true) load 1428053.html
 pref(layout.css.resizeobserver.enabled,true) load 1555786.html
diff --git a/dom/html/HTMLSlotElement.cpp b/dom/html/HTMLSlotElement.cpp
--- a/dom/html/HTMLSlotElement.cpp
+++ b/dom/html/HTMLSlotElement.cpp
@@ -174,39 +174,47 @@ const nsTArray<RefPtr<nsINode>>&
 HTMLSlotElement::AssignedNodes() const
 {
   return mAssignedNodes;
 }
 
 void
 HTMLSlotElement::InsertAssignedNode(uint32_t aIndex, nsINode* aNode)
 {
+  MOZ_ASSERT(!aNode->AsContent()->GetAssignedSlot(), "Losing track of a slot");
   mAssignedNodes.InsertElementAt(aIndex, aNode);
   aNode->AsContent()->SetAssignedSlot(this);
 }
 
 void
 HTMLSlotElement::AppendAssignedNode(nsINode* aNode)
 {
+  MOZ_ASSERT(!aNode->AsContent()->GetAssignedSlot(), "Losing track of a slot");
   mAssignedNodes.AppendElement(aNode);
   aNode->AsContent()->SetAssignedSlot(this);
 }
 
 void
 HTMLSlotElement::RemoveAssignedNode(nsINode* aNode)
 {
+  // This one runs from unlinking, so we can't guarantee that the slot pointer
+  // hasn't been cleared.
+  MOZ_ASSERT(!aNode->AsContent()->GetAssignedSlot() ||
+             aNode->AsContent()->GetAssignedSlot() == this, "How exactly?");
   mAssignedNodes.RemoveElement(aNode);
   aNode->AsContent()->SetAssignedSlot(nullptr);
 }
 
 void
 HTMLSlotElement::ClearAssignedNodes()
 {
-  for (uint32_t i = 0; i < mAssignedNodes.Length(); i++) {
-    mAssignedNodes[i]->AsContent()->SetAssignedSlot(nullptr);
+  for (RefPtr<nsINode>& node : mAssignedNodes) {
+    MOZ_ASSERT(!node->AsContent()->GetAssignedSlot() ||
+               node->AsContent()->GetAssignedSlot() == this, "How exactly?");
+    node->AsContent()->SetAssignedSlot(nullptr);
   }
 
   mAssignedNodes.Clear();
 }
 
 void
 HTMLSlotElement::EnqueueSlotChangeEvent() const
 {
