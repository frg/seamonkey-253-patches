# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1513113658 21600
#      Tue Dec 12 15:20:58 2017 -0600
# Node ID 2f00afc343244ac9b330812b97f4c978a30b71ae
# Parent  400c74532fa9ec1c023753454629986626dce458
Bug 1424946 - Move various GeneralParser::newName-style functions into a baser class.  r=arai

diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -2366,38 +2366,38 @@ PerHandlerParser<ParseHandler>::declareF
             return false;
         }
         funbox->setHasThisBinding();
     }
 
     return true;
 }
 
-template <class ParseHandler, typename CharT>
+template <class ParseHandler>
 typename ParseHandler::Node
-GeneralParser<ParseHandler, CharT>::newInternalDotName(HandlePropertyName name)
+PerHandlerParser<ParseHandler>::newInternalDotName(HandlePropertyName name)
 {
     Node nameNode = newName(name);
     if (!nameNode)
         return null();
     if (!noteUsedName(name))
         return null();
     return nameNode;
 }
 
-template <class ParseHandler, typename CharT>
+template <class ParseHandler>
 typename ParseHandler::Node
-GeneralParser<ParseHandler, CharT>::newThisName()
+PerHandlerParser<ParseHandler>::newThisName()
 {
     return newInternalDotName(context->names().dotThis);
 }
 
-template <class ParseHandler, typename CharT>
+template <class ParseHandler>
 typename ParseHandler::Node
-GeneralParser<ParseHandler, CharT>::newDotGeneratorName()
+PerHandlerParser<ParseHandler>::newDotGeneratorName()
 {
     return newInternalDotName(context->names().dotGenerator);
 }
 
 template <class ParseHandler, typename CharT>
 bool
 GeneralParser<ParseHandler, CharT>::declareDotGeneratorName()
 {
@@ -8775,26 +8775,26 @@ GeneralParser<ParseHandler, CharT>::memb
     if (handler.isSuperBase(lhs)) {
         error(JSMSG_BAD_SUPER);
         return null();
     }
 
     return lhs;
 }
 
-template <class ParseHandler, typename CharT>
-typename ParseHandler::Node
-GeneralParser<ParseHandler, CharT>::newName(PropertyName* name)
+template <class ParseHandler>
+inline typename ParseHandler::Node
+PerHandlerParser<ParseHandler>::newName(PropertyName* name)
 {
     return newName(name, pos());
 }
 
-template <class ParseHandler, typename CharT>
-typename ParseHandler::Node
-GeneralParser<ParseHandler, CharT>::newName(PropertyName* name, TokenPos pos)
+template <class ParseHandler>
+inline typename ParseHandler::Node
+PerHandlerParser<ParseHandler>::newName(PropertyName* name, TokenPos pos)
 {
     return handler.newName(name, pos, context);
 }
 
 template <class ParseHandler, typename CharT>
 bool
 GeneralParser<ParseHandler, CharT>::checkLabelOrIdentifierReference(PropertyName* ident,
                                                                     uint32_t offset,
diff --git a/js/src/frontend/Parser.h b/js/src/frontend/Parser.h
--- a/js/src/frontend/Parser.h
+++ b/js/src/frontend/Parser.h
@@ -442,16 +442,18 @@ class PerHandlerParser
   protected:
     PerHandlerParser(JSContext* cx, LifoAlloc& alloc, const ReadOnlyCompileOptions& options,
                      const char16_t* chars, size_t length, bool foldConstants,
                      UsedNameTracker& usedNames, LazyScript* lazyOuterFunction);
 
     /* State specific to the kind of parse being performed. */
     ParseHandler handler;
 
+    static Node null() { return ParseHandler::null(); }
+
     const char* nameIsArgumentsOrEval(Node node);
 
     bool noteDestructuredPositionalFormalParameter(Node fn, Node destruct);
 
     bool noteUsedName(HandlePropertyName name) {
         // If the we are delazifying, the LazyScript already has all the
         // closed-over info for bindings and there's no need to track used
         // names.
@@ -461,16 +463,23 @@ class PerHandlerParser
         return ParserBase::noteUsedNameInternal(name);
     }
 
     // Required on Scope exit.
     bool propagateFreeNamesAndMarkClosedOverBindings(ParseContext::Scope& scope);
 
     bool declareFunctionThis();
 
+    inline Node newName(PropertyName* name);
+    inline Node newName(PropertyName* name, TokenPos pos);
+
+    Node newInternalDotName(HandlePropertyName name);
+    Node newThisName();
+    Node newDotGeneratorName();
+
   public:
     bool isValidSimpleAssignmentTarget(Node node,
                                        FunctionCallBehavior behavior = ForbidAssignmentToFunctionCalls);
 
     FunctionBox* newFunctionBox(Node fn, JSFunction* fun, uint32_t toStringStart,
                                 Directives directives, GeneratorKind generatorKind,
                                 FunctionAsyncKind asyncKind);
 };
@@ -523,16 +532,18 @@ class GeneralParser
     using Base::getFilename;
     using Base::hasUsedFunctionSpecialName;
     using Base::hasValidSimpleStrictParameterNames;
     using Base::isUnexpectedEOF_;
     using Base::keepAtoms;
     using Base::nameIsArgumentsOrEval;
     using Base::newFunction;
     using Base::newFunctionBox;
+    using Base::newName;
+    using Base::null;
     using Base::options;
     using Base::pos;
     using Base::propagateFreeNamesAndMarkClosedOverBindings;
     using Base::setLocalStrictMode;
     using Base::traceListHead;
     using Base::yieldExpressionsSupported;
 
   public:
@@ -541,16 +552,19 @@ class GeneralParser
     using Base::handler;
     using Base::isValidSimpleAssignmentTarget;
     using Base::pc;
     using Base::usedNames;
 
   private:
     using Base::declareFunctionThis;
     using Base::hasUsedName;
+    using Base::newDotGeneratorName;
+    using Base::newInternalDotName;
+    using Base::newThisName;
     using Base::noteDestructuredPositionalFormalParameter;
     using Base::noteUsedName;
 
   private:
     inline FinalParser* asFinalParser();
     inline const FinalParser* asFinalParser() const;
 
     /*
@@ -1021,19 +1035,16 @@ class GeneralParser
 
     bool matchLabel(YieldHandling yieldHandling, MutableHandle<PropertyName*> label);
 
     // Indicate if the next token (tokenized as Operand) is |in| or |of|.  If
     // so, consume it.
     bool matchInOrOf(bool* isForInp, bool* isForOfp);
 
     bool declareFunctionArgumentsObject();
-    Node newInternalDotName(HandlePropertyName name);
-    Node newThisName();
-    Node newDotGeneratorName();
     bool declareDotGeneratorName();
 
     inline bool finishFunction(bool isStandaloneFunction = false);
 
     bool leaveInnerFunction(ParseContext* outerpc);
 
   private:
     bool checkIncDecOperand(Node operand, uint32_t operandOffset);
@@ -1082,23 +1093,18 @@ class GeneralParser
                                              PossibleError* exprPossibleError,
                                              PossibleError* possibleError);
 
     Node newNumber(const Token& tok) {
         return handler.newNumber(tok.number(), tok.decimalPoint(), tok.pos);
     }
 
   protected:
-    static Node null() { return ParseHandler::null(); }
-
     Node stringLiteral();
 
-    inline Node newName(PropertyName* name);
-    inline Node newName(PropertyName* name, TokenPos pos);
-
     // Match the current token against the BindingIdentifier production with
     // the given Yield parameter.  If there is no match, report a syntax
     // error.
     PropertyName* bindingIdentifier(YieldHandling yieldHandling);
 
     bool checkLabelOrIdentifierReference(PropertyName* ident, uint32_t offset,
                                          YieldHandling yieldHandling,
                                          TokenKind hint = TokenKind::Limit);
