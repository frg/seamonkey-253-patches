# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1513760967 28800
# Node ID 3d1558337a613854f9e8d42276989ade2c9e3d0a
# Parent  e61663b19d89cf5608f9aaa579457c560c4d99fa
Bug 1427668 - Reject too-large MozFramebuffer requests. - r=daoshengmu

MozReview-Commit-ID: G2jqeb7QqhE

diff --git a/gfx/gl/GLContext.h b/gfx/gl/GLContext.h
--- a/gfx/gl/GLContext.h
+++ b/gfx/gl/GLContext.h
@@ -3617,21 +3617,20 @@ protected:
                                 target <= LOCAL_GL_TEXTURE_CUBE_MAP_NEGATIVE_Z)
                               ? mMaxCubeMapTextureSize
                               : mMaxTextureSize;
             return width <= maxSize && height <= maxSize;
         }
         return true;
     }
 
-
 public:
-    GLsizei MaxSamples() const {
-        return mMaxSamples;
-    }
+    auto MaxSamples() const { return uint32_t(mMaxSamples); }
+    auto MaxTextureSize() const { return uint32_t(mMaxTextureSize); }
+    auto MaxRenderbufferSize() const { return uint32_t(mMaxRenderbufferSize); }
 
 #ifdef MOZ_GL_DEBUG
     void CreatedProgram(GLContext* aOrigin, GLuint aName);
     void CreatedShader(GLContext* aOrigin, GLuint aName);
     void CreatedBuffers(GLContext* aOrigin, GLsizei aCount, GLuint* aNames);
     void CreatedQueries(GLContext* aOrigin, GLsizei aCount, GLuint* aNames);
     void CreatedTextures(GLContext* aOrigin, GLsizei aCount, GLuint* aNames);
     void CreatedFramebuffers(GLContext* aOrigin, GLsizei aCount, GLuint* aNames);
diff --git a/gfx/gl/GLScreenBuffer.cpp b/gfx/gl/GLScreenBuffer.cpp
--- a/gfx/gl/GLScreenBuffer.cpp
+++ b/gfx/gl/GLScreenBuffer.cpp
@@ -812,17 +812,17 @@ DrawBuffer::Create(GLContext* const gl,
         // Nothing is needed.
         return true;
     }
 
     if (caps.antialias) {
         if (formats.samples == 0)
             return false; // Can't create it.
 
-        MOZ_ASSERT(formats.samples <= gl->MaxSamples());
+        MOZ_ASSERT(uint32_t(formats.samples) <= gl->MaxSamples());
     }
 
     GLuint colorMSRB = 0;
     GLuint depthRB   = 0;
     GLuint stencilRB = 0;
 
     GLuint* pColorMSRB = caps.antialias ? &colorMSRB : nullptr;
     GLuint* pDepthRB   = caps.depth     ? &depthRB   : nullptr;
diff --git a/gfx/gl/MozFramebuffer.cpp b/gfx/gl/MozFramebuffer.cpp
--- a/gfx/gl/MozFramebuffer.cpp
+++ b/gfx/gl/MozFramebuffer.cpp
@@ -30,22 +30,33 @@ MozFramebuffer::Create(GLContext* const 
 
     gl->MakeCurrent();
 
     GLContext::LocalErrorScope errorScope(*gl);
 
     GLenum colorTarget;
     GLuint colorName;
     if (samples) {
+        if (uint32_t(size.width) > gl->MaxRenderbufferSize() ||
+            uint32_t(size.height) > gl->MaxRenderbufferSize() ||
+            samples > gl->MaxSamples())
+        {
+            return nullptr;
+        }
         colorTarget = LOCAL_GL_RENDERBUFFER;
         colorName = gl->CreateRenderbuffer();
         const ScopedBindRenderbuffer bindRB(gl, colorName);
         gl->fRenderbufferStorageMultisample(colorTarget, samples, LOCAL_GL_RGBA8,
                                             size.width, size.height);
     } else {
+        if (uint32_t(size.width) > gl->MaxTextureSize() ||
+            uint32_t(size.height) > gl->MaxTextureSize())
+        {
+            return nullptr;
+        }
         colorTarget = LOCAL_GL_TEXTURE_2D;
         colorName = gl->CreateTexture();
         const ScopedBindTexture bindTex(gl, colorName);
         gl->TexParams_SetClampNoMips();
         const ScopedBindPBO bindPBO(gl, LOCAL_GL_PIXEL_UNPACK_BUFFER);
         gl->fBindBuffer(LOCAL_GL_PIXEL_UNPACK_BUFFER, 0);
         gl->fTexImage2D(colorTarget, 0, LOCAL_GL_RGBA,
                         size.width, size.height, 0,
