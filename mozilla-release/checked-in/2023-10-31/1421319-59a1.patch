# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1511954953 0
# Node ID dab9aa2ed975046aba9507d1d68efc8f95d98496
# Parent  a51dd2e3464e884a9904f0aeff2d98c2c6b1e3b2
Bug 1421319 - Split out GCManagedDeletePolicy into its own header r=sfink

diff --git a/js/src/gc/DeletePolicy.h b/js/src/gc/DeletePolicy.h
new file mode 100644
--- /dev/null
+++ b/js/src/gc/DeletePolicy.h
@@ -0,0 +1,79 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
+ * vim: set ts=8 sts=4 et sw=4 tw=99:
+ * This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef gc_DeletePolicy_h
+#define gc_DeletePolicy_h
+
+#include "js/TracingAPI.h"
+
+namespace js {
+namespace gc {
+
+struct ClearEdgesTracer : public JS::CallbackTracer
+{
+    ClearEdgesTracer();
+
+#ifdef DEBUG
+    TracerKind getTracerKind() const override { return TracerKind::ClearEdges; }
+#endif
+
+    template <typename T>
+    inline void clearEdge(T** thingp);
+
+    void onObjectEdge(JSObject** objp) override;
+    void onStringEdge(JSString** strp) override;
+    void onSymbolEdge(JS::Symbol** symp) override;
+    void onScriptEdge(JSScript** scriptp) override;
+    void onShapeEdge(js::Shape** shapep) override;
+    void onObjectGroupEdge(js::ObjectGroup** groupp) override;
+    void onBaseShapeEdge(js::BaseShape** basep) override;
+    void onJitCodeEdge(js::jit::JitCode** codep) override;
+    void onLazyScriptEdge(js::LazyScript** lazyp) override;
+    void onScopeEdge(js::Scope** scopep) override;
+    void onRegExpSharedEdge(js::RegExpShared** sharedp) override;
+    void onChild(const JS::GCCellPtr& thing) override;
+};
+
+#ifdef DEBUG
+inline bool
+IsClearEdgesTracer(JSTracer *trc)
+{
+    return trc->isCallbackTracer() &&
+           trc->asCallbackTracer()->getTracerKind() == JS::CallbackTracer::TracerKind::ClearEdges;
+}
+#endif
+
+} // namespace gc
+
+/*
+ * Provides a delete policy that can be used for objects which have their
+ * lifetime managed by the GC so they can be safely destroyed outside of GC.
+ *
+ * This is necessary for example when initializing such an object may fail after
+ * the initial allocation. The partially-initialized object must be destroyed,
+ * but it may not be safe to do so at the current time as the store buffer may
+ * contain pointers into it.
+ *
+ * This policy traces GC pointers in the object and clears them, making sure to
+ * trigger barriers while doing so. This will remove any store buffer pointers
+ * into the object and make it safe to delete.
+ */
+template <typename T>
+struct GCManagedDeletePolicy
+{
+    void operator()(const T* constPtr) {
+        if (constPtr) {
+            auto ptr = const_cast<T*>(constPtr);
+            gc::ClearEdgesTracer trc;
+            ptr->trace(&trc);
+            js_delete(ptr);
+        }
+    }
+};
+
+} // namespace js
+
+#endif // gc_DeletePolicy_h
diff --git a/js/src/gc/Zone.h b/js/src/gc/Zone.h
--- a/js/src/gc/Zone.h
+++ b/js/src/gc/Zone.h
@@ -4,31 +4,30 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef gc_Zone_h
 #define gc_Zone_h
 
 #include "mozilla/Atomics.h"
 #include "mozilla/HashFunctions.h"
-#include "mozilla/MemoryReporting.h"
 
-#include "jscntxt.h"
-
-#include "ds/SplayTree.h"
 #include "gc/FindSCCs.h"
 #include "gc/GCRuntime.h"
 #include "js/GCHashTable.h"
-#include "js/TracingAPI.h"
 #include "vm/MallocProvider.h"
 #include "vm/RegExpShared.h"
-#include "vm/TypeInference.h"
+#include "vm/Runtime.h"
+
+struct JSContext;
 
 namespace js {
 
+class Debugger;
+
 namespace jit {
 class JitZone;
 } // namespace jit
 
 namespace gc {
 
 class GCSchedulingState;
 class GCSchedulingTunables;
@@ -978,97 +977,11 @@ ZoneAllocPolicy::pod_calloc(size_t numEl
 
 template <typename T>
 inline T*
 ZoneAllocPolicy::pod_realloc(T* p, size_t oldSize, size_t newSize)
 {
     return zone->pod_realloc<T>(p, oldSize, newSize);
 }
 
-/*
- * Provides a delete policy that can be used for objects which have their
- * lifetime managed by the GC so they can be safely destroyed outside of GC.
- *
- * This is necessary for example when initializing such an object may fail after
- * the initial allocation. The partially-initialized object must be destroyed,
- * but it may not be safe to do so at the current time as the store buffer may
- * contain pointers into it.
- *
- * This policy traces GC pointers in the object and clears them, making sure to
- * trigger barriers while doing so. This will remove any store buffer pointers
- * into the object and make it safe to delete.
- */
-template <typename T>
-struct GCManagedDeletePolicy
-{
-    struct ClearEdgesTracer : public JS::CallbackTracer
-    {
-        explicit ClearEdgesTracer(JSContext* cx) : CallbackTracer(cx, TraceWeakMapKeysValues) {}
-#ifdef DEBUG
-        TracerKind getTracerKind() const override { return TracerKind::ClearEdges; }
-#endif
-
-        template <typename S>
-        void clearEdge(S** thingp) {
-            InternalBarrierMethods<S*>::preBarrier(*thingp);
-            InternalBarrierMethods<S*>::postBarrier(thingp, *thingp, nullptr);
-            *thingp = nullptr;
-        }
-
-        void onObjectEdge(JSObject** objp) override { clearEdge(objp); }
-        void onStringEdge(JSString** strp) override { clearEdge(strp); }
-        void onSymbolEdge(JS::Symbol** symp) override { clearEdge(symp); }
-        void onScriptEdge(JSScript** scriptp) override { clearEdge(scriptp); }
-        void onShapeEdge(js::Shape** shapep) override { clearEdge(shapep); }
-        void onObjectGroupEdge(js::ObjectGroup** groupp) override { clearEdge(groupp); }
-        void onBaseShapeEdge(js::BaseShape** basep) override { clearEdge(basep); }
-        void onJitCodeEdge(js::jit::JitCode** codep) override { clearEdge(codep); }
-        void onLazyScriptEdge(js::LazyScript** lazyp) override { clearEdge(lazyp); }
-        void onScopeEdge(js::Scope** scopep) override { clearEdge(scopep); }
-        void onRegExpSharedEdge(js::RegExpShared** sharedp) override { clearEdge(sharedp); }
-        void onChild(const JS::GCCellPtr& thing) override { MOZ_CRASH(); }
-    };
-
-    void operator()(const T* constPtr) {
-        if (constPtr) {
-            auto ptr = const_cast<T*>(constPtr);
-            ClearEdgesTracer trc(TlsContext.get());
-            ptr->trace(&trc);
-            js_delete(ptr);
-        }
-    }
-};
-
-#ifdef DEBUG
-inline bool
-IsClearEdgesTracer(JSTracer *trc)
-{
-    return trc->isCallbackTracer() &&
-           trc->asCallbackTracer()->getTracerKind() == JS::CallbackTracer::TracerKind::ClearEdges;
-}
-#endif
-
 } // namespace js
 
-namespace JS {
-
-// Scope data that contain GCPtrs must use the correct DeletePolicy.
-//
-// This is defined here because vm/Scope.h cannot #include "vm/Runtime.h"
-
-template <>
-struct DeletePolicy<js::FunctionScope::Data>
-  : public js::GCManagedDeletePolicy<js::FunctionScope::Data>
-{ };
-
-template <>
-struct DeletePolicy<js::ModuleScope::Data>
-  : public js::GCManagedDeletePolicy<js::ModuleScope::Data>
-{ };
-
-template <>
-struct DeletePolicy<js::WasmInstanceScope::Data>
-  : public js::GCManagedDeletePolicy<js::WasmInstanceScope::Data>
-{ };
-
-} // namespace JS
-
 #endif // gc_Zone_h
diff --git a/js/src/jscntxt.h b/js/src/jscntxt.h
--- a/js/src/jscntxt.h
+++ b/js/src/jscntxt.h
@@ -13,16 +13,17 @@
 
 #include "js/CharacterEncoding.h"
 #include "js/GCVector.h"
 #include "js/Result.h"
 #include "js/Utility.h"
 #include "js/Vector.h"
 #include "threading/ProtectedData.h"
 #include "vm/ErrorReporting.h"
+#include "vm/MallocProvider.h"
 #include "vm/Runtime.h"
 
 #ifdef _MSC_VER
 #pragma warning(push)
 #pragma warning(disable:4100) /* Silence unreferenced formal parameter warnings */
 #endif
 
 struct DtoaState;
diff --git a/js/src/jsgc.cpp b/js/src/jsgc.cpp
--- a/js/src/jsgc.cpp
+++ b/js/src/jsgc.cpp
@@ -9108,8 +9108,34 @@ js::gc::detail::CellIsNotGray(const Cell
 
 extern JS_PUBLIC_API(bool)
 js::gc::detail::ObjectIsMarkedBlack(const JSObject* obj)
 {
     return obj->isMarkedBlack();
 }
 
 #endif
+
+js::gc::ClearEdgesTracer::ClearEdgesTracer()
+  : CallbackTracer(TlsContext.get(), TraceWeakMapKeysValues)
+{}
+
+template <typename S>
+inline void
+js::gc::ClearEdgesTracer::clearEdge(S** thingp)
+{
+    InternalBarrierMethods<S*>::preBarrier(*thingp);
+    InternalBarrierMethods<S*>::postBarrier(thingp, *thingp, nullptr);
+    *thingp = nullptr;
+}
+
+void js::gc::ClearEdgesTracer::onObjectEdge(JSObject** objp) { clearEdge(objp); }
+void js::gc::ClearEdgesTracer::onStringEdge(JSString** strp) { clearEdge(strp); }
+void js::gc::ClearEdgesTracer::onSymbolEdge(JS::Symbol** symp) { clearEdge(symp); }
+void js::gc::ClearEdgesTracer::onScriptEdge(JSScript** scriptp) { clearEdge(scriptp); }
+void js::gc::ClearEdgesTracer::onShapeEdge(js::Shape** shapep) { clearEdge(shapep); }
+void js::gc::ClearEdgesTracer::onObjectGroupEdge(js::ObjectGroup** groupp) { clearEdge(groupp); }
+void js::gc::ClearEdgesTracer::onBaseShapeEdge(js::BaseShape** basep) { clearEdge(basep); }
+void js::gc::ClearEdgesTracer::onJitCodeEdge(js::jit::JitCode** codep) { clearEdge(codep); }
+void js::gc::ClearEdgesTracer::onLazyScriptEdge(js::LazyScript** lazyp) { clearEdge(lazyp); }
+void js::gc::ClearEdgesTracer::onScopeEdge(js::Scope** scopep) { clearEdge(scopep); }
+void js::gc::ClearEdgesTracer::onRegExpSharedEdge(js::RegExpShared** sharedp) { clearEdge(sharedp); }
+void js::gc::ClearEdgesTracer::onChild(const JS::GCCellPtr& thing) { MOZ_CRASH(); }
diff --git a/js/src/jsweakmap.h b/js/src/jsweakmap.h
--- a/js/src/jsweakmap.h
+++ b/js/src/jsweakmap.h
@@ -9,16 +9,17 @@
 
 #include "mozilla/LinkedList.h"
 #include "mozilla/Move.h"
 
 #include "jscompartment.h"
 #include "jsfriendapi.h"
 #include "jsobj.h"
 
+#include "gc/DeletePolicy.h"
 #include "gc/StoreBuffer.h"
 #include "js/HashTable.h"
 
 namespace js {
 
 class GCMarker;
 class WeakMapBase;
 
diff --git a/js/src/vm/Scope.h b/js/src/vm/Scope.h
--- a/js/src/vm/Scope.h
+++ b/js/src/vm/Scope.h
@@ -8,16 +8,17 @@
 #define vm_Scope_h
 
 #include "mozilla/Maybe.h"
 #include "mozilla/Variant.h"
 
 #include "jsobj.h"
 #include "jsopcode.h"
 
+#include "gc/DeletePolicy.h"
 #include "gc/Heap.h"
 #include "gc/Policy.h"
 #include "js/UbiNode.h"
 #include "js/UniquePtr.h"
 #include "vm/Xdr.h"
 
 namespace js {
 
@@ -1533,16 +1534,33 @@ DEFINE_SCOPE_DATA_GCPOLICY(js::FunctionS
 DEFINE_SCOPE_DATA_GCPOLICY(js::VarScope::Data);
 DEFINE_SCOPE_DATA_GCPOLICY(js::GlobalScope::Data);
 DEFINE_SCOPE_DATA_GCPOLICY(js::EvalScope::Data);
 DEFINE_SCOPE_DATA_GCPOLICY(js::ModuleScope::Data);
 DEFINE_SCOPE_DATA_GCPOLICY(js::WasmFunctionScope::Data);
 
 #undef DEFINE_SCOPE_DATA_GCPOLICY
 
+// Scope data that contain GCPtrs must use the correct DeletePolicy.
+
+template <>
+struct DeletePolicy<js::FunctionScope::Data>
+  : public js::GCManagedDeletePolicy<js::FunctionScope::Data>
+{};
+
+template <>
+struct DeletePolicy<js::ModuleScope::Data>
+  : public js::GCManagedDeletePolicy<js::ModuleScope::Data>
+{};
+
+template <>
+struct DeletePolicy<js::WasmInstanceScope::Data>
+  : public js::GCManagedDeletePolicy<js::WasmInstanceScope::Data>
+{ };
+
 namespace ubi {
 
 template <>
 class Concrete<js::Scope> : TracerConcrete<js::Scope>
 {
   protected:
     explicit Concrete(js::Scope* ptr) : TracerConcrete<js::Scope>(ptr) { }
 
diff --git a/js/src/vm/UnboxedObject.h b/js/src/vm/UnboxedObject.h
--- a/js/src/vm/UnboxedObject.h
+++ b/js/src/vm/UnboxedObject.h
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef vm_UnboxedObject_h
 #define vm_UnboxedObject_h
 
 #include "jsobj.h"
 
+#include "gc/DeletePolicy.h"
 #include "gc/Zone.h"
 #include "vm/Runtime.h"
 #include "vm/TypeInference.h"
 
 namespace js {
 
 // Memory required for an unboxed value of a given type. Returns zero for types
 // which can't be used for unboxed objects.
