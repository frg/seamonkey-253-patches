# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1512146493 21600
# Node ID 54af014e3fc6f5dcdacb47785d20b303b9d3efe7
# Parent  89e419a2890c4c1e0ac723b2952c461338dfe564
servo: Merge #19446 - style: Make -moz-border-colors chrome only (from emilio:bye-moz-border-colors); r=xidorn

This also makes the `border` shorthand not reset them.

Bug: 1417200
Reviewed-by: xidorn
MozReview-Commit-ID: KNais1e5FnE
Source-Repo: https://github.com/servo/servo
Source-Revision: af8d53372f1cb7364b07f48bbd3d9ecd01d9b69a

diff --git a/servo/components/style/properties/longhand/border.mako.rs b/servo/components/style/properties/longhand/border.mako.rs
--- a/servo/components/style/properties/longhand/border.mako.rs
+++ b/servo/components/style/properties/longhand/border.mako.rs
@@ -69,16 +69,17 @@
 
 /// -moz-border-*-colors: color, string, enum, none, inherit/initial
 /// These non-spec properties are just for Gecko (Stylo) internal use.
 % for side in PHYSICAL_SIDES:
     <%helpers:longhand name="-moz-border-${side}-colors" animation_value_type="discrete"
                        spec="Nonstandard (https://developer.mozilla.org/en-US/docs/Web/CSS/-moz-border-*-colors)"
                        products="gecko"
                        flags="APPLIES_TO_FIRST_LETTER"
+                       enabled_in="chrome"
                        ignored_when_colors_disabled="True">
         use std::fmt;
         use style_traits::ToCss;
         use values::specified::RGBAColor;
 
         pub mod computed_value {
             use cssparser::RGBA;
             #[derive(Clone, Debug, MallocSizeOf, PartialEq)]
diff --git a/servo/components/style/properties/shorthand/border.mako.rs b/servo/components/style/properties/shorthand/border.mako.rs
--- a/servo/components/style/properties/shorthand/border.mako.rs
+++ b/servo/components/style/properties/shorthand/border.mako.rs
@@ -126,40 +126,30 @@ pub fn parse_border<'i, 't>(context: &Pa
     </%helpers:shorthand>
 % endfor
 
 <%helpers:shorthand name="border"
     sub_properties="${' '.join('border-%s-%s' % (side, prop)
         for side in PHYSICAL_SIDES
         for prop in ['color', 'style', 'width'])}
         ${' '.join('border-image-%s' % name
-        for name in ['outset', 'repeat', 'slice', 'source', 'width'])}
-        ${' '.join('-moz-border-%s-colors' % side
-        for side in PHYSICAL_SIDES) if product == 'gecko' else ''}"
+        for name in ['outset', 'repeat', 'slice', 'source', 'width'])}"
     spec="https://drafts.csswg.org/css-backgrounds/#border">
 
-    % if product == "gecko":
-        use properties::longhands::{_moz_border_top_colors, _moz_border_right_colors,
-                                    _moz_border_bottom_colors, _moz_border_left_colors};
-    % endif
-
     pub fn parse_value<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
                                -> Result<Longhands, ParseError<'i>> {
         use properties::longhands::{border_image_outset, border_image_repeat, border_image_slice};
         use properties::longhands::{border_image_source, border_image_width};
 
         let (color, style, width) = super::parse_border(context, input)?;
         Ok(expanded! {
             % for side in PHYSICAL_SIDES:
                 border_${side}_color: color.clone(),
                 border_${side}_style: style,
                 border_${side}_width: width.clone(),
-                % if product == "gecko":
-                    _moz_border_${side}_colors: _moz_border_${side}_colors::get_initial_specified_value(),
-                % endif
             % endfor
 
             // The ‘border’ shorthand resets ‘border-image’ to its initial value.
             // See https://drafts.csswg.org/css-backgrounds-3/#the-border-shorthands
             % for name in "outset repeat slice source width".split():
                 border_image_${name}: border_image_${name}::get_initial_specified_value(),
             % endfor
         })
