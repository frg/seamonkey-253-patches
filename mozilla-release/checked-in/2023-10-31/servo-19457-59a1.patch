# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1512466638 21600
# Node ID 43e678a70a2a7175b59ebd5e84ab6e071ec9e009
# Parent  f0b0d756e6c77591bbb8150085d9cc1a8b14b0c9
servo: Merge #19457 - style: support calc() in color functions (from emilio:color-calc); r=SimonSapin

Depends on #19456 and https://github.com/servo/rust-cssparser/pull/207.

Fixes https://bugzilla.mozilla.org/show_bug.cgi?id=984021

Source-Repo: https://github.com/servo/servo
Source-Revision: 3cef09ae217ece7fa276d1be653c7c36dee7febc

diff --git a/servo/components/malloc_size_of/Cargo.toml b/servo/components/malloc_size_of/Cargo.toml
--- a/servo/components/malloc_size_of/Cargo.toml
+++ b/servo/components/malloc_size_of/Cargo.toml
@@ -8,17 +8,17 @@ publish = false
 [lib]
 path = "lib.rs"
 
 [features]
 servo = ["mozjs", "string_cache", "url", "webrender_api", "xml5ever"]
 
 [dependencies]
 app_units = "0.5.5"
-cssparser = "0.22.0"
+cssparser = "0.23.0"
 euclid = "0.15"
 hashglobe = { path = "../hashglobe" }
 mozjs = { version = "0.1.8", features = ["promises"], optional = true }
 servo_arc = { path = "../servo_arc" }
 smallbitvec = "1.0.3"
 smallvec = "0.4"
 string_cache = { version = "0.6", optional = true }
 url = { version = "1.2", optional = true }
diff --git a/servo/components/selectors/Cargo.toml b/servo/components/selectors/Cargo.toml
--- a/servo/components/selectors/Cargo.toml
+++ b/servo/components/selectors/Cargo.toml
@@ -20,17 +20,17 @@ doctest = false
 
 [features]
 gecko_like_types = []
 bench = []
 
 [dependencies]
 bitflags = "1.0"
 matches = "0.1"
-cssparser = "0.22.0"
+cssparser = "0.23.0"
 log = "0.3"
 fnv = "1.0"
 malloc_size_of = { path = "../malloc_size_of" }
 malloc_size_of_derive = { path = "../malloc_size_of_derive" }
 phf = "0.7.18"
 precomputed-hash = "0.1"
 servo_arc = { path = "../servo_arc" }
 smallvec = "0.4"
diff --git a/servo/components/style/Cargo.toml b/servo/components/style/Cargo.toml
--- a/servo/components/style/Cargo.toml
+++ b/servo/components/style/Cargo.toml
@@ -30,17 +30,17 @@ gecko_debug = ["nsstring/gecko_debug"]
 
 [dependencies]
 app_units = "0.5.6"
 arrayvec = "0.3.20"
 atomic_refcell = "0.1"
 bitflags = "1.0"
 byteorder = "1.0"
 cfg-if = "0.1.0"
-cssparser = "0.22.0"
+cssparser = "0.23.0"
 encoding_rs = {version = "0.7", optional = true}
 euclid = "0.15"
 fallible = { path = "../fallible" }
 fnv = "1.0"
 hashglobe = { path = "../hashglobe" }
 html5ever = {version = "0.21", optional = true}
 itertools = "0.5"
 itoa = "0.3"
diff --git a/servo/components/style/error_reporting.rs b/servo/components/style/error_reporting.rs
--- a/servo/components/style/error_reporting.rs
+++ b/servo/components/style/error_reporting.rs
@@ -71,17 +71,16 @@ impl<'a> fmt::Display for ContextualPars
                 Token::Colon => write!(f, "colon (:)"),
                 Token::Semicolon => write!(f, "semicolon (;)"),
                 Token::Comma => write!(f, "comma (,)"),
                 Token::IncludeMatch => write!(f, "include match (~=)"),
                 Token::DashMatch => write!(f, "dash match (|=)"),
                 Token::PrefixMatch => write!(f, "prefix match (^=)"),
                 Token::SuffixMatch => write!(f, "suffix match ($=)"),
                 Token::SubstringMatch => write!(f, "substring match (*=)"),
-                Token::Column => write!(f, "column (||)"),
                 Token::CDO => write!(f, "CDO (<!--)"),
                 Token::CDC => write!(f, "CDC (-->)"),
                 Token::Function(ref name) => write!(f, "function {}", name),
                 Token::ParenthesisBlock => write!(f, "parenthesis ("),
                 Token::SquareBracketBlock => write!(f, "square bracket ["),
                 Token::CurlyBracketBlock => write!(f, "curly bracket {{"),
                 Token::BadUrl(ref _u) => write!(f, "bad url parse error"),
                 Token::BadString(ref _s) => write!(f, "bad string parse error"),
diff --git a/servo/components/style/values/specified/angle.rs b/servo/components/style/values/specified/angle.rs
--- a/servo/components/style/values/specified/angle.rs
+++ b/servo/components/style/values/specified/angle.rs
@@ -76,16 +76,23 @@ impl Angle {
     }
 
     /// Returns the amount of radians this angle represents.
     #[inline]
     pub fn radians(self) -> f32 {
         self.value.radians()
     }
 
+    /// Returns the amount of degrees this angle represents.
+    #[inline]
+    pub fn degrees(self) -> f32 {
+        use std::f32::consts::PI;
+        self.radians() * 360. / (2. * PI)
+    }
+
     /// Returns `0deg`.
     pub fn zero() -> Self {
         Self::from_degrees(0.0, false)
     }
 
     /// Returns an `Angle` parsed from a `calc()` expression.
     pub fn from_calc(radians: CSSFloat) -> Self {
         Angle {
diff --git a/servo/components/style/values/specified/calc.rs b/servo/components/style/values/specified/calc.rs
--- a/servo/components/style/values/specified/calc.rs
+++ b/servo/components/style/values/specified/calc.rs
@@ -1,17 +1,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! [Calc expressions][calc].
 //!
 //! [calc]: https://drafts.csswg.org/css-values/#calc-notation
 
-use cssparser::{Parser, Token};
+use cssparser::{Parser, Token, NumberOrPercentage, AngleOrNumber};
 use parser::ParserContext;
 #[allow(unused_imports)] use std::ascii::AsciiExt;
 use std::fmt;
 use style_traits::{ToCss, ParseError, StyleParseErrorKind};
 use style_traits::values::specified::AllowedNumericType;
 use values::{CSSInteger, CSSFloat};
 use values::computed;
 use values::specified::{Angle, Time};
@@ -197,19 +197,18 @@ impl CalcNode {
     }
 
     /// Parse a top-level `calc` expression, with all nested sub-expressions.
     ///
     /// This is in charge of parsing, for example, `2 + 3 * 100%`.
     fn parse<'i, 't>(
         context: &ParserContext,
         input: &mut Parser<'i, 't>,
-        expected_unit: CalcUnit)
-        -> Result<Self, ParseError<'i>>
-    {
+        expected_unit: CalcUnit,
+    ) -> Result<Self, ParseError<'i>> {
         let mut root = Self::parse_product(context, input, expected_unit)?;
 
         loop {
             let start = input.state();
             match input.next_including_whitespace() {
                 Ok(&Token::WhiteSpace(_)) => {
                     if input.is_exhausted() {
                         break; // allow trailing whitespace
@@ -608,9 +607,44 @@ impl CalcNode {
     pub fn parse_time<'i, 't>(
         context: &ParserContext,
         input: &mut Parser<'i, 't>
     ) -> Result<Time, ParseError<'i>> {
         Self::parse(context, input, CalcUnit::Time)?
             .to_time()
             .map_err(|()| input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
     }
+
+    /// Convenience parsing function for `<number>` or `<percentage>`.
+    pub fn parse_number_or_percentage<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>
+    ) -> Result<NumberOrPercentage, ParseError<'i>> {
+        let node = Self::parse(context, input, CalcUnit::Percentage)?;
+
+        if let Ok(value) = node.to_number() {
+            return Ok(NumberOrPercentage::Number { value })
+        }
+
+        match node.to_percentage() {
+            Ok(unit_value) => Ok(NumberOrPercentage::Percentage { unit_value }),
+            Err(()) => Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError)),
+        }
+    }
+
+    /// Convenience parsing function for `<number>` or `<angle>`.
+    pub fn parse_angle_or_number<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>
+    ) -> Result<AngleOrNumber, ParseError<'i>> {
+        let node = Self::parse(context, input, CalcUnit::Angle)?;
+
+        if let Ok(angle) = node.to_angle() {
+            let degrees = angle.degrees();
+            return Ok(AngleOrNumber::Angle { degrees })
+        }
+
+        match node.to_number() {
+            Ok(value) => Ok(AngleOrNumber::Number { value }),
+            Err(()) => Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError)),
+        }
+    }
 }
diff --git a/servo/components/style/values/specified/color.rs b/servo/components/style/values/specified/color.rs
--- a/servo/components/style/values/specified/color.rs
+++ b/servo/components/style/values/specified/color.rs
@@ -1,26 +1,28 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Specified color values.
 
-use cssparser::{Color as CSSParserColor, Parser, RGBA, Token, BasicParseError, BasicParseErrorKind};
+use cssparser::{AngleOrNumber, Color as CSSParserColor, Parser, RGBA, Token};
+use cssparser::{BasicParseErrorKind, NumberOrPercentage, ParseErrorKind};
 #[cfg(feature = "gecko")]
 use gecko_bindings::structs::nscolor;
 use itoa;
 use parser::{ParserContext, Parse};
 #[cfg(feature = "gecko")]
 use properties::longhands::system_colors::SystemColor;
 use std::fmt;
 use std::io::Write;
 use style_traits::{ToCss, ParseError, StyleParseErrorKind, ValueParseErrorKind};
 use super::AllowQuirks;
 use values::computed::{Color as ComputedColor, Context, ToComputedValue};
+use values::specified::calc::CalcNode;
 
 /// Specified color value
 #[derive(Clone, Debug, MallocSizeOf, PartialEq)]
 pub enum Color {
     /// The 'currentColor' keyword
     CurrentColor,
     /// A specific RGBA color
     Numeric {
@@ -38,17 +40,16 @@ pub enum Color {
     /// A special color keyword value used in Gecko
     #[cfg(feature = "gecko")]
     Special(gecko::SpecialColorKeyword),
     /// Quirksmode-only rule for inheriting color from the body
     #[cfg(feature = "gecko")]
     InheritFromBodyQuirk,
 }
 
-
 #[cfg(feature = "gecko")]
 mod gecko {
     define_css_keyword_enum! { SpecialColorKeyword:
         "-moz-default-color" => MozDefaultColor,
         "-moz-default-background-color" => MozDefaultBackgroundColor,
         "-moz-hyperlinktext" => MozHyperlinktext,
         "-moz-activehyperlinktext" => MozActiveHyperlinktext,
         "-moz-visitedhyperlinktext" => MozVisitedHyperlinktext,
@@ -56,19 +57,137 @@ mod gecko {
 }
 
 impl From<RGBA> for Color {
     fn from(value: RGBA) -> Self {
         Color::rgba(value)
     }
 }
 
+struct ColorComponentParser<'a, 'b: 'a>(&'a ParserContext<'b>);
+impl<'a, 'b: 'a, 'i: 'a> ::cssparser::ColorComponentParser<'i> for ColorComponentParser<'a, 'b> {
+    type Error = StyleParseErrorKind<'i>;
+
+    fn parse_angle_or_number<'t>(
+        &self,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<AngleOrNumber, ParseError<'i>> {
+        #[allow(unused_imports)] use std::ascii::AsciiExt;
+        use values::specified::Angle;
+
+        let location = input.current_source_location();
+        let token = input.next()?.clone();
+        match token {
+            Token::Dimension { value, ref unit, .. } => {
+                let angle = Angle::parse_dimension(
+                    value,
+                    unit,
+                    /* from_calc = */ false,
+                );
+
+                let degrees = match angle {
+                    Ok(angle) => angle.degrees(),
+                    Err(()) => return Err(location.new_unexpected_token_error(token.clone())),
+                };
+
+                Ok(AngleOrNumber::Angle { degrees })
+            }
+            Token::Number { value, .. } => {
+                Ok(AngleOrNumber::Number { value })
+            }
+            Token::Function(ref name) if name.eq_ignore_ascii_case("calc") => {
+                input.parse_nested_block(|i| CalcNode::parse_angle_or_number(self.0, i))
+            }
+            t => return Err(location.new_unexpected_token_error(t)),
+        }
+    }
+
+    fn parse_percentage<'t>(
+        &self,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<f32, ParseError<'i>> {
+        use values::specified::Percentage;
+
+        Ok(Percentage::parse(self.0, input)?.get())
+    }
+
+    fn parse_number<'t>(
+        &self,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<f32, ParseError<'i>> {
+        use values::specified::Number;
+
+        Ok(Number::parse(self.0, input)?.get())
+    }
+
+    fn parse_number_or_percentage<'t>(
+        &self,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<NumberOrPercentage, ParseError<'i>> {
+        #[allow(unused_imports)] use std::ascii::AsciiExt;
+
+        let location = input.current_source_location();
+
+        match input.next()?.clone() {
+            Token::Number { value, .. } => Ok(NumberOrPercentage::Number { value }),
+            Token::Percentage { unit_value, .. } => {
+                Ok(NumberOrPercentage::Percentage { unit_value })
+            },
+            Token::Function(ref name) if name.eq_ignore_ascii_case("calc") => {
+                input.parse_nested_block(|i| CalcNode::parse_number_or_percentage(self.0, i))
+            }
+            t => return Err(location.new_unexpected_token_error(t))
+        }
+    }
+}
+
 impl Parse for Color {
-    fn parse<'i, 't>(_: &ParserContext, input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
-        Color::parse_color(input)
+    fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
+        #[allow(unused_imports)] use std::ascii::AsciiExt;
+
+        // Currently we only store authored value for color keywords,
+        // because all browsers serialize those values as keywords for
+        // specified value.
+        let start = input.state();
+        let authored = input.expect_ident_cloned().ok();
+        input.reset(&start);
+
+        let compontent_parser = ColorComponentParser(&*context);
+        match input.try(|i| CSSParserColor::parse_with(&compontent_parser, i)) {
+            Ok(value) => {
+                Ok(match value {
+                    CSSParserColor::CurrentColor => Color::CurrentColor,
+                    CSSParserColor::RGBA(rgba) => Color::Numeric {
+                        parsed: rgba,
+                        authored: authored.map(|s| s.to_ascii_lowercase().into_boxed_str()),
+                    },
+                })
+            }
+            Err(e) => {
+                #[cfg(feature = "gecko")]
+                {
+                    if let Ok(system) = input.try(SystemColor::parse) {
+                        return Ok(Color::System(system));
+                    }
+
+                    if let Ok(c) = gecko::SpecialColorKeyword::parse(input) {
+                        return Ok(Color::Special(c));
+                    }
+                }
+
+                match e.kind {
+                    ParseErrorKind::Basic(BasicParseErrorKind::UnexpectedToken(t)) => {
+                        Err(e.location.new_custom_error(
+                            StyleParseErrorKind::ValueError(ValueParseErrorKind::InvalidColor(t))
+                        ))
+                    }
+                    _ => Err(e)
+                }
+            }
+        }
     }
 }
 
 impl ToCss for Color {
     fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
         match *self {
             Color::CurrentColor => CSSParserColor::CurrentColor.to_css(dest),
             Color::Numeric { authored: Some(ref authored), .. } => dest.write_str(authored),
@@ -81,19 +200,19 @@ impl ToCss for Color {
             #[cfg(feature = "gecko")]
             Color::InheritFromBodyQuirk => Ok(()),
         }
     }
 }
 
 /// A wrapper of cssparser::Color::parse_hash.
 ///
-/// That function should never return CurrentColor, so it makes no sense
-/// to handle a cssparser::Color here. This should really be done in
-/// cssparser directly rather than here.
+/// That function should never return CurrentColor, so it makes no sense to
+/// handle a cssparser::Color here. This should really be done in cssparser
+/// directly rather than here.
 fn parse_hash_color(value: &[u8]) -> Result<RGBA, ()> {
     CSSParserColor::parse_hash(value).map(|color| {
         match color {
             CSSParserColor::RGBA(rgba) => rgba,
             CSSParserColor::CurrentColor =>
                 unreachable!("parse_hash should never return currentcolor"),
         }
     })
@@ -120,26 +239,27 @@ impl Color {
             parsed: rgba,
             authored: None,
         }
     }
 
     /// Parse a color, with quirks.
     ///
     /// <https://quirks.spec.whatwg.org/#the-hashless-hex-color-quirk>
-    pub fn parse_quirky<'i, 't>(context: &ParserContext,
-                                input: &mut Parser<'i, 't>,
-                                allow_quirks: AllowQuirks)
-                                -> Result<Self, ParseError<'i>> {
+    pub fn parse_quirky<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+        allow_quirks: AllowQuirks,
+    ) -> Result<Self, ParseError<'i>> {
         input.try(|i| Self::parse(context, i)).or_else(|e| {
             if !allow_quirks.allowed(context.quirks_mode) {
                 return Err(e);
             }
             Color::parse_quirky_color(input)
-                .map(|rgba| Color::rgba(rgba))
+                .map(Color::rgba)
                 .map_err(|_| e)
         })
     }
 
     /// Parse a <quirky-color> value.
     ///
     /// <https://quirks.spec.whatwg.org/#the-hashless-hex-color-quirk>
     fn parse_quirky_color<'i, 't>(input: &mut Parser<'i, 't>) -> Result<RGBA, ParseError<'i>> {
@@ -200,62 +320,16 @@ impl Color {
     /// Returns false if the color is completely transparent, and
     /// true otherwise.
     pub fn is_non_transparent(&self) -> bool {
         match *self {
             Color::Numeric { ref parsed, .. } => parsed.alpha != 0,
             _ => true,
         }
     }
-
-    /// Parse a <color> value.
-    pub fn parse_color<'i, 't>(input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
-        // Currently we only store authored value for color keywords,
-        // because all browsers serialize those values as keywords for
-        // specified value.
-        let start = input.state();
-        let authored = match input.next() {
-            Ok(&Token::Ident(ref s)) => Some(s.clone()),
-            _ => None,
-        };
-        input.reset(&start);
-        match input.try(CSSParserColor::parse) {
-            Ok(value) =>
-                Ok(match value {
-                    CSSParserColor::CurrentColor => Color::CurrentColor,
-                    CSSParserColor::RGBA(rgba) => {
-                        Color::Numeric {
-                            parsed: rgba,
-                            authored: authored.map(|s| s.to_lowercase().into_boxed_str()),
-                        }
-                    }
-                }),
-            Err(e) => {
-                #[cfg(feature = "gecko")]
-                {
-                    if let Ok(system) = input.try(SystemColor::parse) {
-                        return Ok(Color::System(system));
-                    }
-
-                    if let Ok(c) = gecko::SpecialColorKeyword::parse(input) {
-                        return Ok(Color::Special(c));
-                    }
-                }
-
-                match e {
-                    BasicParseError { kind: BasicParseErrorKind::UnexpectedToken(t), location } => {
-                        Err(location.new_custom_error(
-                            StyleParseErrorKind::ValueError(ValueParseErrorKind::InvalidColor(t))
-                        ))
-                    }
-                    e => Err(e.into())
-                }
-            }
-        }
-    }
 }
 
 #[cfg(feature = "gecko")]
 fn convert_nscolor_to_computedcolor(color: nscolor) -> ComputedColor {
     use gecko::values::convert_nscolor_to_rgba;
     ComputedColor::rgba(convert_nscolor_to_rgba(color))
 }
 
diff --git a/servo/components/style_traits/Cargo.toml b/servo/components/style_traits/Cargo.toml
--- a/servo/components/style_traits/Cargo.toml
+++ b/servo/components/style_traits/Cargo.toml
@@ -10,17 +10,17 @@ name = "style_traits"
 path = "lib.rs"
 
 [features]
 servo = ["serde", "servo_atoms", "cssparser/serde", "webrender_api"]
 gecko = []
 
 [dependencies]
 app_units = "0.5"
-cssparser = "0.22.0"
+cssparser = "0.23.0"
 bitflags = "1.0"
 euclid = "0.15"
 malloc_size_of = { path = "../malloc_size_of" }
 malloc_size_of_derive = { path = "../malloc_size_of_derive" }
 selectors = { path = "../selectors" }
 serde = {version = "1.0", optional = true}
 webrender_api = {git = "https://github.com/servo/webrender", optional = true}
 servo_atoms = {path = "../atoms", optional = true}
diff --git a/servo/ports/geckolib/Cargo.toml b/servo/ports/geckolib/Cargo.toml
--- a/servo/ports/geckolib/Cargo.toml
+++ b/servo/ports/geckolib/Cargo.toml
@@ -10,17 +10,17 @@ path = "lib.rs"
 crate-type = ["staticlib", "rlib"]
 
 [features]
 bindgen = ["style/use_bindgen"]
 gecko_debug = ["style/gecko_debug"]
 
 [dependencies]
 atomic_refcell = "0.1"
-cssparser = "0.22.0"
+cssparser = "0.23.0"
 env_logger = {version = "0.4", default-features = false} # disable `regex` to reduce code size
 libc = "0.2"
 log = {version = "0.3.5", features = ["release_max_level_info"]}
 malloc_size_of = {path = "../../components/malloc_size_of"}
 nsstring = {path = "../../support/gecko/nsstring"}
 parking_lot = "0.4"
 # Turn on gecko_like_types because of so that crates which use this
 # crate and also dev-depend on stylo_tests get reasonable behavior
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -4558,18 +4558,27 @@ pub unsafe extern "C" fn Servo_SelectorL
 }
 
 fn parse_color(
     value: &str,
     error_reporter: Option<&ErrorReporter>,
 ) -> Result<specified::Color, ()> {
     let mut input = ParserInput::new(value);
     let mut parser = Parser::new(&mut input);
+    let url_data = unsafe { dummy_url_data() };
+    let context = ParserContext::new(
+        Origin::Author,
+        url_data,
+        Some(CssRuleType::Style),
+        ParsingMode::DEFAULT,
+        QuirksMode::NoQuirks,
+    );
+
     let start_position = parser.position();
-    parser.parse_entirely(specified::Color::parse_color).map_err(|err| {
+    parser.parse_entirely(|i| specified::Color::parse(&context, i)).map_err(|err| {
         if let Some(error_reporter) = error_reporter {
             match err.kind {
                 ParseErrorKind::Custom(StyleParseErrorKind::ValueError(..)) => {
                     let location = err.location.clone();
                     let error = ContextualParseError::UnsupportedValue(
                         parser.slice_from(start_position),
                         err,
                     );
diff --git a/servo/tests/unit/style/Cargo.toml b/servo/tests/unit/style/Cargo.toml
--- a/servo/tests/unit/style/Cargo.toml
+++ b/servo/tests/unit/style/Cargo.toml
@@ -7,17 +7,17 @@ license = "MPL-2.0"
 [lib]
 name = "style_tests"
 path = "lib.rs"
 doctest = false
 
 [dependencies]
 byteorder = "1.0"
 app_units = "0.5"
-cssparser = "0.22.0"
+cssparser = "0.23.0"
 euclid = "0.15"
 html5ever = "0.21"
 parking_lot = "0.4"
 rayon = "0.8"
 rustc-serialize = "0.3"
 selectors = {path = "../../../components/selectors"}
 servo_arc = {path = "../../../components/servo_arc"}
 size_of_test = {path = "../../../components/size_of_test"}
