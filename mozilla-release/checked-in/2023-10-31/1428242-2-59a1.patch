# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1515120494 -28800
#      Fri Jan 05 10:48:14 2018 +0800
# Node ID 90ffd059bdfa679780d7d4dd99a259e0f457546b
# Parent  a5f886553c4beaea9816d24058a797a033b3e804
Bug 1428242. P2 - MediaCache::ReadCacheFile() doesn't need to drop the cache monitor. r=bechen,gerald

Since we now never take the lock on the main thread, we can safely do file IO
while holding the lock without blocking the main thread.

This reverts the change of Bug 1354389 P1.

MozReview-Commit-ID: EhEwTjINQIT

diff --git a/dom/media/MediaCache.cpp b/dom/media/MediaCache.cpp
--- a/dom/media/MediaCache.cpp
+++ b/dom/media/MediaCache.cpp
@@ -812,27 +812,20 @@ MediaCache::GetMediaCache(int64_t aConte
 
 nsresult
 MediaCache::ReadCacheFile(AutoLock&,
                           int64_t aOffset,
                           void* aData,
                           int32_t aLength,
                           int32_t* aBytes)
 {
-  RefPtr<MediaBlockCacheBase> blockCache = mBlockCache;
-  if (!blockCache) {
+  if (!mBlockCache) {
     return NS_ERROR_FAILURE;
   }
-  {
-    // Since the monitor might be acquired on the main thread, we need to drop
-    // the monitor while doing IO in order not to block the main thread.
-    AutoUnlock unlock(mMonitor);
-    return blockCache->Read(
-      aOffset, reinterpret_cast<uint8_t*>(aData), aLength, aBytes);
-  }
+  return mBlockCache->Read(aOffset, reinterpret_cast<uint8_t*>(aData), aLength, aBytes);
 }
 
 // Allowed range is whatever can be accessed with an int32_t block index.
 static bool
 IsOffsetAllowed(int64_t aOffset)
 {
   return aOffset < (int64_t(INT32_MAX) + 1) * MediaCache::BLOCK_SIZE &&
          aOffset >= 0;
