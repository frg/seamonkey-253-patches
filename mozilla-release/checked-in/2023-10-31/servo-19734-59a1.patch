# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1515562455 21600
# Node ID bc9d1434a49833a499a7ad68a717c1e3e638ca21
# Parent  790d430658aba29be6c5b710f5238961eb72f49b
servo: Merge #19734 - style: Remove some unneeded indirection (from emilio:less-indirection); r=mbrubeck

All TElement's implement Copy, and are just pointers, so the double indirection
is stupid.

I'm going to try to see if removing this double-indirection fixes some
selector-matching performance, and this is a trivial pre-requisite while I wait
for Talos results.

Source-Repo: https://github.com/servo/servo
Source-Revision: e2c89df8eeb5f2dbac1436335aea52099a622d0d

diff --git a/servo/components/style/selector_map.rs b/servo/components/style/selector_map.rs
--- a/servo/components/style/selector_map.rs
+++ b/servo/components/style/selector_map.rs
@@ -150,18 +150,18 @@ impl<T: 'static> SelectorMap<T> {
 
 impl SelectorMap<Rule> {
     /// Append to `rule_list` all Rules in `self` that match element.
     ///
     /// Extract matching rules as per element's ID, classes, tag name, etc..
     /// Sort the Rules at the end to maintain cascading order.
     pub fn get_all_matching_rules<E, F>(
         &self,
-        element: &E,
-        rule_hash_target: &E,
+        element: E,
+        rule_hash_target: E,
         matching_rules_list: &mut ApplicableDeclarationList,
         context: &mut MatchingContext<E::Impl>,
         quirks_mode: QuirksMode,
         flags_setter: &mut F,
         cascade_level: CascadeLevel,
     )
     where
         E: TElement,
@@ -212,32 +212,32 @@ impl SelectorMap<Rule> {
                                         cascade_level);
 
         // Sort only the rules we just added.
         matching_rules_list[init_len..].sort_unstable_by_key(|block| (block.specificity, block.source_order()));
     }
 
     /// Adds rules in `rules` that match `element` to the `matching_rules` list.
     fn get_matching_rules<E, F>(
-        element: &E,
+        element: E,
         rules: &[Rule],
         matching_rules: &mut ApplicableDeclarationList,
         context: &mut MatchingContext<E::Impl>,
         flags_setter: &mut F,
         cascade_level: CascadeLevel,
     )
     where
         E: TElement,
         F: FnMut(&E, ElementSelectorFlags),
     {
         for rule in rules {
             if matches_selector(&rule.selector,
                                 0,
                                 Some(&rule.hashes),
-                                element,
+                                &element,
                                 context,
                                 flags_setter) {
                 matching_rules.push(
                     rule.to_applicable_declaration_block(cascade_level));
             }
         }
     }
 }
diff --git a/servo/components/style/style_resolver.rs b/servo/components/style/style_resolver.rs
--- a/servo/components/style/style_resolver.rs
+++ b/servo/components/style/style_resolver.rs
@@ -423,17 +423,17 @@ where
         {
             let resolving_element = self.element;
             let mut set_selector_flags = |element: &E, flags: ElementSelectorFlags| {
                 resolving_element.apply_selector_flags(map, element, flags);
             };
 
             // Compute the primary rule node.
             stylist.push_applicable_declarations(
-                &self.element,
+                self.element,
                 implemented_pseudo.as_ref(),
                 self.element.style_attribute(),
                 self.element.get_smil_override(),
                 self.element.get_animation_rules(),
                 self.rule_inclusion,
                 &mut applicable_declarations,
                 &mut matching_context,
                 &mut set_selector_flags,
@@ -497,17 +497,17 @@ where
         let resolving_element = self.element;
         let mut set_selector_flags = |element: &E, flags: ElementSelectorFlags| {
             resolving_element.apply_selector_flags(map, element, flags);
         };
 
         // NB: We handle animation rules for ::before and ::after when
         // traversing them.
         stylist.push_applicable_declarations(
-            &self.element,
+            self.element,
             Some(pseudo_element),
             None,
             None,
             AnimationRules(None, None),
             self.rule_inclusion,
             &mut applicable_declarations,
             &mut matching_context,
             &mut set_selector_flags
diff --git a/servo/components/style/stylist.rs b/servo/components/style/stylist.rs
--- a/servo/components/style/stylist.rs
+++ b/servo/components/style/stylist.rs
@@ -790,17 +790,17 @@ impl Stylist {
     /// This can only be done for a certain set of pseudo-elements, like
     /// :selection.
     ///
     /// Check the documentation on lazy pseudo-elements in
     /// docs/components/style.md
     pub fn lazily_compute_pseudo_element_style<E>(
         &self,
         guards: &StylesheetGuards,
-        element: &E,
+        element: E,
         pseudo: &PseudoElement,
         rule_inclusion: RuleInclusion,
         parent_style: &ComputedValues,
         is_probe: bool,
         font_metrics: &FontMetricsProvider,
         matching_fn: Option<&Fn(&PseudoElement) -> bool>,
     ) -> Option<Arc<ComputedValues>>
     where
@@ -977,17 +977,17 @@ impl Stylist {
 
     /// Computes the cascade inputs for a lazily-cascaded pseudo-element.
     ///
     /// See the documentation on lazy pseudo-elements in
     /// docs/components/style.md
     fn lazy_pseudo_rules<E>(
         &self,
         guards: &StylesheetGuards,
-        element: &E,
+        element: E,
         parent_style: &ComputedValues,
         pseudo: &PseudoElement,
         is_probe: bool,
         rule_inclusion: RuleInclusion,
         matching_fn: Option<&Fn(&PseudoElement) -> bool>,
     ) -> CascadeInputs
     where
         E: TElement
@@ -1198,17 +1198,17 @@ impl Stylist {
         self.quirks_mode = quirks_mode;
     }
 
     /// Returns the applicable CSS declarations for the given element.
     ///
     /// This corresponds to `ElementRuleCollector` in WebKit.
     pub fn push_applicable_declarations<E, F>(
         &self,
-        element: &E,
+        element: E,
         pseudo_element: Option<&PseudoElement>,
         style_attribute: Option<ArcBorrow<Locked<PropertyDeclarationBlock>>>,
         smil_override: Option<ArcBorrow<Locked<PropertyDeclarationBlock>>>,
         animation_rules: AnimationRules,
         rule_inclusion: RuleInclusion,
         applicable_declarations: &mut ApplicableDeclarationList,
         context: &mut MatchingContext<E::Impl>,
         flags_setter: &mut F,
@@ -1233,17 +1233,17 @@ impl Stylist {
             rule_inclusion == RuleInclusion::DefaultOnly;
         let matches_user_and_author_rules =
             rule_hash_target.matches_user_and_author_rules();
 
         // Step 1: Normal user-agent rules.
         if let Some(map) = self.cascade_data.user_agent.cascade_data.normal_rules(pseudo_element) {
             map.get_all_matching_rules(
                 element,
-                &rule_hash_target,
+                rule_hash_target,
                 applicable_declarations,
                 context,
                 self.quirks_mode,
                 flags_setter,
                 CascadeLevel::UANormal
             );
         }
 
@@ -1271,17 +1271,17 @@ impl Stylist {
         //      rule_hash_target.matches_user_and_author_rules())
         //
         // Which may be more what you would probably expect.
         if matches_user_and_author_rules {
             // Step 3a: User normal rules.
             if let Some(map) = self.cascade_data.user.normal_rules(pseudo_element) {
                 map.get_all_matching_rules(
                     element,
-                    &rule_hash_target,
+                    rule_hash_target,
                     applicable_declarations,
                     context,
                     self.quirks_mode,
                     flags_setter,
                     CascadeLevel::UserNormal,
                 );
             }
         }
@@ -1302,17 +1302,17 @@ impl Stylist {
                 current = slot.assigned_slot();
             }
 
             for slot in slots.iter().rev() {
                 slot.each_xbl_stylist(|stylist| {
                     if let Some(map) = stylist.cascade_data.author.slotted_rules(pseudo_element) {
                         map.get_all_matching_rules(
                             element,
-                            &rule_hash_target,
+                            rule_hash_target,
                             applicable_declarations,
                             context,
                             self.quirks_mode,
                             flags_setter,
                             CascadeLevel::AuthorNormal
                         );
                     }
                 });
@@ -1336,34 +1336,34 @@ impl Stylist {
                     context.bloom_filter,
                     context.nth_index_cache.as_mut().map(|s| &mut **s),
                     stylist.quirks_mode,
                 );
                 matching_context.pseudo_element_matching_fn = context.pseudo_element_matching_fn;
 
                 map.get_all_matching_rules(
                     element,
-                    &rule_hash_target,
+                    rule_hash_target,
                     applicable_declarations,
                     &mut matching_context,
                     stylist.quirks_mode,
                     flags_setter,
                     CascadeLevel::AuthorNormal,
                 );
             }
         });
 
         if matches_user_and_author_rules && !only_default_rules &&
             !cut_off_inheritance
         {
             // Step 3c: Author normal rules.
             if let Some(map) = self.cascade_data.author.normal_rules(pseudo_element) {
                 map.get_all_matching_rules(
                     element,
-                    &rule_hash_target,
+                    rule_hash_target,
                     applicable_declarations,
                     context,
                     self.quirks_mode,
                     flags_setter,
                     CascadeLevel::AuthorNormal
                 );
             }
         }
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -2244,27 +2244,26 @@ fn get_pseudo_style(
                                   &**styles.primary()));
             let base = if pseudo.inherits_from_default_values() {
                 doc_data.default_computed_values()
             } else {
                 styles.primary()
             };
             let guards = StylesheetGuards::same(guard);
             let metrics = get_metrics_provider_for_product();
-            doc_data.stylist
-                .lazily_compute_pseudo_element_style(
-                    &guards,
-                    &element,
-                    &pseudo,
-                    rule_inclusion,
-                    base,
-                    is_probe,
-                    &metrics,
-                    matching_func,
-                )
+            doc_data.stylist.lazily_compute_pseudo_element_style(
+                &guards,
+                element,
+                &pseudo,
+                rule_inclusion,
+                base,
+                is_probe,
+                &metrics,
+                matching_func,
+            )
         },
     };
 
     if is_probe {
         return style;
     }
 
     Some(style.unwrap_or_else(|| {
