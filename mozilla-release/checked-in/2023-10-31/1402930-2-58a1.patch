# HG changeset patch
# User ZER0 <zer0.kaos@gmail.com>
# Date 1506362730 -7200
#      Mon Sep 25 20:05:30 2017 +0200
# Node ID 7f51f5b71274109bc2e903eacc302a72bc09ab11
# Parent  5953394ee488f779f656de3885f51b7edf8109f8
Bug 1402930 - Added Unit Test for Image's srcset scenario; r=bradwerth

MozReview-Commit-ID: CrTtkEKOkuW

diff --git a/dom/tests/mochitest/general/test_contentViewer_overrideDPPX.html b/dom/tests/mochitest/general/test_contentViewer_overrideDPPX.html
--- a/dom/tests/mochitest/general/test_contentViewer_overrideDPPX.html
+++ b/dom/tests/mochitest/general/test_contentViewer_overrideDPPX.html
@@ -5,28 +5,41 @@
   <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
   <script type="text/javascript" src="/tests/SimpleTest/EventUtils.js"></script>
   <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css">
 </head>
 
 <body>
 
 <iframe></iframe>
+<img>
 
 <script type="application/javascript">
 
 SimpleTest.waitForExplicitFinish();
 
 const frameWindow = document.querySelector("iframe").contentWindow;
+const image = document.querySelector("img");
 
 const originalDPR = window.devicePixelRatio;
 const originalZoom = SpecialPowers.getFullZoom(window);
 const dppx = originalDPR * 1.5;
 const zoom = originalZoom * 0.5;
 
+const imageSets = {
+  "1x"  : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA" +
+          "GUlEQVQ4jWP4z8DwnxLMMGrAqAGjBgwXAwAwxP4QWURl4wAAAABJRU5ErkJggg==",
+  "1.5x": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA" +
+          "GElEQVQ4jWNgaGD4TxEeNWDUgFEDhosBAOsIfxAZ/CYXAAAAAElFTkSuQmCC",
+  "2x"  : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA" +
+          "GElEQVQ4jWNgYPj/nzI8asCoAaMGDBMDADKm/hBZaHKGAAAAAElFTkSuQmCC",
+  "8x"  : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA" +
+          "GklEQVQ4jWP4f+D0f0oww6gBowaMGjBcDAAAskKJLhIvZvgAAAAASUVORK5CYII=",
+};
+
 const setOverrideDPPX = (value) => {
   if (value > 0) {
     info(`override window's dppx to ${value}`);
   } else {
     info(`restore window's dppx to default value`);
   }
 
   SpecialPowers.setOverrideDPPX(window, value);
@@ -321,16 +334,86 @@ const gTests = {
       frameStyle.remove();
 
       setOverrideDPPX(0);
 
       done();
     }, {once: true});
 
     frameWindow.location.reload(true);
+  },
+
+  "test overrideDPPX with srcset": async function (done) {
+    assertValuesAreInitial();
+
+    let loaded;
+
+    // Set the image srcset and default src.  This is delayed until this test so
+    // that dppx overrides in other test blocks don't trigger load event on the
+    // image.
+    loaded = new Promise(resolve => image.onload = resolve);
+    image.srcset = Object.entries(imageSets).map(v => v[1] + " " + v[0]).join(", ");
+    image.src = imageSets["1x"];
+    await loaded;
+
+    let originalSrc = image.currentSrc;
+
+    // Make sure to test some very large value that can't be the default density
+    // so that the first override will always trigger a load.
+    isnot(originalDPR, 8, "originalDPR differs from final test value");
+    loaded = new Promise(resolve => image.onload = resolve);
+    setOverrideDPPX(8);
+    await loaded;
+
+    is(image.currentSrc, imageSets["8x"],
+      "Image is properly set for 8dppx.");
+
+    loaded = new Promise(resolve => image.onload = resolve);
+    setOverrideDPPX(1);
+    await loaded;
+
+    is(image.currentSrc, imageSets["1x"],
+      "Image url is properly set for 1dppx.");
+
+    loaded = new Promise(resolve => image.onload = resolve);
+    setOverrideDPPX(1.5);
+    await loaded;
+
+    is(image.currentSrc, imageSets["1.5x"],
+      "Image url is properly set for 1.5dppx.");
+
+    loaded = new Promise(resolve => image.onload = resolve);
+    setOverrideDPPX(2);
+    await loaded;
+
+    is(image.currentSrc, imageSets["2x"],
+      "Image is properly set for 2dppx.");
+
+    // Make sure to test some very large value that can't be the default density
+    // so that resetting to the default will always trigger a load.
+    isnot(originalDPR, 8, "originalDPR differs from final test value");
+    loaded = new Promise(resolve => image.onload = resolve);
+    setOverrideDPPX(8);
+    await loaded;
+
+    is(image.currentSrc, imageSets["8x"],
+      "Image is properly set for 8dppx.");
+
+    loaded = new Promise(resolve => image.onload = resolve);
+    setOverrideDPPX(0);
+    await loaded;
+
+    is(image.currentSrc, originalSrc,
+      "Image is properly restored to the default value.");
+
+    // Clear sources so any future dppx test blocks don't trigger image loads.
+    image.srcset = "";
+    image.src = "";
+
+    done();
   }
 };
 
 function* runner(tests) {
   for (let name of Object.keys(tests)) {
     info(name);
     tests[name](next);
     yield undefined;
