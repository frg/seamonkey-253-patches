# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1509115963 -3600
#      Fri Oct 27 15:52:43 2017 +0100
# Node ID 9641170641e5c917996826780c61d151d4eec429
# Parent  2d986ed9e5ba326eaf16fe30cc45140d216cacf7
Bug 1411302 - Don't try and OOM test worker threads r=jandem

diff --git a/js/public/Utility.h b/js/public/Utility.h
--- a/js/public/Utility.h
+++ b/js/public/Utility.h
@@ -52,16 +52,19 @@ JS_Assert(const char* s, const char* fil
 #else
 
 namespace js {
 
 /*
  * Thread types are used to tag threads for certain kinds of testing (see
  * below), and also used to characterize threads in the thread scheduler (see
  * js/src/vm/HelperThreads.cpp).
+ *
+ * Please update oom::FirstThreadTypeToTest and oom::LastThreadTypeToTest when
+ * adding new thread types.
  */
 enum ThreadType {
     THREAD_TYPE_NONE = 0,       // 0
     THREAD_TYPE_COOPERATING,    // 1
     THREAD_TYPE_WASM,           // 2
     THREAD_TYPE_ION,            // 3
     THREAD_TYPE_PARSE,          // 4
     THREAD_TYPE_COMPRESS,       // 5
@@ -81,23 +84,32 @@ namespace oom {
  * OOM in certain helper threads more effective, we allow restricting the OOM
  * testing to a certain helper thread type. This allows us to fail e.g. in
  * off-thread script parsing without causing an OOM in the active thread first.
  *
  * Getter/Setter functions to encapsulate mozilla::ThreadLocal, implementation
  * is in jsutil.cpp.
  */
 # if defined(DEBUG) || defined(JS_OOM_BREAKPOINT)
+
+// Define the range of threads tested by simulated OOM testing and the
+// like. Testing worker threads is not supported.
+const ThreadType FirstThreadTypeToTest = THREAD_TYPE_COOPERATING;
+const ThreadType LastThreadTypeToTest = THREAD_TYPE_WASM_TIER2;
+
 extern bool InitThreadType(void);
 extern void SetThreadType(ThreadType);
 extern JS_FRIEND_API(uint32_t) GetThreadType(void);
+
 # else
+
 inline bool InitThreadType(void) { return true; }
 inline void SetThreadType(ThreadType t) {};
 inline uint32_t GetThreadType(void) { return 0; }
+
 # endif
 
 } /* namespace oom */
 } /* namespace js */
 
 # if defined(DEBUG) || defined(JS_OOM_BREAKPOINT)
 
 #ifdef JS_OOM_BREAKPOINT
diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -1584,23 +1584,23 @@ OOMTest(JSContext* cx, unsigned argc, Va
         args.rval().setUndefined();
         return true;
     }
 
     RootedFunction function(cx, &args[0].toObject().as<JSFunction>());
 
     bool verbose = EnvVarIsDefined("OOM_VERBOSE");
 
-    unsigned threadStart = THREAD_TYPE_COOPERATING;
-    unsigned threadEnd = THREAD_TYPE_MAX;
+    unsigned threadStart = oom::FirstThreadTypeToTest;
+    unsigned threadEnd = oom::LastThreadTypeToTest;
 
     // Test a single thread type if specified by the OOM_THREAD environment variable.
     int threadOption = 0;
     if (EnvVarAsInt("OOM_THREAD", &threadOption)) {
-        if (threadOption < THREAD_TYPE_COOPERATING || threadOption > THREAD_TYPE_MAX) {
+        if (threadOption < oom::FirstThreadTypeToTest || threadOption > oom::LastThreadTypeToTest) {
             JS_ReportErrorASCII(cx, "OOM_THREAD value out of range.");
             return false;
         }
 
         threadStart = threadOption;
         threadEnd = threadOption + 1;
     }
 
@@ -1727,23 +1727,23 @@ StackTest(JSContext* cx, unsigned argc, 
         args.rval().setUndefined();
         return true;
     }
 
     RootedFunction function(cx, &args[0].toObject().as<JSFunction>());
 
     bool verbose = EnvVarIsDefined("OOM_VERBOSE");
 
-    unsigned threadStart = THREAD_TYPE_COOPERATING;
-    unsigned threadEnd = THREAD_TYPE_MAX;
+    unsigned threadStart = oom::FirstThreadTypeToTest;
+    unsigned threadEnd = oom::LastThreadTypeToTest;
 
     // Test a single thread type if specified by the OOM_THREAD environment variable.
     int threadOption = 0;
     if (EnvVarAsInt("OOM_THREAD", &threadOption)) {
-        if (threadOption < THREAD_TYPE_COOPERATING || threadOption > THREAD_TYPE_MAX) {
+        if (threadOption < oom::FirstThreadTypeToTest || threadOption > oom::LastThreadTypeToTest) {
             JS_ReportErrorASCII(cx, "OOM_THREAD value out of range.");
             return false;
         }
 
         threadStart = threadOption;
         threadEnd = threadOption + 1;
     }
 
@@ -1859,23 +1859,23 @@ InterruptTest(JSContext* cx, unsigned ar
         args.rval().setUndefined();
         return true;
     }
 
     RootedFunction function(cx, &args[0].toObject().as<JSFunction>());
 
     bool verbose = EnvVarIsDefined("OOM_VERBOSE");
 
-    unsigned threadStart = THREAD_TYPE_COOPERATING;
-    unsigned threadEnd = THREAD_TYPE_MAX;
+    unsigned threadStart = oom::FirstThreadTypeToTest;
+    unsigned threadEnd = oom::LastThreadTypeToTest;
 
     // Test a single thread type if specified by the OOM_THREAD environment variable.
     int threadOption = 0;
     if (EnvVarAsInt("OOM_THREAD", &threadOption)) {
-        if (threadOption < THREAD_TYPE_COOPERATING || threadOption > THREAD_TYPE_MAX) {
+        if (threadOption < oom::FirstThreadTypeToTest || threadOption > oom::LastThreadTypeToTest) {
             JS_ReportErrorASCII(cx, "OOM_THREAD value out of range.");
             return false;
         }
 
         threadStart = threadOption;
         threadEnd = threadOption + 1;
     }
 
diff --git a/js/src/jit-test/tests/gc/bug-1411302.js b/js/src/jit-test/tests/gc/bug-1411302.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/gc/bug-1411302.js
@@ -0,0 +1,18 @@
+if (!('oomTest' in this))
+    quit();
+
+let lfPreamble = `
+    value:{
+`;
+try {
+    evaluate("");
+    evalInWorker("");
+} catch (exc) {}
+try {
+    evalInWorker("");
+} catch (exc) {}
+try {
+    oomTest(function() {
+        eval("function testDeepBail1() {");
+    });
+} catch (exc) {}
