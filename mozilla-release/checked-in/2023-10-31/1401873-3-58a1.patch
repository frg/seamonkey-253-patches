# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1505966525 -36000
# Node ID 8203dbc82127f9e19408d08ab4d3f1ddd64be40c
# Parent  969b2d0933f14ab030decce1ac4ef9b904e945f9
Bug 1401873 - Remove nsHtml5Atom. r=froydnj,hsivonen.

nsHtml5Atoms are very similar to dynamic nsAtoms. This patch removes the former
in favour of the latter, which leaves nsAtom as the only subclass of nsIAtom.

nsAtom::mKind is still used to distinguish dynamic atoms from HTML5 atoms, and
the HTML5 parser still uses manual memory management to handle its HTML5 atoms.

nsHtml5AtomEntry::mAtom had to be changed from an nsAutoPtr to a raw pointer
because nsAtom's destructor is private.

MozReview-Commit-ID: 1pBzwkog3ut

diff --git a/parser/html/moz.build b/parser/html/moz.build
--- a/parser/html/moz.build
+++ b/parser/html/moz.build
@@ -51,17 +51,16 @@ EXPORTS += [
     'nsHtml5UTF16Buffer.h',
     'nsHtml5UTF16BufferHSupplement.h',
     'nsHtml5ViewSourceUtils.h',
     'nsIContentHandle.h',
     'nsParserUtils.h',
 ]
 
 UNIFIED_SOURCES += [
-    'nsHtml5Atom.cpp',
     'nsHtml5AtomTable.cpp',
     'nsHtml5AttributeName.cpp',
     'nsHtml5DependentUTF16Buffer.cpp',
     'nsHtml5DocumentBuilder.cpp',
     'nsHtml5ElementName.cpp',
     'nsHtml5Highlighter.cpp',
     'nsHtml5HtmlAttributes.cpp',
     'nsHtml5MetaScanner.cpp',
diff --git a/parser/html/nsHtml5Atom.cpp b/parser/html/nsHtml5Atom.cpp
deleted file mode 100644
--- a/parser/html/nsHtml5Atom.cpp
+++ /dev/null
@@ -1,77 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#include "nsHtml5Atom.h"
-#include "nsAutoPtr.h"
-#include "mozilla/Unused.h"
-
-nsHtml5Atom::nsHtml5Atom(const nsAString& aString)
-{
-  mLength = aString.Length();
-  SetKind(AtomKind::HTML5Atom);
-  RefPtr<nsStringBuffer> buf = nsStringBuffer::FromString(aString);
-  if (buf) {
-    mString = static_cast<char16_t*>(buf->Data());
-  } else {
-    const size_t size = (mLength + 1) * sizeof(char16_t);
-    buf = nsStringBuffer::Alloc(size);
-    if (MOZ_UNLIKELY(!buf)) {
-      // We OOM because atom allocations should be small and it's hard to
-      // handle them more gracefully in a constructor.
-      NS_ABORT_OOM(size);
-    }
-    mString = static_cast<char16_t*>(buf->Data());
-    CopyUnicodeTo(aString, 0, mString, mLength);
-    mString[mLength] = char16_t(0);
-  }
-
-  NS_ASSERTION(mString[mLength] == char16_t(0), "null terminated");
-  NS_ASSERTION(buf && buf->StorageSize() >= (mLength+1) * sizeof(char16_t),
-               "enough storage");
-  NS_ASSERTION(Equals(aString), "correct data");
-
-  // Take ownership of buffer
-  mozilla::Unused << buf.forget();
-}
-
-nsHtml5Atom::~nsHtml5Atom()
-{
-  nsStringBuffer::FromData(mString)->Release();
-}
-
-NS_IMETHODIMP
-nsHtml5Atom::QueryInterface(REFNSIID aIID, void** aInstancePtr)
-{
-  NS_NOTREACHED("Attempt to call QueryInterface an nsHtml5Atom.");
-  return NS_ERROR_UNEXPECTED;
-}
-
-NS_IMETHODIMP
-nsHtml5Atom::ScriptableToString(nsAString& aBuf)
-{
-  NS_NOTREACHED("Should not call ScriptableToString.");
-  return NS_ERROR_NOT_IMPLEMENTED;
-}
-
-NS_IMETHODIMP
-nsHtml5Atom::ToUTF8String(nsACString& aReturn)
-{
-  NS_NOTREACHED("Should not attempt to convert to an UTF-8 string.");
-  return NS_ERROR_NOT_IMPLEMENTED;
-}
-
-NS_IMETHODIMP
-nsHtml5Atom::ScriptableEquals(const nsAString& aString, bool* aResult)
-{
-  NS_NOTREACHED("Should not call ScriptableEquals.");
-  return NS_ERROR_NOT_IMPLEMENTED;
-}
-
-NS_IMETHODIMP_(size_t)
-nsHtml5Atom::SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf)
-{
-  NS_NOTREACHED("Should not call SizeOfIncludingThis.");
-  return 0;
-}
-
diff --git a/parser/html/nsHtml5Atom.h b/parser/html/nsHtml5Atom.h
deleted file mode 100644
--- a/parser/html/nsHtml5Atom.h
+++ /dev/null
@@ -1,28 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#ifndef nsHtml5Atom_h
-#define nsHtml5Atom_h
-
-#include "nsIAtom.h"
-#include "mozilla/Attributes.h"
-
-/**
- * A dynamic atom implementation meant for use within the nsHtml5Tokenizer and 
- * nsHtml5TreeBuilder owned by one nsHtml5Parser or nsHtml5StreamParser 
- * instance.
- *
- * Usage is documented in nsHtml5AtomTable and nsIAtom.
- */
-class nsHtml5Atom final : public nsIAtom
-{
-  public:
-    NS_IMETHOD QueryInterface(REFNSIID aIID, void** aInstancePtr) final;
-    NS_DECL_NSIATOM
-
-    explicit nsHtml5Atom(const nsAString& aString);
-    ~nsHtml5Atom();
-};
-
-#endif // nsHtml5Atom_h
diff --git a/parser/html/nsHtml5AtomTable.cpp b/parser/html/nsHtml5AtomTable.cpp
--- a/parser/html/nsHtml5AtomTable.cpp
+++ b/parser/html/nsHtml5AtomTable.cpp
@@ -1,31 +1,31 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsHtml5AtomTable.h"
-#include "nsHtml5Atom.h"
 #include "nsThreadUtils.h"
 
 nsHtml5AtomEntry::nsHtml5AtomEntry(KeyTypePointer aStr)
   : nsStringHashKey(aStr)
-  , mAtom(new nsHtml5Atom(*aStr))
+  , mAtom(new nsAtom(nsAtom::AtomKind::HTML5Atom, *aStr, 0))
 {
 }
 
 nsHtml5AtomEntry::nsHtml5AtomEntry(const nsHtml5AtomEntry& aOther)
   : nsStringHashKey(aOther)
   , mAtom(nullptr)
 {
   NS_NOTREACHED("nsHtml5AtomTable is broken and tried to copy an entry");
 }
 
 nsHtml5AtomEntry::~nsHtml5AtomEntry()
 {
+  delete mAtom;
 }
 
 nsHtml5AtomTable::nsHtml5AtomTable()
   : mRecentlyUsedParserAtoms{}
 {
 #ifdef DEBUG
   mPermittedLookupEventTarget = mozilla::GetCurrentThreadSerialEventTarget();
 #endif
diff --git a/parser/html/nsHtml5AtomTable.h b/parser/html/nsHtml5AtomTable.h
--- a/parser/html/nsHtml5AtomTable.h
+++ b/parser/html/nsHtml5AtomTable.h
@@ -2,36 +2,30 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsHtml5AtomTable_h
 #define nsHtml5AtomTable_h
 
 #include "nsHashKeys.h"
 #include "nsTHashtable.h"
-#include "nsAutoPtr.h"
 #include "nsIAtom.h"
 #include "nsISerialEventTarget.h"
 
 #define RECENTLY_USED_PARSER_ATOMS_SIZE 31
 
-class nsHtml5Atom;
-
 class nsHtml5AtomEntry : public nsStringHashKey
 {
   public:
     explicit nsHtml5AtomEntry(KeyTypePointer aStr);
     nsHtml5AtomEntry(const nsHtml5AtomEntry& aOther);
     ~nsHtml5AtomEntry();
-    inline nsHtml5Atom* GetAtom()
-    {
-      return mAtom;
-    }
+    inline nsAtom* GetAtom() { return mAtom; }
   private:
-    nsAutoPtr<nsHtml5Atom> mAtom;
+    nsAtom* mAtom;
 };
 
 /**
  * nsHtml5AtomTable provides non-locking lookup and creation of atoms for 
  * nsHtml5Parser or nsHtml5StreamParser.
  *
  * The hashtable holds dynamically allocated atoms that are private to an 
  * instance of nsHtml5Parser or nsHtml5StreamParser. (Static atoms are used on 
diff --git a/parser/html/nsHtml5SpeculativeLoad.cpp b/parser/html/nsHtml5SpeculativeLoad.cpp
--- a/parser/html/nsHtml5SpeculativeLoad.cpp
+++ b/parser/html/nsHtml5SpeculativeLoad.cpp
@@ -1,14 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsHtml5SpeculativeLoad.h"
 #include "nsHtml5TreeOpExecutor.h"
+#include "mozilla/Encoding.h"
 
 using namespace mozilla;
 
 nsHtml5SpeculativeLoad::nsHtml5SpeculativeLoad()
   :
 #ifdef DEBUG
   mOpCode(eSpeculativeLoadUninitialized),
 #endif
diff --git a/xpcom/ds/nsAtomTable.cpp b/xpcom/ds/nsAtomTable.cpp
--- a/xpcom/ds/nsAtomTable.cpp
+++ b/xpcom/ds/nsAtomTable.cpp
@@ -130,22 +130,23 @@ public:
 
 private:
   nsStringBuffer* mBuffer;
 };
 
 UniquePtr<nsTArray<FakeBufferRefcountHelper>> gFakeBuffers;
 #endif
 
-// This constructor is for dynamic atoms.
-nsAtom::nsAtom(const nsAString& aString, uint32_t aHash)
+// This constructor is for dynamic atoms and HTML5 atoms.
+nsAtom::nsAtom(AtomKind aKind, const nsAString& aString, uint32_t aHash)
   : mRefCnt(1)
 {
   mLength = aString.Length();
-  SetKind(AtomKind::DynamicAtom);
+  SetKind(aKind);
+  MOZ_ASSERT(IsDynamicAtom() || IsHTML5Atom());
   RefPtr<nsStringBuffer> buf = nsStringBuffer::FromString(aString);
   if (buf) {
     mString = static_cast<char16_t*>(buf->Data());
   } else {
     const size_t size = (mLength + 1) * sizeof(char16_t);
     buf = nsStringBuffer::Alloc(size);
     if (MOZ_UNLIKELY(!buf)) {
       // We OOM because atom allocations should be small and it's hard to
@@ -153,17 +154,17 @@ nsAtom::nsAtom(const nsAString& aString,
       NS_ABORT_OOM(size);
     }
     mString = static_cast<char16_t*>(buf->Data());
     CopyUnicodeTo(aString, 0, mString, mLength);
     mString[mLength] = char16_t(0);
   }
 
   mHash = aHash;
-  MOZ_ASSERT(mHash == HashString(mString, mLength));
+  MOZ_ASSERT_IF(IsDynamicAtom(), mHash == HashString(mString, mLength));
 
   NS_ASSERTION(mString[mLength] == char16_t(0), "null terminated");
   NS_ASSERTION(buf && buf->StorageSize() >= (mLength + 1) * sizeof(char16_t),
                "enough storage");
   NS_ASSERTION(Equals(aString), "correct data");
 
   // Take ownership of buffer
   mozilla::Unused << buf.forget();
@@ -197,49 +198,50 @@ nsAtom::nsAtom(nsStringBuffer* aStringBu
              "correct storage");
 }
 
 // We don't need a virtual destructor because we always delete via an nsAtom*
 // pointer (in AtomTableClearEntry() for static atoms, and in
 // GCAtomTableLocked() for dynamic atoms), not an nsIAtom* pointer.
 nsAtom::~nsAtom()
 {
-  if (IsDynamicAtom()) {
+  if (!IsStaticAtom()) {
+    MOZ_ASSERT(IsDynamicAtom() || IsHTML5Atom());
     nsStringBuffer::FromData(mString)->Release();
-  } else {
-    MOZ_ASSERT(IsStaticAtom());
   }
 }
 
 NS_IMPL_QUERY_INTERFACE(nsAtom, nsIAtom);
 
 NS_IMETHODIMP
 nsAtom::ScriptableToString(nsAString& aBuf)
 {
   nsStringBuffer::FromData(mString)->ToString(mLength, aBuf);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsAtom::ToUTF8String(nsACString& aBuf)
 {
+  MOZ_ASSERT(!IsHTML5Atom(), "Called ToUTF8String() on an HTML5 atom");
   CopyUTF16toUTF8(nsDependentString(mString, mLength), aBuf);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsAtom::ScriptableEquals(const nsAString& aString, bool* aResult)
 {
   *aResult = aString.Equals(nsDependentString(mString, mLength));
   return NS_OK;
 }
 
 NS_IMETHODIMP_(size_t)
 nsAtom::SizeOfIncludingThis(MallocSizeOf aMallocSizeOf)
 {
+  MOZ_ASSERT(!IsHTML5Atom(), "Called SizeOfIncludingThis() on an HTML5 atom");
   size_t n = aMallocSizeOf(this);
   // String buffers pointed to by static atoms are in static memory, and so
   // are not measured here.
   if (IsDynamicAtom()) {
     n += nsStringBuffer::FromData(mString)->SizeOfIncludingThisIfUnshared(
            aMallocSizeOf);
   } else {
     MOZ_ASSERT(IsStaticAtom());
@@ -247,28 +249,28 @@ nsAtom::SizeOfIncludingThis(MallocSizeOf
   return n;
 }
 
 //----------------------------------------------------------------------
 
 NS_IMETHODIMP_(MozExternalRefCountType)
 nsIAtom::AddRef()
 {
-  MOZ_ASSERT(!IsHTML5Atom(), "Attempt to AddRef an nsHtml5Atom");
+  MOZ_ASSERT(!IsHTML5Atom(), "Attempt to AddRef an HTML5 atom");
   if (!IsDynamicAtom()) {
     MOZ_ASSERT(IsStaticAtom());
     return 2;
   }
   return static_cast<nsAtom*>(this)->DynamicAddRef();
 }
 
 NS_IMETHODIMP_(MozExternalRefCountType)
 nsIAtom::Release()
 {
-  MOZ_ASSERT(!IsHTML5Atom(), "Attempt to Release an nsHtml5Atom");
+  MOZ_ASSERT(!IsHTML5Atom(), "Attempt to Release an HTML5 atom");
   if (!IsDynamicAtom()) {
     MOZ_ASSERT(IsStaticAtom());
     return 1;
   }
   return static_cast<nsAtom*>(this)->DynamicRelease();
 }
 
 //----------------------------------------------------------------------
@@ -730,17 +732,18 @@ nsAtomFriend::Atomize(const nsACString& 
     return atom.forget();
   }
 
   // This results in an extra addref/release of the nsStringBuffer.
   // Unfortunately there doesn't seem to be any APIs to avoid that.
   // Actually, now there is, sort of: ForgetSharedBuffer.
   nsString str;
   CopyUTF8toUTF16(aUTF8String, str);
-  RefPtr<nsAtom> atom = dont_AddRef(new nsAtom(str, hash));
+  RefPtr<nsAtom> atom =
+    dont_AddRef(new nsAtom(nsAtom::AtomKind::DynamicAtom, str, hash));
 
   he->mAtom = atom;
 
   return atom.forget();
 }
 
 already_AddRefed<nsIAtom>
 NS_Atomize(const nsACString& aUTF8String)
@@ -764,17 +767,18 @@ nsAtomFriend::Atomize(const nsAString& a
                                         &hash);
 
   if (he->mAtom) {
     nsCOMPtr<nsIAtom> atom = he->mAtom;
 
     return atom.forget();
   }
 
-  RefPtr<nsAtom> atom = dont_AddRef(new nsAtom(aUTF16String, hash));
+  RefPtr<nsAtom> atom =
+    dont_AddRef(new nsAtom(nsAtom::AtomKind::DynamicAtom, aUTF16String, hash));
   he->mAtom = atom;
 
   return atom.forget();
 }
 
 already_AddRefed<nsIAtom>
 NS_Atomize(const nsAString& aUTF16String)
 {
@@ -801,17 +805,18 @@ nsAtomFriend::AtomizeMainThread(const ns
   }
 
   MutexAutoLock lock(*gAtomTableLock);
   AtomTableEntry* he = static_cast<AtomTableEntry*>(gAtomTable->Add(&key));
 
   if (he->mAtom) {
     retVal = he->mAtom;
   } else {
-    RefPtr<nsAtom> newAtom = dont_AddRef(new nsAtom(aUTF16String, hash));
+    RefPtr<nsAtom> newAtom = dont_AddRef(
+      new nsAtom(nsAtom::AtomKind::DynamicAtom, aUTF16String, hash));
     he->mAtom = newAtom;
     retVal = newAtom.forget();
   }
 
   sRecentlyUsedMainThreadAtoms[index] = he->mAtom;
   return retVal.forget();
 }
 
diff --git a/xpcom/ds/nsIAtom.h.1401873-3.later b/xpcom/ds/nsIAtom.h.1401873-3.later
new file mode 100644
--- /dev/null
+++ b/xpcom/ds/nsIAtom.h.1401873-3.later
@@ -0,0 +1,47 @@
+--- nsIAtom.h
++++ nsIAtom.h
+@@ -74,17 +74,21 @@ public:
+     return nsStringBuffer::FromData(mString);
+   }
+ 
+   NS_IMETHOD_(MozExternalRefCountType) AddRef() final;
+   NS_IMETHOD_(MozExternalRefCountType) Release() final;
+ 
+   // A hashcode that is better distributed than the actual atom pointer, for
+   // use in situations that need a well-distributed hashcode.
+-  uint32_t hash() const { return mHash; }
++  uint32_t hash() const
++  {
++    MOZ_ASSERT(!IsHTML5Atom());
++    return mHash;
++  }
+ 
+ protected:
+   uint32_t mLength: 30;
+   uint32_t mKind: 2; // nsIAtom::AtomKind
+   uint32_t mHash;
+   // WARNING! There is an invisible constraint on |mString|: the chars it
+   // points to must belong to an nsStringBuffer. This is so that the
+   // nsStringBuffer::FromData() calls above are valid.
+@@ -102,19 +106,20 @@ class nsAtom final : public nsIAtom
+ public:
+   NS_DECL_NSIATOM
+   NS_IMETHOD QueryInterface(REFNSIID aIID, void** aInstancePtr) final;
+   typedef mozilla::TrueType HasThreadSafeRefCnt;
+ 
+ private:
+   friend class nsIAtom;
+   friend class nsAtomFriend;
++  friend class nsHtml5AtomEntry;
+ 
+   // Construction and destruction is done entirely by |friend|s.
+-  nsAtom(const nsAString& aString, uint32_t aHash);
++  nsAtom(AtomKind aKind, const nsAString& aString, uint32_t aHash);
+   nsAtom(nsStringBuffer* aStringBuffer, uint32_t aLength, uint32_t aHash);
+   ~nsAtom();
+ 
+   MozExternalRefCountType DynamicAddRef();
+   MozExternalRefCountType DynamicRelease();
+ 
+   mozilla::ThreadSafeAutoRefCnt mRefCnt;
+   NS_DECL_OWNINGTHREAD
diff --git a/xpcom/ds/nsIAtom.idl b/xpcom/ds/nsIAtom.idl
--- a/xpcom/ds/nsIAtom.idl
+++ b/xpcom/ds/nsIAtom.idl
@@ -99,16 +99,17 @@ interface nsIAtom : nsISupports
   NS_IMETHOD_(MozExternalRefCountType) Release() final;
 
   /**
    * A hashcode that is better distributed than the actual atom
    * pointer, for use in situations that need a well-distributed
    * hashcode.
    */
   inline uint32_t hash() const {
+    MOZ_ASSERT(!IsHTML5Atom());
     return mHash;
   }
 
 protected:
   uint32_t mLength: 30;
   uint32_t mKind: 2; // nsIAtom::AtomKind
   uint32_t mHash;
   /**
@@ -127,19 +128,20 @@ class nsAtom final : public nsIAtom
 public:
   NS_DECL_NSIATOM
   NS_IMETHOD QueryInterface(REFNSIID aIID, void** aInstancePtr) final;
   typedef mozilla::TrueType HasThreadSafeRefCnt;
 
 private:
   friend class nsIAtom;
   friend class nsAtomFriend;
+  friend class nsHtml5AtomEntry;
 
   // Construction and destruction is done entirely by |friend|s.
-  nsAtom(const nsAString& aString, uint32_t aHash);
+  nsAtom(AtomKind aKind, const nsAString& aString, uint32_t aHash);
   nsAtom(nsStringBuffer* aStringBuffer, uint32_t aLength, uint32_t aHash);
   ~nsAtom();
 
   MozExternalRefCountType DynamicAddRef();
   MozExternalRefCountType DynamicRelease();
 
   mozilla::ThreadSafeAutoRefCnt mRefCnt;
   NS_DECL_OWNINGTHREAD
