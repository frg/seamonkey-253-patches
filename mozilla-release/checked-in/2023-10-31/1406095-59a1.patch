# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1511865441 28800
# Node ID 13aadb12e47178f150be6d186416191f48ed06a6
# Parent  4bad0f5c533e959fd8cd9172c5ce91b0241e6e93
Bug 1406095 - Add fast path for native objects to Object.values/entries. r=jandem

diff --git a/js/src/builtin/Object.cpp b/js/src/builtin/Object.cpp
--- a/js/src/builtin/Object.cpp
+++ b/js/src/builtin/Object.cpp
@@ -2,16 +2,17 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "builtin/Object.h"
 
 #include "mozilla/ArrayUtils.h"
+#include "mozilla/MaybeOneOf.h"
 
 #include "jscntxt.h"
 #include "jsstr.h"
 
 #include "builtin/Eval.h"
 #include "builtin/SelfHostingDefines.h"
 #include "frontend/BytecodeCompiler.h"
 #include "jit/InlinableNatives.h"
@@ -1166,32 +1167,314 @@ js::GetOwnPropertyDescriptorToArray(JSCo
     // [[GetOwnProperty]] is spec'ed to always return a complete property
     // descriptor record (ES2017, 6.1.7.3, invariants of [[GetOwnProperty]]).
     desc.assertCompleteIfFound();
 
     // Step 4.
     return FromPropertyDescriptorToArray(cx, desc, args.rval());
 }
 
-enum EnumerableOwnPropertiesKind {
+static bool
+NewValuePair(JSContext* cx, HandleValue val1, HandleValue val2, MutableHandleValue rval)
+{
+    ArrayObject* array = NewDenseFullyAllocatedArray(cx, 2);
+    if (!array)
+        return false;
+
+    array->setDenseInitializedLength(2);
+    array->initDenseElement(0, val1);
+    array->initDenseElement(1, val2);
+
+    rval.setObject(*array);
+    return true;
+}
+
+enum class EnumerableOwnPropertiesKind {
     Keys,
     Values,
-    KeysAndValues
+    KeysAndValues,
+    Names
 };
 
-// ES7 proposal 2015-12-14
-// http://tc39.github.io/proposal-object-values-entries/#EnumerableOwnProperties
+static bool
+HasEnumerableStringNonDataProperties(NativeObject* obj)
+{
+    // We also check for enumerability and symbol properties, so uninteresting
+    // non-data properties like |array.length| don't let us fall into the slow
+    // path.
+    for (Shape::Range<NoGC> r(obj->lastProperty()); !r.empty(); r.popFront()) {
+        Shape* shape = &r.front();
+        if (!shape->isDataProperty() && shape->enumerable() && !JSID_IS_SYMBOL(shape->propid()))
+            return true;
+    }
+    return false;
+}
+
+template <EnumerableOwnPropertiesKind kind>
 static bool
-EnumerableOwnProperties(JSContext* cx, const JS::CallArgs& args, EnumerableOwnPropertiesKind kind)
+TryEnumerableOwnPropertiesNative(JSContext* cx, HandleObject obj, MutableHandleValue rval,
+                                 bool* optimized)
 {
+    *optimized = false;
+
+    // Use the fast path if |obj| has neither extra indexed properties nor a
+    // newEnumerate hook. String objects need to be special-cased, because
+    // they're only marked as indexed after their enumerate hook ran. And
+    // because their enumerate hook is slowish, it's more performant to
+    // exclude them directly instead of executing the hook first.
+    if (!obj->isNative() ||
+        obj->as<NativeObject>().isIndexed() ||
+        obj->getClass()->getNewEnumerate() ||
+        obj->is<StringObject>())
+    {
+        return true;
+    }
+
+    HandleNativeObject nobj = obj.as<NativeObject>();
+
+    // Resolve lazy properties on |nobj|.
+    if (JSEnumerateOp enumerate = nobj->getClass()->getEnumerate()) {
+        if (!enumerate(cx, nobj))
+            return false;
+
+        // Ensure no extra indexed properties were added through enumerate().
+        if (nobj->isIndexed())
+            return true;
+    }
+
+    *optimized = true;
+
+    AutoValueVector properties(cx);
+    RootedValue key(cx);
+    RootedValue value(cx);
+
+    // We have ensured |nobj| contains no extra indexed properties, so the
+    // only indexed properties we need to handle here are dense and typed
+    // array elements.
+
+    for (uint32_t i = 0, len = nobj->getDenseInitializedLength(); i < len; i++) {
+        value.set(nobj->getDenseElement(i));
+        if (value.isMagic(JS_ELEMENTS_HOLE))
+            continue;
+
+        JSString* str;
+        if (kind != EnumerableOwnPropertiesKind::Values) {
+            static_assert(NativeObject::MAX_DENSE_ELEMENTS_COUNT <= JSID_INT_MAX,
+                          "dense elements don't exceed JSID_INT_MAX");
+            str = Int32ToString<CanGC>(cx, i);
+            if (!str)
+                return false;
+        }
+
+        if (kind == EnumerableOwnPropertiesKind::Keys ||
+            kind == EnumerableOwnPropertiesKind::Names)
+        {
+            value.setString(str);
+        } else if (kind == EnumerableOwnPropertiesKind::KeysAndValues) {
+            key.setString(str);
+            if (!NewValuePair(cx, key, value, &value))
+                return false;
+        }
+
+        if (!properties.append(value))
+            return false;
+    }
+
+    if (obj->is<TypedArrayObject>()) {
+        Handle<TypedArrayObject*> tobj = obj.as<TypedArrayObject>();
+        uint32_t len = tobj->length();
+
+        // Fail early if the typed array contains too many elements for a
+        // dense array, because we likely OOM anyway when trying to allocate
+        // more than 2GB for the properties vector. This also means we don't
+        // need to handle indices greater than MAX_INT32 in the loop below.
+        if (len > NativeObject::MAX_DENSE_ELEMENTS_COUNT) {
+            ReportOutOfMemory(cx);
+            return false;
+        }
+
+        MOZ_ASSERT(properties.empty(), "typed arrays cannot have dense elements");
+        if (!properties.resize(len))
+            return false;
+
+        for (uint32_t i = 0; i < len; i++) {
+            JSString* str;
+            if (kind != EnumerableOwnPropertiesKind::Values) {
+                static_assert(NativeObject::MAX_DENSE_ELEMENTS_COUNT <= JSID_INT_MAX,
+                              "dense elements don't exceed JSID_INT_MAX");
+                str = Int32ToString<CanGC>(cx, i);
+                if (!str)
+                    return false;
+            }
+
+            if (kind == EnumerableOwnPropertiesKind::Keys ||
+                kind == EnumerableOwnPropertiesKind::Names)
+            {
+                value.setString(str);
+            } else if (kind == EnumerableOwnPropertiesKind::Values) {
+                value.set(tobj->getElement(i));
+            } else {
+                key.setString(str);
+                value.set(tobj->getElement(i));
+                if (!NewValuePair(cx, key, value, &value))
+                    return false;
+            }
+
+            properties[i].set(value);
+        }
+    }
+
+    // Up to this point no side-effects through accessor properties are
+    // possible which could have replaced |obj| with a non-native object.
+    MOZ_ASSERT(obj->isNative());
+
+    if (kind == EnumerableOwnPropertiesKind::Keys ||
+        kind == EnumerableOwnPropertiesKind::Names ||
+        !HasEnumerableStringNonDataProperties(nobj))
+    {
+        // If |kind == Values| or |kind == KeysAndValues|:
+        // All enumerable properties with string property keys are data
+        // properties. This allows us to collect the property values while
+        // iterating over the shape hierarchy without worrying over accessors
+        // modifying any state.
+
+        size_t elements = properties.length();
+        constexpr bool onlyEnumerable = kind != EnumerableOwnPropertiesKind::Names;
+        constexpr AllowGC allowGC = kind != EnumerableOwnPropertiesKind::KeysAndValues
+                                    ? AllowGC::NoGC
+                                    : AllowGC::CanGC;
+        mozilla::MaybeOneOf<Shape::Range<NoGC>, Shape::Range<CanGC>> m;
+        if (allowGC == AllowGC::NoGC)
+            m.construct<Shape::Range<NoGC>>(nobj->lastProperty());
+        else
+            m.construct<Shape::Range<CanGC>>(cx, nobj->lastProperty());
+        for (Shape::Range<allowGC>& r = m.ref<Shape::Range<allowGC>>(); !r.empty(); r.popFront()) {
+            Shape* shape = &r.front();
+            jsid id = shape->propid();
+            if ((onlyEnumerable && !shape->enumerable()) || JSID_IS_SYMBOL(id))
+                continue;
+            MOZ_ASSERT(!JSID_IS_INT(id), "Unexpected indexed property");
+            MOZ_ASSERT_IF(kind == EnumerableOwnPropertiesKind::Values ||
+                          kind == EnumerableOwnPropertiesKind::KeysAndValues,
+                          shape->isDataProperty());
+
+            if (kind == EnumerableOwnPropertiesKind::Keys ||
+                kind == EnumerableOwnPropertiesKind::Names)
+            {
+                value.setString(JSID_TO_STRING(id));
+            } else if (kind == EnumerableOwnPropertiesKind::Values) {
+                value.set(nobj->getSlot(shape->slot()));
+            } else {
+                key.setString(JSID_TO_STRING(id));
+                value.set(nobj->getSlot(shape->slot()));
+                if (!NewValuePair(cx, key, value, &value))
+                    return false;
+            }
+
+            if (!properties.append(value))
+                return false;
+        }
+
+        // The (non-indexed) properties were visited in reverse iteration
+        // order, call Reverse() to ensure they appear in iteration order.
+        Reverse(properties.begin() + elements, properties.end());
+    } else {
+        MOZ_ASSERT(kind == EnumerableOwnPropertiesKind::Values ||
+                   kind == EnumerableOwnPropertiesKind::KeysAndValues);
+
+        // Get a list of all |obj| shapes. As long as obj->lastProperty()
+        // is equal to |objShape|, we can use this to speed up both the
+        // enumerability check and GetProperty.
+        using ShapeVector = GCVector<Shape*, 8>;
+        Rooted<ShapeVector> shapes(cx, ShapeVector(cx));
+
+        // Collect all non-symbol properties.
+        RootedShape objShape(cx, nobj->lastProperty());
+        for (Shape::Range<NoGC> r(objShape); !r.empty(); r.popFront()) {
+            Shape* shape = &r.front();
+            if (JSID_IS_SYMBOL(shape->propid()))
+                continue;
+            MOZ_ASSERT(!JSID_IS_INT(shape->propid()), "Unexpected indexed property");
+
+            if (!shapes.append(shape))
+                return false;
+        }
+
+        RootedId id(cx);
+        for (size_t i = shapes.length(); i > 0; i--) {
+            Shape* shape = shapes[i - 1];
+            id = shape->propid();
+
+            // Ensure |obj| is still native: a getter might have turned it
+            // into an unboxed object or it could have been swapped with a
+            // non-native object.
+            if (obj->isNative() &&
+                obj->as<NativeObject>().lastProperty() == objShape &&
+                shape->isDataProperty())
+            {
+                if (!shape->enumerable())
+                    continue;
+                value = obj->as<NativeObject>().getSlot(shape->slot());
+            } else {
+                // |obj| changed shape or the property is not a data property,
+                // so we have to do the slower enumerability check and
+                // GetProperty.
+                bool enumerable;
+                if (!PropertyIsEnumerable(cx, obj, id, &enumerable))
+                    return false;
+                if (!enumerable)
+                    continue;
+                if (!GetProperty(cx, obj, obj, id, &value))
+                    return false;
+            }
+
+            if (kind == EnumerableOwnPropertiesKind::KeysAndValues) {
+                key.setString(JSID_TO_STRING(id));
+                if (!NewValuePair(cx, key, value, &value))
+                    return false;
+            }
+
+            if (!properties.append(value))
+                return false;
+        }
+    }
+
+    JSObject* array = NewDenseCopiedArray(cx, properties.length(), properties.begin());
+    if (!array)
+        return false;
+
+    rval.setObject(*array);
+    return true;
+}
+
+// ES2018 draft rev c164be80f7ea91de5526b33d54e5c9321ed03d3f
+// 7.3.21 EnumerableOwnProperties ( O, kind )
+template <EnumerableOwnPropertiesKind kind>
+static bool
+EnumerableOwnProperties(JSContext* cx, const JS::CallArgs& args)
+{
+    static_assert(kind == EnumerableOwnPropertiesKind::Values ||
+                  kind == EnumerableOwnPropertiesKind::KeysAndValues,
+                  "Only implemented for Object.keys and Object.entries");
+
     // Step 1. (Step 1 of Object.{keys,values,entries}, really.)
     RootedObject obj(cx, ToObject(cx, args.get(0)));
     if (!obj)
         return false;
 
+    bool optimized;
+    if (!TryEnumerableOwnPropertiesNative<kind>(cx, obj, args.rval(), &optimized))
+        return false;
+
+    if (optimized)
+        return true;
+
+    // Typed arrays are always handled in the fast path.
+    MOZ_ASSERT(!obj->is<TypedArrayObject>());
+
     // Step 2.
     AutoIdVector ids(cx);
     if (!GetPropertyKeys(cx, obj, JSITER_OWNONLY | JSITER_HIDDEN, &ids))
         return false;
 
     // Step 3.
     AutoValueVector properties(cx);
     size_t len = ids.length();
@@ -1206,29 +1489,29 @@ EnumerableOwnProperties(JSContext* cx, c
     // Step 4.
     size_t out = 0;
     for (size_t i = 0; i < len; i++) {
         id = ids[i];
 
         // Step 4.a. (Symbols were filtered out in step 2.)
         MOZ_ASSERT(!JSID_IS_SYMBOL(id));
 
-        if (kind != Values) {
+        if (kind != EnumerableOwnPropertiesKind::Values) {
             if (!IdToStringOrSymbol(cx, id, &key))
                 return false;
         }
 
         // Step 4.a.i.
         if (obj->is<NativeObject>()) {
             HandleNativeObject nobj = obj.as<NativeObject>();
             if (JSID_IS_INT(id) && nobj->containsDenseElement(JSID_TO_INT(id))) {
                 value = nobj->getDenseOrTypedArrayElement(JSID_TO_INT(id));
             } else {
                 shape = nobj->lookup(cx, id);
-                if (!shape || !(shape->attributes() & JSPROP_ENUMERATE))
+                if (!shape || !shape->enumerable())
                     continue;
                 if (!shape->isAccessorShape()) {
                     if (!NativeGetExistingProperty(cx, nobj, nobj, shape, &value))
                         return false;
                 } else if (!GetProperty(cx, obj, obj, id, &value)) {
                     return false;
                 }
             }
@@ -1244,17 +1527,17 @@ EnumerableOwnProperties(JSContext* cx, c
             // (Omitted because Object.keys doesn't use this implementation.)
 
             // Step 4.a.ii.2.a.
             if (!GetProperty(cx, obj, obj, id, &value))
                 return false;
         }
 
         // Steps 4.a.ii.2.b-c.
-        if (kind == Values)
+        if (kind == EnumerableOwnPropertiesKind::Values)
             properties[out++].set(value);
         else if (!NewValuePair(cx, key, value, properties[out++]))
             return false;
     }
 
     // Step 5.
     // (Implemented in step 2.)
 
@@ -1262,41 +1545,60 @@ EnumerableOwnProperties(JSContext* cx, c
     JSObject* aobj = NewDenseCopiedArray(cx, out, properties.begin());
     if (!aobj)
         return false;
 
     args.rval().setObject(*aobj);
     return true;
 }
 
-// ES7 proposal 2015-12-14
-// http://tc39.github.io/proposal-object-values-entries/#Object.keys
+// ES2018 draft rev c164be80f7ea91de5526b33d54e5c9321ed03d3f
+// 19.1.2.16 Object.keys ( O )
 static bool
 obj_keys(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    return GetOwnPropertyKeys(cx, args, JSITER_OWNONLY);
+
+    // Step 1.
+    RootedObject obj(cx, ToObject(cx, args.get(0)));
+    if (!obj)
+        return false;
+
+    bool optimized;
+    static constexpr EnumerableOwnPropertiesKind kind = EnumerableOwnPropertiesKind::Keys;
+    if (!TryEnumerableOwnPropertiesNative<kind>(cx, obj, args.rval(), &optimized))
+        return false;
+
+    if (optimized)
+        return true;
+
+    // Steps 2-3.
+    return GetOwnPropertyKeys(cx, obj, JSITER_OWNONLY, args.rval());
 }
 
-// ES7 proposal 2015-12-14
-// http://tc39.github.io/proposal-object-values-entries/#Object.values
+// ES2018 draft rev c164be80f7ea91de5526b33d54e5c9321ed03d3f
+// 19.1.2.21 Object.values ( O )
 static bool
 obj_values(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    return EnumerableOwnProperties(cx, args, Values);
+
+    // Steps 1-3.
+    return EnumerableOwnProperties<EnumerableOwnPropertiesKind::Values>(cx, args);
 }
 
-// ES7 proposal 2015-12-14
-// http://tc39.github.io/proposal-object-values-entries/#Object.entries
+// ES2018 draft rev c164be80f7ea91de5526b33d54e5c9321ed03d3f
+// 19.1.2.5 Object.entries ( O )
 static bool
 obj_entries(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    return EnumerableOwnProperties(cx, args, KeysAndValues);
+
+    // Steps 1-3.
+    return EnumerableOwnProperties<EnumerableOwnPropertiesKind::KeysAndValues>(cx, args);
 }
 
 /* ES6 draft 15.2.3.16 */
 static bool
 obj_is(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
@@ -1322,22 +1624,19 @@ js::IdToStringOrSymbol(JSContext* cx, Ha
         result.setSymbol(JSID_TO_SYMBOL(id));
     }
     return true;
 }
 
 // ES2018 draft rev c164be80f7ea91de5526b33d54e5c9321ed03d3f
 // 19.1.2.10.1 Runtime Semantics: GetOwnPropertyKeys ( O, Type )
 bool
-js::GetOwnPropertyKeys(JSContext* cx, const JS::CallArgs& args, unsigned flags)
+js::GetOwnPropertyKeys(JSContext* cx, HandleObject obj, unsigned flags, MutableHandleValue rval)
 {
-    // Step 1.
-    RootedObject obj(cx, ToObject(cx, args.get(0)));
-    if (!obj)
-        return false;
+    // Step 1 (Performed in caller).
 
     // Steps 2-4.
     AutoIdVector keys(cx);
     if (!GetPropertyKeys(cx, obj, flags, &keys))
         return false;
 
     // Step 5 (Inlined CreateArrayFromList).
     RootedArrayObject array(cx, NewDenseFullyAllocatedArray(cx, keys.length()));
@@ -1350,34 +1649,56 @@ js::GetOwnPropertyKeys(JSContext* cx, co
     for (size_t i = 0, len = keys.length(); i < len; i++) {
         MOZ_ASSERT_IF(JSID_IS_SYMBOL(keys[i]), flags & JSITER_SYMBOLS);
         MOZ_ASSERT_IF(!JSID_IS_SYMBOL(keys[i]), !(flags & JSITER_SYMBOLSONLY));
         if (!IdToStringOrSymbol(cx, keys[i], &val))
             return false;
         array->initDenseElement(i, val);
     }
 
-    args.rval().setObject(*array);
+    rval.setObject(*array);
     return true;
 }
 
+// ES2018 draft rev c164be80f7ea91de5526b33d54e5c9321ed03d3f
+// 19.1.2.9 Object.getOwnPropertyNames ( O )
 bool
 js::obj_getOwnPropertyNames(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    return GetOwnPropertyKeys(cx, args, JSITER_OWNONLY | JSITER_HIDDEN);
+
+    RootedObject obj(cx, ToObject(cx, args.get(0)));
+    if (!obj)
+        return false;
+
+    bool optimized;
+    static constexpr EnumerableOwnPropertiesKind kind = EnumerableOwnPropertiesKind::Names;
+    if (!TryEnumerableOwnPropertiesNative<kind>(cx, obj, args.rval(), &optimized))
+        return false;
+
+    if (optimized)
+        return true;
+
+    return GetOwnPropertyKeys(cx, obj, JSITER_OWNONLY | JSITER_HIDDEN, args.rval());
 }
 
-/* ES6 draft rev 25 (2014 May 22) 19.1.2.8 */
+// ES2018 draft rev c164be80f7ea91de5526b33d54e5c9321ed03d3f
+// 19.1.2.10 Object.getOwnPropertySymbols ( O )
 static bool
 obj_getOwnPropertySymbols(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    return GetOwnPropertyKeys(cx, args,
-                              JSITER_OWNONLY | JSITER_HIDDEN | JSITER_SYMBOLS | JSITER_SYMBOLSONLY);
+
+    RootedObject obj(cx, ToObject(cx, args.get(0)));
+    if (!obj)
+        return false;
+
+    return GetOwnPropertyKeys(cx, obj,
+                              JSITER_OWNONLY | JSITER_HIDDEN | JSITER_SYMBOLS | JSITER_SYMBOLSONLY,
+                              args.rval());
 }
 
 /* ES6 draft rev 32 (2015 Feb 2) 19.1.2.4: Object.defineProperty(O, P, Attributes) */
 bool
 js::obj_defineProperty(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
diff --git a/js/src/builtin/Object.h b/js/src/builtin/Object.h
--- a/js/src/builtin/Object.h
+++ b/js/src/builtin/Object.h
@@ -52,17 +52,17 @@ obj_isExtensible(JSContext* cx, unsigned
 MOZ_MUST_USE bool
 obj_toString(JSContext* cx, unsigned argc, JS::Value* vp);
 
 JSString*
 ObjectClassToString(JSContext* cx, HandleObject obj);
 
 // Exposed so SelfHosting.cpp can use it in the OwnPropertyKeys intrinsic
 MOZ_MUST_USE bool
-GetOwnPropertyKeys(JSContext* cx, const JS::CallArgs& args, unsigned flags);
+GetOwnPropertyKeys(JSContext* cx, HandleObject obj, unsigned flags, JS::MutableHandleValue rval);
 
 // Exposed for SelfHosting.cpp
 MOZ_MUST_USE bool
 GetOwnPropertyDescriptorToArray(JSContext* cx, unsigned argc, JS::Value* vp);
 
 /*
  * Like IdToValue, but convert int jsids to strings. This is used when
  * exposing a jsid to script for Object.getOwnProperty{Names,Symbols}
diff --git a/js/src/builtin/Object.js b/js/src/builtin/Object.js
--- a/js/src/builtin/Object.js
+++ b/js/src/builtin/Object.js
@@ -3,17 +3,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 // ES stage 4 proposal
 function ObjectGetOwnPropertyDescriptors(O) {
     // Step 1.
     var obj = ToObject(O);
 
     // Step 2.
-    var keys = OwnPropertyKeys(obj, JSITER_OWNONLY | JSITER_HIDDEN | JSITER_SYMBOLS);
+    var keys = OwnPropertyKeys(obj);
 
     // Step 3.
     var descriptors = {};
 
     // Step 4.
     for (var index = 0, len = keys.length; index < len; index++) {
         var key = keys[index];
 
diff --git a/js/src/builtin/Reflect.cpp b/js/src/builtin/Reflect.cpp
--- a/js/src/builtin/Reflect.cpp
+++ b/js/src/builtin/Reflect.cpp
@@ -80,28 +80,31 @@ js::Reflect_isExtensible(JSContext* cx, 
     // Step 2.
     bool extensible;
     if (!IsExtensible(cx, target, &extensible))
         return false;
     args.rval().setBoolean(extensible);
     return true;
 }
 
-/* ES6 26.1.11 Reflect.ownKeys(target) */
+// ES2018 draft rev c164be80f7ea91de5526b33d54e5c9321ed03d3f
+// 26.1.10 Reflect.ownKeys ( target )
 static bool
 Reflect_ownKeys(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
     // Step 1.
-    if (!NonNullObjectArg(cx, "`target`", "Reflect.ownKeys", args.get(0)))
+    RootedObject target(cx, NonNullObjectArg(cx, "`target`", "Reflect.ownKeys", args.get(0)));
+    if (!target)
         return false;
 
-    // Steps 2-4.
-    return GetOwnPropertyKeys(cx, args, JSITER_OWNONLY | JSITER_HIDDEN | JSITER_SYMBOLS);
+    // Steps 2-3.
+    return GetOwnPropertyKeys(cx, target, JSITER_OWNONLY | JSITER_HIDDEN | JSITER_SYMBOLS,
+                              args.rval());
 }
 
 /* ES6 26.1.12 Reflect.preventExtensions(target) */
 static bool
 Reflect_preventExtensions(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
diff --git a/js/src/builtin/SelfHostingDefines.h b/js/src/builtin/SelfHostingDefines.h
--- a/js/src/builtin/SelfHostingDefines.h
+++ b/js/src/builtin/SelfHostingDefines.h
@@ -79,23 +79,16 @@
 // Used for list, i.e. Array and String, iterators.
 #define ITERATOR_SLOT_NEXT_INDEX 1
 #define ITERATOR_SLOT_ITEM_KIND 2
 
 #define ITEM_KIND_KEY 0
 #define ITEM_KIND_VALUE 1
 #define ITEM_KIND_KEY_AND_VALUE 2
 
-// NB: keep these in sync with the copy in jsfriendapi.h.
-#define JSITER_OWNONLY    0x8   /* iterate over obj's own properties only */
-#define JSITER_HIDDEN     0x10  /* also enumerate non-enumerable properties */
-#define JSITER_SYMBOLS    0x20  /* also include symbol property keys */
-#define JSITER_SYMBOLSONLY 0x40 /* exclude string property keys */
-
-
 #define REGEXP_SOURCE_SLOT 1
 #define REGEXP_FLAGS_SLOT 2
 
 #define REGEXP_IGNORECASE_FLAG  0x01
 #define REGEXP_GLOBAL_FLAG      0x02
 #define REGEXP_MULTILINE_FLAG   0x04
 #define REGEXP_STICKY_FLAG      0x08
 #define REGEXP_UNICODE_FLAG     0x10
diff --git a/js/src/builtin/Utilities.js b/js/src/builtin/Utilities.js
--- a/js/src/builtin/Utilities.js
+++ b/js/src/builtin/Utilities.js
@@ -247,17 +247,17 @@ function CopyDataProperties(target, sour
     // Steps 3, 6.
     if (source === undefined || source === null)
         return;
 
     // Step 4.a.
     source = ToObject(source);
 
     // Step 4.b.
-    var keys = OwnPropertyKeys(source, JSITER_OWNONLY | JSITER_HIDDEN | JSITER_SYMBOLS);
+    var keys = OwnPropertyKeys(source);
 
     // Step 5.
     for (var index = 0; index < keys.length; index++) {
         var key = keys[index];
 
         // We abbreviate this by calling propertyIsEnumerable which is faster
         // and returns false for not defined properties.
         if (!hasOwn(key, excluded) && callFunction(std_Object_propertyIsEnumerable, source, key))
@@ -278,17 +278,17 @@ function CopyDataPropertiesUnfiltered(ta
     // Steps 3, 6.
     if (source === undefined || source === null)
         return;
 
     // Step 4.a.
     source = ToObject(source);
 
     // Step 4.b.
-    var keys = OwnPropertyKeys(source, JSITER_OWNONLY | JSITER_HIDDEN | JSITER_SYMBOLS);
+    var keys = OwnPropertyKeys(source);
 
     // Step 5.
     for (var index = 0; index < keys.length; index++) {
         var key = keys[index];
 
         // We abbreviate this by calling propertyIsEnumerable which is faster
         // and returns false for not defined properties.
         if (callFunction(std_Object_propertyIsEnumerable, source, key))
diff --git a/js/src/jsarray.cpp b/js/src/jsarray.cpp
--- a/js/src/jsarray.cpp
+++ b/js/src/jsarray.cpp
@@ -4051,30 +4051,16 @@ js::NewCopiedArrayForCallingAllocationSi
                                            HandleObject proto /* = nullptr */)
 {
     RootedObjectGroup group(cx, ObjectGroup::callingAllocationSiteGroup(cx, JSProto_Array, proto));
     if (!group)
         return nullptr;
     return NewCopiedArrayTryUseGroup(cx, group, vp, length);
 }
 
-bool
-js::NewValuePair(JSContext* cx, const Value& val1, const Value& val2, MutableHandleValue rval)
-{
-    JS::AutoValueArray<2> vec(cx);
-    vec[0].set(val1);
-    vec[1].set(val2);
-
-    JSObject* aobj = js::NewDenseCopiedArray(cx, 2, vec.begin());
-    if (!aobj)
-        return false;
-    rval.setObject(*aobj);
-    return true;
-}
-
 #ifdef DEBUG
 bool
 js::ArrayInfo(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     RootedObject obj(cx);
 
     for (unsigned i = 0; i < args.length(); i++) {
diff --git a/js/src/jsarray.h b/js/src/jsarray.h
--- a/js/src/jsarray.h
+++ b/js/src/jsarray.h
@@ -105,19 +105,16 @@ NewCopiedArrayTryUseGroup(JSContext* cx,
                           const Value* vp, size_t length,
                           NewObjectKind newKind = GenericObject,
                           ShouldUpdateTypes updateTypes = ShouldUpdateTypes::Update);
 
 extern ArrayObject*
 NewCopiedArrayForCallingAllocationSite(JSContext* cx, const Value* vp, size_t length,
                                        HandleObject proto = nullptr);
 
-extern bool
-NewValuePair(JSContext* cx, const Value& val1, const Value& val2, MutableHandleValue rval);
-
 /*
  * Determines whether a write to the given element on |obj| should fail because
  * |obj| is an Array with a non-writable length, and writing that element would
  * increase the length of the array.
  */
 extern bool
 WouldDefinePastNonwritableLength(HandleNativeObject obj, uint32_t index);
 
diff --git a/js/src/tests/non262/object/values-entries-indexed.js b/js/src/tests/non262/object/values-entries-indexed.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/object/values-entries-indexed.js
@@ -0,0 +1,71 @@
+function assertSameEntries(actual, expected) {
+    assertEq(actual.length, expected.length);
+    for (let i = 0; i < expected.length; ++i)
+        assertEqArray(actual[i], expected[i]);
+}
+
+// Test Object.keys/values/entries on objects with indexed properties.
+
+// Empty dense elements, test with array and plain object.
+{
+    let array = [];
+    assertEqArray(Object.keys(array), []);
+    assertEqArray(Object.values(array), []);
+    assertSameEntries(Object.entries(array), []);
+
+    let object = {};
+    assertEqArray(Object.keys(object), []);
+    assertEqArray(Object.values(object), []);
+    assertSameEntries(Object.entries(object), []);
+}
+
+// Dense elements only, test with array and plain object.
+{
+    let array = [1, 2, 3];
+    assertEqArray(Object.keys(array), ["0", "1", "2"]);
+    assertEqArray(Object.values(array), [1, 2, 3]);
+    assertSameEntries(Object.entries(array), [["0", 1], ["1", 2], ["2", 3]]);
+
+    let object = {0: 4, 1: 5, 2: 6};
+    assertEqArray(Object.keys(object), ["0", "1", "2"]);
+    assertEqArray(Object.values(object), [4, 5, 6]);
+    assertSameEntries(Object.entries(object), [["0", 4], ["1", 5], ["2", 6]]);
+}
+
+// Extra indexed properties only, test with array and plain object.
+{
+    let array = [];
+    Object.defineProperty(array, 0, {configurable: true, enumerable: true, value: 123});
+
+    assertEqArray(Object.keys(array), ["0"]);
+    assertEqArray(Object.values(array), [123]);
+    assertSameEntries(Object.entries(array), [["0", 123]]);
+
+    let object = [];
+    Object.defineProperty(object, 0, {configurable: true, enumerable: true, value: 123});
+
+    assertEqArray(Object.keys(object), ["0"]);
+    assertEqArray(Object.values(object), [123]);
+    assertSameEntries(Object.entries(object), [["0", 123]]);
+}
+
+// Dense and extra indexed properties, test with array and plain object.
+{
+    let array = [1, 2, 3];
+    Object.defineProperty(array, 0, {writable: false});
+
+    assertEqArray(Object.keys(array), ["0", "1", "2"]);
+    assertEqArray(Object.values(array), [1, 2, 3]);
+    assertSameEntries(Object.entries(array), [["0", 1], ["1", 2], ["2", 3]]);
+
+    let object = {0: 4, 1: 5, 2: 6};
+    Object.defineProperty(object, 0, {writable: false});
+
+    assertEqArray(Object.keys(object), ["0", "1", "2"]);
+    assertEqArray(Object.values(object), [4, 5, 6]);
+    assertSameEntries(Object.entries(object), [["0", 4], ["1", 5], ["2", 6]]);
+}
+
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
diff --git a/js/src/tests/non262/object/values-entries-lazy-props.js b/js/src/tests/non262/object/values-entries-lazy-props.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/object/values-entries-lazy-props.js
@@ -0,0 +1,93 @@
+// Return new objects for each test case.
+function makeTestCases() {
+    // Call the resolve hook for arguments/string objects.
+    const resolveIndex = object => 0 in object;
+
+    // Calls the resolve hook for functions.
+    const resolveFunction = object => "length" in object;
+
+    const expected = array => ({
+        keys: Object.keys(array),
+        values: Object.values(array),
+        entries: Object.entries(array),
+    });
+
+    return [
+        // Mapped arguments objects.
+        {
+            object: function(){ return arguments; }(),
+            resolve: resolveIndex,
+            ...expected([]),
+        },
+        {
+            object: function(x){ return arguments; }(1),
+            resolve: resolveIndex,
+            ...expected([1]),
+        },
+        {
+            object: function(x, y, z){ return arguments; }(1, 2, 3),
+            resolve: resolveIndex,
+            ...expected([1, 2, 3]),
+        },
+
+        // Unmapped arguments objects.
+        {
+            object: function(){ "use strict"; return arguments; }(),
+            resolve: resolveIndex,
+            ...expected([]),
+        },
+        {
+            object: function(x){ "use strict"; return arguments; }(1),
+            resolve: resolveIndex,
+            ...expected([1]),
+        },
+        {
+            object: function(x, y, z){ "use strict"; return arguments; }(1, 2, 3),
+            resolve: resolveIndex,
+            ...expected([1, 2, 3]),
+        },
+
+        // String objects.
+        { object: new String(""), resolve: resolveIndex, ...expected([]), },
+        { object: new String("a"), resolve: resolveIndex, ...expected(["a"]), },
+        { object: new String("abc"), resolve: resolveIndex, ...expected(["a","b","c"]), },
+
+        // Function objects.
+        { object: function(){}, resolve: resolveFunction, ...expected([]), },
+    ];
+}
+
+var assertWith = {
+    keys: assertEqArray,
+    values: assertEqArray,
+    entries(actual, expected) {
+        assertEq(actual.length, expected.length);
+        for (let i = 0; i < expected.length; ++i)
+            assertEqArray(actual[i], expected[i]);
+    }
+};
+
+// Test Object.keys/values/entries on objects with enumerate/resolve hooks.
+
+for (let method of ["keys", "values", "entries"]) {
+    let assert = assertWith[method];
+
+    // Call each method twice to test the case when
+    // - the enumerate hook wasn't yet called,
+    // - the enumerate hook was already called.
+    for (let {object, [method]: expected} of makeTestCases()) {
+        assert(Object[method](object), expected);
+        assert(Object[method](object), expected);
+    }
+
+    // Test the case when enumerate wasn't yet called, but a property was already resolved.
+    for (let {object, resolve, [method]: expected} of makeTestCases()) {
+        resolve(object); // Call the resolve hook.
+
+        assert(Object[method](object), expected);
+        assert(Object[method](object), expected);
+    }
+}
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
diff --git a/js/src/tests/non262/object/values-entries-typedarray.js b/js/src/tests/non262/object/values-entries-typedarray.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/object/values-entries-typedarray.js
@@ -0,0 +1,53 @@
+function assertSameEntries(actual, expected) {
+    assertEq(actual.length, expected.length);
+    for (let i = 0; i < expected.length; ++i)
+        assertEqArray(actual[i], expected[i]);
+}
+
+function throwsTypeError(fn) {
+    try {
+        fn();
+    } catch (e) {
+        assertEq(e instanceof TypeError, true);
+        return true;
+    }
+    return false;
+}
+
+// Non-standard: Accessing elements of detached array buffers should throw, but
+// this is currently not implemented.
+const ACCESS_ON_DETACHED_ARRAY_BUFFER_THROWS = (() => {
+    let ta = new Int32Array(10);
+    detachArrayBuffer(ta.buffer);
+    let throws = throwsTypeError(() => ta[0]);
+    // Ensure [[Get]] and [[GetOwnProperty]] return consistent results.
+    assertEq(throwsTypeError(() => Object.getOwnPropertyDescriptor(ta, 0)), throws);
+    return throws;
+})();
+
+function maybeThrowOnDetached(fn, returnValue) {
+    if (ACCESS_ON_DETACHED_ARRAY_BUFFER_THROWS) {
+        assertThrowsInstanceOf(fn, TypeError);
+        return returnValue;
+    }
+    return fn();
+}
+
+// Ensure Object.keys/values/entries work correctly on typed arrays.
+for (let len of [0, 1, 10]) {
+    let array = new Array(len).fill(1);
+    let ta = new Int32Array(array);
+
+    assertEqArray(Object.keys(ta), Object.keys(array));
+    assertEqArray(Object.values(ta), Object.values(array));
+    assertSameEntries(Object.entries(ta), Object.entries(array));
+
+    detachArrayBuffer(ta.buffer);
+
+    assertEqArray(maybeThrowOnDetached(() => Object.keys(ta), []), []);
+    assertEqArray(maybeThrowOnDetached(() => Object.values(ta), []), []);
+    assertSameEntries(maybeThrowOnDetached(() => Object.entries(ta), []), []);
+}
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -303,19 +303,21 @@ intrinsic_SubstringKernel(JSContext* cx,
     args.rval().setString(substr);
     return true;
 }
 
 static bool
 intrinsic_OwnPropertyKeys(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
+    MOZ_ASSERT(args.length() == 1);
     MOZ_ASSERT(args[0].isObject());
-    MOZ_ASSERT(args[1].isInt32());
-    return GetOwnPropertyKeys(cx, args, args[1].toInt32());
+    RootedObject obj(cx, &args[0].toObject());
+    return GetOwnPropertyKeys(cx, obj, JSITER_OWNONLY | JSITER_HIDDEN | JSITER_SYMBOLS,
+                              args.rval());
 }
 
 static void
 ThrowErrorWithType(JSContext* cx, JSExnType type, const CallArgs& args)
 {
     uint32_t errorNumber = args[0].toInt32();
 
 #ifdef DEBUG
