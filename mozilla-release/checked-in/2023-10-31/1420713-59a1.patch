# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1511728544 -3600
# Node ID fb979e1d8a49504783271d68459fdf541f926ad8
# Parent  dcd5b9c326c746704b4e3a0448e2a3669b013927
Bug 1420713: Ensure ReparseStyleSheet refreshes the stylist even when there's no document. r=heycam

MozReview-Commit-ID: 1acIXdXOgO

diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -1029,16 +1029,17 @@ void
 ServoStyleSet::RecordStyleSheetChange(
     ServoStyleSheet* aSheet,
     StyleSheet::ChangeType aChangeType)
 {
   switch (aChangeType) {
     case StyleSheet::ChangeType::RuleAdded:
     case StyleSheet::ChangeType::RuleRemoved:
     case StyleSheet::ChangeType::RuleChanged:
+    case StyleSheet::ChangeType::ReparsedFromInspector:
       // FIXME(emilio): We can presumably do better in a bunch of these.
       return MarkOriginsDirty(aSheet->GetOrigin());
     case StyleSheet::ChangeType::ApplicableStateChanged:
     case StyleSheet::ChangeType::Added:
     case StyleSheet::ChangeType::Removed:
       // Do nothing, we've already recorded the change in the
       // Append/Remove/Replace methods, etc, and will act consequently.
       return;
diff --git a/layout/style/ServoStyleSheet.cpp b/layout/style/ServoStyleSheet.cpp
--- a/layout/style/ServoStyleSheet.cpp
+++ b/layout/style/ServoStyleSheet.cpp
@@ -4,19 +4,20 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/ServoStyleSheet.h"
 
 #include "mozilla/css/Rule.h"
 #include "mozilla/StyleBackendType.h"
 #include "mozilla/ServoBindings.h"
+#include "mozilla/ServoCSSRuleList.h"
 #include "mozilla/ServoImportRule.h"
 #include "mozilla/ServoMediaList.h"
-#include "mozilla/ServoCSSRuleList.h"
+#include "mozilla/ServoStyleSet.h"
 #include "mozilla/css/GroupRule.h"
 #include "mozilla/dom/CSSRuleList.h"
 #include "mozilla/dom/MediaList.h"
 #include "nsIStyleSheetLinkingElement.h"
 #include "Loader.h"
 
 
 #include "mozAutoDocUpdate.h"
@@ -337,16 +338,29 @@ ServoStyleSheet::ReparseSheet(const nsAS
       // Document observers could possibly detach document from this sheet.
       if (!mDocument) {
         // If detached, don't process any more rules.
         break;
       }
     }
   }
 
+  // FIXME(emilio): This is kind-of a hack for bug 1420713. As you may notice,
+  // there's nothing that triggers a style flush or anything similar (neither
+  // here or in the relevant Gecko path inside DidDirty).
+  //
+  // The tl;dr is: if we want to make sure scripted changes to sheets not
+  // associated with any document get properly reflected, we need to rejigger a
+  // fair amount of stuff. I'm probably doing that work as part of the shadow
+  // DOM stuff.
+  for (StyleSetHandle handle : mStyleSets) {
+    handle->AsServo()->RecordStyleSheetChange(
+      this, StyleSheet::ChangeType::ReparsedFromInspector);
+  }
+
   return NS_OK;
 }
 
 // nsICSSLoaderObserver implementation
 NS_IMETHODIMP
 ServoStyleSheet::StyleSheetLoaded(StyleSheet* aSheet,
                                   bool aWasAlternate,
                                   nsresult aStatus)
diff --git a/layout/style/StyleSheet.h b/layout/style/StyleSheet.h
--- a/layout/style/StyleSheet.h
+++ b/layout/style/StyleSheet.h
@@ -72,16 +72,17 @@ public:
    */
   enum class ChangeType {
     Added,
     Removed,
     ApplicableStateChanged,
     RuleAdded,
     RuleRemoved,
     RuleChanged,
+    ReparsedFromInspector,
   };
 
   void SetOwningNode(nsINode* aOwningNode)
   {
     mOwningNode = aOwningNode;
   }
 
   css::SheetParsingMode ParsingMode() const { return mParsingMode; }
