# HG changeset patch
# User Lee Salzman <lsalzman@mozilla.com>
# Date 1515512269 18000
# Node ID dc271c111e3352f23b424f0a041432dac027ee78
# Parent  db2bd2d5cb76bd78ce105be0444856658d9b902b
Bug 1394568 - plumb synthetic italics flag through thebes and Moz2D into WebRender. r=gankro

diff --git a/gfx/2d/2D.h b/gfx/2d/2D.h
--- a/gfx/2d/2D.h
+++ b/gfx/2d/2D.h
@@ -1548,24 +1548,25 @@ public:
   static already_AddRefed<ScaledFont>
     CreateScaledFontForNativeFont(const NativeFont &aNativeFont,
                                   const RefPtr<UnscaledFont>& aUnscaledFont,
                                   Float aSize);
 
 #ifdef MOZ_WIDGET_GTK
   static already_AddRefed<ScaledFont>
     CreateScaledFontForFontconfigFont(cairo_scaled_font_t* aScaledFont, FcPattern* aPattern,
-                                      const RefPtr<UnscaledFont>& aUnscaledFont, Float aSize);
+                                      const RefPtr<UnscaledFont>& aUnscaledFont, Float aSize,
+                                      bool aNeedsOblique = false);
 #endif
 
 #ifdef XP_DARWIN
   static already_AddRefed<ScaledFont>
     CreateScaledFontForMacFont(CGFontRef aCGFont, const RefPtr<UnscaledFont>& aUnscaledFont, Float aSize,
                                const Color& aFontSmoothingBackgroundColor, bool aUseFontSmoothing = true,
-                               bool aApplySyntheticBold = false);
+                               bool aApplySyntheticBold = false, bool aNeedsOblique = false);
 #endif
 
   /**
    * This creates a NativeFontResource from TrueType data.
    *
    * @param aData Pointer to the data
    * @param aSize Size of the TrueType data
    * @param aBackendType Type of the reference DrawTarget the font should be created for.
@@ -1721,16 +1722,17 @@ public:
 
   static already_AddRefed<ScaledFont>
     CreateScaledFontForDWriteFont(IDWriteFontFace* aFontFace,
                                   const gfxFontStyle* aStyle,
                                   const RefPtr<UnscaledFont>& aUnscaledFont,
                                   Float aSize,
                                   bool aUseEmbeddedBitmap,
                                   bool aForceGDIMode,
+                                  bool aNeedsOblique,
                                   IDWriteRenderingParams *aParams,
                                   Float aGamma,
                                   Float aContrast);
 
   static void UpdateSystemTextQuality();
 
 private:
   static StaticRefPtr<ID2D1Device> mD2D1Device;
diff --git a/gfx/2d/Factory.cpp b/gfx/2d/Factory.cpp
--- a/gfx/2d/Factory.cpp
+++ b/gfx/2d/Factory.cpp
@@ -650,35 +650,37 @@ Factory::CreateScaledFontWithCairo(const
 #else
   return nullptr;
 #endif
 }
 
 #ifdef MOZ_WIDGET_GTK
 already_AddRefed<ScaledFont>
 Factory::CreateScaledFontForFontconfigFont(cairo_scaled_font_t* aScaledFont, FcPattern* aPattern,
-                                           const RefPtr<UnscaledFont>& aUnscaledFont, Float aSize)
+                                           const RefPtr<UnscaledFont>& aUnscaledFont, Float aSize,
+                                           bool aNeedsOblique)
 {
-  return MakeAndAddRef<ScaledFontFontconfig>(aScaledFont, aPattern, aUnscaledFont, aSize);
+  return MakeAndAddRef<ScaledFontFontconfig>(aScaledFont, aPattern, aUnscaledFont, aSize, aNeedsOblique);
 }
 #endif
 
 #ifdef XP_DARWIN
 already_AddRefed<ScaledFont>
 Factory::CreateScaledFontForMacFont(CGFontRef aCGFont,
                                     const RefPtr<UnscaledFont>& aUnscaledFont,
                                     Float aSize,
                                     const Color& aFontSmoothingBackgroundColor,
                                     bool aUseFontSmoothing,
-                                    bool aApplySyntheticBold)
+                                    bool aApplySyntheticBold,
+                                    bool aNeedsOblique)
 {
   return MakeAndAddRef<ScaledFontMac>(
     aCGFont, aUnscaledFont, aSize, false,
     aFontSmoothingBackgroundColor, aUseFontSmoothing,
-    aApplySyntheticBold);
+    aApplySyntheticBold, aNeedsOblique);
 }
 #endif
 
 already_AddRefed<DrawTarget>
 Factory::CreateDualDrawTarget(DrawTarget *targetA, DrawTarget *targetB)
 {
   MOZ_ASSERT(targetA && targetB);
 
@@ -947,22 +949,23 @@ Factory::D2DCleanup()
 
 already_AddRefed<ScaledFont>
 Factory::CreateScaledFontForDWriteFont(IDWriteFontFace* aFontFace,
                                        const gfxFontStyle* aStyle,
                                        const RefPtr<UnscaledFont>& aUnscaledFont,
                                        float aSize,
                                        bool aUseEmbeddedBitmap,
                                        bool aForceGDIMode,
+                                       bool aNeedsOblique,
                                        IDWriteRenderingParams* aParams,
                                        Float aGamma,
                                        Float aContrast)
 {
   return MakeAndAddRef<ScaledFontDWrite>(aFontFace, aUnscaledFont, aSize,
-                                         aUseEmbeddedBitmap, aForceGDIMode,
+                                         aUseEmbeddedBitmap, aForceGDIMode, aNeedsOblique,
                                          aParams, aGamma, aContrast,
                                          aStyle);
 }
 
 #endif // XP_WIN
 
 #ifdef USE_SKIA_GPU
 already_AddRefed<DrawTarget>
diff --git a/gfx/2d/ScaledFontDWrite.cpp b/gfx/2d/ScaledFontDWrite.cpp
--- a/gfx/2d/ScaledFontDWrite.cpp
+++ b/gfx/2d/ScaledFontDWrite.cpp
@@ -105,24 +105,26 @@ DWriteFontStretchFromStretch(int16_t aSt
     }
 }
 
 ScaledFontDWrite::ScaledFontDWrite(IDWriteFontFace *aFontFace,
                                    const RefPtr<UnscaledFont>& aUnscaledFont,
                                    Float aSize,
                                    bool aUseEmbeddedBitmap,
                                    bool aForceGDIMode,
+                                   bool aNeedsOblique,
                                    IDWriteRenderingParams* aParams,
                                    Float aGamma,
                                    Float aContrast,
                                    const gfxFontStyle* aStyle)
     : ScaledFontBase(aUnscaledFont, aSize)
     , mFontFace(aFontFace)
     , mUseEmbeddedBitmap(aUseEmbeddedBitmap)
     , mForceGDIMode(aForceGDIMode)
+    , mNeedsOblique(aNeedsOblique)
     , mParams(aParams)
     , mGamma(aGamma)
     , mContrast(aContrast)
 {
   if (aStyle) {
     mStyle = SkFontStyle(aStyle->weight,
                          DWriteFontStretchFromStretch(aStyle->stretch),
                          aStyle->style == NS_FONT_STYLE_NORMAL ?
@@ -399,16 +401,19 @@ ScaledFontDWrite::GetWRFontInstanceOptio
     options.flags |= wr::FontInstanceFlags::SYNTHETIC_BOLD;
   }
   if (UseEmbeddedBitmaps()) {
     options.flags |= wr::FontInstanceFlags::EMBEDDED_BITMAPS;
   }
   if (ForceGDIMode()) {
     options.flags |= wr::FontInstanceFlags::FORCE_GDI;
   }
+  if (mNeedsOblique) {
+    options.flags |= wr::FontInstanceFlags::SYNTHETIC_ITALICS;
+  }
   options.bg_color = wr::ToColorU(Color());
   *aOutOptions = Some(options);
   return true;
 }
 
 already_AddRefed<ScaledFont>
 UnscaledFontDWrite::CreateScaledFont(Float aGlyphSize,
                                      const uint8_t* aInstanceData,
@@ -422,16 +427,17 @@ UnscaledFontDWrite::CreateScaledFont(Flo
   }
 
   const ScaledFontDWrite::InstanceData *instanceData =
     reinterpret_cast<const ScaledFontDWrite::InstanceData*>(aInstanceData);
   RefPtr<ScaledFontBase> scaledFont =
     new ScaledFontDWrite(mFontFace, this, aGlyphSize,
                          instanceData->mUseEmbeddedBitmap,
                          instanceData->mForceGDIMode,
+                         false,
                          nullptr,
                          instanceData->mGamma,
                          instanceData->mContrast);
 
   if (mNeedsCairo && !scaledFont->PopulateCairoScaledFont()) {
     gfxWarning() << "Unable to create cairo scaled font DWrite font.";
     return nullptr;
   }
diff --git a/gfx/2d/ScaledFontDWrite.h b/gfx/2d/ScaledFontDWrite.h
--- a/gfx/2d/ScaledFontDWrite.h
+++ b/gfx/2d/ScaledFontDWrite.h
@@ -25,25 +25,27 @@ public:
   MOZ_DECLARE_REFCOUNTED_VIRTUAL_TYPENAME(ScaledFontDWrite, override)
   ScaledFontDWrite(IDWriteFontFace *aFont,
                    const RefPtr<UnscaledFont>& aUnscaledFont,
                    Float aSize)
     : ScaledFontBase(aUnscaledFont, aSize)
     , mFontFace(aFont)
     , mUseEmbeddedBitmap(false)
     , mForceGDIMode(false)
+    , mNeedsOblique(false)
     , mGamma(2.2f)
     , mContrast(1.0f)
   {}
 
   ScaledFontDWrite(IDWriteFontFace *aFontFace,
                    const RefPtr<UnscaledFont>& aUnscaledFont,
                    Float aSize,
                    bool aUseEmbeddedBitmap,
                    bool aForceGDIMode,
+                   bool aNeedsOblique,
                    IDWriteRenderingParams *aParams,
                    Float aGamma,
                    Float aContrast,
                    const gfxFontStyle* aStyle = nullptr);
 
   FontType GetType() const override { return FontType::DWRITE; }
 
   already_AddRefed<Path> GetPathForGlyphs(const GlyphBuffer &aBuffer, const DrawTarget *aTarget) override;
@@ -69,16 +71,17 @@ public:
 #ifdef USE_SKIA
   SkTypeface* GetSkTypeface() override;
   SkFontStyle mStyle;
 #endif
 
   RefPtr<IDWriteFontFace> mFontFace;
   bool mUseEmbeddedBitmap;
   bool mForceGDIMode;
+  bool mNeedsOblique;
   // DrawTargetD2D1 requires the IDWriteRenderingParams,
   // but we also separately need to store the gamma and contrast
   // since Skia needs to be able to access these without having
   // to use the full set of DWrite parameters (which would be
   // required to recreate an IDWriteRenderingParams) in a
   // DrawTargetRecording playback.
   RefPtr<IDWriteRenderingParams> mParams;
   Float mGamma;
diff --git a/gfx/2d/ScaledFontFontconfig.cpp b/gfx/2d/ScaledFontFontconfig.cpp
--- a/gfx/2d/ScaledFontFontconfig.cpp
+++ b/gfx/2d/ScaledFontFontconfig.cpp
@@ -20,19 +20,21 @@ namespace gfx {
 
 // On Linux and Android our "platform" font is a cairo_scaled_font_t and we use
 // an SkFontHost implementation that allows Skia to render using this.
 // This is mainly because FT_Face is not good for sharing between libraries, which
 // is a requirement when we consider runtime switchable backends and so on
 ScaledFontFontconfig::ScaledFontFontconfig(cairo_scaled_font_t* aScaledFont,
                                            FcPattern* aPattern,
                                            const RefPtr<UnscaledFont>& aUnscaledFont,
-                                           Float aSize)
-  : ScaledFontBase(aUnscaledFont, aSize),
-    mPattern(aPattern)
+                                           Float aSize,
+                                           bool aNeedsOblique)
+  : ScaledFontBase(aUnscaledFont, aSize)
+  , mPattern(aPattern)
+  , mNeedsOblique(aNeedsOblique)
 {
   SetCairoScaledFont(aScaledFont);
   FcPatternReference(aPattern);
 }
 
 ScaledFontFontconfig::~ScaledFontFontconfig()
 {
   FcPatternDestroy(mPattern);
@@ -243,16 +245,20 @@ ScaledFontFontconfig::GetWRFontInstanceO
   options.subpx_dir = wr::SubpixelDirection::Horizontal;
   options.flags = 0;
   options.bg_color = wr::ToColorU(Color());
 
   wr::FontInstancePlatformOptions platformOptions;
   platformOptions.lcd_filter = wr::FontLCDFilter::Legacy;
   platformOptions.hinting = wr::FontHinting::Normal;
 
+  if (mNeedsOblique) {
+    options.flags |= wr::FontInstanceFlags::SYNTHETIC_ITALICS;
+  }
+
   FcBool autohint;
   if (FcPatternGetBool(mPattern, FC_AUTOHINT, 0, &autohint) == FcResultMatch && autohint) {
     options.flags |= wr::FontInstanceFlags::FORCE_AUTOHINT;
   }
   FcBool embolden;
   if (FcPatternGetBool(mPattern, FC_EMBOLDEN, 0, &embolden) == FcResultMatch && embolden) {
     options.flags |= wr::FontInstanceFlags::SYNTHETIC_BOLD;
   }
diff --git a/gfx/2d/ScaledFontFontconfig.h b/gfx/2d/ScaledFontFontconfig.h
--- a/gfx/2d/ScaledFontFontconfig.h
+++ b/gfx/2d/ScaledFontFontconfig.h
@@ -17,17 +17,18 @@ namespace gfx {
 class NativeFontResourceFontconfig;
 class UnscaledFontFontconfig;
 
 class ScaledFontFontconfig : public ScaledFontBase
 {
 public:
   MOZ_DECLARE_REFCOUNTED_VIRTUAL_TYPENAME(ScaledFontFontconfig, override)
   ScaledFontFontconfig(cairo_scaled_font_t* aScaledFont, FcPattern* aPattern,
-                       const RefPtr<UnscaledFont>& aUnscaledFont, Float aSize);
+                       const RefPtr<UnscaledFont>& aUnscaledFont, Float aSize,
+                       bool aNeedsOblique = false);
   ~ScaledFontFontconfig();
 
   FontType GetType() const override { return FontType::FONTCONFIG; }
 
 #ifdef USE_SKIA
   SkTypeface* GetSkTypeface() override;
 #endif
 
@@ -70,14 +71,15 @@ private:
 
   static already_AddRefed<ScaledFont>
     CreateFromInstanceData(const InstanceData& aInstanceData,
                            UnscaledFontFontconfig* aUnscaledFont,
                            Float aSize,
                            NativeFontResource* aNativeFontResource = nullptr);
 
   FcPattern* mPattern;
+  bool mNeedsOblique;
 };
 
 }
 }
 
 #endif /* MOZILLA_GFX_SCALEDFONTFONTCONFIG_H_ */
diff --git a/gfx/2d/ScaledFontMac.cpp b/gfx/2d/ScaledFontMac.cpp
--- a/gfx/2d/ScaledFontMac.cpp
+++ b/gfx/2d/ScaledFontMac.cpp
@@ -103,22 +103,24 @@ CreateCTFontFromCGFontWithVariations(CGF
 }
 
 ScaledFontMac::ScaledFontMac(CGFontRef aFont,
                              const RefPtr<UnscaledFont>& aUnscaledFont,
                              Float aSize,
                              bool aOwnsFont,
                              const Color &aFontSmoothingBackgroundColor,
                              bool aUseFontSmoothing,
-                             bool aApplySyntheticBold)
+                             bool aApplySyntheticBold,
+                             bool aNeedsOblique)
   : ScaledFontBase(aUnscaledFont, aSize)
   , mFont(aFont)
   , mFontSmoothingBackgroundColor(aFontSmoothingBackgroundColor)
   , mUseFontSmoothing(aUseFontSmoothing)
   , mApplySyntheticBold(aApplySyntheticBold)
+  , mNeedsOblique(aNeedsOblique)
 {
   if (!sSymbolLookupDone) {
     CTFontDrawGlyphsPtr =
       (CTFontDrawGlyphsFuncT*)dlsym(RTLD_DEFAULT, "CTFontDrawGlyphs");
     sSymbolLookupDone = true;
   }
 
   if (!aOwnsFont) {
@@ -410,16 +412,19 @@ ScaledFontMac::GetWRFontInstanceOptions(
   options.subpx_dir = wr::SubpixelDirection::Horizontal;
   options.flags = 0;
   if (mUseFontSmoothing) {
     options.flags |= wr::FontInstanceFlags::FONT_SMOOTHING;
   }
   if (mApplySyntheticBold) {
     options.flags |= wr::FontInstanceFlags::SYNTHETIC_BOLD;
   }
+  if (mNeedsOblique) {
+    options.flags |= wr::FontInstanceFlags::SYNTHETIC_ITALICS;
+  }
   options.bg_color = wr::ToColorU(mFontSmoothingBackgroundColor);
   *aOutOptions = Some(options);
   return true;
 }
 
 static CFDictionaryRef
 CreateVariationDictionaryOrNull(CGFontRef aCGFont, uint32_t aVariationCount,
                                 const FontVariation* aVariations)
diff --git a/gfx/2d/ScaledFontMac.h b/gfx/2d/ScaledFontMac.h
--- a/gfx/2d/ScaledFontMac.h
+++ b/gfx/2d/ScaledFontMac.h
@@ -23,17 +23,19 @@ namespace gfx {
 
 class ScaledFontMac : public ScaledFontBase
 {
 public:
   MOZ_DECLARE_REFCOUNTED_VIRTUAL_TYPENAME(ScaledFontMac, override)
   ScaledFontMac(CGFontRef aFont, const RefPtr<UnscaledFont>& aUnscaledFont, Float aSize,
                 bool aOwnsFont = false,
                 const Color &aFontSmoothingBackgroundColor = Color(),
-                bool aUseFontSmoothing = true, bool aApplySyntheticBold = false);
+                bool aUseFontSmoothing = true,
+                bool aApplySyntheticBold = false,
+                bool aNeedsOblique = false);
   ~ScaledFontMac();
 
   FontType GetType() const override { return FontType::MAC; }
 #ifdef USE_SKIA
   SkTypeface* GetSkTypeface() override;
 #endif
   already_AddRefed<Path> GetPathForGlyphs(const GlyphBuffer &aBuffer, const DrawTarget *aTarget) override;
 
@@ -53,16 +55,17 @@ public:
 
 private:
   friend class DrawTargetSkia;
   CGFontRef mFont;
   CTFontRef mCTFont; // only created if CTFontDrawGlyphs is available, otherwise null
   Color mFontSmoothingBackgroundColor;
   bool mUseFontSmoothing;
   bool mApplySyntheticBold;
+  bool mNeedsOblique;
 
   typedef void (CTFontDrawGlyphsFuncT)(CTFontRef,
                                        const CGGlyph[], const CGPoint[],
                                        size_t, CGContextRef);
 
   static bool sSymbolLookupDone;
 
 public:
diff --git a/gfx/thebes/gfxDWriteFonts.cpp b/gfx/thebes/gfxDWriteFonts.cpp
--- a/gfx/thebes/gfxDWriteFonts.cpp
+++ b/gfx/thebes/gfxDWriteFonts.cpp
@@ -72,29 +72,20 @@ gfxDWriteFont::gfxDWriteFont(const RefPt
                              gfxFontEntry *aFontEntry,
                              const gfxFontStyle *aFontStyle,
                              bool aNeedsBold,
                              AntialiasOption anAAOption)
     : gfxFont(aUnscaledFont, aFontEntry, aFontStyle, anAAOption)
     , mCairoFontFace(nullptr)
     , mMetrics(nullptr)
     , mSpaceGlyph(0)
-    , mNeedsOblique(false)
     , mNeedsBold(aNeedsBold)
     , mUseSubpixelPositions(false)
     , mAllowManualShowGlyphs(true)
 {
-    if ((GetStyle()->style != NS_FONT_STYLE_NORMAL) &&
-        aFontEntry->IsUpright() &&
-        GetStyle()->allowSyntheticStyle) {
-            // For this we always use the font_matrix for uniformity. Not the
-            // DWrite simulation.
-            mNeedsOblique = true;
-    }
-
     mFontFace = aUnscaledFont->GetFontFace();
 
     // If the IDWriteFontFace1 interface is available, we can use that for
     // faster glyph width retrieval.
     mFontFace->QueryInterface(__uuidof(IDWriteFontFace1),
                               (void**)getter_AddRefs(mFontFace1));
 
     ComputeMetrics(anAAOption);
@@ -690,16 +681,17 @@ gfxDWriteFont::GetScaledFont(mozilla::gf
 
         const gfxFontStyle* fontStyle = GetStyle();
         mAzureScaledFont =
             Factory::CreateScaledFontForDWriteFont(mFontFace, fontStyle,
                                                    GetUnscaledFont(),
                                                    GetAdjustedSize(),
                                                    useEmbeddedBitmap,
                                                    forceGDI,
+                                                   IsSyntheticOblique(),
                                                    params,
                                                    params->GetGamma(),
                                                    params->GetEnhancedContrast());
         if (!mAzureScaledFont) {
             return nullptr;
         }
     }
 
diff --git a/gfx/thebes/gfxDWriteFonts.h b/gfx/thebes/gfxDWriteFonts.h
--- a/gfx/thebes/gfxDWriteFonts.h
+++ b/gfx/thebes/gfxDWriteFonts.h
@@ -96,17 +96,16 @@ protected:
 
     Metrics *mMetrics;
 
     // cache of glyph widths in 16.16 fixed-point pixels
     mozilla::UniquePtr<nsDataHashtable<nsUint32HashKey,int32_t>> mGlyphWidths;
 
     uint32_t mSpaceGlyph;
 
-    bool mNeedsOblique;
     bool mNeedsBold;
     bool mUseSubpixelPositions;
     bool mAllowManualShowGlyphs;
 
     static bool sUseClearType;
 };
 
 #endif
diff --git a/gfx/thebes/gfxFcPlatformFontList.cpp b/gfx/thebes/gfxFcPlatformFontList.cpp
--- a/gfx/thebes/gfxFcPlatformFontList.cpp
+++ b/gfx/thebes/gfxFcPlatformFontList.cpp
@@ -1303,17 +1303,18 @@ gfxFontconfigFont::~gfxFontconfigFont()
 already_AddRefed<ScaledFont>
 gfxFontconfigFont::GetScaledFont(mozilla::gfx::DrawTarget *aTarget)
 {
     if (!mAzureScaledFont) {
         mAzureScaledFont =
             Factory::CreateScaledFontForFontconfigFont(GetCairoScaledFont(),
                                                        GetPattern(),
                                                        GetUnscaledFont(),
-                                                       GetAdjustedSize());
+                                                       GetAdjustedSize(),
+                                                       IsSyntheticOblique());
     }
 
     RefPtr<ScaledFont> scaledFont(mAzureScaledFont);
     return scaledFont.forget();
 }
 
 gfxFcPlatformFontList::gfxFcPlatformFontList()
     : mLocalNames(64)
diff --git a/gfx/thebes/gfxFont.cpp b/gfx/thebes/gfxFont.cpp
--- a/gfx/thebes/gfxFont.cpp
+++ b/gfx/thebes/gfxFont.cpp
@@ -1917,22 +1917,24 @@ void
 gfxFont::DrawOneGlyph(uint32_t aGlyphID, const gfx::Point& aPt,
                       GlyphBufferAzure& aBuffer, bool *aEmittedGlyphs) const
 {
     const TextRunDrawParams& runParams(aBuffer.mRunParams);
 
     gfx::Point devPt(ToDeviceUnits(aPt.x, runParams.devPerApp),
                      ToDeviceUnits(aPt.y, runParams.devPerApp));
 
-    gfxContextMatrixAutoSaveRestore matrixRestore;
-
     if (FC == FontComplexityT::ComplexFont) {
         const FontDrawParams& fontParams(aBuffer.mFontParams);
 
-        if (fontParams.needsOblique && fontParams.isVerticalFont) {
+        auto* textDrawer = runParams.context->GetTextDrawer();
+
+        gfxContextMatrixAutoSaveRestore matrixRestore;
+
+        if (fontParams.needsOblique && fontParams.isVerticalFont && !textDrawer) {
             // We have to flush each glyph individually when doing
             // synthetic-oblique for vertical-upright text, because
             // the skew transform needs to be applied to a separate
             // origin for each glyph, not once for the whole run.
             aBuffer.Flush();
             matrixRestore.SetContext(runParams.context);
             gfx::Matrix mat =
                 runParams.context->CurrentMatrix().
@@ -1959,51 +1961,51 @@ gfxFont::DrawOneGlyph(uint32_t aGlyphID,
         if (fontParams.haveColorGlyphs &&
             RenderColorGlyph(runParams.dt, runParams.context,
                              fontParams.scaledFont,
                              fontParams.drawOptions,
                              devPt,
                              aGlyphID)) {
             return;
         }
-    }
-
-    aBuffer.OutputGlyph(aGlyphID, devPt);
-
-    if (FC == FontComplexityT::ComplexFont) {
-        const FontDrawParams& fontParams(aBuffer.mFontParams);
+
+        aBuffer.OutputGlyph(aGlyphID, devPt);
+
         // Synthetic bolding (if required) by multi-striking.
         for (int32_t i = 0; i < fontParams.extraStrikes; ++i) {
             if (fontParams.isVerticalFont) {
                 devPt.y += fontParams.synBoldOnePixelOffset;
             } else {
                 devPt.x += fontParams.synBoldOnePixelOffset;
             }
             aBuffer.OutputGlyph(aGlyphID, devPt);
         }
 
-        if (fontParams.needsOblique && fontParams.isVerticalFont) {
+        if (fontParams.needsOblique && fontParams.isVerticalFont && !textDrawer) {
             aBuffer.Flush();
         }
+    } else {
+        aBuffer.OutputGlyph(aGlyphID, devPt);
     }
 
     *aEmittedGlyphs = true;
 }
 
 bool
 gfxFont::DrawMissingGlyph(const TextRunDrawParams&            aRunParams,
                           const FontDrawParams&               aFontParams,
                           const gfxShapedText::DetailedGlyph* aDetails,
                           const gfx::Point&                   aPt)
 {
     // Default-ignorable chars will have zero advance width;
     // we don't have to draw the hexbox for them.
     float advance = aDetails->mAdvance;
     if (aRunParams.drawMode != DrawMode::GLYPH_PATH && advance > 0) {
-        if (auto* textDrawer = aRunParams.context->GetTextDrawer()) {
+        auto* textDrawer = aRunParams.context->GetTextDrawer();
+        if (textDrawer) {
             textDrawer->FoundUnsupportedFeature();
             return false;
         }
 
         Point pt(Float(ToDeviceUnits(aPt.x, aRunParams.devPerApp)),
                  Float(ToDeviceUnits(aPt.y, aRunParams.devPerApp)));
         Float advanceDevUnits =
             Float(ToDeviceUnits(advance, aRunParams.devPerApp));
@@ -2013,17 +2015,17 @@ gfxFont::DrawMissingGlyph(const TextRunD
                  height, advanceDevUnits) :
             Rect(pt.x, pt.y - height,
                  advanceDevUnits, height);
 
         // If there's a fake-italic skew in effect as part
         // of the drawTarget's transform, we need to undo
         // this before drawing the hexbox. (Bug 983985)
         gfxContextMatrixAutoSaveRestore matrixRestore;
-        if (aFontParams.needsOblique && !aFontParams.isVerticalFont) {
+        if (aFontParams.needsOblique && !aFontParams.isVerticalFont && !textDrawer) {
             matrixRestore.SetContext(aRunParams.context);
             gfx::Matrix mat =
                 aRunParams.context->CurrentMatrix().
                 PreTranslate(pt).
                 PreMultiply(gfx::Matrix(1, 0, OBLIQUE_SKEW_FACTOR, 1, 0, 0)).
                 PreTranslate(-pt);
             aRunParams.context->SetMatrix(mat);
         }
@@ -2097,22 +2099,20 @@ gfxFont::Draw(const gfxTextRun *aTextRun
 
     fontParams.scaledFont = GetScaledFont(aRunParams.dt);
     if (!fontParams.scaledFont) {
         return;
     }
 
     auto* textDrawer = aRunParams.context->GetTextDrawer();
 
-    fontParams.needsOblique = mFontEntry->IsUpright() &&
-                              mStyle.style != NS_FONT_STYLE_NORMAL &&
-                              mStyle.allowSyntheticStyle;
+    fontParams.needsOblique = IsSyntheticOblique();
     fontParams.haveSVGGlyphs = GetFontEntry()->TryGetSVGData(this);
 
-    if ((fontParams.needsOblique || fontParams.haveSVGGlyphs) && textDrawer) {
+    if (fontParams.haveSVGGlyphs && textDrawer) {
         textDrawer->FoundUnsupportedFeature();
         return;
     }
 
     fontParams.haveColorGlyphs = GetFontEntry()->TryGetColorGlyphs();
     fontParams.contextPaint = aRunParams.runContextPaint;
     fontParams.isVerticalFont =
         aOrientation == gfx::ShapedTextFlags::TEXT_ORIENT_VERTICAL_UPRIGHT;
@@ -2157,17 +2157,17 @@ gfxFont::Draw(const gfxTextRun *aTextRun
         if (aTextRun->UseCenterBaseline()) {
             gfxPoint baseAdj(0, (metrics.emAscent - metrics.emDescent) / 2);
             mat.PreTranslate(baseAdj);
         }
 
         aRunParams.context->SetMatrixDouble(mat);
     }
 
-    if (fontParams.needsOblique && !fontParams.isVerticalFont) {
+    if (fontParams.needsOblique && !fontParams.isVerticalFont && !textDrawer) {
         // Adjust matrix for synthetic-oblique, except if we're doing vertical-
         // upright text, in which case this will be handled for each glyph
         // individually in DrawOneGlyph.
         if (!matrixRestore.HasMatrix()) {
             matrixRestore.SetContext(aRunParams.context);
         }
         gfx::Point p(aPt->x * aRunParams.devPerApp,
                      aPt->y * aRunParams.devPerApp);
diff --git a/gfx/thebes/gfxFont.h b/gfx/thebes/gfxFont.h
--- a/gfx/thebes/gfxFont.h
+++ b/gfx/thebes/gfxFont.h
@@ -1792,16 +1792,22 @@ public:
 
     // This is called by the default Draw() implementation above.
     virtual bool SetupCairoFont(DrawTarget* aDrawTarget) = 0;
 
     virtual bool AllowSubpixelAA() { return true; }
 
     bool IsSyntheticBold() { return mApplySyntheticBold; }
 
+    bool IsSyntheticOblique() {
+        return mFontEntry->IsUpright() &&
+               mStyle.style != NS_FONT_STYLE_NORMAL &&
+               mStyle.allowSyntheticStyle;
+    }
+
     // Amount by which synthetic bold "fattens" the glyphs:
     // For size S up to a threshold size T, we use (0.25 + 3S / 4T),
     // so that the result ranges from 0.25 to 1.0; thereafter,
     // simply use (S / T).
     gfxFloat GetSyntheticBoldOffset() {
         gfxFloat size = GetAdjustedSize();
         const gfxFloat threshold = 48.0;
         return size < threshold ? (0.25 + 0.75 * size / threshold) :
diff --git a/gfx/thebes/gfxMacFont.cpp b/gfx/thebes/gfxMacFont.cpp
--- a/gfx/thebes/gfxMacFont.cpp
+++ b/gfx/thebes/gfxMacFont.cpp
@@ -504,17 +504,18 @@ gfxMacFont::GetScaledFont(DrawTarget *aT
 {
     if (!mAzureScaledFont) {
         mAzureScaledFont =
             Factory::CreateScaledFontForMacFont(GetCGFontRef(),
                                                 GetUnscaledFont(),
                                                 GetAdjustedSize(),
                                                 Color::FromABGR(mFontSmoothingBackgroundColor),
                                                 !mStyle.useGrayscaleAntialiasing,
-                                                IsSyntheticBold());
+                                                IsSyntheticBold(),
+                                                IsSyntheticOblique());
         if (!mAzureScaledFont) {
             return nullptr;
         }
 
         mAzureScaledFont->SetCairoScaledFont(mScaledFont);
     }
 
     RefPtr<ScaledFont> scaledFont(mAzureScaledFont);
