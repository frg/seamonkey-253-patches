# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1504750851 -36000
# Node ID cb6fcd43e5a505475df2c38a8475bc84d485975b
# Parent  89f2c49ec3e94c8ddd457ba8d8741961e76a49bb
Bug 1393642 (follow-up) - Fix potential leak in HTMLEditor methods. r=masayuki.

As written, these functions will leak if they are passed strings that don't
match static atoms. In practice, all strings passed *do* match static atoms,
but let's fix it anyway in case that changes in the future.

diff --git a/editor/libeditor/HTMLStyleEditor.cpp b/editor/libeditor/HTMLStyleEditor.cpp
--- a/editor/libeditor/HTMLStyleEditor.cpp
+++ b/editor/libeditor/HTMLStyleEditor.cpp
@@ -54,17 +54,18 @@ IsEmptyTextNode(HTMLEditor* aThis, nsINo
          isEmptyTextNode;
 }
 
 NS_IMETHODIMP
 HTMLEditor::SetInlineProperty(const nsAString& aProperty,
                               const nsAString& aAttribute,
                               const nsAString& aValue)
 {
-  return SetInlineProperty(NS_Atomize(aProperty).take(), aAttribute, aValue);
+  nsCOMPtr<nsIAtom> property = NS_Atomize(aProperty);
+  return SetInlineProperty(property, aAttribute, aValue);
 }
 
 nsresult
 HTMLEditor::SetInlineProperty(nsIAtom* aProperty,
                               const nsAString& aAttribute,
                               const nsAString& aValue)
 {
   NS_ENSURE_TRUE(aProperty, NS_ERROR_NULL_POINTER);
@@ -1073,18 +1074,18 @@ HTMLEditor::GetInlinePropertyBase(nsIAto
 NS_IMETHODIMP
 HTMLEditor::GetInlineProperty(const nsAString& aProperty,
                               const nsAString& aAttribute,
                               const nsAString& aValue,
                               bool* aFirst,
                               bool* aAny,
                               bool* aAll)
 {
-  return GetInlineProperty(NS_Atomize(aProperty).take(), aAttribute, aValue,
-                           aFirst, aAny, aAll);
+  nsCOMPtr<nsIAtom> property = NS_Atomize(aProperty);
+  return GetInlineProperty(property, aAttribute, aValue, aFirst, aAny, aAll);
 }
 
 nsresult
 HTMLEditor::GetInlineProperty(nsIAtom* aProperty,
                               const nsAString& aAttribute,
                               const nsAString& aValue,
                               bool* aFirst,
                               bool* aAny,
@@ -1104,19 +1105,19 @@ NS_IMETHODIMP
 HTMLEditor::GetInlinePropertyWithAttrValue(const nsAString& aProperty,
                                            const nsAString& aAttribute,
                                            const nsAString& aValue,
                                            bool* aFirst,
                                            bool* aAny,
                                            bool* aAll,
                                            nsAString& outValue)
 {
-  return GetInlinePropertyWithAttrValue(NS_Atomize(aProperty).take(),
-                                        aAttribute, aValue, aFirst, aAny,
-                                        aAll, outValue);
+  nsCOMPtr<nsIAtom> property = NS_Atomize(aProperty);
+  return GetInlinePropertyWithAttrValue(property, aAttribute, aValue, aFirst,
+                                        aAny, aAll, outValue);
 }
 
 nsresult
 HTMLEditor::GetInlinePropertyWithAttrValue(nsIAtom* aProperty,
                                            const nsAString& aAttribute,
                                            const nsAString& aValue,
                                            bool* aFirst,
                                            bool* aAny,
@@ -1144,17 +1145,18 @@ HTMLEditor::RemoveAllInlineProperties()
   NS_ENSURE_SUCCESS(rv, rv);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 HTMLEditor::RemoveInlineProperty(const nsAString& aProperty,
                                  const nsAString& aAttribute)
 {
-  return RemoveInlineProperty(NS_Atomize(aProperty).take(), aAttribute);
+  nsCOMPtr<nsIAtom> property = NS_Atomize(aProperty);
+  return RemoveInlineProperty(property, aAttribute);
 }
 
 nsresult
 HTMLEditor::RemoveInlineProperty(nsIAtom* aProperty,
                                  const nsAString& aAttribute)
 {
   return RemoveInlinePropertyImpl(aProperty, &aAttribute);
 }
