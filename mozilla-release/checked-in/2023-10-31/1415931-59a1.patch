# HG changeset patch
# User Dragan Mladjenovic <dragan.mladjenovic@rt-rk.com>
# Date 1510604769 18000
# Node ID afbde1e5a67d2fe8dd8763f69f1ed4ddf4422b68
# Parent  3b54f03895db4c807bcc13ef143208405545cd90
Bug 1415931 - [MIPS] Make long jumps visible to Wasm module code linking and caching. r=bbouvier

diff --git a/js/src/jit/mips-shared/Assembler-mips-shared.h b/js/src/jit/mips-shared/Assembler-mips-shared.h
--- a/js/src/jit/mips-shared/Assembler-mips-shared.h
+++ b/js/src/jit/mips-shared/Assembler-mips-shared.h
@@ -872,17 +872,16 @@ class AssemblerMIPSShared : public Assem
         RelativePatch(BufferOffset offset, void* target, Relocation::Kind kind)
           : offset(offset),
             target(target),
             kind(kind)
         { }
     };
 
     js::Vector<RelativePatch, 8, SystemAllocPolicy> jumps_;
-    js::Vector<uint32_t, 8, SystemAllocPolicy> longJumps_;
 
     CompactBufferWriter jumpRelocations_;
     CompactBufferWriter dataRelocations_;
 
     MIPSBufferWithExecutableCopy m_buffer;
 
   public:
     AssemblerMIPSShared()
@@ -1275,28 +1274,23 @@ class AssemblerMIPSShared : public Assem
   protected:
     InstImm invertBranch(InstImm branch, BOffImm16 skipOffset);
     void addPendingJump(BufferOffset src, ImmPtr target, Relocation::Kind kind) {
         enoughMemory_ &= jumps_.append(RelativePatch(src, target.value, kind));
         if (kind == Relocation::JITCODE)
             writeRelocation(src);
     }
 
-    void addLongJump(BufferOffset src) {
-        enoughMemory_ &= longJumps_.append(src.getOffset());
+    void addLongJump(BufferOffset src, BufferOffset dst) {
+        CodeOffset patchAt(src.getOffset());
+        CodeOffset target(dst.getOffset());
+        addCodeLabel(CodeLabel(patchAt, target));
     }
 
   public:
-    size_t numLongJumps() const {
-        return longJumps_.length();
-    }
-    uint32_t longJump(size_t i) {
-        return longJumps_[i];
-    }
-
     void flushBuffer() {
     }
 
     void comment(const char* msg) {
         spew("; %s", msg);
     }
 
     static uint32_t NopSize() { return 4; }
diff --git a/js/src/jit/mips32/Assembler-mips32.cpp b/js/src/jit/mips32/Assembler-mips32.cpp
--- a/js/src/jit/mips32/Assembler-mips32.cpp
+++ b/js/src/jit/mips32/Assembler-mips32.cpp
@@ -158,24 +158,16 @@ jit::PatchBackedge(CodeLocationJump& jum
 }
 
 void
 Assembler::executableCopy(uint8_t* buffer, bool flushICache)
 {
     MOZ_ASSERT(isFinished);
     m_buffer.executableCopy(buffer);
 
-    // Patch all long jumps during code copy.
-    for (size_t i = 0; i < longJumps_.length(); i++) {
-        Instruction* inst1 = (Instruction*) ((uint32_t)buffer + longJumps_[i]);
-
-        uint32_t value = Assembler::ExtractLuiOriValue(inst1, inst1->next());
-        AssemblerMIPSShared::UpdateLuiOriValue(inst1, inst1->next(), (uint32_t)buffer + value);
-    }
-
     if (flushICache)
         AutoFlushICache::setRange(uintptr_t(buffer), m_buffer.size());
 }
 
 uintptr_t
 Assembler::GetPointer(uint8_t* instPtr)
 {
     Instruction* inst = (Instruction*)instPtr;
@@ -324,18 +316,19 @@ Assembler::bind(InstImm* inst, uintptr_t
         inst[0].setBOffImm16(BOffImm16(offset));
         inst[1].makeNop();
         return;
     }
 
     // Generate the long jump for calls because return address has to be the
     // address after the reserved block.
     if (inst[0].encode() == inst_bgezal.encode()) {
-        addLongJump(BufferOffset(branch));
-        Assembler::WriteLuiOriInstructions(inst, &inst[1], ScratchRegister, target);
+        addLongJump(BufferOffset(branch), BufferOffset(target));
+        Assembler::WriteLuiOriInstructions(inst, &inst[1], ScratchRegister,
+                                           LabelBase::INVALID_OFFSET);
         inst[2] = InstReg(op_special, ScratchRegister, zero, ra, ff_jalr).encode();
         // There is 1 nop after this.
         return;
     }
 
     if (BOffImm16::IsInRange(offset)) {
         bool conditional = (inst[0].encode() != inst_bgezal.encode() &&
                             inst[0].encode() != inst_beq.encode());
@@ -348,26 +341,28 @@ Assembler::bind(InstImm* inst, uintptr_t
             inst[2] = InstImm(op_regimm, zero, rt_bgez, BOffImm16(3 * sizeof(void*))).encode();
             // There are 2 nops after this
         }
         return;
     }
 
     if (inst[0].encode() == inst_beq.encode()) {
         // Handle long unconditional jump.
-        addLongJump(BufferOffset(branch));
-        Assembler::WriteLuiOriInstructions(inst, &inst[1], ScratchRegister, target);
+        addLongJump(BufferOffset(branch), BufferOffset(target));
+        Assembler::WriteLuiOriInstructions(inst, &inst[1], ScratchRegister,
+                                           LabelBase::INVALID_OFFSET);
         inst[2] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         // There is 1 nop after this.
     } else {
         // Handle long conditional jump.
         inst[0] = invertBranch(inst[0], BOffImm16(5 * sizeof(void*)));
         // No need for a "nop" here because we can clobber scratch.
-        addLongJump(BufferOffset(branch + sizeof(void*)));
-        Assembler::WriteLuiOriInstructions(&inst[1], &inst[2], ScratchRegister, target);
+        addLongJump(BufferOffset(branch + sizeof(void*)), BufferOffset(target));
+        Assembler::WriteLuiOriInstructions(&inst[1], &inst[2], ScratchRegister,
+                                           LabelBase::INVALID_OFFSET);
         inst[3] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         // There is 1 nop after this.
     }
 }
 
 void
 Assembler::bind(RepatchLabel* label)
 {
@@ -381,17 +376,17 @@ Assembler::bind(RepatchLabel* label)
         uint32_t offset = dest.getOffset() - label->offset();
 
         // If first instruction is lui, then this is a long jump.
         // If second instruction is lui, then this is a loop backedge.
         if (inst[0].extractOpcode() == (uint32_t(op_lui) >> OpcodeShift)) {
             // For unconditional long branches generated by ma_liPatchable,
             // such as under:
             //     jumpWithpatch
-            AssemblerMIPSShared::UpdateLuiOriValue(inst, inst->next(), dest.getOffset());
+            addLongJump(BufferOffset(label->offset()), dest);
         } else if (inst[1].extractOpcode() == (uint32_t(op_lui) >> OpcodeShift) ||
                    BOffImm16::IsInRange(offset))
         {
             // Handle code produced by:
             //     backedgeJump
             //     branchWithCode
             MOZ_ASSERT(BOffImm16::IsInRange(offset));
             MOZ_ASSERT(inst[0].extractOpcode() == (uint32_t(op_beq) >> OpcodeShift) ||
@@ -402,32 +397,34 @@ Assembler::bind(RepatchLabel* label)
         } else if (inst[0].encode() == inst_beq.encode()) {
             // Handle open long unconditional jumps created by
             // MacroAssemblerMIPSShared::ma_b(..., wasm::Trap, ...).
             // We need to add it to long jumps array here.
             // See MacroAssemblerMIPS::branchWithCode().
             MOZ_ASSERT(inst[1].encode() == NopInst);
             MOZ_ASSERT(inst[2].encode() == NopInst);
             MOZ_ASSERT(inst[3].encode() == NopInst);
-            addLongJump(BufferOffset(label->offset()));
-            Assembler::WriteLuiOriInstructions(inst, &inst[1], ScratchRegister, dest.getOffset());
+            addLongJump(BufferOffset(label->offset()), dest);
+            Assembler::WriteLuiOriInstructions(inst, &inst[1], ScratchRegister,
+                                               LabelBase::INVALID_OFFSET);
             inst[2] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         } else {
             // Handle open long conditional jumps created by
             // MacroAssemblerMIPSShared::ma_b(..., wasm::Trap, ...).
             inst[0] = invertBranch(inst[0], BOffImm16(5 * sizeof(void*)));
             // No need for a "nop" here because we can clobber scratch.
             // We need to add it to long jumps array here.
             // See MacroAssemblerMIPS::branchWithCode().
             MOZ_ASSERT(inst[1].encode() == NopInst);
             MOZ_ASSERT(inst[2].encode() == NopInst);
             MOZ_ASSERT(inst[3].encode() == NopInst);
             MOZ_ASSERT(inst[4].encode() == NopInst);
-            addLongJump(BufferOffset(label->offset() + sizeof(void*)));
-            Assembler::WriteLuiOriInstructions(&inst[1], &inst[2], ScratchRegister, dest.getOffset());
+            addLongJump(BufferOffset(label->offset() + sizeof(void*)), dest);
+            Assembler::WriteLuiOriInstructions(&inst[1], &inst[2], ScratchRegister,
+                                               LabelBase::INVALID_OFFSET);
             inst[3] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         }
     }
     label->bind(dest.getOffset());
 }
 
 void
 Assembler::processCodeLabels(uint8_t* rawCode)
diff --git a/js/src/jit/mips32/MacroAssembler-mips32.cpp b/js/src/jit/mips32/MacroAssembler-mips32.cpp
--- a/js/src/jit/mips32/MacroAssembler-mips32.cpp
+++ b/js/src/jit/mips32/MacroAssembler-mips32.cpp
@@ -506,18 +506,18 @@ MacroAssemblerMIPS::ma_b(Address addr, I
 
 void
 MacroAssemblerMIPS::ma_bal(Label* label, DelaySlotFill delaySlotFill)
 {
     spew("branch .Llabel %p\n", label);
     if (label->bound()) {
         // Generate the long jump for calls because return address has to be
         // the address after the reserved block.
-        addLongJump(nextOffset());
-        ma_liPatchable(ScratchRegister, Imm32(label->offset()));
+        addLongJump(nextOffset(), BufferOffset(label->offset()));
+        ma_liPatchable(ScratchRegister, Imm32(LabelBase::INVALID_OFFSET));
         as_jalr(ScratchRegister);
         if (delaySlotFill == FillDelaySlot)
             as_nop();
         return;
     }
 
     // Second word holds a pointer to the next branch in label's chain.
     uint32_t nextInChain = label->used() ? label->offset() : LabelBase::INVALID_OFFSET;
@@ -557,34 +557,34 @@ MacroAssemblerMIPS::branchWithCode(InstI
 #endif
             writeInst(code.encode());
             as_nop();
             return;
         }
 
         if (code.encode() == inst_beq.encode()) {
             // Handle long jump
-            addLongJump(nextOffset());
-            ma_liPatchable(ScratchRegister, Imm32(label->offset()));
+            addLongJump(nextOffset(), BufferOffset(label->offset()));
+            ma_liPatchable(ScratchRegister, Imm32(LabelBase::INVALID_OFFSET));
             as_jr(ScratchRegister);
             as_nop();
             return;
         }
 
         // Handle long conditional branch
         spew("invert branch .Llabel %p", label);
         InstImm code_r = invertBranch(code, BOffImm16(5 * sizeof(uint32_t)));
 #ifdef JS_JITSPEW
         decodeBranchInstAndSpew(code_r);
 #endif
         writeInst(code_r.encode());
 
         // No need for a "nop" here because we can clobber scratch.
-        addLongJump(nextOffset());
-        ma_liPatchable(ScratchRegister, Imm32(label->offset()));
+        addLongJump(nextOffset(), BufferOffset(label->offset()));
+        ma_liPatchable(ScratchRegister, Imm32(LabelBase::INVALID_OFFSET));
         as_jr(ScratchRegister);
         as_nop();
         return;
     }
 
     // Generate open jump and link it to a label.
 
     // Second word holds a pointer to the next branch in label's chain.
@@ -1545,53 +1545,45 @@ MacroAssemblerMIPSCompat::moveData(const
  * The backedge is done this way to avoid patching lui+ori pair while it is
  * being executed. Look also at jit::PatchBackedge().
  */
 CodeOffsetJump
 MacroAssemblerMIPSCompat::backedgeJump(RepatchLabel* label, Label* documentation)
 {
     // Only one branch per label.
     MOZ_ASSERT(!label->used());
-    uint32_t dest = label->bound() ? label->offset() : LabelBase::INVALID_OFFSET;
+
     BufferOffset bo = nextOffset();
     label->use(bo.getOffset());
 
     // Backedges are short jumps when bound, but can become long when patched.
     m_buffer.ensureSpace(8 * sizeof(uint32_t));
-    if (label->bound()) {
-        int32_t offset = label->offset() - bo.getOffset();
-        MOZ_ASSERT(BOffImm16::IsInRange(offset));
-        as_b(BOffImm16(offset));
-    } else {
-        // Jump to "label1" by default to jump to the loop header.
-        as_b(BOffImm16(2 * sizeof(uint32_t)));
-    }
+    // Jump to "label1" by default to jump to the loop header.
+    as_b(BOffImm16(2 * sizeof(uint32_t)));
     // No need for nop here. We can safely put next instruction in delay slot.
-    ma_liPatchable(ScratchRegister, Imm32(dest));
+    ma_liPatchable(ScratchRegister, Imm32(LabelBase::INVALID_OFFSET));
     MOZ_ASSERT(nextOffset().getOffset() - bo.getOffset() == 3 * sizeof(uint32_t));
     as_jr(ScratchRegister);
     // No need for nop here. We can safely put next instruction in delay slot.
-    ma_liPatchable(ScratchRegister, Imm32(dest));
+    ma_liPatchable(ScratchRegister, Imm32(LabelBase::INVALID_OFFSET));
     as_jr(ScratchRegister);
     as_nop();
     MOZ_ASSERT(nextOffset().getOffset() - bo.getOffset() == 8 * sizeof(uint32_t));
     return CodeOffsetJump(bo.getOffset());
 }
 
 CodeOffsetJump
 MacroAssemblerMIPSCompat::jumpWithPatch(RepatchLabel* label, Label* documentation)
 {
     // Only one branch per label.
     MOZ_ASSERT(!label->used());
-    uint32_t dest = label->bound() ? label->offset() : LabelBase::INVALID_OFFSET;
 
     BufferOffset bo = nextOffset();
     label->use(bo.getOffset());
-    addLongJump(bo);
-    ma_liPatchable(ScratchRegister, Imm32(dest));
+    ma_liPatchable(ScratchRegister, Imm32(LabelBase::INVALID_OFFSET));
     as_jr(ScratchRegister);
     as_nop();
     return CodeOffsetJump(bo.getOffset());
 }
 
 /////////////////////////////////////////////////////////////////
 // X86/X64-common/ARM/MIPS interface.
 /////////////////////////////////////////////////////////////////
diff --git a/js/src/jit/mips64/Assembler-mips64.cpp b/js/src/jit/mips64/Assembler-mips64.cpp
--- a/js/src/jit/mips64/Assembler-mips64.cpp
+++ b/js/src/jit/mips64/Assembler-mips64.cpp
@@ -126,24 +126,16 @@ jit::PatchBackedge(CodeLocationJump& jum
 }
 
 void
 Assembler::executableCopy(uint8_t* buffer, bool flushICache)
 {
     MOZ_ASSERT(isFinished);
     m_buffer.executableCopy(buffer);
 
-    // Patch all long jumps during code copy.
-    for (size_t i = 0; i < longJumps_.length(); i++) {
-        Instruction* inst = (Instruction*) ((uintptr_t)buffer + longJumps_[i]);
-
-        uint64_t value = Assembler::ExtractLoad64Value(inst);
-        Assembler::UpdateLoad64Value(inst, (uint64_t)buffer + value);
-    }
-
     if (flushICache)
         AutoFlushICache::setRange(uintptr_t(buffer), m_buffer.size());
 }
 
 uintptr_t
 Assembler::GetPointer(uint8_t* instPtr)
 {
     Instruction* inst = (Instruction*)instPtr;
@@ -257,18 +249,18 @@ Assembler::bind(InstImm* inst, uintptr_t
         inst[0].setBOffImm16(BOffImm16(offset));
         inst[1].makeNop();
         return;
     }
 
     // Generate the long jump for calls because return address has to be the
     // address after the reserved block.
     if (inst[0].encode() == inst_bgezal.encode()) {
-        addLongJump(BufferOffset(branch));
-        Assembler::WriteLoad64Instructions(inst, ScratchRegister, target);
+        addLongJump(BufferOffset(branch), BufferOffset(target));
+        Assembler::WriteLoad64Instructions(inst, ScratchRegister, LabelBase::INVALID_OFFSET);
         inst[4] = InstReg(op_special, ScratchRegister, zero, ra, ff_jalr).encode();
         // There is 1 nop after this.
         return;
     }
 
     if (BOffImm16::IsInRange(offset)) {
         // Don't skip trailing nops can improve performance
         // on Loongson3 platform.
@@ -282,26 +274,26 @@ Assembler::bind(InstImm* inst, uintptr_t
             inst[2] = InstImm(op_regimm, zero, rt_bgez, BOffImm16(5 * sizeof(uint32_t))).encode();
             // There are 4 nops after this
         }
         return;
     }
 
     if (inst[0].encode() == inst_beq.encode()) {
         // Handle long unconditional jump.
-        addLongJump(BufferOffset(branch));
-        Assembler::WriteLoad64Instructions(inst, ScratchRegister, target);
+        addLongJump(BufferOffset(branch), BufferOffset(target));
+        Assembler::WriteLoad64Instructions(inst, ScratchRegister, LabelBase::INVALID_OFFSET);
         inst[4] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         // There is 1 nop after this.
     } else {
         // Handle long conditional jump.
         inst[0] = invertBranch(inst[0], BOffImm16(7 * sizeof(uint32_t)));
         // No need for a "nop" here because we can clobber scratch.
-        addLongJump(BufferOffset(branch + sizeof(uint32_t)));
-        Assembler::WriteLoad64Instructions(&inst[1], ScratchRegister, target);
+        addLongJump(BufferOffset(branch + sizeof(uint32_t)), BufferOffset(target));
+        Assembler::WriteLoad64Instructions(&inst[1], ScratchRegister, LabelBase::INVALID_OFFSET);
         inst[5] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         // There is 1 nop after this.
     }
 }
 
 void
 Assembler::bind(RepatchLabel* label)
 {
@@ -315,17 +307,17 @@ Assembler::bind(RepatchLabel* label)
         uint64_t offset = dest.getOffset() - label->offset();
 
         // If first instruction is lui, then this is a long jump.
         // If second instruction is lui, then this is a loop backedge.
         if (inst[0].extractOpcode() == (uint32_t(op_lui) >> OpcodeShift)) {
             // For unconditional long branches generated by ma_liPatchable,
             // such as under:
             //     jumpWithpatch
-            Assembler::UpdateLoad64Value(inst, dest.getOffset());
+            addLongJump(BufferOffset(label->offset()), dest);
         } else if (inst[1].extractOpcode() == (uint32_t(op_lui) >> OpcodeShift) ||
                    BOffImm16::IsInRange(offset))
         {
             // Handle code produced by:
             //     backedgeJump
             //     branchWithCode
             MOZ_ASSERT(BOffImm16::IsInRange(offset));
             MOZ_ASSERT(inst[0].extractOpcode() == (uint32_t(op_beq) >> OpcodeShift) ||
@@ -338,34 +330,34 @@ Assembler::bind(RepatchLabel* label)
             // MacroAssemblerMIPSShared::ma_b(..., wasm::Trap, ...).
             // We need to add it to long jumps array here.
             // See MacroAssemblerMIPS64::branchWithCode().
             MOZ_ASSERT(inst[1].encode() == NopInst);
             MOZ_ASSERT(inst[2].encode() == NopInst);
             MOZ_ASSERT(inst[3].encode() == NopInst);
             MOZ_ASSERT(inst[4].encode() == NopInst);
             MOZ_ASSERT(inst[5].encode() == NopInst);
-            addLongJump(BufferOffset(label->offset()));
-            Assembler::WriteLoad64Instructions(inst, ScratchRegister, dest.getOffset());
+            addLongJump(BufferOffset(label->offset()), dest);
+            Assembler::WriteLoad64Instructions(inst, ScratchRegister, LabelBase::INVALID_OFFSET);
             inst[4] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         } else {
             // Handle open long conditional jumps created by
             // MacroAssemblerMIPSShared::ma_b(..., wasm::Trap, ...).
             inst[0] = invertBranch(inst[0], BOffImm16(7 * sizeof(uint32_t)));
             // No need for a "nop" here because we can clobber scratch.
             // We need to add it to long jumps array here.
             // See MacroAssemblerMIPS64::branchWithCode().
             MOZ_ASSERT(inst[1].encode() == NopInst);
             MOZ_ASSERT(inst[2].encode() == NopInst);
             MOZ_ASSERT(inst[3].encode() == NopInst);
             MOZ_ASSERT(inst[4].encode() == NopInst);
             MOZ_ASSERT(inst[5].encode() == NopInst);
             MOZ_ASSERT(inst[6].encode() == NopInst);
-            addLongJump(BufferOffset(label->offset() + sizeof(uint32_t)));
-            Assembler::WriteLoad64Instructions(&inst[1], ScratchRegister, dest.getOffset());
+            addLongJump(BufferOffset(label->offset() + sizeof(uint32_t)), dest);
+            Assembler::WriteLoad64Instructions(&inst[1], ScratchRegister, LabelBase::INVALID_OFFSET);
             inst[5] = InstReg(op_special, ScratchRegister, zero, zero, ff_jr).encode();
         }
     }
     label->bind(dest.getOffset());
 }
 
 void
 Assembler::processCodeLabels(uint8_t* rawCode)
diff --git a/js/src/jit/mips64/MacroAssembler-mips64.cpp b/js/src/jit/mips64/MacroAssembler-mips64.cpp
--- a/js/src/jit/mips64/MacroAssembler-mips64.cpp
+++ b/js/src/jit/mips64/MacroAssembler-mips64.cpp
@@ -758,18 +758,18 @@ MacroAssemblerMIPS64::ma_b(Address addr,
 
 void
 MacroAssemblerMIPS64::ma_bal(Label* label, DelaySlotFill delaySlotFill)
 {
     spew("branch .Llabel %p\n", label);
     if (label->bound()) {
         // Generate the long jump for calls because return address has to be
         // the address after the reserved block.
-        addLongJump(nextOffset());
-        ma_liPatchable(ScratchRegister, ImmWord(label->offset()));
+        addLongJump(nextOffset(), BufferOffset(label->offset()));
+        ma_liPatchable(ScratchRegister, ImmWord(LabelBase::INVALID_OFFSET));
         as_jalr(ScratchRegister);
         if (delaySlotFill == FillDelaySlot)
             as_nop();
         return;
     }
 
     // Second word holds a pointer to the next branch in label's chain.
     uint32_t nextInChain = label->used() ? label->offset() : LabelBase::INVALID_OFFSET;
@@ -814,34 +814,34 @@ MacroAssemblerMIPS64::branchWithCode(Ins
 #endif
             writeInst(code.encode());
             as_nop();
             return;
         }
 
         if (code.encode() == inst_beq.encode()) {
             // Handle long jump
-            addLongJump(nextOffset());
-            ma_liPatchable(ScratchRegister, ImmWord(label->offset()));
+            addLongJump(nextOffset(), BufferOffset(label->offset()));
+            ma_liPatchable(ScratchRegister, ImmWord(LabelBase::INVALID_OFFSET));
             as_jr(ScratchRegister);
             as_nop();
             return;
         }
 
         // Handle long conditional branch, the target offset is based on self,
         // point to next instruction of nop at below.
         spew("invert branch .Llabel %p", label);
         InstImm code_r = invertBranch(code, BOffImm16(7 * sizeof(uint32_t)));
 #ifdef JS_JITSPEW
         decodeBranchInstAndSpew(code_r);
 #endif
         writeInst(code_r.encode());
         // No need for a "nop" here because we can clobber scratch.
-        addLongJump(nextOffset());
-        ma_liPatchable(ScratchRegister, ImmWord(label->offset()));
+        addLongJump(nextOffset(), BufferOffset(label->offset()));
+        ma_liPatchable(ScratchRegister, ImmWord(LabelBase::INVALID_OFFSET));
         as_jr(ScratchRegister);
         as_nop();
         return;
     }
 
     // Generate open jump and link it to a label.
 
     // Second word holds a pointer to the next branch in label's chain.
@@ -1790,53 +1790,45 @@ MacroAssemblerMIPS64Compat::extractTag(c
  * The backedge is done this way to avoid patching lui+ori pair while it is
  * being executed. Look also at jit::PatchBackedge().
  */
 CodeOffsetJump
 MacroAssemblerMIPS64Compat::backedgeJump(RepatchLabel* label, Label* documentation)
 {
     // Only one branch per label.
     MOZ_ASSERT(!label->used());
-    uint32_t dest = label->bound() ? label->offset() : LabelBase::INVALID_OFFSET;
+
     BufferOffset bo = nextOffset();
     label->use(bo.getOffset());
 
     // Backedges are short jumps when bound, but can become long when patched.
     m_buffer.ensureSpace(16 * sizeof(uint32_t));
-    if (label->bound()) {
-        int32_t offset = label->offset() - bo.getOffset();
-        MOZ_ASSERT(BOffImm16::IsInRange(offset));
-        as_b(BOffImm16(offset));
-    } else {
-        // Jump to "label1" by default to jump to the loop header.
-        as_b(BOffImm16(2 * sizeof(uint32_t)));
-    }
+    // Jump to "label1" by default to jump to the loop header.
+    as_b(BOffImm16(2 * sizeof(uint32_t)));
     // No need for nop here. We can safely put next instruction in delay slot.
-    ma_liPatchable(ScratchRegister, ImmWord(dest));
+    ma_liPatchable(ScratchRegister, ImmWord(LabelBase::INVALID_OFFSET));
     MOZ_ASSERT(nextOffset().getOffset() - bo.getOffset() == 5 * sizeof(uint32_t));
     as_jr(ScratchRegister);
     // No need for nop here. We can safely put next instruction in delay slot.
-    ma_liPatchable(ScratchRegister, ImmWord(dest));
+    ma_liPatchable(ScratchRegister, ImmWord(LabelBase::INVALID_OFFSET));
     as_jr(ScratchRegister);
     as_nop();
     MOZ_ASSERT(nextOffset().getOffset() - bo.getOffset() == 12 * sizeof(uint32_t));
     return CodeOffsetJump(bo.getOffset());
 }
 
 CodeOffsetJump
 MacroAssemblerMIPS64Compat::jumpWithPatch(RepatchLabel* label, Label* documentation)
 {
     // Only one branch per label.
     MOZ_ASSERT(!label->used());
-    uint32_t dest = label->bound() ? label->offset() : LabelBase::INVALID_OFFSET;
 
     BufferOffset bo = nextOffset();
     label->use(bo.getOffset());
-    addLongJump(bo);
-    ma_liPatchable(ScratchRegister, ImmWord(dest));
+    ma_liPatchable(ScratchRegister, ImmWord(LabelBase::INVALID_OFFSET));
     as_jr(ScratchRegister);
     as_nop();
     return CodeOffsetJump(bo.getOffset());
 }
 
 /////////////////////////////////////////////////////////////////
 // X86/X64-common/ARM/MIPS interface.
 /////////////////////////////////////////////////////////////////
