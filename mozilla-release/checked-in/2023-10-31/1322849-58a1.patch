# HG changeset patch
# User Robert Longson <longsonr@gmail.com>
# Date 1508275194 -3600
#      Tue Oct 17 22:19:54 2017 +0100
# Node ID 11fcd28ed76e67363e0bcb090da763d401e2a811
# Parent  cc3088927682eae0e152e0e7fbc6762f40c29674
Bug 1322849 - Add a range check when the duration is multiplied by the repeat count. r=birtles

diff --git a/dom/smil/crashtests/1322849-1.svg b/dom/smil/crashtests/1322849-1.svg
new file mode 100644
--- /dev/null
+++ b/dom/smil/crashtests/1322849-1.svg
@@ -0,0 +1,2 @@
+<svg>
+<set fill='freeze' dur='8' repeatCount='1844674737095516'>
diff --git a/dom/smil/crashtests/crashtests.list b/dom/smil/crashtests/crashtests.list
--- a/dom/smil/crashtests/crashtests.list
+++ b/dom/smil/crashtests/crashtests.list
@@ -47,10 +47,11 @@ load 678938-1.svg
 load 690994-1.svg
 load 691337-1.svg
 load 691337-2.svg
 load 697640-1.svg
 load 699325-1.svg
 load 709907-1.svg
 load 720103-1.svg
 load 1010681-1.svg
+load 1322849-1.svg
 load 1375596-1.svg
 load 1402547-1.html
diff --git a/dom/smil/nsSMILTimeValue.cpp b/dom/smil/nsSMILTimeValue.cpp
--- a/dom/smil/nsSMILTimeValue.cpp
+++ b/dom/smil/nsSMILTimeValue.cpp
@@ -1,17 +1,18 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsSMILTimeValue.h"
 
-const nsSMILTime nsSMILTimeValue::kUnresolvedMillis = INT64_MAX;
+const nsSMILTime nsSMILTimeValue::kUnresolvedMillis =
+  std::numeric_limits<nsSMILTime>::max();
 
 //----------------------------------------------------------------------
 // nsSMILTimeValue methods:
 
 static inline int8_t
 Cmp(int64_t aA, int64_t aB)
 {
   return aA == aB ? 0 : (aA > aB ? 1 : -1);
diff --git a/dom/smil/nsSMILTimedElement.cpp b/dom/smil/nsSMILTimedElement.cpp
--- a/dom/smil/nsSMILTimedElement.cpp
+++ b/dom/smil/nsSMILTimedElement.cpp
@@ -225,17 +225,18 @@ const nsAttrValue::EnumTable nsSMILTimed
 
 const nsAttrValue::EnumTable nsSMILTimedElement::sRestartModeTable[] = {
       {"always", RESTART_ALWAYS},
       {"whenNotActive", RESTART_WHENNOTACTIVE},
       {"never", RESTART_NEVER},
       {nullptr, 0}
 };
 
-const nsSMILMilestone nsSMILTimedElement::sMaxMilestone(INT64_MAX, false);
+const nsSMILMilestone nsSMILTimedElement::sMaxMilestone(
+  std::numeric_limits<nsSMILTime>::max(), false);
 
 // The thresholds at which point we start filtering intervals and instance times
 // indiscriminately.
 // See FilterIntervals and FilterInstanceTimes.
 const uint8_t nsSMILTimedElement::sMaxNumIntervals = 20;
 const uint8_t nsSMILTimedElement::sMaxNumInstanceTimes = 100;
 
 // Detect if we arrive in some sort of undetected recursive syncbase dependency
@@ -1922,18 +1923,21 @@ nsSMILTimedElement::CalcActiveEnd(const 
   return result;
 }
 
 nsSMILTimeValue
 nsSMILTimedElement::GetRepeatDuration() const
 {
   nsSMILTimeValue multipliedDuration;
   if (mRepeatCount.IsDefinite() && mSimpleDur.IsDefinite()) {
-    multipliedDuration.SetMillis(
-      nsSMILTime(mRepeatCount * double(mSimpleDur.GetMillis())));
+    if (mRepeatCount * double(mSimpleDur.GetMillis()) <=
+        std::numeric_limits<nsSMILTime>::max()) {
+      multipliedDuration.SetMillis(
+        nsSMILTime(mRepeatCount * mSimpleDur.GetMillis()));
+    }
   } else {
     multipliedDuration.SetIndefinite();
   }
 
   nsSMILTimeValue repeatDuration;
 
   if (mRepeatDur.IsResolved()) {
     repeatDuration = std::min(multipliedDuration, mRepeatDur);
@@ -2202,23 +2206,23 @@ nsSMILTimedElement::SampleFillValue()
     mClient->SampleAt(simpleTime, mSimpleDur, repeatIteration);
   }
 }
 
 nsresult
 nsSMILTimedElement::AddInstanceTimeFromCurrentTime(nsSMILTime aCurrentTime,
     double aOffsetSeconds, bool aIsBegin)
 {
-  double offset = aOffsetSeconds * PR_MSEC_PER_SEC;
+  double offset = NS_round(aOffsetSeconds * PR_MSEC_PER_SEC);
 
   // Check we won't overflow the range of nsSMILTime
-  if (aCurrentTime + NS_round(offset) > INT64_MAX)
+  if (aCurrentTime + offset > std::numeric_limits<nsSMILTime>::max())
     return NS_ERROR_ILLEGAL_VALUE;
 
-  nsSMILTimeValue timeVal(aCurrentTime + int64_t(NS_round(offset)));
+  nsSMILTimeValue timeVal(aCurrentTime + int64_t(offset));
 
   RefPtr<nsSMILInstanceTime> instanceTime =
     new nsSMILInstanceTime(timeVal, nsSMILInstanceTime::SOURCE_DOM);
 
   AddInstanceTime(instanceTime, aIsBegin);
 
   return NS_OK;
 }
