# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1512110195 -28800
#      Fri Dec 01 14:36:35 2017 +0800
# Node ID e5d7450bab22a5dd1ad4b1204206f5488afe30db
# Parent  fe6793815a79464d43a62ac0c9ceb47f5c7b0b6b
Bug 1422677. P3 - pass a MediaStatistics to ShouldThrottleDownload(). r=bechen,gerald

So it won't have to call GetStatistics() again.

MozReview-Commit-ID: 1tRnRHAiX4L

diff --git a/dom/media/ChannelMediaDecoder.cpp b/dom/media/ChannelMediaDecoder.cpp
--- a/dom/media/ChannelMediaDecoder.cpp
+++ b/dom/media/ChannelMediaDecoder.cpp
@@ -427,20 +427,20 @@ void
 ChannelMediaDecoder::DownloadProgressed()
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_DIAGNOSTIC_ASSERT(!IsShutdown());
   AbstractThread::AutoEnter context(AbstractMainThread());
   GetOwner()->DownloadProgressed();
   ComputePlaybackRate();
   UpdatePlaybackRate();
-  mResource->ThrottleReadahead(ShouldThrottleDownload());
   // Note GetStatistics() depends on the side effect of ComputePlaybackRate().
   MediaStatistics stats = GetStatistics();
   GetStateMachine()->DispatchCanPlayThrough(stats.CanPlayThrough());
+  mResource->ThrottleReadahead(ShouldThrottleDownload(stats));
 }
 
 void
 ChannelMediaDecoder::ComputePlaybackRate()
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(mResource);
 
@@ -489,45 +489,44 @@ ChannelMediaDecoder::GetStatistics()
   result.mTotalBytes = mResource->GetLength();
   result.mPlaybackRate = mPlaybackBytesPerSecond;
   result.mPlaybackRateReliable = mPlaybackRateReliable;
   result.mPlaybackPosition = mPlaybackPosition;
   return result;
 }
 
 bool
-ChannelMediaDecoder::ShouldThrottleDownload()
+ChannelMediaDecoder::ShouldThrottleDownload(const MediaStatistics& aStats)
 {
   // We throttle the download if either the throttle override pref is set
   // (so that we can always throttle in Firefox on mobile) or if the download
   // is fast enough that there's no concern about playback being interrupted.
   MOZ_ASSERT(NS_IsMainThread());
   NS_ENSURE_TRUE(GetStateMachine(), false);
 
-  int64_t length = mResource->GetLength();
+  int64_t length = aStats.mTotalBytes;
   if (length > 0 &&
       length <= int64_t(MediaPrefs::MediaMemoryCacheMaxSize()) * 1024) {
     // Don't throttle the download of small resources. This is to speed
     // up seeking, as seeks into unbuffered ranges would require starting
     // up a new HTTP transaction, which adds latency.
     return false;
   }
 
   if (Preferences::GetBool("media.throttle-regardless-of-download-rate",
                            false)) {
     return true;
   }
 
-  MediaStatistics stats = GetStatistics();
-  if (!stats.mDownloadRateReliable || !stats.mPlaybackRateReliable) {
+  if (!aStats.mDownloadRateReliable || !aStats.mPlaybackRateReliable) {
     return false;
   }
   uint32_t factor =
     std::max(2u, Preferences::GetUint("media.throttle-factor", 2));
-  return stats.mDownloadRate > factor * stats.mPlaybackRate;
+  return aStats.mDownloadRate > factor * aStats.mPlaybackRate;
 }
 
 void
 ChannelMediaDecoder::AddSizeOfResources(ResourceSizes* aSizes)
 {
   MOZ_ASSERT(NS_IsMainThread());
   if (mResource) {
     aSizes->mByteSize += mResource->SizeOfIncludingThis(aSizes->mMallocSizeOf);
diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -135,17 +135,17 @@ private:
   void UpdatePlaybackRate();
 
   // Return statistics. This is used for progress events and other things.
   // This can be called from any thread. It's only a snapshot of the
   // current state, since other threads might be changing the state
   // at any time.
   MediaStatistics GetStatistics();
 
-  bool ShouldThrottleDownload();
+  bool ShouldThrottleDownload(const MediaStatistics& aStats);
 
   WatchManager<ChannelMediaDecoder> mWatchManager;
 
   // True when seeking or otherwise moving the play position around in
   // such a manner that progress event data is inaccurate. This is set
   // during seek and duration operations to prevent the progress indicator
   // from jumping around. Read/Write on the main thread only.
   bool mIgnoreProgressData = false;
