# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1513113660 21600
#      Tue Dec 12 15:21:00 2017 -0600
# Node ID 5996e1f1de797642c550dfed18838cd13190acda
# Parent  cc7edfa91306c2118bd1856eb40275b27da9b9f1
Bug 1424946 - Move GeneralParser::nextTokenContinuesLet into a baser class.  r=arai

diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -7315,27 +7315,23 @@ GeneralParser<ParseHandler, CharT>::clas
     }
 
     MOZ_ALWAYS_TRUE(setLocalStrictMode(savedStrictness));
 
     return handler.newClass(nameNode, classHeritage, methodsOrBlock,
                             TokenPos(classStartOffset, classEndOffset));
 }
 
-template <class ParseHandler, typename CharT>
 bool
-GeneralParser<ParseHandler, CharT>::nextTokenContinuesLetDeclaration(TokenKind next)
+ParserBase::nextTokenContinuesLetDeclaration(TokenKind next)
 {
     MOZ_ASSERT(anyChars.isCurrentTokenType(TokenKind::Let));
-
-#ifdef DEBUG
-    TokenKind verify;
-    MOZ_ALWAYS_TRUE(tokenStream.peekToken(&verify));
-    MOZ_ASSERT(next == verify);
-#endif
+    MOZ_ASSERT(anyChars.nextToken().type == next);
+
+    TokenStreamShared::verifyConsistentModifier(TokenStreamShared::None, anyChars.nextToken());
 
     // Destructuring continues a let declaration.
     if (next == TokenKind::Lb || next == TokenKind::Lc)
         return true;
 
     // A "let" edge case deserves special comment.  Consider this:
     //
     //   let     // not an ASI opportunity
diff --git a/js/src/frontend/Parser.h b/js/src/frontend/Parser.h
--- a/js/src/frontend/Parser.h
+++ b/js/src/frontend/Parser.h
@@ -387,16 +387,21 @@ class ParserBase
                                                               bool hasParameterExprs);
     mozilla::Maybe<VarScope::Data*> newVarScopeData(ParseContext::Scope& scope);
     mozilla::Maybe<LexicalScope::Data*> newLexicalScopeData(ParseContext::Scope& scope);
 
   protected:
     enum InvokedPrediction { PredictUninvoked = false, PredictInvoked = true };
     enum ForInitLocation { InForInit, NotInForInit };
 
+    // While on a |let| TOK_NAME token, examine |next| (which must already be
+    // gotten).  Indicate whether |next|, the next token already gotten with
+    // modifier TokenStream::None, continues a LexicalDeclaration.
+    bool nextTokenContinuesLetDeclaration(TokenKind next);
+
     bool noteUsedNameInternal(HandlePropertyName name);
     bool hasUsedName(HandlePropertyName name);
     bool hasUsedFunctionSpecialName(HandlePropertyName name);
 
     bool declareDotGeneratorName();
 
     bool leaveInnerFunction(ParseContext* outerpc);
 
@@ -691,16 +696,17 @@ class GeneralParser
     using Base::declareFunctionArgumentsObject;
     using Base::declareFunctionThis;
     using Base::finishFunction;
     using Base::hasUsedName;
     using Base::leaveInnerFunction;
     using Base::newDotGeneratorName;
     using Base::newInternalDotName;
     using Base::newThisName;
+    using Base::nextTokenContinuesLetDeclaration;
     using Base::noteDestructuredPositionalFormalParameter;
     using Base::noteUsedName;
     using Base::prefixAccessorName;
     using Base::processExport;
     using Base::processExportFrom;
 
   private:
     inline FinalParser* asFinalParser();
@@ -994,21 +1000,16 @@ class GeneralParser
     Node variableStatement(YieldHandling yieldHandling);
 
     Node labeledStatement(YieldHandling yieldHandling);
     Node labeledItem(YieldHandling yieldHandling);
 
     Node ifStatement(YieldHandling yieldHandling);
     Node consequentOrAlternative(YieldHandling yieldHandling);
 
-    // While on a |let| Name token, examine |next|.  Indicate whether
-    // |next|, the next token already gotten with modifier TokenStream::None,
-    // continues a LexicalDeclaration.
-    bool nextTokenContinuesLetDeclaration(TokenKind next);
-
     Node lexicalDeclaration(YieldHandling yieldHandling, DeclarationKind kind);
 
     inline Node importDeclaration();
 
     Node exportFrom(uint32_t begin, Node specList);
     Node exportBatch(uint32_t begin);
     inline bool checkLocalExportNames(Node node);
     Node exportClause(uint32_t begin);
