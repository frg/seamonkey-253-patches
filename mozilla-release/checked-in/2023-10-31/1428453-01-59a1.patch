# HG changeset patch
# User Luke Wagner <luke@mozilla.com>
# Date 1515455031 21600
#      Mon Jan 08 17:43:51 2018 -0600
# Node ID 02fc8728537344ef9cb38f0c8a91fed6ffde5097
# Parent  9f3c0878879f4e54a79b6824fe1eb7bbdfa7e2e4
Bug 1428453 - Baldr: move code and remove dead argument/field (r=bbouvier)

MozReview-Commit-ID: IHXAThgE5VH

diff --git a/js/src/jscntxt.h b/js/src/jscntxt.h
--- a/js/src/jscntxt.h
+++ b/js/src/jscntxt.h
@@ -15,16 +15,19 @@
 #include "js/GCVector.h"
 #include "js/Result.h"
 #include "js/Utility.h"
 #include "js/Vector.h"
 #include "threading/ProtectedData.h"
 #include "vm/ErrorReporting.h"
 #include "vm/MallocProvider.h"
 #include "vm/Runtime.h"
+#ifdef XP_DARWIN
+# include "wasm/WasmSignalHandlers.h"
+#endif
 
 #ifdef _MSC_VER
 #pragma warning(push)
 #pragma warning(disable:4100) /* Silence unreferenced formal parameter warnings */
 #endif
 
 struct DtoaState;
 
diff --git a/js/src/vm/Runtime.h b/js/src/vm/Runtime.h
--- a/js/src/vm/Runtime.h
+++ b/js/src/vm/Runtime.h
@@ -16,19 +16,16 @@
 #include "mozilla/Scoped.h"
 #include "mozilla/ThreadLocal.h"
 #include "mozilla/Vector.h"
 
 #include <setjmp.h>
 
 #include "jsscript.h"
 
-#ifdef XP_DARWIN
-# include "wasm/WasmSignalHandlers.h"
-#endif
 #include "builtin/AtomicsObject.h"
 #include "builtin/intl/SharedIntlData.h"
 #include "builtin/Promise.h"
 #include "frontend/NameCollections.h"
 #include "gc/GCRuntime.h"
 #include "gc/Tracer.h"
 #include "gc/ZoneGroup.h"
 #include "irregexp/RegExpStack.h"
diff --git a/js/src/vm/Stack.cpp b/js/src/vm/Stack.cpp
--- a/js/src/vm/Stack.cpp
+++ b/js/src/vm/Stack.cpp
@@ -1704,17 +1704,17 @@ jit::JitActivation::startWasmInterrupt(c
 
     // Execution can only be interrupted in function code. Afterwards, control
     // flow does not reenter function code and thus there should be no
     // interrupt-during-interrupt.
     MOZ_ASSERT(!isWasmInterrupted());
 
     bool ignoredUnwound;
     wasm::UnwindState unwindState;
-    MOZ_ALWAYS_TRUE(wasm::StartUnwinding(*this, state, &unwindState, &ignoredUnwound));
+    MOZ_ALWAYS_TRUE(wasm::StartUnwinding(state, &unwindState, &ignoredUnwound));
 
     void* pc = unwindState.pc;
     MOZ_ASSERT(wasm::LookupCode(pc)->lookupRange(pc)->isFunction());
 
     cx_->runtime()->startWasmInterrupt(state.pc, pc);
     setWasmExitFP(unwindState.fp);
 
     MOZ_ASSERT(compartment() == unwindState.fp->tls->instance->compartment());
diff --git a/js/src/wasm/WasmCode.cpp b/js/src/wasm/WasmCode.cpp
--- a/js/src/wasm/WasmCode.cpp
+++ b/js/src/wasm/WasmCode.cpp
@@ -762,56 +762,16 @@ struct CallSiteRetAddrOffset
 {
     const CallSiteVector& callSites;
     explicit CallSiteRetAddrOffset(const CallSiteVector& callSites) : callSites(callSites) {}
     uint32_t operator[](size_t index) const {
         return callSites[index].returnAddressOffset();
     }
 };
 
-size_t
-Code::serializedSize() const
-{
-    return metadata().serializedSize() +
-           segment(Tier::Serialized).serializedSize();
-}
-
-uint8_t*
-Code::serialize(uint8_t* cursor, const LinkData& linkData) const
-{
-    MOZ_RELEASE_ASSERT(!metadata().debugEnabled);
-
-    cursor = metadata().serialize(cursor);
-    cursor = segment(Tier::Serialized).serialize(cursor, linkData.linkData(Tier::Serialized));
-    return cursor;
-}
-
-const uint8_t*
-Code::deserialize(const uint8_t* cursor, const SharedBytes& bytecode, const LinkData& linkData,
-                  Metadata& metadata)
-{
-    cursor = metadata.deserialize(cursor);
-    if (!cursor)
-        return nullptr;
-
-    UniqueCodeSegment codeSegment = js::MakeUnique<CodeSegment>();
-    if (!codeSegment)
-        return nullptr;
-
-    cursor = codeSegment->deserialize(cursor, *bytecode, linkData.linkData(Tier::Serialized),
-                                      metadata);
-    if (!cursor)
-        return nullptr;
-
-    segment1_ = takeOwnership(Move(codeSegment));
-    metadata_ = &metadata;
-
-    return cursor;
-}
-
 const CallSite*
 Code::lookupCallSite(void* returnAddress) const
 {
     for (auto t : tiers()) {
         uint32_t target = ((uint8_t*)returnAddress) - segment(t).base();
         size_t lowerBound = 0;
         size_t upperBound = metadata(t).callSites.length();
 
@@ -954,8 +914,48 @@ Code::addSizeOfMiscIfNotSeen(MallocSizeO
 
     *data += mallocSizeOf(this) +
              metadata().sizeOfIncludingThisIfNotSeen(mallocSizeOf, seenMetadata) +
              profilingLabels_.lock()->sizeOfExcludingThis(mallocSizeOf);
 
     for (auto t : tiers())
         segment(t).addSizeOfMisc(mallocSizeOf, code, data);
 }
+
+size_t
+Code::serializedSize() const
+{
+    return metadata().serializedSize() +
+           segment(Tier::Serialized).serializedSize();
+}
+
+uint8_t*
+Code::serialize(uint8_t* cursor, const LinkData& linkData) const
+{
+    MOZ_RELEASE_ASSERT(!metadata().debugEnabled);
+
+    cursor = metadata().serialize(cursor);
+    cursor = segment(Tier::Serialized).serialize(cursor, linkData.linkData(Tier::Serialized));
+    return cursor;
+}
+
+const uint8_t*
+Code::deserialize(const uint8_t* cursor, const SharedBytes& bytecode, const LinkData& linkData,
+                  Metadata& metadata)
+{
+    cursor = metadata.deserialize(cursor);
+    if (!cursor)
+        return nullptr;
+
+    UniqueCodeSegment codeSegment = js::MakeUnique<CodeSegment>();
+    if (!codeSegment)
+        return nullptr;
+
+    cursor = codeSegment->deserialize(cursor, *bytecode, linkData.linkData(Tier::Serialized),
+                                      metadata);
+    if (!cursor)
+        return nullptr;
+
+    segment1_ = takeOwnership(Move(codeSegment));
+    metadata_ = &metadata;
+
+    return cursor;
+}
diff --git a/js/src/wasm/WasmCode.h b/js/src/wasm/WasmCode.h
--- a/js/src/wasm/WasmCode.h
+++ b/js/src/wasm/WasmCode.h
@@ -125,17 +125,17 @@ class CodeSegment
                                     const LinkDataTier& linkData,
                                     const Metadata& metadata);
 
     void initCode(const Code* code) {
         MOZ_ASSERT(!code_);
         code_ = code;
     }
 
-    const Code* code() const { MOZ_ASSERT(code_); return code_; }
+    const Code& code() const { MOZ_ASSERT(code_); return *code_; }
     Tier tier() const { return tier_; }
 
     uint8_t* base() const { return bytes_.get(); }
     uint32_t length() const { return length_; }
 
     uint8_t* interruptCode() const { return interruptCode_; }
     uint8_t* outOfBoundsCode() const { return outOfBoundsCode_; }
     uint8_t* unalignedAccessCode() const { return unalignedAccessCode_; }
diff --git a/js/src/wasm/WasmFrameIter.cpp b/js/src/wasm/WasmFrameIter.cpp
--- a/js/src/wasm/WasmFrameIter.cpp
+++ b/js/src/wasm/WasmFrameIter.cpp
@@ -37,41 +37,41 @@ WasmFrameIter::WasmFrameIter(JitActivati
     code_(nullptr),
     callsite_(nullptr),
     codeRange_(nullptr),
     fp_(fp ? fp : activation->wasmExitFP()),
     unwind_(Unwind::False)
 {
     MOZ_ASSERT(fp_);
 
-    // Normally, execution exits wasm code via an exit stub which sets exitFP
-    // to the exit stub's frame. Thus, in this case, we want to start iteration
-    // at the caller of the exit frame, whose Code, CodeRange and CallSite are
-    // indicated by the returnAddress of the exit stub's frame.
-
-    if (!activation->isWasmInterrupted()) {
-        popFrame();
-        MOZ_ASSERT(!done());
-        return;
-    }
-
     // When asynchronously interrupted, exitFP is set to the interrupted frame
     // itself and so we do not want to skip it. Instead, we can recover the
     // Code and CodeRange from the JitActivation, which are set when control
     // flow was interrupted. There is no CallSite (b/c the interrupt was
     // async), but this is fine because CallSite is only used for line number
     // for which we can use the beginning of the function from the CodeRange
     // instead.
 
-    code_ = &fp_->tls->instance->code();
-    MOZ_ASSERT(code_ == LookupCode(activation->wasmUnwindPC()));
+    if (activation->isWasmInterrupted()) {
+        code_ = &fp_->tls->instance->code();
+        MOZ_ASSERT(code_ == LookupCode(activation->wasmUnwindPC()));
+
+        codeRange_ = code_->lookupRange(activation->wasmUnwindPC());
+        MOZ_ASSERT(codeRange_->kind() == CodeRange::Function);
 
-    codeRange_ = code_->lookupRange(activation->wasmUnwindPC());
-    MOZ_ASSERT(codeRange_->kind() == CodeRange::Function);
+        MOZ_ASSERT(!done());
+        return;
+    }
 
+    // Otherwise, execution exits wasm code via an exit stub which sets exitFP
+    // to the exit stub's frame. Thus, in this case, we want to start iteration
+    // at the caller of the exit frame, whose Code, CodeRange and CallSite are
+    // indicated by the returnAddress of the exit stub's frame.
+
+    popFrame();
     MOZ_ASSERT(!done());
 }
 
 bool
 WasmFrameIter::done() const
 {
     MOZ_ASSERT(!!fp_ == !!code_);
     MOZ_ASSERT(!!fp_ == !!codeRange_);
@@ -541,54 +541,51 @@ wasm::GenerateJitExitEpilogue(MacroAssem
     GenerateCallableEpilogue(masm, framePushed, ExitReason::None(), &offsets->ret);
     masm.setFramePushed(0);
 }
 
 /*****************************************************************************/
 // ProfilingFrameIterator
 
 ProfilingFrameIterator::ProfilingFrameIterator()
-  : activation_(nullptr),
-    code_(nullptr),
+  : code_(nullptr),
     codeRange_(nullptr),
     callerFP_(nullptr),
     callerPC_(nullptr),
     stackAddress_(nullptr),
     exitReason_(ExitReason::Fixed::None)
 {
     MOZ_ASSERT(done());
 }
 
 ProfilingFrameIterator::ProfilingFrameIterator(const JitActivation& activation)
-  : activation_(&activation),
-    code_(nullptr),
+  : code_(nullptr),
     codeRange_(nullptr),
     callerFP_(nullptr),
     callerPC_(nullptr),
     stackAddress_(nullptr),
     exitReason_(activation.wasmExitReason())
 {
     initFromExitFP(activation.wasmExitFP());
 }
 
 ProfilingFrameIterator::ProfilingFrameIterator(const JitActivation& activation, const Frame* fp)
-  : activation_(&activation),
-    code_(nullptr),
+  : code_(nullptr),
     codeRange_(nullptr),
     callerFP_(nullptr),
     callerPC_(nullptr),
     stackAddress_(nullptr),
     exitReason_(ExitReason::Fixed::ImportJit)
 {
     MOZ_ASSERT(fp);
     initFromExitFP(fp);
 }
 
 static inline void
-AssertMatchesCallSite(const JitActivation& activation, void* callerPC, Frame* callerFP)
+AssertMatchesCallSite(void* callerPC, Frame* callerFP)
 {
 #ifdef DEBUG
     const Code* code = LookupCode(callerPC);
     MOZ_ASSERT(code);
 
     const CodeRange* callerCodeRange = code->lookupRange(callerPC);
     MOZ_ASSERT(callerCodeRange);
 
@@ -627,17 +624,17 @@ ProfilingFrameIterator::initFromExitFP(c
       case CodeRange::InterpEntry:
         callerPC_ = nullptr;
         callerFP_ = nullptr;
         break;
       case CodeRange::Function:
         fp = fp->callerFP;
         callerPC_ = fp->returnAddress;
         callerFP_ = fp->callerFP;
-        AssertMatchesCallSite(*activation_, callerPC_, callerFP_);
+        AssertMatchesCallSite(callerPC_, callerFP_);
         break;
       case CodeRange::ImportJitExit:
       case CodeRange::ImportInterpExit:
       case CodeRange::BuiltinThunk:
       case CodeRange::TrapExit:
       case CodeRange::DebugTrap:
       case CodeRange::OutOfBoundsExit:
       case CodeRange::UnalignedExit:
@@ -646,18 +643,18 @@ ProfilingFrameIterator::initFromExitFP(c
       case CodeRange::FarJumpIsland:
         MOZ_CRASH("Unexpected CodeRange kind");
     }
 
     MOZ_ASSERT(!done());
 }
 
 bool
-js::wasm::StartUnwinding(const JitActivation& activation, const RegisterState& registers,
-                         UnwindState* unwindState, bool* unwoundCaller)
+js::wasm::StartUnwinding(const RegisterState& registers, UnwindState* unwindState,
+                         bool* unwoundCaller)
 {
     // Shorthands.
     uint8_t* const pc = (uint8_t*) registers.pc;
     void** const sp = (void**) registers.sp;
 
     // The frame pointer might be in the process of tagging/untagging; make
     // sure it's untagged.
     Frame* const fp = (Frame*) (intptr_t(registers.fp) & ~JitActivation::ExitFpWasmBit);
@@ -667,17 +664,17 @@ js::wasm::StartUnwinding(const JitActiva
     // thunk, then execution must be entering from or leaving to the C++ caller
     // that pushed the JitActivation.
     const CodeRange* codeRange;
     uint8_t* codeBase;
     const Code* code = nullptr;
 
     const CodeSegment* codeSegment = LookupCodeSegment(pc);
     if (codeSegment) {
-        code = codeSegment->code();
+        code = &codeSegment->code();
         codeRange = code->lookupRange(pc);
         codeBase = codeSegment->base();
     } else if (!LookupBuiltinThunk(pc, &codeRange, &codeBase)) {
         return false;
     }
 
     // When the pc is inside the prologue/epilogue, the innermost call's Frame
     // is not complete and thus fp points to the second-to-innermost call's
@@ -725,101 +722,101 @@ js::wasm::StartUnwinding(const JitActiva
             // and storing the value to sp.
             // Execution might be interrupted in between the two operation so we
             // have to relay on register state instead of state saved on stack
             // until the wasm::Frame is completely built.
             // On entry the return address is in ra (registers.lr) and
             // fp holds the caller's fp.
             fixedPC = (uint8_t*) registers.lr;
             fixedFP = fp;
-            AssertMatchesCallSite(activation, fixedPC, fixedFP);
+            AssertMatchesCallSite(fixedPC, fixedFP);
         } else
 #elif defined(JS_CODEGEN_ARM) || defined(JS_CODEGEN_ARM64)
         if (offsetFromEntry == BeforePushRetAddr || codeRange->isThunk()) {
             // The return address is still in lr and fp holds the caller's fp.
             fixedPC = (uint8_t*) registers.lr;
             fixedFP = fp;
-            AssertMatchesCallSite(activation, fixedPC, fixedFP);
+            AssertMatchesCallSite(fixedPC, fixedFP);
         } else
 #endif
         if (offsetFromEntry == PushedRetAddr || codeRange->isThunk()) {
             // The return address has been pushed on the stack but fp still
             // points to the caller's fp.
             fixedPC = sp[0];
             fixedFP = fp;
-            AssertMatchesCallSite(activation, fixedPC, fixedFP);
+            AssertMatchesCallSite(fixedPC, fixedFP);
         } else if (offsetFromEntry >= PushedTLS && offsetFromEntry < PushedFP) {
             // The return address and caller's TLS have been pushed on the
             // stack; fp is still the caller's fp.
             fixedPC = sp[1];
             fixedFP = fp;
-            AssertMatchesCallSite(activation, fixedPC, fixedFP);
+            AssertMatchesCallSite(fixedPC, fixedFP);
         } else if (offsetFromEntry == PushedFP) {
             // The full Frame has been pushed; fp is still the caller's fp.
             MOZ_ASSERT(fp == reinterpret_cast<Frame*>(sp)->callerFP);
             fixedPC = reinterpret_cast<Frame*>(sp)->returnAddress;
             fixedFP = fp;
-            AssertMatchesCallSite(activation, fixedPC, fixedFP);
+            AssertMatchesCallSite(fixedPC, fixedFP);
         } else if (offsetInCode >= codeRange->ret() - PoppedFP &&
                    offsetInCode < codeRange->ret() - PoppedTLSReg)
         {
             // The fixedFP field of the Frame has been popped into fp.
             fixedPC = sp[1];
             fixedFP = fp;
-            AssertMatchesCallSite(activation, fixedPC, fixedFP);
+            AssertMatchesCallSite(fixedPC, fixedFP);
 #if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
         } else if (offsetInCode >= codeRange->ret() - PoppedTLSReg &&
                    offsetInCode < codeRange->ret())
         {
             // The fixedFP field of the Frame has been popped into fp, but the
             // exit reason hasn't been popped yet.
             fixedPC = sp[0];
             fixedFP = fp;
-            AssertMatchesCallSite(activation, fixedPC, fixedFP);
+            AssertMatchesCallSite(fixedPC, fixedFP);
         } else if (offsetInCode == codeRange->ret()) {
             // Both the TLS, fixedFP and ra have been popped and fp now
             // points to the caller's frame.
             fixedPC = (uint8_t*) registers.lr;
             fixedFP = fp;
-            AssertMatchesCallSite(activation, fixedPC, fixedFP);
+            AssertMatchesCallSite(fixedPC, fixedFP);
 #else
         } else if (offsetInCode == codeRange->ret()) {
             // Both the TLS and fixedFP fields have been popped and fp now
             // points to the caller's frame.
             fixedPC = sp[0];
             fixedFP = fp;
-            AssertMatchesCallSite(activation, fixedPC, fixedFP);
+            AssertMatchesCallSite(fixedPC, fixedFP);
 #endif
         } else {
             if (codeRange->kind() == CodeRange::ImportJitExit) {
                 // The jit exit contains a range where the value of FP can't be
                 // trusted. Technically, we could recover fp from sp, but since
                 // the range is so short, for now just drop the stack.
                 if (offsetInCode >= codeRange->jitExitUntrustedFPStart() &&
                     offsetInCode < codeRange->jitExitUntrustedFPEnd())
                 {
                     return false;
                 }
             }
             // Not in the prologue/epilogue.
             fixedPC = pc;
             fixedFP = fp;
             *unwoundCaller = false;
-            AssertMatchesCallSite(activation, fp->returnAddress, fp->callerFP);
+            AssertMatchesCallSite(fp->returnAddress, fp->callerFP);
             break;
         }
         break;
       case CodeRange::OutOfBoundsExit:
       case CodeRange::UnalignedExit:
         // These code stubs execute after the prologue/epilogue have completed
         // so pc/fp contains the right values here.
         fixedPC = pc;
         fixedFP = fp;
         *unwoundCaller = false;
-        AssertMatchesCallSite(activation, fp->returnAddress, fp->callerFP);
+        AssertMatchesCallSite(fp->returnAddress, fp->callerFP);
         break;
       case CodeRange::InterpEntry:
         // The entry trampoline is the final frame in an wasm JitActivation. The
         // entry trampoline also doesn't GeneratePrologue/Epilogue so we can't
         // use the general unwinding logic above.
         break;
       case CodeRange::Throw:
         // The throw stub executes a small number of instructions before popping
@@ -837,18 +834,17 @@ js::wasm::StartUnwinding(const JitActiva
     unwindState->codeRange = codeRange;
     unwindState->fp = fixedFP;
     unwindState->pc = fixedPC;
     return true;
 }
 
 ProfilingFrameIterator::ProfilingFrameIterator(const JitActivation& activation,
                                                const RegisterState& state)
-  : activation_(&activation),
-    code_(nullptr),
+  : code_(nullptr),
     codeRange_(nullptr),
     callerFP_(nullptr),
     callerPC_(nullptr),
     stackAddress_(nullptr),
     exitReason_(ExitReason::Fixed::None)
 {
     // Let wasmExitFP take precedence to StartUnwinding when it is set since
     // during the body of an exit stub, the register state may not be valid
@@ -856,17 +852,17 @@ ProfilingFrameIterator::ProfilingFrameIt
     if (activation.hasWasmExitFP()) {
         exitReason_ = activation.wasmExitReason();
         initFromExitFP(activation.wasmExitFP());
         return;
     }
 
     bool unwoundCaller;
     UnwindState unwindState;
-    if (!StartUnwinding(*activation_, state, &unwindState, &unwoundCaller)) {
+    if (!StartUnwinding(state, &unwindState, &unwoundCaller)) {
         MOZ_ASSERT(done());
         return;
     }
 
     if (unwoundCaller) {
         callerFP_ = unwindState.fp;
         callerPC_ = unwindState.pc;
     } else {
@@ -918,17 +914,17 @@ ProfilingFrameIterator::operator++()
       case CodeRange::BuiltinThunk:
       case CodeRange::TrapExit:
       case CodeRange::DebugTrap:
       case CodeRange::OutOfBoundsExit:
       case CodeRange::UnalignedExit:
       case CodeRange::FarJumpIsland:
         stackAddress_ = callerFP_;
         callerPC_ = callerFP_->returnAddress;
-        AssertMatchesCallSite(*activation_, callerPC_, callerFP_->callerFP);
+        AssertMatchesCallSite(callerPC_, callerFP_->callerFP);
         callerFP_ = callerFP_->callerFP;
         break;
       case CodeRange::InterpEntry:
         MOZ_CRASH("should have had null caller fp");
       case CodeRange::Interrupt:
       case CodeRange::Throw:
         MOZ_CRASH("code range doesn't have frame");
     }
@@ -1090,28 +1086,28 @@ Instance*
 wasm::LookupFaultingInstance(const CodeSegment& codeSegment, void* pc, void* fp)
 {
     // Assume bug-caused faults can be raised at any PC and apply the logic of
     // ProfilingFrameIterator to reject any pc outside the (post-prologue,
     // pre-epilogue) body of a wasm function. This is exhaustively tested by the
     // simulators which call this function at every load/store before even
     // knowing whether there is a fault.
 
-    const CodeRange* codeRange = codeSegment.code()->lookupRange(pc);
+    const CodeRange* codeRange = codeSegment.code().lookupRange(pc);
     if (!codeRange || !codeRange->isFunction())
         return nullptr;
 
     size_t offsetInModule = ((uint8_t*)pc) - codeSegment.base();
     if (offsetInModule < codeRange->funcNormalEntry() + SetFP)
         return nullptr;
     if (offsetInModule >= codeRange->ret() - PoppedFP)
         return nullptr;
 
     Instance* instance = reinterpret_cast<Frame*>(fp)->tls->instance;
-    MOZ_RELEASE_ASSERT(&instance->code() == codeSegment.code());
+    MOZ_RELEASE_ASSERT(&instance->code() == &codeSegment.code());
     return instance;
 }
 
 bool
 wasm::InCompiledCode(void* pc)
 {
     if (LookupCodeSegment(pc))
         return true;
diff --git a/js/src/wasm/WasmFrameIter.h b/js/src/wasm/WasmFrameIter.h
--- a/js/src/wasm/WasmFrameIter.h
+++ b/js/src/wasm/WasmFrameIter.h
@@ -155,17 +155,16 @@ class ExitReason
         return SymbolicAddress(payload_ >> 1);
     }
 };
 
 // Iterates over the frames of a single wasm JitActivation, given an
 // asynchronously-interrupted thread's state.
 class ProfilingFrameIterator
 {
-    const jit::JitActivation* activation_;
     const Code* code_;
     const CodeRange* codeRange_;
     Frame* callerFP_;
     void* callerPC_;
     void* stackAddress_;
     ExitReason exitReason_;
 
     void initFromExitFP(const Frame* fp);
@@ -248,15 +247,15 @@ typedef JS::ProfilingFrameIterator::Regi
 //
 // unwoundCaller is set to true if we were in a transitional state and had to
 // rewind to the caller's frame instead of the current frame.
 //
 // Returns true if it was possible to get to a clear state, or false if the
 // frame should be ignored.
 
 bool
-StartUnwinding(const jit::JitActivation& activation, const RegisterState& registers,
-               UnwindState* unwindState, bool* unwoundCaller);
+StartUnwinding(const RegisterState& registers, UnwindState* unwindState,
+               bool* unwoundCaller);
 
 } // namespace wasm
 } // namespace js
 
 #endif // wasm_frame_iter_h
diff --git a/js/src/wasm/WasmGenerator.cpp b/js/src/wasm/WasmGenerator.cpp
--- a/js/src/wasm/WasmGenerator.cpp
+++ b/js/src/wasm/WasmGenerator.cpp
@@ -201,18 +201,18 @@ ModuleGenerator::init(Metadata* maybeAsm
     uint32_t codeSectionSize = env_->codeSection ? env_->codeSection->size : 0;
 
     if (!masm_.reserve(size_t(1.2 * EstimateCompiledCodeSize(tier(), codeSectionSize))))
         return false;
 
     if (!metadataTier_->codeRanges.reserve(2 * env_->numFuncDefs()))
         return false;
 
-    const size_t CallSitesPerByteCode = 10;
-    if (!metadataTier_->callSites.reserve(codeSectionSize / CallSitesPerByteCode))
+    const size_t ByteCodesPerCallSite = 10;
+    if (!metadataTier_->callSites.reserve(codeSectionSize / ByteCodesPerCallSite))
         return false;
 
     const size_t MemoryAccessesPerByteCode = 10;
     if (!metadataTier_->memoryAccesses.reserve(codeSectionSize / MemoryAccessesPerByteCode))
         return false;
 
     // Allocate space in TlsData for declarations that need it.
 
diff --git a/js/src/wasm/WasmProcess.cpp b/js/src/wasm/WasmProcess.cpp
--- a/js/src/wasm/WasmProcess.cpp
+++ b/js/src/wasm/WasmProcess.cpp
@@ -230,16 +230,16 @@ wasm::LookupCodeSegment(const void* pc)
 {
     return processCodeSegmentMap.lookup(pc);
 }
 
 const Code*
 wasm::LookupCode(const void* pc)
 {
     const CodeSegment* found = LookupCodeSegment(pc);
-    return found ? found->code() : nullptr;
+    return found ? &found->code() : nullptr;
 }
 
 void
 wasm::ShutDownProcessStaticData()
 {
     processCodeSegmentMap.freeAll();
 }
diff --git a/js/src/wasm/WasmSignalHandlers.cpp b/js/src/wasm/WasmSignalHandlers.cpp
--- a/js/src/wasm/WasmSignalHandlers.cpp
+++ b/js/src/wasm/WasmSignalHandlers.cpp
@@ -1427,19 +1427,18 @@ wasm::InInterruptibleCode(JSContext* cx,
 
     if (!cx->compartment())
         return false;
 
     *cs = LookupCodeSegment(pc);
     if (!*cs)
         return false;
 
-    const Code* code = (*cs)->code();
-
-    const CodeRange* codeRange = code->lookupRange(pc);
+    const Code& code = (*cs)->code();
+    const CodeRange* codeRange = code.lookupRange(pc);
     return codeRange && codeRange->isFunction();
 }
 
 // The return value indicates whether the PC was changed, not whether there was
 // a failure.
 static bool
 RedirectJitCodeToInterruptCheck(JSContext* cx, CONTEXT* context)
 {
diff --git a/js/src/wasm/WasmTypes.h b/js/src/wasm/WasmTypes.h
--- a/js/src/wasm/WasmTypes.h
+++ b/js/src/wasm/WasmTypes.h
@@ -36,18 +36,16 @@
 #include "js/UniquePtr.h"
 #include "js/Utility.h"
 #include "js/Vector.h"
 #include "vm/MallocProvider.h"
 #include "wasm/WasmBinaryConstants.h"
 
 namespace js {
 
-class PropertyName;
-class WasmFunctionCallObject;
 namespace jit {
     struct BaselineScript;
     enum class RoundingMode;
 }
 
 // This is a widespread header, so lets keep out the core wasm impl types.
 
 class WasmMemoryObject;
