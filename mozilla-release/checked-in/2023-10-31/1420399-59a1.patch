# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1513180145 21600
# Node ID 035c35133387ae8792fa589798f738e142b88166
# Parent  c8c223c3b01e157ce06f62ba7fbc47e640ea64b1
Bug 1420399 - Rename GCRuntime::hasBufferedGrayRoots(). r=pbone

Rename to hasValidGrayRootsBuffer which better matches the semantics of the
call.

diff --git a/js/src/gc/GCRuntime.h b/js/src/gc/GCRuntime.h
--- a/js/src/gc/GCRuntime.h
+++ b/js/src/gc/GCRuntime.h
@@ -1261,17 +1261,17 @@ class GCRuntime
     // them later, after black marking is complete for each compartment. This
     // accumulation can fail, but in that case we switch to non-incremental GC.
     enum class GrayBufferState {
         Unused,
         Okay,
         Failed
     };
     ActiveThreadOrGCTaskData<GrayBufferState> grayBufferState;
-    bool hasBufferedGrayRoots() const { return grayBufferState == GrayBufferState::Okay; }
+    bool hasValidGrayRootsBuffer() const { return grayBufferState == GrayBufferState::Okay; }
 
     // Clear each zone's gray buffers, but do not change the current state.
     void resetBufferedGrayRoots() const;
 
     // Reset the gray buffering state to Unused.
     void clearBufferedGrayRoots() {
         grayBufferState = GrayBufferState::Unused;
         resetBufferedGrayRoots();
diff --git a/js/src/jsgc.cpp b/js/src/jsgc.cpp
--- a/js/src/jsgc.cpp
+++ b/js/src/jsgc.cpp
@@ -4476,17 +4476,17 @@ GCRuntime::markWeakReferencesInCurrentGr
     markWeakReferences<SweepGroupZonesIter>(phase);
 }
 
 template <class ZoneIterT, class CompartmentIterT>
 void
 GCRuntime::markGrayReferences(gcstats::PhaseKind phase)
 {
     gcstats::AutoPhase ap(stats(), phase);
-    if (hasBufferedGrayRoots()) {
+    if (hasValidGrayRootsBuffer()) {
         for (ZoneIterT zone(rt); !zone.done(); zone.next())
             markBufferedGrayRoots(zone);
     } else {
         MOZ_ASSERT(!isIncremental);
         if (JSTraceDataOp op = grayRootTracer.op)
             (*op)(&marker, grayRootTracer.data);
     }
     auto unlimited = SliceBudget::unlimited();
@@ -7007,17 +7007,17 @@ GCRuntime::incrementalCollectSlice(Slice
 
         MOZ_FALLTHROUGH;
 
       case State::Mark:
         for (const CooperatingContext& target : rt->cooperatingContexts())
             AutoGCRooter::traceAllWrappers(target, &marker);
 
         /* If we needed delayed marking for gray roots, then collect until done. */
-        if (isIncremental && !hasBufferedGrayRoots()) {
+        if (isIncremental && !hasValidGrayRootsBuffer()) {
             budget.makeUnlimited();
             isIncremental = false;
             stats().nonincremental(AbortReason::GrayRootBufferingFailed);
         }
 
         if (drainMarkStack(budget, gcstats::PhaseKind::MARK) == NotFinished)
             break;
 
