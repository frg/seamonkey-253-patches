# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1513802006 -3600
#      Wed Dec 20 21:33:26 2017 +0100
# Node ID 5475206095b7b4ae1107d725ade1f37310aaf670
# Parent  82491c7ff5ef674a153a378a0497d5d28b434af1
Bug 1426486 - P1. Make GetInputStream()->AsSourceStream() invariant. r=pehrsons

It can never be nullptr, strongly assert that this is the case and remove unecessary tests.

MozReview-Commit-ID: 7fi6jNnFUH8

diff --git a/media/webrtc/signaling/src/mediapipeline/MediaPipeline.cpp b/media/webrtc/signaling/src/mediapipeline/MediaPipeline.cpp
--- a/media/webrtc/signaling/src/mediapipeline/MediaPipeline.cpp
+++ b/media/webrtc/signaling/src/mediapipeline/MediaPipeline.cpp
@@ -2030,55 +2030,56 @@ MediaPipelineTransmit::PipelineListener:
   NewData(aSegment);
 }
 
 class GenericReceiveListener : public MediaStreamListener
 {
 public:
   explicit GenericReceiveListener(dom::MediaStreamTrack* aTrack)
     : mTrack(aTrack)
+    , mSource(mTrack->GetInputStream()->AsSourceStream())
     , mPlayedTicks(0)
     , mPrincipalHandle(PRINCIPAL_HANDLE_NONE)
     , mListening(false)
     , mMaybeTrackNeedsUnmute(true)
   {
-    MOZ_ASSERT(aTrack->GetInputStream()->AsSourceStream());
+    MOZ_RELEASE_ASSERT(mSource, "Must be used with a SourceMediaStream");
   }
 
   virtual ~GenericReceiveListener()
   {
     NS_ReleaseOnMainThreadSystemGroup(
       "GenericReceiveListener::track_", mTrack.forget());
   }
 
   void AddSelf()
   {
     if (!mListening) {
       mListening = true;
-      mTrack->GetInputStream()->AddListener(this);
+      mSource->AddListener(this);
       mMaybeTrackNeedsUnmute = true;
     }
   }
 
   void RemoveSelf()
   {
     if (mListening) {
       mListening = false;
-      mTrack->GetInputStream()->RemoveListener(this);
+      mSource->RemoveListener(this);
     }
   }
 
   void OnRtpReceived()
   {
     if (mMaybeTrackNeedsUnmute) {
       mMaybeTrackNeedsUnmute = false;
-      NS_DispatchToMainThread(NewRunnableMethod(
-            "GenericReceiveListener::OnRtpReceived_m",
-            this,
-            &GenericReceiveListener::OnRtpReceived_m));
+      NS_DispatchToMainThread(
+        NewRunnableMethod("GenericReceiveListener::OnRtpReceived_m",
+                          this,
+                          &GenericReceiveListener::OnRtpReceived_m));
     }
   }
 
   void OnRtpReceived_m()
   {
     if (mListening && mTrack->Muted()) {
       mTrack->MutedChanged(false);
     }
@@ -2100,17 +2101,17 @@ public:
 
       void Run() override { mStream->AsSourceStream()->EndTrack(mTrackId); }
 
       const TrackID mTrackId;
     };
 
     mTrack->GraphImpl()->AppendMessage(MakeUnique<Message>(mTrack));
     // This breaks the cycle with the SourceMediaStream
-    mTrack->GetInputStream()->RemoveListener(this);
+    mSource->RemoveListener(this);
   }
 
   // Must be called on the main thread
   void SetPrincipalHandle_m(const PrincipalHandle& aPrincipalHandle)
   {
     class Message : public ControlMessage
     {
     public:
@@ -2138,16 +2139,17 @@ public:
   // Must be called on the MediaStreamGraph thread
   void SetPrincipalHandle_msg(const PrincipalHandle& aPrincipalHandle)
   {
     mPrincipalHandle = aPrincipalHandle;
   }
 
 protected:
   RefPtr<dom::MediaStreamTrack> mTrack;
+  const RefPtr<SourceMediaStream> mSource;
   TrackTicks mPlayedTicks;
   PrincipalHandle mPrincipalHandle;
   bool mListening;
   Atomic<bool> mMaybeTrackNeedsUnmute;
 };
 
 MediaPipelineReceive::MediaPipelineReceive(const std::string& aPc,
                                            nsCOMPtr<nsIEventTarget> aMainThread,
@@ -2166,55 +2168,43 @@ MediaPipelineReceive::~MediaPipelineRece
 class MediaPipelineReceiveAudio::PipelineListener
   : public GenericReceiveListener
 {
 public:
   PipelineListener(dom::MediaStreamTrack* aTrack,
                    const RefPtr<MediaSessionConduit>& aConduit)
     : GenericReceiveListener(aTrack)
     , mConduit(aConduit)
-    , mSource(mTrack->GetInputStream()->AsSourceStream())
     , mTrackId(mTrack->GetInputTrackId())
     // AudioSession conduit only supports 16, 32, 44.1 and 48kHz
     // This is an artificial limitation, it would however require more changes
     // to support any rates.
     // If the sampling rate is not-supported, we will use 48kHz instead.
-    , mRate(mSource ? (static_cast<AudioSessionConduit*>(mConduit.get())
-                           ->IsSamplingFreqSupported(mSource->GraphRate())
-                         ? mSource->GraphRate()
-                         : WEBRTC_MAX_SAMPLE_RATE)
-                    : WEBRTC_MAX_SAMPLE_RATE)
+    , mRate(static_cast<AudioSessionConduit*>(mConduit.get())
+                ->IsSamplingFreqSupported(mSource->GraphRate())
+              ? mSource->GraphRate()
+              : WEBRTC_MAX_SAMPLE_RATE)
     , mTaskQueue(
         new AutoTaskQueue(GetMediaThreadPool(MediaThreadType::WEBRTC_DECODER),
                           "AudioPipelineListener"))
     , mLastLog(0)
   {
-    MOZ_ASSERT(mSource);
   }
 
   // Implement MediaStreamListener
   void NotifyPull(MediaStreamGraph* aGraph,
                   StreamTime aDesiredTime) override
   {
-    if (!mSource) {
-      CSFLogError(LOGTAG, "NotifyPull() called from a non-SourceMediaStream");
-      return;
-    }
     NotifyPullImpl(aDesiredTime);
   }
 
   RefPtr<SourceMediaStream::NotifyPullPromise> AsyncNotifyPull(
     MediaStreamGraph* aGraph,
     StreamTime aDesiredTime) override
   {
-    if (!mSource) {
-      CSFLogError(LOGTAG, "NotifyPull() called from a non-SourceMediaStream");
-      return SourceMediaStream::NotifyPullPromise::CreateAndReject(true,
-                                                                   __func__);
-    }
     RefPtr<PipelineListener> self = this;
     return InvokeAsync(mTaskQueue, __func__, [self, aDesiredTime]() {
       self->NotifyPullImpl(aDesiredTime);
       return SourceMediaStream::NotifyPullPromise::CreateAndResolve(true,
                                                                     __func__);
     });
   }
 
@@ -2317,17 +2307,16 @@ private:
         // we can't un-read the data, but that's ok since we don't want to
         // buffer - but don't i-loop!
         break;
       }
     }
   }
 
   RefPtr<MediaSessionConduit> mConduit;
-  const RefPtr<SourceMediaStream> mSource;
   const TrackID mTrackId;
   const TrackRate mRate;
   const RefPtr<AutoTaskQueue> mTaskQueue;
   // Graph's current sampling rate
   TrackTicks mLastLog = 0; // mPlayedTicks when we last logged
 };
 
 MediaPipelineReceiveAudio::MediaPipelineReceiveAudio(
@@ -2410,18 +2399,17 @@ public:
     // Don't append if we've already provided a frame that supposedly
     // goes past the current aDesiredTime Doing so means a negative
     // delta and thus messes up handling of the graph
     if (delta > 0) {
       VideoSegment segment;
       IntSize size = image ? image->GetSize() : IntSize(mWidth, mHeight);
       segment.AppendFrame(image.forget(), delta, size, mPrincipalHandle);
       // Handle track not actually added yet or removed/finished
-      if (!mTrack->GetInputStream()->AsSourceStream()->AppendToTrack(
-            mTrack->GetInputTrackId(), &segment)) {
+      if (!mSource->AppendToTrack(mTrack->GetInputTrackId(), &segment)) {
         CSFLogError(LOGTAG, "AppendToTrack failed");
         return;
       }
       mPlayedTicks = aDesiredTime;
     }
   }
 
   // Accessors for external writes from the renderer
