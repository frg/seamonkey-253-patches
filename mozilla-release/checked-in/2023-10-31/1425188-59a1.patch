# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1512658345 0
# Node ID c85b83645bb39169625db1e46cc92db83ce1b9e4
# Parent  328818ca5c231beae14d611f4a356d5051c0527c
Bug 1425188 - Remove the ability to save as Postscript from the print dialog on Linux. r=karlt

Saving to PDF will now be the only option.

MozReview-Commit-ID: 9WIDws6lByG

diff --git a/widget/gtk/nsDeviceContextSpecG.cpp b/widget/gtk/nsDeviceContextSpecG.cpp
--- a/widget/gtk/nsDeviceContextSpecG.cpp
+++ b/widget/gtk/nsDeviceContextSpecG.cpp
@@ -145,18 +145,18 @@ already_AddRefed<PrintTarget> nsDeviceCo
     return nullptr;
 
   int16_t format;
   mPrintSettings->GetOutputFormat(&format);
 
   // Determine the real format with some GTK magic
   if (format == nsIPrintSettings::kOutputFormatNative) {
     if (mIsPPreview) {
-      // There is nothing to detect on Print Preview, use PS.
-      format = nsIPrintSettings::kOutputFormatPS;
+      // There is nothing to detect on Print Preview, use PDF.
+      format = nsIPrintSettings::kOutputFormatPDF;
     } else {
       return nullptr;
     }
   }
 
   IntSize size = IntSize::Truncate(width, height);
 
   if (format == nsIPrintSettings::kOutputFormatPDF) {
diff --git a/widget/gtk/nsPrintDialogGTK.cpp b/widget/gtk/nsPrintDialogGTK.cpp
--- a/widget/gtk/nsPrintDialogGTK.cpp
+++ b/widget/gtk/nsPrintDialogGTK.cpp
@@ -176,17 +176,16 @@ nsPrintDialogWidgetGTK::nsPrintDialogWid
   gtk_print_unix_dialog_set_manual_capabilities(GTK_PRINT_UNIX_DIALOG(dialog),
                     GtkPrintCapabilities(
                         GTK_PRINT_CAPABILITY_PAGE_SET
                       | GTK_PRINT_CAPABILITY_COPIES
                       | GTK_PRINT_CAPABILITY_COLLATE
                       | GTK_PRINT_CAPABILITY_REVERSE
                       | GTK_PRINT_CAPABILITY_SCALE
                       | GTK_PRINT_CAPABILITY_GENERATE_PDF
-                      | GTK_PRINT_CAPABILITY_GENERATE_PS
                     )
                   );
 
   // The vast majority of magic numbers in this widget construction are padding. e.g. for
   // the set_border_width below, 12px matches that of just about every other window.
   GtkWidget* custom_options_tab = gtk_vbox_new(FALSE, 0);
   gtk_container_set_border_width(GTK_CONTAINER(custom_options_tab), 12);
   GtkWidget* tab_label = gtk_label_new(GetUTF8FromBundle("optionsTabLabelGTK").get());
diff --git a/widget/gtk/nsPrintSettingsGTK.cpp b/widget/gtk/nsPrintSettingsGTK.cpp
--- a/widget/gtk/nsPrintSettingsGTK.cpp
+++ b/widget/gtk/nsPrintSettingsGTK.cpp
@@ -210,26 +210,17 @@ NS_IMETHODIMP nsPrintSettingsGTK::GetOut
 
   int16_t format;
   nsresult rv = nsPrintSettings::GetOutputFormat(&format);
   if (NS_FAILED(rv)) {
     return rv;
   }
 
   if (format == nsIPrintSettings::kOutputFormatNative) {
-    const gchar* fmtGTK =
-      gtk_print_settings_get(mPrintSettings,
-                             GTK_PRINT_SETTINGS_OUTPUT_FILE_FORMAT);
-    if (fmtGTK) {
-      if (nsDependentCString(fmtGTK).EqualsIgnoreCase("pdf")) {
-        format = nsIPrintSettings::kOutputFormatPDF;
-      } else {
-        format = nsIPrintSettings::kOutputFormatPS;
-      }
-    } else if (GTK_IS_PRINTER(mGTKPrinter)) {
+    if (GTK_IS_PRINTER(mGTKPrinter)) {
       // Prior to gtk 2.24, gtk_printer_accepts_pdf() and
       // gtk_printer_accepts_ps() always returned true regardless of the
       // printer's capability.
       bool shouldTrustGTK =
         (gtk_major_version > 2 ||
          (gtk_major_version == 2 && gtk_minor_version >= 24));
       bool acceptsPDF = shouldTrustGTK && gtk_printer_accepts_pdf(mGTKPrinter);
 
@@ -434,21 +425,17 @@ nsPrintSettingsGTK::SetToFileName(const 
 {
   if (aToFileName.IsEmpty()) {
     mToFileName.SetLength(0);
     gtk_print_settings_set(mPrintSettings, GTK_PRINT_SETTINGS_OUTPUT_URI,
                            nullptr);
     return NS_OK;
   }
 
-  if (StringEndsWith(aToFileName, NS_LITERAL_STRING(".ps"))) {
-    gtk_print_settings_set(mPrintSettings, GTK_PRINT_SETTINGS_OUTPUT_FILE_FORMAT, "ps");
-  } else {
-    gtk_print_settings_set(mPrintSettings, GTK_PRINT_SETTINGS_OUTPUT_FILE_FORMAT, "pdf");
-  }
+  gtk_print_settings_set(mPrintSettings, GTK_PRINT_SETTINGS_OUTPUT_FILE_FORMAT, "pdf");
 
   nsCOMPtr<nsIFile> file;
   nsresult rv = NS_NewLocalFile(aToFileName, true, getter_AddRefs(file));
   NS_ENSURE_SUCCESS(rv, rv);
 
   // Convert the nsIFile to a URL
   nsAutoCString url;
   rv = NS_GetURLSpecFromFile(file, url);
diff --git a/widget/nsPrintOptionsImpl.cpp b/widget/nsPrintOptionsImpl.cpp
--- a/widget/nsPrintOptionsImpl.cpp
+++ b/widget/nsPrintOptionsImpl.cpp
@@ -614,16 +614,24 @@ nsPrintOptions::ReadPrefs(nsIPrintSettin
     if (GETBOOLPREF(kPrintToFile, &b)) {
       aPS->SetPrintToFile(b);
       DUMP_BOOL(kReadStr, kPrintToFile, b);
     }
   }
 
   if (aFlags & nsIPrintSettings::kInitSaveToFileName) {
     if (GETSTRPREF(kPrintToFileName, str)) {
+      if (StringEndsWith(str, NS_LITERAL_STRING(".ps"))) {
+        // We only support PDF since bug 1425188 landed.  Users may still have
+        // prefs with .ps filenames if they last saved a file as Postscript
+        // though, so we fix that up here.  (The pref values will be
+        // overwritten the next time they save to file as a PDF.)
+        str.Truncate(str.Length() - 2);
+        str.AppendLiteral("pdf");
+      }
       aPS->SetToFileName(str);
       DUMP_STR(kReadStr, kPrintToFileName, str.get());
     }
   }
 
   if (aFlags & nsIPrintSettings::kInitSavePageDelay) {
     if (GETINTPREF(kPrintPageDelay, &iVal)) {
       aPS->SetPrintPageDelay(iVal);
