# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1505961413 -36000
# Node ID 6602b0670805a8303607ea45bebcbb15a59ee222
# Parent  63aca69b3c771b6c2978fb3979f90e94f27588e1
Bug 1401873 - Rename Atom as nsAtom. r=froydnj.

Because it's going to be exposed via nsIAtom.h in the next few patches.

MozReview-Commit-ID: A81s1nWrvmB

diff --git a/xpcom/ds/nsAtomTable.cpp b/xpcom/ds/nsAtomTable.cpp
--- a/xpcom/ds/nsAtomTable.cpp
+++ b/xpcom/ds/nsAtomTable.cpp
@@ -23,26 +23,26 @@
 #include "nsDataHashtable.h"
 #include "nsHashKeys.h"
 #include "nsAutoPtr.h"
 #include "nsUnicharUtils.h"
 #include "nsPrintfCString.h"
 
 // There are two kinds of atoms handled by this module.
 //
-// - Dynamic: the Atom itself is heap allocated, as is the nsStringBuffer it
-//   points to. |gAtomTable| holds weak references to dynamic Atoms. When the
-//   refcount of a dynamic Atom drops to zero, we increment a static counter.
-//   When that counter reaches a certain threshold, we iterate over the Atom
-//   table, removing and deleting dynamic Atoms with refcount zero. This allows
+// - Dynamic: the atom itself is heap allocated, as is the nsStringBuffer it
+//   points to. |gAtomTable| holds weak references to dynamic atoms. When the
+//   refcount of a dynamic atom drops to zero, we increment a static counter.
+//   When that counter reaches a certain threshold, we iterate over the atom
+//   table, removing and deleting dynamic atoms with refcount zero. This allows
 //   us to avoid acquiring the atom table lock during normal refcounting.
 //
-// - Static: the Atom itself is heap allocated, but it points to a static
-//   nsStringBuffer. |gAtomTable| effectively owns static Atoms, because such
-//   Atoms ignore all AddRef/Release calls, which ensures they stay alive until
+// - Static: the atom itself is heap allocated, but it points to a static
+//   nsStringBuffer. |gAtomTable| effectively owns static atoms, because such
+//   atoms ignore all AddRef/Release calls, which ensures they stay alive until
 //   |gAtomTable| itself is destroyed whereupon they are explicitly deleted.
 //
 // Note that gAtomTable is used on multiple threads, and callers must acquire
 // gAtomTableLock before touching it.
 
 using namespace mozilla;
 
 //----------------------------------------------------------------------
@@ -104,45 +104,45 @@ public:
 
 private:
   nsStringBuffer* mBuffer;
 };
 
 UniquePtr<nsTArray<FakeBufferRefcountHelper>> gFakeBuffers;
 #endif
 
-class Atom final : public nsIAtom
+class nsAtom final : public nsIAtom
 {
 public:
-  static already_AddRefed<Atom> CreateDynamic(const nsAString& aString,
-                                              uint32_t aHash)
+  static already_AddRefed<nsAtom> CreateDynamic(const nsAString& aString,
+                                                uint32_t aHash)
   {
     // The refcount is appropriately initialized in the constructor.
-    return dont_AddRef(new Atom(aString, aHash));
+    return dont_AddRef(new nsAtom(aString, aHash));
   }
 
-  static Atom* CreateStatic(nsStringBuffer* aStringBuffer, uint32_t aLength,
-                            uint32_t aHash)
+  static nsAtom* CreateStatic(nsStringBuffer* aStringBuffer, uint32_t aLength,
+                              uint32_t aHash)
   {
-    return new Atom(aStringBuffer, aLength, aHash);
+    return new nsAtom(aStringBuffer, aLength, aHash);
   }
 
   static void GCAtomTable();
 
   enum class GCKind {
     RegularOperation,
     Shutdown,
   };
 
   static void GCAtomTableLocked(const MutexAutoLock& aProofOfLock,
                                 GCKind aKind);
 
 private:
-  // This constructor is for dynamic Atoms.
-  Atom(const nsAString& aString, uint32_t aHash)
+  // This constructor is for dynamic atoms.
+  nsAtom(const nsAString& aString, uint32_t aHash)
     : mRefCnt(1)
   {
     mLength = aString.Length();
     SetKind(AtomKind::DynamicAtom);
     RefPtr<nsStringBuffer> buf = nsStringBuffer::FromString(aString);
     if (buf) {
       mString = static_cast<char16_t*>(buf->Data());
     } else {
@@ -165,18 +165,18 @@ private:
     NS_ASSERTION(buf && buf->StorageSize() >= (mLength + 1) * sizeof(char16_t),
                  "enough storage");
     NS_ASSERTION(Equals(aString), "correct data");
 
     // Take ownership of buffer
     mozilla::Unused << buf.forget();
   }
 
-  // This constructor is for static Atoms.
-  Atom(nsStringBuffer* aStringBuffer, uint32_t aLength, uint32_t aHash)
+  // This constructor is for static atoms.
+  nsAtom(nsStringBuffer* aStringBuffer, uint32_t aLength, uint32_t aHash)
   {
     mLength = aLength;
     SetKind(AtomKind::StaticAtom);
     mString = static_cast<char16_t*>(aStringBuffer->Data());
 
 #if defined(NS_BUILD_REFCNT_LOGGING)
     MOZ_ASSERT(NS_IsMainThread());
     if (!gFakeBuffers) {
@@ -194,20 +194,21 @@ private:
 
     MOZ_ASSERT(mString[mLength] == char16_t(0), "null terminated");
     MOZ_ASSERT(aStringBuffer &&
                aStringBuffer->StorageSize() == (mLength + 1) * sizeof(char16_t),
                "correct storage");
   }
 
 public:
-  // We don't need a virtual destructor because we always delete via an Atom*
-  // pointer (in AtomTableClearEntry() for static Atoms, and in
-  // GCAtomTableLocked() for dynamic Atoms), not an nsIAtom* pointer.
-  ~Atom() {
+  // We don't need a virtual destructor because we always delete via an nsAtom*
+  // pointer (in AtomTableClearEntry() for static atoms, and in
+  // GCAtomTableLocked() for dynamic atoms), not an nsIAtom* pointer.
+  ~nsAtom()
+  {
     if (IsDynamicAtom()) {
       nsStringBuffer::FromData(mString)->Release();
     } else {
       MOZ_ASSERT(IsStaticAtom());
     }
   }
 
   NS_DECL_NSIATOM
@@ -217,41 +218,41 @@ public:
   MozExternalRefCountType DynamicAddRef();
   MozExternalRefCountType DynamicRelease();
 
 protected:
   ThreadSafeAutoRefCnt mRefCnt;
   NS_DECL_OWNINGTHREAD
 };
 
-NS_IMPL_QUERY_INTERFACE(Atom, nsIAtom);
+NS_IMPL_QUERY_INTERFACE(nsAtom, nsIAtom);
 
 NS_IMETHODIMP
-Atom::ScriptableToString(nsAString& aBuf)
+nsAtom::ScriptableToString(nsAString& aBuf)
 {
   nsStringBuffer::FromData(mString)->ToString(mLength, aBuf);
   return NS_OK;
 }
 
 NS_IMETHODIMP
-Atom::ToUTF8String(nsACString& aBuf)
+nsAtom::ToUTF8String(nsACString& aBuf)
 {
   CopyUTF16toUTF8(nsDependentString(mString, mLength), aBuf);
   return NS_OK;
 }
 
 NS_IMETHODIMP
-Atom::ScriptableEquals(const nsAString& aString, bool* aResult)
+nsAtom::ScriptableEquals(const nsAString& aString, bool* aResult)
 {
   *aResult = aString.Equals(nsDependentString(mString, mLength));
   return NS_OK;
 }
 
 NS_IMETHODIMP_(size_t)
-Atom::SizeOfIncludingThis(MallocSizeOf aMallocSizeOf)
+nsAtom::SizeOfIncludingThis(MallocSizeOf aMallocSizeOf)
 {
   size_t n = aMallocSizeOf(this);
   // String buffers pointed to by static atoms are in static memory, and so
   // are not measured here.
   if (IsDynamicAtom()) {
     n += nsStringBuffer::FromData(mString)->SizeOfIncludingThisIfUnshared(
            aMallocSizeOf);
   } else {
@@ -265,28 +266,28 @@ Atom::SizeOfIncludingThis(MallocSizeOf a
 NS_IMETHODIMP_(MozExternalRefCountType)
 nsIAtom::AddRef()
 {
   MOZ_ASSERT(!IsHTML5Atom(), "Attempt to AddRef an nsHtml5Atom");
   if (!IsDynamicAtom()) {
     MOZ_ASSERT(IsStaticAtom());
     return 2;
   }
-  return static_cast<Atom*>(this)->DynamicAddRef();
+  return static_cast<nsAtom*>(this)->DynamicAddRef();
 }
 
 NS_IMETHODIMP_(MozExternalRefCountType)
 nsIAtom::Release()
 {
   MOZ_ASSERT(!IsHTML5Atom(), "Attempt to Release an nsHtml5Atom");
   if (!IsDynamicAtom()) {
     MOZ_ASSERT(IsStaticAtom());
     return 1;
   }
-  return static_cast<Atom*>(this)->DynamicRelease();
+  return static_cast<nsAtom*>(this)->DynamicRelease();
 }
 
 //----------------------------------------------------------------------
 
 /**
  * The shared hash table for atom lookups.
  *
  * Callers must hold gAtomTableLock before manipulating the table.
@@ -343,20 +344,20 @@ struct AtomTableKey
   const char16_t* mUTF16String;
   const char* mUTF8String;
   uint32_t mLength;
   uint32_t mHash;
 };
 
 struct AtomTableEntry : public PLDHashEntryHdr
 {
-  // These references are either to dynamic Atoms, in which case they are
-  // non-owning, or they are to static Atoms, which aren't really refcounted.
+  // These references are either to dynamic atoms, in which case they are
+  // non-owning, or they are to static atoms, which aren't really refcounted.
   // See the comment at the top of this file for more details.
-  Atom* MOZ_NON_OWNING_REF mAtom;
+  nsAtom* MOZ_NON_OWNING_REF mAtom;
 };
 
 static PLDHashNumber
 AtomTableGetHash(const void* aKey)
 {
   const AtomTableKey* k = static_cast<const AtomTableKey*>(aKey);
   return k->mHash;
 }
@@ -376,20 +377,20 @@ AtomTableMatchKey(const PLDHashEntryHdr*
 
   return he->mAtom->Equals(k->mUTF16String, k->mLength);
 }
 
 static void
 AtomTableClearEntry(PLDHashTable* aTable, PLDHashEntryHdr* aEntry)
 {
   auto entry = static_cast<AtomTableEntry*>(aEntry);
-  Atom* atom = entry->mAtom;
+  nsAtom* atom = entry->mAtom;
   if (atom->IsStaticAtom()) {
-    // This case -- when the entry being cleared holds a static Atom -- only
-    // occurs when gAtomTable is destroyed, whereupon all static Atoms within it
+    // This case -- when the entry being cleared holds a static atom -- only
+    // occurs when gAtomTable is destroyed, whereupon all static atoms within it
     // must be explicitly deleted.
     delete atom;
   }
 }
 
 static void
 AtomTableInitEntry(PLDHashEntryHdr* aEntry, const void* aKey)
 {
@@ -402,46 +403,46 @@ static const PLDHashTableOps AtomTableOp
   PLDHashTable::MoveEntryStub,
   AtomTableClearEntry,
   AtomTableInitEntry
 };
 
 //----------------------------------------------------------------------
 
 #define RECENTLY_USED_MAIN_THREAD_ATOM_CACHE_SIZE 31
-static Atom*
+static nsAtom*
   sRecentlyUsedMainThreadAtoms[RECENTLY_USED_MAIN_THREAD_ATOM_CACHE_SIZE] = {};
 
 void
-Atom::GCAtomTable()
+nsAtom::GCAtomTable()
 {
   if (NS_IsMainThread()) {
     MutexAutoLock lock(*gAtomTableLock);
     GCAtomTableLocked(lock, GCKind::RegularOperation);
   }
 }
 
 void
-Atom::GCAtomTableLocked(const MutexAutoLock& aProofOfLock, GCKind aKind)
+nsAtom::GCAtomTableLocked(const MutexAutoLock& aProofOfLock, GCKind aKind)
 {
   MOZ_ASSERT(NS_IsMainThread());
   for (uint32_t i = 0; i < RECENTLY_USED_MAIN_THREAD_ATOM_CACHE_SIZE; ++i) {
     sRecentlyUsedMainThreadAtoms[i] = nullptr;
   }
 
   int32_t removedCount = 0; // Use a non-atomic temporary for cheaper increments.
   nsAutoCString nonZeroRefcountAtoms;
   uint32_t nonZeroRefcountAtomsCount = 0;
   for (auto i = gAtomTable->Iter(); !i.Done(); i.Next()) {
     auto entry = static_cast<AtomTableEntry*>(i.Get());
     if (entry->mAtom->IsStaticAtom()) {
       continue;
     }
 
-    Atom* atom = entry->mAtom;
+    nsAtom* atom = entry->mAtom;
     if (atom->mRefCnt == 0) {
       i.Remove();
       delete atom;
       ++removedCount;
     }
 #ifdef NS_FREE_PERMANENT_DATA
     else if (aKind == GCKind::Shutdown && PR_GetEnv("XPCOM_MEM_BLOAT_LOG")) {
       // Only report leaking atoms in leak-checking builds in a run
@@ -483,17 +484,17 @@ Atom::GCAtomTableLocked(const MutexAutoL
   // it.
 
   MOZ_ASSERT_IF(aKind == GCKind::Shutdown, removedCount == gUnusedAtomCount);
 
   gUnusedAtomCount -= removedCount;
 }
 
 MozExternalRefCountType
-Atom::DynamicAddRef()
+nsAtom::DynamicAddRef()
 {
   MOZ_ASSERT(IsDynamicAtom());
   nsrefcnt count = ++mRefCnt;
   if (count == 1) {
     gUnusedAtomCount--;
   }
   return count;
 }
@@ -502,17 +503,17 @@ Atom::DynamicAddRef()
 // We set a lower GC threshold for atoms in debug builds so that we exercise
 // the GC machinery more often.
 static const int32_t kAtomGCThreshold = 20;
 #else
 static const int32_t kAtomGCThreshold = 10000;
 #endif
 
 MozExternalRefCountType
-Atom::DynamicRelease()
+nsAtom::DynamicRelease()
 {
   MOZ_ASSERT(IsDynamicAtom());
   MOZ_ASSERT(mRefCnt > 0);
   nsrefcnt count = --mRefCnt;
   if (count == 0) {
     if (++gUnusedAtomCount >= kAtomGCThreshold) {
       GCAtomTable();
     }
@@ -545,19 +546,19 @@ public:
   static KeyTypePointer KeyToPointer(KeyType aKey) { return &aKey; }
   static PLDHashNumber HashKey(KeyTypePointer aKey)
   {
     return HashString(*aKey);
   }
 
   enum { ALLOW_MEMMOVE = true };
 
-  // Static Atoms aren't really refcounted. Because these entries live in a
+  // Static atoms aren't really refcounted. Because these entries live in a
   // global hashtable, this reference is essentially owning.
-  Atom* MOZ_OWNING_REF mAtom;
+  nsAtom* MOZ_OWNING_REF mAtom;
 };
 
 /**
  * A hashtable of static atoms that existed at app startup. This hashtable
  * helps nsHtml5AtomTable.
  */
 typedef nsTHashtable<StaticAtomEntry> StaticAtomTable;
 static StaticAtomTable* gStaticAtomTable = nullptr;
@@ -612,17 +613,17 @@ NS_ShutdownAtomTable()
   delete gStaticAtomTable;
   gStaticAtomTable = nullptr;
 
 #ifdef NS_FREE_PERMANENT_DATA
   // Do a final GC to satisfy leak checking. We skip this step in release
   // builds.
   {
     MutexAutoLock lock(*gAtomTableLock);
-    Atom::GCAtomTableLocked(lock, Atom::GCKind::Shutdown);
+    nsAtom::GCAtomTableLocked(lock, nsAtom::GCKind::Shutdown);
   }
 #endif
 
   delete gAtomTable;
   gAtomTable = nullptr;
   delete gAtomTableLock;
   gAtomTableLock = nullptr;
 }
@@ -683,30 +684,30 @@ RegisterStaticAtoms(const nsStaticAtom* 
 
     uint32_t stringLen = stringBuffer->StorageSize() / sizeof(char16_t) - 1;
 
     uint32_t hash;
     AtomTableEntry* he =
       GetAtomHashEntry(static_cast<char16_t*>(stringBuffer->Data()),
                        stringLen, &hash);
 
-    Atom* atom = he->mAtom;
+    nsAtom* atom = he->mAtom;
     if (atom) {
       // Disallow creating a dynamic atom, and then later, while the
       // dynamic atom is still alive, registering that same atom as a
       // static atom.  It causes subtle bugs, and we're programming in
       // C++ here, not Smalltalk.
       if (!atom->IsStaticAtom()) {
         nsAutoCString name;
         atom->ToUTF8String(name);
         MOZ_CRASH_UNSAFE_PRINTF(
           "Static atom registration for %s should be pushed back", name.get());
       }
     } else {
-      atom = Atom::CreateStatic(stringBuffer, stringLen, hash);
+      atom = nsAtom::CreateStatic(stringBuffer, stringLen, hash);
       he->mAtom = atom;
     }
     *atomp = atom;
 
     if (!gStaticAtomTableSealed) {
       StaticAtomEntry* entry =
         gStaticAtomTable->PutEntry(nsDependentAtomString(atom));
       MOZ_ASSERT(atom->IsStaticAtom());
@@ -736,17 +737,17 @@ NS_Atomize(const nsACString& aUTF8String
     return atom.forget();
   }
 
   // This results in an extra addref/release of the nsStringBuffer.
   // Unfortunately there doesn't seem to be any APIs to avoid that.
   // Actually, now there is, sort of: ForgetSharedBuffer.
   nsString str;
   CopyUTF8toUTF16(aUTF8String, str);
-  RefPtr<Atom> atom = Atom::CreateDynamic(str, hash);
+  RefPtr<nsAtom> atom = nsAtom::CreateDynamic(str, hash);
 
   he->mAtom = atom;
 
   return atom.forget();
 }
 
 already_AddRefed<nsIAtom>
 NS_Atomize(const char16_t* aUTF16String)
@@ -764,60 +765,60 @@ NS_Atomize(const nsAString& aUTF16String
                                         &hash);
 
   if (he->mAtom) {
     nsCOMPtr<nsIAtom> atom = he->mAtom;
 
     return atom.forget();
   }
 
-  RefPtr<Atom> atom = Atom::CreateDynamic(aUTF16String, hash);
+  RefPtr<nsAtom> atom = nsAtom::CreateDynamic(aUTF16String, hash);
   he->mAtom = atom;
 
   return atom.forget();
 }
 
 already_AddRefed<nsIAtom>
 NS_AtomizeMainThread(const nsAString& aUTF16String)
 {
   MOZ_ASSERT(NS_IsMainThread());
   nsCOMPtr<nsIAtom> retVal;
   uint32_t hash;
   AtomTableKey key(aUTF16String.Data(), aUTF16String.Length(), &hash);
   uint32_t index = hash % RECENTLY_USED_MAIN_THREAD_ATOM_CACHE_SIZE;
-  Atom* atom = sRecentlyUsedMainThreadAtoms[index];
+  nsAtom* atom = sRecentlyUsedMainThreadAtoms[index];
   if (atom) {
     uint32_t length = atom->GetLength();
     if (length == key.mLength &&
         (memcmp(atom->GetUTF16String(),
                 key.mUTF16String, length * sizeof(char16_t)) == 0)) {
       retVal = atom;
       return retVal.forget();
     }
   }
 
   MutexAutoLock lock(*gAtomTableLock);
   AtomTableEntry* he = static_cast<AtomTableEntry*>(gAtomTable->Add(&key));
 
   if (he->mAtom) {
     retVal = he->mAtom;
   } else {
-    RefPtr<Atom> newAtom = Atom::CreateDynamic(aUTF16String, hash);
+    RefPtr<nsAtom> newAtom = nsAtom::CreateDynamic(aUTF16String, hash);
     he->mAtom = newAtom;
     retVal = newAtom.forget();
   }
 
   sRecentlyUsedMainThreadAtoms[index] = he->mAtom;
   return retVal.forget();
 }
 
 nsrefcnt
 NS_GetNumberOfAtoms(void)
 {
-  Atom::GCAtomTable(); // Trigger a GC so that we return a deterministic result.
+  nsAtom::GCAtomTable(); // Trigger a GC so we return a deterministic result.
   MutexAutoLock lock(*gAtomTableLock);
   return gAtomTable->EntryCount();
 }
 
 int32_t
 NS_GetUnusedAtomCount(void)
 {
   return gUnusedAtomCount;
