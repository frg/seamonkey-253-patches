# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1505185885 18000
# Node ID 623f813e5a7563a8b5065761324b007f32585d95
# Parent  ab82d9355a1ba41e7e90893e312758ef68b57952
servo: Merge #18452 - Overhaul MallocSizeOf and related things (from nnethercote:bug-1398737); r=jdm

This patch makes the MallocSizeOf stuff in Stylo work more like the HeapSizeOf
stuff already in Servo, except better. In particular, it adds deriving support
for MallocSizeOf, which will make it easier to improve coverage.

The patch does the following.

- Combines servo/components/style/stylesheets/memory.rs and the heapsize crate
  into a new crate, malloc_size_of.

- Forks the heapsize_derive crate, calling it malloc_size_of, so that
  MallocSizeOf can be derived.

- Both the new crates have MIT/Apache licenses, like heapsize, in case they are
  incorporated into heapsize in the future.

- Renames the methods within MallocSizeOf and the related traits so they are
  more concise.

- Removes MallocSizeOfWithGuard.

- Adds `derive(MallocSizeOf)` to a lot of types, in some cases replacing an
  equivalent or almost-equivalent hand-written implementation.

- Adds stuff so that Rc/Arc can be handled properly.

<!-- Please describe your changes on the following line: -->

---
<!-- Thank you for contributing to Servo! Please replace each `[ ]` by `[X]` when the step is complete, and replace `__` with appropriate data: -->
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [ ] These changes fix #__ (github issue number if applicable).

<!-- Either: -->
- [ ] There are tests for these changes OR
- [X] These changes do not require tests because tested on Gecko side.

<!-- Also, please make sure that "Allow edits from maintainers" checkbox is checked, so that we can help you if you get stuck somewhere along the way.-->

<!-- Pull requests that do not address these steps are welcome, but they will require additional verification as part of the review process. -->

Source-Repo: https://github.com/servo/servo
Source-Revision: 7f4cb1861b172423781a369b2decca6c65d60546

diff --git a/servo/components/malloc_size_of_derive/Cargo.toml b/servo/components/malloc_size_of_derive/Cargo.toml
--- a/servo/components/malloc_size_of_derive/Cargo.toml
+++ b/servo/components/malloc_size_of_derive/Cargo.toml
@@ -5,11 +5,11 @@ authors = ["The Servo Project Developers
 license = "MIT/Apache-2.0"
 publish = false
 
 [lib]
 path = "lib.rs"
 proc-macro = true
 
 [dependencies]
-quote = "0.3"
+quote = "0.3.15"
 syn = "0.11"
 synstructure = "0.5"
