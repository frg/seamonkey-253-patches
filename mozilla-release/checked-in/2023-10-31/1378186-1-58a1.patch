# HG changeset patch
# User Tom Schuster <evilpies@gmail.com>
# Date 1508352401 -7200
#      Wed Oct 18 20:46:41 2017 +0200
# Node ID 1e8b051b15f8418405b3b1bb8ca2f85a16bc8a91
# Parent  77886a3626c2e45fbaa38767fd81202849ce1ef9
Bug 1378186 - Fix GetElemSuper stack handling in baseline. r=jandem

diff --git a/js/src/jit-test/tests/basic/expression-autopsy.js b/js/src/jit-test/tests/basic/expression-autopsy.js
--- a/js/src/jit-test/tests/basic/expression-autopsy.js
+++ b/js/src/jit-test/tests/basic/expression-autopsy.js
@@ -145,16 +145,91 @@ check_one("super(...)",
               constructor() {
                 super(...[])();
               }
             }
             new X();
           },
           " is not a function");
 
+check_one("super.a",
+          function() {
+            class X extends Object {
+              test() {
+                super.a();
+              }
+            }
+            var x = new X();
+            x.test();
+          },
+          " is not a function");
+
+check_one("super[a]",
+          function() {
+            var a = "a";
+            class X extends Object {
+              test() {
+                super[a]();
+              }
+            }
+            var x = new X();
+            x.test();
+          },
+          " is not a function");
+
+check_one("super.a(...)",
+          function() {
+            class Y {
+              a() {
+                return 5;
+              }
+            }
+
+            class X extends Y {
+              test() {
+                super.a()();
+              }
+            }
+
+            var x = new X();
+            x.test();
+          },
+          " is not a function");
+
+check_one("super[a](...)",
+          function() {
+            class Y {
+              a() {
+                return 5;
+              }
+            }
+
+            var a = "a";
+            class X extends Y {
+              test() {
+                super[a]()();
+              }
+            }
+
+            var x = new X();
+            x.test();
+          },
+          " is not a function");
+
+check_one("super[1]",
+          function() {
+            class X extends Object {
+              foo() {
+                return super[1]();
+              }
+            }
+            new X().foo();
+          },
+          " is not a function");
+
 check_one("eval(...)",
           function() { eval("")(); },
           " is not a function");
 check_one("eval(...)",
           function() { "use strict"; eval("")(); },
           " is not a function");
 check_one("eval(...)",
           function() { eval(...[""])(); },
diff --git a/js/src/jit/BaselineBailouts.cpp b/js/src/jit/BaselineBailouts.cpp
--- a/js/src/jit/BaselineBailouts.cpp
+++ b/js/src/jit/BaselineBailouts.cpp
@@ -1109,16 +1109,22 @@ InitFromBailout(JSContext* cx, HandleScr
             JitSpew(JitSpew_BaselineBailouts, "      [TYPE-MONITOR CHAIN]");
             ICMonitoredFallbackStub* monFallbackStub = fallbackStub->toMonitoredFallbackStub();
             ICStub* firstMonStub = monFallbackStub->fallbackMonitorStub()->firstMonitorStub();
 
             // To enter a monitoring chain, we load the top stack value into R0
             JitSpew(JitSpew_BaselineBailouts, "      Popping top stack value into R0.");
             builder.popValueInto(PCMappingSlotInfo::SlotInR0);
 
+            if (JSOp(*pc) == JSOP_GETELEM_SUPER) {
+                // Push a fake value so that the stack stays balanced.
+                if (!builder.writeValue(UndefinedValue(), "GETELEM_SUPER stack blance"))
+                    return false;
+            }
+
             // Need to adjust the frameSize for the frame to match the values popped
             // into registers.
             frameSize -= sizeof(Value);
             blFrame->setFrameSize(frameSize);
             JitSpew(JitSpew_BaselineBailouts, "      Adjusted framesize -= %d: %d",
                             (int) sizeof(Value), (int) frameSize);
 
             // If resuming into a JSOP_CALL, baseline keeps the arguments on the
diff --git a/js/src/jit/BaselineCacheIRCompiler.cpp b/js/src/jit/BaselineCacheIRCompiler.cpp
--- a/js/src/jit/BaselineCacheIRCompiler.cpp
+++ b/js/src/jit/BaselineCacheIRCompiler.cpp
@@ -2090,16 +2090,21 @@ BaselineCacheIRCompiler::init(CacheKind 
       case CacheKind::SetProp:
       case CacheKind::In:
       case CacheKind::HasOwn:
         MOZ_ASSERT(numInputs == 2);
         allocator.initInputLocation(0, R0);
         allocator.initInputLocation(1, R1);
         break;
       case CacheKind::GetElemSuper:
+        MOZ_ASSERT(numInputs == 3);
+        allocator.initInputLocation(0, BaselineFrameSlot(0));
+        allocator.initInputLocation(1, R0);
+        allocator.initInputLocation(2, R1);
+        break;
       case CacheKind::SetElem:
         MOZ_ASSERT(numInputs == 3);
         allocator.initInputLocation(0, R0);
         allocator.initInputLocation(1, R1);
         allocator.initInputLocation(2, BaselineFrameSlot(0));
         break;
       case CacheKind::GetName:
       case CacheKind::BindName:
diff --git a/js/src/jit/BaselineCompiler.cpp b/js/src/jit/BaselineCompiler.cpp
--- a/js/src/jit/BaselineCompiler.cpp
+++ b/js/src/jit/BaselineCompiler.cpp
@@ -2286,31 +2286,31 @@ BaselineCompiler::emit_JSOP_GETELEM()
     // Mark R0 as pushed stack value.
     frame.push(R0);
     return true;
 }
 
 bool
 BaselineCompiler::emit_JSOP_GETELEM_SUPER()
 {
-    // Index -> R1, Receiver -> R2, Object -> R0
-    frame.popRegsAndSync(1);
-    masm.loadValue(frame.addressOfStackValue(frame.peek(-1)), R2);
-    masm.loadValue(frame.addressOfStackValue(frame.peek(-2)), R1);
-
-    // Keep receiver on stack.
-    frame.popn(2);
-    frame.push(R2);
-    frame.syncStack(0);
+    // Store obj in the scratch slot.
+    storeValue(frame.peek(-1), frame.addressOfScratchValue(), R2);
+    frame.pop();
+
+    // Keep index and receiver in R0 and R1.
+    frame.popRegsAndSync(2);
+
+    // Keep obj on the stack.
+    frame.pushScratchValue();
 
     ICGetElem_Fallback::Compiler stubCompiler(cx, /* hasReceiver = */ true);
     if (!emitOpIC(stubCompiler.getStub(&stubSpace_)))
         return false;
 
-    frame.pop();
+    frame.pop(); // This value is also popped in InitFromBailout.
     frame.push(R0);
     return true;
 }
 
 bool
 BaselineCompiler::emit_JSOP_CALLELEM()
 {
     return emit_JSOP_GETELEM();
diff --git a/js/src/jit/BaselineIC.cpp b/js/src/jit/BaselineIC.cpp
--- a/js/src/jit/BaselineIC.cpp
+++ b/js/src/jit/BaselineIC.cpp
@@ -855,17 +855,17 @@ DoGetElemFallback(JSContext* cx, Baselin
     if (!attached && !isTemporarilyUnoptimizable)
         stub->noteUnoptimizableAccess();
 
     return true;
 }
 
 static bool
 DoGetElemSuperFallback(JSContext* cx, BaselineFrame* frame, ICGetElem_Fallback* stub_,
-                       HandleValue receiver, HandleValue lhs, HandleValue rhs,
+                       HandleValue lhs, HandleValue receiver, HandleValue rhs,
                        MutableHandleValue res)
 {
     // This fallback stub may trigger debug mode toggling.
     DebugModeOSRVolatileStub<ICGetElem_Fallback*> stub(frame, stub_);
 
     RootedScript script(cx, frame->script());
     jsbytecode* pc = stub->icEntry()->pc(frame->script());
     StackTypeSet* types = TypeScript::BytecodeTypes(script, pc);
@@ -937,36 +937,41 @@ static const VMFunction DoGetElemFallbac
     FunctionInfo<DoGetElemFallbackFn>(DoGetElemFallback, "DoGetElemFallback", TailCall,
                                       PopValues(2));
 
 typedef bool (*DoGetElemSuperFallbackFn)(JSContext*, BaselineFrame*, ICGetElem_Fallback*,
                                          HandleValue, HandleValue, HandleValue,
                                          MutableHandleValue);
 static const VMFunction DoGetElemSuperFallbackInfo =
     FunctionInfo<DoGetElemSuperFallbackFn>(DoGetElemSuperFallback, "DoGetElemSuperFallback",
-                                           TailCall, PopValues(1));
+                                           TailCall, PopValues(3));
 
 bool
 ICGetElem_Fallback::Compiler::generateStubCode(MacroAssembler& masm)
 {
     MOZ_ASSERT(engine_ == Engine::Baseline);
     MOZ_ASSERT(R0 == JSReturnOperand);
 
     // Restore the tail call register.
     EmitRestoreTailCallReg(masm);
 
     // Super property getters use a |this| that differs from base object
     if (hasReceiver_) {
+        // State: index in R0, receiver in R1, obj on the stack
+
         // Ensure stack is fully synced for the expression decompiler.
+        // We need: index, receiver, obj
+        masm.pushValue(R0);
         masm.pushValue(R1);
-
-        masm.pushValue(R1); // Index
-        masm.pushValue(R0); // Object
-        masm.loadValue(Address(masm.getStackPointer(), 3 * sizeof(Value)), R0);
-        masm.pushValue(R0); // Receiver
+        masm.pushValue(Address(masm.getStackPointer(), sizeof(Value) * 2));
+
+        // Push arguments.
+        masm.pushValue(R0); // Index
+        masm.pushValue(R1); // Reciver
+        masm.pushValue(Address(masm.getStackPointer(), sizeof(Value) * 5)); // Obj
         masm.push(ICStubReg);
         pushStubPayload(masm, R0.scratchReg());
 
         return tailCallVM(DoGetElemSuperFallbackInfo, masm);
     }
 
     // Ensure stack is fully synced for the expression decompiler.
     masm.pushValue(R0);
