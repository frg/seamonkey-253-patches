# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1513677412 -28800
#      Tue Dec 19 17:56:52 2017 +0800
# Node ID c362d0ac18f44862da9b0593ad85ebfc56416325
# Parent  c688c8c074ca6bf0f1106b4e461f3899b328d41e
Bug 1427931. P1 - remove MediaDecoder::PinForSeek/UnpinForSeek. r=bechen,gerald

PinForSeek() is called only when playback reaches the end. In other words,
it is not called for most cases of seeking. It should be OK not to call it at
all during seeking.

MozReview-Commit-ID: 1xXX1321bO7

diff --git a/dom/media/ChannelMediaDecoder.cpp b/dom/media/ChannelMediaDecoder.cpp
--- a/dom/media/ChannelMediaDecoder.cpp
+++ b/dom/media/ChannelMediaDecoder.cpp
@@ -568,39 +568,16 @@ ChannelMediaDecoder::Resume()
 {
   MOZ_ASSERT(NS_IsMainThread());
   if (mResource) {
     mResource->Resume();
   }
 }
 
 void
-ChannelMediaDecoder::PinForSeek()
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  if (!mResource || mPinnedForSeek) {
-    return;
-  }
-  mPinnedForSeek = true;
-  mResource->Pin();
-}
-
-void
-ChannelMediaDecoder::UnpinForSeek()
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  MOZ_DIAGNOSTIC_ASSERT(!IsShutdown());
-  if (!mResource || !mPinnedForSeek) {
-    return;
-  }
-  mPinnedForSeek = false;
-  mResource->Unpin();
-}
-
-void
 ChannelMediaDecoder::MetadataLoaded(
   UniquePtr<MediaInfo> aInfo,
   UniquePtr<MetadataTags> aTags,
   MediaDecoderEventVisibility aEventVisibility)
 {
   MediaDecoder::MetadataLoaded(Move(aInfo), Move(aTags), aEventVisibility);
   // Set mode to PLAYBACK after reading metadata.
   mResource->SetReadMode(MediaCacheStream::MODE_PLAYBACK);
diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -98,18 +98,16 @@ public:
   already_AddRefed<nsIPrincipal> GetCurrentPrincipal() override;
   bool IsTransportSeekable() override;
   void SetLoadInBackground(bool aLoadInBackground) override;
   void Suspend() override;
   void Resume() override;
 
 private:
   void DownloadProgressed();
-  void PinForSeek() override;
-  void UnpinForSeek() override;
 
   // Create a new state machine to run this decoder.
   MediaDecoderStateMachine* CreateStateMachine();
 
   nsresult Load(BaseMediaResource* aOriginal);
 
   // Called by MediaResource when the download has ended.
   // Called on the main thread only. aStatus is the result from OnStopRequest.
@@ -148,20 +146,16 @@ private:
 
   bool ShouldThrottleDownload(const MediaStatistics& aStats);
 
   // Data needed to estimate playback data rate. The timeline used for
   // this estimate is "decode time" (where the "current time" is the
   // time of the last decoded video frame).
   MediaChannelStatistics mPlaybackStatistics;
 
-  // True when our media stream has been pinned. We pin the stream
-  // while seeking.
-  bool mPinnedForSeek = false;
-
   // Current playback position in the stream. This is (approximately)
   // where we're up to playing back the stream. This is not adjusted
   // during decoder seek operations, but it's updated at the end when we
   // start playing back again.
   int64_t mPlaybackPosition = 0;
 
   bool mCanPlayThrough = false;
 };
diff --git a/dom/media/MediaDecoder.cpp b/dom/media/MediaDecoder.cpp
--- a/dom/media/MediaDecoder.cpp
+++ b/dom/media/MediaDecoder.cpp
@@ -274,18 +274,16 @@ MediaDecoder::MediaDecoder(MediaDecoderI
 
 void
 MediaDecoder::Shutdown()
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_DIAGNOSTIC_ASSERT(!IsShutdown());
   AbstractThread::AutoEnter context(AbstractMainThread());
 
-  UnpinForSeek();
-
   // Unwatch all watch targets to prevent further notifications.
   mWatchManager.Shutdown();
 
   DiscardOngoingSeekIfExists();
 
   // This changes the decoder state to SHUTDOWN and does other things
   // necessary to unblock the state machine thread if it's blocked, so
   // the asynchronous shutdown in nsDestroyStateMachine won't deadlock.
@@ -535,17 +533,16 @@ MediaDecoder::Seek(double aTime, SeekTar
 
   mLogicalPosition = aTime;
 
   mLogicallySeeking = true;
   SeekTarget target = SeekTarget(timeUsecs, aSeekType);
   CallSeek(target);
 
   if (mPlayState == PLAY_STATE_ENDED) {
-    PinForSeek();
     ChangeState(GetOwner()->GetPaused() ? PLAY_STATE_PAUSED : PLAY_STATE_PLAYING);
   }
   return NS_OK;
 }
 
 void
 MediaDecoder::DiscardOngoingSeekIfExists()
 {
@@ -785,22 +782,17 @@ MediaDecoder::NotifyPrincipalChanged()
 void
 MediaDecoder::OnSeekResolved()
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_DIAGNOSTIC_ASSERT(!IsShutdown());
   AbstractThread::AutoEnter context(AbstractMainThread());
   mSeekRequest.Complete();
 
-  {
-    // An additional seek was requested while the current seek was
-    // in operation.
-    UnpinForSeek();
-    mLogicallySeeking = false;
-  }
+  mLogicallySeeking = false;
 
   // Ensure logical position is updated after seek.
   UpdateLogicalPositionInternal();
 
   GetOwner()->SeekCompleted();
   GetOwner()->AsyncResolveSeekDOMPromiseIfExists();
 }
 
diff --git a/dom/media/MediaDecoder.h b/dom/media/MediaDecoder.h
--- a/dom/media/MediaDecoder.h
+++ b/dom/media/MediaDecoder.h
@@ -472,22 +472,16 @@ protected:
   // the next frame is available.
   // An arbitrary value of 250ms is used.
   static constexpr auto DEFAULT_NEXT_FRAME_AVAILABLE_BUFFERED =
     media::TimeUnit::FromMicroseconds(250000);
 
   virtual nsCString GetDebugInfo();
 
 private:
-  // Ensures our media resource has been pinned.
-  virtual void PinForSeek() = 0;
-
-  // Ensures our media resource has been unpinned.
-  virtual void UnpinForSeek() = 0;
-
   // Called when the owner's activity changed.
   void NotifyCompositor();
 
   void OnPlaybackErrorEvent(const MediaResult& aError);
 
   void OnDecoderDoctorEvent(DecoderDoctorEvent aEvent);
 
   void OnMediaNotSeekable()
diff --git a/dom/media/hls/HLSDecoder.h b/dom/media/hls/HLSDecoder.h
--- a/dom/media/hls/HLSDecoder.h
+++ b/dom/media/hls/HLSDecoder.h
@@ -42,19 +42,16 @@ public:
   void Shutdown() override;
 
   // Called as data arrives on the underlying HLS player. Main thread only.
   void NotifyDataArrived();
 
 private:
   friend class HLSResourceCallbacksSupport;
 
-  void PinForSeek() override {}
-  void UnpinForSeek() override {}
-
   MediaDecoderStateMachine* CreateStateMachine();
 
   bool CanPlayThroughImpl() override final
   {
     // TODO: We don't know how to estimate 'canplaythrough' for this decoder.
     // For now we just return true for 'autoplay' can work.
     return true;
   }
diff --git a/dom/media/mediasource/MediaSourceDecoder.h b/dom/media/mediasource/MediaSourceDecoder.h
--- a/dom/media/mediasource/MediaSourceDecoder.h
+++ b/dom/media/mediasource/MediaSourceDecoder.h
@@ -68,18 +68,16 @@ public:
 
   void NotifyInitDataArrived();
 
   // Called as data appended to the source buffer or EOS is called on the media
   // source. Main thread only.
   void NotifyDataArrived();
 
 private:
-  void PinForSeek() override {}
-  void UnpinForSeek() override {}
   MediaDecoderStateMachine* CreateStateMachine();
   void DoSetMediaSourceDuration(double aDuration);
   media::TimeInterval ClampIntervalToEnd(const media::TimeInterval& aInterval);
   bool CanPlayThroughImpl() override;
 
   RefPtr<nsIPrincipal> mPrincipal;
 
   // The owning MediaSource holds a strong reference to this decoder, and
