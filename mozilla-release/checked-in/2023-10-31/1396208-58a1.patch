# HG changeset patch
# User Henri Sivonen <hsivonen@hsivonen.fi>
# Date 1507618106 -10800
# Node ID c0a033707d7a1905c4837b6265739105766d99a4
# Parent  4c0e2b13cdebdbb20567c36eac393b153d1c4fe4
Bug 1396208 - For benchmarking, add a black box function that is opaque to the optimizer. r=froydnj

MozReview-Commit-ID: 82jn6u6WRf0

diff --git a/testing/gtest/benchmark/AUTHORS b/testing/gtest/benchmark/AUTHORS
new file mode 100644
--- /dev/null
+++ b/testing/gtest/benchmark/AUTHORS
@@ -0,0 +1,39 @@
+# This is the official list of benchmark authors for copyright purposes.
+# This file is distinct from the CONTRIBUTORS files.
+# See the latter for an explanation.
+#
+# Names should be added to this file as:
+#	Name or Organization <email address>
+# The email address is not required for organizations.
+#
+# Please keep the list sorted.
+
+Albert Pretorius <pretoalb@gmail.com>
+Arne Beer <arne@twobeer.de>
+Christopher Seymour <chris.j.seymour@hotmail.com>
+David Coeurjolly <david.coeurjolly@liris.cnrs.fr>
+Dirac Research 
+Dominik Czarnota <dominik.b.czarnota@gmail.com>
+Eric Fiselier <eric@efcs.ca>
+Eugene Zhuk <eugene.zhuk@gmail.com>
+Evgeny Safronov <division494@gmail.com>
+Felix Homann <linuxaudio@showlabor.de>
+Google Inc.
+International Business Machines Corporation
+Ismael Jimenez Martinez <ismael.jimenez.martinez@gmail.com>
+Jern-Kuan Leong <jernkuan@gmail.com>
+JianXiong Zhou <zhoujianxiong2@gmail.com>
+Joao Paulo Magalhaes <joaoppmagalhaes@gmail.com>
+Jussi Knuuttila <jussi.knuuttila@gmail.com>
+Kaito Udagawa <umireon@gmail.com>
+Lei Xu <eddyxu@gmail.com>
+Matt Clarkson <mattyclarkson@gmail.com>
+Maxim Vafin <maxvafin@gmail.com>
+Nick Hutchinson <nshutchinson@gmail.com>
+Oleksandr Sochka <sasha.sochka@gmail.com>
+Paul Redmond <paul.redmond@gmail.com>
+Radoslav Yovchev <radoslav.tm@gmail.com>
+Shuo Chen <chenshuo@chenshuo.com>
+Yixuan Qiu <yixuanq@gmail.com>
+Yusuke Suzuki <utatane.tea@gmail.com>
+Zbigniew Skowron <zbychs@gmail.com>
diff --git a/testing/gtest/benchmark/BlackBox.cpp b/testing/gtest/benchmark/BlackBox.cpp
new file mode 100644
--- /dev/null
+++ b/testing/gtest/benchmark/BlackBox.cpp
@@ -0,0 +1,27 @@
+// Copyright 2015 Google Inc. All rights reserved.
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+//     http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+#if defined(_MSC_VER)
+
+#include "gtest/BlackBox.h"
+
+namespace mozilla {
+
+char volatile* UseCharPointer(char volatile* aPtr) {
+  return aPtr;
+}
+
+}  // namespace mozilla
+
+#endif
diff --git a/testing/gtest/benchmark/BlackBox.h b/testing/gtest/benchmark/BlackBox.h
new file mode 100644
--- /dev/null
+++ b/testing/gtest/benchmark/BlackBox.h
@@ -0,0 +1,61 @@
+// Copyright 2015 Google Inc. All rights reserved.
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+//     http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+#ifndef GTEST_BLACKBOX_H
+#define GTEST_BLACKBOX_H
+
+#include "mozilla/Attributes.h"
+#if defined(_MSC_VER)
+#include <intrin.h>
+#endif // _MSC_VER
+
+namespace mozilla {
+
+#if defined(_MSC_VER)
+
+char volatile* UseCharPointer(char volatile*);
+
+MOZ_ALWAYS_INLINE_EVEN_DEBUG void* BlackBoxVoidPtr(void* aPtr) {
+  aPtr = const_cast<char*>(UseCharPointer(reinterpret_cast<char*>(aPtr)));
+  _ReadWriteBarrier();
+  return aPtr;
+}
+
+#else
+
+// See: https://youtu.be/nXaxk27zwlk?t=2441
+MOZ_ALWAYS_INLINE_EVEN_DEBUG void* BlackBoxVoidPtr(void* aPtr) {
+  // "g" is what we want here, but the comment in the Google
+  // benchmark code suggests that GCC likes "i,r,m" better.
+  // However, on Mozilla try server i,r,m breaks GCC but g
+  // works in GCC, so using g for both clang and GCC.
+  // godbolt.org indicates that g works already in GCC 4.9,
+  // which is the oldest GCC we support at the time of this
+  // code landing. godbolt.org suggests that this clearly
+  // works is LLVM 5, but it's unclear if this inhibits
+  // all relevant optimizations properly on earlier LLVM.
+  asm volatile("" : "+g"(aPtr) : "g"(aPtr) : "memory");
+  return aPtr;
+}
+
+#endif // _MSC_VER
+
+template<class T>
+MOZ_ALWAYS_INLINE_EVEN_DEBUG T* BlackBox(T* aPtr) {
+  return static_cast<T*>(BlackBoxVoidPtr(aPtr));
+}
+
+} // namespace mozilla
+
+#endif // GTEST_BLACKBOX_H
diff --git a/testing/gtest/benchmark/LICENSE b/testing/gtest/benchmark/LICENSE
new file mode 100644
--- /dev/null
+++ b/testing/gtest/benchmark/LICENSE
@@ -0,0 +1,202 @@
+
+                                 Apache License
+                           Version 2.0, January 2004
+                        http://www.apache.org/licenses/
+
+   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
+
+   1. Definitions.
+
+      "License" shall mean the terms and conditions for use, reproduction,
+      and distribution as defined by Sections 1 through 9 of this document.
+
+      "Licensor" shall mean the copyright owner or entity authorized by
+      the copyright owner that is granting the License.
+
+      "Legal Entity" shall mean the union of the acting entity and all
+      other entities that control, are controlled by, or are under common
+      control with that entity. For the purposes of this definition,
+      "control" means (i) the power, direct or indirect, to cause the
+      direction or management of such entity, whether by contract or
+      otherwise, or (ii) ownership of fifty percent (50%) or more of the
+      outstanding shares, or (iii) beneficial ownership of such entity.
+
+      "You" (or "Your") shall mean an individual or Legal Entity
+      exercising permissions granted by this License.
+
+      "Source" form shall mean the preferred form for making modifications,
+      including but not limited to software source code, documentation
+      source, and configuration files.
+
+      "Object" form shall mean any form resulting from mechanical
+      transformation or translation of a Source form, including but
+      not limited to compiled object code, generated documentation,
+      and conversions to other media types.
+
+      "Work" shall mean the work of authorship, whether in Source or
+      Object form, made available under the License, as indicated by a
+      copyright notice that is included in or attached to the work
+      (an example is provided in the Appendix below).
+
+      "Derivative Works" shall mean any work, whether in Source or Object
+      form, that is based on (or derived from) the Work and for which the
+      editorial revisions, annotations, elaborations, or other modifications
+      represent, as a whole, an original work of authorship. For the purposes
+      of this License, Derivative Works shall not include works that remain
+      separable from, or merely link (or bind by name) to the interfaces of,
+      the Work and Derivative Works thereof.
+
+      "Contribution" shall mean any work of authorship, including
+      the original version of the Work and any modifications or additions
+      to that Work or Derivative Works thereof, that is intentionally
+      submitted to Licensor for inclusion in the Work by the copyright owner
+      or by an individual or Legal Entity authorized to submit on behalf of
+      the copyright owner. For the purposes of this definition, "submitted"
+      means any form of electronic, verbal, or written communication sent
+      to the Licensor or its representatives, including but not limited to
+      communication on electronic mailing lists, source code control systems,
+      and issue tracking systems that are managed by, or on behalf of, the
+      Licensor for the purpose of discussing and improving the Work, but
+      excluding communication that is conspicuously marked or otherwise
+      designated in writing by the copyright owner as "Not a Contribution."
+
+      "Contributor" shall mean Licensor and any individual or Legal Entity
+      on behalf of whom a Contribution has been received by Licensor and
+      subsequently incorporated within the Work.
+
+   2. Grant of Copyright License. Subject to the terms and conditions of
+      this License, each Contributor hereby grants to You a perpetual,
+      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
+      copyright license to reproduce, prepare Derivative Works of,
+      publicly display, publicly perform, sublicense, and distribute the
+      Work and such Derivative Works in Source or Object form.
+
+   3. Grant of Patent License. Subject to the terms and conditions of
+      this License, each Contributor hereby grants to You a perpetual,
+      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
+      (except as stated in this section) patent license to make, have made,
+      use, offer to sell, sell, import, and otherwise transfer the Work,
+      where such license applies only to those patent claims licensable
+      by such Contributor that are necessarily infringed by their
+      Contribution(s) alone or by combination of their Contribution(s)
+      with the Work to which such Contribution(s) was submitted. If You
+      institute patent litigation against any entity (including a
+      cross-claim or counterclaim in a lawsuit) alleging that the Work
+      or a Contribution incorporated within the Work constitutes direct
+      or contributory patent infringement, then any patent licenses
+      granted to You under this License for that Work shall terminate
+      as of the date such litigation is filed.
+
+   4. Redistribution. You may reproduce and distribute copies of the
+      Work or Derivative Works thereof in any medium, with or without
+      modifications, and in Source or Object form, provided that You
+      meet the following conditions:
+
+      (a) You must give any other recipients of the Work or
+          Derivative Works a copy of this License; and
+
+      (b) You must cause any modified files to carry prominent notices
+          stating that You changed the files; and
+
+      (c) You must retain, in the Source form of any Derivative Works
+          that You distribute, all copyright, patent, trademark, and
+          attribution notices from the Source form of the Work,
+          excluding those notices that do not pertain to any part of
+          the Derivative Works; and
+
+      (d) If the Work includes a "NOTICE" text file as part of its
+          distribution, then any Derivative Works that You distribute must
+          include a readable copy of the attribution notices contained
+          within such NOTICE file, excluding those notices that do not
+          pertain to any part of the Derivative Works, in at least one
+          of the following places: within a NOTICE text file distributed
+          as part of the Derivative Works; within the Source form or
+          documentation, if provided along with the Derivative Works; or,
+          within a display generated by the Derivative Works, if and
+          wherever such third-party notices normally appear. The contents
+          of the NOTICE file are for informational purposes only and
+          do not modify the License. You may add Your own attribution
+          notices within Derivative Works that You distribute, alongside
+          or as an addendum to the NOTICE text from the Work, provided
+          that such additional attribution notices cannot be construed
+          as modifying the License.
+
+      You may add Your own copyright statement to Your modifications and
+      may provide additional or different license terms and conditions
+      for use, reproduction, or distribution of Your modifications, or
+      for any such Derivative Works as a whole, provided Your use,
+      reproduction, and distribution of the Work otherwise complies with
+      the conditions stated in this License.
+
+   5. Submission of Contributions. Unless You explicitly state otherwise,
+      any Contribution intentionally submitted for inclusion in the Work
+      by You to the Licensor shall be under the terms and conditions of
+      this License, without any additional terms or conditions.
+      Notwithstanding the above, nothing herein shall supersede or modify
+      the terms of any separate license agreement you may have executed
+      with Licensor regarding such Contributions.
+
+   6. Trademarks. This License does not grant permission to use the trade
+      names, trademarks, service marks, or product names of the Licensor,
+      except as required for reasonable and customary use in describing the
+      origin of the Work and reproducing the content of the NOTICE file.
+
+   7. Disclaimer of Warranty. Unless required by applicable law or
+      agreed to in writing, Licensor provides the Work (and each
+      Contributor provides its Contributions) on an "AS IS" BASIS,
+      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
+      implied, including, without limitation, any warranties or conditions
+      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
+      PARTICULAR PURPOSE. You are solely responsible for determining the
+      appropriateness of using or redistributing the Work and assume any
+      risks associated with Your exercise of permissions under this License.
+
+   8. Limitation of Liability. In no event and under no legal theory,
+      whether in tort (including negligence), contract, or otherwise,
+      unless required by applicable law (such as deliberate and grossly
+      negligent acts) or agreed to in writing, shall any Contributor be
+      liable to You for damages, including any direct, indirect, special,
+      incidental, or consequential damages of any character arising as a
+      result of this License or out of the use or inability to use the
+      Work (including but not limited to damages for loss of goodwill,
+      work stoppage, computer failure or malfunction, or any and all
+      other commercial damages or losses), even if such Contributor
+      has been advised of the possibility of such damages.
+
+   9. Accepting Warranty or Additional Liability. While redistributing
+      the Work or Derivative Works thereof, You may choose to offer,
+      and charge a fee for, acceptance of support, warranty, indemnity,
+      or other liability obligations and/or rights consistent with this
+      License. However, in accepting such obligations, You may act only
+      on Your own behalf and on Your sole responsibility, not on behalf
+      of any other Contributor, and only if You agree to indemnify,
+      defend, and hold each Contributor harmless for any liability
+      incurred by, or claims asserted against, such Contributor by reason
+      of your accepting any such warranty or additional liability.
+
+   END OF TERMS AND CONDITIONS
+
+   APPENDIX: How to apply the Apache License to your work.
+
+      To apply the Apache License to your work, attach the following
+      boilerplate notice, with the fields enclosed by brackets "[]"
+      replaced with your own identifying information. (Don't include
+      the brackets!)  The text should be enclosed in the appropriate
+      comment syntax for the file format. We also recommend that a
+      file or class name and description of purpose be included on the
+      same "printed page" as the copyright notice for easier
+      identification within third-party archives.
+
+   Copyright [yyyy] [name of copyright owner]
+
+   Licensed under the Apache License, Version 2.0 (the "License");
+   you may not use this file except in compliance with the License.
+   You may obtain a copy of the License at
+
+       http://www.apache.org/licenses/LICENSE-2.0
+
+   Unless required by applicable law or agreed to in writing, software
+   distributed under the License is distributed on an "AS IS" BASIS,
+   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+   See the License for the specific language governing permissions and
+   limitations under the License.
diff --git a/testing/gtest/benchmark/README.txt b/testing/gtest/benchmark/README.txt
new file mode 100644
--- /dev/null
+++ b/testing/gtest/benchmark/README.txt
@@ -0,0 +1,5 @@
+The files in this directory are adapted from the Google benchmark library
+(https://github.com/google/benchmark) at git revision
+a96ff121b34532bb007c51ffd8e626e38decd732.
+
+The AUTHORS and LICENSE files in this directory are copied from there.
diff --git a/testing/gtest/benchmark/moz.build b/testing/gtest/benchmark/moz.build
new file mode 100644
--- /dev/null
+++ b/testing/gtest/benchmark/moz.build
@@ -0,0 +1,11 @@
+# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
+# vim: set filetype=python:
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+if CONFIG['ENABLE_TESTS']:
+    SOURCES += [
+        'BlackBox.cpp',
+    ]
+
+    FINAL_LIBRARY = 'xul-gtest'
diff --git a/testing/gtest/moz.build b/testing/gtest/moz.build
--- a/testing/gtest/moz.build
+++ b/testing/gtest/moz.build
@@ -5,16 +5,17 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 EXPORTS.gtest += [
     'MozGtestFriend.h',
 ]
 
 if CONFIG['ENABLE_TESTS']:
     gtest_exports = [
+        'benchmark/BlackBox.h',
         'gtest/include/gtest/gtest-death-test.h',
         'gtest/include/gtest/gtest-message.h',
         'gtest/include/gtest/gtest-param-test.h',
         'gtest/include/gtest/gtest-printers.h',
         'gtest/include/gtest/gtest-spi.h',
         'gtest/include/gtest/gtest-test-part.h',
         'gtest/include/gtest/gtest-typed-test.h',
         'gtest/include/gtest/gtest.h',
@@ -92,11 +93,11 @@ if CONFIG['ENABLE_TESTS']:
         'gtest/include',
     ]
 
     if CONFIG['OS_ARCH'] == 'WINNT':
         LOCAL_INCLUDES += [
             '/security/sandbox/chromium',
         ]
 
-    DIRS += ['mozilla']
+    DIRS += ['benchmark', 'mozilla']
 
     FINAL_LIBRARY = 'xul-gtest'
diff --git a/xpcom/tests/gtest/TestStrings.cpp b/xpcom/tests/gtest/TestStrings.cpp
--- a/xpcom/tests/gtest/TestStrings.cpp
+++ b/xpcom/tests/gtest/TestStrings.cpp
@@ -11,20 +11,22 @@
 #include "nsStringBuffer.h"
 #include "nsReadableUtils.h"
 #include "nsCRTGlue.h"
 #include "mozilla/RefPtr.h"
 #include "mozilla/Unused.h"
 #include "nsTArray.h"
 #include "gtest/gtest.h"
 #include "gtest/MozGTestBench.h" // For MOZ_GTEST_BENCH
+#include "gtest/BlackBox.h"
 
 namespace TestStrings {
 
 using mozilla::fallible;
+using mozilla::BlackBox;
 
 void test_assign_helper(const nsACString& in, nsACString &_retval)
 {
   _retval = in;
 }
 
 // Simple helper struct to test if conditionally enabled string functions are
 // working.
@@ -1388,62 +1390,70 @@ MOZ_GTEST_BENCH(Strings, PerfStripCharsC
 #define FifteenASCII "Lorem ipsum dol"
 
 // Around hundred is common length for IsUTF8 check
 #define HundredASCII "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ac tellus eget velit viverra viverra i"
 
 MOZ_GTEST_BENCH(Strings, PerfIsUTF8One, [] {
     nsCString test(OneASCII);
     for (int i = 0; i < 200000; i++) {
-      IsUTF8(test);
+      bool b = IsUTF8(*BlackBox(&test));
+      BlackBox(&b);
     }
 });
 
 MOZ_GTEST_BENCH(Strings, PerfIsUTF8Fifteen, [] {
     nsCString test(FifteenASCII);
     for (int i = 0; i < 200000; i++) {
-      IsUTF8(test);
+      bool b = IsUTF8(*BlackBox(&test));
+      BlackBox(&b);
     }
 });
 
 MOZ_GTEST_BENCH(Strings, PerfIsUTF8Hundred, [] {
     nsCString test(HundredASCII);
     for (int i = 0; i < 200000; i++) {
-      IsUTF8(test);
+      bool b = IsUTF8(*BlackBox(&test));
+      BlackBox(&b);
     }
 });
 
 MOZ_GTEST_BENCH(Strings, PerfIsUTF8Example3, [] {
     nsCString test(TestExample3);
     for (int i = 0; i < 100000; i++) {
-      IsUTF8(test);
+      bool b = IsUTF8(*BlackBox(&test));
+      BlackBox(&b);
     }
 });
 
 MOZ_GTEST_BENCH(Strings, PerfIsASCII8One, [] {
     nsCString test(OneASCII);
     for (int i = 0; i < 200000; i++) {
-      IsASCII(test);
+      bool b = IsASCII(*BlackBox(&test));
+      BlackBox(&b);
     }
 });
 
 MOZ_GTEST_BENCH(Strings, PerfIsASCIIFifteen, [] {
     nsCString test(FifteenASCII);
     for (int i = 0; i < 200000; i++) {
-      IsASCII(test);
+      bool b = IsASCII(*BlackBox(&test));
+      BlackBox(&b);
     }
 });
 
 MOZ_GTEST_BENCH(Strings, PerfIsASCIIHundred, [] {
     nsCString test(HundredASCII);
     for (int i = 0; i < 200000; i++) {
-      IsASCII(test);
+      bool b = IsASCII(*BlackBox(&test));
+      BlackBox(&b);
     }
 });
 
 MOZ_GTEST_BENCH(Strings, PerfIsASCIIExample3, [] {
     nsCString test(TestExample3);
     for (int i = 0; i < 100000; i++) {
-      IsUTF8(test);
+      bool b = IsASCII(*BlackBox(&test));
+      BlackBox(&b);
     }
 });
 
 } // namespace TestStrings
