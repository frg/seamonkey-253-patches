# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1510338475 -3600
#      Fri Nov 10 19:27:55 2017 +0100
# Node ID a9246983376b7ed12da57765b3ad677342f9c234
# Parent  49c77a589977fc90f3168ee8fafd23a548a9d0d9
Bug 1416174 - part 3 - moving gPaths to mPaths,r=smaug

diff --git a/dom/system/OSFileConstants.cpp b/dom/system/OSFileConstants.cpp
--- a/dom/system/OSFileConstants.cpp
+++ b/dom/system/OSFileConstants.cpp
@@ -88,17 +88,21 @@
 
 
 namespace mozilla {
 
 namespace {
 
 StaticRefPtr<OSFileConstantsService> gInstance;
 
-struct Paths {
+} // anonymous namespace
+
+struct
+OSFileConstantsService::Paths
+{
   /**
    * The name of the directory holding all the libraries (libxpcom, libnss, etc.)
    */
   nsString libDir;
   nsString tmpDir;
   nsString profileDir;
   nsString localProfileDir;
   /**
@@ -172,29 +176,16 @@ struct Paths {
     macUserLibDir.SetIsVoid(true);
     macLocalApplicationsDir.SetIsVoid(true);
     macTrashDir.SetIsVoid(true);
 #endif // defined(XP_MACOSX)
   }
 };
 
 /**
- * System directories.
- */
-Paths* gPaths = nullptr;
-
-/**
- * (Unix) the umask, which goes in OS.Constants.Sys but
- * can only be looked up (via the system-info service)
- * on the main thread.
- */
-uint32_t gUserUmask = 0;
-} // namespace
-
-/**
  * Return the path to one of the special directories.
  *
  * @param aKey The key to the special directory (e.g. "TmpD", "ProfD", ...)
  * @param aOutPath The path to the special directory. In case of error,
  * the string is set to void.
  */
 nsresult GetPathToSpecialDir(const char *aKey, nsString& aOutPath)
 {
@@ -217,26 +208,27 @@ nsresult GetPathToSpecialDir(const char 
  * For this purpose, we register an observer to set |mPaths->profileDir|
  * and |mPaths->localProfileDir| once the profile is setup.
  */
 NS_IMETHODIMP
 OSFileConstantsService::Observe(nsISupports*,
                                 const char* aTopic,
                                 const char16_t*)
 {
-  if (gPaths == nullptr) {
-    // Initialization of gPaths has not taken place, something is wrong,
+  if (!mInitialized) {
+    // Initialization has not taken place, something is wrong,
     // don't make things worse.
     return NS_OK;
   }
-  nsresult rv = GetPathToSpecialDir(NS_APP_USER_PROFILE_50_DIR, gPaths->profileDir);
+
+  nsresult rv = GetPathToSpecialDir(NS_APP_USER_PROFILE_50_DIR, mPaths->profileDir);
   if (NS_FAILED(rv)) {
     return rv;
   }
-  rv = GetPathToSpecialDir(NS_APP_USER_PROFILE_LOCAL_50_DIR, gPaths->localProfileDir);
+  rv = GetPathToSpecialDir(NS_APP_USER_PROFILE_LOCAL_50_DIR, mPaths->localProfileDir);
   if (NS_FAILED(rv)) {
     return rv;
   }
 
   return NS_OK;
 }
 
 /**
@@ -306,24 +298,24 @@ OSFileConstantsService::InitOSFileConsta
 #endif // defined(XP_WIN)
 
 #if defined(XP_MACOSX)
   GetPathToSpecialDir(NS_MAC_USER_LIB_DIR, paths->macUserLibDir);
   GetPathToSpecialDir(NS_OSX_LOCAL_APPLICATIONS_DIR, paths->macLocalApplicationsDir);
   GetPathToSpecialDir(NS_MAC_TRASH_DIR, paths->macTrashDir);
 #endif // defined(XP_MACOSX)
 
-  gPaths = paths.forget();
+  mPaths = Move(paths);
 
   // Get the umask from the system-info service.
   // The property will always be present, but it will be zero on
   // non-Unix systems.
   // nsSystemInfo::gUserUmask is initialized by NS_InitXPCOM2 so we don't need
   // to initialize the service.
-  gUserUmask = nsSystemInfo::gUserUmask;
+  mUserUmask = nsSystemInfo::gUserUmask;
 
   mInitialized = true;
   return NS_OK;
 }
 
 /**
  * Define a simple read-only property holding an integer.
  *
@@ -846,21 +838,17 @@ bool SetStringProperty(JSContext *cx, JS
  *
  * This function creates or uses JS object |OS.Constants| to store
  * all its constants.
  */
 bool
 OSFileConstantsService::DefineOSFileConstants(JSContext* aCx,
                                               JS::Handle<JSObject*> aGlobal)
 {
-  if (!mInitialized || gPaths == nullptr) {
-    // If an initialization error was ignored, we may end up with
-    // |gInitialized == true| but |gPaths == nullptr|. We cannot
-    // |MOZ_ASSERT| this, as this would kill precompile_cache.js,
-    // so we simply return an error.
+  if (!mInitialized) {
     JS_ReportErrorNumberASCII(aCx, js::GetErrorMessage, nullptr,
                               JSMSG_CANT_OPEN,
                               "OSFileConstants", "initialization has failed");
     return false;
   }
 
   JS::Rooted<JSObject*> objOS(aCx);
   if (!(objOS = GetOrCreateObjectProperty(aCx, aGlobal, "OS"))) {
@@ -928,17 +916,17 @@ OSFileConstantsService::DefineOSFileCons
   JS::Rooted<JS::Value> valBits(aCx, JS::Int32Value(64));
 #else
   JS::Rooted<JS::Value> valBits(aCx, JS::Int32Value(32));
 #endif //defined (HAVE_64BIT_BUILD)
   if (!JS_SetProperty(aCx, objSys, "bits", valBits)) {
     return false;
   }
 
-  if (!JS_DefineProperty(aCx, objSys, "umask", gUserUmask,
+  if (!JS_DefineProperty(aCx, objSys, "umask", mUserUmask,
                          JSPROP_ENUMERATE | JSPROP_READONLY | JSPROP_PERMANENT)) {
       return false;
   }
 
   // Build OS.Constants.Path
 
   JS::Rooted<JSObject*> objPath(aCx);
   if (!(objPath = GetOrCreateObjectProperty(aCx, objConstants, "Path"))) {
@@ -948,83 +936,83 @@ OSFileConstantsService::DefineOSFileCons
   // Locate libxul
   // Note that we don't actually provide the full path, only the name of the
   // library, which is sufficient to link to the library using js-ctypes.
 
 #if defined(XP_MACOSX)
   // Under MacOS X, for some reason, libxul is called simply "XUL",
   // and we need to provide the full path.
   nsAutoString libxul;
-  libxul.Append(gPaths->libDir);
+  libxul.Append(mPaths->libDir);
   libxul.AppendLiteral("/XUL");
 #else
   // On other platforms, libxul is a library "xul" with regular
   // library prefix/suffix.
   nsAutoString libxul;
   libxul.AppendLiteral(DLL_PREFIX);
   libxul.AppendLiteral("xul");
   libxul.AppendLiteral(DLL_SUFFIX);
 #endif // defined(XP_MACOSX)
 
   if (!SetStringProperty(aCx, objPath, "libxul", libxul)) {
     return false;
   }
 
-  if (!SetStringProperty(aCx, objPath, "libDir", gPaths->libDir)) {
+  if (!SetStringProperty(aCx, objPath, "libDir", mPaths->libDir)) {
     return false;
   }
 
-  if (!SetStringProperty(aCx, objPath, "tmpDir", gPaths->tmpDir)) {
+  if (!SetStringProperty(aCx, objPath, "tmpDir", mPaths->tmpDir)) {
     return false;
   }
 
   // Configure profileDir only if it is available at this stage
-  if (!gPaths->profileDir.IsVoid()
-    && !SetStringProperty(aCx, objPath, "profileDir", gPaths->profileDir)) {
+  if (!mPaths->profileDir.IsVoid()
+    && !SetStringProperty(aCx, objPath, "profileDir", mPaths->profileDir)) {
     return false;
   }
 
   // Configure localProfileDir only if it is available at this stage
-  if (!gPaths->localProfileDir.IsVoid()
-    && !SetStringProperty(aCx, objPath, "localProfileDir", gPaths->localProfileDir)) {
+  if (!mPaths->localProfileDir.IsVoid()
+    && !SetStringProperty(aCx, objPath, "localProfileDir", mPaths->localProfileDir)) {
     return false;
   }
 
-  if (!SetStringProperty(aCx, objPath, "homeDir", gPaths->homeDir)) {
+  if (!SetStringProperty(aCx, objPath, "homeDir", mPaths->homeDir)) {
     return false;
   }
 
-  if (!SetStringProperty(aCx, objPath, "desktopDir", gPaths->desktopDir)) {
+  if (!SetStringProperty(aCx, objPath, "desktopDir", mPaths->desktopDir)) {
     return false;
   }
 
-  if (!SetStringProperty(aCx, objPath, "userApplicationDataDir", gPaths->userApplicationDataDir)) {
+  if (!SetStringProperty(aCx, objPath, "userApplicationDataDir", mPaths->userApplicationDataDir)) {
     return false;
   }
 
 #if defined(XP_WIN)
-  if (!SetStringProperty(aCx, objPath, "winAppDataDir", gPaths->winAppDataDir)) {
+  if (!SetStringProperty(aCx, objPath, "winAppDataDir", mPaths->winAppDataDir)) {
     return false;
   }
 
-  if (!SetStringProperty(aCx, objPath, "winStartMenuProgsDir", gPaths->winStartMenuProgsDir)) {
+  if (!SetStringProperty(aCx, objPath, "winStartMenuProgsDir", mPaths->winStartMenuProgsDir)) {
     return false;
   }
 #endif // defined(XP_WIN)
 
 #if defined(XP_MACOSX)
-  if (!SetStringProperty(aCx, objPath, "macUserLibDir", gPaths->macUserLibDir)) {
+  if (!SetStringProperty(aCx, objPath, "macUserLibDir", mPaths->macUserLibDir)) {
     return false;
   }
 
-  if (!SetStringProperty(aCx, objPath, "macLocalApplicationsDir", gPaths->macLocalApplicationsDir)) {
+  if (!SetStringProperty(aCx, objPath, "macLocalApplicationsDir", mPaths->macLocalApplicationsDir)) {
     return false;
   }
 
-  if (!SetStringProperty(aCx, objPath, "macTrashDir", gPaths->macTrashDir)) {
+  if (!SetStringProperty(aCx, objPath, "macTrashDir", mPaths->macTrashDir)) {
     return false;
   }
 #endif // defined(XP_MACOSX)
 
   // sqlite3 is linked from different places depending on the platform
   nsAutoString libsqlite3;
 #if defined(ANDROID)
   // On Android, we use the system's libsqlite3
@@ -1068,28 +1056,24 @@ OSFileConstantsService::GetOrCreate()
   }
 
   RefPtr<OSFileConstantsService> copy = gInstance;
   return copy.forget();
 }
 
 OSFileConstantsService::OSFileConstantsService()
   : mInitialized(false)
+  , mUserUmask(0)
 {
   MOZ_ASSERT(NS_IsMainThread());
 }
 
 OSFileConstantsService::~OSFileConstantsService()
 {
   MOZ_ASSERT(NS_IsMainThread());
-
-  if (mInitialized) {
-    delete gPaths;
-    gPaths = nullptr;
-  }
 }
 
 NS_IMETHODIMP
 OSFileConstantsService::Init(JSContext *aCx)
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   nsresult rv = InitOSFileConstants();
diff --git a/dom/system/OSFileConstants.h b/dom/system/OSFileConstants.h
--- a/dom/system/OSFileConstants.h
+++ b/dom/system/OSFileConstants.h
@@ -37,13 +37,23 @@ private:
 
   nsresult
   InitOSFileConstants();
 
   OSFileConstantsService();
   ~OSFileConstantsService();
 
   bool mInitialized;
+
+  struct Paths;
+  nsAutoPtr<Paths> mPaths;
+
+  /**
+   * (Unix) the umask, which goes in OS.Constants.Sys but
+   * can only be looked up (via the system-info service)
+   * on the main thread.
+   */
+  uint32_t mUserUmask;
 };
 
 } // namespace mozilla
 
 #endif // mozilla_osfileconstants_h__
