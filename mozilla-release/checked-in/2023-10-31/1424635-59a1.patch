# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1513275296 21600
# Node ID 3870805aea8119df80fc64db4eea8965bc809c69
# Parent  c7509bed46c6385dad1e2e22f7c04e363ed0ddc6
Bug 1424635 - Consider DOCUMENT_NODE a valid web element. r=automatedtester

When looking up the parent of <html> using an XPath parent expression
such as "..", the nodeType of the returned HTMLDocument will be 9
(DOCUMENT_NODE).  <html> is a valid web element and we should be
able to serialise and return it to the user.

It is worth noting that other WebDriver implementations fail this
test because they fail on the nodeType check.

MozReview-Commit-ID: 4FMJEd8B4PZ

diff --git a/testing/marionette/element.js b/testing/marionette/element.js
--- a/testing/marionette/element.js
+++ b/testing/marionette/element.js
@@ -24,16 +24,18 @@ this.EXPORTED_SYMBOLS = [
   "element",
   "WebElement",
 ];
 
 const {
   FIRST_ORDERED_NODE_TYPE,
   ORDERED_NODE_ITERATOR_TYPE,
 } = Ci.nsIDOMXPathResult;
+const ELEMENT_NODE = 1;
+const DOCUMENT_NODE = 9;
 
 const XBLNS = "http://www.mozilla.org/xbl";
 const XULNS = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";
 
 /** XUL elements that support checked property. */
 const XUL_CHECKED_ELS = new Set([
   "button",
   "checkbox",
@@ -1083,17 +1085,17 @@ element.isElement = function(node) {
  *
  * @return {boolean}
  *     True if <var>node</var> is a DOM element, false otherwise.
  */
 element.isDOMElement = function(node) {
   return typeof node == "object" &&
       node !== null &&
       "nodeType" in node &&
-      node.nodeType === node.ELEMENT_NODE &&
+      [ELEMENT_NODE, DOCUMENT_NODE].includes(node.nodeType) &&
       !element.isXULElement(node);
 };
 
 /**
  * Ascertains whether <var>el</var> is a XUL- or XBL element.
  *
  * @param {*} node
  *     Element thought to be a XUL- or XBL element.
