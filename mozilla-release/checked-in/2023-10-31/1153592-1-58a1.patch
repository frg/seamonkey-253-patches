# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1506421556 -7200
#      Tue Sep 26 12:25:56 2017 +0200
# Node ID b4724648181dec232d4308f283df5db7e683afda
# Parent  5f576eb7e781728a65f4d743e524e205100994e0
Bug 1153592 part 1 - Replace redundant checks in CheckCanChangeAttrs with debug asserts. r=evilpie

diff --git a/js/src/vm/Shape.cpp b/js/src/vm/Shape.cpp
--- a/js/src/vm/Shape.cpp
+++ b/js/src/vm/Shape.cpp
@@ -661,39 +661,33 @@ js::ReshapeForAllocKind(JSContext* cx, S
         if (!newShape)
             return nullptr;
     }
 
     return newShape;
 }
 
 /*
- * Check and adjust the new attributes for the shape to make sure that our
- * slot access optimizations are sound. It is responsibility of the callers to
- * enforce all restrictions from ECMA-262 v5 8.12.9 [[DefineOwnProperty]].
+ * Assert some invariants that should hold when changing properties. It's the
+ * responsibility of the callers to ensure these hold.
  */
-static inline bool
-CheckCanChangeAttrs(JSContext* cx, JSObject* obj, Shape* shape, unsigned* attrsp)
+static void
+AssertCanChangeAttrs(Shape* shape, unsigned attrs)
 {
+#ifdef DEBUG
     if (shape->configurable())
-        return true;
+        return;
 
     /* A permanent property must stay permanent. */
-    *attrsp |= JSPROP_PERMANENT;
+    MOZ_ASSERT(attrs & JSPROP_PERMANENT);
 
     /* Reject attempts to remove a slot from the permanent data property. */
-    if (shape->isDataDescriptor() && shape->hasSlot() &&
-        (*attrsp & (JSPROP_GETTER | JSPROP_SETTER | JSPROP_SHARED)))
-    {
-        if (!cx->helperThread())
-            JSObject::reportNotConfigurable(cx, shape->propid());
-        return false;
-    }
-
-    return true;
+    MOZ_ASSERT_IF(shape->isDataDescriptor() && shape->hasSlot(),
+                  !(attrs & (JSPROP_GETTER | JSPROP_SETTER | JSPROP_SHARED)));
+#endif
 }
 
 /* static */ Shape*
 NativeObject::putProperty(JSContext* cx, HandleNativeObject obj, HandleId id,
                           GetterOp getter, SetterOp setter, uint32_t slot, unsigned attrs,
                           unsigned flags)
 {
     MOZ_ASSERT(!JSID_IS_VOID(id));
@@ -737,18 +731,17 @@ NativeObject::putProperty(JSContext* cx,
 
         return addPropertyInternal(cx, obj, id, getter, setter, slot, attrs, flags,
                                    entry, true, keep);
     }
 
     /* Property exists: search must have returned a valid entry. */
     MOZ_ASSERT_IF(entry, !entry->isRemoved());
 
-    if (!CheckCanChangeAttrs(cx, obj, shape, &attrs))
-        return nullptr;
+    AssertCanChangeAttrs(shape, attrs);
 
     /*
      * If the caller wants to allocate a slot, but doesn't care which slot,
      * copy the existing shape's slot into slot so we can match shape, if all
      * other members match.
      */
     bool hadSlot = shape->hasSlot();
     uint32_t oldSlot = shape->maybeSlot();
@@ -879,18 +872,17 @@ NativeObject::changeProperty(JSContext* 
     MOZ_ASSERT_IF(attrs & (JSPROP_GETTER | JSPROP_SETTER), attrs & JSPROP_SHARED);
 
     /* Allow only shared (slotless) => unshared (slotful) transition. */
     MOZ_ASSERT(!((attrs ^ shape->attrs) & JSPROP_SHARED) ||
                !(attrs & JSPROP_SHARED));
 
     MarkTypePropertyNonData(cx, obj, shape->propid());
 
-    if (!CheckCanChangeAttrs(cx, obj, shape, &attrs))
-        return nullptr;
+    AssertCanChangeAttrs(shape, attrs);
 
     if (shape->attrs == attrs && shape->getter() == getter && shape->setter() == setter)
         return shape;
 
     /*
      * Let JSObject::putProperty handle this |overwriting| case, including
      * the conservation of shape->slot (if it's valid). We must not call
      * removeProperty because it will free an allocated shape->slot, and
