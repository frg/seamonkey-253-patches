# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1516132138 -3600
# Node ID 83d6c4007b512d0535759b2ff7eeeb5082c66012
# Parent  0fce1f8d0443616bac7e96c308b6f0284ce8840c
Bug 1430844: Add assertions that would've caught this. r=bz

MozReview-Commit-ID: 1UfhIRn2We2

diff --git a/layout/base/GeckoRestyleManager.cpp b/layout/base/GeckoRestyleManager.cpp
--- a/layout/base/GeckoRestyleManager.cpp
+++ b/layout/base/GeckoRestyleManager.cpp
@@ -557,16 +557,18 @@ GeckoRestyleManager::FinishRebuildAllSty
 }
 
 void
 GeckoRestyleManager::ProcessPendingRestyles()
 {
   NS_PRECONDITION(PresContext()->Document(), "No document?  Pshaw!");
   NS_PRECONDITION(!nsContentUtils::IsSafeToRunScript(),
                   "Missing a script blocker!");
+  MOZ_ASSERT(!PresContext()->HasPendingMediaQueryUpdates(),
+             "Someone forgot to update media queries?");
 
   // First do any queued-up frame creation.  (We should really
   // merge this into the rest of the process, though; see bug 827239.)
   PresContext()->FrameConstructor()->CreateNeededFrames();
 
   // Process non-animation restyles...
   MOZ_ASSERT(!mIsProcessingRestyles,
              "Nesting calls to ProcessPendingRestyles?");
diff --git a/layout/base/ServoRestyleManager.cpp b/layout/base/ServoRestyleManager.cpp
--- a/layout/base/ServoRestyleManager.cpp
+++ b/layout/base/ServoRestyleManager.cpp
@@ -1088,40 +1088,45 @@ ServoRestyleManager::SnapshotFor(Element
   // Now that we have a snapshot, make sure a restyle is triggered.
   aElement->NoteDirtyForServo();
   return *snapshot;
 }
 
 void
 ServoRestyleManager::DoProcessPendingRestyles(ServoTraversalFlags aFlags)
 {
-  MOZ_ASSERT(PresContext()->Document(), "No document?  Pshaw!");
+  nsPresContext* presContext = PresContext();
+
+  MOZ_ASSERT(presContext->Document(), "No document?  Pshaw!");
+  MOZ_ASSERT(!presContext->HasPendingMediaQueryUpdates(),
+             "Someone forgot to update media queries?");
   MOZ_ASSERT(!nsContentUtils::IsSafeToRunScript(), "Missing a script blocker!");
   MOZ_ASSERT(!mInStyleRefresh, "Reentrant call?");
 
-  if (MOZ_UNLIKELY(!PresContext()->PresShell()->DidInitialize())) {
+
+  if (MOZ_UNLIKELY(!presContext->PresShell()->DidInitialize())) {
     // PresShell::FlushPendingNotifications doesn't early-return in the case
-    // where the PreShell hasn't yet been initialized (and therefore we haven't
+    // where the PresShell hasn't yet been initialized (and therefore we haven't
     // yet done the initial style traversal of the DOM tree). We should arguably
     // fix up the callers and assert against this case, but we just detect and
     // handle it for now.
     return;
   }
 
   // Create a AnimationsWithDestroyedFrame during restyling process to
   // stop animations and transitions on elements that have no frame at the end
   // of the restyling process.
   AnimationsWithDestroyedFrame animationsWithDestroyedFrame(this);
 
   ServoStyleSet* styleSet = StyleSet();
-  nsIDocument* doc = PresContext()->Document();
+  nsIDocument* doc = presContext->Document();
 
   // Ensure the refresh driver is active during traversal to avoid mutating
   // mActiveTimer and mMostRecentRefresh time.
-  PresContext()->RefreshDriver()->MostRecentRefresh();
+  presContext->RefreshDriver()->MostRecentRefresh();
 
 
   // Perform the Servo traversal, and the post-traversal if required. We do this
   // in a loop because certain rare paths in the frame constructor (like
   // uninstalling XBL bindings) can trigger additional style validations.
   mInStyleRefresh = true;
   if (mHaveNonAnimationRestyles) {
     ++mAnimationGeneration;
@@ -1135,17 +1140,17 @@ ServoRestyleManager::DoProcessPendingRes
     ClearSnapshots();
 
     nsStyleChangeList currentChanges(StyleBackendType::Servo);
     bool anyStyleChanged = false;
 
     // Recreate style contexts, and queue up change hints (which also handle
     // lazy frame construction).
     {
-      AutoRestyleTimelineMarker marker(mPresContext->GetDocShell(), false);
+      AutoRestyleTimelineMarker marker(presContext->GetDocShell(), false);
       DocumentStyleRootIterator iter(doc->GetServoRestyleRoot());
       while (Element* root = iter.GetNextStyleRoot()) {
         nsTArray<nsIFrame*> wrappersToRestyle;
         ServoRestyleState state(*styleSet, currentChanges, wrappersToRestyle);
         ServoPostTraversalFlags flags = ServoPostTraversalFlags::Empty;
         anyStyleChanged |= ProcessPostTraversal(root, nullptr, state, flags);
       }
     }
@@ -1154,17 +1159,17 @@ ServoRestyleManager::DoProcessPendingRes
 
     // Process the change hints.
     //
     // Unfortunately, the frame constructor can generate new change hints while
     // processing existing ones. We redirect those into a secondary queue and
     // iterate until there's nothing left.
     {
       AutoTimelineMarker marker(
-        mPresContext->GetDocShell(), "StylesApplyChanges");
+        presContext->GetDocShell(), "StylesApplyChanges");
       ReentrantChangeList newChanges;
       mReentrantChanges = &newChanges;
       while (!currentChanges.IsEmpty()) {
         ProcessRestyledFrames(currentChanges);
         MOZ_ASSERT(currentChanges.IsEmpty());
         for (ReentrantChange& change: newChanges)  {
           if (!(change.mHint & nsChangeHint_ReconstructFrame) &&
               !change.mContent->GetPrimaryFrame()) {
diff --git a/layout/base/nsPresContext.h b/layout/base/nsPresContext.h
--- a/layout/base/nsPresContext.h
+++ b/layout/base/nsPresContext.h
@@ -217,16 +217,23 @@ public:
       NS_ASSERTION(!mShell || !mShell->GetDocument() ||
                    mShell->GetDocument() == mDocument,
                    "nsPresContext doesn't have the same document as nsPresShell!");
       return mDocument;
   }
 
   mozilla::StyleSetHandle StyleSet() const { return GetPresShell()->StyleSet(); }
 
+#ifdef DEBUG
+  bool HasPendingMediaQueryUpdates() const
+  {
+    return mPendingMediaFeatureValuesChanged;
+  }
+#endif
+
   nsFrameManager* FrameManager()
     { return PresShell()->FrameManager(); }
 
   nsCSSFrameConstructor* FrameConstructor()
     { return PresShell()->FrameConstructor(); }
 
   mozilla::EffectCompositor* EffectCompositor() { return mEffectCompositor; }
   nsTransitionManager* TransitionManager() { return mTransitionManager; }
diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -1353,16 +1353,18 @@ ServoStyleSet::AppendFontFaceRules(nsTAr
   UpdateStylistIfNeeded();
   Servo_StyleSet_GetFontFaceRules(mRawSet.get(), &aArray);
   return true;
 }
 
 nsCSSCounterStyleRule*
 ServoStyleSet::CounterStyleRuleForName(nsIAtom* aName)
 {
+  // FIXME(emilio): This should probably call UpdateStylistIfNeeded, or
+  // otherwise assert?
   return Servo_StyleSet_GetCounterStyleRule(mRawSet.get(), aName);
 }
 
 already_AddRefed<gfxFontFeatureValueSet>
 ServoStyleSet::BuildFontFeatureValueSet()
 {
   UpdateStylistIfNeeded();
   RefPtr<gfxFontFeatureValueSet> set =
