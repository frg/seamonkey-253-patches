# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1507568620 25200
#      Mon Oct 09 10:03:40 2017 -0700
# Node ID 479b4df3b5007a858c1f03e93e83cf1aeef037d0
# Parent  04d1c1eb0b313168703a11df22b5bf895906aab1
Bug 1391693 P1 Rename nsIInterceptedChannel.cancel() to cancelInterception() r=jdm

diff --git a/docshell/base/nsDocShell.cpp b/docshell/base/nsDocShell.cpp
--- a/docshell/base/nsDocShell.cpp
+++ b/docshell/base/nsDocShell.cpp
@@ -15075,17 +15075,17 @@ nsDocShell::ShouldPrepareForIntercept(ns
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsDocShell::ChannelIntercepted(nsIInterceptedChannel* aChannel)
 {
   RefPtr<ServiceWorkerManager> swm = ServiceWorkerManager::GetInstance();
   if (!swm) {
-    aChannel->Cancel(NS_ERROR_INTERCEPTION_FAILED);
+    aChannel->CancelInterception(NS_ERROR_INTERCEPTION_FAILED);
     return NS_OK;
   }
 
   nsCOMPtr<nsIChannel> channel;
   nsresult rv = aChannel->GetChannel(getter_AddRefs(channel));
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsCOMPtr<nsIDocument> doc;
diff --git a/dom/workers/ServiceWorkerEvents.cpp b/dom/workers/ServiceWorkerEvents.cpp
--- a/dom/workers/ServiceWorkerEvents.cpp
+++ b/dom/workers/ServiceWorkerEvents.cpp
@@ -110,17 +110,17 @@ CancelChannelRunnable::Run()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   // TODO: When bug 1204254 is implemented, this time marker should be moved to
   // the point where the body of the network request is complete.
   mChannel->SetHandleFetchEventEnd(TimeStamp::Now());
   mChannel->SaveTimeStamps();
 
-  mChannel->Cancel(mStatus);
+  mChannel->CancelInterception(mStatus);
   mRegistration->MaybeScheduleUpdate();
   return NS_OK;
 }
 
 FetchEvent::FetchEvent(EventTarget* aOwner)
   : ExtendableEvent(aOwner)
   , mPreventDefaultLineNumber(0)
   , mPreventDefaultColumnNumber(0)
@@ -194,53 +194,53 @@ public:
 
     nsCOMPtr<nsIChannel> underlyingChannel;
     nsresult rv = mChannel->GetChannel(getter_AddRefs(underlyingChannel));
     NS_ENSURE_SUCCESS(rv, rv);
     NS_ENSURE_TRUE(underlyingChannel, NS_ERROR_UNEXPECTED);
     nsCOMPtr<nsILoadInfo> loadInfo = underlyingChannel->GetLoadInfo();
 
     if (!loadInfo || !CSPPermitsResponse(loadInfo)) {
-      mChannel->Cancel(NS_ERROR_CONTENT_BLOCKED);
+      mChannel->CancelInterception(NS_ERROR_CONTENT_BLOCKED);
       return NS_OK;
     }
 
     ChannelInfo channelInfo;
     if (mInternalResponse->GetChannelInfo().IsInitialized()) {
       channelInfo = mInternalResponse->GetChannelInfo();
     } else {
       // We are dealing with a synthesized response here, so fall back to the
       // channel info for the worker script.
       channelInfo = mWorkerChannelInfo;
     }
     rv = mChannel->SetChannelInfo(&channelInfo);
     if (NS_WARN_IF(NS_FAILED(rv))) {
-      mChannel->Cancel(NS_ERROR_INTERCEPTION_FAILED);
+      mChannel->CancelInterception(NS_ERROR_INTERCEPTION_FAILED);
       return NS_OK;
     }
 
     rv = mChannel->SynthesizeStatus(mInternalResponse->GetUnfilteredStatus(),
                                     mInternalResponse->GetUnfilteredStatusText());
     if (NS_WARN_IF(NS_FAILED(rv))) {
-      mChannel->Cancel(NS_ERROR_INTERCEPTION_FAILED);
+      mChannel->CancelInterception(NS_ERROR_INTERCEPTION_FAILED);
       return NS_OK;
     }
 
     AutoTArray<InternalHeaders::Entry, 5> entries;
     mInternalResponse->UnfilteredHeaders()->GetEntries(entries);
     for (uint32_t i = 0; i < entries.Length(); ++i) {
        mChannel->SynthesizeHeader(entries[i].mName, entries[i].mValue);
     }
 
     auto castLoadInfo = static_cast<LoadInfo*>(loadInfo.get());
     castLoadInfo->SynthesizeServiceWorkerTainting(mInternalResponse->GetTainting());
 
     rv = mChannel->FinishSynthesizedResponse(mResponseURLSpec);
     if (NS_WARN_IF(NS_FAILED(rv))) {
-      mChannel->Cancel(NS_ERROR_INTERCEPTION_FAILED);
+      mChannel->CancelInterception(NS_ERROR_INTERCEPTION_FAILED);
       return NS_OK;
     }
 
     TimeStamp timeStamp = TimeStamp::Now();
     mChannel->SetHandleFetchEventEnd(timeStamp);
     mChannel->SetFinishSynthesizedResponseEnd(timeStamp);
     mChannel->SaveTimeStamps();
 
diff --git a/netwerk/base/nsINetworkInterceptController.idl b/netwerk/base/nsINetworkInterceptController.idl
--- a/netwerk/base/nsINetworkInterceptController.idl
+++ b/netwerk/base/nsINetworkInterceptController.idl
@@ -63,17 +63,17 @@ interface nsIInterceptedChannel : nsISup
      */
     void finishSynthesizedResponse(in ACString finalURLSpec);
 
     /**
      * Cancel the pending intercepted request.
      * @return NS_ERROR_FAILURE if the response has already been synthesized or
      *         the original request has been instructed to continue.
      */
-    void cancel(in nsresult status);
+    void cancelInterception(in nsresult status);
 
     /**
      * The synthesized response body to be produced.
      */
     readonly attribute nsIOutputStream responseBody;
 
     /**
      * The underlying channel object that was intercepted.
diff --git a/netwerk/protocol/http/HttpChannelParentListener.cpp b/netwerk/protocol/http/HttpChannelParentListener.cpp
--- a/netwerk/protocol/http/HttpChannelParentListener.cpp
+++ b/netwerk/protocol/http/HttpChannelParentListener.cpp
@@ -405,15 +405,15 @@ HttpChannelParentListener::SetupIntercep
     mShouldSuspendIntercept = true;
   }
 }
 
 void
 HttpChannelParentListener::ClearInterceptedChannel()
 {
   if (mInterceptedChannel) {
-    mInterceptedChannel->Cancel(NS_ERROR_INTERCEPTION_FAILED);
+    mInterceptedChannel->CancelInterception(NS_ERROR_INTERCEPTION_FAILED);
     mInterceptedChannel = nullptr;
   }
 }
 
 } // namespace net
 } // namespace mozilla
diff --git a/netwerk/protocol/http/InterceptedChannel.cpp b/netwerk/protocol/http/InterceptedChannel.cpp
--- a/netwerk/protocol/http/InterceptedChannel.cpp
+++ b/netwerk/protocol/http/InterceptedChannel.cpp
@@ -352,17 +352,17 @@ InterceptedChannelChrome::FinishSynthesi
   }
 
   mClosed = true;
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
-InterceptedChannelChrome::Cancel(nsresult aStatus)
+InterceptedChannelChrome::CancelInterception(nsresult aStatus)
 {
   MOZ_ASSERT(NS_FAILED(aStatus));
 
   if (mClosed) {
     return NS_ERROR_FAILURE;
   }
 
   mReportCollector->FlushConsoleReports(mChannel);
@@ -520,17 +520,17 @@ InterceptedChannelContent::FinishSynthes
   mResponseBody = nullptr;
   mStreamListener = nullptr;
   mClosed = true;
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
-InterceptedChannelContent::Cancel(nsresult aStatus)
+InterceptedChannelContent::CancelInterception(nsresult aStatus)
 {
   MOZ_ASSERT(NS_FAILED(aStatus));
 
   if (mClosed) {
     return NS_ERROR_FAILURE;
   }
 
   mReportCollector->FlushConsoleReports(mChannel);
diff --git a/netwerk/protocol/http/InterceptedChannel.h b/netwerk/protocol/http/InterceptedChannel.h
--- a/netwerk/protocol/http/InterceptedChannel.h
+++ b/netwerk/protocol/http/InterceptedChannel.h
@@ -185,17 +185,17 @@ public:
                            nsICacheEntry* aEntry);
 
   NS_IMETHOD ResetInterception() override;
   NS_IMETHOD FinishSynthesizedResponse(const nsACString& aFinalURLSpec) override;
   NS_IMETHOD GetChannel(nsIChannel** aChannel) override;
   NS_IMETHOD GetSecureUpgradedChannelURI(nsIURI** aURI) override;
   NS_IMETHOD SynthesizeStatus(uint16_t aStatus, const nsACString& aReason) override;
   NS_IMETHOD SynthesizeHeader(const nsACString& aName, const nsACString& aValue) override;
-  NS_IMETHOD Cancel(nsresult aStatus) override;
+  NS_IMETHOD CancelInterception(nsresult aStatus) override;
   NS_IMETHOD SetChannelInfo(mozilla::dom::ChannelInfo* aChannelInfo) override;
   NS_IMETHOD GetInternalContentPolicyType(nsContentPolicyType *aInternalContentPolicyType) override;
 
   virtual void NotifyController() override;
 };
 
 class InterceptedChannelContent : public InterceptedChannelBase
 {
@@ -218,17 +218,17 @@ public:
                             bool aSecureUpgrade);
 
   NS_IMETHOD ResetInterception() override;
   NS_IMETHOD FinishSynthesizedResponse(const nsACString& aFinalURLSpec) override;
   NS_IMETHOD GetChannel(nsIChannel** aChannel) override;
   NS_IMETHOD GetSecureUpgradedChannelURI(nsIURI** aURI) override;
   NS_IMETHOD SynthesizeStatus(uint16_t aStatus, const nsACString& aReason) override;
   NS_IMETHOD SynthesizeHeader(const nsACString& aName, const nsACString& aValue) override;
-  NS_IMETHOD Cancel(nsresult aStatus) override;
+  NS_IMETHOD CancelInterception(nsresult aStatus) override;
   NS_IMETHOD SetChannelInfo(mozilla::dom::ChannelInfo* aChannelInfo) override;
   NS_IMETHOD GetInternalContentPolicyType(nsContentPolicyType *aInternalContentPolicyType) override;
 
   virtual void NotifyController() override;
 };
 
 } // namespace net
 } // namespace mozilla
diff --git a/netwerk/test/unit/test_synthesized_response.js b/netwerk/test/unit/test_synthesized_response.js
--- a/netwerk/test/unit/test_synthesized_response.js
+++ b/netwerk/test/unit/test_synthesized_response.js
@@ -178,29 +178,29 @@ add_test(function() {
   });
   chan.asyncOpen2(new ChannelListener(handle_synthesized_response, null,
 				     CL_ALLOW_UNKNOWN_CL | CL_SUSPEND | CL_EXPECT_3S_DELAY));
 });
 
 // ensure that the intercepted channel can be cancelled
 add_test(function() {
   var chan = make_channel(URL + '/body', null, function(intercepted) {
-    intercepted.cancel(Cr.NS_BINDING_ABORTED);
+    intercepted.cancelInterception(Cr.NS_BINDING_ABORTED);
   });
   chan.asyncOpen2(new ChannelListener(run_next_test, null, CL_EXPECT_FAILURE));
 });
 
 // ensure that the channel can't be cancelled via nsIInterceptedChannel after making a decision
 add_test(function() {
   var chan = make_channel(URL + '/body', null, function(chan) {
     chan.resetInterception();
     do_timeout(0, function() {
       var gotexception = false;
       try {
-        chan.cancel();
+        chan.cancelInterception();
       } catch (x) {
         gotexception = true;
       }
       Assert.ok(gotexception);
     });
   });
   chan.asyncOpen2(new ChannelListener(handle_remote_response, null));
 });
