# HG changeset patch
# User Cameron McCormack <cam@mcc.id.au>
# Date 1512444170 21600
# Node ID e6eb76a8470df54958f1905f253d0ce3cdc86cd6
# Parent  cef30ce07f52dc2f2103f4e37ce7ec304a8dfb35
servo: Merge #19489 - add FFI functions for Gecko @counter-style value parsing (from heycam:counter-parse-2); r=upsuper

Trying to land #19441 again.

Source-Repo: https://github.com/servo/servo
Source-Revision: 5bfab782ec862189209931e424fbd4325b8f9172

diff --git a/servo/components/style/counter_style/mod.rs b/servo/components/style/counter_style/mod.rs
--- a/servo/components/style/counter_style/mod.rs
+++ b/servo/components/style/counter_style/mod.rs
@@ -6,29 +6,33 @@
 //!
 //! [counter-style]: https://drafts.csswg.org/css-counter-styles/
 
 use Atom;
 use cssparser::{AtRuleParser, DeclarationListParser, DeclarationParser};
 use cssparser::{Parser, Token, serialize_identifier, CowRcStr};
 use error_reporting::{ContextualParseError, ParseErrorReporter};
 #[cfg(feature = "gecko")] use gecko::rules::CounterStyleDescriptors;
-#[cfg(feature = "gecko")] use gecko_bindings::structs::nsCSSCounterDesc;
+#[cfg(feature = "gecko")] use gecko_bindings::structs::{ nsCSSCounterDesc, nsCSSValue };
 use parser::{ParserContext, ParserErrorContext, Parse};
 use selectors::parser::SelectorParseErrorKind;
 use shared_lock::{SharedRwLockReadGuard, ToCssWithGuard};
 #[allow(unused_imports)] use std::ascii::AsciiExt;
 use std::borrow::Cow;
 use std::fmt;
 use std::ops::Range;
 use style_traits::{Comma, OneOrMoreSeparated, ParseError, StyleParseErrorKind, ToCss};
 use values::CustomIdent;
 
-/// Parse the prelude of an @counter-style rule
-pub fn parse_counter_style_name<'i, 't>(input: &mut Parser<'i, 't>) -> Result<CustomIdent, ParseError<'i>> {
+/// Parse a counter style name reference.
+///
+/// This allows the reserved counter style names "decimal" and "disc".
+pub fn parse_counter_style_name<'i, 't>(
+    input: &mut Parser<'i, 't>
+) -> Result<CustomIdent, ParseError<'i>> {
     macro_rules! predefined {
         ($($name: expr,)+) => {
             {
                 ascii_case_insensitive_phf_map! {
                     // FIXME: use static atoms https://github.com/rust-lang/rust/issues/33156
                     predefined -> &'static str = {
                         $(
                             $name => $name,
@@ -36,25 +40,39 @@ pub fn parse_counter_style_name<'i, 't>(
                     }
                 }
 
                 let location = input.current_source_location();
                 let ident = input.expect_ident()?;
                 if let Some(&lower_cased) = predefined(&ident) {
                     Ok(CustomIdent(Atom::from(lower_cased)))
                 } else {
-                    // https://github.com/w3c/csswg-drafts/issues/1295 excludes "none"
+                    // none is always an invalid <counter-style> value.
                     CustomIdent::from_ident(location, ident, &["none"])
                 }
             }
         }
     }
     include!("predefined.rs")
 }
 
+/// Parse the prelude of an @counter-style rule
+pub fn parse_counter_style_name_definition<'i, 't>(
+    input: &mut Parser<'i, 't>
+) -> Result<CustomIdent, ParseError<'i>> {
+    parse_counter_style_name(input)
+        .and_then(|ident| {
+            if ident.0 == atom!("decimal") || ident.0 == atom!("disc") {
+                Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
+            } else {
+                Ok(ident)
+            }
+        })
+}
+
 /// Parse the body (inside `{}`) of an @counter-style rule
 pub fn parse_counter_style_body<'i, 't, R>(name: CustomIdent,
                                            context: &ParserContext,
                                            error_context: &ParserErrorContext<R>,
                                            input: &mut Parser<'i, 't>)
                                            -> Result<CounterStyleRuleData, ParseError<'i>>
     where R: ParseErrorReporter
 {
@@ -220,16 +238,40 @@ macro_rules! counter_style_descriptors {
                         dest.write_str(concat!("  ", $name, ": "))?;
                         ToCss::to_css(value, dest)?;
                         dest.write_str(";\n")?;
                     }
                 )+
                 dest.write_str("}")
             }
         }
+
+        /// Parse a descriptor into an `nsCSSValue`.
+        #[cfg(feature = "gecko")]
+        pub fn parse_counter_style_descriptor<'i, 't>(
+            context: &ParserContext,
+            input: &mut Parser<'i, 't>,
+            descriptor: nsCSSCounterDesc,
+            value: &mut nsCSSValue
+        ) -> Result<(), ParseError<'i>> {
+            match descriptor {
+                $(
+                    nsCSSCounterDesc::$gecko_ident => {
+                        let v: $ty =
+                            input.parse_entirely(|i| Parse::parse(context, i))?;
+                        value.set_from(v);
+                    }
+                )*
+                nsCSSCounterDesc::eCSSCounterDesc_COUNT |
+                nsCSSCounterDesc::eCSSCounterDesc_UNKNOWN => {
+                    panic!("invalid counter descriptor");
+                }
+            }
+            Ok(())
+        }
     }
 }
 
 counter_style_descriptors! {
     /// <https://drafts.csswg.org/css-counter-styles/#counter-style-system>
     "system" system / eCSSCounterDesc_System: System = {
         System::Symbolic
     }
diff --git a/servo/components/style/stylesheets/rule_parser.rs b/servo/components/style/stylesheets/rule_parser.rs
--- a/servo/components/style/stylesheets/rule_parser.rs
+++ b/servo/components/style/stylesheets/rule_parser.rs
@@ -1,16 +1,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Parsing of the stylesheet contents.
 
 use {Namespace, Prefix};
-use counter_style::{parse_counter_style_body, parse_counter_style_name};
+use counter_style::{parse_counter_style_body, parse_counter_style_name_definition};
 use cssparser::{AtRuleParser, AtRuleType, Parser, QualifiedRuleParser, RuleListParser};
 use cssparser::{CowRcStr, SourceLocation, BasicParseError, BasicParseErrorKind};
 use error_reporting::{ContextualParseError, ParseErrorReporter};
 use font_face::parse_font_face_block;
 use media_queries::{parse_media_query_list, MediaList};
 use parser::{Parse, ParserContext, ParserErrorContext};
 use properties::parse_property_declaration_list;
 use selector_parser::{SelectorImpl, SelectorParser};
@@ -378,23 +378,17 @@ impl<'a, 'b, 'i, R: ParseErrorReporter> 
                 let family_names = parse_family_name_list(self.context, input)?;
                 Ok(AtRuleType::WithBlock(AtRuleBlockPrelude::FontFeatureValues(family_names, location)))
             },
             "counter-style" => {
                 if !cfg!(feature = "gecko") {
                     // Support for this rule is not fully implemented in Servo yet.
                     return Err(input.new_custom_error(StyleParseErrorKind::UnsupportedAtRule(name.clone())))
                 }
-                let name = parse_counter_style_name(input)?;
-                // ASCII-case-insensitive matches for "decimal" and "disc".
-                // The name is already lower-cased by `parse_counter_style_name`
-                // so we can use == here.
-                if name.0 == atom!("decimal") || name.0 == atom!("disc") {
-                    return Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
-                }
+                let name = parse_counter_style_name_definition(input)?;
                 Ok(AtRuleType::WithBlock(AtRuleBlockPrelude::CounterStyle(name)))
             },
             "viewport" => {
                 if viewport_rule::enabled() {
                     Ok(AtRuleType::WithBlock(AtRuleBlockPrelude::Viewport))
                 } else {
                     Err(input.new_custom_error(StyleParseErrorKind::UnsupportedAtRule(name.clone())))
                 }
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -13,16 +13,17 @@ use std::cell::RefCell;
 use std::env;
 use std::fmt::Write;
 use std::iter;
 use std::mem;
 use std::ptr;
 use style::applicable_declarations::ApplicableDeclarationBlock;
 use style::context::{CascadeInputs, QuirksMode, SharedStyleContext, StyleContext};
 use style::context::ThreadLocalStyleContext;
+use style::counter_style;
 use style::data::{ElementStyles, self};
 use style::dom::{ShowSubtreeData, TDocument, TElement, TNode};
 use style::driver;
 use style::element_state::{DocumentState, ElementState};
 use style::error_reporting::{NullReporter, ParseErrorReporter};
 use style::font_metrics::{FontMetricsProvider, get_metrics_provider_for_product};
 use style::gecko::data::{GeckoStyleSheet, PerDocumentStyleData, PerDocumentStyleDataImpl};
 use style::gecko::global_style_data::{GLOBAL_STYLE_DATA, GlobalStyleData, STYLE_THREAD_POOL};
@@ -99,16 +100,18 @@ use style::gecko_bindings::structs::RawS
 use style::gecko_bindings::structs::RawServoSourceSizeList;
 use style::gecko_bindings::structs::SeenPtrs;
 use style::gecko_bindings::structs::ServoElementSnapshotTable;
 use style::gecko_bindings::structs::ServoStyleSetSizes;
 use style::gecko_bindings::structs::ServoTraversalFlags;
 use style::gecko_bindings::structs::StyleRuleInclusion;
 use style::gecko_bindings::structs::URLExtraData;
 use style::gecko_bindings::structs::gfxFontFeatureValueSet;
+use style::gecko_bindings::structs::nsCSSCounterDesc;
+use style::gecko_bindings::structs::nsCSSValue;
 use style::gecko_bindings::structs::nsCSSValueSharedList;
 use style::gecko_bindings::structs::nsCompatibility;
 use style::gecko_bindings::structs::nsIDocument;
 use style::gecko_bindings::structs::nsStyleTransformMatrix::MatrixTransformOperator;
 use style::gecko_bindings::structs::nsTArray;
 use style::gecko_bindings::structs::nsresult;
 use style::gecko_bindings::sugar::ownership::{FFIArcHelpers, HasFFI, HasArcFFI};
 use style::gecko_bindings::sugar::ownership::{HasSimpleFFI, Strong};
@@ -4730,8 +4733,54 @@ pub unsafe extern "C" fn Servo_SourceSiz
 
     result.0
 }
 
 #[no_mangle]
 pub unsafe extern "C" fn Servo_SourceSizeList_Drop(list: RawServoSourceSizeListOwned) {
     let _ = list.into_box::<SourceSizeList>();
 }
+
+#[no_mangle]
+pub extern "C" fn Servo_ParseCounterStyleName(
+    value: *const nsACString,
+) -> *mut nsIAtom {
+    let value = unsafe { value.as_ref().unwrap().as_str_unchecked() };
+    let mut input = ParserInput::new(&value);
+    let mut parser = Parser::new(&mut input);
+    match parser.parse_entirely(counter_style::parse_counter_style_name_definition) {
+        Ok(name) => name.0.into_addrefed(),
+        Err(_) => ptr::null_mut(),
+    }
+}
+
+#[no_mangle]
+pub extern "C" fn Servo_ParseCounterStyleDescriptor(
+    descriptor: nsCSSCounterDesc,
+    value: *const nsACString,
+    raw_extra_data: *mut URLExtraData,
+    result: *mut nsCSSValue,
+) -> bool {
+    let value = unsafe { value.as_ref().unwrap().as_str_unchecked() };
+    let url_data = unsafe {
+        if raw_extra_data.is_null() {
+            dummy_url_data()
+        } else {
+            RefPtr::from_ptr_ref(&raw_extra_data)
+        }
+    };
+    let result = unsafe { result.as_mut().unwrap() };
+    let mut input = ParserInput::new(&value);
+    let mut parser = Parser::new(&mut input);
+    let context = ParserContext::new(
+        Origin::Author,
+        url_data,
+        Some(CssRuleType::CounterStyle),
+        ParsingMode::DEFAULT,
+        QuirksMode::NoQuirks,
+    );
+    counter_style::parse_counter_style_descriptor(
+        &context,
+        &mut parser,
+        descriptor,
+        result,
+    ).is_ok()
+}
