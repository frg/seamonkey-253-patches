# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1510451004 28800
# Node ID 9bbdf1312345f94113ad58e9ddd59dbd841780ee
# Parent  bba98c0c15780f17fe486b0fb1429a6aed45ec98
Bug 1384956 - Get log level from main process when script is reloaded. r=whimboo

This is a workaround for https://bugzil.la/1411513 about Log.jsm
not being compatible with E10s.  It queries the log level via
synchronous IPC message to the main process.

MozReview-Commit-ID: 5RZluH8Rv9o

diff --git a/testing/marionette/driver.js b/testing/marionette/driver.js
--- a/testing/marionette/driver.js
+++ b/testing/marionette/driver.js
@@ -3491,16 +3491,19 @@ GeckoDriver.prototype.receiveMessage = f
       if (message.json.listenerId === this.curBrowser.curFrameId) {
         // If the frame script gets reloaded we need to call newSession.
         // In the case of desktop this just sets up a small amount of state
         // that doesn't change over the course of a session.
         this.sendAsync("newSession", this.capabilities);
         this.curBrowser.flushPendingCommands();
       }
       break;
+
+    case "Marionette:GetLogLevel":
+      return logger.level;
   }
 };
 /* eslint-enable consistent-return */
 
 GeckoDriver.prototype.responseCompleted = function() {
   if (this.curBrowser !== null) {
     this.curBrowser.pendingCommands = [];
   }
diff --git a/testing/marionette/frame.js b/testing/marionette/frame.js
--- a/testing/marionette/frame.js
+++ b/testing/marionette/frame.js
@@ -214,16 +214,17 @@ frame.Manager = class {
     mm.addWeakMessageListener("Marionette:emitTouchEvent", this.driver);
     mm.addWeakMessageListener("Marionette:log", this.driver);
     mm.addWeakMessageListener("Marionette:shareData", this.driver);
     mm.addWeakMessageListener("Marionette:switchToModalOrigin", this.driver);
     mm.addWeakMessageListener("Marionette:switchedToFrame", this.driver);
     mm.addWeakMessageListener("Marionette:getVisibleCookies", this.driver);
     mm.addWeakMessageListener("Marionette:register", this.driver);
     mm.addWeakMessageListener("Marionette:listenersAttached", this.driver);
+    mm.addWeakMessageListener("Marionette:GetLogLevel", this.driver);
     mm.addWeakMessageListener("MarionetteFrame:handleModal", this);
     mm.addWeakMessageListener("MarionetteFrame:getCurrentFrameId", this);
     mm.addWeakMessageListener("MarionetteFrame:getInterruptedState", this);
   }
 
   /**
    * Removes listeners for messages from content frame scripts.
    * We do not remove the MarionetteFrame:getInterruptedState or
@@ -241,16 +242,17 @@ frame.Manager = class {
     mm.removeWeakMessageListener("Marionette:done", this.driver);
     mm.removeWeakMessageListener("Marionette:error", this.driver);
     mm.removeWeakMessageListener("Marionette:log", this.driver);
     mm.removeWeakMessageListener("Marionette:shareData", this.driver);
     mm.removeWeakMessageListener("Marionette:switchedToFrame", this.driver);
     mm.removeWeakMessageListener("Marionette:getVisibleCookies", this.driver);
     mm.removeWeakMessageListener(
         "Marionette:getImportedScripts", this.driver.importedScripts);
+    mm.removeWeakMessageListener("Marionette:GetLogLevel", this.driver);
     mm.removeWeakMessageListener("Marionette:listenersAttached", this.driver);
     mm.removeWeakMessageListener("Marionette:register", this.driver);
     mm.removeWeakMessageListener("MarionetteFrame:handleModal", this);
     mm.removeWeakMessageListener("MarionetteFrame:getCurrentFrameId", this);
   }
 };
 
 frame.Manager.prototype.QueryInterface = XPCOMUtils.generateQI(
diff --git a/testing/marionette/listener.js b/testing/marionette/listener.js
--- a/testing/marionette/listener.js
+++ b/testing/marionette/listener.js
@@ -65,19 +65,21 @@ const SUPPORTED_STRATEGIES = new Set([
 
 let capabilities;
 
 let legacyactions = new legacyaction.Chain(checkForInterrupted);
 
 // last touch for each fingerId
 let multiLast = {};
 
+// TODO: Log.jsm is not e10s compatible (see https://bugzil.la/1411513),
+// query the main process for the current log level
 const logger = Log.repository.getLogger("Marionette");
-// Append only once to avoid duplicated output after listener.js gets reloaded
 if (logger.ownAppenders.length == 0) {
+  logger.level = sendSyncMessage("Marionette:GetLogLevel");
   logger.addAppender(new Log.DumpAppender());
 }
 
 // sandbox storage and name of the current sandbox
 const sandboxes = new Sandboxes(() => curContainer.frame);
 
 const eventObservers = new ContentEventObserverService(
     content, sendAsyncMessage.bind(this));
