# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1512784778 21600
# Node ID ac0f09782e8b2c6eb66df710040d59b40d970865
# Parent  fbe88736030a42bead8cf226b2beeb10e3b97b86
servo: Merge #19530 - style: Don't unconditionally extend() the declaration block vector (from emilio:extend-not-much); r=jdm

Since it appears in profiles when used from CSSOM, like the one in the bug
mentioned in the comment.

Source-Repo: https://github.com/servo/servo
Source-Revision: f8a87a76b2c940fdcf1ced230c616b5312e97402

diff --git a/servo/components/style/properties/declaration_block.rs b/servo/components/style/properties/declaration_block.rs
--- a/servo/components/style/properties/declaration_block.rs
+++ b/servo/components/style/properties/declaration_block.rs
@@ -394,25 +394,38 @@ impl PropertyDeclarationBlock {
     /// See the documentation of `push` to see what impact `source` has when the
     /// property is already there.
     pub fn extend(
         &mut self,
         mut drain: SourcePropertyDeclarationDrain,
         importance: Importance,
         source: DeclarationSource,
     ) -> bool {
-        let all_shorthand_len = match drain.all_shorthand {
-            AllShorthand::NotSet => 0,
-            AllShorthand::CSSWideKeyword(_) |
-            AllShorthand::WithVariables(_) => ShorthandId::All.longhands().len()
-        };
-        let push_calls_count = drain.declarations.len() + all_shorthand_len;
+        match source {
+            DeclarationSource::Parsing => {
+                let all_shorthand_len = match drain.all_shorthand {
+                    AllShorthand::NotSet => 0,
+                    AllShorthand::CSSWideKeyword(_) |
+                    AllShorthand::WithVariables(_) => ShorthandId::All.longhands().len()
+                };
+                let push_calls_count =
+                    drain.declarations.len() + all_shorthand_len;
 
-        // With deduplication the actual length increase may be less than this.
-        self.declarations.reserve(push_calls_count);
+                // With deduplication the actual length increase may be less than this.
+                self.declarations.reserve(push_calls_count);
+            }
+            DeclarationSource::CssOm => {
+                // Don't bother reserving space, since it's usually the case
+                // that CSSOM just overrides properties and we don't need to use
+                // more memory. See bug 1424346 for an example where this
+                // matters.
+                //
+                // TODO: Would it be worth to call reserve() just if it's empty?
+            }
+        }
 
         let mut changed = false;
         for decl in &mut drain.declarations {
             changed |= self.push(
                 decl,
                 importance,
                 source,
             );
