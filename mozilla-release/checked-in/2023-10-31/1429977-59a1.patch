# HG changeset patch
# User Jeremy Chen <jeremychen@mozilla.com>
# Date 1515725909 0
# Node ID d97273fcb62f01f5bf9743d441ea0f625e774ff5
# Parent  40161b05e29e5561520b8d468c2ccd01b3e1ebd9
Bug 1429977 - make comments for BreakAndMeasureText up-to-date (comment-only, DONTBUILD). r=jfkthame

MozReview-Commit-ID: ILWAnMT9Fdp

diff --git a/gfx/thebes/gfxTextRun.h b/gfx/thebes/gfxTextRun.h
--- a/gfx/thebes/gfxTextRun.h
+++ b/gfx/thebes/gfxTextRun.h
@@ -362,58 +362,60 @@ public:
     };
 
     void ClassifyAutoHyphenations(uint32_t aStart, Range aRange,
                                   nsTArray<HyphenType>& aHyphenBuffer,
                                   HyphenationState* aWordState);
 
     /**
      * Finds the longest substring that will fit into the given width.
-     * Uses GetHyphenationBreaks and GetSpacing from aBreakProvider.
+     * Uses GetHyphenationBreaks and GetSpacing from aProvider.
      * Guarantees the following:
      * -- 0 <= result <= aMaxLength
      * -- result is the maximal value of N such that either
-     *       N < aMaxLength && line break at N && GetAdvanceWidth(aStart, N) <= aWidth
-     *   OR  N < aMaxLength && hyphen break at N && GetAdvanceWidth(aStart, N) + GetHyphenWidth() <= aWidth
-     *   OR  N == aMaxLength && GetAdvanceWidth(aStart, N) <= aWidth
+     *       N < aMaxLength && line break at N && GetAdvanceWidth(Range(aStart, N), aProvider) <= aWidth
+     *   OR  N < aMaxLength && hyphen break at N && GetAdvanceWidth(Range(aStart, N), aProvider) + GetHyphenWidth() <= aWidth
+     *   OR  N == aMaxLength && GetAdvanceWidth(Range(aStart, N), aProvider) <= aWidth
      * where GetAdvanceWidth assumes the effect of
-     * SetLineBreaks(aStart, N, aLineBreakBefore, N < aMaxLength, aProvider)
+     * SetLineBreaks(Range(aStart, N), aLineBreakBefore, N < aMaxLength, aProvider)
      * -- if no such N exists, then result is the smallest N such that
      *       N < aMaxLength && line break at N
      *   OR  N < aMaxLength && hyphen break at N
      *   OR  N == aMaxLength
      *
      * The call has the effect of
-     * SetLineBreaks(aStart, result, aLineBreakBefore, result < aMaxLength, aProvider)
+     * SetLineBreaks(Range(aStart, result), aLineBreakBefore, result < aMaxLength, aProvider)
      * and the returned metrics and the invariants above reflect this.
      *
      * @param aMaxLength this can be UINT32_MAX, in which case the length used
      * is up to the end of the string
      * @param aLineBreakBefore set to true if and only if there is an actual
      * line break at the start of this string.
      * @param aSuppressBreak what break should be suppressed.
      * @param aTrimWhitespace if non-null, then we allow a trailing run of
      * spaces to be trimmed; the width of the space(s) will not be included in
      * the measured string width for comparison with the limit aWidth, and
      * trimmed spaces will not be included in returned metrics. The width
      * of the trimmed spaces will be returned in aTrimWhitespace.
      * Trimmed spaces are still counted in the "characters fit" result.
+     * @param aHangWhitespace true if we allow whitespace to overflow the
+     * container at a soft-wrap
      * @param aMetrics if non-null, we fill this in for the returned substring.
      * If a hyphenation break was used, the hyphen is NOT included in the returned metrics.
      * @param aBoundingBoxType whether to make the bounding box in aMetrics tight
      * @param aDrawTargetForTightBoundingbox a reference DrawTarget to get the
      * tight bounding box, if requested
      * @param aUsedHyphenation if non-null, records if we selected a hyphenation break
      * @param aLastBreak if non-null and result is aMaxLength, we set this to
      * the maximal N such that
-     *       N < aMaxLength && line break at N && GetAdvanceWidth(aStart, N) <= aWidth
-     *   OR  N < aMaxLength && hyphen break at N && GetAdvanceWidth(aStart, N) + GetHyphenWidth() <= aWidth
+     *       N < aMaxLength && line break at N && GetAdvanceWidth(Range(aStart, N), aProvider) <= aWidth
+     *   OR  N < aMaxLength && hyphen break at N && GetAdvanceWidth(Range(aStart, N), aProvider) + GetHyphenWidth() <= aWidth
      * or UINT32_MAX if no such N exists, where GetAdvanceWidth assumes
      * the effect of
-     * SetLineBreaks(aStart, N, aLineBreakBefore, N < aMaxLength, aProvider)
+     * SetLineBreaks(Range(aStart, N), aLineBreakBefore, N < aMaxLength, aProvider)
      *
      * @param aCanWordWrap true if we can break between any two grapheme
      * clusters. This is set by overflow-wrap|word-wrap: break-word
      *
      * @param aBreakPriority in/out the priority of the break opportunity
      * saved in the line. If we are prioritizing break opportunities, we will
      * not set a break with a lower priority. @see gfxBreakPriority.
      *

