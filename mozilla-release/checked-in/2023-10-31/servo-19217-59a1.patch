# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1510666048 21600
# Node ID 62b26140b19e391fba7fcf9657312744341ce196
# Parent  6e932f074b0ed010e2c7309c35c226c2d0994195
servo: Merge #19217 - style: Remove mozmm CSS unit (from emilio:bye-mozmm); r=heycam

Bug: 1416564
Reviewed-by: heycam
MozReview-Commit-ID: AU4CUq09tw4
Source-Repo: https://github.com/servo/servo
Source-Revision: d287ec8d3ee5af9e64979ec77894f218cae1b46d

diff --git a/servo/components/style/values/computed/length.rs b/servo/components/style/values/computed/length.rs
--- a/servo/components/style/values/computed/length.rs
+++ b/servo/components/style/values/computed/length.rs
@@ -31,19 +31,16 @@ impl ToComputedValue for specified::NoCa
             specified::NoCalcLength::Absolute(length) =>
                 length.to_computed_value(context),
             specified::NoCalcLength::FontRelative(length) =>
                 length.to_computed_value(context, FontBaseSize::CurrentStyle),
             specified::NoCalcLength::ViewportPercentage(length) =>
                 length.to_computed_value(context.viewport_size_for_viewport_unit_resolution()),
             specified::NoCalcLength::ServoCharacterWidth(length) =>
                 length.to_computed_value(context.style().get_font().clone_font_size().size()),
-            #[cfg(feature = "gecko")]
-            specified::NoCalcLength::Physical(length) =>
-                length.to_computed_value(context),
         }
     }
 
     #[inline]
     fn from_computed_value(computed: &Self::ComputedValue) -> Self {
         specified::NoCalcLength::Absolute(AbsoluteLength::Px(computed.px()))
     }
 }
diff --git a/servo/components/style/values/specified/calc.rs b/servo/components/style/values/specified/calc.rs
--- a/servo/components/style/values/specified/calc.rs
+++ b/servo/components/style/values/specified/calc.rs
@@ -72,18 +72,16 @@ pub struct CalcLengthOrPercentage {
     pub vh: Option<CSSFloat>,
     pub vmin: Option<CSSFloat>,
     pub vmax: Option<CSSFloat>,
     pub em: Option<CSSFloat>,
     pub ex: Option<CSSFloat>,
     pub ch: Option<CSSFloat>,
     pub rem: Option<CSSFloat>,
     pub percentage: Option<computed::Percentage>,
-    #[cfg(feature = "gecko")]
-    pub mozmm: Option<CSSFloat>,
 }
 
 impl ToCss for CalcLengthOrPercentage {
     /// <https://drafts.csswg.org/css-values/#calc-serialize>
     ///
     /// FIXME(emilio): Should this simplify away zeros?
     #[allow(unused_assignments)]
     fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
@@ -137,24 +135,17 @@ impl ToCss for CalcLengthOrPercentage {
             val.abs().to_css(dest)?;
         }
 
         // NOTE(emilio): The order here it's very intentional, and alphabetic
         // per the spec linked above.
         serialize!(ch);
         serialize_abs!(Cm);
         serialize!(em, ex);
-        serialize_abs!(In);
-
-        #[cfg(feature = "gecko")]
-        {
-            serialize!(mozmm);
-        }
-
-        serialize_abs!(Mm, Pc, Pt, Px, Q);
+        serialize_abs!(In, Mm, Pc, Pt, Px, Q);
         serialize!(rem, vh, vmax, vmin, vw);
 
         dest.write_str(")")
     }
 }
 
 impl CalcNode {
     /// Tries to parse a single element in the expression, that is, a
@@ -393,26 +384,16 @@ impl CalcNode {
                                 ret.vmax = Some(ret.vmax.unwrap_or(0.) + vmax * factor)
                             }
                             ViewportPercentageLength::Vmin(vmin) => {
                                 ret.vmin = Some(ret.vmin.unwrap_or(0.) + vmin * factor)
                             }
                         }
                     }
                     NoCalcLength::ServoCharacterWidth(..) => unreachable!(),
-                    #[cfg(feature = "gecko")]
-                    NoCalcLength::Physical(physical) => {
-                        use values::specified::length::PhysicalLength;
-
-                        match physical {
-                            PhysicalLength::Mozmm(mozmm) => {
-                                ret.mozmm = Some(ret.mozmm.unwrap_or(0.) + mozmm * factor);
-                            }
-                        }
-                    }
                 }
             }
             CalcNode::Sub(ref a, ref b) => {
                 a.add_length_or_percentage_to(ret, factor)?;
                 b.add_length_or_percentage_to(ret, factor * -1.0)?;
             }
             CalcNode::Sum(ref a, ref b) => {
                 a.add_length_or_percentage_to(ret, factor)?;
diff --git a/servo/components/style/values/specified/length.rs b/servo/components/style/values/specified/length.rs
--- a/servo/components/style/values/specified/length.rs
+++ b/servo/components/style/values/specified/length.rs
@@ -355,70 +355,16 @@ impl Add<AbsoluteLength> for AbsoluteLen
             (AbsoluteLength::Q(x), AbsoluteLength::Q(y)) => AbsoluteLength::Q(x + y),
             (AbsoluteLength::Pt(x), AbsoluteLength::Pt(y)) => AbsoluteLength::Pt(x + y),
             (AbsoluteLength::Pc(x), AbsoluteLength::Pc(y)) => AbsoluteLength::Pc(x + y),
             _ => AbsoluteLength::Px(self.to_px() + rhs.to_px()),
         }
     }
 }
 
-/// Represents a physical length based on DPI.
-///
-/// FIXME(emilio): Unship (https://bugzilla.mozilla.org/show_bug.cgi?id=1416564)
-#[derive(Clone, Copy, Debug, PartialEq, ToCss)]
-#[derive(MallocSizeOf)]
-pub enum PhysicalLength {
-    /// A physical length in millimetres.
-    #[css(dimension)]
-    Mozmm(CSSFloat),
-}
-
-impl PhysicalLength {
-    /// Checks whether the length value is zero.
-    pub fn is_zero(&self) -> bool {
-        match *self {
-            PhysicalLength::Mozmm(v) => v == 0.,
-        }
-    }
-
-    /// Computes the given character width.
-    #[cfg(feature = "gecko")]
-    pub fn to_computed_value(&self, context: &Context) -> CSSPixelLength {
-        use gecko_bindings::bindings;
-        use std::f32;
-
-        // Same as Gecko
-        const INCH_PER_MM: f32 = 1. / 25.4;
-
-        let au_per_physical_inch = unsafe {
-            bindings::Gecko_GetAppUnitsPerPhysicalInch(context.device().pres_context()) as f32
-        };
-
-        let px_per_physical_inch = au_per_physical_inch / AU_PER_PX;
-
-        let mm = match *self {
-            PhysicalLength::Mozmm(v) => v,
-        };
-
-        let pixel = mm * px_per_physical_inch * INCH_PER_MM;
-        CSSPixelLength::new(pixel.min(f32::MAX).max(f32::MIN))
-    }
-}
-
-impl Mul<CSSFloat> for PhysicalLength {
-    type Output = Self ;
-
-    #[inline]
-    fn mul(self, scalar: CSSFloat) -> Self {
-        match self {
-            PhysicalLength::Mozmm(v) => PhysicalLength::Mozmm(v * scalar),
-        }
-    }
-}
-
 /// A `<length>` without taking `calc` expressions into account
 ///
 /// <https://drafts.csswg.org/css-values/#lengths>
 #[derive(Clone, Copy, Debug, MallocSizeOf, PartialEq, ToCss)]
 pub enum NoCalcLength {
     /// An absolute length
     ///
     /// <https://drafts.csswg.org/css-values/#absolute-length>
@@ -435,34 +381,28 @@ pub enum NoCalcLength {
     ViewportPercentage(ViewportPercentageLength),
 
     /// HTML5 "character width", as defined in HTML5 § 14.5.4.
     ///
     /// This cannot be specified by the user directly and is only generated by
     /// `Stylist::synthesize_rules_for_legacy_attributes()`.
     #[css(function)]
     ServoCharacterWidth(CharacterWidth),
-
-    /// A physical length (mozmm) based on DPI
-    #[cfg(feature = "gecko")]
-    Physical(PhysicalLength),
 }
 
 impl Mul<CSSFloat> for NoCalcLength {
     type Output = NoCalcLength;
 
     #[inline]
     fn mul(self, scalar: CSSFloat) -> NoCalcLength {
         match self {
             NoCalcLength::Absolute(v) => NoCalcLength::Absolute(v * scalar),
             NoCalcLength::FontRelative(v) => NoCalcLength::FontRelative(v * scalar),
             NoCalcLength::ViewportPercentage(v) => NoCalcLength::ViewportPercentage(v * scalar),
             NoCalcLength::ServoCharacterWidth(_) => panic!("Can't multiply ServoCharacterWidth!"),
-            #[cfg(feature = "gecko")]
-            NoCalcLength::Physical(v) => NoCalcLength::Physical(v * scalar),
         }
     }
 }
 
 impl NoCalcLength {
     /// Parse a given absolute or relative dimension.
     pub fn parse_dimension(context: &ParserContext, value: CSSFloat, unit: &str)
                            -> Result<NoCalcLength, ()> {
@@ -500,35 +440,31 @@ impl NoCalcLength {
                 Ok(NoCalcLength::ViewportPercentage(ViewportPercentageLength::Vmin(value)))
             },
             "vmax" => {
                 if in_page_rule {
                     return Err(())
                 }
                 Ok(NoCalcLength::ViewportPercentage(ViewportPercentageLength::Vmax(value)))
             },
-            #[cfg(feature = "gecko")]
-            "mozmm" => Ok(NoCalcLength::Physical(PhysicalLength::Mozmm(value))),
             _ => Err(())
         }
     }
 
     #[inline]
     /// Returns a `zero` length.
     pub fn zero() -> NoCalcLength {
         NoCalcLength::Absolute(AbsoluteLength::Px(0.))
     }
 
     #[inline]
     /// Checks whether the length value is zero.
     pub fn is_zero(&self) -> bool {
         match *self {
             NoCalcLength::Absolute(length) => length.is_zero(),
-            #[cfg(feature = "gecko")]
-            NoCalcLength::Physical(length) => length.is_zero(),
             _ => false
         }
     }
 
     /// Get an absolute length from a px value.
     #[inline]
     pub fn from_px(px_value: CSSFloat) -> NoCalcLength {
         NoCalcLength::Absolute(AbsoluteLength::Px(px_value))
diff --git a/servo/components/style_derive/to_css.rs b/servo/components/style_derive/to_css.rs
--- a/servo/components/style_derive/to_css.rs
+++ b/servo/components/style_derive/to_css.rs
@@ -54,21 +54,16 @@ pub fn derive(input: DeriveInput) -> Tok
             }}
         } else {
             quote! {
                 ::std::fmt::Write::write_str(dest, #identifier)
             }
         };
 
         if variant_attrs.dimension {
-            // FIXME(emilio): Remove when bug 1416564 lands.
-            if identifier == "-mozmm" {
-                identifier = "mozmm".into();
-            }
-
             expr = quote! {
                 #expr?;
                 ::std::fmt::Write::write_str(dest, #identifier)
             }
         } else if variant_attrs.function {
             identifier.push_str("(");
             expr = quote! {
                 ::std::fmt::Write::write_str(dest, #identifier)?;
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -3158,28 +3158,27 @@ pub extern "C" fn Servo_DeclarationBlock
     property: nsCSSPropertyID,
     value: f32,
     unit: structs::nsCSSUnit,
 ) {
     use style::properties::{PropertyDeclaration, LonghandId};
     use style::properties::longhands::_moz_script_min_size::SpecifiedValue as MozScriptMinSize;
     use style::properties::longhands::width::SpecifiedValue as Width;
     use style::values::specified::MozLength;
-    use style::values::specified::length::{AbsoluteLength, FontRelativeLength, PhysicalLength};
+    use style::values::specified::length::{AbsoluteLength, FontRelativeLength};
     use style::values::specified::length::{LengthOrPercentage, NoCalcLength};
 
     let long = get_longhand_from_id!(property);
     let nocalc = match unit {
         structs::nsCSSUnit::eCSSUnit_EM => NoCalcLength::FontRelative(FontRelativeLength::Em(value)),
         structs::nsCSSUnit::eCSSUnit_XHeight => NoCalcLength::FontRelative(FontRelativeLength::Ex(value)),
         structs::nsCSSUnit::eCSSUnit_Pixel => NoCalcLength::Absolute(AbsoluteLength::Px(value)),
         structs::nsCSSUnit::eCSSUnit_Inch => NoCalcLength::Absolute(AbsoluteLength::In(value)),
         structs::nsCSSUnit::eCSSUnit_Centimeter => NoCalcLength::Absolute(AbsoluteLength::Cm(value)),
         structs::nsCSSUnit::eCSSUnit_Millimeter => NoCalcLength::Absolute(AbsoluteLength::Mm(value)),
-        structs::nsCSSUnit::eCSSUnit_PhysicalMillimeter => NoCalcLength::Physical(PhysicalLength::Mozmm(value)),
         structs::nsCSSUnit::eCSSUnit_Point => NoCalcLength::Absolute(AbsoluteLength::Pt(value)),
         structs::nsCSSUnit::eCSSUnit_Pica => NoCalcLength::Absolute(AbsoluteLength::Pc(value)),
         structs::nsCSSUnit::eCSSUnit_Quarter => NoCalcLength::Absolute(AbsoluteLength::Q(value)),
         _ => unreachable!("Unknown unit {:?} passed to SetLengthValue", unit)
     };
 
     let prop = match_wrap_declared! { long,
         Width => Width(MozLength::LengthOrPercentageOrAuto(nocalc.into())),
