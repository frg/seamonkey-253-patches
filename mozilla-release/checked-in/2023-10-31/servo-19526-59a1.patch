# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1512780579 21600
# Node ID 33d7b8a51e80d543dde79ba44be161461b408f4b
# Parent  bd2ff781886fec45a3a949fb8b68cbdb6002b98a
servo: Merge #19526 - Add @supports -moz-bool-pref() support for stylo (from upsuper:moz-bool-pref); r=emilio

This is the Servo side change for [bug 1267890](https://bugzilla.mozilla.org/show_bug.cgi?id=1267890).

Source-Repo: https://github.com/servo/servo
Source-Revision: 42a3c1631c27dde2d845338ccf80e0fb1b3cf9bf

diff --git a/servo/components/style/stylesheets/supports_rule.rs b/servo/components/style/stylesheets/supports_rule.rs
--- a/servo/components/style/stylesheets/supports_rule.rs
+++ b/servo/components/style/stylesheets/supports_rule.rs
@@ -8,17 +8,20 @@ use cssparser::{Delimiter, parse_importa
 use cssparser::{ParseError as CssParseError, ParserInput};
 #[cfg(feature = "gecko")]
 use malloc_size_of::{MallocSizeOfOps, MallocUnconditionalShallowSizeOf};
 use parser::ParserContext;
 use properties::{PropertyId, PropertyDeclaration, SourcePropertyDeclaration};
 use selectors::parser::SelectorParseErrorKind;
 use servo_arc::Arc;
 use shared_lock::{DeepCloneParams, DeepCloneWithLock, Locked, SharedRwLock, SharedRwLockReadGuard, ToCssWithGuard};
+#[allow(unused_imports)] use std::ascii::AsciiExt;
+use std::ffi::{CStr, CString};
 use std::fmt;
+use std::str;
 use style_traits::{ToCss, ParseError};
 use stylesheets::{CssRuleType, CssRules};
 
 /// An [`@supports`][supports] rule.
 ///
 /// [supports]: https://drafts.csswg.org/css-conditional-3/#at-supports
 #[derive(Debug)]
 pub struct SupportsRule {
@@ -78,16 +81,20 @@ pub enum SupportsCondition {
     /// `(condition)`
     Parenthesized(Box<SupportsCondition>),
     /// `(condition) and (condition) and (condition) ..`
     And(Vec<SupportsCondition>),
     /// `(condition) or (condition) or (condition) ..`
     Or(Vec<SupportsCondition>),
     /// `property-ident: value` (value can be any tokens)
     Declaration(Declaration),
+    /// `-moz-bool-pref("pref-name")`
+    /// Since we need to pass it through FFI to get the pref value,
+    /// we store it as CString directly.
+    MozBoolPref(CString),
     /// `(any tokens)` or `func(any tokens)`
     FutureSyntax(String),
 }
 
 impl SupportsCondition {
     /// Parse a condition
     ///
     /// <https://drafts.csswg.org/css-conditional/#supports_condition>
@@ -140,36 +147,71 @@ impl SupportsCondition {
             Token::ParenthesisBlock => {
                 let nested = input.try(|input| {
                     input.parse_nested_block(|i| parse_condition_or_declaration(i))
                 });
                 if nested.is_ok() {
                     return nested;
                 }
             }
-            Token::Function(_) => {}
+            Token::Function(ident) => {
+                // Although this is an internal syntax, it is not necessary to check
+                // parsing context as far as we accept any unexpected token as future
+                // syntax, and evaluate it to false when not in chrome / ua sheet.
+                // See https://drafts.csswg.org/css-conditional-3/#general_enclosed
+                if ident.eq_ignore_ascii_case("-moz-bool-pref") {
+                    if let Ok(name) = input.try(|i| {
+                        i.parse_nested_block(|i| {
+                            i.expect_string()
+                                .map(|s| s.to_string())
+                                .map_err(CssParseError::<()>::from)
+                        }).and_then(|s| {
+                            CString::new(s)
+                                .map_err(|_| location.new_custom_error(()))
+                        })
+                    }) {
+                        return Ok(SupportsCondition::MozBoolPref(name));
+                    }
+                }
+            }
             t => return Err(location.new_unexpected_token_error(t)),
         }
         input.parse_nested_block(|i| consume_any_value(i))?;
         Ok(SupportsCondition::FutureSyntax(input.slice_from(pos).to_owned()))
     }
 
     /// Evaluate a supports condition
     pub fn eval(&self, cx: &ParserContext) -> bool {
         match *self {
             SupportsCondition::Not(ref cond) => !cond.eval(cx),
             SupportsCondition::Parenthesized(ref cond) => cond.eval(cx),
             SupportsCondition::And(ref vec) => vec.iter().all(|c| c.eval(cx)),
             SupportsCondition::Or(ref vec) => vec.iter().any(|c| c.eval(cx)),
             SupportsCondition::Declaration(ref decl) => decl.eval(cx),
+            SupportsCondition::MozBoolPref(ref name) => eval_moz_bool_pref(name, cx),
             SupportsCondition::FutureSyntax(_) => false
         }
     }
 }
 
+#[cfg(feature = "gecko")]
+fn eval_moz_bool_pref(name: &CStr, cx: &ParserContext) -> bool {
+    use gecko_bindings::bindings;
+    use stylesheets::Origin;
+    if cx.stylesheet_origin != Origin::UserAgent && !cx.chrome_rules_enabled() {
+        return false;
+    }
+    unsafe { bindings::Gecko_GetBoolPrefValue(name.as_ptr()) }
+}
+
+#[cfg(feature = "servo")]
+fn eval_moz_bool_pref(_: &CStr, _: &ParserContext) -> bool {
+    false
+}
+
 /// supports_condition | declaration
 /// <https://drafts.csswg.org/css-conditional/#dom-css-supports-conditiontext-conditiontext>
 pub fn parse_condition_or_declaration<'i, 't>(input: &mut Parser<'i, 't>)
                                               -> Result<SupportsCondition, ParseError<'i>> {
     if let Ok(condition) = input.try(SupportsCondition::parse) {
         Ok(SupportsCondition::Parenthesized(Box::new(condition)))
     } else {
         Declaration::parse(input).map(SupportsCondition::Declaration)
@@ -212,16 +254,23 @@ impl ToCss for SupportsCondition {
                 }
                 Ok(())
             }
             SupportsCondition::Declaration(ref decl) => {
                 dest.write_str("(")?;
                 decl.to_css(dest)?;
                 dest.write_str(")")
             }
+            SupportsCondition::MozBoolPref(ref name) => {
+                dest.write_str("-moz-bool-pref(")?;
+                let name = str::from_utf8(name.as_bytes())
+                    .expect("Should be parsed from valid UTF-8");
+                name.to_css(dest)?;
+                dest.write_str(")")
+            }
             SupportsCondition::FutureSyntax(ref s) => dest.write_str(&s),
         }
     }
 }
 
 #[derive(Clone, Debug)]
 /// A possibly-invalid property declaration
 pub struct Declaration(pub String);
