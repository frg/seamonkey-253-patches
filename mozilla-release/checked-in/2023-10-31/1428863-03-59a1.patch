# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1514054834 18000
#      Sat Dec 23 13:47:14 2017 -0500
# Node ID 98378bbaf80721b363d52d77601ca8738142da71
# Parent  c34b5f7f857cd85ebeddf85c4b699ff6eea1e4ca
Bug 1428863 - Change a few TokenStream::* uses in AsmJS.cpp to TokenStreamShared::*, because TokenStream no longer has the meaning it's traditionally had.  r=arai

diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -758,17 +758,17 @@ NextNonEmptyStatement(ParseNode* pn)
 }
 
 static bool
 GetToken(AsmJSParser& parser, TokenKind* tkp)
 {
     auto& ts = parser.tokenStream;
     TokenKind tk;
     while (true) {
-        if (!ts.getToken(&tk, TokenStream::Operand))
+        if (!ts.getToken(&tk, TokenStreamShared::Operand))
             return false;
         if (tk != TokenKind::Semi)
             break;
     }
     *tkp = tk;
     return true;
 }
 
@@ -777,17 +777,17 @@ PeekToken(AsmJSParser& parser, TokenKind
 {
     auto& ts = parser.tokenStream;
     TokenKind tk;
     while (true) {
         if (!ts.peekToken(&tk, TokenStream::Operand))
             return false;
         if (tk != TokenKind::Semi)
             break;
-        ts.consumeKnownToken(TokenKind::Semi, TokenStream::Operand);
+        ts.consumeKnownToken(TokenKind::Semi, TokenStreamShared::Operand);
     }
     *tkp = tk;
     return true;
 }
 
 static bool
 ParseVarOrConstStatement(AsmJSParser& parser, ParseNode** var)
 {
@@ -2445,17 +2445,17 @@ class MOZ_STACK_CLASS ModuleValidator
             if (!funcName || !asmJSMetadata_->asmJSFuncNames.emplaceBack(Move(funcName)))
                 return nullptr;
         }
 
         uint32_t endBeforeCurly = tokenStream().anyCharsAccess().currentToken().pos.end;
         asmJSMetadata_->srcLength = endBeforeCurly - asmJSMetadata_->srcStart;
 
         TokenPos pos;
-        JS_ALWAYS_TRUE(tokenStream().peekTokenPos(&pos, TokenStream::Operand));
+        JS_ALWAYS_TRUE(tokenStream().peekTokenPos(&pos, TokenStreamShared::Operand));
         uint32_t endAfterCurly = pos.end;
         asmJSMetadata_->srcLengthWithRightBrace = endAfterCurly - asmJSMetadata_->srcStart;
 
         ScriptedCaller scriptedCaller;
         if (parser_.ss->filename()) {
             scriptedCaller.line = scriptedCaller.column = 0;  // unused
             scriptedCaller.filename = DuplicateString(parser_.ss->filename());
             if (!scriptedCaller.filename)
@@ -3838,17 +3838,17 @@ CheckModuleGlobal(ModuleValidator& m, Pa
 }
 
 static bool
 CheckModuleProcessingDirectives(ModuleValidator& m)
 {
     auto& ts = m.parser().tokenStream;
     while (true) {
         bool matched;
-        if (!ts.matchToken(&matched, TokenKind::String, TokenStream::Operand))
+        if (!ts.matchToken(&matched, TokenKind::String, TokenStreamShared::Operand))
             return false;
         if (!matched)
             return true;
 
         if (!IsIgnoredDirectiveName(m.cx(), ts.anyCharsAccess().currentToken().atom()))
             return m.failCurrentOffset("unsupported processing directive");
 
         TokenKind tt;
@@ -7166,24 +7166,24 @@ CheckStatement(FunctionValidator& f, Par
     return f.fail(stmt, "unexpected statement kind");
 }
 
 static bool
 ParseFunction(ModuleValidator& m, ParseNode** fnOut, unsigned* line)
 {
     auto& tokenStream = m.tokenStream();
 
-    tokenStream.consumeKnownToken(TokenKind::Function, TokenStream::Operand);
+    tokenStream.consumeKnownToken(TokenKind::Function, TokenStreamShared::Operand);
 
     auto& anyChars = tokenStream.anyCharsAccess();
     uint32_t toStringStart = anyChars.currentToken().pos.begin;
     *line = anyChars.srcCoords.lineNum(anyChars.currentToken().pos.end);
 
     TokenKind tk;
-    if (!tokenStream.getToken(&tk, TokenStream::Operand))
+    if (!tokenStream.getToken(&tk, TokenStreamShared::Operand))
         return false;
     if (tk == TokenKind::Mul)
         return m.failCurrentOffset("unexpected generator function");
     if (!TokenKindIsPossibleIdentifier(tk))
         return false;  // The regular parser will throw a SyntaxError, no need to m.fail.
 
     RootedPropertyName name(m.cx(), m.parser().bindingIdentifier(YieldIsName));
     if (!name)
@@ -8380,17 +8380,17 @@ class ModuleChars
 
   public:
     static uint32_t beginOffset(AsmJSParser& parser) {
         return parser.pc->functionBox()->functionNode->pn_pos.begin;
     }
 
     static uint32_t endOffset(AsmJSParser& parser) {
         TokenPos pos(0, 0);  // initialize to silence GCC warning
-        MOZ_ALWAYS_TRUE(parser.tokenStream.peekTokenPos(&pos, TokenStream::Operand));
+        MOZ_ALWAYS_TRUE(parser.tokenStream.peekTokenPos(&pos, TokenStreamShared::Operand));
         return pos.end;
     }
 };
 
 class ModuleCharsForStore : ModuleChars
 {
     uint32_t uncompressedSize_;
     uint32_t compressedSize_;
