# HG changeset patch
# User Chris Peterson <cpeterson@mozilla.com>
# Date 1487302069 28800
# Node ID 2fc7552a16ecddc956a2db6dbf22ff6054de38ea
# Parent  d6f755fd81c15548a2069dc32a0f4ce8d7094938
Bug 1160368 - Part 1: Add flag to treat third-party cookies set over nonsecure HTTP as session cookies. r=jdm

"Nonsecure HTTP" here just means regular, not-HTTPS HTTP. It doesn't mean HTTPS without the `Secure` cookie flag. Honor the expiration time of third-party cookies set over HTTPS, whether or not they have the `Secure` cookie flag. If a third-party cookie is set over HTTPS and then later sent in nonsecure HTTP request (which is allowed for cookies without the `Secure` cookie flag), the cookie won't be turned into a session cookie unless the nonsecure HTTP response sets a new cookie value.

This feature is controlled by the pref "network.cookie.thirdparty.nonsecureSessionOnly".

MozReview-Commit-ID: HlCg21JyvNC

diff --git a/extensions/cookie/test/unit/test_cookies_thirdparty_session.js b/extensions/cookie/test/unit/test_cookies_thirdparty_nonsecure_session.js
copy from extensions/cookie/test/unit/test_cookies_thirdparty_session.js
copy to extensions/cookie/test/unit/test_cookies_thirdparty_nonsecure_session.js
--- a/extensions/cookie/test/unit/test_cookies_thirdparty_session.js
+++ b/extensions/cookie/test/unit/test_cookies_thirdparty_nonsecure_session.js
@@ -1,14 +1,16 @@
 /* Any copyright is dedicated to the Public Domain.
    http://creativecommons.org/publicdomain/zero/1.0/ */
 
-// test third party persistence across sessions, for the cases:
-// 1) network.cookie.thirdparty.sessionOnly = false
-// 2) network.cookie.thirdparty.sessionOnly = true
+// test nonsecure third party persistence across sessions, for the cases:
+// 1) network.cookie.thirdparty.nonsecureSessionOnly = false
+// 2) network.cookie.thirdparty.nonsecureSessionOnly = true
+
+"use strict";
 
 var test_generator = do_run_test();
 
 function run_test() {
   do_test_pending();
   test_generator.next();
 }
 
@@ -21,49 +23,49 @@ function finish_test() {
 
 function* do_run_test() {
   // Set up a profile.
   let profile = do_get_profile();
 
   // Create URIs and channels pointing to foo.com and bar.com.
   // We will use these to put foo.com into first and third party contexts.
   var spec1 = "http://foo.com/foo.html";
-  var spec2 = "http://bar.com/bar.html";
+  var spec2 = "https://bar.com/bar.html";
   var uri1 = NetUtil.newURI(spec1);
   var uri2 = NetUtil.newURI(spec2);
   var channel1 = NetUtil.newChannel({uri: uri1, loadUsingSystemPrincipal: true});
   var channel2 = NetUtil.newChannel({uri: uri2, loadUsingSystemPrincipal: true});
 
   // Force the channel URI to be used when determining the originating URI of
   // the channel.
   var httpchannel1 = channel1.QueryInterface(Ci.nsIHttpChannelInternal);
   var httpchannel2 = channel2.QueryInterface(Ci.nsIHttpChannelInternal);
   httpchannel1.forceAllowThirdPartyCookie = true;
   httpchannel2.forceAllowThirdPartyCookie = true;
 
-  // test with cookies enabled, and third party cookies persistent.
+  // test with cookies enabled and nonsecure third party cookies persistent.
   Services.prefs.setIntPref("network.cookie.cookieBehavior", 0);
-  Services.prefs.setBoolPref("network.cookie.thirdparty.sessionOnly", false);
-  do_set_cookies(uri1, channel2, false, [1, 2, 3, 4]);
-  do_set_cookies(uri2, channel1, true, [1, 2, 3, 4]);
+  Services.prefs.setBoolPref("network.cookie.thirdparty.nonsecureSessionOnly", false);
+  do_set_cookies(uri1, channel2, false, [1, 2, 3, 4]); // third-party HTTP
+  do_set_cookies(uri2, channel1, false, [1, 2, 3, 4]); // third-party HTTPS
 
   // fake a profile change
   do_close_profile(test_generator);
   yield;
   do_load_profile();
-  Assert.equal(Services.cookies.countCookiesFromHost(uri1.host), 4);
-  Assert.equal(Services.cookies.countCookiesFromHost(uri2.host), 0);
+  Assert.equal(Services.cookies.countCookiesFromHost(uri1.host), 4); // HTTP cookies OK
+  Assert.equal(Services.cookies.countCookiesFromHost(uri2.host), 4); // HTTPS cookies OK
 
-  // test with third party cookies for session only.
-  Services.prefs.setBoolPref("network.cookie.thirdparty.sessionOnly", true);
+  // test with nonsecure third party cookies for session only.
+  Services.prefs.setBoolPref("network.cookie.thirdparty.nonsecureSessionOnly", true);
   Services.cookies.removeAll();
-  do_set_cookies(uri1, channel2, false, [1, 2, 3, 4]);
-  do_set_cookies(uri2, channel1, true, [1, 2, 3, 4]);
+  do_set_cookies(uri1, channel2, false, [1, 2, 3, 4]); // third-party HTTP
+  do_set_cookies(uri2, channel1, false, [1, 2, 3, 4]); // third-party HTTPS
 
   // fake a profile change
   do_close_profile(test_generator);
   yield;
   do_load_profile();
-  Assert.equal(Services.cookies.countCookiesFromHost(uri1.host), 0);
-  Assert.equal(Services.cookies.countCookiesFromHost(uri2.host), 0);
+  Assert.equal(Services.cookies.countCookiesFromHost(uri1.host), 0); // no HTTP cookies!
+  Assert.equal(Services.cookies.countCookiesFromHost(uri2.host), 4); // HTTPS cookies OK
 
   finish_test();
 }
diff --git a/extensions/cookie/test/unit/xpcshell.ini b/extensions/cookie/test/unit/xpcshell.ini
--- a/extensions/cookie/test/unit/xpcshell.ini
+++ b/extensions/cookie/test/unit/xpcshell.ini
@@ -8,16 +8,17 @@ skip-if = toolkit == 'android'
 [test_cookies_async_failure.js]
 [test_cookies_persistence.js]
 skip-if = true # Bug 863738
 [test_cookies_privatebrowsing.js]
 [test_cookies_profile_close.js]
 [test_cookies_read.js]
 [test_cookies_sync_failure.js]
 [test_cookies_thirdparty.js]
+[test_cookies_thirdparty_nonsecure_session.js]
 [test_cookies_thirdparty_session.js]
 [test_domain_eviction.js]
 [test_eviction.js]
 [test_permmanager_default_pref.js]
 [test_permmanager_defaults.js]
 [test_permmanager_expiration.js]
 [test_permmanager_getAllForURI.js]
 [test_permmanager_getPermissionObject.js]
diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -2347,16 +2347,17 @@ pref("network.proxy.proxy_over_tls",    
 pref("network.proxy.no_proxies_on",         "localhost, 127.0.0.1");
 pref("network.proxy.failover_timeout",      1800); // 30 minutes
 pref("network.online",                      true); //online/offline
 pref("network.cookie.cookieBehavior",       0); // 0-Accept, 1-dontAcceptForeign, 2-dontAcceptAny, 3-limitForeign
 #ifdef ANDROID
 pref("network.cookie.cookieBehavior",       0); // Keep the old default of accepting all cookies
 #endif
 pref("network.cookie.thirdparty.sessionOnly", false);
+pref("network.cookie.thirdparty.nonsecureSessionOnly", false);
 pref("network.cookie.leave-secure-alone",   true);
 pref("network.cookie.ipc.sync",             false);
 pref("network.cookie.lifetimePolicy",       0); // 0-accept, 1-dontUse 2-acceptForSession, 3-acceptForNDays
 pref("network.cookie.prefsMigrated",        false);
 pref("network.cookie.lifetime.days",        90); // Ignored unless network.cookie.lifetimePolicy is 3.
 
 // The PAC file to load.  Ignored unless network.proxy.type is 2.
 pref("network.proxy.autoconfig_url", "");
diff --git a/netwerk/cookie/CookieServiceChild.cpp b/netwerk/cookie/CookieServiceChild.cpp
--- a/netwerk/cookie/CookieServiceChild.cpp
+++ b/netwerk/cookie/CookieServiceChild.cpp
@@ -28,16 +28,18 @@ using mozilla::OriginAttributes;
 
 namespace mozilla {
 namespace net {
 
 // Pref string constants
 static const char kPrefCookieBehavior[] = "network.cookie.cookieBehavior";
 static const char kPrefThirdPartySession[] =
   "network.cookie.thirdparty.sessionOnly";
+static const char kPrefThirdPartyNonsecureSession[] =
+  "network.cookie.thirdparty.nonsecureSessionOnly";
 static const char kPrefCookieIPCSync[] = "network.cookie.ipc.sync";
 static const char kCookieLeaveSecurityAlone[] = "network.cookie.leave-secure-alone";
 
 static StaticRefPtr<CookieServiceChild> gCookieService;
 
 already_AddRefed<CookieServiceChild>
 CookieServiceChild::GetSingleton()
 {
@@ -52,16 +54,17 @@ CookieServiceChild::GetSingleton()
 NS_IMPL_ISUPPORTS(CookieServiceChild,
                   nsICookieService,
                   nsIObserver,
                   nsISupportsWeakReference)
 
 CookieServiceChild::CookieServiceChild()
   : mCookieBehavior(nsICookieService::BEHAVIOR_ACCEPT)
   , mThirdPartySession(false)
+  , mThirdPartyNonsecureSession(false)
   , mLeaveSecureAlone(true)
   , mIPCSync(false)
   , mIPCOpen(false)
 {
   NS_ASSERTION(IsNeckoChild(), "not a child process");
 
   mozilla::dom::ContentChild* cc =
     static_cast<mozilla::dom::ContentChild*>(gNeckoChild->Manager());
@@ -84,16 +87,17 @@ CookieServiceChild::CookieServiceChild()
 
   // Init our prefs and observer.
   nsCOMPtr<nsIPrefBranch> prefBranch =
     do_GetService(NS_PREFSERVICE_CONTRACTID);
   NS_WARNING_ASSERTION(prefBranch, "no prefservice");
   if (prefBranch) {
     prefBranch->AddObserver(kPrefCookieBehavior, this, true);
     prefBranch->AddObserver(kPrefThirdPartySession, this, true);
+    prefBranch->AddObserver(kPrefThirdPartyNonsecureSession, this, true);
     prefBranch->AddObserver(kPrefCookieIPCSync, this, true);
     prefBranch->AddObserver(kCookieLeaveSecurityAlone, this, true);
     PrefChanged(prefBranch);
   }
 }
 
 CookieServiceChild::~CookieServiceChild()
 {
@@ -225,16 +229,20 @@ CookieServiceChild::PrefChanged(nsIPrefB
       val >= nsICookieService::BEHAVIOR_ACCEPT &&
       val <= nsICookieService::BEHAVIOR_LIMIT_FOREIGN
         ? val : nsICookieService::BEHAVIOR_ACCEPT;
 
   bool boolval;
   if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kPrefThirdPartySession, &boolval)))
     mThirdPartySession = !!boolval;
 
+  if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kPrefThirdPartyNonsecureSession,
+                                            &boolval)))
+    mThirdPartyNonsecureSession = boolval;
+
   if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kPrefCookieIPCSync, &boolval)))
     mIPCSync = !!boolval;
 
   if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kCookieLeaveSecurityAlone, &boolval)))
     mLeaveSecureAlone = !!boolval;
 
   if (!mThirdPartyUtil && RequireThirdPartyCheck()) {
     mThirdPartyUtil = do_GetService(THIRDPARTYUTIL_CONTRACTID);
@@ -269,17 +277,18 @@ CookieServiceChild::GetCookieStringFromC
   aHostURI->GetPath(pathFromURI);
   aHostURI->SchemeIs("https", &isSecure);
   int64_t currentTimeInUsec = PR_Now();
   int64_t currentTime = currentTimeInUsec / PR_USEC_PER_SEC;
 
   nsCOMPtr<nsICookiePermission> permissionService = do_GetService(NS_COOKIEPERMISSION_CONTRACTID);
   CookieStatus cookieStatus =
     nsCookieService::CheckPrefs(permissionService, mCookieBehavior,
-                                mThirdPartySession, aHostURI,
+                                mThirdPartySession,
+                                mThirdPartyNonsecureSession, aHostURI,
                                 aIsForeign, nullptr,
                                 CountCookiesFromHashTable(baseDomain, aOriginAttrs),
                                 aOriginAttrs);
 
   if (cookieStatus != STATUS_ACCEPTED && cookieStatus != STATUS_ACCEPT_SESSION) {
     return;
   }
 
@@ -367,17 +376,18 @@ CookieServiceChild::SetCookieInternal(ns
   RecordDocumentCookie(cookie, aAttrs);
 }
 
 bool
 CookieServiceChild::RequireThirdPartyCheck()
 {
   return mCookieBehavior == nsICookieService::BEHAVIOR_REJECT_FOREIGN ||
     mCookieBehavior == nsICookieService::BEHAVIOR_LIMIT_FOREIGN ||
-    mThirdPartySession;
+    mThirdPartySession ||
+    mThirdPartyNonsecureSession;
 }
 
 void
 CookieServiceChild::RecordDocumentCookie(nsCookie               *aCookie,
                                          const OriginAttributes &aAttrs)
 {
   nsAutoCString baseDomain;
   nsCookieService::
@@ -515,18 +525,19 @@ CookieServiceChild::SetCookieStringInter
   nsCString baseDomain;
   nsCookieService::
     GetBaseDomain(mTLDService, aHostURI, baseDomain, requireHostMatch);
 
   nsCOMPtr<nsICookiePermission> permissionService = do_GetService(NS_COOKIEPERMISSION_CONTRACTID);
 
   CookieStatus cookieStatus =
     nsCookieService::CheckPrefs(permissionService, mCookieBehavior,
-                                mThirdPartySession, aHostURI,
-                                !!isForeign, aCookieString,
+                                mThirdPartySession,
+                                mThirdPartyNonsecureSession, aHostURI,
+                                isForeign, aCookieString,
                                 CountCookiesFromHashTable(baseDomain, attrs),
                                 attrs);
 
   if (cookieStatus != STATUS_ACCEPTED && cookieStatus != STATUS_ACCEPT_SESSION) {
     return NS_OK;
   }
 
   nsCString serverTimeString(aServerTime);
diff --git a/netwerk/cookie/CookieServiceChild.h b/netwerk/cookie/CookieServiceChild.h
--- a/netwerk/cookie/CookieServiceChild.h
+++ b/netwerk/cookie/CookieServiceChild.h
@@ -116,16 +116,17 @@ protected:
   mozilla::ipc::IPCResult RecvAddCookie(const CookieStruct &aCookie,
                                         const OriginAttributes &aAttrs) override;
 
   CookiesMap mCookiesMap;
   nsCOMPtr<mozIThirdPartyUtil> mThirdPartyUtil;
   nsCOMPtr<nsIEffectiveTLDService> mTLDService;
   uint8_t mCookieBehavior;
   bool mThirdPartySession;
+  bool mThirdPartyNonsecureSession;
   bool mLeaveSecureAlone;
   bool mIPCSync;
   bool mIPCOpen;
 
 };
 
 } // namespace net
 } // namespace mozilla
diff --git a/netwerk/cookie/nsCookieService.cpp b/netwerk/cookie/nsCookieService.cpp
--- a/netwerk/cookie/nsCookieService.cpp
+++ b/netwerk/cookie/nsCookieService.cpp
@@ -116,16 +116,17 @@ static const uint32_t kMaxBytesPerCookie
 static const uint32_t kMaxBytesPerPath    = 1024;
 
 // pref string constants
 static const char kPrefCookieBehavior[]       = "network.cookie.cookieBehavior";
 static const char kPrefMaxNumberOfCookies[]   = "network.cookie.maxNumber";
 static const char kPrefMaxCookiesPerHost[]    = "network.cookie.maxPerHost";
 static const char kPrefCookiePurgeAge[]       = "network.cookie.purgeAge";
 static const char kPrefThirdPartySession[]    = "network.cookie.thirdparty.sessionOnly";
+static const char kPrefThirdPartyNonsecureSession[] = "network.cookie.thirdparty.nonsecureSessionOnly";
 static const char kCookieLeaveSecurityAlone[] = "network.cookie.leave-secure-alone";
 
 static void
 bindCookieParameters(mozIStorageBindingParamsArray *aParamsArray,
                      const nsCookieKey &aKey,
                      const nsCookie *aCookie);
 
 // stores the nsCookieEntry entryclass and an index into the cookie array
@@ -684,16 +685,17 @@ NS_IMPL_ISUPPORTS(nsCookieService,
                   nsIObserver,
                   nsISupportsWeakReference,
                   nsIMemoryReporter)
 
 nsCookieService::nsCookieService()
  : mDBState(nullptr)
  , mCookieBehavior(nsICookieService::BEHAVIOR_ACCEPT)
  , mThirdPartySession(false)
+ , mThirdPartyNonsecureSession(false)
  , mLeaveSecureAlone(true)
  , mMaxNumberOfCookies(kMaxNumberOfCookies)
  , mMaxCookiesPerHost(kMaxCookiesPerHost)
  , mCookiePurgeAge(kCookiePurgeAge)
 {
 }
 
 nsresult
@@ -712,16 +714,17 @@ nsCookieService::Init()
   // init our pref and observer
   nsCOMPtr<nsIPrefBranch> prefBranch = do_GetService(NS_PREFSERVICE_CONTRACTID);
   if (prefBranch) {
     prefBranch->AddObserver(kPrefCookieBehavior,        this, true);
     prefBranch->AddObserver(kPrefMaxNumberOfCookies,    this, true);
     prefBranch->AddObserver(kPrefMaxCookiesPerHost,     this, true);
     prefBranch->AddObserver(kPrefCookiePurgeAge,        this, true);
     prefBranch->AddObserver(kPrefThirdPartySession,     this, true);
+    prefBranch->AddObserver(kPrefThirdPartyNonsecureSession, this, true);
     prefBranch->AddObserver(kCookieLeaveSecurityAlone,  this, true);
     PrefChanged(prefBranch);
   }
 
   mStorageService = do_GetService("@mozilla.org/storage/service;1", &rv);
   NS_ENSURE_SUCCESS(rv, rv);
 
   // Init our default, and possibly private DBStates.
@@ -2115,18 +2118,20 @@ nsCookieService::SetCookieStringInternal
   nsCookieKey key(baseDomain, aOriginAttrs);
 
   // check default prefs
   uint32_t priorCookieCount = 0;
   nsAutoCString hostFromURI;
   aHostURI->GetHost(hostFromURI);
   CountCookiesFromHost(hostFromURI, &priorCookieCount);
   CookieStatus cookieStatus = CheckPrefs(mPermissionService, mCookieBehavior,
-                                         mThirdPartySession, aHostURI, aIsForeign,
-                                         aCookieHeader.get(), priorCookieCount, aOriginAttrs);
+                                         mThirdPartySession,
+                                         mThirdPartyNonsecureSession, aHostURI,
+                                         aIsForeign, aCookieHeader.get(),
+                                         priorCookieCount, aOriginAttrs);
 
   // fire a notification if third party or if cookie was rejected
   // (but not if there was an error)
   switch (cookieStatus) {
   case STATUS_REJECTED:
     NotifyRejected(aHostURI);
     if (aIsForeign) {
       NotifyThirdParty(aHostURI, false, aChannel);
@@ -2314,16 +2319,19 @@ nsCookieService::PrefChanged(nsIPrefBran
     mCookiePurgeAge =
       int64_t(LIMIT(val, 0, INT32_MAX, INT32_MAX)) * PR_USEC_PER_SEC;
   }
 
   bool boolval;
   if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kPrefThirdPartySession, &boolval)))
     mThirdPartySession = boolval;
 
+  if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kPrefThirdPartyNonsecureSession, &boolval)))
+    mThirdPartyNonsecureSession = boolval;
+
   if (NS_SUCCEEDED(aPrefBranch->GetBoolPref(kCookieLeaveSecurityAlone, &boolval)))
     mLeaveSecureAlone = boolval;
 }
 
 /******************************************************************************
  * nsICookieManager impl:
  * nsICookieManager
  ******************************************************************************/
@@ -3263,18 +3271,20 @@ nsCookieService::GetCookiesForURI(nsIURI
     COOKIE_LOGFAILURE(GET_COOKIE, aHostURI, nullptr, "invalid host/path from URI");
     return;
   }
 
   // check default prefs
   uint32_t priorCookieCount = 0;
   CountCookiesFromHost(hostFromURI, &priorCookieCount);
   CookieStatus cookieStatus = CheckPrefs(mPermissionService, mCookieBehavior,
-                                         mThirdPartySession, aHostURI, aIsForeign,
-                                         nullptr, priorCookieCount, aOriginAttrs);
+                                         mThirdPartySession,
+                                         mThirdPartyNonsecureSession, aHostURI,
+                                         aIsForeign, nullptr,
+                                         priorCookieCount, aOriginAttrs);
 
   // for GetCookie(), we don't fire rejection notifications.
   switch (cookieStatus) {
   case STATUS_REJECTED:
   case STATUS_REJECTED_WITH_ERROR:
     return;
   default:
     break;
@@ -4126,16 +4136,17 @@ static inline bool IsSubdomainOf(const n
     return a[a.Length() - b.Length() - 1] == '.' && StringEndsWith(a, b);
   return false;
 }
 
 CookieStatus
 nsCookieService::CheckPrefs(nsICookiePermission    *aPermissionService,
                             uint8_t                 aCookieBehavior,
                             bool                    aThirdPartySession,
+                            bool                    aThirdPartyNonsecureSession,
                             nsIURI                 *aHostURI,
                             bool                    aIsForeign,
                             const char             *aCookieHeader,
                             const int               aNumOfCookies,
                             const OriginAttributes &aOriginAttrs)
 {
   nsresult rv;
 
@@ -4200,30 +4211,38 @@ nsCookieService::CheckPrefs(nsICookiePer
   // check default prefs
   if (aCookieBehavior == nsICookieService::BEHAVIOR_REJECT) {
     COOKIE_LOGFAILURE(aCookieHeader ? SET_COOKIE : GET_COOKIE, aHostURI, aCookieHeader, "cookies are disabled");
     return STATUS_REJECTED;
   }
 
   // check if cookie is foreign
   if (aIsForeign) {
-    if (aCookieBehavior == nsICookieService::BEHAVIOR_ACCEPT && aThirdPartySession)
-      return STATUS_ACCEPT_SESSION;
-
     if (aCookieBehavior == nsICookieService::BEHAVIOR_REJECT_FOREIGN) {
       COOKIE_LOGFAILURE(aCookieHeader ? SET_COOKIE : GET_COOKIE, aHostURI, aCookieHeader, "context is third party");
       return STATUS_REJECTED;
     }
 
     if (aCookieBehavior == nsICookieService::BEHAVIOR_LIMIT_FOREIGN) {
       if (aNumOfCookies == 0) {
         COOKIE_LOGFAILURE(aCookieHeader ? SET_COOKIE : GET_COOKIE, aHostURI, aCookieHeader, "context is third party");
         return STATUS_REJECTED;
       }
-      if (aThirdPartySession)
+    }
+
+    MOZ_ASSERT(aCookieBehavior == nsICookieService::BEHAVIOR_ACCEPT ||
+               aCookieBehavior == nsICookieService::BEHAVIOR_LIMIT_FOREIGN);
+
+    if (aThirdPartySession)
+      return STATUS_ACCEPT_SESSION;
+
+    if (aThirdPartyNonsecureSession) {
+      bool isHTTPS = false;
+      aHostURI->SchemeIs("https", &isHTTPS);
+      if (!isHTTPS)
         return STATUS_ACCEPT_SESSION;
     }
   }
 
   // if nothing has complained, accept cookie
   return STATUS_ACCEPTED;
 }
 
diff --git a/netwerk/cookie/nsCookieService.h b/netwerk/cookie/nsCookieService.h
--- a/netwerk/cookie/nsCookieService.h
+++ b/netwerk/cookie/nsCookieService.h
@@ -226,17 +226,17 @@ class nsCookieService final : public nsI
    */
   static void AppClearDataObserverInit();
   static nsCString GetPathFromURI(nsIURI *aHostURI);
   static nsresult GetBaseDomain(nsIEffectiveTLDService *aTLDService, nsIURI *aHostURI, nsCString &aBaseDomain, bool &aRequireHostMatch);
   static nsresult GetBaseDomainFromHost(nsIEffectiveTLDService *aTLDService, const nsACString &aHost, nsCString &aBaseDomain);
   static bool DomainMatches(nsCookie* aCookie, const nsACString& aHost);
   static bool PathMatches(nsCookie* aCookie, const nsACString& aPath);
   static bool CanSetCookie(nsIURI *aHostURI, const nsCookieKey& aKey, nsCookieAttributes &aCookieAttributes, bool aRequireHostMatch, CookieStatus aStatus, nsDependentCString &aCookieHeader, int64_t aServerTime, bool aFromHttp, nsIChannel* aChannel, bool aLeaveSercureAlone, bool &aSetCookie);
-  static CookieStatus CheckPrefs(nsICookiePermission *aPermissionServices, uint8_t aCookieBehavior, bool aThirdPartyUtil, nsIURI *aHostURI, bool aIsForeign, const char *aCookieHeader, const int aNumOfCookies, const OriginAttributes& aOriginAttrs);
+  static CookieStatus CheckPrefs(nsICookiePermission *aPermissionServices, uint8_t aCookieBehavior, bool aThirdPartySession, bool aThirdPartyNonsecureSession, nsIURI *aHostURI, bool aIsForeign, const char *aCookieHeader, const int aNumOfCookies, const OriginAttributes& aOriginAttrs);
   static int64_t ParseServerTime(const nsCString &aServerTime);
   void GetCookiesForURI(nsIURI *aHostURI, bool aIsForeign, bool aHttpBound, const OriginAttributes& aOriginAttrs, nsTArray<nsCookie*>& aCookieList);
 
   protected:
     virtual ~nsCookieService();
 
     void                          PrefChanged(nsIPrefBranch *aPrefBranch);
     void                          InitDBStates();
@@ -314,16 +314,17 @@ class nsCookieService final : public nsI
     // want to be dealing with the on-disk DB when in private browsing.
     DBState                      *mDBState;
     RefPtr<DBState>             mDefaultDBState;
     RefPtr<DBState>             mPrivateDBState;
 
     // cached prefs
     uint8_t                       mCookieBehavior; // BEHAVIOR_{ACCEPT, REJECTFOREIGN, REJECT, LIMITFOREIGN}
     bool                          mThirdPartySession;
+    bool                          mThirdPartyNonsecureSession;
     bool                          mLeaveSecureAlone;
     uint16_t                      mMaxNumberOfCookies;
     uint16_t                      mMaxCookiesPerHost;
     int64_t                       mCookiePurgeAge;
 
     // friends!
     friend class DBListenerErrorHandler;
     friend class ReadCookieDBListener;
