# HG changeset patch
# User Wei-Cheng Pan <wpan@mozilla.com>
# Date 1516221840 -7200
# Node ID 4567f550794daf96a1e50e1cfa27a56ce41460a6
# Parent  15496302414806e5f1f69bfd6974f158baa1f62b
Bug 1425213 - Unthrottle transform animations regardless in overflowable frames or not. r=hiro
Because we don't know whether the transformed position will be visible or not.

diff --git a/dom/animation/KeyframeEffectReadOnly.cpp b/dom/animation/KeyframeEffectReadOnly.cpp
--- a/dom/animation/KeyframeEffectReadOnly.cpp
+++ b/dom/animation/KeyframeEffectReadOnly.cpp
@@ -1459,53 +1459,59 @@ KeyframeEffectReadOnly::CanThrottle() co
         effectSet->GetAnimationGeneration() !=
           layer->GetAnimationGeneration()) {
       return false;
     }
 
     // If this is a transform animation that affects the overflow region,
     // we should unthrottle the animation periodically.
     if (record.mProperty == eCSSProperty_transform &&
-        !CanThrottleTransformChanges(*frame)) {
+        !CanThrottleTransformChangesForCompositor(*frame)) {
       return false;
     }
   }
 
   for (const AnimationProperty& property : mProperties) {
     if (!property.mIsRunningOnCompositor) {
       return false;
     }
   }
 
   return true;
 }
 
 bool
-KeyframeEffectReadOnly::CanThrottleTransformChanges(nsIFrame& aFrame) const
+KeyframeEffectReadOnly::CanThrottleTransformChanges(const nsIFrame& aFrame) const
 {
-  // If we know that the animation cannot cause overflow,
-  // we can just disable flushes for this animation.
-
-  // If we don't show scrollbars, we don't care about overflow.
-  if (LookAndFeel::GetInt(LookAndFeel::eIntID_ShowHideScrollbars) == 0) {
-    return true;
-  }
-
   TimeStamp now = aFrame.PresContext()->RefreshDriver()->MostRecentRefresh();
 
   EffectSet* effectSet = EffectSet::GetEffectSet(mTarget->mElement,
                                                  mTarget->mPseudoType);
   MOZ_ASSERT(effectSet, "CanThrottleTransformChanges is expected to be called"
                         " on an effect in an effect set");
   MOZ_ASSERT(mAnimation, "CanThrottleTransformChanges is expected to be called"
                          " on an effect with a parent animation");
   TimeStamp lastSyncTime = effectSet->LastTransformSyncTime();
   // If this animation can cause overflow, we can throttle some of the ticks.
-  if (!lastSyncTime.IsNull() &&
-      (now - lastSyncTime) < OverflowRegionRefreshInterval()) {
+  return (!lastSyncTime.IsNull() &&
+    (now - lastSyncTime) < OverflowRegionRefreshInterval());
+}
+
+bool
+KeyframeEffectReadOnly::CanThrottleTransformChangesForCompositor(nsIFrame& aFrame) const
+{
+  // If we know that the animation cannot cause overflow,
+  // we can just disable flushes for this animation.
+
+  // If we don't show scrollbars, we don't care about overflow.
+  if (LookAndFeel::GetInt(LookAndFeel::eIntID_ShowHideScrollbars) == 0) {
+    return true;
+  }
+
+  if (CanThrottleTransformChanges(aFrame)) {
     return true;
   }
 
   // If the nearest scrollable ancestor has overflow:hidden,
   // we don't care about overflow.
   nsIScrollableFrame* scrollable =
     nsLayoutUtils::GetNearestScrollableFrame(&aFrame);
   if (!scrollable) {
diff --git a/dom/animation/KeyframeEffectReadOnly.h b/dom/animation/KeyframeEffectReadOnly.h
--- a/dom/animation/KeyframeEffectReadOnly.h
+++ b/dom/animation/KeyframeEffectReadOnly.h
@@ -447,17 +447,18 @@ private:
   already_AddRefed<nsStyleContext> CreateStyleContextForAnimationValue(
     nsCSSPropertyID aProperty,
     const AnimationValue& aValue,
     const ServoStyleContext* aBaseStyleContext);
 
   nsIFrame* GetAnimationFrame() const;
 
   bool CanThrottle() const;
-  bool CanThrottleTransformChanges(nsIFrame& aFrame) const;
+  bool CanThrottleTransformChanges(const nsIFrame& aFrame) const;
+  bool CanThrottleTransformChangesForCompositor(nsIFrame& aFrame) const;
 
   // Returns true if the computedTiming has changed since the last
   // composition.
   bool HasComputedTimingChanged() const;
 
   // Returns true unless Gecko limitations prevent performing transform
   // animations for |aFrame|. When returning true, the reason for the
   // limitation is stored in |aOutPerformanceWarning|.
diff --git a/dom/animation/test/mozilla/file_restyles.html b/dom/animation/test/mozilla/file_restyles.html
--- a/dom/animation/test/mozilla/file_restyles.html
+++ b/dom/animation/test/mozilla/file_restyles.html
@@ -31,16 +31,20 @@ SimpleTest.finish = function finish() {
 @keyframes background-color {
   from { background-color: red; }
   to { background-color: blue; }
 }
 @keyframes rotate {
   from { transform: rotate(0deg); }
   to { transform: rotate(360deg); }
 }
+@keyframes move-in {
+  from { transform: translate(120%, 120%); }
+  to { transform: translate(0%, 0%); }
+}
 div {
   /* Element needs geometry to be eligible for layerization */
   width: 100px;
   height: 100px;
   background-color: white;
 }
 </style>
 </head>
@@ -487,16 +491,115 @@ waitForAllPaints(() => {
          'Transform animation running on the element which is scrolled out ' +
          'should be unthrottled after around 200ms have elapsed. now: ' +
          now + ' start time: ' + timeAtStart);
 
       await ensureElementRemoval(parentElement);
     }
   );
 
+  add_task(
+    async function restyling_out_of_view_transform_animations_in_another_element() {
+      if (hasConformantPromiseHandling) {
+        return;
+      }
+
+      // Skip this test on Android since this test have been failing
+      // intermittently.
+      // Bug 1413817: We should audit this test still fails once we have the
+      // conformant Promise micro task.
+      if (isAndroid) {
+        return;
+      }
+
+      var parentElement = addDiv(null,
+        { style: 'overflow: hidden;' });
+      var div = addDiv(null,
+        { style: 'animation: move-in 100s;' });
+      parentElement.appendChild(div);
+      var animation = div.getAnimations()[0];
+      var timeAtStart = document.timeline.currentTime;
+
+      ok(!animation.isRunningOnCompositor,
+         'The transform animation on out of view element ' +
+         'is not running on the compositor');
+
+      var markers;
+      var now;
+      while (true) {
+        markers = await observeStyling(1);
+        // Check restyle markers until 200ms is elapsed.
+        now = document.timeline.currentTime;
+        if ((now - timeAtStart) >= 200) {
+          break;
+        }
+
+        is(markers.length, 0,
+           'Transform animation running on out of view element ' +
+           'should be throttled until 200ms is elapsed');
+      }
+
+      is(markers.length, 1,
+         'Transform animation running on out of view element ' +
+         'should be unthrottled after around 200ms have elapsed. now: ' +
+         now + ' start time: ' + timeAtStart);
+
+      await ensureElementRemoval(parentElement);
+    }
+  );
+
+  add_task(
+    async function restyling_out_of_view_transform_animations_in_another_element() {
+      if (!hasConformantPromiseHandling) {
+        return;
+      }
+
+      // Make sure we start from the state right after requestAnimationFrame.
+      await waitForFrame();
+
+      var parentElement = addDiv(null,
+        { style: 'overflow: hidden;' });
+      var div = addDiv(null,
+        { style: 'animation: move-in 100s;' });
+      parentElement.appendChild(div);
+      var animation = div.getAnimations()[0];
+      var timeAtStart = document.timeline.currentTime;
+
+      ok(!animation.isRunningOnCompositor,
+         'The transform animation on out of view element ' +
+         'is not running on the compositor');
+
+      var markers;
+      var now;
+      while (true) {
+        now = document.timeline.currentTime;
+        if ((now - timeAtStart) >= 200) {
+          // If the current time has elapsed over 200ms since the animation was
+          // created, it means that the animation should have already
+          // unthrottled in this tick, let's see what we observe in this tick's
+          // restyling process.
+          markers = await observeStyling(1);
+          break;
+        }
+
+        markers = await observeStyling(1);
+        is(markers.length, 0,
+           'Transform animation running on out of view element ' +
+           'should be throttled until 200ms is elapsed');
+      }
+
+      is(markers.length, 1,
+         'Transform animation running on out of view element ' +
+         'should be unthrottled after around 200ms have elapsed. now: ' +
+         now + ' start time: ' + timeAtStart);
+
+      await ensureElementRemoval(parentElement);
+    }
+  );
+
   add_task(async function restyling_main_thread_animations_in_scrolled_out_element() {
     var parentElement = addDiv(null,
       { style: 'overflow-y: scroll; height: 20px;' });
     var div = addDiv(null,
       { style: 'animation: background-color 100s; position: relative; top: 20px;' });
     parentElement.appendChild(div);
     var animation = div.getAnimations()[0];
 
