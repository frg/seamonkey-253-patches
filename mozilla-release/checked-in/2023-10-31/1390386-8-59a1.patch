# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1511826136 28800
# Node ID a3d350120375f7e8f82a56e4e96bc7279d10e3fd
# Parent  34148c2dbe72c092e6add2f6b03fe6e4273bbc0f
Bug 1390386 - Use uintptr for the thread-local. - r=daoshengmu

We're just doing a value comparison, so there's no reason this needs to be a pointer.

MozReview-Commit-ID: 6ck2s2fsdq5

diff --git a/gfx/gl/GLContext.cpp b/gfx/gl/GLContext.cpp
--- a/gfx/gl/GLContext.cpp
+++ b/gfx/gl/GLContext.cpp
@@ -55,17 +55,17 @@
 #endif
 
 namespace mozilla {
 namespace gl {
 
 using namespace mozilla::gfx;
 using namespace mozilla::layers;
 
-MOZ_THREAD_LOCAL(const GLContext*) GLContext::sCurrentContext;
+MOZ_THREAD_LOCAL(uintptr_t) GLContext::sCurrentContext;
 
 // If adding defines, don't forget to undefine symbols. See #undef block below.
 #define CORE_SYMBOL(x) { (PRFuncPtr*) &mSymbols.f##x, { #x, nullptr } }
 #define CORE_EXT_SYMBOL2(x,y,z) { (PRFuncPtr*) &mSymbols.f##x, { #x, #x #y, #x #z, nullptr } }
 #define EXT_SYMBOL2(x,y,z) { (PRFuncPtr*) &mSymbols.f##x, { #x #y, #x #z, nullptr } }
 #define EXT_SYMBOL3(x,y,z,w) { (PRFuncPtr*) &mSymbols.f##x, { #x #y, #x #z, #x #w, nullptr } }
 #define END_SYMBOLS { nullptr, { nullptr } }
 
@@ -291,17 +291,17 @@ GLContext::GLContext(CreateContextFlags 
     mWorkAroundDriverBugs(true),
     mSyncGLCallCount(0),
     mHeavyGLCallsSinceLastFlush(false)
 {
     mMaxViewportDims[0] = 0;
     mMaxViewportDims[1] = 0;
     mOwningThreadId = PlatformThread::CurrentId();
     MOZ_ALWAYS_TRUE( sCurrentContext.init() );
-    sCurrentContext.set(nullptr);
+    sCurrentContext.set(0);
 }
 
 GLContext::~GLContext() {
     NS_ASSERTION(IsDestroyed(), "GLContext implementation must call MarkDestroyed in destructor!");
 #ifdef MOZ_GL_DEBUG
     if (mSharedContext) {
         GLContext* tip = mSharedContext;
         while (tip->mSharedContext)
@@ -3017,31 +3017,31 @@ bool
 GLContext::MakeCurrent(bool aForce) const
 {
     if (MOZ_UNLIKELY( IsDestroyed() ))
         return false;
 
     if (MOZ_LIKELY( !aForce )) {
         bool isCurrent;
         if (mUseTLSIsCurrent) {
-            isCurrent = (sCurrentContext.get() == this);
+            isCurrent = (sCurrentContext.get() == reinterpret_cast<uintptr_t>(this));
         } else {
             isCurrent = IsCurrentImpl();
         }
         if (MOZ_LIKELY( isCurrent )) {
             MOZ_ASSERT(IsCurrentImpl());
             return true;
         }
     }
 
     if (!MakeCurrentImpl())
         return false;
 
     if (mUseTLSIsCurrent) {
-        sCurrentContext.set(this);
+        sCurrentContext.set(reinterpret_cast<uintptr_t>(this));
     }
     return true;
 }
 
 void
 GLContext::ResetSyncCallCount(const char* resetReason) const
 {
     if (ShouldSpew()) {
diff --git a/gfx/gl/GLContext.h b/gfx/gl/GLContext.h
--- a/gfx/gl/GLContext.h
+++ b/gfx/gl/GLContext.h
@@ -192,17 +192,17 @@ enum class GLRenderer {
 
 class GLContext
     : public GLLibraryLoader
     , public GenericAtomicRefCounted
     , public SupportsWeakPtr<GLContext>
 {
 public:
     MOZ_DECLARE_WEAKREFERENCE_TYPENAME(GLContext)
-    static MOZ_THREAD_LOCAL(const GLContext*) sCurrentContext;
+    static MOZ_THREAD_LOCAL(uintptr_t) sCurrentContext;
 
     bool mImplicitMakeCurrent;
 
 // -----------------------------------------------------------------------------
 // basic getters
 public:
 
     /**
