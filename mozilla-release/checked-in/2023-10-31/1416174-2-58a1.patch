# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1510338449 -3600
#      Fri Nov 10 19:27:29 2017 +0100
# Node ID 9b1d10a8644bd66cf35e7994b9772d6f89613d37
# Parent  e7c3ddfd5f5d70a46f53875977ee40652b2cd9f2
Bug 1416174 - part 2 - OSFileConstants as nsIObserver, r=smaug

diff --git a/dom/system/OSFileConstants.cpp b/dom/system/OSFileConstants.cpp
--- a/dom/system/OSFileConstants.cpp
+++ b/dom/system/OSFileConstants.cpp
@@ -209,33 +209,23 @@ nsresult GetPathToSpecialDir(const char 
 
 /**
  * In some cases, OSFileConstants may be instantiated before the
  * profile is setup. In such cases, |OS.Constants.Path.profileDir| and
  * |OS.Constants.Path.localProfileDir| are undefined. However, we want
  * to ensure that this does not break existing code, so that future
  * workers spawned after the profile is setup have these constants.
  *
- * For this purpose, we register an observer to set |gPaths->profileDir|
- * and |gPaths->localProfileDir| once the profile is setup.
+ * For this purpose, we register an observer to set |mPaths->profileDir|
+ * and |mPaths->localProfileDir| once the profile is setup.
  */
-class DelayedPathSetter final: public nsIObserver
-{
-  ~DelayedPathSetter() {}
-
-  NS_DECL_ISUPPORTS
-  NS_DECL_NSIOBSERVER
-
-  DelayedPathSetter() {}
-};
-
-NS_IMPL_ISUPPORTS(DelayedPathSetter, nsIObserver)
-
 NS_IMETHODIMP
-DelayedPathSetter::Observe(nsISupports*, const char * aTopic, const char16_t*)
+OSFileConstantsService::Observe(nsISupports*,
+                                const char* aTopic,
+                                const char16_t*)
 {
   if (gPaths == nullptr) {
     // Initialization of gPaths has not taken place, something is wrong,
     // don't make things worse.
     return NS_OK;
   }
   nsresult rv = GetPathToSpecialDir(NS_APP_USER_PROFILE_50_DIR, gPaths->profileDir);
   if (NS_FAILED(rv)) {
@@ -291,18 +281,17 @@ OSFileConstantsService::InitOSFileConsta
 
   // Otherwise, delay setup of profileDir/localProfileDir until they
   // become available.
   if (NS_FAILED(rv)) {
     nsCOMPtr<nsIObserverService> obsService = do_GetService(NS_OBSERVERSERVICE_CONTRACTID, &rv);
     if (NS_FAILED(rv)) {
       return rv;
     }
-    RefPtr<DelayedPathSetter> pathSetter = new DelayedPathSetter();
-    rv = obsService->AddObserver(pathSetter, "profile-do-change", false);
+    rv = obsService->AddObserver(this, "profile-do-change", false);
     if (NS_FAILED(rv)) {
       return rv;
     }
   }
 
   // For other directories, ignore errors (they may be undefined on
   // some platforms or in non-Firefox embeddings of Gecko).
 
@@ -1054,17 +1043,18 @@ OSFileConstantsService::DefineOSFileCons
 
   if (!SetStringProperty(aCx, objPath, "libsqlite3", libsqlite3)) {
     return false;
   }
 
   return true;
 }
 
-NS_IMPL_ISUPPORTS(OSFileConstantsService, nsIOSFileConstantsService)
+NS_IMPL_ISUPPORTS(OSFileConstantsService, nsIOSFileConstantsService,
+                  nsIObserver)
 
 /* static */ already_AddRefed<OSFileConstantsService>
 OSFileConstantsService::GetOrCreate()
 {
   if (!gInstance) {
     MOZ_ASSERT(NS_IsMainThread());
 
     RefPtr<OSFileConstantsService> service = new OSFileConstantsService();
diff --git a/dom/system/OSFileConstants.h b/dom/system/OSFileConstants.h
--- a/dom/system/OSFileConstants.h
+++ b/dom/system/OSFileConstants.h
@@ -2,31 +2,34 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_osfileconstants_h__
 #define mozilla_osfileconstants_h__
 
+#include "nsIObserver.h"
 #include "nsIOSFileConstantsService.h"
 #include "mozilla/Attributes.h"
 
 namespace mozilla {
 
 /**
  * XPConnect initializer, for use in the main thread.
  * This class is thread-safe but it must be first be initialized on the
  * main-thread.
  */
 class OSFileConstantsService final : public nsIOSFileConstantsService
+                                   , public nsIObserver
 {
- public:
+public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIOSFILECONSTANTSSERVICE
+  NS_DECL_NSIOBSERVER
 
   static already_AddRefed<OSFileConstantsService>
   GetOrCreate();
 
   bool
   DefineOSFileConstants(JSContext* aCx,
                         JS::Handle<JSObject*> aGlobal);
 
