# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1514908822 21600
# Node ID 030a95a498c6f77e34b1f2ded81924f98c6aff2e
# Parent  c2769abbf29ced42ce6a3673eb6101a41e30970c
servo: Merge #19673 - style: Make GetFlattenedTreeParent more straight-forward (from emilio:flattened-tree-parent); r=smaug

Now that accessing nsIContent slots is not a blob of virtual function calls, we
should be able to unify logic here, and speed up the not-so-rare case for
chrome, while keeping the usual case fast.

Bug: 1427511
Reviewed-by: smaug
MozReview-Commit-ID: 87iY5Cbhx4T
Source-Repo: https://github.com/servo/servo
Source-Revision: 9187c9a093860d9f3c31b5a5f402458aa4a607cb

diff --git a/servo/components/style/gecko/wrapper.rs b/servo/components/style/gecko/wrapper.rs
--- a/servo/components/style/gecko/wrapper.rs
+++ b/servo/components/style/gecko/wrapper.rs
@@ -231,23 +231,33 @@ impl<'ln> GeckoNode<'ln> {
             }
         }
 
         true
     }
 
     #[inline]
     fn flattened_tree_parent(&self) -> Option<Self> {
-        let fast_path = self.flattened_tree_parent_is_parent();
-        debug_assert!(fast_path == unsafe { bindings::Gecko_FlattenedTreeParentIsParent(self.0) });
-        if fast_path {
-            unsafe { self.0.mParent.as_ref().map(GeckoNode) }
-        } else {
-            unsafe { bindings::Gecko_GetFlattenedTreeParentNode(self.0).map(GeckoNode) }
+        // TODO(emilio): Measure and consider not doing this fast-path and take
+        // always the common path, it's only a function call and from profiles
+        // it seems that keeping this fast path makes the compiler not inline
+        // `flattened_tree_parent`.
+        if self.flattened_tree_parent_is_parent() {
+            debug_assert_eq!(
+                unsafe { bindings::Gecko_GetFlattenedTreeParentNode(self.0).map(GeckoNode) },
+                self.parent_node(),
+                "Fast path stopped holding!"
+            );
+
+            return self.parent_node();
         }
+
+        // NOTE(emilio): If this call is too expensive, we could manually
+        // inline more aggressively.
+        unsafe { bindings::Gecko_GetFlattenedTreeParentNode(self.0).map(GeckoNode) }
     }
 
     #[inline]
     fn contains_non_whitespace_content(&self) -> bool {
         unsafe { Gecko_IsSignificantChild(self.0, true, false) }
     }
 }
 
