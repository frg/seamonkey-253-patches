# HG changeset patch
# User Andreas Pehrson <pehrsons@mozilla.com>
# Date 1515411856 -3600
#      Mon Jan 08 12:44:16 2018 +0100
# Node ID c75357e076963d148e4c7453648438aa4e497a4c
# Parent  df5cd53793749e472272585eae53df02834c086d
Bug 1428390 - Remove virtual keyword for overrides in PCameras impl. r=jib

MozReview-Commit-ID: FqhgyTFXH5W

diff --git a/dom/media/systemservices/CamerasChild.h b/dom/media/systemservices/CamerasChild.h
--- a/dom/media/systemservices/CamerasChild.h
+++ b/dom/media/systemservices/CamerasChild.h
@@ -157,37 +157,37 @@ class CamerasChild final : public PCamer
 
 public:
   // We are owned by the PBackground thread only. CamerasSingleton
   // takes a non-owning reference.
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(CamerasChild)
 
   // IPC messages recevied, received on the PBackground thread
   // these are the actual callbacks with data
-  virtual mozilla::ipc::IPCResult RecvDeliverFrame(const CaptureEngine&, const int&,
-                                                   mozilla::ipc::Shmem&&,
-                                                   const VideoFrameProperties & prop) override;
-  virtual mozilla::ipc::IPCResult RecvFrameSizeChange(const CaptureEngine&, const int&,
-                                                      const int& w, const int& h) override;
+  mozilla::ipc::IPCResult RecvDeliverFrame(const CaptureEngine&, const int&,
+                                           mozilla::ipc::Shmem&&,
+                                           const VideoFrameProperties & prop) override;
+  mozilla::ipc::IPCResult RecvFrameSizeChange(const CaptureEngine&, const int&,
+                                              const int& w, const int& h) override;
 
-  virtual mozilla::ipc::IPCResult RecvDeviceChange() override;
-  virtual int AddDeviceChangeCallback(DeviceChangeCallback* aCallback) override;
+  mozilla::ipc::IPCResult RecvDeviceChange() override;
+  int AddDeviceChangeCallback(DeviceChangeCallback* aCallback) override;
   int SetFakeDeviceChangeEvents();
 
   // these are response messages to our outgoing requests
-  virtual mozilla::ipc::IPCResult RecvReplyNumberOfCaptureDevices(const int&) override;
-  virtual mozilla::ipc::IPCResult RecvReplyNumberOfCapabilities(const int&) override;
-  virtual mozilla::ipc::IPCResult RecvReplyAllocateCaptureDevice(const int&) override;
-  virtual mozilla::ipc::IPCResult RecvReplyGetCaptureCapability(const VideoCaptureCapability& capability) override;
-  virtual mozilla::ipc::IPCResult RecvReplyGetCaptureDevice(const nsCString& device_name,
+  mozilla::ipc::IPCResult RecvReplyNumberOfCaptureDevices(const int&) override;
+  mozilla::ipc::IPCResult RecvReplyNumberOfCapabilities(const int&) override;
+  mozilla::ipc::IPCResult RecvReplyAllocateCaptureDevice(const int&) override;
+  mozilla::ipc::IPCResult RecvReplyGetCaptureCapability(const VideoCaptureCapability& capability) override;
+  mozilla::ipc::IPCResult RecvReplyGetCaptureDevice(const nsCString& device_name,
                                                             const nsCString& device_id,
                                                             const bool& scary) override;
-  virtual mozilla::ipc::IPCResult RecvReplyFailure(void) override;
-  virtual mozilla::ipc::IPCResult RecvReplySuccess(void) override;
-  virtual void ActorDestroy(ActorDestroyReason aWhy) override;
+  mozilla::ipc::IPCResult RecvReplyFailure(void) override;
+  mozilla::ipc::IPCResult RecvReplySuccess(void) override;
+  void ActorDestroy(ActorDestroyReason aWhy) override;
 
   // the webrtc.org ViECapture calls are mirrored here, but with access
   // to a specific PCameras instance to communicate over. These also
   // run on the MediaManager thread
   int NumberOfCaptureDevices(CaptureEngine aCapEngine);
   int NumberOfCapabilities(CaptureEngine aCapEngine,
                            const char* deviceUniqueIdUTF8);
   int ReleaseCaptureDevice(CaptureEngine aCapEngine,
diff --git a/dom/media/systemservices/CamerasParent.h b/dom/media/systemservices/CamerasParent.h
--- a/dom/media/systemservices/CamerasParent.h
+++ b/dom/media/systemservices/CamerasParent.h
@@ -41,17 +41,17 @@ class CallbackHelper :
   public rtc::VideoSinkInterface<webrtc::VideoFrame>
 {
 public:
   CallbackHelper(CaptureEngine aCapEng, uint32_t aStreamId, CamerasParent *aParent)
     : mCapEngine(aCapEng), mStreamId(aStreamId), mParent(aParent) {};
 
   // These callbacks end up running on the VideoCapture thread.
   // From  VideoCaptureCallback
-  virtual void OnFrame(const webrtc::VideoFrame& videoFrame) override;
+  void OnFrame(const webrtc::VideoFrame& videoFrame) override;
 
   friend CamerasParent;
 
 private:
   CaptureEngine mCapEngine;
   uint32_t mStreamId;
   CamerasParent *mParent;
 };
@@ -79,35 +79,35 @@ class CamerasParent :  public PCamerasPa
 {
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIOBSERVER
 
 public:
   static already_AddRefed<CamerasParent> Create();
 
   // Messages received form the child. These run on the IPC/PBackground thread.
-  virtual mozilla::ipc::IPCResult
+  mozilla::ipc::IPCResult
   RecvAllocateCaptureDevice(const CaptureEngine& aEngine,
                             const nsCString& aUnique_idUTF8,
                             const ipc::PrincipalInfo& aPrincipalInfo) override;
-  virtual mozilla::ipc::IPCResult RecvReleaseCaptureDevice(const CaptureEngine&,
-                                                           const int&) override;
-  virtual mozilla::ipc::IPCResult RecvNumberOfCaptureDevices(const CaptureEngine&) override;
-  virtual mozilla::ipc::IPCResult RecvNumberOfCapabilities(const CaptureEngine&,
-                                                           const nsCString&) override;
-  virtual mozilla::ipc::IPCResult RecvGetCaptureCapability(const CaptureEngine&, const nsCString&,
-                                                           const int&) override;
-  virtual mozilla::ipc::IPCResult RecvGetCaptureDevice(const CaptureEngine&, const int&) override;
-  virtual mozilla::ipc::IPCResult RecvStartCapture(const CaptureEngine&, const int&,
-                                                   const VideoCaptureCapability&) override;
-  virtual mozilla::ipc::IPCResult RecvStopCapture(const CaptureEngine&, const int&) override;
-  virtual mozilla::ipc::IPCResult RecvReleaseFrame(mozilla::ipc::Shmem&&) override;
-  virtual mozilla::ipc::IPCResult RecvAllDone() override;
-  virtual void ActorDestroy(ActorDestroyReason aWhy) override;
-  virtual mozilla::ipc::IPCResult RecvEnsureInitialized(const CaptureEngine&) override;
+  mozilla::ipc::IPCResult RecvReleaseCaptureDevice(const CaptureEngine&,
+                                                   const int&) override;
+  mozilla::ipc::IPCResult RecvNumberOfCaptureDevices(const CaptureEngine&) override;
+  mozilla::ipc::IPCResult RecvNumberOfCapabilities(const CaptureEngine&,
+                                                   const nsCString&) override;
+  mozilla::ipc::IPCResult RecvGetCaptureCapability(const CaptureEngine&, const nsCString&,
+                                                   const int&) override;
+  mozilla::ipc::IPCResult RecvGetCaptureDevice(const CaptureEngine&, const int&) override;
+  mozilla::ipc::IPCResult RecvStartCapture(const CaptureEngine&, const int&,
+                                           const VideoCaptureCapability&) override;
+  mozilla::ipc::IPCResult RecvStopCapture(const CaptureEngine&, const int&) override;
+  mozilla::ipc::IPCResult RecvReleaseFrame(mozilla::ipc::Shmem&&) override;
+  mozilla::ipc::IPCResult RecvAllDone() override;
+  void ActorDestroy(ActorDestroyReason aWhy) override;
+  mozilla::ipc::IPCResult RecvEnsureInitialized(const CaptureEngine&) override;
 
   nsIEventTarget* GetBackgroundEventTarget() { return mPBackgroundEventTarget; };
   bool IsShuttingDown()
   {
     return !mChildIsAlive || mDestroyed || !mWebRTCAlive;
   };
   ShmemBuffer GetBuffer(size_t aSize);
 
