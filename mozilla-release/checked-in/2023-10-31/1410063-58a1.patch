# HG changeset patch
# User Valentin Gosu <valentin.gosu@gmail.com>
# Date 1508945747 -7200
# Node ID 44a8f978d20803f39be701b2347d3d02096d8a37
# Parent  33324e0e742745309715f1c8710170c6cd149918
Bug 1410063 - Implement MozURL::GetHostPort/SetHostPort r=mayhemer

MozReview-Commit-ID: 2wXsvmHf60

diff --git a/netwerk/base/MozURL.cpp b/netwerk/base/MozURL.cpp
--- a/netwerk/base/MozURL.cpp
+++ b/netwerk/base/MozURL.cpp
@@ -49,16 +49,38 @@ MozURL::GetPassword(nsACString& aPasswor
 
 nsresult
 MozURL::GetHostname(nsACString& aHost)
 {
   return rusturl_get_host(mURL.get(), &aHost);
 }
 
 nsresult
+MozURL::GetHostPort(nsACString& aHostPort)
+{
+  nsresult rv = rusturl_get_host(mURL.get(), &aHostPort);
+  if (NS_FAILED(rv)) {
+    return rv;
+  }
+
+  int32_t port;
+  rv = GetPort(&port);
+  if (NS_FAILED(rv)) {
+    aHostPort.Truncate();
+    return rv;
+  }
+  if (port != -1) {
+    aHostPort.AppendLiteral(":");
+    aHostPort.AppendInt(port);
+  }
+
+  return NS_OK;
+}
+
+nsresult
 MozURL::GetPort(int32_t* aPort)
 {
   return rusturl_get_port(mURL.get(), aPort);
 }
 
 nsresult
 MozURL::GetFilePath(nsACString& aPath)
 {
@@ -134,16 +156,24 @@ MozURL::Mutator&
 MozURL::Mutator::SetHostname(const nsACString& aHost)
 {
   ENSURE_VALID();
   mStatus = rusturl_set_host(mURL.get(), &aHost);
   return *this;
 }
 
 MozURL::Mutator&
+MozURL::Mutator::SetHostPort(const nsACString& aHostPort)
+{
+  ENSURE_VALID();
+  mStatus = rusturl_set_host_port(mURL.get(), &aHostPort);
+  return *this;
+}
+
+MozURL::Mutator&
 MozURL::Mutator::SetFilePath(const nsACString& aPath)
 {
   ENSURE_VALID();
   mStatus = rusturl_set_path(mURL.get(), &aPath);
   return *this;
 }
 
 MozURL::Mutator&
diff --git a/netwerk/base/MozURL.h b/netwerk/base/MozURL.h
--- a/netwerk/base/MozURL.h
+++ b/netwerk/base/MozURL.h
@@ -38,16 +38,20 @@ public:
 
   nsresult GetScheme(nsACString& aScheme);
   nsresult GetSpec(nsACString& aSpec);
   nsresult GetUsername(nsACString& aUser);
   nsresult GetPassword(nsACString& aPassword);
   // Will return the hostname of URL. If the hostname is an IPv6 address,
   // it will be enclosed in square brackets, such as `[::1]`
   nsresult GetHostname(nsACString& aHost);
+  // If the URL's port number is equal to the default port, will only return the
+  // hostname, otherwise it will return a string of the form `{host}:{port}`
+  // See: https://url.spec.whatwg.org/#default-port
+  nsresult GetHostPort(nsACString& aHostPort);
   // Will return the port number, if specified, or -1
   nsresult GetPort(int32_t* aPort);
   nsresult GetFilePath(nsACString& aPath);
   nsresult GetQuery(nsACString& aQuery);
   nsresult GetRef(nsACString& aRef);
 
 private:
   explicit MozURL(rusturl* rawPtr)
@@ -79,16 +83,17 @@ public:
     // nsresult rv = url->Mutate().SetHostname(NS_LITERAL_CSTRING("newhost"))
     //                            .SetFilePath(NS_LITERAL_CSTRING("new/file/path"))
     //                            .Finalize(getter_AddRefs(url2));
     // if (NS_SUCCEEDED(rv)) { /* use url2 */ }
     Mutator& SetScheme(const nsACString& aScheme);
     Mutator& SetUsername(const nsACString& aUser);
     Mutator& SetPassword(const nsACString& aPassword);
     Mutator& SetHostname(const nsACString& aHost);
+    Mutator& SetHostPort(const nsACString& aHostPort);
     Mutator& SetFilePath(const nsACString& aPath);
     Mutator& SetQuery(const nsACString& aQuery);
     Mutator& SetRef(const nsACString& aRef);
     Mutator& SetPort(int32_t aPort);
 
     // This method returns the status code of the setter operations.
     // If any of the setters failed, it will return the code of the first error
     // that occured. If none of the setters failed, it will return NS_OK.
diff --git a/netwerk/test/gtest/TestMozURL.cpp b/netwerk/test/gtest/TestMozURL.cpp
--- a/netwerk/test/gtest/TestMozURL.cpp
+++ b/netwerk/test/gtest/TestMozURL.cpp
@@ -114,8 +114,37 @@ TEST(TestMozURL, InitWithBase)
 
   RefPtr<MozURL> url2;
   ASSERT_EQ(MozURL::Init(getter_AddRefs(url2), NS_LITERAL_CSTRING("c.png"),
                          url), NS_OK);
 
   ASSERT_EQ(url2->GetSpec(out), NS_OK);
   ASSERT_TRUE(out.EqualsLiteral("https://example.net/a/c.png"));
 }
+
+TEST(TestMozURL, HostPort)
+{
+  nsAutoCString href("https://user:pass@example.net:1234/path?query#ref");
+  RefPtr<MozURL> url;
+  ASSERT_EQ(MozURL::Init(getter_AddRefs(url), href), NS_OK);
+  nsAutoCString out;
+
+  ASSERT_EQ(url->GetHostPort(out), NS_OK);
+  ASSERT_TRUE(out.EqualsLiteral("example.net:1234"));
+
+  RefPtr<MozURL> url2;
+  url->Mutate().SetHostPort(NS_LITERAL_CSTRING("test:321"))
+               .Finalize(getter_AddRefs(url2));
+
+  ASSERT_EQ(url2->GetHostPort(out), NS_OK);
+  ASSERT_TRUE(out.EqualsLiteral("test:321"));
+  ASSERT_EQ(url2->GetSpec(out), NS_OK);
+  ASSERT_TRUE(out.EqualsLiteral("https://user:pass@test:321/path?query#ref"));
+
+  href.Assign("https://user:pass@example.net:443/path?query#ref");
+  ASSERT_EQ(MozURL::Init(getter_AddRefs(url), href), NS_OK);
+  ASSERT_EQ(url->GetHostPort(out), NS_OK);
+  ASSERT_TRUE(out.EqualsLiteral("example.net"));
+  int32_t port;
+  ASSERT_EQ(url->GetPort(&port), NS_OK);
+  ASSERT_EQ(port, -1);
+}
+
