# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1510538772 21600
# Node ID b7ebbb451e5a493673f89edb06307caefed0833a
# Parent  cedf83e98855bf678cfa40c47f39fad175c6c7f4
servo: Merge #19194 - stylo: Bring back support for calc() in media-queries (from emilio:media-query-calc); r=Manishearth

Bug: 1396057
Reviewed-by: Manishearth
Source-Repo: https://github.com/servo/servo
Source-Revision: 4970b5d1544fe8f33287b01540e972a639baa689

diff --git a/servo/components/style/gecko/media_queries.rs b/servo/components/style/gecko/media_queries.rs
--- a/servo/components/style/gecko/media_queries.rs
+++ b/servo/components/style/gecko/media_queries.rs
@@ -14,31 +14,31 @@ use gecko::values::{convert_nscolor_to_r
 use gecko_bindings::bindings;
 use gecko_bindings::structs;
 use gecko_bindings::structs::{nsCSSKeyword, nsCSSProps_KTableEntry, nsCSSValue, nsCSSUnit};
 use gecko_bindings::structs::{nsMediaExpression_Range, nsMediaFeature};
 use gecko_bindings::structs::{nsMediaFeature_ValueType, nsMediaFeature_RangeType};
 use gecko_bindings::structs::{nsPresContext, RawGeckoPresContextOwned};
 use gecko_bindings::structs::nsIAtom;
 use media_queries::MediaType;
-use parser::ParserContext;
+use parser::{Parse, ParserContext};
 use properties::ComputedValues;
 use servo_arc::Arc;
 use std::fmt::{self, Write};
 use std::sync::atomic::{AtomicBool, AtomicIsize, AtomicUsize, Ordering};
 use str::starts_with_ignore_ascii_case;
 use string_cache::Atom;
 use style_traits::{CSSPixel, DevicePixel};
 use style_traits::{ToCss, ParseError, StyleParseErrorKind};
 use style_traits::viewport::ViewportConstraints;
 use stylesheets::Origin;
 use values::{CSSFloat, CustomIdent};
 use values::computed::{self, ToComputedValue};
 use values::computed::font::FontSize;
-use values::specified::Length;
+use values::specified::{Integer, Length, Number};
 
 /// The `Device` in Gecko wraps a pres context, has a default values computed,
 /// and contains all the viewport rule state.
 pub struct Device {
     /// NB: The pres context lifetime is tied to the styleset, who owns the
     /// stylist, and thus the `Device`, so having a raw pres context pointer
     /// here is fine.
     pres_context: RawGeckoPresContextOwned,
@@ -303,16 +303,23 @@ impl Resolution {
             "dppx" => Ok(Resolution::Dppx(value)),
             "dpcm" => Ok(Resolution::Dpcm(value)),
             _ => Err(())
         }).map_err(|()| location.new_custom_error(StyleParseErrorKind::UnexpectedDimension(unit.clone())))
     }
 }
 
 /// A value found or expected in a media expression.
+///
+/// FIXME(emilio): How should calc() serialize in the Number / Integer /
+/// BoolInteger / IntRatio case, as computed or as specified value?
+///
+/// If the first, this would need to store the relevant values.
+///
+/// See: https://github.com/w3c/csswg-drafts/issues/1968
 #[derive(Clone, Debug, PartialEq)]
 pub enum MediaExpressionValue {
     /// A length.
     Length(Length),
     /// A (non-negative) integer.
     Integer(u32),
     /// A floating point value.
     Float(CSSFloat),
@@ -435,34 +442,37 @@ impl MediaExpressionValue {
 
                 dest.write_str(string)
             }
         }
     }
 }
 
 fn find_feature<F>(mut f: F) -> Option<&'static nsMediaFeature>
-    where F: FnMut(&'static nsMediaFeature) -> bool,
+where
+    F: FnMut(&'static nsMediaFeature) -> bool,
 {
     unsafe {
         let mut features = structs::nsMediaFeatures_features.as_ptr();
         while !(*features).mName.is_null() {
             if f(&*features) {
                 return Some(&*features);
             }
             features = features.offset(1);
         }
     }
     None
 }
 
-unsafe fn find_in_table<F>(mut current_entry: *const nsCSSProps_KTableEntry,
-                           mut f: F)
-                           -> Option<(nsCSSKeyword, i16)>
-    where F: FnMut(nsCSSKeyword, i16) -> bool
+unsafe fn find_in_table<F>(
+    mut current_entry: *const nsCSSProps_KTableEntry,
+    mut f: F,
+) -> Option<(nsCSSKeyword, i16)>
+where
+    F: FnMut(nsCSSKeyword, i16) -> bool
 {
     loop {
         let value = (*current_entry).mValue;
         let keyword = (*current_entry).mKeyword;
 
         if value == -1 {
             return None; // End of the table.
         }
@@ -470,76 +480,60 @@ unsafe fn find_in_table<F>(mut current_e
         if f(keyword, value) {
             return Some((keyword, value));
         }
 
         current_entry = current_entry.offset(1);
     }
 }
 
-fn parse_feature_value<'i, 't>(feature: &nsMediaFeature,
-                               feature_value_type: nsMediaFeature_ValueType,
-                               context: &ParserContext,
-                               input: &mut Parser<'i, 't>)
-                               -> Result<MediaExpressionValue, ParseError<'i>> {
+fn parse_feature_value<'i, 't>(
+    feature: &nsMediaFeature,
+    feature_value_type: nsMediaFeature_ValueType,
+    context: &ParserContext,
+    input: &mut Parser<'i, 't>,
+) -> Result<MediaExpressionValue, ParseError<'i>> {
     let value = match feature_value_type {
         nsMediaFeature_ValueType::eLength => {
-           let length = Length::parse_non_negative(context, input)?;
-           // FIXME(canaltinova): See bug 1396057. Gecko doesn't support calc
-           // inside media queries. This check is for temporarily remove it
-           // for parity with gecko. We should remove this check when we want
-           // to support it.
-           if let Length::Calc(_) = length {
-               return Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
-           }
-           MediaExpressionValue::Length(length)
+            let length = Length::parse_non_negative(context, input)?;
+            MediaExpressionValue::Length(length)
         },
         nsMediaFeature_ValueType::eInteger => {
-           // FIXME(emilio): We should use `Integer::parse` to handle `calc`
-           // properly in integer expressions. Note that calc is still not
-           // supported in media queries per FIXME above.
-           let i = input.expect_integer()?;
-           if i < 0 {
-               return Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
-           }
-           MediaExpressionValue::Integer(i as u32)
+            let integer = Integer::parse_non_negative(context, input)?;
+            MediaExpressionValue::Integer(integer.value() as u32)
         }
         nsMediaFeature_ValueType::eBoolInteger => {
-           let i = input.expect_integer()?;
-           if i < 0 || i > 1 {
-               return Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
-           }
-           MediaExpressionValue::BoolInteger(i == 1)
+            let integer = Integer::parse_non_negative(context, input)?;
+            let value = integer.value();
+            if value > 1 {
+                return Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
+            }
+            MediaExpressionValue::BoolInteger(value == 1)
         }
         nsMediaFeature_ValueType::eFloat => {
-           MediaExpressionValue::Float(input.expect_number()?)
+            let number = Number::parse(context, input)?;
+            MediaExpressionValue::Float(number.get())
         }
         nsMediaFeature_ValueType::eIntRatio => {
-           let a = input.expect_integer()?;
-           if a <= 0 {
-               return Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
-           }
-
-           input.expect_delim('/')?;
-
-           let b = input.expect_integer()?;
-           if b <= 0 {
-               return Err(input.new_custom_error(StyleParseErrorKind::UnspecifiedError))
-           }
-           MediaExpressionValue::IntRatio(a as u32, b as u32)
+            let a = Integer::parse_positive(context, input)?;
+            input.expect_delim('/')?;
+            let b = Integer::parse_positive(context, input)?;
+            MediaExpressionValue::IntRatio(a.value() as u32, b.value() as u32)
         }
         nsMediaFeature_ValueType::eResolution => {
-           MediaExpressionValue::Resolution(Resolution::parse(input)?)
+            MediaExpressionValue::Resolution(Resolution::parse(input)?)
         }
         nsMediaFeature_ValueType::eEnumerated => {
             let location = input.current_source_location();
             let keyword = input.expect_ident()?;
             let keyword = unsafe {
-                bindings::Gecko_LookupCSSKeyword(keyword.as_bytes().as_ptr(),
-                keyword.len() as u32)
+                bindings::Gecko_LookupCSSKeyword(
+                    keyword.as_bytes().as_ptr(),
+                    keyword.len() as u32,
+                )
             };
 
             let first_table_entry: *const nsCSSProps_KTableEntry = unsafe {
                 *feature.mData.mKeywordTable.as_ref()
             };
 
             let value = match unsafe { find_in_table(first_table_entry, |kw, _| kw == keyword) } {
                 Some((_kw, value)) => value,
@@ -553,24 +547,22 @@ fn parse_feature_value<'i, 't>(feature: 
         }
     };
 
     Ok(value)
 }
 
 impl Expression {
     /// Trivially construct a new expression.
-    fn new(feature: &'static nsMediaFeature,
-           value: Option<MediaExpressionValue>,
-           range: nsMediaExpression_Range) -> Self {
-        Expression {
-            feature: feature,
-            value: value,
-            range: range,
-        }
+    fn new(
+        feature: &'static nsMediaFeature,
+        value: Option<MediaExpressionValue>,
+        range: nsMediaExpression_Range,
+    ) -> Self {
+        Self { feature, value, range }
     }
 
     /// Parse a media expression of the form:
     ///
     /// ```
     /// (media-feature: media-value)
     /// ```
     pub fn parse<'i, 't>(
diff --git a/servo/components/style/values/specified/length.rs b/servo/components/style/values/specified/length.rs
--- a/servo/components/style/values/specified/length.rs
+++ b/servo/components/style/values/specified/length.rs
@@ -181,17 +181,17 @@ impl FontRelativeLength {
             }
             FontRelativeLength::Rem(length) => {
                 // https://drafts.csswg.org/css-values/#rem:
                 //
                 //     When specified on the font-size property of the root
                 //     element, the rem units refer to the property’s initial
                 //     value.
                 //
-                let reference_size = if context.is_root_element {
+                let reference_size = if context.is_root_element || context.in_media_query {
                     reference_font_size
                 } else {
                     context.device().root_font_size()
                 };
                 (reference_size, length)
             }
         }
     }
