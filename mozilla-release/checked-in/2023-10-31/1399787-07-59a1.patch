# HG changeset patch
# User cku <cku@mozilla.com>
# Date 1511946931 -28800
# Node ID 5006b383bddb29156e6141e0e8d0ead036239be8
# Parent  9d8fac8eed5c821f72fa9e5bbd9ab5c0f7538e9d
Bug 1399787 - Part 7. Have PrintTargetEMF launch the PDFium process. r=jwatt

MozReview-Commit-ID: CVfB81ifV2p

diff --git a/gfx/thebes/PrintTargetEMF.cpp b/gfx/thebes/PrintTargetEMF.cpp
--- a/gfx/thebes/PrintTargetEMF.cpp
+++ b/gfx/thebes/PrintTargetEMF.cpp
@@ -1,28 +1,37 @@
 /* -*- Mode: C++; tab-width: 20; indent-tabs-mode: nil; c-basic-offset: 4 -*-
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "PrintTargetEMF.h"
 #include "nsAnonymousTemporaryFile.h"
 #include "nsIFile.h"
+#include "mozilla/widget/PDFiumProcessParent.h"
 
 using mozilla::gfx::DrawTarget;
 
 namespace mozilla {
 namespace gfx {
 
 PrintTargetEMF::PrintTargetEMF(HDC aDC, const IntSize& aSize)
   : PrintTarget(/* not using cairo_surface_t */ nullptr, aSize)
+  , mPDFiumProcess(nullptr)
   , mPrinterDC(aDC)
 {
 }
 
+PrintTargetEMF::~PrintTargetEMF()
+{
+  if (mPDFiumProcess) {
+    mPDFiumProcess->Delete();
+  }
+}
+
 /* static */ already_AddRefed<PrintTargetEMF>
 PrintTargetEMF::CreateOrNull(HDC aDC, const IntSize& aSizeInPoints)
 {
   return do_AddRef(new PrintTargetEMF(aDC, aSizeInPoints));
 }
 
 nsresult
 PrintTargetEMF::BeginPrinting(const nsAString& aTitle,
@@ -46,16 +55,19 @@ PrintTargetEMF::BeginPrinting(const nsAS
   docinfo.cbSize = sizeof(docinfo);
   docinfo.lpszDocName = titleStr.Length() > 0 ? titleStr.get() : L"Mozilla Document";
   docinfo.lpszOutput = docName.Length() > 0 ? docName.get() : nullptr;
   docinfo.lpszDatatype = nullptr;
   docinfo.fwType = 0;
 
   ::StartDocW(mPrinterDC, &docinfo);
 
+  mPDFiumProcess = new PDFiumProcessParent();
+  NS_ENSURE_TRUE(mPDFiumProcess->Launch(), NS_ERROR_FAILURE);
+
   return NS_OK;
 }
 
 nsresult
 PrintTargetEMF::EndPrinting()
 {
   return (::EndDoc(mPrinterDC) <= 0) ? NS_ERROR_FAILURE : NS_OK;
 }
diff --git a/gfx/thebes/PrintTargetEMF.h b/gfx/thebes/PrintTargetEMF.h
--- a/gfx/thebes/PrintTargetEMF.h
+++ b/gfx/thebes/PrintTargetEMF.h
@@ -7,30 +7,37 @@
 #define MOZILLA_GFX_PRINTTARGETEMF_H
 
 #include "PrintTargetSkPDF.h"
 
 /* include windows.h for the HDC definitions that we need. */
 #include <windows.h>
 
 namespace mozilla {
+namespace widget {
+  class PDFiumProcessParent;
+}
+}
+
+namespace mozilla {
 namespace gfx {
 
 /**
  * A new subclass of PrintTarget.
  * 1. It uses PrintTargetSkPDF to generate one PDF file for one page.
  * 2. It then passes the FileDescriptor of that generated PDF file to the
  *    PDFium process for EMF conversion.
  * 3. After getting the converted EMF contents from the PDFium process, it then
  *    draws it onto the printer DC to finish one page printing task.
  */
 class PrintTargetEMF final : public mozilla::gfx::PrintTarget
 {
 public:
   typedef gfx::IntSize IntSize;
+  typedef mozilla::widget::PDFiumProcessParent PDFiumProcessParent;
 
   static already_AddRefed<PrintTargetEMF>
   CreateOrNull(HDC aDC, const IntSize& aSizeInPoints);
 
   nsresult BeginPrinting(const nsAString& aTitle,
                                  const nsAString& aPrintToFileName,
                                  int32_t aStartPage,
                                  int32_t aEndPage) final;
@@ -43,20 +50,22 @@ public:
   MakeDrawTarget(const IntSize& aSize,
                  DrawEventRecorder* aRecorder = nullptr) final;
 
   already_AddRefed<DrawTarget>
   GetReferenceDrawTarget(DrawEventRecorder* aRecorder) final;
 
 private:
   PrintTargetEMF(HDC aDC, const IntSize& aSize);
+  ~PrintTargetEMF() override;
 
   nsString mTitle;
   RefPtr<PrintTargetSkPDF> mTargetForCurrentPage;
   nsCOMPtr<nsIFile>        mPDFFileForOnePage;
   RefPtr<PrintTargetSkPDF> mRefTarget;
+  PDFiumProcessParent*     mPDFiumProcess;
   HDC mPrinterDC;
 };
 
 } // namespace gfx
 } // namespace mozilla
 
 #endif /* MOZILLA_GFX_PRINTTARGETEMF_H */
diff --git a/widget/windows/moz.build b/widget/windows/moz.build
--- a/widget/windows/moz.build
+++ b/widget/windows/moz.build
@@ -22,16 +22,17 @@ EXPORTS.mozilla.widget += [
     'AudioSession.h',
     'CompositorWidgetChild.h',
     'CompositorWidgetParent.h',
     'InProcessWinCompositorWidget.h',
     'PDFiumChild.h',
     'PDFiumEngineShim.h',
     'PDFiumParent.h',
     'PDFiumProcessChild.h',
+    'PDFiumProcessParent.h',
     'PDFViaEMFPrintHelper.h',
     'WinCompositorWidget.h',
     'WinMessages.h',
     'WinModifierKeyState.h',
 ]
 
 UNIFIED_SOURCES += [
     'AudioSession.cpp',
