# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1513662383 -32400
# Node ID 5f4691a291cf811206ee77d40cc1477f54e2f084
# Parent  1a708423c324f5a035bccc9ec475b45b62013f93
Bug 1417504 - Also wrap Heap{Alloc,ReAlloc,Free} when building without our allocator. r=njn

Bug 1280578 added some wrapping for the Win32 Heap* functions, mainly
for the rust static libraries that use them. Because pointer ownership
might cross the C++/Rust boundary, and because about:memory uses
malloc_usable_size/msize, we need both C++ and Rust to still use the
same heap on builds where our allocator is not enabled.

diff --git a/memory/mozalloc/winheap.cpp b/memory/mozalloc/winheap.cpp
--- a/memory/mozalloc/winheap.cpp
+++ b/memory/mozalloc/winheap.cpp
@@ -17,16 +17,25 @@
 // See mozmemory_wrap.h for more details. This file is part of libmozglue, so
 // it needs to use _impl suffixes. However, with libmozglue growing, this is
 // becoming cumbersome, so we will likely use a malloc.h wrapper of some sort
 // and allow the use of the functions without a _impl suffix.
 #define MALLOC_DECL(name, return_type, ...) \
   MOZ_MEMORY_API return_type name ## _impl(__VA_ARGS__);
 #define MALLOC_FUNCS MALLOC_FUNCS_MALLOC
 #include "malloc_decls.h"
+#else
+
+#include <malloc.h>
+#define malloc_impl malloc
+#define calloc_impl calloc
+#define realloc_impl realloc
+#define free_impl free
+
+#endif
 
 // Warning: C4273: 'HeapAlloc': inconsistent dll linkage
 // The Windows headers define HeapAlloc as dllimport, but we define it as
 // dllexport, which is a voluntary inconsistency.
 #pragma warning(disable: 4273)
 
 MFBT_API
 LPVOID WINAPI HeapAlloc(_In_ HANDLE hHeap, _In_ DWORD dwFlags,
@@ -59,10 +68,8 @@ LPVOID WINAPI HeapReAlloc(_In_ HANDLE hH
 
 MFBT_API
 BOOL WINAPI HeapFree(_In_ HANDLE hHeap, _In_ DWORD dwFlags,
                      _In_ LPVOID lpMem)
 {
     free_impl(lpMem);
     return true;
 }
-
-#endif
