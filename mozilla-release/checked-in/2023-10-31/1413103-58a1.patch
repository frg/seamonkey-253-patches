# HG changeset patch
# User Sylvestre Ledru <sledru@mozilla.com>
# Date 1509435362 -3600
#      Tue Oct 31 08:36:02 2017 +0100
# Node ID ea7edb8d90e08de65388b7a30b2b2aeb78d36562
# Parent  c3028f6a16bf1fd327f5e93809435c770058c030
Bug 1413103 - nsEscape.cpp: disable the formatting of netCharType & EscapeChars r=erahm

MozReview-Commit-ID: GHM6EnO9S5h

diff --git a/xpcom/io/nsEscape.cpp b/xpcom/io/nsEscape.cpp
--- a/xpcom/io/nsEscape.cpp
+++ b/xpcom/io/nsEscape.cpp
@@ -13,41 +13,43 @@
 #include "nsCRT.h"
 #include "plstr.h"
 #include "nsASCIIMask.h"
 
 static const char hexCharsUpper[] = "0123456789ABCDEF";
 static const char hexCharsUpperLower[] = "0123456789ABCDEFabcdef";
 
 static const int netCharType[256] =
+// clang-format off
 /*  Bit 0       xalpha      -- the alphas
 **  Bit 1       xpalpha     -- as xalpha but
 **                             converts spaces to plus and plus to %2B
 **  Bit 3 ...   path        -- as xalphas but doesn't escape '/'
 */
   /* 0 1 2 3 4 5 6 7 8 9 A B C D E F */
   {  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   /* 0x */
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,   /* 1x */
      0,0,0,0,0,0,0,0,0,0,7,4,0,7,7,4,   /* 2x   !"#$%&'()*+,-./  */
      7,7,7,7,7,7,7,7,7,7,0,0,0,0,0,0,   /* 3x  0123456789:;<=>?  */
      0,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,   /* 4x  @ABCDEFGHIJKLMNO  */
      /* bits for '@' changed from 7 to 0 so '@' can be escaped   */
      /* in usernames and passwords in publishing.                */
      7,7,7,7,7,7,7,7,7,7,7,0,0,0,0,7,   /* 5X  PQRSTUVWXYZ[\]^_  */
      0,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,   /* 6x  `abcdefghijklmno  */
      7,7,7,7,7,7,7,7,7,7,7,0,0,0,0,0,   /* 7X  pqrstuvwxyz{\}~  DEL */
-     0, };
+     0,
+  };
 
 /* decode % escaped hex codes into character values
  */
 #define UNHEX(C) \
     ((C >= '0' && C <= '9') ? C - '0' : \
      ((C >= 'A' && C <= 'F') ? C - 'A' + 10 : \
      ((C >= 'a' && C <= 'f') ? C - 'a' + 10 : 0)))
-
+// clang-format on
 
 #define IS_OK(C) (netCharType[((unsigned int)(C))] & (aFlags))
 #define HEX_ESCAPE '%'
 
 static const uint32_t ENCODE_MAX_LEN = 6; // %uABCD
 
 static uint32_t
 AppendPercentHex(char* aBuffer, unsigned char aChar)
@@ -249,28 +251,30 @@ nsAppendEscapedHTML(const nsACString& aS
 // esc_Directory     =    16
 // esc_FileBaseName  =    32
 // esc_FileExtension =    64
 // esc_Param         =   128
 // esc_Query         =   256
 // esc_Ref           =   512
 
 static const uint32_t EscapeChars[256] =
+// clang-format off
 //   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
 {
      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 0x
      0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  // 1x
      0,1023,   0, 512,1023,   0,1023, 624,1023,1023,1023,1023,1023,1023, 953, 784,  // 2x   !"#$%&'()*+,-./
   1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1008,1008,   0,1008,   0, 768,  // 3x  0123456789:;<=>?
   1008,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,  // 4x  @ABCDEFGHIJKLMNO
   1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1008, 896,1008, 896,1023,  // 5x  PQRSTUVWXYZ[\]^_
    384,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,  // 6x  `abcdefghijklmno
   1023,1023,1023,1023,1023,1023,1023,1023,1023,1023,1023, 896,1012, 896,1023,   0,  // 7x  pqrstuvwxyz{|}~ DEL
      0                                                                              // 80 to FF are zero
 };
+// clang-format on
 
 static uint16_t dontNeedEscape(unsigned char aChar, uint32_t aFlags)
 {
   return EscapeChars[(uint32_t)aChar] & aFlags;
 }
 static uint16_t dontNeedEscape(uint16_t aChar, uint32_t aFlags)
 {
   return aChar < mozilla::ArrayLength(EscapeChars) ?
