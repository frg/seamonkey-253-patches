# HG changeset patch
# User Ho-Pang Hsu <hopang.hsu@gmail.com>
# Date 1494406056 -28800
#      Wed May 10 16:47:36 2017 +0800
# Node ID 97db331fc5ac194f00fe3dd16573f0f46ab6852c
# Parent  321836f84e28ef64abe7e45d6af46a450b1bde86
Bug 1359677 - P1: Add a new mochitest to verify the bypassing service worker. r=asuth

Since we decide to do this by setting the nsIChannel::LOAD_BYPASS_SERVICE_WORKER
to defaultLoadFlags in the docShell and the implement of the bypass service
worker flag is done, only having this test to verify the behavior. So that we
can simplely copy the code for setBypassSW() and getBypassSW() to dev-tools
server in the future when we decide the UI/UX and designs on dev-tools clients.

diff --git a/dom/workers/test/serviceworkers/mochitest.ini b/dom/workers/test/serviceworkers/mochitest.ini
--- a/dom/workers/test/serviceworkers/mochitest.ini
+++ b/dom/workers/test/serviceworkers/mochitest.ini
@@ -228,16 +228,17 @@ support-files =
 [test_bug1408734.html]
 [test_claim.html]
 [test_claim_oninstall.html]
 [test_controller.html]
 [test_cookie_fetch.html]
 [test_cross_origin_url_after_redirect.html]
 skip-if = debug # Bug 1262224
 [test_csp_upgrade-insecure_intercept.html]
+[test_devtools_bypass_serviceworker.html]
 [test_empty_serviceworker.html]
 [test_error_reporting.html]
 [test_escapedSlashes.html]
 [test_eval_allowed.html]
 [test_eventsource_intercept.html]
 [test_fetch_event.html]
 skip-if = (debug && e10s) # Bug 1262224
 [test_fetch_event_with_thirdpartypref.html]
diff --git a/dom/workers/test/serviceworkers/test_devtools_bypass_serviceworker.html b/dom/workers/test/serviceworkers/test_devtools_bypass_serviceworker.html
new file mode 100644
--- /dev/null
+++ b/dom/workers/test/serviceworkers/test_devtools_bypass_serviceworker.html
@@ -0,0 +1,118 @@
+<!DOCTYPE HTML>
+<html>
+<head>
+  <title> Verify devtools can utilize nsIChannel::LOAD_BYPASS_SERVICE_WORKER to bypass the service worker </title>
+  <script src="/tests/SimpleTest/SimpleTest.js"></script>
+  <script src="/tests/SimpleTest/SpawnTask.js"></script>
+  <script src="error_reporting_helpers.js"></script>
+  <link rel="stylesheet" href="/tests/SimpleTest/test.css"/>
+  <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
+</head>
+<body>
+<div id="content" style="display: none"></div>
+<script type="text/javascript">
+"use strict";
+
+async function testBypassSW () {
+  // Bypass SW imitates the "Disable Cache" option in dev-tools.
+  // Note: if we put the setter/getter into dev-tools, we should take care of
+  // the implementation of enabling/disabling cache since it just overwrite the
+  // defaultLoadFlags of docShell.
+  function setBypassServiceWorker(aDocShell, aBypass) {
+    if (aBypass) {
+      aDocShell.defaultLoadFlags |= Ci.nsIChannel.LOAD_BYPASS_SERVICE_WORKER;
+      return;
+    }
+
+    aDocShell.defaultLoadFlags &= ~Ci.nsIChannel.LOAD_BYPASS_SERVICE_WORKER;
+  }
+
+  function getBypassServiceWorker(aDocShell) {
+    return !!(aDocShell.defaultLoadFlags &
+              Ci.nsIChannel.LOAD_BYPASS_SERVICE_WORKER);
+  }
+
+  async function fetchFakeDocAndCheckIfIntercepted(aWindow) {
+    const fakeDoc = "fake.html";
+
+    // Note: The fetching document doesn't exist, so the expected status of the
+    //       repsonse is 404 unless the request is hijacked.
+    let response = await aWindow.fetch(fakeDoc);
+    if (response.status === 404) {
+      return false;
+    } else if (!response.ok) {
+      throw(response.statusText);
+    }
+
+    let text = await response.text();
+    if (text.includes("Hello")) {
+      // Intercepted
+      return true;
+    }
+
+    throw("Unexpected error");
+    return;
+  }
+
+  let Ci = SpecialPowers.Ci;
+  let docShell = SpecialPowers.wrap(window)
+                              .QueryInterface(Ci.nsIInterfaceRequestor)
+                              .getInterface(Ci.nsIWebNavigation)
+                              .QueryInterface(Ci.nsIDocShell);
+
+  info("Test 1: Enable bypass service worker for the docShell");
+
+  setBypassServiceWorker(docShell, true);
+  ok(getBypassServiceWorker(docShell),
+     "The loadFlags in docShell does bypass the serviceWorker by default");
+
+  let intercepted = await fetchFakeDocAndCheckIfIntercepted(window);
+  ok(!intercepted,
+     "The fetched document wasn't intercepted by the serviceWorker");
+
+  info("Test 2: Disable the bypass service worker for the docShell");
+
+  setBypassServiceWorker(docShell, false);
+  ok(!getBypassServiceWorker(docShell),
+     "The loadFlags in docShell doesn't bypass the serviceWorker by default");
+
+  intercepted = await fetchFakeDocAndCheckIfIntercepted(window);
+  ok(intercepted,
+     "The fetched document was intercepted by the serviceWorker");
+}
+
+// (This doesn't really need to be its own task, but it allows the actual test
+// case to be self-contained.)
+add_task(function setupPrefs() {
+  return SpecialPowers.pushPrefEnv({"set": [
+    ["dom.serviceWorkers.enabled", true],
+    ["dom.serviceWorkers.testing.enabled", true],
+  ]});
+});
+
+add_task(async function test_bypassServiceWorker() {
+  const swURL = "fetch.js";
+  let registration = await navigator.serviceWorker.register(swURL);
+
+  // Wait for the service worker to control the document
+  let waitForControlled = new Promise(resolve => {
+    navigator.serviceWorker.oncontrollerchange = resolve;
+  });
+
+  let sw =
+    registration.active || registration.waiting || registration.installing;
+  sw.postMessage("claim");
+  await waitForControlled;
+
+  try {
+    await testBypassSW();
+  } catch (e) {
+    ok(false, "Reason:" + e);
+  }
+
+  await registration.unregister();
+});
+
+</script>
+</body>
+</html>
