# HG changeset patch
# User Jeff Muizelaar <jmuizelaar@mozilla.com>
# Date 1512017512 18000
# Node ID d4fb75b8f858fc95c023fb81427ddc2004baa861
# Parent  e39903df5e31b03ae3896b6506ec0ca005ffc36c
Bug 1421860. Remove unused gfxContext::SetSource.

This removes some state from AzureState which will
make gfxContext creation and save()/restore() cheaper

diff --git a/gfx/thebes/gfxContext.cpp b/gfx/thebes/gfxContext.cpp
--- a/gfx/thebes/gfxContext.cpp
+++ b/gfx/thebes/gfxContext.cpp
@@ -45,34 +45,16 @@ UserDataKey gfxContext::sDontUseAsSource
 PatternFromState::operator mozilla::gfx::Pattern&()
 {
   gfxContext::AzureState &state = mContext->CurrentState();
 
   if (state.pattern) {
     return *state.pattern->GetPattern(mContext->mDT, state.patternTransformChanged ? &state.patternTransform : nullptr);
   }
 
-  if (state.sourceSurface) {
-    Matrix transform = state.surfTransform;
-
-    if (state.patternTransformChanged) {
-      Matrix mat = mContext->GetDTTransform();
-      if (!mat.Invert()) {
-        mPattern = new (mColorPattern.addr())
-        ColorPattern(Color()); // transparent black to paint nothing
-        return *mPattern;
-      }
-      transform = transform * state.patternTransform * mat;
-    }
-
-    mPattern = new (mSurfacePattern.addr())
-    SurfacePattern(state.sourceSurface, ExtendMode::CLAMP, transform);
-    return *mPattern;
-  }
-
   mPattern = new (mColorPattern.addr())
   ColorPattern(state.color);
   return *mPattern;
 }
 
 
 gfxContext::gfxContext(DrawTarget *aTarget, const Point& aDeviceOffset)
   : mPathIsRect(false)
@@ -688,80 +670,54 @@ gfxContext::ClipContainsRect(const gfxRe
 
 // rendering sources
 
 void
 gfxContext::SetColor(const Color& aColor)
 {
   CURRENTSTATE_CHANGED()
   CurrentState().pattern = nullptr;
-  CurrentState().sourceSurfCairo = nullptr;
-  CurrentState().sourceSurface = nullptr;
   CurrentState().color = ToDeviceColor(aColor);
 }
 
 void
 gfxContext::SetDeviceColor(const Color& aColor)
 {
   CURRENTSTATE_CHANGED()
   CurrentState().pattern = nullptr;
-  CurrentState().sourceSurfCairo = nullptr;
-  CurrentState().sourceSurface = nullptr;
   CurrentState().color = aColor;
 }
 
 bool
 gfxContext::GetDeviceColor(Color& aColorOut)
 {
-  if (CurrentState().sourceSurface) {
-    return false;
-  }
   if (CurrentState().pattern) {
     return CurrentState().pattern->GetSolidColor(aColorOut);
   }
 
   aColorOut = CurrentState().color;
   return true;
 }
 
 void
-gfxContext::SetSource(gfxASurface *surface, const gfxPoint& offset)
-{
-  CURRENTSTATE_CHANGED()
-  CurrentState().surfTransform = Matrix(1.0f, 0, 0, 1.0f, Float(offset.x), Float(offset.y));
-  CurrentState().pattern = nullptr;
-  CurrentState().patternTransformChanged = false;
-  // Keep the underlying cairo surface around while we keep the
-  // sourceSurface.
-  CurrentState().sourceSurfCairo = surface;
-  CurrentState().sourceSurface =
-  gfxPlatform::GetPlatform()->GetSourceSurfaceForSurface(mDT, surface);
-  CurrentState().color = Color(0, 0, 0, 0);
-}
-
-void
 gfxContext::SetPattern(gfxPattern *pattern)
 {
   CURRENTSTATE_CHANGED()
-  CurrentState().sourceSurfCairo = nullptr;
-  CurrentState().sourceSurface = nullptr;
   CurrentState().patternTransformChanged = false;
   CurrentState().pattern = pattern;
 }
 
 already_AddRefed<gfxPattern>
 gfxContext::GetPattern()
 {
   RefPtr<gfxPattern> pat;
 
   AzureState &state = CurrentState();
   if (state.pattern) {
     pat = state.pattern;
-  } else if (state.sourceSurface) {
-    NS_ASSERTION(false, "Ugh, this isn't good.");
   } else {
     pat = new gfxPattern(state.color);
   }
   return pat.forget();
 }
 
 // masking
 void
@@ -786,38 +742,16 @@ gfxContext::Mask(SourceSurface *surface,
             DrawOptions(alpha, CurrentState().op, CurrentState().aaMode));
 }
 
 void
 gfxContext::Paint(gfxFloat alpha)
 {
   AUTO_PROFILER_LABEL("gfxContext::Paint", GRAPHICS);
 
-  AzureState &state = CurrentState();
-
-  if (state.sourceSurface && !state.sourceSurfCairo &&
-      !state.patternTransformChanged)
-  {
-    // This is the case where a PopGroupToSource has been done and this
-    // paint is executed without changing the transform or the source.
-    Matrix oldMat = mDT->GetTransform();
-
-    IntSize surfSize = state.sourceSurface->GetSize();
-
-    mDT->SetTransform(Matrix::Translation(-state.deviceOffset.x,
-                                          -state.deviceOffset.y));
-
-    mDT->DrawSurface(state.sourceSurface,
-                     Rect(state.sourceSurfaceDeviceOffset, Size(surfSize.width, surfSize.height)),
-                     Rect(Point(), Size(surfSize.width, surfSize.height)),
-                     DrawSurfaceOptions(), DrawOptions(alpha, GetOp()));
-    mDT->SetTransform(oldMat);
-    return;
-  }
-
   Matrix mat = mDT->GetTransform();
   mat.Invert();
   Rect paintRect = mat.TransformBounds(Rect(Point(0, 0), Size(mDT->GetSize())));
 
   mDT->FillRect(paintRect, PatternFromState(this),
                 DrawOptions(Float(alpha), GetOp()));
 }
 
@@ -987,22 +921,16 @@ gfxContext::GetOp()
 
   AzureState &state = CurrentState();
   if (state.pattern) {
     if (state.pattern->IsOpaque()) {
       return CompositionOp::OP_OVER;
     } else {
       return CompositionOp::OP_SOURCE;
     }
-  } else if (state.sourceSurface) {
-    if (state.sourceSurface->GetFormat() == SurfaceFormat::B8G8R8X8) {
-      return CompositionOp::OP_OVER;
-    } else {
-      return CompositionOp::OP_SOURCE;
-    }
   } else {
     if (state.color.a > 0.999) {
       return CompositionOp::OP_OVER;
     } else {
       return CompositionOp::OP_SOURCE;
     }
   }
 }
@@ -1018,17 +946,17 @@ gfxContext::GetOp()
  * a change they might become invalid since patternTransformChanged is part of
  * the state and might be false for the restored AzureState.
  */
 void
 gfxContext::ChangeTransform(const Matrix &aNewMatrix, bool aUpdatePatternTransform)
 {
   AzureState &state = CurrentState();
 
-  if (aUpdatePatternTransform && (state.pattern || state.sourceSurface)
+  if (aUpdatePatternTransform && (state.pattern)
       && !state.patternTransformChanged) {
     state.patternTransform = GetDTTransform();
     state.patternTransformChanged = true;
   }
 
   if (mPathIsRect) {
     Matrix invMatrix = aNewMatrix;
     
diff --git a/gfx/thebes/gfxContext.h b/gfx/thebes/gfxContext.h
--- a/gfx/thebes/gfxContext.h
+++ b/gfx/thebes/gfxContext.h
@@ -35,17 +35,17 @@ class ClipExporter;
  * This is the main class for doing actual drawing. It is initialized using
  * a surface and can be drawn on. It manages various state information like
  * a current transformation matrix (CTM), a current path, current color,
  * etc.
  *
  * All drawing happens by creating a path and then stroking or filling it.
  * The functions like Rectangle and Arc do not do any drawing themselves.
  * When a path is drawn (stroked or filled), it is filled/stroked with a
- * pattern set by SetPattern, SetColor or SetSource.
+ * pattern set by SetPattern or SetColor.
  *
  * Note that the gfxContext takes coordinates in device pixels,
  * as opposed to app units.
  */
 class gfxContext final {
     typedef mozilla::gfx::CapStyle CapStyle;
     typedef mozilla::gfx::CompositionOp CompositionOp;
     typedef mozilla::gfx::JoinStyle JoinStyle;
@@ -265,25 +265,16 @@ public:
     /**
      * Set a solid color in the sRGB color space to use for drawing.
      * If CMS is not enabled, the color is treated as a device-space color
      * and this call is identical to SetDeviceColor().
      */
     void SetColor(const mozilla::gfx::Color& aColor);
 
     /**
-     * Uses a surface for drawing. This is a shorthand for creating a
-     * pattern and setting it.
-     *
-     * @param offset from the source surface, to use only part of it.
-     *        May need to make it negative.
-     */
-    void SetSource(gfxASurface *surface, const gfxPoint& offset = gfxPoint(0.0, 0.0));
-
-    /**
      * Uses a pattern for drawing.
      */
     void SetPattern(gfxPattern *pattern);
 
     /**
      * Get the source pattern (solid color, normal pattern, surface, etc)
      */
     already_AddRefed<gfxPattern> GetPattern();
@@ -484,20 +475,16 @@ private:
 #ifdef DEBUG
       , mContentChanged(false)
 #endif
     {}
 
     mozilla::gfx::CompositionOp op;
     Color color;
     RefPtr<gfxPattern> pattern;
-    RefPtr<gfxASurface> sourceSurfCairo;
-    RefPtr<SourceSurface> sourceSurface;
-    mozilla::gfx::Point sourceSurfaceDeviceOffset;
-    Matrix surfTransform;
     Matrix transform;
     struct PushedClip {
       RefPtr<Path> path;
       Rect rect;
       Matrix transform;
     };
     nsTArray<PushedClip> pushedClips;
     nsTArray<Float> dashPattern;
diff --git a/gfx/thebes/gfxFont.cpp b/gfx/thebes/gfxFont.cpp
--- a/gfx/thebes/gfxFont.cpp
+++ b/gfx/thebes/gfxFont.cpp
@@ -1709,22 +1709,16 @@ private:
                 } else {
                     pat = fillPattern->GetPattern(mRunParams.dt);
                 }
 
                 if (pat) {
                     mRunParams.dt->FillGlyphs(mFontParams.scaledFont, buf,
                                               *pat, mFontParams.drawOptions);
                 }
-            } else if (state.sourceSurface) {
-                mRunParams.dt->FillGlyphs(mFontParams.scaledFont, buf,
-                                          SurfacePattern(state.sourceSurface,
-                                                         ExtendMode::CLAMP,
-                                                         state.surfTransform),
-                                          mFontParams.drawOptions);
             } else {
                 mRunParams.dt->FillGlyphs(mFontParams.scaledFont, buf,
                                           ColorPattern(state.color),
                                           mFontParams.drawOptions);
             }
         }
         if (GetStrokeMode(mRunParams.drawMode) == DrawMode::GLYPH_STROKE &&
             mRunParams.strokeOpts) {
