# HG changeset patch
# User sotaro <sotaro.ikeda.g@gmail.com>
# Date 1512045527 -32400
# Node ID 34e36fdc6b3ed14f58f506aa482569c069e05223
# Parent  9ab4413d71ddff7ee6e5c69d4c8feb6b5c95fc8e
Bug 1416533 - Skip to generate frame if there is no update r=nical,kats

diff --git a/gfx/layers/ipc/CompositorBridgeParent.cpp b/gfx/layers/ipc/CompositorBridgeParent.cpp
--- a/gfx/layers/ipc/CompositorBridgeParent.cpp
+++ b/gfx/layers/ipc/CompositorBridgeParent.cpp
@@ -838,17 +838,21 @@ CompositorBridgeParent::NotifyShadowTree
 void
 CompositorBridgeParent::ScheduleComposition()
 {
   MOZ_ASSERT(CompositorThreadHolder::IsInCompositorThread());
   if (mPaused) {
     return;
   }
 
-  mCompositorScheduler->ScheduleComposition();
+  if (mWrBridge) {
+    mWrBridge->ScheduleGenerateFrame();
+  } else {
+    mCompositorScheduler->ScheduleComposition();
+  }
 }
 
 // Go down the composite layer tree, setting properties to match their
 // content-side counterparts.
 /* static */ void
 CompositorBridgeParent::SetShadowProperties(Layer* aLayer)
 {
   ForEachNode<ForwardIterator>(
diff --git a/gfx/layers/wr/AsyncImagePipelineManager.cpp b/gfx/layers/wr/AsyncImagePipelineManager.cpp
--- a/gfx/layers/wr/AsyncImagePipelineManager.cpp
+++ b/gfx/layers/wr/AsyncImagePipelineManager.cpp
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "AsyncImagePipelineManager.h"
 
 #include "CompositableHost.h"
 #include "gfxEnv.h"
 #include "mozilla/gfx/gfxVars.h"
+#include "mozilla/layers/CompositorThread.h"
 #include "mozilla/layers/WebRenderImageHost.h"
 #include "mozilla/layers/WebRenderTextureHost.h"
 #include "mozilla/webrender/WebRenderAPI.h"
 #include "mozilla/webrender/WebRenderTypes.h"
 
 namespace mozilla {
 namespace layers {
 
@@ -25,16 +26,17 @@ AsyncImagePipelineManager::AsyncImagePip
  , mMixBlendMode(wr::MixBlendMode::Normal)
 {}
 
 AsyncImagePipelineManager::AsyncImagePipelineManager(already_AddRefed<wr::WebRenderAPI>&& aApi)
  : mApi(aApi)
  , mIdNamespace(mApi->GetNamespace())
  , mResourceId(0)
  , mAsyncImageEpoch(0)
+ , mWillGenerateFrame(false)
  , mDestroyed(false)
 {
   MOZ_COUNT_CTOR(AsyncImagePipelineManager);
 }
 
 AsyncImagePipelineManager::~AsyncImagePipelineManager()
 {
   MOZ_COUNT_DTOR(AsyncImagePipelineManager);
@@ -44,16 +46,34 @@ void
 AsyncImagePipelineManager::Destroy()
 {
   MOZ_ASSERT(!mDestroyed);
   mApi = nullptr;
   mDestroyed = true;
 }
 
 void
+AsyncImagePipelineManager::SetWillGenerateFrame()
+{
+  MOZ_ASSERT(CompositorThreadHolder::IsInCompositorThread());
+
+  mWillGenerateFrame = true;
+}
+
+bool
+AsyncImagePipelineManager::GetAndResetWillGenerateFrame()
+{
+  MOZ_ASSERT(CompositorThreadHolder::IsInCompositorThread());
+
+  bool ret = mWillGenerateFrame;
+  mWillGenerateFrame = false;
+  return ret;
+}
+
+void
 AsyncImagePipelineManager::AddPipeline(const wr::PipelineId& aPipelineId)
 {
   if (mDestroyed) {
     return;
   }
   uint64_t id = wr::AsUint64(aPipelineId);
 
   PipelineTexturesHolder* holder = mPipelineTexturesHolders.Get(wr::AsUint64(aPipelineId));
@@ -265,16 +285,21 @@ AsyncImagePipelineManager::ApplyAsyncIma
 
     nsTArray<wr::ImageKey> keys;
     auto op = UpdateImageKeys(resourceUpdates, pipeline, keys);
 
     bool updateDisplayList = pipeline->mInitialised &&
                              (pipeline->mIsChanged || op == Some(TextureHost::ADD_IMAGE)) &&
                              !!pipeline->mCurrentTexture;
 
+    // Request to generate frame if there is an update.
+    if (updateDisplayList || !op.isNothing()) {
+      SetWillGenerateFrame();
+    }
+
     if (!updateDisplayList) {
       // We don't need to update the display list, either because we can't or because
       // the previous one is still up to date.
       // We may, however, have updated some resources.
       mApi->UpdatePipelineResources(resourceUpdates, pipelineId, epoch);
       if (pipeline->mCurrentTexture) {
         HoldExternalImage(pipelineId, epoch, pipeline->mCurrentTexture->AsWebRenderTextureHost());
       }
diff --git a/gfx/layers/wr/AsyncImagePipelineManager.h b/gfx/layers/wr/AsyncImagePipelineManager.h
--- a/gfx/layers/wr/AsyncImagePipelineManager.h
+++ b/gfx/layers/wr/AsyncImagePipelineManager.h
@@ -86,16 +86,19 @@ public:
     mImageCompositeNotifications.AppendElement(aNotification);
   }
 
   void FlushImageNotifications(nsTArray<ImageCompositeNotificationInfo>* aNotifications)
   {
     aNotifications->AppendElements(Move(mImageCompositeNotifications));
   }
 
+  void SetWillGenerateFrame();
+  bool GetAndResetWillGenerateFrame();
+
 private:
 
   uint32_t GetNextResourceId() { return ++mResourceId; }
   wr::IdNamespace GetNamespace() { return mIdNamespace; }
   wr::ImageKey GenerateImageKey()
   {
     wr::ImageKey key;
     key.mNamespace = GetNamespace();
@@ -163,16 +166,17 @@ private:
 
   RefPtr<wr::WebRenderAPI> mApi;
   wr::IdNamespace mIdNamespace;
   uint32_t mResourceId;
 
   nsClassHashtable<nsUint64HashKey, PipelineTexturesHolder> mPipelineTexturesHolders;
   nsClassHashtable<nsUint64HashKey, AsyncImagePipeline> mAsyncImagePipelines;
   uint32_t mAsyncImageEpoch;
+  bool mWillGenerateFrame;
   bool mDestroyed;
 
   // Render time for the current composition.
   TimeStamp mCompositionTime;
 
   // When nonnull, during rendering, some compositable indicated that it will
   // change its rendering at this time. In order not to miss it, we composite
   // on every vsync until this time occurs (this is the latest such time).
diff --git a/gfx/layers/wr/WebRenderBridgeParent.cpp b/gfx/layers/wr/WebRenderBridgeParent.cpp
--- a/gfx/layers/wr/WebRenderBridgeParent.cpp
+++ b/gfx/layers/wr/WebRenderBridgeParent.cpp
@@ -607,17 +607,17 @@ WebRenderBridgeParent::RecvSetDisplayLis
       mApi->SetWindowParameters(size);
     }
     gfx::Color clearColor(0.f, 0.f, 0.f, 0.f);
     mApi->SetDisplayList(clearColor, wr::NewEpoch(wrEpoch), LayerSize(aSize.width, aSize.height),
                         mPipelineId, aContentSize,
                         dlDesc, dl.mData, dl.mLen,
                         resources);
 
-    ScheduleComposition();
+    ScheduleGenerateFrame();
 
     if (ShouldParentObserveEpoch()) {
       mCompositorBridge->ObserveLayerUpdate(GetLayersId(), GetChildLayerObserverEpoch(), true);
     }
   }
 
   HoldPendingTransactionId(wrEpoch, aTransactionId, aTxnStartTime, aFwdTime);
 
@@ -659,17 +659,17 @@ WebRenderBridgeParent::RecvEmptyTransact
 
   // This ensures that destroy operations are always processed. It is not safe
   // to early-return without doing so.
   AutoWebRenderBridgeParentAsyncMessageSender autoAsyncMessageSender(this, &aToDestroy);
 
   if (!aCommands.IsEmpty()) {
     mAsyncImageManager->SetCompositionTime(TimeStamp::Now());
     ProcessWebRenderParentCommands(aCommands);
-    mCompositorScheduler->ScheduleComposition();
+    ScheduleGenerateFrame();
   }
 
   mScrollData.SetFocusTarget(aFocusTarget);
   UpdateAPZ(false);
 
   if (!aCommands.IsEmpty()) {
     uint32_t wrEpoch = GetNextWrEpoch();
     // Send empty UpdatePipelineResources to WebRender just to notify a new epoch.
@@ -935,18 +935,18 @@ WebRenderBridgeParent::RecvClearCachedRe
 {
   if (mDestroyed) {
     return IPC_OK();
   }
   mCompositorBridge->ObserveLayerUpdate(GetLayersId(), GetChildLayerObserverEpoch(), false);
 
   // Clear resources
   mApi->ClearDisplayList(wr::NewEpoch(GetNextWrEpoch()), mPipelineId);
-  // Schedule composition to clean up Pipeline
-  mCompositorScheduler->ScheduleComposition();
+  // Schedule generate frame to clean up Pipeline
+  ScheduleGenerateFrame();
   // Remove animations.
   for (std::unordered_set<uint64_t>::iterator iter = mActiveAnimations.begin(); iter != mActiveAnimations.end(); iter++) {
     mAnimStorage->ClearById(*iter);
   }
   mActiveAnimations.clear();
   return IPC_OK();
 }
 
@@ -993,17 +993,17 @@ WebRenderBridgeParent::UpdateWebRender(C
 }
 
 mozilla::ipc::IPCResult
 WebRenderBridgeParent::RecvForceComposite()
 {
   if (mDestroyed) {
     return IPC_OK();
   }
-  ScheduleComposition();
+  ScheduleGenerateFrame();
   return IPC_OK();
 }
 
 already_AddRefed<AsyncPanZoomController>
 WebRenderBridgeParent::GetTargetAPZC(const FrameMetrics::ViewID& aScrollId)
 {
   RefPtr<AsyncPanZoomController> apzc;
   if (CompositorBridgeParent* cbp = GetRootCompositorBridgeParent()) {
@@ -1191,56 +1191,60 @@ WebRenderBridgeParent::CompositeToTarget
   AUTO_PROFILER_TRACING("Paint", "CompositeToTraget");
   if (mPaused) {
     return;
   }
 
   if (!mForceRendering &&
       wr::RenderThread::Get()->TooManyPendingFrames(mApi->GetId())) {
     // Render thread is busy, try next time.
-    ScheduleComposition();
+    mCompositorScheduler->ScheduleComposition();
     return;
   }
 
-  bool scheduleComposite = false;
-  nsTArray<wr::WrOpacityProperty> opacityArray;
-  nsTArray<wr::WrTransformProperty> transformArray;
-
   mAsyncImageManager->SetCompositionTime(TimeStamp::Now());
   mAsyncImageManager->ApplyAsyncImages();
 
+  if (!mAsyncImageManager->GetCompositeUntilTime().IsNull()) {
+    // Trigger another CompositeToTarget() call because there might be another
+    // frame that we want to generate after this one.
+    // It will check if we actually want to generate the frame or not.
+    mCompositorScheduler->ScheduleComposition();
+  }
+
+  if (!mAsyncImageManager->GetAndResetWillGenerateFrame() &&
+      !mForceRendering) {
+    // Could skip generating frame now.
+    return;
+  }
+
+  nsTArray<wr::WrOpacityProperty> opacityArray;
+  nsTArray<wr::WrTransformProperty> transformArray;
+
   SampleAnimations(opacityArray, transformArray);
   if (!transformArray.IsEmpty() || !opacityArray.IsEmpty()) {
-    scheduleComposite = true;
+    ScheduleGenerateFrame();
   }
 
   if (PushAPZStateToWR(transformArray)) {
-    scheduleComposite = true;
+    ScheduleGenerateFrame();
   }
 
   wr::RenderThread::Get()->IncPendingFrameCount(mApi->GetId());
 
 #if defined(ENABLE_FRAME_LATENCY_LOG)
   auto startTime = TimeStamp::Now();
   mApi->SetFrameStartTime(startTime);
 #endif
 
   if (!transformArray.IsEmpty() || !opacityArray.IsEmpty()) {
     mApi->GenerateFrame(opacityArray, transformArray);
   } else {
     mApi->GenerateFrame();
   }
-
-  if (!mAsyncImageManager->GetCompositeUntilTime().IsNull()) {
-    scheduleComposite = true;
-  }
-
-  if (scheduleComposite) {
-    ScheduleComposition();
-  }
 }
 
 void
 WebRenderBridgeParent::HoldPendingTransactionId(uint32_t aWrEpoch,
                                                 uint64_t aTransactionId,
                                                 const TimeStamp& aTxnStartTime,
                                                 const TimeStamp& aFwdTime)
 {
@@ -1304,19 +1308,20 @@ WebRenderBridgeParent::FlushTransactionI
 
 uint64_t
 WebRenderBridgeParent::GetLayersId() const
 {
   return wr::AsUint64(mPipelineId);
 }
 
 void
-WebRenderBridgeParent::ScheduleComposition()
+WebRenderBridgeParent::ScheduleGenerateFrame()
 {
   if (mCompositorScheduler) {
+    mAsyncImageManager->SetWillGenerateFrame();
     mCompositorScheduler->ScheduleComposition();
   }
 }
 
 void
 WebRenderBridgeParent::FlushRendering(bool aIsSync)
 {
   if (mDestroyed) {
@@ -1370,18 +1375,18 @@ void
 WebRenderBridgeParent::ClearResources()
 {
   if (!mApi) {
     return;
   }
 
   uint32_t wrEpoch = GetNextWrEpoch();
   mApi->ClearDisplayList(wr::NewEpoch(wrEpoch), mPipelineId);
-  // Schedule composition to clean up Pipeline
-  mCompositorScheduler->ScheduleComposition();
+  // Schedule generate frame to clean up Pipeline
+  ScheduleGenerateFrame();
   // WrFontKeys and WrImageKeys are deleted during WebRenderAPI destruction.
   for (auto iter = mExternalImageIds.Iter(); !iter.Done(); iter.Next()) {
     iter.Data()->ClearWrBridge();
   }
   mExternalImageIds.Clear();
   for (auto iter = mAsyncCompositables.Iter(); !iter.Done(); iter.Next()) {
     wr::PipelineId pipelineId = wr::AsPipelineId(iter.Key());
     RefPtr<WebRenderImageHost> host = iter.Data();
diff --git a/gfx/layers/wr/WebRenderBridgeParent.h b/gfx/layers/wr/WebRenderBridgeParent.h
--- a/gfx/layers/wr/WebRenderBridgeParent.h
+++ b/gfx/layers/wr/WebRenderBridgeParent.h
@@ -168,17 +168,28 @@ public:
     return mIdNamespace;
   }
 
   void UpdateAPZ(bool aUpdateHitTestingTree);
   const WebRenderScrollData& GetScrollData() const;
 
   void FlushRendering(bool aIsSync);
 
-  void ScheduleComposition();
+  /**
+   * Schedule generating WebRender frame definitely at next composite timing.
+   *
+   * WebRenderBridgeParent uses composite timing to check if there is an update
+   * to AsyncImagePipelines. If there is no update, WebRenderBridgeParent skips
+   * to generate frame. If we need to generate new frame at next composite timing,
+   * call this method.
+   *
+   * Call CompositorVsyncScheduler::ScheduleComposition() directly, if we just
+   * want to trigger AsyncImagePipelines update checks.
+   */
+  void ScheduleGenerateFrame();
 
   void UpdateWebRender(CompositorVsyncScheduler* aScheduler,
                        wr::WebRenderAPI* aApi,
                        AsyncImagePipelineManager* aImageMgr,
                        CompositorAnimationStorage* aAnimStorage);
 
 private:
   void DeallocShmems(nsTArray<ipc::Shmem>& aShmems);
diff --git a/gfx/layers/wr/WebRenderImageHost.cpp b/gfx/layers/wr/WebRenderImageHost.cpp
--- a/gfx/layers/wr/WebRenderImageHost.cpp
+++ b/gfx/layers/wr/WebRenderImageHost.cpp
@@ -65,18 +65,19 @@ WebRenderImageHost::UseTextureHost(const
     img.mProducerID = t.mProducerID;
     img.mTextureHost->SetCropRect(img.mPictureRect);
     img.mTextureHost->Updated();
   }
 
   mImages.SwapElements(newImages);
   newImages.Clear();
 
-  if (mWrBridge && GetAsyncRef()) {
-    mWrBridge->ScheduleComposition();
+  if (mWrBridge && mWrBridge->CompositorScheduler() && GetAsyncRef()) {
+    // Will check if we will generate frame.
+    mWrBridge->CompositorScheduler()->ScheduleComposition();
   }
 
   // Video producers generally send replacement images with the same frameID but
   // slightly different timestamps in order to sync with the audio clock. This
   // means that any CompositeUntil() call we made in Composite() may no longer
   // guarantee that we'll composite until the next frame is ready. Fix that here.
   if (mWrBridge && mLastFrameID >= 0) {
     MOZ_ASSERT(mWrBridge->AsyncImageManager());
