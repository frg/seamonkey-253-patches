# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1509481818 0
#      Tue Oct 31 20:30:18 2017 +0000
# Node ID 5b014ab0f9074697d38df5f816f672e7203ce4bc
# Parent  ef02257885c645cd7b2e490081dd61f1027a0552
Bug 1413292 - Stop sending geckodriver backtrace with errors. r=jgraham

The Rust backtrace from geckodriver is  not useful since any error
we actually return is by-definition handled by the code, and we
can probably get more information just by looking at the error type
and string.

At the same time, it runs the risk of confusing users into thinking
there was a bug in the driver when actually it's perfectly normal
handling of invalid input.

MozReview-Commit-ID: 9S5IaioA5AA

diff --git a/testing/geckodriver/CHANGES.md b/testing/geckodriver/CHANGES.md
--- a/testing/geckodriver/CHANGES.md
+++ b/testing/geckodriver/CHANGES.md
@@ -1,13 +1,22 @@
 Change log
 ==========
 
 All notable changes to this program is documented in this file.
 
+Unreleased
+----------
+
+### Changed
+
+- Backtraces from geckodriver no longer substitute for missing
+  Marionette stacktraces
+
+
 0.19.1 (2017-10-30)
 -------------------
 
 ### Changed
 
 - Search suggestions in the location bar turned off as not to
   trigger network connections
 
@@ -23,17 +32,17 @@ 0.19.1 (2017-10-30)
 
 - Removed obsolete `socksUsername` and `socksPassword` proxy
   configuration keys because neither were picked up or recognised
 
 
 0.19.0 (2017-09-16)
 -------------------
 
-Note that with geckodriver v0.19.0 the following versions are recommended:
+Note that with geckodriver 0.19.0 the following versions are recommended:
 - Firefox 55.0 (and greater)
 - Selenium 3.5 (and greater)
 
 ### Added
 
 - Added endpoint:
   - POST `/session/{session id}/window/minimize` for the [Minimize Window]
     command
diff --git a/testing/webdriver/src/error.rs b/testing/webdriver/src/error.rs
--- a/testing/webdriver/src/error.rs
+++ b/testing/webdriver/src/error.rs
@@ -1,9 +1,8 @@
-use backtrace::Backtrace;
 use hyper::status::StatusCode;
 use rustc_serialize::base64::FromBase64Error;
 use rustc_serialize::json::{DecoderError, Json, ParserError, ToJson};
 use std::borrow::Cow;
 use std::collections::BTreeMap;
 use std::convert::From;
 use std::error::Error;
 use std::fmt;
@@ -264,17 +263,17 @@ pub struct WebDriverError {
 
 impl WebDriverError {
     pub fn new<S>(error: ErrorStatus, message: S) -> WebDriverError
         where S: Into<Cow<'static, str>>
     {
         WebDriverError {
             error: error,
             message: message.into(),
-            stack: format!("{:?}", Backtrace::new()).into(),
+            stack: "".into(),
             delete_session: false,
         }
     }
 
     pub fn new_with_stack<S>(error: ErrorStatus, message: S, stack: S) -> WebDriverError
         where S: Into<Cow<'static, str>>
     {
         WebDriverError {
diff --git a/testing/webdriver/src/lib.rs b/testing/webdriver/src/lib.rs
--- a/testing/webdriver/src/lib.rs
+++ b/testing/webdriver/src/lib.rs
@@ -1,11 +1,10 @@
 #![allow(non_snake_case)]
 
-extern crate backtrace;
 #[macro_use]
 extern crate log;
 extern crate rustc_serialize;
 extern crate hyper;
 extern crate regex;
 extern crate cookie;
 extern crate time;
 extern crate url;
