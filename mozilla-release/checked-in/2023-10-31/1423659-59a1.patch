# HG changeset patch
# User bechen@mozilla.com <bechen@mozilla.com>
# Date 1515555779 -28800
#      Wed Jan 10 11:42:59 2018 +0800
# Node ID 0932f3bbbc1a8f6b3a683c98f106755a2ef27872
# Parent  4edc392d61faa09c886074775360b9c82af787b3
Bug 1423659 - Remove ftyp and HasMetadata(). r=alfredo

MozReview-Commit-ID: ChscPHrohHb

diff --git a/dom/media/gtest/mp4_demuxer/TestParser.cpp b/dom/media/gtest/mp4_demuxer/TestParser.cpp
--- a/dom/media/gtest/mp4_demuxer/TestParser.cpp
+++ b/dom/media/gtest/mp4_demuxer/TestParser.cpp
@@ -114,17 +114,16 @@ TEST(MoofParser, EmptyStream)
 
   MediaByteRangeSet byteRanges;
   EXPECT_FALSE(parser.RebuildFragmentedIndex(byteRanges));
 
   EXPECT_TRUE(parser.GetCompositionRange(byteRanges).IsNull());
   EXPECT_TRUE(parser.mInitRange.IsEmpty());
   EXPECT_EQ(0u, parser.mOffset);
   EXPECT_TRUE(parser.ReachedEnd());
-  EXPECT_FALSE(parser.HasMetadata());
   RefPtr<MediaByteBuffer> metadataBuffer = parser.Metadata();
   EXPECT_FALSE(metadataBuffer);
   EXPECT_TRUE(parser.FirstCompleteMediaSegment().IsEmpty());
   EXPECT_TRUE(parser.FirstCompleteMediaHeader().IsEmpty());
 }
 
 nsTArray<uint8_t>
 ReadTestFile(const char* aFilename)
@@ -390,17 +389,16 @@ TEST(MoofParser, test_case_mp4)
     ASSERT_FALSE(buffer.IsEmpty());
     RefPtr<ByteStream> stream = new TestStream(buffer.Elements(), buffer.Length());
 
     MoofParser parser(stream, 0, false);
     EXPECT_EQ(0u, parser.mOffset) << tests[test].mFilename;
     EXPECT_FALSE(parser.ReachedEnd()) << tests[test].mFilename;
     EXPECT_TRUE(parser.mInitRange.IsEmpty()) << tests[test].mFilename;
 
-    EXPECT_TRUE(parser.HasMetadata()) << tests[test].mFilename;
     RefPtr<MediaByteBuffer> metadataBuffer = parser.Metadata();
     EXPECT_TRUE(metadataBuffer) << tests[test].mFilename;
 
     EXPECT_FALSE(parser.mInitRange.IsEmpty()) << tests[test].mFilename;
     const MediaByteRangeSet byteRanges(
       MediaByteRange(0, int64_t(buffer.Length())));
     EXPECT_EQ(tests[test].mValidMoof,
               parser.RebuildFragmentedIndex(byteRanges))  << tests[test].mFilename;
@@ -437,17 +435,16 @@ TEST(MoofParser, test_case_mp4_subsets)
       while (size > 0) {
         RefPtr<TestStream> stream =
           new TestStream(buffer.Elements() + offset, size);
 
         MoofParser parser(stream, 0, false);
         MediaByteRangeSet byteRanges;
         EXPECT_FALSE(parser.RebuildFragmentedIndex(byteRanges));
         parser.GetCompositionRange(byteRanges);
-        parser.HasMetadata();
         RefPtr<MediaByteBuffer> metadataBuffer = parser.Metadata();
         parser.FirstCompleteMediaSegment();
         parser.FirstCompleteMediaHeader();
 
         if (stream->mHighestSuccessfulEndOffset <= 0) {
           // No successful reads -> Cutting down the size won't change anything.
           break;
         }
diff --git a/dom/media/mp4/MoofParser.cpp b/dom/media/mp4/MoofParser.cpp
--- a/dom/media/mp4/MoofParser.cpp
+++ b/dom/media/mp4/MoofParser.cpp
@@ -160,81 +160,55 @@ MoofParser::BlockingReadNextMoof()
       byteRanges += MediaByteRange(mOffset, box.Range().mEnd);
       return RebuildFragmentedIndex(context);
     }
   }
   return false;
 }
 
 void
-MoofParser::ScanForMetadata(mozilla::MediaByteRange& aFtyp,
-                            mozilla::MediaByteRange& aMoov)
+MoofParser::ScanForMetadata(mozilla::MediaByteRange& aMoov)
 {
   int64_t length = std::numeric_limits<int64_t>::max();
   mSource->Length(&length);
   MediaByteRangeSet byteRanges;
   byteRanges += MediaByteRange(0, length);
   RefPtr<BlockingStream> stream = new BlockingStream(mSource);
 
   BoxContext context(stream, byteRanges);
   for (Box box(&context, mOffset); box.IsAvailable(); box = box.Next()) {
-    if (box.IsType("ftyp")) {
-      aFtyp = box.Range();
-      continue;
-    }
     if (box.IsType("moov")) {
       aMoov = box.Range();
       break;
     }
   }
-  mInitRange = aFtyp.Span(aMoov);
-}
-
-bool
-MoofParser::HasMetadata()
-{
-  MediaByteRange ftyp;
-  MediaByteRange moov;
-  ScanForMetadata(ftyp, moov);
-  return !!ftyp.Length() && !!moov.Length();
+  mInitRange = aMoov;
 }
 
 already_AddRefed<mozilla::MediaByteBuffer>
 MoofParser::Metadata()
 {
-  MediaByteRange ftyp;
   MediaByteRange moov;
-  ScanForMetadata(ftyp, moov);
-  CheckedInt<MediaByteBuffer::size_type> ftypLength = ftyp.Length();
+  ScanForMetadata(moov);
   CheckedInt<MediaByteBuffer::size_type> moovLength = moov.Length();
-  if (!ftypLength.isValid() || !moovLength.isValid()
-      || !ftypLength.value() || !moovLength.value()) {
-    // No ftyp or moov, or they cannot be used as array size.
+  if (!moovLength.isValid() || !moovLength.value()) {
+    // No moov, or cannot be used as array size.
     return nullptr;
   }
-  CheckedInt<MediaByteBuffer::size_type> totalLength = ftypLength + moovLength;
-  if (!totalLength.isValid()) {
-    // Addition overflow, or sum cannot be used as array size.
-    return nullptr;
-  }
+
   RefPtr<MediaByteBuffer> metadata = new MediaByteBuffer();
-  if (!metadata->SetLength(totalLength.value(), fallible)) {
+  if (!metadata->SetLength(moovLength.value(), fallible)) {
     LOG(Moof, "OOM");
     return nullptr;
   }
 
   RefPtr<BlockingStream> stream = new BlockingStream(mSource);
   size_t read;
   bool rv =
-    stream->ReadAt(ftyp.mStart, metadata->Elements(), ftypLength.value(), &read);
-  if (!rv || read != ftypLength.value()) {
-    return nullptr;
-  }
-  rv =
-    stream->ReadAt(moov.mStart, metadata->Elements() + ftypLength.value(), moovLength.value(), &read);
+    stream->ReadAt(moov.mStart, metadata->Elements(), moovLength.value(), &read);
   if (!rv || read != moovLength.value()) {
     return nullptr;
   }
   return metadata.forget();
 }
 
 MP4Interval<Microseconds>
 MoofParser::GetCompositionRange(const MediaByteRangeSet& aByteRanges)
diff --git a/dom/media/mp4/MoofParser.h b/dom/media/mp4/MoofParser.h
--- a/dom/media/mp4/MoofParser.h
+++ b/dom/media/mp4/MoofParser.h
@@ -318,17 +318,17 @@ public:
 
   void ParseMinf(Box& aBox);
   void ParseStbl(Box& aBox);
   void ParseStsd(Box& aBox);
   void ParseEncrypted(Box& aBox);
   void ParseSinf(Box& aBox);
 
   bool BlockingReadNextMoof();
-  bool HasMetadata();
+
   already_AddRefed<mozilla::MediaByteBuffer> Metadata();
   MediaByteRange FirstCompleteMediaSegment();
   MediaByteRange FirstCompleteMediaHeader();
 
   mozilla::MediaByteRange mInitRange;
   RefPtr<ByteStream> mSource;
   uint64_t mOffset;
   Mvhd mMvhd;
@@ -338,18 +338,17 @@ public:
   Edts mEdts;
   Sinf mSinf;
 
   FallibleTArray<CencSampleEncryptionInfoEntry> mTrackSampleEncryptionInfoEntries;
   FallibleTArray<SampleToGroupEntry> mTrackSampleToGroupEntries;
 
   nsTArray<Moof>& Moofs() { return mMoofs; }
 private:
-  void ScanForMetadata(mozilla::MediaByteRange& aFtyp,
-                       mozilla::MediaByteRange& aMoov);
+  void ScanForMetadata(mozilla::MediaByteRange& aMoov);
   nsTArray<Moof> mMoofs;
   nsTArray<MediaByteRange> mMediaRanges;
   bool mIsAudio;
   uint64_t mLastDecodeTime;
 };
 }
 
 #endif
