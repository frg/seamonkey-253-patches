# HG changeset patch
# User Ray Lin <ralin@mozilla.com>
# Date 1511416065 -28800
# Node ID 4d7297509f5ff18ba3e73e52a3c492e75d7c2649
# Parent  2bdc7e1f145241c5c472fe42e86e551da847acd7
Bug 1419724 - Explicitly make searchString consistent with autocompleteController as form autofill goes different route to fill the value. r=MattN

MozReview-Commit-ID: BpoGAyWlT7q

diff --git a/browser/extensions/formautofill/FormAutofillContent.jsm b/browser/extensions/formautofill/FormAutofillContent.jsm
--- a/browser/extensions/formautofill/FormAutofillContent.jsm
+++ b/browser/extensions/formautofill/FormAutofillContent.jsm
@@ -27,16 +27,19 @@ ChromeUtils.defineModuleGetter(this, "Fo
                                "resource://formautofill/FormAutofillHandler.jsm");
 ChromeUtils.defineModuleGetter(this, "FormLikeFactory",
                                "resource://gre/modules/FormLikeFactory.jsm");
 ChromeUtils.defineModuleGetter(this, "InsecurePasswordUtils",
                                "resource://gre/modules/InsecurePasswordUtils.jsm");
 
 const formFillController = Cc["@mozilla.org/satchel/form-fill-controller;1"]
                              .getService(Ci.nsIFormFillController);
+const autocompleteController = Cc["@mozilla.org/autocomplete/controller;1"]
+                                 .getService(Ci.nsIAutoCompleteController);
+
 const {ADDRESSES_COLLECTION_NAME, CREDITCARDS_COLLECTION_NAME, FIELD_STATES} = FormAutofillUtils;
 
 // Register/unregister a constructor as a factory.
 function AutocompleteFactory() {}
 AutocompleteFactory.prototype = {
   register(targetConstructor) {
     let proto = targetConstructor.prototype;
     this._classID = proto.classID;
@@ -286,19 +289,22 @@ let ProfileAutocomplete = {
     let selectedIndex = this._getSelectedIndex(focusedInput.ownerGlobal);
     if (selectedIndex == -1 ||
         !this.lastProfileAutoCompleteResult ||
         this.lastProfileAutoCompleteResult.getStyleAt(selectedIndex) != "autofill-profile") {
       return;
     }
 
     let profile = JSON.parse(this.lastProfileAutoCompleteResult.getCommentAt(selectedIndex));
+    let {fieldName} = FormAutofillContent.getInputDetails(focusedInput);
     let formHandler = FormAutofillContent.getFormHandler(focusedInput);
 
-    formHandler.autofillFormFields(profile, focusedInput);
+    formHandler.autofillFormFields(profile, focusedInput).then(() => {
+      autocompleteController.searchString = profile[fieldName];
+    });
   },
 
   _clearProfilePreview() {
     let focusedInput = formFillController.focusedInput || this.lastProfileAutoCompleteFocusedInput;
     if (!focusedInput || !FormAutofillContent.getFormDetails(focusedInput)) {
       return;
     }
 
@@ -514,16 +520,17 @@ var FormAutofillContent = {
   clearForm() {
     let focusedInput = formFillController.focusedInput || ProfileAutocomplete._lastAutoCompleteFocusedInput;
     if (!focusedInput) {
       return;
     }
 
     let formHandler = this.getFormHandler(focusedInput);
     formHandler.clearPopulatedForm(focusedInput);
+    autocompleteController.searchString = "";
   },
 
   previewProfile(doc) {
     let docWin = doc.ownerGlobal;
     let selectedIndex = ProfileAutocomplete._getSelectedIndex(docWin);
     let lastAutoCompleteResult = ProfileAutocomplete.lastProfileAutoCompleteResult;
     let focusedInput = formFillController.focusedInput;
     let mm = this._messageManagerFromWindow(docWin);
diff --git a/browser/extensions/formautofill/test/mochitest/test_basic_autocomplete_form.html b/browser/extensions/formautofill/test/mochitest/test_basic_autocomplete_form.html
--- a/browser/extensions/formautofill/test/mochitest/test_basic_autocomplete_form.html
+++ b/browser/extensions/formautofill/test/mochitest/test_basic_autocomplete_form.html
@@ -137,27 +137,29 @@ add_task(async function check_search_res
   await expectPopup();
   checkMenuEntries(["+1234567890"], false);
 
   await SpecialPowers.popPrefEnv();
 });
 
 // Autofill the address from dropdown menu.
 add_task(async function check_fields_after_form_autofill() {
-  await setInput("#organization", "Moz");
+  const focusedInput = await setInput("#organization", "Moz");
   doKey("down");
   await expectPopup();
   checkMenuEntries(MOCK_STORAGE.map(address =>
     JSON.stringify({
       primary: address.organization,
       secondary: FormAutofillUtils.toOneLineAddress(address["street-address"]),
     })
   ).slice(1));
   doKey("down");
   await triggerAutofillAndCheckProfile(MOCK_STORAGE[1]);
+  doKey("escape");
+  is(focusedInput.value, "Mozilla", "Filled field shouldn't be reverted by ESC key");
 });
 
 // Fallback to history search after autofill address.
 add_task(async function check_fallback_after_form_autofill() {
   await setInput("#tel", "", true);
   doKey("down");
   await expectPopup();
   checkMenuEntries(["+1234567890"], false);
