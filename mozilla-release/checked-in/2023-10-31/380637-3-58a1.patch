# HG changeset patch
# User Neil Deakin <neil@mozilla.com>
# Date 1510270959 18000
# Node ID 3a4fbb2ae0221a9cf3986bc18a199d3ddfcdb4b7
# Parent  32359f7bd67c400338a462aa6f0835fd6042d5be
Bug 380637, move reserved key checking into ContentUtils so that it can be shared with menu accesskey checks , r=felipe

diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -158,16 +158,17 @@
 #include "nsIObjectLoadingContent.h"
 #include "nsIObserver.h"
 #include "nsIObserverService.h"
 #include "nsIOfflineCacheUpdate.h"
 #include "nsIParser.h"
 #include "nsIParserUtils.h"
 #include "nsIParserService.h"
 #include "nsIPermissionManager.h"
+#include "nsIRemoteBrowser.h"
 #include "nsIRequest.h"
 #include "nsIRunnable.h"
 #include "nsIScriptContext.h"
 #include "nsIScriptError.h"
 #include "nsIScriptGlobalObject.h"
 #include "nsIScriptObjectPrincipal.h"
 #include "nsIScriptSecurityManager.h"
 #include "nsIScrollable.h"
@@ -10861,16 +10862,50 @@ nsContentUtils::CreateJSValueFromSequenc
       return NS_ERROR_OUT_OF_MEMORY;
     }
   }
 
   aValue.setObject(*array);
   return NS_OK;
 }
 
+/* static */
+bool
+nsContentUtils::ShouldBlockReservedKeys(WidgetKeyboardEvent* aKeyEvent)
+{
+  nsCOMPtr<nsIPrincipal> principal;
+  nsCOMPtr<nsIRemoteBrowser> targetBrowser = do_QueryInterface(aKeyEvent->mOriginalTarget);
+  if (targetBrowser) {
+    targetBrowser->GetContentPrincipal(getter_AddRefs(principal));
+  }
+  else {
+    // Get the top-level document.
+    nsCOMPtr<nsIContent> content = do_QueryInterface(aKeyEvent->mOriginalTarget);
+    if (content) {
+      nsIDocument* doc = content->GetUncomposedDoc();
+      if (doc) {
+        nsCOMPtr<nsIDocShellTreeItem> docShell = doc->GetDocShell();
+        if (docShell && docShell->ItemType() == nsIDocShellTreeItem::typeContent) {
+          nsCOMPtr<nsIDocShellTreeItem> rootItem;
+          docShell->GetSameTypeRootTreeItem(getter_AddRefs(rootItem));
+          if (rootItem && rootItem->GetDocument()) {
+            principal = rootItem->GetDocument()->NodePrincipal();
+          }
+        }
+      }
+    }
+  }
+
+  if (principal) {
+    return nsContentUtils::IsSitePermDeny(principal, "shortcuts");
+  }
+
+  return false;
+}
+
 /* static */ Element*
 nsContentUtils::GetClosestNonNativeAnonymousAncestor(Element* aElement)
 {
   MOZ_ASSERT(aElement);
   MOZ_ASSERT(aElement->IsNativeAnonymous());
 
   Element* e = aElement;
   while (e && e->IsNativeAnonymous()) {
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -3173,16 +3173,22 @@ public:
   CreateJSValueFromSequenceOfObject(JSContext* aCx,
                                     const mozilla::dom::Sequence<JSObject*>& aTransfer,
                                     JS::MutableHandle<JS::Value> aValue);
 
   static bool
   IsWebComponentsEnabled() { return sIsWebComponentsEnabled; }
 
   /**
+   * Returns true if reserved key events should be prevented from being sent
+   * to their target. Instead, the key event should be handled by chrome only.
+   */
+  static bool ShouldBlockReservedKeys(mozilla::WidgetKeyboardEvent* aKeyEvent);
+
+  /**
    * Walks up the tree from aElement until it finds an element that is
    * not native anonymous content.  aElement must be NAC itself.
    */
   static Element* GetClosestNonNativeAnonymousAncestor(Element* aElement);
 
   /**
    * Returns the nsIPluginTag for the plugin we should try to use for a given
    * MIME type.
@@ -3450,16 +3456,17 @@ private:
 #ifndef RELEASE_OR_BETA
   static bool sBypassCSSOMOriginCheck;
 #endif
   static bool sIsScopedStyleEnabled;
   static bool sIsBytecodeCacheEnabled;
   static int32_t sBytecodeCacheStrategy;
   static uint32_t sCookiesLifetimePolicy;
   static uint32_t sCookiesBehavior;
+  static bool sShortcutsCustomized;
 
   static int32_t sPrivacyMaxInnerWidth;
   static int32_t sPrivacyMaxInnerHeight;
 
   class UserInteractionObserver;
   static UserInteractionObserver* sUserInteractionObserver;
 
   static nsHtml5StringParser* sHTMLFragmentParser;
diff --git a/dom/xbl/nsXBLWindowKeyHandler.cpp b/dom/xbl/nsXBLWindowKeyHandler.cpp
--- a/dom/xbl/nsXBLWindowKeyHandler.cpp
+++ b/dom/xbl/nsXBLWindowKeyHandler.cpp
@@ -18,17 +18,16 @@
 #include "nsFocusManager.h"
 #include "nsIURI.h"
 #include "nsNetUtil.h"
 #include "nsContentUtils.h"
 #include "nsXBLPrototypeBinding.h"
 #include "nsPIDOMWindow.h"
 #include "nsIDocShell.h"
 #include "nsIDOMDocument.h"
-#include "nsIRemoteBrowser.h"
 #include "nsISelectionController.h"
 #include "nsIPresShell.h"
 #include "mozilla/EventListenerManager.h"
 #include "mozilla/EventStateManager.h"
 #include "mozilla/HTMLEditor.h"
 #include "mozilla/Move.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/StaticPtr.h"
@@ -773,50 +772,25 @@ nsXBLWindowKeyHandler::WalkHandlersAndEx
 bool
 nsXBLWindowKeyHandler::IsReservedKey(WidgetKeyboardEvent* aKeyEvent,
                                      nsXBLPrototypeHandler* aHandler)
 {
   XBLReservedKey reserved = aHandler->GetIsReserved();
   // reserved="true" means that the key is always reserved. reserved="false"
   // means that the key is never reserved. Otherwise, we check site-specific
   // permissions.
+  if (reserved == XBLReservedKey_False) {
+    return false;
+  }
+
   if (reserved == XBLReservedKey_True) {
     return true;
   }
 
-  if (reserved == XBLReservedKey_Unset) {
-    nsCOMPtr<nsIPrincipal> principal;
-    nsCOMPtr<nsIRemoteBrowser> targetBrowser = do_QueryInterface(aKeyEvent->mOriginalTarget);
-    if (targetBrowser) {
-      targetBrowser->GetContentPrincipal(getter_AddRefs(principal));
-    }
-    else {
-      // Get the top-level document.
-      nsCOMPtr<nsIContent> content = do_QueryInterface(aKeyEvent->mOriginalTarget);
-      if (content) {
-        nsIDocument* doc = content->GetUncomposedDoc();
-        if (doc) {
-          nsCOMPtr<nsIDocShellTreeItem> docShell = doc->GetDocShell();
-          if (docShell && docShell->ItemType() == nsIDocShellTreeItem::typeContent) {
-            nsCOMPtr<nsIDocShellTreeItem> rootItem;
-            docShell->GetSameTypeRootTreeItem(getter_AddRefs(rootItem));
-            if (rootItem && rootItem->GetDocument()) {
-              principal = rootItem->GetDocument()->NodePrincipal();
-            }
-          }
-        }
-      }
-    }
-
-    if (principal) {
-      return nsContentUtils::IsSitePermDeny(principal, "shortcuts");
-    }
-  }
-
-  return false;
+  return nsContentUtils::ShouldBlockReservedKeys(aKeyEvent);
 }
 
 bool
 nsXBLWindowKeyHandler::HasHandlerForEvent(nsIDOMKeyEvent* aEvent,
                                           bool* aOutReservedForChrome)
 {
   WidgetKeyboardEvent* widgetKeyboardEvent =
     aEvent->AsEvent()->WidgetEventPtr()->AsKeyboardEvent();
