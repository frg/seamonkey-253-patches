# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1509486907 -39600
#      Wed Nov 01 08:55:07 2017 +1100
# Node ID 870f7ca1b57a0b68f123eca351bea20d2ae1922e
# Parent  deed1dd5ae25c4135baa8991f281813c3b458c4c
Bug 1413085 - Make PREF_Get{CString,Int,Bool}Pref() more uniform. r=glandium.

This patch does the following.

- Reduces nesting and simplifies control flow by handling failure cases
  earlier, and makes all three functions have identical structure.

- Avoids unnecessary temporary local variables, including |rv|.

- Removes low-value comments, including some misleading ones (e.g. "This
  function will perform conversion...").

- PREF_GetCStringValue() previously did not check HasDefaultValue(), unlike the
  other two. It now does.

- PREF_GetCStringValue() now calls SetIsVoid(true) at the start, so that
  aValueOut will be Voided on all failure paths.

MozReview-Commit-ID: 2uCSv76Y8eu

diff --git a/modules/libpref/Preferences.cpp b/modules/libpref/Preferences.cpp
--- a/modules/libpref/Preferences.cpp
+++ b/modules/libpref/Preferences.cpp
@@ -687,106 +687,100 @@ PREF_HasUserPref(const char* aPrefName)
   return pref && pref->mPrefFlags.HasUserValue();
 }
 
 static nsresult
 PREF_GetCStringPref(const char* aPrefName,
                     nsACString& aValueOut,
                     bool aGetDefault)
 {
+  aValueOut.SetIsVoid(true);
+
   if (!gHashTable) {
     return NS_ERROR_NOT_INITIALIZED;
   }
 
+  PrefHashEntry* pref = pref_HashTableLookup(aPrefName);
+  if (!pref || !pref->mPrefFlags.IsTypeString()) {
+    return NS_ERROR_UNEXPECTED;
+  }
+
   const char* stringVal = nullptr;
-  PrefHashEntry* pref = pref_HashTableLookup(aPrefName);
-
-  if (pref && pref->mPrefFlags.IsTypeString()) {
-    if (aGetDefault || pref->mPrefFlags.IsLocked() ||
-        !pref->mPrefFlags.HasUserValue()) {
-      stringVal = pref->mDefaultPref.mStringVal;
-    } else {
-      stringVal = pref->mUserPref.mStringVal;
+  if (aGetDefault || pref->mPrefFlags.IsLocked() ||
+      !pref->mPrefFlags.HasUserValue()) {
+
+    // Do we have a default?
+    if (!pref->mPrefFlags.HasDefault()) {
+      return NS_ERROR_UNEXPECTED;
     }
+    stringVal = pref->mDefaultPref.mStringVal;
+  } else {
+    stringVal = pref->mUserPref.mStringVal;
   }
 
   if (!stringVal) {
-    aValueOut.SetIsVoid(true);
     return NS_ERROR_UNEXPECTED;
   }
 
   aValueOut = stringVal;
   return NS_OK;
 }
 
-// Get an int preference. This function takes a dotted notation of the
-// preference name (e.g. "browser.startup.homepage")
-//
-// It also takes a pointer to fill in with the return value and return an error
-// value. At the moment, this is simply an int but it may be converted to an
-// enum once the global error strategy is worked out.
-//
-// This function will perform conversion if the type doesn't match what was
-// requested. (If it is reasonably possible.)
 static nsresult
 PREF_GetIntPref(const char* aPrefName, int32_t* aValueOut, bool aGetDefault)
 {
   if (!gHashTable) {
     return NS_ERROR_NOT_INITIALIZED;
   }
 
-  nsresult rv = NS_ERROR_UNEXPECTED;
   PrefHashEntry* pref = pref_HashTableLookup(aPrefName);
-  if (pref && pref->mPrefFlags.IsTypeInt()) {
-    if (aGetDefault || pref->mPrefFlags.IsLocked() ||
-        !pref->mPrefFlags.HasUserValue()) {
-      int32_t tempInt = pref->mDefaultPref.mIntVal;
-
-      // Check to see if we even had a default.
-      if (!pref->mPrefFlags.HasDefault()) {
-        return NS_ERROR_UNEXPECTED;
-      }
-      *aValueOut = tempInt;
-    } else {
-      *aValueOut = pref->mUserPref.mIntVal;
+  if (!pref || !pref->mPrefFlags.IsTypeInt()) {
+    return NS_ERROR_UNEXPECTED;
+  }
+
+  if (aGetDefault || pref->mPrefFlags.IsLocked() ||
+      !pref->mPrefFlags.HasUserValue()) {
+
+    // Do we have a default?
+    if (!pref->mPrefFlags.HasDefault()) {
+      return NS_ERROR_UNEXPECTED;
     }
-    rv = NS_OK;
-  }
-
-  return rv;
-}
-
-// Like PREF_GetIntPref(), but for booleans.
+    *aValueOut = pref->mDefaultPref.mIntVal;
+  } else {
+    *aValueOut = pref->mUserPref.mIntVal;
+  }
+
+  return NS_OK;
+}
+
 static nsresult
 PREF_GetBoolPref(const char* aPrefName, bool* aValueOut, bool aGetDefault)
 {
   if (!gHashTable) {
     return NS_ERROR_NOT_INITIALIZED;
   }
 
-  nsresult rv = NS_ERROR_UNEXPECTED;
   PrefHashEntry* pref = pref_HashTableLookup(aPrefName);
-  //NS_ASSERTION(pref, aPrefName);
-  if (pref && pref->mPrefFlags.IsTypeBool()) {
-    if (aGetDefault || pref->mPrefFlags.IsLocked() ||
-        !pref->mPrefFlags.HasUserValue()) {
-      bool tempBool = pref->mDefaultPref.mBoolVal;
-
-      // Check to see if we even had a default.
-      if (pref->mPrefFlags.HasDefault()) {
-        *aValueOut = tempBool;
-        rv = NS_OK;
-      }
-    } else {
-      *aValueOut = pref->mUserPref.mBoolVal;
-      rv = NS_OK;
+  if (!pref || !pref->mPrefFlags.IsTypeBool()) {
+    return NS_ERROR_UNEXPECTED;
+  }
+
+  if (aGetDefault || pref->mPrefFlags.IsLocked() ||
+      !pref->mPrefFlags.HasUserValue()) {
+
+    // Do we have a default?
+    if (!pref->mPrefFlags.HasDefault()) {
+      return NS_ERROR_UNEXPECTED;
     }
-  }
-
-  return rv;
+    *aValueOut = pref->mDefaultPref.mBoolVal;
+  } else {
+    *aValueOut = pref->mUserPref.mBoolVal;
+  }
+
+  return NS_OK;
 }
 
 // Clears the given pref (reverts it to its default value).
 static nsresult
 PREF_ClearUserPref(const char* aPrefName)
 {
   if (!gHashTable) {
     return NS_ERROR_NOT_INITIALIZED;
