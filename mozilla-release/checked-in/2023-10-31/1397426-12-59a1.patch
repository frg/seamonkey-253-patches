# HG changeset patch
# User Mike Conley <mconley@mozilla.com>
# Date 1513881652 18000
# Node ID 2cfdaedf635996efdc462bb12d5075b84b3e93e8
# Parent  c7653a9309b0a53925f28fabc75b3584a314a524
Bug 1397426 - Ensure we attempt to activate docShell of current tab after reacting to sidemode or occlusionstate change events. r=billm

During the macOS fullscreen mode change, the window entering fullscreen reports being occluded
for a brief time (presumably due to the "stretch" effect that macOS is applying overtop of the
window). This causes us to preserve the layers for the selected tab, and deactivate its docShell.

According to https://bugzilla.mozilla.org/show_bug.cgi?id=1279086#c34, we preserve layers like this
when minimizing or fully occluding the window. This way, we avoid a flicker when the window is
un-minimized or partially revealed.

However, if during that transition, the mouse hovers over a background tab (which is the
case during the automated tests) and an async tab switcher doesn't already exist, we can
trigger a warming attempt on that background tab, which creates an async tab switcher, and
causes the switcher to run its init() method.

Beyond setting some initial state, the init() method also makes it so that the initial browser
at the time of calling init() sets preserve layers to false.

If we get into this situation, then what we end up with is a tab that is rendering layers,
is no longer preserving them, but the docShell is not activated.

Soon after, the async tab switcher responds to the occlusionstatechange event when the window
is revealed again. Before this patch, we did nothing, since it was assumed that postActions
would eventually activate the docShell. However, since warming isn't currently activated by
default, the async tab switcher soon destroys itself, and the initial tab's docShell is never
activated. This causes the browser_domFullscreen_fullscreenMode.js test to hang (and probably
other bugs as well).

This patch makes it so that if we see an occlusionstatechange or sizemodechange event when
the async tab switcher exists, and the window is no longer minimized or fully occluded, that
we attempt to activate the currently selected tab's docShell.

MozReview-Commit-ID: yJTUj8bBrs

diff --git a/browser/base/content/tabbrowser.xml b/browser/base/content/tabbrowser.xml
--- a/browser/base/content/tabbrowser.xml
+++ b/browser/base/content/tabbrowser.xml
@@ -4729,18 +4729,19 @@
                   }
                 }
                 if (this.loadTimer) {
                   this.clearTimer(this.loadTimer);
                   this.loadTimer = null;
                 }
                 this.loadingTab = null;
               } else {
-                // Do nothing. We'll automatically start loading the requested tab in
-                // postActions.
+                // We're no longer minimized or occluded. This means we might want
+                // to activate the current tab's docShell.
+                this.maybeActivateDocShell(gBrowser.selectedTab);
               }
             },
 
             onSwapDocShells(ourBrowser, otherBrowser) {
               // This event fires before the swap. ourBrowser is from
               // our window. We save the state of otherBrowser since ourBrowser
               // needs to take on that state at the end of the swap.
 
