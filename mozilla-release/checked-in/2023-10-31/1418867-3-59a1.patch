# HG changeset patch
# User Hiroyuki Ikezoe <hikezoe@mozilla.com>
# Date 1511316220 -32400
# Node ID 0fe9ca39473a4c71adaa53d6c9e41552ccab6099
# Parent  80f6fef318486bea53fd84fe6f7032e1736456e4
Bug 1418867 - Pass element or pseudo element to Servo_StyleSet_GetBaseComputedValuesForElement(). r=emilio

MozReview-Commit-ID: Ae3iZ6g3x3c

diff --git a/dom/animation/KeyframeEffectReadOnly.cpp b/dom/animation/KeyframeEffectReadOnly.cpp
--- a/dom/animation/KeyframeEffectReadOnly.cpp
+++ b/dom/animation/KeyframeEffectReadOnly.cpp
@@ -549,21 +549,23 @@ KeyframeEffectReadOnly::EnsureBaseStyle(
     }
   }
 
   if (!hasAdditiveValues) {
     return;
   }
 
   if (!aBaseStyleContext) {
+    Element* animatingElement =
+      EffectCompositor::GetElementToRestyle(mTarget->mElement,
+                                            mTarget->mPseudoType);
     aBaseStyleContext =
       aPresContext->StyleSet()->AsServo()->GetBaseContextForElement(
-          mTarget->mElement,
+          animatingElement,
           aPresContext,
-          mTarget->mPseudoType,
           aComputedStyle);
   }
   RefPtr<RawServoAnimationValue> baseValue =
     Servo_ComputedValues_ExtractAnimationValue(aBaseStyleContext,
                                                aProperty.mProperty).Consume();
   mBaseStyleValuesForServo.Put(aProperty.mProperty, baseValue);
 }
 
diff --git a/layout/style/ServoBindingList.h b/layout/style/ServoBindingList.h
--- a/layout/style/ServoBindingList.h
+++ b/layout/style/ServoBindingList.h
@@ -685,18 +685,17 @@ SERVO_BINDING_FUNC(Servo_IsWorkerThread,
 SERVO_BINDING_FUNC(Servo_MaybeGCRuleTree, void, RawServoStyleSetBorrowed set)
 
 // Returns computed values for the given element without any animations rules.
 SERVO_BINDING_FUNC(Servo_StyleSet_GetBaseComputedValuesForElement,
                    ServoStyleContextStrong,
                    RawServoStyleSetBorrowed set,
                    RawGeckoElementBorrowed element,
                    ServoStyleContextBorrowed existing_style,
-                   const mozilla::ServoElementSnapshotTable* snapshots,
-                   mozilla::CSSPseudoElementType pseudo_type)
+                   const mozilla::ServoElementSnapshotTable* snapshots)
 // Returns computed values for the given element by adding an animation value.
 SERVO_BINDING_FUNC(Servo_StyleSet_GetComputedValuesByAddingAnimation,
                    ServoStyleContextStrong,
                    RawServoStyleSetBorrowed set,
                    RawGeckoElementBorrowed element,
                    ServoStyleContextBorrowed existing_style,
                    const mozilla::ServoElementSnapshotTable* snapshots,
                    RawServoAnimationValueBorrowed animation)
diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -1104,24 +1104,22 @@ ServoStyleSet::GetAnimationValues(
                            mRawSet.get(),
                            &aAnimationValues);
 }
 
 already_AddRefed<ServoStyleContext>
 ServoStyleSet::GetBaseContextForElement(
   Element* aElement,
   nsPresContext* aPresContext,
-  CSSPseudoElementType aPseudoType,
   const ServoStyleContext* aStyle)
 {
   return Servo_StyleSet_GetBaseComputedValuesForElement(mRawSet.get(),
                                                         aElement,
                                                         aStyle,
-                                                        &Snapshots(),
-                                                        aPseudoType).Consume();
+                                                        &Snapshots()).Consume();
 }
 
 already_AddRefed<ServoStyleContext>
 ServoStyleSet::ResolveServoStyleByAddingAnimation(
   Element* aElement,
   const ServoStyleContext* aStyle,
   RawServoAnimationValue* aAnimationValue)
 {
diff --git a/layout/style/ServoStyleSet.h b/layout/style/ServoStyleSet.h
--- a/layout/style/ServoStyleSet.h
+++ b/layout/style/ServoStyleSet.h
@@ -387,17 +387,16 @@ public:
   nsCSSCounterStyleRule* CounterStyleRuleForName(nsIAtom* aName);
 
   // Get all the currently-active font feature values set.
   already_AddRefed<gfxFontFeatureValueSet> BuildFontFeatureValueSet();
 
   already_AddRefed<ServoStyleContext>
   GetBaseContextForElement(dom::Element* aElement,
                            nsPresContext* aPresContext,
-                           CSSPseudoElementType aPseudoType,
                            const ServoStyleContext* aStyle);
 
   // Get a style context that represents |aStyle|, but as though
   // it additionally matched the rules of the newly added |aAnimaitonaValue|.
   // We use this function to temporarily generate a ServoStyleContext for
   // calculating the cumulative change hints.
   // This must hold:
   //   The additional rules must be appropriate for the transition
diff --git a/layout/style/nsComputedDOMStyle.cpp b/layout/style/nsComputedDOMStyle.cpp
--- a/layout/style/nsComputedDOMStyle.cpp
+++ b/layout/style/nsComputedDOMStyle.cpp
@@ -744,18 +744,19 @@ nsComputedDOMStyle::DoGetStyleContextNoF
                      eRestyle_AllHintsWithAnimations);
           } else {
             Element* elementOrPseudoElement =
               EffectCompositor::GetElementToRestyle(aElement, pseudoType);
             if (!elementOrPseudoElement) {
               return nullptr;
             }
             return presContext->StyleSet()->AsServo()->
-              GetBaseContextForElement(aElement, presContext,
-                                       pseudoType, result->AsServo());
+              GetBaseContextForElement(elementOrPseudoElement,
+                                       presContext,
+                                       result->AsServo());
           }
         }
 
         // this function returns an addrefed style context
         RefPtr<nsStyleContext> ret = result;
         return ret.forget();
       }
     }
@@ -782,18 +783,19 @@ nsComputedDOMStyle::DoGetStyleContextNoF
       return result.forget();
     }
 
     Element* elementOrPseudoElement =
       EffectCompositor::GetElementToRestyle(aElement, pseudoType);
     if (!elementOrPseudoElement) {
       return nullptr;
     }
-    return servoSet->GetBaseContextForElement(aElement, presContext,
-                                              pseudoType, result);
+    return servoSet->GetBaseContextForElement(elementOrPseudoElement,
+                                              presContext,
+                                              result);
   }
 
   RefPtr<GeckoStyleContext> parentContext;
   nsIContent* parent = aPseudo ? aElement : aElement->GetParent();
   // Don't resolve parent context for document fragments.
   if (parent && parent->IsElement()) {
     RefPtr<nsStyleContext> p =
       GetStyleContextNoFlush(parent->AsElement(), nullptr,
