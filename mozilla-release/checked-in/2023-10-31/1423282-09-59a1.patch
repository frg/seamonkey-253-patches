# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1512508128 0
# Node ID 60511611ac52f7b6fd46131b07a26ae79d21b6c1
# Parent  ae00358dfafbca03e524065814d9314a89dc4972
Bug 1423282 - Drop Marionette:emitTouchEvent IPC message and related infra. r=automatedtester,maja_zf

This removes the Marionette:emitTouchEvent IPC message which is
currently not in use by any tests.  Along with removing this message
listener we can get rid of a tonne of complicated infrastructure
in testing/marionette/frame.js.

On switching the content frame we no longer await frame scripts to
register themselves because they implicitly inherit the parent's
frame script in Firefox/Fennec.  This was a relic from the B2G days
when each frame was OOP.

MozReview-Commit-ID: 5vxrWHjzd68

diff --git a/testing/marionette/driver.js b/testing/marionette/driver.js
--- a/testing/marionette/driver.js
+++ b/testing/marionette/driver.js
@@ -500,34 +500,16 @@ GeckoDriver.prototype.getVisibleText = f
 /**
  * Handles registration of new content listener browsers.  Depending on
  * their type they are either accepted or ignored.
  */
 GeckoDriver.prototype.registerBrowser = function(id, be) {
   let nullPrevious = this.curBrowser.curFrameId === null;
   let listenerWindow = Services.wm.getOuterWindowWithId(id);
 
-  // go in here if we're already in a remote frame
-  if (this.curBrowser.frameManager.currentRemoteFrame !== null &&
-      (!listenerWindow || this.mm == this.curBrowser.frameManager
-          .currentRemoteFrame.messageManager.get())) {
-    // The outerWindowID from an OOP frame will not be meaningful to
-    // the parent process here, since each process maintains its own
-    // independent window list.  So, it will either be null (!listenerWindow)
-    // if we're already in a remote frame, or it will point to some
-    // random window, which will hopefully cause an href mismatch.
-    // Currently this only happens in B2G for OOP frames registered in
-    // Marionette:switchToFrame, so we'll acknowledge the switchToFrame
-    // message here.
-    //
-    // TODO: Should have a better way of determining that this message
-    // is from a remote frame.
-    this.curBrowser.frameManager.currentRemoteFrame.targetFrameId = id;
-  }
-
   // We want to ignore frames that are XUL browsers that aren't in the "main"
   // tabbrowser, but accept things on Fennec (which doesn't have a
   // xul:tabbrowser), and accept HTML iframes (because tests depend on it),
   // as well as XUL frames. Ideally this should be cleaned up and we should
   // keep track of browsers a different way.
   if (this.appId != APP_ID_FIREFOX || be.namespaceURI != XUL_NS ||
       be.nodeName != "browser" || be.getTabBrowser()) {
     // curBrowser holds all the registered frames in knownFrames
@@ -1827,25 +1809,17 @@ GeckoDriver.prototype.switchToFrame = as
       checkTimer.initWithCallback(
           checkLoad.bind(this), 100, Ci.nsITimer.TYPE_ONE_SHOT);
     } else {
       throw new NoSuchFrameError(`Unable to locate frame: ${id}`);
     }
 
   } else if (this.context == Context.Content) {
     cmd.commandID = cmd.id;
-
-    let res = await this.listener.switchToFrame(cmd.parameters);
-    if (res) {
-      let {win: winId, frame: frameId} = res;
-      this.mm = this.curBrowser.frameManager.getFrameMM(winId, frameId);
-
-      await this.registerPromise();
-      await this.listeningPromise();
-    }
+    await this.listener.switchToFrame(cmd.parameters);
   }
 };
 
 GeckoDriver.prototype.getTimeouts = function() {
   return this.timeouts;
 };
 
 /**
@@ -3349,21 +3323,16 @@ GeckoDriver.prototype.receiveMessage = f
           this.currentFrameElement =
               new ChromeWebElement(message.json.frameValue);
         } else {
           this.currentFrameElement = null;
         }
       }
       break;
 
-    case "Marionette:emitTouchEvent":
-      globalMessageManager.broadcastAsyncMessage(
-          "MarionetteMainListener:emitTouchEvent", message.json);
-      break;
-
     case "Marionette:register":
       let wid = message.json.value;
       let be = message.target;
       let rv = this.registerBrowser(wid, be);
       return rv;
 
     case "Marionette:listenersAttached":
       if (message.json.listenerId === this.curBrowser.curFrameId) {
diff --git a/testing/marionette/frame.js b/testing/marionette/frame.js
--- a/testing/marionette/frame.js
+++ b/testing/marionette/frame.js
@@ -6,116 +6,37 @@
 
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 
 this.EXPORTED_SYMBOLS = ["frame"];
 
 /** @namespace */
 this.frame = {};
 
-const FRAME_SCRIPT = "chrome://marionette/content/listener.js";
-
-/**
- * An object representing a frame that Marionette has loaded a
- * frame script in.
- */
-frame.RemoteFrame = function(windowId, frameId) {
-  // outerWindowId relative to main process
-  this.windowId = windowId;
-  // actual frame relative to the windowId's frames list
-  this.frameId = frameId;
-  // assigned frame ID, used for messaging
-  this.targetFrameId = this.frameId;
-  // list of OOP frames that has the frame script loaded
-  this.remoteFrames = [];
-};
-
 /**
  * The FrameManager will maintain the list of Out Of Process (OOP)
  * frames and will handle frame switching between them.
  *
- * It handles explicit frame switching (switchToFrame), and implicit
- * frame switching, which occurs when a modal dialog is triggered in B2G.
- *
  * @param {GeckoDriver} driver
  *     Reference to the driver instance.
  */
 frame.Manager = class {
   constructor(driver) {
-    // messageManager maintains the messageManager
-    // for the current process' chrome frame or the global message manager
-
-    // holds a member of the remoteFrames (for an OOP frame)
-    // or null (for the main process)
-    this.currentRemoteFrame = null;
-    // frame we'll need to restore once interrupt is gone
-    this.previousRemoteFrame = null;
     this.driver = driver;
   }
 
-  getOopFrame(winId, frameId) {
-    // get original frame window
-    let outerWin = Services.wm.getOuterWindowWithId(winId);
-    // find the OOP frame
-    let f = outerWin.document.getElementsByTagName("iframe")[frameId];
-    return f;
-  }
-
-  getFrameMM(winId, frameId) {
-    let oopFrame = this.getOopFrame(winId, frameId);
-    let mm = oopFrame.frameLoader.messageManager;
-    return mm;
-  }
-
-  /**
-   * Switch to OOP frame.  We're handling this here so we can maintain
-   * a list of remote frames.
-   */
-  switchToFrame(winId, frameId) {
-    let oopFrame = this.getOopFrame(winId, frameId);
-    let mm = this.getFrameMM(winId, frameId);
-
-    // see if this frame already has our frame script loaded in it;
-    // if so, just wake it up
-    for (let i = 0; i < this.remoteFrames.length; i++) {
-      let f = this.remoteFrames[i];
-      let fmm = f.messageManager.get();
-
-      if (fmm == mm) {
-        this.currentRemoteFrame = f;
-        this.addMessageManagerListeners(mm);
-
-        return oopFrame.id;
-      }
-    }
-
-    // if we get here, then we need to load the frame script in this frame,
-    // and set the frame's ChromeMessageSender as the active message manager
-    // the driver will listen to.
-    this.addMessageManagerListeners(mm);
-    let f = new frame.RemoteFrame(winId, frameId);
-    f.messageManager = Cu.getWeakReference(mm);
-    this.remoteFrames.push(f);
-    this.currentRemoteFrame = f;
-
-    mm.loadFrameScript(FRAME_SCRIPT, true, true);
-
-    return oopFrame.id;
-  }
-
   /**
    * Adds message listeners to the driver,  listening for
    * messages from content frame scripts.
    *
    * @param {nsIMessageListenerManager} mm
    *     The message manager object, typically
    *     ChromeMessageBroadcaster or ChromeMessageSender.
    */
   addMessageManagerListeners(mm) {
-    mm.addWeakMessageListener("Marionette:emitTouchEvent", this.driver);
     mm.addWeakMessageListener("Marionette:switchedToFrame", this.driver);
     mm.addWeakMessageListener("Marionette:getVisibleCookies", this.driver);
     mm.addWeakMessageListener("Marionette:register", this.driver);
     mm.addWeakMessageListener("Marionette:listenersAttached", this.driver);
     mm.addWeakMessageListener("Marionette:GetLogLevel", this.driver);
   }
 
   /**
diff --git a/testing/marionette/listener.js b/testing/marionette/listener.js
--- a/testing/marionette/listener.js
+++ b/testing/marionette/listener.js
@@ -1438,32 +1438,23 @@ function switchToParentFrame(msg) {
 
 /**
  * Switch to frame given either the server-assigned element id,
  * its index in window.frames, or the iframe's name or id.
  */
 function switchToFrame(msg) {
   let commandID = msg.json.commandID;
   let foundFrame = null;
+
+  // check if curContainer.frame reference is dead
   let frames = [];
-  let parWindow = null;
-
-  // Check of the curContainer.frame reference is dead
   try {
     frames = curContainer.frame.frames;
-    // Until Bug 761935 lands, we won't have multiple nested OOP
-    // iframes. We will only have one.  parWindow will refer to the iframe
-    // above the nested OOP frame.
-    parWindow = curContainer.frame.QueryInterface(Ci.nsIInterfaceRequestor)
-        .getInterface(Ci.nsIDOMWindowUtils).outerWindowID;
   } catch (e) {
-    // We probably have a dead compartment so accessing it is going to
-    // make Firefox very upset. Let's now try redirect everything to the
-    // top frame even if the user has given us a frame since search doesnt
-    // look up.
+    // dead comparment, redirect to top frame
     msg.json.id = null;
     msg.json.element = null;
   }
 
   if ((msg.json.id === null || msg.json.id === undefined) &&
       (msg.json.element == null)) {
     // returning to root frame
     sendSyncMessage("Marionette:switchedToFrame", {frameValue: null});
@@ -1562,32 +1553,22 @@ function switchToFrame(msg) {
     return;
   }
 
   // send a synchronous message to let the server update the currently active
   // frame element (for getActiveFrame)
   let frameWebEl = seenEls.add(curContainer.frame.wrappedJSObject);
   sendSyncMessage("Marionette:switchedToFrame", {"frameValue": frameWebEl.uuid});
 
-  if (curContainer.frame.contentWindow === null) {
-    // The frame we want to switch to is a remote/OOP frame;
-    // notify our parent to handle the switch
-    curContainer.frame = content;
-    let rv = {win: parWindow, frame: foundFrame};
-    sendResponse(rv, commandID);
+  curContainer.frame = curContainer.frame.contentWindow;
+  if (msg.json.focus) {
+    curContainer.frame.focus();
+  }
 
-  } else {
-    curContainer.frame = curContainer.frame.contentWindow;
-
-    if (msg.json.focus) {
-      curContainer.frame.focus();
-    }
-
-    sendOk(commandID);
-  }
+  sendOk(commandID);
 }
 
 /**
  * Perform a screen capture in content context.
  *
  * Accepted values for |opts|:
  *
  *     @param {UUID=} id
