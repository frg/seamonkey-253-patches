# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1512015672 -32400
# Node ID fba5a40806a6f8c636e0a91bdc4460c126e75555
# Parent  e69708533efd72913c2de5ce86364f265cbb9a34
Bug 1421877 - Fix some warnings which hit (almost) always in EditorDOMPointBase r=m_kato

There are 3 spam warning assertion in EditorDOMPointBase.

One is in constructor. Which warns if IsSet() returns false, but it checks
before finishing initializing.

The others are in IsStartOfContainer() and IsEndOfContainer().  They try to
check if mOffset is valid value with current DOM tree if it's initialized
when it's at start or end of the container.  However, they do it even when
it's not start nor end of the container.

This patch fixes those spammers.

MozReview-Commit-ID: DvFa501m9V0

diff --git a/editor/libeditor/EditorDOMPoint.h b/editor/libeditor/EditorDOMPoint.h
--- a/editor/libeditor/EditorDOMPoint.h
+++ b/editor/libeditor/EditorDOMPoint.h
@@ -107,21 +107,21 @@ public:
    */
   explicit EditorDOMPointBase(nsINode* aPointedNode)
     : mParent(aPointedNode && aPointedNode->IsContent() ?
                 aPointedNode->GetParentNode() : nullptr)
     , mChild(aPointedNode && aPointedNode->IsContent() ?
                 aPointedNode->AsContent() : nullptr)
     , mIsChildInitialized(false)
   {
+    mIsChildInitialized = aPointedNode && mChild;
     NS_WARNING_ASSERTION(IsSet(),
       "The child is nullptr or doesn't have its parent");
     NS_WARNING_ASSERTION(mChild && mChild->GetParentNode() == mParent,
       "Initializing RangeBoundary with invalid value");
-    mIsChildInitialized = aPointedNode && mChild;
   }
 
   EditorDOMPointBase(nsINode* aContainer,
                      nsIContent* aPointedNode,
                      int32_t aOffset)
     : mParent(aContainer)
     , mChild(aPointedNode)
     , mOffset(mozilla::Some(aOffset))
@@ -426,19 +426,25 @@ public:
     //   If mChild isn't initialized and the offset is 0.
     if (NS_WARN_IF(!mParent)) {
       return false;
     }
     if (!mParent->IsContainerNode()) {
       return !mOffset.value();
     }
     if (mIsChildInitialized) {
-      NS_WARNING_ASSERTION(!mOffset.isSome() || !mOffset.value(),
-        "If offset was initialized, mOffset should be 0");
-      return mParent->GetFirstChild() == mChild;
+      if (mParent->GetFirstChild() == mChild) {
+        NS_WARNING_ASSERTION(!mOffset.isSome() || !mOffset.value(),
+          "If mOffset was initialized, it should be 0");
+        return true;
+      }
+      NS_WARNING_ASSERTION(!mOffset.isSome() ||
+                           mParent->GetChildAt(mOffset.value()) == mChild,
+        "If mOffset and mChild are mismatched");
+      return false;
     }
     MOZ_ASSERT(mOffset.isSome());
     return !mOffset.value();
   }
 
   bool
   IsEndOfContainer() const
   {
@@ -447,20 +453,26 @@ public:
     //   length of the container.
     //   If mChild is initialized and it's nullptr.
     //   If mChild isn't initialized and mOffset is same as the length of the
     //   container.
     if (NS_WARN_IF(!mParent)) {
       return false;
     }
     if (mIsChildInitialized) {
+      if (!mChild) {
+        NS_WARNING_ASSERTION(!mOffset.isSome() ||
+                             mOffset.value() == mParent->Length(),
+          "If mOffset was initialized, it should be length of the container");
+        return true;
+      }
       NS_WARNING_ASSERTION(!mOffset.isSome() ||
-                           mOffset.value() == mParent->Length(),
-        "If offset was initialized, mOffset should be length of the container");
-      return !mChild;
+                           mParent->GetChildAt(mOffset.value()) == mChild,
+        "If mOffset and mChild are mismatched");
+      return false;
     }
     MOZ_ASSERT(mOffset.isSome());
     return mOffset.value() == mParent->Length();
   }
 
   // Convenience methods for switching between the two types
   // of EditorDOMPointBase.
   EditorDOMPointBase<nsINode*, nsIContent*>
