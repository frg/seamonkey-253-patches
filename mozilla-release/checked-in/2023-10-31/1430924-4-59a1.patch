# HG changeset patch
# User Hiroyuki Ikezoe <hikezoe@mozilla.com>
# Date 1516144381 -32400
# Node ID 0d52b29a58e7d3d9212bde50433499d1c66c41f9
# Parent  a71583b5a3e2002978b57ca90a147775a5cdc46e
Bug 1430924 - Create animation event struct directly. r=boris

We don't need to create AnimationEventParams, we can direcly create
AnimationEventInfo array and move it to dispatcher.

MozReview-Commit-ID: BCzKGAV8VTw

diff --git a/layout/style/nsAnimationManager.cpp b/layout/style/nsAnimationManager.cpp
--- a/layout/style/nsAnimationManager.cpp
+++ b/layout/style/nsAnimationManager.cpp
@@ -37,26 +37,16 @@ using namespace mozilla::css;
 using mozilla::dom::Animation;
 using mozilla::dom::AnimationEffectReadOnly;
 using mozilla::dom::AnimationPlayState;
 using mozilla::dom::KeyframeEffectReadOnly;
 using mozilla::dom::CSSAnimation;
 
 typedef mozilla::ComputedTiming::AnimationPhase AnimationPhase;
 
-namespace {
-
-struct AnimationEventParams {
-  EventMessage mMessage;
-  StickyTimeDuration mElapsedTime;
-  TimeStamp mTimeStamp;
-};
-
-} // anonymous namespace
-
 ////////////////////////// CSSAnimation ////////////////////////////
 
 JSObject*
 CSSAnimation::WrapObject(JSContext* aCx, JS::Handle<JSObject*> aGivenProto)
 {
   return dom::CSSAnimationBinding::Wrap(aCx, this, aGivenProto);
 }
 
@@ -237,95 +227,80 @@ CSSAnimation::QueueEvents(StickyTimeDura
       computedTiming.mDuration.MultDouble(
         (iterationBoundary - computedTiming.mIterationStart));
   }
 
   TimeStamp startTimeStamp     = ElapsedTimeToTimeStamp(intervalStartTime);
   TimeStamp endTimeStamp       = ElapsedTimeToTimeStamp(intervalEndTime);
   TimeStamp iterationTimeStamp = ElapsedTimeToTimeStamp(iterationStartTime);
 
-  AutoTArray<AnimationEventParams, 2> events;
+  AutoTArray<AnimationEventInfo, 2> events;
+
+  auto appendAnimationEvent = [&](EventMessage aMessage,
+                                  StickyTimeDuration aElapsedTime,
+                                  TimeStamp aTimeStamp) {
+    events.AppendElement(AnimationEventInfo(mOwningElement.Target(),
+                                            aMessage,
+                                            mAnimationName,
+                                            aElapsedTime,
+                                            aTimeStamp,
+                                            this));
+  };
 
   // Handle cancel event first
   if ((mPreviousPhase != AnimationPhase::Idle &&
        mPreviousPhase != AnimationPhase::After) &&
       currentPhase == AnimationPhase::Idle) {
     TimeStamp activeTimeStamp = ElapsedTimeToTimeStamp(aActiveTime);
-    events.AppendElement(AnimationEventParams{ eAnimationCancel,
-                                               aActiveTime,
-                                               activeTimeStamp });
+    appendAnimationEvent(eAnimationCancel, aActiveTime, activeTimeStamp);
   }
 
   switch (mPreviousPhase) {
     case AnimationPhase::Idle:
     case AnimationPhase::Before:
       if (currentPhase == AnimationPhase::Active) {
-        events.AppendElement(AnimationEventParams{ eAnimationStart,
-                                                   intervalStartTime,
-                                                   startTimeStamp });
+        appendAnimationEvent(eAnimationStart,
+                             intervalStartTime,
+                             startTimeStamp);
       } else if (currentPhase == AnimationPhase::After) {
-        events.AppendElement(AnimationEventParams{ eAnimationStart,
-                                                   intervalStartTime,
-                                                   startTimeStamp });
-        events.AppendElement(AnimationEventParams{ eAnimationEnd,
-                                                   intervalEndTime,
-                                                   endTimeStamp });
+        appendAnimationEvent(eAnimationStart,
+                             intervalStartTime,
+                             startTimeStamp);
+        appendAnimationEvent(eAnimationEnd, intervalEndTime, endTimeStamp);
       }
       break;
     case AnimationPhase::Active:
       if (currentPhase == AnimationPhase::Before) {
-        events.AppendElement(AnimationEventParams{ eAnimationEnd,
-                                                   intervalStartTime,
-                                                   startTimeStamp });
+        appendAnimationEvent(eAnimationEnd, intervalStartTime, startTimeStamp);
       } else if (currentPhase == AnimationPhase::Active) {
         // The currentIteration must have changed or element we would have
         // returned early above.
         MOZ_ASSERT(currentIteration != mPreviousIteration);
-        events.AppendElement(AnimationEventParams{ eAnimationIteration,
-                                                   iterationStartTime,
-                                                   iterationTimeStamp });
+        appendAnimationEvent(eAnimationIteration,
+                             iterationStartTime,
+                             iterationTimeStamp);
       } else if (currentPhase == AnimationPhase::After) {
-        events.AppendElement(AnimationEventParams{ eAnimationEnd,
-                                                   intervalEndTime,
-                                                   endTimeStamp });
+        appendAnimationEvent(eAnimationEnd, intervalEndTime, endTimeStamp);
       }
       break;
     case AnimationPhase::After:
       if (currentPhase == AnimationPhase::Before) {
-        events.AppendElement(AnimationEventParams{ eAnimationStart,
-                                                   intervalEndTime,
-                                                   startTimeStamp});
-        events.AppendElement(AnimationEventParams{ eAnimationEnd,
-                                                   intervalStartTime,
-                                                   endTimeStamp });
+        appendAnimationEvent(eAnimationStart, intervalEndTime, startTimeStamp);
+        appendAnimationEvent(eAnimationEnd, intervalStartTime, endTimeStamp);
       } else if (currentPhase == AnimationPhase::Active) {
-        events.AppendElement(AnimationEventParams{ eAnimationStart,
-                                                   intervalEndTime,
-                                                   endTimeStamp });
+        appendAnimationEvent(eAnimationStart, intervalEndTime, endTimeStamp);
       }
       break;
   }
   mPreviousPhase = currentPhase;
   mPreviousIteration = currentIteration;
 
-  if (events.IsEmpty()) {
-    return;
+  if (!events.IsEmpty()) {
+    presContext->AnimationManager()->QueueEvents(Move(events));
   }
-
-  AutoTArray<AnimationEventInfo, 2> animationEvents;
-  for (const AnimationEventParams& event : events){
-    animationEvents.AppendElement(
-      AnimationEventInfo(mOwningElement.Target(),
-                         event.mMessage,
-                         mAnimationName,
-                         event.mElapsedTime,
-                         event.mTimeStamp,
-                         this));
-  }
-  presContext->AnimationManager()->QueueEvents(Move(animationEvents));
 }
 
 void
 CSSAnimation::UpdateTiming(SeekFlag aSeekFlag, SyncNotifyFlag aSyncNotifyFlag)
 {
   if (mNeedsNewAnimationIndexWhenRun &&
       PlayState() != AnimationPlayState::Idle) {
     mAnimationIndex = sNextAnimationIndex++;
diff --git a/layout/style/nsTransitionManager.cpp b/layout/style/nsTransitionManager.cpp
--- a/layout/style/nsTransitionManager.cpp
+++ b/layout/style/nsTransitionManager.cpp
@@ -43,24 +43,16 @@ using mozilla::TimeDuration;
 using mozilla::dom::Animation;
 using mozilla::dom::AnimationPlayState;
 using mozilla::dom::CSSTransition;
 using mozilla::dom::KeyframeEffectReadOnly;
 
 using namespace mozilla;
 using namespace mozilla::css;
 
-namespace {
-struct TransitionEventParams {
-  EventMessage mMessage;
-  StickyTimeDuration mElapsedTime;
-  TimeStamp mTimeStamp;
-};
-} // anonymous namespace
-
 double
 ElementPropertyTransition::CurrentValuePortion() const
 {
   MOZ_ASSERT(!GetLocalTime().IsNull(),
              "Getting the value portion of an animation that's not being "
              "sampled");
 
   // Transitions use a fill mode of 'backwards' so GetComputedTiming will
@@ -251,116 +243,101 @@ CSSTransition::QueueEvents(StickyTimeDur
 
   if (mPendingState != PendingState::NotPending &&
       (mPreviousTransitionPhase == TransitionPhase::Idle ||
        mPreviousTransitionPhase == TransitionPhase::Pending))
   {
     currentPhase = TransitionPhase::Pending;
   }
 
-  AutoTArray<TransitionEventParams, 3> events;
+  AutoTArray<TransitionEventInfo, 3> events;
+
+  auto appendTransitionEvent = [&](EventMessage aMessage,
+                                   StickyTimeDuration aElapsedTime,
+                                   TimeStamp aTimeStamp) {
+    events.AppendElement(TransitionEventInfo(mOwningElement.Target(),
+                                             aMessage,
+                                             TransitionProperty(),
+                                             aElapsedTime,
+                                             aTimeStamp,
+                                             this));
+  };
 
   // Handle cancel events first
   if ((mPreviousTransitionPhase != TransitionPhase::Idle &&
        mPreviousTransitionPhase != TransitionPhase::After) &&
       currentPhase == TransitionPhase::Idle) {
     TimeStamp activeTimeStamp = ElapsedTimeToTimeStamp(aActiveTime);
-    events.AppendElement(TransitionEventParams{ eTransitionCancel,
-                                                aActiveTime,
-                                                activeTimeStamp });
+    appendTransitionEvent(eTransitionCancel, aActiveTime, activeTimeStamp);
   }
 
   // All other events
   switch (mPreviousTransitionPhase) {
     case TransitionPhase::Idle:
       if (currentPhase == TransitionPhase::Pending ||
           currentPhase == TransitionPhase::Before) {
-        events.AppendElement(TransitionEventParams{ eTransitionRun,
-                                                    intervalStartTime,
-                                                    zeroTimeStamp });
+        appendTransitionEvent(eTransitionRun, intervalStartTime, zeroTimeStamp);
       } else if (currentPhase == TransitionPhase::Active) {
-        events.AppendElement(TransitionEventParams{ eTransitionRun,
-                                                    intervalStartTime,
-                                                    zeroTimeStamp });
-        events.AppendElement(TransitionEventParams{ eTransitionStart,
-                                                    intervalStartTime,
-                                                    startTimeStamp });
+        appendTransitionEvent(eTransitionRun, intervalStartTime, zeroTimeStamp);
+        appendTransitionEvent(eTransitionStart,
+                              intervalStartTime,
+                              startTimeStamp);
       } else if (currentPhase == TransitionPhase::After) {
-        events.AppendElement(TransitionEventParams{ eTransitionRun,
-                                                    intervalStartTime,
-                                                    zeroTimeStamp });
-        events.AppendElement(TransitionEventParams{ eTransitionStart,
-                                                    intervalStartTime,
-                                                    startTimeStamp });
-        events.AppendElement(TransitionEventParams{ eTransitionEnd,
-                                                    intervalEndTime,
-                                                    endTimeStamp });
+        appendTransitionEvent(eTransitionRun, intervalStartTime, zeroTimeStamp);
+        appendTransitionEvent(eTransitionStart,
+                              intervalStartTime,
+                              startTimeStamp);
+        appendTransitionEvent(eTransitionEnd, intervalEndTime, endTimeStamp);
       }
       break;
 
     case TransitionPhase::Pending:
     case TransitionPhase::Before:
       if (currentPhase == TransitionPhase::Active) {
-        events.AppendElement(TransitionEventParams{ eTransitionStart,
-                                                    intervalStartTime,
-                                                    startTimeStamp });
+        appendTransitionEvent(eTransitionStart,
+                              intervalStartTime,
+                              startTimeStamp);
       } else if (currentPhase == TransitionPhase::After) {
-        events.AppendElement(TransitionEventParams{ eTransitionStart,
-                                                    intervalStartTime,
-                                                    startTimeStamp });
-        events.AppendElement(TransitionEventParams{ eTransitionEnd,
-                                                    intervalEndTime,
-                                                    endTimeStamp });
+        appendTransitionEvent(eTransitionStart,
+                              intervalStartTime,
+                              startTimeStamp);
+        appendTransitionEvent(eTransitionEnd, intervalEndTime, endTimeStamp);
       }
       break;
 
     case TransitionPhase::Active:
       if (currentPhase == TransitionPhase::After) {
-        events.AppendElement(TransitionEventParams{ eTransitionEnd,
-                                                    intervalEndTime,
-                                                    endTimeStamp });
+        appendTransitionEvent(eTransitionEnd, intervalEndTime, endTimeStamp);
       } else if (currentPhase == TransitionPhase::Before) {
-        events.AppendElement(TransitionEventParams{ eTransitionEnd,
-                                                    intervalStartTime,
-                                                    startTimeStamp });
+        appendTransitionEvent(eTransitionEnd,
+                              intervalStartTime,
+                              startTimeStamp);
       }
       break;
 
     case TransitionPhase::After:
       if (currentPhase == TransitionPhase::Active) {
-        events.AppendElement(TransitionEventParams{ eTransitionStart,
-                                                    intervalEndTime,
-                                                    startTimeStamp });
+        appendTransitionEvent(eTransitionStart,
+                              intervalEndTime,
+                              startTimeStamp);
       } else if (currentPhase == TransitionPhase::Before) {
-        events.AppendElement(TransitionEventParams{ eTransitionStart,
-                                                    intervalEndTime,
-                                                    startTimeStamp });
-        events.AppendElement(TransitionEventParams{ eTransitionEnd,
-                                                    intervalStartTime,
-                                                    endTimeStamp });
+        appendTransitionEvent(eTransitionStart,
+                              intervalEndTime,
+                              startTimeStamp);
+        appendTransitionEvent(eTransitionEnd,
+                              intervalStartTime,
+                              endTimeStamp);
       }
       break;
   }
   mPreviousTransitionPhase = currentPhase;
 
-  if (events.IsEmpty()) {
-    return;
+  if (!events.IsEmpty()) {
+    presContext->TransitionManager()->QueueEvents(Move(events));
   }
-
-  AutoTArray<TransitionEventInfo, 3> transitionEvents;
-  for (const TransitionEventParams& evt : events) {
-    transitionEvents.AppendElement(
-      TransitionEventInfo(mOwningElement.Target(),
-                          evt.mMessage,
-                          TransitionProperty(),
-                          evt.mElapsedTime,
-                          evt.mTimeStamp,
-                          this));
-  }
-  presContext->TransitionManager()->QueueEvents(Move(transitionEvents));
 }
 
 void
 CSSTransition::Tick()
 {
   Animation::Tick();
   QueueEvents();
 }
