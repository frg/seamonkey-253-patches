# HG changeset patch
# User Ethan Lin <ethlin@mozilla.com>
# Date 1510724928 -28800
# Node ID 92ac9fd6870de189612f1958ceeadf52d503881a
# Parent  13b2b269a4b9cf1da4178af6df28dd3d767c13f7
Bug 1416995 - Enable retained mode for the basic layer manager in WR fallback. r=jrmuizel,kats

MozReview-Commit-ID: InYJSiKGqQE

diff --git a/gfx/layers/wr/WebRenderCommandBuilder.cpp b/gfx/layers/wr/WebRenderCommandBuilder.cpp
--- a/gfx/layers/wr/WebRenderCommandBuilder.cpp
+++ b/gfx/layers/wr/WebRenderCommandBuilder.cpp
@@ -337,36 +337,33 @@ WebRenderCommandBuilder::PushImage(nsDis
   aBuilder.PushImage(r, r, !aItem->BackfaceIsHidden(), wr::ToImageRendering(sampleFilter), key.value());
 
   return true;
 }
 
 static bool
 PaintByLayer(nsDisplayItem* aItem,
              nsDisplayListBuilder* aDisplayListBuilder,
-             RefPtr<BasicLayerManager>& aManager,
+             const RefPtr<BasicLayerManager>& aManager,
              gfxContext* aContext,
+             const gfx::Size& aScale,
              const std::function<void()>& aPaintFunc)
 {
-  if (aManager == nullptr) {
-    aManager = new BasicLayerManager(BasicLayerManager::BLM_INACTIVE);
-  }
-
   UniquePtr<LayerProperties> props;
   if (aManager->GetRoot()) {
     props = Move(LayerProperties::CloneFrom(aManager->GetRoot()));
   }
   FrameLayerBuilder* layerBuilder = new FrameLayerBuilder();
   layerBuilder->Init(aDisplayListBuilder, aManager, nullptr, true);
   layerBuilder->DidBeginRetainedLayerTransaction(aManager);
 
   aManager->BeginTransactionWithTarget(aContext);
   bool isInvalidated = false;
 
-  ContainerLayerParameters param;
+  ContainerLayerParameters param(aScale.width, aScale.height);
   RefPtr<Layer> root = aItem->BuildLayer(aDisplayListBuilder, aManager, param);
 
   if (root) {
     aManager->SetRoot(root);
     layerBuilder->WillEndTransaction();
 
     aPaintFunc();
 
@@ -401,53 +398,54 @@ PaintByLayer(nsDisplayItem* aItem,
 }
 
 static bool
 PaintItemByDrawTarget(nsDisplayItem* aItem,
                       gfx::DrawTarget* aDT,
                       const LayerRect& aImageRect,
                       const LayoutDevicePoint& aOffset,
                       nsDisplayListBuilder* aDisplayListBuilder,
-                      RefPtr<BasicLayerManager>& aManager,
-                      WebRenderLayerManager* aWrManager,
+                      const RefPtr<BasicLayerManager>& aManager,
                       const gfx::Size& aScale,
                       Maybe<gfx::Color>& aHighlight)
 {
   MOZ_ASSERT(aDT);
 
   bool isInvalidated = false;
   aDT->ClearRect(aImageRect.ToUnknownRect());
   RefPtr<gfxContext> context = gfxContext::CreateOrNull(aDT);
   MOZ_ASSERT(context);
 
-  context->SetMatrix(context->CurrentMatrix().PreScale(aScale.width, aScale.height).PreTranslate(-aOffset.x, -aOffset.y));
-
   switch (aItem->GetType()) {
   case DisplayItemType::TYPE_MASK:
+    context->SetMatrix(context->CurrentMatrix().PreScale(aScale.width, aScale.height).PreTranslate(-aOffset.x, -aOffset.y));
     static_cast<nsDisplayMask*>(aItem)->PaintMask(aDisplayListBuilder, context);
     isInvalidated = true;
     break;
   case DisplayItemType::TYPE_SVG_WRAPPER:
     {
-      isInvalidated = PaintByLayer(aItem, aDisplayListBuilder, aManager, context, [&]() {
+      context->SetMatrix(context->CurrentMatrix().PreTranslate(-aOffset.x, -aOffset.y));
+      isInvalidated = PaintByLayer(aItem, aDisplayListBuilder, aManager, context, aScale, [&]() {
         aManager->EndTransaction(FrameLayerBuilder::DrawPaintedLayer, aDisplayListBuilder);
       });
       break;
     }
 
   case DisplayItemType::TYPE_FILTER:
     {
-      isInvalidated = PaintByLayer(aItem, aDisplayListBuilder, aManager, context, [&]() {
+      context->SetMatrix(context->CurrentMatrix().PreTranslate(-aOffset.x, -aOffset.y));
+      isInvalidated = PaintByLayer(aItem, aDisplayListBuilder, aManager, context, aScale, [&]() {
         static_cast<nsDisplayFilter*>(aItem)->PaintAsLayer(aDisplayListBuilder,
                                                            context, aManager);
       });
       break;
     }
 
   default:
+    context->SetMatrix(context->CurrentMatrix().PreScale(aScale.width, aScale.height).PreTranslate(-aOffset.x, -aOffset.y));
     aItem->Paint(aDisplayListBuilder, context);
     isInvalidated = true;
     break;
   }
 
   if (aHighlight) {
     aDT->SetTransform(gfx::Matrix());
     aDT->FillRect(gfx::Rect(0, 0, aImageRect.Width(), aImageRect.Height()), gfx::ColorPattern(aHighlight.value()));
@@ -568,18 +566,21 @@ WebRenderCommandBuilder::GenerateFallbac
           for (auto unscaled : aUnscaledFonts) {
             wr::FontKey key = mManager->WrBridge()->GetFontKeyForUnscaledFont(unscaled);
             aStream.write((const char*)&key, sizeof(key));
           }
         });
       RefPtr<gfx::DrawTarget> dummyDt =
         gfx::Factory::CreateDrawTarget(gfx::BackendType::SKIA, gfx::IntSize(1, 1), format);
       RefPtr<gfx::DrawTarget> dt = gfx::Factory::CreateRecordingDrawTarget(recorder, dummyDt, paintSize.ToUnknownSize());
+      if (!fallbackData->mBasicLayerManager) {
+        fallbackData->mBasicLayerManager = new BasicLayerManager(BasicLayerManager::BLM_INACTIVE);
+      }
       bool isInvalidated = PaintItemByDrawTarget(aItem, dt, paintRect, offset, aDisplayListBuilder,
-                                                 fallbackData->mBasicLayerManager, mManager, scale, highlight);
+                                                 fallbackData->mBasicLayerManager, scale, highlight);
       recorder->FlushItem(IntRect());
       recorder->Finish();
 
       if (isInvalidated) {
         Range<uint8_t> bytes((uint8_t *)recorder->mOutputStream.mData, recorder->mOutputStream.mLength);
         wr::ImageKey key = mManager->WrBridge()->GetNextImageKey();
         wr::ImageDescriptor descriptor(paintSize.ToUnknownSize(), 0, dt->GetFormat(), isOpaque);
         if (!aResources.AddBlobImage(key, descriptor, bytes)) {
@@ -588,34 +589,35 @@ WebRenderCommandBuilder::GenerateFallbac
         fallbackData->SetKey(key);
       } else {
         // If there is no invalidation region and we don't have a image key,
         // it means we don't need to push image for the item.
         if (!fallbackData->GetKey().isSome()) {
           return nullptr;
         }
       }
-
-
     } else {
       fallbackData->CreateImageClientIfNeeded();
       RefPtr<ImageClient> imageClient = fallbackData->GetImageClient();
       RefPtr<ImageContainer> imageContainer = LayerManager::CreateImageContainer();
       bool isInvalidated = false;
 
       {
         UpdateImageHelper helper(imageContainer, imageClient, paintSize.ToUnknownSize(), format);
         {
           RefPtr<gfx::DrawTarget> dt = helper.GetDrawTarget();
           if (!dt) {
             return nullptr;
           }
+          if (!fallbackData->mBasicLayerManager) {
+            fallbackData->mBasicLayerManager = new BasicLayerManager(mManager->GetWidget());
+          }
           isInvalidated = PaintItemByDrawTarget(aItem, dt, paintRect, offset,
                                                aDisplayListBuilder,
-                                               fallbackData->mBasicLayerManager, mManager, scale,
+                                               fallbackData->mBasicLayerManager, scale,
                                                highlight);
         }
 
         if (isInvalidated) {
           // Update image if there it's invalidated.
           if (!helper.UpdateImage()) {
             return nullptr;
           }
diff --git a/gfx/layers/wr/WebRenderLayerManager.h b/gfx/layers/wr/WebRenderLayerManager.h
--- a/gfx/layers/wr/WebRenderLayerManager.h
+++ b/gfx/layers/wr/WebRenderLayerManager.h
@@ -156,16 +156,17 @@ public:
                                                 const ScrollUpdateInfo& aUpdateInfo) override;
 
   WebRenderCommandBuilder& CommandBuilder() { return mWebRenderCommandBuilder; }
   WebRenderUserDataRefTable* GetWebRenderUserDataTable() { return mWebRenderCommandBuilder.GetWebRenderUserDataTable(); }
   WebRenderScrollData& GetScrollData() { return mScrollData; }
 
   void WrUpdated();
   void WindowOverlayChanged() { mWindowOverlayChanged = true; }
+  nsIWidget* GetWidget() { return mWidget; }
 
   dom::TabGroup* GetTabGroup();
 
 private:
   /**
    * Take a snapshot of the parent context, and copy
    * it into mTarget.
    */

