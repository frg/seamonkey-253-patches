# HG changeset patch
# User Cervantes Yu <cyu@mozilla.com>
# Date 1514966771 -28800
# Node ID ea35726ecdf874f09ddff50b0fec140d05ebad7c
# Parent  4255fd07c33b09b067bc438c89f17f904c9ba39f
Bug 1426090 - Use minidump type 'MiniDumpWithIndirectlyReferencedMemory' for nightly on Windows. r=ted

This makes heap objects directly referenced from stack objects accessbile when
debugging minidumps with a debugger, with the cost of doubling the size of
minidumps.

MozReview-Commit-ID: 52ox1lFcaAz

diff --git a/toolkit/crashreporter/nsExceptionHandler.cpp b/toolkit/crashreporter/nsExceptionHandler.cpp
--- a/toolkit/crashreporter/nsExceptionHandler.cpp
+++ b/toolkit/crashreporter/nsExceptionHandler.cpp
@@ -37,16 +37,17 @@
 #include "breakpad-client/windows/crash_generation/client_info.h"
 #include "breakpad-client/windows/crash_generation/crash_generation_server.h"
 #include "breakpad-client/windows/handler/exception_handler.h"
 #include <dbghelp.h>
 #include <string.h>
 #include "nsDirectoryServiceUtils.h"
 
 #include "nsWindowsDllInterceptor.h"
+#include "mozilla/WindowsVersion.h"
 #elif defined(XP_MACOSX)
 #include "breakpad-client/mac/crash_generation/client_info.h"
 #include "breakpad-client/mac/crash_generation/crash_generation_server.h"
 #include "breakpad-client/mac/handler/exception_handler.h"
 #include <string>
 #include <Carbon/Carbon.h>
 #include <CoreFoundation/CoreFoundation.h>
 #include <crt_externs.h>
@@ -1395,16 +1396,25 @@ GetMinidumpType()
   MINIDUMP_TYPE minidump_type = MiniDumpWithFullMemoryInfo;
 
 #ifdef NIGHTLY_BUILD
   // This is Nightly only because this doubles the size of minidumps based
   // on the experimental data.
   minidump_type = static_cast<MINIDUMP_TYPE>(minidump_type |
       MiniDumpWithUnloadedModules |
       MiniDumpWithProcessThreadData);
+
+  // dbghelp.dll on Win7 can't handle overlapping memory regions so we only
+  // enable this feature on Win8 or later.
+  if (IsWin8OrLater()) {
+    minidump_type = static_cast<MINIDUMP_TYPE>(minidump_type |
+      // This allows us to examine heap objects referenced from stack objects
+      // at the cost of further doubling the size of minidumps.
+      MiniDumpWithIndirectlyReferencedMemory);
+  }
 #endif
 
   const char* e = PR_GetEnv("MOZ_CRASHREPORTER_FULLDUMP");
   if (e && *e) {
     minidump_type = MiniDumpWithFullMemory;
   }
 
   return minidump_type;
