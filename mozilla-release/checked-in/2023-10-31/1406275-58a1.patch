# HG changeset patch
# User Cameron McCormack <cam@mcc.id.au>
# Date 1507281967 -28800
#      Fri Oct 06 17:26:07 2017 +0800
# Node ID 2c2fbf414099ca5e1369e7619107ce9ca8ba6680
# Parent  59f8eeda02b4288db333ce1e4c42cf678d61b9c4
Bug 1406275 - Don't assert the status of dirty bits in an unbinding subtree when choosing a new restyle root. r=emilio

MozReview-Commit-ID: 7EVGjjCoF6p

diff --git a/dom/base/Element.cpp b/dom/base/Element.cpp
--- a/dom/base/Element.cpp
+++ b/dom/base/Element.cpp
@@ -4574,22 +4574,41 @@ BitsArePropagated(const Element* aElemen
                   parentNode == aElement->OwnerDoc() ||
                   parentNode == parentNode->OwnerDoc()->GetRootElement());
   }
   return true;
 }
 #endif
 
 static inline void
-AssertNoBitsPropagatedFrom(nsINode* aRoot) {
+AssertNoBitsPropagatedFrom(nsINode* aRoot)
+{
 #ifdef DEBUG
   if (!aRoot || !aRoot->IsElement()) {
     return;
   }
 
+  // If we are in the middle of unbinding a subtree, then the bits on elements
+  // in that subtree (and which we haven't yet unbound) could be dirty.
+  // Just skip asserting the absence of bits on those element that are in
+  // the unbinding subtree.  (The incorrect bits will only cause us to
+  // incorrectly choose a new restyle root if the newly dirty element is
+  // also within the unbinding subtree, so it is OK to leave them there.)
+  for (nsINode* n = aRoot; n; n = n->GetFlattenedTreeParentElementForStyle()) {
+    if (!n->IsInComposedDoc()) {
+      // Find the top-most element that is marked as no longer in the document,
+      // so we can start checking bits from its parent.
+      do {
+        aRoot = n;
+        n = n->GetFlattenedTreeParentElementForStyle();
+      } while (n && !n->IsInComposedDoc());
+      break;
+    }
+  }
+
   auto* element = aRoot->GetFlattenedTreeParentElementForStyle();
   while (element) {
     MOZ_ASSERT(!element->HasAnyOfFlags(Element::kAllServoDescendantBits));
     element = element->GetFlattenedTreeParentElementForStyle();
   }
 #endif
 }
 
