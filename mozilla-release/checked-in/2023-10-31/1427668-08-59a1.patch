# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1513827598 28800
# Node ID e31cefdad58256dfe46d32f6555aadfbcc01df84
# Parent  4955f77933f659e3ae9f3ebc3f4bda803d2f54cc
Bug 1427668 - Unify MaxRenderbufferSize and MaxTextureSize. - r=daoshengmu

MozReview-Commit-ID: AFS0IqmkQEo

diff --git a/gfx/gl/GLContext.cpp b/gfx/gl/GLContext.cpp
--- a/gfx/gl/GLContext.cpp
+++ b/gfx/gl/GLContext.cpp
@@ -941,16 +941,18 @@ GLContext::InitWithPrefixImpl(const char
 #endif
 
     mMaxTextureImageSize = mMaxTextureSize;
 
     if (IsSupported(GLFeature::framebuffer_multisample)) {
         fGetIntegerv(LOCAL_GL_MAX_SAMPLES, (GLint*)&mMaxSamples);
     }
 
+    mMaxTexOrRbSize = std::min(mMaxTextureSize, mMaxRenderbufferSize);
+
     ////////////////////////////////////////////////////////////////////////////
 
     // We're ready for final setup.
     fBindFramebuffer(LOCAL_GL_FRAMEBUFFER, 0);
 
     // TODO: Remove SurfaceCaps::any.
     if (mCaps.any) {
         mCaps.any = false;
diff --git a/gfx/gl/GLContext.h b/gfx/gl/GLContext.h
--- a/gfx/gl/GLContext.h
+++ b/gfx/gl/GLContext.h
@@ -3586,16 +3586,17 @@ private:
                             GLFeature feature);
 
 protected:
     void InitExtensions();
 
     GLint mViewportRect[4];
     GLint mScissorRect[4];
 
+    uint32_t mMaxTexOrRbSize = 0;
     GLint mMaxTextureSize;
     GLint mMaxCubeMapTextureSize;
     GLint mMaxTextureImageSize;
     GLint mMaxRenderbufferSize;
     GLint mMaxViewportDims[2];
     GLsizei mMaxSamples;
     bool mNeedsTextureSizeChecks;
     bool mNeedsFlushBeforeDeleteFB;
@@ -3621,16 +3622,17 @@ protected:
         }
         return true;
     }
 
 public:
     auto MaxSamples() const { return uint32_t(mMaxSamples); }
     auto MaxTextureSize() const { return uint32_t(mMaxTextureSize); }
     auto MaxRenderbufferSize() const { return uint32_t(mMaxRenderbufferSize); }
+    auto MaxTexOrRbSize() const { return mMaxTexOrRbSize; }
 
 #ifdef MOZ_GL_DEBUG
     void CreatedProgram(GLContext* aOrigin, GLuint aName);
     void CreatedShader(GLContext* aOrigin, GLuint aName);
     void CreatedBuffers(GLContext* aOrigin, GLsizei aCount, GLuint* aNames);
     void CreatedQueries(GLContext* aOrigin, GLsizei aCount, GLuint* aNames);
     void CreatedTextures(GLContext* aOrigin, GLsizei aCount, GLuint* aNames);
     void CreatedFramebuffers(GLContext* aOrigin, GLsizei aCount, GLuint* aNames);
diff --git a/gfx/gl/MozFramebuffer.cpp b/gfx/gl/MozFramebuffer.cpp
--- a/gfx/gl/MozFramebuffer.cpp
+++ b/gfx/gl/MozFramebuffer.cpp
@@ -23,40 +23,36 @@ DeleteByTarget(GLContext* const gl, cons
 
 UniquePtr<MozFramebuffer>
 MozFramebuffer::Create(GLContext* const gl, const gfx::IntSize& size,
                        const uint32_t samples, const bool depthStencil)
 {
     if (samples && !gl->IsSupported(GLFeature::framebuffer_multisample))
         return nullptr;
 
+    if (uint32_t(size.width) > gl->MaxTexOrRbSize() ||
+        uint32_t(size.height) > gl->MaxTexOrRbSize() ||
+        samples > gl->MaxSamples())
+    {
+        return nullptr;
+    }
+
     gl->MakeCurrent();
 
     GLContext::LocalErrorScope errorScope(*gl);
 
     GLenum colorTarget;
     GLuint colorName;
     if (samples) {
-        if (uint32_t(size.width) > gl->MaxRenderbufferSize() ||
-            uint32_t(size.height) > gl->MaxRenderbufferSize() ||
-            samples > gl->MaxSamples())
-        {
-            return nullptr;
-        }
         colorTarget = LOCAL_GL_RENDERBUFFER;
         colorName = gl->CreateRenderbuffer();
         const ScopedBindRenderbuffer bindRB(gl, colorName);
         gl->fRenderbufferStorageMultisample(colorTarget, samples, LOCAL_GL_RGBA8,
                                             size.width, size.height);
     } else {
-        if (uint32_t(size.width) > gl->MaxTextureSize() ||
-            uint32_t(size.height) > gl->MaxTextureSize())
-        {
-            return nullptr;
-        }
         colorTarget = LOCAL_GL_TEXTURE_2D;
         colorName = gl->CreateTexture();
         const ScopedBindTexture bindTex(gl, colorName);
         gl->TexParams_SetClampNoMips();
         const ScopedBindPBO bindPBO(gl, LOCAL_GL_PIXEL_UNPACK_BUFFER);
         gl->fBindBuffer(LOCAL_GL_PIXEL_UNPACK_BUFFER, 0);
         gl->fTexImage2D(colorTarget, 0, LOCAL_GL_RGBA,
                         size.width, size.height, 0,
