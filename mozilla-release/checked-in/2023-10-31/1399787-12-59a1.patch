# HG changeset patch
# User cku <cku@mozilla.com>
# Date 1509956931 -28800
# Node ID fba1b5a37709e1fb564830ce83f678bc374f24b0
# Parent  d932b258e5dc957f86e34adcd2007333cf4afa43
Bug 1399787 - Part 12. Delay dispatching FinalizePrint message until the last page was processed. r=jwatt

For the last page, here is the final three messages sent between the content
process, RemotePrintJobChild, and the chrome process, RemotePrintJobParent, for
printing:
1. The content process sends *ProcessPage* to the chrome process via
   SendProcessPrint to request the chrome process print the last page.
2. The content process sends *FinalizePrint* to the chrome process via
   SendFinalizePrint to notify the chrome that there are no more outstanding
   print requests, and that the chrome process can release interal resource
   now.
3. The content process receive PageProcessed message from the chrome process.

This calling sequence is fine for sync style PrintTarget (even though the
FinalizePrint message is sent out a bit ealy). Since a sync PrintTarget
completes its print task right after receiving *ProcessPage* message in #1,
sending FinalizePrint before getting PageProcessed response is harmless.

But this message dispatching sequence does cause a problem for async style
PrintTargetEMF. After getting a message sent in #2, PrintTargetEMF release all
resources before getting a EMF conversion response from the PDFium process. So
the last page can not be printed correctly. This patch reorder the #2 and #3
message, that is to send FinalizePrint after the content process received
PageProcessed message of the last page.

MozReview-Commit-ID: 9ZVSrFnuHBU

diff --git a/layout/printing/nsPagePrintTimer.cpp b/layout/printing/nsPagePrintTimer.cpp
--- a/layout/printing/nsPagePrintTimer.cpp
+++ b/layout/printing/nsPagePrintTimer.cpp
@@ -70,18 +70,21 @@ nsPagePrintTimer::Run()
   // inRange will be true if a page is actually printed
   bool inRange;
   bool donePrinting;
 
   // donePrinting will be true if it completed successfully or
   // if the printing was cancelled
   donePrinting = !mPrintEngine || mPrintEngine->PrintPage(mPrintObj, inRange);
   if (donePrinting) {
-    // now clean up print or print the next webshell
-    if (!mPrintEngine || mPrintEngine->DonePrintingPages(mPrintObj, NS_OK)) {
+
+    if (mWaitingForRemotePrint ||
+        // If we are not waiting for the remote printing, it is the time to
+        // end printing task by calling DonePrintingPages.
+        (!mPrintEngine || mPrintEngine->DonePrintingPages(mPrintObj, NS_OK))) {
       initNewTimer = false;
       mDone = true;
     }
   }
 
   // Note that the Stop() destroys this after the print job finishes
   // (The PrintEngine stops holding a reference when DonePrintingPages
   // returns true.)
@@ -170,16 +173,21 @@ nsPagePrintTimer::WaitForRemotePrint()
 
 void
 nsPagePrintTimer::RemotePrintFinished()
 {
   if (!mWaitingForRemotePrint) {
     return;
   }
 
+  // now clean up print or print the next webshell
+  if (mDone && mPrintEngine) {
+    mDone = mPrintEngine->DonePrintingPages(mPrintObj, NS_OK);
+  }
+
   mWaitingForRemotePrint->SetTarget(
     mDocument->EventTargetFor(mozilla::TaskCategory::Other));
   mozilla::Unused <<
     mWaitingForRemotePrint->InitWithCallback(this, 0, nsITimer::TYPE_ONE_SHOT);
 }
 
 nsresult
 nsPagePrintTimer::Start(nsPrintObject* aPO)
