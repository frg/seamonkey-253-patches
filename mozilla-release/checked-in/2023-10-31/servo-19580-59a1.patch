# HG changeset patch
# User Brian Birtles <birtles@gmail.com>
# Date 1513381195 21600
# Node ID 232f3035aeb041d2b280f6ec05dd80e453f38090
# Parent  52abe5cb8aae18c105712ea8d085696ffe13f556
servo: Merge #19580 - Update references to Web Animations spec (from birtles:update-web-animations-link); r=hiro

This has been reviewed in [Gecko bug 1425548](https://bugzilla.mozilla.org/show_bug.cgi?id=1425548).

Source-Repo: https://github.com/servo/servo
Source-Revision: f7440bf1a68cd0beaf12621c9c460c6f6ddc57e3

diff --git a/servo/components/style/properties/helpers/animated_properties.mako.rs b/servo/components/style/properties/helpers/animated_properties.mako.rs
--- a/servo/components/style/properties/helpers/animated_properties.mako.rs
+++ b/servo/components/style/properties/helpers/animated_properties.mako.rs
@@ -257,17 +257,17 @@ impl AnimatedProperty {
 
     /// Update `style` with the proper computed style corresponding to this
     /// animation at `progress`.
     pub fn update(&self, style: &mut ComputedValues, progress: f64) {
         match *self {
             % for prop in data.longhands:
                 % if prop.animatable:
                     AnimatedProperty::${prop.camel_case}(ref from, ref to) => {
-                        // https://w3c.github.io/web-animations/#discrete-animation-type
+                        // https://drafts.csswg.org/web-animations/#discrete-animation-type
                         % if prop.animation_value_type == "discrete":
                             let value = if progress < 0.5 { from.clone() } else { to.clone() };
                         % else:
                             let value = match from.animate(to, Procedure::Interpolate { progress }) {
                                 Ok(value) => value,
                                 Err(()) => return,
                             };
                         % endif
@@ -2828,17 +2828,17 @@ impl ComputeSquaredDistance for Animated
 /// Example orderings that result from this:
 ///
 ///   margin-left, margin
 ///
 /// and:
 ///
 ///   border-top-color, border-color, border-top, border
 ///
-/// [property-order] https://w3c.github.io/web-animations/#calculating-computed-keyframes
+/// [property-order] https://drafts.csswg.org/web-animations/#calculating-computed-keyframes
 #[cfg(feature = "gecko")]
 pub fn compare_property_priority(a: &PropertyId, b: &PropertyId) -> cmp::Ordering {
     match (a.as_shorthand(), b.as_shorthand()) {
         // Within shorthands, sort by the number of subproperties, then by IDL name.
         (Ok(a), Ok(b)) => {
             let subprop_count_a = a.longhands().len();
             let subprop_count_b = b.longhands().len();
             subprop_count_a.cmp(&subprop_count_b).then_with(
diff --git a/servo/components/style/values/animated/mod.rs b/servo/components/style/values/animated/mod.rs
--- a/servo/components/style/values/animated/mod.rs
+++ b/servo/components/style/values/animated/mod.rs
@@ -43,25 +43,25 @@ pub mod effects;
 /// function has been specified through `#[animate(fallback)]`.
 pub trait Animate: Sized {
     /// Animate a value towards another one, given an animation procedure.
     fn animate(&self, other: &Self, procedure: Procedure) -> Result<Self, ()>;
 }
 
 /// An animation procedure.
 ///
-/// <https://w3c.github.io/web-animations/#procedures-for-animating-properties>
+/// <https://drafts.csswg.org/web-animations/#procedures-for-animating-properties>
 #[allow(missing_docs)]
 #[derive(Clone, Copy, Debug, PartialEq)]
 pub enum Procedure {
-    /// <https://w3c.github.io/web-animations/#animation-interpolation>
+    /// <https://drafts.csswg.org/web-animations/#animation-interpolation>
     Interpolate { progress: f64 },
-    /// <https://w3c.github.io/web-animations/#animation-addition>
+    /// <https://drafts.csswg.org/web-animations/#animation-addition>
     Add,
-    /// <https://w3c.github.io/web-animations/#animation-accumulation>
+    /// <https://drafts.csswg.org/web-animations/#animation-accumulation>
     Accumulate { count: u64 },
 }
 
 /// Conversion between computed values and intermediate values for animations.
 ///
 /// Notably, colors are represented as four floats during animations.
 ///
 /// This trait is derivable with `#[derive(ToAnimatedValue)]`.
