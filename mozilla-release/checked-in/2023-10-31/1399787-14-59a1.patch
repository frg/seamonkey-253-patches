# HG changeset patch
# User cku <cku@mozilla.com>
# Date 1508745438 -28800
# Node ID 8b5b57e746e649b0ac472f53cc5bd6ae23dbb183
# Parent  a9675910f1bd57a2b1159364c78414555cf1f302
Bug 1399787 - Part 14. Prevent RemotePrintJobChild using ipc calls after the channel was destroyed. r=jwatt

If in the future nsDeviceContextSpecWin::BeginDocument was to return
NS_ERROR_FAILURE, then the channel between RemotePrintJobParent and
RemotePrintJobChild will be close at [1]. RemotePrintJobChild keep using ipc
calls after the channel is broken and hits assertions.

PS:
We always hits this assertion by forcing nsDeviceContextSpecWin::BeginDocument
returning NS_ERROR_FAILURE. It's not relative to the change we made in
previous patches.

[1]
https://hg.mozilla.org/mozilla-central/file/b186fddce27f/layout/printing/ipc/RemotePrintJobParent.cpp#l44
MozReview-Commit-ID: 79mZBf301nb

diff --git a/layout/printing/ipc/RemotePrintJobChild.cpp b/layout/printing/ipc/RemotePrintJobChild.cpp
--- a/layout/printing/ipc/RemotePrintJobChild.cpp
+++ b/layout/printing/ipc/RemotePrintJobChild.cpp
@@ -66,17 +66,19 @@ RemotePrintJobChild::SetNextPageFD(const
 }
 
 void
 RemotePrintJobChild::ProcessPage()
 {
   MOZ_ASSERT(mPagePrintTimer);
 
   mPagePrintTimer->WaitForRemotePrint();
-  Unused << SendProcessPage();
+  if (!mDestroyed) {
+    Unused << SendProcessPage();
+  }
 }
 
 mozilla::ipc::IPCResult
 RemotePrintJobChild::RecvPageProcessed(const mozilla::ipc::FileDescriptor& aFd)
 {
   MOZ_ASSERT(mPagePrintTimer);
   SetNextPageFD(aFd);
 
@@ -111,47 +113,56 @@ RemotePrintJobChild::SetPrintEngine(nsPr
 
 // nsIWebProgressListener
 
 NS_IMETHODIMP
 RemotePrintJobChild::OnStateChange(nsIWebProgress* aProgress,
                                    nsIRequest* aRequest, uint32_t aStateFlags,
                                    nsresult aStatus)
 {
-  Unused << SendStateChange(aStateFlags, aStatus);
+  if (!mDestroyed) {
+    Unused << SendStateChange(aStateFlags, aStatus);
+  }
+
   return NS_OK;
 }
 
 NS_IMETHODIMP
 RemotePrintJobChild::OnProgressChange(nsIWebProgress * aProgress,
                                       nsIRequest * aRequest,
                                       int32_t aCurSelfProgress,
                                       int32_t aMaxSelfProgress,
                                       int32_t aCurTotalProgress,
                                       int32_t aMaxTotalProgress)
 {
-  Unused << SendProgressChange(aCurSelfProgress, aMaxSelfProgress,
-                               aCurTotalProgress, aMaxTotalProgress);
+  if (!mDestroyed) {
+    Unused << SendProgressChange(aCurSelfProgress, aMaxSelfProgress,
+                                 aCurTotalProgress, aMaxTotalProgress);
+  }
+
   return NS_OK;
 }
 
 NS_IMETHODIMP
 RemotePrintJobChild::OnLocationChange(nsIWebProgress* aProgress,
                                       nsIRequest* aRequest, nsIURI* aURI,
                                       uint32_t aFlags)
 {
   return NS_OK;
 }
 
 NS_IMETHODIMP
 RemotePrintJobChild::OnStatusChange(nsIWebProgress* aProgress,
                                     nsIRequest* aRequest, nsresult aStatus,
                                     const char16_t* aMessage)
 {
-  Unused << SendStatusChange(aStatus);
+  if (!mDestroyed) {
+    Unused << SendStatusChange(aStatus);
+  }
+
   return NS_OK;
 }
 
 NS_IMETHODIMP
 RemotePrintJobChild::OnSecurityChange(nsIWebProgress* aProgress,
                                       nsIRequest* aRequest, uint32_t aState)
 {
   return NS_OK;
@@ -163,12 +174,14 @@ RemotePrintJobChild::~RemotePrintJobChil
 {
 }
 
 void
 RemotePrintJobChild::ActorDestroy(ActorDestroyReason aWhy)
 {
   mPagePrintTimer = nullptr;
   mPrintEngine = nullptr;
+
+  mDestroyed = true;
 }
 
 } // namespace layout
 } // namespace mozilla
diff --git a/layout/printing/ipc/RemotePrintJobChild.h b/layout/printing/ipc/RemotePrintJobChild.h
--- a/layout/printing/ipc/RemotePrintJobChild.h
+++ b/layout/printing/ipc/RemotePrintJobChild.h
@@ -50,16 +50,17 @@ public:
 
   PRFileDesc* GetNextPageFD();
 
 private:
   ~RemotePrintJobChild() final;
   void SetNextPageFD(const mozilla::ipc::FileDescriptor& aFd);
 
   bool mPrintInitialized = false;
+  bool mDestroyed = false;
   nsresult mInitializationResult = NS_OK;
   RefPtr<nsPagePrintTimer> mPagePrintTimer;
   RefPtr<nsPrintEngine> mPrintEngine;
   PRFileDesc* mNextPageFD = nullptr;
 };
 
 } // namespace layout
 } // namespace mozilla
