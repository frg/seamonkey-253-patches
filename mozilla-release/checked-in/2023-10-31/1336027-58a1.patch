# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1507874394 -7200
#      Fri Oct 13 07:59:54 2017 +0200
# Node ID c4c074bb5d8fa683dcb082913ffd64442ddc3314
# Parent  7ec02f3d44a2f7cadc1b6155ed15e2866759afeb
Bug 1336027 - wasm baseline, move platform functionality into MacroAssembler layer. r=nbp

diff --git a/js/src/jit/MacroAssembler.h b/js/src/jit/MacroAssembler.h
--- a/js/src/jit/MacroAssembler.h
+++ b/js/src/jit/MacroAssembler.h
@@ -739,19 +739,32 @@ class MacroAssembler : public MacroAssem
     // Move instructions
 
     inline void move64(Imm64 imm, Register64 dest) PER_ARCH;
     inline void move64(Register64 src, Register64 dest) PER_ARCH;
 
     inline void moveFloat32ToGPR(FloatRegister src, Register dest) PER_SHARED_ARCH;
     inline void moveGPRToFloat32(Register src, FloatRegister dest) PER_SHARED_ARCH;
 
+    inline void moveDoubleToGPR64(FloatRegister src, Register64 dest) PER_ARCH;
+    inline void moveGPR64ToDouble(Register64 src, FloatRegister dest) PER_ARCH;
+
     inline void move8SignExtend(Register src, Register dest) PER_SHARED_ARCH;
     inline void move16SignExtend(Register src, Register dest) PER_SHARED_ARCH;
 
+    // move64To32 will clear the high bits of `dest` on 64-bit systems.
+    inline void move64To32(Register64 src, Register dest) PER_ARCH;
+
+    inline void move32To64ZeroExtend(Register src, Register64 dest) PER_ARCH;
+
+    // On x86, `dest` must be edx:eax for the sign extend operations.
+    inline void move8To64SignExtend(Register src, Register64 dest) PER_ARCH;
+    inline void move16To64SignExtend(Register src, Register64 dest) PER_ARCH;
+    inline void move32To64SignExtend(Register src, Register64 dest) PER_ARCH;
+
     // Copy a constant, typed-register, or a ValueOperand into a ValueOperand
     // destination.
     inline void moveValue(const ConstantOrRegister& src, const ValueOperand& dest);
     void moveValue(const TypedOrValueRegister& src, const ValueOperand& dest) PER_ARCH;
     void moveValue(const ValueOperand& src, const ValueOperand& dest) PER_ARCH;
     void moveValue(const Value& src, const ValueOperand& dest) PER_ARCH;
 
   public:
diff --git a/js/src/jit/arm/MacroAssembler-arm-inl.h b/js/src/jit/arm/MacroAssembler-arm-inl.h
--- a/js/src/jit/arm/MacroAssembler-arm-inl.h
+++ b/js/src/jit/arm/MacroAssembler-arm-inl.h
@@ -47,16 +47,65 @@ MacroAssembler::move8SignExtend(Register
 }
 
 void
 MacroAssembler::move16SignExtend(Register src, Register dest)
 {
     as_sxth(dest, src, 0);
 }
 
+void
+MacroAssembler::moveDoubleToGPR64(FloatRegister src, Register64 dest)
+{
+    ma_vxfer(src, dest.low, dest.high);
+}
+
+void
+MacroAssembler::moveGPR64ToDouble(Register64 src, FloatRegister dest)
+{
+    ma_vxfer(src.low, src.high, dest);
+}
+
+void
+MacroAssembler::move64To32(Register64 src, Register dest)
+{
+    if (src.low != dest)
+        move32(src.low, dest);
+}
+
+void
+MacroAssembler::move32To64ZeroExtend(Register src, Register64 dest)
+{
+    if (src != dest.low)
+        move32(src, dest.low);
+    move32(Imm32(0), dest.high);
+}
+
+void
+MacroAssembler::move8To64SignExtend(Register src, Register64 dest)
+{
+    as_sxtb(dest.low, src, 0);
+    ma_asr(Imm32(31), dest.low, dest.high);
+}
+
+void
+MacroAssembler::move16To64SignExtend(Register src, Register64 dest)
+{
+    as_sxth(dest.low, src, 0);
+    ma_asr(Imm32(31), dest.low, dest.high);
+}
+
+void
+MacroAssembler::move32To64SignExtend(Register src, Register64 dest)
+{
+    if (src != dest.low)
+        move32(src, dest.low);
+    ma_asr(Imm32(31), dest.low, dest.high);
+}
+
 // ===============================================================
 // Logical instructions
 
 void
 MacroAssembler::not32(Register reg)
 {
     ma_mvn(reg, reg);
 }
diff --git a/js/src/jit/arm64/MacroAssembler-arm64-inl.h b/js/src/jit/arm64/MacroAssembler-arm64-inl.h
--- a/js/src/jit/arm64/MacroAssembler-arm64-inl.h
+++ b/js/src/jit/arm64/MacroAssembler-arm64-inl.h
@@ -45,16 +45,58 @@ MacroAssembler::move8SignExtend(Register
 }
 
 void
 MacroAssembler::move16SignExtend(Register src, Register dest)
 {
     MOZ_CRASH("NYI: move16SignExtend");
 }
 
+void
+MacroAssembler::moveDoubleToGPR64(FloatRegister src, Register64 dest)
+{
+    MOZ_CRASH("NYI: moveDoubleToGPR64");
+}
+
+void
+MacroAssembler::moveGPR64ToDouble(Register64 src, FloatRegister dest)
+{
+    MOZ_CRASH("NYI: moveGPR64ToDouble");
+}
+
+void
+MacroAssembler::move64To32(Register64 src, Register dest)
+{
+    MOZ_CRASH("NYI: move64To32");
+}
+
+void
+MacroAssembler::move32To64ZeroExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move32To64ZeroExtend");
+}
+
+void
+MacroAssembler::move8To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move8To64SignExtend");
+}
+
+void
+MacroAssembler::move16To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move16To64SignExtend");
+}
+
+void
+MacroAssembler::move32To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move32To64SignExtend");
+}
+
 // ===============================================================
 // Logical instructions
 
 void
 MacroAssembler::not32(Register reg)
 {
     Orn(ARMRegister(reg, 32), vixl::wzr, ARMRegister(reg, 32));
 }
diff --git a/js/src/jit/mips32/MacroAssembler-mips32-inl.h b/js/src/jit/mips32/MacroAssembler-mips32-inl.h
--- a/js/src/jit/mips32/MacroAssembler-mips32-inl.h
+++ b/js/src/jit/mips32/MacroAssembler-mips32-inl.h
@@ -25,16 +25,58 @@ MacroAssembler::move64(Register64 src, R
 
 void
 MacroAssembler::move64(Imm64 imm, Register64 dest)
 {
     move32(Imm32(imm.value & 0xFFFFFFFFL), dest.low);
     move32(Imm32((imm.value >> 32) & 0xFFFFFFFFL), dest.high);
 }
 
+void
+MacroAssembler::moveDoubleToGPR64(FloatRegister src, Register64 dest)
+{
+    MOZ_CRASH("NYI: moveDoubleToGPR64");
+}
+
+void
+MacroAssembler::moveGPR64ToDouble(Register64 src, FloatRegister dest)
+{
+    MOZ_CRASH("NYI: moveGPR64ToDouble");
+}
+
+void
+MacroAssembler::move64To32(Register64 src, Register dest)
+{
+    MOZ_CRASH("NYI: move64To32");
+}
+
+void
+MacroAssembler::move32To64ZeroExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move32To64ZeroExtend");
+}
+
+void
+MacroAssembler::move8To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move8To64SignExtend");
+}
+
+void
+MacroAssembler::move16To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move16To64SignExtend");
+}
+
+void
+MacroAssembler::move32To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move32To64SignExtend");
+}
+
 // ===============================================================
 // Logical instructions
 
 void
 MacroAssembler::andPtr(Register src, Register dest)
 {
     ma_and(dest, src);
 }
diff --git a/js/src/jit/mips64/MacroAssembler-mips64-inl.h b/js/src/jit/mips64/MacroAssembler-mips64-inl.h
--- a/js/src/jit/mips64/MacroAssembler-mips64-inl.h
+++ b/js/src/jit/mips64/MacroAssembler-mips64-inl.h
@@ -23,16 +23,58 @@ MacroAssembler::move64(Register64 src, R
 }
 
 void
 MacroAssembler::move64(Imm64 imm, Register64 dest)
 {
     movePtr(ImmWord(imm.value), dest.reg);
 }
 
+void
+MacroAssembler::moveDoubleToGPR64(FloatRegister src, Register64 dest)
+{
+    MOZ_CRASH("NYI: moveDoubleToGPR64");
+}
+
+void
+MacroAssembler::moveGPR64ToDouble(Register64 src, FloatRegister dest)
+{
+    MOZ_CRASH("NYI: moveGPR64ToDouble");
+}
+
+void
+MacroAssembler::move64To32(Register64 src, Register dest)
+{
+    MOZ_CRASH("NYI: move64To32");
+}
+
+void
+MacroAssembler::move32To64ZeroExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move32To64ZeroExtend");
+}
+
+void
+MacroAssembler::move8To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move8To64SignExtend");
+}
+
+void
+MacroAssembler::move16To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move16To64SignExtend");
+}
+
+void
+MacroAssembler::move32To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_CRASH("NYI: move32To64SignExtend");
+}
+
 // ===============================================================
 // Logical instructions
 
 void
 MacroAssembler::andPtr(Register src, Register dest)
 {
     ma_and(dest, src);
 }
diff --git a/js/src/jit/x64/MacroAssembler-x64-inl.h b/js/src/jit/x64/MacroAssembler-x64-inl.h
--- a/js/src/jit/x64/MacroAssembler-x64-inl.h
+++ b/js/src/jit/x64/MacroAssembler-x64-inl.h
@@ -25,16 +25,58 @@ MacroAssembler::move64(Imm64 imm, Regist
 
 void
 MacroAssembler::move64(Register64 src, Register64 dest)
 {
     movq(src.reg, dest.reg);
 }
 
 void
+MacroAssembler::moveDoubleToGPR64(FloatRegister src, Register64 dest)
+{
+    vmovq(src, dest.reg);
+}
+
+void
+MacroAssembler::moveGPR64ToDouble(Register64 src, FloatRegister dest)
+{
+    vmovq(src.reg, dest);
+}
+
+void
+MacroAssembler::move64To32(Register64 src, Register dest)
+{
+    movl(src.reg, dest);
+}
+
+void
+MacroAssembler::move32To64ZeroExtend(Register src, Register64 dest)
+{
+    movl(src, dest.reg);
+}
+
+void
+MacroAssembler::move8To64SignExtend(Register src, Register64 dest)
+{
+    movsbq(Operand(src), dest.reg);
+}
+
+void
+MacroAssembler::move16To64SignExtend(Register src, Register64 dest)
+{
+    movswq(Operand(src), dest.reg);
+}
+
+void
+MacroAssembler::move32To64SignExtend(Register src, Register64 dest)
+{
+    movslq(src, dest.reg);
+}
+
+void
 MacroAssembler::andPtr(Register src, Register dest)
 {
     andq(src, dest);
 }
 
 void
 MacroAssembler::andPtr(Imm32 imm, Register dest)
 {
diff --git a/js/src/jit/x86/MacroAssembler-x86-inl.h b/js/src/jit/x86/MacroAssembler-x86-inl.h
--- a/js/src/jit/x86/MacroAssembler-x86-inl.h
+++ b/js/src/jit/x86/MacroAssembler-x86-inl.h
@@ -25,16 +25,90 @@ MacroAssembler::move64(Imm64 imm, Regist
 
 void
 MacroAssembler::move64(Register64 src, Register64 dest)
 {
     movl(src.low, dest.low);
     movl(src.high, dest.high);
 }
 
+void
+MacroAssembler::moveDoubleToGPR64(FloatRegister src, Register64 dest)
+{
+    ScratchDoubleScope scratch(*this);
+
+    if (Assembler::HasSSE41()) {
+        vmovd(src, dest.low);
+        vpextrd(1, src, dest.high);
+    } else {
+        vmovd(src, dest.low);
+        moveDouble(src, scratch);
+        vpsrldq(Imm32(4), scratch, scratch);
+        vmovd(scratch, dest.high);
+    }
+}
+
+void
+MacroAssembler::moveGPR64ToDouble(Register64 src, FloatRegister dest)
+{
+    ScratchDoubleScope scratch(*this);
+
+    if (Assembler::HasSSE41()) {
+        vmovd(src.low, dest);
+        vpinsrd(1, src.high, dest, dest);
+    } else {
+        vmovd(src.low, dest);
+        vmovd(src.high, ScratchDoubleReg);
+        vunpcklps(ScratchDoubleReg, dest, dest);
+    }
+}
+
+void
+MacroAssembler::move64To32(Register64 src, Register dest)
+{
+    if (src.low != dest)
+        movl(src.low, dest);
+}
+
+void
+MacroAssembler::move32To64ZeroExtend(Register src, Register64 dest)
+{
+    if (src != dest.low)
+        movl(src, dest.low);
+    movl(Imm32(0), dest.high);
+}
+
+void
+MacroAssembler::move8To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_ASSERT(dest.low == eax);
+    MOZ_ASSERT(dest.high == edx);
+    move8SignExtend(src, eax);
+    masm.cdq();
+}
+
+void
+MacroAssembler::move16To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_ASSERT(dest.low == eax);
+    MOZ_ASSERT(dest.high == edx);
+    move16SignExtend(src, eax);
+    masm.cdq();
+}
+
+void
+MacroAssembler::move32To64SignExtend(Register src, Register64 dest)
+{
+    MOZ_ASSERT(dest.low == eax);
+    MOZ_ASSERT(dest.high == edx);
+    if (src != eax)
+        movl(src, eax);
+    masm.cdq();
+}
+
 // ===============================================================
 // Logical functions
 
 void
 MacroAssembler::andPtr(Register src, Register dest)
 {
     andl(src, dest);
 }
diff --git a/js/src/wasm/WasmBaselineCompile.cpp b/js/src/wasm/WasmBaselineCompile.cpp
--- a/js/src/wasm/WasmBaselineCompile.cpp
+++ b/js/src/wasm/WasmBaselineCompile.cpp
@@ -2934,58 +2934,16 @@ class BaseCompiler
         return !AssemblerX86Shared::HasPOPCNT();
 #elif defined(JS_CODEGEN_ARM)
         return true;
 #else
         MOZ_CRASH("BaseCompiler platform hook: popcnt64NeedsTemp");
 #endif
     }
 
-    void reinterpretI64AsF64(RegI64 src, RegF64 dest) {
-#if defined(JS_CODEGEN_X64)
-        masm.vmovq(src.reg, dest);
-#elif defined(JS_CODEGEN_X86)
-        masm.Push(src.high);
-        masm.Push(src.low);
-        masm.vmovq(Operand(esp, 0), dest);
-        masm.freeStack(sizeof(uint64_t));
-#elif defined(JS_CODEGEN_ARM)
-        masm.ma_vxfer(src.low, src.high, dest);
-#else
-        MOZ_CRASH("BaseCompiler platform hook: reinterpretI64AsF64");
-#endif
-    }
-
-    void reinterpretF64AsI64(RegF64 src, RegI64 dest) {
-#if defined(JS_CODEGEN_X64)
-        masm.vmovq(src, dest.reg);
-#elif defined(JS_CODEGEN_X86)
-        masm.reserveStack(sizeof(uint64_t));
-        masm.vmovq(src, Operand(esp, 0));
-        masm.Pop(dest.low);
-        masm.Pop(dest.high);
-#elif defined(JS_CODEGEN_ARM)
-        masm.ma_vxfer(src, dest.low, dest.high);
-#else
-        MOZ_CRASH("BaseCompiler platform hook: reinterpretF64AsI64");
-#endif
-    }
-
-    void wrapI64ToI32(RegI64 src, RegI32 dest) {
-#if defined(JS_CODEGEN_X64)
-        // movl clears the high bits if the two registers are the same.
-        masm.movl(src.reg, dest);
-#elif defined(JS_NUNBOX32)
-        if (src.low != dest)
-            masm.move32(src.low, dest);
-#else
-        MOZ_CRASH("BaseCompiler platform hook: wrapI64ToI32");
-#endif
-    }
-
     RegI64 popI32ForSignExtendI64() {
 #if defined(JS_CODEGEN_X86)
         need2xI32(specific_edx, specific_eax);
         RegI32 r0 = popI32ToSpecific(specific_eax);
         RegI64 x0 = RegI64(Register64(specific_edx, specific_eax));
         (void)r0;               // x0 is the widening of r0
 #else
         RegI32 r0 = popI32();
@@ -2999,66 +2957,16 @@ class BaseCompiler
         need2xI32(specific_edx, specific_eax);
         // Low on top, high underneath
         return popI64ToSpecific(RegI64(Register64(specific_edx, specific_eax)));
 #else
         return popI64();
 #endif
     }
 
-    void signExtendI64_8(RegI64 r) {
-#if defined(JS_CODEGEN_X64)
-        masm.movsbq(Operand(r.reg), r.reg);
-#elif defined(JS_CODEGEN_X86) || defined(JS_CODEGEN_ARM)
-        masm.move8SignExtend(r.low, r.low);
-        signExtendI32ToI64(RegI32(r.low), r);
-#else
-        MOZ_CRASH("Basecompiler platform hook: signExtendI64_8");
-#endif
-    }
-
-    void signExtendI64_16(RegI64 r) {
-#if defined(JS_CODEGEN_X64)
-        masm.movswq(Operand(r.reg), r.reg);
-#elif defined(JS_CODEGEN_X86) || defined(JS_CODEGEN_ARM)
-        masm.move16SignExtend(r.low, r.low);
-        signExtendI32ToI64(RegI32(r.low), r);
-#else
-        MOZ_CRASH("Basecompiler platform hook: signExtendI64_16");
-#endif
-    }
-
-    void signExtendI32ToI64(RegI32 src, RegI64 dest) {
-#if defined(JS_CODEGEN_X64)
-        masm.movslq(src, dest.reg);
-#elif defined(JS_CODEGEN_X86)
-        MOZ_ASSERT(dest.low == src);
-        MOZ_ASSERT(dest.low == eax);
-        MOZ_ASSERT(dest.high == edx);
-        masm.cdq();
-#elif defined(JS_CODEGEN_ARM)
-        masm.ma_mov(src, dest.low);
-        masm.ma_asr(Imm32(31), src, dest.high);
-#else
-        MOZ_CRASH("BaseCompiler platform hook: signExtendI32ToI64");
-#endif
-    }
-
-    void extendU32ToI64(RegI32 src, RegI64 dest) {
-#if defined(JS_CODEGEN_X64)
-        masm.movl(src, dest.reg);
-#elif defined(JS_NUNBOX32)
-        if (src != dest.low)
-            masm.move32(src, dest.low);
-        masm.move32(Imm32(0), dest.high);
-#else
-        MOZ_CRASH("BaseCompiler platform hook: extendU32ToI64");
-#endif
-    }
-
     class OutOfLineTruncateF32OrF64ToI32 : public OutOfLineCode
     {
         AnyReg src;
         RegI32 dest;
         bool isUnsigned;
         BytecodeOffset off;
 
       public:
@@ -4480,22 +4388,22 @@ BaseCompiler::emitCopysignF32()
 
 void
 BaseCompiler::emitCopysignF64()
 {
     RegF64 r0, r1;
     pop2xF64(&r0, &r1);
     RegI64 x0 = needI64();
     RegI64 x1 = needI64();
-    reinterpretF64AsI64(r0, x0);
-    reinterpretF64AsI64(r1, x1);
+    masm.moveDoubleToGPR64(r0, x0);
+    masm.moveDoubleToGPR64(r1, x1);
     masm.and64(Imm64(INT64_MAX), x0);
     masm.and64(Imm64(INT64_MIN), x1);
     masm.or64(x1, x0);
-    reinterpretI64AsF64(x0, r0);
+    masm.moveGPR64ToDouble(x0, r0);
     freeI64(x0);
     freeI64(x1);
     freeF64(r1);
     pushF64(r0);
 }
 
 void
 BaseCompiler::emitOrI32()
@@ -4984,17 +4892,17 @@ BaseCompiler::emitTruncateF64ToI64()
 }
 #endif // FLOAT_TO_I64_CALLOUT
 
 void
 BaseCompiler::emitWrapI64ToI32()
 {
     RegI64 r0 = popI64();
     RegI32 i0 = fromI64(r0);
-    wrapI64ToI32(r0, i0);
+    masm.move64To32(r0, i0);
     freeI64Except(r0, i0);
     pushI32(i0);
 }
 
 void
 BaseCompiler::emitExtendI32_8()
 {
     RegI32 r = popI32();
@@ -5009,56 +4917,51 @@ BaseCompiler::emitExtendI32_16()
     masm.move16SignExtend(r, r);
     pushI32(r);
 }
 
 void
 BaseCompiler::emitExtendI64_8()
 {
     RegI64 r = popI64ForSignExtendI64();
-    signExtendI64_8(r);
+    masm.move8To64SignExtend(lowPart(r), r);
     pushI64(r);
 }
 
 void
 BaseCompiler::emitExtendI64_16()
 {
     RegI64 r = popI64ForSignExtendI64();
-    signExtendI64_16(r);
+    masm.move16To64SignExtend(lowPart(r), r);
     pushI64(r);
 }
 
 void
 BaseCompiler::emitExtendI64_32()
 {
     RegI64 x0 = popI64ForSignExtendI64();
-    RegI32 r0 = RegI32(lowPart(x0));
-    signExtendI32ToI64(r0, x0);
+    masm.move32To64SignExtend(lowPart(x0), x0);
     pushI64(x0);
-    // Note: no need to free r0, since it is part of x0
 }
 
 void
 BaseCompiler::emitExtendI32ToI64()
 {
     RegI64 x0 = popI32ForSignExtendI64();
-    RegI32 r0 = RegI32(lowPart(x0));
-    signExtendI32ToI64(r0, x0);
+    masm.move32To64SignExtend(lowPart(x0), x0);
     pushI64(x0);
-    // Note: no need to free r0, since it is part of x0
 }
 
 void
 BaseCompiler::emitExtendU32ToI64()
 {
     RegI32 r0 = popI32();
     RegI64 x0 = widenI32(r0);
-    extendU32ToI64(r0, x0);
+    masm.move32To64ZeroExtend(r0, x0);
     pushI64(x0);
-    // Note: no need to free r0, since it is part of x0
 }
 
 void
 BaseCompiler::emitReinterpretF32AsI32()
 {
     RegF32 r0 = popF32();
     RegI32 i0 = needI32();
     masm.moveFloat32ToGPR(r0, i0);
@@ -5066,17 +4969,17 @@ BaseCompiler::emitReinterpretF32AsI32()
     pushI32(i0);
 }
 
 void
 BaseCompiler::emitReinterpretF64AsI64()
 {
     RegF64 r0 = popF64();
     RegI64 x0 = needI64();
-    reinterpretF64AsI64(r0, x0);
+    masm.moveDoubleToGPR64(r0, x0);
     freeF64(r0);
     pushI64(x0);
 }
 
 void
 BaseCompiler::emitConvertF64ToF32()
 {
     RegF64 r0 = popF64();
@@ -5200,17 +5103,17 @@ BaseCompiler::emitReinterpretI32AsF32()
     pushF32(f0);
 }
 
 void
 BaseCompiler::emitReinterpretI64AsF64()
 {
     RegI64 r0 = popI64();
     RegF64 d0 = needF64();
-    reinterpretI64AsF64(r0, d0);
+    masm.moveGPR64ToDouble(r0, d0);
     freeI64(r0);
     pushF64(d0);
 }
 
 template<typename Cond>
 bool
 BaseCompiler::sniffConditionalControlCmp(Cond compareOp, ValType operandType)
 {
