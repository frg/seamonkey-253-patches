# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1515164997 -3600
# Node ID 2e7604aa6ad8c3020c73a635630084311da4e8ae
# Parent  2861ca89c6fbdeeef2adf98a513d43221433dca2
Bug 1427729: Have EnsureBareExitFrame and JSJitFrameIter take only JitActivation parameters; r=jandem

MozReview-Commit-ID: CZKK1D8bThW

diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -2547,17 +2547,17 @@ testingFunc_inIon(JSContext* cx, unsigne
         // was taken, in which case we actually have jit frames on the stack.
         args.rval().setBoolean(false);
         return true;
     }
 
     ScriptFrameIter iter(cx);
     if (!iter.done() && iter.isIon()) {
         // Reset the counter of the IonScript's script.
-        jit::JSJitFrameIter jitIter(cx);
+        jit::JSJitFrameIter jitIter(cx->activation()->asJit());
         ++jitIter;
         jitIter.script()->resetWarmUpResetCounter();
     } else {
         // Check if we missed multiple attempts at compiling the innermost script.
         JSScript* script = cx->currentScript();
         if (script && script->getWarmUpResetCount() >= 20) {
             JSString* error = JS_NewStringCopyZ(cx, "Compilation is being repeatedly prevented. Giving up.");
             if (!error)
diff --git a/js/src/jit/BaselineBailouts.cpp b/js/src/jit/BaselineBailouts.cpp
--- a/js/src/jit/BaselineBailouts.cpp
+++ b/js/src/jit/BaselineBailouts.cpp
@@ -1895,17 +1895,17 @@ jit::FinishBailoutToBaseline(BaselineBai
     }
 
     // Create arguments objects for bailed out frames, to maintain the invariant
     // that script->needsArgsObj() implies frame->hasArgsObj().
     RootedScript innerScript(cx, nullptr);
     RootedScript outerScript(cx, nullptr);
 
     MOZ_ASSERT(cx->currentlyRunningInJit());
-    JSJitFrameIter iter(cx);
+    JSJitFrameIter iter(cx->activation()->asJit());
     uint8_t* outerFp = nullptr;
 
     // Iter currently points at the exit frame.  Get the previous frame
     // (which must be a baseline frame), and set it as the last profiling
     // frame.
     if (cx->runtime()->jitRuntime()->isProfilerInstrumentationEnabled(cx->runtime()))
         cx->jitActivation->setLastProfilingFrame(iter.prevFp());
 
@@ -1957,17 +1957,17 @@ jit::FinishBailoutToBaseline(BaselineBai
     MOZ_ASSERT(outerFp);
 
     // If we rematerialized Ion frames due to debug mode toggling, copy their
     // values into the baseline frame. We need to do this even when debug mode
     // is off, as we should respect the mutations made while debug mode was
     // on.
     JitActivation* act = cx->activation()->asJit();
     if (act->hasRematerializedFrame(outerFp)) {
-        JSJitFrameIter iter(cx);
+        JSJitFrameIter iter(cx->activation()->asJit());
         size_t inlineDepth = numFrames;
         bool ok = true;
         while (inlineDepth > 0) {
             if (iter.isBaselineJS()) {
                 // We must attempt to copy all rematerialized frames over,
                 // even if earlier ones failed, to invoke the proper frame
                 // cleanup in the Debugger.
                 ok = CopyFromRematerializedFrame(cx, act, outerFp, --inlineDepth,
diff --git a/js/src/jit/BaselineFrame.cpp b/js/src/jit/BaselineFrame.cpp
--- a/js/src/jit/BaselineFrame.cpp
+++ b/js/src/jit/BaselineFrame.cpp
@@ -137,17 +137,17 @@ BaselineFrame::initForOsr(InterpreterFra
 
         // For debuggee frames, update any Debugger.Frame objects for the
         // InterpreterFrame to point to the BaselineFrame.
 
         // The caller pushed a fake return address. ScriptFrameIter, used by the
         // debugger, wants a valid return address, but it's okay to just pick one.
         // In debug mode there's always at least 1 ICEntry (since there are always
         // debug prologue/epilogue calls).
-        JSJitFrameIter frame(cx);
+        JSJitFrameIter frame(cx->activation()->asJit());
         MOZ_ASSERT(frame.returnAddress() == nullptr);
         BaselineScript* baseline = fp->script()->baselineScript();
         frame.current()->setReturnAddress(baseline->returnAddressForIC(baseline->icEntry(0)));
 
         if (!Debugger::handleBaselineOsr(cx, fp, this))
             return false;
 
         setIsDebuggee();
diff --git a/js/src/jit/IonCacheIRCompiler.cpp b/js/src/jit/IonCacheIRCompiler.cpp
--- a/js/src/jit/IonCacheIRCompiler.cpp
+++ b/js/src/jit/IonCacheIRCompiler.cpp
@@ -323,17 +323,17 @@ CacheRegisterAllocator::restoreIonLiveRe
 
     availableRegs_.set() = GeneralRegisterSet();
     availableRegsAfterSpill_.set() = GeneralRegisterSet::All();
 }
 
 static void*
 GetReturnAddressToIonCode(JSContext* cx)
 {
-    JSJitFrameIter frame(cx);
+    JSJitFrameIter frame(cx->activation()->asJit());
     MOZ_ASSERT(frame.type() == JitFrame_Exit,
                "An exit frame is expected as update functions are called with a VMFunction.");
 
     void* returnAddr = frame.returnAddress();
 #ifdef DEBUG
     ++frame;
     MOZ_ASSERT(frame.isIonJS());
 #endif
diff --git a/js/src/jit/JSJitFrameIter.cpp b/js/src/jit/JSJitFrameIter.cpp
--- a/js/src/jit/JSJitFrameIter.cpp
+++ b/js/src/jit/JSJitFrameIter.cpp
@@ -26,21 +26,16 @@ JSJitFrameIter::JSJitFrameIter(const Jit
         current_ = activation_->bailoutData()->fp();
         frameSize_ = activation_->bailoutData()->topFrameSize();
         type_ = JitFrame_Bailout;
     } else {
         MOZ_ASSERT(!TlsContext.get()->inUnsafeCallWithABI);
     }
 }
 
-JSJitFrameIter::JSJitFrameIter(JSContext* cx)
-  : JSJitFrameIter(cx->activation()->asJit())
-{
-}
-
 bool
 JSJitFrameIter::checkInvalidation() const
 {
     IonScript* dummy;
     return checkInvalidation(&dummy);
 }
 
 bool
diff --git a/js/src/jit/JSJitFrameIter.h b/js/src/jit/JSJitFrameIter.h
--- a/js/src/jit/JSJitFrameIter.h
+++ b/js/src/jit/JSJitFrameIter.h
@@ -107,17 +107,16 @@ class JSJitFrameIter
     mutable const SafepointIndex* cachedSafepointIndex_;
     const JitActivation* activation_;
 
     void dumpBaseline() const;
 
   public:
     // See comment above the class.
     explicit JSJitFrameIter(const JitActivation* activation);
-    explicit JSJitFrameIter(JSContext* cx);
 
     // Used only by DebugModeOSRVolatileJitFrameIter.
     void exchangeReturnAddressIfMatch(uint8_t* oldAddr, uint8_t* newAddr) {
         if (returnAddressToFp_ == oldAddr)
             returnAddressToFp_ = newAddr;
     }
 
     // Current frame information.
diff --git a/js/src/jit/JitFrames-inl.h b/js/src/jit/JitFrames-inl.h
--- a/js/src/jit/JitFrames-inl.h
+++ b/js/src/jit/JitFrames-inl.h
@@ -24,17 +24,17 @@ SafepointIndex::resolve()
 #ifdef DEBUG
     resolved = true;
 #endif
 }
 
 inline BaselineFrame*
 GetTopBaselineFrame(JSContext* cx)
 {
-    JSJitFrameIter frame(cx);
+    JSJitFrameIter frame(cx->activation()->asJit());
     MOZ_ASSERT(frame.type() == JitFrame_Exit);
     ++frame;
     if (frame.isBaselineStub())
         ++frame;
     MOZ_ASSERT(frame.isBaselineJS());
     return frame.baselineFrame();
 }
 
diff --git a/js/src/jit/JitFrames.cpp b/js/src/jit/JitFrames.cpp
--- a/js/src/jit/JitFrames.cpp
+++ b/js/src/jit/JitFrames.cpp
@@ -771,56 +771,56 @@ HandleException(ResumeFromException* rfe
         ++iter;
 
         if (current) {
             // Unwind the frame by updating packedExitFP. This is necessary so
             // that (1) debugger exception unwind and leave frame hooks don't
             // see this frame when they use ScriptFrameIter, and (2)
             // ScriptFrameIter does not crash when accessing an IonScript
             // that's destroyed by the ionScript->decref call.
-            EnsureBareExitFrame(cx, current);
+            EnsureBareExitFrame(cx->activation()->asJit(), current);
         }
 
         if (overrecursed) {
             // We hit an overrecursion error during bailout. Report it now.
             ReportOverRecursed(cx);
         }
     }
 
     // Wasm sets its own value of SP in HandleExceptionWasm.
     if (iter.isJSJit())
         rfe->stackPointer = iter.asJSJit().fp();
 }
 
 // Turns a JitFrameLayout into an ExitFrameLayout. Note that it has to be a
 // bare exit frame so it's ignored by TraceJitExitFrame.
 void
-EnsureBareExitFrame(JSContext* cx, JitFrameLayout* frame)
+EnsureBareExitFrame(JitActivation* act, JitFrameLayout* frame)
 {
     ExitFrameLayout* exitFrame = reinterpret_cast<ExitFrameLayout*>(frame);
 
-    if (cx->activation()->asJit()->jsExitFP() == (uint8_t*)frame) {
+    if (act->jsExitFP() == (uint8_t*)frame) {
         // If we already called this function for the current frame, do
         // nothing.
         MOZ_ASSERT(exitFrame->isBareExit());
         return;
     }
 
 #ifdef DEBUG
-    JSJitFrameIter iter(cx);
+    JSJitFrameIter iter(act);
     while (!iter.isScripted())
         ++iter;
     MOZ_ASSERT(iter.current() == frame, "|frame| must be the top JS frame");
 
-    MOZ_ASSERT(!!cx->activation()->asJit()->jsExitFP());
-    MOZ_ASSERT((uint8_t*)exitFrame->footer() >= cx->activation()->asJit()->jsExitFP(),
+    MOZ_ASSERT(!!act->jsExitFP());
+    MOZ_ASSERT((uint8_t*)exitFrame->footer() >= act->jsExitFP(),
                "Must have space for ExitFooterFrame before jsExitFP");
 #endif
 
-    cx->activation()->asJit()->setJSExitFP((uint8_t*)frame);
+    act->setJSExitFP((uint8_t*)frame);
     exitFrame->footer()->setBareExitFrame();
     MOZ_ASSERT(exitFrame->isBareExit());
 }
 
 JSScript* MaybeForwardedScriptFromCalleeToken(CalleeToken token) {
   switch (GetCalleeTokenTag(token)) {
     case CalleeToken_Script:
       return MaybeForwarded(CalleeTokenToScript(token));
diff --git a/js/src/jit/JitFrames.h b/js/src/jit/JitFrames.h
--- a/js/src/jit/JitFrames.h
+++ b/js/src/jit/JitFrames.h
@@ -281,17 +281,17 @@ struct ResumeFromException
     // Value to push when resuming into a |finally| block.
     Value exception;
 
     BaselineBailoutInfo* bailoutInfo;
 };
 
 void HandleException(ResumeFromException* rfe);
 
-void EnsureBareExitFrame(JSContext* cx, JitFrameLayout* frame);
+void EnsureBareExitFrame(JitActivation* act, JitFrameLayout* frame);
 
 void TraceJitActivations(JSContext* cx, const CooperatingContext& target, JSTracer* trc);
 
 void UpdateJitActivationsForMinorGC(JSRuntime* rt, JSTracer* trc);
 
 static inline uint32_t
 EncodeFrameHeaderSize(size_t headerSize)
 {
@@ -309,17 +309,17 @@ MakeFrameDescriptor(uint32_t frameSize, 
     headerSize = EncodeFrameHeaderSize(headerSize);
     return 0 | (frameSize << FRAMESIZE_SHIFT) | (headerSize << FRAME_HEADER_SIZE_SHIFT) | type;
 }
 
 // Returns the JSScript associated with the topmost JIT frame.
 inline JSScript*
 GetTopJitJSScript(JSContext* cx)
 {
-    JSJitFrameIter frame(cx);
+    JSJitFrameIter frame(cx->activation()->asJit());
     MOZ_ASSERT(frame.type() == JitFrame_Exit);
     ++frame;
 
     if (frame.isBaselineStub()) {
         ++frame;
         MOZ_ASSERT(frame.isBaselineJS());
     }
 
diff --git a/js/src/jit/VMFunctions.cpp b/js/src/jit/VMFunctions.cpp
--- a/js/src/jit/VMFunctions.cpp
+++ b/js/src/jit/VMFunctions.cpp
@@ -386,17 +386,17 @@ ArrayPushDense(JSContext* cx, HandleArra
         (*length)++;
         return result == DenseElementResult::Success;
     }
 
     // AutoDetectInvalidation uses GetTopJitJSScript(cx)->ionScript(), but it's
     // possible the setOrExtendDenseElements call already invalidated the
     // IonScript. JSJitFrameIter::ionScript works when the script is invalidated
     // so we use that instead.
-    JSJitFrameIter frame(cx);
+    JSJitFrameIter frame(cx->activation()->asJit());
     MOZ_ASSERT(frame.type() == JitFrame_Exit);
     ++frame;
     IonScript* ionScript = frame.ionScript();
 
     JS::AutoValueArray<3> argv(cx);
     AutoDetectInvalidation adi(cx, argv[0], ionScript);
     argv[0].setUndefined();
     argv[1].setObject(*arr);
@@ -865,17 +865,17 @@ DebugEpilogue(JSContext* cx, BaselineFra
     UnwindAllEnvironmentsInFrame(cx, ei);
     JSScript* script = frame->script();
     frame->setOverridePc(script->lastPC());
 
     if (!ok) {
         // Pop this frame by updating packedExitFP, so that the exception
         // handling code will start at the previous frame.
         JitFrameLayout* prefix = frame->framePrefix();
-        EnsureBareExitFrame(cx, prefix);
+        EnsureBareExitFrame(cx->activation()->asJit(), prefix);
         return false;
     }
 
     // Clear the override pc. This is not necessary for correctness: the frame
     // will return immediately, but this simplifies the check we emit in debug
     // builds after each callVM, to ensure this flag is not set.
     frame->clearOverridePc();
     return true;
