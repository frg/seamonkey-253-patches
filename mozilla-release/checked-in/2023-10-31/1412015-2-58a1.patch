# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1509373801 14400
#      Mon Oct 30 10:30:01 2017 -0400
# Node ID 07285260f7356f41f212c601feb29609d6b0af6d
# Parent  e2560aa25a0026a0d8645e0093c1a9cfeff8e877
Bug 1412015 P2 Create a shared method in HttpBaseChannel to check the redirection limit. r=valentin

diff --git a/netwerk/protocol/http/HttpBaseChannel.cpp b/netwerk/protocol/http/HttpBaseChannel.cpp
--- a/netwerk/protocol/http/HttpBaseChannel.cpp
+++ b/netwerk/protocol/http/HttpBaseChannel.cpp
@@ -4360,16 +4360,45 @@ HttpBaseChannel::GetLastRedirectFlags(ui
 
 NS_IMETHODIMP
 HttpBaseChannel::SetLastRedirectFlags(uint32_t aValue)
 {
   mLastRedirectFlags = aValue;
   return NS_OK;
 }
 
+nsresult
+HttpBaseChannel::CheckRedirectLimit(uint32_t aRedirectFlags) const
+{
+  if (aRedirectFlags & nsIChannelEventSink::REDIRECT_INTERNAL) {
+    // Some platform features, like Service Workers, depend on internal
+    // redirects.  We should allow some number of internal redirects above
+    // and beyond the normal redirect limit so these features continue
+    // to work.
+    static const int8_t kMinInternalRedirects = 5;
+
+    if (mInternalRedirectCount >= (mRedirectionLimit + kMinInternalRedirects)) {
+      LOG(("internal redirection limit reached!\n"));
+      return NS_ERROR_REDIRECT_LOOP;
+    }
+    return NS_OK;
+  }
+
+  MOZ_ASSERT(aRedirectFlags & (nsIChannelEventSink::REDIRECT_TEMPORARY |
+                               nsIChannelEventSink::REDIRECT_PERMANENT |
+                               nsIChannelEventSink::REDIRECT_STS_UPGRADE));
+
+  if (mRedirectCount >= mRedirectionLimit) {
+    LOG(("redirection limit reached!\n"));
+    return NS_ERROR_REDIRECT_LOOP;
+  }
+
+  return NS_OK;
+}
+
 // NOTE: This function duplicates code from nsBaseChannel. This will go away
 // once HTTP uses nsBaseChannel (part of bug 312760)
 /* static */ void
 HttpBaseChannel::CallTypeSniffers(void *aClosure, const uint8_t *aData,
                                   uint32_t aCount)
 {
   nsIChannel *chan = static_cast<nsIChannel*>(aClosure);
 
diff --git a/netwerk/protocol/http/HttpBaseChannel.h b/netwerk/protocol/http/HttpBaseChannel.h
--- a/netwerk/protocol/http/HttpBaseChannel.h
+++ b/netwerk/protocol/http/HttpBaseChannel.h
@@ -464,16 +464,19 @@ protected:
 #endif
 
   // Called before we create the redirect target channel.
   already_AddRefed<nsILoadInfo> CloneLoadInfoForRedirect(nsIURI *newURI, uint32_t redirectFlags);
 
   static void CallTypeSniffers(void *aClosure, const uint8_t *aData,
                                uint32_t aCount);
 
+  nsresult
+  CheckRedirectLimit(uint32_t aRedirectFlags) const;
+
   friend class PrivateBrowsingChannel<HttpBaseChannel>;
   friend class InterceptFailedOnStop;
 
 protected:
   // this section is for main-thread-only object
   // all the references need to be proxy released on main thread.
   nsCOMPtr<nsIURI> mURI;
   nsCOMPtr<nsIURI> mOriginalURI;
diff --git a/netwerk/protocol/http/HttpChannelChild.cpp b/netwerk/protocol/http/HttpChannelChild.cpp
--- a/netwerk/protocol/http/HttpChannelChild.cpp
+++ b/netwerk/protocol/http/HttpChannelChild.cpp
@@ -3683,16 +3683,25 @@ HttpChannelChild::OverrideWithSynthesize
 
   if (!mSynthesizedInput) {
     rv = NS_NewCStringInputStream(getter_AddRefs(mSynthesizedInput),
                                   EmptyCString());
     NS_ENSURE_SUCCESS_VOID(rv);
   }
 
   if (nsHttpChannel::WillRedirect(mResponseHead)) {
+    // Normally we handle redirect limits in the parent process.  The way
+    // e10s synthesized redirects work, however, the parent process does not
+    // get an accurate redirect count.  Therefore we need to enforce it here.
+    rv = CheckRedirectLimit(nsIChannelEventSink::REDIRECT_TEMPORARY);
+    if (NS_WARN_IF(NS_FAILED(rv))) {
+      Cancel(rv);
+      return;
+    }
+
     mShouldInterceptSubsequentRedirect = true;
     if (mInterceptListener) {
       mInterceptListener->Cleanup();
       mInterceptListener = nullptr;
     }
     // Continue with the original cross-process request
     rv = ContinueAsyncOpen();
     return;
diff --git a/netwerk/protocol/http/InterceptedHttpChannel.cpp b/netwerk/protocol/http/InterceptedHttpChannel.cpp
--- a/netwerk/protocol/http/InterceptedHttpChannel.cpp
+++ b/netwerk/protocol/http/InterceptedHttpChannel.cpp
@@ -68,16 +68,19 @@ InterceptedHttpChannel::SetupReplacement
 {
   nsresult rv = HttpBaseChannel::SetupReplacementChannel(aURI, aChannel,
                                                          aPreserveMethod,
                                                          aRedirectFlags);
   if (NS_FAILED(rv)) {
     return rv;
   }
 
+  rv = CheckRedirectLimit(aRedirectFlags);
+  NS_ENSURE_SUCCESS(rv, rv);
+
   // While we can't resume an synthetic response, we can still propagate
   // the resume params across redirects for other channels to handle.
   if (mResumeStartPos > 0) {
     nsCOMPtr<nsIResumableChannel> resumable = do_QueryInterface(aChannel);
     if (!resumable) {
       return NS_ERROR_NOT_RESUMABLE;
     }
 
@@ -179,20 +182,16 @@ InterceptedHttpChannel::FollowSyntheticR
   NS_ENSURE_SUCCESS(rv, NS_ERROR_FAILURE);
 
   // make sure non-ASCII characters in the location header are escaped.
   nsAutoCString locationBuf;
   if (NS_EscapeURL(location.get(), -1, esc_OnlyNonASCII, locationBuf)) {
     location = locationBuf;
   }
 
-  if (NS_WARN_IF(mRedirectionLimit == 0)) {
-    return NS_ERROR_REDIRECT_LOOP;
-  }
-
   nsCOMPtr<nsIURI> redirectURI;
   rv = ioService->NewURI(nsDependentCString(location.get()),
                          nullptr,
                          mURI,
                          getter_AddRefs(redirectURI));
   NS_ENSURE_SUCCESS(rv, NS_ERROR_CORRUPTED_CONTENT);
 
   uint32_t redirectFlags = nsIChannelEventSink::REDIRECT_TEMPORARY;
diff --git a/netwerk/protocol/http/nsHttpChannel.cpp b/netwerk/protocol/http/nsHttpChannel.cpp
--- a/netwerk/protocol/http/nsHttpChannel.cpp
+++ b/netwerk/protocol/http/nsHttpChannel.cpp
@@ -5362,16 +5362,19 @@ nsHttpChannel::SetupReplacementChannel(n
          this, newChannel, preserveMethod));
 
     nsresult rv =
       HttpBaseChannel::SetupReplacementChannel(newURI, newChannel,
                                                preserveMethod, redirectFlags);
     if (NS_FAILED(rv))
         return rv;
 
+    rv = CheckRedirectLimit(redirectFlags);
+    NS_ENSURE_SUCCESS(rv, rv);
+
     nsCOMPtr<nsIHttpChannel> httpChannel = do_QueryInterface(newChannel);
     if (!httpChannel)
         return NS_OK; // no other options to set
 
     // convey the mApplyConversion flag (bug 91862)
     nsCOMPtr<nsIEncodedChannel> encodedChannel = do_QueryInterface(httpChannel);
     if (encodedChannel)
         encodedChannel->SetApplyConversion(mApplyConversion);
@@ -5402,21 +5405,16 @@ nsHttpChannel::AsyncProcessRedirection(u
     if (NS_FAILED(mResponseHead->GetHeader(nsHttp::Location, location)))
         return NS_ERROR_FAILURE;
 
     // make sure non-ASCII characters in the location header are escaped.
     nsAutoCString locationBuf;
     if (NS_EscapeURL(location.get(), -1, esc_OnlyNonASCII, locationBuf))
         location = locationBuf;
 
-    if (mRedirectCount >= mRedirectionLimit || mInternalRedirectCount >= mRedirectionLimit) {
-        LOG(("redirection limit reached!\n"));
-        return NS_ERROR_REDIRECT_LOOP;
-    }
-
     mRedirectType = redirectType;
 
     LOG(("redirecting to: %s [redirection-limit=%u]\n",
         location.get(), uint32_t(mRedirectionLimit)));
 
     nsresult rv = CreateNewURI(location.get(), getter_AddRefs(mRedirectURI));
 
     if (NS_FAILED(rv)) {
