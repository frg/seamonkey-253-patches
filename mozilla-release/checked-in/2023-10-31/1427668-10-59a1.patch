# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1513906972 28800
# Node ID e1edf80827aa769a8d179fe0cf0e3a411a38c392
# Parent  2f5481b2db0cb9af4ea26b6619dc7234fb5e5f06
Bug 1427668 - Assert that no-alpha backbuffers have 0xff alpha. - r=daoshengmu

MozReview-Commit-ID: 5UJdoud0f2C

diff --git a/dom/canvas/WebGLContext.cpp b/dom/canvas/WebGLContext.cpp
--- a/dom/canvas/WebGLContext.cpp
+++ b/dom/canvas/WebGLContext.cpp
@@ -1544,16 +1544,25 @@ WebGLContext::PresentScreenBuffer()
         GenerateWarning("screen->Resize failed. Losing context.");
         ForceLoseContext();
         return false;
     }
 
     gl->fBindFramebuffer(LOCAL_GL_FRAMEBUFFER, 0);
     BlitBackbufferToCurDriverFB();
 
+#ifdef DEBUG
+    if (!mOptions.alpha) {
+        gl->fBindFramebuffer(LOCAL_GL_FRAMEBUFFER, 0);
+        uint32_t pixel = 3;
+        gl->fReadPixels(0, 0, 1, 1, LOCAL_GL_RGBA, LOCAL_GL_UNSIGNED_BYTE, &pixel);
+        MOZ_ASSERT((pixel & 0xff000000) == 0xff000000);
+    }
+#endif
+
     if (!screen->PublishFrame(screen->Size())) {
         GenerateWarning("PublishFrame failed. Losing context.");
         ForceLoseContext();
         return false;
     }
 
     if (!mOptions.preserveDrawingBuffer) {
         if (gl->IsSupported(gl::GLFeature::invalidate_framebuffer)) {
diff --git a/dom/canvas/WebGLContext.h b/dom/canvas/WebGLContext.h
--- a/dom/canvas/WebGLContext.h
+++ b/dom/canvas/WebGLContext.h
@@ -358,17 +358,17 @@ public:
     NS_IMETHOD GetInputStream(const char* mimeType,
                               const char16_t* encoderOptions,
                               nsIInputStream** out_stream) override;
 
     virtual already_AddRefed<mozilla::gfx::SourceSurface>
     GetSurfaceSnapshot(gfxAlphaType* out_alphaType) override;
 
     virtual void SetIsOpaque(bool) override {};
-    bool GetIsOpaque() override { return false; }
+    bool GetIsOpaque() override { return !mOptions.alpha; }
     NS_IMETHOD SetContextOptions(JSContext* cx,
                                  JS::Handle<JS::Value> options,
                                  ErrorResult& aRvForDictionaryInit) override;
 
     NS_IMETHOD SetIsIPC(bool) override {
         return NS_ERROR_NOT_IMPLEMENTED;
     }
 
diff --git a/gfx/gl/GLContext.h b/gfx/gl/GLContext.h
--- a/gfx/gl/GLContext.h
+++ b/gfx/gl/GLContext.h
@@ -1539,17 +1539,16 @@ public:
 
     void fReadBuffer(GLenum mode) {
         BEFORE_GL_CALL;
         mSymbols.fReadBuffer(mode);
         AFTER_GL_CALL;
     }
 
     void raw_fReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid* pixels) {
-        ASSERT_NOT_PASSING_STACK_BUFFER_TO_GL(pixels);
         BEFORE_GL_CALL;
         mSymbols.fReadPixels(x, y, width, height, format, type, pixels);
         OnSyncCall();
         AFTER_GL_CALL;
         mHeavyGLCallsSinceLastFlush = true;
     }
 
     void fReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format,
diff --git a/gfx/gl/GLReadTexImageHelper.cpp b/gfx/gl/GLReadTexImageHelper.cpp
--- a/gfx/gl/GLReadTexImageHelper.cpp
+++ b/gfx/gl/GLReadTexImageHelper.cpp
@@ -627,21 +627,16 @@ GLReadTexImageHelper::ReadTexImage(DataS
         ScopedVertexAttribPointer autoAttrib1(mGL, 1, 2, LOCAL_GL_FLOAT, LOCAL_GL_FALSE, 0, 0, texCoordArray);
 
         /* Bind the texture */
         if (aTextureId) {
             mGL->fBindTexture(aTextureTarget, aTextureId);
             CLEANUP_IF_GLERROR_OCCURRED("when binding texture");
         }
 
-        /* Draw quad */
-        mGL->fClearColor(1.0f, 0.0f, 1.0f, 1.0f);
-        mGL->fClear(LOCAL_GL_COLOR_BUFFER_BIT);
-        CLEANUP_IF_GLERROR_OCCURRED("when clearing color buffer");
-
         mGL->fDrawArrays(LOCAL_GL_TRIANGLE_STRIP, 0, 4);
         CLEANUP_IF_GLERROR_OCCURRED("when drawing texture");
 
         /* Read-back draw results */
         ReadPixelsIntoDataSurface(mGL, aDest);
         CLEANUP_IF_GLERROR_OCCURRED("when reading pixels into surface");
     } while (false);
 
diff --git a/gfx/layers/CopyableCanvasRenderer.cpp b/gfx/layers/CopyableCanvasRenderer.cpp
--- a/gfx/layers/CopyableCanvasRenderer.cpp
+++ b/gfx/layers/CopyableCanvasRenderer.cpp
@@ -149,18 +149,18 @@ CopyableCanvasRenderer::ReadbackSurface(
   }
 
   if (!frontbuffer) {
     NS_WARNING("Null frame received.");
     return nullptr;
   }
 
   IntSize readSize(frontbuffer->mSize);
-  SurfaceFormat format = frontbuffer->mHasAlpha ? SurfaceFormat::B8G8R8X8
-                                                : SurfaceFormat::B8G8R8A8;
+  SurfaceFormat format = frontbuffer->mHasAlpha ? SurfaceFormat::B8G8R8A8
+                                                : SurfaceFormat::B8G8R8X8;
   bool needsPremult = frontbuffer->mHasAlpha && !mIsAlphaPremultiplied;
 
   RefPtr<DataSourceSurface> resultSurf = GetTempSurface(readSize, format);
   // There will already be a warning from inside of GetTempSurface, but
   // it doesn't hurt to complain:
   if (NS_WARN_IF(!resultSurf)) {
     return nullptr;
   }
