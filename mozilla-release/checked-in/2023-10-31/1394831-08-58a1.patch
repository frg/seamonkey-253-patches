# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1509532792 -3600
#      Wed Nov 01 11:39:52 2017 +0100
# Node ID 444453765199fc52a86a1d44832f893a02e7ff80
# Parent  b346f9223045cf96218003982ba9735127b8768a
Bug 1394831 part 8 - Remove flags argument from addAccessorProperty. r=bhackett

diff --git a/js/src/jsarray.cpp b/js/src/jsarray.cpp
--- a/js/src/jsarray.cpp
+++ b/js/src/jsarray.cpp
@@ -1017,17 +1017,17 @@ AddLengthProperty(JSContext* cx, HandleA
      */
 
     RootedId lengthId(cx, NameToId(cx->names().length));
     MOZ_ASSERT(!obj->lookup(cx, lengthId));
 
     return NativeObject::addAccessorProperty(cx, obj, lengthId,
                                              array_length_getter, array_length_setter,
                                              JSPROP_PERMANENT | JSPROP_SHADOWABLE,
-                                             0, /* allowDictionary = */ false);
+                                             /* allowDictionary = */ false);
 }
 
 static bool
 IsArrayConstructor(const JSObject* obj)
 {
     // This must only return true if v is *the* Array constructor for the
     // current compartment; we rely on the fact that any other Array
     // constructor would be represented as a wrapper.
diff --git a/js/src/vm/NativeObject.cpp b/js/src/vm/NativeObject.cpp
--- a/js/src/vm/NativeObject.cpp
+++ b/js/src/vm/NativeObject.cpp
@@ -1447,17 +1447,17 @@ AddOrChangeProperty(JSContext* cx, Handl
     // the slower putProperty.
     Shape* shape;
     if (AddOrChange == IsAddOrChange::Add) {
         if (Shape::isDataProperty(desc.attributes(), desc.getter(), desc.setter())) {
             shape = NativeObject::addDataProperty(cx, obj, id, SHAPE_INVALID_SLOT,
                                                   desc.attributes());
         } else {
             shape = NativeObject::addAccessorProperty(cx, obj, id, desc.getter(), desc.setter(),
-                                                      desc.attributes(), 0);
+                                                      desc.attributes());
         }
     } else {
         if (Shape::isDataProperty(desc.attributes(), desc.getter(), desc.setter())) {
             shape = NativeObject::putDataProperty(cx, obj, id, desc.attributes());
         } else {
             shape = NativeObject::putAccessorProperty(cx, obj, id, desc.getter(), desc.setter(),
                                                       desc.attributes());
         }
diff --git a/js/src/vm/NativeObject.h b/js/src/vm/NativeObject.h
--- a/js/src/vm/NativeObject.h
+++ b/js/src/vm/NativeObject.h
@@ -853,18 +853,18 @@ class NativeObject : public ShapedObject
 
   public:
     /* Add a property whose id is not yet in this scope. */
     static MOZ_ALWAYS_INLINE Shape* addDataProperty(JSContext* cx, HandleNativeObject obj, HandleId id,
                                                     uint32_t slot, unsigned attrs);
 
     static MOZ_ALWAYS_INLINE Shape* addAccessorProperty(JSContext* cx, HandleNativeObject obj, HandleId id,
                                                         JSGetterOp getter, JSSetterOp setter,
-                                                        unsigned attrs, unsigned flags,
-                                                        bool allowDictionary = true);
+                                                        unsigned attrs, bool allowDictionary = true);
+
     static Shape* addEnumerableDataProperty(JSContext* cx, HandleNativeObject obj, HandleId id);
 
     /* Add a data property whose id is not yet in this scope. */
     static Shape* addDataProperty(JSContext* cx, HandleNativeObject obj,
                                   HandlePropertyName name, uint32_t slot, unsigned attrs);
 
     /* Add or overwrite a property for id in this scope. */
     static Shape*
@@ -896,17 +896,17 @@ class NativeObject : public ShapedObject
     static Shape*
     addDataPropertyInternal(JSContext* cx, HandleNativeObject obj, HandleId id,
                             uint32_t slot, unsigned attrs, ShapeTable::Entry* entry,
                             const AutoKeepShapeTables& keep);
 
     static Shape*
     addAccessorPropertyInternal(JSContext* cx, HandleNativeObject obj, HandleId id,
                                 JSGetterOp getter, JSSetterOp setter, unsigned attrs,
-                                unsigned flags, ShapeTable::Entry* entry, bool allowDictionary,
+                                ShapeTable::Entry* entry, bool allowDictionary,
                                 const AutoKeepShapeTables& keep);
 
     static MOZ_MUST_USE bool fillInAfterSwap(JSContext* cx, HandleNativeObject obj,
                                              const Vector<Value>& values, void* priv);
 
   public:
     // Return true if this object has been converted from shared-immutable
     // prototype-rooted shape storage to dictionary-shapes in a doubly-linked
diff --git a/js/src/vm/Shape-inl.h b/js/src/vm/Shape-inl.h
--- a/js/src/vm/Shape-inl.h
+++ b/js/src/vm/Shape-inl.h
@@ -407,30 +407,30 @@ NativeObject::addDataProperty(JSContext*
     }
 
     return addDataPropertyInternal(cx, obj, id, slot, attrs, entry, keep);
 }
 
 /* static */ MOZ_ALWAYS_INLINE Shape*
 NativeObject::addAccessorProperty(JSContext* cx, HandleNativeObject obj, HandleId id,
                                   GetterOp getter, SetterOp setter, unsigned attrs,
-                                  unsigned flags, bool allowDictionary)
+                                  bool allowDictionary)
 {
     MOZ_ASSERT(!JSID_IS_VOID(id));
     MOZ_ASSERT(obj->uninlinedNonProxyIsExtensible());
     MOZ_ASSERT(!obj->containsPure(id));
 
     AutoKeepShapeTables keep(cx);
     ShapeTable::Entry* entry = nullptr;
     if (obj->inDictionaryMode()) {
         ShapeTable* table = obj->lastProperty()->ensureTableForDictionary(cx, keep);
         if (!table)
             return nullptr;
         entry = &table->search<MaybeAdding::Adding>(id, keep);
     }
 
-    return addAccessorPropertyInternal(cx, obj, id, getter, setter, attrs, flags, entry,
+    return addAccessorPropertyInternal(cx, obj, id, getter, setter, attrs, entry,
                                        allowDictionary, keep);
 }
 
 } /* namespace js */
 
 #endif /* vm_Shape_inl_h */
diff --git a/js/src/vm/Shape.cpp b/js/src/vm/Shape.cpp
--- a/js/src/vm/Shape.cpp
+++ b/js/src/vm/Shape.cpp
@@ -474,17 +474,17 @@ class MOZ_RAII AutoCheckShapeConsistency
 };
 
 } // namespace js
 
 /* static */ Shape*
 NativeObject::addAccessorPropertyInternal(JSContext* cx,
                                           HandleNativeObject obj, HandleId id,
                                           GetterOp getter, SetterOp setter,
-                                          unsigned attrs, unsigned flags, ShapeTable::Entry* entry,
+                                          unsigned attrs, ShapeTable::Entry* entry,
                                           bool allowDictionary, const AutoKeepShapeTables& keep)
 {
     MOZ_ASSERT_IF(!allowDictionary, !obj->inDictionaryMode());
 
     AutoCheckShapeConsistency check(obj);
     AutoRooterGetterSetter gsRoot(cx, attrs, &getter, &setter);
 
     /*
@@ -516,17 +516,17 @@ NativeObject::addAccessorPropertyInterna
     /* Find or create a property tree node labeled by our arguments. */
     RootedShape shape(cx);
     {
         RootedShape last(cx, obj->lastProperty());
         Rooted<UnownedBaseShape*> nbase(cx, GetBaseShapeForNewShape(cx, last, id));
         if (!nbase)
             return nullptr;
 
-        Rooted<StackShape> child(cx, StackShape(nbase, id, SHAPE_INVALID_SLOT, attrs, flags));
+        Rooted<StackShape> child(cx, StackShape(nbase, id, SHAPE_INVALID_SLOT, attrs, 0));
         child.updateGetterSetter(getter, setter);
         shape = getChildProperty(cx, obj, last, &child);
         if (!shape)
             return nullptr;
     }
 
     MOZ_ASSERT(shape == obj->lastProperty());
 
@@ -948,18 +948,17 @@ NativeObject::putAccessorProperty(JSCont
 
     if (!shape) {
         /*
          * You can't add properties to a non-extensible object, but you can change
          * attributes of properties in such objects.
          */
         MOZ_ASSERT(obj->nonProxyIsExtensible());
 
-        return addAccessorPropertyInternal(cx, obj, id, getter, setter, attrs, 0, entry, true,
-                                           keep);
+        return addAccessorPropertyInternal(cx, obj, id, getter, setter, attrs, entry, true, keep);
     }
 
     /* Property exists: search must have returned a valid entry. */
     MOZ_ASSERT_IF(entry, !entry->isRemoved());
 
     AssertCanChangeAttrs(shape, attrs);
 
     bool hadSlot = shape->isDataProperty();
