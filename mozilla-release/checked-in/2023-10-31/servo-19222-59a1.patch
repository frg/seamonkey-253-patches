# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1510746024 21600
# Node ID 311a0f15026f3a5f23faa037b27ffeb9f676f565
# Parent  3b3d63ed5ae8a563e76fa3ca1e42a181fd73bbc2
servo: Merge #19222 - Serialize media rule and supports rule like Gecko (from upsuper:serialize-group-rule); r=emilio

This should fix [bug 1417207](https://bugzilla.mozilla.org/show_bug.cgi?id=1417207).

Source-Repo: https://github.com/servo/servo
Source-Revision: b2c19b15447b126cbcb4726fccb416675532dd11

diff --git a/servo/components/style/stylesheets/media_rule.rs b/servo/components/style/stylesheets/media_rule.rs
--- a/servo/components/style/stylesheets/media_rule.rs
+++ b/servo/components/style/stylesheets/media_rule.rs
@@ -41,22 +41,17 @@ impl MediaRule {
 
 impl ToCssWithGuard for MediaRule {
     // Serialization of MediaRule is not specced.
     // https://drafts.csswg.org/cssom/#serialize-a-css-rule CSSMediaRule
     fn to_css<W>(&self, guard: &SharedRwLockReadGuard, dest: &mut W) -> fmt::Result
     where W: fmt::Write {
         dest.write_str("@media ")?;
         self.media_queries.read_with(guard).to_css(dest)?;
-        dest.write_str(" {")?;
-        for rule in self.rules.read_with(guard).0.iter() {
-            dest.write_str(" ")?;
-            rule.to_css(guard, dest)?;
-        }
-        dest.write_str(" }")
+        self.rules.read_with(guard).to_css_block(guard, dest)
     }
 }
 
 impl DeepCloneWithLock for MediaRule {
     fn deep_clone_with_lock(
         &self,
         lock: &SharedRwLock,
         guard: &SharedRwLockReadGuard,
diff --git a/servo/components/style/stylesheets/rule_list.rs b/servo/components/style/stylesheets/rule_list.rs
--- a/servo/components/style/stylesheets/rule_list.rs
+++ b/servo/components/style/stylesheets/rule_list.rs
@@ -2,17 +2,19 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! A list of CSS rules.
 
 #[cfg(feature = "gecko")]
 use malloc_size_of::{MallocShallowSizeOf, MallocSizeOfOps};
 use servo_arc::{Arc, RawOffsetArc};
-use shared_lock::{DeepCloneParams, DeepCloneWithLock, Locked, SharedRwLock, SharedRwLockReadGuard};
+use shared_lock::{DeepCloneParams, DeepCloneWithLock, Locked};
+use shared_lock::{SharedRwLock, SharedRwLockReadGuard, ToCssWithGuard};
+use std::fmt;
 use stylesheets::{CssRule, RulesMutateError};
 use stylesheets::loader::StylesheetLoader;
 use stylesheets::rule_parser::State;
 use stylesheets::stylesheet::StylesheetContents;
 
 /// A list of CSS rules.
 #[derive(Debug)]
 pub struct CssRules(pub Vec<CssRule>);
@@ -83,16 +85,31 @@ impl CssRules {
                 }
             }
         }
 
         // Step 5, 6
         self.0.remove(index);
         Ok(())
     }
+
+    /// Serializes this CSSRules to CSS text as a block of rules.
+    ///
+    /// This should be speced into CSSOM spec at some point. See
+    /// <https://github.com/w3c/csswg-drafts/issues/1985>
+    pub fn to_css_block<W>(&self, guard: &SharedRwLockReadGuard, dest: &mut W)
+        -> fmt::Result where W: fmt::Write
+    {
+        dest.write_str(" {")?;
+        for rule in self.0.iter() {
+            dest.write_str("\n  ")?;
+            rule.to_css(guard, dest)?;
+        }
+        dest.write_str("\n}")
+    }
 }
 
 /// A trait to implement helpers for `Arc<Locked<CssRules>>`.
 pub trait CssRulesHelpers {
     /// <https://drafts.csswg.org/cssom/#insert-a-css-rule>
     ///
     /// Written in this funky way because parsing an @import rule may cause us
     /// to clone a stylesheet from the same document due to caching in the CSS
diff --git a/servo/components/style/stylesheets/supports_rule.rs b/servo/components/style/stylesheets/supports_rule.rs
--- a/servo/components/style/stylesheets/supports_rule.rs
+++ b/servo/components/style/stylesheets/supports_rule.rs
@@ -42,22 +42,17 @@ impl SupportsRule {
     }
 }
 
 impl ToCssWithGuard for SupportsRule {
     fn to_css<W>(&self, guard: &SharedRwLockReadGuard, dest: &mut W) -> fmt::Result
     where W: fmt::Write {
         dest.write_str("@supports ")?;
         self.condition.to_css(dest)?;
-        dest.write_str(" {")?;
-        for rule in self.rules.read_with(guard).0.iter() {
-            dest.write_str(" ")?;
-            rule.to_css(guard, dest)?;
-        }
-        dest.write_str(" }")
+        self.rules.read_with(guard).to_css_block(guard, dest)
     }
 }
 
 impl DeepCloneWithLock for SupportsRule {
     fn deep_clone_with_lock(
         &self,
         lock: &SharedRwLock,
         guard: &SharedRwLockReadGuard,
