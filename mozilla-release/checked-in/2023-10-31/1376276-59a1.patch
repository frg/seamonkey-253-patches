# HG changeset patch
# User Dan Minor <dminor@mozilla.com>
# Date 1509119157 14400
# Node ID 009adb3d4e24b51e34ef8cb2f19444864bd7d14f
# Parent  de7bbf9922d8e4c582a25f953023a23c84b1b464
Bug 1376276 - Fix limiting framerate in getusermedia with screen on Windows; r=jesup

This changes code in PlatformUIThread::Run to match what was present in
ThreadWindowsUI::Run prior to the branch 49 update. With this update in place
it is possible to limit framerate on Windows again. The limit does not work
as well as on other platforms, the actual framerate seems to be 1.5x to 2x
the request value on the systems I have tested. That was also the case prior
to the branch 49 update.

MozReview-Commit-ID: W5xnWfkaET

diff --git a/media/webrtc/trunk/webrtc/base/platform_thread.cc b/media/webrtc/trunk/webrtc/base/platform_thread.cc
--- a/media/webrtc/trunk/webrtc/base/platform_thread.cc
+++ b/media/webrtc/trunk/webrtc/base/platform_thread.cc
@@ -296,18 +296,39 @@ void PlatformThread::Run() {
 #else
   } while (!stop_event_.Wait(0));
 #endif  // defined(WEBRTC_WIN)
 }
 
 #if defined(WEBRTC_WIN)
 void PlatformUIThread::Run() {
   RTC_CHECK(InternalInit()); // always evaluates
-  PlatformThread::Run();
-  // Don't need to DestroyWindow(hwnd_) due to WM_CLOSE->WM_DESTROY handling
+  do {
+    // The interface contract of Start/Stop is that for a successful call to
+    // Start, there should be at least one call to the run function.  So we
+    // call the function before checking |stop_|.
+    if (!run_function_(obj_))
+      break;
+
+    // Alertable sleep to permit RaiseFlag to run and update |stop_|.
+    if (MsgWaitForMultipleObjectsEx(0, nullptr, INFINITE, QS_ALLINPUT,
+                                    MWMO_ALERTABLE | MWMO_INPUTAVAILABLE) ==
+        WAIT_OBJECT_0) {
+      MSG msg;
+      if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
+        if (msg.message == WM_QUIT) {
+          stop_ = true;
+          break;
+        }
+        TranslateMessage(&msg);
+        DispatchMessage(&msg);
+      }
+    }
+
+  } while (!stop_);
 }
 
 void PlatformUIThread::NativeEventCallback() {
   if (!run_function_) {
     stop_ = true;
     return;
   }
   stop_ = !run_function_(obj_);
