# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1511402824 -39600
# Node ID b84316f4f60c2ceba94dd3a285754b8a2c70d247
# Parent  d26f75be5ced1b87d1e2ac624640b25492c02c03
Bug 1394578 - Remove the kPref* enum. r=glandium

It's something of an obfuscation, and it forces together various bool values
that don't necessarily have to be together (and won't be together after future
refactorings).

The patch also reorders some function arguments for consistency: PrefType, then
PrefValueKind, then PrefValue.

MozReview-Commit-ID: KNY0Pxo0Nxf

diff --git a/modules/libpref/Preferences.cpp b/modules/libpref/Preferences.cpp
--- a/modules/libpref/Preferences.cpp
+++ b/modules/libpref/Preferences.cpp
@@ -223,23 +223,16 @@ StrEscape(const char* aOriginal, nsCStri
         aResult.Append(*p);
         break;
     }
   }
 
   aResult.Append('"');
 }
 
-enum
-{
-  kPrefSetDefault = 1,
-  kPrefForceSet = 2,
-  kPrefSticky = 4,
-};
-
 static ArenaAllocator<8192, 1> gPrefNameArena;
 
 class Pref : public PLDHashEntryHdr
 {
 public:
   Pref(const char* aName, PrefType aType)
   {
     mName = ArenaStrdup(aName, gPrefNameArena);
@@ -486,32 +479,34 @@ public:
       free(const_cast<char*>(mUserValue.mStringVal));
       mUserValue.mStringVal = nullptr;
     }
 
     mHasUserValue = false;
   }
 
   nsresult SetValue(PrefType aType,
+                    PrefValueKind aKind,
                     PrefValue aValue,
-                    uint32_t aFlags,
+                    bool aIsSticky,
+                    bool aForceSet,
                     bool* aValueChanged,
                     bool* aDirty)
   {
-    if (aFlags & kPrefSetDefault) {
+    if (aKind == PrefValueKind::Default) {
       // Types must always match when setting the default value.
       if (!IsType(aType)) {
         return NS_ERROR_UNEXPECTED;
       }
 
       // Should we set the default value? Only if the pref is not locked, and
       // doing so would change the default value.
       if (!IsLocked() && !ValueMatches(PrefValueKind::Default, aType, aValue)) {
         ReplaceValue(PrefValueKind::Default, aType, aValue);
-        if (aFlags & kPrefSticky) {
+        if (aIsSticky) {
           mIsSticky = true;
         }
         if (!mHasUserValue) {
           *aValueChanged = true;
         }
         // What if we change the default to be the same as the user value?
         // Should we clear the user value? Currently we don't.
       }
@@ -521,17 +516,17 @@ public:
       if (mHasDefaultValue && !IsType(aType)) {
         return NS_ERROR_UNEXPECTED;
       }
 
       // Should we clear the user value, if present? Only if the new user value
       // matches the default value, and the pref isn't sticky, and we aren't
       // force-setting it.
       if (ValueMatches(PrefValueKind::Default, aType, aValue) && !mIsSticky &&
-          !(aFlags & kPrefForceSet)) {
+          !aForceSet) {
         if (mHasUserValue) {
           ClearUserValue();
           if (!IsLocked()) {
             *aDirty = true;
             *aValueChanged = true;
           }
         }
 
@@ -756,19 +751,21 @@ pref_HashTableLookup(const char* aKey)
   }
 #endif
 
   return static_cast<Pref*>(gHashTable->Search(aKey));
 }
 
 static nsresult
 pref_SetPref(const char* aPrefName,
+             PrefType aType,
+             PrefValueKind aKind,
              PrefValue aValue,
-             PrefType aType,
-             uint32_t aFlags)
+             bool aIsSticky,
+             bool aForceSet)
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   if (!gHashTable) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
 
   auto pref = static_cast<Pref*>(gHashTable->Add(aPrefName, fallible));
@@ -777,24 +774,24 @@ pref_SetPref(const char* aPrefName,
   }
 
   if (!pref->Name()) {
     // New (zeroed) entry. Initialize it.
     new (pref) Pref(aPrefName, aType);
   }
 
   bool valueChanged = false, handleDirty = false;
-  nsresult rv =
-    pref->SetValue(aType, aValue, aFlags, &valueChanged, &handleDirty);
+  nsresult rv = pref->SetValue(
+    aType, aKind, aValue, aIsSticky, aForceSet, &valueChanged, &handleDirty);
   if (NS_FAILED(rv)) {
     NS_WARNING(
       nsPrintfCString(
         "Rejected attempt to change type of pref %s's %s value from %s to %s",
         aPrefName,
-        (aFlags & kPrefSetDefault) ? "default" : "user",
+        (aKind == PrefValueKind::Default) ? "default" : "user",
         PrefTypeToString(pref->Type()),
         PrefTypeToString(aType))
         .get());
 
     return rv;
   }
 
   if (handleDirty) {
@@ -1005,26 +1002,26 @@ Parser::GrowBuf()
 
 void
 Parser::HandleValue(const char* aPrefName,
                     PrefType aType,
                     PrefValue aValue,
                     bool aIsDefault,
                     bool aIsSticky)
 {
-  uint32_t flags = 0;
+  PrefValueKind kind;
+  bool forceSet;
   if (aIsDefault) {
-    flags |= kPrefSetDefault;
-    if (aIsSticky) {
-      flags |= kPrefSticky;
-    }
+    kind = PrefValueKind::Default;
+    forceSet = false;
   } else {
-    flags |= kPrefForceSet;
+    kind = PrefValueKind::User;
+    forceSet = true;
   }
-  pref_SetPref(aPrefName, aValue, aType, flags);
+  pref_SetPref(aPrefName, aType, kind, aValue, aIsSticky, forceSet);
 }
 
 // Report an error or a warning. If not specified, just dump to stderr.
 void
 Parser::ReportProblem(const char* aMessage, int aLine, bool aError)
 {
   nsPrintfCString message("** Preference parsing %s (line %d) = %s **\n",
                           (aError ? "error" : "warning"),
@@ -4466,19 +4463,21 @@ Preferences::SetCStringInAnyProcess(cons
   }
 
   // It's ok to stash a pointer to the temporary PromiseFlatCString's chars in
   // pref because pref_SetPref() duplicates those chars.
   PrefValue prefValue;
   const nsCString& flat = PromiseFlatCString(aValue);
   prefValue.mStringVal = flat.get();
   return pref_SetPref(aPrefName,
+                      PrefType::String,
+                      aKind,
                       prefValue,
-                      PrefType::String,
-                      aKind == PrefValueKind::Default ? kPrefSetDefault : 0);
+                      /* isSticky */ false,
+                      /* forceSet */ false);
 }
 
 /* static */ nsresult
 Preferences::SetCString(const char* aPrefName,
                         const nsACString& aValue,
                         PrefValueKind aKind)
 {
   ENSURE_PARENT_PROCESS("SetCString", aPrefName);
@@ -4490,19 +4489,21 @@ Preferences::SetBoolInAnyProcess(const c
                                  bool aValue,
                                  PrefValueKind aKind)
 {
   NS_ENSURE_TRUE(InitStaticMembers(), NS_ERROR_NOT_AVAILABLE);
 
   PrefValue prefValue;
   prefValue.mBoolVal = aValue;
   return pref_SetPref(aPrefName,
+                      PrefType::Bool,
+                      aKind,
                       prefValue,
-                      PrefType::Bool,
-                      aKind == PrefValueKind::Default ? kPrefSetDefault : 0);
+                      /* isSticky */ false,
+                      /* forceSet */ false);
 }
 
 /* static */ nsresult
 Preferences::SetBool(const char* aPrefName, bool aValue, PrefValueKind aKind)
 {
   ENSURE_PARENT_PROCESS("SetBool", aPrefName);
   return SetBoolInAnyProcess(aPrefName, aValue, aKind);
 }
@@ -4512,19 +4513,21 @@ Preferences::SetIntInAnyProcess(const ch
                                 int32_t aValue,
                                 PrefValueKind aKind)
 {
   NS_ENSURE_TRUE(InitStaticMembers(), NS_ERROR_NOT_AVAILABLE);
 
   PrefValue prefValue;
   prefValue.mIntVal = aValue;
   return pref_SetPref(aPrefName,
+                      PrefType::Int,
+                      aKind,
                       prefValue,
-                      PrefType::Int,
-                      aKind == PrefValueKind::Default ? kPrefSetDefault : 0);
+                      /* isSticky */ false,
+                      /* forceSet */ false);
 }
 
 /* static */ nsresult
 Preferences::SetInt(const char* aPrefName, int32_t aValue, PrefValueKind aKind)
 {
   ENSURE_PARENT_PROCESS("SetInt", aPrefName);
   return SetIntInAnyProcess(aPrefName, aValue, aKind);
 }
