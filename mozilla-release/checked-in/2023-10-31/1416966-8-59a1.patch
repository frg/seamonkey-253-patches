# HG changeset patch
# User Hiroyuki Ikezoe <hikezoe@mozilla.com>
# Date 1513071845 -32400
# Node ID 7ad629927e45b991464d680a1a7b39e2b25d67cf
# Parent  39850d2b42f6249164c40a89c65137ca3d48b4f3
Bug 1416966 - Drop the check for the new micro task checkpoint for animations. r=birtles

MozReview-Commit-ID: HVxDrxV3Qmb

diff --git a/dom/animation/test/mozilla/file_restyles.html b/dom/animation/test/mozilla/file_restyles.html
--- a/dom/animation/test/mozilla/file_restyles.html
+++ b/dom/animation/test/mozilla/file_restyles.html
@@ -100,86 +100,48 @@ function startsRightNow(aAnimation) {
 }
 
 var omtaEnabled = isOMTAEnabled();
 
 var isAndroid = !!navigator.userAgent.includes("Android");
 var isServo = isStyledByServo();
 var offscreenThrottlingEnabled =
   SpecialPowers.getBoolPref('dom.animations.offscreen-throttling');
-var hasMicroTaskCheckpointForAnimation;
 
 function add_task_if_omta_enabled(test) {
   if (!omtaEnabled) {
     info(test.name + " is skipped because OMTA is disabled");
     return;
   }
   add_task(test);
 }
 
 // We need to wait for all paints before running tests to avoid contaminations
 // from styling of this document itself.
 waitForAllPaints(() => {
-  // Drop this once we have the micro task checkpoint for animations
-  // (bug 1416966).
-  add_task(async function check_micro_task_checkpoint_for_animations() {
-    const div = addDiv(null);
-
-    async function checkMicroTaskCheckpoint() {
-      return new Promise(resolve => {
-        let didRequestAnimationFrame = false;
-
-        requestAnimationFrame(() => {
-          const animation = div.animate({ }, 100 * MS_PER_SEC);
-
-          requestAnimationFrame(() => {
-            didRequestAnimationFrame = true;
-          });
-
-          animation.ready.then(() => {
-            // If the animation.ready.then callback is called before the next
-            // requestAnimationFrame callback, that means that there is a
-            // micro task checkpoint between animation tick and
-            // requestAnimationFrame callbacks.
-            resolve(!didRequestAnimationFrame);
-          });
-        });
-      });
-    }
-
-    hasMicroTaskCheckpointForAnimation = await checkMicroTaskCheckpoint();
-
-    await ensureElementRemoval(div);
-  });
-
   add_task(async function restyling_for_main_thread_animations() {
     var div = addDiv(null, { style: 'animation: background-color 100s' });
     var animation = div.getAnimations()[0];
 
     await animation.ready;
     ok(!SpecialPowers.wrap(animation).isRunningOnCompositor);
 
     // Normally we expect one restyling for each requestAnimationFrame (as
     // called by observeRestyling) PLUS one for the last frame becasue of bug
     // 1193394.  However, we won't observe that initial restyling unless BOTH of
     // the following two conditions hold:
     //
-    // 1. We are running *before* restyling happens.  This only happens if we
-    //    perform a micro task checkpoint after resolving the 'ready' promise
-    //    above (bug 1416966).
+    // 1. We are running *before* restyling happens.
     // 2. The animation actually needs a restyle because it started prior to
     //    this frame.  Even if (1) is true, in some cases due to aligning with
     //    the refresh driver, the animation fame in which the ready promise is
     //    resolved happens to coincide perfectly with the start time of the
     //    animation.  In this case no restyling is needed so we won't observe
     //    an additional restyle.
-    const expectedRestyleCount =
-      hasMicroTaskCheckpointForAnimation && !startsRightNow(animation)
-        ? 6
-        : 5;
+    const expectedRestyleCount = !startsRightNow(animation) ? 6 : 5;
     var markers = await observeStyling(5);
     is(markers.length, expectedRestyleCount,
        'CSS animations running on the main-thread should update style ' +
        'on the main thread');
     await ensureElementRemoval(div);
   });
 
   add_task_if_omta_enabled(async function no_restyling_for_compositor_animations() {
@@ -959,20 +921,17 @@ waitForAllPaints(() => {
     }
 
     var div = addDiv(null, { style: 'transform: translateY(-400px);' });
     var animation =
       div.animate([{ visibility: 'visible' }], 100 * MS_PER_SEC);
 
     await animation.ready;
 
-    const expectedRestyleCount =
-      hasMicroTaskCheckpointForAnimation && !startsRightNow(animation)
-        ? 6
-        : 5;
+    const expectedRestyleCount = !startsRightNow(animation) ? 6 : 5;
     var markers = await observeStyling(5);
 
     is(markers.length, expectedRestyleCount,
        'Discrete animation has has no keyframe whose offset is 0 or 1 in an ' +
        'out-of-view element should not be throttled');
     await ensureElementRemoval(div);
   });
 
@@ -1030,20 +989,17 @@ waitForAllPaints(() => {
     var rect = addSVGElement(svg, 'rect', { x:      '-10',
                                             y:      '-10',
                                             width:  '10',
                                             height: '10',
                                             fill:   'red' });
     var animation = rect.animate({ fill: ['blue', 'lime'] }, 100 * MS_PER_SEC);
     await animation.ready;
 
-    const expectedRestyleCount =
-      hasMicroTaskCheckpointForAnimation && !startsRightNow(animation)
-        ? 6
-        : 5;
+    const expectedRestyleCount = !startsRightNow(animation) ? 6 : 5;
     var markers = await observeStyling(5);
     is(markers.length, expectedRestyleCount,
        'CSS animations on an in-view svg element with post-transform should ' +
        'not be throttled.');
 
     await ensureElementRemoval(div);
   });
 
@@ -1094,20 +1050,17 @@ waitForAllPaints(() => {
     var targetDiv = addDiv(null,
                            { style: 'animation: background-color 100s;' +
                                     'transform: translate(-50px, -50px);' });
     scrollDiv.appendChild(targetDiv);
 
     var animation = targetDiv.getAnimations()[0];
     await animation.ready;
 
-    const expectedRestyleCount =
-      hasMicroTaskCheckpointForAnimation && !startsRightNow(animation)
-        ? 6
-        : 5;
+    const expectedRestyleCount = !startsRightNow(animation) ? 6 : 5;
     var markers = await observeStyling(5);
     is(markers.length, expectedRestyleCount,
        'CSS animation on an in-view element with pre-transform should not ' +
        'be throttled.');
 
     await ensureElementRemoval(scrollDiv);
   });
 

