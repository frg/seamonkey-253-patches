# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1508567154 25200
#      Fri Oct 20 23:25:54 2017 -0700
# Node ID 26b1ee21f3658d945fef274318c0960f54b906c2
# Parent  1550b8ffee8186e008e5ab57de330fe2dace1113
Bug 1379686 - Make ForwardingProxyHandler::isConstructor query and return whether the target is a constructor, rather than pretending is-constructor is identical to is-callable.  r=till

diff --git a/js/src/jit-test/tests/self-hosting/is-constructor-inlined.js b/js/src/jit-test/tests/self-hosting/is-constructor-inlined.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/self-hosting/is-constructor-inlined.js
@@ -0,0 +1,16 @@
+var g = newGlobal();
+var w = g.eval("() => {}");
+var v = g.eval("Array");
+
+function f()
+{
+  try {
+    Reflect.construct(v, {}, w);
+  } catch (e) {
+    assertEq(e instanceof TypeError, true);
+  }
+}
+
+f();
+f();
+f();
diff --git a/js/src/proxy/Wrapper.cpp b/js/src/proxy/Wrapper.cpp
--- a/js/src/proxy/Wrapper.cpp
+++ b/js/src/proxy/Wrapper.cpp
@@ -291,27 +291,25 @@ ForwardingProxyHandler::boxedValue_unbox
 {
     RootedObject target(cx, proxy->as<ProxyObject>().target());
     return Unbox(cx, target, vp);
 }
 
 bool
 ForwardingProxyHandler::isCallable(JSObject* obj) const
 {
-    JSObject * target = obj->as<ProxyObject>().target();
+    JSObject* target = obj->as<ProxyObject>().target();
     return target->isCallable();
 }
 
 bool
 ForwardingProxyHandler::isConstructor(JSObject* obj) const
 {
-    // For now, all wrappers are constructable if they are callable. We will want to eventually
-    // decouple this behavior, but none of the Wrapper infrastructure is currently prepared for
-    // that.
-    return isCallable(obj);
+    JSObject* target = obj->as<ProxyObject>().target();
+    return target->isConstructor();
 }
 
 JSObject*
 Wrapper::weakmapKeyDelegate(JSObject* proxy) const
 {
     // This may be called during GC.
     return UncheckedUnwrapWithoutExpose(proxy);
 }
diff --git a/js/src/tests/non262/extensions/new-cross-compartment.js b/js/src/tests/non262/extensions/new-cross-compartment.js
--- a/js/src/tests/non262/extensions/new-cross-compartment.js
+++ b/js/src/tests/non262/extensions/new-cross-compartment.js
@@ -13,30 +13,27 @@ print(BUGNUMBER + ": " + summary);
  **************/
 
 var g = newGlobal();
 
 var otherStr = new g.String("foo");
 assertEq(otherStr instanceof g.String, true);
 assertEq(otherStr.valueOf(), "foo");
 
-// THIS IS WRONG.  |new| itself should throw if !IsConstructor(constructor),
-// meaning this global's TypeError should be used.  The problem ultimately is
-// that wrappers conflate callable/constructible, so any old function from
-// another global appears to be both.  Somebody fix bug XXXXXX!
 try
 {
   var constructor = g.parseInt;
   new constructor();
   throw new Error("no error thrown");
 }
 catch (e)
 {
-  assertEq(e instanceof g.TypeError, true,
-           "THIS REALLY SHOULD BE |e instanceof TypeError|");
+  // NOTE: not |g.TypeError|, because |new| itself throws because
+  //       |!IsConstructor(constructor)|.
+  assertEq(e instanceof TypeError, true);
 }
 
 /******************************************************************************/
 
 if (typeof reportCompare === "function")
   reportCompare(true, true);
 
 print("Tests complete");
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -186,30 +186,17 @@ intrinsic_IsCallable(JSContext* cx, unsi
     return true;
 }
 
 static bool
 intrinsic_IsConstructor(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     MOZ_ASSERT(args.length() == 1);
-
-    if (!IsConstructor(args[0])) {
-        args.rval().setBoolean(false);
-        return true;
-    }
-
-    JSObject* obj = &args[0].toObject();
-    if (!IsWrapper(obj)) {
-        args.rval().setBoolean(true);
-        return true;
-    }
-
-    obj = UncheckedUnwrap(obj);
-    args.rval().setBoolean(obj && obj->isConstructor());
+    args.rval().setBoolean(IsConstructor(args[0]));
     return true;
 }
 
 template<typename T>
 static bool
 intrinsic_IsInstanceOfBuiltin(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
diff --git a/toolkit/components/extensions/test/mochitest/file_permission_xhr.html b/toolkit/components/extensions/test/mochitest/file_permission_xhr.html
--- a/toolkit/components/extensions/test/mochitest/file_permission_xhr.html
+++ b/toolkit/components/extensions/test/mochitest/file_permission_xhr.html
@@ -14,39 +14,45 @@
 
 addEventListener("message", function rcv(event) {
   removeEventListener("message", rcv, false);
 
   function assertTrue(condition, description) {
     postMessage({msg: "assertTrue", condition, description}, "*");
   }
 
+  function assertThrows(func, expectedError, msg) {
+    try {
+      func();
+    } catch (e) {
+      assertTrue(expectedError.test(e), msg + ": threw " + e);
+      return;
+    }
+
+    assertTrue(false, "Function did not throw, " +
+                      "expected error should have matched " + expectedError);
+  }
+
   function passListener() {
     assertTrue(true, "Content XHR has no elevated privileges");
     postMessage({"msg": "finish"}, "*");
   }
 
   function failListener() {
     assertTrue(false, "Content XHR has no elevated privileges");
     postMessage({"msg": "finish"}, "*");
   }
 
-  try {
-    new privilegedXHR();
-    assertTrue(false, "Content should not have access to privileged XHR constructor");
-  } catch (e) {
-    assertTrue(/Permission denied to access object/.test(e), "Content should not have access to privileged XHR constructor");
-  }
+  assertThrows(function() { new privilegedXHR(); },
+               /Permission denied to access object/,
+               "Content should not be allowed to construct a privileged XHR constructor");
 
-  try {
-    new privilegedFetch();
-    assertTrue(false, "Content should not have access to privileged fetch() constructor");
-  } catch (e) {
-    assertTrue(/Permission denied to access object/.test(e), "Content should not have access to privileged fetch() constructor");
-  }
+  assertThrows(function() { new privilegedFetch(); },
+               / is not a constructor/,
+               "Content should not be allowed to construct a privileged fetch() constructor");
 
   let req = new XMLHttpRequest();
   req.addEventListener("load", failListener);
   req.addEventListener("error", passListener);
   req.open("GET", "http://example.org/example.txt");
   req.send();
 }, false);
 </script>
