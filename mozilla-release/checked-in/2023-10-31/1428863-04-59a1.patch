# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1514054838 18000
#      Sat Dec 23 13:47:18 2017 -0500
# Node ID ae260591e20e49bbaab6b178fa10838531ada0bf
# Parent  e248159651748d8f2e5635ad321dea3c67e1b266
Bug 1428863 - Add non-qualified Position typenames to various parser structs.  r=arai

diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -3402,17 +3402,17 @@ GeneralParser<ParseHandler, CharT>::func
 
     // Speculatively parse using the directives of the parent parsing context.
     // If a directive is encountered (e.g., "use strict") that changes how the
     // function should have been parsed, we backup and reparse with the new set
     // of directives.
     Directives directives(pc);
     Directives newDirectives = directives;
 
-    typename TokenStream::Position start(keepAtoms);
+    Position start(keepAtoms);
     tokenStream.tell(&start);
 
     // Parse the inner function. The following is a loop as we may attempt to
     // reparse a function due to failed syntax parsing and encountering new
     // "use foo" directives.
     while (true) {
         if (trySyntaxParseInnerFunction(funcNode, fun, toStringStart, inHandling, yieldHandling,
                                         kind, generatorKind, asyncKind, tryAnnexB, directives,
@@ -3469,17 +3469,17 @@ Parser<FullParseHandler, CharT>::trySynt
 
         SyntaxParser* syntaxParser = getSyntaxParser();
         if (!syntaxParser)
             break;
 
         UsedNameTracker::RewindToken token = usedNames.getRewindToken();
 
         // Move the syntax parser to the current position in the stream.
-        typename TokenStream::Position position(keepAtoms);
+        Position position(keepAtoms);
         tokenStream.tell(&position);
         if (!syntaxParser->tokenStream.seek(position, anyChars))
             return false;
 
         // Make a FunctionBox before we enter the syntax parser, because |pn|
         // still expects a FunctionBox to be attached to it during BCE, and
         // the syntax parser cannot attach one to it.
         FunctionBox* funbox = newFunctionBox(funcNode, fun, toStringStart, inheritedDirectives,
@@ -8073,17 +8073,17 @@ GeneralParser<ParseHandler, CharT>::assi
         if (TokenKindIsPossibleIdentifier(nextSameLine))
             maybeAsyncArrow = true;
     }
 
     anyChars.ungetToken();
 
     // Save the tokenizer state in case we find an arrow function and have to
     // rewind.
-    typename TokenStream::Position start(keepAtoms);
+    Position start(keepAtoms);
     tokenStream.tell(&start);
 
     PossibleError possibleErrorInner(*this);
     Node lhs;
     TokenKind tokenAfterLHS;
     bool isArrow;
     if (maybeAsyncArrow) {
         tokenStream.consumeKnownToken(TokenKind::Async, TokenStream::Operand);
diff --git a/js/src/frontend/Parser.h b/js/src/frontend/Parser.h
--- a/js/src/frontend/Parser.h
+++ b/js/src/frontend/Parser.h
@@ -649,25 +649,29 @@ enum TripledotHandling { TripledotAllowe
 
 template <class ParseHandler, typename CharT>
 class Parser;
 
 template <class ParseHandler, typename CharT>
 class GeneralParser
   : public PerHandlerParser<ParseHandler>
 {
+  public:
+    using TokenStream = TokenStreamSpecific<CharT, ParserAnyCharsAccess<GeneralParser>>;
+
   private:
     using Base = PerHandlerParser<ParseHandler>;
     using FinalParser = Parser<ParseHandler, CharT>;
     using Node = typename ParseHandler::Node;
     using typename Base::InvokedPrediction;
     using SyntaxParser = Parser<SyntaxParseHandler, CharT>;
 
   protected:
     using Modifier = TokenStreamShared::Modifier;
+    using Position = typename TokenStream::Position;
 
     using Base::PredictUninvoked;
     using Base::PredictInvoked;
 
     using Base::alloc;
     using Base::awaitIsKeyword;
 #if DEBUG
     using Base::checkOptionsCalled;
@@ -867,17 +871,16 @@ class GeneralParser
         return reinterpret_cast<SyntaxParser*>(internalSyntaxParser_);
     }
 
     void setSyntaxParser(SyntaxParser* syntaxParser) {
         internalSyntaxParser_ = syntaxParser;
     }
 
   public:
-    using TokenStream = TokenStreamSpecific<CharT, ParserAnyCharsAccess<GeneralParser>>;
     TokenStream tokenStream;
 
   public:
     GeneralParser(JSContext* cx, LifoAlloc& alloc, const ReadOnlyCompileOptions& options,
                   const CharT* chars, size_t length, bool foldConstants,
                   UsedNameTracker& usedNames, SyntaxParser* syntaxParser,
                   LazyScript* lazyOuterFunction);
 
@@ -1277,16 +1280,18 @@ class Parser<SyntaxParseHandler, CharT> 
     // so functions here can be hidden when appropriate.
     friend class GeneralParser<SyntaxParseHandler, CharT>;
 
   public:
     using Base::Base;
 
     // Inherited types, listed here to have non-dependent names.
     using typename Base::Modifier;
+    using typename Base::Position;
+    using typename Base::TokenStream;
 
     // Inherited functions, listed here to have non-dependent names.
 
   public:
     using Base::anyChars;
     using Base::clearAbortedSyntaxParse;
     using Base::context;
     using Base::hadAbortedSyntaxParse;
@@ -1382,16 +1387,18 @@ class Parser<FullParseHandler, CharT> fi
     // so functions here can be hidden when appropriate.
     friend class GeneralParser<FullParseHandler, CharT>;
 
   public:
     using Base::Base;
 
     // Inherited types, listed here to have non-dependent names.
     using typename Base::Modifier;
+    using typename Base::Position;
+    using typename Base::TokenStream;
 
     // Inherited functions, listed here to have non-dependent names.
 
   public:
     using Base::anyChars;
     using Base::clearAbortedSyntaxParse;
     using Base::functionFormalParametersAndBody;
     using Base::hadAbortedSyntaxParse;
