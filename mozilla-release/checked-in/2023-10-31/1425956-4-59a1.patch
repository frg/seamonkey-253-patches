# HG changeset patch
# User Byron Campen [:bwc] <docfaraday@gmail.com>
# Date 1513641164 21600
#      Mon Dec 18 17:52:44 2017 -0600
# Node ID 8e682e487ef12970f63169ac97045221ee1be2c9
# Parent  246e691fa8a81bb9d957ddb65d9bee285e5bc1df
Bug 1425956 - Part 3: Remove duplicate track ids on incoming SDP. r=drno

MozReview-Commit-ID: Ldac3oB5ocz

diff --git a/media/webrtc/signaling/src/jsep/JsepSessionImpl.cpp b/media/webrtc/signaling/src/jsep/JsepSessionImpl.cpp
--- a/media/webrtc/signaling/src/jsep/JsepSessionImpl.cpp
+++ b/media/webrtc/signaling/src/jsep/JsepSessionImpl.cpp
@@ -358,16 +358,20 @@ JsepSessionImpl::GetRemoteIds(const Sdp&
 nsresult
 JsepSessionImpl::RemoveDuplicateTrackIds(Sdp* sdp)
 {
   std::set<std::string> trackIds;
 
   for (size_t i = 0; i < sdp->GetMediaSectionCount(); ++i) {
     SdpMediaSection& msection(sdp->GetMediaSection(i));
 
+    if (mSdpHelper.MsectionIsDisabled(msection)) {
+      continue;
+    }
+
     std::vector<std::string> streamIds;
     std::string trackId;
     nsresult rv = mSdpHelper.GetIdsFromMsid(*sdp,
                                             msection,
                                             &streamIds,
                                             &trackId);
 
     if (NS_SUCCEEDED(rv)) {
@@ -384,16 +388,19 @@ JsepSessionImpl::RemoveDuplicateTrackIds
             new SdpMsidAttributeList(mediaAttrs.GetMsid()));
         for (auto& msid : newMsids->mMsids) {
           msid.appdata = trackId;
         }
 
         mediaAttrs.SetAttribute(newMsids.release());
       }
       trackIds.insert(trackId);
+    } else if (rv != NS_ERROR_NOT_AVAILABLE) {
+      // Error has already been set
+      return rv;
     }
   }
 
   return NS_OK;
 }
 
 nsresult
 JsepSessionImpl::CreateOffer(const JsepOfferOptions& options,
@@ -1302,28 +1309,16 @@ JsepSessionImpl::ParseSdp(const std::str
     if (mediaAttrs.HasAttribute(SdpAttribute::kSetupAttribute, true) &&
         mediaAttrs.GetSetup().mRole == SdpSetupAttribute::kHoldconn) {
       JSEP_SET_ERROR("Description has illegal setup attribute "
                      "\"holdconn\" in m-section at level "
                      << i);
       return NS_ERROR_INVALID_ARG;
     }
 
-    std::vector<std::string> streamIds;
-    std::string trackId;
-    nsresult rv = mSdpHelper.GetIdsFromMsid(*parsed,
-                                            parsed->GetMediaSection(i),
-                                            &streamIds,
-                                            &trackId);
-
-    if (NS_FAILED(rv) && (rv != NS_ERROR_NOT_AVAILABLE)) {
-      // Error has already been set
-      return rv;
-    }
-
     static const std::bitset<128> forbidden = GetForbiddenSdpPayloadTypes();
     if (msection.GetMediaType() == SdpMediaSection::kAudio ||
         msection.GetMediaType() == SdpMediaSection::kVideo) {
       // Sanity-check that payload type can work with RTP
       for (const std::string& fmt : msection.GetFormats()) {
         uint16_t payloadType;
         if (!SdpHelper::GetPtAsInt(fmt, &payloadType)) {
           JSEP_SET_ERROR("Payload type \"" << fmt <<
@@ -1339,16 +1334,19 @@ JsepSessionImpl::ParseSdp(const std::str
           JSEP_SET_ERROR("Illegal audio/video payload type \"" << fmt <<
                          "\" at level " << i);
           return NS_ERROR_INVALID_ARG;
         }
       }
     }
   }
 
+  nsresult rv = RemoveDuplicateTrackIds(parsed.get());
+  NS_ENSURE_SUCCESS(rv, rv);
+
   *parsedp = Move(parsed);
   return NS_OK;
 }
 
 nsresult
 JsepSessionImpl::SetRemoteDescriptionOffer(UniquePtr<Sdp> offer)
 {
   MOZ_ASSERT(mState == kJsepStateStable);
