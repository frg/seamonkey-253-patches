# HG changeset patch
# User Martin Stransky <stransky@redhat.com>
# Date 1513611688 -3600
# Node ID 61006be16563924ebe802be812a78de19da081fc
# Parent  981daee1ad40d201b51b0f97e5566a49e57a1026
Bug 1425841 - Set up nsWindow configuration for Wayland rendering, r=jhorak

nsWindow needs configuration change when rendering on Wayland:

- Always draw to mContainer on Wayland as we can't create custom subsurface for
  mShell GdkWindow and listen events on mContainer then.
- GTK_WINDOW_TOPLEVEL can't be positioned on Wayland so create popup windows
  as GTK_WINDOW_POPUP to control window position on Wayland.
- Create ePopupTypeMenu GdkWindow as GDK_WINDOW_TYPE_HINT_UTILITY instead of
  GDK_WINDOW_TYPE_HINT_POPUP_MENU to create it as subsurface (Bug 1423598).
- Don't do pointer grab on Wayland (see Bug 1377084 for reference).

MozReview-Commit-ID: 6InzhTONtrD

diff --git a/widget/gtk/nsWindow.cpp b/widget/gtk/nsWindow.cpp
--- a/widget/gtk/nsWindow.cpp
+++ b/widget/gtk/nsWindow.cpp
@@ -3581,20 +3581,24 @@ nsWindow::Create(nsIWidget* aParent,
         mIsTopLevel = true;
 
         // Popups that are not noautohide are only temporary. The are used
         // for menus and the like and disappear when another window is used.
         // For most popups, use the standard GtkWindowType GTK_WINDOW_POPUP,
         // which will use a Window with the override-redirect attribute
         // (for temporary windows).
         // For long-lived windows, their stacking order is managed by the
-        // window manager, as indicated by GTK_WINDOW_TOPLEVEL ...
-        GtkWindowType type =
-            mWindowType != eWindowType_popup || aInitData->mNoAutoHide ?
-              GTK_WINDOW_TOPLEVEL : GTK_WINDOW_POPUP;
+        // window manager, as indicated by GTK_WINDOW_TOPLEVEL.
+        // For Wayland we have to always use GTK_WINDOW_POPUP to control
+        // popup window position.
+        GtkWindowType type = GTK_WINDOW_TOPLEVEL;
+        if (mWindowType == eWindowType_popup) {
+            type = (mIsX11Display && aInitData->mNoAutoHide) ?
+                GTK_WINDOW_TOPLEVEL : GTK_WINDOW_POPUP;
+        }
         mShell = gtk_window_new(type);
 
         bool useAlphaVisual = (mWindowType == eWindowType_popup &&
                                aInitData->mSupportTranslucency);
 
         // mozilla.widget.use-argb-visuals is a hidden pref defaulting to false
         // to allow experimentation
         if (Preferences::GetBool("mozilla.widget.use-argb-visuals", false))
@@ -3667,17 +3671,21 @@ nsWindow::Create(nsIWidget* aParent,
             GdkWindowTypeHint gtkTypeHint;
             if (aInitData->mIsDragPopup) {
                 gtkTypeHint = GDK_WINDOW_TYPE_HINT_DND;
                 mIsDragPopup = true;
             }
             else {
                 switch (aInitData->mPopupHint) {
                     case ePopupTypeMenu:
-                        gtkTypeHint = GDK_WINDOW_TYPE_HINT_POPUP_MENU;
+                        // Use GDK_WINDOW_TYPE_HINT_UTILITY on Wayland which
+                        // guides Gtk to create the popup as subsurface
+                        // instead of xdg_shell popup (see Bug 1423598).
+                        gtkTypeHint = mIsX11Display ? GDK_WINDOW_TYPE_HINT_POPUP_MENU :
+                                                      GDK_WINDOW_TYPE_HINT_UTILITY;
                         break;
                     case ePopupTypeTooltip:
                         gtkTypeHint = GDK_WINDOW_TYPE_HINT_TOOLTIP;
                         break;
                     default:
                         gtkTypeHint = GDK_WINDOW_TYPE_HINT_UTILITY;
                         break;
                 }
@@ -3718,19 +3726,22 @@ nsWindow::Create(nsIWidget* aParent,
         /* There are two cases here:
          *
          * 1) We're running on Gtk+ without client side decorations.
          *    Content is rendered to mShell window and we listen
          *    to the Gtk+ events on mShell
          * 2) We're running on Gtk+ and client side decorations
          *    are drawn by Gtk+ to mShell. Content is rendered to mContainer
          *    and we listen to the Gtk+ events on mContainer.
+         * 3) We're running on Wayland. All gecko content is rendered
+         *    to mContainer and we listen to the Gtk+ events on mContainer.
          */
         GtkStyleContext* style = gtk_widget_get_style_context(mShell);
         drawToContainer =
+            !mIsX11Display ||
             (mIsCSDAvailable && GetCSDSupportLevel() == CSD_SUPPORT_FLAT ) ||
             gtk_style_context_has_class(style, "csd");
         eventWidget = (drawToContainer) ? container : mShell;
 
         gtk_widget_add_events(eventWidget, kEvents);
         if (drawToContainer)
             gtk_widget_add_events(mShell, GDK_PROPERTY_CHANGE_MASK);
 
@@ -4690,16 +4701,22 @@ nsWindow::GrabPointer(guint32 aTime)
         LOG(("GrabPointer: window not visible\n"));
         mRetryPointerGrab = true;
         return;
     }
 
     if (!mGdkWindow)
         return;
 
+    if (!mIsX11Display) {
+        // Don't to the grab on Wayland as it causes a regression
+        // from Bug 1377084.
+        return;
+    }
+
     gint retval;
     // Note that we need GDK_TOUCH_MASK below to work around a GDK/X11 bug that
     // causes touch events that would normally be received by this client on
     // other windows to be discarded during the grab.
     retval = gdk_pointer_grab(mGdkWindow, TRUE,
                               (GdkEventMask)(GDK_BUTTON_PRESS_MASK |
                                              GDK_BUTTON_RELEASE_MASK |
                                              GDK_ENTER_NOTIFY_MASK |
@@ -4727,16 +4744,23 @@ nsWindow::GrabPointer(guint32 aTime)
 }
 
 void
 nsWindow::ReleaseGrabs(void)
 {
     LOG(("ReleaseGrabs\n"));
 
     mRetryPointerGrab = false;
+
+    if (!mIsX11Display) {
+        // Don't to the ungrab on Wayland as it causes a regression
+        // from Bug 1377084.
+        return;
+    }
+
     gdk_pointer_ungrab(GDK_CURRENT_TIME);
 }
 
 GtkWidget *
 nsWindow::GetToplevelWidget()
 {
     if (mShell) {
         return mShell;
