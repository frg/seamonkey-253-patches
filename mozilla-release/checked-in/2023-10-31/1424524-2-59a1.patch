# HG changeset patch
# User Mats Palmgren <mats@mozilla.com>
# Date 1513264909 -3600
# Node ID 13fa32dda5f2c0a42cd949aa2c94b32f8045fb54
# Parent  0738bac2fb9a687857c4aedcf360a6a949724d24
Bug 1424524 part 2 - Use IsFlexOrGridContainer() in a few more places.  r=dholbert

MozReview-Commit-ID: 3BwSheNfbrx

diff --git a/layout/base/nsCSSFrameConstructor.cpp b/layout/base/nsCSSFrameConstructor.cpp
--- a/layout/base/nsCSSFrameConstructor.cpp
+++ b/layout/base/nsCSSFrameConstructor.cpp
@@ -10051,22 +10051,17 @@ nsCSSFrameConstructor::sPseudoParentData
 };
 
 void
 nsCSSFrameConstructor::CreateNeededAnonFlexOrGridItems(
   nsFrameConstructorState& aState,
   FrameConstructionItemList& aItems,
   nsIFrame* aParentFrame)
 {
-  if (aItems.IsEmpty()) {
-    return;
-  }
-  const LayoutFrameType parentType = aParentFrame->Type();
-  if (parentType != LayoutFrameType::FlexContainer &&
-      parentType != LayoutFrameType::GridContainer) {
+  if (aItems.IsEmpty() || !::IsFlexOrGridContainer(aParentFrame)) {
     return;
   }
 
   const bool isWebkitBox = IsFlexContainerForLegacyBox(aParentFrame);
   FCItemIterator iter(aItems);
   do {
     // Advance iter past children that don't want to be wrapped
     if (iter.SkipItemsThatDontNeedAnonFlexOrGridItem(aState, isWebkitBox)) {
@@ -10740,18 +10735,17 @@ nsCSSFrameConstructor::CreateNeededPseud
  * purely for sanity-checking the children of these container types.
  * NOTE: See also NeedsAnonFlexOrGridItem(), for the non-debug version of this
  * logic (which operates a bit earlier, on FCData instead of frames).
  */
 static bool
 FrameWantsToBeInAnonymousItem(const nsIFrame* aContainerFrame,
                               const nsIFrame* aFrame)
 {
-  MOZ_ASSERT(aContainerFrame->Type() == LayoutFrameType::FlexContainer ||
-             aContainerFrame->Type() == LayoutFrameType::GridContainer);
+  MOZ_ASSERT(::IsFlexOrGridContainer(aContainerFrame));
 
   // Any line-participant frames (e.g. text) definitely want to be wrapped in
   // an anonymous flex/grid item.
   if (aFrame->IsFrameOfType(nsIFrame::eLineParticipant)) {
     return true;
   }
 
   // If the container is a -webkit-box/-webkit-inline-box, then placeholders
@@ -10765,19 +10759,17 @@ FrameWantsToBeInAnonymousItem(const nsIF
 }
 #endif
 
 static void
 VerifyGridFlexContainerChildren(nsIFrame* aParentFrame,
                                 const nsFrameList& aChildren)
 {
 #ifdef DEBUG
-  auto parentType = aParentFrame->Type();
-  if (parentType != LayoutFrameType::FlexContainer &&
-      parentType != LayoutFrameType::GridContainer) {
+  if (!::IsFlexOrGridContainer(aParentFrame)) {
     return;
   }
 
   bool prevChildWasAnonItem = false;
   for (const nsIFrame* child : aChildren) {
     MOZ_ASSERT(!FrameWantsToBeInAnonymousItem(aParentFrame, child),
                "frame wants to be inside an anonymous item, but it isn't");
     if (IsAnonymousFlexOrGridItem(child)) {
