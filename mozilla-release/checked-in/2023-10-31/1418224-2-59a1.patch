# HG changeset patch
# User Ting-Yu Lin <tlin@mozilla.com>
# Date 1511259874 -28800
# Node ID 3936d5282bb1d1f44c3377092fb1fd5728d5c68d
# Parent  d25c240574f58a646d5827b1d883642fec9374e3
Bug 1418224 Part 2 - Extract ShapeInfo::CreateBasicShape(). r=heycam

MozReview-Commit-ID: DZ1O0CzzsyT

diff --git a/layout/generic/nsFloatManager.cpp b/layout/generic/nsFloatManager.cpp
--- a/layout/generic/nsFloatManager.cpp
+++ b/layout/generic/nsFloatManager.cpp
@@ -771,34 +771,18 @@ nsFloatManager::FloatInfo::FloatInfo(nsI
   LogicalRect shapeBoxRect =
     ShapeInfo::ComputeShapeBoxRect(shapeOutside, mFrame, aMarginRect, aWM);
 
   if (shapeOutside.GetType() == StyleShapeSourceType::Box) {
     mShapeInfo = ShapeInfo::CreateShapeBox(mFrame, shapeBoxRect, aWM,
                                            aContainerSize);
   } else if (shapeOutside.GetType() == StyleShapeSourceType::Shape) {
     const UniquePtr<StyleBasicShape>& basicShape = shapeOutside.GetBasicShape();
-
-    switch (basicShape->GetShapeType()) {
-      case StyleBasicShapeType::Polygon:
-        mShapeInfo =
-          ShapeInfo::CreatePolygon(basicShape, shapeBoxRect, aWM,
-                                   aContainerSize);
-        break;
-      case StyleBasicShapeType::Circle:
-      case StyleBasicShapeType::Ellipse:
-        mShapeInfo =
-          ShapeInfo::CreateCircleOrEllipse(basicShape, shapeBoxRect, aWM,
-                                           aContainerSize);
-        break;
-      case StyleBasicShapeType::Inset:
-        mShapeInfo =
-          ShapeInfo::CreateInset(basicShape, shapeBoxRect, aWM, aContainerSize);
-        break;
-    }
+    mShapeInfo = ShapeInfo::CreateBasicShape(basicShape, shapeBoxRect, aWM,
+                                             aContainerSize);
   } else {
     MOZ_ASSERT_UNREACHABLE("Unknown StyleShapeSourceType!");
   }
 
   MOZ_ASSERT(mShapeInfo,
              "All shape-outside values except none should have mShapeInfo!");
 
   // Translate the shape to the same origin as nsFloatManager.
@@ -956,16 +940,36 @@ nsFloatManager::ShapeInfo::CreateShapeBo
   }
 
   return MakeUnique<RoundedBoxShapeInfo>(logicalShapeBoxRect,
                                          ConvertToFloatLogical(physicalRadii,
                                                                aWM));
 }
 
 /* static */ UniquePtr<nsFloatManager::ShapeInfo>
+nsFloatManager::ShapeInfo::CreateBasicShape(
+  const UniquePtr<StyleBasicShape>& aBasicShape,
+  const LogicalRect& aShapeBoxRect,
+  WritingMode aWM,
+  const nsSize& aContainerSize)
+{
+  switch (aBasicShape->GetShapeType()) {
+    case StyleBasicShapeType::Polygon:
+      return CreatePolygon(aBasicShape, aShapeBoxRect, aWM, aContainerSize);
+    case StyleBasicShapeType::Circle:
+    case StyleBasicShapeType::Ellipse:
+      return CreateCircleOrEllipse(aBasicShape, aShapeBoxRect, aWM,
+                                   aContainerSize);
+    case StyleBasicShapeType::Inset:
+      return CreateInset(aBasicShape, aShapeBoxRect, aWM, aContainerSize);
+  }
+  return nullptr;
+}
+
+/* static */ UniquePtr<nsFloatManager::ShapeInfo>
 nsFloatManager::ShapeInfo::CreateInset(
   const UniquePtr<StyleBasicShape>& aBasicShape,
   const LogicalRect& aShapeBoxRect,
   WritingMode aWM,
   const nsSize& aContainerSize)
 {
   // Use physical coordinates to compute inset() because the top, right,
   // bottom and left offsets are physical.
diff --git a/layout/generic/nsFloatManager.h b/layout/generic/nsFloatManager.h
--- a/layout/generic/nsFloatManager.h
+++ b/layout/generic/nsFloatManager.h
@@ -376,16 +376,22 @@ private:
     }
 
     static mozilla::UniquePtr<ShapeInfo> CreateShapeBox(
       nsIFrame* const aFrame,
       const mozilla::LogicalRect& aShapeBoxRect,
       mozilla::WritingMode aWM,
       const nsSize& aContainerSize);
 
+    static mozilla::UniquePtr<ShapeInfo> CreateBasicShape(
+      const mozilla::UniquePtr<mozilla::StyleBasicShape>& aBasicShape,
+      const mozilla::LogicalRect& aShapeBoxRect,
+      mozilla::WritingMode aWM,
+      const nsSize& aContainerSize);
+
     static mozilla::UniquePtr<ShapeInfo> CreateInset(
       const mozilla::UniquePtr<mozilla::StyleBasicShape>& aBasicShape,
       const mozilla::LogicalRect& aShapeBoxRect,
       mozilla::WritingMode aWM,
       const nsSize& aContainerSize);
 
     static mozilla::UniquePtr<ShapeInfo> CreateCircleOrEllipse(
       const mozilla::UniquePtr<mozilla::StyleBasicShape>& aBasicShape,

