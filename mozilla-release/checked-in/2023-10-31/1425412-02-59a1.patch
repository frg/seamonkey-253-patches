# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1513326892 -32400
# Node ID d692dc23699b887aac003eaceb25346e8919fd86
# Parent  63807b154535428dbc1850adb9b6f80740c2aa56
Bug 1425412 - part 2: Create InsertNodeTransaction::Create() and remove EditorBase::CreateTxnForInsertNode() r=m_kato

EditorBase::CreateTxnForInsertNode() just hides what it does.  Let's create
InsertNodeTransaction::Create() and make the caller clearer.

MozReview-Commit-ID: 2J2WV73cdsm

diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -1503,17 +1503,17 @@ EditorBase::InsertNode(nsIContent& aCont
     for (auto& listener : listeners) {
       listener->WillInsertNode(
                   aContentToInsert.AsDOMNode(),
                   GetAsDOMNode(aPointToInsert.GetNextSiblingOfChild()));
     }
   }
 
   RefPtr<InsertNodeTransaction> transaction =
-    CreateTxnForInsertNode(aContentToInsert, aPointToInsert);
+    InsertNodeTransaction::Create(*this, aContentToInsert, aPointToInsert);
   nsresult rv = DoTransaction(transaction);
 
   mRangeUpdater.SelAdjInsertNode(aPointToInsert.GetContainer(),
                                  aPointToInsert.Offset());
 
   {
     AutoActionListenerArray listeners(mActionListeners);
     for (auto& listener : listeners) {
@@ -4634,26 +4634,16 @@ EditorBase::CreateTxnForCreateElement(ns
                                       const EditorRawDOMPoint& aPointToInsert)
 {
   RefPtr<CreateElementTransaction> transaction =
     new CreateElementTransaction(*this, aTag, aPointToInsert);
 
   return transaction.forget();
 }
 
-
-already_AddRefed<InsertNodeTransaction>
-EditorBase::CreateTxnForInsertNode(nsIContent& aNode,
-                                   const EditorRawDOMPoint& aPointToInsert)
-{
-  RefPtr<InsertNodeTransaction> transaction =
-    new InsertNodeTransaction(*this, aNode, aPointToInsert);
-  return transaction.forget();
-}
-
 already_AddRefed<DeleteNodeTransaction>
 EditorBase::CreateTxnForDeleteNode(nsINode* aNode)
 {
   if (NS_WARN_IF(!aNode)) {
     return nullptr;
   }
 
   RefPtr<DeleteNodeTransaction> deleteNodeTransaction =
diff --git a/editor/libeditor/EditorBase.h b/editor/libeditor/EditorBase.h
--- a/editor/libeditor/EditorBase.h
+++ b/editor/libeditor/EditorBase.h
@@ -549,33 +549,16 @@ protected:
    *                        Otherwise, will insert the element before the
    *                        child node referred by this.
    * @return                The created new element node.
    */
   already_AddRefed<Element> CreateNode(nsIAtom* aTag,
                                        const EditorRawDOMPoint& aPointToInsert);
 
   /**
-   * Create a transaction for inserting aContentToInsert before the child
-   * at aPointToInsert.
-   *
-   * @param aContentToInsert    The node to be inserted.
-   * @param aPointToInsert      The insertion point of aContentToInsert.
-   *                            If this refers end of the container, the
-   *                            transaction will append the node to the
-   *                            container.  Otherwise, will insert the node
-   *                            before child node referred by this.
-   * @return                    A InsertNodeTranaction which was initialized
-   *                            with the arguments.
-   */
-  already_AddRefed<InsertNodeTransaction>
-    CreateTxnForInsertNode(nsIContent& aContentToInsert,
-                           const EditorRawDOMPoint& aPointToInsert);
-
-  /**
    * Create a transaction for removing aNode from its parent.
    */
   already_AddRefed<DeleteNodeTransaction>
     CreateTxnForDeleteNode(nsINode* aNode);
 
   /**
    * Create an aggregate transaction for delete selection.  The result may
    * include DeleteNodeTransactions and/or DeleteTextTransactions as its
diff --git a/editor/libeditor/InsertNodeTransaction.cpp b/editor/libeditor/InsertNodeTransaction.cpp
--- a/editor/libeditor/InsertNodeTransaction.cpp
+++ b/editor/libeditor/InsertNodeTransaction.cpp
@@ -17,16 +17,27 @@
 #include "nsMemory.h"                   // for nsMemory
 #include "nsReadableUtils.h"            // for ToNewCString
 #include "nsString.h"                   // for nsString
 
 namespace mozilla {
 
 using namespace dom;
 
+// static
+already_AddRefed<InsertNodeTransaction>
+InsertNodeTransaction::Create(EditorBase& aEditorBase,
+                              nsIContent& aContentToInsert,
+                              const EditorRawDOMPoint& aPointToInsert)
+{
+  RefPtr<InsertNodeTransaction> transaction =
+    new InsertNodeTransaction(aEditorBase, aContentToInsert, aPointToInsert);
+  return transaction.forget();
+}
+
 InsertNodeTransaction::InsertNodeTransaction(
                          EditorBase& aEditorBase,
                          nsIContent& aContentToInsert,
                          const EditorRawDOMPoint& aPointToInsert)
   : mContentToInsert(&aContentToInsert)
   , mPointToInsert(aPointToInsert)
   , mEditorBase(&aEditorBase)
 {
diff --git a/editor/libeditor/InsertNodeTransaction.h b/editor/libeditor/InsertNodeTransaction.h
--- a/editor/libeditor/InsertNodeTransaction.h
+++ b/editor/libeditor/InsertNodeTransaction.h
@@ -17,29 +17,42 @@ namespace mozilla {
 
 class EditorBase;
 
 /**
  * A transaction that inserts a single element
  */
 class InsertNodeTransaction final : public EditTransactionBase
 {
-public:
-  /**
-   * Initialize the transaction.
-   *
-   * @param aEditorBase         The editor.
-   * @param aContentToInsert    The node to insert.
-   * @param aPointToInsert      The node to insert into.  I.e., aContentToInsert
-   *                            will be inserted before the child at offset.
-   */
+protected:
   InsertNodeTransaction(EditorBase& aEditorBase,
                         nsIContent& aContentToInsert,
                         const EditorRawDOMPoint& aPointToInsert);
 
+public:
+  /**
+   * Create a transaction for inserting aContentToInsert before the child
+   * at aPointToInsert.
+   *
+   * @param aEditorBase         The editor which manages the transaction.
+   * @param aContentToInsert    The node to be inserted.
+   * @param aPointToInsert      The insertion point of aContentToInsert.
+   *                            If this refers end of the container, the
+   *                            transaction will append the node to the
+   *                            container.  Otherwise, will insert the node
+   *                            before child node referred by this.
+   * @return                    A InsertNodeTranaction which was initialized
+   *                            with the arguments.
+   */
+  static already_AddRefed<InsertNodeTransaction>
+  Create(EditorBase& aEditorBase,
+         nsIContent& aContentToInsert,
+         const EditorRawDOMPoint& aPointToInsert);
+
+
   NS_DECL_ISUPPORTS_INHERITED
   NS_DECL_CYCLE_COLLECTION_CLASS_INHERITED(InsertNodeTransaction,
                                            EditTransactionBase)
 
   NS_DECL_EDITTRANSACTIONBASE
 
 protected:
   virtual ~InsertNodeTransaction();
