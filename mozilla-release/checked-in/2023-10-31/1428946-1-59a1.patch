# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1515678526 18000
# Node ID 779a3a7cd1fd41339e339f2120a68b98622dd914
# Parent  3d7ea120111efc8d654c904054b8cd6f8ab06374
Bug 1428946 - Part 1. Make comparing and mapping DataSourceSurfaceWrapper objects work consistently. r=bas

diff --git a/dom/base/nsDOMWindowUtils.cpp b/dom/base/nsDOMWindowUtils.cpp
--- a/dom/base/nsDOMWindowUtils.cpp
+++ b/dom/base/nsDOMWindowUtils.cpp
@@ -1518,16 +1518,21 @@ nsDOMWindowUtils::CompareCanvases(nsISup
 
   if (!canvas1 || !canvas2) {
     return NS_ERROR_FAILURE;
   }
 
   RefPtr<DataSourceSurface> img1 = CanvasToDataSourceSurface(canvas1);
   RefPtr<DataSourceSurface> img2 = CanvasToDataSourceSurface(canvas2);
 
+  if (img1->Equals(img2)) {
+    // They point to the same underlying content.
+    return NS_OK;
+  }
+
   DataSourceSurface::ScopedMap map1(img1, DataSourceSurface::READ);
   DataSourceSurface::ScopedMap map2(img2, DataSourceSurface::READ);
 
   if (img1 == nullptr || img2 == nullptr ||
       !map1.IsMapped() || !map2.IsMapped() ||
       img1->GetSize() != img2->GetSize() ||
       map1.GetStride() != map2.GetStride()) {
     return NS_ERROR_FAILURE;
diff --git a/gfx/2d/2D.h b/gfx/2d/2D.h
--- a/gfx/2d/2D.h
+++ b/gfx/2d/2D.h
@@ -374,16 +374,27 @@ public:
   /** This returns false if some event has made this source surface invalid for
    * usage with current DrawTargets. For example in the case of Direct2D this
    * could return false if we have switched devices since this surface was
    * created.
    */
   virtual bool IsValid() const { return true; }
 
   /**
+   * This returns true if it is the same underlying surface data, even if
+   * the objects are different (e.g. indirection due to
+   * DataSourceSurfaceWrapper).
+   */
+  virtual bool Equals(SourceSurface* aOther, bool aSymmetric = true)
+  {
+    return this == aOther ||
+           (aSymmetric && aOther && aOther->Equals(this, false));
+  }
+
+  /**
    * This function will return true if the surface type matches that of a
    * DataSourceSurface and if GetDataSurface will return the same object.
    */
   bool IsDataSourceSurface() const {
     SurfaceType type = GetType();
     return type == SurfaceType::DATA ||
            type == SurfaceType::DATA_SHARED;
   }
diff --git a/gfx/2d/DataSourceSurfaceWrapper.h b/gfx/2d/DataSourceSurfaceWrapper.h
--- a/gfx/2d/DataSourceSurfaceWrapper.h
+++ b/gfx/2d/DataSourceSurfaceWrapper.h
@@ -9,32 +9,45 @@
 
 #include "2D.h"
 
 namespace mozilla {
 namespace gfx {
 
 // Wraps a DataSourceSurface and forwards all methods except for GetType(),
 // from which it always returns SurfaceType::DATA.
-class DataSourceSurfaceWrapper : public DataSourceSurface
+class DataSourceSurfaceWrapper final : public DataSourceSurface
 {
 public:
   MOZ_DECLARE_REFCOUNTED_VIRTUAL_TYPENAME(DataSourceSurfaceWrapper, override)
   explicit DataSourceSurfaceWrapper(DataSourceSurface *aSurface)
    : mSurface(aSurface)
   {}
 
+  bool Equals(SourceSurface* aOther, bool aSymmetric = true) override
+  {
+    return DataSourceSurface::Equals(aOther, aSymmetric) ||
+           mSurface->Equals(aOther, aSymmetric);
+  }
+
   virtual SurfaceType GetType() const override { return SurfaceType::DATA; }
 
   virtual uint8_t *GetData() override { return mSurface->GetData(); }
   virtual int32_t Stride() override { return mSurface->Stride(); }
   virtual IntSize GetSize() const override { return mSurface->GetSize(); }
   virtual SurfaceFormat GetFormat() const override { return mSurface->GetFormat(); }
   virtual bool IsValid() const override { return mSurface->IsValid(); }
 
+  bool Map(MapType aType, MappedSurface *aMappedSurface) override
+  {
+    return mSurface->Map(aType, aMappedSurface);
+  }
+
+  void Unmap() override { mSurface->Unmap(); }
+
 private:
   RefPtr<DataSourceSurface> mSurface;
 };
 
 } // namespace gfx
 } // namespace mozilla
 
 #endif /* MOZILLA_GFX_DATASOURCESURFACEWRAPPER_H_ */
