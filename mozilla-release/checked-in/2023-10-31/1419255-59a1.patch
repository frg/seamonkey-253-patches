# HG changeset patch
# User Nicolas Silva <nsilva@mozilla.com>
# Date 1512471380 -3600
# Node ID 49b95b5bf51538a7dbe9f2dc9afdb760537a028b
# Parent  387982a58239a97e7aa97e810cc63a940f3c5b1e
Bug 1419255 - Add a Proxy KnowsCompositor implementation that can be used off the main thread. r=sotaro

diff --git a/dom/media/MediaDecoder.cpp b/dom/media/MediaDecoder.cpp
--- a/dom/media/MediaDecoder.cpp
+++ b/dom/media/MediaDecoder.cpp
@@ -913,17 +913,17 @@ already_AddRefed<KnowsCompositor>
 MediaDecoder::GetCompositor()
 {
   MediaDecoderOwner* owner = GetOwner();
   nsIDocument* ownerDoc = owner ? owner->GetDocument() : nullptr;
   RefPtr<LayerManager> layerManager =
     ownerDoc ? nsContentUtils::LayerManagerForDocument(ownerDoc) : nullptr;
   RefPtr<KnowsCompositor> knows =
     layerManager ? layerManager->AsKnowsCompositor() : nullptr;
-  return knows.forget();
+  return knows ? knows->GetForMedia().forget() : nullptr;
 }
 
 void
 MediaDecoder::NotifyCompositor()
 {
   RefPtr<KnowsCompositor> knowsCompositor = GetCompositor();
   if (knowsCompositor) {
     nsCOMPtr<nsIRunnable> r =
diff --git a/dom/media/platforms/PlatformDecoderModule.h b/dom/media/platforms/PlatformDecoderModule.h
--- a/dom/media/platforms/PlatformDecoderModule.h
+++ b/dom/media/platforms/PlatformDecoderModule.h
@@ -119,17 +119,20 @@ private:
   }
   void Set(MediaResult* aError) { mError = aError; }
   void Set(GMPCrashHelper* aCrashHelper) { mCrashHelper = aCrashHelper; }
   void Set(UseNullDecoder aUseNullDecoder) { mUseNullDecoder = aUseNullDecoder; }
   void Set(OptionSet aOptions) { mOptions = aOptions; }
   void Set(VideoFrameRate aRate) { mRate = aRate; }
   void Set(layers::KnowsCompositor* aKnowsCompositor)
   {
-    mKnowsCompositor = aKnowsCompositor;
+    if (aKnowsCompositor) {
+      mKnowsCompositor = aKnowsCompositor;
+      MOZ_ASSERT(aKnowsCompositor->IsThreadSafe());
+    }
   }
   void Set(TrackInfo::TrackType aType)
   {
     mType = aType;
   }
   void Set(MediaEventProducer<TrackInfo::TrackType>* aOnWaitingForKey)
   {
     mOnWaitingForKeyEvent = aOnWaitingForKey;
diff --git a/gfx/layers/ipc/KnowsCompositor.h b/gfx/layers/ipc/KnowsCompositor.h
--- a/gfx/layers/ipc/KnowsCompositor.h
+++ b/gfx/layers/ipc/KnowsCompositor.h
@@ -12,16 +12,17 @@
 #include "nsExpirationTracker.h"
 
 namespace mozilla {
 namespace layers {
 
 class SyncObjectClient;
 class TextureForwarder;
 class LayersIPCActor;
+class ImageBridgeChild;
 
 /**
  * See ActiveResourceTracker below.
  */
 class ActiveResource
 {
 public:
  virtual void NotifyInactive() = 0;
@@ -59,16 +60,22 @@ public:
 
   KnowsCompositor();
   ~KnowsCompositor();
 
   void IdentifyTextureHost(const TextureFactoryIdentifier& aIdentifier);
 
   SyncObjectClient* GetSyncObject() { return mSyncObject; }
 
+  /// And by "thread-safe" here we merely mean "okay to hold strong references to
+  /// from multiple threads". Not all methods actually are thread-safe.
+  virtual bool IsThreadSafe() const { return true; }
+
+  virtual RefPtr<KnowsCompositor> GetForMedia() { return RefPtr<KnowsCompositor>(this); }
+
   int32_t GetMaxTextureSize() const
   {
     return mTextureFactoryIdentifier.mMaxTextureSize;
   }
 
   /**
    * Returns the type of backend that is used off the main thread.
    * We only don't allow changing the backend type at runtime so this value can
@@ -127,28 +134,62 @@ public:
   virtual void SyncWithCompositor()
   {
     MOZ_ASSERT_UNREACHABLE("Unimplemented");
   }
 
   /**
    * Helpers for finding other related interface. These are infallible.
    */
-  virtual TextureForwarder* GetTextureForwarder() = 0;
+   virtual TextureForwarder* GetTextureForwarder() = 0;
   virtual LayersIPCActor* GetLayersIPCActor() = 0;
   virtual ActiveResourceTracker* GetActiveResourceTracker()
   {
     MOZ_ASSERT_UNREACHABLE("Unimplemented");
     return nullptr;
   }
 
 protected:
   TextureFactoryIdentifier mTextureFactoryIdentifier;
   RefPtr<SyncObjectClient> mSyncObject;
 
   const int32_t mSerial;
   static mozilla::Atomic<int32_t> sSerialCounter;
 };
 
+
+/// Some implementations of KnowsCompositor can be used off their IPDL thread
+/// like the ImageBridgeChild, and others just can't. Instead of passing them
+/// we create a proxy KnowsCompositor that has information about compositor
+/// backend but proxies allocations to the ImageBridge.
+/// This is kind of specific to the needs of media which wants to allocate
+/// textures, usually through the Image Bridge accessed by KnowsCompositor but
+/// also wants access to the compositor backend information that ImageBridge
+/// doesn't know about.
+///
+/// This is really a band aid to what turned into a class hierarchy horror show.
+/// Hopefully we can come back and simplify this some way.
+class KnowsCompositorMediaProxy: public KnowsCompositor
+{
+public:
+  NS_INLINE_DECL_THREADSAFE_REFCOUNTING(KnowsCompositorMediaProxy, override);
+
+  explicit KnowsCompositorMediaProxy(const TextureFactoryIdentifier& aIdentifier);
+
+  virtual TextureForwarder* GetTextureForwarder() override;
+
+
+  virtual LayersIPCActor* GetLayersIPCActor() override;
+
+  virtual ActiveResourceTracker* GetActiveResourceTracker() override;
+
+  virtual void SyncWithCompositor() override;
+
+protected:
+  virtual ~KnowsCompositorMediaProxy();
+
+  RefPtr<ImageBridgeChild> mThreadSafeAllocator;
+};
+
 } // namespace layers
 } // namespace mozilla
 
 #endif
diff --git a/gfx/layers/ipc/ShadowLayers.cpp b/gfx/layers/ipc/ShadowLayers.cpp
--- a/gfx/layers/ipc/ShadowLayers.cpp
+++ b/gfx/layers/ipc/ShadowLayers.cpp
@@ -180,16 +180,58 @@ KnowsCompositor::IdentifyTextureHost(con
 
 KnowsCompositor::KnowsCompositor()
   : mSerial(++sSerialCounter)
 {}
 
 KnowsCompositor::~KnowsCompositor()
 {}
 
+KnowsCompositorMediaProxy::KnowsCompositorMediaProxy(const TextureFactoryIdentifier& aIdentifier)
+{
+  mTextureFactoryIdentifier = aIdentifier;
+  // overwrite mSerial's value set by the parent class because we use the same serial
+  // as the KnowsCompositor we are proxying.
+  mThreadSafeAllocator = ImageBridgeChild::GetSingleton();
+  mSyncObject = mThreadSafeAllocator->GetSyncObject();
+}
+
+KnowsCompositorMediaProxy::~KnowsCompositorMediaProxy()
+{}
+
+TextureForwarder*
+KnowsCompositorMediaProxy::GetTextureForwarder()
+{
+  return mThreadSafeAllocator->GetTextureForwarder();
+}
+
+LayersIPCActor*
+KnowsCompositorMediaProxy::GetLayersIPCActor()
+{
+  return mThreadSafeAllocator->GetLayersIPCActor();
+}
+
+ActiveResourceTracker*
+KnowsCompositorMediaProxy::GetActiveResourceTracker()
+{
+  return mThreadSafeAllocator->GetActiveResourceTracker();
+}
+
+void
+KnowsCompositorMediaProxy::SyncWithCompositor()
+{
+  mThreadSafeAllocator->SyncWithCompositor();
+}
+
+RefPtr<KnowsCompositor>
+ShadowLayerForwarder::GetForMedia()
+{
+  return MakeAndAddRef<KnowsCompositorMediaProxy>(GetTextureFactoryIdentifier());
+}
+
 ShadowLayerForwarder::ShadowLayerForwarder(ClientLayerManager* aClientLayerManager)
  : mClientLayerManager(aClientLayerManager)
  , mMessageLoop(MessageLoop::current())
  , mDiagnosticTypes(DiagnosticTypes::NO_DIAGNOSTIC)
  , mIsFirstPaint(false)
  , mWindowOverlayChanged(false)
  , mNextLayerHandle(1)
 {
diff --git a/gfx/layers/ipc/ShadowLayers.h b/gfx/layers/ipc/ShadowLayers.h
--- a/gfx/layers/ipc/ShadowLayers.h
+++ b/gfx/layers/ipc/ShadowLayers.h
@@ -375,16 +375,20 @@ public:
   LayersIPCActor* GetLayersIPCActor() override { return this; }
 
   ActiveResourceTracker* GetActiveResourceTracker() override { return mActiveResourceTracker.get(); }
 
   CompositorBridgeChild* GetCompositorBridgeChild();
 
   nsIEventTarget* GetEventTarget() { return mEventTarget; };
 
+  virtual bool IsThreadSafe() const override { return false; }
+
+  virtual RefPtr<KnowsCompositor> GetForMedia() override;
+
 protected:
   virtual ~ShadowLayerForwarder();
 
   explicit ShadowLayerForwarder(ClientLayerManager* aClientLayerManager);
 
 #ifdef DEBUG
   void CheckSurfaceDescriptor(const SurfaceDescriptor* aDescriptor) const;
 #else
diff --git a/gfx/layers/wr/WebRenderBridgeChild.cpp b/gfx/layers/wr/WebRenderBridgeChild.cpp
--- a/gfx/layers/wr/WebRenderBridgeChild.cpp
+++ b/gfx/layers/wr/WebRenderBridgeChild.cpp
@@ -624,10 +624,18 @@ WebRenderBridgeChild::SetWebRenderLayerM
 }
 
 ipc::IShmemAllocator*
 WebRenderBridgeChild::GetShmemAllocator()
 {
   return static_cast<CompositorBridgeChild*>(Manager());
 }
 
+
+RefPtr<KnowsCompositor>
+WebRenderBridgeChild::GetForMedia()
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  return MakeAndAddRef<KnowsCompositorMediaProxy>(GetTextureFactoryIdentifier());
+}
+
 } // namespace layers
 } // namespace mozilla
diff --git a/gfx/layers/wr/WebRenderBridgeChild.h b/gfx/layers/wr/WebRenderBridgeChild.h
--- a/gfx/layers/wr/WebRenderBridgeChild.h
+++ b/gfx/layers/wr/WebRenderBridgeChild.h
@@ -145,16 +145,20 @@ public:
 
   void BeginClearCachedResources();
   void EndClearCachedResources();
 
   void SetWebRenderLayerManager(WebRenderLayerManager* aManager);
 
   ipc::IShmemAllocator* GetShmemAllocator();
 
+  virtual bool IsThreadSafe() const override { return false; }
+
+  virtual RefPtr<KnowsCompositor> GetForMedia() override;
+
 private:
   friend class CompositorBridgeChild;
 
   ~WebRenderBridgeChild();
 
   wr::ExternalImageId GetNextExternalImageId();
 
   // CompositableForwarder
