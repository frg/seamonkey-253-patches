# HG changeset patch
# User Valentin Gosu <valentin.gosu@gmail.com>
# Date 1508353822 -7200
#      Wed Oct 18 21:10:22 2017 +0200
# Node ID bf32a14dfacff945c3dbc516525c497663f9e467
# Parent  9df799a34526451e0b7533ebd34981ee89231f91
Bug 1408552 - Make sure we only instantiate CacheStorageService in the main process r=mayhemer

* A memory leak occurs when this happens in the content process
* I added an assertion that we only create it in the parent process

MozReview-Commit-ID: 1UTyusRg0qx

diff --git a/dom/base/test/test_bug482935.html b/dom/base/test/test_bug482935.html
--- a/dom/base/test/test_bug482935.html
+++ b/dom/base/test/test_bug482935.html
@@ -6,19 +6,21 @@
   <link rel="stylesheet" type="text/css" href="	/tests/SimpleTest/test.css" />
 </head>
 <body onload="onWindowLoad()">
 <script class="testbody" type="text/javascript">
 
 var url = "bug482935.sjs";
 
 function clearCache() {
-    SpecialPowers.Cc["@mozilla.org/netwerk/cache-storage-service;1"].
-               getService(SpecialPowers.Ci.nsICacheStorageService).
-               clear();
+    if (SpecialPowers.isMainProcess()) {
+      SpecialPowers.Cc["@mozilla.org/netwerk/cache-storage-service;1"].
+                    getService(SpecialPowers.Ci.nsICacheStorageService).
+                    clear();
+    }
 }
 
 // Tests that the response is cached if the request is cancelled
 // after it has reached state 4
 function testCancelInPhase4() {
 
   clearCache();
 
diff --git a/netwerk/cache2/CacheStorageService.cpp b/netwerk/cache2/CacheStorageService.cpp
--- a/netwerk/cache2/CacheStorageService.cpp
+++ b/netwerk/cache2/CacheStorageService.cpp
@@ -24,16 +24,17 @@
 #include "nsIFile.h"
 #include "nsIURI.h"
 #include "nsCOMPtr.h"
 #include "nsAutoPtr.h"
 #include "nsNetCID.h"
 #include "nsNetUtil.h"
 #include "nsServiceManagerUtils.h"
 #include "nsWeakReference.h"
+#include "nsXULAppAPI.h"
 #include "mozilla/TimeStamp.h"
 #include "mozilla/DebugOnly.h"
 #include "mozilla/Services.h"
 #include "mozilla/IntegerPrintfMacros.h"
 
 namespace mozilla {
 namespace net {
 
@@ -116,16 +117,17 @@ CacheStorageService::CacheStorageService
 : mLock("CacheStorageService.mLock")
 , mForcedValidEntriesLock("CacheStorageService.mForcedValidEntriesLock")
 , mShutdown(false)
 , mDiskPool(MemoryPool::DISK)
 , mMemoryPool(MemoryPool::MEMORY)
 {
   CacheFileIOManager::Init();
 
+  MOZ_ASSERT(XRE_IsParentProcess());
   MOZ_ASSERT(!sSelf);
 
   sSelf = this;
   sGlobalEntryTables = new GlobalEntryTables();
 
   RegisterStrongMemoryReporter(this);
 }
 
diff --git a/netwerk/test/unit/test_alt-data_simple.js b/netwerk/test/unit/test_alt-data_simple.js
--- a/netwerk/test/unit/test_alt-data_simple.js
+++ b/netwerk/test/unit/test_alt-data_simple.js
@@ -111,29 +111,39 @@ function readServerContent(request, buff
 
     executeSoon(flushAndOpenAltChannel);
   });
 }
 
 // needs to be rooted
 var cacheFlushObserver = cacheFlushObserver = { observe: function() {
   cacheFlushObserver = null;
-
-  var chan = make_channel(URL);
-  var cc = chan.QueryInterface(Ci.nsICacheInfoChannel);
-  cc.preferAlternativeDataType(altContentType);
-
-  chan.asyncOpen2(new ChannelListener(readAltContent, null));
+  openAltChannel();
 }};
 
 function flushAndOpenAltChannel()
 {
   // We need to do a GC pass to ensure the cache entry has been freed.
   gc();
-  Services.cache2.QueryInterface(Ci.nsICacheTesting).flush(cacheFlushObserver);
+  if (!inChildProcess()) {
+    Services.cache2.QueryInterface(Ci.nsICacheTesting).flush(cacheFlushObserver);
+  } else {
+    do_send_remote_message('flush');
+    do_await_remote_message('flushed').then(() => {
+      openAltChannel();
+    });
+  }
+}
+
+function openAltChannel() {
+  var chan = make_channel(URL);
+  var cc = chan.QueryInterface(Ci.nsICacheInfoChannel);
+  cc.preferAlternativeDataType(altContentType);
+
+  chan.asyncOpen2(new ChannelListener(readAltContent, null));
 }
 
 function readAltContent(request, buffer)
 {
   var cc = request.QueryInterface(Ci.nsICacheInfoChannel);
 
   Assert.equal(servedNotModified, true);
   Assert.equal(cc.alternativeDataType, altContentType);
diff --git a/netwerk/test/unit/test_cache-entry-id.js b/netwerk/test/unit/test_cache-entry-id.js
--- a/netwerk/test/unit/test_cache-entry-id.js
+++ b/netwerk/test/unit/test_cache-entry-id.js
@@ -12,16 +12,21 @@ XPCOMUtils.defineLazyGetter(this, "URL",
 
 var httpServer = null;
 
 const responseContent = "response body";
 const responseContent2 = "response body 2";
 const altContent = "!@#$%^&*()";
 const altContentType = "text/binary";
 
+function isParentProcess() {
+    let appInfo = Cc["@mozilla.org/xre/app-info;1"];
+    return (!appInfo || appInfo.getService(Ci.nsIXULRuntime).processType == Ci.nsIXULRuntime.PROCESS_TYPE_DEFAULT);
+}
+
 var handlers = [
   (m, r) => {r.bodyOutputStream.write(responseContent, responseContent.length)},
   (m, r) => {r.setStatusLine(m.httpVersion, 304, "Not Modified")},
   (m, r) => {r.setStatusLine(m.httpVersion, 304, "Not Modified")},
   (m, r) => {r.setStatusLine(m.httpVersion, 304, "Not Modified")},
   (m, r) => {r.setStatusLine(m.httpVersion, 304, "Not Modified")},
   (m, r) => {r.bodyOutputStream.write(responseContent2, responseContent2.length)},
   (m, r) => {r.setStatusLine(m.httpVersion, 304, "Not Modified")},
@@ -77,18 +82,23 @@ function writeAltData(request)
 {
   var cc = request.QueryInterface(Ci.nsICacheInfoChannel);
   var os = cc.openAlternativeOutputStream(altContentType);
   os.write(altContent, altContent.length);
   os.close();
   gc(); // We need to do a GC pass to ensure the cache entry has been freed.
 
   return new Promise(resolve => {
-    Services.cache2.QueryInterface(Ci.nsICacheTesting)
-            .flush(resolve);
+    if (isParentProcess()) {
+      Services.cache2.QueryInterface(Ci.nsICacheTesting)
+              .flush(resolve);
+    } else {
+      do_send_remote_message('flush');
+      do_await_remote_message('flushed').then(resolve);
+    }
   });
 }
 
 function run_test()
 {
   do_get_profile();
   httpServer = new HttpServer();
   httpServer.registerPathHandler("/content", contentHandler);
diff --git a/netwerk/test/unit/test_synthesized_response.js b/netwerk/test/unit/test_synthesized_response.js
--- a/netwerk/test/unit/test_synthesized_response.js
+++ b/netwerk/test/unit/test_synthesized_response.js
@@ -12,18 +12,27 @@ XPCOMUtils.defineLazyGetter(this, "URL",
 var httpServer = null;
 
 function make_uri(url) {
   var ios = Cc["@mozilla.org/network/io-service;1"].
             getService(Ci.nsIIOService);
   return ios.newURI(url);
 }
 
-// ensure the cache service is prepped when running the test
-Cc["@mozilla.org/netwerk/cache-storage-service;1"].getService(Ci.nsICacheStorageService);
+function isParentProcess() {
+    let appInfo = Cc["@mozilla.org/xre/app-info;1"];
+    return (!appInfo || appInfo.getService(Ci.nsIXULRuntime).processType == Ci.nsIXULRuntime.PROCESS_TYPE_DEFAULT);
+}
+
+if (isParentProcess()) {
+  // ensure the cache service is prepped when running the test
+  // We only do this in the main process, as the cache storage service leaks
+  // when instantiated in the content process.
+  Cc["@mozilla.org/netwerk/cache-storage-service;1"].getService(Ci.nsICacheStorageService);
+}
 
 var gotOnProgress;
 var gotOnStatus;
 
 function make_channel(url, body, cb) {
   gotOnProgress = false;
   gotOnStatus = false;
   var chan = NetUtil.newChannel({uri: url, loadUsingSystemPrincipal: true})
diff --git a/netwerk/test/unit_ipc/test_alt-data_simple_wrap.js b/netwerk/test/unit_ipc/test_alt-data_simple_wrap.js
--- a/netwerk/test/unit_ipc/test_alt-data_simple_wrap.js
+++ b/netwerk/test/unit_ipc/test_alt-data_simple_wrap.js
@@ -1,3 +1,15 @@
+Cu.import("resource://gre/modules/Services.jsm");
+
+// needs to be rooted
+var cacheFlushObserver = { observe: function() {
+  cacheFlushObserver = null;
+  do_send_remote_message('flushed');
+}};
+
 function run_test() {
+  do_get_profile();
+  do_await_remote_message('flush').then(() => {
+    Services.cache2.QueryInterface(Ci.nsICacheTesting).flush(cacheFlushObserver);
+  });
   run_test_in_child("../unit/test_alt-data_simple.js");
 }
diff --git a/netwerk/test/unit_ipc/test_cache-entry-id_wrap.js b/netwerk/test/unit_ipc/test_cache-entry-id_wrap.js
--- a/netwerk/test/unit_ipc/test_cache-entry-id_wrap.js
+++ b/netwerk/test/unit_ipc/test_cache-entry-id_wrap.js
@@ -1,3 +1,16 @@
+Cu.import("resource://gre/modules/Services.jsm");
+
 function run_test() {
+  do_get_profile();
   run_test_in_child("../unit/test_cache-entry-id.js");
+
+  do_await_remote_message('flush').then(() => {
+    let p = new Promise(resolve => {
+      Services.cache2.QueryInterface(Ci.nsICacheTesting)
+              .flush(resolve);
+    });
+    p.then(() => {
+      do_send_remote_message('flushed');
+    });
+  });
 }
diff --git a/netwerk/test/unit_ipc/test_synthesized_response_wrap.js b/netwerk/test/unit_ipc/test_synthesized_response_wrap.js
--- a/netwerk/test/unit_ipc/test_synthesized_response_wrap.js
+++ b/netwerk/test/unit_ipc/test_synthesized_response_wrap.js
@@ -1,3 +1,6 @@
 function run_test() {
+  // ensure the cache service is prepped when running the test
+  Cc["@mozilla.org/netwerk/cache-storage-service;1"].getService(Ci.nsICacheStorageService);
+
   run_test_in_child("../unit/test_synthesized_response.js");
 }
