# HG changeset patch
# User cku <cku@mozilla.com>
# Date 1509544280 -28800
# Node ID 810b268d3044b1f283f0fe531ceea930602a6ec2
# Parent  64a0a54b5a1d215588a62b30efd1f1d379da1628
Bug 1399787 - Part 16. Hide function table in PDFiumEngineShim.cpp. r=jwatt

This is to make the header file more concise.

MozReview-Commit-ID: 7RFkppBdvbU

diff --git a/widget/windows/PDFiumEngineShim.cpp b/widget/windows/PDFiumEngineShim.cpp
--- a/widget/windows/PDFiumEngineShim.cpp
+++ b/widget/windows/PDFiumEngineShim.cpp
@@ -1,21 +1,68 @@
 /* -*- Mode: C++; tab-width: 20; indent-tabs-mode: nil; c-basic-offset: 4 -*-
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "PDFiumEngineShim.h"
 #include "private/pprio.h"
 
+typedef void (STDCALL *FPDF_InitLibrary_Pfn)();
+typedef void (STDCALL *FPDF_DestroyLibrary_Pfn)();
+
+typedef FPDF_DOCUMENT (STDCALL *FPDF_LoadDocument_Pfn)(FPDF_STRING file_path,
+                                                      FPDF_BYTESTRING password);
+typedef FPDF_DOCUMENT (STDCALL *FPDF_LoadCustomDocument_Pfn)(FPDF_FILEACCESS* pFileAccess,
+                                                             FPDF_BYTESTRING password);
+typedef void(STDCALL *FPDF_CloseDocument_Pfn)(FPDF_DOCUMENT aDocument);
+
+typedef int (STDCALL *FPDF_GetPageCount_Pfn)(FPDF_DOCUMENT aDocument);
+
+typedef FPDF_PAGE (STDCALL *FPDF_LoadPage_Pfn)(FPDF_DOCUMENT aDocument,
+                                               int aPageIndex);
+typedef void (STDCALL *FPDF_ClosePage_Pfn)(FPDF_PAGE aPage);
+typedef void (STDCALL *FPDF_RenderPage_Pfn)(HDC aDC,
+                                            FPDF_PAGE aPage,
+                                            int aStartX,
+                                            int aStartY,
+                                            int aSizeX,
+                                            int aSizeY,
+                                            int aRotate,
+                                            int aFlags);
+
 namespace mozilla {
 namespace widget {
 
 static PDFiumEngineShim* sPDFiumEngineShim;
 
+struct PDFFunctionPointerTable
+{
+  PDFFunctionPointerTable()
+    : mFPDF_InitLibrary(nullptr)
+    , mFPDF_DestroyLibrary(nullptr)
+    , mFPDF_CloseDocument(nullptr)
+    , mFPDF_GetPageCount(nullptr)
+    , mFPDF_LoadPage(nullptr)
+    , mFPDF_ClosePage(nullptr)
+    , mFPDF_RenderPage(nullptr)
+  {
+  }
+
+  FPDF_InitLibrary_Pfn        mFPDF_InitLibrary;
+  FPDF_DestroyLibrary_Pfn     mFPDF_DestroyLibrary;
+  FPDF_LoadDocument_Pfn       mFPDF_LoadDocument;
+  FPDF_LoadCustomDocument_Pfn mFPDF_LoadCustomDocument;
+  FPDF_CloseDocument_Pfn      mFPDF_CloseDocument;
+  FPDF_GetPageCount_Pfn       mFPDF_GetPageCount;
+  FPDF_LoadPage_Pfn           mFPDF_LoadPage;
+  FPDF_ClosePage_Pfn          mFPDF_ClosePage;
+  FPDF_RenderPage_Pfn         mFPDF_RenderPage;
+};
+
 /* static */
 already_AddRefed<PDFiumEngineShim>
 PDFiumEngineShim::GetInstanceOrNull()
 {
   RefPtr<PDFiumEngineShim> inst = sPDFiumEngineShim;
   if (!inst) {
     inst = new PDFiumEngineShim();
     if (!inst->Init(NS_LITERAL_STRING("pdfium.dll"))) {
@@ -35,32 +82,26 @@ PDFiumEngineShim::GetInstanceOrNull(cons
   if (!shim->Init(aLibrary)) {
     return nullptr;
   }
 
   return shim.forget();
 }
 
 PDFiumEngineShim::PDFiumEngineShim()
-  : mInitialized(false)
-  , mFPDF_InitLibrary(nullptr)
-  , mFPDF_DestroyLibrary(nullptr)
-  , mFPDF_CloseDocument(nullptr)
-  , mFPDF_GetPageCount(nullptr)
-  , mFPDF_LoadPage(nullptr)
-  , mFPDF_ClosePage(nullptr)
-  , mFPDF_RenderPage(nullptr)
+  : mTable(MakeUnique<PDFFunctionPointerTable>())
+  , mInitialized(false)
   , mPRLibrary(nullptr)
 {
 }
 
 PDFiumEngineShim::~PDFiumEngineShim()
 {
   if (mInitialized) {
-    mFPDF_DestroyLibrary();
+    mTable->mFPDF_DestroyLibrary();
   }
 
   sPDFiumEngineShim = nullptr;
 
   if (mPRLibrary) {
     PR_UnloadLibrary(mPRLibrary);
   }
 }
@@ -73,63 +114,69 @@ PDFiumEngineShim::Init(const nsString& a
   }
 
   PRLibSpec libSpec;
   libSpec.type = PR_LibSpec_PathnameU;
   libSpec.value.pathname_u = aLibrary.get();
   mPRLibrary = PR_LoadLibraryWithFlags(libSpec, 0);
   NS_ENSURE_TRUE(mPRLibrary, false);
 
-  mFPDF_InitLibrary = (FPDF_InitLibrary_Pfn)PR_FindFunctionSymbol(
-    mPRLibrary, "FPDF_InitLibrary");
-  NS_ENSURE_TRUE(mFPDF_InitLibrary, false);
+  mTable->mFPDF_InitLibrary =
+    (FPDF_InitLibrary_Pfn)PR_FindFunctionSymbol(mPRLibrary,
+                                                "FPDF_InitLibrary");
+  NS_ENSURE_TRUE(mTable->mFPDF_InitLibrary, false)
 
-  mFPDF_DestroyLibrary = (FPDF_DestroyLibrary_Pfn)PR_FindFunctionSymbol(
-    mPRLibrary, "FPDF_DestroyLibrary");
-  NS_ENSURE_TRUE(mFPDF_DestroyLibrary, false);
+  mTable->mFPDF_DestroyLibrary =
+    (FPDF_DestroyLibrary_Pfn)PR_FindFunctionSymbol(mPRLibrary,
+                                                  "FPDF_DestroyLibrary");
+  NS_ENSURE_TRUE(mTable->mFPDF_DestroyLibrary, false);
 
-  mFPDF_LoadDocument = (FPDF_LoadDocument_Pfn)PR_FindFunctionSymbol(
-    mPRLibrary, "FPDF_LoadDocument");
-  NS_ENSURE_TRUE(mFPDF_LoadDocument, false);
+  mTable->mFPDF_LoadDocument =
+    (FPDF_LoadDocument_Pfn)PR_FindFunctionSymbol(mPRLibrary,
+                                                 "FPDF_LoadDocument");
+  NS_ENSURE_TRUE(mTable->mFPDF_LoadDocument, false);
 
-  mFPDF_LoadCustomDocument = (FPDF_LoadCustomDocument_Pfn)PR_FindFunctionSymbol(
-    mPRLibrary, "FPDF_LoadCustomDocument");
-  NS_ENSURE_TRUE(mFPDF_LoadCustomDocument, false);
+  mTable->mFPDF_LoadCustomDocument =
+    (FPDF_LoadCustomDocument_Pfn)PR_FindFunctionSymbol(mPRLibrary,
+                                                       "FPDF_LoadCustomDocument");
+  NS_ENSURE_TRUE(mTable->mFPDF_LoadCustomDocument, false);
 
-  mFPDF_CloseDocument = (FPDF_CloseDocument_Pfn)PR_FindFunctionSymbol(
-    mPRLibrary, "FPDF_CloseDocument");
-  NS_ENSURE_TRUE(mFPDF_CloseDocument, false);
+  mTable->mFPDF_CloseDocument =
+    (FPDF_CloseDocument_Pfn)PR_FindFunctionSymbol(mPRLibrary,
+                                                  "FPDF_CloseDocument");
+  NS_ENSURE_TRUE(mTable->mFPDF_CloseDocument, false);
 
-  mFPDF_GetPageCount = (FPDF_GetPageCount_Pfn)PR_FindFunctionSymbol(
-    mPRLibrary, "FPDF_GetPageCount");
-  NS_ENSURE_TRUE(mFPDF_GetPageCount, false);
+  mTable->mFPDF_GetPageCount =
+    (FPDF_GetPageCount_Pfn)PR_FindFunctionSymbol(mPRLibrary,
+                                                 "FPDF_GetPageCount");
+  NS_ENSURE_TRUE(mTable->mFPDF_GetPageCount, false);
 
-  mFPDF_LoadPage = (FPDF_LoadPage_Pfn)PR_FindFunctionSymbol(
-    mPRLibrary, "FPDF_LoadPage");
-  NS_ENSURE_TRUE(mFPDF_LoadPage, false);
+  mTable->mFPDF_LoadPage =
+    (FPDF_LoadPage_Pfn)PR_FindFunctionSymbol(mPRLibrary, "FPDF_LoadPage");
+  NS_ENSURE_TRUE(mTable->mFPDF_LoadPage, false);
 
-  mFPDF_ClosePage = (FPDF_ClosePage_Pfn)PR_FindFunctionSymbol(
-    mPRLibrary, "FPDF_ClosePage");
-  NS_ENSURE_TRUE(mFPDF_ClosePage, false);
+  mTable->mFPDF_ClosePage =
+    (FPDF_ClosePage_Pfn)PR_FindFunctionSymbol(mPRLibrary, "FPDF_ClosePage");
+  NS_ENSURE_TRUE(mTable->mFPDF_ClosePage, false);
 
-  mFPDF_RenderPage = (FPDF_RenderPage_Pfn)PR_FindFunctionSymbol(
-    mPRLibrary, "FPDF_RenderPage");
-  NS_ENSURE_TRUE(mFPDF_RenderPage, false);
+  mTable->mFPDF_RenderPage =
+    (FPDF_RenderPage_Pfn)PR_FindFunctionSymbol(mPRLibrary, "FPDF_RenderPage");
+  NS_ENSURE_TRUE(mTable->mFPDF_RenderPage, false);
 
-  mFPDF_InitLibrary();
+  mTable->mFPDF_InitLibrary();
   mInitialized = true;
   return true;
 }
 
 FPDF_DOCUMENT
 PDFiumEngineShim::LoadDocument(FPDF_STRING file_path,
                                FPDF_BYTESTRING aPassword)
 {
   MOZ_ASSERT(mInitialized);
-  return mFPDF_LoadDocument(file_path, aPassword);
+  return mTable->mFPDF_LoadDocument(file_path, aPassword);
 }
 
 FPDF_DOCUMENT
 PDFiumEngineShim::LoadDocument(PRFileDesc* aPrfile,
                                FPDF_BYTESTRING aPassword)
 {
   MOZ_ASSERT(mInitialized && aPrfile);
 
@@ -153,52 +200,52 @@ PDFiumEngineShim::LoadDocument(PRFileDes
 
       if (PR_Read(prfile, buf, size) <= 0) {
         return 0;
       }
 
       return 1;
     };
 
-  return mFPDF_LoadCustomDocument(&fileAccess, aPassword);
+  return mTable->mFPDF_LoadCustomDocument(&fileAccess, aPassword);
 }
 
 void
 PDFiumEngineShim::CloseDocument(FPDF_DOCUMENT aDocument)
 {
   MOZ_ASSERT(mInitialized);
-  mFPDF_CloseDocument(aDocument);
+  mTable->mFPDF_CloseDocument(aDocument);
 }
 
 int
 PDFiumEngineShim::GetPageCount(FPDF_DOCUMENT aDocument)
 {
   MOZ_ASSERT(mInitialized);
-  return mFPDF_GetPageCount(aDocument);
+  return mTable->mFPDF_GetPageCount(aDocument);
 }
 
 FPDF_PAGE
 PDFiumEngineShim::LoadPage(FPDF_DOCUMENT aDocument, int aPageIndex)
 {
   MOZ_ASSERT(mInitialized);
-  return mFPDF_LoadPage(aDocument, aPageIndex);
+  return mTable->mFPDF_LoadPage(aDocument, aPageIndex);
 }
 
 void
 PDFiumEngineShim::ClosePage(FPDF_PAGE aPage)
 {
   MOZ_ASSERT(mInitialized);
-  mFPDF_ClosePage(aPage);
+  mTable->mFPDF_ClosePage(aPage);
 }
 
 void
 PDFiumEngineShim::RenderPage(HDC aDC, FPDF_PAGE aPage,
                              int aStartX, int aStartY,
                              int aSizeX, int aSizeY,
                              int aRotate, int aFlags)
 {
   MOZ_ASSERT(mInitialized);
-  mFPDF_RenderPage(aDC, aPage, aStartX, aStartY,
-                   aSizeX, aSizeY, aRotate, aFlags);
+  mTable->mFPDF_RenderPage(aDC, aPage, aStartX, aStartY,
+                           aSizeX, aSizeY, aRotate, aFlags);
 }
 
 } // namespace widget
 } // namespace mozilla
diff --git a/widget/windows/PDFiumEngineShim.h b/widget/windows/PDFiumEngineShim.h
--- a/widget/windows/PDFiumEngineShim.h
+++ b/widget/windows/PDFiumEngineShim.h
@@ -7,42 +7,22 @@
 #define PDFIUMENGINESHIM_H
 
 #include "prlink.h"
 #include "fpdfview.h"
 
 /* include windows.h for the HDC definitions that we need. */
 #include <windows.h>
 #include "private/pprio.h"
+#include "mozilla/UniquePtr.h"
 
 namespace mozilla {
 namespace widget {
 
-typedef void (STDCALL *FPDF_InitLibrary_Pfn)();
-typedef void (STDCALL *FPDF_DestroyLibrary_Pfn)();
-
-typedef FPDF_DOCUMENT (STDCALL *FPDF_LoadDocument_Pfn)(FPDF_STRING file_path,
-                                                      FPDF_BYTESTRING password);
-typedef FPDF_DOCUMENT (STDCALL *FPDF_LoadCustomDocument_Pfn)(FPDF_FILEACCESS* pFileAccess,
-                                                             FPDF_BYTESTRING password);
-typedef void(STDCALL *FPDF_CloseDocument_Pfn)(FPDF_DOCUMENT aDocument);
-
-typedef int (STDCALL *FPDF_GetPageCount_Pfn)(FPDF_DOCUMENT aDocument);
-
-typedef FPDF_PAGE (STDCALL *FPDF_LoadPage_Pfn)(FPDF_DOCUMENT aDocument,
-                                               int aPageIndex);
-typedef void (STDCALL *FPDF_ClosePage_Pfn)(FPDF_PAGE aPage);
-typedef void (STDCALL *FPDF_RenderPage_Pfn)(HDC aDC,
-                                            FPDF_PAGE aPage,
-                                            int aStartX,
-                                            int aStartY,
-                                            int aSizeX,
-                                            int aSizeY,
-                                            int aRotate,
-                                            int aFlags);
+struct PDFFunctionPointerTable;
 
 /**
  * This class exposes an interface to the PDFium library and
  * takes care of loading and linking to the appropriate PDFium symbols.
  */
 class PDFiumEngineShim
 {
 public:
@@ -71,27 +51,18 @@ public:
                   int aSizeX, int aSizeY,
                   int aRotate, int aFlags);
 
 private:
   PDFiumEngineShim();
   ~PDFiumEngineShim();
   bool Init(const nsString& aLibrary);
 
+  UniquePtr<PDFFunctionPointerTable> mTable;
   bool        mInitialized ;
 
-  FPDF_InitLibrary_Pfn        mFPDF_InitLibrary;
-  FPDF_DestroyLibrary_Pfn     mFPDF_DestroyLibrary;
-  FPDF_LoadDocument_Pfn       mFPDF_LoadDocument;
-  FPDF_LoadCustomDocument_Pfn mFPDF_LoadCustomDocument;
-  FPDF_CloseDocument_Pfn      mFPDF_CloseDocument;
-  FPDF_GetPageCount_Pfn       mFPDF_GetPageCount;
-  FPDF_LoadPage_Pfn           mFPDF_LoadPage;
-  FPDF_ClosePage_Pfn          mFPDF_ClosePage;
-  FPDF_RenderPage_Pfn         mFPDF_RenderPage;
-
   PRLibrary*  mPRLibrary;
 };
 
 } // namespace widget
 } // namespace mozilla
 
 #endif /* PDFIUMENGINESHIM_H */
