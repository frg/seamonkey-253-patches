# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1511793706 21600
# Node ID 24638ee166eac6ff9ff1eafde4e7525bcf89d1d8
# Parent  932b0e4eaac21667c99ae7f11ac37b8b8f0ebf1e
servo: Merge #19390 - style: Minor cleanups while the sync service is down (from emilio:never-enough-cleanup); r=nox

I cannot land my Gecko-dependent patches, so let's do some cleanup in the meantime.

Source-Repo: https://github.com/servo/servo
Source-Revision: 6d49ec83a15f440f85cb9a045eaf104f30bd3053

diff --git a/servo/components/style/gecko/restyle_damage.rs b/servo/components/style/gecko/restyle_damage.rs
--- a/servo/components/style/gecko/restyle_damage.rs
+++ b/servo/components/style/gecko/restyle_damage.rs
@@ -67,17 +67,18 @@ impl GeckoRestyleDamage {
             any_style_changed = true;
             reset_only = false;
         }
         let change = if any_style_changed {
             StyleChange::Changed { reset_only }
         } else {
             StyleChange::Unchanged
         };
-        StyleDifference::new(GeckoRestyleDamage(nsChangeHint(hint)), change)
+        let damage = GeckoRestyleDamage(nsChangeHint(hint));
+        StyleDifference { damage, change }
     }
 
     /// Returns true if this restyle damage contains all the damage of |other|.
     pub fn contains(self, other: Self) -> bool {
         self & other == other
     }
 
     /// Gets restyle damage to reconstruct the entire frame, subsuming all
diff --git a/servo/components/style/matching.rs b/servo/components/style/matching.rs
--- a/servo/components/style/matching.rs
+++ b/servo/components/style/matching.rs
@@ -26,26 +26,16 @@ use traversal_flags::TraversalFlags;
 pub struct StyleDifference {
     /// The resulting damage.
     pub damage: RestyleDamage,
 
     /// Whether any styles changed.
     pub change: StyleChange,
 }
 
-impl StyleDifference {
-    /// Creates a new `StyleDifference`.
-    pub fn new(damage: RestyleDamage, change: StyleChange) -> Self {
-        StyleDifference {
-            change: change,
-            damage: damage,
-        }
-    }
-}
-
 /// Represents whether or not the style of an element has changed.
 #[derive(Clone, Copy, Debug)]
 pub enum StyleChange {
     /// The style hasn't changed.
     Unchanged,
     /// The style has changed.
     Changed {
         /// Whether only reset structs changed.
@@ -347,88 +337,90 @@ trait PrivateMatchMethods: TElement {
         // We need to cascade the children in order to ensure the correct
         // propagation of inherited computed value flags.
         if old_values.flags.inherited() != new_values.flags.inherited() {
             debug!(" > flags changed: {:?} != {:?}", old_values.flags, new_values.flags);
             return ChildCascadeRequirement::MustCascadeChildren;
         }
 
         match difference.change {
-            StyleChange::Unchanged => ChildCascadeRequirement::CanSkipCascade,
+            StyleChange::Unchanged => {
+                return ChildCascadeRequirement::CanSkipCascade
+            },
             StyleChange::Changed { reset_only } => {
                 // If inherited properties changed, the best we can do is
                 // cascade the children.
                 if !reset_only {
                     return ChildCascadeRequirement::MustCascadeChildren
                 }
-
-                let old_display = old_values.get_box().clone_display();
-                let new_display = new_values.get_box().clone_display();
-
-                // If we used to be a display: none element, and no longer are,
-                // our children need to be restyled because they're unstyled.
-                //
-                // NOTE(emilio): Gecko has the special-case of -moz-binding, but
-                // that gets handled on the frame constructor when processing
-                // the reframe, so no need to handle that here.
-                if old_display == display::T::none && old_display != new_display {
-                    return ChildCascadeRequirement::MustCascadeChildren
-                }
+            }
+        }
 
-                // Blockification of children may depend on our display value,
-                // so we need to actually do the recascade. We could potentially
-                // do better, but it doesn't seem worth it.
-                if old_display.is_item_container() != new_display.is_item_container() {
-                    return ChildCascadeRequirement::MustCascadeChildren
-                }
-
-                // Line break suppression may also be affected if the display
-                // type changes from ruby to non-ruby.
-                #[cfg(feature = "gecko")]
-                {
-                    if old_display.is_ruby_type() != new_display.is_ruby_type() {
-                        return ChildCascadeRequirement::MustCascadeChildren
-                    }
-                }
+        let old_display = old_values.get_box().clone_display();
+        let new_display = new_values.get_box().clone_display();
 
-                // Children with justify-items: auto may depend on our
-                // justify-items property value.
-                //
-                // Similarly, we could potentially do better, but this really
-                // seems not common enough to care about.
-                #[cfg(feature = "gecko")]
-                {
-                    use values::specified::align::AlignFlags;
-
-                    let old_justify_items =
-                        old_values.get_position().clone_justify_items();
-                    let new_justify_items =
-                        new_values.get_position().clone_justify_items();
-
-                    let was_legacy_justify_items =
-                        old_justify_items.computed.0.contains(AlignFlags::LEGACY);
+        // If we used to be a display: none element, and no longer are,
+        // our children need to be restyled because they're unstyled.
+        //
+        // NOTE(emilio): Gecko has the special-case of -moz-binding, but
+        // that gets handled on the frame constructor when processing
+        // the reframe, so no need to handle that here.
+        if old_display == display::T::none && old_display != new_display {
+            return ChildCascadeRequirement::MustCascadeChildren
+        }
 
-                    let is_legacy_justify_items =
-                        new_justify_items.computed.0.contains(AlignFlags::LEGACY);
-
-                    if is_legacy_justify_items != was_legacy_justify_items {
-                        return ChildCascadeRequirement::MustCascadeChildren;
-                    }
+        // Blockification of children may depend on our display value,
+        // so we need to actually do the recascade. We could potentially
+        // do better, but it doesn't seem worth it.
+        if old_display.is_item_container() != new_display.is_item_container() {
+            return ChildCascadeRequirement::MustCascadeChildren
+        }
 
-                    if was_legacy_justify_items &&
-                        old_justify_items.computed != new_justify_items.computed {
-                        return ChildCascadeRequirement::MustCascadeChildren;
-                    }
-                }
-
-                // We could prove that, if our children don't inherit reset
-                // properties, we can stop the cascade.
-                ChildCascadeRequirement::MustCascadeChildrenIfInheritResetStyle
+        // Line break suppression may also be affected if the display
+        // type changes from ruby to non-ruby.
+        #[cfg(feature = "gecko")]
+        {
+            if old_display.is_ruby_type() != new_display.is_ruby_type() {
+                return ChildCascadeRequirement::MustCascadeChildren
             }
         }
+
+        // Children with justify-items: auto may depend on our
+        // justify-items property value.
+        //
+        // Similarly, we could potentially do better, but this really
+        // seems not common enough to care about.
+        #[cfg(feature = "gecko")]
+        {
+            use values::specified::align::AlignFlags;
+
+            let old_justify_items =
+                old_values.get_position().clone_justify_items();
+            let new_justify_items =
+                new_values.get_position().clone_justify_items();
+
+            let was_legacy_justify_items =
+                old_justify_items.computed.0.contains(AlignFlags::LEGACY);
+
+            let is_legacy_justify_items =
+                new_justify_items.computed.0.contains(AlignFlags::LEGACY);
+
+            if is_legacy_justify_items != was_legacy_justify_items {
+                return ChildCascadeRequirement::MustCascadeChildren;
+            }
+
+            if was_legacy_justify_items &&
+                old_justify_items.computed != new_justify_items.computed {
+                return ChildCascadeRequirement::MustCascadeChildren;
+            }
+        }
+
+        // We could prove that, if our children don't inherit reset
+        // properties, we can stop the cascade.
+        ChildCascadeRequirement::MustCascadeChildrenIfInheritResetStyle
     }
 
     #[cfg(feature = "servo")]
     fn update_animations_for_cascade(&self,
                                      context: &SharedStyleContext,
                                      style: &mut Arc<ComputedValues>,
                                      possibly_expired_animations: &mut Vec<::animation::PropertyAnimation>,
                                      font_metrics: &::font_metrics::FontMetricsProvider) {
diff --git a/servo/components/style/parser.rs b/servo/components/style/parser.rs
--- a/servo/components/style/parser.rs
+++ b/servo/components/style/parser.rs
@@ -56,16 +56,17 @@ pub struct ParserContext<'a> {
     /// The quirks mode of this stylesheet.
     pub quirks_mode: QuirksMode,
     /// The currently active namespaces.
     pub namespaces: Option<&'a Namespaces>,
 }
 
 impl<'a> ParserContext<'a> {
     /// Create a parser context.
+    #[inline]
     pub fn new(
         stylesheet_origin: Origin,
         url_data: &'a UrlExtraData,
         rule_type: Option<CssRuleType>,
         parsing_mode: ParsingMode,
         quirks_mode: QuirksMode,
     ) -> Self {
         ParserContext {
@@ -74,32 +75,34 @@ impl<'a> ParserContext<'a> {
             rule_type,
             parsing_mode,
             quirks_mode,
             namespaces: None,
         }
     }
 
     /// Create a parser context for on-the-fly parsing in CSSOM
+    #[inline]
     pub fn new_for_cssom(
         url_data: &'a UrlExtraData,
         rule_type: Option<CssRuleType>,
         parsing_mode: ParsingMode,
         quirks_mode: QuirksMode,
     ) -> Self {
         Self::new(
             Origin::Author,
             url_data,
             rule_type,
             parsing_mode,
             quirks_mode,
         )
     }
 
     /// Create a parser context based on a previous context, but with a modified rule type.
+    #[inline]
     pub fn new_with_rule_type(
         context: &'a ParserContext,
         rule_type: CssRuleType,
         namespaces: &'a Namespaces,
     ) -> ParserContext<'a> {
         ParserContext {
             stylesheet_origin: context.stylesheet_origin,
             url_data: context.url_data,
diff --git a/servo/components/style/properties/declaration_block.rs b/servo/components/style/properties/declaration_block.rs
--- a/servo/components/style/properties/declaration_block.rs
+++ b/servo/components/style/properties/declaration_block.rs
@@ -1011,28 +1011,33 @@ pub fn append_serialization<'a, W, I, N>
         dest.write_str(" !important")?;
     }
 
     dest.write_char(';')
 }
 
 /// A helper to parse the style attribute of an element, in order for this to be
 /// shared between Servo and Gecko.
-pub fn parse_style_attribute<R>(input: &str,
-                                url_data: &UrlExtraData,
-                                error_reporter: &R,
-                                quirks_mode: QuirksMode)
-                                -> PropertyDeclarationBlock
-    where R: ParseErrorReporter
+pub fn parse_style_attribute<R>(
+    input: &str,
+    url_data: &UrlExtraData,
+    error_reporter: &R,
+    quirks_mode: QuirksMode,
+) -> PropertyDeclarationBlock
+where
+    R: ParseErrorReporter
 {
-    let context = ParserContext::new(Origin::Author,
-                                     url_data,
-                                     Some(CssRuleType::Style),
-                                     ParsingMode::DEFAULT,
-                                     quirks_mode);
+    let context = ParserContext::new(
+        Origin::Author,
+        url_data,
+        Some(CssRuleType::Style),
+        ParsingMode::DEFAULT,
+        quirks_mode,
+    );
+
     let error_context = ParserErrorContext { error_reporter: error_reporter };
     let mut input = ParserInput::new(input);
     parse_property_declaration_list(&context, &error_context, &mut Parser::new(&mut input))
 }
 
 /// Parse a given property declaration. Can result in multiple
 /// `PropertyDeclaration`s when expanding a shorthand, for example.
 ///
@@ -1044,21 +1049,24 @@ pub fn parse_one_declaration_into<R>(
     url_data: &UrlExtraData,
     error_reporter: &R,
     parsing_mode: ParsingMode,
     quirks_mode: QuirksMode
 ) -> Result<(), ()>
 where
     R: ParseErrorReporter
 {
-    let context = ParserContext::new(Origin::Author,
-                                     url_data,
-                                     Some(CssRuleType::Style),
-                                     parsing_mode,
-                                     quirks_mode);
+    let context = ParserContext::new(
+        Origin::Author,
+        url_data,
+        Some(CssRuleType::Style),
+        parsing_mode,
+        quirks_mode,
+    );
+
     let mut input = ParserInput::new(input);
     let mut parser = Parser::new(&mut input);
     let start_position = parser.position();
     parser.parse_entirely(|parser| {
         let name = id.name().into();
         PropertyDeclaration::parse_into(declarations, id, name, &context, parser)
             .map_err(|e| e.into())
     }).map_err(|err| {
diff --git a/servo/components/style/properties/properties.mako.rs b/servo/components/style/properties/properties.mako.rs
--- a/servo/components/style/properties/properties.mako.rs
+++ b/servo/components/style/properties/properties.mako.rs
@@ -949,24 +949,26 @@ impl UnparsedValue {
         &self,
         longhand_id: LonghandId,
         custom_properties: Option<<&Arc<::custom_properties::CustomPropertiesMap>>,
         quirks_mode: QuirksMode,
     ) -> PropertyDeclaration {
         ::custom_properties::substitute(&self.css, self.first_token_type, custom_properties)
         .ok()
         .and_then(|css| {
-            // As of this writing, only the base URL is used for property values:
+            // As of this writing, only the base URL is used for property
+            // values.
             let context = ParserContext::new(
                 Origin::Author,
                 &self.url_data,
                 None,
                 ParsingMode::DEFAULT,
                 quirks_mode,
             );
+
             let mut input = ParserInput::new(&css);
             Parser::new(&mut input).parse_entirely(|input| {
                 match self.from_shorthand {
                     None => longhand_id.parse_value(&context, input),
                     Some(ShorthandId::All) => {
                         // No need to parse the 'all' shorthand as anything other than a CSS-wide
                         // keyword, after variable substitution.
                         Err(input.new_custom_error(SelectorParseErrorKind::UnexpectedIdent("all".into())))
diff --git a/servo/components/style/servo/restyle_damage.rs b/servo/components/style/servo/restyle_damage.rs
--- a/servo/components/style/servo/restyle_damage.rs
+++ b/servo/components/style/servo/restyle_damage.rs
@@ -1,58 +1,62 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! The restyle damage is a hint that tells layout which kind of operations may
 //! be needed in presence of incremental style changes.
 
-#![deny(missing_docs)]
-
 use computed_values::display;
 use matching::{StyleChange, StyleDifference};
 use properties::ComputedValues;
 use std::fmt;
 
 bitflags! {
-    #[doc = "Individual layout actions that may be necessary after restyling."]
+    /// Individual layout actions that may be necessary after restyling.
     pub struct ServoRestyleDamage: u8 {
-        #[doc = "Repaint the node itself."]
-        #[doc = "Currently unused; need to decide how this propagates."]
+        /// Repaint the node itself.
+        ///
+        /// Currently unused; need to decide how this propagates.
         const REPAINT = 0x01;
 
-        #[doc = "The stacking-context-relative position of this node or its descendants has \
-                 changed."]
-        #[doc = "Propagates both up and down the flow tree."]
+        /// The stacking-context-relative position of this node or its
+        /// descendants has changed.
+        ///
+        /// Propagates both up and down the flow tree.
         const REPOSITION = 0x02;
 
-        #[doc = "Recompute the overflow regions (bounding box of object and all descendants)."]
-        #[doc = "Propagates down the flow tree because the computation is bottom-up."]
+        /// Recompute the overflow regions (bounding box of object and all descendants).
+        ///
+        /// Propagates down the flow tree because the computation is bottom-up.
         const STORE_OVERFLOW = 0x04;
 
-        #[doc = "Recompute intrinsic inline_sizes (minimum and preferred)."]
-        #[doc = "Propagates down the flow tree because the computation is"]
-        #[doc = "bottom-up."]
+        /// Recompute intrinsic inline_sizes (minimum and preferred).
+        ///
+        /// Propagates down the flow tree because the computation is.
+        /// bottom-up.
         const BUBBLE_ISIZES = 0x08;
 
-        #[doc = "Recompute actual inline-sizes and block-sizes, only taking out-of-flow children \
-                 into account. \
-                 Propagates up the flow tree because the computation is top-down."]
+        /// Recompute actual inline-sizes and block-sizes, only taking
+        /// out-of-flow children into account.
+        ///
+        /// Propagates up the flow tree because the computation is top-down.
         const REFLOW_OUT_OF_FLOW = 0x10;
 
-        #[doc = "Recompute actual inline_sizes and block_sizes."]
-        #[doc = "Propagates up the flow tree because the computation is"]
-        #[doc = "top-down."]
+        /// Recompute actual inline_sizes and block_sizes.
+        ///
+        /// Propagates up the flow tree because the computation is top-down.
         const REFLOW = 0x20;
 
-        #[doc = "Re-resolve generated content. \
-                 Propagates up the flow tree because the computation is inorder."]
+        /// Re-resolve generated content.
+        ///
+        /// Propagates up the flow tree because the computation is inorder.
         const RESOLVE_GENERATED_CONTENT = 0x40;
 
-        #[doc = "The entire flow needs to be reconstructed."]
+        /// The entire flow needs to be reconstructed.
         const RECONSTRUCT_FLOW = 0x80;
     }
 }
 
 malloc_size_of_is_0!(ServoRestyleDamage);
 
 impl ServoRestyleDamage {
     /// Compute the `StyleDifference` (including the appropriate restyle damage)
@@ -65,17 +69,17 @@ impl ServoRestyleDamage {
         let change = if damage.is_empty() {
             StyleChange::Unchanged
         } else {
             // FIXME(emilio): Differentiate between reset and inherited
             // properties here, and set `reset_only` appropriately so the
             // optimization to skip the cascade in those cases applies.
             StyleChange::Changed { reset_only: false }
         };
-        StyleDifference::new(damage, change)
+        StyleDifference { damage, change }
     }
 
     /// Returns a bitmask that represents a flow that needs to be rebuilt and
     /// reflowed.
     ///
     /// FIXME(bholley): Do we ever actually need this? Shouldn't
     /// RECONSTRUCT_FLOW imply everything else?
     pub fn rebuild_and_reflow() -> ServoRestyleDamage {
diff --git a/servo/components/style/stylesheets/rule_parser.rs b/servo/components/style/stylesheets/rule_parser.rs
--- a/servo/components/style/stylesheets/rule_parser.rs
+++ b/servo/components/style/stylesheets/rule_parser.rs
@@ -308,18 +308,21 @@ struct NestedRuleParser<'a, 'b: 'a, R: '
 }
 
 impl<'a, 'b, R: ParseErrorReporter> NestedRuleParser<'a, 'b, R> {
     fn parse_nested_rules(
         &mut self,
         input: &mut Parser,
         rule_type: CssRuleType
     ) -> Arc<Locked<CssRules>> {
-        let context =
-            ParserContext::new_with_rule_type(self.context, rule_type, self.namespaces);
+        let context = ParserContext::new_with_rule_type(
+            self.context,
+            rule_type,
+            self.namespaces,
+        );
 
         let nested_parser = NestedRuleParser {
             stylesheet_origin: self.stylesheet_origin,
             shared_lock: self.shared_lock,
             context: &context,
             error_context: &self.error_context,
             namespaces: self.namespaces,
         };
@@ -434,100 +437,98 @@ impl<'a, 'b, 'i, R: ParseErrorReporter> 
 
     fn parse_block<'t>(
         &mut self,
         prelude: AtRuleBlockPrelude,
         input: &mut Parser<'i, 't>
     ) -> Result<CssRule, ParseError<'i>> {
         match prelude {
             AtRuleBlockPrelude::FontFace(location) => {
-                let context =
-                    ParserContext::new_with_rule_type(
-                        self.context,
-                        CssRuleType::FontFace,
-                        self.namespaces,
-                    );
+                let context = ParserContext::new_with_rule_type(
+                    self.context,
+                    CssRuleType::FontFace,
+                    self.namespaces,
+                );
 
                 Ok(CssRule::FontFace(Arc::new(self.shared_lock.wrap(
                    parse_font_face_block(&context, self.error_context, input, location).into()))))
             }
             AtRuleBlockPrelude::FontFeatureValues(family_names, location) => {
-                let context =
-                    ParserContext::new_with_rule_type(
-                        self.context,
-                        CssRuleType::FontFeatureValues,
-                        self.namespaces,
-                    );
+                let context = ParserContext::new_with_rule_type(
+                    self.context,
+                    CssRuleType::FontFeatureValues,
+                    self.namespaces,
+                );
+
                 Ok(CssRule::FontFeatureValues(Arc::new(self.shared_lock.wrap(
                     FontFeatureValuesRule::parse(&context, self.error_context, input, family_names, location)))))
             }
             AtRuleBlockPrelude::CounterStyle(name) => {
-                let context =
-                    ParserContext::new_with_rule_type(
-                        self.context,
-                        CssRuleType::CounterStyle,
-                        self.namespaces,
-                    );
+                let context = ParserContext::new_with_rule_type(
+                    self.context,
+                    CssRuleType::CounterStyle,
+                    self.namespaces,
+                );
+
                 Ok(CssRule::CounterStyle(Arc::new(self.shared_lock.wrap(
                    parse_counter_style_body(name, &context, self.error_context, input)?.into()))))
             }
             AtRuleBlockPrelude::Media(media_queries, location) => {
                 Ok(CssRule::Media(Arc::new(self.shared_lock.wrap(MediaRule {
                     media_queries: media_queries,
                     rules: self.parse_nested_rules(input, CssRuleType::Media),
                     source_location: location,
                 }))))
             }
             AtRuleBlockPrelude::Supports(cond, location) => {
-                let eval_context =
-                    ParserContext::new_with_rule_type(
-                        self.context,
-                        CssRuleType::Style,
-                        self.namespaces,
-                    );
+                let eval_context = ParserContext::new_with_rule_type(
+                    self.context,
+                    CssRuleType::Style,
+                    self.namespaces,
+                );
+
                 let enabled = cond.eval(&eval_context);
                 Ok(CssRule::Supports(Arc::new(self.shared_lock.wrap(SupportsRule {
                     condition: cond,
                     rules: self.parse_nested_rules(input, CssRuleType::Supports),
                     enabled: enabled,
                     source_location: location,
                 }))))
             }
             AtRuleBlockPrelude::Viewport => {
-                let context =
-                    ParserContext::new_with_rule_type(
-                        self.context,
-                        CssRuleType::Viewport,
-                        self.namespaces,
-                    );
+                let context = ParserContext::new_with_rule_type(
+                    self.context,
+                    CssRuleType::Viewport,
+                    self.namespaces,
+                );
+
                 Ok(CssRule::Viewport(Arc::new(self.shared_lock.wrap(
                    ViewportRule::parse(&context, self.error_context, input)?))))
             }
             AtRuleBlockPrelude::Keyframes(name, prefix, location) => {
-                let context =
-                    ParserContext::new_with_rule_type(
-                        self.context,
-                        CssRuleType::Keyframes,
-                        self.namespaces,
-                    );
+                let context = ParserContext::new_with_rule_type(
+                    self.context,
+                    CssRuleType::Keyframes,
+                    self.namespaces,
+                );
 
                 Ok(CssRule::Keyframes(Arc::new(self.shared_lock.wrap(KeyframesRule {
                     name: name,
                     keyframes: parse_keyframe_list(&context, self.error_context, input, self.shared_lock),
                     vendor_prefix: prefix,
                     source_location: location,
                 }))))
             }
             AtRuleBlockPrelude::Page(location) => {
-                let context =
-                    ParserContext::new_with_rule_type(
-                        self.context,
-                        CssRuleType::Page,
-                        self.namespaces,
-                    );
+                let context = ParserContext::new_with_rule_type(
+                    self.context,
+                    CssRuleType::Page,
+                    self.namespaces,
+                );
+
                 let declarations = parse_property_declaration_list(&context, self.error_context, input);
                 Ok(CssRule::Page(Arc::new(self.shared_lock.wrap(PageRule {
                     block: Arc::new(self.shared_lock.wrap(declarations)),
                     source_location: location,
                 }))))
             }
             AtRuleBlockPrelude::Document(cond, location) => {
                 if cfg!(feature = "gecko") {
@@ -568,22 +569,22 @@ impl<'a, 'b, 'i, R: ParseErrorReporter> 
         })
     }
 
     fn parse_block<'t>(
         &mut self,
         prelude: QualifiedRuleParserPrelude,
         input: &mut Parser<'i, 't>
     ) -> Result<CssRule, ParseError<'i>> {
-        let context =
-            ParserContext::new_with_rule_type(
-                self.context,
-                CssRuleType::Style,
-                self.namespaces,
-            );
+        let context = ParserContext::new_with_rule_type(
+            self.context,
+            CssRuleType::Style,
+            self.namespaces,
+        );
+
         let declarations = parse_property_declaration_list(&context, self.error_context, input);
         Ok(CssRule::Style(Arc::new(self.shared_lock.wrap(StyleRule {
             selectors: prelude.selectors,
             block: Arc::new(self.shared_lock.wrap(declarations)),
             source_location: prelude.source_location,
         }))))
     }
 }
diff --git a/servo/components/style/stylesheets/stylesheet.rs b/servo/components/style/stylesheets/stylesheet.rs
--- a/servo/components/style/stylesheets/stylesheet.rs
+++ b/servo/components/style/stylesheets/stylesheet.rs
@@ -304,23 +304,26 @@ impl StylesheetInDocument for DocumentSt
 
     fn enabled(&self) -> bool {
         self.0.enabled()
     }
 }
 
 impl Stylesheet {
     /// Updates an empty stylesheet from a given string of text.
-    pub fn update_from_str<R>(existing: &Stylesheet,
-                              css: &str,
-                              url_data: UrlExtraData,
-                              stylesheet_loader: Option<&StylesheetLoader>,
-                              error_reporter: &R,
-                              line_number_offset: u32)
-        where R: ParseErrorReporter
+    pub fn update_from_str<R>(
+        existing: &Stylesheet,
+        css: &str,
+        url_data: UrlExtraData,
+        stylesheet_loader: Option<&StylesheetLoader>,
+        error_reporter: &R,
+        line_number_offset: u32,
+    )
+    where
+        R: ParseErrorReporter,
     {
         let namespaces = RwLock::new(Namespaces::default());
         let (rules, source_map_url, source_url) =
             Stylesheet::parse_rules(
                 css,
                 &url_data,
                 existing.contents.origin,
                 &mut *namespaces.write(),
@@ -354,24 +357,24 @@ impl Stylesheet {
         error_reporter: &R,
         quirks_mode: QuirksMode,
         line_number_offset: u32
     ) -> (Vec<CssRule>, Option<String>, Option<String>) {
         let mut rules = Vec::new();
         let mut input = ParserInput::new_with_line_number_offset(css, line_number_offset);
         let mut input = Parser::new(&mut input);
 
-        let context =
-            ParserContext::new(
-                origin,
-                url_data,
-                None,
-                ParsingMode::DEFAULT,
-                quirks_mode
-            );
+        let context = ParserContext::new(
+            origin,
+            url_data,
+            None,
+            ParsingMode::DEFAULT,
+            quirks_mode
+        );
+
         let error_context = ParserErrorContext { error_reporter };
 
         let rule_parser = TopLevelRuleParser {
             stylesheet_origin: origin,
             shared_lock: shared_lock,
             loader: stylesheet_loader,
             context: context,
             error_context: error_context,
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -2487,21 +2487,23 @@ pub extern "C" fn Servo_ParseEasing(
     easing: *const nsAString,
     data: *mut URLExtraData,
     output: nsTimingFunctionBorrowedMut
 ) -> bool {
     use style::properties::longhands::transition_timing_function;
 
     // FIXME Dummy URL data would work fine here.
     let url_data = unsafe { RefPtr::from_ptr_ref(&data) };
-    let context = ParserContext::new(Origin::Author,
-                                     url_data,
-                                     Some(CssRuleType::Style),
-                                     ParsingMode::DEFAULT,
-                                     QuirksMode::NoQuirks);
+    let context = ParserContext::new(
+        Origin::Author,
+        url_data,
+        Some(CssRuleType::Style),
+        ParsingMode::DEFAULT,
+        QuirksMode::NoQuirks,
+    );
     let easing = unsafe { (*easing).to_string() };
     let mut input = ParserInput::new(&easing);
     let mut parser = Parser::new(&mut input);
     let result =
         parser.parse_entirely(|p| transition_timing_function::single_value::parse(&context, p));
     match result {
         Ok(parsed_easing) => {
             *output = parsed_easing.into();
@@ -2956,34 +2958,40 @@ pub extern "C" fn Servo_MediaList_GetMed
 
 #[no_mangle]
 pub extern "C" fn Servo_MediaList_AppendMedium(
     list: RawServoMediaListBorrowed,
     new_medium: *const nsACString,
 ) {
     let new_medium = unsafe { new_medium.as_ref().unwrap().as_str_unchecked() };
     let url_data = unsafe { dummy_url_data() };
-    let context = ParserContext::new_for_cssom(url_data, Some(CssRuleType::Media),
-                                               ParsingMode::DEFAULT,
-                                               QuirksMode::NoQuirks);
+    let context = ParserContext::new_for_cssom(
+        url_data,
+        Some(CssRuleType::Media),
+        ParsingMode::DEFAULT,
+        QuirksMode::NoQuirks,
+    );
     write_locked_arc(list, |list: &mut MediaList| {
         list.append_medium(&context, new_medium);
     })
 }
 
 #[no_mangle]
 pub extern "C" fn Servo_MediaList_DeleteMedium(
     list: RawServoMediaListBorrowed,
     old_medium: *const nsACString,
 ) -> bool {
     let old_medium = unsafe { old_medium.as_ref().unwrap().as_str_unchecked() };
     let url_data = unsafe { dummy_url_data() };
-    let context = ParserContext::new_for_cssom(url_data, Some(CssRuleType::Media),
-                                               ParsingMode::DEFAULT,
-                                               QuirksMode::NoQuirks);
+    let context = ParserContext::new_for_cssom(
+        url_data,
+        Some(CssRuleType::Media),
+        ParsingMode::DEFAULT,
+        QuirksMode::NoQuirks,
+    );
     write_locked_arc(list, |list: &mut MediaList| list.delete_medium(&context, old_medium))
 }
 
 macro_rules! get_longhand_from_id {
     ($id:expr) => {
         match PropertyId::from_nscsspropertyid($id) {
             Ok(PropertyId::Longhand(long)) => long,
             _ => {
@@ -3357,19 +3365,23 @@ pub extern "C" fn Servo_DeclarationBlock
     use style::properties::PropertyDeclaration;
     use style::properties::longhands::background_image::SpecifiedValue as BackgroundImage;
     use style::values::Either;
     use style::values::generics::image::Image;
     use style::values::specified::url::SpecifiedUrl;
 
     let url_data = unsafe { RefPtr::from_ptr_ref(&raw_extra_data) };
     let string = unsafe { (*value).to_string() };
-    let context = ParserContext::new(Origin::Author, url_data,
-                                     Some(CssRuleType::Style), ParsingMode::DEFAULT,
-                                     QuirksMode::NoQuirks);
+    let context = ParserContext::new(
+        Origin::Author,
+        url_data,
+        Some(CssRuleType::Style),
+        ParsingMode::DEFAULT,
+        QuirksMode::NoQuirks,
+    );
     if let Ok(mut url) = SpecifiedUrl::parse_from_string(string.into(), &context) {
         url.build_image_value();
         let decl = PropertyDeclaration::BackgroundImage(BackgroundImage(
             vec![Either::Second(Image::Url(url))]
         ));
         write_locked_arc(declarations, |decls: &mut PropertyDeclarationBlock| {
             decls.push(decl, Importance::Normal, DeclarationSource::CssOm);
         })
@@ -3415,23 +3427,23 @@ pub extern "C" fn Servo_CSSSupports(cond
     let condition = unsafe { cond.as_ref().unwrap().as_str_unchecked() };
     let mut input = ParserInput::new(&condition);
     let mut input = Parser::new(&mut input);
     let cond = input.parse_entirely(|i| parse_condition_or_declaration(i));
     if let Ok(cond) = cond {
         let url_data = unsafe { dummy_url_data() };
         // NOTE(emilio): The supports API is not associated to any stylesheet,
         // so the fact that there are no namespace map here is fine.
-        let context =
-            ParserContext::new_for_cssom(
-                url_data,
-                Some(CssRuleType::Style),
-                ParsingMode::DEFAULT,
-                QuirksMode::NoQuirks,
-            );
+        let context = ParserContext::new_for_cssom(
+            url_data,
+            Some(CssRuleType::Style),
+            ParsingMode::DEFAULT,
+            QuirksMode::NoQuirks,
+        );
+
         cond.eval(&context)
     } else {
         false
     }
 }
 
 #[no_mangle]
 pub extern "C" fn Servo_NoteExplicitHints(
