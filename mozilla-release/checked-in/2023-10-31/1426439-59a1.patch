# HG changeset patch
# User Steve Fink <sfink@mozilla.com>
# Date 1511223001 28800
# Node ID 47eabc18d2945fa94e0f5aed76c877b9ad3427dd
# Parent  3cfe2c39733f0bac96c2b01985fa30259c49cb63
Bug 1426439 - Allow stack reporting for timed out tests, r=jonco

diff --git a/js/src/tests/lib/tasks_unix.py b/js/src/tests/lib/tasks_unix.py
--- a/js/src/tests/lib/tasks_unix.py
+++ b/js/src/tests/lib/tasks_unix.py
@@ -1,13 +1,13 @@
 # A unix-oriented process dispatcher.  Uses a single thread with select and
 # waitpid to dispatch tasks.  This avoids several deadlocks that are possible
 # with fork/exec + threads + Python.
 
-import errno, os, select, sys
+import errno, os, select, signal, sys
 from datetime import datetime, timedelta
 from progressbar import ProgressBar
 from results import NullTestOutput, TestOutput, escape_cmdline
 
 class Task(object):
     def __init__(self, test, prefix, pid, stdout, stderr):
         self.test = test
         self.cmd = test.get_command(prefix)
@@ -126,24 +126,26 @@ def remove_task(tasks, pid):
         raise KeyError("No such pid: {}".format(pid))
 
     out = tasks[index]
     tasks.pop(index)
     return out
 
 def timed_out(task, timeout):
     """
-    Return True if the given task has been running for longer than |timeout|.
-    |timeout| may be falsy, indicating an infinite timeout (in which case
-    timed_out always returns False).
+    Return a timedelta with the amount we are overdue, or False if the timeout
+    has not yet been reached (or timeout is falsy, indicating there is no
+    timeout.)
     """
-    if timeout:
-        now = datetime.now()
-        return (now - task.start) > timedelta(seconds=timeout)
-    return False
+    if not timeout:
+        return False
+
+    elapsed = datetime.now() - task.start
+    over = elapsed - timedelta(seconds=timeout)
+    return over if over.total_seconds() > 0 else False
 
 def reap_zombies(tasks, timeout):
     """
     Search for children of this process that have finished. If they are tasks,
     then this routine will clean up the child. This method returns a new task
     list that has had the ended tasks removed, followed by the list of finished
     tasks.
     """
@@ -176,21 +178,27 @@ def reap_zombies(tasks, timeout):
                 ''.join(ended.err),
                 returncode,
                 (datetime.now() - ended.start).total_seconds(),
                 timed_out(ended, timeout)))
     return tasks, finished
 
 def kill_undead(tasks, timeout):
     """
-    Signal all children that are over the given timeout.
+    Signal all children that are over the given timeout. Use SIGABRT first to
+    generate a stack dump. If it still doesn't die for another 30 seconds, kill
+    with SIGKILL.
     """
     for task in tasks:
-        if timed_out(task, timeout):
-            os.kill(task.pid, 9)
+        over = timed_out(task, timeout)
+        if over:
+            if over.total_seconds() < 30:
+                os.kill(task.pid, signal.SIGABRT)
+            else:
+                os.kill(task.pid, signal.SIGKILL)
 
 def run_all_tests(tests, prefix, pb, options):
     # Copy and reverse for fast pop off end.
     tests = list(tests)
     tests = tests[:]
     tests.reverse()
 
     # The set of currently running tests.

