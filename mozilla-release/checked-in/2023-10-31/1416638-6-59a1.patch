# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1510646762 -39600
# Node ID 46af1340907eb154c9dd63bb13b1d250b48ca1bb
# Parent  42e61258b09633f871e61e37eeaa2dd7c2b9fd1f
Bug 1416638 - Move a couple of functions into Preferences. r=glandium.

This will allow other functions to be moved into Preferences and be marked as
`private` in subsequent patches.

The patch also renames SetPrefValue() as SetValueFromDom(), because that's a
clearer name.

MozReview-Commit-ID: CB1xmPSmac6

diff --git a/modules/libpref/Preferences.cpp b/modules/libpref/Preferences.cpp
--- a/modules/libpref/Preferences.cpp
+++ b/modules/libpref/Preferences.cpp
@@ -447,38 +447,16 @@ PREF_SetBoolPref(const char* aPrefName, 
 {
   PrefValue pref;
   pref.mBoolVal = aValue;
 
   return pref_SetPref(
     aPrefName, pref, PrefType::Bool, aSetDefault ? kPrefSetDefault : 0);
 }
 
-static nsresult
-SetPrefValue(const char* aPrefName,
-             const dom::PrefValue& aValue,
-             PrefValueKind aKind)
-{
-  bool setDefault = (aKind == PrefValueKind::Default);
-
-  switch (aValue.type()) {
-    case dom::PrefValue::TnsCString:
-      return PREF_SetCStringPref(aPrefName, aValue.get_nsCString(), setDefault);
-
-    case dom::PrefValue::Tint32_t:
-      return PREF_SetIntPref(aPrefName, aValue.get_int32_t(), setDefault);
-
-    case dom::PrefValue::Tbool:
-      return PREF_SetBoolPref(aPrefName, aValue.get_bool(), setDefault);
-
-    default:
-      MOZ_CRASH();
-  }
-}
-
 static PrefSaveData
 pref_savePrefs()
 {
   PrefSaveData savedPrefs(gHashTable->EntryCount());
 
   for (auto iter = gHashTable->Iter(); !iter.Done(); iter.Next()) {
     auto pref = static_cast<PrefHashEntry*>(iter.Get());
 
@@ -3130,19 +3108,16 @@ Preferences::HandleDirty()
         PREF_DELAY_MS);
     }
   }
 }
 
 static nsresult
 openPrefFile(nsIFile* aFile);
 
-static Result<Ok, const char*>
-pref_InitInitialObjects();
-
 static nsresult
 pref_LoadPrefsInDirList(const char* aListId);
 
 static const char kTelemetryPref[] = "toolkit.telemetry.enabled";
 static const char kOldTelemetryPref[] = "toolkit.telemetry.enabledPreRelease";
 static const char kChannelPref[] = "app.update.channel";
 
 // clang-format off
@@ -3558,17 +3533,17 @@ Preferences::GetInstanceForService()
   }
 
   sPreferences = new Preferences();
 
   MOZ_ASSERT(!gHashTable);
   gHashTable = new PLDHashTable(
     &pref_HashTableOps, sizeof(PrefHashEntry), PREF_HASHTABLE_INITIAL_LENGTH);
 
-  Result<Ok, const char*> res = pref_InitInitialObjects();
+  Result<Ok, const char*> res = InitInitialObjects();
   if (res.isErr()) {
     sPreferences = nullptr;
     gCacheDataDesc = res.unwrapErr();
     return nullptr;
   }
 
   if (!XRE_IsParentProcess()) {
     MOZ_ASSERT(gInitPrefs);
@@ -3777,17 +3752,17 @@ Preferences::Observe(nsISupports* aSubje
     MOZ_ASSERT(!mDirty, "Preferences should not be dirty");
     mProfileShutdown = true;
 
   } else if (!strcmp(aTopic, "load-extension-defaults")) {
     pref_LoadPrefsInDirList(NS_EXT_PREFS_DEFAULTS_DIR_LIST);
 
   } else if (!nsCRT::strcmp(aTopic, "reload-default-prefs")) {
     // Reload the default prefs from file.
-    Unused << pref_InitInitialObjects();
+    Unused << InitInitialObjects();
 
   } else if (!nsCRT::strcmp(aTopic, "suspend_process_notification")) {
     // Our process is being suspended. The OS may wake our process later,
     // or it may kill the process. In case our process is going to be killed
     // from the suspended state, we save preferences before suspending.
     rv = SavePrefFileBlocking();
   }
 
@@ -3812,17 +3787,17 @@ Preferences::ResetPrefs()
 {
   ENSURE_MAIN_PROCESS("Preferences::ResetPrefs", "all prefs");
 
   NotifyServiceObservers(NS_PREFSERVICE_RESET_TOPIC_ID);
 
   gHashTable->ClearAndPrepareForLength(PREF_HASHTABLE_INITIAL_LENGTH);
   gPrefNameArena.Clear();
 
-  return pref_InitInitialObjects().isOk() ? NS_OK : NS_ERROR_FAILURE;
+  return InitInitialObjects().isOk() ? NS_OK : NS_ERROR_FAILURE;
 }
 
 NS_IMETHODIMP
 Preferences::ResetUserPrefs()
 {
   ENSURE_MAIN_PROCESS("Preferences::ResetUserPrefs", "all prefs");
 
   PREF_ClearAllUserPrefs();
@@ -3919,33 +3894,53 @@ ReadExtensionPrefs(nsIFile* aFile)
       PREF_ParseBuf(&ps, buffer, read);
     }
     PREF_FinalizeParseState(&ps);
   }
 
   return rv;
 }
 
+/* static */ nsresult
+Preferences::SetValueFromDom(const char* aPrefName,
+                             const dom::PrefValue& aValue,
+                             PrefValueKind aKind)
+{
+  switch (aValue.type()) {
+    case dom::PrefValue::TnsCString:
+      return Preferences::SetCString(aPrefName, aValue.get_nsCString(), aKind);
+
+    case dom::PrefValue::Tint32_t:
+      return Preferences::SetInt(aPrefName, aValue.get_int32_t(), aKind);
+
+    case dom::PrefValue::Tbool:
+      return Preferences::SetBool(aPrefName, aValue.get_bool(), aKind);
+
+    default:
+      MOZ_CRASH();
+  }
+}
+
 void
 Preferences::SetPreference(const PrefSetting& aPref)
 {
   const char* prefName = aPref.name().get();
   const dom::MaybePrefValue& defaultValue = aPref.defaultValue();
   const dom::MaybePrefValue& userValue = aPref.userValue();
 
   if (defaultValue.type() == dom::MaybePrefValue::TPrefValue) {
-    nsresult rv = SetPrefValue(
+    nsresult rv = SetValueFromDom(
       prefName, defaultValue.get_PrefValue(), PrefValueKind::Default);
     if (NS_FAILED(rv)) {
       return;
     }
   }
 
   if (userValue.type() == dom::MaybePrefValue::TPrefValue) {
-    SetPrefValue(prefName, userValue.get_PrefValue(), PrefValueKind::User);
+    SetValueFromDom(prefName, userValue.get_PrefValue(), PrefValueKind::User);
   } else {
     PREF_ClearUserPref(prefName);
   }
 
   // NB: we should never try to clear a default value, that doesn't
   // make sense
 }
 
@@ -4425,18 +4420,18 @@ pref_ReadPrefFromJar(nsZipArchive* aJarR
   PREF_ParseBuf(&ps, manifest.get(), manifest.Length());
   PREF_FinalizeParseState(&ps);
 
   return NS_OK;
 }
 
 // Initialize default preference JavaScript buffers from appropriate TEXT
 // resources.
-static Result<Ok, const char*>
-pref_InitInitialObjects()
+/* static */ Result<Ok, const char*>
+Preferences::InitInitialObjects()
 {
   // In the omni.jar case, we load the following prefs:
   // - jar:$gre/omni.jar!/greprefs.js
   // - jar:$gre/omni.jar!/defaults/pref/*.js
   //
   // In the non-omni.jar case, we load:
   // - $gre/greprefs.js
   //
diff --git a/modules/libpref/Preferences.h b/modules/libpref/Preferences.h
--- a/modules/libpref/Preferences.h
+++ b/modules/libpref/Preferences.h
@@ -8,16 +8,17 @@
 #define mozilla_Preferences_h
 
 #ifndef MOZILLA_INTERNAL_API
 #error "This header is only usable from within libxul (MOZILLA_INTERNAL_API)."
 #endif
 
 #include "mozilla/Atomics.h"
 #include "mozilla/MemoryReporting.h"
+#include "mozilla/Result.h"
 #include "mozilla/StaticPtr.h"
 #include "nsCOMPtr.h"
 #include "nsIObserver.h"
 #include "nsIPrefBranch.h"
 #include "nsIPrefService.h"
 #include "nsTArray.h"
 #include "nsWeakReference.h"
 
@@ -43,16 +44,17 @@ enum pref_initPhase
   do {                                                                         \
   } while (0)
 #endif
 
 namespace mozilla {
 
 namespace dom {
 class PrefSetting;
+class PrefValue;
 } // namespace dom
 
 enum class PrefValueKind : bool
 {
   Default,
   User
 };
 
@@ -358,16 +360,22 @@ public:
   // Public so the ValueObserver classes can use it.
   enum MatchKind
   {
     PrefixMatch,
     ExactMatch,
   };
 
 private:
+  static mozilla::Result<mozilla::Ok, const char*> InitInitialObjects();
+
+  static nsresult SetValueFromDom(const char* aPrefName,
+                                  const dom::PrefValue& aValue,
+                                  PrefValueKind aKind);
+
   static nsresult RegisterCallback(PrefChangedFunc aCallback,
                                    const char* aPref,
                                    void* aClosure,
                                    MatchKind aMatchKind);
   static nsresult UnregisterCallback(PrefChangedFunc aCallback,
                                      const char* aPref,
                                      void* aClosure,
                                      MatchKind aMatchKind);
