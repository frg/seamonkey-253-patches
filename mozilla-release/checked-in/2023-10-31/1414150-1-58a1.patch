# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1510043658 -39600
#      Tue Nov 07 19:34:18 2017 +1100
# Node ID b84d87e9fa100abe4f3aa073d2e169d3392d9b4c
# Parent  5a03c3391998893bb536c18f2ebfaa5df3d58b0e
Bug 1414150 - Remove the "memory.free_dirty_pages" pref. r=glandium.

This was originally added for b2g, where the pref had a different value.
Bug 1398033 enabled it everywhere.

diff --git a/dom/ipc/ContentPrefs.cpp b/dom/ipc/ContentPrefs.cpp
--- a/dom/ipc/ContentPrefs.cpp
+++ b/dom/ipc/ContentPrefs.cpp
@@ -179,17 +179,16 @@ const char* mozilla::dom::ContentPrefs::
   "media.webspeech.test.enable",
   "media.webspeech.test.fake_fsm_events",
   "media.webspeech.test.fake_recognition_service",
   "media.wmf.allow-unsupported-resolutions",
   "media.wmf.decoder.thread-count",
   "media.wmf.enabled",
   "media.wmf.skip-blacklist",
   "media.wmf.vp9.enabled",
-  "memory.free_dirty_pages",
   "memory.low_commit_space_threshold_mb",
   "memory.low_memory_notification_interval_ms",
   "memory.low_physical_memory_threshold_mb",
   "memory.low_virtual_mem_threshold_mb",
   "network.IDN.blacklist_chars",
   "network.IDN.restriction_profile",
   "network.IDN.use_whitelist",
   "network.IDN_show_punycode",
diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -5304,20 +5304,16 @@ pref("memory.low_physical_memory_thresho
 pref("memory.low_memory_notification_interval_ms", 10000);
 #endif
 
 // How long must we wait before declaring that a window is a "ghost" (i.e., a
 // likely leak)?  This should be longer than it usually takes for an eligible
 // window to be collected via the GC/CC.
 pref("memory.ghost_window_timeout_seconds", 60);
 
-// On memory pressure, release dirty but unused pages held by jemalloc
-// back to the system.
-pref("memory.free_dirty_pages", true);
-
 // Don't dump memory reports on OOM, by default.
 pref("memory.dump_reports_on_oom", false);
 
 // Number of stack frames to capture in createObjectURL for about:memory.
 pref("memory.blob_report.stack_frames", 0);
 
 // Disable idle observer fuzz, because only privileged content can access idle
 // observers (bug 780507).
diff --git a/xpcom/base/AvailableMemoryTracker.cpp b/xpcom/base/AvailableMemoryTracker.cpp
--- a/xpcom/base/AvailableMemoryTracker.cpp
+++ b/xpcom/base/AvailableMemoryTracker.cpp
@@ -321,58 +321,48 @@ class nsMemoryPressureWatcher final : pu
 {
   ~nsMemoryPressureWatcher() {}
 
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIOBSERVER
 
   void Init();
-
-private:
-  static bool sFreeDirtyPages;
 };
 
 NS_IMPL_ISUPPORTS(nsMemoryPressureWatcher, nsIObserver)
 
-bool nsMemoryPressureWatcher::sFreeDirtyPages = true;
-
 /**
  * Initialize and subscribe to the memory-pressure events. We subscribe to the
  * observer service in this method and not in the constructor because we need
  * to hold a strong reference to 'this' before calling the observer service.
  */
 void
 nsMemoryPressureWatcher::Init()
 {
   nsCOMPtr<nsIObserverService> os = services::GetObserverService();
 
   if (os) {
     os->AddObserver(this, "memory-pressure", /* ownsWeak */ false);
   }
-
-  Preferences::AddBoolVarCache(&sFreeDirtyPages, "memory.free_dirty_pages",
-                               true);
 }
 
 /**
  * Reacts to all types of memory-pressure events, launches a runnable to
  * free dirty pages held by jemalloc.
  */
 NS_IMETHODIMP
 nsMemoryPressureWatcher::Observe(nsISupports* aSubject, const char* aTopic,
                                  const char16_t* aData)
 {
   MOZ_ASSERT(!strcmp(aTopic, "memory-pressure"), "Unknown topic");
 
-  if (sFreeDirtyPages) {
-    nsCOMPtr<nsIRunnable> runnable = new nsJemallocFreeDirtyPagesRunnable();
+  nsCOMPtr<nsIRunnable> runnable = new nsJemallocFreeDirtyPagesRunnable();
 
-    NS_DispatchToMainThread(runnable);
-  }
+  NS_DispatchToMainThread(runnable);
 
   return NS_OK;
 }
 
 } // namespace
 
 namespace mozilla {
 namespace AvailableMemoryTracker {
