# HG changeset patch
# User Axel Hecht <axel@pike.org>
# Date 1501859952 -7200
# Node ID 0abbf75bd0ecfa99ab4386f551a622983f5f27ea
# Parent  48140fc1c8f76db9a1bb2a88fd277b3d4de62116
bug 1385227, use proper make steps to put l10n repacks in sequence, r=gps

This drops the abuse of make dependencies to factor our steps
in l10n repacks and language packs.
It's replaced with clobbers, libs, and the packaging, which
can be shared between installers and the language pack.

MozReview-Commit-ID: 2vEHp0QogXQ

diff --git a/browser/locales/Makefile.in b/browser/locales/Makefile.in
--- a/browser/locales/Makefile.in
+++ b/browser/locales/Makefile.in
@@ -14,18 +14,16 @@ SUBMAKEFILES += \
 # This makefile uses variable overrides from the libs-% target to
 # build non-default locales to non-default dist/ locations. Be aware!
 
 PWD := $(CURDIR)
 
 # These are defaulted to be compatible with the files the wget-en-US target
 # pulls. You may override them if you provide your own files.
 ZIP_IN ?= $(ABS_DIST)/$(PACKAGE)
-WIN32_INSTALLER_IN ?= $(ABS_DIST)/$(PKG_INST_PATH)$(PKG_INST_BASENAME).exe
-RETRIEVE_WINDOWS_INSTALLER = 1
 
 MOZ_LANGPACK_EID=langpack-$(AB_CD)@firefox.mozilla.org
 # For Nightly, we know where to get the builds from to do local repacks
 ifdef NIGHTLY_BUILD
 export EN_US_BINARY_URL ?= https://archive.mozilla.org/pub/firefox/nightly/latest-mozilla-central
 endif
 
 ifneq (,$(filter cocoa,$(MOZ_WIDGET_TOOLKIT)))
@@ -33,31 +31,16 @@ MOZ_PKG_MAC_DSSTORE=$(ABS_DIST)/branding
 MOZ_PKG_MAC_BACKGROUND=$(ABS_DIST)/branding/background.png
 MOZ_PKG_MAC_ICON=$(ABS_DIST)/branding/disk.icns
 MOZ_PKG_MAC_EXTRA=--symlink '/Applications:/ '
 endif
 
 MOZ_SFX_PACKAGE=$(topsrcdir)/other-licenses/7zstub/firefox/7zSD.sfx
 MOZ_INSTALLER_PATH=$(topsrcdir)/browser/installer/windows
 
-ifeq (WINNT,$(OS_ARCH))
-UNINSTALLER_PACKAGE_HOOK = $(RM) -r $(STAGEDIST)/uninstall; \
-    $(NSINSTALL) -D $(STAGEDIST)/uninstall; \
-    cp ../installer/windows/l10ngen/helper.exe $(STAGEDIST)/uninstall; \
-    $(RM) $(ABS_DIST)/l10n-stage/setup.exe; \
-    cp ../installer/windows/l10ngen/setup.exe $(ABS_DIST)/l10n-stage; \
-    $(NULL)
-
-STUB_HOOK = $(NSINSTALL) -D '$(ABS_DIST)/$(PKG_INST_PATH)'; \
-    $(RM) '$(ABS_DIST)/$(PKG_INST_PATH)$(PKG_STUB_BASENAME).exe'; \
-    cp ../installer/windows/l10ngen/stub.exe '$(ABS_DIST)/$(PKG_INST_PATH)$(PKG_STUB_BASENAME).exe'; \
-    chmod 0755 '$(ABS_DIST)/$(PKG_INST_PATH)$(PKG_STUB_BASENAME).exe'; \
-    $(NULL)
-endif
-
 SEARCHPLUGINS_FILENAMES := $(shell $(call py_action,output_searchplugins_list,$(srcdir)/search/list.json $(AB_CD)))
 SEARCHPLUGINS_PATH := .deps/generated_$(AB_CD)
 SEARCHPLUGINS_TARGET := libs searchplugins
 SEARCHPLUGINS := $(foreach plugin,$(addsuffix .xml,$(SEARCHPLUGINS_FILENAMES)),$(or $(wildcard $(srcdir)/searchplugins/$(plugin)),$(warning Missing searchplugin: $(plugin))))
 # Some locale-specific search plugins may have preprocessor directives, but the
 # default en-US ones do not.
 SEARCHPLUGINS_FLAGS := --silence-missing-directive-warnings
 PP_TARGETS += SEARCHPLUGINS
@@ -122,47 +105,52 @@ endif
 	@$(MAKE) -C ../../intl/locales chrome AB_CD=$*
 	@$(MAKE) -C ../../devtools/client/locales chrome AB_CD=$*
 	@$(MAKE) chrome AB_CD=$*
 	@$(MAKE) -C $(DEPTH)/$(MOZ_BRANDING_DIRECTORY)/locales chrome AB_CD=$*
 ifdef NIGHTLY_BUILD
 	@$(MAKE) -C ../extensions/webcompat-reporter/locales chrome AB_CD=$*
 endif
 
-repackage-win32-installer: WIN32_INSTALLER_OUT=$(ABS_DIST)/$(PKG_INST_PATH)$(PKG_INST_BASENAME).exe
-repackage-win32-installer: $(call ESCAPE_WILDCARD,$(WIN32_INSTALLER_IN)) $(SUBMAKEFILES) libs-$(AB_CD)
-	@echo 'Repackaging $(WIN32_INSTALLER_IN) into $(WIN32_INSTALLER_OUT).'
-	$(MAKE) -C $(DEPTH)/$(MOZ_BRANDING_DIRECTORY) export
+package-win32-installer: WIN32_INSTALLER_OUT=$(ABS_DIST)/$(PKG_INST_PATH)$(PKG_INST_BASENAME).exe
+package-win32-installer: $(SUBMAKEFILES)
+	@echo 'Packaging $(WIN32_INSTALLER_OUT).'
 	$(MAKE) -C ../installer/windows CONFIG_DIR=l10ngen l10ngen/setup.exe l10ngen/7zSD.sfx
-	$(MAKE) repackage-zip \
-	  AB_CD=$(AB_CD) \
-	  MOZ_PKG_FORMAT=SFX7Z \
-	  ZIP_IN='$(WIN32_INSTALLER_IN)' \
-	  ZIP_OUT='$(WIN32_INSTALLER_OUT)' \
-	  SFX_HEADER='$(PWD)/../installer/windows/l10ngen/7zSD.sfx \
-	              $(topsrcdir)/browser/installer/windows/app.tag'
-
-ifeq (WINNT,$(OS_ARCH))
-repackage-win32-installer-%: unpack
-	@$(MAKE) repackage-win32-installer AB_CD=$* WIN32_INSTALLER_IN='$(WIN32_INSTALLER_IN)'
-
-repackage-zip-%: repackage-win32-installer-%
-else
-repackage-win32-installer-%: ;
+	$(RM) -r $(STAGEDIST)/uninstall
+	$(NSINSTALL) -D $(STAGEDIST)/uninstall
+	cp ../installer/windows/l10ngen/helper.exe $(STAGEDIST)/uninstall
+	$(RM) $(ABS_DIST)/l10n-stage/setup.exe
+	cp ../installer/windows/l10ngen/setup.exe $(ABS_DIST)/l10n-stage
+	$(NSINSTALL) -D '$(ABS_DIST)/$(PKG_INST_PATH)'
+	(cd $(DIST)/l10n-stage; \
+	  $(MAKE_PACKAGE))
+	mv -f '$(DIST)/l10n-stage/$(PACKAGE)' '$(WIN32_INSTALLER_OUT)'
+	if test -f '$(DIST)/l10n-stage/$(PACKAGE).asc'; then mv -f '$(DIST)/l10n-stage/$(PACKAGE).asc' '$(WIN32_INSTALLER_OUT).asc'; fi
+ifdef MOZ_STUB_INSTALLER
+	$(RM) '$(ABS_DIST)/$(PKG_INST_PATH)$(PKG_STUB_BASENAME).exe'
+	cp ../installer/windows/l10ngen/stub.exe '$(ABS_DIST)/$(PKG_INST_PATH)$(PKG_STUB_BASENAME).exe'
+	chmod 0755 '$(ABS_DIST)/$(PKG_INST_PATH)$(PKG_STUB_BASENAME).exe'
 endif
 
 
 langpack: langpack-$(AB_CD)
 
 # This is a generic target that will make a langpack, repack ZIP (+tarball)
 # builds, and repack an installer if applicable. It is called from the
 # tinderbox scripts. Alter it with caution.
 
 installers-%: IS_LANGUAGE_REPACK=1
-installers-%: clobber-% langpack-% repackage-win32-installer-% repackage-zip-%
+installers-%:
+	@$(MAKE) clobber-$*
+	@$(MAKE) libs-$*
+	@$(MAKE) package-langpack-$*
+	@$(MAKE) repackage-zip-$*
+ifeq (WINNT,$(OS_ARCH))
+	@$(MAKE) package-win32-installer AB_CD=$* MOZ_PKG_FORMAT=SFX7Z
+endif
 	@echo 'repackaging done'
 
 ifdef MOZ_UPDATER
 # Note that we want updater.ini to be in the top directory, not the browser/
 # subdirectory, because that's where the updater is installed and runs.
 libs:: $(call MERGE_FILE,updater/updater.ini) $(call mkdir_deps,$(DIST)/bin)
 ifeq ($(OS_ARCH),WINNT)
 	cat $< $(srcdir)/../installer/windows/nsis/updater_append.ini | \
@@ -192,11 +180,11 @@ l10n-check::
 	$(RM) -rf x-test
 	$(NSINSTALL) -D x-test/toolkit
 	echo '#define MOZ_LANG_TITLE Just testing' > x-test/toolkit/defines.inc
 	@# ZIP_IN='$(ZIP_IN)' will pass down the *current* value of ZIP_IN, and
 	@# we do the same for WIN32_INSTALLER_IN, which are based on MOZ_SIMPLE_PACKAGE_NAME
 	@# not being reset, overwriting the value they would get with MOZ_SIMPLE_PACKAGE_NAME
 	@# reset.
 	$(MAKE) installers-x-test L10NBASEDIR='$(PWD)' \
-	    ZIP_IN='$(ZIP_IN)' WIN32_INSTALLER_IN='$(WIN32_INSTALLER_IN)' MOZ_SIMPLE_PACKAGE_NAME=
+	    ZIP_IN='$(ZIP_IN)' MOZ_SIMPLE_PACKAGE_NAME=
 	$(PYTHON) $(topsrcdir)/toolkit/mozapps/installer/unpack.py $(DIST)/l10n-stage/$(MOZ_PKG_DIR)$(_RESPATH)
 	cd $(DIST)/l10n-stage && test $$(cat $(MOZ_PKG_DIR)$(_RESPATH)/update.locale) = x-test
diff --git a/toolkit/locales/l10n.mk b/toolkit/locales/l10n.mk
--- a/toolkit/locales/l10n.mk
+++ b/toolkit/locales/l10n.mk
@@ -106,32 +106,18 @@ ifeq ($(OS_ARCH), WINNT)
 else
 	rsync -rav --delete $(UNPACKED_INSTALLER)/ $(ABS_DIST)/l10n-stage
 endif
 
 # The path to the object dir for the mozilla-central build system,
 # may be overridden if necessary.
 MOZDEPTH ?= $(DEPTH)
 
-ifdef MOZ_MAKE_COMPLETE_MAR
-MAKE_COMPLETE_MAR = 1
-ifeq ($(OS_ARCH), WINNT)
-ifneq ($(MOZ_PKG_FORMAT), SFX7Z)
-MAKE_COMPLETE_MAR =
-endif
-endif
-endif
 repackage-zip: UNPACKAGE='$(ZIP_IN)'
-repackage-zip:  libs-$(AB_CD)
-# call a hook for apps to put their uninstall helper.exe into the package
-	$(UNINSTALLER_PACKAGE_HOOK)
-# call a hook for apps to build the stub installer
-ifdef MOZ_STUB_INSTALLER
-	$(STUB_HOOK)
-endif
+repackage-zip:
 	$(PYTHON) $(MOZILLA_DIR)/toolkit/mozapps/installer/l10n-repack.py $(STAGEDIST) $(DIST)/xpi-stage/locale-$(AB_CD) \
 		$(MOZ_PKG_EXTRAL10N) \
 		$(if $(filter omni,$(MOZ_PACKAGER_FORMAT)),$(if $(NON_OMNIJAR_FILES),--non-resource $(NON_OMNIJAR_FILES)))
 
 ifeq (cocoa,$(MOZ_WIDGET_TOOLKIT))
 ifneq (en,$(LPROJ_ROOT))
 	mv $(STAGEDIST)/en.lproj $(STAGEDIST)/$(LPROJ_ROOT).lproj
 endif
@@ -141,19 +127,19 @@ ifdef MOZ_CRASHREPORTER
 # located to the crash reporter bundle's Resources directory.
 	mv $(STAGEDIST)/crashreporter.app/Contents/Resources/crashreporter.ini \
 	  $(STAGEDIST)/../MacOS/crashreporter.app/Contents/Resources/crashreporter.ini
 	$(RM) -rf $(STAGEDIST)/crashreporter.app
 endif
 endif
 
 	$(NSINSTALL) -D $(DIST)/l10n-stage/$(PKG_PATH)
-	cd $(DIST)/l10n-stage; \
-	  $(MAKE_PACKAGE)
-ifdef MAKE_COMPLETE_MAR
+	(cd $(DIST)/l10n-stage; \
+	  $(MAKE_PACKAGE))
+ifdef MOZ_MAKE_COMPLETE_MAR
 	$(MAKE) -C $(MOZDEPTH)/tools/update-packaging full-update AB_CD=$(AB_CD) \
 	  PACKAGE_BASE_DIR='$(ABS_DIST)/l10n-stage'
 endif
 # packaging done, undo l10n stuff
 ifneq (en,$(LPROJ_ROOT))
 ifeq (cocoa,$(MOZ_WIDGET_TOOLKIT))
 	mv $(STAGEDIST)/$(LPROJ_ROOT).lproj $(STAGEDIST)/en.lproj
 endif
@@ -199,22 +185,28 @@ ifdef NIGHTLY_BUILD
 	@if  ! test -d $(L10NBASEDIR)/$(AB_CD) ; then \
 		$(NSINSTALL) -D $(L10NBASEDIR) ; \
 		$(L10N_CO) ; \
 	fi
 endif
 	$(RM) -rf $(REAL_LOCALE_MERGEDIR)
 	$(MOZILLA_DIR)/mach compare-locales --l10n-base $(L10NBASEDIR) --merge-dir $(REAL_LOCALE_MERGEDIR) $*
 
-langpack-%: LANGPACK_FILE=$(ABS_DIST)/$(PKG_LANGPACK_PATH)$(PKG_LANGPACK_BASENAME).xpi
-langpack-%: AB_CD=$*
-langpack-%: XPI_NAME=locale-$*
 langpack-%: IS_LANGUAGE_REPACK=1
 langpack-%: IS_LANGPACK=1
-langpack-%: libs-%
+langpack-%: AB_CD=$*
+langpack-%:
+	@echo 'Making langpack $(LANGPACK_FILE)'
+	@$(MAKE) libs-$(AB_CD)
+	@$(MAKE) package-langpack-$(AB_CD)
+
+package-langpack-%: LANGPACK_FILE=$(ABS_DIST)/$(PKG_LANGPACK_PATH)$(PKG_LANGPACK_BASENAME).xpi
+package-langpack-%: XPI_NAME=locale-$*
+package-langpack-%: AB_CD=$*
+package-langpack-%:
 	@echo 'Making langpack $(LANGPACK_FILE)'
 	$(NSINSTALL) -D $(DIST)/$(PKG_LANGPACK_PATH)
 	$(call py_action,preprocessor,$(DEFINES) $(ACDEFINES) \
 	  -DTK_DEFINES=$(TK_DEFINES) -DAPP_DEFINES=$(APP_DEFINES) $(MOZILLA_DIR)/toolkit/locales/generic/install.rdf -o $(DIST)/xpi-stage/$(XPI_NAME)/install.rdf)
 	$(call py_action,zip,-C $(DIST)/xpi-stage/locale-$(AB_CD) $(LANGPACK_FILE) install.rdf $(PKG_ZIP_DIRS) chrome.manifest)
 
 # This variable is to allow the wget-en-US target to know which ftp server to download from
 ifndef EN_US_BINARY_URL 
