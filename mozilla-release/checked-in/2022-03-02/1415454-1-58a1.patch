# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1509691720 -32400
#      Fri Nov 03 15:48:40 2017 +0900
# Node ID f64defe0d11edb2a76bb6cca295a7fd864542501
# Parent  f761b3c3d85b6084b9789ba419cda762f174ca1c
Bug 1415454 - Inline MallocBinEasy and MallocBinHard. r=njn

They are both only used once, are trivial wrappers, and even repeat the
same assertions.

diff --git a/memory/build/mozjemalloc.cpp b/memory/build/mozjemalloc.cpp
--- a/memory/build/mozjemalloc.cpp
+++ b/memory/build/mozjemalloc.cpp
@@ -1025,20 +1025,16 @@ private:
                    size_t aNewSize);
 
   void TrimRunTail(arena_chunk_t* aChunk,
                    arena_run_t* aRun,
                    size_t aOldSize,
                    size_t aNewSize,
                    bool dirty);
 
-  inline void* MallocBinEasy(arena_bin_t* aBin, arena_run_t* aRun);
-
-  void* MallocBinHard(arena_bin_t* aBin);
-
   arena_run_t* GetNonFullBinRun(arena_bin_t* aBin);
 
   inline void* MallocSmall(size_t aSize, bool aZero);
 
   void* MallocLarge(size_t aSize, bool aZero);
 
 public:
   inline void* Malloc(size_t aSize, bool aZero);
@@ -2937,46 +2933,16 @@ arena_t::GetNonFullBinRun(arena_bin_t* a
 #if defined(MOZ_DIAGNOSTIC_ASSERT_ENABLED)
   run->mMagic = ARENA_RUN_MAGIC;
 #endif
 
   aBin->mNumRuns++;
   return run;
 }
 
-// bin->mCurrentRun must have space available before this function is called.
-void*
-arena_t::MallocBinEasy(arena_bin_t* aBin, arena_run_t* aRun)
-{
-  void* ret;
-
-  MOZ_DIAGNOSTIC_ASSERT(aRun->mMagic == ARENA_RUN_MAGIC);
-  MOZ_DIAGNOSTIC_ASSERT(aRun->mNumFree > 0);
-
-  ret = arena_run_reg_alloc(aRun, aBin);
-  MOZ_DIAGNOSTIC_ASSERT(ret);
-  aRun->mNumFree--;
-
-  return ret;
-}
-
-// Re-fill aBin->mCurrentRun, then call arena_t::MallocBinEasy().
-void*
-arena_t::MallocBinHard(arena_bin_t* aBin)
-{
-  aBin->mCurrentRun = GetNonFullBinRun(aBin);
-  if (!aBin->mCurrentRun) {
-    return nullptr;
-  }
-  MOZ_DIAGNOSTIC_ASSERT(aBin->mCurrentRun->mMagic == ARENA_RUN_MAGIC);
-  MOZ_DIAGNOSTIC_ASSERT(aBin->mCurrentRun->mNumFree > 0);
-
-  return MallocBinEasy(aBin, aBin->mCurrentRun);
-}
-
 void
 arena_bin_t::Init(SizeClass aSizeClass)
 {
   size_t try_run_size;
   unsigned try_nregs, try_mask_nelms, try_reg0_offset;
   // Size of the run header, excluding mRegionsMask.
   static const size_t kFixedHeaderSize = offsetof(arena_run_t, mRegionsMask);
 
@@ -3075,22 +3041,28 @@ arena_t::MallocSmall(size_t aSize, bool 
       break;
     default:
       MOZ_MAKE_COMPILER_ASSUME_IS_UNREACHABLE("Unexpected size class type");
   }
   MOZ_DIAGNOSTIC_ASSERT(aSize == bin->mSizeClass);
 
   {
     MutexAutoLock lock(mLock);
-    if ((run = bin->mCurrentRun) && run->mNumFree > 0) {
-      ret = MallocBinEasy(bin, run);
-    } else {
-      ret = MallocBinHard(bin);
+    run = bin->mCurrentRun;
+    if (MOZ_UNLIKELY(!run || run->mNumFree == 0)) {
+      run = bin->mCurrentRun = GetNonFullBinRun(bin);
     }
-
+    if (MOZ_UNLIKELY(!run)) {
+      return nullptr;
+    }
+    MOZ_DIAGNOSTIC_ASSERT(run->mMagic == ARENA_RUN_MAGIC);
+    MOZ_DIAGNOSTIC_ASSERT(run->mNumFree > 0);
+    ret = arena_run_reg_alloc(run, bin);
+    MOZ_DIAGNOSTIC_ASSERT(ret);
+    run->mNumFree--;
     if (!ret) {
       return nullptr;
     }
 
     mStats.allocated_small += aSize;
   }
 
   if (aZero == false) {
