# HG changeset patch
# User Bobby Holley <bobbyholley@gmail.com>
# Date 1501099980 25200
#      Wed Jul 26 13:13:00 2017 -0700
# Node ID d01d9b48a0682da316f8bbc14422b34f9c9cb753
# Parent  8f91a57cc74dbce4fee808627666869cc898c434
Bug 1389347 - Hoist StyleNew{Children,Subtree} into their own paths. r=emilio

The buggy animation handling isn't a regression, since currently we pass
UnstyledChildrenOnly in those cases, which blocks the animation traversal
in Servo_TraverseSubtree.

In general I really wanted to handle these two paths together. But there's
enough broken with the NewChildren path that I wanted to scope the buginess
as tightly as possible. And I really need to separate the handling here from
StyleDocument() to make the restyle root stuff work.

MozReview-Commit-ID: 9F0mcQl7AAX

diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -918,31 +918,79 @@ ServoStyleSet::StyleDocument(ServoTraver
   return postTraversalRequired;
 }
 
 void
 ServoStyleSet::StyleNewSubtree(Element* aRoot)
 {
   MOZ_ASSERT(!aRoot->HasServoData(), "Should have called StyleNewChildren");
   PreTraverseSync();
+  AutoPrepareTraversal guard(this);
 
+  // Do the traversal. The snapshots will not be used.
+  const SnapshotTable& snapshots = Snapshots();
+  auto flags = ServoTraversalFlags::Empty;
   DebugOnly<bool> postTraversalRequired =
-    PrepareAndTraverseSubtree(aRoot, ServoTraversalFlags::Empty);
+    Servo_TraverseSubtree(aRoot, mRawSet.get(), &snapshots, flags);
   MOZ_ASSERT(!postTraversalRequired);
+
+  // Annoyingly, the newly-styled content may have animations that need
+  // starting, which requires traversing them again. Mark the elements
+  // that need animation processing, then do a forgetful traversal to
+  // update the styles and clear the animation bits.
+  if (mPresContext->EffectCompositor()->PreTraverseInSubtree(flags, aRoot)) {
+    postTraversalRequired =
+      Servo_TraverseSubtree(aRoot, mRawSet.get(), &snapshots,
+                            ServoTraversalFlags::AnimationOnly |
+                            ServoTraversalFlags::Forgetful |
+                            ServoTraversalFlags::ClearAnimationOnlyDirtyDescendants);
+    MOZ_ASSERT(!postTraversalRequired);
+  }
 }
 
 void
 ServoStyleSet::StyleNewChildren(Element* aParent)
 {
   MOZ_ASSERT(aParent->HasServoData(), "Should have called StyleNewSubtree");
   PreTraverseSync();
+  AutoPrepareTraversal guard(this);
 
-  PrepareAndTraverseSubtree(aParent, ServoTraversalFlags::UnstyledChildrenOnly);
-  // We can't assert that Servo_TraverseSubtree returns false, since aParent
-  // or some of its other children might have pending restyles.
+  // Implementing StyleNewChildren correctly is very annoying, for two reasons:
+  // (1) We have to tiptoe around existing invalidations in the tree. In rare
+  //     cases Gecko calls into the frame constructor with pending invalidations,
+  //     and in other rare cases the frame constructor needs to perform
+  //     synchronous styling rather than using the normal lazy frame
+  //     construction mechanism. If both of these cases happen together, then we
+  //     get an |aParent| with dirty style and/or dirty descendants, which we
+  //     can't process right now because we're not set up to update the frames
+  //     and process the change hints. We handle this case by passing the
+  //     UnstyledOnly flag to servo.
+  // (2) We don't have a good way to handle animations. When styling unstyled
+  //     content, a followup animation traversal may be required (for example
+  //     to change the transition style from the after-change style we used in
+  //     the animation cascade to the timeline-correct style). But once we do
+  //     the initial styling, we don't have a good way to distinguish the new
+  //     content and scope our animation processing to that. We should handle
+  //     this somehow, but for now we just don't do the followup animation
+  //     traversal, which is buggy.
+
+  // Set ourselves up to find the children by marking the parent as having
+  // dirty descendants.
+  bool hadDirtyDescendants = aParent->HasDirtyDescendantsForServo();
+  aParent->SetHasDirtyDescendantsForServo();
+
+  // Do the traversal. The snapshots will be ignored.
+  const SnapshotTable& snapshots = Snapshots();
+  Servo_TraverseSubtree(aParent, mRawSet.get(), &snapshots,
+                        ServoTraversalFlags::UnstyledOnly);
+
+  // Restore the old state of the dirty descendants bit.
+  if (!hadDirtyDescendants) {
+    aParent->UnsetHasDirtyDescendantsForServo();
+  }
 }
 
 void
 ServoStyleSet::StyleNewlyBoundElement(Element* aElement)
 {
   // In general the element is always styled by the time we're applying XBL
   // bindings, because we need to style the element to know what the binding
   // URI is. However, programmatic consumers of the XBL service (like the
diff --git a/layout/style/ServoTypes.h b/layout/style/ServoTypes.h
--- a/layout/style/ServoTypes.h
+++ b/layout/style/ServoTypes.h
@@ -54,18 +54,19 @@ enum class LazyComputeBehavior {
 
 // Various flags for the servo traversal.
 enum class ServoTraversalFlags : uint32_t {
   Empty = 0,
   // Perform animation processing but not regular styling.
   AnimationOnly = 1 << 0,
   // Traverses as normal mode but tries to update all CSS animations.
   ForCSSRuleChanges = 1 << 1,
-  // Traverse only unstyled children of the root (and their descendants).
-  UnstyledChildrenOnly = 1 << 2,
+  // Styles unstyled elements, but does not handle invalidations on
+  // already-styled elements.
+  UnstyledOnly = 1 << 2,
   // A forgetful traversal ignores the previous state of the frame tree, and
   // thus does not compute damage or maintain other state describing the styles
   // pre-traversal. A forgetful traversal is usually the right thing if you
   // aren't going to do a post-traversal.
   Forgetful = 1 << 3,
   // Actively seeks out and clears change hints that may have been posted into
   // the tree. Nonsensical without also passing Forgetful.
   AggressivelyForgetful = 1 << 4,
