# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1511538742 0
# Node ID ebe3e6a61f6772d6fce2ad5df87e3c8329cec919
# Parent  f5a0150be3866440dfe3a50d0dc1f4f5808f6dbc
Bug 1420412 - Use a single slot to store the module environment record r=anba

diff --git a/js/src/builtin/Module.js b/js/src/builtin/Module.js
--- a/js/src/builtin/Module.js
+++ b/js/src/builtin/Module.js
@@ -237,18 +237,21 @@ function GetModuleEnvironment(module)
 {
     assert(IsModule(module), "Non-module passed to GetModuleEnvironment");
 
     // Check for a previous failed attempt to instantiate this module. This can
     // only happen due to a bug in the module loader.
     if (module.status === MODULE_STATUS_ERRORED)
         ThrowInternalError(JSMSG_MODULE_INSTANTIATE_FAILED, module.status);
 
+    assert(module.status >= MODULE_STATUS_INSTANTIATING,
+           "Attempt to access module environement before instantation");
+
     let env = UnsafeGetReservedSlot(module, MODULE_OBJECT_ENVIRONMENT_SLOT);
-    assert(env === undefined || IsModuleEnvironment(env),
+    assert(IsModuleEnvironment(env),
            "Module environment slot contains unexpected value");
 
     return env;
 }
 
 function RecordModuleError(module, error)
 {
     // Set the module's status to 'errored' to indicate a failed module
@@ -438,17 +441,17 @@ function ModuleDeclarationEnvironmentSet
                "Unexpected failure to resolve export in ModuleDeclarationEnvironmentSetup");
         if (!resolution.resolved) {
             return ResolutionError(resolution, "indirectExport", e.exportName,
                                    e.lineNumber, e.columnNumber);
         }
     }
 
     // Steps 5-6
-    CreateModuleEnvironment(module);
+    // Note that we have already created the environment by this point.
     let env = GetModuleEnvironment(module);
 
     // Step 8
     let importEntries = module.importEntries;
     for (let i = 0; i < importEntries.length; i++) {
         let imp = importEntries[i];
         let importedModule = CallModuleResolveHook(module, imp.moduleRequest,
                                                    MODULE_STATUS_INSTANTIATING);
diff --git a/js/src/builtin/ModuleObject.cpp b/js/src/builtin/ModuleObject.cpp
--- a/js/src/builtin/ModuleObject.cpp
+++ b/js/src/builtin/ModuleObject.cpp
@@ -726,26 +726,34 @@ ModuleObject::finalize(js::FreeOp* fop, 
     if (self->hasImportBindings())
         fop->delete_(&self->importBindings());
     if (IndirectBindingMap* bindings = self->namespaceBindings())
         fop->delete_(bindings);
     if (FunctionDeclarationVector* funDecls = self->functionDeclarations())
         fop->delete_(funDecls);
 }
 
+ModuleEnvironmentObject&
+ModuleObject::initialEnvironment() const
+{
+    Value value = getReservedSlot(EnvironmentSlot);
+    return value.toObject().as<ModuleEnvironmentObject>();
+}
+
 ModuleEnvironmentObject*
 ModuleObject::environment() const
 {
     MOZ_ASSERT(status() != MODULE_STATUS_ERRORED);
 
-    Value value = getReservedSlot(EnvironmentSlot);
-    if (value.isUndefined())
+    // According to the spec the environment record is created during
+    // instantiation, but we create it earlier than that.
+    if (status() < MODULE_STATUS_INSTANTIATED)
         return nullptr;
 
-    return &value.toObject().as<ModuleEnvironmentObject>();
+    return &initialEnvironment();
 }
 
 bool
 ModuleObject::hasImportBindings() const
 {
     // Import bindings may not be present if we hit OOM in initialization.
     return !getReservedSlot(ImportBindingsSlot).isUndefined();
 }
@@ -800,17 +808,17 @@ ModuleObject::init(HandleScript script)
 {
     initReservedSlot(ScriptSlot, PrivateValue(script));
     initReservedSlot(StatusSlot, Int32Value(MODULE_STATUS_ERRORED));
 }
 
 void
 ModuleObject::setInitialEnvironment(HandleModuleEnvironmentObject initialEnvironment)
 {
-    initReservedSlot(InitialEnvironmentSlot, ObjectValue(*initialEnvironment));
+    initReservedSlot(EnvironmentSlot, ObjectValue(*initialEnvironment));
 }
 
 void
 ModuleObject::initImportExportData(HandleArrayObject requestedModules,
                                    HandleArrayObject importEntries,
                                    HandleArrayObject localExportEntries,
                                    HandleArrayObject indirectExportEntries,
                                    HandleArrayObject starExportEntries)
@@ -940,22 +948,16 @@ ModuleObject::hostDefinedField() const
 }
 
 void
 ModuleObject::setHostDefinedField(const JS::Value& value)
 {
     setReservedSlot(HostDefinedSlot, value);
 }
 
-ModuleEnvironmentObject&
-ModuleObject::initialEnvironment() const
-{
-    return getReservedSlot(InitialEnvironmentSlot).toObject().as<ModuleEnvironmentObject>();
-}
-
 Scope*
 ModuleObject::enclosingScope() const
 {
     return script()->enclosingScope();
 }
 
 /* static */ void
 ModuleObject::trace(JSTracer* trc, JSObject* obj)
@@ -971,42 +973,33 @@ ModuleObject::trace(JSTracer* trc, JSObj
         module.importBindings().trace(trc);
     if (IndirectBindingMap* bindings = module.namespaceBindings())
         bindings->trace(trc);
 
     if (FunctionDeclarationVector* funDecls = module.functionDeclarations())
         funDecls->trace(trc);
 }
 
-void
-ModuleObject::createEnvironment()
-{
-    // The environment has already been created, we just neet to set it in the
-    // right slot.
-    MOZ_ASSERT(!getReservedSlot(InitialEnvironmentSlot).isUndefined());
-    MOZ_ASSERT(getReservedSlot(EnvironmentSlot).isUndefined());
-    setReservedSlot(EnvironmentSlot, getReservedSlot(InitialEnvironmentSlot));
-}
-
 bool
 ModuleObject::noteFunctionDeclaration(JSContext* cx, HandleAtom name, HandleFunction fun)
 {
     FunctionDeclarationVector* funDecls = functionDeclarations();
     if (!funDecls->emplaceBack(name, fun)) {
         ReportOutOfMemory(cx);
         return false;
     }
 
     return true;
 }
 
 /* static */ bool
 ModuleObject::instantiateFunctionDeclarations(JSContext* cx, HandleModuleObject self)
 {
 #ifdef DEBUG
+    MOZ_ASSERT(self->status() == MODULE_STATUS_INSTANTIATING);
     if (!AssertFrozen(cx, self))
         return false;
 #endif
 
     FunctionDeclarationVector* funDecls = self->functionDeclarations();
     if (!funDecls) {
         JS_ReportErrorASCII(cx, "Module function declarations have already been instantiated");
         return false;
@@ -1040,16 +1033,17 @@ ModuleObject::instantiateFunctionDeclara
     self->setReservedSlot(FunctionDeclarationsSlot, UndefinedValue());
     return true;
 }
 
 /* static */ bool
 ModuleObject::execute(JSContext* cx, HandleModuleObject self, MutableHandleValue rval)
 {
 #ifdef DEBUG
+    MOZ_ASSERT(self->status() == MODULE_STATUS_EVALUATING);
     if (!AssertFrozen(cx, self))
         return false;
 #endif
 
     RootedScript script(cx, self->script());
     RootedModuleEnvironmentObject scope(cx, self->environment());
     if (!scope) {
         JS_ReportErrorASCII(cx, "Module declarations have not yet been instantiated");
diff --git a/js/src/builtin/ModuleObject.h b/js/src/builtin/ModuleObject.h
--- a/js/src/builtin/ModuleObject.h
+++ b/js/src/builtin/ModuleObject.h
@@ -235,17 +235,16 @@ using FunctionDeclarationVector = GCVect
 using ModuleStatus = int32_t;
 
 class ModuleObject : public NativeObject
 {
   public:
     enum ModuleSlot
     {
         ScriptSlot = 0,
-        InitialEnvironmentSlot,
         EnvironmentSlot,
         NamespaceSlot,
         StatusSlot,
         ErrorSlot,
         HostDefinedSlot,
         RequestedModulesSlot,
         ImportEntriesSlot,
         LocalExportEntriesSlot,
@@ -306,19 +305,16 @@ class ModuleObject : public NativeObject
     JSObject* namespaceExports();
     IndirectBindingMap* namespaceBindings();
 
     static bool Instantiate(JSContext* cx, HandleModuleObject self);
     static bool Evaluate(JSContext* cx, HandleModuleObject self);
 
     void setHostDefinedField(const JS::Value& value);
 
-    // For intrinsic_CreateModuleEnvironment.
-    void createEnvironment();
-
     // For BytecodeEmitter.
     bool noteFunctionDeclaration(JSContext* cx, HandleAtom name, HandleFunction fun);
 
     // For intrinsic_InstantiateModuleFunctionDeclarations.
     static bool instantiateFunctionDeclarations(JSContext* cx, HandleModuleObject self);
 
     // For intrinsic_ExecuteModule.
     static bool execute(JSContext* cx, HandleModuleObject self, MutableHandleValue rval);
diff --git a/js/src/builtin/SelfHostingDefines.h b/js/src/builtin/SelfHostingDefines.h
--- a/js/src/builtin/SelfHostingDefines.h
+++ b/js/src/builtin/SelfHostingDefines.h
@@ -88,21 +88,21 @@
 #define REGEXP_FLAGS_SLOT 2
 
 #define REGEXP_IGNORECASE_FLAG  0x01
 #define REGEXP_GLOBAL_FLAG      0x02
 #define REGEXP_MULTILINE_FLAG   0x04
 #define REGEXP_STICKY_FLAG      0x08
 #define REGEXP_UNICODE_FLAG     0x10
 
-#define MODULE_OBJECT_ENVIRONMENT_SLOT        2
-#define MODULE_OBJECT_STATUS_SLOT             4
-#define MODULE_OBJECT_ERROR_SLOT              5
-#define MODULE_OBJECT_DFS_INDEX_SLOT          16
-#define MODULE_OBJECT_DFS_ANCESTOR_INDEX_SLOT 17
+#define MODULE_OBJECT_ENVIRONMENT_SLOT        1
+#define MODULE_OBJECT_STATUS_SLOT             3
+#define MODULE_OBJECT_ERROR_SLOT              4
+#define MODULE_OBJECT_DFS_INDEX_SLOT          15
+#define MODULE_OBJECT_DFS_ANCESTOR_INDEX_SLOT 16
 
 #define MODULE_STATUS_ERRORED        0
 #define MODULE_STATUS_UNINSTANTIATED 1
 #define MODULE_STATUS_INSTANTIATING  2
 #define MODULE_STATUS_INSTANTIATED   3
 #define MODULE_STATUS_EVALUATING     4
 #define MODULE_STATUS_EVALUATED      5
 
diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -3917,18 +3917,16 @@ SetRNGState(JSContext* cx, unsigned argc
 
 static ModuleEnvironmentObject*
 GetModuleEnvironment(JSContext* cx, HandleModuleObject module)
 {
     // Use the initial environment so that tests can check bindings exists
     // before they have been instantiated.
     RootedModuleEnvironmentObject env(cx, &module->initialEnvironment());
     MOZ_ASSERT(env);
-    MOZ_ASSERT_IF(module->environment(), module->environment() == env);
-
     return env;
 }
 
 static bool
 GetModuleEnvironmentNames(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     if (args.length() != 1) {
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -1969,27 +1969,16 @@ intrinsic_HostResolveImportedModule(JSCo
         return false;
     }
 
     args.rval().set(result);
     return true;
 }
 
 static bool
-intrinsic_CreateModuleEnvironment(JSContext* cx, unsigned argc, Value* vp)
-{
-    CallArgs args = CallArgsFromVp(argc, vp);
-    MOZ_ASSERT(args.length() == 1);
-    RootedModuleObject module(cx, &args[0].toObject().as<ModuleObject>());
-    module->createEnvironment();
-    args.rval().setUndefined();
-    return true;
-}
-
-static bool
 intrinsic_CreateImportBinding(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     MOZ_ASSERT(args.length() == 4);
     RootedModuleEnvironmentObject environment(cx, &args[0].toObject().as<ModuleEnvironmentObject>());
     RootedAtom importedName(cx, &args[1].toString()->asAtom());
     RootedModuleObject module(cx, &args[2].toObject().as<ModuleObject>());
     RootedAtom localName(cx, &args[3].toString()->asAtom());
@@ -2515,17 +2504,16 @@ static const JSFunctionSpec intrinsic_fu
     JS_FN("regexp_test_no_statics", regexp_test_no_statics, 2,0),
     JS_FN("regexp_construct_raw_flags", regexp_construct_raw_flags, 2,0),
 
     JS_FN("IsModule", intrinsic_IsInstanceOfBuiltin<ModuleObject>, 1, 0),
     JS_FN("CallModuleMethodIfWrapped",
           CallNonGenericSelfhostedMethod<Is<ModuleObject>>, 2, 0),
     JS_FN("HostResolveImportedModule", intrinsic_HostResolveImportedModule, 2, 0),
     JS_FN("IsModuleEnvironment", intrinsic_IsInstanceOfBuiltin<ModuleEnvironmentObject>, 1, 0),
-    JS_FN("CreateModuleEnvironment", intrinsic_CreateModuleEnvironment, 1, 0),
     JS_FN("CreateImportBinding", intrinsic_CreateImportBinding, 4, 0),
     JS_FN("CreateNamespaceBinding", intrinsic_CreateNamespaceBinding, 3, 0),
     JS_FN("InstantiateModuleFunctionDeclarations",
           intrinsic_InstantiateModuleFunctionDeclarations, 1, 0),
     JS_FN("ExecuteModule", intrinsic_ExecuteModule, 1, 0),
     JS_FN("NewModuleNamespace", intrinsic_NewModuleNamespace, 2, 0),
     JS_FN("AddModuleNamespaceBinding", intrinsic_AddModuleNamespaceBinding, 4, 0),
     JS_FN("ModuleNamespaceExports", intrinsic_ModuleNamespaceExports, 1, 0),
