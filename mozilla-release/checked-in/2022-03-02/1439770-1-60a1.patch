# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1519166687 28800
#      Tue Feb 20 14:44:47 2018 -0800
# Node ID 486abdc9fc6a9208e88151d2e5a2095090265559
# Parent  98f7af406360d7a95924afcf401d8bc29496e375
Bug 1439770, part 1 - Fix integer overflow in InterfaceDescriptorAddTypes. r=njn

num_additional_types is a uint8_t, so its max value is 255. 1 + 255 is
not greater than 256, so the check will pass, but then
num_additional_types += 1 will overflow in the next line.

What I think happened is that bug 1249174 part 6 introduced a bounds
check on an index (which is ok), but then part 8 repurposed this as a
bounds check on the length.

I noticed this because while writing the next patch I ended up with
  if (id->num_additional_types > 255)
and then the compiler warned that the check would never fail.

MozReview-Commit-ID: KqiaOyBjj7v

diff --git a/xpcom/typelib/xpt/xpt_struct.cpp b/xpcom/typelib/xpt/xpt_struct.cpp
--- a/xpcom/typelib/xpt/xpt_struct.cpp
+++ b/xpcom/typelib/xpt/xpt_struct.cpp
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* Implementation of XDR routines for typelib structures. */
 
 #include "xpt_xdr.h"
 #include "xpt_struct.h"
 #include <string.h>
+#include <stdint.h>
 #include <stdio.h>
 
 using mozilla::WrapNotNull;
 
 /***************************************************************************/
 /* Forward declarations. */
 
 static bool
@@ -182,17 +183,17 @@ InterfaceDescriptorAddTypes(XPTArena *ar
     new_ = static_cast<XPTTypeDescriptor*>(XPT_CALLOC8(arena, new_size));
     if (!new_)
         return false;
     if (old) {
         memcpy(new_, old, old_size);
     }
     id->additional_types = new_;
 
-    if (num + uint16_t(id->num_additional_types) > 256)
+    if (num + uint16_t(id->num_additional_types) > UINT8_MAX)
         return false;
 
     id->num_additional_types += num;
     return true;
 }
 
 bool
 DoInterfaceDescriptor(XPTArena *arena, NotNull<XPTCursor*> outer,
