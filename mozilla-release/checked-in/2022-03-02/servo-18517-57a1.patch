# HG changeset patch
# User Boris Chiou <boris.chiou@gmail.com>
# Date 1505730216 18000
#      Mon Sep 18 05:23:36 2017 -0500
# Node ID 32bb0db8301d0676b167c550ac8f9468045e6a4d
# Parent  7cd9b9ab4d46dd6275a4a756cf7d26699f77ca62
servo: Merge #18517 - style: Clamp some filter functions to one if the values over 100% (from BorisChiou:stylo/filter/clamp); r=emilio

For grayscale, invert, opacity, and sepia, "values of amount over 100%
are allowed but UAs must clamp the values to 1" [1]-[4], so we clamp its value
in the parser.

[1] https://drafts.fxtf.org/filter-effects/#funcdef-filter-grayscale
[2] https://drafts.fxtf.org/filter-effects/#funcdef-filter-invert
[3] https://drafts.fxtf.org/filter-effects/#funcdef-filter-opacity
[4] https://drafts.fxtf.org/filter-effects/#funcdef-filter-sepia

---
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [X] These changes fix [Bug 1399799](https://bugzilla.mozilla.org/show_bug.cgi?id=1399799).
- [X] There are tests for these changes

Source-Repo: https://github.com/servo/servo
Source-Revision: cff9b427fca1ab878fd23929cf7835666aab6d84

diff --git a/servo/components/style/values/specified/effects.rs b/servo/components/style/values/specified/effects.rs
--- a/servo/components/style/values/specified/effects.rs
+++ b/servo/components/style/values/specified/effects.rs
@@ -35,16 +35,40 @@ pub type Filter = GenericFilter<Angle, F
 pub type Filter = GenericFilter<Angle, Factor, NonNegativeLength, Impossible>;
 
 /// A value for the `<factor>` parts in `Filter`.
 #[derive(Clone, Debug, PartialEq, ToCss)]
 #[cfg_attr(feature = "gecko", derive(MallocSizeOf))]
 #[cfg_attr(feature = "servo", derive(HeapSizeOf))]
 pub struct Factor(NumberOrPercentage);
 
+impl Factor {
+    /// Parse this factor but clamp to one if the value is over 100%.
+    #[inline]
+    pub fn parse_with_clamping_to_one<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>
+    ) -> Result<Self, ParseError<'i>> {
+        Factor::parse(context, input).map(|v| v.clamp_to_one())
+    }
+
+    /// Clamp the value to 1 if the value is over 100%.
+    #[inline]
+    fn clamp_to_one(self) -> Self {
+        match self.0 {
+            NumberOrPercentage::Percentage(percent) => {
+                Factor(NumberOrPercentage::Percentage(percent.clamp_to_hundred()))
+            },
+            NumberOrPercentage::Number(number) => {
+                Factor(NumberOrPercentage::Number(number.clamp_to_one()))
+            }
+        }
+    }
+}
+
 impl Parse for Factor {
     #[inline]
     fn parse<'i, 't>(
         context: &ParserContext,
         input: &mut Parser<'i, 't>
     ) -> Result<Self, ParseError<'i>> {
         NumberOrPercentage::parse_non_negative(context, input).map(Factor)
     }
@@ -168,22 +192,38 @@ impl Parse for Filter {
                 return Err(ValueParseError::InvalidFilter(t.clone()).into()),
             Err(e) => return Err(e.into()),
         };
         input.parse_nested_block(|i| {
             match_ignore_ascii_case! { &*function,
                 "blur" => Ok(GenericFilter::Blur((Length::parse_non_negative(context, i)?).into())),
                 "brightness" => Ok(GenericFilter::Brightness(Factor::parse(context, i)?)),
                 "contrast" => Ok(GenericFilter::Contrast(Factor::parse(context, i)?)),
-                "grayscale" => Ok(GenericFilter::Grayscale(Factor::parse(context, i)?)),
+                "grayscale" => {
+                    // Values of amount over 100% are allowed but UAs must clamp the values to 1.
+                    // https://drafts.fxtf.org/filter-effects/#funcdef-filter-grayscale
+                    Ok(GenericFilter::Grayscale(Factor::parse_with_clamping_to_one(context, i)?))
+                },
                 "hue-rotate" => Ok(GenericFilter::HueRotate(Angle::parse(context, i)?)),
-                "invert" => Ok(GenericFilter::Invert(Factor::parse(context, i)?)),
-                "opacity" => Ok(GenericFilter::Opacity(Factor::parse(context, i)?)),
+                "invert" => {
+                    // Values of amount over 100% are allowed but UAs must clamp the values to 1.
+                    // https://drafts.fxtf.org/filter-effects/#funcdef-filter-invert
+                    Ok(GenericFilter::Invert(Factor::parse_with_clamping_to_one(context, i)?))
+                },
+                "opacity" => {
+                    // Values of amount over 100% are allowed but UAs must clamp the values to 1.
+                    // https://drafts.fxtf.org/filter-effects/#funcdef-filter-opacity
+                    Ok(GenericFilter::Opacity(Factor::parse_with_clamping_to_one(context, i)?))
+                },
                 "saturate" => Ok(GenericFilter::Saturate(Factor::parse(context, i)?)),
-                "sepia" => Ok(GenericFilter::Sepia(Factor::parse(context, i)?)),
+                "sepia" => {
+                    // Values of amount over 100% are allowed but UAs must clamp the values to 1.
+                    // https://drafts.fxtf.org/filter-effects/#funcdef-filter-sepia
+                    Ok(GenericFilter::Sepia(Factor::parse_with_clamping_to_one(context, i)?))
+                },
                 "drop-shadow" => Ok(GenericFilter::DropShadow(Parse::parse(context, i)?)),
                 _ => Err(ValueParseError::InvalidFilter(Token::Function(function.clone())).into()),
             }
         })
     }
 }
 
 impl Parse for SimpleShadow {
diff --git a/servo/components/style/values/specified/mod.rs b/servo/components/style/values/specified/mod.rs
--- a/servo/components/style/values/specified/mod.rs
+++ b/servo/components/style/values/specified/mod.rs
@@ -212,16 +212,25 @@ impl Number {
         parse_number_with_clamping_mode(context, input, AllowedNumericType::NonNegative)
     }
 
     #[allow(missing_docs)]
     pub fn parse_at_least_one<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
                                       -> Result<Number, ParseError<'i>> {
         parse_number_with_clamping_mode(context, input, AllowedNumericType::AtLeastOne)
     }
+
+    /// Clamp to 1.0 if the value is over 1.0.
+    #[inline]
+    pub fn clamp_to_one(self) -> Self {
+        Number {
+            value: self.value.min(1.),
+            calc_clamping_mode: self.calc_clamping_mode,
+        }
+    }
 }
 
 impl ToComputedValue for Number {
     type ComputedValue = CSSFloat;
 
     #[inline]
     fn to_computed_value(&self, _: &Context) -> CSSFloat { self.get() }
 
diff --git a/servo/components/style/values/specified/percentage.rs b/servo/components/style/values/specified/percentage.rs
--- a/servo/components/style/values/specified/percentage.rs
+++ b/servo/components/style/values/specified/percentage.rs
@@ -120,16 +120,25 @@ impl Percentage {
 
     /// Parses a percentage token, but rejects it if it's negative.
     pub fn parse_non_negative<'i, 't>(
         context: &ParserContext,
         input: &mut Parser<'i, 't>,
     ) -> Result<Self, ParseError<'i>> {
         Self::parse_with_clamping_mode(context, input, AllowedNumericType::NonNegative)
     }
+
+    /// Clamp to 100% if the value is over 100%.
+    #[inline]
+    pub fn clamp_to_hundred(self) -> Self {
+        Percentage {
+            value: self.value.min(1.),
+            calc_clamping_mode: self.calc_clamping_mode,
+        }
+    }
 }
 
 impl Parse for Percentage {
     #[inline]
     fn parse<'i, 't>(
         context: &ParserContext,
         input: &mut Parser<'i, 't>
     ) -> Result<Self, ParseError<'i>> {
