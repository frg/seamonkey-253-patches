# HG changeset patch
# User Steve Fink <sfink@mozilla.com>
# Date 1504908202 25200
#      Fri Sep 08 15:03:22 2017 -0700
# Node ID c5239fd503f2c277f2dc185ce8c55789fbefcad2
# Parent  a8c4e5bf2882a8dcea13d29d760e9ab13ab4705e
Bug 1400442 - Targeted annotation for static local array of member pointers in LangGroupFontPrefs::Initialize, r=bhackett

The code is

    void
    LangGroupFontPrefs::Initialize(nsIAtom* aLangGroupAtom)
    {
        nsFont* fontTypes[] = {
            &mDefaultVariableFont,
            &mDefaultFixedFont,
            &mDefaultSerifFont,
            &mDefaultSansSerifFont,
            &mDefaultMonospaceFont,
            &mDefaultCursiveFont,
            &mDefaultFantasyFont
        };

        nsFont* font = fontTypes[3];
        font->size = 42;
    }

'this' is known to be a safe pointer (exclusively owned by the current thread), so a pointer to one of its members is also safe. But the analysis can't track safety across all that, so I have a special-case annotation here that says that fontTypes[3] returns a safe pointer if and only if 'this' is safe.

Note that all of those fields (eg mDefaultVariableFont) are nsFont structs, not pointers, so although you'd expect this to be one dereference away from a safe pointer's memory, it is not; assigning to font->size ends up being a write to some offset within the 'this' pointer, which is known to be safe here.

diff --git a/js/src/devtools/rootAnalysis/analyzeHeapWrites.js b/js/src/devtools/rootAnalysis/analyzeHeapWrites.js
--- a/js/src/devtools/rootAnalysis/analyzeHeapWrites.js
+++ b/js/src/devtools/rootAnalysis/analyzeHeapWrites.js
@@ -240,16 +240,46 @@ function treatAsSafeArgument(entry, varN
             continue;
         if (csuMatch && (!csuName || !nameMatches(csuName, csuMatch)))
             continue;
         return true;
     }
     return false;
 }
 
+function isSafeAssignment(entry, edge, variable)
+{
+    if (edge.Kind != 'Assign')
+        return false;
+
+    var [mangled, unmangled] = splitFunction(entry.name);
+
+    // The assignment
+    //
+    //   nsFont* font = fontTypes[eType];
+    //
+    // ends up with 'font' pointing to a member of 'this', so it should inherit
+    // the safety of 'this'.
+    if (unmangled.includes("mozilla::LangGroupFontPrefs::Initialize") &&
+        variable == 'font')
+    {
+        const [lhs, rhs] = edge.Exp;
+        const {Kind, Exp: [{Kind: indexKind, Exp: [collection, index]}]} = rhs;
+        if (Kind == 'Drf' &&
+            indexKind == 'Index' &&
+            collection.Kind == 'Var' &&
+            collection.Variable.Name[0] == 'fontTypes')
+        {
+            return entry.isSafeArgument(0); // 'this'
+        }
+    }
+
+    return false;
+}
+
 function checkFieldWrite(entry, location, fields)
 {
     var name = entry.name;
     for (var field of fields) {
         // The analysis is having some trouble keeping track of whether
         // already_AddRefed and nsCOMPtr structures are safe to access.
         // Hopefully these will be thread local, but it would be better to
         // improve the analysis to handle these.
@@ -1185,16 +1215,19 @@ function isSafeVariable(entry, variable)
             // Coercion via AsAString preserves safety.
             if (isDirectCall(edge, /AsAString/) &&
                 isEdgeSafeArgument(entry, edge.PEdgeCallInstance.Exp))
             {
                 return true;
             }
         }
 
+        if (isSafeAssignment(entry, edge, name))
+            return true;
+
         // Watch out for variables which were assigned arguments.
         var rhsVariable = variableAssignRhs(edge);
         if (rhsVariable)
             return isSafeVariable(entry, rhsVariable);
     }
 
     // When temporary stack structures are created (either to return or to call
     // methods on without assigning them a name), the generated sixgill JSON is
