# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1504852207 18000
#      Fri Sep 08 01:30:07 2017 -0500
# Node ID 404a3fe96ab028d5aea5a811388f1ffbb5701297
# Parent  34db5bec9cad5bd0b83235b3d1a9ea1ebce8692a
servo: Merge #18417 - style: Stop the grid shorthand from resetting grid-gap properties (from emilio:grid-gap-shorthand); r=heycam

Bug: 1387410
Reviewed-by: heycam
MozReview-Commit-ID: GxU9YuAUc00
Source-Repo: https://github.com/servo/servo
Source-Revision: 44072163eb563187c6c1d60c9ad5c004bdf65a66

diff --git a/servo/components/style/properties/shorthand/position.mako.rs b/servo/components/style/properties/shorthand/position.mako.rs
--- a/servo/components/style/properties/shorthand/position.mako.rs
+++ b/servo/components/style/properties/shorthand/position.mako.rs
@@ -443,26 +443,25 @@
             serialize_grid_template(self.grid_template_rows, self.grid_template_columns,
                                     self.grid_template_areas, dest)
         }
     }
 </%helpers:shorthand>
 
 <%helpers:shorthand name="grid"
                     sub_properties="grid-template-rows grid-template-columns grid-template-areas
-                                    grid-auto-rows grid-auto-columns grid-row-gap grid-column-gap
-                                    grid-auto-flow"
+                                    grid-auto-rows grid-auto-columns grid-auto-flow"
                     spec="https://drafts.csswg.org/css-grid/#propdef-grid"
                     products="gecko">
     use parser::Parse;
     use properties::longhands::{grid_auto_columns, grid_auto_rows, grid_auto_flow};
     use properties::longhands::grid_auto_flow::computed_value::{AutoFlow, T as SpecifiedAutoFlow};
     use values::{Either, None_};
     use values::generics::grid::{GridTemplateComponent, TrackListType};
-    use values::specified::{GenericGridTemplateComponent, NonNegativeLengthOrPercentage, TrackSize};
+    use values::specified::{GenericGridTemplateComponent, TrackSize};
 
     pub fn parse_value<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
                                -> Result<Longhands, ParseError<'i>> {
         let mut temp_rows = GridTemplateComponent::None;
         let mut temp_cols = GridTemplateComponent::None;
         let mut temp_areas = Either::Second(None_);
         let mut auto_rows = TrackSize::default();
         let mut auto_cols = TrackSize::default();
@@ -512,42 +511,31 @@
 
         Ok(expanded! {
             grid_template_rows: temp_rows,
             grid_template_columns: temp_cols,
             grid_template_areas: temp_areas,
             grid_auto_rows: auto_rows,
             grid_auto_columns: auto_cols,
             grid_auto_flow: flow,
-            // This shorthand also resets grid gap
-            grid_row_gap: NonNegativeLengthOrPercentage::zero(),
-            grid_column_gap: NonNegativeLengthOrPercentage::zero(),
         })
     }
 
     impl<'a> LonghandsToSerialize<'a> {
         /// Returns true if other sub properties except template-{rows,columns} are initial.
         fn is_grid_template(&self) -> bool {
             *self.grid_template_areas == Either::Second(None_) &&
             *self.grid_auto_rows == TrackSize::default() &&
             *self.grid_auto_columns == TrackSize::default() &&
             *self.grid_auto_flow == grid_auto_flow::get_initial_value()
         }
     }
 
     impl<'a> ToCss for LonghandsToSerialize<'a> {
         fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
-            // `grid` shorthand resets these properties. If they are not zero, that means they
-            // are changed by longhands and in that case we should fail serializing `grid`.
-            if *self.grid_row_gap != NonNegativeLengthOrPercentage::zero() ||
-               *self.grid_column_gap != NonNegativeLengthOrPercentage::zero() {
-                return Ok(());
-            }
-
-
             if *self.grid_template_areas != Either::Second(None_) ||
                (*self.grid_template_rows != GridTemplateComponent::None &&
                    *self.grid_template_columns != GridTemplateComponent::None) ||
                self.is_grid_template() {
                 return super::grid_template::serialize_grid_template(self.grid_template_rows,
                                                                      self.grid_template_columns,
                                                                      self.grid_template_areas, dest);
             }
