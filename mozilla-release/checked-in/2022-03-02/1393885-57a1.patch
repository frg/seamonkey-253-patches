# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1504640380 14400
#      Tue Sep 05 15:39:40 2017 -0400
# Node ID f56376db333bb81776048002c27b60912fac593f
# Parent  23c3efbc1afa1fa5d5c04f6e3b5661092f7d92f6
Bug 1393885 - provide a faster path for do_GetWeakReference; r=erahm

NS_GetWeakReference, called from do_GetWeakReference, QI's its argument
to nsISupportsWeakReference to determine whether a weak reference can be
obtained.  If NS_GetWeakReference is already receiving a
nsISupportsWeakReference pointer, or something than can be converted to
one, then we can skip the QI for a small performance win.

diff --git a/xpcom/base/nsIWeakReferenceUtils.h b/xpcom/base/nsIWeakReferenceUtils.h
--- a/xpcom/base/nsIWeakReferenceUtils.h
+++ b/xpcom/base/nsIWeakReferenceUtils.h
@@ -37,30 +37,38 @@ do_QueryReferent(nsIWeakReference* aRawP
 }
 
 
 /**
  * Deprecated, use |do_GetWeakReference| instead.
  */
 extern nsIWeakReference* NS_GetWeakReference(nsISupports*,
                                              nsresult* aResult = 0);
+extern nsIWeakReference* NS_GetWeakReference(nsISupportsWeakReference*,
+                                             nsresult* aResult = 0);
 
 /**
  * |do_GetWeakReference| is a convenience function that bundles up all the work needed
  * to get a weak reference to an arbitrary object, i.e., the |QueryInterface|, test, and
  * call through to |GetWeakReference|, and put it into your |nsCOMPtr|.
  * It is specifically designed to cooperate with |nsCOMPtr| (or |nsWeakPtr|) like so:
  * |nsWeakPtr myWeakPtr = do_GetWeakReference(aPtr);|.
  */
 inline already_AddRefed<nsIWeakReference>
 do_GetWeakReference(nsISupports* aRawPtr, nsresult* aError = 0)
 {
   return dont_AddRef(NS_GetWeakReference(aRawPtr, aError));
 }
 
+inline already_AddRefed<nsIWeakReference>
+do_GetWeakReference(nsISupportsWeakReference* aRawPtr, nsresult* aError = 0)
+{
+  return dont_AddRef(NS_GetWeakReference(aRawPtr, aError));
+}
+
 inline void
 do_GetWeakReference(nsIWeakReference* aRawPtr, nsresult* aError = 0)
 {
   // This signature exists solely to _stop_ you from doing a bad thing.
   //  Saying |do_GetWeakReference()| on a weak reference itself,
   //  is very likely to be a programmer error.
 }
 
diff --git a/xpcom/base/nsWeakReference.cpp b/xpcom/base/nsWeakReference.cpp
--- a/xpcom/base/nsWeakReference.cpp
+++ b/xpcom/base/nsWeakReference.cpp
@@ -62,16 +62,36 @@ nsQueryReferent::operator()(const nsIID&
   }
 
   if (mErrorPtr) {
     *mErrorPtr = status;
   }
   return status;
 }
 
+nsIWeakReference*
+NS_GetWeakReference(nsISupportsWeakReference* aInstancePtr, nsresult* aErrorPtr)
+{
+  nsresult status;
+
+  nsIWeakReference* result = nullptr;
+
+  if (aInstancePtr) {
+    status = aInstancePtr->GetWeakReference(&result);
+  } else {
+    status = NS_ERROR_NULL_POINTER;
+  }
+
+  if (aErrorPtr) {
+    *aErrorPtr = status;
+  }
+
+  return result;
+}
+
 nsIWeakReference*  // or else |already_AddRefed<nsIWeakReference>|
 NS_GetWeakReference(nsISupports* aInstancePtr, nsresult* aErrorPtr)
 {
   nsresult status;
 
   nsIWeakReference* result = nullptr;
 
   if (aInstancePtr) {
