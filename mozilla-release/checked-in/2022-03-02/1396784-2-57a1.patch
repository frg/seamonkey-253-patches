# HG changeset patch
# User Alexandre Poirot <poirot.alex@gmail.com>
# Date 1505316693 -7200
#      Wed Sep 13 17:31:33 2017 +0200
# Node ID 1bf043d31e2ec7c869eaf4288944a950f05d7e33
# Parent  56a4529e1b30f85a9b9f49ed89f40d5cd6dd1f79
Bug 1396784 - Move gridInspector property to layout view. r=gl

MozReview-Commit-ID: 204N6yi0fx3

diff --git a/devtools/client/inspector/grids/grid-inspector.js b/devtools/client/inspector/grids/grid-inspector.js
--- a/devtools/client/inspector/grids/grid-inspector.js
+++ b/devtools/client/inspector/grids/grid-inspector.js
@@ -108,17 +108,21 @@ GridInspector.prototype = {
   destroy() {
     this.highlighters.off("grid-highlighter-hidden", this.onHighlighterChange);
     this.highlighters.off("grid-highlighter-shown", this.onHighlighterChange);
     this.inspector.sidebar.off("select", this.onSidebarSelect);
     this.inspector.off("new-root", this.onNavigate);
 
     this.inspector.reflowTracker.untrackReflows(this, this.onReflow);
 
-    this.swatchColorPickerTooltip.destroy();
+    // The color picker may not be ready as `init` function is async,
+    // and we do not wait for its completion before calling destroy in tests
+    if (this.swatchColorPickerTooltip) {
+      this.swatchColorPickerTooltip.destroy();
+    }
 
     this.document = null;
     this.highlighters = null;
     this.inspector = null;
     this.layoutInspector = null;
     this.store = null;
     this.swatchColorPickerTooltip = null;
     this.walker = null;
diff --git a/devtools/client/inspector/inspector.js b/devtools/client/inspector/inspector.js
--- a/devtools/client/inspector/inspector.js
+++ b/devtools/client/inspector/inspector.js
@@ -18,17 +18,16 @@ const {Task} = require("devtools/shared/
 const Telemetry = require("devtools/client/shared/telemetry");
 const HighlightersOverlay = require("devtools/client/inspector/shared/highlighters-overlay");
 const ReflowTracker = require("devtools/client/inspector/shared/reflow-tracker");
 const Store = require("devtools/client/inspector/store");
 
 loader.lazyRequireGetter(this, "initCssProperties", "devtools/shared/fronts/css-properties", true);
 loader.lazyRequireGetter(this, "HTMLBreadcrumbs", "devtools/client/inspector/breadcrumbs", true);
 loader.lazyRequireGetter(this, "KeyShortcuts", "devtools/client/shared/key-shortcuts");
-loader.lazyRequireGetter(this, "GridInspector", "devtools/client/inspector/grids/grid-inspector");
 loader.lazyRequireGetter(this, "InspectorSearch", "devtools/client/inspector/inspector-search", true);
 loader.lazyRequireGetter(this, "ToolSidebar", "devtools/client/inspector/toolsidebar", true);
 loader.lazyRequireGetter(this, "MarkupView", "devtools/client/inspector/markup/markup");
 
 loader.lazyRequireGetter(this, "nodeConstants", "devtools/shared/dom-node-constants");
 loader.lazyRequireGetter(this, "Menu", "devtools/client/framework/menu");
 loader.lazyRequireGetter(this, "MenuItem", "devtools/client/framework/menu-item");
 loader.lazyRequireGetter(this, "ExtensionSidebar", "devtools/client/inspector/extensions/extension-sidebar");
@@ -622,20 +621,16 @@ Inspector.prototype = {
       INSPECTOR_L10N.getStr("inspector.sidebar.ruleViewTitle"),
       defaultTab == "ruleview");
 
     this.sidebar.addExistingTab(
       "computedview",
       INSPECTOR_L10N.getStr("inspector.sidebar.computedViewTitle"),
       defaultTab == "computedview");
 
-    // Grid and layout panels aren't lazy-loaded as their module end up
-    // calling inspector.addSidebarTab
-    this.gridInspector = new GridInspector(this, this.panelWin);
-
     // Inject a lazy loaded react tab by exposing a fake React object
     // with a lazy defined Tab thanks to `panel` being a function
     let layoutId = "layoutview";
     let layoutTitle = INSPECTOR_L10N.getStr("inspector.sidebar.layoutViewTitle2");
     this.sidebar.addTab(
       layoutId,
       layoutTitle,
       {
@@ -1068,20 +1063,16 @@ Inspector.prototype = {
     this.target.off("thread-resumed", this.updateDebuggerPausedWarning);
     this._toolbox.off("select", this.updateDebuggerPausedWarning);
 
     for (let [, panel] of this._panels) {
       panel.destroy();
     }
     this._panels.clear();
 
-    if (this.gridInspector) {
-      this.gridInspector.destroy();
-    }
-
     if (this.layoutview) {
       this.layoutview.destroy();
     }
 
     if (this.fontinspector) {
       this.fontinspector.destroy();
     }
 
diff --git a/devtools/client/inspector/layout/layout.js b/devtools/client/inspector/layout/layout.js
--- a/devtools/client/inspector/layout/layout.js
+++ b/devtools/client/inspector/layout/layout.js
@@ -16,16 +16,18 @@ const INSPECTOR_L10N =
   new LocalizationHelper("devtools/client/locales/inspector.properties");
 
 // @remove after release 56 (See Bug 1355747)
 const PROMOTE_COUNT_PREF = "devtools.promote.layoutview";
 
 // @remove after release 56 (See Bug 1355747)
 const GRID_LINK = "https://www.mozilla.org/en-US/developer/css-grid/?utm_source=gridtooltip&utm_medium=devtools&utm_campaign=cssgrid_layout";
 
+loader.lazyRequireGetter(this, "GridInspector", "devtools/client/inspector/grids/grid-inspector");
+
 function LayoutView(inspector, window) {
   this.document = window.document;
   this.inspector = inspector;
   this.store = inspector.store;
 
   this.onPromoteLearnMoreClick = this.onPromoteLearnMoreClick.bind(this);
 
   this.init();
@@ -45,27 +47,28 @@ LayoutView.prototype = {
 
     let {
       onHideBoxModelHighlighter,
       onShowBoxModelEditor,
       onShowBoxModelHighlighter,
       onToggleGeometryEditor,
     } = this.inspector.getPanel("boxmodel").getComponentProps();
 
+    this.gridInspector = new GridInspector(this.inspector, this.inspector.panelWin);
     let {
       getSwatchColorPickerTooltip,
       onSetGridOverlayColor,
       onShowGridAreaHighlight,
       onShowGridCellHighlight,
       onShowGridLineNamesHighlight,
       onToggleGridHighlighter,
       onToggleShowGridAreas,
       onToggleShowGridLineNumbers,
       onToggleShowInfiniteLines,
-    } = this.inspector.gridInspector.getComponentProps();
+    } = this.gridInspector.getComponentProps();
 
     let {
       onPromoteLearnMoreClick,
     } = this;
 
     let app = App({
       getSwatchColorPickerTooltip,
       setSelectedNode,
@@ -104,16 +107,18 @@ LayoutView.prototype = {
     // Expose the provider to let inspector.js use it in setupSidebar.
     this.provider = provider;
   },
 
   /**
    * Destruction function called when the inspector is destroyed. Cleans up references.
    */
   destroy() {
+    this.gridInspector.destroy();
+
     this.document = null;
     this.inspector = null;
     this.store = null;
   },
 
   onPromoteLearnMoreClick() {
     let browserWin = this.inspector.target.tab.ownerDocument.defaultView;
     browserWin.openUILinkIn(GRID_LINK, "current");
diff --git a/devtools/client/inspector/test/shared-head.js b/devtools/client/inspector/test/shared-head.js
--- a/devtools/client/inspector/test/shared-head.js
+++ b/devtools/client/inspector/test/shared-head.js
@@ -131,17 +131,17 @@ function openLayoutView() {
       };
     }
     mockHighlighter(data.toolbox);
 
     return {
       toolbox: data.toolbox,
       inspector: data.inspector,
       boxmodel: data.inspector.getPanel("boxmodel"),
-      gridInspector: data.inspector.gridInspector,
+      gridInspector: data.inspector.layoutview.gridInspector,
       testActor: data.testActor
     };
   });
 }
 
 /**
  * Select the rule view sidebar tab on an already opened inspector panel.
  *
