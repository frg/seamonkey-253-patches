# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1519767394 28800
#      Tue Feb 27 13:36:34 2018 -0800
# Node ID ded8385fa2981a3650bdc044fd4f331fb04a7380
# Parent  bed9758b4f582cc698f0d7ea7f8185303def8f95
Bug 1441677, part 6 - Get rid of macros for nsXPTParamInfo flags. r=njn

Also, get rid of a gratuitous use of a trinary operator in
nsXPCWrappedJSClass::CallMethod, clean up the style a little, and mark
an unimplemented ctor as deleted.

MozReview-Commit-ID: Kp64sMxyRWc

diff --git a/js/xpconnect/src/XPCWrappedJSClass.cpp b/js/xpconnect/src/XPCWrappedJSClass.cpp
--- a/js/xpconnect/src/XPCWrappedJSClass.cpp
+++ b/js/xpconnect/src/XPCWrappedJSClass.cpp
@@ -978,18 +978,20 @@ nsXPCWrappedJSClass::CallMethod(nsXPCWra
     AutoValueVector args(cx);
     AutoScriptEvaluate scriptEval(cx);
 
     XPCJSContext* xpccx = ccx.GetContext();
     AutoSavePendingResult apr(xpccx);
 
     // XXX ASSUMES that retval is last arg. The xpidl compiler ensures this.
     uint8_t paramCount = info->GetParamCount();
-    uint8_t argc = paramCount -
-        (paramCount && XPT_PD_IS_RETVAL(info->GetParam(paramCount - 1).flags) ? 1 : 0);
+    uint8_t argc = paramCount;
+    if (paramCount > 0 && info->GetParam(paramCount - 1).IsRetval()) {
+        argc -= 1;
+    }
 
     if (!scriptEval.StartEvaluating(obj))
         goto pre_call_clean_up;
 
     xpccx->SetPendingException(nullptr);
 
     // We use js_Invoke so that the gcthings we use as args will be rooted by
     // the engine as we do conversions and prepare to do the function call.
diff --git a/xpcom/reflect/xptinfo/xptinfo.h b/xpcom/reflect/xptinfo/xptinfo.h
--- a/xpcom/reflect/xptinfo/xptinfo.h
+++ b/xpcom/reflect/xptinfo/xptinfo.h
@@ -118,20 +118,20 @@ public:
 class nsXPTParamInfo : public XPTParamDescriptor
 {
 // NO DATA - this a flyweight wrapper
 public:
     MOZ_IMPLICIT nsXPTParamInfo(const XPTParamDescriptor& desc)
         {*(XPTParamDescriptor*)this = desc;}
 
 
-    bool IsIn()  const    {return 0 != (XPT_PD_IS_IN(flags));}
-    bool IsOut() const    {return 0 != (XPT_PD_IS_OUT(flags));}
-    bool IsRetval() const {return 0 != (XPT_PD_IS_RETVAL(flags));}
-    bool IsShared() const {return 0 != (XPT_PD_IS_SHARED(flags));}
+    bool IsIn() const {return !!(flags & kInMask);}
+    bool IsOut() const {return !!(flags & kOutMask);}
+    bool IsRetval() const {return !!(flags & kRetvalMask);}
+    bool IsShared() const {return !!(flags & kSharedMask);}
 
     // Dipper types are one of the more inscrutable aspects of xpidl. In a
     // nutshell, dippers are empty container objects, created and passed by
     // the caller, and filled by the callee. The callee receives a fully-
     // formed object, and thus does not have to construct anything. But
     // the object is functionally empty, and the callee is responsible for
     // putting something useful inside of it.
     //
@@ -140,18 +140,18 @@ public:
     // limited to 4 string types.
     //
     // When a dipper type is declared as an 'out' parameter, xpidl internally
     // converts it to an 'in', and sets the XPT_PD_DIPPER flag on it. For this
     // reason, dipper types are sometimes referred to as 'out parameters
     // masquerading as in'. The burden of maintaining this illusion falls mostly
     // on XPConnect, which creates the empty containers, and harvest the results
     // after the call.
-    bool IsDipper() const {return 0 != (XPT_PD_IS_DIPPER(flags));}
-    bool IsOptional() const {return 0 != (XPT_PD_IS_OPTIONAL(flags));}
+    bool IsDipper() const {return !!(flags & kDipperMask);}
+    bool IsOptional() const {return !!(flags & kOptionalMask);}
     const nsXPTType GetType() const {return type.prefix;}
 
     bool IsStringClass() const {
       switch (GetType().TagPart()) {
         case nsXPTType::T_ASTRING:
         case nsXPTType::T_DOMSTRING:
         case nsXPTType::T_UTF8STRING:
         case nsXPTType::T_CSTRING:
@@ -165,17 +165,24 @@ public:
     // applies to out/inout params, but we use it unconditionally for certain
     // types.
     bool IsIndirect() const {return IsOut() ||
                                GetType().TagPart() == nsXPTType::T_JSVAL;}
 
     // NOTE: other activities on types are done via methods on nsIInterfaceInfo
 
 private:
-    nsXPTParamInfo();   // no implementation
+    static const uint8_t kInMask =       0x80;
+    static const uint8_t kOutMask =      0x40;
+    static const uint8_t kRetvalMask =   0x20;
+    static const uint8_t kSharedMask =   0x10;
+    static const uint8_t kDipperMask =   0x08;
+    static const uint8_t kOptionalMask = 0x04;
+
+    nsXPTParamInfo() = delete;
 // NO DATA - this a flyweight wrapper
 };
 
 class nsXPTMethodInfo : public XPTMethodDescriptor
 {
 // NO DATA - this a flyweight wrapper
 public:
     MOZ_IMPLICIT nsXPTMethodInfo(const XPTMethodDescriptor& desc)
diff --git a/xpcom/typelib/xpt/xpt_struct.h b/xpcom/typelib/xpt/xpt_struct.h
--- a/xpcom/typelib/xpt/xpt_struct.h
+++ b/xpcom/typelib/xpt/xpt_struct.h
@@ -256,31 +256,16 @@ struct XPTConstDescriptor {
  * A ParamDescriptor is a variable-size record used to describe either a
  * single argument to a method or a method's result.
  */
 struct XPTParamDescriptor {
   uint8_t flags;
   XPTTypeDescriptor type;
 };
 
-/* flag bits */
-#define XPT_PD_IN       0x80
-#define XPT_PD_OUT      0x40
-#define XPT_PD_RETVAL   0x20
-#define XPT_PD_SHARED   0x10
-#define XPT_PD_DIPPER   0x08
-#define XPT_PD_OPTIONAL 0x04
-
-#define XPT_PD_IS_IN(flags) (flags & XPT_PD_IN)
-#define XPT_PD_IS_OUT(flags) (flags & XPT_PD_OUT)
-#define XPT_PD_IS_RETVAL(flags) (flags & XPT_PD_RETVAL)
-#define XPT_PD_IS_SHARED(flags) (flags & XPT_PD_SHARED)
-#define XPT_PD_IS_DIPPER(flags) (flags & XPT_PD_DIPPER)
-#define XPT_PD_IS_OPTIONAL(flags) (flags & XPT_PD_OPTIONAL)
-
 /*
  * A MethodDescriptor is a variable-size record used to describe a single
  * interface method.
  */
 struct XPTMethodDescriptor {
   char* name;
   XPTParamDescriptor* params;
   //XPTParamDescriptor result; // Present on disk, omitted here.
