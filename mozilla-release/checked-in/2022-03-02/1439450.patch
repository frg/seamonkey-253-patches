# HG changeset patch
# User Dimi Lee <dlee@mozilla.com>
# Date 1539686161 0
# Node ID 37d138b1e58b9c45ee3baef530bb133597b14eb3
# Parent  ce4409635ca1be25918a451f420d7504d7132998
Bug 1439450 - Ignore has_first_value() check in ProtocolParser. r=francois

Sometimes the protocol buffer data (RiceEncodingData) sent by Google's Safe Browsing server has the following properties:

1. |has_first_value| is false
2. |num_entries| > 0

In this case, we can still parse the data and apply partial update correctly by assuming that the first value is equal to 0.

Differential Revision: https://phabricator.services.mozilla.com/D6393

diff --git a/toolkit/components/url-classifier/ProtocolParser.cpp b/toolkit/components/url-classifier/ProtocolParser.cpp
--- a/toolkit/components/url-classifier/ProtocolParser.cpp
+++ b/toolkit/components/url-classifier/ProtocolParser.cpp
@@ -978,44 +978,45 @@ ProtocolParserProtobuf::ProcessRawRemova
 
   return NS_OK;
 }
 
 static nsresult
 DoRiceDeltaDecode(const RiceDeltaEncoding& aEncoding,
                   nsTArray<uint32_t>& aDecoded)
 {
-  if (!aEncoding.has_first_value()) {
-    PARSER_LOG(("The encoding info is incomplete."));
-    return NS_ERROR_FAILURE;
-  }
   if (aEncoding.num_entries() > 0 &&
       (!aEncoding.has_rice_parameter() || !aEncoding.has_encoded_data())) {
     PARSER_LOG(("Rice parameter or encoded data is missing."));
     return NS_ERROR_FAILURE;
+  } else if (aEncoding.num_entries() == 0 && !aEncoding.has_first_value()) {
+    PARSER_LOG(("Missing first_value for an single-integer Rice encoding."));
+    return NS_ERROR_FAILURE;
   }
 
+  auto first_value = aEncoding.has_first_value() ? aEncoding.first_value() : 0;
+
   PARSER_LOG(("* Encoding info:"));
-  PARSER_LOG(("  - First value: %" PRId64, aEncoding.first_value()));
+  PARSER_LOG(("  - First value: %" PRId64, first_value));
   PARSER_LOG(("  - Num of entries: %d", aEncoding.num_entries()));
   PARSER_LOG(("  - Rice parameter: %d", aEncoding.rice_parameter()));
 
   // Set up the input buffer. Note that the bits should be read
   // from LSB to MSB so that we in-place reverse the bits before
   // feeding to the decoder.
   auto encoded = const_cast<RiceDeltaEncoding&>(aEncoding).mutable_encoded_data();
   RiceDeltaDecoder decoder((uint8_t*)encoded->c_str(), encoded->size());
 
   // Setup the output buffer. The "first value" is included in
   // the output buffer.
   aDecoded.SetLength(aEncoding.num_entries() + 1);
 
   // Decode!
   bool rv = decoder.Decode(aEncoding.rice_parameter(),
-                           aEncoding.first_value(), // first value.
+                           first_value,
                            aEncoding.num_entries(), // # of entries (first value not included).
                            &aDecoded[0]);
 
   NS_ENSURE_TRUE(rv, NS_ERROR_FAILURE);
 
   return NS_OK;
 }
 
