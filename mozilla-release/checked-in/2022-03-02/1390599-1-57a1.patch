# HG changeset patch
# User Geoff Brown <gbrown@mozilla.com>
# Date 1502809576 21600
# Node ID e95891f5a161b69192a3290ee9f3eb34ddb03dc1
# Parent  40095d291eb4572b73b9bf0233f1bd15d7774620
Bug 1390599 - 1. Simplify argument passing in xpcshell test harness; r=jmaher

The complexity of the xpcshell test harness has grown over time. I am reluctant
to make it more complex without first trying to simplify it. Here I consolidate
some of the argument passing between functions in an attempt to simplify some
important interfaces. Changes are strictly structural and should result in no
change in behavior.

diff --git a/testing/xpcshell/mach_commands.py b/testing/xpcshell/mach_commands.py
--- a/testing/xpcshell/mach_commands.py
+++ b/testing/xpcshell/mach_commands.py
@@ -129,17 +129,17 @@ class XPCShellRunner(MozbuildObject):
             if isinstance(v, unicode_type):
                 v = v.encode('utf-8')
 
             if isinstance(k, unicode_type):
                 k = k.encode('utf-8')
 
             filtered_args[k] = v
 
-        result = xpcshell.runTests(**filtered_args)
+        result = xpcshell.runTests(filtered_args)
 
         self.log_manager.disable_unstructured()
 
         if not result and not xpcshell.sequential:
             print("Tests were run in parallel. Try running with --sequential "
                   "to make sure the failures were not caused by this.")
         return int(not result)
 
@@ -201,22 +201,20 @@ class AndroidXPCShellRunner(MozbuildObje
                     print ("using APK: %s" % kwargs["localAPK"])
                     break
             else:
                 raise Exception("APK not found in objdir. You must specify an APK.")
 
         if not kwargs["sequential"]:
             kwargs["sequential"] = True
 
-        options = argparse.Namespace(**kwargs)
-        xpcshell = remotexpcshelltests.XPCShellRemote(dm, options, log)
+        xpcshell = remotexpcshelltests.XPCShellRemote(dm, kwargs, log)
 
-        result = xpcshell.runTests(testClass=remotexpcshelltests.RemoteXPCShellTestThread,
-                                   mobileArgs=xpcshell.mobileArgs,
-                                   **vars(options))
+        result = xpcshell.runTests(kwargs, testClass=remotexpcshelltests.RemoteXPCShellTestThread,
+                                   mobileArgs=xpcshell.mobileArgs)
 
         self.log_manager.disable_unstructured()
 
         return int(not result)
 
 
 def get_parser():
     build_obj = MozbuildObject.from_environment(cwd=here)
diff --git a/testing/xpcshell/remotexpcshelltests.py b/testing/xpcshell/remotexpcshelltests.py
--- a/testing/xpcshell/remotexpcshelltests.py
+++ b/testing/xpcshell/remotexpcshelltests.py
@@ -6,16 +6,17 @@
 
 import logging
 import posixpath
 import sys, os
 import subprocess
 import runxpcshelltests as xpcshell
 import tempfile
 import time
+from argparse import Namespace
 from zipfile import ZipFile
 from mozlog import commandline
 import shutil
 import mozdevice
 import mozfile
 import mozinfo
 
 from xpcshellcommandline import parser_remote
@@ -117,17 +118,17 @@ class RemoteXPCShellTestThread(xpcshell.
         self.headJSPath = remoteJoin(self.remoteScriptsDir, 'head.js')
         self.httpdJSPath = remoteJoin(self.remoteComponentsDir, 'httpd.js')
         self.httpdManifest = remoteJoin(self.remoteComponentsDir, 'httpd.manifest')
         self.testingModulesDir = self.remoteModulesDir
         self.testharnessdir = self.remoteScriptsDir
         xpcshell.XPCShellTestThread.buildXpcsCmd(self)
         # remove "-g <dir> -a <dir>" and add "--greomni <apk>"
         del(self.xpcsCmd[1:5])
-        if self.options.localAPK:
+        if self.options['localAPK']:
             self.xpcsCmd.insert(3, '--greomni')
             self.xpcsCmd.insert(4, self.remoteAPK)
 
         if self.remoteDebugger:
             # for example, "/data/local/gdbserver" "localhost:12345"
             self.xpcsCmd = [
               self.remoteDebugger,
               self.remoteDebuggerArgs,
@@ -235,18 +236,18 @@ class XPCShellRemote(xpcshell.XPCShellTe
     def __init__(self, devmgr, options, log):
         xpcshell.XPCShellTests.__init__(self, log)
 
         # Add Android version (SDK level) to mozinfo so that manifest entries
         # can be conditional on android_version.
         androidVersion = devmgr.shellCheckOutput(['getprop', 'ro.build.version.sdk'])
         mozinfo.info['android_version'] = androidVersion
 
-        self.localLib = options.localLib
-        self.localBin = options.localBin
+        self.localLib = options['localLib']
+        self.localBin = options['localBin']
         self.options = options
         self.device = devmgr
         self.pathMapping = []
         self.remoteTestRoot = "%s/xpc" % self.device.deviceRoot
         # remoteBinDir contains xpcshell and its wrapper script, both of which must
         # be executable. Since +x permissions cannot usually be set on /mnt/sdcard,
         # and the test root may be on /mnt/sdcard, remoteBinDir is set to be on
         # /data/local, always.
@@ -258,40 +259,40 @@ class XPCShellRemote(xpcshell.XPCShellTe
         # line can be quite complex.
         self.remoteTmpDir = remoteJoin(self.remoteTestRoot, "tmp")
         self.remoteScriptsDir = self.remoteTestRoot
         self.remoteComponentsDir = remoteJoin(self.remoteTestRoot, "c")
         self.remoteModulesDir = remoteJoin(self.remoteTestRoot, "m")
         self.remoteMinidumpDir = remoteJoin(self.remoteTestRoot, "minidumps")
         self.remoteClearDirScript = remoteJoin(self.remoteBinDir, "cleardir")
         self.profileDir = remoteJoin(self.remoteTestRoot, "p")
-        self.remoteDebugger = options.debugger
-        self.remoteDebuggerArgs = options.debuggerArgs
-        self.testingModulesDir = options.testingModulesDir
+        self.remoteDebugger = options['debugger']
+        self.remoteDebuggerArgs = options['debuggerArgs']
+        self.testingModulesDir = options['testingModulesDir']
 
         self.env = {}
 
-        if self.options.objdir:
-            self.xpcDir = os.path.join(self.options.objdir, "_tests/xpcshell")
+        if options['objdir']:
+            self.xpcDir = os.path.join(options['objdir'], "_tests/xpcshell")
         elif os.path.isdir(os.path.join(here, 'tests')):
             self.xpcDir = os.path.join(here, 'tests')
         else:
             print >> sys.stderr, "Couldn't find local xpcshell test directory"
             sys.exit(1)
 
-        if options.localAPK:
-            self.localAPKContents = ZipFile(options.localAPK)
-        if options.setup:
+        if options['localAPK']:
+            self.localAPKContents = ZipFile(options['localAPK'])
+        if options['setup']:
             self.setupTestDir()
             self.setupUtilities()
             self.setupModules()
         self.setupMinidumpDir()
         self.remoteAPK = None
-        if options.localAPK:
-            self.remoteAPK = remoteJoin(self.remoteBinDir, os.path.basename(options.localAPK))
+        if options['localAPK']:
+            self.remoteAPK = remoteJoin(self.remoteBinDir, os.path.basename(options['localAPK']))
             self.setAppRoot()
 
         # data that needs to be passed to the RemoteXPCShellTestThread
         self.mobileArgs = {
             'device': self.device,
             'remoteBinDir': self.remoteBinDir,
             'remoteScriptsDir': self.remoteScriptsDir,
             'remoteComponentsDir': self.remoteComponentsDir,
@@ -348,32 +349,32 @@ class XPCShellRemote(xpcshell.XPCShellTe
         os.remove(localWrapper)
 
         self.device.chmodDir(self.remoteBinDir)
 
     def buildEnvironment(self):
         self.buildCoreEnvironment()
         self.setLD_LIBRARY_PATH()
         self.env["MOZ_LINKER_CACHE"] = self.remoteBinDir
-        if self.options.localAPK and self.appRoot:
+        if self.options['localAPK'] and self.appRoot:
             self.env["GRE_HOME"] = self.appRoot
         self.env["XPCSHELL_TEST_PROFILE_DIR"] = self.profileDir
         self.env["TMPDIR"] = self.remoteTmpDir
         self.env["HOME"] = self.profileDir
         self.env["XPCSHELL_TEST_TEMP_DIR"] = self.remoteTmpDir
         self.env["XPCSHELL_MINIDUMP_DIR"] = self.remoteMinidumpDir
-        if self.options.setup:
+        if self.options['setup']:
             self.pushWrapper()
 
     def setAppRoot(self):
         # Determine the application root directory associated with the package
         # name used by the Fennec APK.
         self.appRoot = None
         packageName = None
-        if self.options.localAPK:
+        if self.options['localAPK']:
             try:
                 packageName = self.localAPKContents.read("package-name.txt")
                 if packageName:
                     self.appRoot = self.device.getAppRoot(packageName.strip())
             except Exception as detail:
                 print "unable to determine app root: " + str(detail)
                 pass
         return None
@@ -429,25 +430,25 @@ class XPCShellRemote(xpcshell.XPCShellTe
         local = os.path.join(self.localBin, "components/httpd.manifest")
         remoteFile = remoteJoin(self.remoteComponentsDir, "httpd.manifest")
         self.device.pushFile(local, remoteFile)
 
         local = os.path.join(self.localBin, "components/test_necko.xpt")
         remoteFile = remoteJoin(self.remoteComponentsDir, "test_necko.xpt")
         self.device.pushFile(local, remoteFile)
 
-        if self.options.localAPK:
-            remoteFile = remoteJoin(self.remoteBinDir, os.path.basename(self.options.localAPK))
-            self.device.pushFile(self.options.localAPK, remoteFile)
+        if self.options['localAPK']:
+            remoteFile = remoteJoin(self.remoteBinDir, os.path.basename(self.options['localAPK']))
+            self.device.pushFile(self.options['localAPK'], remoteFile)
 
         self.pushLibs()
 
     def pushLibs(self):
         pushed_libs_count = 0
-        if self.options.localAPK:
+        if self.options['localAPK']:
             try:
                 dir = tempfile.mkdtemp()
                 for info in self.localAPKContents.infolist():
                     if info.filename.endswith(".so"):
                         print >> sys.stderr, "Pushing %s.." % info.filename
                         remoteFile = remoteJoin(self.remoteBinDir, os.path.basename(info.filename))
                         self.localAPKContents.extract(info, dir)
                         localFile = os.path.join(dir, info.filename)
@@ -514,43 +515,46 @@ class XPCShellRemote(xpcshell.XPCShellTe
         for test in self.alltests:
             uniqueTestPaths.add(test['here'])
         for testdir in uniqueTestPaths:
             abbrevTestDir = os.path.relpath(testdir, self.xpcDir)
             remoteScriptDir = remoteJoin(self.remoteScriptsDir, abbrevTestDir)
             self.pathMapping.append(PathMapping(testdir, remoteScriptDir))
 
 def verifyRemoteOptions(parser, options):
-    if options.localLib is None:
-        if options.localAPK and options.objdir:
+    if isinstance(options, Namespace):
+        options = vars(options)
+
+    if options['localLib'] is None:
+        if options['localAPK'] and options['objdir']:
             for path in ['dist/fennec', 'fennec/lib']:
-                options.localLib = os.path.join(options.objdir, path)
-                if os.path.isdir(options.localLib):
+                options['localLib'] = os.path.join(options['objdir'], path)
+                if os.path.isdir(options['localLib']):
                     break
             else:
                 parser.error("Couldn't find local library dir, specify --local-lib-dir")
-        elif options.objdir:
-            options.localLib = os.path.join(options.objdir, 'dist/bin')
+        elif options['objdir']:
+            options['localLib'] = os.path.join(options['objdir'], 'dist/bin')
         elif os.path.isfile(os.path.join(here, '..', 'bin', 'xpcshell')):
             # assume tests are being run from a tests.zip
-            options.localLib = os.path.abspath(os.path.join(here, '..', 'bin'))
+            options['localLib'] = os.path.abspath(os.path.join(here, '..', 'bin'))
         else:
             parser.error("Couldn't find local library dir, specify --local-lib-dir")
 
-    if options.localBin is None:
-        if options.objdir:
+    if options['localBin'] is None:
+        if options['objdir']:
             for path in ['dist/bin', 'bin']:
-                options.localBin = os.path.join(options.objdir, path)
-                if os.path.isdir(options.localBin):
+                options['localBin'] = os.path.join(options['objdir'], path)
+                if os.path.isdir(options['localBin']):
                     break
             else:
                 parser.error("Couldn't find local binary dir, specify --local-bin-dir")
         elif os.path.isfile(os.path.join(here, '..', 'bin', 'xpcshell')):
             # assume tests are being run from a tests.zip
-            options.localBin = os.path.abspath(os.path.join(here, '..', 'bin'))
+            options['localBin'] = os.path.abspath(os.path.join(here, '..', 'bin'))
         else:
             parser.error("Couldn't find local binary dir, specify --local-bin-dir")
     return options
 
 class PathMapping:
 
     def __init__(self, localDir, remoteDir):
         self.local = localDir
@@ -574,36 +578,36 @@ def main():
             print >>sys.stderr, "Error: please specify an APK"
             sys.exit(1)
 
     options = verifyRemoteOptions(parser, options)
     log = commandline.setup_logging("Remote XPCShell",
                                     options,
                                     {"tbpl": sys.stdout})
 
-    dm_args = {'deviceRoot': options.remoteTestRoot}
-    if options.deviceIP:
-        dm_args['host'] = options.deviceIP
-        dm_args['port'] = options.devicePort
-    if options.log_tbpl_level == 'debug' or options.log_mach_level == 'debug':
+    dm_args = {'deviceRoot': options['remoteTestRoot']}
+    if options['deviceIP']:
+        dm_args['host'] = options['deviceIP']
+        dm_args['port'] = options['devicePort']
+    if options['log_tbpl_level'] == 'debug' or options['log_mach_level'] == 'debug':
         dm_args['logLevel'] = logging.DEBUG
     dm = mozdevice.DroidADB(**dm_args)
 
-    if options.interactive and not options.testPath:
+    if options['interactive'] and not options['testPath']:
         print >>sys.stderr, "Error: You must specify a test filename in interactive mode!"
         sys.exit(1)
 
-    if options.xpcshell is None:
-        options.xpcshell = "xpcshell"
+    if options['xpcshell'] is None:
+        options['xpcshell'] = "xpcshell"
 
     xpcsh = XPCShellRemote(dm, options, log)
 
     # we don't run concurrent tests on mobile
-    options.sequential = True
+    options['sequential'] = True
 
-    if not xpcsh.runTests(testClass=RemoteXPCShellTestThread,
-                          mobileArgs=xpcsh.mobileArgs,
-                          **vars(options)):
+    if not xpcsh.runTests(options,
+                          testClass=RemoteXPCShellTestThread,
+                          mobileArgs=xpcsh.mobileArgs):
         sys.exit(1)
 
 
 if __name__ == '__main__':
     main()
diff --git a/testing/xpcshell/runxpcshelltests.py b/testing/xpcshell/runxpcshelltests.py
--- a/testing/xpcshell/runxpcshelltests.py
+++ b/testing/xpcshell/runxpcshelltests.py
@@ -15,17 +15,17 @@ import random
 import re
 import shutil
 import signal
 import sys
 import tempfile
 import time
 import traceback
 
-from argparse import ArgumentParser
+from argparse import ArgumentParser, Namespace
 from collections import defaultdict, deque, namedtuple
 from distutils import dir_util
 from functools import partial
 from multiprocessing import cpu_count
 from subprocess import Popen, PIPE, STDOUT
 from tempfile import mkdtemp, gettempdir
 from threading import (
     Timer,
@@ -104,26 +104,25 @@ def cleanup_encoding(s):
 
 """ Control-C handling """
 gotSIGINT = False
 def markGotSIGINT(signum, stackFrame):
     global gotSIGINT
     gotSIGINT = True
 
 class XPCShellTestThread(Thread):
-    def __init__(self, test_object, event, cleanup_dir_list, retry=True,
-            app_dir_key=None, interactive=False,
-            verbose=False, pStdout=None, pStderr=None, keep_going=False,
-            log=None, usingTSan=False, **kwargs):
+    def __init__(self, test_object, retry=True, verbose=False, usingTSan=False,
+            **kwargs):
         Thread.__init__(self)
         self.daemon = True
 
         self.test_object = test_object
-        self.cleanup_dir_list = cleanup_dir_list
         self.retry = retry
+        self.verbose = verbose
+        self.usingTSan = usingTSan
 
         self.appPath = kwargs.get('appPath')
         self.xrePath = kwargs.get('xrePath')
         self.utility_path = kwargs.get('utility_path')
         self.testingModulesDir = kwargs.get('testingModulesDir')
         self.debuggerInfo = kwargs.get('debuggerInfo')
         self.jsDebuggerInfo = kwargs.get('jsDebuggerInfo')
         self.pluginsPath = kwargs.get('pluginsPath')
@@ -137,25 +136,23 @@ class XPCShellTestThread(Thread):
         self.symbolsPath = kwargs.get('symbolsPath')
         self.logfiles = kwargs.get('logfiles')
         self.xpcshell = kwargs.get('xpcshell')
         self.xpcsRunArgs = kwargs.get('xpcsRunArgs')
         self.failureManifest = kwargs.get('failureManifest')
         self.jscovdir = kwargs.get('jscovdir')
         self.stack_fixer_function = kwargs.get('stack_fixer_function')
         self._rootTempDir = kwargs.get('tempDir')
-
-        self.app_dir_key = app_dir_key
-        self.interactive = interactive
-        self.verbose = verbose
-        self.pStdout = pStdout
-        self.pStderr = pStderr
-        self.keep_going = keep_going
-        self.log = log
-        self.usingTSan = usingTSan
+        self.cleanup_dir_list = kwargs.get('cleanup_dir_list')
+        self.pStdout = kwargs.get('pStdout')
+        self.pStderr = kwargs.get('pStderr')
+        self.keep_going = kwargs.get('keep_going')
+        self.log = kwargs.get('log')
+        self.app_dir_key = kwargs.get('app_dir_key')
+        self.interactive = kwargs.get('interactive')
 
         # only one of these will be set to 1. adding them to the totals in
         # the harness
         self.passCount = 0
         self.todoCount = 0
         self.failCount = 0
 
         # Context for output processing
@@ -163,17 +160,17 @@ class XPCShellTestThread(Thread):
         self.has_failure_output = False
         self.saw_proc_start = False
         self.saw_proc_end = False
         self.complete_command = None
         self.harness_timeout = kwargs.get('harness_timeout')
         self.timedout = False
 
         # event from main thread to signal work done
-        self.event = event
+        self.event = kwargs.get('event')
         self.done = False # explicitly set flag so we don't rely on thread.isAlive
 
     def run(self):
         try:
             self.run_test()
         except Exception as e:
             self.exception = e
             self.traceback = traceback.format_exc()
@@ -1109,130 +1106,93 @@ class XPCShellTests(object):
         else:
             self.xpcsRunArgs = ['-e', '_execute_test(); quit(0);']
 
     def addTestResults(self, test):
         self.passCount += test.passCount
         self.failCount += test.failCount
         self.todoCount += test.todoCount
 
-    def runTests(self, xpcshell=None, xrePath=None, appPath=None, symbolsPath=None,
-                 manifest=None, testPaths=None, mobileArgs=None, tempDir=None,
-                 interactive=False, verbose=False, keepGoing=False, logfiles=True,
-                 thisChunk=1, totalChunks=1, debugger=None,
-                 debuggerArgs=None, debuggerInteractive=False,
-                 profileName=None, mozInfo=None, sequential=False, shuffle=False,
-                 testingModulesDir=None, pluginsPath=None,
-                 testClass=XPCShellTestThread, failureManifest=None,
-                 log=None, stream=None, jsDebugger=False, jsDebuggerPort=0,
-                 test_tags=None, dump_tests=None, utility_path=None,
-                 rerun_failures=False, threadCount=NUM_THREADS,
-                 failure_manifest=None, jscovdir=None, **otherOptions):
-        """Run xpcshell tests.
-
-        |xpcshell|, is the xpcshell executable to use to run the tests.
-        |xrePath|, if provided, is the path to the XRE to use.
-        |appPath|, if provided, is the path to an application directory.
-        |symbolsPath|, if provided is the path to a directory containing
-          breakpad symbols for processing crashes in tests.
-        |manifest|, if provided, is a file containing a list of
-          test directories to run.
-        |testPaths|, if provided, is a list of paths to files or directories containing
-                     tests to run.
-        |pluginsPath|, if provided, custom plugins directory to be returned from
-          the xpcshell dir svc provider for NS_APP_PLUGINS_DIR_LIST.
-        |interactive|, if set to True, indicates to provide an xpcshell prompt
-          instead of automatically executing the test.
-        |verbose|, if set to True, will cause stdout/stderr from tests to
-          be printed always
-        |logfiles|, if set to False, indicates not to save output to log files.
-          Non-interactive only option.
-        |debugger|, if set, specifies the name of the debugger that will be used
-          to launch xpcshell.
-        |debuggerArgs|, if set, specifies arguments to use with the debugger.
-        |debuggerInteractive|, if set, allows the debugger to be run in interactive
-          mode.
-        |profileName|, if set, specifies the name of the application for the profile
-          directory if running only a subset of tests.
-        |mozInfo|, if set, specifies specifies build configuration information, either as a filename containing JSON, or a dict.
-        |shuffle|, if True, execute tests in random order.
-        |testingModulesDir|, if provided, specifies where JS modules reside.
-          xpcshell will register a resource handler mapping this path.
-        |tempDir|, if provided, specifies a temporary directory to use.
-        |otherOptions| may be present for the convenience of subclasses
+    def runTests(self, options, testClass=XPCShellTestThread, mobileArgs=None):
+        """
+          Run xpcshell tests.
         """
 
         global gotSIGINT
 
+        if isinstance(options, Namespace):
+            options = vars(options)
+
         # Try to guess modules directory.
         # This somewhat grotesque hack allows the buildbot machines to find the
         # modules directory without having to configure the buildbot hosts. This
         # code path should never be executed in local runs because the build system
         # should always set this argument.
-        if not testingModulesDir:
+        if not options.get('testingModulesDir'):
             possible = os.path.join(here, os.path.pardir, 'modules')
 
             if os.path.isdir(possible):
                 testingModulesDir = possible
 
-        if rerun_failures:
-            if os.path.exists(failure_manifest):
-                rerun_manifest = os.path.join(os.path.dirname(failure_manifest), "rerun.ini")
-                shutil.copyfile(failure_manifest, rerun_manifest)
-                os.remove(failure_manifest)
+        if options.get('rerun_failures'):
+            if os.path.exists(options.get('failure_manifest')):
+                rerun_manifest = os.path.join(os.path.dirname(options['failure_manifest']), "rerun.ini")
+                shutil.copyfile(options['failure_manifest'], rerun_manifest)
+                os.remove(options['failure_manifest'])
                 manifest = rerun_manifest
             else:
                 print >> sys.stderr, "No failures were found to re-run."
                 sys.exit(1)
 
-        if testingModulesDir:
+        if options.get('testingModulesDir'):
             # The resource loader expects native paths. Depending on how we were
             # invoked, a UNIX style path may sneak in on Windows. We try to
             # normalize that.
-            testingModulesDir = os.path.normpath(testingModulesDir)
+            testingModulesDir = os.path.normpath(options['testingModulesDir'])
 
             if not os.path.isabs(testingModulesDir):
                 testingModulesDir = os.path.abspath(testingModulesDir)
 
             if not testingModulesDir.endswith(os.path.sep):
                 testingModulesDir += os.path.sep
 
         self.debuggerInfo = None
 
-        if debugger:
-            self.debuggerInfo = mozdebug.get_debugger_info(debugger, debuggerArgs, debuggerInteractive)
+        if options.get('debugger'):
+            self.debuggerInfo = mozdebug.get_debugger_info(options.get('debugger'),
+                options.get('debuggerArgs'), options.get('debuggerInteractive'))
 
         self.jsDebuggerInfo = None
-        if jsDebugger:
+        if options.get('jsDebugger'):
             # A namedtuple let's us keep .port instead of ['port']
             JSDebuggerInfo = namedtuple('JSDebuggerInfo', ['port'])
-            self.jsDebuggerInfo = JSDebuggerInfo(port=jsDebuggerPort)
+            self.jsDebuggerInfo = JSDebuggerInfo(port=options['jsDebuggerPort'])
 
-        self.xpcshell = xpcshell
-        self.xrePath = xrePath
-        self.utility_path = utility_path
-        self.appPath = appPath
-        self.symbolsPath = symbolsPath
-        self.tempDir = os.path.normpath(tempDir or tempfile.gettempdir())
-        self.manifest = manifest
-        self.dump_tests = dump_tests
-        self.interactive = interactive
-        self.verbose = verbose
-        self.keepGoing = keepGoing
-        self.logfiles = logfiles
-        self.totalChunks = totalChunks
-        self.thisChunk = thisChunk
-        self.profileName = profileName or "xpcshell"
-        self.mozInfo = mozInfo
+        self.xpcshell = options.get('xpcshell')
+        self.xrePath = options.get('xrePath')
+        self.utility_path = options.get('utility_path')
+        self.appPath = options.get('appPath')
+        self.symbolsPath = options.get('symbolsPath')
+        self.tempDir = os.path.normpath(options.get('tempDir') or tempfile.gettempdir())
+        self.manifest = options.get('manifest')
+        self.dump_tests = options.get('dump_tests')
+        self.interactive = options.get('interactive')
+        self.verbose = options.get('verbose')
+        self.keepGoing = options.get('keepGoing')
+        self.logfiles = options.get('logfiles')
+        self.totalChunks = options.get('totalChunks')
+        self.thisChunk = options.get('thisChunk')
+        self.profileName = options.get('profileName') or "xpcshell"
+        self.mozInfo = options.get('mozInfo')
         self.testingModulesDir = testingModulesDir
-        self.pluginsPath = pluginsPath
-        self.sequential = sequential
-        self.failure_manifest = failure_manifest
-        self.threadCount = threadCount or NUM_THREADS
-        self.jscovdir = jscovdir
+        self.pluginsPath = options.get('pluginsPath')
+        self.sequential = options.get('sequential')
+        self.failure_manifest = options.get('failure_manifest')
+        self.threadCount = options.get('threadCount') or NUM_THREADS
+        self.jscovdir = options.get('jscovdir')
 
         self.testCount = 0
         self.passCount = 0
         self.failCount = 0
         self.todoCount = 0
 
         self.setAbsPath()
         self.buildXpcsRunArgs()
@@ -1279,21 +1239,21 @@ class XPCShellTests(object):
             appDirKey = self.mozInfo["appname"] + "-appdir"
 
         # We have to do this before we run tests that depend on having the node
         # http/2 server.
         self.trySetupNode()
 
         pStdout, pStderr = self.getPipes()
 
-        self.buildTestList(test_tags, testPaths)
+        self.buildTestList(options.get('test_tags'), options.get('testPaths'))
         if self.singleFile:
             self.sequential = True
 
-        if shuffle:
+        if options.get('shuffle'):
             random.shuffle(self.alltests)
 
         self.cleanup_dir_list = []
         self.try_again_list = []
 
         kwargs = {
             'appPath': self.appPath,
             'xrePath': self.xrePath,
@@ -1313,16 +1273,24 @@ class XPCShellTests(object):
             'symbolsPath': self.symbolsPath,
             'logfiles': self.logfiles,
             'xpcshell': self.xpcshell,
             'xpcsRunArgs': self.xpcsRunArgs,
             'failureManifest': self.failure_manifest,
             'jscovdir': self.jscovdir,
             'harness_timeout': self.harness_timeout,
             'stack_fixer_function': self.stack_fixer_function,
+            'event': self.event,
+            'cleanup_dir_list': self.cleanup_dir_list,
+            'pStdout': pStdout,
+            'pStderr': pStderr,
+            'keep_going': self.keepGoing,
+            'log': self.log,
+            'interactive': self.interactive,
+            'app_dir_key': appDirKey
         }
 
         if self.sequential:
             # Allow user to kill hung xpcshell subprocess with SIGINT
             # when we are only running tests sequentially.
             signal.signal(signal.SIGINT, markGotSIGINT)
 
         if self.debuggerInfo:
@@ -1360,22 +1328,19 @@ class XPCShellTests(object):
 
             path = test_object['path']
 
             if self.singleFile and not path.endswith(self.singleFile):
                 continue
 
             self.testCount += 1
 
-            test = testClass(test_object, self.event, self.cleanup_dir_list,
-                    app_dir_key=appDirKey,
-                    interactive=interactive,
-                    verbose=verbose or test_object.get("verbose") == "true",
-                    pStdout=pStdout, pStderr=pStderr,
-                    keep_going=keepGoing, log=self.log, usingTSan=usingTSan,
+            test = testClass(test_object,
+                    verbose=self.verbose or test_object.get("verbose") == "true",
+                    usingTSan=usingTSan,
                     mobileArgs=mobileArgs, **kwargs)
             if 'run-sequentially' in test_object or self.sequential:
                 sequential_tests.append(test)
             else:
                 tests_queue.append(test)
 
         if self.sequential:
             self.log.info("Running tests sequentially.")
@@ -1456,21 +1421,20 @@ class XPCShellTests(object):
                     tracebacks.append(test.traceback)
                     break
                 keep_going = test.keep_going
 
         # retry tests that failed when run in parallel
         if self.try_again_list:
             self.log.info("Retrying tests that failed when run in parallel.")
         for test_object in self.try_again_list:
-            test = testClass(test_object, self.event, self.cleanup_dir_list,
+            test = testClass(test_object,
                     retry=False,
-                    app_dir_key=appDirKey, interactive=interactive,
-                    verbose=verbose, pStdout=pStdout, pStderr=pStderr,
-                    keep_going=keepGoing, log=self.log, mobileArgs=mobileArgs,
+                    verbose=self.verbose,
+                    mobileArgs=mobileArgs,
                     **kwargs)
             test.start()
             test.join()
             self.addTestResults(test)
             # did the test encounter any exception?
             if test.exception:
                 exceptions.append(test.exception)
                 tracebacks.append(test.traceback)
@@ -1526,13 +1490,13 @@ def main():
         print >> sys.stderr, """Must provide path to xpcshell using --xpcshell"""
 
     xpcsh = XPCShellTests(log)
 
     if options.interactive and not options.testPath:
         print >>sys.stderr, "Error: You must specify a test filename in interactive mode!"
         sys.exit(1)
 
-    if not xpcsh.runTests(**vars(options)):
+    if not xpcsh.runTests(options):
         sys.exit(1)
 
 if __name__ == '__main__':
     main()
diff --git a/testing/xpcshell/selftest.py b/testing/xpcshell/selftest.py
--- a/testing/xpcshell/selftest.py
+++ b/testing/xpcshell/selftest.py
@@ -498,26 +498,28 @@ tail =
 
 """ + "\n".join(testlines))
 
     def assertTestResult(self, expected, shuffle=False, verbose=False):
         """
         Assert that self.x.runTests with manifest=self.manifest
         returns |expected|.
         """
+        kwargs = {}
+        kwargs['xpcshell'] = xpcshellBin
+        kwargs['symbolsPath'] = self.symbols_path
+        kwargs['manifest'] = self.manifest
+        kwargs['mozInfo'] = mozinfo.info
+        kwargs['shuffle'] = shuffle
+        kwargs['verbose'] = verbose
+        kwargs['sequential'] = True
+        kwargs['testingModulesDir'] = os.path.join(objdir, '_tests', 'modules')
+        kwargs['utility_path'] = self.utility_path
         self.assertEquals(expected,
-                          self.x.runTests(xpcshellBin,
-                                          symbolsPath=self.symbols_path,
-                                          manifest=self.manifest,
-                                          mozInfo=mozinfo.info,
-                                          shuffle=shuffle,
-                                          verbose=verbose,
-                                          sequential=True,
-                                          testingModulesDir=os.path.join(objdir, '_tests', 'modules'),
-                                          utility_path=self.utility_path),
+                          self.x.runTests(kwargs),
                           msg="""Tests should have %s, log:
 ========
 %s
 ========
 """ % ("passed" if expected else "failed", self.log.getvalue()))
 
     def _assertLog(self, s, expected):
         l = self.log.getvalue()

