# HG changeset patch
# User Dragana Damjanovic dd.mozilla@gmail.com
# Date 1502114190 -7200
#      Mon Aug 07 15:56:30 2017 +0200
# Node ID 44fec2ed960a03a1d25626ea1b8014e6a234d508
# Parent  55cdf1551bc9e904b7105b82ddf0d5bae31f65be
Bug 1381282 - Change nsScriptErrorBase::InitWithWindowID so that it does not call GetSensitiveInfoHiddenSpec as much as now. r=bz r=valentin

diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -4120,30 +4120,36 @@ nsContentUtils::ReportToConsoleByWindowI
 
   nsAutoCString spec;
   if (!aLineNumber && aLocationMode == eUSE_CALLING_LOCATION) {
     JSContext *cx = GetCurrentJSContext();
     if (cx) {
       nsJSUtils::GetCallingLocation(cx, spec, &aLineNumber, &aColumnNumber);
     }
   }
-  if (spec.IsEmpty() && aURI) {
-    spec = aURI->GetSpecOrDefault();
-  }
 
   nsCOMPtr<nsIScriptError> errorObject =
       do_CreateInstance(NS_SCRIPTERROR_CONTRACTID, &rv);
   NS_ENSURE_SUCCESS(rv, rv);
 
-  rv = errorObject->InitWithWindowID(aErrorText,
-                                     NS_ConvertUTF8toUTF16(spec), // file name
-                                     aSourceLine,
-                                     aLineNumber, aColumnNumber,
-                                     aErrorFlags, aCategory,
-                                     aInnerWindowID);
+  if (!spec.IsEmpty()) {
+    rv = errorObject->InitWithWindowID(aErrorText,
+                                       NS_ConvertUTF8toUTF16(spec), // file name
+                                       aSourceLine,
+                                       aLineNumber, aColumnNumber,
+                                       aErrorFlags, aCategory,
+                                       aInnerWindowID);
+  } else {
+    rv = errorObject->InitWithSourceURI(aErrorText,
+                                        aURI,
+                                        aSourceLine,
+                                        aLineNumber, aColumnNumber,
+                                        aErrorFlags, aCategory,
+                                        aInnerWindowID);
+  }
   NS_ENSURE_SUCCESS(rv, rv);
 
   return sConsoleService->LogMessage(errorObject);
 }
 
 void
 nsContentUtils::LogMessageToConsole(const char* aMsg)
 {
diff --git a/dom/bindings/nsIScriptError.idl b/dom/bindings/nsIScriptError.idl
--- a/dom/bindings/nsIScriptError.idl
+++ b/dom/bindings/nsIScriptError.idl
@@ -6,16 +6,17 @@
 /*
  * nsIConsoleMessage subclass for representing JavaScript errors and warnings.
  */
 
 
 #include "nsISupports.idl"
 #include "nsIArray.idl"
 #include "nsIConsoleMessage.idl"
+interface nsIURI;
 
 %{C++
 #include "nsString.h" // for nsDependentCString
 %}
 
 [scriptable, uuid(e8933fc9-c302-4e12-a55b-4f88611d9c6c)]
 interface nsIScriptErrorNote : nsISupports
 {
@@ -92,42 +93,108 @@ interface nsIScriptError : nsIConsoleMes
               in AString sourceName,
               in AString sourceLine,
               in uint32_t lineNumber,
               in uint32_t columnNumber,
               in uint32_t flags,
               in string category);
 
     /* This should be called instead of nsIScriptError.init to
-       initialize with a window id.  The window id should be for the
-       inner window associated with this error. */
+     * initialize with a window id.  The window id should be for the
+     * inner window associated with this error.
+     *
+     * This function will check whether sourceName is a uri and sanitize it if
+     * needed. If you know the source name is sanitized already, use
+     * initWithSanitizedSource.
+     * A "sanitized" source name means that passwords are not shown. It will
+     * use the sensitiveInfoHiddenSpec function of nsIURI interface, that is
+     * replacing paswords with ***
+     * (e.g. https://USERNAME:****@example.com/some/path).
+     */
     void initWithWindowID(in AString message,
                           in AString sourceName,
                           in AString sourceLine,
                           in uint32_t lineNumber,
                           in uint32_t columnNumber,
                           in uint32_t flags,
                           in ACString category,
                           in unsigned long long innerWindowID);
+
+    /* This is the same function as initWithWindowID, but it expects an already
+     * sanitized sourceName.
+     * Please use it only if sourceName string is already sanitized.
+     */
+    void initWithSanitizedSource(in AString message,
+                                 in AString sourceName,
+                                 in AString sourceLine,
+                                 in uint32_t lineNumber,
+                                 in uint32_t columnNumber,
+                                 in uint32_t flags,
+                                 in ACString category,
+                                 in unsigned long long innerWindowID);
+
+    /* This is the same function as initWithWindowID with an uri as a source parameter.
+     */
+    void initWithSourceURI(in AString message,
+                           in nsIURI sourceURI,
+                           in AString sourceLine,
+                           in uint32_t lineNumber,
+                           in uint32_t columnNumber,
+                           in uint32_t flags,
+                           in ACString category,
+                           in unsigned long long innerWindowID);
+
 %{C++
-    // This overload allows passing a literal string for category.
+    // These overloads allow passing a literal string for category.
     template<uint32_t N>
     nsresult InitWithWindowID(const nsAString& message,
                               const nsAString& sourceName,
                               const nsAString& sourceLine,
                               uint32_t lineNumber,
                               uint32_t columnNumber,
                               uint32_t flags,
                               const char (&c)[N],
                               uint64_t aInnerWindowID)
     {
         nsDependentCString category(c, N - 1);
         return InitWithWindowID(message, sourceName, sourceLine, lineNumber,
                                 columnNumber, flags, category, aInnerWindowID);
     }
+
+    template<uint32_t N>
+    nsresult InitWithSanitizedSource(const nsAString& message,
+                                     const nsAString& sourceName,
+                                     const nsAString& sourceLine,
+                                     uint32_t lineNumber,
+                                     uint32_t columnNumber,
+                                     uint32_t flags,
+                                     const char (&c)[N],
+                                     uint64_t aInnerWindowID)
+    {
+        nsDependentCString category(c, N - 1);
+        return InitWithSanitizedSource(message, sourceName, sourceLine,
+                                       lineNumber, columnNumber, flags,
+                                       category, aInnerWindowID);
+    }
+
+    template<uint32_t N>
+    nsresult InitWithSourceURI(const nsAString& message,
+                               nsIURI* sourceURI,
+                               const nsAString& sourceLine,
+                               uint32_t lineNumber,
+                               uint32_t columnNumber,
+                               uint32_t flags,
+                               const char (&c)[N],
+                               uint64_t aInnerWindowID)
+    {
+        nsDependentCString category(c, N - 1);
+        return InitWithSourceURI(message, sourceURI, sourceLine,
+                                 lineNumber, columnNumber, flags,
+                                 category, aInnerWindowID);
+    }
 %}
 
 };
 
 %{ C++
 #define NS_SCRIPTERROR_CID \
 { 0x1950539a, 0x90f0, 0x4d22, { 0xb5, 0xaf, 0x71, 0x32, 0x9c, 0x68, 0xfa, 0x35 }}
 
diff --git a/dom/bindings/nsScriptError.cpp b/dom/bindings/nsScriptError.cpp
--- a/dom/bindings/nsScriptError.cpp
+++ b/dom/bindings/nsScriptError.cpp
@@ -205,43 +205,104 @@ AssignSourceNameHelper(nsString& aSource
     aSourceNameDest.Assign(aSourceNameSrc);
 
     nsCOMPtr<nsIURI> uri;
     nsAutoCString pass;
     if (NS_SUCCEEDED(NS_NewURI(getter_AddRefs(uri), aSourceNameSrc)) &&
         NS_SUCCEEDED(uri->GetPassword(pass)) &&
         !pass.IsEmpty())
     {
-        nsCOMPtr<nsISensitiveInfoHiddenURI> safeUri = do_QueryInterface(uri);
+        NS_GetSanitizedURIStringFromURI(uri, aSourceNameDest);
+    }
+}
+
+static void
+AssignSourceNameHelper(nsIURI* aSourceURI, nsString& aSourceNameDest)
+{
+    if (!aSourceURI)
+        return;
+
+    if (NS_FAILED(NS_GetSanitizedURIStringFromURI(aSourceURI,
+                                                  aSourceNameDest))) {
+        aSourceNameDest.AssignLiteral("[nsIURI::GetSpec failed]");
+    }
+}
 
-        nsAutoCString loc;
-        if (safeUri && NS_SUCCEEDED(safeUri->GetSensitiveInfoHiddenSpec(loc)))
-            aSourceNameDest.Assign(NS_ConvertUTF8toUTF16(loc));
-    }
+void
+nsScriptErrorBase::InitializationHelper(const nsAString& message,
+                                        const nsAString& sourceLine,
+                                        uint32_t lineNumber,
+                                        uint32_t columnNumber,
+                                        uint32_t flags,
+                                        const nsACString& category,
+                                        uint64_t aInnerWindowID)
+{
+    mMessage.Assign(message);
+    mLineNumber = lineNumber;
+    mSourceLine.Assign(sourceLine);
+    mColumnNumber = columnNumber;
+    mFlags = flags;
+    mCategory = category;
+    mTimeStamp = JS_Now() / 1000;
+    mInnerWindowID = aInnerWindowID;
 }
 
 NS_IMETHODIMP
 nsScriptErrorBase::InitWithWindowID(const nsAString& message,
                                     const nsAString& sourceName,
                                     const nsAString& sourceLine,
                                     uint32_t lineNumber,
                                     uint32_t columnNumber,
                                     uint32_t flags,
                                     const nsACString& category,
                                     uint64_t aInnerWindowID)
 {
-    mMessage.Assign(message);
+    InitializationHelper(message, sourceLine, lineNumber, columnNumber, flags,
+                         category, aInnerWindowID);
     AssignSourceNameHelper(mSourceName, sourceName);
-    mLineNumber = lineNumber;
-    mSourceLine.Assign(sourceLine);
-    mColumnNumber = columnNumber;
-    mFlags = flags;
-    mCategory = category;
-    mTimeStamp = JS_Now() / 1000;
-    mInnerWindowID = aInnerWindowID;
+
+    if (aInnerWindowID && NS_IsMainThread())
+        InitializeOnMainThread();
+
+    return NS_OK;
+}
+
+NS_IMETHODIMP
+nsScriptErrorBase::InitWithSanitizedSource(const nsAString& message,
+                                           const nsAString& sourceName,
+                                           const nsAString& sourceLine,
+                                           uint32_t lineNumber,
+                                           uint32_t columnNumber,
+                                           uint32_t flags,
+                                           const nsACString& category,
+                                           uint64_t aInnerWindowID)
+{
+    InitializationHelper(message, sourceLine, lineNumber, columnNumber, flags,
+                         category, aInnerWindowID);
+    mSourceName = sourceName;
+
+    if (aInnerWindowID && NS_IsMainThread())
+        InitializeOnMainThread();
+
+    return NS_OK;
+}
+
+NS_IMETHODIMP
+nsScriptErrorBase::InitWithSourceURI(const nsAString& message,
+                                     nsIURI *sourceURI,
+                                     const nsAString& sourceLine,
+                                     uint32_t lineNumber,
+                                     uint32_t columnNumber,
+                                     uint32_t flags,
+                                     const nsACString& category,
+                                     uint64_t aInnerWindowID)
+{
+    InitializationHelper(message, sourceLine, lineNumber, columnNumber, flags,
+                         category, aInnerWindowID);
+    AssignSourceNameHelper(sourceURI, mSourceName);
 
     if (aInnerWindowID && NS_IsMainThread())
         InitializeOnMainThread();
 
     return NS_OK;
 }
 
 static nsresult
diff --git a/dom/bindings/nsScriptError.h b/dom/bindings/nsScriptError.h
--- a/dom/bindings/nsScriptError.h
+++ b/dom/bindings/nsScriptError.h
@@ -48,16 +48,22 @@ public:
   void AddNote(nsIScriptErrorNote* note);
 
 protected:
   virtual ~nsScriptErrorBase();
 
   void
   InitializeOnMainThread();
 
+  void InitializationHelper(const nsAString& message,
+                            const nsAString& sourceLine, uint32_t lineNumber,
+                            uint32_t columnNumber, uint32_t flags,
+                            const nsACString& category,
+                            uint64_t aInnerWindowID);
+
   nsCOMArray<nsIScriptErrorNote> mNotes;
   nsString mMessage;
   nsString mMessageName;
   nsString mSourceName;
   uint32_t mLineNumber;
   nsString mSourceLine;
   uint32_t mColumnNumber;
   uint32_t mFlags;
diff --git a/dom/security/FramingChecker.cpp b/dom/security/FramingChecker.cpp
--- a/dom/security/FramingChecker.cpp
+++ b/dom/security/FramingChecker.cpp
@@ -325,18 +325,20 @@ FramingChecker::ReportXFOViolation(nsIDo
       break;
     case eALLOWFROM:
       msg.AppendLiteral(" does not permit framing by ");
       msg.Append(NS_ConvertUTF8toUTF16(topURIString));
       msg.Append('.');
       break;
   }
 
-  rv = errorObject->InitWithWindowID(msg, EmptyString(), EmptyString(), 0, 0,
-                                     nsIScriptError::errorFlag,
-                                     "X-Frame-Options",
-                                     topInnerWindow->WindowID());
+  // It is ok to use InitWithSanitizedSource, because the source string is
+  // empty.
+  rv = errorObject->InitWithSanitizedSource(msg, EmptyString(), EmptyString(),
+                                            0, 0, nsIScriptError::errorFlag,
+                                            "X-Frame-Options",
+                                            topInnerWindow->WindowID());
   if (NS_FAILED(rv)) {
     return;
   }
 
   consoleService->LogMessage(errorObject);
 }
diff --git a/dom/xml/XMLDocument.cpp b/dom/xml/XMLDocument.cpp
--- a/dom/xml/XMLDocument.cpp
+++ b/dom/xml/XMLDocument.cpp
@@ -343,38 +343,36 @@ XMLDocument::Load(const nsAString& aUrl,
   }
 
   if (nsContentUtils::IsSystemPrincipal(principal)) {
     // We're called from chrome, check to make sure the URI we're
     // about to load is also chrome.
 
     bool isChrome = false;
     if (NS_FAILED(uri->SchemeIs("chrome", &isChrome)) || !isChrome) {
-      nsAutoCString spec;
-      if (mDocumentURI)
-        mDocumentURI->GetSpec(spec);
 
       nsAutoString error;
       error.AssignLiteral("Cross site loading using document.load is no "
                           "longer supported. Use XMLHttpRequest instead.");
       nsCOMPtr<nsIScriptError> errorObject =
           do_CreateInstance(NS_SCRIPTERROR_CONTRACTID, &rv);
       if (NS_FAILED(rv)) {
         aRv.Throw(rv);
         return false;
       }
 
-      rv = errorObject->InitWithWindowID(error,
-                                         NS_ConvertUTF8toUTF16(spec),
-                                         EmptyString(),
-                                         0, 0, nsIScriptError::warningFlag,
-                                         "DOM",
-                                         callingDoc ?
-                                           callingDoc->InnerWindowID() :
-                                           this->InnerWindowID());
+      rv = errorObject->InitWithSourceURI(error,
+                                          mDocumentURI,
+                                          EmptyString(),
+                                          0, 0,
+                                          nsIScriptError::warningFlag,
+                                          "DOM",
+                                          callingDoc ?
+                                          callingDoc->InnerWindowID() :
+                                          this->InnerWindowID());
 
       if (NS_FAILED(rv)) {
         aRv.Throw(rv);
         return false;
       }
 
       nsCOMPtr<nsIConsoleService> consoleService =
         do_GetService(NS_CONSOLESERVICE_CONTRACTID);
diff --git a/layout/style/ErrorReporter.cpp b/layout/style/ErrorReporter.cpp
--- a/layout/style/ErrorReporter.cpp
+++ b/layout/style/ErrorReporter.cpp
@@ -12,42 +12,41 @@
 #include "mozilla/Preferences.h"
 #include "mozilla/Services.h"
 #include "mozilla/SystemGroup.h"
 #include "nsCSSScanner.h"
 #include "nsIConsoleService.h"
 #include "nsIDocument.h"
 #include "nsIFactory.h"
 #include "nsIScriptError.h"
+#include "nsISensitiveInfoHiddenURI.h"
 #include "nsIStringBundle.h"
 #include "nsServiceManagerUtils.h"
 #include "nsStyleUtil.h"
 #include "nsThreadUtils.h"
+#include "nsNetUtil.h"
 
 #ifdef CSS_REPORT_PARSE_ERRORS
 
 using namespace mozilla;
 
 namespace {
 class ShortTermURISpecCache : public Runnable {
 public:
   ShortTermURISpecCache()
    : Runnable("ShortTermURISpecCache")
    , mPending(false) {}
 
   nsString const& GetSpec(nsIURI* aURI) {
     if (mURI != aURI) {
       mURI = aURI;
 
-      nsAutoCString cSpec;
-      nsresult rv = mURI->GetSpec(cSpec);
-      if (NS_FAILED(rv)) {
-        cSpec.AssignLiteral("[nsIURI::GetSpec failed]");
+      if (NS_FAILED(NS_GetSanitizedURIStringFromURI(mURI, mSpec))) {
+        mSpec.AssignLiteral("[nsIURI::GetSpec failed]");
       }
-      CopyUTF8toUTF16(cSpec, mSpec);
     }
     return mSpec;
   }
 
   bool IsInUse() const { return mURI != nullptr; }
   bool IsPending() const { return mPending; }
   void SetPending() { mPending = true; }
 
@@ -214,24 +213,26 @@ ErrorReporter::OutputError()
     }
   }
 
   nsresult rv;
   nsCOMPtr<nsIScriptError> errorObject =
     do_CreateInstance(sScriptErrorFactory, &rv);
 
   if (NS_SUCCEEDED(rv)) {
-    rv = errorObject->InitWithWindowID(mError,
-                                       mFileName,
-                                       mErrorLine,
-                                       mErrorLineNumber,
-                                       mErrorColNumber,
-                                       nsIScriptError::warningFlag,
-                                       "CSS Parser",
-                                       mInnerWindowID);
+    // It is safe to used InitWithSanitizedSource because mFileName is
+    // an already anonymized uri spec.
+    rv = errorObject->InitWithSanitizedSource(mError,
+                                              mFileName,
+                                              mErrorLine,
+                                              mErrorLineNumber,
+                                              mErrorColNumber,
+                                              nsIScriptError::warningFlag,
+                                              "CSS Parser",
+                                              mInnerWindowID);
     if (NS_SUCCEEDED(rv)) {
       sConsoleService->LogMessage(errorObject);
     }
   }
 
   ClearError();
 }
 
diff --git a/netwerk/base/nsNetUtil.cpp b/netwerk/base/nsNetUtil.cpp
--- a/netwerk/base/nsNetUtil.cpp
+++ b/netwerk/base/nsNetUtil.cpp
@@ -41,16 +41,17 @@
 #include "nsIOfflineCacheUpdate.h"
 #include "nsIPersistentProperties2.h"
 #include "nsIPrivateBrowsingChannel.h"
 #include "nsIPropertyBag2.h"
 #include "nsIProtocolProxyService.h"
 #include "nsIRedirectChannelRegistrar.h"
 #include "nsIRequestObserverProxy.h"
 #include "nsIScriptSecurityManager.h"
+#include "nsISensitiveInfoHiddenURI.h"
 #include "nsISimpleStreamListener.h"
 #include "nsISocketProvider.h"
 #include "nsISocketProviderService.h"
 #include "nsIStandardURL.h"
 #include "nsIStreamLoader.h"
 #include "nsIIncrementalStreamLoader.h"
 #include "nsIStreamTransportService.h"
 #include "nsStringStream.h"
@@ -1581,16 +1582,36 @@ NS_NewURI(nsIURI **result,
           const char *spec,
           nsIURI *baseURI /* = nullptr */,
           nsIIOService *ioService /* = nullptr */)     // pass in nsIIOService to optimize callers
 {
     return NS_NewURI(result, nsDependentCString(spec), nullptr, baseURI, ioService);
 }
 
 nsresult
+NS_GetSanitizedURIStringFromURI(nsIURI *aUri, nsAString &aSanitizedSpec)
+{
+    aSanitizedSpec.Truncate();
+
+    nsCOMPtr<nsISensitiveInfoHiddenURI> safeUri = do_QueryInterface(aUri);
+    nsAutoCString cSpec;
+    nsresult rv;
+    if (safeUri) {
+        rv = safeUri->GetSensitiveInfoHiddenSpec(cSpec);
+    } else {
+        rv = aUri->GetSpec(cSpec);
+    }
+
+    if (NS_SUCCEEDED(rv)) {
+        aSanitizedSpec.Assign(NS_ConvertUTF8toUTF16(cSpec));
+    }
+    return rv;
+}
+
+nsresult
 NS_LoadPersistentPropertiesFromURISpec(nsIPersistentProperties **outResult,
                                        const nsACString         &aSpec)
 {
     nsCOMPtr<nsIURI> uri;
     nsresult rv = NS_NewURI(getter_AddRefs(uri), aSpec);
     NS_ENSURE_SUCCESS(rv, rv);
 
     nsCOMPtr<nsIChannel> channel;
diff --git a/netwerk/base/nsNetUtil.h b/netwerk/base/nsNetUtil.h
--- a/netwerk/base/nsNetUtil.h
+++ b/netwerk/base/nsNetUtil.h
@@ -95,16 +95,19 @@ nsresult NS_NewURI(nsIURI **result,
                   const char *spec,
                   nsIURI *baseURI = nullptr,
                   nsIIOService *ioService = nullptr);     // pass in nsIIOService to optimize callers
 
 nsresult NS_NewFileURI(nsIURI **result,
                        nsIFile *spec,
                        nsIIOService *ioService = nullptr);     // pass in nsIIOService to optimize callers
 
+nsresult NS_GetSanitizedURIStringFromURI(nsIURI *aUri,
+                                         nsAString &aSanitizedSpec);
+
 /*
 * How to create a new Channel, using NS_NewChannel,
 * NS_NewChannelWithTriggeringPrincipal,
 * NS_NewInputStreamChannel, NS_NewChannelInternal
 * and it's variations:
 *
 * What specific API function to use:
 * * The NS_NewChannelInternal functions should almost never be directly
diff --git a/netwerk/protocol/http/HttpBaseChannel.cpp b/netwerk/protocol/http/HttpBaseChannel.cpp
--- a/netwerk/protocol/http/HttpBaseChannel.cpp
+++ b/netwerk/protocol/http/HttpBaseChannel.cpp
@@ -2492,26 +2492,23 @@ HttpBaseChannel::AddSecurityMessage(cons
 
   nsAutoString errorText;
   rv = nsContentUtils::GetLocalizedString(
           nsContentUtils::eSECURITY_PROPERTIES,
           NS_ConvertUTF16toUTF8(aMessageTag).get(),
           errorText);
   NS_ENSURE_SUCCESS(rv, rv);
 
-  nsAutoCString spec;
-  if (mURI) {
-    spec = mURI->GetSpecOrDefault();
-  }
-
   nsCOMPtr<nsIScriptError> error(do_CreateInstance(NS_SCRIPTERROR_CONTRACTID));
-  error->InitWithWindowID(errorText, NS_ConvertUTF8toUTF16(spec),
-                          EmptyString(), 0, 0, nsIScriptError::warningFlag,
-                          NS_ConvertUTF16toUTF8(aMessageCategory),
-                          innerWindowID);
+  error->InitWithSourceURI(errorText, mURI,
+                           EmptyString(), 0, 0,
+                           nsIScriptError::warningFlag,
+                           NS_ConvertUTF16toUTF8(aMessageCategory),
+                           innerWindowID);
+
   console->LogMessage(error);
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
 HttpBaseChannel::GetLocalPort(int32_t* port)
 {
diff --git a/netwerk/protocol/http/nsCORSListenerProxy.cpp b/netwerk/protocol/http/nsCORSListenerProxy.cpp
--- a/netwerk/protocol/http/nsCORSListenerProxy.cpp
+++ b/netwerk/protocol/http/nsCORSListenerProxy.cpp
@@ -1575,24 +1575,24 @@ nsCORSListenerProxy::LogBlockedCORSReque
   if (NS_FAILED(rv)) {
     NS_WARNING("Failed to log blocked cross-site request (no scriptError)");
     return;
   }
 
   // query innerWindowID and log to web console, otherwise log to
   // the error to the browser console.
   if (aInnerWindowID > 0) {
-    rv = scriptError->InitWithWindowID(aMessage,
-                                       EmptyString(), // sourceName
-                                       EmptyString(), // sourceLine
-                                       0,             // lineNumber
-                                       0,             // columnNumber
-                                       nsIScriptError::warningFlag,
-                                       "CORS",
-                                       aInnerWindowID);
+    rv = scriptError->InitWithSanitizedSource(aMessage,
+                                              EmptyString(), // sourceName
+                                              EmptyString(), // sourceLine
+                                              0,             // lineNumber
+                                              0,             // columnNumber
+                                              nsIScriptError::warningFlag,
+                                              "CORS",
+                                              aInnerWindowID);
   }
   else {
     rv = scriptError->Init(aMessage,
                            EmptyString(), // sourceName
                            EmptyString(), // sourceLine
                            0,             // lineNumber
                            0,             // columnNumber
                            nsIScriptError::warningFlag,
diff --git a/toolkit/mozapps/extensions/AddonContentPolicy.cpp b/toolkit/mozapps/extensions/AddonContentPolicy.cpp
--- a/toolkit/mozapps/extensions/AddonContentPolicy.cpp
+++ b/toolkit/mozapps/extensions/AddonContentPolicy.cpp
@@ -67,25 +67,23 @@ GetWindowIDFromContext(nsISupports* aCon
 
 static nsresult
 LogMessage(const nsAString &aMessage, nsIURI* aSourceURI, const nsAString &aSourceSample,
            nsISupports* aContext)
 {
   nsCOMPtr<nsIScriptError> error = do_CreateInstance(NS_SCRIPTERROR_CONTRACTID);
   NS_ENSURE_TRUE(error, NS_ERROR_OUT_OF_MEMORY);
 
-  nsCString sourceName = aSourceURI->GetSpecOrDefault();
-
   uint64_t windowID = 0;
   GetWindowIDFromContext(aContext, &windowID);
 
   nsresult rv =
-    error->InitWithWindowID(aMessage, NS_ConvertUTF8toUTF16(sourceName),
-                            aSourceSample, 0, 0, nsIScriptError::errorFlag,
-                            "JavaScript", windowID);
+    error->InitWithSourceURI(aMessage, aSourceURI,
+                             aSourceSample, 0, 0, nsIScriptError::errorFlag,
+                             "JavaScript", windowID);
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsCOMPtr<nsIConsoleService> console = do_GetService(NS_CONSOLESERVICE_CONTRACTID);
   NS_ENSURE_TRUE(console, NS_ERROR_OUT_OF_MEMORY);
 
   console->LogMessage(error);
   return NS_OK;
 }
