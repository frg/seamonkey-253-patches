# HG changeset patch
# User Tamir Duberstein <tamird@gmail.com>
# Date 1503150676 18000
#      Sat Aug 19 08:51:16 2017 -0500
# Node ID 3d7de8517c75b9e3f08f85ad10bc6058d66d2fc3
# Parent  c7eb64f509b7893d278dbd88cf695edba28b6d72
servo: Merge #18148 - style: pass a borrow instead of an Arc (from tamird:computedvalues-arc-borrow); r=emilio

https://bugzilla.mozilla.org/show_bug.cgi?id=1380133 has landed,
permitting us to pass a plain borrow into Gecko_CalcStyleDifference;
propagate this through style.

Closes #17795.

- [x] `./mach build -d` does not report any errors
- [x] `./mach test-tidy` does not report any errors

Source-Repo: https://github.com/servo/servo
Source-Revision: c1b196b7cb7e5efd3cd4de0c9562dcca5da34564

diff --git a/servo/components/style/gecko/restyle_damage.rs b/servo/components/style/gecko/restyle_damage.rs
--- a/servo/components/style/gecko/restyle_damage.rs
+++ b/servo/components/style/gecko/restyle_damage.rs
@@ -4,17 +4,16 @@
 
 //! Gecko's restyle damage computation (aka change hints, aka `nsChangeHint`).
 
 use gecko_bindings::bindings;
 use gecko_bindings::structs;
 use gecko_bindings::structs::{nsChangeHint, nsStyleContext, nsStyleStructID};
 use matching::{StyleChange, StyleDifference};
 use properties::ComputedValues;
-use servo_arc::Arc;
 use std::ops::{BitAnd, BitOr, BitOrAssign, Not};
 
 /// The representation of Gecko's restyle damage is just a wrapper over
 /// `nsChangeHint`.
 #[derive(Clone, Copy, Debug, PartialEq)]
 pub struct GeckoRestyleDamage(nsChangeHint);
 
 impl GeckoRestyleDamage {
@@ -44,17 +43,17 @@ impl GeckoRestyleDamage {
     ///
     /// Note that we could in theory just get two `ComputedValues` here and diff
     /// them, but Gecko has an interesting optimization when they mark accessed
     /// structs, so they effectively only diff structs that have ever been
     /// accessed from layout.
     pub fn compute_style_difference(
         source: &nsStyleContext,
         old_style: &ComputedValues,
-        new_style: &Arc<ComputedValues>,
+        new_style: &ComputedValues,
     ) -> StyleDifference {
         let mut any_style_changed: bool = false;
         let hint = unsafe {
             bindings::Gecko_CalcStyleDifference(old_style,
                                                 new_style,
                                                 source.mBits,
                                                 &mut any_style_changed)
         };
diff --git a/servo/components/style/matching.rs b/servo/components/style/matching.rs
--- a/servo/components/style/matching.rs
+++ b/servo/components/style/matching.rs
@@ -336,17 +336,17 @@ trait PrivateMatchMethods: TElement {
 
 
     /// Computes and applies non-redundant damage.
     #[cfg(feature = "gecko")]
     fn accumulate_damage_for(&self,
                              shared_context: &SharedStyleContext,
                              restyle: &mut RestyleData,
                              old_values: &ComputedValues,
-                             new_values: &Arc<ComputedValues>,
+                             new_values: &ComputedValues,
                              pseudo: Option<&PseudoElement>)
                              -> ChildCascadeRequirement {
         // Don't accumulate damage if we're in a forgetful traversal.
         if shared_context.traversal_flags.contains(traversal_flags::Forgetful) {
             return ChildCascadeRequirement::MustCascadeChildren;
         }
 
         // If an ancestor is already getting reconstructed by Gecko's top-down
@@ -355,17 +355,17 @@ trait PrivateMatchMethods: TElement {
         //
         // See https://bugzilla.mozilla.org/show_bug.cgi?id=1301258#c12
         // for followup work to make the optimization here more optimal by considering
         // each bit individually.
         let skip_applying_damage =
             restyle.reconstructed_self_or_ancestor();
 
         let difference =
-            self.compute_style_difference(&old_values, &new_values, pseudo);
+            self.compute_style_difference(old_values, new_values, pseudo);
 
         if !skip_applying_damage {
             restyle.damage |= difference.damage;
         }
 
         // We need to cascade the children in order to ensure the correct
         // propagation of computed value flags.
         //
@@ -384,20 +384,20 @@ trait PrivateMatchMethods: TElement {
     }
 
     /// Computes and applies restyle damage unless we've already maxed it out.
     #[cfg(feature = "servo")]
     fn accumulate_damage_for(&self,
                              _shared_context: &SharedStyleContext,
                              restyle: &mut RestyleData,
                              old_values: &ComputedValues,
-                             new_values: &Arc<ComputedValues>,
+                             new_values: &ComputedValues,
                              pseudo: Option<&PseudoElement>)
                              -> ChildCascadeRequirement {
-        let difference = self.compute_style_difference(&old_values, &new_values, pseudo);
+        let difference = self.compute_style_difference(old_values, new_values, pseudo);
         restyle.damage |= difference.damage;
         match difference.change {
             StyleChange::Changed => ChildCascadeRequirement::MustCascadeChildren,
             StyleChange::Unchanged => ChildCascadeRequirement::CanSkipCascade,
         }
     }
 
     #[cfg(feature = "servo")]
@@ -652,17 +652,17 @@ pub trait MatchMethods : TElement {
         }
     }
 
     /// Computes and applies restyle damage.
     fn accumulate_damage(&self,
                          shared_context: &SharedStyleContext,
                          restyle: &mut RestyleData,
                          old_values: Option<&ComputedValues>,
-                         new_values: &Arc<ComputedValues>,
+                         new_values: &ComputedValues,
                          pseudo: Option<&PseudoElement>)
                          -> ChildCascadeRequirement {
         let old_values = match old_values {
             Some(v) => v,
             None => return ChildCascadeRequirement::MustCascadeChildren,
         };
 
         // ::before and ::after are element-backed in Gecko, so they do the
@@ -812,17 +812,17 @@ pub trait MatchMethods : TElement {
     }
 
     /// Given the old and new style of this element, and whether it's a
     /// pseudo-element, compute the restyle damage used to determine which
     /// kind of layout or painting operations we'll need.
     fn compute_style_difference(
         &self,
         old_values: &ComputedValues,
-        new_values: &Arc<ComputedValues>,
+        new_values: &ComputedValues,
         pseudo: Option<&PseudoElement>
     ) -> StyleDifference {
         debug_assert!(pseudo.map_or(true, |p| p.is_eager()));
         if let Some(source) = self.existing_style_for_restyle_damage(old_values, pseudo) {
             return RestyleDamage::compute_style_difference(source, old_values, new_values)
         }
 
         let new_display = new_values.get_box().clone_display();
