# HG changeset patch
# User Andreas Pehrson <pehrsons@gmail.com>
# Date 1494932762 -7200
# Node ID 5cba097bf1eda93c6e7e0b1d7e57e9362377c7e6
# Parent  1dfbe4e4481a4385ce023f1fa3e55fb85cc7e20b
Bug 1296531 - Add MediaSegment::IsNull. r=jesup

This allows us to see if all chunks in a MediaSegment are null.
When this is true for the MediaSegment passed to NotifyQueuedChanges, we can
assume that the track's input is blocked for the given time.

MozReview-Commit-ID: zf7V7aiohg

diff --git a/dom/media/AudioSegment.h b/dom/media/AudioSegment.h
--- a/dom/media/AudioSegment.h
+++ b/dom/media/AudioSegment.h
@@ -391,26 +391,16 @@ public:
     for (ChunkIterator ci(*this); !ci.IsEnded(); ci.Next()) {
       if (ci->ChannelCount()) {
         return ci->ChannelCount();
       }
     }
     return 0;
   }
 
-  bool IsNull() const {
-    for (ChunkIterator ci(*const_cast<AudioSegment*>(this)); !ci.IsEnded();
-         ci.Next()) {
-      if (!ci->IsNull()) {
-        return false;
-      }
-    }
-    return true;
-  }
-
   static Type StaticType() { return AUDIO; }
 
   size_t SizeOfIncludingThis(MallocSizeOf aMallocSizeOf) const override
   {
     return aMallocSizeOf(this) + SizeOfExcludingThis(aMallocSizeOf);
   }
 };
 
diff --git a/dom/media/MediaSegment.h b/dom/media/MediaSegment.h
--- a/dom/media/MediaSegment.h
+++ b/dom/media/MediaSegment.h
@@ -144,16 +144,21 @@ public:
    * principal id than the current one.
    */
   void SetLastPrincipalHandle(const PrincipalHandle& aLastPrincipalHandle)
   {
     mLastPrincipalHandle = aLastPrincipalHandle;
   }
 
   /**
+   * Returns true if all chunks in this segment are null.
+   */
+  virtual bool IsNull() const = 0;
+
+  /**
    * Create a MediaSegment of the same type.
    */
   virtual MediaSegment* CreateEmptyClone() const = 0;
   /**
    * Moves contents of aSource to the end of this segment.
    */
   virtual void AppendFrom(MediaSegment* aSource) = 0;
   /**
@@ -224,16 +229,25 @@ protected:
 };
 
 /**
  * C is the implementation class subclassed from MediaSegmentBase.
  * C must contain a Chunk class.
  */
 template <class C, class Chunk> class MediaSegmentBase : public MediaSegment {
 public:
+  bool IsNull() const override
+  {
+    for (typename C::ConstChunkIterator iter(*this); !iter.IsEnded(); iter.Next()) {
+      if (!iter->IsNull()) {
+        return false;
+      }
+    }
+    return true;
+  }
   MediaSegment* CreateEmptyClone() const override
   {
     return new C();
   }
   void AppendFrom(MediaSegment* aSource) override
   {
     NS_ASSERTION(aSource->GetType() == C::StaticType(), "Wrong type");
     AppendFromInternal(static_cast<C*>(aSource));
