# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1504257231 -3600
# Node ID c048372283ce995e4b1e6e2dab07ffb77f7dac20
# Parent  5006670dd0999acdc63bd019683948dd671e6bff
Bug 1384513 - Remove AutoAssertNoAlloc r=sfink

diff --git a/js/public/GCAPI.h b/js/public/GCAPI.h
--- a/js/public/GCAPI.h
+++ b/js/public/GCAPI.h
@@ -522,62 +522,54 @@ class JS_PUBLIC_API(AutoRequireNoGC)
  * This works by entering a GC unsafe region, which is checked on allocation and
  * on GC.
  */
 class JS_PUBLIC_API(AutoAssertNoGC) : public AutoRequireNoGC
 {
     JSContext* cx_;
 
   public:
+#ifdef MOZ_DIAGNOSTIC_ASSERT_ENABLED
+    // This gets the context from TLS if it is not passed in.
     explicit AutoAssertNoGC(JSContext* cx = nullptr);
     ~AutoAssertNoGC();
-};
-
-/**
- * Assert if an allocation of a GC thing occurs while this class is live. This
- * class does not disable the static rooting hazard analysis.
- */
-class JS_PUBLIC_API(AutoAssertNoAlloc) : public AutoRequireNoGC
-{
-#ifdef JS_DEBUG
-    js::gc::GCRuntime* gc;
-
-  public:
-    AutoAssertNoAlloc() : gc(nullptr) {}
-    explicit AutoAssertNoAlloc(JSContext* cx);
-    void disallowAlloc(JSRuntime* rt);
-    ~AutoAssertNoAlloc();
 #else
-  public:
-    AutoAssertNoAlloc() {}
-    explicit AutoAssertNoAlloc(JSContext* cx) {}
-    void disallowAlloc(JSRuntime* rt) {}
+    explicit AutoAssertNoGC(JSContext* cx = nullptr) {}
+    ~AutoAssertNoGC() {}
 #endif
 };
 
 /**
- * Disable the static rooting hazard analysis in the live region and assert if
- * any allocation that could potentially trigger a GC occurs while this guard
- * object is live. This is most useful to help the exact rooting hazard analysis
- * in complex regions, since it cannot understand dataflow.
+ * Disable the static rooting hazard analysis in the live region and assert in
+ * debug builds if any allocation that could potentially trigger a GC occurs
+ * while this guard object is live. This is most useful to help the exact
+ * rooting hazard analysis in complex regions, since it cannot understand
+ * dataflow.
  *
  * Note: GC behavior is unpredictable even when deterministic and is generally
  *       non-deterministic in practice. The fact that this guard has not
  *       asserted is not a guarantee that a GC cannot happen in the guarded
  *       region. As a rule, anyone performing a GC unsafe action should
  *       understand the GC properties of all code in that region and ensure
  *       that the hazard analysis is correct for that code, rather than relying
  *       on this class.
  */
-class JS_PUBLIC_API(AutoSuppressGCAnalysis) : public AutoAssertNoAlloc
+#ifdef DEBUG
+class JS_PUBLIC_API(AutoSuppressGCAnalysis) : public AutoAssertNoGC
 {
   public:
-    AutoSuppressGCAnalysis() : AutoAssertNoAlloc() {}
-    explicit AutoSuppressGCAnalysis(JSContext* cx) : AutoAssertNoAlloc(cx) {}
+    explicit AutoSuppressGCAnalysis(JSContext* cx = nullptr) : AutoAssertNoGC(cx) {}
 } JS_HAZ_GC_SUPPRESSED;
+#else
+class JS_PUBLIC_API(AutoSuppressGCAnalysis) : public AutoRequireNoGC
+{
+  public:
+    explicit AutoSuppressGCAnalysis(JSContext* cx = nullptr) {}
+} JS_HAZ_GC_SUPPRESSED;
+#endif
 
 /**
  * Assert that code is only ever called from a GC callback, disable the static
  * rooting hazard analysis and assert if any allocation that could potentially
  * trigger a GC occurs while this guard object is live.
  *
  * This is useful to make the static analysis ignore code that runs in GC
  * callbacks.
diff --git a/js/src/jsgc.cpp b/js/src/jsgc.cpp
--- a/js/src/jsgc.cpp
+++ b/js/src/jsgc.cpp
@@ -8125,50 +8125,36 @@ js::gc::AssertGCThingHasType(js::gc::Cel
         MOZ_ASSERT(kind == JS::TraceKind::Null);
     else if (IsInsideNursery(cell))
         MOZ_ASSERT(kind == JS::TraceKind::Object);
     else
         MOZ_ASSERT(MapAllocToTraceKind(cell->asTenured().getAllocKind()) == kind);
 }
 #endif
 
+#ifdef MOZ_DIAGNOSTIC_ASSERT_ENABLED
+
 JS::AutoAssertNoGC::AutoAssertNoGC(JSContext* maybecx)
   : cx_(maybecx ? maybecx : TlsContext.get())
 {
     if (cx_)
         cx_->inUnsafeRegion++;
 }
 
 JS::AutoAssertNoGC::~AutoAssertNoGC()
 {
     if (cx_) {
         MOZ_ASSERT(cx_->inUnsafeRegion > 0);
         cx_->inUnsafeRegion--;
     }
 }
 
+#endif // MOZ_DIAGNOSTIC_ASSERT_ENABLED
+
 #ifdef DEBUG
-JS::AutoAssertNoAlloc::AutoAssertNoAlloc(JSContext* cx)
-  : gc(nullptr)
-{
-    disallowAlloc(cx->runtime());
-}
-
-void JS::AutoAssertNoAlloc::disallowAlloc(JSRuntime* rt)
-{
-    MOZ_ASSERT(!gc);
-    gc = &rt->gc;
-    TlsContext.get()->disallowAlloc();
-}
-
-JS::AutoAssertNoAlloc::~AutoAssertNoAlloc()
-{
-    if (gc)
-        TlsContext.get()->allowAlloc();
-}
 
 AutoAssertNoNurseryAlloc::AutoAssertNoNurseryAlloc()
 {
     TlsContext.get()->disallowNurseryAlloc();
 }
 
 AutoAssertNoNurseryAlloc::~AutoAssertNoNurseryAlloc()
 {
@@ -8187,17 +8173,18 @@ JS::AutoEnterCycleCollection::~AutoEnter
     TlsContext.get()->heapState = HeapState::Idle;
 }
 
 JS::AutoAssertGCCallback::AutoAssertGCCallback()
   : AutoSuppressGCAnalysis()
 {
     MOZ_ASSERT(JS::CurrentThreadIsHeapCollecting());
 }
-#endif
+
+#endif // DEBUG
 
 JS_FRIEND_API(const char*)
 JS::GCTraceKindToAscii(JS::TraceKind kind)
 {
     switch(kind) {
 #define MAP_NAME(name, _0, _1) case JS::TraceKind::name: return #name;
 JS_FOR_EACH_TRACEKIND(MAP_NAME);
 #undef MAP_NAME
