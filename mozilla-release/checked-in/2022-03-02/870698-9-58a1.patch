# HG changeset patch
# User Chris Peterson <cpeterson@mozilla.com>
# Date 1504687393 25200
# Node ID a5b71c927db1ab9e3e32d58760ec497320d77340
# Parent  cbf08d87daaf9494ba76c31b7db8408a74ece293
Bug 870698 - Part 9: Replace Assign(NS_LITERAL_STRING("")) with AssignLiteral(u""). r=erahm

The NS_LITERAL_STRING macro creates a temporary nsLiteralString to encapsulate the char16_t string literal and its length, but AssignLiteral() can determine the char16_t string literal's length at compile-time without nsLiteralString.

MozReview-Commit-ID: 6vgQiU8zN3o

diff --git a/dom/base/XPathGenerator.cpp b/dom/base/XPathGenerator.cpp
--- a/dom/base/XPathGenerator.cpp
+++ b/dom/base/XPathGenerator.cpp
@@ -42,19 +42,19 @@ bool ContainNonWordCharacter(const nsASt
 }
 
 /**
  * Get the prefix according to the given namespace and assign the result to aResult.
  * */
 void GetPrefix(const nsINode* aNode, nsAString& aResult)
 {
   if (aNode->IsXULElement()) {
-    aResult.Assign(NS_LITERAL_STRING("xul"));
+    aResult.AssignLiteral(u"xul");
   } else if (aNode->IsHTMLElement()) {
-    aResult.Assign(NS_LITERAL_STRING("xhtml"));
+    aResult.AssignLiteral(u"xhtml");
   }
 }
 
 void GetNameAttribute(const nsINode* aNode, nsAString& aResult)
 {
   if (aNode->HasName()) {
     const Element* elem = aNode->AsElement();
     elem->GetAttr(kNameSpaceID_None, nsGkAtoms::name, aResult);
@@ -182,15 +182,15 @@ void XPathGenerator::Generate(const nsIN
   nsAutoString namePart;
   nsAutoString countPart;
   if (!nodeNameAttribute.IsEmpty()) {
     nsAutoString quotedArgument;
     QuoteArgument(nodeNameAttribute, quotedArgument);
     namePart.Assign(NS_LITERAL_STRING("[@name=") + quotedArgument + NS_LITERAL_STRING("]"));
   }
   if (count != 1) {
-    countPart.Assign(NS_LITERAL_STRING("["));
+    countPart.AssignLiteral(u"[");
     countPart.AppendInt(count);
     countPart.Append(NS_LITERAL_STRING("]"));
   }
   Generate(aNode->GetParentNode(), aResult);
   aResult.Append(NS_LITERAL_STRING("/") + tag + namePart + countPart);
 }
diff --git a/dom/base/test/gtest/TestXPathGenerator.cpp b/dom/base/test/gtest/TestXPathGenerator.cpp
--- a/dom/base/test/gtest/TestXPathGenerator.cpp
+++ b/dom/base/test/gtest/TestXPathGenerator.cpp
@@ -6,135 +6,135 @@
 
 #include "gtest/gtest.h"
 #include "XPathGenerator.h"
 #include "nsString.h"
 
 TEST(TestXPathGenerator, TestQuoteArgumentWithoutQuote)
 {
   nsAutoString arg;
-  arg.Assign(NS_LITERAL_STRING("testing"));
+  arg.AssignLiteral(u"testing");
 
   nsAutoString expectedResult;
-  expectedResult.Assign(NS_LITERAL_STRING("\'testing\'"));
+  expectedResult.AssignLiteral(u"\'testing\'");
 
   nsAutoString result;
   XPathGenerator::QuoteArgument(arg, result);
 
   ASSERT_TRUE(expectedResult.Equals(result));
 }
 
 TEST(TestXPathGenerator, TestQuoteArgumentWithSingleQuote)
 {
   nsAutoString arg;
-  arg.Assign(NS_LITERAL_STRING("\'testing\'"));
+  arg.AssignLiteral(u"\'testing\'");
 
   nsAutoString expectedResult;
-  expectedResult.Assign(NS_LITERAL_STRING("\"\'testing\'\""));
+  expectedResult.AssignLiteral(u"\"\'testing\'\"");
 
   nsAutoString result;
   XPathGenerator::QuoteArgument(arg, result);
 
   ASSERT_TRUE(expectedResult.Equals(result));
 }
 
 TEST(TestXPathGenerator, TestQuoteArgumentWithDoubleQuote)
 {
   nsAutoString arg;
-  arg.Assign(NS_LITERAL_STRING("\"testing\""));
+  arg.AssignLiteral(u"\"testing\"");
 
   nsAutoString expectedResult;
-  expectedResult.Assign(NS_LITERAL_STRING("\'\"testing\"\'"));
+  expectedResult.AssignLiteral(u"\'\"testing\"\'");
 
   nsAutoString result;
   XPathGenerator::QuoteArgument(arg, result);
 
   ASSERT_TRUE(expectedResult.Equals(result));
 }
 
 TEST(TestXPathGenerator, TestQuoteArgumentWithSingleAndDoubleQuote)
 {
   nsAutoString arg;
-  arg.Assign(NS_LITERAL_STRING("\'testing\""));
+  arg.AssignLiteral(u"\'testing\"");
 
   nsAutoString expectedResult;
-  expectedResult.Assign(NS_LITERAL_STRING("concat(\'\',\"\'\",\'testing\"\')"));
+  expectedResult.AssignLiteral(u"concat(\'\',\"\'\",\'testing\"\')");
 
   nsAutoString result;
   XPathGenerator::QuoteArgument(arg, result);
   printf("Result: %s\nExpected: %s\n", NS_ConvertUTF16toUTF8(result).get(), NS_ConvertUTF16toUTF8(expectedResult).get());
 
   ASSERT_TRUE(expectedResult.Equals(result));
 }
 
 TEST(TestXPathGenerator, TestQuoteArgumentWithDoubleQuoteAndASequenceOfSingleQuote)
 {
   nsAutoString arg;
-  arg.Assign(NS_LITERAL_STRING("\'\'\'\'testing\""));
+  arg.AssignLiteral(u"\'\'\'\'testing\"");
 
   nsAutoString expectedResult;
-  expectedResult.Assign(NS_LITERAL_STRING("concat(\'\',\"\'\'\'\'\",\'testing\"\')"));
+  expectedResult.AssignLiteral(u"concat(\'\',\"\'\'\'\'\",\'testing\"\')");
 
   nsAutoString result;
   XPathGenerator::QuoteArgument(arg, result);
   printf("Result: %s\nExpected: %s\n", NS_ConvertUTF16toUTF8(result).get(), NS_ConvertUTF16toUTF8(expectedResult).get());
 
   ASSERT_TRUE(expectedResult.Equals(result));
 }
 
 TEST(TestXPathGenerator, TestQuoteArgumentWithDoubleQuoteAndTwoSequencesOfSingleQuote)
 {
   nsAutoString arg;
-  arg.Assign(NS_LITERAL_STRING("\'\'\'\'testing\'\'\'\'\'\'\""));
+  arg.AssignLiteral(u"\'\'\'\'testing\'\'\'\'\'\'\"");
 
   nsAutoString expectedResult;
-  expectedResult.Assign(NS_LITERAL_STRING("concat(\'\',\"\'\'\'\'\",\'testing\',\"\'\'\'\'\'\'\",\'\"\')"));
+  expectedResult.AssignLiteral(u"concat(\'\',\"\'\'\'\'\",\'testing\',\"\'\'\'\'\'\'\",\'\"\')");
 
   nsAutoString result;
   XPathGenerator::QuoteArgument(arg, result);
   printf("Result: %s\nExpected: %s\n", NS_ConvertUTF16toUTF8(result).get(), NS_ConvertUTF16toUTF8(expectedResult).get());
 
   ASSERT_TRUE(expectedResult.Equals(result));
 }
 
 TEST(TestXPathGenerator, TestQuoteArgumentWithDoubleQuoteAndTwoSequencesOfSingleQuoteInMiddle)
 {
   nsAutoString arg;
-  arg.Assign(NS_LITERAL_STRING("t\'\'\'\'estin\'\'\'\'\'\'\"g"));
+  arg.AssignLiteral(u"t\'\'\'\'estin\'\'\'\'\'\'\"g");
 
   nsAutoString expectedResult;
-  expectedResult.Assign(NS_LITERAL_STRING("concat(\'t\',\"\'\'\'\'\",\'estin\',\"\'\'\'\'\'\'\",\'\"g\')"));
+  expectedResult.AssignLiteral(u"concat(\'t\',\"\'\'\'\'\",\'estin\',\"\'\'\'\'\'\'\",\'\"g\')");
 
   nsAutoString result;
   XPathGenerator::QuoteArgument(arg, result);
   printf("Result: %s\nExpected: %s\n", NS_ConvertUTF16toUTF8(result).get(), NS_ConvertUTF16toUTF8(expectedResult).get());
 
   ASSERT_TRUE(expectedResult.Equals(result));
 }
 
 TEST(TestXPathGenerator, TestEscapeNameWithNormalCharacters)
 {
   nsAutoString arg;
-  arg.Assign(NS_LITERAL_STRING("testing"));
+  arg.AssignLiteral(u"testing");
 
   nsAutoString expectedResult;
-  expectedResult.Assign(NS_LITERAL_STRING("testing"));
+  expectedResult.AssignLiteral(u"testing");
 
   nsAutoString result;
   XPathGenerator::EscapeName(arg, result);
 
   ASSERT_TRUE(expectedResult.Equals(result));
 }
 
 TEST(TestXPathGenerator, TestEscapeNameWithSpecialCharacters)
 {
   nsAutoString arg;
-  arg.Assign(NS_LITERAL_STRING("^testing!"));
+  arg.AssignLiteral(u"^testing!");
 
   nsAutoString expectedResult;
-  expectedResult.Assign(NS_LITERAL_STRING("*[local-name()=\'^testing!\']"));
+  expectedResult.AssignLiteral(u"*[local-name()=\'^testing!\']");
 
   nsAutoString result;
   XPathGenerator::EscapeName(arg, result);
   printf("Result: %s\nExpected: %s\n", NS_ConvertUTF16toUTF8(result).get(), NS_ConvertUTF16toUTF8(expectedResult).get());
 
   ASSERT_TRUE(expectedResult.Equals(result));
 }
diff --git a/dom/media/MediaManager.cpp b/dom/media/MediaManager.cpp
--- a/dom/media/MediaManager.cpp
+++ b/dom/media/MediaManager.cpp
@@ -857,21 +857,21 @@ MediaDevice::SetRawId(const nsAString& a
 {
   mRawID.Assign(aID);
 }
 
 NS_IMETHODIMP
 MediaDevice::GetMediaSource(nsAString& aMediaSource)
 {
   if (mMediaSource == MediaSourceEnum::Microphone) {
-    aMediaSource.Assign(NS_LITERAL_STRING("microphone"));
+    aMediaSource.AssignLiteral(u"microphone");
   } else if (mMediaSource == MediaSourceEnum::AudioCapture) {
-    aMediaSource.Assign(NS_LITERAL_STRING("audioCapture"));
+    aMediaSource.AssignLiteral(u"audioCapture");
   } else if (mMediaSource == MediaSourceEnum::Window) { // this will go away
-    aMediaSource.Assign(NS_LITERAL_STRING("window"));
+    aMediaSource.AssignLiteral(u"window");
   } else { // all the rest are shared
     aMediaSource.Assign(NS_ConvertUTF8toUTF16(
       dom::MediaSourceEnumValues::strings[uint32_t(mMediaSource)].value));
   }
   return NS_OK;
 }
 
 VideoDevice::Source*
diff --git a/dom/webauthn/WebAuthnManager.cpp b/dom/webauthn/WebAuthnManager.cpp
--- a/dom/webauthn/WebAuthnManager.cpp
+++ b/dom/webauthn/WebAuthnManager.cpp
@@ -82,17 +82,17 @@ AssembleClientData(const nsAString& aOri
   nsresult rv = aChallenge.ToJwkBase64(challengeBase64);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return NS_ERROR_FAILURE;
   }
 
   CollectedClientData clientDataObject;
   clientDataObject.mChallenge.Assign(challengeBase64);
   clientDataObject.mOrigin.Assign(aOrigin);
-  clientDataObject.mHashAlg.Assign(NS_LITERAL_STRING("SHA-256"));
+  clientDataObject.mHashAlg.AssignLiteral(u"SHA-256");
 
   nsAutoString temp;
   if (NS_WARN_IF(!clientDataObject.ToJSON(temp))) {
     return NS_ERROR_FAILURE;
   }
 
   aJsonOut.Assign(NS_ConvertUTF16toUTF8(temp));
   return NS_OK;
diff --git a/widget/cocoa/nsMenuItemX.mm b/widget/cocoa/nsMenuItemX.mm
--- a/widget/cocoa/nsMenuItemX.mm
+++ b/widget/cocoa/nsMenuItemX.mm
@@ -245,17 +245,17 @@ void nsMenuItemX::SetKeyEquiv()
         nsAutoString keyCodeName;
         keyContent->GetAttr(kNameSpaceID_None, nsGkAtoms::keycode, keyCodeName);
         uint32_t charCode =
           nsCocoaUtils::ConvertGeckoNameToMacCharCode(keyCodeName);
         if (charCode) {
           keyChar.Assign(charCode);
         }
         else {
-          keyChar.Assign(NS_LITERAL_STRING(" "));
+          keyChar.AssignLiteral(u" ");
         }
       }
 
       nsAutoString modifiersStr;
       keyContent->GetAttr(kNameSpaceID_None, nsGkAtoms::modifiers, modifiersStr);
       uint8_t modifiers = nsMenuUtilsX::GeckoModifiersForNodeAttribute(modifiersStr);
 
       unsigned int macModifiers = nsMenuUtilsX::MacModifiersForGeckoModifiers(modifiers);
