# HG changeset patch
# User Samael Wang <freesamael@gmail.com>
# Date 1515787921 -7200
# Node ID 5a556e5558de569582b9d1a9386518c84ca8c1c7
# Parent  5485abfec94dcb15605c5ad5d1678bee36199e53
Bug 1416878 - Move the implementation of nsIWidgetListener from nsWebBrowser / nsWebShellWindow to a separate object. r=bz a=gchang

MozReview-Commit-ID: 5QV6lkCCGW5

diff --git a/toolkit/components/browser/nsWebBrowser.cpp b/toolkit/components/browser/nsWebBrowser.cpp
--- a/toolkit/components/browser/nsWebBrowser.cpp
+++ b/toolkit/components/browser/nsWebBrowser.cpp
@@ -63,16 +63,17 @@ static NS_DEFINE_CID(kChildCID, NS_CHILD
 nsWebBrowser::nsWebBrowser()
   : mInitInfo(new nsWebBrowserInitInfo())
   , mContentType(typeContentWrapper)
   , mActivating(false)
   , mShouldEnableHistory(true)
   , mIsActive(true)
   , mParentNativeWindow(nullptr)
   , mProgressListener(nullptr)
+  , mWidgetListenerDelegate(this)
   , mBackgroundColor(0)
   , mPersistCurrentState(nsIWebBrowserPersist::PERSIST_STATE_READY)
   , mPersistResult(NS_OK)
   , mPersistFlags(nsIWebBrowserPersist::PERSIST_FLAGS_NONE)
   , mParentWidget(nullptr)
 {
   mWWatch = do_GetService(NS_WINDOWWATCHER_CONTRACTID);
   NS_ASSERTION(mWWatch, "failed to get WindowWatcher");
@@ -1188,17 +1189,17 @@ nsWebBrowser::Create()
     nsWidgetInitData widgetInit;
 
     widgetInit.clipChildren = true;
 
     widgetInit.mWindowType = eWindowType_child;
     LayoutDeviceIntRect bounds(mInitInfo->x, mInitInfo->y,
                                mInitInfo->cx, mInitInfo->cy);
 
-    mInternalWidget->SetWidgetListener(this);
+    mInternalWidget->SetWidgetListener(&mWidgetListenerDelegate);
     rv = mInternalWidget->Create(nullptr, mParentNativeWindow, bounds,
                                  &widgetInit);
     NS_ENSURE_SUCCESS(rv, rv);
   }
 
   nsCOMPtr<nsIDocShell> docShell(
     do_CreateInstance("@mozilla.org/docshell;1", &rv));
   NS_ENSURE_SUCCESS(rv, rv);
@@ -1893,16 +1894,24 @@ nsWebBrowser::GetFocusedElement(nsIDOMEl
 
 NS_IMETHODIMP
 nsWebBrowser::SetFocusedElement(nsIDOMElement* aFocusedElement)
 {
   nsCOMPtr<nsIFocusManager> fm = do_GetService(FOCUSMANAGER_CONTRACTID);
   return fm ? fm->SetFocus(aFocusedElement, 0) : NS_OK;
 }
 
+bool
+nsWebBrowser::WidgetListenerDelegate::PaintWindow(
+  nsIWidget* aWidget, mozilla::LayoutDeviceIntRegion aRegion)
+{
+  RefPtr<nsWebBrowser> holder = mWebBrowser;
+  return holder->PaintWindow(aWidget, aRegion);
+}
+
 //*****************************************************************************
 // nsWebBrowser::nsIWebBrowserStream
 //*****************************************************************************
 
 NS_IMETHODIMP
 nsWebBrowser::OpenStream(nsIURI* aBaseURI, const nsACString& aContentType)
 {
   nsresult rv;
diff --git a/toolkit/components/browser/nsWebBrowser.h b/toolkit/components/browser/nsWebBrowser.h
--- a/toolkit/components/browser/nsWebBrowser.h
+++ b/toolkit/components/browser/nsWebBrowser.h
@@ -79,22 +79,38 @@ class nsWebBrowser final : public nsIWeb
                            public nsIBaseWindow,
                            public nsIScrollable,
                            public nsITextScroll,
                            public nsIInterfaceRequestor,
                            public nsIWebBrowserPersist,
                            public nsIWebBrowserFocus,
                            public nsIWebProgressListener,
                            public nsIWebBrowserStream,
-                           public nsIWidgetListener,
                            public nsSupportsWeakReference
 {
   friend class nsDocShellTreeOwner;
 
 public:
+
+  // The implementation of non-refcounted nsIWidgetListener, which would hold a
+  // strong reference on stack before calling nsWebBrowser.
+  class WidgetListenerDelegate : public nsIWidgetListener
+  {
+  public:
+    explicit WidgetListenerDelegate(nsWebBrowser* aWebBrowser)
+      : mWebBrowser(aWebBrowser) {}
+    virtual bool PaintWindow(
+      nsIWidget* aWidget, mozilla::LayoutDeviceIntRegion aRegion) override;
+
+  private:
+    // The lifetime of WidgetListenerDelegate is bound to nsWebBrowser so we
+    // just use raw pointer here.
+    nsWebBrowser* mWebBrowser;
+  };
+
   nsWebBrowser();
 
   NS_DECL_ISUPPORTS
 
   NS_DECL_NSIBASEWINDOW
   NS_DECL_NSIDOCSHELLTREEITEM
   NS_DECL_NSIINTERFACEREQUESTOR
   NS_DECL_NSISCROLLABLE
@@ -117,18 +133,17 @@ protected:
   NS_IMETHOD EnsureDocShellTreeOwner();
   NS_IMETHOD BindListener(nsISupports* aListener, const nsIID& aIID);
   NS_IMETHOD UnBindListener(nsISupports* aListener, const nsIID& aIID);
   NS_IMETHOD EnableGlobalHistory(bool aEnable);
 
   // nsIWidgetListener
   virtual void WindowRaised(nsIWidget* aWidget);
   virtual void WindowLowered(nsIWidget* aWidget);
-  virtual bool PaintWindow(nsIWidget* aWidget,
-                           mozilla::LayoutDeviceIntRegion aRegion) override;
+  bool PaintWindow(nsIWidget* aWidget, mozilla::LayoutDeviceIntRegion aRegion);
 
 protected:
   RefPtr<nsDocShellTreeOwner> mDocShellTreeOwner;
   nsCOMPtr<nsIDocShell> mDocShell;
   nsCOMPtr<nsIInterfaceRequestor> mDocShellAsReq;
   nsCOMPtr<nsIBaseWindow> mDocShellAsWin;
   nsCOMPtr<nsIWebNavigation> mDocShellAsNav;
   nsCOMPtr<nsIScrollable> mDocShellAsScrollable;
@@ -143,16 +158,18 @@ protected:
   bool mShouldEnableHistory;
   bool mIsActive;
   nativeWindow mParentNativeWindow;
   nsIWebProgressListener* mProgressListener;
   nsCOMPtr<nsIWebProgress> mWebProgress;
 
   nsCOMPtr<nsIPrintSettings> mPrintSettings;
 
+  WidgetListenerDelegate mWidgetListenerDelegate;
+
   // cached background color
   nscolor mBackgroundColor;
 
   // persistence object
   nsCOMPtr<nsIWebBrowserPersist> mPersist;
   uint32_t mPersistCurrentState;
   nsresult mPersistResult;
   uint32_t mPersistFlags;
diff --git a/xpfe/appshell/nsWebShellWindow.cpp b/xpfe/appshell/nsWebShellWindow.cpp
--- a/xpfe/appshell/nsWebShellWindow.cpp
+++ b/xpfe/appshell/nsWebShellWindow.cpp
@@ -85,16 +85,17 @@ using namespace mozilla::dom;
 /* Define Class IDs */
 static NS_DEFINE_CID(kWindowCID,           NS_WINDOW_CID);
 
 #define SIZE_PERSISTENCE_TIMEOUT 500 // msec
 
 nsWebShellWindow::nsWebShellWindow(uint32_t aChromeFlags)
   : nsXULWindow(aChromeFlags)
   , mSPTimerLock("nsWebShellWindow.mSPTimerLock")
+  , mWidgetListenerDelegate(this)
 {
 }
 
 nsWebShellWindow::~nsWebShellWindow()
 {
   MutexAutoLock lock(mSPTimerLock);
   if (mSPTimer)
     mSPTimer->Cancel();
@@ -174,17 +175,17 @@ nsresult nsWebShellWindow::Initialize(ns
      top-level child windows in OSes that do not. Later.
   */
   nsCOMPtr<nsIBaseWindow> parentAsWin(do_QueryInterface(aParent));
   if (parentAsWin) {
     parentAsWin->GetMainWidget(getter_AddRefs(parentWidget));
     mParentWindow = do_GetWeakReference(aParent);
   }
 
-  mWindow->SetWidgetListener(this);
+  mWindow->SetWidgetListener(&mWidgetListenerDelegate);
   rv = mWindow->Create((nsIWidget *)parentWidget, // Parent nsIWidget
                        nullptr,                   // Native parent widget
                        deskRect,                  // Widget dimensions
                        &widgetInitData);          // Widget initialization data
   NS_ENSURE_SUCCESS(rv, rv);
 
   LayoutDeviceIntRect r = mWindow->GetClientBounds();
   // Match the default background color of content. Important on windows
@@ -779,8 +780,113 @@ NS_IMETHODIMP nsWebShellWindow::Destroy(
     if (mSPTimer) {
       mSPTimer->Cancel();
       SavePersistentAttributes();
       mSPTimer = nullptr;
     }
   }
   return nsXULWindow::Destroy();
 }
+
+nsIXULWindow*
+nsWebShellWindow::WidgetListenerDelegate::GetXULWindow()
+{
+  return mWebShellWindow->GetXULWindow();
+}
+
+nsIPresShell*
+nsWebShellWindow::WidgetListenerDelegate::GetPresShell()
+{
+  return mWebShellWindow->GetPresShell();
+}
+
+bool
+nsWebShellWindow::WidgetListenerDelegate::WindowMoved(
+  nsIWidget* aWidget, int32_t aX, int32_t aY)
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  return holder->WindowMoved(aWidget, aX, aY);
+}
+
+bool
+nsWebShellWindow::WidgetListenerDelegate::WindowResized(
+  nsIWidget* aWidget, int32_t aWidth, int32_t aHeight)
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  return holder->WindowResized(aWidget, aWidth, aHeight);
+}
+
+bool
+nsWebShellWindow::WidgetListenerDelegate::RequestWindowClose(nsIWidget* aWidget)
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  return holder->RequestWindowClose(aWidget);
+}
+
+void
+nsWebShellWindow::WidgetListenerDelegate::SizeModeChanged(nsSizeMode aSizeMode)
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  holder->SizeModeChanged(aSizeMode);
+}
+
+void
+nsWebShellWindow::WidgetListenerDelegate::UIResolutionChanged()
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  holder->UIResolutionChanged();
+}
+
+void
+nsWebShellWindow::WidgetListenerDelegate::FullscreenWillChange(
+  bool aInFullscreen)
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  holder->FullscreenWillChange(aInFullscreen);
+}
+
+void
+nsWebShellWindow::WidgetListenerDelegate::FullscreenChanged(bool aInFullscreen)
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  holder->FullscreenChanged(aInFullscreen);
+}
+
+void
+nsWebShellWindow::WidgetListenerDelegate::OcclusionStateChanged(
+  bool aIsFullyOccluded)
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  holder->OcclusionStateChanged(aIsFullyOccluded);
+}
+
+void
+nsWebShellWindow::WidgetListenerDelegate::OSToolbarButtonPressed()
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  holder->OSToolbarButtonPressed();
+}
+
+bool
+nsWebShellWindow::WidgetListenerDelegate::ZLevelChanged(
+  bool aImmediate, nsWindowZ *aPlacement, nsIWidget* aRequestBelow,
+  nsIWidget** aActualBelow)
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  return holder->ZLevelChanged(aImmediate,
+                               aPlacement,
+                               aRequestBelow,
+                               aActualBelow);
+}
+
+void
+nsWebShellWindow::WidgetListenerDelegate::WindowActivated()
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  holder->WindowActivated();
+}
+
+void
+nsWebShellWindow::WidgetListenerDelegate::WindowDeactivated()
+{
+  RefPtr<nsWebShellWindow> holder = mWebShellWindow;
+  holder->WindowDeactivated();
+}
diff --git a/xpfe/appshell/nsWebShellWindow.h b/xpfe/appshell/nsWebShellWindow.h
--- a/xpfe/appshell/nsWebShellWindow.h
+++ b/xpfe/appshell/nsWebShellWindow.h
@@ -19,20 +19,52 @@ class nsIURI;
 
 struct nsWidgetInitData;
 
 namespace mozilla {
 class WebShellWindowTimerCallback;
 } // namespace mozilla
 
 class nsWebShellWindow final : public nsXULWindow,
-                               public nsIWebProgressListener,
-                               public nsIWidgetListener
+                               public nsIWebProgressListener
 {
 public:
+
+  // The implementation of non-refcounted nsIWidgetListener, which would hold a
+  // strong reference on stack before calling nsWebShellWindow
+  class WidgetListenerDelegate : public nsIWidgetListener
+  {
+  public:
+    explicit WidgetListenerDelegate(nsWebShellWindow* aWebShellWindow)
+      : mWebShellWindow(aWebShellWindow) {}
+
+    virtual nsIXULWindow* GetXULWindow() override;
+    virtual nsIPresShell* GetPresShell() override;
+    virtual bool WindowMoved(nsIWidget* aWidget, int32_t x, int32_t y) override;
+    virtual bool WindowResized(nsIWidget* aWidget, int32_t aWidth, int32_t aHeight) override;
+    virtual bool RequestWindowClose(nsIWidget* aWidget) override;
+    virtual void SizeModeChanged(nsSizeMode sizeMode) override;
+    virtual void UIResolutionChanged() override;
+    virtual void FullscreenWillChange(bool aInFullscreen) override;
+    virtual void FullscreenChanged(bool aInFullscreen) override;
+    virtual void OcclusionStateChanged(bool aIsFullyOccluded) override;
+    virtual void OSToolbarButtonPressed() override;
+    virtual bool ZLevelChanged(bool aImmediate,
+                               nsWindowZ *aPlacement,
+                               nsIWidget* aRequestBelow,
+                               nsIWidget** aActualBelow) override;
+    virtual void WindowActivated() override;
+    virtual void WindowDeactivated() override;
+
+  private:
+    // The lifetime of WidgetListenerDelegate is bound to nsWebShellWindow so
+    // we just use a raw pointer here.
+    nsWebShellWindow* mWebShellWindow;
+  };
+
   explicit nsWebShellWindow(uint32_t aChromeFlags);
 
   // nsISupports interface...
   NS_DECL_ISUPPORTS_INHERITED
 
   // nsWebShellWindow methods...
   nsresult Initialize(nsIXULWindow * aParent, nsIXULWindow * aOpener,
                       nsIURI* aUrl,
@@ -45,42 +77,42 @@ public:
   nsresult Toolbar();
 
   // nsIWebProgressListener
   NS_DECL_NSIWEBPROGRESSLISTENER
 
   // nsIBaseWindow
   NS_IMETHOD Destroy() override;
 
-  // nsIWidgetListener
-  virtual nsIXULWindow* GetXULWindow() override { return this; }
-  virtual nsIPresShell* GetPresShell() override;
-  virtual bool WindowMoved(nsIWidget* aWidget, int32_t x, int32_t y) override;
-  virtual bool WindowResized(nsIWidget* aWidget, int32_t aWidth, int32_t aHeight) override;
-  virtual bool RequestWindowClose(nsIWidget* aWidget) override;
-  virtual void SizeModeChanged(nsSizeMode sizeMode) override;
-  virtual void UIResolutionChanged() override;
-  virtual void FullscreenWillChange(bool aInFullscreen) override;
-  virtual void FullscreenChanged(bool aInFullscreen) override;
-  virtual void OcclusionStateChanged(bool aIsFullyOccluded) override;
-  virtual void OSToolbarButtonPressed() override;
-  virtual bool ZLevelChanged(bool aImmediate, nsWindowZ *aPlacement,
-                             nsIWidget* aRequestBelow, nsIWidget** aActualBelow) override;
-  virtual void WindowActivated() override;
-  virtual void WindowDeactivated() override;
+  // nsIWidgetListener methods for WidgetListenerDelegate.
+  nsIXULWindow* GetXULWindow() { return this; }
+  nsIPresShell* GetPresShell();
+  bool WindowMoved(nsIWidget* aWidget, int32_t aX, int32_t aY);
+  bool WindowResized(nsIWidget* aWidget, int32_t aWidth, int32_t aHeight);
+  bool RequestWindowClose(nsIWidget* aWidget);
+  void SizeModeChanged(nsSizeMode aSizeMode);
+  void UIResolutionChanged();
+  void FullscreenWillChange(bool aInFullscreen);
+  void FullscreenChanged(bool aInFullscreen);
+  void OcclusionStateChanged(bool aIsFullyOccluded);
+  void OSToolbarButtonPressed();
+  bool ZLevelChanged(bool aImmediate, nsWindowZ *aPlacement,
+                     nsIWidget* aRequestBelow, nsIWidget** aActualBelow);
+  void WindowActivated();
+  void WindowDeactivated();
 
 protected:
   friend class mozilla::WebShellWindowTimerCallback;
 
   virtual ~nsWebShellWindow();
 
   bool                     ExecuteCloseHandler();
   void                     ConstrainToOpenerScreen(int32_t* aX, int32_t* aY);
 
   nsCOMPtr<nsITimer>      mSPTimer;
   mozilla::Mutex          mSPTimerLock;
+  WidgetListenerDelegate  mWidgetListenerDelegate;
 
   void        SetPersistenceTimer(uint32_t aDirtyFlags);
   void        FirePersistenceTimer();
 };
 
-
 #endif /* nsWebShellWindow_h__ */
