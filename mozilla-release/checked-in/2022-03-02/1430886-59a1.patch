# HG changeset patch
# User Ralph Giles <giles@mozilla.com>
# Date 1516206771 28800
# Node ID 261725e65af9b5d98ff593db3db08632bf019454
# Parent  3f10158e6cc158091c8c89a35bce079c235009ed
Bug 1430886 - Disable incremental rust in automation. r=froydnj

Work around excessive command-line length issues by
disabling incremental rust compilation, which is enabled
by default outside `cargo --release` starting with Rust 1.24.

Incremental rust builds shouldn't help much in automation,
where sccache provides the only continuity between build
environments. In the meantime, they add a lot of object
files to the link line.

See https://github.com/rust-lang/rust/pull/47507 about addressing
the underlying issue upstream.

MozReview-Commit-ID: LRwUj3fhiaO

diff --git a/config/rules.mk b/config/rules.mk
--- a/config/rules.mk
+++ b/config/rules.mk
@@ -861,16 +861,22 @@ cargo_rustc_flags += -Clto
 # Versions of rust >= 1.45 need -Cembed-bitcode=yes for all crates when
 # using -Clto.
 ifeq (,$(filter 1.37.% 1.38.% 1.39.% 1.40.% 1.41.% 1.42.% 1.43.% 1.44.%,$(RUSTC_VERSION)))
 RUSTFLAGS += -Cembed-bitcode=yes
 endif
 endif
 endif
 
+# Disable incremental Rust compilation in automation builds, where
+# the lack of environmental continuity makes it unhelpful.
+ifdef MOZ_AUTOMATION
+cargo_incremental := CARGO_INCREMENTAL=0
+endif
+
 rustflags_override = RUSTFLAGS='$(MOZ_RUST_DEFAULT_FLAGS) $(RUSTFLAGS)'
 
 ifdef MOZ_MSVCBITS
 # If we are building a MozillaBuild shell, we want to clear out the
 # vcvars.bat environment variables for cargo builds. This is because
 # a 32-bit MozillaBuild shell on a 64-bit machine will try to use
 # the 32-bit compiler/linker for everything, while cargo/rustc wants
 # to use the 64-bit linker for build.rs scripts. This conflict results
@@ -897,16 +903,17 @@ define RUN_CARGO
 	RUSTC=$(RUSTC) \
 	MOZ_SRC=$(topsrcdir) \
 	MOZ_DIST=$(ABS_DIST) \
 	LIBCLANG_PATH="$(MOZ_LIBCLANG_PATH)" \
 	CLANG_PATH="$(MOZ_CLANG_PATH)" \
 	PKG_CONFIG_ALLOW_CROSS=1 \
 	RUST_BACKTRACE=full \
 	MOZ_TOPOBJDIR=$(topobjdir) \
+	$(cargo_incremental) \
 	$(2) \
 	$(CARGO) $(1) $(cargo_build_flags)
 endef
 
 # This function is intended to be called by:
 #
 #   $(call CARGO_BUILD,EXTRA_ENV_VAR1=X EXTRA_ENV_VAR2=Y ...)
 #
