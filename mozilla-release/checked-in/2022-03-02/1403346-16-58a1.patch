# HG changeset patch
# User Chris Manchester <cmanchester@mozilla.com>
# Date 1508969530 25200
# Node ID c2886f83079382ba130132cb981f82eefc799c09
# Parent  edd848554127403d227ca6e62de0be23de63e28b
Bug 1403346 - Define flags loading the clang plugin in configure rather than the make backend. r=glandium

MozReview-Commit-ID: EubsjJl1LBS

diff --git a/build/autoconf/clang-plugin.m4 b/build/autoconf/clang-plugin.m4
--- a/build/autoconf/clang-plugin.m4
+++ b/build/autoconf/clang-plugin.m4
@@ -1,18 +1,14 @@
 dnl This Source Code Form is subject to the terms of the Mozilla Public
 dnl License, v. 2.0. If a copy of the MPL was not distributed with this
 dnl file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 AC_DEFUN([MOZ_CONFIG_CLANG_PLUGIN], [
 
-MOZ_ARG_ENABLE_BOOL(clang-plugin,
-[  --enable-clang-plugin   Enable building with the mozilla clang plugin ],
-   ENABLE_CLANG_PLUGIN=1,
-   ENABLE_CLANG_PLUGIN= )
 if test -n "$ENABLE_CLANG_PLUGIN"; then
     if test -z "${CLANG_CC}${CLANG_CL}"; then
         AC_MSG_ERROR([Can't use clang plugin without clang.])
     fi
 
     AC_MSG_CHECKING([for llvm-config])
     if test -z "$LLVMCONFIG"; then
       if test -n "$CLANG_CL"; then
@@ -150,18 +146,21 @@ if test -n "$ENABLE_CLANG_PLUGIN"; then
             CXXFLAGS="$_SAVE_CXXFLAGS"
             export MACOSX_DEPLOYMENT_TARGET="$_SAVE_MACOSX_DEPLOYMENT_TARGET"
             AC_LANG_RESTORE
         ])
     if test "$ac_cv_has_accepts_ignoringParenImpCasts" = "yes"; then
       LLVM_CXXFLAGS="$LLVM_CXXFLAGS -DHAS_ACCEPTS_IGNORINGPARENIMPCASTS"
     fi
 
+    CLANG_PLUGIN_FLAGS="-Xclang -load -Xclang $CLANG_PLUGIN -Xclang -add-plugin -Xclang moz-check"
+
     AC_DEFINE(MOZ_CLANG_PLUGIN)
 fi
 
+AC_SUBST_LIST(CLANG_PLUGIN_FLAGS)
 AC_SUBST(LLVM_CXXFLAGS)
 AC_SUBST(LLVM_LDFLAGS)
 AC_SUBST(CLANG_LDFLAGS)
 
 AC_SUBST(ENABLE_CLANG_PLUGIN)
 
 ])
diff --git a/build/moz.configure/old.configure b/build/moz.configure/old.configure
--- a/build/moz.configure/old.configure
+++ b/build/moz.configure/old.configure
@@ -168,17 +168,16 @@ def old_configure_options(*options):
     '--datadir',
     '--enable-accessibility',
     '--enable-address-sanitizer',
     '--enable-alsa',
     '--enable-b2g-bt',
     '--enable-b2g-camera',
     '--enable-b2g-ril',
     '--enable-bundled-fonts',
-    '--enable-clang-plugin',
     '--enable-content-sandbox',
     '--enable-cookies',
     '--enable-cpp-rtti',
     '--enable-crashreporter',
     '--enable-dbus',
     '--enable-debug-js-modules',
     '--enable-directshow',
     '--enable-dtrace',
diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -1327,10 +1327,18 @@ def select_linker(linker, c_compiler, de
             return namespace(
                 KIND='lld',
                 LINKER_FLAG=lld,
             )
         else:
             die("Could not use lld as linker")
 
 
-set_config('LD_IS_BFD', depends(select_linker.KIND)(lambda x: x == 'bfd' or None))
+set_config('LD_IS_BFD', depends(select_linker.KIND)
+           (lambda x: x == 'bfd' or None))
 set_config('LINKER_LDFLAGS', select_linker.LINKER_FLAG)
+
+
+js_option('--enable-clang-plugin', env='ENABLE_CLANG_PLUGIN',
+          help="Enable building with the mozilla clang plugin")
+
+add_old_configure_assignment('ENABLE_CLANG_PLUGIN',
+                             depends_if('--enable-clang-plugin')(lambda _: True))
diff --git a/config/static-checking-config.mk b/config/static-checking-config.mk
--- a/config/static-checking-config.mk
+++ b/config/static-checking-config.mk
@@ -9,12 +9,11 @@ ifdef ENABLE_CLANG_PLUGIN
 # Replace "clang-cl.exe" to "clang.exe --driver-mode=cl" to avoid loading the
 # module clang.exe again when load the plugin dll, which links to the import
 # library of clang.exe.
 ifeq ($(OS_ARCH),WINNT)
 CC := $(subst clang-cl.exe,clang.exe --driver-mode=cl,$(CC:.EXE=.exe))
 CXX := $(subst clang-cl.exe,clang.exe --driver-mode=cl,$(CXX:.EXE=.exe))
 endif
 
-CLANG_PLUGIN := $(topobjdir)/build/clang-plugin/$(DLL_PREFIX)clang-plugin$(DLL_SUFFIX)
-OS_CXXFLAGS += -Xclang -load -Xclang $(CLANG_PLUGIN) -Xclang -add-plugin -Xclang moz-check
-OS_CFLAGS += -Xclang -load -Xclang $(CLANG_PLUGIN) -Xclang -add-plugin -Xclang moz-check
+OS_CXXFLAGS += $(CLANG_PLUGIN_FLAGS)
+OS_CFLAGS += $(CLANG_PLUGIN_FLAGS)
 endif
diff --git a/moz.configure b/moz.configure
--- a/moz.configure
+++ b/moz.configure
@@ -275,16 +275,32 @@ def build_gtest(pgo, build_project, targ
     if not enabled:
         return None
     if (automation and build_project == 'browser' and
         not (pgo and target.os == 'WINNT')):
         return True
 
 set_config('LINK_GTEST_DURING_COMPILE', build_gtest)
 
+# clang-plugin location
+# ==============================================================
+@depends(library_name_info, check_build_environment, when='--enable-clang-plugin')
+def clang_plugin_path(library_name_info, build_env):
+    topobjdir = build_env.topobjdir
+    if topobjdir.endswith('/js/src'):
+        topobjdir = topobjdir[:-7]
+    return os.path.abspath(
+        os.path.join(topobjdir, 'build', 'clang-plugin',
+                     '%sclang-plugin%s' % (library_name_info.dll.prefix,
+                                           library_name_info.dll.suffix))
+    )
+
+add_old_configure_assignment('CLANG_PLUGIN', clang_plugin_path)
+
+
 # Awk detection
 # ==============================================================
 awk = check_prog('AWK', ('gawk', 'mawk', 'nawk', 'awk'))
 
 # Until the AWK variable is not necessary in old-configure
 @depends(awk)
 def awk_for_old_configure(value):
     return value
