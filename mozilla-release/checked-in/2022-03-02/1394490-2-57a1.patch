# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1504209263 14400
#      Thu Aug 31 15:54:23 2017 -0400
# Node ID f44c599a011063c50aa192e1ae3f11a58ccf0f97
# Parent  e68ea9a127adc6244168c5f899520c3bf45e2866
Bug 1394490 - Use global lexical this to initialize NSVO lexical r=jandem

This is already current behavior, but make it explicit to allow cleanup
of NSVOs handling.

MozReview-Commit-ID: LeWjzwxstEB

diff --git a/js/src/builtin/Eval.cpp b/js/src/builtin/Eval.cpp
--- a/js/src/builtin/Eval.cpp
+++ b/js/src/builtin/Eval.cpp
@@ -458,17 +458,19 @@ js::ExecuteInGlobalAndReturnScope(JSCont
     }
 
     Rooted<EnvironmentObject*> env(cx, NonSyntacticVariablesObject::create(cx));
     if (!env)
         return false;
 
     // Unlike the non-syntactic scope chain API used by the subscript loader,
     // this API creates a fresh block scope each time.
-    env = LexicalEnvironmentObject::createNonSyntactic(cx, env);
+    //
+    // NOTE: Gecko FrameScripts expect lexical |this| to be the global.
+    env = LexicalEnvironmentObject::createNonSyntactic(cx, env, global);
     if (!env)
         return false;
 
     RootedValue rval(cx);
     if (!ExecuteKernel(cx, script, *env, UndefinedValue(),
                        NullFramePtr() /* evalInFrame */, rval.address()))
     {
         return false;
diff --git a/js/src/jscompartment.cpp b/js/src/jscompartment.cpp
--- a/js/src/jscompartment.cpp
+++ b/js/src/jscompartment.cpp
@@ -576,17 +576,17 @@ JSCompartment::getOrCreateNonSyntacticLe
     RootedObject key(cx, enclosing);
     if (enclosing->is<WithEnvironmentObject>()) {
         MOZ_ASSERT(!enclosing->as<WithEnvironmentObject>().isSyntactic());
         key = &enclosing->as<WithEnvironmentObject>().object();
     }
     RootedObject lexicalEnv(cx, nonSyntacticLexicalEnvironments_->lookup(key));
 
     if (!lexicalEnv) {
-        lexicalEnv = LexicalEnvironmentObject::createNonSyntactic(cx, enclosing);
+        lexicalEnv = LexicalEnvironmentObject::createNonSyntactic(cx, enclosing, enclosing);
         if (!lexicalEnv)
             return nullptr;
         if (!nonSyntacticLexicalEnvironments_->add(cx, key, lexicalEnv))
             return nullptr;
     }
 
     return &lexicalEnv->as<LexicalEnvironmentObject>();
 }
diff --git a/js/src/vm/EnvironmentObject.cpp b/js/src/vm/EnvironmentObject.cpp
--- a/js/src/vm/EnvironmentObject.cpp
+++ b/js/src/vm/EnvironmentObject.cpp
@@ -971,31 +971,33 @@ LexicalEnvironmentObject::createGlobal(J
     if (!JSObject::setSingleton(cx, env))
         return nullptr;
 
     env->initThisValue(global);
     return env;
 }
 
 /* static */ LexicalEnvironmentObject*
-LexicalEnvironmentObject::createNonSyntactic(JSContext* cx, HandleObject enclosing)
+LexicalEnvironmentObject::createNonSyntactic(JSContext* cx, HandleObject enclosing,
+                                             HandleObject thisv)
 {
     MOZ_ASSERT(enclosing);
     MOZ_ASSERT(!IsSyntacticEnvironment(enclosing));
 
     RootedShape shape(cx, LexicalScope::getEmptyExtensibleEnvironmentShape(cx));
     if (!shape)
         return nullptr;
 
     LexicalEnvironmentObject* env =
         LexicalEnvironmentObject::createTemplateObject(cx, shape, enclosing, gc::TenuredHeap);
     if (!env)
         return nullptr;
 
-    env->initThisValue(enclosing);
+    env->initThisValue(thisv);
+
     return env;
 }
 
 /* static */ LexicalEnvironmentObject*
 LexicalEnvironmentObject::createHollowForDebug(JSContext* cx, Handle<LexicalScope*> scope)
 {
     MOZ_ASSERT(!scope->hasEnvironment());
 
diff --git a/js/src/vm/EnvironmentObject.h b/js/src/vm/EnvironmentObject.h
--- a/js/src/vm/EnvironmentObject.h
+++ b/js/src/vm/EnvironmentObject.h
@@ -505,17 +505,18 @@ class LexicalEnvironmentObject : public 
     }
 
   public:
     static LexicalEnvironmentObject* create(JSContext* cx, Handle<LexicalScope*> scope,
                                             HandleObject enclosing, gc::InitialHeap heap);
     static LexicalEnvironmentObject* create(JSContext* cx, Handle<LexicalScope*> scope,
                                             AbstractFramePtr frame);
     static LexicalEnvironmentObject* createGlobal(JSContext* cx, Handle<GlobalObject*> global);
-    static LexicalEnvironmentObject* createNonSyntactic(JSContext* cx, HandleObject enclosing);
+    static LexicalEnvironmentObject* createNonSyntactic(JSContext* cx, HandleObject enclosing,
+                                                        HandleObject thisv);
     static LexicalEnvironmentObject* createHollowForDebug(JSContext* cx,
                                                           Handle<LexicalScope*> scope);
 
     // Create a new LexicalEnvironmentObject with the same enclosing env and
     // variable values as this.
     static LexicalEnvironmentObject* clone(JSContext* cx, Handle<LexicalEnvironmentObject*> env);
 
     // Create a new LexicalEnvironmentObject with the same enclosing env as
