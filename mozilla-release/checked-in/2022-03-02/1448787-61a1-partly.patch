# HG changeset patch
# User Franziskus Kiefer <franziskuskiefer@gmail.com>
# Date 1522317093 -7200
# Node ID 17e7971cb125034bb7b603804113cedfdb6db0ab
# Parent  2d75b228460cd41d7d34fee2d938620c2a0b5cf7
Bug 1448787 - separate error for self-signed certs, r=keeler,johannh

Reviewed By: keeler, johannh

Bug #: 1448787

Differential Revision: https://phabricator.services.mozilla.com/D805

diff --git a/security/certverifier/CertVerifier.cpp b/security/certverifier/CertVerifier.cpp
--- a/security/certverifier/CertVerifier.cpp
+++ b/security/certverifier/CertVerifier.cpp
@@ -864,16 +864,36 @@ CertVerifier::VerifyCert(CERTCertificate
 
   if (rv != Success) {
     return rv;
   }
 
   return Success;
 }
 
+
+static bool
+CertIsSelfSigned(const UniqueCERTCertificate& cert, void* pinarg)
+{
+  if (!SECITEM_ItemsAreEqual(&cert->derIssuer, &cert->derSubject)) {
+    return false;
+  }
+
+  // Check that the certificate is signed with the cert's spki.
+  SECStatus rv = CERT_VerifySignedDataWithPublicKeyInfo(
+    const_cast<CERTSignedData*>(&cert->signatureWrap),
+    const_cast<CERTSubjectPublicKeyInfo*>(&cert->subjectPublicKeyInfo),
+    pinarg);
+  if (rv != SECSuccess) {
+    return false;
+  }
+
+  return true;
+}
+
 Result
 CertVerifier::VerifySSLServerCert(const UniqueCERTCertificate& peerCert,
                      /*optional*/ const SECItem* stapledOCSPResponse,
                      /*optional*/ const SECItem* sctsFromTLS,
                                   Time time,
                      /*optional*/ void* pinarg,
                                   const nsACString& hostname,
                           /*out*/ UniqueCERTCertList& builtChain,
@@ -903,16 +923,22 @@ CertVerifier::VerifySSLServerCert(const 
   // if VerifyCert succeeded.
   Result rv = VerifyCert(peerCert.get(), certificateUsageSSLServer, time,
                          pinarg, PromiseFlatCString(hostname).get(), builtChain,
                          flags, stapledOCSPResponse, sctsFromTLS,
                          originAttributes, evOidPolicy, ocspStaplingStatus,
                          keySizeStatus, sha1ModeResult, pinningTelemetryInfo,
                          ctInfo);
   if (rv != Success) {
+    if (rv == Result::ERROR_UNKNOWN_ISSUER &&
+        CertIsSelfSigned(peerCert, pinarg)) {
+      // In this case we didn't find any issuer for the certificate and the
+      // certificate is self-signed.
+      return Result::ERROR_SELF_SIGNED_CERT;
+    }
     return rv;
   }
 
   Input peerCertInput;
   rv = peerCertInput.Init(peerCert->derCert.data, peerCert->derCert.len);
   if (rv != Success) {
     return rv;
   }
diff --git a/security/manager/locales/en-US/chrome/pipnss/nsserrors.properties b/security/manager/locales/en-US/chrome/pipnss/nsserrors.properties
--- a/security/manager/locales/en-US/chrome/pipnss/nsserrors.properties
+++ b/security/manager/locales/en-US/chrome/pipnss/nsserrors.properties
@@ -323,8 +323,9 @@ MOZILLA_PKIX_ERROR_NOT_YET_VALID_CERTIFI
 MOZILLA_PKIX_ERROR_NOT_YET_VALID_ISSUER_CERTIFICATE=A certificate that is not yet valid was used to issue the server’s certificate.
 MOZILLA_PKIX_ERROR_SIGNATURE_ALGORITHM_MISMATCH=The signature algorithm in the signature field of the certificate does not match the algorithm in its signatureAlgorithm field.
 MOZILLA_PKIX_ERROR_OCSP_RESPONSE_FOR_CERT_MISSING=The OCSP response does not include a status for the certificate being verified.
 MOZILLA_PKIX_ERROR_VALIDITY_TOO_LONG=The server presented a certificate that is valid for too long.
 MOZILLA_PKIX_ERROR_REQUIRED_TLS_FEATURE_MISSING=A required TLS feature is missing.
 MOZILLA_PKIX_ERROR_INVALID_INTEGER_ENCODING=The server presented a certificate that contains an invalid encoding of an integer. Common causes include negative serial numbers, negative RSA moduli, and encodings that are longer than necessary.
 MOZILLA_PKIX_ERROR_EMPTY_ISSUER_NAME=The server presented a certificate with an empty issuer distinguished name.
 MOZILLA_PKIX_ERROR_ADDITIONAL_POLICY_CONSTRAINT_FAILED=An additional policy constraint failed when validating this certificate.
+MOZILLA_PKIX_ERROR_SELF_SIGNED_CERT=The certificate is not trusted because it is self-signed.
diff --git a/security/manager/ssl/NSSErrorsService.cpp b/security/manager/ssl/NSSErrorsService.cpp
--- a/security/manager/ssl/NSSErrorsService.cpp
+++ b/security/manager/ssl/NSSErrorsService.cpp
@@ -147,16 +147,17 @@ ErrorIsOverridable(PRErrorCode code)
   {
     // Overridable errors.
     case mozilla::pkix::MOZILLA_PKIX_ERROR_ADDITIONAL_POLICY_CONSTRAINT_FAILED:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_CA_CERT_USED_AS_END_ENTITY:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_EMPTY_ISSUER_NAME:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_INADEQUATE_KEY_SIZE:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_NOT_YET_VALID_CERTIFICATE:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_NOT_YET_VALID_ISSUER_CERTIFICATE:
+    case mozilla::pkix::MOZILLA_PKIX_ERROR_SELF_SIGNED_CERT:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_V1_CERT_USED_AS_CA:
     case SEC_ERROR_CA_CERT_INVALID:
     case SEC_ERROR_CERT_SIGNATURE_ALGORITHM_DISABLED:
     case SEC_ERROR_EXPIRED_CERTIFICATE:
     case SEC_ERROR_EXPIRED_ISSUER_CERTIFICATE:
     case SEC_ERROR_INVALID_TIME:
     case SEC_ERROR_UNKNOWN_ISSUER:
     case SSL_ERROR_BAD_CERT_DOMAIN:
diff --git a/security/manager/ssl/SSLServerCertVerification.cpp b/security/manager/ssl/SSLServerCertVerification.cpp
--- a/security/manager/ssl/SSLServerCertVerification.cpp
+++ b/security/manager/ssl/SSLServerCertVerification.cpp
@@ -298,16 +298,17 @@ MapOverridableErrorToProbeValue(PRErrorC
     case mozilla::pkix::MOZILLA_PKIX_ERROR_INADEQUATE_KEY_SIZE: return 13;
     case mozilla::pkix::MOZILLA_PKIX_ERROR_NOT_YET_VALID_CERTIFICATE: return 14;
     case mozilla::pkix::MOZILLA_PKIX_ERROR_NOT_YET_VALID_ISSUER_CERTIFICATE:
       return 15;
     case SEC_ERROR_INVALID_TIME: return 16;
     case mozilla::pkix::MOZILLA_PKIX_ERROR_EMPTY_ISSUER_NAME: return 17;
     case mozilla::pkix::MOZILLA_PKIX_ERROR_ADDITIONAL_POLICY_CONSTRAINT_FAILED:
       return 18;
+    case mozilla::pkix::MOZILLA_PKIX_ERROR_SELF_SIGNED_CERT: return 19;
   }
   NS_WARNING("Unknown certificate error code. Does MapOverridableErrorToProbeValue "
              "handle everything in DetermineCertOverrideErrors?");
   return 0;
 }
 
 static uint32_t
 MapCertErrorToProbeValue(PRErrorCode errorCode)
@@ -357,20 +358,21 @@ DetermineCertOverrideErrors(const Unique
   // called if CertVerifier::VerifyCert succeeded.
   switch (defaultErrorCodeToReport) {
     case SEC_ERROR_CERT_SIGNATURE_ALGORITHM_DISABLED:
     case SEC_ERROR_EXPIRED_ISSUER_CERTIFICATE:
     case SEC_ERROR_UNKNOWN_ISSUER:
     case SEC_ERROR_CA_CERT_INVALID:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_ADDITIONAL_POLICY_CONSTRAINT_FAILED:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_CA_CERT_USED_AS_END_ENTITY:
+    case mozilla::pkix::MOZILLA_PKIX_ERROR_EMPTY_ISSUER_NAME:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_INADEQUATE_KEY_SIZE:
+    case mozilla::pkix::MOZILLA_PKIX_ERROR_NOT_YET_VALID_ISSUER_CERTIFICATE:
+    case mozilla::pkix::MOZILLA_PKIX_ERROR_SELF_SIGNED_CERT:
     case mozilla::pkix::MOZILLA_PKIX_ERROR_V1_CERT_USED_AS_CA:
-    case mozilla::pkix::MOZILLA_PKIX_ERROR_NOT_YET_VALID_ISSUER_CERTIFICATE:
-    case mozilla::pkix::MOZILLA_PKIX_ERROR_EMPTY_ISSUER_NAME:
     {
       collectedErrors = nsICertOverrideService::ERROR_UNTRUSTED;
       errorCodeTrust = defaultErrorCodeToReport;
 
       SECCertTimeValidity validity = CERT_CheckCertValidTimes(cert.get(), now,
                                                               false);
       if (validity == secCertTimeUndetermined) {
         // This only happens if cert is null. CERT_CheckCertValidTimes will
diff --git a/security/manager/ssl/tests/unit/head_psm.js b/security/manager/ssl/tests/unit/head_psm.js
--- a/security/manager/ssl/tests/unit/head_psm.js
+++ b/security/manager/ssl/tests/unit/head_psm.js
@@ -78,16 +78,17 @@ const MOZILLA_PKIX_ERROR_CA_CERT_USED_AS
 const MOZILLA_PKIX_ERROR_INADEQUATE_KEY_SIZE            = MOZILLA_PKIX_ERROR_BASE + 2;
 const MOZILLA_PKIX_ERROR_V1_CERT_USED_AS_CA             = MOZILLA_PKIX_ERROR_BASE + 3;
 const MOZILLA_PKIX_ERROR_NOT_YET_VALID_CERTIFICATE      = MOZILLA_PKIX_ERROR_BASE + 5;
 const MOZILLA_PKIX_ERROR_NOT_YET_VALID_ISSUER_CERTIFICATE = MOZILLA_PKIX_ERROR_BASE + 6;
 const MOZILLA_PKIX_ERROR_OCSP_RESPONSE_FOR_CERT_MISSING = MOZILLA_PKIX_ERROR_BASE + 8;
 const MOZILLA_PKIX_ERROR_REQUIRED_TLS_FEATURE_MISSING   = MOZILLA_PKIX_ERROR_BASE + 10;
 const MOZILLA_PKIX_ERROR_EMPTY_ISSUER_NAME              = MOZILLA_PKIX_ERROR_BASE + 12;
 const MOZILLA_PKIX_ERROR_ADDITIONAL_POLICY_CONSTRAINT_FAILED = MOZILLA_PKIX_ERROR_BASE + 13;
+const MOZILLA_PKIX_ERROR_SELF_SIGNED_CERT               = MOZILLA_PKIX_ERROR_BASE + 14;
 
 // Supported Certificate Usages
 const certificateUsageSSLClient              = 0x0001;
 const certificateUsageSSLServer              = 0x0002;
 const certificateUsageSSLCA                  = 0x0008;
 const certificateUsageEmailSigner            = 0x0010;
 const certificateUsageEmailRecipient         = 0x0020;
 
diff --git a/security/manager/ssl/tests/unit/test_cert_overrides.js b/security/manager/ssl/tests/unit/test_cert_overrides.js
--- a/security/manager/ssl/tests/unit/test_cert_overrides.js
+++ b/security/manager/ssl/tests/unit/test_cert_overrides.js
@@ -131,17 +131,17 @@ function add_simple_tests() {
   add_cert_override_test("notyetvalid.example.com",
                          Ci.nsICertOverrideService.ERROR_TIME,
                          MOZILLA_PKIX_ERROR_NOT_YET_VALID_CERTIFICATE);
   add_cert_override_test("before-epoch.example.com",
                          Ci.nsICertOverrideService.ERROR_TIME,
                          SEC_ERROR_INVALID_TIME);
   add_cert_override_test("selfsigned.example.com",
                          Ci.nsICertOverrideService.ERROR_UNTRUSTED,
-                         SEC_ERROR_UNKNOWN_ISSUER);
+                         MOZILLA_PKIX_ERROR_SELF_SIGNED_CERT);
   add_cert_override_test("unknownissuer.example.com",
                          Ci.nsICertOverrideService.ERROR_UNTRUSTED,
                          SEC_ERROR_UNKNOWN_ISSUER);
   add_cert_override_test("expiredissuer.example.com",
                          Ci.nsICertOverrideService.ERROR_UNTRUSTED,
                          SEC_ERROR_EXPIRED_ISSUER_CERTIFICATE);
   add_cert_override_test("notyetvalidissuer.example.com",
                          Ci.nsICertOverrideService.ERROR_UNTRUSTED,
@@ -196,17 +196,17 @@ function add_simple_tests() {
     run_next_test();
   });
 
   // Bug 990603: Apache documentation has recommended generating a self-signed
   // test certificate with basic constraints: CA:true. For compatibility, this
   // is a scenario in which an override is allowed.
   add_cert_override_test("self-signed-end-entity-with-cA-true.example.com",
                          Ci.nsICertOverrideService.ERROR_UNTRUSTED,
-                         SEC_ERROR_UNKNOWN_ISSUER);
+                         MOZILLA_PKIX_ERROR_SELF_SIGNED_CERT);
 
   add_cert_override_test("ca-used-as-end-entity.example.com",
                          Ci.nsICertOverrideService.ERROR_UNTRUSTED,
                          MOZILLA_PKIX_ERROR_CA_CERT_USED_AS_END_ENTITY);
 
   // If an X.509 version 1 certificate is not a trust anchor, we will
   // encounter an overridable error.
   add_cert_override_test("end-entity-issued-by-v1-cert.example.com",
