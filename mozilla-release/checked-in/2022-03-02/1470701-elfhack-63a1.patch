# HG changeset patch
# User Mike Hommey <mh@glandium.org>
# Date 1529798558 -32400
#      Sun Jun 24 09:02:38 2018 +0900
# Node ID ded85684155377e4ac405fca8d01f61ae150dd0d
# Parent  28bedb658af486ccd8ed69dff559327aa2653a1c
Bug 1470701 - Use run-time page size when changing mapping permissions in elfhack injected code. r=froydnj

When a binary has a PT_GNU_RELRO segment, the elfhack injected code
uses mprotect to add the writable flag to relocated pages before
applying relocations, removing it afterwards. To do so, the elfhack
program uses the location and size of the PT_GNU_RELRO segment, and
adjusts it to be aligned according to the PT_LOAD alignment.

The problem here is that the PT_LOAD alignment doesn't necessarily match
the actual page alignment, and the resulting mprotect may end up not
covering the full extent of what the dynamic linker has protected
read-only according to the PT_GNU_RELRO segment. In turn, this can lead
to a crash on startup when trying to apply relocations to the still
read-only locations.

Practically speaking, this doesn't end up being a problem on x86, where
the PT_LOAD alignment is usually 4096, which happens to be the page
size, but on Debian armhf, it is 64k, while the run time page size can be
4k.

diff --git a/build/unix/elfhack/elfhack.cpp b/build/unix/elfhack/elfhack.cpp
--- a/build/unix/elfhack/elfhack.cpp
+++ b/build/unix/elfhack/elfhack.cpp
@@ -85,19 +85,20 @@ public:
     }
 private:
     std::vector<Elf_RelHack> rels;
 };
 
 class ElfRelHackCode_Section: public ElfSection {
 public:
     ElfRelHackCode_Section(Elf_Shdr &s, Elf &e, ElfRelHack_Section &relhack_section,
-                           unsigned int init, unsigned int mprotect_cb)
+                           unsigned int init, unsigned int mprotect_cb,
+                           unsigned int sysconf_cb)
     : ElfSection(s, nullptr, nullptr), parent(e), relhack_section(relhack_section),
-      init(init), mprotect_cb(mprotect_cb) {
+      init(init), mprotect_cb(mprotect_cb), sysconf_cb(sysconf_cb) {
         std::string file(rundir);
         file += "/inject/";
         switch (parent.getMachine()) {
         case EM_386:
             file += "x86";
             break;
         case EM_X86_64:
             file += "x86_64";
@@ -123,17 +124,16 @@ public:
              section = section->getNext()) {
             if (section->getType() == SHT_SYMTAB)
                 symtab = (ElfSymtab_Section *) section;
         }
         if (symtab == nullptr)
             throw std::runtime_error("Couldn't find a symbol table for the injected code");
 
         relro = parent.getSegmentByType(PT_GNU_RELRO);
-        align = parent.getSegmentByType(PT_LOAD)->getAlign();
 
         // Find the init symbol
         entry_point = -1;
         std::string symbol = "init";
         if (!init)
             symbol += "_noinit";
         if (relro)
             symbol += "_relro";
@@ -360,22 +360,22 @@ private:
                 } else if (strcmp(name, "elf_header") == 0) {
                     // TODO: change this ungly hack to something better
                     ElfSection *ehdr = parent.getSection(1)->getPrevious()->getPrevious();
                     addr = ehdr->getAddr();
                 } else if (strcmp(name, "original_init") == 0) {
                     addr = init;
                 } else if (relro && strcmp(name, "mprotect_cb") == 0) {
                     addr = mprotect_cb;
+                } else if (relro && strcmp(name, "sysconf_cb") == 0) {
+                    addr = sysconf_cb;
                 } else if (relro && strcmp(name, "relro_start") == 0) {
-                    // Align relro segment start to the start of the page it starts in.
-                    addr = relro->getAddr() & ~(align - 1);
-                    // Align relro segment end to the start of the page it ends into.
+                    addr = relro->getAddr();
                 } else if (relro && strcmp(name, "relro_end") == 0) {
-                    addr = (relro->getAddr() + relro->getMemSize()) & ~(align - 1);
+                    addr = (relro->getAddr() + relro->getMemSize());
                 } else if (strcmp(name, "_GLOBAL_OFFSET_TABLE_") == 0) {
                     // We actually don't need a GOT, but need it as a reference for
                     // GOTOFF relocations. We'll just use the start of the ELF file
                     addr = 0;
                 } else if (strcmp(name, "") == 0) {
                     // This is for R_ARM_V4BX, until we find something better
                     addr = -1;
                 } else {
@@ -419,19 +419,19 @@ private:
         }
     }
 
     Elf *elf, &parent;
     ElfRelHack_Section &relhack_section;
     std::vector<ElfSection *> code;
     unsigned int init;
     unsigned int mprotect_cb;
+    unsigned int sysconf_cb;
     int entry_point;
     ElfSegment *relro;
-    unsigned int align;
 };
 
 unsigned int get_addend(Elf_Rel *rel, Elf *elf) {
     ElfLocation loc(rel->r_offset, elf);
     Elf_Addr addr(loc.getBuffer(), Elf_Addr::size(elf->getClass()), elf->getClass(), elf->getData());
     return addr.value;
 }
 
@@ -697,81 +697,98 @@ int do_relocation_section(Elf *elf, unsi
             original_init = symtab->syms[ELF32_R_SYM(rel->r_info)].value.getValue() + addend;
         } else {
             fprintf(stderr, "Unsupported relocation type for DT_INIT_ARRAY's first entry. Skipping\n");
             return -1;
         }
     }
 
     unsigned int mprotect_cb = 0;
+    unsigned int sysconf_cb = 0;
     // If there is a relro segment, our injected code will run after the linker sets the
     // corresponding pages read-only. We need to make our code change that to read-write
     // before applying relocations, which means it needs to call mprotect.
     // To do that, we need to find a reference to the mprotect symbol. In case the library
     // already has one, we use that, but otherwise, we add the symbol.
     // Then the injected code needs to be able to call the corresponding function, which
     // means it needs access to a pointer to it. We get such a pointer by making the linker
     // apply a relocation for the symbol at an address our code can read.
     // The problem here is that there is not much relocated space where we can put such a
     // pointer, so we abuse the bss section temporarily (it will be restored to a null
     // value before any code can actually use it)
     if (elf->getSegmentByType(PT_GNU_RELRO)) {
-        Elf_SymValue *mprotect = symtab->lookup("mprotect", STT(FUNC));
-        if (!mprotect) {
-            symtab->syms.emplace_back();
-            mprotect = &symtab->syms.back();
-            symtab->grow(symtab->syms.size() * symtab->getEntSize());
-            mprotect->name = ((ElfStrtab_Section *)symtab->getLink())->getStr("mprotect");
-            mprotect->info = ELF32_ST_INFO(STB_GLOBAL, STT_FUNC);
-            mprotect->other = STV_DEFAULT;
-            new (&mprotect->value) ElfLocation(nullptr, 0, ElfLocation::ABSOLUTE);
-            mprotect->size = 0;
-            mprotect->defined = false;
+        ElfSection *gnu_versym = dyn->getSectionForType(DT_VERSYM);
+        auto lookup = [&symtab, &gnu_versym](const char* symbol) {
+            Elf_SymValue *sym_value = symtab->lookup(symbol, STT(FUNC));
+            if (!sym_value) {
+                symtab->syms.emplace_back();
+                sym_value = &symtab->syms.back();
+                symtab->grow(symtab->syms.size() * symtab->getEntSize());
+                sym_value->name = ((ElfStrtab_Section *)symtab->getLink())->getStr(symbol);
+                sym_value->info = ELF32_ST_INFO(STB_GLOBAL, STT_FUNC);
+                sym_value->other = STV_DEFAULT;
+                new (&sym_value->value) ElfLocation(nullptr, 0, ElfLocation::ABSOLUTE);
+                sym_value->size = 0;
+                sym_value->defined = false;
 
-            // The DT_VERSYM data (in the .gnu.version section) has the same number of
-            // entries as the symbols table. Since we added one entry there, we need to
-            // add one entry here. Zeroes in the extra data means no version for that
-            // symbol, which is the simplest thing to do.
-            ElfSection *gnu_versym = dyn->getSectionForType(DT_VERSYM);
-            if (gnu_versym) {
-               gnu_versym->grow(gnu_versym->getSize() + gnu_versym->getEntSize());
+                // The DT_VERSYM data (in the .gnu.version section) has the same number of
+                // entries as the symbols table. Since we added one entry there, we need to
+                // add one entry here. Zeroes in the extra data means no version for that
+                // symbol, which is the simplest thing to do.
+                if (gnu_versym) {
+                   gnu_versym->grow(gnu_versym->getSize() + gnu_versym->getEntSize());
+                }
             }
-        }
+            return sym_value;
+        };
+
+        Elf_SymValue *mprotect = lookup("mprotect");
+        Elf_SymValue *sysconf = lookup("sysconf");
 
-        // Add a relocation for the mprotect symbol.
-        new_rels.emplace_back();
-        Rel_Type &rel = new_rels.back();
-        memset(&rel, 0, sizeof(rel));
-        rel.r_info = ELF32_R_INFO(std::distance(symtab->syms.begin(), std::vector<Elf_SymValue>::iterator(mprotect)), rel_type2);
+        // Add relocations for the mprotect and sysconf symbols.
+        auto add_relocation_to = [&new_rels, &symtab, rel_type2](Elf_SymValue *symbol, unsigned int location) {
+            new_rels.emplace_back();
+            Rel_Type &rel = new_rels.back();
+            memset(&rel, 0, sizeof(rel));
+            rel.r_info = ELF32_R_INFO(
+                std::distance(symtab->syms.begin(),
+                              std::vector<Elf_SymValue>::iterator(symbol)),
+                rel_type2);
+            rel.r_offset = location;
+            return location;
+        };
+
 
         // Find the beginning of the bss section, and use an aligned location in there
         // for the relocation.
         for (ElfSection *s = elf->getSection(1); s != nullptr; s = s->getNext()) {
             if (s->getType() != SHT_NOBITS || (s->getFlags() & (SHF_TLS | SHF_WRITE)) != SHF_WRITE) {
                 continue;
             }
             size_t ptr_size = Elf_Addr::size(elf->getClass());
             size_t usable_start = (s->getAddr() + ptr_size - 1) & ~(ptr_size - 1);
             size_t usable_end = (s->getAddr() + s->getSize()) & ~(ptr_size - 1);
-            if (usable_end - usable_start >= ptr_size) {
-                mprotect_cb = rel.r_offset = usable_start;
+            if (usable_end - usable_start >= 2 * ptr_size) {
+                mprotect_cb = add_relocation_to(mprotect, usable_start);
+                sysconf_cb = add_relocation_to(sysconf, usable_start + ptr_size);
                 break;
             }
         }
 
-        if (mprotect_cb == 0) {
+        if (mprotect_cb == 0 || sysconf_cb == 0) {
             fprintf(stderr, "Couldn't find .bss. Skipping\n");
             return -1;
         }
     }
 
     section->rels.assign(new_rels.begin(), new_rels.end());
     section->shrink(new_rels.size() * section->getEntSize());
 
-    ElfRelHackCode_Section *relhackcode = new ElfRelHackCode_Section(relhackcode_section, *elf, *relhack, original_init, mprotect_cb);
+    ElfRelHackCode_Section *relhackcode = new ElfRelHackCode_Section(
+        relhackcode_section, *elf, *relhack, original_init, mprotect_cb, sysconf_cb);
     // Find the first executable section, and insert the relhack code before
     // that. The relhack data is inserted between .rel.dyn and .rel.plt.
     ElfSection *first_executable = nullptr;
     for (ElfSection *s = elf->getSection(1); s != nullptr;
          s = s->getNext()) {
         if (s->getFlags() & SHF_EXECINSTR) {
             first_executable = s;
             break;
diff --git a/build/unix/elfhack/inject.c b/build/unix/elfhack/inject.c
--- a/build/unix/elfhack/inject.c
+++ b/build/unix/elfhack/inject.c
@@ -1,14 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include <stdint.h>
 #include <stdlib.h>
+#include <unistd.h>
 #include <sys/mman.h>
 #include <elf.h>
 
 /* The Android NDK headers define those */
 #undef Elf_Ehdr
 #undef Elf_Addr
 
 #if defined(__LP64__)
@@ -20,16 +21,17 @@
 #endif
 
 extern __attribute__((visibility("hidden"))) void original_init(int argc, char **argv, char **env);
 
 extern __attribute__((visibility("hidden"))) Elf32_Rel relhack[];
 extern __attribute__((visibility("hidden"))) Elf_Ehdr elf_header;
 
 extern __attribute__((visibility("hidden"))) int (*mprotect_cb)(void *addr, size_t len, int prot);
+extern __attribute__((visibility("hidden"))) long (*sysconf_cb)(int name);
 extern __attribute__((visibility("hidden"))) char relro_start[];
 extern __attribute__((visibility("hidden"))) char relro_end[];
 
 static inline __attribute__((always_inline))
 void do_relocations(void)
 {
     Elf32_Rel *rel;
     Elf_Addr *ptr, *start;
@@ -53,43 +55,48 @@ int init(int argc, char **argv, char **e
     do_relocations();
     original_init(argc, argv, env);
     // Ensure there is no tail-call optimization, avoiding the use of the
     // B.W instruction in Thumb for the call above.
     return 0;
 }
 
 static inline __attribute__((always_inline))
-void relro_pre()
+void do_relocations_with_relro(void)
 {
+    long page_size = sysconf_cb(_SC_PAGESIZE);
+    uintptr_t aligned_relro_start = ((uintptr_t) relro_start) & ~(page_size - 1);
+    // The relro segment may not end at a page boundary. If that's the case, the
+    // remainder of the page needs to stay read-write, so the last page is never
+    // set read-only. Thus the aligned relro end is page-rounded down.
+    uintptr_t aligned_relro_end = ((uintptr_t) relro_end) & ~(page_size - 1);
     // By the time the injected code runs, the relro segment is read-only. But
     // we want to apply relocations in it, so we set it r/w first. We'll restore
     // it to read-only in relro_post.
-    mprotect_cb(relro_start, relro_end - relro_start, PROT_READ | PROT_WRITE);
-}
+    mprotect_cb((void *)aligned_relro_start,
+                aligned_relro_end - aligned_relro_start,
+                PROT_READ | PROT_WRITE);
+
+    do_relocations();
 
-static inline __attribute__((always_inline))
-void relro_post()
-{
-    mprotect_cb(relro_start, relro_end - relro_start, PROT_READ);
-    // mprotect_cb is a pointer allocated in .bss, so we need to restore it to
-    // a NULL value.
+    mprotect_cb((void *)aligned_relro_start,
+                aligned_relro_end - aligned_relro_start,
+                PROT_READ);
+    // mprotect_cb and sysconf_cb are allocated in .bss, so we need to restore
+    // them to a NULL value.
     mprotect_cb = NULL;
+    sysconf_cb = NULL;
 }
 
 __attribute__((section(".text._init_noinit_relro")))
 int init_noinit_relro(int argc, char **argv, char **env)
 {
-    relro_pre();
-    do_relocations();
-    relro_post();
+    do_relocations_with_relro();
     return 0;
 }
 
 __attribute__((section(".text._init_relro")))
 int init_relro(int argc, char **argv, char **env)
 {
-    relro_pre();
-    do_relocations();
-    relro_post();
+    do_relocations_with_relro();
     original_init(argc, argv, env);
     return 0;
 }
diff --git a/build/unix/elfhack/test.c b/build/unix/elfhack/test.c
--- a/build/unix/elfhack/test.c
+++ b/build/unix/elfhack/test.c
@@ -124,16 +124,20 @@ int print_status() {
  * Also, when .tbss is big enough, elfhack may wrongfully consider
  * following sections as part of the PT_TLS segment.
  * Finally, gold makes TLS segments end on an aligned virtual address,
  * even when the underlying section ends before that, and elfhack
  * sanity checks may yield an error. */
 __thread int foo;
 __thread long long int bar[512];
 
+/* We need a .bss that can hold at least 2 pointers. The static in
+ * end_test() plus this variable should do. */
+size_t dummy;
+
 void end_test() {
     static size_t count = 0;
     /* Only exit when both constructors have been called */
     if (++count == 2)
         ret = 0;
 }
 
 void test() {
