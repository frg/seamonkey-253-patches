# HG changeset patch
# User Nick Fitzgerald <fitzgen@gmail.com>
# Date 1504220353 25200
# Node ID a17a6af7ff937f4552d83017153ab6f4d7ffa7e3
# Parent  8048242656039657142d8d55e8dec3f9286874a4
Bug 1277338 - Part 7: Export mozglue when JS_STANDALONE && !jemalloc; r=glandium,sfink

This makes sure that:

* We don't define `MOZ_GLUE_IN_PROGRAM` so that everything in mozglue gets
  defined.

* `MFBT_API`'s symbol export rules match `JS_PUBLIC_API` and `EXPORT_JS_API`.

* We add mozglue to SpiderMonkey's `USE_LIBS` when jemalloc is disabled.

diff --git a/config/check_vanilla_allocations.py b/config/check_vanilla_allocations.py
--- a/config/check_vanilla_allocations.py
+++ b/config/check_vanilla_allocations.py
@@ -123,16 +123,22 @@ def main():
     emit_line_info = False
 
     for line in lines:
         m = re.search(alloc_fns_re, line)
         if m is None:
             continue
 
         filename = m.group(1)
+
+        # mozalloc contains calls to memalign. These are ok, so we whitelist
+        # them.
+        if "mozalloc" in filename:
+            continue
+
         fn = m.group(2)
         if filename == 'jsutil.o':
             jsutil_cpp.add(fn)
         else:
             # An allocation is present in a non-special file.  Fail!
             fail("'" + fn + "' present in " + filename)
             # Try to give more precise information about the offending code.
             emit_line_info = True
@@ -183,9 +189,8 @@ def main():
         sys.exit(1)
 
     print('TEST-PASS | check_vanilla_allocations.py | ok')
     sys.exit(0)
 
 
 if __name__ == '__main__':
     main()
-
diff --git a/js/moz.configure b/js/moz.configure
--- a/js/moz.configure
+++ b/js/moz.configure
@@ -17,22 +17,21 @@ def building_js(build_project, help):
 # will still need to be set depending on building_js above.
 option(env='JS_STANDALONE', default=building_js,
        help='Reserved for internal use')
 
 @depends('JS_STANDALONE')
 def js_standalone(value):
     if value:
         return True
-
 set_config('JS_STANDALONE', js_standalone)
+set_define('JS_STANDALONE', js_standalone)
 add_old_configure_assignment('JS_STANDALONE', js_standalone)
-
 js_option('--disable-js-shell', default=building_js,
-       help='Do not build the JS shell')
+          help='Do not build the JS shell')
 
 @depends('--disable-js-shell')
 def js_disable_shell(value):
     if not value:
         return True
 
 set_config('JS_DISABLE_SHELL', js_disable_shell)
 
diff --git a/js/src/build.rs b/js/src/build.rs
--- a/js/src/build.rs
+++ b/js/src/build.rs
@@ -20,17 +20,22 @@ fn main() {
         "plaindebug"
     } else {
         "plain"
     };
 
     let python = env::var("PYTHON").unwrap_or("python2.7".into());
     let mut cmd = Command::new(&python);
     cmd.args(&["./devtools/automation/autospider.py",
+               // Only build SpiderMonkey, don't run all the tests.
                "--build-only",
+               // Disable Mozilla's jemalloc; Rust has its own jemalloc that we
+               // can swap in instead and everything using a single malloc is
+               // good.
+               "--no-jemalloc",
                "--objdir", &out_dir,
                variant])
         .env("SOURCE", &js_src)
         .env("PWD", &js_src)
         .env("AUTOMATION", "1")
         .stdout(Stdio::inherit())
         .stderr(Stdio::inherit());
     println!("Running command: {:?}", cmd);
diff --git a/js/src/devtools/automation/autospider.py b/js/src/devtools/automation/autospider.py
--- a/js/src/devtools/automation/autospider.py
+++ b/js/src/devtools/automation/autospider.py
@@ -57,16 +57,24 @@ group.add_argument('--no-optimize', acti
 group.set_defaults(optimize=None)
 group = parser.add_mutually_exclusive_group()
 group.add_argument('--debug', action='store_true',
                    help='generate a debug build. Overrides variant setting.')
 group.add_argument('--no-debug', action='store_false',
                    dest='debug',
                    help='generate a non-debug build. Overrides variant setting.')
 group.set_defaults(debug=None)
+group = parser.add_mutually_exclusive_group()
+group.add_argument('--jemalloc', action='store_true',
+                   dest='jemalloc',
+                   help='use mozilla\'s jemalloc instead of the default allocator')
+group.add_argument('--no-jemalloc', action='store_false',
+                   dest='jemalloc',
+                   help='use the default allocator instead of mozilla\'s jemalloc')
+group.set_defaults(jemalloc=None)
 parser.add_argument('--run-tests', '--tests', type=str, metavar='TESTSUITE',
                     default='',
                     help="comma-separated set of test suites to add to the variant's default set")
 parser.add_argument('--skip-tests', '--skip', type=str, metavar='TESTSUITE',
                     default='',
                     help="comma-separated set of test suites to remove from the variant's default set")
 parser.add_argument('--build-only', '--build',
                     dest='skip_tests', action='store_const', const='all',
@@ -168,16 +176,20 @@ opt = args.debug
 if opt is None and args.platform:
     # Override variant['debug'].
     opt = ('-debug' in args.platform)
 if opt is None:
     opt = variant.get('debug')
 if opt is not None:
     CONFIGURE_ARGS += (" --enable-debug" if opt else " --disable-debug")
 
+opt = args.jemalloc
+if opt is not None:
+    CONFIGURE_ARGS += (" --enable-jemalloc" if opt else " --disable-jemalloc")
+
 # Any jobs that wish to produce additional output can save them into the upload
 # directory if there is such a thing, falling back to OBJDIR.
 env.setdefault('MOZ_UPLOAD_DIR', OBJDIR)
 ensure_dir_exists(env['MOZ_UPLOAD_DIR'], clobber=False)
 
 # Some of the variants request a particular word size (eg ARM simulators).
 word_bits = variant.get('bits')
 
diff --git a/js/src/old-configure.in b/js/src/old-configure.in
--- a/js/src/old-configure.in
+++ b/js/src/old-configure.in
@@ -1488,26 +1488,30 @@ dnl ====================================
 if test -z "$MOZ_ENABLE_WARNINGS_AS_ERRORS"; then
    WARNINGS_AS_ERRORS=''
 fi
 
 dnl ========================================================
 dnl = Enable jemalloc
 dnl ========================================================
 
-case "${OS_TARGET}" in
-Android|WINNT|Darwin)
+if test "$JS_STANDALONE" -a -z "$MOZ_MEMORY"; then
   MOZ_GLUE_IN_PROGRAM=
-  ;;
-*)
-  dnl On !Android !Windows !OSX, we only want to link executables against mozglue
-  MOZ_GLUE_IN_PROGRAM=1
-  AC_DEFINE(MOZ_GLUE_IN_PROGRAM)
-  ;;
-esac
+else
+  case "${OS_TARGET}" in
+  Android|WINNT|Darwin)
+    MOZ_GLUE_IN_PROGRAM=
+    ;;
+  *)
+    dnl On !Android !Windows !OSX, we only want to link executables against mozglue
+    MOZ_GLUE_IN_PROGRAM=1
+    AC_DEFINE(MOZ_GLUE_IN_PROGRAM)
+    ;;
+  esac
+fi
 
 if test "$MOZ_MEMORY"; then
   dnl The generic feature tests that determine how to compute ncpus are long and
   dnl complicated.  Therefore, simply define special cpp variables for the
   dnl platforms we have special knowledge of.
   case "${target}" in
   *-mingw*)
     export MOZ_NO_DEBUG_RTL=1
diff --git a/mfbt/Types.h b/mfbt/Types.h
--- a/mfbt/Types.h
+++ b/mfbt/Types.h
@@ -73,33 +73,38 @@
 #  define MOZ_IMPORT_DATA  MOZ_EXPORT
 #endif
 
 /*
  * Consistent with the above comment, the MFBT_API and MFBT_DATA macros expose
  * export mfbt declarations when building mfbt, and they expose import mfbt
  * declarations when using mfbt.
  */
-#if defined(IMPL_MFBT)
+#if defined(IMPL_MFBT) || (defined(JS_STANDALONE) && !defined(MOZ_MEMORY) && (defined(EXPORT_JS_API) || defined(STATIC_EXPORTABLE_JS_API)))
 #  define MFBT_API     MOZ_EXPORT
 #  define MFBT_DATA    MOZ_EXPORT
 #else
-  /*
-   * On linux mozglue is linked in the program and we link libxul.so with
-   * -z,defs. Normally that causes the linker to reject undefined references in
-   * libxul.so, but as a loophole it allows undefined references to weak
-   * symbols. We add the weak attribute to the import version of the MFBT API
-   * macros to exploit this.
-   */
-#  if defined(MOZ_GLUE_IN_PROGRAM)
-#    define MFBT_API   __attribute__((weak)) MOZ_IMPORT_API
-#    define MFBT_DATA  __attribute__((weak)) MOZ_IMPORT_DATA
+#  if defined(JS_STANDALONE) && !defined(MOZ_MEMORY) && defined(STATIC_JS_API)
+#    define MFBT_API
+#    define MFBT_DATA
 #  else
-#    define MFBT_API   MOZ_IMPORT_API
-#    define MFBT_DATA  MOZ_IMPORT_DATA
+    /*
+     * On linux mozglue is linked in the program and we link libxul.so with
+     * -z,defs. Normally that causes the linker to reject undefined references in
+     * libxul.so, but as a loophole it allows undefined references to weak
+     * symbols. We add the weak attribute to the import version of the MFBT API
+     * macros to exploit this.
+     */
+#    if defined(MOZ_GLUE_IN_PROGRAM)
+#      define MFBT_API   __attribute__((weak)) MOZ_IMPORT_API
+#      define MFBT_DATA  __attribute__((weak)) MOZ_IMPORT_DATA
+#    else
+#      define MFBT_API   MOZ_IMPORT_API
+#      define MFBT_DATA  MOZ_IMPORT_DATA
+#    endif
 #  endif
 #endif
 
 /*
  * C symbols in C++ code must be declared immediately within |extern "C"|
  * blocks.  However, in C code, they need not be declared specially.  This
  * difference is abstracted behind the MOZ_BEGIN_EXTERN_C and MOZ_END_EXTERN_C
  * macros, so that the user need not know whether he is being used in C or C++
