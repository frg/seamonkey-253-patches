# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1519772283 28800
# Node ID 031309abee2cf74705007a7635dc07f1b5816919
# Parent  a64b8e7072571d2d235a47d680588c2d12dc22f8
Bug 1442363, part 1 - Constify RegisterXPTHeader(). r=njn

Once the XPT data is statically allocated, all of the pointers in
xpt_struct.h have to be made const, as well as the XPTHeader
itself. The existing consumers mostly assume things are const already,
so the bulk of the work is tweaking the deserialization code in
xpt_struct.cpp so that the final result is const. I've broken up these
changes into a set of patches.

This patch also gets rid of xptiTypelibGuts::GetHeader(), which is
never called.

MozReview-Commit-ID: FJpmNjY87SN

diff --git a/xpcom/reflect/xptinfo/XPTInterfaceInfoManager.h b/xpcom/reflect/xptinfo/XPTInterfaceInfoManager.h
--- a/xpcom/reflect/xptinfo/XPTInterfaceInfoManager.h
+++ b/xpcom/reflect/xptinfo/XPTInterfaceInfoManager.h
@@ -52,17 +52,17 @@ public:
     size_t SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf);
 
 private:
     XPTInterfaceInfoManager();
     ~XPTInterfaceInfoManager();
 
     void InitMemoryReporter();
 
-    void RegisterXPTHeader(XPTHeader* aHeader);
+    void RegisterXPTHeader(const XPTHeader* aHeader);
 
     // idx is the index of this interface in the XPTHeader
     void VerifyAndAddEntryIfNew(XPTInterfaceDirectoryEntry* iface,
                                 uint16_t idx,
                                 xptiTypelibGuts* typelib);
 
 private:
 
diff --git a/xpcom/reflect/xptinfo/xptiInterfaceInfoManager.cpp b/xpcom/reflect/xptinfo/xptiInterfaceInfoManager.cpp
--- a/xpcom/reflect/xptinfo/xptiInterfaceInfoManager.cpp
+++ b/xpcom/reflect/xptinfo/xptiInterfaceInfoManager.cpp
@@ -111,17 +111,17 @@ XPTInterfaceInfoManager::RegisterBuffer(
 
     XPTHeader *header = nullptr;
     if (XPT_DoHeader(gXPTIStructArena, cursor, &header)) {
         RegisterXPTHeader(header);
     }
 }
 
 void
-XPTInterfaceInfoManager::RegisterXPTHeader(XPTHeader* aHeader)
+XPTInterfaceInfoManager::RegisterXPTHeader(const XPTHeader* aHeader)
 {
     if (aHeader->major_version >= XPT_MAJOR_INCOMPATIBLE_VERSION) {
         NS_ASSERTION(!aHeader->num_interfaces,"bad libxpt");
         LOG_AUTOREG(("      file is version %d.%d  Type file of version %d.0 or higher can not be read.\n", (int)header->major_version, (int)header->minor_version, (int)XPT_MAJOR_INCOMPATIBLE_VERSION));
     }
 
     xptiTypelibGuts* typelib = xptiTypelibGuts::Create(aHeader);
 
diff --git a/xpcom/reflect/xptinfo/xptiTypelibGuts.cpp b/xpcom/reflect/xptinfo/xptiTypelibGuts.cpp
--- a/xpcom/reflect/xptinfo/xptiTypelibGuts.cpp
+++ b/xpcom/reflect/xptinfo/xptiTypelibGuts.cpp
@@ -15,17 +15,17 @@ using namespace mozilla;
 template <class T>
 class MOZ_NEEDS_NO_VTABLE_TYPE CheckNoVTable
 {
 };
 CheckNoVTable<xptiTypelibGuts> gChecker;
 
 // static
 xptiTypelibGuts*
-xptiTypelibGuts::Create(XPTHeader* aHeader)
+xptiTypelibGuts::Create(const XPTHeader* aHeader)
 {
     NS_ASSERTION(aHeader, "bad param");
     size_t n = sizeof(xptiTypelibGuts) +
                sizeof(xptiInterfaceEntry*) * (aHeader->num_interfaces - 1);
     void* place = XPT_CALLOC8(gXPTIStructArena, n);
     if (!place)
         return nullptr;
     return new(place) xptiTypelibGuts(aHeader);
diff --git a/xpcom/reflect/xptinfo/xptiprivate.h b/xpcom/reflect/xptinfo/xptiprivate.h
--- a/xpcom/reflect/xptinfo/xptiprivate.h
+++ b/xpcom/reflect/xptinfo/xptiprivate.h
@@ -86,39 +86,38 @@ extern XPTArena* gXPTIStructArena;
 
 // No virtuals.
 // These are always constructed in the struct arena using placement new.
 // dtor need not be called.
 
 class xptiTypelibGuts
 {
 public:
-    static xptiTypelibGuts* Create(XPTHeader* aHeader);
+    static xptiTypelibGuts* Create(const XPTHeader* aHeader);
 
-    XPTHeader*          GetHeader()           {return mHeader;}
     uint16_t            GetEntryCount() const {return mHeader->num_interfaces;}
 
     void                SetEntryAt(uint16_t i, xptiInterfaceEntry* ptr)
     {
         NS_ASSERTION(mHeader,"bad state!");
         NS_ASSERTION(i < GetEntryCount(),"bad param!");
         mEntryArray[i] = ptr;
     }
 
     xptiInterfaceEntry* GetEntryAt(uint16_t i);
     const char* GetEntryNameAt(uint16_t i);
 
 private:
-    explicit xptiTypelibGuts(XPTHeader* aHeader)
+    explicit xptiTypelibGuts(const XPTHeader* aHeader)
         : mHeader(aHeader)
     { }
     ~xptiTypelibGuts();
 
 private:
-    XPTHeader*           mHeader;        // hold pointer into arena
+    const XPTHeader*     mHeader;        // hold pointer into arena
     xptiInterfaceEntry*  mEntryArray[1]; // Always last. Sized to fit.
 };
 
 /***************************************************************************/
 
 /***************************************************************************/
 
 // This class exists to help xptiInterfaceInfo store a 4-state (2 bit) value
