# HG changeset patch
# User Chris Pearce <cpearce@mozilla.com>
# Date 1507128950 -7200
# Node ID 34e8c0b24dda0c3e025c1c8021c001d236e8a466
# Parent  d626833c3f4c216fd37199a9f7737e1a7c3efd37
Bug 1405697 - Move SEEK_VS_READ_THRESHOLD to where it's used. r=jwwang

It's only used in MediaCache.cpp, so it may as well be defined there.

MozReview-Commit-ID: HcA499xFOUg

diff --git a/dom/media/MediaCache.cpp b/dom/media/MediaCache.cpp
--- a/dom/media/MediaCache.cpp
+++ b/dom/media/MediaCache.cpp
@@ -31,16 +31,24 @@
 namespace mozilla {
 
 #undef LOG
 #undef LOGI
 LazyLogModule gMediaCacheLog("MediaCache");
 #define LOG(...) MOZ_LOG(gMediaCacheLog, LogLevel::Debug, (__VA_ARGS__))
 #define LOGI(...) MOZ_LOG(gMediaCacheLog, LogLevel::Info, (__VA_ARGS__))
 
+// For HTTP seeking, if number of bytes needing to be
+// seeked forward is less than this value then a read is
+// done rather than a byte range request.
+//
+// If we assume a 100Mbit connection, and assume reissuing an HTTP seek causes
+// a delay of 200ms, then in that 200ms we could have simply read ahead 2MB. So
+// setting SEEK_VS_READ_THRESHOLD to 1MB sounds reasonable.
+static const int64_t SEEK_VS_READ_THRESHOLD = 1 * 1024 * 1024;
 
 // Readahead blocks for non-seekable streams will be limited to this
 // fraction of the cache space. We don't normally evict such blocks
 // because replacing them requires a seek, but we need to make sure
 // they don't monopolize the cache.
 static const double NONSEEKABLE_READAHEAD_MAX = 0.5;
 
 // Data N seconds before the current playback position is given the same priority
diff --git a/dom/media/MediaResource.h b/dom/media/MediaResource.h
--- a/dom/media/MediaResource.h
+++ b/dom/media/MediaResource.h
@@ -22,25 +22,16 @@
 #include "MediaResourceCallback.h"
 #include "mozilla/Atomics.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/TimeStamp.h"
 #include "mozilla/UniquePtr.h"
 #include "nsThreadUtils.h"
 #include <algorithm>
 
-// For HTTP seeking, if number of bytes needing to be
-// seeked forward is less than this value then a read is
-// done rather than a byte range request.
-//
-// If we assume a 100Mbit connection, and assume reissuing an HTTP seek causes
-// a delay of 200ms, then in that 200ms we could have simply read ahead 2MB. So
-// setting SEEK_VS_READ_THRESHOLD to 1MB sounds reasonable.
-static const int64_t SEEK_VS_READ_THRESHOLD = 1 * 1024 * 1024;
-
 class nsIHttpChannel;
 class nsIPrincipal;
 
 namespace mozilla {
 
 // Represents a section of contiguous media, with a start and end offset.
 // Used to denote ranges of data which are cached.
 

