# HG changeset patch
# User Seinlin <seinlin.maung@gmail.com>
# Date 1510760672 -28800
# Node ID cd86cbfcd98fdd39a38eccf1300a39a7b8fa03c7
# Parent  547486ffad95e85faa961e301cf6e0e7ad9aee1e
Bug 1274518 - Allow to use preprocessor in IPDL files. r=mshal

MozReview-Commit-ID: KfALhgTzrJT

diff --git a/python/mozbuild/mozbuild/backend/common.py b/python/mozbuild/mozbuild/backend/common.py
--- a/python/mozbuild/mozbuild/backend/common.py
+++ b/python/mozbuild/mozbuild/backend/common.py
@@ -26,16 +26,17 @@ from mozbuild.frontend.data import (
     ExampleWebIDLInterface,
     Exports,
     IPDLFile,
     FinalTargetPreprocessedFiles,
     FinalTargetFiles,
     GeneratedEventWebIDLFile,
     GeneratedSources,
     GeneratedWebIDLFile,
+    PreprocessedIPDLFile,
     GnProjectData,
     PreprocessedTestWebIDLFile,
     PreprocessedWebIDLFile,
     SharedLibrary,
     TestWebIDLFile,
     UnifiedSources,
     XPIDLFile,
     WebIDLFile,
@@ -168,26 +169,41 @@ class WebIDLCollection(object):
 
 class BinariesCollection(object):
     """Tracks state of binaries produced by the build."""
 
     def __init__(self):
         self.shared_libraries = []
         self.programs = []
 
+class IPDLCollection(object):
+    """Collects IPDL files during the build."""
+
+    def __init__(self):
+        self.sources = set()
+        self.preprocessed_sources = set()
+
+    def all_sources(self):
+        return self.sources | self.preprocessed_sources
+
+    def all_regular_sources(self):
+        return self.sources
+
+    def all_preprocessed_sources(self):
+        return self.preprocessed_sources
 
 class CommonBackend(BuildBackend):
     """Holds logic common to all build backends."""
 
     def _init(self):
         self._idl_manager = XPIDLManager(self.environment)
         self._webidls = WebIDLCollection()
         self._binaries = BinariesCollection()
         self._configs = set()
-        self._ipdl_sources = set()
+        self._ipdls = IPDLCollection()
         self._generated_sources = set()
 
     def consume_object(self, obj):
         self._configs.add(obj.config)
 
         if isinstance(obj, XPIDLFile):
             # TODO bug 1240134 tracks not processing XPIDL files during
             # artifact builds.
@@ -237,16 +253,23 @@ class CommonBackend(BuildBackend):
         elif isinstance(obj, GeneratedWebIDLFile):
             # WebIDL isn't relevant to artifact builds.
             if self.environment.is_artifact_build:
                 return True
 
             self._webidls.generated_sources.add(mozpath.join(obj.srcdir,
                 obj.basename))
 
+        elif isinstance(obj, PreprocessedIPDLFile):
+            if self.environment.is_artifact_build:
+                return True
+
+            self._ipdls.preprocessed_sources.add(mozpath.join(
+                obj.srcdir, obj.basename))
+
         elif isinstance(obj, PreprocessedWebIDLFile):
             # WebIDL isn't relevant to artifact builds.
             if self.environment.is_artifact_build:
                 return True
 
             self._webidls.preprocessed_sources.add(mozpath.join(
                 obj.srcdir, obj.basename))
 
@@ -257,17 +280,17 @@ class CommonBackend(BuildBackend):
 
             self._webidls.example_interfaces.add(obj.name)
 
         elif isinstance(obj, IPDLFile):
             # IPDL isn't relevant to artifact builds.
             if self.environment.is_artifact_build:
                 return True
 
-            self._ipdl_sources.add(mozpath.join(obj.srcdir, obj.basename))
+            self._ipdls.sources.add(mozpath.join(obj.srcdir, obj.basename))
 
         elif isinstance(obj, UnifiedSources):
             # Unified sources aren't relevant to artifact builds.
             if self.environment.is_artifact_build:
                 return True
 
             if obj.have_unified_mapping:
                 self._write_unified_files(obj.unified_source_mapping, obj.objdir)
@@ -304,17 +327,19 @@ class CommonBackend(BuildBackend):
 
     def consume_finished(self):
         if len(self._idl_manager.idls):
             self._handle_idl_manager(self._idl_manager)
             self._handle_generated_sources(mozpath.join(self.environment.topobjdir, 'dist/include/%s.h' % idl['root']) for idl in self._idl_manager.idls.values())
 
         self._handle_webidl_collection(self._webidls)
 
-        sorted_ipdl_sources = list(sorted(self._ipdl_sources))
+        sorted_ipdl_sources = list(sorted(self._ipdls.all_sources()))
+        sorted_nonstatic_ipdl_sources = list(sorted(self._ipdls.all_preprocessed_sources()))
+        sorted_static_ipdl_sources = list(sorted(self._ipdls.all_regular_sources()))
 
         def files_from(ipdl):
             base = mozpath.basename(ipdl)
             root, ext = mozpath.splitext(base)
 
             # Both .ipdl and .ipdlh become .cpp files
             files = ['%s.cpp' % root]
             if ext == '.ipdl':
@@ -328,17 +353,18 @@ class CommonBackend(BuildBackend):
         ipdl_cppsrcs = list(itertools.chain(*[files_from(p) for p in sorted_ipdl_sources]))
         self._handle_generated_sources(mozpath.join(ipdl_dir, f) for f in ipdl_cppsrcs)
         unified_source_mapping = list(group_unified_files(ipdl_cppsrcs,
                                                           unified_prefix='UnifiedProtocols',
                                                           unified_suffix='cpp',
                                                           files_per_unified_file=16))
 
         self._write_unified_files(unified_source_mapping, ipdl_dir, poison_windows_h=False)
-        self._handle_ipdl_sources(ipdl_dir, sorted_ipdl_sources, unified_source_mapping)
+        self._handle_ipdl_sources(ipdl_dir, sorted_ipdl_sources, sorted_nonstatic_ipdl_sources,
+                                  sorted_static_ipdl_sources, unified_source_mapping)
 
         for config in self._configs:
             self.backend_input_files.add(config.source)
 
         # Write out a machine-readable file describing binaries.
         topobjdir = self.environment.topobjdir
         with self._write_file(mozpath.join(topobjdir, 'binaries.json')) as fh:
             d = {
diff --git a/python/mozbuild/mozbuild/backend/recursivemake.py b/python/mozbuild/mozbuild/backend/recursivemake.py
--- a/python/mozbuild/mozbuild/backend/recursivemake.py
+++ b/python/mozbuild/mozbuild/backend/recursivemake.py
@@ -1622,28 +1622,42 @@ class RecursiveMakeBackend(CommonBackend
 
         self._makefile_out_count += 1
 
     def _handle_linked_rust_crates(self, obj, extern_crate_file):
         backend_file = self._get_backend_file_for(obj)
 
         backend_file.write('RS_STATICLIB_CRATE_SRC := %s\n' % extern_crate_file)
 
-    def _handle_ipdl_sources(self, ipdl_dir, sorted_ipdl_sources,
-                             unified_ipdl_cppsrcs_mapping):
+    def _handle_ipdl_sources(self, ipdl_dir, sorted_ipdl_sources, sorted_nonstatic_ipdl_sources,
+                             sorted_static_ipdl_sources, unified_ipdl_cppsrcs_mapping):
         # Write out a master list of all IPDL source files.
         mk = Makefile()
 
-        mk.add_statement('ALL_IPDLSRCS := %s' % ' '.join(sorted_ipdl_sources))
+        sorted_nonstatic_ipdl_basenames = list()
+        for source in sorted_nonstatic_ipdl_sources:
+            basename = os.path.basename(source)
+            sorted_nonstatic_ipdl_basenames.append(basename)
+            rule = mk.create_rule([basename])
+            rule.add_dependencies([source])
+            rule.add_commands([
+                '$(RM) $@',
+                '$(call py_action,preprocessor,$(DEFINES) $(ACDEFINES) '
+                    '$< -o $@)'
+            ])
+
+        mk.add_statement('ALL_IPDLSRCS := %s %s' % (' '.join(sorted_nonstatic_ipdl_basenames),
+                         ' '.join(sorted_static_ipdl_sources)))
 
         self._add_unified_build_rules(mk, unified_ipdl_cppsrcs_mapping,
                                       unified_files_makefile_variable='CPPSRCS')
 
-        mk.add_statement('IPDLDIRS := %s' % ' '.join(sorted(set(mozpath.dirname(p)
-            for p in self._ipdl_sources))))
+        # Preprocessed ipdl files are generated in ipdl_dir.
+        mk.add_statement('IPDLDIRS := %s %s' % (ipdl_dir, ' '.join(sorted(set(mozpath.dirname(p)
+            for p in sorted_static_ipdl_sources)))))
 
         with self._write_file(mozpath.join(ipdl_dir, 'ipdlsrcs.mk')) as ipdls:
             mk.dump(ipdls, removal_guard=False)
 
     def _handle_webidl_build(self, bindings_dir, unified_source_mapping,
                              webidls, expected_build_output_files,
                              global_define_files):
         include_dir = mozpath.join(self.environment.topobjdir, 'dist',
diff --git a/python/mozbuild/mozbuild/compilation/database.py b/python/mozbuild/mozbuild/compilation/database.py
--- a/python/mozbuild/mozbuild/compilation/database.py
+++ b/python/mozbuild/mozbuild/compilation/database.py
@@ -136,18 +136,18 @@ class CompileDBBackend(CommonBackend):
                                 obj.canonical_suffix)
             for entry in f[1]:
                 self._build_db_line(obj.objdir, obj.relativedir, obj.config,
                                     entry, obj.canonical_suffix, unified=f[0])
 
     def _handle_idl_manager(self, idl_manager):
         pass
 
-    def _handle_ipdl_sources(self, ipdl_dir, sorted_ipdl_sources,
-                             unified_ipdl_cppsrcs_mapping):
+    def _handle_ipdl_sources(self, ipdl_dir, sorted_ipdl_sources, sorted_nonstatic_ipdl_sources,
+                             sorted_static_ipdl_sources, unified_ipdl_cppsrcs_mapping):
         for f in unified_ipdl_cppsrcs_mapping:
             self._build_db_line(ipdl_dir, None, self.environment, f[0],
                                 '.cpp')
 
     def _handle_webidl_build(self, bindings_dir, unified_source_mapping,
                              webidls, expected_build_output_files,
                              global_define_files):
         for f in unified_source_mapping:
diff --git a/python/mozbuild/mozbuild/frontend/context.py b/python/mozbuild/mozbuild/frontend/context.py
--- a/python/mozbuild/mozbuild/frontend/context.py
+++ b/python/mozbuild/mozbuild/frontend/context.py
@@ -1822,16 +1822,23 @@ VARIABLES = {
     'XPIDL_NO_MANIFEST': (bool, bool,
         """Indicate that the XPIDL module should not be added to a manifest.
 
         This flag exists primarily to prevent test-only XPIDL modules from being
         added to the application's chrome manifest. Most XPIDL modules should
         not use this flag.
         """),
 
+    'PREPROCESSED_IPDL_SOURCES': (StrictOrderingOnAppendList, list,
+        """Preprocessed IPDL source files.
+
+        These files will be preprocessed, then parsed and converted to
+        ``.cpp`` files.
+        """),
+
     'IPDL_SOURCES': (StrictOrderingOnAppendList, list,
         """IPDL source files.
 
         These are ``.ipdl`` files that will be parsed and converted to
         ``.cpp`` files.
         """),
 
     'WEBIDL_FILES': (StrictOrderingOnAppendList, list,
diff --git a/python/mozbuild/mozbuild/frontend/data.py b/python/mozbuild/mozbuild/frontend/data.py
--- a/python/mozbuild/mozbuild/frontend/data.py
+++ b/python/mozbuild/mozbuild/frontend/data.py
@@ -239,16 +239,28 @@ class IPDLFile(ContextDerived):
         'basename',
     )
 
     def __init__(self, context, path):
         ContextDerived.__init__(self, context)
 
         self.basename = path
 
+class PreprocessedIPDLFile(ContextDerived):
+    """Describes an individual .ipdl source file that requires preprocessing."""
+
+    __slots__ = (
+        'basename',
+    )
+
+    def __init__(self, context, path):
+        ContextDerived.__init__(self, context)
+
+        self.basename = path
+
 class WebIDLFile(ContextDerived):
     """Describes an individual .webidl source file."""
 
     __slots__ = (
         'basename',
     )
 
     def __init__(self, context, path):
diff --git a/python/mozbuild/mozbuild/frontend/emitter.py b/python/mozbuild/mozbuild/frontend/emitter.py
--- a/python/mozbuild/mozbuild/frontend/emitter.py
+++ b/python/mozbuild/mozbuild/frontend/emitter.py
@@ -57,16 +57,17 @@ from .data import (
     Library,
     Linkable,
     LocalInclude,
     LocalizedFiles,
     LocalizedPreprocessedFiles,
     ObjdirFiles,
     ObjdirPreprocessedFiles,
     PerSourceFlag,
+    PreprocessedIPDLFile,
     PreprocessedTestWebIDLFile,
     PreprocessedWebIDLFile,
     Program,
     RustLibrary,
     HostRustLibrary,
     RustProgram,
     RustTest,
     SharedLibrary,
@@ -1070,16 +1071,17 @@ class TreeMetadataEmitter(LoggingMixin):
             if defines_from_obj:
                 for flags in backend_flags:
                     flags.resolve_flags(defines_var, defines_from_obj)
 
         simple_lists = [
             ('GENERATED_EVENTS_WEBIDL_FILES', GeneratedEventWebIDLFile),
             ('GENERATED_WEBIDL_FILES', GeneratedWebIDLFile),
             ('IPDL_SOURCES', IPDLFile),
+            ('PREPROCESSED_IPDL_SOURCES', PreprocessedIPDLFile),
             ('PREPROCESSED_TEST_WEBIDL_FILES', PreprocessedTestWebIDLFile),
             ('PREPROCESSED_WEBIDL_FILES', PreprocessedWebIDLFile),
             ('TEST_WEBIDL_FILES', TestWebIDLFile),
             ('WEBIDL_FILES', WebIDLFile),
             ('WEBIDL_EXAMPLE_INTERFACES', ExampleWebIDLInterface),
         ]
         for context_var, klass in simple_lists:
             for name in context.get(context_var, []):
diff --git a/python/mozbuild/mozbuild/test/backend/data/ipdl_sources/bar/moz.build b/python/mozbuild/mozbuild/test/backend/data/ipdl_sources/bar/moz.build
--- a/python/mozbuild/mozbuild/test/backend/data/ipdl_sources/bar/moz.build
+++ b/python/mozbuild/mozbuild/test/backend/data/ipdl_sources/bar/moz.build
@@ -1,10 +1,14 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+PREPROCESSED_IPDL_SOURCES += [
+    'bar1.ipdl',
+]
+
 IPDL_SOURCES += [
     'bar.ipdl',
     'bar2.ipdlh',
 ]
diff --git a/python/mozbuild/mozbuild/test/backend/data/ipdl_sources/foo/moz.build b/python/mozbuild/mozbuild/test/backend/data/ipdl_sources/foo/moz.build
--- a/python/mozbuild/mozbuild/test/backend/data/ipdl_sources/foo/moz.build
+++ b/python/mozbuild/mozbuild/test/backend/data/ipdl_sources/foo/moz.build
@@ -1,10 +1,14 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+PREPROCESSED_IPDL_SOURCES += [
+    'foo1.ipdl',
+]
+
 IPDL_SOURCES += [
     'foo.ipdl',
     'foo2.ipdlh',
 ]
diff --git a/python/mozbuild/mozbuild/test/backend/test_recursivemake.py b/python/mozbuild/mozbuild/test/backend/test_recursivemake.py
--- a/python/mozbuild/mozbuild/test/backend/test_recursivemake.py
+++ b/python/mozbuild/mozbuild/test/backend/test_recursivemake.py
@@ -651,30 +651,30 @@ class TestRecursiveMakeBackend(BackendTe
         for e in expected:
             full = mozpath.join(man_dir, e)
             self.assertTrue(os.path.exists(full))
 
             m2 = InstallManifest(path=full)
             self.assertEqual(m, m2)
 
     def test_ipdl_sources(self):
-        """Test that IPDL_SOURCES are written to ipdlsrcs.mk correctly."""
+        """Test that PREPROCESSED_IPDL_SOURCES and IPDL_SOURCES are written to ipdlsrcs.mk correctly."""
         env = self._consume('ipdl_sources', RecursiveMakeBackend)
 
         manifest_path = mozpath.join(env.topobjdir,
             'ipc', 'ipdl', 'ipdlsrcs.mk')
         lines = [l.strip() for l in open(manifest_path, 'rt').readlines()]
 
         # Handle Windows paths correctly
         topsrcdir = env.topsrcdir.replace(os.sep, '/')
 
         expected = [
-            "ALL_IPDLSRCS := %s/bar/bar.ipdl %s/bar/bar2.ipdlh %s/foo/foo.ipdl %s/foo/foo2.ipdlh" % tuple([topsrcdir] * 4),
+            "ALL_IPDLSRCS := bar1.ipdl foo1.ipdl %s/bar/bar.ipdl %s/bar/bar2.ipdlh %s/foo/foo.ipdl %s/foo/foo2.ipdlh" % tuple([topsrcdir] * 4),
             "CPPSRCS := UnifiedProtocols0.cpp",
-            "IPDLDIRS := %s/bar %s/foo" % (topsrcdir, topsrcdir),
+            "IPDLDIRS := %s/ipc/ipdl %s/bar %s/foo" % (env.topobjdir, topsrcdir, topsrcdir),
         ]
 
         found = [str for str in lines if str.startswith(('ALL_IPDLSRCS',
                                                          'CPPSRCS',
                                                          'IPDLDIRS'))]
         self.assertEqual(found, expected)
 
     def test_defines(self):
diff --git a/python/mozbuild/mozbuild/test/frontend/data/ipdl_sources/bar/moz.build b/python/mozbuild/mozbuild/test/frontend/data/ipdl_sources/bar/moz.build
--- a/python/mozbuild/mozbuild/test/frontend/data/ipdl_sources/bar/moz.build
+++ b/python/mozbuild/mozbuild/test/frontend/data/ipdl_sources/bar/moz.build
@@ -1,10 +1,14 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+PREPROCESSED_IPDL_SOURCES += [
+    'bar1.ipdl',
+]
+
 IPDL_SOURCES += [
     'bar.ipdl',
     'bar2.ipdlh',
 ]
diff --git a/python/mozbuild/mozbuild/test/frontend/data/ipdl_sources/foo/moz.build b/python/mozbuild/mozbuild/test/frontend/data/ipdl_sources/foo/moz.build
--- a/python/mozbuild/mozbuild/test/frontend/data/ipdl_sources/foo/moz.build
+++ b/python/mozbuild/mozbuild/test/frontend/data/ipdl_sources/foo/moz.build
@@ -1,10 +1,14 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+PREPROCESSED_IPDL_SOURCES += [
+    'foo1.ipdl',
+]
+
 IPDL_SOURCES += [
     'foo.ipdl',
     'foo2.ipdlh',
 ]
diff --git a/python/mozbuild/mozbuild/test/frontend/test_emitter.py b/python/mozbuild/mozbuild/test/frontend/test_emitter.py
--- a/python/mozbuild/mozbuild/test/frontend/test_emitter.py
+++ b/python/mozbuild/mozbuild/test/frontend/test_emitter.py
@@ -29,16 +29,17 @@ from mozbuild.frontend.data import (
     HostRustProgram,
     HostSources,
     IPDLFile,
     JARManifest,
     LinkageMultipleRustLibrariesError,
     LocalInclude,
     LocalizedFiles,
     LocalizedPreprocessedFiles,
+    PreprocessedIPDLFile,
     Program,
     RustLibrary,
     RustProgram,
     SharedLibrary,
     SimpleProgram,
     Sources,
     StaticLibrary,
     TestHarnessFiles,
@@ -922,29 +923,39 @@ class TestEmitterBasic(unittest.TestCase
             'lists test that does not exist: missing.js'):
             self.read_topsrcdir(reader)
 
     def test_ipdl_sources(self):
         reader = self.reader('ipdl_sources')
         objs = self.read_topsrcdir(reader)
 
         ipdls = []
+        nonstatic_ipdls = []
         for o in objs:
             if isinstance(o, IPDLFile):
                 ipdls.append('%s/%s' % (o.relativedir, o.basename))
+            elif isinstance(o, PreprocessedIPDLFile):
+                nonstatic_ipdls.append('%s/%s' % (o.relativedir, o.basename))
 
         expected = [
             'bar/bar.ipdl',
             'bar/bar2.ipdlh',
             'foo/foo.ipdl',
             'foo/foo2.ipdlh',
         ]
 
         self.assertEqual(ipdls, expected)
 
+        expected = [
+            'bar/bar1.ipdl',
+            'foo/foo1.ipdl',
+        ]
+
+        self.assertEqual(nonstatic_ipdls, expected)
+
     def test_local_includes(self):
         """Test that LOCAL_INCLUDES is emitted correctly."""
         reader = self.reader('local_includes')
         objs = self.read_topsrcdir(reader)
 
         local_includes = [o.path for o in objs if isinstance(o, LocalInclude)]
         expected = [
             '/bar/baz',
