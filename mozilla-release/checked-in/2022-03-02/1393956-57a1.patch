# HG changeset patch
# User Lee Salzman <lsalzman@mozilla.com>
# Date 1504639813 14400
# Node ID 19a399059c8b9309e54928e8b268638bc4707c44
# Parent  0db60aeea27779fdd188862649bae7f1e8e9ed38
Bug 1393956 - ensure NativeFontResourceDWrite is thread-safe. r=bas

MozReview-Commit-ID: 4uME7zNmXrg

diff --git a/gfx/2d/NativeFontResourceDWrite.cpp b/gfx/2d/NativeFontResourceDWrite.cpp
--- a/gfx/2d/NativeFontResourceDWrite.cpp
+++ b/gfx/2d/NativeFontResourceDWrite.cpp
@@ -6,21 +6,23 @@
 
 #include "NativeFontResourceDWrite.h"
 #include "UnscaledFontDWrite.h"
 
 #include <unordered_map>
 
 #include "Logging.h"
 #include "mozilla/RefPtr.h"
+#include "mozilla/StaticMutex.h"
 
 namespace mozilla {
 namespace gfx {
 
-static Atomic<uint64_t> sNextFontFileKey;
+static StaticMutex sFontFileStreamsMutex;
+static uint64_t sNextFontFileKey = 0;
 static std::unordered_map<uint64_t, IDWriteFontFileStream*> sFontFileStreams;
 
 class DWriteFontFileLoader : public IDWriteFontFileLoader
 {
 public:
   DWriteFontFileLoader()
   {
   }
@@ -104,59 +106,58 @@ public:
       return S_OK;
     } else {
       return E_NOINTERFACE;
     }
   }
 
   IFACEMETHOD_(ULONG, AddRef)()
   {
-    ++mRefCnt;
-    return mRefCnt;
+    return ++mRefCnt;
   }
 
   IFACEMETHOD_(ULONG, Release)()
   {
-    --mRefCnt;
-    if (mRefCnt == 0) {
+    uint32_t count = --mRefCnt;
+    if (count == 0) {
       delete this;
-      return 0;
     }
-    return mRefCnt;
+    return count;
   }
 
   // IDWriteFontFileStream methods
   virtual HRESULT STDMETHODCALLTYPE ReadFileFragment(void const** fragmentStart,
                                                      UINT64 fileOffset,
                                                      UINT64 fragmentSize,
                                                      OUT void** fragmentContext);
 
   virtual void STDMETHODCALLTYPE ReleaseFileFragment(void* fragmentContext);
 
   virtual HRESULT STDMETHODCALLTYPE GetFileSize(OUT UINT64* fileSize);
 
   virtual HRESULT STDMETHODCALLTYPE GetLastWriteTime(OUT UINT64* lastWriteTime);
 
 private:
   std::vector<uint8_t> mData;
-  uint32_t mRefCnt;
+  Atomic<uint32_t> mRefCnt;
   uint64_t mFontFileKey;
 };
 
 IDWriteFontFileLoader* DWriteFontFileLoader::mInstance = nullptr;
 
 HRESULT STDMETHODCALLTYPE
 DWriteFontFileLoader::CreateStreamFromKey(const void *fontFileReferenceKey,
                                           UINT32 fontFileReferenceKeySize,
                                           IDWriteFontFileStream **fontFileStream)
 {
   if (!fontFileReferenceKey || !fontFileStream) {
     return E_POINTER;
   }
 
+  StaticMutexAutoLock lock(sFontFileStreamsMutex);
   uint64_t fontFileKey = *static_cast<const uint64_t*>(fontFileReferenceKey);
   auto found = sFontFileStreams.find(fontFileKey);
   if (found == sFontFileStreams.end()) {
     *fontFileStream = nullptr;
     return E_FAIL;
   }
 
   found->second->AddRef();
@@ -170,16 +171,17 @@ DWriteFontFileStream::DWriteFontFileStre
   , mFontFileKey(aFontFileKey)
 {
   mData.resize(aSize);
   memcpy(&mData.front(), aData, aSize);
 }
 
 DWriteFontFileStream::~DWriteFontFileStream()
 {
+  StaticMutexAutoLock lock(sFontFileStreamsMutex);
   sFontFileStreams.erase(mFontFileKey);
 }
 
 HRESULT STDMETHODCALLTYPE
 DWriteFontFileStream::GetFileSize(UINT64 *fileSize)
 {
   *fileSize = mData.size();
   return S_OK;
@@ -222,20 +224,22 @@ NativeFontResourceDWrite::Create(uint8_t
                                  bool aNeedsCairo)
 {
   RefPtr<IDWriteFactory> factory = Factory::GetDWriteFactory();
   if (!factory) {
     gfxWarning() << "Failed to get DWrite Factory.";
     return nullptr;
   }
 
+  sFontFileStreamsMutex.Lock();
   uint64_t fontFileKey = sNextFontFileKey++;
   RefPtr<IDWriteFontFileStream> ffsRef =
     new DWriteFontFileStream(aFontData, aDataLength, fontFileKey);
   sFontFileStreams[fontFileKey] = ffsRef;
+  sFontFileStreamsMutex.Unlock();
 
   RefPtr<IDWriteFontFile> fontFile;
   HRESULT hr =
     factory->CreateCustomFontFileReference(&fontFileKey, sizeof(fontFileKey),
                                            DWriteFontFileLoader::Instance(),
                                            getter_AddRefs(fontFile));
   if (FAILED(hr)) {
     gfxWarning() << "Failed to load font file from data!";
