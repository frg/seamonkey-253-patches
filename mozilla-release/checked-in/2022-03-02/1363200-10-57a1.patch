# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1498678313 25200
# Node ID 3792a715a50dcd211d5a72ae3be1bf58a9de8ebf
# Parent  78fcb00d6a7e2932c77326d62db31639044c381f
Bug 1363200 - JSAPI for realms: Change a few XPConnect methods to take Realm arguments instead of JSCompartments. r=mrbkap

This also introduces JS::GetObjectRealmOrNull, which returns an object's realm,
or null if the object is a cross-compartment wrapper. In the new order,
wrappers can't have realms, since they must be shared across all realms in a
compartment. We're introducing this new function early (even though it's
*currently* possible to assign a realm to wrappers) in order to see in
advance if the possibility of returning null will cause problems.
(It looks like it won't.)

diff --git a/caps/nsScriptSecurityManager.cpp b/caps/nsScriptSecurityManager.cpp
--- a/caps/nsScriptSecurityManager.cpp
+++ b/caps/nsScriptSecurityManager.cpp
@@ -1222,30 +1222,31 @@ nsScriptSecurityManager::CanCreateWrappe
 // XXX Special case for nsIXPCException ?
     ClassInfoData objClassInfo = ClassInfoData(aClassInfo, nullptr);
     if (objClassInfo.IsDOMClass())
     {
         return NS_OK;
     }
 
     // We give remote-XUL whitelisted domains a free pass here. See bug 932906.
-    JSCompartment* contextCompartment = js::GetContextCompartment(cx);
-    if (!xpc::AllowContentXBLScope(contextCompartment))
+    JS::Rooted<JS::Realm*> contextRealm(cx, JS::GetCurrentRealmOrNull(cx));
+    MOZ_RELEASE_ASSERT(contextRealm);
+    if (!xpc::AllowContentXBLScope(contextRealm))
     {
         return NS_OK;
     }
 
     if (nsContentUtils::IsCallerChrome())
     {
         return NS_OK;
     }
 
     // We want to expose nsIDOMXULCommandDispatcher and nsITreeSelection implementations
     // in XBL scopes.
-    if (xpc::IsContentXBLCompartment(contextCompartment)) {
+    if (xpc::IsContentXBLScope(contextRealm)) {
       nsCOMPtr<nsIDOMXULCommandDispatcher> dispatcher = do_QueryInterface(aObj);
       if (dispatcher) {
         return NS_OK;
       }
 
       nsCOMPtr<nsITreeSelection> treeSelection = do_QueryInterface(aObj);
       if (treeSelection) {
         return NS_OK;
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -2486,30 +2486,33 @@ nsContentUtils::ThreadsafeIsCallerChrome
   return NS_IsMainThread() ?
     IsCallerChrome() :
     mozilla::dom::workers::IsCurrentThreadRunningChromeWorker();
 }
 
 bool
 nsContentUtils::IsCallerContentXBL()
 {
-    JSContext *cx = GetCurrentJSContext();
-    if (!cx)
-        return false;
-
-    JSCompartment *c = js::GetContextCompartment(cx);
-
-    // For remote XUL, we run XBL in the XUL scope. Given that we care about
-    // compat and not security for remote XUL, just always claim to be XBL.
-    if (!xpc::AllowContentXBLScope(c)) {
-      MOZ_ASSERT(nsContentUtils::AllowXULXBLForPrincipal(xpc::GetCompartmentPrincipal(c)));
-      return true;
-    }
-
-    return xpc::IsContentXBLCompartment(c);
+  JSContext *cx = GetCurrentJSContext();
+  if (!cx)
+    return false;
+
+  JS::Realm* realm = JS::GetCurrentRealmOrNull(cx);
+  if (!realm)
+    return false;
+
+  // For remote XUL, we run XBL in the XUL scope. Given that we care about
+  // compat and not security for remote XUL, just always claim to be XBL.
+  if (!xpc::AllowContentXBLScope(realm)) {
+    DebugOnly<JSCompartment*> c = JS::GetCompartmentForRealm(realm);
+    MOZ_ASSERT(nsContentUtils::AllowXULXBLForPrincipal(xpc::GetCompartmentPrincipal(c)));
+    return true;
+  }
+
+  return xpc::IsContentXBLScope(realm);
 }
 
 bool
 nsContentUtils::IsSystemCaller(JSContext* aCx)
 {
   // Note that SubjectPrincipal() assumes we are in a compartment here.
   return SubjectPrincipal(aCx) == sSystemPrincipal;
 }
diff --git a/dom/base/nsINode.cpp b/dom/base/nsINode.cpp
--- a/dom/base/nsINode.cpp
+++ b/dom/base/nsINode.cpp
@@ -2984,17 +2984,17 @@ nsINode::WrapObject(JSContext *aCx, JS::
       !nsContentUtils::IsSystemCaller(aCx)) {
     Throw(aCx, NS_ERROR_UNEXPECTED);
     return nullptr;
   }
 
   JS::Rooted<JSObject*> obj(aCx, WrapNode(aCx, aGivenProto));
   MOZ_ASSERT_IF(obj && ChromeOnlyAccess(),
                 xpc::IsInContentXBLScope(obj) ||
-                !xpc::UseContentXBLScope(js::GetObjectCompartment(obj)));
+                !xpc::UseContentXBLScope(JS::GetObjectRealmOrNull(obj)));
   return obj;
 }
 
 already_AddRefed<nsINode>
 nsINode::CloneNode(bool aDeep, ErrorResult& aError)
 {
   nsCOMPtr<nsINode> result;
   aError = nsNodeUtils::CloneNodeImpl(this, aDeep, getter_AddRefs(result));
diff --git a/dom/xbl/nsXBLBinding.cpp b/dom/xbl/nsXBLBinding.cpp
--- a/dom/xbl/nsXBLBinding.cpp
+++ b/dom/xbl/nsXBLBinding.cpp
@@ -273,17 +273,17 @@ nsXBLBinding::SetBoundElement(nsIContent
   // Compute whether we're using an XBL scope.
   //
   // We disable XBL scopes for remote XUL, where we care about compat more
   // than security. So we need to know whether we're using an XBL scope so that
   // we can decide what to do about untrusted events when "allowuntrusted"
   // is not given in the handler declaration.
   nsCOMPtr<nsIGlobalObject> go = mBoundElement->OwnerDoc()->GetScopeObject();
   NS_ENSURE_TRUE_VOID(go && go->GetGlobalJSObject());
-  mUsingContentXBLScope = xpc::UseContentXBLScope(js::GetObjectCompartment(go->GetGlobalJSObject()));
+  mUsingContentXBLScope = xpc::UseContentXBLScope(JS::GetObjectRealmOrNull(go->GetGlobalJSObject()));
 }
 
 bool
 nsXBLBinding::HasStyleSheets() const
 {
   // Find out if we need to re-resolve style.  We'll need to do this
   // if we have additional stylesheets in our binding document.
   if (mPrototypeBinding->HasStyleSheets())
diff --git a/js/public/MemoryMetrics.h b/js/public/MemoryMetrics.h
--- a/js/public/MemoryMetrics.h
+++ b/js/public/MemoryMetrics.h
@@ -494,18 +494,18 @@ struct NotableScriptSourceInfo : public 
 
     char* filename_;
 
   private:
     NotableScriptSourceInfo(const NotableScriptSourceInfo& info) = delete;
 };
 
 /**
- * These measurements relate directly to the JSRuntime, and not to zones and
- * compartments within it.
+ * These measurements relate directly to the JSRuntime, and not to zones,
+ * compartments, and realms within it.
  */
 struct RuntimeSizes
 {
 #define FOR_EACH_SIZE(macro) \
     macro(_, MallocHeap, object) \
     macro(_, MallocHeap, atomsTable) \
     macro(_, MallocHeap, atomsMarkBitmaps) \
     macro(_, MallocHeap, contexts) \
diff --git a/js/public/Realm.h b/js/public/Realm.h
--- a/js/public/Realm.h
+++ b/js/public/Realm.h
@@ -49,21 +49,30 @@ GetCurrentRealmOrNull(JSContext* cx);
 inline JSCompartment*
 GetCompartmentForRealm(Realm* realm) {
     // Implementation note: For now, realms are a fiction; we treat realms and
     // compartments as being one-to-one, but they are actually identical.
     return reinterpret_cast<JSCompartment*>(realm);
 }
 
 // Return the realm in a given compartment.
+//
+// Deprecated. There is currently exactly one realm per compartment, but this
+// will change.
 inline Realm*
 GetRealmForCompartment(JSCompartment* compartment) {
     return reinterpret_cast<Realm*>(compartment);
 }
 
+// Return an object's realm. All objects except cross-compartment wrappers are
+// created in a particular realm, which never changes. Returns null if obj is
+// a cross-compartment wrapper.
+extern JS_PUBLIC_API(Realm*)
+GetObjectRealmOrNull(JSObject* obj);
+
 // Get the value of the "private data" internal field of the given Realm.
 // This field is initially null and is set using SetRealmPrivate.
 // It's a pointer to embeddding-specific data that SpiderMonkey never uses.
 extern JS_PUBLIC_API(void*)
 GetRealmPrivate(Realm* realm);
 
 // Set the "private data" internal field of the given Realm.
 extern JS_PUBLIC_API(void)
@@ -84,16 +93,21 @@ SetDestroyRealmCallback(JSContext* cx, D
 typedef void
 (* RealmNameCallback)(JSContext* cx, Handle<Realm*> realm, char* buf, size_t bufsize);
 
 // Set the callback SpiderMonkey calls to get the name of a realm, for
 // diagnostic output.
 extern JS_PUBLIC_API(void)
 SetRealmNameCallback(JSContext* cx, RealmNameCallback callback);
 
+// Get the global object for the given realm. Returns null only if `realm` is
+// in the atoms compartment.
+extern JS_PUBLIC_API(JSObject*)
+GetRealmGlobalOrNull(Handle<Realm*> realm);
+
 extern JS_PUBLIC_API(JSObject*)
 GetRealmObjectPrototype(JSContext* cx);
 
 extern JS_PUBLIC_API(JSObject*)
 GetRealmFunctionPrototype(JSContext* cx);
 
 extern JS_PUBLIC_API(JSObject*)
 GetRealmArrayPrototype(JSContext* cx);
diff --git a/js/src/vm/Realm.cpp b/js/src/vm/Realm.cpp
--- a/js/src/vm/Realm.cpp
+++ b/js/src/vm/Realm.cpp
@@ -29,16 +29,28 @@ gc::TraceRealm(JSTracer* trc, JS::Realm*
 }
 
 JS_PUBLIC_API(bool)
 gc::RealmNeedsSweep(JS::Realm* realm)
 {
     return JS::GetCompartmentForRealm(realm)->globalIsAboutToBeFinalized();
 }
 
+JS_PUBLIC_API(JS::Realm*)
+JS::GetCurrentRealmOrNull(JSContext* cx)
+{
+    return JS::GetRealmForCompartment(cx->compartment());
+}
+
+JS_PUBLIC_API(JS::Realm*)
+JS::GetObjectRealmOrNull(JSObject* obj)
+{
+    return IsCrossCompartmentWrapper(obj) ? nullptr : GetRealmForCompartment(obj->compartment());
+}
+
 JS_PUBLIC_API(void*)
 JS::GetRealmPrivate(JS::Realm* realm)
 {
     return GetCompartmentForRealm(realm)->realmData;
 }
 
 JS_PUBLIC_API(void)
 JS::SetRealmPrivate(JS::Realm* realm, void* data)
@@ -54,16 +66,22 @@ JS::SetDestroyRealmCallback(JSContext* c
 
 JS_PUBLIC_API(void)
 JS::SetRealmNameCallback(JSContext* cx, JS::RealmNameCallback callback)
 {
     cx->runtime()->realmNameCallback = callback;
 }
 
 JS_PUBLIC_API(JSObject*)
+JS::GetRealmGlobalOrNull(Handle<JS::Realm*> realm)
+{
+    return GetCompartmentForRealm(realm)->maybeGlobal();
+}
+
+JS_PUBLIC_API(JSObject*)
 JS::GetRealmObjectPrototype(JSContext* cx)
 {
     CHECK_REQUEST(cx);
     return GlobalObject::getOrCreateObjectPrototype(cx, cx->global());
 }
 
 JS_PUBLIC_API(JSObject*)
 JS::GetRealmFunctionPrototype(JSContext* cx)
diff --git a/js/xpconnect/src/XPCJSRuntime.cpp b/js/xpconnect/src/XPCJSRuntime.cpp
--- a/js/xpconnect/src/XPCJSRuntime.cpp
+++ b/js/xpconnect/src/XPCJSRuntime.cpp
@@ -2205,17 +2205,16 @@ class XPCJSRuntimeStats : public JS::Run
         mGetLocations(getLocations),
         mAnonymizeID(anonymize ? 1 : 0)
     {}
 
     ~XPCJSRuntimeStats() {
         for (size_t i = 0; i != compartmentStatsVector.length(); ++i)
             delete static_cast<xpc::CompartmentStatsExtras*>(compartmentStatsVector[i].extra);
 
-
         for (size_t i = 0; i != zoneStatsVector.length(); ++i)
             delete static_cast<xpc::ZoneStatsExtras*>(zoneStatsVector[i].extra);
     }
 
     virtual void initExtraZoneStats(JS::Zone* zone, JS::ZoneStats* zStats) override {
         // Get the compartment's global.
         AutoSafeJSContext cx;
         JSCompartment* comp = js::GetAnyCompartmentInZone(zone);
diff --git a/js/xpconnect/src/XPCWrappedNativeScope.cpp b/js/xpconnect/src/XPCWrappedNativeScope.cpp
--- a/js/xpconnect/src/XPCWrappedNativeScope.cpp
+++ b/js/xpconnect/src/XPCWrappedNativeScope.cpp
@@ -402,27 +402,29 @@ GetScopeForXBLExecution(JSContext* cx, H
 
     NS_ENSURE_TRUE(scope, nullptr); // See bug 858642.
     scope = js::UncheckedUnwrap(scope);
     JS::ExposeObjectToActiveJS(scope);
     return scope;
 }
 
 bool
-AllowContentXBLScope(JSCompartment* c)
+AllowContentXBLScope(JS::Realm* realm)
 {
-  XPCWrappedNativeScope* scope = CompartmentPrivate::Get(c)->scope;
-  return scope && scope->AllowContentXBLScope();
+    XPCWrappedNativeScope* scope =
+        CompartmentPrivate::Get(JS::GetCompartmentForRealm(realm))->scope;
+    return scope && scope->AllowContentXBLScope();
 }
 
 bool
-UseContentXBLScope(JSCompartment* c)
+UseContentXBLScope(JS::Realm* realm)
 {
-  XPCWrappedNativeScope* scope = CompartmentPrivate::Get(c)->scope;
-  return scope && scope->UseContentXBLScope();
+    XPCWrappedNativeScope* scope =
+        CompartmentPrivate::Get(JS::GetCompartmentForRealm(realm))->scope;
+    return scope && scope->UseContentXBLScope();
 }
 
 void
 ClearContentXBLScope(JSObject* global)
 {
     CompartmentPrivate::Get(global)->scope->ClearContentXBLScope();
 }
 
diff --git a/js/xpconnect/src/nsXPConnect.cpp b/js/xpconnect/src/nsXPConnect.cpp
--- a/js/xpconnect/src/nsXPConnect.cpp
+++ b/js/xpconnect/src/nsXPConnect.cpp
@@ -1364,25 +1364,27 @@ AllowCPOWsInAddon(const nsACString& addo
 
 namespace mozilla {
 namespace dom {
 
 bool
 IsChromeOrXBL(JSContext* cx, JSObject* /* unused */)
 {
     MOZ_ASSERT(NS_IsMainThread());
-    JSCompartment* c = js::GetContextCompartment(cx);
+    JS::Realm* realm = JS::GetCurrentRealmOrNull(cx);
+    MOZ_ASSERT(realm);
+    JSCompartment* c = JS::GetCompartmentForRealm(realm);
 
     // For remote XUL, we run XBL in the XUL scope. Given that we care about
     // compat and not security for remote XUL, we just always claim to be XBL.
     //
     // Note that, for performance, we don't check AllowXULXBLForPrincipal here,
     // and instead rely on the fact that AllowContentXBLScope() only returns false in
     // remote XUL situations.
-    return AccessCheck::isChrome(c) || IsContentXBLCompartment(c) || !AllowContentXBLScope(c);
+    return AccessCheck::isChrome(c) || IsContentXBLCompartment(c) || !AllowContentXBLScope(realm);
 }
 
 namespace workers {
 extern bool IsCurrentThreadRunningChromeWorker();
 } // namespace workers
 
 bool
 ThreadSafeIsChromeOrXBL(JSContext* cx, JSObject* obj)
diff --git a/js/xpconnect/src/xpcpublic.h b/js/xpconnect/src/xpcpublic.h
--- a/js/xpconnect/src/xpcpublic.h
+++ b/js/xpconnect/src/xpcpublic.h
@@ -110,24 +110,24 @@ GetXBLScopeOrGlobal(JSContext* cx, JSObj
 // Like GetXBLScopeOrGlobal, it returns the scope of |obj| if it's already a
 // content XBL scope. But it asserts that |obj| is not an add-on scope.
 JSObject*
 GetScopeForXBLExecution(JSContext* cx, JS::HandleObject obj, JSAddonId* addonId);
 
 // Returns whether XBL scopes have been explicitly disabled for code running
 // in this compartment. See the comment around mAllowContentXBLScope.
 bool
-AllowContentXBLScope(JSCompartment* c);
+AllowContentXBLScope(JS::Realm* realm);
 
-// Returns whether we will use an XBL scope for this compartment. This is
+// Returns whether we will use an XBL scope for this realm. This is
 // semantically equivalent to comparing global != GetXBLScope(global), but it
 // does not have the side-effect of eagerly creating the XBL scope if it does
 // not already exist.
 bool
-UseContentXBLScope(JSCompartment* c);
+UseContentXBLScope(JS::Realm* realm);
 
 // Clear out the content XBL scope (if any) on the given global.  This will
 // force creation of a new one if one is needed again.
 void
 ClearContentXBLScope(JSObject* global);
 
 bool
 IsAddonCompartment(JSCompartment* c);
