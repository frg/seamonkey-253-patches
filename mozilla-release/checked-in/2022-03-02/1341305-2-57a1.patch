# HG changeset patch
# User Luca Greco <lgreco@mozilla.com>
# Date 1500908335 -7200
#      Mon Jul 24 16:58:55 2017 +0200
# Node ID 9b8dbd9c83abec7d91e0c00407866204b5b1aa20
# Parent  fa13efb7d3b1f4906cbed2eea269b471d56be6a7
Bug 1341305 - Implement devtools.panels.elements.createSidebarPane and sidebar's setObject APIs. r=aswan

MozReview-Commit-ID: 2OhNuUWY9CP

diff --git a/browser/components/extensions/ext-c-devtools-panels.js b/browser/components/extensions/ext-c-devtools-panels.js
--- a/browser/components/extensions/ext-c-devtools-panels.js
+++ b/browser/components/extensions/ext-c-devtools-panels.js
@@ -128,24 +128,142 @@ class ChildDevToolsPanel extends Extensi
     this.mm.removeMessageListener("Extension:DevToolsPanelShown", this);
     this.mm.removeMessageListener("Extension:DevToolsPanelHidden", this);
 
     this._panelContext = null;
     this.context = null;
   }
 }
 
+/**
+ * Represents an addon devtools inspector sidebar in the child process.
+ *
+ * @param {DevtoolsExtensionContext}
+ *   A devtools extension context running in a child process.
+ * @param {object} sidebarOptions
+ * @param {string} sidebarOptions.id
+ *   The id of the addon devtools sidebar registered in the main process.
+ */
+class ChildDevToolsInspectorSidebar extends ExtensionUtils.EventEmitter {
+  constructor(context, {id}) {
+    super();
+
+    this.context = context;
+    this.context.callOnClose(this);
+
+    this.id = id;
+
+    this.mm = context.messageManager;
+    this.mm.addMessageListener("Extension:DevToolsInspectorSidebarShown", this);
+    this.mm.addMessageListener("Extension:DevToolsInspectorSidebarHidden", this);
+  }
+
+  close() {
+    this.mm.removeMessageListener("Extension:DevToolsInspectorSidebarShown", this);
+    this.mm.removeMessageListener("Extension:DevToolsInspectorSidebarHidden", this);
+
+    this.content = null;
+  }
+
+  receiveMessage({name, data}) {
+    // Filter out any message that is not related to the id of this
+    // toolbox panel.
+    if (!data || data.inspectorSidebarId !== this.id) {
+      return;
+    }
+
+    switch (name) {
+      case "Extension:DevToolsInspectorSidebarShown":
+        this.onParentSidebarShown();
+        break;
+      case "Extension:DevToolsInspectorSidebarHidden":
+        this.onParentSidebarHidden();
+        break;
+    }
+  }
+
+  onParentSidebarShown() {
+    // TODO: wait and emit sidebar contentWindow once sidebar.setPage is supported.
+    this.emit("shown");
+  }
+
+  onParentSidebarHidden() {
+    this.emit("hidden");
+  }
+
+  api() {
+    const {context, id} = this;
+
+    return {
+      onShown: new EventManager(
+        context, "devtoolsInspectorSidebar.onShown", fire => {
+          const listener = (eventName, panelContentWindow) => {
+            fire.asyncWithoutClone(panelContentWindow);
+          };
+          this.on("shown", listener);
+          return () => {
+            this.off("shown", listener);
+          };
+        }).api(),
+
+      onHidden: new EventManager(
+        context, "devtoolsInspectorSidebar.onHidden", fire => {
+          const listener = () => {
+            fire.async();
+          };
+          this.on("hidden", listener);
+          return () => {
+            this.off("hidden", listener);
+          };
+        }).api(),
+
+      setObject(jsonObject, rootTitle) {
+        return context.cloneScope.Promise.resolve().then(() => {
+          return context.childManager.callParentAsyncFunction(
+            "devtools.panels.elements.Sidebar.setObject",
+            [id, jsonObject, rootTitle]
+          );
+        });
+      },
+    };
+  }
+}
+
 this.devtools_panels = class extends ExtensionAPI {
   getAPI(context) {
     const themeChangeObserver = ExtensionChildDevToolsUtils.getThemeChangeObserver();
 
     return {
       devtools: {
         panels: {
+          elements: {
+            createSidebarPane(title) {
+              // NOTE: this is needed to be able to return to the caller (the extension)
+              // a promise object that it had the privileges to use (e.g. by marking this
+              // method async we will return a promise object which can only be used by
+              // chrome privileged code).
+              return context.cloneScope.Promise.resolve().then(async () => {
+                const sidebarId = await context.childManager.callParentAsyncFunction(
+                  "devtools.panels.elements.createSidebarPane", [title]);
+
+                const sidebar = new ChildDevToolsInspectorSidebar(context, {id: sidebarId});
+
+                const sidebarAPI = Cu.cloneInto(sidebar.api(),
+                                                context.cloneScope,
+                                                {cloneFunctions: true});
+
+                return sidebarAPI;
+              });
+            },
+          },
           create(title, icon, url) {
+            // NOTE: this is needed to be able to return to the caller (the extension)
+            // a promise object that it had the privileges to use (e.g. by marking this
+            // method async we will return a promise object which can only be used by
+            // chrome privileged code).
             return context.cloneScope.Promise.resolve().then(async () => {
               const panelId = await context.childManager.callParentAsyncFunction(
                 "devtools.panels.create", [title, icon, url]);
 
               const devtoolsPanel = new ChildDevToolsPanel(context, {id: panelId});
 
               const devtoolsPanelAPI = Cu.cloneInto(devtoolsPanel.api(),
                                                     context.cloneScope,
diff --git a/browser/components/extensions/ext-devtools-panels.js b/browser/components/extensions/ext-devtools-panels.js
--- a/browser/components/extensions/ext-devtools-panels.js
+++ b/browser/components/extensions/ext-devtools-panels.js
@@ -262,51 +262,185 @@ class DevToolsSelectionObserver extends 
     this.destroyed = true;
   }
 
   onSelected(event) {
     this.emit("selectionChanged");
   }
 }
 
+/**
+ * Represents an addon devtools inspector sidebar in the main process.
+ *
+ * @param {ExtensionChildProxyContext} context
+ *        A devtools extension proxy context running in a main process.
+ * @param {object} options
+ * @param {string} options.id
+ *        The id of the addon devtools sidebar.
+ * @param {string} options.title
+ *        The title of the addon devtools sidebar.
+ */
+class ParentDevToolsInspectorSidebar {
+  constructor(context, sidebarOptions) {
+    const toolbox = context.devToolsToolbox;
+    if (!toolbox) {
+      // This should never happen when this constructor is called with a valid
+      // devtools extension context.
+      throw Error("Missing mandatory toolbox");
+    }
+
+    this.extension = context.extension;
+    this.visible = false;
+    this.destroyed = false;
+
+    this.toolbox = toolbox;
+    this.context = context;
+    this.sidebarOptions = sidebarOptions;
+
+    this.context.callOnClose(this);
+
+    this.id = this.sidebarOptions.id;
+    this.onSidebarSelect = this.onSidebarSelect.bind(this);
+    this.onSidebarCreated = this.onSidebarCreated.bind(this);
+
+    this.toolbox.once(`extension-sidebar-created-${this.id}`, this.onSidebarCreated);
+    this.toolbox.on(`inspector-sidebar-select`, this.onSidebarSelect);
+
+    // Set by setObject if the sidebar has not been created yet.
+    this._initializeSidebar = null;
+
+    this.toolbox.registerInspectorExtensionSidebar(this.id, {
+      title: sidebarOptions.title,
+    });
+  }
+
+  close() {
+    if (this.destroyed) {
+      throw new Error("Unable to close a destroyed DevToolsSelectionObserver");
+    }
+
+    this.toolbox.off(`extension-sidebar-created-${this.id}`, this.onSidebarCreated);
+    this.toolbox.off(`inspector-sidebar-select`, this.onSidebarSelect);
+
+    this.toolbox.unregisterInspectorExtensionSidebar(this.id);
+    this.extensionSidebar = null;
+    this._initializeSidebar = null;
+
+    this.destroyed = true;
+  }
+
+  onSidebarCreated(evt, sidebar) {
+    this.extensionSidebar = sidebar;
+
+    if (typeof this._initializeSidebar === "function") {
+      this._initializeSidebar();
+      this._initializeSidebar = null;
+    }
+  }
+
+  onSidebarSelect(what, id) {
+    if (!this.extensionSidebar) {
+      return;
+    }
+
+    if (!this.visible && id === this.id) {
+      // TODO: Wait for top level context if extension page
+      this.visible = true;
+      this.context.parentMessageManager.sendAsyncMessage("Extension:DevToolsInspectorSidebarShown", {
+        inspectorSidebarId: this.id,
+      });
+    } else if (this.visible && id !== this.id) {
+      this.visible = false;
+      this.context.parentMessageManager.sendAsyncMessage("Extension:DevToolsInspectorSidebarHidden", {
+        inspectorSidebarId: this.id,
+      });
+    }
+  }
+
+  setObject(object, rootTitle) {
+    // Nest the object inside an object, as the value of the `rootTitle` property.
+    if (rootTitle) {
+      object = {[rootTitle]: object};
+    }
+
+    if (this.extensionSidebar) {
+      this.extensionSidebar.setObject(object);
+    } else {
+      // Defer the sidebar.setObject call.
+      this._initializeSidebar = () => this.extensionSidebar.setObject(object);
+    }
+  }
+}
+
+const sidebarsById = new Map();
+
 this.devtools_panels = class extends ExtensionAPI {
   getAPI(context) {
     // An incremental "per context" id used in the generated devtools panel id.
     let nextPanelId = 0;
 
     const toolboxSelectionObserver = new DevToolsSelectionObserver(context);
 
+    function newBasePanelId() {
+      return `${context.extension.id}-${context.contextId}-${nextPanelId++}`;
+    }
+
     return {
       devtools: {
         panels: {
           elements: {
             onSelectionChanged: new EventManager(
               context, "devtools.panels.elements.onSelectionChanged", fire => {
                 const listener = (eventName) => {
                   fire.async();
                 };
                 toolboxSelectionObserver.on("selectionChanged", listener);
                 return () => {
                   toolboxSelectionObserver.off("selectionChanged", listener);
                 };
               }).api(),
+            createSidebarPane(title) {
+              const id = `devtools-inspector-sidebar-${makeWidgetId(newBasePanelId())}`;
+
+              const parentSidebar = new ParentDevToolsInspectorSidebar(context, {title, id});
+              sidebarsById.set(id, parentSidebar);
+
+              context.callOnClose({
+                close() {
+                  sidebarsById.delete(id);
+                },
+              });
+
+              // Resolved to the devtools sidebar id into the child addon process,
+              // where it will be used to identify the messages related
+              // to the panel API onShown/onHidden events.
+              return Promise.resolve(id);
+            },
+            // The following methods are used internally to allow the sidebar API
+            // piece that is running in the child process to asks the parent process
+            // to execute the sidebar methods.
+            Sidebar: {
+              setObject(sidebarId, jsonObject, rootTitle) {
+                const sidebar = sidebarsById.get(sidebarId);
+                return sidebar.setObject(jsonObject, rootTitle);
+              },
+            },
           },
           create(title, icon, url) {
             // Get a fallback icon from the manifest data.
             if (icon === "" && context.extension.manifest.icons) {
               const iconInfo = IconDetails.getPreferredIcon(context.extension.manifest.icons,
                                                             context.extension, 128);
               icon = iconInfo ? iconInfo.icon : "";
             }
 
             icon = context.extension.baseURI.resolve(icon);
             url = context.extension.baseURI.resolve(url);
 
-            const baseId = `${context.extension.id}-${context.contextId}-${nextPanelId++}`;
-            const id = `${makeWidgetId(baseId)}-devtools-panel`;
+            const id = `${makeWidgetId(newBasePanelId())}-devtools-panel`;
 
             new ParentDevToolsPanel(context, {title, icon, url, id});
 
             // Resolved to the devtools panel id into the child addon process,
             // where it will be used to identify the messages related
             // to the panel API onShown/onHidden events.
             return Promise.resolve(id);
           },
diff --git a/browser/components/extensions/schemas/devtools_panels.json b/browser/components/extensions/schemas/devtools_panels.json
--- a/browser/components/extensions/schemas/devtools_panels.json
+++ b/browser/components/extensions/schemas/devtools_panels.json
@@ -19,17 +19,17 @@
             "name": "onSelectionChanged",
             "type": "function",
             "description": "Fired when an object is selected in the panel."
           }
         ],
         "functions": [
           {
             "name": "createSidebarPane",
-            "unsupported": true,
+            "async": "callback",
             "type": "function",
             "description": "Creates a pane within panel's sidebar.",
             "parameters": [
               {
                 "name": "title",
                 "type": "string",
                 "description": "Text that is displayed in sidebar caption."
               },
@@ -176,16 +176,17 @@
                 "type": "string",
                 "description": "A CSS-like size specification, such as <code>'100px'</code> or <code>'12ex'</code>."
               }
             ]
           },
           {
             "name": "setExpression",
             "unsupported": true,
+            "async": "callback",
             "type": "function",
             "description": "Sets an expression that is evaluated within the inspected page. The result is displayed in the sidebar pane.",
             "parameters": [
               {
                 "name": "expression",
                 "type": "string",
                 "description": "An expression to be evaluated in context of the inspected page. JavaScript objects and DOM nodes are displayed in an expandable tree similar to the console/watch."
               },
@@ -200,17 +201,17 @@
                 "type": "function",
                 "optional": true,
                 "description": "A callback invoked after the sidebar pane is updated with the expression evaluation results."
               }
             ]
           },
           {
             "name": "setObject",
-            "unsupported": true,
+            "async": "callback",
             "type": "function",
             "description": "Sets a JSON-compliant object to be displayed in the sidebar pane.",
             "parameters": [
               {
                 "name": "jsonObject",
                 "type": "string",
                 "description": "An object to be displayed in context of the inspected page. Evaluated in the context of the caller (API client)."
               },
@@ -240,32 +241,30 @@
                 "description": "Relative path of an extension page to display within the sidebar."
               }
             ]
           }
         ],
         "events": [
           {
             "name": "onShown",
-            "unsupported": true,
             "type": "function",
             "description": "Fired when the sidebar pane becomes visible as a result of user switching to the panel that hosts it.",
             "parameters": [
               {
                 "name": "window",
                 "type": "object",
                 "isInstanceOf": "global",
                 "additionalProperties": { "type": "any" },
                 "description": "The JavaScript <code>window</code> object of the sidebar page, if one was set with the <code>setPage()</code> method."
               }
             ]
           },
           {
             "name": "onHidden",
-            "unsupported": true,
             "type": "function",
             "description": "Fired when the sidebar pane becomes hidden as a result of the user switching away from the panel that hosts the sidebar pane."
           }
         ]
       },
       {
         "id": "Button",
         "type": "object",
diff --git a/browser/components/extensions/test/browser/browser-common.ini b/browser/components/extensions/test/browser/browser-common.ini
--- a/browser/components/extensions/test/browser/browser-common.ini
+++ b/browser/components/extensions/test/browser/browser-common.ini
@@ -71,16 +71,17 @@ skip-if = (os == 'win' && !debug) # bug 
 [browser_ext_currentWindow.js]
 [browser_ext_devtools_inspectedWindow.js]
 [browser_ext_devtools_inspectedWindow_eval_bindings.js]
 [browser_ext_devtools_inspectedWindow_reload.js]
 [browser_ext_devtools_network.js]
 [browser_ext_devtools_page.js]
 [browser_ext_devtools_panel.js]
 [browser_ext_devtools_panels_elements.js]
+[browser_ext_devtools_panels_elements_sidebar.js]
 [browser_ext_find.js]
 [browser_ext_geckoProfiler_symbolicate.js]
 [browser_ext_getViews.js]
 [browser_ext_identity_indication.js]
 [browser_ext_incognito_views.js]
 [browser_ext_incognito_popup.js]
 [browser_ext_lastError.js]
 [browser_ext_menus.js]
diff --git a/browser/components/extensions/test/browser/browser_ext_devtools_panels_elements_sidebar.js b/browser/components/extensions/test/browser/browser_ext_devtools_panels_elements_sidebar.js
new file mode 100644
--- /dev/null
+++ b/browser/components/extensions/test/browser/browser_ext_devtools_panels_elements_sidebar.js
@@ -0,0 +1,123 @@
+/* -*- Mode: indent-tabs-mode: nil; js-indent-level: 2 -*- */
+/* vim: set sts=2 sw=2 et tw=80: */
+"use strict";
+
+XPCOMUtils.defineLazyModuleGetter(this, "gDevTools",
+                                  "resource://devtools/client/framework/gDevTools.jsm");
+XPCOMUtils.defineLazyModuleGetter(this, "devtools",
+                                  "resource://devtools/shared/Loader.jsm");
+
+add_task(async function test_devtools_panels_elements_sidebar() {
+  let tab = await BrowserTestUtils.openNewForegroundTab(gBrowser, "http://mochi.test:8888/");
+
+  async function devtools_page() {
+    const sidebar1 = await browser.devtools.panels.elements.createSidebarPane("Test Sidebar 1");
+    const sidebar2 = await browser.devtools.panels.elements.createSidebarPane("Test Sidebar 2");
+
+    const onShownListener = (event, sidebarInstance) => {
+      browser.test.sendMessage(`devtools_sidebar_${event}`, sidebarInstance);
+    };
+
+    sidebar1.onShown.addListener(() => onShownListener("shown", "sidebar1"));
+    sidebar2.onShown.addListener(() => onShownListener("shown", "sidebar2"));
+    sidebar1.onHidden.addListener(() => onShownListener("hidden", "sidebar1"));
+    sidebar2.onHidden.addListener(() => onShownListener("hidden", "sidebar2"));
+
+    sidebar1.setObject({propertyName: "propertyValue"}, "Optional Root Object Title");
+    sidebar2.setObject({anotherPropertyName: 123});
+
+    browser.test.sendMessage("devtools_page_loaded");
+  }
+
+  let extension = ExtensionTestUtils.loadExtension({
+    manifest: {
+      devtools_page: "devtools_page.html",
+    },
+    files: {
+      "devtools_page.html": `<!DOCTYPE html>
+      <html>
+       <head>
+         <meta charset="utf-8">
+       </head>
+       <body>
+         <script src="devtools_page.js"></script>
+       </body>
+      </html>`,
+      "devtools_page.js": devtools_page,
+    },
+  });
+
+  await extension.startup();
+
+  let target = devtools.TargetFactory.forTab(tab);
+
+  const toolbox = await gDevTools.showToolbox(target, "webconsole");
+  info("developer toolbox opened");
+
+  await extension.awaitMessage("devtools_page_loaded");
+
+  const waitInspector = toolbox.once("inspector-selected");
+  toolbox.selectTool("inspector");
+  await waitInspector;
+
+  const sidebarIds = Array.from(toolbox._inspectorExtensionSidebars.keys());
+
+  const inspector = await toolbox.getPanel("inspector");
+
+  inspector.sidebar.show(sidebarIds[0]);
+
+  const shownSidebarInstance = await extension.awaitMessage("devtools_sidebar_shown");
+
+  is(shownSidebarInstance, "sidebar1", "Got the shown event on the first extension sidebar");
+
+  const sidebarPanel1 = inspector.sidebar.getTabPanel(sidebarIds[0]);
+
+  ok(sidebarPanel1, "Got a rendered sidebar panel for the first registered extension sidebar");
+
+  is(sidebarPanel1.querySelectorAll("table.treeTable").length, 1,
+     "The first sidebar panel contains a rendered TreeView component");
+
+  is(sidebarPanel1.querySelectorAll("table.treeTable .stringCell").length, 1,
+     "The TreeView component contains the expected number of string cells.");
+
+  const sidebarPanel1Tree = sidebarPanel1.querySelector("table.treeTable");
+  ok(
+    sidebarPanel1Tree.innerText.includes("Optional Root Object Title"),
+    "The optional root object title has been included in the object tree"
+  );
+
+  inspector.sidebar.show(sidebarIds[1]);
+
+  const shownSidebarInstance2 = await extension.awaitMessage("devtools_sidebar_shown");
+  const hiddenSidebarInstance1 = await extension.awaitMessage("devtools_sidebar_hidden");
+
+  is(shownSidebarInstance2, "sidebar2", "Got the shown event on the second extension sidebar");
+  is(hiddenSidebarInstance1, "sidebar1", "Got the hidden event on the first extension sidebar");
+
+  const sidebarPanel2 = inspector.sidebar.getTabPanel(sidebarIds[1]);
+
+  ok(sidebarPanel2, "Got a rendered sidebar panel for the second registered extension sidebar");
+
+  is(sidebarPanel2.querySelectorAll("table.treeTable").length, 1,
+     "The second sidebar panel contains a rendered TreeView component");
+
+  is(sidebarPanel2.querySelectorAll("table.treeTable .numberCell").length, 1,
+     "The TreeView component contains the expected a cell of type number.");
+
+  await extension.unload();
+
+  is(Array.from(toolbox._inspectorExtensionSidebars.keys()).length, 0,
+     "All the registered sidebars have been unregistered on extension unload");
+
+  is(inspector.sidebar.getTabPanel(sidebarIds[0]), undefined,
+     "The first registered sidebar has been removed");
+
+  is(inspector.sidebar.getTabPanel(sidebarIds[1]), undefined,
+     "The second registered sidebar has been removed");
+
+  await gDevTools.closeToolbox(target);
+
+  await target.destroy();
+
+  await BrowserTestUtils.removeTab(tab);
+});
diff --git a/toolkit/components/extensions/Schemas.jsm b/toolkit/components/extensions/Schemas.jsm
--- a/toolkit/components/extensions/Schemas.jsm
+++ b/toolkit/components/extensions/Schemas.jsm
@@ -2175,17 +2175,19 @@ FunctionEntry = class FunctionEntry exte
     this.hasAsyncCallback = type.hasAsyncCallback;
     this.requireUserInput = type.requireUserInput;
   }
 
   checkValue({type, optional, name}, value, context) {
     if (optional && value == null) {
       return;
     }
-    if (type.reference === "ExtensionPanel" || type.reference === "Port") {
+    if (type.reference === "ExtensionPanel" ||
+        type.reference === "ExtensionSidebarPane" ||
+        type.reference === "Port") {
       // TODO: We currently treat objects with functions as SubModuleType,
       // which is just wrong, and a bigger yak.  Skipping for now.
       return;
     }
     const {error} = type.normalize(value, context);
     if (error) {
       this.throwError(context, `Type error for ${name} value (${forceString(error)})`);
     }
