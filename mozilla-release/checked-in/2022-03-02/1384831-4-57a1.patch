# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1501095127 -28800
# Node ID e0d7d98271e246dcfcb986381755cc215570f6d5
# Parent  34906a2a7b4ca5fb3cf172922e960fb88381bc2b
Bug 1384831. P4 - let HLSDecoder inherit MediaDecoder. r=kikuo

MozReview-Commit-ID: B9XvfbjjMxX

diff --git a/dom/media/hls/HLSDecoder.cpp b/dom/media/hls/HLSDecoder.cpp
--- a/dom/media/hls/HLSDecoder.cpp
+++ b/dom/media/hls/HLSDecoder.cpp
@@ -22,17 +22,17 @@ namespace mozilla {
 void
 HLSDecoder::Shutdown()
 {
   MOZ_ASSERT(NS_IsMainThread());
   auto resource = static_cast<HLSResource*>(mResource.get());
   if (resource) {
     resource->Detach();
   }
-  ChannelMediaDecoder::Shutdown();
+  MediaDecoder::Shutdown();
 }
 
 MediaDecoderStateMachine*
 HLSDecoder::CreateStateMachine()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   MediaResource* resource = GetResource();
@@ -45,23 +45,16 @@ HLSDecoder::CreateStateMachine()
   init.mCrashHelper = GetOwner()->CreateGMPCrashHelper();
   init.mFrameStats = mFrameStats;
   mReader =
     new MediaFormatReader(init, new HLSDemuxer(resourceWrapper->GetPlayerId()));
 
   return new MediaDecoderStateMachine(this, mReader);
 }
 
-ChannelMediaDecoder*
-HLSDecoder::Clone(MediaDecoderInit& aInit)
-{
-  MOZ_CRASH("Clone is not supported");
-  return nullptr;
-}
-
 bool
 HLSDecoder::IsEnabled()
 {
   return MediaPrefs::HLSEnabled() && (jni::GetAPIVersion() >= 16);
 }
 
 bool
 HLSDecoder::IsSupportedType(const MediaContainerType& aContainerType)
@@ -93,23 +86,16 @@ HLSDecoder::Load(nsIChannel* aChannel,
 
   SetStateMachine(CreateStateMachine());
   NS_ENSURE_TRUE(GetStateMachine(), NS_ERROR_FAILURE);
 
   return InitializeStateMachine();
 }
 
 nsresult
-HLSDecoder::Load(MediaResource*)
-{
-  MOZ_CRASH("Clone is not supported");
-  return NS_ERROR_FAILURE;
-}
-
-nsresult
 HLSDecoder::Play()
 {
   MOZ_ASSERT(NS_IsMainThread());
   HLS_DEBUG("HLSDecoder", "MediaElement called Play");
   auto resourceWrapper =
         static_cast<HLSResource*>(GetResource())->GetResourceWrapper();
   resourceWrapper->Play();
   return MediaDecoder::Play();
diff --git a/dom/media/hls/HLSDecoder.h b/dom/media/hls/HLSDecoder.h
--- a/dom/media/hls/HLSDecoder.h
+++ b/dom/media/hls/HLSDecoder.h
@@ -2,48 +2,44 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef HLSDecoder_h_
 #define HLSDecoder_h_
 
-#include "ChannelMediaDecoder.h"
+#include "MediaDecoder.h"
 
 namespace mozilla {
-class MediaFormatReader;
 
-class HLSDecoder final : public ChannelMediaDecoder
+class HLSDecoder final : public MediaDecoder
 {
 public:
   // MediaDecoder interface.
   explicit HLSDecoder(MediaDecoderInit& aInit)
-    : ChannelMediaDecoder(aInit)
+    : MediaDecoder(aInit)
   {
   }
 
   void Shutdown() override;
 
-  ChannelMediaDecoder* Clone(MediaDecoderInit& aInit) override;
-
   MediaDecoderStateMachine* CreateStateMachine() override;
 
   // Returns true if the HLS backend is pref'ed on.
   static bool IsEnabled();
 
   // Returns true if aContainerType is an HLS type that we think we can render
   // with the a platform decoder backend.
   // If provided, codecs are checked for support.
   static bool IsSupportedType(const MediaContainerType& aContainerType);
 
   nsresult Load(nsIChannel* aChannel,
                 bool aIsPrivateBrowsing,
-                nsIStreamListener**) override;
-  nsresult Load(MediaResource*) override;
+                nsIStreamListener**);
 
   nsresult Play() override;
 
   void Pause() override;
 };
 
 } // namespace mozilla
 

