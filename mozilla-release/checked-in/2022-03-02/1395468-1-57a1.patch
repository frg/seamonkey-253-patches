# HG changeset patch
# User James Cheng <jacheng@mozilla.com>
# Date 1503994395 -28800
# Node ID 0dc9ff0ccee0c1f4c7b09851fc6cb0ec2c72a440
# Parent  4a6b59709df21d5f162e3a75c88f17b31e0b7e83
Bug 1395468 - [Part1] Remove media.eme.chromium-api.enabled preference and its related code. r=cpearce

1. Delete MediaPrefs::EMEChromiumAPIEnabled() and related logic.
2. We now only use the Chromium CDM interface so delete the opposite side check of MediaPrefs::EMEChromiumAPIEnabled().

MozReview-Commit-ID: GDFrrf4WlWf

diff --git a/dom/ipc/ContentPrefs.cpp b/dom/ipc/ContentPrefs.cpp
--- a/dom/ipc/ContentPrefs.cpp
+++ b/dom/ipc/ContentPrefs.cpp
@@ -126,17 +126,16 @@ const char* mozilla::dom::ContentPrefs::
   "media.decoder-doctor.wmf-disabled-is-failure",
   "media.decoder.fuzzing.dont-delay-inputexhausted",
   "media.decoder.fuzzing.enabled",
   "media.decoder.fuzzing.video-output-minimum-interval-ms",
   "media.decoder.limit",
   "media.decoder.recycle.enabled",
   "media.dormant-on-pause-timeout-ms",
   "media.eme.audio.blank",
-  "media.eme.chromium-api.enabled",
   "media.eme.enabled",
   "media.eme.video.blank",
   "media.ffmpeg.enabled",
   "media.ffvpx.enabled",
   "media.ffvpx.low-latency.enabled",
   "media.flac.enabled",
   "media.forcestereo.enabled",
   "media.gmp.async-shutdown-timeout",
diff --git a/dom/media/MediaPrefs.h b/dom/media/MediaPrefs.h
--- a/dom/media/MediaPrefs.h
+++ b/dom/media/MediaPrefs.h
@@ -145,17 +145,16 @@ private:
   DECL_MEDIA_PREF("media.decoder.fuzzing.dont-delay-inputexhausted", PDMFuzzingDelayInputExhausted, bool, true);
   DECL_MEDIA_PREF("media.decoder.recycle.enabled",            MediaDecoderCheckRecycling, bool, false);
   DECL_MEDIA_PREF("media.decoder.skip-to-next-key-frame.enabled", MFRSkipToNextKeyFrameEnabled, bool, true);
   DECL_MEDIA_PREF("media.gmp.decoder.enabled",                PDMGMPEnabled, bool, true);
   DECL_MEDIA_PREF("media.gmp.decoder.aac",                    GMPAACPreferred, uint32_t, 0);
   DECL_MEDIA_PREF("media.gmp.decoder.h264",                   GMPH264Preferred, uint32_t, 0);
   DECL_MEDIA_PREF("media.eme.audio.blank",                    EMEBlankAudio, bool, false);
   DECL_MEDIA_PREF("media.eme.video.blank",                    EMEBlankVideo, bool, false);
-  DECL_MEDIA_PREF("media.eme.chromium-api.enabled",           EMEChromiumAPIEnabled, bool, false);
   DECL_MEDIA_PREF("media.eme.chromium-api.video-shmems",
                   EMEChromiumAPIVideoShmemCount,
                   uint32_t,
                   3);
 
   // MediaDecoderStateMachine
   DECL_MEDIA_PREF("media.suspend-bkgnd-video.enabled",        MDSMSuspendBackgroundVideoEnabled, bool, false);
   DECL_MEDIA_PREF("media.suspend-bkgnd-video.delay-ms",       MDSMSuspendBackgroundVideoDelay, AtomicUint32, SUSPEND_BACKGROUND_VIDEO_DELAY_MS);
diff --git a/dom/media/eme/MediaKeySystemAccess.cpp b/dom/media/eme/MediaKeySystemAccess.cpp
--- a/dom/media/eme/MediaKeySystemAccess.cpp
+++ b/dom/media/eme/MediaKeySystemAccess.cpp
@@ -98,19 +98,17 @@ MediaKeySystemAccess::CreateMediaKeys(Er
                                        mKeySystem,
                                        mConfig));
   return keys->Init(aRv);
 }
 
 static bool
 HavePluginForKeySystem(const nsCString& aKeySystem)
 {
-  nsCString api = MediaPrefs::EMEChromiumAPIEnabled()
-                    ? NS_LITERAL_CSTRING(CHROMIUM_CDM_API)
-                    : NS_LITERAL_CSTRING(GMP_API_DECRYPTOR);
+  nsCString api = NS_LITERAL_CSTRING(CHROMIUM_CDM_API);
 
   bool havePlugin = HaveGMPFor(api, { aKeySystem });
 #ifdef MOZ_WIDGET_ANDROID
   // Check if we can use MediaDrm for this keysystem.
   if (!havePlugin) {
     havePlugin = mozilla::java::MediaDrmProxy::IsSchemeSupported(aKeySystem);
   }
 #endif
diff --git a/dom/media/eme/MediaKeys.cpp b/dom/media/eme/MediaKeys.cpp
--- a/dom/media/eme/MediaKeys.cpp
+++ b/dom/media/eme/MediaKeys.cpp
@@ -337,33 +337,23 @@ MediaKeys::CreateCDMProxy(nsIEventTarget
     proxy = new MediaDrmCDMProxy(this,
                                  mKeySystem,
                                  mConfig.mDistinctiveIdentifier == MediaKeysRequirement::Required,
                                  mConfig.mPersistentState == MediaKeysRequirement::Required,
                                  aMainThread);
   } else
 #endif
   {
-    if (MediaPrefs::EMEChromiumAPIEnabled()) {
-      proxy = new ChromiumCDMProxy(
-        this,
-        mKeySystem,
-        new MediaKeysGMPCrashHelper(this),
-        mConfig.mDistinctiveIdentifier == MediaKeysRequirement::Required,
-        mConfig.mPersistentState == MediaKeysRequirement::Required,
-        aMainThread);
-    } else {
-      proxy = new GMPCDMProxy(
-        this,
-        mKeySystem,
-        new MediaKeysGMPCrashHelper(this),
-        mConfig.mDistinctiveIdentifier == MediaKeysRequirement::Required,
-        mConfig.mPersistentState == MediaKeysRequirement::Required,
-        aMainThread);
-    }
+    proxy = new ChromiumCDMProxy(
+      this,
+      mKeySystem,
+      new MediaKeysGMPCrashHelper(this),
+      mConfig.mDistinctiveIdentifier == MediaKeysRequirement::Required,
+      mConfig.mPersistentState == MediaKeysRequirement::Required,
+      aMainThread);
   }
   return proxy.forget();
 }
 
 already_AddRefed<DetailedPromise>
 MediaKeys::Init(ErrorResult& aRv)
 {
   RefPtr<DetailedPromise> promise(MakePromise(aRv,
diff --git a/dom/media/gmp/GMPParent.cpp b/dom/media/gmp/GMPParent.cpp
--- a/dom/media/gmp/GMPParent.cpp
+++ b/dom/media/gmp/GMPParent.cpp
@@ -735,21 +735,17 @@ GMPParent::ReadChromiumManifestFile(nsIF
 static bool
 IsCDMAPISupported(const mozilla::dom::WidevineCDMManifest& aManifest)
 {
   nsresult ignored; // Note: ToInteger returns 0 on failure.
   int32_t moduleVersion = aManifest.mX_cdm_module_versions.ToInteger(&ignored);
   int32_t interfaceVersion =
     aManifest.mX_cdm_interface_versions.ToInteger(&ignored);
   int32_t hostVersion = aManifest.mX_cdm_host_versions.ToInteger(&ignored);
-  if (MediaPrefs::EMEChromiumAPIEnabled()) {
-    return ChromiumCDMAdapter::Supports(
-      moduleVersion, interfaceVersion, hostVersion);
-  }
-  return WidevineAdapter::Supports(
+  return ChromiumCDMAdapter::Supports(
     moduleVersion, interfaceVersion, hostVersion);
 }
 
 RefPtr<GenericPromise>
 GMPParent::ParseChromiumManifest(const nsAString& aJSON)
 {
   LOGD("%s: for '%s'", __FUNCTION__, NS_LossyConvertUTF16toASCII(aJSON).get());
 
@@ -817,27 +813,19 @@ GMPParent::ParseChromiumManifest(const n
       return GenericPromise::CreateAndReject(NS_ERROR_FAILURE, __func__);
     }
 
     video.mAPITags.AppendElement(codec);
   }
 
   video.mAPITags.AppendElement(kEMEKeySystem);
 
-  if (MediaPrefs::EMEChromiumAPIEnabled()) {
-    video.mAPIName = NS_LITERAL_CSTRING(CHROMIUM_CDM_API);
-    mAdapter = NS_LITERAL_STRING("chromium");
-  } else {
-    video.mAPIName = NS_LITERAL_CSTRING(GMP_API_VIDEO_DECODER);
-    mAdapter = NS_LITERAL_STRING("widevine");
+  video.mAPIName = NS_LITERAL_CSTRING(CHROMIUM_CDM_API);
+  mAdapter = NS_LITERAL_STRING("chromium");
 
-    GMPCapability decrypt(NS_LITERAL_CSTRING(GMP_API_DECRYPTOR));
-    decrypt.mAPITags.AppendElement(kEMEKeySystem);
-    mCapabilities.AppendElement(Move(decrypt));
-  }
   mCapabilities.AppendElement(Move(video));
 
   return GenericPromise::CreateAndResolve(true, __func__);
 }
 
 bool
 GMPParent::CanBeSharedCrossNodeIds() const
 {
diff --git a/dom/media/platforms/agnostic/eme/EMEDecoderModule.cpp b/dom/media/platforms/agnostic/eme/EMEDecoderModule.cpp
--- a/dom/media/platforms/agnostic/eme/EMEDecoderModule.cpp
+++ b/dom/media/platforms/agnostic/eme/EMEDecoderModule.cpp
@@ -352,21 +352,17 @@ EMEDecoderModule::CreateVideoDecoder(con
     return m->CreateVideoDecoder(aParams);
   }
 
   if (SupportsMimeType(aParams.mConfig.mMimeType, nullptr)) {
     // GMP decodes. Assume that means it can decrypt too.
     RefPtr<MediaDataDecoderProxy> wrapper =
       CreateDecoderWrapper(mProxy, aParams);
     auto params = GMPVideoDecoderParams(aParams);
-    if (MediaPrefs::EMEChromiumAPIEnabled()) {
-      wrapper->SetProxyTarget(new ChromiumCDMVideoDecoder(params, mProxy));
-    } else {
-      wrapper->SetProxyTarget(new EMEVideoDecoder(mProxy, params));
-    }
+    wrapper->SetProxyTarget(new ChromiumCDMVideoDecoder(params, mProxy));
     return wrapper.forget();
   }
 
   MOZ_ASSERT(mPDM);
   RefPtr<MediaDataDecoder> decoder(mPDM->CreateDecoder(aParams));
   if (!decoder) {
     return nullptr;
   }
diff --git a/dom/media/platforms/agnostic/gmp/GMPDecoderModule.cpp b/dom/media/platforms/agnostic/gmp/GMPDecoderModule.cpp
--- a/dom/media/platforms/agnostic/gmp/GMPDecoderModule.cpp
+++ b/dom/media/platforms/agnostic/gmp/GMPDecoderModule.cpp
@@ -74,19 +74,17 @@ GMPDecoderModule::CreateAudioDecoder(con
 bool
 GMPDecoderModule::SupportsMimeType(const nsACString& aMimeType,
                                    const Maybe<nsCString>& aGMP)
 {
   if (aGMP.isNothing()) {
     return false;
   }
 
-  nsCString api = MediaPrefs::EMEChromiumAPIEnabled()
-    ? NS_LITERAL_CSTRING(CHROMIUM_CDM_API)
-    : NS_LITERAL_CSTRING(GMP_API_VIDEO_DECODER);
+  nsCString api = NS_LITERAL_CSTRING(CHROMIUM_CDM_API);
 
   if (MP4Decoder::IsH264(aMimeType)) {
     return HaveGMPFor(api, { NS_LITERAL_CSTRING("h264"), aGMP.value()});
   }
 
   if (VPXDecoder::IsVP9(aMimeType)) {
     return HaveGMPFor(api, { NS_LITERAL_CSTRING("vp9"), aGMP.value()});
   }
diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -413,17 +413,16 @@ pref("media.gmp.decoder.h264", 0);
 #ifdef MOZ_RAW
 pref("media.raw.enabled", true);
 #endif
 pref("media.ogg.enabled", true);
 pref("media.opus.enabled", true);
 pref("media.wave.enabled", true);
 pref("media.webm.enabled", true);
 
-pref("media.eme.chromium-api.enabled", true);
 pref("media.eme.chromium-api.video-shmems", 6);
 
 #ifdef MOZ_APPLEMEDIA
 #ifdef MOZ_WIDGET_UIKIT
 pref("media.mp3.enabled", true);
 #endif
 pref("media.apple.mp3.enabled", true);
 pref("media.apple.mp4.enabled", true);
