# HG changeset patch
# User Jean-Luc Bonnafoux <jeanluc.bonnafoux@wanadoo.fr>
# Date 1515400232 -3600
# Node ID fbb8f063f6dcc99ac5f1ac8ae5749d70533f9145
# Parent  0f39f0db6e44e9c66074b3889046e1fc8ad52d95
Bug 1428629 - elfhack.cpp prefer prefix ++ operator for non primitive types r=froydnj

MozReview-Commit-ID: C0L2NUsbmc4

diff --git a/build/unix/elfhack/elfhack.cpp b/build/unix/elfhack/elfhack.cpp
--- a/build/unix/elfhack/elfhack.cpp
+++ b/build/unix/elfhack/elfhack.cpp
@@ -144,48 +144,48 @@ public:
         entry_point = sym->value.getValue();
 
         // Get all relevant sections from the injected code object.
         add_code_section(sym->value.getSection());
 
         // Adjust code sections offsets according to their size
         std::vector<ElfSection *>::iterator c = code.begin();
         (*c)->getShdr().sh_addr = 0;
-        for(ElfSection *last = *(c++); c != code.end(); c++) {
+        for(ElfSection *last = *(c++); c != code.end(); ++c) {
             unsigned int addr = last->getShdr().sh_addr + last->getSize();
             if (addr & ((*c)->getAddrAlign() - 1))
                 addr = (addr | ((*c)->getAddrAlign() - 1)) + 1;
             (*c)->getShdr().sh_addr = addr;
             // We need to align this section depending on the greater
             // alignment required by code sections.
             if (shdr.sh_addralign < (*c)->getAddrAlign())
                 shdr.sh_addralign = (*c)->getAddrAlign();
         }
         shdr.sh_size = code.back()->getAddr() + code.back()->getSize();
         data = new char[shdr.sh_size];
         char *buf = data;
-        for (c = code.begin(); c != code.end(); c++) {
+        for (c = code.begin(); c != code.end(); ++c) {
             memcpy(buf, (*c)->getData(), (*c)->getSize());
             buf += (*c)->getSize();
         }
         name = elfhack_text;
     }
 
     ~ElfRelHackCode_Section() {
         delete elf;
     }
 
     void serialize(std::ofstream &file, char ei_class, char ei_data)
     {
         // Readjust code offsets
-        for (std::vector<ElfSection *>::iterator c = code.begin(); c != code.end(); c++)
+        for (std::vector<ElfSection *>::iterator c = code.begin(); c != code.end(); ++c)
             (*c)->getShdr().sh_addr += getAddr();
 
         // Apply relocations
-        for (std::vector<ElfSection *>::iterator c = code.begin(); c != code.end(); c++) {
+        for (std::vector<ElfSection *>::iterator c = code.begin(); c != code.end(); ++c) {
             for (ElfSection *rel = elf->getSection(1); rel != nullptr; rel = rel->getNext())
                 if (((rel->getType() == SHT_REL) ||
                      (rel->getType() == SHT_RELA)) &&
                     (rel->getInfo().section == *c)) {
                     if (rel->getType() == SHT_REL)
                         apply_relocations((ElfRel_Section<Elf_Rel> *)rel, *c);
                     else
                         apply_relocations((ElfRel_Section<Elf_Rela> *)rel, *c);
@@ -232,17 +232,17 @@ private:
             }
         }
     }
 
     template <typename Rel_Type>
     void scan_relocs_for_code(ElfRel_Section<Rel_Type> *rel)
     {
         ElfSymtab_Section *symtab = (ElfSymtab_Section *)rel->getLink();
-        for (auto r = rel->rels.begin(); r != rel->rels.end(); r++) {
+        for (auto r = rel->rels.begin(); r != rel->rels.end(); ++r) {
             ElfSection *section = symtab->syms[ELF32_R_SYM(r->r_info)].value.getSection();
             add_code_section(section);
         }
     }
 
     class pc32_relocation {
     public:
         Elf32_Addr operator()(unsigned int base_addr, Elf32_Off offset,
@@ -345,17 +345,17 @@ private:
 
     template <typename Rel_Type>
     void apply_relocations(ElfRel_Section<Rel_Type> *rel, ElfSection *the_code)
     {
         assert(rel->getType() == Rel_Type::sh_type);
         char *buf = data + (the_code->getAddr() - code.front()->getAddr());
         // TODO: various checks on the sections
         ElfSymtab_Section *symtab = (ElfSymtab_Section *)rel->getLink();
-        for (typename std::vector<Rel_Type>::iterator r = rel->rels.begin(); r != rel->rels.end(); r++) {
+        for (typename std::vector<Rel_Type>::iterator r = rel->rels.begin(); r != rel->rels.end(); ++r) {
             // TODO: various checks on the symbol
             const char *name = symtab->syms[ELF32_R_SYM(r->r_info)].name;
             unsigned int addr;
             if (symtab->syms[ELF32_R_SYM(r->r_info)].value.getSection() == nullptr) {
                 if (strcmp(name, "relhack") == 0) {
                     addr = relhack_section.getAddr();
                 } else if (strcmp(name, "elf_header") == 0) {
                     // TODO: change this ungly hack to something better
@@ -472,17 +472,17 @@ void maybe_split_segment(Elf *elf, ElfSe
             phdr.p_filesz = (unsigned int)-1;
             phdr.p_memsz = (unsigned int)-1;
             ElfSegment *newSegment = new ElfSegment(&phdr);
             elf->insertSegmentAfter(segment, newSegment);
             ElfSection *section = *it;
             for (; it != segment->end(); ++it) {
                 newSegment->addSection(*it);
             }
-            for (it = newSegment->begin(); it != newSegment->end(); it++) {
+            for (it = newSegment->begin(); it != newSegment->end(); ++it) {
                 segment->removeSection(*it);
             }
             // Fill the virtual address space gap left between the two PT_LOADs
             // with a new PT_LOAD with no permissions. This avoids the linker
             // (especially bionic's) filling the gap with anonymous memory,
             // which breakpad doesn't like.
             // /!\ running strip on a elfhacked binary will break this filler
             // PT_LOAD.
@@ -572,17 +572,17 @@ int do_relocation_section(Elf *elf, unsi
     ElfSymtab_Section *symtab = (ElfSymtab_Section *) section->getLink();
     Elf_SymValue *sym = symtab->lookup("__cxa_pure_virtual");
 
     std::vector<Rel_Type> new_rels;
     Elf_RelHack relhack_entry;
     relhack_entry.r_offset = relhack_entry.r_info = 0;
     size_t init_array_reloc = 0;
     for (typename std::vector<Rel_Type>::iterator i = section->rels.begin();
-         i != section->rels.end(); i++) {
+         i != section->rels.end(); ++i) {
         // We don't need to keep R_*_NONE relocations
         if (!ELF32_R_TYPE(i->r_info))
             continue;
         ElfLocation loc(i->r_offset, elf);
         // __cxa_pure_virtual is a function used in vtables to point at pure
         // virtual methods. The __cxa_pure_virtual function usually abort()s.
         // These functions are however normally never called. In the case
         // where they would, jumping to the null address instead of calling
