# HG changeset patch
# User Ehsan Akhgari <ehsan@mozilla.com>
# Date 1502824331 14400
#      Tue Aug 15 15:12:11 2017 -0400
# Node ID ae748be16f808d7a0483d6bb81a7e07eae5ec5b6
# Parent  5cbe0430f33d7dec2d6f4c03ab0904c3cf7579b1
Bug 1390382 - Part 3: Inline nsCaret::IsVisible(); r=dholbert

diff --git a/layout/base/nsCaret.cpp b/layout/base/nsCaret.cpp
--- a/layout/base/nsCaret.cpp
+++ b/layout/base/nsCaret.cpp
@@ -28,17 +28,16 @@
 #include "nsBlockFrame.h"
 #include "nsISelectionController.h"
 #include "nsTextFrame.h"
 #include "nsXULPopupManager.h"
 #include "nsMenuPopupFrame.h"
 #include "nsTextFragment.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/LookAndFeel.h"
-#include "mozilla/dom/Selection.h"
 #include "nsIBidiKeyboard.h"
 #include "nsContentUtils.h"
 
 using namespace mozilla;
 using namespace mozilla::dom;
 using namespace mozilla::gfx;
 
 // The bidi indicator hangs off the caret to one side, to show which
@@ -247,41 +246,16 @@ void nsCaret::SetSelection(nsISelection 
 void nsCaret::SetVisible(bool inMakeVisible)
 {
   mVisible = inMakeVisible;
   mIgnoreUserModify = mVisible;
   ResetBlinking();
   SchedulePaint();
 }
 
-bool nsCaret::IsVisible(nsISelection* aSelection)
-{
-  if (!mVisible || mHideCount) {
-    return false;
-  }
-
-  if (!mShowDuringSelection) {
-    mozilla::dom::Selection* selection;
-    if (aSelection) {
-      selection = aSelection->AsSelection();
-    } else {
-      selection = GetSelectionInternal();
-    }
-    if (!selection || !selection->IsCollapsed()) {
-      return false;
-    }
-  }
-
-  if (IsMenuPopupHidingCaret()) {
-    return false;
-  }
-
-  return true;
-}
-
 void nsCaret::AddForceHide()
 {
   MOZ_ASSERT(mHideCount < UINT32_MAX);
   if (++mHideCount > 1) {
     return;
   }
   ResetBlinking();
   SchedulePaint();
diff --git a/layout/base/nsCaret.h b/layout/base/nsCaret.h
--- a/layout/base/nsCaret.h
+++ b/layout/base/nsCaret.h
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* the caret is the text cursor used, e.g., when editing */
 
 #ifndef nsCaret_h__
 #define nsCaret_h__
 
 #include "mozilla/MemoryReporting.h"
+#include "mozilla/dom/Selection.h"
 #include "nsCoord.h"
 #include "nsISelectionListener.h"
 #include "nsIWeakReferenceUtils.h"
 #include "CaretAssociationHint.h"
 #include "nsPoint.h"
 #include "nsRect.h"
 
 class nsDisplayListBuilder;
@@ -22,19 +23,16 @@ class nsFrameSelection;
 class nsIContent;
 class nsIDOMNode;
 class nsIFrame;
 class nsINode;
 class nsIPresShell;
 class nsITimer;
 
 namespace mozilla {
-namespace dom {
-class Selection;
-} // namespace dom
 namespace gfx {
 class DrawTarget;
 } // namespace gfx
 } // namespace mozilla
 
 //-----------------------------------------------------------------------------
 class nsCaret final : public nsISelectionListener
 {
@@ -72,17 +70,40 @@ class nsCaret final : public nsISelectio
     void SetVisible(bool intMakeVisible);
     /** IsVisible will get the visibility of the caret.
      *  This returns false if the caret is hidden because it was set
      *  to not be visible, or because the selection is not collapsed, or
      *  because an open popup is hiding the caret.
      *  It does not take account of blinking or the caret being hidden
      *  because we're in non-editable/disabled content.
      */
-    bool IsVisible(nsISelection* aSelection = nullptr);
+    bool IsVisible(nsISelection* aSelection = nullptr)
+    {
+      if (!mVisible || mHideCount) {
+        return false;
+      }
+
+      if (!mShowDuringSelection) {
+        mozilla::dom::Selection* selection;
+        if (aSelection) {
+          selection = static_cast<mozilla::dom::Selection*>(aSelection);
+        } else {
+          selection = GetSelectionInternal();
+        }
+        if (!selection || !selection->IsCollapsed()) {
+          return false;
+        }
+      }
+
+      if (IsMenuPopupHidingCaret()) {
+        return false;
+      }
+
+      return true;
+    }
     /**
      * AddForceHide() increases mHideCount and hide the caret even if
      * SetVisible(true) has been or will be called.  This is useful when the
      * caller wants to hide caret temporarily and it needs to cancel later.
      * Especially, in the latter case, it's too difficult to decide if the
      * caret should be actually visible or not because caret visible state
      * is set from a lot of event handlers.  So, it's very stateful.
      */
