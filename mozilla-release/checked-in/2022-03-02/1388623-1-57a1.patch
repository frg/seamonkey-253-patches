# HG changeset patch
# User Bobby Holley <bobbyholley@gmail.com>
# Date 1501218478 25200
#      Thu Jul 27 22:07:58 2017 -0700
# Node ID 83319f6a69b28af60b4b981d063394f2e0078732
# Parent  472a26bed30a7cda78d3b3c77461badbd0338392
Bug 1388623 - Switch to PreTraverseSync for new-element styling. r=hiro

the PreTraverse stuff is all about ticking animations, which isn't something we
want to do when we're trying to get styles synchronously in the frame
constructor.

MozReview-Commit-ID: L6lw4ef4Jdk

diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -880,52 +880,49 @@ ServoStyleSet::StyleDocument(ServoTraver
     }
   }
   return postTraversalRequired;
 }
 
 void
 ServoStyleSet::StyleNewSubtree(Element* aRoot)
 {
-  MOZ_ASSERT(!aRoot->HasServoData());
-
-  PreTraverse(ServoTraversalFlags::Empty);
+  MOZ_ASSERT(!aRoot->HasServoData(), "Should have called StyleNewChildren");
+  PreTraverseSync();
 
   DebugOnly<bool> postTraversalRequired =
     PrepareAndTraverseSubtree(aRoot, ServoTraversalFlags::Empty);
   MOZ_ASSERT(!postTraversalRequired);
 }
 
 void
 ServoStyleSet::StyleNewChildren(Element* aParent)
 {
-  PreTraverse(ServoTraversalFlags::Empty);
+  MOZ_ASSERT(aParent->HasServoData(), "Should have called StyleNewSubtree");
+  PreTraverseSync();
 
   PrepareAndTraverseSubtree(aParent, ServoTraversalFlags::UnstyledChildrenOnly);
   // We can't assert that Servo_TraverseSubtree returns false, since aParent
   // or some of its other children might have pending restyles.
 }
 
 void
 ServoStyleSet::StyleNewlyBoundElement(Element* aElement)
 {
-  PreTraverse(ServoTraversalFlags::Empty);
-
   // In general the element is always styled by the time we're applying XBL
   // bindings, because we need to style the element to know what the binding
   // URI is. However, programmatic consumers of the XBL service (like the
   // XML pretty printer) _can_ apply bindings without having styled the bound
   // element. We could assert against this and require the callers manually
   // resolve the style first, but it's easy enough to just handle here.
-
-  ServoTraversalFlags flags = (MOZ_UNLIKELY(!aElement->HasServoData())
-      ? ServoTraversalFlags::Empty
-      : ServoTraversalFlags::UnstyledChildrenOnly);
-
-  PrepareAndTraverseSubtree(aElement, flags);
+  if (MOZ_LIKELY(aElement->HasServoData())) {
+    StyleNewChildren(aElement);
+  } else {
+    StyleNewSubtree(aElement);
+  }
 }
 
 void
 ServoStyleSet::StyleSubtreeForReconstruct(Element* aRoot)
 {
   PreTraverse(ServoTraversalFlags::Empty, aRoot);
 
   auto flags = ServoTraversalFlags::Forgetful |
