# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1519396343 0
# Node ID ff135d4dc797776720a4aa61436ba9b3e55d33de
# Parent  461870a693cb3119b3dcdb23b6c0fc40335236d8
Bug 1440656 - Update brotli library to upstream release 1.0.2. r=fredw

diff --git a/modules/brotli/README.mozilla b/modules/brotli/README.mozilla
--- a/modules/brotli/README.mozilla
+++ b/modules/brotli/README.mozilla
@@ -9,9 +9,9 @@ Upstream code can be viewed at
 
 and cloned by
   git clone https://github.com/google/brotli
 
 The in-tree copy is updated by running
   sh update.sh
 from within the modules/brotli directory.
 
-Current version: [commit 5b4769990dc14a2bd466d2599c946c5652cba4b2].
+Current version: [commit 0ad94eed00420bf1154cb16a289aa27efbb30c01].
diff --git a/modules/brotli/common/dictionary.h b/modules/brotli/common/dictionary.h
--- a/modules/brotli/common/dictionary.h
+++ b/modules/brotli/common/dictionary.h
@@ -22,23 +22,23 @@ typedef struct BrotliDictionary {
    *
    * Specification: Appendix A. Static Dictionary Data
    *
    * Words in a dictionary are bucketed by length.
    * @c 0 means that there are no words of a given length.
    * Dictionary consists of words with length of [4..24] bytes.
    * Values at [0..3] and [25..31] indices should not be addressed.
    */
-  const uint8_t size_bits_by_length[32];
+  uint8_t size_bits_by_length[32];
 
   /* assert(offset[i + 1] == offset[i] + (bits[i] ? (i << bits[i]) : 0)) */
-  const uint32_t offsets_by_length[32];
+  uint32_t offsets_by_length[32];
 
   /* assert(data_size == offsets_by_length[31]) */
-  const size_t data_size;
+  size_t data_size;
 
   /* Data array is not bound, and should obey to size_bits_by_length values.
      Specified size matches default (RFC 7932) dictionary. Its size is
      defined by data_size */
   const uint8_t* data;
 } BrotliDictionary;
 
 BROTLI_COMMON_API extern const BrotliDictionary* BrotliGetDictionary(void);
diff --git a/modules/brotli/common/version.h b/modules/brotli/common/version.h
--- a/modules/brotli/common/version.h
+++ b/modules/brotli/common/version.h
@@ -9,11 +9,18 @@
 #ifndef BROTLI_COMMON_VERSION_H_
 #define BROTLI_COMMON_VERSION_H_
 
 /* This macro should only be used when library is compiled together with client.
    If library is dynamically linked, use BrotliDecoderVersion and
    BrotliEncoderVersion methods. */
 
 /* Semantic version, calculated as (MAJOR << 24) | (MINOR << 12) | PATCH */
-#define BROTLI_VERSION 0x1000001
+#define BROTLI_VERSION 0x1000002
+
+/* This macro is used by build system to produce Libtool-friendly soname. See
+   https://www.gnu.org/software/libtool/manual/html_node/Libtool-versioning.html
+ */
+
+/* ABI version, calculated as (CURRENT << 24) | (REVISION << 12) | AGE */
+#define BROTLI_ABI_VERSION 0x1002000
 
 #endif  /* BROTLI_COMMON_VERSION_H_ */
diff --git a/modules/brotli/dec/decode.c b/modules/brotli/dec/decode.c
--- a/modules/brotli/dec/decode.c
+++ b/modules/brotli/dec/decode.c
@@ -1906,16 +1906,20 @@ BrotliDecoderResult BrotliDecoderDecompr
     * when result is "success" decoder MUST return all unused data back to input
       buffer; this is possible because the invariant is hold on enter
 */
 BrotliDecoderResult BrotliDecoderDecompressStream(
     BrotliDecoderState* s, size_t* available_in, const uint8_t** next_in,
     size_t* available_out, uint8_t** next_out, size_t* total_out) {
   BrotliDecoderErrorCode result = BROTLI_DECODER_SUCCESS;
   BrotliBitReader* br = &s->br;
+  /* Ensure that *total_out is set, even if no data will ever be pushed out. */
+  if (total_out) {
+    *total_out = s->partial_pos_out;
+  }
   /* Do not try to process further in a case of unrecoverable error. */
   if ((int)s->error_code < 0) {
     return BROTLI_DECODER_RESULT_ERROR;
   }
   if (*available_out && (!next_out || !*next_out)) {
     return SaveErrorCode(
         s, BROTLI_FAILURE(BROTLI_DECODER_ERROR_INVALID_ARGUMENTS));
   }
diff --git a/modules/brotli/enc/backward_references_hq.c b/modules/brotli/enc/backward_references_hq.c
--- a/modules/brotli/enc/backward_references_hq.c
+++ b/modules/brotli/enc/backward_references_hq.c
@@ -49,21 +49,21 @@ static BROTLI_INLINE uint32_t ZopfliNode
 }
 
 static BROTLI_INLINE uint32_t ZopfliNodeLengthCode(const ZopfliNode* self) {
   const uint32_t modifier = self->length >> 24;
   return ZopfliNodeCopyLength(self) + 9u - modifier;
 }
 
 static BROTLI_INLINE uint32_t ZopfliNodeCopyDistance(const ZopfliNode* self) {
-  return self->distance & 0x1ffffff;
+  return self->distance & 0x7ffffff;
 }
 
 static BROTLI_INLINE uint32_t ZopfliNodeDistanceCode(const ZopfliNode* self) {
-  const uint32_t short_code = self->distance >> 25;
+  const uint32_t short_code = self->distance >> 27;
   return short_code == 0 ?
       ZopfliNodeCopyDistance(self) + BROTLI_NUM_DISTANCE_SHORT_CODES - 1 :
       short_code - 1;
 }
 
 static BROTLI_INLINE uint32_t ZopfliNodeCommandLength(const ZopfliNode* self) {
   return ZopfliNodeCopyLength(self) + self->insert_length;
 }
@@ -217,17 +217,17 @@ static BROTLI_INLINE float ZopfliCostMod
 /* REQUIRES: len >= 2, start_pos <= pos */
 /* REQUIRES: cost < kInfinity, nodes[start_pos].cost < kInfinity */
 /* Maintains the "ZopfliNode array invariant". */
 static BROTLI_INLINE void UpdateZopfliNode(ZopfliNode* nodes, size_t pos,
     size_t start_pos, size_t len, size_t len_code, size_t dist,
     size_t short_code, float cost) {
   ZopfliNode* next = &nodes[pos + len];
   next->length = (uint32_t)(len | ((len + 9u - len_code) << 24));
-  next->distance = (uint32_t)(dist | (short_code << 25));
+  next->distance = (uint32_t)(dist | (short_code << 27));
   next->insert_length = (uint32_t)(pos - start_pos);
   next->u.cost = cost;
 }
 
 typedef struct PosData {
   size_t pos;
   int distance_cache[4];
   float costdiff;
@@ -555,17 +555,16 @@ void BrotliZopfliCreateCommands(const si
     }
     {
       size_t distance = ZopfliNodeCopyDistance(next);
       size_t len_code = ZopfliNodeLengthCode(next);
       size_t max_distance =
           BROTLI_MIN(size_t, block_start + pos, max_backward_limit);
       BROTLI_BOOL is_dictionary = TO_BROTLI_BOOL(distance > max_distance + gap);
       size_t dist_code = ZopfliNodeDistanceCode(next);
-
       InitCommand(&commands[i], insert_length,
           copy_length, (int)len_code - (int)copy_length, dist_code);
 
       if (!is_dictionary && dist_code > 0) {
         dist_cache[3] = dist_cache[2];
         dist_cache[2] = dist_cache[1];
         dist_cache[1] = dist_cache[0];
         dist_cache[0] = (int)distance;
@@ -633,34 +632,35 @@ size_t BrotliZopfliComputeShortestPath(M
                                        const BrotliEncoderParams* params,
                                        const size_t max_backward_limit,
                                        const int* dist_cache,
                                        HasherHandle hasher,
                                        ZopfliNode* nodes) {
   const size_t max_zopfli_len = MaxZopfliLen(params);
   ZopfliCostModel model;
   StartPosQueue queue;
-  BackwardMatch matches[MAX_NUM_MATCHES_H10];
+  BackwardMatch matches[2 * (MAX_NUM_MATCHES_H10 + 64)];
   const size_t store_end = num_bytes >= StoreLookaheadH10() ?
       position + num_bytes - StoreLookaheadH10() + 1 : position;
   size_t i;
   size_t gap = 0;
+  size_t lz_matches_offset = 0;
   nodes[0].length = 0;
   nodes[0].u.cost = 0;
   InitZopfliCostModel(m, &model, num_bytes);
   if (BROTLI_IS_OOM(m)) return 0;
   ZopfliCostModelSetFromLiteralCosts(
       &model, position, ringbuffer, ringbuffer_mask);
   InitStartPosQueue(&queue);
   for (i = 0; i + HashTypeLengthH10() - 1 < num_bytes; i++) {
     const size_t pos = position + i;
     const size_t max_distance = BROTLI_MIN(size_t, pos, max_backward_limit);
     size_t num_matches = FindAllMatchesH10(hasher, dictionary, ringbuffer,
         ringbuffer_mask, pos, num_bytes - i, max_distance, gap, params,
-        matches);
+        &matches[lz_matches_offset]);
     size_t skip;
     if (num_matches > 0 &&
         BackwardMatchLength(&matches[num_matches - 1]) > max_zopfli_len) {
       matches[0] = matches[num_matches - 1];
       num_matches = 1;
     }
     skip = UpdateNodes(num_bytes, position, i, ringbuffer, ringbuffer_mask,
         params, max_backward_limit, dist_cache, num_matches, matches, &model,
@@ -723,31 +723,32 @@ void BrotliCreateHqZopfliBackwardReferen
   size_t orig_num_literals;
   size_t orig_last_insert_len;
   int orig_dist_cache[4];
   size_t orig_num_commands;
   ZopfliCostModel model;
   ZopfliNode* nodes;
   BackwardMatch* matches = BROTLI_ALLOC(m, BackwardMatch, matches_size);
   size_t gap = 0;
+  size_t shadow_matches = 0;
   if (BROTLI_IS_OOM(m)) return;
   for (i = 0; i + HashTypeLengthH10() - 1 < num_bytes; ++i) {
     const size_t pos = position + i;
     size_t max_distance = BROTLI_MIN(size_t, pos, max_backward_limit);
     size_t max_length = num_bytes - i;
     size_t num_found_matches;
     size_t cur_match_end;
     size_t j;
     /* Ensure that we have enough free slots. */
     BROTLI_ENSURE_CAPACITY(m, BackwardMatch, matches, matches_size,
-        cur_match_pos + MAX_NUM_MATCHES_H10);
+        cur_match_pos + MAX_NUM_MATCHES_H10 + shadow_matches);
     if (BROTLI_IS_OOM(m)) return;
     num_found_matches = FindAllMatchesH10(hasher, dictionary, ringbuffer,
         ringbuffer_mask, pos, max_length, max_distance, gap, params,
-        &matches[cur_match_pos]);
+        &matches[cur_match_pos + shadow_matches]);
     cur_match_end = cur_match_pos + num_found_matches;
     for (j = cur_match_pos; j + 1 < cur_match_end; ++j) {
       assert(BackwardMatchLength(&matches[j]) <=
           BackwardMatchLength(&matches[j + 1]));
     }
     num_matches[i] = (uint32_t)num_found_matches;
     if (num_found_matches > 0) {
       const size_t match_len = BackwardMatchLength(&matches[cur_match_end - 1]);
diff --git a/modules/brotli/enc/backward_references_hq.h b/modules/brotli/enc/backward_references_hq.h
--- a/modules/brotli/enc/backward_references_hq.h
+++ b/modules/brotli/enc/backward_references_hq.h
@@ -35,19 +35,19 @@ BROTLI_INTERNAL void BrotliCreateHqZopfl
     const BrotliEncoderParams* params, HasherHandle hasher, int* dist_cache,
     size_t* last_insert_len, Command* commands, size_t* num_commands,
     size_t* num_literals);
 
 typedef struct ZopfliNode {
   /* best length to get up to this byte (not including this byte itself)
      highest 8 bit is used to reconstruct the length code */
   uint32_t length;
-  /* distance associated with the length
-     highest 7 bit contains distance short code + 1 (or zero if no short code)
-  */
+  /* distance associated with the length; highest 5 bits contain distance
+     short code + 1 (or zero if no short code); this way only distances shorter
+     than 128MiB are allowed here */
   uint32_t distance;
   /* number of literal inserts before this copy */
   uint32_t insert_length;
 
   /* This union holds information used by dynamic-programming. During forward
      pass |cost| it used to store the goal function. When node is processed its
      |cost| is invalidated in favor of |shortcut|. On path back-tracing pass
      |next| is assigned the offset to next node on the path. */
diff --git a/modules/brotli/enc/port.h b/modules/brotli/enc/port.h
--- a/modules/brotli/enc/port.h
+++ b/modules/brotli/enc/port.h
@@ -162,23 +162,31 @@ TEMPLATE_(size_t) TEMPLATE_(uint32_t) TE
 #define BROTLI_MAX(T, A, B) (brotli_max_ ## T((A), (B)))
 
 #define BROTLI_SWAP(T, A, I, J) { \
   T __brotli_swap_tmp = (A)[(I)]; \
   (A)[(I)] = (A)[(J)];            \
   (A)[(J)] = __brotli_swap_tmp;   \
 }
 
+/*
+Dynamically grows array capacity to at least the requested size
+M: MemoryManager
+T: data type
+A: array
+C: capacity
+R: requested size
+*/
 #define BROTLI_ENSURE_CAPACITY(M, T, A, C, R) {  \
   if (C < (R)) {                                 \
     size_t _new_size = (C == 0) ? (R) : C;       \
     T* new_array;                                \
     while (_new_size < (R)) _new_size *= 2;      \
     new_array = BROTLI_ALLOC((M), T, _new_size); \
-    if (!BROTLI_IS_OOM(m) && C != 0)             \
+    if (!BROTLI_IS_OOM(M) && C != 0)             \
       memcpy(new_array, A, C * sizeof(T));       \
     BROTLI_FREE((M), A);                         \
     A = new_array;                               \
     C = _new_size;                               \
   }                                              \
 }
 
 #endif  /* BROTLI_ENC_PORT_H_ */
diff --git a/modules/brotli/enc/quality.h b/modules/brotli/enc/quality.h
--- a/modules/brotli/enc/quality.h
+++ b/modules/brotli/enc/quality.h
@@ -6,16 +6,17 @@
 
 /* Constants and formulas that affect speed-ratio trade-offs and thus define
    quality levels. */
 
 #ifndef BROTLI_ENC_QUALITY_H_
 #define BROTLI_ENC_QUALITY_H_
 
 #include <brotli/encode.h>
+#include "./port.h"
 
 #define FAST_ONE_PASS_COMPRESSION_QUALITY 0
 #define FAST_TWO_PASS_COMPRESSION_QUALITY 1
 #define ZOPFLIFICATION_QUALITY 10
 #define HQ_ZOPFLIFICATION_QUALITY 11
 
 #define MAX_QUALITY_FOR_STATIC_ENTROPY_CODES 2
 #define MIN_QUALITY_FOR_BLOCK_SPLIT 4
diff --git a/modules/brotli/include/brotli/decode.h b/modules/brotli/include/brotli/decode.h
--- a/modules/brotli/include/brotli/decode.h
+++ b/modules/brotli/include/brotli/decode.h
@@ -154,20 +154,21 @@ BROTLI_DEC_API BROTLI_BOOL BrotliDecoder
  * Creates an instance of ::BrotliDecoderState and initializes it.
  *
  * The instance can be used once for decoding and should then be destroyed with
  * ::BrotliDecoderDestroyInstance, it cannot be reused for a new decoding
  * session.
  *
  * @p alloc_func and @p free_func @b MUST be both zero or both non-zero. In the
  * case they are both zero, default memory allocators are used. @p opaque is
- * passed to @p alloc_func and @p free_func when they are called.
+ * passed to @p alloc_func and @p free_func when they are called. @p free_func
+ * should return without doing anything when asked to free a NULL pointer.
  *
  * @param alloc_func custom memory allocation function
- * @param free_func custom memory fee function
+ * @param free_func custom memory free function
  * @param opaque custom memory manager handle
  * @returns @c 0 if instance can not be allocated or initialized
  * @returns pointer to initialized ::BrotliDecoderState otherwise
  */
 BROTLI_DEC_API BrotliDecoderState* BrotliDecoderCreateInstance(
     brotli_alloc_func alloc_func, brotli_free_func free_func, void* opaque);
 
 /**
diff --git a/modules/brotli/include/brotli/encode.h b/modules/brotli/include/brotli/encode.h
--- a/modules/brotli/include/brotli/encode.h
+++ b/modules/brotli/include/brotli/encode.h
@@ -204,20 +204,21 @@ typedef struct BrotliEncoderStateStruct 
 BROTLI_ENC_API BROTLI_BOOL BrotliEncoderSetParameter(
     BrotliEncoderState* state, BrotliEncoderParameter param, uint32_t value);
 
 /**
  * Creates an instance of ::BrotliEncoderState and initializes it.
  *
  * @p alloc_func and @p free_func @b MUST be both zero or both non-zero. In the
  * case they are both zero, default memory allocators are used. @p opaque is
- * passed to @p alloc_func and @p free_func when they are called.
+ * passed to @p alloc_func and @p free_func when they are called. @p free_func
+ * should return without doing anything when asked to free a NULL pointer.
  *
  * @param alloc_func custom memory allocation function
- * @param free_func custom memory fee function
+ * @param free_func custom memory free function
  * @param opaque custom memory manager handle
  * @returns @c 0 if instance can not be allocated or initialized
  * @returns pointer to initialized ::BrotliEncoderState otherwise
  */
 BROTLI_ENC_API BrotliEncoderState* BrotliEncoderCreateInstance(
     brotli_alloc_func alloc_func, brotli_free_func free_func, void* opaque);
 
 /**
@@ -278,17 +279,17 @@ BROTLI_ENC_API BROTLI_BOOL BrotliEncoder
  *
  * After each call, @p *available_in will be decremented by the amount of input
  * bytes consumed, and the @p *next_in pointer will be incremented by that
  * amount. Similarly, @p *available_out will be decremented by the amount of
  * output bytes written, and the @p *next_out pointer will be incremented by
  * that amount.
  *
  * @p total_out, if it is not a null-pointer, will be set to the number
- * of bytes decompressed since the last @p state initialization.
+ * of bytes compressed since the last @p state initialization.
  *
  *
  *
  * Internally workflow consists of 3 tasks:
  *  -# (optionally) copy input data to internal buffer
  *  -# actually compress data and (optionally) store it to internal buffer
  *  -# (optionally) copy compressed bytes from internal buffer to output stream
  *
diff --git a/modules/brotli/update.sh b/modules/brotli/update.sh
--- a/modules/brotli/update.sh
+++ b/modules/brotli/update.sh
@@ -1,17 +1,17 @@
 #!/bin/sh
 
 # Script to update the mozilla in-tree copy of the Brotli decompressor.
 # Run this within the /modules/brotli directory of the source tree.
 
 MY_TEMP_DIR=`mktemp -d -t brotli_update.XXXXXX` || exit 1
 
 git clone https://github.com/google/brotli ${MY_TEMP_DIR}/brotli
-git -C ${MY_TEMP_DIR}/brotli checkout v1.0.1
+git -C ${MY_TEMP_DIR}/brotli checkout v1.0.2
 
 COMMIT=$(git -C ${MY_TEMP_DIR}/brotli rev-parse HEAD)
 perl -p -i -e "s/\[commit [0-9a-f]{40}\]/[commit ${COMMIT}]/" README.mozilla;
 
 DIRS="common dec enc include tools"
 
 for d in $DIRS; do
 	rm -rf $d
