# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1507559978 14400
# Node ID 1df1edaf60c3126f70c31328411547d3caf2165b
# Parent  1779da025280d3521be05f5c4275095f6224d2f6
Bug 1406486 - provide nsClientAuthRememberEntry/nsCertOverrideEntry with move constructors; r=keeler

Move constructors are more appropriate for these classes, since the
underlying hashtable code will be moving them around, not copying them.
We can take this opportunity to fix a bug in nsClientAuthRememberEntry:
it wasn't transferring the value of mEntryKey, which would have been
disastrous if the underlying hash table was ever resized.

diff --git a/security/manager/ssl/nsCertOverrideService.h b/security/manager/ssl/nsCertOverrideService.h
--- a/security/manager/ssl/nsCertOverrideService.h
+++ b/security/manager/ssl/nsCertOverrideService.h
@@ -2,16 +2,17 @@
  *
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsCertOverrideService_h
 #define nsCertOverrideService_h
 
+#include "mozilla/Move.h"
 #include "mozilla/Mutex.h"
 #include "mozilla/TypedEnumBits.h"
 #include "nsICertOverrideService.h"
 #include "nsIFile.h"
 #include "nsIObserver.h"
 #include "nsString.h"
 #include "nsTHashtable.h"
 #include "nsWeakReference.h"
@@ -74,20 +75,20 @@ class nsCertOverrideEntry final : public
     typedef const char* KeyType;
     typedef const char* KeyTypePointer;
 
     // do nothing with aHost - we require mHead to be set before we're live!
     explicit nsCertOverrideEntry(KeyTypePointer aHostWithPortUTF8)
     {
     }
 
-    nsCertOverrideEntry(const nsCertOverrideEntry& toCopy)
+    nsCertOverrideEntry(nsCertOverrideEntry&& toMove)
+      : mSettings(mozilla::Move(toMove.mSettings))
+      , mHostWithPort(mozilla::Move(toMove.mHostWithPort))
     {
-      mSettings = toCopy.mSettings;
-      mHostWithPort = toCopy.mHostWithPort;
     }
 
     ~nsCertOverrideEntry()
     {
     }
 
     KeyType GetKey() const
     {
diff --git a/security/manager/ssl/nsClientAuthRemember.h b/security/manager/ssl/nsClientAuthRemember.h
--- a/security/manager/ssl/nsClientAuthRemember.h
+++ b/security/manager/ssl/nsClientAuthRemember.h
@@ -2,16 +2,17 @@
  *
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef __NSCLIENTAUTHREMEMBER_H__
 #define __NSCLIENTAUTHREMEMBER_H__
 
+#include "mozilla/Move.h"
 #include "mozilla/ReentrantMonitor.h"
 #include "nsTHashtable.h"
 #include "nsIObserver.h"
 #include "nsIX509Cert.h"
 #include "nsNSSCertificate.h"
 #include "nsString.h"
 #include "nsWeakReference.h"
 #include "mozilla/Attributes.h"
@@ -57,19 +58,20 @@ class nsClientAuthRememberEntry final : 
     typedef const char* KeyType;
     typedef const char* KeyTypePointer;
 
     // do nothing with aHost - we require mHead to be set before we're live!
     explicit nsClientAuthRememberEntry(KeyTypePointer aHostWithCertUTF8)
     {
     }
 
-    nsClientAuthRememberEntry(const nsClientAuthRememberEntry& aToCopy)
+    nsClientAuthRememberEntry(nsClientAuthRememberEntry&& aToMove)
+      : mSettings(mozilla::Move(aToMove.mSettings))
+      , mEntryKey(mozilla::Move(aToMove.mEntryKey))
     {
-      mSettings = aToCopy.mSettings;
     }
 
     ~nsClientAuthRememberEntry()
     {
     }
 
     KeyType GetKey() const
     {

