# HG changeset patch
# User Bill McCloskey <billm@mozilla.com>
# Date 1500506775 25200
#      Wed Jul 19 16:26:15 2017 -0700
# Node ID f7ed5293a8cd95686240388a11f3f999ca75e7e9
# Parent  32b40e163965abb7e63c6dda6e22b47c844ca387
Bug 1391865 - Use same PBackground for all cooperative threads (r=bevis)

MozReview-Commit-ID: 5Fgp42o8zbE

diff --git a/ipc/glue/BackgroundImpl.cpp b/ipc/glue/BackgroundImpl.cpp
--- a/ipc/glue/BackgroundImpl.cpp
+++ b/ipc/glue/BackgroundImpl.cpp
@@ -315,16 +315,21 @@ class ChildImpl final : public Backgroun
 
     RefPtr<ChildImpl> mActor;
     nsAutoPtr<BackgroundChildImpl::ThreadLocal> mConsumerThreadLocal;
 #ifdef DEBUG
     bool mClosed;
 #endif
   };
 
+  // On the main thread, we store TLS in this global instead of in
+  // sThreadLocalIndex. That way, cooperative main threads all share the same
+  // thread info.
+  static ThreadLocalInfo* sMainThreadInfo;
+
   // This is only modified on the main thread. It prevents us from trying to
   // create the background thread after application shutdown has started.
   static bool sShutdownHasStarted;
 
 #if defined(DEBUG) || !defined(RELEASE_OR_BETA)
   nsISerialEventTarget* mOwningEventTarget;
 #endif
 
@@ -1441,39 +1446,45 @@ ChildImpl::Shutdown()
   if (sShutdownHasStarted) {
     MOZ_ASSERT_IF(sThreadLocalIndex != kBadThreadLocalIndex,
                   !PR_GetThreadPrivate(sThreadLocalIndex));
     return;
   }
 
   sShutdownHasStarted = true;
 
-#ifdef DEBUG
   MOZ_ASSERT(sThreadLocalIndex != kBadThreadLocalIndex);
 
-  auto threadLocalInfo =
-    static_cast<ThreadLocalInfo*>(PR_GetThreadPrivate(sThreadLocalIndex));
+  ThreadLocalInfo* threadLocalInfo;
+#ifdef DEBUG
+  threadLocalInfo = static_cast<ThreadLocalInfo*>(PR_GetThreadPrivate(sThreadLocalIndex));
+  MOZ_ASSERT(!threadLocalInfo);
+#endif
+  threadLocalInfo = sMainThreadInfo;
 
   if (threadLocalInfo) {
+#ifdef DEBUG
     MOZ_ASSERT(!threadLocalInfo->mClosed);
     threadLocalInfo->mClosed = true;
-  }
 #endif
 
-  DebugOnly<PRStatus> status = PR_SetThreadPrivate(sThreadLocalIndex, nullptr);
-  MOZ_ASSERT(status == PR_SUCCESS);
+    ThreadLocalDestructor(threadLocalInfo);
+  }
 }
 
+ChildImpl::ThreadLocalInfo* ChildImpl::sMainThreadInfo = nullptr;
+
 // static
 PBackgroundChild*
 ChildImpl::GetForCurrentThread()
 {
   MOZ_ASSERT(sThreadLocalIndex != kBadThreadLocalIndex);
 
   auto threadLocalInfo =
+    NS_IsMainThread() ? sMainThreadInfo :
     static_cast<ThreadLocalInfo*>(PR_GetThreadPrivate(sThreadLocalIndex));
 
   if (!threadLocalInfo) {
     return nullptr;
   }
 
   return threadLocalInfo->mActor;
 }
@@ -1499,25 +1510,29 @@ ChildImpl::GetOrCreateForCurrentThread(
 
 /* static */
 PBackgroundChild*
 ChildImpl::GetOrCreateForCurrentThread()
 {
   MOZ_ASSERT(sThreadLocalIndex != kBadThreadLocalIndex,
              "BackgroundChild::Startup() was never called!");
 
-  auto threadLocalInfo =
+  auto threadLocalInfo = NS_IsMainThread() ? sMainThreadInfo :
     static_cast<ThreadLocalInfo*>(PR_GetThreadPrivate(sThreadLocalIndex));
 
   if (!threadLocalInfo) {
     nsAutoPtr<ThreadLocalInfo> newInfo(new ThreadLocalInfo());
 
-    if (PR_SetThreadPrivate(sThreadLocalIndex, newInfo) != PR_SUCCESS) {
-      CRASH_IN_CHILD_PROCESS("PR_SetThreadPrivate failed!");
-      return nullptr;
+    if (NS_IsMainThread()) {
+      sMainThreadInfo = newInfo;
+    } else {
+      if (PR_SetThreadPrivate(sThreadLocalIndex, newInfo) != PR_SUCCESS) {
+        CRASH_IN_CHILD_PROCESS("PR_SetThreadPrivate failed!");
+        return nullptr;
+      }
     }
 
     threadLocalInfo = newInfo.forget();
   }
 
   if (threadLocalInfo->mActor) {
     return threadLocalInfo->mActor;
   }
@@ -1584,16 +1599,19 @@ ChildImpl::GetOrCreateForCurrentThread()
 
   return actor;
 }
 
 // static
 void
 ChildImpl::CloseForCurrentThread()
 {
+  MOZ_ASSERT(!NS_IsMainThread(),
+             "PBackground for the main thread should be shut down via ChildImpl::Shutdown().");
+
   if (sThreadLocalIndex == kBadThreadLocalIndex) {
     return;
   }
 
   auto threadLocalInfo =
     static_cast<ThreadLocalInfo*>(PR_GetThreadPrivate(sThreadLocalIndex));
 
   if (!threadLocalInfo) {
@@ -1612,17 +1630,17 @@ ChildImpl::CloseForCurrentThread()
 
 // static
 BackgroundChildImpl::ThreadLocal*
 ChildImpl::GetThreadLocalForCurrentThread()
 {
   MOZ_ASSERT(sThreadLocalIndex != kBadThreadLocalIndex,
              "BackgroundChild::Startup() was never called!");
 
-  auto threadLocalInfo =
+  auto threadLocalInfo = NS_IsMainThread() ? sMainThreadInfo :
     static_cast<ThreadLocalInfo*>(PR_GetThreadPrivate(sThreadLocalIndex));
 
   if (!threadLocalInfo) {
     return nullptr;
   }
 
   if (!threadLocalInfo->mConsumerThreadLocal) {
     threadLocalInfo->mConsumerThreadLocal =
