# HG changeset patch
# User Bryce Seager van Dyk <bvandyk@mozilla.com>
# Date 1588191085 0
# Node ID 239cd9654b8659b22a751f8018c93e32873f3e62
# Parent  7470f02403bec30ca2c269a63bb83f7ec632ea24
Bug 1632717 - r=jwalden, a=jcristau

Differential Revision: https://phabricator.services.mozilla.com/D72476

diff --git a/dom/media/eme/EMEUtils.cpp b/dom/media/eme/EMEUtils.cpp
--- a/dom/media/eme/EMEUtils.cpp
+++ b/dom/media/eme/EMEUtils.cpp
@@ -1,15 +1,17 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/EMEUtils.h"
+
+#include "jsfriendapi.h"
 #include "mozilla/dom/UnionTypes.h"
 
 namespace mozilla {
 
 LogModule* GetEMELog() {
   static LazyLogModule log("EME");
   return log;
 }
@@ -18,40 +20,49 @@ LogModule* GetEMEVerboseLog() {
   static LazyLogModule log("EMEV");
   return log;
 }
 
 ArrayData
 GetArrayBufferViewOrArrayBufferData(const dom::ArrayBufferViewOrArrayBuffer& aBufferOrView)
 {
   MOZ_ASSERT(aBufferOrView.IsArrayBuffer() || aBufferOrView.IsArrayBufferView());
+  JS::AutoCheckCannotGC nogc;
   if (aBufferOrView.IsArrayBuffer()) {
     const dom::ArrayBuffer& buffer = aBufferOrView.GetAsArrayBuffer();
     buffer.ComputeLengthAndData();
     return ArrayData(buffer.Data(), buffer.Length());
   } else if (aBufferOrView.IsArrayBufferView()) {
     const dom::ArrayBufferView& bufferview = aBufferOrView.GetAsArrayBufferView();
     bufferview.ComputeLengthAndData();
     return ArrayData(bufferview.Data(), bufferview.Length());
   }
   return ArrayData(nullptr, 0);
 }
 
 void
 CopyArrayBufferViewOrArrayBufferData(const dom::ArrayBufferViewOrArrayBuffer& aBufferOrView,
-                                     nsTArray<uint8_t>& aOutData)
-{
+                                     nsTArray<uint8_t>& aOutData) {
+  JS::AutoCheckCannotGC nogc;
   ArrayData data = GetArrayBufferViewOrArrayBufferData(aBufferOrView);
   aOutData.Clear();
   if (!data.IsValid()) {
     return;
   }
   aOutData.AppendElements(data.mData, data.mLength);
 }
 
+void CopyArrayBufferViewOrArrayBufferData(const dom::ArrayBuffer& aBuffer,
+                                          nsTArray<uint8_t>& aOutData) {
+  JS::AutoCheckCannotGC nogc;
+  aBuffer.ComputeLengthAndData();
+  aOutData.Clear();
+  aOutData.AppendElements(aBuffer.Data(), aBuffer.Length());
+}
+
 bool
 IsClearkeyKeySystem(const nsAString& aKeySystem)
 {
   return !CompareUTF8toUTF16(kEMEKeySystemClearkey, aKeySystem);
 }
 
 bool
 IsWidevineKeySystem(const nsAString& aKeySystem)
diff --git a/dom/media/eme/EMEUtils.h b/dom/media/eme/EMEUtils.h
--- a/dom/media/eme/EMEUtils.h
+++ b/dom/media/eme/EMEUtils.h
@@ -39,16 +39,20 @@ class ArrayBufferViewOrArrayBuffer;
 
 // Helper function to extract a copy of data coming in from JS in an
 // (ArrayBuffer or ArrayBufferView) IDL typed function argument.
 //
 // Only call this on a properly initialized ArrayBufferViewOrArrayBuffer.
 void
 CopyArrayBufferViewOrArrayBufferData(const dom::ArrayBufferViewOrArrayBuffer& aBufferOrView,
                                      nsTArray<uint8_t>& aOutData);
+ 
+// Overload for ArrayBuffer
+void CopyArrayBufferViewOrArrayBufferData(const dom::ArrayBuffer& aBufferOrView,
+                                          nsTArray<uint8_t>& aOutData);
 
 struct ArrayData {
   explicit ArrayData(const uint8_t* aData, size_t aLength)
     : mData(aData)
     , mLength(aLength)
   {
   }
   const uint8_t* mData;
diff --git a/dom/media/eme/MediaEncryptedEvent.cpp b/dom/media/eme/MediaEncryptedEvent.cpp
--- a/dom/media/eme/MediaEncryptedEvent.cpp
+++ b/dom/media/eme/MediaEncryptedEvent.cpp
@@ -82,20 +82,20 @@ MediaEncryptedEvent::Constructor(const G
 {
   nsCOMPtr<EventTarget> owner = do_QueryInterface(aGlobal.GetAsSupports());
   RefPtr<MediaEncryptedEvent> e = new MediaEncryptedEvent(owner);
   bool trusted = e->Init(owner);
   e->InitEvent(aType, aEventInitDict.mBubbles, aEventInitDict.mCancelable);
   e->mInitDataType = aEventInitDict.mInitDataType;
   if (!aEventInitDict.mInitData.IsNull()) {
     const auto& a = aEventInitDict.mInitData.Value();
-    a.ComputeLengthAndData();
-    e->mInitData = ArrayBuffer::Create(aGlobal.Context(),
-                                       a.Length(),
-                                       a.Data());
+    nsTArray<uint8_t> initData;
+    CopyArrayBufferViewOrArrayBufferData(a, initData);
+    e->mInitData = ArrayBuffer::Create(aGlobal.Context(), initData.Length(),
+                                       initData.Elements());
     if (!e->mInitData) {
       aRv.Throw(NS_ERROR_OUT_OF_MEMORY);
       return nullptr;
     }
   }
   e->SetTrusted(trusted);
   return e.forget();
 }
diff --git a/dom/media/eme/MediaKeyMessageEvent.cpp b/dom/media/eme/MediaKeyMessageEvent.cpp
--- a/dom/media/eme/MediaKeyMessageEvent.cpp
+++ b/dom/media/eme/MediaKeyMessageEvent.cpp
@@ -80,20 +80,20 @@ MediaKeyMessageEvent::Constructor(const 
                                   const nsAString& aType,
                                   const MediaKeyMessageEventInit& aEventInitDict,
                                   ErrorResult& aRv)
 {
   nsCOMPtr<EventTarget> owner = do_QueryInterface(aGlobal.GetAsSupports());
   RefPtr<MediaKeyMessageEvent> e = new MediaKeyMessageEvent(owner);
   bool trusted = e->Init(owner);
   e->InitEvent(aType, aEventInitDict.mBubbles, aEventInitDict.mCancelable);
-  aEventInitDict.mMessage.ComputeLengthAndData();
-  e->mMessage = ArrayBuffer::Create(aGlobal.Context(),
-                                    aEventInitDict.mMessage.Length(),
-                                    aEventInitDict.mMessage.Data());
+  nsTArray<uint8_t> initData;
+  CopyArrayBufferViewOrArrayBufferData(aEventInitDict.mMessage, initData);
+  e->mMessage = ArrayBuffer::Create(aGlobal.Context(), initData.Length(),
+                                    initData.Elements());
   if (!e->mMessage) {
     aRv.Throw(NS_ERROR_OUT_OF_MEMORY);
     return nullptr;
   }
   e->mMessageType = aEventInitDict.mMessageType;
   e->SetTrusted(trusted);
   e->SetComposed(aEventInitDict.mComposed);
   return e.forget();
