# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1503574609 -7200
#      Thu Aug 24 13:36:49 2017 +0200
# Node ID a71d0947dfd6fbb8af224facfcaa570c3e1f3a62
# Parent  99aaaed43621023a510a7a1559b55c0da10efb31
Bug 1146817 - Make none/AtomicOperations-feeling-lucky.h 64-bit aware, and cleaner. r=sstangl

diff --git a/js/src/jit/none/AtomicOperations-feeling-lucky.h b/js/src/jit/none/AtomicOperations-feeling-lucky.h
--- a/js/src/jit/none/AtomicOperations-feeling-lucky.h
+++ b/js/src/jit/none/AtomicOperations-feeling-lucky.h
@@ -23,24 +23,33 @@
  */
 
 #ifndef jit_none_AtomicOperations_feeling_lucky_h
 #define jit_none_AtomicOperations_feeling_lucky_h
 
 #include "mozilla/Assertions.h"
 #include "mozilla/Types.h"
 
-// 64-bit JS atomics are not required by SpiderMonkey or the JS spec, but define
-// these names if you want to provide them anyway.
+// 64-bit atomics are not required by the JS spec, and you can compile
+// SpiderMonkey without them.
+//
+// 64-bit lock-free atomics are however required for WebAssembly, and
+// WebAssembly will be disabled if you do not define both HAS_64BIT_ATOMICS and
+// HAS_64BIT_LOCKFREE.
+//
+// If you are only able to provide 64-bit non-lock-free atomics and you really
+// want WebAssembly support you can always just lie about the lock-freedom.
+// After all, you're already feeling lucky.
 
 #if defined(__ppc__) || defined(__PPC__)
 #  define GNUC_COMPATIBLE
 #endif
 
 #if defined(__ppc64__) ||  defined (__PPC64__) || defined(__ppc64le__) || defined (__PPC64LE__)
+#  define HAS_64BIT_ATOMICS
 #  define HAS_64BIT_LOCKFREE
 #  define GNUC_COMPATIBLE
 #endif
 
 #ifdef __sparc__
 #  define GNUC_COMPATIBLE
 #  ifdef  __LP64__
 #    define HAS_64BIT_ATOMICS
@@ -67,207 +76,396 @@
 // ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS is kept as a backward
 // compatible option for older compilers: enable this to use GCC's old
 // __sync functions instead of the newer __atomic functions.  This
 // will be required for GCC 4.6.x and earlier, and probably for Clang
 // 3.1, should we need to use those versions.
 
 //#define ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
 
+// Sanity check.
+
+#if defined(HAS_64BIT_LOCKFREE) && !defined(HAS_64BIT_ATOMICS)
+#  error "This combination of features is senseless, please fix"
+#endif
 
 // Try to avoid platform #ifdefs below this point.
 
 #ifdef GNUC_COMPATIBLE
 
 inline bool
 js::jit::AtomicOperations::isLockfree8()
 {
-# ifndef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
-    MOZ_ASSERT(__atomic_always_lock_free(sizeof(int8_t), 0));
-    MOZ_ASSERT(__atomic_always_lock_free(sizeof(int16_t), 0));
-    MOZ_ASSERT(__atomic_always_lock_free(sizeof(int32_t), 0));
-#  ifdef HAS_64BIT_LOCKFREE
-    MOZ_ASSERT(__atomic_always_lock_free(sizeof(int64_t), 0));
-#  endif
+#if defined(HAS_64BIT_LOCKFREE)
     return true;
-# else
+#else
     return false;
-# endif
+#endif
 }
 
 inline void
 js::jit::AtomicOperations::fenceSeqCst()
 {
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     __sync_synchronize();
 # else
     __atomic_thread_fence(__ATOMIC_SEQ_CST);
 # endif
 }
 
 template<typename T>
 inline T
 js::jit::AtomicOperations::loadSeqCst(T* addr)
 {
-    MOZ_ASSERT(sizeof(T) < 8 || isLockfree8());
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     __sync_synchronize();
     T v = *addr;
     __sync_synchronize();
 # else
     T v;
     __atomic_load(addr, &v, __ATOMIC_SEQ_CST);
 # endif
     return v;
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline int64_t
+AtomicOperations::loadSeqCst(int64_t* addr) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline uint64_t
+AtomicOperations::loadSeqCst(uint64_t* addr) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<typename T>
 inline void
 js::jit::AtomicOperations::storeSeqCst(T* addr, T val)
 {
-    MOZ_ASSERT(sizeof(T) < 8 || isLockfree8());
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     __sync_synchronize();
     *addr = val;
     __sync_synchronize();
 # else
     __atomic_store(addr, &val, __ATOMIC_SEQ_CST);
 # endif
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline void
+AtomicOperations::storeSeqCst(int64_t* addr, int64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline void
+AtomicOperations::storeSeqCst(uint64_t* addr, uint64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<typename T>
 inline T
 js::jit::AtomicOperations::compareExchangeSeqCst(T* addr, T oldval, T newval)
 {
-    MOZ_ASSERT(sizeof(T) < 8 || isLockfree8());
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     return __sync_val_compare_and_swap(addr, oldval, newval);
 # else
     __atomic_compare_exchange(addr, &oldval, &newval, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST);
     return oldval;
 # endif
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline int64_t
+AtomicOperations::compareExchangeSeqCst(int64_t* addr, int64_t oldval, int64_t newval) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline uint64_t
+AtomicOperations::compareExchangeSeqCst(uint64_t* addr, uint64_t oldval, uint64_t newval) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<typename T>
 inline T
 js::jit::AtomicOperations::fetchAddSeqCst(T* addr, T val)
 {
-#ifndef HAS_64BIT_ATOMICS
-    static_assert(sizeof(T) <= 4, "not available for 8-byte values yet");
-#endif
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     return __sync_fetch_and_add(addr, val);
 # else
     return __atomic_fetch_add(addr, val, __ATOMIC_SEQ_CST);
 # endif
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline int64_t
+AtomicOperations::fetchAddSeqCst(int64_t* addr, int64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline uint64_t
+AtomicOperations::fetchAddSeqCst(uint64_t* addr, uint64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<typename T>
 inline T
 js::jit::AtomicOperations::fetchSubSeqCst(T* addr, T val)
 {
-#ifndef HAS_64BIT_ATOMICS
-    static_assert(sizeof(T) <= 4, "not available for 8-byte values yet");
-#endif
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     return __sync_fetch_and_sub(addr, val);
 # else
     return __atomic_fetch_sub(addr, val, __ATOMIC_SEQ_CST);
 # endif
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline int64_t
+AtomicOperations::fetchSubSeqCst(int64_t* addr, int64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline uint64_t
+AtomicOperations::fetchSubSeqCst(uint64_t* addr, uint64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<typename T>
 inline T
 js::jit::AtomicOperations::fetchAndSeqCst(T* addr, T val)
 {
-#ifndef HAS_64BIT_ATOMICS
-    static_assert(sizeof(T) <= 4, "not available for 8-byte values yet");
-#endif
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     return __sync_fetch_and_and(addr, val);
 # else
     return __atomic_fetch_and(addr, val, __ATOMIC_SEQ_CST);
 # endif
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline int64_t
+AtomicOperations::fetchAndSeqCst(int64_t* addr, int64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline uint64_t
+AtomicOperations::fetchAndSeqCst(uint64_t* addr, uint64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<typename T>
 inline T
 js::jit::AtomicOperations::fetchOrSeqCst(T* addr, T val)
 {
-#ifndef HAS_64BIT_ATOMICS
-    static_assert(sizeof(T) <= 4, "not available for 8-byte values yet");
-#endif
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     return __sync_fetch_and_or(addr, val);
 # else
     return __atomic_fetch_or(addr, val, __ATOMIC_SEQ_CST);
 # endif
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline int64_t
+AtomicOperations::fetchOrSeqCst(int64_t* addr, int64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline uint64_t
+AtomicOperations::fetchOrSeqCst(uint64_t* addr, uint64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<typename T>
 inline T
 js::jit::AtomicOperations::fetchXorSeqCst(T* addr, T val)
 {
-#ifndef HAS_64BIT_ATOMICS
-    static_assert(sizeof(T) <= 4, "not available for 8-byte values yet");
-#endif
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     return __sync_fetch_and_xor(addr, val);
 # else
     return __atomic_fetch_xor(addr, val, __ATOMIC_SEQ_CST);
 # endif
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline int64_t
+AtomicOperations::fetchXorSeqCst(int64_t* addr, int64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline uint64_t
+AtomicOperations::fetchXorSeqCst(uint64_t* addr, uint64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<typename T>
 inline T
 js::jit::AtomicOperations::loadSafeWhenRacy(T* addr)
 {
-    return *addr;               // FIXME (1208663): not yet safe
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
+    return *addr;
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline int64_t
+AtomicOperations::loadSafeWhenRacy(int64_t* addr) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline uint64_t
+AtomicOperations::loadSafeWhenRacy(uint64_t* addr) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<typename T>
 inline void
 js::jit::AtomicOperations::storeSafeWhenRacy(T* addr, T val)
 {
-    *addr = val;                // FIXME (1208663): not yet safe
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
+    *addr = val;
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline void
+AtomicOperations::storeSafeWhenRacy(int64_t* addr, int64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline void
+AtomicOperations::storeSafeWhenRacy(uint64_t* addr, uint64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 inline void
 js::jit::AtomicOperations::memcpySafeWhenRacy(void* dest, const void* src, size_t nbytes)
 {
     MOZ_ASSERT(!((char*)dest <= (char*)src && (char*)src < (char*)dest+nbytes));
     MOZ_ASSERT(!((char*)src <= (char*)dest && (char*)dest < (char*)src+nbytes));
-    ::memcpy(dest, src, nbytes); // FIXME (1208663): not yet safe
+    ::memcpy(dest, src, nbytes);
 }
 
 inline void
 js::jit::AtomicOperations::memmoveSafeWhenRacy(void* dest, const void* src, size_t nbytes)
 {
-    ::memmove(dest, src, nbytes); // FIXME (1208663): not yet safe
+    ::memmove(dest, src, nbytes);
 }
 
 template<typename T>
 inline T
 js::jit::AtomicOperations::exchangeSeqCst(T* addr, T val)
 {
-    MOZ_ASSERT(sizeof(T) < 8 || isLockfree8());
+    static_assert(sizeof(T) <= 8, "atomics supported up to 8 bytes only");
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     T v;
     __sync_synchronize();
     do {
 	v = *addr;
     } while (__sync_val_compare_and_swap(addr, v, val) != v);
     return v;
 # else
     T v;
     __atomic_exchange(addr, &val, &v, __ATOMIC_SEQ_CST);
     return v;
 # endif
 }
 
+#ifndef HAS_64BIT_ATOMICS
+namespace js { namespace jit {
+
+template<>
+inline int64_t
+AtomicOperations::exchangeSeqCst(int64_t* addr, int64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+template<>
+inline uint64_t
+AtomicOperations::exchangeSeqCst(uint64_t* addr, uint64_t val) {
+    MOZ_CRASH("No 64-bit atomics");
+}
+
+} }
+#endif
+
 template<size_t nbytes>
 inline void
 js::jit::RegionLock::acquire(void* addr)
 {
 # ifdef ATOMICS_IMPLEMENTED_WITH_SYNC_INTRINSICS
     while (!__sync_bool_compare_and_swap(&spinlock, 0, 1))
         ;
 # else
