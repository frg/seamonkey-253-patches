# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1504702629 14400
#      Wed Sep 06 08:57:09 2017 -0400
# Node ID 7e1396b4b627f92f8c0e8e85319c6f38e304ea08
# Parent  955c0bda66a6339b2de03a1abb2cc676ee904206
Bug 1396862 - eliminate -Wunused-private-field warning in AutoFlushICache; r=jandem

The `name_` field of AutoFlushICache is only ever passed to JitSpew* on
ARM, and on non-debug builds, JitSpew* essentially disappears, so clang
claims `name_` is unused.  It is annoying to jump through hoops to
satisfy clang here, but that's life with fatal warnings.

diff --git a/js/src/jit/Ion.cpp b/js/src/jit/Ion.cpp
--- a/js/src/jit/Ion.cpp
+++ b/js/src/jit/Ion.cpp
@@ -3417,17 +3417,19 @@ AutoFlushICache::setInhibit()
 // The JS compiler can be re-entered while within an AutoFlushICache dynamic context and
 // it is assumed that code being assembled or patched is not executed before the exit of
 // the respective AutoFlushICache dynamic context.
 //
 AutoFlushICache::AutoFlushICache(const char* nonce, bool inhibit)
 #if defined(JS_CODEGEN_ARM) || defined(JS_CODEGEN_ARM64) || defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
   : start_(0),
     stop_(0),
+#ifdef JS_JITSPEW
     name_(nonce),
+#endif
     inhibit_(inhibit)
 #endif
 {
 #if defined(JS_CODEGEN_ARM) || defined(JS_CODEGEN_ARM64) || defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
     JSContext* cx = TlsContext.get();
     AutoFlushICache* afc = cx->autoFlushICache();
     if (afc)
         JitSpew(JitSpew_CacheFlush, "<%s,%s%s ", nonce, afc->name_, inhibit ? " I" : "");
diff --git a/js/src/jit/IonCode.h b/js/src/jit/IonCode.h
--- a/js/src/jit/IonCode.h
+++ b/js/src/jit/IonCode.h
@@ -734,17 +734,19 @@ struct IonScriptCounts
 struct VMFunction;
 
 struct AutoFlushICache
 {
   private:
 #if defined(JS_CODEGEN_ARM) || defined(JS_CODEGEN_ARM64) || defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
     uintptr_t start_;
     uintptr_t stop_;
+#ifdef JS_JITSPEW
     const char* name_;
+#endif
     bool inhibit_;
     AutoFlushICache* prev_;
 #endif
 
   public:
     static void setRange(uintptr_t p, size_t len);
     static void flush(uintptr_t p, size_t len);
     static void setInhibit();
