# HG changeset patch
# User Steve Fink <sfink@mozilla.com>
# Date 1505521088 25200
#      Fri Sep 15 17:18:08 2017 -0700
# Node ID eb2b0c8fa756eec150b4c3c9c6254083ac4a37dc
# Parent  17b650e4c65e2f8fd8db266813a579742c0618d9
Bug 1400442 - Annotate border colors array as being thread-owned by container, r=bhackett

nsStyleStruct has the field:

  nsBorderColors** mBorderColors;

It starts out nullptr, and when it is needed, it allocates an array of 4 nsBorderColors pointers. But the nsStyleStruct exclusively owns the array; nothing else can get at it. This change teaches the analysis that if 'this' is a safe nsStyleStruct*, then it should treat mBorderColors as if it were an inline length-4 array.

diff --git a/js/src/devtools/rootAnalysis/analyzeHeapWrites.js b/js/src/devtools/rootAnalysis/analyzeHeapWrites.js
--- a/js/src/devtools/rootAnalysis/analyzeHeapWrites.js
+++ b/js/src/devtools/rootAnalysis/analyzeHeapWrites.js
@@ -226,16 +226,20 @@ function treatAsSafeArgument(entry, varN
         ["Gecko_nsStyleSVG_CopyContextProperties", "aDst", null],
         ["Gecko_nsStyleFont_PrefillDefaultForGeneric", "aFont", null],
         ["Gecko_nsStyleSVG_SetContextPropertiesLength", "aSvg", null],
         ["Gecko_ClearAlternateValues", "aFont", null],
         ["Gecko_AppendAlternateValues", "aFont", null],
         ["Gecko_CopyAlternateValuesFrom", "aDest", null],
         ["Gecko_CounterStyle_GetName", "aResult", null],
         ["Gecko_CounterStyle_GetSingleString", "aResult", null],
+        ["Gecko_EnsureMozBorderColors", "aBorder", null],
+        ["Gecko_ClearMozBorderColors", "aBorder", null],
+        ["Gecko_AppendMozBorderColors", "aBorder", null],
+        ["Gecko_CopyMozBorderColors", "aDest", null],
     ];
     for (var [entryMatch, varMatch, csuMatch] of whitelist) {
         assert(entryMatch || varMatch || csuMatch);
         if (entryMatch && !nameMatches(entry.name, entryMatch))
             continue;
         if (varMatch && !nameMatches(varName, varMatch))
             continue;
         if (csuMatch && (!csuName || !nameMatches(csuName, csuMatch)))
@@ -894,16 +898,32 @@ function processAssign(entry, location, 
         }
         return;
       case "Drf":
         var variable = null;
         if (lhs.Exp[0].Kind == "Var") {
             variable = lhs.Exp[0].Variable;
             if (isSafeVariable(entry, variable))
                 return;
+        } else if (lhs.Exp[0].Kind == "Fld") {
+            const {
+                Type: {Kind, Type: fieldType},
+                FieldCSU: {Type: {Kind: containerTypeKind,
+                                  Name: containerTypeName}}
+            } = lhs.Exp[0].Field;
+            const [containerExpr] = lhs.Exp[0].Exp;
+
+            if (containerTypeKind == 'CSU' &&
+                Kind == 'Pointer' &&
+                isEdgeSafeArgument(entry, containerExpr) &&
+                isSafeMemberPointer(containerTypeName, fieldType))
+            {
+                return;
+            }
+
         }
         if (fields.length)
             checkFieldWrite(entry, location, fields);
         else
             checkDereferenceWrite(entry, location, variableName(variable));
         return;
       case "Int":
         if (isZero(lhs)) {
@@ -1269,16 +1289,36 @@ function isSafeLocalVariable(entry, name
     // If it is initialized at this point we should have seen *some* write
     // already, since the CFG edges are visited in reverse post order.
     if (name in assignments)
         return false;
 
     return true;
 }
 
+function isSafeMemberPointer(containerType, memberType)
+{
+    if (memberType.Kind != 'Pointer')
+        return false;
+
+    const {Type: {Kind: pointeeKind, Name: pointeeTypeName}} = memberType;
+
+    // nsStyleBorder has a member mBorderColors of type nsBorderColors**. It is
+    // lazily initialized to an array of 4 nsBorderColors, and should inherit
+    // the safety of its container.
+    if (containerType == 'nsStyleBorder' &&
+        pointeeKind == 'CSU' &&
+        pointeeTypeName == 'nsBorderColors')
+    {
+        return true;
+    }
+
+    return false;
+}
+
 // Return whether 'exp == value' holds only when execution is on the main thread.
 function testFailsOffMainThread(exp, value) {
     switch (exp.Kind) {
       case "Drf":
         var edge = expressionValueEdge(exp.Exp[0]);
         if (edge) {
             if (isDirectCall(edge, /NS_IsMainThread/) && value)
                 return true;
