# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1503996402 -28800
# Node ID 87c1afc748cca3e7b7d5cd64598375b95e61fe3f
# Parent  a91707ac4a3aad0200fd6d34bfb2d253c89661b2
Bug 1394724. P4 - merge MediaCacheStream::Close() and CloseInternal(). r=cpearce

MozReview-Commit-ID: Fo43lKYPA0m

diff --git a/dom/media/MediaCache.cpp b/dom/media/MediaCache.cpp
--- a/dom/media/MediaCache.cpp
+++ b/dom/media/MediaCache.cpp
@@ -2103,46 +2103,39 @@ MediaCacheStream::AreAllStreamsForResour
   return true;
 }
 
 void
 MediaCacheStream::Close()
 {
   NS_ASSERTION(NS_IsMainThread(), "Only call on main thread");
 
-  if (!mMediaCache) {
+  if (!mMediaCache || mClosed) {
     return;
   }
 
+  mClosed = true;
+
+  // Closing a stream will change the return value of
+  // MediaCacheStream::AreAllStreamsForResourceSuspended as well as
+  // ChannelMediaResource::IsSuspendedByCache. Let's notify it.
+  mMediaCache->QueueSuspendedStatusUpdate(mResourceID);
+
   ReentrantMonitorAutoEnter mon(mMediaCache->GetReentrantMonitor());
-  CloseInternal(mon);
+  mMediaCache->ReleaseStreamBlocks(this);
+  // Wake up any blocked readers
+  mon.NotifyAll();
+
   // Queue an Update since we may have created more free space. Don't do
   // it from CloseInternal since that gets called by Update() itself
   // sometimes, and we try to not to queue updates from Update().
   mMediaCache->QueueUpdate();
 }
 
 void
-MediaCacheStream::CloseInternal(ReentrantMonitorAutoEnter& aReentrantMonitor)
-{
-  NS_ASSERTION(NS_IsMainThread(), "Only call on main thread");
-
-  if (mClosed)
-    return;
-  mClosed = true;
-  // Closing a stream will change the return value of
-  // MediaCacheStream::AreAllStreamsForResourceSuspended as well as
-  // ChannelMediaResource::IsSuspendedByCache. Let's notify it.
-  mMediaCache->QueueSuspendedStatusUpdate(mResourceID);
-  mMediaCache->ReleaseStreamBlocks(this);
-  // Wake up any blocked readers
-  aReentrantMonitor.NotifyAll();
-}
-
-void
 MediaCacheStream::Pin()
 {
   ReentrantMonitorAutoEnter mon(mMediaCache->GetReentrantMonitor());
   ++mPinCount;
   // Queue an Update since we may no longer want to read more into the
   // cache, if this stream's block have become non-evictable
   mMediaCache->QueueUpdate();
 }
diff --git a/dom/media/MediaCache.h b/dom/media/MediaCache.h
--- a/dom/media/MediaCache.h
+++ b/dom/media/MediaCache.h
@@ -420,22 +420,16 @@ private:
   // This method assumes that the cache monitor is held and can be called on
   // any thread.
   int64_t GetNextCachedDataInternal(int64_t aOffset);
   // Writes |mPartialBlock| to disk.
   // Used by |NotifyDataEnded| and |FlushPartialBlock|.
   // If |aNotifyAll| is true, this function will wake up readers who may be
   // waiting on the media cache monitor. Called on the main thread only.
   void FlushPartialBlockInternal(bool aNotify, ReentrantMonitorAutoEnter& aReentrantMonitor);
-  // A helper function to do the work of closing the stream. Assumes
-  // that the cache monitor is held. Main thread only.
-  // aReentrantMonitor is the nsAutoReentrantMonitor wrapper holding the cache monitor.
-  // This is used to NotifyAll to wake up threads that might be
-  // blocked on reading from this stream.
-  void CloseInternal(ReentrantMonitorAutoEnter& aReentrantMonitor);
   // Update mPrincipal given that data has been received from aPrincipal
   bool UpdatePrincipal(nsIPrincipal* aPrincipal);
 
   // Instance of MediaCache to use with this MediaCacheStream.
   RefPtr<MediaCache> mMediaCache;
 
   // These fields are main-thread-only.
   ChannelMediaResource*  mClient;

