# HG changeset patch
# User Karl Tomlinson <karlt+@karlt.net>
# Date 1502254321 -43200
# Node ID 92e3ce541ccd8dd2685881ec469570428a5b7411
# Parent  49259b3833d75f094db54b302ac13909a709e0cc
bug 1024182 remove buffer copy for short impulse responses and so correct normalization scale r=padenot

Despite the comment, this was necessary only for a direct convolver stage,
which has been removed:
https://hg.mozilla.org/mozilla-central/rev/e66105937eef190dec073f1b9859e07a0272706b#l4.29

FFT convolver stages pad the buffer to the necessary length in
FFTBlock::PadAndMakeScaledDFT().

Trailing zeros in the impulse change the scale used in normalization
and so padding the buffer before calculating the scale led to the wrong scale
being used.
https://github.com/WebAudio/web-audio-api/issues/481

MozReview-Commit-ID: LqP1x1hmLOM

diff --git a/dom/media/webaudio/ConvolverNode.cpp b/dom/media/webaudio/ConvolverNode.cpp
--- a/dom/media/webaudio/ConvolverNode.cpp
+++ b/dom/media/webaudio/ConvolverNode.cpp
@@ -267,33 +267,16 @@ ConvolverNode::SetBuffer(JSContext* aCx,
 
   // Send the buffer to the stream
   AudioNodeStream* ns = mStream;
   MOZ_ASSERT(ns, "Why don't we have a stream here?");
   if (mBuffer) {
     uint32_t length = mBuffer->Length();
     RefPtr<ThreadSharedFloatArrayBufferList> data =
       mBuffer->GetThreadSharedChannelsForRate(aCx);
-    if (data && length < WEBAUDIO_BLOCK_SIZE) {
-      // For very small impulse response buffers, we need to pad the
-      // buffer with 0 to make sure that the Reverb implementation
-      // has enough data to compute FFTs from.
-      length = WEBAUDIO_BLOCK_SIZE;
-      RefPtr<ThreadSharedFloatArrayBufferList> paddedBuffer =
-        new ThreadSharedFloatArrayBufferList(data->GetChannels());
-      void* channelData = malloc(sizeof(float) * length * data->GetChannels() + 15);
-      float* alignedChannelData = ALIGNED16(channelData);
-      ASSERT_ALIGNED16(alignedChannelData);
-      for (uint32_t i = 0; i < data->GetChannels(); ++i) {
-        PodCopy(alignedChannelData + length * i, data->GetData(i), mBuffer->Length());
-        PodZero(alignedChannelData + length * i + mBuffer->Length(), WEBAUDIO_BLOCK_SIZE - mBuffer->Length());
-        paddedBuffer->SetData(i, (i == 0) ? channelData : nullptr, free, alignedChannelData);
-      }
-      data = paddedBuffer;
-    }
     SendInt32ParameterToStream(ConvolverNodeEngine::BUFFER_LENGTH, length);
     SendDoubleParameterToStream(ConvolverNodeEngine::SAMPLE_RATE,
                                 mBuffer->SampleRate());
     ns->SetBuffer(data.forget());
   } else {
     ns->SetBuffer(nullptr);
   }
 }

