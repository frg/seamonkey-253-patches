# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1502338449 -36000
# Node ID a8aa2a5c2870498d9c21b05fed673d786591f817
# Parent  4c2f6c34dd4c7ee8d925d3305f9d07a7b007b425
Bug 1387956 (part 2) - Overhaul handling of nsWindowSizes. r=mccr8.

This patch does the following.

- Moves nsWindowSizes from nsWindowMemoryReporter.h to its own file,
  nsWindowSizes.h, so it can be included more widely without exposing
  nsWindowMemoryReporter.

- Merges nsArenaMemoryStats.h (which defines nsTabSizes and nsArenaMemoryStats)
  into nsWindowSizes.h.

- Renames nsArenaMemoryStats as nsArenaSizes, and nsWindowSizes::mArenaStats as
  nsWindowSizes::mArenaSizes. This is the more usual naming scheme for such
  types.

- Renames FRAME_ID_STAT_FIELD as NS_ARENA_SIZES_FIELD.

- Passes nsWindowSizes to PresShell::AddSizeOfIncludingThis() and
  nsPresArena::AddSizeOfExcludingThis(), instead of a bunch of smaller things.
  One nice consequence is that the odd nsArenaMemoryStats::mOther field is no
  longer necessary, because we can update nsWindowSizes::mLayoutPresShellSize
  directly in nsPresArena::AddSizeOfExcludingThis().

- Adds |const| to a few methods.

MozReview-Commit-ID: EpgFWKFqy7Y

diff --git a/dom/base/moz.build b/dom/base/moz.build
--- a/dom/base/moz.build
+++ b/dom/base/moz.build
@@ -120,16 +120,17 @@ EXPORTS += [
     'nsStubDocumentObserver.h',
     'nsStubMutationObserver.h',
     'nsStyledElement.h',
     'nsTextFragment.h',
     'nsTraversal.h',
     'nsTreeSanitizer.h',
     'nsViewportInfo.h',
     'nsWindowMemoryReporter.h',
+    'nsWindowSizes.h',
     'nsWrapperCache.h',
     'nsWrapperCacheInlines.h',
     'nsXMLNameSpaceMap.h',
     'XPathGenerator.h',
 ]
 
 if CONFIG['MOZ_WEBRTC']:
     EXPORTS += [
diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -239,17 +239,17 @@
 #include "mozilla/dom/XPathEvaluator.h"
 #include "mozilla/dom/XPathNSResolverBinding.h"
 #include "mozilla/dom/XPathResult.h"
 #include "nsIDocumentEncoder.h"
 #include "nsIDocumentActivity.h"
 #include "nsIStructuredCloneContainer.h"
 #include "nsIMutableArray.h"
 #include "mozilla/dom/DOMStringList.h"
-#include "nsWindowMemoryReporter.h"
+#include "nsWindowSizes.h"
 #include "mozilla/dom/Location.h"
 #include "mozilla/dom/FontFaceSet.h"
 #include "gfxPrefs.h"
 #include "nsISupportsPrimitives.h"
 #include "mozilla/StyleSetHandle.h"
 #include "mozilla/StyleSetHandleInlines.h"
 #include "mozilla/StyleSheet.h"
 #include "mozilla/StyleSheetInlines.h"
@@ -12401,23 +12401,17 @@ nsDocument::GetVisibilityState(nsAString
 
 /* virtual */ void
 nsIDocument::DocAddSizeOfExcludingThis(nsWindowSizes& aWindowSizes) const
 {
   aWindowSizes.mDOMOtherSize +=
     nsINode::SizeOfExcludingThis(aWindowSizes.mState);
 
   if (mPresShell) {
-    mPresShell->AddSizeOfIncludingThis(aWindowSizes.mState.mMallocSizeOf,
-                                       &aWindowSizes.mArenaStats,
-                                       &aWindowSizes.mLayoutPresShellSize,
-                                       &aWindowSizes.mLayoutStyleSetsSize,
-                                       &aWindowSizes.mLayoutTextRunsSize,
-                                       &aWindowSizes.mLayoutPresContextSize,
-                                       &aWindowSizes.mLayoutFramePropertiesSize);
+    mPresShell->AddSizeOfIncludingThis(aWindowSizes);
   }
 
   aWindowSizes.mPropertyTablesSize +=
     mPropertyTable.SizeOfExcludingThis(aWindowSizes.mState.mMallocSizeOf);
   for (uint32_t i = 0, count = mExtraPropertyTables.Length();
        i < count; ++i) {
     aWindowSizes.mPropertyTablesSize +=
       mExtraPropertyTables[i]->SizeOfIncludingThis(
diff --git a/dom/base/nsGlobalWindow.cpp b/dom/base/nsGlobalWindow.cpp
--- a/dom/base/nsGlobalWindow.cpp
+++ b/dom/base/nsGlobalWindow.cpp
@@ -47,16 +47,17 @@
 #include "nsIPermissionManager.h"
 #include "nsIScriptContext.h"
 #include "nsIScriptTimeoutHandler.h"
 #include "nsITimeoutHandler.h"
 #include "nsIController.h"
 #include "nsScriptNameSpaceManager.h"
 #include "nsISlowScriptDebug.h"
 #include "nsWindowMemoryReporter.h"
+#include "nsWindowSizes.h"
 #include "WindowNamedPropertiesHandler.h"
 #include "nsFrameSelection.h"
 #include "nsNetUtil.h"
 #include "nsVariant.h"
 #include "nsPrintfCString.h"
 #include "mozilla/intl/LocaleService.h"
 
 // Helper Classes
diff --git a/dom/base/nsWindowMemoryReporter.cpp b/dom/base/nsWindowMemoryReporter.cpp
--- a/dom/base/nsWindowMemoryReporter.cpp
+++ b/dom/base/nsWindowMemoryReporter.cpp
@@ -1,16 +1,17 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "amIAddonManager.h"
 #include "nsWindowMemoryReporter.h"
+#include "nsWindowSizes.h"
 #include "nsGlobalWindow.h"
 #include "nsIDocument.h"
 #include "nsIDOMWindowCollection.h"
 #include "nsIEffectiveTLDService.h"
 #include "mozilla/ClearOnShutdown.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/Services.h"
 #include "mozilla/StaticPtr.h"
@@ -363,35 +364,35 @@ CollectWindowReports(nsGlobalWindow *aWi
   aWindowTotalSizes->mStyleSheetsSize += windowSizes.mStyleSheetsSize;
 
   REPORT_SIZE("/layout/pres-shell", windowSizes.mLayoutPresShellSize,
               "Memory used by layout's PresShell, along with any structures "
               "allocated in its arena and not measured elsewhere, "
               "within a window.");
   aWindowTotalSizes->mLayoutPresShellSize += windowSizes.mLayoutPresShellSize;
 
-  REPORT_SIZE("/layout/line-boxes", windowSizes.mArenaStats.mLineBoxes,
+  REPORT_SIZE("/layout/line-boxes", windowSizes.mArenaSizes.mLineBoxes,
               "Memory used by line boxes within a window.");
-  aWindowTotalSizes->mArenaStats.mLineBoxes
-    += windowSizes.mArenaStats.mLineBoxes;
+  aWindowTotalSizes->mArenaSizes.mLineBoxes
+    += windowSizes.mArenaSizes.mLineBoxes;
 
-  REPORT_SIZE("/layout/rule-nodes", windowSizes.mArenaStats.mRuleNodes,
+  REPORT_SIZE("/layout/rule-nodes", windowSizes.mArenaSizes.mRuleNodes,
               "Memory used by CSS rule nodes within a window.");
-  aWindowTotalSizes->mArenaStats.mRuleNodes
-    += windowSizes.mArenaStats.mRuleNodes;
+  aWindowTotalSizes->mArenaSizes.mRuleNodes
+    += windowSizes.mArenaSizes.mRuleNodes;
 
-  REPORT_SIZE("/layout/style-contexts", windowSizes.mArenaStats.mStyleContexts,
+  REPORT_SIZE("/layout/style-contexts", windowSizes.mArenaSizes.mStyleContexts,
               "Memory used by style contexts within a window.");
-  aWindowTotalSizes->mArenaStats.mStyleContexts
-    += windowSizes.mArenaStats.mStyleContexts;
+  aWindowTotalSizes->mArenaSizes.mStyleContexts
+    += windowSizes.mArenaSizes.mStyleContexts;
 
-  REPORT_SIZE("/layout/style-structs", windowSizes.mArenaStats.mStyleStructs,
+  REPORT_SIZE("/layout/style-structs", windowSizes.mArenaSizes.mStyleStructs,
               "Memory used by style structs within a window.");
-  aWindowTotalSizes->mArenaStats.mStyleStructs
-    += windowSizes.mArenaStats.mStyleStructs;
+  aWindowTotalSizes->mArenaSizes.mStyleStructs
+    += windowSizes.mArenaSizes.mStyleStructs;
 
   REPORT_SIZE("/layout/style-sets", windowSizes.mLayoutStyleSetsSize,
               "Memory used by style sets within a window.");
   aWindowTotalSizes->mLayoutStyleSetsSize += windowSizes.mLayoutStyleSetsSize;
 
   REPORT_SIZE("/layout/text-runs", windowSizes.mLayoutTextRunsSize,
               "Memory used for text-runs (glyph layout) in the PresShell's "
               "frame tree, within a window.");
@@ -426,25 +427,25 @@ CollectWindowReports(nsGlobalWindow *aWi
   // about:memory with many uninteresting entries.
   const size_t FRAME_SUNDRIES_THRESHOLD =
     js::MemoryReportingSundriesThreshold();
 
   size_t frameSundriesSize = 0;
 #define FRAME_ID(classname, ...)                                        \
   {                                                                     \
     size_t frameSize                                                    \
-      = windowSizes.mArenaStats.FRAME_ID_STAT_FIELD(classname);         \
+      = windowSizes.mArenaSizes.NS_ARENA_SIZES_FIELD(classname);        \
     if (frameSize < FRAME_SUNDRIES_THRESHOLD) {                         \
       frameSundriesSize += frameSize;                                   \
     } else {                                                            \
       REPORT_SIZE("/layout/frames/" # classname, frameSize,             \
                   "Memory used by frames of "                           \
                   "type " #classname " within a window.");              \
     }                                                                   \
-    aWindowTotalSizes->mArenaStats.FRAME_ID_STAT_FIELD(classname)       \
+    aWindowTotalSizes->mArenaSizes.NS_ARENA_SIZES_FIELD(classname)      \
       += frameSize;                                                     \
   }
 #define ABSTRACT_FRAME_ID(...)
 #include "nsFrameIdList.h"
 #undef FRAME_ID
 #undef ABSTRACT_FRAME_ID
 
   if (frameSundriesSize > 0) {
@@ -569,46 +570,46 @@ nsWindowMemoryReporter::CollectReports(n
 
   REPORT("window-objects/style-sheets", windowTotalSizes.mStyleSheetsSize,
          "This is the sum of all windows' 'style-sheets' numbers.");
 
   REPORT("window-objects/layout/pres-shell", windowTotalSizes.mLayoutPresShellSize,
          "This is the sum of all windows' 'layout/arenas' numbers.");
 
   REPORT("window-objects/layout/line-boxes",
-         windowTotalSizes.mArenaStats.mLineBoxes,
+         windowTotalSizes.mArenaSizes.mLineBoxes,
          "This is the sum of all windows' 'layout/line-boxes' numbers.");
 
   REPORT("window-objects/layout/rule-nodes",
-         windowTotalSizes.mArenaStats.mRuleNodes,
+         windowTotalSizes.mArenaSizes.mRuleNodes,
          "This is the sum of all windows' 'layout/rule-nodes' numbers.");
 
   REPORT("window-objects/layout/style-contexts",
-         windowTotalSizes.mArenaStats.mStyleContexts,
+         windowTotalSizes.mArenaSizes.mStyleContexts,
          "This is the sum of all windows' 'layout/style-contexts' numbers.");
 
   REPORT("window-objects/layout/style-structs",
-         windowTotalSizes.mArenaStats.mStyleStructs,
+         windowTotalSizes.mArenaSizes.mStyleStructs,
          "This is the sum of all windows' 'layout/style-structs' numbers.");
 
   REPORT("window-objects/layout/style-sets", windowTotalSizes.mLayoutStyleSetsSize,
          "This is the sum of all windows' 'layout/style-sets' numbers.");
 
   REPORT("window-objects/layout/text-runs", windowTotalSizes.mLayoutTextRunsSize,
          "This is the sum of all windows' 'layout/text-runs' numbers.");
 
   REPORT("window-objects/layout/pres-contexts", windowTotalSizes.mLayoutPresContextSize,
          "This is the sum of all windows' 'layout/pres-contexts' numbers.");
 
   REPORT("window-objects/layout/frame-properties", windowTotalSizes.mLayoutFramePropertiesSize,
          "This is the sum of all windows' 'layout/frame-properties' numbers.");
 
   size_t frameTotal = 0;
-#define FRAME_ID(classname, ...)                \
-  frameTotal += windowTotalSizes.mArenaStats.FRAME_ID_STAT_FIELD(classname);
+#define FRAME_ID(classname, ...) \
+  frameTotal += windowTotalSizes.mArenaSizes.NS_ARENA_SIZES_FIELD(classname);
 #define ABSTRACT_FRAME_ID(...)
 #include "nsFrameIdList.h"
 #undef FRAME_ID
 #undef ABSTRACT_FRAME_ID
 
   REPORT("window-objects/layout/frames", frameTotal,
          "Memory used for layout frames within windows. "
          "This is the sum of all windows' 'layout/frames/' numbers.");
diff --git a/dom/base/nsWindowMemoryReporter.h b/dom/base/nsWindowMemoryReporter.h
--- a/dom/base/nsWindowMemoryReporter.h
+++ b/dom/base/nsWindowMemoryReporter.h
@@ -13,77 +13,16 @@
 #include "nsITimer.h"
 #include "nsDataHashtable.h"
 #include "nsWeakReference.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/Assertions.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/PodOperations.h"
 #include "mozilla/TimeStamp.h"
-#include "nsArenaMemoryStats.h"
-
-class nsWindowSizes {
-#define FOR_EACH_SIZE(macro) \
-  macro(DOM,   mDOMElementNodesSize) \
-  macro(DOM,   mDOMTextNodesSize) \
-  macro(DOM,   mDOMCDATANodesSize) \
-  macro(DOM,   mDOMCommentNodesSize) \
-  macro(DOM,   mDOMEventTargetsSize) \
-  macro(DOM,   mDOMPerformanceUserEntries) \
-  macro(DOM,   mDOMPerformanceResourceEntries) \
-  macro(DOM,   mDOMOtherSize) \
-  macro(Style, mStyleSheetsSize) \
-  macro(Other, mLayoutPresShellSize) \
-  macro(Style, mLayoutStyleSetsSize) \
-  macro(Other, mLayoutTextRunsSize) \
-  macro(Other, mLayoutPresContextSize) \
-  macro(Other, mLayoutFramePropertiesSize) \
-  macro(Other, mPropertyTablesSize) \
-
-public:
-  explicit nsWindowSizes(mozilla::SizeOfState& aState)
-    :
-      #define ZERO_SIZE(kind, mSize)  mSize(0),
-      FOR_EACH_SIZE(ZERO_SIZE)
-      #undef ZERO_SIZE
-      mDOMEventTargetsCount(0),
-      mDOMEventListenersCount(0),
-      mArenaStats(),
-      mState(aState)
-  {}
-
-  void addToTabSizes(nsTabSizes *sizes) const {
-    #define ADD_TO_TAB_SIZES(kind, mSize) sizes->add(nsTabSizes::kind, mSize);
-    FOR_EACH_SIZE(ADD_TO_TAB_SIZES)
-    #undef ADD_TO_TAB_SIZES
-    mArenaStats.addToTabSizes(sizes);
-  }
-
-  size_t getTotalSize() const
-  {
-    size_t total = 0;
-    #define ADD_TO_TOTAL_SIZE(kind, mSize) total += mSize;
-    FOR_EACH_SIZE(ADD_TO_TOTAL_SIZE)
-    #undef ADD_TO_TOTAL_SIZE
-    total += mArenaStats.getTotalSize();
-    return total;
-  }
-
-  #define DECL_SIZE(kind, mSize) size_t mSize;
-  FOR_EACH_SIZE(DECL_SIZE);
-  #undef DECL_SIZE
-
-  uint32_t mDOMEventTargetsCount;
-  uint32_t mDOMEventListenersCount;
-
-  nsArenaMemoryStats mArenaStats;
-  mozilla::SizeOfState& mState;
-
-#undef FOR_EACH_SIZE
-};
 
 /**
  * nsWindowMemoryReporter is responsible for the 'explicit/window-objects'
  * memory reporter.
  *
  * We classify DOM window objects into one of three categories:
  *
  *   - "active" windows, which are displayed in a tab (as the top-level window
diff --git a/dom/base/nsWindowSizes.h b/dom/base/nsWindowSizes.h
new file mode 100644
--- /dev/null
+++ b/dom/base/nsWindowSizes.h
@@ -0,0 +1,164 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef nsWindowSizes_h
+#define nsWindowSizes_h
+
+#include "mozilla/Assertions.h"
+#include "mozilla/PodOperations.h"
+#include "mozilla/SizeOfState.h"
+
+class nsTabSizes {
+public:
+  enum Kind {
+      DOM,        // DOM stuff.
+      Style,      // Style stuff.
+      Other       // Everything else.
+  };
+
+  nsTabSizes() { mozilla::PodZero(this); }
+
+  void add(Kind kind, size_t n)
+  {
+    switch (kind) {
+      case DOM:   mDom   += n; break;
+      case Style: mStyle += n; break;
+      case Other: mOther += n; break;
+      default:    MOZ_CRASH("bad nsTabSizes kind");
+    }
+  }
+
+  size_t mDom;
+  size_t mStyle;
+  size_t mOther;
+};
+
+#define NS_ARENA_SIZES_FIELD(classname) mArena##classname
+
+struct nsArenaSizes {
+#define FOR_EACH_SIZE(macro) \
+  macro(Other, mLineBoxes) \
+  macro(Style, mRuleNodes) \
+  macro(Style, mStyleContexts) \
+  macro(Style, mStyleStructs)
+
+  nsArenaSizes()
+    :
+      #define ZERO_SIZE(kind, mSize) mSize(0),
+      FOR_EACH_SIZE(ZERO_SIZE)
+      #undef ZERO_SIZE
+      #define FRAME_ID(classname, ...) NS_ARENA_SIZES_FIELD(classname)(),
+      #define ABSTRACT_FRAME_ID(...)
+      #include "nsFrameIdList.h"
+      #undef FRAME_ID
+      #undef ABSTRACT_FRAME_ID
+      dummy()
+  {}
+
+  void addToTabSizes(nsTabSizes *sizes) const
+  {
+    #define ADD_TO_TAB_SIZES(kind, mSize) sizes->add(nsTabSizes::kind, mSize);
+    FOR_EACH_SIZE(ADD_TO_TAB_SIZES)
+    #undef ADD_TO_TAB_SIZES
+    #define FRAME_ID(classname, ...) \
+      sizes->add(nsTabSizes::Other, NS_ARENA_SIZES_FIELD(classname));
+    #define ABSTRACT_FRAME_ID(...)
+    #include "nsFrameIdList.h"
+    #undef FRAME_ID
+    #undef ABSTRACT_FRAME_ID
+  }
+
+  size_t getTotalSize() const
+  {
+    size_t total = 0;
+    #define ADD_TO_TOTAL_SIZE(kind, mSize) total += mSize;
+    FOR_EACH_SIZE(ADD_TO_TOTAL_SIZE)
+    #undef ADD_TO_TOTAL_SIZE
+    #define FRAME_ID(classname, ...) \
+      total += NS_ARENA_SIZES_FIELD(classname);
+    #define ABSTRACT_FRAME_ID(...)
+    #include "nsFrameIdList.h"
+    #undef FRAME_ID
+    #undef ABSTRACT_FRAME_ID
+    return total;
+  }
+
+  #define DECL_SIZE(kind, mSize) size_t mSize;
+  FOR_EACH_SIZE(DECL_SIZE)
+  #undef DECL_SIZE
+  #define FRAME_ID(classname, ...) size_t NS_ARENA_SIZES_FIELD(classname);
+  #define ABSTRACT_FRAME_ID(...)
+  #include "nsFrameIdList.h"
+  #undef FRAME_ID
+  #undef ABSTRACT_FRAME_ID
+  int dummy;  // present just to absorb the trailing comma from FRAME_ID in the
+              // constructor
+
+#undef FOR_EACH_SIZE
+};
+
+class nsWindowSizes
+{
+#define FOR_EACH_SIZE(macro) \
+  macro(DOM,   mDOMElementNodesSize) \
+  macro(DOM,   mDOMTextNodesSize) \
+  macro(DOM,   mDOMCDATANodesSize) \
+  macro(DOM,   mDOMCommentNodesSize) \
+  macro(DOM,   mDOMEventTargetsSize) \
+  macro(DOM,   mDOMPerformanceUserEntries) \
+  macro(DOM,   mDOMPerformanceResourceEntries) \
+  macro(DOM,   mDOMOtherSize) \
+  macro(Style, mStyleSheetsSize) \
+  macro(Other, mLayoutPresShellSize) \
+  macro(Style, mLayoutStyleSetsSize) \
+  macro(Other, mLayoutTextRunsSize) \
+  macro(Other, mLayoutPresContextSize) \
+  macro(Other, mLayoutFramePropertiesSize) \
+  macro(Other, mPropertyTablesSize) \
+
+public:
+  explicit nsWindowSizes(mozilla::SizeOfState& aState)
+    :
+      #define ZERO_SIZE(kind, mSize)  mSize(0),
+      FOR_EACH_SIZE(ZERO_SIZE)
+      #undef ZERO_SIZE
+      mDOMEventTargetsCount(0),
+      mDOMEventListenersCount(0),
+      mArenaSizes(),
+      mState(aState)
+  {}
+
+  void addToTabSizes(nsTabSizes *sizes) const {
+    #define ADD_TO_TAB_SIZES(kind, mSize) sizes->add(nsTabSizes::kind, mSize);
+    FOR_EACH_SIZE(ADD_TO_TAB_SIZES)
+    #undef ADD_TO_TAB_SIZES
+    mArenaSizes.addToTabSizes(sizes);
+  }
+
+  size_t getTotalSize() const
+  {
+    size_t total = 0;
+    #define ADD_TO_TOTAL_SIZE(kind, mSize) total += mSize;
+    FOR_EACH_SIZE(ADD_TO_TOTAL_SIZE)
+    #undef ADD_TO_TOTAL_SIZE
+    total += mArenaSizes.getTotalSize();
+    return total;
+  }
+
+  #define DECL_SIZE(kind, mSize) size_t mSize;
+  FOR_EACH_SIZE(DECL_SIZE);
+  #undef DECL_SIZE
+
+  uint32_t mDOMEventTargetsCount;
+  uint32_t mDOMEventListenersCount;
+
+  nsArenaSizes mArenaSizes;
+  mozilla::SizeOfState& mState;
+
+#undef FOR_EACH_SIZE
+};
+
+#endif // nsWindowSizes_h
diff --git a/image/VectorImage.cpp b/image/VectorImage.cpp
--- a/image/VectorImage.cpp
+++ b/image/VectorImage.cpp
@@ -20,17 +20,17 @@
 #include "nsIPresShell.h"
 #include "nsIStreamListener.h"
 #include "nsMimeTypes.h"
 #include "nsPresContext.h"
 #include "nsRect.h"
 #include "nsString.h"
 #include "nsStubDocumentObserver.h"
 #include "nsSVGEffects.h" // for nsSVGRenderingObserver
-#include "nsWindowMemoryReporter.h"
+#include "nsWindowSizes.h"
 #include "ImageRegion.h"
 #include "ISurfaceProvider.h"
 #include "LookupResult.h"
 #include "Orientation.h"
 #include "SVGDocumentWrapper.h"
 #include "SVGDrawingParameters.h"
 #include "nsIDOMEventListener.h"
 #include "SurfaceCache.h"
diff --git a/layout/base/PresShell.cpp b/layout/base/PresShell.cpp
--- a/layout/base/PresShell.cpp
+++ b/layout/base/PresShell.cpp
@@ -70,16 +70,17 @@
 #include "mozilla/dom/Selection.h"
 #include "nsGkAtoms.h"
 #include "nsIDOMRange.h"
 #include "nsIDOMDocument.h"
 #include "nsIDOMNode.h"
 #include "nsIDOMNodeList.h"
 #include "nsIDOMElement.h"
 #include "nsRange.h"
+#include "nsWindowSizes.h"
 #include "nsCOMPtr.h"
 #include "nsAutoPtr.h"
 #include "nsReadableUtils.h"
 #include "nsIPageSequenceFrame.h"
 #include "nsIPermissionManager.h"
 #include "nsIMozBrowserFrame.h"
 #include "nsCaret.h"
 #include "AccessibleCaretEventHub.h"
@@ -118,17 +119,16 @@
 #include "nsNetUtil.h"
 #include "nsThreadUtils.h"
 #include "nsStyleSheetService.h"
 #include "gfxUtils.h"
 #include "nsSMILAnimationController.h"
 #include "SVGContentUtils.h"
 #include "nsSVGEffects.h"
 #include "SVGFragmentIdentifier.h"
-#include "nsArenaMemoryStats.h"
 #include "nsFrameSelection.h"
 
 #include "mozilla/dom/Performance.h"
 #include "nsRefreshDriver.h"
 #include "nsDOMNavigationTiming.h"
 
 // Drag & Drop, Clipboard
 #include "nsIDocShellTreeItem.h"
@@ -10754,49 +10754,45 @@ PresShell::GetRootPresShell()
     if (rootPresContext) {
       return static_cast<PresShell*>(rootPresContext->PresShell());
     }
   }
   return nullptr;
 }
 
 void
-PresShell::AddSizeOfIncludingThis(MallocSizeOf aMallocSizeOf,
-                                  nsArenaMemoryStats* aArenaObjectsSize,
-                                  size_t* aPresShellSize,
-                                  size_t* aStyleSetsSize,
-                                  size_t* aTextRunsSize,
-                                  size_t* aPresContextSize,
-                                  size_t* aFramePropertiesSize)
-{
-  mFrameArena.AddSizeOfExcludingThis(aMallocSizeOf, aArenaObjectsSize);
-  *aPresShellSize += aMallocSizeOf(this);
+PresShell::AddSizeOfIncludingThis(nsWindowSizes& aSizes) const
+{
+  MallocSizeOf mallocSizeOf = aSizes.mState.mMallocSizeOf;
+  mFrameArena.AddSizeOfExcludingThis(aSizes);
+  aSizes.mLayoutPresShellSize += mallocSizeOf(this);
   if (mCaret) {
-    *aPresShellSize += mCaret->SizeOfIncludingThis(aMallocSizeOf);
-  }
-  *aPresShellSize += mApproximatelyVisibleFrames.ShallowSizeOfExcludingThis(aMallocSizeOf);
-  *aPresShellSize += mFramesToDirty.ShallowSizeOfExcludingThis(aMallocSizeOf);
-  *aPresShellSize += aArenaObjectsSize->mOther;
+    aSizes.mLayoutPresShellSize += mCaret->SizeOfIncludingThis(mallocSizeOf);
+  }
+  aSizes.mLayoutPresShellSize +=
+    mApproximatelyVisibleFrames.ShallowSizeOfExcludingThis(mallocSizeOf) +
+    mFramesToDirty.ShallowSizeOfExcludingThis(mallocSizeOf);
 
   if (nsStyleSet* styleSet = StyleSet()->GetAsGecko()) {
-    *aStyleSetsSize += styleSet->SizeOfIncludingThis(aMallocSizeOf);
+    aSizes.mLayoutStyleSetsSize += styleSet->SizeOfIncludingThis(mallocSizeOf);
   } else if (ServoStyleSet* styleSet = StyleSet()->GetAsServo()) {
-    *aStyleSetsSize += styleSet->SizeOfIncludingThis(aMallocSizeOf);
+    aSizes.mLayoutStyleSetsSize += styleSet->SizeOfIncludingThis(mallocSizeOf);
   } else {
     MOZ_CRASH();
   }
 
-  *aTextRunsSize += SizeOfTextRuns(aMallocSizeOf);
-
-  *aPresContextSize += mPresContext->SizeOfIncludingThis(aMallocSizeOf);
+  aSizes.mLayoutTextRunsSize += SizeOfTextRuns(mallocSizeOf);
+
+  aSizes.mLayoutPresContextSize +=
+    mPresContext->SizeOfIncludingThis(mallocSizeOf);
 
   nsIFrame* rootFrame = mFrameConstructor->GetRootFrame();
   if (rootFrame) {
-    *aFramePropertiesSize +=
-      rootFrame->SizeOfFramePropertiesForTree(aMallocSizeOf);
+    aSizes.mLayoutFramePropertiesSize +=
+      rootFrame->SizeOfFramePropertiesForTree(mallocSizeOf);
   }
 }
 
 size_t
 PresShell::SizeOfTextRuns(MallocSizeOf aMallocSizeOf) const
 {
   nsIFrame* rootFrame = mFrameConstructor->GetRootFrame();
   if (!rootFrame) {
diff --git a/layout/base/PresShell.h b/layout/base/PresShell.h
--- a/layout/base/PresShell.h
+++ b/layout/base/PresShell.h
@@ -364,23 +364,18 @@ public:
   virtual bool IsLayoutFlushObserver() override
   {
     return GetPresContext()->RefreshDriver()->
       IsLayoutFlushObserver(this);
   }
 
   virtual void LoadComplete() override;
 
-  void AddSizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf,
-                              nsArenaMemoryStats* aArenaObjectsSize,
-                              size_t* aPresShellSize,
-                              size_t* aStyleSetsSize,
-                              size_t* aTextRunsSize,
-                              size_t* aPresContextSize,
-                              size_t* aFramePropertiesSize) override;
+  virtual void AddSizeOfIncludingThis(nsWindowSizes& aWindowSizes)
+    const override;
   size_t SizeOfTextRuns(mozilla::MallocSizeOf aMallocSizeOf) const;
 
   // This data is stored as a content property (nsGkAtoms::scrolling) on
   // mContentToScrollTo when we have a pending ScrollIntoView.
   struct ScrollIntoViewData {
     ScrollAxis mContentScrollVAxis;
     ScrollAxis mContentScrollHAxis;
     uint32_t   mContentToScrollToFlags;
diff --git a/layout/base/moz.build b/layout/base/moz.build
--- a/layout/base/moz.build
+++ b/layout/base/moz.build
@@ -32,17 +32,16 @@ if CONFIG['MOZ_DEBUG']:
 
 XPIDL_MODULE = 'layout_base'
 
 EXPORTS += [
     'CaretAssociationHint.h',
     'FrameProperties.h',
     'LayoutLogging.h',
     'MobileViewportManager.h',
-    'nsArenaMemoryStats.h',
     'nsBidi.h',
     'nsBidiPresUtils.h',
     'nsCaret.h',
     'nsChangeHint.h',
     'nsCompatibility.h',
     'nsCSSFrameConstructor.h',
     'nsFrameManager.h',
     'nsFrameManagerBase.h',
diff --git a/layout/base/nsArenaMemoryStats.h b/layout/base/nsArenaMemoryStats.h
deleted file mode 100644
--- a/layout/base/nsArenaMemoryStats.h
+++ /dev/null
@@ -1,103 +0,0 @@
-/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* vim: set ts=8 sts=2 et sw=2 tw=80: */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#ifndef nsArenaMemoryStats_h
-#define nsArenaMemoryStats_h
-
-#include "mozilla/Assertions.h"
-#include "mozilla/PodOperations.h"
-
-class nsTabSizes {
-public:
-  enum Kind {
-      DOM,        // DOM stuff.
-      Style,      // Style stuff.
-      Other       // Everything else.
-  };
-
-  nsTabSizes() { mozilla::PodZero(this); }
-
-  void add(Kind kind, size_t n)
-  {
-    switch (kind) {
-      case DOM:   mDom   += n; break;
-      case Style: mStyle += n; break;
-      case Other: mOther += n; break;
-      default:    MOZ_CRASH("bad nsTabSizes kind");
-    }
-  }
-
-  size_t mDom;
-  size_t mStyle;
-  size_t mOther;
-};
-
-#define FRAME_ID_STAT_FIELD(classname) mArena##classname
-
-struct nsArenaMemoryStats {
-#define FOR_EACH_SIZE(macro) \
-  macro(Other, mLineBoxes) \
-  macro(Style, mRuleNodes) \
-  macro(Style, mStyleContexts) \
-  macro(Style, mStyleStructs) \
-  macro(Other, mOther)
-
-  nsArenaMemoryStats()
-    :
-      #define ZERO_SIZE(kind, mSize) mSize(0),
-      FOR_EACH_SIZE(ZERO_SIZE)
-      #undef ZERO_SIZE
-      #define FRAME_ID(classname, ...) FRAME_ID_STAT_FIELD(classname)(),
-      #define ABSTRACT_FRAME_ID(...)
-      #include "nsFrameIdList.h"
-      #undef FRAME_ID
-      #undef ABSTRACT_FRAME_ID
-      dummy()
-  {}
-
-  void addToTabSizes(nsTabSizes *sizes) const
-  {
-    #define ADD_TO_TAB_SIZES(kind, mSize) sizes->add(nsTabSizes::kind, mSize);
-    FOR_EACH_SIZE(ADD_TO_TAB_SIZES)
-    #undef ADD_TO_TAB_SIZES
-    #define FRAME_ID(classname, ...) \
-      sizes->add(nsTabSizes::Other, FRAME_ID_STAT_FIELD(classname));
-    #define ABSTRACT_FRAME_ID(...)
-    #include "nsFrameIdList.h"
-    #undef FRAME_ID
-    #undef ABSTRACT_FRAME_ID
-  }
-
-  size_t getTotalSize() const
-  {
-    size_t total = 0;
-    #define ADD_TO_TOTAL_SIZE(kind, mSize) total += mSize;
-    FOR_EACH_SIZE(ADD_TO_TOTAL_SIZE)
-    #undef ADD_TO_TOTAL_SIZE
-    #define FRAME_ID(classname, ...) \
-      total += FRAME_ID_STAT_FIELD(classname);
-    #define ABSTRACT_FRAME_ID(...)
-    #include "nsFrameIdList.h"
-    #undef FRAME_ID
-    #undef ABSTRACT_FRAME_ID
-    return total;
-  }
-
-  #define DECL_SIZE(kind, mSize) size_t mSize;
-  FOR_EACH_SIZE(DECL_SIZE)
-  #undef DECL_SIZE
-  #define FRAME_ID(classname, ...) size_t FRAME_ID_STAT_FIELD(classname);
-  #define ABSTRACT_FRAME_ID(...)
-  #include "nsFrameIdList.h"
-  #undef FRAME_ID
-  #undef ABSTRACT_FRAME_ID
-  int dummy;  // present just to absorb the trailing comma from FRAME_ID in the
-              // constructor
-
-#undef FOR_EACH_SIZE
-};
-
-#endif // nsArenaMemoryStats_h
diff --git a/layout/base/nsIPresShell.h b/layout/base/nsIPresShell.h
--- a/layout/base/nsIPresShell.h
+++ b/layout/base/nsIPresShell.h
@@ -52,16 +52,17 @@
 #include "nsFrameState.h"
 #include "Units.h"
 
 class gfxContext;
 class nsDocShell;
 class nsIDocument;
 class nsIFrame;
 class nsPresContext;
+class nsWindowSizes;
 class nsViewManager;
 class nsView;
 class nsIPageSequenceFrame;
 class nsCanvasFrame;
 class nsCaret;
 namespace mozilla {
 class AccessibleCaretEventHub;
 class CSSStyleSheet;
@@ -91,17 +92,16 @@ class nsAPostRefreshObserver;
 #ifdef ACCESSIBILITY
 class nsAccessibilityService;
 namespace mozilla {
 namespace a11y {
 class DocAccessible;
 } // namespace a11y
 } // namespace mozilla
 #endif
-struct nsArenaMemoryStats;
 class nsITimer;
 
 namespace mozilla {
 class EventStates;
 
 namespace dom {
 class Element;
 class Touch;
@@ -1587,23 +1587,17 @@ public:
     PAINT_DELAYED_COMPRESS
   };
   virtual void ScheduleViewManagerFlush(PaintType aType = PAINT_DEFAULT) = 0;
   virtual void ClearMouseCaptureOnView(nsView* aView) = 0;
   virtual bool IsVisible() = 0;
   virtual void DispatchSynthMouseMove(mozilla::WidgetGUIEvent* aEvent,
                                       bool aFlushOnHoverChange) = 0;
 
-  virtual void AddSizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf,
-                                      nsArenaMemoryStats* aArenaObjectsSize,
-                                      size_t* aPresShellSize,
-                                      size_t* aStyleSetsSize,
-                                      size_t* aTextRunsSize,
-                                      size_t* aPresContextSize,
-                                      size_t* aFramePropertiesSize) = 0;
+  virtual void AddSizeOfIncludingThis(nsWindowSizes& aWindowSizes) const = 0;
 
   /**
    * Methods that retrieve the cached font inflation preferences.
    */
   uint32_t FontSizeInflationEmPerLine() const {
     return mFontSizeInflationEmPerLine;
   }
 
diff --git a/layout/base/nsPresArena.cpp b/layout/base/nsPresArena.cpp
--- a/layout/base/nsPresArena.cpp
+++ b/layout/base/nsPresArena.cpp
@@ -6,23 +6,23 @@
  */
 
 /* arena allocation for the frame tree and closely-related objects */
 
 #include "nsPresArena.h"
 
 #include "mozilla/Poison.h"
 #include "nsDebug.h"
-#include "nsArenaMemoryStats.h"
 #include "nsPrintfCString.h"
 #include "GeckoStyleContext.h"
 #include "FrameLayerBuilder.h"
 #include "mozilla/ArrayUtils.h"
 #include "nsStyleContext.h"
 #include "nsStyleContextInlines.h"
+#include "nsWindowSizes.h"
 
 #include <inttypes.h>
 
 using namespace mozilla;
 
 nsPresArena::nsPresArena()
 {
 }
@@ -159,70 +159,71 @@ nsPresArena::Free(uint32_t aCode, void* 
 
   mozWritePoison(aPtr, list->mEntrySize);
 
   MOZ_MAKE_MEM_NOACCESS(aPtr, list->mEntrySize);
   list->mEntries.AppendElement(aPtr);
 }
 
 void
-nsPresArena::AddSizeOfExcludingThis(mozilla::MallocSizeOf aMallocSizeOf,
-                                    nsArenaMemoryStats* aArenaStats)
+nsPresArena::AddSizeOfExcludingThis(nsWindowSizes& aSizes) const
 {
   // We do a complicated dance here because we want to measure the
   // space taken up by the different kinds of objects in the arena,
   // but we don't have pointers to those objects.  And even if we did,
-  // we wouldn't be able to use aMallocSizeOf on them, since they were
+  // we wouldn't be able to use mMallocSizeOf on them, since they were
   // allocated out of malloc'd chunks of memory.  So we compute the
   // size of the arena as known by malloc and we add up the sizes of
   // all the objects that we care about.  Subtracting these two
   // quantities gives us a catch-all "other" number, which includes
   // slop in the arena itself as well as the size of objects that
   // we've not measured explicitly.
 
-  size_t mallocSize = mPool.SizeOfExcludingThis(aMallocSizeOf);
+  size_t mallocSize = mPool.SizeOfExcludingThis(aSizes.mState.mMallocSizeOf);
 
   size_t totalSizeInFreeLists = 0;
-  for (FreeList* entry = mFreeLists; entry != ArrayEnd(mFreeLists); ++entry) {
-    mallocSize += entry->SizeOfExcludingThis(aMallocSizeOf);
+  for (const FreeList* entry = mFreeLists;
+       entry != ArrayEnd(mFreeLists);
+       ++entry) {
+    mallocSize += entry->SizeOfExcludingThis(aSizes.mState.mMallocSizeOf);
 
     // Note that we're not measuring the size of the entries on the free
     // list here.  The free list knows how many objects we've allocated
     // ever (which includes any objects that may be on the FreeList's
     // |mEntries| at this point) and we're using that to determine the
     // total size of objects allocated with a given ID.
     size_t totalSize = entry->mEntrySize * entry->mEntriesEverAllocated;
     size_t* p;
 
     switch (entry - mFreeLists) {
-#define FRAME_ID(classname, ...)                          \
-      case nsQueryFrame::classname##_id:                  \
-        p = &aArenaStats->FRAME_ID_STAT_FIELD(classname); \
+#define FRAME_ID(classname, ...) \
+      case nsQueryFrame::classname##_id: \
+        p = &aSizes.mArenaSizes.NS_ARENA_SIZES_FIELD(classname); \
         break;
 #define ABSTRACT_FRAME_ID(...)
 #include "nsFrameIdList.h"
 #undef FRAME_ID
 #undef ABSTRACT_FRAME_ID
       case eArenaObjectID_nsLineBox:
-        p = &aArenaStats->mLineBoxes;
+        p = &aSizes.mArenaSizes.mLineBoxes;
         break;
       case eArenaObjectID_nsRuleNode:
-        p = &aArenaStats->mRuleNodes;
+        p = &aSizes.mArenaSizes.mRuleNodes;
         break;
       case eArenaObjectID_GeckoStyleContext:
-        p = &aArenaStats->mStyleContexts;
+        p = &aSizes.mArenaSizes.mStyleContexts;
         break;
 #define STYLE_STRUCT(name_, checkdata_cb_)      \
         case eArenaObjectID_nsStyle##name_:
 #include "nsStyleStructList.h"
 #undef STYLE_STRUCT
-        p = &aArenaStats->mStyleStructs;
+        p = &aSizes.mArenaSizes.mStyleStructs;
         break;
       default:
         continue;
     }
 
     *p += totalSize;
     totalSizeInFreeLists += totalSize;
   }
 
-  aArenaStats->mOther += mallocSize - totalSizeInFreeLists;
+  aSizes.mLayoutPresShellSize += mallocSize - totalSizeInFreeLists;
 }
diff --git a/layout/base/nsPresArena.h b/layout/base/nsPresArena.h
--- a/layout/base/nsPresArena.h
+++ b/layout/base/nsPresArena.h
@@ -17,17 +17,17 @@
 #include "mozilla/MemoryReporting.h"
 #include <stdint.h>
 #include "nscore.h"
 #include "nsDataHashtable.h"
 #include "nsHashKeys.h"
 #include "nsTArray.h"
 #include "nsTHashtable.h"
 
-struct nsArenaMemoryStats;
+class nsWindowSizes;
 
 class nsPresArena {
 public:
   nsPresArena();
   ~nsPresArena();
 
   /**
    * Pool allocation with recycler lists indexed by frame-type ID.
@@ -90,21 +90,20 @@ public:
    * Clears all currently registered ArenaRefPtrs for the given ArenaObjectID.
    * This is called when we reconstruct the rule tree so that style contexts
    * pointing into the old rule tree aren't released afterwards, triggering an
    * assertion in ~nsStyleContext.
    */
   void ClearArenaRefPtrs(mozilla::ArenaObjectID aObjectID);
 
   /**
-   * Increment aArenaStats with sizes of interesting objects allocated in this
-   * arena and its mOther field with the size of everything else.
+   * Increment nsWindowSizes with sizes of interesting objects allocated in this
+   * arena.
    */
-  void AddSizeOfExcludingThis(mozilla::MallocSizeOf aMallocSizeOf,
-                              nsArenaMemoryStats* aArenaStats);
+  void AddSizeOfExcludingThis(nsWindowSizes& aWindowSizes) const;
 
 private:
   void* Allocate(uint32_t aCode, size_t aSize);
   void Free(uint32_t aCode, void* aPtr);
 
   inline void ClearArenaRefPtrWithoutDeregistering(
       void* aPtr,
       mozilla::ArenaObjectID aObjectID);
