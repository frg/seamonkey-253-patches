# HG changeset patch
# User violet <violet.bugreport@gmail.com>
# Date 1555085351 0
# Node ID 5c98e7a25bc0bd99889d8f1d23bb93b222bc4698
# Parent  759028c35a3aed5a139a6474f08f7fc3dcf9ee59
Bug 1426594 - Should convert rect from TransformFramePointToTextChild to device pixel r=longsonr

We need device pixel, but TransformFramePointToTextChild returns in css pixel,
thus a scaling is necessary.

Differential Revision: https://phabricator.services.mozilla.com/D27322

diff --git a/dom/svg/test/mochitest.ini b/dom/svg/test/mochitest.ini
--- a/dom/svg/test/mochitest.ini
+++ b/dom/svg/test/mochitest.ini
@@ -33,16 +33,17 @@ support-files =
 [test_animLengthObjectIdentity.xhtml]
 [test_animLengthReadonly.xhtml]
 [test_animLengthUnits.xhtml]
 [test_bbox-with-invalid-viewBox.xhtml]
 [test_bbox.xhtml]
 [test_bbox-changes.xhtml]
 [test_bounds.html]
 [test_bug872812.html]
+[test_bug1426594.html]
 [test_getBBox-method.html]
 [test_dataTypes.html]
 [test_dataTypesModEvents.html]
 [test_fragments.html]
 [test_getCTM.html]
 [test_getElementById.xhtml]
 [test_getSubStringLength.xhtml]
 [test_lang.xhtml]
diff --git a/dom/svg/test/test_bug1426594.html b/dom/svg/test/test_bug1426594.html
new file mode 100644
--- /dev/null
+++ b/dom/svg/test/test_bug1426594.html
@@ -0,0 +1,34 @@
+<!DOCTYPE html>
+<html>
+<!--
+https://bugzilla.mozilla.org/show_bug.cgi?id=1426594
+-->
+<head>
+  <title>Test for Bug 1426594</title>
+  <script type="application/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
+  <script class="testbody" type="application/javascript">
+    SimpleTest.waitForExplicitFinish();
+
+    function runTests() {
+      let textElement = document.getElementById("textId"),
+          tspanElement = document.getElementById("tspanId");
+
+      isfuzzy(textElement.getBoundingClientRect().width, tspanElement.getBoundingClientRect().width, 5);
+      isfuzzy(textElement.getBoundingClientRect().height, tspanElement.getBoundingClientRect().height, 5);
+
+      SimpleTest.finish();
+    }
+  </script>
+</head>
+<body onload="runTests()">
+<a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=589640">Mozilla Bug 1426594</a>
+<svg height="1.5em" width="200px">
+<text id="textId" y="1em"><tspan id="tspanId">ABCDEF</tspan></text>
+</svg>
+<div style="pointer-events: none; border: 1px solid red; position: absolute;
+     z-index: 1"
+  id="highlight">
+</div>
+</body>
+</html>
diff --git a/layout/base/nsLayoutUtils.cpp b/layout/base/nsLayoutUtils.cpp
--- a/layout/base/nsLayoutUtils.cpp
+++ b/layout/base/nsLayoutUtils.cpp
@@ -3075,16 +3075,23 @@ nsLayoutUtils::TransformFrameRectToAnces
 {
   SVGTextFrame* text = GetContainingSVGTextFrame(aFrame);
 
   float srcAppUnitsPerDevPixel = aFrame->PresContext()->AppUnitsPerDevPixel();
   Rect result;
 
   if (text) {
     result = ToRect(text->TransformFrameRectFromTextChild(aRect, aFrame));
+
+    // |result| from TransformFrameRectFromTextChild() is in user space (css
+    // pixel), should convert to device pixel
+    float devPixelPerCSSPixel =
+        1.f * AppUnitsPerCSSPixel() / srcAppUnitsPerDevPixel;
+    result.Scale(devPixelPerCSSPixel);
+
     result = TransformGfxRectToAncestor(text, result, aAncestor, nullptr, aMatrixCache);
     // TransformFrameRectFromTextChild could involve any kind of transform, we
     // could drill down into it to get an answer out of it but we don't yet.
     if (aPreservesAxisAlignedRectangles)
       *aPreservesAxisAlignedRectangles = false;
   } else {
     result = Rect(NSAppUnitsToFloatPixels(aRect.x, srcAppUnitsPerDevPixel),
                   NSAppUnitsToFloatPixels(aRect.y, srcAppUnitsPerDevPixel),
