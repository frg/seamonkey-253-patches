# HG changeset patch
# User Gregory Szorc <gps@mozilla.com>
# Date 1511218021 28800
# Node ID a1040ffa24e2988c36af13aa09106494dc08274f
# Parent  489860a7e03a7113521434f2cd6699664232dcfe
Bug 1419182 - Consolidate mozconfig comparison logic into compare-mozconfigs.py; r=mshal

Hopefully this makes it more obvious how all of this is hooked
together.

MozReview-Commit-ID: 1jeyIZgHYht

diff --git a/build/compare-mozconfig/compare-mozconfigs-wrapper.py b/build/compare-mozconfig/compare-mozconfigs-wrapper.py
--- a/build/compare-mozconfig/compare-mozconfigs-wrapper.py
+++ b/build/compare-mozconfig/compare-mozconfigs-wrapper.py
@@ -2,55 +2,33 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import unicode_literals
 
 import logging
 import mozunit
+import os
 import subprocess
 import unittest
 
-from os import path
 from buildconfig import substs
 
 log = logging.getLogger(__name__)
 
-PLATFORMS = (
-    'linux32',
-    'linux64',
-    'macosx64',
-    'win32',
-    'win64',
-)
-
 
 class TestCompareMozconfigs(unittest.TestCase):
     def test_compare_mozconfigs(self):
-        """ A wrapper script that calls compare-mozconfig.py
-        based on the platform that the machine is building for"""
-        for platform in PLATFORMS:
-            log.info('Comparing platform %s' % platform)
-            python_exe = substs['PYTHON']
-            topsrcdir = substs['top_srcdir']
+        topsrcdir = substs['top_srcdir']
 
-            # construct paths and args for compare-mozconfig
-            browser_dir = path.join(topsrcdir, 'browser')
-            script_path = path.join(topsrcdir, 'build/compare-mozconfig/compare-mozconfigs.py')
-            beta_mozconfig_path = path.join(browser_dir, 'config/mozconfigs', platform, 'beta')
-            release_mozconfig_path = path.join(browser_dir, 'config/mozconfigs', platform, 'release')
-            nightly_mozconfig_path = path.join(browser_dir, 'config/mozconfigs', platform, 'nightly')
+        ret = subprocess.call([
+            substs['PYTHON'],
+            os.path.join(topsrcdir, 'build', 'compare-mozconfig',
+                         'compare-mozconfigs.py'),
+            topsrcdir
+        ])
 
-            log.info("Comparing beta against nightly mozconfigs")
-            ret_code = subprocess.call([python_exe, script_path, topsrcdir,
-                                        platform + ',' + beta_mozconfig_path +
-                                        ',' + nightly_mozconfig_path])
-            self.assertEqual(0, ret_code)
+        self.assertEqual(0, ret)
 
-            log.info("Comparing release against nightly mozconfigs")
-            ret_code = subprocess.call([python_exe, script_path, topsrcdir,
-                                        platform + ',' + release_mozconfig_path +
-                                        ',' + nightly_mozconfig_path])
-            self.assertEqual(0, ret_code)
 
 if __name__ == '__main__':
     mozunit.main()
diff --git a/build/compare-mozconfig/compare-mozconfigs.py b/build/compare-mozconfig/compare-mozconfigs.py
--- a/build/compare-mozconfig/compare-mozconfigs.py
+++ b/build/compare-mozconfig/compare-mozconfigs.py
@@ -10,16 +10,24 @@ from __future__ import unicode_literals
 import logging
 import os
 import sys
 import difflib
 
 FAILURE_CODE = 1
 SUCCESS_CODE = 0
 
+PLATFORMS = (
+    'linux32',
+    'linux64',
+    'macosx64',
+    'win32',
+    'win64',
+)
+
 log = logging.getLogger(__name__)
 
 class ConfigError(Exception):
     pass
 
 def readConfig(configfile):
     c = {}
     execfile(configfile, c)
@@ -87,43 +95,61 @@ def verify_mozconfigs(mozconfig_pair, ni
             success = False
     return success
 
 def get_mozconfig(path):
     """Consumes a path and returns a list of lines from the mozconfig file."""
     with open(path, 'rb') as fh:
         return fh.readlines()
 
+
+def compare(topsrcdir):
+    app = os.path.join(topsrcdir, 'browser')
+    whitelist = readConfig(os.path.join(app, 'config', 'mozconfigs',
+                                        'whitelist'))
+
+    success = True
+
+    for platform in PLATFORMS:
+        log.info('Comparing platform %s' % platform)
+
+        mozconfigs_path = os.path.join(app, 'config', 'mozconfigs', platform)
+
+        nightly_path = os.path.join(mozconfigs_path, 'nightly')
+        beta_path = os.path.join(mozconfigs_path, 'beta')
+        release_path = os.path.join(mozconfigs_path, 'release')
+
+        nightly_lines = get_mozconfig(nightly_path)
+        beta_lines = get_mozconfig(beta_path)
+        release_lines = get_mozconfig(release_path)
+
+        log.info('Comparing beta and nightly mozconfigs')
+        passed = verify_mozconfigs((beta_path, beta_lines),
+                                   (nightly_path, nightly_lines),
+                                   platform,
+                                   whitelist)
+
+        if not passed:
+            success = False
+
+        log.info('Comparing release and nightly mozconfigs')
+        passed = verify_mozconfigs((release_path, release_lines),
+                                   (nightly_path, nightly_lines),
+                                   platform,
+                                   whitelist)
+        if not passed:
+            success = False
+
+    return success
+
+
 if __name__ == '__main__':
     import argparse
 
     parser = argparse.ArgumentParser()
     parser.add_argument('topsrcdir', help='Path to root of source checkout')
-    parser.add_argument('args', nargs='*')
 
     args = parser.parse_args()
 
     logging.basicConfig(level=logging.INFO)
 
-    whitelist = os.path.join(args.topsrcdir, 'browser', 'config', 'mozconfigs',
-                             'whitelist')
-
-    mozconfig_whitelist = readConfig(whitelist)
-
-    for arg in args.args:
-        platform, mozconfig_path, nightly_mozconfig_path = arg.split(',')
-
-        mozconfig_lines = get_mozconfig(mozconfig_path)
-        nightly_mozconfig_lines = get_mozconfig(nightly_mozconfig_path)
-
-        mozconfig_pair = (mozconfig_path, mozconfig_lines)
-        nightly_mozconfig_pair = (nightly_mozconfig_path,
-                                  nightly_mozconfig_lines)
-
-        passed = verify_mozconfigs(mozconfig_pair, nightly_mozconfig_pair,
-                                   platform, mozconfig_whitelist)
-
-        if passed:
-            logging.info('Mozconfig check passed!')
-        else:
-            logging.error('Mozconfig check failed!')
-            sys.exit(FAILURE_CODE)
-    sys.exit(SUCCESS_CODE)
+    if not compare(args.topsrcdir):
+        sys.exit(1)

