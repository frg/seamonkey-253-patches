# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1501852505 -7200
#      Fri Aug 04 15:15:05 2017 +0200
# Node ID 24fad9deb6a98e39a73b62cc4dd609bc1cda59fa
# Parent  ec87eae3cebc76d4dcf085cc94a5be56fe7f9eb3
Bug 1386555 - Inline AutoGeckoProfilerEntry constructor and destructor. r=anba

diff --git a/js/src/jsarray.cpp b/js/src/jsarray.cpp
--- a/js/src/jsarray.cpp
+++ b/js/src/jsarray.cpp
@@ -38,16 +38,17 @@
 #include "vm/TypedArrayObject.h"
 #include "vm/WrapperObject.h"
 
 #include "jsatominlines.h"
 
 #include "vm/ArgumentsObject-inl.h"
 #include "vm/ArrayObject-inl.h"
 #include "vm/Caches-inl.h"
+#include "vm/GeckoProfiler-inl.h"
 #include "vm/Interpreter-inl.h"
 #include "vm/NativeObject-inl.h"
 #include "vm/UnboxedObject-inl.h"
 
 using namespace js;
 using namespace js::gc;
 
 using mozilla::Abs;
diff --git a/js/src/vm/GeckoProfiler-inl.h b/js/src/vm/GeckoProfiler-inl.h
--- a/js/src/vm/GeckoProfiler-inl.h
+++ b/js/src/vm/GeckoProfiler-inl.h
@@ -42,11 +42,43 @@ class MOZ_RAII AutoSuppressProfilerSampl
 
   private:
     JSContext* cx_;
     bool previouslyEnabled_;
     JSRuntime::AutoProhibitActiveContextChange prohibitContextChange_;
     MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 };
 
+MOZ_ALWAYS_INLINE
+AutoGeckoProfilerEntry::AutoGeckoProfilerEntry(JSContext* cx, const char* label,
+                                               ProfileEntry::Category category
+                                               MOZ_GUARD_OBJECT_NOTIFIER_PARAM_IN_IMPL)
+  : profiler_(&cx->geckoProfiler())
+{
+    MOZ_GUARD_OBJECT_NOTIFIER_INIT;
+    if (MOZ_LIKELY(!profiler_->installed())) {
+        profiler_ = nullptr;
+        return;
+    }
+#ifdef DEBUG
+    spBefore_ = profiler_->stackPointer();
+#endif
+    profiler_->pseudoStack_->pushCppFrame(label,
+                                          /* dynamicString = */ nullptr,
+                                          /* sp = */ this,
+                                          /* line = */ 0,
+                                          ProfileEntry::Kind::CPP_NORMAL,
+                                          category);
+}
+
+MOZ_ALWAYS_INLINE
+AutoGeckoProfilerEntry::~AutoGeckoProfilerEntry()
+{
+    if (MOZ_LIKELY(!profiler_))
+        return;
+
+    profiler_->pseudoStack_->pop();
+    MOZ_ASSERT(spBefore_ == profiler_->stackPointer());
+}
+
 } // namespace js
 
 #endif // vm_GeckoProfiler_inl_h
diff --git a/js/src/vm/GeckoProfiler.cpp b/js/src/vm/GeckoProfiler.cpp
--- a/js/src/vm/GeckoProfiler.cpp
+++ b/js/src/vm/GeckoProfiler.cpp
@@ -403,42 +403,16 @@ GeckoProfilerEntryMarker::~GeckoProfiler
     if (profiler == nullptr)
         return;
 
     profiler->pseudoStack_->pop();    // the JS frame
     profiler->pseudoStack_->pop();    // the BEGIN_PSEUDO_JS frame
     MOZ_ASSERT(spBefore_ == profiler->stackPointer());
 }
 
-AutoGeckoProfilerEntry::AutoGeckoProfilerEntry(JSContext* cx, const char* label,
-                                               ProfileEntry::Category category
-                                               MOZ_GUARD_OBJECT_NOTIFIER_PARAM_IN_IMPL)
-    : profiler_(&cx->geckoProfiler())
-{
-    MOZ_GUARD_OBJECT_NOTIFIER_INIT;
-    if (!profiler_->installed()) {
-        profiler_ = nullptr;
-        return;
-    }
-    spBefore_ = profiler_->stackPointer();
-
-    profiler_->pseudoStack_->pushCppFrame(
-        label, /* dynamicString = */ nullptr, /* sp = */ this, /* line = */ 0,
-        ProfileEntry::Kind::CPP_NORMAL, category);
-}
-
-AutoGeckoProfilerEntry::~AutoGeckoProfilerEntry()
-{
-    if (!profiler_)
-        return;
-
-    profiler_->pseudoStack_->pop();
-    MOZ_ASSERT(spBefore_ == profiler_->stackPointer());
-}
-
 GeckoProfilerBaselineOSRMarker::GeckoProfilerBaselineOSRMarker(JSContext* cx, bool hasProfilerFrame
                                                                MOZ_GUARD_OBJECT_NOTIFIER_PARAM_IN_IMPL)
     : profiler(&cx->geckoProfiler())
 {
     MOZ_GUARD_OBJECT_NOTIFIER_INIT;
     if (!hasProfilerFrame || !cx->runtime()->geckoProfiler().enabled()) {
         profiler = nullptr;
         return;
diff --git a/js/src/vm/GeckoProfiler.h b/js/src/vm/GeckoProfiler.h
--- a/js/src/vm/GeckoProfiler.h
+++ b/js/src/vm/GeckoProfiler.h
@@ -223,24 +223,27 @@ class MOZ_RAII GeckoProfilerEntryMarker
 /*
  * RAII class to automatically add Gecko Profiler pseudo frame entries.
  *
  * NB: The `label` string must be statically allocated.
  */
 class MOZ_NONHEAP_CLASS AutoGeckoProfilerEntry
 {
   public:
-    explicit AutoGeckoProfilerEntry(JSContext* cx, const char* label,
-                                    ProfileEntry::Category category = ProfileEntry::Category::JS
-                                    MOZ_GUARD_OBJECT_NOTIFIER_PARAM);
-    ~AutoGeckoProfilerEntry();
+    explicit MOZ_ALWAYS_INLINE
+    AutoGeckoProfilerEntry(JSContext* cx, const char* label,
+                           ProfileEntry::Category category = ProfileEntry::Category::JS
+                           MOZ_GUARD_OBJECT_NOTIFIER_PARAM);
+    MOZ_ALWAYS_INLINE ~AutoGeckoProfilerEntry();
 
   private:
     GeckoProfilerThread* profiler_;
-    mozilla::DebugOnly<uint32_t> spBefore_;
+#ifdef DEBUG
+    uint32_t spBefore_;
+#endif
     MOZ_DECL_USE_GUARD_OBJECT_NOTIFIER
 };
 
 /*
  * This class is used in the interpreter to bound regions where the baseline JIT
  * being entered via OSR.  It marks the current top pseudostack entry as
  * OSR-ed
  */
diff --git a/js/src/vm/SavedStacks.cpp b/js/src/vm/SavedStacks.cpp
--- a/js/src/vm/SavedStacks.cpp
+++ b/js/src/vm/SavedStacks.cpp
@@ -31,16 +31,17 @@
 #include "vm/GeckoProfiler.h"
 #include "vm/SavedFrame.h"
 #include "vm/StringBuffer.h"
 #include "vm/Time.h"
 #include "vm/WrapperObject.h"
 
 #include "jscntxtinlines.h"
 
+#include "vm/GeckoProfiler-inl.h"
 #include "vm/NativeObject-inl.h"
 #include "vm/Stack-inl.h"
 
 using mozilla::AddToHash;
 using mozilla::DebugOnly;
 using mozilla::HashString;
 using mozilla::Maybe;
 using mozilla::Move;
@@ -1168,17 +1169,17 @@ SavedStacks::saveCurrentStack(JSContext*
         cx->isExceptionPending() ||
         !cx->global() ||
         !cx->global()->isStandardClassResolved(JSProto_Object))
     {
         frame.set(nullptr);
         return true;
     }
 
-    AutoGeckoProfilerEntry psuedoFrame(cx, "js::SavedStacks::saveCurrentStack");
+    AutoGeckoProfilerEntry pseudoFrame(cx, "js::SavedStacks::saveCurrentStack");
     FrameIter iter(cx);
     return insertFrames(cx, iter, frame, mozilla::Move(capture));
 }
 
 bool
 SavedStacks::copyAsyncStack(JSContext* cx, HandleObject asyncStack, HandleString asyncCause,
                             MutableHandleSavedFrame adoptedStack, uint32_t maxFrameCount)
 {
diff --git a/js/src/vm/String.cpp b/js/src/vm/String.cpp
--- a/js/src/vm/String.cpp
+++ b/js/src/vm/String.cpp
@@ -16,16 +16,18 @@
 #include "gc/Marking.h"
 #include "js/GCAPI.h"
 #include "js/UbiNode.h"
 #include "vm/GeckoProfiler.h"
 
 #include "jscntxtinlines.h"
 #include "jscompartmentinlines.h"
 
+#include "vm/GeckoProfiler-inl.h"
+
 using namespace js;
 
 using mozilla::IsSame;
 using mozilla::PodCopy;
 using mozilla::PodEqual;
 using mozilla::RangedPtr;
 using mozilla::RoundUpPow2;
 using mozilla::Unused;
