# HG changeset patch
# User Josh Matthews <josh@joshmatthews.net>
# Date 1505504610 25200
#      Fri Sep 15 12:43:30 2017 -0700
# Node ID 13ec4ded3b4ed25913047b1bed312ef67011fc1f
# Parent  f3622439a0fcc47a4870653a681d10810ee9b5ff
servo: Merge #18516 - Share specified URLs with Gecko (from jdm:stringshare); r=heycam

This makes use of our Arc offset machinery to allow Gecko to store Arc<String> values and ensure there are used appropriately.

---
- [x] `./mach build -d` does not report any errors
- [x] `./mach test-tidy` does not report any errors
- [x] These changes fix [bug 1397971](https://bugzilla.mozilla.org/show_bug.cgi?id=1397971).

Source-Repo: https://github.com/servo/servo
Source-Revision: 70a6b2fa91ed377ce5d0d1696bfb1503be8aa454

diff --git a/servo/components/style/gecko/url.rs b/servo/components/style/gecko/url.rs
--- a/servo/components/style/gecko/url.rs
+++ b/servo/components/style/gecko/url.rs
@@ -1,41 +1,40 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Common handling for the specified value CSS url() values.
 
 use gecko_bindings::structs::{ServoBundledURI, URLExtraData};
 use gecko_bindings::structs::mozilla::css::URLValueData;
+use gecko_bindings::structs::root::{nsStyleImageRequest, RustString};
 use gecko_bindings::structs::root::mozilla::css::ImageValue;
-use gecko_bindings::structs::root::nsStyleImageRequest;
 use gecko_bindings::sugar::refptr::RefPtr;
+use malloc_size_of::{MallocSizeOf, MallocSizeOfOps};
 use parser::ParserContext;
-use servo_arc::Arc;
+use servo_arc::{Arc, RawOffsetArc};
 use std::fmt;
+use std::mem;
 use style_traits::{ToCss, ParseError};
 
 /// A specified url() value for gecko. Gecko does not eagerly resolve SpecifiedUrls.
-#[derive(Clone, Debug, MallocSizeOf, PartialEq)]
+#[derive(Clone, Debug, PartialEq)]
 pub struct SpecifiedUrl {
     /// The URL in unresolved string form.
     ///
     /// Refcounted since cloning this should be cheap and data: uris can be
     /// really large.
-    #[ignore_malloc_size_of = "XXX: do this once bug 1397971 lands"]
     serialization: Arc<String>,
 
     /// The URL extra data.
-    #[ignore_malloc_size_of = "RefPtr is tricky, and there aren't many of these in practise"]
     pub extra_data: RefPtr<URLExtraData>,
 
     /// Cache ImageValue, if any, so that we can reuse it while rematching a
     /// a property with this specified url value.
-    #[ignore_malloc_size_of = "XXX: do this once bug 1397971 lands"]
     pub image_value: Option<RefPtr<ImageValue>>,
 }
 trivial_to_computed_value!(SpecifiedUrl);
 
 impl SpecifiedUrl {
     /// Try to parse a URL from a string value that is a valid CSS token for a
     /// URL.
     ///
@@ -56,17 +55,24 @@ impl SpecifiedUrl {
     pub fn is_invalid(&self) -> bool {
         false
     }
 
     /// Convert from URLValueData to SpecifiedUrl.
     pub unsafe fn from_url_value_data(url: &URLValueData)
                                        -> Result<SpecifiedUrl, ()> {
         Ok(SpecifiedUrl {
-            serialization: Arc::new(url.mString.to_string()),
+            serialization: if url.mUsingRustString {
+                let arc_type = url.mStrings.mRustString.as_ref()
+                    as *const _ as
+                    *const RawOffsetArc<String>;
+                Arc::from_raw_offset((*arc_type).clone())
+            } else {
+                Arc::new(url.mStrings.mString.as_ref().to_string())
+            },
             extra_data: url.mExtraData.to_safe(),
             image_value: None,
         })
     }
 
     /// Convert from nsStyleImageRequest to SpecifiedUrl.
     pub unsafe fn from_image_request(image_request: &nsStyleImageRequest) -> Result<SpecifiedUrl, ()> {
         if image_request.mImageValue.mRawPtr.is_null() {
@@ -112,24 +118,49 @@ impl SpecifiedUrl {
 
     /// Build and carry an image value on request.
     pub fn build_image_value(&mut self) {
         use gecko_bindings::bindings::Gecko_ImageValue_Create;
 
         debug_assert_eq!(self.image_value, None);
         self.image_value = {
             unsafe {
-                let ptr = Gecko_ImageValue_Create(self.for_ffi());
+                let arc_offset = Arc::into_raw_offset(self.serialization.clone());
+                let ptr = Gecko_ImageValue_Create(
+                    self.for_ffi(),
+                    mem::transmute::<_, RawOffsetArc<RustString>>(arc_offset));
                 // We do not expect Gecko_ImageValue_Create returns null.
                 debug_assert!(!ptr.is_null());
                 Some(RefPtr::from_addrefed(ptr))
             }
         }
     }
 }
 
 impl ToCss for SpecifiedUrl {
     fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
         dest.write_str("url(")?;
         self.serialization.to_css(dest)?;
         dest.write_str(")")
     }
 }
+
+impl MallocSizeOf for SpecifiedUrl {
+    fn size_of(&self, _ops: &mut MallocSizeOfOps) -> usize {
+        use gecko_bindings::bindings::Gecko_ImageValue_SizeOfIncludingThis;
+
+        let mut n = 0;
+
+        // XXX: measure `serialization` once bug 1397971 lands
+
+        // We ignore `extra_data`, because RefPtr is tricky, and there aren't
+        // many of them in practise (sharing is common).
+
+        if let Some(ref image_value) = self.image_value {
+            // Although this is a RefPtr, this is the primary reference because
+            // SpecifiedUrl is responsible for creating the image_value. So we
+            // measure unconditionally here.
+            n += unsafe { Gecko_ImageValue_SizeOfIncludingThis(image_value.clone().get()) };
+        }
+
+        n
+    }
+}
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -8,16 +8,17 @@ use env_logger::LogBuilder;
 use malloc_size_of::MallocSizeOfOps;
 use selectors::Element;
 use selectors::matching::{MatchingContext, MatchingMode, matches_selector};
 use servo_arc::{Arc, ArcBorrow, RawOffsetArc};
 use std::cell::RefCell;
 use std::env;
 use std::fmt::Write;
 use std::iter;
+use std::mem;
 use std::ptr;
 use style::applicable_declarations::ApplicableDeclarationBlock;
 use style::context::{CascadeInputs, QuirksMode, SharedStyleContext, StyleContext};
 use style::context::ThreadLocalStyleContext;
 use style::data::ElementStyles;
 use style::dom::{ShowSubtreeData, TElement, TNode};
 use style::driver;
 use style::element_state::ElementState;
@@ -72,17 +73,17 @@ use style::gecko_bindings::bindings::Raw
 use style::gecko_bindings::bindings::RawServoStyleSet;
 use style::gecko_bindings::bindings::ServoStyleContextBorrowedOrNull;
 use style::gecko_bindings::bindings::nsTArrayBorrowed_uintptr_t;
 use style::gecko_bindings::bindings::nsTimingFunctionBorrowed;
 use style::gecko_bindings::bindings::nsTimingFunctionBorrowedMut;
 use style::gecko_bindings::structs;
 use style::gecko_bindings::structs::{CSSPseudoElementType, CompositeOperation};
 use style::gecko_bindings::structs::{Loader, LoaderReusableStyleSheets};
-use style::gecko_bindings::structs::{RawServoStyleRule, ServoStyleContextStrong};
+use style::gecko_bindings::structs::{RawServoStyleRule, ServoStyleContextStrong, RustString};
 use style::gecko_bindings::structs::{ServoStyleSheet, SheetParsingMode, nsIAtom, nsCSSPropertyID};
 use style::gecko_bindings::structs::{nsCSSFontFaceRule, nsCSSCounterStyleRule};
 use style::gecko_bindings::structs::{nsRestyleHint, nsChangeHint, PropertyValuePair};
 use style::gecko_bindings::structs::IterationCompositeOperation;
 use style::gecko_bindings::structs::MallocSizeOf as GeckoMallocSizeOf;
 use style::gecko_bindings::structs::OriginFlags;
 use style::gecko_bindings::structs::OriginFlags_Author;
 use style::gecko_bindings::structs::OriginFlags_User;
@@ -3830,16 +3831,41 @@ pub extern "C" fn Servo_GetCustomPropert
 
     let name = unsafe { name.as_mut().unwrap() };
     name.assign(&*property_name.as_slice());
 
     true
 }
 
 #[no_mangle]
+pub unsafe extern "C" fn Servo_ReleaseArcStringData(string: *const RawOffsetArc<RustString>) {
+    let string = string as *const RawOffsetArc<String>;
+    // Cause RawOffsetArc::drop to run, releasing the strong reference to the string data.
+    let _ = ptr::read(string);
+}
+
+#[no_mangle]
+pub unsafe extern "C" fn Servo_CloneArcStringData(string: *const RawOffsetArc<RustString>)
+                                                  -> RawOffsetArc<RustString> {
+    let string = string as *const RawOffsetArc<String>;
+    let cloned = (*string).clone();
+    mem::transmute::<_, RawOffsetArc<RustString>>(cloned)
+}
+
+#[no_mangle]
+pub unsafe extern "C" fn Servo_GetArcStringData(string: *const RustString,
+                                                utf8_chars: *mut *const u8,
+                                                utf8_len: *mut u32)
+{
+    let string = &*(string as *const String);
+    *utf8_len = string.len() as u32;
+    *utf8_chars = string.as_ptr();
+}
+
+#[no_mangle]
 pub extern "C" fn Servo_ProcessInvalidations(set: RawServoStyleSetBorrowed,
                                              element: RawGeckoElementBorrowed,
                                              snapshots: *const ServoElementSnapshotTable) {
     debug_assert!(!snapshots.is_null());
 
     let element = GeckoElement(element);
     debug_assert!(element.has_snapshot());
     debug_assert!(!element.handled_snapshot());
