# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1503079810 25200
#      Fri Aug 18 11:10:10 2017 -0700
# Node ID 114ff6330fcb2879d0092a10777a018a4ed2d495
# Parent  ed306bb46d8001781255fb28097bc600cd9f66dc
Bug 1391405: Part 1 - Add WebIDL versions of much used Components.utils helpers. r=gabor,qdot

In the code that I'm profiling, the XPC WrappedNative overhead of calling
these functions adds up to about a quarter of the time spent executing the
code. The overhead of the WebIDL versions is negligible.

MozReview-Commit-ID: 30qJy5RtP9d

diff --git a/dom/base/ChromeUtils.cpp b/dom/base/ChromeUtils.cpp
--- a/dom/base/ChromeUtils.cpp
+++ b/dom/base/ChromeUtils.cpp
@@ -1,15 +1,18 @@
 /* -*-  Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2; -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ChromeUtils.h"
 
+#include "jsfriendapi.h"
+#include "WrapperFactory.h"
+
 #include "mozilla/Base64.h"
 #include "mozilla/BasePrincipal.h"
 #include "mozJSComponentLoader.h"
 
 namespace mozilla {
 namespace dom {
 
 /* static */ void
@@ -123,16 +126,63 @@ ChromeUtils::Base64URLDecode(GlobalObjec
   if (NS_WARN_IF(!buffer)) {
     aRv.Throw(NS_ERROR_OUT_OF_MEMORY);
     return;
   }
   aRetval.set(buffer);
 }
 
 /* static */ void
+ChromeUtils::WaiveXrays(GlobalObject& aGlobal,
+                        JS::HandleValue aVal,
+                        JS::MutableHandleValue aRetval,
+                        ErrorResult& aRv)
+{
+  JS::RootedValue value(aGlobal.Context(), aVal);
+  if (!xpc::WrapperFactory::WaiveXrayAndWrap(aGlobal.Context(), &value)) {
+    aRv.NoteJSContextException(aGlobal.Context());
+  } else {
+    aRetval.set(value);
+  }
+}
+
+/* static */ void
+ChromeUtils::UnwaiveXrays(GlobalObject& aGlobal,
+                          JS::HandleValue aVal,
+                          JS::MutableHandleValue aRetval,
+                          ErrorResult& aRv)
+{
+  if (!aVal.isObject()) {
+      aRetval.set(aVal);
+      return;
+  }
+
+  JS::RootedObject obj(aGlobal.Context(), js::UncheckedUnwrap(&aVal.toObject()));
+  if (!JS_WrapObject(aGlobal.Context(), &obj)) {
+    aRv.NoteJSContextException(aGlobal.Context());
+  } else {
+    aRetval.setObject(*obj);
+  }
+}
+
+/* static */ void
+ChromeUtils::GetClassName(GlobalObject& aGlobal,
+                          JS::HandleObject aObj,
+                          bool aUnwrap,
+                          nsAString& aRetval)
+{
+  JS::RootedObject obj(aGlobal.Context(), aObj);
+  if (aUnwrap) {
+    obj = js::UncheckedUnwrap(obj, /* stopAtWindowProxy = */ false);
+  }
+
+  aRetval = NS_ConvertUTF8toUTF16(nsDependentCString(js::GetObjectClass(obj)->name));
+}
+
+/* static */ void
 ChromeUtils::Import(const GlobalObject& aGlobal,
                     const nsAString& aResourceURI,
                     const Optional<JS::Handle<JSObject*>>& aTargetObj,
                     JS::MutableHandle<JSObject*> aRetval,
                     ErrorResult& aRv)
 {
 
   RefPtr<mozJSComponentLoader> moduleloader = mozJSComponentLoader::Get();
diff --git a/dom/base/ChromeUtils.h b/dom/base/ChromeUtils.h
--- a/dom/base/ChromeUtils.h
+++ b/dom/base/ChromeUtils.h
@@ -115,16 +115,31 @@ public:
 
   // Implemented in js/xpconnect/loader/ChromeScriptLoader.cpp
   static already_AddRefed<Promise>
   CompileScript(GlobalObject& aGlobal,
                 const nsAString& aUrl,
                 const dom::CompileScriptOptionsDictionary& aOptions,
                 ErrorResult& aRv);
 
+  static void WaiveXrays(GlobalObject& aGlobal,
+                         JS::HandleValue aVal,
+                         JS::MutableHandleValue aRetval,
+                         ErrorResult& aRv);
+
+  static void UnwaiveXrays(GlobalObject& aGlobal,
+                           JS::HandleValue aVal,
+                           JS::MutableHandleValue aRetval,
+                           ErrorResult& aRv);
+
+  static void GetClassName(GlobalObject& aGlobal,
+                           JS::HandleObject aObj,
+                           bool aUnwrap,
+                           nsAString& aRetval);
+
   static void Import(const GlobalObject& aGlobal,
                      const nsAString& aResourceURI,
                      const Optional<JS::Handle<JSObject*>>& aTargetObj,
                      JS::MutableHandle<JSObject*> aRetval,
                      ErrorResult& aRv);
 
   static void DefineModuleGetter(const GlobalObject& global,
                                  JS::Handle<JSObject*> target,
diff --git a/dom/webidl/ChromeUtils.webidl b/dom/webidl/ChromeUtils.webidl
--- a/dom/webidl/ChromeUtils.webidl
+++ b/dom/webidl/ChromeUtils.webidl
@@ -155,16 +155,37 @@ partial namespace ChromeUtils {
    * which may be used to execute it repeatedly, in different globals, without
    * re-parsing.
    */
   [NewObject]
   Promise<PrecompiledScript>
   compileScript(DOMString url, optional CompileScriptOptionsDictionary options);
 
   /**
+   * Waive Xray on a given value. Identity op for primitives.
+   */
+  [Throws]
+  any waiveXrays(any val);
+
+  /**
+   * Strip off Xray waivers on a given value. Identity op for primitives.
+   */
+  [Throws]
+  any unwaiveXrays(any val);
+
+  /**
+   * Gets the name of the JSClass of the object.
+   *
+   * if |unwrap| is true, all wrappers are unwrapped first. Unless you're
+   * specifically trying to detect whether the object is a proxy, this is
+   * probably what you want.
+   */
+  DOMString getClassName(object obj, optional boolean unwrap = true);
+
+  /**
    * Synchronously loads and evaluates the js file located at
    * 'aResourceURI' with a new, fully privileged global object.
    *
    * If 'aTargetObj' is specified and null, this method just returns
    * the module's global object. Otherwise (if 'aTargetObj' is not
    * specified, or 'aTargetObj' is != null) looks for a property
    * 'EXPORTED_SYMBOLS' on the new global object. 'EXPORTED_SYMBOLS'
    * is expected to be an array of strings identifying properties on
