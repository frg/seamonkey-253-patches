# HG changeset patch
# User Gregory Szorc <gps@mozilla.com>
# Date 1504112556 25200
# Node ID 2e2062e0bc3691e9a85b0925a9055d0d51ca5935
# Parent  5e6a8c0a1d7509610ab7d8c4c09cc96172be0418
Bug 1395079 - Query version of watchman without using daemon; r=mshal

See inline comment for why.

We may want a follow-up configure check for whether watchman is
usable (whether we can communicate with the daemon). This can be
deferred to another bug.

MozReview-Commit-ID: IHfyn7v7vm8

diff --git a/moz.configure b/moz.configure
--- a/moz.configure
+++ b/moz.configure
@@ -391,36 +391,32 @@ def tup_progs(build_backends):
 tup = check_prog('TUP', tup_progs)
 
 # watchman detection
 # ==============================================================
 
 option(env='WATCHMAN', nargs=1, help='Path to the watchman program')
 
 @depends('WATCHMAN')
-@imports('json')
 def watchman_info(prog):
     if not prog:
         prog = find_program('watchman')
 
     if not prog:
         return
 
-    out = check_cmd_output(prog, 'version', onerror=lambda: None)
+    # `watchman version` will talk to the Watchman daemon service.
+    # This can hang due to permissions problems. e.g.
+    # https://github.com/facebook/watchman/issues/376. So use
+    # `watchman --version` to prevent a class of failures.
+    out = check_cmd_output(prog, '--version', onerror=lambda: None)
     if out is None:
         return
 
-    # Assume a process not emitting JSON is not watchman or is a
-    # broken watchman.
-    try:
-        res = json.loads(out)
-    except ValueError:
-        return
-
-    return namespace(path=prog, version=Version(res['version']))
+    return namespace(path=prog, version=Version(out.strip()))
 
 @depends_if(watchman_info)
 @checking('for watchman')
 def watchman(w):
     return w.path
 
 @depends_if(watchman_info)
 @checking('for watchman version')
