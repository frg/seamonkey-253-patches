# HG changeset patch
# User Michael Layzell <michael@thelayzells.com>
# Date 1505158516 14400
#      Mon Sep 11 15:35:16 2017 -0400
# Node ID 81c5d6a866100e6c2d74b1ef584633fb5ba31295
# Parent  58fb9fc36c38e92563b86db154616849b549c74c
Bug 1398883 - Disable the DataTransfer::Protected state for Firefox 57, r=baku

This isn't a super essential feature, and is just a change to try to bring us in
line with chromium and the spec. As this has apparent web compat issues, and
DataTransfer is a hard to test area, this patch moves the changes behind a pref,
which we can come back to turning on after we ship 57.

diff --git a/dom/base/nsCopySupport.cpp b/dom/base/nsCopySupport.cpp
--- a/dom/base/nsCopySupport.cpp
+++ b/dom/base/nsCopySupport.cpp
@@ -832,18 +832,26 @@ nsCopySupport::FireClipboardEvent(EventM
     doDefault = (status != nsEventStatus_eConsumeNoDefault);
   }
 
   // When this function exits, the event dispatch is over. We want to disconnect
   // our DataTransfer, which means setting its mode to `Protected` and clearing
   // all stored data, before we return.
   auto clearAfter = MakeScopeExit([&] {
     if (clipboardData) {
-      clipboardData->SetMode(DataTransfer::Mode::Protected);
-      clipboardData->ClearAll();
+      clipboardData->Disconnect();
+
+      // NOTE: Disconnect may not actually clear the DataTransfer if the
+      // dom.events.dataTransfer.protected.enabled pref is not on, so we make
+      // sure we clear here, as not clearing could provide the DataTransfer
+      // access to information from the system clipboard at an arbitrary point
+      // in the future.
+      if (originalEventMessage == ePaste) {
+        clipboardData->ClearAll();
+      }
     }
   });
 
   // No need to do anything special during a paste. Either an event listener
   // took care of it and cancelled the event, or the caller will handle it.
   // Return true to indicate that the event wasn't cancelled.
   if (originalEventMessage == ePaste) {
     if (aActionTaken) {
diff --git a/dom/events/DataTransfer.cpp b/dom/events/DataTransfer.cpp
--- a/dom/events/DataTransfer.cpp
+++ b/dom/events/DataTransfer.cpp
@@ -76,16 +76,33 @@ const char DataTransfer::sEffects[8][9] 
 };
 
 // Used for custom clipboard types.
 enum CustomClipboardTypeId {
   eCustomClipboardTypeId_None,
   eCustomClipboardTypeId_String
 };
 
+// The dom.events.dataTransfer.protected.enabled preference controls whether or
+// not the `protected` dataTransfer state is enabled. If the `protected`
+// dataTransfer stae is disabled, then the DataTransfer will be read-only
+// whenever it should be protected, and will not be disconnected after a drag
+// event is completed.
+static bool
+PrefProtected()
+{
+  static bool sInitialized = false;
+  static bool sValue = false;
+  if (!sInitialized) {
+    sInitialized = true;
+    Preferences::AddBoolVarCache(&sValue, "dom.events.dataTransfer.protected.enabled");
+  }
+  return sValue;
+}
+
 static DataTransfer::Mode
 ModeForEvent(EventMessage aEventMessage)
 {
   switch (aEventMessage) {
   case eCut:
   case eCopy:
   case eDragStart:
     // For these events, we want to be able to add data to the data transfer,
@@ -93,17 +110,19 @@ ModeForEvent(EventMessage aEventMessage)
     return DataTransfer::Mode::ReadWrite;
   case eDrop:
   case ePaste:
   case ePasteNoFormatting:
     // For these events we want to be able to read the data which is stored in
     // the DataTransfer, rather than just the type information.
     return DataTransfer::Mode::ReadOnly;
   default:
-    return DataTransfer::Mode::Protected;
+    return PrefProtected()
+      ? DataTransfer::Mode::Protected
+      : DataTransfer::Mode::ReadOnly;
   }
 }
 
 DataTransfer::DataTransfer(nsISupports* aParent, EventMessage aEventMessage,
                            bool aIsExternal, int32_t aClipboardType)
   : mParent(aParent)
   , mDropEffect(nsIDragService::DRAGDROP_ACTION_NONE)
   , mEffectAllowed(nsIDragService::DRAGDROP_ACTION_UNINITIALIZED)
@@ -1272,16 +1291,25 @@ DataTransfer::ConvertFromVariant(nsIVari
 
   // each character is two bytes
   *aLength = str.Length() << 1;
 
   return true;
 }
 
 void
+DataTransfer::Disconnect()
+{
+  SetMode(Mode::Protected);
+  if (PrefProtected()) {
+    ClearAll();
+  }
+}
+
+void
 DataTransfer::ClearAll()
 {
   mItems->ClearAllItems();
 }
 
 uint32_t
 DataTransfer::MozItemCount() const
 {
@@ -1603,10 +1631,20 @@ DataTransfer::FillInExternalCustomTypes(
       rv = variant->SetAsAString(data);
       NS_ENSURE_SUCCESS_VOID(rv);
 
       SetDataWithPrincipal(format, variant, aIndex, aPrincipal);
     }
   } while (type != eCustomClipboardTypeId_None);
 }
 
+void
+DataTransfer::SetMode(DataTransfer::Mode aMode)
+{
+  if (!PrefProtected() && aMode == Mode::Protected) {
+    mMode = Mode::ReadOnly;
+  } else {
+    mMode = aMode;
+  }
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/events/DataTransfer.h b/dom/events/DataTransfer.h
--- a/dom/events/DataTransfer.h
+++ b/dom/events/DataTransfer.h
@@ -224,19 +224,17 @@ public:
   }
 
   // Returns the current "Drag Data Store Mode" of the DataTransfer. This
   // determines what modifications may be performed on the DataTransfer, and
   // what data may be read from it.
   Mode GetMode() const {
     return mMode;
   }
-  void SetMode(Mode aMode) {
-    mMode = aMode;
-  }
+  void SetMode(Mode aMode);
 
   // Helper method. Is true if the DataTransfer's mode is ReadOnly or Protected,
   // which means that the DataTransfer cannot be modified.
   bool IsReadOnly() const {
     return mMode != Mode::ReadWrite;
   }
   // Helper method. Is true if the DataTransfer's mode is Protected, which means
   // that DataTransfer type information may be read, but data may not be.
@@ -267,16 +265,22 @@ public:
   GetTransferable(uint32_t aIndex, nsILoadContext* aLoadContext);
 
   // converts the data in the variant to an nsISupportString if possible or
   // an nsISupports or null otherwise.
   bool ConvertFromVariant(nsIVariant* aVariant,
                           nsISupports** aSupports,
                           uint32_t* aLength) const;
 
+  // Disconnects the DataTransfer from the Drag Data Store. If the
+  // dom.dataTransfer.disconnect pref is enabled, this will clear the
+  // DataTransfer and set it to the `Protected` state, otherwise this method is
+  // a no-op.
+  void Disconnect();
+
   // clears all of the data
   void ClearAll();
 
   // Similar to SetData except also specifies the principal to store.
   // aData may be null when called from CacheExternalDragFormats or
   // CacheExternalClipboardFormats.
   nsresult SetDataWithPrincipal(const nsAString& aFormat,
                                 nsIVariant* aData,
diff --git a/dom/events/EventStateManager.cpp b/dom/events/EventStateManager.cpp
--- a/dom/events/EventStateManager.cpp
+++ b/dom/events/EventStateManager.cpp
@@ -1772,18 +1772,17 @@ EventStateManager::GenerateDragGesture(n
       nsCOMPtr<nsPIDOMWindowOuter> window = docshell->GetWindow();
       if (!window)
         return;
 
       RefPtr<DataTransfer> dataTransfer =
         new DataTransfer(window, eDragStart, false, -1);
       auto protectDataTransfer = MakeScopeExit([&] {
         if (dataTransfer) {
-          dataTransfer->SetMode(DataTransfer::Mode::Protected);
-          dataTransfer->ClearAll();
+          dataTransfer->Disconnect();
         }
       });
 
       nsCOMPtr<nsISelection> selection;
       nsCOMPtr<nsIContent> eventContent, targetContent;
       mCurrentTarget->GetContentForEvent(aEvent, getter_AddRefs(eventContent));
       if (eventContent)
         DetermineDragTargetAndDefaultData(window, eventContent, dataTransfer,
diff --git a/dom/events/test/test_bug508479.html b/dom/events/test/test_bug508479.html
--- a/dom/events/test/test_bug508479.html
+++ b/dom/events/test/test_bug508479.html
@@ -69,17 +69,17 @@ function runTests()
   is(gGotNotHandlingDrop, false, "Didn't get drop on unaccepting element (1)");
 
   // reset
   gGotHandlingDrop = false;
   gGotNotHandlingDrop = false;
 
   SpecialPowers.addChromeEventListener("drop", chromeListener, true, false);
   var targetNotHandling = document.getElementById("nothandling_target");
-  fireDrop(targetNotHandling, false, true);
+  fireDrop(targetNotHandling, true, true);
   SpecialPowers.removeChromeEventListener("drop", chromeListener, true);
   ok(chromeGotEvent, "Chrome should have got drop event!");
   is(gGotHandlingDrop, false, "Didn't get drop on accepting element (2)");
   is(gGotNotHandlingDrop, false, "Didn't get drop on unaccepting element (2)");
 
   SimpleTest.finish();
 }
 
diff --git a/dom/tests/mochitest/general/test_clipboard_events.html b/dom/tests/mochitest/general/test_clipboard_events.html
--- a/dom/tests/mochitest/general/test_clipboard_events.html
+++ b/dom/tests/mochitest/general/test_clipboard_events.html
@@ -741,14 +741,20 @@ function test_image_dataTransfer() {
   try {
     synthesizeKey("v", {accelKey: 1});
     ok(onpasteCalled, "The paste event listener must have been called");
   } finally {
     document.onpaste = null;
   }
 }
 
-SimpleTest.waitForFocus(doTests);
+SimpleTest.waitForFocus(() => {
+  SpecialPowers.pushPrefEnv({
+    // NOTE: These tests operate under the assumption that the protected mode of
+    // DataTransfer is enabled.
+    "set": [["dom.events.dataTransfer.protected.enabled", true]]
+  }, doTests);
+});
 
 </script>
 </pre>
 </body>
 </html>
diff --git a/layout/base/PresShell.cpp b/layout/base/PresShell.cpp
--- a/layout/base/PresShell.cpp
+++ b/layout/base/PresShell.cpp
@@ -8074,18 +8074,17 @@ PresShell::HandleEventInternal(WidgetEve
     case eDragLeave:
     case eDragOver:
     case eDrop: {
       // After any drag event other than dragstart (which is handled separately,
       // as we need to collect the data first), the DataTransfer needs to be
       // made protected, and then disconnected.
       DataTransfer* dataTransfer = aEvent->AsDragEvent()->mDataTransfer;
       if (dataTransfer) {
-        dataTransfer->SetMode(DataTransfer::Mode::Protected);
-        dataTransfer->ClearAll();
+        dataTransfer->Disconnect();
       }
       break;
     }
     default:
       break;
     }
   }
 
