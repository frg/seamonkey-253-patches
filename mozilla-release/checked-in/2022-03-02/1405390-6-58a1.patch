# HG changeset patch
# User David Anderson <danderson@mozilla.com>
# Date 1510217011 28800
# Node ID 9ae742527820a5c0735dd17b0a2bbade6cc593ef
# Parent  00fdab8f8daacea30fb10a649173a7be31d95e78
Fix DataSourceSurface mapping in GLReadTexImageHelper.cpp. (bug 1405390 part 6, r=jgilbert)

diff --git a/gfx/gl/GLReadTexImageHelper.cpp b/gfx/gl/GLReadTexImageHelper.cpp
--- a/gfx/gl/GLReadTexImageHelper.cpp
+++ b/gfx/gl/GLReadTexImageHelper.cpp
@@ -310,29 +310,33 @@ ReadPixelsIntoDataSurface(GLContext* gl,
     case SurfaceFormat::R5G6B5_UINT16:
         destFormat = LOCAL_GL_RGB;
         destType = LOCAL_GL_UNSIGNED_SHORT_5_6_5_REV;
         break;
     default:
         MOZ_CRASH("GFX: Bad format, read pixels.");
     }
     destPixelSize = BytesPerPixel(dest->GetFormat());
-    MOZ_ASSERT(dest->GetSize().width * destPixelSize <= dest->Stride());
+
+    Maybe<DataSourceSurface::ScopedMap> map;
+    map.emplace(dest, DataSourceSurface::READ_WRITE);
+
+    MOZ_ASSERT(dest->GetSize().width * destPixelSize <= map->GetStride());
 
     GLenum readFormat = destFormat;
     GLenum readType = destType;
     bool needsTempSurf = !GetActualReadFormats(gl,
                                                destFormat, destType,
                                                &readFormat, &readType);
 
     RefPtr<DataSourceSurface> tempSurf;
     DataSourceSurface* readSurf = dest;
     int readAlignment = GuessAlignment(dest->GetSize().width,
                                        destPixelSize,
-                                       dest->Stride());
+                                       map->GetStride());
     if (!readAlignment) {
         needsTempSurf = true;
     }
     if (needsTempSurf) {
         if (GLContext::ShouldSpew()) {
             NS_WARNING("Needing intermediary surface for ReadPixels. This will be slow!");
         }
         SurfaceFormat readFormatGFX;
@@ -384,35 +388,38 @@ ReadPixelsIntoDataSurface(GLContext* gl,
         tempSurf = Factory::CreateDataSourceSurfaceWithStride(dest->GetSize(),
                                                               readFormatGFX,
                                                               stride);
         if (NS_WARN_IF(!tempSurf)) {
             return;
         }
 
         readSurf = tempSurf;
+        map = Nothing();
+        map.emplace(readSurf, DataSourceSurface::READ_WRITE);
     }
 
-    DataSourceSurface::ScopedMap map(readSurf, DataSourceSurface::READ_WRITE);
     MOZ_ASSERT(readAlignment);
-    MOZ_ASSERT(reinterpret_cast<uintptr_t>(map.GetData()) % readAlignment == 0);
+    MOZ_ASSERT(reinterpret_cast<uintptr_t>(map->GetData()) % readAlignment == 0);
 
     GLsizei width = dest->GetSize().width;
     GLsizei height = dest->GetSize().height;
 
     {
         ScopedPackState safePackState(gl);
         gl->fPixelStorei(LOCAL_GL_PACK_ALIGNMENT, readAlignment);
 
         gl->fReadPixels(0, 0,
                         width, height,
                         readFormat, readType,
-                        map.GetData());
+                        map->GetData());
     }
 
+    map = Nothing();
+
     if (readSurf != dest) {
         MOZ_ASSERT(readFormat == LOCAL_GL_RGBA);
         MOZ_ASSERT(readType == LOCAL_GL_UNSIGNED_BYTE);
         gfx::Factory::CopyDataSourceSurface(readSurf, dest);
     }
 }
 
 already_AddRefed<gfx::DataSourceSurface>
