# HG changeset patch
# User Boris Chiou <boris.chiou@gmail.com>
# Date 1505810132 -28800
#      Tue Sep 19 16:35:32 2017 +0800
# Node ID f1327c641da9f5950c7c29f545ea9de8de8c01aa
# Parent  42a5a15bc2caf05fc92b3fa8a6761ab8cd2e8608
Bug 1390026 - Update test_transitions_per_property.html for shape-outside. r=birtles

MozReview-Commit-ID: KJKTJkM84UN

diff --git a/layout/style/test/test_transitions_per_property.html b/layout/style/test/test_transitions_per_property.html
--- a/layout/style/test/test_transitions_per_property.html
+++ b/layout/style/test/test_transitions_per_property.html
@@ -131,17 +131,17 @@ var supported_properties = {
                            test_length_clamped ],
     "bottom": [ test_length_transition, test_percent_transition,
                 test_length_percent_calc_transition,
                 test_length_unclamped, test_percent_unclamped ],
     "caret-color": [ test_color_transition,
                      test_true_currentcolor_transition,
                      test_auto_color_transition ],
     "clip": [ test_rect_transition ],
-    "clip-path": [ test_clip_path_transition ],
+    "clip-path": [ test_basic_shape_or_url_transition ],
     "color": [ test_color_transition,
                test_currentcolor_transition ],
     "fill": [ test_color_transition,
               test_currentcolor_transition ],
     "fill-opacity" : [ test_float_zeroToOne_transition,
                        // opacity is clamped in computed style
                        // (not parsing/interpolation)
                        test_float_zeroToOne_clamped ],
@@ -281,16 +281,23 @@ var supported_properties = {
     "word-spacing": [ test_length_transition, test_length_unclamped ],
     "z-index": [ test_integer_transition, test_pos_integer_or_auto_transition ],
     "-webkit-text-fill-color": [ test_color_transition,
                                  test_true_currentcolor_transition ],
     "-webkit-text-stroke-color": [ test_color_transition,
                                    test_true_currentcolor_transition ]
 };
 
+// Bug 1289049: After making shape-outside animatable on Gecko, we can remove
+// |isServo| flag from this if-condition.
+if (SpecialPowers.getBoolPref("layout.css.shape-outside.enabled") && isServo) {
+  supported_properties["shape-outside"] =
+    [ test_basic_shape_or_url_transition ];
+}
+
 if (SupportsMaskShorthand()) {
   supported_properties["mask-position"] = [ test_background_position_transition,
                                      // FIXME: We don't currently test clamping,
                                      // since mask-position uses calc() as
                                      // an intermediate form.
                                      /* test_length_percent_pair_unclamped */ ];
   supported_properties["mask-position-x"] = [ test_background_position_coord_transition,
                                               test_length_transition,
@@ -625,17 +632,23 @@ var transformTests = [
     end: 'skewX(-45deg) rotate(90deg)',
     expected_uncomputed: 'skewX(22.5deg) rotate(90deg)' },
   { start: 'skewX(-60deg) rotate(90deg) translate(0)',
     end: 'skewX(60deg) rotate(90deg)',
     expected: computeMatrix('rotate(120deg) skewX(' + Math.atan(Math.tan(Math.PI * 60/180) / 2) + 'rad) scale(2, 0.5)'),
     round_error_ok: true },
 ];
 
-var clipPathTests = [
+// Even if the default reference-box of shape-outside is margin-box, which is
+// different from the default reference-box of clip-path, we still can reuse
+// these tests for both properties because we always explicitly assign a
+// reference-box (i.e. border-box or content-box) if needed.
+// Bug 1313619: Add some tests for two basic shapes with an explicit
+// reference-box and a default one.
+var clipPathAndShapeOutsideTests = [
   { start: "none", end: "none",
     expected: ["none"] },
   // none to shape
   { start: "none",
     end: "circle(500px at 500px 500px) border-box",
     expected:
       isServo ? ["circle", ["500px at 500px 500px"], "border-box"]
               : ["circle", ["500px at calc(500px + 0%) calc(500px + 0%)"], "border-box"]
@@ -1428,22 +1441,26 @@ function test_color_shorthand_transition
 function test_currentcolor_shorthand_transition(prop) {
   test_currentcolor_transition(prop, get_color_from_shorthand_value, true);
 }
 
 function test_true_currentcolor_shorthand_transition(prop) {
   test_true_currentcolor_transition(prop, get_color_from_shorthand_value, true);
 }
 
-function test_clip_path_equals(computedValStr, expectedList)
+function test_shape_or_url_equals(computedValStr, expected)
 {
   // Check simple case "none"
-  if (computedValStr == "none" && computedValStr == expectedList[0]) {
+  if (computedValStr == "none" && computedValStr == expected[0]) {
     return true;
   }
+  // We will update the expected list in this function for checking the result,
+  // so we clone it first to avoid affecting the input parameter.
+  var expectedList = expected.slice();
+
   var start = String(computedValStr);
 
   var regBox = /\s*(content\-box|padding\-box|border\-box|margin\-box|view\-box|stroke\-box|fill\-box)/
   var matches = computedValStr.split(regBox);
   var expectRefBox = typeof expectedList[expectedList.length - 1] == "string" &&
                      expectedList[expectedList.length - 1].match(regBox) !== null;
 
   // Found a reference box? Format: "shape()" or "shape() reference-box"
@@ -1469,17 +1486,17 @@ function test_clip_path_equals(computedV
       return false;
     }
   }
   computedValStr = matches[0];
   if (expectedList.length == 0) {
     if (computedValStr == "") {
       return true;
     } else {
-      ok(false, "expected clip-path shape");
+      ok(false, "expected basic shape");
       return false;
     }
   }
 
   // The regular expression does not filter out the last parenthesis.
   // Remove last character for now.
   is(computedValStr.substring(computedValStr.length - 1, computedValStr.length),
                               ')', "Function should have close-paren");
@@ -1557,27 +1574,27 @@ function filter_function_list_equals(com
     if (isNaN(functionValue) ||
       Math.abs(parseFloat(functionValue) - expected) > tolerance) {
       return false;
     }
   }
   return true;
 }
 
-function test_clip_path_transition(prop) {
-  for (var i in clipPathTests) {
-    var test = clipPathTests[i];
+function test_basic_shape_or_url_transition(prop) {
+  for (var i in clipPathAndShapeOutsideTests) {
+    var test = clipPathAndShapeOutsideTests[i];
     div.style.setProperty("transition-property", "none", "");
     div.style.setProperty(prop, test.start, "");
     cs.getPropertyValue(prop);
     div.style.setProperty("transition-property", prop, "");
     div.style.setProperty(prop, test.end, "");
     var actual = cs.getPropertyValue(prop);
-    ok(test_clip_path_equals(actual, test.expected),
-       "Clip-path property is " + actual + " expected values of " +
+    ok(test_shape_or_url_equals(actual, test.expected),
+       prop + " property is " + actual + " expected values of " +
        test.expected);
   }
 }
 
 function test_filter_transition(prop) {
   if (!SpecialPowers.getBoolPref("layout.css.filters.enabled")) {
     return;
   }
