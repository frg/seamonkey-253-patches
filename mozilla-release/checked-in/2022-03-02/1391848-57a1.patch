# HG changeset patch
# User Bill McCloskey <billm@mozilla.com>
# Date 1502406032 25200
#      Thu Aug 10 16:00:32 2017 -0700
# Node ID 4673ed66865e8955eb2d9c0185508b34f03c0af0
# Parent  09d7901c9bb0fd59612f28b13aa1e1140b27b972
Bug 1391848 - Use nsIEventTarget instead of nsIThread for MessageLoop/Pump (r=kanru)

MozReview-Commit-ID: 8op94arX6FE

diff --git a/ipc/chromium/src/base/message_loop.cc b/ipc/chromium/src/base/message_loop.cc
--- a/ipc/chromium/src/base/message_loop.cc
+++ b/ipc/chromium/src/base/message_loop.cc
@@ -170,17 +170,17 @@ MessageLoop* MessageLoop::current() {
 
 // static
 void MessageLoop::set_current(MessageLoop* loop) {
   get_tls_ptr().Set(loop);
 }
 
 static mozilla::Atomic<int32_t> message_loop_id_seq(0);
 
-MessageLoop::MessageLoop(Type type, nsIThread* aThread)
+MessageLoop::MessageLoop(Type type, nsIEventTarget* aEventTarget)
     : type_(type),
       id_(++message_loop_id_seq),
       nestable_tasks_allowed_(true),
       exception_restoration_(false),
       state_(NULL),
       run_depth_base_(1),
       shutting_down_(false),
 #ifdef OS_WIN
@@ -192,41 +192,41 @@ MessageLoop::MessageLoop(Type type, nsIT
   DCHECK(!current()) << "should only have one message loop per thread";
   get_tls_ptr().Set(this);
 
   // Must initialize after current() is initialized.
   mEventTarget = new EventTarget(this);
 
   switch (type_) {
   case TYPE_MOZILLA_PARENT:
-    MOZ_RELEASE_ASSERT(!aThread);
-    pump_ = new mozilla::ipc::MessagePump(aThread);
+    MOZ_RELEASE_ASSERT(!aEventTarget);
+    pump_ = new mozilla::ipc::MessagePump(aEventTarget);
     return;
   case TYPE_MOZILLA_CHILD:
-    MOZ_RELEASE_ASSERT(!aThread);
+    MOZ_RELEASE_ASSERT(!aEventTarget);
     pump_ = new mozilla::ipc::MessagePumpForChildProcess();
     // There is a MessageLoop Run call from XRE_InitChildProcess
     // and another one from MessagePumpForChildProcess. The one
     // from MessagePumpForChildProcess becomes the base, so we need
     // to set run_depth_base_ to 2 or we'll never be able to process
     // Idle tasks.
     run_depth_base_ = 2;
     return;
   case TYPE_MOZILLA_NONMAINTHREAD:
-    pump_ = new mozilla::ipc::MessagePumpForNonMainThreads(aThread);
+    pump_ = new mozilla::ipc::MessagePumpForNonMainThreads(aEventTarget);
     return;
 #if defined(OS_WIN)
   case TYPE_MOZILLA_NONMAINUITHREAD:
-    pump_ = new mozilla::ipc::MessagePumpForNonMainUIThreads(aThread);
+    pump_ = new mozilla::ipc::MessagePumpForNonMainUIThreads(aEventTarget);
     return;
 #endif
 #if defined(MOZ_WIDGET_ANDROID)
   case TYPE_MOZILLA_ANDROID_UI:
-    MOZ_RELEASE_ASSERT(aThread);
-    pump_ = new mozilla::ipc::MessagePumpForAndroidUI(aThread);
+    MOZ_RELEASE_ASSERT(aEventTarget);
+    pump_ = new mozilla::ipc::MessagePumpForAndroidUI(aEventTarget);
     return;
 #endif // defined(MOZ_WIDGET_ANDROID)
   default:
     // Create one of Chromium's standard MessageLoop types below.
     break;
   }
 
 #if defined(OS_WIN)
diff --git a/ipc/chromium/src/base/message_loop.h b/ipc/chromium/src/base/message_loop.h
--- a/ipc/chromium/src/base/message_loop.h
+++ b/ipc/chromium/src/base/message_loop.h
@@ -25,18 +25,18 @@
 #include "base/message_pump_libevent.h"
 #endif
 
 #include "nsAutoPtr.h"
 #include "nsCOMPtr.h"
 #include "nsIRunnable.h"
 #include "nsThreadUtils.h"
 
+class nsIEventTarget;
 class nsISerialEventTarget;
-class nsIThread;
 
 namespace mozilla {
 namespace ipc {
 
 class DoWorkRunnable;
 
 } /* namespace ipc */
 } /* namespace mozilla */
@@ -195,17 +195,17 @@ public:
     TYPE_MOZILLA_PARENT,
     TYPE_MOZILLA_NONMAINTHREAD,
     TYPE_MOZILLA_NONMAINUITHREAD,
     TYPE_MOZILLA_ANDROID_UI
   };
 
   // Normally, it is not necessary to instantiate a MessageLoop.  Instead, it
   // is typical to make use of the current thread's MessageLoop instance.
-  explicit MessageLoop(Type type = TYPE_DEFAULT, nsIThread* aThread = nullptr);
+  explicit MessageLoop(Type type = TYPE_DEFAULT, nsIEventTarget* aEventTarget = nullptr);
   ~MessageLoop();
 
   // Returns the type passed to the constructor.
   Type type() const { return type_; }
 
   // Unique, non-repeating ID for this message loop.
   int32_t id() const { return id_; }
 
diff --git a/ipc/glue/MessagePump.cpp b/ipc/glue/MessagePump.cpp
--- a/ipc/glue/MessagePump.cpp
+++ b/ipc/glue/MessagePump.cpp
@@ -60,33 +60,33 @@ private:
   MessagePump* mPump;
   // DoWorkRunnable is designed as a stateless singleton.  Do not add stateful
   // members here!
 };
 
 } /* namespace ipc */
 } /* namespace mozilla */
 
-MessagePump::MessagePump(nsIThread* aThread)
-: mThread(aThread)
+MessagePump::MessagePump(nsIEventTarget* aEventTarget)
+  : mEventTarget(aEventTarget)
 {
   mDoWorkEvent = new DoWorkRunnable(this);
 }
 
 MessagePump::~MessagePump()
 {
 }
 
 void
 MessagePump::Run(MessagePump::Delegate* aDelegate)
 {
   MOZ_ASSERT(keep_running_);
   MOZ_RELEASE_ASSERT(NS_IsMainThread(),
                      "Use mozilla::ipc::MessagePumpForNonMainThreads instead!");
-  MOZ_RELEASE_ASSERT(!mThread);
+  MOZ_RELEASE_ASSERT(!mEventTarget);
 
   nsIThread* thisThread = NS_GetCurrentThread();
   MOZ_ASSERT(thisThread);
 
   mDelayedWorkTimer = do_CreateInstance(kNS_TIMER_CID);
   MOZ_ASSERT(mDelayedWorkTimer);
 
   base::ScopedNSAutoreleasePool autoReleasePool;
@@ -129,18 +129,18 @@ if (did_work && delayed_work_time_.is_nu
 
   keep_running_ = true;
 }
 
 void
 MessagePump::ScheduleWork()
 {
   // Make sure the event loop wakes up.
-  if (mThread) {
-    mThread->Dispatch(mDoWorkEvent, NS_DISPATCH_NORMAL);
+  if (mEventTarget) {
+    mEventTarget->Dispatch(mDoWorkEvent, NS_DISPATCH_NORMAL);
   } else {
     // Some things (like xpcshell) don't use the app shell and so Run hasn't
     // been called. We still need to wake up the main thread.
     NS_DispatchToMainThread(mDoWorkEvent);
   }
   event_.Signal();
 }
 
@@ -153,18 +153,18 @@ MessagePump::ScheduleWorkForNestedLoop()
   // cost of what will be a no-op nsThread::Dispatch(mDoWorkEvent).
 }
 
 void
 MessagePump::ScheduleDelayedWork(const base::TimeTicks& aDelayedTime)
 {
   // To avoid racing on mDelayedWorkTimer, we need to be on the same thread as
   // ::Run().
-  MOZ_RELEASE_ASSERT(NS_GetCurrentThread() == mThread ||
-                     (!mThread && NS_IsMainThread()));
+  MOZ_RELEASE_ASSERT((!mEventTarget && NS_IsMainThread())
+                     || mEventTarget->IsOnCurrentThread());
 
   if (!mDelayedWorkTimer) {
     mDelayedWorkTimer = do_CreateInstance(kNS_TIMER_CID);
     if (!mDelayedWorkTimer) {
         // Called before XPCOM has started up? We can't do this correctly.
         NS_WARNING("Delayed task might not run!");
         delayed_work_time_ = aDelayedTime;
         return;
@@ -185,23 +185,22 @@ MessagePump::ScheduleDelayedWork(const b
   uint32_t delayMS = uint32_t(delay.InMilliseconds());
   mDelayedWorkTimer->InitWithCallback(mDoWorkEvent, delayMS,
                                       nsITimer::TYPE_ONE_SHOT);
 }
 
 nsIEventTarget*
 MessagePump::GetXPCOMThread()
 {
-  if (mThread) {
-    return mThread;
+  if (mEventTarget) {
+    return mEventTarget;
   }
 
   // Main thread
-  nsCOMPtr<nsIThread> mainThread = do_GetMainThread();
-  return mainThread;
+  return GetMainThreadEventTarget();
 }
 
 void
 MessagePump::DoDelayedWork(base::MessagePump::Delegate* aDelegate)
 {
   aDelegate->DoDelayedWork(&delayed_work_time_);
   if (!delayed_work_time_.is_null()) {
     ScheduleDelayedWork(delayed_work_time_);
@@ -304,35 +303,35 @@ MessagePumpForChildProcess::Run(base::Me
 
 void
 MessagePumpForNonMainThreads::Run(base::MessagePump::Delegate* aDelegate)
 {
   MOZ_ASSERT(keep_running_);
   MOZ_RELEASE_ASSERT(!NS_IsMainThread(), "Use mozilla::ipc::MessagePump instead!");
 
   nsIThread* thread = NS_GetCurrentThread();
-  MOZ_RELEASE_ASSERT(mThread == thread);
+  MOZ_RELEASE_ASSERT(mEventTarget->IsOnCurrentThread());
 
   mDelayedWorkTimer = do_CreateInstance(kNS_TIMER_CID);
   MOZ_ASSERT(mDelayedWorkTimer);
 
-  if (NS_FAILED(mDelayedWorkTimer->SetTarget(thread))) {
+  if (NS_FAILED(mDelayedWorkTimer->SetTarget(mEventTarget))) {
     MOZ_CRASH("Failed to set timer target!");
   }
 
   // Chromium event notifications to be processed will be received by this
   // event loop as a DoWorkRunnables via ScheduleWork. Chromium events that
   // were received before our thread is valid, however, will not generate
   // runnable wrappers. We must process any of these before we enter this
   // loop, or we will forever have unprocessed chromium messages in our queue.
   //
   // Note we would like to request a flush of the chromium event queue
   // using a runnable on the xpcom side, but some thread implementations
   // (dom workers) get cranky if we call ScheduleWork here (ScheduleWork
-  // calls dispatch on mThread) before the thread processes an event. As
+  // calls dispatch on mEventTarget) before the thread processes an event. As
   // such, clear the queue manually.
   while (aDelegate->DoWork()) {
   }
 
   base::ScopedNSAutoreleasePool autoReleasePool;
   for (;;) {
     autoReleasePool.Recycle();
 
diff --git a/ipc/glue/MessagePump.h b/ipc/glue/MessagePump.h
--- a/ipc/glue/MessagePump.h
+++ b/ipc/glue/MessagePump.h
@@ -13,30 +13,30 @@
 #endif
 
 #include "base/time.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/Mutex.h"
 #include "nsCOMPtr.h"
 #include "nsIThreadInternal.h"
 
-class nsIThread;
+class nsIEventTarget;
 class nsITimer;
 
 namespace mozilla {
 namespace ipc {
 
 class DoWorkRunnable;
 
 class MessagePump : public base::MessagePumpDefault
 {
   friend class DoWorkRunnable;
 
 public:
-  explicit MessagePump(nsIThread* aThread);
+  explicit MessagePump(nsIEventTarget* aEventTarget);
 
   // From base::MessagePump.
   virtual void
   Run(base::MessagePump::Delegate* aDelegate) override;
 
   // From base::MessagePump.
   virtual void
   ScheduleWork() override;
@@ -55,19 +55,19 @@ public:
 protected:
   virtual ~MessagePump();
 
 private:
   // Only called by DoWorkRunnable.
   void DoDelayedWork(base::MessagePump::Delegate* aDelegate);
 
 protected:
-  nsIThread* mThread;
+  nsIEventTarget* mEventTarget;
 
-  // mDelayedWorkTimer and mThread are set in Run() by this class or its
+  // mDelayedWorkTimer and mEventTarget are set in Run() by this class or its
   // subclasses.
   nsCOMPtr<nsITimer> mDelayedWorkTimer;
 
 private:
   // Only accessed by this class.
   RefPtr<DoWorkRunnable> mDoWorkEvent;
 };
 
@@ -86,18 +86,18 @@ private:
   { }
 
   bool mFirstRun;
 };
 
 class MessagePumpForNonMainThreads final : public MessagePump
 {
 public:
-  explicit MessagePumpForNonMainThreads(nsIThread* aThread)
-    : MessagePump(aThread)
+  explicit MessagePumpForNonMainThreads(nsIEventTarget* aEventTarget)
+    : MessagePump(aEventTarget)
   { }
 
   virtual void Run(base::MessagePump::Delegate* aDelegate) override;
 
 private:
   ~MessagePumpForNonMainThreads()
   { }
 };
@@ -118,17 +118,17 @@ public:
   NS_IMETHOD_(MozExternalRefCountType) Release(void) override  {
     return 1;
   }
   NS_IMETHOD QueryInterface(REFNSIID aIID, void** aInstancePtr) override;
 
   NS_DECL_NSITHREADOBSERVER
 
 public:
-  explicit MessagePumpForNonMainUIThreads(nsIThread* aThread) :
+  explicit MessagePumpForNonMainUIThreads(nsIEventTarget* aEventTarget) :
     mInWait(false),
     mWaitLock("mInWait")
   {
   }
 
   // The main run loop for this thread.
   virtual void DoRunLoop() override;
 
