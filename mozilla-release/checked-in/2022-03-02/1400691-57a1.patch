# HG changeset patch
# User Geoff Brown <gbrown@mozilla.com>
# Date 1505747496 21600
#      Mon Sep 18 09:11:36 2017 -0600
# Node ID d296dbfbf6cc8f8f1ddee4dbf571d7d80255a91b
# Parent  636cf41c10d03c3276a284f3c477e59f1debba71
Bug 1400691 - Minor changes to logging for test-verify; r=jmaher

diff --git a/testing/mozharness/mozharness/mozilla/testing/verify_tools.py b/testing/mozharness/mozharness/mozilla/testing/verify_tools.py
--- a/testing/mozharness/mozharness/mozilla/testing/verify_tools.py
+++ b/testing/mozharness/mozharness/mozilla/testing/verify_tools.py
@@ -63,31 +63,33 @@ class VerifyToolsMixin(object):
         ]
 
         tests_by_path = {}
         for (path, suite) in manifests:
             if os.path.exists(path):
                 man = TestManifest([path], strict=False)
                 active = man.active_tests(exists=False, disabled=False, filters=[], **mozinfo.info)
                 tests_by_path.update({t['relpath']:(suite,t.get('subsuite')) for t in active})
+                self.info("Verification updated with manifest %s" % path)
 
         # determine which files were changed on this push
         url = '%s/json-automationrelevance/%s' % (repository.rstrip('/'), revision)
         contents = self.retry(get_automationrelevance, attempts=2, sleeptime=10)
         changed_files = set()
         for c in contents['changesets']:
             self.info(" {cset} {desc}".format(
                 cset=c['node'][0:12],
                 desc=c['desc'].splitlines()[0].encode('ascii', 'ignore')))
             changed_files |= set(c['files'])
 
         # for each changed file, determine if it is a test file, and what suite it is in
         for file in changed_files:
             entry = tests_by_path.get(file)
             if entry:
+                self.info("Verification found test %s" % file)
                 subsuite_mapping = {
                     ('browser-chrome', 'clipboard') : 'browser-chrome-clipboard',
                     ('chrome', 'clipboard') : 'chrome-clipboard',
                     ('plain', 'clipboard') : 'plain-clipboard',
                     ('browser-chrome', 'devtools') : 'mochitest-devtools-chrome',
                     ('browser-chrome', 'gpu') : 'browser-chrome-gpu',
                     ('chrome', 'gpu') : 'chrome-gpu',
                     ('plain', 'gpu') : 'plain-gpu',
@@ -123,16 +125,17 @@ class VerifyToolsMixin(object):
             args = [[]]
         else:
             # in verify mode, run nothing by default (unsupported suite or no files modified)
             args = []
             # otherwise, run once for each file in requested suite
             files = self.verify_suites.get(suite)
             for file in files:
                 args.append(['--verify-max-time=%d' % MAX_TIME_PER_TEST, '--verify', file])
+            self.info("Verification file for '%s': %s" % (suite, files))
         return args
 
     def query_verify_category_suites(self, category, all_suites):
         """
            In verify mode, determine which suites are active, for the given
            suite category.
         """
         suites = None
@@ -143,8 +146,31 @@ class VerifyToolsMixin(object):
             else:
                 # Until test zips are downloaded, manifests are not available,
                 # so it is not possible to determine which suites are active/
                 # required for verification; assume all suites from supported
                 # suite categories are required.
                 if category in ['mochitest', 'xpcshell']:
                     suites = all_suites
         return suites
+
+    def log_verify_status(self, test_name, tbpl_status, log_level):
+        """
+           Log verification status of a single test. This will display in the
+           Job Details pane in treeherder - a convenient summary of verification.
+           Special test name formatting is needed because treeherder truncates
+           lines that are too long, and may remove duplicates after truncation.
+        """
+        max_test_name_len = 40
+        if len(test_name) > max_test_name_len:
+            head = test_name
+            new = ""
+            previous = None
+            max_test_name_len = max_test_name_len - len('.../')
+            while len(new) < max_test_name_len:
+                head, tail = os.path.split(head)
+                previous = new
+                new = os.path.join(tail, new)
+            test_name = os.path.join('...', previous or new)
+            test_name = test_name.rstrip(os.path.sep)
+        self.log("TinderboxPrint: Verification of %s<br/>: %s" %
+                 (test_name, tbpl_status), level=log_level)
+
diff --git a/testing/mozharness/scripts/desktop_unittest.py b/testing/mozharness/scripts/desktop_unittest.py
--- a/testing/mozharness/scripts/desktop_unittest.py
+++ b/testing/mozharness/scripts/desktop_unittest.py
@@ -787,18 +787,17 @@ class DesktopUnittest(TestingMixin, Merc
                         success_codes = [0, 1]
 
                     tbpl_status, log_level = parser.evaluate_parser(return_code,
                                                                     success_codes=success_codes)
                     parser.append_tinderboxprint_line(suite_name)
 
                     self.buildbot_status(tbpl_status, level=log_level)
                     if len(verify_args) > 0:
-                        self.log("TinderboxPrint: verification of %s in %s<br/>: %s" %
-                                 (verify_args[-1], suite, tbpl_status), level=log_level)
+                        self.log_verify_status(verify_args[-1], tbpl_status, log_level)
                     else:
                         self.log("The %s suite: %s ran with return status: %s" %
                                  (suite_category, suite, tbpl_status), level=log_level)
 
                 if verify_time_exceeded:
                     # Verification ran out of time, detected in inner loop.
                     break
         else:
