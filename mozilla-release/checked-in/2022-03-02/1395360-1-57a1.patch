# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1504370725 14400
# Node ID d89ac77cac51787685fe4c2a8a87776765593082
# Parent  1dca70dfe391082dd0b5dc5fc97a38a1b5795ac9
Bug 1395360 - Factor out ExecuteInNonSyntacticGlobalInternal r=jorendorff

MozReview-Commit-ID: AMISJPCKqw9

diff --git a/js/src/builtin/Eval.cpp b/js/src/builtin/Eval.cpp
--- a/js/src/builtin/Eval.cpp
+++ b/js/src/builtin/Eval.cpp
@@ -439,49 +439,55 @@ js::DirectEval(JSContext* cx, HandleValu
 }
 
 bool
 js::IsAnyBuiltinEval(JSFunction* fun)
 {
     return fun->maybeNative() == IndirectEval;
 }
 
-JS_FRIEND_API(bool)
-js::ExecuteInGlobalAndReturnScope(JSContext* cx, HandleObject global, HandleScript scriptArg,
-                                  MutableHandleObject envArg)
+static bool
+ExecuteInNonSyntacticGlobalInternal(JSContext* cx, HandleObject global, HandleScript scriptArg,
+                                    HandleObject varEnv, HandleObject lexEnv)
 {
     CHECK_REQUEST(cx);
-    assertSameCompartment(cx, global);
+    assertSameCompartment(cx, global, varEnv, lexEnv);
     MOZ_ASSERT(global->is<GlobalObject>());
+    MOZ_ASSERT(varEnv->is<NonSyntacticVariablesObject>());
+    MOZ_ASSERT(IsExtensibleLexicalEnvironment(lexEnv));
+    MOZ_ASSERT(lexEnv->enclosingEnvironment() == varEnv);
     MOZ_RELEASE_ASSERT(scriptArg->hasNonSyntacticScope());
 
     RootedScript script(cx, scriptArg);
     Rooted<GlobalObject*> globalRoot(cx, &global->as<GlobalObject>());
     if (script->compartment() != cx->compartment()) {
         script = CloneGlobalScript(cx, ScopeKind::NonSyntactic, script);
         if (!script)
             return false;
 
         Debugger::onNewScript(cx, script);
     }
 
-    Rooted<EnvironmentObject*> env(cx, NonSyntacticVariablesObject::create(cx));
-    if (!env)
+    RootedValue rval(cx);
+    return ExecuteKernel(cx, script, *lexEnv, UndefinedValue(),
+                         NullFramePtr() /* evalInFrame */, rval.address());
+}
+
+JS_FRIEND_API(bool)
+js::ExecuteInGlobalAndReturnScope(JSContext* cx, HandleObject global, HandleScript scriptArg,
+                                  MutableHandleObject envArg)
+{
+    RootedObject varEnv(cx, NonSyntacticVariablesObject::create(cx));
+    if (!varEnv)
         return false;
 
-    // Unlike the non-syntactic scope chain API used by the subscript loader,
-    // this API creates a fresh block scope each time.
-    //
-    // NOTE: Gecko FrameScripts expect lexical |this| to be the global.
-    env = LexicalEnvironmentObject::createNonSyntactic(cx, env, global);
-    if (!env)
+    // Create lexical environment with |this| == global.
+    // NOTE: This is required behavior for Gecko FrameScriptLoader
+    RootedObject lexEnv(cx, LexicalEnvironmentObject::createNonSyntactic(cx, varEnv, global));
+    if (!lexEnv)
         return false;
 
-    RootedValue rval(cx);
-    if (!ExecuteKernel(cx, script, *env, UndefinedValue(),
-                       NullFramePtr() /* evalInFrame */, rval.address()))
-    {
+    if (!ExecuteInNonSyntacticGlobalInternal(cx, global, scriptArg, varEnv, lexEnv))
         return false;
-    }
 
-    envArg.set(env);
+    envArg.set(lexEnv);
     return true;
 }

