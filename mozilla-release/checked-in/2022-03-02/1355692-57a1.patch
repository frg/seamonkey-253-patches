# HG changeset patch
# User Cykesiopka <cykesiopka.bmo@gmail.com>
# Date 1501171888 -28800
# Node ID 3403a367f1a77edf74a45b0c60ddce203efa81e4
# Parent  83c57c844812f9b6dbd32f56bde7da49d70a3bcb
Bug 1355692 - Add MOZ_FALLTHROUGH macro definition for gcc 7 to suppress -Wimplicit-fallthrough warnings. r=glandium

The generic fallback MOZ_FALLTHROUGH definition is insufficient for GCC 7 and
above, resulting in --enable-warnings-as-errors builds failing.

The check for clang support is changed to use the __has_cpp_attribute macro,
which is more robust than checking the __cplusplus version.

Also, MOZ_FALLTHROUGH is now only defined in C++ code, since GCC errors out if
it encounters a scoped attribute being used with __has_cpp_attribute in C code.
No C code uses MOZ_FALLTHROUGH or derivatives at the moment.

MozReview-Commit-ID: 4nKFBRD5jSF

diff --git a/mfbt/Attributes.h b/mfbt/Attributes.h
--- a/mfbt/Attributes.h
+++ b/mfbt/Attributes.h
@@ -329,16 +329,18 @@
 #if defined(__GNUC__) || defined(__clang__)
 #  define MOZ_MAYBE_UNUSED __attribute__ ((__unused__))
 #elif defined(_MSC_VER)
 #  define MOZ_MAYBE_UNUSED __pragma(warning(suppress:4505))
 #else
 #  define MOZ_MAYBE_UNUSED
 #endif
 
+#ifdef __cplusplus
+
 /**
  * MOZ_FALLTHROUGH is an annotation to suppress compiler warnings about switch
  * cases that fall through without a break or return statement. MOZ_FALLTHROUGH
  * is only needed on cases that have code.
  *
  * MOZ_FALLTHROUGH_ASSERT is an annotation to suppress compiler warnings about
  * switch cases that MOZ_ASSERT(false) (or its alias MOZ_ASSERT_UNREACHABLE) in
  * debug builds, but intentionally fall through in release builds. See comment
@@ -355,32 +357,35 @@
  *
  *   default:
  *     // This case asserts in debug builds, falls through in release.
  *     MOZ_FALLTHROUGH_ASSERT("Unexpected foo value?!");
  *   case 5:
  *     return 5;
  * }
  */
-#if defined(__clang__) && __cplusplus >= 201103L
-   /* clang's fallthrough annotations are only available starting in C++11. */
+#ifndef __has_cpp_attribute
+#  define __has_cpp_attribute(x) 0
+#endif
+
+#if __has_cpp_attribute(clang::fallthrough)
 #  define MOZ_FALLTHROUGH [[clang::fallthrough]]
+#elif __has_cpp_attribute(gnu::fallthrough)
+#  define MOZ_FALLTHROUGH [[gnu::fallthrough]]
 #elif defined(_MSC_VER)
    /*
     * MSVC's __fallthrough annotations are checked by /analyze (Code Analysis):
     * https://msdn.microsoft.com/en-us/library/ms235402%28VS.80%29.aspx
     */
 #  include <sal.h>
 #  define MOZ_FALLTHROUGH __fallthrough
 #else
 #  define MOZ_FALLTHROUGH /* FALLTHROUGH */
 #endif
 
-#ifdef __cplusplus
-
 /*
  * The following macros are attributes that support the static analysis plugin
  * included with Mozilla, and will be implemented (when such support is enabled)
  * as C++11 attributes. Since such attributes are legal pretty much everywhere
  * and have subtly different semantics depending on their placement, the
  * following is a guide on where to place the attributes.
  *
  * Attributes that apply to a struct or class precede the name of the class:
