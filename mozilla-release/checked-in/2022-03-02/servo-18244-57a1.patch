# HG changeset patch
# User Kevin Hsieh <kevin.hsieh@ucla.edu>
# Date 1503700222 18000
#      Fri Aug 25 17:30:22 2017 -0500
# Node ID 51343eff65c3b775f8364602137f3484b17b934c
# Parent  ecf44020610a192b1d21f9d85345f45926c7830e
servo: Merge #18244 - stylo: Update CSS fallback font when lang changes (from kahsieh:master); r=Manishearth

r=manishearth in BugZilla.

---
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [X] These changes fix [Bug 1367860](https://bugzilla.mozilla.org/show_bug.cgi?id=1367860).

Source-Repo: https://github.com/servo/servo
Source-Revision: 2136dd2bb7fc9d78f796de4a49e01339bbbd2d31

diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -2164,19 +2164,20 @@ fn static_assert() {
 
     pub fn fixup_none_generic(&mut self, device: &Device) {
         self.gecko.mFont.systemFont = false;
         unsafe {
             bindings::Gecko_nsStyleFont_FixupNoneGeneric(&mut self.gecko, device.pres_context())
         }
     }
 
-    pub fn fixup_system(&mut self) {
+    pub fn fixup_system(&mut self, default_font_type: structs::FontFamilyType) {
         self.gecko.mFont.systemFont = true;
         self.gecko.mGenericID = structs::kGenericFont_NONE;
+        self.gecko.mFont.fontlist.mDefaultFontType = default_font_type;
     }
 
     pub fn set_font_family(&mut self, v: longhands::font_family::computed_value::T) {
         use properties::longhands::font_family::computed_value::{FontFamily, FamilyNameSyntax};
 
         let list = &mut self.gecko.mFont.fontlist;
         unsafe { Gecko_FontFamilyList_Clear(list); }
 
@@ -2218,17 +2219,26 @@ fn static_assert() {
     }
 
     pub fn clone_font_family(&self) -> longhands::font_family::computed_value::T {
         use cssparser::serialize_identifier;
         use properties::longhands::font_family::computed_value::{FontFamily, FamilyName, FamilyNameSyntax};
         use gecko_bindings::structs::FontFamilyType;
         use gecko_string_cache::Atom;
 
-        ::properties::longhands::font_family::computed_value::T(
+        if self.gecko.mFont.fontlist.mFontlist.is_empty() {
+            let default = match self.gecko.mFont.fontlist.mDefaultFontType {
+                FontFamilyType::eFamily_serif => FontFamily::Generic(atom!("serif")),
+                FontFamilyType::eFamily_sans_serif => FontFamily::Generic(atom!("sans-serif")),
+                _ => panic!("Default generic must be serif or sans-serif"),
+            };
+            return longhands::font_family::computed_value::T(vec![default]);
+        }
+
+        longhands::font_family::computed_value::T(
             self.gecko.mFont.fontlist.mFontlist.iter().map(|gecko_font_family_name| {
                 match gecko_font_family_name.mType {
                     FontFamilyType::eFamily_serif => FontFamily::Generic(atom!("serif")),
                     FontFamilyType::eFamily_sans_serif => FontFamily::Generic(atom!("sans-serif")),
                     FontFamilyType::eFamily_monospace => FontFamily::Generic(atom!("monospace")),
                     FontFamilyType::eFamily_cursive => FontFamily::Generic(atom!("cursive")),
                     FontFamilyType::eFamily_fantasy => FontFamily::Generic(atom!("fantasy")),
                     FontFamilyType::eFamily_moz_fixed => FontFamily::Generic(Atom::from("-moz-fixed")),
diff --git a/servo/components/style/properties/longhand/font.mako.rs b/servo/components/style/properties/longhand/font.mako.rs
--- a/servo/components/style/properties/longhand/font.mako.rs
+++ b/servo/components/style/properties/longhand/font.mako.rs
@@ -2486,21 +2486,23 @@ https://drafts.csswg.org/css-fonts-4/#lo
         //! each font longhand has a special SystemFont variant which contains
         //! the specified system font. When the cascade function (in helpers)
         //! detects that a value has a system font, it will resolve it, and
         //! cache it on the ComputedValues. After this, it can be just fetched
         //! whenever a font longhand on the same element needs the system font.
 
         use app_units::Au;
         use cssparser::{Parser, ToCss};
+        use gecko_bindings::structs::FontFamilyType;
         use properties::longhands;
         use std::fmt;
         use std::hash::{Hash, Hasher};
         use style_traits::ParseError;
         use values::computed::{ToComputedValue, Context};
+
         <%
             system_fonts = """caption icon menu message-box small-caption status-bar
                               -moz-window -moz-document -moz-workspace -moz-desktop
                               -moz-info -moz-dialog -moz-button -moz-pull-down-menu
                               -moz-list -moz-field""".split()
             kw_font_props = """font_style font_variant_caps font_stretch
                                font_kerning font_variant_position font_variant_ligatures
                                font_variant_east_asian font_variant_numeric""".split()
@@ -2579,16 +2581,17 @@ https://drafts.csswg.org/css-fonts-4/#lo
                             % endif
                         ),
                     % endfor
                     font_language_override: longhands::font_language_override::computed_value
                                                      ::T(system.languageOverride),
                     font_feature_settings: longhands::font_feature_settings::get_initial_value(),
                     font_variant_alternates: longhands::font_variant_alternates::get_initial_value(),
                     system_font: *self,
+                    default_font_type: system.fontlist.mDefaultFontType,
                 };
                 unsafe { bindings::Gecko_nsFont_Destroy(&mut system); }
                 ret
             }
 
             fn from_computed_value(_: &ComputedSystemFont) -> Self {
                 unreachable!()
             }
@@ -2610,16 +2613,17 @@ https://drafts.csswg.org/css-fonts-4/#lo
         }
 
         #[derive(Clone, Debug)]
         pub struct ComputedSystemFont {
             % for name in SYSTEM_FONT_LONGHANDS:
                 pub ${name}: longhands::${name}::computed_value::T,
             % endfor
             pub system_font: SystemFont,
+            pub default_font_type: FontFamilyType,
         }
 
         impl SystemFont {
             pub fn parse<'i, 't>(input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
                 try_match_ident_ignore_ascii_case! { input.expect_ident()?,
                     % for font in system_fonts:
                         "${font}" => Ok(SystemFont::${to_camel_case(font)}),
                     % endfor
diff --git a/servo/components/style/properties/properties.mako.rs b/servo/components/style/properties/properties.mako.rs
--- a/servo/components/style/properties/properties.mako.rs
+++ b/servo/components/style/properties/properties.mako.rs
@@ -3326,30 +3326,25 @@ where
                                 //
                                 // In case of the language changing, we wish for a specified font-
                                 // family to override this, so we do not skip cascading then.
                                 _skip_font_family = true;
                             }
                         }
                     }
 
-                    // In case of just the language changing, the parent could have had no generic,
-                    // which Gecko just does regular cascading with. Do the same.
-                    // This can only happen in the case where the language changed but the family did not
-                    if generic != structs::kGenericFont_NONE {
-                        let pres_context = context.builder.device.pres_context();
-                        let gecko_font = context.builder.mutate_font().gecko_mut();
-                        gecko_font.mGenericID = generic;
-                        unsafe {
-                            bindings::Gecko_nsStyleFont_PrefillDefaultForGeneric(
-                                gecko_font,
-                                pres_context,
-                                generic,
-                            );
-                        }
+                    let pres_context = context.builder.device.pres_context();
+                    let gecko_font = context.builder.mutate_font().gecko_mut();
+                    gecko_font.mGenericID = generic;
+                    unsafe {
+                        bindings::Gecko_nsStyleFont_PrefillDefaultForGeneric(
+                            gecko_font,
+                            pres_context,
+                            generic,
+                        );
                     }
                 }
             % endif
 
             // It is important that font_size is computed before
             // the late properties (for em units), but after font-family
             // (for the base-font-size dependence for default and keyword font-sizes)
             // Additionally, when we support system fonts they will have to be
@@ -3366,17 +3361,19 @@ where
                     let discriminant = LonghandId::FontFamily as usize;
                     (CASCADE_PROPERTY[discriminant])(declaration,
                                                      &mut context,
                                                      &mut cascade_info);
                     % if product == "gecko":
                         let device = context.builder.device;
                         if let PropertyDeclaration::FontFamily(ref val) = **declaration {
                             if val.get_system().is_some() {
-                                context.builder.mutate_font().fixup_system();
+                                let default = context.cached_system_font
+                                                     .as_ref().unwrap().default_font_type;
+                                context.builder.mutate_font().fixup_system(default);
                             } else {
                                 context.builder.mutate_font().fixup_none_generic(device);
                             }
                         }
                     % endif
                 }
             }
 
