# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1512543908 -32400
#      Wed Dec 06 16:05:08 2017 +0900
# Node ID e35fce376c40ba862aba11384035d3e8b4cb9616
# Parent  b174ca627207ca06dceea9fc1fe491e05244fa99
Bug 1423461 - Use mozilla/Likely.h in mozalloc.cpp. r=njn

Back when mozalloc.cpp was written, mozilla/Likely.h didn't exist.

diff --git a/memory/mozalloc/mozalloc.cpp b/memory/mozalloc/mozalloc.cpp
--- a/memory/mozalloc/mozalloc.cpp
+++ b/memory/mozalloc/mozalloc.cpp
@@ -54,77 +54,70 @@ MOZ_MEMORY_API char *strndup_impl(const 
 
 #include <errno.h>
 #include <new>                  // for std::bad_alloc
 #include <string.h>
 
 #include <sys/types.h>
 
 #include "mozilla/Assertions.h"
+#include "mozilla/Likely.h"
 #include "mozilla/mozalloc.h"
 #include "mozilla/mozalloc_oom.h"  // for mozalloc_handle_oom
 
-#ifdef __GNUC__
-#define LIKELY(x)    (__builtin_expect(!!(x), 1))
-#define UNLIKELY(x)  (__builtin_expect(!!(x), 0))
-#else
-#define LIKELY(x)    (x)
-#define UNLIKELY(x)  (x)
-#endif
-
 void*
 moz_xmalloc(size_t size)
 {
     void* ptr = malloc_impl(size);
-    if (UNLIKELY(!ptr && size)) {
+    if (MOZ_UNLIKELY(!ptr && size)) {
         mozalloc_handle_oom(size);
         return moz_xmalloc(size);
     }
     return ptr;
 }
 
 void*
 moz_xcalloc(size_t nmemb, size_t size)
 {
     void* ptr = calloc_impl(nmemb, size);
-    if (UNLIKELY(!ptr && nmemb && size)) {
+    if (MOZ_UNLIKELY(!ptr && nmemb && size)) {
         mozalloc_handle_oom(size);
         return moz_xcalloc(nmemb, size);
     }
     return ptr;
 }
 
 void*
 moz_xrealloc(void* ptr, size_t size)
 {
     void* newptr = realloc_impl(ptr, size);
-    if (UNLIKELY(!newptr && size)) {
+    if (MOZ_UNLIKELY(!newptr && size)) {
         mozalloc_handle_oom(size);
         return moz_xrealloc(ptr, size);
     }
     return newptr;
 }
 
 char*
 moz_xstrdup(const char* str)
 {
     char* dup = strdup_impl(str);
-    if (UNLIKELY(!dup)) {
+    if (MOZ_UNLIKELY(!dup)) {
         mozalloc_handle_oom(0);
         return moz_xstrdup(str);
     }
     return dup;
 }
 
 #if defined(HAVE_STRNDUP)
 char*
 moz_xstrndup(const char* str, size_t strsize)
 {
     char* dup = strndup_impl(str, strsize);
-    if (UNLIKELY(!dup)) {
+    if (MOZ_UNLIKELY(!dup)) {
         mozalloc_handle_oom(strsize);
         return moz_xstrndup(str, strsize);
     }
     return dup;
 }
 #endif  // if defined(HAVE_STRNDUP)
 
 #ifndef HAVE_MEMALIGN
@@ -132,17 +125,17 @@ moz_xstrndup(const char* str, size_t str
 // necessarily come with a declaration.
 extern "C" void* memalign(size_t, size_t);
 #endif
 
 void*
 moz_xmemalign(size_t boundary, size_t size)
 {
     void* ptr = memalign_impl(boundary, size);
-    if (UNLIKELY(!ptr && EINVAL != errno)) {
+    if (MOZ_UNLIKELY(!ptr && EINVAL != errno)) {
         mozalloc_handle_oom(size);
         return moz_xmemalign(boundary, size);
     }
     // non-NULL ptr or errno == EINVAL
     return ptr;
 }
 
 size_t
