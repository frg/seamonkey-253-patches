# HG changeset patch
# User Stephanie Ouillon <stephouillon@mozilla.com>
# Date 1502954609 -7200
#      Thu Aug 17 09:23:29 2017 +0200
# Node ID 8192716611b6d4ab389b0ba2e1fd68be77abefd2
# Parent  0e4a32681bdde6634bd9aed9e366288474d50164
Bug 1343202 - Move Buffer definition into its own file; r=keeler,rbarnes

MozReview-Commit-ID: JHAXllvfG3x

diff --git a/security/certverifier/Buffer.cpp b/security/certverifier/Buffer.cpp
new file mode 100644
--- /dev/null
+++ b/security/certverifier/Buffer.cpp
@@ -0,0 +1,24 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "Buffer.h"
+
+namespace mozilla {
+
+bool
+operator==(const ct::Buffer& a, const ct::Buffer& b)
+{
+  return (a.empty() && b.empty()) ||
+    (a.length() == b.length() && memcmp(a.begin(), b.begin(), a.length()) == 0);
+}
+
+bool
+operator!=(const ct::Buffer& a, const ct::Buffer& b)
+{
+  return !(a == b);
+}
+
+} // namespace mozilla
diff --git a/security/certverifier/Buffer.h b/security/certverifier/Buffer.h
new file mode 100644
--- /dev/null
+++ b/security/certverifier/Buffer.h
@@ -0,0 +1,28 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef Buffer_h
+#define Buffer_h
+
+#include "mozilla/Vector.h"
+
+namespace mozilla { namespace ct {
+
+typedef Vector<uint8_t> Buffer;
+
+} } // namespace mozilla::ct
+
+namespace mozilla {
+
+// Comparison operators are placed under mozilla namespace since
+// mozilla::ct::Buffer is actually mozilla::Vector.
+bool operator==(const ct::Buffer& a, const ct::Buffer& b);
+bool operator!=(const ct::Buffer& a, const ct::Buffer& b);
+
+} // namespace mozilla
+
+
+#endif // Buffer_h
diff --git a/security/certverifier/SignedCertificateTimestamp.cpp b/security/certverifier/SignedCertificateTimestamp.cpp
--- a/security/certverifier/SignedCertificateTimestamp.cpp
+++ b/security/certverifier/SignedCertificateTimestamp.cpp
@@ -21,26 +21,8 @@ bool
 DigitallySigned::SignatureParametersMatch(HashAlgorithm aHashAlgorithm,
   SignatureAlgorithm aSignatureAlgorithm) const
 {
   return (hashAlgorithm == aHashAlgorithm) &&
          (signatureAlgorithm == aSignatureAlgorithm);
 }
 
 } } // namespace mozilla::ct
-
-
-namespace mozilla {
-
-bool
-operator==(const ct::Buffer& a, const ct::Buffer& b)
-{
-  return (a.empty() && b.empty()) ||
-    (a.length() == b.length() && memcmp(a.begin(), b.begin(), a.length()) == 0);
-}
-
-bool
-operator!=(const ct::Buffer& a, const ct::Buffer& b)
-{
-  return !(a == b);
-}
-
-} // namespace mozilla
diff --git a/security/certverifier/SignedCertificateTimestamp.h b/security/certverifier/SignedCertificateTimestamp.h
--- a/security/certverifier/SignedCertificateTimestamp.h
+++ b/security/certverifier/SignedCertificateTimestamp.h
@@ -2,25 +2,24 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef SignedCertificateTimestamp_h
 #define SignedCertificateTimestamp_h
 
+#include "Buffer.h"
 #include "mozilla/Vector.h"
 #include "mozpkix/Input.h"
 #include "mozpkix/Result.h"
 
 // Structures related to Certificate Transparency (RFC 6962).
 namespace mozilla { namespace ct {
 
-typedef Vector<uint8_t> Buffer;
-
 // LogEntry struct in RFC 6962, Section 3.1.
 struct LogEntry
 {
 
   // LogEntryType enum in RFC 6962, Section 3.1.
   enum class Type {
     X509 = 0,
     Precert = 1
@@ -101,18 +100,9 @@ inline pkix::Result InputToBuffer(pkix::
   if (!buffer.append(input.UnsafeGetData(), input.GetLength())) {
     return pkix::Result::FATAL_ERROR_NO_MEMORY;
   }
   return pkix::Success;
 }
 
 } } // namespace mozilla::ct
 
-namespace mozilla {
-
-// Comparison operators are placed under mozilla namespace since
-// mozilla::ct::Buffer is actually mozilla::Vector.
-bool operator==(const ct::Buffer& a, const ct::Buffer& b);
-bool operator!=(const ct::Buffer& a, const ct::Buffer& b);
-
-} // namespace mozilla
-
 #endif // SignedCertificateTimestamp_h
diff --git a/security/certverifier/moz.build b/security/certverifier/moz.build
--- a/security/certverifier/moz.build
+++ b/security/certverifier/moz.build
@@ -4,27 +4,29 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 with Files("**"):
     BUG_COMPONENT = ("Core", "Security: PSM")
 
 EXPORTS += [
     'BRNameMatchingPolicy.h',
+    'Buffer.h',
     'CertVerifier.h',
     'CTLog.h',
     'CTPolicyEnforcer.h',
     'CTVerifyResult.h',
     'OCSPCache.h',
     'SignedCertificateTimestamp.h',
     'SignedTreeHead.h',
 ]
 
 UNIFIED_SOURCES += [
     'BRNameMatchingPolicy.cpp',
+    'Buffer.cpp',
     'CertVerifier.cpp',
     'CTDiversityPolicy.cpp',
     'CTLogVerifier.cpp',
     'CTObjectsExtractor.cpp',
     'CTPolicyEnforcer.cpp',
     'CTSerialization.cpp',
     'CTVerifyResult.cpp',
     'MultiLogCTVerifier.cpp',
