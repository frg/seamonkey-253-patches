# HG changeset patch
# User Jeremy Chen <jeremychen@mozilla.com>
# Date 1503483243 -28800
#      Wed Aug 23 18:14:03 2017 +0800
# Node ID 665197f96a9bcbbac99fe24db19532876bef9dd1
# Parent  a80cbbac4625e39eedfac79c4827ae70bbce0701
Bug 1375332 - stylo: move minimum font size applying into the default constructor of nsStyleFont. r=emilio

In Stylo, we read font related user prefs to set the default font size only
if we set 'font-size' property. However, users are allowed to set their
preferred minimum font size through the user prefs, even without using
'font-size' property.

Gecko used to do this in nsRuleNode::SetDefaultOnRoot, which calles the
default constructor of nsStyleFont and does the minimum font size applying
right after. Moving the minimum font size applying into the default
constructor of nsStyleFont shoud be no harm to Gecko, but makes Stylo
share the same code path and behave the same.

MozReview-Commit-ID: BDcJX92o0uR

diff --git a/layout/style/nsRuleNode.cpp b/layout/style/nsRuleNode.cpp
--- a/layout/style/nsRuleNode.cpp
+++ b/layout/style/nsRuleNode.cpp
@@ -2836,24 +2836,16 @@ nsRuleNode::WalkRuleTree(const nsStyleSt
 
 const void*
 nsRuleNode::SetDefaultOnRoot(const nsStyleStructID aSID, GeckoStyleContext* aContext)
 {
   switch (aSID) {
     case eStyleStruct_Font:
     {
       nsStyleFont* fontData = new (mPresContext) nsStyleFont(mPresContext);
-      nscoord minimumFontSize = mPresContext->MinFontSize(fontData->mLanguage);
-
-      if (minimumFontSize > 0 && !mPresContext->IsChrome()) {
-        fontData->mFont.size = std::max(fontData->mSize, minimumFontSize);
-      }
-      else {
-        fontData->mFont.size = fontData->mSize;
-      }
       aContext->SetStyle(eStyleStruct_Font, fontData);
       return fontData;
     }
     case eStyleStruct_Display:
     {
       nsStyleDisplay* disp = new (mPresContext) nsStyleDisplay(mPresContext);
       aContext->SetStyle(eStyleStruct_Display, disp);
       return disp;
diff --git a/layout/style/nsStyleStruct.cpp b/layout/style/nsStyleStruct.cpp
--- a/layout/style/nsStyleStruct.cpp
+++ b/layout/style/nsStyleStruct.cpp
@@ -146,16 +146,23 @@ nsStyleFont::nsStyleFont(const nsStyleFo
   MOZ_COUNT_CTOR(nsStyleFont);
 }
 
 nsStyleFont::nsStyleFont(const nsPresContext* aContext)
   : nsStyleFont(*aContext->GetDefaultFont(kPresContext_DefaultVariableFont_ID,
                                           nullptr),
                 aContext)
 {
+  MOZ_ASSERT(NS_IsMainThread());
+  nscoord minimumFontSize = aContext->MinFontSize(mLanguage);
+  if (minimumFontSize > 0 && !aContext->IsChrome()) {
+    mFont.size = std::max(mSize, minimumFontSize);
+  } else {
+    mFont.size = mSize;
+  }
 }
 
 void
 nsStyleFont::Destroy(nsPresContext* aContext) {
   this->~nsStyleFont();
   aContext->PresShell()->
     FreeByObjectID(eArenaObjectID_nsStyleFont, this);
 }
