# HG changeset patch
# User Kai Engert <kaie@kuix.de>
# Date 1550739845 -3600
#      Thu Feb 21 10:04:05 2019 +0100
# Node ID cab425acc7bb3ec7f86607c798a9c1ff65204524
# Parent  7b896502001060f4e885bd1b95315ff74baa113f
Bug 1526010, NSPR_4_21_BETA2, r=me
UPGRADE_NSPR_RELEASE

diff --git a/nsprpub/TAG-INFO b/nsprpub/TAG-INFO
--- a/nsprpub/TAG-INFO
+++ b/nsprpub/TAG-INFO
@@ -1,1 +1,1 @@
-NSPR_4_21_BETA1
+NSPR_4_21_BETA2
diff --git a/nsprpub/config/prdepend.h b/nsprpub/config/prdepend.h
--- a/nsprpub/config/prdepend.h
+++ b/nsprpub/config/prdepend.h
@@ -5,9 +5,8 @@
 
 /*
  * A dummy header file that is a dependency for all the object files.
  * Used to force a full recompilation of NSPR in Mozilla's Tinderbox
  * depend builds.  See comments in rules.mk.
  */
 
 #error "Do not include this header file."
-
diff --git a/nsprpub/pr/src/md/unix/unix.c b/nsprpub/pr/src/md/unix/unix.c
--- a/nsprpub/pr/src/md/unix/unix.c
+++ b/nsprpub/pr/src/md/unix/unix.c
@@ -54,17 +54,17 @@
 #else
 #error "Cannot determine architecture"
 #endif
 
 /*
 ** Global lock variable used to bracket calls into rusty libraries that
 ** aren't thread safe (like libc, libX, etc).
 */
-static PRLock *_pr_rename_lock = NULL;
+static PRLock *_pr_unix_rename_lock = NULL;
 static PRMonitor *_pr_Xfe_mon = NULL;
 
 static PRInt64 minus_one;
 
 sigset_t timer_set;
 
 #if !defined(_PR_PTHREADS)
 
@@ -199,30 +199,30 @@ PRInt32 _MD_rename(const char *from, con
     PRInt32 rv = -1, err;
 
     /*
     ** This is trying to enforce the semantics of WINDOZE' rename
     ** operation. That means one is not allowed to rename over top
     ** of an existing file. Holding a lock across these two function
     ** and the open function is known to be a bad idea, but ....
     */
-    if (NULL != _pr_rename_lock)
-        PR_Lock(_pr_rename_lock);
+    if (NULL != _pr_unix_rename_lock)
+        PR_Lock(_pr_unix_rename_lock);
     if (0 == access(to, F_OK))
         PR_SetError(PR_FILE_EXISTS_ERROR, 0);
     else
     {
         rv = rename(from, to);
         if (rv < 0) {
             err = _MD_ERRNO();
             _PR_MD_MAP_RENAME_ERROR(err);
         }
     }
-    if (NULL != _pr_rename_lock)
-        PR_Unlock(_pr_rename_lock);
+    if (NULL != _pr_unix_rename_lock)
+        PR_Unlock(_pr_unix_rename_lock);
     return rv;
 }
 
 PRInt32 _MD_access(const char *name, PRAccessHow how)
 {
 PRInt32 rv, err;
 int amode;
 
@@ -255,25 +255,25 @@ done:
 PRInt32 _MD_mkdir(const char *name, PRIntn mode)
 {
 int rv, err;
 
     /*
     ** This lock is used to enforce rename semantics as described
     ** in PR_Rename. Look there for more fun details.
     */
-    if (NULL !=_pr_rename_lock)
-        PR_Lock(_pr_rename_lock);
+    if (NULL !=_pr_unix_rename_lock)
+        PR_Lock(_pr_unix_rename_lock);
     rv = mkdir(name, mode);
     if (rv < 0) {
         err = _MD_ERRNO();
         _PR_MD_MAP_MKDIR_ERROR(err);
     }
-    if (NULL !=_pr_rename_lock)
-        PR_Unlock(_pr_rename_lock);
+    if (NULL !=_pr_unix_rename_lock)
+        PR_Unlock(_pr_unix_rename_lock);
     return rv;
 }
 
 PRInt32 _MD_rmdir(const char *name)
 {
 int rv, err;
 
     rv = rmdir(name);
@@ -2214,33 +2214,33 @@ PRInt32 _MD_open(const char *name, PRInt
 
     /*
     ** On creations we hold the 'create' lock in order to enforce
     ** the semantics of PR_Rename. (see the latter for more details)
     */
     if (flags & PR_CREATE_FILE)
     {
         osflags |= O_CREAT;
-        if (NULL !=_pr_rename_lock)
-            PR_Lock(_pr_rename_lock);
+        if (NULL !=_pr_unix_rename_lock)
+            PR_Lock(_pr_unix_rename_lock);
     }
 
 #if defined(ANDROID)
     osflags |= O_LARGEFILE;
 #endif
 
     rv = _md_iovector._open64(name, osflags, mode);
 
     if (rv < 0) {
         err = _MD_ERRNO();
         _PR_MD_MAP_OPEN_ERROR(err);
     }
 
-    if ((flags & PR_CREATE_FILE) && (NULL !=_pr_rename_lock))
-        PR_Unlock(_pr_rename_lock);
+    if ((flags & PR_CREATE_FILE) && (NULL !=_pr_unix_rename_lock))
+        PR_Unlock(_pr_unix_rename_lock);
     return rv;
 }
 
 PRIntervalTime intr_timeout_ticks;
 
 #if defined(SOLARIS) || defined(IRIX)
 static void sigsegvhandler() {
     fprintf(stderr,"Received SIGSEGV\n");
@@ -2866,29 +2866,29 @@ void _PR_UnixInit(void)
 #endif  /* !defined(_PR_PTHREADS) */
 
     sigact.sa_handler = SIG_IGN;
     sigemptyset(&sigact.sa_mask);
     sigact.sa_flags = 0;
     rv = sigaction(SIGPIPE, &sigact, 0);
     PR_ASSERT(0 == rv);
 
-    _pr_rename_lock = PR_NewLock();
-    PR_ASSERT(NULL != _pr_rename_lock);
+    _pr_unix_rename_lock = PR_NewLock();
+    PR_ASSERT(NULL != _pr_unix_rename_lock);
     _pr_Xfe_mon = PR_NewMonitor();
     PR_ASSERT(NULL != _pr_Xfe_mon);
 
     _PR_InitIOV();  /* one last hack */
 }
 
 void _PR_UnixCleanup(void)
 {
-    if (_pr_rename_lock) {
-        PR_DestroyLock(_pr_rename_lock);
-        _pr_rename_lock = NULL;
+    if (_pr_unix_rename_lock) {
+        PR_DestroyLock(_pr_unix_rename_lock);
+        _pr_unix_rename_lock = NULL;
     }
     if (_pr_Xfe_mon) {
         PR_DestroyMonitor(_pr_Xfe_mon);
         _pr_Xfe_mon = NULL;
     }
 }
 
 #if !defined(_PR_PTHREADS)
