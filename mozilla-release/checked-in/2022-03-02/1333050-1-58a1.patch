# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1506022896 -7200
# Node ID 3ff6b103fa9b73a4263fa85af671959ce54e97aa
# Parent  2caa22ae308a9a31fba9daa59e4aa311508334a2
Bug 1333050 - Introduce BrowsingData.removeIndexedDB, r=kmag, r=janv

diff --git a/browser/components/extensions/ext-browsingData.js b/browser/components/extensions/ext-browsingData.js
--- a/browser/components/extensions/ext-browsingData.js
+++ b/browser/components/extensions/ext-browsingData.js
@@ -12,16 +12,19 @@ XPCOMUtils.defineLazyModuleGetter(this, 
 XPCOMUtils.defineLazyModuleGetter(this, "Services",
                                   "resource://gre/modules/Services.jsm");
 XPCOMUtils.defineLazyModuleGetter(this, "setTimeout",
                                   "resource://gre/modules/Timer.jsm");
 
 XPCOMUtils.defineLazyServiceGetter(this, "serviceWorkerManager",
                                    "@mozilla.org/serviceworkers/manager;1",
                                    "nsIServiceWorkerManager");
+XPCOMUtils.defineLazyServiceGetter(this, "quotaManagerService",
+                                   "@mozilla.org/dom/quota-manager-service;1",
+                                   "nsIQuotaManagerService");
 
 /**
 * A number of iterations after which to yield time back
 * to the system.
 */
 const YIELD_PERIOD = 10;
 
 const PREF_DOMAIN = "privacy.cpd.";
@@ -78,16 +81,39 @@ const clearDownloads = options => {
 const clearFormData = options => {
   return sanitizer.items.formdata.clear(makeRange(options));
 };
 
 const clearHistory = options => {
   return sanitizer.items.history.clear(makeRange(options));
 };
 
+const clearIndexedDB = async function(options) {
+  let promises = [];
+
+  await new Promise(resolve => {
+    quotaManagerService.getUsage(request => {
+      for (let item of request.result) {
+        let principal = Services.scriptSecurityManager.createCodebasePrincipalFromOrigin(item.origin);
+        let uri = principal.URI;
+        if (uri.scheme == "http" || uri.scheme == "https" || uri.scheme == "file") {
+          promises.push(new Promise(r => {
+            let req = quotaManagerService.clearStoragesForPrincipal(principal, null, true);
+            req.callback = () => { r(); };
+          }));
+        }
+      }
+
+      resolve();
+    });
+  });
+
+  return Promise.all(promises);
+};
+
 const clearLocalStorage = async function(options) {
   Services.obs.notifyObservers(null, "extension:purge-localStorage");
 };
 
 const clearPasswords = async function(options) {
   let loginManager = Services.logins;
   let yieldCounter = 0;
 
@@ -151,16 +177,19 @@ const doRemoval = (options, dataToRemove
           removalPromises.push(clearDownloads(options));
           break;
         case "formData":
           removalPromises.push(clearFormData(options));
           break;
         case "history":
           removalPromises.push(clearHistory(options));
           break;
+        case "indexedDB":
+          removalPromises.push(clearIndexedDB(options));
+          break;
         case "localStorage":
           removalPromises.push(clearLocalStorage(options));
           break;
         case "passwords":
           removalPromises.push(clearPasswords(options));
           break;
         case "pluginData":
           removalPromises.push(clearPluginData(options));
@@ -227,16 +256,19 @@ this.browsingData = class extends Extens
           return doRemoval(options, {downloads: true});
         },
         removeFormData(options) {
           return doRemoval(options, {formData: true});
         },
         removeHistory(options) {
           return doRemoval(options, {history: true});
         },
+        removeIndexedDB(options) {
+          return doRemoval(options, {indexedDB: true});
+        },
         removeLocalStorage(options) {
           return doRemoval(options, {localStorage: true});
         },
         removePasswords(options) {
           return doRemoval(options, {passwords: true});
         },
         removePluginData(options) {
           return doRemoval(options, {pluginData: true});
diff --git a/browser/components/extensions/test/xpcshell/test_ext_browsingData.js b/browser/components/extensions/test/xpcshell/test_ext_browsingData.js
--- a/browser/components/extensions/test/xpcshell/test_ext_browsingData.js
+++ b/browser/components/extensions/test/xpcshell/test_ext_browsingData.js
@@ -36,31 +36,8 @@ add_task(async function testInvalidArgum
     },
   };
 
   let extension = ExtensionTestUtils.loadExtension(extensionData);
   await extension.startup();
   await extension.awaitFinish("invalidArguments");
   await extension.unload();
 });
-
-add_task(async function testUnimplementedDataType() {
-  function background() {
-    browser.browsingData.remove({}, {indexedDB: true});
-    browser.test.sendMessage("finished");
-  }
-
-  let {messages} = await promiseConsoleOutput(async function() {
-    let extension = ExtensionTestUtils.loadExtension({
-      background: background,
-      manifest: {
-        permissions: ["browsingData"],
-      },
-    });
-
-    await extension.startup();
-    await extension.awaitMessage("finished");
-    await extension.unload();
-  });
-
-  let warningObserved = messages.find(line => /Firefox does not support dataTypes: indexedDB/.test(line));
-  ok(warningObserved, "Warning issued when calling remove with an unimplemented dataType.");
-});
