# HG changeset patch
# User James Cheng <jacheng@mozilla.com>
# Date 1504062830 -28800
#      Wed Aug 30 11:13:50 2017 +0800
# Node ID a104181ac18b7d25cb058790d09a024d24f0a333
# Parent  4eec43df6b84e8500b9ca901b8ddd88dbdad0ed8
Bug 1393710 - Add thread name into NS_DebugBreak for better debugging. r=froydnj

MozReview-Commit-ID: AvqajMgtpuh

diff --git a/xpcom/base/nsDebugImpl.cpp b/xpcom/base/nsDebugImpl.cpp
--- a/xpcom/base/nsDebugImpl.cpp
+++ b/xpcom/base/nsDebugImpl.cpp
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 // Chromium headers must come before Mozilla headers.
 #include "base/process_util.h"
 
 #include "mozilla/Atomics.h"
 #include "mozilla/Printf.h"
 
+#include "MainThreadUtils.h"
 #include "nsDebugImpl.h"
 #include "nsDebug.h"
 #ifdef MOZ_CRASHREPORTER
 # include "nsExceptionHandler.h"
 #endif
 #include "nsString.h"
 #include "nsXULAppAPI.h"
 #include "prprf.h"
@@ -347,18 +348,27 @@ NS_DebugBreak(uint32_t aSeverity, const 
     nonPIDBuf.print("line %d", aLine);
   }
 
   // Print "[PID]" or "[Desc PID]" at the beginning of the message.
   buf.print("[");
   if (sMultiprocessDescription) {
     buf.print("%s ", sMultiprocessDescription);
   }
-  buf.print("%d] %s", base::GetCurrentProcId(), nonPIDBuf.buffer);
 
+  bool isMainthread = (NS_IsMainThreadTLSInitialized() && NS_IsMainThread());
+  PRThread *currentThread = PR_GetCurrentThread();
+  const char *currentThreadName = isMainthread
+    ? "Main Thread"
+    : PR_GetThreadName(currentThread);
+  if(currentThreadName) {
+    buf.print("%d, %s] %s", base::GetCurrentProcId(), currentThreadName , nonPIDBuf.buffer);
+  } else {
+    buf.print("%d, Unnamed thread %p] %s", base::GetCurrentProcId(), currentThread, nonPIDBuf.buffer);
+  }
 
   // errors on platforms without a debugdlg ring a bell on stderr
 #if !defined(XP_WIN)
   if (aSeverity != NS_DEBUG_WARNING) {
     fprintf(stderr, "\07");
   }
 #endif
 
diff --git a/xpcom/threads/MainThreadUtils.h b/xpcom/threads/MainThreadUtils.h
--- a/xpcom/threads/MainThreadUtils.h
+++ b/xpcom/threads/MainThreadUtils.h
@@ -23,12 +23,13 @@ extern nsresult NS_GetMainThread(nsIThre
 // Fast access to the current thread.  Do not release the returned pointer!  If
 // you want to use this pointer from some other thread, then you will need to
 // AddRef it.  Otherwise, you should only consider this pointer valid from code
 // running on the current thread.
 extern nsIThread* NS_GetCurrentThread();
 #endif
 
 #ifdef MOZILLA_INTERNAL_API
+bool NS_IsMainThreadTLSInitialized();
 bool NS_IsMainThread();
 #endif
 
 #endif // MainThreadUtils_h_
diff --git a/xpcom/threads/nsThreadManager.cpp b/xpcom/threads/nsThreadManager.cpp
--- a/xpcom/threads/nsThreadManager.cpp
+++ b/xpcom/threads/nsThreadManager.cpp
@@ -29,16 +29,22 @@
 #include "InputEventStatistics.h"
 
 using namespace mozilla;
 
 static MOZ_THREAD_LOCAL(bool) sTLSIsMainThread;
 static MOZ_THREAD_LOCAL(PRThread*) gTlsCurrentVirtualThread;
 
 bool
+NS_IsMainThreadTLSInitialized()
+{
+  return sTLSIsMainThread.initialized();
+}
+
+bool
 NS_IsMainThread()
 {
   return sTLSIsMainThread.get();
 }
 
 void
 NS_SetMainThread()
 {
