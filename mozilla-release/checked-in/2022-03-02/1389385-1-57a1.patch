# HG changeset patch
# User Bobby Holley <bobbyholley@gmail.com>
# Date 1500578911 25200
#      Thu Jul 20 12:28:31 2017 -0700
# Node ID 3342b1c0d5eceeafb284dd3f562665f6ea203a6d
# Parent  fbb78984a60ec54544cf31b31f42e0a88c34a920
Bug 1389385 - Move NoteDirtyDescendantsForServo out of line. r=emilio

This function is large enough that it doesn't really make sense to have inline,
and we'll be adding more to it in the coming patches.

MozReview-Commit-ID: AnDfzwsMvNy

diff --git a/dom/base/Element.cpp b/dom/base/Element.cpp
--- a/dom/base/Element.cpp
+++ b/dom/base/Element.cpp
@@ -4293,8 +4293,48 @@ Element::AddSizeOfExcludingThis(SizeOfSt
           if (!aState.HaveSeenPtr(sc.get())) {
             sc->AddSizeOfIncludingThis(aState, aSizes, /* isDOM = */ true);
           }
         }
       }
     }
   }
 }
+
+void
+Element::NoteDirtyDescendantsForServo()
+{
+  if (!HasServoData()) {
+    // The dirty descendants bit only applies to styled elements.
+    return;
+  }
+
+  Element* curr = this;
+  while (curr && !curr->HasDirtyDescendantsForServo()) {
+    curr->SetHasDirtyDescendantsForServo();
+    curr = curr->GetFlattenedTreeParentElementForStyle();
+  }
+
+  if (nsIPresShell* shell = OwnerDoc()->GetShell()) {
+    shell->EnsureStyleFlush();
+  }
+
+  MOZ_ASSERT(DirtyDescendantsBitIsPropagatedForServo());
+}
+
+#ifdef DEBUG
+bool
+Element::DirtyDescendantsBitIsPropagatedForServo()
+{
+  Element* curr = this;
+  while (curr) {
+    if (!curr->HasDirtyDescendantsForServo()) {
+      return false;
+    }
+    nsINode* parentNode = curr->GetParentNode();
+    curr = curr->GetFlattenedTreeParentElementForStyle();
+    MOZ_ASSERT_IF(!curr,
+                  parentNode == OwnerDoc() ||
+                  parentNode == parentNode->OwnerDoc()->GetRootElement());
+  }
+  return true;
+}
+#endif
diff --git a/dom/base/Element.h b/dom/base/Element.h
--- a/dom/base/Element.h
+++ b/dom/base/Element.h
@@ -482,30 +482,30 @@ public:
     SetFlags(ELEMENT_HAS_DIRTY_DESCENDANTS_FOR_SERVO);
   }
 
   void UnsetHasDirtyDescendantsForServo() {
     MOZ_ASSERT(IsStyledByServo());
     UnsetFlags(ELEMENT_HAS_DIRTY_DESCENDANTS_FOR_SERVO);
   }
 
-  inline void NoteDirtyDescendantsForServo();
+  void NoteDirtyDescendantsForServo();
 
   bool HasAnimationOnlyDirtyDescendantsForServo() const {
     MOZ_ASSERT(IsStyledByServo());
     return HasFlag(ELEMENT_HAS_ANIMATION_ONLY_DIRTY_DESCENDANTS_FOR_SERVO);
   }
 
   void UnsetHasAnimationOnlyDirtyDescendantsForServo() {
     MOZ_ASSERT(IsStyledByServo());
     UnsetFlags(ELEMENT_HAS_ANIMATION_ONLY_DIRTY_DESCENDANTS_FOR_SERVO);
   }
 
 #ifdef DEBUG
-  inline bool DirtyDescendantsBitIsPropagatedForServo();
+  bool DirtyDescendantsBitIsPropagatedForServo();
 #endif
 
   bool HasServoData() const {
     return !!mServoData.Get();
   }
 
   void ClearServoData();
 
diff --git a/dom/base/ElementInlines.h b/dom/base/ElementInlines.h
--- a/dom/base/ElementInlines.h
+++ b/dom/base/ElementInlines.h
@@ -46,52 +46,12 @@ Element::GetFlattenedTreeParentElementFo
   nsINode* parentNode = GetFlattenedTreeParentNodeForStyle();
   if MOZ_LIKELY(parentNode && parentNode->IsElement()) {
     return parentNode->AsElement();
   }
 
   return nullptr;
 }
 
-inline void
-Element::NoteDirtyDescendantsForServo()
-{
-  if (!HasServoData()) {
-    // The dirty descendants bit only applies to styled elements.
-    return;
-  }
-
-  Element* curr = this;
-  while (curr && !curr->HasDirtyDescendantsForServo()) {
-    curr->SetHasDirtyDescendantsForServo();
-    curr = curr->GetFlattenedTreeParentElementForStyle();
-  }
-
-  if (nsIPresShell* shell = OwnerDoc()->GetShell()) {
-    shell->EnsureStyleFlush();
-  }
-
-  MOZ_ASSERT(DirtyDescendantsBitIsPropagatedForServo());
-}
-
-#ifdef DEBUG
-inline bool
-Element::DirtyDescendantsBitIsPropagatedForServo()
-{
-  Element* curr = this;
-  while (curr) {
-    if (!curr->HasDirtyDescendantsForServo()) {
-      return false;
-    }
-    nsINode* parentNode = curr->GetParentNode();
-    curr = curr->GetFlattenedTreeParentElementForStyle();
-    MOZ_ASSERT_IF(!curr,
-                  parentNode == OwnerDoc() ||
-                  parentNode == parentNode->OwnerDoc()->GetRootElement());
-  }
-  return true;
-}
-#endif
-
 } // namespace dom
 } // namespace mozilla
 
 #endif // mozilla_dom_ElementInlines_h
