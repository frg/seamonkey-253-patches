# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1501720622 -36000
#      Thu Aug 03 10:37:02 2017 +1000
# Node ID e99dcc6bd34cd97eb274b17d724a1143bdc34804
# Parent  876d16fa61ae6d3ecde58e1e6eca40f82fade01e
Bug 1386871 - Move font-display pref into StylePrefs. r=heycam

MozReview-Commit-ID: 9CDZSdTOacs

diff --git a/layout/style/StylePrefs.cpp b/layout/style/StylePrefs.cpp
--- a/layout/style/StylePrefs.cpp
+++ b/layout/style/StylePrefs.cpp
@@ -5,27 +5,30 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/StylePrefs.h"
 
 #include "mozilla/Preferences.h"
 
 namespace mozilla {
 
+bool StylePrefs::sFontDisplayEnabled;
 bool StylePrefs::sOpentypeSVGEnabled;
 bool StylePrefs::sWebkitPrefixedAliasesEnabled;
 bool StylePrefs::sWebkitDevicePixelRatioEnabled;
 bool StylePrefs::sMozGradientsEnabled;
 bool StylePrefs::sControlCharVisibility;
 bool StylePrefs::sFramesTimingFunctionEnabled;
 bool StylePrefs::sMozDocumentEnabledInContent;
 
 /* static */ void
 StylePrefs::Init()
 {
+  Preferences::AddBoolVarCache(&sFontDisplayEnabled,
+                               "layout.css.font-display.enabled");
   Preferences::AddBoolVarCache(&sOpentypeSVGEnabled,
                                "gfx.font_rendering.opentype_svg.enabled");
   Preferences::AddBoolVarCache(&sWebkitPrefixedAliasesEnabled,
                                "layout.css.prefixes.webkit");
   Preferences::AddBoolVarCache(&sWebkitDevicePixelRatioEnabled,
                                "layout.css.prefixes.device-pixel-ratio-webkit");
   Preferences::AddBoolVarCache(&sMozGradientsEnabled,
                                "layout.css.prefixes.gradients");
diff --git a/layout/style/StylePrefs.h b/layout/style/StylePrefs.h
--- a/layout/style/StylePrefs.h
+++ b/layout/style/StylePrefs.h
@@ -8,16 +8,17 @@
 
 #ifndef mozilla_StylePrefs_h
 #define mozilla_StylePrefs_h
 
 namespace mozilla {
 
 struct StylePrefs
 {
+  static bool sFontDisplayEnabled;
   static bool sOpentypeSVGEnabled;
   static bool sWebkitPrefixedAliasesEnabled;
   static bool sWebkitDevicePixelRatioEnabled;
   static bool sMozGradientsEnabled;
   static bool sControlCharVisibility;
   static bool sFramesTimingFunctionEnabled;
   static bool sMozDocumentEnabledInContent;
 
diff --git a/layout/style/nsCSSParser.cpp b/layout/style/nsCSSParser.cpp
--- a/layout/style/nsCSSParser.cpp
+++ b/layout/style/nsCSSParser.cpp
@@ -3979,18 +3979,17 @@ CSSParserImpl::ParseFontDescriptor(nsCSS
     OUTPUT_ERROR();
     return false;
   }
 
   nsCSSFontDesc descID = nsCSSProps::LookupFontDesc(descName);
   nsCSSValue value;
 
   if (descID == eCSSFontDesc_UNKNOWN ||
-      (descID == eCSSFontDesc_Display &&
-       !Preferences::GetBool("layout.css.font-display.enabled"))) {
+      (descID == eCSSFontDesc_Display && !StylePrefs::sFontDisplayEnabled)) {
     if (NonMozillaVendorIdentifier(descName)) {
       // silently skip other vendors' extensions
       Unused << SkipDeclaration(true);
       return true;
     } else {
       REPORT_UNEXPECTED_P(PEUnknownFontDesc, descName);
       return false;
     }
diff --git a/layout/style/nsCSSProps.cpp b/layout/style/nsCSSProps.cpp
--- a/layout/style/nsCSSProps.cpp
+++ b/layout/style/nsCSSProps.cpp
@@ -21,16 +21,17 @@
 
 #include "mozilla/dom/AnimationEffectReadOnlyBinding.h" // for PlaybackDirection
 #include "mozilla/LookAndFeel.h" // for system colors
 
 #include "nsString.h"
 #include "nsStaticNameTable.h"
 
 #include "mozilla/Preferences.h"
+#include "mozilla/StylePrefs.h"
 
 using namespace mozilla;
 
 typedef nsCSSProps::KTableEntry KTableEntry;
 
 // By wrapping internal-only properties in this macro, we are not
 // exposing them in the CSSOM. Since currently it is not necessary to
 // allow accessing them in that way, it is easier and cheaper to just
@@ -612,18 +613,17 @@ nsCSSProps::LookupPropertyByIDLName(cons
 }
 
 nsCSSFontDesc
 nsCSSProps::LookupFontDesc(const nsACString& aFontDesc)
 {
   MOZ_ASSERT(gFontDescTable, "no lookup table, needs addref");
   nsCSSFontDesc which = nsCSSFontDesc(gFontDescTable->Lookup(aFontDesc));
 
-  if (which == eCSSFontDesc_Display &&
-      !Preferences::GetBool("layout.css.font-display.enabled")) {
+  if (which == eCSSFontDesc_Display && !StylePrefs::sFontDisplayEnabled) {
     which = eCSSFontDesc_UNKNOWN;
   } else if (which == eCSSFontDesc_UNKNOWN) {
     // check for unprefixed font-feature-settings/font-language-override
     nsAutoCString prefixedProp;
     prefixedProp.AppendLiteral("-moz-");
     prefixedProp.Append(aFontDesc);
     which = nsCSSFontDesc(gFontDescTable->Lookup(prefixedProp));
   }
@@ -631,18 +631,17 @@ nsCSSProps::LookupFontDesc(const nsACStr
 }
 
 nsCSSFontDesc
 nsCSSProps::LookupFontDesc(const nsAString& aFontDesc)
 {
   MOZ_ASSERT(gFontDescTable, "no lookup table, needs addref");
   nsCSSFontDesc which = nsCSSFontDesc(gFontDescTable->Lookup(aFontDesc));
 
-  if (which == eCSSFontDesc_Display &&
-      !Preferences::GetBool("layout.css.font-display.enabled")) {
+  if (which == eCSSFontDesc_Display && !StylePrefs::sFontDisplayEnabled) {
     which = eCSSFontDesc_UNKNOWN;
   } else if (which == eCSSFontDesc_UNKNOWN) {
     // check for unprefixed font-feature-settings/font-language-override
     nsAutoString prefixedProp;
     prefixedProp.AppendLiteral("-moz-");
     prefixedProp.Append(aFontDesc);
     which = nsCSSFontDesc(gFontDescTable->Lookup(prefixedProp));
   }
diff --git a/layout/style/nsFontFaceLoader.cpp b/layout/style/nsFontFaceLoader.cpp
--- a/layout/style/nsFontFaceLoader.cpp
+++ b/layout/style/nsFontFaceLoader.cpp
@@ -9,16 +9,17 @@
 #include "mozilla/IntegerPrintfMacros.h"
 #include "mozilla/Logging.h"
 
 #include "nsFontFaceLoader.h"
 
 #include "nsError.h"
 #include "nsContentUtils.h"
 #include "mozilla/Preferences.h"
+#include "mozilla/StylePrefs.h"
 #include "FontFaceSet.h"
 #include "nsPresContext.h"
 #include "nsIPrincipal.h"
 #include "nsIScriptSecurityManager.h"
 #include "nsIHttpChannel.h"
 #include "nsIContentPolicy.h"
 #include "nsContentPolicyUtils.h"
 
@@ -300,13 +301,13 @@ nsFontFaceLoader::Cancel()
   }
   mChannel->Cancel(NS_BINDING_ABORTED);
 }
 
 uint8_t
 nsFontFaceLoader::GetFontDisplay()
 {
   uint8_t fontDisplay = NS_FONT_DISPLAY_AUTO;
-  if (Preferences::GetBool("layout.css.font-display.enabled")) {
+  if (StylePrefs::sFontDisplayEnabled) {
     fontDisplay = mUserFontEntry->GetFontDisplay();
   }
   return fontDisplay;
 }
