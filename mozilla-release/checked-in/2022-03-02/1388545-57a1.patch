# HG changeset patch
# User Jed Davis <jld@mozilla.com>
# Date 1502230672 21600
#      Tue Aug 08 16:17:52 2017 -0600
# Node ID babbce26f79b577209f11e953e1332baff93dea3
# Parent  db6484fe669243229ce91a388557db4ffe4733f9
Bug 1388545 - Fix PulseAudio breakage caused by read restrictions. r=gcp

MozReview-Commit-ID: 518mslh9xy

diff --git a/security/sandbox/linux/broker/SandboxBrokerPolicyFactory.cpp b/security/sandbox/linux/broker/SandboxBrokerPolicyFactory.cpp
--- a/security/sandbox/linux/broker/SandboxBrokerPolicyFactory.cpp
+++ b/security/sandbox/linux/broker/SandboxBrokerPolicyFactory.cpp
@@ -74,23 +74,30 @@ SandboxBrokerPolicyFactory::SandboxBroke
   policy->AddDir(rdwr, "/dev/dri");
 
 #ifdef MOZ_ALSA
   // Bug 1309098: ALSA support
   policy->AddDir(rdwr, "/dev/snd");
 #endif
 
 #ifdef MOZ_WIDGET_GTK
-  // Bug 1321134: DConf's single bit of shared memory
   if (const auto userDir = g_get_user_runtime_dir()) {
+    // Bug 1321134: DConf's single bit of shared memory
     // The leaf filename is "user" by default, but is configurable.
     nsPrintfCString shmPath("%s/dconf/", userDir);
     policy->AddPrefix(rdwrcr, shmPath.get());
+#ifdef MOZ_PULSEAUDIO
+    // PulseAudio, if it can't get server info from X11, will break
+    // unless it can open this directory (or create it, but in our use
+    // case we know it already exists).  See bug 1335329.
+    nsPrintfCString pulsePath("%s/pulse", userDir);
+    policy->AddPath(rdonly, pulsePath.get());
+#endif // MOZ_PULSEAUDIO
   }
-#endif
+#endif // MOZ_WIDGET_GTK
 
   // Read permissions
   policy->AddPath(rdonly, "/dev/urandom");
   policy->AddPath(rdonly, "/proc/cpuinfo");
   policy->AddPath(rdonly, "/proc/meminfo");
   policy->AddDir(rdonly, "/lib");
   policy->AddDir(rdonly, "/etc");
   policy->AddDir(rdonly, "/usr/share");
@@ -106,16 +113,23 @@ SandboxBrokerPolicyFactory::SandboxBroke
   policy->AddDir(rdonly, "/nix/store");
 
   // Bug 1384178: Mesa driver loader
   policy->AddPrefix(rdonly, "/sys/dev/char/226:");
 
   // Bug 1385715: NVIDIA PRIME support
   policy->AddPath(rdonly, "/proc/modules");
 
+#ifdef MOZ_PULSEAUDIO
+  // See bug 1384986 comment #1.
+  if (const auto xauth = PR_GetEnv("XAUTHORITY")) {
+    policy->AddPath(rdonly, xauth);
+  }
+#endif
+
   // Configuration dirs in the homedir that we want to allow read
   // access to.
   mozilla::Array<const char*, 3> confDirs = {
     ".config",
     ".themes",
     ".fonts",
   };
 
diff --git a/security/sandbox/linux/broker/moz.build b/security/sandbox/linux/broker/moz.build
--- a/security/sandbox/linux/broker/moz.build
+++ b/security/sandbox/linux/broker/moz.build
@@ -14,16 +14,18 @@ SOURCES += [
     'SandboxBroker.cpp',
     'SandboxBrokerCommon.cpp',
     'SandboxBrokerPolicyFactory.cpp',
     'SandboxBrokerRealpath.cpp',
 ]
 
 if CONFIG['MOZ_ALSA']:
     DEFINES['MOZ_ALSA'] = True
+if CONFIG['MOZ_PULSEAUDIO']:
+    DEFINES['MOZ_PULSEAUDIO'] = True
 
 LOCAL_INCLUDES += [
     '/security/sandbox/linux', # SandboxLogging.h, SandboxInfo.h
 ]
 
 # Need this for mozilla::ipc::FileDescriptor etc.
 include('/ipc/chromium/chromium-config.mozbuild')
 
