# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1503606820 25200
#      Thu Aug 24 13:33:40 2017 -0700
# Node ID c617d5572903568f4e861b8b648cb6aa9b15fada
# Parent  a7b1097ffc1e1c6ef22726d080989dde56cf3514
Bug 1391405: Part 5 - Add helper for retrieving the enumerable value properties of a cross-compartment object. r=gabor,qdot

As part of the normalization process for WebExtension API calls, we need to
extract and validate the full set of value properties (including properties
X-rays would normally deny access to) from cross-compartment objects. This
currently involves waiving X-rays, enumerating property descriptors, and
unwaiving X-rays - all through X-ray wrappers and waivers - and generating a
lot of expensive and short-lived wrappers in-between.

This helper reads out the list of safe properties from within the object's
compartment, and then copies them over to an object in the target compartment,
without any X-ray overhead, or any unnecessary intermediate wrappers or
compartment switches. It cuts about 40% off the overhead of our normalization
code.

MozReview-Commit-ID: H582oAYocaX

diff --git a/dom/base/ChromeUtils.cpp b/dom/base/ChromeUtils.cpp
--- a/dom/base/ChromeUtils.cpp
+++ b/dom/base/ChromeUtils.cpp
@@ -173,16 +173,104 @@ ChromeUtils::GetClassName(GlobalObject& 
   if (aUnwrap) {
     obj = js::UncheckedUnwrap(obj, /* stopAtWindowProxy = */ false);
   }
 
   aRetval = NS_ConvertUTF8toUTF16(nsDependentCString(js::GetObjectClass(obj)->name));
 }
 
 /* static */ void
+ChromeUtils::ShallowClone(GlobalObject& aGlobal,
+                          JS::HandleObject aObj,
+                          JS::HandleObject aTarget,
+                          JS::MutableHandleObject aRetval,
+                          ErrorResult& aRv)
+{
+  JSContext* cx = aGlobal.Context();
+
+  auto cleanup = MakeScopeExit([&] () {
+    aRv.NoteJSContextException(cx);
+  });
+
+  JS::Rooted<JS::IdVector> ids(cx, JS::IdVector(cx));
+  JS::AutoValueVector values(cx);
+
+  {
+    JS::RootedObject obj(cx, js::CheckedUnwrap(aObj));
+    if (!obj) {
+      js::ReportAccessDenied(cx);
+      return;
+    }
+
+    if (js::IsScriptedProxy(obj)) {
+      JS_ReportErrorASCII(cx, "Shallow cloning a proxy object is not allowed");
+      return;
+    }
+
+    JSAutoCompartment ac(cx, obj);
+
+    if (!JS_Enumerate(cx, obj, &ids) ||
+        !values.reserve(ids.length())) {
+      return;
+    }
+
+    JS::Rooted<JS::PropertyDescriptor> desc(cx);
+    JS::RootedId id(cx);
+    for (jsid idVal : ids) {
+      id = idVal;
+      if (!JS_GetOwnPropertyDescriptorById(cx, obj, id, &desc)) {
+        continue;
+      }
+      if (desc.setter() || desc.getter()) {
+        continue;
+      }
+      values.infallibleAppend(desc.value());
+    }
+  }
+
+  JS::RootedObject obj(cx);
+  {
+    Maybe<JSAutoCompartment> ac;
+    if (aTarget) {
+      JS::RootedObject target(cx, js::CheckedUnwrap(aTarget));
+      if (!target) {
+        js::ReportAccessDenied(cx);
+        return;
+      }
+      ac.emplace(cx, target);
+    }
+
+    obj = JS_NewPlainObject(cx);
+    if (!obj) {
+      return;
+    }
+
+    JS::RootedValue value(cx);
+    JS::RootedId id(cx);
+    for (uint32_t i = 0; i < ids.length(); i++) {
+      id = ids[i];
+      value = values[i];
+
+      JS_MarkCrossZoneId(cx, id);
+      if (!JS_WrapValue(cx, &value) ||
+          !JS_SetPropertyById(cx, obj, id, value)) {
+        return;
+      }
+    }
+  }
+
+  if (aTarget && !JS_WrapObject(cx, &obj)) {
+    return;
+  }
+
+  cleanup.release();
+  aRetval.set(obj);
+}
+
+/* static */ void
 ChromeUtils::Import(const GlobalObject& aGlobal,
                     const nsAString& aResourceURI,
                     const Optional<JS::Handle<JSObject*>>& aTargetObj,
                     JS::MutableHandle<JSObject*> aRetval,
                     ErrorResult& aRv)
 {
 
   RefPtr<mozJSComponentLoader> moduleloader = mozJSComponentLoader::Get();
diff --git a/dom/base/ChromeUtils.h b/dom/base/ChromeUtils.h
--- a/dom/base/ChromeUtils.h
+++ b/dom/base/ChromeUtils.h
@@ -130,16 +130,22 @@ public:
                            JS::MutableHandleValue aRetval,
                            ErrorResult& aRv);
 
   static void GetClassName(GlobalObject& aGlobal,
                            JS::HandleObject aObj,
                            bool aUnwrap,
                            nsAString& aRetval);
 
+  static void ShallowClone(GlobalObject& aGlobal,
+                           JS::HandleObject aObj,
+                           JS::HandleObject aTarget,
+                           JS::MutableHandleObject aRetval,
+                           ErrorResult& aRv);
+
   static void Import(const GlobalObject& aGlobal,
                      const nsAString& aResourceURI,
                      const Optional<JS::Handle<JSObject*>>& aTargetObj,
                      JS::MutableHandle<JSObject*> aRetval,
                      ErrorResult& aRv);
 
   static void DefineModuleGetter(const GlobalObject& global,
                                  JS::Handle<JSObject*> target,
diff --git a/dom/webidl/ChromeUtils.webidl b/dom/webidl/ChromeUtils.webidl
--- a/dom/webidl/ChromeUtils.webidl
+++ b/dom/webidl/ChromeUtils.webidl
@@ -175,16 +175,28 @@ partial namespace ChromeUtils {
    * Gets the name of the JSClass of the object.
    *
    * if |unwrap| is true, all wrappers are unwrapped first. Unless you're
    * specifically trying to detect whether the object is a proxy, this is
    * probably what you want.
    */
   DOMString getClassName(object obj, optional boolean unwrap = true);
 
+ /**
+  * Clones the properties of the given object into a new object in the given
+  * target compartment (or the caller compartment if no target is provided).
+  * Property values themeselves are not cloned.
+  *
+  * Ignores non-enumerable properties, properties on prototypes, and properties
+  * with getters or setters.
+  */
+ [Throws]
+ object shallowClone(object obj, optional object? target = null);
+
+
   /**
    * Synchronously loads and evaluates the js file located at
    * 'aResourceURI' with a new, fully privileged global object.
    *
    * If 'aTargetObj' is specified and null, this method just returns
    * the module's global object. Otherwise (if 'aTargetObj' is not
    * specified, or 'aTargetObj' is != null) looks for a property
    * 'EXPORTED_SYMBOLS' on the new global object. 'EXPORTED_SYMBOLS'
