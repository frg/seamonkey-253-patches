# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1504058645 14400
#      Tue Aug 29 22:04:05 2017 -0400
# Node ID 1e89ea2871e7f3a2d3158ea0e660199bd282efce
# Parent  a846663e27d86311f0c3b664f7856ecbd95a2f09
Bug 1394662.  Make ::-moz-table-column a non-inheriting anonymous box.  r=heycam

In our UA sheet the only style we apply to ::-moz-table-column is "display: table-column".

Per spec, the only styles that apply to table columns are border props,
background props, 'width', and 'visibility'.

The only one of those that inherits is 'visibility'.  And the only relevant
value per spec is "collapse".  But an anonymous column can only be "visibility:
collapse" in Gecko right now if its colgroup is, and then the colgroup will get
collapsed away as a whole.  So it doesn't matter whether the column inherits the
visibility style.

In practice, we do something special for "hidden" on a table-column as well,
during display list building, which affects hit-testing.  Per
https://github.com/w3c/csswg-drafts/issues/1763 it's not clear that we should be
doing this at all, but for now we just keep our existing behavior and ensure
that the parent colgroup's visibility style is taken into account for anonymous
columns when building display lists.

diff --git a/layout/style/nsCSSAnonBoxList.h b/layout/style/nsCSSAnonBoxList.h
--- a/layout/style/nsCSSAnonBoxList.h
+++ b/layout/style/nsCSSAnonBoxList.h
@@ -77,17 +77,17 @@ CSS_ANON_BOX(fieldsetContent, ":-moz-fie
 CSS_NON_INHERITING_ANON_BOX(framesetBlank, ":-moz-frameset-blank")
 CSS_ANON_BOX(mozDisplayComboboxControlFrame, ":-moz-display-comboboxcontrol-frame")
 CSS_ANON_BOX(htmlCanvasContent, ":-moz-html-canvas-content")
 
 CSS_WRAPPER_ANON_BOX(inlineTable, ":-moz-inline-table")
 CSS_WRAPPER_ANON_BOX(table, ":-moz-table")
 CSS_WRAPPER_ANON_BOX(tableCell, ":-moz-table-cell")
 CSS_WRAPPER_ANON_BOX(tableColGroup, ":-moz-table-column-group")
-CSS_ANON_BOX(tableCol, ":-moz-table-column")
+CSS_NON_INHERITING_ANON_BOX(tableCol, ":-moz-table-column")
 CSS_ANON_BOX(tableWrapper, ":-moz-table-wrapper")
 CSS_WRAPPER_ANON_BOX(tableRowGroup, ":-moz-table-row-group")
 CSS_WRAPPER_ANON_BOX(tableRow, ":-moz-table-row")
 
 CSS_ANON_BOX(canvas, ":-moz-canvas")
 CSS_NON_INHERITING_ANON_BOX(pageBreak, ":-moz-pagebreak")
 CSS_ANON_BOX(page, ":-moz-page")
 CSS_ANON_BOX(pageContent, ":-moz-pagecontent")
diff --git a/layout/tables/nsTableColGroupFrame.cpp b/layout/tables/nsTableColGroupFrame.cpp
--- a/layout/tables/nsTableColGroupFrame.cpp
+++ b/layout/tables/nsTableColGroupFrame.cpp
@@ -190,17 +190,16 @@ nsTableColGroupFrame::AppendFrames(Child
 
   // Our next colframe should be an eColContent.  We've removed all the
   // eColAnonymousColGroup colframes, eColAnonymousCol colframes always follow
   // eColContent ones, and eColAnonymousCell colframes only appear in an
   // eColGroupAnonymousCell colgroup, which never gets AppendFrames() called on
   // it.
   MOZ_ASSERT(!col || col->GetColType() == eColContent,
              "What's going on with our columns?");
-  RemoveStateBits(NS_FRAME_OWNS_ANON_BOXES);
 
   const nsFrameList::Slice& newFrames =
     mFrames.AppendFrames(this, aFrameList);
   InsertColsReflow(GetStartColumnIndex() + mColCount, newFrames);
 }
 
 void
 nsTableColGroupFrame::InsertFrames(ChildListID     aListID,
@@ -232,17 +231,16 @@ nsTableColGroupFrame::InsertFrames(Child
 
   // Our next colframe should be an eColContent.  We've removed all the
   // eColAnonymousColGroup colframes, eColAnonymousCol colframes always follow
   // eColContent ones, and eColAnonymousCell colframes only appear in an
   // eColGroupAnonymousCell colgroup, which never gets InsertFrames() called on
   // it.
   MOZ_ASSERT(!col || col->GetColType() == eColContent,
              "What's going on with our columns?");
-  RemoveStateBits(NS_FRAME_OWNS_ANON_BOXES);
 
   NS_ASSERTION(!aPrevFrame || aPrevFrame == aPrevFrame->LastContinuation(),
                "Prev frame should be last in continuation chain");
   NS_ASSERTION(!aPrevFrame || !GetNextColumn(aPrevFrame) ||
                GetNextColumn(aPrevFrame)->GetColType() != eColAnonymousCol,
                "Shouldn't be inserting before a spanned colframe");
 
   const nsFrameList::Slice& newFrames =
@@ -496,40 +494,16 @@ nsTableColGroupFrame::InvalidateFrameWit
 {
   nsIFrame::InvalidateFrameWithRect(aRect, aDisplayItemKey);
   // If we have filters applied that would affects our bounds, then
   // we get an inactive layer created and this is computed
   // within FrameLayerBuilder
   GetParent()->InvalidateFrameWithRect(aRect + GetPosition(), aDisplayItemKey);
 }
 
-void
-nsTableColGroupFrame::AppendDirectlyOwnedAnonBoxes(
-  nsTArray<OwnedAnonBox>& aResult)
-{
-  nsTableColFrame* col = GetFirstColumn();
-  if (!col) {
-    // No columns, nothing to do.
-    return;
-  }
-
-  if (col->GetColType() == eColContent) {
-    // We have actual columns; no anon boxes here.
-    return;
-  }
-
-  for ( ; col; col = col->GetNextCol()) {
-    MOZ_ASSERT(col->GetColType() != eColContent,
-               "We should not have any real columns after anonymous ones");
-    MOZ_ASSERT(col->GetColType() != eColAnonymousCol,
-               "We shouldn't have spanning anonymous columns");
-    aResult.AppendElement(OwnedAnonBox(col));
-  }
-}
-
 #ifdef DEBUG_FRAME_DUMP
 nsresult
 nsTableColGroupFrame::GetFrameName(nsAString& aResult) const
 {
   return MakeFrameName(NS_LITERAL_STRING("TableColGroup"), aResult);
 }
 
 void nsTableColGroupFrame::Dump(int32_t aIndent)
diff --git a/layout/tables/nsTableColGroupFrame.h b/layout/tables/nsTableColGroupFrame.h
--- a/layout/tables/nsTableColGroupFrame.h
+++ b/layout/tables/nsTableColGroupFrame.h
@@ -196,19 +196,16 @@ public:
   {
     return nsContainerFrame::IsFrameOfType(aFlags & ~(nsIFrame::eTablePart));
   }
 
   virtual void InvalidateFrame(uint32_t aDisplayItemKey = 0) override;
   virtual void InvalidateFrameWithRect(const nsRect& aRect, uint32_t aDisplayItemKey = 0) override;
   virtual void InvalidateFrameForRemoval() override { InvalidateFrameSubtree(); }
 
-  // Return any anonymous columns we contain.
-  void AppendDirectlyOwnedAnonBoxes(nsTArray<OwnedAnonBox>& aResult) override;
-
 protected:
   explicit nsTableColGroupFrame(nsStyleContext* aContext);
 
   void InsertColsReflow(int32_t                   aColIndex,
                         const nsFrameList::Slice& aCols);
 
   virtual LogicalSides GetLogicalSkipSides(const ReflowInput* aReflowInput = nullptr) const override;
 
diff --git a/layout/tables/nsTableFrame.cpp b/layout/tables/nsTableFrame.cpp
--- a/layout/tables/nsTableFrame.cpp
+++ b/layout/tables/nsTableFrame.cpp
@@ -746,30 +746,25 @@ nsTableFrame::AppendAnonymousColFrames(n
   nsIPresShell *shell = PresContext()->PresShell();
 
   // Get the last col frame
   nsFrameList newColFrames;
 
   int32_t startIndex = mColFrames.Length();
   int32_t lastIndex  = startIndex + aNumColsToAdd - 1;
 
-  // aColGroupFrame will need to handle restyling these cols we're about to add.
-  aColGroupFrame->AddStateBits(NS_FRAME_OWNS_ANON_BOXES);
   for (int32_t childX = startIndex; childX <= lastIndex; childX++) {
     nsIContent* iContent;
     RefPtr<nsStyleContext> styleContext;
-    nsStyleContext* parentStyleContext;
 
     // all anonymous cols that we create here use a pseudo style context of the
     // col group
     iContent = aColGroupFrame->GetContent();
-    parentStyleContext = aColGroupFrame->StyleContext();
     styleContext = shell->StyleSet()->
-      ResolveInheritingAnonymousBoxStyle(nsCSSAnonBoxes::tableCol,
-                                         parentStyleContext);
+      ResolveNonInheritingAnonymousBoxStyle(nsCSSAnonBoxes::tableCol);
     // ASSERTION to check for bug 54454 sneaking back in...
     NS_ASSERTION(iContent, "null content in CreateAnonymousColFrames");
 
     // create the new col frame
     nsIFrame* colFrame = NS_NewTableColFrame(shell, styleContext);
     ((nsTableColFrame *) colFrame)->SetColType(aColType);
     colFrame->Init(iContent, aColGroupFrame, nullptr);
 
@@ -1519,17 +1514,35 @@ void nsTableFrame::CalcHasBCBorders()
 }
 
 /* static */ void
 nsTableFrame::DisplayGenericTablePart(nsDisplayListBuilder* aBuilder,
                                       nsFrame* aFrame,
                                       const nsDisplayListSet& aLists,
                                       DisplayGenericTablePartTraversal aTraversal)
 {
-  if (aFrame->IsVisibleForPainting(aBuilder)) {
+  bool isVisible = aFrame->IsVisibleForPainting(aBuilder);
+  bool isColumn = aFrame->IsTableColFrame();
+  // If we have an anonymous column in the AppendAnonymousColFrames sense, it
+  // might think it's visible for painting (due to not inheriting its colgroup's
+  // styles) while the colgroup as a whole is actually not visible for painting
+  // because it has hidden visibility.  In that situation we should also treat
+  // the column as not visible, because otherwise hit-testing can get a bit
+  // confused.
+  //
+  // XXXbz It's not clear whether anonymous columns and column groups should
+  // ever affect hit-testing at all.  See
+  // https://github.com/w3c/csswg-drafts/issues/1763 which might result in them
+  // not existing conceptually and hence not affecting hit-testing.
+  if (isVisible && isColumn &&
+      aFrame->StyleContext()->GetPseudo() == nsCSSAnonBoxes::tableCol &&
+      !aFrame->GetParent()->StyleVisibility()->IsVisible()) {
+    isVisible = false;
+  }
+  if (isVisible) {
     nsDisplayTableItem* currentItem = aBuilder->GetCurrentTableItem();
     // currentItem may be null, when none of the table parts have a
     // background or border
     if (currentItem) {
       currentItem->UpdateForFrameBackground(aFrame);
     }
 
     // Paint the outset box-shadows for the table frames
@@ -1559,17 +1572,17 @@ nsTableFrame::DisplayGenericTablePart(ns
       table->OrderRowGroups(rowGroups);
       for (nsTableRowGroupFrame* rowGroup : rowGroups) {
         auto offset = rowGroup->GetNormalPosition() - colGroup->GetNormalPosition();
         if (!aBuilder->GetDirtyRect().Intersects(nsRect(offset, rowGroup->GetSize()))) {
           continue;
         }
         PaintRowGroupBackgroundByColIdx(rowGroup, aFrame, aBuilder, aLists, colIdx, offset);
       }
-    } else if (aFrame->IsTableColFrame()) {
+    } else if (isColumn) {
       // Compute background rect by iterating all cell frame.
       nsTableColFrame* col = static_cast<nsTableColFrame*>(aFrame);
       AutoTArray<int32_t, 1> colIdx;
       colIdx.AppendElement(col->GetColIndex());
 
       nsTableFrame* table = col->GetTableFrame();
       RowGroupArray rowGroups;
       table->OrderRowGroups(rowGroups);
@@ -1592,17 +1605,17 @@ nsTableFrame::DisplayGenericTablePart(ns
     if (hasBoxShadow) {
       aLists.BorderBackground()->AppendNewToTop(
         new (aBuilder) nsDisplayBoxShadowInner(aBuilder, aFrame));
     }
   }
 
   aTraversal(aBuilder, aFrame, aLists);
 
-  if (aFrame->IsVisibleForPainting(aBuilder)) {
+  if (isVisible) {
     if (aFrame->IsTableFrame()) {
       nsTableFrame* table = static_cast<nsTableFrame*>(aFrame);
       // In the collapsed border model, overlay all collapsed borders.
       if (table->IsBorderCollapse()) {
         if (table->HasBCBorders()) {
           aLists.BorderBackground()->AppendNewToTop(
             new (aBuilder) nsDisplayTableBorderCollapse(aBuilder, table));
         }
