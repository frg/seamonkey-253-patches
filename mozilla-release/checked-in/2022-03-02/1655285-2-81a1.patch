# HG changeset patch
# User Chris Peterson <cpeterson@mozilla.com>
# Date 1596261873 0
# Node ID b9e60a11e10ad94d414d67aa47bcad8268d2c243
# Parent  b0cd2fe9f3952b7797fe1f0797aa2c185fd3b9f8
Bug 1655285 - Part 2: Return an "Intel" User-Agent string on ARM64 macOS. r=smaug

Safari returns an "Intel" User-Agent string (and "MacIntel" navigator.platform) on ARM64 macOS (presumably for web compat) and on iPadOS (so sites serve desktop page layouts). We should follow Safari's lead for Firefox on ARM64 macOS.

Note that I do not have an Apple Silicon DTK so I have not personally tested this change on ARM64 macOS. Based on visual inspection of our User-Agent string code, I expect Firefox's current User-Agent string on ARM64 macOS is "Mozilla/5.0 (Macintosh;  Mac OS X 10.16; rv:80.0) Gecko/20100101 Firefox/80.0" (and navigator.oscpu is " Mac OS X 10.16"). Note the missing "Intel" and extra space before "Mac OS X".

Example webcompat breakage even in mozilla-central: the Octane JS benchmark assumes Firefox's macOS navigator.platform will never return any values other than "MacIntel" or "MacPPC":

https://searchfox.org/mozilla-central/rev/dcd9c2d2bc19d96d487825eb70c2333a4d60994e/js/src/octane/gbemu-part1.js#659-669

Differential Revision: https://phabricator.services.mozilla.com/D84912

diff --git a/dom/base/Navigator.cpp b/dom/base/Navigator.cpp
--- a/dom/base/Navigator.cpp
+++ b/dom/base/Navigator.cpp
@@ -1694,19 +1694,18 @@ Navigator::GetPlatform(nsAString& aPlatf
   NS_ENSURE_SUCCESS(rv, rv);
 
   // Sorry for the #if platform ugliness, but Communicator is likewise
   // hardcoded and we are seeking backward compatibility here (bug 47080).
 #if defined(_WIN64)
   aPlatform.AssignLiteral("Win64");
 #elif defined(WIN32)
   aPlatform.AssignLiteral("Win32");
-#elif defined(XP_MACOSX) && defined(__i386__)
-  aPlatform.AssignLiteral("MacIntel");
-#elif defined(XP_MACOSX) && defined(__x86_64__)
+#elif defined(XP_MACOSX)
+  // Always return "MacIntel", even on ARM64 macOS like Safari does.
   aPlatform.AssignLiteral("MacIntel");
 #else
   // XXX Communicator uses compiled-in build-time string defines
   // to indicate the platform it was compiled *for*, not what it is
   // currently running *on* which is what this does.
   nsAutoCString plat;
   rv = service->GetOscpu(plat);
   CopyASCIItoUTF16(plat, aPlatform);
diff --git a/netwerk/protocol/http/nsHttpHandler.cpp b/netwerk/protocol/http/nsHttpHandler.cpp
--- a/netwerk/protocol/http/nsHttpHandler.cpp
+++ b/netwerk/protocol/http/nsHttpHandler.cpp
@@ -1041,23 +1041,23 @@ nsHttpHandler::InitUserAgentComponents()
         SmprintfPointer buf = mozilla::Smprintf(format,
                                                 info.dwMajorVersion,
                                                 info.dwMinorVersion);
         if (buf) {
             mOscpu = buf.get();
         }
     }
 #elif defined (XP_MACOSX)
-#if defined(__i386__) || defined(__x86_64__)
-    mOscpu.AssignLiteral("Intel Mac OS X");
-#endif
-    SInt32 majorVersion = nsCocoaFeatures::macOSVersionMajor();
-    SInt32 minorVersion = nsCocoaFeatures::macOSVersionMinor();
-    mOscpu += nsPrintfCString(" %d.%d", static_cast<int>(majorVersion),
-                              static_cast<int>(minorVersion));
+  SInt32 majorVersion = nsCocoaFeatures::macOSVersionMajor();
+  SInt32 minorVersion = nsCocoaFeatures::macOSVersionMinor();
+
+  // Always return an "Intel" UA string, even on ARM64 macOS like Safari does.
+  mOscpu =
+      nsPrintfCString("Intel Mac OS X %d.%d", static_cast<int>(majorVersion),
+                      static_cast<int>(minorVersion));
 #elif defined (XP_UNIX)
     struct utsname name;
 
     int ret = uname(&name);
     if (ret >= 0) {
         nsAutoCString buf;
         buf =  (char*)name.sysname;
 
