# HG changeset patch
# User James Teh <jteh@mozilla.com>
# Date 1530508632 -36000
#      Mon Jul 02 15:17:12 2018 +1000
# Node ID 03d0bbff4c5c3987a2229a85dd917b6a0f0c7c22
# Parent  16d24d1901052433cf87ccf417ac862ef51cf553
Bug 1472137 - Prevent mutex reentry in mscom::Interceptor::Create if GetInitialInterceptorForIID fails. r=aklotz, a=RyanVM

If GetInitialInterceptorForIID fails, the live set lock is not released in most cases, but the newly created Interceptor will be destroyed.
The Interceptor's destructor tries to acquire the live set lock again, but that causes a deadlock, since reentry is no longer allowed for a mutex after bug 1364624.
GetInitialInterceptorForIID now ensures the live set lock is always released  on failure, thus preventing the deadlock.

MozReview-Commit-ID: z0Q7JLnJXQ

diff --git a/ipc/mscom/Interceptor.cpp b/ipc/mscom/Interceptor.cpp
--- a/ipc/mscom/Interceptor.cpp
+++ b/ipc/mscom/Interceptor.cpp
@@ -453,36 +453,49 @@ Interceptor::GetInitialInterceptorForIID
                                          REFIID aTargetIid,
                                          STAUniquePtr<IUnknown> aTarget,
                                          void** aOutInterceptor)
 {
   MOZ_ASSERT(aOutInterceptor);
   MOZ_ASSERT(aTargetIid != IID_IMarshal);
   MOZ_ASSERT(!IsProxy(aTarget.get()));
 
+  HRESULT hr = E_UNEXPECTED;
+
+  auto hasFailed = [&hr]() -> bool {
+    return FAILED(hr);
+  };
+
+  auto cleanup = [&aLiveSetLock]() -> void {
+    aLiveSetLock.Unlock();
+  };
+
+  ExecuteWhen<decltype(hasFailed), decltype(cleanup)>
+    onFail(hasFailed, cleanup);
+
   if (aTargetIid == IID_IUnknown) {
     // We must lock mInterceptorMapMutex so that nothing can race with us once
     // we have been published to the live set.
     MutexAutoLock lock(mInterceptorMapMutex);
 
-    HRESULT hr = PublishTarget(aLiveSetLock, nullptr, aTargetIid, Move(aTarget));
+    hr = PublishTarget(aLiveSetLock, nullptr, aTargetIid, Move(aTarget));
     if (FAILED(hr)) {
       return hr;
     }
 
     return QueryInterface(aTargetIid, aOutInterceptor);
   }
 
   // Raise the refcount for stabilization purposes during aggregation
   RefPtr<IUnknown> kungFuDeathGrip(static_cast<IUnknown*>(
         static_cast<WeakReferenceSupport*>(this)));
 
   RefPtr<IUnknown> unkInterceptor;
-  HRESULT hr = CreateInterceptor(aTargetIid, kungFuDeathGrip,
-                                 getter_AddRefs(unkInterceptor));
+  hr = CreateInterceptor(aTargetIid, kungFuDeathGrip,
+                         getter_AddRefs(unkInterceptor));
   if (FAILED(hr)) {
     return hr;
   }
 
   RefPtr<ICallInterceptor> interceptor;
   hr = unkInterceptor->QueryInterface(IID_ICallInterceptor,
                                       getter_AddRefs(interceptor));
   if (FAILED(hr)) {
diff --git a/ipc/mscom/Utils.h b/ipc/mscom/Utils.h
--- a/ipc/mscom/Utils.h
+++ b/ipc/mscom/Utils.h
@@ -6,16 +6,17 @@
 
 #ifndef mozilla_mscom_Utils_h
 #define mozilla_mscom_Utils_h
 
 #if defined(MOZILLA_INTERNAL_API)
 #include "nsString.h"
 #endif // defined(MOZILLA_INTERNAL_API)
 
+#include "mozilla/Attributes.h"
 #include <guiddef.h>
 
 struct IUnknown;
 
 namespace mozilla {
 namespace mscom {
 
 bool IsCurrentThreadMTA();
@@ -33,13 +34,47 @@ bool IsVtableIndexFromParentInterface(RE
 
 #if defined(MOZILLA_INTERNAL_API)
 bool IsInterfaceEqualToOrInheritedFrom(REFIID aInterface, REFIID aFrom,
                                        unsigned long aVtableIndexHint);
 #endif // defined(MOZILLA_INTERNAL_API)
 
 #endif // defined(ACCESSIBILITY)
 
+/**
+ * Execute cleanup code when going out of scope if a condition is met.
+ * This is useful when, for example, particular cleanup needs to be performed
+ * whenever a call returns a failure HRESULT.
+ * Both the condition and cleanup code are provided as functions (usually
+ * lambdas).
+ */
+template <typename CondFnT, typename ExeFnT>
+class MOZ_RAII ExecuteWhen final
+{
+public:
+  ExecuteWhen(CondFnT& aCondFn, ExeFnT& aExeFn)
+    : mCondFn(aCondFn)
+    , mExeFn(aExeFn)
+  {
+  }
+
+  ~ExecuteWhen()
+  {
+    if (mCondFn()) {
+      mExeFn();
+    }
+  }
+
+  ExecuteWhen(const ExecuteWhen&) = delete;
+  ExecuteWhen(ExecuteWhen&&) = delete;
+  ExecuteWhen& operator=(const ExecuteWhen&) = delete;
+  ExecuteWhen& operator=(ExecuteWhen&&) = delete;
+
+private:
+  CondFnT&  mCondFn;
+  ExeFnT&   mExeFn;
+};
+
 } // namespace mscom
 } // namespace mozilla
 
 #endif // mozilla_mscom_Utils_h
 
