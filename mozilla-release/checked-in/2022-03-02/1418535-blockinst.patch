# HG changeset patch
# User Aaron Klotz <aklotz@mozilla.com>
# Date 1511212515 25200
#      Mon Nov 20 14:15:15 2017 -0700
# Node ID 1516807b05d3f6084b58d54b12032ccce5cbdcfc
# Parent  b6917c2d3e4a2c0aff058a070a955da806be2de7
Bug 1418535: Block a11y instntiation if no known ATs are present and known bad DLLs are; r=jimm

MozReview-Commit-ID: FtoEamY9P8r

diff --git a/accessible/windows/msaa/Compatibility.cpp b/accessible/windows/msaa/Compatibility.cpp
--- a/accessible/windows/msaa/Compatibility.cpp
+++ b/accessible/windows/msaa/Compatibility.cpp
@@ -127,21 +127,22 @@ DetectInSendMessageExCompat(PEXCEPTION_P
       sVectoredExceptionHandler = nullptr;
     }
   }
   return EXCEPTION_CONTINUE_SEARCH;
 }
 
 uint32_t Compatibility::sConsumers = Compatibility::UNKNOWN;
 
-void
-Compatibility::Init()
+/**
+ * This function is safe to call multiple times.
+ */
+/* static */ void
+Compatibility::InitConsumers()
 {
-  // Note we collect some AT statistics/telemetry here for convenience.
-
   HMODULE jawsHandle = ::GetModuleHandleW(L"jhook");
   if (jawsHandle)
     sConsumers |= (IsModuleVersionLessThan(jawsHandle, 18, 4315)) ?
                    OLDJAWS : JAWS;
 
   if (::GetModuleHandleW(L"gwm32inc"))
     sConsumers |= WE;
 
@@ -168,17 +169,31 @@ Compatibility::Init()
     sConsumers |= YOUDAO;
 
   if (::GetModuleHandleW(L"uiautomation") ||
       ::GetModuleHandleW(L"uiautomationcore"))
     sConsumers |= UIAUTOMATION;
 
   // If we have a known consumer remove the unknown bit.
   if (sConsumers != Compatibility::UNKNOWN)
-    sConsumers ^= Compatibility::UNKNOWN;
+    sConsumers &= ~Compatibility::UNKNOWN;
+}
+
+/* static */ bool
+Compatibility::HasKnownNonUiaConsumer()
+{
+  InitConsumers();
+  return sConsumers & ~(Compatibility::UNKNOWN | UIAUTOMATION);
+}
+
+void
+Compatibility::Init()
+{
+  // Note we collect some AT statistics/telemetry here for convenience.
+  InitConsumers();
 
 #ifdef MOZ_CRASHREPORTER
   CrashReporter::
     AnnotateCrashReport(NS_LITERAL_CSTRING("AccessibilityInProcClient"),
                         nsPrintfCString("0x%X", sConsumers));
 #endif
 
   // Gather telemetry
diff --git a/accessible/windows/msaa/Compatibility.h b/accessible/windows/msaa/Compatibility.h
--- a/accessible/windows/msaa/Compatibility.h
+++ b/accessible/windows/msaa/Compatibility.h
@@ -40,16 +40,21 @@ public:
   static bool IsDolphin() { return !!(sConsumers & DOLPHIN); }
 
   /**
    * @return ID of a11y manifest resource to be passed to
    * mscom::ActivationContext
    */
   static uint16_t GetActCtxResourceId();
 
+  /**
+   * return true if a known, non-UIA a11y consumer is present
+   */
+  static bool HasKnownNonUiaConsumer();
+
 private:
   Compatibility();
   Compatibility(const Compatibility&);
   Compatibility& operator = (const Compatibility&);
 
   /**
    * Initialize compatibility mode. Called by platform (see Platform.h) during
    * accessibility initialization.
@@ -72,14 +77,15 @@ private:
     KAZAGURU = 1 << 8,
     YOUDAO = 1 << 9,
     UNKNOWN = 1 << 10,
     UIAUTOMATION = 1 << 11
   };
 
 private:
   static uint32_t sConsumers;
+  static void InitConsumers();
 };
 
 } // a11y namespace
 } // mozilla namespace
 
 #endif
diff --git a/accessible/windows/msaa/LazyInstantiator.cpp b/accessible/windows/msaa/LazyInstantiator.cpp
--- a/accessible/windows/msaa/LazyInstantiator.cpp
+++ b/accessible/windows/msaa/LazyInstantiator.cpp
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "LazyInstantiator.h"
 
 #include "MainThreadUtils.h"
 #include "mozilla/a11y/Accessible.h"
+#include "mozilla/a11y/Compatibility.h"
 #include "mozilla/Assertions.h"
 #include "mozilla/mscom/MainThreadRuntime.h"
 #include "mozilla/mscom/Registration.h"
 #include "mozilla/UniquePtr.h"
 #include "nsAccessibilityService.h"
 #include "nsWindowsHelpers.h"
 #include "nsCOMPtr.h"
 #include "nsIFile.h"
@@ -225,42 +226,76 @@ LazyInstantiator::GetClientExecutableNam
     return false;
   }
 
   file.forget(aOutClientExe);
   return NS_SUCCEEDED(rv);
 }
 
 /**
+ * This is the blocklist for known "bad" DLLs that instantiate a11y.
+ */
+static const wchar_t* gBlockedInprocDlls[] = {
+  L"dtvhooks.dll",  // RealPlayer, bug 1418535
+  L"dtvhooks64.dll" // RealPlayer, bug 1418535
+};
+
+/**
+ * Check for the presence of any known "bad" injected DLLs that may be trying
+ * to instantiate a11y.
+ *
+ * @return true to block a11y instantiation, otherwise false to continue
+ */
+bool
+LazyInstantiator::IsBlockedInjection()
+{
+  if (Compatibility::HasKnownNonUiaConsumer()) {
+    // If we already see a known AT, don't block a11y instantiation
+    return false;
+  }
+
+  for (size_t index = 0, len = ArrayLength(gBlockedInprocDlls); index < len;
+       ++index) {
+    if (::GetModuleHandleW(gBlockedInprocDlls[index])) {
+      return true;
+    }
+  }
+
+  return false;
+}
+
+/**
  * Given a remote client's thread ID, determine whether we should proceed with
  * a11y instantiation. This is where telemetry should be gathered and any
  * potential blocking of unwanted a11y clients should occur.
  *
  * @return true if we should instantiate a11y
  */
 bool
 LazyInstantiator::ShouldInstantiate(const DWORD aClientTid)
 {
   if (!aClientTid) {
     // aClientTid == 0 implies that this is either an in-process call, or else
     // we failed to retrieve information about the remote caller.
-    // We should always default to instantiating a11y in this case.
-    return true;
+    // We should always default to instantiating a11y in this case, provided
+    // that we don't see any known bad injected DLLs.
+    return !IsBlockedInjection();
   }
 
   nsCOMPtr<nsIFile> clientExe;
   if (!GetClientExecutableName(aClientTid, getter_AddRefs(clientExe))) {
 #if defined(MOZ_TELEMETRY_REPORTING)
     AccumulateTelemetry(NS_LITERAL_STRING("(Failed to retrieve client image name)"));
 #endif // defined(MOZ_TELEMETRY_REPORTING)
     // We should return true as a failsafe
     return true;
   }
 
-  // Blocklist checks should go here. return false if we should not instantiate.
+  // Blocklist checks for external clients should go here.
+  // return false if we should not instantiate.
   /*
   if (ClientShouldBeBlocked(clientExe)) {
     return false;
   }
   */
 
 #if defined(MOZ_TELEMETRY_REPORTING) || defined(MOZ_CRASHREPORTER)
   if (!mTelemetryThread) {
diff --git a/accessible/windows/msaa/LazyInstantiator.h b/accessible/windows/msaa/LazyInstantiator.h
--- a/accessible/windows/msaa/LazyInstantiator.h
+++ b/accessible/windows/msaa/LazyInstantiator.h
@@ -78,16 +78,17 @@ public:
 
   // IServiceProvider
   STDMETHODIMP QueryService(REFGUID aServiceId, REFIID aServiceIid, void** aOutInterface) override;
 
 private:
   explicit LazyInstantiator(HWND aHwnd);
   ~LazyInstantiator();
 
+  bool IsBlockedInjection();
   bool ShouldInstantiate(const DWORD aClientTid);
 
   bool GetClientExecutableName(const DWORD aClientTid, nsIFile** aOutClientExe);
 #if defined(MOZ_TELEMETRY_REPORTING) || defined(MOZ_CRASHREPORTER)
   class AccumulateRunnable final : public Runnable
   {
   public:
     explicit AccumulateRunnable(LazyInstantiator* aObj)
