# HG changeset patch
# User Stone Shih <sshih@mozilla.com>
# Date 1496816896 -28800
#      Wed Jun 07 14:28:16 2017 +0800
# Node ID f355b970d3a14aa6592d3d6cb1fababc911c374b
# Parent  53bf33f99df8eb8ccc597286f18d79351b5c21c7
Bug 1351148 Part4: Revise those test cases that have some tasks have to be processed before or after the synthesized key events. r=smaug.

Make sure input events are processed before or after the dependent tasks.

MozReview-Commit-ID: 8KfZnT2wjJR

diff --git a/devtools/client/inspector/test/browser_inspector_highlighter-eyedropper-events.js b/devtools/client/inspector/test/browser_inspector_highlighter-eyedropper-events.js
--- a/devtools/client/inspector/test/browser_inspector_highlighter-eyedropper-events.js
+++ b/devtools/client/inspector/test/browser_inspector_highlighter-eyedropper-events.js
@@ -108,34 +108,34 @@ function* respondsToMoveEvents(helper, t
     } else {
       info(`Simulating ${type} event: ${desc}`);
     }
 
     if (type === "mouse") {
       yield mouse.move(x, y);
     } else if (type === "keyboard") {
       let options = shift ? {shiftKey: true} : {};
-      yield EventUtils.synthesizeKey(key, options);
+      yield EventUtils.synthesizeAndWaitKey(key, options);
     }
     yield checkPosition(expected, helper);
   }
 }
 
 function* checkPosition({x, y}, {getElementAttribute}) {
   let style = yield getElementAttribute("root", "style");
   is(style, `top:${y}px;left:${x}px;`,
      `The eyedropper is at the expected ${x} ${y} position`);
 }
 
 function* respondsToReturnAndEscape({isElementHidden, show}) {
   info("Simulating return to select the color and hide the eyedropper");
 
-  yield EventUtils.synthesizeKey("VK_RETURN", {});
+  yield EventUtils.synthesizeAndWaitKey("VK_RETURN", {});
   let hidden = yield isElementHidden("root");
   ok(hidden, "The eyedropper has been hidden");
 
   info("Showing the eyedropper again and simulating escape to hide it");
 
   yield show("html");
-  yield EventUtils.synthesizeKey("VK_ESCAPE", {});
+  yield EventUtils.synthesizeAndWaitKey("VK_ESCAPE", {});
   hidden = yield isElementHidden("root");
   ok(hidden, "The eyedropper has been hidden again");
 }
diff --git a/dom/tests/browser/browser_bug1316330.js b/dom/tests/browser/browser_bug1316330.js
--- a/dom/tests/browser/browser_bug1316330.js
+++ b/dom/tests/browser/browser_bug1316330.js
@@ -13,24 +13,24 @@ const URL =
   "  if (e.charCode == 'p'.charCodeAt(0)) while (Date.now() - startTime < 500) {}" +
   "};" +
   "</script>";
 
 add_task(async function() {
   let tab = await BrowserTestUtils.openNewForegroundTab(gBrowser, URL);
   let browser = tab.linkedBrowser;
 
-  EventUtils.synthesizeKey("d", { code: "KeyD", repeat: 3 });
+  await EventUtils.synthesizeAndWaitKey("d", { code: "KeyD", repeat: 3 });
 
   await ContentTask.spawn(browser, null, async function() {
     is(content.document.body.getAttribute("data-down"), "2", "Correct number of events");
     is(content.document.body.getAttribute("data-press"), "2", "Correct number of events");
   });
 
-  EventUtils.synthesizeKey("p", { code: "KeyP", repeat: 3 });
+  await EventUtils.synthesizeAndWaitKey("p", { code: "KeyP", repeat: 3 });
 
   await ContentTask.spawn(browser, null, async function() {
     is(content.document.body.getAttribute("data-down"), "4", "Correct number of events");
     is(content.document.body.getAttribute("data-press"), "4", "Correct number of events");
   });
 
   gBrowser.removeCurrentTab();
 });
diff --git a/editor/libeditor/tests/browser_bug629172.js b/editor/libeditor/tests/browser_bug629172.js
--- a/editor/libeditor/tests/browser_bug629172.js
+++ b/editor/libeditor/tests/browser_bug629172.js
@@ -38,21 +38,20 @@ add_task(async function() {
       };
     });
 
     function simulateCtrlShiftX(aBrowser) {
       // In e10s, the keypress event will be dispatched to the content process,
       // but in non-e10s it is handled by the browser UI code and hence won't
       // reach the web page.  As a result, we need to observe the event in
       // the content process only in e10s mode.
-      var waitForKeypressContent = BrowserTestUtils.waitForContentEvent(aBrowser, "keypress");
+      if (gMultiProcessBrowser) {
+        return EventUtils.synthesizeAndWaitKey("x", {accelKey: true, shiftKey: true});
+      }
       EventUtils.synthesizeKey("x", {accelKey: true, shiftKey: true});
-      if (gMultiProcessBrowser) {
-        return waitForKeypressContent;
-      }
       return Promise.resolve();
     }
 
     async function testDirection(initialDir, aBrowser) {
       await ContentTask.spawn(aBrowser, {initialDir}, function({initialDir}) {
         var window = content.window.wrappedJSObject;
         var document = window.document;
 
diff --git a/testing/mochitest/tests/SimpleTest/EventUtils.js b/testing/mochitest/tests/SimpleTest/EventUtils.js
--- a/testing/mochitest/tests/SimpleTest/EventUtils.js
+++ b/testing/mochitest/tests/SimpleTest/EventUtils.js
@@ -683,16 +683,54 @@ function synthesizeNativeMouseMove(aTarg
       if (aCallback && topic == "mouseevent") {
         aCallback(data);
       }
     }
   };
   utils.sendNativeMouseMove(x * scale, y * scale, null, observer);
 }
 
+/**
+ * This is a wrapper around synthesizeNativeMouseMove that waits for the mouse
+ * event to be dispatched to the target content.
+ *
+ * This API is supposed to be used in those test cases that synthesize some
+ * input events to chrome process and have some checks in content.
+ */
+function synthesizeAndWaitNativeMouseMove(aTarget, aOffsetX, aOffsetY,
+                                          aCallback, aWindow = window) {
+  let browser = gBrowser.selectedTab.linkedBrowser;
+  let mm = browser.messageManager;
+  let ContentTask =
+    _EU_Cu.import("resource://testing-common/ContentTask.jsm", null).ContentTask;
+
+  let eventRegisteredPromise = new Promise(resolve => {
+    mm.addMessageListener("Test:MouseMoveRegistered", function processed(message) {
+      mm.removeMessageListener("Test:MouseMoveRegistered", processed);
+      resolve();
+    });
+  });
+  let eventReceivedPromise = ContentTask.spawn(browser, [aOffsetX, aOffsetY],
+                                               ([clientX, clientY]) => {
+    return new Promise(resolve => {
+      addEventListener("mousemove", function onMouseMoveEvent(e) {
+        if (e.clientX == clientX && e.clientY == clientY) {
+          removeEventListener("mousemove", onMouseMoveEvent);
+          resolve();
+        }
+      });
+      sendAsyncMessage("Test:MouseMoveRegistered");
+    });
+  });
+  eventRegisteredPromise.then(() => {
+    synthesizeNativeMouseMove(aTarget, aOffsetX, aOffsetY, null, aWindow);
+  });
+  return eventReceivedPromise;
+}
+
 function _computeKeyCodeFromChar(aChar)
 {
   if (aChar.length != 1) {
     return 0;
   }
   var KeyEvent = _EU_Ci.nsIDOMKeyEvent;
   if (aChar >= 'a' && aChar <= 'z') {
     return KeyEvent.DOM_VK_A + aChar.charCodeAt(0) - 'a'.charCodeAt(0);
@@ -834,16 +872,61 @@ function synthesizeKey(aKey, aEvent, aWi
     if (dispatchKeyup) {
       TIP.keyup(keyEvent, keyEventDict.flags);
     }
   } finally {
     _emulateToInactivateModifiers(TIP, modifiers, aWindow);
   }
 }
 
+/**
+ * This is a wrapper around synthesizeKey that waits for the key event to be
+ * dispatched to the target content. It returns a promise which is resolved
+ * when the content receives the key event.
+ *
+ * This API is supposed to be used in those test cases that synthesize some
+ * input events to chrome process and have some checks in content.
+ */
+function synthesizeAndWaitKey(aKey, aEvent, aWindow = window,
+                              checkBeforeSynthesize, checkAfterSynthesize)
+{
+  let browser = gBrowser.selectedTab.linkedBrowser;
+  let mm = browser.messageManager;
+  let keyCode = _createKeyboardEventDictionary(aKey, aEvent, aWindow).dictionary.keyCode;
+  let ContentTask = _EU_Cu.import("resource://testing-common/ContentTask.jsm", null).ContentTask;
+
+  let keyRegisteredPromise = new Promise(resolve => {
+    mm.addMessageListener("Test:KeyRegistered", function processed(message) {
+      mm.removeMessageListener("Test:KeyRegistered", processed);
+      resolve();
+    });
+  });
+  let keyReceivedPromise = ContentTask.spawn(browser, keyCode, (keyCode) => {
+    return new Promise(resolve => {
+      addEventListener("keyup", function onKeyEvent(e) {
+        if (e.keyCode == keyCode) {
+          removeEventListener("keyup", onKeyEvent);
+          resolve();
+        }
+      });
+      sendAsyncMessage("Test:KeyRegistered");
+    });
+  });
+  keyRegisteredPromise.then(() => {
+    if (checkBeforeSynthesize) {
+      checkBeforeSynthesize();
+    }
+    synthesizeKey(aKey, aEvent, aWindow);
+    if (checkAfterSynthesize) {
+      checkAfterSynthesize();
+    }
+  });
+  return keyReceivedPromise;
+}
+
 function _parseNativeModifiers(aModifiers, aWindow = window)
 {
   var navigator = _getNavigator(aWindow);
   var modifiers;
   if (aModifiers.capsLockKey) {
     modifiers |= 0x00000001;
   }
   if (aModifiers.numLockKey) {
diff --git a/toolkit/components/tooltiptext/tests/browser_input_file_tooltips.js b/toolkit/components/tooltiptext/tests/browser_input_file_tooltips.js
--- a/toolkit/components/tooltiptext/tests/browser_input_file_tooltips.js
+++ b/toolkit/components/tooltiptext/tests/browser_input_file_tooltips.js
@@ -29,19 +29,17 @@ add_task(async function test_requiredset
 });
 
 async function do_test(test) {
   info(`starting test ${JSON.stringify(test)}`);
 
   let tab = await BrowserTestUtils.openNewForegroundTab(gBrowser);
 
   info("Moving mouse out of the way.");
-  await new Promise(resolve => {
-    EventUtils.synthesizeNativeMouseMove(tab.linkedBrowser, 300, 300, resolve);
-  });
+  await EventUtils.synthesizeAndWaitNativeMouseMove(tab.linkedBrowser, 300, 300);
 
   info("creating input field");
   await ContentTask.spawn(tab.linkedBrowser, test, async function(test) {
     let doc = content.document;
     let input = doc.createElement("input");
     doc.body.appendChild(input);
     input.id = "test_input";
     input.setAttribute("style", "position: absolute; top: 0; left: 0;");
@@ -87,25 +85,21 @@ async function do_test(test) {
                       "remoteBrowserTooltip" :
                       "aHTMLTooltip";
     let tooltip = document.getElementById(tooltipId);
     tooltip.addEventListener("popupshown", function(event) {
       resolve(event.target);
     }, {once: true});
   });
   info("Initial mouse move");
-  await new Promise(resolve => {
-    EventUtils.synthesizeNativeMouseMove(tab.linkedBrowser, 50, 5, resolve);
-  });
+  await EventUtils.synthesizeAndWaitNativeMouseMove(tab.linkedBrowser, 50, 5);
   info("Waiting");
   await new Promise(resolve => setTimeout(resolve, 400));
   info("Second mouse move");
-  await new Promise(resolve => {
-    EventUtils.synthesizeNativeMouseMove(tab.linkedBrowser, 70, 5, resolve);
-  });
+  await EventUtils.synthesizeAndWaitNativeMouseMove(tab.linkedBrowser, 70, 5);
   info("Waiting for tooltip to open");
   let tooltip = await awaitTooltipOpen;
 
   is(tooltip.getAttribute("label"), test.result, "tooltip label should match expectation");
 
   info("Closing tab");
   await BrowserTestUtils.removeTab(tab);
 }
diff --git a/toolkit/content/tests/browser/browser_findbar.js b/toolkit/content/tests/browser/browser_findbar.js
--- a/toolkit/content/tests/browser/browser_findbar.js
+++ b/toolkit/content/tests/browser/browser_findbar.js
@@ -170,26 +170,28 @@ add_task(async function() {
   let tab = await BrowserTestUtils.openNewForegroundTab(gBrowser, TEST_PAGE_URI);
   let browser = tab.linkedBrowser;
 
   ok(!gFindBarInitialized, "findbar isn't initialized yet");
 
   let findBar = gFindBar;
   let initialValue = findBar._findField.value;
 
-  EventUtils.synthesizeKey("f", { accelKey: true }, window);
+  await EventUtils.synthesizeAndWaitKey("f", { accelKey: true }, window, null,
+                                        () => {
+    isnot(document.activeElement, findBar._findField.inputField,
+      "findbar is not yet focused");
+  });
 
   let promises = [
     BrowserTestUtils.sendChar("a", browser),
     BrowserTestUtils.sendChar("b", browser),
     BrowserTestUtils.sendChar("c", browser)
   ];
 
-  isnot(document.activeElement, findBar._findField.inputField,
-    "findbar is not yet focused");
   is(findBar._findField.value, initialValue, "still has initial find query");
 
   await Promise.all(promises);
   is(document.activeElement, findBar._findField.inputField,
     "findbar is now focused");
   is(findBar._findField.value, "abc", "abc fully entered as find query");
 
   await BrowserTestUtils.removeTab(tab);
diff --git a/widget/tests/test_assign_event_data.html b/widget/tests/test_assign_event_data.html
--- a/widget/tests/test_assign_event_data.html
+++ b/widget/tests/test_assign_event_data.html
@@ -95,16 +95,26 @@ function onEvent(aEvent)
         attr != "multipleActionsPrevented" && // multipleActionsPrevented isn't defined in any DOM event specs.
         typeof(aEvent[attr]) != "function") {
       gCopiedEvent.push({ name: attr, value: aEvent[attr]});
     }
   }
   setTimeout(gCallback, 0);
 }
 
+function observeKeyUpOnContent(aKeyCode, aCallback)
+{
+  document.addEventListener("keyup", function keyUp(ev) {
+    if (ev.keyCode == aKeyCode) {
+      document.removeEventListener("keyup", keyUp);
+      SimpleTest.executeSoon(aCallback);
+    }
+  });
+}
+
 const kTests = [
   { description: "InternalScrollPortEvent (overflow, vertical)",
     targetID: "scrollable-div", eventType: "overflow",
     dispatchEvent: function () {
       document.getElementById("scrolled-div").style.height = "500px";
     },
     canRun: function () {
       return true;
@@ -145,55 +155,69 @@ const kTests = [
   },
   { description: "WidgetKeyboardEvent (keydown of 'a' key without modifiers)",
     targetID: "input-text", eventType: "keydown",
     dispatchEvent: function () {
       document.getElementById(this.targetID).value = "";
       document.getElementById(this.targetID).focus();
       synthesizeNativeKey(KEYBOARD_LAYOUT_EN_US, kIsWin ? WIN_VK_A : MAC_VK_ANSI_A,
                           {}, "a", "a");
+      observeKeyUpOnContent(KeyboardEvent.DOM_VK_A, runNextTest);
+      return true;
     },
     canRun: function () {
       return (kIsMac || kIsWin);
     },
     todoMismatch: [],
   },
   { description: "WidgetKeyboardEvent (keyup of 'a' key without modifiers)",
     targetID: "input-text", eventType: "keydown",
     dispatchEvent: function () {
       document.getElementById(this.targetID).value = "";
       document.getElementById(this.targetID).focus();
       synthesizeNativeKey(KEYBOARD_LAYOUT_EN_US, kIsWin ? WIN_VK_A : MAC_VK_ANSI_A,
                           {}, "a", "a");
+      observeKeyUpOnContent(KeyboardEvent.DOM_VK_A, runNextTest);
+      return true;
     },
     canRun: function () {
       return (kIsMac || kIsWin);
     },
     todoMismatch: [],
   },
   { description: "WidgetKeyboardEvent (keypress of 'b' key with Shift)",
     targetID: "input-text", eventType: "keypress",
     dispatchEvent: function () {
       document.getElementById(this.targetID).value = "";
       document.getElementById(this.targetID).focus();
       synthesizeNativeKey(KEYBOARD_LAYOUT_EN_US, kIsWin ? WIN_VK_B : MAC_VK_ANSI_B,
                           { shiftKey: true }, "B", "B");
+      observeKeyUpOnContent(KeyboardEvent.DOM_VK_B, runNextTest);
+      return true;
     },
     canRun: function () {
       return (kIsMac || kIsWin);
     },
     todoMismatch: [],
   },
   { description: "WidgetKeyboardEvent (keypress of 'c' key with Accel)",
     targetID: "input-text", eventType: "keypress",
     dispatchEvent: function () {
       document.getElementById(this.targetID).value = "";
       document.getElementById(this.targetID).focus();
       synthesizeNativeKey(KEYBOARD_LAYOUT_EN_US, kIsWin ? WIN_VK_C : MAC_VK_ANSI_C,
                           { accelKey: true }, kIsWin ? "\u0003" : "c", "c");
+
+      // On Windows, synthesizeNativeKey will also fire keyup for accelKey
+      // (control key on Windows). We have to wait for it to prevent the key
+      // event break the next test case.
+      let waitKeyCode = _EU_isWin(window) ? KeyboardEvent.DOM_VK_CONTROL :
+                                            KeyboardEvent.DOM_VK_C;
+      observeKeyUpOnContent(waitKeyCode, runNextTest);
+      return true;
     },
     canRun: function () {
       return (kIsMac || kIsWin);
     },
     todoMismatch: [],
   },
   { description: "WidgetKeyboardEvent (keyup during composition)",
     targetID: "input-text", eventType: "keyup",
@@ -320,16 +344,18 @@ const kTests = [
   },
   { description: "InternalEditorInputEvent (input at key input)",
     targetID: "input-text", eventType: "input",
     dispatchEvent: function () {
       document.getElementById(this.targetID).value = "";
       document.getElementById(this.targetID).focus();
       synthesizeNativeKey(KEYBOARD_LAYOUT_EN_US, kIsWin ? WIN_VK_B : MAC_VK_ANSI_B,
                           { shiftKey: true }, "B", "B");
+      observeKeyUpOnContent(KeyboardEvent.DOM_VK_B, runNextTest);
+      return true;
     },
     canRun: function () {
       return (kIsMac || kIsWin);
     },
     todoMismatch: [],
   },
   { description: "InternalEditorInputEvent (input at composing)",
     targetID: "input-text", eventType: "input",
