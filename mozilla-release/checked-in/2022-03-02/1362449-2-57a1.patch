# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1504731517 14400
# Node ID 014a075a5a0d08447f521b841533bb6b2fc9195d
# Parent  36050261b425023269851bfe7d2e2fdb548e54dd
Bug 1362449 - part 2 - use our Base64 encode implementation rather than NSPR's; r=erahm

One less use of NSPR is a good thing.  The failure cases that
PL_Base64Encode would have caught for us are:

1. "Truncation".
2. Integer overflow when computing destination string length.
3. Malloc failures.

The first one only gets checked if we pass in zero for the source
length, which we never do.  The latter two only get checked if we pass
in a null pointer for the destination, which we never do.  So removing
the error handling PL_Base64Encode implies here is a good thing.

diff --git a/xpcom/io/Base64.cpp b/xpcom/io/Base64.cpp
--- a/xpcom/io/Base64.cpp
+++ b/xpcom/io/Base64.cpp
@@ -299,73 +299,60 @@ Base64EncodeInputStream(nsIInputStream* 
 nsresult
 Base64Encode(const char* aBinary, uint32_t aBinaryLen, char** aBase64)
 {
   // Check for overflow.
   if (aBinaryLen > (UINT32_MAX / 4) * 3) {
     return NS_ERROR_FAILURE;
   }
 
-  // Don't ask PR_Base64Encode to encode empty strings.
   if (aBinaryLen == 0) {
     *aBase64 = (char*)moz_xmalloc(1);
     (*aBase64)[0] = '\0';
     return NS_OK;
   }
 
   *aBase64 = nullptr;
   uint32_t base64Len = ((aBinaryLen + 2) / 3) * 4;
 
   // Add one byte for null termination.
   UniqueFreePtr<char[]> base64((char*)malloc(base64Len + 1));
   if (!base64) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
 
-  if (!PL_Base64Encode(aBinary, aBinaryLen, base64.get())) {
-    return NS_ERROR_INVALID_ARG;
-  }
-
-  // PL_Base64Encode doesn't null terminate the buffer for us when we pass
-  // the buffer in. Do that manually.
+  Encode(aBinary, aBinaryLen, base64.get());
   base64[base64Len] = '\0';
 
   *aBase64 = base64.release();
   return NS_OK;
 }
 
 nsresult
 Base64Encode(const nsACString& aBinary, nsACString& aBase64)
 {
   // Check for overflow.
   if (aBinary.Length() > (UINT32_MAX / 4) * 3) {
     return NS_ERROR_FAILURE;
   }
 
-  // Don't ask PR_Base64Encode to encode empty strings.
   if (aBinary.IsEmpty()) {
     aBase64.Truncate();
     return NS_OK;
   }
 
   uint32_t base64Len = ((aBinary.Length() + 2) / 3) * 4;
 
   // Add one byte for null termination.
   if (!aBase64.SetCapacity(base64Len + 1, fallible)) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
 
   char* base64 = aBase64.BeginWriting();
-  if (!PL_Base64Encode(aBinary.BeginReading(), aBinary.Length(), base64)) {
-    aBase64.Truncate();
-    return NS_ERROR_INVALID_ARG;
-  }
-
-  // PL_Base64Encode doesn't null terminate the buffer for us when we pass
-  // the buffer in. Do that manually.
+  Encode(aBinary.BeginReading(), aBinary.Length(), base64);
   base64[base64Len] = '\0';
 
   aBase64.SetLength(base64Len);
   return NS_OK;
 }
 
 nsresult
 Base64Encode(const nsAString& aBinary, nsAString& aBase64)
