# HG changeset patch
# User Edgar Chen <echen@mozilla.com>
# Date 1533626665 -7200
#      Tue Aug 07 09:24:25 2018 +0200
# Node ID a62385e8d213ffa4cf5cbbe820c8ebebbeaa0e23
# Parent  6372bd2c4ef671d8273894a9f3412a2a66a00c9a
Bug 1450989 - Capture the action and target as part of the form submission creation. r=bz, a=RyanVM

diff --git a/dom/base/FormData.cpp b/dom/base/FormData.cpp
--- a/dom/base/FormData.cpp
+++ b/dom/base/FormData.cpp
@@ -13,17 +13,17 @@
 #include "mozilla/Encoding.h"
 
 #include "MultipartBlobImpl.h"
 
 using namespace mozilla;
 using namespace mozilla::dom;
 
 FormData::FormData(nsISupports* aOwner)
-  : HTMLFormSubmission(UTF_8_ENCODING, nullptr)
+  : HTMLFormSubmission(nullptr, EmptyString(), UTF_8_ENCODING, nullptr)
   , mOwner(aOwner)
 {
 }
 
 namespace {
 
 already_AddRefed<File>
 GetOrCreateFileCalledBlob(Blob& aBlob, ErrorResult& aRv)
@@ -397,17 +397,17 @@ FormData::Constructor(const GlobalObject
 
 // -------------------------------------------------------------------------
 // nsIXHRSendable
 
 NS_IMETHODIMP
 FormData::GetSendInfo(nsIInputStream** aBody, uint64_t* aContentLength,
                       nsACString& aContentTypeWithCharset, nsACString& aCharset)
 {
-  FSMultipartFormData fs(UTF_8_ENCODING, nullptr);
+  FSMultipartFormData fs(nullptr, EmptyString(), UTF_8_ENCODING, nullptr);
 
   for (uint32_t i = 0; i < mFormData.Length(); ++i) {
     if (mFormData[i].wasNullBlob) {
       MOZ_ASSERT(mFormData[i].value.IsUSVString());
       fs.AddNameBlobOrNullPair(mFormData[i].name, nullptr);
     } else if (mFormData[i].value.IsUSVString()) {
       fs.AddNameValuePair(mFormData[i].name,
                           mFormData[i].value.GetAsUSVString());
diff --git a/dom/html/HTMLFormElement.cpp b/dom/html/HTMLFormElement.cpp
--- a/dom/html/HTMLFormElement.cpp
+++ b/dom/html/HTMLFormElement.cpp
@@ -192,23 +192,16 @@ HTMLFormElement::GetElements(nsIDOMHTMLC
 nsresult
 HTMLFormElement::BeforeSetAttr(int32_t aNamespaceID, nsIAtom* aName,
                                const nsAttrValueOrString* aValue, bool aNotify)
 {
   if (aNamespaceID == kNameSpaceID_None) {
     if (aName == nsGkAtoms::action || aName == nsGkAtoms::target) {
       // This check is mostly to preserve previous behavior.
       if (aValue) {
-        if (mPendingSubmission) {
-          // aha, there is a pending submission that means we're in
-          // the script and we need to flush it. let's tell it
-          // that the event was ignored to force the flush.
-          // the second argument is not playing a role at all.
-          FlushPendingSubmission();
-        }
         // Don't forget we've notified the password manager already if the
         // page sets the action/target in the during submit. (bug 343182)
         bool notifiedObservers = mNotifiedObservers;
         ForgetCurrentSubmission();
         mNotifiedObservers = notifiedObservers;
       }
     }
   }
@@ -704,25 +697,17 @@ HTMLFormElement::BuildSubmission(HTMLFor
 
   return NS_OK;
 }
 
 nsresult
 HTMLFormElement::SubmitSubmission(HTMLFormSubmission* aFormSubmission)
 {
   nsresult rv;
-  nsIContent* originatingElement = aFormSubmission->GetOriginatingElement();
-
-  //
-  // Get the action and target
-  //
-  nsCOMPtr<nsIURI> actionURI;
-  rv = GetActionURL(getter_AddRefs(actionURI), originatingElement);
-  NS_ENSURE_SUBMIT_SUCCESS(rv);
-
+  nsCOMPtr<nsIURI> actionURI = aFormSubmission->GetActionURL();
   if (!actionURI) {
     mIsSubmitting = false;
     return NS_OK;
   }
 
   // If there is no link handler, then we won't actually be able to submit.
   nsIDocument* doc = GetComposedDoc();
   nsCOMPtr<nsISupports> container = doc ? doc->GetContainer() : nullptr;
@@ -744,31 +729,16 @@ HTMLFormElement::SubmitSubmission(HTMLFo
   // we're not submitting when submitting to a JS URL.  That's kinda bogus, but
   // there we are.
   bool schemeIsJavaScript = false;
   if (NS_SUCCEEDED(actionURI->SchemeIs("javascript", &schemeIsJavaScript)) &&
       schemeIsJavaScript) {
     mIsSubmitting = false;
   }
 
-  // The target is the originating element formtarget attribute if the element
-  // is a submit control and has such an attribute.
-  // Otherwise, the target is the form owner's target attribute,
-  // if it has such an attribute.
-  // Finally, if one of the child nodes of the head element is a base element
-  // with a target attribute, then the value of the target attribute of the
-  // first such base element; or, if there is no such element, the empty string.
-  nsAutoString target;
-  if (!(originatingElement && originatingElement->GetAttr(kNameSpaceID_None,
-                                                          nsGkAtoms::formtarget,
-                                                          target)) &&
-      !GetAttr(kNameSpaceID_None, nsGkAtoms::target, target)) {
-    GetBaseTarget(target);
-  }
-
   //
   // Notify observers of submit
   //
   bool cancelSubmit = false;
   if (mNotifiedObservers) {
     cancelSubmit = mNotifiedObserversResult;
   } else {
     rv = NotifySubmitObservers(actionURI, &cancelSubmit, true);
@@ -801,16 +771,18 @@ HTMLFormElement::SubmitSubmission(HTMLFo
                                        mSubmitInitiatedFromUserInput,
                                        nullptr, doc);
 
     nsCOMPtr<nsIInputStream> postDataStream;
     rv = aFormSubmission->GetEncodedSubmission(actionURI,
                                                getter_AddRefs(postDataStream));
     NS_ENSURE_SUBMIT_SUCCESS(rv);
 
+    nsAutoString target;
+    aFormSubmission->GetTarget(target);
     rv = linkHandler->OnLinkClickSync(this, actionURI,
                                       target.get(),
                                       NullString(),
                                       postDataStream, nullptr, false,
                                       getter_AddRefs(docShell),
                                       getter_AddRefs(mSubmittingRequest));
     NS_ENSURE_SUBMIT_SUCCESS(rv);
   }
diff --git a/dom/html/HTMLFormElement.h b/dom/html/HTMLFormElement.h
--- a/dom/html/HTMLFormElement.h
+++ b/dom/html/HTMLFormElement.h
@@ -500,24 +500,16 @@ protected:
 
   /**
    * Find form controls in this form with the correct value in the name
    * attribute.
    */
   already_AddRefed<nsISupports> DoResolveName(const nsAString& aName, bool aFlushContent);
 
   /**
-   * Get the full URL to submit to.  Do not submit if the returned URL is null.
-   *
-   * @param aActionURL the full, unadulterated URL you'll be submitting to [OUT]
-   * @param aOriginatingElement the originating element of the form submission [IN]
-   */
-  nsresult GetActionURL(nsIURI** aActionURL, nsIContent* aOriginatingElement);
-
-  /**
    * Check the form validity following this algorithm:
    * http://www.whatwg.org/specs/web-apps/current-work/#statically-validate-the-constraints
    *
    * @param aInvalidElements [out] parameter containing the list of unhandled
    * invalid controls.
    *
    * @return Whether the form is currently valid.
    */
@@ -546,16 +538,24 @@ protected:
 public:
   /**
    * Flush a possible pending submission. If there was a scripted submission
    * triggered by a button or image, the submission was defered. This method
    * forces the pending submission to be submitted. (happens when the handler
    * returns false or there is an action/target change in the script)
    */
   void FlushPendingSubmission();
+
+  /**
+   * Get the full URL to submit to.  Do not submit if the returned URL is null.
+   *
+   * @param aActionURL the full, unadulterated URL you'll be submitting to [OUT]
+   * @param aOriginatingElement the originating element of the form submission [IN]
+   */
+  nsresult GetActionURL(nsIURI** aActionURL, nsIContent* aOriginatingElement);
 protected:
 
   //
   // Data members
   //
   /** The list of controls (form.elements as well as stuff not in elements) */
   RefPtr<HTMLFormControlsCollection> mControls;
   /** The currently selected radio button of each group */
diff --git a/dom/html/HTMLFormSubmission.cpp b/dom/html/HTMLFormSubmission.cpp
--- a/dom/html/HTMLFormSubmission.cpp
+++ b/dom/html/HTMLFormSubmission.cpp
@@ -87,21 +87,23 @@ RetrieveDirectoryName(Directory* aDirect
 class FSURLEncoded : public EncodingFormSubmission
 {
 public:
   /**
    * @param aEncoding the character encoding of the form
    * @param aMethod the method of the submit (either NS_FORM_METHOD_GET or
    *        NS_FORM_METHOD_POST).
    */
-  FSURLEncoded(NotNull<const Encoding*> aEncoding,
+  FSURLEncoded(nsIURI* aActionURL,
+               const nsAString& aTarget,
+               NotNull<const Encoding*> aEncoding,
                int32_t aMethod,
                nsIDocument* aDocument,
                nsIContent* aOriginatingElement)
-    : EncodingFormSubmission(aEncoding, aOriginatingElement)
+    : EncodingFormSubmission(aActionURL, aTarget, aEncoding, aOriginatingElement)
     , mMethod(aMethod)
     , mDocument(aDocument)
     , mWarnedFileControl(false)
   {
   }
 
   virtual nsresult
   AddNameValuePair(const nsAString& aName, const nsAString& aValue) override;
@@ -387,19 +389,21 @@ FSURLEncoded::URLEncode(const nsAString&
 
   return NS_OK;
 }
 
 } // anonymous namespace
 
 // --------------------------------------------------------------------------
 
-FSMultipartFormData::FSMultipartFormData(NotNull<const Encoding*> aEncoding,
+FSMultipartFormData::FSMultipartFormData(nsIURI* aActionURL,
+                                         const nsAString& aTarget,
+                                         NotNull<const Encoding*> aEncoding,
                                          nsIContent* aOriginatingElement)
-  : EncodingFormSubmission(aEncoding, aOriginatingElement)
+  : EncodingFormSubmission(aActionURL, aTarget, aEncoding, aOriginatingElement)
 {
   mPostDataStream =
     do_CreateInstance("@mozilla.org/io/multiplex-input-stream;1");
   mTotalLength = 0;
 
   mBoundary.AssignLiteral("---------------------------");
   mBoundary.AppendInt(rand());
   mBoundary.AppendInt(rand());
@@ -654,19 +658,21 @@ FSMultipartFormData::AddPostDataStream()
 
 // --------------------------------------------------------------------------
 
 namespace {
 
 class FSTextPlain : public EncodingFormSubmission
 {
 public:
-  FSTextPlain(NotNull<const Encoding*> aEncoding,
+  FSTextPlain(nsIURI* aActionURL,
+              const nsAString& aTarget,
+              NotNull<const Encoding*> aEncoding,
               nsIContent* aOriginatingElement)
-    : EncodingFormSubmission(aEncoding, aOriginatingElement)
+    : EncodingFormSubmission(aActionURL, aTarget, aEncoding, aOriginatingElement)
   {
   }
 
   virtual nsresult
   AddNameValuePair(const nsAString& aName, const nsAString& aValue) override;
 
   virtual nsresult
   AddNameBlobOrNullPair(const nsAString& aName, Blob* aBlob) override;
@@ -774,19 +780,21 @@ FSTextPlain::GetEncodedSubmission(nsIURI
   return rv;
 }
 
 } // anonymous namespace
 
 // --------------------------------------------------------------------------
 
 EncodingFormSubmission::EncodingFormSubmission(
+  nsIURI* aActionURL,
+  const nsAString& aTarget,
   NotNull<const Encoding*> aEncoding,
   nsIContent* aOriginatingElement)
-  : HTMLFormSubmission(aEncoding, aOriginatingElement)
+  : HTMLFormSubmission(aActionURL, aTarget, aEncoding, aOriginatingElement)
 {
   if (!aEncoding->CanEncodeEverything()) {
     nsAutoCString name;
     aEncoding->Name(name);
     NS_ConvertUTF8toUTF16 nameUtf16(name);
     const char16_t* namePtr = nameUtf16.get();
     SendJSWarning(aOriginatingElement ? aOriginatingElement->GetOwnerDocument()
                                       : nullptr,
@@ -873,24 +881,47 @@ GetEnumAttr(nsGenericHTMLElement* aConte
   if (value && value->Type() == nsAttrValue::eEnum) {
     *aValue = value->GetEnumValue();
   }
 }
 
 } // anonymous namespace
 
 /* static */ nsresult
-HTMLFormSubmission::GetFromForm(nsGenericHTMLElement* aForm,
+HTMLFormSubmission::GetFromForm(HTMLFormElement* aForm,
                                 nsGenericHTMLElement* aOriginatingElement,
                                 HTMLFormSubmission** aFormSubmission)
 {
   // Get all the information necessary to encode the form data
   NS_ASSERTION(aForm->GetComposedDoc(),
                "Should have doc if we're building submission!");
 
+  nsresult rv;
+
+  // Get action
+  nsCOMPtr<nsIURI> actionURL;
+  rv = aForm->GetActionURL(getter_AddRefs(actionURL), aOriginatingElement);
+  NS_ENSURE_SUCCESS(rv, rv);
+
+  // Get target
+  // The target is the originating element formtarget attribute if the element
+  // is a submit control and has such an attribute.
+  // Otherwise, the target is the form owner's target attribute,
+  // if it has such an attribute.
+  // Finally, if one of the child nodes of the head element is a base element
+  // with a target attribute, then the value of the target attribute of the
+  // first such base element; or, if there is no such element, the empty string.
+  nsAutoString target;
+  if (!(aOriginatingElement && aOriginatingElement->GetAttr(kNameSpaceID_None,
+                                                            nsGkAtoms::formtarget,
+                                                            target)) &&
+      !aForm->GetAttr(kNameSpaceID_None, nsGkAtoms::target, target)) {
+    aForm->GetBaseTarget(target);
+  }
+
   // Get encoding type (default: urlencoded)
   int32_t enctype = NS_FORM_ENCTYPE_URLENCODED;
   if (aOriginatingElement &&
       aOriginatingElement->HasAttr(kNameSpaceID_None, nsGkAtoms::formenctype)) {
     GetEnumAttr(aOriginatingElement, nsGkAtoms::formenctype, &enctype);
   } else {
     GetEnumAttr(aForm, nsGkAtoms::enctype, &enctype);
   }
@@ -905,20 +936,20 @@ HTMLFormSubmission::GetFromForm(nsGeneri
   }
 
   // Get encoding
   auto encoding = GetSubmitEncoding(aForm)->OutputEncoding();
 
   // Choose encoder
   if (method == NS_FORM_METHOD_POST &&
       enctype == NS_FORM_ENCTYPE_MULTIPART) {
-    *aFormSubmission = new FSMultipartFormData(encoding, aOriginatingElement);
+    *aFormSubmission = new FSMultipartFormData(actionURL, target, encoding, aOriginatingElement);
   } else if (method == NS_FORM_METHOD_POST &&
              enctype == NS_FORM_ENCTYPE_TEXTPLAIN) {
-    *aFormSubmission = new FSTextPlain(encoding, aOriginatingElement);
+    *aFormSubmission = new FSTextPlain(actionURL, target, encoding, aOriginatingElement);
   } else {
     nsIDocument* doc = aForm->OwnerDoc();
     if (enctype == NS_FORM_ENCTYPE_MULTIPART ||
         enctype == NS_FORM_ENCTYPE_TEXTPLAIN) {
       nsAutoString enctypeStr;
       if (aOriginatingElement &&
           aOriginatingElement->HasAttr(kNameSpaceID_None,
                                        nsGkAtoms::formenctype)) {
@@ -927,16 +958,16 @@ HTMLFormSubmission::GetFromForm(nsGeneri
       } else {
         aForm->GetAttr(kNameSpaceID_None, nsGkAtoms::enctype, enctypeStr);
       }
       const char16_t* enctypeStrPtr = enctypeStr.get();
       SendJSWarning(doc, "ForgotPostWarning",
                     &enctypeStrPtr, 1);
     }
     *aFormSubmission =
-      new FSURLEncoded(encoding, method, doc, aOriginatingElement);
+      new FSURLEncoded(actionURL, target, encoding, method, doc, aOriginatingElement);
   }
 
   return NS_OK;
 }
 
 } // dom namespace
 } // mozilla namespace
diff --git a/dom/html/HTMLFormSubmission.h b/dom/html/HTMLFormSubmission.h
--- a/dom/html/HTMLFormSubmission.h
+++ b/dom/html/HTMLFormSubmission.h
@@ -18,16 +18,17 @@ class nsIInputStream;
 class nsGenericHTMLElement;
 class nsIMultiplexInputStream;
 
 namespace mozilla {
 namespace dom {
 
 class Blob;
 class Directory;
+class HTMLFormElement;
 
 /**
  * Class for form submissions; encompasses the function to call to submit as
  * well as the form submission name/value pairs
  */
 class HTMLFormSubmission
 {
 public:
@@ -35,17 +36,17 @@ public:
    * Get a submission object based on attributes in the form (ENCTYPE and
    * METHOD)
    *
    * @param aForm the form to get a submission object based on
    * @param aOriginatingElement the originating element (can be null)
    * @param aFormSubmission the form submission object (out param)
    */
   static nsresult
-  GetFromForm(nsGenericHTMLElement* aForm,
+  GetFromForm(HTMLFormElement* aForm,
               nsGenericHTMLElement* aOriginatingElement,
               HTMLFormSubmission** aFormSubmission);
 
   virtual ~HTMLFormSubmission()
   {
     MOZ_COUNT_DTOR(HTMLFormSubmission);
   }
 
@@ -93,42 +94,70 @@ public:
    */
   void GetCharset(nsACString& aCharset) { mEncoding->Name(aCharset); }
 
   nsIContent* GetOriginatingElement() const
   {
     return mOriginatingElement.get();
   }
 
+  /**
+   * Get the action URI that will be used for submission.
+   */
+  nsIURI* GetActionURL() const
+  {
+    return mActionURL;
+  }
+
+  /**
+   * Get the target that will be used for submission.
+   */
+  void GetTarget(nsAString& aTarget)
+  {
+    aTarget = mTarget;
+  }
+
 protected:
   /**
    * Can only be constructed by subclasses.
    *
    * @param aEncoding the character encoding of the form
    * @param aOriginatingElement the originating element (can be null)
    */
-  HTMLFormSubmission(mozilla::NotNull<const mozilla::Encoding*> aEncoding,
+  HTMLFormSubmission(nsIURI* aActionURL,
+                     const nsAString& aTarget,
+                     mozilla::NotNull<const mozilla::Encoding*> aEncoding,
                      nsIContent* aOriginatingElement)
-    : mEncoding(aEncoding)
+    : mActionURL(aActionURL)
+    , mTarget(aTarget)
+    , mEncoding(aEncoding)
     , mOriginatingElement(aOriginatingElement)
   {
     MOZ_COUNT_CTOR(HTMLFormSubmission);
   }
 
+  // The action url.
+  nsCOMPtr<nsIURI> mActionURL;
+
+  // The target.
+  nsString mTarget;
+
   // The character encoding of this form submission
   mozilla::NotNull<const mozilla::Encoding*> mEncoding;
 
   // Originating element.
   nsCOMPtr<nsIContent> mOriginatingElement;
 };
 
 class EncodingFormSubmission : public HTMLFormSubmission
 {
 public:
-  EncodingFormSubmission(mozilla::NotNull<const mozilla::Encoding*> aEncoding,
+  EncodingFormSubmission(nsIURI* aActionURL,
+                         const nsAString& aTarget,
+                         mozilla::NotNull<const mozilla::Encoding*> aEncoding,
                          nsIContent* aOriginatingElement);
 
   virtual ~EncodingFormSubmission();
 
   /**
    * Encode a Unicode string to bytes using the encoder (or just copy the input
    * if there is no encoder).
    * @param aStr the string to encode
@@ -146,17 +175,19 @@ public:
  * inputs.  This always does POST.
  */
 class FSMultipartFormData : public EncodingFormSubmission
 {
 public:
   /**
    * @param aEncoding the character encoding of the form
    */
-  FSMultipartFormData(mozilla::NotNull<const mozilla::Encoding*> aEncoding,
+  FSMultipartFormData(nsIURI* aActionURL,
+                      const nsAString& aTarget,
+                      mozilla::NotNull<const mozilla::Encoding*> aEncoding,
                       nsIContent* aOriginatingElement);
   ~FSMultipartFormData();
 
   virtual nsresult
   AddNameValuePair(const nsAString& aName, const nsAString& aValue) override;
 
   virtual nsresult
   AddNameBlobOrNullPair(const nsAString& aName, Blob* aBlob) override;
