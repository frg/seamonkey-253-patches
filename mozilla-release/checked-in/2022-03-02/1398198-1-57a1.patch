# HG changeset patch
# User Florian Queze <florian@queze.net>
# Date 1505330393 -7200
#      Wed Sep 13 21:19:53 2017 +0200
# Node ID 7030bd97757ee347c92c971e5865d81e05e6d009
# Parent  1c6777988a04ae93d821719be95ab87becd772fe
Bug 1398198 - browser_startup.js should show the stack when a JS file was loaded earlier than expected, r=felipe,mccr8.

diff --git a/browser/base/content/test/performance/browser.ini b/browser/base/content/test/performance/browser.ini
--- a/browser/base/content/test/performance/browser.ini
+++ b/browser/base/content/test/performance/browser.ini
@@ -1,9 +1,11 @@
 [DEFAULT]
+prefs =
+  browser.startup.record=true
 support-files =
   head.js
 [browser_appmenu_reflows.js]
 skip-if = asan || debug # Bug 1382809, bug 1369959
 [browser_startup.js]
 [browser_startup_content.js]
 skip-if = !e10s
 [browser_startup_images.js]
diff --git a/browser/base/content/test/performance/browser_startup.js b/browser/base/content/test/performance/browser_startup.js
--- a/browser/base/content/test/performance/browser_startup.js
+++ b/browser/base/content/test/performance/browser_startup.js
@@ -11,16 +11,19 @@
  * If your code isn't strictly required to show the first browser window,
  * it shouldn't be loaded before we are done with first paint.
  * Finally, if your code isn't really needed during startup, it should not be
  * loaded before we have started handling user events.
  */
 
 "use strict";
 
+/* Set this to true only for debugging purpose; it makes the output noisy. */
+const kDumpAllStacks = false;
+
 const startupPhases = {
   // For app-startup, we have a whitelist of acceptable JS files.
   // Anything loaded during app-startup must have a compelling reason
   // to run before we have even selected the user profile.
   // Consider loading your code after first paint instead,
   // eg. from nsBrowserGlue.js' _onFirstWindowLoaded method).
   "before profile selection": {whitelist: {
     components: new Set([
@@ -143,37 +146,52 @@ add_task(async function() {
     ok(!("@mozilla.org/test/startuprecorder;1" in Cc),
        "the startup recorder component shouldn't exist in this non-nightly non-debug build.");
     return;
   }
 
   let startupRecorder = Cc["@mozilla.org/test/startuprecorder;1"].getService().wrappedJSObject;
   await startupRecorder.done;
 
+  let loader = Cc["@mozilla.org/moz/jsloader;1"].getService(Ci.xpcIJSModuleLoader);
+  let componentStacks = new Map();
   let data = startupRecorder.data.code;
   // Keep only the file name for components, as the path is an absolute file
   // URL rather than a resource:// URL like for modules.
   for (let phase in data) {
     data[phase].components =
-      data[phase].components.map(f => f.replace(/.*\//, ""))
-                            .filter(c => c != "startupRecorder.js");
+      data[phase].components.map(uri => {
+        let fileName = uri.replace(/.*\//, "");
+        componentStacks.set(fileName, loader.getComponentLoadStack(uri));
+        return fileName;
+      }).filter(c => c != "startupRecorder.js");
+  }
+
+  function printStack(scriptType, name) {
+    if (scriptType == "modules")
+      info(loader.getModuleImportStack(name));
+    else if (scriptType == "components")
+      info(componentStacks.get(name));
   }
 
   // This block only adds debug output to help find the next bugs to file,
   // it doesn't contribute to the actual test.
   SimpleTest.requestCompleteLog();
   let previous;
   for (let phase in data) {
     for (let scriptType in data[phase]) {
       for (let f of data[phase][scriptType]) {
         // phases are ordered, so if a script wasn't loaded yet at the immediate
         // previous phase, it wasn't loaded during any of the previous phases
         // either, and is new in the current phase.
-        if (!previous || !data[previous][scriptType].includes(f))
+        if (!previous || !data[previous][scriptType].includes(f)) {
           info(`${scriptType} loaded ${phase}: ${f}`);
+          if (kDumpAllStacks)
+            printStack(scriptType, f);
+        }
       }
     }
     previous = phase;
   }
 
   for (let phase in startupPhases) {
     let loadedList = data[phase];
     let whitelist = startupPhases[phase].whitelist || null;
@@ -184,26 +202,30 @@ add_task(async function() {
             return true;
           whitelist[scriptType].delete(c);
           return false;
         });
         is(loadedList[scriptType].length, 0,
            `should have no unexpected ${scriptType} loaded ${phase}`);
         for (let script of loadedList[scriptType]) {
           ok(false, `unexpected ${scriptType}: ${script}`);
+          printStack(scriptType, script);
         }
         is(whitelist[scriptType].size, 0,
            `all ${scriptType} whitelist entries should have been used`);
         for (let script of whitelist[scriptType]) {
           ok(false, `unused ${scriptType} whitelist entry: ${script}`);
         }
       }
     }
     let blacklist = startupPhases[phase].blacklist || null;
     if (blacklist) {
       for (let scriptType in blacklist) {
         for (let file of blacklist[scriptType]) {
-          ok(!loadedList[scriptType].includes(file), `${file} is not allowed ${phase}`);
+          let loaded = loadedList[scriptType].includes(file);
+          ok(!loaded, `${file} is not allowed ${phase}`);
+          if (loaded)
+            printStack(scriptType, file);
         }
       }
     }
   }
 });
diff --git a/browser/components/tests/startupRecorder.js b/browser/components/tests/startupRecorder.js
--- a/browser/components/tests/startupRecorder.js
+++ b/browser/components/tests/startupRecorder.js
@@ -36,16 +36,19 @@ function startupRecorder() {
   this.done = new Promise(resolve => { this._resolve = resolve; });
 }
 startupRecorder.prototype = {
   classID: Components.ID("{11c095b2-e42e-4bdf-9dd0-aed87595f6a4}"),
 
   QueryInterface: XPCOMUtils.generateQI([Ci.nsIObserver]),
 
   record(name) {
+    if (!Services.prefs.getBoolPref("browser.startup.record", false))
+      return;
+
     this.data.code[name] = {
       components: this.loader.loadedComponents(),
       modules: this.loader.loadedModules(),
       services: Object.keys(Cc).filter(c => {
         try {
           return Cm.isServiceInstantiatedByContractID(c, Ci.nsISupports);
         } catch (e) {
           return false;
@@ -76,16 +79,22 @@ startupRecorder.prototype = {
     if (topic == "image-drawing" || topic == "image-loading") {
       this.data.images[topic].add(data);
       return;
     }
 
     Services.obs.removeObserver(this, topic);
 
     if (topic == "sessionstore-windows-restored") {
+      if (!Services.prefs.getBoolPref("browser.startup.record", false)) {
+        this._resolve();
+        this._resolve = null;
+        return;
+      }
+
       // We use idleDispatchToMainThread here to record the set of
       // loaded scripts after we are fully done with startup and ready
       // to react to user events.
       Services.tm.dispatchToMainThread(
         this.record.bind(this, "before handling user events"));
 
       // 10 is an arbitrary value here, it needs to be at least 2 to avoid
       // races with code initializing itself using idle callbacks.
diff --git a/js/xpconnect/idl/xpcIJSModuleLoader.idl b/js/xpconnect/idl/xpcIJSModuleLoader.idl
--- a/js/xpconnect/idl/xpcIJSModuleLoader.idl
+++ b/js/xpconnect/idl/xpcIJSModuleLoader.idl
@@ -3,16 +3,22 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsISupports.idl"
 
 [scriptable, builtinclass, uuid(4f94b21f-2920-4bd9-8251-5fb60fb054b2)]
 interface xpcIJSModuleLoader : nsISupports
 {
-  // These 2 functions are for startup testing purposes. They are not expected
+  // These functions are for startup testing purposes. They are not expected
   // to be used for production code.
   void loadedModules([optional] out unsigned long length,
                      [retval, array, size_is(length)] out string aModules);
 
   void loadedComponents([optional] out unsigned long length,
                         [retval, array, size_is(length)] out string aComponents);
+
+  // These 2 functions will only return useful values if the
+  // "browser.startup.record" preference was true at the time the JS file
+  // was loaded.
+  ACString getModuleImportStack(in AUTF8String aLocation);
+  ACString getComponentLoadStack(in AUTF8String aLocation);
 };
diff --git a/js/xpconnect/loader/mozJSComponentLoader.cpp b/js/xpconnect/loader/mozJSComponentLoader.cpp
--- a/js/xpconnect/loader/mozJSComponentLoader.cpp
+++ b/js/xpconnect/loader/mozJSComponentLoader.cpp
@@ -429,16 +429,22 @@ mozJSComponentLoader::LoadModule(FileLoc
     if (NS_FAILED(rv)) {
         /* XXX report error properly */
 #ifdef DEBUG
         fprintf(stderr, "mJCL: couldn't get nsIModule from jsval\n");
 #endif
         return nullptr;
     }
 
+#if defined(NIGHTLY_BUILD) || defined(DEBUG)
+    if (Preferences::GetBool("browser.startup.record", false)) {
+        entry->importStack = xpc_PrintJSStack(cx, false, false, false).get();
+    }
+#endif
+
     // Cache this module for later
     mModules.Put(spec, entry);
 
     // The hash owns the ModuleEntry now, forget about it
     return entry.forget();
 }
 
 void
@@ -1000,16 +1006,62 @@ NS_IMETHODIMP mozJSComponentLoader::Load
     for (auto iter = mModules.Iter(); !iter.Done(); iter.Next()) {
         *comp = NS_strdup(iter.Data()->location);
         comp++;
     }
 
     return NS_OK;
 }
 
+NS_IMETHODIMP
+mozJSComponentLoader::GetModuleImportStack(const nsACString& aLocation,
+                                           nsACString& retval)
+{
+#if defined(NIGHTLY_BUILD) || defined(DEBUG)
+    MOZ_ASSERT(nsContentUtils::IsCallerChrome());
+    MOZ_ASSERT(mInitialized);
+
+    ComponentLoaderInfo info(aLocation);
+    nsresult rv = info.EnsureKey();
+    NS_ENSURE_SUCCESS(rv, rv);
+
+    ModuleEntry* mod;
+    if (!mImports.Get(info.Key(), &mod))
+        return NS_ERROR_FAILURE;
+
+    retval = mod->importStack;
+    return NS_OK;
+#else
+    return NS_ERROR_NOT_IMPLEMENTED;
+#endif
+}
+
+NS_IMETHODIMP
+mozJSComponentLoader::GetComponentLoadStack(const nsACString& aLocation,
+                                            nsACString& retval)
+{
+#if defined(NIGHTLY_BUILD) || defined(DEBUG)
+    MOZ_ASSERT(nsContentUtils::IsCallerChrome());
+    MOZ_ASSERT(mInitialized);
+
+    ComponentLoaderInfo info(aLocation);
+    nsresult rv = info.EnsureURI();
+    NS_ENSURE_SUCCESS(rv, rv);
+
+    ModuleEntry* mod;
+    if (!mModules.Get(info.Key(), &mod))
+        return NS_ERROR_FAILURE;
+
+    retval = mod->importStack;
+    return NS_OK;
+#else
+    return NS_ERROR_NOT_IMPLEMENTED;
+#endif
+}
+
 static JSObject*
 ResolveModuleObjectPropertyById(JSContext* aCx, HandleObject aModObj, HandleId id)
 {
     if (JS_HasExtensibleLexicalEnvironment(aModObj)) {
         RootedObject lexical(aCx, JS_ExtensibleLexicalEnvironment(aModObj));
         bool found;
         if (!JS_HasOwnPropertyById(aCx, lexical, id, &found)) {
             return nullptr;
@@ -1242,16 +1294,23 @@ mozJSComponentLoader::Import(JSContext* 
                 JS_SetPendingException(aCx, exception);
                 return NS_ERROR_FAILURE;
             }
 
             // Something failed, but we don't know what it is, guess.
             return NS_ERROR_FILE_NOT_FOUND;
         }
 
+#if defined(NIGHTLY_BUILD) || defined(DEBUG)
+        if (Preferences::GetBool("browser.startup.record", false)) {
+            newEntry->importStack =
+                xpc_PrintJSStack(callercx, false, false, false).get();
+        }
+#endif
+
         mod = newEntry;
     }
 
     MOZ_ASSERT(mod->obj, "Import table contains entry with no object");
     aModuleGlobal.set(mod->obj);
 
     JS::RootedObject exports(aCx, mod->exports);
     if (!exports && !aIgnoreExports) {
diff --git a/js/xpconnect/loader/mozJSComponentLoader.h b/js/xpconnect/loader/mozJSComponentLoader.h
--- a/js/xpconnect/loader/mozJSComponentLoader.h
+++ b/js/xpconnect/loader/mozJSComponentLoader.h
@@ -165,29 +165,35 @@ class mozJSComponentLoader final : publi
             }
 
             if (location)
                 free(location);
 
             obj = nullptr;
             thisObjectKey = nullptr;
             location = nullptr;
+#if defined(NIGHTLY_BUILD) || defined(DEBUG)
+            importStack.Truncate();
+#endif
         }
 
         size_t SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) const;
 
         static already_AddRefed<nsIFactory> GetFactory(const mozilla::Module& module,
                                                        const mozilla::Module::CIDEntry& entry);
 
         nsCOMPtr<xpcIJSGetFactory> getfactoryobj;
         JS::PersistentRootedObject obj;
         JS::PersistentRootedObject exports;
         JS::PersistentRootedScript thisObjectKey;
         char* location;
         nsCString resolvedURL;
+#if defined(NIGHTLY_BUILD) || defined(DEBUG)
+        nsCString importStack;
+#endif
     };
 
     nsresult ExtractExports(JSContext* aCx, ComponentLoaderInfo& aInfo,
                             ModuleEntry* aMod,
                             JS::MutableHandleObject aExports);
 
     static size_t DataEntrySizeOfExcludingThis(const nsACString& aKey, ModuleEntry* const& aData,
                                                mozilla::MallocSizeOf aMallocSizeOf, void* arg);
