# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1505558987 -3600
# Node ID fbc5b903dbdd95722caa919703e1cb911ea99512
# Parent  ff561b4af9b8d904d9b4f316655d49e16801f578
Bug 1399540 - patch 1 - Failure to decode an individual label within the IDN should not block decoding of other valid punycode labels. r=valentin

diff --git a/netwerk/dns/nsIDNService.cpp b/netwerk/dns/nsIDNService.cpp
--- a/netwerk/dns/nsIDNService.cpp
+++ b/netwerk/dns/nsIDNService.cpp
@@ -279,48 +279,55 @@ NS_IMETHODIMP nsIDNService::ConvertACEto
 }
 
 nsresult nsIDNService::ACEtoUTF8(const nsACString & input, nsACString & _retval,
                                  stringPrepFlag flag)
 {
   // RFC 3490 - 4.2 ToUnicode
   // ToUnicode never fails.  If any step fails, then the original input
   // sequence is returned immediately in that step.
+  //
+  // Note that this refers to the decoding of a single label.
+  // ACEtoUTF8 may be called with a sequence of labels separated by dots;
+  // this test applies individually to each label.
 
   uint32_t len = 0, offset = 0;
   nsAutoCString decodedBuf;
 
   nsACString::const_iterator start, end;
   input.BeginReading(start);
   input.EndReading(end);
   _retval.Truncate();
 
   // loop and decode nodes
   while (start != end) {
     len++;
     if (*start++ == '.') {
-      if (NS_FAILED(decodeACE(Substring(input, offset, len - 1), decodedBuf,
-                              flag))) {
-        _retval.Assign(input);
-        return NS_OK;
+      nsDependentCSubstring origLabel(input, offset, len - 1);
+      if (NS_FAILED(decodeACE(origLabel, decodedBuf, flag))) {
+        // If decoding failed, use the original input sequence
+        // for this label.
+        _retval.Append(origLabel);
+      } else {
+        _retval.Append(decodedBuf);
       }
 
-      _retval.Append(decodedBuf);
       _retval.Append('.');
       offset += len;
       len = 0;
     }
   }
   // decode the last node
   if (len) {
-    if (NS_FAILED(decodeACE(Substring(input, offset, len), decodedBuf,
-                            flag)))
-      _retval.Assign(input);
-    else
+    nsDependentCSubstring origLabel(input, offset, len);
+    if (NS_FAILED(decodeACE(origLabel, decodedBuf, flag))) {
+      _retval.Append(origLabel);
+    } else {
       _retval.Append(decodedBuf);
+    }
   }
 
   return NS_OK;
 }
 
 NS_IMETHODIMP nsIDNService::IsACE(const nsACString & input, bool *_retval)
 {
   const char *data = input.BeginReading();
