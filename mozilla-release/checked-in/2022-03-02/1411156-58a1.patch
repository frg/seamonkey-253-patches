# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1508821657 -32400
# Node ID 396203b98e350e0e52111423a8e04080c0ea60ef
# Parent  8f430277e6616c54376f6995ac561b4199269638
Bug 1411156 - Set MOZ_DEBUG globally as both a CONFIG and a #define. r=mshal

Bug 1365460 introduced code paths behind MOZ_DEBUG #ifdefs, but
MOZ_DEBUG is never defined, while it is available in CONFIG in
moz.builds. This is kind of a confusing situation, but the fact that
we've been able to avoid those problems for so long would tend to
put the blame on mozjemalloc, and fixes should go there.

Except that bug 1261161 explains that the only existing alternative
(the DEBUG #define), as used in MFBT, is not working for spidermonkey,
so it actually makes sense to converge to MOZ_DEBUG rather than DEBUG.

So start defining MOZ_DEBUG globally, fixing the mozjemalloc issues of
not having the debug code enabled. Bug 1261161 can then take care of
changing the DEBUG #ifdefs.

diff --git a/js/src/old-configure.in b/js/src/old-configure.in
--- a/js/src/old-configure.in
+++ b/js/src/old-configure.in
@@ -1872,17 +1872,16 @@ AC_SUBST(AS)
 AC_SUBST(ASFLAGS)
 AC_SUBST(AS_DASH_C_FLAG)
 AC_SUBST(RC)
 AC_SUBST(RCFLAGS)
 AC_SUBST(WINDRES)
 AC_SUBST(IMPLIB)
 AC_SUBST(FILTER)
 AC_SUBST(BIN_FLAGS)
-AC_SUBST(MOZ_DEBUG)
 AC_SUBST(MOZ_DEBUG_LDFLAGS)
 AC_SUBST(WARNINGS_AS_ERRORS)
 AC_SUBST(LIBICONV)
 
 AC_SUBST(ENABLE_STRIP)
 AC_SUBST(PKG_SKIP_STRIP)
 AC_SUBST(INCREMENTAL_LINKER)
 
diff --git a/moz.configure b/moz.configure
--- a/moz.configure
+++ b/moz.configure
@@ -107,18 +107,24 @@ set_define('GTEST_USE_OWN_TR1_TUPLE',
 set_define('GTEST_HAS_CLONE',
            linux_gtest_defines.has_clone)
 
 js_option('--enable-debug',
           nargs='?',
           help='Enable building with developer debug info '
                '(using the given compiler flags).')
 
-add_old_configure_assignment('MOZ_DEBUG',
-                             depends('--enable-debug')(lambda v: bool(v)))
+@depends('--enable-debug')
+def moz_debug(debug):
+    if debug:
+        return bool(debug)
+
+set_config('MOZ_DEBUG', moz_debug)
+set_define('MOZ_DEBUG', moz_debug)
+add_old_configure_assignment('MOZ_DEBUG', moz_debug)
 
 js_option('--enable-rust-debug',
           help='Build Rust code with debug assertions turned on.')
 
 @depends('--enable-rust-debug', '--enable-debug')
 def debug_rust(value, debug):
     if value.origin == 'default':
         return bool(debug) or None
diff --git a/old-configure.in b/old-configure.in
--- a/old-configure.in
+++ b/old-configure.in
@@ -4069,17 +4069,16 @@ AC_SUBST(AS_DASH_C_FLAG)
 AC_SUBST(RC)
 AC_SUBST(RCFLAGS)
 AC_SUBST(WINDRES)
 AC_SUBST(IMPLIB)
 AC_SUBST(FILTER)
 AC_SUBST(BIN_FLAGS)
 AC_SUBST(MOZ_AUTH_EXTENSION)
 AC_SUBST(MOZ_PREF_EXTENSIONS)
-AC_SUBST(MOZ_DEBUG)
 AC_SUBST(MOZ_DEBUG_LDFLAGS)
 AC_SUBST(WARNINGS_AS_ERRORS)
 AC_SUBST_SET(MOZ_EXTENSIONS)
 AC_SUBST(MOZ_TOOLKIT_SEARCH)
 AC_SUBST(MOZ_FEEDS)
 
 AC_SUBST(MOZ_UNIVERSALCHARDET)
 AC_SUBST(ACCESSIBILITY)
diff --git a/python/mozbuild/mozbuild/action/generate_symbols_file.py b/python/mozbuild/mozbuild/action/generate_symbols_file.py
--- a/python/mozbuild/mozbuild/action/generate_symbols_file.py
+++ b/python/mozbuild/mozbuild/action/generate_symbols_file.py
@@ -24,17 +24,17 @@ def generate_symbols_file(output, *args)
     pp = Preprocessor()
     pp.context.update(buildconfig.defines['ALLDEFINES'])
     if args.D:
         pp.context.update(args.D)
     for undefine in args.U:
         if undefine in pp.context:
             del pp.context[undefine]
     # Hack until MOZ_DEBUG_FLAGS are simply part of buildconfig.defines
-    if buildconfig.substs['MOZ_DEBUG']:
+    if buildconfig.substs.get('MOZ_DEBUG'):
         pp.context['DEBUG'] = '1'
     # Ensure @DATA@ works as expected (see the Windows section further below)
     if buildconfig.substs['OS_TARGET'] == 'WINNT':
         pp.context['DATA'] = 'DATA'
     else:
         pp.context['DATA'] = ''
     pp.out = StringIO()
     pp.do_filter('substitution')
diff --git a/python/mozbuild/mozbuild/frontend/gyp_reader.py b/python/mozbuild/mozbuild/frontend/gyp_reader.py
--- a/python/mozbuild/mozbuild/frontend/gyp_reader.py
+++ b/python/mozbuild/mozbuild/frontend/gyp_reader.py
@@ -143,17 +143,17 @@ def process_gyp_result(gyp_result, gyp_d
         # The list of included files returned by gyp are relative to build_file
         for f in data[build_file]['included_files']:
             context.add_source(mozpath.abspath(mozpath.join(
                 mozpath.dirname(build_file), f)))
 
         spec = targets[target]
 
         # Derive which gyp configuration to use based on MOZ_DEBUG.
-        c = 'Debug' if config.substs['MOZ_DEBUG'] else 'Release'
+        c = 'Debug' if config.substs.get('MOZ_DEBUG') else 'Release'
         if c not in spec['configurations']:
             raise RuntimeError('Missing %s gyp configuration for target %s '
                                'in %s' % (c, target_name, build_file))
         target_conf = spec['configurations'][c]
 
         if 'actions' in spec:
           handle_actions(spec['actions'], context, action_overrides)
         if 'copies' in spec:
