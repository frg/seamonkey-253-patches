# HG changeset patch
# User David Anderson <danderson@mozilla.com>
# Date 1509476551 25200
# Node ID 811d8f83793ae6a31ae6d693f9ae7d6ece1f0a9c
# Parent  6bc610d0722dfa03f1986bb3ddb896b5826a646c
Don't call MarkChanged for commands that don't affect pixels. (bug 1395478 part 8, r=rhunt)

diff --git a/gfx/2d/DrawCommand.h b/gfx/2d/DrawCommand.h
--- a/gfx/2d/DrawCommand.h
+++ b/gfx/2d/DrawCommand.h
@@ -175,16 +175,18 @@ public:
     CLONE_INTO(DrawSurfaceCommand)(mSurface, mDest, mSource, mSurfOptions, mOptions);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->DrawSurface(mSurface, mDest, mSource, mSurfOptions, mOptions);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   RefPtr<SourceSurface> mSurface;
   Rect mDest;
   Rect mSource;
   DrawSurfaceOptions mSurfOptions;
   DrawOptions mOptions;
 };
 
@@ -211,16 +213,18 @@ public:
     CLONE_INTO(DrawSurfaceWithShadowCommand)(mSurface, mDest, mColor, mOffset, mSigma, mOperator);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->DrawSurfaceWithShadow(mSurface, mDest, mColor, mOffset, mSigma, mOperator);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   RefPtr<SourceSurface> mSurface;
   Point mDest;
   Color mColor;
   Point mOffset;
   Float mSigma;
   CompositionOp mOperator;
 };
@@ -240,16 +244,18 @@ public:
     CLONE_INTO(DrawFilterCommand)(mFilter, mSourceRect, mDestPoint, mOptions);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->DrawFilter(mFilter, mSourceRect, mDestPoint, mOptions);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   RefPtr<FilterNode> mFilter;
   Rect mSourceRect;
   Point mDestPoint;
   DrawOptions mOptions;
 };
 
 class ClearRectCommand : public DrawingCommand
@@ -265,16 +271,18 @@ public:
     CLONE_INTO(ClearRectCommand)(mRect);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->ClearRect(mRect);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   Rect mRect;
 };
 
 class CopySurfaceCommand : public DrawingCommand
 {
 public:
   CopySurfaceCommand(SourceSurface* aSurface,
@@ -296,16 +304,18 @@ public:
     MOZ_ASSERT(!aTransform || !aTransform->HasNonIntegerTranslation());
     Point dest(Float(mDestination.x), Float(mDestination.y));
     if (aTransform) {
       dest = aTransform->TransformPoint(dest);
     }
     aDT->CopySurface(mSurface, mSourceRect, IntPoint(uint32_t(dest.x), uint32_t(dest.y)));
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   RefPtr<SourceSurface> mSurface;
   IntRect mSourceRect;
   IntPoint mDestination;
 };
 
 class FillRectCommand : public DrawingCommand
 {
@@ -330,16 +340,18 @@ public:
   }
 
   bool GetAffectedRect(Rect& aDeviceRect, const Matrix& aTransform) const
   {
     aDeviceRect = aTransform.TransformBounds(mRect);
     return true;
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   Rect mRect;
   StoredPattern mPattern;
   DrawOptions mOptions;
 };
 
 class StrokeRectCommand : public StrokeOptionsCommand
 {
@@ -359,16 +371,18 @@ public:
     CLONE_INTO(StrokeRectCommand)(mRect, mPattern, mStrokeOptions, mOptions);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->StrokeRect(mRect, mPattern, mStrokeOptions, mOptions);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   Rect mRect;
   StoredPattern mPattern;
   DrawOptions mOptions;
 };
 
 class StrokeLineCommand : public StrokeOptionsCommand
 {
@@ -390,16 +404,18 @@ public:
     CLONE_INTO(StrokeLineCommand)(mStart, mEnd, mPattern, mStrokeOptions, mOptions);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->StrokeLine(mStart, mEnd, mPattern, mStrokeOptions, mOptions);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   Point mStart;
   Point mEnd;
   StoredPattern mPattern;
   DrawOptions mOptions;
 };
 
 class FillCommand : public DrawingCommand
@@ -425,16 +441,18 @@ public:
   }
 
   bool GetAffectedRect(Rect& aDeviceRect, const Matrix& aTransform) const
   {
     aDeviceRect = mPath->GetBounds(aTransform);
     return true;
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   RefPtr<Path> mPath;
   StoredPattern mPattern;
   DrawOptions mOptions;
 };
 
 #ifndef M_SQRT2
 #define M_SQRT2 1.41421356237309504880
@@ -501,16 +519,18 @@ public:
   }
 
   bool GetAffectedRect(Rect& aDeviceRect, const Matrix& aTransform) const
   {
     aDeviceRect = PathExtentsToMaxStrokeExtents(mStrokeOptions, mPath->GetBounds(aTransform), aTransform);
     return true;
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   RefPtr<Path> mPath;
   StoredPattern mPattern;
   DrawOptions mOptions;
 };
 
 class FillGlyphsCommand : public DrawingCommand
 {
@@ -542,16 +562,18 @@ public:
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     GlyphBuffer buf;
     buf.mNumGlyphs = mGlyphs.size();
     buf.mGlyphs = &mGlyphs.front();
     aDT->FillGlyphs(mFont, buf, mPattern, mOptions, mRenderingOptions);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   RefPtr<ScaledFont> mFont;
   std::vector<Glyph> mGlyphs;
   StoredPattern mPattern;
   DrawOptions mOptions;
   RefPtr<GlyphRenderingOptions> mRenderingOptions;
 };
 
@@ -586,16 +608,18 @@ public:
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     GlyphBuffer buf;
     buf.mNumGlyphs = mGlyphs.size();
     buf.mGlyphs = &mGlyphs.front();
     aDT->StrokeGlyphs(mFont, buf, mPattern, mStrokeOptions, mOptions, mRenderingOptions);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   RefPtr<ScaledFont> mFont;
   std::vector<Glyph> mGlyphs;
   StoredPattern mPattern;
   DrawOptions mOptions;
   RefPtr<GlyphRenderingOptions> mRenderingOptions;
 };
 
@@ -616,16 +640,18 @@ public:
     CLONE_INTO(MaskCommand)(mSource, mMask, mOptions);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->Mask(mSource, mMask, mOptions);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   StoredPattern mSource;
   StoredPattern mMask;
   DrawOptions mOptions;
 };
 
 class MaskSurfaceCommand : public DrawingCommand
 {
@@ -646,16 +672,18 @@ public:
     CLONE_INTO(MaskSurfaceCommand)(mSource, mMask, mOffset, mOptions);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->MaskSurface(mSource, mMask, mOffset, mOptions);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   StoredPattern mSource;
   RefPtr<SourceSurface> mMask;
   Point mOffset;
   DrawOptions mOptions;
 };
 
 class PushClipCommand : public DrawingCommand
@@ -671,16 +699,18 @@ public:
     CLONE_INTO(PushClipCommand)(mPath);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PushClip(mPath);
   }
 
+  static const bool AffectsSnapshot = false;
+
 private:
   RefPtr<Path> mPath;
 };
 
 class PushClipRectCommand : public DrawingCommand
 {
 public:
   explicit PushClipRectCommand(const Rect& aRect)
@@ -693,16 +723,18 @@ public:
     CLONE_INTO(PushClipRectCommand)(mRect);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PushClipRect(mRect);
   }
 
+  static const bool AffectsSnapshot = false;
+
 private:
   Rect mRect;
 };
 
 class PushLayerCommand : public DrawingCommand
 {
 public:
   PushLayerCommand(const bool aOpaque,
@@ -726,16 +758,18 @@ public:
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PushLayer(mOpaque, mOpacity, mMask,
                    mMaskTransform, mBounds, mCopyBackground);
   }
 
+  static const bool AffectsSnapshot = false;
+
 private:
   bool mOpaque;
   float mOpacity;
   RefPtr<SourceSurface> mMask;
   Matrix mMaskTransform;
   IntRect mBounds;
   bool mCopyBackground;
 };
@@ -751,16 +785,18 @@ public:
   void CloneInto(CaptureCommandList* aList) {
     CLONE_INTO(PopClipCommand)();
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PopClip();
   }
+
+  static const bool AffectsSnapshot = false;
 };
 
 class PopLayerCommand : public DrawingCommand
 {
 public:
   PopLayerCommand()
     : DrawingCommand(CommandType::POPLAYER)
   {
@@ -769,16 +805,18 @@ public:
   void CloneInto(CaptureCommandList* aList) {
     CLONE_INTO(PopLayerCommand)();
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PopLayer();
   }
+
+  static const bool AffectsSnapshot = true;
 };
 
 class SetTransformCommand : public DrawingCommand
 {
   friend class DrawTargetCaptureImpl;
 public:
   explicit SetTransformCommand(const Matrix& aTransform)
     : DrawingCommand(CommandType::SETTRANSFORM)
@@ -794,16 +832,18 @@ public:
   {
     if (aMatrix) {
       aDT->SetTransform(mTransform * (*aMatrix));
     } else {
       aDT->SetTransform(mTransform);
     }
   }
 
+  static const bool AffectsSnapshot = false;
+
 private:
   Matrix mTransform;
 };
 
 class SetPermitSubpixelAACommand : public DrawingCommand
 {
   friend class DrawTargetCaptureImpl;
 public:
@@ -817,16 +857,18 @@ public:
     CLONE_INTO(SetPermitSubpixelAACommand)(mPermitSubpixelAA);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix* aMatrix) const
   {
     aDT->SetPermitSubpixelAA(mPermitSubpixelAA);
   }
 
+  static const bool AffectsSnapshot = false;
+
 private:
   bool mPermitSubpixelAA;
 };
 
 class FlushCommand : public DrawingCommand
 {
 public:
   explicit FlushCommand()
@@ -837,16 +879,18 @@ public:
   void CloneInto(CaptureCommandList* aList) {
     CLONE_INTO(FlushCommand)();
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->Flush();
   }
+
+  static const bool AffectsSnapshot = false;
 };
 
 class BlurCommand : public DrawingCommand
 {
 public:
   explicit BlurCommand(const AlphaBoxBlur& aBlur)
    : DrawingCommand(CommandType::BLUR)
    , mBlur(aBlur)
@@ -855,16 +899,18 @@ public:
   void CloneInto(CaptureCommandList* aList) {
     CLONE_INTO(BlurCommand)(mBlur);
   }
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const {
     aDT->Blur(mBlur);
   }
 
+  static const bool AffectsSnapshot = true;
+
 private:
   AlphaBoxBlur mBlur;
 };
 
 #undef CLONE_INTO
 
 } // namespace gfx
 } // namespace mozilla
diff --git a/gfx/2d/DrawTargetCapture.h b/gfx/2d/DrawTargetCapture.h
--- a/gfx/2d/DrawTargetCapture.h
+++ b/gfx/2d/DrawTargetCapture.h
@@ -158,17 +158,19 @@ protected:
   void MarkChanged();
 
 private:
   // This storage system was used to minimize the amount of heap allocations
   // that are required while recording. It should be noted there's no
   // guarantees on the alignments of DrawingCommands allocated in this array.
   template<typename T>
   T* AppendToCommandList() {
-    MarkChanged();
+    if (T::AffectsSnapshot) {
+      MarkChanged();
+    }
     return mCommands.Append<T>();
   }
 
   RefPtr<DrawTarget> mRefDT;
   IntSize mSize;
   RefPtr<SourceSurfaceCapture> mSnapshot;
 
   // These are set if the draw target must be explicitly backed by data.
