# HG changeset patch
# User Mark Hammond <mhammond@skippinet.com.au>
# Date 1504746833 -36000
#      Thu Sep 07 11:13:53 2017 +1000
# Node ID def8963114c345d89e9c3e318ddb7471ec622467
# Parent  8dce49fef68f0ba5583679b484efbbc94a386a06
Bug 1397537 - check FxA has a session token and enter a needs-reauth state even when the user is unverified. r=eoger

MozReview-Commit-ID: KuldXySDvt4

diff --git a/browser/components/preferences/in-content/sync.js b/browser/components/preferences/in-content/sync.js
--- a/browser/components/preferences/in-content/sync.js
+++ b/browser/components/preferences/in-content/sync.js
@@ -257,41 +257,46 @@ var gSyncPane = {
     let fxaEmailAddress1Label = document.getElementById("fxaEmailAddress1");
     fxaEmailAddress1Label.hidden = false;
     displayNameLabel.hidden = true;
 
     // determine the fxa status...
     this._showLoadPage(service);
 
     fxAccounts.getSignedInUser().then(data => {
+      return fxAccounts.hasLocalSession().then(hasLocalSession => {
+        return [data, hasLocalSession];
+      });
+    }).then(([data, hasLocalSession]) => {
       if (!data) {
         this.page = FXA_PAGE_LOGGED_OUT;
         return false;
       }
       this.page = FXA_PAGE_LOGGED_IN;
       // We are logged in locally, but maybe we are in a state where the
       // server rejected our credentials (eg, password changed on the server)
       let fxaLoginStatus = document.getElementById("fxaLoginStatus");
       let syncReady;
-      // Not Verfied implies login error state, so check that first.
-      if (!data.verified) {
+      // We need to check error states that need a re-authenticate to resolve
+      // themselves first.
+      if (!hasLocalSession || Weave.Status.login == Weave.LOGIN_FAILED_LOGIN_REJECTED) {
+        fxaLoginStatus.selectedIndex = FXA_LOGIN_FAILED;
+        syncReady = false;
+      } else if (!data.verified) {
         fxaLoginStatus.selectedIndex = FXA_LOGIN_UNVERIFIED;
         syncReady = false;
       // So we think we are logged in, so login problems are next.
       // (Although if the Sync identity manager is still initializing, we
       // ignore login errors and assume all will eventually be good.)
       // LOGIN_FAILED_LOGIN_REJECTED explicitly means "you must log back in".
       // All other login failures are assumed to be transient and should go
       // away by themselves, so aren't reflected here.
-      } else if (Weave.Status.login == Weave.LOGIN_FAILED_LOGIN_REJECTED) {
-        fxaLoginStatus.selectedIndex = FXA_LOGIN_FAILED;
-        syncReady = false;
-      // Else we must be golden (or in an error state we expect to magically
-      // resolve itself)
       } else {
+        // We must be golden (or in an error state we expect to magically
+        // resolve itself)
         fxaLoginStatus.selectedIndex = FXA_LOGIN_VERIFIED;
         syncReady = true;
       }
       fxaEmailAddress1Label.textContent = data.email;
       document.getElementById("fxaEmailAddress2").textContent = data.email;
       document.getElementById("fxaEmailAddress3").textContent = data.email;
       this._populateComputerName(Weave.Service.clientsEngine.localName);
       let engines = document.getElementById("fxaSyncEngines")
diff --git a/services/fxaccounts/FxAccounts.jsm b/services/fxaccounts/FxAccounts.jsm
--- a/services/fxaccounts/FxAccounts.jsm
+++ b/services/fxaccounts/FxAccounts.jsm
@@ -46,16 +46,17 @@ var publicProperties = [
   "getDeviceList",
   "getKeys",
   "getOAuthToken",
   "getProfileCache",
   "getSignedInUser",
   "getSignedInUserProfile",
   "handleDeviceDisconnection",
   "handleAccountDestroyed",
+  "hasLocalSession",
   "invalidateCertificate",
   "loadAndPoll",
   "localtimeOffsetMsec",
   "notifyDevices",
   "now",
   "promiseAccountsChangeProfileURI",
   "promiseAccountsForceSigninURI",
   "promiseAccountsManageURI",
@@ -864,16 +865,34 @@ FxAccountsInternal.prototype = {
         return Promise.reject(new Error(
           "sessionStatus called without a session token"));
       }
       return this.fxAccountsClient.sessionStatus(data.sessionToken);
     });
   },
 
   /**
+   * Checks if we have a valid local session state for the current account.
+   *
+   * @return Promise
+   *        Resolves with a boolean, with true indicating that we appear to
+   *        have a valid local session, or false if we need to reauthenticate
+   *        with the content server to obtain one.
+   *        Note that this doesn't check with the server - it really just tells
+   *        us if we are even able to perform that server check. To fully check
+   *        the account status, you should first call this method, and if this
+   *        returns true, you should then call sessionStatus() to check with
+   *        the server.
+   */
+  async hasLocalSession() {
+    let data = await this.getSignedInUser();
+    return data && data.sessionToken;
+  },
+
+  /**
    * Fetch encryption keys for the signed-in-user from the FxA API server.
    *
    * Not for user consumption.  Exists to cause the keys to be fetch.
    *
    * Returns user data so that it can be chained with other methods.
    *
    * @return Promise
    *        The promise resolves to the credentials object of the signed-in user:
diff --git a/services/fxaccounts/tests/xpcshell/test_accounts.js b/services/fxaccounts/tests/xpcshell/test_accounts.js
--- a/services/fxaccounts/tests/xpcshell/test_accounts.js
+++ b/services/fxaccounts/tests/xpcshell/test_accounts.js
@@ -808,16 +808,19 @@ add_task(async function test_getAssertio
   let creds = {
     sessionToken: "sessionToken",
     kA: expandHex("11"),
     kB: expandHex("66"),
     verified: true,
     email: "sonia@example.com",
   };
   await fxa.setSignedInUser(creds);
+  // we have what we still believe to be a valid session token, so we should
+  // consider that we have a local session.
+  do_check_true(await fxa.hasLocalSession());
 
   try {
     let promiseAssertion = fxa.getAssertion("audience.example.com");
     fxa.internal._d_signCertificate.reject({
       code: 401,
       errno: ERRNO_INVALID_AUTH_TOKEN,
     });
     await promiseAssertion;
@@ -825,16 +828,17 @@ add_task(async function test_getAssertio
   } catch (err) {
     Assert.equal(err.code, 401);
     Assert.equal(err.errno, ERRNO_INVALID_AUTH_TOKEN);
   }
 
   let user = await fxa.internal.getUserAccountData();
   Assert.equal(user.email, creds.email);
   Assert.equal(user.sessionToken, null);
+  Assert.ok(!(await fxa.hasLocalSession()));
 });
 
 add_task(async function test_getAssertion() {
   let fxa = new MockFxAccounts();
 
   do_check_throws(async function() {
     await fxa.getAssertion("nonaudience");
   });
diff --git a/services/sync/modules/UIState.jsm b/services/sync/modules/UIState.jsm
--- a/services/sync/modules/UIState.jsm
+++ b/services/sync/modules/UIState.jsm
@@ -123,33 +123,34 @@ const UIStateInternal = {
   },
 
   notifyStateUpdated() {
     Services.obs.notifyObservers(null, ON_UPDATE);
   },
 
   async _refreshFxAState(newState) {
     let userData = await this._getUserData();
-    this._populateWithUserData(newState, userData);
+    await this._populateWithUserData(newState, userData);
     if (newState.status != STATUS_SIGNED_IN) {
       return;
     }
     let profile = await this._getProfile();
     if (!profile) {
       return;
     }
     this._populateWithProfile(newState, profile);
   },
 
-  _populateWithUserData(state, userData) {
+  async _populateWithUserData(state, userData) {
     let status;
     if (!userData) {
       status = STATUS_NOT_CONFIGURED;
     } else {
-      if (this._loginFailed()) {
+      let loginFailed = await this._loginFailed();
+      if (loginFailed) {
         status = STATUS_LOGIN_FAILED;
       } else if (!userData.verified) {
         status = STATUS_NOT_VERIFIED;
       } else {
         status = STATUS_SIGNED_IN;
       }
       state.email = userData.email;
     }
@@ -191,17 +192,26 @@ const UIStateInternal = {
       try {
         state.lastSync = new Date(Services.prefs.getCharPref("services.sync.lastSync", ""));
       } catch (_) {
         state.lastSync = null;
       }
     }
   },
 
-  _loginFailed() {
+  async _loginFailed() {
+    // First ask FxA if it thinks the user needs re-authentication. In practice,
+    // this check is probably canonical (ie, we probably don't really need
+    // the check below at all as we drop local session info on the first sign
+    // of a problem) - but we keep it for now to keep the risk down.
+    let hasLocalSession = await this.fxAccounts.hasLocalSession();
+    if (!hasLocalSession) {
+      return true;
+    }
+
     // Referencing Weave.Service will implicitly initialize sync, and we don't
     // want to force that - so first check if it is ready.
     let service = Cc["@mozilla.org/weave/service;1"]
                   .getService(Ci.nsISupports)
                   .wrappedJSObject;
     if (!service.ready) {
       return false;
     }
diff --git a/services/sync/tests/unit/test_uistate.js b/services/sync/tests/unit/test_uistate.js
--- a/services/sync/tests/unit/test_uistate.js
+++ b/services/sync/tests/unit/test_uistate.js
@@ -48,17 +48,18 @@ add_task(async function test_refreshStat
   const fxAccountsOrig = UIStateInternal.fxAccounts;
 
   const now = new Date().toString();
   Services.prefs.setCharPref("services.sync.lastSync", now);
   UIStateInternal.syncing = false;
 
   UIStateInternal.fxAccounts = {
     getSignedInUser: () => Promise.resolve({ verified: true, email: "foo@bar.com" }),
-    getSignedInUserProfile: () => Promise.resolve({ displayName: "Foo Bar", avatar: "https://foo/bar", email: "foo@bar.com" })
+    getSignedInUserProfile: () => Promise.resolve({ displayName: "Foo Bar", avatar: "https://foo/bar", email: "foo@bar.com" }),
+    hasLocalSession: () => Promise.resolve(true),
   }
 
   let state = await UIState.refresh();
 
   equal(state.status, UIState.STATUS_SIGNED_IN);
   equal(state.email, "foo@bar.com");
   equal(state.displayName, "Foo Bar");
   equal(state.avatarURL, "https://foo/bar");
@@ -73,17 +74,18 @@ add_task(async function test_refreshStat
   const fxAccountsOrig = UIStateInternal.fxAccounts;
 
   const now = new Date().toString();
   Services.prefs.setCharPref("services.sync.lastSync", now);
   UIStateInternal.syncing = false;
 
   UIStateInternal.fxAccounts = {
     getSignedInUser: () => Promise.resolve({ verified: true, email: "foo@bar.com" }),
-    getSignedInUserProfile: () => Promise.resolve({ displayName: "Foo Bar", avatar: "https://foo/bar", email: "bar@foo.com" })
+    getSignedInUserProfile: () => Promise.resolve({ displayName: "Foo Bar", avatar: "https://foo/bar", email: "bar@foo.com" }),
+    hasLocalSession: () => Promise.resolve(true),
   }
 
   let state = await UIState.refresh();
 
   equal(state.status, UIState.STATUS_SIGNED_IN);
   equal(state.email, "bar@foo.com");
   equal(state.displayName, "Foo Bar");
   equal(state.avatarURL, "https://foo/bar");
@@ -98,17 +100,18 @@ add_task(async function test_refreshStat
   const fxAccountsOrig = UIStateInternal.fxAccounts;
 
   const now = new Date().toString();
   Services.prefs.setCharPref("services.sync.lastSync", now);
   UIStateInternal.syncing = false;
 
   UIStateInternal.fxAccounts = {
     getSignedInUser: () => Promise.resolve({ verified: true, email: "foo@bar.com" }),
-    getSignedInUserProfile: () => Promise.reject(new Error("Profile unavailable"))
+    getSignedInUserProfile: () => Promise.reject(new Error("Profile unavailable")),
+    hasLocalSession: () => Promise.resolve(true),
   }
 
   let state = await UIState.refresh();
 
   equal(state.status, UIState.STATUS_SIGNED_IN);
   equal(state.email, "foo@bar.com");
   equal(state.displayName, undefined);
   equal(state.avatarURL, undefined);
@@ -143,32 +146,58 @@ add_task(async function test_refreshStat
 
 add_task(async function test_refreshState_unverified() {
   UIState.reset();
   const fxAccountsOrig = UIStateInternal.fxAccounts;
 
   let getSignedInUserProfile = sinon.spy();
   UIStateInternal.fxAccounts = {
     getSignedInUser: () => Promise.resolve({ verified: false, email: "foo@bar.com" }),
-    getSignedInUserProfile
+    getSignedInUserProfile,
+    hasLocalSession: () => Promise.resolve(true),
   }
 
   let state = await UIState.refresh();
 
   equal(state.status, UIState.STATUS_NOT_VERIFIED);
   equal(state.email, "foo@bar.com");
   equal(state.displayName, undefined);
   equal(state.avatarURL, undefined);
   equal(state.lastSync, undefined);
 
   ok(!getSignedInUserProfile.called);
 
   UIStateInternal.fxAccounts = fxAccountsOrig;
 });
 
+add_task(async function test_refreshState_unverified_nosession() {
+  UIState.reset();
+  const fxAccountsOrig = UIStateInternal.fxAccounts;
+
+  let getSignedInUserProfile = sinon.spy();
+  UIStateInternal.fxAccounts = {
+    getSignedInUser: () => Promise.resolve({ verified: false, email: "foo@bar.com" }),
+    getSignedInUserProfile,
+    hasLocalSession: () => Promise.resolve(false),
+  }
+
+  let state = await UIState.refresh();
+
+  // No session should "win" over the unverified state.
+  equal(state.status, UIState.STATUS_LOGIN_FAILED);
+  equal(state.email, "foo@bar.com");
+  equal(state.displayName, undefined);
+  equal(state.avatarURL, undefined);
+  equal(state.lastSync, undefined);
+
+  ok(!getSignedInUserProfile.called);
+
+  UIStateInternal.fxAccounts = fxAccountsOrig;
+});
+
 add_task(async function test_refreshState_loginFailed() {
   UIState.reset();
   const fxAccountsOrig = UIStateInternal.fxAccounts;
 
   let loginFailed = sinon.stub(UIStateInternal, "_loginFailed");
   loginFailed.returns(true);
 
   let getSignedInUserProfile = sinon.spy();
@@ -215,17 +244,18 @@ async function configureUIState(syncing,
   UIState.reset();
   const fxAccountsOrig = UIStateInternal.fxAccounts;
 
   UIStateInternal._syncing = syncing;
   Services.prefs.setCharPref("services.sync.lastSync", lastSync.toString());
 
   UIStateInternal.fxAccounts = {
     getSignedInUser: () => Promise.resolve({ verified: true, email: "foo@bar.com" }),
-    getSignedInUserProfile: () => Promise.resolve({ displayName: "Foo Bar", avatar: "https://foo/bar" })
+    getSignedInUserProfile: () => Promise.resolve({ displayName: "Foo Bar", avatar: "https://foo/bar" }),
+    hasLocalSession: () => Promise.resolve(true),
   }
   await UIState.refresh();
   UIStateInternal.fxAccounts = fxAccountsOrig;
 }
 
 add_task(async function test_syncStarted() {
   await configureUIState(false);
 
