# HG changeset patch
# User Blake Kaplan <mrbkap@gmail.com>
# Date 1503535209 25200
#      Wed Aug 23 17:40:09 2017 -0700
# Node ID ded0b612dba768607ab11687d874b974a9855201
# Parent  a55907093f6ab921323711197059c3743dd29722
Bug 1376507 - Move state onto XPCJSContext. r=billm

The current code assumes it can store data about "the" XPCJSContext on the
WatchdogManager singleton. Once we have more than one XPCJSContext running
around, that won't be possible. This moves the needed data onto the
XPCJSContext itself and gives the WatchdogManager the proper context to
operate on.

MozReview-Commit-ID: AxyFKp9LmH3

diff --git a/js/xpconnect/src/XPCJSContext.cpp b/js/xpconnect/src/XPCJSContext.cpp
--- a/js/xpconnect/src/XPCJSContext.cpp
+++ b/js/xpconnect/src/XPCJSContext.cpp
@@ -237,21 +237,19 @@ class Watchdog
 #define PREF_MAX_SCRIPT_RUN_TIME_EXT_CONTENT "dom.max_ext_content_script_run_time"
 
 class WatchdogManager : public nsIObserver
 {
   public:
 
     NS_DECL_ISUPPORTS
     explicit WatchdogManager(XPCJSContext* aContext) : mContext(aContext)
-                                                     , mContextState(CONTEXT_INACTIVE)
     {
-        // All the timestamps start at zero except for context state change.
+        // All the timestamps start at zero.
         PodArrayZero(mTimestamps);
-        mTimestamps[TimestampContextStateChange] = PR_Now();
 
         // Enable the watchdog, if appropriate.
         RefreshWatchdog();
 
         // Register ourselves as an observer to get updates on the pref.
         mozilla::Preferences::AddStrongObserver(this, "dom.use_watchdog");
         mozilla::Preferences::AddStrongObserver(this, PREF_MAX_SCRIPT_RUN_TIME_CONTENT);
         mozilla::Preferences::AddStrongObserver(this, PREF_MAX_SCRIPT_RUN_TIME_CHROME);
@@ -283,52 +281,70 @@ class WatchdogManager : public nsIObserv
     {
         RefreshWatchdog();
         return NS_OK;
     }
 
     // Context statistics. These live on the watchdog manager, are written
     // from the main thread, and are read from the watchdog thread (holding
     // the lock in each case).
-    void
-    RecordContextActivity(bool active)
+    void RecordContextActivity(XPCJSContext* aContext, bool active)
     {
         // The watchdog reads this state, so acquire the lock before writing it.
         MOZ_ASSERT(NS_IsMainThread());
         Maybe<AutoLockWatchdog> lock;
         if (mWatchdog)
             lock.emplace(mWatchdog);
 
         // Write state.
-        mTimestamps[TimestampContextStateChange] = PR_Now();
-        mContextState = active ? CONTEXT_ACTIVE : CONTEXT_INACTIVE;
+        aContext->mLastStateChange = PR_Now();
+        aContext->mActive = active ? XPCJSContext::CONTEXT_ACTIVE :
+            XPCJSContext::CONTEXT_INACTIVE;
 
         // The watchdog may be hibernating, waiting for the context to go
         // active. Wake it up if necessary.
         if (active && mWatchdog && mWatchdog->Hibernating())
             mWatchdog->WakeUp();
     }
-    bool IsContextActive() { return mContextState == CONTEXT_ACTIVE; }
+    bool IsContextActive() { return mContext->mActive == XPCJSContext::CONTEXT_ACTIVE; }
     PRTime TimeSinceLastContextStateChange()
     {
-        return PR_Now() - GetTimestamp(TimestampContextStateChange);
+        // Called on the watchdog thread with the lock held.
+        MOZ_ASSERT(!NS_IsMainThread());
+        return PR_Now() - GetContextTimestamp(mContext);
     }
 
     // Note - Because of the context activity timestamp, these are read and
     // written from both threads.
     void RecordTimestamp(WatchdogTimestampCategory aCategory)
     {
+        // Calls to get a context state change must include a context.
+        MOZ_ASSERT(aCategory != TimestampContextStateChange,
+                   "Use RecordContextActivity to update this");
+
         // The watchdog thread always holds the lock when it runs.
         Maybe<AutoLockWatchdog> maybeLock;
         if (NS_IsMainThread() && mWatchdog)
             maybeLock.emplace(mWatchdog);
+
         mTimestamps[aCategory] = PR_Now();
     }
+
+    PRTime GetContextTimestamp(XPCJSContext* aContext)
+    {
+        Maybe<AutoLockWatchdog> maybeLock;
+        if (NS_IsMainThread() && mWatchdog)
+            maybeLock.emplace(mWatchdog);
+        return aContext->mLastStateChange;
+    }
+
     PRTime GetTimestamp(WatchdogTimestampCategory aCategory)
     {
+        MOZ_ASSERT(aCategory != TimestampContextStateChange,
+                   "Use GetContextTimestamp to retrieve this");
         // The watchdog thread always holds the lock when it runs.
         Maybe<AutoLockWatchdog> maybeLock;
         if (NS_IsMainThread() && mWatchdog)
             maybeLock.emplace(mWatchdog);
         return mTimestamps[aCategory];
     }
 
     XPCJSContext* Context() { return mContext; }
@@ -371,18 +387,18 @@ class WatchdogManager : public nsIObserv
         mWatchdog->Shutdown();
         mWatchdog = nullptr;
     }
 
   private:
     XPCJSContext* mContext;
     nsAutoPtr<Watchdog> mWatchdog;
 
-    enum { CONTEXT_ACTIVE, CONTEXT_INACTIVE } mContextState;
-    PRTime mTimestamps[TimestampCount];
+    // We store ContextStateChange on the contexts themselves.
+    PRTime mTimestamps[kWatchdogTimestampCategoryCount - 1];
 };
 
 NS_IMPL_ISUPPORTS(WatchdogManager, nsIObserver)
 
 AutoLockWatchdog::AutoLockWatchdog(Watchdog* aWatchdog) : mWatchdog(aWatchdog)
 {
     PR_Lock(mWatchdog->GetLock());
 }
@@ -450,17 +466,19 @@ WatchdogMain(void* arg)
 
     // Tell the manager that we've shut down.
     self->Finished();
 }
 
 PRTime
 XPCJSContext::GetWatchdogTimestamp(WatchdogTimestampCategory aCategory)
 {
-    return mWatchdogManager->GetTimestamp(aCategory);
+    return aCategory == TimestampContextStateChange ?
+        mWatchdogManager->GetContextTimestamp(this) :
+        mWatchdogManager->GetTimestamp(aCategory);
 }
 
 void
 xpc::SimulateActivityCallback(bool aActive)
 {
     XPCJSContext::ActivityCallback(XPCJSContext::Get(), aActive);
 }
 
@@ -468,17 +486,17 @@ xpc::SimulateActivityCallback(bool aActi
 void
 XPCJSContext::ActivityCallback(void* arg, bool active)
 {
     if (!active) {
         ProcessHangMonitor::ClearHang();
     }
 
     XPCJSContext* self = static_cast<XPCJSContext*>(arg);
-    self->mWatchdogManager->RecordContextActivity(active);
+    self->mWatchdogManager->RecordContextActivity(self, active);
 }
 
 static inline bool
 IsWebExtensionPrincipal(nsIPrincipal* principal, nsAString& addonId)
 {
     return (NS_SUCCEEDED(principal->GetAddonId(addonId)) &&
             !addonId.IsEmpty());
 }
@@ -820,17 +838,19 @@ XPCJSContext::~XPCJSContext()
 XPCJSContext::XPCJSContext()
  : mCallContext(nullptr),
    mAutoRoots(nullptr),
    mResolveName(JSID_VOID),
    mResolvingWrapper(nullptr),
    mWatchdogManager(new WatchdogManager(this)),
    mSlowScriptSecondHalf(false),
    mTimeoutAccumulated(false),
-   mPendingResult(NS_OK)
+   mPendingResult(NS_OK),
+   mActive(CONTEXT_INACTIVE),
+   mLastStateChange(PR_Now())
 {
     MOZ_COUNT_CTOR_INHERITED(XPCJSContext, CycleCollectedJSContext);
     MOZ_RELEASE_ASSERT(!gTlsContext.get());
     gTlsContext.set(this);
 }
 
 /* static */ XPCJSContext*
 XPCJSContext::Get()
diff --git a/js/xpconnect/src/xpcprivate.h b/js/xpconnect/src/xpcprivate.h
--- a/js/xpconnect/src/xpcprivate.h
+++ b/js/xpconnect/src/xpcprivate.h
@@ -71,16 +71,17 @@
 #include "mozilla/Alignment.h"
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/Assertions.h"
 #include "mozilla/Atomics.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/CycleCollectedJSContext.h"
 #include "mozilla/CycleCollectedJSRuntime.h"
 #include "mozilla/DebugOnly.h"
+#include "mozilla/DefineEnum.h"
 #include "mozilla/GuardObjects.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/TimeStamp.h"
 #include "mozilla/UniquePtr.h"
 
 #include "mozilla/dom/ScriptSettings.h"
@@ -331,24 +332,22 @@ private:
 
 /***************************************************************************/
 
 // In the current xpconnect system there can only be one XPCJSContext.
 // So, xpconnect can only be used on one JSContext within the process.
 
 class WatchdogManager;
 
-enum WatchdogTimestampCategory
-{
-    TimestampContextStateChange = 0,
+MOZ_DEFINE_ENUM(WatchdogTimestampCategory, (
     TimestampWatchdogWakeup,
     TimestampWatchdogHibernateStart,
     TimestampWatchdogHibernateStop,
-    TimestampCount
-};
+    TimestampContextStateChange
+));
 
 class AsyncFreeSnowWhite;
 
 template <class StringType>
 class ShortLivedStringBuffer
 {
 public:
     StringType* Create()
@@ -518,18 +517,23 @@ private:
     // Accumulates total time we actually waited for telemetry
     mozilla::TimeDuration mSlowScriptActualWait;
     bool mTimeoutAccumulated;
 
     // mPendingResult is used to implement Components.returnCode.  Only really
     // meaningful while calling through XPCWrappedJS.
     nsresult mPendingResult;
 
+    // These members must be accessed via WatchdogManager.
+    enum { CONTEXT_ACTIVE, CONTEXT_INACTIVE } mActive;
+    PRTime mLastStateChange;
+
     friend class XPCJSRuntime;
     friend class Watchdog;
+    friend class WatchdogManager;
     friend class AutoLockWatchdog;
 };
 
 class XPCJSRuntime final : public mozilla::CycleCollectedJSRuntime
 {
 public:
     static XPCJSRuntime* Get();
 
