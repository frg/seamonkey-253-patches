# HG changeset patch
# User Daisuke Akatsuka <dakatsuka@mozilla.com>
# Date 1505786454 -32400
#      Tue Sep 19 11:00:54 2017 +0900
# Node ID 5fc5a88872adb1ca891b85bcdd072b8159a26c05
# Parent  26d728270f1e34b65f9cc2dfbfec721a12e794f0
Bug 1366989 - Part 3: Add test for updating the bounds of summary graph, delay and endDelay element. r=pbro

MozReview-Commit-ID: Dwa2n471M64

diff --git a/devtools/client/animationinspector/test/browser.ini b/devtools/client/animationinspector/test/browser.ini
--- a/devtools/client/animationinspector/test/browser.ini
+++ b/devtools/client/animationinspector/test/browser.ini
@@ -1,12 +1,13 @@
 [DEFAULT]
 tags = devtools
 subsuite = devtools
 support-files =
+  doc_add_animation.html
   doc_body_animation.html
   doc_delayed_starttime_animations.html
   doc_end_delay.html
   doc_frame_script.js
   doc_keyframes.html
   doc_modify_playbackRate.html
   doc_negative_animation.html
   doc_pseudo_elements.html
@@ -49,16 +50,17 @@ skip-if = os == "linux" && !debug # Bug 
 [browser_animation_running_on_compositor.js]
 [browser_animation_same_nb_of_playerWidgets_and_playerFronts.js]
 [browser_animation_shows_player_on_valid_node.js]
 [browser_animation_spacebar_toggles_animations.js]
 [browser_animation_spacebar_toggles_node_animations.js]
 [browser_animation_summarygraph_for_multiple_easings.js]
 [browser_animation_target_highlight_select.js]
 [browser_animation_target_highlighter_lock.js]
+[browser_animation_timeline_add_animation.js]
 [browser_animation_timeline_currentTime.js]
 [browser_animation_timeline_header.js]
 [browser_animation_timeline_iterationStart.js]
 [browser_animation_timeline_pause_button_01.js]
 [browser_animation_timeline_pause_button_02.js]
 [browser_animation_timeline_pause_button_03.js]
 [browser_animation_timeline_rate_selector.js]
 [browser_animation_timeline_rewind_button.js]
diff --git a/devtools/client/animationinspector/test/browser_animation_timeline_add_animation.js b/devtools/client/animationinspector/test/browser_animation_timeline_add_animation.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/animationinspector/test/browser_animation_timeline_add_animation.js
@@ -0,0 +1,68 @@
+/* vim: set ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+   http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test re-used animation element since we re-use existent animation element
+// for the render performance.
+
+add_task(function* () {
+  yield addTab(URL_ROOT + "doc_add_animation.html");
+  let {panel, controller} = yield openAnimationInspector();
+  const timelineComponent = panel.animationsTimelineComponent;
+
+  // Add new animation which has delay and endDelay.
+  yield startNewAnimation(controller, panel, "#target2");
+  const previousAnimationEl =
+    timelineComponent.animationsEl.querySelector(".animation:nth-child(2)");
+  const previousSummaryGraphEl = previousAnimationEl.querySelector(".summary");
+  const previousDelayEl = previousAnimationEl.querySelector(".delay");
+  const previousEndDelayEl = previousAnimationEl.querySelector(".end-delay");
+  const previousSummaryGraphWidth = previousSummaryGraphEl.viewBox.baseVal.width;
+  const previousDelayBounds = previousDelayEl.getBoundingClientRect();
+  const previousEndDelayBounds = previousEndDelayEl.getBoundingClientRect();
+
+  // Add another animation.
+  yield startNewAnimation(controller, panel, "#target3");
+  const currentAnimationEl =
+    timelineComponent.animationsEl.querySelector(".animation:nth-child(2)");
+  const currentSummaryGraphEl = currentAnimationEl.querySelector(".summary");
+  const currentDelayEl = currentAnimationEl.querySelector(".delay");
+  const currentEndDelayEl = currentAnimationEl.querySelector(".end-delay");
+  const currentSummaryGraphWidth = currentSummaryGraphEl.viewBox.baseVal.width;
+  const currentDelayBounds = currentDelayEl.getBoundingClientRect();
+  const currentEndDelayBounds = currentEndDelayEl.getBoundingClientRect();
+
+  is(previousAnimationEl, currentAnimationEl, ".animation element should be reused");
+  is(previousSummaryGraphEl, currentSummaryGraphEl, ".summary element should be reused");
+  is(previousDelayEl, currentDelayEl, ".delay element should be reused");
+  is(previousEndDelayEl, currentEndDelayEl, ".end-delay element should be reused");
+
+  ok(currentSummaryGraphWidth > previousSummaryGraphWidth,
+     "Reused .summary element viewBox width should be longer");
+  ok(currentDelayBounds.left < previousDelayBounds.left,
+     "Reused .delay element should move to the left");
+  ok(currentDelayBounds.width < previousDelayBounds.width,
+     "Reused .delay element should be shorter");
+  ok(currentEndDelayBounds.left < previousEndDelayBounds.left,
+     "Reused .end-delay element should move to the left");
+  ok(currentEndDelayBounds.width < previousEndDelayBounds.width,
+     "Reused .end-delay element should be shorter");
+});
+
+function* startNewAnimation(controller, panel, selector) {
+  info("Add a new animation to the page and check the time again");
+  let onPlayerAdded = controller.once(controller.PLAYERS_UPDATED_EVENT);
+  let onRendered = waitForAnimationTimelineRendering(panel);
+
+  yield executeInContent("devtools:test:setAttribute", {
+    selector: selector,
+    attributeName: "class",
+    attributeValue: "animation"
+  });
+
+  yield onPlayerAdded;
+  yield onRendered;
+  yield waitForAllAnimationTargets(panel);
+}
diff --git a/devtools/client/animationinspector/test/doc_add_animation.html b/devtools/client/animationinspector/test/doc_add_animation.html
new file mode 100644
--- /dev/null
+++ b/devtools/client/animationinspector/test/doc_add_animation.html
@@ -0,0 +1,57 @@
+<!DOCTYPE html>
+<html lang="en">
+  <head>
+    <meta charset="UTF-8">
+    <style>
+    div {
+      background-color: lime;
+      height: 50px;
+    }
+
+    @keyframes anim {
+      from {
+        opacity: 0;
+      }
+      to {
+        opacity: 1;
+      }
+    }
+
+    #target1 {
+      animation: anim 1s infinite;
+    }
+    </style>
+  </head>
+  <body>
+    <div id="target1">1</div>
+    <div id="target2">2</div>
+    <div id="target3">3</div>
+    <script>
+    "use strict";
+
+    const duration = 1000;
+    const delay = 1000;
+    const endDelay = 1000;
+
+    // ".animation" is appended from test.
+    const observer = new MutationObserver(mutations => {
+      for (const mutation of mutations) {
+        if (mutation.type === "attributes" &&
+            mutation.attributeName === "class" &&
+            mutation.target.classList.contains("animation")) {
+          const effect = new KeyframeEffect(mutation.target,
+                                            { opacity: [1, 0] },
+                                            { duration, delay, endDelay });
+          const animation = new Animation(effect, document.timeline);
+          animation.play();
+          // wait
+          animation.currentTime = 100;
+        }
+      }
+    });
+
+    observer.observe(document.querySelector("#target2"), { attributes: true });
+    observer.observe(document.querySelector("#target3"), { attributes: true });
+    </script>
+  </body>
+</html>
