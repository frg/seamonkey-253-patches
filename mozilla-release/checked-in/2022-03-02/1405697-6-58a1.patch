# HG changeset patch
# User Chris Pearce <cpearce@mozilla.com>
# Date 1507142047 -7200
# Node ID fb6818a58e110ea8d7c13275398fd8de61419f4d
# Parent  4073a3d2656014e63e6b9a330ca966a567044370
Bug 1405697 - Move BaseMediaResource implementation into cpp file. r=jwwang

This means MediaResource.cpp now only contains the stuff for the MediaResource
super class, and MediaResourceIndex.

MozReview-Commit-ID: 5xFxibn0aJ4

diff --git a/dom/media/BaseMediaResource.cpp b/dom/media/BaseMediaResource.cpp
new file mode 100644
--- /dev/null
+++ b/dom/media/BaseMediaResource.cpp
@@ -0,0 +1,171 @@
+#include "BaseMediaResource.h"
+
+#include "ChannelMediaResource.h"
+#include "CloneableWithRangeMediaResource.h"
+#include "FileMediaResource.h"
+#include "MediaContainerType.h"
+#include "mozilla/dom/BlobImpl.h"
+#include "mozilla/dom/HTMLMediaElement.h"
+#include "nsDebug.h"
+#include "nsError.h"
+#include "nsHostObjectProtocolHandler.h"
+#include "nsICloneableInputStream.h"
+#include "nsIFile.h"
+#include "nsIFileChannel.h"
+#include "nsIInputStream.h"
+#include "nsISeekableStream.h"
+#include "nsNetUtil.h"
+
+namespace mozilla {
+
+already_AddRefed<BaseMediaResource>
+BaseMediaResource::Create(MediaResourceCallback* aCallback,
+                          nsIChannel* aChannel,
+                          bool aIsPrivateBrowsing)
+{
+  NS_ASSERTION(NS_IsMainThread(),
+               "MediaResource::Open called on non-main thread");
+
+  // If the channel was redirected, we want the post-redirect URI;
+  // but if the URI scheme was expanded, say from chrome: to jar:file:,
+  // we want the original URI.
+  nsCOMPtr<nsIURI> uri;
+  nsresult rv = NS_GetFinalChannelURI(aChannel, getter_AddRefs(uri));
+  NS_ENSURE_SUCCESS(rv, nullptr);
+
+  nsAutoCString contentTypeString;
+  aChannel->GetContentType(contentTypeString);
+  Maybe<MediaContainerType> containerType =
+    MakeMediaContainerType(contentTypeString);
+  if (!containerType) {
+    return nullptr;
+  }
+
+  // Let's try to create a FileMediaResource in case the channel is a nsIFile
+  nsCOMPtr<nsIFileChannel> fc = do_QueryInterface(aChannel);
+  if (fc) {
+    RefPtr<BaseMediaResource> resource =
+      new FileMediaResource(aCallback, aChannel, uri);
+    return resource.forget();
+  }
+
+  RefPtr<mozilla::dom::BlobImpl> blobImpl;
+  if (IsBlobURI(uri) &&
+      NS_SUCCEEDED(NS_GetBlobForBlobURI(uri, getter_AddRefs(blobImpl))) &&
+      blobImpl) {
+    IgnoredErrorResult rv;
+
+    nsCOMPtr<nsIInputStream> stream;
+    blobImpl->CreateInputStream(getter_AddRefs(stream), rv);
+    if (NS_WARN_IF(rv.Failed())) {
+      return nullptr;
+    }
+
+    // It's better to read the size from the blob instead of using ::Available,
+    // because, if the stream implements nsIAsyncInputStream interface,
+    // ::Available will not return the size of the stream, but what can be
+    // currently read.
+    uint64_t size = blobImpl->GetSize(rv);
+    if (NS_WARN_IF(rv.Failed())) {
+      return nullptr;
+    }
+
+    // If the URL is a blob URL, with a seekable inputStream, we can still use
+    // a FileMediaResource.
+    nsCOMPtr<nsISeekableStream> seekableStream = do_QueryInterface(stream);
+    if (seekableStream) {
+      RefPtr<BaseMediaResource> resource =
+        new FileMediaResource(aCallback, aChannel, uri, size);
+      return resource.forget();
+    }
+
+    // Maybe this blob URL can be cloned with a range.
+    nsCOMPtr<nsICloneableInputStreamWithRange> cloneableWithRange =
+      do_QueryInterface(stream);
+    if (cloneableWithRange) {
+      RefPtr<BaseMediaResource> resource = new CloneableWithRangeMediaResource(
+        aCallback, aChannel, uri, stream, size);
+      return resource.forget();
+    }
+  }
+
+  RefPtr<BaseMediaResource> resource =
+    new ChannelMediaResource(aCallback, aChannel, uri, aIsPrivateBrowsing);
+  return resource.forget();
+}
+
+void
+BaseMediaResource::SetLoadInBackground(bool aLoadInBackground)
+{
+  if (aLoadInBackground == mLoadInBackground) {
+    return;
+  }
+  mLoadInBackground = aLoadInBackground;
+  if (!mChannel) {
+    // No channel, resource is probably already loaded.
+    return;
+  }
+
+  MediaDecoderOwner* owner = mCallback->GetMediaOwner();
+  if (!owner) {
+    NS_WARNING("Null owner in MediaResource::SetLoadInBackground()");
+    return;
+  }
+  dom::HTMLMediaElement* element = owner->GetMediaElement();
+  if (!element) {
+    NS_WARNING("Null element in MediaResource::SetLoadInBackground()");
+    return;
+  }
+
+  bool isPending = false;
+  if (NS_SUCCEEDED(mChannel->IsPending(&isPending)) && isPending) {
+    nsLoadFlags loadFlags;
+    DebugOnly<nsresult> rv = mChannel->GetLoadFlags(&loadFlags);
+    NS_ASSERTION(NS_SUCCEEDED(rv), "GetLoadFlags() failed!");
+
+    if (aLoadInBackground) {
+      loadFlags |= nsIRequest::LOAD_BACKGROUND;
+    } else {
+      loadFlags &= ~nsIRequest::LOAD_BACKGROUND;
+    }
+    ModifyLoadFlags(loadFlags);
+  }
+}
+
+void
+BaseMediaResource::ModifyLoadFlags(nsLoadFlags aFlags)
+{
+  nsCOMPtr<nsILoadGroup> loadGroup;
+  nsresult rv = mChannel->GetLoadGroup(getter_AddRefs(loadGroup));
+  MOZ_ASSERT(NS_SUCCEEDED(rv), "GetLoadGroup() failed!");
+
+  nsresult status;
+  mChannel->GetStatus(&status);
+
+  bool inLoadGroup = false;
+  if (loadGroup) {
+    rv = loadGroup->RemoveRequest(mChannel, nullptr, status);
+    if (NS_SUCCEEDED(rv)) {
+      inLoadGroup = true;
+    }
+  }
+
+  rv = mChannel->SetLoadFlags(aFlags);
+  MOZ_ASSERT(NS_SUCCEEDED(rv), "SetLoadFlags() failed!");
+
+  if (inLoadGroup) {
+    rv = loadGroup->AddRequest(mChannel, nullptr);
+    MOZ_ASSERT(NS_SUCCEEDED(rv), "AddRequest() failed!");
+  }
+}
+
+void
+BaseMediaResource::DispatchBytesConsumed(int64_t aNumBytes, int64_t aOffset)
+{
+  if (aNumBytes <= 0) {
+    return;
+  }
+  mCallback->NotifyBytesConsumed(aNumBytes, aOffset);
+}
+
+} // namespace mozilla
diff --git a/dom/media/ChannelMediaResource.h b/dom/media/ChannelMediaResource.h
--- a/dom/media/ChannelMediaResource.h
+++ b/dom/media/ChannelMediaResource.h
@@ -4,17 +4,19 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_media_ChannelMediaResource_h
 #define mozilla_dom_media_ChannelMediaResource_h
 
 #include "BaseMediaResource.h"
 #include "MediaCache.h"
 #include "MediaChannelStatistics.h"
+#include "mozilla/Mutex.h"
 #include "nsIChannelEventSink.h"
+#include "nsIHttpChannel.h"
 #include "nsIInterfaceRequestor.h"
 #include "nsIThreadRetargetableStreamListener.h"
 
 namespace mozilla {
 
 /**
  * This class is responsible for managing the suspend count and report suspend
  * status of channel.
diff --git a/dom/media/MediaResource.cpp b/dom/media/MediaResource.cpp
--- a/dom/media/MediaResource.cpp
+++ b/dom/media/MediaResource.cpp
@@ -1,49 +1,20 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
+#include "MediaResource.h"
 #include "mozilla/DebugOnly.h"
-
-#include "ChannelMediaResource.h"
-#include "CloneableWithRangeMediaResource.h"
-#include "DecoderTraits.h"
-#include "FileMediaResource.h"
-#include "MediaResource.h"
-#include "MediaResourceCallback.h"
 #include "mozilla/Logging.h"
 #include "mozilla/SystemGroup.h"
 #include "mozilla/ErrorNames.h"
 
-#include "mozilla/Mutex.h"
-#include "nsDebug.h"
-#include "nsNetUtil.h"
-#include "nsThreadUtils.h"
-#include "nsIFile.h"
-#include "nsIFileChannel.h"
-#include "nsIHttpChannel.h"
-#include "nsISeekableStream.h"
-#include "nsIInputStream.h"
-#include "nsIRequestObserver.h"
-#include "nsIStreamListener.h"
-#include "nsIScriptSecurityManager.h"
-#include "mozilla/dom/BlobImpl.h"
-#include "mozilla/dom/HTMLMediaElement.h"
-#include "nsError.h"
-#include "nsContentUtils.h"
-#include "nsHostObjectProtocolHandler.h"
-#include <algorithm>
-#include "nsProxyRelease.h"
-#include "nsICloneableInputStream.h"
-#include "nsIContentPolicy.h"
-#include "mozilla/ErrorNames.h"
-
 using mozilla::media::TimeUnit;
 
 #undef ILOG
 
 mozilla::LazyLogModule gMediaResourceIndexLog("MediaResourceIndex");
 // Debug logging macro with object pointer and class name.
 #define ILOG(msg, ...)                                                         \
   MOZ_LOG(gMediaResourceIndexLog,                                              \
@@ -65,163 +36,16 @@ MediaResource::Destroy()
     NewNonOwningRunnableMethod(
       "MediaResource::Destroy", this, &MediaResource::Destroy));
   MOZ_ALWAYS_SUCCEEDS(rv);
 }
 
 NS_IMPL_ADDREF(MediaResource)
 NS_IMPL_RELEASE_WITH_DESTROY(MediaResource, Destroy())
 
-already_AddRefed<BaseMediaResource>
-BaseMediaResource::Create(MediaResourceCallback* aCallback,
-                          nsIChannel* aChannel,
-                          bool aIsPrivateBrowsing)
-{
-  NS_ASSERTION(NS_IsMainThread(),
-               "MediaResource::Open called on non-main thread");
-
-  // If the channel was redirected, we want the post-redirect URI;
-  // but if the URI scheme was expanded, say from chrome: to jar:file:,
-  // we want the original URI.
-  nsCOMPtr<nsIURI> uri;
-  nsresult rv = NS_GetFinalChannelURI(aChannel, getter_AddRefs(uri));
-  NS_ENSURE_SUCCESS(rv, nullptr);
-
-  nsAutoCString contentTypeString;
-  aChannel->GetContentType(contentTypeString);
-  Maybe<MediaContainerType> containerType = MakeMediaContainerType(contentTypeString);
-  if (!containerType) {
-    return nullptr;
-  }
-
-  // Let's try to create a FileMediaResource in case the channel is a nsIFile
-  nsCOMPtr<nsIFileChannel> fc = do_QueryInterface(aChannel);
-  if (fc) {
-    RefPtr<BaseMediaResource> resource =
-      new FileMediaResource(aCallback, aChannel, uri);
-    return resource.forget();
-  }
-
-  RefPtr<mozilla::dom::BlobImpl> blobImpl;
-  if (IsBlobURI(uri) &&
-      NS_SUCCEEDED(NS_GetBlobForBlobURI(uri, getter_AddRefs(blobImpl))) &&
-      blobImpl) {
-    IgnoredErrorResult rv;
-
-    nsCOMPtr<nsIInputStream> stream;
-    blobImpl->CreateInputStream(getter_AddRefs(stream), rv);
-    if (NS_WARN_IF(rv.Failed())) {
-      return nullptr;
-    }
-
-    // It's better to read the size from the blob instead of using ::Available,
-    // because, if the stream implements nsIAsyncInputStream interface,
-    // ::Available will not return the size of the stream, but what can be
-    // currently read.
-    uint64_t size = blobImpl->GetSize(rv);
-    if (NS_WARN_IF(rv.Failed())) {
-      return nullptr;
-    }
-
-    // If the URL is a blob URL, with a seekable inputStream, we can still use
-    // a FileMediaResource.
-    nsCOMPtr<nsISeekableStream> seekableStream = do_QueryInterface(stream);
-    if (seekableStream) {
-      RefPtr<BaseMediaResource> resource =
-        new FileMediaResource(aCallback, aChannel, uri, size);
-      return resource.forget();
-    }
-
-    // Maybe this blob URL can be cloned with a range.
-    nsCOMPtr<nsICloneableInputStreamWithRange> cloneableWithRange =
-      do_QueryInterface(stream);
-    if (cloneableWithRange) {
-      RefPtr<BaseMediaResource> resource =
-        new CloneableWithRangeMediaResource(aCallback, aChannel, uri, stream,
-                                            size);
-      return resource.forget();
-    }
-  }
-
-  RefPtr<BaseMediaResource> resource =
-      new ChannelMediaResource(aCallback, aChannel, uri, aIsPrivateBrowsing);
-  return resource.forget();
-}
-
-void BaseMediaResource::SetLoadInBackground(bool aLoadInBackground) {
-  if (aLoadInBackground == mLoadInBackground) {
-    return;
-  }
-  mLoadInBackground = aLoadInBackground;
-  if (!mChannel) {
-    // No channel, resource is probably already loaded.
-    return;
-  }
-
-  MediaDecoderOwner* owner = mCallback->GetMediaOwner();
-  if (!owner) {
-    NS_WARNING("Null owner in MediaResource::SetLoadInBackground()");
-    return;
-  }
-  dom::HTMLMediaElement* element = owner->GetMediaElement();
-  if (!element) {
-    NS_WARNING("Null element in MediaResource::SetLoadInBackground()");
-    return;
-  }
-
-  bool isPending = false;
-  if (NS_SUCCEEDED(mChannel->IsPending(&isPending)) &&
-      isPending) {
-    nsLoadFlags loadFlags;
-    DebugOnly<nsresult> rv = mChannel->GetLoadFlags(&loadFlags);
-    NS_ASSERTION(NS_SUCCEEDED(rv), "GetLoadFlags() failed!");
-
-    if (aLoadInBackground) {
-      loadFlags |= nsIRequest::LOAD_BACKGROUND;
-    } else {
-      loadFlags &= ~nsIRequest::LOAD_BACKGROUND;
-    }
-    ModifyLoadFlags(loadFlags);
-  }
-}
-
-void BaseMediaResource::ModifyLoadFlags(nsLoadFlags aFlags)
-{
-  nsCOMPtr<nsILoadGroup> loadGroup;
-  nsresult rv = mChannel->GetLoadGroup(getter_AddRefs(loadGroup));
-  MOZ_ASSERT(NS_SUCCEEDED(rv), "GetLoadGroup() failed!");
-
-  nsresult status;
-  mChannel->GetStatus(&status);
-
-  bool inLoadGroup = false;
-  if (loadGroup) {
-    rv = loadGroup->RemoveRequest(mChannel, nullptr, status);
-    if (NS_SUCCEEDED(rv)) {
-      inLoadGroup = true;
-    }
-  }
-
-  rv = mChannel->SetLoadFlags(aFlags);
-  MOZ_ASSERT(NS_SUCCEEDED(rv), "SetLoadFlags() failed!");
-
-  if (inLoadGroup) {
-    rv = loadGroup->AddRequest(mChannel, nullptr);
-    MOZ_ASSERT(NS_SUCCEEDED(rv), "AddRequest() failed!");
-  }
-}
-
-void BaseMediaResource::DispatchBytesConsumed(int64_t aNumBytes, int64_t aOffset)
-{
-  if (aNumBytes <= 0) {
-    return;
-  }
-  mCallback->NotifyBytesConsumed(aNumBytes, aOffset);
-}
-
 nsresult
 MediaResourceIndex::Read(char* aBuffer, uint32_t aCount, uint32_t* aBytes)
 {
   NS_ASSERTION(!NS_IsMainThread(), "Don't call on main thread");
 
   // We purposefuly don't check that we may attempt to read past
   // mResource->GetLength() as the resource's length may change over time.
 
diff --git a/dom/media/moz.build b/dom/media/moz.build
--- a/dom/media/moz.build
+++ b/dom/media/moz.build
@@ -205,16 +205,17 @@ UNIFIED_SOURCES += [
     'AudioCompactor.cpp',
     'AudioConverter.cpp',
     'AudioDeviceInfo.cpp',
     'AudioSegment.cpp',
     'AudioStream.cpp',
     'AudioStreamTrack.cpp',
     'AudioTrack.cpp',
     'AudioTrackList.cpp',
+    'BaseMediaResource.cpp',
     'Benchmark.cpp',
     'CanvasCaptureMediaStream.cpp',
     'ChannelMediaDecoder.cpp',
     'ChannelMediaResource.cpp',
     'CloneableWithRangeMediaResource.cpp',
     'CubebUtils.cpp',
     'DOMMediaStream.cpp',
     'EncodedBufferCache.cpp',
