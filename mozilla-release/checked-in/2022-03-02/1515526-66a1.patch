# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1545432916 0
# Node ID 5d8df624d01631ab668ef462f17132ad91f60ba7
# Parent  54b7b2f8f17f2c9aff76d8d4f35cbf751d53382d
Bug 1515526 - Remove need_help_dependency arguments in python configure code. r=chmanchester

It turns out all the changes related to --help linting in lint.py make
them unnecessary, and we still can detect missing --help arguments
without them, per test_lint.py. On the flip side, keeping those
need_help_dependency arguments makes some functions executed twice
because some memoized functions end up being called for both cases
need_help_dependency=True and need_help_dependency=False.

Differential Revision: https://phabricator.services.mozilla.com/D15054

diff --git a/python/mozbuild/mozbuild/configure/__init__.py b/python/mozbuild/mozbuild/configure/__init__.py
--- a/python/mozbuild/mozbuild/configure/__init__.py
+++ b/python/mozbuild/mozbuild/configure/__init__.py
@@ -132,22 +132,21 @@ class DependsFunction(object):
     @property
     def sandboxed_dependencies(self):
         return [
             d.sandboxed if isinstance(d, DependsFunction) else d
             for d in self.dependencies
         ]
 
     @memoize
-    def result(self, need_help_dependency=False):
-        if self.when and not self.sandbox._value_for(self.when,
-                                                     need_help_dependency):
+    def result(self):
+        if self.when and not self.sandbox._value_for(self.when):
             return None
 
-        resolved_args = [self.sandbox._value_for(d, need_help_dependency)
+        resolved_args = [self.sandbox._value_for(d)
                          for d in self.dependencies]
         return self._func(*resolved_args)
 
     def __repr__(self):
         return '<%s.%s %s(%s)>' % (
             self.__class__.__module__,
             self.__class__.__name__,
             self.name,
@@ -212,18 +211,18 @@ class CombinedDependsFunction(DependsFun
                         flatten_deps.append(d2)
             elif d not in flatten_deps:
                 flatten_deps.append(d)
 
         super(CombinedDependsFunction, self).__init__(
             sandbox, func, flatten_deps)
 
     @memoize
-    def result(self, need_help_dependency=False):
-        resolved_args = (self.sandbox._value_for(d, need_help_dependency)
+    def result(self):
+        resolved_args = (self.sandbox._value_for(d)
                          for d in self.dependencies)
         return self._func(resolved_args)
 
     def __eq__(self, other):
         return (isinstance(other, self.__class__) and
                 self._func is other._func and
                 set(self.dependencies) == set(other.dependencies))
 
@@ -428,18 +427,17 @@ class ConfigureSandbox(dict):
                     'Option `%s` is not handled ; reference it with a @depends'
                     % option.option
                 )
 
             self._value_for(option)
 
         # All implied options should exist.
         for implied_option in self._implied_options:
-            value = self._resolve(implied_option.value,
-                                  need_help_dependency=False)
+            value = self._resolve(implied_option.value)
             if value is not None:
                 raise ConfigureError(
                     '`%s`, emitted from `%s` line %d, is unknown.'
                     % (implied_option.option, implied_option.caller[1],
                        implied_option.caller[2]))
 
         # All options should have been removed (handled) by now.
         for arg in self._helper:
@@ -476,54 +474,51 @@ class ConfigureSandbox(dict):
             raise KeyError('Cannot assign `%s` because it is neither a '
                            '@depends nor a @template' % key)
 
         if isinstance(value, SandboxDependsFunction):
             self._depends[value].name = key
 
         return super(ConfigureSandbox, self).__setitem__(key, value)
 
-    def _resolve(self, arg, need_help_dependency=True):
+    def _resolve(self, arg):
         if isinstance(arg, SandboxDependsFunction):
-            return self._value_for_depends(self._depends[arg],
-                                           need_help_dependency)
+            return self._value_for_depends(self._depends[arg])
         return arg
 
-    def _value_for(self, obj, need_help_dependency=False):
+    def _value_for(self, obj):
         if isinstance(obj, SandboxDependsFunction):
             assert obj in self._depends
-            return self._value_for_depends(self._depends[obj],
-                                           need_help_dependency)
+            return self._value_for_depends(self._depends[obj])
 
         elif isinstance(obj, DependsFunction):
-            return self._value_for_depends(obj, need_help_dependency)
+            return self._value_for_depends(obj)
 
         elif isinstance(obj, Option):
             return self._value_for_option(obj)
 
         assert False
 
     @memoize
-    def _value_for_depends(self, obj, need_help_dependency=False):
-        return obj.result(need_help_dependency)
+    def _value_for_depends(self, obj):
+        return obj.result()
 
     @memoize
     def _value_for_option(self, option):
         implied = {}
         for implied_option in self._implied_options[:]:
             if implied_option.name not in (option.name, option.env):
                 continue
             self._implied_options.remove(implied_option)
 
             if (implied_option.when and
                 not self._value_for(implied_option.when)):
                 continue
 
-            value = self._resolve(implied_option.value,
-                                  need_help_dependency=False)
+            value = self._resolve(implied_option.value)
 
             if value is not None:
                 if isinstance(value, OptionValue):
                     pass
                 elif value is True:
                     value = PositiveOptionValue()
                 elif value is False or value == ():
                     value = NegativeOptionValue()
@@ -549,17 +544,17 @@ class ConfigureSandbox(dict):
             raise InvalidOptionError(
                 "'%s' implied by '%s' conflicts with '%s' from the %s"
                 % (e.arg, reason, e.old_arg, e.old_origin))
 
         if option_string:
             self._raw_options[option] = option_string
 
         when = self._conditions.get(option)
-        if (when and not self._value_for(when, need_help_dependency=True) and
+        if (when and not self._value_for(when) and
             value is not None and value.origin != 'default'):
             if value.origin == 'environment':
                 # The value we return doesn't really matter, because of the
                 # requirement for @depends to have the same when.
                 return None
             raise InvalidOptionError(
                 '%s is not available in this configuration'
                 % option_string.split('=', 1)[0])
@@ -640,18 +635,17 @@ class ConfigureSandbox(dict):
             raise ConfigureError('Option `%s` already defined' % option.option)
         if option.env in self._options:
             raise ConfigureError('Option `%s` already defined' % option.env)
         if option.name:
             self._options[option.name] = option
         if option.env:
             self._options[option.env] = option
 
-        if self._help and (when is None or
-                           self._value_for(when, need_help_dependency=True)):
+        if self._help and (when is None or self._value_for(when)):
             self._help.add(option)
 
         return option
 
     def depends_impl(self, *args, **kwargs):
         '''Implementation of @depends()
         This function is a decorator. It returns a function that subsequently
         takes a function and returns a dummy function. The dummy function
@@ -844,26 +838,26 @@ class ConfigureSandbox(dict):
         return glob['imported']
 
     def _resolve_and_set(self, data, name, value, when=None):
         # Don't set anything when --help was on the command line
         if self._help:
             return
         if when and not self._value_for(when):
             return
-        name = self._resolve(name, need_help_dependency=False)
+        name = self._resolve(name)
         if name is None:
             return
         if not isinstance(name, types.StringTypes):
             raise TypeError("Unexpected type: '%s'" % type(name).__name__)
         if name in data:
             raise ConfigureError(
                 "Cannot add '%s' to configuration: Key already "
                 "exists" % name)
-        value = self._resolve(value, need_help_dependency=False)
+        value = self._resolve(value)
         if value is not None:
             data[name] = value
 
     def set_config_impl(self, name, value, when=None):
         '''Implementation of set_config().
         Set the configuration items with the given name to the given value.
         Both `name` and `value` can be references to @depends functions,
         in which case the result from these functions is used. If the result
diff --git a/python/mozbuild/mozbuild/configure/lint.py b/python/mozbuild/mozbuild/configure/lint.py
--- a/python/mozbuild/mozbuild/configure/lint.py
+++ b/python/mozbuild/mozbuild/configure/lint.py
@@ -96,37 +96,36 @@ class LintSandbox(ConfigureSandbox):
                     if arg == 'os' and glob.get('os') is self.OS:
                         continue
                     if arg in self.BUILTINS:
                         continue
                     return True
         return False
 
     @memoize
-    def _value_for_depends(self, obj, need_help_dependency=False):
+    def _value_for_depends(self, obj):
         with_help = self._help_option in obj.dependencies
         if with_help:
             for arg in obj.dependencies:
                 if self._missing_help_dependency(arg):
                     raise ConfigureError(
                         "`%s` depends on '--help' and `%s`. "
                         "`%s` must depend on '--help'"
                         % (obj.name, arg.name, arg.name))
-        elif ((self._help or need_help_dependency) and
-              self._missing_help_dependency(obj)):
+        elif self._missing_help_dependency(obj):
             raise ConfigureError("Missing @depends for `%s`: '--help'" %
                                  obj.name)
-        return super(LintSandbox, self)._value_for_depends(
-            obj, need_help_dependency)
+        return super(LintSandbox, self)._value_for_depends(obj)
 
     def option_impl(self, *args, **kwargs):
         result = super(LintSandbox, self).option_impl(*args, **kwargs)
         when = self._conditions.get(result)
         if when:
-            self._value_for(when, need_help_dependency=True)
+            self._value_for(when)
+
         return result
 
     def unwrap(self, func):
         glob = func.func_globals
         while func in self._wrapped:
             if isinstance(func.func_globals, SandboxedGlobal):
                 glob = func.func_globals
             func = self._wrapped[func]
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
@@ -1545,17 +1545,17 @@ class RustTest(BaseConfigureTest):
             mozpath.abspath('/usr/bin/rustc'): self.invoke_rustc,
         }
 
         self.TARGET = target
         sandbox = self.get_sandbox(paths, {}, [], environ)
 
         # Trick the sandbox into not running the target compiler check
         dep = sandbox._depends[sandbox['c_compiler']]
-        getattr(sandbox, '__value_for_depends')[(dep, False)] = \
+        getattr(sandbox, '__value_for_depends')[(dep,)] = \
             CompilerResult(type=compiler_type)
         return sandbox._value_for(sandbox['rust_target_triple'])
 
     def test_rust_target(self):
         # Cases where the output of config.sub matches a rust target
         for straightforward in (
             'x86_64-unknown-dragonfly',
             'aarch64-unknown-freebsd',
