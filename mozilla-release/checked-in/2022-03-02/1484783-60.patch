# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1536627129 0
#      Tue Sep 11 00:52:09 2018 +0000
# Node ID 91653d2bc3acf89d590dbf5033c6b7abd4e1e60b
# Parent  68084f2aa5715d022067c5c1b845cf5b8c07c655
Bug 1484783 - Ensure to read the default stride when hardware acceleration isn't usable. r=mattwoodrow, a=RyanVM

On some platforms where a hardware decoder is present, but non functioning, we would fail to initialize the video stride, leading to the frames being incorrectly displayed later.

Also delete the DXVA2 manager early under those circumstances

Differential Revision: https://phabricator.services.mozilla.com/D5402

diff --git a/dom/media/platforms/wmf/WMFVideoMFTManager.cpp b/dom/media/platforms/wmf/WMFVideoMFTManager.cpp
--- a/dom/media/platforms/wmf/WMFVideoMFTManager.cpp
+++ b/dom/media/platforms/wmf/WMFVideoMFTManager.cpp
@@ -598,28 +598,33 @@ WMFVideoMFTManager::InitInternal()
       //hr = attr->SetUINT32(CODECAPI_AVDecVideoAcceleration_H264, TRUE);
       //NS_ENSURE_TRUE(SUCCEEDED(hr), hr);
       MOZ_ASSERT(mDXVA2Manager);
       ULONG_PTR manager = ULONG_PTR(mDXVA2Manager->GetDXVADeviceManager());
       hr = decoder->SendMFTMessage(MFT_MESSAGE_SET_D3D_MANAGER, manager);
       if (SUCCEEDED(hr)) {
         mUseHwAccel = true;
       } else {
-        DeleteOnMainThread(mDXVA2Manager);
         mDXVAFailureReason = nsPrintfCString(
           "MFT_MESSAGE_SET_D3D_MANAGER failed with code %X", hr);
       }
     }
     else {
       mDXVAFailureReason.AssignLiteral(
         "Decoder returned false for MF_SA_D3D_AWARE");
     }
   }
 
   if (!mUseHwAccel) {
+    if (mDXVA2Manager) {
+      // Either mDXVAEnabled was set to false prior the second call to
+      // InitInternal() due to CanUseDXVA() returning false, or
+      // MFT_MESSAGE_SET_D3D_MANAGER failed
+      DeleteOnMainThread(mDXVA2Manager);
+    }
     if (mStreamType == VP9 || mStreamType == VP8) {
       return MediaResult(NS_ERROR_DOM_MEDIA_FATAL_ERR,
                          RESULT_DETAIL("Use VP8/9 MFT only if HW acceleration "
                                        "is available."));
     }
     Telemetry::Accumulate(Telemetry::MEDIA_DECODER_BACKEND_USED,
                           uint32_t(media::MediaDecoderBackend::WMFSoftware));
   }
@@ -640,17 +645,17 @@ WMFVideoMFTManager::InitInternal()
     mDXVAEnabled = false;
     // DXVA initialization actually failed, re-do initialisation.
     return InitInternal();
   }
 
   LOG("Video Decoder initialized, Using DXVA: %s",
       (mUseHwAccel ? "Yes" : "No"));
 
-  if (mDXVA2Manager) {
+  if (mUseHwAccel) {
     hr = mDXVA2Manager->ConfigureForSize(mVideoInfo.ImageRect().width,
                                          mVideoInfo.ImageRect().height);
     NS_ENSURE_TRUE(SUCCEEDED(hr),
                    MediaResult(NS_ERROR_DOM_MEDIA_FATAL_ERR,
                                RESULT_DETAIL("Fail to configure image size for "
                                              "DXVA2Manager.")));
   } else {
     GetDefaultStride(outputType, mVideoInfo.ImageRect().width, &mVideoStride);
