# HG changeset patch
# User Matt Woodrow <mwoodrow@mozilla.com>
# Date 1502364402 -43200
# Node ID 61e9f34166f4c657cd8ddb012e2bbeb1cce4641c
# Parent  b92f968bc1cefab348de8f9b4046d211f6c35058
Bug 1388614 - Make sure MathML display items are unique. r=karlt

FrameLayerBuilder requires the the (frame,per-frame-key) for each display item is unique. It only enforces this in certain situations though, so there's cases where we've gotten away without this.

Retained display lists introduces more situations where we rely on this, so I've found a few.

MathML nsDisplayNotation and nsDisplayMathMLBar are the two fixed by this patch.

diff --git a/layout/mathml/nsMathMLFrame.cpp b/layout/mathml/nsMathMLFrame.cpp
--- a/layout/mathml/nsMathMLFrame.cpp
+++ b/layout/mathml/nsMathMLFrame.cpp
@@ -323,31 +323,36 @@ nsMathMLFrame::DisplayBoundingMetrics(ns
   aLists.Content()->AppendNewToTop(new (aBuilder)
       nsDisplayMathMLBoundingMetrics(aBuilder, aFrame, nsRect(x,y,w,h)));
 }
 #endif
 
 class nsDisplayMathMLBar : public nsDisplayItem {
 public:
   nsDisplayMathMLBar(nsDisplayListBuilder* aBuilder,
-                     nsIFrame* aFrame, const nsRect& aRect)
-    : nsDisplayItem(aBuilder, aFrame), mRect(aRect) {
+                     nsIFrame* aFrame, const nsRect& aRect, uint32_t aIndex)
+    : nsDisplayItem(aBuilder, aFrame), mRect(aRect), mIndex(aIndex) {
     MOZ_COUNT_CTOR(nsDisplayMathMLBar);
   }
 #ifdef NS_BUILD_REFCNT_LOGGING
   virtual ~nsDisplayMathMLBar() {
     MOZ_COUNT_DTOR(nsDisplayMathMLBar);
   }
 #endif
 
+  virtual uint32_t GetPerFrameKey() override {
+    return (mIndex << TYPE_BITS) | nsDisplayItem::GetPerFrameKey();
+  }
+
   virtual void Paint(nsDisplayListBuilder* aBuilder,
                      gfxContext* aCtx) override;
   NS_DISPLAY_DECL_NAME("MathMLBar", TYPE_MATHML_BAR)
 private:
   nsRect    mRect;
+  uint32_t  mIndex;
 };
 
 void nsDisplayMathMLBar::Paint(nsDisplayListBuilder* aBuilder,
                                gfxContext* aCtx)
 {
   // paint the bar with the current text color
   DrawTarget* drawTarget = aCtx->GetDrawTarget();
   Rect rect =
@@ -357,22 +362,23 @@ void nsDisplayMathMLBar::Paint(nsDisplay
   ColorPattern color(ToDeviceColor(
     mFrame->GetVisitedDependentColor(&nsStyleText::mWebkitTextFillColor)));
   drawTarget->FillRect(rect, color);
 }
 
 void
 nsMathMLFrame::DisplayBar(nsDisplayListBuilder* aBuilder,
                           nsIFrame* aFrame, const nsRect& aRect,
-                          const nsDisplayListSet& aLists) {
+                          const nsDisplayListSet& aLists,
+                          uint32_t aIndex) {
   if (!aFrame->StyleVisibility()->IsVisible() || aRect.IsEmpty())
     return;
 
   aLists.Content()->AppendNewToTop(new (aBuilder)
-    nsDisplayMathMLBar(aBuilder, aFrame, aRect));
+    nsDisplayMathMLBar(aBuilder, aFrame, aRect, aIndex));
 }
 
 void
 nsMathMLFrame::GetRadicalParameters(nsFontMetrics* aFontMetrics,
                                     bool aDisplayStyle,
                                     nscoord& aRadicalRuleThickness,
                                     nscoord& aRadicalExtraAscender,
                                     nscoord& aRadicalVerticalGap)
diff --git a/layout/mathml/nsMathMLFrame.h b/layout/mathml/nsMathMLFrame.h
--- a/layout/mathml/nsMathMLFrame.h
+++ b/layout/mathml/nsMathMLFrame.h
@@ -358,17 +358,18 @@ protected:
 #endif
 
   /**
    * Display a solid rectangle in the frame's text color. Used for drawing
    * fraction separators and root/sqrt overbars.
    */
   void DisplayBar(nsDisplayListBuilder* aBuilder,
                   nsIFrame* aFrame, const nsRect& aRect,
-                  const nsDisplayListSet& aLists);
+                  const nsDisplayListSet& aLists,
+                  uint32_t aIndex = 0);
 
   // information about the presentation policy of the frame
   nsPresentationData mPresentationData;
 
   // information about a container that is an embellished operator
   nsEmbellishData mEmbellishData;
 
   // Metrics that _exactly_ enclose the text of the frame
diff --git a/layout/mathml/nsMathMLmencloseFrame.cpp b/layout/mathml/nsMathMLmencloseFrame.cpp
--- a/layout/mathml/nsMathMLmencloseFrame.cpp
+++ b/layout/mathml/nsMathMLmencloseFrame.cpp
@@ -42,17 +42,17 @@ nsIFrame*
 NS_NewMathMLmencloseFrame(nsIPresShell* aPresShell, nsStyleContext* aContext)
 {
   return new (aPresShell) nsMathMLmencloseFrame(aContext);
 }
 
 NS_IMPL_FRAMEARENA_HELPERS(nsMathMLmencloseFrame)
 
 nsMathMLmencloseFrame::nsMathMLmencloseFrame(nsStyleContext* aContext, ClassID aID) :
-  nsMathMLContainerFrame(aContext, aID), mNotationsToDraw(0),
+  nsMathMLContainerFrame(aContext, aID),
   mRuleThickness(0), mRadicalRuleThickness(0),
   mLongDivCharIndex(-1), mRadicalCharIndex(-1), mContentWidth(0)
 {
 }
 
 nsMathMLmencloseFrame::~nsMathMLmencloseFrame()
 {
 }
@@ -94,63 +94,68 @@ nsresult nsMathMLmencloseFrame::Allocate
  */
 nsresult nsMathMLmencloseFrame::AddNotation(const nsAString& aNotation)
 {
   nsresult rv;
 
   if (aNotation.EqualsLiteral("longdiv")) {
     rv = AllocateMathMLChar(NOTATION_LONGDIV);
     NS_ENSURE_SUCCESS(rv, rv);
-    mNotationsToDraw |= NOTATION_LONGDIV;
+    mNotationsToDraw += NOTATION_LONGDIV;
   } else if (aNotation.EqualsLiteral("actuarial")) {
-    mNotationsToDraw |= (NOTATION_RIGHT | NOTATION_TOP);
+    mNotationsToDraw += NOTATION_RIGHT;
+    mNotationsToDraw += NOTATION_TOP;
   } else if (aNotation.EqualsLiteral("radical")) {
     rv = AllocateMathMLChar(NOTATION_RADICAL);
     NS_ENSURE_SUCCESS(rv, rv);
-    mNotationsToDraw |= NOTATION_RADICAL;
+    mNotationsToDraw += NOTATION_RADICAL;
   } else if (aNotation.EqualsLiteral("box")) {
-    mNotationsToDraw |= (NOTATION_LEFT | NOTATION_RIGHT |
-                         NOTATION_TOP | NOTATION_BOTTOM);
+    mNotationsToDraw += NOTATION_LEFT;
+    mNotationsToDraw += NOTATION_RIGHT;
+    mNotationsToDraw += NOTATION_TOP;
+    mNotationsToDraw += NOTATION_BOTTOM;
   } else if (aNotation.EqualsLiteral("roundedbox")) {
-    mNotationsToDraw |= NOTATION_ROUNDEDBOX;
+    mNotationsToDraw += NOTATION_ROUNDEDBOX;
   } else if (aNotation.EqualsLiteral("circle")) {
-    mNotationsToDraw |= NOTATION_CIRCLE;
+    mNotationsToDraw += NOTATION_CIRCLE;
   } else if (aNotation.EqualsLiteral("left")) {
-    mNotationsToDraw |= NOTATION_LEFT;
+    mNotationsToDraw += NOTATION_LEFT;
   } else if (aNotation.EqualsLiteral("right")) {
-    mNotationsToDraw |= NOTATION_RIGHT;
+    mNotationsToDraw += NOTATION_RIGHT;
   } else if (aNotation.EqualsLiteral("top")) {
-    mNotationsToDraw |= NOTATION_TOP;
+    mNotationsToDraw += NOTATION_TOP;
   } else if (aNotation.EqualsLiteral("bottom")) {
-    mNotationsToDraw |= NOTATION_BOTTOM;
+    mNotationsToDraw += NOTATION_BOTTOM;
   } else if (aNotation.EqualsLiteral("updiagonalstrike")) {
-    mNotationsToDraw |= NOTATION_UPDIAGONALSTRIKE;
+    mNotationsToDraw += NOTATION_UPDIAGONALSTRIKE;
   } else if (aNotation.EqualsLiteral("updiagonalarrow")) {
-    mNotationsToDraw |= NOTATION_UPDIAGONALARROW;
+    mNotationsToDraw += NOTATION_UPDIAGONALARROW;
   } else if (aNotation.EqualsLiteral("downdiagonalstrike")) {
-    mNotationsToDraw |= NOTATION_DOWNDIAGONALSTRIKE;
+    mNotationsToDraw += NOTATION_DOWNDIAGONALSTRIKE;
   } else if (aNotation.EqualsLiteral("verticalstrike")) {
-    mNotationsToDraw |= NOTATION_VERTICALSTRIKE;
+    mNotationsToDraw += NOTATION_VERTICALSTRIKE;
   } else if (aNotation.EqualsLiteral("horizontalstrike")) {
-    mNotationsToDraw |= NOTATION_HORIZONTALSTRIKE;
+    mNotationsToDraw += NOTATION_HORIZONTALSTRIKE;
   } else if (aNotation.EqualsLiteral("madruwb")) {
-    mNotationsToDraw |= (NOTATION_RIGHT | NOTATION_BOTTOM);
+    mNotationsToDraw += NOTATION_RIGHT;
+    mNotationsToDraw += NOTATION_BOTTOM;
   } else if (aNotation.EqualsLiteral("phasorangle")) {
-    mNotationsToDraw |= (NOTATION_BOTTOM | NOTATION_PHASORANGLE);
+    mNotationsToDraw += NOTATION_BOTTOM;
+    mNotationsToDraw += NOTATION_PHASORANGLE;
   }
 
   return NS_OK;
 }
 
 /*
  * Initialize the list of notations to draw
  */
 void nsMathMLmencloseFrame::InitNotations()
 {
-  mNotationsToDraw = 0;
+  mNotationsToDraw.clear();
   mLongDivCharIndex = mRadicalCharIndex = -1;
   mMathMLChar.Clear();
 
   nsAutoString value;
 
   if (mContent->GetAttr(kNameSpaceID_None, nsGkAtoms::notation_, value)) {
     // parse the notation attribute
     nsWhitespaceTokenizer tokenizer(value);
@@ -158,23 +163,23 @@ void nsMathMLmencloseFrame::InitNotation
     while (tokenizer.hasMoreTokens())
       AddNotation(tokenizer.nextToken());
 
     if (IsToDraw(NOTATION_UPDIAGONALARROW)) {
       // For <menclose notation="updiagonalstrike updiagonalarrow">, if
       // the two notations are drawn then the strike line may cause the point of
       // the arrow to be too wide. Hence we will only draw the updiagonalarrow
       // and the arrow shaft may be thought to be the updiagonalstrike.
-      mNotationsToDraw &= ~NOTATION_UPDIAGONALSTRIKE;
+      mNotationsToDraw -= NOTATION_UPDIAGONALSTRIKE;
     }
   } else {
     // default: longdiv
     if (NS_FAILED(AllocateMathMLChar(NOTATION_LONGDIV)))
       return;
-    mNotationsToDraw = NOTATION_LONGDIV;
+    mNotationsToDraw += NOTATION_LONGDIV;
   }
 }
 
 NS_IMETHODIMP
 nsMathMLmencloseFrame::InheritAutomaticData(nsIFrame* aParent)
 {
   // let the base class get the default from our parent
   nsMathMLContainerFrame::InheritAutomaticData(aParent);
@@ -215,53 +220,53 @@ nsMathMLmencloseFrame::BuildDisplayList(
 
   if (IsToDraw(NOTATION_RADICAL)) {
     mMathMLChar[mRadicalCharIndex].Display(aBuilder, this, aLists, 0);
 
     nsRect rect;
     mMathMLChar[mRadicalCharIndex].GetRect(rect);
     rect.MoveBy(StyleVisibility()->mDirection ? -mContentWidth : rect.width, 0);
     rect.SizeTo(mContentWidth, mRadicalRuleThickness);
-    DisplayBar(aBuilder, this, rect, aLists);
+    DisplayBar(aBuilder, this, rect, aLists, NOTATION_RADICAL);
   }
 
   if (IsToDraw(NOTATION_PHASORANGLE)) {
     DisplayNotation(aBuilder, this, mencloseRect, aLists,
                 mRuleThickness, NOTATION_PHASORANGLE);
   }
 
   if (IsToDraw(NOTATION_LONGDIV)) {
     mMathMLChar[mLongDivCharIndex].Display(aBuilder, this, aLists, 1);
 
     nsRect rect;
     mMathMLChar[mLongDivCharIndex].GetRect(rect);
     rect.SizeTo(rect.width + mContentWidth, mRuleThickness);
-    DisplayBar(aBuilder, this, rect, aLists);
+    DisplayBar(aBuilder, this, rect, aLists, NOTATION_LONGDIV);
   }
 
   if (IsToDraw(NOTATION_TOP)) {
     nsRect rect(0, 0, mencloseRect.width, mRuleThickness);
-    DisplayBar(aBuilder, this, rect, aLists);
+    DisplayBar(aBuilder, this, rect, aLists, NOTATION_TOP);
   }
 
   if (IsToDraw(NOTATION_BOTTOM)) {
     nsRect rect(0, mencloseRect.height - mRuleThickness,
                 mencloseRect.width, mRuleThickness);
-    DisplayBar(aBuilder, this, rect, aLists);
+    DisplayBar(aBuilder, this, rect, aLists, NOTATION_BOTTOM);
   }
 
   if (IsToDraw(NOTATION_LEFT)) {
     nsRect rect(0, 0, mRuleThickness, mencloseRect.height);
-    DisplayBar(aBuilder, this, rect, aLists);
+    DisplayBar(aBuilder, this, rect, aLists, NOTATION_LEFT);
   }
 
   if (IsToDraw(NOTATION_RIGHT)) {
     nsRect rect(mencloseRect.width - mRuleThickness, 0,
                 mRuleThickness, mencloseRect.height);
-    DisplayBar(aBuilder, this, rect, aLists);
+    DisplayBar(aBuilder, this, rect, aLists, NOTATION_RIGHT);
   }
 
   if (IsToDraw(NOTATION_ROUNDEDBOX)) {
     DisplayNotation(aBuilder, this, mencloseRect, aLists,
                     mRuleThickness, NOTATION_ROUNDEDBOX);
   }
 
   if (IsToDraw(NOTATION_CIRCLE)) {
@@ -282,23 +287,23 @@ nsMathMLmencloseFrame::BuildDisplayList(
   if (IsToDraw(NOTATION_DOWNDIAGONALSTRIKE)) {
     DisplayNotation(aBuilder, this, mencloseRect, aLists,
                     mRuleThickness, NOTATION_DOWNDIAGONALSTRIKE);
   }
 
   if (IsToDraw(NOTATION_HORIZONTALSTRIKE)) {
     nsRect rect(0, mencloseRect.height / 2 - mRuleThickness / 2,
                 mencloseRect.width, mRuleThickness);
-    DisplayBar(aBuilder, this, rect, aLists);
+    DisplayBar(aBuilder, this, rect, aLists, NOTATION_HORIZONTALSTRIKE);
   }
 
   if (IsToDraw(NOTATION_VERTICALSTRIKE)) {
     nsRect rect(mencloseRect.width / 2 - mRuleThickness / 2, 0,
                 mRuleThickness, mencloseRect.height);
-    DisplayBar(aBuilder, this, rect, aLists);
+    DisplayBar(aBuilder, this, rect, aLists, NOTATION_VERTICALSTRIKE);
   }
 }
 
 /* virtual */ nsresult
 nsMathMLmencloseFrame::MeasureForWidth(DrawTarget* aDrawTarget,
                                        ReflowOutput& aDesiredSize)
 {
   return PlaceInternal(aDrawTarget, false, aDesiredSize, true);
@@ -751,16 +756,20 @@ public:
     MOZ_COUNT_CTOR(nsDisplayNotation);
   }
 #ifdef NS_BUILD_REFCNT_LOGGING
   virtual ~nsDisplayNotation() {
     MOZ_COUNT_DTOR(nsDisplayNotation);
   }
 #endif
 
+  virtual uint32_t GetPerFrameKey() override {
+    return (mType << TYPE_BITS) | nsDisplayItem::GetPerFrameKey();
+  }
+
   virtual void Paint(nsDisplayListBuilder* aBuilder,
                      gfxContext* aCtx) override;
   NS_DISPLAY_DECL_NAME("MathMLMencloseNotation", TYPE_MATHML_MENCLOSE_NOTATION)
 
 private:
   nsRect             mRect;
   nscoord            mThickness;
   nsMencloseNotation mType;
diff --git a/layout/mathml/nsMathMLmencloseFrame.h b/layout/mathml/nsMathMLmencloseFrame.h
--- a/layout/mathml/nsMathMLmencloseFrame.h
+++ b/layout/mathml/nsMathMLmencloseFrame.h
@@ -3,16 +3,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 
 #ifndef nsMathMLmencloseFrame_h___
 #define nsMathMLmencloseFrame_h___
 
 #include "mozilla/Attributes.h"
+#include "mozilla/EnumSet.h"
 #include "nsMathMLContainerFrame.h"
 
 //
 // <menclose> -- enclose content with a stretching symbol such
 // as a long division sign.
 //
 
 /*
@@ -21,30 +22,30 @@
   The menclose element renders its content inside the enclosing notation
   specified by its notation attribute. menclose accepts any number of arguments;
   if this number is not 1, its contents are treated as a single "inferred mrow"
   containing its arguments, as described in Section 3.1.3 Required Arguments.
 */
 
 enum nsMencloseNotation
   {
-    NOTATION_LONGDIV = 0x1,
-    NOTATION_RADICAL = 0x2,
-    NOTATION_ROUNDEDBOX = 0x4,
-    NOTATION_CIRCLE = 0x8,
-    NOTATION_LEFT = 0x10,
-    NOTATION_RIGHT = 0x20,
-    NOTATION_TOP = 0x40,
-    NOTATION_BOTTOM = 0x80,
-    NOTATION_UPDIAGONALSTRIKE = 0x100,
-    NOTATION_DOWNDIAGONALSTRIKE = 0x200,
-    NOTATION_VERTICALSTRIKE = 0x400,
-    NOTATION_HORIZONTALSTRIKE = 0x800,
-    NOTATION_UPDIAGONALARROW = 0x1000,
-    NOTATION_PHASORANGLE = 0x2000
+    NOTATION_LONGDIV,
+    NOTATION_RADICAL,
+    NOTATION_ROUNDEDBOX,
+    NOTATION_CIRCLE,
+    NOTATION_LEFT,
+    NOTATION_RIGHT,
+    NOTATION_TOP,
+    NOTATION_BOTTOM,
+    NOTATION_UPDIAGONALSTRIKE,
+    NOTATION_DOWNDIAGONALSTRIKE,
+    NOTATION_VERTICALSTRIKE,
+    NOTATION_HORIZONTALSTRIKE,
+    NOTATION_UPDIAGONALARROW,
+    NOTATION_PHASORANGLE
   };
 
 class nsMathMLmencloseFrame : public nsMathMLContainerFrame {
 public:
   NS_DECL_FRAMEARENA_HELPERS(nsMathMLmencloseFrame)
 
   friend nsIFrame* NS_NewMathMLmencloseFrame(nsIPresShell*   aPresShell,
                                              nsStyleContext* aContext);
@@ -96,20 +97,20 @@ protected:
                          ReflowOutput& aDesiredSize,
                          bool                 aWidthOnly);
 
   // functions to parse the "notation" attribute.
   nsresult AddNotation(const nsAString& aNotation);
   void InitNotations();
 
   // Description of the notations to draw
-  uint32_t mNotationsToDraw;
-  bool IsToDraw(nsMencloseNotation mask)
+  mozilla::EnumSet<nsMencloseNotation> mNotationsToDraw;
+  bool IsToDraw(nsMencloseNotation notation)
   {
-    return mask & mNotationsToDraw;
+    return mNotationsToDraw.contains(notation);
   }
 
   nscoord mRuleThickness;
   nscoord mRadicalRuleThickness;
   nsTArray<nsMathMLChar> mMathMLChar;
   int8_t mLongDivCharIndex, mRadicalCharIndex;
   nscoord mContentWidth;
   nsresult AllocateMathMLChar(nsMencloseNotation mask);
diff --git a/layout/mathml/nsMathMLmsqrtFrame.cpp b/layout/mathml/nsMathMLmsqrtFrame.cpp
--- a/layout/mathml/nsMathMLmsqrtFrame.cpp
+++ b/layout/mathml/nsMathMLmsqrtFrame.cpp
@@ -29,17 +29,17 @@ nsMathMLmsqrtFrame::~nsMathMLmsqrtFrame(
 
 void
 nsMathMLmsqrtFrame::Init(nsIContent*       aContent,
                          nsContainerFrame* aParent,
                          nsIFrame*         aPrevInFlow)
 {
   nsMathMLContainerFrame::Init(aContent, aParent, aPrevInFlow);
   AllocateMathMLChar(NOTATION_RADICAL);
-  mNotationsToDraw |= NOTATION_RADICAL;
+  mNotationsToDraw += NOTATION_RADICAL;
 }
 
 NS_IMETHODIMP
 nsMathMLmsqrtFrame::InheritAutomaticData(nsIFrame* aParent)
 {
   nsMathMLContainerFrame::InheritAutomaticData(aParent);
 
   mPresentationData.flags |= NS_MATHML_STRETCH_ALL_CHILDREN_VERTICALLY;
