# HG changeset patch
# User Tom Tromey <tom@tromey.com>
# Date 1505504643 25200
#      Fri Sep 15 12:44:03 2017 -0700
# Node ID d66f3c9329a8a7e9bc8dcccc51656e335033d4ef
# Parent  a4c0d1071346c73088c670e830e6dff8fa2c87b4
servo: Merge #18512 - Preserve sourceURL comment on style sheets (from tromey:preserve-style-sheet-source-url); r=SimonSapin

In addition to the sourceMappingURL comment, there is a second special
comment, "sourceURL", that can be used to set the "display name" of a
style sheet for developer tools.  This name is also used as the base
URL for the source-map URL resolution algorithm.  sourceURL is
described here:
https://blog.getfirebug.com/2009/08/11/give-your-eval-a-name-with-sourceurl/
The devtools feature bug is here:
https://bugzilla.mozilla.org/show_bug.cgi?id=880831

This patch changes servo to preserve and expose this value for use in M-C.

---
<!-- Thank you for contributing to Servo! Please replace each `[ ]` by `[X]` when the step is complete, and replace `__` with appropriate data: -->
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [ ] These changes fix #__ (github issue number if applicable).

<!-- Either: -->
- [X] There are tests for these changes OR
- [ ] These changes do not require tests because _____

Source-Repo: https://github.com/servo/servo
Source-Revision: 7cc0af37cfcd03b0615a408428b53d221e493570

diff --git a/servo/components/malloc_size_of/Cargo.toml b/servo/components/malloc_size_of/Cargo.toml
--- a/servo/components/malloc_size_of/Cargo.toml
+++ b/servo/components/malloc_size_of/Cargo.toml
@@ -5,14 +5,14 @@ authors = ["The Servo Project Developers
 license = "MIT/Apache-2.0"
 publish = false
 
 [lib]
 path = "lib.rs"
 
 [dependencies]
 app_units = "0.5.5"
-cssparser = "0.21.0"
+cssparser = "0.21.1"
 euclid = "0.15"
 hashglobe = { path = "../hashglobe" }
 servo_arc = { path = "../servo_arc" }
 smallbitvec = "1.0.3"
 smallvec = "0.4"
diff --git a/servo/components/selectors/Cargo.toml b/servo/components/selectors/Cargo.toml
--- a/servo/components/selectors/Cargo.toml
+++ b/servo/components/selectors/Cargo.toml
@@ -20,17 +20,17 @@ doctest = false
 
 [features]
 gecko_like_types = []
 unstable = []
 
 [dependencies]
 bitflags = "0.7"
 matches = "0.1"
-cssparser = "0.21.0"
+cssparser = "0.21.1"
 log = "0.3"
 fnv = "1.0"
 malloc_size_of = { path = "../malloc_size_of" }
 malloc_size_of_derive = { path = "../malloc_size_of_derive" }
 phf = "0.7.18"
 precomputed-hash = "0.1"
 servo_arc = { path = "../servo_arc" }
 smallvec = "0.4"
diff --git a/servo/components/style/Cargo.toml b/servo/components/style/Cargo.toml
--- a/servo/components/style/Cargo.toml
+++ b/servo/components/style/Cargo.toml
@@ -31,17 +31,17 @@ gecko_debug = ["nsstring_vendor/gecko_de
 
 [dependencies]
 app_units = "0.5.6"
 arrayvec = "0.3.20"
 atomic_refcell = "0.1"
 bitflags = "0.7"
 byteorder = "1.0"
 cfg-if = "0.1.0"
-cssparser = "0.21.0"
+cssparser = "0.21.1"
 encoding = {version = "0.2", optional = true}
 euclid = "0.15"
 fallible = { path = "../fallible" }
 fnv = "1.0"
 hashglobe = { path = "../hashglobe" }
 heapsize = {version = "0.4", optional = true}
 heapsize_derive = {version = "0.1", optional = true}
 itertools = "0.5"
diff --git a/servo/components/style/stylesheets/stylesheet.rs b/servo/components/style/stylesheets/stylesheet.rs
--- a/servo/components/style/stylesheets/stylesheet.rs
+++ b/servo/components/style/stylesheets/stylesheet.rs
@@ -57,33 +57,35 @@ pub struct StylesheetContents {
     /// The url data this stylesheet should use.
     pub url_data: RwLock<UrlExtraData>,
     /// The namespaces that apply to this stylesheet.
     pub namespaces: RwLock<Namespaces>,
     /// The quirks mode of this stylesheet.
     pub quirks_mode: QuirksMode,
     /// This stylesheet's source map URL.
     pub source_map_url: RwLock<Option<String>>,
+    /// This stylesheet's source URL.
+    pub source_url: RwLock<Option<String>>,
 }
 
 impl StylesheetContents {
     /// Parse a given CSS string, with a given url-data, origin, and
     /// quirks mode.
     pub fn from_str<R: ParseErrorReporter>(
         css: &str,
         url_data: UrlExtraData,
         origin: Origin,
         shared_lock: &SharedRwLock,
         stylesheet_loader: Option<&StylesheetLoader>,
         error_reporter: &R,
         quirks_mode: QuirksMode,
         line_number_offset: u32
     ) -> Self {
         let namespaces = RwLock::new(Namespaces::default());
-        let (rules, source_map_url) = Stylesheet::parse_rules(
+        let (rules, source_map_url, source_url) = Stylesheet::parse_rules(
             css,
             &url_data,
             origin,
             &mut *namespaces.write(),
             &shared_lock,
             stylesheet_loader,
             error_reporter,
             quirks_mode,
@@ -92,16 +94,17 @@ impl StylesheetContents {
 
         Self {
             rules: CssRules::new(rules, &shared_lock),
             origin: origin,
             url_data: RwLock::new(url_data),
             namespaces: namespaces,
             quirks_mode: quirks_mode,
             source_map_url: RwLock::new(source_map_url),
+            source_url: RwLock::new(source_url),
         }
     }
 
     /// Return an iterator using the condition `C`.
     #[inline]
     pub fn iter_rules<'a, 'b, C>(
         &'a self,
         device: &'a Device,
@@ -141,16 +144,17 @@ impl DeepCloneWithLock for StylesheetCon
 
         Self {
             rules: Arc::new(lock.wrap(rules)),
             quirks_mode: self.quirks_mode,
             origin: self.origin,
             url_data: RwLock::new((*self.url_data.read()).clone()),
             namespaces: RwLock::new((*self.namespaces.read()).clone()),
             source_map_url: RwLock::new((*self.source_map_url.read()).clone()),
+            source_url: RwLock::new((*self.source_map_url.read()).clone()),
         }
     }
 }
 
 /// The structure servo uses to represent a stylesheet.
 #[derive(Debug)]
 pub struct Stylesheet {
     /// The contents of this stylesheet.
@@ -309,17 +313,17 @@ impl Stylesheet {
                               css: &str,
                               url_data: UrlExtraData,
                               stylesheet_loader: Option<&StylesheetLoader>,
                               error_reporter: &R,
                               line_number_offset: u32)
         where R: ParseErrorReporter
     {
         let namespaces = RwLock::new(Namespaces::default());
-        let (rules, source_map_url) =
+        let (rules, source_map_url, source_url) =
             Stylesheet::parse_rules(
                 css,
                 &url_data,
                 existing.contents.origin,
                 &mut *namespaces.write(),
                 &existing.shared_lock,
                 stylesheet_loader,
                 error_reporter,
@@ -332,29 +336,30 @@ impl Stylesheet {
             &mut *existing.contents.namespaces.write(),
             &mut *namespaces.write()
         );
 
         // Acquire the lock *after* parsing, to minimize the exclusive section.
         let mut guard = existing.shared_lock.write();
         *existing.contents.rules.write_with(&mut guard) = CssRules(rules);
         *existing.contents.source_map_url.write() = source_map_url;
+        *existing.contents.source_url.write() = source_url;
     }
 
     fn parse_rules<R: ParseErrorReporter>(
         css: &str,
         url_data: &UrlExtraData,
         origin: Origin,
         namespaces: &mut Namespaces,
         shared_lock: &SharedRwLock,
         stylesheet_loader: Option<&StylesheetLoader>,
         error_reporter: &R,
         quirks_mode: QuirksMode,
         line_number_offset: u32
-    ) -> (Vec<CssRule>, Option<String>) {
+    ) -> (Vec<CssRule>, Option<String>, Option<String>) {
         let mut rules = Vec::new();
         let mut input = ParserInput::new_with_line_number_offset(css, line_number_offset);
         let mut input = Parser::new(&mut input);
 
         let context =
             ParserContext::new(
                 origin,
                 url_data,
@@ -394,17 +399,18 @@ impl Stylesheet {
                         iter.parser.context.log_css_error(&iter.parser.error_context,
                                                           err.location, error);
                     }
                 }
             }
         }
 
         let source_map_url = input.current_source_map_url().map(String::from);
-        (rules, source_map_url)
+        let source_url = input.current_source_url().map(String::from);
+        (rules, source_map_url, source_url)
     }
 
     /// Creates an empty stylesheet and parses it with a given base url, origin
     /// and media.
     ///
     /// Effectively creates a new stylesheet and forwards the hard work to
     /// `Stylesheet::update_from_str`.
     pub fn from_str<R: ParseErrorReporter>(
diff --git a/servo/components/style_traits/Cargo.toml b/servo/components/style_traits/Cargo.toml
--- a/servo/components/style_traits/Cargo.toml
+++ b/servo/components/style_traits/Cargo.toml
@@ -11,17 +11,17 @@ path = "lib.rs"
 
 [features]
 servo = ["heapsize", "heapsize_derive", "serde", "servo_atoms", "cssparser/heapsize", "cssparser/serde", "webrender_api"]
 gecko = []
 
 [dependencies]
 app_units = "0.5"
 bitflags = "0.7"
-cssparser = "0.21.0"
+cssparser = "0.21.1"
 euclid = "0.15"
 heapsize = {version = "0.4", optional = true}
 heapsize_derive = {version = "0.1", optional = true}
 malloc_size_of = { path = "../malloc_size_of" }
 malloc_size_of_derive = { path = "../malloc_size_of_derive" }
 selectors = { path = "../selectors" }
 serde = {version = "1.0", optional = true}
 webrender_api = {git = "https://github.com/servo/webrender", optional = true}
diff --git a/servo/ports/geckolib/Cargo.toml b/servo/ports/geckolib/Cargo.toml
--- a/servo/ports/geckolib/Cargo.toml
+++ b/servo/ports/geckolib/Cargo.toml
@@ -10,17 +10,17 @@ path = "lib.rs"
 crate-type = ["staticlib", "rlib"]
 
 [features]
 bindgen = ["style/use_bindgen"]
 gecko_debug = ["style/gecko_debug"]
 
 [dependencies]
 atomic_refcell = "0.1"
-cssparser = "0.21.0"
+cssparser = "0.21.1"
 env_logger = {version = "0.4", default-features = false} # disable `regex` to reduce code size
 libc = "0.2"
 log = {version = "0.3.5", features = ["release_max_level_info"]}
 malloc_size_of = {path = "../../components/malloc_size_of"}
 nsstring_vendor = {path = "../../components/style/gecko_bindings/nsstring_vendor"}
 parking_lot = "0.4"
 # Turn on gecko_like_types because of so that crates which use this
 # crate and also dev-depend on stylo_tests get reasonable behavior
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -1119,16 +1119,28 @@ pub extern "C" fn Servo_StyleSheet_GetSo
 ) {
     let contents = StylesheetContents::as_arc(&sheet);
     let url_opt = contents.source_map_url.read();
     if let Some(ref url) = *url_opt {
         write!(unsafe { &mut *result }, "{}", url).unwrap();
     }
 }
 
+#[no_mangle]
+pub extern "C" fn Servo_StyleSheet_GetSourceURL(
+    sheet: RawServoStyleSheetContentsBorrowed,
+    result: *mut nsAString
+) {
+    let contents = StylesheetContents::as_arc(&sheet);
+    let url_opt = contents.source_url.read();
+    if let Some(ref url) = *url_opt {
+        write!(unsafe { &mut *result }, "{}", url).unwrap();
+    }
+}
+
 fn read_locked_arc<T, R, F>(raw: &<Locked<T> as HasFFI>::FFIType, func: F) -> R
     where Locked<T>: HasArcFFI, F: FnOnce(&T) -> R
 {
     let global_style_data = &*GLOBAL_STYLE_DATA;
     let guard = global_style_data.shared_lock.read();
     func(Locked::<T>::as_arc(&raw).read_with(&guard))
 }
 
diff --git a/servo/tests/unit/style/Cargo.toml b/servo/tests/unit/style/Cargo.toml
--- a/servo/tests/unit/style/Cargo.toml
+++ b/servo/tests/unit/style/Cargo.toml
@@ -7,17 +7,17 @@ license = "MPL-2.0"
 [lib]
 name = "style_tests"
 path = "lib.rs"
 doctest = false
 
 [dependencies]
 byteorder = "1.0"
 app_units = "0.5"
-cssparser = "0.21.0"
+cssparser = "0.21.1"
 euclid = "0.15"
 html5ever = "0.19"
 parking_lot = "0.4"
 rayon = "0.8"
 rustc-serialize = "0.3"
 selectors = {path = "../../../components/selectors"}
 servo_arc = {path = "../../../components/servo_arc"}
 size_of_test = {path = "../../../components/size_of_test"}
diff --git a/servo/tests/unit/style/stylesheets.rs b/servo/tests/unit/style/stylesheets.rs
--- a/servo/tests/unit/style/stylesheets.rs
+++ b/servo/tests/unit/style/stylesheets.rs
@@ -244,16 +244,17 @@ fn test_parse_stylesheet() {
                     vendor_prefix: None,
                     source_location: SourceLocation {
                         line: 16,
                         column: 19,
                     },
                 })))
             ], &stylesheet.shared_lock),
             source_map_url: RwLock::new(None),
+            source_url: RwLock::new(None),
         },
         media: Arc::new(stylesheet.shared_lock.wrap(MediaList::empty())),
         shared_lock: stylesheet.shared_lock.clone(),
         disabled: AtomicBool::new(false),
     };
 
     assert_eq!(format!("{:#?}", stylesheet), format!("{:#?}", expected));
 }
@@ -372,8 +373,27 @@ fn test_source_map_url() {
         let media = Arc::new(lock.wrap(MediaList::empty()));
         let stylesheet = Stylesheet::from_str(test.0, url.clone(), Origin::UserAgent, media, lock,
                                               None, &CSSErrorReporterTest, QuirksMode::NoQuirks,
                                               0);
         let url_opt = stylesheet.contents.source_map_url.read();
         assert_eq!(*url_opt, test.1);
     }
 }
+
+#[test]
+fn test_source_url() {
+    let tests = vec![
+        ("", None),
+        ("/*# sourceURL=something */", Some("something".to_string())),
+    ];
+
+    for test in tests {
+        let url = ServoUrl::parse("about::test").unwrap();
+        let lock = SharedRwLock::new();
+        let media = Arc::new(lock.wrap(MediaList::empty()));
+        let stylesheet = Stylesheet::from_str(test.0, url.clone(), Origin::UserAgent, media, lock,
+                                              None, &CSSErrorReporterTest, QuirksMode::NoQuirks,
+                                              0);
+        let url_opt = stylesheet.contents.source_url.read();
+        assert_eq!(*url_opt, test.1);
+    }
+}
