# HG changeset patch
# User Jan-Ivar Bruaroey <jib@mozilla.com>
# Date 1506445422 14400
#      Tue Sep 26 13:03:42 2017 -0400
# Node ID 4f7815b36c2675463e561d2e7485fba8f8167e83
# Parent  e87031b8c4d6ed8d6525c4bd1593e728c3c3ccb8
Bug 1400912 - Remove default values in CamerasChild's LockAndDispatch. r=padenot

diff --git a/dom/media/systemservices/CamerasChild.cpp b/dom/media/systemservices/CamerasChild.cpp
--- a/dom/media/systemservices/CamerasChild.cpp
+++ b/dom/media/systemservices/CamerasChild.cpp
@@ -211,17 +211,17 @@ CamerasChild::RecvReplyNumberOfCapabilit
 // dispatching succeeded.
 template <class T = int>
 class LockAndDispatch
 {
 public:
   LockAndDispatch(CamerasChild* aCamerasChild,
                   const char* aRequestingFunc,
                   nsIRunnable *aRunnable,
-                  const T& aFailureValue = T(-1), const T& aSuccessValue = T(0))
+                  const T& aFailureValue, const T& aSuccessValue)
     : mCamerasChild(aCamerasChild), mRequestingFunc(aRequestingFunc),
       mRunnable(aRunnable),
       mReplyLock(aCamerasChild->mReplyMonitor),
       mRequestLock(aCamerasChild->mRequestMutex),
       mSuccess(true),
       mFailureValue(aFailureValue), mSuccessValue(aSuccessValue)
   {
     Dispatch();
@@ -251,17 +251,17 @@ private:
   const char* mRequestingFunc;
   nsIRunnable* mRunnable;
   // Prevent concurrent use of the reply variables by holding
   // the mReplyMonitor. Note that this is unlocked while waiting for
   // the reply to be filled in, necessitating the additional mRequestLock/Mutex;
   MonitorAutoLock mReplyLock;
   MutexAutoLock mRequestLock;
   bool mSuccess;
-  const T& mFailureValue;
+  const T mFailureValue;
   const T& mSuccessValue;
 };
 
 bool
 CamerasChild::DispatchToParent(nsIRunnable* aRunnable,
                                MonitorAutoLock& aMonitor)
 {
   CamerasSingleton::Mutex().AssertCurrentThreadOwns();
@@ -355,17 +355,17 @@ CamerasChild::GetCaptureCapability(Captu
   nsCOMPtr<nsIRunnable> runnable =
     mozilla::NewNonOwningRunnableMethod<CaptureEngine, nsCString, unsigned int>(
       "camera::PCamerasChild::SendGetCaptureCapability",
       this,
       &CamerasChild::SendGetCaptureCapability,
       aCapEngine,
       unique_id,
       capability_number);
-  LockAndDispatch<> dispatcher(this, __func__, runnable);
+  LockAndDispatch<> dispatcher(this, __func__, runnable, -1, mZero);
   if (dispatcher.Success()) {
     capability = mReplyCapability;
   }
   return dispatcher.ReturnValue();
 }
 
 mozilla::ipc::IPCResult
 CamerasChild::RecvReplyGetCaptureCapability(const VideoCaptureCapability& ipcCapability)
@@ -396,17 +396,17 @@ CamerasChild::GetCaptureDevice(CaptureEn
   LOG((__PRETTY_FUNCTION__));
   nsCOMPtr<nsIRunnable> runnable =
     mozilla::NewNonOwningRunnableMethod<CaptureEngine, unsigned int>(
       "camera::PCamerasChild::SendGetCaptureDevice",
       this,
       &CamerasChild::SendGetCaptureDevice,
       aCapEngine,
       list_number);
-  LockAndDispatch<> dispatcher(this, __func__, runnable);
+  LockAndDispatch<> dispatcher(this, __func__, runnable, -1, mZero);
   if (dispatcher.Success()) {
     base::strlcpy(device_nameUTF8, mReplyDeviceName.get(), device_nameUTF8Length);
     base::strlcpy(unique_idUTF8, mReplyDeviceID.get(), unique_idUTF8Length);
     if (scary) {
       *scary = mReplyScary;
     }
     LOG(("Got %s name %s id", device_nameUTF8, unique_idUTF8));
   }
@@ -443,17 +443,17 @@ CamerasChild::AllocateCaptureDevice(Capt
                                         nsCString,
                                         const mozilla::ipc::PrincipalInfo&>(
       "camera::PCamerasChild::SendAllocateCaptureDevice",
       this,
       &CamerasChild::SendAllocateCaptureDevice,
       aCapEngine,
       unique_id,
       aPrincipalInfo);
-  LockAndDispatch<> dispatcher(this, __func__, runnable);
+  LockAndDispatch<> dispatcher(this, __func__, runnable, -1, mZero);
   if (dispatcher.Success()) {
     LOG(("Capture Device allocated: %d", mReplyInteger));
     aStreamId = mReplyInteger;
   }
   return dispatcher.ReturnValue();
 }
 
 
@@ -476,17 +476,17 @@ CamerasChild::ReleaseCaptureDevice(Captu
   LOG((__PRETTY_FUNCTION__));
   nsCOMPtr<nsIRunnable> runnable =
     mozilla::NewNonOwningRunnableMethod<CaptureEngine, int>(
       "camera::PCamerasChild::SendReleaseCaptureDevice",
       this,
       &CamerasChild::SendReleaseCaptureDevice,
       aCapEngine,
       capture_id);
-  LockAndDispatch<> dispatcher(this, __func__, runnable);
+  LockAndDispatch<> dispatcher(this, __func__, runnable, -1, mZero);
   return dispatcher.ReturnValue();
 }
 
 void
 CamerasChild::AddCallback(const CaptureEngine aCapEngine, const int capture_id,
                           FrameRelay* render)
 {
   MutexAutoLock lock(mCallbackMutex);
@@ -528,32 +528,32 @@ CamerasChild::StartCapture(CaptureEngine
   nsCOMPtr<nsIRunnable> runnable = mozilla::
     NewNonOwningRunnableMethod<CaptureEngine, int, VideoCaptureCapability>(
       "camera::PCamerasChild::SendStartCapture",
       this,
       &CamerasChild::SendStartCapture,
       aCapEngine,
       capture_id,
       capCap);
-  LockAndDispatch<> dispatcher(this, __func__, runnable);
+  LockAndDispatch<> dispatcher(this, __func__, runnable, -1, mZero);
   return dispatcher.ReturnValue();
 }
 
 int
 CamerasChild::StopCapture(CaptureEngine aCapEngine, const int capture_id)
 {
   LOG((__PRETTY_FUNCTION__));
   nsCOMPtr<nsIRunnable> runnable =
     mozilla::NewNonOwningRunnableMethod<CaptureEngine, int>(
       "camera::PCamerasChild::SendStopCapture",
       this,
       &CamerasChild::SendStopCapture,
       aCapEngine,
       capture_id);
-  LockAndDispatch<> dispatcher(this, __func__, runnable);
+  LockAndDispatch<> dispatcher(this, __func__, runnable, -1, mZero);
   if (dispatcher.Success()) {
     RemoveCallback(aCapEngine, capture_id);
   }
   return dispatcher.ReturnValue();
 }
 
 void
 Shutdown(void)
@@ -716,17 +716,18 @@ CamerasChild::ActorDestroy(ActorDestroyR
   // on replies that'll never come.
   monitor.NotifyAll();
 }
 
 CamerasChild::CamerasChild()
   : mCallbackMutex("mozilla::cameras::CamerasChild::mCallbackMutex"),
     mIPCIsAlive(true),
     mRequestMutex("mozilla::cameras::CamerasChild::mRequestMutex"),
-    mReplyMonitor("mozilla::cameras::CamerasChild::mReplyMonitor")
+    mReplyMonitor("mozilla::cameras::CamerasChild::mReplyMonitor"),
+    mZero(0)
 {
   LOG(("CamerasChild: %p", this));
 
   MOZ_COUNT_CTOR(CamerasChild);
 }
 
 CamerasChild::~CamerasChild()
 {
diff --git a/dom/media/systemservices/CamerasChild.h b/dom/media/systemservices/CamerasChild.h
--- a/dom/media/systemservices/CamerasChild.h
+++ b/dom/media/systemservices/CamerasChild.h
@@ -236,16 +236,17 @@ private:
   // Take this one before taking mReplyMonitor.
   Mutex mRequestMutex;
   // Hold to wait for an async response to our calls
   Monitor mReplyMonitor;
   // Async response valid?
   bool mReceivedReply;
   // Async responses data contents;
   bool mReplySuccess;
+  const int mZero;
   int mReplyInteger;
   webrtc::VideoCaptureCapability mReplyCapability;
   nsCString mReplyDeviceName;
   nsCString mReplyDeviceID;
   bool mReplyScary;
 };
 
 } // namespace camera
