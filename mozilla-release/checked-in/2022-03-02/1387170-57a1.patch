# HG changeset patch
# User Brendan Dahl <brendan.dahl@gmail.com>
# Date 1501798096 25200
#      Thu Aug 03 15:08:16 2017 -0700
# Node ID 411fe4772f31d9ec41fa95fc2e5e15c8df1c7133
# Parent  450ae81d54a9ecce12ad890e0c22e36c0f089c70
Bug 1387170 - Use custom clipboard constructor instead of singleton. r=jrmuizel

This allows instances of the clipboard to be created (like it
was pre-headless).

diff --git a/widget/gtk/nsClipboard.cpp b/widget/gtk/nsClipboard.cpp
--- a/widget/gtk/nsClipboard.cpp
+++ b/widget/gtk/nsClipboard.cpp
@@ -4,28 +4,26 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/ArrayUtils.h"
 
 #include "nsArrayUtils.h"
 #include "nsClipboard.h"
-#include "HeadlessClipboard.h"
 #include "nsSupportsPrimitives.h"
 #include "nsString.h"
 #include "nsReadableUtils.h"
 #include "nsPrimitiveHelpers.h"
 #include "nsIServiceManager.h"
 #include "nsImageToPixbuf.h"
 #include "nsStringStream.h"
 #include "nsIObserverService.h"
 #include "mozilla/Services.h"
 #include "mozilla/RefPtr.h"
-#include "mozilla/ClearOnShutdown.h"
 #include "mozilla/TimeStamp.h"
 
 #include "imgIContainer.h"
 
 #include <gtk/gtk.h>
 
 // For manipulation of the X event queue
 #include <X11/Xlib.h>
@@ -33,17 +31,16 @@
 #include <sys/time.h>
 #include <sys/types.h>
 #include <errno.h>
 #include <unistd.h>
 #include "X11UndefineNone.h"
 
 #include "mozilla/Encoding.h"
 
-#include "gfxPlatform.h"
 
 using namespace mozilla;
 
 // Callback when someone asks us for the data
 void
 clipboard_get_cb(GtkClipboard *aGtkClipboard,
                  GtkSelectionData *aSelectionData,
                  guint info,
@@ -75,44 +72,16 @@ wait_for_contents          (GtkClipboard
 static gchar *
 wait_for_text              (GtkClipboard *clipboard);
 
 static GdkFilterReturn
 selection_request_filter   (GdkXEvent *gdk_xevent,
                             GdkEvent *event,
                             gpointer data);
 
-namespace mozilla {
-namespace clipboard {
-StaticRefPtr<nsIClipboard> sInstance;
-}
-}
-/* static */ already_AddRefed<nsIClipboard>
-nsClipboard::GetInstance()
-{
-    using namespace mozilla::clipboard;
-
-    if (!sInstance) {
-        if (gfxPlatform::IsHeadless()) {
-            sInstance = new widget::HeadlessClipboard();
-        } else {
-            RefPtr<nsClipboard> clipboard = new nsClipboard();
-            nsresult rv = clipboard->Init();
-            if (NS_FAILED(rv)) {
-                return nullptr;
-            }
-            sInstance = clipboard.forget();
-        }
-        ClearOnShutdown(&sInstance);
-    }
-
-    RefPtr<nsIClipboard> service = sInstance.get();
-    return service.forget();
-}
-
 nsClipboard::nsClipboard()
 {
 }
 
 nsClipboard::~nsClipboard()
 {
     // We have to clear clipboard before gdk_display_close() call.
     // See bug 531580 for details.
diff --git a/widget/gtk/nsClipboard.h b/widget/gtk/nsClipboard.h
--- a/widget/gtk/nsClipboard.h
+++ b/widget/gtk/nsClipboard.h
@@ -18,18 +18,16 @@ class nsClipboard : public nsIClipboard,
 public:
     nsClipboard();
 
     NS_DECL_ISUPPORTS
 
     NS_DECL_NSICLIPBOARD
     NS_DECL_NSIOBSERVER
 
-    static already_AddRefed<nsIClipboard> GetInstance();
-
     // Make sure we are initialized, called from the factory
     // constructor
     nsresult  Init              (void);
 
     // Someone requested the selection
     void   SelectionGetEvent    (GtkClipboard     *aGtkClipboard,
                                  GtkSelectionData *aSelectionData);
     void   SelectionClearEvent  (GtkClipboard     *aGtkClipboard);
diff --git a/widget/gtk/nsWidgetFactory.cpp b/widget/gtk/nsWidgetFactory.cpp
--- a/widget/gtk/nsWidgetFactory.cpp
+++ b/widget/gtk/nsWidgetFactory.cpp
@@ -12,16 +12,17 @@
 #include "nsAppShell.h"
 #include "nsAppShellSingleton.h"
 #include "nsBaseWidget.h"
 #include "nsGtkKeyUtils.h"
 #include "nsLookAndFeel.h"
 #include "nsWindow.h"
 #include "nsTransferable.h"
 #include "nsHTMLFormatConverter.h"
+#include "HeadlessClipboard.h"
 #include "IMContextWrapper.h"
 #ifdef MOZ_X11
 #include "nsClipboardHelper.h"
 #include "nsClipboard.h"
 #include "nsDragService.h"
 #endif
 #ifdef MOZ_WIDGET_GTK
 #include "nsApplicationChooser.h"
@@ -68,17 +69,16 @@ static NS_DEFINE_CID(kXULFilePickerCID, 
 
 NS_GENERIC_FACTORY_CONSTRUCTOR(nsWindow)
 NS_GENERIC_FACTORY_CONSTRUCTOR(nsTransferable)
 NS_GENERIC_FACTORY_CONSTRUCTOR(nsBidiKeyboard)
 NS_GENERIC_FACTORY_CONSTRUCTOR(nsHTMLFormatConverter)
 #ifdef MOZ_X11
 NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(nsIdleServiceGTK, nsIdleServiceGTK::GetInstance)
 NS_GENERIC_FACTORY_CONSTRUCTOR(nsClipboardHelper)
-NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(nsIClipboard, nsClipboard::GetInstance)
 NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(nsDragService, nsDragService::GetInstance)
 #endif
 NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(nsISound, nsSound::GetInstance)
 NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(ScreenManager, ScreenManager::GetAddRefedSingleton)
 NS_GENERIC_FACTORY_CONSTRUCTOR(TaskbarProgress)
 
 // from nsWindow.cpp
 extern bool gDisableNativeTheme;
@@ -189,16 +189,38 @@ nsColorPickerConstructor(nsISupports *aO
 
     if (!picker) {
         return NS_ERROR_OUT_OF_MEMORY;
     }
 
     return picker->QueryInterface(aIID, aResult);
 }
 
+static nsresult
+nsClipboardConstructor(nsISupports *aOuter, REFNSIID aIID,
+                       void **aResult)
+{
+  *aResult = nullptr;
+  if (aOuter != nullptr) {
+    return NS_ERROR_NO_AGGREGATION;
+  }
+
+  nsCOMPtr<nsIClipboard> inst;
+  if (gfxPlatform::IsHeadless()) {
+    inst = new HeadlessClipboard();
+  } else {
+    RefPtr<nsClipboard> clipboard = new nsClipboard();
+    nsresult rv = clipboard->Init();
+    NS_ENSURE_SUCCESS(rv, rv);
+    inst = clipboard;
+  }
+
+  return inst->QueryInterface(aIID, aResult);
+}
+
 NS_DEFINE_NAMED_CID(NS_WINDOW_CID);
 NS_DEFINE_NAMED_CID(NS_CHILD_CID);
 NS_DEFINE_NAMED_CID(NS_APPSHELL_CID);
 NS_DEFINE_NAMED_CID(NS_COLORPICKER_CID);
 NS_DEFINE_NAMED_CID(NS_FILEPICKER_CID);
 #ifdef MOZ_WIDGET_GTK
 NS_DEFINE_NAMED_CID(NS_APPLICATIONCHOOSER_CID);
 #endif
@@ -235,17 +257,17 @@ static const mozilla::Module::CIDEntry k
     { &kNS_FILEPICKER_CID, false, nullptr, nsFilePickerConstructor, Module::MAIN_PROCESS_ONLY },
 #ifdef MOZ_WIDGET_GTK
     { &kNS_APPLICATIONCHOOSER_CID, false, nullptr, nsApplicationChooserConstructor, Module::MAIN_PROCESS_ONLY },
 #endif
     { &kNS_GTK_TASKBARPROGRESS_CID, false, nullptr, TaskbarProgressConstructor},
     { &kNS_SOUND_CID, false, nullptr, nsISoundConstructor, Module::MAIN_PROCESS_ONLY },
     { &kNS_TRANSFERABLE_CID, false, nullptr, nsTransferableConstructor },
 #ifdef MOZ_X11
-    { &kNS_CLIPBOARD_CID, false, nullptr, nsIClipboardConstructor, Module::MAIN_PROCESS_ONLY },
+    { &kNS_CLIPBOARD_CID, false, nullptr, nsClipboardConstructor, Module::MAIN_PROCESS_ONLY },
     { &kNS_CLIPBOARDHELPER_CID, false, nullptr, nsClipboardHelperConstructor },
     { &kNS_DRAGSERVICE_CID, false, nullptr, nsDragServiceConstructor, Module::MAIN_PROCESS_ONLY },
 #endif
     { &kNS_HTMLFORMATCONVERTER_CID, false, nullptr, nsHTMLFormatConverterConstructor },
     { &kNS_BIDIKEYBOARD_CID, false, nullptr, nsBidiKeyboardConstructor },
     { &kNS_SCREENMANAGER_CID, false, nullptr, ScreenManagerConstructor,
       Module::MAIN_PROCESS_ONLY },
     { &kNS_THEMERENDERER_CID, false, nullptr, nsNativeThemeGTKConstructor },
