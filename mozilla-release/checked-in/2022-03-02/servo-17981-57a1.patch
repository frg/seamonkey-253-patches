# HG changeset patch
# User Cameron McCormack <cam@mcc.id.au>
# Date 1501926968 18000
#      Sat Aug 05 04:56:08 2017 -0500
# Node ID b8b4ea3fa7a06f0569c98610e2f719081f4290ec
# Parent  1ffe9d8d2b5b06cd67721bcb40b260020a850c47
servo: Merge #17981 - style: Preserve font-family identifier sequence when serializing (from heycam:family-serialization); r=emilio

Reviewed by Emilio in https://bugzilla.mozilla.org/show_bug.cgi?id=1384398.

Source-Repo: https://github.com/servo/servo
Source-Revision: 6cb790f8eb5024dba1d82ad5f7fb4bb912aeda46

diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -2176,27 +2176,28 @@ fn static_assert() {
 
     pub fn fixup_none_generic(&mut self, device: &Device) {
         unsafe {
             bindings::Gecko_nsStyleFont_FixupNoneGeneric(&mut self.gecko, device.pres_context())
         }
     }
 
     pub fn set_font_family(&mut self, v: longhands::font_family::computed_value::T) {
-        use properties::longhands::font_family::computed_value::FontFamily;
+        use properties::longhands::font_family::computed_value::{FontFamily, FamilyNameSyntax};
 
         let list = &mut self.gecko.mFont.fontlist;
         unsafe { Gecko_FontFamilyList_Clear(list); }
 
         self.gecko.mGenericID = structs::kGenericFont_NONE;
 
         for family in &v.0 {
             match *family {
                 FontFamily::FamilyName(ref f) => {
-                    unsafe { Gecko_FontFamilyList_AppendNamed(list, f.name.as_ptr(), f.quoted); }
+                    let quoted = matches!(f.syntax, FamilyNameSyntax::Quoted);
+                    unsafe { Gecko_FontFamilyList_AppendNamed(list, f.name.as_ptr(), quoted); }
                 }
                 FontFamily::Generic(ref name) => {
                     let (family_type, generic) = FontFamily::generic(name);
                     if v.0.len() == 1 {
                         self.gecko.mGenericID = generic;
                     }
                     unsafe { Gecko_FontFamilyList_AppendGeneric(list, family_type); }
                 }
@@ -2217,36 +2218,42 @@ fn static_assert() {
         self.gecko.mGenericID = other.gecko.mGenericID;
     }
 
     pub fn reset_font_family(&mut self, other: &Self) {
         self.copy_font_family_from(other)
     }
 
     pub fn clone_font_family(&self) -> longhands::font_family::computed_value::T {
-        use properties::longhands::font_family::computed_value::{FontFamily, FamilyName};
+        use cssparser::serialize_identifier;
+        use properties::longhands::font_family::computed_value::{FontFamily, FamilyName, FamilyNameSyntax};
         use gecko_bindings::structs::FontFamilyType;
         use gecko_string_cache::Atom;
 
         ::properties::longhands::font_family::computed_value::T(
             self.gecko.mFont.fontlist.mFontlist.iter().map(|gecko_font_family_name| {
                 match gecko_font_family_name.mType {
                     FontFamilyType::eFamily_serif => FontFamily::Generic(atom!("serif")),
                     FontFamilyType::eFamily_sans_serif => FontFamily::Generic(atom!("sans-serif")),
                     FontFamilyType::eFamily_monospace => FontFamily::Generic(atom!("monospace")),
                     FontFamilyType::eFamily_cursive => FontFamily::Generic(atom!("cursive")),
                     FontFamilyType::eFamily_fantasy => FontFamily::Generic(atom!("fantasy")),
                     FontFamilyType::eFamily_moz_fixed => FontFamily::Generic(Atom::from("-moz-fixed")),
-                    FontFamilyType::eFamily_named => FontFamily::FamilyName(FamilyName {
-                        name: (&*gecko_font_family_name.mName).into(),
-                        quoted: false
-                    }),
+                    FontFamilyType::eFamily_named => {
+                        let name = Atom::from(&*gecko_font_family_name.mName);
+                        let mut serialization = String::new();
+                        serialize_identifier(&name.to_string(), &mut serialization).unwrap();
+                        FontFamily::FamilyName(FamilyName {
+                            name: name.clone(),
+                            syntax: FamilyNameSyntax::Identifiers(serialization),
+                        })
+                    },
                     FontFamilyType::eFamily_named_quoted => FontFamily::FamilyName(FamilyName {
                         name: (&*gecko_font_family_name.mName).into(),
-                        quoted: true
+                        syntax: FamilyNameSyntax::Quoted,
                     }),
                     x => panic!("Found unexpected font FontFamilyType: {:?}", x),
                 }
             }).collect()
         )
     }
 
     pub fn unzoom_fonts(&mut self, device: &Device) {
diff --git a/servo/components/style/properties/longhand/font.mako.rs b/servo/components/style/properties/longhand/font.mako.rs
--- a/servo/components/style/properties/longhand/font.mako.rs
+++ b/servo/components/style/properties/longhand/font.mako.rs
@@ -91,17 +91,30 @@ macro_rules! impl_gecko_keyword_conversi
             FamilyName(FamilyName),
             Generic(Atom),
         }
 
         #[derive(Debug, PartialEq, Eq, Clone, Hash)]
         #[cfg_attr(feature = "servo", derive(HeapSizeOf, Deserialize, Serialize))]
         pub struct FamilyName {
             pub name: Atom,
-            pub quoted: bool,
+            pub syntax: FamilyNameSyntax,
+        }
+
+        #[derive(Debug, PartialEq, Eq, Clone, Hash)]
+        #[cfg_attr(feature = "servo", derive(HeapSizeOf, Deserialize, Serialize))]
+        pub enum FamilyNameSyntax {
+            /// The family name was specified in a quoted form, e.g. "Font Name"
+            /// or 'Font Name'.
+            Quoted,
+
+            /// The family name was specified in an unquoted form as a sequence of
+            /// identifiers.  The `String` is the serialization of the sequence of
+            /// identifiers.
+            Identifiers(String),
         }
 
         impl FontFamily {
             #[inline]
             pub fn atom(&self) -> &Atom {
                 match *self {
                     FontFamily::FamilyName(ref family_name) => &family_name.name,
                     FontFamily::Generic(ref name) => name,
@@ -134,26 +147,26 @@ macro_rules! impl_gecko_keyword_conversi
                     "monospace" => return FontFamily::Generic(atom!("monospace")),
                     _ => {}
                 }
 
                 // We don't know if it's quoted or not. So we set it to
                 // quoted by default.
                 FontFamily::FamilyName(FamilyName {
                     name: input,
-                    quoted: true,
+                    syntax: FamilyNameSyntax::Quoted,
                 })
             }
 
             /// Parse a font-family value
             pub fn parse<'i, 't>(input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
                 if let Ok(value) = input.try(|i| i.expect_string_cloned()) {
                     return Ok(FontFamily::FamilyName(FamilyName {
                         name: Atom::from(&*value),
-                        quoted: true,
+                        syntax: FamilyNameSyntax::Quoted,
                     }))
                 }
                 let first_ident = input.expect_ident()?.clone();
 
                 // FIXME(bholley): The fast thing to do here would be to look up the
                 // string (as lowercase) in the static atoms table. We don't have an
                 // API to do that yet though, so we do the simple thing for now.
                 let mut css_wide_keyword = false;
@@ -177,30 +190,38 @@ macro_rules! impl_gecko_keyword_conversi
                     "inherit" => css_wide_keyword = true,
                     "initial" => css_wide_keyword = true,
                     "unset" => css_wide_keyword = true,
                     "default" => css_wide_keyword = true,
                     _ => {}
                 }
 
                 let mut value = first_ident.as_ref().to_owned();
+                let mut serialization = String::new();
+                serialize_identifier(&first_ident, &mut serialization).unwrap();
+
                 // These keywords are not allowed by themselves.
                 // The only way this value can be valid with with another keyword.
                 if css_wide_keyword {
                     let ident = input.expect_ident()?;
-                    value.push_str(" ");
+                    value.push(' ');
                     value.push_str(&ident);
+                    serialization.push(' ');
+                    serialize_identifier(&ident, &mut serialization).unwrap();
                 }
                 while let Ok(ident) = input.try(|i| i.expect_ident_cloned()) {
-                    value.push_str(" ");
+                    value.push(' ');
                     value.push_str(&ident);
+                    serialization.push(' ');
+                    serialize_identifier(&ident, &mut serialization).unwrap();
                 }
+                println!("serialization: {}", serialization);
                 Ok(FontFamily::FamilyName(FamilyName {
                     name: Atom::from(value),
-                    quoted: false,
+                    syntax: FamilyNameSyntax::Identifiers(serialization),
                 }))
             }
 
             #[cfg(feature = "gecko")]
             /// Return the generic ID for a given generic font name
             pub fn generic(name: &Atom) -> (::gecko_bindings::structs::FontFamilyType, u8) {
                 use gecko_bindings::structs::{self, FontFamilyType};
                 if *name == atom!("serif") {
@@ -224,22 +245,27 @@ macro_rules! impl_gecko_keyword_conversi
                 } else {
                     panic!("Unknown generic {}", name);
                 }
             }
         }
 
         impl ToCss for FamilyName {
             fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
-                if self.quoted {
-                    dest.write_char('"')?;
-                    write!(CssStringWriter::new(dest), "{}", self.name)?;
-                    dest.write_char('"')
-                } else {
-                    serialize_identifier(&*self.name.to_string(), dest)
+                match self.syntax {
+                    FamilyNameSyntax::Quoted => {
+                        dest.write_char('"')?;
+                        write!(CssStringWriter::new(dest), "{}", self.name)?;
+                        dest.write_char('"')
+                    }
+                    FamilyNameSyntax::Identifiers(ref serialization) => {
+                        // Note that `serialization` is already escaped/
+                        // serialized appropriately.
+                        dest.write_str(&*serialization)
+                    }
                 }
             }
         }
 
         impl ToCss for FontFamily {
             fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
                 match *self {
                     FontFamily::FamilyName(ref name) => name.to_css(dest),
@@ -2503,17 +2529,17 @@ https://drafts.csswg.org/css-fonts-4/#lo
                         cx.style().get_font().gecko(),
                         cx.device().pres_context()
                     )
                 }
                 let family = system.fontlist.mFontlist.iter().map(|font| {
                     use properties::longhands::font_family::computed_value::*;
                     FontFamily::FamilyName(FamilyName {
                         name: (&*font.mName).into(),
-                        quoted: true
+                        syntax: FamilyNameSyntax::Quoted,
                     })
                 }).collect::<Vec<_>>();
                 let weight = longhands::font_weight::computed_value::T::from_gecko_weight(system.weight);
                 let ret = ComputedSystemFont {
                     font_family: longhands::font_family::computed_value::T(family),
                     font_size: Au(system.size),
                     font_weight: weight,
                     font_size_adjust: longhands::font_size_adjust::computed_value
diff --git a/servo/tests/unit/style/stylesheets.rs b/servo/tests/unit/style/stylesheets.rs
--- a/servo/tests/unit/style/stylesheets.rs
+++ b/servo/tests/unit/style/stylesheets.rs
@@ -9,17 +9,17 @@ use parking_lot::RwLock;
 use selectors::attr::*;
 use selectors::parser::*;
 use servo_arc::Arc;
 use servo_atoms::Atom;
 use servo_url::ServoUrl;
 use std::borrow::ToOwned;
 use std::sync::Mutex;
 use std::sync::atomic::AtomicBool;
-use style::computed_values::font_family::FamilyName;
+use style::computed_values::font_family::{FamilyName, FamilyNameSyntax};
 use style::context::QuirksMode;
 use style::error_reporting::{ParseErrorReporter, ContextualParseError};
 use style::media_queries::MediaList;
 use style::properties::Importance;
 use style::properties::{CSSWideKeyword, DeclaredValueOwned, PropertyDeclaration, PropertyDeclarationBlock};
 use style::properties::longhands;
 use style::properties::longhands::animation_timing_function;
 use style::shared_lock::SharedRwLock;
@@ -249,17 +249,17 @@ fn test_parse_stylesheet() {
                     source_location: SourceLocation {
                         line: 16,
                         column: 19,
                     },
                 }))),
                 CssRule::FontFeatureValues(Arc::new(stylesheet.shared_lock.wrap(FontFeatureValuesRule {
                     family_names: vec![FamilyName {
                         name: Atom::from("test"),
-                        quoted: false,
+                        syntax: FamilyNameSyntax::Identifiers(String::from("test")),
                     }],
                     swash: vec![
                         FFVDeclaration {
                             name: "foo".into(),
                             value: SingleValue(12 as u32),
                         },
                         FFVDeclaration {
                             name: "bar".into(),
