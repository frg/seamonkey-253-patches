# HG changeset patch
# User Gerald Squelart <gsquelart@mozilla.com>
# Date 1512345171 -39600
# Node ID ce2d2aa44948bf070b7ae8e9fab13c11d3d2b2a9
# Parent  11d29f2ac55d2223ff18ba843deefea7c18ea703
Bug 1407810 - Enable DDLogging from the beginning if MOZ_LOG contains DDLogger - r=jwwang

This allows logging from the command line without using a webextension.

MozReview-Commit-ID: 6pkDbLXzz3X

diff --git a/dom/media/doctor/DecoderDoctorLogger.cpp b/dom/media/doctor/DecoderDoctorLogger.cpp
--- a/dom/media/doctor/DecoderDoctorLogger.cpp
+++ b/dom/media/doctor/DecoderDoctorLogger.cpp
@@ -16,16 +16,26 @@ namespace mozilla {
 
 /* static */ Atomic<DecoderDoctorLogger::LogState, ReleaseAcquire>
   DecoderDoctorLogger::sLogState{ DecoderDoctorLogger::scDisabled };
 
 /* static */ const char* DecoderDoctorLogger::sShutdownReason = nullptr;
 
 static DDMediaLogs* sMediaLogs;
 
+/* static */ void
+DecoderDoctorLogger::Init()
+{
+  MOZ_ASSERT(static_cast<LogState>(sLogState) == scDisabled);
+  if (MOZ_LOG_TEST(sDecoderDoctorLoggerLog, LogLevel::Error) ||
+      MOZ_LOG_TEST(sDecoderDoctorLoggerEndLog, LogLevel::Error)) {
+    EnableLogging();
+  }
+}
+
 // First DDLogShutdowner sets sLogState to scShutdown, to prevent further
 // logging.
 struct DDLogShutdowner
 {
   ~DDLogShutdowner()
   {
     DDL_INFO("Shutting down");
     // Prevent further logging, some may racily seep in, it's fine as the
diff --git a/dom/media/doctor/DecoderDoctorLogger.h b/dom/media/doctor/DecoderDoctorLogger.h
--- a/dom/media/doctor/DecoderDoctorLogger.h
+++ b/dom/media/doctor/DecoderDoctorLogger.h
@@ -32,16 +32,20 @@ namespace mozilla {
 //
 // A separate thread processes log messages, and can asynchronously retrieve
 // processed messages that correspond to a given HTMLMediaElement.
 // That thread is also responsible for removing dated messages, so as not to
 // take too much memory.
 class DecoderDoctorLogger
 {
 public:
+  // Called by nsLayoutStatics::Initialize() before any other media work.
+  // Pre-enables logging if MOZ_LOG requires DDLogger.
+  static void Init();
+
   // Is logging currently enabled? This is tested anyway in all public `Log...`
   // functions, but it may be used to prevent logging-only work in clients.
   static inline bool IsDDLoggingEnabled()
   {
     return MOZ_UNLIKELY(static_cast<LogState>(sLogState) == scEnabled);
   }
 
   // Shutdown logging. This will prevent more messages to be queued, but the
diff --git a/layout/build/nsLayoutStatics.cpp b/layout/build/nsLayoutStatics.cpp
--- a/layout/build/nsLayoutStatics.cpp
+++ b/layout/build/nsLayoutStatics.cpp
@@ -111,16 +111,17 @@
 #include "nsPermissionManager.h"
 #include "nsCookieService.h"
 #include "nsApplicationCacheService.h"
 #include "mozilla/dom/CustomElementRegistry.h"
 #include "mozilla/EventDispatcher.h"
 #include "mozilla/IMEStateManager.h"
 #include "mozilla/dom/HTMLVideoElement.h"
 #include "TouchManager.h"
+#include "DecoderDoctorLogger.h"
 #include "MediaDecoder.h"
 #include "MediaPrefs.h"
 #include "mozilla/ServoBindings.h"
 #include "mozilla/StaticPresData.h"
 #include "mozilla/StylePrefs.h"
 #include "mozilla/dom/WebIDLGlobalNameHash.h"
 #include "mozilla/dom/ipc/IPCBlobInputStreamStorage.h"
 #include "mozilla/dom/U2FTokenManager.h"
@@ -247,16 +248,17 @@ nsLayoutStatics::Initialize()
 
   rv = nsFocusManager::Init();
   if (NS_FAILED(rv)) {
     NS_ERROR("Could not initialize nsFocusManager");
     return rv;
   }
 
   AsyncLatencyLogger::InitializeStatics();
+  DecoderDoctorLogger::Init();
   MediaManager::StartupInit();
   CubebUtils::InitLibrary();
 
   nsContentSink::InitializeStatics();
   nsHtml5Module::InitializeStatics();
   mozilla::dom::FallbackEncoding::Initialize();
   nsLayoutUtils::Initialize();
   nsIPresShell::InitializeStatics();
