# HG changeset patch
# User Andrew Sutherland <asutherland@asutherland.org>
# Date 1500953260 14400
#      Mon Jul 24 23:27:40 2017 -0400
# Node ID 1c9560f99aa0669b6a650b49af359de346b4a430
# Parent  9f8161b093c69e7420581619e3869fbb32a7f8a0
Bug 1383978 - Update child process names based on remote type. r=billm

Without this patch, all remote process types share a process name of
"Web Content".  With this patch, specific names are added for "file",
"extension", and "webLargeAllocationTypes", with the default of "web"
left as the default "Web Content".

This patch also eliminates undocumented b2g-era legacy logic that had a
notion of whether it's acceptable to override the process name.  In the
b2g era, I believe processes were named based on the "app" that was
running.  It would have made sense to have the process initially named
the preallocated process, then to change the process to its app name
when specialized, trying to make it hard/impossible for the process to
rename itself so it couldn't masquerade as another app if it became
compromised.

diff --git a/dom/ipc/ContentChild.cpp b/dom/ipc/ContentChild.cpp
--- a/dom/ipc/ContentChild.cpp
+++ b/dom/ipc/ContentChild.cpp
@@ -486,17 +486,16 @@ ContentChild* ContentChild::sSingleton;
 StaticAutoPtr<ContentChild::ShutdownCanary> ContentChild::sShutdownCanary;
 
 ContentChild::ContentChild()
  : mID(uint64_t(-1))
 #if defined(XP_WIN) && defined(ACCESSIBILITY)
  , mMainChromeTid(0)
  , mMsaaID(0)
 #endif
- , mCanOverrideProcessName(true)
  , mIsAlive(true)
  , mShuttingDown(false)
 {
   // This process is a content process, so it's clearly running in
   // multiprocess mode!
   nsDebugImpl::SetMultiprocessMode("Child");
 
   // When ContentChild is created, the observer service does not even exist.
@@ -636,28 +635,24 @@ ContentChild::Init(MessageLoop* aIOLoop,
 
 #ifdef MOZ_CRASHREPORTER
   CrashReporterClient::InitSingleton(this);
 #endif
 
   mID = aChildID;
   mIsForBrowser = aIsForBrowser;
 
-  SetProcessName(NS_LITERAL_STRING("Web Content"), true);
+  SetProcessName(NS_LITERAL_STRING("Web Content"));
 
   return true;
 }
 
 void
-ContentChild::SetProcessName(const nsAString& aName, bool aDontOverride)
+ContentChild::SetProcessName(const nsAString& aName)
 {
-  if (!mCanOverrideProcessName) {
-    return;
-  }
-
   char* name;
   if ((name = PR_GetEnv("MOZ_DEBUG_APP_PROCESS")) &&
     aName.EqualsASCII(name)) {
 #ifdef OS_POSIX
     printf_stderr("\n\nCHILDCHILDCHILDCHILD\n  [%s] debug me @%d\n\n", name,
                   getpid());
     sleep(30);
 #elif defined(OS_WIN)
@@ -666,20 +661,16 @@ ContentChild::SetProcessName(const nsASt
     NS_DebugBreak(NS_DEBUG_BREAK,
                  "Invoking NS_DebugBreak() to debug child process",
                  nullptr, __FILE__, __LINE__);
 #endif
   }
 
   mProcessName = aName;
   mozilla::ipc::SetThisProcessName(NS_LossyConvertUTF16toASCII(aName).get());
-
-  if (aDontOverride) {
-    mCanOverrideProcessName = false;
-  }
 }
 
 NS_IMETHODIMP
 ContentChild::ProvideWindow(mozIDOMWindowProxy* aParent,
                             uint32_t aChromeFlags,
                             bool aCalledFromJS,
                             bool aPositionSpecified,
                             bool aSizeSpecified,
@@ -2613,16 +2604,27 @@ ContentChild::RecvAppInfo(const nsCStrin
 }
 
 mozilla::ipc::IPCResult
 ContentChild::RecvRemoteType(const nsString& aRemoteType)
 {
   MOZ_ASSERT(DOMStringIsNull(mRemoteType));
 
   mRemoteType.Assign(aRemoteType);
+
+  // For non-default ("web") types, update the process name so about:memory's
+  // process names are more obvious.
+  if (aRemoteType.EqualsLiteral(FILE_REMOTE_TYPE)) {
+    SetProcessName(NS_LITERAL_STRING("file:// Content"));
+  } else if (aRemoteType.EqualsLiteral(EXTENSION_REMOTE_TYPE)) {
+    SetProcessName(NS_LITERAL_STRING("WebExtensions"));
+  } else if (aRemoteType.EqualsLiteral(LARGE_ALLOCATION_REMOTE_TYPE)) {
+    SetProcessName(NS_LITERAL_STRING("Large Allocation Web Content"));
+  }
+
   return IPC_OK();
 }
 
 const nsAString&
 ContentChild::GetRemoteType() const
 {
   return mRemoteType;
 }
diff --git a/dom/ipc/ContentChild.h b/dom/ipc/ContentChild.h
--- a/dom/ipc/ContentChild.h
+++ b/dom/ipc/ContentChild.h
@@ -117,17 +117,17 @@ public:
     return sSingleton;
   }
 
   const AppInfo& GetAppInfo()
   {
     return mAppInfo;
   }
 
-  void SetProcessName(const nsAString& aName, bool aDontOverride = false);
+  void SetProcessName(const nsAString& aName);
 
   void GetProcessName(nsAString& aName) const;
 
   void GetProcessName(nsACString& aName) const;
 
 #if defined(XP_MACOSX) && defined(MOZ_CONTENT_SANDBOX)
   void GetProfileDir(nsIFile** aProfileDir) const
   {
@@ -708,17 +708,16 @@ private:
    */
   uint32_t mMsaaID;
 #endif
 
   AppInfo mAppInfo;
 
   bool mIsForBrowser;
   nsString mRemoteType = VoidString();
-  bool mCanOverrideProcessName;
   bool mIsAlive;
   nsString mProcessName;
 
   static ContentChild* sSingleton;
 
   class ShutdownCanary;
   static StaticAutoPtr<ShutdownCanary> sShutdownCanary;
 
diff --git a/dom/ipc/ContentParent.h b/dom/ipc/ContentParent.h
--- a/dom/ipc/ContentParent.h
+++ b/dom/ipc/ContentParent.h
@@ -31,16 +31,19 @@
 #include "nsIDOMGeoPositionErrorCallback.h"
 #include "nsRefPtrHashtable.h"
 #include "PermissionMessageUtils.h"
 #include "DriverCrashGuard.h"
 
 #define CHILD_PROCESS_SHUTDOWN_MESSAGE NS_LITERAL_STRING("child-process-shutdown")
 
 // These must match the similar ones in E10SUtils.jsm.
+// Process names as reported by about:memory are defined in
+// ContentChild:RecvRemoteType.  Add your value there too or it will be called
+// "Web Content".
 #define DEFAULT_REMOTE_TYPE "web"
 #define FILE_REMOTE_TYPE "file"
 #define EXTENSION_REMOTE_TYPE "extension"
 
 // This must start with the DEFAULT_REMOTE_TYPE above.
 #define LARGE_ALLOCATION_REMOTE_TYPE "webLargeAllocation"
 
 class nsConsoleService;
