# HG changeset patch
# User David Keeler <dkeeler@mozilla.com>
# Date 1516833841 28800
# Node ID e21956fd51a330cad2301e49bb458e2ca94c5368
# Parent  ba03af0dcdd8c538d47715246edb6e5f59500f7e
bug 1421084 - part 4/4 - remove nsNSSShutDown.h and (hopefully) all references to it r=mt,ttaubert

MozReview-Commit-ID: 2mhvHsC5Nil

diff --git a/dom/crypto/CryptoKey.h b/dom/crypto/CryptoKey.h
--- a/dom/crypto/CryptoKey.h
+++ b/dom/crypto/CryptoKey.h
@@ -5,17 +5,16 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_CryptoKey_h
 #define mozilla_dom_CryptoKey_h
 
 #include "nsCycleCollectionParticipant.h"
 #include "nsWrapperCache.h"
 #include "nsIGlobalObject.h"
-#include "nsNSSShutDown.h"
 #include "pk11pub.h"
 #include "keyhi.h"
 #include "ScopedNSSTypes.h"
 #include "mozilla/ErrorResult.h"
 #include "mozilla/dom/CryptoBuffer.h"
 #include "mozilla/dom/KeyAlgorithmProxy.h"
 #include "js/StructuredClone.h"
 #include "js/TypeDecls.h"
@@ -53,19 +52,18 @@ Thus, internally, a key has the followin
 * uint32_t - flags for extractable, usage, type
 * KeyAlgorithm - the algorithm (which must serialize/deserialize itself)
 * The actual keys (which the CryptoKey must serialize)
 
 */
 
 struct JsonWebKey;
 
-class CryptoKey final : public nsISupports,
-                        public nsWrapperCache,
-                        public nsNSSShutDownObject
+class CryptoKey final : public nsISupports
+                      , public nsWrapperCache
 {
 public:
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS(CryptoKey)
 
   static const uint32_t CLEAR_EXTRACTABLE = 0xFFFFFFE;
   static const uint32_t EXTRACTABLE = 0x00000001;
 
diff --git a/dom/crypto/WebCryptoTask.h b/dom/crypto/WebCryptoTask.h
--- a/dom/crypto/WebCryptoTask.h
+++ b/dom/crypto/WebCryptoTask.h
@@ -8,17 +8,16 @@
 #define mozilla_dom_WebCryptoTask_h
 
 #include "ScopedNSSTypes.h"
 #include "mozilla/dom/CryptoKey.h"
 #include "mozilla/dom/DOMException.h"
 #include "mozilla/dom/Promise.h"
 #include "mozilla/dom/SubtleCryptoBinding.h"
 #include "nsIGlobalObject.h"
-#include "nsNSSShutDown.h"
 
 namespace mozilla {
 namespace dom {
 
 typedef ArrayBufferViewOrArrayBuffer CryptoOperationData;
 typedef ArrayBufferViewOrArrayBuffer KeyData;
 
 /*
@@ -52,18 +51,17 @@ Cleanup should execute regardless of wha
 */
 
 #define MAYBE_EARLY_FAIL(rv) \
 if (NS_FAILED(rv)) { \
   FailWithError(rv); \
   return; \
 }
 
-class WebCryptoTask : public CancelableRunnable,
-                      public nsNSSShutDownObject
+class WebCryptoTask : public CancelableRunnable
 {
 public:
   virtual void DispatchWithPromise(Promise* aResultPromise);
 
 protected:
   static WebCryptoTask* CreateEncryptDecryptTask(JSContext* aCx,
                            const ObjectOrString& aAlgorithm,
                            CryptoKey& aKey,
diff --git a/dom/media/webrtc/RTCCertificate.cpp b/dom/media/webrtc/RTCCertificate.cpp
--- a/dom/media/webrtc/RTCCertificate.cpp
+++ b/dom/media/webrtc/RTCCertificate.cpp
@@ -301,27 +301,16 @@ RTCCertificate::RTCCertificate(nsIGlobal
     : mGlobal(aGlobal),
       mPrivateKey(aPrivateKey),
       mCertificate(aCertificate),
       mAuthType(aAuthType),
       mExpires(aExpires)
 {
 }
 
-// This creates some interesting lifecycle consequences, since the DtlsIdentity
-// holds NSS objects, but does not implement nsNSSShutDownObject.
-
-// Unfortunately, the code that uses DtlsIdentity cannot always use that lock
-// due to external linkage requirements.  Therefore, the lock is held on this
-// object instead.  Consequently, the DtlsIdentity that this method returns must
-// have a lifetime that is strictly shorter than the RTCCertificate.
-//
-// RTCPeerConnection provides this guarantee by holding a strong reference to
-// the RTCCertificate.  It will cleanup any DtlsIdentity instances that it
-// creates before the RTCCertificate reference is released.
 RefPtr<DtlsIdentity>
 RTCCertificate::CreateDtlsIdentity() const
 {
   if (!mPrivateKey || !mCertificate) {
     return nullptr;
   }
   UniqueSECKEYPrivateKey key(SECKEY_CopyPrivateKey(mPrivateKey.get()));
   UniqueCERTCertificate cert(CERT_DupCertificate(mCertificate.get()));
diff --git a/dom/media/webrtc/RTCCertificate.h b/dom/media/webrtc/RTCCertificate.h
--- a/dom/media/webrtc/RTCCertificate.h
+++ b/dom/media/webrtc/RTCCertificate.h
@@ -5,17 +5,16 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_RTCCertificate_h
 #define mozilla_dom_RTCCertificate_h
 
 #include "nsCycleCollectionParticipant.h"
 #include "nsWrapperCache.h"
 #include "nsIGlobalObject.h"
-#include "nsNSSShutDown.h"
 #include "prtime.h"
 #include "sslt.h"
 #include "ScopedNSSTypes.h"
 
 #include "mozilla/ErrorResult.h"
 #include "mozilla/UniquePtr.h"
 #include "mozilla/RefPtr.h"
 #include "mozilla/dom/CryptoKey.h"
@@ -24,20 +23,18 @@
 #include "js/StructuredClone.h"
 #include "js/TypeDecls.h"
 
 namespace mozilla {
 namespace dom {
 
 class ObjectOrString;
 
-class RTCCertificate final
-    : public nsISupports,
-      public nsWrapperCache,
-      public nsNSSShutDownObject
+class RTCCertificate final : public nsISupports
+                           , public nsWrapperCache
 {
 public:
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS(RTCCertificate)
 
   // WebIDL method that implements RTCPeerConnection.generateCertificate.
   static already_AddRefed<Promise> GenerateCertificate(
       const GlobalObject& aGlobal, const ObjectOrString& aOptions,
diff --git a/dom/webauthn/U2FSoftTokenManager.h b/dom/webauthn/U2FSoftTokenManager.h
--- a/dom/webauthn/U2FSoftTokenManager.h
+++ b/dom/webauthn/U2FSoftTokenManager.h
@@ -4,28 +4,26 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_U2FSoftTokenManager_h
 #define mozilla_dom_U2FSoftTokenManager_h
 
 #include "mozilla/dom/U2FTokenTransport.h"
 #include "ScopedNSSTypes.h"
-#include "nsNSSShutDown.h"
 
 /*
  * U2FSoftTokenManager is a software implementation of a secure token manager
  * for the U2F and WebAuthn APIs.
  */
 
 namespace mozilla {
 namespace dom {
 
-class U2FSoftTokenManager final : public U2FTokenTransport,
-                                  public nsNSSShutDownObject
+class U2FSoftTokenManager final : public U2FTokenTransport
 {
 public:
   explicit U2FSoftTokenManager(uint32_t aCounter);
 
   virtual RefPtr<U2FRegisterPromise>
   Register(const nsTArray<WebAuthnScopedCredentialDescriptor>& aDescriptors,
            const nsTArray<uint8_t>& aApplication,
            const nsTArray<uint8_t>& aChallenge,
diff --git a/netwerk/base/BackgroundFileSaver.h b/netwerk/base/BackgroundFileSaver.h
--- a/netwerk/base/BackgroundFileSaver.h
+++ b/netwerk/base/BackgroundFileSaver.h
@@ -14,34 +14,32 @@
 
 #include "ScopedNSSTypes.h"
 #include "mozilla/Mutex.h"
 #include "nsCOMArray.h"
 #include "nsCOMPtr.h"
 #include "nsIAsyncOutputStream.h"
 #include "nsIBackgroundFileSaver.h"
 #include "nsIStreamListener.h"
-#include "nsNSSShutDown.h"
 #include "nsStreamUtils.h"
 #include "nsString.h"
 
 class nsIAsyncInputStream;
 class nsIThread;
 class nsIX509CertList;
 
 namespace mozilla {
 namespace net {
 
 class DigestOutputStream;
 
 ////////////////////////////////////////////////////////////////////////////////
 //// BackgroundFileSaver
 
-class BackgroundFileSaver : public nsIBackgroundFileSaver,
-                            public nsNSSShutDownObject
+class BackgroundFileSaver : public nsIBackgroundFileSaver
 {
 public:
   NS_DECL_NSIBACKGROUNDFILESAVER
 
   BackgroundFileSaver();
 
   /**
    * Initializes the pipe and the worker thread on XPCOM construction.
@@ -373,27 +371,26 @@ private:
    * Called on the control thread to suspend or resume the request.
    */
   nsresult NotifySuspendOrResume();
 };
 
 // A wrapper around nsIOutputStream, so that we can compute hashes on the
 // stream without copying and without polluting pristine NSS code with XPCOM
 // interfaces.
-class DigestOutputStream : public nsNSSShutDownObject,
-                           public nsIOutputStream
+class DigestOutputStream : public nsIOutputStream
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIOUTPUTSTREAM
   // Constructor. Neither parameter may be null. The caller owns both.
   DigestOutputStream(nsIOutputStream* outputStream, PK11Context* aContext);
 
 private:
-  ~DigestOutputStream() {}
+  virtual ~DigestOutputStream() {}
 
   // Calls to write are passed to this stream.
   nsCOMPtr<nsIOutputStream> mOutputStream;
   // Digest context used to compute the hash, owned by the caller.
   PK11Context* mDigestContext;
 
   // Don't accidentally copy construct.
   DigestOutputStream(const DigestOutputStream& d);
diff --git a/security/certverifier/ExtendedValidation.h b/security/certverifier/ExtendedValidation.h
--- a/security/certverifier/ExtendedValidation.h
+++ b/security/certverifier/ExtendedValidation.h
@@ -3,17 +3,16 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef ExtendedValidation_h
 #define ExtendedValidation_h
 
 #include "ScopedNSSTypes.h"
 #include "certt.h"
-#include "nsNSSShutDown.h"
 
 namespace mozilla { namespace pkix { struct CertPolicyId; } }
 
 namespace mozilla { namespace psm {
 
 nsresult LoadExtendedValidationInfo();
 
 /**
diff --git a/security/manager/ssl/ContentSignatureVerifier.h b/security/manager/ssl/ContentSignatureVerifier.h
--- a/security/manager/ssl/ContentSignatureVerifier.h
+++ b/security/manager/ssl/ContentSignatureVerifier.h
@@ -7,30 +7,28 @@
 
 #ifndef ContentSignatureVerifier_h
 #define ContentSignatureVerifier_h
 
 #include "cert.h"
 #include "CSTrustDomain.h"
 #include "nsIContentSignatureVerifier.h"
 #include "nsIStreamListener.h"
-#include "nsNSSShutDown.h"
 #include "nsString.h"
 #include "ScopedNSSTypes.h"
 
 // 45a5fe2f-c350-4b86-962d-02d5aaaa955a
 #define NS_CONTENTSIGNATUREVERIFIER_CID \
   { 0x45a5fe2f, 0xc350, 0x4b86, \
     { 0x96, 0x2d, 0x02, 0xd5, 0xaa, 0xaa, 0x95, 0x5a } }
 #define NS_CONTENTSIGNATUREVERIFIER_CONTRACTID \
     "@mozilla.org/security/contentsignatureverifier;1"
 
 class ContentSignatureVerifier final : public nsIContentSignatureVerifier
                                      , public nsIStreamListener
-                                     , public nsNSSShutDownObject
                                      , public nsIInterfaceRequestor
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSICONTENTSIGNATUREVERIFIER
   NS_DECL_NSIINTERFACEREQUESTOR
   NS_DECL_NSISTREAMLISTENER
   NS_DECL_NSIREQUESTOBSERVER
diff --git a/security/manager/ssl/CryptoTask.h b/security/manager/ssl/CryptoTask.h
--- a/security/manager/ssl/CryptoTask.h
+++ b/security/manager/ssl/CryptoTask.h
@@ -4,31 +4,29 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla__CryptoTask_h
 #define mozilla__CryptoTask_h
 
 #include "mozilla/Attributes.h"
 #include "nsThreadUtils.h"
-#include "nsNSSShutDown.h"
 
 namespace mozilla {
 
 /**
  * Frequently we need to run a task on a background thread without blocking
  * the main thread, and then call a callback on the main thread with the
  * result. This class provides the framework for that. Subclasses must:
  *
  *   (1) Override CalculateResult for the off-the-main-thread computation.
  *   (2) Override CallCallback() for the on-the-main-thread call of the
  *       callback.
  */
-class CryptoTask : public Runnable,
-                   public nsNSSShutDownObject
+class CryptoTask : public Runnable
 {
 public:
   template <size_t LEN>
   nsresult Dispatch(const char (&taskThreadName)[LEN])
   {
     static_assert(LEN <= 15,
                   "Thread name must be no more than 15 characters");
     return Dispatch(nsDependentCString(taskThreadName, LEN - 1));
diff --git a/security/manager/ssl/PKCS11ModuleDB.h b/security/manager/ssl/PKCS11ModuleDB.h
--- a/security/manager/ssl/PKCS11ModuleDB.h
+++ b/security/manager/ssl/PKCS11ModuleDB.h
@@ -3,27 +3,25 @@
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #ifndef PKCS11ModuleDB_h
 #define PKCS11ModuleDB_h
 
 #include "nsIPKCS11ModuleDB.h"
 
-#include "nsNSSShutDown.h"
 #include "nsString.h"
 
 namespace mozilla { namespace psm {
 
 #define NS_PKCS11MODULEDB_CID \
 { 0xff9fbcd7, 0x9517, 0x4334, \
   { 0xb9, 0x7a, 0xce, 0xed, 0x78, 0x90, 0x99, 0x74 }}
 
 class PKCS11ModuleDB : public nsIPKCS11ModuleDB
-                     , public nsNSSShutDownObject
 {
 public:
   PKCS11ModuleDB() {}
 
   NS_DECL_ISUPPORTS
   NS_DECL_NSIPKCS11MODULEDB
 
 protected:
diff --git a/security/manager/ssl/SSLServerCertVerification.cpp b/security/manager/ssl/SSLServerCertVerification.cpp
--- a/security/manager/ssl/SSLServerCertVerification.cpp
+++ b/security/manager/ssl/SSLServerCertVerification.cpp
@@ -120,17 +120,16 @@
 #include "nsIBadCertListener2.h"
 #include "nsICertOverrideService.h"
 #include "nsISiteSecurityService.h"
 #include "nsISocketProvider.h"
 #include "nsIThreadPool.h"
 #include "nsNSSCertificate.h"
 #include "nsNSSComponent.h"
 #include "nsNSSIOLayer.h"
-#include "nsNSSShutDown.h"
 #include "nsSSLStatus.h"
 #include "nsServiceManagerUtils.h"
 #include "nsString.h"
 #include "nsURLHelper.h"
 #include "nsXPCOMCIDInternal.h"
 #include "pkix/pkix.h"
 #include "pkix/pkixnss.h"
 #include "secerr.h"
diff --git a/security/manager/ssl/ScopedNSSTypes.h b/security/manager/ssl/ScopedNSSTypes.h
--- a/security/manager/ssl/ScopedNSSTypes.h
+++ b/security/manager/ssl/ScopedNSSTypes.h
@@ -76,17 +76,17 @@ typedef std::unique_ptr<Type, name##Dele
 
 MOZ_TYPE_SPECIFIC_UNIQUE_PTR_TEMPLATE(UniquePK11Context,
                                       PK11Context,
                                       internal::PK11_DestroyContext_true)
 
 /** A more convenient way of dealing with digests calculated into
  *  stack-allocated buffers. NSS must be initialized on the main thread before
  *  use, and the caller must ensure NSS isn't shut down, typically by
- *  subclassing nsNSSShutDownObject, while Digest is in use.
+ *  being within the lifetime of XPCOM.
  *
  * Typical usage, for digesting a buffer in memory:
  *
  *   nsCOMPtr<nsISupports> nssDummy = do_GetService("@mozilla.org/psm;1", &rv);
  *   Digest digest;
  *   nsresult rv = digest.DigestBuf(SEC_OID_SHA256, mybuffer, myBufferLen);
  *   NS_ENSURE_SUCCESS(rv, rv);
  *   rv = MapSECStatus(SomeNSSFunction(..., digest.get(), ...));
diff --git a/security/manager/ssl/SecretDecoderRing.h b/security/manager/ssl/SecretDecoderRing.h
--- a/security/manager/ssl/SecretDecoderRing.h
+++ b/security/manager/ssl/SecretDecoderRing.h
@@ -3,26 +3,24 @@
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef SecretDecoderRing_h
 #define SecretDecoderRing_h
 
 #include "nsISecretDecoderRing.h"
-#include "nsNSSShutDown.h"
 #include "nsString.h"
 
 #define NS_SECRETDECODERRING_CONTRACTID "@mozilla.org/security/sdr;1"
 
 #define NS_SECRETDECODERRING_CID \
   { 0x0c4f1ddc, 0x1dd2, 0x11b2, { 0x9d, 0x95, 0xf2, 0xfd, 0xf1, 0x13, 0x04, 0x4b } }
 
 class SecretDecoderRing : public nsISecretDecoderRing
-                        , public nsNSSShutDownObject
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSISECRETDECODERRING
 
   SecretDecoderRing() {}
 
 protected:
diff --git a/security/manager/ssl/TransportSecurityInfo.h b/security/manager/ssl/TransportSecurityInfo.h
--- a/security/manager/ssl/TransportSecurityInfo.h
+++ b/security/manager/ssl/TransportSecurityInfo.h
@@ -13,17 +13,16 @@
 #include "mozilla/BasePrincipal.h"
 #include "mozilla/Mutex.h"
 #include "mozilla/RefPtr.h"
 #include "nsDataHashtable.h"
 #include "nsIAssociatedContentSecurity.h"
 #include "nsIInterfaceRequestor.h"
 #include "nsISSLStatusProvider.h"
 #include "nsITransportSecurityInfo.h"
-#include "nsNSSShutDown.h"
 #include "nsSSLStatus.h"
 #include "nsString.h"
 #include "pkix/pkixtypes.h"
 
 namespace mozilla { namespace psm {
 
 enum class SSLErrorMessageType {
   OverridableCert = 1, // for *overridable* certificate errors
@@ -31,17 +30,16 @@ enum class SSLErrorMessageType {
 };
 
 class TransportSecurityInfo : public nsITransportSecurityInfo
                             , public nsIInterfaceRequestor
                             , public nsISSLStatusProvider
                             , public nsIAssociatedContentSecurity
                             , public nsISerializable
                             , public nsIClassInfo
-                            , public nsNSSShutDownObject
 {
 protected:
   virtual ~TransportSecurityInfo() {}
 public:
   TransportSecurityInfo();
 
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSITRANSPORTSECURITYINFO
diff --git a/security/manager/ssl/moz.build b/security/manager/ssl/moz.build
--- a/security/manager/ssl/moz.build
+++ b/security/manager/ssl/moz.build
@@ -61,17 +61,16 @@ TESTING_JS_MODULES.psm += [
 
 EXPORTS += [
     'CryptoTask.h',
     'nsClientAuthRemember.h',
     'nsNSSCallbacks.h',
     'nsNSSCertificate.h',
     'nsNSSComponent.h',
     'nsNSSHelper.h',
-    'nsNSSShutDown.h',
     'nsRandomGenerator.h',
     'nsSecurityHeaderParser.h',
     'NSSErrorsService.h',
     'ScopedNSSTypes.h',
     'SharedCertVerifier.h',
 ]
 
 EXPORTS.mozilla += [
diff --git a/security/manager/ssl/nsCryptoHash.h b/security/manager/ssl/nsCryptoHash.h
--- a/security/manager/ssl/nsCryptoHash.h
+++ b/security/manager/ssl/nsCryptoHash.h
@@ -6,45 +6,45 @@
 
 #ifndef nsCryptoHash_h
 #define nsCryptoHash_h
 
 #include "ScopedNSSTypes.h"
 #include "hasht.h"
 #include "nsICryptoHMAC.h"
 #include "nsICryptoHash.h"
-#include "nsNSSShutDown.h"
 #include "secmodt.h"
 
 class nsIInputStream;
 
 #define NS_CRYPTO_HASH_CID {0x36a1d3b3, 0xd886, 0x4317, {0x96, 0xff, 0x87, 0xb0, 0x00, 0x5c, 0xfe, 0xf7}}
 #define NS_CRYPTO_HMAC_CID {0xa496d0a2, 0xdff7, 0x4e23, {0xbd, 0x65, 0x1c, 0xa7, 0x42, 0xfa, 0x17, 0x8a}}
 
-class nsCryptoHash final : public nsICryptoHash, public nsNSSShutDownObject
+class nsCryptoHash final : public nsICryptoHash
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSICRYPTOHASH
 
   nsCryptoHash();
 
 private:
-  ~nsCryptoHash() {}
+  virtual ~nsCryptoHash() {}
 
   mozilla::UniqueHASHContext mHashContext;
   bool mInitialized;
 };
 
-class nsCryptoHMAC : public nsICryptoHMAC, public nsNSSShutDownObject
+class nsCryptoHMAC : public nsICryptoHMAC
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSICRYPTOHMAC
 
   nsCryptoHMAC();
 
 private:
-  ~nsCryptoHMAC() {}
+  virtual ~nsCryptoHMAC() {}
+
   mozilla::UniquePK11Context mHMACContext;
 };
 
 #endif // nsCryptoHash_h
diff --git a/security/manager/ssl/nsDataSignatureVerifier.h b/security/manager/ssl/nsDataSignatureVerifier.h
--- a/security/manager/ssl/nsDataSignatureVerifier.h
+++ b/security/manager/ssl/nsDataSignatureVerifier.h
@@ -1,26 +1,24 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsDataSignatureVerifier_h
 #define nsDataSignatureVerifier_h
 
 #include "nsIDataSignatureVerifier.h"
-#include "nsNSSShutDown.h"
 
 #define NS_DATASIGNATUREVERIFIER_CID \
     { 0x296d76aa, 0x275b, 0x4f3c, \
     { 0xaf, 0x8a, 0x30, 0xa4, 0x02, 0x6c, 0x18, 0xfc } }
 #define NS_DATASIGNATUREVERIFIER_CONTRACTID \
     "@mozilla.org/security/datasignatureverifier;1"
 
 class nsDataSignatureVerifier final : public nsIDataSignatureVerifier
-                                    , public nsNSSShutDownObject
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIDATASIGNATUREVERIFIER
 
   nsDataSignatureVerifier() {}
 
 private:
diff --git a/security/manager/ssl/nsKeyModule.h b/security/manager/ssl/nsKeyModule.h
--- a/security/manager/ssl/nsKeyModule.h
+++ b/security/manager/ssl/nsKeyModule.h
@@ -2,30 +2,28 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsKeyModule_h
 #define nsKeyModule_h
 
 #include "ScopedNSSTypes.h"
 #include "nsIKeyModule.h"
-#include "nsNSSShutDown.h"
 #include "pk11pub.h"
 
 #define NS_KEYMODULEOBJECT_CID   \
 { 0x9d383ddd, 0x6856, 0x4187, {0x84, 0x85, 0xf3, 0x61, 0x95, 0xb2, 0x9a, 0x0e} }
 #define NS_KEYMODULEOBJECT_CONTRACTID "@mozilla.org/security/keyobject;1"
 
 #define NS_KEYMODULEOBJECTFACTORY_CID   \
 { 0x2a35dd47, 0xb026, 0x4e8d, {0xb6, 0xb7, 0x57, 0x40, 0xf6, 0x1a, 0xb9, 0x02} }
 #define NS_KEYMODULEOBJECTFACTORY_CONTRACTID \
 "@mozilla.org/security/keyobjectfactory;1"
 
 class nsKeyObject final : public nsIKeyObject
-                        , public nsNSSShutDownObject
 {
 public:
   nsKeyObject();
 
   NS_DECL_ISUPPORTS
   NS_DECL_NSIKEYOBJECT
 
 private:
@@ -34,17 +32,16 @@ private:
   // Disallow copy constructor
   nsKeyObject(nsKeyObject&);
 
   UniquePK11SymKey mSymKey;
 };
 
 
 class nsKeyObjectFactory final : public nsIKeyObjectFactory
-                               , public nsNSSShutDownObject
 {
 public:
   nsKeyObjectFactory() {}
 
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIKEYOBJECTFACTORY
 
 private:
diff --git a/security/manager/ssl/nsKeygenHandler.h b/security/manager/ssl/nsKeygenHandler.h
--- a/security/manager/ssl/nsKeygenHandler.h
+++ b/security/manager/ssl/nsKeygenHandler.h
@@ -8,32 +8,30 @@
 #define nsKeygenHandler_h
 
 #include "ScopedNSSTypes.h"
 #include "keythi.h"
 #include "nsCOMPtr.h"
 #include "nsError.h"
 #include "nsIFormProcessor.h"
 #include "nsIInterfaceRequestor.h"
-#include "nsNSSShutDown.h"
 #include "nsString.h"
 #include "nsTArray.h"
 #include "secmodt.h"
 
 nsresult GetSlotWithMechanism(uint32_t mechanism,
                               nsIInterfaceRequestor* ctx,
                               PK11SlotInfo** retSlot);
 
 #define DEFAULT_RSA_KEYGEN_PE 65537L
 #define DEFAULT_RSA_KEYGEN_ALG SEC_OID_PKCS1_MD5_WITH_RSA_ENCRYPTION
 
 mozilla::UniqueSECItem DecodeECParams(const char* curve);
 
 class nsKeygenFormProcessor : public nsIFormProcessor
-                            , public nsNSSShutDownObject
 {
 public:
   nsKeygenFormProcessor();
   nsresult Init();
 
   virtual nsresult ProcessValue(nsIDOMHTMLElement* aElement,
                                 const nsAString& aName,
                                 nsAString& aValue) override;
diff --git a/security/manager/ssl/nsKeygenThread.cpp b/security/manager/ssl/nsKeygenThread.cpp
--- a/security/manager/ssl/nsKeygenThread.cpp
+++ b/security/manager/ssl/nsKeygenThread.cpp
@@ -8,17 +8,16 @@
 
 #include "mozilla/Assertions.h"
 #include "mozilla/DebugOnly.h"
 
 #include "GeckoProfiler.h"
 #include "PSMRunnable.h"
 #include "nsCOMPtr.h"
 #include "nsIObserver.h"
-#include "nsNSSShutDown.h"
 #include "nsThreadUtils.h"
 #include "pk11func.h"
 
 using namespace mozilla;
 using namespace mozilla::psm;
 
 NS_IMPL_ISUPPORTS(nsKeygenThread, nsIKeygenThread)
 
diff --git a/security/manager/ssl/nsNSSCallbacks.cpp b/security/manager/ssl/nsNSSCallbacks.cpp
--- a/security/manager/ssl/nsNSSCallbacks.cpp
+++ b/security/manager/ssl/nsNSSCallbacks.cpp
@@ -718,17 +718,16 @@ ShowProtectedAuthPrompt(PK11SlotInfo* sl
       NS_RELEASE(protectedAuthRunnable);
     }
   }
 
   return protAuthRetVal;
 }
 
 class PK11PasswordPromptRunnable : public SyncRunnableBase
-                                 , public nsNSSShutDownObject
 {
 public:
   PK11PasswordPromptRunnable(PK11SlotInfo* slot,
                              nsIInterfaceRequestor* ir)
     : mResult(nullptr),
       mSlot(slot),
       mIR(ir)
   {
diff --git a/security/manager/ssl/nsNSSCertValidity.h b/security/manager/ssl/nsNSSCertValidity.h
--- a/security/manager/ssl/nsNSSCertValidity.h
+++ b/security/manager/ssl/nsNSSCertValidity.h
@@ -3,20 +3,18 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsNSSCertValidity_h
 #define nsNSSCertValidity_h
 
 #include "DateTimeFormat.h"
 #include "ScopedNSSTypes.h"
 #include "nsIX509CertValidity.h"
-#include "nsNSSShutDown.h"
 
 class nsX509CertValidity : public nsIX509CertValidity
-                         , public nsNSSShutDownObject
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIX509CERTVALIDITY
 
   explicit nsX509CertValidity(const mozilla::UniqueCERTCertificate& cert);
 
 protected:
diff --git a/security/manager/ssl/nsNSSCertificate.h b/security/manager/ssl/nsNSSCertificate.h
--- a/security/manager/ssl/nsNSSCertificate.h
+++ b/security/manager/ssl/nsNSSCertificate.h
@@ -11,28 +11,26 @@
 #include "nsCOMPtr.h"
 #include "nsIASN1Object.h"
 #include "nsIClassInfo.h"
 #include "nsISerializable.h"
 #include "nsISimpleEnumerator.h"
 #include "nsIX509Cert.h"
 #include "nsIX509CertDB.h"
 #include "nsIX509CertList.h"
-#include "nsNSSShutDown.h"
 
 namespace mozilla { namespace pkix { class DERArray; } }
 
 class nsAutoString;
 class nsINSSComponent;
 class nsIASN1Sequence;
 
-class nsNSSCertificate final : public nsIX509Cert,
-                               public nsISerializable,
-                               public nsIClassInfo,
-                               public nsNSSShutDownObject
+class nsNSSCertificate final : public nsIX509Cert
+                             , public nsISerializable
+                             , public nsIClassInfo
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIX509CERT
   NS_DECL_NSISERIALIZABLE
   NS_DECL_NSICLASSINFO
 
   explicit nsNSSCertificate(CERTCertificate* cert);
@@ -62,19 +60,18 @@ private:
 namespace mozilla {
 
 SECStatus ConstructCERTCertListFromReversedDERArray(
             const mozilla::pkix::DERArray& certArray,
             /*out*/ mozilla::UniqueCERTCertList& certList);
 
 } // namespace mozilla
 
-class nsNSSCertList: public nsIX509CertList,
-                     public nsISerializable,
-                     public nsNSSShutDownObject
+class nsNSSCertList: public nsIX509CertList
+                   , public nsISerializable
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIX509CERTLIST
   NS_DECL_NSISERIALIZABLE
 
   // certList is adopted
   explicit nsNSSCertList(mozilla::UniqueCERTCertList certList);
@@ -88,18 +85,17 @@ private:
    virtual ~nsNSSCertList() {}
 
    mozilla::UniqueCERTCertList mCertList;
 
    nsNSSCertList(const nsNSSCertList&) = delete;
    void operator=(const nsNSSCertList&) = delete;
 };
 
-class nsNSSCertListEnumerator: public nsISimpleEnumerator,
-                               public nsNSSShutDownObject
+class nsNSSCertListEnumerator : public nsISimpleEnumerator
 {
 public:
    NS_DECL_THREADSAFE_ISUPPORTS
    NS_DECL_NSISIMPLEENUMERATOR
 
    explicit nsNSSCertListEnumerator(const mozilla::UniqueCERTCertList& certList);
 private:
    virtual ~nsNSSCertListEnumerator() {}
diff --git a/security/manager/ssl/nsNSSCertificateDB.cpp b/security/manager/ssl/nsNSSCertificateDB.cpp
--- a/security/manager/ssl/nsNSSCertificateDB.cpp
+++ b/security/manager/ssl/nsNSSCertificateDB.cpp
@@ -25,17 +25,16 @@
 #include "nsIPrefBranch.h"
 #include "nsIPrefService.h"
 #include "nsIPrompt.h"
 #include "nsNSSCertHelper.h"
 #include "nsNSSCertTrust.h"
 #include "nsNSSCertificate.h"
 #include "nsNSSComponent.h"
 #include "nsNSSHelper.h"
-#include "nsNSSShutDown.h"
 #include "nsPKCS12Blob.h"
 #include "nsPromiseFlatString.h"
 #include "nsProxyRelease.h"
 #include "nsReadableUtils.h"
 #include "nsThreadUtils.h"
 #include "nspr.h"
 #include "pkix/Time.h"
 #include "pkix/pkixnss.h"
diff --git a/security/manager/ssl/nsNSSCertificateDB.h b/security/manager/ssl/nsNSSCertificateDB.h
--- a/security/manager/ssl/nsNSSCertificateDB.h
+++ b/security/manager/ssl/nsNSSCertificateDB.h
@@ -7,24 +7,22 @@
 
 #include "ScopedNSSTypes.h"
 #include "certt.h"
 #include "mozilla/Mutex.h"
 #include "mozilla/NotNull.h"
 #include "mozilla/RefPtr.h"
 #include "mozilla/UniquePtr.h"
 #include "nsIX509CertDB.h"
-#include "nsNSSShutDown.h"
 #include "nsString.h"
 
 class nsCString;
 class nsIArray;
 
 class nsNSSCertificateDB final : public nsIX509CertDB
-                               , public nsNSSShutDownObject
 
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIX509CERTDB
 
   // Use this function to generate a default nickname for a user
   // certificate that is to be imported onto a token.
diff --git a/security/manager/ssl/nsNSSComponent.cpp b/security/manager/ssl/nsNSSComponent.cpp
--- a/security/manager/ssl/nsNSSComponent.cpp
+++ b/security/manager/ssl/nsNSSComponent.cpp
@@ -37,17 +37,16 @@
 #include "nsIProperties.h"
 #include "nsISiteSecurityService.h"
 #include "nsITokenPasswordDialogs.h"
 #include "nsIWindowWatcher.h"
 #include "nsIXULRuntime.h"
 #include "nsLiteralString.h"
 #include "nsNSSCertificateDB.h"
 #include "nsNSSHelper.h"
-#include "nsNSSShutDown.h"
 #include "nsPrintfCString.h"
 #include "nsServiceManagerUtils.h"
 #include "nsThreadUtils.h"
 #include "nsXULAppAPI.h"
 #include "nss.h"
 #include "p12plcy.h"
 #include "pkix/pkixnss.h"
 #include "secerr.h"
@@ -1045,17 +1044,16 @@ nsNSSComponent::TrustLoaded3rdPartyRoots
     MOZ_LOG(gPIPNSSLog, LogLevel::Debug,
             ("couldn't trust family safety certificate for TLS server auth"));
   }
   return NS_OK;
 }
 #endif // XP_WIN
 
 class LoadLoadableRootsTask final : public Runnable
-                                  , public nsNSSShutDownObject
 {
 public:
   explicit LoadLoadableRootsTask(nsNSSComponent* nssComponent)
     : Runnable("LoadLoadableRootsTask")
     , mNSSComponent(nssComponent)
   {
     MOZ_ASSERT(nssComponent);
   }
diff --git a/security/manager/ssl/nsNSSComponent.h b/security/manager/ssl/nsNSSComponent.h
--- a/security/manager/ssl/nsNSSComponent.h
+++ b/security/manager/ssl/nsNSSComponent.h
@@ -87,18 +87,16 @@ public:
   NS_IMETHOD BlockUntilLoadableRootsLoaded() = 0;
 
   virtual ::already_AddRefed<mozilla::psm::SharedCertVerifier>
     GetDefaultCertVerifier() = 0;
 };
 
 NS_DEFINE_STATIC_IID_ACCESSOR(nsINSSComponent, NS_INSSCOMPONENT_IID)
 
-class nsNSSShutDownList;
-
 // Implementation of the PSM component interface.
 class nsNSSComponent final : public nsINSSComponent
                            , public nsIObserver
 {
 public:
   // LoadLoadableRootsTask updates mLoadableRootsLoaded and
   // mLoadableRootsLoadedResult and then signals mLoadableRootsLoadedMonitor.
   friend class LoadLoadableRootsTask;
diff --git a/security/manager/ssl/nsNSSHelper.h b/security/manager/ssl/nsNSSHelper.h
--- a/security/manager/ssl/nsNSSHelper.h
+++ b/security/manager/ssl/nsNSSHelper.h
@@ -4,17 +4,16 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef NSS_HELPER_
 #define NSS_HELPER_
 
 #include "nsIInterfaceRequestor.h"
 #include "nsIInterfaceRequestorUtils.h"
-#include "nsNSSShutDown.h"
 #include "pk11func.h"
 
 //
 // Implementation of an nsIInterfaceRequestor for use
 // as context for NSS calls
 //
 class PipUIContext : public nsIInterfaceRequestor
 {
diff --git a/security/manager/ssl/nsNSSShutDown.h b/security/manager/ssl/nsNSSShutDown.h
deleted file mode 100644
--- a/security/manager/ssl/nsNSSShutDown.h
+++ /dev/null
@@ -1,20 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#ifndef nsNSSShutDown_h
-#define nsNSSShutDown_h
-
-// This is the vestigial remains of the old NSS shutdown tracking
-// infrastructure. It will be removed entirely in bug 1421084 when we've
-// demonstrated that shutting down NSS only when all non-main-threads have been
-// joined is feasible (and beneficial).
-
-class nsNSSShutDownObject
-{
-public:
-  nsNSSShutDownObject() {}
-  virtual ~nsNSSShutDownObject() {}
-};
-
-#endif // nsNSSShutDown_h
diff --git a/security/manager/ssl/nsNTLMAuthModule.cpp b/security/manager/ssl/nsNTLMAuthModule.cpp
--- a/security/manager/ssl/nsNTLMAuthModule.cpp
+++ b/security/manager/ssl/nsNTLMAuthModule.cpp
@@ -20,17 +20,16 @@
 #include "mozilla/Sprintf.h"
 #include "mozilla/Telemetry.h"
 #include "nsCOMPtr.h"
 #include "nsComponentManagerUtils.h"
 #include "nsICryptoHMAC.h"
 #include "nsICryptoHash.h"
 #include "nsIKeyModule.h"
 #include "nsKeyModule.h"
-#include "nsNSSShutDown.h"
 #include "nsNativeCharsetUtils.h"
 #include "nsNetCID.h"
 #include "nsUnicharUtils.h"
 #include "pk11pub.h"
 #include "prsystem.h"
 
 static bool sNTLMv1Forced = false;
 static mozilla::LazyLogModule sNTLMLog("NTLM");
diff --git a/security/manager/ssl/nsPK11TokenDB.h b/security/manager/ssl/nsPK11TokenDB.h
--- a/security/manager/ssl/nsPK11TokenDB.h
+++ b/security/manager/ssl/nsPK11TokenDB.h
@@ -7,23 +7,21 @@
 #ifndef nsPK11TokenDB_h
 #define nsPK11TokenDB_h
 
 #include "nsCOMPtr.h"
 #include "nsIPK11Token.h"
 #include "nsIPK11TokenDB.h"
 #include "nsISupports.h"
 #include "nsNSSHelper.h"
-#include "nsNSSShutDown.h"
 #include "nsString.h"
 #include "pk11func.h"
 #include "ScopedNSSTypes.h"
 
-class nsPK11Token : public nsIPK11Token,
-                    public nsNSSShutDownObject
+class nsPK11Token : public nsIPK11Token
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIPK11TOKEN
 
   explicit nsPK11Token(PK11SlotInfo *slot);
 
 protected:
@@ -42,17 +40,16 @@ private:
   mozilla::UniquePK11SlotInfo mSlot;
   int mSeries;
   nsCOMPtr<nsIInterfaceRequestor> mUIContext;
   nsresult GetAttributeHelper(const nsACString& attribute,
                       /*out*/ nsACString& xpcomOutParam);
 };
 
 class nsPK11TokenDB : public nsIPK11TokenDB
-                    , public nsNSSShutDownObject
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIPK11TOKENDB
 
   nsPK11TokenDB() {}
 
 protected:
diff --git a/security/manager/ssl/nsPKCS11Slot.h b/security/manager/ssl/nsPKCS11Slot.h
--- a/security/manager/ssl/nsPKCS11Slot.h
+++ b/security/manager/ssl/nsPKCS11Slot.h
@@ -6,22 +6,20 @@
 
 #ifndef nsPKCS11Slot_h
 #define nsPKCS11Slot_h
 
 #include "ScopedNSSTypes.h"
 #include "nsIPKCS11Module.h"
 #include "nsIPKCS11Slot.h"
 #include "nsISupports.h"
-#include "nsNSSShutDown.h"
 #include "nsString.h"
 #include "pk11func.h"
 
-class nsPKCS11Slot : public nsIPKCS11Slot,
-                     public nsNSSShutDownObject
+class nsPKCS11Slot : public nsIPKCS11Slot
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIPKCS11SLOT
 
   explicit nsPKCS11Slot(PK11SlotInfo* slot);
 
 protected:
@@ -35,18 +33,17 @@ private:
   nsCString mSlotFWVersion;
   int mSeries;
 
   nsresult refreshSlotInfo();
   nsresult GetAttributeHelper(const nsACString& attribute,
                       /*out*/ nsACString& xpcomOutParam);
 };
 
-class nsPKCS11Module : public nsIPKCS11Module,
-                       public nsNSSShutDownObject
+class nsPKCS11Module : public nsIPKCS11Module
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIPKCS11MODULE
 
   explicit nsPKCS11Module(SECMODModule* module);
 
 protected:
diff --git a/security/manager/ssl/nsPKCS12Blob.h b/security/manager/ssl/nsPKCS12Blob.h
--- a/security/manager/ssl/nsPKCS12Blob.h
+++ b/security/manager/ssl/nsPKCS12Blob.h
@@ -2,30 +2,29 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsPKCS12Blob_h
 #define nsPKCS12Blob_h
 
 #include "nsCOMPtr.h"
 #include "nsIMutableArray.h"
-#include "nsNSSShutDown.h"
 #include "nsString.h"
 #include "p12.h"
 #include "seccomon.h"
 
 class nsIFile;
 class nsIX509Cert;
 
 //
 // nsPKCS12Blob
 //
 // Class for importing/exporting PKCS#12 blobs
 //
-class nsPKCS12Blob : public nsNSSShutDownObject
+class nsPKCS12Blob
 {
 public:
   nsPKCS12Blob();
   virtual ~nsPKCS12Blob() {}
 
   // PKCS#12 Import
   nsresult ImportFromFile(nsIFile *file);
 
diff --git a/security/manager/ssl/nsRandomGenerator.h b/security/manager/ssl/nsRandomGenerator.h
--- a/security/manager/ssl/nsRandomGenerator.h
+++ b/security/manager/ssl/nsRandomGenerator.h
@@ -2,26 +2,24 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsRandomGenerator_h
 #define nsRandomGenerator_h
 
 #include "mozilla/Attributes.h"
 #include "nsIRandomGenerator.h"
-#include "nsNSSShutDown.h"
 
 #define NS_RANDOMGENERATOR_CID \
   {0xbe65e2b7, 0xfe46, 0x4e0f, {0x88, 0xe0, 0x4b, 0x38, 0x5d, 0xb4, 0xd6, 0x8a}}
 
 #define NS_RANDOMGENERATOR_CONTRACTID \
   "@mozilla.org/security/random-generator;1"
 
 class nsRandomGenerator final : public nsIRandomGenerator
-                              , public nsNSSShutDownObject
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIRANDOMGENERATOR
 
 private:
   ~nsRandomGenerator() {}
 };
diff --git a/services/crypto/component/IdentityCryptoService.cpp b/services/crypto/component/IdentityCryptoService.cpp
--- a/services/crypto/component/IdentityCryptoService.cpp
+++ b/services/crypto/component/IdentityCryptoService.cpp
@@ -2,17 +2,16 @@
 /* vim: set ts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsIIdentityCryptoService.h"
 #include "mozilla/ModuleUtils.h"
 #include "nsServiceManagerUtils.h"
-#include "nsNSSShutDown.h"
 #include "nsIThread.h"
 #include "nsThreadUtils.h"
 #include "nsCOMPtr.h"
 #include "nsProxyRelease.h"
 #include "nsString.h"
 #include "mozilla/ArrayUtils.h" // ArrayLength
 #include "mozilla/Base64.h"
 #include "ScopedNSSTypes.h"
@@ -42,27 +41,27 @@ HexEncode(const SECItem * it, nsACString
     *p++ = digits[it->data[i] >> 4];
     *p++ = digits[it->data[i] & 0x0f];
   }
 }
 
 #define DSA_KEY_TYPE_STRING (NS_LITERAL_CSTRING("DS160"))
 #define RSA_KEY_TYPE_STRING (NS_LITERAL_CSTRING("RS256"))
 
-class KeyPair : public nsIIdentityKeyPair, public nsNSSShutDownObject
+class KeyPair : public nsIIdentityKeyPair
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIIDENTITYKEYPAIR
 
   KeyPair(SECKEYPrivateKey* aPrivateKey, SECKEYPublicKey* aPublicKey,
           nsIEventTarget* aOperationThread);
 
 private:
-  ~KeyPair() override
+  virtual ~KeyPair()
   {
     if (mPrivateKey) {
       SECKEY_DestroyPrivateKey(mPrivateKey);
     }
     if (mPublicKey) {
       SECKEY_DestroyPublicKey(mPublicKey);
     }
   }
@@ -72,40 +71,36 @@ private:
   nsCOMPtr<nsIEventTarget> mThread;
 
   KeyPair(const KeyPair &) = delete;
   void operator=(const KeyPair &) = delete;
 };
 
 NS_IMPL_ISUPPORTS(KeyPair, nsIIdentityKeyPair)
 
-class KeyGenRunnable : public Runnable, public nsNSSShutDownObject
+class KeyGenRunnable : public Runnable
 {
 public:
   NS_DECL_NSIRUNNABLE
 
   KeyGenRunnable(KeyType keyType, nsIIdentityKeyGenCallback * aCallback,
                  nsIEventTarget* aOperationThread);
 
 private:
-  ~KeyGenRunnable() override
-  {
-  }
-
   const KeyType mKeyType; // in
   nsMainThreadPtrHandle<nsIIdentityKeyGenCallback> mCallback; // in
   nsresult mRv; // out
   nsCOMPtr<nsIIdentityKeyPair> mKeyPair; // out
   nsCOMPtr<nsIEventTarget> mThread;
 
   KeyGenRunnable(const KeyGenRunnable &) = delete;
   void operator=(const KeyGenRunnable &) = delete;
 };
 
-class SignRunnable : public Runnable, public nsNSSShutDownObject
+class SignRunnable : public Runnable
 {
 public:
   NS_DECL_NSIRUNNABLE
 
   SignRunnable(const nsACString & textToSign, SECKEYPrivateKey * privateKey,
                nsIIdentitySignCallback * aCallback);
 
 private:
