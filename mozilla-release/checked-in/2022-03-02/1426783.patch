# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1515187055 21600
#      Fri Jan 05 15:17:35 2018 -0600
# Node ID fe271a2b9503021b9660256f261eece7004d1d5e
# Parent  622061dbf70ea35dc8910237669990ac3050c7eb
Bug 1426783 - Fix error handling in deserialization of invalid typed arrays. r=sfink, a=ritu.

diff --git a/js/src/vm/StructuredClone.cpp b/js/src/vm/StructuredClone.cpp
--- a/js/src/vm/StructuredClone.cpp
+++ b/js/src/vm/StructuredClone.cpp
@@ -2006,18 +2006,27 @@ JSStructuredCloneReader::readSharedArray
  * endianness-conversion while reading.
  */
 bool
 JSStructuredCloneReader::readV1ArrayBuffer(uint32_t arrayType, uint32_t nelems,
                                            MutableHandleValue vp)
 {
     MOZ_ASSERT(arrayType <= Scalar::Uint8Clamped);
 
-    uint32_t nbytes = nelems << TypedArrayShift(static_cast<Scalar::Type>(arrayType));
-    JSObject* obj = ArrayBufferObject::create(context(), nbytes);
+    mozilla::CheckedInt<size_t> nbytes =
+        mozilla::CheckedInt<size_t>(nelems) *
+        TypedArrayElemSize(static_cast<Scalar::Type>(arrayType));
+    if (!nbytes.isValid() || nbytes.value() > UINT32_MAX) {
+        JS_ReportErrorNumberASCII(context(), GetErrorMessage, nullptr,
+                                  JSMSG_SC_BAD_SERIALIZED_DATA,
+                                  "invalid typed array size");
+        return false;
+    }
+
+    JSObject* obj = ArrayBufferObject::create(context(), nbytes.value());
     if (!obj)
         return false;
     vp.setObject(*obj);
     ArrayBufferObject& buffer = obj->as<ArrayBufferObject>();
     MOZ_ASSERT(buffer.byteLength() == nbytes);
 
     switch (arrayType) {
       case Scalar::Int8:
