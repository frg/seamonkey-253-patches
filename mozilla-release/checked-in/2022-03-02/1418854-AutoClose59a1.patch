# HG changeset patch
# User Michal Novotny <michal.novotny@gmail.com>
# Date 1512611286 18000
#      Wed Dec 06 20:48:06 2017 -0500
# Node ID 4186bb70043e1c56d2211d83273d47d631963891
# Parent  de4d9955af650c807795d644b0fe443b10d257c4
Bug 1418854 - Race condition in AutoClose. r=honzab, a=jcristau

diff --git a/netwerk/base/AutoClose.h b/netwerk/base/AutoClose.h
--- a/netwerk/base/AutoClose.h
+++ b/netwerk/base/AutoClose.h
@@ -3,74 +3,75 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_net_AutoClose_h
 #define mozilla_net_AutoClose_h
 
 #include "nsCOMPtr.h"
+#include "mozilla/Mutex.h"
 
 namespace mozilla { namespace net {
 
 // Like an nsAutoPtr for XPCOM streams (e.g. nsIAsyncInputStream) and other
 // refcounted classes that need to have the Close() method called explicitly
 // before they are destroyed.
 template <typename T>
 class AutoClose
 {
 public:
-  AutoClose() { }
+  AutoClose() : mMutex("net::AutoClose.mMutex") { }
   ~AutoClose(){
-    Close();
+    CloseAndRelease();
   }
 
-  explicit operator bool() const
+  explicit operator bool()
   {
+    MutexAutoLock lock(mMutex);
     return mPtr;
   }
 
   already_AddRefed<T> forget()
   {
+    MutexAutoLock lock(mMutex);
     return mPtr.forget();
   }
 
   void takeOver(nsCOMPtr<T> & rhs)
   {
-    Close();
-    mPtr = rhs.forget();
-  }
-
-  void takeOver(AutoClose<T> & rhs)
-  {
-    Close();
-    mPtr = rhs.mPtr.forget();
+    already_AddRefed<T> other = rhs.forget();
+    TakeOverInternal(&other);
   }
 
   void CloseAndRelease()
   {
-    Close();
-    mPtr = nullptr;
-  }
-
-  T* operator->() const MOZ_NO_ADDREF_RELEASE_ON_RETURN
-  {
-    return mPtr.operator->();
+    TakeOverInternal(nullptr);
   }
 
 private:
-  void Close()
+  void TakeOverInternal(already_AddRefed<T> *aOther)
   {
-    if (mPtr) {
-      mPtr->Close();
+    nsCOMPtr<T> ptr;
+    {
+      MutexAutoLock lock(mMutex);
+      ptr.swap(mPtr);
+      if (aOther) {
+        mPtr = *aOther;
+      }
+    }
+
+    if (ptr) {
+      ptr->Close();
     }
   }
 
   void operator=(const AutoClose<T> &) = delete;
   AutoClose(const AutoClose<T> &) = delete;
 
   nsCOMPtr<T> mPtr;
+  Mutex mMutex;
 };
 
 } // namespace net
 } // namespace mozilla
 
 #endif // mozilla_net_AutoClose_h
