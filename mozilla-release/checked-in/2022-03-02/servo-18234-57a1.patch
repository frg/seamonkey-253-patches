# HG changeset patch
# User Boris Chiou <boris.chiou@gmail.com>
# Date 1504091012 18000
#      Wed Aug 30 06:03:32 2017 -0500
# Node ID 10bd7a6f14b8ea02ad024e47ed207edb53655553
# Parent  c58bbcff388ec0a7c442487af5e771f0e16f8792
servo: Merge #18234 - stylo: Bug 1390039 - Implement compute_distance for mismatched transform lists (from BorisChiou:stylo/transform/distance_mismatch); r=birtles,nox

Implement ComputeSquaredDistance for mismatched transform lists.
In order to do this, we have to convert a transform list into a 3d matrix,
so I move the code from layout module into style module for reusing it.

---
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [X] These changes fix [Bug 1390039](https://bugzilla.mozilla.org/show_bug.cgi?id=1390039).
- [X] These changes do not require tests because this is a Gecko feature and I add many tests in Gecko already.

Source-Repo: https://github.com/servo/servo
Source-Revision: 3fa5d83ab798a9f1f88a73bf8618e6d7ccbb4b64

diff --git a/servo/components/style/properties/helpers/animated_properties.mako.rs b/servo/components/style/properties/helpers/animated_properties.mako.rs
--- a/servo/components/style/properties/helpers/animated_properties.mako.rs
+++ b/servo/components/style/properties/helpers/animated_properties.mako.rs
@@ -3,17 +3,16 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 <%namespace name="helpers" file="/helpers.mako.rs" />
 
 <% from data import to_idl_name, SYSTEM_FONT_LONGHANDS %>
 
 use app_units::Au;
 use cssparser::Parser;
-use euclid::Point3D;
 #[cfg(feature = "gecko")] use gecko_bindings::bindings::RawServoAnimationValueMap;
 #[cfg(feature = "gecko")] use gecko_bindings::structs::RawGeckoGfxMatrix4x4;
 #[cfg(feature = "gecko")] use gecko_bindings::structs::nsCSSPropertyID;
 #[cfg(feature = "gecko")] use gecko_bindings::sugar::ownership::{HasFFI, HasSimpleFFI};
 #[cfg(feature = "gecko")] use gecko_string_cache::Atom;
 use itertools::{EitherOrBoth, Itertools};
 use properties::{CSSWideKeyword, PropertyDeclaration};
 use properties::longhands;
@@ -49,16 +48,17 @@ use values::computed::{Angle, BorderCorn
 use values::computed::{ClipRect, Context, ComputedUrl, ComputedValueAsSpecified};
 use values::computed::{LengthOrPercentage, LengthOrPercentageOrAuto};
 use values::computed::{LengthOrPercentageOrNone, MaxLength, NonNegativeAu};
 use values::computed::{NonNegativeNumber, Number, NumberOrPercentage, Percentage};
 use values::computed::{PositiveIntegerOrAuto, ToComputedValue};
 #[cfg(feature = "gecko")] use values::computed::MozLength;
 use values::computed::length::{NonNegativeLengthOrAuto, NonNegativeLengthOrNormal};
 use values::computed::length::NonNegativeLengthOrPercentage;
+use values::computed::transform::DirectionVector;
 use values::distance::{ComputeSquaredDistance, SquaredDistance};
 use values::generics::NonNegative;
 use values::generics::effects::Filter;
 use values::generics::position as generic_position;
 use values::generics::svg::{SVGLength,  SvgLengthOrPercentageOrNumber, SVGPaint};
 use values::generics::svg::{SVGPaintKind, SVGStrokeDashArray, SVGOpacity};
 
 /// https://drafts.csswg.org/css-transitions/#animtype-repeatable-list
@@ -954,17 +954,17 @@ impl ToAnimatedZero for TransformOperati
                     ty.to_animated_zero()?,
                     tz.to_animated_zero()?,
                 ))
             },
             TransformOperation::Scale(..) => {
                 Ok(TransformOperation::Scale(1.0, 1.0, 1.0))
             },
             TransformOperation::Rotate(x, y, z, a) => {
-                let (x, y, z, _) = get_normalized_vector_and_angle(x, y, z, a);
+                let (x, y, z, _) = TransformList::get_normalized_vector_and_angle(x, y, z, a);
                 Ok(TransformOperation::Rotate(x, y, z, Angle::zero()))
             },
             TransformOperation::Perspective(..) |
             TransformOperation::AccumulateMatrix { .. } |
             TransformOperation::InterpolateMatrix { .. } => {
                 // Perspective: We convert a perspective function into an equivalent
                 //     ComputedMatrix, and then decompose/interpolate/recompose these matrices.
                 // AccumulateMatrix/InterpolateMatrix: We do interpolation on
@@ -1031,18 +1031,20 @@ impl Animate for TransformOperation {
                     animate_multiplicative_factor(*fy, *ty, procedure)?,
                     animate_multiplicative_factor(*fz, *tz, procedure)?,
                 ))
             },
             (
                 &TransformOperation::Rotate(fx, fy, fz, fa),
                 &TransformOperation::Rotate(tx, ty, tz, ta),
             ) => {
-                let (fx, fy, fz, fa) = get_normalized_vector_and_angle(fx, fy, fz, fa);
-                let (tx, ty, tz, ta) = get_normalized_vector_and_angle(tx, ty, tz, ta);
+                let (fx, fy, fz, fa) =
+                    TransformList::get_normalized_vector_and_angle(fx, fy, fz, fa);
+                let (tx, ty, tz, ta) =
+                    TransformList::get_normalized_vector_and_angle(tx, ty, tz, ta);
                 if (fx, fy, fz) == (tx, ty, tz) {
                     let ia = fa.animate(&ta, procedure)?;
                     Ok(TransformOperation::Rotate(fx, fy, fz, ia))
                 } else {
                     let matrix_f = rotate_to_matrix(fx, fy, fz, fa);
                     let matrix_t = rotate_to_matrix(tx, ty, tz, ta);
                     Ok(TransformOperation::Matrix(
                         matrix_f.animate(&matrix_t, procedure)?,
@@ -1445,39 +1447,34 @@ pub struct MatrixDecomposed3D {
     /// The skew component of the transformation.
     pub skew: Skew,
     /// The perspective component of the transformation.
     pub perspective: Perspective,
     /// The quaternion used to represent the rotation.
     pub quaternion: Quaternion,
 }
 
-/// A wrapper of Point3D to represent the direction vector (rotate axis) for Rotate3D.
-#[derive(Clone, Copy, Debug, PartialEq)]
-#[cfg_attr(feature = "servo", derive(HeapSizeOf))]
-pub struct DirectionVector(Point3D<f64>);
-
 impl Quaternion {
     /// Return a quaternion from a unit direction vector and angle (unit: radian).
     #[inline]
     fn from_direction_and_angle(vector: &DirectionVector, angle: f64) -> Self {
-        debug_assert!((vector.length() - 1.).abs() < 0.0001f64,
-                       "Only accept an unit direction vector to create a quaternion");
+        debug_assert!((vector.length() - 1.).abs() < 0.0001,
+                      "Only accept an unit direction vector to create a quaternion");
         // Reference:
         // https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation
         //
         // if the direction axis is (x, y, z) = xi + yj + zk,
         // and the angle is |theta|, this formula can be done using
         // an extension of Euler's formula:
         //   q = cos(theta/2) + (xi + yj + zk)(sin(theta/2))
         //     = cos(theta/2) +
         //       x*sin(theta/2)i + y*sin(theta/2)j + z*sin(theta/2)k
-        Quaternion(vector.0.x * (angle / 2.).sin(),
-                   vector.0.y * (angle / 2.).sin(),
-                   vector.0.z * (angle / 2.).sin(),
+        Quaternion(vector.x as f64 * (angle / 2.).sin(),
+                   vector.y as f64 * (angle / 2.).sin(),
+                   vector.z as f64 * (angle / 2.).sin(),
                    (angle / 2.).cos())
     }
 
     /// Calculate the dot product.
     #[inline]
     fn dot(&self, other: &Self) -> f64 {
         self.0 * other.0 + self.1 * other.1 + self.2 * other.2 + self.3 * other.3
     }
@@ -1489,57 +1486,16 @@ impl ComputeSquaredDistance for Quaterni
         // Use quaternion vectors to get the angle difference. Both q1 and q2 are unit vectors,
         // so we can get their angle difference by:
         // cos(theta/2) = (q1 dot q2) / (|q1| * |q2|) = q1 dot q2.
         let distance = self.dot(other).max(-1.0).min(1.0).acos() * 2.0;
         Ok(SquaredDistance::Value(distance * distance))
     }
 }
 
-impl DirectionVector {
-    /// Create a DirectionVector.
-    #[inline]
-    fn new(x: f32, y: f32, z: f32) -> Self {
-        DirectionVector(Point3D::new(x as f64, y as f64, z as f64))
-    }
-
-    /// Return the normalized direction vector.
-    #[inline]
-    fn normalize(&mut self) -> bool {
-        let len = self.length();
-        if len > 0. {
-            self.0.x = self.0.x / len;
-            self.0.y = self.0.y / len;
-            self.0.z = self.0.z / len;
-            true
-        } else {
-            false
-        }
-    }
-
-    /// Get the length of this vector.
-    #[inline]
-    fn length(&self) -> f64 {
-        self.0.to_array().iter().fold(0f64, |sum, v| sum + v * v).sqrt()
-    }
-}
-
-/// Return the normalized direction vector and its angle.
-// A direction vector that cannot be normalized, such as [0,0,0], will cause the
-// rotation to not be applied. i.e. Use an identity matrix or rotate3d(0, 0, 1, 0).
-fn get_normalized_vector_and_angle(x: f32, y: f32, z: f32, angle: Angle)
-                                   -> (f32, f32, f32, Angle) {
-    let mut vector = DirectionVector::new(x, y, z);
-    if vector.normalize() {
-        (vector.0.x as f32, vector.0.y as f32, vector.0.z as f32, angle)
-    } else {
-        (0., 0., 1., Angle::zero())
-    }
-}
-
 /// Decompose a 3D matrix.
 /// https://drafts.csswg.org/css-transforms/#decomposing-a-3d-matrix
 fn decompose_3d_matrix(mut matrix: ComputedMatrix) -> Result<MatrixDecomposed3D, ()> {
     // Normalize the matrix.
     if matrix.m44 == 0.0 {
         return Err(());
     }
 
@@ -2196,18 +2152,20 @@ impl ComputeSquaredDistance for Transfor
                     fy.compute_squared_distance(&ty)? +
                     fz.compute_squared_distance(&tz)?,
                 )
             },
             (
                 &TransformOperation::Rotate(fx, fy, fz, fa),
                 &TransformOperation::Rotate(tx, ty, tz, ta),
             ) => {
-                let (fx, fy, fz, angle1) = get_normalized_vector_and_angle(fx, fy, fz, fa);
-                let (tx, ty, tz, angle2) = get_normalized_vector_and_angle(tx, ty, tz, ta);
+                let (fx, fy, fz, angle1) =
+                    TransformList::get_normalized_vector_and_angle(fx, fy, fz, fa);
+                let (tx, ty, tz, angle2) =
+                    TransformList::get_normalized_vector_and_angle(tx, ty, tz, ta);
                 if (fx, fy, fz) == (tx, ty, tz) {
                     angle1.compute_squared_distance(&angle2)
                 } else {
                     let v1 = DirectionVector::new(fx, fy, fz);
                     let v2 = DirectionVector::new(tx, ty, tz);
                     let q1 = Quaternion::from_direction_and_angle(&v1, angle1.radians64());
                     let q2 = Quaternion::from_direction_and_angle(&v2, angle2.radians64());
                     q1.compute_squared_distance(&q2)
@@ -2244,29 +2202,38 @@ impl ComputeSquaredDistance for Transfor
             _ => Err(()),
         }
     }
 }
 
 impl ComputeSquaredDistance for TransformList {
     #[inline]
     fn compute_squared_distance(&self, other: &Self) -> Result<SquaredDistance, ()> {
-        let this = self.0.as_ref().map_or(&[][..], |l| l);
-        let other = other.0.as_ref().map_or(&[][..], |l| l);
+        let list1 = self.0.as_ref().map_or(&[][..], |l| l);
+        let list2 = other.0.as_ref().map_or(&[][..], |l| l);
 
-        this.iter().zip_longest(other).map(|it| {
+        let squared_dist: Result<SquaredDistance, _> = list1.iter().zip_longest(list2).map(|it| {
             match it {
                 EitherOrBoth::Both(this, other) => {
                     this.compute_squared_distance(other)
                 },
                 EitherOrBoth::Left(list) | EitherOrBoth::Right(list) => {
                     list.to_animated_zero()?.compute_squared_distance(list)
                 },
             }
-        }).sum()
+        }).sum();
+
+        // Roll back to matrix interpolation if there is any Err(()) in the transform lists, such
+        // as mismatched transform functions.
+        if let Err(_) = squared_dist {
+            let matrix1: ComputedMatrix = self.to_transform_3d_matrix(None).ok_or(())?.into();
+            let matrix2: ComputedMatrix = other.to_transform_3d_matrix(None).ok_or(())?.into();
+            return matrix1.compute_squared_distance(&matrix2);
+        }
+        squared_dist
     }
 }
 
 impl ToAnimatedZero for TransformList {
     #[inline]
     fn to_animated_zero(&self) -> Result<Self, ()> {
         match self.0 {
             None => Ok(TransformList(None)),
diff --git a/servo/components/style/values/computed/angle.rs b/servo/components/style/values/computed/angle.rs
--- a/servo/components/style/values/computed/angle.rs
+++ b/servo/components/style/values/computed/angle.rs
@@ -1,14 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Computed angles.
 
+use euclid::Radians;
 use std::{f32, f64, fmt};
 use std::f64::consts::PI;
 use style_traits::ToCss;
 use values::CSSFloat;
 use values::animated::{Animate, Procedure};
 use values::distance::{ComputeSquaredDistance, SquaredDistance};
 
 /// A computed angle.
@@ -94,8 +95,15 @@ impl ToCss for Angle {
         match *self {
             Angle::Degree(val) => write(val, "deg"),
             Angle::Gradian(val) => write(val, "grad"),
             Angle::Radian(val) => write(val, "rad"),
             Angle::Turn(val) => write(val, "turn"),
         }
     }
 }
+
+impl From<Angle> for Radians<CSSFloat> {
+    #[inline]
+    fn from(a: Angle) -> Self {
+        Radians::new(a.radians())
+    }
+}
diff --git a/servo/components/style/values/computed/transform.rs b/servo/components/style/values/computed/transform.rs
--- a/servo/components/style/values/computed/transform.rs
+++ b/servo/components/style/values/computed/transform.rs
@@ -1,27 +1,172 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Computed types for CSS values that are related to transformations.
 
-use values::computed::{Length, LengthOrPercentage, Number, Percentage};
+use app_units::Au;
+use euclid::{Rect, Transform3D, Vector3D};
+use properties::longhands::transform::computed_value::{ComputedOperation, ComputedMatrix};
+use properties::longhands::transform::computed_value::T as TransformList;
+use std::f32;
+use super::CSSFloat;
+use values::computed::{Angle, Length, LengthOrPercentage, Number, Percentage};
 use values::generics::transform::TimingFunction as GenericTimingFunction;
 use values::generics::transform::TransformOrigin as GenericTransformOrigin;
 
 /// The computed value of a CSS `<transform-origin>`
 pub type TransformOrigin = GenericTransformOrigin<LengthOrPercentage, LengthOrPercentage, Length>;
 
 /// A computed timing function.
 pub type TimingFunction = GenericTimingFunction<u32, Number>;
 
+/// A vector to represent the direction vector (rotate axis) for Rotate3D.
+pub type DirectionVector = Vector3D<CSSFloat>;
+
 impl TransformOrigin {
     /// Returns the initial computed value for `transform-origin`.
     #[inline]
     pub fn initial_value() -> Self {
         Self::new(
             LengthOrPercentage::Percentage(Percentage(0.5)),
             LengthOrPercentage::Percentage(Percentage(0.5)),
             Length::from_px(0),
         )
     }
 }
+
+impl From<ComputedMatrix> for Transform3D<CSSFloat> {
+    #[inline]
+    fn from(m: ComputedMatrix) -> Self {
+        Transform3D::row_major(
+            m.m11, m.m12, m.m13, m.m14,
+            m.m21, m.m22, m.m23, m.m24,
+            m.m31, m.m32, m.m33, m.m34,
+            m.m41, m.m42, m.m43, m.m44)
+    }
+}
+
+impl From<Transform3D<CSSFloat>> for ComputedMatrix {
+    #[inline]
+    fn from(m: Transform3D<CSSFloat>) -> Self {
+        ComputedMatrix {
+            m11: m.m11, m12: m.m12, m13: m.m13, m14: m.m14,
+            m21: m.m21, m22: m.m22, m23: m.m23, m24: m.m24,
+            m31: m.m31, m32: m.m32, m33: m.m33, m34: m.m34,
+            m41: m.m41, m42: m.m42, m43: m.m43, m44: m.m44
+        }
+    }
+}
+
+impl TransformList {
+    /// Return the equivalent 3d matrix of this transform list.
+    /// If |reference_box| is None, we will drop the percent part from translate because
+    /// we can resolve it without the layout info.
+    pub fn to_transform_3d_matrix(&self, reference_box: Option<&Rect<Au>>)
+                                  -> Option<Transform3D<CSSFloat>> {
+        let mut transform = Transform3D::identity();
+        let list = match self.0.as_ref() {
+            Some(list) => list,
+            None => return None,
+        };
+
+        let extract_pixel_length = |lop: &LengthOrPercentage| {
+            match *lop {
+                LengthOrPercentage::Length(au) => au.to_f32_px(),
+                LengthOrPercentage::Percentage(_) => 0.,
+                LengthOrPercentage::Calc(calc) => calc.length().to_f32_px(),
+            }
+        };
+
+        for operation in list {
+            let matrix = match *operation {
+                ComputedOperation::Rotate(ax, ay, az, theta) => {
+                    let theta = Angle::from_radians(2.0f32 * f32::consts::PI - theta.radians());
+                    let (ax, ay, az, theta) =
+                        Self::get_normalized_vector_and_angle(ax, ay, az, theta);
+                    Transform3D::create_rotation(ax, ay, az, theta.into())
+                }
+                ComputedOperation::Perspective(d) => {
+                    Self::create_perspective_matrix(d)
+                }
+                ComputedOperation::Scale(sx, sy, sz) => {
+                    Transform3D::create_scale(sx, sy, sz)
+                }
+                ComputedOperation::Translate(tx, ty, tz) => {
+                    let (tx, ty) = match reference_box {
+                        Some(relative_border_box) => {
+                            (tx.to_used_value(relative_border_box.size.width).to_f32_px(),
+                             ty.to_used_value(relative_border_box.size.height).to_f32_px())
+                        },
+                        None => {
+                            // If we don't have reference box, we cannot resolve the used value,
+                            // so only retrieve the length part. This will be used for computing
+                            // distance without any layout info.
+                            (extract_pixel_length(&tx), extract_pixel_length(&ty))
+                        }
+                    };
+                    let tz = tz.to_f32_px();
+                    Transform3D::create_translation(tx, ty, tz)
+                }
+                ComputedOperation::Matrix(m) => {
+                    m.into()
+                }
+                ComputedOperation::MatrixWithPercents(_) => {
+                    // `-moz-transform` is not implemented in Servo yet.
+                    unreachable!()
+                }
+                ComputedOperation::Skew(theta_x, theta_y) => {
+                    Transform3D::create_skew(theta_x.into(), theta_y.into())
+                }
+                ComputedOperation::InterpolateMatrix { .. } |
+                ComputedOperation::AccumulateMatrix { .. } => {
+                    // TODO: Convert InterpolateMatrix/AccmulateMatrix into a valid Transform3D by
+                    // the reference box and do interpolation on these two Transform3D matrices.
+                    // Both Gecko and Servo don't support this for computing distance, and Servo
+                    // doesn't support animations on InterpolateMatrix/AccumulateMatrix, so
+                    // return None.
+                    return None;
+                }
+            };
+
+            transform = transform.pre_mul(&matrix);
+        }
+
+        Some(transform)
+    }
+
+    /// Return the transform matrix from a perspective length.
+    #[inline]
+    pub fn create_perspective_matrix(d: Au) -> Transform3D<f32> {
+        // TODO(gw): The transforms spec says that perspective length must
+        // be positive. However, there is some confusion between the spec
+        // and browser implementations as to handling the case of 0 for the
+        // perspective value. Until the spec bug is resolved, at least ensure
+        // that a provided perspective value of <= 0.0 doesn't cause panics
+        // and behaves as it does in other browsers.
+        // See https://lists.w3.org/Archives/Public/www-style/2016Jan/0020.html for more details.
+        let d = d.to_f32_px();
+        if d <= 0.0 {
+            Transform3D::identity()
+        } else {
+            Transform3D::create_perspective(d)
+        }
+    }
+
+    /// Return the normalized direction vector and its angle for Rotate3D.
+    pub fn get_normalized_vector_and_angle(x: f32, y: f32, z: f32, angle: Angle)
+                                           -> (f32, f32, f32, Angle) {
+        use euclid::approxeq::ApproxEq;
+        use euclid::num::Zero;
+        let vector = DirectionVector::new(x, y, z);
+        if vector.square_length().approx_eq(&f32::zero()) {
+            // https://www.w3.org/TR/css-transforms-1/#funcdef-rotate3d
+            // A direction vector that cannot be normalized, such as [0, 0, 0], will cause the
+            // rotation to not be applied, so we use identity matrix (i.e. rotate3d(0, 0, 1, 0)).
+            (0., 0., 1., Angle::zero())
+        } else {
+            let vector = vector.normalize();
+            (vector.x, vector.y, vector.z, angle)
+        }
+    }
+}
