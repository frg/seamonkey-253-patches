# HG changeset patch
# User Jeff Muizelaar <jmuizelaar@mozilla.com>
# Date 1527815958 14400
#      Thu May 31 21:19:18 2018 -0400
# Node ID f1a745f8c42dc5ef18081d4c29cc8a1fbf0dc1ea
# Parent  8d7b80bf33e441c1a6d021c4d9a11996fdd137c0
Bug 1452375 - ssse3-scaler: handle init failure. r=sotaro, a=abillings

diff --git a/gfx/2d/ssse3-scaler.c b/gfx/2d/ssse3-scaler.c
--- a/gfx/2d/ssse3-scaler.c
+++ b/gfx/2d/ssse3-scaler.c
@@ -32,16 +32,17 @@
 
 #include <stdlib.h>
 #include <mmintrin.h>
 #include <xmmintrin.h>
 #include <emmintrin.h>
 #include <tmmintrin.h>
 #include <stdint.h>
 #include <assert.h>
+#include "ssse3-scaler.h"
 
 typedef int32_t                 pixman_fixed_16_16_t;
 typedef pixman_fixed_16_16_t    pixman_fixed_t;
 #define pixman_fixed_1                  (pixman_int_to_fixed(1))
 #define pixman_fixed_to_int(f)          ((int) ((f) >> 16))
 #define pixman_int_to_fixed(i)          ((pixman_fixed_t) ((i) << 16))
 #define pixman_double_to_fixed(d)       ((pixman_fixed_t) ((d) * 65536.0))
 typedef struct pixman_vector pixman_vector_t;
@@ -500,17 +501,17 @@ fail:
      * we don't guarantee any particular rendering.
      */
     iter->fini = NULL;
 }
 
 /* scale the src from src_width/height to dest_width/height drawn
  * into the rectangle x,y width,height
  * src_stride and dst_stride are 4 byte units */
-void ssse3_scale_data(uint32_t *src, int src_width, int src_height, int src_stride,
+bool ssse3_scale_data(uint32_t *src, int src_width, int src_height, int src_stride,
                       uint32_t *dest, int dest_width, int dest_height,
                       int dest_stride,
                       int x, int y,
                       int width, int height)
 {
     //XXX: assert(src_width > 1)
     pixman_transform_t transform = {
         { { pixman_fixed_1, 0, 0 },
@@ -546,16 +547,21 @@ void ssse3_scale_data(uint32_t *src, int
     iter.x = x;
     iter.y = y;
     iter.width = width;
     iter.height = src_height;
     iter.buffer = dest;
     iter.data = NULL;
 
     ssse3_bilinear_cover_iter_init(&iter);
+
+    if (!iter.fini)
+      return false;
+
     if (iter.data) {
         for (int iy = 0; iy < height; iy++) {
             ssse3_fetch_bilinear_cover(&iter, NULL);
             iter.buffer += dest_stride;
         }
         ssse3_bilinear_cover_iter_fini(&iter);
     }
+    return true;
 }
diff --git a/gfx/2d/ssse3-scaler.h b/gfx/2d/ssse3-scaler.h
--- a/gfx/2d/ssse3-scaler.h
+++ b/gfx/2d/ssse3-scaler.h
@@ -1,20 +1,22 @@
 #/* -*- Mode: C++; tab-width: 20; indent-tabs-mode: nil; c-basic-offset: 2 -*-
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef MOZILLA_GFX_2D_SSSE3_SCALER_H_
 #define MOZILLA_GFX_2D_SSSE3_SCALER_H_
 
+#include <stdbool.h>
+
 #ifdef __cplusplus
 extern "C" {
 #endif
-void ssse3_scale_data(uint32_t *src, int src_width, int src_height,
+bool ssse3_scale_data(uint32_t *src, int src_width, int src_height,
                 int src_stride,
                 uint32_t *dest, int dest_width, int dest_height,
                 int dest_rowstride,
                 int x, int y,
                 int width, int height);
 #ifdef __cplusplus
 }
 #endif
diff --git a/gfx/layers/basic/BasicCompositor.cpp b/gfx/layers/basic/BasicCompositor.cpp
--- a/gfx/layers/basic/BasicCompositor.cpp
+++ b/gfx/layers/basic/BasicCompositor.cpp
@@ -551,25 +551,25 @@ AttemptVideoScale(TextureSourceBasic* aS
     }
 
     fillRect = fillRect.Intersect(IntRect(IntPoint(0, 0), aDest->GetSize()));
     IntPoint offset = fillRect.TopLeft() - dstRect.TopLeft();
 
     RefPtr<DataSourceSurface> srcSource = aSource->GetSurface(aDest)->GetDataSurface();
     DataSourceSurface::ScopedMap mapSrc(srcSource, DataSourceSurface::READ);
 
-    ssse3_scale_data((uint32_t*)mapSrc.GetData(), srcSource->GetSize().width, srcSource->GetSize().height,
-                     mapSrc.GetStride()/4,
-                     ((uint32_t*)dstData) + fillRect.x + (dstStride / 4) * fillRect.y, dstRect.width, dstRect.height,
-                     dstStride / 4,
-                     offset.x, offset.y,
-                     fillRect.width, fillRect.height);
+    bool success = ssse3_scale_data((uint32_t*)mapSrc.GetData(), srcSource->GetSize().width, srcSource->GetSize().height,
+                                    mapSrc.GetStride()/4,
+                                    ((uint32_t*)dstData) + fillRect.x + (dstStride / 4) * fillRect.y, dstRect.width, dstRect.height,
+                                    dstStride / 4,
+                                    offset.x, offset.y,
+                                    fillRect.width, fillRect.height);
 
     aDest->ReleaseBits(dstData);
-    return true;
+    return success;
   } else
 #endif // MOZILLA_SSE_HAVE_CPUID_DETECTION
     return false;
 }
 
 static bool
 AttemptVideoConvertAndScale(TextureSource* aSource, const SourceSurface* aSourceMask,
                             gfx::Float aOpacity, CompositionOp aBlendMode,
