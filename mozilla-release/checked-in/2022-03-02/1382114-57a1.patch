# HG changeset patch
# User Daosheng Mu <daoshengmu@gmail.com>
# Date 1501574606 -28800
# Node ID 66d84fd556ff3457be3226e6e010c35e755f65a9
# Parent  1977484bba4ef75e164fe86d60d721b2cd24fa28
Bug 1382114 - Update openvr to 1.0.9; r=kip

MozReview-Commit-ID: BthbIIWFywD

diff --git a/gfx/vr/openvr/README.mozilla b/gfx/vr/openvr/README.mozilla
--- a/gfx/vr/openvr/README.mozilla
+++ b/gfx/vr/openvr/README.mozilla
@@ -1,53 +1,53 @@
-This directory contains files from the OpenVR SDK, version 1.0.8.
-
-This SDK contains the OpenVR API interface headers and functions to load the
-OpenVR runtime libraries which actually implement the functionality. The
-loading functions parse a .json file in a pre-defined location on the
-end-user's machine to get details used to bind the correct runtime library.
-The OpenVR implementation ensures forward and backwards compatibility as of
-the current version.
-
-Updated versions of the OpenVR SDK are available on Github:
-
-https://github.com/ValveSoftware/openvr
-
-
-We only use some files from the SDK:
-
-- We strip out files such as C# headers, plain C API versions, and files
-  needed only when implementing OpenVR drivers.
-
-- CMake related files, such as CMakeLists.txt are skipped as we use moz.build
-  files to configure the library.
-
-- The "src/jsoncpp.cpp" file and the "src/json" directory can be skipped. OpenVR
-  uses the jsoncpp library, which we have already imported elsewhere.
-
-
-Steps to update the library:
-
-- Copy "README.md" from the root of the openvr repo to the "gfx/vr/openvr" directory.
-
-- Copy "headers/openvr.h" to "gfx/vr/openvr/headers" directory. The other files
-  in this directory can be ignored.
-
-- The rest of the files in the "src" directory and the "src/vrcommon" are copied
-  to the "gfx/vr/openvr/src" directory.
-
-- Update "gfx/vr/openvr/moz.build" when files are added or removed.
-
-- Update the "strtools_public.h" and "strtools_public.cpp" files, commenting out
-  the "Uint64ToString", "wcsncpy_s", and "strncpy_s" functions.
-  The "Uint64ToString" function name conflicts with another used in Gecko and
-  the "errno_t" return type returned by the other functions is not defined in
-  Mozilla's macOS continuous integration build environments.  Fortunately, the
-  OpenVR SDK does not use these functions.
-
-- Replace the #define VR_INTERFACE in openvr.h to avoid extern'ing the functions.
-  Unlike the usual OpenVR API builds, we are not building a separate dll.
-
-- Update this README.mozilla file with the new OpenVR SDK version and any
-  additional steps needed for newer versions.
-
-- Ensure that any changes made within the OpenVR files have comments including
-  the string "Mozilla" and reference this file for easy identification.
+This directory contains files from the OpenVR SDK, version 1.0.8.
+
+This SDK contains the OpenVR API interface headers and functions to load the
+OpenVR runtime libraries which actually implement the functionality. The
+loading functions parse a .json file in a pre-defined location on the
+end-user's machine to get details used to bind the correct runtime library.
+The OpenVR implementation ensures forward and backwards compatibility as of
+the current version.
+
+Updated versions of the OpenVR SDK are available on Github:
+
+https://github.com/ValveSoftware/openvr
+
+
+We only use some files from the SDK:
+
+- We strip out files such as C# headers, plain C API versions, and files
+  needed only when implementing OpenVR drivers.
+
+- CMake related files, such as CMakeLists.txt are skipped as we use moz.build
+  files to configure the library.
+
+- The "src/jsoncpp.cpp" file and the "src/json" directory can be skipped. OpenVR
+  uses the jsoncpp library, which we have already imported elsewhere.
+
+
+Steps to update the library:
+
+- Copy "README.md" from the root of the openvr repo to the "gfx/vr/openvr" directory.
+
+- Copy "headers/openvr.h" to "gfx/vr/openvr/headers" directory. The other files
+  in this directory can be ignored.
+
+- The rest of the files in the "src" directory and the "src/vrcommon" are copied
+  to the "gfx/vr/openvr/src" directory.
+
+- Update "gfx/vr/openvr/moz.build" when files are added or removed.
+
+- Update the "strtools_public.h" and "strtools_public.cpp" files, commenting out
+  the "Uint64ToString", "wcsncpy_s", and "strncpy_s" functions.
+  The "Uint64ToString" function name conflicts with another used in Gecko and
+  the "errno_t" return type returned by the other functions is not defined in
+  Mozilla's macOS continuous integration build environments.  Fortunately, the
+  OpenVR SDK does not use these functions.
+
+- Replace the #define VR_INTERFACE in openvr.h to avoid extern'ing the functions.
+  Unlike the usual OpenVR API builds, we are not building a separate dll.
+
+- Update this README.mozilla file with the new OpenVR SDK version and any
+  additional steps needed for newer versions.
+
+- Ensure that any changes made within the OpenVR files have comments including
+  the string "Mozilla" and reference this file for easy identification.
diff --git a/gfx/vr/openvr/headers/openvr.h b/gfx/vr/openvr/headers/openvr.h
--- a/gfx/vr/openvr/headers/openvr.h
+++ b/gfx/vr/openvr/headers/openvr.h
@@ -310,16 +310,17 @@ enum ETrackedDeviceProperty
 	Prop_DisplayMCImageWidth_Int32				= 2038,
 	Prop_DisplayMCImageHeight_Int32				= 2039,
 	Prop_DisplayMCImageNumChannels_Int32		= 2040,
 	Prop_DisplayMCImageData_Binary				= 2041,
 	Prop_SecondsFromPhotonsToVblank_Float		= 2042,
 	Prop_DriverDirectModeSendsVsyncEvents_Bool	= 2043,
 	Prop_DisplayDebugMode_Bool					= 2044,
 	Prop_GraphicsAdapterLuid_Uint64				= 2045,
+	Prop_DriverProvidedChaperonePath_String		= 2048,
 
 	// Properties that are unique to TrackedDeviceClass_Controller
 	Prop_AttachedDeviceId_String				= 3000,
 	Prop_SupportedButtons_Uint64				= 3001,
 	Prop_Axis0Type_Int32						= 3002, // Return value is of type EVRControllerAxisType
 	Prop_Axis1Type_Int32						= 3003, // Return value is of type EVRControllerAxisType
 	Prop_Axis2Type_Int32						= 3004, // Return value is of type EVRControllerAxisType
 	Prop_Axis3Type_Int32						= 3005, // Return value is of type EVRControllerAxisType
@@ -459,16 +460,18 @@ enum EVREventType
 	VREvent_TrackedDeviceUserInteractionEnded	= 104,
 	VREvent_IpdChanged					= 105,
 	VREvent_EnterStandbyMode			= 106,
 	VREvent_LeaveStandbyMode			= 107,
 	VREvent_TrackedDeviceRoleChanged	= 108,
 	VREvent_WatchdogWakeUpRequested		= 109,
 	VREvent_LensDistortionChanged		= 110,
 	VREvent_PropertyChanged				= 111,
+	VREvent_WirelessDisconnect			= 112,
+	VREvent_WirelessReconnect			= 113,
 
 	VREvent_ButtonPress					= 200, // data is controller
 	VREvent_ButtonUnpress				= 201, // data is controller
 	VREvent_ButtonTouch					= 202, // data is controller
 	VREvent_ButtonUntouch				= 203, // data is controller
 
 	VREvent_MouseMove					= 300, // data is mouse
 	VREvent_MouseButtonDown				= 301, // data is mouse
@@ -1538,16 +1541,17 @@ namespace vr
 		VRApplicationError_InvalidManifest = 107,
 		VRApplicationError_InvalidApplication = 108,
 		VRApplicationError_LaunchFailed = 109,			// the process didn't start
 		VRApplicationError_ApplicationAlreadyStarting = 110, // the system was already starting the same application
 		VRApplicationError_LaunchInProgress = 111,		// The system was already starting a different application
 		VRApplicationError_OldApplicationQuitting = 112, 
 		VRApplicationError_TransitionAborted = 113,
 		VRApplicationError_IsTemplate = 114, // error when you try to call LaunchApplication() on a template type app (use LaunchTemplateApplication)
+		VRApplicationError_SteamVRIsExiting = 115,
 
 		VRApplicationError_BufferTooSmall = 200,		// The provided buffer was too small to fit the requested data
 		VRApplicationError_PropertyNotSet = 201,		// The requested property was not set
 		VRApplicationError_UnknownProperty = 202,
 		VRApplicationError_InvalidParameter = 203,
 	};
 
 	/** The maximum length of an application key */
@@ -1568,16 +1572,17 @@ namespace vr
 		VRApplicationProperty_NewsURL_String			= 51,
 		VRApplicationProperty_ImagePath_String			= 52,
 		VRApplicationProperty_Source_String				= 53,
 
 		VRApplicationProperty_IsDashboardOverlay_Bool	= 60,
 		VRApplicationProperty_IsTemplate_Bool			= 61,
 		VRApplicationProperty_IsInstanced_Bool			= 62,
 		VRApplicationProperty_IsInternal_Bool			= 63,
+		VRApplicationProperty_WantsCompositorPauseInStandby_Bool = 64,
 
 		VRApplicationProperty_LastLaunchTime_Uint64		= 70,
 	};
 
 	/** These are states the scene application startup process will go through. */
 	enum EVRApplicationTransitionState
 	{
 		VRApplicationTransition_None = 0,
@@ -1917,16 +1922,17 @@ namespace vr
 	//-----------------------------------------------------------------------------
 	// power management keys
 	static const char * const k_pch_Power_Section = "power";
 	static const char * const k_pch_Power_PowerOffOnExit_Bool = "powerOffOnExit";
 	static const char * const k_pch_Power_TurnOffScreensTimeout_Float = "turnOffScreensTimeout";
 	static const char * const k_pch_Power_TurnOffControllersTimeout_Float = "turnOffControllersTimeout";
 	static const char * const k_pch_Power_ReturnToWatchdogTimeout_Float = "returnToWatchdogTimeout";
 	static const char * const k_pch_Power_AutoLaunchSteamVROnButtonPress = "autoLaunchSteamVROnButtonPress";
+	static const char * const k_pch_Power_PauseCompositorOnStandby_Bool = "pauseCompositorOnStandby";
 
 	//-----------------------------------------------------------------------------
 	// dashboard keys
 	static const char * const k_pch_Dashboard_Section = "dashboard";
 	static const char * const k_pch_Dashboard_EnableDashboard_Bool = "enableDashboard";
 	static const char * const k_pch_Dashboard_ArcadeMode_Bool = "arcadeMode";
 
 	//-----------------------------------------------------------------------------
diff --git a/gfx/vr/openvr/moz.build b/gfx/vr/openvr/moz.build
--- a/gfx/vr/openvr/moz.build
+++ b/gfx/vr/openvr/moz.build
@@ -1,59 +1,59 @@
-# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
-# vim: set filetype=python:
-# This Source Code Form is subject to the terms of the Mozilla Public
-# License, v. 2.0. If a copy of the MPL was not distributed with this
-# file, You can obtain one at http://mozilla.org/MPL/2.0/.
-
-FINAL_LIBRARY = 'xul'
-
-DEFINES['VR_API_PUBLIC'] = True
-
-if CONFIG['OS_ARCH'] == 'WINNT':
-    if CONFIG['HAVE_64BIT_BUILD']:
-        DEFINES['WIN64'] = True
-    else:
-        DEFINES['WIN32'] = True
-
-if CONFIG['OS_ARCH'] == 'Darwin':
-    DEFINES['POSIX'] = True
-    DEFINES['OSX'] = True
-    if CONFIG['GNU_CXX']:
-        CXXFLAGS += ['-xobjective-c++']
-
-if CONFIG['OS_ARCH'] == 'Linux':
-    DEFINES['POSIX'] = True
-    DEFINES['LINUX'] = True
-    if CONFIG['HAVE_64BIT_BUILD']:
-        DEFINES['LINUX64'] = True
-    else:
-        DEFINES['LINUX32'] = True
-
-LOCAL_INCLUDES += [
-    '/toolkit/components/jsoncpp/include',
-]
-
-USE_LIBS += [
-    'jsoncpp',
-]
-
-EXPORTS += [
-    'headers/openvr.h',
-]
-
-SOURCES += [
-    'src/dirtools_public.cpp',
-    'src/envvartools_public.cpp',
-    'src/hmderrors_public.cpp',
-    'src/openvr_api_public.cpp',
-    'src/pathtools_public.cpp',
-    'src/sharedlibtools_public.cpp',
-    'src/strtools_public.cpp',
-    'src/vrpathregistry_public.cpp',
-]
-
-if CONFIG['CLANG_CXX'] or CONFIG['GNU_CXX']:
-    # Harmless warnings in 3rd party code
-    CXXFLAGS += [
-        '-Wno-error=parentheses',
-        '-Wno-error=unused-variable',
-    ]
+# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
+# vim: set filetype=python:
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+FINAL_LIBRARY = 'xul'
+
+DEFINES['VR_API_PUBLIC'] = True
+
+if CONFIG['OS_ARCH'] == 'WINNT':
+    if CONFIG['HAVE_64BIT_BUILD']:
+        DEFINES['WIN64'] = True
+    else:
+        DEFINES['WIN32'] = True
+
+if CONFIG['OS_ARCH'] == 'Darwin':
+    DEFINES['POSIX'] = True
+    DEFINES['OSX'] = True
+    if CONFIG['GNU_CXX']:
+        CXXFLAGS += ['-xobjective-c++']
+
+if CONFIG['OS_ARCH'] == 'Linux':
+    DEFINES['POSIX'] = True
+    DEFINES['LINUX'] = True
+    if CONFIG['HAVE_64BIT_BUILD']:
+        DEFINES['LINUX64'] = True
+    else:
+        DEFINES['LINUX32'] = True
+
+LOCAL_INCLUDES += [
+    '/toolkit/components/jsoncpp/include',
+]
+
+USE_LIBS += [
+    'jsoncpp',
+]
+
+EXPORTS += [
+    'headers/openvr.h',
+]
+
+SOURCES += [
+    'src/dirtools_public.cpp',
+    'src/envvartools_public.cpp',
+    'src/hmderrors_public.cpp',
+    'src/openvr_api_public.cpp',
+    'src/pathtools_public.cpp',
+    'src/sharedlibtools_public.cpp',
+    'src/strtools_public.cpp',
+    'src/vrpathregistry_public.cpp',
+]
+
+if CONFIG['CLANG_CXX'] or CONFIG['GNU_CXX']:
+    # Harmless warnings in 3rd party code
+    CXXFLAGS += [
+        '-Wno-error=parentheses',
+        '-Wno-error=unused-variable',
+    ]
diff --git a/gfx/vr/openvr/src/sharedlibtools_public.cpp b/gfx/vr/openvr/src/sharedlibtools_public.cpp
--- a/gfx/vr/openvr/src/sharedlibtools_public.cpp
+++ b/gfx/vr/openvr/src/sharedlibtools_public.cpp
@@ -17,17 +17,17 @@ SharedLibHandle SharedLib_Load( const ch
 #elif defined(POSIX)
 	return (SharedLibHandle)dlopen(pchPath, RTLD_LOCAL|RTLD_NOW);
 #endif
 }
 
 void *SharedLib_GetFunction( SharedLibHandle lib, const char *pchFunctionName)
 {
 #if defined( _WIN32)
-  return (void*)GetProcAddress( (HMODULE)lib, pchFunctionName );
+	return (void*)GetProcAddress( (HMODULE)lib, pchFunctionName );
 #elif defined(POSIX)
 	return dlsym( lib, pchFunctionName );
 #endif
 }
 
 
 void SharedLib_Unload( SharedLibHandle lib )
 {
