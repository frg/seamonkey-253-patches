# HG changeset patch
# User Mason Chang <mchang@mozilla.com>
# Date 1501826144 25200
# Node ID a72203a86ed1a199aa7d40471e2fba72995605a5
# Parent  dedaa9700e9dce7324f36504651bb9664cbd4919
Bug 1386849 - Always force CompositorBridgeChild::NotifyFinishedAsyncPaint. r=dvander

diff --git a/gfx/layers/PaintThread.cpp b/gfx/layers/PaintThread.cpp
--- a/gfx/layers/PaintThread.cpp
+++ b/gfx/layers/PaintThread.cpp
@@ -21,16 +21,58 @@ namespace mozilla {
 namespace layers {
 
 using namespace gfx;
 
 StaticAutoPtr<PaintThread> PaintThread::sSingleton;
 StaticRefPtr<nsIThread> PaintThread::sThread;
 PlatformThreadId PaintThread::sThreadId;
 
+// RAII make sure we clean up and restore our draw targets
+// when we paint async.
+struct AutoCapturedPaintSetup {
+  AutoCapturedPaintSetup(DrawTarget* aTarget,
+                         DrawTargetCapture* aCapture,
+                         CompositorBridgeChild* aBridge)
+  : mTarget(aTarget)
+  , mRestorePermitsSubpixelAA(aTarget->GetPermitSubpixelAA())
+  , mOldTransform(aTarget->GetTransform())
+  , mBridge(aBridge)
+  {
+    MOZ_ASSERT(mTarget);
+    MOZ_ASSERT(aCapture);
+
+    mTarget->SetTransform(aCapture->GetTransform());
+    mTarget->SetPermitSubpixelAA(aCapture->GetPermitSubpixelAA());
+  }
+
+  ~AutoCapturedPaintSetup()
+  {
+    mTarget->SetTransform(mOldTransform);
+    mTarget->SetPermitSubpixelAA(mRestorePermitsSubpixelAA);
+
+    // Textureclient forces a flush once we "end paint", so
+    // users of this texture expect all the drawing to be complete.
+    // Force a flush now.
+    // TODO: This might be a performance bottleneck because
+    // main thread painting only does one flush at the end of all paints
+    // whereas we force a flush after each draw target paint.
+    mTarget->Flush();
+
+    if (mBridge) {
+      mBridge->NotifyFinishedAsyncPaint();
+    }
+  }
+
+  DrawTarget* mTarget;
+  bool mRestorePermitsSubpixelAA;
+  Matrix mOldTransform;
+  RefPtr<CompositorBridgeChild> mBridge;
+};
+
 void
 PaintThread::Release()
 {
 }
 
 void
 PaintThread::AddRef()
 {
@@ -138,76 +180,58 @@ PaintThread::ShutdownOnPaintThread()
 /* static */ bool
 PaintThread::IsOnPaintThread()
 {
   return sThreadId == PlatformThread::CurrentId();
 }
 
 void
 PaintThread::PaintContentsAsync(CompositorBridgeChild* aBridge,
-                                gfx::DrawTargetCapture* aCapture,
                                 CapturedPaintState* aState,
                                 PrepDrawTargetForPaintingCallback aCallback)
 {
   MOZ_ASSERT(IsOnPaintThread());
-  MOZ_ASSERT(aCapture);
   MOZ_ASSERT(aState);
 
   DrawTarget* target = aState->mTarget;
+  DrawTargetCapture* capture = aState->mCapture;
 
-  Matrix oldTransform = target->GetTransform();
-  target->SetTransform(aState->mTargetTransform);
-  target->SetPermitSubpixelAA(aCapture->GetPermitSubpixelAA());
+  AutoCapturedPaintSetup setup(target, capture, aBridge);
 
   if (!aCallback(aState)) {
     return;
   }
 
   // Draw all the things into the actual dest target.
-  target->DrawCapturedDT(aCapture, Matrix());
-  target->SetTransform(oldTransform);
-
-  // Textureclient forces a flush once we "end paint", so
-  // users of this texture expect all the drawing to be complete.
-  // Force a flush now.
-  // TODO: This might be a performance bottleneck because
-  // main thread painting only does one flush at the end of all paints
-  // whereas we force a flush after each draw target paint.
-  target->Flush();
-
-  if (aBridge) {
-    aBridge->NotifyFinishedAsyncPaint();
-  }
+  target->DrawCapturedDT(capture, Matrix());
 }
 
 void
-PaintThread::PaintContents(DrawTargetCapture* aCapture,
-                           CapturedPaintState* aState,
+PaintThread::PaintContents(CapturedPaintState* aState,
                            PrepDrawTargetForPaintingCallback aCallback)
 {
   MOZ_ASSERT(NS_IsMainThread());
-  MOZ_ASSERT(aCapture);
   MOZ_ASSERT(aState);
 
   // If painting asynchronously, we need to acquire the compositor bridge which
   // owns the underlying MessageChannel. Otherwise we leave it null and use
   // synchronous dispatch.
   RefPtr<CompositorBridgeChild> cbc;
   if (!gfxPrefs::LayersOMTPForceSync()) {
     cbc = CompositorBridgeChild::Get();
     cbc->NotifyBeginAsyncPaint();
   }
-  RefPtr<DrawTargetCapture> capture(aCapture);
   RefPtr<CapturedPaintState> state(aState);
+  RefPtr<DrawTargetCapture> capture(aState->mCapture);
 
   RefPtr<PaintThread> self = this;
   RefPtr<Runnable> task = NS_NewRunnableFunction("PaintThread::PaintContents",
     [self, cbc, capture, state, aCallback]() -> void
   {
-    self->PaintContentsAsync(cbc, capture,
+    self->PaintContentsAsync(cbc,
                              state,
                              aCallback);
   });
 
   if (cbc) {
     sThread->Dispatch(task.forget());
   } else {
     SyncRunnable::DispatchToThread(sThread, task);
diff --git a/gfx/layers/PaintThread.h b/gfx/layers/PaintThread.h
--- a/gfx/layers/PaintThread.h
+++ b/gfx/layers/PaintThread.h
@@ -22,30 +22,33 @@ class DrawTargetCapture;
 namespace layers {
 
 // Holds the key parts from a RotatedBuffer::PaintState
 // required to draw the captured paint state
 class CapturedPaintState {
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(CapturedPaintState)
 public:
   CapturedPaintState(nsIntRegion& aRegionToDraw,
+                     gfx::DrawTargetCapture* aCapture,
                      gfx::DrawTarget* aTarget,
                      gfx::DrawTarget* aTargetOnWhite,
                      gfx::Matrix aTargetTransform,
                      SurfaceMode aSurfaceMode,
                      gfxContentType aContentType)
   : mRegionToDraw(aRegionToDraw)
+  , mCapture(aCapture)
   , mTarget(aTarget)
   , mTargetOnWhite(aTargetOnWhite)
   , mTargetTransform(aTargetTransform)
   , mSurfaceMode(aSurfaceMode)
   , mContentType(aContentType)
   {}
 
   nsIntRegion mRegionToDraw;
+  RefPtr<gfx::DrawTargetCapture> mCapture;
   RefPtr<gfx::DrawTarget> mTarget;
   RefPtr<gfx::DrawTarget> mTargetOnWhite;
   gfx::Matrix mTargetTransform;
   SurfaceMode mSurfaceMode;
   gfxContentType mContentType;
 
 protected:
   virtual ~CapturedPaintState() {}
@@ -58,18 +61,17 @@ class CompositorBridgeChild;
 class PaintThread final
 {
   friend void DestroyPaintThread(UniquePtr<PaintThread>&& aPaintThread);
 
 public:
   static void Start();
   static void Shutdown();
   static PaintThread* Get();
-  void PaintContents(gfx::DrawTargetCapture* aCapture,
-                     CapturedPaintState* aState,
+  void PaintContents(CapturedPaintState* aState,
                      PrepDrawTargetForPaintingCallback aCallback);
 
   // Sync Runnables need threads to be ref counted,
   // But this thread lives through the whole process.
   // We're only temporarily using sync runnables so
   // Override release/addref but don't do anything.
   void Release();
   void AddRef();
@@ -77,17 +79,16 @@ public:
   // Helper for asserts.
   static bool IsOnPaintThread();
 
 private:
   bool Init();
   void ShutdownOnPaintThread();
   void InitOnPaintThread();
   void PaintContentsAsync(CompositorBridgeChild* aBridge,
-                          gfx::DrawTargetCapture* aCapture,
                           CapturedPaintState* aState,
                           PrepDrawTargetForPaintingCallback aCallback);
 
   static StaticAutoPtr<PaintThread> sSingleton;
   static StaticRefPtr<nsIThread> sThread;
   static PlatformThreadId sThreadId;
 };
 
diff --git a/gfx/layers/RotatedBuffer.cpp b/gfx/layers/RotatedBuffer.cpp
--- a/gfx/layers/RotatedBuffer.cpp
+++ b/gfx/layers/RotatedBuffer.cpp
@@ -831,16 +831,17 @@ RotatedContentBuffer::BorrowDrawTargetFo
   ExpandDrawRegion(aPaintState, aIter, result->GetBackendType());
 
   nsIntRegion regionToDraw = aIter ? aIter->mDrawRegion
                                    : aPaintState.mRegionToDraw;
 
   // Can't stack allocate refcounted objects.
   RefPtr<CapturedPaintState> capturedPaintState =
     MakeAndAddRef<CapturedPaintState>(regionToDraw,
+                                      nullptr,
                                       mDTBuffer,
                                       mDTBufferOnWhite,
                                       Matrix(),
                                       aPaintState.mMode,
                                       aPaintState.mContentType);
 
   if (!RotatedContentBuffer::PrepareDrawTargetForPainting(capturedPaintState)) {
     return nullptr;
diff --git a/gfx/layers/client/ClientPaintedLayer.cpp b/gfx/layers/client/ClientPaintedLayer.cpp
--- a/gfx/layers/client/ClientPaintedLayer.cpp
+++ b/gfx/layers/client/ClientPaintedLayer.cpp
@@ -257,23 +257,23 @@ ClientPaintedLayer::PaintOffMainThread()
                                               ClientManager()->GetPaintedLayerCallbackData());
 
     ctx = nullptr;
 
     // TODO: Fixup component alpha
     DrawTarget* targetOnWhite = nullptr;
     RefPtr<CapturedPaintState> capturedState
       = MakeAndAddRef<CapturedPaintState>(state.mRegionToDraw,
+                                          captureDT,
                                           target, targetOnWhite,
                                           capturedTransform,
                                           state.mMode,
                                           state.mContentType);
 
-    PaintThread::Get()->PaintContents(captureDT,
-                                      capturedState,
+    PaintThread::Get()->PaintContents(capturedState,
                                       RotatedContentBuffer::PrepareDrawTargetForPainting);
 
     mContentClient->ReturnDrawTargetToBuffer(target);
 
     didUpdate = true;
   }
   mContentClient->EndPaint(nullptr);
 
