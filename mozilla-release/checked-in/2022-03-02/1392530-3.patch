# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1503467425 -7200
# Node ID e634da7e61a9373fbf7fecaa6894f10ff192fa7b
# Parent  0fd6f550ef454fdf27589f82fd38db01967ae487
Bug 1392530 part 3 - Make MDefinition::valueHash() overloads more consistent. r=nbp

diff --git a/js/src/jit/MIR.cpp b/js/src/jit/MIR.cpp
--- a/js/src/jit/MIR.cpp
+++ b/js/src/jit/MIR.cpp
@@ -219,32 +219,86 @@ EvaluateExactReciprocal(TempAllocator& a
 void
 MDefinition::printName(GenericPrinter& out) const
 {
     PrintOpcodeName(out, op());
     out.printf("%u", id());
 }
 
 HashNumber
-MDefinition::addU32ToHash(HashNumber hash, uint32_t data)
-{
-    return data + (hash << 6) + (hash << 16) - hash;
-}
-
-HashNumber
 MDefinition::valueHash() const
 {
     HashNumber out = HashNumber(op());
     for (size_t i = 0, e = numOperands(); i < e; i++)
         out = addU32ToHash(out, getOperand(i)->id());
     if (MDefinition* dep = dependency())
         out = addU32ToHash(out, dep->id());
     return out;
 }
 
+HashNumber
+MNullaryInstruction::valueHash() const
+{
+    HashNumber hash = HashNumber(op());
+    if (MDefinition* dep = dependency())
+        hash = addU32ToHash(hash, dep->id());
+    MOZ_ASSERT(hash == MDefinition::valueHash());
+    return hash;
+}
+
+HashNumber
+MUnaryInstruction::valueHash() const
+{
+    HashNumber hash = HashNumber(op());
+    hash = addU32ToHash(hash, getOperand(0)->id());
+    if (MDefinition* dep = dependency())
+        hash = addU32ToHash(hash, dep->id());
+    MOZ_ASSERT(hash == MDefinition::valueHash());
+    return hash;
+}
+
+HashNumber
+MBinaryInstruction::valueHash() const
+{
+    HashNumber hash = HashNumber(op());
+    hash = addU32ToHash(hash, getOperand(0)->id());
+    hash = addU32ToHash(hash, getOperand(1)->id());
+    if (MDefinition* dep = dependency())
+        hash = addU32ToHash(hash, dep->id());
+    MOZ_ASSERT(hash == MDefinition::valueHash());
+    return hash;
+}
+
+HashNumber
+MTernaryInstruction::valueHash() const
+{
+    HashNumber hash = HashNumber(op());
+    hash = addU32ToHash(hash, getOperand(0)->id());
+    hash = addU32ToHash(hash, getOperand(1)->id());
+    hash = addU32ToHash(hash, getOperand(2)->id());
+    if (MDefinition* dep = dependency())
+        hash = addU32ToHash(hash, dep->id());
+    MOZ_ASSERT(hash == MDefinition::valueHash());
+    return hash;
+}
+
+HashNumber
+MQuaternaryInstruction::valueHash() const
+{
+    HashNumber hash = HashNumber(op());
+    hash = addU32ToHash(hash, getOperand(0)->id());
+    hash = addU32ToHash(hash, getOperand(1)->id());
+    hash = addU32ToHash(hash, getOperand(2)->id());
+    hash = addU32ToHash(hash, getOperand(3)->id());
+    if (MDefinition* dep = dependency())
+        hash = addU32ToHash(hash, dep->id());
+    MOZ_ASSERT(hash == MDefinition::valueHash());
+    return hash;
+}
+
 bool
 MDefinition::congruentIfOperandsEqual(const MDefinition* ins) const
 {
     if (op() != ins->op())
         return false;
 
     if (type() != ins->type())
         return false;
diff --git a/js/src/jit/MIR.h b/js/src/jit/MIR.h
--- a/js/src/jit/MIR.h
+++ b/js/src/jit/MIR.h
@@ -543,17 +543,19 @@ class MDefinition : public MNode
     bool isDefinition() const = delete;
     bool isResumePoint() const = delete;
 
   protected:
     void setBlock(MBasicBlock* block) {
         setBlockAndKind(block, Kind::Definition);
     }
 
-    static HashNumber addU32ToHash(HashNumber hash, uint32_t data);
+    static HashNumber addU32ToHash(HashNumber hash, uint32_t data) {
+        return data + (hash << 6) + (hash << 16) - hash;
+    }
 
   public:
     explicit MDefinition(Opcode op)
       : MNode(nullptr, Kind::Definition),
         id_(0),
         op_(op),
         flags_(0),
         range_(nullptr),
@@ -1329,27 +1331,31 @@ class MAryInstruction : public MInstruct
 class MNullaryInstruction
   : public MAryInstruction<0>,
     public NoTypePolicy::Data
 {
   protected:
     explicit MNullaryInstruction(Opcode op)
       : MAryInstruction(op)
     { }
+
+    HashNumber valueHash() const override;
 };
 
 class MUnaryInstruction : public MAryInstruction<1>
 {
   protected:
     MUnaryInstruction(Opcode op, MDefinition* ins)
       : MAryInstruction(op)
     {
         initOperand(0, ins);
     }
 
+    HashNumber valueHash() const override;
+
   public:
     NAMED_OPERANDS((0, input))
 };
 
 class MBinaryInstruction : public MAryInstruction<2>
 {
   protected:
     MBinaryInstruction(Opcode op, MDefinition* left, MDefinition* right)
@@ -1363,23 +1369,18 @@ class MBinaryInstruction : public MAryIn
     NAMED_OPERANDS((0, lhs), (1, rhs))
     void swapOperands() {
         MDefinition* temp = getOperand(0);
         replaceOperand(0, getOperand(1));
         replaceOperand(1, temp);
     }
 
   protected:
-    HashNumber valueHash() const
-    {
-        MDefinition* lhs = getOperand(0);
-        MDefinition* rhs = getOperand(1);
-
-        return HashNumber(op()) + lhs->id() + rhs->id();
-    }
+    HashNumber valueHash() const override;
+
     bool binaryCongruentTo(const MDefinition* ins) const
     {
         if (op() != ins->op())
             return false;
 
         if (type() != ins->type())
             return false;
 
@@ -1425,52 +1426,34 @@ class MTernaryInstruction : public MAryI
     MTernaryInstruction(Opcode op, MDefinition* first, MDefinition* second, MDefinition* third)
       : MAryInstruction(op)
     {
         initOperand(0, first);
         initOperand(1, second);
         initOperand(2, third);
     }
 
-  protected:
-    HashNumber valueHash() const
-    {
-        MDefinition* first = getOperand(0);
-        MDefinition* second = getOperand(1);
-        MDefinition* third = getOperand(2);
-
-        return HashNumber(op()) + first->id() + second->id() + third->id();
-    }
+    HashNumber valueHash() const override;
 };
 
 class MQuaternaryInstruction : public MAryInstruction<4>
 {
   protected:
     MQuaternaryInstruction(Opcode op,
                            MDefinition* first, MDefinition* second,
                            MDefinition* third, MDefinition* fourth)
       : MAryInstruction(op)
     {
         initOperand(0, first);
         initOperand(1, second);
         initOperand(2, third);
         initOperand(3, fourth);
     }
 
-  protected:
-    HashNumber valueHash() const
-    {
-        MDefinition* first = getOperand(0);
-        MDefinition* second = getOperand(1);
-        MDefinition* third = getOperand(2);
-        MDefinition* fourth = getOperand(3);
-
-        return HashNumber(op()) + first->id() + second->id() +
-            third->id() + fourth->id();
-    }
+    HashNumber valueHash() const override;
 };
 
 template <class T>
 class MVariadicT : public T
 {
     FixedList<MUse> operands_;
 
   protected:
@@ -14143,17 +14126,17 @@ class MWasmLoadTls
 
     bool congruentTo(const MDefinition* ins) const override {
         return op() == ins->op() &&
                offset() == ins->toWasmLoadTls()->offset() &&
                type() == ins->type();
     }
 
     HashNumber valueHash() const override {
-        return HashNumber(op()) + offset();
+        return addU32ToHash(HashNumber(op()), offset());
     }
 
     AliasSet getAliasSet() const override {
         return aliases_;
     }
 };
 
 class MWasmBoundsCheck
