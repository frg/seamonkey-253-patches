# HG changeset patch
# User Eden Chuang <echuang@mozilla.com>
# Date 1505456392 -28800
#      Fri Sep 15 14:19:52 2017 +0800
# Node ID 080349a1a6214fb01f40c40daa3c961a285a22ca
# Parent  706830347930dd8e3d53a260c817a2c1207a5d7c
Bug 1398991 - Set PaymentRequest.ShippingOption only if options.requestShipping is true. r=baku

This patch implements the following changes according to the spec change.
See https://w3c.github.io/payment-request/#constructor step 8 for more
details.

1. Return TypeError during PaymentRequest construction with duplicate
   shippingOption id.
2. Set PaymentRequest.shippingOption with the selected shippingOption only
   if options.requestShipping is true.

diff --git a/dom/payments/PaymentRequest.cpp b/dom/payments/PaymentRequest.cpp
--- a/dom/payments/PaymentRequest.cpp
+++ b/dom/payments/PaymentRequest.cpp
@@ -271,24 +271,32 @@ PaymentRequest::IsValidDetailsBase(const
         return rv;
       }
     }
   }
 
   // Check the shipping option
   if (aDetails.mShippingOptions.WasPassed()) {
     const Sequence<PaymentShippingOption>& shippingOptions = aDetails.mShippingOptions.Value();
+    nsTArray<nsString> seenIDs;
     for (const PaymentShippingOption& shippingOption : shippingOptions) {
       rv = IsValidCurrencyAmount(NS_LITERAL_STRING("details.shippingOptions"),
                                  shippingOption.mAmount,
                                  false,  // isTotalItem
                                  aErrorMsg);
       if (NS_FAILED(rv)) {
         return rv;
       }
+      if (seenIDs.Contains(shippingOption.mId)) {
+        aErrorMsg.AssignLiteral("Duplicate shippingOption id '");
+        aErrorMsg.Append(shippingOption.mId);
+        aErrorMsg.AppendLiteral("'");
+        return NS_ERROR_TYPE_ERR;
+      }
+      seenIDs.AppendElement(shippingOption.mId);
     }
   }
 
   // Check payment details modifiers
   if (aDetails.mModifiers.WasPassed()) {
     const Sequence<PaymentDetailsModifier>& modifiers = aDetails.mModifiers.Value();
     for (const PaymentDetailsModifier& modifier : modifiers) {
       rv = IsValidCurrencyAmount(NS_LITERAL_STRING("details.modifiers.total"),
diff --git a/dom/payments/PaymentRequestManager.cpp b/dom/payments/PaymentRequestManager.cpp
--- a/dom/payments/PaymentRequestManager.cpp
+++ b/dom/payments/PaymentRequestManager.cpp
@@ -100,28 +100,27 @@ ConvertShippingOption(const PaymentShipp
   aIPCOption = IPCPaymentShippingOption(aOption.mId, aOption.mLabel, amount, aOption.mSelected);
 }
 
 nsresult
 ConvertDetailsBase(JSContext* aCx,
                    const PaymentDetailsBase& aDetails,
                    nsTArray<IPCPaymentItem>& aDisplayItems,
                    nsTArray<IPCPaymentShippingOption>& aShippingOptions,
-                   nsTArray<IPCPaymentDetailsModifier>& aModifiers,
-                   bool aResetShippingOptions)
+                   nsTArray<IPCPaymentDetailsModifier>& aModifiers)
 {
   NS_ENSURE_ARG_POINTER(aCx);
   if (aDetails.mDisplayItems.WasPassed()) {
     for (const PaymentItem& item : aDetails.mDisplayItems.Value()) {
       IPCPaymentItem displayItem;
       ConvertItem(item, displayItem);
       aDisplayItems.AppendElement(displayItem);
     }
   }
-  if (aDetails.mShippingOptions.WasPassed() && !aResetShippingOptions) {
+  if (aDetails.mShippingOptions.WasPassed()) {
     for (const PaymentShippingOption& option : aDetails.mShippingOptions.Value()) {
       IPCPaymentShippingOption shippingOption;
       ConvertShippingOption(option, shippingOption);
       aShippingOptions.AppendElement(shippingOption);
     }
   }
   if (aDetails.mModifiers.WasPassed()) {
     for (const PaymentDetailsModifier& modifier : aDetails.mModifiers.Value()) {
@@ -134,26 +133,25 @@ ConvertDetailsBase(JSContext* aCx,
     }
   }
   return NS_OK;
 }
 
 nsresult
 ConvertDetailsInit(JSContext* aCx,
                    const PaymentDetailsInit& aDetails,
-                   IPCPaymentDetails& aIPCDetails,
-                   bool aResetShippingOptions)
+                   IPCPaymentDetails& aIPCDetails)
 {
   NS_ENSURE_ARG_POINTER(aCx);
   // Convert PaymentDetailsBase members
   nsTArray<IPCPaymentItem> displayItems;
   nsTArray<IPCPaymentShippingOption> shippingOptions;
   nsTArray<IPCPaymentDetailsModifier> modifiers;
   nsresult rv = ConvertDetailsBase(aCx, aDetails, displayItems, shippingOptions,
-                                   modifiers, aResetShippingOptions);
+                                   modifiers);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   // Convert |id|
   nsString id(EmptyString());
   if (aDetails.mId.WasPassed()) {
     id = aDetails.mId.Value();
@@ -180,20 +178,18 @@ ConvertDetailsUpdate(JSContext* aCx,
                      const PaymentDetailsUpdate& aDetails,
                      IPCPaymentDetails& aIPCDetails)
 {
   NS_ENSURE_ARG_POINTER(aCx);
   // Convert PaymentDetailsBase members
   nsTArray<IPCPaymentItem> displayItems;
   nsTArray<IPCPaymentShippingOption> shippingOptions;
   nsTArray<IPCPaymentDetailsModifier> modifiers;
-  // [TODO] Populate a boolean flag as aResetShippingOptions based on the
-  // result of processing details.shippingOptions in UpdatePayment method.
   nsresult rv = ConvertDetailsBase(aCx, aDetails, displayItems, shippingOptions,
-                                   modifiers, false);
+                                   modifiers);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   // Convert required |total|
   IPCPaymentItem total;
   ConvertItem(aDetails.mTotal, total);
 
@@ -362,38 +358,26 @@ PaymentRequestManager::GetPaymentRequest
       return paymentRequest.forget();
     }
   }
   return nullptr;
 }
 
 void
 GetSelectedShippingOption(const PaymentDetailsInit& aDetails,
-                          nsAString& aOption,
-                          bool* aResetOptions)
+                          nsAString& aOption)
 {
   SetDOMStringToNull(aOption);
   if (!aDetails.mShippingOptions.WasPassed()) {
     return;
   }
 
-  nsTArray<nsString> seenIDs;
   const Sequence<PaymentShippingOption>& shippingOptions =
     aDetails.mShippingOptions.Value();
   for (const PaymentShippingOption& shippingOption : shippingOptions) {
-    // If there are duplicate IDs present in the shippingOptions, reset aOption
-    // to null and set resetOptions flag to reset details.shippingOptions later
-    // when converting to IPC structure.
-    if (seenIDs.Contains(shippingOption.mId)) {
-      SetDOMStringToNull(aOption);
-      *aResetOptions = true;
-      return;
-    }
-    seenIDs.AppendElement(shippingOption.mId);
-
     // set aOption to last selected option's ID
     if (shippingOption.mSelected) {
       aOption = shippingOption.mId;
     }
   }
 }
 
 nsresult
@@ -425,50 +409,43 @@ PaymentRequestManager::CreatePayment(JSC
   if (aDetails.mId.WasPassed() && !aDetails.mId.Value().IsEmpty()) {
     requestId = aDetails.mId.Value();
   } else {
     request->GetInternalId(requestId);
   }
   request->SetId(requestId);
 
   /*
-   *  Set request's mShippingOption to last selected option's ID if
-   *  details.shippingOptions exists and IDs of all options are unique.
-   *  Otherwise, set mShippingOption to null and set the resetShippingOptions
-   *  flag to reset details.shippingOptions to an empty array later when
-   *  converting details to IPC structure.
-   */
-  nsAutoString shippingOption;
-  bool resetShippingOptions = false;
-  GetSelectedShippingOption(aDetails, shippingOption, &resetShippingOptions);
-  request->SetShippingOption(shippingOption);
-
-  /*
-   * Set request's |mShippingType| if shipping is required.
+   * Set request's |mShippingType| and |mShippingOption| if shipping is required.
+   * Set request's mShippingOption to last selected option's ID if
+   * details.shippingOptions exists.
    */
   if (aOptions.mRequestShipping) {
     request->SetShippingType(
         Nullable<PaymentShippingType>(aOptions.mShippingType));
+    nsAutoString shippingOption;
+    GetSelectedShippingOption(aDetails, shippingOption);
+    request->SetShippingOption(shippingOption);
   }
 
   nsAutoString internalId;
   request->GetInternalId(internalId);
 
   nsTArray<IPCPaymentMethodData> methodData;
   for (const PaymentMethodData& data : aMethodData) {
     IPCPaymentMethodData ipcMethodData;
     rv = ConvertMethodData(aCx, data, ipcMethodData);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
     }
     methodData.AppendElement(ipcMethodData);
   }
 
   IPCPaymentDetails details;
-  rv = ConvertDetailsInit(aCx, aDetails, details, resetShippingOptions);
+  rv = ConvertDetailsInit(aCx, aDetails, details);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   IPCPaymentOptions options;
   ConvertOptions(aOptions, options);
 
   IPCPaymentCreateActionRequest action(internalId,
