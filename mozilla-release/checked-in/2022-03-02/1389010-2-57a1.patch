# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1502097477 -3600
#      Mon Aug 07 10:17:57 2017 +0100
# Node ID 68f0c27b410163aeb5dbc952383e361699ab4173
# Parent  dfcc8657b7253a448d02a22eea8a49f19bd56000
Bug 1389010, part 2 - Move an early return in nsCSSBorderRenderer::DrawBorders up to avoid unnecessary work. r=dholbert

MozReview-Commit-ID: AfuBCfYs3S1

diff --git a/layout/painting/nsCSSRenderingBorders.cpp b/layout/painting/nsCSSRenderingBorders.cpp
--- a/layout/painting/nsCSSRenderingBorders.cpp
+++ b/layout/painting/nsCSSRenderingBorders.cpp
@@ -3209,16 +3209,24 @@ nsCSSBorderRenderer::DrawBorders()
     // All borders are the same style, and the style is either none or hidden, or the color
     // is transparent.
     // This also checks if the first composite color is transparent, and there are
     // no others. It doesn't check if there are subsequent transparent ones, because
     // that would be very silly.
     return;
   }
 
+  bool allBordersSameWidth = AllBordersSameWidth();
+
+  if (allBordersSameWidth && mBorderWidths[0] == 0.0) {
+    // Some of the allBordersSameWidth codepaths depend on the border
+    // width being greater than zero.
+    return;
+  }
+
   AutoRestoreTransform autoRestoreTransform;
   Matrix mat = mDrawTarget->GetTransform();
 
   // Clamp the CTM to be pixel-aligned; we do this only
   // for translation-only matrices now, but we could do it
   // if the matrix has just a scale as well.  We should not
   // do it if there's a rotation.
   if (mat.HasNonTranslation()) {
@@ -3236,24 +3244,16 @@ nsCSSBorderRenderer::DrawBorders()
     // round mOuterRect and mInnerRect; they're already an integer
     // number of pixels apart and should stay that way after
     // rounding. We don't do this if there's a scale in the current transform
     // since this loses information that might be relevant when we're scaling.
     mOuterRect.Round();
     mInnerRect.Round();
   }
 
-  bool allBordersSameWidth = AllBordersSameWidth();
-
-  if (allBordersSameWidth && mBorderWidths[0] == 0.0) {
-    // Some of the allBordersSameWidth codepaths depend on the border
-    // width being greater than zero.
-    return;
-  }
-
   // Initial values only used when the border colors/widths are all the same:
   ColorPattern color(ToDeviceColor(mBorderColors[eSideTop]));
   StrokeOptions strokeOptions(mBorderWidths[eSideTop]); // stroke width
 
   bool allBordersSolid;
 
   // First there's a couple of 'special cases' that have specifically optimized
   // drawing paths, when none of these can be used we move on to the generalized
