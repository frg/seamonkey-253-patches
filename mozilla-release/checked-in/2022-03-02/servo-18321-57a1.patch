# HG changeset patch
# User Nazim Can Altinova <canaltinova@gmail.com>
# Date 1504239503 18000
#      Thu Aug 31 23:18:23 2017 -0500
# Node ID ff49e982f6ed91a49fc3084092f71663f4620184
# Parent  03a8b545262b17ffdf0a28b8846dbf16c9c5c2bf
servo: Merge #18321 - Properly set default direction of prefixed linear gradients (from canaltinova:default-linear-gradient); r=Manishearth

The default linear gradient direction is `to bottom`. This correspondes to
`top` keyword in prefixed linear gradients. We should preserve the default value.

---
<!-- Thank you for contributing to Servo! Please replace each `[ ]` by `[X]` when the step is complete, and replace `__` with appropriate data: -->
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [X] These changes fix [Bug 1395189](https://bugzilla.mozilla.org/show_bug.cgi?id=1395189)

Source-Repo: https://github.com/servo/servo
Source-Revision: ff30f582a02133074a92786af256d09325bc9d22

diff --git a/servo/components/style/values/computed/image.rs b/servo/components/style/values/computed/image.rs
--- a/servo/components/style/values/computed/image.rs
+++ b/servo/components/style/values/computed/image.rs
@@ -73,20 +73,23 @@ pub type GradientItem = GenericGradientI
 
 /// A computed color stop.
 pub type ColorStop = GenericColorStop<RGBA, LengthOrPercentage>;
 
 /// Computed values for `-moz-image-rect(...)`.
 pub type MozImageRect = GenericMozImageRect<NumberOrPercentage, ComputedUrl>;
 
 impl GenericLineDirection for LineDirection {
-    fn points_downwards(&self) -> bool {
+    fn points_downwards(&self, compat_mode: CompatMode) -> bool {
         match *self {
             LineDirection::Angle(angle) => angle.radians() == PI,
-            LineDirection::Vertical(Y::Bottom) => true,
+            LineDirection::Vertical(Y::Bottom)
+                if compat_mode == CompatMode::Modern => true,
+            LineDirection::Vertical(Y::Top)
+                if compat_mode != CompatMode::Modern => true,
             LineDirection::Corner(..) => false,
             #[cfg(feature = "gecko")]
             LineDirection::MozPosition(_, _) => false,
             _ => false,
         }
     }
 
     fn to_css<W>(&self, dest: &mut W, compat_mode: CompatMode) -> fmt::Result
diff --git a/servo/components/style/values/generics/image.rs b/servo/components/style/values/generics/image.rs
--- a/servo/components/style/values/generics/image.rs
+++ b/servo/components/style/values/generics/image.rs
@@ -225,17 +225,18 @@ impl<D, L, LoP, P, C, A> ToCss for Gradi
         }
 
         if self.repeating {
             dest.write_str("repeating-")?;
         }
         dest.write_str(self.kind.label())?;
         dest.write_str("-gradient(")?;
         let mut skip_comma = match self.kind {
-            GradientKind::Linear(ref direction) if direction.points_downwards() => true,
+            GradientKind::Linear(ref direction)
+                if direction.points_downwards(self.compat_mode) => true,
             GradientKind::Linear(ref direction) => {
                 direction.to_css(dest, self.compat_mode)?;
                 false
             },
             GradientKind::Radial(ref shape, ref position, ref angle) => {
                 let omit_shape = match *shape {
                     EndingShape::Ellipse(Ellipse::Extent(ShapeExtent::Cover)) |
                     EndingShape::Ellipse(Ellipse::Extent(ShapeExtent::FarthestCorner)) => {
@@ -282,17 +283,17 @@ impl<D, L, LoP, P, A> GradientKind<D, L,
             GradientKind::Radial(..) => "radial",
         }
     }
 }
 
 /// The direction of a linear gradient.
 pub trait LineDirection {
     /// Whether this direction points towards, and thus can be omitted.
-    fn points_downwards(&self) -> bool;
+    fn points_downwards(&self, compat_mode: CompatMode) -> bool;
 
     /// Serialises this direction according to the compatibility mode.
     fn to_css<W>(&self, dest: &mut W, compat_mode: CompatMode) -> fmt::Result
         where W: fmt::Write;
 }
 
 impl<L> ToCss for Circle<L>
 where
diff --git a/servo/components/style/values/specified/image.rs b/servo/components/style/values/specified/image.rs
--- a/servo/components/style/values/specified/image.rs
+++ b/servo/components/style/values/specified/image.rs
@@ -521,17 +521,20 @@ impl GradientKind {
     fn parse_linear<'i, 't>(context: &ParserContext,
                             input: &mut Parser<'i, 't>,
                             compat_mode: &mut CompatMode)
                             -> Result<Self, ParseError<'i>> {
         let direction = if let Ok(d) = input.try(|i| LineDirection::parse(context, i, compat_mode)) {
             input.expect_comma()?;
             d
         } else {
-            LineDirection::Vertical(Y::Bottom)
+            match *compat_mode {
+                CompatMode::Modern => LineDirection::Vertical(Y::Bottom),
+                _ => LineDirection::Vertical(Y::Top),
+            }
         };
         Ok(GenericGradientKind::Linear(direction))
     }
 
     fn parse_radial<'i, 't>(context: &ParserContext,
                             input: &mut Parser<'i, 't>,
                             compat_mode: &mut CompatMode)
                             -> Result<Self, ParseError<'i>> {
@@ -610,20 +613,23 @@ impl GradientKind {
         #[cfg(not(feature = "gecko"))]
         {
             return Ok(GenericGradientKind::Radial(shape, position, angle));
         }
     }
 }
 
 impl GenericsLineDirection for LineDirection {
-    fn points_downwards(&self) -> bool {
+    fn points_downwards(&self, compat_mode: CompatMode) -> bool {
         match *self {
             LineDirection::Angle(ref angle) => angle.radians() == PI,
-            LineDirection::Vertical(Y::Bottom) => true,
+            LineDirection::Vertical(Y::Bottom)
+                if compat_mode == CompatMode::Modern => true,
+            LineDirection::Vertical(Y::Top)
+                if compat_mode != CompatMode::Modern => true,
             _ => false,
         }
     }
 
     fn to_css<W>(&self, dest: &mut W, compat_mode: CompatMode) -> fmt::Result
         where W: fmt::Write
     {
         match *self {
