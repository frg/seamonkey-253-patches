# HG changeset patch
# User Morris Tseng <mtseng@mozilla.com>
# Date 1501739715 -28800
# Node ID b9317e74f705bdceb4f6cf075ea90a86af45402e
# Parent  3ecd7180c6a7f16b21ce826dde0b5b21836013d4
Bug 1379920 - Introduce mLastCanvasDatas. r=kats

mLastCanvasDatas store used WebRenderCanvasData in last full
transaction. So that in next empty transaction, we can update canvas
content through mLastCanvasDatas.

MozReview-Commit-ID: 2H2m8R7Kzwf

diff --git a/gfx/layers/wr/WebRenderLayerManager.cpp b/gfx/layers/wr/WebRenderLayerManager.cpp
--- a/gfx/layers/wr/WebRenderLayerManager.cpp
+++ b/gfx/layers/wr/WebRenderLayerManager.cpp
@@ -10,16 +10,17 @@
 #include "LayersLogging.h"
 #include "mozilla/gfx/DrawEventRecorder.h"
 #include "mozilla/layers/CompositorBridgeChild.h"
 #include "mozilla/layers/StackingContextHelper.h"
 #include "mozilla/layers/TextureClient.h"
 #include "mozilla/layers/WebRenderBridgeChild.h"
 #include "mozilla/layers/UpdateImageHelper.h"
 #include "WebRenderCanvasLayer.h"
+#include "WebRenderCanvasRenderer.h"
 #include "WebRenderColorLayer.h"
 #include "WebRenderContainerLayer.h"
 #include "WebRenderImageLayer.h"
 #include "WebRenderPaintedLayer.h"
 #include "WebRenderPaintedLayerBlob.h"
 #include "WebRenderTextLayer.h"
 #include "WebRenderDisplayItemLayer.h"
 
@@ -588,31 +589,38 @@ WebRenderLayerManager::EndTransactionInt
     // aDisplayList being null here means this is an empty transaction following a layers-free
     // transaction, so we reuse the previously built displaylist and scroll
     // metadata information
     if (aDisplayList && aDisplayListBuilder) {
       StackingContextHelper sc;
       mParentCommands.Clear();
       mScrollData = WebRenderScrollData();
       MOZ_ASSERT(mLayerScrollData.empty());
+      mLastCanvasDatas.Clear();
 
       CreateWebRenderCommandsFromDisplayList(aDisplayList, aDisplayListBuilder, sc, builder);
 
       builder.Finalize(contentSize, mBuiltDisplayList);
 
       // Make a "root" layer data that has everything else as descendants
       mLayerScrollData.emplace_back();
       mLayerScrollData.back().InitializeRoot(mLayerScrollData.size() - 1);
       // Append the WebRenderLayerScrollData items into WebRenderScrollData
       // in reverse order, from topmost to bottommost. This is in keeping with
       // the semantics of WebRenderScrollData.
       for (auto i = mLayerScrollData.crbegin(); i != mLayerScrollData.crend(); i++) {
         mScrollData.AddLayerData(*i);
       }
       mLayerScrollData.clear();
+    } else {
+      for (auto iter = mLastCanvasDatas.Iter(); !iter.Done(); iter.Next()) {
+        RefPtr<WebRenderCanvasData> canvasData = iter.Get()->GetKey();
+        WebRenderCanvasRendererAsync* canvas = canvasData->GetCanvasRenderer();
+        canvas->UpdateCompositableClient();
+      }
     }
 
     builder.PushBuiltDisplayList(mBuiltDisplayList);
     WrBridge()->AddWebRenderParentCommands(mParentCommands);
   } else {
     mScrollData = WebRenderScrollData();
 
     mRoot->StartPendingAnimations(mAnimationReadyTime);
diff --git a/gfx/layers/wr/WebRenderLayerManager.h b/gfx/layers/wr/WebRenderLayerManager.h
--- a/gfx/layers/wr/WebRenderLayerManager.h
+++ b/gfx/layers/wr/WebRenderLayerManager.h
@@ -204,16 +204,19 @@ public:
       frame->GetProperty(nsIFrame::WebRenderUserDataProperty());
     RefPtr<WebRenderUserData>& data = userDataTable->GetOrInsert(aItem->GetPerFrameKey());
     if (!data || (data->GetType() != T::Type())) {
       data = new T(this);
     }
 
     MOZ_ASSERT(data);
     MOZ_ASSERT(data->GetType() == T::Type());
+    if (T::Type() == WebRenderUserData::UserDataType::eCanvas) {
+      mLastCanvasDatas.PutEntry(data->AsCanvasData());
+    }
     RefPtr<T> res = static_cast<T*>(data.get());
     return res.forget();
   }
 
 private:
   /**
    * Take a snapshot of the parent context, and copy
    * it into mTarget.
@@ -285,14 +288,18 @@ private:
  // being drawn to the default target, and then copy those pixels
  // back to mTarget.
  RefPtr<gfxContext> mTarget;
 
   // See equivalent field in ClientLayerManager
   uint32_t mPaintSequenceNumber;
   // See equivalent field in ClientLayerManager
   APZTestData mApzTestData;
+
+  typedef nsTHashtable<nsRefPtrHashKey<WebRenderCanvasData>> CanvasDataSet;
+  // Store of WebRenderCanvasData objects for use in empty transactions
+  CanvasDataSet mLastCanvasDatas;
 };
 
 } // namespace layers
 } // namespace mozilla
 
 #endif /* GFX_WEBRENDERLAYERMANAGER_H */

