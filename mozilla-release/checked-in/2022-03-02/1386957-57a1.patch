# HG changeset patch
# User Matthew Gregan <kinetik@flim.org>
# Date 1501742809 -43200
# Node ID 6bf5e9b6be4c5a0c6a8cbafab2ddd8f4941ea690
# Parent  975d8155e4d0652a2fa534de06e7683e03e7d0f3
Bug 1386957 -Update libcubeb to revision 0e103884.  r=achronop

MozReview-Commit-ID: HFkJ6TPLIfv

diff --git a/media/libcubeb/README_MOZILLA b/media/libcubeb/README_MOZILLA
--- a/media/libcubeb/README_MOZILLA
+++ b/media/libcubeb/README_MOZILLA
@@ -1,8 +1,8 @@
 The source from this directory was copied from the cubeb 
 git repository using the update.sh script.  The only changes
 made were those applied by update.sh and the addition of
 Makefile.in build files for the Mozilla build system.
 
 The cubeb git repository is: git://github.com/kinetiknz/cubeb.git
 
-The git commit ID used was cf622778bbc3d240d7079879fabd2dc945237cbc (2017-07-29 11:28:16 +1200)
+The git commit ID used was 0e10388459fe1e252c075e4a1fab7a9363dc13a1 (2017-08-03 17:58:06 +1200)
diff --git a/media/libcubeb/src/cubeb_audiounit.cpp b/media/libcubeb/src/cubeb_audiounit.cpp
--- a/media/libcubeb/src/cubeb_audiounit.cpp
+++ b/media/libcubeb/src/cubeb_audiounit.cpp
@@ -1611,17 +1611,18 @@ audiounit_activate_clock_drift_compensat
     LOG("AudioObjectGetPropertyData/kAudioObjectPropertyOwnedObjects, rv=%d", rv);
     return CUBEB_ERROR;
   }
 
   AudioObjectPropertyAddress address_drift = { kAudioSubDevicePropertyDriftCompensation,
                                                kAudioObjectPropertyScopeGlobal,
                                                kAudioObjectPropertyElementMaster };
 
-  for (UInt32 i = 0; i < subdevices_num; ++i) {
+  // Start from the second device since the first is the master clock
+  for (UInt32 i = 1; i < subdevices_num; ++i) {
     UInt32 drift_compensation_value = 1;
     rv = AudioObjectSetPropertyData(sub_devices[i],
                                     &address_drift,
                                     0,
                                     nullptr,
                                     sizeof(UInt32),
                                     &drift_compensation_value);
     if (rv != noErr) {
@@ -1685,16 +1686,19 @@ audiounit_create_aggregate_device(cubeb_
   }
 
   return CUBEB_OK;
 }
 
 static int
 audiounit_destroy_aggregate_device(AudioObjectID plugin_id, AudioDeviceID * aggregate_device_id)
 {
+  assert(aggregate_device_id &&
+         *aggregate_device_id != kAudioDeviceUnknown &&
+         plugin_id != kAudioObjectUnknown);
   AudioObjectPropertyAddress destroy_aggregate_device_addr = { kAudioPlugInDestroyAggregateDevice,
                                                                kAudioObjectPropertyScopeGlobal,
                                                                kAudioObjectPropertyElementMaster};
   UInt32 size;
   OSStatus rv = AudioObjectGetPropertyDataSize(plugin_id,
                                                &destroy_aggregate_device_addr,
                                                0,
                                                NULL,
@@ -2257,17 +2261,18 @@ audiounit_setup_stream(cubeb_stream * st
 {
   stm->mutex.assert_current_thread_owns();
 
   int r = 0;
 
   device_info in_dev_info = stm->input_device;
   device_info out_dev_info = stm->output_device;
 
-  if (has_input(stm) && has_output(stm)) {
+  if (has_input(stm) && has_output(stm) &&
+      stm->input_device.id != stm->output_device.id) {
     r = audiounit_create_aggregate_device(stm);
     if (r != CUBEB_OK) {
       stm->aggregate_device_id = 0;
       LOG("(%p) Create aggregate devices failed.", stm);
       // !!!NOTE: It is not necessary to return here. If it does not
       // return it will fallback to the old implementation. The intention
       // is to investigate how often it fails. I plan to remove
       // it after a couple of weeks.
diff --git a/media/libcubeb/src/cubeb_jack.cpp b/media/libcubeb/src/cubeb_jack.cpp
--- a/media/libcubeb/src/cubeb_jack.cpp
+++ b/media/libcubeb/src/cubeb_jack.cpp
@@ -3,17 +3,19 @@
  * Copyright © 2013 Sebastien Alaiwan
  * Copyright © 2016 Damien Zammit
  *
  * This program is made available under an ISC-style license.  See the
  * accompanying file LICENSE for details.
  */
 #define _DEFAULT_SOURCE
 #define _BSD_SOURCE
+#ifndef __FreeBSD__
 #define _POSIX_SOURCE
+#endif
 #include <dlfcn.h>
 #include <stdio.h>
 #include <string.h>
 #include <limits.h>
 #include <stdlib.h>
 #include <pthread.h>
 #include <math.h>
 #include "cubeb/cubeb.h"
diff --git a/media/libcubeb/src/cubeb_mixer.cpp b/media/libcubeb/src/cubeb_mixer.cpp
--- a/media/libcubeb/src/cubeb_mixer.cpp
+++ b/media/libcubeb/src/cubeb_mixer.cpp
@@ -556,16 +556,16 @@ cubeb_mixer * cubeb_mixer_create(cubeb_s
 
 void cubeb_mixer_destroy(cubeb_mixer * mixer)
 {
   delete mixer;
 }
 
 void cubeb_mixer_mix(cubeb_mixer * mixer, long frames,
                      void * input_buffer, unsigned long input_buffer_length,
-                     void * output_buffer, unsigned long outputput_buffer_length,
+                     void * output_buffer, unsigned long output_buffer_length,
                      cubeb_stream_params const * stream_params,
                      cubeb_stream_params const * mixer_params)
 {
   assert(mixer);
-  mixer->mix(frames, input_buffer, input_buffer_length, output_buffer, outputput_buffer_length,
+  mixer->mix(frames, input_buffer, input_buffer_length, output_buffer, output_buffer_length,
              stream_params, mixer_params);
 }
diff --git a/media/libcubeb/src/cubeb_mixer.h b/media/libcubeb/src/cubeb_mixer.h
--- a/media/libcubeb/src/cubeb_mixer.h
+++ b/media/libcubeb/src/cubeb_mixer.h
@@ -74,17 +74,17 @@ typedef enum {
 } cubeb_mixer_direction;
 
 typedef struct cubeb_mixer cubeb_mixer;
 cubeb_mixer * cubeb_mixer_create(cubeb_sample_format format,
                                  unsigned char direction);
 void cubeb_mixer_destroy(cubeb_mixer * mixer);
 void cubeb_mixer_mix(cubeb_mixer * mixer, long frames,
                      void * input_buffer, unsigned long input_buffer_length,
-                     void * output_buffer, unsigned long outputput_buffer_length,
+                     void * output_buffer, unsigned long output_buffer_length,
                      cubeb_stream_params const * stream_params,
                      cubeb_stream_params const * mixer_params);
 
 #if defined(__cplusplus)
 }
 #endif
 
 #endif // CUBEB_MIXER
diff --git a/media/libcubeb/src/cubeb_wasapi.cpp b/media/libcubeb/src/cubeb_wasapi.cpp
--- a/media/libcubeb/src/cubeb_wasapi.cpp
+++ b/media/libcubeb/src/cubeb_wasapi.cpp
@@ -446,18 +446,32 @@ channel_layout_to_mask(cubeb_channel_lay
     MASK_3F2_LFE,         // CUBEB_LAYOUT_3F2_LFE
     MASK_3F3R_LFE,        // CUBEB_LAYOUT_3F3R_LFE
     MASK_3F4_LFE,         // CUBEB_LAYOUT_3F4_LFE
   };
   return map[layout];
 }
 
 cubeb_channel_layout
-mask_to_channel_layout(DWORD mask)
+mask_to_channel_layout(WAVEFORMATEX const * fmt)
 {
+  DWORD mask = 0;
+
+  if (fmt->wFormatTag == WAVE_FORMAT_EXTENSIBLE) {
+    WAVEFORMATEXTENSIBLE const * ext = reinterpret_cast<WAVEFORMATEXTENSIBLE const *>(fmt);
+    mask = ext->dwChannelMask;
+  } else if (fmt->wFormatTag == WAVE_FORMAT_PCM ||
+             fmt->wFormatTag == WAVE_FORMAT_IEEE_FLOAT) {
+    if (fmt->nChannels == 1) {
+      mask = MASK_MONO;
+    } else if (fmt->nChannels == 2) {
+      mask = MASK_STEREO;
+    }
+  }
+
   switch (mask) {
     // MASK_DUAL_MONO(_LFE) is same as STEREO(_LFE), so we skip it.
     case MASK_MONO: return CUBEB_LAYOUT_MONO;
     case MASK_MONO_LFE: return CUBEB_LAYOUT_MONO_LFE;
     case MASK_STEREO: return CUBEB_LAYOUT_STEREO;
     case MASK_STEREO_LFE: return CUBEB_LAYOUT_STEREO_LFE;
     case MASK_3F: return CUBEB_LAYOUT_3F;
     case MASK_3F_LFE: return CUBEB_LAYOUT_3F_LFE;
@@ -1363,18 +1377,17 @@ wasapi_get_preferred_channel_layout(cube
 
   WAVEFORMATEX * tmp = nullptr;
   hr = client->GetMixFormat(&tmp);
   if (FAILED(hr)) {
     return CUBEB_ERROR;
   }
   com_heap_ptr<WAVEFORMATEX> mix_format(tmp);
 
-  WAVEFORMATEXTENSIBLE * format_pcm = reinterpret_cast<WAVEFORMATEXTENSIBLE *>(mix_format.get());
-  *layout = mask_to_channel_layout(format_pcm->dwChannelMask);
+  *layout = mask_to_channel_layout(mix_format.get());
 
   LOG("Preferred channel layout: %s", CUBEB_CHANNEL_LAYOUT_MAPS[*layout].name);
 
   return CUBEB_OK;
 }
 
 void wasapi_stream_destroy(cubeb_stream * stm);
 
@@ -1423,24 +1436,26 @@ handle_channel_layout(cubeb_stream * stm
   HRESULT hr = audio_client->IsFormatSupported(AUDCLNT_SHAREMODE_SHARED,
                                                mix_format.get(),
                                                &closest);
   if (hr == S_FALSE) {
     /* Channel layout not supported, but WASAPI gives us a suggestion. Use it,
        and handle the eventual upmix/downmix ourselves. Ignore the subformat of
        the suggestion, since it seems to always be IEEE_FLOAT. */
     LOG("Using WASAPI suggested format: channels: %d", closest->nChannels);
+    XASSERT(closest->wFormatTag == WAVE_FORMAT_EXTENSIBLE);
     WAVEFORMATEXTENSIBLE * closest_pcm = reinterpret_cast<WAVEFORMATEXTENSIBLE *>(closest);
     format_pcm->dwChannelMask = closest_pcm->dwChannelMask;
     mix_format->nChannels = closest->nChannels;
     waveformatex_update_derived_properties(mix_format.get());
   } else if (hr == AUDCLNT_E_UNSUPPORTED_FORMAT) {
     /* Not supported, no suggestion. This should not happen, but it does in the
        field with some sound cards. We restore the mix format, and let the rest
        of the code figure out the right conversion path. */
+    XASSERT(mix_format->wFormatTag == WAVE_FORMAT_EXTENSIBLE);
     *reinterpret_cast<WAVEFORMATEXTENSIBLE *>(mix_format.get()) = hw_mix_format;
   } else if (hr == S_OK) {
     LOG("Requested format accepted by WASAPI.");
   } else {
     LOG("IsFormatSupported unhandled error: %lx", hr);
   }
 }
 
@@ -1509,31 +1524,33 @@ int setup_wasapi_stream_one_side(cubeb_s
   hr = audio_client->GetMixFormat(&tmp);
   if (FAILED(hr)) {
     LOG("Could not fetch current mix format from the audio"
         " client for %s: error: %lx", DIRECTION_NAME, hr);
     return CUBEB_ERROR;
   }
   com_heap_ptr<WAVEFORMATEX> mix_format(tmp);
 
-  WAVEFORMATEXTENSIBLE * format_pcm = reinterpret_cast<WAVEFORMATEXTENSIBLE *>(mix_format.get());
   mix_format->wBitsPerSample = stm->bytes_per_sample * 8;
-  format_pcm->SubFormat = stm->waveformatextensible_sub_format;
+  if (mix_format->wFormatTag == WAVE_FORMAT_EXTENSIBLE) {
+    WAVEFORMATEXTENSIBLE * format_pcm = reinterpret_cast<WAVEFORMATEXTENSIBLE *>(mix_format.get());
+    format_pcm->SubFormat = stm->waveformatextensible_sub_format;
+  }
   waveformatex_update_derived_properties(mix_format.get());
   /* Set channel layout only when there're more than two channels. Otherwise,
    * use the default setting retrieved from the stream format of the audio
    * engine's internal processing by GetMixFormat. */
   if (mix_format->nChannels > 2) {
-    handle_channel_layout(stm, direction ,mix_format, stream_params);
+    handle_channel_layout(stm, direction, mix_format, stream_params);
   }
 
   mix_params->format = stream_params->format;
   mix_params->rate = mix_format->nSamplesPerSec;
   mix_params->channels = mix_format->nChannels;
-  mix_params->layout = mask_to_channel_layout(format_pcm->dwChannelMask);
+  mix_params->layout = mask_to_channel_layout(mix_format.get());
   if (mix_params->layout == CUBEB_LAYOUT_UNDEFINED) {
     LOG("Output using undefined layout!\n");
   } else if (mix_format->nChannels != CUBEB_CHANNEL_LAYOUT_MAPS[mix_params->layout].channels) {
     // The CUBEB_CHANNEL_LAYOUT_MAPS[mix_params->layout].channels may be
     // different from the mix_params->channels. 6 channel ouput with stereo
     // layout is acceptable in Windows. If this happens, it should not downmix
     // audio according to layout.
     LOG("Channel count is different from the layout standard!\n");

