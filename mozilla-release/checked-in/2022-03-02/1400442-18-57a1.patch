# HG changeset patch
# User Steve Fink <sfink@mozilla.com>
# Date 1505521093 25200
#      Fri Sep 15 17:18:13 2017 -0700
# Node ID 1a53a959fd021f7cc31c6e0b0e5d869d5a23949a
# Parent  29b005a5b0a68135dfc3b0beb86ae111cb39f633
Bug 1400442 - Trim down whitelists to only what is required, and mark all known issues with bug numbers, r=jonco

I also snuck in some last-minute assertions and minor fixes into this patch:

- don't stop reporting for a callee if we've seen it already (or rather, make the reachable set local to a root rather than global to all roots). This slows down runs with hundreds of hazards, but results in every problematic root being reported, for a more accurate count.

- annotate away some thread assertions

- special-case annotation for bug 1400435 since it's a whole family of hazards

diff --git a/js/src/devtools/rootAnalysis/analyzeHeapWrites.js b/js/src/devtools/rootAnalysis/analyzeHeapWrites.js
--- a/js/src/devtools/rootAnalysis/analyzeHeapWrites.js
+++ b/js/src/devtools/rootAnalysis/analyzeHeapWrites.js
@@ -25,16 +25,17 @@ function checkExternalFunction(entry)
         "fmod",
         "floor",
         "ceil",
         "atof",
         /memchr/,
         "strlen",
         "Servo_ComputedValues_EqualCustomProperties",
         /Servo_DeclarationBlock_GetCssText/,
+        "Servo_GetArcStringData",
         /nsIFrame::AppendOwnedAnonBoxes/,
         // Assume that atomic accesses are threadsafe.
         /^__atomic_fetch_/,
         /^__atomic_load_/,
         /^__atomic_thread_fence/,
     ];
     if (entry.matches(whitelist))
         return;
@@ -219,16 +220,17 @@ function treatAsSafeArgument(entry, varN
         ["Gecko_StyleShapeSource_SetURLValue", "aShape", null],
         ["Gecko_nsFont_InitSystem", "aDest", null],
         ["Gecko_nsFont_SetFontFeatureValuesLookup", "aFont", null],
         ["Gecko_nsFont_ResetFontFeatureValuesLookup", "aFont", null],
         ["Gecko_nsStyleFont_FixupNoneGeneric", "aFont", null],
         ["Gecko_StyleTransition_SetUnsupportedProperty", "aTransition", null],
         ["Gecko_AddPropertyToSet", "aPropertySet", null],
         ["Gecko_CalcStyleDifference", "aAnyStyleChanged", null],
+        ["Gecko_CalcStyleDifference", "aOnlyResetStructsChanged", null],
         ["Gecko_nsStyleSVG_CopyContextProperties", "aDst", null],
         ["Gecko_nsStyleFont_PrefillDefaultForGeneric", "aFont", null],
         ["Gecko_nsStyleSVG_SetContextPropertiesLength", "aSvg", null],
         ["Gecko_ClearAlternateValues", "aFont", null],
         ["Gecko_AppendAlternateValues", "aFont", null],
         ["Gecko_CopyAlternateValuesFrom", "aDest", null],
         ["Gecko_CounterStyle_GetName", "aResult", null],
         ["Gecko_CounterStyle_GetSingleString", "aResult", null],
@@ -295,16 +297,24 @@ function checkFieldWrite(entry, location
 
         if (/\bThreadLocal<\b/.test(field))
             return;
     }
 
     var str = "";
     for (var field of fields)
         str += " " + field;
+
+    // Bug 1400435
+    if (entry.stack[entry.stack.length - 1].callee.match(/^Gecko_CSSValue_Set/) &&
+        str == " nsAutoRefCnt.mValue")
+    {
+        return;
+    }
+
     dumpError(entry, location, "Field write" + str);
 }
 
 function checkDereferenceWrite(entry, location, variable)
 {
     var name = entry.name;
 
     // Maybe<T> uses placement new on local storage in a way we don't understand.
@@ -316,16 +326,21 @@ function checkDereferenceWrite(entry, lo
     // Allow this if the UniquePtr<> is threadsafe.
     if (/UniquePtr.*?::reset/.test(name) && entry.isSafeArgument(0))
         return;
 
     // Operations on nsISupports reference counts.
     if (hasThreadsafeReferenceCounts(entry, /nsCOMPtr<T>::swap\(.*?\[with T = (.*?)\]/))
         return;
 
+    // ConvertToLowerCase::write writes through a local pointer into the first
+    // argument.
+    if (/ConvertToLowerCase::write/.test(name) && entry.isSafeArgument(0))
+        return;
+
     dumpError(entry, location, "Dereference write " + (variable ? variable : "<unknown>"));
 }
 
 function ignoreCallEdge(entry, callee)
 {
     var name = entry.name;
 
     // nsPropertyTable::GetPropertyInternal has the option of removing data
@@ -403,30 +418,32 @@ function ignoreCallEdge(entry, callee)
 function ignoreContents(entry)
 {
     var whitelist = [
         // We don't care what happens when we're about to crash.
         "abort",
         /MOZ_ReportAssertionFailure/,
         /MOZ_ReportCrash/,
         /MOZ_CrashPrintf/,
+        /MOZ_CrashOOL/,
         /AnnotateMozCrashReason/,
         /InvalidArrayIndex_CRASH/,
         /NS_ABORT_OOM/,
 
         // These ought to be threadsafe.
         "NS_DebugBreak",
         /mozalloc_handle_oom/,
         /^NS_Log/, /log_print/, /LazyLogModule::operator/,
         /SprintfLiteral/, "PR_smprintf", "PR_smprintf_free",
         /NS_DispatchToMainThread/, /NS_ReleaseOnMainThreadSystemGroup/,
         /NS_NewRunnableFunction/, /NS_Atomize/,
         /nsCSSValue::BufferFromString/,
         /NS_strdup/,
         /Assert_NoQueryNeeded/,
+        /AssertCurrentThreadOwnsMe/,
         /PlatformThread::CurrentId/,
         /imgRequestProxy::GetProgressTracker/, // Uses an AutoLock
         /Smprintf/,
         "malloc",
         "calloc",
         "free",
         "realloc",
         "memalign",
@@ -434,58 +451,54 @@ function ignoreContents(entry)
         "strndup",
         "moz_xmalloc",
         "moz_xcalloc",
         "moz_xrealloc",
         "moz_xmemalign",
         "moz_xstrdup",
         "moz_xstrndup",
         "jemalloc_thread_local_arena",
-        /profiler_register_thread/,
-        /profiler_unregister_thread/,
 
         // These all create static strings in local storage, which is threadsafe
         // to do but not understood by the analysis yet.
         / EmptyString\(\)/,
         /nsCSSProps::LookupPropertyValue/,
         /nsCSSProps::ValueToKeyword/,
         /nsCSSKeywords::GetStringValue/,
 
-        // The analysis can't cope with the indirection used for the objects
-        // being initialized here.
-        "Gecko_GetOrCreateKeyframeAtStart",
-        "Gecko_GetOrCreateInitialKeyframe",
-        "Gecko_GetOrCreateFinalKeyframe",
-        "Gecko_NewStyleQuoteValues",
-        "Gecko_NewCSSValueSharedList",
-        "Gecko_NewNoneTransform",
-        "Gecko_NewGridTemplateAreasValue",
-        /nsCSSValue::SetCalcValue/,
-        /CSSValueSerializeCalcOps::Append/,
-        "Gecko_CSSValue_SetFunction",
-        "Gecko_CSSValue_SetArray",
-        "Gecko_CSSValue_InitSharedList",
-        "Gecko_EnsureMozBorderColors",
-        "Gecko_ClearMozBorderColors",
-        "Gecko_AppendMozBorderColors",
-        "Gecko_CopyMozBorderColors",
-        "Gecko_SetNullImageValue",
+        // These could probably be handled by treating the scope of PSAutoLock
+        // aka BaseAutoLock<PSMutex> as threadsafe.
+        /profiler_register_thread/,
+        /profiler_unregister_thread/,
 
         // The analysis thinks we'll write to mBits in the DoGetStyleFoo<false>
         // call.  Maybe the template parameter confuses it?
         /nsStyleContext::PeekStyle/,
 
-        // Needs main thread assertions or other fixes.
-        /UndisplayedMap::GetEntryFor/,
+        // The analysis can't cope with the indirection used for the objects
+        // being initialized here, from nsCSSValue::Array::Create to the return
+        // value of the Item(i) getter.
+        /nsCSSValue::SetCalcValue/,
+
+        // Unable to analyze safety of linked list initialization.
+        "Gecko_NewCSSValueSharedList",
+        "Gecko_CSSValue_InitSharedList",
+
+        // Unable to trace through dataflow, but straightforward if inspected.
+        "Gecko_NewNoneTransform",
+
+        // Bug 1368922
+        "Gecko_UnsetDirtyStyleAttr",
+
+        // Bug 1400438
+        "Gecko_AppendMozBorderColors",
+
+        // Need main thread assertions or other fixes.
         /EffectCompositor::GetServoAnimationRule/,
         /Gecko_GetLookAndFeelSystemColor/,
-        "Gecko_CopyStyleContentsFrom",
-        "Gecko_CSSValue_SetPixelValue",
-        "Gecko_UnsetDirtyStyleAttr",
-        /nsCSSPropertyIDSet::AddProperty/,
     ];
     if (entry.matches(whitelist))
         return true;
 
     if (entry.isSafeArgument(0)) {
         var heapWhitelist = [
             // Operations on heap structures pointed to by arrays and strings are
             // threadsafe as long as the array/string itself is threadsafe.
@@ -783,18 +796,16 @@ if (options.verbose) {
 
 print(elapsedTime() + "Loading types...");
 if (os.getenv("TYPECACHE"))
     loadTypesWithCache('src_comp.xdb', os.getenv("TYPECACHE"));
 else
     loadTypes('src_comp.xdb');
 print(elapsedTime() + "Starting analysis...");
 
-var reachable = {};
-
 var xdb = xdbLibrary();
 xdb.open("src_body.xdb");
 
 var minStream = xdb.min_data_stream();
 var maxStream = xdb.max_data_stream();
 var roots = [];
 
 var [flag, arg] = scriptArgs;
@@ -833,16 +844,19 @@ var currentBody;
 // All local variable assignments we have seen in either the outer or inner
 // function. This crosses loop boundaries, and currently has an unsoundness
 // where later assignments in a loop are not taken into account.
 var assignments;
 
 // All loops in the current function which are reachable off main thread.
 var reachableLoops;
 
+// Functions that are reachable from the current root.
+var reachable = {};
+
 function dumpError(entry, location, text)
 {
     if (errorHeader) {
         print(errorHeader);
         errorHeader = undefined;
     }
 
     var stack = entry.stack;
@@ -1081,24 +1095,31 @@ function maybeProcessMissingFunction(ent
 }
 
 function processRoot(name)
 {
     var safeArguments = [];
     var parameterNames = {};
     var worklist = [new WorklistEntry(name, safeArguments, [new CallSite(name, safeArguments, null, parameterNames)], parameterNames)];
 
+    reachable = {};
+
+    var armed = false;
+    if (name.includes("Gecko_CSSValue_Set"))
+        armed = true;
+
     while (worklist.length > 0) {
         var entry = worklist.pop();
 
         // In principle we would be better off doing a meet-over-paths here to get
         // the common subset of arguments which are safe to write through. However,
         // analyzing functions separately for each subset if simpler, ensures that
         // the stack traces we produce accurately characterize the stack arguments,
         // and should be fast enough for now.
+
         if (entry.mangledName() in reachable)
             continue;
         reachable[entry.mangledName()] = true;
 
         if (ignoreContents(entry))
             continue;
 
         var data = xdb.read_entry(entry.name);
