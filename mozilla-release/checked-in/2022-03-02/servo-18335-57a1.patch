# HG changeset patch
# User Manish Goregaokar <manishearth@gmail.com>
# Date 1504311423 18000
#      Fri Sep 01 19:17:03 2017 -0500
# Node ID 3aada22a678f9826ccc1952b927340266f26c178
# Parent  6ceb0aff214a9a44f3fbea38eefcfce6b21ecaf2
servo: Merge #18335 - stylo: Compute font-size calcs against appropriate base size (from Manishearth:stylo-calc-anim); r=birtles

r=birtles https://bugzilla.mozilla.org/show_bug.cgi?id=1394302

Source-Repo: https://github.com/servo/servo
Source-Revision: 60daf543524e6c5e6dfb5e309ceae638425f010d

diff --git a/servo/components/style/properties/longhand/font.mako.rs b/servo/components/style/properties/longhand/font.mako.rs
--- a/servo/components/style/properties/longhand/font.mako.rs
+++ b/servo/components/style/properties/longhand/font.mako.rs
@@ -854,17 +854,17 @@ macro_rules! impl_gecko_keyword_conversi
                 }
                 SpecifiedValue::Length(LengthOrPercentage::Length(ref l)) => {
                     l.to_computed_value(context).into()
                 }
                 SpecifiedValue::Length(LengthOrPercentage::Percentage(pc)) => {
                     base_size.resolve(context).scale_by(pc.0).into()
                 }
                 SpecifiedValue::Length(LengthOrPercentage::Calc(ref calc)) => {
-                    let calc = calc.to_computed_value_zoomed(context);
+                    let calc = calc.to_computed_value_zoomed(context, base_size);
                     calc.to_used_value(Some(base_size.resolve(context))).unwrap().into()
                 }
                 SpecifiedValue::Keyword(ref key, fraction) => {
                     context.maybe_zoom_text(key.to_computed_value(context).scale_by(fraction))
                 }
                 SpecifiedValue::Smaller => {
                     FontRelativeLength::Em(1. / LARGER_FONT_SIZE_RATIO)
                         .to_computed_value(context, base_size).into()
diff --git a/servo/components/style/values/computed/length.rs b/servo/components/style/values/computed/length.rs
--- a/servo/components/style/values/computed/length.rs
+++ b/servo/components/style/values/computed/length.rs
@@ -212,17 +212,18 @@ impl ToCss for CalcLengthOrPercentage {
         length.abs().to_css(dest)?;
 
         dest.write_str(")")
     }
 }
 
 impl specified::CalcLengthOrPercentage {
     /// Compute the value, zooming any absolute units by the zoom function.
-    fn to_computed_value_with_zoom<F>(&self, context: &Context, zoom_fn: F) -> CalcLengthOrPercentage
+    fn to_computed_value_with_zoom<F>(&self, context: &Context, zoom_fn: F,
+                                      base_size: FontBaseSize) -> CalcLengthOrPercentage
         where F: Fn(Au) -> Au {
         let mut length = Au(0);
 
         if let Some(absolute) = self.absolute {
             length += zoom_fn(absolute);
         }
 
         for val in &[self.vw.map(ViewportPercentageLength::Vw),
@@ -234,38 +235,39 @@ impl specified::CalcLengthOrPercentage {
             }
         }
 
         for val in &[self.ch.map(FontRelativeLength::Ch),
                      self.em.map(FontRelativeLength::Em),
                      self.ex.map(FontRelativeLength::Ex),
                      self.rem.map(FontRelativeLength::Rem)] {
             if let Some(val) = *val {
-                length += val.to_computed_value(context, FontBaseSize::CurrentStyle);
+                length += val.to_computed_value(context, base_size);
             }
         }
 
         CalcLengthOrPercentage {
             clamping_mode: self.clamping_mode,
             length: length,
             percentage: self.percentage,
         }
     }
 
     /// Compute font-size or line-height taking into account text-zoom if necessary.
-    pub fn to_computed_value_zoomed(&self, context: &Context) -> CalcLengthOrPercentage {
-        self.to_computed_value_with_zoom(context, |abs| context.maybe_zoom_text(abs.into()).0)
+    pub fn to_computed_value_zoomed(&self, context: &Context, base_size: FontBaseSize) -> CalcLengthOrPercentage {
+        self.to_computed_value_with_zoom(context, |abs| context.maybe_zoom_text(abs.into()).0, base_size)
     }
 }
 
 impl ToComputedValue for specified::CalcLengthOrPercentage {
     type ComputedValue = CalcLengthOrPercentage;
 
     fn to_computed_value(&self, context: &Context) -> CalcLengthOrPercentage {
-        self.to_computed_value_with_zoom(context, |abs| abs)
+        // normal properties don't zoom, and compute em units against the current style's font-size
+        self.to_computed_value_with_zoom(context, |abs| abs, FontBaseSize::CurrentStyle)
     }
 
     #[inline]
     fn from_computed_value(computed: &CalcLengthOrPercentage) -> Self {
         specified::CalcLengthOrPercentage {
             clamping_mode: computed.clamping_mode,
             absolute: Some(computed.length),
             percentage: computed.percentage,
diff --git a/servo/components/style/values/specified/length.rs b/servo/components/style/values/specified/length.rs
--- a/servo/components/style/values/specified/length.rs
+++ b/servo/components/style/values/specified/length.rs
@@ -75,16 +75,17 @@ impl ToCss for FontRelativeLength {
             FontRelativeLength::Ex(length) => serialize_dimension(length, "ex", dest),
             FontRelativeLength::Ch(length) => serialize_dimension(length, "ch", dest),
             FontRelativeLength::Rem(length) => serialize_dimension(length, "rem", dest)
         }
     }
 }
 
 /// A source to resolve font-relative units against
+#[derive(Clone, Copy, Debug, PartialEq)]
 pub enum FontBaseSize {
     /// Use the font-size of the current element
     CurrentStyle,
     /// Use the inherited font-size
     InheritedStyle,
     /// Use a custom base size
     Custom(Au),
 }
diff --git a/servo/components/style/values/specified/text.rs b/servo/components/style/values/specified/text.rs
--- a/servo/components/style/values/specified/text.rs
+++ b/servo/components/style/values/specified/text.rs
@@ -107,17 +107,17 @@ impl ToComputedValue for LineHeight {
                     LengthOrPercentage::Percentage(ref p) => {
                         FontRelativeLength::Em(p.0)
                             .to_computed_value(
                                 context,
                                 FontBaseSize::CurrentStyle,
                             ).into()
                     }
                     LengthOrPercentage::Calc(ref calc) => {
-                        let computed_calc = calc.to_computed_value_zoomed(context);
+                        let computed_calc = calc.to_computed_value_zoomed(context, FontBaseSize::CurrentStyle);
                         let font_relative_length =
                             FontRelativeLength::Em(computed_calc.percentage())
                                 .to_computed_value(
                                     context,
                                     FontBaseSize::CurrentStyle,
                                 );
 
                         let absolute_length = computed_calc.unclamped_length();
