# HG changeset patch
# User Bevis Tseng <btseng@mozilla.com>
# Date 1503307822 -28800
#      Mon Aug 21 17:30:22 2017 +0800
# Node ID fe7d953913e0b272eb9b1193c2095f0e4574519e
# Parent  50fd4bb9897fb179ceeca364b04469c19900cc60
Bug 1392201 - Label nsThreadShutdownAckEvent. r=froydnj

diff --git a/xpcom/threads/nsThread.cpp b/xpcom/threads/nsThread.cpp
--- a/xpcom/threads/nsThread.cpp
+++ b/xpcom/threads/nsThread.cpp
@@ -25,16 +25,17 @@
 #include "mozilla/Logging.h"
 #include "nsIObserverService.h"
 #include "mozilla/HangMonitor.h"
 #include "mozilla/IOInterposer.h"
 #include "mozilla/ipc/MessageChannel.h"
 #include "mozilla/ipc/BackgroundChild.h"
 #include "mozilla/SchedulerGroup.h"
 #include "mozilla/Services.h"
+#include "mozilla/SystemGroup.h"
 #include "nsXPCOMPrivate.h"
 #include "mozilla/ChaosMode.h"
 #include "mozilla/Telemetry.h"
 #include "mozilla/TimeStamp.h"
 #include "mozilla/Unused.h"
 #include "mozilla/dom/ScriptSettings.h"
 #include "nsThreadSyncDispatch.h"
 #include "GeckoProfiler.h"
@@ -236,29 +237,31 @@ private:
 struct nsThreadShutdownContext
 {
   nsThreadShutdownContext(NotNull<nsThread*> aTerminatingThread,
                           NotNull<nsThread*> aJoiningThread,
                           bool      aAwaitingShutdownAck)
     : mTerminatingThread(aTerminatingThread)
     , mJoiningThread(aJoiningThread)
     , mAwaitingShutdownAck(aAwaitingShutdownAck)
+    , mIsMainThreadJoining(NS_IsMainThread())
   {
     MOZ_COUNT_CTOR(nsThreadShutdownContext);
   }
   ~nsThreadShutdownContext()
   {
     MOZ_COUNT_DTOR(nsThreadShutdownContext);
   }
 
   // NB: This will be the last reference.
   NotNull<RefPtr<nsThread>> mTerminatingThread;
   NotNull<nsThread*> MOZ_UNSAFE_REF("Thread manager is holding reference to joining thread")
     mJoiningThread;
   bool mAwaitingShutdownAck;
+  bool mIsMainThreadJoining;
 };
 
 // This event is responsible for notifying nsThread::Shutdown that it is time
 // to call PR_JoinThread. It implements nsICancelableRunnable so that it can
 // run on a DOM Worker thread (where all events must implement
 // nsICancelableRunnable.)
 class nsThreadShutdownAckEvent : public CancelableRunnable
 {
@@ -451,17 +454,21 @@ nsThread::ThreadFunc(void* aArg)
 
   profiler_unregister_thread();
 
   // Dispatch shutdown ACK
   NotNull<nsThreadShutdownContext*> context =
     WrapNotNull(self->mShutdownContext);
   MOZ_ASSERT(context->mTerminatingThread == self);
   event = do_QueryObject(new nsThreadShutdownAckEvent(context));
-  context->mJoiningThread->Dispatch(event, NS_DISPATCH_NORMAL);
+  if (context->mIsMainThreadJoining) {
+    SystemGroup::Dispatch(TaskCategory::Other, event.forget());
+  } else {
+    context->mJoiningThread->Dispatch(event, NS_DISPATCH_NORMAL);
+  }
 
   // Release any observer of the thread here.
   self->SetObserver(nullptr);
 
 #ifdef MOZ_TASK_TRACER
   FreeTraceInfo();
 #endif
 
