# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1518642589 28800
#      Wed Feb 14 13:09:49 2018 -0800
# Node ID 6e3cc809579c90edc3076cd0e30b31d6506cdbdf
# Parent  ad5f49328e078847d736a01a2c88d7efaaa7879d
Bug 1438707, part 5 - Replace XPT_ASSERT with MOZ_ASSERT. r=njn

MozReview-Commit-ID: IxLyz5pNYZ3

diff --git a/xpcom/typelib/xpt/xpt_arena.cpp b/xpcom/typelib/xpt/xpt_arena.cpp
--- a/xpcom/typelib/xpt/xpt_arena.cpp
+++ b/xpcom/typelib/xpt/xpt_arena.cpp
@@ -7,16 +7,17 @@
 /* Quick arena hack for xpt. */
 
 /* XXX This exists because we don't want to drag in NSPR. It *seemed*
 *  to make more sense to write a quick and dirty arena than to clone
 *  plarena (like js/src did). This is not optimal, but it works.
 */
 
 #include "xpt_arena.h"
+#include "mozilla/Assertions.h"
 #include "mozilla/MemoryReporting.h"
 #include <string.h>
 #include <stdio.h>
 #include <stdlib.h>
 
 /****************************************************/
 
 /* Block header for each block in the arena */
@@ -89,27 +90,27 @@ XPT_DestroyArena(XPTArena *arena)
 
 void*
 XPT_ArenaCalloc(XPTArena *arena, size_t size, size_t alignment)
 {
     if (!size)
         return NULL;
 
     if (!arena) {
-        XPT_ASSERT(0);
+        MOZ_ASSERT(false);
         return NULL;
     }
 
     XPTSubArena *subarena;
     if (alignment == 8) {
         subarena = &arena->subarena8;
     } else if (alignment == 1) {
         subarena = &arena->subarena1;
     } else {
-        XPT_ASSERT(0);
+        MOZ_ASSERT(false);
         return NULL;
     }
 
     size_t bytes = ALIGN_RND(size, alignment);
 
     if (bytes > subarena->space) {
         BLK_HDR* new_block;
         size_t block_header_size = ALIGN_RND(sizeof(BLK_HDR), alignment);
@@ -141,42 +142,32 @@ XPT_ArenaCalloc(XPTArena *arena, size_t 
 #endif
     }
 
 #ifdef DEBUG
     {
         /* do corruption check */
         size_t i;
         for (i = 0; i < bytes; ++i) {
-            XPT_ASSERT(subarena->next[i] == 0xcd);
+            MOZ_ASSERT(subarena->next[i] == 0xcd);
         }
         /* we guarantee that the block will be filled with zeros */
         memset(subarena->next, 0, bytes);
     }
 #endif
 
     uint8_t* p = subarena->next;
     subarena->next  += bytes;
     subarena->space -= bytes;
 
     return p;
 }
 
 /***************************************************************************/
 
-#ifdef DEBUG
-void
-XPT_AssertFailed(const char *s, const char *file, uint32_t lineno)
-{
-    fprintf(stderr, "Assertion failed: %s, file %s, line %d\n",
-            s, file, lineno);
-    abort();
-}
-#endif
-
 static size_t
 SizeOfSubArenaExcludingThis(XPTSubArena *subarena, MozMallocSizeOf mallocSizeOf)
 {
     size_t n = 0;
 
     BLK_HDR* cur = subarena->first;
     while (cur) {
         BLK_HDR* next = cur->next;
diff --git a/xpcom/typelib/xpt/xpt_arena.h b/xpcom/typelib/xpt/xpt_arena.h
--- a/xpcom/typelib/xpt/xpt_arena.h
+++ b/xpcom/typelib/xpt/xpt_arena.h
@@ -37,19 +37,9 @@ XPT_SizeOfArenaIncludingThis(XPTArena *a
 /* --------------------------------------------------------- */
 
 #define XPT_CALLOC8(_arena, _bytes) XPT_ArenaCalloc((_arena), (_bytes), 8)
 #define XPT_CALLOC1(_arena, _bytes) XPT_ArenaCalloc((_arena), (_bytes), 1)
 #define XPT_NEWZAP(_arena, _struct) ((_struct *) XPT_CALLOC8((_arena), sizeof(_struct)))
 
 /* --------------------------------------------------------- */
 
-#ifdef DEBUG
-void
-XPT_AssertFailed(const char *s, const char *file, uint32_t lineno)
-  MOZ_PRETEND_NORETURN_FOR_STATIC_ANALYSIS;
-#define XPT_ASSERT(_expr) \
-    ((_expr)?((void)0):XPT_AssertFailed(# _expr, __FILE__, __LINE__))
-#else
-#define XPT_ASSERT(_expr) ((void)0)
-#endif
-
 #endif /* __xpt_arena_h__ */
diff --git a/xpcom/typelib/xpt/xpt_xdr.cpp b/xpcom/typelib/xpt/xpt_xdr.cpp
--- a/xpcom/typelib/xpt/xpt_xdr.cpp
+++ b/xpcom/typelib/xpt/xpt_xdr.cpp
@@ -4,25 +4,26 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* Implementation of XDR primitives. */
 
 #include "xpt_xdr.h"
 #include "nscore.h"
 #include <string.h>             /* strchr */
+#include "mozilla/Assertions.h"
 #include "mozilla/EndianUtils.h"
 
 static size_t
 CursPoolOffsetRaw(NotNull<XPTCursor*> cursor)
 {
     if (cursor->pool == XPT_HEADER) {
         return cursor->offset;
     }
-    XPT_ASSERT(cursor->state->data_offset);
+    MOZ_ASSERT(cursor->state->data_offset);
     return cursor->offset + cursor->state->data_offset;
 }
 
 static size_t
 CursPoolOffset(NotNull<XPTCursor*> cursor)
 {
     return CursPoolOffsetRaw(cursor) - 1;
 }
@@ -35,17 +36,17 @@ CursPoint(NotNull<XPTCursor*> cursor)
 
 static bool
 CheckCount(NotNull<XPTCursor*> cursor, uint32_t space)
 {
     // Fail if we're in the data area and about to exceed the allocation.
     // XXX Also fail if we're in the data area and !state->data_offset
     if (cursor->pool == XPT_DATA &&
         (CursPoolOffset(cursor) + space > cursor->state->pool_allocated)) {
-        XPT_ASSERT(0);
+        MOZ_ASSERT(false);
         fprintf(stderr, "FATAL: no room for %u in cursor\n", space);
         return false;
     }
 
     return true;
 }
 
 void
@@ -129,17 +130,17 @@ XPT_DoCString(XPTArena *arena, NotNull<X
     char* start = CursPoint(WrapNotNull(&my_cursor));
 
     char* end = strchr(start, 0); /* find the end of the string */
     if (!end) {
         fprintf(stderr, "didn't find end of string on decode!\n");
         return false;
     }
     int len = end - start;
-    XPT_ASSERT(len > 0);
+    MOZ_ASSERT(len > 0);
 
     if (!ignore) {
         char *ident = (char*)XPT_CALLOC1(arena, len + 1u);
         if (!ident)
             return false;
 
         memcpy(ident, start, (size_t)len);
         ident[len] = 0;
