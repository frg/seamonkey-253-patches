# HG changeset patch
# User Marco Bonardo <mbonardo@mozilla.com>
# Date 1501755396 -7200
#      Thu Aug 03 12:16:36 2017 +0200
# Node ID 105df91f59e5db011f17c959332c6662da007eca
# Parent  24b9616c4f4701e9c936bfe44ad93ef966174cab
Bug 1382991 - Cleanup Places controller InsertionPoint. r=standard8

MozReview-Commit-ID: 2pSbYtPGtYx

diff --git a/browser/base/content/browser-places.js b/browser/base/content/browser-places.js
--- a/browser/base/content/browser-places.js
+++ b/browser/base/content/browser-places.js
@@ -609,48 +609,43 @@ var PlacesCommandHook = {
    */
   bookmarkCurrentPage: function PCH_bookmarkCurrentPage(aShowEditUI, aParent) {
     this.bookmarkPage(gBrowser.selectedBrowser, aParent, aShowEditUI)
         .catch(Cu.reportError);
   },
 
   /**
    * Adds a bookmark to the page targeted by a link.
-   * @param aParentId
+   * @param parentId
    *        The folder in which to create a new bookmark if aURL isn't
    *        bookmarked.
-   * @param aURL (string)
+   * @param url (string)
    *        the address of the link target
-   * @param aTitle
+   * @param title
    *        The link text
-   * @param [optional] aDescription
+   * @param [optional] description
    *        The linked page description, if available
    */
-  async bookmarkLink(aParentId, aURL, aTitle, aDescription = "") {
-    let node = await PlacesUIUtils.fetchNodeLike({ url: aURL });
+  async bookmarkLink(parentId, url, title, description = "") {
+    let node = await PlacesUIUtils.fetchNodeLike({ url });
     if (node) {
-      PlacesUIUtils.showBookmarkDialog({ action: "edit",
-                                         node
-                                       }, window.top);
+      PlacesUIUtils.showBookmarkDialog({ action: "edit", node }, window.top);
       return;
     }
 
-    let parentGuid = aParentId == PlacesUtils.bookmarksMenuFolderId ?
+    let parentGuid = parentId == PlacesUtils.bookmarksMenuFolderId ?
                        PlacesUtils.bookmarks.menuGuid :
-                       await PlacesUtils.promiseItemGuid(aParentId);
-    let ip = new InsertionPoint(aParentId,
-                                PlacesUtils.bookmarks.DEFAULT_INDEX,
-                                Ci.nsITreeView.DROP_ON,
-                                null, null, parentGuid);
+                       await PlacesUtils.promiseItemGuid(parentId);
+    let defaultInsertionPoint = new InsertionPoint({ parentId, parentGuid });
     PlacesUIUtils.showBookmarkDialog({ action: "add",
                                        type: "bookmark",
-                                       uri: makeURI(aURL),
-                                       title: aTitle,
-                                       description: aDescription,
-                                       defaultInsertionPoint: ip,
+                                       uri: makeURI(url),
+                                       title,
+                                       description,
+                                       defaultInsertionPoint,
                                        hiddenRows: [ "description",
                                                      "location",
                                                      "loadInSidebar",
                                                      "keyword" ]
                                      }, window.top);
   },
 
   /**
@@ -710,20 +705,20 @@ var PlacesCommandHook = {
    * @param     url
    *            The nsIURI of the page the feed was attached to
    * @title     title
    *            The title of the feed. Optional.
    * @subtitle  subtitle
    *            A short description of the feed. Optional.
    */
   async addLiveBookmark(url, feedTitle, feedSubtitle) {
-    let toolbarIP = new InsertionPoint(PlacesUtils.toolbarFolderId,
-                                       PlacesUtils.bookmarks.DEFAULT_INDEX,
-                                       Ci.nsITreeView.DROP_ON,
-                                       null, null, PlacesUtils.bookmarks.toolbarGuid);
+    let toolbarIP = new InsertionPoint({
+      parentId: PlacesUtils.toolbarFolderId,
+      parentGuid: PlacesUtils.bookmarks.toolbarGuid
+    });
 
     let feedURI = makeURI(url);
     let title = feedTitle || gBrowser.contentTitle;
     let description = feedSubtitle;
     if (!description) {
       description = (await this._getPageDetails(gBrowser.selectedBrowser)).description;
     }
 
@@ -1127,37 +1122,37 @@ var PlacesMenuDNDHandler = {
   },
 
   /**
    * Called when the user drags over the <menu> element.
    * @param   event
    *          The DragOver event.
    */
   onDragOver: function PMDH_onDragOver(event) {
-    let ip = new InsertionPoint(PlacesUtils.bookmarksMenuFolderId,
-                                PlacesUtils.bookmarks.DEFAULT_INDEX,
-                                Ci.nsITreeView.DROP_ON,
-                                null, null, PlacesUtils.bookmarks.menuGuid);
+    let ip = new InsertionPoint({
+      parentId: PlacesUtils.bookmarksMenuFolderId,
+      parentGuid: PlacesUtils.bookmarks.menuGuid
+    });
     if (ip && PlacesControllerDragHelper.canDrop(ip, event.dataTransfer))
       event.preventDefault();
 
     event.stopPropagation();
   },
 
   /**
    * Called when the user drops on the <menu> element.
    * @param   event
    *          The Drop event.
    */
   onDrop: function PMDH_onDrop(event) {
     // Put the item at the end of bookmark menu.
-    let ip = new InsertionPoint(PlacesUtils.bookmarksMenuFolderId,
-                                PlacesUtils.bookmarks.DEFAULT_INDEX,
-                                Ci.nsITreeView.DROP_ON,
-                                null, null, PlacesUtils.bookmarks.menuGuid);
+    let ip = new InsertionPoint({
+      parentId: PlacesUtils.bookmarksMenuFolderId,
+      parentGuid: PlacesUtils.bookmarks.menuGuid
+    });
     PlacesControllerDragHelper.onDrop(ip, event.dataTransfer);
     PlacesControllerDragHelper.currentDropTarget = null;
     event.stopPropagation();
   }
 };
 
 /**
  * This object handles the initialization and uninitialization of the bookmarks
diff --git a/browser/components/places/content/bookmarkProperties.js b/browser/components/places/content/bookmarkProperties.js
--- a/browser/components/places/content/bookmarkProperties.js
+++ b/browser/components/places/content/bookmarkProperties.js
@@ -157,20 +157,20 @@ var BookmarkPropertiesPanel = {
 
       if ("title" in dialogInfo)
         this._title = dialogInfo.title;
 
       if ("defaultInsertionPoint" in dialogInfo) {
         this._defaultInsertionPoint = dialogInfo.defaultInsertionPoint;
       } else {
         this._defaultInsertionPoint =
-          new InsertionPoint(PlacesUtils.bookmarksMenuFolderId,
-                             PlacesUtils.bookmarks.DEFAULT_INDEX,
-                             Ci.nsITreeView.DROP_ON, null, null,
-                             PlacesUtils.bookmarks.menuGuid);
+          new InsertionPoint({
+            parentId: PlacesUtils.bookmarksMenuFolderId,
+            parentGuid: PlacesUtils.bookmarks.menuGuid
+          });
       }
 
       switch (dialogInfo.type) {
         case "bookmark":
           this._itemType = BOOKMARK_ITEM;
           if ("uri" in dialogInfo) {
             NS_ASSERT(dialogInfo.uri instanceof Ci.nsIURI,
                       "uri property should be a uri object");
@@ -482,17 +482,17 @@ var BookmarkPropertiesPanel = {
    *
    * The container-identifier and insertion-index are returned separately in
    * the form of [containerIdentifier, insertionIndex]
    */
   async _getInsertionPointDetails() {
     return [
       this._defaultInsertionPoint.itemId,
       await this._defaultInsertionPoint.getIndex(),
-      await this._defaultInsertionPoint.promiseGuid(),
+      this._defaultInsertionPoint.guid,
     ]
   },
 
   /**
    * Returns a transaction for creating a new bookmark item representing the
    * various fields and opening arguments of the dialog.
    */
   _getCreateNewBookmarkTransaction:
diff --git a/browser/components/places/content/browserPlacesViews.js b/browser/components/places/content/browserPlacesViews.js
--- a/browser/components/places/content/browserPlacesViews.js
+++ b/browser/components/places/content/browserPlacesViews.js
@@ -213,19 +213,21 @@ PlacesViewBase.prototype = {
             return null;
         }
       }
     }
 
     if (PlacesControllerDragHelper.disallowInsertion(container))
       return null;
 
-    return new InsertionPoint(PlacesUtils.getConcreteItemId(container),
-                              index, orientation, tagName, null,
-                              PlacesUtils.getConcreteItemGuid(container));
+    return new InsertionPoint({
+      parentId: PlacesUtils.getConcreteItemId(container),
+      parentGuid: PlacesUtils.getConcreteItemGuid(container),
+      index, orientation, tagName
+    });
   },
 
   buildContextMenu: function PVB_buildContextMenu(aPopup) {
     this._contextMenuShown = aPopup;
     window.updateCommands("places");
     return this.controller.buildContextMenu(aPopup);
   },
 
@@ -1415,74 +1417,90 @@ PlacesToolbar.prototype = {
         // This is a folder.
         // If we are in the middle of it, drop inside it.
         // Otherwise, drop before it, with regards to RTL mode.
         let threshold = eltRect.width * 0.25;
         if (this.isRTL ? (aEvent.clientX > eltRect.right - threshold)
                        : (aEvent.clientX < eltRect.left + threshold)) {
           // Drop before this folder.
           dropPoint.ip =
-            new InsertionPoint(PlacesUtils.getConcreteItemId(this._resultNode),
-                               eltIndex, Ci.nsITreeView.DROP_BEFORE, null, null,
-                               PlacesUtils.getConcreteItemGuid(this._resultNode));
+            new InsertionPoint({
+              parentId: PlacesUtils.getConcreteItemId(this._resultNode),
+              parentGuid: PlacesUtils.getConcreteItemGuid(this._resultNode),
+              index: eltIndex,
+              orientation: Ci.nsITreeView.DROP_BEFORE
+            });
           dropPoint.beforeIndex = eltIndex;
         } else if (this.isRTL ? (aEvent.clientX > eltRect.left + threshold)
                             : (aEvent.clientX < eltRect.right - threshold)) {
           // Drop inside this folder.
           let tagName = PlacesUtils.nodeIsTagQuery(elt._placesNode) ?
                         elt._placesNode.title : null;
           dropPoint.ip =
-            new InsertionPoint(PlacesUtils.getConcreteItemId(elt._placesNode),
-                               -1, Ci.nsITreeView.DROP_ON, tagName, null,
-                               PlacesUtils.getConcreteItemGuid(elt._placesNode));
+            new InsertionPoint({
+              parentId: PlacesUtils.getConcreteItemId(elt._placesNode),
+              parentGuid: PlacesUtils.getConcreteItemGuid(elt._placesNode),
+              tagName
+            });
           dropPoint.beforeIndex = eltIndex;
           dropPoint.folderElt = elt;
         } else {
           // Drop after this folder.
           let beforeIndex =
             (eltIndex == this._rootElt.childNodes.length - 1) ?
             -1 : eltIndex + 1;
 
           dropPoint.ip =
-            new InsertionPoint(PlacesUtils.getConcreteItemId(this._resultNode),
-                               beforeIndex, Ci.nsITreeView.DROP_BEFORE, null, null,
-                               PlacesUtils.getConcreteItemGuid(this._resultNode));
+            new InsertionPoint({
+              parentId: PlacesUtils.getConcreteItemId(this._resultNode),
+              parentGuid: PlacesUtils.getConcreteItemGuid(this._resultNode),
+              index: beforeIndex,
+              orientation: Ci.nsITreeView.DROP_BEFORE
+            });
           dropPoint.beforeIndex = beforeIndex;
         }
       } else {
         // This is a non-folder node or a read-only folder.
         // Drop before it with regards to RTL mode.
         let threshold = eltRect.width * 0.5;
         if (this.isRTL ? (aEvent.clientX > eltRect.left + threshold)
                        : (aEvent.clientX < eltRect.left + threshold)) {
           // Drop before this bookmark.
           dropPoint.ip =
-            new InsertionPoint(PlacesUtils.getConcreteItemId(this._resultNode),
-                               eltIndex, Ci.nsITreeView.DROP_BEFORE, null, null,
-                               PlacesUtils.getConcreteItemGuid(this._resultNode));
+            new InsertionPoint({
+              parentId: PlacesUtils.getConcreteItemId(this._resultNode),
+              parentGuid: PlacesUtils.getConcreteItemGuid(this._resultNode),
+              index: eltIndex,
+              orientation: Ci.nsITreeView.DROP_BEFORE
+            });
           dropPoint.beforeIndex = eltIndex;
         } else {
           // Drop after this bookmark.
           let beforeIndex =
             eltIndex == this._rootElt.childNodes.length - 1 ?
             -1 : eltIndex + 1;
           dropPoint.ip =
-            new InsertionPoint(PlacesUtils.getConcreteItemId(this._resultNode),
-                               beforeIndex, Ci.nsITreeView.DROP_BEFORE, null, null,
-                               PlacesUtils.getConcreteItemGuid(this._resultNode));
+            new InsertionPoint({
+              parentId: PlacesUtils.getConcreteItemId(this._resultNode),
+              parentGuid: PlacesUtils.getConcreteItemGuid(this._resultNode),
+              index: beforeIndex,
+              orientation: Ci.nsITreeView.DROP_BEFORE
+            });
           dropPoint.beforeIndex = beforeIndex;
         }
       }
     } else {
       // We are most likely dragging on the empty area of the
       // toolbar, we should drop after the last node.
       dropPoint.ip =
-        new InsertionPoint(PlacesUtils.getConcreteItemId(this._resultNode),
-                           -1, Ci.nsITreeView.DROP_BEFORE, null, null,
-                           PlacesUtils.getConcreteItemGuid(this._resultNode));
+        new InsertionPoint({
+          parentId: PlacesUtils.getConcreteItemId(this._resultNode),
+          parentGuid: PlacesUtils.getConcreteItemGuid(this._resultNode),
+          orientation: Ci.nsITreeView.DROP_BEFORE
+        });
       dropPoint.beforeIndex = -1;
     }
 
     return dropPoint;
   },
 
   _setTimer: function PT_setTimer(aTime) {
     let timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
diff --git a/browser/components/places/content/controller.js b/browser/components/places/content/controller.js
--- a/browser/components/places/content/controller.js
+++ b/browser/components/places/content/controller.js
@@ -21,72 +21,52 @@ const RELOAD_ACTION_INSERT = 1;
 const RELOAD_ACTION_REMOVE = 2;
 // Moving items within a view, don't treat the dropped items as additional
 // rows.
 const RELOAD_ACTION_MOVE = 3;
 
 /**
  * Represents an insertion point within a container where we can insert
  * items.
- * @param   aItemId
- *          The identifier of the parent container
- * @param   aIndex
- *          The index within the container where we should insert
- * @param   aOrientation
- *          The orientation of the insertion. NOTE: the adjustments to the
- *          insertion point to accommodate the orientation should be done by
- *          the person who constructs the IP, not the user. The orientation
- *          is provided for informational purposes only!
- * @param   [optional] aTag
- *          The tag name if this IP is set to a tag, null otherwise.
- * @param   [optional] aDropNearNode
- *          When defined we will calculate index based on this node
- * @param   [optional] aGuid
- *          The guid of the parent container
- * @constructor
+ * @param {object} an object containing the following properties:
+ *   - parentId
+ *     The identifier of the parent container
+ *   - parentGuid
+ *     The unique identifier of the parent container
+ *   - index
+ *     The index within the container where to insert, defaults to appending
+ *   - orientation
+ *     The orientation of the insertion. NOTE: the adjustments to the
+ *     insertion point to accommodate the orientation should be done by
+ *     the person who constructs the IP, not the user. The orientation
+ *     is provided for informational purposes only! Defaults to DROP_ON.
+ *   - tagName
+ *     The tag name if this IP is set to a tag, null otherwise.
+ *   - dropNearNode
+ *     When defined index will be calculated based on this node
  */
-function InsertionPoint(aItemId, aIndex, aOrientation, aTagName = null,
-                        aDropNearNode = null, aGuid = null) {
-
-  this.itemId = aItemId;
-  this.guid = aGuid;
-  this._index = aIndex;
-  this.orientation = aOrientation;
-  this.tagName = aTagName;
-  this.dropNearNode = aDropNearNode;
+function InsertionPoint({ parentId, parentGuid,
+                          index = PlacesUtils.bookmarks.DEFAULT_INDEX,
+                          orientation = Components.interfaces.nsITreeView.DROP_ON,
+                          tagName = null,
+                          dropNearNode = null }) {
+  this.itemId = parentId;
+  this.guid = parentGuid;
+  this._index = index;
+  this.orientation = orientation;
+  this.tagName = tagName;
+  this.dropNearNode = dropNearNode;
 }
 
 InsertionPoint.prototype = {
   set index(val) {
     return this._index = val;
   },
 
- // TODO (Bug 1382991): Remove this backwards compatibility shim.
-  promiseGuid() {
-    return this.guid || PlacesUtils.promiseItemGuid(this.itemId);
-  },
-
-  // TODO (Bug 1382991): Remove this backwards compatibility shim.
-  get index() {
-    if (this.dropNearNode && typeof this.dropNearNode != "number")
-      throw new Error("dropNearNode is not a number, use getIndex() instead?");
-    if (this.dropNearNode > 0) {
-      // If dropNearItemId is set up we must calculate the real index of
-      // the item near which we will drop.
-      var index = PlacesUtils.bookmarks.getItemIndex(this.dropNearNode);
-      return this.orientation == Ci.nsITreeView.DROP_BEFORE ? index : index + 1;
-    }
-    return this._index;
-  },
-
   async getIndex() {
-    // TODO (Bug 1382991): Remove this backwards compatibility check.
-    if (typeof this.dropNearNode == "number")
-      return this.index;
-
     if (this.dropNearNode) {
       // If dropNearNode is set up we must calculate the index of the item near
       // which we will drop.
       let index = (await PlacesUtils.bookmarks.fetch(this.dropNearNode.bookmarkGuid)).index;
       return this.orientation == Ci.nsITreeView.DROP_BEFORE ? index : index + 1;
     }
     return this._index;
   },
@@ -783,18 +763,17 @@ PlacesController.prototype = {
       PlacesUtils.transactionManager.doTransaction(txn);
       // Select the new item.
       let insertedNodeId = PlacesUtils.bookmarks
                                       .getIdForItemAt(ip.itemId, index);
       this._view.selectItems([insertedNodeId], false);
       return;
     }
 
-    let txn = PlacesTransactions.NewSeparator({ parentGuid: await ip.promiseGuid(),
-                                                index });
+    let txn = PlacesTransactions.NewSeparator({ parentGuid: ip.guid, index });
     let guid = await txn.transact();
     let itemId = await PlacesUtils.promiseItemId(guid);
     // Select the new item.
     this._view.selectItems([itemId], false);
   },
 
   /**
    * Opens a dialog for moving the selected nodes.
@@ -1294,17 +1273,17 @@ PlacesController.prototype = {
     let itemsToSelect = [];
     if (PlacesUIUtils.useAsyncTransactions) {
       if (ip.isTag) {
         let urls = items.filter(item => "uri" in item).map(item => Services.io.newURI(item.uri));
         await PlacesTransactions.Tag({ urls, tag: ip.tagName }).transact();
       } else {
         await PlacesTransactions.batch(async function() {
           let insertionIndex = await ip.getIndex();
-          let parent = await ip.promiseGuid();
+          let parent = ip.guid;
 
           for (let item of items) {
             let doCopy = action == "copy";
 
             // If this is not a copy, check for safety that we can move the
             // source, otherwise report an error and fallback to a copy.
             if (!doCopy &&
                 !PlacesControllerDragHelper.canMoveUnwrappedNode(item)) {
@@ -1578,18 +1557,17 @@ var PlacesControllerDragHelper = {
    *          The insertion point where the items should be dropped
    */
   async onDrop(insertionPoint, dt) {
     let doCopy = ["copy", "link"].includes(dt.dropEffect);
 
     let transactions = [];
     let dropCount = dt.mozItemCount;
     let movedCount = 0;
-    let parentGuid = PlacesUIUtils.useAsyncTransactions ?
-                       (await insertionPoint.promiseGuid()) : null;
+    let parentGuid = insertionPoint.guid;
     let tagName = insertionPoint.tagName;
 
     // Following flavors may contain duplicated data.
     let duplicable = new Map();
     duplicable.set(PlacesUtils.TYPE_UNICODE, new Set());
     duplicable.set(PlacesUtils.TYPE_X_MOZ_URL, new Set());
 
     for (let i = 0; i < dropCount; ++i) {
diff --git a/browser/components/places/content/editBookmarkOverlay.js b/browser/components/places/content/editBookmarkOverlay.js
--- a/browser/components/places/content/editBookmarkOverlay.js
+++ b/browser/components/places/content/editBookmarkOverlay.js
@@ -1002,27 +1002,27 @@ var gEditItemOverlay = {
                .filter(tag => tag.length > 0); // Kill empty tags.
   },
 
   async newFolder() {
     let ip = this._folderTree.insertionPoint;
 
     // default to the bookmarks menu folder
     if (!ip || ip.itemId == PlacesUIUtils.allBookmarksFolderId) {
-      ip = new InsertionPoint(PlacesUtils.bookmarksMenuFolderId,
-                              PlacesUtils.bookmarks.DEFAULT_INDEX,
-                              Ci.nsITreeView.DROP_ON, null, null,
-                              PlacesUtils.bookmarks.menuGuid);
+      ip = new InsertionPoint({
+        parentId: PlacesUtils.bookmarksMenuFolderId,
+        parentGuid: PlacesUtils.bookmarks.menuGuid
+      });
     }
 
     // XXXmano: add a separate "New Folder" string at some point...
     let title = this._element("newFolderButton").label;
     if (PlacesUIUtils.useAsyncTransactions) {
-      let parentGuid = await ip.promiseGuid();
-      await PlacesTransactions.NewFolder({ parentGuid, title, index: await ip.getIndex() })
+      await PlacesTransactions.NewFolder({ parentGuid: ip.guid, title,
+                                           index: await ip.getIndex() })
                               .transact().catch(Cu.reportError);
     } else {
       let txn = new PlacesCreateFolderTransaction(title, ip.itemId, await ip.getIndex());
       PlacesUtils.transactionManager.doTransaction(txn);
     }
 
     this._folderTree.focus();
     this._folderTree.selectItems([ip.itemId]);
diff --git a/browser/components/places/content/menu.xml b/browser/components/places/content/menu.xml
--- a/browser/components/places/content/menu.xml
+++ b/browser/components/places/content/menu.xml
@@ -85,22 +85,20 @@
             let eventY = aEvent.layerY + (scrollbox.boxObject.y - this.boxObject.y);
             let scrollboxOffset = scrollbox.scrollBoxObject.y -
                                   (scrollbox.boxObject.y - this.boxObject.y);
             let eltY = elt.boxObject.y - scrollboxOffset;
             let eltHeight = elt.boxObject.height;
 
             if (!elt._placesNode) {
               // If we are dragging over a non places node drop at the end.
-              dropPoint.ip = new InsertionPoint(
-                                  PlacesUtils.getConcreteItemId(resultNode),
-                                  -1,
-                                  Ci.nsITreeView.DROP_ON,
-                                  null,
-                                  PlacesUtils.getConcreteItemGuid(resultNode));
+              dropPoint.ip = new InsertionPoint({
+                parentId: PlacesUtils.getConcreteItemId(resultNode),
+                parentGuid: PlacesUtils.getConcreteItemGuid(resultNode)
+              });
               // We can set folderElt if we are dropping over a static menu that
               // has an internal placespopup.
               let isMenu = elt.localName == "menu" ||
                  (elt.localName == "toolbarbutton" &&
                   elt.getAttribute("type") == "menu");
               if (isMenu && elt.lastChild &&
                   elt.lastChild.hasAttribute("placespopup"))
                 dropPoint.folderElt = elt;
@@ -110,57 +108,55 @@
             let tagName = PlacesUtils.nodeIsTagQuery(elt._placesNode) ?
                             elt._placesNode.title : null;
             if ((PlacesUtils.nodeIsFolder(elt._placesNode) &&
                  !PlacesUIUtils.isContentsReadOnly(elt._placesNode)) ||
                 PlacesUtils.nodeIsTagQuery(elt._placesNode)) {
               // This is a folder or a tag container.
               if (eventY - eltY < eltHeight * 0.20) {
                 // If mouse is in the top part of the element, drop above folder.
-                dropPoint.ip = new InsertionPoint(
-                                    PlacesUtils.getConcreteItemId(resultNode),
-                                    -1,
-                                    Ci.nsITreeView.DROP_BEFORE,
-                                    tagName,
-                                    elt._placesNode,
-                                    PlacesUtils.getConcreteItemGuid(resultNode));
+                dropPoint.ip = new InsertionPoint({
+                  parentId: PlacesUtils.getConcreteItemId(resultNode),
+                  parentGuid: PlacesUtils.getConcreteItemGuid(resultNode),
+                  orientation: Ci.nsITreeView.DROP_BEFORE,
+                  tagName,
+                  dropNearNode: elt._placesNode
+                });
                 return dropPoint;
               } else if (eventY - eltY < eltHeight * 0.80) {
                 // If mouse is in the middle of the element, drop inside folder.
-                dropPoint.ip = new InsertionPoint(
-                                    PlacesUtils.getConcreteItemId(elt._placesNode),
-                                    -1,
-                                    Ci.nsITreeView.DROP_ON,
-                                    tagName,
-                                    null,
-                                    PlacesUtils.getConcreteItemGuid(elt._placesNode));
+                dropPoint.ip = new InsertionPoint({
+                  parentId: PlacesUtils.getConcreteItemId(elt._placesNode),
+                  parentGuid: PlacesUtils.getConcreteItemGuid(elt._placesNode),
+                  tagName
+                });
                 dropPoint.folderElt = elt;
                 return dropPoint;
               }
             } else if (eventY - eltY <= eltHeight / 2) {
               // This is a non-folder node or a readonly folder.
               // If the mouse is above the middle, drop above this item.
-              dropPoint.ip = new InsertionPoint(
-                                  PlacesUtils.getConcreteItemId(resultNode),
-                                  -1,
-                                  Ci.nsITreeView.DROP_BEFORE,
-                                  tagName,
-                                  elt._placesNode,
-                                  PlacesUtils.getConcreteItemGuid(resultNode));
+              dropPoint.ip = new InsertionPoint({
+                parentId: PlacesUtils.getConcreteItemId(resultNode),
+                parentGuid: PlacesUtils.getConcreteItemGuid(resultNode),
+                orientation: Ci.nsITreeView.DROP_BEFORE,
+                tagName,
+                dropNearNode: elt._placesNode
+              });
               return dropPoint;
             }
 
             // Drop below the item.
-            dropPoint.ip = new InsertionPoint(
-                                PlacesUtils.getConcreteItemId(resultNode),
-                                -1,
-                                Ci.nsITreeView.DROP_AFTER,
-                                tagName,
-                                elt._placesNode,
-                                PlacesUtils.getConcreteItemGuid(resultNode));
+            dropPoint.ip = new InsertionPoint({
+              parentId: PlacesUtils.getConcreteItemId(resultNode),
+              parentGuid: PlacesUtils.getConcreteItemGuid(resultNode),
+              orientation: Ci.nsITreeView.DROP_AFTER,
+              tagName,
+              dropNearNode: elt._placesNode,
+            });
             return dropPoint;
         ]]></body>
       </method>
 
       <!-- Sub-menus should be opened when the mouse drags over them, and closed
            when the mouse drags off.  The overFolder object manages opening and
            closing of folders when the mouse hovers. -->
       <field name="_overFolder"><![CDATA[({
diff --git a/browser/components/places/content/tree.xml b/browser/components/places/content/tree.xml
--- a/browser/components/places/content/tree.xml
+++ b/browser/components/places/content/tree.xml
@@ -529,21 +529,21 @@
           // TODO (Bug 1160193): properly support dropping on a tag root.
           let tagName = null;
           if (PlacesUtils.nodeIsTagQuery(container)) {
             tagName = container.title;
             if (!tagName)
               return null;
           }
 
-          return new InsertionPoint(PlacesUtils.getConcreteItemId(container),
-                                    index, orientation,
-                                    tagName,
-                                    dropNearNode,
-                                    PlacesUtils.getConcreteItemGuid(container));
+          return new InsertionPoint({
+            parentId: PlacesUtils.getConcreteItemId(container),
+            parentGuid: PlacesUtils.getConcreteItemGuid(container),
+            index, orientation, tagName, dropNearNode
+          });
         ]]></body>
       </method>
 
       <!-- nsIPlacesView -->
       <method name="selectAll">
         <body><![CDATA[
           this.view.selection.selectAll();
         ]]></body>
diff --git a/browser/components/places/content/treeView.js b/browser/components/places/content/treeView.js
--- a/browser/components/places/content/treeView.js
+++ b/browser/components/places/content/treeView.js
@@ -1435,21 +1435,21 @@ PlacesTreeView.prototype = {
     // TODO (Bug 1160193): properly support dropping on a tag root.
     let tagName = null;
     if (PlacesUtils.nodeIsTagQuery(container)) {
       tagName = container.title;
       if (!tagName)
         return null;
     }
 
-    return new InsertionPoint(PlacesUtils.getConcreteItemId(container),
-                              index, orientation,
-                              tagName,
-                              dropNearNode,
-                              PlacesUtils.getConcreteItemGuid(container));
+    return new InsertionPoint({
+      parentId: PlacesUtils.getConcreteItemId(container),
+      parentGuid: PlacesUtils.getConcreteItemGuid(container),
+      index, orientation, tagName, dropNearNode
+    });
   },
 
   drop: function PTV_drop(aRow, aOrientation, aDataTransfer) {
     // We are responsible for translating the |index| and |orientation|
     // parameters into a container id and index within the container,
     // since this information is specific to the tree view.
     let ip = this._getInsertionPoint(aRow, aOrientation);
     if (ip) {
