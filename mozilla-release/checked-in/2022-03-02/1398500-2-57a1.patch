# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1505533506 14400
# Node ID 28bdfd821210b61b8d0196cbc3c99dd06b3730e9
# Parent  ef37e04f7b92934068045b136d594cd832397a65
Bug 1398500 part 2.  Make sure that if we start propagating scroll to viewport from a new body element we reframe it as needed.  r=dholbert

MozReview-Commit-ID: K54u9NmAlpn

diff --git a/layout/base/crashtests/1398500.html b/layout/base/crashtests/1398500.html
new file mode 100644
--- /dev/null
+++ b/layout/base/crashtests/1398500.html
@@ -0,0 +1,21 @@
+<!-- Quirks mode on purpose -->
+<html>
+  <head>
+    <style>
+      body { overflow: scroll; border: 1px solid green; }
+    </style>
+    <script>
+      onload = function() {
+        var newBody = document.createElement("body");
+        newBody.textContent = "This element should not have scrollbars!";
+        document.documentElement.appendChild(newBody);
+        window.nooptimize = newBody.offsetWidth;
+        document.body.remove();
+        newBody.scrollHeight; // Asserts in a debug build
+      }
+    </script>
+  </head>
+  <body>
+    First body
+  </body>
+</html>
diff --git a/layout/base/crashtests/crashtests.list b/layout/base/crashtests/crashtests.list
--- a/layout/base/crashtests/crashtests.list
+++ b/layout/base/crashtests/crashtests.list
@@ -488,10 +488,11 @@ load 1308793.svg
 load 1308848-1.html
 load 1308848-2.html
 load 1338772-1.html
 load 1343937.html
 asserts(0-1) load 1343606.html # bug 1343948
 load 1352380.html
 load 1362423-1.html
 load 1381323.html
+load 1398500.html
 pref(layout.css.resizeobserver.enabled,true) load 1548057.html
 load 1599518.html
diff --git a/layout/base/nsCSSFrameConstructor.cpp b/layout/base/nsCSSFrameConstructor.cpp
--- a/layout/base/nsCSSFrameConstructor.cpp
+++ b/layout/base/nsCSSFrameConstructor.cpp
@@ -8608,26 +8608,41 @@ nsCSSFrameConstructor::ContentRemoved(ns
   if (aFlags == REMOVE_DESTROY_FRAMES && aChild->IsElement() &&
       aChild->IsStyledByServo()) {
     ServoRestyleManager::ClearServoDataFromSubtree(aChild->AsElement());
   }
 
   nsPresContext* presContext = mPresShell->GetPresContext();
   MOZ_ASSERT(presContext, "Our presShell should have a valid presContext");
 
-  if (aChild->IsHTMLElement(nsGkAtoms::body) ||
-      (!aContainer && aChild->IsElement())) {
+  if (aChild == presContext->GetViewportScrollbarStylesOverrideElement()) {
     // We might be removing the element that we propagated viewport scrollbar
     // styles from.  Recompute those. (This clause covers two of the three
     // possible scrollbar-propagation sources: the <body> [as aChild or a
     // descendant] and the root node. The other possible scrollbar-propagation
     // source is a fullscreen element, and we have code elsewhere to update
     // scrollbars after fullscreen elements are removed -- specifically, it's
-    // part of the fullscreen cleanup code called by Element::UnbindFromTree.)
-    presContext->UpdateViewportScrollbarStylesOverride();
+    // part of the fullscreen cleanup code called by Element::UnbindFromTree.
+    // We don't handle the fullscreen case here, because it doesn't change the
+    // scrollbar styles override element stored on the prescontext.)
+    Element* newOverrideElement =
+      presContext->UpdateViewportScrollbarStylesOverride();
+
+    // Now if newOverrideElement is not the root and isn't aChild (which it
+    // could be if all we're doing here is reframing the current override
+    // element), it needs reframing.  In particular, it used to have a
+    // scrollframe (because its overflow was not "visible"), but now it will
+    // propagate its overflow to the viewport, so it should not need a
+    // scrollframe anymore.
+    if (newOverrideElement && newOverrideElement->GetParent() &&
+        newOverrideElement != aChild) {
+      LAYOUT_PHASE_TEMP_EXIT();
+      RecreateFramesForContent(newOverrideElement, InsertionKind::Async);
+      LAYOUT_PHASE_TEMP_REENTER();
+    }
   }
 
 #ifdef DEBUG
   if (gNoisyContentUpdates) {
     printf("nsCSSFrameConstructor::ContentRemoved container=%p child=%p "
            "old-next-sibling=%p\n",
            static_cast<void*>(aContainer),
            static_cast<void*>(aChild),
diff --git a/layout/reftests/bugs/1398500-1-ref.html b/layout/reftests/bugs/1398500-1-ref.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/bugs/1398500-1-ref.html
@@ -0,0 +1,11 @@
+<!DOCTYPE html>
+<html>
+  <head>
+    <style>
+      body { overflow: scroll; border: 1px solid green; }
+    </style>
+  </head>
+  <body>
+    This element should not have scrollbars!
+  </body>
+</html>
diff --git a/layout/reftests/bugs/1398500-1.html b/layout/reftests/bugs/1398500-1.html
new file mode 100644
--- /dev/null
+++ b/layout/reftests/bugs/1398500-1.html
@@ -0,0 +1,20 @@
+<!DOCTYPE html>
+<html>
+  <head>
+    <style>
+      body { overflow: scroll; border: 1px solid green; }
+    </style>
+    <script>
+      onload = function() {
+        var newBody = document.createElement("body");
+        newBody.textContent = "This element should not have scrollbars!";
+        document.documentElement.appendChild(newBody);
+        window.nooptimize = newBody.offsetWidth;
+        document.body.remove();
+      }
+    </script>
+  </head>
+  <body>
+    First body
+  </body>
+</html>
diff --git a/layout/reftests/bugs/reftest.list b/layout/reftests/bugs/reftest.list
--- a/layout/reftests/bugs/reftest.list
+++ b/layout/reftests/bugs/reftest.list
@@ -2045,8 +2045,9 @@ fails-if(!stylo||styloVsGecko) == 136516
 == 1376092.html 1376092-ref.html
 needs-focus == 1377447-1.html 1377447-1-ref.html
 needs-focus != 1377447-1.html 1377447-2.html
 == 1379041.html 1379041-ref.html
 == 1379696.html 1379696-ref.html
 == 1380224-1.html 1380224-1-ref.html
 == 1384065.html 1384065-ref.html
 == 1384275-1.html 1384275-1-ref.html
+== 1398500-1.html 1398500-1-ref.html
\ No newline at end of file
