# HG changeset patch
# User Tom Ritter <tom@mozilla.com>
# Date 1506398555 18000
#      Mon Sep 25 23:02:35 2017 -0500
# Node ID 8ffa060c744644a20bcf70e41010654c13013270
# Parent  8020e1222d8e9ceadf2fc89e2e798230f067e45e
Bug 1364560 Add support for disabling Skia Jumper assembly code r=lsalzman

The assembly code is not working in the MinGW build, so we
rebase and pull in the upstream commit that adds support for
not using the optimized jumper assembly code.

https://skia.googlesource.com/skia/+/6492afa7971cf295a3c3cb92a85218917c02bb4a

MozReview-Commit-ID: CARHRTHmQ0i

diff --git a/gfx/skia/generate_mozbuild.py b/gfx/skia/generate_mozbuild.py
--- a/gfx/skia/generate_mozbuild.py
+++ b/gfx/skia/generate_mozbuild.py
@@ -60,16 +60,18 @@ LOCAL_INCLUDES += [
     'skia/src/sfnt',
     'skia/src/sksl',
     'skia/src/utils',
     'skia/src/utils/mac',
     'skia/src/utils/win',
 ]
 
 if CONFIG['MOZ_WIDGET_TOOLKIT'] == 'windows':
+    if CONFIG['CC_TYPE'] == 'gcc':
+        DEFINES['SK_JUMPER_USE_ASSEMBLY'] = False
     DEFINES['UNICODE'] = True
     DEFINES['_UNICODE'] = True
     UNIFIED_SOURCES += [
         'skia/src/fonts/SkFontMgr_indirect.cpp',
         'skia/src/fonts/SkRemotableFontMgr.cpp',
     ]
 
 # We should autogenerate these SSE related flags.
@@ -192,16 +194,17 @@ def generate_separated_sources(platform_
     'GrGLAssembleInterface',
     'GrGLTestInterface',
     'ImageEncoder',
     'skia/src/c/',
     'skia/src/effects/Gr',
     'skia/src/effects/Sk',
     'skia/src/fonts/',
     'skia/src/images/',
+    'skia/src/jumper/SkJumper_generated_win.S',
     'skia/src/ports/SkImageGenerator',
     'skia/src/gpu/vk/',
     'SkBitmapRegion',
     'SkLight',
     'SkRadialShadow',
     'SkShadow',
     'SkNormal',
     'SkLite',
@@ -257,16 +260,19 @@ def generate_separated_sources(platform_
       'skia/src/ports/SkFontHost_FreeType_common.cpp',
       # 'skia/src/ports/SkTime_Unix.cpp',
       # 'skia/src/utils/SkThreadUtils_pthread.cpp',
     },
     'linux': {
       'skia/src/ports/SkFontHost_cairo.cpp',
       'skia/src/ports/SkFontHost_FreeType_common.cpp',
     },
+    'no-mingw': {
+      'skia/src/jumper/SkJumper_generated_win.S',
+    },
     'intel': set(),
     'arm': set(),
     'arm64': set(),
     'none': set(),
     'pdf': {
       'skia/src/core/SkMD5.cpp',
     },
     'gpu': set()
@@ -428,16 +434,18 @@ def write_mozbuild(sources):
 
   f.write("if CONFIG['MOZ_WIDGET_TOOLKIT'] in ('cocoa', 'uikit'):\n")
   write_sources(f, sources['mac'], 4)
 
   f.write("if CONFIG['MOZ_WIDGET_TOOLKIT'] in ('gtk2', 'gtk3'):\n")
   write_sources(f, sources['linux'], 4)
 
   f.write("if CONFIG['MOZ_WIDGET_TOOLKIT'] == 'windows':\n")
+  f.write("    if CONFIG['CC_TYPE'] != 'gcc':\n")
+  write_list(f, "SOURCES", sources['no-mingw'], 8)
   # Windows-specific files don't get unification because of nasty headers.
   # Luckily there are not many files in this.
   write_list(f, "SOURCES", sources['win'], 4)
 
   f.write("if CONFIG['INTEL_ARCHITECTURE']:\n")
   write_sources(f, sources['intel'], 4)
   write_cflags(f, sources['intel'], opt_whitelist, 'skia_opt_flags', 4)
 
diff --git a/gfx/skia/moz.build b/gfx/skia/moz.build
--- a/gfx/skia/moz.build
+++ b/gfx/skia/moz.build
@@ -592,18 +592,21 @@ if CONFIG['MOZ_WIDGET_TOOLKIT'] in ('gtk
         'skia/src/utils/SkThreadUtils_pthread.cpp',
     ]
     SOURCES += [
         'skia/src/jumper/SkJumper_generated.S',
         'skia/src/ports/SkFontHost_cairo.cpp',
         'skia/src/ports/SkFontHost_FreeType_common.cpp',
     ]
 if CONFIG['MOZ_WIDGET_TOOLKIT'] == 'windows':
+    if CONFIG['CC_TYPE'] != 'gcc':
+        SOURCES += [
+            'skia/src/jumper/SkJumper_generated_win.S',
+        ]
     SOURCES += [
-        'skia/src/jumper/SkJumper_generated_win.S',
         'skia/src/ports/SkDebug_win.cpp',
         'skia/src/ports/SkFontHost_win.cpp',
         'skia/src/ports/SkFontMgr_win_dw.cpp',
         'skia/src/ports/SkFontMgr_win_dw_factory.cpp',
         'skia/src/ports/SkOSFile_win.cpp',
         'skia/src/ports/SkOSLibrary_win.cpp',
         'skia/src/ports/SkScalerContext_win_dw.cpp',
         'skia/src/ports/SkTLS_win.cpp',
@@ -702,16 +705,18 @@ LOCAL_INCLUDES += [
     'skia/src/sfnt',
     'skia/src/sksl',
     'skia/src/utils',
     'skia/src/utils/mac',
     'skia/src/utils/win',
 ]
 
 if CONFIG['MOZ_WIDGET_TOOLKIT'] == 'windows':
+    if CONFIG['CC_TYPE'] == 'gcc':
+        DEFINES['SK_JUMPER_USE_ASSEMBLY'] = False
     DEFINES['UNICODE'] = True
     DEFINES['_UNICODE'] = True
     UNIFIED_SOURCES += [
         'skia/src/fonts/SkFontMgr_indirect.cpp',
         'skia/src/fonts/SkRemotableFontMgr.cpp',
     ]
 
 # We should autogenerate these SSE related flags.
diff --git a/gfx/skia/skia/src/jumper/SkJumper.cpp b/gfx/skia/skia/src/jumper/SkJumper.cpp
--- a/gfx/skia/skia/src/jumper/SkJumper.cpp
+++ b/gfx/skia/skia/src/jumper/SkJumper.cpp
@@ -7,17 +7,17 @@
 
 #include "SkColorPriv.h"
 #include "SkCpu.h"
 #include "SkJumper.h"
 #include "SkRasterPipeline.h"
 #include "SkTemplates.h"
 
 // A debugging mode that helps prioritize porting stages to SkJumper.
-#if 0
+#if 0 && SK_JUMPER_USE_ASSEMBLY
     #include "SkOnce.h"
     #include <atomic>
 
     #define M(st) {0},
     static std::atomic<int> gMissing[] = { SK_RASTER_PIPELINE_STAGES(M) };
     #undef M
 
     #define M(st) #st,
@@ -27,16 +27,23 @@
     #define WHATS_NEXT
 #endif
 
 // We'll use __has_feature(memory_sanitizer) to detect MSAN.
 // SkJumper_generated.S is not compiled with MSAN, so MSAN would yell really loud.
 #if !defined(__has_feature)
     #define __has_feature(x) 0
 #endif
+#if !defined(SK_JUMPER_USE_ASSEMBLY)
+#if __has_feature(memory_sanitizer)
+#define SK_JUMPER_USE_ASSEMBLY 0
+#else
+#define SK_JUMPER_USE_ASSEMBLY 1
+#endif
+#endif
 
 // Stages expect these constants to be set to these values.
 // It's fine to rearrange and add new ones if you update SkJumper_constants.
 using K = const SkJumper_constants;
 static K kConstants = {
     {0.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f},
 };
 
@@ -139,17 +146,17 @@ using StageFn = void(void);
 #if defined(__APPLE__)
     #define ASM(name, suffix)  sk_##name##_##suffix
 #else
     #define ASM(name, suffix) _sk_##name##_##suffix
 #endif
 
 extern "C" {
 
-#if __has_feature(memory_sanitizer)
+#if !SK_JUMPER_USE_ASSEMBLY
     // We'll just run portable code.
 
 #elif defined(__aarch64__)
     size_t ASM(start_pipeline,aarch64)(size_t, void**, K*, size_t);
     StageFn ASM(just_return,aarch64);
     #define M(st) StageFn ASM(st,aarch64);
         STAGES(M)
     #undef M
@@ -191,17 +198,17 @@ extern "C" {
     StageFn sk_just_return;
     #define M(st) StageFn sk_##st;
         STAGES(M)
     #undef M
 }
 
 // Translate SkRasterPipeline's StockStage enum to StageFn function pointers.
 
-#if __has_feature(memory_sanitizer)
+#if !SK_JUMPER_USE_ASSEMBLY
     // We'll just run portable code.
 
 #elif defined(__aarch64__)
     static StageFn* lookup_aarch64(SkRasterPipeline::StockStage st) {
         switch (st) {
             default: return nullptr;
         #define M(st) case SkRasterPipeline::st: return ASM(st,aarch64);
             STAGES(M)
@@ -311,17 +318,17 @@ bool SkRasterPipeline::run_with_jumper(s
             *ip = (void*)just_return;
 
             x = start_pipeline(x, program.get(), &kConstants, limit);
         }
         return true;
     };
 
     // While possible, build and run at full vector stride.
-#if __has_feature(memory_sanitizer)
+#if !SK_JUMPER_USE_ASSEMBLY
     // We'll just run portable code.
 
 #elif defined(__aarch64__)
     if (!build_and_run(4, lookup_aarch64, ASM(just_return,aarch64), ASM(start_pipeline,aarch64))) {
         return false;
     }
 
 #elif defined(__arm__)
