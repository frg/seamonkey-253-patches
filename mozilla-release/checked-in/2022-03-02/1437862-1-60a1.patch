# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1519143480 -3600
# Node ID 6fac52eb6527c7880af268b06e90ba48fe791c9a
# Parent  4f307cbd3ddf4f1e2cfd30ff8cb0e9d55ff1001b
Bug 1437862 part 1 - Clean up register allocation in generateArgumentsChecks. r=nbp

diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -4811,29 +4811,26 @@ CodeGenerator::visitCallDirectEval(LCall
     pushArg(envChain);
 
     callVM(DirectEvalStringInfo, lir);
 }
 
 void
 CodeGenerator::generateArgumentsChecks(bool bailout)
 {
-    // Registers safe for use before generatePrologue().
-    static const uint32_t EntryTempMask = Registers::TempMask & ~(1 << OsrFrameReg.code());
-
     // This function can be used the normal way to check the argument types,
     // before entering the function and bailout when arguments don't match.
     // For debug purpose, this is can also be used to force/check that the
     // arguments are correct. Upon fail it will hit a breakpoint.
 
     MIRGraph& mir = gen->graph();
     MResumePoint* rp = mir.entryResumePoint();
 
     // No registers are allocated yet, so it's safe to grab anything.
-    Register temp = AllocatableGeneralRegisterSet(EntryTempMask).getAny();
+    Register temp = AllocatableGeneralRegisterSet(GeneralRegisterSet::All()).getAny();
 
     const CompileInfo& info = gen->info();
 
     Label miss;
     for (uint32_t i = info.startArgSlot(); i < info.endArgSlot(); i++) {
         // All initial parameters are guaranteed to be MParameters.
         MParameter* param = rp->getOperand(i)->toParameter();
         const TypeSet* types = param->resultTypeSet();
diff --git a/js/src/jit/arm/Architecture-arm.h b/js/src/jit/arm/Architecture-arm.h
--- a/js/src/jit/arm/Architecture-arm.h
+++ b/js/src/jit/arm/Architecture-arm.h
@@ -146,19 +146,16 @@ class Registers
         VolatileMask | NonVolatileMask;
 
     static const SetType NonAllocatableMask =
         (1 << Registers::sp) |
         (1 << Registers::r12) | // r12 = ip = scratch
         (1 << Registers::lr) |
         (1 << Registers::pc);
 
-    // Registers that can be allocated without being saved, generally.
-    static const SetType TempMask = VolatileMask & ~NonAllocatableMask;
-
     // Registers returned from a JS -> JS call.
     static const SetType JSCallMask =
         (1 << Registers::r2) |
         (1 << Registers::r3);
 
     // Registers returned from a JS -> C call.
     static const SetType CallMask =
         (1 << Registers::r0) |
@@ -357,19 +354,16 @@ class FloatRegisters
     static const SetType WrapperMask = VolatileMask;
 
     // d15 is the ARM scratch float register.
     // s30 and s31 alias d15.
     static const SetType NonAllocatableMask = ((1ULL << d15)) |
                                                (1ULL << s30) |
                                                (1ULL << s31);
 
-    // Registers that can be allocated without being saved, generally.
-    static const SetType TempMask = VolatileMask & ~NonAllocatableMask;
-
     static const SetType AllocatableMask = AllMask & ~NonAllocatableMask;
 };
 
 template <typename T>
 class TypedRegisterSet;
 
 class VFPRegister
 {
diff --git a/js/src/jit/arm64/Architecture-arm64.h b/js/src/jit/arm64/Architecture-arm64.h
--- a/js/src/jit/arm64/Architecture-arm64.h
+++ b/js/src/jit/arm64/Architecture-arm64.h
@@ -162,19 +162,16 @@ class Registers {
     static const SetType NonAllocatableMask =
         (1 << Registers::x28) | // PseudoStackPointer.
         (1 << Registers::ip0) | // First scratch register.
         (1 << Registers::ip1) | // Second scratch register.
         (1 << Registers::tls) |
         (1 << Registers::lr) |
         (1 << Registers::sp);
 
-    // Registers that can be allocated without being saved, generally.
-    static const SetType TempMask = VolatileMask & ~NonAllocatableMask;
-
     static const SetType WrapperMask = VolatileMask;
 
     // Registers returned from a JS -> JS call.
     static const SetType JSCallMask = (1 << Registers::x2);
 
     // Registers returned from a JS -> C call.
     static const SetType CallMask = (1 << Registers::x0);
 
@@ -274,19 +271,16 @@ class FloatRegisters
     static const SetType AllDoubleMask = AllPhysMask << TotalPhys;
     static const SetType AllSingleMask = AllPhysMask;
 
     static const SetType WrapperMask = VolatileMask;
 
     // d31 is the ScratchFloatReg.
     static const SetType NonAllocatableMask = (SetType(1) << FloatRegisters::d31) * SpreadCoefficient;
 
-    // Registers that can be allocated without being saved, generally.
-    static const SetType TempMask = VolatileMask & ~NonAllocatableMask;
-
     static const SetType AllocatableMask = AllMask & ~NonAllocatableMask;
     union RegisterContent {
         float s;
         double d;
     };
     enum Kind {
         Double,
         Single
diff --git a/js/src/jit/mips-shared/Architecture-mips-shared.h b/js/src/jit/mips-shared/Architecture-mips-shared.h
--- a/js/src/jit/mips-shared/Architecture-mips-shared.h
+++ b/js/src/jit/mips-shared/Architecture-mips-shared.h
@@ -196,19 +196,16 @@ class Registers
         (1 << Registers::t9) | // t9 = scratch
         (1 << Registers::k0) |
         (1 << Registers::k1) |
         (1 << Registers::gp) |
         (1 << Registers::sp) |
         (1 << Registers::fp) |
         (1 << Registers::ra);
 
-    // Registers that can be allocated without being saved, generally.
-    static const SetType TempMask = VolatileMask & ~NonAllocatableMask;
-
     // Registers returned from a JS -> JS call.
     static const SetType JSCallMask;
 
     // Registers returned from a JS -> C call.
     static const SetType SharedCallMask = (1 << Registers::v0);
     static const SetType CallMask;
 
     static const SetType AllocatableMask = AllMask & ~NonAllocatableMask;
diff --git a/js/src/jit/mips32/Architecture-mips32.h b/js/src/jit/mips32/Architecture-mips32.h
--- a/js/src/jit/mips32/Architecture-mips32.h
+++ b/js/src/jit/mips32/Architecture-mips32.h
@@ -111,19 +111,16 @@ class FloatRegisters : public FloatRegis
     // f16-single and f17-single alias f16-double ...
     static const SetType NonAllocatableMask =
         NonAllocatableDoubleMask |
         (1ULL << FloatRegisters::f16) |
         (1ULL << FloatRegisters::f17) |
         (1ULL << FloatRegisters::f18) |
         (1ULL << FloatRegisters::f19);
 
-    // Registers that can be allocated without being saved, generally.
-    static const SetType TempMask = VolatileMask & ~NonAllocatableMask;
-
     static const SetType AllocatableMask = AllMask & ~NonAllocatableMask;
 };
 
 class FloatRegister : public FloatRegisterMIPSShared
 {
   public:
     enum RegType {
         Single = 0x0,
diff --git a/js/src/jit/mips64/Architecture-mips64.h b/js/src/jit/mips64/Architecture-mips64.h
--- a/js/src/jit/mips64/Architecture-mips64.h
+++ b/js/src/jit/mips64/Architecture-mips64.h
@@ -79,19 +79,16 @@ class FloatRegisters : public FloatRegis
     static const SetType WrapperMask = VolatileMask;
 
     static const SetType NonAllocatableMask =
         ( // f21 and f23 are MIPS scratch float registers.
           (1U << FloatRegisters::f21) |
           (1U << FloatRegisters::f23)
         ) * Spread;
 
-    // Registers that can be allocated without being saved, generally.
-    static const SetType TempMask = VolatileMask & ~NonAllocatableMask;
-
     static const SetType AllocatableMask = AllMask & ~NonAllocatableMask;
 };
 
 template <typename T>
 class TypedRegisterSet;
 
 class FloatRegister : public FloatRegisterMIPSShared
 {
diff --git a/js/src/jit/none/Architecture-none.h b/js/src/jit/none/Architecture-none.h
--- a/js/src/jit/none/Architecture-none.h
+++ b/js/src/jit/none/Architecture-none.h
@@ -55,17 +55,16 @@ class Registers
     static const uint32_t TotalPhys = 0;
     static const uint32_t Allocatable = 0;
     static const SetType AllMask = 0;
     static const SetType ArgRegMask = 0;
     static const SetType VolatileMask = 0;
     static const SetType NonVolatileMask = 0;
     static const SetType NonAllocatableMask = 0;
     static const SetType AllocatableMask = 0;
-    static const SetType TempMask = 0;
     static const SetType JSCallMask = 0;
     static const SetType CallMask = 0;
 };
 
 typedef uint8_t PackedRegisterMask;
 
 class FloatRegisters
 {
diff --git a/js/src/jit/x86-shared/Architecture-x86-shared.h b/js/src/jit/x86-shared/Architecture-x86-shared.h
--- a/js/src/jit/x86-shared/Architecture-x86-shared.h
+++ b/js/src/jit/x86-shared/Architecture-x86-shared.h
@@ -188,19 +188,16 @@ class Registers {
         (1 << X86Encoding::rax);
 
 #endif
 
     static const SetType NonVolatileMask =
         AllMask & ~VolatileMask & ~(1 << X86Encoding::rsp);
 
     static const SetType AllocatableMask = AllMask & ~NonAllocatableMask;
-
-    // Registers that can be allocated without being saved, generally.
-    static const SetType TempMask = VolatileMask & ~NonAllocatableMask;
 };
 
 typedef Registers::SetType PackedRegisterMask;
 
 class FloatRegisters {
   public:
     typedef X86Encoding::XMMRegisterID Encoding;
 
