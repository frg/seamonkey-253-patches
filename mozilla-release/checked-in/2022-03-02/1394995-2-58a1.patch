# HG changeset patch
# User Gerald Squelart <gsquelart@mozilla.com>
# Date 1505442673 -43200
# Node ID 829e37a5150f79a53dd54977a7b24dca4889c8dd
# Parent  6947cddff40c1473a088d3fb2b21bf9a4db808ec
Bug 1394995 - RollingNumber - r=jwwang

Unsigned-number value-class with modified comparison operators that keep
ordering consistent across overflows.
I.e., numbers before the overflow (close to the maximum) will be considered
smaller than numbers after the overflow (close to 0).
(Note that such comparisons break down for numbers separated by more than half
the type range.)

MozReview-Commit-ID: 1hdK2JknlqZ

diff --git a/dom/media/doctor/RollingNumber.h b/dom/media/doctor/RollingNumber.h
new file mode 100644
--- /dev/null
+++ b/dom/media/doctor/RollingNumber.h
@@ -0,0 +1,182 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef mozilla_RollingNumber_h_
+#define mozilla_RollingNumber_h_
+
+#include "mozilla/Attributes.h"
+#include <limits>
+
+namespace mozilla {
+
+// Unsigned number suited to index elements in a never-ending queue, as
+// order-comparison behaves nicely around the overflow.
+//
+// Additive operators work the same as for the underlying value type, but
+// expect "small" jumps, as should normally happen when manipulating indices.
+//
+// Comparison functions are different, they keep the ordering based on the
+// distance between numbers, modulo the value type range:
+// If the distance is less than half the range of the value type, the usual
+// ordering stays.
+// 0 < 1, 2^23 < 2^24
+// However if the distance is more than half the range, we assume that we are
+// continuing along the queue, and therefore consider the smaller number to
+// actually be greater!
+// uint(-1) < 0.
+//
+// The expected usage is to always work on nearby rolling numbers: slowly
+// incrementing/decrementing them, and translating&comparing them within a
+// small window.
+// To enforce this usage during development, debug-build assertions catch API
+// calls involving distances of more than a *quarter* of the type range.
+// In non-debug builds, all APIs will still work as consistently as possible
+// without crashing, but performing operations on "distant" nunbers could lead
+// to unexpected results.
+template<typename T>
+class RollingNumber
+{
+  static_assert(!std::numeric_limits<T>::is_signed,
+                "RollingNumber only accepts unsigned number types");
+
+public:
+  using ValueType = T;
+
+  RollingNumber()
+    : mIndex(0)
+  {
+  }
+
+  explicit RollingNumber(ValueType aIndex)
+    : mIndex(aIndex)
+  {
+  }
+
+  RollingNumber(const RollingNumber&) = default;
+  RollingNumber& operator=(const RollingNumber&) = default;
+
+  ValueType Value() const { return mIndex; }
+
+  // Normal increments/decrements.
+
+  RollingNumber& operator++()
+  {
+    ++mIndex;
+    return *this;
+  }
+
+  RollingNumber operator++(int) { return RollingNumber{ mIndex++ }; }
+
+  RollingNumber& operator--()
+  {
+    --mIndex;
+    return *this;
+  }
+
+  RollingNumber operator--(int) { return RollingNumber{ mIndex-- }; }
+
+  RollingNumber& operator+=(const ValueType& aIncrement)
+  {
+    MOZ_ASSERT(aIncrement <= MaxDiff);
+    mIndex += aIncrement;
+    return *this;
+  }
+
+  RollingNumber operator+(const ValueType& aIncrement) const
+  {
+    RollingNumber n = *this;
+    return n += aIncrement;
+  }
+
+  RollingNumber& operator-=(const ValueType& aDecrement)
+  {
+    MOZ_ASSERT(aDecrement <= MaxDiff);
+    mIndex -= aDecrement;
+    return *this;
+  }
+
+  // Translate a RollingNumber by a negative value.
+  RollingNumber operator-(const ValueType& aDecrement) const
+  {
+    RollingNumber n = *this;
+    return n -= aDecrement;
+  }
+
+  // Distance between two RollingNumbers, giving a value.
+  ValueType operator-(const RollingNumber& aOther) const
+  {
+    ValueType diff = mIndex - aOther.mIndex;
+    MOZ_ASSERT(diff <= MaxDiff);
+    return diff;
+  }
+
+  // Normal (in)equality operators.
+
+  bool operator==(const RollingNumber& aOther) const
+  {
+    return mIndex == aOther.mIndex;
+  }
+  bool operator!=(const RollingNumber& aOther) const
+  {
+    return !(*this == aOther);
+  }
+
+  // Modified comparison operators.
+
+  bool operator<(const RollingNumber& aOther) const
+  {
+    const T& a = mIndex;
+    const T& b = aOther.mIndex;
+    // static_cast needed because of possible integer promotion
+    // (e.g., from uint8_t to int, which would make the test useless).
+    const bool lessThanOther = static_cast<ValueType>(a - b) > MidWay;
+    MOZ_ASSERT((lessThanOther ? (b - a) : (a - b)) <= MaxDiff);
+    return lessThanOther;
+  }
+
+  bool operator<=(const RollingNumber& aOther) const
+  {
+    const T& a = mIndex;
+    const T& b = aOther.mIndex;
+    const bool lessishThanOther = static_cast<ValueType>(b - a) <= MidWay;
+    MOZ_ASSERT((lessishThanOther ? (b - a) : (a - b)) <= MaxDiff);
+    return lessishThanOther;
+  }
+
+  bool operator>=(const RollingNumber& aOther) const
+  {
+    const T& a = mIndex;
+    const T& b = aOther.mIndex;
+    const bool greaterishThanOther = static_cast<ValueType>(a - b) <= MidWay;
+    MOZ_ASSERT((greaterishThanOther ? (a - b) : (b - a)) <= MaxDiff);
+    return greaterishThanOther;
+  }
+
+  bool operator>(const RollingNumber& aOther) const
+  {
+    const T& a = mIndex;
+    const T& b = aOther.mIndex;
+    const bool greaterThanOther = static_cast<ValueType>(b - a) > MidWay;
+    MOZ_ASSERT((greaterThanOther ? (a - b) : (b - a)) <= MaxDiff);
+    return greaterThanOther;
+  }
+
+private:
+  // MidWay is used to split the type range in two, to decide how two numbers
+  // are ordered.
+  static const T MidWay = std::numeric_limits<T>::max() / 2;
+#ifdef DEBUG
+  // MaxDiff is the expected maximum difference between two numbers, either
+  // during comparisons, or when adding/subtracting.
+  // This is only used during debugging, to highlight algorithmic issues.
+  static const T MaxDiff = std::numeric_limits<T>::max() / 4;
+#endif
+  ValueType mIndex;
+};
+
+} // namespace mozilla
+
+#endif // mozilla_RollingNumber_h_
diff --git a/dom/media/doctor/gtest/TestRollingNumber.cpp b/dom/media/doctor/gtest/TestRollingNumber.cpp
new file mode 100644
--- /dev/null
+++ b/dom/media/doctor/gtest/TestRollingNumber.cpp
@@ -0,0 +1,146 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this file,
+ * You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "RollingNumber.h"
+
+#include "mozilla/Assertions.h"
+#include "mozilla/TypeTraits.h"
+
+#include <cstdint>
+#include <gtest/gtest.h>
+
+using RN8 = mozilla::RollingNumber<uint8_t>;
+
+TEST(RollingNumber, Value)
+{
+  // Value type should reflect template argument.
+  static_assert(mozilla::IsSame<RN8::ValueType, uint8_t>::value, "");
+
+  // Default init to 0.
+  const RN8 n;
+  // Access through Value().
+  EXPECT_EQ(0, n.Value());
+
+  // Conversion constructor.
+  RN8 n42{ 42 };
+  EXPECT_EQ(42, n42.Value());
+
+  // Copy Constructor.
+  RN8 n42Copied{ n42 };
+  EXPECT_EQ(42, n42Copied.Value());
+
+  // Assignment construction.
+  RN8 n42Assigned = n42;
+  EXPECT_EQ(42, n42Assigned.Value());
+
+  // Assignment.
+  n42 = n;
+  EXPECT_EQ(0, n42.Value());
+}
+
+TEST(RollingNumber, Operations)
+{
+  RN8 n;
+  EXPECT_EQ(0, n.Value());
+
+  RN8 nPreInc = ++n;
+  EXPECT_EQ(1, n.Value());
+  EXPECT_EQ(1, nPreInc.Value());
+
+  RN8 nPostInc = n++;
+  EXPECT_EQ(2, n.Value());
+  EXPECT_EQ(1, nPostInc.Value());
+
+  RN8 nPreDec = --n;
+  EXPECT_EQ(1, n.Value());
+  EXPECT_EQ(1, nPreDec.Value());
+
+  RN8 nPostDec = n--;
+  EXPECT_EQ(0, n.Value());
+  EXPECT_EQ(1, nPostDec.Value());
+
+  RN8 nPlus = n + 10;
+  EXPECT_EQ(0, n.Value());
+  EXPECT_EQ(10, nPlus.Value());
+
+  n += 20;
+  EXPECT_EQ(20, n.Value());
+
+  RN8 nMinus = n - 2;
+  EXPECT_EQ(20, n.Value());
+  EXPECT_EQ(18, nMinus.Value());
+
+  n -= 5;
+  EXPECT_EQ(15, n.Value());
+
+  uint8_t diff = nMinus - n;
+  EXPECT_EQ(3, diff);
+
+  // Overflows.
+  n = RN8(0);
+  EXPECT_EQ(0, n.Value());
+  n--;
+  EXPECT_EQ(255, n.Value());
+  n++;
+  EXPECT_EQ(0, n.Value());
+  n -= 10;
+  EXPECT_EQ(246, n.Value());
+  n += 20;
+  EXPECT_EQ(10, n.Value());
+}
+
+TEST(RollingNumber, Comparisons)
+{
+  uint8_t i = 0;
+  do {
+    RN8 n{ i };
+    EXPECT_EQ(i, n.Value());
+    EXPECT_TRUE(n == n);
+    EXPECT_FALSE(n != n);
+    EXPECT_FALSE(n < n);
+    EXPECT_TRUE(n <= n);
+    EXPECT_FALSE(n > n);
+    EXPECT_TRUE(n >= n);
+
+    RN8 same = n;
+    EXPECT_TRUE(n == same);
+    EXPECT_FALSE(n != same);
+    EXPECT_FALSE(n < same);
+    EXPECT_TRUE(n <= same);
+    EXPECT_FALSE(n > same);
+    EXPECT_TRUE(n >= same);
+
+#ifdef DEBUG
+    // In debug builds, we are only allowed a quarter of the type range.
+    const uint8_t maxDiff = 255 / 4;
+#else
+    // In non-debug builds, we can go half-way up or down the type range, and
+    // still conserve the expected ordering.
+    const uint8_t maxDiff = 255 / 2;
+#endif
+    for (uint8_t add = 1; add <= maxDiff; ++add) {
+      RN8 bigger = n + add;
+      EXPECT_FALSE(n == bigger);
+      EXPECT_TRUE(n != bigger);
+      EXPECT_TRUE(n < bigger);
+      EXPECT_TRUE(n <= bigger);
+      EXPECT_FALSE(n > bigger);
+      EXPECT_FALSE(n >= bigger);
+    }
+
+    for (uint8_t sub = 1; sub <= maxDiff; ++sub) {
+      RN8 smaller = n - sub;
+      EXPECT_FALSE(n == smaller);
+      EXPECT_TRUE(n != smaller);
+      EXPECT_FALSE(n < smaller);
+      EXPECT_FALSE(n <= smaller);
+      EXPECT_TRUE(n > smaller);
+      EXPECT_TRUE(n >= smaller);
+    }
+
+    ++i;
+  } while (i != 0);
+}
diff --git a/dom/media/doctor/gtest/moz.build b/dom/media/doctor/gtest/moz.build
new file mode 100644
--- /dev/null
+++ b/dom/media/doctor/gtest/moz.build
@@ -0,0 +1,20 @@
+# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
+# vim: set filetype=python:
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+UNIFIED_SOURCES += [
+    'TestRollingNumber.cpp',
+]
+
+include('/ipc/chromium/chromium-config.mozbuild')
+
+LOCAL_INCLUDES += [
+    '/dom/media/doctor',
+]
+
+FINAL_LIBRARY = 'xul-gtest'
+
+if CONFIG['GNU_CXX']:
+    CXXFLAGS += ['-Wno-error=shadow']
diff --git a/dom/media/doctor/moz.build b/dom/media/doctor/moz.build
--- a/dom/media/doctor/moz.build
+++ b/dom/media/doctor/moz.build
@@ -1,14 +1,18 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+TEST_DIRS += [
+    'gtest',
+]
+
 EXPORTS += [
     'DecoderDoctorDiagnostics.h',
 ]
 
 UNIFIED_SOURCES += [
     'DecoderDoctorDiagnostics.cpp',
 ]
 

