# HG changeset patch
# User Tristan Bourvon <tbourvon@mozilla.com>
# Date 1509445212 -3600
# Node ID 41566b43f9740f2a86961544e1dbd0ef1f7900f0
# Parent  c647dad1a94bf5946b244c34327d0810a86c86ed
Bug 1412648 - Initialize some uninitialized fields in tools/profiler/. r=mstange

diff --git a/tools/profiler/core/ProfileJSONWriter.h b/tools/profiler/core/ProfileJSONWriter.h
--- a/tools/profiler/core/ProfileJSONWriter.h
+++ b/tools/profiler/core/ProfileJSONWriter.h
@@ -20,17 +20,17 @@ class SpliceableChunkedJSONWriter;
 // profile are not accessed until the profile is entirely written. For these
 // reasons we use a chunked writer that keeps an array of chunks, which is
 // concatenated together after writing is finished.
 class ChunkedJSONWriteFunc : public mozilla::JSONWriteFunc
 {
 public:
   friend class SpliceableJSONWriter;
 
-  ChunkedJSONWriteFunc() {
+  ChunkedJSONWriteFunc() : mChunkPtr{nullptr}, mChunkEnd{nullptr} {
     AllocChunk(kChunkSize);
   }
 
   bool IsEmpty() const {
     MOZ_ASSERT_IF(!mChunkPtr, !mChunkEnd &&
                               mChunkList.length() == 0 &&
                               mChunkLengths.length() == 0);
     return !mChunkPtr;
diff --git a/tools/profiler/core/ProfilerMarker.h b/tools/profiler/core/ProfilerMarker.h
--- a/tools/profiler/core/ProfilerMarker.h
+++ b/tools/profiler/core/ProfilerMarker.h
@@ -22,18 +22,20 @@ class ProfilerMarker
 
 public:
   explicit ProfilerMarker(const char* aMarkerName,
                           mozilla::UniquePtr<ProfilerMarkerPayload>
                             aPayload = nullptr,
                           double aTime = 0)
     : mMarkerName(strdup(aMarkerName))
     , mPayload(Move(aPayload))
+    , mNext{nullptr}
     , mTime(aTime)
-  {}
+    , mGenID{0}
+    {}
 
   void SetGeneration(uint32_t aGenID) { mGenID = aGenID; }
 
   bool HasExpired(uint32_t aGenID) const { return mGenID + 2 <= aGenID; }
 
   double GetTime() const { return mTime; }
 
   void StreamJSON(SpliceableJSONWriter& aWriter,
diff --git a/tools/profiler/core/ThreadInfo.cpp b/tools/profiler/core/ThreadInfo.cpp
--- a/tools/profiler/core/ThreadInfo.cpp
+++ b/tools/profiler/core/ThreadInfo.cpp
@@ -24,16 +24,17 @@ ThreadInfo::ThreadInfo(const char* aName
   : mName(strdup(aName))
   , mRegisterTime(TimeStamp::Now())
   , mThreadId(aThreadId)
   , mIsMainThread(aIsMainThread)
   , mRacyInfo(mozilla::WrapNotNull(new RacyThreadInfo()))
   , mPlatformData(AllocPlatformData(aThreadId))
   , mStackTop(aStackTop)
   , mIsBeingProfiled(false)
+  , mFirstSavedStreamedSampleTime{0.0}
   , mContext(nullptr)
   , mJSSampling(INACTIVE)
   , mLastSample()
 {
   MOZ_COUNT_CTOR(ThreadInfo);
 
   // We don't have to guess on mac
 #if defined(GP_OS_darwin)
diff --git a/tools/profiler/core/platform.cpp b/tools/profiler/core/platform.cpp
--- a/tools/profiler/core/platform.cpp
+++ b/tools/profiler/core/platform.cpp
@@ -667,17 +667,17 @@ static const char* const kMainThreadName
 ////////////////////////////////////////////////////////////////////////
 // BEGIN sampling/unwinding code
 
 // The registers used for stack unwinding and a few other sampling purposes.
 // The ctor does nothing; users are responsible for filling in the fields.
 class Registers
 {
 public:
-  Registers() {}
+  Registers() : mPC{nullptr}, mSP{nullptr}, mFP{nullptr}, mLR{nullptr} {}
 
 #if defined(HAVE_NATIVE_UNWIND)
   // Fills in mPC, mSP, mFP, mLR, and mContext for a synchronous sample.
   void SyncPopulate();
 #endif
 
   void Clear() { memset(this, 0, sizeof(*this)); }
 
@@ -772,17 +772,17 @@ static const size_t MAX_JS_FRAMES     = 
 
 struct NativeStack
 {
   void* mPCs[MAX_NATIVE_FRAMES];
   void* mSPs[MAX_NATIVE_FRAMES];
   size_t mCount;  // Number of entries filled.
 
   NativeStack()
-    : mCount(0)
+    : mPCs(), mSPs(), mCount(0)
   {}
 };
 
 Atomic<bool> WALKING_JS_STACK(false);
 
 struct AutoWalkJSStack
 {
   bool walkAllowed;
@@ -2489,16 +2489,20 @@ profiler_get_start_params(int* aEntries,
   MOZ_ALWAYS_TRUE(aFilters->resize(filters.length()));
   for (uint32_t i = 0; i < filters.length(); ++i) {
     (*aFilters)[i] = filters[i].c_str();
   }
 }
 
 AutoSetProfilerEnvVarsForChildProcess::AutoSetProfilerEnvVarsForChildProcess(
   MOZ_GUARD_OBJECT_NOTIFIER_ONLY_PARAM_IN_IMPL)
+  : mSetEntries()
+  , mSetInterval()
+  , mSetFeaturesBitfield()
+  , mSetFilters()
 {
   MOZ_GUARD_OBJECT_NOTIFIER_INIT;
 
   MOZ_RELEASE_ASSERT(CorePS::Exists());
 
   PSAutoLock lock(gPSMutex);
 
   if (!ActivePS::Exists(lock)) {
diff --git a/tools/profiler/public/shared-libraries.h b/tools/profiler/public/shared-libraries.h
--- a/tools/profiler/public/shared-libraries.h
+++ b/tools/profiler/public/shared-libraries.h
@@ -104,17 +104,17 @@ public:
     return debugPathStr.get();
   }
   const nsString &GetDebugName() const { return mDebugName; }
   const nsString &GetDebugPath() const { return mDebugPath; }
   const std::string &GetVersion() const { return mVersion; }
   const std::string &GetArch() const { return mArch; }
 
 private:
-  SharedLibrary() {}
+  SharedLibrary() : mStart{0}, mEnd{0}, mOffset{0} {}
 
   uintptr_t mStart;
   uintptr_t mEnd;
   uintptr_t mOffset;
   std::string mBreakpadId;
   nsString mModuleName;
   nsString mModulePath;
   nsString mDebugName;
