# HG changeset patch
# User James Cheng <jacheng@mozilla.com>
# Date 1507701856 -28800
# Node ID c158a3b1c3f1d48025d5f8caf0a7b4bcd3c36a16
# Parent  6699bb07e755b3bca217760c73d7d5e16860fd93
Bug 1407523 - Add more detailed message when GetCDM failed. r=cpearce

MozReview-Commit-ID: qEpvjyc6bY

diff --git a/dom/media/gmp/ChromiumCDMParent.cpp b/dom/media/gmp/ChromiumCDMParent.cpp
--- a/dom/media/gmp/ChromiumCDMParent.cpp
+++ b/dom/media/gmp/ChromiumCDMParent.cpp
@@ -44,16 +44,18 @@ ChromiumCDMParent::ChromiumCDMParent(GMP
 bool
 ChromiumCDMParent::Init(ChromiumCDMCallback* aCDMCallback,
                         bool aAllowDistinctiveIdentifier,
                         bool aAllowPersistentState,
                         nsIEventTarget* aMainThread)
 {
   GMP_LOG("ChromiumCDMParent::Init(this=%p)", this);
   if (!aCDMCallback || !aMainThread) {
+    GMP_LOG("ChromiumCDMParent::Init(this=%p) failure since aCDMCallback(%p) or"
+            " aMainThread(%p) is nullptr", this, aCDMCallback, aMainThread);
     return false;
   }
   mCDMCallback = aCDMCallback;
   mMainThread = aMainThread;
   return SendInit(aAllowDistinctiveIdentifier, aAllowPersistentState);
 }
 
 void
diff --git a/dom/media/gmp/ChromiumCDMProxy.cpp b/dom/media/gmp/ChromiumCDMProxy.cpp
--- a/dom/media/gmp/ChromiumCDMProxy.cpp
+++ b/dom/media/gmp/ChromiumCDMProxy.cpp
@@ -1,16 +1,17 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ChromiumCDMProxy.h"
 #include "ChromiumCDMCallbackProxy.h"
+#include "MediaResult.h"
 #include "mozilla/dom/MediaKeySession.h"
 #include "GMPUtils.h"
 #include "nsPrintfCString.h"
 #include "GMPService.h"
 #include "content_decryption_module.h"
 
 #define NS_DispatchToMainThread(...) CompileError_UseAbstractMainThreadInstead
 
@@ -101,28 +102,28 @@ ChromiumCDMProxy::Init(PromiseId aPromis
           self->mCallback =
             MakeUnique<ChromiumCDMCallbackProxy>(self, self->mMainThread);
           if (!cdm->Init(self->mCallback.get(),
                          self->mDistinctiveIdentifierRequired,
                          self->mPersistentStateRequired,
                          self->mMainThread)) {
             self->RejectPromise(aPromiseId,
                                 NS_ERROR_FAILURE,
-                                NS_LITERAL_CSTRING("GetCDM failed."));
+                                NS_LITERAL_CSTRING("GetCDM failed due to CDM initialization failure."));
             return;
           }
           {
             MutexAutoLock lock(self->mCDMMutex);
             self->mCDM = cdm;
           }
           self->OnCDMCreated(aPromiseId);
         },
-        [self, aPromiseId](nsresult rv) {
+        [self, aPromiseId](MediaResult rv) {
           self->RejectPromise(
-            aPromiseId, NS_ERROR_FAILURE, NS_LITERAL_CSTRING("GetCDM failed."));
+            aPromiseId, rv.Code(), rv.Description());
         });
     }));
 
   mGMPThread->Dispatch(task.forget());
 }
 
 void
 ChromiumCDMProxy::OnCDMCreated(uint32_t aPromiseId)
diff --git a/dom/media/gmp/GMPService.cpp b/dom/media/gmp/GMPService.cpp
--- a/dom/media/gmp/GMPService.cpp
+++ b/dom/media/gmp/GMPService.cpp
@@ -31,16 +31,17 @@
 #include "nsDirectoryServiceUtils.h"
 #include "nsDirectoryServiceDefs.h"
 #include "nsHashKeys.h"
 #include "nsIFile.h"
 #include "nsISimpleEnumerator.h"
 #include "nsThreadUtils.h"
 #include "GMPCrashHelper.h"
 
+#include "MediaResult.h"
 #include "mozilla/dom/PluginCrashedEvent.h"
 #include "mozilla/EventDispatcher.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/SystemGroup.h"
 
 namespace mozilla {
 
 #ifdef LOG
@@ -232,44 +233,52 @@ GeckoMediaPluginService::Init()
 RefPtr<GetCDMParentPromise>
 GeckoMediaPluginService::GetCDM(const NodeId& aNodeId,
                                 nsTArray<nsCString> aTags,
                                 GMPCrashHelper* aHelper)
 {
   MOZ_ASSERT(mGMPThread->EventTarget()->IsOnCurrentThread());
 
   if (mShuttingDownOnGMPThread || aTags.IsEmpty()) {
-    return GetCDMParentPromise::CreateAndReject(NS_ERROR_FAILURE, __func__);
+    nsPrintfCString reason("%s::%s failed, aTags.IsEmpty() = %d, mShuttingDownOnGMPThread = %d.",
+      __CLASS__, __FUNCTION__, aTags.IsEmpty(), mShuttingDownOnGMPThread);
+    return GetCDMParentPromise::CreateAndReject(MediaResult(NS_ERROR_FAILURE, reason.get()), __func__);
   }
 
   typedef MozPromiseHolder<GetCDMParentPromise> PromiseHolder;
   PromiseHolder* rawHolder(new PromiseHolder());
   RefPtr<GetCDMParentPromise> promise = rawHolder->Ensure(__func__);
   RefPtr<AbstractThread> thread(GetAbstractGMPThread());
   RefPtr<GMPCrashHelper> helper(aHelper);
   GetContentParent(
     aHelper, aNodeId, NS_LITERAL_CSTRING(CHROMIUM_CDM_API), aTags)
     ->Then(thread,
            __func__,
            [rawHolder, helper](RefPtr<GMPContentParent::CloseBlocker> wrapper) {
              RefPtr<GMPContentParent> parent = wrapper->mParent;
              UniquePtr<PromiseHolder> holder(rawHolder);
              RefPtr<ChromiumCDMParent> cdm = parent->GetChromiumCDM();
              if (!parent) {
-               holder->Reject(NS_ERROR_FAILURE, __func__);
+               nsPrintfCString reason(
+                 "%s::%s failed since GetChromiumCDM returns nullptr.",
+                 __CLASS__, __FUNCTION__);
+               holder->Reject(MediaResult(NS_ERROR_FAILURE, reason.get()), __func__);
                return;
              }
              if (helper) {
                cdm->SetCrashHelper(helper);
              }
              holder->Resolve(cdm, __func__);
            },
            [rawHolder] {
+             nsPrintfCString reason(
+               "%s::%s failed since GetContentParent rejects the promise.",
+               __CLASS__, __FUNCTION__);
              UniquePtr<PromiseHolder> holder(rawHolder);
-             holder->Reject(NS_ERROR_FAILURE, __func__);
+             holder->Reject(MediaResult(NS_ERROR_FAILURE, reason.get()), __func__);
            });
 
   return promise;
 }
 
 void
 GeckoMediaPluginService::ShutdownGMPThread()
 {
diff --git a/dom/media/gmp/GMPService.h b/dom/media/gmp/GMPService.h
--- a/dom/media/gmp/GMPService.h
+++ b/dom/media/gmp/GMPService.h
@@ -20,16 +20,17 @@
 #include "nsIWeakReference.h"
 #include "mozilla/AbstractThread.h"
 #include "nsClassHashtable.h"
 #include "nsISupportsImpl.h"
 #include "mozilla/MozPromise.h"
 #include "GMPContentParent.h"
 #include "GMPCrashHelper.h"
 #include "ChromiumCDMParent.h"
+#include "MediaResult.h"
 
 template <class> struct already_AddRefed;
 
 namespace mozilla {
 
 class GMPCrashHelper;
 
 extern LogModule* GetGMPLog();
@@ -51,17 +52,17 @@ struct NodeId
   nsString mGMPName;
 };
 
 typedef MozPromise<RefPtr<GMPContentParent::CloseBlocker>,
                    nsresult,
                    /* IsExclusive = */ true>
   GetGMPContentParentPromise;
 typedef MozPromise<RefPtr<ChromiumCDMParent>,
-                   nsresult,
+                   MediaResult,
                    /* IsExclusive = */ true>
   GetCDMParentPromise;
 
 class GeckoMediaPluginService : public mozIGeckoMediaPluginService
                               , public nsIObserver
 {
 public:
   static already_AddRefed<GeckoMediaPluginService> GetGeckoMediaPluginService();
diff --git a/dom/media/gtest/TestCDMStorage.cpp b/dom/media/gtest/TestCDMStorage.cpp
--- a/dom/media/gtest/TestCDMStorage.cpp
+++ b/dom/media/gtest/TestCDMStorage.cpp
@@ -6,16 +6,17 @@
 
 #include "gtest/gtest.h"
 
 #include "mozilla/RefPtr.h"
 
 #include "ChromiumCDMCallback.h"
 #include "GMPTestMonitor.h"
 #include "GMPServiceParent.h"
+#include "MediaResult.h"
 #include "nsIFile.h"
 #include "nsISimpleEnumerator.h"
 #include "nsNSSComponent.h" //For EnsureNSSInitializedChromeOrContent
 #include "nsThreadUtils.h"
 
 using namespace mozilla;
 using namespace mozilla::gmp;
 
@@ -465,17 +466,17 @@ class CDMStorageTest
                     EXPECT_TRUE(!!self->mCDM);
                     self->mCallback.reset(new CallbackProxy(self));
                     self->mCDM->Init(self->mCallback.get(), false, true, GetMainThreadEventTarget());
 
                     for (auto& update : aUpdates) {
                       self->Update(update);
                     }
                   },
-                  [](nsresult rv) { EXPECT_TRUE(false); });
+                  [](MediaResult rv) { EXPECT_TRUE(false); });
   }
 
   void TestBasicStorage() {
     AssertIsOnGMPThread();
     EXPECT_TRUE(IsCDMStorageIsEmpty());
 
     RefPtr<GeckoMediaPluginService> service =
       GeckoMediaPluginService::GetGeckoMediaPluginService();
