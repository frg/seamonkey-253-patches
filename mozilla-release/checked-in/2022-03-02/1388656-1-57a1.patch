# HG changeset patch
# User Karl Tomlinson <karlt+@karlt.net>
# Date 1502164044 -43200
# Node ID a88ed8010517d597fd4fc94a124152575ca19f46
# Parent  eba5c78cfe2cc3723c8cba1eaa5eea36e0d45383
bug 1388656 size to actual used length in nsTArray::SetCapacity() optimization r=padenot

MozReview-Commit-ID: BGFliZTx4QL

diff --git a/dom/media/webaudio/ConvolverNode.cpp b/dom/media/webaudio/ConvolverNode.cpp
--- a/dom/media/webaudio/ConvolverNode.cpp
+++ b/dom/media/webaudio/ConvolverNode.cpp
@@ -93,17 +93,17 @@ public:
 
     if (!mBuffer || !mBufferLength || !mSampleRate) {
       mReverb = nullptr;
       mLeftOverData = INT32_MIN;
       return;
     }
 
     mReverb = new WebCore::Reverb(mBuffer, mBufferLength,
-                                  MaxFFTSize, 2, mUseBackgroundThreads,
+                                  MaxFFTSize, mUseBackgroundThreads,
                                   mNormalize, mSampleRate);
   }
 
   void ProcessBlock(AudioNodeStream* aStream,
                     GraphTime aFrom,
                     const AudioBlock& aInput,
                     AudioBlock* aOutput,
                     bool* aFinished) override
diff --git a/dom/media/webaudio/blink/Reverb.cpp b/dom/media/webaudio/blink/Reverb.cpp
--- a/dom/media/webaudio/blink/Reverb.cpp
+++ b/dom/media/webaudio/blink/Reverb.cpp
@@ -72,17 +72,17 @@ static float calculateNormalizationScale
 
     // True-stereo compensation
     if (response->GetChannels() == 4)
         scale *= 0.5f;
 
     return scale;
 }
 
-Reverb::Reverb(ThreadSharedFloatArrayBufferList* impulseResponse, size_t impulseResponseBufferLength, size_t maxFFTSize, size_t numberOfChannels, bool useBackgroundThreads, bool normalize, float sampleRate)
+Reverb::Reverb(ThreadSharedFloatArrayBufferList* impulseResponse, size_t impulseResponseBufferLength, size_t maxFFTSize, bool useBackgroundThreads, bool normalize, float sampleRate)
 {
     float scale = 1;
 
     AutoTArray<const float*,4> irChannels;
     for (size_t i = 0; i < impulseResponse->GetChannels(); ++i) {
         irChannels.AppendElement(impulseResponse->GetData(i));
     }
     AutoTArray<float,1024> tempBuf;
@@ -97,17 +97,17 @@ Reverb::Reverb(ThreadSharedFloatArrayBuf
                 AudioBufferCopyWithScale(irChannels[i], scale, buf,
                                          impulseResponseBufferLength);
                 irChannels[i] = buf;
             }
         }
     }
 
     initialize(irChannels, impulseResponseBufferLength,
-               maxFFTSize, numberOfChannels, useBackgroundThreads);
+               maxFFTSize, useBackgroundThreads);
 }
 
 size_t Reverb::sizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) const
 {
     size_t amount = aMallocSizeOf(this);
     amount += m_convolvers.ShallowSizeOfExcludingThis(aMallocSizeOf);
     for (size_t i = 0; i < m_convolvers.Length(); i++) {
         if (m_convolvers[i]) {
@@ -117,23 +117,23 @@ size_t Reverb::sizeOfIncludingThis(mozil
 
     amount += m_tempBuffer.SizeOfExcludingThis(aMallocSizeOf, false);
     return amount;
 }
 
 
 void Reverb::initialize(const nsTArray<const float*>& impulseResponseBuffer,
                         size_t impulseResponseBufferLength,
-                        size_t maxFFTSize, size_t numberOfChannels, bool useBackgroundThreads)
+                        size_t maxFFTSize, bool useBackgroundThreads)
 {
     m_impulseResponseLength = impulseResponseBufferLength;
 
     // The reverb can handle a mono impulse response and still do stereo processing
     size_t numResponseChannels = impulseResponseBuffer.Length();
-    m_convolvers.SetCapacity(numberOfChannels);
+    m_convolvers.SetCapacity(numResponseChannels);
 
     int convolverRenderPhase = 0;
     for (size_t i = 0; i < numResponseChannels; ++i) {
         const float* channel = impulseResponseBuffer[i];
         size_t length = impulseResponseBufferLength;
 
         nsAutoPtr<ReverbConvolver> convolver(new ReverbConvolver(channel, length, maxFFTSize, convolverRenderPhase, useBackgroundThreads));
         m_convolvers.AppendElement(convolver.forget());
diff --git a/dom/media/webaudio/blink/Reverb.h b/dom/media/webaudio/blink/Reverb.h
--- a/dom/media/webaudio/blink/Reverb.h
+++ b/dom/media/webaudio/blink/Reverb.h
@@ -45,30 +45,29 @@ namespace WebCore {
 
 class Reverb {
 public:
     enum { MaxFrameSize = 256 };
 
     // renderSliceSize is a rendering hint, so the FFTs can be optimized to not all occur at the same time (very bad when rendering on a real-time thread).
     Reverb(mozilla::ThreadSharedFloatArrayBufferList* impulseResponseBuffer,
            size_t impulseResponseBufferLength, size_t maxFFTSize,
-           size_t numberOfChannels, bool useBackgroundThreads, bool normalize,
-           float sampleRate);
+           bool useBackgroundThreads, bool normalize, float sampleRate);
 
     void process(const mozilla::AudioBlock* sourceBus,
                  mozilla::AudioBlock* destinationBus);
 
     size_t impulseResponseLength() const { return m_impulseResponseLength; }
 
     size_t sizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) const;
 
 private:
     void initialize(const nsTArray<const float*>& impulseResponseBuffer,
                     size_t impulseResponseBufferLength, size_t maxFFTSize,
-                    size_t numberOfChannels, bool useBackgroundThreads);
+                    bool useBackgroundThreads);
 
     size_t m_impulseResponseLength;
 
     nsTArray<nsAutoPtr<ReverbConvolver> > m_convolvers;
 
     // For "True" stereo processing
     mozilla::AudioBlock m_tempBuffer;
 };

