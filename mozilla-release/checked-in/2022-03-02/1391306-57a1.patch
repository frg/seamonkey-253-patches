# HG changeset patch
# User Ehsan Akhgari <ehsan@mozilla.com>
# Date 1502988677 14400
#      Thu Aug 17 12:51:17 2017 -0400
# Node ID 3da239c081bc81b623c765c45e9d4868b6e8bff6
# Parent  68969e75355a2f793c497dd909d98aff7fa87c39
Bug 1391306 - Reduce the hashtable lookups in CycleCollectedJSRuntime::Add/RemoveJSHolder by one each; r=mccr8

diff --git a/xpcom/base/CycleCollectedJSRuntime.cpp b/xpcom/base/CycleCollectedJSRuntime.cpp
--- a/xpcom/base/CycleCollectedJSRuntime.cpp
+++ b/xpcom/base/CycleCollectedJSRuntime.cpp
@@ -1042,25 +1042,26 @@ CycleCollectedJSRuntime::TraceNativeGray
     nsScriptObjectTracer* tracer = iter.Get().mTracer;
     tracer->Trace(holder, JsGcTracer(), aTracer);
   }
 }
 
 void
 CycleCollectedJSRuntime::AddJSHolder(void* aHolder, nsScriptObjectTracer* aTracer)
 {
-  JSHolderInfo* info = nullptr;
-  if (mJSHolderMap.Get(aHolder, &info)) {
+  auto entry = mJSHolderMap.LookupForAdd(aHolder);
+  if (entry) {
+    JSHolderInfo* info = entry.Data();
     MOZ_ASSERT(info->mHolder == aHolder);
     info->mTracer = aTracer;
     return;
   }
 
   mJSHolders.InfallibleAppend(JSHolderInfo {aHolder, aTracer});
-  mJSHolderMap.Put(aHolder, &mJSHolders.GetLast());
+  entry.OrInsert([&] {return &mJSHolders.GetLast();});
 }
 
 struct ClearJSHolder : public TraceCallbacks
 {
   virtual void Trace(JS::Heap<JS::Value>* aPtr, const char*, void*) const override
   {
     aPtr->setUndefined();
   }
@@ -1100,29 +1101,36 @@ struct ClearJSHolder : public TraceCallb
   {
     *aPtr = nullptr;
   }
 };
 
 void
 CycleCollectedJSRuntime::RemoveJSHolder(void* aHolder)
 {
-  JSHolderInfo* info = nullptr;
-  if (mJSHolderMap.Get(aHolder, &info)) {
+  auto entry = mJSHolderMap.Lookup(aHolder);
+  if (entry) {
+    JSHolderInfo* info = entry.Data();
     MOZ_ASSERT(info->mHolder == aHolder);
     info->mTracer->Trace(aHolder, ClearJSHolder(), nullptr);
 
     JSHolderInfo* lastInfo = &mJSHolders.GetLast();
-    if (info != lastInfo) {
+    bool updateLast = (info != lastInfo);
+    if (updateLast) {
       *info = *lastInfo;
-      mJSHolderMap.Put(info->mHolder, info);
     }
 
     mJSHolders.PopLast();
-    mJSHolderMap.Remove(aHolder);
+    entry.Remove();
+
+    if (updateLast) {
+      // We have to do this after removing the entry above to ensure that we
+      // don't trip over the hashtable generation number assertion.
+      mJSHolderMap.Put(info->mHolder, info);
+    }
   }
 }
 
 #ifdef DEBUG
 bool
 CycleCollectedJSRuntime::IsJSHolder(void* aHolder)
 {
   return mJSHolderMap.Get(aHolder, nullptr);
