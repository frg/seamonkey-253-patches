# HG changeset patch
# User Chris Pearce <cpearce@mozilla.com>
# Date 1502163482 -28800
# Node ID df38d825cf4f56fb728461e4fd8787c8675220f6
# Parent  6e1e5adb015bd5824361c465e3591627bb2df2f6
Bug 1388288 - Make dom/media/webaudio build non-Unified. r=padenot

MozReview-Commit-ID: Dihfqa9URpl

diff --git a/dom/media/webaudio/AudioBuffer.h b/dom/media/webaudio/AudioBuffer.h
--- a/dom/media/webaudio/AudioBuffer.h
+++ b/dom/media/webaudio/AudioBuffer.h
@@ -11,16 +11,18 @@
 #include "nsCycleCollectionParticipant.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/StaticPtr.h"
 #include "mozilla/StaticMutex.h"
 #include "nsTArray.h"
 #include "js/TypeDecls.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/dom/TypedArray.h"
+#include "nsPIDOMWindow.h"
+#include "nsIWeakReferenceUtils.h"
 
 namespace mozilla {
 
 class ErrorResult;
 class ThreadSharedFloatArrayBufferList;
 
 namespace dom {
 
diff --git a/dom/media/webaudio/AudioContext.cpp b/dom/media/webaudio/AudioContext.cpp
--- a/dom/media/webaudio/AudioContext.cpp
+++ b/dom/media/webaudio/AudioContext.cpp
@@ -36,16 +36,17 @@
 #include "mozilla/dom/StereoPannerNodeBinding.h"
 #include "mozilla/dom/WaveShaperNodeBinding.h"
 
 #include "AudioBuffer.h"
 #include "AudioBufferSourceNode.h"
 #include "AudioChannelService.h"
 #include "AudioDestinationNode.h"
 #include "AudioListener.h"
+#include "AudioNodeStream.h"
 #include "AudioStream.h"
 #include "BiquadFilterNode.h"
 #include "ChannelMergerNode.h"
 #include "ChannelSplitterNode.h"
 #include "ConstantSourceNode.h"
 #include "ConvolverNode.h"
 #include "DelayNode.h"
 #include "DynamicsCompressorNode.h"
diff --git a/dom/media/webaudio/AudioDestinationNode.cpp b/dom/media/webaudio/AudioDestinationNode.cpp
--- a/dom/media/webaudio/AudioDestinationNode.cpp
+++ b/dom/media/webaudio/AudioDestinationNode.cpp
@@ -1,20 +1,22 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "AudioDestinationNode.h"
+#include "AudioContext.h"
 #include "AlignmentUtils.h"
 #include "AudioContext.h"
 #include "mozilla/dom/AudioDestinationNodeBinding.h"
 #include "mozilla/dom/OfflineAudioCompletionEvent.h"
 #include "mozilla/dom/ScriptSettings.h"
+#include "mozilla/dom/BaseAudioContextBinding.h"
 #include "mozilla/Services.h"
 #include "AudioChannelAgent.h"
 #include "AudioChannelService.h"
 #include "AudioNodeEngine.h"
 #include "AudioNodeStream.h"
 #include "MediaStreamGraph.h"
 #include "nsContentUtils.h"
 #include "nsIInterfaceRequestorUtils.h"
diff --git a/dom/media/webaudio/AudioEventTimeline.cpp b/dom/media/webaudio/AudioEventTimeline.cpp
--- a/dom/media/webaudio/AudioEventTimeline.cpp
+++ b/dom/media/webaudio/AudioEventTimeline.cpp
@@ -1,15 +1,16 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "AudioEventTimeline.h"
+#include "MediaStreamGraph.h"
 
 #include "mozilla/ErrorResult.h"
 
 static float LinearInterpolate(double t0, float v0, double t1, float v1, double t)
 {
   return v0 + (v1 - v0) * ((t - t0) / (t1 - t0));
 }
 
@@ -47,16 +48,69 @@ static float ExtractValueFromCurve(doubl
   } else {
     return aCurve[current];
   }
 }
 
 namespace mozilla {
 namespace dom {
 
+AudioTimelineEvent::AudioTimelineEvent(Type aType,
+                                       double aTime,
+                                       float aValue,
+                                       double aTimeConstant,
+                                       double aDuration,
+                                       const float* aCurve,
+                                       uint32_t aCurveLength)
+  : mType(aType)
+  , mCurve(nullptr)
+  , mTimeConstant(aTimeConstant)
+  , mDuration(aDuration)
+#ifdef DEBUG
+  , mTimeIsInTicks(false)
+#endif
+{
+  mTime = aTime;
+  if (aType == AudioTimelineEvent::SetValueCurve) {
+    SetCurveParams(aCurve, aCurveLength);
+  } else {
+    mValue = aValue;
+  }
+}
+
+AudioTimelineEvent::AudioTimelineEvent(MediaStream* aStream)
+  : mType(Stream)
+  , mCurve(nullptr)
+  , mStream(aStream)
+  , mTimeConstant(0.0)
+  , mDuration(0.0)
+#ifdef DEBUG
+  , mTimeIsInTicks(false)
+#endif
+{
+}
+
+AudioTimelineEvent::AudioTimelineEvent(const AudioTimelineEvent& rhs)
+{
+  PodCopy(this, &rhs, 1);
+
+  if (rhs.mType == AudioTimelineEvent::SetValueCurve) {
+    SetCurveParams(rhs.mCurve, rhs.mCurveLength);
+  } else if (rhs.mType == AudioTimelineEvent::Stream) {
+    new (&mStream) decltype(mStream)(rhs.mStream);
+  }
+}
+
+AudioTimelineEvent::~AudioTimelineEvent()
+{
+  if (mType == AudioTimelineEvent::SetValueCurve) {
+    delete[] mCurve;
+  }
+}
+
 // This method computes the AudioParam value at a given time based on the event timeline
 template<class TimeType> void
 AudioEventTimeline::GetValuesAtTimeHelper(TimeType aTime, float* aBuffer,
                                           const size_t aSize)
 {
   MOZ_ASSERT(aBuffer);
   MOZ_ASSERT(aSize);
 
diff --git a/dom/media/webaudio/AudioEventTimeline.h b/dom/media/webaudio/AudioEventTimeline.h
--- a/dom/media/webaudio/AudioEventTimeline.h
+++ b/dom/media/webaudio/AudioEventTimeline.h
@@ -32,64 +32,26 @@ struct AudioTimelineEvent final
     LinearRamp,
     ExponentialRamp,
     SetTarget,
     SetValueCurve,
     Stream,
     Cancel
   };
 
-  AudioTimelineEvent(Type aType, double aTime, float aValue, double aTimeConstant = 0.0,
-                     double aDuration = 0.0, const float* aCurve = nullptr,
-                     uint32_t aCurveLength = 0)
-    : mType(aType)
-    , mCurve(nullptr)
-    , mTimeConstant(aTimeConstant)
-    , mDuration(aDuration)
-#ifdef DEBUG
-    , mTimeIsInTicks(false)
-#endif
-  {
-    mTime = aTime;
-    if (aType == AudioTimelineEvent::SetValueCurve) {
-      SetCurveParams(aCurve, aCurveLength);
-    } else {
-      mValue = aValue;
-    }
-  }
-
-  explicit AudioTimelineEvent(MediaStream* aStream)
-    : mType(Stream)
-    , mCurve(nullptr)
-    , mStream(aStream)
-    , mTimeConstant(0.0)
-    , mDuration(0.0)
-#ifdef DEBUG
-    , mTimeIsInTicks(false)
-#endif
-  {
-  }
-
-  AudioTimelineEvent(const AudioTimelineEvent& rhs)
-  {
-    PodCopy(this, &rhs, 1);
-
-    if (rhs.mType == AudioTimelineEvent::SetValueCurve) {
-      SetCurveParams(rhs.mCurve, rhs.mCurveLength);
-    } else if (rhs.mType == AudioTimelineEvent::Stream) {
-      new (&mStream) decltype(mStream)(rhs.mStream);
-    }
-  }
-
-  ~AudioTimelineEvent()
-  {
-    if (mType == AudioTimelineEvent::SetValueCurve) {
-      delete[] mCurve;
-    }
-  }
+  AudioTimelineEvent(Type aType,
+                     double aTime,
+                     float aValue,
+                     double aTimeConstant = 0.0,
+                     double aDuration = 0.0,
+                     const float* aCurve = nullptr,
+                     uint32_t aCurveLength = 0);
+  explicit AudioTimelineEvent(MediaStream* aStream);
+  AudioTimelineEvent(const AudioTimelineEvent& rhs);
+  ~AudioTimelineEvent();
 
   template <class TimeType>
   TimeType Time() const;
 
   void SetTimeInTicks(int64_t aTimeInTicks)
   {
     mTimeInTicks = aTimeInTicks;
 #ifdef DEBUG
diff --git a/dom/media/webaudio/AudioNodeEngine.cpp b/dom/media/webaudio/AudioNodeEngine.cpp
--- a/dom/media/webaudio/AudioNodeEngine.cpp
+++ b/dom/media/webaudio/AudioNodeEngine.cpp
@@ -11,16 +11,17 @@
 #include "mozilla/arm.h"
 #include "AudioNodeEngineNEON.h"
 #endif
 #ifdef USE_SSE2
 #include "mozilla/SSE.h"
 #include "AlignmentUtils.h"
 #include "AudioNodeEngineSSE2.h"
 #endif
+#include "AudioBlock.h"
 
 namespace mozilla {
 
 already_AddRefed<ThreadSharedFloatArrayBufferList>
 ThreadSharedFloatArrayBufferList::Create(uint32_t aChannelCount,
                                          size_t aLength,
                                          const mozilla::fallible_t&)
 {
diff --git a/dom/media/webaudio/AudioNodeStream.cpp b/dom/media/webaudio/AudioNodeStream.cpp
--- a/dom/media/webaudio/AudioNodeStream.cpp
+++ b/dom/media/webaudio/AudioNodeStream.cpp
@@ -8,16 +8,17 @@
 #include "MediaStreamGraphImpl.h"
 #include "MediaStreamListener.h"
 #include "AudioNodeEngine.h"
 #include "ThreeDPoint.h"
 #include "AudioChannelFormat.h"
 #include "AudioParamTimeline.h"
 #include "AudioContext.h"
 #include "nsMathUtils.h"
+#include "AlignmentUtils.h"
 
 using namespace mozilla::dom;
 
 namespace mozilla {
 
 /**
  * An AudioNodeStream produces a single audio track with ID
  * AUDIO_TRACK. This track has rate AudioContext::sIdealAudioRate
diff --git a/dom/media/webaudio/ConstantSourceNode.cpp b/dom/media/webaudio/ConstantSourceNode.cpp
--- a/dom/media/webaudio/ConstantSourceNode.cpp
+++ b/dom/media/webaudio/ConstantSourceNode.cpp
@@ -3,16 +3,18 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ConstantSourceNode.h"
 
 #include "AudioDestinationNode.h"
 #include "nsContentUtils.h"
+#include "AudioNodeEngine.h"
+#include "AudioNodeStream.h"
 
 namespace mozilla {
 namespace dom {
 
 NS_IMPL_CYCLE_COLLECTION_INHERITED(ConstantSourceNode, AudioScheduledSourceNode,
                                    mOffset)
 
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION_INHERITED(ConstantSourceNode)
diff --git a/dom/media/webaudio/DelayBuffer.h b/dom/media/webaudio/DelayBuffer.h
--- a/dom/media/webaudio/DelayBuffer.h
+++ b/dom/media/webaudio/DelayBuffer.h
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef DelayBuffer_h_
 #define DelayBuffer_h_
 
 #include "nsTArray.h"
+#include "AudioBlock.h"
 #include "AudioSegment.h"
 #include "mozilla/dom/AudioNodeBinding.h" // for ChannelInterpretation
 
 namespace mozilla {
 
 class DelayBuffer final
 {
   typedef dom::ChannelInterpretation ChannelInterpretation;
diff --git a/dom/media/webaudio/IIRFilterNode.cpp b/dom/media/webaudio/IIRFilterNode.cpp
--- a/dom/media/webaudio/IIRFilterNode.cpp
+++ b/dom/media/webaudio/IIRFilterNode.cpp
@@ -1,18 +1,20 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "IIRFilterNode.h"
 #include "AudioNodeEngine.h"
-
+#include "AudioDestinationNode.h"
 #include "blink/IIRFilter.h"
+#include "PlayingRefChangeHandler.h"
+#include "AlignmentUtils.h"
 
 #include "nsGkAtoms.h"
 
 namespace mozilla {
 namespace dom {
 
 NS_IMPL_ISUPPORTS_INHERITED0(IIRFilterNode, AudioNode)
 
diff --git a/dom/media/webaudio/MediaBufferDecoder.cpp b/dom/media/webaudio/MediaBufferDecoder.cpp
--- a/dom/media/webaudio/MediaBufferDecoder.cpp
+++ b/dom/media/webaudio/MediaBufferDecoder.cpp
@@ -24,16 +24,17 @@
 #include "nsIScriptObjectPrincipal.h"
 #include "nsIScriptError.h"
 #include "nsMimeTypes.h"
 #include "VideoUtils.h"
 #include "WebAudioUtils.h"
 #include "mozilla/dom/Promise.h"
 #include "mozilla/Telemetry.h"
 #include "nsPrintfCString.h"
+#include "AudioNodeEngine.h"
 
 namespace mozilla {
 
 extern LazyLogModule gMediaDecoderLog;
 
 using namespace dom;
 
 class ReportResultTask final : public Runnable
diff --git a/dom/media/webaudio/MediaElementAudioSourceNode.cpp b/dom/media/webaudio/MediaElementAudioSourceNode.cpp
--- a/dom/media/webaudio/MediaElementAudioSourceNode.cpp
+++ b/dom/media/webaudio/MediaElementAudioSourceNode.cpp
@@ -1,16 +1,19 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "MediaElementAudioSourceNode.h"
 #include "mozilla/dom/MediaElementAudioSourceNodeBinding.h"
+#include "AudioDestinationNode.h"
+#include "nsIScriptError.h"
+#include "AudioNodeStream.h"
 
 namespace mozilla {
 namespace dom {
 
 MediaElementAudioSourceNode::MediaElementAudioSourceNode(AudioContext* aContext)
   : MediaStreamAudioSourceNode(aContext)
 {
 }
diff --git a/dom/media/webaudio/MediaStreamAudioSourceNode.cpp b/dom/media/webaudio/MediaStreamAudioSourceNode.cpp
--- a/dom/media/webaudio/MediaStreamAudioSourceNode.cpp
+++ b/dom/media/webaudio/MediaStreamAudioSourceNode.cpp
@@ -6,16 +6,18 @@
 
 #include "MediaStreamAudioSourceNode.h"
 #include "mozilla/dom/MediaStreamAudioSourceNodeBinding.h"
 #include "AudioNodeEngine.h"
 #include "AudioNodeExternalInputStream.h"
 #include "AudioStreamTrack.h"
 #include "nsIDocument.h"
 #include "mozilla/CORSMode.h"
+#include "nsContentUtils.h"
+#include "nsIScriptError.h"
 
 namespace mozilla {
 namespace dom {
 
 NS_IMPL_CYCLE_COLLECTION_CLASS(MediaStreamAudioSourceNode)
 
 NS_IMPL_CYCLE_COLLECTION_UNLINK_BEGIN(MediaStreamAudioSourceNode)
   tmp->Destroy();
diff --git a/dom/media/webaudio/WebAudioUtils.cpp b/dom/media/webaudio/WebAudioUtils.cpp
--- a/dom/media/webaudio/WebAudioUtils.cpp
+++ b/dom/media/webaudio/WebAudioUtils.cpp
@@ -6,16 +6,17 @@
 
 #include "WebAudioUtils.h"
 #include "AudioNodeStream.h"
 #include "blink/HRTFDatabaseLoader.h"
 
 #include "nsContentUtils.h"
 #include "nsIConsoleService.h"
 #include "nsIScriptError.h"
+#include "AudioEventTimeline.h"
 
 namespace mozilla {
 
 LazyLogModule gWebAudioAPILog("WebAudioAPI");
 
 namespace dom {
 
 void WebAudioUtils::ConvertAudioTimelineEventToTicks(AudioTimelineEvent& aEvent,
diff --git a/dom/media/webaudio/blink/IIRFilter.h b/dom/media/webaudio/blink/IIRFilter.h
--- a/dom/media/webaudio/blink/IIRFilter.h
+++ b/dom/media/webaudio/blink/IIRFilter.h
@@ -1,15 +1,17 @@
 // Copyright 2016 The Chromium Authors. All rights reserved.
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
 #ifndef IIRFilter_h
 #define IIRFilter_h
 
+#include "nsTArray.h"
+
 typedef nsTArray<double> AudioDoubleArray;
 
 namespace blink {
 
 class IIRFilter final {
 public:
     // The maximum IIR filter order.  This also limits the number of feedforward coefficients.  The
     // maximum number of coefficients is 20 according to the spec.
