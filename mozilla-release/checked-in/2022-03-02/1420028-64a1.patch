# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1511531410 28800
#      Fri Nov 24 05:50:10 2017 -0800
# Node ID 4878f788fd69346c1c7135cd3968ee0a888bf375
# Parent  a094d24ccd43c7973e74be7185d2e1760efae6a7
Bug 1420028: Handle UTC date/time strings in Date.parse(). r=jorendorff

diff --git a/js/src/jsdate.cpp b/js/src/jsdate.cpp
--- a/js/src/jsdate.cpp
+++ b/js/src/jsdate.cpp
@@ -1036,20 +1036,23 @@ ParseDate(const CharT* s, size_t length,
     int year = -1;
     int mon = -1;
     int mday = -1;
     int hour = -1;
     int min = -1;
     int sec = -1;
     int tzOffset = -1;
 
+    // One of '+', '-', ':', '/', or 0 (the default value).
     int prevc = 0;
 
     bool seenPlusMinus = false;
     bool seenMonthName = false;
+    bool seenFullYear = false;
+    bool negativeYear = false;
 
     size_t i = 0;
     while (i < length) {
         int c = s[i];
         i++;
         if (c <= ' ' || c == ',' || c == '-') {
             if (c == '-' && '0' <= s[i] && s[i] <= '9')
                 prevc = c;
@@ -1065,43 +1068,57 @@ ParseDate(const CharT* s, size_t length,
                 } else if (c == ')') {
                     if (--depth <= 0)
                         break;
                 }
             }
             continue;
         }
         if ('0' <= c && c <= '9') {
+            size_t partStart = i - 1;
             int n = c - '0';
             while (i < length && '0' <= (c = s[i]) && c <= '9') {
                 n = n * 10 + c - '0';
                 i++;
             }
+            size_t partLength = i - partStart;
 
             /*
              * Allow TZA before the year, so 'Wed Nov 05 21:49:11 GMT-0800 1997'
              * works.
              *
              * Uses of seenPlusMinus allow ':' in TZA, so Java no-timezone style
              * of GMT+4:30 works.
              */
 
-            if ((prevc == '+' || prevc == '-')/*  && year>=0 */) {
+            if (prevc == '-' && (tzOffset != 0 || seenPlusMinus) && partLength >= 4 && year < 0) {
+                // Parse as a negative, possibly zero-padded year if
+                // 1. the preceding character is '-',
+                // 2. the TZA is not 'GMT' (tested by |tzOffset != 0|),
+                // 3. or a TZA was already parsed |seenPlusMinus == true|,
+                // 4. the part length is at least 4 (to parse '-08' as a TZA),
+                // 5. and we did not already parse a year |year < 0|.
+                year = n;
+                seenFullYear = true;
+                negativeYear = true;
+            } else if ((prevc == '+' || prevc == '-')/*  && year>=0 */) {
                 /* Make ':' case below change tzOffset. */
                 seenPlusMinus = true;
 
                 /* offset */
                 if (n < 24)
                     n = n * 60; /* EG. "GMT-3" */
                 else
                     n = n % 100 + n / 100 * 60; /* eg "GMT-0430" */
 
                 if (prevc == '+')       /* plus means east of GMT */
                     n = -n;
 
+                // Reject if not preceded by 'GMT' or if a time zone offset
+                // was already parsed.
                 if (tzOffset != 0 && tzOffset != -1)
                     return false;
 
                 tzOffset = n;
             } else if (prevc == '/' && mon >= 0 && mday >= 0 && year < 0) {
                 if (c <= ' ' || c == ',' || c == '/' || i >= length)
                     year = n;
                 else
@@ -1136,16 +1153,17 @@ ParseDate(const CharT* s, size_t length,
             } else if (prevc == ':' && min >= 0 && sec < 0) {
                 sec = /*byte*/ n;
             } else if (mon < 0) {
                 mon = /*byte*/n;
             } else if (mon >= 0 && mday < 0) {
                 mday = /*byte*/ n;
             } else if (mon >= 0 && mday >= 0 && year < 0) {
                 year = n;
+                seenFullYear = partLength >= 4;
             } else {
                 return false;
             }
             prevc = 0;
         } else if (c == '/' || c == ':' || c == '+' || c == '-') {
             prevc = c;
         } else {
             size_t st = i - 1;
@@ -1220,68 +1238,69 @@ ParseDate(const CharT* s, size_t length,
     /*
      * Case 1. The input string contains an English month name.
      *         The form of the string can be month f l, or f month l, or
      *         f l month which each evaluate to the same date.
      *         If f and l are both greater than or equal to 100 the date
      *         is invalid.
      *
      *         The year is taken to be either the greater of the values f, l or
-     *         whichever is set to zero. If the year is greater than or equal to
-     *         50 and less than 100, it is considered to be the number of years
-     *         after 1900. If the year is less than 50 it is considered to be the
-     *         number of years after 2000, otherwise it is considered to be the
-     *         number of years after 0.
+     *         whichever is set to zero.
      *
      * Case 2. The input string is of the form "f/m/l" where f, m and l are
      *         integers, e.g. 7/16/45. mon, mday and year values are adjusted
      *         to achieve Chrome compatibility.
      *
      *         a. If 0 < f <= 12 and 0 < l <= 31, f/m/l is interpreted as
      *         month/day/year.
-     *            i.  If year < 50, it is the number of years after 2000
-     *            ii. If year >= 50, it is the number of years after 1900.
-     *           iii. If year >= 100, it is the number of years after 0.
      *         b. If 31 < f and 0 < m <= 12 and 0 < l <= 31 f/m/l is
      *         interpreted as year/month/day
-     *            i.  If year < 50, it is the number of years after 2000
-     *            ii. If year >= 50, it is the number of years after 1900.
-     *           iii. If year >= 100, it is the number of years after 0.
      */
     if (seenMonthName) {
         if (mday >= 100 && mon >= 100)
             return false;
 
-        if (year > 0 && (mday == 0 || mday > year)) {
+        if (year > 0 && (mday == 0 || mday > year) && !seenFullYear) {
             int temp = year;
             year = mday;
             mday = temp;
         }
 
         if (mday <= 0 || mday > 31)
             return false;
 
     } else if (0 < mon && mon <= 12 && 0 < mday && mday <= 31) {
         /* (a) month/day/year */
     } else {
         /* (b) year/month/day */
-        if (mon > 31 && mday <= 12 && year <= 31) {
+        if (mon > 31 && mday <= 12 && year <= 31 && !seenFullYear) {
             int temp = year;
             year = mon;
             mon = mday;
             mday = temp;
         } else {
             return false;
         }
     }
 
-    if (year < 50)
-        year += 2000;
-    else if (year >= 50 && year < 100)
-        year += 1900;
+    // If the year is greater than or equal to 50 and less than 100, it is
+    // considered to be the number of years after 1900. If the year is less
+    // than 50 it is considered to be the number of years after 2000,
+    // otherwise it is considered to be the number of years after 0.
+    if (!seenFullYear) {
+        if (year < 50) {
+            year += 2000;
+        } else if (year >= 50 && year < 100) {
+            year += 1900;
+        }
+    }
+
+    if (negativeYear) {
+        year = -year;
+    }
 
     mon -= 1; /* convert month to 0-based */
     if (sec < 0)
         sec = 0;
     if (min < 0)
         min = 0;
     if (hour < 0)
         hour = 0;
diff --git a/js/src/tests/ecma_6/Date/parse-from-tostring-methods.js b/js/src/tests/ecma_6/Date/parse-from-tostring-methods.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/ecma_6/Date/parse-from-tostring-methods.js
@@ -0,0 +1,24 @@
+let dates = [
+    "0041-09-23", "+000041-09-23", "-000041-09-23",
+    "0091-09-23", "+000091-09-23", "-000091-09-23",
+    "0217-09-23", "+000217-09-23", "-000217-09-23",
+    "2017-09-23", "+002017-09-23", "-002017-09-23",
+                  "+022017-09-23", "-022017-09-23",
+                  "+202017-09-23", "-202017-09-23",
+];
+
+for (let date of dates) {
+    let d = new Date(date);
+    let timeValue = d.valueOf();
+
+    assertEq(Number.isNaN(timeValue), false, `Cannot parse "${date}" as ISO date-only form`);
+
+    // Ensure parsing the results of toString(), toUTCString(), and toISOString()
+    // gives the same time value as required by 20.3.3.2 Date.parse.
+    assertEq(Date.parse(d.toString()), timeValue, `Cannot parse from toString() of "${date}"`);
+    assertEq(Date.parse(d.toUTCString()), timeValue, `Cannot parse from toUTCString() of "${date}"`);
+    assertEq(Date.parse(d.toISOString()), timeValue, `Cannot parse from toISOString() of "${date}"`);
+}
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
