# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1554734734 0
#      Mo Apr 08 14:45:34 2019 +0000
# Node ID 5acb3927d1ad3c4f36dbeb5dc25b46109a1f315b
# Parent  9b1eb0d13fb6dd14ca687cc64629e008b5e1bbc4
Bug 1541580 - Cleanup ProxyObject creation r=jandem a=lizzard

Differential Revision: https://phabricator.services.mozilla.com/D26157

diff --git a/js/src/vm/ProxyObject.cpp b/js/src/vm/ProxyObject.cpp
--- a/js/src/vm/ProxyObject.cpp
+++ b/js/src/vm/ProxyObject.cpp
@@ -91,21 +91,29 @@ ProxyObject::New(JSContext* cx, const Ba
     values->init(proxy->numReservedSlots());
 
     proxy->data.handler = handler;
     if (IsCrossCompartmentWrapper(proxy))
         proxy->setCrossCompartmentPrivate(priv);
     else
         proxy->setSameCompartmentPrivate(priv);
 
-    /* Don't track types of properties of non-DOM and non-singleton proxies. */
-    if (newKind != SingletonObject && !clasp->isDOMClass())
-        MarkObjectGroupUnknownProperties(cx, proxy->group());
+  if (newKind == SingletonObject) {
+    Rooted<ProxyObject*> rootedProxy(cx, proxy);
+    if (!JSObject::setSingleton(cx, rootedProxy)) {
+      return nullptr;
+    }
+    return rootedProxy;
+  }
 
-    return proxy;
+  /* Don't track types of properties of non-DOM and non-singleton proxies. */
+  if (!clasp->isDOMClass())
+      MarkObjectGroupUnknownProperties(cx, proxy->group());
+
+  return proxy;
 }
 
 gc::AllocKind
 ProxyObject::allocKindForTenure() const
 {
     MOZ_ASSERT(usingInlineValueArray());
     Value priv = const_cast<ProxyObject*>(this)->private_();
     return GetProxyGCObjectKind(getClass(), data.handler, priv);
@@ -176,23 +184,16 @@ ProxyObject::create(JSContext* cx, const
     pobj->group_.init(group);
     pobj->initShape(shape);
 
     MOZ_ASSERT(clasp->shouldDelayMetadataBuilder());
     cx->compartment()->setObjectPendingMetadata(cx, pobj);
 
     js::gc::TraceCreateObject(pobj);
 
-    if (newKind == SingletonObject) {
-        Rooted<ProxyObject*> pobjRoot(cx, pobj);
-        if (!JSObject::setSingleton(cx, pobjRoot))
-            return cx->alreadyReportedOOM();
-        pobj = pobjRoot;
-    }
-
     return pobj;
 }
 
 JS_FRIEND_API(void)
 js::SetValueInProxy(Value* slot, const Value& value)
 {
     // Slots in proxies are not GCPtrValues, so do a cast whenever assigning
     // values to them which might trigger a barrier.
