# HG changeset patch
# User Masatoshi Kimura <VYV03354@nifty.ne.jp>
# Date 1505398762 -32400
#      Thu Sep 14 23:19:22 2017 +0900
# Node ID 790f099a55ab69ca981b46f80d3319f38a5fdf5e
# Parent  f4903067b543b20446ae86e669c0ca0b65f23f81
Bug 1400207 - Remove nsIDocument::AddCharSetObserver. r=hsivonen

MozReview-Commit-ID: 7uWDQvyz4yC

diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -3803,45 +3803,22 @@ nsDocument::GetBaseTarget(nsAString &aBa
 }
 
 void
 nsDocument::SetDocumentCharacterSet(NotNull<const Encoding*> aEncoding)
 {
   if (mCharacterSet != aEncoding) {
     mCharacterSet = aEncoding;
 
-    nsAutoCString charsetID;
-    aEncoding->Name(charsetID);
-    NS_ConvertASCIItoUTF16 charset16(charsetID);
-
-    int32_t n = mCharSetObservers.Length();
-
-    for (int32_t i = 0; i < n; i++) {
-      nsIObserver* observer = mCharSetObservers.ElementAt(i);
-
-      observer->Observe(static_cast<nsIDocument *>(this), "charset",
-                        charset16.get());
-    }
-  }
-}
-
-nsresult
-nsDocument::AddCharSetObserver(nsIObserver* aObserver)
-{
-  NS_ENSURE_ARG_POINTER(aObserver);
-
-  NS_ENSURE_TRUE(mCharSetObservers.AppendElement(aObserver), NS_ERROR_FAILURE);
-
-  return NS_OK;
-}
-
-void
-nsDocument::RemoveCharSetObserver(nsIObserver* aObserver)
-{
-  mCharSetObservers.RemoveElement(aObserver);
+    if (nsIPresShell* shell = GetShell()) {
+      if (nsPresContext* context = shell->GetPresContext()) {
+        context->DispatchCharSetChange(aEncoding);
+      }
+    }
+  }
 }
 
 void
 nsIDocument::GetSandboxFlagsAsString(nsAString& aFlags)
 {
   nsContentUtils::SandboxFlagsToString(mSandboxFlags, aFlags);
 }
 
diff --git a/dom/base/nsDocument.h b/dom/base/nsDocument.h
--- a/dom/base/nsDocument.h
+++ b/dom/base/nsDocument.h
@@ -425,26 +425,16 @@ public:
 
   /**
    * Set the document's character encoding. This will
    * trigger a startDocumentLoad if necessary to answer the question.
    */
   virtual void
     SetDocumentCharacterSet(NotNull<const Encoding*> aEncoding) override;
 
-  /**
-   * Add an observer that gets notified whenever the charset changes.
-   */
-  virtual nsresult AddCharSetObserver(nsIObserver* aObserver) override;
-
-  /**
-   * Remove a charset observer.
-   */
-  virtual void RemoveCharSetObserver(nsIObserver* aObserver) override;
-
   virtual Element* AddIDTargetObserver(nsIAtom* aID, IDTargetObserver aObserver,
                                        void* aData, bool aForImage) override;
   virtual void RemoveIDTargetObserver(nsIAtom* aID, IDTargetObserver aObserver,
                                       void* aData, bool aForImage) override;
 
   /**
    * Access HTTP header data (this may also get set from other sources, like
    * HTML META tags).
@@ -1145,18 +1135,16 @@ protected:
   // Retrieves the classification of the Flash plugins in the document based on
   // the classification lists.
   mozilla::dom::FlashClassification PrincipalFlashClassification();
 
   // Attempts to determine the Flash classification of this page based on the
   // the classification lists and the classification of parent documents.
   mozilla::dom::FlashClassification ComputeFlashClassification();
 
-  nsTArray<nsIObserver*> mCharSetObservers;
-
   PLDHashTable *mSubDocuments;
 
   // Array of owning references to all children
   nsAttrAndChildArray mChildren;
 
   // Pointer to our parser if we're currently in the process of being
   // parsed into.
   nsCOMPtr<nsIParser> mParser;
diff --git a/dom/base/nsIDocument.h b/dom/base/nsIDocument.h
--- a/dom/base/nsIDocument.h
+++ b/dom/base/nsIDocument.h
@@ -511,26 +511,16 @@ public:
   // This method MUST be called before SetDocumentCharacterSet if
   // you're planning to call both.
   void SetDocumentCharacterSetSource(int32_t aCharsetSource)
   {
     mCharacterSetSource = aCharsetSource;
   }
 
   /**
-   * Add an observer that gets notified whenever the charset changes.
-   */
-  virtual nsresult AddCharSetObserver(nsIObserver* aObserver) = 0;
-
-  /**
-   * Remove a charset observer.
-   */
-  virtual void RemoveCharSetObserver(nsIObserver* aObserver) = 0;
-
-  /**
    * This gets fired when the element that an id refers to changes.
    * This fires at difficult times. It is generally not safe to do anything
    * which could modify the DOM in any way. Use
    * nsContentUtils::AddScriptRunner.
    * @return true to keep the callback in the callback set, false
    * to remove it.
    */
   typedef bool (* IDTargetObserver)(Element* aOldElement,
diff --git a/layout/base/nsPresContext.cpp b/layout/base/nsPresContext.cpp
--- a/layout/base/nsPresContext.cpp
+++ b/layout/base/nsPresContext.cpp
@@ -392,17 +392,16 @@ nsPresContext::~nsPresContext()
   NS_PRECONDITION(!mShell, "Presshell forgot to clear our mShell pointer");
   DetachShell();
 
   Destroy();
 }
 
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(nsPresContext)
    NS_INTERFACE_MAP_ENTRY(nsISupports)
-   NS_INTERFACE_MAP_ENTRY(nsIObserver)
 NS_INTERFACE_MAP_END
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(nsPresContext)
 NS_IMPL_CYCLE_COLLECTING_RELEASE_WITH_LAST_RELEASE(nsPresContext, LastRelease())
 
 void
 nsPresContext::LastRelease()
 {
@@ -1010,31 +1009,23 @@ nsPresContext::AttachShell(nsIPresShell*
       docURI->SchemeIs("resource", &isRes);
 
       if (!isChrome && !isRes)
         mImageAnimationMode = mImageAnimationModePref;
       else
         mImageAnimationMode = imgIContainer::kNormalAnimMode;
     }
 
-    doc->AddCharSetObserver(this);
     UpdateCharSet(doc->GetDocumentCharacterSet());
   }
 }
 
 void
 nsPresContext::DetachShell()
 {
-  // Remove ourselves as the charset observer from the shell's doc, because
-  // this shell may be going away for good.
-  nsIDocument *doc = mShell ? mShell->GetDocument() : nullptr;
-  if (doc) {
-    doc->RemoveCharSetObserver(this);
-  }
-
   // The counter style manager's destructor needs to deallocate with the
   // presshell arena. Disconnect it before nulling out the shell.
   //
   // XXXbholley: Given recent refactorings, it probably makes more sense to
   // just null our mShell at the bottom of this function. I'm leaving it
   // this way to preserve the old ordering, but I doubt anything would break.
   if (mCounterStyleManager) {
     mCounterStyleManager->Disconnect();
@@ -1113,31 +1104,22 @@ nsPresContext::UpdateCharSet(NotNull<con
       break;
 
     case IBMBIDI_TEXTTYPE_CHARSET:
     default:
       SetVisualMode(IsVisualCharset(aCharSet));
   }
 }
 
-NS_IMETHODIMP
-nsPresContext::Observe(nsISupports* aSubject,
-                        const char* aTopic,
-                        const char16_t* aData)
+void
+nsPresContext::DispatchCharSetChange(NotNull<const Encoding*> aEncoding)
 {
-  if (!nsCRT::strcmp(aTopic, "charset")) {
-    auto encoding = Encoding::ForName(NS_LossyConvertUTF16toASCII(aData));
-    RefPtr<CharSetChangingRunnable> runnable =
-      new CharSetChangingRunnable(this, encoding);
-    return Document()->Dispatch(TaskCategory::Other,
-                                runnable.forget());
-  }
-
-  NS_WARNING("unrecognized topic in nsPresContext::Observe");
-  return NS_ERROR_FAILURE;
+  RefPtr<CharSetChangingRunnable> runnable =
+    new CharSetChangingRunnable(this, aEncoding);
+  Document()->Dispatch(TaskCategory::Other, runnable.forget());
 }
 
 nsPresContext*
 nsPresContext::GetParentPresContext()
 {
   nsIPresShell* shell = GetPresShell();
   if (shell) {
     nsViewManager* viewManager = shell->GetViewManager();
diff --git a/layout/base/nsPresContext.h b/layout/base/nsPresContext.h
--- a/layout/base/nsPresContext.h
+++ b/layout/base/nsPresContext.h
@@ -16,17 +16,16 @@
 #include "nsCoord.h"
 #include "nsCOMPtr.h"
 #include "nsIPresShell.h"
 #include "nsRect.h"
 #include "nsStringFwd.h"
 #include "nsFont.h"
 #include "gfxFontConstants.h"
 #include "nsIAtom.h"
-#include "nsIObserver.h"
 #include "nsITimer.h"
 #include "nsCRT.h"
 #include "nsIWidgetListener.h"
 #include "nsLanguageAtomService.h"
 #include "nsGkAtoms.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsChangeHint.h"
 #include <algorithm>
@@ -121,27 +120,26 @@ enum nsLayoutPhase {
 #define NS_AUTHOR_SPECIFIED_PADDING         (1 << 2)
 #define NS_AUTHOR_SPECIFIED_TEXT_SHADOW     (1 << 3)
 
 class nsRootPresContext;
 
 // An interface for presentation contexts. Presentation contexts are
 // objects that provide an outer context for a presentation shell.
 
-class nsPresContext : public nsIObserver,
+class nsPresContext : public nsISupports,
                       public mozilla::SupportsWeakPtr<nsPresContext> {
 public:
   using Encoding = mozilla::Encoding;
   template <typename T> using NotNull = mozilla::NotNull<T>;
   typedef mozilla::LangGroupFontPrefs LangGroupFontPrefs;
   typedef mozilla::ScrollbarStyles ScrollbarStyles;
   typedef mozilla::StaticPresData StaticPresData;
 
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
-  NS_DECL_NSIOBSERVER
   NS_DECL_CYCLE_COLLECTION_CLASS(nsPresContext)
   MOZ_DECLARE_WEAKREFERENCE_TYPENAME(nsPresContext)
 
   enum nsPresContextType {
     eContext_Galley,       // unpaginated screen presentation
     eContext_PrintPreview, // paginated screen presentation
     eContext_Print,        // paginated printer presentation
     eContext_PageLayout    // paginated & editable.
@@ -170,16 +168,18 @@ public:
   nsIPresShell* PresShell() const
   {
     NS_ASSERTION(mShell, "Null pres shell");
     return mShell;
   }
 
   nsIPresShell* GetPresShell() const { return mShell; }
 
+  void DispatchCharSetChange(NotNull<const Encoding*> aCharSet);
+
   /**
    * Returns the parent prescontext for this one. Returns null if this is a
    * root.
    */
   nsPresContext* GetParentPresContext();
 
   /**
    * Returns the prescontext of the toplevel content document that contains
