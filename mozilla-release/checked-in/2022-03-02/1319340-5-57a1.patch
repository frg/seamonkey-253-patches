# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1501834321 -32400
# Node ID 2762a672be69a1be86d6dec1a58ddea555e2339b
# Parent  119629a19131ebc2b2d79c05d6f143c409f61f47
Bug 1319340 - part5: Make nsComposerCommands use concrete class when calling methods of editor r=m_kato

Compiler may can optimize to call virtual methods at build time if we call them with concrete classes because some of them may have final keyword.

Even if not so, we can optimize some methods with creating non-virtual methods.

MozReview-Commit-ID: K3bRlc0URml

diff --git a/editor/composer/nsComposerCommands.cpp b/editor/composer/nsComposerCommands.cpp
--- a/editor/composer/nsComposerCommands.cpp
+++ b/editor/composer/nsComposerCommands.cpp
@@ -2,46 +2,49 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 
 #include <stdio.h>                      // for printf
 
 #include "mozilla/Assertions.h"         // for MOZ_ASSERT, etc
+#include "mozilla/EditorBase.h"         // for EditorBase
 #include "mozilla/HTMLEditor.h"         // for HTMLEditor
 #include "nsAString.h"
 #include "nsCOMPtr.h"                   // for nsCOMPtr, do_QueryInterface, etc
 #include "nsComponentManagerUtils.h"    // for do_CreateInstance
 #include "nsComposerCommands.h"
 #include "nsDebug.h"                    // for NS_ENSURE_TRUE, etc
 #include "nsError.h"                    // for NS_OK, NS_ERROR_FAILURE, etc
 #include "nsGkAtoms.h"                  // for nsGkAtoms, nsGkAtoms::font, etc
 #include "nsIAtom.h"                    // for nsIAtom, etc
 #include "nsIClipboard.h"               // for nsIClipboard, etc
 #include "nsICommandParams.h"           // for nsICommandParams, etc
 #include "nsID.h"
 #include "nsIDOMElement.h"              // for nsIDOMElement
 #include "nsIEditor.h"                  // for nsIEditor
-#include "nsIHTMLAbsPosEditor.h"        // for nsIHTMLAbsPosEditor
 #include "nsIHTMLEditor.h"              // for nsIHTMLEditor, etc
 #include "nsLiteralString.h"            // for NS_LITERAL_STRING
 #include "nsReadableUtils.h"            // for EmptyString
 #include "nsString.h"                   // for nsAutoString, nsString, etc
 #include "nsStringFwd.h"                // for nsString
 
 class nsISupports;
 
 //prototype
 nsresult GetListState(mozilla::HTMLEditor* aHTMLEditor,
                       bool* aMixed,
                       nsAString& aLocalName);
-nsresult RemoveOneProperty(nsIHTMLEditor* aEditor, const nsAString& aProp);
-nsresult RemoveTextProperty(nsIHTMLEditor* aEditor, const nsAString& aProp);
-nsresult SetTextProperty(nsIHTMLEditor *aEditor, const nsAString& aProp);
+nsresult RemoveOneProperty(mozilla::HTMLEditor* aHTMLEditor,
+                           const nsAString& aProp);
+nsresult RemoveTextProperty(mozilla::HTMLEditor* aHTMLEditor,
+                            const nsAString& aProp);
+nsresult SetTextProperty(mozilla::HTMLEditor* aHTMLEditor,
+                         const nsAString& aProp);
 
 
 //defines
 #define STATE_ENABLED  "state_enabled"
 #define STATE_ALL "state_all"
 #define STATE_ANY "state_any"
 #define STATE_MIXED "state_mixed"
 #define STATE_BEGIN "state_begin"
@@ -71,35 +74,37 @@ nsBaseStateUpdatingCommand::~nsBaseState
 NS_IMPL_ISUPPORTS_INHERITED0(nsBaseStateUpdatingCommand, nsBaseComposerCommand)
 
 NS_IMETHODIMP
 nsBaseStateUpdatingCommand::IsCommandEnabled(const char *aCommandName,
                                              nsISupports *refCon,
                                              bool *outCmdEnabled)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
-  if (editor)
-    return editor->GetIsSelectionEditable(outCmdEnabled);
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
+  }
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
 
 NS_IMETHODIMP
 nsBaseStateUpdatingCommand::DoCommand(const char *aCommandName,
                                       nsISupports *refCon)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
   if (NS_WARN_IF(!editor)) {
-    return NS_ERROR_INVALID_ARG;
+    return NS_ERROR_FAILURE;
   }
   mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
   if (NS_WARN_IF(!htmlEditor)) {
-    return NS_ERROR_INVALID_ARG;
+    return NS_ERROR_FAILURE;
   }
   return ToggleState(htmlEditor);
 }
 
 NS_IMETHODIMP
 nsBaseStateUpdatingCommand::DoCommandParams(const char *aCommandName,
                                             nsICommandParams *aParams,
                                             nsISupports *refCon)
@@ -113,49 +118,56 @@ nsBaseStateUpdatingCommand::GetCommandSt
                                                   nsISupports *refCon)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
   if (!editor) {
     return NS_OK;
   }
   mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
   if (NS_WARN_IF(!htmlEditor)) {
-    return NS_ERROR_INVALID_ARG;
+    return NS_ERROR_FAILURE;
   }
   return GetCurrentState(htmlEditor, aParams);
 }
 
 NS_IMETHODIMP
 nsPasteNoFormattingCommand::IsCommandEnabled(const char * aCommandName,
                                              nsISupports *refCon,
                                              bool *outCmdEnabled)
 {
   NS_ENSURE_ARG_POINTER(outCmdEnabled);
   *outCmdEnabled = false;
 
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+
   // This command is only implemented by nsIHTMLEditor, since
   //  pasting in a plaintext editor automatically only supplies
   //  "unformatted" text
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_NOT_IMPLEMENTED);
-
-  nsCOMPtr<nsIEditor> editor = do_QueryInterface(htmlEditor);
-  NS_ENSURE_TRUE(editor, NS_ERROR_INVALID_ARG);
-
-  return editor->CanPaste(nsIClipboard::kGlobalClipboard, outCmdEnabled);
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
+  return htmlEditor->CanPaste(nsIClipboard::kGlobalClipboard, outCmdEnabled);
 }
 
-
 NS_IMETHODIMP
 nsPasteNoFormattingCommand::DoCommand(const char *aCommandName,
                                       nsISupports *refCon)
 {
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_NOT_IMPLEMENTED);
-
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
   return htmlEditor->PasteNoFormatting(nsIClipboard::kGlobalClipboard);
 }
 
 NS_IMETHODIMP
 nsPasteNoFormattingCommand::DoCommandParams(const char *aCommandName,
                                             nsICommandParams *aParams,
                                             nsISupports *refCon)
 {
@@ -396,53 +408,59 @@ nsListItemCommand::ToggleState(mozilla::
 
 NS_IMETHODIMP
 nsRemoveListCommand::IsCommandEnabled(const char * aCommandName,
                                       nsISupports *refCon,
                                       bool *outCmdEnabled)
 {
   *outCmdEnabled = false;
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(editor, NS_OK);
+  if (!editor) {
+    return NS_OK;
+  }
+
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
 
   bool isEditable = false;
-  nsresult rv = editor->GetIsSelectionEditable(&isEditable);
+  nsresult rv = editorBase->GetIsSelectionEditable(&isEditable);
   NS_ENSURE_SUCCESS(rv, rv);
   if (!isEditable) {
     return NS_OK;
   }
 
   // It is enabled if we are in any list type
   mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
   if (NS_WARN_IF(!htmlEditor)) {
-    return NS_ERROR_INVALID_ARG;
+    return NS_ERROR_FAILURE;
   }
 
   bool bMixed;
   nsAutoString localName;
   rv = GetListState(htmlEditor, &bMixed, localName);
   NS_ENSURE_SUCCESS(rv, rv);
 
   *outCmdEnabled = bMixed || !localName.IsEmpty();
   return NS_OK;
 }
 
 
 NS_IMETHODIMP
 nsRemoveListCommand::DoCommand(const char *aCommandName, nsISupports *refCon)
 {
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(refCon);
-
-  nsresult rv = NS_OK;
-  if (editor) {
-    // This removes any list type
-    rv = editor->RemoveList(EmptyString());
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    return NS_OK;
   }
-
-  return rv;
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (!htmlEditor) {
+    return NS_OK;
+  }
+  // This removes any list type
+  return htmlEditor->RemoveList(EmptyString());
 }
 
 NS_IMETHODIMP
 nsRemoveListCommand::DoCommandParams(const char *aCommandName,
                                      nsICommandParams *aParams,
                                      nsISupports *refCon)
 {
   return DoCommand(aCommandName, refCon);
@@ -458,35 +476,37 @@ nsRemoveListCommand::GetCommandStatePara
   return aParams->SetBooleanValue(STATE_ENABLED,outCmdEnabled);
 }
 
 NS_IMETHODIMP
 nsIndentCommand::IsCommandEnabled(const char * aCommandName,
                                   nsISupports *refCon, bool *outCmdEnabled)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
-  if (editor)
-    return editor->GetIsSelectionEditable(outCmdEnabled);
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
+  }
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
-
 NS_IMETHODIMP
 nsIndentCommand::DoCommand(const char *aCommandName, nsISupports *refCon)
 {
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(refCon);
-
-  nsresult rv = NS_OK;
-  if (editor) {
-    rv = editor->Indent(NS_LITERAL_STRING("indent"));
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    return NS_OK;
   }
-
-  return rv;
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (!htmlEditor) {
+    return NS_OK;
+  }
+  return htmlEditor->Indent(NS_LITERAL_STRING("indent"));
 }
 
 NS_IMETHODIMP
 nsIndentCommand::DoCommandParams(const char *aCommandName,
                                  nsICommandParams *aParams,
                                  nsISupports *refCon)
 {
   return DoCommand(aCommandName, refCon);
@@ -505,36 +525,38 @@ nsIndentCommand::GetCommandStateParams(c
 
 //OUTDENT
 
 NS_IMETHODIMP
 nsOutdentCommand::IsCommandEnabled(const char * aCommandName,
                                    nsISupports *refCon,
                                    bool *outCmdEnabled)
 {
-  *outCmdEnabled = false;
-
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
-  if (editor) {
-    nsresult rv = editor->GetIsSelectionEditable(outCmdEnabled);
-    NS_ENSURE_SUCCESS(rv, rv);
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
   }
-
-  return NS_OK;
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
-
 NS_IMETHODIMP
 nsOutdentCommand::DoCommand(const char *aCommandName, nsISupports *refCon)
 {
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(refCon);
-  if (htmlEditor)
-    return htmlEditor->Indent(NS_LITERAL_STRING("outdent"));
-
-  return NS_OK;
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    return NS_OK;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (!htmlEditor) {
+    return NS_OK;
+  }
+  return htmlEditor->Indent(NS_LITERAL_STRING("outdent"));
 }
 
 NS_IMETHODIMP
 nsOutdentCommand::DoCommandParams(const char *aCommandName,
                                   nsICommandParams *aParams,
                                   nsISupports *refCon)
 {
   return DoCommand(aCommandName, refCon);
@@ -562,25 +584,26 @@ nsMultiStateCommand::~nsMultiStateComman
 NS_IMPL_ISUPPORTS_INHERITED0(nsMultiStateCommand, nsBaseComposerCommand)
 
 NS_IMETHODIMP
 nsMultiStateCommand::IsCommandEnabled(const char * aCommandName,
                                       nsISupports *refCon,
                                       bool *outCmdEnabled)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
+  }
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
   // should be disabled sometimes, like if the current selection is an image
-  if (editor)
-    return editor->GetIsSelectionEditable(outCmdEnabled);
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
-
 NS_IMETHODIMP
 nsMultiStateCommand::DoCommand(const char *aCommandName, nsISupports *refCon)
 {
 #ifdef DEBUG
   printf("who is calling nsMultiStateCommand::DoCommand \
           (no implementation)? %s\n", aCommandName);
 #endif
 
@@ -593,17 +616,17 @@ nsMultiStateCommand::DoCommandParams(con
                                      nsISupports *refCon)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
   if (!editor) {
     return NS_OK;
   }
   mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
   if (NS_WARN_IF(!htmlEditor)) {
-    return NS_ERROR_INVALID_ARG;
+    return NS_ERROR_FAILURE;
   }
 
   nsAutoString tString;
   if (aParams) {
     nsCString s;
     nsresult rv = aParams->GetCStringValue(STATE_ATTRIBUTE, getter_Copies(s));
     if (NS_SUCCEEDED(rv))
       CopyASCIItoUTF16(s, tString);
@@ -619,17 +642,17 @@ nsMultiStateCommand::GetCommandStatePara
                                            nsISupports *refCon)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
   if (!editor) {
     return NS_OK;
   }
   mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
   if (NS_WARN_IF(!htmlEditor)) {
-    return NS_ERROR_INVALID_ARG;
+    return NS_ERROR_FAILURE;
   }
   return GetCurrentState(htmlEditor, aParams);
 }
 
 nsParagraphStateCommand::nsParagraphStateCommand()
 : nsMultiStateCommand()
 {
 }
@@ -872,21 +895,23 @@ nsHighlightColorStateCommand::SetState(m
 }
 
 NS_IMETHODIMP
 nsHighlightColorStateCommand::IsCommandEnabled(const char * aCommandName,
                                                nsISupports *refCon,
                                                bool *outCmdEnabled)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
-  if (editor)
-    return editor->GetIsSelectionEditable(outCmdEnabled);
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
+  }
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
 
 nsBackgroundColorStateCommand::nsBackgroundColorStateCommand()
 : nsMultiStateCommand()
 {
 }
 
@@ -980,28 +1005,35 @@ nsAbsolutePositioningCommand::nsAbsolute
 {
 }
 
 NS_IMETHODIMP
 nsAbsolutePositioningCommand::IsCommandEnabled(const char * aCommandName,
                                                nsISupports *aCommandRefCon,
                                                bool *outCmdEnabled)
 {
+  *outCmdEnabled = false;
+
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(aCommandRefCon);
-  nsCOMPtr<nsIHTMLAbsPosEditor> htmlEditor = do_QueryInterface(aCommandRefCon);
-  if (htmlEditor) {
-    bool isEditable = false;
-    nsresult rv = editor->GetIsSelectionEditable(&isEditable);
-    NS_ENSURE_SUCCESS(rv, rv);
-    if (isEditable)
-      return htmlEditor->GetAbsolutePositioningEnabled(outCmdEnabled);
+  if (!editor) {
+    return NS_OK;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (!htmlEditor) {
+    return NS_OK;
   }
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  bool isEditable = false;
+  nsresult rv = htmlEditor->GetIsSelectionEditable(&isEditable);
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
+  }
+  if (!isEditable) {
+    return NS_OK;
+  }
+  return htmlEditor->GetAbsolutePositioningEnabled(outCmdEnabled);
 }
 
 nsresult
 nsAbsolutePositioningCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
                                               nsICommandParams* aParams)
 {
   if (NS_WARN_IF(!aHTMLEditor)) {
     return NS_ERROR_INVALID_ARG;
@@ -1045,43 +1077,57 @@ nsAbsolutePositioningCommand::ToggleStat
 }
 
 
 NS_IMETHODIMP
 nsDecreaseZIndexCommand::IsCommandEnabled(const char * aCommandName,
                                           nsISupports *refCon,
                                           bool *outCmdEnabled)
 {
-  nsCOMPtr<nsIHTMLAbsPosEditor> htmlEditor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
 
   htmlEditor->GetAbsolutePositioningEnabled(outCmdEnabled);
   if (!(*outCmdEnabled))
     return NS_OK;
 
   nsCOMPtr<nsIDOMElement> positionedElement;
   htmlEditor->GetPositionedElement(getter_AddRefs(positionedElement));
   *outCmdEnabled = false;
-  if (positionedElement) {
-    int32_t z;
-    nsresult rv = htmlEditor->GetElementZIndex(positionedElement, &z);
-    NS_ENSURE_SUCCESS(rv, rv);
-    *outCmdEnabled = (z > 0);
+  if (!positionedElement) {
+    return NS_OK;
   }
 
+  int32_t z;
+  nsresult rv = htmlEditor->GetElementZIndex(positionedElement, &z);
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
+  }
+  *outCmdEnabled = (z > 0);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsDecreaseZIndexCommand::DoCommand(const char *aCommandName,
                                    nsISupports *refCon)
 {
-  nsCOMPtr<nsIHTMLAbsPosEditor> htmlEditor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_NOT_IMPLEMENTED);
-
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
   return htmlEditor->RelativeChangeZIndex(-1);
 }
 
 NS_IMETHODIMP
 nsDecreaseZIndexCommand::DoCommandParams(const char *aCommandName,
                                          nsICommandParams *aParams,
                                          nsISupports *refCon)
 {
@@ -1102,36 +1148,47 @@ nsDecreaseZIndexCommand::GetCommandState
   return aParams->SetBooleanValue(STATE_ENABLED, enabled);
 }
 
 NS_IMETHODIMP
 nsIncreaseZIndexCommand::IsCommandEnabled(const char * aCommandName,
                                           nsISupports *refCon,
                                           bool *outCmdEnabled)
 {
-  nsCOMPtr<nsIHTMLAbsPosEditor> htmlEditor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
 
   htmlEditor->GetAbsolutePositioningEnabled(outCmdEnabled);
   if (!(*outCmdEnabled))
     return NS_OK;
 
   nsCOMPtr<nsIDOMElement> positionedElement;
   htmlEditor->GetPositionedElement(getter_AddRefs(positionedElement));
   *outCmdEnabled = (nullptr != positionedElement);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsIncreaseZIndexCommand::DoCommand(const char *aCommandName,
                                    nsISupports *refCon)
 {
-  nsCOMPtr<nsIHTMLAbsPosEditor> htmlEditor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_NOT_IMPLEMENTED);
-
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
   return htmlEditor->RelativeChangeZIndex(1);
 }
 
 NS_IMETHODIMP
 nsIncreaseZIndexCommand::DoCommandParams(const char *aCommandName,
                                          nsICommandParams *aParams,
                                          nsISupports *refCon)
 {
@@ -1147,45 +1204,45 @@ nsIncreaseZIndexCommand::GetCommandState
 
   bool enabled = false;
   nsresult rv = IsCommandEnabled(aCommandName, refCon, &enabled);
   NS_ENSURE_SUCCESS(rv, rv);
 
   return aParams->SetBooleanValue(STATE_ENABLED, enabled);
 }
 
-
 NS_IMETHODIMP
 nsRemoveStylesCommand::IsCommandEnabled(const char * aCommandName,
                                         nsISupports *refCon,
                                         bool *outCmdEnabled)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
+  }
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
   // test if we have any styles?
-  if (editor)
-    return editor->GetIsSelectionEditable(outCmdEnabled);
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
-
-
 NS_IMETHODIMP
 nsRemoveStylesCommand::DoCommand(const char *aCommandName,
                                  nsISupports *refCon)
 {
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(refCon);
-
-  nsresult rv = NS_OK;
-  if (editor) {
-    rv = editor->RemoveAllInlineProperties();
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    return NS_OK;
   }
-
-  return rv;
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (!htmlEditor) {
+    return NS_OK;
+  }
+  return htmlEditor->RemoveAllInlineProperties();
 }
 
 NS_IMETHODIMP
 nsRemoveStylesCommand::DoCommandParams(const char *aCommandName,
                                        nsICommandParams *aParams,
                                        nsISupports *refCon)
 {
   return DoCommand(aCommandName, refCon);
@@ -1202,37 +1259,40 @@ nsRemoveStylesCommand::GetCommandStatePa
 }
 
 NS_IMETHODIMP
 nsIncreaseFontSizeCommand::IsCommandEnabled(const char * aCommandName,
                                             nsISupports *refCon,
                                             bool *outCmdEnabled)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
+  }
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
   // test if we are at max size?
-  if (editor)
-    return editor->GetIsSelectionEditable(outCmdEnabled);
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
 
 NS_IMETHODIMP
 nsIncreaseFontSizeCommand::DoCommand(const char *aCommandName,
                                      nsISupports *refCon)
 {
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(refCon);
-
-  nsresult rv = NS_OK;
-  if (editor) {
-    rv = editor->IncreaseFontSize();
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    return NS_OK;
   }
-
-  return rv;
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (!htmlEditor) {
+    return NS_OK;
+  }
+  return htmlEditor->IncreaseFontSize();
 }
 
 NS_IMETHODIMP
 nsIncreaseFontSizeCommand::DoCommandParams(const char *aCommandName,
                                            nsICommandParams *aParams,
                                            nsISupports *refCon)
 {
   return DoCommand(aCommandName, refCon);
@@ -1249,37 +1309,39 @@ nsIncreaseFontSizeCommand::GetCommandSta
 }
 
 NS_IMETHODIMP
 nsDecreaseFontSizeCommand::IsCommandEnabled(const char * aCommandName,
                                             nsISupports *refCon,
                                             bool *outCmdEnabled)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
+  }
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
   // test if we are at min size?
-  if (editor)
-    return editor->GetIsSelectionEditable(outCmdEnabled);
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
-
 NS_IMETHODIMP
 nsDecreaseFontSizeCommand::DoCommand(const char *aCommandName,
                                      nsISupports *refCon)
 {
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(refCon);
-
-  nsresult rv = NS_OK;
-  if (editor) {
-    rv = editor->DecreaseFontSize();
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (!editor) {
+    return NS_OK;
   }
-
-  return rv;
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (!htmlEditor) {
+    return NS_OK;
+  }
+  return htmlEditor->DecreaseFontSize();
 }
 
 NS_IMETHODIMP
 nsDecreaseFontSizeCommand::DoCommandParams(const char *aCommandName,
                                            nsICommandParams *aParams,
                                            nsISupports *refCon)
 {
   return DoCommand(aCommandName, refCon);
@@ -1297,55 +1359,67 @@ nsDecreaseFontSizeCommand::GetCommandSta
 
 NS_IMETHODIMP
 nsInsertHTMLCommand::IsCommandEnabled(const char * aCommandName,
                                       nsISupports *refCon,
                                       bool *outCmdEnabled)
 {
   NS_ENSURE_ARG_POINTER(outCmdEnabled);
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
-  if (editor)
-    return editor->GetIsSelectionEditable(outCmdEnabled);
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
+  }
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
-
 NS_IMETHODIMP
 nsInsertHTMLCommand::DoCommand(const char *aCommandName, nsISupports *refCon)
 {
   // If nsInsertHTMLCommand is called with no parameters, it was probably called with
   // an empty string parameter ''. In this case, it should act the same as the delete command
   NS_ENSURE_ARG_POINTER(refCon);
 
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(editor, NS_ERROR_NOT_IMPLEMENTED);
-
-  nsString html = EmptyString();
-  return editor->InsertHTML(html);
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
+  nsAutoString html;
+  return htmlEditor->InsertHTML(html);
 }
 
 NS_IMETHODIMP
 nsInsertHTMLCommand::DoCommandParams(const char *aCommandName,
                                      nsICommandParams *aParams,
                                      nsISupports *refCon)
 {
   NS_ENSURE_ARG_POINTER(aParams);
   NS_ENSURE_ARG_POINTER(refCon);
 
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(editor, NS_ERROR_NOT_IMPLEMENTED);
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
 
   // Get HTML source string to insert from command params
   nsAutoString html;
   nsresult rv = aParams->GetStringValue(STATE_DATA, html);
   NS_ENSURE_SUCCESS(rv, rv);
 
-  return editor->InsertHTML(html);
+  return htmlEditor->InsertHTML(html);
 }
 
 NS_IMETHODIMP
 nsInsertHTMLCommand::GetCommandStateParams(const char *aCommandName,
                                            nsICommandParams *aParams,
                                            nsISupports *refCon)
 {
   NS_ENSURE_ARG_POINTER(aParams);
@@ -1371,57 +1445,70 @@ nsInsertTagCommand::~nsInsertTagCommand(
 
 NS_IMETHODIMP
 nsInsertTagCommand::IsCommandEnabled(const char * aCommandName,
                                      nsISupports *refCon,
                                      bool *outCmdEnabled)
 {
   NS_ENSURE_ARG_POINTER(outCmdEnabled);
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
-  if (editor)
-    return editor->GetIsSelectionEditable(outCmdEnabled);
-
-  *outCmdEnabled = false;
-  return NS_OK;
+  if (!editor) {
+    *outCmdEnabled = false;
+    return NS_OK;
+  }
+  mozilla::EditorBase* editorBase = editor->AsEditorBase();
+  MOZ_ASSERT(editorBase);
+  return editorBase->GetIsSelectionEditable(outCmdEnabled);
 }
 
-
 // corresponding STATE_ATTRIBUTE is: src (img) and href (a)
 NS_IMETHODIMP
 nsInsertTagCommand::DoCommand(const char *aCmdName, nsISupports *refCon)
 {
   NS_ENSURE_TRUE(mTagName == nsGkAtoms::hr, NS_ERROR_NOT_IMPLEMENTED);
 
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(editor, NS_ERROR_NOT_IMPLEMENTED);
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
 
   nsCOMPtr<nsIDOMElement> domElem;
-  nsresult rv = editor->CreateElementWithDefaults(
+  nsresult rv = htmlEditor->CreateElementWithDefaults(
     nsDependentAtomString(mTagName), getter_AddRefs(domElem));
   NS_ENSURE_SUCCESS(rv, rv);
 
-  return editor->InsertElementAtSelection(domElem, true);
+  return htmlEditor->InsertElementAtSelection(domElem, true);
 }
 
 NS_IMETHODIMP
 nsInsertTagCommand::DoCommandParams(const char *aCommandName,
                                     nsICommandParams *aParams,
                                     nsISupports *refCon)
 {
   NS_ENSURE_ARG_POINTER(refCon);
 
   // inserting an hr shouldn't have an parameters, just call DoCommand for that
   if (mTagName == nsGkAtoms::hr) {
     return DoCommand(aCommandName, refCon);
   }
 
   NS_ENSURE_ARG_POINTER(aParams);
 
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(editor, NS_ERROR_NOT_IMPLEMENTED);
+  nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
+  if (NS_WARN_IF(!editor)) {
+    return NS_ERROR_FAILURE;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_FAILURE;
+  }
 
   // do we have an href to use for creating link?
   nsCString s;
   nsresult rv = aParams->GetCStringValue(STATE_ATTRIBUTE, getter_Copies(s));
   NS_ENSURE_SUCCESS(rv, rv);
   nsAutoString attrib;
   CopyASCIItoUTF16(s, attrib);
 
@@ -1434,28 +1521,28 @@ nsInsertTagCommand::DoCommandParams(cons
     attributeType.AssignLiteral("href");
   } else if (mTagName == nsGkAtoms::img) {
     attributeType.AssignLiteral("src");
   } else {
     return NS_ERROR_NOT_IMPLEMENTED;
   }
 
   nsCOMPtr<nsIDOMElement> domElem;
-  rv = editor->CreateElementWithDefaults(nsDependentAtomString(mTagName),
-                                         getter_AddRefs(domElem));
+  rv = htmlEditor->CreateElementWithDefaults(nsDependentAtomString(mTagName),
+                                             getter_AddRefs(domElem));
   NS_ENSURE_SUCCESS(rv, rv);
 
   rv = domElem->SetAttribute(attributeType, attrib);
   NS_ENSURE_SUCCESS(rv, rv);
 
   // do actual insertion
-  if (mTagName == nsGkAtoms::a)
-    return editor->InsertLinkAroundSelection(domElem);
-
-  return editor->InsertElementAtSelection(domElem, true);
+  if (mTagName == nsGkAtoms::a) {
+    return htmlEditor->InsertLinkAroundSelection(domElem);
+  }
+  return htmlEditor->InsertElementAtSelection(domElem, true);
 }
 
 NS_IMETHODIMP
 nsInsertTagCommand::GetCommandStateParams(const char *aCommandName,
                                           nsICommandParams *aParams,
                                           nsISupports *refCon)
 {
   NS_ENSURE_ARG_POINTER(aParams);
@@ -1496,47 +1583,51 @@ GetListState(mozilla::HTMLEditor* aHTMLE
     aLocalName.AssignLiteral("ul");
   } else if (bDL) {
     aLocalName.AssignLiteral("dl");
   }
   return NS_OK;
 }
 
 nsresult
-RemoveOneProperty(nsIHTMLEditor* aEditor, const nsAString& aProp)
+RemoveOneProperty(mozilla::HTMLEditor* aHTMLEditor,
+                  const nsAString& aProp)
 {
-  MOZ_ASSERT(aEditor);
+  MOZ_ASSERT(aHTMLEditor);
 
   /// XXX Hack alert! Look in nsIEditProperty.h for this
   nsCOMPtr<nsIAtom> styleAtom = NS_Atomize(aProp);
   NS_ENSURE_TRUE(styleAtom, NS_ERROR_OUT_OF_MEMORY);
 
-  return aEditor->RemoveInlineProperty(styleAtom, EmptyString());
+  return aHTMLEditor->RemoveInlineProperty(styleAtom, EmptyString());
 }
 
 
 // the name of the attribute here should be the contents of the appropriate
 // tag, e.g. 'b' for bold, 'i' for italics.
 nsresult
-RemoveTextProperty(nsIHTMLEditor* aEditor, const nsAString& aProp)
+RemoveTextProperty(mozilla::HTMLEditor* aHTMLEditor,
+                   const nsAString& aProp)
 {
-  MOZ_ASSERT(aEditor);
+  MOZ_ASSERT(aHTMLEditor);
 
   if (aProp.LowerCaseEqualsLiteral("all")) {
-    return aEditor->RemoveAllInlineProperties();
+    return aHTMLEditor->RemoveAllInlineProperties();
   }
 
-  return RemoveOneProperty(aEditor, aProp);
+  return RemoveOneProperty(aHTMLEditor, aProp);
 }
 
 // the name of the attribute here should be the contents of the appropriate
 // tag, e.g. 'b' for bold, 'i' for italics.
 nsresult
-SetTextProperty(nsIHTMLEditor* aEditor, const nsAString& aProp)
+SetTextProperty(mozilla::HTMLEditor* aHTMLEditor,
+                const nsAString& aProp)
 {
-  MOZ_ASSERT(aEditor);
+  MOZ_ASSERT(aHTMLEditor);
 
   /// XXX Hack alert! Look in nsIEditProperty.h for this
   nsCOMPtr<nsIAtom> styleAtom = NS_Atomize(aProp);
   NS_ENSURE_TRUE(styleAtom, NS_ERROR_OUT_OF_MEMORY);
 
-  return aEditor->SetInlineProperty(styleAtom, EmptyString(), EmptyString());
+  return aHTMLEditor->SetInlineProperty(styleAtom,
+                                        EmptyString(), EmptyString());
 }
