# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1510326336 18000
# Node ID 9b5adaaf61560f94ed59ed689bcac7bb4d533967
# Parent  0f14cefede32e7c7a50a2b6afb93900f1006f1de
Bug 1341234 - part 1 - add AC_SUBST_TOML_LIST macro; r=gps

Stylo's bindgen is configured partially through a .toml.in file that
substitutes the value of a configure variable (BINDGEN_CFLAGS) into a
TOML list.  We can debate whether this is a good thing to do some other
time; the reality is that the current moz.configure code that provides
the set_config for BINDGEN_CFLAGS needs to perform all the quoting
itself.

We want, however, to define the substituted variable in old-configure.in
land (some of the values that will go into BINDGEN_CFLAGS are only
defined in old-configure.in, and are not trivially ported to
moz.configure), which means that we need to have quoting logic in
m4/Python when we generate config.status.  This patch adds an
appropriate macro for doing so.

diff --git a/build/autoconf/config.status.m4 b/build/autoconf/config.status.m4
--- a/build/autoconf/config.status.m4
+++ b/build/autoconf/config.status.m4
@@ -7,48 +7,67 @@ define([MOZ_DIVERSION_SUBST], 11)
 
 dnl Replace AC_SUBST to store values in a format suitable for python.
 dnl The necessary comma after the tuple can't be put here because it
 dnl can mess around with things like:
 dnl    AC_SOMETHING(foo,AC_SUBST(),bar)
 define([AC_SUBST],
 [ifdef([AC_SUBST_SET_$1], [m4_fatal([Cannot use AC_SUBST and AC_SUBST_SET on the same variable ($1)])],
 [ifdef([AC_SUBST_LIST_$1], [m4_fatal([Cannot use AC_SUBST and AC_SUBST_LIST on the same variable ($1)])],
+[ifdef([AC_SUBST_TOML_LIST_$1], [m4_fatal([Cannot use AC_SUBST and AC_SUBST_TOML_LIST on the same variable ($1)])],
 [ifdef([AC_SUBST_$1], ,
 [define([AC_SUBST_$1], )dnl
 AC_DIVERT_PUSH(MOZ_DIVERSION_SUBST)dnl
     (''' $1 ''', r''' [$]$1 ''')
 AC_DIVERT_POP()dnl
-])])])])
+])])])])])
 
 dnl Like AC_SUBST, but makes the value available as a set in python,
 dnl with values got from the value of the environment variable, split on
 dnl whitespaces.
 define([AC_SUBST_SET],
 [ifdef([AC_SUBST_$1], [m4_fatal([Cannot use AC_SUBST and AC_SUBST_SET on the same variable ($1)])],
 [ifdef([AC_SUBST_LIST_$1], [m4_fatal([Cannot use AC_SUBST_LIST and AC_SUBST_SET on the same variable ($1)])],
+[ifdef([AC_SUBST_TOML_LIST_$1], [m4_fatal([Cannot use AC_SUBST_TOML_LIST and AC_SUBST_SET on the same variable ($1)])],
 [ifdef([AC_SUBST_SET_$1], ,
 [define([AC_SUBST_SET_$1], )dnl
 AC_DIVERT_PUSH(MOZ_DIVERSION_SUBST)dnl
     (''' $1 ''', unique_list(split(r''' [$]$1 ''')))
 AC_DIVERT_POP()dnl
-])])])])
+])])])])])
 
 dnl Like AC_SUBST, but makes the value available as a list in python,
 dnl with values got from the value of the environment variable, split on
 dnl whitespaces.
 define([AC_SUBST_LIST],
 [ifdef([AC_SUBST_$1], [m4_fatal([Cannot use AC_SUBST and AC_SUBST_LIST on the same variable ($1)])],
 [ifdef([AC_SUBST_SET_$1], [m4_fatal([Cannot use AC_SUBST_SET and AC_SUBST_LIST on the same variable ($1)])],
+[ifdef([AC_SUBST_TOML_LIST_$1], [m4_fatal([Cannot use AC_SUBST_TOML_LIST and AC_SUBST_LIST on the same variable ($1)])],
 [ifdef([AC_SUBST_LIST_$1], ,
 [define([AC_SUBST_LIST_$1], )dnl
 AC_DIVERT_PUSH(MOZ_DIVERSION_SUBST)dnl
     (''' $1 ''', list(split(r''' [$]$1 ''')))
 AC_DIVERT_POP()dnl
-])])])])
+])])])])])
+
+dnl Like AC_SUBST, but makes the value available as a string of comma-separated
+dnl quoted strings in python, with values got from the value of the environment
+dnl variable, split on whitespaces. The value is suitable for embedding into a
+dnl .toml list.
+define([AC_SUBST_TOML_LIST],
+[ifdef([AC_SUBST_$1], [m4_fatal([Cannot use AC_SUBST and AC_SUBST_TOML_LIST on the same variable ($1)])],
+[ifdef([AC_SUBST_SET_$1], [m4_fatal([Cannot use AC_SUBST_SET and AC_SUBST_TOML_LIST on the same variable ($1)])],
+[ifdef([AC_SUBST_LIST_$1], [m4_fatal([Cannot use AC_SUBST_LIST and AC_SUBST_TOML_LIST on the same variable ($1)])],
+[ifdef([AC_SUBST_TOML_LIST_$1], ,
+[define([AC_SUBST_TOML_LIST_$1], )dnl
+AC_DIVERT_PUSH(MOZ_DIVERSION_SUBST)dnl
+    (''' $1 ''', r''' %s ''' % str(', '.join("'%s'" % s for s in split(r''' [$]$1 '''))))
+AC_DIVERT_POP()dnl
+])])])])])
+
 
 dnl Ignore AC_SUBSTs for variables we don't have use for but that autoconf
 dnl itself exports.
 define([AC_SUBST_CFLAGS], )
 define([AC_SUBST_CPPFLAGS], )
 define([AC_SUBST_CXXFLAGS], )
 define([AC_SUBST_FFLAGS], )
 define([AC_SUBST_DEFS], )
