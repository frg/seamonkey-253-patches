# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1507774925 -32400
# Node ID 2bd833fbe449fa8b7f6bd3870a4702dba964d455
# Parent  44117f4a48afde4f2ca5da30cc0a2d34ba92c034
Bug 1407838 - Replace UniquePtr<T, NSFreePolicy> with UniqueFreePtr. r=njn

diff --git a/devtools/shared/heapsnapshot/HeapSnapshot.cpp b/devtools/shared/heapsnapshot/HeapSnapshot.cpp
--- a/devtools/shared/heapsnapshot/HeapSnapshot.cpp
+++ b/devtools/shared/heapsnapshot/HeapSnapshot.cpp
@@ -142,17 +142,17 @@ struct GetOrInternStringMatcher
 
   explicit GetOrInternStringMatcher(InternedStringSet& strings) : internedStrings(strings) { }
 
   const CharT* match(const std::string* str) {
     MOZ_ASSERT(str);
     size_t length = str->length() / sizeof(CharT);
     auto tempString = reinterpret_cast<const CharT*>(str->data());
 
-    UniquePtr<CharT[], NSFreePolicy> owned(NS_strndup(tempString, length));
+    UniqueFreePtr<CharT[]> owned(NS_strndup(tempString, length));
     if (!owned || !internedStrings.append(Move(owned)))
       return nullptr;
 
     return internedStrings.back().get();
   }
 
   const CharT* match(uint64_t ref) {
     if (MOZ_LIKELY(ref < internedStrings.length())) {
diff --git a/devtools/shared/heapsnapshot/HeapSnapshot.h b/devtools/shared/heapsnapshot/HeapSnapshot.h
--- a/devtools/shared/heapsnapshot/HeapSnapshot.h
+++ b/devtools/shared/heapsnapshot/HeapSnapshot.h
@@ -11,39 +11,33 @@
 #include "mozilla/devtools/DeserializedNode.h"
 #include "mozilla/dom/BindingDeclarations.h"
 #include "mozilla/dom/Nullable.h"
 #include "mozilla/HashFunctions.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/RefCounted.h"
 #include "mozilla/RefPtr.h"
 #include "mozilla/TimeStamp.h"
-#include "mozilla/UniquePtr.h"
+#include "mozilla/UniquePtrExtensions.h"
 
 #include "CoreDump.pb.h"
 #include "nsCOMPtr.h"
 #include "nsCRTGlue.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsISupports.h"
 #include "nsWrapperCache.h"
 #include "nsXPCOM.h"
 
 namespace mozilla {
 namespace devtools {
 
 class DominatorTree;
 
-struct NSFreePolicy {
-  void operator()(void* ptr) {
-    free(ptr);
-  }
-};
-
-using UniqueTwoByteString = UniquePtr<char16_t[], NSFreePolicy>;
-using UniqueOneByteString = UniquePtr<char[], NSFreePolicy>;
+using UniqueTwoByteString = UniqueFreePtr<char16_t[]>;
+using UniqueOneByteString = UniqueFreePtr<char[]>;
 
 class HeapSnapshot final : public nsISupports
                          , public nsWrapperCache
 {
   friend struct DeserializedNode;
   friend struct DeserializedEdge;
   friend struct DeserializedStackFrame;
   friend class JS::ubi::Concrete<JS::ubi::DeserializedNode>;
diff --git a/xpcom/build/XREAppData.h b/xpcom/build/XREAppData.h
--- a/xpcom/build/XREAppData.h
+++ b/xpcom/build/XREAppData.h
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsXREAppData_h
 #define nsXREAppData_h
 
 #include <stdint.h>
 #include "mozilla/Attributes.h"
+#include "mozilla/UniquePtrExtensions.h"
 #include "nsCOMPtr.h"
 #include "nsCRTGlue.h"
 #include "nsIFile.h"
 
 #if defined(XP_WIN) && defined(MOZ_SANDBOX)
 namespace sandbox {
 class BrokerServices;
 }
@@ -45,23 +46,16 @@ public:
   {
     *this = aOther;
   }
 
   XREAppData& operator=(const StaticXREAppData& aOther);
   XREAppData& operator=(const XREAppData& aOther);
   XREAppData& operator=(XREAppData&& aOther) = default;
 
-  struct NSFreePolicy
-  {
-    void operator()(const void* ptr) {
-      free(const_cast<void*>(ptr));
-    }
-  };
-
   // Lots of code reads these fields directly like a struct, so rather
   // than using UniquePtr directly, use an auto-converting wrapper.
   class CharPtr
   {
   public:
     explicit CharPtr() = default;
     explicit CharPtr(const char* v)
     {
@@ -85,17 +79,17 @@ public:
       return *this;
     }
 
     operator const char*() const {
       return mValue.get();
     }
 
   private:
-    UniquePtr<const char, NSFreePolicy> mValue;
+    UniqueFreePtr<const char> mValue;
   };
 
   /**
    * The directory of the application to be run. May be null if the
    * xulrunner and the app are installed into the same directory.
    */
   nsCOMPtr<nsIFile> directory;
 
