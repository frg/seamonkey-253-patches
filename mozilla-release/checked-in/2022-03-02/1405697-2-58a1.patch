# HG changeset patch
# User Chris Pearce <cpearce@mozilla.com>
# Date 1507127711 -7200
# Node ID 62643787da2badd76e74dfebb2a4ce2a1ec8160a
# Parent  d9a4c5e275f27f017d0d985093d0958044826649
Bug 1405697 - Move ChannelSuspendAgent to ChannelMediaResource.h. r=jwwang

It's only used by things in there anyway.

MozReview-Commit-ID: 4Pk12ab6e7U

diff --git a/dom/media/ChannelMediaResource.h b/dom/media/ChannelMediaResource.h
--- a/dom/media/ChannelMediaResource.h
+++ b/dom/media/ChannelMediaResource.h
@@ -7,16 +7,58 @@
 #define mozilla_dom_media_ChannelMediaResource_h
 
 #include "MediaResource.h"
 #include "MediaChannelStatistics.h"
 
 namespace mozilla {
 
 /**
+ * This class is responsible for managing the suspend count and report suspend
+ * status of channel.
+ **/
+class ChannelSuspendAgent
+{
+public:
+  explicit ChannelSuspendAgent(nsIChannel* aChannel)
+    : mChannel(aChannel)
+    , mIsChannelSuspended(false)
+  {
+  }
+
+  // True when the channel has been suspended or needs to be suspended.
+  bool IsSuspended();
+
+  // Return true when the channel is logically suspended, i.e. the suspend
+  // count goes from 0 to 1.
+  bool Suspend();
+
+  // Return true only when the suspend count is equal to zero.
+  bool Resume();
+
+  // Call after opening channel, set channel and check whether the channel
+  // needs to be suspended.
+  void NotifyChannelOpened(nsIChannel* aChannel);
+
+  // Call before closing channel, reset the channel internal status if needed.
+  void NotifyChannelClosing();
+
+  // Check whether we need to suspend the channel.
+  void UpdateSuspendedStatusIfNeeded();
+
+private:
+  // Only suspends channel but not changes the suspend count.
+  void SuspendInternal();
+
+  nsIChannel* mChannel;
+  uint32_t mSuspendCount = 0;
+  bool mIsChannelSuspended;
+};
+
+/**
  * This is the MediaResource implementation that wraps Necko channels.
  * Much of its functionality is actually delegated to MediaCache via
  * an underlying MediaCacheStream.
  *
  * All synchronization is performed by MediaCacheStream; all off-main-
  * thread operations are delegated directly to that object.
  */
 class ChannelMediaResource : public BaseMediaResource
diff --git a/dom/media/MediaResource.h b/dom/media/MediaResource.h
--- a/dom/media/MediaResource.h
+++ b/dom/media/MediaResource.h
@@ -280,56 +280,16 @@ protected:
 
   // True if SetLoadInBackground() has been called with
   // aLoadInBackground = true, i.e. when the document load event is not
   // blocked by this resource, and all channel loads will be in the
   // background.
   bool mLoadInBackground;
 };
 
-
-/**
- * This class is responsible for managing the suspend count and report suspend
- * status of channel.
- **/
-class ChannelSuspendAgent {
-public:
-  explicit ChannelSuspendAgent(nsIChannel* aChannel)
-  : mChannel(aChannel),
-    mIsChannelSuspended(false)
-  {}
-
-  // True when the channel has been suspended or needs to be suspended.
-  bool IsSuspended();
-
-  // Return true when the channel is logically suspended, i.e. the suspend
-  // count goes from 0 to 1.
-  bool Suspend();
-
-  // Return true only when the suspend count is equal to zero.
-  bool Resume();
-
-  // Call after opening channel, set channel and check whether the channel
-  // needs to be suspended.
-  void NotifyChannelOpened(nsIChannel* aChannel);
-
-  // Call before closing channel, reset the channel internal status if needed.
-  void NotifyChannelClosing();
-
-  // Check whether we need to suspend the channel.
-  void UpdateSuspendedStatusIfNeeded();
-private:
-  // Only suspends channel but not changes the suspend count.
-  void SuspendInternal();
-
-  nsIChannel* mChannel;
-  uint32_t mSuspendCount = 0;
-  bool mIsChannelSuspended;
-};
-
 /**
  * RAII class that handles pinning and unpinning for MediaResource and derived.
  * This should be used when making calculations that involve potentially-cached
  * MediaResource data, so that the state of the world can't change out from under
  * us.
  */
 template<class T>
 class MOZ_RAII AutoPinned {
