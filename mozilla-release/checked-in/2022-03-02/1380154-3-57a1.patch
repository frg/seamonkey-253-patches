# HG changeset patch
# User Eric Rahm <erahm@mozilla.com>
# Date 1502220070 25200
# Node ID 92318a417ec18e031710826e0d532555ae8a7e6c
# Parent  1584170d01665d897941b407bbdfcbf0d601fbd6
Bug 1380154 - Part 3: Cache most recently used eTLD entries. r=njn, r=jduell

This adds a most recently used (MRU) cache for the most common base domain
requests (aAddtionalParts == 1). With a table size of 31 I saw 8777 hits and
22 misses when loading twitter, youtube, and techcrunch. In stress testing
this provided a 75% reduction in run time.


MozReview-Commit-ID: 3JgCwIZagMs

diff --git a/netwerk/dns/nsEffectiveTLDService.cpp b/netwerk/dns/nsEffectiveTLDService.cpp
--- a/netwerk/dns/nsEffectiveTLDService.cpp
+++ b/netwerk/dns/nsEffectiveTLDService.cpp
@@ -4,18 +4,20 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 // This service reads a file of rules describing TLD-like domain names.  For a
 // complete description of the expected file format and parsing rules, see
 // http://wiki.mozilla.org/Gecko:Effective_TLD_Service
 
 #include "mozilla/ArrayUtils.h"
+#include "mozilla/HashFunctions.h"
 #include "mozilla/MemoryReporting.h"
 
+#include "MainThreadUtils.h"
 #include "nsEffectiveTLDService.h"
 #include "nsIIDNService.h"
 #include "nsNetUtil.h"
 #include "prnetdb.h"
 #include "nsIURI.h"
 #include "nsNetCID.h"
 #include "nsServiceManagerUtils.h"
 
@@ -205,16 +207,30 @@ nsEffectiveTLDService::GetBaseDomainInte
     return NS_ERROR_INVALID_ARG;
 
   // Check if we're dealing with an IPv4/IPv6 hostname, and return
   PRNetAddr addr;
   PRStatus result = PR_StringToNetAddr(aHostname.get(), &addr);
   if (result == PR_SUCCESS)
     return NS_ERROR_HOST_IS_IP_ADDRESS;
 
+  // Lookup in the cache if this is a normal query.
+  TLDCacheEntry* entry = nullptr;
+  if (aAdditionalParts == 1) {
+    if (LookupForAdd(aHostname, &entry)) {
+      // There was a match, just return the cached value.
+      aBaseDomain = entry->mBaseDomain;
+      if (trailingDot) {
+        aBaseDomain.Append('.');
+      }
+
+      return NS_OK;
+    }
+  }
+
   // Walk up the domain tree, most specific to least specific,
   // looking for matches at each level.  Note that a given level may
   // have multiple attributes (e.g. IsWild() and IsNormal()).
   const char *prevDomain = nullptr;
   const char *currDomain = aHostname.get();
   const char *nextDot = strchr(currDomain, '.');
   const char *end = currDomain + aHostname.Length();
   // Default value of *eTLD is currDomain as set in the while loop below
@@ -285,16 +301,23 @@ nsEffectiveTLDService::GetBaseDomainInte
       }
     }
   }
 
   if (aAdditionalParts != 0)
     return NS_ERROR_INSUFFICIENT_DOMAIN_LEVELS;
 
   aBaseDomain = Substring(iter, end);
+
+  // Update the MRU table if in use.
+  if (entry) {
+    entry->mHost = aHostname;
+    entry->mBaseDomain = aBaseDomain;
+  }
+
   // add on the trailing dot, if applicable
   if (trailingDot)
     aBaseDomain.Append('.');
 
   return NS_OK;
 }
 
 // Normalizes the given hostname, component by component.  ASCII/ACE
@@ -307,8 +330,18 @@ nsEffectiveTLDService::NormalizeHostname
     nsresult rv = mIDNService->ConvertUTF8toACE(aHostname, aHostname);
     if (NS_FAILED(rv))
       return rv;
   }
 
   ToLowerCase(aHostname);
   return NS_OK;
 }
+
+bool
+nsEffectiveTLDService::LookupForAdd(const nsACString& aHost, TLDCacheEntry** aEntry)
+{
+  MOZ_ASSERT(NS_IsMainThread());
+
+  const uint32_t hash = HashString(aHost.BeginReading(), aHost.Length());
+  *aEntry = &mMruTable[hash % kTableSize];
+  return (*aEntry)->mHost == aHost;
+}
diff --git a/netwerk/dns/nsEffectiveTLDService.h b/netwerk/dns/nsEffectiveTLDService.h
--- a/netwerk/dns/nsEffectiveTLDService.h
+++ b/netwerk/dns/nsEffectiveTLDService.h
@@ -32,12 +32,45 @@ public:
   size_t SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf);
 
 private:
   nsresult GetBaseDomainInternal(nsCString &aHostname, int32_t aAdditionalParts, nsACString &aBaseDomain);
   nsresult NormalizeHostname(nsCString &aHostname);
   ~nsEffectiveTLDService();
 
   nsCOMPtr<nsIIDNService>     mIDNService;
+
+  // The DAFSA provides a compact encoding of the rather large eTLD list.
   mozilla::Dafsa mGraph;
+
+  struct TLDCacheEntry
+  {
+    nsCString mHost;
+    nsCString mBaseDomain;
+  };
+
+  // We use a small most recently used cache to compensate for DAFSA lookups
+  // being slightly slower than a binary search on a larger table of strings.
+  //
+  // We first check the cache for a matching result and avoid a DAFSA lookup
+  // if a match is found. Otherwise we lookup the domain in the DAFSA and then
+  // cache the result. During standard browsing the same domains are repeatedly
+  // fed into |GetBaseDomainInternal| so this ends up being an effective
+  // mitigation getting about a 99% hit rate with four tabs open.
+  //
+  // A size of 31 is used rather than a more logical power-of-two such as 32
+  // since it is a prime number and provides fewer collisions when when used
+  // with our hash algorithms.
+  static const uint32_t kTableSize = 31;
+  TLDCacheEntry mMruTable[kTableSize];
+
+  /**
+   * Performs a lookup on the MRU table and provides a pointer to the hash
+   * entry that matched or should be used for adding this host.
+   *
+   * @param aHost The host to lookup.
+   * @param aEntry Out param, the entry in the MRU table to use.
+   * @return True if a match was found, false if there was a miss.
+   */
+  inline bool LookupForAdd(const nsACString& aHost, TLDCacheEntry** aEntry);
 };
 
 #endif // EffectiveTLDService_h
