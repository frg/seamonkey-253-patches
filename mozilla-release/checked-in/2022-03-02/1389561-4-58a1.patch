# HG changeset patch
# User Jan Varga <jan.varga@gmail.com>
# Date 1509605658 -28800
#      Thu Nov 02 14:54:18 2017 +0800
# Node ID 9ba5ec4843e8da516c3a4f4bbf60cc11d8e80a37
# Parent  8e60c6673e077f60c9ffb293b95b3a34537b966a
Bug 1389561 - Part 4: Split AsmJSCache's Client implementation into declaration and definition; r=luke

diff --git a/dom/asmjscache/AsmJSCache.cpp b/dom/asmjscache/AsmJSCache.cpp
--- a/dom/asmjscache/AsmJSCache.cpp
+++ b/dom/asmjscache/AsmJSCache.cpp
@@ -236,16 +236,70 @@ EvictEntries(nsIFile* aDirectory, const 
 
       usage.mFreed += fileSize;
     }
 
     entry.clear();
   }
 }
 
+/*******************************************************************************
+ * Client
+ ******************************************************************************/
+
+class Client
+  : public quota::Client
+{
+public:
+  NS_INLINE_DECL_REFCOUNTING(Client, override)
+
+  Type
+  GetType() override;
+
+  nsresult
+  InitOrigin(PersistenceType aPersistenceType,
+             const nsACString& aGroup,
+             const nsACString& aOrigin,
+             const AtomicBool& aCanceled,
+             UsageInfo* aUsageInfo) override;
+
+  nsresult
+  GetUsageForOrigin(PersistenceType aPersistenceType,
+                    const nsACString& aGroup,
+                    const nsACString& aOrigin,
+                    const AtomicBool& aCanceled,
+                    UsageInfo* aUsageInfo) override;
+
+  void
+  OnOriginClearCompleted(PersistenceType aPersistenceType,
+                         const nsACString& aOrigin)
+                         override;
+
+  void
+  ReleaseIOThreadObjects() override;
+
+  void
+  AbortOperations(const nsACString& aOrigin) override;
+
+  void
+  AbortOperationsForProcess(ContentParentId aContentParentId) override;
+
+  void
+  StartIdleMaintenance() override;
+
+  void
+  StopIdleMaintenance() override;
+
+  void
+  ShutdownWorkThreads() override;
+
+private:
+  ~Client() override = default;
+};
+
 // FileDescriptorHolder owns a file descriptor and its memory mapping.
 // FileDescriptorHolder is derived by two runnable classes (that is,
 // (Parent|Child)Runnable.
 class FileDescriptorHolder : public Runnable
 {
 public:
   FileDescriptorHolder()
     : Runnable("dom::asmjscache::FileDescriptorHolder")
@@ -1619,146 +1673,151 @@ CloseEntryForWrite(size_t aSize,
   // Flush to disk before writing the cookie (see OpenEntryForRead).
   if (PR_SyncMemMap(childRunnable->FileDesc(),
                     childRunnable->MappedMemory(),
                     childRunnable->FileSize()) == PR_SUCCESS) {
     *(AsmJSCookieType*)childRunnable->MappedMemory() = sAsmJSCookie;
   }
 }
 
-class Client : public quota::Client
-{
-  ~Client() override = default;
+/*******************************************************************************
+ * Client
+ ******************************************************************************/
 
-public:
-  NS_IMETHOD_(MozExternalRefCountType)
-  AddRef() override;
+Client::Type
+Client::GetType()
+{
+  return ASMJS;
+}
 
-  NS_IMETHOD_(MozExternalRefCountType)
-  Release() override;
+nsresult
+Client::InitOrigin(PersistenceType aPersistenceType,
+                   const nsACString& aGroup,
+                   const nsACString& aOrigin,
+                   const AtomicBool& aCanceled,
+                   UsageInfo* aUsageInfo)
+{
+  if (!aUsageInfo) {
+    return NS_OK;
+  }
+  return GetUsageForOrigin(aPersistenceType,
+                           aGroup,
+                           aOrigin,
+                           aCanceled,
+                           aUsageInfo);
+}
 
-  Type
-  GetType() override
-  {
-    return ASMJS;
+nsresult
+Client::GetUsageForOrigin(PersistenceType aPersistenceType,
+                          const nsACString& aGroup,
+                          const nsACString& aOrigin,
+                          const AtomicBool& aCanceled,
+                          UsageInfo* aUsageInfo)
+{
+  QuotaManager* qm = QuotaManager::Get();
+  MOZ_ASSERT(qm, "We were being called by the QuotaManager");
+
+  nsCOMPtr<nsIFile> directory;
+  nsresult rv = qm->GetDirectoryForOrigin(aPersistenceType, aOrigin,
+                                          getter_AddRefs(directory));
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
   }
 
-  nsresult
-  InitOrigin(PersistenceType aPersistenceType,
-             const nsACString& aGroup,
-             const nsACString& aOrigin,
-             const AtomicBool& aCanceled,
-             UsageInfo* aUsageInfo) override
-  {
-    if (!aUsageInfo) {
-      return NS_OK;
-    }
-    return GetUsageForOrigin(aPersistenceType,
-                             aGroup,
-                             aOrigin,
-                             aCanceled,
-                             aUsageInfo);
+  MOZ_ASSERT(directory, "We're here because the origin directory exists");
+
+  rv = directory->Append(NS_LITERAL_STRING(ASMJSCACHE_DIRECTORY_NAME));
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
+  }
+
+  DebugOnly<bool> exists;
+  MOZ_ASSERT(NS_SUCCEEDED(directory->Exists(&exists)) && exists);
+
+  nsCOMPtr<nsISimpleEnumerator> entries;
+  rv = directory->GetDirectoryEntries(getter_AddRefs(entries));
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
   }
 
-  nsresult
-  GetUsageForOrigin(PersistenceType aPersistenceType,
-                    const nsACString& aGroup,
-                    const nsACString& aOrigin,
-                    const AtomicBool& aCanceled,
-                    UsageInfo* aUsageInfo) override
-  {
-    QuotaManager* qm = QuotaManager::Get();
-    MOZ_ASSERT(qm, "We were being called by the QuotaManager");
+  bool hasMore;
+  while (NS_SUCCEEDED((rv = entries->HasMoreElements(&hasMore))) &&
+         hasMore && !aCanceled) {
+    nsCOMPtr<nsISupports> entry;
+    rv = entries->GetNext(getter_AddRefs(entry));
+    if (NS_WARN_IF(NS_FAILED(rv))) {
+      return rv;
+    }
 
-    nsCOMPtr<nsIFile> directory;
-    nsresult rv = qm->GetDirectoryForOrigin(aPersistenceType, aOrigin,
-                                            getter_AddRefs(directory));
-    NS_ENSURE_SUCCESS(rv, rv);
-    MOZ_ASSERT(directory, "We're here because the origin directory exists");
-
-    rv = directory->Append(NS_LITERAL_STRING(ASMJSCACHE_DIRECTORY_NAME));
-    NS_ENSURE_SUCCESS(rv, rv);
-
-    DebugOnly<bool> exists;
-    MOZ_ASSERT(NS_SUCCEEDED(directory->Exists(&exists)) && exists);
+    nsCOMPtr<nsIFile> file = do_QueryInterface(entry);
+    if (NS_WARN_IF(!file)) {
+      return NS_NOINTERFACE;
+    }
 
-    nsCOMPtr<nsISimpleEnumerator> entries;
-    rv = directory->GetDirectoryEntries(getter_AddRefs(entries));
-    NS_ENSURE_SUCCESS(rv, rv);
-
-    bool hasMore;
-    while (NS_SUCCEEDED((rv = entries->HasMoreElements(&hasMore))) &&
-           hasMore && !aCanceled) {
-      nsCOMPtr<nsISupports> entry;
-      rv = entries->GetNext(getter_AddRefs(entry));
-      NS_ENSURE_SUCCESS(rv, rv);
-
-      nsCOMPtr<nsIFile> file = do_QueryInterface(entry);
-      NS_ENSURE_TRUE(file, NS_NOINTERFACE);
+    int64_t fileSize;
+    rv = file->GetFileSize(&fileSize);
+    if (NS_WARN_IF(NS_FAILED(rv))) {
+      return rv;
+    }
 
-      int64_t fileSize;
-      rv = file->GetFileSize(&fileSize);
-      NS_ENSURE_SUCCESS(rv, rv);
-
-      MOZ_ASSERT(fileSize >= 0, "Negative size?!");
+    MOZ_ASSERT(fileSize >= 0, "Negative size?!");
 
-      // Since the client is not explicitly storing files, append to database
-      // usage which represents implicit storage allocation.
-      aUsageInfo->AppendToDatabaseUsage(uint64_t(fileSize));
-    }
-    NS_ENSURE_SUCCESS(rv, rv);
-
-    return NS_OK;
+    // Since the client is not explicitly storing files, append to database
+    // usage which represents implicit storage allocation.
+    aUsageInfo->AppendToDatabaseUsage(uint64_t(fileSize));
+  }
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
   }
 
-  void
-  OnOriginClearCompleted(PersistenceType aPersistenceType,
-                         const nsACString& aOrigin)
-                         override
-  { }
+  return NS_OK;
+}
 
-  void
-  ReleaseIOThreadObjects() override
-  { }
+void
+Client::OnOriginClearCompleted(PersistenceType aPersistenceType,
+                               const nsACString& aOrigin)
+{
+}
 
-  void
-  AbortOperations(const nsACString& aOrigin) override
-  { }
+void
+Client::ReleaseIOThreadObjects()
+{
+}
 
-  void
-  AbortOperationsForProcess(ContentParentId aContentParentId) override
-  { }
-
-  void
-  StartIdleMaintenance() override
-  { }
+void
+Client::AbortOperations(const nsACString& aOrigin)
+{
+}
 
-  void
-  StopIdleMaintenance() override
-  { }
+void
+Client::AbortOperationsForProcess(ContentParentId aContentParentId)
+{
+}
 
-  void
-  ShutdownWorkThreads() override
-  {
-    AssertIsOnBackgroundThread();
+void
+Client::StartIdleMaintenance()
+{
+}
 
-    if (sLiveParentActors) {
-      MOZ_ALWAYS_TRUE(SpinEventLoopUntil([&]() {
-        return !sLiveParentActors;
-      }));
-    }
-  }
+void
+Client::StopIdleMaintenance()
+{
+}
 
-private:
-  nsAutoRefCnt mRefCnt;
-  NS_DECL_OWNINGTHREAD
-};
+void
+Client::ShutdownWorkThreads()
+{
+  AssertIsOnBackgroundThread();
 
-NS_IMPL_ADDREF(asmjscache::Client)
-NS_IMPL_RELEASE(asmjscache::Client)
+  if (sLiveParentActors) {
+    MOZ_ALWAYS_TRUE(SpinEventLoopUntil([&]() {
+      return !sLiveParentActors;
+    }));
+  }
+}
 
 quota::Client*
 CreateClient()
 {
   return new Client();
 }
 
 } // namespace asmjscache
