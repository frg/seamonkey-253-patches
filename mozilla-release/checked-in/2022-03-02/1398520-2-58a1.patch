# HG changeset patch
# User Mats Palmgren <mats@mozilla.com>
# Date 1506514320 -7200
# Node ID b47d2dbb3dcd6d39160f17376d58630f3f7a223f
# Parent  4c8c06d94d38a4f1d1ae12e44e7668d21d8c40fe
Bug 1398520 part 2 - Remove nsGfxCheckboxControlFrame and nsGfxRadioControlFrame; use nsFormControlFrame instead.  r=dholbert

MozReview-Commit-ID: GxnZZXxHDyC

diff --git a/layout/base/nsCSSFrameConstructor.cpp b/layout/base/nsCSSFrameConstructor.cpp
--- a/layout/base/nsCSSFrameConstructor.cpp
+++ b/layout/base/nsCSSFrameConstructor.cpp
@@ -3763,18 +3763,18 @@ nsCSSFrameConstructor::FindImgControlDat
 }
 
 /* static */
 const nsCSSFrameConstructor::FrameConstructionData*
 nsCSSFrameConstructor::FindInputData(Element* aElement,
                                      nsStyleContext* aStyleContext)
 {
   static const FrameConstructionDataByInt sInputData[] = {
-    SIMPLE_INT_CREATE(NS_FORM_INPUT_CHECKBOX, NS_NewGfxCheckboxControlFrame),
-    SIMPLE_INT_CREATE(NS_FORM_INPUT_RADIO, NS_NewGfxRadioControlFrame),
+    SIMPLE_INT_CREATE(NS_FORM_INPUT_CHECKBOX, NS_NewFormControlFrame),
+    SIMPLE_INT_CREATE(NS_FORM_INPUT_RADIO, NS_NewFormControlFrame),
     SIMPLE_INT_CREATE(NS_FORM_INPUT_FILE, NS_NewFileControlFrame),
     SIMPLE_INT_CHAIN(NS_FORM_INPUT_IMAGE,
                      nsCSSFrameConstructor::FindImgControlData),
     SIMPLE_INT_CREATE(NS_FORM_INPUT_EMAIL, NS_NewTextControlFrame),
     SIMPLE_INT_CREATE(NS_FORM_INPUT_SEARCH, NS_NewTextControlFrame),
     SIMPLE_INT_CREATE(NS_FORM_INPUT_TEXT, NS_NewTextControlFrame),
     SIMPLE_INT_CREATE(NS_FORM_INPUT_TEL, NS_NewTextControlFrame),
     SIMPLE_INT_CREATE(NS_FORM_INPUT_URL, NS_NewTextControlFrame),
diff --git a/layout/forms/moz.build b/layout/forms/moz.build
--- a/layout/forms/moz.build
+++ b/layout/forms/moz.build
@@ -22,18 +22,16 @@ UNIFIED_SOURCES += [
     'nsButtonFrameRenderer.cpp',
     'nsColorControlFrame.cpp',
     'nsComboboxControlFrame.cpp',
     'nsDateTimeControlFrame.cpp',
     'nsFieldSetFrame.cpp',
     'nsFileControlFrame.cpp',
     'nsFormControlFrame.cpp',
     'nsGfxButtonControlFrame.cpp',
-    'nsGfxCheckboxControlFrame.cpp',
-    'nsGfxRadioControlFrame.cpp',
     'nsHTMLButtonControlFrame.cpp',
     'nsImageControlFrame.cpp',
     'nsLegendFrame.cpp',
     'nsListControlFrame.cpp',
     'nsMeterFrame.cpp',
     'nsNumberControlFrame.cpp',
     'nsProgressFrame.cpp',
     'nsRangeFrame.cpp',
diff --git a/layout/forms/nsFormControlFrame.cpp b/layout/forms/nsFormControlFrame.cpp
--- a/layout/forms/nsFormControlFrame.cpp
+++ b/layout/forms/nsFormControlFrame.cpp
@@ -13,34 +13,41 @@
 #include "nsDeviceContext.h"
 #include "nsIContent.h"
 #include "nsThemeConstants.h"
 
 using namespace mozilla;
 
 //#define FCF_NOISY
 
-nsFormControlFrame::nsFormControlFrame(nsStyleContext* aContext,
-                                       nsIFrame::ClassID aID)
-  : nsAtomicContainerFrame(aContext, aID)
+nsFormControlFrame*
+NS_NewFormControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext)
+{
+  return new (aPresShell) nsFormControlFrame(aContext);
+}
+
+nsFormControlFrame::nsFormControlFrame(nsStyleContext* aContext)
+  : nsAtomicContainerFrame(aContext, kClassID)
 {
 }
 
 nsFormControlFrame::~nsFormControlFrame()
 {
 }
 
 void
 nsFormControlFrame::DestroyFrom(nsIFrame* aDestructRoot)
 {
   // Unregister the access key registered in reflow
   nsFormControlFrame::RegUnRegAccessKey(static_cast<nsIFrame*>(this), false);
   nsAtomicContainerFrame::DestroyFrom(aDestructRoot);
 }
 
+NS_IMPL_FRAMEARENA_HELPERS(nsFormControlFrame)
+
 NS_QUERYFRAME_HEAD(nsFormControlFrame)
   NS_QUERYFRAME_ENTRY(nsIFormControlFrame)
 NS_QUERYFRAME_TAIL_INHERITING(nsAtomicContainerFrame)
 
 /* virtual */ nscoord
 nsFormControlFrame::GetMinISize(gfxContext *aRenderingContext)
 {
   nscoord result;
diff --git a/layout/forms/nsFormControlFrame.h b/layout/forms/nsFormControlFrame.h
--- a/layout/forms/nsFormControlFrame.h
+++ b/layout/forms/nsFormControlFrame.h
@@ -7,41 +7,36 @@
 #define nsFormControlFrame_h___
 
 #include "mozilla/Attributes.h"
 #include "nsIFormControlFrame.h"
 #include "nsAtomicContainerFrame.h"
 #include "nsDisplayList.h"
 
 /**
- * nsFormControlFrame is the base class for radio buttons and
- * checkboxes.  It also has two static methods (RegUnRegAccessKey and
+ * nsFormControlFrame is used for radio buttons and checkboxes.
+ * It also has two static methods (RegUnRegAccessKey and
  * GetScreenHeight) that are used by other form controls.
  */
-class nsFormControlFrame : public nsAtomicContainerFrame,
-                           public nsIFormControlFrame
+class nsFormControlFrame final : public nsAtomicContainerFrame,
+                                 public nsIFormControlFrame
 {
 public:
-  /**
-    * Main constructor
-    * @param aContent the content representing this frame
-    * @param aParentFrame the parent frame
-    */
-  nsFormControlFrame(nsStyleContext*, nsIFrame::ClassID);
+  NS_DECL_QUERYFRAME
+  NS_DECL_FRAMEARENA_HELPERS(nsFormControlFrame)
 
+  explicit nsFormControlFrame(nsStyleContext* aContext);
+
+  // nsIFrame replacements
   virtual bool IsFrameOfType(uint32_t aFlags) const override
   {
     return nsAtomicContainerFrame::IsFrameOfType(aFlags &
       ~(nsIFrame::eReplaced | nsIFrame::eReplacedContainsBlock));
   }
 
-  NS_DECL_QUERYFRAME
-  NS_DECL_ABSTRACT_FRAME(nsFormControlFrame)
-
-  // nsIFrame replacements
   virtual void BuildDisplayList(nsDisplayListBuilder*   aBuilder,
                                 const nsDisplayListSet& aLists) override {
     DO_GLOBAL_REFLOW_COUNT_DSP("nsFormControlFrame");
     DisplayBorderBackgroundOutline(aBuilder, aLists);
   }
 
   /**
    * Both GetMinISize and GetPrefISize will return whatever GetIntrinsicISize
diff --git a/layout/forms/nsGfxCheckboxControlFrame.cpp b/layout/forms/nsGfxCheckboxControlFrame.cpp
deleted file mode 100644
--- a/layout/forms/nsGfxCheckboxControlFrame.cpp
+++ /dev/null
@@ -1,46 +0,0 @@
-/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#include "nsGfxCheckboxControlFrame.h"
-
-#include "gfxContext.h"
-#include "gfxUtils.h"
-#include "mozilla/gfx/2D.h"
-#include "nsIContent.h"
-#include "nsCOMPtr.h"
-#include "nsLayoutUtils.h"
-#include "nsIDOMHTMLInputElement.h"
-#include "nsDisplayList.h"
-#include <algorithm>
-
-using namespace mozilla;
-using namespace mozilla::gfx;
-
-nsIFrame*
-NS_NewGfxCheckboxControlFrame(nsIPresShell* aPresShell,
-                              nsStyleContext* aContext)
-{
-  return new (aPresShell) nsGfxCheckboxControlFrame(aContext);
-}
-
-NS_IMPL_FRAMEARENA_HELPERS(nsGfxCheckboxControlFrame)
-
-// Initialize GFX-rendered state
-nsGfxCheckboxControlFrame::nsGfxCheckboxControlFrame(nsStyleContext* aContext)
-: nsFormControlFrame(aContext, kClassID)
-{
-}
-
-nsGfxCheckboxControlFrame::~nsGfxCheckboxControlFrame()
-{
-}
-
-#ifdef ACCESSIBILITY
-a11y::AccType
-nsGfxCheckboxControlFrame::AccessibleType()
-{
-  return a11y::eHTMLCheckboxType;
-}
-#endif
diff --git a/layout/forms/nsGfxCheckboxControlFrame.h b/layout/forms/nsGfxCheckboxControlFrame.h
deleted file mode 100644
--- a/layout/forms/nsGfxCheckboxControlFrame.h
+++ /dev/null
@@ -1,32 +0,0 @@
-/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-#ifndef nsGfxCheckboxControlFrame_h___
-#define nsGfxCheckboxControlFrame_h___
-
-#include "mozilla/Attributes.h"
-#include "nsFormControlFrame.h"
-
-class nsGfxCheckboxControlFrame : public nsFormControlFrame
-{
-public:
-  NS_DECL_FRAMEARENA_HELPERS(nsGfxCheckboxControlFrame)
-
-  explicit nsGfxCheckboxControlFrame(nsStyleContext* aContext);
-  virtual ~nsGfxCheckboxControlFrame();
-
-#ifdef DEBUG_FRAME_DUMP
-  virtual nsresult GetFrameName(nsAString& aResult) const override {
-    return MakeFrameName(NS_LITERAL_STRING("CheckboxControl"), aResult);
-  }
-#endif
-
-#ifdef ACCESSIBILITY
-  virtual mozilla::a11y::AccType AccessibleType() override;
-#endif
-
-};
-
-#endif
-
diff --git a/layout/forms/nsGfxRadioControlFrame.cpp b/layout/forms/nsGfxRadioControlFrame.cpp
deleted file mode 100644
--- a/layout/forms/nsGfxRadioControlFrame.cpp
+++ /dev/null
@@ -1,42 +0,0 @@
-/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#include "nsGfxRadioControlFrame.h"
-
-#include "gfx2DGlue.h"
-#include "gfxContext.h"
-#include "gfxUtils.h"
-#include "mozilla/gfx/2D.h"
-#include "mozilla/gfx/PathHelpers.h"
-#include "nsLayoutUtils.h"
-#include "nsDisplayList.h"
-
-using namespace mozilla;
-using namespace mozilla::gfx;
-
-nsIFrame*
-NS_NewGfxRadioControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext)
-{
-  return new (aPresShell) nsGfxRadioControlFrame(aContext);
-}
-
-NS_IMPL_FRAMEARENA_HELPERS(nsGfxRadioControlFrame)
-
-nsGfxRadioControlFrame::nsGfxRadioControlFrame(nsStyleContext* aContext):
-  nsFormControlFrame(aContext, kClassID)
-{
-}
-
-nsGfxRadioControlFrame::~nsGfxRadioControlFrame()
-{
-}
-
-#ifdef ACCESSIBILITY
-a11y::AccType
-nsGfxRadioControlFrame::AccessibleType()
-{
-  return a11y::eHTMLRadioButtonType;
-}
-#endif
diff --git a/layout/forms/nsGfxRadioControlFrame.h b/layout/forms/nsGfxRadioControlFrame.h
deleted file mode 100644
--- a/layout/forms/nsGfxRadioControlFrame.h
+++ /dev/null
@@ -1,28 +0,0 @@
-/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#ifndef nsGfxRadioControlFrame_h___
-#define nsGfxRadioControlFrame_h___
-
-#include "mozilla/Attributes.h"
-#include "nsFormControlFrame.h"
-
-
-// nsGfxRadioControlFrame
-
-class nsGfxRadioControlFrame : public nsFormControlFrame
-{
-public:
-  explicit nsGfxRadioControlFrame(nsStyleContext* aContext);
-  ~nsGfxRadioControlFrame();
-
-  NS_DECL_FRAMEARENA_HELPERS(nsGfxRadioControlFrame)
-
-#ifdef ACCESSIBILITY
-  virtual mozilla::a11y::AccType AccessibleType() override;
-#endif
-};
-
-#endif
diff --git a/layout/generic/nsFrameIdList.h b/layout/generic/nsFrameIdList.h
--- a/layout/generic/nsFrameIdList.h
+++ b/layout/generic/nsFrameIdList.h
@@ -21,20 +21,19 @@ FRAME_ID(nsContinuingTextFrame, Text, Le
 FRAME_ID(nsDateTimeControlFrame, DateTimeControl, NotLeaf)
 FRAME_ID(nsDeckFrame, Deck, NotLeaf)
 FRAME_ID(nsDocElementBoxFrame, Box, NotLeaf)
 FRAME_ID(nsFieldSetFrame, FieldSet, NotLeaf)
 FRAME_ID(nsFileControlFrame, Block, Leaf)
 FRAME_ID(nsFirstLetterFrame, Letter, NotLeaf)
 FRAME_ID(nsFirstLineFrame, Line, NotLeaf)
 FRAME_ID(nsFlexContainerFrame, FlexContainer, NotLeaf)
+FRAME_ID(nsFormControlFrame, FormControl, Leaf)
 FRAME_ID(nsFrame, None, NotLeaf)
 FRAME_ID(nsGfxButtonControlFrame, GfxButtonControl, Leaf)
-FRAME_ID(nsGfxCheckboxControlFrame, FormControl, Leaf)
-FRAME_ID(nsGfxRadioControlFrame, FormControl, Leaf)
 FRAME_ID(nsGridContainerFrame, GridContainer, NotLeaf)
 FRAME_ID(nsGridRowGroupFrame, Box, NotLeaf)
 FRAME_ID(nsGridRowLeafFrame, Box, NotLeaf)
 FRAME_ID(nsGroupBoxFrame, Box, NotLeaf)
 FRAME_ID(nsHTMLButtonControlFrame, HTMLButtonControl, NotLeaf)
 FRAME_ID(nsHTMLCanvasFrame, HTMLCanvas, NotLeaf)
 FRAME_ID(nsHTMLFramesetBlankFrame, None, Leaf)
 FRAME_ID(nsHTMLFramesetBorderFrame, None, Leaf)
@@ -149,17 +148,16 @@ FRAME_ID(ViewportFrame, Viewport, NotLea
 // The following ABSTRACT_FRAME_IDs needs to come after the above
 // FRAME_IDs, because we have two separate enums, one that includes
 // only FRAME_IDs and another which includes both and we depend on
 // FRAME_IDs to have the same number in both.
 // See ClassID (the former) and FrameIID in nsQueryFrame.h.
 
 // Non-concrete classes (for FrameIID use)
 ABSTRACT_FRAME_ID(nsContainerFrame)
-ABSTRACT_FRAME_ID(nsFormControlFrame)
 ABSTRACT_FRAME_ID(nsIFrame)
 ABSTRACT_FRAME_ID(nsLeafFrame)
 ABSTRACT_FRAME_ID(nsMathMLContainerFrame)
 ABSTRACT_FRAME_ID(nsRubyContentFrame)
 ABSTRACT_FRAME_ID(nsSplittableFrame)
 ABSTRACT_FRAME_ID(nsSVGDisplayContainerFrame)
 ABSTRACT_FRAME_ID(nsSVGGradientFrame)
 ABSTRACT_FRAME_ID(nsSVGPaintServerFrame)
diff --git a/layout/generic/nsHTMLParts.h b/layout/generic/nsHTMLParts.h
--- a/layout/generic/nsHTMLParts.h
+++ b/layout/generic/nsHTMLParts.h
@@ -7,16 +7,17 @@
 
 #ifndef nsHTMLParts_h___
 #define nsHTMLParts_h___
 
 #include "nscore.h"
 #include "nsISupports.h"
 #include "nsIFrame.h"
 class nsComboboxControlFrame;
+class nsFormControlFrame;
 class nsIAtom;
 class nsNodeInfoManager;
 class nsIContent;
 class nsIDocument;
 class nsIFrame;
 class nsIHTMLContentSink;
 class nsIFragmentContentSink;
 class nsStyleContext;
@@ -126,43 +127,41 @@ nsFirstLetterFrame*
 NS_NewFirstLetterFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 class nsFirstLineFrame;
 nsFirstLineFrame*
 NS_NewFirstLineFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 
 // forms
 nsContainerFrame*
 NS_NewGfxButtonControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
+nsFormControlFrame*
+NS_NewFormControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
 NS_NewNativeButtonControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
 NS_NewImageControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsContainerFrame*
 NS_NewHTMLButtonControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
-NS_NewGfxCheckboxControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
-nsIFrame*
 NS_NewNativeCheckboxControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsContainerFrame*
 NS_NewFieldSetFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
 NS_NewFileControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
 NS_NewColorControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
 NS_NewLegendFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
 NS_NewNativeTextControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
 NS_NewTextControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
 NS_NewGfxAutoTextControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
-NS_NewGfxRadioControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
-nsIFrame*
 NS_NewNativeRadioControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsIFrame*
 NS_NewNativeSelectControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsContainerFrame*
 NS_NewListControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext);
 nsComboboxControlFrame*
 NS_NewComboboxControlFrame(nsIPresShell* aPresShell, nsStyleContext* aContext, nsFrameState aFlags);
 nsIFrame*
