# HG changeset patch
# User David Anderson <danderson@mozilla.com>
# Date 1509476551 25200
# Node ID fad3fd7b113de08a1b57025cf0f2c1de65d97d29
# Parent  4e5baa18f93205b421a1c104dbd234fb4f3a283d
Allow cloning draw commands. (bug 1395478 part 5, r=rhunt)

diff --git a/gfx/2d/DrawCommand.h b/gfx/2d/DrawCommand.h
--- a/gfx/2d/DrawCommand.h
+++ b/gfx/2d/DrawCommand.h
@@ -8,16 +8,17 @@
 #define MOZILLA_GFX_DRAWCOMMAND_H_
 
 #include <math.h>
 
 #include "2D.h"
 #include "Blur.h"
 #include "Filters.h"
 #include <vector>
+#include "CaptureCommandList.h"
 
 namespace mozilla {
 namespace gfx {
 
 enum class CommandType : int8_t {
   DRAWSURFACE = 0,
   DRAWFILTER,
   DRAWSURFACEWITHSHADOW,
@@ -45,31 +46,33 @@ enum class CommandType : int8_t {
 };
 
 class DrawingCommand
 {
 public:
   virtual ~DrawingCommand() {}
 
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix* aTransform = nullptr) const = 0;
-
   virtual bool GetAffectedRect(Rect& aDeviceRect, const Matrix& aTransform) const { return false; }
+  virtual void CloneInto(CaptureCommandList* aList) = 0;
 
   CommandType GetType() { return mType; }
 
 protected:
   explicit DrawingCommand(CommandType aType)
     : mType(aType)
   {
   }
 
 private:
   CommandType mType;
 };
 
+#define CLONE_INTO(Type) new (aList->Append<Type>()) Type
+
 class StrokeOptionsCommand : public DrawingCommand
 {
 public:
   StrokeOptionsCommand(CommandType aType,
                        const StrokeOptions& aStrokeOptions)
     : DrawingCommand(aType)
     , mStrokeOptions(aStrokeOptions)
   {
@@ -163,16 +166,20 @@ public:
                      const DrawOptions& aOptions)
     : DrawingCommand(CommandType::DRAWSURFACE)
     , mSurface(aSurface), mDest(aDest)
     , mSource(aSource), mSurfOptions(aSurfOptions)
     , mOptions(aOptions)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(DrawSurfaceCommand)(mSurface, mDest, mSource, mSurfOptions, mOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->DrawSurface(mSurface, mDest, mSource, mSurfOptions, mOptions);
   }
 
 private:
   RefPtr<SourceSurface> mSurface;
   Rect mDest;
@@ -187,16 +194,20 @@ public:
   DrawFilterCommand(FilterNode* aFilter, const Rect& aSourceRect,
                     const Point& aDestPoint, const DrawOptions& aOptions)
     : DrawingCommand(CommandType::DRAWSURFACE)
     , mFilter(aFilter), mSourceRect(aSourceRect)
     , mDestPoint(aDestPoint), mOptions(aOptions)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(DrawFilterCommand)(mFilter, mSourceRect, mDestPoint, mOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->DrawFilter(mFilter, mSourceRect, mDestPoint, mOptions);
   }
 
 private:
   RefPtr<FilterNode> mFilter;
   Rect mSourceRect;
@@ -208,16 +219,20 @@ class ClearRectCommand : public DrawingC
 {
 public:
   explicit ClearRectCommand(const Rect& aRect)
     : DrawingCommand(CommandType::CLEARRECT)
     , mRect(aRect)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(ClearRectCommand)(mRect);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->ClearRect(mRect);
   }
 
 private:
   Rect mRect;
 };
@@ -230,16 +245,20 @@ public:
                      const IntPoint& aDestination)
     : DrawingCommand(CommandType::COPYSURFACE)
     , mSurface(aSurface)
     , mSourceRect(aSourceRect)
     , mDestination(aDestination)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(CopySurfaceCommand)(mSurface, mSourceRect, mDestination);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix* aTransform) const
   {
     MOZ_ASSERT(!aTransform || !aTransform->HasNonIntegerTranslation());
     Point dest(Float(mDestination.x), Float(mDestination.y));
     if (aTransform) {
       dest = aTransform->TransformPoint(dest);
     }
     aDT->CopySurface(mSurface, mSourceRect, IntPoint(uint32_t(dest.x), uint32_t(dest.y)));
@@ -259,16 +278,20 @@ public:
                   const DrawOptions& aOptions)
     : DrawingCommand(CommandType::FILLRECT)
     , mRect(aRect)
     , mPattern(aPattern)
     , mOptions(aOptions)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(FillRectCommand)(mRect, mPattern, mOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->FillRect(mRect, mPattern, mOptions);
   }
 
   bool GetAffectedRect(Rect& aDeviceRect, const Matrix& aTransform) const
   {
     aDeviceRect = aTransform.TransformBounds(mRect);
@@ -290,16 +313,20 @@ public:
                     const DrawOptions& aOptions)
     : StrokeOptionsCommand(CommandType::STROKERECT, aStrokeOptions)
     , mRect(aRect)
     , mPattern(aPattern)
     , mOptions(aOptions)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(StrokeRectCommand)(mRect, mPattern, mStrokeOptions, mOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->StrokeRect(mRect, mPattern, mStrokeOptions, mOptions);
   }
 
 private:
   Rect mRect;
   StoredPattern mPattern;
@@ -317,16 +344,20 @@ public:
     : StrokeOptionsCommand(CommandType::STROKELINE, aStrokeOptions)
     , mStart(aStart)
     , mEnd(aEnd)
     , mPattern(aPattern)
     , mOptions(aOptions)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(StrokeLineCommand)(mStart, mEnd, mPattern, mStrokeOptions, mOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->StrokeLine(mStart, mEnd, mPattern, mStrokeOptions, mOptions);
   }
 
 private:
   Point mStart;
   Point mEnd;
@@ -342,16 +373,20 @@ public:
               const DrawOptions& aOptions)
     : DrawingCommand(CommandType::FILL)
     , mPath(const_cast<Path*>(aPath))
     , mPattern(aPattern)
     , mOptions(aOptions)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(FillCommand)(mPath, mPattern, mOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->Fill(mPath, mPattern, mOptions);
   }
 
   bool GetAffectedRect(Rect& aDeviceRect, const Matrix& aTransform) const
   {
     aDeviceRect = mPath->GetBounds(aTransform);
@@ -414,16 +449,20 @@ public:
                 const DrawOptions& aOptions)
     : StrokeOptionsCommand(CommandType::STROKE, aStrokeOptions)
     , mPath(const_cast<Path*>(aPath))
     , mPattern(aPattern)
     , mOptions(aOptions)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(StrokeCommand)(mPath, mPattern, mStrokeOptions, mOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->Stroke(mPath, mPattern, mStrokeOptions, mOptions);
   }
 
   bool GetAffectedRect(Rect& aDeviceRect, const Matrix& aTransform) const
   {
     aDeviceRect = PathExtentsToMaxStrokeExtents(mStrokeOptions, mPath->GetBounds(aTransform), aTransform);
@@ -450,16 +489,24 @@ public:
     , mPattern(aPattern)
     , mOptions(aOptions)
     , mRenderingOptions(const_cast<GlyphRenderingOptions*>(aRenderingOptions))
   {
     mGlyphs.resize(aBuffer.mNumGlyphs);
     memcpy(&mGlyphs.front(), aBuffer.mGlyphs, sizeof(Glyph) * aBuffer.mNumGlyphs);
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    GlyphBuffer glyphs = {
+      mGlyphs.data(),
+      (uint32_t)mGlyphs.size(),
+    };
+    CLONE_INTO(FillGlyphsCommand)(mFont, glyphs, mPattern, mOptions, mRenderingOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     GlyphBuffer buf;
     buf.mNumGlyphs = mGlyphs.size();
     buf.mGlyphs = &mGlyphs.front();
     aDT->FillGlyphs(mFont, buf, mPattern, mOptions, mRenderingOptions);
   }
 
@@ -486,16 +533,24 @@ public:
     , mPattern(aPattern)
     , mOptions(aOptions)
     , mRenderingOptions(const_cast<GlyphRenderingOptions*>(aRenderingOptions))
   {
     mGlyphs.resize(aBuffer.mNumGlyphs);
     memcpy(&mGlyphs.front(), aBuffer.mGlyphs, sizeof(Glyph) * aBuffer.mNumGlyphs);
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    GlyphBuffer glyphs = {
+      mGlyphs.data(),
+      (uint32_t)mGlyphs.size(),
+    };
+    CLONE_INTO(StrokeGlyphsCommand)(mFont, glyphs, mPattern, mStrokeOptions, mOptions, mRenderingOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     GlyphBuffer buf;
     buf.mNumGlyphs = mGlyphs.size();
     buf.mGlyphs = &mGlyphs.front();
     aDT->StrokeGlyphs(mFont, buf, mPattern, mStrokeOptions, mOptions, mRenderingOptions);
   }
 
@@ -515,16 +570,20 @@ public:
               const DrawOptions& aOptions)
     : DrawingCommand(CommandType::MASK)
     , mSource(aSource)
     , mMask(aMask)
     , mOptions(aOptions)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(MaskCommand)(mSource, mMask, mOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->Mask(mSource, mMask, mOptions);
   }
 
 private:
   StoredPattern mSource;
   StoredPattern mMask;
@@ -541,16 +600,20 @@ public:
     : DrawingCommand(CommandType::MASKSURFACE)
     , mSource(aSource)
     , mMask(const_cast<SourceSurface*>(aMask))
     , mOffset(aOffset)
     , mOptions(aOptions)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(MaskSurfaceCommand)(mSource, mMask, mOffset, mOptions);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->MaskSurface(mSource, mMask, mOffset, mOptions);
   }
 
 private:
   StoredPattern mSource;
   RefPtr<SourceSurface> mMask;
@@ -562,16 +625,20 @@ class PushClipCommand : public DrawingCo
 {
 public:
   explicit PushClipCommand(const Path* aPath)
     : DrawingCommand(CommandType::PUSHCLIP)
     , mPath(const_cast<Path*>(aPath))
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(PushClipCommand)(mPath);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PushClip(mPath);
   }
 
 private:
   RefPtr<Path> mPath;
 };
@@ -580,16 +647,20 @@ class PushClipRectCommand : public Drawi
 {
 public:
   explicit PushClipRectCommand(const Rect& aRect)
     : DrawingCommand(CommandType::PUSHCLIPRECT)
     , mRect(aRect)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(PushClipRectCommand)(mRect);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PushClipRect(mRect);
   }
 
 private:
   Rect mRect;
 };
@@ -608,16 +679,20 @@ public:
     , mOpacity(aOpacity)
     , mMask(aMask)
     , mMaskTransform(aMaskTransform)
     , mBounds(aBounds)
     , mCopyBackground(aCopyBackground)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(PushLayerCommand)(mOpaque, mOpacity, mMask, mMaskTransform, mBounds, mCopyBackground);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PushLayer(mOpaque, mOpacity, mMask,
                    mMaskTransform, mBounds, mCopyBackground);
   }
 
 private:
   bool mOpaque;
@@ -631,46 +706,58 @@ private:
 class PopClipCommand : public DrawingCommand
 {
 public:
   PopClipCommand()
     : DrawingCommand(CommandType::POPCLIP)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(PopClipCommand)();
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PopClip();
   }
 };
 
 class PopLayerCommand : public DrawingCommand
 {
 public:
   PopLayerCommand()
     : DrawingCommand(CommandType::POPLAYER)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(PopLayerCommand)();
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->PopLayer();
   }
 };
 
 class SetTransformCommand : public DrawingCommand
 {
   friend class DrawTargetCaptureImpl;
 public:
   explicit SetTransformCommand(const Matrix& aTransform)
     : DrawingCommand(CommandType::SETTRANSFORM)
     , mTransform(aTransform)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(SetTransformCommand)(mTransform);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix* aMatrix) const
   {
     if (aMatrix) {
       aDT->SetTransform(mTransform * (*aMatrix));
     } else {
       aDT->SetTransform(mTransform);
     }
   }
@@ -684,16 +771,20 @@ class SetPermitSubpixelAACommand : publi
   friend class DrawTargetCaptureImpl;
 public:
   explicit SetPermitSubpixelAACommand(bool aPermitSubpixelAA)
     : DrawingCommand(CommandType::SETPERMITSUBPIXELAA)
     , mPermitSubpixelAA(aPermitSubpixelAA)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(SetPermitSubpixelAACommand)(mPermitSubpixelAA);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix* aMatrix) const
   {
     aDT->SetPermitSubpixelAA(mPermitSubpixelAA);
   }
 
 private:
   bool mPermitSubpixelAA;
 };
@@ -701,34 +792,44 @@ private:
 class FlushCommand : public DrawingCommand
 {
 public:
   explicit FlushCommand()
     : DrawingCommand(CommandType::FLUSH)
   {
   }
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(FlushCommand)();
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const
   {
     aDT->Flush();
   }
 };
 
 class BlurCommand : public DrawingCommand
 {
 public:
   explicit BlurCommand(const AlphaBoxBlur& aBlur)
    : DrawingCommand(CommandType::BLUR)
    , mBlur(aBlur)
   {}
 
+  void CloneInto(CaptureCommandList* aList) {
+    CLONE_INTO(BlurCommand)(mBlur);
+  }
+
   virtual void ExecuteOnDT(DrawTarget* aDT, const Matrix*) const {
     aDT->Blur(mBlur);
   }
 
 private:
   AlphaBoxBlur mBlur;
 };
 
+#undef CLONE_INTO
+
 } // namespace gfx
 } // namespace mozilla
 
 #endif /* MOZILLA_GFX_DRAWCOMMAND_H_ */
