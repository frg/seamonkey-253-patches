# HG changeset patch
# User Markus Stange <mstange@themasta.com>
# Date 1501809165 14400
#      Thu Aug 03 21:12:45 2017 -0400
# Node ID 4a26bf37c264cf27d36112dd17c4d93a5d6c8aea
# Parent  5aec29a3a207ae29876393888fb0bdc8058a0af5
Bug 1387155 - Make nsIProfiler::dumpProfileToFileAsync return a promise. r=njn

MozReview-Commit-ID: 5yJMYrcRPBM

diff --git a/tools/profiler/gecko/nsIProfiler.idl b/tools/profiler/gecko/nsIProfiler.idl
--- a/tools/profiler/gecko/nsIProfiler.idl
+++ b/tools/profiler/gecko/nsIProfiler.idl
@@ -56,18 +56,22 @@ interface nsIProfiler : nsISupports
   jsval getProfileData([optional] in double aSinceTime);
 
   [implicit_jscontext]
   nsISupports getProfileDataAsync([optional] in double aSinceTime);
 
   [implicit_jscontext]
   nsISupports getProfileDataAsArrayBuffer([optional] in double aSinceTime);
 
-  void dumpProfileToFileAsync(in ACString aFilename,
-                              [optional] in double aSinceTime);
+  /**
+   * Returns a promise that resolves once the file has been written.
+   */
+  [implicit_jscontext]
+  nsISupports dumpProfileToFileAsync(in ACString aFilename,
+                                     [optional] in double aSinceTime);
 
   boolean IsActive();
 
   /**
    * Returns an array of the features that are supported in this build.
    * Features may vary depending on platform and build flags.
    */
   void GetFeatures(out uint32_t aCount, [retval, array, size_is(aCount)] out string aFeatures);
diff --git a/tools/profiler/gecko/nsProfiler.cpp b/tools/profiler/gecko/nsProfiler.cpp
--- a/tools/profiler/gecko/nsProfiler.cpp
+++ b/tools/profiler/gecko/nsProfiler.cpp
@@ -347,43 +347,66 @@ nsProfiler::GetProfileDataAsArrayBuffer(
     });
 
   promise.forget(aPromise);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsProfiler::DumpProfileToFileAsync(const nsACString& aFilename,
-                                   double aSinceTime)
+                                   double aSinceTime, JSContext* aCx,
+                                   nsISupports** aPromise)
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   if (!profiler_is_active()) {
     return NS_ERROR_FAILURE;
   }
 
+  if (NS_WARN_IF(!aCx)) {
+    return NS_ERROR_FAILURE;
+  }
+
+  nsIGlobalObject* globalObject =
+    xpc::NativeGlobal(JS::CurrentGlobalOrNull(aCx));
+
+  if (NS_WARN_IF(!globalObject)) {
+    return NS_ERROR_FAILURE;
+  }
+
+  ErrorResult result;
+  RefPtr<Promise> promise = Promise::Create(globalObject, result);
+  if (NS_WARN_IF(result.Failed())) {
+    return result.StealNSResult();
+  }
+
   nsCString filename(aFilename);
 
   StartGathering(aSinceTime)->Then(
     GetMainThreadSerialEventTarget(), __func__,
-    [filename](const nsCString& aResult) {
+    [filename, promise](const nsCString& aResult) {
       nsCOMPtr<nsIFile> file = do_CreateInstance(NS_LOCAL_FILE_CONTRACTID);
       nsresult rv = file->InitWithNativePath(filename);
       if (NS_FAILED(rv)) {
         MOZ_CRASH();
       }
       nsCOMPtr<nsIFileOutputStream> of =
         do_CreateInstance("@mozilla.org/network/file-output-stream;1");
       of->Init(file, -1, -1, 0);
       uint32_t sz;
       of->Write(aResult.get(), aResult.Length(), &sz);
       of->Close();
+
+      promise->MaybeResolveWithUndefined();
     },
-    [](nsresult aRv) { });
+    [promise](nsresult aRv) {
+      promise->MaybeReject(aRv);
+    });
 
+  promise.forget(aPromise);
   return NS_OK;
 }
 
 
 NS_IMETHODIMP
 nsProfiler::GetElapsedTime(double* aElapsedTime)
 {
   *aElapsedTime = profiler_time();
