# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1501740328 -28800
# Node ID be090e170cc642dadceeb3a73c146043f25efa34
# Parent  87222afdd01243de8ff12b6df996ca0129400818
Bug 1386951 - have ChannelMediaDecoder implement CreateStateMachine() and fix includes. r=jya

The implementation will be shared by most of the sub-classes except OggDecoder
which needs to call demuxer->SetChainingEvents().

http://searchfox.org/mozilla-central/rev/f0e4ae5f8c40ba742214e89aba3f554da0b89a33/dom/media/ogg/OggDecoder.cpp#25

This helps reducing code changes whenever we add a field to MediaFormatReaderInit.

MozReview-Commit-ID: 5K8NY1oxol4

diff --git a/dom/media/ADTSDecoder.cpp b/dom/media/ADTSDecoder.cpp
--- a/dom/media/ADTSDecoder.cpp
+++ b/dom/media/ADTSDecoder.cpp
@@ -2,41 +2,29 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ADTSDecoder.h"
 #include "ADTSDemuxer.h"
 #include "MediaContainerType.h"
-#include "MediaDecoderStateMachine.h"
-#include "MediaFormatReader.h"
 #include "PDMFactory.h"
 
 namespace mozilla {
 
 ChannelMediaDecoder*
 ADTSDecoder::Clone(MediaDecoderInit& aInit)
 {
   if (!IsEnabled())
     return nullptr;
 
   return new ADTSDecoder(aInit);
 }
 
-MediaDecoderStateMachine*
-ADTSDecoder::CreateStateMachine()
-{
-  MediaFormatReaderInit init;
-  init.mCrashHelper = GetOwner()->CreateGMPCrashHelper();
-  init.mFrameStats = mFrameStats;
-  mReader = new MediaFormatReader(init, new ADTSDemuxer(mResource));
-  return new MediaDecoderStateMachine(this, mReader);
-}
-
 /* static */ bool
 ADTSDecoder::IsEnabled()
 {
   RefPtr<PDMFactory> platform = new PDMFactory();
   return platform->SupportsMimeType(NS_LITERAL_CSTRING("audio/mp4a-latm"),
                                     /* DecoderDoctorDiagnostics* */ nullptr);
 }
 
diff --git a/dom/media/ADTSDecoder.h b/dom/media/ADTSDecoder.h
--- a/dom/media/ADTSDecoder.h
+++ b/dom/media/ADTSDecoder.h
@@ -17,17 +17,16 @@ class ADTSDecoder : public ChannelMediaD
 {
 public:
   // MediaDecoder interface.
   explicit ADTSDecoder(MediaDecoderInit& aInit)
     : ChannelMediaDecoder(aInit)
   {
   }
   ChannelMediaDecoder* Clone(MediaDecoderInit& aInit) override;
-  MediaDecoderStateMachine* CreateStateMachine() override;
 
   // Returns true if the ADTS backend is pref'ed on, and we're running on a
   // platform that is likely to have decoders for the format.
   static bool IsEnabled();
   static bool IsSupportedType(const MediaContainerType& aContainerType);
 };
 
 } // namespace mozilla
diff --git a/dom/media/ChannelMediaDecoder.cpp b/dom/media/ChannelMediaDecoder.cpp
--- a/dom/media/ChannelMediaDecoder.cpp
+++ b/dom/media/ChannelMediaDecoder.cpp
@@ -1,15 +1,18 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "ChannelMediaDecoder.h"
+#include "DecoderTraits.h"
+#include "MediaDecoderStateMachine.h"
+#include "MediaFormatReader.h"
 #include "MediaResource.h"
 #include "MediaShutdownManager.h"
 
 namespace mozilla {
 
 ChannelMediaDecoder::ResourceCallback::ResourceCallback(
   AbstractThread* aMainThread)
   : mAbstractMainThread(aMainThread)
@@ -156,16 +159,29 @@ ChannelMediaDecoder::ResourceCallback::N
 
 ChannelMediaDecoder::ChannelMediaDecoder(MediaDecoderInit& aInit)
   : MediaDecoder(aInit)
   , mResourceCallback(new ResourceCallback(aInit.mOwner->AbstractMainThread()))
 {
   mResourceCallback->Connect(this);
 }
 
+MediaDecoderStateMachine* ChannelMediaDecoder::CreateStateMachine()
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  MediaFormatReaderInit init;
+  init.mVideoFrameContainer = GetVideoFrameContainer();
+  init.mKnowsCompositor = GetCompositor();
+  init.mCrashHelper = GetOwner()->CreateGMPCrashHelper();
+  init.mFrameStats = mFrameStats;
+  init.mResource = mResource;
+  mReader = DecoderTraits::CreateReader(ContainerType(), init);
+  return new MediaDecoderStateMachine(this, mReader);
+}
+
 void
 ChannelMediaDecoder::Shutdown()
 {
   mResourceCallback->Disconnect();
   MediaDecoder::Shutdown();
 }
 
 nsresult
diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -53,16 +53,18 @@ class ChannelMediaDecoder : public Media
   };
 
 protected:
   RefPtr<ResourceCallback> mResourceCallback;
 
 public:
   explicit ChannelMediaDecoder(MediaDecoderInit& aInit);
 
+  MediaDecoderStateMachine* CreateStateMachine() override;
+
   void Shutdown() override;
 
   // Create a new decoder of the same type as this one.
   // Subclasses must implement this.
   virtual ChannelMediaDecoder* Clone(MediaDecoderInit& aInit) = 0;
 
   virtual nsresult Load(nsIChannel* aChannel,
                         bool aIsPrivateBrowsing,
diff --git a/dom/media/flac/FlacDecoder.cpp b/dom/media/flac/FlacDecoder.cpp
--- a/dom/media/flac/FlacDecoder.cpp
+++ b/dom/media/flac/FlacDecoder.cpp
@@ -2,42 +2,30 @@
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "FlacDecoder.h"
 #include "FlacDemuxer.h"
 #include "MediaContainerType.h"
-#include "MediaDecoderStateMachine.h"
-#include "MediaFormatReader.h"
 #include "MediaPrefs.h"
 
 namespace mozilla {
 
 ChannelMediaDecoder*
 FlacDecoder::Clone(MediaDecoderInit& aInit)
 {
   if (!IsEnabled()) {
     return nullptr;
   }
 
   return new FlacDecoder(aInit);
 }
 
-MediaDecoderStateMachine*
-FlacDecoder::CreateStateMachine()
-{
-  MediaFormatReaderInit init;
-  init.mCrashHelper = GetOwner()->CreateGMPCrashHelper();
-  init.mFrameStats = mFrameStats;
-  mReader = new MediaFormatReader(init, new FlacDemuxer(mResource));
-  return new MediaDecoderStateMachine(this, mReader);
-}
-
 /* static */ bool
 FlacDecoder::IsEnabled()
 {
 #ifdef MOZ_FFVPX
   return MediaPrefs::FlacEnabled();
 #else
   // Until bug 1295886 is fixed.
   return false;
diff --git a/dom/media/flac/FlacDecoder.h b/dom/media/flac/FlacDecoder.h
--- a/dom/media/flac/FlacDecoder.h
+++ b/dom/media/flac/FlacDecoder.h
@@ -17,17 +17,16 @@ class FlacDecoder : public ChannelMediaD
 {
 public:
   // MediaDecoder interface.
   explicit FlacDecoder(MediaDecoderInit& aInit)
     : ChannelMediaDecoder(aInit)
   {
   }
   ChannelMediaDecoder* Clone(MediaDecoderInit& aInit) override;
-  MediaDecoderStateMachine* CreateStateMachine() override;
 
   // Returns true if the Flac backend is pref'ed on, and we're running on a
   // platform that is likely to have decoders for the format.
   static bool IsEnabled();
   static bool IsSupportedType(const MediaContainerType& aContainerType);
 };
 
 } // namespace mozilla
diff --git a/dom/media/fmp4/MP4Decoder.cpp b/dom/media/fmp4/MP4Decoder.cpp
--- a/dom/media/fmp4/MP4Decoder.cpp
+++ b/dom/media/fmp4/MP4Decoder.cpp
@@ -1,17 +1,17 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "MP4Decoder.h"
 #include "MediaContainerType.h"
-#include "MediaDecoderStateMachine.h"
+#include "MediaFormatReader.h"
 #include "MP4Demuxer.h"
 #include "mozilla/Preferences.h"
 #include "nsCharSeparatedTokenizer.h"
 #include "mozilla/CDMProxy.h"
 #include "mozilla/Logging.h"
 #include "mozilla/SharedThreadPool.h"
 #include "nsMimeTypes.h"
 #include "VideoUtils.h"
@@ -25,27 +25,16 @@
 
 namespace mozilla {
 
 MP4Decoder::MP4Decoder(MediaDecoderInit& aInit)
   : ChannelMediaDecoder(aInit)
 {
 }
 
-MediaDecoderStateMachine* MP4Decoder::CreateStateMachine()
-{
-  MediaFormatReaderInit init;
-  init.mVideoFrameContainer = GetVideoFrameContainer();
-  init.mKnowsCompositor = GetCompositor();
-  init.mCrashHelper = GetOwner()->CreateGMPCrashHelper();
-  init.mFrameStats = mFrameStats;
-  mReader = new MediaFormatReader(init, new MP4Demuxer(mResource));
-  return new MediaDecoderStateMachine(this, mReader);
-}
-
 static bool
 IsWhitelistedH264Codec(const nsAString& aCodec)
 {
   int16_t profile = 0, level = 0;
 
   if (!ExtractH264CodecDetails(aCodec, profile, level)) {
     return false;
   }
diff --git a/dom/media/fmp4/MP4Decoder.h b/dom/media/fmp4/MP4Decoder.h
--- a/dom/media/fmp4/MP4Decoder.h
+++ b/dom/media/fmp4/MP4Decoder.h
@@ -2,17 +2,16 @@
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #if !defined(MP4Decoder_h_)
 #define MP4Decoder_h_
 
 #include "ChannelMediaDecoder.h"
-#include "MediaFormatReader.h"
 #include "mozilla/dom/Promise.h"
 #include "mozilla/layers/KnowsCompositor.h"
 #include "nsStringFwd.h"
 
 namespace mozilla {
 
 class MediaContainerType;
 
@@ -25,18 +24,16 @@ public:
   ChannelMediaDecoder* Clone(MediaDecoderInit& aInit) override
   {
     if (!IsEnabled()) {
       return nullptr;
     }
     return new MP4Decoder(aInit);
   }
 
-  MediaDecoderStateMachine* CreateStateMachine() override;
-
   // Returns true if aContainerType is an MP4 type that we think we can render
   // with the a platform decoder backend.
   // If provided, codecs are checked for support.
   static bool IsSupportedType(const MediaContainerType& aContainerType,
                               DecoderDoctorDiagnostics* aDiagnostics);
 
   // Return true if aMimeType is a one of the strings used by our demuxers to
   // identify H264. Does not parse general content type strings, i.e. white
diff --git a/dom/media/mp3/MP3Decoder.cpp b/dom/media/mp3/MP3Decoder.cpp
--- a/dom/media/mp3/MP3Decoder.cpp
+++ b/dom/media/mp3/MP3Decoder.cpp
@@ -3,41 +3,30 @@
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "MP3Decoder.h"
 
 #include "MediaContainerType.h"
-#include "MediaDecoderStateMachine.h"
-#include "MediaFormatReader.h"
 #include "MP3Demuxer.h"
 #include "PDMFactory.h"
 
 namespace mozilla {
 
 ChannelMediaDecoder*
 MP3Decoder::Clone(MediaDecoderInit& aInit)
 {
   if (!IsEnabled()) {
     return nullptr;
   }
   return new MP3Decoder(aInit);
 }
 
-MediaDecoderStateMachine*
-MP3Decoder::CreateStateMachine() {
-  MediaFormatReaderInit init;
-  init.mCrashHelper = GetOwner()->CreateGMPCrashHelper();
-  init.mFrameStats = mFrameStats;
-  mReader = new MediaFormatReader(init, new MP3Demuxer(mResource));
-  return new MediaDecoderStateMachine(this, mReader);
-}
-
 /* static */
 bool
 MP3Decoder::IsEnabled() {
   RefPtr<PDMFactory> platform = new PDMFactory();
   return platform->SupportsMimeType(NS_LITERAL_CSTRING("audio/mpeg"),
                                     /* DecoderDoctorDiagnostics* */ nullptr);
 }
 
diff --git a/dom/media/mp3/MP3Decoder.h b/dom/media/mp3/MP3Decoder.h
--- a/dom/media/mp3/MP3Decoder.h
+++ b/dom/media/mp3/MP3Decoder.h
@@ -16,17 +16,16 @@ class MP3Decoder : public ChannelMediaDe
 {
 public:
   // MediaDecoder interface.
   explicit MP3Decoder(MediaDecoderInit& aInit)
     : ChannelMediaDecoder(aInit)
   {
   }
   ChannelMediaDecoder* Clone(MediaDecoderInit& aInit) override;
-  MediaDecoderStateMachine* CreateStateMachine() override;
 
   // Returns true if the MP3 backend is preffed on, and we're running on a
   // platform that is likely to have decoders for the format.
   static bool IsEnabled();
   static bool IsSupportedType(const MediaContainerType& aContainerType);
 };
 
 } // namespace mozilla
diff --git a/dom/media/wave/WaveDecoder.cpp b/dom/media/wave/WaveDecoder.cpp
--- a/dom/media/wave/WaveDecoder.cpp
+++ b/dom/media/wave/WaveDecoder.cpp
@@ -1,39 +1,27 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "WaveDemuxer.h"
 #include "MediaContainerType.h"
-#include "MediaDecoderStateMachine.h"
 #include "WaveDecoder.h"
-#include "MediaFormatReader.h"
 #include "PDMFactory.h"
 
 namespace mozilla {
 
 ChannelMediaDecoder*
 WaveDecoder::Clone(MediaDecoderInit& aInit)
 {
   return new WaveDecoder(aInit);
 }
 
-MediaDecoderStateMachine*
-WaveDecoder::CreateStateMachine()
-{
-  MediaFormatReaderInit init;
-  init.mCrashHelper = GetOwner()->CreateGMPCrashHelper();
-  init.mFrameStats = mFrameStats;
-  mReader = new MediaFormatReader(init, new WAVDemuxer(mResource));
-  return new MediaDecoderStateMachine(this, mReader);
-}
-
 /* static */ bool
 WaveDecoder::IsSupportedType(const MediaContainerType& aContainerType)
 {
   if (!IsWaveEnabled()) {
     return false;
   }
   if (aContainerType.Type() == MEDIAMIMETYPE("audio/wave")
       || aContainerType.Type() == MEDIAMIMETYPE("audio/x-wav")
diff --git a/dom/media/wave/WaveDecoder.h b/dom/media/wave/WaveDecoder.h
--- a/dom/media/wave/WaveDecoder.h
+++ b/dom/media/wave/WaveDecoder.h
@@ -16,17 +16,16 @@ class WaveDecoder : public ChannelMediaD
 {
 public:
   // MediaDecoder interface.
   explicit WaveDecoder(MediaDecoderInit& aInit)
     : ChannelMediaDecoder(aInit)
   {
   }
   ChannelMediaDecoder* Clone(MediaDecoderInit& aInit) override;
-  MediaDecoderStateMachine* CreateStateMachine() override;
 
   // Returns true if the Wave backend is pref'ed on, and we're running on a
   // platform that is likely to have decoders for the format.
   static bool IsSupportedType(const MediaContainerType& aContainerType);
 };
 
 } // namespace mozilla
 
diff --git a/dom/media/webm/WebMDecoder.cpp b/dom/media/webm/WebMDecoder.cpp
--- a/dom/media/webm/WebMDecoder.cpp
+++ b/dom/media/webm/WebMDecoder.cpp
@@ -4,34 +4,23 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/Preferences.h"
 #ifdef MOZ_AV1
 #include "AOMDecoder.h"
 #endif
 #include "MediaContainerType.h"
-#include "MediaDecoderStateMachine.h"
+#include "MediaFormatReader.h"
 #include "WebMDemuxer.h"
 #include "WebMDecoder.h"
 #include "VideoUtils.h"
 
 namespace mozilla {
 
-MediaDecoderStateMachine* WebMDecoder::CreateStateMachine()
-{
-  MediaFormatReaderInit init;
-  init.mVideoFrameContainer = GetVideoFrameContainer();
-  init.mKnowsCompositor = GetCompositor();
-  init.mCrashHelper = GetOwner()->CreateGMPCrashHelper();
-  init.mFrameStats = mFrameStats;
-  mReader = new MediaFormatReader(init, new WebMDemuxer(mResource));
-  return new MediaDecoderStateMachine(this, mReader);
-}
-
 /* static */
 bool
 WebMDecoder::IsSupportedType(const MediaContainerType& aContainerType)
 {
   if (!Preferences::GetBool("media.webm.enabled")) {
     return false;
   }
 
diff --git a/dom/media/webm/WebMDecoder.h b/dom/media/webm/WebMDecoder.h
--- a/dom/media/webm/WebMDecoder.h
+++ b/dom/media/webm/WebMDecoder.h
@@ -2,17 +2,16 @@
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #if !defined(WebMDecoder_h_)
 #define WebMDecoder_h_
 
 #include "ChannelMediaDecoder.h"
-#include "MediaFormatReader.h"
 
 namespace mozilla {
 
 class MediaContainerType;
 
 class WebMDecoder : public ChannelMediaDecoder
 {
 public:
@@ -22,17 +21,16 @@ public:
   }
   ChannelMediaDecoder* Clone(MediaDecoderInit& aInit) override
   {
     if (!IsWebMEnabled()) {
       return nullptr;
     }
     return new WebMDecoder(aInit);
   }
-  MediaDecoderStateMachine* CreateStateMachine() override;
 
   // Returns true if aContainerType is a WebM type that we think we can render
   // with an enabled platform decoder backend.
   // If provided, codecs are checked for support.
   static bool IsSupportedType(const MediaContainerType& aContainerType);
 
   void GetMozDebugReaderData(nsACString& aString) override;
 };
