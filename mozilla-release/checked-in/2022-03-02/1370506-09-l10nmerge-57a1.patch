# HG changeset patch
# User Axel Hecht <axel@pike.org>
# Date 1497611668 -7200
# Node ID 0fdb6a37c65caaffbcf04834589c13bda0b96cd4
# Parent  caf47e2dc4ecdcc1de0f16f2f5f7ec580d298f8a
bug 1370506, update the docs, r=glandium

Mostly removals, but also adding docs on how to create a
multilingual package right now.

I think I'd like to take another pass at those docs in a follow-up.

MozReview-Commit-ID: Dkw4MJ5DLyb

diff --git a/build/docs/locales.rst b/build/docs/locales.rst
--- a/build/docs/locales.rst
+++ b/build/docs/locales.rst
@@ -9,92 +9,71 @@ Single-locale language repacks
 
 To save on build time, the build system and automation collaborate to allow
 downloading a packaged en-US Firefox, performing some locale-specific
 post-processing, and re-packaging a locale-specific Firefox.  Such artifacts
 are termed "single-locale language repacks".  There is another concept of a
 "multi-locale language build", which is more like a regular build and less
 like a re-packaging post-processing step.
 
-There are scripts in-tree in mozharness to orchestrate these re-packaging
-steps for `Desktop
-<https://dxr.mozilla.org/mozilla-central/source/testing/mozharness/scripts/desktop_l10n.py>`_
-and `Android
-<https://dxr.mozilla.org/mozilla-central/source/testing/mozharness/scripts/mobile_l10n.py>`_
-but they rely heavily on buildbot information so they are almost impossible to
-run locally.
-
-The following instructions are extracted from the `Android script with hg hash
-494289c7
-<https://dxr.mozilla.org/mozilla-central/rev/494289c72ba3997183e7b5beaca3e0447ecaf96d/testing/mozharness/scripts/mobile_l10n.py>`_,
-and may need to be updated and slightly modified for Desktop.
-
-Step by step instructions for Android
--------------------------------------
+Instructions for single-locale repacks for developers
+-----------------------------------------------------
 
 This assumes that ``$AB_CD`` is the locale you want to repack with; I tested
 with "ar" and "en-GB".
 
-.. warning:: l10n repacks do not work with artifact builds.  Repackaging
-   compiles no code so supporting ``--disable-compile-environment`` would not
-   save much, if any, time.
-
 #. You must have a built and packaged object directory, or a pre-built
    ``en-US`` package.
 
    .. code-block:: shell
 
       ./mach build
       ./mach package
 
-#. Clone ``l10n-central/$AB_CD`` so that it is a sibling to your
-   ``mozilla-central`` directory.
+#. Repackage using the locale-specific changes.
 
    .. code-block:: shell
 
-      $ ls -al
-      mozilla-central
-      ...
-      $ mkdir -p l10n-central
-      $ hg clone https://hg.mozilla.org/l10n-central/$AB_CD l10n-central/$AB_CD
-      $ ls -al
-      mozilla-central
-      l10n-central/$AB_CD
-      ...
+      ./mach build installers-$AB_CD
 
-#. Copy your ``mozconfig`` to ``mozconfig.l10n`` and add the following.
-
-   ::
-
-      ac_add_options --with-l10n-base=../../l10n-central
-      ac_add_options --disable-tests
-      mk_add_options MOZ_OBJDIR=./objdir-l10n
-
-#. Configure and prepare the l10n object directory.
+You should find a re-packaged build at ``OBJDIR/dist/``, and a
+runnable binary in ``OBJDIR/dist/l10n-stage/``.
+The ``installers`` target runs quite a few things for you, including getting
+the repository for the requested locale from
+https://hg.mozilla.org/l10n-central/. It will clone them into
+``~/.mozbuild/l10n-central``. If you have an existing repository there, you
+may want to occasionally update that via ``hg pull -u``. If you prefer
+to have the l10n repositories at a different location on your disk, you
+can point to the directory via
 
    .. code-block:: shell
 
-      MOZCONFIG=mozconfig.l10n ./mach configure
-      MOZCONFIG=mozconfig.l10n ./mach build -C config export
-      MOZCONFIG=mozconfig.l10n ./mach build buildid.h
+      ac_add_options --with-l10n-base=/make/this/a/absolute/path
 
-#. Copy your built package and unpack it into the l10n object directory.
+Instructions for multi-locale builds
+------------------------------------
+
+If you want to create a single build with mutliple locales, you will do
+
+#. Create a build and package
 
    .. code-block:: shell
 
-      cp $OBJDIR/dist/fennec-*en-US*.apk ./objdir-l10n/dist
-      MOZCONFIG=mozconfig.l10n ./mach build -C mobile/android/locales unpack
+      ./mach build
+      ./mach package
 
-#. Run the ``compare-locales`` script to write locale-specific changes into
-   ``objdir-l10n/merged``.
+#. For each locale you want to include in the build:
 
    .. code-block:: shell
 
-      MOZCONFIG=mozconfig.l10n ./mach compare-locales --merge-dir objdir-l10n/merged $AB_CD
+      export MOZ_CHROME_MULTILOCALE="de it zh-TW"
+      for AB_CD in $MOZ_CHROME_MULTILOCALE; do
+         ./mach build chrome-$AB_CD
+      done
 
-#. Finally, repackage using the locale-specific changes.
+#. Create the multilingual package:
 
    .. code-block:: shell
 
-      MOZCONFIG=mozconfig.l10n LOCALE_MERGEDIR=`realpath objdir-l10n/merged` ./mach build -C mobile/android/locales installers-$AB_CD
+      AB_CD=multi ./mach package
 
-   (Note the absolute path for ``LOCALE_MERGEDIR``.)  You should find a
-   re-packaged build at ``objdir-l10n/dist/fennec-*$AB_CD*.apk``.
+This `currently <https://bugzilla.mozilla.org/show_bug.cgi?id=1362496>`_ only
+works for Firefox for Android.
diff --git a/tools/compare-locales/docs/index.rst b/tools/compare-locales/docs/index.rst
--- a/tools/compare-locales/docs/index.rst
+++ b/tools/compare-locales/docs/index.rst
@@ -143,23 +143,23 @@ l10n-merge
 ----------
 
 Gecko doesn't support fallback from a localization to ``en-US`` at runtime.
 Thus, the build needs to ensure that the localization as it's built into
 the package has all required strings, and that the strings don't contain
 errors. To ensure that, we're *merging* the localization and ``en-US``
 at build time, nick-named :term:`l10n-merge`.
 
-The process is usually triggered via
+The process can be manually triggered via
 
 .. code-block:: bash
 
-    $obj-dir/browser/locales> make merge-de LOCALE_MERGEDIR=$PWD/merge-de
+    $> ./mach build merge-de
 
-It creates another directory in the object dir, :file:`merge-ab-CD`, in
+It creates another directory in the object dir, :file:`merge-dir/ab-CD`, in
 which the modified files are stored. The actual repackaging process looks for
 the localized files in the merge dir first, then the localized file, and then
 in ``en-US``. Thus, for the ``de`` localization of
 :file:`browser/locales/en-US/chrome/browser/browser.dtd`, it checks
 
 1. :file:`$objdir/browser/locales/merge-de/browser/chrome/browser/browser.dtd`
 2. :file:`$(LOCALE_BASEDIR)/de/browser/chrome/browser/browser.dtd`
 3. :file:`browser/locales/en-US/chrome/browser/browser.dtd`
@@ -192,17 +192,14 @@ The Android-specific checks are enabled 
 :file:`mobile/android/base/locales/en-US/`.
 
 Localizations
 -------------
 
 Now that we talked in-depth about how to expose content to localizers,
 where are the localizations?
 
-We host a mercurial repository per locale and per branch. Most of our
-localizations only work starting with aurora, so the bulk of the localizations
-is found on https://hg.mozilla.org/releases/l10n/mozilla-aurora/. We have
-several localizations continuously working with mozilla-central, those
-repositories are on https://hg.mozilla.org/l10n-central/.
+We host a mercurial repository per locale and per branch. All of our
+localizations can be found on https://hg.mozilla.org/l10n-central/.
 
 You can search inside our localized files on
 `Transvision <https://transvision.mozfr.org/>`_ and
-https://dxr.mozilla.org/l10n-mozilla-aurora/source/.
+https://dxr.mozilla.org/l10n-central/source/.
