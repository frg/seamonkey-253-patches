# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1501603964 -3600
# Node ID 9f529070cc663f9b326708aaea8c3527a537f0f7
# Parent  9d179411bdc09c738924a207d7ffd3035ebfc4b3
Bug 1388939, part 7 - Give clear names to the nsFrameManager methods for unregistering undisplayed style contexts. r=dholbert

MozReview-Commit-ID: EFtKqKBOPQH

diff --git a/layout/base/nsCSSFrameConstructor.cpp b/layout/base/nsCSSFrameConstructor.cpp
--- a/layout/base/nsCSSFrameConstructor.cpp
+++ b/layout/base/nsCSSFrameConstructor.cpp
@@ -8626,17 +8626,17 @@ nsCSSFrameConstructor::ContentRemoved(ns
     }
   }
 #endif
 
   nsIFrame* childFrame = aChild->GetPrimaryFrame();
   if (!childFrame || childFrame->GetContent() != aChild) {
     // XXXbz the GetContent() != aChild check is needed due to bug 135040.
     // Remove it once that's fixed.
-    ClearUndisplayedContentIn(aChild, aContainer);
+    UnregisterDisplayNoneStyleFor(aChild, aContainer);
   }
   MOZ_ASSERT(!childFrame || !GetDisplayContentsStyleFor(aChild),
              "display:contents nodes shouldn't have a frame");
   if (!childFrame && GetDisplayContentsStyleFor(aChild)) {
     nsIContent* ancestor = aContainer;
     MOZ_ASSERT(ancestor, "display: contents on the root?");
     while (!ancestor->GetPrimaryFrame()) {
       // FIXME(emilio): Should this use the flattened tree parent instead?
@@ -8662,17 +8662,17 @@ nsCSSFrameConstructor::ContentRemoved(ns
         LAYOUT_PHASE_TEMP_EXIT();
         ContentRemoved(aChild, c, nullptr, aFlags, aDidReconstruct, aDestroyedFramesFor);
         LAYOUT_PHASE_TEMP_REENTER();
         if (aFlags != REMOVE_DESTROY_FRAMES && *aDidReconstruct) {
           return;
         }
       }
     }
-    ClearDisplayContentsIn(aChild, aContainer);
+    UnregisterDisplayContentsStyleFor(aChild, aContainer);
   }
 
 #ifdef MOZ_XUL
   if (NotifyListBoxBody(presContext, aContainer, aChild, aOldNextSibling,
                         childFrame, CONTENT_REMOVED)) {
     if (aFlags == REMOVE_DESTROY_FRAMES) {
       CaptureStateForFramesOf(aChild, mTempFrameTreeState);
     }
@@ -8821,17 +8821,17 @@ nsCSSFrameConstructor::ContentRemoved(ns
       // simpler.
       RemoveLetterFrames(mPresShell, containingBlock);
 
       // Recover childFrame and parentFrame
       childFrame = aChild->GetPrimaryFrame();
       if (!childFrame || childFrame->GetContent() != aChild) {
         // XXXbz the GetContent() != aChild check is needed due to bug 135040.
         // Remove it once that's fixed.
-        ClearUndisplayedContentIn(aChild, aContainer);
+        UnregisterDisplayNoneStyleFor(aChild, aContainer);
         return;
       }
       parentFrame = childFrame->GetParent();
       parentType = parentFrame->Type();
 
 #ifdef NOISY_FIRST_LETTER
       printf("  ==> revised parentFrame=");
       nsFrame::ListTag(stdout, parentFrame);
diff --git a/layout/base/nsFrameManager.cpp b/layout/base/nsFrameManager.cpp
--- a/layout/base/nsFrameManager.cpp
+++ b/layout/base/nsFrameManager.cpp
@@ -238,18 +238,18 @@ nsFrameManager::ChangeStyleContextInMap(
       return;
     }
   }
 
   MOZ_CRASH("couldn't find the entry to change");
 }
 
 void
-nsFrameManager::ClearUndisplayedContentIn(nsIContent* aContent,
-                                          nsIContent* aParentContent)
+nsFrameManager::UnregisterDisplayNoneStyleFor(nsIContent* aContent,
+                                              nsIContent* aParentContent)
 {
 #ifdef DEBUG_UNDISPLAYED_MAP
   static int i = 0;
   printf("ClearUndisplayedContent(%d): content=%p parent=%p --> ", i++, (void *)aContent, (void*)aParentContent);
 #endif
 
   if (!mDisplayNoneMap) {
     return;
@@ -300,18 +300,18 @@ nsFrameManager::ClearAllMapsFor(nsIConte
   // Need to look at aParentContent's content list due to XBL insertions.
   // Nodes in aParentContent's content list do not have aParentContent as a
   // parent, but are treated as children of aParentContent. We iterate over
   // the flattened content list and just ignore any nodes we don't care about.
   FlattenedChildIterator iter(aParentContent);
   for (nsIContent* child = iter.GetNextChild(); child; child = iter.GetNextChild()) {
     auto parent = child->GetParent();
     if (parent != aParentContent) {
-      ClearUndisplayedContentIn(child, parent);
-      ClearDisplayContentsIn(child, parent);
+      UnregisterDisplayNoneStyleFor(child, parent);
+      UnregisterDisplayContentsStyleFor(child, parent);
     }
   }
 }
 
 //----------------------------------------------------------------------
 
 void
 nsFrameManager::RegisterDisplayContentsStyleFor(nsIContent* aContent,
@@ -325,18 +325,18 @@ nsFrameManager::RegisterDisplayContentsS
 
 UndisplayedNode*
 nsFrameManager::GetAllRegisteredDisplayContentsStylesIn(nsIContent* aParentContent)
 {
   return GetAllUndisplayedNodesInMapFor(mDisplayContentsMap, aParentContent);
 }
 
 void
-nsFrameManager::ClearDisplayContentsIn(nsIContent* aContent,
-                                       nsIContent* aParentContent)
+nsFrameManager::UnregisterDisplayContentsStyleFor(nsIContent* aContent,
+                                                  nsIContent* aParentContent)
 {
 #ifdef DEBUG_DISPLAY_CONTENTS_MAP
   static int i = 0;
   printf("ClearDisplayContents(%d): content=%p parent=%p --> ", i++, (void *)aContent, (void*)aParentContent);
 #endif
 
   if (!mDisplayContentsMap) {
     return;
diff --git a/layout/base/nsFrameManager.h b/layout/base/nsFrameManager.h
--- a/layout/base/nsFrameManager.h
+++ b/layout/base/nsFrameManager.h
@@ -165,26 +165,26 @@ public:
     return GetUndisplayedNodeInMapFor(mDisplayContentsMap, aContent);
   }
 
   /**
    * Unregister the style context for the display:none content, aContent,
    * if any.  If found, then this method also unregisters the style contexts
    * for any display:contents and display:none descendants of aContent.
    */
-  void ClearUndisplayedContentIn(nsIContent* aContent,
-                                 nsIContent* aParentContent);
+  void UnregisterDisplayNoneStyleFor(nsIContent* aContent,
+                                     nsIContent* aParentContent);
 
   /**
    * Unregister the style context for the display:contents content, aContent,
    * if any.  If found, then this method also unregisters the style contexts
    * for any display:contents and display:none descendants of aContent.
    */
-  void ClearDisplayContentsIn(nsIContent* aContent,
-                              nsIContent* aParentContent);
+  void UnregisterDisplayContentsStyleFor(nsIContent* aContent,
+                                         nsIContent* aParentContent);
 
 
   // Functions for manipulating the frame model
   void AppendFrames(nsContainerFrame* aParentFrame,
                     ChildListID aListID,
                     nsFrameList& aFrameList);
 
   void InsertFrames(nsContainerFrame* aParentFrame,
