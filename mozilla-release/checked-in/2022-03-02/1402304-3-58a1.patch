# HG changeset patch
# User Nicolas Silva <nsilva@mozilla.com>
# Date 1506088844 14400
#      Fri Sep 22 10:00:44 2017 -0400
# Node ID 5e39bc336365c6d20fffe1c7e25d0e715c898dff
# Parent  12c5ed818974c71e54f524fb0418e3fb593d7767
Bug 1402304 - Fix bindings to go with WR cset 4dbc5d8e. r=kats

MozReview-Commit-ID: CxlZhhTAxwk

diff --git a/gfx/2d/2D.h b/gfx/2d/2D.h
--- a/gfx/2d/2D.h
+++ b/gfx/2d/2D.h
@@ -69,17 +69,17 @@ struct gfxFontStyle;
 struct CGContext;
 typedef struct CGContext *CGContextRef;
 
 namespace mozilla {
 
 class Mutex;
 
 namespace wr {
-struct FontInstanceOptions;
+struct WrFontInstanceOptions;
 struct FontInstancePlatformOptions;
 }
 
 namespace gfx {
 class UnscaledFont;
 class ScaledFont;
 }
 
@@ -830,17 +830,17 @@ public:
   virtual void GetGlyphDesignMetrics(const uint16_t* aGlyphIndices, uint32_t aNumGlyphs, GlyphMetrics* aGlyphMetrics) = 0;
 
   typedef void (*FontInstanceDataOutput)(const uint8_t* aData, uint32_t aLength,
                                          const FontVariation* aVariations, uint32_t aNumVariations,
                                          void* aBaton);
 
   virtual bool GetFontInstanceData(FontInstanceDataOutput, void *) { return false; }
 
-  virtual bool GetWRFontInstanceOptions(Maybe<wr::FontInstanceOptions>* aOutOptions,
+  virtual bool GetWRFontInstanceOptions(Maybe<wr::WrFontInstanceOptions>* aOutOptions,
                                         Maybe<wr::FontInstancePlatformOptions>* aOutPlatformOptions,
                                         std::vector<FontVariation>* aOutVariations)
   {
     return false;
   }
 
   virtual bool CanSerialize() { return false; }
 
diff --git a/gfx/2d/ScaledFontMac.cpp b/gfx/2d/ScaledFontMac.cpp
--- a/gfx/2d/ScaledFontMac.cpp
+++ b/gfx/2d/ScaledFontMac.cpp
@@ -365,17 +365,17 @@ ScaledFontMac::GetFontInstanceData(FontI
     if (!GetVariationsForCTFont(mCTFont, &variations)) {
       return false;
     }
     aCb(nullptr, 0, variations.data(), variations.size(), aBaton);
     return true;
 }
 
 bool
-ScaledFontMac::GetWRFontInstanceOptions(Maybe<wr::FontInstanceOptions>* aOutOptions,
+ScaledFontMac::GetWRFontInstanceOptions(Maybe<wr::WrFontInstanceOptions>* aOutOptions,
                                         Maybe<wr::FontInstancePlatformOptions>* aOutPlatformOptions,
                                         std::vector<FontVariation>* aOutVariations)
 {
     if (!GetVariationsForCTFont(mCTFont, aOutVariations)) {
       return false;
     }
     return true;
 }
diff --git a/gfx/2d/ScaledFontMac.h b/gfx/2d/ScaledFontMac.h
--- a/gfx/2d/ScaledFontMac.h
+++ b/gfx/2d/ScaledFontMac.h
@@ -48,17 +48,17 @@ public:
   FontType GetType() const override { return FontType::MAC; }
 #ifdef USE_SKIA
   SkTypeface* GetSkTypeface() override;
 #endif
   already_AddRefed<Path> GetPathForGlyphs(const GlyphBuffer &aBuffer, const DrawTarget *aTarget) override;
 
   bool GetFontInstanceData(FontInstanceDataOutput aCb, void* aBaton) override;
 
-  bool GetWRFontInstanceOptions(Maybe<wr::FontInstanceOptions>* aOutOptions,
+  bool GetWRFontInstanceOptions(Maybe<wr::WrFontInstanceOptions>* aOutOptions,
                                 Maybe<wr::FontInstancePlatformOptions>* aOutPlatformOptions,
                                 std::vector<FontVariation>* aOutVariations) override;
 
   bool CanSerialize() override { return true; }
 
 #ifdef USE_CAIRO_SCALED_FONT
   cairo_font_face_t* GetCairoFontFace() override;
 #endif
diff --git a/gfx/webrender_bindings/WebRenderTypes.h b/gfx/webrender_bindings/WebRenderTypes.h
--- a/gfx/webrender_bindings/WebRenderTypes.h
+++ b/gfx/webrender_bindings/WebRenderTypes.h
@@ -24,16 +24,17 @@
 namespace mozilla {
 namespace wr {
 
 typedef wr::WrWindowId WindowId;
 typedef wr::WrPipelineId PipelineId;
 typedef wr::WrImageKey ImageKey;
 typedef wr::WrFontKey FontKey;
 typedef wr::WrFontInstanceKey FontInstanceKey;
+typedef wr::WrFontInstanceOptions FontInstanceOptions;
 typedef wr::WrEpoch Epoch;
 typedef wr::WrExternalImageId ExternalImageId;
 typedef wr::WrDebugFlags DebugFlags;
 
 typedef mozilla::Maybe<mozilla::wr::WrImageMask> MaybeImageMask;
 typedef Maybe<ExternalImageId> MaybeExternalImageId;
 
 typedef Maybe<FontInstanceOptions> MaybeFontInstanceOptions;
diff --git a/gfx/webrender_bindings/src/bindings.rs b/gfx/webrender_bindings/src/bindings.rs
--- a/gfx/webrender_bindings/src/bindings.rs
+++ b/gfx/webrender_bindings/src/bindings.rs
@@ -171,16 +171,32 @@ impl MutByteSlice {
 
     pub fn as_mut_slice(&mut self) -> &mut [u8] {
         make_slice_mut(self.buffer, self.len)
     }
 }
 
 #[repr(C)]
 #[derive(Debug, Clone, Copy)]
+pub struct WrFontInstanceOptions {
+    pub render_mode: FontRenderMode,
+    pub synthetic_italics: bool,
+}
+
+impl Into<FontInstanceOptions> for WrFontInstanceOptions {
+    fn into(self) -> FontInstanceOptions {
+        FontInstanceOptions {
+            render_mode: Some(self.render_mode),
+            synthetic_italics: self.synthetic_italics,
+        }
+    }
+}
+
+#[repr(C)]
+#[derive(Debug, Clone, Copy)]
 pub struct WrImageMask {
     image: WrImageKey,
     rect: LayoutRect,
     repeat: bool,
 }
 
 impl Into<ImageMask> for WrImageMask {
     fn into(self) -> ImageMask {
@@ -968,25 +984,28 @@ pub extern "C" fn wr_resource_updates_de
 }
 
 #[no_mangle]
 pub extern "C" fn wr_resource_updates_add_font_instance(
     resources: &mut ResourceUpdates,
     key: WrFontInstanceKey,
     font_key: WrFontKey,
     glyph_size: f32,
-    options: *const FontInstanceOptions,
+    options: *const WrFontInstanceOptions,
     platform_options: *const FontInstancePlatformOptions,
     variations: &mut WrVecU8,
 ) {
+    let instance_options: Option<FontInstanceOptions> = unsafe {
+        options.as_ref().map(|opts|{ (*opts).into() })
+    };
     resources.add_font_instance(
         key,
         font_key,
         Au::from_f32_px(glyph_size),
-        unsafe { options.as_ref().cloned() },
+        instance_options,
         unsafe { platform_options.as_ref().cloned() },
         variations.convert_into_vec::<FontVariation>(),
     );
 }
 
 #[no_mangle]
 pub extern "C" fn wr_resource_updates_delete_font_instance(
     resources: &mut ResourceUpdates,
diff --git a/gfx/webrender_bindings/webrender_ffi_generated.h b/gfx/webrender_bindings/webrender_ffi_generated.h
--- a/gfx/webrender_bindings/webrender_ffi_generated.h
+++ b/gfx/webrender_bindings/webrender_ffi_generated.h
@@ -724,21 +724,23 @@ struct FontKey {
   bool operator==(const FontKey& aOther) const {
     return mNamespace == aOther.mNamespace &&
            mHandle == aOther.mHandle;
   }
 };
 
 typedef FontKey WrFontKey;
 
-struct FontInstanceOptions {
+struct WrFontInstanceOptions {
   FontRenderMode render_mode;
+  bool synthetic_italics;
 
-  bool operator==(const FontInstanceOptions& aOther) const {
-    return render_mode == aOther.render_mode;
+  bool operator==(const WrFontInstanceOptions& aOther) const {
+    return render_mode == aOther.render_mode &&
+           synthetic_italics == aOther.synthetic_italics;
   }
 };
 
 struct FontInstancePlatformOptions {
   bool use_embedded_bitmap;
   bool force_gdi_rendering;
 
   bool operator==(const FontInstancePlatformOptions& aOther) const {
@@ -1216,17 +1218,17 @@ void wr_resource_updates_add_external_im
                                             uint8_t aChannelIndex)
 WR_FUNC;
 
 WR_INLINE
 void wr_resource_updates_add_font_instance(ResourceUpdates *aResources,
                                            WrFontInstanceKey aKey,
                                            WrFontKey aFontKey,
                                            float aGlyphSize,
-                                           const FontInstanceOptions *aOptions,
+                                           const WrFontInstanceOptions *aOptions,
                                            const FontInstancePlatformOptions *aPlatformOptions,
                                            WrVecU8 *aVariations)
 WR_FUNC;
 
 WR_INLINE
 void wr_resource_updates_add_image(ResourceUpdates *aResources,
                                    WrImageKey aImageKey,
                                    const WrImageDescriptor *aDescriptor,
