# HG changeset patch
# User Tom Tromey <tom@tromey.com>
# Date 1502308043 21600
#      Wed Aug 09 13:47:23 2017 -0600
# Node ID 258bede4a3791fc92dccd2cbd2799eee91b2c48a
# Parent  08f41e958bf26597164025053b9c203c2ed07e2f
Bug 1388855 - Simplify source map URL extraction in stylesheet actor; r=gl

MozReview-Commit-ID: 3WeNugNx7M

diff --git a/devtools/server/actors/stylesheets.js b/devtools/server/actors/stylesheets.js
--- a/devtools/server/actors/stylesheets.js
+++ b/devtools/server/actors/stylesheets.js
@@ -564,51 +564,49 @@ var StyleSheetActor = protocol.ActorClas
    * Fetch the source map for this stylesheet.
    *
    * @return {Promise}
    *         A promise that resolves with a SourceMapConsumer, or null.
    */
   _fetchSourceMap: function () {
     let deferred = defer();
 
-    this._getText().then(sheetContent => {
-      let url = this._extractSourceMapUrl(sheetContent);
-      if (!url) {
-        // no source map for this stylesheet
-        deferred.resolve(null);
-        return;
-      }
+    let url = this.rawSheet.sourceMapURL;
+    if (!url) {
+      // no source map for this stylesheet
+      deferred.resolve(null);
+      return deferred.promise;
+    }
 
-      url = normalize(url, this.safeHref);
-      let options = {
-        loadFromCache: false,
-        policy: Ci.nsIContentPolicy.TYPE_INTERNAL_STYLESHEET,
-        window: this.window
-      };
+    url = normalize(url, this.safeHref);
+    let options = {
+      loadFromCache: false,
+      policy: Ci.nsIContentPolicy.TYPE_INTERNAL_STYLESHEET,
+      window: this.window
+    };
 
-      let map = fetch(url, options).then(({content}) => {
-        // Fetching the source map might have failed with a 404 or other. When
-        // this happens, SourceMapConsumer may fail with a JSON.parse error.
-        let consumer;
-        try {
-          consumer = new SourceMapConsumer(content);
-        } catch (e) {
-          deferred.reject(new Error(
-            `Source map at ${url} not found or invalid`));
-          return null;
-        }
-        this._setSourceMapRoot(consumer, url, this.safeHref);
-        this._sourceMap = promise.resolve(consumer);
+    let map = fetch(url, options).then(({content}) => {
+      // Fetching the source map might have failed with a 404 or other. When
+      // this happens, SourceMapConsumer may fail with a JSON.parse error.
+      let consumer;
+      try {
+        consumer = new SourceMapConsumer(content);
+      } catch (e) {
+        deferred.reject(new Error(
+          `Source map at ${url} not found or invalid`));
+        return null;
+      }
+      this._setSourceMapRoot(consumer, url, this.safeHref);
+      this._sourceMap = promise.resolve(consumer);
 
-        deferred.resolve(consumer);
-        return consumer;
-      }, deferred.reject);
+      deferred.resolve(consumer);
+      return consumer;
+    }, deferred.reject);
 
-      this._sourceMap = map;
-    }, deferred.reject);
+    this._sourceMap = map;
 
     return deferred.promise;
   },
 
   /**
    * Clear and unmanage the original source actors for this stylesheet.
    */
   _clearOriginalSources: function () {
@@ -630,36 +628,16 @@ var StyleSheetActor = protocol.ActorClas
         ? scriptURL
         : absSourceMapURL);
     sourceMap.sourceRoot = sourceMap.sourceRoot
       ? normalize(sourceMap.sourceRoot, base)
       : base;
   },
 
   /**
-   * Get the source map url specified in the text of a stylesheet.
-   *
-   * @param  {string} content
-   *         The text of the style sheet.
-   * @return {string}
-   *         Url of source map.
-   */
-  _extractSourceMapUrl: function (content) {
-    // If a SourceMap response header was saved on the style sheet, use it.
-    if (this.rawSheet.sourceMapURL) {
-      return this.rawSheet.sourceMapURL;
-    }
-    let matches = /sourceMappingURL\=([^\s\*]*)/.exec(content);
-    if (matches) {
-      return matches[1];
-    }
-    return null;
-  },
-
-  /**
    * Protocol method that gets the location in the original source of a
    * line, column pair in this stylesheet, if its source mapped, otherwise
    * a promise of the same location.
    */
   getOriginalLocation: function (line, column) {
     return this.getSourceMap().then((sourceMap) => {
       if (sourceMap) {
         return sourceMap.originalPositionFor({ line: line, column: column });
