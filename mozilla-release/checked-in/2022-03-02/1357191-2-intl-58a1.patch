# HG changeset patch
# User Jim Chen <nchen@mozilla.com>
# Date 1507825214 14400
# Node ID cc2f40bc1b130431cb6ae4b575fad8a9db5d5873
# Parent  9168115ef69720230c739955b0d3bc378cf717d1
Bug 1357191 - 2. Fix form tests for new mobile date/time controls; r=jessica

Fix some test failures on mobile for the new date/time controls,

* Because mobile does not use the built-in reset button, tests
  involving the reset button are skipped.

* The mobile-only date/time tests in test_input_typing_sanitization.html
  do not apply to the new date/time controls, so they are removed to make
  mobile match desktop.

* test_input_sanitization.html now takes a lot longer to run on Android
  debug and frequently times out, so it's disabled for debug Android builds.

* time-content-left-aligned.html may fail because of different drawing
  behavior on Android, so the test is made fuzzy on Android.

MozReview-Commit-ID: FTzwHTJgVJe

diff --git a/dom/html/test/forms/mochitest.ini b/dom/html/test/forms/mochitest.ini
--- a/dom/html/test/forms/mochitest.ini
+++ b/dom/html/test/forms/mochitest.ini
@@ -28,29 +28,22 @@ skip-if = os == "android" # up/down arro
 [test_input_autocomplete.html]
 [test_input_color_input_change_events.html]
 [test_input_color_picker_initial.html]
 [test_input_color_picker_popup.html]
 skip-if = android_version == '18' # Android, bug 1147974
 [test_input_color_picker_update.html]
 skip-if = android_version == '18' # Android, bug 1147974
 [test_input_date_bad_input.html]
-skip-if = os == "android"
 [test_input_date_key_events.html]
-skip-if = os == "android"
 [test_input_datetime_input_change_events.html]
-skip-if = os == "android"
 [test_input_datetime_focus_blur.html]
-skip-if = os == "android"
 [test_input_datetime_focus_blur_events.html]
-skip-if = os == "android"
 [test_input_datetime_focus_state.html]
-skip-if = os == "android"
 [test_input_datetime_tabindex.html]
-skip-if = os == "android"
 [test_input_defaultValue.html]
 [test_input_email.html]
 [test_input_event.html]
 skip-if = android_version == '18' # bug 1147974
 [test_input_file_picker.html]
 [test_input_list_attribute.html]
 [test_input_number_l10n.html]
 # We don't build ICU for Firefox for Android:
@@ -66,21 +59,20 @@ skip-if = os == "android"
 skip-if = os == "android"
 [test_input_number_focus.html]
 [test_input_range_attr_order.html]
 [test_input_range_key_events.html]
 [test_input_range_mouse_and_touch_events.html]
 [test_input_range_mozinputrangeignorepreventdefault.html]
 [test_input_range_rounding.html]
 [test_input_sanitization.html]
+skip-if = os == 'android' && debug # Extremely slow on debug android
 [test_input_textarea_set_value_no_scroll.html]
 [test_input_time_key_events.html]
-skip-if = os == "android"
 [test_input_time_sec_millisec_field.html]
-skip-if = os == "android"
 [test_input_types_pref.html]
 [test_input_typing_sanitization.html]
 [test_input_untrusted_key_events.html]
 [test_input_url.html]
 [test_interactive_content_in_label.html]
 [test_label_control_attribute.html]
 [test_label_input_controls.html]
 [test_max_attribute.html]
diff --git a/dom/html/test/forms/test_input_date_bad_input.html b/dom/html/test/forms/test_input_date_bad_input.html
--- a/dom/html/test/forms/test_input_date_bad_input.html
+++ b/dom/html/test/forms/test_input_date_bad_input.html
@@ -32,16 +32,17 @@ SimpleTest.waitForExplicitFinish();
 SimpleTest.waitForFocus(function() {
   SpecialPowers.pushPrefEnv({"set":[["snav.enabled", false]]}, function() {
     test();
     SimpleTest.finish();
   });
 });
 
 const DATE_BAD_INPUT_MSG = "Please enter a valid date.";
+const isDesktop = !/Mobile|Tablet/.test(navigator.userAgent);
 
 function checkValidity(aElement, aIsBadInput) {
   is(aElement.validity.valid, aIsBadInput ? false : true,
      "validity.valid should be " + (aIsBadInput ? "false" : "true"));
   is(aElement.validity.badInput, aIsBadInput ? true : false,
      "validity.badInput should be " + (aIsBadInput ? "true" : "false"));
   is(aElement.validationMessage, aIsBadInput ? DATE_BAD_INPUT_MSG : "",
      "validationMessage should be: " + (aIsBadInput ? DATE_BAD_INPUT_MSG : ""));
@@ -88,17 +89,20 @@ function test() {
   elem.blur();
   checkValidity(elem, false);
 
   elem.focus();
   sendKeys("02292017");
   elem.blur();
   checkValidity(elem, true);
 
-  // Clearing all fields should clear bad input validity state as well.
-  synthesizeMouse(input, resetButton_X, resetButton_Y, {});
-  checkValidity(elem, false);
+  // Reset button is desktop only.
+  if (isDesktop) {
+    // Clearing all fields should clear bad input validity state as well.
+    synthesizeMouse(input, resetButton_X, resetButton_Y, {});
+    checkValidity(elem, false);
+  }
 }
 
 </script>
 </pre>
 </body>
 </html>
diff --git a/dom/html/test/forms/test_input_datetime_input_change_events.html b/dom/html/test/forms/test_input_datetime_input_change_events.html
--- a/dom/html/test/forms/test_input_datetime_input_change_events.html
+++ b/dom/html/test/forms/test_input_datetime_input_change_events.html
@@ -21,16 +21,18 @@ https://bugzilla.mozilla.org/show_bug.cg
 <pre id="test">
 <script class="testbody" type="text/javascript">
 
 /**
  * Test for Bug 1370858.
  * Test that change and input events are (not) fired for date/time inputs.
  **/
 
+const isDesktop = !/Mobile|Tablet/.test(navigator.userAgent);
+
 var inputTypes = ["time", "date"];
 var changeEvents = [0, 0];
 var inputEvents = [0, 0];
 var values = ["10:30", "2017-06-08"];
 var expectedValues = [["09:30", "01:30"], ["2017-05-08", "2017-01-08"]];
 
 SimpleTest.waitForExplicitFinish();
 SimpleTest.waitForFocus(function() {
@@ -68,21 +70,24 @@ function test() {
     // Test that change and input events are fired when changing the value with
     // the keyboard.
     synthesizeKey("0", {});
     synthesizeKey("1", {});
     is(input.value, expectedValues[i][1], "Check that value was set correctly (2).");
     is(changeEvents[i], 2, "Change event should be dispatched (2).");
     is(inputEvents[i], 2, "Input event should be dispatched (2).");
 
-    // Test that change and input events are fired when clearing the value using
-    // the reset button.
-    synthesizeMouse(input, resetButton_X, resetButton_Y, {});
-    is(input.value, "", "Check that value was set correctly (3).");
-    is(changeEvents[i], 3, "Change event should be dispatched (3).");
-    is(inputEvents[i], 3, "Input event should be dispatched (3).");
+    // Reset button is desktop only.
+    if (isDesktop) {
+      // Test that change and input events are fired when clearing the value using
+      // the reset button.
+      synthesizeMouse(input, resetButton_X, resetButton_Y, {});
+      is(input.value, "", "Check that value was set correctly (3).");
+      is(changeEvents[i], 3, "Change event should be dispatched (3).");
+      is(inputEvents[i], 3, "Input event should be dispatched (3).");
+    }
   }
 }
 
 </script>
 </pre>
 </body>
 </html>
diff --git a/dom/html/test/forms/test_input_typing_sanitization.html b/dom/html/test/forms/test_input_typing_sanitization.html
--- a/dom/html/test/forms/test_input_typing_sanitization.html
+++ b/dom/html/test/forms/test_input_typing_sanitization.html
@@ -20,17 +20,16 @@ https://bugzilla.mozilla.org/show_bug.cg
 </div>
 <pre id="test">
 <script type="application/javascript">
 
 /*
  * This test checks that when a user types in some input types, it will not be
  * in a state where the value will be un-sanitized and usable (by a script).
  */
-const isDesktop = !/Mobile|Tablet/.test(navigator.userAgent);
 
 var input = document.getElementById('i');
 var form = document.getElementById('f');
 var submitFrame = document.getElementsByTagName('iframe')[0];
 var testData = [];
 var gCurrentTest = null;
 var gValidData = [];
 var gInvalidData = [];
@@ -133,57 +132,16 @@ function* runTest()
         "e",
         "e2",
         "1e0.1",
         "foo",
         "42,13", // comma can't be used as a decimal separator
       ]
     },
     {
-      mobileOnly: true,
-      type: 'date',
-      validData: [
-        '0001-01-01',
-        '2012-12-21',
-        '2013-01-28',
-        '100000-01-01',
-      ],
-      invalidData: [
-        '1-01-01',
-        'a',
-        '-',
-        '2012-01',
-        '2013-01-1',
-        '1011-23-21',
-        '1000-12-99',
-      ]
-    },
-    {
-      mobileOnly: true,
-      type: 'time',
-      validData: [
-        '00:00',
-        '09:09:00',
-        '08:23:23.1',
-        '21:43:56.12',
-        '23:12:45.100',
-      ],
-      invalidData: [
-        '00:',
-        '00:00:',
-        '25:00',
-        '-00:00',
-        '00:00:00.',
-        '00:60',
-        '10:58:99',
-        ':19:10',
-        '23:08:09.1012',
-      ]
-    },
-    {
       type: 'month',
       validData: [
         '0001-01',
         '2012-12',
         '100000-01',
       ],
       invalidData: [
         '1-01',
@@ -231,20 +189,16 @@ function* runTest()
         'datetime-local'
       ]
     },
   ];
 
   for (test of data) {
     gCurrentTest = test;
 
-    if (gCurrentTest.mobileOnly && isDesktop) {
-      continue;
-    }
-
     input.type = test.type;
     gValidData = test.validData;
     gInvalidData = test.invalidData;
 
     for (data of gValidData) {
       input.value = "";
       input.focus();
       sendString(data);
diff --git a/layout/reftests/forms/input/datetime/reftest.list b/layout/reftests/forms/input/datetime/reftest.list
--- a/layout/reftests/forms/input/datetime/reftest.list
+++ b/layout/reftests/forms/input/datetime/reftest.list
@@ -1,24 +1,21 @@
 default-preferences pref(dom.forms.datetime,true)
 
-# not valid on Android where type=time looks like type=text
-skip-if(Android) != time-simple-unthemed.html time-simple-unthemed-ref.html
-skip-if(Android) != time-large-font.html time-basic.html
-skip-if(Android) != time-width-height.html time-basic.html
-skip-if(Android) != time-border.html time-basic.html
-# only valid on Android where type=number looks the same as type=text
-skip-if(!Android) == time-simple-unthemed.html time-simple-unthemed-ref.html
+!= time-simple-unthemed.html time-simple-unthemed-ref.html
+!= time-large-font.html time-basic.html
+!= time-width-height.html time-basic.html
+!= time-border.html time-basic.html
 
 # type change
-skip-if(Android) == to-time-from-other-type-unthemed.html time-simple-unthemed.html
-skip-if(Android) == from-time-to-other-type-unthemed.html from-time-to-other-type-unthemed-ref.html
+== to-time-from-other-type-unthemed.html time-simple-unthemed.html
+== from-time-to-other-type-unthemed.html from-time-to-other-type-unthemed-ref.html
 
 # content should not overflow on small width/height
-skip-if(Android) == time-small-width.html time-small-width-ref.html
-skip-if(Android) == time-small-height.html time-small-height-ref.html
-skip-if(Android) == time-small-width-height.html time-small-width-height-ref.html
+== time-small-width.html time-small-width-ref.html
+== time-small-height.html time-small-height-ref.html
+== time-small-width-height.html time-small-width-height-ref.html
 
 # content (text) should be left aligned
-skip-if(Android) == time-content-left-aligned.html time-content-left-aligned-ref.html
+fuzzy-if(Android,4,8) == time-content-left-aligned.html time-content-left-aligned-ref.html
 
 # reset button should be right aligned
 skip-if(Android) == time-reset-button-right-aligned.html time-reset-button-right-aligned-ref.html # bug 1372062
