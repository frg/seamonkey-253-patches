# HG changeset patch
# User Nicolas B. Pierron <nicolas.b.pierron@mozilla.com>
# Date 1502454365 0
#      Fri Aug 11 12:26:05 2017 +0000
# Node ID ea704d8cd7798abea03a969734511e123257ca9a
# Parent  cf9d9fd4f0f62747f3cbbffc83f55946aa1cbeb0
Bug 1388014 part 1 - IonMonkey: Devirtualize MResumePoint::getOperand function calls. r=jandem

diff --git a/js/src/jit/LIR.cpp b/js/src/jit/LIR.cpp
--- a/js/src/jit/LIR.cpp
+++ b/js/src/jit/LIR.cpp
@@ -204,18 +204,20 @@ LRecoverInfo::New(MIRGenerator* gen, MRe
         return nullptr;
 
     JitSpew(JitSpew_IonSnapshots, "Generating LIR recover info %p from MIR (%p)",
             (void*)recoverInfo, (void*)mir);
 
     return recoverInfo;
 }
 
+// de-virtualise MResumePoint::getOperand calls.
+template <typename Node>
 bool
-LRecoverInfo::appendOperands(MNode* ins)
+LRecoverInfo::appendOperands(Node* ins)
 {
     for (size_t i = 0, end = ins->numOperands(); i < end; i++) {
         MDefinition* def = ins->getOperand(i);
 
         // As there is no cycle in the data-flow (without MPhi), checking for
         // isInWorkList implies that the definition is already in the
         // instruction vector, and not processed by a caller of the current
         // function.
diff --git a/js/src/jit/LIR.h b/js/src/jit/LIR.h
--- a/js/src/jit/LIR.h
+++ b/js/src/jit/LIR.h
@@ -1221,17 +1221,18 @@ class LRecoverInfo : public TempObject
     // Cached offset where this resume point is encoded.
     RecoverOffset recoverOffset_;
 
     explicit LRecoverInfo(TempAllocator& alloc);
     MOZ_MUST_USE bool init(MResumePoint* mir);
 
     // Fill the instruction vector such as all instructions needed for the
     // recovery are pushed before the current instruction.
-    MOZ_MUST_USE bool appendOperands(MNode* ins);
+    template <typename Node>
+    MOZ_MUST_USE bool appendOperands(Node* ins);
     MOZ_MUST_USE bool appendDefinition(MDefinition* def);
     MOZ_MUST_USE bool appendResumePoint(MResumePoint* rp);
   public:
     static LRecoverInfo* New(MIRGenerator* gen, MResumePoint* mir);
 
     // Resume point of the inner most function.
     MResumePoint* mir() const {
         return instructions_.back()->toResumePoint();
@@ -1255,47 +1256,60 @@ class LRecoverInfo : public TempObject
     }
 
     class OperandIter
     {
       private:
         MNode** it_;
         MNode** end_;
         size_t op_;
+        size_t opEnd_;
+        MResumePoint* rp_;
+        MNode* node_;
 
       public:
         explicit OperandIter(LRecoverInfo* recoverInfo)
-          : it_(recoverInfo->begin()), end_(recoverInfo->end()), op_(0)
+          : it_(recoverInfo->begin()), end_(recoverInfo->end()),
+            op_(0), opEnd_(0), rp_(nullptr), node_(nullptr)
         {
             settle();
         }
 
         void settle() {
-            while ((*it_)->numOperands() == 0) {
+            opEnd_ = (*it_)->numOperands();
+            while (opEnd_ == 0) {
                 ++it_;
                 op_ = 0;
+                opEnd_ = (*it_)->numOperands();
             }
+            node_ = *it_;
+            if (node_->isResumePoint())
+                rp_ = node_->toResumePoint();
         }
 
         MDefinition* operator*() {
-            return (*it_)->getOperand(op_);
+            if (rp_) // de-virtualize MResumePoint::getOperand calls.
+                return rp_->getOperand(op_);
+            return node_->getOperand(op_);
         }
         MDefinition* operator ->() {
-            return (*it_)->getOperand(op_);
+            if (rp_) // de-virtualize MResumePoint::getOperand calls.
+                return rp_->getOperand(op_);
+            return node_->getOperand(op_);
         }
 
         OperandIter& operator ++() {
             ++op_;
-            if (op_ == (*it_)->numOperands()) {
-                op_ = 0;
-                ++it_;
-            }
+            if (op_ != opEnd_)
+                return *this;
+            op_ = 0;
+            ++it_;
+            node_ = rp_ = nullptr;
             if (!*this)
                 settle();
-
             return *this;
         }
 
         explicit operator bool() const {
             return it_ == end_;
         }
 
 #ifdef DEBUG
