# HG changeset patch
# User Paul Adenot <paul@paul.cx>
# Date 1506610666 -7200
# Node ID 65ff7c0adadcb1236ad5ec604bdd8a6cb65fd0aa
# Parent  b337c10a3e4bdc92faf6b9d8795b863560a8a347
Bug 1403041 - Don't re-enter libcubeb from the audio thread. r=kinetik

This delays initializing the channel count until actually initializing the
cubeb stream for an AudioCallbackDriver.

The constructor of an AudioCallbackDriver is usually called from the previous
driver's thread (be it a system thread or the audio callback, the exception
being when it's the first time a GraphDriver is being created), while the
AudioCallbackDriver::Init method is always called from a special thread that is
specially created to run potentially long and blocking operations, such as
creating an audio stream.

MozReview-Commit-ID: GFx85N8PlKX

diff --git a/dom/media/AudioBufferUtils.h b/dom/media/AudioBufferUtils.h
--- a/dom/media/AudioBufferUtils.h
+++ b/dom/media/AudioBufferUtils.h
@@ -28,25 +28,51 @@ static inline uint32_t SamplesToFrames(u
  * Class that gets a buffer pointer from an audio callback and provides a safe
  * interface to manipulate this buffer, and to ensure we are not missing frames
  * by the end of the callback.
  */
 template<typename T>
 class AudioCallbackBufferWrapper
 {
 public:
+  AudioCallbackBufferWrapper()
+    : mBuffer(nullptr)
+    , mSamples(0)
+    , mSampleWriteOffset(1)
+    , mChannels(0)
+  {}
+
   explicit AudioCallbackBufferWrapper(uint32_t aChannels)
-    : mBuffer(nullptr),
-      mSamples(0),
-      mSampleWriteOffset(1),
-      mChannels(aChannels)
+    : mBuffer(nullptr)
+    , mSamples(0)
+    , mSampleWriteOffset(1)
+    , mChannels(aChannels)
 
   {
     MOZ_ASSERT(aChannels);
   }
+
+  AudioCallbackBufferWrapper& operator=(const AudioCallbackBufferWrapper& aOther)
+  {
+    MOZ_ASSERT(!aOther.mBuffer,
+               "Don't use this ctor after AudioCallbackDriver::Init");
+    MOZ_ASSERT(aOther.mSamples == 0,
+               "Don't use this ctor after AudioCallbackDriver::Init");
+    MOZ_ASSERT(aOther.mSampleWriteOffset == 1,
+               "Don't use this ctor after AudioCallbackDriver::Init");
+    MOZ_ASSERT(aOther.mChannels != 0);
+
+    mBuffer = nullptr;
+    mSamples = 0;
+    mSampleWriteOffset = 1;
+    mChannels = aOther.mChannels;
+
+    return *this;
+  }
+
   /**
    * Set the buffer in this wrapper. This is to be called at the beginning of
    * the callback.
    */
   void SetBuffer(T* aBuffer, uint32_t aFrames) {
     MOZ_ASSERT(!mBuffer && !mSamples,
         "SetBuffer called twice.");
     mBuffer = aBuffer;
@@ -104,38 +130,63 @@ private:
   /* This is not an owned pointer, but the pointer passed to use via the audio
    * callback. */
   T* mBuffer;
   /* The number of samples of this audio buffer. */
   uint32_t mSamples;
   /* The position at which new samples should be written. We want to return to
    * the audio callback iff this is equal to mSamples. */
   uint32_t mSampleWriteOffset;
-  uint32_t const mChannels;
+  uint32_t mChannels;
 };
 
 /**
  * This is a class that interfaces with the AudioCallbackBufferWrapper, and is
  * responsible for storing the excess of data produced by the MediaStreamGraph
  * because of different rounding constraints, to be used the next time the audio
  * backend calls back.
  */
 template<typename T, uint32_t BLOCK_SIZE>
 class SpillBuffer
 {
 public:
+  SpillBuffer()
+    : mBuffer(nullptr)
+    , mPosition(0)
+    , mChannels(0)
+  {}
+
   explicit SpillBuffer(uint32_t aChannels)
   : mPosition(0)
   , mChannels(aChannels)
   {
     MOZ_ASSERT(aChannels);
     mBuffer = MakeUnique<T[]>(BLOCK_SIZE * mChannels);
     PodZero(mBuffer.get(), BLOCK_SIZE * mChannels);
   }
 
+  SpillBuffer& operator=(SpillBuffer& aOther)
+  {
+    MOZ_ASSERT(aOther.mPosition == 0,
+        "Don't use this ctor after AudioCallbackDriver::Init");
+    MOZ_ASSERT(aOther.mChannels != 0);
+    MOZ_ASSERT(aOther.mBuffer);
+
+    mPosition = aOther.mPosition;
+    mChannels = aOther.mChannels;
+    mBuffer = Move(aOther.mBuffer);
+
+    return *this;
+  }
+
+  SpillBuffer& operator=(SpillBuffer&& aOther)
+  {
+    return this->operator=(aOther);
+  }
+
   /* Empty the spill buffer into the buffer of the audio callback. This returns
    * the number of frames written. */
   uint32_t Empty(AudioCallbackBufferWrapper<T>& aBuffer) {
     uint32_t framesToWrite = std::min(aBuffer.Available(),
                                       SamplesToFrames(mChannels, mPosition));
 
     aBuffer.WriteFrames(mBuffer.get(), framesToWrite);
 
@@ -165,14 +216,14 @@ public:
     return framesToWrite;
   }
 private:
   /* The spilled data. */
   UniquePtr<T[]> mBuffer;
   /* The current write position, in samples, in the buffer when filling, or the
    * amount of buffer filled when emptying. */
   uint32_t mPosition;
-  uint32_t const mChannels;
+  uint32_t mChannels;
 };
 
 } // namespace mozilla
 
 #endif // MOZILLA_SCRATCHBUFFER_H_
diff --git a/dom/media/GraphDriver.cpp b/dom/media/GraphDriver.cpp
--- a/dom/media/GraphDriver.cpp
+++ b/dom/media/GraphDriver.cpp
@@ -560,19 +560,17 @@ StreamAndPromiseForOperation::StreamAndP
   , mPromise(aPromise)
   , mOperation(aOperation)
 {
   // MOZ_ASSERT(aPromise);
 }
 
 AudioCallbackDriver::AudioCallbackDriver(MediaStreamGraphImpl* aGraphImpl)
   : GraphDriver(aGraphImpl)
-  , mOutputChannels(mGraphImpl->AudioChannelCount())
-  , mScratchBuffer(std::max<uint32_t>(1, mOutputChannels))
-  , mBuffer(std::max<uint32_t>(1, mOutputChannels))
+  , mOutputChannels(0)
   , mSampleRate(0)
   , mInputChannels(1)
   , mIterationDurationMS(MEDIA_GRAPH_TARGET_PERIOD_MS)
   , mStarted(false)
   , mAudioInput(nullptr)
   , mAddedMixer(false)
   , mInCallback(false)
   , mMicrophoneActive(false)
@@ -646,16 +644,21 @@ AudioCallbackDriver::Init()
   mSampleRate = output.rate = CubebUtils::PreferredSampleRate();
 
   if (AUDIO_OUTPUT_FORMAT == AUDIO_FORMAT_S16) {
     output.format = CUBEB_SAMPLE_S16NE;
   } else {
     output.format = CUBEB_SAMPLE_FLOAT32NE;
   }
 
+  // Query and set the number of channels this AudioCallbackDriver will use.
+  mOutputChannels = std::max<uint32_t>(1, mGraphImpl->AudioChannelCount());
+  mBuffer = AudioCallbackBufferWrapper<AudioDataValue>(mOutputChannels);
+  mScratchBuffer = SpillBuffer<AudioDataValue, WEBAUDIO_BLOCK_SIZE * 2>(mOutputChannels);
+
   output.channels = mOutputChannels;
   output.layout = CUBEB_LAYOUT_UNDEFINED;
 
   Maybe<uint32_t> latencyPref = CubebUtils::GetCubebMSGLatencyInFrames();
   if (latencyPref) {
     latency_frames = latencyPref.value();
   } else {
     if (cubeb_get_min_latency(cubebContext, &output, &latency_frames) != CUBEB_OK) {
