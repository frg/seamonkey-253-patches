# HG changeset patch
# User Tom Tromey <tom@tromey.com>
# Date 1504707545 21600
# Node ID e261f147b65f0871223d21bcb170b05da79c4d8c
# Parent  5a294c3eff7cad30386079148ed7ba6872283584
Bug 1388789 - use nsTextFormatter::ssprintf in more places; r=froydnj

A few places were using snprintf where ssprintf would be more
appropriate.

MozReview-Commit-ID: LnBy3IcG98C

diff --git a/dom/svg/SVGLength.cpp b/dom/svg/SVGLength.cpp
--- a/dom/svg/SVGLength.cpp
+++ b/dom/svg/SVGLength.cpp
@@ -18,21 +18,17 @@ namespace mozilla {
 
 // Declare some helpers defined below:
 static void GetUnitString(nsAString& unit, uint16_t unitType);
 static uint16_t GetUnitTypeForString(const nsAString& unitStr);
 
 void
 SVGLength::GetValueAsString(nsAString &aValue) const
 {
-  char16_t buf[24];
-  nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
-                            u"%g",
-                            (double)mValue);
-  aValue.Assign(buf);
+  nsTextFormatter::ssprintf(aValue, u"%g", (double)mValue);
 
   nsAutoString unitString;
   GetUnitString(unitString, mUnit);
   aValue.Append(unitString);
 }
 
 bool
 SVGLength::SetValueFromString(const nsAString &aString)
diff --git a/dom/svg/nsSVGAngle.cpp b/dom/svg/nsSVGAngle.cpp
--- a/dom/svg/nsSVGAngle.cpp
+++ b/dom/svg/nsSVGAngle.cpp
@@ -78,21 +78,17 @@ GetUnitTypeForString(const nsAString& un
   }
 
   return SVG_ANGLETYPE_UNKNOWN;
 }
 
 static void
 GetValueString(nsAString &aValueAsString, float aValue, uint16_t aUnitType)
 {
-  char16_t buf[24];
-  nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
-                            u"%g",
-                            (double)aValue);
-  aValueAsString.Assign(buf);
+  nsTextFormatter::ssprintf(aValueAsString, u"%g", (double)aValue);
 
   nsAutoString unitString;
   GetUnitString(unitString, aUnitType);
   aValueAsString.Append(unitString);
 }
 
 static bool
 GetValueFromString(const nsAString& aString,
diff --git a/dom/svg/nsSVGLength2.cpp b/dom/svg/nsSVGLength2.cpp
--- a/dom/svg/nsSVGLength2.cpp
+++ b/dom/svg/nsSVGLength2.cpp
@@ -81,21 +81,17 @@ GetUnitTypeForString(const nsAString& un
   }
 
   return nsIDOMSVGLength::SVG_LENGTHTYPE_UNKNOWN;
 }
 
 static void
 GetValueString(nsAString &aValueAsString, float aValue, uint16_t aUnitType)
 {
-  char16_t buf[24];
-  nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
-                            u"%g",
-                            (double)aValue);
-  aValueAsString.Assign(buf);
+  nsTextFormatter::ssprintf(aValueAsString, u"%g", (double)aValue);
 
   nsAutoString unitString;
   GetUnitString(unitString, aUnitType);
   aValueAsString.Append(unitString);
 }
 
 static bool
 GetValueFromString(const nsAString& aString,
diff --git a/dom/svg/nsSVGTransform.cpp b/dom/svg/nsSVGTransform.cpp
--- a/dom/svg/nsSVGTransform.cpp
+++ b/dom/svg/nsSVGTransform.cpp
@@ -13,69 +13,65 @@ namespace {
   const double kRadPerDegree = 2.0 * M_PI / 360.0;
 } // namespace
 
 namespace mozilla {
 
 void
 nsSVGTransform::GetValueAsString(nsAString& aValue) const
 {
-  char16_t buf[256];
-
   switch (mType) {
     case SVG_TRANSFORM_TRANSLATE:
       // The spec say that if Y is not provided, it is assumed to be zero.
       if (mMatrix._32 != 0)
-        nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+        nsTextFormatter::ssprintf(aValue,
             u"translate(%g, %g)",
             mMatrix._31, mMatrix._32);
       else
-        nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+        nsTextFormatter::ssprintf(aValue,
             u"translate(%g)",
             mMatrix._31);
       break;
     case SVG_TRANSFORM_ROTATE:
       if (mOriginX != 0.0f || mOriginY != 0.0f)
-        nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+        nsTextFormatter::ssprintf(aValue,
             u"rotate(%g, %g, %g)",
             mAngle, mOriginX, mOriginY);
       else
-        nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+        nsTextFormatter::ssprintf(aValue,
             u"rotate(%g)", mAngle);
       break;
     case SVG_TRANSFORM_SCALE:
       if (mMatrix._11 != mMatrix._22)
-        nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+        nsTextFormatter::ssprintf(aValue,
             u"scale(%g, %g)", mMatrix._11, mMatrix._22);
       else
-        nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+        nsTextFormatter::ssprintf(aValue,
             u"scale(%g)", mMatrix._11);
       break;
     case SVG_TRANSFORM_SKEWX:
-      nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+      nsTextFormatter::ssprintf(aValue,
                                 u"skewX(%g)", mAngle);
       break;
     case SVG_TRANSFORM_SKEWY:
-      nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+      nsTextFormatter::ssprintf(aValue,
                                 u"skewY(%g)", mAngle);
       break;
     case SVG_TRANSFORM_MATRIX:
-      nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+      nsTextFormatter::ssprintf(aValue,
           u"matrix(%g, %g, %g, %g, %g, %g)",
                             mMatrix._11, mMatrix._12,
                             mMatrix._21, mMatrix._22,
                             mMatrix._31, mMatrix._32);
       break;
     default:
-      buf[0] = '\0';
+      aValue.Truncate();
       NS_ERROR("unknown transformation type");
       break;
   }
-
-  aValue.Assign(buf);
 }
 
 void
 nsSVGTransform::SetMatrix(const gfxMatrix& aMatrix)
 {
   mType    = SVG_TRANSFORM_MATRIX;
   mMatrix  = aMatrix;
   // We set the other members here too, since operator== requires it and
diff --git a/dom/svg/nsSVGViewBox.cpp b/dom/svg/nsSVGViewBox.cpp
--- a/dom/svg/nsSVGViewBox.cpp
+++ b/dom/svg/nsSVGViewBox.cpp
@@ -201,22 +201,20 @@ nsSVGViewBox::SetBaseValueString(const n
 
 void
 nsSVGViewBox::GetBaseValueString(nsAString& aValue) const
 {
   if (mBaseVal.none) {
     aValue.AssignLiteral("none");
     return;
   }
-  char16_t buf[200];
-  nsTextFormatter::snprintf(buf, sizeof(buf)/sizeof(char16_t),
+  nsTextFormatter::ssprintf(aValue,
                             u"%g %g %g %g",
                             (double)mBaseVal.x, (double)mBaseVal.y,
                             (double)mBaseVal.width, (double)mBaseVal.height);
-  aValue.Assign(buf);
 }
 
 
 already_AddRefed<dom::SVGAnimatedRect>
 nsSVGViewBox::ToSVGAnimatedRect(nsSVGElement* aSVGElement)
 {
   RefPtr<dom::SVGAnimatedRect> domAnimatedRect =
     sSVGAnimatedRectTearoffTable.GetTearoff(this);

