# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1512730557 0
# Node ID fe87a120bd517a514fa006dfcc2df57e8832bc84
# Parent  d0f2ed3bfc4ab9eac1c744a6882f9f68f988291c
Bug 1372258 - Lazily initialise module binding maps so they are not allocated on a background thread r=anba

diff --git a/js/src/builtin/ModuleObject.cpp b/js/src/builtin/ModuleObject.cpp
--- a/js/src/builtin/ModuleObject.cpp
+++ b/js/src/builtin/ModuleObject.cpp
@@ -310,58 +310,66 @@ RequestedModuleObject::create(JSContext*
 
 ///////////////////////////////////////////////////////////////////////////
 // IndirectBindingMap
 
 IndirectBindingMap::Binding::Binding(ModuleEnvironmentObject* environment, Shape* shape)
   : environment(environment), shape(shape)
 {}
 
-IndirectBindingMap::IndirectBindingMap(Zone* zone)
-  : map_(ZoneAllocPolicy(zone))
-{
-}
-
-bool
-IndirectBindingMap::init()
-{
-    return map_.init();
-}
-
 void
 IndirectBindingMap::trace(JSTracer* trc)
 {
-    for (Map::Enum e(map_); !e.empty(); e.popFront()) {
+    if (!map_)
+        return;
+
+    for (Map::Enum e(*map_); !e.empty(); e.popFront()) {
         Binding& b = e.front().value();
         TraceEdge(trc, &b.environment, "module bindings environment");
         TraceEdge(trc, &b.shape, "module bindings shape");
         jsid bindingName = e.front().key();
         TraceManuallyBarrieredEdge(trc, &bindingName, "module bindings binding name");
         MOZ_ASSERT(bindingName == e.front().key());
     }
 }
 
 bool
 IndirectBindingMap::put(JSContext* cx, HandleId name,
                         HandleModuleEnvironmentObject environment, HandleId localName)
 {
+    // This object might have been allocated on the background parsing thread in
+    // different zone to the final module. Lazily allocate the map so we don't
+    // have to switch its zone when merging compartments.
+    if (!map_) {
+        MOZ_ASSERT(!cx->zone()->group()->createdForHelperThread());
+        map_.emplace(cx->zone());
+        if (!map_->init()) {
+            map_.reset();
+            ReportOutOfMemory(cx);
+            return false;
+        }
+    }
+
     RootedShape shape(cx, environment->lookup(cx, localName));
     MOZ_ASSERT(shape);
-    if (!map_.put(name, Binding(environment, shape))) {
+    if (!map_->put(name, Binding(environment, shape))) {
         ReportOutOfMemory(cx);
         return false;
     }
 
     return true;
 }
 
 bool
 IndirectBindingMap::lookup(jsid name, ModuleEnvironmentObject** envOut, Shape** shapeOut) const
 {
-    auto ptr = map_.lookup(name);
+    if (!map_)
+        return false;
+
+    auto ptr = map_->lookup(name);
     if (!ptr)
         return false;
 
     const Binding& binding = ptr->value();
     MOZ_ASSERT(binding.environment);
     MOZ_ASSERT(!binding.environment->inDictionaryMode());
     MOZ_ASSERT(binding.environment->containsPure(binding.shape));
     *envOut = binding.environment;
@@ -705,20 +713,19 @@ ModuleObject::create(JSContext* cx)
     RootedObject proto(cx, cx->global()->getModulePrototype());
     RootedObject obj(cx, NewObjectWithGivenProto(cx, &class_, proto));
     if (!obj)
         return nullptr;
 
     RootedModuleObject self(cx, &obj->as<ModuleObject>());
 
     Zone* zone = cx->zone();
-    IndirectBindingMap* bindings = zone->new_<IndirectBindingMap>(zone);
-    if (!bindings || !bindings->init()) {
+    IndirectBindingMap* bindings = zone->new_<IndirectBindingMap>();
+    if (!bindings) {
         ReportOutOfMemory(cx);
-        js_delete<IndirectBindingMap>(bindings);
         return nullptr;
     }
 
     self->initReservedSlot(ImportBindingsSlot, PrivateValue(bindings));
 
     FunctionDeclarationVector* funDecls = zone->new_<FunctionDeclarationVector>(zone);
     if (!funDecls) {
         ReportOutOfMemory(cx);
@@ -1049,18 +1056,18 @@ ModuleObject::execute(JSContext* cx, Han
 
 /* static */ ModuleNamespaceObject*
 ModuleObject::createNamespace(JSContext* cx, HandleModuleObject self, HandleObject exports)
 {
     MOZ_ASSERT(!self->namespace_());
     MOZ_ASSERT(exports->is<ArrayObject>());
 
     Zone* zone = cx->zone();
-    auto bindings = zone->make_unique<IndirectBindingMap>(zone);
-    if (!bindings || !bindings->init()) {
+    auto bindings = zone->make_unique<IndirectBindingMap>();
+    if (!bindings) {
         ReportOutOfMemory(cx);
         return nullptr;
     }
 
     auto ns = ModuleNamespaceObject::create(cx, self, exports, Move(bindings));
     if (!ns)
         return nullptr;
 
diff --git a/js/src/builtin/ModuleObject.h b/js/src/builtin/ModuleObject.h
--- a/js/src/builtin/ModuleObject.h
+++ b/js/src/builtin/ModuleObject.h
@@ -2,16 +2,18 @@
  * vim: set ts=8 sts=4 et sw=4 tw=99:
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef builtin_ModuleObject_h
 #define builtin_ModuleObject_h
 
+#include "mozilla/Maybe.h"
+
 #include "jsapi.h"
 #include "jsatom.h"
 
 #include "builtin/SelfHostingDefines.h"
 #include "gc/Zone.h"
 #include "js/GCVector.h"
 #include "js/Id.h"
 #include "js/UniquePtr.h"
@@ -124,51 +126,51 @@ class RequestedModuleObject : public Nat
 };
 
 typedef Rooted<RequestedModuleObject*> RootedRequestedModuleObject;
 typedef Handle<RequestedModuleObject*> HandleRequestedModuleObject;
 
 class IndirectBindingMap
 {
   public:
-    explicit IndirectBindingMap(Zone* zone);
-    bool init();
-
     void trace(JSTracer* trc);
 
     bool put(JSContext* cx, HandleId name,
              HandleModuleEnvironmentObject environment, HandleId localName);
 
     size_t count() const {
-        return map_.count();
+        return map_ ? map_->count() : 0;
     }
 
     bool has(jsid name) const {
-        return map_.has(name);
+        return map_ ? map_->has(name) : false;
     }
 
     bool lookup(jsid name, ModuleEnvironmentObject** envOut, Shape** shapeOut) const;
 
     template <typename Func>
     void forEachExportedName(Func func) const {
-        for (auto r = map_.all(); !r.empty(); r.popFront())
+        if (!map_)
+            return;
+
+        for (auto r = map_->all(); !r.empty(); r.popFront())
             func(r.front().key());
     }
 
   private:
     struct Binding
     {
         Binding(ModuleEnvironmentObject* environment, Shape* shape);
         HeapPtr<ModuleEnvironmentObject*> environment;
         HeapPtr<Shape*> shape;
     };
 
     typedef HashMap<jsid, Binding, DefaultHasher<jsid>, ZoneAllocPolicy> Map;
 
-    Map map_;
+    mozilla::Maybe<Map> map_;
 };
 
 class ModuleNamespaceObject : public ProxyObject
 {
   public:
     enum ModuleNamespaceSlot
     {
         ExportsSlot = 0,
diff --git a/js/src/jit-test/tests/modules/bug-1372258.js b/js/src/jit-test/tests/modules/bug-1372258.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/modules/bug-1372258.js
@@ -0,0 +1,28 @@
+if (helperThreadCount() == 0)
+    quit();
+
+// Overwrite built-in parseModule with off-thread module parser.
+function parseModule(source) {
+    offThreadCompileModule(source);
+    return finishOffThreadModule();
+}
+
+// Test case derived from: js/src/jit-test/tests/modules/many-imports.js
+
+// Test importing an import many times.
+
+load(libdir + "dummyModuleResolveHook.js");
+
+const count = 1024;
+
+let a = moduleRepo['a'] = parseModule("export let a = 1;");
+
+let s = "";
+for (let i = 0; i < count; i++) {
+    s += "import { a as i" + i + " } from 'a';\n";
+    s += "assertEq(i" + i + ", 1);\n";
+}
+let b = moduleRepo['b'] = parseModule(s);
+
+b.declarationInstantiation();
+b.evaluation();
