# HG changeset patch
# User Jared Wein <jwein@mozilla.com>
# Date 1501067280 14400
# Node ID 63f67675955dba501b30e8d48960af6adf0cf5a6
# Parent  7c8ac87e1d1d7fae7acb34294108b1d91f2d6eb2
Bug 1384180 - Don't animate the stop->reload if the page loads within 150ms (too distracting). r=dao

MozReview-Commit-ID: 9KvMCRZo5LZ

diff --git a/browser/base/content/browser.js b/browser/base/content/browser.js
--- a/browser/base/content/browser.js
+++ b/browser/base/content/browser.js
@@ -4776,16 +4776,19 @@ var XULBrowserWindow = {
     gIdentityHandler.updateIdentity(this._state, uri);
     TrackingProtection.onSecurityChange(this._state, aIsSimulated);
   },
 
   // simulate all change notifications after switching tabs
   onUpdateCurrentBrowser: function XWB_onUpdateCurrentBrowser(aStateFlags, aStatus, aMessage, aTotalProgress) {
     if (FullZoom.updateBackgroundTabs)
       FullZoom.onLocationChange(gBrowser.currentURI, true);
+
+    CombinedStopReload.onTabSwitch();
+
     var nsIWebProgressListener = Ci.nsIWebProgressListener;
     var loadingDone = aStateFlags & nsIWebProgressListener.STATE_STOP;
     // use a pseudo-object instead of a (potentially nonexistent) channel for getting
     // a correct error message - and make sure that the UI is always either in
     // loading (STATE_START) or done (STATE_STOP) mode
     this.onStateChange(
       gBrowser.webProgress,
       { URI: gBrowser.currentURI },
@@ -4880,16 +4883,17 @@ var CombinedStopReload = {
 
     this._initialized = true;
     if (XULBrowserWindow.stopCommand.getAttribute("disabled") != "true")
       reload.setAttribute("displaystop", "true");
     stop.addEventListener("click", this);
     this.reload = reload;
     this.stop = stop;
     this.stopReloadContainer = this.reload.parentNode;
+    this.timeWhenSwitchedToStop = 0;
 
     // Disable animations until the browser has fully loaded.
     this.animate = false;
     let startupInfo = Cc["@mozilla.org/toolkit/app-startup;1"]
                         .getService(Ci.nsIAppStartup)
                         .getStartupInfo();
     if (startupInfo.sessionRestored) {
       this.startAnimationPrefMonitoring();
@@ -4947,22 +4951,35 @@ var CombinedStopReload = {
         return;
       this.animate = Services.prefs.getBoolPref("toolkit.cosmeticAnimations.enabled") &&
                      Services.prefs.getBoolPref("browser.stopReloadAnimation.enabled");
       Services.prefs.addObserver("toolkit.cosmeticAnimations.enabled", this);
       this.stopReloadContainer.addEventListener("animationend", this);
     });
   },
 
+  onTabSwitch() {
+    // Reset the time in the event of a tabswitch since the stored time
+    // would have been associated with the previous tab.
+    this.timeWhenSwitchedToStop = 0;
+  },
+
   switchToStop(aRequest, aWebProgress) {
     if (!this._initialized || !this._shouldSwitch(aRequest))
       return;
 
+    // Store the time that we switched to the stop button only if a request
+    // is active. Requests are null if the switch is related to a tabswitch.
+    // This is used to determine if we should show the stop->reload animation.
+    if (aRequest) {
+      this.timeWhenSwitchedToStop = window.performance.now();
+    }
+
     let shouldAnimate = AppConstants.MOZ_PHOTON_ANIMATIONS &&
-                        aRequest instanceof Ci.nsIRequest &&
+                        aRequest &&
                         aWebProgress.isTopLevel &&
                         aWebProgress.isLoadingDocument &&
                         this.animate;
 
     this._cancelTransition();
     if (shouldAnimate) {
       BrowserUtils.setToolbarButtonHeightProperty(this.stopReloadContainer);
       this.stopReloadContainer.setAttribute("animate", "true");
@@ -4973,19 +4990,20 @@ var CombinedStopReload = {
   },
 
   switchToReload(aRequest, aWebProgress) {
     if (!this._initialized || !this._shouldSwitch(aRequest) ||
         !this.reload.hasAttribute("displaystop"))
       return;
 
     let shouldAnimate = AppConstants.MOZ_PHOTON_ANIMATIONS &&
-                        aRequest instanceof Ci.nsIRequest &&
+                        aRequest &&
                         aWebProgress.isTopLevel &&
                         !aWebProgress.isLoadingDocument &&
+                        this._loadTimeExceedsMinimumForAnimation() &&
                         this.animate;
 
     if (shouldAnimate) {
       BrowserUtils.setToolbarButtonHeightProperty(this.stopReloadContainer);
       this.stopReloadContainer.setAttribute("animate", "true");
     } else {
       this.stopReloadContainer.removeAttribute("animate");
     }
@@ -5008,16 +5026,26 @@ var CombinedStopReload = {
     this.reload.disabled = true;
     this._timer = setTimeout(function(self) {
       self._timer = 0;
       self.reload.disabled = XULBrowserWindow.reloadCommand
                                              .getAttribute("disabled") == "true";
     }, 650, this);
   },
 
+  _loadTimeExceedsMinimumForAnimation() {
+    // If the time between switching to the stop button then switching to
+    // the reload button exceeds 150ms, then we will show the animation.
+    // If we don't know when we switched to stop (a tabswitch occured while
+    // the page was loading), then we will not prevent the animation from
+    // occuring.
+    return !this.timeWhenSwitchedToStop ||
+           window.performance.now() - this.timeWhenSwitchedToStop > 150;
+  },
+
   _shouldSwitch(aRequest) {
     if (!aRequest ||
         !aRequest.originalURI ||
         aRequest.originalURI.spec.startsWith("about:reader"))
       return true;
 
     if (aRequest.originalURI.schemeIs("chrome") ||
         aRequest.originalURI.schemeIs("about"))
