# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1502954440 -28800
# Node ID 7518bf7c05bca9f1d517b450856e01a043c320ae
# Parent  5dde19a5323f92fbf0cfc507913fedbd8df385f4
Bug 1391142. P3 - remove IsLiveStream() from MediaResource. r=gerald

MozReview-Commit-ID: DBFrYfj2lGB

diff --git a/dom/media/ChannelMediaDecoder.cpp b/dom/media/ChannelMediaDecoder.cpp
--- a/dom/media/ChannelMediaDecoder.cpp
+++ b/dom/media/ChannelMediaDecoder.cpp
@@ -348,16 +348,23 @@ ChannelMediaDecoder::SeekingChanged()
 bool
 ChannelMediaDecoder::CanPlayThroughImpl()
 {
   MOZ_ASSERT(NS_IsMainThread());
   NS_ENSURE_TRUE(GetStateMachine(), false);
   return GetStatistics().CanPlayThrough();
 }
 
+bool
+ChannelMediaDecoder::IsLiveStream()
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  return mResource->IsLiveStream();
+}
+
 void
 ChannelMediaDecoder::OnPlaybackEvent(MediaEventType aEvent)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MediaDecoder::OnPlaybackEvent(aEvent);
   switch (aEvent) {
     case MediaEventType::PlaybackStarted:
       mPlaybackStatistics.Start();
diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -99,16 +99,18 @@ private:
   // from the resource. Called on the main by an event runner dispatched
   // by the MediaResource read functions.
   void NotifyBytesConsumed(int64_t aBytes, int64_t aOffset);
 
   void SeekingChanged();
 
   bool CanPlayThroughImpl() override final;
 
+  bool IsLiveStream() override final;
+
   // The actual playback rate computation.
   void ComputePlaybackRate();
 
   // Something has changed that could affect the computed playback rate,
   // so recompute it.
   void UpdatePlaybackRate();
 
   // Return statistics. This is used for progress events and other things.
diff --git a/dom/media/MediaDecoder.cpp b/dom/media/MediaDecoder.cpp
--- a/dom/media/MediaDecoder.cpp
+++ b/dom/media/MediaDecoder.cpp
@@ -684,17 +684,17 @@ MediaDecoder::CallSeek(const SeekTarget&
 {
   MOZ_ASSERT(NS_IsMainThread());
   AbstractThread::AutoEnter context(AbstractMainThread());
   DiscardOngoingSeekIfExists();
 
   // Since we don't have a listener for changes in IsLiveStream, our best bet
   // is to ensure IsLiveStream is uptodate when seek begins. This value will be
   // checked when seek is completed.
-  mDecoderStateMachine->DispatchIsLiveStream(GetResource()->IsLiveStream());
+  mDecoderStateMachine->DispatchIsLiveStream(IsLiveStream());
 
   mDecoderStateMachine->InvokeSeek(aTarget)
   ->Then(mAbstractMainThread, __func__, this,
          &MediaDecoder::OnSeekResolved, &MediaDecoder::OnSeekRejected)
   ->Track(mSeekRequest);
 }
 
 double
diff --git a/dom/media/MediaDecoder.h b/dom/media/MediaDecoder.h
--- a/dom/media/MediaDecoder.h
+++ b/dom/media/MediaDecoder.h
@@ -505,16 +505,17 @@ private:
   }
 
   void FinishShutdown();
 
   void ConnectMirrors(MediaDecoderStateMachine* aObject);
   void DisconnectMirrors();
 
   virtual bool CanPlayThroughImpl() = 0;
+  virtual bool IsLiveStream() = 0;
 
   // The state machine object for handling the decoding. It is safe to
   // call methods of this object from other threads. Its internal data
   // is synchronised on a monitor. The lifetime of this object is
   // after mPlayState is LOADING and before mPlayState is SHUTDOWN. It
   // is safe to access it during this period.
   //
   // Explicitly prievate to force access via accessors.
diff --git a/dom/media/MediaResource.h b/dom/media/MediaResource.h
--- a/dom/media/MediaResource.h
+++ b/dom/media/MediaResource.h
@@ -230,22 +230,16 @@ public:
 
   /**
    * Fills aRanges with MediaByteRanges representing the data which is cached
    * in the media cache. Stream should be pinned during call and while
    * aRanges is being used.
    */
   virtual nsresult GetCachedRanges(MediaByteRangeSet& aRanges) = 0;
 
-  // Returns true if the resource is a live stream.
-  virtual bool IsLiveStream()
-  {
-    return GetLength() == -1;
-  }
-
   virtual size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const {
     return 0;
   }
 
   virtual size_t SizeOfIncludingThis(MallocSizeOf aMallocSizeOf) const {
     return aMallocSizeOf(this) + SizeOfExcludingThis(aMallocSizeOf);
   }
 
@@ -322,16 +316,19 @@ public:
   // with a new channel. Any cached data associated with the original
   // stream should be accessible in the new stream too.
   virtual already_AddRefed<BaseMediaResource> CloneData(
     MediaResourceCallback* aCallback)
   {
     return nullptr;
   }
 
+  // Returns true if the resource is a live stream.
+  bool IsLiveStream() { return GetLength() == -1; }
+
   size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const override
   {
     // Might be useful to track in the future:
     // - mChannel
     // - mURI (possibly owned, looks like just a ref from mChannel)
     // Not owned:
     // - mCallback
     size_t size = MediaResource::SizeOfExcludingThis(aMallocSizeOf);
diff --git a/dom/media/hls/HLSDecoder.h b/dom/media/hls/HLSDecoder.h
--- a/dom/media/hls/HLSDecoder.h
+++ b/dom/media/hls/HLSDecoder.h
@@ -47,14 +47,16 @@ private:
 
   bool CanPlayThroughImpl() override final
   {
     // TODO: We don't know how to estimate 'canplaythrough' for this decoder.
     // For now we just return true for 'autoplay' can work.
     return true;
   }
 
+  bool IsLiveStream() override final { return false; }
+
   RefPtr<HLSResource> mResource;
 };
 
 } // namespace mozilla
 
 #endif /* HLSDecoder_h_ */
diff --git a/dom/media/hls/HLSResource.h b/dom/media/hls/HLSResource.h
--- a/dom/media/hls/HLSResource.h
+++ b/dom/media/hls/HLSResource.h
@@ -75,21 +75,16 @@ public:
   nsresult GetCachedRanges(MediaByteRangeSet& aRanges) override
   {
     UNIMPLEMENTED();
     return NS_OK;
   }
 
   bool IsTransportSeekable() override { return true; }
 
-  bool IsLiveStream() override
-  {
-    return false;
-  }
-
   java::GeckoHLSResourceWrapper::GlobalRef GetResourceWrapper() {
     return mHLSResourceWrapper;
   }
 
   void Detach() { mDecoder = nullptr; }
 
 private:
   friend class HLSResourceCallbacksSupport;
diff --git a/dom/media/mediasource/MediaSourceDecoder.h b/dom/media/mediasource/MediaSourceDecoder.h
--- a/dom/media/mediasource/MediaSourceDecoder.h
+++ b/dom/media/mediasource/MediaSourceDecoder.h
@@ -73,16 +73,17 @@ public:
 
   void NotifyInitDataArrived();
 
 private:
   MediaDecoderStateMachine* CreateStateMachine();
   void DoSetMediaSourceDuration(double aDuration);
   media::TimeInterval ClampIntervalToEnd(const media::TimeInterval& aInterval);
   bool CanPlayThroughImpl() override;
+  bool IsLiveStream() override final { return !mEnded; }
 
   RefPtr<MediaSourceResource> mResource;
 
   // The owning MediaSource holds a strong reference to this decoder, and
   // calls Attach/DetachMediaSource on this decoder to set and clear
   // mMediaSource.
   dom::MediaSource* mMediaSource;
   RefPtr<MediaSourceDemuxer> mDemuxer;
diff --git a/dom/media/mediasource/MediaSourceResource.h b/dom/media/mediasource/MediaSourceResource.h
--- a/dom/media/mediasource/MediaSourceResource.h
+++ b/dom/media/mediasource/MediaSourceResource.h
@@ -53,21 +53,16 @@ public:
   {
     UNIMPLEMENTED();
     aRanges += MediaByteRange(0, GetLength());
     return NS_OK;
   }
 
   bool IsTransportSeekable() override { return true; }
 
-  bool IsLiveStream() override
-  {
-    MonitorAutoLock mon(mMonitor);
-    return !mEnded;
-  }
   void SetEnded(bool aEnded)
   {
     MonitorAutoLock mon(mMonitor);
     mEnded = aEnded;
   }
 
 private:
   size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const override
