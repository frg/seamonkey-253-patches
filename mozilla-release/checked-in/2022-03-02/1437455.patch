# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1519737370 -3600
# Node ID 4c96fc4ec2b0a3686a4d12e7c675a423ad223829
# Parent  bf17f2937106040d163233e89027a59534b816ec
Bug 1437455 - Guard against assembler buffer overrun.  r=nbp

Two fixes here, either fixes the problem but only one is there
in release builds:

- assert that we don't construct bad BufferOffset values
- check that we're not extending the buffer past INT32_MAX bytes

We could have chosen a smaller limit value since we're limiting
executable code to 1GB on 64-bit systems, but there's no reason to
bring that complexity in here.

diff --git a/js/src/jit/shared/IonAssemblerBuffer.h b/js/src/jit/shared/IonAssemblerBuffer.h
--- a/js/src/jit/shared/IonAssemblerBuffer.h
+++ b/js/src/jit/shared/IonAssemblerBuffer.h
@@ -24,25 +24,31 @@ class BufferOffset
     friend BufferOffset nextOffset();
 
     BufferOffset()
       : offset(INT_MIN)
     { }
 
     explicit BufferOffset(int offset_)
       : offset(offset_)
-    { }
+    {
+        MOZ_ASSERT(offset >= 0);
+    }
 
     explicit BufferOffset(Label* l)
       : offset(l->offset())
-    { }
+    {
+        MOZ_ASSERT(offset >= 0);
+    }
 
     explicit BufferOffset(RepatchLabel* l)
       : offset(l->offset())
-    { }
+    {
+        MOZ_ASSERT(offset >= 0);
+    }
 
     int getOffset() const { return offset; }
     bool assigned() const { return offset != INT_MIN; }
 
     // A BOffImm is a Branch Offset Immediate. It is an architecture-specific
     // structure that holds the immediate for a pc relative branch. diffB takes
     // the label for the destination of the branch, and encodes the immediate
     // for the branch. This will need to be fixed up later, since A pool may be
@@ -182,18 +188,23 @@ class AssemblerBuffer
     { }
 
   public:
     bool isAligned(size_t alignment) const {
         MOZ_ASSERT(mozilla::IsPowerOfTwo(alignment));
         return !(size() & (alignment - 1));
     }
 
-  protected:
-    virtual Slice* newSlice(LifoAlloc& a) {
+  private:
+    Slice* newSlice(LifoAlloc& a) {
+        // Clients of IonAssemblerBuffer can only handle a size up to INT_MAX.
+        if (size() + sizeof(Slice) > INT32_MAX) {
+            fail_oom();
+            return nullptr;
+        }
         Slice* tmp = static_cast<Slice*>(a.alloc(sizeof(Slice)));
         if (!tmp) {
             fail_oom();
             return nullptr;
         }
         return new (tmp) Slice;
     }
 
