# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1505324175 25200
#      Wed Sep 13 10:36:15 2017 -0700
# Node ID 68e69dd626f03beb32a53fe9cd6278b7c9a09634
# Parent  e4c325f3af123b8089d28cefa2cc939a75855c57
Bug 1398942 P1 Convert existing nsPipe3.cpp MOZ_ASSERT's to use MOZ_DIAGNOSTIC_ASSERT. r=froydnj

diff --git a/xpcom/io/nsPipe3.cpp b/xpcom/io/nsPipe3.cpp
--- a/xpcom/io/nsPipe3.cpp
+++ b/xpcom/io/nsPipe3.cpp
@@ -88,18 +88,18 @@ public:
 
 private:
   struct InputEntry
   {
     InputEntry(nsIAsyncInputStream* aStream, nsIInputStreamCallback* aCallback)
       : mStream(aStream)
       , mCallback(aCallback)
     {
-      MOZ_ASSERT(mStream);
-      MOZ_ASSERT(mCallback);
+      MOZ_DIAGNOSTIC_ASSERT(mStream);
+      MOZ_DIAGNOSTIC_ASSERT(mCallback);
     }
 
     nsCOMPtr<nsIAsyncInputStream> mStream;
     nsCOMPtr<nsIInputStreamCallback> mCallback;
   };
 
   nsTArray<InputEntry> mInputList;
 
@@ -396,64 +396,64 @@ public:
                   uint32_t aMaxLength)
     : mPipe(aPipe)
     , mReadState(aReadState)
     , mStatus(NS_ERROR_FAILURE)
     , mSegment(nullptr)
     , mLength(0)
     , mOffset(0)
   {
-    MOZ_ASSERT(mPipe);
-    MOZ_ASSERT(!mReadState.mActiveRead);
+    MOZ_DIAGNOSTIC_ASSERT(mPipe);
+    MOZ_DIAGNOSTIC_ASSERT(!mReadState.mActiveRead);
     mStatus = mPipe->GetReadSegment(mReadState, mSegment, mLength);
     if (NS_SUCCEEDED(mStatus)) {
-      MOZ_ASSERT(mReadState.mActiveRead);
-      MOZ_ASSERT(mSegment);
+      MOZ_DIAGNOSTIC_ASSERT(mReadState.mActiveRead);
+      MOZ_DIAGNOSTIC_ASSERT(mSegment);
       mLength = std::min(mLength, aMaxLength);
-      MOZ_ASSERT(mLength);
+      MOZ_DIAGNOSTIC_ASSERT(mLength);
     }
   }
 
   ~AutoReadSegment()
   {
     if (NS_SUCCEEDED(mStatus)) {
       if (mOffset) {
         mPipe->AdvanceReadCursor(mReadState, mOffset);
       } else {
         nsPipeEvents events;
         mPipe->ReleaseReadSegment(mReadState, events);
       }
     }
-    MOZ_ASSERT(!mReadState.mActiveRead);
+    MOZ_DIAGNOSTIC_ASSERT(!mReadState.mActiveRead);
   }
 
   nsresult Status() const
   {
     return mStatus;
   }
 
   const char* Data() const
   {
-    MOZ_ASSERT(NS_SUCCEEDED(mStatus));
-    MOZ_ASSERT(mSegment);
+    MOZ_DIAGNOSTIC_ASSERT(NS_SUCCEEDED(mStatus));
+    MOZ_DIAGNOSTIC_ASSERT(mSegment);
     return mSegment + mOffset;
   }
 
   uint32_t Length() const
   {
-    MOZ_ASSERT(NS_SUCCEEDED(mStatus));
-    MOZ_ASSERT(mLength >= mOffset);
+    MOZ_DIAGNOSTIC_ASSERT(NS_SUCCEEDED(mStatus));
+    MOZ_DIAGNOSTIC_ASSERT(mLength >= mOffset);
     return mLength - mOffset;
   }
 
   void
   Advance(uint32_t aCount)
   {
-    MOZ_ASSERT(NS_SUCCEEDED(mStatus));
-    MOZ_ASSERT(aCount <= (mLength - mOffset));
+    MOZ_DIAGNOSTIC_ASSERT(NS_SUCCEEDED(mStatus));
+    MOZ_DIAGNOSTIC_ASSERT(aCount <= (mLength - mOffset));
     mOffset += aCount;
   }
 
   nsPipeReadState&
   ReadState() const
   {
     return mReadState;
   }
@@ -542,17 +542,17 @@ nsPipe::~nsPipe()
 }
 
 NS_IMPL_ADDREF(nsPipe)
 NS_IMPL_QUERY_INTERFACE(nsPipe, nsIPipe)
 
 NS_IMETHODIMP_(MozExternalRefCountType)
 nsPipe::Release()
 {
-  MOZ_ASSERT(int32_t(mRefCnt) > 0, "dup release");
+  MOZ_DIAGNOSTIC_ASSERT(int32_t(mRefCnt) > 0, "dup release");
   nsrefcnt count = --mRefCnt;
   NS_LOG_RELEASE(this, count, "nsPipe");
   if (count == 0) {
     delete (this);
     return 0;
   }
   // Avoid racing on |mOriginalInput| by only looking at it when
   // the refcount is 1, that is, we are the only pointer (hence only
@@ -658,32 +658,32 @@ nsPipe::GetReadSegment(nsPipeReadState& 
     return NS_FAILED(mStatus) ? mStatus : NS_BASE_STREAM_WOULD_BLOCK;
   }
 
   // The input stream locks the pipe while getting the buffer to read from,
   // but then unlocks while actual data copying is taking place.  In
   // order to avoid deleting the buffer out from under this lockless read
   // set a flag to indicate a read is active.  This flag is only modified
   // while the lock is held.
-  MOZ_ASSERT(!aReadState.mActiveRead);
+  MOZ_DIAGNOSTIC_ASSERT(!aReadState.mActiveRead);
   aReadState.mActiveRead = true;
 
   aSegment = aReadState.mReadCursor;
   aLength = aReadState.mReadLimit - aReadState.mReadCursor;
   MOZ_DIAGNOSTIC_ASSERT(aLength <= aReadState.mAvailable);
 
   return NS_OK;
 }
 
 void
 nsPipe::ReleaseReadSegment(nsPipeReadState& aReadState, nsPipeEvents& aEvents)
 {
   ReentrantMonitorAutoEnter mon(mReentrantMonitor);
 
-  MOZ_ASSERT(aReadState.mActiveRead);
+  MOZ_DIAGNOSTIC_ASSERT(aReadState.mActiveRead);
   aReadState.mActiveRead = false;
 
   // When a read completes and releases the mActiveRead flag, we may have blocked
   // a drain from completing.  This occurs when the input stream is closed during
   // the read.  In these cases, we need to complete the drain as soon as the
   // active read completes.
   if (aReadState.mNeedDrain) {
     aReadState.mNeedDrain = false;
@@ -702,17 +702,17 @@ nsPipe::AdvanceReadCursor(nsPipeReadStat
 
     LOG(("III advancing read cursor by %u\n", aBytesRead));
     NS_ASSERTION(aBytesRead <= mBuffer.GetSegmentSize(), "read too much");
 
     aReadState.mReadCursor += aBytesRead;
     NS_ASSERTION(aReadState.mReadCursor <= aReadState.mReadLimit,
                  "read cursor exceeds limit");
 
-    MOZ_ASSERT(aReadState.mAvailable >= aBytesRead);
+    MOZ_DIAGNOSTIC_ASSERT(aReadState.mAvailable >= aBytesRead);
     aReadState.mAvailable -= aBytesRead;
 
     // Check to see if we're at the end of the available read data.  If we
     // are, and this segment is not still being written, then we can possibly
     // free up the segment.
     if (aReadState.mReadCursor == aReadState.mReadLimit &&
         !ReadSegmentBeingWritten(aReadState)) {
 
@@ -762,17 +762,17 @@ nsPipe::AdvanceReadSegment(nsPipeReadSta
 
     // done with this segment
     mBuffer.DeleteFirstSegment();
     LOG(("III deleting first segment\n"));
   }
 
   if (mWriteSegment < aReadState.mSegment) {
     // read cursor has hit the end of written data, so reset it
-    MOZ_ASSERT(mWriteSegment == (aReadState.mSegment - 1));
+    MOZ_DIAGNOSTIC_ASSERT(mWriteSegment == (aReadState.mSegment - 1));
     aReadState.mReadCursor = nullptr;
     aReadState.mReadLimit = nullptr;
     // also, the buffer is completely empty, so reset the write cursor
     if (mWriteSegment == -1) {
       mWriteCursor = nullptr;
       mWriteLimit = nullptr;
     }
   } else {
@@ -806,17 +806,17 @@ nsPipe::DrainInputStream(nsPipeReadState
   ReentrantMonitorAutoEnter mon(mReentrantMonitor);
 
   // If a segment is actively being read in ReadSegments() for this input
   // stream, then we cannot drain the stream.  This can happen because
   // ReadSegments() does not hold the lock while copying from the buffer.
   // If we detect this condition, simply note that we need a drain once
   // the read completes and return immediately.
   if (aReadState.mActiveRead) {
-    MOZ_ASSERT(!aReadState.mNeedDrain);
+    MOZ_DIAGNOSTIC_ASSERT(!aReadState.mNeedDrain);
     aReadState.mNeedDrain = true;
     return;
   }
 
   while(mWriteSegment >= aReadState.mSegment) {
 
     // If the last segment to free is still being written to, we're done
     // draining.  We can't free any more.
@@ -954,17 +954,17 @@ nsPipe::AdvanceWriteCursor(uint32_t aByt
       mon.NotifyAll();
     }
   }
 }
 
 void
 nsPipe::OnInputStreamException(nsPipeInputStream* aStream, nsresult aReason)
 {
-  MOZ_ASSERT(NS_FAILED(aReason));
+  MOZ_DIAGNOSTIC_ASSERT(NS_FAILED(aReason));
 
   nsPipeEvents events;
   {
     ReentrantMonitorAutoEnter mon(mReentrantMonitor);
 
     // Its possible to re-enter this method when we call OnPipeException() or
     // OnInputExection() below.  If there is a caller stuck in our synchronous
     // Wait() method, then they will get woken up with a failure code which
@@ -1098,19 +1098,19 @@ nsPipe::AllReadCursorsMatchWriteCursor()
 }
 
 void
 nsPipe::RollBackAllReadCursors(char* aWriteCursor)
 {
   mReentrantMonitor.AssertCurrentThreadIn();
   for (uint32_t i = 0; i < mInputList.Length(); ++i) {
     nsPipeReadState& readState = mInputList[i]->ReadState();
-    MOZ_ASSERT(mWriteSegment == readState.mSegment);
-    MOZ_ASSERT(mWriteCursor == readState.mReadCursor);
-    MOZ_ASSERT(mWriteCursor == readState.mReadLimit);
+    MOZ_DIAGNOSTIC_ASSERT(mWriteSegment == readState.mSegment);
+    MOZ_DIAGNOSTIC_ASSERT(mWriteCursor == readState.mReadCursor);
+    MOZ_DIAGNOSTIC_ASSERT(mWriteCursor == readState.mReadLimit);
     readState.mReadCursor = aWriteCursor;
     readState.mReadLimit = aWriteCursor;
   }
 }
 
 void
 nsPipe::UpdateAllReadCursors(char* aWriteCursor)
 {
@@ -1165,18 +1165,18 @@ nsPipe::GetBufferSegmentCount(const nsPi
   // The write segment can be smaller than the current reader position
   // in some cases.  For example, when the first write segment has not
   // been allocated yet mWriteSegment is negative.  In these cases
   // the stream is effectively using zero segments.
   if (mWriteSegment < aReadState.mSegment) {
     return 0;
   }
 
-  MOZ_ASSERT(mWriteSegment >= 0);
-  MOZ_ASSERT(aReadState.mSegment >= 0);
+  MOZ_DIAGNOSTIC_ASSERT(mWriteSegment >= 0);
+  MOZ_DIAGNOSTIC_ASSERT(aReadState.mSegment >= 0);
 
   // Otherwise at least one segment is being used.  We add one here
   // since a single segment is being used when the write and read
   // segment indices are the same.
   return 1 + mWriteSegment - aReadState.mSegment;
 }
 
 bool
