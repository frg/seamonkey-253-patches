# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1508595283 -32400
# Node ID 30938a11b6e89382869a626f7ce0cd92de4028a3
# Parent  fecdd6a3f8474c7dd5cbf15504922a609a8d8266
Bug 1410283 - Cache AsyncGeneratorRequest object. r=till

diff --git a/js/src/builtin/Promise.cpp b/js/src/builtin/Promise.cpp
--- a/js/src/builtin/Promise.cpp
+++ b/js/src/builtin/Promise.cpp
@@ -2760,16 +2760,18 @@ js::AsyncGeneratorResolve(JSContext* cx,
     Rooted<AsyncGeneratorRequest*> request(
         cx, AsyncGeneratorObject::dequeueRequest(cx, asyncGenObj));
     if (!request)
         return false;
 
     // Step 5.
     RootedObject resultPromise(cx, request->promise());
 
+    asyncGenObj->cacheRequest(request);
+
     // Step 6.
     RootedObject resultObj(cx, CreateIterResultObject(cx, value, done));
     if (!resultObj)
         return false;
 
     RootedValue resultValue(cx, ObjectValue(*resultObj));
 
     // Step 7.
@@ -2798,16 +2800,18 @@ js::AsyncGeneratorReject(JSContext* cx, 
     Rooted<AsyncGeneratorRequest*> request(
         cx, AsyncGeneratorObject::dequeueRequest(cx, asyncGenObj));
     if (!request)
         return false;
 
     // Step 5.
     RootedObject resultPromise(cx, request->promise());
 
+    asyncGenObj->cacheRequest(request);
+
     // Step 6.
     if (!RejectMaybeWrappedPromise(cx, resultPromise, exception))
         return false;
 
     // Step 7.
     if (!AsyncGeneratorResumeNext(cx, asyncGenObj))
         return false;
 
@@ -2935,17 +2939,18 @@ js::AsyncGeneratorEnqueue(JSContext* cx,
         return true;
     }
 
     Rooted<AsyncGeneratorObject*> asyncGenObj(
         cx, &asyncGenVal.toObject().as<AsyncGeneratorObject>());
 
     // Step 5 (reordered).
     Rooted<AsyncGeneratorRequest*> request(
-        cx, AsyncGeneratorRequest::create(cx, completionKind, completionValue, resultPromise));
+        cx, AsyncGeneratorObject::createRequest(cx, asyncGenObj, completionKind, completionValue,
+                                                resultPromise));
     if (!request)
         return false;
 
     // Steps 4, 6.
     if (!AsyncGeneratorObject::enqueueRequest(cx, asyncGenObj, request))
         return false;
 
     // Step 7.
diff --git a/js/src/vm/AsyncIteration.cpp b/js/src/vm/AsyncIteration.cpp
--- a/js/src/vm/AsyncIteration.cpp
+++ b/js/src/vm/AsyncIteration.cpp
@@ -308,19 +308,34 @@ AsyncGeneratorObject::create(JSContext* 
     asyncGenObj->setGenerator(generatorVal);
 
     // Step 7.
     asyncGenObj->setSuspendedStart();
 
     // Step 8.
     asyncGenObj->clearSingleQueueRequest();
 
+    asyncGenObj->clearCachedRequest();
+
     return asyncGenObj;
 }
 
+/* static */ AsyncGeneratorRequest*
+AsyncGeneratorObject::createRequest(JSContext* cx, Handle<AsyncGeneratorObject*> asyncGenObj,
+                                    CompletionKind completionKind, HandleValue completionValue,
+                                    HandleObject promise)
+{
+    if (!asyncGenObj->hasCachedRequest())
+        return AsyncGeneratorRequest::create(cx, completionKind, completionValue, promise);
+
+    AsyncGeneratorRequest* request = asyncGenObj->takeCachedRequest();
+    request->init(completionKind, completionValue, promise);
+    return request;
+}
+
 /* static */ MOZ_MUST_USE bool
 AsyncGeneratorObject::enqueueRequest(JSContext* cx, Handle<AsyncGeneratorObject*> asyncGenObj,
                                      Handle<AsyncGeneratorRequest*> request)
 {
     if (asyncGenObj->isSingleQueue()) {
         if (asyncGenObj->isSingleQueueEmpty()) {
             asyncGenObj->setSingleQueueRequest(request);
             return true;
@@ -370,27 +385,25 @@ AsyncGeneratorObject::peekRequest(JSCont
 
 const Class AsyncGeneratorRequest::class_ = {
     "AsyncGeneratorRequest",
     JSCLASS_HAS_RESERVED_SLOTS(AsyncGeneratorRequest::Slots)
 };
 
 // Async Iteration proposal 11.4.3.1.
 /* static */ AsyncGeneratorRequest*
-AsyncGeneratorRequest::create(JSContext* cx, CompletionKind completionKind_,
-                              HandleValue completionValue_, HandleObject promise_)
+AsyncGeneratorRequest::create(JSContext* cx, CompletionKind completionKind,
+                              HandleValue completionValue, HandleObject promise)
 {
     RootedObject obj(cx, NewNativeObjectWithGivenProto(cx, &class_, nullptr));
     if (!obj)
         return nullptr;
 
     Handle<AsyncGeneratorRequest*> request = obj.as<AsyncGeneratorRequest>();
-    request->setCompletionKind(completionKind_);
-    request->setCompletionValue(completionValue_);
-    request->setPromise(promise_);
+    request->init(completionKind, completionValue, promise);
     return request;
 }
 
 // Async Iteration proposal 11.4.3.2 AsyncGeneratorStart steps 5.d-g.
 static MOZ_MUST_USE bool
 AsyncGeneratorReturned(JSContext* cx, Handle<AsyncGeneratorObject*> asyncGenObj,
                        HandleValue value)
 {
diff --git a/js/src/vm/AsyncIteration.h b/js/src/vm/AsyncIteration.h
--- a/js/src/vm/AsyncIteration.h
+++ b/js/src/vm/AsyncIteration.h
@@ -46,43 +46,48 @@ MOZ_MUST_USE bool
 AsyncGeneratorYieldReturnAwaitedFulfilled(JSContext* cx,
                                           Handle<AsyncGeneratorObject*> asyncGenObj,
                                           HandleValue value);
 MOZ_MUST_USE bool
 AsyncGeneratorYieldReturnAwaitedRejected(JSContext* cx,
                                          Handle<AsyncGeneratorObject*> asyncGenObj,
                                          HandleValue reason);
 
+class AsyncGeneratorObject;
+
 class AsyncGeneratorRequest : public NativeObject
 {
   private:
     enum AsyncGeneratorRequestSlots {
         Slot_CompletionKind = 0,
         Slot_CompletionValue,
         Slot_Promise,
         Slots,
     };
 
-    void setCompletionKind(CompletionKind completionKind_) {
+    void init(CompletionKind completionKind, HandleValue completionValue,
+              HandleObject promise) {
         setFixedSlot(Slot_CompletionKind,
-                     Int32Value(static_cast<int32_t>(completionKind_)));
+                     Int32Value(static_cast<int32_t>(completionKind)));
+        setFixedSlot(Slot_CompletionValue, completionValue);
+        setFixedSlot(Slot_Promise, ObjectValue(*promise));
     }
-    void setCompletionValue(HandleValue completionValue_) {
-        setFixedSlot(Slot_CompletionValue, completionValue_);
+
+    void clearData() {
+        setFixedSlot(Slot_CompletionValue, NullValue());
+        setFixedSlot(Slot_Promise, NullValue());
     }
-    void setPromise(HandleObject promise_) {
-        setFixedSlot(Slot_Promise, ObjectValue(*promise_));
-    }
+
+    friend AsyncGeneratorObject;
 
   public:
     static const Class class_;
 
-    static AsyncGeneratorRequest*
-    create(JSContext* cx, CompletionKind completionKind, HandleValue completionValue,
-           HandleObject promise);
+    static AsyncGeneratorRequest* create(JSContext* cx, CompletionKind completionKind,
+                                         HandleValue completionValue, HandleObject promise);
 
     CompletionKind completionKind() const {
         return static_cast<CompletionKind>(getFixedSlot(Slot_CompletionKind).toInt32());
     }
     JS::Value completionValue() const {
         return getFixedSlot(Slot_CompletionValue);
     }
     JSObject* promise() const {
@@ -92,16 +97,17 @@ class AsyncGeneratorRequest : public Nat
 
 class AsyncGeneratorObject : public NativeObject
 {
   private:
     enum AsyncGeneratorObjectSlots {
         Slot_State = 0,
         Slot_Generator,
         Slot_QueueOrRequest,
+        Slot_CachedRequest,
         Slots
     };
 
     enum State {
         State_SuspendedStart,
         State_SuspendedYield,
         State_Executing,
         // State_AwaitingYieldReturn corresponds to the case that
@@ -135,17 +141,17 @@ class AsyncGeneratorObject : public Nati
     }
     bool isSingleQueueEmpty() const {
         return getFixedSlot(Slot_QueueOrRequest).isNull();
     }
     void setSingleQueueRequest(AsyncGeneratorRequest* request) {
         setFixedSlot(Slot_QueueOrRequest, ObjectValue(*request));
     }
     void clearSingleQueueRequest() {
-        setFixedSlot(Slot_QueueOrRequest, NullHandleValue);
+        setFixedSlot(Slot_QueueOrRequest, NullValue());
     }
     AsyncGeneratorRequest* singleQueueRequest() const {
         return &getFixedSlot(Slot_QueueOrRequest).toObject().as<AsyncGeneratorRequest>();
     }
 
     NativeObject* queue() const {
         return &getFixedSlot(Slot_QueueOrRequest).toObject().as<NativeObject>();
     }
@@ -214,16 +220,51 @@ class AsyncGeneratorObject : public Nati
     static AsyncGeneratorRequest*
     peekRequest(JSContext* cx, Handle<AsyncGeneratorObject*> asyncGenObj);
 
     bool isQueueEmpty() const {
         if (isSingleQueue())
             return isSingleQueueEmpty();
         return queue()->getDenseInitializedLength() == 0;
     }
+
+    // This function does either of the following:
+    //   * return a cached request object with the slots updated
+    //   * create a new request object with the slots set
+    static AsyncGeneratorRequest* createRequest(JSContext* cx,
+                                                Handle<AsyncGeneratorObject*> asyncGenObj,
+                                                CompletionKind completionKind,
+                                                HandleValue completionValue,
+                                                HandleObject promise);
+
+    // Stores the given request to the generator's cache after clearing its data
+    // slots.  The cached request will be reused in the subsequent createRequest
+    // call.
+    void cacheRequest(AsyncGeneratorRequest* request) {
+        if (hasCachedRequest())
+            return;
+
+        request->clearData();
+        setFixedSlot(Slot_CachedRequest, ObjectValue(*request));
+    }
+
+  private:
+    bool hasCachedRequest() const {
+        return getFixedSlot(Slot_CachedRequest).isObject();
+    }
+
+    AsyncGeneratorRequest* takeCachedRequest() {
+        auto request = &getFixedSlot(Slot_CachedRequest).toObject().as<AsyncGeneratorRequest>();
+        clearCachedRequest();
+        return request;
+    }
+
+    void clearCachedRequest() {
+        setFixedSlot(Slot_CachedRequest, NullValue());
+    }
 };
 
 JSObject*
 CreateAsyncFromSyncIterator(JSContext* cx, HandleObject iter);
 
 class AsyncFromSyncIteratorObject : public NativeObject
 {
   private:

