# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1496761352 -7200
#      Tue Jun 06 17:02:32 2017 +0200
# Node ID 4cdb90db1896574c47c1919406ded08bf82532d7
# Parent  b87ff1900b3004bf95891821accf5a30780f36ba
Bug 1277562 - Part 1: Equate Tier::Debug with Tier::Baseline and Tier::Serialized with Tier::Ion.  r=luke

diff --git a/js/src/wasm/WasmCode.cpp b/js/src/wasm/WasmCode.cpp
--- a/js/src/wasm/WasmCode.cpp
+++ b/js/src/wasm/WasmCode.cpp
@@ -267,17 +267,16 @@ bool
 CodeSegment::initialize(Tier tier,
                         UniqueCodeBytes codeBytes,
                         uint32_t codeLength,
                         const ShareableBytes& bytecode,
                         const LinkDataTier& linkData,
                         const Metadata& metadata)
 {
     MOZ_ASSERT(bytes_ == nullptr);
-    MOZ_ASSERT(tier == Tier::Baseline || tier == Tier::Ion);
 
     tier_ = tier;
     bytes_ = Move(codeBytes);
     functionLength_ = linkData.functionCodeLength;
     length_ = codeLength;
     interruptCode_ = bytes_.get() + linkData.interruptOffset;
     outOfBoundsCode_ = bytes_.get() + linkData.outOfBoundsOffset;
     unalignedAccessCode_ = bytes_.get() + linkData.unalignedAccessOffset;
@@ -307,17 +306,17 @@ CodeSegment::addSizeOfMisc(mozilla::Mall
 {
     *data += mallocSizeOf(this);
     *code += RoundupCodeLength(length_);
 }
 
 uint8_t*
 CodeSegment::serialize(uint8_t* cursor, const LinkDataTier& linkData) const
 {
-    MOZ_ASSERT(tier() == Tier::Ion);
+    MOZ_ASSERT(tier() == Tier::Serialized);
 
     cursor = WriteScalar<uint32_t>(cursor, length_);
     uint8_t* base = cursor;
     cursor = WriteBytes(cursor, bytes_.get(), length_);
     StaticallyUnlink(base, linkData);
     return cursor;
 }
 
@@ -334,17 +333,17 @@ CodeSegment::deserialize(const uint8_t* 
     UniqueCodeBytes bytes = AllocateCodeBytes(length);
     if (!bytes)
         return nullptr;
 
     cursor = ReadBytes(cursor, bytes.get(), length);
     if (!cursor)
         return nullptr;
 
-    if (!initialize(Tier::Ion, Move(bytes), length, bytecode, linkData, metadata))
+    if (!initialize(Tier::Serialized, Move(bytes), length, bytecode, linkData, metadata))
         return nullptr;
 
     return cursor;
 }
 
 size_t
 FuncExport::serializedSize() const
 {
@@ -499,17 +498,16 @@ Metadata::tiers() const
 {
     return Tiers(tier_->tier);
 }
 
 const MetadataTier&
 Metadata::metadata(Tier t) const
 {
     switch (t) {
-      case Tier::Debug:
       case Tier::Baseline:
         MOZ_RELEASE_ASSERT(tier_->tier == Tier::Baseline);
         return *tier_;
       case Tier::Ion:
         MOZ_RELEASE_ASSERT(tier_->tier == Tier::Ion);
         return *tier_;
       case Tier::TBD:
         return *tier_;
@@ -517,17 +515,16 @@ Metadata::metadata(Tier t) const
         MOZ_CRASH();
     }
 }
 
 MetadataTier&
 Metadata::metadata(Tier t)
 {
     switch (t) {
-      case Tier::Debug:
       case Tier::Baseline:
         MOZ_RELEASE_ASSERT(tier_->tier == Tier::Baseline);
         return *tier_;
       case Tier::Ion:
         MOZ_RELEASE_ASSERT(tier_->tier == Tier::Ion);
         return *tier_;
       case Tier::TBD:
         return *tier_;
@@ -535,17 +532,17 @@ Metadata::metadata(Tier t)
         MOZ_CRASH();
     }
 }
 
 size_t
 Metadata::serializedSize() const
 {
     return sizeof(pod()) +
-           metadata(Tier::Ion).serializedSize() +
+           metadata(Tier::Serialized).serializedSize() +
            SerializedVectorSize(sigIds) +
            SerializedPodVectorSize(globals) +
            SerializedPodVectorSize(tables) +
            SerializedPodVectorSize(funcNames) +
            SerializedPodVectorSize(customSections) +
            filename.serializedSize() +
            sizeof(hash);
 }
@@ -567,32 +564,32 @@ Metadata::sizeOfExcludingThis(MallocSize
            filename.sizeOfExcludingThis(mallocSizeOf);
 }
 
 uint8_t*
 Metadata::serialize(uint8_t* cursor) const
 {
     MOZ_ASSERT(!debugEnabled && debugFuncArgTypes.empty() && debugFuncReturnTypes.empty());
     cursor = WriteBytes(cursor, &pod(), sizeof(pod()));
-    cursor = metadata(Tier::Ion).serialize(cursor);
+    cursor = metadata(Tier::Serialized).serialize(cursor);
     cursor = SerializeVector(cursor, sigIds);
     cursor = SerializePodVector(cursor, globals);
     cursor = SerializePodVector(cursor, tables);
     cursor = SerializePodVector(cursor, funcNames);
     cursor = SerializePodVector(cursor, customSections);
     cursor = filename.serialize(cursor);
     cursor = WriteBytes(cursor, hash, sizeof(hash));
     return cursor;
 }
 
 /* static */ const uint8_t*
 Metadata::deserialize(const uint8_t* cursor)
 {
     (cursor = ReadBytes(cursor, &pod(), sizeof(pod()))) &&
-    (cursor = metadata(Tier::Ion).deserialize(cursor)) &&
+    (cursor = metadata(Tier::Serialized).deserialize(cursor)) &&
     (cursor = DeserializeVector(cursor, &sigIds)) &&
     (cursor = DeserializePodVector(cursor, &globals)) &&
     (cursor = DeserializePodVector(cursor, &tables)) &&
     (cursor = DeserializePodVector(cursor, &funcNames)) &&
     (cursor = DeserializePodVector(cursor, &customSections)) &&
     (cursor = filename.deserialize(cursor)) &&
     (cursor = ReadBytes(cursor, hash, sizeof(hash)));
     debugEnabled = false;
@@ -673,17 +670,16 @@ Code::tiers() const
 {
     return Tiers(tier_->tier());
 }
 
 const CodeSegment&
 Code::segment(Tier tier) const
 {
     switch (tier) {
-      case Tier::Debug:
       case Tier::Baseline:
         MOZ_RELEASE_ASSERT(tier_->tier() == Tier::Baseline);
         return *tier_;
       case Tier::Ion:
         MOZ_RELEASE_ASSERT(tier_->tier() == Tier::Ion);
         return *tier_;
       case Tier::TBD:
         return *tier_;
@@ -728,55 +724,55 @@ struct CallSiteRetAddrOffset
         return callSites[index].returnAddressOffset();
     }
 };
 
 size_t
 Code::serializedSize() const
 {
     return metadata().serializedSize() +
-           segment(Tier::Ion).serializedSize();
+           segment(Tier::Serialized).serializedSize();
 }
 
 uint8_t*
 Code::serialize(uint8_t* cursor, const LinkData& linkData) const
 {
     MOZ_RELEASE_ASSERT(!metadata().debugEnabled);
 
     cursor = metadata().serialize(cursor);
-    cursor = segment(Tier::Ion).serialize(cursor, linkData.linkData(Tier::Ion));
+    cursor = segment(Tier::Serialized).serialize(cursor, linkData.linkData(Tier::Serialized));
     return cursor;
 }
 
 const uint8_t*
 Code::deserialize(const uint8_t* cursor, const SharedBytes& bytecode, const LinkData& linkData,
                   Metadata* maybeMetadata)
 {
     MutableMetadata metadata;
     if (maybeMetadata) {
         metadata = maybeMetadata;
     } else {
-        auto tier = js::MakeUnique<MetadataTier>(Tier::Ion);
+        auto tier = js::MakeUnique<MetadataTier>(Tier::Serialized);
         if (!tier)
             return nullptr;
 
         metadata = js_new<Metadata>(Move(tier));
         if (!metadata)
             return nullptr;
     }
 
     cursor = metadata->deserialize(cursor);
     if (!cursor)
         return nullptr;
 
     UniqueCodeSegment codeSegment = js::MakeUnique<CodeSegment>();
     if (!codeSegment)
         return nullptr;
 
-    cursor = codeSegment->deserialize(cursor, *bytecode, linkData.linkData(Tier::Ion), *metadata);
+    cursor = codeSegment->deserialize(cursor, *bytecode, linkData.linkData(Tier::Serialized), *metadata);
     if (!cursor)
         return nullptr;
 
     tier_ = UniqueConstCodeSegment(codeSegment.release());
     metadata_ = metadata;
 
     return cursor;
 }
diff --git a/js/src/wasm/WasmCode.h b/js/src/wasm/WasmCode.h
--- a/js/src/wasm/WasmCode.h
+++ b/js/src/wasm/WasmCode.h
@@ -343,21 +343,17 @@ struct MetadataCacheablePod
         globalDataLength(0)
     {}
 };
 
 typedef uint8_t ModuleHash[8];
 
 struct MetadataTier
 {
-    explicit MetadataTier(Tier tier)
-      : tier(tier)
-    {
-        MOZ_ASSERT(tier == Tier::Baseline || tier == Tier::Ion);
-    }
+    explicit MetadataTier(Tier tier) : tier(tier) {}
 
     const Tier            tier;
 
     MemoryAccessVector    memoryAccesses;
     CodeRangeVector       codeRanges;
     CallSiteVector        callSites;
     FuncImportVector      funcImports;
     FuncExportVector      funcExports;
diff --git a/js/src/wasm/WasmGenerator.h b/js/src/wasm/WasmGenerator.h
--- a/js/src/wasm/WasmGenerator.h
+++ b/js/src/wasm/WasmGenerator.h
@@ -148,17 +148,16 @@ class CompileTask
     }
 
   public:
     CompileTask(const ModuleEnvironment& env, Tier tier, size_t defaultChunkSize)
       : env_(env),
         tier_(tier),
         lifo_(defaultChunkSize)
     {
-        MOZ_ASSERT(tier == Tier::Baseline || tier == Tier::Ion);
         init();
     }
     LifoAlloc& lifo() {
         return lifo_;
     }
     jit::TempAllocator& alloc() {
         return *alloc_;
     }
diff --git a/js/src/wasm/WasmModule.cpp b/js/src/wasm/WasmModule.cpp
--- a/js/src/wasm/WasmModule.cpp
+++ b/js/src/wasm/WasmModule.cpp
@@ -108,17 +108,17 @@ LinkDataTier::serializedSize() const
     return sizeof(pod()) +
            SerializedPodVectorSize(internalLinks) +
            symbolicLinks.serializedSize();
 }
 
 uint8_t*
 LinkDataTier::serialize(uint8_t* cursor) const
 {
-    MOZ_ASSERT(tier == Tier::Ion);
+    MOZ_ASSERT(tier == Tier::Serialized);
 
     cursor = WriteBytes(cursor, &pod(), sizeof(pod()));
     cursor = SerializePodVector(cursor, internalLinks);
     cursor = symbolicLinks.serialize(cursor);
     return cursor;
 }
 
 const uint8_t*
@@ -142,17 +142,16 @@ LinkData::tiers() const
 {
     return Tiers(tier_->tier);
 }
 
 const LinkDataTier&
 LinkData::linkData(Tier tier) const
 {
     switch (tier) {
-      case Tier::Debug:
       case Tier::Baseline:
         MOZ_RELEASE_ASSERT(tier_->tier == Tier::Baseline);
         return *tier_;
       case Tier::Ion:
         MOZ_RELEASE_ASSERT(tier_->tier == Tier::Ion);
         return *tier_;
       case Tier::TBD:
         return *tier_;
@@ -160,17 +159,16 @@ LinkData::linkData(Tier tier) const
         MOZ_CRASH();
     }
 }
 
 LinkDataTier&
 LinkData::linkData(Tier tier)
 {
     switch (tier) {
-      case Tier::Debug:
       case Tier::Baseline:
         MOZ_RELEASE_ASSERT(tier_->tier == Tier::Baseline);
         return *tier_;
       case Tier::Ion:
         MOZ_RELEASE_ASSERT(tier_->tier == Tier::Ion);
         return *tier_;
       case Tier::TBD:
         return *tier_;
@@ -297,17 +295,17 @@ Module::deserialize(const uint8_t* bytec
         memcpy(bytecode->bytes.begin(), bytecodeBegin, bytecodeSize);
 
     Assumptions assumptions;
     const uint8_t* cursor = assumptions.deserialize(compiledBegin, compiledSize);
     if (!cursor)
         return nullptr;
 
     LinkData linkData;
-    if (!linkData.initTier(Tier::Ion))
+    if (!linkData.initTier(Tier::Serialized))
         return nullptr;
 
     cursor = linkData.deserialize(cursor);
     if (!cursor)
         return nullptr;
 
     ImportVector imports;
     cursor = DeserializeVector(cursor, &imports);
diff --git a/js/src/wasm/WasmModule.h b/js/src/wasm/WasmModule.h
--- a/js/src/wasm/WasmModule.h
+++ b/js/src/wasm/WasmModule.h
@@ -42,21 +42,17 @@ struct LinkDataTierCacheablePod
 
     LinkDataTierCacheablePod() { mozilla::PodZero(this); }
 };
 
 struct LinkDataTier : LinkDataTierCacheablePod
 {
     const Tier tier;
 
-    explicit LinkDataTier(Tier tier)
-      : tier(tier)
-    {
-        MOZ_ASSERT(tier == Tier::Baseline || tier == Tier::Ion);
-    }
+    explicit LinkDataTier(Tier tier) : tier(tier) {}
 
     LinkDataTierCacheablePod& pod() { return *this; }
     const LinkDataTierCacheablePod& pod() const { return *this; }
 
     struct InternalLink {
         enum Kind {
             RawPointer,
             CodeLabel,
diff --git a/js/src/wasm/WasmTypes.h b/js/src/wasm/WasmTypes.h
--- a/js/src/wasm/WasmTypes.h
+++ b/js/src/wasm/WasmTypes.h
@@ -1183,42 +1183,39 @@ enum ModuleKind
 //
 // A tier value is used to request tier-variant aspects of code, metadata, or
 // linkdata.  The tiers are normally explicit (Baseline and Ion); implicit tiers
 // can be obtained through accessors on Code objects (eg, stableTier).
 
 enum class Tier
 {
     Baseline,
+    Debug = Baseline,
     Ion,
+    Serialized = Ion,
 
-    Debug,   // An alias for Baseline in calls to tier-variant accessors
-
-    TBD,     // A placeholder while tiering is being implemented
+    TBD      // A placeholder while tiering is being implemented
 };
 
 // Iterator over tiers present in a tiered data structure.
 
 class Tiers
 {
     Tier t_[2];
     uint32_t n_;
 
   public:
     explicit Tiers() {
         n_ = 0;
     }
     explicit Tiers(Tier t) {
-        MOZ_ASSERT(t == Tier::Baseline || t == Tier::Ion);
         t_[0] = t;
         n_ = 1;
     }
     explicit Tiers(Tier t, Tier u) {
-        MOZ_ASSERT(t == Tier::Baseline || t == Tier::Ion);
-        MOZ_ASSERT(u == Tier::Baseline || u == Tier::Ion);
         MOZ_ASSERT(t != u);
         t_[0] = t;
         t_[1] = u;
         n_ = 2;
     }
 
     Tier* begin() {
         return t_;
