# HG changeset patch
# User Ryan VanderMeulen <ryanvm@gmail.com>
# Date 1520897084 14400
# Node ID a7ab282e1d4a1aa1726017a05e04102c7adc9e33
# Parent  88fa51271b9d9dce6984be9182a3a0d3038f42f7
Bug 1424281 - Require Visual Studio 2017 15.6.0 and Win SDK 10.0.15063.0 to build on Windows. r=froydnj

diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -849,35 +849,25 @@ def compiler(language, host_or_target, c
 
         # If you want to bump the version check here search for
         # cxx_alignof above, and see the associated comment.
         if info.type == 'clang' and not info.version:
             raise FatalCheckError(
                 'Only clang/llvm 3.6 or newer is supported.')
 
         if info.type == 'msvc':
-            # 19.00 is VS2015.
-            # 19.10+ is VS2017+.
-            if info.version < '19.00.24213':
+            if info.version < '19.13.26128':
                 raise FatalCheckError(
                     'This version (%s) of the MSVC compiler is not '
                     'supported.\n'
-                    'You must install Visual C++ 2015 Update 3 or newer in '
+                    'You must install Visual C++ 2017 Update 6 or newer in '
                     'order to build.\n'
                     'See https://developer.mozilla.org/en/'
                     'Windows_Build_Prerequisites' % info.version)
 
-            if info.version >= '19.10' and info.version < '19.11.25506':
-                raise FatalCheckError(
-                    'This version (%s) of the MSVC compiler is not supported.\n'
-                    'You must install Visual C++ 2017 15.3 or newer in order '
-                    'to build.\n'
-                    'See https://developer.mozilla.org/en/'
-                    'Windows_Build_Prerequisites' % info.version)
-
         if info.flags:
             raise FatalCheckError(
                 'Unknown compiler or compiler not supported.')
 
         return namespace(
             wrapper=wrapper,
             compiler=compiler,
             flags=flags,
diff --git a/build/moz.configure/windows.configure b/build/moz.configure/windows.configure
--- a/build/moz.configure/windows.configure
+++ b/build/moz.configure/windows.configure
@@ -218,32 +218,32 @@ def valid_ucrt_sdk_dir(windows_sdk_dir, 
                 'CRT.' % windows_sdk_dir_env)
 
     valid_sdks = sorted(sdks, key=lambda x: sdks[x][0], reverse=True)
     if not valid_sdks:
         raise FatalCheckError('Cannot find the Universal CRT SDK. '
                               'Please install it.')
 
     version, sdk = sdks[valid_sdks[0]]
-    minimum_ucrt_version = Version('10.0.14393.0')
+    minimum_ucrt_version = Version('10.0.15063.0')
     if version < minimum_ucrt_version:
         raise FatalCheckError('Latest Universal CRT SDK version found %s'
                               ' and minimum required is %s. This or a later'
                               ' version can be installed using the Visual'
                               ' Studio installer.'
                               % (version, minimum_ucrt_version))
 
     broken_ucrt_version = Version('10.0.16299.0')
     if c_compiler.type == 'clang-cl' and version >= broken_ucrt_version:
         raise FatalCheckError('Found SDK version %s but clang-cl builds'
                               ' currently don\'t work with SDK version %s'
-                              ' and later. You should use an older SDK,'
-                              ' either by uninstalling the broken one or'
+                              ' and later. You should use version %s,'
+                              ' either by uninstalling the newer one or'
                               ' setting a custom WINDOWSSDKDIR.'
-                              % (version, broken_ucrt_version))
+                              % (version, broken_ucrt_version, minimum_ucrt_version))
 
     return namespace(
         path=sdk.path,
         include=sdk.include,
         lib=sdk.lib,
         version=version,
     )
 
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
@@ -235,16 +235,18 @@ def VS(version):
 
 
 VS_2013u2 = VS('18.00.30501')
 VS_2013u3 = VS('18.00.30723')
 VS_2015 = VS('19.00.23026')
 VS_2015u1 = VS('19.00.23506')
 VS_2015u2 = VS('19.00.23918')
 VS_2015u3 = VS('19.00.24213')
+VS_2017u4 = VS('19.11.25547')
+VS_2017u6 = VS('19.13.26128')
 
 VS_PLATFORM_X86 = {
     '_M_IX86': 600,
     '_WIN32': 1,
 }
 
 VS_PLATFORM_X86_64 = {
     '_M_X64': 100,
@@ -858,17 +860,19 @@ class WindowsToolchainTest(BaseToolchain
     # For the purpose of this test, it doesn't matter that the paths are not
     # real Windows paths.
     PATHS = {
         '/opt/VS_2013u2/bin/cl': VS_2013u2 + VS_PLATFORM_X86,
         '/opt/VS_2013u3/bin/cl': VS_2013u3 + VS_PLATFORM_X86,
         '/opt/VS_2015/bin/cl': VS_2015 + VS_PLATFORM_X86,
         '/opt/VS_2015u1/bin/cl': VS_2015u1 + VS_PLATFORM_X86,
         '/opt/VS_2015u2/bin/cl': VS_2015u2 + VS_PLATFORM_X86,
-        '/usr/bin/cl': VS_2015u3 + VS_PLATFORM_X86,
+        '/opt/VS_2015u3/bin/cl': VS_2015u3 + VS_PLATFORM_X86,
+        '/opt/VS_2017u4/bin/cl': VS_2017u4 + VS_PLATFORM_X86,
+        '/usr/bin/cl': VS_2017u6 + VS_PLATFORM_X86,
         '/usr/bin/clang-cl': CLANG_CL_3_9 + CLANG_CL_PLATFORM_X86,
         '/usr/bin/gcc': DEFAULT_GCC + GCC_PLATFORM_X86_WIN,
         '/usr/bin/g++': DEFAULT_GXX + GCC_PLATFORM_X86_WIN,
         '/usr/bin/gcc-4.9': GCC_4_9 + GCC_PLATFORM_X86_WIN,
         '/usr/bin/g++-4.9': GXX_4_9 + GCC_PLATFORM_X86_WIN,
         '/usr/bin/gcc-5': GCC_5 + GCC_PLATFORM_X86_WIN,
         '/usr/bin/g++-5': GXX_5 + GCC_PLATFORM_X86_WIN,
         '/usr/bin/gcc-6': GCC_6 + GCC_PLATFORM_X86_WIN,
@@ -878,44 +882,52 @@ class WindowsToolchainTest(BaseToolchain
         '/usr/bin/clang-3.6': CLANG_3_6 + CLANG_PLATFORM_X86_WIN,
         '/usr/bin/clang++-3.6': CLANGXX_3_6 + CLANG_PLATFORM_X86_WIN,
         '/usr/bin/clang-3.3': CLANG_3_3 + CLANG_PLATFORM_X86_WIN,
         '/usr/bin/clang++-3.3': CLANGXX_3_3 + CLANG_PLATFORM_X86_WIN,
     }
 
     VS_2013u2_RESULT = (
         'This version (18.00.30501) of the MSVC compiler is not supported.\n'
-        'You must install Visual C++ 2015 Update 3 or newer in order to build.\n'
+        'You must install Visual C++ 2017 Update 6 or newer in order to build.\n'
         'See https://developer.mozilla.org/en/Windows_Build_Prerequisites')
     VS_2013u3_RESULT = (
         'This version (18.00.30723) of the MSVC compiler is not supported.\n'
-        'You must install Visual C++ 2015 Update 3 or newer in order to build.\n'
+        'You must install Visual C++ 2017 Update 6 or newer in order to build.\n'
         'See https://developer.mozilla.org/en/Windows_Build_Prerequisites')
     VS_2015_RESULT = (
         'This version (19.00.23026) of the MSVC compiler is not supported.\n'
-        'You must install Visual C++ 2015 Update 3 or newer in order to build.\n'
+        'You must install Visual C++ 2017 Update 6 or newer in order to build.\n'
         'See https://developer.mozilla.org/en/Windows_Build_Prerequisites')
     VS_2015u1_RESULT = (
         'This version (19.00.23506) of the MSVC compiler is not supported.\n'
-        'You must install Visual C++ 2015 Update 3 or newer in order to build.\n'
+        'You must install Visual C++ 2017 Update 6 or newer in order to build.\n'
         'See https://developer.mozilla.org/en/Windows_Build_Prerequisites')
     VS_2015u2_RESULT = (
         'This version (19.00.23918) of the MSVC compiler is not supported.\n'
-        'You must install Visual C++ 2015 Update 3 or newer in order to build.\n'
+        'You must install Visual C++ 2017 Update 6 or newer in order to build.\n'
+        'See https://developer.mozilla.org/en/Windows_Build_Prerequisites')
+    VS_2015u3_RESULT = (
+        'This version (19.00.24213) of the MSVC compiler is not supported.\n'
+        'You must install Visual C++ 2017 Update 6 or newer in order to build.\n'
         'See https://developer.mozilla.org/en/Windows_Build_Prerequisites')
-    VS_2015u3_RESULT = CompilerResult(
+    VS_2017u4_RESULT = (
+        'This version (19.11.25547) of the MSVC compiler is not supported.\n'
+        'You must install Visual C++ 2017 Update 6 or newer in order to build.\n'
+        'See https://developer.mozilla.org/en/Windows_Build_Prerequisites')
+    VS_2017u6_RESULT = CompilerResult(
         flags=[],
-        version='19.00.24213',
+        version='19.13.26128',
         type='msvc',
         compiler='/usr/bin/cl',
         language='C',
     )
-    VSXX_2015u3_RESULT = CompilerResult(
+    VSXX_2017u6_RESULT = CompilerResult(
         flags=[],
-        version='19.00.24213',
+        version='19.13.26128',
         type='msvc',
         compiler='/usr/bin/cl',
         language='C++',
     )
     CLANG_CL_3_9_RESULT = CompilerResult(
         flags=['-Xclang', '-std=gnu99',
                '-fms-compatibility-version=19.13.26128'],
         version='19.13.26128',
@@ -939,25 +951,37 @@ class WindowsToolchainTest(BaseToolchain
     GXX_4_9_RESULT = LinuxToolchainTest.GXX_4_9_RESULT
     GCC_5_RESULT = LinuxToolchainTest.GCC_5_RESULT
     GXX_5_RESULT = LinuxToolchainTest.GXX_5_RESULT
     GCC_6_RESULT = LinuxToolchainTest.GCC_6_RESULT
     GXX_6_RESULT = LinuxToolchainTest.GXX_6_RESULT
     DEFAULT_GCC_RESULT = LinuxToolchainTest.DEFAULT_GCC_RESULT
     DEFAULT_GXX_RESULT = LinuxToolchainTest.DEFAULT_GXX_RESULT
 
-    # VS2015u3 or greater is required.
+    # VS2017u6 or greater is required.
     def test_msvc(self):
         self.do_toolchain_test(self.PATHS, {
-            'c_compiler': self.VS_2015u3_RESULT,
-            'cxx_compiler': self.VSXX_2015u3_RESULT,
+            'c_compiler': self.VS_2017u6_RESULT,
+            'cxx_compiler': self.VSXX_2017u6_RESULT,
         })
 
     def test_unsupported_msvc(self):
         self.do_toolchain_test(self.PATHS, {
+            'c_compiler': self.VS_2017u4_RESULT,
+        }, environ={
+            'CC': '/opt/VS_2017u4/bin/cl',
+        })
+
+        self.do_toolchain_test(self.PATHS, {
+            'c_compiler': self.VS_2015u3_RESULT,
+        }, environ={
+            'CC': '/opt/VS_2015u3/bin/cl',
+        })
+
+        self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.VS_2015u2_RESULT,
         }, environ={
             'CC': '/opt/VS_2015u2/bin/cl',
         })
 
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.VS_2015u1_RESULT,
         }, environ={
@@ -1030,17 +1054,17 @@ class WindowsToolchainTest(BaseToolchain
             'cxx_compiler': self.CLANGXX_3_3_RESULT,
         }, environ={
             'CC': 'clang-3.3',
             'CXX': 'clang++-3.3',
         })
 
     def test_cannot_cross(self):
         paths = {
-            '/usr/bin/cl': VS_2015u3 + VS_PLATFORM_X86_64,
+            '/usr/bin/cl': VS_2017u6 + VS_PLATFORM_X86_64,
         }
         self.do_toolchain_test(paths, {
             'c_compiler': ('Target C compiler target CPU (x86_64) '
                            'does not match --target CPU (i686)'),
         })
 
 
 class Windows64ToolchainTest(WindowsToolchainTest):
@@ -1049,17 +1073,19 @@ class Windows64ToolchainTest(WindowsTool
     # For the purpose of this test, it doesn't matter that the paths are not
     # real Windows paths.
     PATHS = {
         '/opt/VS_2013u2/bin/cl': VS_2013u2 + VS_PLATFORM_X86_64,
         '/opt/VS_2013u3/bin/cl': VS_2013u3 + VS_PLATFORM_X86_64,
         '/opt/VS_2015/bin/cl': VS_2015 + VS_PLATFORM_X86_64,
         '/opt/VS_2015u1/bin/cl': VS_2015u1 + VS_PLATFORM_X86_64,
         '/opt/VS_2015u2/bin/cl': VS_2015u2 + VS_PLATFORM_X86_64,
-        '/usr/bin/cl': VS_2015u3 + VS_PLATFORM_X86_64,
+        '/opt/VS_2015u3/bin/cl': VS_2015u3 + VS_PLATFORM_X86_64,
+        '/opt/VS_2017u4/bin/cl': VS_2017u4 + VS_PLATFORM_X86_64,
+        '/usr/bin/cl': VS_2017u6 + VS_PLATFORM_X86_64,
         '/usr/bin/clang-cl': CLANG_CL_3_9 + CLANG_CL_PLATFORM_X86_64,
         '/usr/bin/gcc': DEFAULT_GCC + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/g++': DEFAULT_GXX + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/gcc-4.9': GCC_4_9 + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/g++-4.9': GXX_4_9 + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/gcc-5': GCC_5 + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/g++-5': GXX_5 + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/gcc-6': GCC_6 + GCC_PLATFORM_X86_64_WIN,
@@ -1071,17 +1097,17 @@ class Windows64ToolchainTest(WindowsTool
         '/usr/bin/clang-3.6': CLANG_3_6 + CLANG_PLATFORM_X86_64_WIN,
         '/usr/bin/clang++-3.6': CLANGXX_3_6 + CLANG_PLATFORM_X86_64_WIN,
         '/usr/bin/clang-3.3': CLANG_3_3 + CLANG_PLATFORM_X86_64_WIN,
         '/usr/bin/clang++-3.3': CLANGXX_3_3 + CLANG_PLATFORM_X86_64_WIN,
     }
 
     def test_cannot_cross(self):
         paths = {
-            '/usr/bin/cl': VS_2015u3 + VS_PLATFORM_X86,
+            '/usr/bin/cl': VS_2017u6 + VS_PLATFORM_X86,
         }
         self.do_toolchain_test(paths, {
             'c_compiler': ('Target C compiler target CPU (x86) '
                            'does not match --target CPU (x86_64)'),
         })
 
 
 class LinuxCrossCompileToolchainTest(BaseToolchainTest):
