# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1509226738 14400
# Node ID 2a03305f96d7dee83ce28b5768848bf22697e445
# Parent  0833d862555f0f85e1d42d15abcfb6a039210312
Bug 1163171 - part 4 - fix jsapi-tests link errors with __atomic_* intrinsics on x86/Android with clang; r=glandium

NDK r15+ clang changed the code generation strategy for the __atomic_*
intrinics such that using them with 64-bit types now requires linking
with libatomic.  Our current configure tests for libatomic doesn't catch
this, because the std::atomic implementation is such that it doesn't
require an external library, even for 64-bit types, whereas the
__atomic_* intrinsics do.  The safest thing to do is to force this
configure check to always return true when we are compiling for
x86/Android with clang.

diff --git a/build/autoconf/toolchain.m4 b/build/autoconf/toolchain.m4
--- a/build/autoconf/toolchain.m4
+++ b/build/autoconf/toolchain.m4
@@ -90,31 +90,41 @@ PATH=$_SAVE_PATH
 AC_DEFUN([MOZ_CXX11],
 [
 dnl Updates to the test below should be duplicated further below for the
 dnl cross-compiling case.
 AC_LANG_CPLUSPLUS
 if test "$GNU_CXX"; then
     AC_CACHE_CHECK([whether 64-bits std::atomic requires -latomic],
         ac_cv_needs_atomic,
-        AC_TRY_LINK(
-            [#include <cstdint>
-             #include <atomic>],
-            [ std::atomic<uint64_t> foo; foo = 1; ],
-            ac_cv_needs_atomic=no,
-            _SAVE_LIBS="$LIBS"
-            LIBS="$LIBS -latomic"
+        dnl x86 with clang is a little peculiar.  std::atomic does not require
+        dnl linking with libatomic, but using atomic intrinsics does, so we
+        dnl force the setting on for such systems.  (This setting is probably
+        dnl applicable to all x86 systems using clang, but Android is currently
+        dnl the one we care most about, and nobody has reported problems on
+        dnl other platforms before this point.)
+        if test "$CC_TYPE" = "clang" -a "$CPU_ARCH" = "x86" -a "$OS_TARGET" = "Android"; then
+            ac_cv_needs_atomic=yes
+        else
             AC_TRY_LINK(
                 [#include <cstdint>
                  #include <atomic>],
                 [ std::atomic<uint64_t> foo; foo = 1; ],
-                ac_cv_needs_atomic=yes,
-                ac_cv_needs_atomic="do not know; assuming no")
-            LIBS="$_SAVE_LIBS"
-        )
+                ac_cv_needs_atomic=no,
+                _SAVE_LIBS="$LIBS"
+                LIBS="$LIBS -latomic"
+                AC_TRY_LINK(
+                    [#include <cstdint>
+                     #include <atomic>],
+                    [ std::atomic<uint64_t> foo; foo = 1; ],
+                    ac_cv_needs_atomic=yes,
+                    ac_cv_needs_atomic="do not know; assuming no")
+                LIBS="$_SAVE_LIBS"
+            )
+        fi
     )
     if test "$ac_cv_needs_atomic" = yes; then
       MOZ_NEEDS_LIBATOMIC=1
     else
       MOZ_NEEDS_LIBATOMIC=
     fi
     AC_SUBST(MOZ_NEEDS_LIBATOMIC)
 fi
diff --git a/js/src/jsapi-tests/moz.build b/js/src/jsapi-tests/moz.build
--- a/js/src/jsapi-tests/moz.build
+++ b/js/src/jsapi-tests/moz.build
@@ -136,16 +136,19 @@ LOCAL_INCLUDES += [
     '!..',
     '..',
 ]
 
 USE_LIBS += [
     'static:js',
 ]
 
+if CONFIG['MOZ_NEEDS_LIBATOMIC']:
+    OS_LIBS += ['atomic']
+
 OS_LIBS += CONFIG['MOZ_ZLIB_LIBS']
 
 if CONFIG['CC_TYPE'] in ('clang', 'gcc'):
     CXXFLAGS += ['-Wno-shadow', '-Werror=format', '-fno-strict-aliasing']
 
 # This is intended as a temporary workaround to enable VS2015.
 if CONFIG['CC_TYPE'] in ('msvc', 'clang-cl'):
     CXXFLAGS += ['-wd4312']
