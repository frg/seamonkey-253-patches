# HG changeset patch
# User James Cheng <jacheng@mozilla.com>
# Date 1506937140 -28800
# Node ID 6e33c46788b4bbeb647ed4ecde47f149674c215f
# Parent  56c9056890ef74fb44313df8d9661daa4062247c
Bug 1404230 - Part2 - Add a web api to support HDCP policy check on MediaKeys. r=cpearce,smaug

MozReview-Commit-ID: HW7DtQ6GuOa

diff --git a/dom/media/eme/MediaKeys.cpp b/dom/media/eme/MediaKeys.cpp
--- a/dom/media/eme/MediaKeys.cpp
+++ b/dom/media/eme/MediaKeys.cpp
@@ -595,10 +595,42 @@ MediaKeys::GetSessionsInfo(nsString& ses
       sessionsInfo.Append(
         NS_ConvertUTF8toUTF16((nsDependentCString(keyStatus))));
       sessionsInfo.AppendLiteral(")");
     }
     sessionsInfo.AppendLiteral(")");
   }
 }
 
+already_AddRefed<Promise>
+MediaKeys::GetStatusForPolicy(const MediaKeysPolicy& aPolicy,
+                              ErrorResult& aRv)
+{
+  RefPtr<DetailedPromise> promise(MakePromise(aRv,
+    NS_LITERAL_CSTRING("MediaKeys::GetStatusForPolicy()")));
+  if (aRv.Failed()) {
+    return nullptr;
+  }
+
+  // Currently, only widevine CDM supports for this API.
+  if (!IsWidevineKeySystem(mKeySystem)) {
+    EME_LOG("MediaKeys[%p]::GetStatusForPolicy() HDCP policy check on unsupported keysystem ", this);
+    NS_WARNING("Tried to query without a CDM");
+    promise->MaybeReject(NS_ERROR_DOM_NOT_SUPPORTED_ERR,
+                         NS_LITERAL_CSTRING("HDCP policy check on unsupported keysystem"));
+    return promise.forget();
+  }
+
+  if (!mProxy) {
+   NS_WARNING("Tried to use a MediaKeys without a CDM");
+   promise->MaybeReject(NS_ERROR_DOM_INVALID_STATE_ERR,
+                        NS_LITERAL_CSTRING("Null CDM in MediaKeys.GetStatusForPolicy()"));
+   return promise.forget();
+  }
+
+  EME_LOG("GetStatusForPolicy minHdcpVersion = %s.", NS_ConvertUTF16toUTF8(aPolicy.mMinHdcpVersion).get());
+  // TODO: Ask CDM to get the real policy.
+  promise->MaybeResolve(MediaKeyStatus::Usable);
+  return promise.forget();
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/media/eme/MediaKeys.h b/dom/media/eme/MediaKeys.h
--- a/dom/media/eme/MediaKeys.h
+++ b/dom/media/eme/MediaKeys.h
@@ -30,16 +30,17 @@ namespace dom {
 class MediaKeys;
 } // namespace dom
 DDLoggedTypeName(dom::MediaKeys);
 
 namespace dom {
 
 class ArrayBufferViewOrArrayBuffer;
 class MediaKeySession;
+struct MediaKeysPolicy;
 class HTMLMediaElement;
 
 typedef nsRefPtrHashtable<nsStringHashKey, MediaKeySession> KeySessionHashMap;
 typedef nsRefPtrHashtable<nsUint32HashKey, dom::DetailedPromise> PromiseHashMap;
 typedef nsRefPtrHashtable<nsUint32HashKey, MediaKeySession> PendingKeySessionsHashMap;
 typedef nsDataHashtable<nsUint32HashKey, uint32_t> PendingPromiseIdTokenHashMap;
 typedef uint32_t PromiseId;
 
@@ -133,16 +134,20 @@ public:
   // Shutdown which is called from the script/dom side.
   void Terminated();
 
   // Returns true if this MediaKeys has been bound to a media element.
   bool IsBoundToMediaElement() const;
 
   void GetSessionsInfo(nsString& sessionsInfo);
 
+  // JavaScript: MediaKeys.GetStatusForPolicy()
+  already_AddRefed<Promise> GetStatusForPolicy(const MediaKeysPolicy& aPolicy,
+                                               ErrorResult& aR);
+
 private:
 
   // Instantiate CDMProxy instance.
   // It could be MediaDrmCDMProxy (Widevine on Fennec) or ChromiumCDMProxy (the rest).
   already_AddRefed<CDMProxy> CreateCDMProxy(nsIEventTarget* aMainThread);
 
   // Removes promise from mPromises, and returns it.
   already_AddRefed<DetailedPromise> RetrievePromise(PromiseId aId);
diff --git a/dom/webidl/MediaKeys.webidl b/dom/webidl/MediaKeys.webidl
--- a/dom/webidl/MediaKeys.webidl
+++ b/dom/webidl/MediaKeys.webidl
@@ -13,17 +13,25 @@
 // Note: "persistent-usage-record" session type is unsupported yet, as
 // it's marked as "at risk" in the spec, and Chrome doesn't support it. 
 enum MediaKeySessionType {
   "temporary",
   "persistent-license",
   // persistent-usage-record,
 };
 
+// https://github.com/WICG/media-capabilities/blob/master/eme-extension-policy-check.md
+dictionary MediaKeysPolicy {
+  DOMString minHdcpVersion = "";
+};
+
 interface MediaKeys {
   readonly attribute DOMString keySystem;
 
   [NewObject, Throws]
   MediaKeySession createSession(optional MediaKeySessionType sessionType = "temporary");
 
   [NewObject]
   Promise<void> setServerCertificate(BufferSource serverCertificate);
+
+  [Pref="media.eme.hdcp-policy-check.enabled", NewObject]
+  Promise<MediaKeyStatus> getStatusForPolicy(optional MediaKeysPolicy policy);
 };
