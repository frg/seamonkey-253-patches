# HG changeset patch
# User Eric Rahm <erahm@mozilla.com>
# Date 1524788062 25200
#      Thu Apr 26 17:14:22 2018 -0700
# Node ID 539d7238171dd4dd23246877deed0126ec544901
# Parent  8e661bd14a9ae4776fc91aea55f2f5c82e8e0a6a
Bug 1364624 - Part 1: Manually manage locks in MessageChannel::Close. r=froydnj, a=jcristau

This switches over to manually managing the locking in MessageChannel::Close
in order to avoid a deadlock on msvc opt builds. It has the added benefit of
avoid a superfluous lock/unlock pair.

diff --git a/ipc/glue/MessageChannel.cpp b/ipc/glue/MessageChannel.cpp
--- a/ipc/glue/MessageChannel.cpp
+++ b/ipc/glue/MessageChannel.cpp
@@ -8,16 +8,17 @@
 #include "mozilla/ipc/MessageChannel.h"
 
 #include "mozilla/Assertions.h"
 #include "mozilla/DebugOnly.h"
 #include "mozilla/dom/ScriptSettings.h"
 #include "mozilla/ipc/ProtocolUtils.h"
 #include "mozilla/Logging.h"
 #include "mozilla/Move.h"
+#include "mozilla/ScopeExit.h"
 #include "mozilla/Sprintf.h"
 #include "mozilla/Telemetry.h"
 #include "mozilla/TimeStamp.h"
 #include "nsAppRunner.h"
 #include "nsAutoPtr.h"
 #include "nsDebug.h"
 #include "nsISupportsImpl.h"
 #include "nsContentUtils.h"
@@ -2657,26 +2658,36 @@ MessageChannel::CloseWithTimeout()
 }
 
 void
 MessageChannel::Close()
 {
     AssertWorkerThread();
 
     {
-        MonitorAutoLock lock(*mMonitor);
+        // We don't use MonitorAutoLock here as that causes some sort of
+        // deadlock in the error/timeout-with-a-listener state below when
+        // compiling an optimized msvc build.
+        mMonitor->Lock();
+
+        // Instead just use a ScopeExit to manage the unlock.
+        RefPtr<RefCountedMonitor> monitor(mMonitor);
+        auto exit = MakeScopeExit([m = Move(monitor)] () {
+          m->Unlock();
+        });
 
         if (ChannelError == mChannelState || ChannelTimeout == mChannelState) {
             // See bug 538586: if the listener gets deleted while the
             // IO thread's NotifyChannelError event is still enqueued
             // and subsequently deletes us, then the error event will
             // also be deleted and the listener will never be notified
             // of the channel error.
             if (mListener) {
-                MonitorAutoUnlock unlock(*mMonitor);
+                exit.release(); // Explicitly unlocking, clear scope exit.
+                mMonitor->Unlock();
                 NotifyMaybeChannelError();
             }
             return;
         }
 
         if (ChannelOpening == mChannelState) {
             // SynchronouslyClose() waits for an ack from the other side, so
             // the opening sequence should complete before this returns.
