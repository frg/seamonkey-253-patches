# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1505477171 14400
#      Fri Sep 15 08:06:11 2017 -0400
# Node ID 1607f6d53f5273361ce35e47f94d3966c26611c6
# Parent  81bcc73b3bad3211147a8c8e0ff790d1ab666eef
Bug 1397823 - part 1 - move work into _generateMessageConstructor; r=kanru

_generateMessageConstructor takes a lot of `md.FOO`-style parameters,
which could be derived inside the function by simply passing `md`.
Especially with the upcoming changes to calculate things like reply-ness
of messages, sync-ness, etc, we'd be wanting to pass even more
parameters like `md.FOO`.  So let's just pass `md` in, and then we can
make all the necessary future changes in a single place.

diff --git a/ipc/ipdl/ipdl/lower.py b/ipc/ipdl/ipdl/lower.py
--- a/ipc/ipdl/ipdl/lower.py
+++ b/ipc/ipdl/ipdl/lower.py
@@ -1592,34 +1592,24 @@ class _GenerateProtocolCode(ipdl.ast.Vis
 
             # Look up the segment capacity used for serializing this
             # message. If the capacity is not specified, use '0' for
             # the default capacity (defined in ipc_message.cc)
             name = '%s::%s' % (md.namespace, md.decl.progname)
             segmentcapacity = self.segmentcapacitydict.get(name, 0)
 
             mfDecl, mfDefn = _splitFuncDeclDefn(
-                _generateMessageConstructor(md.msgCtorFunc(), md.msgId(),
-                                            segmentcapacity,
-                                            md.decl.type.nested,
-                                            md.decl.type.prio,
-                                            md.prettyMsgName(p.name+'::'),
-                                            md.decl.type.compress))
+                _generateMessageConstructor(md, segmentcapacity, p,
+                                            forReply=False))
             decls.append(mfDecl)
             self.funcDefns.append(mfDefn)
 
             if md.hasReply():
                 rfDecl, rfDefn = _splitFuncDeclDefn(
-                    _generateMessageConstructor(
-                        md.replyCtorFunc(), md.replyId(),
-                        0,
-                        md.decl.type.nested,
-                        md.decl.type.prio,
-                        md.prettyReplyName(p.name+'::'),
-                        md.decl.type.compress))
+                    _generateMessageConstructor(md, 0, p, forReply=True))
                 decls.append(rfDecl)
                 self.funcDefns.append(rfDefn)
 
             decls.append(Whitespace.NL)
             ns.addstmts(decls)
 
         ns.addstmts([ Whitespace.NL, Whitespace.NL ])
 
@@ -1706,17 +1696,30 @@ class _GenerateProtocolCode(ipdl.ast.Vis
         fromswitch.addcase(DefaultLabel(), unreachedblock)
 
         transitionfunc.addstmt(fromswitch)
 
         return transitionfunc
 
 ##--------------------------------------------------
 
-def _generateMessageConstructor(clsname, msgid, segmentSize, nested, prio, prettyName, compress):
+def _generateMessageConstructor(md, segmentSize, protocol, forReply=False):
+    if forReply:
+        clsname = md.replyCtorFunc()
+        msgid = md.replyId()
+        prettyName = md.prettyReplyName(protocol.name+'::')
+    else:
+        clsname = md.msgCtorFunc()
+        msgid = md.msgId()
+        prettyName = md.prettyMsgName(protocol.name+'::')
+
+    nested = md.decl.type.nested
+    prio = md.decl.type.prio
+    compress = md.decl.type.compress
+
     routingId = ExprVar('routingId')
 
     func = FunctionDefn(FunctionDecl(
         clsname,
         params=[ Decl(Type('int32_t'), routingId.name) ],
         ret=Type('IPC::Message', ptr=1)))
 
     if compress == 'compress':
