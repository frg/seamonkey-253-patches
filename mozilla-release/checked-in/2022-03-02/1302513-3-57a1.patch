# HG changeset patch
# User Brad Werth <bwerth@mozilla.com>
# Date 1500671925 25200
# Node ID 336a4f9435ce666c26ca7f75da4115523e9f35de
# Parent  cf2bc277cf0507d1083f7b75e692b5930c0bb175
Bug 1302513 Part 3: Remove declarations and implementations of getAuthoredPropertyValue. r=dholbert

MozReview-Commit-ID: LxlUfhPJUaa

diff --git a/layout/style/Declaration.cpp b/layout/style/Declaration.cpp
--- a/layout/style/Declaration.cpp
+++ b/layout/style/Declaration.cpp
@@ -181,27 +181,16 @@ Declaration::GetPropertyValue(const nsAS
 
 void
 Declaration::GetPropertyValueByID(nsCSSPropertyID aPropID,
                                   nsAString& aValue) const
 {
   GetPropertyValueInternal(aPropID, aValue, nsCSSValue::eNormalized);
 }
 
-void
-Declaration::GetAuthoredPropertyValue(const nsAString& aProperty,
-                                      nsAString& aValue) const
-{
-  DispatchPropertyOperation(aProperty,
-    [&](nsCSSPropertyID propID) {
-      GetPropertyValueInternal(propID, aValue, nsCSSValue::eAuthorSpecified);
-    },
-    [&](const nsAString& name) { GetVariableValue(name, aValue); });
-}
-
 bool
 Declaration::GetPropertyIsImportant(const nsAString& aProperty) const
 {
   bool r = false;
   DispatchPropertyOperation(aProperty,
     [&](nsCSSPropertyID propID) { r = GetPropertyIsImportantByID(propID); },
     [&](const nsAString& name) { r = GetVariableIsImportant(name); });
   return r;
diff --git a/layout/style/Declaration.h b/layout/style/Declaration.h
--- a/layout/style/Declaration.h
+++ b/layout/style/Declaration.h
@@ -120,18 +120,16 @@ public:
    * |ValueAppended| must be called to maintain this declaration's
    * |mOrder| whenever a property is parsed into an expanded data block
    * for this declaration.  aProperty must not be a shorthand.
    */
   void ValueAppended(nsCSSPropertyID aProperty);
 
   void GetPropertyValue(const nsAString& aProperty, nsAString& aValue) const;
   void GetPropertyValueByID(nsCSSPropertyID aPropID, nsAString& aValue) const;
-  void GetAuthoredPropertyValue(const nsAString& aProperty,
-                                nsAString& aValue) const;
   bool GetPropertyIsImportant(const nsAString& aProperty) const;
   void RemoveProperty(const nsAString& aProperty);
   void RemovePropertyByID(nsCSSPropertyID aProperty);
 
   bool HasProperty(nsCSSPropertyID aProperty) const;
 
   bool HasImportantData() const {
     return mImportantData || mImportantVariables;
diff --git a/layout/style/DeclarationBlock.h b/layout/style/DeclarationBlock.h
--- a/layout/style/DeclarationBlock.h
+++ b/layout/style/DeclarationBlock.h
@@ -125,18 +125,16 @@ public:
 
   inline uint32_t Count() const;
   inline bool GetNthProperty(uint32_t aIndex, nsAString& aReturn) const;
 
   inline void GetPropertyValue(const nsAString& aProperty,
                                nsAString& aValue) const;
   inline void GetPropertyValueByID(nsCSSPropertyID aPropID,
                                    nsAString& aValue) const;
-  inline void GetAuthoredPropertyValue(const nsAString& aProperty,
-                                       nsAString& aValue) const;
   inline bool GetPropertyIsImportant(const nsAString& aProperty) const;
   inline void RemoveProperty(const nsAString& aProperty);
   inline void RemovePropertyByID(nsCSSPropertyID aProperty);
 
 private:
   union {
     // We only ever have one of these since we have an
     // nsHTMLCSSStyleSheet only for style attributes, and style
diff --git a/layout/style/DeclarationBlockInlines.h b/layout/style/DeclarationBlockInlines.h
--- a/layout/style/DeclarationBlockInlines.h
+++ b/layout/style/DeclarationBlockInlines.h
@@ -79,23 +79,16 @@ DeclarationBlock::GetPropertyValue(const
 
 void
 DeclarationBlock::GetPropertyValueByID(nsCSSPropertyID aPropID,
                                        nsAString& aValue) const
 {
   MOZ_STYLO_FORWARD(GetPropertyValueByID, (aPropID, aValue))
 }
 
-void
-DeclarationBlock::GetAuthoredPropertyValue(const nsAString& aProperty,
-                                           nsAString& aValue) const
-{
-  MOZ_STYLO_FORWARD(GetAuthoredPropertyValue, (aProperty, aValue))
-}
-
 bool
 DeclarationBlock::GetPropertyIsImportant(const nsAString& aProperty) const
 {
   MOZ_STYLO_FORWARD(GetPropertyIsImportant, (aProperty))
 }
 
 void
 DeclarationBlock::RemoveProperty(const nsAString& aProperty)
diff --git a/layout/style/ServoDeclarationBlock.h b/layout/style/ServoDeclarationBlock.h
--- a/layout/style/ServoDeclarationBlock.h
+++ b/layout/style/ServoDeclarationBlock.h
@@ -59,20 +59,16 @@ public:
   }
   bool GetNthProperty(uint32_t aIndex, nsAString& aReturn) const {
     aReturn.Truncate();
     return Servo_DeclarationBlock_GetNthProperty(mRaw, aIndex, &aReturn);
   }
 
   void GetPropertyValue(const nsAString& aProperty, nsAString& aValue) const;
   void GetPropertyValueByID(nsCSSPropertyID aPropID, nsAString& aValue) const;
-  void GetAuthoredPropertyValue(const nsAString& aProperty,
-                                nsAString& aValue) const {
-    GetPropertyValue(aProperty, aValue);
-  }
   bool GetPropertyIsImportant(const nsAString& aProperty) const;
   void RemoveProperty(const nsAString& aProperty);
   void RemovePropertyByID(nsCSSPropertyID aPropID);
 
 private:
   ~ServoDeclarationBlock() {}
 
   RefPtr<RawServoDeclarationBlock> mRaw;
diff --git a/layout/style/nsCSSRules.cpp b/layout/style/nsCSSRules.cpp
--- a/layout/style/nsCSSRules.cpp
+++ b/layout/style/nsCSSRules.cpp
@@ -947,25 +947,16 @@ nsCSSFontFaceStyleDecl::SetCssText(const
 
 NS_IMETHODIMP
 nsCSSFontFaceStyleDecl::GetPropertyValue(const nsAString & propertyName,
                                          nsAString & aResult)
 {
   return GetPropertyValue(nsCSSProps::LookupFontDesc(propertyName), aResult);
 }
 
-NS_IMETHODIMP
-nsCSSFontFaceStyleDecl::GetAuthoredPropertyValue(const nsAString& propertyName,
-                                                 nsAString& aResult)
-{
-  // We don't return any authored property values different from
-  // GetPropertyValue, currently.
-  return GetPropertyValue(nsCSSProps::LookupFontDesc(propertyName), aResult);
-}
-
 already_AddRefed<dom::CSSValue>
 nsCSSFontFaceStyleDecl::GetPropertyCSSValue(const nsAString & propertyName,
                                             ErrorResult& aRv)
 {
   // ??? nsDOMCSSDeclaration returns null/NS_OK, but that seems wrong.
   aRv.Throw(NS_ERROR_NOT_IMPLEMENTED);
   return nullptr;
 }
diff --git a/layout/style/nsComputedDOMStyle.cpp b/layout/style/nsComputedDOMStyle.cpp
--- a/layout/style/nsComputedDOMStyle.cpp
+++ b/layout/style/nsComputedDOMStyle.cpp
@@ -390,25 +390,16 @@ nsComputedDOMStyle::GetPropertyValue(con
     val->GetCssText(text, error);
     aReturn.Assign(text);
     return error.StealNSResult();
   }
 
   return NS_OK;
 }
 
-NS_IMETHODIMP
-nsComputedDOMStyle::GetAuthoredPropertyValue(const nsAString& aPropertyName,
-                                             nsAString& aReturn)
-{
-  // Authored style doesn't make sense to return from computed DOM style,
-  // so just return whatever GetPropertyValue() returns.
-  return GetPropertyValue(aPropertyName, aReturn);
-}
-
 /* static */
 already_AddRefed<nsStyleContext>
 nsComputedDOMStyle::GetStyleContext(Element* aElement,
                                     nsIAtom* aPseudo,
                                     nsIPresShell* aPresShell,
                                     StyleType aStyleType)
 {
   // If the content has a pres shell, we must use it.  Otherwise we'd
diff --git a/layout/style/nsDOMCSSDeclaration.cpp b/layout/style/nsDOMCSSDeclaration.cpp
--- a/layout/style/nsDOMCSSDeclaration.cpp
+++ b/layout/style/nsDOMCSSDeclaration.cpp
@@ -190,26 +190,16 @@ nsDOMCSSDeclaration::GetPropertyValue(co
   aReturn.Truncate();
   if (DeclarationBlock* decl = GetCSSDeclaration(eOperation_Read)) {
     decl->GetPropertyValue(aPropertyName, aReturn);
   }
   return NS_OK;
 }
 
 NS_IMETHODIMP
-nsDOMCSSDeclaration::GetAuthoredPropertyValue(const nsAString& aPropertyName,
-                                              nsAString& aReturn)
-{
-  if (DeclarationBlock* decl = GetCSSDeclaration(eOperation_Read)) {
-    decl->GetAuthoredPropertyValue(aPropertyName, aReturn);
-  }
-  return NS_OK;
-}
-
-NS_IMETHODIMP
 nsDOMCSSDeclaration::GetPropertyPriority(const nsAString& aPropertyName,
                                          nsAString& aReturn)
 {
   DeclarationBlock* decl = GetCSSDeclaration(eOperation_Read);
 
   aReturn.Truncate();
   if (decl && decl->GetPropertyIsImportant(aPropertyName)) {
     aReturn.AssignLiteral("important");
diff --git a/layout/style/nsICSSDeclaration.h b/layout/style/nsICSSDeclaration.h
--- a/layout/style/nsICSSDeclaration.h
+++ b/layout/style/nsICSSDeclaration.h
@@ -45,19 +45,16 @@ public:
 
   /**
    * Method analogous to nsIDOMCSSStyleDeclaration::GetPropertyValue,
    * which obeys all the same restrictions.
    */
   NS_IMETHOD GetPropertyValue(const nsCSSPropertyID aPropID,
                               nsAString& aValue) = 0;
 
-  NS_IMETHOD GetAuthoredPropertyValue(const nsAString& aPropName,
-                                      nsAString& aValue) = 0;
-
   /**
    * Method analogous to nsIDOMCSSStyleDeclaration::SetProperty.  This
    * method does NOT allow setting a priority (the priority will
    * always be set to default priority).
    */
   NS_IMETHOD SetPropertyValue(const nsCSSPropertyID aPropID,
                               const nsAString& aValue) = 0;
 
@@ -123,20 +120,16 @@ public:
 
   // The actual implementation of the Item method and the WebIDL indexed getter
   virtual void IndexedGetter(uint32_t aIndex, bool& aFound, nsAString& aPropName) = 0;
 
   void GetPropertyValue(const nsAString& aPropName, nsString& aValue,
                         mozilla::ErrorResult& rv) {
     rv = GetPropertyValue(aPropName, aValue);
   }
-  void GetAuthoredPropertyValue(const nsAString& aPropName, nsString& aValue,
-                                mozilla::ErrorResult& rv) {
-    rv = GetAuthoredPropertyValue(aPropName, aValue);
-  }
   void GetPropertyPriority(const nsAString& aPropName, nsString& aPriority) {
     GetPropertyPriority(aPropName, static_cast<nsAString&>(aPriority));
   }
   void SetProperty(const nsAString& aPropName, const nsAString& aValue,
                    const nsAString& aPriority, mozilla::ErrorResult& rv) {
     rv = SetProperty(aPropName, aValue, aPriority);
   }
   void RemoveProperty(const nsAString& aPropName, nsString& aRetval,
@@ -150,18 +143,16 @@ public:
   }
 };
 
 NS_DEFINE_STATIC_IID_ACCESSOR(nsICSSDeclaration, NS_ICSSDECLARATION_IID)
 
 #define NS_DECL_NSICSSDECLARATION                                   \
   NS_IMETHOD GetPropertyValue(const nsCSSPropertyID aPropID,          \
                               nsAString& aValue) override;          \
-  NS_IMETHOD GetAuthoredPropertyValue(const nsAString& aPropName,   \
-                                      nsAString& aValue) override;  \
   NS_IMETHOD SetPropertyValue(const nsCSSPropertyID aPropID,          \
                               const nsAString& aValue) override;
 
 #define NS_DECL_NSIDOMCSSSTYLEDECLARATION_HELPER \
   NS_IMETHOD GetCssText(nsAString & aCssText) override; \
   NS_IMETHOD SetCssText(const nsAString & aCssText) override; \
   NS_IMETHOD GetPropertyValue(const nsAString & propertyName, nsAString & _retval) override; \
   NS_IMETHOD RemoveProperty(const nsAString & propertyName, nsAString & _retval) override; \

