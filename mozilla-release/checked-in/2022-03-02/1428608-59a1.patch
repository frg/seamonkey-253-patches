# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1516176620 -32400
# Node ID ca36a6c4f8a4a0ddaa033fdbe20836d87bbfb873
# Parent  167f4b9a7f8009fbbc06c949a379fa4c95ca3c2e
Bug 1428608 - Forbid / or !/ in LOCAL_INCLUDES. r=froydnj

And remove the two cases that currently set that, without actually using
it. The webrtc gtest one never relied on it, and the gfx one was added
in bug 1427668 for a single header, and the corresponding #includes were
changed in bug 1428678.

diff --git a/dom/canvas/moz.build b/dom/canvas/moz.build
--- a/dom/canvas/moz.build
+++ b/dom/canvas/moz.build
@@ -192,16 +192,17 @@ LOCAL_INCLUDES += [
 include('/ipc/chromium/chromium-config.mozbuild')
 
 
 USE_LIBS += [ 'translator' ] # Grab the Angle shader translator.
 
 FINAL_LIBRARY = 'xul'
 LOCAL_INCLUDES += [
     '../workers',
+    # -    '/', # Allow including relpaths from root.
     '/dom/base',
     '/dom/html',
     '/dom/svg',
     '/dom/workers',
     '/dom/xul',
     '/gfx/angle/checkout/include',
     '/gfx/gl',
     '/image',
diff --git a/media/webrtc/trunk/gtest/moz.build b/media/webrtc/trunk/gtest/moz.build
--- a/media/webrtc/trunk/gtest/moz.build
+++ b/media/webrtc/trunk/gtest/moz.build
@@ -16,17 +16,16 @@ DEFINES['WEBRTC_APM_DEBUG_DUMP'] = True
 DEFINES['WEBRTC_INTELLIGIBILITY_ENHANCER'] = 0
 DEFINES['WEBRTC_MOZILLA_BUILD'] = 1
 
 # Hit build errors on windows with xutility otherwise
 DisableStlWrapping()
 
 LOCAL_INCLUDES += [
     '../',
-    '/',
     '/ipc/chromium/src/',
     '/media/libopus/celt/',
     '/media/libopus/include',
     '/media/libopus/src',
     '/media/libyuv/libyuv/include',
 ]
 
 USE_LIBS += [
diff --git a/python/mozbuild/mozbuild/frontend/emitter.py b/python/mozbuild/mozbuild/frontend/emitter.py
--- a/python/mozbuild/mozbuild/frontend/emitter.py
+++ b/python/mozbuild/mozbuild/frontend/emitter.py
@@ -1157,21 +1157,27 @@ class TreeMetadataEmitter(LoggingMixin):
             for name in context.get(context_var, []):
                 self._idls[context_var].add(mozpath.join(context.srcdir, name))
         # WEBIDL_EXAMPLE_INTERFACES do not correspond to files.
         for name in context.get('WEBIDL_EXAMPLE_INTERFACES', []):
             self._idls['WEBIDL_EXAMPLE_INTERFACES'].add(name)
 
         local_includes = []
         for local_include in context.get('LOCAL_INCLUDES', []):
+            full_path = local_include.full_path
             if (not isinstance(local_include, ObjDirPath) and
-                    not os.path.exists(local_include.full_path)):
+                    not os.path.exists(full_path)):
                 raise SandboxValidationError('Path specified in LOCAL_INCLUDES '
                     'does not exist: %s (resolved to %s)' % (local_include,
-                    local_include.full_path), context)
+                    full_path), context)
+            if (full_path == context.config.topsrcdir or
+                    full_path == context.config.topobjdir):
+                raise SandboxValidationError('Path specified in LOCAL_INCLUDES '
+                    'is not allowed: %s (resolved to %s)' % (local_include,
+                    full_path), context)
             include_obj = LocalInclude(context, local_include)
             local_includes.append(include_obj.path.full_path)
             yield include_obj
 
         computed_flags.resolve_flags('LOCAL_INCLUDES', ['-I%s' % p for p in local_includes])
         computed_as_flags.resolve_flags('LOCAL_INCLUDES', ['-I%s' % p for p in local_includes])
         computed_host_flags.resolve_flags('LOCAL_INCLUDES', ['-I%s' % p for p in local_includes])
 
diff --git a/python/mozbuild/mozbuild/test/frontend/data/local_includes-invalid/objdir/moz.build b/python/mozbuild/mozbuild/test/frontend/data/local_includes-invalid/objdir/moz.build
new file mode 100644
--- /dev/null
+++ b/python/mozbuild/mozbuild/test/frontend/data/local_includes-invalid/objdir/moz.build
@@ -0,0 +1,5 @@
+# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
+# Any copyright is dedicated to the Public Domain.
+# http://creativecommons.org/publicdomain/zero/1.0/
+
+LOCAL_INCLUDES += ['!/']
diff --git a/python/mozbuild/mozbuild/test/frontend/data/local_includes-invalid/srcdir/moz.build b/python/mozbuild/mozbuild/test/frontend/data/local_includes-invalid/srcdir/moz.build
new file mode 100644
--- /dev/null
+++ b/python/mozbuild/mozbuild/test/frontend/data/local_includes-invalid/srcdir/moz.build
@@ -0,0 +1,5 @@
+# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
+# Any copyright is dedicated to the Public Domain.
+# http://creativecommons.org/publicdomain/zero/1.0/
+
+LOCAL_INCLUDES += ['/']
diff --git a/python/mozbuild/mozbuild/test/frontend/test_emitter.py b/python/mozbuild/mozbuild/test/frontend/test_emitter.py
--- a/python/mozbuild/mozbuild/test/frontend/test_emitter.py
+++ b/python/mozbuild/mozbuild/test/frontend/test_emitter.py
@@ -1008,16 +1008,32 @@ class TestEmitterBasic(unittest.TestCase
                           for o in objs if isinstance(o, LocalInclude)]
         expected = [
             mozpath.join(reader.config.topsrcdir, 'bar/baz'),
             mozpath.join(reader.config.topsrcdir, 'foo'),
         ]
 
         self.assertEqual(local_includes, expected)
 
+    def test_local_includes_invalid(self):
+        """Test that invalid LOCAL_INCLUDES are properly detected."""
+        reader = self.reader('local_includes-invalid/srcdir')
+
+        with self.assertRaisesRegexp(
+                SandboxValidationError,
+                'Path specified in LOCAL_INCLUDES is not allowed:'):
+            objs = self.read_topsrcdir(reader)
+
+        reader = self.reader('local_includes-invalid/objdir')
+
+        with self.assertRaisesRegexp(
+                SandboxValidationError,
+                'Path specified in LOCAL_INCLUDES is not allowed:'):
+            objs = self.read_topsrcdir(reader)
+
     def test_generated_includes(self):
         """Test that GENERATED_INCLUDES is emitted correctly."""
         reader = self.reader('generated_includes')
         objs = self.read_topsrcdir(reader)
 
         generated_includes = [o.path for o in objs if isinstance(o, LocalInclude)]
         expected = [
             '!/bar/baz',
