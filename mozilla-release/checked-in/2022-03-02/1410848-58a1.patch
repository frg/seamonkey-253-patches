# HG changeset patch
# User Henri Sivonen <hsivonen@hsivonen.fi>
# Date 1509087993 -10800
# Node ID 3660e2cf9fdbb05e7890f70e9a35572a0d714694
# Parent  0855fbd4196f870caa768ba2f21489078d8c9b94
Bug 1410848 - Use RAII with nsHtml5TreeOpExecutor::mFlushState. r=smaug.

MozReview-Commit-ID: 3a1tyYGT8u5

diff --git a/parser/html/nsHtml5AutoPauseUpdate.h b/parser/html/nsHtml5AutoPauseUpdate.h
new file mode 100644
--- /dev/null
+++ b/parser/html/nsHtml5AutoPauseUpdate.h
@@ -0,0 +1,30 @@
+/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=2 sw=2 et tw=78: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef nsHtml5AutoPauseUpdate_h
+#define nsHtml5AutoPauseUpdate_h
+
+class MOZ_RAII nsHtml5AutoPauseUpdate final
+{
+private:
+  RefPtr<nsHtml5DocumentBuilder> mBuilder;
+
+public:
+  explicit nsHtml5AutoPauseUpdate(nsHtml5DocumentBuilder* aBuilder)
+    : mBuilder(aBuilder)
+  {
+    mBuilder->EndDocUpdate();
+  }
+  ~nsHtml5AutoPauseUpdate()
+  {
+    // Something may have terminated the parser during the update pause.
+    if (!mBuilder->IsComplete()) {
+      mBuilder->BeginDocUpdate();
+    }
+  }
+};
+
+#endif // nsHtml5AutoPauseUpdate_h
diff --git a/parser/html/nsHtml5DocumentBuilder.h b/parser/html/nsHtml5DocumentBuilder.h
--- a/parser/html/nsHtml5DocumentBuilder.h
+++ b/parser/html/nsHtml5DocumentBuilder.h
@@ -13,17 +13,16 @@
 #include "nsIContent.h"
 
 typedef nsIContent* nsIContentPtr;
 
 enum eHtml5FlushState {
   eNotFlushing = 0,  // not flushing
   eInFlush = 1,      // the Flush() method is on the call stack
   eInDocUpdate = 2,  // inside an update batch on the document
-  eNotifying = 3     // flushing pending append notifications
 };
 
 class nsHtml5DocumentBuilder : public nsContentSink
 {
   using Encoding = mozilla::Encoding;
   template <typename T> using NotNull = mozilla::NotNull<T>;
 public:
   NS_DECL_CYCLE_COLLECTION_CLASS_INHERITED(nsHtml5DocumentBuilder,
@@ -62,38 +61,55 @@ public:
    * Checks if this parser is broken. Returns a non-NS_OK (i.e. non-0)
    * value if broken.
    */
   inline nsresult IsBroken()
   {
     return mBroken;
   }
 
+  inline bool IsComplete()
+  {
+    return !mParser;
+  }
+
   inline void BeginDocUpdate()
   {
-    NS_PRECONDITION(mFlushState == eInFlush, "Tried to double-open update.");
-    NS_PRECONDITION(mParser, "Started update without parser.");
+    MOZ_RELEASE_ASSERT(IsInFlush(), "Tried to double-open doc update.");
+    MOZ_RELEASE_ASSERT(mParser, "Started doc update without parser.");
     mFlushState = eInDocUpdate;
     mDocument->BeginUpdate(UPDATE_CONTENT_MODEL);
   }
 
   inline void EndDocUpdate()
   {
-    NS_PRECONDITION(mFlushState != eNotifying, "mFlushState out of sync");
-    if (mFlushState == eInDocUpdate) {
-      mFlushState = eInFlush;
-      mDocument->EndUpdate(UPDATE_CONTENT_MODEL);
-    }
+    MOZ_RELEASE_ASSERT(IsInDocUpdate(),
+                       "Tried to end doc update without one open.");
+    mFlushState = eInFlush;
+    mDocument->EndUpdate(UPDATE_CONTENT_MODEL);
   }
 
-  bool IsInDocUpdate()
+  inline void BeginFlush()
   {
-    return mFlushState == eInDocUpdate;
+    MOZ_RELEASE_ASSERT(mFlushState == eNotFlushing,
+                       "Tried to start a flush when already flushing.");
+    MOZ_RELEASE_ASSERT(mParser, "Started a flush without parser.");
+    mFlushState = eInFlush;
   }
 
+  inline void EndFlush()
+  {
+    MOZ_RELEASE_ASSERT(IsInFlush(), "Tried to end flush when not flushing.");
+    mFlushState = eNotFlushing;
+  }
+
+  inline bool IsInDocUpdate() { return mFlushState == eInDocUpdate; }
+
+  inline bool IsInFlush() { return mFlushState == eInFlush; }
+
   void SetDocumentCharsetAndSource(NotNull<const Encoding*> aEncoding,
                                    int32_t aCharsetSource);
 
   /**
    * Sets up style sheet load / parse
    */
   void UpdateStyleSheet(nsIContent* aElement);
 
diff --git a/parser/html/nsHtml5OplessBuilder.cpp b/parser/html/nsHtml5OplessBuilder.cpp
--- a/parser/html/nsHtml5OplessBuilder.cpp
+++ b/parser/html/nsHtml5OplessBuilder.cpp
@@ -18,32 +18,32 @@ nsHtml5OplessBuilder::nsHtml5OplessBuild
 
 nsHtml5OplessBuilder::~nsHtml5OplessBuilder()
 {
 }
 
 void
 nsHtml5OplessBuilder::Start()
 {
-  mFlushState = eInFlush;
+  BeginFlush();
   BeginDocUpdate();
 }
 
 void
 nsHtml5OplessBuilder::Finish()
 {
   EndDocUpdate();
+  EndFlush();
   DropParserAndPerfHint();
   mScriptLoader = nullptr;
   mDocument = nullptr;
   mNodeInfoManager = nullptr;
   mCSSLoader = nullptr;
   mDocumentURI = nullptr;
   mDocShell = nullptr;
   mOwnedElements.Clear();
-  mFlushState = eNotFlushing;
 }
 
 void
 nsHtml5OplessBuilder::SetParser(nsParserBase* aParser)
 {
   mParser = aParser;
 }
diff --git a/parser/html/nsHtml5SpeculativeLoad.h b/parser/html/nsHtml5SpeculativeLoad.h
--- a/parser/html/nsHtml5SpeculativeLoad.h
+++ b/parser/html/nsHtml5SpeculativeLoad.h
@@ -221,16 +221,19 @@ class nsHtml5SpeculativeLoad {
       mOpCode = eSpeculativeLoadPreconnect;
       aUrl.ToString(mUrlOrSizes);
       aCrossOrigin.ToString(mCrossOriginOrMedia);
     }
 
     void Perform(nsHtml5TreeOpExecutor* aExecutor);
 
   private:
+    nsHtml5SpeculativeLoad(const nsHtml5SpeculativeLoad&) = delete;
+    nsHtml5SpeculativeLoad& operator=(const nsHtml5SpeculativeLoad&) = delete;
+
     eHtml5SpeculativeLoad mOpCode;
 
     /**
      * Whether the refering element has async and/or defer attributes.
      */
     bool mIsAsync;
     bool mIsDefer;
 
diff --git a/parser/html/nsHtml5TreeOpExecutor.cpp b/parser/html/nsHtml5TreeOpExecutor.cpp
--- a/parser/html/nsHtml5TreeOpExecutor.cpp
+++ b/parser/html/nsHtml5TreeOpExecutor.cpp
@@ -29,16 +29,17 @@
 #include "GeckoProfiler.h"
 #include "nsIScriptError.h"
 #include "nsIScriptContext.h"
 #include "mozilla/Preferences.h"
 #include "nsIHTMLDocument.h"
 #include "nsIViewSourceChannel.h"
 #include "xpcpublic.h"
 #include "mozilla/IdleTaskRunner.h"
+#include "nsHtml5AutoPauseUpdate.h"
 
 using namespace mozilla;
 
 NS_INTERFACE_TABLE_HEAD_CYCLE_COLLECTION_INHERITED(nsHtml5TreeOpExecutor)
   NS_INTERFACE_TABLE_INHERITED(nsHtml5TreeOpExecutor,
                                nsIContentSink)
 NS_INTERFACE_TABLE_TAIL_INHERITING(nsHtml5DocumentBuilder)
 
@@ -57,16 +58,53 @@ class nsHtml5ExecutorReflusher : public 
     {}
     NS_IMETHOD Run() override
     {
       mExecutor->RunFlushLoop();
       return NS_OK;
     }
 };
 
+class MOZ_RAII nsHtml5AutoFlush final
+{
+private:
+  RefPtr<nsHtml5TreeOpExecutor> mExecutor;
+  size_t mOpsToRemove;
+
+public:
+  explicit nsHtml5AutoFlush(nsHtml5TreeOpExecutor* aExecutor)
+    : mExecutor(aExecutor)
+    , mOpsToRemove(aExecutor->OpQueueLength())
+  {
+    mExecutor->BeginFlush();
+    mExecutor->BeginDocUpdate();
+  }
+  ~nsHtml5AutoFlush()
+  {
+    if (mExecutor->IsInDocUpdate()) {
+      mExecutor->EndDocUpdate();
+    } else {
+      // We aren't in an update if nsHtml5AutoPauseUpdate
+      // caused something to terminate the parser.
+      MOZ_RELEASE_ASSERT(
+        mExecutor->IsComplete(),
+        "How do we have mParser but the doc update isn't open?");
+    }
+    mExecutor->EndFlush();
+    mExecutor->RemoveFromStartOfOpQueue(mOpsToRemove);
+  }
+  void SetNumberOfOpsToRemove(size_t aOpsToRemove)
+  {
+    MOZ_ASSERT(aOpsToRemove < mOpsToRemove,
+               "Requested partial clearing of op queue but the number to clear "
+               "wasn't less than the length of the queue.");
+    mOpsToRemove = aOpsToRemove;
+  }
+};
+
 static mozilla::LinkedList<nsHtml5TreeOpExecutor>* gBackgroundFlushList = nullptr;
 StaticRefPtr<IdleTaskRunner> gBackgroundFlushRunner;
 
 nsHtml5TreeOpExecutor::nsHtml5TreeOpExecutor()
   : nsHtml5DocumentBuilder(false)
   , mSuppressEOF(false)
   , mReadingFromStage(false)
   , mStreamParser(nullptr)
@@ -76,17 +114,17 @@ nsHtml5TreeOpExecutor::nsHtml5TreeOpExec
   , mRunFlushLoopOnStack(false)
   , mCallContinueInterruptedParsingIfEnabled(false)
   , mAlreadyComplainedAboutCharset(false)
   , mAlreadyComplainedAboutDeepTree(false) {}
 
 nsHtml5TreeOpExecutor::~nsHtml5TreeOpExecutor()
 {
   if (gBackgroundFlushList && isInList()) {
-    mOpQueue.Clear();
+    ClearOpQueue();
     removeFrom(*gBackgroundFlushList);
     if (gBackgroundFlushList->isEmpty()) {
       delete gBackgroundFlushList;
       gBackgroundFlushList = nullptr;
       if (gBackgroundFlushRunner) {
         gBackgroundFlushRunner->Cancel();
         gBackgroundFlushRunner = nullptr;
       }
@@ -287,19 +325,19 @@ nsHtml5TreeOpExecutor::ContinueInterrupt
   }
 }
 
 void
 nsHtml5TreeOpExecutor::FlushSpeculativeLoads()
 {
   nsTArray<nsHtml5SpeculativeLoad> speculativeLoadQueue;
   mStage.MoveSpeculativeLoadsTo(speculativeLoadQueue);
-  const nsHtml5SpeculativeLoad* start = speculativeLoadQueue.Elements();
-  const nsHtml5SpeculativeLoad* end = start + speculativeLoadQueue.Length();
-  for (nsHtml5SpeculativeLoad* iter = const_cast<nsHtml5SpeculativeLoad*>(start);
+  nsHtml5SpeculativeLoad* start = speculativeLoadQueue.Elements();
+  nsHtml5SpeculativeLoad* end = start + speculativeLoadQueue.Length();
+  for (nsHtml5SpeculativeLoad* iter = start;
        iter < end;
        ++iter) {
     if (MOZ_UNLIKELY(!mParser)) {
       // An extension terminated the parser from a HTTP observer.
       return;
     }
     iter->Perform(this);
   }
@@ -363,17 +401,17 @@ nsHtml5TreeOpExecutor::RunFlushLoop()
       << streamParserGrip;  // Intentionally not used within function
 
   // Remember the entry time
   (void) nsContentSink::WillParseImpl();
 
   for (;;) {
     if (!mParser) {
       // Parse has terminated.
-      mOpQueue.Clear(); // clear in order to be able to assert in destructor
+      ClearOpQueue(); // clear in order to be able to assert in destructor
       return;
     }
 
     if (NS_FAILED(IsBroken())) {
       return;
     }
 
     if (!parserKungFuDeathGrip->IsParserEnabled()) {
@@ -390,38 +428,41 @@ nsHtml5TreeOpExecutor::RunFlushLoop()
     // (probably due to a synchronous XMLHttpRequest) and will re-enable us
     // later, see bug 460706.
     if (IsScriptExecuting()) {
       return;
     }
 
     if (mReadingFromStage) {
       nsTArray<nsHtml5SpeculativeLoad> speculativeLoadQueue;
+      MOZ_RELEASE_ASSERT(mFlushState == eNotFlushing,
+                         "mOpQueue modified during flush.");
+
       mStage.MoveOpsAndSpeculativeLoadsTo(mOpQueue, speculativeLoadQueue);
       // Make sure speculative loads never start after the corresponding
       // normal loads for the same URLs.
-      const nsHtml5SpeculativeLoad* start = speculativeLoadQueue.Elements();
-      const nsHtml5SpeculativeLoad* end = start + speculativeLoadQueue.Length();
-      for (nsHtml5SpeculativeLoad* iter = (nsHtml5SpeculativeLoad*)start;
+      nsHtml5SpeculativeLoad* start = speculativeLoadQueue.Elements();
+      nsHtml5SpeculativeLoad* end = start + speculativeLoadQueue.Length();
+      for (nsHtml5SpeculativeLoad* iter = start;
            iter < end;
            ++iter) {
         iter->Perform(this);
         if (MOZ_UNLIKELY(!mParser)) {
           // An extension terminated the parser from a HTTP observer.
-          mOpQueue.Clear(); // clear in order to be able to assert in destructor
+          ClearOpQueue(); // clear in order to be able to assert in destructor
           return;
         }
       }
     } else {
       FlushSpeculativeLoads(); // Make sure speculative loads never start after
                                // the corresponding normal loads for the same
                                // URLs.
       if (MOZ_UNLIKELY(!mParser)) {
         // An extension terminated the parser from a HTTP observer.
-        mOpQueue.Clear(); // clear in order to be able to assert in destructor
+        ClearOpQueue(); // clear in order to be able to assert in destructor
         return;
       }
       // Now parse content left in the document.write() buffer queue if any.
       // This may generate tree ops on its own or dequeue a speculation.
       nsresult rv = GetParser()->ParseUntilBlocked();
       if (NS_FAILED(rv)) {
         MarkAsBroken(rv);
         return;
@@ -429,68 +470,56 @@ nsHtml5TreeOpExecutor::RunFlushLoop()
     }
 
     if (mOpQueue.IsEmpty()) {
       // Avoid bothering the rest of the engine with a doc update if there's 
       // nothing to do.
       return;
     }
 
-    mFlushState = eInFlush;
 
     nsIContent* scriptElement = nullptr;
     bool interrupted = false;
     bool streamEnded = false;
-    
-    BeginDocUpdate();
-
-    uint32_t numberOfOpsToFlush = mOpQueue.Length();
+    {
+      // autoFlush clears mOpQueue in its destructor unless
+      // SetNumberOfOpsToRemove is called first, in which case only
+      // some ops from the start of the queue are cleared.
+      nsHtml5AutoFlush autoFlush(this);
 
-    const nsHtml5TreeOperation* first = mOpQueue.Elements();
-    const nsHtml5TreeOperation* last = first + numberOfOpsToFlush - 1;
-    for (nsHtml5TreeOperation* iter = const_cast<nsHtml5TreeOperation*>(first);;) {
-      if (MOZ_UNLIKELY(!mParser)) {
-        // The previous tree op caused a call to nsIParser::Terminate().
-        break;
-      }
-      NS_ASSERTION(mFlushState == eInDocUpdate, 
-        "Tried to perform tree op outside update batch.");
-      nsresult rv = iter->Perform(this, &scriptElement, &interrupted, &streamEnded);
-      if (NS_FAILED(rv)) {
-        MarkAsBroken(rv);
-        break;
+      nsHtml5TreeOperation* first = mOpQueue.Elements();
+      nsHtml5TreeOperation* last = first + mOpQueue.Length() - 1;
+      for (nsHtml5TreeOperation* iter = first;; ++iter) {
+        if (MOZ_UNLIKELY(!mParser)) {
+          // The previous tree op caused a call to nsIParser::Terminate().
+          return;
+        }
+        MOZ_ASSERT(IsInDocUpdate(),
+                   "Tried to perform tree op outside update batch.");
+        nsresult rv = iter->Perform(this, &scriptElement, &interrupted,  &streamEnded);
+        if (NS_FAILED(rv)) {
+          MarkAsBroken(rv);
+          break;
+        }
+
+        // Be sure not to check the deadline if the last op was just performed.
+        if (MOZ_UNLIKELY(iter == last)) {
+          break;
+        } else if (MOZ_UNLIKELY(interrupted) ||
+                   MOZ_UNLIKELY(nsContentSink::DidProcessATokenImpl() ==
+                                NS_ERROR_HTMLPARSER_INTERRUPTED)) {
+
+          autoFlush.SetNumberOfOpsToRemove((iter - first) + 1);
+
+          nsHtml5TreeOpExecutor::ContinueInterruptedParsingAsync();
+          return;
+        }
       }
 
-      // Be sure not to check the deadline if the last op was just performed.
-      if (MOZ_UNLIKELY(iter == last)) {
-        break;
-      } else if (MOZ_UNLIKELY(interrupted) ||
-                 MOZ_UNLIKELY(nsContentSink::DidProcessATokenImpl() ==
-                              NS_ERROR_HTMLPARSER_INTERRUPTED)) {
-        mOpQueue.RemoveElementsAt(0, (iter - first) + 1);
-        
-        EndDocUpdate();
-
-        mFlushState = eNotFlushing;
-
-        #ifdef DEBUG_NS_HTML5_TREE_OP_EXECUTOR_FLUSH
-          printf("REFLUSH SCHEDULED (executing ops): %d\n", 
-            ++sTimesFlushLoopInterrupted);
-        #endif
-        nsHtml5TreeOpExecutor::ContinueInterruptedParsingAsync();
-        return;
-      }
-      ++iter;
-    }
-    
-    mOpQueue.Clear();
-    
-    EndDocUpdate();
-
-    mFlushState = eNotFlushing;
+    } // end autoFlush
 
     if (MOZ_UNLIKELY(!mParser)) {
       // The parse ended already.
       return;
     }
 
     if (streamEnded) {
       DidBuildModel(false);
@@ -528,76 +557,70 @@ nsHtml5TreeOpExecutor::FlushDocumentWrit
   nsresult rv = IsBroken();
   NS_ENSURE_SUCCESS(rv, rv);
 
   FlushSpeculativeLoads(); // Make sure speculative loads never start after the
                 // corresponding normal loads for the same URLs.
 
   if (MOZ_UNLIKELY(!mParser)) {
     // The parse has ended.
-    mOpQueue.Clear(); // clear in order to be able to assert in destructor
+    ClearOpQueue(); // clear in order to be able to assert in destructor
     return rv;
   }
   
   if (mFlushState != eNotFlushing) {
     // XXX Can this happen? In case it can, let's avoid crashing.
     return rv;
   }
 
-  mFlushState = eInFlush;
-
   // avoid crashing near EOF
   RefPtr<nsHtml5TreeOpExecutor> kungFuDeathGrip(this);
   RefPtr<nsParserBase> parserKungFuDeathGrip(mParser);
   mozilla::Unused << parserKungFuDeathGrip; // Intentionally not used within function
   RefPtr<nsHtml5StreamParser> streamParserGrip;
   if (mParser) {
     streamParserGrip = GetParser()->GetStreamParser();
   }
   mozilla::Unused
       << streamParserGrip;  // Intentionally not used within function
 
-  NS_ASSERTION(!mReadingFromStage,
-    "Got doc write flush when reading from stage");
+  MOZ_RELEASE_ASSERT(!mReadingFromStage,
+                     "Got doc write flush when reading from stage");
 
 #ifdef DEBUG
   mStage.AssertEmpty();
 #endif
   
   nsIContent* scriptElement = nullptr;
   bool interrupted = false;
   bool streamEnded = false;
-  
-  BeginDocUpdate();
 
-  uint32_t numberOfOpsToFlush = mOpQueue.Length();
+  {
+    // autoFlush clears mOpQueue in its destructor.
+    nsHtml5AutoFlush autoFlush(this);
 
-  const nsHtml5TreeOperation* start = mOpQueue.Elements();
-  const nsHtml5TreeOperation* end = start + numberOfOpsToFlush;
-  for (nsHtml5TreeOperation* iter = const_cast<nsHtml5TreeOperation*>(start);
+  nsHtml5TreeOperation* start = mOpQueue.Elements();
+  nsHtml5TreeOperation* end = start + mOpQueue.Length();
+  for (nsHtml5TreeOperation* iter = start;
        iter < end;
        ++iter) {
-    if (MOZ_UNLIKELY(!mParser)) {
-      // The previous tree op caused a call to nsIParser::Terminate().
-      break;
+      if (MOZ_UNLIKELY(!mParser)) {
+        // The previous tree op caused a call to nsIParser::Terminate().
+        return rv;
+      }
+      NS_ASSERTION(IsInDocUpdate(),
+                   "Tried to perform tree op outside update batch.");
+      rv = iter->Perform(this, &scriptElement, &interrupted, &streamEnded);
+      if (NS_FAILED(rv)) {
+        MarkAsBroken(rv);
+        break;
+      }
     }
-    NS_ASSERTION(mFlushState == eInDocUpdate, 
-      "Tried to perform tree op outside update batch.");
-    rv = iter->Perform(this, &scriptElement, &interrupted, &streamEnded);
-    if (NS_FAILED(rv)) {
-      MarkAsBroken(rv);
-      break;
-    }
-  }
 
-  mOpQueue.Clear();
-  
-  EndDocUpdate();
-
-  mFlushState = eNotFlushing;
+  } // autoFlush
 
   if (MOZ_UNLIKELY(!mParser)) {
     // Ending the doc update caused a call to nsIParser::Terminate().
     return rv;
   }
 
   if (streamEnded) {
     DidBuildModel(false);
@@ -637,42 +660,38 @@ nsHtml5TreeOpExecutor::IsScriptEnabled()
 }
 
 void
 nsHtml5TreeOpExecutor::StartLayout(bool* aInterrupted) {
   if (mLayoutStarted || !mDocument) {
     return;
   }
 
-  EndDocUpdate();
+  nsHtml5AutoPauseUpdate autoPause(this);
 
   if (MOZ_UNLIKELY(!mParser)) {
     // got terminate
     return;
   }
 
   nsContentSink::StartLayout(false);
 
   if (mParser) {
     *aInterrupted = !GetParser()->IsParserEnabled();
-
-    BeginDocUpdate();
   }
 }
 
 void
 nsHtml5TreeOpExecutor::PauseDocUpdate(bool* aInterrupted) {
   // Pausing the document update allows JS to run, and potentially block
   // further parsing.
-  EndDocUpdate();
+  nsHtml5AutoPauseUpdate autoPause(this);
 
   if (MOZ_LIKELY(mParser)) {
     *aInterrupted = !GetParser()->IsParserEnabled();
-
-    BeginDocUpdate();
   }
 }
 
 /**
  * The reason why this code is here and not in the tree builder even in the 
  * main-thread case is to allow the control to return from the tokenizer 
  * before scripts run. This way, the tokenizer is not invoked re-entrantly 
  * although the parser is.
@@ -700,18 +719,19 @@ nsHtml5TreeOpExecutor::RunScript(nsICont
     return;
   }
   
   if (sele->GetScriptDeferred() || sele->GetScriptAsync()) {
     DebugOnly<bool> block = sele->AttemptToExecute();
     NS_ASSERTION(!block, "Defer or async script tried to block.");
     return;
   }
-  
-  NS_ASSERTION(mFlushState == eNotFlushing, "Tried to run script when flushing.");
+
+  MOZ_RELEASE_ASSERT(mFlushState == eNotFlushing,
+                     "Tried to run script while flushing.");
 
   mReadingFromStage = false;
   
   sele->SetCreatorParser(GetParser());
 
   // Copied from nsXMLContentSink
   // Now tell the script that it's ready to go. This may execute the script
   // or return true, or neither if the script doesn't need executing.
@@ -739,18 +759,17 @@ nsHtml5TreeOpExecutor::Start()
   mStarted = true;
 }
 
 void
 nsHtml5TreeOpExecutor::NeedsCharsetSwitchTo(NotNull<const Encoding*> aEncoding,
                                             int32_t aSource,
                                             uint32_t aLineNumber)
 {
-  EndDocUpdate();
-
+  nsHtml5AutoPauseUpdate autoPause(this);
   if (MOZ_UNLIKELY(!mParser)) {
     // got terminate
     return;
   }
   
   nsCOMPtr<nsIWebShellServices> wss = do_QueryInterface(mDocShell);
   if (!wss) {
     return;
@@ -773,18 +792,16 @@ nsHtml5TreeOpExecutor::NeedsCharsetSwitc
     return;
   }
 
   if (aSource == kCharsetFromMetaTag) {
     MaybeComplainAboutCharset("EncLateMetaTooLate", true, aLineNumber);
   }
 
   GetParser()->ContinueAfterFailedCharsetSwitch();
-
-  BeginDocUpdate();
 }
 
 void
 nsHtml5TreeOpExecutor::MaybeComplainAboutCharset(const char* aMsgId,
                                                  bool aError,
                                                  uint32_t aLineNumber)
 {
   if (mAlreadyComplainedAboutCharset) {
@@ -849,22 +866,41 @@ nsHtml5TreeOpExecutor::GetParser()
 {
   MOZ_ASSERT(!mRunsToCompletion);
   return static_cast<nsHtml5Parser*>(mParser.get());
 }
 
 void
 nsHtml5TreeOpExecutor::MoveOpsFrom(nsTArray<nsHtml5TreeOperation>& aOpQueue)
 {
-  NS_PRECONDITION(mFlushState == eNotFlushing, "mOpQueue modified during tree op execution.");
+  MOZ_RELEASE_ASSERT(mFlushState == eNotFlushing,
+                     "Ops added to mOpQueue during tree op execution.");
   mOpQueue.AppendElements(Move(aOpQueue));
 }
 
 void
-nsHtml5TreeOpExecutor::InitializeDocWriteParserState(nsAHtml5TreeBuilderState* aState, int32_t aLine)
+nsHtml5TreeOpExecutor::ClearOpQueue()
+{
+  MOZ_RELEASE_ASSERT(mFlushState == eNotFlushing,
+                     "mOpQueue cleared during tree op execution.");
+  mOpQueue.Clear();
+}
+
+void
+nsHtml5TreeOpExecutor::RemoveFromStartOfOpQueue(size_t aNumberOfOpsToRemove)
+{
+  MOZ_RELEASE_ASSERT(mFlushState == eNotFlushing,
+                     "Ops removed from mOpQueue during tree op execution.");
+  mOpQueue.RemoveElementsAt(0, aNumberOfOpsToRemove);
+}
+
+void
+nsHtml5TreeOpExecutor::InitializeDocWriteParserState(
+  nsAHtml5TreeBuilderState* aState,
+  int32_t aLine)
 {
   GetParser()->InitializeDocWriteParserState(aState, aLine);
 }
 
 nsIURI*
 nsHtml5TreeOpExecutor::GetViewSourceBaseURI()
 {
   if (!mViewSourceBaseURI) {
diff --git a/parser/html/nsHtml5TreeOpExecutor.h b/parser/html/nsHtml5TreeOpExecutor.h
--- a/parser/html/nsHtml5TreeOpExecutor.h
+++ b/parser/html/nsHtml5TreeOpExecutor.h
@@ -199,21 +199,16 @@ class nsHtml5TreeOpExecutor final : publ
     void MaybeComplainAboutCharset(const char* aMsgId,
                                    bool aError,
                                    uint32_t aLineNumber);
 
     void ComplainAboutBogusProtocolCharset(nsIDocument* aDoc);
 
   void MaybeComplainAboutDeepTree(uint32_t aLineNumber);
 
-    bool IsComplete()
-    {
-      return !mParser;
-    }
-    
     bool HasStarted()
     {
       return mStarted;
     }
     
     bool IsFlushing()
     {
       return mFlushState >= eInFlush;
@@ -228,17 +223,23 @@ class nsHtml5TreeOpExecutor final : publ
     
     void RunScript(nsIContent* aScriptElement);
     
     /**
      * Flush the operations from the tree operations from the argument
      * queue unconditionally. (This is for the main thread case.)
      */
     virtual void MoveOpsFrom(nsTArray<nsHtml5TreeOperation>& aOpQueue) override;
-    
+
+    void ClearOpQueue();
+
+    void RemoveFromStartOfOpQueue(size_t aNumberOfOpsToRemove);
+
+    inline size_t OpQueueLength() { return mOpQueue.Length(); }
+
     nsHtml5TreeOpStage* GetStage()
     {
       return &mStage;
     }
     
     void StartReadingFromStage()
     {
       mReadingFromStage = true;
diff --git a/parser/html/nsHtml5TreeOperation.cpp b/parser/html/nsHtml5TreeOperation.cpp
--- a/parser/html/nsHtml5TreeOperation.cpp
+++ b/parser/html/nsHtml5TreeOperation.cpp
@@ -36,16 +36,17 @@
 #include "mozilla/dom/HTMLTemplateElement.h"
 #include "nsHtml5SVGLoadDispatcher.h"
 #include "nsIURI.h"
 #include "nsIProtocolHandler.h"
 #include "nsNetUtil.h"
 #include "nsIHTMLDocument.h"
 #include "mozilla/Likely.h"
 #include "nsTextNode.h"
+#include "nsHtml5AutoPauseUpdate.h"
 
 using namespace mozilla;
 
 static NS_DEFINE_CID(kFormProcessorCID, NS_FORMPROCESSOR_CID);
 
 /**
  * Helper class that opens a notification batch if the current doc
  * is different from the executor doc.
diff --git a/parser/html/nsHtml5TreeOperation.cpp.1410848.later b/parser/html/nsHtml5TreeOperation.cpp.1410848.later
new file mode 100644
--- /dev/null
+++ b/parser/html/nsHtml5TreeOperation.cpp.1410848.later
@@ -0,0 +1,64 @@
+NEEDS
+Bug 1378079 - Part 3: Complete the steps related to custom elements in "create an element for a token". r=hsivonen,smaug
+
+--- nsHtml5TreeOperation.cpp
++++ nsHtml5TreeOperation.cpp
+@@ -69,39 +70,16 @@ class MOZ_STACK_CLASS nsHtml5OtherDocUpd
+       if (MOZ_UNLIKELY(mDocument)) {
+         mDocument->EndUpdate(UPDATE_CONTENT_MODEL);
+       }
+     }
+   private:
+     nsCOMPtr<nsIDocument> mDocument;
+ };
+ 
+-/**
+- * Helper class to temporary break out of the document update batch. Use this
+- * with caution as this will cause blocked scripts to run.
+- */
+-class MOZ_RAII mozAutoPauseContentUpdate final
+-{
+-public:
+-  explicit mozAutoPauseContentUpdate(nsIDocument* aDocument)
+-    : mDocument(aDocument)
+-  {
+-    MOZ_ASSERT(mDocument);
+-    mDocument->EndUpdate(UPDATE_CONTENT_MODEL);
+-  }
+-
+-  ~mozAutoPauseContentUpdate()
+-  {
+-    mDocument->BeginUpdate(UPDATE_CONTENT_MODEL);
+-  }
+-
+-private:
+-  nsCOMPtr<nsIDocument> mDocument;
+-};
+-
+ nsHtml5TreeOperation::nsHtml5TreeOperation()
+  : mOpCode(eTreeOpUninitialized)
+ {
+   MOZ_COUNT_CTOR(nsHtml5TreeOperation);
+ }
+ 
+ nsHtml5TreeOperation::~nsHtml5TreeOperation()
+ {
+@@ -437,17 +415,17 @@ nsHtml5TreeOperation::CreateHTMLElement(
+         willExecuteScript = true;
+       }
+     }
+   }
+ 
+   if (willExecuteScript) { // This will cause custom element constructors to run
+     AutoSetThrowOnDynamicMarkupInsertionCounter
+       throwOnDynamicMarkupInsertionCounter(document);
+-    mozAutoPauseContentUpdate autoPauseContentUpdate(document);
++    nsHtml5AutoPauseUpdate autoPauseContentUpdate(aBuilder);
+     {
+       nsAutoMicroTask mt;
+     }
+     dom::AutoCEReaction
+       autoCEReaction(document->GetDocGroup()->CustomElementReactionsStack());
+ 
+     nsCOMPtr<dom::Element> newElement;
+     NS_NewHTMLElement(getter_AddRefs(newElement), nodeInfo.forget(),
diff --git a/parser/html/nsHtml5TreeOperation.h b/parser/html/nsHtml5TreeOperation.h
--- a/parser/html/nsHtml5TreeOperation.h
+++ b/parser/html/nsHtml5TreeOperation.h
@@ -549,16 +549,19 @@ class nsHtml5TreeOperation final {
     }
 
     nsresult Perform(nsHtml5TreeOpExecutor* aBuilder,
                      nsIContent** aScriptElement,
                      bool* aInterrupted,
                      bool* aStreamEnded);
 
   private:
+    nsHtml5TreeOperation(const nsHtml5TreeOperation&) = delete;
+    nsHtml5TreeOperation& operator=(const nsHtml5TreeOperation&) = delete;
+
     // possible optimization:
     // Make the queue take items the size of pointer and make the op code
     // decide how many operands it dequeues after it.
     eHtml5TreeOperation mOpCode;
     union {
       nsIContent**                    node;
       nsIAtom*                        atom;
       nsHtml5HtmlAttributes*          attributes;
