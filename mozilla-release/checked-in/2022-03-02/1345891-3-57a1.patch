# HG changeset patch
# User David Anderson <danderson@mozilla.com>
# Date 1501820569 25200
# Node ID 5d7badf38b240fce314b8466e6d6842d30145753
# Parent  3c6dd6c64aff741e175009dda603f49eb2f2fc3f
Handle invalidation bounds overflow in ContainerLayerMLGPU. (bug 1345891 part 3, r=mattwoodrow)

diff --git a/gfx/layers/LayerTreeInvalidation.cpp b/gfx/layers/LayerTreeInvalidation.cpp
--- a/gfx/layers/LayerTreeInvalidation.cpp
+++ b/gfx/layers/LayerTreeInvalidation.cpp
@@ -339,16 +339,17 @@ public:
     ContainerLayer* container = mLayer->AsContainerLayer();
     nsIntRegion invalidOfLayer; // Invalid regions of this layer.
     nsIntRegion result;         // Invliad regions for children only.
 
     container->CheckCanary();
 
     bool childrenChanged = false;
     bool invalidateWholeLayer = false;
+    bool areaOverflowed = false;
     if (mPreXScale != container->GetPreXScale() ||
         mPreYScale != container->GetPreYScale()) {
       invalidOfLayer = OldTransformedBounds();
       AddRegion(invalidOfLayer, NewTransformedBounds());
       childrenChanged = true;
       invalidateWholeLayer = true;
 
       // Can't bail out early, we need to update the child container layers
@@ -386,17 +387,19 @@ public:
               AddRegion(result, mChildren[j]->OldTransformedBounds());
               childrenChanged |= true;
             }
             if (childsOldIndex >= mChildren.Length()) {
               MOZ_CRASH("Out of bounds");
             }
             // Invalidate any regions of the child that have changed:
             nsIntRegion region;
-            mChildren[childsOldIndex]->ComputeChange(LTI_DEEPER(aPrefix), region, aCallback);
+            if (!mChildren[childsOldIndex]->ComputeChange(LTI_DEEPER(aPrefix), region, aCallback)) {
+              areaOverflowed = true;
+            }
             i = childsOldIndex + 1;
             if (!region.IsEmpty()) {
               LTI_LOG("%s%p: child %p produced %s\n", aPrefix, mLayer.get(),
                 mChildren[childsOldIndex]->mLayer.get(), Stringify(region).c_str());
               AddRegion(result, region);
               childrenChanged |= true;
             }
           } else {
@@ -439,31 +442,27 @@ public:
     }
 
     if (childrenChanged) {
       container->SetChildrenChanged(true);
     }
 
     if (container->UseIntermediateSurface()) {
       Maybe<IntRect> bounds;
-
-      if (!invalidateWholeLayer) {
+      if (!invalidateWholeLayer && !areaOverflowed) {
         bounds = Some(result.GetBounds());
 
         // Process changes in the visible region.
         IntRegion newVisible = mLayer->GetLocalVisibleRegion().ToUnknownRegion();
         if (!newVisible.IsEqual(mVisibleRegion)) {
           newVisible.XorWith(mVisibleRegion);
           bounds = bounds->SafeUnion(newVisible.GetBounds());
         }
       }
-      if (!bounds) {
-        bounds = Some(mLayer->GetLocalVisibleRegion().GetBounds().ToUnknownRect());
-      }
-      container->SetInvalidCompositeRect(bounds.value());
+      container->SetInvalidCompositeRect(bounds ? bounds.ptr() : nullptr);
     }
 
     if (!mLayer->Extend3DContext()) {
       // |result| contains invalid regions only of children.
       result.Transform(GetTransformForInvalidation(mLayer));
     }
     // else, effective transforms have applied on children.
 
diff --git a/gfx/layers/Layers.h b/gfx/layers/Layers.h
--- a/gfx/layers/Layers.h
+++ b/gfx/layers/Layers.h
@@ -2329,17 +2329,19 @@ public:
   }
 
   void SetFilterChain(nsTArray<CSSFilter>&& aFilterChain) {
     mFilterChain = aFilterChain;
   }
 
   nsTArray<CSSFilter>& GetFilterChain() { return mFilterChain; }
   
-  virtual void SetInvalidCompositeRect(const gfx::IntRect& aRect) {}
+  // If |aRect| is null, the entire layer should be considered invalid for
+  // compositing.
+  virtual void SetInvalidCompositeRect(const gfx::IntRect* aRect) {}
 
 protected:
   friend class ReadbackProcessor;
 
   // Note that this is not virtual, and is based on the implementation of
   // ContainerLayer::RemoveChild, so it should only be called where you would
   // want to explicitly call the base class implementation of RemoveChild;
   // e.g., while (mFirstChild) ContainerLayer::RemoveChild(mFirstChild);
diff --git a/gfx/layers/mlgpu/ContainerLayerMLGPU.cpp b/gfx/layers/mlgpu/ContainerLayerMLGPU.cpp
--- a/gfx/layers/mlgpu/ContainerLayerMLGPU.cpp
+++ b/gfx/layers/mlgpu/ContainerLayerMLGPU.cpp
@@ -185,27 +185,31 @@ ContainerLayerMLGPU::UpdateRenderTarget(
     gfxWarning() << "Failed to create an intermediate render target for ContainerLayer";
     return nullptr;
   }
 
   return mRenderTarget;
 }
 
 void
-ContainerLayerMLGPU::SetInvalidCompositeRect(const gfx::IntRect& aRect)
+ContainerLayerMLGPU::SetInvalidCompositeRect(const gfx::IntRect* aRect)
 {
   // For simplicity we only track the bounds of the invalid area, since regions
   // are expensive.
   //
   // Note we add the bounds to the invalid rect from the last frame, since we
   // only clear the area that we actually paint. If this overflows we use the
   // last render target size, since if that changes we'll invalidate everything
   // anyway.
-  if (Maybe<gfx::IntRect> result = mInvalidRect.SafeUnion(aRect)) {
-    mInvalidRect = result.value();
+  if (aRect) {
+    if (Maybe<gfx::IntRect> result = mInvalidRect.SafeUnion(*aRect)) {
+      mInvalidRect = result.value();
+    } else {
+      mInvalidateEntireSurface = true;
+    }
   } else {
     mInvalidateEntireSurface = true;
   }
 }
 
 void
 ContainerLayerMLGPU::ClearCachedResources()
 {
diff --git a/gfx/layers/mlgpu/ContainerLayerMLGPU.h b/gfx/layers/mlgpu/ContainerLayerMLGPU.h
--- a/gfx/layers/mlgpu/ContainerLayerMLGPU.h
+++ b/gfx/layers/mlgpu/ContainerLayerMLGPU.h
@@ -25,17 +25,17 @@ public:
 
   HostLayer* AsHostLayer() override { return this; }
   ContainerLayerMLGPU* AsContainerLayerMLGPU() override { return this; }
   Layer* GetLayer() override { return this; }
 
   void ComputeEffectiveTransforms(const gfx::Matrix4x4& aTransformToSurface) override {
     DefaultComputeEffectiveTransforms(aTransformToSurface);
   }
-  void SetInvalidCompositeRect(const gfx::IntRect &aRect) override;
+  void SetInvalidCompositeRect(const gfx::IntRect* aRect) override;
   void ClearCachedResources() override;
 
   RefPtr<MLGRenderTarget> UpdateRenderTarget(
     MLGDevice* aDevice,
     MLGRenderTargetFlags aFlags);
 
   MLGRenderTarget* GetRenderTarget() const {
     return mRenderTarget;
