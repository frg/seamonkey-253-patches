# HG changeset patch
# User Nicolas B. Pierron <nicolas.b.pierron@mozilla.com>
# Date 1502200208 0
#      Tue Aug 08 13:50:08 2017 +0000
# Node ID 487266e0b3787494a6471751f80d564dd25ebac6
# Parent  9839ea55022977fbe020d77939f2c9b5d541fe5f
Bug 1382449 - irregexp: Do not assert for stack overflow exceptions. r=bhackett

diff --git a/js/src/irregexp/RegExpEngine.cpp b/js/src/irregexp/RegExpEngine.cpp
--- a/js/src/irregexp/RegExpEngine.cpp
+++ b/js/src/irregexp/RegExpEngine.cpp
@@ -1783,17 +1783,17 @@ irregexp::CompilePattern(JSContext* cx, 
             first_step_node->AddAlternative(GuardedAlternative(char_class));
             node = first_step_node;
         } else {
             node = loop_node;
         }
     }
 
     if (compiler.isRegExpTooBig()) {
-        MOZ_ASSERT(compiler.cx()->isExceptionPending()); // over recursed
+        // This might erase the over-recurse error, if any.
         JS_ReportErrorASCII(cx, "regexp too big");
         return RegExpCode();
     }
 
     if (is_latin1) {
         node = node->FilterLATIN1(RegExpCompiler::kMaxRecursion, ignore_case, unicode);
         // Do it again to propagate the new nodes to places where they were not
         // put because they had not been calculated yet.
@@ -1907,17 +1907,17 @@ RegExpNode*
 RegExpDisjunction::ToNode(RegExpCompiler* compiler, RegExpNode* on_success)
 {
     if (!compiler->CheckOverRecursed())
         return on_success;
 
     const RegExpTreeVector& alternatives = this->alternatives();
     size_t length = alternatives.length();
     ChoiceNode* result = compiler->alloc()->newInfallible<ChoiceNode>(compiler->alloc(), length);
-    for (size_t i = 0; i < length; i++) {
+    for (size_t i = 0; i < length && !compiler->isRegExpTooBig(); i++) {
         GuardedAlternative alternative(alternatives[i]->ToNode(compiler, on_success));
         result->AddAlternative(alternative);
     }
     return result;
 }
 
 RegExpNode*
 RegExpQuantifier::ToNode(RegExpCompiler* compiler, RegExpNode* on_success)
@@ -2258,17 +2258,17 @@ RegExpCapture::ToNode(RegExpTree* body,
 RegExpNode*
 RegExpAlternative::ToNode(RegExpCompiler* compiler, RegExpNode* on_success)
 {
     if (!compiler->CheckOverRecursed())
         return on_success;
 
     const RegExpTreeVector& children = nodes();
     RegExpNode* current = on_success;
-    for (int i = children.length() - 1; i >= 0; i--)
+    for (int i = children.length() - 1; i >= 0 && !compiler->isRegExpTooBig(); i--)
         current = children[i]->ToNode(compiler, current);
     return current;
 }
 
 // -------------------------------------------------------------------
 // BoyerMooreLookahead
 
 ContainedInLattice
diff --git a/js/src/jit-test/tests/regexp/huge-01.js b/js/src/jit-test/tests/regexp/huge-01.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/regexp/huge-01.js
@@ -0,0 +1,19 @@
+
+function g(N, p) {
+    var prefix = p.repeat(N);
+    var str = prefix + "[AB]";
+
+    try {
+        var re = new RegExp(str);
+        re.exec(prefix + "A");
+    } catch(e) {
+        // 1. Regexp too big is raised by the lack of the 64k virtual registers
+        // reserved for the regexp evaluation.
+        // 2. The stack overflow can occur during the analysis of the regexp
+        assertEq(e.message.includes("regexp too big") || e.message.includes("Stack overflow"), true);
+    }
+}
+
+var prefix = "/(?=k)ok/";
+for (var i = 0; i < 18; i++)
+    g(Math.pow(2, i), prefix)
