# HG changeset patch
# User Jim Chen <nchen@mozilla.com>
# Date 1501696089 14400
#      Wed Aug 02 13:48:09 2017 -0400
# Node ID f9b9ffb6fea52259bbcf00eca45285a930f0ca59
# Parent  274953221bdfcec279aa53c02d1aa96d441a4359
Bug 1383242 - Properly compare node to traversal range under different modes; r=smaug

When the node borders one of the range bounds, `NodeIsInTraversalRange`
should return different results depending on whether it's in pre mode or
not.

>  <div><br></div>
>       \__/

In this pre mode example, the node <br> is within the range, and the
node position (which is at the start of the node in pre mode) and the
start bound are both (<div>, 0). Therefore, it shows the start bound
should be inclusive in pre mode.

>  <div><br></div>
>  \___/

In this pre mode example, the node <br> is outside of the range, yet the
node position and the end bound are both (<div>, 0). Therefore, it shows
the end bound should be exclusive in pre mode.

>  <div><br></div>
>           \____/

in this post mode example, the node <br> is outside of the range, yet
the node position (which is at the end of the node in post mode) and the
start bound are both (<div>, 1). Therefore, it shows the start bound
should be exclusive in post mode.

>  <div><br></div>
>       \__/

In this post mode example, the node <br> is within the range, and the
node position and the end bound are both (<div>, 1). Therefore, it shows
the end bound should be inclusive in post mode.

In summary, the correct pre mode bound check is `start <= node < end`,
and the correct post mode bound check is `start < node <= end`. This
patch fixes `NodeIsInTraversalRange` to have the correct bounds check.

MozReview-Commit-ID: IjJN1ua6jQ9

diff --git a/dom/base/nsContentIterator.cpp b/dom/base/nsContentIterator.cpp
--- a/dom/base/nsContentIterator.cpp
+++ b/dom/base/nsContentIterator.cpp
@@ -75,23 +75,28 @@ NodeIsInTraversalRange(nsINode* aNode, b
   if (!parent) {
     return false;
   }
 
   int32_t indx = parent->IndexOf(aNode);
   NS_WARNING_ASSERTION(indx != -1, "bad indx");
 
   if (!aIsPreMode) {
-    ++indx;
+    // Post mode: start < node <= end.
+    return nsContentUtils::ComparePoints(aStartContainer, aStartOffset,
+                                         parent, indx + 1) < 0 &&
+           nsContentUtils::ComparePoints(aEndContainer, aEndOffset,
+                                         parent, indx + 1) >= 0;
   }
 
+  // Pre mode: start <= node < end.
   return nsContentUtils::ComparePoints(aStartContainer, aStartOffset,
                                        parent, indx) <= 0 &&
          nsContentUtils::ComparePoints(aEndContainer, aEndOffset,
-                                       parent, indx) >= 0;
+                                       parent, indx) > 0;
 }
 
 
 
 /*
  *  A simple iterator class for traversing the content in "close tag" order
  */
 class nsContentIterator : public nsIContentIterator
