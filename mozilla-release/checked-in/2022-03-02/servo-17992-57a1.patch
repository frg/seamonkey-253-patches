# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1502184324 18000
#      Tue Aug 08 04:25:24 2017 -0500
# Node ID 7568e53109c8d9f99a6f4c0fe00c871bbc9533ec
# Parent  e25b4bd21ac89cccc286a51d22e5aece4e006016
servo: Merge #17992 - style: Rework how precomputed pseudo stuff works, to avoid malloc/free churn (from emilio:rework-precomputed-pseudo); r=heycam

This showed up in a few profiles, and was an easy improvement.

Source-Repo: https://github.com/servo/servo
Source-Revision: 69f02f4f7f102a3fe3f82d5f000533a9277d233e

diff --git a/servo/components/style/selector_map.rs b/servo/components/style/selector_map.rs
--- a/servo/components/style/selector_map.rs
+++ b/servo/components/style/selector_map.rs
@@ -196,39 +196,16 @@ impl SelectorMap<Rule> {
                                         flags_setter,
                                         cascade_level);
 
         // Sort only the rules we just added.
         sort_by_key(&mut matching_rules_list[init_len..],
                     |block| (block.specificity, block.source_order()));
     }
 
-    /// Append to `rule_list` all universal Rules (rules with selector `*|*`) in
-    /// `self` sorted by specificity and source order.
-    pub fn get_universal_rules(&self,
-                               cascade_level: CascadeLevel)
-                               -> Vec<ApplicableDeclarationBlock> {
-        debug_assert!(!cascade_level.is_important());
-        if self.is_empty() {
-            return vec![];
-        }
-
-        let mut rules_list = vec![];
-        for rule in self.other.iter() {
-            if rule.selector.is_universal() {
-                rules_list.push(rule.to_applicable_declaration_block(cascade_level))
-            }
-        }
-
-        sort_by_key(&mut rules_list,
-                    |block| (block.specificity, block.source_order()));
-
-        rules_list
-    }
-
     /// Adds rules in `rules` that match `element` to the `matching_rules` list.
     fn get_matching_rules<E, V, F>(element: &E,
                                    rules: &[Rule],
                                    matching_rules: &mut V,
                                    context: &mut MatchingContext,
                                    flags_setter: &mut F,
                                    cascade_level: CascadeLevel)
         where E: TElement,
diff --git a/servo/components/style/stylist.rs b/servo/components/style/stylist.rs
--- a/servo/components/style/stylist.rs
+++ b/servo/components/style/stylist.rs
@@ -398,23 +398,16 @@ impl Stylist {
         let sheets_to_add = doc_stylesheets.filter(|s| {
             !author_style_disabled || s.origin(guards.author) != Origin::Author
         });
 
         for stylesheet in sheets_to_add {
             self.add_stylesheet(stylesheet, guards.author, extra_data);
         }
 
-        SelectorImpl::each_precomputed_pseudo_element(|pseudo| {
-            if let Some(map) = self.cascade_data.user_agent.pseudos_map.remove(&pseudo) {
-                let declarations = map.get_universal_rules(CascadeLevel::UANormal);
-                self.precomputed_pseudo_element_decls.insert(pseudo, declarations);
-            }
-        });
-
         self.is_device_dirty = false;
         true
     }
 
     /// clear the stylist and then rebuild it.  Chances are, you want to use
     /// either clear() or rebuild(), with the latter done lazily, instead.
     pub fn update<'a, 'b, I, S>(
         &mut self,
@@ -465,27 +458,58 @@ impl Stylist {
         for rule in stylesheet.effective_rules(&self.device, guard) {
             match *rule {
                 CssRule::Style(ref locked) => {
                     let style_rule = locked.read_with(&guard);
                     self.num_declarations += style_rule.block.read_with(&guard).len();
                     for selector in &style_rule.selectors.0 {
                         self.num_selectors += 1;
 
+                        let map = match selector.pseudo_element() {
+                            Some(pseudo) if pseudo.is_precomputed() => {
+                                if !selector.is_universal() ||
+                                   !matches!(origin, Origin::UserAgent) {
+                                    // ::-moz-tree selectors may appear in
+                                    // non-UA sheets (even though they never
+                                    // match).
+                                    continue;
+                                }
+
+                                self.precomputed_pseudo_element_decls
+                                    .entry(pseudo.canonical())
+                                    .or_insert_with(Vec::new)
+                                    .push(ApplicableDeclarationBlock::new(
+                                        StyleSource::Style(locked.clone()),
+                                        self.rules_source_order,
+                                        CascadeLevel::UANormal,
+                                        selector.specificity()
+                                    ));
+
+                                continue;
+                            }
+                            None => &mut origin_cascade_data.element_map,
+                            Some(pseudo) => {
+                                origin_cascade_data
+                                    .pseudos_map
+                                    .entry(pseudo.canonical())
+                                    .or_insert_with(SelectorMap::new)
+                            }
+                        };
+
                         let hashes =
                             AncestorHashes::new(&selector, self.quirks_mode);
 
-                        origin_cascade_data
-                            .borrow_mut_for_pseudo_or_insert(selector.pseudo_element())
-                            .insert(
-                                Rule::new(selector.clone(),
-                                          hashes.clone(),
-                                          locked.clone(),
-                                          self.rules_source_order),
-                                self.quirks_mode);
+                        let rule = Rule::new(
+                            selector.clone(),
+                            hashes.clone(),
+                            locked.clone(),
+                            self.rules_source_order
+                        );
+
+                        map.insert(rule, self.quirks_mode);
 
                         self.invalidation_map.note_selector(selector, self.quirks_mode);
                         let mut visitor = StylistSelectorVisitor {
                             needs_revalidation: false,
                             passed_rightmost_selector: false,
                             attribute_dependencies: &mut self.attribute_dependencies,
                             style_attribute_dependency: &mut self.style_attribute_dependency,
                             state_dependencies: &mut self.state_dependencies,
@@ -1655,28 +1679,16 @@ impl PerOriginCascadeData {
     #[inline]
     fn borrow_for_pseudo(&self, pseudo: Option<&PseudoElement>) -> Option<&SelectorMap<Rule>> {
         match pseudo {
             Some(pseudo) => self.pseudos_map.get(&pseudo.canonical()),
             None => Some(&self.element_map),
         }
     }
 
-    #[inline]
-    fn borrow_mut_for_pseudo_or_insert(&mut self, pseudo: Option<&PseudoElement>) -> &mut SelectorMap<Rule> {
-        match pseudo {
-            Some(pseudo) => {
-                self.pseudos_map
-                    .entry(pseudo.canonical())
-                    .or_insert_with(SelectorMap::new)
-            }
-            None => &mut self.element_map,
-        }
-    }
-
     fn clear(&mut self) {
         *self = Self::new();
     }
 
     fn has_rules_for_pseudo(&self, pseudo: &PseudoElement) -> bool {
         // FIXME(emilio): We should probably make the pseudos map be an
         // enumerated array.
         self.pseudos_map.contains_key(pseudo)
@@ -1715,24 +1727,27 @@ impl SelectorMapEntry for Rule {
 impl Rule {
     /// Returns the specificity of the rule.
     pub fn specificity(&self) -> u32 {
         self.selector.specificity()
     }
 
     /// Turns this rule into an `ApplicableDeclarationBlock` for the given
     /// cascade level.
-    pub fn to_applicable_declaration_block(&self,
-                                           level: CascadeLevel)
-                                           -> ApplicableDeclarationBlock {
+    pub fn to_applicable_declaration_block(
+        &self,
+        level: CascadeLevel
+    ) -> ApplicableDeclarationBlock {
         let source = StyleSource::Style(self.style_rule.clone());
-        ApplicableDeclarationBlock::new(source,
-                                        self.source_order,
-                                        level,
-                                        self.specificity())
+        ApplicableDeclarationBlock::new(
+            source,
+            self.source_order,
+            level,
+            self.specificity()
+        )
     }
 
     /// Creates a new Rule.
     pub fn new(selector: Selector<SelectorImpl>,
                hashes: AncestorHashes,
                style_rule: Arc<Locked<StyleRule>>,
                source_order: u32)
                -> Self
diff --git a/servo/tests/unit/style/stylist.rs b/servo/tests/unit/style/stylist.rs
--- a/servo/tests/unit/style/stylist.rs
+++ b/servo/tests/unit/style/stylist.rs
@@ -217,26 +217,16 @@ fn test_insert() {
     let mut selector_map = SelectorMap::new();
     selector_map.insert(rules_list[1][0].clone(), QuirksMode::NoQuirks);
     assert_eq!(1, selector_map.id_hash.get(&Atom::from("top"), QuirksMode::NoQuirks).unwrap()[0].source_order);
     selector_map.insert(rules_list[0][0].clone(), QuirksMode::NoQuirks);
     assert_eq!(0, selector_map.class_hash.get(&Atom::from("intro"), QuirksMode::NoQuirks).unwrap()[0].source_order);
     assert!(selector_map.class_hash.get(&Atom::from("foo"), QuirksMode::NoQuirks).is_none());
 }
 
-#[test]
-fn test_get_universal_rules() {
-    thread_state::initialize(thread_state::LAYOUT);
-    let (map, _shared_lock) = get_mock_map(&["*|*", "#foo > *|*", "*|* > *|*", ".klass", "#id"]);
-
-    let decls = map.get_universal_rules(CascadeLevel::UserNormal);
-
-    assert_eq!(decls.len(), 1, "{:?}", decls);
-}
-
 fn mock_stylist() -> Stylist {
     let device = Device::new(MediaType::Screen, TypedSize2D::new(0f32, 0f32), ScaleFactor::new(1.0));
     Stylist::new(device, QuirksMode::NoQuirks)
 }
 
 #[test]
 fn test_stylist_device_accessors() {
     let stylist = mock_stylist();
