# HG changeset patch
# User Brendan Dahl <brendan.dahl@gmail.com>
# Date 1501786317 25200
#      Thu Aug 03 11:51:57 2017 -0700
# Node ID 1de7f5d2deccc57ac8f8f4eb4c92baa92823c123
# Parent  15a8cf2d8ed8202be4716d348fc273b5d0b6d21c
Bug 1387168 - Use custom clipboard constructor instead of singleton. r=jrmuizel

This allows instances of the clipboard to be created (like it
was pre-headless).

diff --git a/widget/windows/nsClipboard.cpp b/widget/windows/nsClipboard.cpp
--- a/widget/windows/nsClipboard.cpp
+++ b/widget/windows/nsClipboard.cpp
@@ -30,49 +30,25 @@
 #include "nsIComponentManager.h"
 #include "nsWidgetsCID.h"
 #include "nsCRT.h"
 #include "nsNetUtil.h"
 #include "nsIFileProtocolHandler.h"
 #include "nsIOutputStream.h"
 #include "nsEscape.h"
 #include "nsIObserverService.h"
-#include "HeadlessClipboard.h"
-#include "mozilla/ClearOnShutdown.h"
 
 using mozilla::LogLevel;
 
 static mozilla::LazyLogModule gWin32ClipboardLog("nsClipboard");
 
 // oddly, this isn't in the MSVC headers anywhere.
 UINT nsClipboard::CF_HTML = ::RegisterClipboardFormatW(L"HTML Format");
 UINT nsClipboard::CF_CUSTOMTYPES = ::RegisterClipboardFormatW(L"application/x-moz-custom-clipdata");
 
-namespace mozilla {
-namespace clipboard {
-StaticRefPtr<nsIClipboard> sInstance;
-}
-}
-/* static */ already_AddRefed<nsIClipboard>
-nsClipboard::GetInstance()
-{
-  using namespace mozilla::clipboard;
-
-  if (!sInstance) {
-    if (gfxPlatform::IsHeadless()) {
-      sInstance = new widget::HeadlessClipboard();
-    } else {
-      sInstance = new nsClipboard();
-    }
-    ClearOnShutdown(&sInstance);
-  }
-
-  RefPtr<nsIClipboard> service = sInstance.get();
-  return service.forget();
-}
 
 //-------------------------------------------------------------------------
 //
 // nsClipboard constructor
 //
 //-------------------------------------------------------------------------
 nsClipboard::nsClipboard() : nsBaseClipboard()
 {
diff --git a/widget/windows/nsClipboard.h b/widget/windows/nsClipboard.h
--- a/widget/windows/nsClipboard.h
+++ b/widget/windows/nsClipboard.h
@@ -28,18 +28,16 @@ class nsClipboard : public nsBaseClipboa
 public:
   nsClipboard();
 
   NS_DECL_ISUPPORTS_INHERITED
 
   // nsIObserver
   NS_DECL_NSIOBSERVER
 
-  static already_AddRefed<nsIClipboard> GetInstance();
-
   // nsIClipboard
   NS_IMETHOD HasDataMatchingFlavors(const char** aFlavorList, uint32_t aLength,
                                     int32_t aWhichClipboard, bool *_retval) override; 
   NS_IMETHOD EmptyClipboard(int32_t aWhichClipboard) override;
 
   // Internal Native Routines
   static nsresult CreateNativeDataObject(nsITransferable * aTransferable, 
                                          IDataObject ** aDataObj,
diff --git a/widget/windows/nsWidgetFactory.cpp b/widget/windows/nsWidgetFactory.cpp
--- a/widget/windows/nsWidgetFactory.cpp
+++ b/widget/windows/nsWidgetFactory.cpp
@@ -30,16 +30,17 @@
 #include "nsNativeThemeWin.h"
 #include "nsWindow.h"
 // Content processes
 #include "nsFilePickerProxy.h"
 
 // Drag & Drop, Clipboard
 #include "nsClipboardHelper.h"
 #include "nsClipboard.h"
+#include "HeadlessClipboard.h"
 #include "nsBidiKeyboard.h"
 #include "nsDragService.h"
 #include "nsTransferable.h"
 #include "nsHTMLFormatConverter.h"
 
 #include "WinTaskbar.h"
 #include "JumpListBuilder.h"
 #include "JumpListItem.h"
@@ -98,19 +99,35 @@ ColorPickerConstructor(nsISupports *aOut
   *aResult = nullptr;
   if (aOuter != nullptr) {
     return NS_ERROR_NO_AGGREGATION;
   }
   nsCOMPtr<nsIColorPicker> picker = new nsColorPicker;
   return picker->QueryInterface(aIID, aResult);
 }
 
+static nsresult
+nsClipboardConstructor(nsISupports *aOuter, REFNSIID aIID,
+                       void **aResult)
+{
+  *aResult = nullptr;
+  if (aOuter != nullptr) {
+    return NS_ERROR_NO_AGGREGATION;
+  }
+  nsCOMPtr<nsIClipboard> inst;
+  if (gfxPlatform::IsHeadless()) {
+    inst = new HeadlessClipboard();
+  } else {
+    inst = new nsClipboard();
+  }
+  return inst->QueryInterface(aIID, aResult);
+}
+
 NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(ScreenManager, ScreenManager::GetAddRefedSingleton)
 NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(nsIdleServiceWin, nsIdleServiceWin::GetInstance)
-NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(nsIClipboard, nsClipboard::GetInstance)
 NS_GENERIC_FACTORY_SINGLETON_CONSTRUCTOR(nsISound, nsSound::GetInstance)
 NS_GENERIC_FACTORY_CONSTRUCTOR(nsClipboardHelper)
 NS_GENERIC_FACTORY_CONSTRUCTOR(WinTaskbar)
 NS_GENERIC_FACTORY_CONSTRUCTOR(JumpListBuilder)
 NS_GENERIC_FACTORY_CONSTRUCTOR(JumpListItem)
 NS_GENERIC_FACTORY_CONSTRUCTOR(JumpListSeparator)
 NS_GENERIC_FACTORY_CONSTRUCTOR(JumpListLink)
 NS_GENERIC_FACTORY_CONSTRUCTOR(JumpListShortcut)
@@ -172,17 +189,17 @@ static const mozilla::Module::CIDEntry k
   { &kNS_FILEPICKER_CID, false, nullptr, FilePickerConstructor, Module::MAIN_PROCESS_ONLY },
   { &kNS_COLORPICKER_CID, false, nullptr, ColorPickerConstructor, Module::MAIN_PROCESS_ONLY },
   { &kNS_APPSHELL_CID, false, nullptr, nsAppShellConstructor, Module::ALLOW_IN_GPU_PROCESS },
   { &kNS_SCREENMANAGER_CID, false, nullptr, ScreenManagerConstructor,
     Module::MAIN_PROCESS_ONLY },
   { &kNS_GFXINFO_CID, false, nullptr, GfxInfoConstructor, Module::ALLOW_IN_GPU_PROCESS },
   { &kNS_THEMERENDERER_CID, false, nullptr, NS_NewNativeTheme },
   { &kNS_IDLE_SERVICE_CID, false, nullptr, nsIdleServiceWinConstructor },
-  { &kNS_CLIPBOARD_CID, false, nullptr, nsIClipboardConstructor, Module::MAIN_PROCESS_ONLY },
+  { &kNS_CLIPBOARD_CID, false, nullptr, nsClipboardConstructor, Module::MAIN_PROCESS_ONLY },
   { &kNS_CLIPBOARDHELPER_CID, false, nullptr, nsClipboardHelperConstructor },
   { &kNS_SOUND_CID, false, nullptr, nsISoundConstructor, Module::MAIN_PROCESS_ONLY },
   { &kNS_TRANSFERABLE_CID, false, nullptr, nsTransferableConstructor },
   { &kNS_HTMLFORMATCONVERTER_CID, false, nullptr, nsHTMLFormatConverterConstructor },
   { &kNS_WIN_TASKBAR_CID, false, nullptr, WinTaskbarConstructor },
   { &kNS_WIN_JUMPLISTBUILDER_CID, false, nullptr, JumpListBuilderConstructor },
   { &kNS_WIN_JUMPLISTITEM_CID, false, nullptr, JumpListItemConstructor },
   { &kNS_WIN_JUMPLISTSEPARATOR_CID, false, nullptr, JumpListSeparatorConstructor },
