# HG changeset patch
# User David Anderson <danderson@mozilla.com>
# Date 1509476550 25200
# Node ID 93d6f274e108669c08f5a01bd48043a5decf2bbc
# Parent  52c0c40046097184992d471e97c3d8a8bc3df632
Create a CaptureCommandList abstraction for DrawTargetCapture. (bug 1395478 part 4, r=mchang)

diff --git a/gfx/2d/CaptureCommandList.cpp b/gfx/2d/CaptureCommandList.cpp
new file mode 100644
--- /dev/null
+++ b/gfx/2d/CaptureCommandList.cpp
@@ -0,0 +1,21 @@
+/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
+ * This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "CaptureCommandList.h"
+#include "DrawCommand.h"
+
+namespace mozilla {
+namespace gfx {
+
+CaptureCommandList::~CaptureCommandList()
+{
+  for (iterator iter(*this); !iter.Done(); iter.Next()) {
+    DrawingCommand* cmd = iter.Get();
+    cmd->~DrawingCommand();
+  }
+}
+
+} // namespace gfx
+} // namespace mozilla
diff --git a/gfx/2d/CaptureCommandList.h b/gfx/2d/CaptureCommandList.h
new file mode 100644
--- /dev/null
+++ b/gfx/2d/CaptureCommandList.h
@@ -0,0 +1,84 @@
+/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
+ * This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef mozilla_gfx_2d_CaptureCommandList_h
+#define mozilla_gfx_2d_CaptureCommandList_h
+
+#include "mozilla/Move.h"
+#include "mozilla/PodOperations.h"
+#include <vector>
+
+namespace mozilla {
+namespace gfx {
+
+class DrawingCommand;
+
+class CaptureCommandList
+{
+public:
+  CaptureCommandList()
+  {}
+  CaptureCommandList(CaptureCommandList&& aOther)
+   : mStorage(Move(aOther.mStorage))
+  {}
+  ~CaptureCommandList();
+
+  CaptureCommandList& operator =(CaptureCommandList&& aOther) {
+    mStorage = Move(aOther.mStorage);
+    return *this;
+  }
+
+  template <typename T>
+  T* Append() {
+    size_t oldSize = mStorage.size();
+    mStorage.resize(mStorage.size() + sizeof(T) + sizeof(uint32_t));
+    uint8_t* nextDrawLocation = &mStorage.front() + oldSize;
+    *(uint32_t*)(nextDrawLocation) = sizeof(T) + sizeof(uint32_t);
+    return reinterpret_cast<T*>(nextDrawLocation + sizeof(uint32_t));
+  }
+
+  class iterator
+  {
+  public:
+    explicit iterator(CaptureCommandList& aParent)
+     : mParent(aParent),
+       mCurrent(nullptr),
+       mEnd(nullptr)
+    {
+      if (!mParent.mStorage.empty()) {
+        mCurrent = &mParent.mStorage.front();
+        mEnd = mCurrent + mParent.mStorage.size();
+      }
+    }
+    bool Done() const {
+      return mCurrent >= mEnd;
+    }
+    void Next() {
+      MOZ_ASSERT(!Done());
+      mCurrent += *reinterpret_cast<uint32_t*>(mCurrent);
+    }
+    DrawingCommand* Get() {
+      MOZ_ASSERT(!Done());
+      return reinterpret_cast<DrawingCommand*>(mCurrent + sizeof(uint32_t));
+    }
+
+  private:
+    CaptureCommandList& mParent;
+    uint8_t* mCurrent;
+    uint8_t* mEnd;
+  };
+
+private:
+  CaptureCommandList(const CaptureCommandList& aOther) = delete;
+  void operator =(const CaptureCommandList& aOther) = delete;
+
+private:
+  std::vector<uint8_t> mStorage;
+};
+
+} // namespace gfx
+} // namespace mozilla
+
+#endif // mozilla_gfx_2d_CaptureCommandList_h
diff --git a/gfx/2d/DrawTargetCapture.cpp b/gfx/2d/DrawTargetCapture.cpp
--- a/gfx/2d/DrawTargetCapture.cpp
+++ b/gfx/2d/DrawTargetCapture.cpp
@@ -9,24 +9,16 @@
 #include "gfxPlatform.h"
 
 namespace mozilla {
 namespace gfx {
 
 
 DrawTargetCaptureImpl::~DrawTargetCaptureImpl()
 {
-  uint8_t* start = &mDrawCommandStorage.front();
-
-  uint8_t* current = start;
-
-  while (current < start + mDrawCommandStorage.size()) {
-    reinterpret_cast<DrawingCommand*>(current + sizeof(uint32_t))->~DrawingCommand();
-    current += *(uint32_t*)current;
-  }
 }
 
 DrawTargetCaptureImpl::DrawTargetCaptureImpl(BackendType aBackend,
                                              const IntSize& aSize,
                                              SurfaceFormat aFormat)
   : mSize(aSize),
     mStride(0),
     mSurfaceAllocationSize(0)
@@ -312,39 +304,31 @@ DrawTargetCaptureImpl::Blur(const AlphaB
   MOZ_ASSERT(GetBackendType() == BackendType::SKIA);
 
   AppendCommand(BlurCommand)(aBlur);
 }
 
 void
 DrawTargetCaptureImpl::ReplayToDrawTarget(DrawTarget* aDT, const Matrix& aTransform)
 {
-  uint8_t* start = &mDrawCommandStorage.front();
-
-  uint8_t* current = start;
-
-  while (current < start + mDrawCommandStorage.size()) {
-    reinterpret_cast<DrawingCommand*>(current + sizeof(uint32_t))->ExecuteOnDT(aDT, &aTransform);
-    current += *(uint32_t*)current;
+  for (CaptureCommandList::iterator iter(mCommands); !iter.Done(); iter.Next()) {
+    DrawingCommand* cmd = iter.Get();
+    cmd->ExecuteOnDT(aDT, &aTransform);
   }
 }
 
 bool
 DrawTargetCaptureImpl::ContainsOnlyColoredGlyphs(RefPtr<ScaledFont>& aScaledFont,
                                                  Color& aColor,
                                                  std::vector<Glyph>& aGlyphs)
 {
-  uint8_t* start = &mDrawCommandStorage.front();
-  uint8_t* current = start;
   bool result = false;
 
-  while (current < start + mDrawCommandStorage.size()) {
-    DrawingCommand* command =
-      reinterpret_cast<DrawingCommand*>(current + sizeof(uint32_t));
-    current += *(uint32_t*)current;
+  for (CaptureCommandList::iterator iter(mCommands); !iter.Done(); iter.Next()) {
+    DrawingCommand* command = iter.Get();
 
     if (command->GetType() != CommandType::FILLGLYPHS &&
         command->GetType() != CommandType::SETTRANSFORM) {
       return false;
     }
 
     if (command->GetType() == CommandType::SETTRANSFORM) {
       SetTransformCommand* transform = static_cast<SetTransformCommand*>(command);
diff --git a/gfx/2d/DrawTargetCapture.h b/gfx/2d/DrawTargetCapture.h
--- a/gfx/2d/DrawTargetCapture.h
+++ b/gfx/2d/DrawTargetCapture.h
@@ -3,17 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef MOZILLA_GFX_DRAWTARGETCAPTURE_H_
 #define MOZILLA_GFX_DRAWTARGETCAPTURE_H_
 
 #include "2D.h"
-#include <vector>
+#include "CaptureCommandList.h"
 
 #include "Filters.h"
 
 namespace mozilla {
 namespace gfx {
 
 class DrawingCommand;
 class AlphaBoxBlur;
@@ -156,40 +156,35 @@ protected:
   virtual ~DrawTargetCaptureImpl();
 
 private:
 
   // This storage system was used to minimize the amount of heap allocations
   // that are required while recording. It should be noted there's no
   // guarantees on the alignments of DrawingCommands allocated in this array.
   template<typename T>
-  T* AppendToCommandList()
-  {
-    size_t oldSize = mDrawCommandStorage.size();
-    mDrawCommandStorage.resize(mDrawCommandStorage.size() + sizeof(T) + sizeof(uint32_t));
-    uint8_t* nextDrawLocation = &mDrawCommandStorage.front() + oldSize;
-    *(uint32_t*)(nextDrawLocation) = sizeof(T) + sizeof(uint32_t);
-    return reinterpret_cast<T*>(nextDrawLocation + sizeof(uint32_t));
+  T* AppendToCommandList() {
+    return mCommands.Append<T>();
   }
+
   RefPtr<DrawTarget> mRefDT;
   IntSize mSize;
 
   // These are set if the draw target must be explicitly backed by data.
   int32_t mStride;
   size_t mSurfaceAllocationSize;
 
   struct PushedLayer
   {
     explicit PushedLayer(bool aOldPermitSubpixelAA)
       : mOldPermitSubpixelAA(aOldPermitSubpixelAA)
     {}
     bool mOldPermitSubpixelAA;
   };
   std::vector<PushedLayer> mPushedLayers;
-  std::vector<uint8_t> mDrawCommandStorage;
+
+  CaptureCommandList mCommands;
 };
 
 } // namespace gfx
-
 } // namespace mozilla
 
-
 #endif /* MOZILLA_GFX_DRAWTARGETCAPTURE_H_ */
diff --git a/gfx/2d/ScaledFontMac.cpp b/gfx/2d/ScaledFontMac.cpp
--- a/gfx/2d/ScaledFontMac.cpp
+++ b/gfx/2d/ScaledFontMac.cpp
@@ -13,16 +13,17 @@
 #include "skia/include/ports/SkTypeface_mac.h"
 #endif
 #include <vector>
 #include <dlfcn.h>
 #ifdef MOZ_WIDGET_UIKIT
 #include <CoreFoundation/CoreFoundation.h>
 #endif
 #include "nsCocoaFeatures.h"
+#include "mozilla/gfx/Logging.h"
 
 #ifdef MOZ_WIDGET_COCOA
 // prototype for private API
 extern "C" {
 CGPathRef CGFontGetGlyphPath(CGFontRef fontRef, CGAffineTransform *textTransform, int unknown, CGGlyph glyph);
 };
 #endif
 
diff --git a/gfx/2d/moz.build b/gfx/2d/moz.build
--- a/gfx/2d/moz.build
+++ b/gfx/2d/moz.build
@@ -149,16 +149,17 @@ if CONFIG['INTEL_ARCHITECTURE']:
 elif CONFIG['CPU_ARCH'].startswith('mips'):
     SOURCES += [
         'BlurLS3.cpp',
     ]
 
 UNIFIED_SOURCES += [
     'BezierUtils.cpp',
     'Blur.cpp',
+    'CaptureCommandList.cpp',
     'DataSourceSurface.cpp',
     'DataSurfaceHelpers.cpp',
     'DrawEventRecorder.cpp',
     'DrawingJob.cpp',
     'DrawTarget.cpp',
     'DrawTargetCairo.cpp',
     'DrawTargetCapture.cpp',
     'DrawTargetDual.cpp',
