# HG changeset patch
# User Mantaroh Yoshinaga <mantaroh@gmail.com>
# Date 1503031140 18000
#      Thu Aug 17 23:39:00 2017 -0500
# Node ID 82041bd92f97430b37aa4e59430e21913d3bcb45
# Parent  c1081e912b836132f1b5a706d9df975915fe4b0a
servo: Merge #17704 - Add animation value related with stroke-dasharray / stroke-dashoffset / stroke-width (from mantaroh:interpolate-stroke); r=nox

<!-- Please describe your changes on the following line: -->
This is a PR for https://bugzilla.mozilla.org/show_bug.cgi?id=1369614

This patch will:
 * Add animation value conversion of LengthOrPercentage in order to interpolate length and number.
 * Add animation value of Vec<LengthOrPercentage> in order to interpolate the stroke-dasharray.

Spec is as follow:
https://svgwg.org/svg2-draft/painting.html#StrokeDashing

---
<!-- Thank you for contributing to Servo! Please replace each `[ ]` by `[X]` when the step is complete, and replace `__` with appropriate data: -->
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors

<!-- Either: -->
There are tests for these changes, a test case will be landed in wpt in https://bugzilla.mozilla.org/show_bug.cgi?id=1369614.

<!-- Also, please make sure that "Allow edits from maintainers" checkbox is checked, so that we can help you if you get stuck somewhere along the way.-->

<!-- Pull requests that do not address these steps are welcome, but they will require additional verification as part of the review process. -->

Source-Repo: https://github.com/servo/servo
Source-Revision: d17f27640be455c91daaf8733289dca90eb505c5

diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -583,66 +583,74 @@ def set_gecko_property(ffi_name, expr):
     % endif
 </%def>
 
 <%def name="impl_svg_length(ident, gecko_ffi_name, need_clone=False)">
     // When context-value is used on an SVG length, the corresponding flag is
     // set on mContextFlags, and the length field is set to the initial value.
 
     pub fn set_${ident}(&mut self, v: longhands::${ident}::computed_value::T) {
-        use values::generics::svg::SVGLength;
+        use values::generics::svg::{SVGLength, SvgLengthOrPercentageOrNumber};
         use gecko_bindings::structs::nsStyleSVG_${ident.upper()}_CONTEXT as CONTEXT_VALUE;
         let length = match v {
             SVGLength::Length(length) => {
                 self.gecko.mContextFlags &= !CONTEXT_VALUE;
                 length
             }
             SVGLength::ContextValue => {
                 self.gecko.mContextFlags |= CONTEXT_VALUE;
                 match longhands::${ident}::get_initial_value() {
                     SVGLength::Length(length) => length,
                     _ => unreachable!("Initial value should not be context-value"),
                 }
             }
         };
         match length {
-            Either::First(number) =>
-                self.gecko.${gecko_ffi_name}.set_value(CoordDataValue::Factor(From::from(number))),
-            Either::Second(lop) => self.gecko.${gecko_ffi_name}.set(lop),
+            SvgLengthOrPercentageOrNumber::LengthOrPercentage(lop) =>
+                self.gecko.${gecko_ffi_name}.set(lop),
+            SvgLengthOrPercentageOrNumber::Number(num) =>
+                self.gecko.${gecko_ffi_name}.set_value(CoordDataValue::Factor(num.into())),
         }
     }
 
     pub fn copy_${ident}_from(&mut self, other: &Self) {
         use gecko_bindings::structs::nsStyleSVG_${ident.upper()}_CONTEXT as CONTEXT_VALUE;
         self.gecko.${gecko_ffi_name}.copy_from(&other.gecko.${gecko_ffi_name});
         self.gecko.mContextFlags =
             (self.gecko.mContextFlags & !CONTEXT_VALUE) |
             (other.gecko.mContextFlags & CONTEXT_VALUE);
     }
 
     pub fn reset_${ident}(&mut self, other: &Self) {
         self.copy_${ident}_from(other)
     }
 
     pub fn clone_${ident}(&self) -> longhands::${ident}::computed_value::T {
-        use values::generics::svg::SVGLength;
+        use values::generics::svg::{SVGLength, SvgLengthOrPercentageOrNumber};
         use values::computed::LengthOrPercentage;
         use gecko_bindings::structs::nsStyleSVG_${ident.upper()}_CONTEXT as CONTEXT_VALUE;
         if (self.gecko.mContextFlags & CONTEXT_VALUE) != 0 {
             return SVGLength::ContextValue;
         }
         let length = match self.gecko.${gecko_ffi_name}.as_value() {
-            CoordDataValue::Factor(number) => Either::First(From::from(number)),
-            CoordDataValue::Coord(coord) => Either::Second(From::from(LengthOrPercentage::Length(Au(coord)))),
-            CoordDataValue::Percent(p) => Either::Second(From::from(LengthOrPercentage::Percentage(Percentage(p)))),
-            CoordDataValue::Calc(calc) => Either::Second(From::from(LengthOrPercentage::Calc(calc.into()))),
+            CoordDataValue::Factor(number) =>
+                SvgLengthOrPercentageOrNumber::Number(number),
+            CoordDataValue::Coord(coord) =>
+                SvgLengthOrPercentageOrNumber::LengthOrPercentage(
+                    LengthOrPercentage::Length(Au(coord))),
+            CoordDataValue::Percent(p) =>
+                SvgLengthOrPercentageOrNumber::LengthOrPercentage(
+                    LengthOrPercentage::Percentage(Percentage(p))),
+            CoordDataValue::Calc(calc) =>
+                SvgLengthOrPercentageOrNumber::LengthOrPercentage(
+                    LengthOrPercentage::Calc(calc.into())),
             _ => unreachable!("Unexpected coordinate {:?} in ${ident}",
                               self.gecko.${gecko_ffi_name}.as_value()),
         };
-        SVGLength::Length(length)
+        SVGLength::Length(length.into())
     }
 </%def>
 
 <%def name="impl_svg_opacity(ident, gecko_ffi_name, need_clone=False)">
     <% source_prefix = ident.split("_")[0].upper() + "_OPACITY_SOURCE" %>
 
     pub fn set_${ident}(&mut self, v: longhands::${ident}::computed_value::T) {
         use gecko_bindings::structs::nsStyleSVG_${source_prefix}_MASK as MASK;
@@ -5136,29 +5144,31 @@ clip-path
                 };
             order |= value << (pos * SHIFT);
         };
         T(order)
     }
 
     pub fn set_stroke_dasharray(&mut self, v: longhands::stroke_dasharray::computed_value::T) {
         use gecko_bindings::structs::nsStyleSVG_STROKE_DASHARRAY_CONTEXT as CONTEXT_VALUE;
-        use values::generics::svg::SVGStrokeDashArray;
+        use values::generics::svg::{SVGStrokeDashArray, SvgLengthOrPercentageOrNumber};
 
         match v {
             SVGStrokeDashArray::Values(v) => {
                 let v = v.into_iter();
                 self.gecko.mContextFlags &= !CONTEXT_VALUE;
                 unsafe {
                     bindings::Gecko_nsStyleSVG_SetDashArrayLength(&mut self.gecko, v.len() as u32);
                 }
                 for (gecko, servo) in self.gecko.mStrokeDasharray.iter_mut().zip(v) {
                     match servo {
-                        Either::First(number) => gecko.set_value(CoordDataValue::Factor(number.into())),
-                        Either::Second(lop) => gecko.set(lop),
+                        SvgLengthOrPercentageOrNumber::LengthOrPercentage(lop) =>
+                            gecko.set(lop),
+                        SvgLengthOrPercentageOrNumber::Number(num) =>
+                            gecko.set_value(CoordDataValue::Factor(num.into())),
                     }
                 }
             }
             SVGStrokeDashArray::ContextValue => {
                 self.gecko.mContextFlags |= CONTEXT_VALUE;
                 unsafe {
                     bindings::Gecko_nsStyleSVG_SetDashArrayLength(&mut self.gecko, 0);
                 }
@@ -5178,32 +5188,36 @@ clip-path
 
     pub fn reset_stroke_dasharray(&mut self, other: &Self) {
         self.copy_stroke_dasharray_from(other)
     }
 
     pub fn clone_stroke_dasharray(&self) -> longhands::stroke_dasharray::computed_value::T {
         use gecko_bindings::structs::nsStyleSVG_STROKE_DASHARRAY_CONTEXT as CONTEXT_VALUE;
         use values::computed::LengthOrPercentage;
-        use values::generics::svg::SVGStrokeDashArray;
+        use values::generics::svg::{SVGStrokeDashArray, SvgLengthOrPercentageOrNumber};
 
         if self.gecko.mContextFlags & CONTEXT_VALUE != 0 {
             debug_assert_eq!(self.gecko.mStrokeDasharray.len(), 0);
             return SVGStrokeDashArray::ContextValue;
         }
         let mut vec = vec![];
         for gecko in self.gecko.mStrokeDasharray.iter() {
             match gecko.as_value() {
-                CoordDataValue::Factor(number) => vec.push(Either::First(number.into())),
+                CoordDataValue::Factor(number) =>
+                    vec.push(SvgLengthOrPercentageOrNumber::Number(number.into())),
                 CoordDataValue::Coord(coord) =>
-                    vec.push(Either::Second(LengthOrPercentage::Length(Au(coord)).into())),
+                    vec.push(SvgLengthOrPercentageOrNumber::LengthOrPercentage(
+                        LengthOrPercentage::Length(Au(coord)).into())),
                 CoordDataValue::Percent(p) =>
-                    vec.push(Either::Second(LengthOrPercentage::Percentage(Percentage(p)).into())),
+                    vec.push(SvgLengthOrPercentageOrNumber::LengthOrPercentage(
+                        LengthOrPercentage::Percentage(Percentage(p)).into())),
                 CoordDataValue::Calc(calc) =>
-                    vec.push(Either::Second(LengthOrPercentage::Calc(calc.into()).into())),
+                    vec.push(SvgLengthOrPercentageOrNumber::LengthOrPercentage(
+                        LengthOrPercentage::Calc(calc.into()).into())),
                 _ => unreachable!(),
             }
         }
         SVGStrokeDashArray::Values(vec)
     }
 
     #[allow(non_snake_case)]
     pub fn set__moz_context_properties<I>(&mut self, v: I)
diff --git a/servo/components/style/properties/helpers/animated_properties.mako.rs b/servo/components/style/properties/helpers/animated_properties.mako.rs
--- a/servo/components/style/properties/helpers/animated_properties.mako.rs
+++ b/servo/components/style/properties/helpers/animated_properties.mako.rs
@@ -38,28 +38,30 @@ use super::ComputedValues;
 use values::Auto;
 use values::{CSSFloat, CustomIdent, Either};
 use values::animated::{ToAnimatedValue, ToAnimatedZero};
 use values::animated::color::{Color as AnimatedColor, RGBA as AnimatedRGBA};
 use values::animated::effects::BoxShadowList as AnimatedBoxShadowList;
 use values::animated::effects::Filter as AnimatedFilter;
 use values::animated::effects::FilterList as AnimatedFilterList;
 use values::animated::effects::TextShadowList as AnimatedTextShadowList;
-use values::computed::{Angle, LengthOrPercentageOrAuto, LengthOrPercentageOrNone};
-use values::computed::{BorderCornerRadius, ClipRect};
-use values::computed::{CalcLengthOrPercentage, Context, ComputedValueAsSpecified, ComputedUrl};
-use values::computed::{LengthOrPercentage, MaxLength, MozLength, Percentage, ToComputedValue};
-use values::computed::{NonNegativeAu, NonNegativeNumber, PositiveIntegerOrAuto};
+use values::computed::{Angle, BorderCornerRadius, CalcLengthOrPercentage};
+use values::computed::{ClipRect, Context, ComputedUrl, ComputedValueAsSpecified};
+use values::computed::{LengthOrPercentage, LengthOrPercentageOrAuto};
+use values::computed::{LengthOrPercentageOrNone, MaxLength, MozLength, NonNegativeAu};
+use values::computed::{NonNegativeNumber, Number, NumberOrPercentage, Percentage};
+use values::computed::{PositiveIntegerOrAuto, ToComputedValue};
 use values::computed::length::{NonNegativeLengthOrAuto, NonNegativeLengthOrNormal};
 use values::computed::length::NonNegativeLengthOrPercentage;
 use values::distance::{ComputeSquaredDistance, SquaredDistance};
 use values::generics::{GreaterThanOrEqualToOne, NonNegative};
 use values::generics::effects::Filter;
 use values::generics::position as generic_position;
-use values::generics::svg::{SVGLength, SVGOpacity, SVGPaint, SVGPaintKind, SVGStrokeDashArray};
+use values::generics::svg::{SVGLength,  SvgLengthOrPercentageOrNumber, SVGPaint};
+use values::generics::svg::{SVGPaintKind, SVGStrokeDashArray, SVGOpacity};
 
 /// A trait used to implement various procedures used during animation.
 pub trait Animatable: Sized {
     /// Performs a weighted sum of this value and |other|. This is used for
     /// interpolation and addition of animation values.
     fn add_weighted(&self, other: &Self, self_portion: f64, other_portion: f64)
         -> Result<Self, ()>;
 
@@ -776,16 +778,17 @@ impl ToAnimatedZero for AnimationValue {
             _ => Err(()),
         }
     }
 }
 
 impl RepeatableListAnimatable for LengthOrPercentage {}
 impl RepeatableListAnimatable for Either<f32, LengthOrPercentage> {}
 impl RepeatableListAnimatable for Either<NonNegativeNumber, NonNegativeLengthOrPercentage> {}
+impl RepeatableListAnimatable for SvgLengthOrPercentageOrNumber<NonNegativeLengthOrPercentage, NonNegativeNumber> {}
 
 macro_rules! repeated_vec_impl {
     ($($ty:ty),*) => {
         $(impl<T: RepeatableListAnimatable> Animatable for $ty {
             fn add_weighted(&self, other: &Self, self_portion: f64, other_portion: f64)
                 -> Result<Self, ()> {
                 // If the length of either list is zero, the least common multiple is undefined.
                 if self.is_empty() || other.is_empty() {
@@ -1011,17 +1014,22 @@ impl Animatable for LengthOrPercentage {
             }
         }
     }
 }
 
 impl ToAnimatedZero for LengthOrPercentage {
     #[inline]
     fn to_animated_zero(&self) -> Result<Self, ()> {
-        Ok(LengthOrPercentage::zero())
+        match self {
+            &LengthOrPercentage::Length(_) | &LengthOrPercentage::Calc(_) =>
+                Ok(LengthOrPercentage::zero()),
+            &LengthOrPercentage::Percentage(_) =>
+                Ok(LengthOrPercentage::Percentage(Percentage::zero())),
+        }
     }
 }
 
 /// https://drafts.csswg.org/css-transitions/#animtype-lpcalc
 impl Animatable for LengthOrPercentageOrAuto {
     #[inline]
     fn add_weighted(&self, other: &Self, self_portion: f64, other_portion: f64) -> Result<Self, ()> {
         match (*self, *other) {
@@ -2491,16 +2499,130 @@ impl ToAnimatedZero for IntermediateSVGP
             SVGPaintKind::None |
             SVGPaintKind::ContextFill |
             SVGPaintKind::ContextStroke => Ok(self.clone()),
             _ => Err(()),
         }
     }
 }
 
+impl From<NonNegativeLengthOrPercentage> for NumberOrPercentage {
+    fn from(lop: NonNegativeLengthOrPercentage) -> NumberOrPercentage {
+        lop.0.into()
+    }
+}
+
+impl From<NonNegativeNumber> for NumberOrPercentage {
+    fn from(num: NonNegativeNumber) -> NumberOrPercentage {
+        num.0.into()
+    }
+}
+
+impl From<LengthOrPercentage> for NumberOrPercentage {
+    fn from(lop: LengthOrPercentage) -> NumberOrPercentage {
+        match lop {
+            LengthOrPercentage::Length(len) => NumberOrPercentage::Number(len.to_f32_px()),
+            LengthOrPercentage::Percentage(p) => NumberOrPercentage::Percentage(p),
+            LengthOrPercentage::Calc(_) => {
+                panic!("We dont't expected calc interpolation for SvgLengthOrPercentageOrNumber");
+            },
+        }
+    }
+}
+
+impl From<Number> for NumberOrPercentage {
+    fn from(num: Number) -> NumberOrPercentage {
+        NumberOrPercentage::Number(num)
+    }
+}
+
+fn convert_to_number_or_percentage<LengthOrPercentageType, NumberType>(
+    from: SvgLengthOrPercentageOrNumber<LengthOrPercentageType, NumberType>)
+    -> NumberOrPercentage
+    where LengthOrPercentageType: Into<NumberOrPercentage>,
+          NumberType: Into<NumberOrPercentage>
+{
+    match from {
+        SvgLengthOrPercentageOrNumber::LengthOrPercentage(lop) => {
+            lop.into()
+        }
+        SvgLengthOrPercentageOrNumber::Number(num) => {
+            num.into()
+        }
+    }
+}
+
+fn convert_from_number_or_percentage<LengthOrPercentageType, NumberType>(
+    from: NumberOrPercentage)
+    -> SvgLengthOrPercentageOrNumber<LengthOrPercentageType, NumberType>
+    where LengthOrPercentageType: From<LengthOrPercentage>,
+          NumberType: From<Number>
+{
+    match from {
+        NumberOrPercentage::Number(num) =>
+            SvgLengthOrPercentageOrNumber::Number(num.into()),
+        NumberOrPercentage::Percentage(p) =>
+            SvgLengthOrPercentageOrNumber::LengthOrPercentage(
+                (LengthOrPercentage::Percentage(p)).into())
+    }
+}
+
+impl <LengthOrPercentageType, NumberType> Animatable for
+    SvgLengthOrPercentageOrNumber<LengthOrPercentageType, NumberType>
+    where LengthOrPercentageType: Animatable + Into<NumberOrPercentage> + From<LengthOrPercentage> + Copy,
+          NumberType: Animatable + Into<NumberOrPercentage> + From<Number>,
+          SvgLengthOrPercentageOrNumber<LengthOrPercentageType, NumberType>: Copy,
+          LengthOrPercentage: From<LengthOrPercentageType>
+{
+    #[inline]
+    fn add_weighted(&self, other: &Self, self_portion: f64, other_portion: f64) -> Result<Self, ()> {
+        if self.has_calc() || other.has_calc() {
+            // TODO: We need to treat calc value.
+            // https://bugzilla.mozilla.org/show_bug.cgi?id=1386967
+            return Err(());
+        }
+
+        let from_value = convert_to_number_or_percentage(*self);
+        let to_value = convert_to_number_or_percentage(*other);
+
+        match (from_value, to_value) {
+            (NumberOrPercentage::Number(from),
+             NumberOrPercentage::Number(to)) => {
+                from.add_weighted(&to, self_portion, other_portion)
+                    .map(|num| NumberOrPercentage::Number(num))
+                    .map(|nop| convert_from_number_or_percentage(nop))
+            },
+            (NumberOrPercentage::Percentage(from),
+             NumberOrPercentage::Percentage(to)) => {
+                from.add_weighted(&to, self_portion, other_portion)
+                    .map(|p| NumberOrPercentage::Percentage(p))
+                    .map(|nop| convert_from_number_or_percentage(nop))
+            },
+            _ => Err(()),
+        }
+    }
+}
+
+impl <LengthOrPercentageType, NumberType> ToAnimatedZero for
+    SvgLengthOrPercentageOrNumber<LengthOrPercentageType, NumberType>
+    where LengthOrPercentageType: ToAnimatedZero, NumberType: ToAnimatedZero
+{
+    #[inline]
+    fn to_animated_zero(&self) -> Result<Self, ()> {
+        match self {
+            &SvgLengthOrPercentageOrNumber::LengthOrPercentage(ref lop) =>
+                lop.to_animated_zero()
+                    .map(SvgLengthOrPercentageOrNumber::LengthOrPercentage),
+            &SvgLengthOrPercentageOrNumber::Number(ref num) =>
+                num.to_animated_zero()
+                    .map(SvgLengthOrPercentageOrNumber::Number),
+        }
+    }
+}
+
 impl<LengthType> Animatable for SVGLength<LengthType>
         where LengthType: Animatable + Clone
 {
     #[inline]
     fn add_weighted(&self, other: &Self, self_portion: f64, other_portion: f64) -> Result<Self, ()> {
         match (self, other) {
             (&SVGLength::Length(ref this), &SVGLength::Length(ref other)) => {
                 this.add_weighted(&other, self_portion, other_portion).map(SVGLength::Length)
@@ -2517,31 +2639,44 @@ impl<LengthType> ToAnimatedZero for SVGL
     fn to_animated_zero(&self) -> Result<Self, ()> {
         match self {
             &SVGLength::Length(ref length) => length.to_animated_zero().map(SVGLength::Length),
             &SVGLength::ContextValue => Ok(SVGLength::ContextValue),
         }
     }
 }
 
+/// https://www.w3.org/TR/SVG11/painting.html#StrokeDasharrayProperty
 impl<LengthType> Animatable for SVGStrokeDashArray<LengthType>
     where LengthType : RepeatableListAnimatable + Clone
 {
     #[inline]
     fn add_weighted(&self, other: &Self, self_portion: f64, other_portion: f64) -> Result<Self, ()> {
         match (self, other) {
             (&SVGStrokeDashArray::Values(ref this), &SVGStrokeDashArray::Values(ref other))=> {
                 this.add_weighted(other, self_portion, other_portion)
                     .map(SVGStrokeDashArray::Values)
             }
             _ => {
                 Ok(if self_portion > other_portion { self.clone() } else { other.clone() })
             }
         }
     }
+
+    /// stroke-dasharray is non-additive
+    #[inline]
+    fn add(&self, _other: &Self) -> Result<Self, ()> {
+        Err(())
+    }
+
+    /// stroke-dasharray is non-additive
+    #[inline]
+    fn accumulate(&self, _other: &Self, _count: u64) -> Result<Self, ()> {
+        Err(())
+    }
 }
 
 impl<LengthType> ToAnimatedZero for SVGStrokeDashArray<LengthType>
     where LengthType : ToAnimatedZero + Clone
 {
     #[inline]
     fn to_animated_zero(&self) -> Result<Self, ()> {
         match self {
diff --git a/servo/components/style/values/computed/length.rs b/servo/components/style/values/computed/length.rs
--- a/servo/components/style/values/computed/length.rs
+++ b/servo/components/style/values/computed/length.rs
@@ -601,16 +601,23 @@ impl From<NonNegativeAu> for NonNegative
 
 impl From<LengthOrPercentage> for NonNegativeLengthOrPercentage {
     #[inline]
     fn from(lop: LengthOrPercentage) -> Self {
         NonNegative::<LengthOrPercentage>(lop)
     }
 }
 
+impl From<NonNegativeLengthOrPercentage> for LengthOrPercentage {
+    #[inline]
+    fn from(lop: NonNegativeLengthOrPercentage) -> LengthOrPercentage {
+        lop.0
+    }
+}
+
 impl NonNegativeLengthOrPercentage {
     /// Get zero value.
     #[inline]
     pub fn zero() -> Self {
         NonNegative::<LengthOrPercentage>(LengthOrPercentage::zero())
     }
 
     /// Returns true if the computed value is absolute 0 or 0%.
diff --git a/servo/components/style/values/computed/svg.rs b/servo/components/style/values/computed/svg.rs
--- a/servo/components/style/values/computed/svg.rs
+++ b/servo/components/style/values/computed/svg.rs
@@ -1,19 +1,19 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Computed types for SVG properties.
 
 use app_units::Au;
-use values::{Either, RGBA};
-use values::computed::{LengthOrPercentageOrNumber, Opacity};
-use values::computed::{NonNegativeAu, NonNegativeLengthOrPercentageOrNumber};
-use values::computed::ComputedUrl;
+use values::RGBA;
+use values::computed::{ComputedUrl, LengthOrPercentage, NonNegativeAu};
+use values::computed::{NonNegativeNumber, NonNegativeLengthOrPercentage, Number};
+use values::computed::Opacity;
 use values::generics::svg as generic;
 
 /// Computed SVG Paint value
 pub type SVGPaint = generic::SVGPaint<RGBA, ComputedUrl>;
 /// Computed SVG Paint Kind value
 pub type SVGPaintKind = generic::SVGPaintKind<RGBA, ComputedUrl>;
 
 impl Default for SVGPaint {
@@ -31,36 +31,61 @@ impl SVGPaint {
         let rgba = RGBA::from_floats(0., 0., 0., 1.);
         SVGPaint {
             kind: generic::SVGPaintKind::Color(rgba),
             fallback: None,
         }
     }
 }
 
+/// A value of <length> | <percentage> | <number> for stroke-dashoffset.
+/// https://www.w3.org/TR/SVG11/painting.html#StrokeProperties
+pub type SvgLengthOrPercentageOrNumber =
+    generic::SvgLengthOrPercentageOrNumber<LengthOrPercentage, Number>;
+
 /// <length> | <percentage> | <number> | context-value
-pub type SVGLength = generic::SVGLength<LengthOrPercentageOrNumber>;
+pub type SVGLength = generic::SVGLength<SvgLengthOrPercentageOrNumber>;
 
 impl From<Au> for SVGLength {
     fn from(length: Au) -> Self {
-        generic::SVGLength::Length(Either::Second(length.into()))
+        generic::SVGLength::Length(
+            generic::SvgLengthOrPercentageOrNumber::LengthOrPercentage(length.into()))
+    }
+}
+
+/// A value of <length> | <percentage> | <number> for stroke-width/stroke-dasharray.
+/// https://www.w3.org/TR/SVG11/painting.html#StrokeProperties
+pub type NonNegativeSvgLengthOrPercentageOrNumber =
+    generic::SvgLengthOrPercentageOrNumber<NonNegativeLengthOrPercentage, NonNegativeNumber>;
+
+impl Into<NonNegativeSvgLengthOrPercentageOrNumber> for SvgLengthOrPercentageOrNumber {
+    fn into(self) -> NonNegativeSvgLengthOrPercentageOrNumber {
+        match self {
+            generic::SvgLengthOrPercentageOrNumber::LengthOrPercentage(lop) =>{
+                generic::SvgLengthOrPercentageOrNumber::LengthOrPercentage(lop.into())
+            },
+            generic::SvgLengthOrPercentageOrNumber::Number(num) => {
+                generic::SvgLengthOrPercentageOrNumber::Number(num.into())
+            },
+        }
     }
 }
 
 /// An non-negative wrapper of SVGLength.
-pub type SVGWidth = generic::SVGLength<NonNegativeLengthOrPercentageOrNumber>;
+pub type SVGWidth = generic::SVGLength<NonNegativeSvgLengthOrPercentageOrNumber>;
 
 impl From<NonNegativeAu> for SVGWidth {
     fn from(length: NonNegativeAu) -> Self {
-        generic::SVGLength::Length(Either::Second(length.into()))
+        generic::SVGLength::Length(
+            generic::SvgLengthOrPercentageOrNumber::LengthOrPercentage(length.into()))
     }
 }
 
 /// [ <length> | <percentage> | <number> ]# | context-value
-pub type SVGStrokeDashArray = generic::SVGStrokeDashArray<NonNegativeLengthOrPercentageOrNumber>;
+pub type SVGStrokeDashArray = generic::SVGStrokeDashArray<NonNegativeSvgLengthOrPercentageOrNumber>;
 
 impl Default for SVGStrokeDashArray {
     fn default() -> Self {
         generic::SVGStrokeDashArray::Values(vec![])
     }
 }
 
 /// <opacity-value> | context-fill-opacity | context-stroke-opacity
diff --git a/servo/components/style/values/generics/svg.rs b/servo/components/style/values/generics/svg.rs
--- a/servo/components/style/values/generics/svg.rs
+++ b/servo/components/style/values/generics/svg.rs
@@ -3,16 +3,18 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Generic types for CSS values in SVG
 
 use cssparser::Parser;
 use parser::{Parse, ParserContext};
 use std::fmt;
 use style_traits::{ParseError, StyleParseError, ToCss};
+use values::computed::length::LengthOrPercentage;
+
 
 /// An SVG paint value
 ///
 /// https://www.w3.org/TR/SVG2/painting.html#SpecifyingPaint
 #[cfg_attr(feature = "servo", derive(HeapSizeOf))]
 #[derive(Clone, Debug, PartialEq, ToAnimatedValue, ToComputedValue, ToCss)]
 pub struct SVGPaint<ColorType, UrlPaintServer> {
     /// The paint source
@@ -90,16 +92,63 @@ impl<ColorType: Parse, UrlPaintServer: P
                 fallback: None,
             })
         } else {
             Err(StyleParseError::UnspecifiedError.into())
         }
     }
 }
 
+/// A value of <length> | <percentage> | <number> for svg which allow unitless length.
+/// https://www.w3.org/TR/SVG11/painting.html#StrokeProperties
+#[cfg_attr(feature = "servo", derive(HeapSizeOf))]
+#[derive(Clone, Copy, Debug, PartialEq, ToCss, HasViewportPercentage)]
+#[derive(ToComputedValue, ToAnimatedValue, ComputeSquaredDistance)]
+pub enum SvgLengthOrPercentageOrNumber<LengthOrPercentageType, NumberType> {
+    /// <length> | <percentage>
+    LengthOrPercentage(LengthOrPercentageType),
+    /// <number>
+    Number(NumberType),
+}
+
+impl<LengthOrPercentageType, NumberType> SvgLengthOrPercentageOrNumber<LengthOrPercentageType, NumberType>
+    where LengthOrPercentage: From<LengthOrPercentageType>,
+          LengthOrPercentageType: Copy
+{
+    /// return true if this struct has calc value.
+    pub fn has_calc(&self) -> bool {
+        match self {
+            &SvgLengthOrPercentageOrNumber::LengthOrPercentage(lop) => {
+                match LengthOrPercentage::from(lop) {
+                    LengthOrPercentage::Calc(_) => true,
+                    _ => false,
+                }
+            },
+            _ => false,
+        }
+    }
+}
+
+/// Parsing the SvgLengthOrPercentageOrNumber. At first, we need to parse number
+/// since prevent converting to the length.
+impl <LengthOrPercentageType: Parse, NumberType: Parse> Parse for
+    SvgLengthOrPercentageOrNumber<LengthOrPercentageType, NumberType> {
+    fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
+                     -> Result<Self, ParseError<'i>> {
+        if let Ok(num) = input.try(|i| NumberType::parse(context, i)) {
+            return Ok(SvgLengthOrPercentageOrNumber::Number(num));
+        }
+
+        if let Ok(lop) = input.try(|i| LengthOrPercentageType::parse(context, i)) {
+            return Ok(SvgLengthOrPercentageOrNumber::LengthOrPercentage(lop));
+        }
+        Err(StyleParseError::UnspecifiedError.into())
+    }
+}
+
 /// An SVG length value supports `context-value` in addition to length.
 #[cfg_attr(feature = "servo", derive(HeapSizeOf))]
 #[derive(Clone, ComputeSquaredDistance, Copy, Debug, PartialEq)]
 #[derive(HasViewportPercentage, ToAnimatedValue, ToComputedValue, ToCss)]
 pub enum SVGLength<LengthType> {
     /// `<length> | <percentage> | <number>`
     Length(LengthType),
     /// `context-value`
diff --git a/servo/components/style/values/specified/svg.rs b/servo/components/style/values/specified/svg.rs
--- a/servo/components/style/values/specified/svg.rs
+++ b/servo/components/style/values/specified/svg.rs
@@ -3,17 +3,18 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Specified types for SVG properties.
 
 use cssparser::Parser;
 use parser::{Parse, ParserContext};
 use style_traits::{CommaWithSpace, ParseError, Separator, StyleParseError};
 use values::generics::svg as generic;
-use values::specified::{LengthOrPercentageOrNumber, NonNegativeLengthOrPercentageOrNumber, Opacity, SpecifiedUrl};
+use values::specified::{LengthOrPercentage, NonNegativeLengthOrPercentage, NonNegativeNumber};
+use values::specified::{Number, Opacity, SpecifiedUrl};
 use values::specified::color::RGBAColor;
 
 /// Specified SVG Paint value
 pub type SVGPaint = generic::SVGPaint<RGBAColor, SpecifiedUrl>;
 
 no_viewport_percentage!(SVGPaint);
 
 /// Specified SVG Paint Kind value
@@ -37,60 +38,70 @@ fn parse_context_value<'i, 't, T>(input:
     if is_context_value_enabled() {
         if input.expect_ident_matching("context-value").is_ok() {
             return Ok(value);
         }
     }
     Err(StyleParseError::UnspecifiedError.into())
 }
 
+/// A value of <length> | <percentage> | <number> for stroke-dashoffset.
+/// https://www.w3.org/TR/SVG11/painting.html#StrokeProperties
+pub type SvgLengthOrPercentageOrNumber =
+    generic::SvgLengthOrPercentageOrNumber<LengthOrPercentage, Number>;
+
 /// <length> | <percentage> | <number> | context-value
-pub type SVGLength = generic::SVGLength<LengthOrPercentageOrNumber>;
+pub type SVGLength = generic::SVGLength<SvgLengthOrPercentageOrNumber>;
 
 impl Parse for SVGLength {
     fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
                      -> Result<Self, ParseError<'i>> {
-        input.try(|i| LengthOrPercentageOrNumber::parse(context, i))
+        input.try(|i| SvgLengthOrPercentageOrNumber::parse(context, i))
              .map(Into::into)
              .or_else(|_| parse_context_value(input, generic::SVGLength::ContextValue))
     }
 }
 
-impl From<LengthOrPercentageOrNumber> for SVGLength {
-    fn from(length: LengthOrPercentageOrNumber) -> Self {
+impl From<SvgLengthOrPercentageOrNumber> for SVGLength {
+    fn from(length: SvgLengthOrPercentageOrNumber) -> Self {
         generic::SVGLength::Length(length)
     }
 }
 
+/// A value of <length> | <percentage> | <number> for stroke-width/stroke-dasharray.
+/// https://www.w3.org/TR/SVG11/painting.html#StrokeProperties
+pub type NonNegativeSvgLengthOrPercentageOrNumber =
+    generic::SvgLengthOrPercentageOrNumber<NonNegativeLengthOrPercentage, NonNegativeNumber>;
+
 /// A non-negative version of SVGLength.
-pub type SVGWidth = generic::SVGLength<NonNegativeLengthOrPercentageOrNumber>;
+pub type SVGWidth = generic::SVGLength<NonNegativeSvgLengthOrPercentageOrNumber>;
 
 impl Parse for SVGWidth {
     fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
                      -> Result<Self, ParseError<'i>> {
-        input.try(|i| NonNegativeLengthOrPercentageOrNumber::parse(context, i))
+        input.try(|i| NonNegativeSvgLengthOrPercentageOrNumber::parse(context, i))
              .map(Into::into)
              .or_else(|_| parse_context_value(input, generic::SVGLength::ContextValue))
     }
 }
 
-impl From<NonNegativeLengthOrPercentageOrNumber> for SVGWidth {
-    fn from(length: NonNegativeLengthOrPercentageOrNumber) -> Self {
+impl From<NonNegativeSvgLengthOrPercentageOrNumber> for SVGWidth {
+    fn from(length: NonNegativeSvgLengthOrPercentageOrNumber) -> Self {
         generic::SVGLength::Length(length)
     }
 }
 
 /// [ <length> | <percentage> | <number> ]# | context-value
-pub type SVGStrokeDashArray = generic::SVGStrokeDashArray<NonNegativeLengthOrPercentageOrNumber>;
+pub type SVGStrokeDashArray = generic::SVGStrokeDashArray<NonNegativeSvgLengthOrPercentageOrNumber>;
 
 impl Parse for SVGStrokeDashArray {
     fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
                      -> Result<Self, ParseError<'i>> {
         if let Ok(values) = input.try(|i| CommaWithSpace::parse(i, |i| {
-            NonNegativeLengthOrPercentageOrNumber::parse(context, i)
+            NonNegativeSvgLengthOrPercentageOrNumber::parse(context, i)
         })) {
             Ok(generic::SVGStrokeDashArray::Values(values))
         } else if let Ok(_) = input.try(|i| i.expect_ident_matching("none")) {
             Ok(generic::SVGStrokeDashArray::Values(vec![]))
         } else {
             parse_context_value(input, generic::SVGStrokeDashArray::ContextValue)
         }
     }
