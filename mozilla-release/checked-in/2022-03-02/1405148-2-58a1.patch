# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1507756363 -3600
# Node ID 5ec596adde758cfff86428583e0e3095504ffc32
# Parent  dfddd7fa3c48be596e59b5d15df03a1d349dcad0
Bug 1405148 - part 2 - Map the 'smcp' OpenType feature to its AAT feature-selector equivalent when shaping via Core Text. r=jrmuizel

diff --git a/gfx/thebes/gfxCoreTextShaper.cpp b/gfx/thebes/gfxCoreTextShaper.cpp
--- a/gfx/thebes/gfxCoreTextShaper.cpp
+++ b/gfx/thebes/gfxCoreTextShaper.cpp
@@ -14,17 +14,19 @@
 #include <algorithm>
 
 #include <dlfcn.h>
 
 using namespace mozilla;
 
 // standard font descriptors that we construct the first time they're needed
 CTFontDescriptorRef gfxCoreTextShaper::sDefaultFeaturesDescriptor = nullptr;
+CTFontDescriptorRef gfxCoreTextShaper::sSmallCapsDescriptor = nullptr;
 CTFontDescriptorRef gfxCoreTextShaper::sDisableLigaturesDescriptor = nullptr;
+CTFontDescriptorRef gfxCoreTextShaper::sSmallCapDisableLigDescriptor = nullptr;
 CTFontDescriptorRef gfxCoreTextShaper::sIndicFeaturesDescriptor = nullptr;
 CTFontDescriptorRef gfxCoreTextShaper::sIndicDisableLigaturesDescriptor = nullptr;
 
 // Helper to create a CFDictionary with the right attributes for shaping our
 // text, including imposing the given directionality.
 CFDictionaryRef
 gfxCoreTextShaper::CreateAttrDict(bool aRightToLeft)
 {
@@ -99,16 +101,36 @@ gfxCoreTextShaper::ShapeText(DrawTarget 
     bool isRightToLeft = aShapedText->IsRightToLeft();
     const UniChar* text = reinterpret_cast<const UniChar*>(aText);
 
     CFStringRef stringObj =
         ::CFStringCreateWithCharactersNoCopy(kCFAllocatorDefault,
                                              text, aLength,
                                              kCFAllocatorNull);
 
+    // Figure out whether we should try to set the AAT small-caps feature:
+    // examine OpenType tags for the requested style, and see if 'smcp' is
+    // among them.
+    const gfxFontStyle *style = mFont->GetStyle();
+    gfxFontEntry *entry = mFont->GetFontEntry();
+    auto handleFeatureTag = [](const uint32_t& aTag, uint32_t& aValue,
+                               void *aUserArg) -> void {
+        if (aTag == HB_TAG('s','m','c','p') && aValue) {
+            *static_cast<bool*>(aUserArg) = true;
+        }
+    };
+    bool addSmallCaps = false;
+    MergeFontFeatures(style,
+                      entry->mFeatureSettings,
+                      false,
+                      entry->FamilyName(),
+                      false,
+                      handleFeatureTag,
+                      &addSmallCaps);
+
     // Get an attributes dictionary suitable for shaping text in the
     // current direction, creating it if necessary.
     CFDictionaryRef attrObj =
         isRightToLeft ? mAttributesDictRTL : mAttributesDictLTR;
     if (!attrObj) {
         attrObj = CreateAttrDict(isRightToLeft);
         (isRightToLeft ? mAttributesDictRTL : mAttributesDictLTR) = attrObj;
     }
@@ -126,29 +148,35 @@ gfxCoreTextShaper::ShapeText(DrawTarget 
                                      aShapedText->DisableLigatures()
                                          ? GetIndicDisableLigaturesDescriptor()
                                          : GetIndicFeaturesDescriptor());
     } else if (aShapedText->DisableLigatures()) {
         // For letterspacing (or maybe other situations) we need to make
         // a copy of the CTFont with the ligature feature disabled.
         tempCTFont =
             CreateCTFontWithFeatures(::CTFontGetSize(mCTFont),
-                                     GetDisableLigaturesDescriptor());
+                                     addSmallCaps
+                                         ? GetSmallCapDisableLigDescriptor()
+                                         : GetDisableLigaturesDescriptor());
+    } else if (addSmallCaps) {
+        tempCTFont =
+            CreateCTFontWithFeatures(::CTFontGetSize(mCTFont),
+                                     GetSmallCapsDescriptor());
     }
 
-    // For the disabled-ligature or buggy-indic-font case, we need to replace
+    // For the disabled-ligature, buggy-indic-font or small-caps case, replace
     // the standard CTFont in the attribute dictionary with a tweaked version.
     CFMutableDictionaryRef mutableAttr = nullptr;
     if (tempCTFont) {
         mutableAttr = ::CFDictionaryCreateMutableCopy(kCFAllocatorDefault, 2,
                                                       attrObj);
         ::CFDictionaryReplaceValue(mutableAttr,
                                    kCTFontAttributeName, tempCTFont);
         // Having created the dict, we're finished with our temporary
-        // Indic and/or ligature-disabled CTFontRef.
+        // Indic and/or ligature-disabled or small-caps CTFontRef.
         ::CFRelease(tempCTFont);
         attrObj = mutableAttr;
     }
 
     // Now we can create an attributed string
     CFAttributedStringRef attrStringObj =
         ::CFAttributedStringCreate(kCFAllocatorDefault, stringObj, attrObj);
     ::CFRelease(stringObj);
@@ -540,17 +568,17 @@ gfxCoreTextShaper::SetGlyphsFromRun(gfxS
 
 // Construct the font attribute descriptor that we'll apply by default when
 // creating a CTFontRef. This will turn off line-edge swashes by default,
 // because we don't know the actual line breaks when doing glyph shaping.
 
 // We also cache feature descriptors for shaping with disabled ligatures, and
 // for buggy Indic AAT font workarounds, created on an as-needed basis.
 
-#define MAX_FEATURES  3 // max used by any of our Get*Descriptor functions
+#define MAX_FEATURES  5 // max used by any of our Get*Descriptor functions
 
 CTFontDescriptorRef
 gfxCoreTextShaper::CreateFontFeaturesDescriptor(
     const std::pair<SInt16,SInt16> aFeatures[],
     size_t aCount)
 {
     MOZ_ASSERT(aCount <= MAX_FEATURES);
 
@@ -611,61 +639,108 @@ gfxCoreTextShaper::CreateFontFeaturesDes
 CTFontDescriptorRef
 gfxCoreTextShaper::GetDefaultFeaturesDescriptor()
 {
     if (sDefaultFeaturesDescriptor == nullptr) {
         const std::pair<SInt16,SInt16> kDefaultFeatures[] = {
             { kSmartSwashType, kLineInitialSwashesOffSelector },
             { kSmartSwashType, kLineFinalSwashesOffSelector }
         };
+        static_assert(ArrayLength(kDefaultFeatures) <= MAX_FEATURES,
+                      "need to increase MAX_FEATURES");
         sDefaultFeaturesDescriptor =
             CreateFontFeaturesDescriptor(kDefaultFeatures,
                                          ArrayLength(kDefaultFeatures));
     }
     return sDefaultFeaturesDescriptor;
 }
 
 CTFontDescriptorRef
+gfxCoreTextShaper::GetSmallCapsDescriptor()
+{
+    if (sSmallCapsDescriptor == nullptr) {
+        const std::pair<SInt16,SInt16> kSmallCaps[] = {
+            { kSmartSwashType, kLineInitialSwashesOffSelector },
+            { kSmartSwashType, kLineFinalSwashesOffSelector },
+            { kLetterCaseType, kSmallCapsSelector },
+            { kLowerCaseType, kLowerCaseSmallCapsSelector }
+        };
+        static_assert(ArrayLength(kSmallCaps) <= MAX_FEATURES,
+                      "need to increase MAX_FEATURES");
+        sSmallCapsDescriptor =
+            CreateFontFeaturesDescriptor(kSmallCaps,
+                                         ArrayLength(kSmallCaps));
+    }
+    return sSmallCapsDescriptor;
+}
+
+CTFontDescriptorRef
 gfxCoreTextShaper::GetDisableLigaturesDescriptor()
 {
     if (sDisableLigaturesDescriptor == nullptr) {
         const std::pair<SInt16,SInt16> kDisableLigatures[] = {
             { kSmartSwashType, kLineInitialSwashesOffSelector },
             { kSmartSwashType, kLineFinalSwashesOffSelector },
             { kLigaturesType, kCommonLigaturesOffSelector }
         };
+        static_assert(ArrayLength(kDisableLigatures) <= MAX_FEATURES,
+                      "need to increase MAX_FEATURES");
         sDisableLigaturesDescriptor =
             CreateFontFeaturesDescriptor(kDisableLigatures,
                                          ArrayLength(kDisableLigatures));
     }
     return sDisableLigaturesDescriptor;
 }
 
 CTFontDescriptorRef
+gfxCoreTextShaper::GetSmallCapDisableLigDescriptor()
+{
+    if (sSmallCapDisableLigDescriptor == nullptr) {
+        const std::pair<SInt16,SInt16> kFeatures[] = {
+            { kSmartSwashType, kLineInitialSwashesOffSelector },
+            { kSmartSwashType, kLineFinalSwashesOffSelector },
+            { kLigaturesType, kCommonLigaturesOffSelector },
+            { kLetterCaseType, kSmallCapsSelector },
+            { kLowerCaseType, kLowerCaseSmallCapsSelector }
+        };
+        static_assert(ArrayLength(kFeatures) <= MAX_FEATURES,
+                      "need to increase MAX_FEATURES");
+        sSmallCapDisableLigDescriptor =
+            CreateFontFeaturesDescriptor(kFeatures,
+                                         ArrayLength(kFeatures));
+    }
+    return sSmallCapDisableLigDescriptor;
+}
+
+CTFontDescriptorRef
 gfxCoreTextShaper::GetIndicFeaturesDescriptor()
 {
     if (sIndicFeaturesDescriptor == nullptr) {
         const std::pair<SInt16,SInt16> kIndicFeatures[] = {
             { kSmartSwashType, kLineFinalSwashesOffSelector }
         };
+        static_assert(ArrayLength(kIndicFeatures) <= MAX_FEATURES,
+                      "need to increase MAX_FEATURES");
         sIndicFeaturesDescriptor =
             CreateFontFeaturesDescriptor(kIndicFeatures,
                                          ArrayLength(kIndicFeatures));
     }
     return sIndicFeaturesDescriptor;
 }
 
 CTFontDescriptorRef
 gfxCoreTextShaper::GetIndicDisableLigaturesDescriptor()
 {
     if (sIndicDisableLigaturesDescriptor == nullptr) {
         const std::pair<SInt16,SInt16> kIndicDisableLigatures[] = {
             { kSmartSwashType, kLineFinalSwashesOffSelector },
             { kLigaturesType, kCommonLigaturesOffSelector }
         };
+        static_assert(ArrayLength(kIndicDisableLigatures) <= MAX_FEATURES,
+                      "need to increase MAX_FEATURES");
         sIndicDisableLigaturesDescriptor =
             CreateFontFeaturesDescriptor(kIndicDisableLigatures,
                                          ArrayLength(kIndicDisableLigatures));
     }
     return sIndicDisableLigaturesDescriptor;
 }
 
 CTFontRef
diff --git a/gfx/thebes/gfxCoreTextShaper.h b/gfx/thebes/gfxCoreTextShaper.h
--- a/gfx/thebes/gfxCoreTextShaper.h
+++ b/gfx/thebes/gfxCoreTextShaper.h
@@ -48,24 +48,28 @@ protected:
     CFDictionaryRef CreateAttrDict(bool aRightToLeft);
     CFDictionaryRef CreateAttrDictWithoutDirection();
 
     static CTFontDescriptorRef
     CreateFontFeaturesDescriptor(const std::pair<SInt16,SInt16> aFeatures[],
                                  size_t aCount);
 
     static CTFontDescriptorRef GetDefaultFeaturesDescriptor();
+    static CTFontDescriptorRef GetSmallCapsDescriptor();
     static CTFontDescriptorRef GetDisableLigaturesDescriptor();
+    static CTFontDescriptorRef GetSmallCapDisableLigDescriptor();
     static CTFontDescriptorRef GetIndicFeaturesDescriptor();
     static CTFontDescriptorRef GetIndicDisableLigaturesDescriptor();
 
     // cached font descriptor, created the first time it's needed
     static CTFontDescriptorRef    sDefaultFeaturesDescriptor;
+    static CTFontDescriptorRef    sSmallCapsDescriptor;
 
     // cached descriptor for adding disable-ligatures setting to a font
     static CTFontDescriptorRef    sDisableLigaturesDescriptor;
+    static CTFontDescriptorRef    sSmallCapDisableLigDescriptor;
 
     // feature descriptors for buggy Indic AAT font workaround
     static CTFontDescriptorRef    sIndicFeaturesDescriptor;
     static CTFontDescriptorRef    sIndicDisableLigaturesDescriptor;
 };
 
 #endif /* GFX_CORETEXTSHAPER_H */
diff --git a/gfx/thebes/gfxMacPlatformFontList.h b/gfx/thebes/gfxMacPlatformFontList.h
--- a/gfx/thebes/gfxMacPlatformFontList.h
+++ b/gfx/thebes/gfxMacPlatformFontList.h
@@ -67,18 +67,19 @@ public:
     // Return true if the font has a 'trak' table (and we can successfully
     // interpret it), otherwise false. This will load and cache the table
     // the first time it is called.
     bool HasTrackingTable();
 
     bool SupportsOpenTypeFeature(Script aScript, uint32_t aFeatureTag) override
     {
         // If we're going to shape with Core Text, we don't support added
-        // OpenType features (aside from any CT applies by default).
-        if (RequiresAATLayout()) {
+        // OpenType features (aside from any CT applies by default), except
+        // for 'smcp' which we map to an AAT feature selector.
+        if (RequiresAATLayout() && aFeatureTag != HB_TAG('s','m','c','p')) {
             return false;
         }
         return gfxFontEntry::SupportsOpenTypeFeature(aScript, aFeatureTag);
     }
 
     // Return the tracking (in font units) to be applied for the given size.
     // (This is a floating-point number because of possible interpolation.)
     float TrackingForCSSPx(float aPointSize) const;
