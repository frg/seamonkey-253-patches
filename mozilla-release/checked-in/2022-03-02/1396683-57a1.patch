# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1504727010 14400
# Node ID 58e5b2cc2d8d43a1aa199b9b7d5d908bc700b5e7
# Parent  4d492424cba2d599feba617463c83ef616a17f09
Bug 1396683 - clarify bindgen error messages around clang; r=rillian

Stylo's recent enabled-by-default behavior, combined with some Linux
distributions's packaging of LLVM separately from Clang, causes
confusion.  To allay such confusion, rearrange the configure checks to
do two things:

1) Check for the clang binary prior to checking for clang's shared
   libraries; it should be more obvious what you need to install from
   an error about clang, and installing clang should pull in the
   necessary clang libraries, avoiding the following scenario:

     - run configure
     - get error message about libclang
     - install libclang
     - run configure
     - get error message about clang

2) Provide some context for what to do to avoid this error; the user may
not understand why we need a separate C/C++ compiler when they already
have a perfectly suitable one on their system.

diff --git a/toolkit/moz.configure b/toolkit/moz.configure
--- a/toolkit/moz.configure
+++ b/toolkit/moz.configure
@@ -734,37 +734,40 @@ with only_when(building_stylo_bindgen):
                                       'clang')
             libclang_path = normsep(libclang_path)
             clang_path = normsep(clang_path)
 
             # Debian-based distros, at least, can have llvm-config installed
             # but not have other packages installed.  Since the user is trying
             # to use their system packages, we can't be more specific about what
             # they need.
+            clang_resolved = find_program(clang_path)
+            if not clang_resolved:
+                die(dedent('''\
+                The file {} returned by `llvm-config {}` does not exist.
+                clang is required to build Stylo.  Please install the necessary packages,
+                run `mach bootstrap`, or add --disable-stylo to your mozconfig.
+                '''.format(clang_path, '--bindir')))
+
             if not os.path.exists(libclang_path):
                 die(dedent('''\
                 The directory {} returned by `llvm-config {}` does not exist.
-                Please check your system configuration.
+                clang is required to build Stylo.  Please install the necessary packages,
+                run `mach bootstrap`, or add --disable-stylo to your mozconfig.
                 '''.format(libclang_path, libclang_arg)))
 
             (found, searched) = search_for_libclang(libclang_path)
             if not found:
                 die(dedent('''\
                 Could not find the clang shared library in the path {}
                 returned by `llvm-config {}` (searched for files {}).
-                Please check your system configuration.
+                clang is required to build Stylo.  Please install the necessary packages,
+                run `mach bootstrap`, or add --disable-stylo to your mozconfig.
                 '''.format(libclang_path, libclang_arg, searched)))
 
-            clang_resolved = find_program(clang_path)
-            if not clang_resolved:
-                die(dedent('''\
-                The file {} returned by `llvm-config {}` does not exist.
-                Please check your system configuration.
-                '''.format(clang_path, '--bindir')))
-
             return namespace(
                 libclang_path=libclang_path,
                 clang_path=clang_resolved,
             )
 
         if (not libclang_path and clang_path) or \
            (libclang_path and not clang_path):
             die(dedent('''\

