# HG changeset patch
# User Gerald Squelart <gsquelart@mozilla.com>
# Date 1557426375 0
# Node ID 3ddac071d565fb30846e19708adb4d302a487466
# Parent  8cbc7671e858dab6d6118df740bd85f809468033
Bug 1549232 - Only use profiler_current_{process,thread}_id in the Gecko Profiler instead of alternatives - r=mstange

There were many inconsistent ways to retrieve process/thread ids in the
profiler. Now we have only one platform-dependent implementation each:
profiler_current_process_id() and profiler_current_thread_id().

Note that this removes the need for the small `class Thread` in platform.h.
However memory_hooks.cpp still needs to be built non-unified, because of the
required order of #includes (replace_malloc.h before replace_malloc_bridge.h),
which could be disturbed by other cpp's.

Differential Revision: https://phabricator.services.mozilla.com/D29977

diff --git a/tools/profiler/core/ThreadInfo.cpp b/tools/profiler/core/ThreadInfo.cpp
--- a/tools/profiler/core/ThreadInfo.cpp
+++ b/tools/profiler/core/ThreadInfo.cpp
@@ -7,23 +7,16 @@
 #include "ThreadInfo.h"
 
 #include "mozilla/DebugOnly.h"
 
 #if defined(GP_OS_darwin)
 #include <pthread.h>
 #endif
 
-#ifdef XP_WIN
-#include <process.h>
-#define getpid _getpid
-#else
-#include <unistd.h> // for getpid()
-#endif
-
 ThreadInfo::ThreadInfo(const char* aName, int aThreadId, bool aIsMainThread,
                        void* aStackTop)
   : mName(strdup(aName))
   , mRegisterTime(TimeStamp::Now())
   , mThreadId(aThreadId)
   , mIsMainThread(aIsMainThread)
   , mRacyInfo(mozilla::WrapNotNull(new RacyThreadInfo()))
   , mPlatformData(AllocPlatformData(aThreadId))
@@ -162,17 +155,18 @@ StreamSamplesAndMarkers(const char* aNam
                         char* aSavedStreamedMarkers,
                         UniqueStacks& aUniqueStacks)
 {
   aWriter.StringProperty("processType",
                          XRE_ChildProcessTypeToString(XRE_GetProcessType()));
 
   aWriter.StringProperty("name", aName);
   aWriter.IntProperty("tid", static_cast<int64_t>(aThreadId));
-  aWriter.IntProperty("pid", static_cast<int64_t>(getpid()));
+  aWriter.IntProperty("pid",
+                      static_cast<int64_t>(profiler_current_process_id()));
 
   if (aRegisterTime) {
     aWriter.DoubleProperty("registerTime",
       (aRegisterTime - aProcessStartTime).ToMilliseconds());
   } else {
     aWriter.NullProperty("registerTime");
   }
 
diff --git a/tools/profiler/core/platform-linux-android.cpp b/tools/profiler/core/platform-linux-android.cpp
--- a/tools/profiler/core/platform-linux-android.cpp
+++ b/tools/profiler/core/platform-linux-android.cpp
@@ -64,20 +64,25 @@
 #include "mozilla/PodOperations.h"
 #include "mozilla/DebugOnly.h"
 
 #include <string.h>
 #include <list>
 
 using namespace mozilla;
 
-/* static */ int
-Thread::GetCurrentId()
-{
-  return gettid();
+int profiler_current_process_id() { return getpid(); }
+
+int profiler_current_thread_id() {
+  // glibc doesn't provide a wrapper for gettid().
+#if defined(__GLIBC__)
+  return static_cast<int>(static_cast<pid_t>(syscall(SYS_gettid)));
+#else
+  return static_cast<int>(gettid());
+#endif
 }
 
 void*
 GetStackTop(void* aGuess)
 {
   return aGuess;
 }
 
@@ -250,17 +255,17 @@ SigprofHandler(int aSignal, siginfo_t* a
   // |sSigHandlerCoordinator|.
   r = sem_post(&Sampler::sSigHandlerCoordinator->mMessage4);
   MOZ_ASSERT(r == 0);
 
   errno = savedErrno;
 }
 
 Sampler::Sampler(PSLockRef aLock)
-  : mMyPid(getpid())
+  : mMyPid(profiler_current_process_id())
   // We don't know what the sampler thread's ID will be until it runs, so set
   // mSamplerTid to a dummy value and fill it in for real in
   // SuspendAndSampleAndResumeThread().
   , mSamplerTid(-1)
 {
 #if defined(USE_EHABI_STACKWALK)
   mozilla::EHABIStackWalkInit();
 #elif defined(USE_LUL_STACKWALK)
@@ -314,17 +319,17 @@ Sampler::SuspendAndSampleAndResumeThread
                                          const ThreadInfo& aThreadInfo,
                                          const Func& aProcessRegs)
 {
   // Only one sampler thread can be sampling at once.  So we expect to have
   // complete control over |sSigHandlerCoordinator|.
   MOZ_ASSERT(!sSigHandlerCoordinator);
 
   if (mSamplerTid == -1) {
-    mSamplerTid = gettid();
+    mSamplerTid = profiler_current_thread_id();
   }
   int sampleeTid = aThreadInfo.ThreadId();
   MOZ_RELEASE_ASSERT(sampleeTid != mSamplerTid);
 
   //----------------------------------------------------------------//
   // Suspend the samplee thread and get its context.
 
   SigHandlerCoordinator coord;   // on sampler thread's stack
diff --git a/tools/profiler/core/platform-macos.cpp b/tools/profiler/core/platform-macos.cpp
--- a/tools/profiler/core/platform-macos.cpp
+++ b/tools/profiler/core/platform-macos.cpp
@@ -17,30 +17,31 @@
 #include <libkern/OSAtomic.h>
 #include <mach/mach.h>
 #include <mach/semaphore.h>
 #include <mach/task.h>
 #include <mach/thread_act.h>
 #include <mach/vm_statistics.h>
 #include <sys/time.h>
 #include <sys/resource.h>
+#include <sys/syscall.h>
 #include <sys/types.h>
 #include <sys/sysctl.h>
 #include <stdarg.h>
 #include <stdlib.h>
 #include <string.h>
 #include <errno.h>
 #include <math.h>
 
 // this port is based off of v8 svn revision 9837
 
-/* static */ int
-Thread::GetCurrentId()
-{
-  return gettid();
+int profiler_current_process_id() { return getpid(); }
+
+int profiler_current_thread_id() {
+  return static_cast<int>(static_cast<pid_t>(syscall(SYS_thread_selfid)));
 }
 
 void*
 GetStackTop(void* aGuess)
 {
   pthread_t thread = pthread_self();
   return pthread_get_stackaddr_np(thread);
 }
diff --git a/tools/profiler/core/platform-win32.cpp b/tools/profiler/core/platform-win32.cpp
--- a/tools/profiler/core/platform-win32.cpp
+++ b/tools/profiler/core/platform-win32.cpp
@@ -31,19 +31,19 @@
 #include <windows.h>
 #include <mmsystem.h>
 #include <process.h>
 
 #include "nsWindowsDllInterceptor.h"
 #include "mozilla/StackWalk_windows.h"
 #include "mozilla/WindowsVersion.h"
 
-/* static */ int
-Thread::GetCurrentId()
-{
+int profiler_current_process_id() { return _getpid(); }
+
+int profiler_current_thread_id() {
   DWORD threadId = GetCurrentThreadId();
   MOZ_ASSERT(threadId <= INT32_MAX, "native thread ID is > INT32_MAX");
   return int(threadId);
 }
 
 void*
 GetStackTop(void* aGuess)
 {
diff --git a/tools/profiler/core/platform.cpp b/tools/profiler/core/platform.cpp
--- a/tools/profiler/core/platform.cpp
+++ b/tools/profiler/core/platform.cpp
@@ -2110,17 +2110,17 @@ ParseFeaturesFromStringArray(const char*
 
 // Find the ThreadInfo for the current thread. This should only be called in
 // places where TLSInfo can't be used. On success, *aIndexOut is set to the
 // index if it is non-null.
 static ThreadInfo*
 FindLiveThreadInfo(PSLockRef aLock, int* aIndexOut = nullptr)
 {
   ThreadInfo* ret = nullptr;
-  int id = Thread::GetCurrentId();
+  int id = profiler_current_thread_id();
   const CorePS::ThreadVector& liveThreads = CorePS::LiveThreads(aLock);
   for (uint32_t i = 0; i < liveThreads.size(); i++) {
     ThreadInfo* info = liveThreads.at(i);
     if (info->ThreadId() == id) {
       if (aIndexOut) {
         *aIndexOut = i;
       }
       ret = info;
@@ -2137,17 +2137,17 @@ locked_register_thread(PSLockRef aLock, 
   MOZ_RELEASE_ASSERT(CorePS::Exists());
 
   MOZ_RELEASE_ASSERT(!FindLiveThreadInfo(aLock));
 
   if (!TLSInfo::Init(aLock)) {
     return;
   }
 
-  ThreadInfo* info = new ThreadInfo(aName, Thread::GetCurrentId(),
+  ThreadInfo* info = new ThreadInfo(aName, profiler_current_thread_id(),
                                     NS_IsMainThread(), aStackTop);
   TLSInfo::SetInfo(aLock, info);
 
   if (ActivePS::Exists(aLock) && ActivePS::ShouldProfileThread(aLock, info)) {
     info->StartProfiling();
     if (ActivePS::FeatureJS(aLock)) {
       // This StartJSSampling() call is on-thread, so we can poll manually to
       // start JS sampling immediately.
@@ -2675,17 +2675,17 @@ locked_profiler_start(PSLockRef aLock, i
 
   // Fall back to the default values if the passed-in values are unreasonable.
   int entries = aEntries > 0 ? aEntries : PROFILER_DEFAULT_ENTRIES;
   double interval = aInterval > 0 ? aInterval : PROFILER_DEFAULT_INTERVAL;
 
   ActivePS::Create(aLock, entries, interval, aFeatures, aFilters, aFilterCount);
 
   // Set up profiling for each registered thread, if appropriate.
-  int tid = Thread::GetCurrentId();
+  int tid = profiler_current_thread_id();
   const CorePS::ThreadVector& liveThreads = CorePS::LiveThreads(aLock);
   for (uint32_t i = 0; i < liveThreads.size(); i++) {
     ThreadInfo* info = liveThreads.at(i);
 
     if (ActivePS::ShouldProfileThread(aLock, info)) {
       info->StartProfiling();
       if (ActivePS::FeatureJS(aLock)) {
         info->StartJSSampling();
@@ -2819,17 +2819,17 @@ locked_profiler_stop(PSLockRef aLock)
 
 #ifdef MOZ_TASK_TRACER
   if (ActivePS::FeatureTaskTracer(aLock)) {
     tasktracer::StopLogging();
   }
 #endif
 
   // Stop sampling live threads.
-  int tid = Thread::GetCurrentId();
+  int tid = profiler_current_thread_id();
   CorePS::ThreadVector& liveThreads = CorePS::LiveThreads(aLock);
   for (uint32_t i = 0; i < liveThreads.size(); i++) {
     ThreadInfo* info = liveThreads.at(i);
     if (info->IsBeingProfiled()) {
       if (ActivePS::FeatureJS(aLock)) {
         info->StopJSSampling();
         if (info->ThreadId() == tid) {
           // We can manually poll the current thread so it stops profiling
@@ -3114,17 +3114,17 @@ profiler_get_backtrace()
   }
 
   ThreadInfo* info = TLSInfo::Info(lock);
   if (!info) {
     MOZ_ASSERT(info);
     return nullptr;
   }
 
-  int tid = Thread::GetCurrentId();
+  int tid = profiler_current_thread_id();
 
   TimeStamp now = TimeStamp::Now();
 
   Registers regs;
 #if defined(HAVE_NATIVE_UNWIND)
   regs.SyncPopulate();
 #else
   regs.Clear();
@@ -3327,22 +3327,16 @@ profiler_clear_js_context()
   }
 
   // We don't call info->StopJSSampling() here; there's no point doing that for
   // a JS thread that is in the process of disappearing.
 
   info->mContext = nullptr;
 }
 
-int
-profiler_current_thread_id()
-{
-  return Thread::GetCurrentId();
-}
-
 // NOTE: The callback function passed in will be called while the target thread
 // is paused. Doing stuff in this function like allocating which may try to
 // claim locks is a surefire way to deadlock.
 void
 profiler_suspend_and_sample_thread(
   int aThreadId,
   const std::function<ProfilerStackCallback>& aCallback,
   bool aSampleNative /* = true */)
diff --git a/tools/profiler/core/platform.h b/tools/profiler/core/platform.h
--- a/tools/profiler/core/platform.h
+++ b/tools/profiler/core/platform.h
@@ -35,71 +35,44 @@
 #include "ThreadResponsiveness.h"
 #include "mozilla/Logging.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/StaticMutex.h"
 #include "mozilla/TimeStamp.h"
 #include "mozilla/UniquePtr.h"
 #include "mozilla/Unused.h"
 #include "PlatformMacros.h"
-#include <vector>
 
-// We need a definition of gettid(), but glibc doesn't provide a
-// wrapper for it.
-#if defined(__GLIBC__)
-#  include <unistd.h>
-#  include <sys/syscall.h>
-#  define gettid() static_cast<pid_t>(syscall(SYS_gettid))
-#elif defined(GP_OS_darwin)
-#  include <unistd.h>
-#  include <sys/syscall.h>
-#  define gettid() static_cast<pid_t>(syscall(SYS_thread_selfid))
-#elif defined(GP_OS_android)
-#  include <unistd.h>
-#elif defined(GP_OS_windows)
-#  include <windows.h>
-#  include <process.h>
-#  ifndef getpid
-#    define getpid _getpid
-#endif
-#endif
+#include "GeckoProfiler.h"
+
+#include <vector>
 
 extern mozilla::LazyLogModule gProfilerLog;
 
 // These are for MOZ_LOG="prof:3" or higher. It's the default logging level for
 // the profiler, and should be used sparingly.
 #define LOG_TEST \
   MOZ_LOG_TEST(gProfilerLog, mozilla::LogLevel::Info)
 #define LOG(arg, ...) \
   MOZ_LOG(gProfilerLog, mozilla::LogLevel::Info, \
-          ("[%d] " arg, getpid(), ##__VA_ARGS__))
+          ("[%d] " arg, profiler_current_process_id(), ##__VA_ARGS__))
+
 
 // These are for MOZ_LOG="prof:4" or higher. It should be used for logging that
 // is somewhat more verbose than LOG.
 #define DEBUG_LOG_TEST \
   MOZ_LOG_TEST(gProfilerLog, mozilla::LogLevel::Debug)
 #define DEBUG_LOG(arg, ...) \
   MOZ_LOG(gProfilerLog, mozilla::LogLevel::Debug, \
-          ("[%d] " arg, getpid(), ##__VA_ARGS__))
+          ("[%d] " arg, profiler_current_process_id(), ##__VA_ARGS__))
+
 
 typedef uint8_t* Address;
 
 // ----------------------------------------------------------------------------
-// Thread
-//
-// This class has static methods for the different platform specific
-// functions. Add methods here to cope with differences between the
-// supported platforms.
-
-class Thread {
-public:
-  static int GetCurrentId();
-};
-
-// ----------------------------------------------------------------------------
 // Miscellaneous
 
 class PlatformData;
 
 // We can't new/delete the type safely without defining it
 // (-Wdelete-incomplete).  Use these to hide the details from clients.
 struct PlatformDataDestructor {
   void operator()(PlatformData*);
diff --git a/tools/profiler/core/shared-libraries-linux.cc b/tools/profiler/core/shared-libraries-linux.cc
--- a/tools/profiler/core/shared-libraries-linux.cc
+++ b/tools/profiler/core/shared-libraries-linux.cc
@@ -137,17 +137,17 @@ SharedLibraryInfo SharedLibraryInfo::Get
     // So if libxul was loaded by the system linker (e.g. as part of
     // xpcshell when running tests), it won't be available and we should
     // not call it.
     return info;
   }
 #endif
 
   // Read info from /proc/self/maps. We ignore most of it.
-  pid_t pid = getpid();
+  pid_t pid = profiler_current_process_id();
   char path[PATH_MAX];
   SprintfLiteral(path, "/proc/%d/maps", pid);
   std::ifstream maps(path);
   std::string line;
   while (std::getline(maps, line)) {
     int ret;
     unsigned long start;
     unsigned long end;
diff --git a/tools/profiler/lul/LulMain.cpp b/tools/profiler/lul/LulMain.cpp
--- a/tools/profiler/lul/LulMain.cpp
+++ b/tools/profiler/lul/LulMain.cpp
@@ -22,17 +22,17 @@
 #include "mozilla/Sprintf.h"
 #include "mozilla/UniquePtr.h"
 
 #include "LulCommonExt.h"
 #include "LulElfExt.h"
 
 #include "LulMainInt.h"
 
-#include "platform-linux-lul.h"  // for gettid()
+#include "GeckoProfiler.h"  // for profiler_current_thread_id()
 
 // Set this to 1 for verbose logging
 #define DEBUG_MAIN 0
 
 namespace lul {
 
 using std::string;
 using std::vector;
@@ -645,30 +645,30 @@ class PriMap {
   void (*mLog)(const char*);
 };
 
 
 ////////////////////////////////////////////////////////////////
 // LUL                                                        //
 ////////////////////////////////////////////////////////////////
 
-#define LUL_LOG(_str) \
-  do { \
-    char buf[200]; \
-    SprintfLiteral(buf, \
-                   "LUL: pid %d tid %d lul-obj %p: %s", \
-                   getpid(), gettid(), this, (_str));   \
-    buf[sizeof(buf)-1] = 0; \
-    mLog(buf); \
+#define LUL_LOG(_str)                                           \
+  do {                                                          \
+    char buf[200];                                              \
+    SprintfLiteral(buf, "LUL: pid %d tid %d lul-obj %p: %s",    \
+                   profiler_current_process_id(),               \
+                   profiler_current_thread_id(), this, (_str)); \
+    buf[sizeof(buf) - 1] = 0;                                   \
+    mLog(buf);                                                  \
   } while (0)
 
 LUL::LUL(void (*aLog)(const char*))
   : mLog(aLog)
   , mAdminMode(true)
-  , mAdminThreadId(gettid())
+  , mAdminThreadId(profiler_current_thread_id())
   , mPriMap(new PriMap(aLog))
   , mSegArray(new SegArray())
   , mUSU(new UniqueStringUniverse())
 {
   LUL_LOG("LUL::LUL: Created object");
 }
 
 
@@ -723,28 +723,28 @@ LUL::SizeOfIncludingThis(MallocSizeOf aM
 
 
 void
 LUL::EnableUnwinding()
 {
   LUL_LOG("LUL::EnableUnwinding");
   // Don't assert for Admin mode here.  That is, tolerate a call here
   // if we are already in Unwinding mode.
-  MOZ_ASSERT(gettid() == mAdminThreadId);
+  MOZ_ASSERT(profiler_current_thread_id() == mAdminThreadId);
 
   mAdminMode = false;
 }
 
 
 void
 LUL::NotifyAfterMap(uintptr_t aRXavma, size_t aSize,
                     const char* aFileName, const void* aMappedImage)
 {
   MOZ_ASSERT(mAdminMode);
-  MOZ_ASSERT(gettid() == mAdminThreadId);
+  MOZ_ASSERT(profiler_current_thread_id() == mAdminThreadId);
 
   mLog(":\n");
   char buf[200];
   SprintfLiteral(buf, "NotifyMap %llx %llu %s\n",
                  (unsigned long long int)aRXavma, (unsigned long long int)aSize,
                  aFileName);
   buf[sizeof(buf)-1] = 0;
   mLog(buf);
@@ -785,17 +785,17 @@ LUL::NotifyAfterMap(uintptr_t aRXavma, s
   }
 }
 
 
 void
 LUL::NotifyExecutableArea(uintptr_t aRXavma, size_t aSize)
 {
   MOZ_ASSERT(mAdminMode);
-  MOZ_ASSERT(gettid() == mAdminThreadId);
+  MOZ_ASSERT(profiler_current_thread_id() == mAdminThreadId);
 
   mLog(":\n");
   char buf[200];
   SprintfLiteral(buf, "NotifyExecutableArea %llx %llu\n",
                    (unsigned long long int)aRXavma, (unsigned long long int)aSize);
   buf[sizeof(buf)-1] = 0;
   mLog(buf);
 
@@ -807,17 +807,17 @@ LUL::NotifyExecutableArea(uintptr_t aRXa
   }
 }
 
 
 void
 LUL::NotifyBeforeUnmap(uintptr_t aRXavmaMin, uintptr_t aRXavmaMax)
 {
   MOZ_ASSERT(mAdminMode);
-  MOZ_ASSERT(gettid() == mAdminThreadId);
+  MOZ_ASSERT(profiler_current_thread_id() == mAdminThreadId);
 
   mLog(":\n");
   char buf[100];
   SprintfLiteral(buf, "NotifyUnmap %016llx-%016llx\n",
                  (unsigned long long int)aRXavmaMin,
                  (unsigned long long int)aRXavmaMax);
   buf[sizeof(buf)-1] = 0;
   mLog(buf);
@@ -838,17 +838,17 @@ LUL::NotifyBeforeUnmap(uintptr_t aRXavma
   mLog(buf);
 }
 
 
 size_t
 LUL::CountMappings()
 {
   MOZ_ASSERT(mAdminMode);
-  MOZ_ASSERT(gettid() == mAdminThreadId);
+  MOZ_ASSERT(profiler_current_thread_id() == mAdminThreadId);
 
   return mPriMap->CountSecMaps();
 }
 
 
 // RUNS IN NO-MALLOC CONTEXT
 static
 TaggedUWord DerefTUW(TaggedUWord aAddr, const StackImage* aStackImg)
diff --git a/tools/profiler/lul/platform-linux-lul.cpp b/tools/profiler/lul/platform-linux-lul.cpp
--- a/tools/profiler/lul/platform-linux-lul.cpp
+++ b/tools/profiler/lul/platform-linux-lul.cpp
@@ -72,10 +72,11 @@ read_procmaps(lul::LUL* aLUL)
 // LUL needs a callback for its logging sink.
 void
 logging_sink_for_LUL(const char* str)
 {
   // These are only printed when Verbose logging is enabled (e.g. with
   // MOZ_LOG="prof:5"). This is because LUL's logging is much more verbose than
   // the rest of the profiler's logging, which occurs at the Info (3) and Debug
   // (4) levels.
-  MOZ_LOG(gProfilerLog, mozilla::LogLevel::Verbose, ("[%d] %s", getpid(), str));
+  MOZ_LOG(gProfilerLog, mozilla::LogLevel::Verbose,
+          ("[%d] %s", profiler_current_process_id(), str));
 }
diff --git a/tools/profiler/public/GeckoProfiler.h b/tools/profiler/public/GeckoProfiler.h
--- a/tools/profiler/public/GeckoProfiler.h
+++ b/tools/profiler/public/GeckoProfiler.h
@@ -257,16 +257,19 @@ PROFILER_FUNC_VOID(
                             mozilla::Vector<const char*, 0,
                                             mozilla::MallocAllocPolicy>*
                               aFilters))
 
 // The number of milliseconds since the process started. Operates the same
 // whether the profiler is active or inactive.
 PROFILER_FUNC(double profiler_time(), 0)
 
+// Get the current process's ID.
+PROFILER_FUNC(int profiler_current_process_id(), 0)
+
 // Get the current thread's ID.
 PROFILER_FUNC(int profiler_current_thread_id(), 0)
 
 // This is the function type of the callback passed to profiler_suspend_and_sample_thread.
 //
 // The callback is passed the following arguments:
 //   void** aPCs         The program counters for the target thread's stack.
 //   size_t aCount       The number of program counters in the aPCs array.
diff --git a/tools/profiler/tasktracer/GeckoTaskTracer.cpp b/tools/profiler/tasktracer/GeckoTaskTracer.cpp
--- a/tools/profiler/tasktracer/GeckoTaskTracer.cpp
+++ b/tools/profiler/tasktracer/GeckoTaskTracer.cpp
@@ -16,21 +16,16 @@
 
 #include "nsString.h"
 #include "nsThreadUtils.h"
 #include "platform.h"
 #include "prtime.h"
 
 #include <stdarg.h>
 
-#if defined(GP_OS_windows)
-#include <windows.h>
-#define getpid GetCurrentProcessId
-#endif
-
 #define MAX_SIZE_LOG (1024 * 128)
 
 // NS_ENSURE_TRUE_VOID() without the warning on the debug build.
 #define ENSURE_TRUE_VOID(x)   \
   do {                        \
     if (MOZ_UNLIKELY(!(x))) { \
        return;                \
     }                         \
@@ -241,32 +236,32 @@ GetOrCreateTraceInfo()
 
   if (info && info->mObsolete) {
     // TraceInfo is obsolete: remove it.
     FreeTraceInfo(info);
     info = nullptr;
   }
 
   if (!info) {
-    info = AllocTraceInfo(Thread::GetCurrentId());
+    info = AllocTraceInfo(profiler_current_thread_id());
     sTraceInfoTLS.set(info);
   }
 
   return TraceInfoHolder{info}; // |mLogsMutex| will be held, then
                                 // ||sMutex| will be released for
                                 // efficiency reason.
 }
 
 uint64_t
 GenNewUniqueTaskId()
 {
   TraceInfoHolder info = GetOrCreateTraceInfo();
   ENSURE_TRUE(info, 0);
 
-  int tid = Thread::GetCurrentId();
+  int tid = profiler_current_thread_id();
   uint64_t taskid = ((uint64_t)tid << 32) | ++info->mLastUniqueTaskId;
   return taskid;
 }
 
 AutoSaveCurTraceInfoImpl::AutoSaveCurTraceInfoImpl()
 {
   GetCurTraceInfo(&mSavedSourceEventId, &mSavedTaskId, &mSavedSourceEventType);
 }
@@ -340,18 +335,18 @@ LogBegin(uint64_t aTaskId, uint64_t aSou
 
   // Log format:
   // [1 taskId beginTime processId threadId]
   TraceInfoLogType* log = info->AppendLog();
   if (log) {
     log->mBegin.mType = ACTION_BEGIN;
     log->mBegin.mTaskId = aTaskId;
     log->mBegin.mTime = GetTimestamp();
-    log->mBegin.mPid = getpid();
-    log->mBegin.mTid = Thread::GetCurrentId();
+    log->mBegin.mPid = profiler_current_process_id();
+    log->mBegin.mTid = profiler_current_thread_id();
 
     MOZ_ASSERT(log->mBegin.mPid >= 0, "native process ID is < 0 (signed integer overflow)");
     MOZ_ASSERT(log->mBegin.mTid >= 0, "native thread ID is < 0  (signed integer overflow)");
   }
 }
 
 void
 LogEnd(uint64_t aTaskId, uint64_t aSourceEventId)
diff --git a/tools/profiler/tests/gtest/GeckoProfiler.cpp b/tools/profiler/tests/gtest/GeckoProfiler.cpp
--- a/tools/profiler/tests/gtest/GeckoProfiler.cpp
+++ b/tools/profiler/tests/gtest/GeckoProfiler.cpp
@@ -776,17 +776,17 @@ void DoSuspendAndSample(int aTid, nsIThr
 }
 
 TEST(GeckoProfiler, SuspendAndSample)
 {
   nsCOMPtr<nsIThread> thread;
   nsresult rv = NS_NewNamedThread("GeckoProfGTest", getter_AddRefs(thread));
   ASSERT_TRUE(NS_SUCCEEDED(rv));
 
-  int tid = Thread::GetCurrentId();
+  int tid = profiler_current_thread_id();
 
   ASSERT_TRUE(!profiler_is_active());
 
   // Suspend and sample while the profiler is inactive.
   DoSuspendAndSample(tid, thread);
 
   uint32_t features = ProfilerFeature::JS | ProfilerFeature::Threads;
   const char* filters[] = { "GeckoMain", "Compositor" };
