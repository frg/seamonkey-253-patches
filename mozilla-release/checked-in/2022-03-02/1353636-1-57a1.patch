# HG changeset patch
# User Ho-Pang Hsu <hopang.hsu@gmail.com>
# Date 1502864332 -28800
#      Wed Aug 16 14:18:52 2017 +0800
# Node ID 9865180c7e5972d6bf0f490121a2426046624575
# Parent  451ba9bf269effc5c1ed3ae5b91845564ab45518
Bug 1353636 - Part 1: Move from nsLoadFlags to UpdateViaCache. r=bkelly

diff --git a/dom/interfaces/base/nsIServiceWorkerManager.idl b/dom/interfaces/base/nsIServiceWorkerManager.idl
--- a/dom/interfaces/base/nsIServiceWorkerManager.idl
+++ b/dom/interfaces/base/nsIServiceWorkerManager.idl
@@ -60,16 +60,21 @@ interface nsIServiceWorkerInfo : nsISupp
 interface nsIServiceWorkerRegistrationInfoListener : nsISupports
 {
   void onChange();
 };
 
 [scriptable, builtinclass, uuid(ddbc1fd4-2f2e-4fca-a395-6e010bbedfe3)]
 interface nsIServiceWorkerRegistrationInfo : nsISupports
 {
+  // State values below should match the ServiceWorkerUpdateViaCache enumeration.
+  const unsigned short UPDATE_VIA_CACHE_IMPORTS = 0;
+  const unsigned short UPDATE_VIA_CACHE_ALL = 1;
+  const unsigned short UPDATE_VIA_CACHE_NONE = 2;
+
   readonly attribute nsIPrincipal principal;
 
   readonly attribute DOMString scope;
   readonly attribute DOMString scriptSpec;
 
   readonly attribute PRTime lastUpdateTime;
 
   readonly attribute nsIServiceWorkerInfo installingWorker;
@@ -105,17 +110,17 @@ interface nsIServiceWorkerManager : nsIS
    * the stack. This means you must call this from content code 'within'
    * a window.
    *
    * Returns a Promise.
    */
   nsISupports register(in mozIDOMWindow aWindow,
                        in nsIURI aScope,
                        in nsIURI aScriptURI,
-                       in nsLoadFlags aLoadFlags);
+                       in uint16_t aUpdateViaCache);
 
   /**
    * Unregister an existing ServiceWorker registration for `aScope`.
    * It keeps aCallback alive until the operation is concluded.
    */
   void unregister(in nsIPrincipal aPrincipal,
                   in nsIServiceWorkerUnregisterCallback aCallback,
                   in DOMString aScope);
diff --git a/dom/webidl/ServiceWorkerContainer.webidl b/dom/webidl/ServiceWorkerContainer.webidl
--- a/dom/webidl/ServiceWorkerContainer.webidl
+++ b/dom/webidl/ServiceWorkerContainer.webidl
@@ -37,9 +37,10 @@ interface ServiceWorkerContainer : Event
 // Testing only.
 partial interface ServiceWorkerContainer {
   [Throws,Pref="dom.serviceWorkers.testing.enabled"]
   DOMString getScopeForUrl(DOMString url);
 };
 
 dictionary RegistrationOptions {
   USVString scope;
+  ServiceWorkerUpdateViaCache updateViaCache = "imports";
 };
diff --git a/dom/webidl/ServiceWorkerRegistration.webidl b/dom/webidl/ServiceWorkerRegistration.webidl
--- a/dom/webidl/ServiceWorkerRegistration.webidl
+++ b/dom/webidl/ServiceWorkerRegistration.webidl
@@ -23,16 +23,22 @@ interface ServiceWorkerRegistration : Ev
 
   [Throws, NewObject]
   Promise<boolean> unregister();
 
   // event
   attribute EventHandler onupdatefound;
 };
 
+enum ServiceWorkerUpdateViaCache {
+  "imports",
+  "all",
+  "none"
+};
+
 // https://w3c.github.io/push-api/
 partial interface ServiceWorkerRegistration {
   [Throws, Exposed=(Window,Worker), Func="nsContentUtils::PushEnabled"]
   readonly attribute PushManager pushManager;
 };
 
 // https://notifications.spec.whatwg.org/
 partial interface ServiceWorkerRegistration {
diff --git a/dom/workers/ServiceWorkerContainer.cpp b/dom/workers/ServiceWorkerContainer.cpp
--- a/dom/workers/ServiceWorkerContainer.cpp
+++ b/dom/workers/ServiceWorkerContainer.cpp
@@ -194,23 +194,20 @@ ServiceWorkerContainer::Register(const n
     }
 
     aRv = CheckForSlashEscapedCharsInPath(scopeURI);
     if (NS_WARN_IF(aRv.Failed())) {
       return nullptr;
     }
   }
 
-  // This is a quick fix for temporarily turning off script loading setting when
-  // registering a service worker. This should be removed in Bug 1353636.
-  nsLoadFlags loadFlags = nsIRequest::LOAD_NORMAL;
-
   // The spec says that the "client" passed to Register() must be the global
   // where the ServiceWorkerContainer was retrieved from.
-  aRv = swm->Register(GetOwner(), scopeURI, scriptURI, loadFlags,
+  aRv = swm->Register(GetOwner(), scopeURI, scriptURI,
+                      static_cast<uint16_t>(aOptions.mUpdateViaCache),
                       getter_AddRefs(promise));
   if (NS_WARN_IF(aRv.Failed())) {
     return nullptr;
   }
 
   RefPtr<Promise> ret = static_cast<Promise*>(promise.get());
   MOZ_ASSERT(ret);
   return ret.forget();
diff --git a/dom/workers/ServiceWorkerInfo.cpp b/dom/workers/ServiceWorkerInfo.cpp
--- a/dom/workers/ServiceWorkerInfo.cpp
+++ b/dom/workers/ServiceWorkerInfo.cpp
@@ -204,39 +204,44 @@ ServiceWorkerInfo::UpdateState(ServiceWo
     serviceWorkerScriptCache::PurgeCache(mPrincipal, mCacheName);
   }
 }
 
 ServiceWorkerInfo::ServiceWorkerInfo(nsIPrincipal* aPrincipal,
                                      const nsACString& aScope,
                                      const nsACString& aScriptSpec,
                                      const nsAString& aCacheName,
-                                     nsLoadFlags aLoadFlags)
+                                     nsLoadFlags aImportsLoadFlags)
   : mPrincipal(aPrincipal)
   , mScope(aScope)
   , mScriptSpec(aScriptSpec)
   , mCacheName(aCacheName)
-  , mLoadFlags(aLoadFlags)
   , mState(ServiceWorkerState::EndGuard_)
+  , mImportsLoadFlags(aImportsLoadFlags)
   , mServiceWorkerID(GetNextID())
   , mCreationTime(PR_Now())
   , mCreationTimeStamp(TimeStamp::Now())
   , mInstalledTime(0)
   , mActivatedTime(0)
   , mRedundantTime(0)
   , mServiceWorkerPrivate(new ServiceWorkerPrivate(this))
   , mSkipWaitingFlag(false)
   , mHandlesFetch(Unknown)
 {
   MOZ_ASSERT(mPrincipal);
   // cache origin attributes so we can use them off main thread
   mOriginAttributes = mPrincipal->OriginAttributesRef();
   MOZ_ASSERT(!mScope.IsEmpty());
   MOZ_ASSERT(!mScriptSpec.IsEmpty());
   MOZ_ASSERT(!mCacheName.IsEmpty());
+
+  // Scripts of a service worker should always be loaded bypass service workers.
+  // Otherwise, we might not be able to update a service worker correctly, if
+  // there is a service worker generating the script.
+  MOZ_DIAGNOSTIC_ASSERT(mImportsLoadFlags & nsIChannel::LOAD_BYPASS_SERVICE_WORKER);
 }
 
 ServiceWorkerInfo::~ServiceWorkerInfo()
 {
   MOZ_ASSERT(mServiceWorkerPrivate);
   mServiceWorkerPrivate->NoteDeadServiceWorkerInfo();
 }
 
diff --git a/dom/workers/ServiceWorkerInfo.h b/dom/workers/ServiceWorkerInfo.h
--- a/dom/workers/ServiceWorkerInfo.h
+++ b/dom/workers/ServiceWorkerInfo.h
@@ -26,20 +26,29 @@ class ServiceWorkerPrivate;
  */
 class ServiceWorkerInfo final : public nsIServiceWorkerInfo
 {
 private:
   nsCOMPtr<nsIPrincipal> mPrincipal;
   const nsCString mScope;
   const nsCString mScriptSpec;
   const nsString mCacheName;
-  const nsLoadFlags mLoadFlags;
   ServiceWorkerState mState;
   OriginAttributes mOriginAttributes;
 
+  // This LoadFlags is only applied to imported scripts, since the main script
+  // has already been downloaded when performing the bytecheck. This LoadFlag is
+  // composed of three parts:
+  //   1. nsIChannel::LOAD_BYPASS_SERVICE_WORKER
+  //   2. (Optional) nsIRequest::VALIDATE_ALWAYS
+  //      depends on ServiceWorkerUpdateViaCache of its registration.
+  //   3. (optional) nsIRequest::LOAD_BYPASS_CACHE
+  //      depends on whether the update timer is expired.
+  const nsLoadFlags mImportsLoadFlags;
+
   // This id is shared with WorkerPrivate to match requests issued by service
   // workers to their corresponding serviceWorkerInfo.
   uint64_t mServiceWorkerID;
 
   // Timestamp to track SW's state
   PRTime mCreationTime;
   TimeStamp mCreationTimeStamp;
 
@@ -132,19 +141,19 @@ public:
 
   const nsString&
   CacheName() const
   {
     return mCacheName;
   }
 
   nsLoadFlags
-  GetLoadFlags() const
+  GetImportsLoadFlags() const
   {
-    return mLoadFlags;
+    return mImportsLoadFlags;
   }
 
   uint64_t
   ID() const
   {
     return mServiceWorkerID;
   }
 
diff --git a/dom/workers/ServiceWorkerManager.cpp b/dom/workers/ServiceWorkerManager.cpp
--- a/dom/workers/ServiceWorkerManager.cpp
+++ b/dom/workers/ServiceWorkerManager.cpp
@@ -128,16 +128,29 @@ static_assert(nsIHttpChannelInternal::FE
              "RequestCache enumeration value should match Necko Cache mode value.");
 static_assert(nsIHttpChannelInternal::FETCH_CACHE_MODE_FORCE_CACHE == static_cast<uint32_t>(RequestCache::Force_cache),
              "RequestCache enumeration value should match Necko Cache mode value.");
 static_assert(nsIHttpChannelInternal::FETCH_CACHE_MODE_ONLY_IF_CACHED == static_cast<uint32_t>(RequestCache::Only_if_cached),
              "RequestCache enumeration value should match Necko Cache mode value.");
 static_assert(6 == static_cast<uint32_t>(RequestCache::EndGuard_),
              "RequestCache enumeration value should match Necko Cache mode value.");
 
+static_assert(static_cast<uint16_t>(ServiceWorkerUpdateViaCache::Imports) ==
+              nsIServiceWorkerRegistrationInfo::UPDATE_VIA_CACHE_IMPORTS,
+              "nsIServiceWorkerRegistrationInfo::UPDATE_VIA_CACHE_*"
+              " should match ServiceWorkerUpdateViaCache enumeration.");
+static_assert(static_cast<uint16_t>(ServiceWorkerUpdateViaCache::All) ==
+              nsIServiceWorkerRegistrationInfo::UPDATE_VIA_CACHE_ALL,
+              "nsIServiceWorkerRegistrationInfo::UPDATE_VIA_CACHE_*"
+              " should match ServiceWorkerUpdateViaCache enumeration.");
+static_assert(static_cast<uint16_t>(ServiceWorkerUpdateViaCache::None) ==
+              nsIServiceWorkerRegistrationInfo::UPDATE_VIA_CACHE_NONE,
+              "nsIServiceWorkerRegistrationInfo::UPDATE_VIA_CACHE_*"
+              " should match ServiceWorkerUpdateViaCache enumeration.");
+
 static StaticRefPtr<ServiceWorkerManager> gInstance;
 
 struct ServiceWorkerManager::RegistrationDataPerPrincipal final
 {
   // Ordered list of scopes for glob matching.
   // Each entry is an absolute URL representing the scope.
   // Each value of the hash table is an array of an absolute URLs representing
   // the scopes.
@@ -191,17 +204,22 @@ PopulateRegistrationData(nsIPrincipal* a
     aData.currentWorkerHandlesFetch() = aRegistration->GetActive()->HandlesFetch();
 
     aData.currentWorkerInstalledTime() =
       aRegistration->GetActive()->GetInstalledTime();
     aData.currentWorkerActivatedTime() =
       aRegistration->GetActive()->GetActivatedTime();
   }
 
-  aData.loadFlags() = aRegistration->GetLoadFlags();
+  // This is a workaround before we update SERVICEWORKERREGISTRAR_VERSION and
+  // the related implementation.
+  ServiceWorkerUpdateViaCache uvc = aRegistration->GetUpdateViaCache();
+  aData.loadFlags() =
+    uvc == ServiceWorkerUpdateViaCache::None ? nsIRequest::LOAD_NORMAL
+                                             : nsIRequest::VALIDATE_ALWAYS;
 
   aData.lastUpdateTime() = aRegistration->GetLastUpdateTime();
 
   return NS_OK;
 }
 
 class TeardownRunnable final : public Runnable
 {
@@ -806,17 +824,17 @@ IsFromAuthenticatedOrigin(nsIDocument* a
 }
 
 // If we return an error code here, the ServiceWorkerContainer will
 // automatically reject the Promise.
 NS_IMETHODIMP
 ServiceWorkerManager::Register(mozIDOMWindow* aWindow,
                                nsIURI* aScopeURI,
                                nsIURI* aScriptURI,
-                               nsLoadFlags aLoadFlags,
+                               uint16_t aUpdateViaCache,
                                nsISupports** aPromise)
 {
   AssertIsOnMainThread();
 
   if (NS_WARN_IF(!aWindow)) {
     return NS_ERROR_DOM_INVALID_STATE_ERR;
   }
 
@@ -931,19 +949,21 @@ ServiceWorkerManager::Register(mozIDOMWi
   ir->MaybeAddTabChild(docLoadGroup);
 
   // Create a load group that is separate from, yet related to, the document's load group.
   // This allows checks for interfaces like nsILoadContext to yield the values used by the
   // the document, yet will not cancel the update job if the document's load group is cancelled.
   nsCOMPtr<nsILoadGroup> loadGroup = do_CreateInstance(NS_LOADGROUP_CONTRACTID);
   MOZ_ALWAYS_SUCCEEDS(loadGroup->SetNotificationCallbacks(ir));
 
-  RefPtr<ServiceWorkerRegisterJob> job =
-    new ServiceWorkerRegisterJob(documentPrincipal, cleanedScope, spec,
-                                 loadGroup, aLoadFlags);
+  RefPtr<ServiceWorkerRegisterJob> job = new ServiceWorkerRegisterJob(
+    documentPrincipal, cleanedScope, spec, loadGroup,
+    static_cast<ServiceWorkerUpdateViaCache>(aUpdateViaCache)
+  );
+
   job->AppendResultCallback(cb);
   queue->ScheduleJob(job);
 
   AssertIsOnMainThread();
   Telemetry::Accumulate(Telemetry::SERVICE_WORKER_REGISTRATIONS, 1);
 
   ContentChild* contentChild = ContentChild::GetSingleton();
   if (contentChild &&
@@ -2035,21 +2055,27 @@ ServiceWorkerManager::LoadRegistration(
   AssertIsOnMainThread();
 
   nsCOMPtr<nsIPrincipal> principal =
     PrincipalInfoToPrincipal(aRegistration.principal());
   if (!principal) {
     return;
   }
 
+  // This is a workaround before we update SERVICEWORKERREGISTRAR_VERSION and
+  // the related implementation.
+  nsLoadFlags flags = aRegistration.loadFlags();
+  ServiceWorkerUpdateViaCache uvc =
+    flags == nsIRequest::VALIDATE_ALWAYS ? ServiceWorkerUpdateViaCache::Imports
+                                         : ServiceWorkerUpdateViaCache::All;
+
   RefPtr<ServiceWorkerRegistrationInfo> registration =
     GetRegistration(principal, aRegistration.scope());
   if (!registration) {
-    registration = CreateNewRegistration(aRegistration.scope(), principal,
-                                         aRegistration.loadFlags());
+    registration = CreateNewRegistration(aRegistration.scope(), principal, uvc);
   } else {
     // If active worker script matches our expectations for a "current worker",
     // then we are done. Since scripts with the same URL might have different
     // contents such as updated scripts or scripts with different LoadFlags, we
     // use the CacheName to judje whether the two scripts are identical, where
     // the CacheName is an UUID generated when a new script is found.
     if (registration->GetActive() &&
         registration->GetActive()->CacheName() == aRegistration.cacheName()) {
@@ -2062,17 +2088,17 @@ ServiceWorkerManager::LoadRegistration(
 
   const nsCString& currentWorkerURL = aRegistration.currentWorkerURL();
   if (!currentWorkerURL.IsEmpty()) {
     registration->SetActive(
       new ServiceWorkerInfo(registration->mPrincipal,
                             registration->mScope,
                             currentWorkerURL,
                             aRegistration.cacheName(),
-                            registration->GetLoadFlags()));
+                            nsIChannel::LOAD_BYPASS_SERVICE_WORKER | flags));
     registration->GetActive()->SetHandlesFetch(aRegistration.currentWorkerHandlesFetch());
     registration->GetActive()->SetInstalledTime(aRegistration.currentWorkerInstalledTime());
     registration->GetActive()->SetActivatedTime(aRegistration.currentWorkerActivatedTime());
   }
 }
 
 void
 ServiceWorkerManager::LoadRegistrations(
@@ -3122,17 +3148,17 @@ ServiceWorkerManager::SoftUpdateInternal
   // TODO(catalinb): We don't implement the force bypass cache flag.
   // See: https://github.com/slightlyoff/ServiceWorker/issues/759
   RefPtr<ServiceWorkerJobQueue> queue = GetOrCreateJobQueue(scopeKey,
                                                             aScope);
 
   RefPtr<ServiceWorkerUpdateJob> job =
     new ServiceWorkerUpdateJob(principal, registration->mScope,
                                newest->ScriptSpec(), nullptr,
-                               registration->GetLoadFlags());
+                               registration->GetUpdateViaCache());
 
   RefPtr<UpdateJobCallback> cb = new UpdateJobCallback(aCallback);
   job->AppendResultCallback(cb);
 
   queue->ScheduleJob(job);
 }
 
 void
@@ -3205,17 +3231,17 @@ ServiceWorkerManager::UpdateInternal(nsI
 
   RefPtr<ServiceWorkerJobQueue> queue = GetOrCreateJobQueue(scopeKey, aScope);
 
   // "Invoke Update algorithm, or its equivalent, with client, registration as
   // its argument."
   RefPtr<ServiceWorkerUpdateJob> job =
     new ServiceWorkerUpdateJob(aPrincipal, registration->mScope,
                                newest->ScriptSpec(), nullptr,
-                               registration->GetLoadFlags());
+                               registration->GetUpdateViaCache());
 
   RefPtr<UpdateJobCallback> cb = new UpdateJobCallback(aCallback);
   job->AppendResultCallback(cb);
 
   queue->ScheduleJob(job);
 }
 
 namespace {
@@ -3550,33 +3576,35 @@ ServiceWorkerManager::GetRegistration(co
     return reg.forget();
   }
 
   data->mInfos.Get(aScope, getter_AddRefs(reg));
   return reg.forget();
 }
 
 already_AddRefed<ServiceWorkerRegistrationInfo>
-ServiceWorkerManager::CreateNewRegistration(const nsCString& aScope,
-                                            nsIPrincipal* aPrincipal,
-                                            nsLoadFlags aLoadFlags)
+ServiceWorkerManager::CreateNewRegistration(
+    const nsCString& aScope,
+    nsIPrincipal* aPrincipal,
+    ServiceWorkerUpdateViaCache aUpdateViaCache)
 {
 #ifdef DEBUG
   AssertIsOnMainThread();
   nsCOMPtr<nsIURI> scopeURI;
   nsresult rv = NS_NewURI(getter_AddRefs(scopeURI), aScope, nullptr, nullptr);
   MOZ_ASSERT(NS_SUCCEEDED(rv));
 
   RefPtr<ServiceWorkerRegistrationInfo> tmp =
     GetRegistration(aPrincipal, aScope);
   MOZ_ASSERT(!tmp);
 #endif
 
   RefPtr<ServiceWorkerRegistrationInfo> registration =
-    new ServiceWorkerRegistrationInfo(aScope, aPrincipal, aLoadFlags);
+    new ServiceWorkerRegistrationInfo(aScope, aPrincipal, aUpdateViaCache);
+
   // From now on ownership of registration is with
   // mServiceWorkerRegistrationInfos.
   AddScopeAndRegistration(aScope, registration);
   return registration.forget();
 }
 
 void
 ServiceWorkerManager::MaybeRemoveRegistration(ServiceWorkerRegistrationInfo* aRegistration)
diff --git a/dom/workers/ServiceWorkerManager.h b/dom/workers/ServiceWorkerManager.h
--- a/dom/workers/ServiceWorkerManager.h
+++ b/dom/workers/ServiceWorkerManager.h
@@ -196,17 +196,17 @@ public:
   RemoveAll();
 
   already_AddRefed<ServiceWorkerRegistrationInfo>
   GetRegistration(nsIPrincipal* aPrincipal, const nsACString& aScope) const;
 
   already_AddRefed<ServiceWorkerRegistrationInfo>
   CreateNewRegistration(const nsCString& aScope,
                         nsIPrincipal* aPrincipal,
-                        nsLoadFlags aLoadFlags);
+                        ServiceWorkerUpdateViaCache aUpdateViaCache);
 
   void
   RemoveRegistration(ServiceWorkerRegistrationInfo* aRegistration);
 
   void StoreRegistration(nsIPrincipal* aPrincipal,
                          ServiceWorkerRegistrationInfo* aRegistration);
 
   void
diff --git a/dom/workers/ServiceWorkerPrivate.cpp b/dom/workers/ServiceWorkerPrivate.cpp
--- a/dom/workers/ServiceWorkerPrivate.cpp
+++ b/dom/workers/ServiceWorkerPrivate.cpp
@@ -1806,17 +1806,17 @@ ServiceWorkerPrivate::SpawnWorkerIfNeede
   info.mServiceWorkerCacheName = mInfo->CacheName();
   info.mServiceWorkerID = mInfo->ID();
   info.mLoadGroup = aLoadGroup;
   info.mLoadFailedAsyncRunnable = aLoadFailedRunnable;
 
   // If we are loading a script for a ServiceWorker then we must not
   // try to intercept it.  If the interception matches the current
   // ServiceWorker's scope then we could deadlock the load.
-  info.mLoadFlags = mInfo->GetLoadFlags() |
+  info.mLoadFlags = mInfo->GetImportsLoadFlags() |
                     nsIChannel::LOAD_BYPASS_SERVICE_WORKER;
 
   rv = info.mBaseURI->GetHost(info.mDomain);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   nsCOMPtr<nsIURI> uri;
diff --git a/dom/workers/ServiceWorkerRegisterJob.cpp b/dom/workers/ServiceWorkerRegisterJob.cpp
--- a/dom/workers/ServiceWorkerRegisterJob.cpp
+++ b/dom/workers/ServiceWorkerRegisterJob.cpp
@@ -7,23 +7,24 @@
 #include "ServiceWorkerRegisterJob.h"
 
 #include "Workers.h"
 
 namespace mozilla {
 namespace dom {
 namespace workers {
 
-ServiceWorkerRegisterJob::ServiceWorkerRegisterJob(nsIPrincipal* aPrincipal,
-                                                   const nsACString& aScope,
-                                                   const nsACString& aScriptSpec,
-                                                   nsILoadGroup* aLoadGroup,
-                                                   nsLoadFlags aLoadFlags)
+ServiceWorkerRegisterJob::ServiceWorkerRegisterJob(
+    nsIPrincipal* aPrincipal,
+    const nsACString& aScope,
+    const nsACString& aScriptSpec,
+    nsILoadGroup* aLoadGroup,
+    ServiceWorkerUpdateViaCache aUpdateViaCache)
   : ServiceWorkerUpdateJob(Type::Register, aPrincipal, aScope, aScriptSpec,
-                           aLoadGroup, aLoadFlags)
+                           aLoadGroup, aUpdateViaCache)
 {
 }
 
 void
 ServiceWorkerRegisterJob::AsyncExecute()
 {
   AssertIsOnMainThread();
 
@@ -32,36 +33,36 @@ ServiceWorkerRegisterJob::AsyncExecute()
     FailUpdateJob(NS_ERROR_DOM_ABORT_ERR);
     return;
   }
 
   RefPtr<ServiceWorkerRegistrationInfo> registration =
     swm->GetRegistration(mPrincipal, mScope);
 
   if (registration) {
-    bool isSameLoadFlags = registration->GetLoadFlags() == GetLoadFlags();
-    registration->SetLoadFlags(GetLoadFlags());
+    bool sameUVC = GetUpdateViaCache() == registration->GetUpdateViaCache();
+    registration->SetUpdateViaCache(GetUpdateViaCache());
 
     // If we are resurrecting an uninstalling registration, then persist
     // it to disk again.  We preemptively removed it earlier during
     // unregister so that closing the window by shutting down the browser
     // results in the registration being gone on restart.
     if (registration->mPendingUninstall) {
       swm->StoreRegistration(mPrincipal, registration);
     }
     registration->mPendingUninstall = false;
     RefPtr<ServiceWorkerInfo> newest = registration->Newest();
-    if (newest && mScriptSpec.Equals(newest->ScriptSpec()) && isSameLoadFlags) {
+    if (newest && mScriptSpec.Equals(newest->ScriptSpec()) && sameUVC) {
       SetRegistration(registration);
       Finish(NS_OK);
       return;
     }
   } else {
     registration = swm->CreateNewRegistration(mScope, mPrincipal,
-                                              GetLoadFlags());
+                                              GetUpdateViaCache());
     if (!registration) {
       FailUpdateJob(NS_ERROR_DOM_ABORT_ERR);
       return;
     }
   }
 
   SetRegistration(registration);
   Update();
diff --git a/dom/workers/ServiceWorkerRegisterJob.h b/dom/workers/ServiceWorkerRegisterJob.h
--- a/dom/workers/ServiceWorkerRegisterJob.h
+++ b/dom/workers/ServiceWorkerRegisterJob.h
@@ -18,17 +18,17 @@ namespace workers {
 // spec algorithms.
 class ServiceWorkerRegisterJob final : public ServiceWorkerUpdateJob
 {
 public:
   ServiceWorkerRegisterJob(nsIPrincipal* aPrincipal,
                            const nsACString& aScope,
                            const nsACString& aScriptSpec,
                            nsILoadGroup* aLoadGroup,
-                           nsLoadFlags aLoadFlags);
+                           ServiceWorkerUpdateViaCache aUpdateViaCache);
 
 private:
   // Implement the Register algorithm steps and then call the parent class
   // Update() to complete the job execution.
   virtual void
   AsyncExecute() override;
 
   virtual ~ServiceWorkerRegisterJob();
diff --git a/dom/workers/ServiceWorkerRegistrationInfo.cpp b/dom/workers/ServiceWorkerRegistrationInfo.cpp
--- a/dom/workers/ServiceWorkerRegistrationInfo.cpp
+++ b/dom/workers/ServiceWorkerRegistrationInfo.cpp
@@ -72,25 +72,26 @@ ServiceWorkerRegistrationInfo::Clear()
     mActiveWorker->UpdateRedundantTime();
     mActiveWorker->WorkerPrivate()->NoteDeadServiceWorkerInfo();
     mActiveWorker = nullptr;
   }
 
   NotifyChromeRegistrationListeners();
 }
 
-ServiceWorkerRegistrationInfo::ServiceWorkerRegistrationInfo(const nsACString& aScope,
-                                                             nsIPrincipal* aPrincipal,
-                                                             nsLoadFlags aLoadFlags)
+ServiceWorkerRegistrationInfo::ServiceWorkerRegistrationInfo(
+    const nsACString& aScope,
+    nsIPrincipal* aPrincipal,
+    ServiceWorkerUpdateViaCache aUpdateViaCache)
   : mControlledDocumentsCounter(0)
   , mUpdateState(NoUpdate)
   , mCreationTime(PR_Now())
   , mCreationTimeStamp(TimeStamp::Now())
   , mLastUpdateTime(0)
-  , mLoadFlags(aLoadFlags)
+  , mUpdateViaCache(aUpdateViaCache)
   , mScope(aScope)
   , mPrincipal(aPrincipal)
   , mPendingUninstall(false)
 {}
 
 ServiceWorkerRegistrationInfo::~ServiceWorkerRegistrationInfo()
 {
   if (IsControllingDocuments()) {
@@ -632,26 +633,27 @@ ServiceWorkerRegistrationInfo::Transitio
 }
 
 bool
 ServiceWorkerRegistrationInfo::IsIdle() const
 {
   return !mActiveWorker || mActiveWorker->WorkerPrivate()->IsIdle();
 }
 
-nsLoadFlags
-ServiceWorkerRegistrationInfo::GetLoadFlags() const
+ServiceWorkerUpdateViaCache
+ServiceWorkerRegistrationInfo::GetUpdateViaCache() const
 {
-  return mLoadFlags;
+  return mUpdateViaCache;
 }
 
 void
-ServiceWorkerRegistrationInfo::SetLoadFlags(nsLoadFlags aLoadFlags)
+ServiceWorkerRegistrationInfo::SetUpdateViaCache(
+    ServiceWorkerUpdateViaCache aUpdateViaCache)
 {
-  mLoadFlags = aLoadFlags;
+  mUpdateViaCache = aUpdateViaCache;
 }
 
 int64_t
 ServiceWorkerRegistrationInfo::GetLastUpdateTime() const
 {
   return mLastUpdateTime;
 }
 
diff --git a/dom/workers/ServiceWorkerRegistrationInfo.h b/dom/workers/ServiceWorkerRegistrationInfo.h
--- a/dom/workers/ServiceWorkerRegistrationInfo.h
+++ b/dom/workers/ServiceWorkerRegistrationInfo.h
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_workers_serviceworkerregistrationinfo_h
 #define mozilla_dom_workers_serviceworkerregistrationinfo_h
 
 #include "mozilla/dom/workers/ServiceWorkerInfo.h"
+#include "mozilla/dom/ServiceWorkerRegistrationBinding.h"
 
 namespace mozilla {
 namespace dom {
 namespace workers {
 
 class ServiceWorkerRegistrationInfo final
   : public nsIServiceWorkerRegistrationInfo
 {
@@ -26,17 +27,17 @@ class ServiceWorkerRegistrationInfo fina
   } mUpdateState;
 
   // Timestamp to track SWR's last update time
   PRTime mCreationTime;
   TimeStamp mCreationTimeStamp;
   // The time of update is 0, if SWR've never been updated yet.
   PRTime mLastUpdateTime;
 
-  nsLoadFlags mLoadFlags;
+  ServiceWorkerUpdateViaCache mUpdateViaCache;
 
   RefPtr<ServiceWorkerInfo> mEvaluatingWorker;
   RefPtr<ServiceWorkerInfo> mActiveWorker;
   RefPtr<ServiceWorkerInfo> mWaitingWorker;
   RefPtr<ServiceWorkerInfo> mInstallingWorker;
 
   virtual ~ServiceWorkerRegistrationInfo();
 
@@ -52,17 +53,17 @@ public:
 
   // When unregister() is called on a registration, it is not immediately
   // removed since documents may be controlled. It is marked as
   // pendingUninstall and when all controlling documents go away, removed.
   bool mPendingUninstall;
 
   ServiceWorkerRegistrationInfo(const nsACString& aScope,
                                 nsIPrincipal* aPrincipal,
-                                nsLoadFlags aLoadFlags);
+                                ServiceWorkerUpdateViaCache aUpdateViaCache);
 
   already_AddRefed<ServiceWorkerInfo>
   Newest() const
   {
     RefPtr<ServiceWorkerInfo> newest;
     if (mInstallingWorker) {
       newest = mInstallingWorker;
     } else if (mWaitingWorker) {
@@ -178,21 +179,21 @@ public:
   // worker is updated to the Activating state.
   void
   TransitionWaitingToActive();
 
   // Determine if the registration is actively performing work.
   bool
   IsIdle() const;
 
-  nsLoadFlags
-  GetLoadFlags() const;
+  ServiceWorkerUpdateViaCache
+  GetUpdateViaCache() const;
 
   void
-  SetLoadFlags(nsLoadFlags aLoadFlags);
+  SetUpdateViaCache(ServiceWorkerUpdateViaCache aUpdateViaCache);
 
   int64_t
   GetLastUpdateTime() const;
 
   void
   SetLastUpdateTime(const int64_t aTime);
 
 private:
diff --git a/dom/workers/ServiceWorkerScriptCache.cpp b/dom/workers/ServiceWorkerScriptCache.cpp
--- a/dom/workers/ServiceWorkerScriptCache.cpp
+++ b/dom/workers/ServiceWorkerScriptCache.cpp
@@ -673,17 +673,24 @@ CompareNetwork::Initialize(nsIPrincipal*
 
   nsCOMPtr<nsILoadGroup> loadGroup;
   rv = NS_NewLoadGroup(getter_AddRefs(loadGroup), aPrincipal);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   // Update LoadFlags for propagating to ServiceWorkerInfo.
-  mLoadFlags |= mRegistration->GetLoadFlags();
+  mLoadFlags = nsIChannel::LOAD_BYPASS_SERVICE_WORKER;
+
+  ServiceWorkerUpdateViaCache uvc = mRegistration->GetUpdateViaCache();
+  if (uvc == ServiceWorkerUpdateViaCache::None ||
+      (uvc == ServiceWorkerUpdateViaCache::Imports && mIsMainScript)) {
+    mLoadFlags |= nsIRequest::VALIDATE_ALWAYS;
+  }
+
   if (mRegistration->IsLastUpdateCheckTimeOverOneDay()) {
     mLoadFlags |= nsIRequest::LOAD_BYPASS_CACHE;
   }
 
   // Different settings are needed for fetching imported scripts, since they
   // might be cross-origin scripts.
   uint32_t secFlags =
       mIsMainScript ? nsILoadInfo::SEC_REQUIRE_SAME_ORIGIN_DATA_IS_BLOCKED
diff --git a/dom/workers/ServiceWorkerUpdateJob.cpp b/dom/workers/ServiceWorkerUpdateJob.cpp
--- a/dom/workers/ServiceWorkerUpdateJob.cpp
+++ b/dom/workers/ServiceWorkerUpdateJob.cpp
@@ -164,44 +164,46 @@ public:
   {
     AssertIsOnMainThread();
     mJob->ContinueAfterInstallEvent(mSuccess);
     mJob = nullptr;
     return NS_OK;
   }
 };
 
-ServiceWorkerUpdateJob::ServiceWorkerUpdateJob(nsIPrincipal* aPrincipal,
-                                               const nsACString& aScope,
-                                               const nsACString& aScriptSpec,
-                                               nsILoadGroup* aLoadGroup,
-                                               nsLoadFlags aLoadFlags)
+ServiceWorkerUpdateJob::ServiceWorkerUpdateJob(
+    nsIPrincipal* aPrincipal,
+    const nsACString& aScope,
+    const nsACString& aScriptSpec,
+    nsILoadGroup* aLoadGroup,
+    ServiceWorkerUpdateViaCache aUpdateViaCache)
   : ServiceWorkerJob(Type::Update, aPrincipal, aScope, aScriptSpec)
   , mLoadGroup(aLoadGroup)
-  , mLoadFlags(aLoadFlags)
+  , mUpdateViaCache(aUpdateViaCache)
 {
 }
 
 already_AddRefed<ServiceWorkerRegistrationInfo>
 ServiceWorkerUpdateJob::GetRegistration() const
 {
   AssertIsOnMainThread();
   RefPtr<ServiceWorkerRegistrationInfo> ref = mRegistration;
   return ref.forget();
 }
 
-ServiceWorkerUpdateJob::ServiceWorkerUpdateJob(Type aType,
-                                               nsIPrincipal* aPrincipal,
-                                               const nsACString& aScope,
-                                               const nsACString& aScriptSpec,
-                                               nsILoadGroup* aLoadGroup,
-                                               nsLoadFlags aLoadFlags)
+ServiceWorkerUpdateJob::ServiceWorkerUpdateJob(
+    Type aType,
+    nsIPrincipal* aPrincipal,
+    const nsACString& aScope,
+    const nsACString& aScriptSpec,
+    nsILoadGroup* aLoadGroup,
+    ServiceWorkerUpdateViaCache aUpdateViaCache)
   : ServiceWorkerJob(aType, aPrincipal, aScope, aScriptSpec)
   , mLoadGroup(aLoadGroup)
-  , mLoadFlags(aLoadFlags)
+  , mUpdateViaCache(aUpdateViaCache)
 {
 }
 
 ServiceWorkerUpdateJob::~ServiceWorkerUpdateJob()
 {
 }
 
 void
@@ -322,20 +324,20 @@ ServiceWorkerUpdateJob::Update()
                                       NS_ConvertUTF8toUTF16(mScriptSpec),
                                       callback, mLoadGroup);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     FailUpdateJob(rv);
     return;
   }
 }
 
-nsLoadFlags
-ServiceWorkerUpdateJob::GetLoadFlags() const
+ServiceWorkerUpdateViaCache
+ServiceWorkerUpdateJob::GetUpdateViaCache() const
 {
-  return mLoadFlags;
+  return mUpdateViaCache;
 }
 
 void
 ServiceWorkerUpdateJob::ComparisonResult(nsresult aStatus,
                                          bool aInCacheAndEqual,
                                          const nsAString& aNewCacheName,
                                          const nsACString& aMaxScope,
                                          nsLoadFlags aLoadFlags)
@@ -376,18 +378,16 @@ ServiceWorkerUpdateJob::ComparisonResult
     rv = NS_NewURI(getter_AddRefs(maxScopeURI), aMaxScope,
                    nullptr, scriptURI);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       FailUpdateJob(NS_ERROR_DOM_SECURITY_ERR);
       return;
     }
   }
 
-  mLoadFlags = aLoadFlags;
-
   nsAutoCString defaultAllowedPrefix;
   rv = GetRequiredScopeStringPrefix(scriptURI, defaultAllowedPrefix,
                                     eUseDirectory);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     FailUpdateJob(NS_ERROR_DOM_SECURITY_ERR);
     return;
   }
 
@@ -424,23 +424,27 @@ ServiceWorkerUpdateJob::ComparisonResult
   if (aInCacheAndEqual) {
     Finish(NS_OK);
     return;
   }
 
   Telemetry::Accumulate(Telemetry::SERVICE_WORKER_UPDATED, 1);
 
   // Begin step 7 of the Update algorithm to evaluate the new script.
+  nsLoadFlags flags = aLoadFlags;
+  if (GetUpdateViaCache() == ServiceWorkerUpdateViaCache::None) {
+    flags |= nsIRequest::VALIDATE_ALWAYS;
+  }
 
   RefPtr<ServiceWorkerInfo> sw =
     new ServiceWorkerInfo(mRegistration->mPrincipal,
                           mRegistration->mScope,
                           mScriptSpec,
                           aNewCacheName,
-                          mLoadFlags);
+                          flags);
 
   mRegistration->SetEvaluating(sw);
 
   nsMainThreadPtrHandle<ServiceWorkerUpdateJob> handle(
       new nsMainThreadPtrHolder<ServiceWorkerUpdateJob>(
         "ServiceWorkerUpdateJob", this));
   RefPtr<LifeCycleEventCallback> callback = new ContinueUpdateRunnable(handle);
 
diff --git a/dom/workers/ServiceWorkerUpdateJob.h b/dom/workers/ServiceWorkerUpdateJob.h
--- a/dom/workers/ServiceWorkerUpdateJob.h
+++ b/dom/workers/ServiceWorkerUpdateJob.h
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_workers_serviceworkerupdatejob_h
 #define mozilla_dom_workers_serviceworkerupdatejob_h
 
 #include "ServiceWorkerJob.h"
+#include "ServiceWorkerRegistration.h"
 
 namespace mozilla {
 namespace dom {
 namespace workers {
 
 class ServiceWorkerManager;
 
 // A job class that performs the Update and Install algorithms from the
@@ -23,29 +24,29 @@ class ServiceWorkerManager;
 class ServiceWorkerUpdateJob : public ServiceWorkerJob
 {
 public:
   // Construct an update job to be used only for updates.
   ServiceWorkerUpdateJob(nsIPrincipal* aPrincipal,
                          const nsACString& aScope,
                          const nsACString& aScriptSpec,
                          nsILoadGroup* aLoadGroup,
-                         nsLoadFlags aLoadFlags);
+                         ServiceWorkerUpdateViaCache aUpdateViaCache);
 
   already_AddRefed<ServiceWorkerRegistrationInfo>
   GetRegistration() const;
 
 protected:
   // Construct an update job that is overriden as another job type.
   ServiceWorkerUpdateJob(Type aType,
                          nsIPrincipal* aPrincipal,
                          const nsACString& aScope,
                          const nsACString& aScriptSpec,
                          nsILoadGroup* aLoadGroup,
-                         nsLoadFlags aLoadFlags);
+                         ServiceWorkerUpdateViaCache aUpdateViaCache);
 
   virtual ~ServiceWorkerUpdateJob();
 
   // FailUpdateJob() must be called if an update job needs Finish() with
   // an error.
   void
   FailUpdateJob(ErrorResult& aRv);
 
@@ -65,18 +66,18 @@ protected:
 
   // Execute the bulk of the update job logic using the registration defined
   // by a previous SetRegistration() call.  This can be called by the overriden
   // AsyncExecute() to complete the job.  The Update() method will always call
   // Finish().  This method corresponds to the spec Update algorithm.
   void
   Update();
 
-  nsLoadFlags
-  GetLoadFlags() const;
+  ServiceWorkerUpdateViaCache
+  GetUpdateViaCache() const;
 
 private:
   class CompareCallback;
   class ContinueUpdateRunnable;
   class ContinueInstallRunnable;
 
   // Utility method called after a script is loaded and compared to
   // our current cached script.
@@ -95,17 +96,17 @@ private:
   void
   Install(ServiceWorkerManager* aSWM);
 
   // Utility method called after the install event is handled.
   void
   ContinueAfterInstallEvent(bool aInstallEventSuccess);
 
   nsCOMPtr<nsILoadGroup> mLoadGroup;
-  nsLoadFlags mLoadFlags;
+  ServiceWorkerUpdateViaCache mUpdateViaCache;
 
   RefPtr<ServiceWorkerRegistrationInfo> mRegistration;
 };
 
 } // namespace workers
 } // namespace dom
 } // namespace mozilla
 
