# HG changeset patch
# User Johann Hofmann <jhofmann@mozilla.com>
# Date 1511358469 -3600
# Node ID 07ca590ac6691ca821ddecfd6a1ac1fb67c8b39b
# Parent  bb24f5fa22ebb267cfd4fe14e2f3ee4a783f4b56
Bug 1374574 - Remove the FlyWeb system add-on from Firefox. r=mconley

MozReview-Commit-ID: BWqZIGz5hQx

diff --git a/browser/extensions/flyweb/bootstrap.js b/browser/extensions/flyweb/bootstrap.js
deleted file mode 100644
--- a/browser/extensions/flyweb/bootstrap.js
+++ /dev/null
@@ -1,295 +0,0 @@
-/* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-Cu.import("resource://gre/modules/XPCOMUtils.jsm");
-
-XPCOMUtils.defineLazyModuleGetter(this, "CustomizableUI",
-                                  "resource:///modules/CustomizableUI.jsm");
-XPCOMUtils.defineLazyModuleGetter(this, "Console",
-                                  "resource://gre/modules/Console.jsm");
-XPCOMUtils.defineLazyModuleGetter(this, "Services",
-                                  "resource://gre/modules/Services.jsm");
-XPCOMUtils.defineLazyModuleGetter(this, "Integration",
-                                  "resource://gre/modules/Integration.jsm");
-XPCOMUtils.defineLazyModuleGetter(this, "PermissionUI",
-                                  "resource:///modules/PermissionUI.jsm");
-
-XPCOMUtils.defineLazyGetter(this, "gFlyWebBundle", function() {
-  const tns = {
-    "flyweb-button.label": "FlyWeb",
-    "flyweb-button.tooltiptext": "Discover nearby FlyWeb services",
-    "flyweb-items-empty": "There are no FlyWeb services currently nearby"
-  };
-  return {
-    GetStringFromName(name) {
-      return tns[name];
-    }
-  };
-});
-
-const FLYWEB_ENABLED_PREF = "dom.flyweb.enabled";
-
-function install(aData, aReason) {}
-
-function uninstall(aData, aReason) {}
-
-function startup(aData, aReason) {
-  // Observe pref changes and enable/disable as necessary.
-  Services.prefs.addObserver(FLYWEB_ENABLED_PREF, prefObserver);
-
-  // Only initialize if pref is enabled.
-  let enabled = Services.prefs.getBoolPref(FLYWEB_ENABLED_PREF);
-  if (enabled) {
-    FlyWebView.init();
-  }
-}
-
-function shutdown(aData, aReason) {
-  Services.prefs.removeObserver(FLYWEB_ENABLED_PREF, prefObserver);
-
-  let enabled = Services.prefs.getBoolPref(FLYWEB_ENABLED_PREF);
-  if (enabled) {
-    FlyWebView.uninit();
-  }
-}
-
-// use enabled pref as a way for tests (e.g. test_contextmenu.html) to disable
-// the addon when running.
-function prefObserver(aSubject, aTopic, aData) {
-  let enabled = Services.prefs.getBoolPref(FLYWEB_ENABLED_PREF);
-  if (enabled) {
-    FlyWebView.init();
-  } else {
-    FlyWebView.uninit();
-  }
-}
-
-let gDiscoveryManagerInstance;
-
-class DiscoveryManager {
-  constructor(aWindow) {
-    this._discoveryManager = new aWindow.FlyWebDiscoveryManager();
-  }
-
-  destroy() {
-    if (this._id) {
-      this.stop();
-    }
-
-    this._discoveryManager = null;
-  }
-
-  start(callback) {
-    if (!this._id) {
-      this._id = this._discoveryManager.startDiscovery(this);
-    }
-
-    this._callback = callback;
-  }
-
-  stop() {
-    this._discoveryManager.stopDiscovery(this._id);
-
-    this._id = null;
-  }
-
-  pairWith(serviceId, callback) {
-    this._discoveryManager.pairWithService(serviceId, {
-      pairingSucceeded(service) {
-        callback(service);
-      },
-
-      pairingFailed(error) {
-        console.error("FlyWeb failed to pair with service " + serviceId, error);
-      }
-    });
-  }
-
-  onDiscoveredServicesChanged(services) {
-    if (!this._id || !this._callback) {
-      return;
-    }
-
-    this._callback(services);
-  }
-}
-
-const FlyWebPermissionPromptIntegration = (base) => ({
-  __proto__: base,
-  createPermissionPrompt(type, request) {
-    if (type != "flyweb-publish-server") {
-      return super.createPermissionPrompt(...arguments);
-    }
-
-    return {
-      __proto__: PermissionUI.PermissionPromptForRequestPrototype,
-      get request() {
-        return request;
-      },
-      get permissionKey() {
-        return "flyweb-publish-server";
-      },
-      get popupOptions() {
-        return {
-          learnMoreURL: "https://flyweb.github.io",
-          popupIconURL: "chrome://flyweb/skin/icon-64.png",
-        };
-      },
-      get notificationID() {
-        return "flyweb-publish-server";
-      },
-      get anchorID() {
-        const kAnchorID = "flyweb-publish-server-notification-icon";
-        let chromeDoc = this.browser.ownerDocument;
-        let anchor = chromeDoc.getElementById(kAnchorID);
-        if (!anchor) {
-          let notificationPopupBox =
-            chromeDoc.getElementById("notification-popup-box");
-          let notificationIcon = chromeDoc.createElement("image");
-          notificationIcon.id = kAnchorID;
-          notificationIcon.setAttribute("src",
-                                        "chrome://flyweb/skin/icon-64.png");
-          notificationIcon.classList.add("notification-anchor-icon");
-          notificationIcon.setAttribute("role", "button");
-          notificationIcon.setAttribute("aria-label",
-                                        "View the publish-server request");
-          notificationIcon.style.filter =
-            "url('chrome://global/skin/filters.svg#fill')";
-          notificationIcon.style.fill = "currentcolor";
-          notificationIcon.style.opacity = "0.4";
-          notificationPopupBox.appendChild(notificationIcon);
-        }
-
-        return kAnchorID;
-      },
-      get message() {
-        return "Would you like to let this site start a server accessible " +
-               "to nearby devices and people?";
-      },
-      get promptActions() {
-        return [{
-          label: "Allow Server",
-          accessKey: "A",
-          action: Ci.nsIPermissionManager.ALLOW_ACTION,
-          expireType:  Ci.nsIPermissionManager.EXPIRE_SESSION,
-        }, {
-          label: "Block Server",
-          accessKey: "B",
-          action: Ci.nsIPermissionManager.DENY_ACTION,
-          expireType: Ci.nsIPermissionManager.EXPIRE_SESSION,
-        }];
-      },
-    };
-  },
-});
-
-let FlyWebView = {
-  init() {
-    // Create widget and add it to the menu panel.
-    CustomizableUI.createWidget({
-      id: "flyweb-button",
-      type: "view",
-      viewId: "flyweb-panel",
-      label: gFlyWebBundle.GetStringFromName("flyweb-button.label"),
-      tooltiptext: gFlyWebBundle.GetStringFromName("flyweb-button.tooltiptext"),
-
-      onBeforeCreated(aDocument) {
-        let panel = aDocument.createElement("panelview");
-        panel.id = "flyweb-panel";
-        panel.setAttribute("class", "PanelUI-subView");
-        panel.setAttribute("flex", "1");
-
-        let label = aDocument.createElement("label");
-        label.setAttribute("class", "panel-subview-header");
-        label.setAttribute("value", gFlyWebBundle.GetStringFromName("flyweb-button.label"));
-
-        let empty = aDocument.createElement("description");
-        empty.id = "flyweb-items-empty";
-        empty.setAttribute("mousethrough", "always");
-        empty.textContent = gFlyWebBundle.GetStringFromName("flyweb-items-empty");
-
-        let items = aDocument.createElement("vbox");
-        items.id = "flyweb-items";
-        items.setAttribute("class", "panel-subview-body");
-
-        panel.appendChild(label);
-        panel.appendChild(empty);
-        panel.appendChild(items);
-
-        panel.addEventListener("command", this);
-
-        aDocument.getElementById("PanelUI-multiView").appendChild(panel);
-
-        this._sheetURI = Services.io.newURI("chrome://flyweb/skin/flyweb.css");
-        aDocument.defaultView.QueryInterface(Ci.nsIInterfaceRequestor).
-            getInterface(Ci.nsIDOMWindowUtils).loadSheet(this._sheetURI, 1);
-      },
-
-      onDestroyed(aDocument) {
-        aDocument.defaultView.QueryInterface(Ci.nsIInterfaceRequestor).
-            getInterface(Ci.nsIDOMWindowUtils).removeSheet(this._sheetURI, 1);
-      },
-
-      onViewShowing(aEvent) {
-        let doc = aEvent.target.ownerDocument;
-
-        let items = doc.getElementById("flyweb-items");
-        let empty = doc.getElementById("flyweb-items-empty");
-
-        if (!gDiscoveryManagerInstance) {
-          gDiscoveryManagerInstance = new DiscoveryManager(doc.defaultView);
-        }
-
-        gDiscoveryManagerInstance.start((services) => {
-          while (items.firstChild) {
-            items.firstChild.remove();
-          }
-
-          let fragment = doc.createDocumentFragment();
-
-          for (let service of services) {
-            let button = doc.createElement("toolbarbutton");
-            button.setAttribute("class", "subviewbutton cui-withicon");
-            button.setAttribute("label", service.displayName);
-            button.setAttribute("data-service-id", service.serviceId);
-            fragment.appendChild(button);
-          }
-
-          items.appendChild(fragment);
-
-          empty.hidden = services.length > 0;
-        });
-      },
-
-      onViewHiding(aEvent) {
-        gDiscoveryManagerInstance.stop();
-      },
-
-      handleEvent(aEvent) {
-        if (aEvent.type === "command") {
-          let serviceId = aEvent.target.getAttribute("data-service-id");
-          gDiscoveryManagerInstance.pairWith(serviceId, (service) => {
-            aEvent.view.openUILinkIn(service.uiUrl, "tab");
-          });
-        }
-      }
-    });
-
-    Integration.contentPermission
-               .register(FlyWebPermissionPromptIntegration);
-  },
-
-  uninit() {
-    CustomizableUI.destroyWidget("flyweb-button");
-
-    if (gDiscoveryManagerInstance) {
-      gDiscoveryManagerInstance.destroy();
-      gDiscoveryManagerInstance = null;
-    }
-
-    Integration.contentPermission
-               .unregister(FlyWebPermissionPromptIntegration);
-  }
-};
diff --git a/browser/extensions/flyweb/install.rdf.in b/browser/extensions/flyweb/install.rdf.in
deleted file mode 100644
--- a/browser/extensions/flyweb/install.rdf.in
+++ /dev/null
@@ -1,32 +0,0 @@
-<?xml version="1.0"?>
-<!-- This Source Code Form is subject to the terms of the Mozilla Public
-   - License, v. 2.0. If a copy of the MPL was not distributed with this
-   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
-
-#filter substitution
-
-<RDF xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
-     xmlns:em="http://www.mozilla.org/2004/em-rdf#">
-
-  <Description about="urn:mozilla:install-manifest">
-    <em:id>flyweb@mozilla.org</em:id>
-    <em:version>1.0.0</em:version>
-    <em:type>2</em:type>
-    <em:bootstrap>true</em:bootstrap>
-    <em:multiprocessCompatible>true</em:multiprocessCompatible>
-
-    <!-- Target Application this theme can install into,
-        with minimum and maximum supported versions. -->
-    <em:targetApplication>
-      <Description>
-        <em:id>{ec8030f7-c20a-464f-9b0e-13a3a9e97384}</em:id>
-        <em:minVersion>@MOZ_APP_VERSION@</em:minVersion>
-        <em:maxVersion>@MOZ_APP_MAXVERSION@</em:maxVersion>
-      </Description>
-    </em:targetApplication>
-
-    <!-- Front End MetaData -->
-    <em:name>FlyWeb</em:name>
-    <em:description>Discover nearby services in the browser</em:description>
-  </Description>
-</RDF>
diff --git a/browser/extensions/flyweb/jar.mn b/browser/extensions/flyweb/jar.mn
deleted file mode 100644
--- a/browser/extensions/flyweb/jar.mn
+++ /dev/null
@@ -1,10 +0,0 @@
-# This Source Code Form is subject to the terms of the Mozilla Public
-# License, v. 2.0. If a copy of the MPL was not distributed with this
-# file, You can obtain one at http://mozilla.org/MPL/2.0/.
-
-[features/flyweb@mozilla.org] chrome.jar:
-% skin flyweb classic/1.0 %skin/linux/ os=LikeUnix
-% skin flyweb classic/1.0 %skin/osx/ os=Darwin
-% skin flyweb classic/1.0 %skin/windows/ os=WINNT
-% skin flyweb-shared classic/1.0 %skin/shared/
-  skin/  (skin/*)
diff --git a/browser/extensions/flyweb/moz.build b/browser/extensions/flyweb/moz.build
deleted file mode 100644
--- a/browser/extensions/flyweb/moz.build
+++ /dev/null
@@ -1,18 +0,0 @@
-# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
-# vim: set filetype=python:
-# This Source Code Form is subject to the terms of the Mozilla Public
-# License, v. 2.0. If a copy of the MPL was not distributed with this
-# file, You can obtain one at http://mozilla.org/MPL/2.0/.
-
-DEFINES['MOZ_APP_VERSION'] = CONFIG['MOZ_APP_VERSION']
-DEFINES['MOZ_APP_MAXVERSION'] = CONFIG['MOZ_APP_MAXVERSION']
-
-FINAL_TARGET_FILES.features['flyweb@mozilla.org'] += [
-  'bootstrap.js'
-]
-
-FINAL_TARGET_PP_FILES.features['flyweb@mozilla.org'] += [
-  'install.rdf.in'
-]
-
-JAR_MANIFESTS += ['jar.mn']
diff --git a/browser/extensions/flyweb/skin/flyweb-icon.svg b/browser/extensions/flyweb/skin/flyweb-icon.svg
deleted file mode 100644
--- a/browser/extensions/flyweb/skin/flyweb-icon.svg
+++ /dev/null
@@ -1,39 +0,0 @@
-<?xml version="1.0" encoding="utf-8"?>
-<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
-<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
-  width="64px" height="64px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve">
-<circle fill="#797C80" cx="32" cy="52" r="6"/>
-<g>
- <path fill="#797C80" d="M6.894,15.255c-2.254,1.547-4.386,3.304-6.361,5.279L0.18,20.887l3.536,3.536l0.354-0.354
-   c1.621-1.62,3.363-3.072,5.196-4.369C8.126,18.464,7.296,16.943,6.894,15.255z"/>
- <path fill="#797C80" d="M63.465,20.532C55.061,12.128,43.887,7.5,32,7.5c-2.265,0-4.504,0.17-6.703,0.501
-   c0.822,1.44,1.3,3.1,1.312,4.87C28.382,12.631,30.181,12.5,32,12.5c10.55,0,20.468,4.108,27.928,11.567l0.354,0.354l3.537-3.535
-   L63.465,20.532z"/>
-</g>
-<g>
- <path fill="#797C80" d="M16.613,10.94c1.103,0,2,0.897,2,2s-0.897,2-2,2s-2-0.897-2-2S15.51,10.94,16.613,10.94 M16.613,6.94
-   c-3.313,0-6,2.687-6,6s2.687,6,6,6s6-2.687,6-6S19.926,6.94,16.613,6.94L16.613,6.94z"/>
-</g>
-<g>
- <path fill="#797C80" d="M46.492,37.502c-1.853-1.852-4.002-3.292-6.334-4.305c0.031,0.324,0.05,0.652,0.05,0.984
-   c0,1.477-0.33,2.874-0.906,4.137c1.329,0.712,2.561,1.623,3.657,2.719l0.354,0.354l3.533-3.535L46.492,37.502z"/>
- <path fill="#797C80" d="M20.262,35.207c-0.972,0.683-1.9,1.439-2.758,2.297l-0.354,0.354l3.536,3.537l0.354-0.354
-   c0.35-0.35,0.715-0.679,1.091-0.99C21.118,38.66,20.446,37.007,20.262,35.207z"/>
-</g>
-<g>
- <path fill="#797C80" d="M30.209,32.182c1.102,0,1.999,0.897,1.999,2s-0.896,2-1.999,2c-1.103,0-2-0.897-2-2
-   S29.106,32.182,30.209,32.182 M30.209,28.182c-3.313,0-6,2.686-6,6c0,3.312,2.687,6,6,6c3.313,0,5.999-2.688,5.999-6
-   C36.208,30.867,33.522,28.182,30.209,28.182L30.209,28.182z"/>
-</g>
-<g>
- <path fill="#797C80" d="M32.207,23.716c0-1.497,0.34-2.912,0.932-4.188C32.76,19.515,32.381,19.5,32,19.5
-   c-8.681,0-16.843,3.381-22.981,9.52l-0.354,0.354l3.535,3.535l0.354-0.354C17.748,27.36,24.654,24.5,32,24.5
-   c0.083,0,0.165,0.005,0.247,0.006C32.227,24.245,32.207,23.982,32.207,23.716z"/>
- <path fill="#797C80" d="M54.98,29.018c-0.987-0.987-2.033-1.896-3.119-2.738c-0.447,1.68-1.313,3.188-2.491,4.399
-   c0.717,0.586,1.409,1.21,2.073,1.874l0.354,0.354l3.537-3.535L54.98,29.018z"/>
-</g>
-<g>
- <path fill="#797C80" d="M42.207,21.716c1.103,0,2,0.897,2,2s-0.897,2-2,2s-2-0.897-2-2S41.104,21.716,42.207,21.716 M42.207,17.716
-   c-3.313,0-6,2.687-6,6s2.687,6,6,6s6-2.687,6-6S45.521,17.716,42.207,17.716L42.207,17.716z"/>
-</g>
-</svg>
diff --git a/browser/extensions/flyweb/skin/linux/flyweb.css b/browser/extensions/flyweb/skin/linux/flyweb.css
deleted file mode 100644
--- a/browser/extensions/flyweb/skin/linux/flyweb.css
+++ /dev/null
@@ -1,5 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-@import url("chrome://flyweb-shared/skin/flyweb.css");
diff --git a/browser/extensions/flyweb/skin/linux/icon-16.png b/browser/extensions/flyweb/skin/linux/icon-16.png
deleted file mode 100644
index 3a6c2e43a8c2e0fff5ecf72325e62dfae6812a7d..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/linux/icon-32-anchored.png b/browser/extensions/flyweb/skin/linux/icon-32-anchored.png
deleted file mode 100644
index b05590ddace409c581d697b322551fb9e5c088b3..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/linux/icon-32.png b/browser/extensions/flyweb/skin/linux/icon-32.png
deleted file mode 100644
index a04ec20b9968f149945615673fc8f19930777d7c..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/linux/icon-64-anchored.png b/browser/extensions/flyweb/skin/linux/icon-64-anchored.png
deleted file mode 100644
index b617b92086d7ea122d76ba1c842f53a42a9fa60a..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/linux/icon-64.png b/browser/extensions/flyweb/skin/linux/icon-64.png
deleted file mode 100644
index be8ece467b7b1df0683febca9253079af55abad2..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/osx/flyweb.css b/browser/extensions/flyweb/skin/osx/flyweb.css
deleted file mode 100644
--- a/browser/extensions/flyweb/skin/osx/flyweb.css
+++ /dev/null
@@ -1,5 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-@import url("chrome://flyweb-shared/skin/flyweb.css");
diff --git a/browser/extensions/flyweb/skin/osx/icon-16.png b/browser/extensions/flyweb/skin/osx/icon-16.png
deleted file mode 100644
index 7c87435a48276a3688c9d59e17bd32c61f587753..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/osx/icon-32-anchored.png b/browser/extensions/flyweb/skin/osx/icon-32-anchored.png
deleted file mode 100644
index b05590ddace409c581d697b322551fb9e5c088b3..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/osx/icon-32.png b/browser/extensions/flyweb/skin/osx/icon-32.png
deleted file mode 100644
index 5b0140ffad3331edd354da07497f6023b7a33c0e..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/osx/icon-64-anchored.png b/browser/extensions/flyweb/skin/osx/icon-64-anchored.png
deleted file mode 100644
index b617b92086d7ea122d76ba1c842f53a42a9fa60a..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/osx/icon-64.png b/browser/extensions/flyweb/skin/osx/icon-64.png
deleted file mode 100644
index eb67c29ec1330ff97c1063bff574e09866b59137..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/shared/flyweb.css b/browser/extensions/flyweb/skin/shared/flyweb.css
deleted file mode 100644
--- a/browser/extensions/flyweb/skin/shared/flyweb.css
+++ /dev/null
@@ -1,54 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#flyweb-panel {
-  width: 20em;
-}
-
-#flyweb-items-empty {
-  box-sizing: border-box;
-  color: GrayText;
-  padding: 10px 20px;
-  text-align: center;
-}
-
-#flyweb-button {
-  list-style-image: url("chrome://flyweb/skin/icon-16.png");
-}
-
-#flyweb-button[cui-areatype="menu-panel"],
-toolbarpaletteitem[place="palette"] > #flyweb-button {
-  list-style-image: url("chrome://flyweb/skin/icon-32.png");
-}
-
-#flyweb-button[cui-areatype="menu-panel"][panel-multiview-anchor="true"] {
-  list-style-image: url("chrome://flyweb/skin/icon-32-anchored.png");
-}
-
-#flyweb-items > toolbarbutton {
-  list-style-image: url("chrome://flyweb/skin/icon-16.png");
-}
-
-@media (min-resolution: 2dppx) {
-  #flyweb-button {
-    list-style-image: url("chrome://flyweb/skin/icon-32.png");
-  }
-
-  #flyweb-button[cui-areatype="menu-panel"],
-  toolbarpaletteitem[place="palette"] > #flyweb-button {
-    list-style-image: url("chrome://flyweb/skin/icon-64.png");
-  }
-
-  #flyweb-button[cui-areatype="menu-panel"][panel-multiview-anchor="true"] {
-    list-style-image: url("chrome://flyweb/skin/icon-64-anchored.png");
-  }
-
-  #flyweb-items > toolbarbutton {
-    list-style-image: url("chrome://flyweb/skin/icon-32.png");
-  }
-
-  #flyweb-items > toolbarbutton > .toolbarbutton-icon {
-    width: 16px;
-  }
-}
diff --git a/browser/extensions/flyweb/skin/windows/flyweb.css b/browser/extensions/flyweb/skin/windows/flyweb.css
deleted file mode 100644
--- a/browser/extensions/flyweb/skin/windows/flyweb.css
+++ /dev/null
@@ -1,5 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-@import url("chrome://flyweb-shared/skin/flyweb.css");
diff --git a/browser/extensions/flyweb/skin/windows/icon-16.png b/browser/extensions/flyweb/skin/windows/icon-16.png
deleted file mode 100644
index 3a6c2e43a8c2e0fff5ecf72325e62dfae6812a7d..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/windows/icon-32-anchored.png b/browser/extensions/flyweb/skin/windows/icon-32-anchored.png
deleted file mode 100644
index b05590ddace409c581d697b322551fb9e5c088b3..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/windows/icon-32.png b/browser/extensions/flyweb/skin/windows/icon-32.png
deleted file mode 100644
index a04ec20b9968f149945615673fc8f19930777d7c..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/windows/icon-64-anchored.png b/browser/extensions/flyweb/skin/windows/icon-64-anchored.png
deleted file mode 100644
index b617b92086d7ea122d76ba1c842f53a42a9fa60a..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/flyweb/skin/windows/icon-64.png b/browser/extensions/flyweb/skin/windows/icon-64.png
deleted file mode 100644
index be8ece467b7b1df0683febca9253079af55abad2..e69de29bb2d1d6434b8b29ae775ad8c2e48c5391
GIT binary patch
literal 0
Hc$@<O00001

diff --git a/browser/extensions/moz.build b/browser/extensions/moz.build
--- a/browser/extensions/moz.build
+++ b/browser/extensions/moz.build
@@ -17,17 +17,16 @@ DIRS += [
     'screenshots',
     'shield-recipe-client',
     'webcompat',
 ]
 
 # Only include the following system add-ons if building Aurora or Nightly
 if not CONFIG['RELEASE_OR_BETA']:
     DIRS += [
-        'flyweb',
         'presentation',
     ]
 
 # Only include the following system add-ons if building DevEdition or Nightly
 if CONFIG['MOZ_DEV_EDITION'] or CONFIG['NIGHTLY_BUILD']:
     DIRS += [
         'webcompat-reporter',
     ]
diff --git a/browser/installer/allowed-dupes.mn b/browser/installer/allowed-dupes.mn
--- a/browser/installer/allowed-dupes.mn
+++ b/browser/installer/allowed-dupes.mn
@@ -117,32 +117,16 @@ browser/chrome/pdfjs/content/web/images/
 browser/chrome/pdfjs/content/web/images/findbarButton-previous-rtl@2x.png
 browser/chrome/pdfjs/content/web/images/findbarButton-previous.png
 browser/chrome/pdfjs/content/web/images/findbarButton-previous@2x.png
 browser/extensions/{972ce4c6-7e08-4474-a285-3208198ce6fd}/icon.png
 browser/features/firefox@getpocket.com/chrome/skin/linux/menuPanel.png
 browser/features/firefox@getpocket.com/chrome/skin/linux/menuPanel@2x.png
 browser/features/firefox@getpocket.com/chrome/skin/windows/menuPanel.png
 browser/features/firefox@getpocket.com/chrome/skin/windows/menuPanel@2x.png
-# flyweb reduction is bug 1313107
-browser/features/flyweb@mozilla.org/chrome/skin/linux/flyweb.css
-browser/features/flyweb@mozilla.org/chrome/skin/linux/icon-16.png
-browser/features/flyweb@mozilla.org/chrome/skin/linux/icon-32-anchored.png
-browser/features/flyweb@mozilla.org/chrome/skin/linux/icon-32.png
-browser/features/flyweb@mozilla.org/chrome/skin/linux/icon-64-anchored.png
-browser/features/flyweb@mozilla.org/chrome/skin/linux/icon-64.png
-browser/features/flyweb@mozilla.org/chrome/skin/osx/flyweb.css
-browser/features/flyweb@mozilla.org/chrome/skin/osx/icon-32-anchored.png
-browser/features/flyweb@mozilla.org/chrome/skin/osx/icon-64-anchored.png
-browser/features/flyweb@mozilla.org/chrome/skin/windows/flyweb.css
-browser/features/flyweb@mozilla.org/chrome/skin/windows/icon-16.png
-browser/features/flyweb@mozilla.org/chrome/skin/windows/icon-32-anchored.png
-browser/features/flyweb@mozilla.org/chrome/skin/windows/icon-32.png
-browser/features/flyweb@mozilla.org/chrome/skin/windows/icon-64-anchored.png
-browser/features/flyweb@mozilla.org/chrome/skin/windows/icon-64.png
 browser/icons/mozicon128.png
 chrome.manifest
 chrome/toolkit/skin/classic/global/autocomplete.css
 chrome/toolkit/skin/classic/global/button.css
 chrome/toolkit/skin/classic/global/checkbox.css
 chrome/toolkit/skin/classic/global/dialog.css
 chrome/toolkit/skin/classic/global/dropmarker.css
 chrome/toolkit/skin/classic/global/global.css
diff --git a/devtools/client/commandline/test/browser_cmd_addon.js b/devtools/client/commandline/test/browser_cmd_addon.js
--- a/devtools/client/commandline/test/browser_cmd_addon.js
+++ b/devtools/client/commandline/test/browser_cmd_addon.js
@@ -30,17 +30,17 @@ function* spawnTest() {
         input:  "addon list extension",
         hints:                      "",
         markup: "VVVVVVVVVVVVVVVVVVVV",
         status: "VALID"
       },
       exec: {
         output: [/The following/, /Mochitest/, /Special Powers/],
         notinoutput: [
-          /Web Compat/, /FlyWeb/, /Pocket/, /Multi-process staged rollout/,
+          /Web Compat/, /Pocket/, /Multi-process staged rollout/,
           /Form Autofill/, /Application Update Service Helper/, /Presentation/,
           /Shield Recipe Client/]
       }
     },
     {
       setup: "addon list locale",
       check: {
         input:  "addon list locale",
