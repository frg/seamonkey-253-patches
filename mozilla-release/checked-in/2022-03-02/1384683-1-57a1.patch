# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1502470381 -7200
# Node ID 72d456d328c7c3c9c0da55a42558ed42bbda7fc1
# Parent  4c8106b412d6a0b670aeb39fa2a7251405068855
Bug 1384683: Rename wasm::FrameIterator into wasm::WasmFrameIter; r=luke

MozReview-Commit-ID: HOpqIlAVHcA

diff --git a/js/src/moz.build b/js/src/moz.build
--- a/js/src/moz.build
+++ b/js/src/moz.build
@@ -366,17 +366,17 @@ UNIFIED_SOURCES += [
     'wasm/WasmBinaryIterator.cpp',
     'wasm/WasmBinaryToAST.cpp',
     'wasm/WasmBinaryToText.cpp',
     'wasm/WasmBuiltins.cpp',
     'wasm/WasmCode.cpp',
     'wasm/WasmCompartment.cpp',
     'wasm/WasmCompile.cpp',
     'wasm/WasmDebug.cpp',
-    'wasm/WasmFrameIterator.cpp',
+    'wasm/WasmFrameIter.cpp',
     'wasm/WasmGenerator.cpp',
     'wasm/WasmInstance.cpp',
     'wasm/WasmIonCompile.cpp',
     'wasm/WasmJS.cpp',
     'wasm/WasmModule.cpp',
     'wasm/WasmSignalHandlers.cpp',
     'wasm/WasmStubs.cpp',
     'wasm/WasmTable.cpp',
diff --git a/js/src/vm/Stack.cpp b/js/src/vm/Stack.cpp
--- a/js/src/vm/Stack.cpp
+++ b/js/src/vm/Stack.cpp
@@ -548,17 +548,17 @@ FrameIter::settleOnActivation()
             }
 
             nextJitFrame();
             data_.state_ = JIT;
             return;
         }
 
         if (activation->isWasm()) {
-            data_.wasmFrames_ = wasm::FrameIterator(data_.activations_->asWasm());
+            data_.wasmFrames_ = wasm::WasmFrameIter(data_.activations_->asWasm());
 
             if (data_.wasmFrames_.done()) {
                 ++data_.activations_;
                 continue;
             }
 
             data_.pc_ = nullptr;
             data_.state_ = WASM;
@@ -1074,17 +1074,17 @@ void
 FrameIter::wasmUpdateBytecodeOffset()
 {
     MOZ_RELEASE_ASSERT(data_.state_ == WASM, "Unexpected state");
 
     wasm::DebugFrame* frame = data_.wasmFrames_.debugFrame();
     WasmActivation* activation = data_.activations_->asWasm();
 
     // Relookup the current frame, updating the bytecode offset in the process.
-    data_.wasmFrames_ = wasm::FrameIterator(activation);
+    data_.wasmFrames_ = wasm::WasmFrameIter(activation);
     while (data_.wasmFrames_.debugFrame() != frame)
         ++data_.wasmFrames_;
 
     MOZ_ASSERT(data_.wasmFrames_.debugFrame() == frame);
 }
 
 JSFunction*
 FrameIter::calleeTemplate() const
diff --git a/js/src/vm/Stack.h b/js/src/vm/Stack.h
--- a/js/src/vm/Stack.h
+++ b/js/src/vm/Stack.h
@@ -20,17 +20,17 @@
 #include "gc/Rooting.h"
 #include "jit/JitFrameIterator.h"
 #ifdef CHECK_OSIPOINT_REGISTERS
 #include "jit/Registers.h" // for RegisterDump
 #endif
 #include "js/RootingAPI.h"
 #include "vm/ArgumentsObject.h"
 #include "vm/SavedFrame.h"
-#include "wasm/WasmFrameIterator.h"
+#include "wasm/WasmFrameIter.h"
 #include "wasm/WasmTypes.h"
 
 struct JSCompartment;
 
 namespace JS {
 namespace dbg {
 #ifdef JS_BROKEN_GCC_ATTRIBUTE_WARNING
 #pragma GCC diagnostic push
@@ -1762,17 +1762,17 @@ class WasmActivation : public Activation
     // simulator) and cleared by WasmHandleExecutionInterrupt or WasmHandleThrow
     // when the interrupt is handled.
     void startInterrupt(const JS::ProfilingFrameIterator::RegisterState& state);
     void finishInterrupt();
     bool interrupted() const;
     void* unwindPC() const;
     void* resumePC() const;
 
-    // Used by wasm::FrameIterator during stack unwinding.
+    // Used by wasm::WasmFrameIter during stack unwinding.
     void unwindExitFP(wasm::Frame* exitFP);
 };
 
 // A FrameIter walks over a context's stack of JS script activations,
 // abstracting over whether the JS scripts were running in the interpreter or
 // different modes of compiled code.
 //
 // FrameIter is parameterized by what it includes in the stack iteration:
@@ -1807,17 +1807,17 @@ class FrameIter
 
         jsbytecode *        pc_;
 
         InterpreterFrameIterator interpFrames_;
         ActivationIterator activations_;
 
         jit::JitFrameIterator jitFrames_;
         unsigned ionInlineFrameNo_;
-        wasm::FrameIterator wasmFrames_;
+        wasm::WasmFrameIter wasmFrames_;
 
         Data(JSContext* cx, DebuggerEvalOption debuggerEvalOption, JSPrincipals* principals);
         Data(JSContext* cx, const CooperatingContext& target, DebuggerEvalOption debuggerEvalOption);
         Data(const Data& other);
     };
 
     explicit FrameIter(JSContext* cx,
                        DebuggerEvalOption = FOLLOW_DEBUGGER_EVAL_PREV_LINK);
diff --git a/js/src/wasm/WasmBuiltins.cpp b/js/src/wasm/WasmBuiltins.cpp
--- a/js/src/wasm/WasmBuiltins.cpp
+++ b/js/src/wasm/WasmBuiltins.cpp
@@ -97,17 +97,17 @@ WasmHandleExecutionInterrupt()
 
 static bool
 WasmHandleDebugTrap()
 {
     WasmActivation* activation = CallingActivation();
     MOZ_ASSERT(activation);
     JSContext* cx = activation->cx();
 
-    FrameIterator iter(activation);
+    WasmFrameIter iter(activation);
     MOZ_ASSERT(iter.debugEnabled());
     const CallSite* site = iter.debugTrapCallsite();
     MOZ_ASSERT(site);
     if (site->kind() == CallSite::EnterFrame) {
         if (!iter.instance()->enterFrameTrapsEnabled())
             return true;
         DebugFrame* frame = iter.debugFrame();
         frame->setIsDebuggee();
@@ -164,28 +164,28 @@ WasmHandleDebugTrap()
 // value is the new stack address which the calling stub will set to the sp
 // register before executing a return instruction.
 static void*
 WasmHandleThrow()
 {
     WasmActivation* activation = CallingActivation();
     JSContext* cx = activation->cx();
 
-    // FrameIterator iterates down wasm frames in the activation starting at
+    // WasmFrameIter iterates down wasm frames in the activation starting at
     // WasmActivation::exitFP. Pass Unwind::True to pop WasmActivation::exitFP
-    // once each time FrameIterator is incremented, ultimately leaving exitFP
-    // null when the FrameIterator is done(). This is necessary to prevent a
+    // once each time WasmFrameIter is incremented, ultimately leaving exitFP
+    // null when the WasmFrameIter is done().  This is necessary to prevent a
     // DebugFrame from being observed again after we just called onLeaveFrame
     // (which would lead to the frame being re-added to the map of live frames,
     // right as it becomes trash).
-    FrameIterator iter(activation, FrameIterator::Unwind::True);
+    WasmFrameIter iter(activation, WasmFrameIter::Unwind::True);
     MOZ_ASSERT(!iter.done());
 
     // Live wasm code on the stack is kept alive (in wasm::TraceActivations) by
-    // marking the instance of every wasm::Frame found by FrameIterator.
+    // marking the instance of every wasm::Frame found by WasmFrameIter.
     // However, as explained above, we're popping frames while iterating which
     // means that a GC during this loop could collect the code of frames whose
     // code is still on the stack. This is actually mostly fine: as soon as we
     // return to the throw stub, the entire stack will be popped as a whole,
     // returning to the C++ caller. However, we must keep the throw stub alive
     // itself which is owned by the innermost instance.
     RootedWasmInstanceObject keepAlive(cx, iter.instance()->object());
 
diff --git a/js/src/wasm/WasmCode.h b/js/src/wasm/WasmCode.h
--- a/js/src/wasm/WasmCode.h
+++ b/js/src/wasm/WasmCode.h
@@ -29,17 +29,16 @@ struct AsmJSMetadata;
 class WasmInstanceObject;
 
 namespace wasm {
 
 struct LinkData;
 struct LinkDataTier;
 struct Metadata;
 struct MetadataTier;
-class FrameIterator;
 
 // ShareableBytes is a reference-counted Vector of bytes.
 
 struct ShareableBytes : ShareableBase<ShareableBytes>
 {
     // Vector is 'final', so instead make Vector a member and add boilerplate.
     Bytes bytes;
     size_t sizeOfExcludingThis(MallocSizeOf m) const { return bytes.sizeOfExcludingThis(m); }
diff --git a/js/src/wasm/WasmFrameIterator.cpp b/js/src/wasm/WasmFrameIter.cpp
rename from js/src/wasm/WasmFrameIterator.cpp
rename to js/src/wasm/WasmFrameIter.cpp
--- a/js/src/wasm/WasmFrameIterator.cpp
+++ b/js/src/wasm/WasmFrameIter.cpp
@@ -11,45 +11,45 @@
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
 
-#include "wasm/WasmFrameIterator.h"
+#include "wasm/WasmFrameIter.h"
 
 #include "wasm/WasmInstance.h"
 
 #include "jit/MacroAssembler-inl.h"
 
 using namespace js;
 using namespace js::jit;
 using namespace js::wasm;
 
 using mozilla::DebugOnly;
 using mozilla::Swap;
 
 /*****************************************************************************/
-// FrameIterator implementation
+// WasmFrameIter implementation
 
-FrameIterator::FrameIterator()
+WasmFrameIter::WasmFrameIter()
   : activation_(nullptr),
     code_(nullptr),
     callsite_(nullptr),
     codeRange_(nullptr),
     fp_(nullptr),
     unwind_(Unwind::False),
     unwoundAddressOfReturnAddress_(nullptr)
 {
     MOZ_ASSERT(done());
 }
 
-FrameIterator::FrameIterator(WasmActivation* activation, Unwind unwind)
+WasmFrameIter::WasmFrameIter(WasmActivation* activation, Unwind unwind)
   : activation_(activation),
     code_(nullptr),
     callsite_(nullptr),
     codeRange_(nullptr),
     fp_(activation->exitFP()),
     unwind_(unwind)
 {
     MOZ_ASSERT(fp_);
@@ -78,25 +78,25 @@ FrameIterator::FrameIterator(WasmActivat
 
     codeRange_ = code_->lookupRange(activation->unwindPC());
     MOZ_ASSERT(codeRange_->kind() == CodeRange::Function);
 
     MOZ_ASSERT(!done());
 }
 
 bool
-FrameIterator::done() const
+WasmFrameIter::done() const
 {
     MOZ_ASSERT(!!fp_ == !!code_);
     MOZ_ASSERT(!!fp_ == !!codeRange_);
     return !fp_;
 }
 
 void
-FrameIterator::operator++()
+WasmFrameIter::operator++()
 {
     MOZ_ASSERT(!done());
 
     // When the iterator is set to Unwind::True, each time the iterator pops a
     // frame, the WasmActivation is updated so that the just-popped frame
     // is no longer visible. This is necessary since Debugger::onLeaveFrame is
     // called before popping each frame and, once onLeaveFrame is called for a
     // given frame, that frame must not be visible to subsequent stack iteration
@@ -110,17 +110,17 @@ FrameIterator::operator++()
             activation_->finishInterrupt();
         activation_->unwindExitFP(fp_);
     }
 
     popFrame();
 }
 
 void
-FrameIterator::popFrame()
+WasmFrameIter::popFrame()
 {
     Frame* prevFP = fp_;
     fp_ = prevFP->callerFP;
 
     if (!fp_) {
         code_ = nullptr;
         codeRange_ = nullptr;
         callsite_ = nullptr;
@@ -144,99 +144,99 @@ FrameIterator::popFrame()
 
     callsite_ = code_->lookupCallSite(returnAddress);
     MOZ_ASSERT(callsite_);
 
     MOZ_ASSERT(!done());
 }
 
 const char*
-FrameIterator::filename() const
+WasmFrameIter::filename() const
 {
     MOZ_ASSERT(!done());
     return code_->metadata().filename.get();
 }
 
 const char16_t*
-FrameIterator::displayURL() const
+WasmFrameIter::displayURL() const
 {
     MOZ_ASSERT(!done());
     return code_->metadata().displayURL();
 }
 
 bool
-FrameIterator::mutedErrors() const
+WasmFrameIter::mutedErrors() const
 {
     MOZ_ASSERT(!done());
     return code_->metadata().mutedErrors();
 }
 
 JSAtom*
-FrameIterator::functionDisplayAtom() const
+WasmFrameIter::functionDisplayAtom() const
 {
     MOZ_ASSERT(!done());
 
     JSContext* cx = activation_->cx();
     JSAtom* atom = instance()->getFuncAtom(cx, codeRange_->funcIndex());
     if (!atom) {
         cx->clearPendingException();
         return cx->names().empty;
     }
 
     return atom;
 }
 
 unsigned
-FrameIterator::lineOrBytecode() const
+WasmFrameIter::lineOrBytecode() const
 {
     MOZ_ASSERT(!done());
     MOZ_ASSERT_IF(!callsite_, activation_->interrupted());
     return callsite_ ? callsite_->lineOrBytecode() : codeRange_->funcLineOrBytecode();
 }
 
 Instance*
-FrameIterator::instance() const
+WasmFrameIter::instance() const
 {
     MOZ_ASSERT(!done());
     return fp_->tls->instance;
 }
 
 void**
-FrameIterator::unwoundAddressOfReturnAddress() const
+WasmFrameIter::unwoundAddressOfReturnAddress() const
 {
     MOZ_ASSERT(done());
     MOZ_ASSERT(unwind_ == Unwind::True);
     MOZ_ASSERT(unwoundAddressOfReturnAddress_);
     return unwoundAddressOfReturnAddress_;
 }
 
 bool
-FrameIterator::debugEnabled() const
+WasmFrameIter::debugEnabled() const
 {
     MOZ_ASSERT(!done());
 
     // Only non-imported functions can have debug frames.
     //
     // Metadata::debugEnabled is only set if debugging is actually enabled (both
     // requested, and available via baseline compilation), and Tier::Debug code
     // will be available.
     return code_->metadata().debugEnabled &&
            codeRange_->funcIndex() >= code_->metadata(Tier::Debug).funcImports.length();
 }
 
 DebugFrame*
-FrameIterator::debugFrame() const
+WasmFrameIter::debugFrame() const
 {
     MOZ_ASSERT(!done());
     MOZ_ASSERT(debugEnabled());
     return reinterpret_cast<DebugFrame*>((uint8_t*)fp_ - DebugFrame::offsetOfFrame());
 }
 
 const CallSite*
-FrameIterator::debugTrapCallsite() const
+WasmFrameIter::debugTrapCallsite() const
 {
     MOZ_ASSERT(!done());
     MOZ_ASSERT(callsite_);
     MOZ_ASSERT(debugEnabled());
     MOZ_ASSERT(callsite_->kind() == CallSite::EnterFrame ||
                callsite_->kind() == CallSite::LeaveFrame ||
                callsite_->kind() == CallSite::Breakpoint);
     return callsite_;
@@ -1004,17 +1004,17 @@ ProfilingFrameIterator::label() const
     MOZ_CRASH("bad code range kind");
 }
 
 void
 wasm::TraceActivations(JSContext* cx, const CooperatingContext& target, JSTracer* trc)
 {
     for (ActivationIterator iter(cx, target); !iter.done(); ++iter) {
         if (iter.activation()->isWasm()) {
-            for (FrameIterator fi(iter.activation()->asWasm()); !fi.done(); ++fi)
+            for (WasmFrameIter fi(iter.activation()->asWasm()); !fi.done(); ++fi)
                 fi.instance()->trace(trc);
         }
     }
 }
 
 Instance*
 wasm::LookupFaultingInstance(WasmActivation* activation, void* pc, void* fp)
 {
diff --git a/js/src/wasm/WasmFrameIterator.h b/js/src/wasm/WasmFrameIter.h
rename from js/src/wasm/WasmFrameIterator.h
rename to js/src/wasm/WasmFrameIter.h
--- a/js/src/wasm/WasmFrameIterator.h
+++ b/js/src/wasm/WasmFrameIter.h
@@ -11,18 +11,18 @@
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
 
-#ifndef wasm_frame_iterator_h
-#define wasm_frame_iterator_h
+#ifndef wasm_frame_iter_h
+#define wasm_frame_iter_h
 
 #include "js/ProfilingFrameIterator.h"
 
 class JSAtom;
 
 namespace js {
 
 class WasmActivation;
@@ -44,35 +44,35 @@ struct CallableOffsets;
 // Iterates over the frames of a single WasmActivation, called synchronously
 // from C++ in the thread of the asm.js.
 //
 // The one exception is that this iterator may be called from the interrupt
 // callback which may be called asynchronously from asm.js code; in this case,
 // the backtrace may not be correct. That being said, we try our best printing
 // an informative message to the user and at least the name of the innermost
 // function stack frame.
-class FrameIterator
+class WasmFrameIter
 {
   public:
     enum class Unwind { True, False };
 
   private:
     WasmActivation* activation_;
     const Code* code_;
     const CallSite* callsite_;
     const CodeRange* codeRange_;
     Frame* fp_;
     Unwind unwind_;
     void** unwoundAddressOfReturnAddress_;
 
     void popFrame();
 
   public:
-    explicit FrameIterator();
-    explicit FrameIterator(WasmActivation* activation, Unwind unwind = Unwind::False);
+    explicit WasmFrameIter();
+    explicit WasmFrameIter(WasmActivation* activation, Unwind unwind = Unwind::False);
     void operator++();
     bool done() const;
     const char* filename() const;
     const char16_t* displayURL() const;
     bool mutedErrors() const;
     JSAtom* functionDisplayAtom() const;
     unsigned lineOrBytecode() const;
     const CodeRange* codeRange() const { return codeRange_; }
@@ -231,9 +231,9 @@ typedef JS::ProfilingFrameIterator::Regi
 
 bool
 StartUnwinding(const WasmActivation& activation, const RegisterState& registers,
                UnwindState* unwindState, bool* unwoundCaller);
 
 } // namespace wasm
 } // namespace js
 
-#endif // wasm_frame_iterator_h
+#endif // wasm_frame_iter_h
