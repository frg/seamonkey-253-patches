# HG changeset patch
# User Ryan VanderMeulen <ryanvm@gmail.com>
# Date 1537802797 14400
# Node ID 0888032a7a08230b694c8a3a33c72cf6bc859388
# Parent  288b0922b3d29d70a6d8a10bb6e7abfc6b207b71
Bug 1493700 - Update brotli to version 1.0.6. r=jfkthame

diff --git a/modules/brotli/README.mozilla b/modules/brotli/README.mozilla
--- a/modules/brotli/README.mozilla
+++ b/modules/brotli/README.mozilla
@@ -6,9 +6,9 @@ Upstream code can be viewed at
 
 and cloned by
   git clone https://github.com/google/brotli
 
 The in-tree copy is updated by running
   sh update.sh
 from within the modules/brotli directory.
 
-Current version: [commit b601fe817bd3217cb144bbb380a43cae8e847388].
+Current version: [commit 6eba239a5bb553fd557b7d78f7da8f0059618b9e].
diff --git a/modules/brotli/common/platform.h b/modules/brotli/common/platform.h
--- a/modules/brotli/common/platform.h
+++ b/modules/brotli/common/platform.h
@@ -182,17 +182,24 @@ OR:
 
 #if (defined(__ARM_ARCH) && (__ARM_ARCH == 7)) || \
     (defined(M_ARM) && (M_ARM == 7))
 #define BROTLI_TARGET_ARMV7
 #endif  /* ARMv7 */
 
 #if (defined(__ARM_ARCH) && (__ARM_ARCH == 8)) || \
     defined(__aarch64__) || defined(__ARM64_ARCH_8__)
-#define BROTLI_TARGET_ARMV8
+#define BROTLI_TARGET_ARMV8_ANY
+
+#if defined(__ARM_32BIT_STATE)
+#define BROTLI_TARGET_ARMV8_32
+#elif defined(__ARM_64BIT_STATE)
+#define BROTLI_TARGET_ARMV8_64
+#endif
+
 #endif  /* ARMv8 */
 
 #if defined(__i386) || defined(_M_IX86)
 #define BROTLI_TARGET_X86
 #endif
 
 #if defined(__x86_64__) || defined(_M_X64)
 #define BROTLI_TARGET_X64
@@ -205,17 +212,17 @@ OR:
 #if defined(__riscv) && defined(__riscv_xlen) && __riscv_xlen == 64
 #define BROTLI_TARGET_RISCV64
 #endif
 
 #if defined(BROTLI_BUILD_64_BIT)
 #define BROTLI_64_BITS 1
 #elif defined(BROTLI_BUILD_32_BIT)
 #define BROTLI_64_BITS 0
-#elif defined(BROTLI_TARGET_X64) || defined(BROTLI_TARGET_ARMV8) || \
+#elif defined(BROTLI_TARGET_X64) || defined(BROTLI_TARGET_ARMV8_64) || \
     defined(BROTLI_TARGET_POWERPC64) || defined(BROTLI_TARGET_RISCV64)
 #define BROTLI_64_BITS 1
 #else
 #define BROTLI_64_BITS 0
 #endif
 
 #if (BROTLI_64_BITS)
 #define brotli_reg_t uint64_t
@@ -256,17 +263,17 @@ OR:
 #undef BROTLI_X_BYTE_ORDER
 #undef BROTLI_X_LITTLE_ENDIAN
 #undef BROTLI_X_BIG_ENDIAN
 #endif
 
 #if defined(BROTLI_BUILD_PORTABLE)
 #define BROTLI_ALIGNED_READ (!!1)
 #elif defined(BROTLI_TARGET_X86) || defined(BROTLI_TARGET_X64) || \
-    defined(BROTLI_TARGET_ARMV7) || defined(BROTLI_TARGET_ARMV8) || \
+    defined(BROTLI_TARGET_ARMV7) || defined(BROTLI_TARGET_ARMV8_ANY) || \
     defined(BROTLI_TARGET_RISCV64)
 /* Allow unaligned read only for white-listed CPUs. */
 #define BROTLI_ALIGNED_READ (!!0)
 #else
 #define BROTLI_ALIGNED_READ (!!1)
 #endif
 
 #if BROTLI_ALIGNED_READ
@@ -286,41 +293,83 @@ static BROTLI_INLINE uint64_t BrotliUnal
   memcpy(&t, p, sizeof t);
   return t;
 }
 static BROTLI_INLINE void BrotliUnalignedWrite64(void* p, uint64_t v) {
   memcpy(p, &v, sizeof v);
 }
 #else  /* BROTLI_ALIGNED_READ */
 /* Unaligned memory access is allowed: just cast pointer to requested type. */
+#if defined(ADDRESS_SANITIZER) || defined(THREAD_SANITIZER) || \
+    defined(MEMORY_SANITIZER)
+/* Consider we have an unaligned load/store of 4 bytes from address 0x...05.
+   AddressSanitizer will treat it as a 3-byte access to the range 05:07 and
+   will miss a bug if 08 is the first unaddressable byte.
+   ThreadSanitizer will also treat this as a 3-byte access to 05:07 and will
+   miss a race between this access and some other accesses to 08.
+   MemorySanitizer will correctly propagate the shadow on unaligned stores
+   and correctly report bugs on unaligned loads, but it may not properly
+   update and report the origin of the uninitialized memory.
+   For all three tools, replacing an unaligned access with a tool-specific
+   callback solves the problem. */
+#if defined(__cplusplus)
+extern "C" {
+#endif  /* __cplusplus */
+  uint16_t __sanitizer_unaligned_load16(const void* p);
+  uint32_t __sanitizer_unaligned_load32(const void* p);
+  uint64_t __sanitizer_unaligned_load64(const void* p);
+  void __sanitizer_unaligned_store64(void* p, uint64_t v);
+#if defined(__cplusplus)
+}  /* extern "C" */
+#endif  /* __cplusplus */
+#define BrotliUnalignedRead16 __sanitizer_unaligned_load16
+#define BrotliUnalignedRead32 __sanitizer_unaligned_load32
+#define BrotliUnalignedRead64 __sanitizer_unaligned_load64
+#define BrotliUnalignedWrite64 __sanitizer_unaligned_store64
+#else
 static BROTLI_INLINE uint16_t BrotliUnalignedRead16(const void* p) {
   return *(const uint16_t*)p;
 }
 static BROTLI_INLINE uint32_t BrotliUnalignedRead32(const void* p) {
   return *(const uint32_t*)p;
 }
 #if (BROTLI_64_BITS)
 static BROTLI_INLINE uint64_t BrotliUnalignedRead64(const void* p) {
   return *(const uint64_t*)p;
 }
 static BROTLI_INLINE void BrotliUnalignedWrite64(void* p, uint64_t v) {
   *(uint64_t*)p = v;
 }
 #else  /* BROTLI_64_BITS */
 /* Avoid emitting LDRD / STRD, which require properly aligned address. */
+/* If __attribute__(aligned) is available, use that. Otherwise, memcpy. */
+
+#if BROTLI_GNUC_HAS_ATTRIBUTE(aligned, 2, 7, 0)
+typedef  __attribute__((aligned(1))) uint64_t brotli_unaligned_uint64_t;
+
 static BROTLI_INLINE uint64_t BrotliUnalignedRead64(const void* p) {
-  const uint32_t* dwords = (const uint32_t*)p;
-  return dwords[0] | ((uint64_t)dwords[1] << 32);
+  return (uint64_t) ((brotli_unaligned_uint64_t*) p)[0];
 }
 static BROTLI_INLINE void BrotliUnalignedWrite64(void* p, uint64_t v) {
-  uint32_t* dwords = (uint32_t *)p;
-  dwords[0] = (uint32_t)v;
-  dwords[1] = (uint32_t)(v >> 32);
+  brotli_unaligned_uint64_t* dwords = (brotli_unaligned_uint64_t*) p;
+  dwords[0] = (brotli_unaligned_uint64_t) v;
 }
+#else /* BROTLI_GNUC_HAS_ATTRIBUTE(aligned, 2, 7, 0) */
+static BROTLI_INLINE uint64_t BrotliUnalignedRead64(const void* p) {
+  uint64_t v;
+  memcpy(&v, p, sizeof(uint64_t));
+  return v;
+}
+
+static BROTLI_INLINE void BrotliUnalignedWrite64(void* p, uint64_t v) {
+  memcpy(p, &v, sizeof(uint64_t));
+}
+#endif  /* BROTLI_GNUC_HAS_ATTRIBUTE(aligned, 2, 7, 0) */
 #endif  /* BROTLI_64_BITS */
+#endif  /* ASAN / TSAN / MSAN */
 #endif  /* BROTLI_ALIGNED_READ */
 
 #if BROTLI_LITTLE_ENDIAN
 /* Straight endianness. Just read / write values. */
 #define BROTLI_UNALIGNED_LOAD16LE BrotliUnalignedRead16
 #define BROTLI_UNALIGNED_LOAD32LE BrotliUnalignedRead32
 #define BROTLI_UNALIGNED_LOAD64LE BrotliUnalignedRead64
 #define BROTLI_UNALIGNED_STORE64LE BrotliUnalignedWrite64
@@ -395,17 +444,17 @@ static BROTLI_INLINE void BROTLI_UNALIGN
 /* BROTLI_IS_CONSTANT macros returns true for compile-time constants. */
 #if BROTLI_GNUC_HAS_BUILTIN(__builtin_constant_p, 3, 0, 1) || \
     BROTLI_INTEL_VERSION_CHECK(16, 0, 0)
 #define BROTLI_IS_CONSTANT(x) (!!__builtin_constant_p(x))
 #else
 #define BROTLI_IS_CONSTANT(x) (!!0)
 #endif
 
-#if defined(BROTLI_TARGET_ARMV7) || defined(BROTLI_TARGET_ARMV8)
+#if defined(BROTLI_TARGET_ARMV7) || defined(BROTLI_TARGET_ARMV8_ANY)
 #define BROTLI_HAS_UBFX (!!1)
 #else
 #define BROTLI_HAS_UBFX (!!0)
 #endif
 
 #if defined(BROTLI_ENABLE_LOG)
 #define BROTLI_DCHECK(x) assert(x)
 #define BROTLI_LOG(x) printf x
@@ -422,17 +471,17 @@ static BROTLI_INLINE void BrotliDump(con
 #define BROTLI_DUMP() BrotliDump(__FILE__, __LINE__, __FUNCTION__)
 #else
 #define BROTLI_DUMP() (void)(0)
 #endif
 
 /* TODO: add appropriate icc/sunpro/arm/ibm/ti checks. */
 #if (BROTLI_GNUC_VERSION_CHECK(3, 0, 0) || defined(__llvm__)) && \
     !defined(BROTLI_BUILD_NO_RBIT)
-#if defined(BROTLI_TARGET_ARMV7) || defined(BROTLI_TARGET_ARMV8)
+#if defined(BROTLI_TARGET_ARMV7) || defined(BROTLI_TARGET_ARMV8_ANY)
 /* TODO: detect ARMv6T2 and enable this code for it. */
 static BROTLI_INLINE brotli_reg_t BrotliRBit(brotli_reg_t input) {
   brotli_reg_t output;
   __asm__("rbit %0, %1\n" : "=r"(output) : "r"(input));
   return output;
 }
 #define BROTLI_RBIT(x) BrotliRBit(x)
 #endif  /* armv7 / armv8 */
diff --git a/modules/brotli/common/version.h b/modules/brotli/common/version.h
--- a/modules/brotli/common/version.h
+++ b/modules/brotli/common/version.h
@@ -9,18 +9,18 @@
 #ifndef BROTLI_COMMON_VERSION_H_
 #define BROTLI_COMMON_VERSION_H_
 
 /* This macro should only be used when library is compiled together with client.
    If library is dynamically linked, use BrotliDecoderVersion and
    BrotliEncoderVersion methods. */
 
 /* Semantic version, calculated as (MAJOR << 24) | (MINOR << 12) | PATCH */
-#define BROTLI_VERSION 0x1000005
+#define BROTLI_VERSION 0x1000006
 
 /* This macro is used by build system to produce Libtool-friendly soname. See
    https://www.gnu.org/software/libtool/manual/html_node/Libtool-versioning.html
  */
 
 /* ABI version, calculated as (CURRENT << 24) | (REVISION << 12) | AGE */
-#define BROTLI_ABI_VERSION 0x1005000
+#define BROTLI_ABI_VERSION 0x1006000
 
 #endif  /* BROTLI_COMMON_VERSION_H_ */
diff --git a/modules/brotli/enc/metablock.c b/modules/brotli/enc/metablock.c
--- a/modules/brotli/enc/metablock.c
+++ b/modules/brotli/enc/metablock.c
@@ -176,17 +176,18 @@ void BrotliBuildMetaBlock(MemoryManager*
     if (ndirect_msb > 0) ndirect_msb--;
     ndirect_msb /= 2;
   }
   if (check_orig) {
     double dist_cost;
     ComputeDistanceCost(cmds, num_commands,
                         &orig_params.dist, &orig_params.dist, &dist_cost);
     if (dist_cost < best_dist_cost) {
-      best_dist_cost = dist_cost;
+      /* NB: currently unused; uncomment when more param tuning is added. */
+      /* best_dist_cost = dist_cost; */
       params->dist = orig_params.dist;
     }
   }
   RecomputeDistancePrefixes(cmds, num_commands,
                             &orig_params.dist, &params->dist);
 
   BrotliSplitBlock(m, cmds, num_commands,
                    ringbuffer, pos, mask, params,
diff --git a/modules/brotli/update.sh b/modules/brotli/update.sh
--- a/modules/brotli/update.sh
+++ b/modules/brotli/update.sh
@@ -1,17 +1,17 @@
 #!/bin/sh
 
 # Script to update the mozilla in-tree copy of the Brotli library.
 # Run this within the /modules/brotli directory of the source tree.
 
 MY_TEMP_DIR=`mktemp -d -t brotli_update.XXXXXX` || exit 1
 
 git clone https://github.com/google/brotli ${MY_TEMP_DIR}/brotli
-git -C ${MY_TEMP_DIR}/brotli checkout v1.0.5
+git -C ${MY_TEMP_DIR}/brotli checkout v1.0.6
 
 COMMIT=$(git -C ${MY_TEMP_DIR}/brotli rev-parse HEAD)
 perl -p -i -e "s/\[commit [0-9a-f]{40}\]/[commit ${COMMIT}]/" README.mozilla;
 
 DIRS="common dec enc include tools"
 
 for d in $DIRS; do
 	rm -rf $d
