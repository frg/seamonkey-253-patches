# HG changeset patch
# User Olli Pettay <Olli.Pettay@helsinki.fi>
# Date 1505231997 -10800
#      Tue Sep 12 18:59:57 2017 +0300
# Node ID 644d9742cf75fecc073c6f13553c7806cfe48ffb
# Parent  de169a4b3982e17c6c3d6f69339279b152daf8e1
Bug 1399160 - Make CCGraphBuilder::BuildGraph to check the budget more often, r=mccr8

diff --git a/xpcom/base/nsCycleCollector.cpp b/xpcom/base/nsCycleCollector.cpp
--- a/xpcom/base/nsCycleCollector.cpp
+++ b/xpcom/base/nsCycleCollector.cpp
@@ -2094,16 +2094,17 @@ private:
   EdgePool::Builder mEdgeBuilder;
   MOZ_INIT_OUTSIDE_CTOR PtrInfo* mCurrPi;
   nsCycleCollectionParticipant* mJSParticipant;
   nsCycleCollectionParticipant* mJSZoneParticipant;
   nsCString mNextEdgeName;
   RefPtr<nsCycleCollectorLogger> mLogger;
   bool mMergeZones;
   nsAutoPtr<NodePool::Enumerator> mCurrNode;
+  uint32_t mNoteChildCount;
 
 public:
   CCGraphBuilder(CCGraph& aGraph,
                  CycleCollectorResults& aResults,
                  CycleCollectedJSRuntime* aCCRuntime,
                  nsCycleCollectorLogger* aLogger,
                  bool aMergeZones);
   virtual ~CCGraphBuilder();
@@ -2207,16 +2208,17 @@ CCGraphBuilder::CCGraphBuilder(CCGraph& 
   : mGraph(aGraph)
   , mResults(aResults)
   , mNodeBuilder(aGraph.mNodes)
   , mEdgeBuilder(aGraph.mEdges)
   , mJSParticipant(nullptr)
   , mJSZoneParticipant(nullptr)
   , mLogger(aLogger)
   , mMergeZones(aMergeZones)
+  , mNoteChildCount(0)
 {
   if (aCCRuntime) {
     mJSParticipant = aCCRuntime->GCThingParticipant();
     mJSZoneParticipant = aCCRuntime->ZoneParticipant();
   }
 
   if (mLogger) {
     mFlags |= nsCycleCollectionTraversalCallback::WANT_DEBUG_INFO;
@@ -2284,22 +2286,24 @@ CCGraphBuilder::DoneAddingRoots()
   mGraph.mRootCount = mGraph.MapCount();
 
   mCurrNode = new NodePool::Enumerator(mGraph.mNodes);
 }
 
 MOZ_NEVER_INLINE bool
 CCGraphBuilder::BuildGraph(SliceBudget& aBudget)
 {
-  const intptr_t kNumNodesBetweenTimeChecks = 500;
+  const intptr_t kNumNodesBetweenTimeChecks = 1000;
   const intptr_t kStep = SliceBudget::CounterReset / kNumNodesBetweenTimeChecks;
 
   MOZ_ASSERT(mCurrNode);
 
   while (!aBudget.isOverBudget() && !mCurrNode->IsDone()) {
+    mNoteChildCount = 0;
+
     PtrInfo* pi = mCurrNode->GetNext();
     if (!pi) {
       MOZ_CRASH();
     }
 
     mCurrPi = pi;
 
     // We need to call SetFirstChild() even on deleted nodes, to set their
@@ -2310,17 +2314,17 @@ CCGraphBuilder::BuildGraph(SliceBudget& 
       nsresult rv = pi->mParticipant->TraverseNativeAndJS(pi->mPointer, *this);
       MOZ_RELEASE_ASSERT(!NS_FAILED(rv), "Cycle collector Traverse method failed");
     }
 
     if (mCurrNode->AtBlockEnd()) {
       SetLastChild();
     }
 
-    aBudget.step(kStep);
+    aBudget.step(kStep * (mNoteChildCount + 1));
   }
 
   if (!mCurrNode->IsDone()) {
     return false;
   }
 
   if (mGraph.mRootCount > 0) {
     SetLastChild();
@@ -2401,16 +2405,18 @@ CCGraphBuilder::NoteXPCOMChild(nsISuppor
   if (WantDebugInfo()) {
     edgeName.Assign(mNextEdgeName);
     mNextEdgeName.Truncate();
   }
   if (!aChild || !(aChild = CanonicalizeXPCOMParticipant(aChild))) {
     return;
   }
 
+  ++mNoteChildCount;
+
   nsXPCOMCycleCollectionParticipant* cp;
   ToParticipant(aChild, &cp);
   if (cp && (!cp->CanSkipThis(aChild) || WantAllTraces())) {
     NoteChild(aChild, cp, edgeName);
   }
 }
 
 NS_IMETHODIMP_(void)
@@ -2421,29 +2427,33 @@ CCGraphBuilder::NoteNativeChild(void* aC
   if (WantDebugInfo()) {
     edgeName.Assign(mNextEdgeName);
     mNextEdgeName.Truncate();
   }
   if (!aChild) {
     return;
   }
 
+  ++mNoteChildCount;
+
   MOZ_ASSERT(aParticipant, "Need a nsCycleCollectionParticipant!");
   if (!aParticipant->CanSkipThis(aChild) || WantAllTraces()) {
     NoteChild(aChild, aParticipant, edgeName);
   }
 }
 
 NS_IMETHODIMP_(void)
 CCGraphBuilder::NoteJSChild(const JS::GCCellPtr& aChild)
 {
   if (!aChild) {
     return;
   }
 
+  ++mNoteChildCount;
+
   nsCString edgeName;
   if (MOZ_UNLIKELY(WantDebugInfo())) {
     edgeName.Assign(mNextEdgeName);
     mNextEdgeName.Truncate();
   }
 
   if (GCThingIsGrayCCThing(aChild) || MOZ_UNLIKELY(WantAllTraces())) {
     if (JS::Zone* zone = MergeZone(aChild)) {
