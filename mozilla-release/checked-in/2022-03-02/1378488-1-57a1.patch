# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1502744398 14400
#      Mon Aug 14 16:59:58 2017 -0400
# Node ID ed28a90d1edb3918d4ac7363412a88f4f8dbbf88
# Parent  47b298a10edc32f5783257ca7c73d713887c167a
Bug 1378488 - Part 1. imgRequest::FinishPreparingForNewPart should dispatch using ProgressTracker::GetEventTarget. r=tnikkel

diff --git a/image/imgRequest.cpp b/image/imgRequest.cpp
--- a/image/imgRequest.cpp
+++ b/image/imgRequest.cpp
@@ -1136,32 +1136,45 @@ imgRequest::OnDataAvailable(nsIRequest* 
   if (newPartPending) {
     NewPartResult result = PrepareForNewPart(aRequest, aInStr, aCount, mURI,
                                              isMultipart, image,
                                              progressTracker, mInnerWindowId);
     bool succeeded = result.mSucceeded;
 
     if (result.mImage) {
       image = result.mImage;
+      nsCOMPtr<nsIEventTarget> eventTarget;
 
       // Update our state to reflect this new part.
       {
         MutexAutoLock lock(mMutex);
         mImage = image;
+
+        // We only get an event target if we are not on the main thread, because
+        // we have to dispatch in that case. If we are on the main thread, but
+        // on a different scheduler group than ProgressTracker would give us,
+        // that is okay because nothing in imagelib requires that, just our
+        // listeners (which have their own checks).
+        if (!NS_IsMainThread()) {
+          eventTarget = mProgressTracker->GetEventTarget();
+          MOZ_ASSERT(eventTarget);
+        }
+
         mProgressTracker = nullptr;
       }
 
       // Some property objects are not threadsafe, and we need to send
       // OnImageAvailable on the main thread, so finish on the main thread.
-      if (NS_IsMainThread()) {
+      if (!eventTarget) {
+        MOZ_ASSERT(NS_IsMainThread());
         FinishPreparingForNewPart(result);
       } else {
         nsCOMPtr<nsIRunnable> runnable =
           new FinishPreparingForNewPartRunnable(this, Move(result));
-        NS_DispatchToMainThread(runnable);
+        eventTarget->Dispatch(runnable.forget(), NS_DISPATCH_NORMAL);
       }
     }
 
     if (!succeeded) {
       // Something went wrong; probably a content type issue.
       Cancel(NS_IMAGELIB_ERROR_FAILURE);
       return NS_BINDING_ABORTED;
     }
