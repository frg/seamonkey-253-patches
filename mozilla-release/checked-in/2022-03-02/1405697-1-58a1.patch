# HG changeset patch
# User Chris Pearce <cpearce@mozilla.com>
# Date 1507127432 -7200
# Node ID 44b026781b82fddd1a9c6dbf8f79c0588ab255a6
# Parent  0a1d669c007cd5c537de4a6f20b4a5f5d11457c8
Bug 1405697 - Move MediaChannelStatistics to its own file. r=jwwang

It currently resides in MediaResource.h, but it's only used in a
ChannelMediaDecoder and ChannelMediaResource, so it doesn't need to be in the
abstract header.


MozReview-Commit-ID: GskE5mjMav1

diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef ChannelMediaDecoder_h_
 #define ChannelMediaDecoder_h_
 
 #include "MediaDecoder.h"
 #include "MediaResourceCallback.h"
+#include "MediaChannelStatistics.h"
 
 class nsIChannel;
 class nsIStreamListener;
 
 namespace mozilla {
 
 class BaseMediaResource;
 
diff --git a/dom/media/ChannelMediaResource.h b/dom/media/ChannelMediaResource.h
--- a/dom/media/ChannelMediaResource.h
+++ b/dom/media/ChannelMediaResource.h
@@ -2,16 +2,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_media_ChannelMediaResource_h
 #define mozilla_dom_media_ChannelMediaResource_h
 
 #include "MediaResource.h"
+#include "MediaChannelStatistics.h"
 
 namespace mozilla {
 
 /**
  * This is the MediaResource implementation that wraps Necko channels.
  * Much of its functionality is actually delegated to MediaCache via
  * an underlying MediaCacheStream.
  *
diff --git a/dom/media/MediaChannelStatistics.h b/dom/media/MediaChannelStatistics.h
new file mode 100644
--- /dev/null
+++ b/dom/media/MediaChannelStatistics.h
@@ -0,0 +1,90 @@
+/* vim:set ts=2 sw=2 sts=2 et cindent: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#if !defined(MediaChannelStatistics_h_)
+#define MediaChannelStatistics_h_
+
+namespace mozilla {
+
+// Number of bytes we have accumulated before we assume the connection download
+// rate can be reliably calculated. 57 Segments at IW=3 allows slow start to
+// reach a CWND of 30 (See bug 831998)
+static const int64_t RELIABLE_DATA_THRESHOLD = 57 * 1460;
+
+/**
+ * This class is useful for estimating rates of data passing through
+ * some channel. The idea is that activity on the channel "starts"
+ * and "stops" over time. At certain times data passes through the
+ * channel (usually while the channel is active; data passing through
+ * an inactive channel is ignored). The GetRate() function computes
+ * an estimate of the "current rate" of the channel, which is some
+ * kind of average of the data passing through over the time the
+ * channel is active.
+ *
+ * All methods take "now" as a parameter so the user of this class can
+ * control the timeline used.
+ */
+class MediaChannelStatistics {
+public:
+  MediaChannelStatistics() = default;
+
+  MediaChannelStatistics(const MediaChannelStatistics&) = default;
+
+  void Reset() {
+    mLastStartTime = TimeStamp();
+    mAccumulatedTime = TimeDuration(0);
+    mAccumulatedBytes = 0;
+    mIsStarted = false;
+  }
+  void Start() {
+    if (mIsStarted)
+      return;
+    mLastStartTime = TimeStamp::Now();
+    mIsStarted = true;
+  }
+  void Stop() {
+    if (!mIsStarted)
+      return;
+    mAccumulatedTime += TimeStamp::Now() - mLastStartTime;
+    mIsStarted = false;
+  }
+  void AddBytes(int64_t aBytes) {
+    if (!mIsStarted) {
+      // ignore this data, it may be related to seeking or some other
+      // operation we don't care about
+      return;
+    }
+    mAccumulatedBytes += aBytes;
+  }
+  double GetRateAtLastStop(bool* aReliable) {
+    double seconds = mAccumulatedTime.ToSeconds();
+    *aReliable = (seconds >= 1.0) ||
+                 (mAccumulatedBytes >= RELIABLE_DATA_THRESHOLD);
+    if (seconds <= 0.0)
+      return 0.0;
+    return static_cast<double>(mAccumulatedBytes)/seconds;
+  }
+  double GetRate(bool* aReliable) {
+    TimeDuration time = mAccumulatedTime;
+    if (mIsStarted) {
+      time += TimeStamp::Now() - mLastStartTime;
+    }
+    double seconds = time.ToSeconds();
+    *aReliable = (seconds >= 3.0) ||
+                 (mAccumulatedBytes >= RELIABLE_DATA_THRESHOLD);
+    if (seconds <= 0.0)
+      return 0.0;
+    return static_cast<double>(mAccumulatedBytes)/seconds;
+  }
+private:
+  int64_t mAccumulatedBytes = 0;
+  TimeDuration mAccumulatedTime;
+  TimeStamp mLastStartTime;
+  bool mIsStarted = false;
+};
+
+} // namespace mozilla
+
+#endif // MediaChannelStatistics_h_
diff --git a/dom/media/MediaResource.h b/dom/media/MediaResource.h
--- a/dom/media/MediaResource.h
+++ b/dom/media/MediaResource.h
@@ -33,100 +33,21 @@
 //
 // If we assume a 100Mbit connection, and assume reissuing an HTTP seek causes
 // a delay of 200ms, then in that 200ms we could have simply read ahead 2MB. So
 // setting SEEK_VS_READ_THRESHOLD to 1MB sounds reasonable.
 static const int64_t SEEK_VS_READ_THRESHOLD = 1 * 1024 * 1024;
 
 static const uint32_t HTTP_REQUESTED_RANGE_NOT_SATISFIABLE_CODE = 416;
 
-// Number of bytes we have accumulated before we assume the connection download
-// rate can be reliably calculated. 57 Segments at IW=3 allows slow start to
-// reach a CWND of 30 (See bug 831998)
-static const int64_t RELIABLE_DATA_THRESHOLD = 57 * 1460;
-
 class nsIHttpChannel;
 class nsIPrincipal;
 
 namespace mozilla {
 
-class MediaChannelStatistics;
-
-/**
- * This class is useful for estimating rates of data passing through
- * some channel. The idea is that activity on the channel "starts"
- * and "stops" over time. At certain times data passes through the
- * channel (usually while the channel is active; data passing through
- * an inactive channel is ignored). The GetRate() function computes
- * an estimate of the "current rate" of the channel, which is some
- * kind of average of the data passing through over the time the
- * channel is active.
- *
- * All methods take "now" as a parameter so the user of this class can
- * control the timeline used.
- */
-class MediaChannelStatistics {
-public:
-  MediaChannelStatistics() = default;
-
-  MediaChannelStatistics(const MediaChannelStatistics&) = default;
-
-  void Reset() {
-    mLastStartTime = TimeStamp();
-    mAccumulatedTime = TimeDuration(0);
-    mAccumulatedBytes = 0;
-    mIsStarted = false;
-  }
-  void Start() {
-    if (mIsStarted)
-      return;
-    mLastStartTime = TimeStamp::Now();
-    mIsStarted = true;
-  }
-  void Stop() {
-    if (!mIsStarted)
-      return;
-    mAccumulatedTime += TimeStamp::Now() - mLastStartTime;
-    mIsStarted = false;
-  }
-  void AddBytes(int64_t aBytes) {
-    if (!mIsStarted) {
-      // ignore this data, it may be related to seeking or some other
-      // operation we don't care about
-      return;
-    }
-    mAccumulatedBytes += aBytes;
-  }
-  double GetRateAtLastStop(bool* aReliable) {
-    double seconds = mAccumulatedTime.ToSeconds();
-    *aReliable = (seconds >= 1.0) ||
-                 (mAccumulatedBytes >= RELIABLE_DATA_THRESHOLD);
-    if (seconds <= 0.0)
-      return 0.0;
-    return static_cast<double>(mAccumulatedBytes)/seconds;
-  }
-  double GetRate(bool* aReliable) {
-    TimeDuration time = mAccumulatedTime;
-    if (mIsStarted) {
-      time += TimeStamp::Now() - mLastStartTime;
-    }
-    double seconds = time.ToSeconds();
-    *aReliable = (seconds >= 3.0) ||
-                 (mAccumulatedBytes >= RELIABLE_DATA_THRESHOLD);
-    if (seconds <= 0.0)
-      return 0.0;
-    return static_cast<double>(mAccumulatedBytes)/seconds;
-  }
-private:
-  int64_t mAccumulatedBytes = 0;
-  TimeDuration mAccumulatedTime;
-  TimeStamp mLastStartTime;
-  bool mIsStarted = false;
-};
-
 // Represents a section of contiguous media, with a start and end offset.
 // Used to denote ranges of data which are cached.
 
 typedef media::Interval<int64_t> MediaByteRange;
 typedef media::IntervalSet<int64_t> MediaByteRangeSet;
 
 /**
  * Provides a thread-safe, seek/read interface to resources
