# HG changeset patch
# User Ryan Hunt <rhunt@eqrion.net>
# Date 1504800378 18000
#      Thu Sep 07 11:06:18 2017 -0500
# Node ID e5333aad4139ff4e480078a4a5f064dfde475f4c
# Parent  3d172d0b6d8d6d881ed6dda074741702992c74ad
Bug 1397770 - Update webrender to commit dbffdc219fa394ab1225cebc71fe5998b9337cb2. r=jrmuizel

MozReview-Commit-ID: CUc8Oki3doq

diff --git a/gfx/doc/README.webrender b/gfx/doc/README.webrender
--- a/gfx/doc/README.webrender
+++ b/gfx/doc/README.webrender
@@ -74,9 +74,9 @@ there is another crate in m-c called moz
 the same folder to store its rust dependencies. If one of the libraries that is
 required by both mozjs_sys and webrender is updated without updating the other
 project's Cargo.lock file, that results in build bustage.
 This means that any time you do this sort of manual update of packages, you need
 to make sure that mozjs_sys also has its Cargo.lock file updated if needed, hence
 the need to run the cargo update command in js/src as well. Hopefully this will
 be resolved soon.
 
-Latest Commit: fe83b424e7b8bce3d3661c3561cfd58a6cd186fe
+Latest Commit: dbffdc219fa394ab1225cebc71fe5998b9337cb2
diff --git a/gfx/webrender/src/prim_store.rs b/gfx/webrender/src/prim_store.rs
--- a/gfx/webrender/src/prim_store.rs
+++ b/gfx/webrender/src/prim_store.rs
@@ -157,17 +157,17 @@ pub struct PrimitiveMetadata {
 }
 
 impl PrimitiveMetadata {
     pub fn needs_clipping(&self) -> bool {
         self.clip_task_id.is_some()
     }
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 #[repr(C)]
 pub struct RectanglePrimitive {
     pub color: ColorF,
 }
 
 impl ToGpuBlocks for RectanglePrimitive {
     fn write_gpu_blocks(&self, mut request: GpuDataRequest) {
         request.push(self.color);
@@ -221,17 +221,17 @@ pub struct YuvImagePrimitiveCpu {
 }
 
 impl ToGpuBlocks for YuvImagePrimitiveCpu {
     fn write_gpu_blocks(&self, mut request: GpuDataRequest) {
         request.push(self.gpu_block);
     }
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub struct BorderPrimitiveCpu {
     pub corner_instances: [BorderCornerInstance; 4],
     pub gpu_blocks: [GpuBlockData; 8],
 }
 
 impl ToGpuBlocks for BorderPrimitiveCpu {
     fn write_gpu_blocks(&self, mut request: GpuDataRequest) {
         request.extend_from_slice(&self.gpu_blocks);
@@ -241,17 +241,17 @@ impl ToGpuBlocks for BorderPrimitiveCpu 
 #[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
 pub struct BoxShadowPrimitiveCacheKey {
     pub shadow_rect_size: Size2D<Au>,
     pub border_radius: Au,
     pub blur_radius: Au,
     pub inverted: bool,
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub struct BoxShadowPrimitiveCpu {
     // todo(gw): generate on demand
     // gpu data
     pub src_rect: LayerRect,
     pub bs_rect: LayerRect,
     pub color: ColorF,
     pub border_radius: f32,
     pub edge_size: f32,
@@ -322,17 +322,17 @@ pub const GRADIENT_DATA_TABLE_BEGIN: usi
 // The exclusive bound of the gradient data table
 pub const GRADIENT_DATA_TABLE_END: usize = GRADIENT_DATA_LAST_STOP;
 // The number of entries in the gradient data table.
 pub const GRADIENT_DATA_TABLE_SIZE: usize = 128;
 
 // The number of entries in a gradient data: GRADIENT_DATA_TABLE_SIZE + first stop entry + last stop entry
 pub const GRADIENT_DATA_SIZE: usize = GRADIENT_DATA_TABLE_SIZE + 2;
 
-#[derive(Debug, Clone, Copy)]
+#[derive(Debug)]
 #[repr(C)]
 // An entry in a gradient data table representing a segment of the gradient color space.
 pub struct GradientDataEntry {
     pub start_color: ColorF,
     pub end_color: ColorF,
 }
 
 struct GradientGpuBlockBuilder<'a> {
@@ -484,23 +484,17 @@ impl RadialGradientPrimitiveCpu {
         request.extend_from_slice(&self.gpu_blocks);
 
         let gradient_builder = GradientGpuBlockBuilder::new(self.stops_range,
                                                             display_list);
         gradient_builder.build(false, &mut request);
     }
 }
 
-#[derive(Debug, Clone)]
-pub struct TextDecoration {
-    pub local_rect: LayerRect,
-    pub prim: RectanglePrimitive,
-}
-
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub struct TextShadowPrimitiveCpu {
     pub shadow: TextShadow,
     pub primitives: Vec<PrimitiveIndex>,
 }
 
 #[derive(Debug, Clone)]
 pub struct TextRunPrimitiveCpu {
     pub font: FontInstance,
@@ -588,31 +582,24 @@ impl TextRunPrimitiveCpu {
                       self.font.subpx_dir as u32 as f32,
                       0.0]);
         request.extend_from_slice(&self.glyph_gpu_blocks);
 
         assert!(request.current_used_block_num() <= MAX_VERTEX_TEXTURE_WIDTH);
     }
 }
 
-#[derive(Debug, Clone)]
-#[repr(C)]
-struct GlyphPrimitive {
-    offset: LayerPoint,
-    padding: LayerPoint,
-}
-
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 #[repr(C)]
 struct ClipRect {
     rect: LayerRect,
     mode: f32,
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 #[repr(C)]
 struct ClipCorner {
     rect: LayerRect,
     outer_radius_x: f32,
     outer_radius_y: f32,
     inner_radius_x: f32,
     inner_radius_y: f32,
 }
@@ -637,29 +624,29 @@ impl ClipCorner {
             outer_radius_x: outer_radius,
             outer_radius_y: outer_radius,
             inner_radius_x: inner_radius,
             inner_radius_y: inner_radius,
         }
     }
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 #[repr(C)]
 pub struct ImageMaskData {
     pub local_rect: LayerRect,
 }
 
 impl ToGpuBlocks for ImageMaskData {
     fn write_gpu_blocks(&self, mut request: GpuDataRequest) {
         request.push(self.local_rect);
     }
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub struct ClipData {
     rect: ClipRect,
     top_left: ClipCorner,
     top_right: ClipCorner,
     bottom_left: ClipCorner,
     bottom_right: ClipCorner,
 }
 
diff --git a/gfx/webrender/src/render_task.rs b/gfx/webrender/src/render_task.rs
--- a/gfx/webrender/src/render_task.rs
+++ b/gfx/webrender/src/render_task.rs
@@ -112,32 +112,32 @@ impl RenderTaskTree {
 #[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
 pub enum RenderTaskKey {
     /// Draw this box shadow to a cache target.
     BoxShadow(BoxShadowPrimitiveCacheKey),
     /// Draw the alpha mask for a shared clip.
     CacheMask(ClipId),
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub enum RenderTaskLocation {
     Fixed,
     Dynamic(Option<(DeviceIntPoint, RenderTargetIndex)>, DeviceIntSize),
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub enum AlphaRenderItem {
     Primitive(Option<ClipScrollGroupIndex>, PrimitiveIndex, i32),
     Blend(StackingContextIndex, RenderTaskId, FilterOp, i32),
     Composite(StackingContextIndex, RenderTaskId, RenderTaskId, MixBlendMode, i32),
     SplitComposite(StackingContextIndex, RenderTaskId, GpuCacheHandle, i32),
     HardwareComposite(StackingContextIndex, RenderTaskId, HardwareCompositeOp, i32),
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub struct AlphaRenderTask {
     pub screen_origin: DeviceIntPoint,
     pub items: Vec<AlphaRenderItem>,
 }
 
 #[derive(Debug, Copy, Clone)]
 #[repr(C)]
 pub enum MaskSegment {
@@ -154,42 +154,42 @@ pub enum MaskSegment {
 pub enum MaskGeometryKind {
     Default,        // Draw the entire rect
     CornersOnly,    // Draw the corners (simple axis aligned mask)
     // TODO(gw): Add more types here (e.g. 4 rectangles outside the inner rect)
 }
 
 pub type ClipWorkItem = (PackedLayerIndex, MaskCacheInfo);
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub struct CacheMaskTask {
     actual_rect: DeviceIntRect,
     inner_rect: DeviceIntRect,
     pub clips: Vec<ClipWorkItem>,
     pub geometry_kind: MaskGeometryKind,
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub struct RenderTaskData {
     pub data: [f32; FLOATS_PER_RENDER_TASK_INFO],
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub enum RenderTaskKind {
     Alpha(AlphaRenderTask),
     CachePrimitive(PrimitiveIndex),
     BoxShadow(PrimitiveIndex),
     CacheMask(CacheMaskTask),
     VerticalBlur(DeviceIntLength),
     HorizontalBlur(DeviceIntLength),
     Readback(DeviceIntRect),
     Alias(RenderTaskId),
 }
 
-#[derive(Debug, Clone)]
+#[derive(Debug)]
 pub struct RenderTask {
     pub cache_key: Option<RenderTaskKey>,
     pub location: RenderTaskLocation,
     pub children: Vec<RenderTaskId>,
     pub kind: RenderTaskKind,
 }
 
 impl RenderTask {
diff --git a/gfx/webrender/src/renderer.rs b/gfx/webrender/src/renderer.rs
--- a/gfx/webrender/src/renderer.rs
+++ b/gfx/webrender/src/renderer.rs
@@ -221,30 +221,25 @@ const DESC_CACHE_BOX_SHADOW: VertexDescr
         VertexAttribute { name: "aPosition", count: 2, kind: VertexAttributeKind::F32 },
     ],
     instance_attributes: &[
         VertexAttribute { name: "aPrimAddress", count: 2, kind: VertexAttributeKind::U16 },
         VertexAttribute { name: "aTaskIndex", count: 1, kind: VertexAttributeKind::I32 },
     ]
 };
 
+#[derive(Debug, Copy, Clone)]
 enum VertexArrayKind {
     Primitive,
     Blur,
     Clip,
 
     CacheBoxShadow,
 }
 
-#[derive(Copy, Clone, Debug, PartialEq, Eq)]
-pub enum VertexFormat {
-    PrimitiveInstances,
-    Blur,
-}
-
 #[derive(Clone, Debug, PartialEq)]
 pub enum GraphicsApi {
     OpenGL,
 }
 
 #[derive(Clone, Debug)]
 pub struct GraphicsApiInfo {
     pub kind: GraphicsApi,
@@ -702,17 +697,17 @@ impl VertexDataTexture {
 }
 
 const TRANSFORM_FEATURE: &str = "TRANSFORM";
 const SUBPIXEL_AA_FEATURE: &str = "SUBPIXEL_AA";
 const CLIP_FEATURE: &str = "CLIP";
 
 enum ShaderKind {
     Primitive,
-    Cache(VertexFormat),
+    Cache(VertexArrayKind),
     ClipCache,
 }
 
 struct LazilyCompiledShader {
     program: Option<Program>,
     name: &'static str,
     kind: ShaderKind,
     features: Vec<&'static str>,
@@ -753,17 +748,17 @@ impl LazilyCompiledShader {
     fn get(&mut self, device: &mut Device) -> Result<&Program, ShaderError> {
         if self.program.is_none() {
             let program = try!{
                 match self.kind {
                     ShaderKind::Primitive => {
                         create_prim_shader(self.name,
                                            device,
                                            &self.features,
-                                           VertexFormat::PrimitiveInstances)
+                                           VertexArrayKind::Primitive)
                     }
                     ShaderKind::Cache(format) => {
                         create_prim_shader(self.name,
                                            device,
                                            &self.features,
                                            format)
                     }
                     ShaderKind::ClipCache => {
@@ -847,29 +842,31 @@ impl PrimitiveShader {
         self.simple.deinit(device);
         self.transform.deinit(device);
     }
 }
 
 fn create_prim_shader(name: &'static str,
                       device: &mut Device,
                       features: &[&'static str],
-                      vertex_format: VertexFormat) -> Result<Program, ShaderError> {
+                      vertex_format: VertexArrayKind) -> Result<Program, ShaderError> {
     let mut prefix = format!("#define WR_MAX_VERTEX_TEXTURE_WIDTH {}\n",
                               MAX_VERTEX_TEXTURE_WIDTH);
 
     for feature in features {
         prefix.push_str(&format!("#define WR_FEATURE_{}\n", feature));
     }
 
     debug!("PrimShader {}", name);
 
     let vertex_descriptor = match vertex_format {
-        VertexFormat::PrimitiveInstances => DESC_PRIM_INSTANCES,
-        VertexFormat::Blur => DESC_BLUR,
+        VertexArrayKind::Primitive => DESC_PRIM_INSTANCES,
+        VertexArrayKind::Blur => DESC_BLUR,
+        VertexArrayKind::Clip => DESC_CLIP,
+        VertexArrayKind::CacheBoxShadow => DESC_CACHE_BOX_SHADOW,
     };
 
     let program = device.create_program(name,
                                         &prefix,
                                         &vertex_descriptor);
 
     if let Ok(ref program) = program {
         device.bind_shader_samplers(program, &[
@@ -1090,41 +1087,41 @@ impl Renderer {
         );
 
         register_thread_with_profiler("Compositor".to_owned());
 
         // device-pixel ratio doesn't matter here - we are just creating resources.
         device.begin_frame(1.0);
 
         let cs_box_shadow = try!{
-            LazilyCompiledShader::new(ShaderKind::Cache(VertexFormat::PrimitiveInstances),
+            LazilyCompiledShader::new(ShaderKind::Cache(VertexArrayKind::CacheBoxShadow),
                                       "cs_box_shadow",
                                       &[],
                                       &mut device,
                                       options.precache_shaders)
         };
 
         let cs_text_run = try!{
-            LazilyCompiledShader::new(ShaderKind::Cache(VertexFormat::PrimitiveInstances),
+            LazilyCompiledShader::new(ShaderKind::Cache(VertexArrayKind::Primitive),
                                       "cs_text_run",
                                       &[],
                                       &mut device,
                                       options.precache_shaders)
         };
 
         let cs_line = try!{
-            LazilyCompiledShader::new(ShaderKind::Cache(VertexFormat::PrimitiveInstances),
+            LazilyCompiledShader::new(ShaderKind::Cache(VertexArrayKind::Primitive),
                                       "ps_line",
                                       &["CACHE"],
                                       &mut device,
                                       options.precache_shaders)
         };
 
         let cs_blur = try!{
-            LazilyCompiledShader::new(ShaderKind::Cache(VertexFormat::Blur),
+            LazilyCompiledShader::new(ShaderKind::Cache(VertexArrayKind::Blur),
                                      "cs_blur",
                                       &[],
                                       &mut device,
                                       options.precache_shaders)
         };
 
         let cs_clip_rectangle = try!{
             LazilyCompiledShader::new(ShaderKind::ClipCache,
diff --git a/gfx/webrender/src/tiling.rs b/gfx/webrender/src/tiling.rs
--- a/gfx/webrender/src/tiling.rs
+++ b/gfx/webrender/src/tiling.rs
@@ -1011,16 +1011,17 @@ impl RenderTarget for ColorRenderTarget 
                                 PrimitiveKind::TextRun => {
                                     // Add instances that reference the text run GPU location. Also supply
                                     // the parent text-shadow prim address as a user data field, allowing
                                     // the shader to fetch the text-shadow parameters.
                                     let text = &ctx.prim_store.cpu_text_runs[sub_metadata.cpu_prim_index.0];
 
                                     let mut font = text.font.clone();
                                     font.size = font.size.scale_by(ctx.device_pixel_ratio);
+                                    font.render_mode = text.shadow_render_mode;
 
                                     let texture_id = ctx.resource_cache.get_glyphs(font,
                                                                                    &text.glyph_keys,
                                                                                    |index, handle| {
                                         let uv_address = handle.as_int(gpu_cache);
                                         instances.push(instance.build(index as i32,
                                                                       uv_address,
                                                                       prim_address));
