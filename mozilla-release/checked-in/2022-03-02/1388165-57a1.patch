# HG changeset patch
# User Ting-Yu Lin <tlin@mozilla.com>
# Date 1503384385 -28800
#      Tue Aug 22 14:46:25 2017 +0800
# Node ID 9a93541800d3cf2e3c59362ae3d77c449db6f9d1
# Parent  210ddb51ef86e1e8329affce6f050b1cd5cb8cf7
Bug 1388165 - Clear mPresContext after creating XBL styleset. r=xidorn

MozReview-Commit-ID: VBmKMTt8lc

diff --git a/devtools/client/inspector/markup/test/browser.ini b/devtools/client/inspector/markup/test/browser.ini
--- a/devtools/client/inspector/markup/test/browser.ini
+++ b/devtools/client/inspector/markup/test/browser.ini
@@ -78,17 +78,16 @@ skip-if = os == "mac" # Full keyboard na
 skip-if = os == "mac" # Full keyboard navigation on OSX only works if Full Keyboard Access setting is set to All Control in System Keyboard Preferences
 [browser_markup_accessibility_semantics.js]
 [browser_markup_anonymous_01.js]
 [browser_markup_anonymous_02.js]
 skip-if = e10s # scratchpad.xul is not loading in e10s window
 [browser_markup_anonymous_03.js]
 skip-if = stylo # Stylo doesn't support shadow DOM yet, bug 1293844
 [browser_markup_anonymous_04.js]
-skip-if = stylo && debug # Bug 1386865 - Triggers assertion
 [browser_markup_copy_image_data.js]
 subsuite = clipboard
 skip-if = (os == 'linux' && bits == 32 && debug) # bug 1328915, disable linux32 debug devtools for timeouts
 [browser_markup_css_completion_style_attribute_01.js]
 [browser_markup_css_completion_style_attribute_02.js]
 [browser_markup_css_completion_style_attribute_03.js]
 [browser_markup_dragdrop_autoscroll_01.js]
 [browser_markup_dragdrop_autoscroll_02.js]
diff --git a/dom/xbl/nsXBLPrototypeResources.cpp b/dom/xbl/nsXBLPrototypeResources.cpp
--- a/dom/xbl/nsXBLPrototypeResources.cpp
+++ b/dom/xbl/nsXBLPrototypeResources.cpp
@@ -174,16 +174,21 @@ nsXBLPrototypeResources::ComputeServoSty
                "This should only be called with Servo-flavored style backend!");
     // The XBL style sheets aren't document level sheets, but we need to
     // decide a particular SheetType to add them to style set. This type
     // doesn't affect the place where we pull those rules from
     // stylist::push_applicable_declarations_as_xbl_only_stylist().
     mServoStyleSet->AppendStyleSheet(SheetType::Doc, sheet->AsServo());
   }
   mServoStyleSet->UpdateStylistIfNeeded();
+
+  // The PresContext of the bound document could be destroyed anytime later,
+  // which shouldn't be used for XBL styleset, so we clear it here to avoid
+  // dangling pointer.
+  mServoStyleSet->ClearPresContext();
 }
 
 void
 nsXBLPrototypeResources::AppendStyleSheet(StyleSheet* aSheet)
 {
   mStyleSheetList.AppendElement(aSheet);
 }
 
diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -1465,19 +1465,21 @@ ServoStyleSet::RunPostTraversalTasks()
   }
 }
 
 ServoStyleRuleMap*
 ServoStyleSet::StyleRuleMap()
 {
   if (!mStyleRuleMap) {
     mStyleRuleMap = new ServoStyleRuleMap(this);
-    nsIDocument* doc = mPresContext->Document();
-    doc->AddObserver(mStyleRuleMap);
-    doc->CSSLoader()->AddObserver(mStyleRuleMap);
+    if (mPresContext) {
+      nsIDocument* doc = mPresContext->Document();
+      doc->AddObserver(mStyleRuleMap);
+      doc->CSSLoader()->AddObserver(mStyleRuleMap);
+    }
   }
   return mStyleRuleMap;
 }
 
 bool
 ServoStyleSet::MightHaveAttributeDependency(const Element& aElement,
                                             nsIAtom* aAttribute) const
 {
diff --git a/layout/style/ServoStyleSet.h b/layout/style/ServoStyleSet.h
--- a/layout/style/ServoStyleSet.h
+++ b/layout/style/ServoStyleSet.h
@@ -419,16 +419,21 @@ public:
   // its inner.
   void SetNeedsRestyleAfterEnsureUniqueInner() {
     mNeedsRestyleAfterEnsureUniqueInner = true;
   }
 
   // Returns the style rule map.
   ServoStyleRuleMap* StyleRuleMap();
 
+  // Clear mPresContext. This is needed after XBL ServoStyleSet is created.
+  void ClearPresContext() {
+    mPresContext = nullptr;
+  }
+
   /**
    * Returns true if a modification to an an attribute with the specified
    * local name might require us to restyle the element.
    *
    * This function allows us to skip taking a an attribute snapshot when
    * the modified attribute doesn't appear in an attribute selector in
    * a style sheet.
    */
