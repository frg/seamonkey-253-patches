# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1486040196 -3600
#      Thu Feb 02 13:56:36 2017 +0100
# Node ID 293183f088dc120fc4ab1d0bdfcc8818eadd6d97
# Parent  9088ae2f3e481e7bd11430c701fc10ab942369c8
Bug 1277562 - Part 7: Tiering control logic. r=luke

diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -1875,17 +1875,17 @@ class MOZ_STACK_CLASS ModuleValidator
             !env->tables.resize(AsmJSMaxTables) ||
             !env->asmJSSigToTableIndex.resize(AsmJSMaxTypes))
         {
             return false;
         }
 
         env->minMemoryLength = RoundUpToNextValidAsmJSHeapLength(0);
 
-        if (!mg_.init(Move(env), args, asmJSMetadata_.get()))
+        if (!mg_.init(Move(env), args, CompileMode::Once, asmJSMetadata_.get()))
             return false;
 
         return true;
     }
 
     JSContext* cx() const                    { return cx_; }
     PropertyName* moduleFunctionName() const { return moduleFunctionName_; }
     PropertyName* globalArgumentName() const { return globalArgumentName_; }
diff --git a/js/src/wasm/WasmCompile.cpp b/js/src/wasm/WasmCompile.cpp
--- a/js/src/wasm/WasmCompile.cpp
+++ b/js/src/wasm/WasmCompile.cpp
@@ -15,16 +15,17 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
 
 #include "wasm/WasmCompile.h"
 
 #include "jsprf.h"
 
+#include "wasm/WasmBaselineCompile.h"
 #include "wasm/WasmBinaryIterator.h"
 #include "wasm/WasmGenerator.h"
 #include "wasm/WasmSignalHandlers.h"
 #include "wasm/WasmValidate.h"
 
 using namespace js;
 using namespace js::jit;
 using namespace js::wasm;
@@ -89,43 +90,107 @@ DecodeCodeSection(Decoder& d, ModuleGene
 
     return mg.finishFuncDefs();
 }
 
 bool
 CompileArgs::initFromContext(JSContext* cx, ScriptedCaller&& scriptedCaller)
 {
     baselineEnabled = cx->options().wasmBaseline();
+    ionEnabled = cx->options().ion();
 
     // Debug information such as source view or debug traps will require
     // additional memory and permanently stay in baseline code, so we try to
     // only enable it when a developer actually cares: when the debugger tab
     // is open.
     debugEnabled = cx->compartment()->debuggerObservesAsmJS();
 
     this->scriptedCaller = Move(scriptedCaller);
     return assumptions.initBuildIdFromContext(cx);
 }
 
+static void
+CompilerAvailability(ModuleKind kind, const CompileArgs& args, bool* baselineEnabled,
+                     bool* debugEnabled, bool* ionEnabled)
+{
+    bool baselinePossible = kind == ModuleKind::Wasm && BaselineCanCompile();
+    *baselineEnabled = baselinePossible && args.baselineEnabled;
+    *debugEnabled = baselinePossible && args.debugEnabled;
+    *ionEnabled = args.ionEnabled;
+
+    // Default to Ion if necessary: We will never get to this point on platforms
+    // that don't have Ion at all, so this can happen if the user has disabled
+    // both compilers or if she has disabled Ion but baseline can't compile the
+    // code.
+
+    if (!(*baselineEnabled || *ionEnabled))
+        *ionEnabled = true;
+}
+
+bool
+wasm::GetDebugEnabled(const CompileArgs& args, ModuleKind kind)
+{
+    bool baselineEnabled, debugEnabled, ionEnabled;
+    CompilerAvailability(kind, args, &baselineEnabled, &debugEnabled, &ionEnabled);
+
+    return debugEnabled;
+}
+
+wasm::CompileMode
+wasm::GetInitialCompileMode(const CompileArgs& args, ModuleKind kind)
+{
+    bool baselineEnabled, debugEnabled, ionEnabled;
+    CompilerAvailability(kind, args, &baselineEnabled, &debugEnabled, &ionEnabled);
+
+    return (baselineEnabled && ionEnabled && !debugEnabled)
+           ? CompileMode::Tier1
+           : CompileMode::Once;
+}
+
+wasm::Tier
+wasm::GetTier(const CompileArgs& args, CompileMode compileMode, ModuleKind kind)
+{
+    bool baselineEnabled, debugEnabled, ionEnabled;
+    CompilerAvailability(kind, args, &baselineEnabled, &debugEnabled, &ionEnabled);
+
+    switch (compileMode) {
+      case CompileMode::Tier1:
+        MOZ_ASSERT(baselineEnabled);
+        return Tier::Baseline;
+
+      case CompileMode::Tier2:
+        MOZ_ASSERT(ionEnabled);
+        return Tier::Ion;
+
+      case CompileMode::Once:
+        return (debugEnabled || !ionEnabled) ? Tier::Baseline : Tier::Ion;
+
+      default:
+        MOZ_CRASH("Bad mode");
+    }
+}
+
 SharedModule
 wasm::Compile(const ShareableBytes& bytecode, const CompileArgs& args, UniqueChars* error)
 {
     MOZ_RELEASE_ASSERT(wasm::HaveSignalHandlers());
 
     Decoder d(bytecode.bytes, error);
 
     auto env = js::MakeUnique<ModuleEnvironment>();
     if (!env)
         return nullptr;
 
     if (!DecodeModuleEnvironment(d, env.get()))
         return nullptr;
 
+    CompileMode compileMode = GetInitialCompileMode(args);
+
     ModuleGenerator mg(error);
-    if (!mg.init(Move(env), args))
+    if (!mg.init(Move(env), args, compileMode))
         return nullptr;
 
     if (!DecodeCodeSection(d, mg))
         return nullptr;
 
     if (!DecodeModuleTail(d, &mg.mutableEnv()))
         return nullptr;
 
diff --git a/js/src/wasm/WasmCompile.h b/js/src/wasm/WasmCompile.h
--- a/js/src/wasm/WasmCompile.h
+++ b/js/src/wasm/WasmCompile.h
@@ -36,22 +36,24 @@ struct ScriptedCaller
 // Describes all the parameters that control wasm compilation.
 
 struct CompileArgs
 {
     Assumptions assumptions;
     ScriptedCaller scriptedCaller;
     bool baselineEnabled;
     bool debugEnabled;
+    bool ionEnabled;
 
     CompileArgs(Assumptions&& assumptions, ScriptedCaller&& scriptedCaller)
       : assumptions(Move(assumptions)),
         scriptedCaller(Move(scriptedCaller)),
         baselineEnabled(false),
-        debugEnabled(false)
+        debugEnabled(false),
+        ionEnabled(false)
     {}
 
     // If CompileArgs is constructed without arguments, initFromContext() must
     // be called to complete initialization.
     CompileArgs() = default;
     bool initFromContext(JSContext* cx, ScriptedCaller&& scriptedCaller);
 };
 
@@ -59,12 +61,36 @@ struct CompileArgs
 // wasm::Module. On success, the Module is returned. On failure, the returned
 // SharedModule pointer is null and either:
 //  - *error points to a string description of the error
 //  - *error is null and the caller should report out-of-memory.
 
 SharedModule
 Compile(const ShareableBytes& bytecode, const CompileArgs& args, UniqueChars* error);
 
+// Select whether debugging is available based on the available compilers, the
+// configuration options, and the nature of the module.  Note debugging can be
+// unavailable even if selected, if Rabaldr is unavailable or the module is not
+// compilable by Rabaldr.
+
+bool
+GetDebugEnabled(const CompileArgs& args, ModuleKind kind = ModuleKind::Wasm);
+
+// Select the mode for the initial compilation of a module.  The mode is "Tier1"
+// precisely if both compilers are available and we're not debugging, and in
+// that case, we'll compile twice, with the mode set to "Tier2" during the
+// second compilation.  Otherwise, the tier is "Once" and we'll compile once,
+// with the appropriate compiler.
+
+CompileMode
+GetInitialCompileMode(const CompileArgs& args, ModuleKind kind = ModuleKind::Wasm);
+
+// Select the tier for a compilation.  The tier is Tier::Baseline if we're
+// debugging, if Baldr is not available, or if both compilers are are available
+// and the compileMode is Tier1; otherwise the tier is Tier::Ion.
+
+Tier
+GetTier(const CompileArgs& args, CompileMode compileMode, ModuleKind kind = ModuleKind::Wasm);
+
 }  // namespace wasm
 }  // namespace js
 
 #endif // namespace wasm_compile_h
diff --git a/js/src/wasm/WasmGenerator.cpp b/js/src/wasm/WasmGenerator.cpp
--- a/js/src/wasm/WasmGenerator.cpp
+++ b/js/src/wasm/WasmGenerator.cpp
@@ -42,16 +42,17 @@ using mozilla::MakeEnumeratedRange;
 // ModuleGenerator
 
 static const unsigned GENERATOR_LIFO_DEFAULT_CHUNK_SIZE = 4 * 1024;
 static const unsigned COMPILATION_LIFO_DEFAULT_CHUNK_SIZE = 64 * 1024;
 static const uint32_t BAD_CODE_RANGE = UINT32_MAX;
 
 ModuleGenerator::ModuleGenerator(UniqueChars* error)
   : tier_(Tier(-1)),
+    compileMode_(CompileMode::Once),
     error_(error),
     linkDataTier_(nullptr),
     metadataTier_(nullptr),
     numSigs_(0),
     numTables_(0),
     lifo_(GENERATOR_LIFO_DEFAULT_CHUNK_SIZE),
     masmAlloc_(&lifo_),
     masm_(MacroAssembler::WasmToken(), masmAlloc_),
@@ -136,21 +137,17 @@ ModuleGenerator::initAsmJS(Metadata* asm
     return true;
 }
 
 bool
 ModuleGenerator::initWasm(const CompileArgs& args)
 {
     MOZ_ASSERT(!env_->isAsmJS());
 
-    bool canBaseline = BaselineCanCompile();
-    bool debugEnabled = args.debugEnabled && canBaseline;
-    tier_ = ((args.baselineEnabled || debugEnabled) && canBaseline)
-            ? Tier::Baseline
-            : Tier::Ion;
+    tier_ = GetTier(args, compileMode_);
 
     auto metadataTier = js::MakeUnique<MetadataTier>(tier_);
     if (!metadataTier)
         return false;
 
     metadata_ = js_new<Metadata>(Move(metadataTier));
     if (!metadata_)
         return false;
@@ -158,17 +155,17 @@ ModuleGenerator::initWasm(const CompileA
     metadataTier_ = &metadata_->metadata(tier_);
 
     if (!linkData_.initTier1(tier_, *metadata_))
         return false;
     linkDataTier_ = &linkData_.linkData(tier_);
 
     MOZ_ASSERT(!isAsmJS());
 
-    metadata_->debugEnabled = debugEnabled;
+    metadata_->debugEnabled = GetDebugEnabled(args);
 
     // For wasm, the Vectors are correctly-sized and already initialized.
 
     numSigs_ = env_->sigs.length();
     numTables_ = env_->tables.length();
 
     for (size_t i = 0; i < env_->funcImportGlobalDataOffsets.length(); i++) {
         env_->funcImportGlobalDataOffsets[i] = metadata_->globalDataLength;
@@ -234,19 +231,20 @@ ModuleGenerator::initWasm(const CompileA
         }
     }
 
     return true;
 }
 
 bool
 ModuleGenerator::init(UniqueModuleEnvironment env, const CompileArgs& args,
-                      Metadata* maybeAsmJSMetadata)
+                      CompileMode compileMode, Metadata* maybeAsmJSMetadata)
 {
     env_ = Move(env);
+    compileMode_ = compileMode;
 
     if (!funcToCodeRange_.appendN(BAD_CODE_RANGE, env_->funcSigs.length()))
         return false;
 
     if (!assumptions_.clone(args.assumptions))
         return false;
 
     if (!exportedFuncs_.init())
@@ -974,16 +972,17 @@ ModuleGenerator::launchBatchCompile()
     numFinishedFuncDefs_ += numBatchedFuncs;
     return true;
 }
 
 bool
 ModuleGenerator::finishFuncDef(uint32_t funcIndex, FunctionGenerator* fg)
 {
     MOZ_ASSERT(activeFuncDef_ == fg);
+    MOZ_ASSERT_IF(compileMode_ == CompileMode::Tier1, funcIndex < env_->numFuncs());
 
     UniqueFuncBytes func = Move(fg->funcBytes_);
     func->setFunc(funcIndex, &funcSig(funcIndex));
     uint32_t funcBytecodeLength = func->bytes().length();
     if (!currentTask_->units().emplaceBack(Move(func)))
         return false;
 
     uint32_t threshold;
diff --git a/js/src/wasm/WasmGenerator.h b/js/src/wasm/WasmGenerator.h
--- a/js/src/wasm/WasmGenerator.h
+++ b/js/src/wasm/WasmGenerator.h
@@ -15,16 +15,17 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
 
 #ifndef wasm_generator_h
 #define wasm_generator_h
 
 #include "jit/MacroAssembler.h"
+#include "wasm/WasmCompile.h"
 #include "wasm/WasmModule.h"
 #include "wasm/WasmValidate.h"
 
 namespace js {
 namespace wasm {
 
 struct ModuleEnvironment;
 
@@ -205,16 +206,17 @@ class MOZ_STACK_CLASS ModuleGenerator
 {
     typedef HashSet<uint32_t, DefaultHasher<uint32_t>, SystemAllocPolicy> Uint32Set;
     typedef Vector<CompileTask, 0, SystemAllocPolicy> CompileTaskVector;
     typedef Vector<CompileTask*, 0, SystemAllocPolicy> CompileTaskPtrVector;
     typedef EnumeratedArray<Trap, Trap::Limit, CallableOffsets> TrapExitOffsetArray;
 
     // Constant parameters
     Tier                            tier_;
+    CompileMode                     compileMode_;
     UniqueChars*                    error_;
 
     // Data that is moved into the result of finish()
     Assumptions                     assumptions_;
     LinkDataTier*                   linkDataTier_; // Owned by linkData_
     LinkData                        linkData_;
     MetadataTier*                   metadataTier_; // Owned by metadata_
     MutableMetadata                 metadata_;
@@ -270,22 +272,25 @@ class MOZ_STACK_CLASS ModuleGenerator
     MOZ_MUST_USE bool initAsmJS(Metadata* asmJSMetadata);
     MOZ_MUST_USE bool initWasm(const CompileArgs& args);
 
   public:
     explicit ModuleGenerator(UniqueChars* error);
     ~ModuleGenerator();
 
     MOZ_MUST_USE bool init(UniqueModuleEnvironment env, const CompileArgs& args,
+                           CompileMode compileMode = CompileMode::Once,
                            Metadata* maybeAsmJSMetadata = nullptr);
 
     const ModuleEnvironment& env() const { return *env_; }
     ModuleEnvironment& mutableEnv();
 
     bool isAsmJS() const { return metadata_->kind == ModuleKind::AsmJS; }
+    CompileMode mode() const { return compileMode_; }
+    Tier tier() const { return tier_; }
     jit::MacroAssembler& masm() { return masm_; }
 
     // Memory:
     bool usesMemory() const { return env_->usesMemory(); }
     uint32_t minMemoryLength() const { return env_->minMemoryLength; }
 
     // Tables:
     uint32_t numTables() const { return numTables_; }
diff --git a/js/src/wasm/WasmTypes.h b/js/src/wasm/WasmTypes.h
--- a/js/src/wasm/WasmTypes.h
+++ b/js/src/wasm/WasmTypes.h
@@ -359,16 +359,26 @@ enum class Tier
     Baseline,
     Debug = Baseline,
     Ion,
     Serialized = Ion,
 
     TBD      // A placeholder while tiering is being implemented};
 };
 
+// The CompileMode controls how compilation of a module is performed (notably,
+// how many times we compile it).
+
+enum class CompileMode
+{
+    Once,
+    Tier1,
+    Tier2
+};
+
 // Iterator over tiers present in a tiered data structure.
 
 class Tiers
 {
     Tier t_[2];
     uint32_t n_;
 
   public:
