# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1503306201 -32400
#      Mon Aug 21 18:03:21 2017 +0900
# Node ID 08c538a185408e3f79fc7a0a08fba042ebebc689
# Parent  6efad7981f2db38a060430a25c600f6893ecf8c4
Bug 1392181 - part2: HasRTLChars() should check if the character is at least equal or larger than the minimum RTL character, U+0590 r=emk

HasRTLChars() appears in profile of attachment 8848015 after landing the patches
for bug 1391538 because the landing made text in <input> or <textarea> always
treated as non-single byte characters.  Therefore, HasRTLChars() is now called
by nsTextFragment::SetTo() a lot in editors.

HasRTLChar() checks if it's in an RTL character for each character until it
becomes true.  However, if character is less than the minimum RTL character,
U+0590, it works faster at least for Western languages.

MozReview-Commit-ID: 4YecxQWUcmK

diff --git a/intl/unicharutil/util/nsBidiUtils.cpp b/intl/unicharutil/util/nsBidiUtils.cpp
--- a/intl/unicharutil/util/nsBidiUtils.cpp
+++ b/intl/unicharutil/util/nsBidiUtils.cpp
@@ -1,15 +1,19 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
  *
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #include "nsBidiUtils.h"
 
+namespace mozilla {
+static const uint32_t kMinRTLChar = 0x0590;
+} // namespace mozilla;
+
 #define ARABIC_TO_HINDI_DIGIT_INCREMENT (START_HINDI_DIGITS - START_ARABIC_DIGITS)
 #define PERSIAN_TO_HINDI_DIGIT_INCREMENT (START_HINDI_DIGITS - START_FARSI_DIGITS)
 #define ARABIC_TO_PERSIAN_DIGIT_INCREMENT (START_FARSI_DIGITS - START_ARABIC_DIGITS)
 #define NUM_TO_ARABIC(c) \
   ((((c)>=START_HINDI_DIGITS) && ((c)<=END_HINDI_DIGITS)) ? \
    ((c) - (uint16_t)ARABIC_TO_HINDI_DIGIT_INCREMENT) : \
    ((((c)>=START_FARSI_DIGITS) && ((c)<=END_FARSI_DIGITS)) ? \
     ((c) - (uint16_t)ARABIC_TO_PERSIAN_DIGIT_INCREMENT) : \
@@ -85,16 +89,19 @@ nsresult HandleNumbers(char16_t* aBuffer
 bool HasRTLChars(const char16_t* aText, uint32_t aLength)
 {
   // This is used to determine whether a string has right-to-left characters
   // that mean it will require bidi processing.
   const char16_t* cp = aText;
   const char16_t* end = cp + aLength;
   while (cp < end) {
     uint32_t ch = *cp++;
+    if (ch < mozilla::kMinRTLChar) {
+      continue;
+    }
     if (NS_IS_HIGH_SURROGATE(ch) && cp < end && NS_IS_LOW_SURROGATE(*cp)) {
       ch = SURROGATE_TO_UCS4(ch, *cp++);
     }
     if (UTF32_CHAR_IS_BIDI(ch) || IsBidiControlRTL(ch)) {
       return true;
     }
   }
   return false;
