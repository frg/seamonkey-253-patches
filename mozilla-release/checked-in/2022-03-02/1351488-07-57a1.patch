# HG changeset patch
# User Luke Wagner <luke@mozilla.com> and Jan Varga <janv@mozilla.com>
# Date 1504022209 18000
#      Tue Aug 29 10:56:49 2017 -0500
# Node ID 55547791ee99e01e0a4f3cb8974141c2c4af32e7
# Parent  08d40ce6531e2f46695b1d9f0e824464cc51cd73
Bug 1351488 - Add async notification API for wasm compilation and async stream using it (r=janv,luke,asuth)

MozReview-Commit-ID: K0MOcxS8CMT

diff --git a/dom/file/moz.build b/dom/file/moz.build
--- a/dom/file/moz.build
+++ b/dom/file/moz.build
@@ -30,16 +30,17 @@ EXPORTS.mozilla.dom += [
     'FileBlobImpl.h',
     'FileCreatorHelper.h',
     'FileList.h',
     'FileReader.h',
     'MemoryBlobImpl.h',
     'MultipartBlobImpl.h',
     'MutableBlobStorage.h',
     'MutableBlobStreamListener.h',
+    'StreamBlobImpl.h',
 ]
 
 UNIFIED_SOURCES += [
     'BaseBlobImpl.cpp',
     'Blob.cpp',
     'BlobImpl.cpp',
     'BlobSet.cpp',
     'EmptyBlobImpl.cpp',
diff --git a/dom/indexedDB/IDBObjectStore.cpp b/dom/indexedDB/IDBObjectStore.cpp
--- a/dom/indexedDB/IDBObjectStore.cpp
+++ b/dom/indexedDB/IDBObjectStore.cpp
@@ -30,23 +30,26 @@
 #include "mozilla/dom/ContentChild.h"
 #include "mozilla/dom/ContentParent.h"
 #include "mozilla/dom/DOMStringList.h"
 #include "mozilla/dom/File.h"
 #include "mozilla/dom/IDBMutableFileBinding.h"
 #include "mozilla/dom/BlobBinding.h"
 #include "mozilla/dom/IDBObjectStoreBinding.h"
 #include "mozilla/dom/MemoryBlobImpl.h"
+#include "mozilla/dom/StreamBlobImpl.h"
 #include "mozilla/dom/StructuredCloneHolder.h"
 #include "mozilla/dom/StructuredCloneTags.h"
 #include "mozilla/dom/indexedDB/PBackgroundIDBSharedTypes.h"
 #include "mozilla/ipc/BackgroundChild.h"
 #include "mozilla/ipc/PBackgroundSharedTypes.h"
 #include "nsCOMPtr.h"
 #include "nsQueryObject.h"
+#include "nsStreamUtils.h"
+#include "nsStringStream.h"
 #include "ProfilerHelpers.h"
 #include "ReportInternalError.h"
 #include "WorkerPrivate.h"
 #include "WorkerScope.h"
 
 // Include this last to avoid path problems on Windows.
 #include "ActorsChild.h"
 
@@ -184,16 +187,298 @@ GenerateRequest(JSContext* aCx, IDBObjec
 
   RefPtr<IDBRequest> request =
     IDBRequest::Create(aCx, aObjectStore, transaction->Database(), transaction);
   MOZ_ASSERT(request);
 
   return request.forget();
 }
 
+// Blob internal code clones this stream before it's passed to IPC, so we
+// serialize module's optimized code only when AsyncWait() is called.
+class WasmCompiledModuleStream final
+  : public nsIAsyncInputStream
+  , public nsICloneableInputStream
+  , private JS::WasmModuleListener
+{
+  nsCOMPtr<nsISerialEventTarget> mOwningThread;
+
+  // Initially and while waiting for compilation to complete, the mModule field
+  // is non-null, keeping alive the module whose code needs to be serialized.
+  RefPtr<JS::WasmModule> mModule;
+
+  // A module stream can have up to one input stream callback.
+  nsCOMPtr<nsIInputStreamCallback> mCallback;
+
+  // When a module's optimized code is ready to be serialized, it will be
+  // serialized into mStream and the mModule will be released. At this point,
+  // stream reads will succeed.
+  nsCOMPtr<nsIInputStream> mStream;
+
+  // When the stream is finished reading or closed, mStatus will be changed from
+  // NS_OK to NS_BASE_STREAM_CLOSED or whatever status was passed to
+  // CloseWithStatus.
+  nsresult mStatus;
+
+public:
+  explicit WasmCompiledModuleStream(JS::WasmModule* aModule)
+    : mOwningThread(GetCurrentThreadSerialEventTarget())
+    , mModule(aModule)
+    , mStatus(NS_OK)
+  {
+    AssertIsOnOwningThread();
+    MOZ_ASSERT(aModule);
+  }
+
+  bool
+  IsOnOwningThread() const
+  {
+    MOZ_ASSERT(mOwningThread);
+
+    bool current;
+    return NS_SUCCEEDED(mOwningThread->IsOnCurrentThread(&current)) && current;
+  }
+
+  void
+  AssertIsOnOwningThread() const
+  {
+    MOZ_ASSERT(IsOnOwningThread());
+  }
+
+private:
+  // Copy constructor for cloning.
+  explicit WasmCompiledModuleStream(const WasmCompiledModuleStream& aOther)
+    : mOwningThread(aOther.mOwningThread)
+    , mModule(aOther.mModule)
+    , mStatus(aOther.mStatus)
+  {
+    AssertIsOnOwningThread();
+
+    if (aOther.mStream) {
+      nsCOMPtr<nsICloneableInputStream> cloneableStream =
+        do_QueryInterface(aOther.mStream);
+      MOZ_ASSERT(cloneableStream);
+
+      MOZ_ALWAYS_SUCCEEDS(cloneableStream->Clone(getter_AddRefs(mStream)));
+    }
+  }
+
+  ~WasmCompiledModuleStream() override
+  {
+    AssertIsOnOwningThread();
+
+    MOZ_ALWAYS_SUCCEEDS(Close());
+  }
+
+  void
+  CallCallback()
+  {
+    AssertIsOnOwningThread();
+    MOZ_ASSERT(mCallback);
+
+    nsCOMPtr<nsIInputStreamCallback> callback;
+    callback.swap(mCallback);
+
+    callback->OnInputStreamReady(this);
+  }
+
+  NS_DECL_THREADSAFE_ISUPPORTS
+
+  // nsIInputStream:
+
+  NS_IMETHOD
+  Close() override
+  {
+    AssertIsOnOwningThread();
+
+    return CloseWithStatus(NS_BASE_STREAM_CLOSED);
+  }
+
+  NS_IMETHOD
+  Available(uint64_t* _retval) override
+  {
+    AssertIsOnOwningThread();
+
+    if (NS_FAILED(mStatus)) {
+      return mStatus;
+    }
+
+    if (!mStream) {
+      *_retval = 0;
+      return NS_OK;
+    }
+
+    return mStream->Available(_retval);
+  }
+
+  NS_IMETHOD
+  Read(char* aBuf, uint32_t aCount, uint32_t* _retval) override
+  {
+    AssertIsOnOwningThread();
+
+    return ReadSegments(NS_CopySegmentToBuffer, aBuf, aCount, _retval);
+  }
+
+  NS_IMETHOD
+  ReadSegments(nsWriteSegmentFun aWriter,
+               void* aClosure,
+               uint32_t aCount,
+               uint32_t* _retval) override
+  {
+    AssertIsOnOwningThread();
+
+    if (NS_FAILED(mStatus)) {
+      *_retval = 0;
+      return NS_OK;
+    }
+
+    if (!mStream) {
+      return NS_BASE_STREAM_WOULD_BLOCK;
+    }
+
+    return mStream->ReadSegments(aWriter, aClosure, aCount, _retval);
+  }
+
+  NS_IMETHOD
+  IsNonBlocking(bool* _retval) override
+  {
+    AssertIsOnOwningThread();
+
+    *_retval = true;
+    return NS_OK;
+  }
+
+  // nsIAsyncInputStream:
+
+  NS_IMETHOD
+  CloseWithStatus(nsresult aStatus) override
+  {
+    AssertIsOnOwningThread();
+
+    if (NS_FAILED(mStatus)) {
+      return NS_OK;
+    }
+
+    mModule = nullptr;
+
+    if (mStream) {
+      MOZ_ALWAYS_SUCCEEDS(mStream->Close());
+      mStream = nullptr;
+    }
+
+    mStatus = NS_FAILED(aStatus) ? aStatus : NS_BASE_STREAM_CLOSED;
+
+    if (mCallback) {
+      CallCallback();
+    }
+
+    return NS_OK;
+  }
+
+  NS_IMETHOD
+  AsyncWait(nsIInputStreamCallback* aCallback,
+            uint32_t aFlags,
+            uint32_t aRequestedCount,
+            nsIEventTarget* aEventTarget) override
+  {
+    AssertIsOnOwningThread();
+    MOZ_ASSERT_IF(mCallback, !aCallback);
+
+    if (aFlags) {
+      return NS_ERROR_NOT_IMPLEMENTED;
+    }
+
+    if (!aCallback) {
+      mCallback = nullptr;
+      return NS_OK;
+    }
+
+    if (aEventTarget) {
+      mCallback =
+        NS_NewInputStreamReadyEvent("WasmCompiledModuleStream::AsyncWait",
+                                    aCallback,
+                                    aEventTarget);
+    } else {
+      mCallback = aCallback;
+    }
+
+    if (NS_FAILED(mStatus) || mStream) {
+      CallCallback();
+      return NS_OK;
+    }
+
+    MOZ_ASSERT(mModule);
+    mModule->notifyWhenCompilationComplete(this);
+
+    return NS_OK;
+  }
+
+  // nsICloneableInputStream:
+
+  NS_IMETHOD
+  GetCloneable(bool* aCloneable) override
+  {
+    AssertIsOnOwningThread();
+
+    *aCloneable = true;
+    return NS_OK;
+  }
+
+  NS_IMETHOD
+  Clone(nsIInputStream** _retval) override
+  {
+    AssertIsOnOwningThread();
+
+    nsCOMPtr<nsIInputStream> clone = new WasmCompiledModuleStream(*this);
+
+    clone.forget(_retval);
+    return NS_OK;
+  }
+
+  // JS::WasmModuleListener:
+
+  void
+  onCompilationComplete() override
+  {
+    if (!IsOnOwningThread()) {
+      MOZ_ALWAYS_SUCCEEDS(mOwningThread->Dispatch(NewCancelableRunnableMethod(
+        "WasmCompiledModuleStream::onCompilationComplete",
+        this,
+        &WasmCompiledModuleStream::onCompilationComplete)));
+      return;
+    }
+
+    if (NS_FAILED(mStatus) || !mCallback) {
+      return;
+    }
+
+    MOZ_ASSERT(mModule);
+
+    size_t compiledSize = mModule->compiledSerializedSize();
+
+    nsCString compiled;
+    compiled.SetLength(compiledSize);
+
+    mModule->compiledSerialize(
+      reinterpret_cast<uint8_t*>(compiled.BeginWriting()), compiledSize);
+
+    MOZ_ALWAYS_SUCCEEDS(NS_NewCStringInputStream(getter_AddRefs(mStream),
+                                                 compiled));
+
+    mModule = nullptr;
+
+    CallCallback();
+  }
+};
+
+NS_IMPL_ISUPPORTS(WasmCompiledModuleStream,
+                  nsIInputStream,
+                  nsIAsyncInputStream,
+                  nsICloneableInputStream)
+
 bool
 StructuredCloneWriteCallback(JSContext* aCx,
                              JSStructuredCloneWriter* aWriter,
                              JS::Handle<JSObject*> aObj,
                              void* aClosure)
 {
   MOZ_ASSERT(aCx);
   MOZ_ASSERT(aWriter);
@@ -346,30 +631,36 @@ StructuredCloneWriteCallback(JSContext* 
   }
 
   if (JS::IsWasmModuleObject(aObj)) {
     RefPtr<JS::WasmModule> module = JS::GetWasmModule(aObj);
     MOZ_ASSERT(module);
 
     size_t bytecodeSize = module->bytecodeSerializedSize();
     UniquePtr<uint8_t[]> bytecode(new uint8_t[bytecodeSize]);
-    MOZ_ASSERT(bytecode);
     module->bytecodeSerialize(bytecode.get(), bytecodeSize);
 
     RefPtr<BlobImpl> blobImpl =
       new MemoryBlobImpl(bytecode.release(), bytecodeSize, EmptyString());
+
     RefPtr<Blob> bytecodeBlob = Blob::Create(nullptr, blobImpl);
 
-    size_t compiledSize = module->compiledSerializedSize();
-    UniquePtr<uint8_t[]> compiled(new uint8_t[compiledSize]);
-    MOZ_ASSERT(compiled);
-    module->compiledSerialize(compiled.get(), compiledSize);
-
-    blobImpl =
-      new MemoryBlobImpl(compiled.release(), compiledSize, EmptyString());
+    if (module->compilationComplete()) {
+      size_t compiledSize = module->compiledSerializedSize();
+      UniquePtr<uint8_t[]> compiled(new uint8_t[compiledSize]);
+      module->compiledSerialize(compiled.get(), compiledSize);
+
+      blobImpl =
+        new MemoryBlobImpl(compiled.release(), compiledSize, EmptyString());
+    } else {
+      nsCOMPtr<nsIInputStream> stream(new WasmCompiledModuleStream(module));
+
+      blobImpl = StreamBlobImpl::Create(stream, EmptyString(), UINT64_MAX);
+    }
+
     RefPtr<Blob> compiledBlob = Blob::Create(nullptr, blobImpl);
 
     if (cloneWriteInfo->mFiles.Length() + 1 > size_t(UINT32_MAX)) {
       MOZ_ASSERT(false, "Fix the structured clone data to use a bigger type!");
       return false;
     }
 
     const uint32_t index = cloneWriteInfo->mFiles.Length();
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -6504,18 +6504,17 @@ typedef bool
 (* BuildIdOp)(BuildIdCharVector* buildId);
 
 extern JS_PUBLIC_API(void)
 SetBuildIdOp(JSContext* cx, BuildIdOp buildIdOp);
 
 /**
  * The WasmModule interface allows the embedding to hold a reference to the
  * underying C++ implementation of a JS WebAssembly.Module object for purposes
- * of efficient postMessage() of WebAssembly.Module and (de)serialization off
- * the object's JSRuntime's thread for IndexedDB.
+ * of efficient postMessage() and (de)serialization from a random thread.
  *
  * For postMessage() sharing:
  *
  * - GetWasmModule() is called when making a structured clone of payload
  * containing a WebAssembly.Module object. The structured clone buffer holds a
  * refcount of the JS::WasmModule until createObject() is called in the target
  * agent's JSContext. The new WebAssembly.Module object continues to hold the
  * JS::WasmModule and thus the final reference of a JS::WasmModule may be
@@ -6526,37 +6525,60 @@ SetBuildIdOp(JSContext* cx, BuildIdOp bu
  *
  * - Serialization starts when WebAssembly.Module is passed to the
  * structured-clone algorithm. JS::GetWasmModule is called on the JSRuntime
  * thread that initiated the structured clone to get the JS::WasmModule.
  * This interface is then taken to a background thread where the bytecode and
  * compiled code are written into separate files: a bytecode file that always
  * allows successful deserialization and a compiled-code file keyed on cpu- and
  * build-id that may become invalid if either of these change between
- * serialization and deserialization. After serialization, a reference is
- * dropped from a separate thread so the virtual destructor must be thread-safe.
+ * serialization and deserialization. Due to tiering, the serialization must
+ * asynchronously wait for compilation to complete before requesting the
+ * module's compiled code. After serialization, a reference is dropped from a
+ * separate thread so the virtual destructor must be thread-safe.
  *
  * - Deserialization starts when the structured clone algorithm encounters a
  * serialized WebAssembly.Module. On a background thread, the compiled-code file
  * is opened and CompiledWasmModuleAssumptionsMatch is called to see if it is
  * still valid (as described above). DeserializeWasmModule is then called to
  * construct a JS::WasmModule (also on the background thread), passing the
  * bytecode file descriptor and, if valid, the compiled-code file descriptor.
- * The JS::WasmObject is then transported to the JSRuntime thread (which
- * originated the request) and the wrapping WebAssembly.Module object is created
- * by calling createObject().
- */
+ * The JS::WasmObject is then transported to a JSContext thread and the wrapping
+ * WebAssembly.Module object is created by calling createObject().
+ */
+
+class WasmModuleListener
+{
+  protected:
+    virtual ~WasmModuleListener() {}
+
+  public:
+    // These method signatures are chosen to exactly match nsISupports so that a
+    // plain nsISupports-implementing class can trivially implement this
+    // interface too. We can't simply #include "nsISupports.h" so we use MFBT
+    // equivalents for all the platform-dependent types.
+    virtual MozExternalRefCountType MOZ_XPCOM_ABI AddRef() = 0;
+    virtual MozExternalRefCountType MOZ_XPCOM_ABI Release() = 0;
+
+    virtual void onCompilationComplete() = 0;
+};
 
 struct WasmModule : js::AtomicRefCounted<WasmModule>
 {
     virtual ~WasmModule() {}
 
     virtual size_t bytecodeSerializedSize() const = 0;
     virtual void bytecodeSerialize(uint8_t* bytecodeBegin, size_t bytecodeSize) const = 0;
 
+    // Compilation must complete before the serialized code is requested. If
+    // compilation is not complete, the embedding must wait until notified by
+    // implementing WasmModuleListener. SpiderMonkey will hold a RefPtr to
+    // 'listener' until onCompilationComplete() is called.
+    virtual bool compilationComplete() const = 0;
+    virtual bool notifyWhenCompilationComplete(WasmModuleListener* listener) = 0;
     virtual size_t compiledSerializedSize() const = 0;
     virtual void compiledSerialize(uint8_t* compiledBegin, size_t compiledSize) const = 0;
 
     virtual JSObject* createObject(JSContext* cx) = 0;
 };
 
 extern JS_PUBLIC_API(bool)
 IsWasmModuleObject(HandleObject obj);
diff --git a/js/src/wasm/WasmModule.cpp b/js/src/wasm/WasmModule.cpp
--- a/js/src/wasm/WasmModule.cpp
+++ b/js/src/wasm/WasmModule.cpp
@@ -333,16 +333,28 @@ Module::bytecodeSerialize(uint8_t* bytec
     // change incompatibly between builds. For simplicity, the format of the
     // bytecode file is a .wasm file which ensures backwards compatibility.
 
     const Bytes& bytes = bytecode_->bytes;
     uint8_t* bytecodeEnd = WriteBytes(bytecodeBegin, bytes.begin(), bytes.length());
     MOZ_RELEASE_ASSERT(bytecodeEnd == bytecodeBegin + bytecodeSize);
 }
 
+/* virtual */ bool
+Module::compilationComplete() const
+{
+    return true;
+}
+
+/* virtual */ bool
+Module::notifyWhenCompilationComplete(JS::WasmModuleListener* listener)
+{
+    MOZ_CRASH("unreachable");
+}
+
 /* virtual */ size_t
 Module::compiledSerializedSize() const
 {
     // The compiled debug code must not be saved, set compiled size to 0,
     // so Module::assumptionsMatch will return false during assumptions
     // deserialization.
     if (metadata().debugEnabled)
         return 0;
diff --git a/js/src/wasm/WasmModule.h b/js/src/wasm/WasmModule.h
--- a/js/src/wasm/WasmModule.h
+++ b/js/src/wasm/WasmModule.h
@@ -235,20 +235,22 @@ class Module : public JS::WasmModule
 
     void blockOnIonCompileFinished() const;
 
     // Signal all waiters that are waiting on tier-2 compilation to be done that
     // they should wake up.  They will be waiting in blockOnIonCompileFinished.
 
     void unblockOnTier2GeneratorFinished(CompileMode newMode) const;
 
-    // Structured clone support:
+    // JS API and JS::WasmModule implementation:
 
     size_t bytecodeSerializedSize() const override;
     void bytecodeSerialize(uint8_t* bytecodeBegin, size_t bytecodeSize) const override;
+    bool compilationComplete() const override;
+    bool notifyWhenCompilationComplete(JS::WasmModuleListener* listener) override;
     size_t compiledSerializedSize() const override;
     void compiledSerialize(uint8_t* compiledBegin, size_t compiledSize) const override;
 
     static bool assumptionsMatch(const Assumptions& current, const uint8_t* compiledBegin,
                                  size_t remain);
     static RefPtr<Module> deserialize(const uint8_t* bytecodeBegin, size_t bytecodeSize,
                                       const uint8_t* compiledBegin, size_t compiledSize,
                                       Metadata* maybeMetadata = nullptr);
