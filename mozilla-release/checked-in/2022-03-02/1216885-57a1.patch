# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1505168362 25200
#      Mon Sep 11 15:19:22 2017 -0700
# Node ID 1ca25d95e1aac287652580b29e3220201c1d6086
# Parent  19dec38b37da506e686556f3272143f1aad4d730
Bug 1216885 - Make nsISelectionPrivate not inherit from nsISelection. r=smaug

nsISelectionPrivate is accessible to script, while nsISelection is
not, so making the former inherit from the latter means script doesn't
have a complete view of the inheritance chain so the XPIDL compiler
produces an error.

It turns out that nothing in script relies on this inheritance, which
makes sense because I'm not sure how it would even work, so just
remove it.

MozReview-Commit-ID: 3Py2T7cprlD

diff --git a/dom/base/Selection.h b/dom/base/Selection.h
--- a/dom/base/Selection.h
+++ b/dom/base/Selection.h
@@ -51,29 +51,30 @@ struct RangeData
 // Note, the ownership of mozilla::dom::Selection depends on which way the
 // object is created. When nsFrameSelection has created Selection,
 // addreffing/releasing the Selection object is aggregated to nsFrameSelection.
 // Otherwise normal addref/release is used.  This ensures that nsFrameSelection
 // is never deleted before its Selections.
 namespace mozilla {
 namespace dom {
 
-class Selection final : public nsISelectionPrivate,
+class Selection final : public nsISelection,
                         public nsWrapperCache,
+                        public nsISelectionPrivate,
                         public nsSupportsWeakReference
 {
 protected:
   virtual ~Selection();
 
 public:
   Selection();
   explicit Selection(nsFrameSelection *aList);
 
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
-  NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS_AMBIGUOUS(Selection, nsISelectionPrivate)
+  NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS_AMBIGUOUS(Selection, nsISelection)
   NS_DECL_NSISELECTION
   NS_DECL_NSISELECTIONPRIVATE
 
   // match this up with EndbatchChanges. will stop ui updates while multiple
   // selection methods are called
   void StartBatchChanges();
 
   // match this up with StartBatchChanges
diff --git a/dom/base/nsISelectionPrivate.idl b/dom/base/nsISelectionPrivate.idl
--- a/dom/base/nsISelectionPrivate.idl
+++ b/dom/base/nsISelectionPrivate.idl
@@ -23,17 +23,17 @@ struct ScrollAxis;
 
 [ptr] native nsIFrame(nsIFrame);
 [ptr] native RangeArray(nsTArray<nsRange*>);
 [ref] native nsPointRef(nsPoint);
 native nsDirection(nsDirection);
 native ScrollAxis(nsIPresShell::ScrollAxis);
 
 [scriptable, builtinclass, uuid(0c9f4f74-ee7e-4fe9-be6b-0ba856368178)]
-interface nsISelectionPrivate : nsISelection
+interface nsISelectionPrivate : nsISupports
  {
     const short ENDOFPRECEDINGLINE=0;
     const short STARTOFNEXTLINE=1;
 
     attribute boolean interlinePosition;
     [noscript] attribute nsIContent ancestorLimiter;
 
     DOMString  toStringWithFormat(in string formatType, in unsigned long flags, in int32_t wrapColumn);
