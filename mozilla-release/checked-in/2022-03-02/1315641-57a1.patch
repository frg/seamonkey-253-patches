# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1503655753 -7200
# Node ID 711cba275460ece6b1c24b8d383a78def7dca7f8
# Parent  7db07f935d3b134cb5ea22514616073fb3ea2e48
Bug 1315641 - Align error messages for (Typed)Array.from with standard iteration error messages. r=evilpie

diff --git a/js/src/builtin/Array.js b/js/src/builtin/Array.js
--- a/js/src/builtin/Array.js
+++ b/js/src/builtin/Array.js
@@ -800,31 +800,37 @@ function ArrayFrom(items, mapfn = undefi
 
     // Steps 2-3.
     var mapping = mapfn !== undefined;
     if (mapping && !IsCallable(mapfn))
         ThrowTypeError(JSMSG_NOT_FUNCTION, DecompileArg(1, mapfn));
     var T = thisArg;
 
     // Step 4.
-    var usingIterator = GetMethod(items, std_iterator);
+    // Inlined: GetMethod, steps 1-2.
+    var usingIterator = items[std_iterator];
 
     // Step 5.
-    if (usingIterator !== undefined) {
-        // Steps 5.a-c.
+    // Inlined: GetMethod, step 3.
+    if (usingIterator !== undefined && usingIterator !== null) {
+        // Inlined: GetMethod, step 4.
+        if (!IsCallable(usingIterator))
+            ThrowTypeError(JSMSG_NOT_ITERABLE, DecompileArg(0, items));
+
+        // Steps 5.a-b.
         var A = IsConstructor(C) ? new C() : [];
 
+        // Step 5.c.
+        var iterator = MakeIteratorWrapper(items, usingIterator);
+
         // Step 5.d.
         var k = 0;
 
-        // Step 5.c, 5.e.
-        var iterator = GetIterator(items, usingIterator);
-
-        var iteratorWrapper = MakeIteratorWrapper(iterator);
-        for (var nextValue of allowContentIter(iteratorWrapper)) {
+        // Step 5.e
+        for (var nextValue of allowContentIter(iterator)) {
             // Step 5.e.i.
             // Disabled for performance reason.  We won't hit this case on
             // normal array, since _DefineDataProperty will throw before it.
             // We could hit this when |A| is a proxy and it ignores
             // |_DefineDataProperty|, but it happens only after too long loop.
             /*
             if (k >= 0x1fffffffffffff)
                 ThrowTypeError(JSMSG_TOO_LONG_ARRAY);
@@ -837,18 +843,18 @@ function ArrayFrom(items, mapfn = undefi
             _DefineDataProperty(A, k++, mappedValue);
         }
 
         // Step 5.e.iv.
         A.length = k;
         return A;
     }
 
-    // Step 7.
-    assert(usingIterator === undefined, "`items` can't be an Iterable after step 6.g.iv");
+    // Step 7 is an assertion: items is not an Iterator. Testing this is
+    // literally the very last thing we did, so we don't assert here.
 
     // Steps 8-9.
     var arrayLike = ToObject(items);
 
     // Steps 10-11.
     var len = ToLength(arrayLike.length);
 
     // Steps 12-14.
@@ -868,25 +874,27 @@ function ArrayFrom(items, mapfn = undefi
 
     // Steps 17-18.
     A.length = len;
 
     // Step 19.
     return A;
 }
 
-function MakeIteratorWrapper(iterator) {
+function MakeIteratorWrapper(items, method) {
+    assert(IsCallable(method), "method argument is a function");
+
     // This function is not inlined in ArrayFrom, because function default
     // parameters combined with nested functions are currently not optimized
     // correctly.
     return {
         // Use a named function expression instead of a method definition, so
         // we don't create an inferred name for this function at runtime.
         [std_iterator]: function IteratorMethod() {
-            return iterator;
+            return callContentFunction(method, items);
         }
     };
 }
 
 // ES2015 22.1.3.27 Array.prototype.toString.
 function ArrayToString() {
     // Steps 1-2.
     var array = ToObject(this);
diff --git a/js/src/builtin/TypedArray.js b/js/src/builtin/TypedArray.js
--- a/js/src/builtin/TypedArray.js
+++ b/js/src/builtin/TypedArray.js
@@ -1440,20 +1440,26 @@ function TypedArrayStaticFrom(source, ma
         // Step 4.
         mapping = false;
     }
 
     // Step 5.
     var T = thisArg;
 
     // Step 6.
-    var usingIterator = GetMethod(source, std_iterator);
+    // Inlined: GetMethod, steps 1-2.
+    var usingIterator = source[std_iterator];
 
     // Step 7.
-    if (usingIterator !== undefined) {
+    // Inlined: GetMethod, step 3.
+    if (usingIterator !== undefined && usingIterator !== null) {
+        // Inlined: GetMethod, step 4.
+        if (!IsCallable(usingIterator))
+            ThrowTypeError(JSMSG_NOT_ITERABLE, DecompileArg(0, source));
+
         // Step 7.a.
         var values = IterableToList(source, usingIterator);
 
         // Step 7.b.
         var len = values.length;
 
         // Step 7.c.
         var targetObj = TypedArrayCreateWithLength(C, len);
diff --git a/js/src/builtin/Utilities.js b/js/src/builtin/Utilities.js
--- a/js/src/builtin/Utilities.js
+++ b/js/src/builtin/Utilities.js
@@ -166,17 +166,17 @@ function GetIterator(obj, method) {
     // Steps 1-2.
     assert(IsCallable(method), "method argument is not optional");
 
     // Steps 3-4.
     var iterator = callContentFunction(method, obj);
 
     // Step 5.
     if (!IsObject(iterator))
-        ThrowTypeError(JSMSG_NOT_ITERATOR, ToString(iterator));
+        ThrowTypeError(JSMSG_GET_ITER_RETURNED_PRIMITIVE);
 
     // Step 6.
     return iterator;
 }
 
 var _builtinCtorsCache = {__proto__: null};
 
 function GetBuiltinConstructor(builtinName) {
diff --git a/js/src/js.msg b/js/src/js.msg
--- a/js/src/js.msg
+++ b/js/src/js.msg
@@ -86,17 +86,16 @@ MSG_DEF(JSMSG_OBJECT_NOT_EXTENSIBLE,   1
 MSG_DEF(JSMSG_CANT_DEFINE_PROP_OBJECT_NOT_EXTENSIBLE, 2, JSEXN_TYPEERR, "can't define property {1}: {0} is not extensible")
 MSG_DEF(JSMSG_CANT_REDEFINE_PROP,      1, JSEXN_TYPEERR, "can't redefine non-configurable property {0}")
 MSG_DEF(JSMSG_CANT_REDEFINE_ARRAY_LENGTH, 0, JSEXN_TYPEERR, "can't redefine array length")
 MSG_DEF(JSMSG_CANT_DEFINE_PAST_ARRAY_LENGTH, 0, JSEXN_TYPEERR, "can't define array index property past the end of an array with non-writable length")
 MSG_DEF(JSMSG_BAD_GET_SET_FIELD,       1, JSEXN_TYPEERR, "property descriptor's {0} field is neither undefined nor a function")
 MSG_DEF(JSMSG_THROW_TYPE_ERROR,        0, JSEXN_TYPEERR, "'caller', 'callee', and 'arguments' properties may not be accessed on strict mode functions or the arguments objects for calls to them")
 MSG_DEF(JSMSG_NOT_EXPECTED_TYPE,       3, JSEXN_TYPEERR, "{0}: expected {1}, got {2}")
 MSG_DEF(JSMSG_NOT_ITERABLE,            1, JSEXN_TYPEERR, "{0} is not iterable")
-MSG_DEF(JSMSG_NOT_ITERATOR,            1, JSEXN_TYPEERR, "{0} is not iterator")
 MSG_DEF(JSMSG_ALREADY_HAS_PRAGMA,      2, JSEXN_WARN, "{0} is being assigned a {1}, but already has one")
 MSG_DEF(JSMSG_GET_ITER_RETURNED_PRIMITIVE, 0, JSEXN_TYPEERR, "[Symbol.iterator]() returned a non-object value")
 MSG_DEF(JSMSG_ITER_METHOD_RETURNED_PRIMITIVE, 1, JSEXN_TYPEERR, "iterator.{0}() returned a non-object value")
 MSG_DEF(JSMSG_CANT_SET_PROTO,          0, JSEXN_TYPEERR, "can't set prototype of this object")
 MSG_DEF(JSMSG_CANT_SET_PROTO_OF,       1, JSEXN_TYPEERR, "can't set prototype of {0}")
 MSG_DEF(JSMSG_CANT_SET_PROTO_CYCLE,    0, JSEXN_TYPEERR, "can't set prototype: it would cause a prototype chain cycle")
 MSG_DEF(JSMSG_INVALID_ARG_TYPE,        3, JSEXN_TYPEERR, "Invalid type: {0} can't be a{1} {2}")
 MSG_DEF(JSMSG_TERMINATED,              1, JSEXN_ERR, "Script terminated by timeout at:\n{0}")
