# HG changeset patch
# User Andreas Pehrson <pehrsons@gmail.com>
# Date 1495644880 -7200
# Node ID 7e3345ae9751c6a7dae74ffec68e368986906783
# Parent  549dbf6b044afbef4b4939b40e1b4e91a4c67e41
Bug 1296531 - Make logic that passes buffered data to direct listener generic. r=jesup

MozReview-Commit-ID: GdGFJHTNBS

diff --git a/dom/media/MediaStreamGraph.cpp b/dom/media/MediaStreamGraph.cpp
--- a/dom/media/MediaStreamGraph.cpp
+++ b/dom/media/MediaStreamGraph.cpp
@@ -2978,86 +2978,82 @@ SourceMediaStream::RemoveDirectListener(
   }
 }
 
 void
 SourceMediaStream::AddDirectTrackListenerImpl(already_AddRefed<DirectMediaStreamTrackListener> aListener,
                                               TrackID aTrackID)
 {
   MOZ_ASSERT(IsTrackIDExplicit(aTrackID));
-  TrackData* updateData = nullptr;
-  StreamTracks::Track* track = nullptr;
-  VideoSegment bufferedData;
-  bool isAudio = false;
-  bool isVideo = false;
+  MutexAutoLock lock(mMutex);
+
   RefPtr<DirectMediaStreamTrackListener> listener = aListener;
   LOG(LogLevel::Debug,
       ("Adding direct track listener %p bound to track %d to source stream %p",
        listener.get(),
        aTrackID,
        this));
 
-  {
-    MutexAutoLock lock(mMutex);
-    updateData = FindDataForTrack(aTrackID);
-    track = FindTrack(aTrackID);
-    if (track) {
-      isAudio = track->GetType() == MediaSegment::AUDIO;
-      isVideo = track->GetType() == MediaSegment::VIDEO;
-    }
-
-    if (track && isVideo && listener->AsMediaStreamVideoSink()) {
-      // Re-send missed VideoSegment to new added MediaStreamVideoSink.
-      VideoSegment* trackSegment = static_cast<VideoSegment*>(track->GetSegment());
-      if (mTracks.GetForgottenDuration() < trackSegment->GetDuration()) {
-        bufferedData.AppendSlice(*trackSegment,
-                                 mTracks.GetForgottenDuration(),
-                                 trackSegment->GetDuration());
-      }
-      if (updateData) {
-        bufferedData.AppendSlice(*updateData->mData, 0, updateData->mData->GetDuration());
-      }
-    }
-
-    if (track && (isAudio || isVideo)) {
-      for (auto entry : mDirectTrackListeners) {
-        if (entry.mListener == listener &&
-            (entry.mTrackID == TRACK_ANY || entry.mTrackID == aTrackID)) {
-          listener->NotifyDirectListenerInstalled(
-            DirectMediaStreamTrackListener::InstallationResult::ALREADY_EXISTS);
-          return;
-        }
-      }
-
-      TrackBound<DirectMediaStreamTrackListener>* sourceListener =
-        mDirectTrackListeners.AppendElement();
-      sourceListener->mListener = listener;
-      sourceListener->mTrackID = aTrackID;
-    }
-  }
+  StreamTracks::Track* track = FindTrack(aTrackID);
+
   if (!track) {
     LOG(LogLevel::Warning,
         ("Couldn't find source track for direct track listener %p",
          listener.get()));
     listener->NotifyDirectListenerInstalled(
       DirectMediaStreamTrackListener::InstallationResult::TRACK_NOT_FOUND_AT_SOURCE);
     return;
   }
+
+  bool isAudio = track->GetType() == MediaSegment::AUDIO;
+  bool isVideo = track->GetType() == MediaSegment::VIDEO;
   if (!isAudio && !isVideo) {
     LOG(
       LogLevel::Warning,
       ("Source track for direct track listener %p is unknown", listener.get()));
-    // It is not a video or audio track.
     MOZ_ASSERT(false);
     return;
   }
+
+  for (auto entry : mDirectTrackListeners) {
+    if (entry.mListener == listener &&
+        (entry.mTrackID == TRACK_ANY || entry.mTrackID == aTrackID)) {
+      listener->NotifyDirectListenerInstalled(
+        DirectMediaStreamTrackListener::InstallationResult::ALREADY_EXISTS);
+      return;
+    }
+  }
+
+  TrackBound<DirectMediaStreamTrackListener>* sourceListener =
+    mDirectTrackListeners.AppendElement();
+  sourceListener->mListener = listener;
+  sourceListener->mTrackID = aTrackID;
+
   LOG(LogLevel::Debug, ("Added direct track listener %p", listener.get()));
   listener->NotifyDirectListenerInstalled(
     DirectMediaStreamTrackListener::InstallationResult::SUCCESS);
 
+  // Pass buffered data to the listener
+  AudioSegment bufferedAudio;
+  VideoSegment bufferedVideo;
+  MediaSegment& bufferedData =
+    isAudio ? static_cast<MediaSegment&>(bufferedAudio)
+            : static_cast<MediaSegment&>(bufferedVideo);
+
+  MediaSegment& trackSegment = *track->GetSegment();
+  if (mTracks.GetForgottenDuration() < trackSegment.GetDuration()) {
+    bufferedData.AppendSlice(trackSegment,
+                             mTracks.GetForgottenDuration(),
+                             trackSegment.GetDuration());
+  }
+
+  if (TrackData* updateData = FindDataForTrack(aTrackID)) {
+    bufferedData.AppendSlice(*updateData->mData, 0, updateData->mData->GetDuration());
+  }
+
   if (bufferedData.GetDuration() != 0) {
     listener->NotifyRealtimeTrackData(Graph(), 0, bufferedData);
   }
 }
 
 void
 SourceMediaStream::RemoveDirectTrackListenerImpl(DirectMediaStreamTrackListener* aListener,
                                                  TrackID aTrackID)
