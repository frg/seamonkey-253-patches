# HG changeset patch
# User Ethan Lin <ethlin@mozilla.com>
# Date 1503470798 -28800
#      Wed Aug 23 14:46:38 2017 +0800
# Node ID d41aab9f4c3417657676c11ee7e4dadc3ae09029
# Parent  4a3f6c79c90f1bbb8de94b8ee43bd117bbad1fa8
Bug 1392921 - In TopmostScrollId(), return FrameMetrics::NULL_SCROLL_ID when the scroll id stack is empty. r=kats

MozReview-Commit-ID: 3kmgsisMhW5

diff --git a/gfx/layers/wr/ScrollingLayersHelper.cpp b/gfx/layers/wr/ScrollingLayersHelper.cpp
--- a/gfx/layers/wr/ScrollingLayersHelper.cpp
+++ b/gfx/layers/wr/ScrollingLayersHelper.cpp
@@ -123,17 +123,17 @@ ScrollingLayersHelper::ScrollingLayersHe
   // Finally, if clip chain's ASR was the leafmost ASR, then the top of the
   // scroll id stack right now will point to that, rather than the item's ASR
   // which is what we want. So we override that by doing a PushClipAndScrollInfo
   // call. This should generally only happen for fixed-pos type items, but we
   // use code generic enough to handle other cases.
   FrameMetrics::ViewID scrollId = aItem->GetActiveScrolledRoot()
       ? nsLayoutUtils::ViewIDForASR(aItem->GetActiveScrolledRoot())
       : FrameMetrics::NULL_SCROLL_ID;
-  if (aBuilder.TopmostScrollId() != Some(scrollId)) {
+  if (aBuilder.TopmostScrollId() != scrollId) {
     Maybe<wr::WrClipId> clipId = mBuilder->TopmostClipId();
     mBuilder->PushClipAndScrollInfo(scrollId, clipId.ptrOr(nullptr));
     mPushedClipAndScroll = true;
   }
 }
 
 void
 ScrollingLayersHelper::DefineAndPushScrollLayers(nsDisplayItem* aItem,
@@ -146,17 +146,17 @@ ScrollingLayersHelper::DefineAndPushScro
 {
   if (!aAsr) {
     return;
   }
   Maybe<ScrollMetadata> metadata = aAsr->mScrollableFrame->ComputeScrollMetadata(
       nullptr, aItem->ReferenceFrame(), ContainerLayerParameters(), nullptr);
   MOZ_ASSERT(metadata);
   FrameMetrics::ViewID scrollId = metadata->GetMetrics().GetScrollId();
-  if (aBuilder.TopmostScrollId() == Some(scrollId)) {
+  if (aBuilder.TopmostScrollId() == scrollId) {
     // it's already been pushed, so we don't need to recurse any further.
     return;
   }
 
   // Find the first clip up the chain that's "outside" aAsr. Any clips
   // that are "inside" aAsr (i.e. that are scrolled by aAsr) will need to be
   // pushed onto the stack after aAsr has been pushed. On the recursive call
   // we need to skip up the clip chain past these clips.
diff --git a/gfx/webrender_bindings/WebRenderAPI.cpp b/gfx/webrender_bindings/WebRenderAPI.cpp
--- a/gfx/webrender_bindings/WebRenderAPI.cpp
+++ b/gfx/webrender_bindings/WebRenderAPI.cpp
@@ -996,23 +996,23 @@ Maybe<wr::WrClipId>
 DisplayListBuilder::TopmostClipId()
 {
   if (mClipIdStack.empty()) {
     return Nothing();
   }
   return Some(mClipIdStack.back());
 }
 
-Maybe<layers::FrameMetrics::ViewID>
+layers::FrameMetrics::ViewID
 DisplayListBuilder::TopmostScrollId()
 {
   if (mScrollIdStack.empty()) {
-    return Nothing();
+    return layers::FrameMetrics::NULL_SCROLL_ID;
   }
-  return Some(mScrollIdStack.back());
+  return mScrollIdStack.back();
 }
 
 Maybe<layers::FrameMetrics::ViewID>
 DisplayListBuilder::ParentScrollIdFor(layers::FrameMetrics::ViewID aScrollId)
 {
   auto it = mScrollParents.find(aScrollId);
   return (it == mScrollParents.end() ? Nothing() : Some(it->second));
 }
diff --git a/gfx/webrender_bindings/WebRenderAPI.h b/gfx/webrender_bindings/WebRenderAPI.h
--- a/gfx/webrender_bindings/WebRenderAPI.h
+++ b/gfx/webrender_bindings/WebRenderAPI.h
@@ -325,17 +325,17 @@ public:
                      const float& aBorderRadius,
                      const wr::BoxShadowClipMode& aClipMode);
 
   // Returns the clip id that was most recently pushed with PushClip and that
   // has not yet been popped with PopClip. Return Nothing() if the clip stack
   // is empty.
   Maybe<wr::WrClipId> TopmostClipId();
   // Same as TopmostClipId() but for scroll layers.
-  Maybe<layers::FrameMetrics::ViewID> TopmostScrollId();
+  layers::FrameMetrics::ViewID TopmostScrollId();
   // Returns the scroll id that was pushed just before the given scroll id. This
   // function returns Nothing() if the given scrollid has not been encountered,
   // or if it is the rootmost scroll id (and therefore has no ancestor).
   Maybe<layers::FrameMetrics::ViewID> ParentScrollIdFor(layers::FrameMetrics::ViewID aScrollId);
 
   // Try to avoid using this when possible.
   wr::WrState* Raw() { return mWrState; }
 protected:
