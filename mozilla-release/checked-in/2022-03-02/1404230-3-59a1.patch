# HG changeset patch
# User James Cheng <jacheng@mozilla.com>
# Date 1507189045 -28800
# Node ID 4958b7135ea3b0d0fc8fc1ff3b8f6e28e7423ffb
# Parent  c21c8ef9e36c29a3893ef325a19d0c13ab353aad
Bug 1404230 - Part3 - Add GetStatusForPolicy method in CDMProxy and its derived classes. r=cpearce

MozReview-Commit-ID: myrPzVi0rl

diff --git a/dom/media/eme/CDMProxy.h b/dom/media/eme/CDMProxy.h
--- a/dom/media/eme/CDMProxy.h
+++ b/dom/media/eme/CDMProxy.h
@@ -220,16 +220,22 @@ public:
   virtual  CDMCaps& Capabilites() = 0;
 
   // Main thread only.
   virtual void OnKeyStatusesChange(const nsAString& aSessionId) = 0;
 
   virtual void GetSessionIdsForKeyId(const nsTArray<uint8_t>& aKeyId,
                                      nsTArray<nsCString>& aSessionIds) = 0;
 
+  // Main thread only.
+  // Calls MediaKeys->ResolvePromiseWithKeyStatus(aPromiseId, aKeyStatus) after
+  // the CDM has processed the request.
+  virtual void GetStatusForPolicy(PromiseId aPromiseId,
+                                  const nsAString& aMinHdcpVersion) = 0;
+
 #ifdef DEBUG
   virtual bool IsOnOwnerThread() = 0;
 #endif
 
   virtual uint32_t GetDecryptorId() { return 0; }
 
   virtual ChromiumCDMProxy* AsChromiumCDMProxy() { return nullptr; }
 
diff --git a/dom/media/eme/mediadrm/MediaDrmCDMProxy.cpp b/dom/media/eme/mediadrm/MediaDrmCDMProxy.cpp
--- a/dom/media/eme/mediadrm/MediaDrmCDMProxy.cpp
+++ b/dom/media/eme/mediadrm/MediaDrmCDMProxy.cpp
@@ -103,26 +103,26 @@ MediaDrmCDMProxy::CreateSession(uint32_t
 
 void
 MediaDrmCDMProxy::LoadSession(PromiseId aPromiseId,
                               dom::MediaKeySessionType aSessionType,
                               const nsAString& aSessionId)
 {
   // TODO: Implement LoadSession.
   RejectPromise(aPromiseId, NS_ERROR_DOM_INVALID_STATE_ERR,
-                NS_LITERAL_CSTRING("Currently Fennec did not support LoadSession"));
+                NS_LITERAL_CSTRING("Currently Fennec does not support LoadSession"));
 }
 
 void
 MediaDrmCDMProxy::SetServerCertificate(PromiseId aPromiseId,
                                      nsTArray<uint8_t>& aCert)
 {
   // TODO: Implement SetServerCertificate.
   RejectPromise(aPromiseId, NS_ERROR_DOM_INVALID_STATE_ERR,
-                NS_LITERAL_CSTRING("Currently Fennec did not support SetServerCertificate"));
+                NS_LITERAL_CSTRING("Currently Fennec does not support SetServerCertificate"));
 }
 
 void
 MediaDrmCDMProxy::UpdateSession(const nsAString& aSessionId,
                               PromiseId aPromiseId,
                               nsTArray<uint8_t>& aResponse)
 {
   MOZ_ASSERT(NS_IsMainThread());
@@ -163,17 +163,17 @@ MediaDrmCDMProxy::CloseSession(const nsA
 }
 
 void
 MediaDrmCDMProxy::RemoveSession(const nsAString& aSessionId,
                               PromiseId aPromiseId)
 {
   // TODO: Implement RemoveSession.
   RejectPromise(aPromiseId, NS_ERROR_DOM_INVALID_STATE_ERR,
-                NS_LITERAL_CSTRING("Currently Fennec did not support RemoveSession"));
+                NS_LITERAL_CSTRING("Currently Fennec does not support RemoveSession"));
 }
 
 void
 MediaDrmCDMProxy::Shutdown()
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(mOwnerThread);
   nsCOMPtr<nsIRunnable> task(
@@ -367,16 +367,25 @@ MediaDrmCDMProxy::OnKeyStatusesChange(co
 void
 MediaDrmCDMProxy::GetSessionIdsForKeyId(const nsTArray<uint8_t>& aKeyId,
                                       nsTArray<nsCString>& aSessionIds)
 {
   CDMCaps::AutoLock caps(Capabilites());
   caps.GetSessionIdsForKeyId(aKeyId, aSessionIds);
 }
 
+void
+MediaDrmCDMProxy::GetStatusForPolicy(PromiseId aPromiseId,
+                                     const nsAString& aMinHdcpVersion)
+{
+  // TODO: Implement GetStatusForPolicy.
+  RejectPromise(aPromiseId, NS_ERROR_DOM_NOT_SUPPORTED_ERR,
+                NS_LITERAL_CSTRING("Currently Fennec does not support GetStatusForPolicy"));
+}
+
 #ifdef DEBUG
 bool
 MediaDrmCDMProxy::IsOnOwnerThread()
 {
   return NS_GetCurrentThread() == mOwnerThread;
 }
 #endif
 
diff --git a/dom/media/eme/mediadrm/MediaDrmCDMProxy.h b/dom/media/eme/mediadrm/MediaDrmCDMProxy.h
--- a/dom/media/eme/mediadrm/MediaDrmCDMProxy.h
+++ b/dom/media/eme/mediadrm/MediaDrmCDMProxy.h
@@ -110,16 +110,19 @@ public:
 
   CDMCaps& Capabilites() override;
 
   void OnKeyStatusesChange(const nsAString& aSessionId) override;
 
   void GetSessionIdsForKeyId(const nsTArray<uint8_t>& aKeyId,
                              nsTArray<nsCString>& aSessionIds) override;
 
+  void GetStatusForPolicy(PromiseId aPromiseId,
+                          const nsAString& aMinHdcpVersion) override;
+
 #ifdef DEBUG
   bool IsOnOwnerThread() override;
 #endif
 
   const nsString& GetMediaDrmStubId() const;
 
 private:
   virtual ~MediaDrmCDMProxy();
diff --git a/dom/media/gmp/ChromiumCDMProxy.cpp b/dom/media/gmp/ChromiumCDMProxy.cpp
--- a/dom/media/gmp/ChromiumCDMProxy.cpp
+++ b/dom/media/gmp/ChromiumCDMProxy.cpp
@@ -594,16 +594,34 @@ void
 ChromiumCDMProxy::GetSessionIdsForKeyId(const nsTArray<uint8_t>& aKeyId,
                                         nsTArray<nsCString>& aSessionIds)
 {
   CDMCaps::AutoLock caps(Capabilites());
   caps.GetSessionIdsForKeyId(aKeyId, aSessionIds);
 }
 
 void
+ChromiumCDMProxy::GetStatusForPolicy(PromiseId aPromiseId,
+                                     const nsAString& aMinHdcpVersion)
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  EME_LOG("ChromiumCDMProxy::GetStatusForPolicy(pid=%u) minHdcpVersion=%s",
+          aPromiseId,
+          NS_ConvertUTF16toUTF8(aMinHdcpVersion).get());
+
+  RefPtr<gmp::ChromiumCDMParent> cdm = GetCDMParent();
+  if (!cdm) {
+    RejectPromise(aPromiseId,
+                  NS_ERROR_DOM_INVALID_STATE_ERR,
+                  NS_LITERAL_CSTRING("Null CDM in GetStatusForPolicy"));
+    return;
+  }
+}
+
+void
 ChromiumCDMProxy::Terminated()
 {
   if (!mKeys.IsNull()) {
     mKeys->Terminated();
   }
 }
 
 already_AddRefed<gmp::ChromiumCDMParent>
diff --git a/dom/media/gmp/ChromiumCDMProxy.h b/dom/media/gmp/ChromiumCDMProxy.h
--- a/dom/media/gmp/ChromiumCDMProxy.h
+++ b/dom/media/gmp/ChromiumCDMProxy.h
@@ -100,16 +100,19 @@ public:
 
   CDMCaps& Capabilites() override;
 
   void OnKeyStatusesChange(const nsAString& aSessionId) override;
 
   void GetSessionIdsForKeyId(const nsTArray<uint8_t>& aKeyId,
                              nsTArray<nsCString>& aSessionIds) override;
 
+  void GetStatusForPolicy(PromiseId aPromiseId,
+                          const nsAString& aMinHdcpVersion) override;
+
 #ifdef DEBUG
   bool IsOnOwnerThread() override;
 #endif
 
   ChromiumCDMProxy* AsChromiumCDMProxy() override { return this; }
 
   // Threadsafe. Note this may return a reference to a shutdown
   // CDM, which will fail on all operations.
