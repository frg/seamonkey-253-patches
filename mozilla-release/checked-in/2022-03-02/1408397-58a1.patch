# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1508239071 -7200
# Node ID 61118e8a62afbc51169515f50c3b12b334219aa3
# Parent  d148d73cfeb9bc025fa722abe89a7fa599c4793f
Bug 1408397 - FileReader should not use a bufferStream together with an async inputStream, r=smaug

diff --git a/dom/file/FileReader.cpp b/dom/file/FileReader.cpp
--- a/dom/file/FileReader.cpp
+++ b/dom/file/FileReader.cpp
@@ -14,16 +14,17 @@
 
 #include "mozilla/Base64.h"
 #include "mozilla/CheckedInt.h"
 #include "mozilla/dom/DOMException.h"
 #include "mozilla/dom/File.h"
 #include "mozilla/dom/FileReaderBinding.h"
 #include "mozilla/dom/ProgressEvent.h"
 #include "mozilla/Encoding.h"
+#include "nsAlgorithm.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsDOMJSUtils.h"
 #include "nsError.h"
 #include "nsNetCID.h"
 #include "nsNetUtil.h"
 #include "xpcpublic.h"
 
 #include "WorkerPrivate.h"
@@ -182,37 +183,16 @@ FileReader::GetResult(JSContext* aCx,
 
   nsString tmpResult = mResult;
   if (!xpc::StringToJsval(aCx, tmpResult, aResult)) {
     aRv.Throw(NS_ERROR_FAILURE);
     return;
   }
 }
 
-static nsresult
-ReadFuncBinaryString(nsIInputStream* in,
-                     void* closure,
-                     const char* fromRawSegment,
-                     uint32_t toOffset,
-                     uint32_t count,
-                     uint32_t *writeCount)
-{
-  char16_t* dest = static_cast<char16_t*>(closure) + toOffset;
-  char16_t* end = dest + count;
-  const unsigned char* source = (const unsigned char*)fromRawSegment;
-  while (dest != end) {
-    *dest = *source;
-    ++dest;
-    ++source;
-  }
-  *writeCount = count;
-
-  return NS_OK;
-}
-
 void
 FileReader::OnLoadEndArrayBuffer()
 {
   AutoJSAPI jsapi;
   if (!jsapi.Init(GetParentObject())) {
     FreeDataAndDispatchError(NS_ERROR_FAILURE);
     return;
   }
@@ -288,42 +268,66 @@ nsresult
 FileReader::DoReadData(uint64_t aCount)
 {
   MOZ_ASSERT(mAsyncStream);
 
   uint32_t bytesRead = 0;
 
   if (mDataFormat == FILE_AS_BINARY) {
     //Continuously update our binary string as data comes in
-    uint32_t oldLen = mResult.Length();
-    MOZ_ASSERT(mResult.Length() == mDataLen, "unexpected mResult length");
-    if (uint64_t(oldLen) + aCount > UINT32_MAX)
-      return NS_ERROR_OUT_OF_MEMORY;
-    char16_t *buf = nullptr;
-    mResult.GetMutableData(&buf, oldLen + aCount, fallible);
-    NS_ENSURE_TRUE(buf, NS_ERROR_OUT_OF_MEMORY);
+    CheckedInt<uint64_t> size = mResult.Length();
+    size += aCount;
 
-    nsresult rv;
-
-    // nsFileStreams do not implement ReadSegment. In case here we are dealing
-    // with a nsIAsyncInputStream, in content process, we need to wrap a
-    // nsIBufferedInputStream around it.
-    if (!mBufferedStream) {
-      rv = NS_NewBufferedInputStream(getter_AddRefs(mBufferedStream),
-                                     mAsyncStream, 8192);
-      NS_ENSURE_SUCCESS(rv, rv);
+    if (!size.isValid() ||
+        size.value() > UINT32_MAX ||
+        size.value() > mTotal) {
+      return NS_ERROR_OUT_OF_MEMORY;
     }
 
-    rv = mBufferedStream->ReadSegments(ReadFuncBinaryString, buf + oldLen,
-                                       aCount, &bytesRead);
-    if (NS_WARN_IF(NS_FAILED(rv))) {
-      return rv;
+    uint32_t oldLen = mResult.Length();
+    MOZ_ASSERT(oldLen == mDataLen, "unexpected mResult length");
+
+    char16_t* dest = nullptr;
+    mResult.GetMutableData(&dest, size.value(), fallible);
+    NS_ENSURE_TRUE(dest, NS_ERROR_OUT_OF_MEMORY);
+
+    dest += oldLen;
+
+    while (aCount > 0) {
+      char tmpBuffer[4096];
+      uint32_t minCount =
+        XPCOM_MIN(aCount, static_cast<uint64_t>(sizeof(tmpBuffer)));
+      uint32_t read;
+
+      nsresult rv = mAsyncStream->Read(tmpBuffer, minCount, &read);
+      if (rv == NS_BASE_STREAM_CLOSED) {
+        rv = NS_OK;
+      }
+
+      NS_ENSURE_SUCCESS(rv, rv);
+
+      if (read == 0) {
+        // The stream finished too early.
+        return NS_ERROR_OUT_OF_MEMORY;
+      }
+
+      char16_t* end = dest + read;
+      const unsigned char* source = (const unsigned char*)tmpBuffer;
+      while (dest != end) {
+        *dest = *source;
+        ++dest;
+        ++source;
+      }
+
+      aCount -= read;
+      bytesRead += read;
     }
 
-    mResult.Truncate(oldLen + bytesRead);
+    MOZ_ASSERT(size.value() == oldLen + bytesRead);
+    mResult.Truncate(size.value());
   }
   else {
     CheckedInt<uint64_t> size = mDataLen;
     size += aCount;
 
     //Update memory buffer to reflect the contents of the file
     if (!size.isValid() ||
         // PR_Realloc doesn't support over 4GB memory size even if 64-bit OS
@@ -361,17 +365,16 @@ FileReader::ReadFileContent(Blob& aBlob,
   }
 
   mError = nullptr;
 
   SetDOMStringToNull(mResult);
   mResultArrayBuffer = nullptr;
 
   mAsyncStream = nullptr;
-  mBufferedStream = nullptr;
 
   mTransferred = 0;
   mTotal = 0;
   mReadyState = EMPTY;
   FreeFileData();
 
   mBlob = &aBlob;
   mDataFormat = aDataFormat;
@@ -555,33 +558,31 @@ FileReader::ClearProgressEventTimer()
 }
 
 void
 FileReader::FreeDataAndDispatchSuccess()
 {
   FreeFileData();
   mResult.SetIsVoid(false);
   mAsyncStream = nullptr;
-  mBufferedStream = nullptr;
   mBlob = nullptr;
 
   // Dispatch event to signify end of a successful operation
   DispatchProgressEvent(NS_LITERAL_STRING(LOAD_STR));
   DispatchProgressEvent(NS_LITERAL_STRING(LOADEND_STR));
 }
 
 void
 FileReader::FreeDataAndDispatchError()
 {
   MOZ_ASSERT(mError);
 
   FreeFileData();
   mResult.SetIsVoid(true);
   mAsyncStream = nullptr;
-  mBufferedStream = nullptr;
   mBlob = nullptr;
 
   // Dispatch error event to signify load failure
   DispatchProgressEvent(NS_LITERAL_STRING(ERROR_STR));
   DispatchProgressEvent(NS_LITERAL_STRING(LOADEND_STR));
 }
 
 void
@@ -766,17 +767,16 @@ FileReader::Abort()
   // XXX The spec doesn't say this
   mError = DOMException::Create(NS_ERROR_DOM_ABORT_ERR);
 
   // Revert status and result attributes
   SetDOMStringToNull(mResult);
   mResultArrayBuffer = nullptr;
 
   mAsyncStream = nullptr;
-  mBufferedStream = nullptr;
   mBlob = nullptr;
 
   //Clean up memory buffer
   FreeFileData();
 
   // Dispatch the events
   DispatchProgressEvent(NS_LITERAL_STRING(ABORT_STR));
   DispatchProgressEvent(NS_LITERAL_STRING(LOADEND_STR));
@@ -820,21 +820,16 @@ FileReader::Shutdown()
 {
   mReadyState = DONE;
 
   if (mAsyncStream) {
     mAsyncStream->Close();
     mAsyncStream = nullptr;
   }
 
-  if (mBufferedStream) {
-    mBufferedStream->Close();
-    mBufferedStream = nullptr;
-  }
-
   FreeFileData();
   mResultArrayBuffer = nullptr;
 
   if (mWorkerPrivate && mBusyCount != 0) {
     ReleaseWorker();
     mWorkerPrivate = nullptr;
     mBusyCount = 0;
   }
diff --git a/dom/file/FileReader.h b/dom/file/FileReader.h
--- a/dom/file/FileReader.h
+++ b/dom/file/FileReader.h
@@ -178,17 +178,16 @@ private:
 
   JS::Heap<JSObject*> mResultArrayBuffer;
 
   nsCOMPtr<nsITimer> mProgressNotifier;
   bool mProgressEventWasDelayed;
   bool mTimerIsActive;
 
   nsCOMPtr<nsIAsyncInputStream> mAsyncStream;
-  nsCOMPtr<nsIInputStream> mBufferedStream;
 
   RefPtr<DOMException> mError;
 
   eReadyState mReadyState;
 
   uint64_t mTotal;
   uint64_t mTransferred;
 
