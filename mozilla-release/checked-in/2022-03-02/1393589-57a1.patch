# HG changeset patch
# User Aaron Klotz <aklotz@mozilla.com>
# Date 1503006868 21600
#      Thu Aug 17 15:54:28 2017 -0600
# Node ID 909007a82f770a1cd1e702ca4e506e46afaf9dd9
# Parent  f74f61a3db86f03bcfc6621b8ddea5dcb54ce77b
Bug 1393589: Refactor NOPING marshaling into its own class, mscom::FastMarshaler, and use it with IGeckoBackChannel; r=jimm

MozReview-Commit-ID: 9osDoYcvtWV

diff --git a/accessible/ipc/win/HandlerProvider.cpp b/accessible/ipc/win/HandlerProvider.cpp
--- a/accessible/ipc/win/HandlerProvider.cpp
+++ b/accessible/ipc/win/HandlerProvider.cpp
@@ -11,16 +11,17 @@
 #include "Accessible2_3.h"
 #include "HandlerData.h"
 #include "HandlerData_i.c"
 #include "mozilla/Assertions.h"
 #include "mozilla/a11y/AccessibleWrap.h"
 #include "mozilla/dom/ContentChild.h"
 #include "mozilla/Move.h"
 #include "mozilla/mscom/AgileReference.h"
+#include "mozilla/mscom/FastMarshaler.h"
 #include "mozilla/mscom/MainThreadInvoker.h"
 #include "mozilla/mscom/Ptr.h"
 #include "mozilla/mscom/StructStream.h"
 #include "mozilla/mscom/Utils.h"
 #include "nsThreadUtils.h"
 
 #include <memory.h>
 
@@ -38,28 +39,35 @@ HandlerProvider::HandlerProvider(REFIID 
 
 HRESULT
 HandlerProvider::QueryInterface(REFIID riid, void** ppv)
 {
   if (!ppv) {
     return E_INVALIDARG;
   }
 
-  RefPtr<IUnknown> punk;
-
   if (riid == IID_IUnknown || riid == IID_IGeckoBackChannel) {
-    punk = static_cast<IGeckoBackChannel*>(this);
+    RefPtr<IUnknown> punk(static_cast<IGeckoBackChannel*>(this));
+    punk.forget(ppv);
+    return S_OK;
   }
 
-  if (!punk) {
-    return E_NOINTERFACE;
+  if (riid == IID_IMarshal) {
+    if (!mFastMarshalUnk) {
+      HRESULT hr = mscom::FastMarshaler::Create(
+        static_cast<IGeckoBackChannel*>(this), getter_AddRefs(mFastMarshalUnk));
+      if (FAILED(hr)) {
+        return hr;
+      }
+    }
+
+    return mFastMarshalUnk->QueryInterface(riid, ppv);
   }
 
-  punk.forget(ppv);
-  return S_OK;
+  return E_NOINTERFACE;
 }
 
 ULONG
 HandlerProvider::AddRef()
 {
   return ++mRefCnt;
 }
 
diff --git a/accessible/ipc/win/HandlerProvider.h b/accessible/ipc/win/HandlerProvider.h
--- a/accessible/ipc/win/HandlerProvider.h
+++ b/accessible/ipc/win/HandlerProvider.h
@@ -62,14 +62,15 @@ private:
   static void ClearIA2Data(IA2Data& aData);
   bool IsTargetInterfaceCacheable();
 
   Atomic<uint32_t>                  mRefCnt;
   Mutex                             mMutex; // Protects mSerializer
   const IID                         mTargetUnkIid;
   mscom::InterceptorTargetPtr<IUnknown> mTargetUnk; // Constant, main thread only
   UniquePtr<mscom::StructToStream>  mSerializer;
+  RefPtr<IUnknown>                  mFastMarshalUnk;
 };
 
 } // namespace a11y
 } // namespace mozilla
 
 #endif // mozilla_a11y_HandlerProvider_h
diff --git a/ipc/mscom/FastMarshaler.cpp b/ipc/mscom/FastMarshaler.cpp
new file mode 100644
--- /dev/null
+++ b/ipc/mscom/FastMarshaler.cpp
@@ -0,0 +1,190 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "mozilla/mscom/FastMarshaler.h"
+
+#include "mozilla/mscom/Utils.h"
+
+#include <objbase.h>
+
+namespace mozilla {
+namespace mscom {
+
+HRESULT
+FastMarshaler::Create(IUnknown* aOuter,
+                      IUnknown** aOutMarshalerUnk)
+{
+  MOZ_ASSERT(XRE_IsContentProcess());
+
+  if (!aOuter || !aOutMarshalerUnk) {
+    return E_INVALIDARG;
+  }
+
+  *aOutMarshalerUnk = nullptr;
+
+  HRESULT hr;
+  RefPtr<FastMarshaler> fm(new FastMarshaler(aOuter, &hr));
+  if (FAILED(hr)) {
+    return hr;
+  }
+
+  return fm->InternalQueryInterface(IID_IUnknown, (void**)aOutMarshalerUnk);
+}
+
+FastMarshaler::FastMarshaler(IUnknown* aOuter,
+                             HRESULT* aResult)
+  : mRefCnt(0)
+  , mOuter(aOuter)
+  , mStdMarshalWeak(nullptr)
+{
+  *aResult = ::CoGetStdMarshalEx(aOuter, SMEXF_SERVER,
+                                 getter_AddRefs(mStdMarshalUnk));
+  if (FAILED(*aResult)) {
+    return;
+  }
+
+  *aResult = mStdMarshalUnk->QueryInterface(IID_IMarshal,
+                                            (void**)&mStdMarshalWeak);
+  if (FAILED(*aResult)) {
+    return;
+  }
+
+  // mStdMarshalWeak is weak
+  mStdMarshalWeak->Release();
+}
+
+HRESULT
+FastMarshaler::InternalQueryInterface(REFIID riid, void** ppv)
+{
+  if (!ppv) {
+    return E_INVALIDARG;
+  }
+
+  if (riid == IID_IUnknown) {
+    RefPtr<IUnknown> punk(static_cast<IUnknown*>(&mInternalUnknown));
+    punk.forget(ppv);
+    return S_OK;
+  }
+
+  if (riid == IID_IMarshal) {
+    RefPtr<IMarshal> ptr(this);
+    ptr.forget(ppv);
+    return S_OK;
+  }
+
+  return mStdMarshalUnk->QueryInterface(riid, ppv);
+}
+
+ULONG
+FastMarshaler::InternalAddRef()
+{
+  return ++mRefCnt;
+}
+
+ULONG
+FastMarshaler::InternalRelease()
+{
+  ULONG result = --mRefCnt;
+  if (!result) {
+    delete this;
+  }
+
+  return result;
+}
+
+DWORD
+FastMarshaler::GetMarshalFlags(DWORD aDestContext, DWORD aMshlFlags)
+{
+  // Only worry about local contexts.
+  if (aDestContext != MSHCTX_LOCAL) {
+    return aMshlFlags;
+  }
+
+  if (!IsCallerExternalProcess()) {
+    return aMshlFlags;
+  }
+
+  // The caller is our parent main thread. Disable ping functionality.
+  return aMshlFlags | MSHLFLAGS_NOPING;
+}
+
+HRESULT
+FastMarshaler::GetUnmarshalClass(REFIID riid, void* pv, DWORD dwDestContext,
+                                 void* pvDestContext, DWORD mshlflags,
+                                 CLSID* pCid)
+{
+  if (!mStdMarshalWeak) {
+    return E_POINTER;
+  }
+
+  return mStdMarshalWeak->GetUnmarshalClass(riid, pv, dwDestContext,
+                                            pvDestContext,
+                                            GetMarshalFlags(dwDestContext,
+                                                            mshlflags), pCid);
+}
+
+HRESULT
+FastMarshaler::GetMarshalSizeMax(REFIID riid, void* pv, DWORD dwDestContext,
+                                 void* pvDestContext, DWORD mshlflags,
+                                 DWORD* pSize)
+{
+  if (!mStdMarshalWeak) {
+    return E_POINTER;
+  }
+
+  return mStdMarshalWeak->GetMarshalSizeMax(riid, pv, dwDestContext,
+                                            pvDestContext,
+                                            GetMarshalFlags(dwDestContext,
+                                                            mshlflags), pSize);
+}
+
+HRESULT
+FastMarshaler::MarshalInterface(IStream* pStm, REFIID riid, void* pv,
+                                DWORD dwDestContext, void* pvDestContext,
+                                DWORD mshlflags)
+{
+  if (!mStdMarshalWeak) {
+    return E_POINTER;
+  }
+
+  return mStdMarshalWeak->MarshalInterface(pStm, riid, pv, dwDestContext,
+                                           pvDestContext,
+                                           GetMarshalFlags(dwDestContext,
+                                                           mshlflags));
+}
+
+HRESULT
+FastMarshaler::UnmarshalInterface(IStream* pStm, REFIID riid, void** ppv)
+{
+  if (!mStdMarshalWeak) {
+    return E_POINTER;
+  }
+
+  return mStdMarshalWeak->UnmarshalInterface(pStm, riid, ppv);
+}
+
+HRESULT
+FastMarshaler::ReleaseMarshalData(IStream* pStm)
+{
+  if (!mStdMarshalWeak) {
+    return E_POINTER;
+  }
+
+  return mStdMarshalWeak->ReleaseMarshalData(pStm);
+}
+
+HRESULT
+FastMarshaler::DisconnectObject(DWORD dwReserved)
+{
+  if (!mStdMarshalWeak) {
+    return E_POINTER;
+  }
+
+  return mStdMarshalWeak->DisconnectObject(dwReserved);
+}
+
+} // namespace mscom
+} // namespace mozilla
diff --git a/ipc/mscom/FastMarshaler.h b/ipc/mscom/FastMarshaler.h
new file mode 100644
--- /dev/null
+++ b/ipc/mscom/FastMarshaler.h
@@ -0,0 +1,65 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef mozilla_mscom_FastMarshaler_h
+#define mozilla_mscom_FastMarshaler_h
+
+#include "mozilla/Atomics.h"
+#include "mozilla/mscom/Aggregation.h"
+#include "mozilla/RefPtr.h"
+
+#include <objidl.h>
+
+namespace mozilla {
+namespace mscom {
+
+/**
+ * When we are marshaling to the parent process main thread, we want to turn
+ * off COM's ping functionality. That functionality is designed to free
+ * strong references held by defunct client processes. Since our content
+ * processes cannot outlive our parent process, we turn off pings when we know
+ * that the COM client is going to be our parent process. This provides a
+ * significant performance boost in a11y code due to large numbers of remote
+ * objects being created and destroyed within a short period of time.
+ */
+class FastMarshaler final : public IMarshal
+{
+public:
+  static HRESULT Create(IUnknown* aOuter,
+                        IUnknown** aOutMarshalerUnk);
+
+  // IMarshal
+  STDMETHODIMP GetUnmarshalClass(REFIID riid, void* pv, DWORD dwDestContext,
+                                 void* pvDestContext, DWORD mshlflags,
+                                 CLSID* pCid) override;
+  STDMETHODIMP GetMarshalSizeMax(REFIID riid, void* pv, DWORD dwDestContext,
+                                 void* pvDestContext, DWORD mshlflags,
+                                 DWORD* pSize) override;
+  STDMETHODIMP MarshalInterface(IStream* pStm, REFIID riid, void* pv,
+                                DWORD dwDestContext, void* pvDestContext,
+                                DWORD mshlflags) override;
+  STDMETHODIMP UnmarshalInterface(IStream* pStm, REFIID riid,
+                                  void** ppv) override;
+  STDMETHODIMP ReleaseMarshalData(IStream* pStm) override;
+  STDMETHODIMP DisconnectObject(DWORD dwReserved) override;
+
+private:
+  FastMarshaler(IUnknown* aOuter, HRESULT* aResult);
+  ~FastMarshaler() = default;
+
+  static DWORD GetMarshalFlags(DWORD aDestContext, DWORD aMshlFlags);
+
+  Atomic<ULONG>     mRefCnt;
+  IUnknown*         mOuter;
+  RefPtr<IUnknown>  mStdMarshalUnk;
+  IMarshal*         mStdMarshalWeak;
+  DECLARE_AGGREGATABLE(FastMarshaler);
+};
+
+} // namespace mscom
+} // namespace mozilla
+
+#endif // mozilla_mscom_FastMarshaler_h
diff --git a/ipc/mscom/Interceptor.cpp b/ipc/mscom/Interceptor.cpp
--- a/ipc/mscom/Interceptor.cpp
+++ b/ipc/mscom/Interceptor.cpp
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #define INITGUID
 
 #include "mozilla/dom/ContentChild.h"
 #include "mozilla/Move.h"
 #include "mozilla/mscom/DispatchForwarder.h"
+#include "mozilla/mscom/FastMarshaler.h"
 #include "mozilla/mscom/Interceptor.h"
 #include "mozilla/mscom/InterceptorLog.h"
 #include "mozilla/mscom/MainThreadInvoker.h"
 #include "mozilla/mscom/Objref.h"
 #include "mozilla/mscom/Registration.h"
 #include "mozilla/mscom/Utils.h"
 #include "MainThreadUtils.h"
 #include "mozilla/Assertions.h"
@@ -190,71 +191,32 @@ Interceptor::GetClassForHandler(DWORD aD
       aDestContext == MSHCTX_DIFFERENTMACHINE) {
     return E_INVALIDARG;
   }
 
   MOZ_ASSERT(mEventSink);
   return mEventSink->GetHandler(WrapNotNull(aHandlerClsid));
 }
 
-/**
- * When we are marshaling to the parent process main thread, we want to turn
- * off COM's ping functionality. That functionality is designed to free
- * strong references held by defunct client processes. Since our content
- * processes cannot outlive our parent process, we turn off pings when we know
- * that the COM client is going to be our parent process. This provides a
- * significant performance boost in a11y code due to large numbers of remote
- * objects being created and destroyed within a short period of time.
- */
-DWORD
-Interceptor::GetMarshalFlags(DWORD aDestContext, DWORD aMarshalFlags)
-{
-  // Only worry about local contexts.
-  if (aDestContext != MSHCTX_LOCAL) {
-    return aMarshalFlags;
-  }
-
-  // Get the caller TID. We check for S_FALSE to ensure that the caller is a
-  // different process from ours, which is the only scenario we care about.
-  DWORD callerTid;
-  if (::CoGetCallerTID(&callerTid) != S_FALSE) {
-    return aMarshalFlags;
-  }
-
-  // Now we compare the caller TID to our parent main thread TID.
-  const DWORD chromeMainTid =
-    dom::ContentChild::GetSingleton()->GetChromeMainThreadId();
-  if (callerTid != chromeMainTid) {
-    return aMarshalFlags;
-  }
-
-  // The caller is our parent main thread. Disable ping functionality.
-  return aMarshalFlags | MSHLFLAGS_NOPING;
-}
-
 HRESULT
 Interceptor::GetUnmarshalClass(REFIID riid, void* pv, DWORD dwDestContext,
                                void* pvDestContext, DWORD mshlflags,
                                CLSID* pCid)
 {
   return mStdMarshal->GetUnmarshalClass(riid, pv, dwDestContext, pvDestContext,
-                                        GetMarshalFlags(dwDestContext,
-                                                        mshlflags), pCid);
+                                        mshlflags, pCid);
 }
 
 HRESULT
 Interceptor::GetMarshalSizeMax(REFIID riid, void* pv, DWORD dwDestContext,
                                void* pvDestContext, DWORD mshlflags,
                                DWORD* pSize)
 {
   HRESULT hr = mStdMarshal->GetMarshalSizeMax(riid, pv, dwDestContext,
-                                              pvDestContext,
-                                              GetMarshalFlags(dwDestContext,
-                                                              mshlflags),
-                                              pSize);
+                                              pvDestContext, mshlflags, pSize);
   if (FAILED(hr)) {
     return hr;
   }
 
   DWORD payloadSize = 0;
   hr = mEventSink->GetHandlerPayloadSize(WrapNotNull(&payloadSize));
   if (hr == E_NOTIMPL) {
     return S_OK;
@@ -283,54 +245,40 @@ Interceptor::MarshalInterface(IStream* p
   hr = pStm->Seek(seekTo, STREAM_SEEK_CUR, &objrefPos);
   if (FAILED(hr)) {
     return hr;
   }
 
 #endif // defined(MOZ_MSCOM_REMARSHAL_NO_HANDLER)
 
   hr = mStdMarshal->MarshalInterface(pStm, riid, pv, dwDestContext,
-                                     pvDestContext,
-                                     GetMarshalFlags(dwDestContext, mshlflags));
+                                     pvDestContext, mshlflags);
   if (FAILED(hr)) {
     return hr;
   }
 
 #if defined(MOZ_MSCOM_REMARSHAL_NO_HANDLER)
-  if (XRE_IsContentProcess()) {
-    const DWORD chromeMainTid =
-      dom::ContentChild::GetSingleton()->GetChromeMainThreadId();
-
-    /*
-     * CoGetCallerTID() gives us the caller's thread ID when that thread resides
-     * in a single-threaded apartment. Since our chrome main thread does live
-     * inside an STA, we will therefore be able to check whether the caller TID
-     * equals our chrome main thread TID. This enables us to distinguish
-     * between our chrome thread vs other out-of-process callers.
-     */
-    DWORD callerTid;
-    if (::CoGetCallerTID(&callerTid) == S_FALSE && callerTid != chromeMainTid) {
-      // The caller isn't our chrome process, so do not provide a handler.
+  if (XRE_IsContentProcess() && IsCallerExternalProcess()) {
+    // The caller isn't our chrome process, so do not provide a handler.
 
-      // First, save the current position that marks the current end of the
-      // OBJREF in the stream.
-      ULARGE_INTEGER endPos;
-      hr = pStm->Seek(seekTo, STREAM_SEEK_CUR, &endPos);
-      if (FAILED(hr)) {
-        return hr;
-      }
+    // First, save the current position that marks the current end of the
+    // OBJREF in the stream.
+    ULARGE_INTEGER endPos;
+    hr = pStm->Seek(seekTo, STREAM_SEEK_CUR, &endPos);
+    if (FAILED(hr)) {
+      return hr;
+    }
 
-      // Now strip out the handler.
-      if (!StripHandlerFromOBJREF(WrapNotNull(pStm), objrefPos.QuadPart,
-                                  endPos.QuadPart)) {
-        return E_FAIL;
-      }
+    // Now strip out the handler.
+    if (!StripHandlerFromOBJREF(WrapNotNull(pStm), objrefPos.QuadPart,
+                                endPos.QuadPart)) {
+      return E_FAIL;
+    }
 
-      return S_OK;
-    }
+    return S_OK;
   }
 #endif // defined(MOZ_MSCOM_REMARSHAL_NO_HANDLER)
 
   hr = mEventSink->WriteHandlerPayload(WrapNotNull(pStm));
   if (hr == E_NOTIMPL) {
     return S_OK;
   }
 
@@ -711,28 +659,34 @@ Interceptor::WeakRefQueryInterface(REFII
     RefPtr<IStdMarshalInfo> std(this);
     std.forget(aOutInterface);
     return S_OK;
   }
 
   if (aIid == IID_IMarshal) {
     MutexAutoLock lock(mStdMarshalMutex);
 
+    HRESULT hr;
+
     if (!mStdMarshalUnk) {
-      HRESULT hr = ::CoGetStdMarshalEx(static_cast<IWeakReferenceSource*>(this),
-                                       SMEXF_SERVER,
-                                       getter_AddRefs(mStdMarshalUnk));
+      if (XRE_IsContentProcess()) {
+        hr = FastMarshaler::Create(static_cast<IWeakReferenceSource*>(this),
+                                   getter_AddRefs(mStdMarshalUnk));
+      } else {
+        hr = ::CoGetStdMarshalEx(static_cast<IWeakReferenceSource*>(this),
+                                 SMEXF_SERVER, getter_AddRefs(mStdMarshalUnk));
+      }
+
       if (FAILED(hr)) {
         return hr;
       }
     }
 
     if (!mStdMarshal) {
-      HRESULT hr = mStdMarshalUnk->QueryInterface(IID_IMarshal,
-                                                  (void**)&mStdMarshal);
+      hr = mStdMarshalUnk->QueryInterface(IID_IMarshal, (void**)&mStdMarshal);
       if (FAILED(hr)) {
         return hr;
       }
 
       // mStdMarshal is weak, so drop its refcount
       mStdMarshal->Release();
     }
 
diff --git a/ipc/mscom/Interceptor.h b/ipc/mscom/Interceptor.h
--- a/ipc/mscom/Interceptor.h
+++ b/ipc/mscom/Interceptor.h
@@ -131,18 +131,16 @@ private:
   HRESULT WeakRefQueryInterface(REFIID aIid,
                                 IUnknown** aOutInterface) override;
   HRESULT CreateInterceptor(REFIID aIid, IUnknown* aOuter, IUnknown** aOutput);
   HRESULT PublishTarget(detail::LiveSetAutoLock& aLiveSetLock,
                         RefPtr<IUnknown> aInterceptor,
                         REFIID aTargetIid,
                         STAUniquePtr<IUnknown> aTarget);
 
-  static DWORD GetMarshalFlags(DWORD aDestContext, DWORD aMarshalFlags);
-
 private:
   InterceptorTargetPtr<IUnknown>  mTarget;
   RefPtr<IInterceptorSink>  mEventSink;
   mozilla::Mutex            mInterceptorMapMutex; // Guards mInterceptorMap
   // Using a nsTArray since the # of interfaces is not going to be very high
   nsTArray<MapEntry>        mInterceptorMap;
   mozilla::Mutex            mStdMarshalMutex; // Guards mStdMarshalUnk and mStdMarshal
   RefPtr<IUnknown>          mStdMarshalUnk;
diff --git a/ipc/mscom/Utils.cpp b/ipc/mscom/Utils.cpp
--- a/ipc/mscom/Utils.cpp
+++ b/ipc/mscom/Utils.cpp
@@ -1,14 +1,18 @@
 /* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
+#if defined(MOZILLA_INTERNAL_API)
+#include "mozilla/dom/ContentChild.h"
+#endif
+
 #if defined(ACCESSIBILITY)
 #include "mozilla/mscom/Registration.h"
 #if defined(MOZILLA_INTERNAL_API)
 #include "nsTArray.h"
 #endif
 #endif
 
 #include "mozilla/mscom/Objref.h"
@@ -233,16 +237,41 @@ GUIDToString(REFGUID aGuid, nsAString& a
   int result = StringFromGUID2(aGuid, char16ptr_t(aOutString.BeginWriting()), kBufLenWithNul);
   MOZ_ASSERT(result);
   if (result) {
     // Truncate the terminator
     aOutString.SetLength(result - 1);
   }
 }
 
+bool
+IsCallerExternalProcess()
+{
+  MOZ_ASSERT(XRE_IsContentProcess());
+
+  /**
+   * CoGetCallerTID() gives us the caller's thread ID when that thread resides
+   * in a single-threaded apartment. Since our chrome main thread does live
+   * inside an STA, we will therefore be able to check whether the caller TID
+   * equals our chrome main thread TID. This enables us to distinguish
+   * between our chrome thread vs other out-of-process callers. We check for
+   * S_FALSE to ensure that the caller is a different process from ours, which
+   * is the only scenario that we care about.
+   */
+  DWORD callerTid;
+  if (::CoGetCallerTID(&callerTid) != S_FALSE) {
+    return false;
+  }
+
+  // Now check whether the caller is our parent process main thread.
+  const DWORD parentMainTid =
+    dom::ContentChild::GetSingleton()->GetChromeMainThreadId();
+  return callerTid != parentMainTid;
+}
+
 #endif // defined(MOZILLA_INTERNAL_API)
 
 #if defined(ACCESSIBILITY)
 
 static bool
 IsVtableIndexFromParentInterface(TYPEATTR* aTypeAttr,
                                  unsigned long aVtableIndex)
 {
diff --git a/ipc/mscom/Utils.h b/ipc/mscom/Utils.h
--- a/ipc/mscom/Utils.h
+++ b/ipc/mscom/Utils.h
@@ -44,16 +44,17 @@ uint32_t CreateStream(const uint8_t* aBu
  *                  be positioned to point at the beginning of the proxy data.
  * @param aOutStream Outparam to receive the newly created stream.
  * @return HRESULT error code.
  */
 uint32_t CopySerializedProxy(IStream* aInStream, IStream** aOutStream);
 
 #if defined(MOZILLA_INTERNAL_API)
 void GUIDToString(REFGUID aGuid, nsAString& aOutString);
+bool IsCallerExternalProcess();
 #endif // defined(MOZILLA_INTERNAL_API)
 
 #if defined(ACCESSIBILITY)
 bool IsVtableIndexFromParentInterface(REFIID aInterface,
                                       unsigned long aVtableIndex);
 
 #if defined(MOZILLA_INTERNAL_API)
 bool IsInterfaceEqualToOrInheritedFrom(REFIID aInterface, REFIID aFrom,
diff --git a/ipc/mscom/moz.build b/ipc/mscom/moz.build
--- a/ipc/mscom/moz.build
+++ b/ipc/mscom/moz.build
@@ -6,32 +6,34 @@
 
 EXPORTS.mozilla.mscom += [
     'Aggregation.h',
     'AgileReference.h',
     'AsyncInvoker.h',
     'COMApartmentRegion.h',
     'COMPtrHolder.h',
     'EnsureMTA.h',
+    'FastMarshaler.h',
     'MainThreadClientInfo.h',
     'MainThreadRuntime.h',
     'Objref.h',
     'PassthruProxy.h',
     'ProxyStream.h',
     'Ptr.h',
     'Utils.h',
 ]
 
 SOURCES += [
     'VTableBuilder.c',
 ]
 
 UNIFIED_SOURCES += [
     'AgileReference.cpp',
     'EnsureMTA.cpp',
+    'FastMarshaler.cpp',
     'MainThreadClientInfo.cpp',
     'MainThreadRuntime.cpp',
     'Objref.cpp',
     'PassthruProxy.cpp',
     'ProxyStream.cpp',
     'Utils.cpp',
 ]
 
