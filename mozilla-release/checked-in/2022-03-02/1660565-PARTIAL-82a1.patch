# HG changeset patch
# User Haik Aftandilian <haftandilian@mozilla.com>
# Date 1600032862 0
#      Sun Sep 13 21:34:22 2020 +0000
# Node ID ad6fce90a3b796b9b5eb716fbf81f9869ee6a863
# Parent  66f28c65f3ffbee55f8f770ecb128bd50d58798f
Bug 1660565 - Disable WebRender when running under Rosetta - r=mattwoodrow

Disable WebRender when running under Rosetta to workaround graphics-related crashes.

Use the Apple-documented sysctl.proc_translated sysctl to detect when running
under Rosetta using the same code in the Sandboxing module (Sandbox.mm.)

Differential Revision: https://phabricator.services.mozilla.com/D89961

diff --git a/widget/cocoa/nsCocoaFeatures.h b/widget/cocoa/nsCocoaFeatures.h
--- a/widget/cocoa/nsCocoaFeatures.h
+++ b/widget/cocoa/nsCocoaFeatures.h
@@ -24,16 +24,18 @@ public:
   static bool OnHighSierraOrLater();
   static bool OnMojaveOrLater();
   static bool OnCatalinaOrLater();
   static bool OnBigSurOrLater();
 
   static bool IsAtLeastVersion(int32_t aMajor, int32_t aMinor,
                                int32_t aBugFix = 0);
 
+  static bool ProcessIsRosettaTranslated();
+
   // These are utilities that do not change or depend on the value of
   // mOSVersion and instead just encapsulate the encoding algorithm. Note that
   // GetVersion actually adjusts to the lowest supported OS, so it will always
   // return a "supported" version. GetSystemVersion does not make any
   // modifications.
   static void GetSystemVersion(int &aMajor, int &aMinor, int &aBugFix);
   static int32_t GetVersion(int32_t aMajor, int32_t aMinor, int32_t aBugFix);
   static int32_t ExtractMajorVersion(int32_t aVersion);
diff --git a/widget/cocoa/nsCocoaFeatures.mm b/widget/cocoa/nsCocoaFeatures.mm
--- a/widget/cocoa/nsCocoaFeatures.mm
+++ b/widget/cocoa/nsCocoaFeatures.mm
@@ -26,16 +26,17 @@
 #define MACOS_VERSION_11_0_HEX 0x000B0000
 
 #include "nsCocoaFeatures.h"
 #include "nsCocoaUtils.h"
 #include "nsDebug.h"
 #include "nsObjCExceptions.h"
 
 #import <Cocoa/Cocoa.h>
+#include <sys/sysctl.h>
 
 /*static*/ int32_t nsCocoaFeatures::mOSVersion = 0;
 
 // This should not be called with unchecked aMajor, which should be >= 10.
 inline int32_t AssembleVersion(int32_t aMajor, int32_t aMinor, int32_t aBugFix) {
   MOZ_ASSERT(aMajor >= 10);
   return (aMajor << 16) + (aMinor << 8) + aBugFix;
 }
@@ -178,8 +179,27 @@ bool Gecko_OnSierraOrLater() { return ns
   return ((macOSVersion() >= MACOS_VERSION_10_16_HEX) ||
           (macOSVersion() >= MACOS_VERSION_11_0_HEX));
 }
 
 /* static */ bool nsCocoaFeatures::IsAtLeastVersion(int32_t aMajor, int32_t aMinor,
                                                     int32_t aBugFix) {
   return macOSVersion() >= GetVersion(aMajor, aMinor, aBugFix);
 }
+
+/*
+ * Returns true if the process is running under Rosetta translation. Returns
+ * false if running natively or if an error was encountered. We use the
+ * `sysctl.proc_translated` sysctl which is documented by Apple to be used
+ * for this purpose. Note: using this in a sandboxed process requires allowing
+ * the sysctl in the sandbox policy.
+ */
+/* static */ bool nsCocoaFeatures::ProcessIsRosettaTranslated() {
+  int ret = 0;
+  size_t size = sizeof(ret);
+  if (sysctlbyname("sysctl.proc_translated", &ret, &size, NULL, 0) == -1) {
+    if (errno != ENOENT) {
+      fprintf(stderr, "Failed to check for translation environment\n");
+    }
+    return false;
+  }
+  return (ret == 1);
+}
