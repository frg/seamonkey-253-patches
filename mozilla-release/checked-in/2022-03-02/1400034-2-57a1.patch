# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1505659063 14400
#      Sun Sep 17 10:37:43 2017 -0400
# Node ID 5524d88599f61ecbf0b85287e760939f4634001b
# Parent  b4df705d305b1737c6e2c144d8effe027f827774
Bug 1400034 - Do a more direct translation of transforms from Gecko to WR in layers-free mode. r=ethlin,pchang

In layers-free mode the gecko display list and coordinate system is very
similar to what WR is expecting. Instead of having each
StackingContextHelper shift the origin of the coordinate system, we can
leave it in one spot and just pass everything relative to that. The
semantics of the Gecko display list already matches this; the exception
is that nsDisplayTransform items are also considered reference frames,
and anything inside them is relative to the nsDisplayTransform. On the
WR side this is also the case, because stacking contexts with a
transform are implicitly turned into reference frames.

Additionally, the size of the bounds passed to the WR stacking context
is never actually used, except on the root stacking context (which is
not created by StackingContextHelper). Since we want a zero origin (as
explained above) and the size is never used, we can just pass a zero
rect to the WR stacking context from StackingContextHelper.

In terms of the actual transform matrix, this patch now passes the full
unmodified transform from nsDisplayTransform into WR. This transform
gets applied onto the contents of the nsDisplayTransform. The contents'
coordinate system is relative to the frame that generated the
nsDisplayTransform. Again this maps directly to WR, where the transform
on the stacking context gets applied to the contents of the stacking
context; the contents' coordinates are relative to the stacking context.

MozReview-Commit-ID: 9hdDxdKXPPi

diff --git a/gfx/layers/wr/StackingContextHelper.cpp b/gfx/layers/wr/StackingContextHelper.cpp
--- a/gfx/layers/wr/StackingContextHelper.cpp
+++ b/gfx/layers/wr/StackingContextHelper.cpp
@@ -74,51 +74,29 @@ StackingContextHelper::StackingContextHe
                                              uint64_t aAnimationsId,
                                              float* aOpacityPtr,
                                              gfx::Matrix4x4* aTransformPtr,
                                              gfx::Matrix4x4* aPerspectivePtr,
                                              const nsTArray<wr::WrFilterOp>& aFilters,
                                              const gfx::CompositionOp& aMixBlendMode)
   : mBuilder(&aBuilder)
 {
-  nsRect visibleRect;
   bool is2d = !aTransformPtr || (aTransformPtr->Is2D() && !aPerspectivePtr);
-  if (is2d) {
-    nsRect itemBounds = aDisplayList->GetClippedBoundsWithRespectToASR(aDisplayListBuilder, aItem->GetActiveScrolledRoot());
-    nsRect childrenVisible = aItem->GetVisibleRectForChildren();
-    visibleRect = itemBounds.Intersect(childrenVisible);
-  } else {
-    visibleRect = aDisplayList->GetBounds(aDisplayListBuilder);
-    // The position of bounds are calculated by transform and perspective matrix in 3d case. reset it to (0, 0)
-    visibleRect.MoveTo(0, 0);
-  }
-  float appUnitsPerDevPixel = aItem->Frame()->PresContext()->AppUnitsPerDevPixel();
-  LayerRect bounds = ViewAs<LayerPixel>(LayoutDeviceRect::FromAppUnits(visibleRect, appUnitsPerDevPixel),
-                                        PixelCastJustification::WebRenderHasUnitResolution);
-
-  // WR will only apply the 'translate' of the transform, so we need to do the scale/rotation manually.
-  if (aBoundTransform && !aBoundTransform->IsIdentity() && is2d) {
-    bounds.MoveTo(aBoundTransform->TransformPoint(bounds.TopLeft()));
-  }
-
-  wr::LayoutRect scBounds = aParentSC.ToRelativeLayoutRect(bounds);
   if (aTransformPtr) {
     mTransform = *aTransformPtr;
   }
 
-  mBuilder->PushStackingContext(scBounds,
+  mBuilder->PushStackingContext(wr::LayoutRect(),
                                 aAnimationsId,
                                 aOpacityPtr,
                                 aTransformPtr,
                                 is2d ? wr::TransformStyle::Flat : wr::TransformStyle::Preserve3D,
                                 aPerspectivePtr,
                                 wr::ToMixBlendMode(aMixBlendMode),
                                 aFilters);
-
-  mOrigin = bounds.TopLeft();
 }
 
 StackingContextHelper::~StackingContextHelper()
 {
   if (mBuilder) {
     mBuilder->PopStackingContext();
   }
 }
diff --git a/layout/painting/nsDisplayList.cpp b/layout/painting/nsDisplayList.cpp
--- a/layout/painting/nsDisplayList.cpp
+++ b/layout/painting/nsDisplayList.cpp
@@ -7893,20 +7893,17 @@ nsDisplayTransform::CreateWebRenderComma
     OptionalTransform transformForCompositor = newTransformMatrix;
 
     OpAddCompositorAnimations
       anim(CompositorAnimations(animationInfo.GetAnimations(), animationsId),
            transformForCompositor, void_t());
     aManager->WrBridge()->AddWebRenderParentCommand(anim);
   }
 
-  gfx::Matrix4x4Typed<LayerPixel, LayerPixel> boundTransform = ViewAs< gfx::Matrix4x4Typed<LayerPixel, LayerPixel> >(newTransformMatrix);
-  boundTransform._41 = 0.0f;
-  boundTransform._42 = 0.0f;
-  boundTransform._43 = 0.0f;
+  gfx::Matrix4x4Typed<LayerPixel, LayerPixel> boundTransform = ViewAs<gfx::Matrix4x4Typed<LayerPixel, LayerPixel>>(newTransformMatrix);
 
   nsTArray<mozilla::wr::WrFilterOp> filters;
   StackingContextHelper sc(aSc,
                            aBuilder,
                            aDisplayListBuilder,
                            this,
                            mStoredList.GetChildren(),
                            &boundTransform,
diff --git a/layout/reftests/async-scrolling/reftest.list b/layout/reftests/async-scrolling/reftest.list
--- a/layout/reftests/async-scrolling/reftest.list
+++ b/layout/reftests/async-scrolling/reftest.list
@@ -33,20 +33,20 @@ skip-if(!asyncPan) == culling-1.html cul
 skip-if(!asyncPan) == position-fixed-iframe-1.html position-fixed-iframe-1-ref.html
 skip-if(!asyncPan) == position-fixed-iframe-2.html position-fixed-iframe-2-ref.html
 fuzzy-if(skiaContent,1,11300) skip-if(!asyncPan) == position-fixed-in-scroll-container.html position-fixed-in-scroll-container-ref.html
 skip-if(!asyncPan) == position-fixed-inside-sticky-1.html position-fixed-inside-sticky-1-ref.html
 skip-if(!asyncPan) == position-fixed-inside-sticky-2.html position-fixed-inside-sticky-2-ref.html
 fuzzy(1,60000) skip-if(!asyncPan) == group-opacity-surface-size-1.html group-opacity-surface-size-1-ref.html
 fails-if(webrender) skip-if(!asyncPan) == position-sticky-transformed.html position-sticky-transformed-ref.html # bug 1366295 for webrender
 skip-if(!asyncPan) == offscreen-prerendered-active-opacity.html offscreen-prerendered-active-opacity-ref.html
-fuzzy-if(Android,6,4) fuzzy-if(skiaContent&&!Android,1,34) skip-if(!asyncPan) fuzzy-if(webrender,128-128,10-10) == offscreen-clipped-blendmode-1.html offscreen-clipped-blendmode-ref.html
-fuzzy-if(Android,6,4) fuzzy-if(webrender,128-128,10-10) skip-if(!asyncPan) == offscreen-clipped-blendmode-2.html offscreen-clipped-blendmode-ref.html
+fuzzy-if(Android,6,4) fuzzy-if(skiaContent&&!Android,1,34) skip-if(!asyncPan) == offscreen-clipped-blendmode-1.html offscreen-clipped-blendmode-ref.html
+fuzzy-if(Android,6,4) skip-if(!asyncPan) == offscreen-clipped-blendmode-2.html offscreen-clipped-blendmode-ref.html
 fuzzy-if(Android,6,4) skip == offscreen-clipped-blendmode-3.html offscreen-clipped-blendmode-ref.html # bug 1251588 - wrong AGR on mix-blend-mode item
-fuzzy-if(Android,6,4) fuzzy-if(webrender,128-128,10-10) skip-if(!asyncPan) == offscreen-clipped-blendmode-4.html offscreen-clipped-blendmode-ref.html
+fuzzy-if(Android,6,4) skip-if(!asyncPan) == offscreen-clipped-blendmode-4.html offscreen-clipped-blendmode-ref.html
 fuzzy-if(Android,7,4) fails-if(webrender) skip-if(!asyncPan) == perspective-scrolling-1.html perspective-scrolling-1-ref.html # bug 1361720 for webrender
 fuzzy-if(Android,7,4) fails-if(webrender) skip-if(!asyncPan) == perspective-scrolling-2.html perspective-scrolling-2-ref.html # bug 1361720 for webrender
 fuzzy-if(Android,7,4) fails-if(webrender) skip-if(!asyncPan) == perspective-scrolling-3.html perspective-scrolling-3-ref.html # bug 1361720 for webrender
 fuzzy-if(Android,7,4) fails-if(webrender) skip-if(!asyncPan) == perspective-scrolling-4.html perspective-scrolling-4-ref.html # bug 1361720 for webrender
 pref(apz.disable_for_scroll_linked_effects,true) skip-if(!asyncPan) == disable-apz-for-sle-pages.html disable-apz-for-sle-pages-ref.html
 fuzzy-if(browserIsRemote&&d2d,1,19) skip-if(!asyncPan) == background-blend-mode-1.html background-blend-mode-1-ref.html
 random-if(webrender) skip-if(Android||!asyncPan) != opaque-fractional-displayport-1.html about:blank    # test is specific to "layers" and not valid with webrender
 random-if(webrender) skip-if(Android||!asyncPan) != opaque-fractional-displayport-2.html about:blank    # test is specific to "layers" and not valid with webrender
