# HG changeset patch
# User Michael Froman <mfroman@mozilla.com>
# Date 1517372634 21600
# Node ID 07eb80c262533713bd52045e77c0452b33259cae
# Parent  bb9b10ffa41d2e9aa435b4b2277e0fb02f96dab4
Bug 1414171 - pt 2 - Sort ICE stats by componentId on about:webrtc. r=drno

- add new component_id field to NrIceCandidatePair
- add the candidate pair component_id to RTCIceCandidatePairStats in
  RecordIceStats_s
- add new column in ice stats table for component id
- sort ice stats by component id first


MozReview-Commit-ID: J89ZIYEUyRk

diff --git a/dom/media/webrtc/WebrtcGlobal.h b/dom/media/webrtc/WebrtcGlobal.h
--- a/dom/media/webrtc/WebrtcGlobal.h
+++ b/dom/media/webrtc/WebrtcGlobal.h
@@ -178,16 +178,17 @@ struct ParamTraits<mozilla::dom::RTCIceC
     WriteParam(aMsg, aParam.mTransportId);
     WriteParam(aMsg, aParam.mLocalCandidateId);
     WriteParam(aMsg, aParam.mPriority);
     WriteParam(aMsg, aParam.mNominated);
     WriteParam(aMsg, aParam.mWritable);
     WriteParam(aMsg, aParam.mReadable);
     WriteParam(aMsg, aParam.mRemoteCandidateId);
     WriteParam(aMsg, aParam.mSelected);
+    WriteParam(aMsg, aParam.mComponentId);
     WriteParam(aMsg, aParam.mState);
     WriteParam(aMsg, aParam.mBytesSent);
     WriteParam(aMsg, aParam.mBytesReceived);
     WriteParam(aMsg, aParam.mLastPacketSentTimestamp);
     WriteParam(aMsg, aParam.mLastPacketReceivedTimestamp);
     WriteRTCStats(aMsg, aParam);
   }
 
@@ -196,16 +197,17 @@ struct ParamTraits<mozilla::dom::RTCIceC
     if (!ReadParam(aMsg, aIter, &(aResult->mTransportId)) ||
         !ReadParam(aMsg, aIter, &(aResult->mLocalCandidateId)) ||
         !ReadParam(aMsg, aIter, &(aResult->mPriority)) ||
         !ReadParam(aMsg, aIter, &(aResult->mNominated)) ||
         !ReadParam(aMsg, aIter, &(aResult->mWritable)) ||
         !ReadParam(aMsg, aIter, &(aResult->mReadable)) ||
         !ReadParam(aMsg, aIter, &(aResult->mRemoteCandidateId)) ||
         !ReadParam(aMsg, aIter, &(aResult->mSelected)) ||
+        !ReadParam(aMsg, aIter, &(aResult->mComponentId)) ||
         !ReadParam(aMsg, aIter, &(aResult->mState)) ||
         !ReadParam(aMsg, aIter, &(aResult->mBytesSent)) ||
         !ReadParam(aMsg, aIter, &(aResult->mBytesReceived)) ||
         !ReadParam(aMsg, aIter, &(aResult->mLastPacketSentTimestamp)) ||
         !ReadParam(aMsg, aIter, &(aResult->mLastPacketReceivedTimestamp)) ||
         !ReadRTCStats(aMsg, aIter, aResult)) {
       return false;
     }
diff --git a/media/mtransport/nricemediastream.cpp b/media/mtransport/nricemediastream.cpp
--- a/media/mtransport/nricemediastream.cpp
+++ b/media/mtransport/nricemediastream.cpp
@@ -399,16 +399,18 @@ nsresult NrIceMediaStream::GetCandidateP
         pair.state = NrIceCandidatePair::State::STATE_CANCELLED;
         break;
       default:
         MOZ_ASSERT(0);
     }
 
     pair.priority = p1->priority;
     pair.nominated = p1->peer_nominated || p1->nominated;
+    pair.component_id = p1->remote->component->component_id;
+
     // As discussed with drno: a component's can_send field (set to true
     // by ICE consent) is a very close approximation for writable and
     // readable. Note: the component for the local candidate never has
     // the can_send member set to true, remote for both readable and
     // writable. (mjf)
     pair.writable = p1->remote->component->can_send;
     pair.readable = p1->remote->component->can_send;
     pair.selected = p1->remote->component &&
diff --git a/media/mtransport/nricemediastream.h b/media/mtransport/nricemediastream.h
--- a/media/mtransport/nricemediastream.h
+++ b/media/mtransport/nricemediastream.h
@@ -121,16 +121,17 @@ struct NrIceCandidatePair {
   bool readable;
   // Set if this candidate pair has been selected. Note: Since we are using
   // aggressive nomination, this could change frequently as ICE runs.
   bool selected;
   NrIceCandidate local;
   NrIceCandidate remote;
   // TODO(bcampen@mozilla.com): Is it important to put the foundation in here?
   std::string codeword;
+  uint64_t component_id;
 
   // for RTCIceCandidatePairStats
   uint64_t bytes_sent;
   uint64_t bytes_recvd;
   uint64_t ms_since_last_send;
   uint64_t ms_since_last_recv;
 };
 
diff --git a/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp b/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp
--- a/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp
+++ b/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp
@@ -3649,16 +3649,17 @@ static void RecordIceStats_s(
     s.mReadable.Construct(candPair.readable);
     s.mPriority.Construct(candPair.priority);
     s.mSelected.Construct(candPair.selected);
     s.mBytesSent.Construct(candPair.bytes_sent);
     s.mBytesReceived.Construct(candPair.bytes_recvd);
     s.mLastPacketSentTimestamp.Construct(candPair.ms_since_last_send);
     s.mLastPacketReceivedTimestamp.Construct(candPair.ms_since_last_recv);
     s.mState.Construct(RTCStatsIceCandidatePairState(candPair.state));
+    s.mComponentId.Construct(candPair.component_id);
     report->mIceCandidatePairStats.Value().AppendElement(s, fallible);
   }
 
   std::vector<NrIceCandidate> candidates;
   if (NS_SUCCEEDED(mediaStream.GetLocalCandidates(&candidates))) {
     ToRTCIceCandidateStats(candidates,
                            RTCStatsType::Local_candidate,
                            transportId,
diff --git a/toolkit/content/aboutwebrtc/aboutWebrtc.css b/toolkit/content/aboutwebrtc/aboutWebrtc.css
--- a/toolkit/content/aboutwebrtc/aboutWebrtc.css
+++ b/toolkit/content/aboutwebrtc/aboutWebrtc.css
@@ -78,16 +78,21 @@ html {
 .peer-connection table caption {
   text-align: left;
 }
 
 .peer-connection table.raw-candidate {
   text-align: left;
 }
 
+.bottom-border td {
+  border-bottom: thin solid;
+  border-color: black;
+}
+
 .trickled {
   background-color: #bdf;
 }
 
 .info-label {
   font-weight: bold;
 }
 
diff --git a/toolkit/content/aboutwebrtc/aboutWebrtc.js b/toolkit/content/aboutwebrtc/aboutWebrtc.js
--- a/toolkit/content/aboutwebrtc/aboutWebrtc.js
+++ b/toolkit/content/aboutwebrtc/aboutWebrtc.js
@@ -657,26 +657,28 @@ ICEStats.prototype = {
           renderElement("span", `${end}`));
     }
 
     let stats = this.generateICEStats();
     // don't use |stat.x || ""| here because it hides 0 values
     let tbody = stats.map(stat => [
       stat["local-candidate"],
       stat["remote-candidate"],
+      stat.componentId,
       stat.state,
       stat.priority,
       stat.nominated,
       stat.selected,
       stat.bytesSent,
       stat.bytesReceived
     ].map(entry => Object.is(entry, undefined) ? "" : entry));
 
     let statsTable = new SimpleTable(
-      ["local_candidate", "remote_candidate", "ice_state",
+      ["local_candidate", "remote_candidate",
+       "ice_component_id", "ice_state",
        "priority", "nominated", "selected",
        "ice_pair_bytes_sent", "ice_pair_bytes_received"
       ].map(columnName => getString(columnName)),
       tbody, caption).render();
 
     // after rendering the table, we need to change the class name for each
     // candidate pair's local or remote candidate if it was trickled.
     stats.forEach((stat, index) => {
@@ -685,16 +687,27 @@ ICEStats.prototype = {
       if (stat["remote-trickled"]) {
         statsTable.rows[rowIndex].cells[1].className = "trickled";
       }
       if (stat["local-trickled"]) {
         statsTable.rows[rowIndex].cells[0].className = "trickled";
       }
     });
 
+    // if the next row's component id changes, mark the bottom of the
+    // current row with a thin, black border to differentiate the
+    // component id grouping.
+    let rowCount = statsTable.rows.length - 1;
+    for (var i = 0; i < rowCount; i++) {
+      if (statsTable.rows[i].cells[2].innerHTML !==
+          statsTable.rows[i + 1].cells[2].innerHTML) {
+        statsTable.rows[i].className = "bottom-border";
+      }
+    }
+
     return statsTable;
   },
 
   renderRawICECandidates() {
     let div = document.createElement("div");
 
     let tbody = [];
     let rows = this.generateRawICECandidates();
@@ -784,20 +797,20 @@ ICEStats.prototype = {
     // Combine those with both; these will be the peer candidates.
     let matched = {};
     let stats = [];
     let stat;
 
     for (let pair of this._report.iceCandidatePairStats) {
       let local = candidates.get(pair.localCandidateId);
       let remote = candidates.get(pair.remoteCandidateId);
-
       if (local) {
         stat = {
           ["local-candidate"]: this.candidateToString(local),
+          componentId: pair.componentId,
           state: pair.state,
           priority: pair.priority,
           nominated: pair.nominated,
           selected: pair.selected,
           bytesSent: pair.bytesSent,
           bytesReceived: pair.bytesReceived
         };
         matched[local.id] = true;
@@ -811,20 +824,27 @@ ICEStats.prototype = {
           if (isTrickled(remote.id)) {
             stat["remote-trickled"] = true;
           }
         }
         stats.push(stat);
       }
     }
 
-    return stats.sort((a, b) => (b.bytesSent ?
-                                 (b.bytesSent || 0) - (a.bytesSent || 0) :
-                                 (b.priority || 0) - (a.priority || 0)
-                                ));
+    // sort (group by) componentId first, then bytesSent if available, else by
+    // priority
+    return stats.sort((a, b) => {
+        if (a.componentId != b.componentId) {
+          return a.componentId - b.componentId;
+        }
+        return (b.bytesSent ?
+                (b.bytesSent || 0) - (a.bytesSent || 0) :
+                (b.priority || 0) - (a.priority || 0)
+               );
+      });
   },
 
   candidateToString(c) {
     if (!c) {
       return "*";
     }
 
     var type = c.candidateType;
diff --git a/toolkit/locales/en-US/chrome/global/aboutWebrtc.properties b/toolkit/locales/en-US/chrome/global/aboutWebrtc.properties
--- a/toolkit/locales/en-US/chrome/global/aboutWebrtc.properties
+++ b/toolkit/locales/en-US/chrome/global/aboutWebrtc.properties
@@ -66,16 +66,17 @@ rtp_stats_heading = RTP Stats
 # and should not normally be translated. "Stats" is an abbreviation for
 # Statistics.
 ice_state = ICE State
 ice_stats_heading = ICE Stats
 ice_restart_count_label = ICE restarts
 ice_rollback_count_label = ICE rollbacks
 ice_pair_bytes_sent = Bytes sent
 ice_pair_bytes_received = Bytes received
+ice_component_id = Component ID
 
 # LOCALIZATION NOTE (av_sync_label): "A/V" stands for Audio/Video.
 # "sync" is an abbreviation for sychronization. This is used as
 # a data label.
 av_sync_label = A/V sync
 
 # LOCALIZATION NOTE (jitter_buffer_delay_label): A jitter buffer is an
 # element in the processing chain, see http://wikipedia.org/wiki/Jitter
