# HG changeset patch
# User Markus Stange <mstange@themasta.com>
# Date 1539809358 0
# Node ID c9da89f17cf9426dbfad7af2940ebc9f05225c3a
# Parent  6e6ac5f96dad1acc90f2d14d0d2cbeb77bd067db
Bug 1496823 - Inline -[ChildView drawRect:inContext:] into -[ChildView drawRect:]. r=spohl

This was separate because at some point in the past we were calling
-[ChildView drawRect:inContext:] from a separate draw-in-titlebar pass. That separate
pass was removed in bug 676241.

Depends on D7929

Differential Revision: https://phabricator.services.mozilla.com/D7931

diff --git a/widget/cocoa/nsChildView.mm b/widget/cocoa/nsChildView.mm
--- a/widget/cocoa/nsChildView.mm
+++ b/widget/cocoa/nsChildView.mm
@@ -171,17 +171,16 @@ static NSMutableDictionary* sNativeKeyEv
 - (void) convertCocoaMouseEvent:(NSEvent*)aMouseEvent
                    toGeckoEvent:(WidgetInputEvent*)outGeckoEvent;
 - (void) convertCocoaTabletPointerEvent:(NSEvent*)aMouseEvent
                            toGeckoEvent:(WidgetMouseEvent*)outGeckoEvent;
 - (NSMenu*)contextMenu;
 
 - (BOOL)isRectObscuredBySubview:(NSRect)inRect;
 
-- (void)drawRect:(NSRect)aRect inContext:(CGContextRef)aContext;
 - (LayoutDeviceIntRegion)nativeDirtyRegionWithBoundingRect:(NSRect)aRect;
 - (BOOL)isUsingOpenGL;
 
 - (BOOL)hasRoundedBottomCorners;
 - (CGFloat)cornerRadius;
 - (void)clearCorners;
 
 -(void)setGLOpaque:(BOOL)aOpaque;
@@ -3626,24 +3625,20 @@ NSEvent* gLastDragMouseDownEvent = nil;
   region.And(region, boundingRect);
   return region;
 }
 
 // The display system has told us that a portion of our view is dirty. Tell
 // gecko to paint it
 - (void)drawRect:(NSRect)aRect
 {
-  CGContextRef cgContext = (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort];
-  [self drawRect:aRect inContext:cgContext];
-}
-
-- (void)drawRect:(NSRect)aRect inContext:(CGContextRef)aContext
-{
   if (!mGeckoChild || !mGeckoChild->IsVisible())
     return;
+  CGContextRef cgContext =
+    (CGContextRef)[[NSGraphicsContext currentContext] graphicsPort];
 
   if ([self isUsingOpenGL]) {
     // Since this view is usually declared as opaque, the window's pixel
     // buffer may now contain garbage which we need to prevent from reaching
     // the screen. The only place where garbage can show is in the window
     // corners and the vibrant regions of the window - the rest of the window
     // is covered by opaque content in our OpenGL surface.
     // So we need to clear the pixel buffer contents in these areas.
@@ -3659,40 +3654,41 @@ NSEvent* gLastDragMouseDownEvent = nil;
 
   AUTO_PROFILER_LABEL("ChildView::drawRect", GRAPHICS);
 
   // The CGContext that drawRect supplies us with comes with a transform that
   // scales one user space unit to one Cocoa point, which can consist of
   // multiple dev pixels. But Gecko expects its supplied context to be scaled
   // to device pixels, so we need to reverse the scaling.
   double scale = mGeckoChild->BackingScaleFactor();
-  CGContextSaveGState(aContext);
-  CGContextScaleCTM(aContext, 1.0 / scale, 1.0 / scale);
+  CGContextSaveGState(cgContext);
+  CGContextScaleCTM(cgContext, 1.0 / scale, 1.0 / scale);
 
   NSSize viewSize = [self bounds].size;
   gfx::IntSize backingSize = gfx::IntSize::Truncate(viewSize.width * scale, viewSize.height * scale);
   LayoutDeviceIntRegion region = [self nativeDirtyRegionWithBoundingRect:aRect];
 
-  bool painted = mGeckoChild->PaintWindowInContext(aContext, region, backingSize);
+  bool painted =
+    mGeckoChild->PaintWindowInContext(cgContext, region, backingSize);
 
   // Undo the scale transform so that from now on the context is in
   // CocoaPoints again.
-  CGContextRestoreGState(aContext);
+  CGContextRestoreGState(cgContext);
 
   if (!painted && [self isOpaque]) {
     // Gecko refused to draw, but we've claimed to be opaque, so we have to
     // draw something--fill with white.
-    CGContextSetRGBFillColor(aContext, 1, 1, 1, 1);
-    CGContextFillRect(aContext, NSRectToCGRect(aRect));
+    CGContextSetRGBFillColor(cgContext, 1, 1, 1, 1);
+    CGContextFillRect(cgContext, NSRectToCGRect(aRect));
   }
 
   if ([self isCoveringTitlebar]) {
     [self drawTitleString];
     [self drawTitlebarHighlight];
-    [self maskTopCornersInContext:aContext];
+    [self maskTopCornersInContext:cgContext];
   }
 }
 
 - (BOOL)isUsingOpenGL
 {
   if (!mGeckoChild || ![self window])
     return NO;
 
