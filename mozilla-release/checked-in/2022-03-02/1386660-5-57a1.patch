# HG changeset patch
# User Paul Bone <pbone@mozilla.com>
# Date 1502934939 -36000
# Node ID f1fcbcb67951f04de3e86909c205e7aa2798319f
# Parent  debcef2bb3c74b71d0ae3c28b310d4c8db5d8e16
Bug 1386660 - Part 5: Add constants to avoid multiple hard-coded values. r=sfink

diff --git a/js/src/gc/Marking.cpp b/js/src/gc/Marking.cpp
--- a/js/src/gc/Marking.cpp
+++ b/js/src/gc/Marking.cpp
@@ -2357,17 +2357,17 @@ MarkStackIter::saveValueArray(NativeObje
 /*
  * ExpandWeakMaps: the GC is recomputing the liveness of WeakMap entries by
  * expanding each live WeakMap into its constituent key->value edges, a table
  * of which will be consulted in a later phase whenever marking a potential
  * key.
  */
 GCMarker::GCMarker(JSRuntime* rt)
   : JSTracer(rt, JSTracer::TracerKindTag::Marking, ExpandWeakMaps),
-    stack(size_t(-1)),
+    stack(),
     color(MarkColor::Black),
     unmarkedArenaStackTop(nullptr)
 #ifdef DEBUG
   , markLaterArenas(0)
   , started(false)
   , strictCompartmentChecking(false)
 #endif
 {
diff --git a/js/src/gc/Marking.h b/js/src/gc/Marking.h
--- a/js/src/gc/Marking.h
+++ b/js/src/gc/Marking.h
@@ -109,19 +109,21 @@ class MarkStack
     {
         SavedValueArray(JSObject* obj, size_t index, HeapSlot::Kind kind);
 
         uintptr_t kind;
         uintptr_t index;
         TaggedPtr ptr;
     };
 
-    explicit MarkStack(size_t maxCapacity);
+    explicit MarkStack(size_t maxCapacity = DefaultCapacity);
     ~MarkStack();
 
+    static const size_t DefaultCapacity = SIZE_MAX;
+
     size_t capacity() { return end_ - stack_; }
 
     size_t position() const {
         auto result = tos_ - stack_;
         MOZ_ASSERT(result >= 0);
         return size_t(result);
     }
 
diff --git a/js/src/jsgc.cpp b/js/src/jsgc.cpp
--- a/js/src/jsgc.cpp
+++ b/js/src/jsgc.cpp
@@ -260,16 +260,52 @@ using mozilla::Get;
 using mozilla::HashCodeScrambler;
 using mozilla::Maybe;
 using mozilla::Move;
 using mozilla::Swap;
 using mozilla::TimeStamp;
 
 using JS::AutoGCRooter;
 
+/*
+ * Default settings for tuning the GC.  Some of these can be set at runtime,
+ * This list is not complete, some tuning parameters are not listed here.
+ */
+namespace js {
+namespace gc {
+namespace TuningDefaults {
+
+    static const size_t GCZoneAllocThresholdBase = 30 * 1024 * 1024;
+    static const float ZoneAllocThresholdFactor = 0.9f;
+    static const float ZoneAllocThresholdFactorAvoidInterrupt = 0.9f;
+    static const size_t ZoneAllocDelayBytes = 1024 * 1024;
+    static const bool DynamicHeapGrowthEnabled = false;
+    static const uint64_t HighFrequencyThresholdUsec = 1000000;
+    static const uint64_t HighFrequencyLowLimitBytes = 100 * 1024 * 1024;
+    static const uint64_t HighFrequencyHighLimitBytes = 500 * 1024 * 1024;
+    static const double HighFrequencyHeapGrowthMax = 3.0;
+    static const double HighFrequencyHeapGrowthMin = 1.5;
+    static const double LowFrequencyHeapGrowth = 1.5;
+    static const bool DynamicMarkSliceEnabled = false;
+    static const bool RefreshFrameSlicesEnabled = true;
+    static const uint32_t MinEmptyChunkCount = 1;
+    static const uint32_t MaxEmptyChunkCount = 30;
+
+    /*
+     * JSGC_SLICE_TIME_BUDGET.
+     * javascript.options.mem.gc_incremental_slice_ms
+     */
+    static const int64_t DefaultTimeBudget =
+        SliceBudget::UnlimitedTimeBudget;
+
+    static const JSGCMode Mode = JSGC_MODE_INCREMENTAL;
+
+    static const bool CompactingEnabled = true;
+}}} // namespace js::gc::TuningDefaults
+
 /* Increase the IGC marking slice time if we are in highFrequencyGC mode. */
 static const int IGC_MARK_SLICE_MULTIPLIER = 2;
 
 const AllocKind gc::slotsToThingKind[] = {
     /*  0 */ AllocKind::OBJECT0,  AllocKind::OBJECT2,  AllocKind::OBJECT2,  AllocKind::OBJECT4,
     /*  4 */ AllocKind::OBJECT4,  AllocKind::OBJECT8,  AllocKind::OBJECT8,  AllocKind::OBJECT8,
     /*  8 */ AllocKind::OBJECT8,  AllocKind::OBJECT12, AllocKind::OBJECT12, AllocKind::OBJECT12,
     /* 12 */ AllocKind::OBJECT12, AllocKind::OBJECT16, AllocKind::OBJECT16, AllocKind::OBJECT16,
@@ -825,17 +861,17 @@ GCRuntime::GCRuntime(JSRuntime* rt) :
     stats_(rt),
     marker(rt),
     usage(nullptr),
     nextCellUniqueId_(LargestTaggedNullCellPointer + 1), // Ensure disjoint from null tagged pointers.
     numArenasFreeCommitted(0),
     verifyPreData(nullptr),
     chunkAllocationSinceLastGC(false),
     lastGCTime(PRMJ_Now()),
-    mode(JSGC_MODE_INCREMENTAL),
+    mode(TuningDefaults::Mode),
     numActiveZoneIters(0),
     cleanUpEverything(false),
     grayBufferState(GCRuntime::GrayBufferState::Unused),
     grayBitsValid(false),
     majorGCTriggerReason(JS::gcreason::NO_REASON),
     fullGCForAtomsRequested_(false),
     minorGCNumber(0),
     majorGCNumber(0),
@@ -853,19 +889,19 @@ GCRuntime::GCRuntime(JSRuntime* rt) :
     abortSweepAfterCurrentGroup(false),
     arenasAllocatedDuringSweep(nullptr),
     startedCompacting(false),
     relocatedArenasToRelease(nullptr),
 #ifdef JS_GC_ZEAL
     markingValidator(nullptr),
 #endif
     interFrameGC(false),
-    defaultTimeBudget_((int64_t) SliceBudget::UnlimitedTimeBudget),
+    defaultTimeBudget_(TuningDefaults::DefaultTimeBudget),
     incrementalAllowed(true),
-    compactingEnabled(true),
+    compactingEnabled(TuningDefaults::CompactingEnabled),
     rootsRemoved(false),
 #ifdef JS_GC_ZEAL
     zealModeBits(0),
     zealFrequency(0),
     nextScheduled(0),
     deterministicOnly(false),
     incrementalLimit(0),
 #endif
@@ -1326,74 +1362,57 @@ void
 GCSchedulingTunables::setMaxEmptyChunkCount(uint32_t value)
 {
     maxEmptyChunkCount_ = value;
     if (minEmptyChunkCount_ > maxEmptyChunkCount_)
         minEmptyChunkCount_ = maxEmptyChunkCount_;
     MOZ_ASSERT(maxEmptyChunkCount_ >= minEmptyChunkCount_);
 }
 
-static const size_t GCZoneAllocThresholdBaseDefault = 30 * 1024 * 1024;
-static const float ZoneAllocThresholdFactorDefault = 0.9f;
-static const float ZoneAllocThresholdFactorAvoidInterruptDefault = 0.9f;
-static const size_t ZoneAllocDelayBytesDefault = 1024 * 1024;
-static const bool DynamicHeapGrowthEnabledDefault = false;
-static const uint64_t HighFrequencyThresholdUsecDefault = 1000000;
-static const uint64_t HighFrequencyLowLimitBytesDefault = 100 * 1024 * 1024;
-static const uint64_t HighFrequencyHighLimitBytesDefault = 500 * 1024 * 1024;
-static const double HighFrequencyHeapGrowthMaxDefault = 3.0;
-static const double HighFrequencyHeapGrowthMinDefault = 1.5;
-static const double LowFrequencyHeapGrowthDefault = 1.5;
-static const bool DynamicMarkSliceEnabledDefault = false;
-static const bool RefreshFrameSlicesEnabledDefault = true;
-static const uint32_t MinEmptyChunkCountDefault = 1;
-static const uint32_t MaxEmptyChunkCountDefault = 30;
-
 GCSchedulingTunables::GCSchedulingTunables()
   : gcMaxBytes_(0),
     gcMaxNurseryBytes_(0),
-    gcZoneAllocThresholdBase_(GCZoneAllocThresholdBaseDefault),
-    zoneAllocThresholdFactor_(ZoneAllocThresholdFactorDefault),
+    gcZoneAllocThresholdBase_(TuningDefaults::GCZoneAllocThresholdBase),
+    zoneAllocThresholdFactor_(TuningDefaults::ZoneAllocThresholdFactor),
     zoneAllocThresholdFactorAvoidInterrupt_(
-        ZoneAllocThresholdFactorAvoidInterruptDefault),
-    zoneAllocDelayBytes_(ZoneAllocDelayBytesDefault),
-    dynamicHeapGrowthEnabled_(DynamicHeapGrowthEnabledDefault),
-    highFrequencyThresholdUsec_(HighFrequencyThresholdUsecDefault),
-    highFrequencyLowLimitBytes_(HighFrequencyLowLimitBytesDefault),
-    highFrequencyHighLimitBytes_(HighFrequencyHighLimitBytesDefault),
-    highFrequencyHeapGrowthMax_(HighFrequencyHeapGrowthMaxDefault),
-    highFrequencyHeapGrowthMin_(HighFrequencyHeapGrowthMinDefault),
-    lowFrequencyHeapGrowth_(LowFrequencyHeapGrowthDefault),
-    dynamicMarkSliceEnabled_(DynamicMarkSliceEnabledDefault),
-    refreshFrameSlicesEnabled_(RefreshFrameSlicesEnabledDefault),
-    minEmptyChunkCount_(MinEmptyChunkCountDefault),
-    maxEmptyChunkCount_(MaxEmptyChunkCountDefault)
+        TuningDefaults::ZoneAllocThresholdFactorAvoidInterrupt),
+    zoneAllocDelayBytes_(TuningDefaults::ZoneAllocDelayBytes),
+    dynamicHeapGrowthEnabled_(TuningDefaults::DynamicHeapGrowthEnabled),
+    highFrequencyThresholdUsec_(TuningDefaults::HighFrequencyThresholdUsec),
+    highFrequencyLowLimitBytes_(TuningDefaults::HighFrequencyLowLimitBytes),
+    highFrequencyHighLimitBytes_(TuningDefaults::HighFrequencyHighLimitBytes),
+    highFrequencyHeapGrowthMax_(TuningDefaults::HighFrequencyHeapGrowthMax),
+    highFrequencyHeapGrowthMin_(TuningDefaults::HighFrequencyHeapGrowthMin),
+    lowFrequencyHeapGrowth_(TuningDefaults::LowFrequencyHeapGrowth),
+    dynamicMarkSliceEnabled_(TuningDefaults::DynamicMarkSliceEnabled),
+    refreshFrameSlicesEnabled_(TuningDefaults::RefreshFrameSlicesEnabled),
+    minEmptyChunkCount_(TuningDefaults::MinEmptyChunkCount),
+    maxEmptyChunkCount_(TuningDefaults::MaxEmptyChunkCount)
 {}
 
 void
 GCRuntime::resetParameter(JSGCParamKey key, AutoLockGC& lock)
 {
     switch (key) {
       case JSGC_MAX_MALLOC_BYTES:
         setMaxMallocBytes(0xffffffff);
         for (ZonesIter zone(rt, WithAtoms); !zone.done(); zone.next())
             zone->setGCMaxMallocBytes(maxMallocBytesAllocated() * 0.9);
         break;
       case JSGC_SLICE_TIME_BUDGET:
-        defaultTimeBudget_ =
-            static_cast<int64_t>(SliceBudget::UnlimitedTimeBudget);
+        defaultTimeBudget_ = TuningDefaults::DefaultTimeBudget;
         break;
       case JSGC_MARK_STACK_LIMIT:
-        setMarkStackLimit(size_t(-1), lock);
+        setMarkStackLimit(MarkStack::DefaultCapacity, lock);
         break;
       case JSGC_MODE:
-        mode = JSGC_MODE_INCREMENTAL;
+        mode = TuningDefaults::Mode;
         break;
       case JSGC_COMPACTING_ENABLED:
-        compactingEnabled = true;
+        compactingEnabled = TuningDefaults::CompactingEnabled;
         break;
       default:
         tunables.resetParameter(key, lock);
         for (ZonesIter zone(rt, WithAtoms); !zone.done(); zone.next()) {
             zone->threshold.updateAfterGC(zone->usage.gcBytes(), GC_NORMAL,
                 tunables, schedulingState, lock);
         }
     }
@@ -1405,53 +1424,56 @@ GCSchedulingTunables::resetParameter(JSG
     switch(key) {
       case JSGC_MAX_BYTES:
         gcMaxBytes_ = 0xffffffff;
         break;
       case JSGC_MAX_NURSERY_BYTES:
         gcMaxNurseryBytes_ = JS::DefaultNurseryBytes;
         break;
       case JSGC_HIGH_FREQUENCY_TIME_LIMIT:
-        highFrequencyThresholdUsec_ = HighFrequencyThresholdUsecDefault;
+        highFrequencyThresholdUsec_ =
+            TuningDefaults::HighFrequencyThresholdUsec;
         break;
       case JSGC_HIGH_FREQUENCY_LOW_LIMIT:
-        setHighFrequencyLowLimit(HighFrequencyLowLimitBytesDefault);
+        setHighFrequencyLowLimit(TuningDefaults::HighFrequencyLowLimitBytes);
         break;
       case JSGC_HIGH_FREQUENCY_HIGH_LIMIT:
-        setHighFrequencyHighLimit(HighFrequencyHighLimitBytesDefault);
+        setHighFrequencyHighLimit(TuningDefaults::HighFrequencyHighLimitBytes);
         break;
       case JSGC_HIGH_FREQUENCY_HEAP_GROWTH_MAX:
-        highFrequencyHeapGrowthMax_ = HighFrequencyHeapGrowthMaxDefault;
+        highFrequencyHeapGrowthMax_ =
+            TuningDefaults::HighFrequencyHeapGrowthMax;
         MOZ_ASSERT(highFrequencyHeapGrowthMax_ / 0.85 > 1.0);
         break;
       case JSGC_HIGH_FREQUENCY_HEAP_GROWTH_MIN:
-        highFrequencyHeapGrowthMin_ = HighFrequencyHeapGrowthMinDefault;
+        highFrequencyHeapGrowthMin_ =
+            TuningDefaults::HighFrequencyHeapGrowthMin;
         MOZ_ASSERT(highFrequencyHeapGrowthMin_ / 0.85 > 1.0);
         break;
       case JSGC_LOW_FREQUENCY_HEAP_GROWTH:
-        lowFrequencyHeapGrowth_ = LowFrequencyHeapGrowthDefault;
+        lowFrequencyHeapGrowth_ = TuningDefaults::LowFrequencyHeapGrowth;
         MOZ_ASSERT(lowFrequencyHeapGrowth_ / 0.9 > 1.0);
         break;
       case JSGC_DYNAMIC_HEAP_GROWTH:
-        dynamicHeapGrowthEnabled_ = DynamicHeapGrowthEnabledDefault;
+        dynamicHeapGrowthEnabled_ = TuningDefaults::DynamicHeapGrowthEnabled;
         break;
       case JSGC_DYNAMIC_MARK_SLICE:
-        dynamicMarkSliceEnabled_ = DynamicMarkSliceEnabledDefault;
+        dynamicMarkSliceEnabled_ = TuningDefaults::DynamicMarkSliceEnabled;
         break;
       case JSGC_ALLOCATION_THRESHOLD:
-        gcZoneAllocThresholdBase_ = GCZoneAllocThresholdBaseDefault;
+        gcZoneAllocThresholdBase_ = TuningDefaults::GCZoneAllocThresholdBase;
         break;
       case JSGC_MIN_EMPTY_CHUNK_COUNT:
-        setMinEmptyChunkCount(MinEmptyChunkCountDefault);
+        setMinEmptyChunkCount(TuningDefaults::MinEmptyChunkCount);
         break;
       case JSGC_MAX_EMPTY_CHUNK_COUNT:
-        setMaxEmptyChunkCount(MaxEmptyChunkCountDefault);
+        setMaxEmptyChunkCount(TuningDefaults::MaxEmptyChunkCount);
         break;
       case JSGC_REFRESH_FRAME_SLICES_ENABLED:
-        refreshFrameSlicesEnabled_ = RefreshFrameSlicesEnabledDefault;
+        refreshFrameSlicesEnabled_ = TuningDefaults::RefreshFrameSlicesEnabled;
         break;
       default:
         MOZ_CRASH("Unknown GC parameter.");
     }
 }
 
 uint32_t
 GCRuntime::getParameter(JSGCParamKey key, const AutoLockGC& lock)
