# HG changeset patch
# User Jason Tarka <jason@tarka.ca>
# Date 1500315597 14400
# Node ID f8ee927a4c3761592bb971081ee0fd839818eed2
# Parent  83bf6b3bffbfcdd0025dda5f2154039b9feb75d9
Bug 1380755 - Examine & report on frame-ancestors CSP in report-only mode. r=ckerschb

Despite what the comment here says, there is nowhere in the W3C CSP spec stating
that frame-ancestors should be ignored in report-only mode.

diff --git a/dom/security/nsCSPContext.cpp b/dom/security/nsCSPContext.cpp
--- a/dom/security/nsCSPContext.cpp
+++ b/dom/security/nsCSPContext.cpp
@@ -218,24 +218,16 @@ nsCSPContext::permitsInternal(CSPDirecti
                               bool aSendViolationReports,
                               bool aSendContentLocationInViolationReports,
                               bool aParserCreated)
 {
   bool permits = true;
 
   nsAutoString violatedDirective;
   for (uint32_t p = 0; p < mPolicies.Length(); p++) {
-
-    // According to the W3C CSP spec, frame-ancestors checks are ignored for
-    // report-only policies (when "monitoring").
-    if (aDir == nsIContentSecurityPolicy::FRAME_ANCESTORS_DIRECTIVE &&
-        mPolicies[p]->getReportOnlyFlag()) {
-      continue;
-    }
-
     if (!mPolicies[p]->permits(aDir,
                                aContentLocation,
                                aNonce,
                                aWasRedirected,
                                aSpecific,
                                aParserCreated,
                                violatedDirective)) {
       // If the policy is violated and not report-only, reject the load and
diff --git a/dom/security/test/csp/file_frame_ancestors_ro.html b/dom/security/test/csp/file_frame_ancestors_ro.html
new file mode 100644
--- /dev/null
+++ b/dom/security/test/csp/file_frame_ancestors_ro.html
@@ -0,0 +1,1 @@
+<html><body>Child Document</body></html>
diff --git a/dom/security/test/csp/file_frame_ancestors_ro.html^headers^ b/dom/security/test/csp/file_frame_ancestors_ro.html^headers^
new file mode 100644
--- /dev/null
+++ b/dom/security/test/csp/file_frame_ancestors_ro.html^headers^
@@ -0,0 +1,1 @@
+Content-Security-Policy-Report-Only: frame-ancestors 'none'; report-uri http://mochi.test:8888/foo.sjs
diff --git a/dom/security/test/csp/mochitest.ini b/dom/security/test/csp/mochitest.ini
--- a/dom/security/test/csp/mochitest.ini
+++ b/dom/security/test/csp/mochitest.ini
@@ -85,16 +85,18 @@ support-files =
   file_policyuri_regression_from_multipolicy.html
   file_policyuri_regression_from_multipolicy.html^headers^
   file_policyuri_regression_from_multipolicy_policy
   file_nonce_source.html
   file_nonce_source.html^headers^
   file_bug941404.html
   file_bug941404_xhr.html
   file_bug941404_xhr.html^headers^
+  file_frame_ancestors_ro.html
+  file_frame_ancestors_ro.html^headers^
   file_hash_source.html
   file_dual_header_testserver.sjs
   file_hash_source.html^headers^
   file_scheme_relative_sources.js
   file_scheme_relative_sources.sjs
   file_ignore_unsafe_inline.html
   file_ignore_unsafe_inline_multiple_policies_server.sjs
   file_self_none_as_hostname_confusion.html
@@ -240,16 +242,17 @@ skip-if = toolkit == 'android' # Times o
 [test_inlinestyle.html]
 [test_invalid_source_expression.html]
 [test_bug836922_npolicies.html]
 [test_bug886164.html]
 [test_redirects.html]
 [test_bug910139.html]
 [test_bug909029.html]
 [test_bug1229639.html]
+[test_frame_ancestors_ro.html]
 [test_policyuri_regression_from_multipolicy.html]
 [test_nonce_source.html]
 [test_bug941404.html]
 [test_form-action.html]
 [test_hash_source.html]
 [test_scheme_relative_sources.html]
 [test_ignore_unsafe_inline.html]
 [test_self_none_as_hostname_confusion.html]
diff --git a/dom/security/test/csp/test_frame_ancestors_ro.html b/dom/security/test/csp/test_frame_ancestors_ro.html
new file mode 100644
--- /dev/null
+++ b/dom/security/test/csp/test_frame_ancestors_ro.html
@@ -0,0 +1,69 @@
+<!DOCTYPE HTML>
+<html>
+<head>
+  <title>Test for frame-ancestors support in Content-Security-Policy-Report-Only</title>
+  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
+</head>
+<body>
+<iframe style="width: 100%" id="cspframe"></iframe>
+<script type="text/javascript">
+const docUri = "http://mochi.test:8888/tests/dom/security/test/csp/file_frame_ancestors_ro.html";
+const frame = document.getElementById("cspframe");
+
+let testResults = {
+  reportFired: false,
+  frameLoaded: false
+};
+
+function checkResults(reportObj) {
+  let cspReport = reportObj["csp-report"];
+  is(cspReport["document-uri"], docUri, "Incorrect document-uri");
+
+  // we can not test for the whole referrer since it includes platform specific information
+  is(cspReport["referrer"], document.location.toString(), "Incorrect referrer");
+  is(cspReport["blocked-uri"], document.location.toString(), "Incorrect blocked-uri");
+  is(cspReport["violated-directive"], "frame-ancestors 'none'", "Incorrect violated-directive");
+  is(cspReport["original-policy"], "frame-ancestors 'none'; report-uri http://mochi.test:8888/foo.sjs", "Incorrect original-policy");
+  testResults.reportFired = true;
+}
+
+let chromeScriptUrl = SimpleTest.getTestFileURL("file_report_chromescript.js");
+let script = SpecialPowers.loadChromeScript(chromeScriptUrl);
+
+script.addMessageListener('opening-request-completed', function ml(msg) {
+  if (msg.error) {
+    ok(false, "Could not query report (exception: " + msg.error + ")");
+  } else {
+    try {
+      let reportObj = JSON.parse(msg.report);
+      // test for the proper values in the report object
+      checkResults(reportObj);
+    } catch (e) {
+      ok(false, "Error verifying report object (exception: " + e + ")");
+    }
+  }
+
+  script.removeMessageListener('opening-request-completed', ml);
+  script.sendAsyncMessage("finish");
+  checkTestResults();
+});
+
+frame.addEventListener( 'load', () => {
+  // Make sure the frame is still loaded
+  testResults.frameLoaded = true;
+  checkTestResults()
+} );
+
+function checkTestResults() {
+  if( testResults.reportFired && testResults.frameLoaded ) {
+    SimpleTest.finish();
+  }
+}
+
+SimpleTest.waitForExplicitFinish();
+frame.src = docUri;
+
+</script>
+</body>
+</html>
