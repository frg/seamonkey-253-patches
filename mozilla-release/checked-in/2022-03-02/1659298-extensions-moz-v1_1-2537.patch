# HG changeset patch
# User Dmitry Butskoy <dmitry@butskoy.name>
# Date 1608991044 0
# Parent  df810afbb3ddb63eca115828ed3cefdee836df06
Bug 1659298 - Get rid of distribution extensions in favor of application global - mozilla patch. r=frg a=frg

diff --git a/config/rules.mk b/config/rules.mk
--- a/config/rules.mk
+++ b/config/rules.mk
@@ -1279,19 +1279,19 @@ endif
 
 # See comment above about moving this out of the tools tier.
 ifdef INSTALL_EXTENSION_ID
 ifndef XPI_NAME
 $(error XPI_NAME must be set for INSTALL_EXTENSION_ID)
 endif
 
 tools::
-	$(RM) -r '$(DIST)/bin/distribution$(DIST_SUBDIR:%=/%)/extensions/$(INSTALL_EXTENSION_ID)'
-	$(NSINSTALL) -D '$(DIST)/bin/distribution$(DIST_SUBDIR:%=/%)/extensions/$(INSTALL_EXTENSION_ID)'
-	$(call copy_dir,$(FINAL_TARGET),$(DIST)/bin/distribution$(DIST_SUBDIR:%=/%)/extensions/$(INSTALL_EXTENSION_ID))
+	$(RM) -r '$(DIST)/bin$(DIST_SUBDIR:%=/%)/extensions/$(INSTALL_EXTENSION_ID)'
+	$(NSINSTALL) -D '$(DIST)/bin$(DIST_SUBDIR:%=/%)/extensions/$(INSTALL_EXTENSION_ID)'
+	$(call copy_dir,$(FINAL_TARGET),$(DIST)/bin$(DIST_SUBDIR:%=/%)/extensions/$(INSTALL_EXTENSION_ID))
 
 endif
 
 #############################################################################
 # MDDEPDIR is the subdirectory where all the dependency files are placed.
 #   This uses a make rule (instead of a macro) to support parallel
 #   builds (-jN). If this were done in the LOOP_OVER_DIRS macro, two
 #   processes could simultaneously try to create the same directory.
diff --git a/toolkit/mozapps/extensions/internal/XPIProvider.jsm b/toolkit/mozapps/extensions/internal/XPIProvider.jsm
--- a/toolkit/mozapps/extensions/internal/XPIProvider.jsm
+++ b/toolkit/mozapps/extensions/internal/XPIProvider.jsm
@@ -74,16 +74,17 @@ const PREF_XPI_DIRECT_WHITELISTED     = 
 const PREF_XPI_FILE_WHITELISTED       = "xpinstall.whitelist.fileRequest";
 // xpinstall.signatures.required only supported in dev builds
 const PREF_XPI_SIGNATURES_REQUIRED    = "xpinstall.signatures.required";
 const PREF_XPI_SIGNATURES_DEV_ROOT    = "xpinstall.signatures.dev-root";
 const PREF_XPI_PERMISSIONS_BRANCH     = "xpinstall.";
 const PREF_INSTALL_REQUIRESECUREORIGIN = "extensions.install.requireSecureOrigin";
 const PREF_INSTALL_DISTRO_ADDONS      = "extensions.installDistroAddons";
 const PREF_BRANCH_INSTALLED_ADDON     = "extensions.installedDistroAddon.";
+const PREF_DISTRO_ADDONS_CHECK        = "extensions.distroAddons.check";
 const PREF_INTERPOSITION_ENABLED      = "extensions.interposition.enabled";
 const PREF_SYSTEM_ADDON_SET           = "extensions.systemAddonSet";
 const PREF_SYSTEM_ADDON_UPDATE_URL    = "extensions.systemAddon.update.url";
 const PREF_E10S_BLOCK_ENABLE          = "extensions.e10sBlocksEnabling";
 const PREF_E10S_ADDON_BLOCKLIST       = "extensions.e10s.rollout.blocklist";
 const PREF_E10S_ADDON_POLICY          = "extensions.e10s.rollout.policy";
 const PREF_E10S_HAS_NONEXEMPT_ADDON   = "extensions.e10s.rollout.hasAddon";
 const PREF_ALLOW_LEGACY               = "extensions.legacy.enabled";
@@ -3093,16 +3094,104 @@ this.XPIProvider = {
     // If the application has changed then check for new distribution add-ons
     if (Services.prefs.getBoolPref(PREF_INSTALL_DISTRO_ADDONS, true)) {
       updated = this.installDistributionAddons(manifests, aAppChanged);
       if (updated) {
         updateReasons.push("installDistributionAddons");
       }
     }
 
+    // Check whether some KEY_APP_GLOBAL addons were previously "distribution"
+    // add-ons (ie. moved from "distribution/extensions" just to "extensions").
+    // In such a case, a copy of the previous "distribution" add-on still
+    // remains in the user profile (because of highest priority) and should be
+    // deleted.
+
+    // Fortunately, all such add-ons have the pref
+    // "PREF_BRANCH_INSTALLED_ADDON + id" set, which means that the add-on was
+    // not installed by the user (manually), but just was copied automatically
+    // from a system location.
+
+    if (aAppChanged ||
+        !Services.prefs.getBoolPref(PREF_DISTRO_ADDONS_CHECK, false)) {
+      Services.prefs.setBoolPref(PREF_DISTRO_ADDONS_CHECK, true);
+
+      let systemLocation = this.installLocationsByName[KEY_APP_GLOBAL];
+      let profileLocation = this.installLocationsByName[KEY_APP_PROFILE];
+
+      let prefList = Services.prefs.getChildList(PREF_BRANCH_INSTALLED_ADDON);
+      for (let pref of prefList) {
+        let id = pref.slice(PREF_BRANCH_INSTALLED_ADDON.length);
+
+        // logger.warn("ID = ${ext}", {ext: id});
+
+        let systemEntry = null;
+        try {
+          systemEntry = systemLocation.getLocationForID(id);
+        } catch (e) {
+        }
+        let profileEntry = null;
+        try {
+          profileEntry = profileLocation.getLocationForID(id);
+        } catch (e) {
+        }
+
+        // Catch the case, remove the preference.
+        Services.prefs.clearUserPref(PREF_BRANCH_INSTALLED_ADDON + id);
+
+        // If there is an orphaned entry in the user profile, remove it.
+        if (!systemEntry && profileEntry)
+          profileLocation.uninstallAddon(id);
+
+        if (!systemEntry || !profileEntry)
+          continue;
+
+        let systemAddon;
+        let profileAddon;
+        try {
+          systemAddon = syncLoadManifestFromFile(systemEntry, systemLocation);
+          profileAddon = syncLoadManifestFromFile(profileEntry, profileLocation);
+
+          // Allow the user to have a more recent version in their profile.
+          if (Services.vc.compare(systemAddon.version, profileAddon.version) < 0)
+            continue;
+        } catch (e) {
+          continue;
+        }
+
+        // Preserve userDisabled state, normally XPIDatabase loaded a bit later.
+        XPIDatabase.syncLoadDB(false);
+        let addonDB = XPIDatabase.getAddons().find(addonDB => addonDB.id == id);
+        if (addonDB) {
+          systemAddon.userDisabled = addonDB.userDisabled;
+        }
+
+        // Remove the old add-on from the user profile.
+        profileLocation.uninstallAddon(id);
+
+        // Complete the system add-on handling.
+        try {
+          XPIProvider._addURIMapping(id, systemEntry);
+          XPIStates.addAddon(systemAddon);
+          logger.debug("Removed the distribution add-on " + id + " due to an existing system one");
+
+          if (KEY_APP_PROFILE in manifests)
+            delete manifests[KEY_APP_PROFILE][id];
+          if (!(KEY_APP_GLOBAL in manifests))
+            manifests[KEY_APP_GLOBAL] = {};
+          manifests[KEY_APP_GLOBAL][id] = systemAddon;
+
+        } catch (e) {
+          logger.error("Failed to add system add-on " + systemEntry.path, e);
+        }
+      }
+
+      // No need for an extra updateReasons, since aAppChanged == true anyway.
+    }
+
     let haveAnyAddons = (XPIStates.size > 0);
 
     // If the schema appears to have changed then we should update the database
     if (DB_SCHEMA != Services.prefs.getIntPref(PREF_DB_SCHEMA, 0)) {
       // If we don't have any add-ons, just update the pref, since we don't need to
       // write the database
       if (!haveAnyAddons) {
         logger.debug("Empty XPI database, setting schema version preference to " + DB_SCHEMA);
