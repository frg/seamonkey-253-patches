# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1503569644 -32400
#      Thu Aug 24 19:14:04 2017 +0900
# Node ID a69d5ede21ba487ffad8cc9d8a399b732dd7d43d
# Parent  8e9bbdad25fa8c90009e8bf58eef5c8c853d0888
Bug 1393348 - part2: nsISelectionController::SELECTION_* should be sequential integer values r=smaug

nsISelectionController::SELECTION_* are declared as bit-mask.  However, no
methods of nsISelectionController treat them as bit-mask and these
values need a switch statement in nsFrameSelection to convert SelectionType to
array index of nsFrameSelection::mDOMSelections because it's too big to create
an array to do it.  Additionally, this conversion appears profile of
attachment 8848015.

So, now, we should declare these values as sequential integer values.

However, only nsTextFrame uses these values as bit-mask.  Therefore, this patch
adds new type, SelectionTypeMask and creates new inline method,
ToSelectionTypeMask(SelectionType), to retrieve mask value for a SelectionType.

MozReview-Commit-ID: 5Za8mA6iu4

diff --git a/dom/base/Selection.h b/dom/base/Selection.h
--- a/dom/base/Selection.h
+++ b/dom/base/Selection.h
@@ -518,60 +518,50 @@ public:
       mSelection->RemoveSelectionChangeBlocker();
     }
   }
 };
 
 } // namespace dom
 
 inline bool
-IsValidSelectionType(RawSelectionType aRawSelectionType)
+IsValidRawSelectionType(RawSelectionType aRawSelectionType)
 {
-  switch (static_cast<SelectionType>(aRawSelectionType)) {
-    case SelectionType::eNone:
-    case SelectionType::eNormal:
-    case SelectionType::eSpellCheck:
-    case SelectionType::eIMERawClause:
-    case SelectionType::eIMESelectedRawClause:
-    case SelectionType::eIMEConvertedClause:
-    case SelectionType::eIMESelectedClause:
-    case SelectionType::eAccessibility:
-    case SelectionType::eFind:
-    case SelectionType::eURLSecondary:
-    case SelectionType::eURLStrikeout:
-      return true;
-    default:
-      return false;
-  }
+  return aRawSelectionType >= nsISelectionController::SELECTION_NONE &&
+         aRawSelectionType <= nsISelectionController::SELECTION_URLSTRIKEOUT;
 }
 
 inline SelectionType
 ToSelectionType(RawSelectionType aRawSelectionType)
 {
-  if (!IsValidSelectionType(aRawSelectionType)) {
+  if (!IsValidRawSelectionType(aRawSelectionType)) {
     return SelectionType::eInvalid;
   }
   return static_cast<SelectionType>(aRawSelectionType);
 }
 
 inline RawSelectionType
 ToRawSelectionType(SelectionType aSelectionType)
 {
+  MOZ_ASSERT(aSelectionType != SelectionType::eInvalid);
   return static_cast<RawSelectionType>(aSelectionType);
 }
 
-inline RawSelectionType ToRawSelectionType(TextRangeType aTextRangeType)
+inline RawSelectionType
+ToRawSelectionType(TextRangeType aTextRangeType)
 {
   return ToRawSelectionType(ToSelectionType(aTextRangeType));
 }
 
-inline bool operator &(SelectionType aSelectionType,
-                       RawSelectionType aRawSelectionTypes)
+inline SelectionTypeMask
+ToSelectionTypeMask(SelectionType aSelectionType)
 {
-  return (ToRawSelectionType(aSelectionType) & aRawSelectionTypes) != 0;
+  MOZ_ASSERT(aSelectionType != SelectionType::eInvalid);
+  return aSelectionType == SelectionType::eNone ? 0 :
+           (1 << (static_cast<uint8_t>(aSelectionType) - 1));
 }
 
 } // namespace mozilla
 
 inline mozilla::dom::Selection*
 nsISelection::AsSelection()
 {
   return static_cast<mozilla::dom::Selection*>(this);
diff --git a/dom/base/nsISelectionController.idl b/dom/base/nsISelectionController.idl
--- a/dom/base/nsISelectionController.idl
+++ b/dom/base/nsISelectionController.idl
@@ -21,28 +21,29 @@ interface nsISelection;
 interface nsISelectionDisplay;
 
 [ptr] native SelectionPtr(mozilla::dom::Selection);
 
 [scriptable, uuid(3801c9d4-8e69-4bfc-9edb-b58278621f8f)]
 interface nsISelectionController : nsISelectionDisplay
 {
    // RawSelectionType values:
-   const short SELECTION_NONE=0;
-   const short SELECTION_NORMAL=1;
-   const short SELECTION_SPELLCHECK=2;
-   const short SELECTION_IME_RAWINPUT=4;
-   const short SELECTION_IME_SELECTEDRAWTEXT=8;
-   const short SELECTION_IME_CONVERTEDTEXT=16;
-   const short SELECTION_IME_SELECTEDCONVERTEDTEXT=32;
-   const short SELECTION_ACCESSIBILITY=64; // For accessibility API usage
-   const short SELECTION_FIND=128;
-   const short SELECTION_URLSECONDARY=256;
-   const short SELECTION_URLSTRIKEOUT=512;
-   const short NUM_SELECTIONTYPES=11;
+   const short SELECTION_NONE                      = 0;
+   const short SELECTION_NORMAL                    = 1;
+   const short SELECTION_SPELLCHECK                = 2;
+   const short SELECTION_IME_RAWINPUT              = 3;
+   const short SELECTION_IME_SELECTEDRAWTEXT       = 4;
+   const short SELECTION_IME_CONVERTEDTEXT         = 5;
+   const short SELECTION_IME_SELECTEDCONVERTEDTEXT = 6;
+   // For accessibility API usage
+   const short SELECTION_ACCESSIBILITY             = 7;
+   const short SELECTION_FIND                      = 8;
+   const short SELECTION_URLSECONDARY              = 9;
+   const short SELECTION_URLSTRIKEOUT              = 10;
+   const short NUM_SELECTIONTYPES                  = 11;
 
    // SelectionRegion values:
    const short SELECTION_ANCHOR_REGION = 0;
    const short SELECTION_FOCUS_REGION = 1;
    const short SELECTION_WHOLE_SELECTION = 2;
    const short NUM_SELECTION_REGIONS = 3;
 
    const short SELECTION_OFF = 0;
@@ -288,17 +289,25 @@ interface nsISelectionController : nsISe
 };
 %{ C++
    #define NS_ISELECTIONCONTROLLER_CID \
    { 0x513b9460, 0xd56a, 0x4c4e, \
    { 0xb6, 0xf9, 0x0b, 0x8a, 0xe4, 0x37, 0x2a, 0x3b }}
 
 namespace mozilla {
 
+// RawSelectionType should be used to store nsISelectionController::SELECTION_*.
 typedef short RawSelectionType;
+
+// SelectionTypeMask should be used to store bit-mask of selection types.
+// The value can be retrieved with ToSelectionTypeMask() and checking if
+// a selection type is in a mask with |SelectionType & SelectionTypeMask|.
+typedef uint16_t SelectionTypeMask;
+
+// SelectionType should be used in internal handling because of type safe.
 enum class SelectionType : RawSelectionType
 {
   eInvalid = -1,
   eNone = nsISelectionController::SELECTION_NONE,
   eNormal = nsISelectionController::SELECTION_NORMAL,
   eSpellCheck = nsISelectionController::SELECTION_SPELLCHECK,
   eIMERawClause = nsISelectionController::SELECTION_IME_RAWINPUT,
   eIMESelectedRawClause = nsISelectionController::SELECTION_IME_SELECTEDRAWTEXT,
@@ -315,21 +324,34 @@ enum class SelectionType : RawSelectionT
 // used at defining fixed size array in some header files (e.g.,
 // nsFrameSelection.h).  So, the values needs to be defined here, but we cannot
 // use static/const even with extern since it causes failing to link or
 // initializes them after such headers.
 enum : size_t
 {
   // kSelectionTypeCount is number of SelectionType.
   kSelectionTypeCount = nsISelectionController::NUM_SELECTIONTYPES,
-  // kPresentSelectionTypeCount is number of SelectionType except "none".
-  kPresentSelectionTypeCount = kSelectionTypeCount - 1
+};
+
+// kPresentSelectionTypes is selection types which may be displayed.
+// I.e., selection types except eNone.
+static const SelectionType kPresentSelectionTypes[] = {
+  SelectionType::eNormal,
+  SelectionType::eSpellCheck,
+  SelectionType::eIMERawClause,
+  SelectionType::eIMESelectedRawClause,
+  SelectionType::eIMEConvertedClause,
+  SelectionType::eIMESelectedClause,
+  SelectionType::eAccessibility,
+  SelectionType::eFind,
+  SelectionType::eURLSecondary,
+  SelectionType::eURLStrikeout,
 };
 
 // Please include mozilla/dom/Selection.h for the following APIs.
 const char* ToChar(SelectionType aSelectionType);
+inline bool IsValidRawSelectionType(RawSelectionType aRawSelectionType);
 inline SelectionType ToSelectionType(RawSelectionType aRawSelectionType);
 inline RawSelectionType ToRawSelectionType(SelectionType aSelectionType);
-inline bool operator &(SelectionType aSelectionType,
-                       RawSelectionType aRawSelectionTypes);
+inline SelectionTypeMask ToSelectionTypeMask(SelectionType aSelectionType);
 
 } // namespace mozilla
 %}
diff --git a/editor/libeditor/EditorBase.cpp b/editor/libeditor/EditorBase.cpp
--- a/editor/libeditor/EditorBase.cpp
+++ b/editor/libeditor/EditorBase.cpp
@@ -2886,18 +2886,17 @@ struct SavedRange final
 
 nsresult
 EditorBase::SplitNodeImpl(nsIContent& aExistingRightNode,
                           int32_t aOffset,
                           nsIContent& aNewLeftNode)
 {
   // Remember all selection points.
   AutoTArray<SavedRange, 10> savedRanges;
-  for (size_t i = 0; i < kPresentSelectionTypeCount; ++i) {
-    SelectionType selectionType(ToSelectionType(1 << i));
+  for (SelectionType selectionType : kPresentSelectionTypes) {
     SavedRange range;
     range.mSelection = GetSelection(selectionType);
     if (selectionType == SelectionType::eNormal) {
       NS_ENSURE_TRUE(range.mSelection, NS_ERROR_NULL_POINTER);
     } else if (!range.mSelection) {
       // For non-normal selections, skip over the non-existing ones.
       continue;
     }
@@ -3035,18 +3034,17 @@ EditorBase::JoinNodesImpl(nsINode* aNode
 
   int32_t joinOffset;
   GetNodeLocation(aNodeToJoin, &joinOffset);
   int32_t keepOffset;
   nsINode* parent = GetNodeLocation(aNodeToKeep, &keepOffset);
 
   // Remember all selection points.
   AutoTArray<SavedRange, 10> savedRanges;
-  for (size_t i = 0; i < kPresentSelectionTypeCount; ++i) {
-    SelectionType selectionType(ToSelectionType(1 << i));
+  for (SelectionType selectionType : kPresentSelectionTypes) {
     SavedRange range;
     range.mSelection = GetSelection(selectionType);
     if (selectionType == SelectionType::eNormal) {
       NS_ENSURE_TRUE(range.mSelection, NS_ERROR_NULL_POINTER);
     } else if (!range.mSelection) {
       // For non-normal selections, skip over the non-existing ones.
       continue;
     }
diff --git a/layout/generic/nsFrameSelection.cpp b/layout/generic/nsFrameSelection.cpp
--- a/layout/generic/nsFrameSelection.cpp
+++ b/layout/generic/nsFrameSelection.cpp
@@ -169,38 +169,16 @@ GetIndexFromSelectionType(SelectionType 
     case SelectionType::eURLStrikeout:
       return 9;
     default:
       return -1;
   }
   /* NOTREACHED */
 }
 
-static SelectionType
-GetSelectionTypeFromIndex(int8_t aIndex)
-{
-  static const SelectionType kSelectionTypes[] = {
-    SelectionType::eNormal,
-    SelectionType::eSpellCheck,
-    SelectionType::eIMERawClause,
-    SelectionType::eIMESelectedRawClause,
-    SelectionType::eIMEConvertedClause,
-    SelectionType::eIMESelectedClause,
-    SelectionType::eAccessibility,
-    SelectionType::eFind,
-    SelectionType::eURLSecondary,
-    SelectionType::eURLStrikeout
-  };
-  if (NS_WARN_IF(aIndex < 0) ||
-      NS_WARN_IF(static_cast<size_t>(aIndex) >= ArrayLength(kSelectionTypes))) {
-    return SelectionType::eNormal;
-  }
-  return kSelectionTypes[aIndex];
-}
-
 /*
 The limiter is used specifically for the text areas and textfields
 In that case it is the DIV tag that is anonymously created for the text
 areas/fields.  Text nodes and BR nodes fall beneath it.  In the case of a
 BR node the limiter will be the parent and the offset will point before or
 after the BR node.  In the case of the text node the parent content is
 the text node itself and the offset will be the exact character position.
 The offset is not important to check for validity.  Simply look at the
@@ -321,19 +299,19 @@ struct MOZ_RAII AutoPrepareFocusRange
 };
 
 } // namespace mozilla
 
 ////////////BEGIN nsFrameSelection methods
 
 nsFrameSelection::nsFrameSelection()
 {
-  for (size_t i = 0; i < kPresentSelectionTypeCount; i++){
+  for (size_t i = 0; i < ArrayLength(mDomSelections); i++) {
     mDomSelections[i] = new Selection(this);
-    mDomSelections[i]->SetType(GetSelectionTypeFromIndex(i));
+    mDomSelections[i]->SetType(kPresentSelectionTypes[i]);
   }
   mBatching = 0;
   mChangesDuringBatching = false;
   mNotifyFrames = true;
 
   mMouseDoubleDownState = false;
   mDesiredPosSet = false;
   mAccessibleCaretEnabled = false;
@@ -378,17 +356,17 @@ nsFrameSelection::nsFrameSelection()
 
 nsFrameSelection::~nsFrameSelection()
 {
 }
 
 NS_IMPL_CYCLE_COLLECTION_CLASS(nsFrameSelection)
 
 NS_IMPL_CYCLE_COLLECTION_UNLINK_BEGIN(nsFrameSelection)
-  for (size_t i = 0; i < kPresentSelectionTypeCount; ++i) {
+  for (size_t i = 0; i < ArrayLength(tmp->mDomSelections); ++i) {
     tmp->mDomSelections[i] = nullptr;
   }
 
   NS_IMPL_CYCLE_COLLECTION_UNLINK(mCellParent)
   tmp->mSelectingTableCellMode = 0;
   tmp->mDragSelectingCells = false;
   NS_IMPL_CYCLE_COLLECTION_UNLINK(mStartSelectedCell)
   NS_IMPL_CYCLE_COLLECTION_UNLINK(mEndSelectedCell)
@@ -400,17 +378,17 @@ NS_IMPL_CYCLE_COLLECTION_UNLINK_BEGIN(ns
 NS_IMPL_CYCLE_COLLECTION_UNLINK_END
 NS_IMPL_CYCLE_COLLECTION_TRAVERSE_BEGIN(nsFrameSelection)
   if (tmp->mShell && tmp->mShell->GetDocument() &&
       nsCCUncollectableMarker::InGeneration(cb,
                                             tmp->mShell->GetDocument()->
                                               GetMarkedCCGeneration())) {
     return NS_SUCCESS_INTERRUPTED_TRAVERSE;
   }
-  for (size_t i = 0; i < kPresentSelectionTypeCount; ++i) {
+  for (size_t i = 0; i < ArrayLength(tmp->mDomSelections); ++i) {
     NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mDomSelections[i])
   }
 
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mCellParent)
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mStartSelectedCell)
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mEndSelectedCell)
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mAppendStartSelectedCell)
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mUnselectCellOnMouseUp)
@@ -1535,21 +1513,21 @@ nsFrameSelection::LookUpSelection(nsICon
                                   int32_t aContentLength,
                                   bool aSlowCheck) const
 {
   if (!aContent || !mShell)
     return nullptr;
 
   UniquePtr<SelectionDetails> details;
 
-  for (size_t j = 0; j < kPresentSelectionTypeCount; j++) {
+  for (size_t j = 0; j < ArrayLength(mDomSelections); j++) {
     if (mDomSelections[j]) {
       details = mDomSelections[j]->LookUpSelection(aContent, aContentOffset,
                                                    aContentLength, Move(details),
-                                                   ToSelectionType(1 << j),
+                                                   kPresentSelectionTypes[j],
                                                    aSlowCheck);
     }
   }
 
   return details;
 }
 
 void
@@ -2962,17 +2940,17 @@ nsFrameSelection::DisconnectFromPresShel
     RefPtr<AccessibleCaretEventHub> eventHub = mShell->GetAccessibleCaretEventHub();
     if (eventHub) {
       int8_t index = GetIndexFromSelectionType(SelectionType::eNormal);
       mDomSelections[index]->RemoveSelectionListener(eventHub);
     }
   }
 
   StopAutoScrollTimer();
-  for (size_t i = 0; i < kPresentSelectionTypeCount; i++) {
+  for (size_t i = 0; i < ArrayLength(mDomSelections); i++) {
     mDomSelections[i]->Clear(nullptr);
   }
   mShell = nullptr;
 }
 
 /**
  * See Bug 1288453.
  *
diff --git a/layout/generic/nsFrameSelection.h b/layout/generic/nsFrameSelection.h
--- a/layout/generic/nsFrameSelection.h
+++ b/layout/generic/nsFrameSelection.h
@@ -698,17 +698,18 @@ private:
   // so remember to use nsCOMPtr when needed.
   nsresult     NotifySelectionListeners(mozilla::SelectionType aSelectionType);
   // Update the selection cache on repaint when the
   // selection being repainted is not empty.
   nsresult     UpdateSelectionCacheOnRepaintSelection(mozilla::dom::
                                                       Selection* aSel);
 
   RefPtr<mozilla::dom::Selection>
-    mDomSelections[mozilla::kPresentSelectionTypeCount];
+    mDomSelections[
+      sizeof(mozilla::kPresentSelectionTypes) / sizeof(mozilla::SelectionType)];
 
   // Table selection support.
   nsITableCellLayout* GetCellLayout(nsIContent *aCellContent) const;
 
   nsresult SelectBlockOfCells(nsIContent *aStartNode, nsIContent *aEndNode);
   nsresult SelectRowOrColumn(nsIContent *aCellContent, uint32_t aTarget);
   nsresult UnselectCells(nsIContent *aTable,
                          int32_t aStartRowIndex, int32_t aStartColumnIndex,
diff --git a/layout/generic/nsTextFrame.cpp b/layout/generic/nsTextFrame.cpp
--- a/layout/generic/nsTextFrame.cpp
+++ b/layout/generic/nsTextFrame.cpp
@@ -5980,25 +5980,24 @@ nsTextFrame::ComputeDescentLimitForSelec
                                       GetFontSizeInflation());
   gfxFloat lineHeight = gfxFloat(lineHeightApp) / app;
   if (lineHeight <= aFontMetrics.maxHeight) {
     return aFontMetrics.maxDescent;
   }
   return aFontMetrics.maxDescent + (lineHeight - aFontMetrics.maxHeight) / 2;
 }
 
-
 // Make sure this stays in sync with DrawSelectionDecorations below
-static const RawSelectionType kRawSelectionTypesWithDecorations =
-  nsISelectionController::SELECTION_SPELLCHECK |
-  nsISelectionController::SELECTION_URLSTRIKEOUT |
-  nsISelectionController::SELECTION_IME_RAWINPUT |
-  nsISelectionController::SELECTION_IME_SELECTEDRAWTEXT |
-  nsISelectionController::SELECTION_IME_CONVERTEDTEXT |
-  nsISelectionController::SELECTION_IME_SELECTEDCONVERTEDTEXT;
+static const SelectionTypeMask kSelectionTypesWithDecorations =
+  ToSelectionTypeMask(SelectionType::eSpellCheck) |
+  ToSelectionTypeMask(SelectionType::eURLStrikeout) |
+  ToSelectionTypeMask(SelectionType::eIMERawClause) |
+  ToSelectionTypeMask(SelectionType::eIMESelectedRawClause) |
+  ToSelectionTypeMask(SelectionType::eIMEConvertedClause) |
+  ToSelectionTypeMask(SelectionType::eIMESelectedClause);
 
 /* static */
 gfxFloat
 nsTextFrame::ComputeSelectionUnderlineHeight(
                nsPresContext* aPresContext,
                const gfxFont::Metrics& aFontMetrics,
                SelectionType aSelectionType)
 {
@@ -6067,17 +6066,17 @@ nsTextFrame::PaintDecorationLine(const P
     }
   } else {
     nsCSSRendering::PaintDecorationLine(
       this, *aParams.context->GetDrawTarget(), params);
   }
 }
 
 /**
- * This, plus kRawSelectionTypesWithDecorations, encapsulates all knowledge
+ * This, plus kSelectionTypesWithDecorations, encapsulates all knowledge
  * about drawing text decoration for selections.
  */
 void
 nsTextFrame::DrawSelectionDecorations(gfxContext* aContext,
                                       const LayoutDeviceRect& aDirtyRect,
                                       SelectionType aSelectionType,
                                       nsTextPaintStyle& aTextPaintStyle,
                                       const TextRangeStyle &aRangeStyle,
@@ -6515,45 +6514,45 @@ nsTextFrame::PaintOneShadow(const PaintS
 }
 
 // Paints selection backgrounds and text in the correct colors. Also computes
 // aAllTypes, the union of all selection types that are applying to this text.
 bool
 nsTextFrame::PaintTextWithSelectionColors(
     const PaintTextSelectionParams& aParams,
     const UniquePtr<SelectionDetails>& aDetails,
-    RawSelectionType* aAllRawSelectionTypes,
+    SelectionTypeMask* aAllSelectionTypeMask,
     const nsCharClipDisplayItem::ClipEdges& aClipEdges)
 {
   const gfxTextRun::Range& contentRange = aParams.contentRange;
 
   // Figure out which selections control the colors to use for each character.
   // Note: prevailingSelectionsBuffer is keeping extra raw pointers to
   // uniquely-owned resources, but it's safe because it's temporary and the
   // resources are owned by the caller. Therefore, they'll outlive this object.
   AutoTArray<SelectionDetails*,BIG_TEXT_NODE_SIZE> prevailingSelectionsBuffer;
   SelectionDetails** prevailingSelections =
     prevailingSelectionsBuffer.AppendElements(contentRange.Length(), fallible);
   if (!prevailingSelections) {
     return false;
   }
 
-  RawSelectionType allRawSelectionTypes = 0;
+  SelectionTypeMask allSelectionTypeMask = 0;
   for (uint32_t i = 0; i < contentRange.Length(); ++i) {
     prevailingSelections[i] = nullptr;
   }
 
   bool anyBackgrounds = false;
   for (SelectionDetails* sdptr = aDetails.get(); sdptr; sdptr = sdptr->mNext.get()) {
     int32_t start = std::max(0, sdptr->mStart - int32_t(contentRange.start));
     int32_t end = std::min(int32_t(contentRange.Length()),
                            sdptr->mEnd - int32_t(contentRange.start));
     SelectionType selectionType = sdptr->mSelectionType;
     if (start < end) {
-      allRawSelectionTypes |= ToRawSelectionType(selectionType);
+      allSelectionTypeMask |= ToSelectionTypeMask(selectionType);
       // Ignore selections that don't set colors
       nscolor foreground, background;
       if (GetSelectionTextColors(selectionType, *aParams.textPaintStyle,
                                  sdptr->mTextRangeStyle,
                                  &foreground, &background)) {
         if (NS_GET_A(background) > 0) {
           anyBackgrounds = true;
         }
@@ -6562,19 +6561,19 @@ nsTextFrame::PaintTextWithSelectionColor
           if (!prevailingSelections[i] ||
               selectionType < prevailingSelections[i]->mSelectionType) {
             prevailingSelections[i] = sdptr;
           }
         }
       }
     }
   }
-  *aAllRawSelectionTypes = allRawSelectionTypes;
-
-  if (!allRawSelectionTypes) {
+  *aAllSelectionTypeMask = allSelectionTypeMask;
+
+  if (!allSelectionTypeMask) {
     // Nothing is selected in the given text range. XXX can this still occur?
     return false;
   }
 
   bool vertical = mTextRun->IsVertical();
   const gfxFloat startIOffset = vertical ?
     aParams.textBaselinePt.y - aParams.framePt.y :
     aParams.textBaselinePt.x - aParams.framePt.x;
@@ -6793,29 +6792,32 @@ nsTextFrame::PaintTextWithSelection(
 {
   NS_ASSERTION(GetContent()->IsSelectionDescendant(), "wrong paint path");
 
   UniquePtr<SelectionDetails> details = GetSelectionDetails();
   if (!details) {
     return false;
   }
 
-  RawSelectionType allRawSelectionTypes;
-  if (!PaintTextWithSelectionColors(aParams, details, &allRawSelectionTypes,
+  SelectionTypeMask allSelectionTypeMask;
+  if (!PaintTextWithSelectionColors(aParams, details, &allSelectionTypeMask,
                                     aClipEdges)) {
     return false;
   }
   // Iterate through just the selection rawSelectionTypes that paint decorations
   // and paint decorations for any that actually occur in this frame. Paint
   // higher-numbered selection rawSelectionTypes below lower-numered ones on the
   // general principal that lower-numbered selections are higher priority.
-  allRawSelectionTypes &= kRawSelectionTypesWithDecorations;
-  for (size_t i = kSelectionTypeCount - 1; i >= 1; --i) {
-    SelectionType selectionType = ToSelectionType(1 << (i - 1));
-    if (selectionType & allRawSelectionTypes) {
+  allSelectionTypeMask &= kSelectionTypesWithDecorations;
+  MOZ_ASSERT(kPresentSelectionTypes[0] == SelectionType::eNormal,
+             "The following for loop assumes that the first item of "
+             "kPresentSelectionTypes is SelectionType::eNormal");
+  for (size_t i = ArrayLength(kPresentSelectionTypes) - 1; i >= 1; --i) {
+    SelectionType selectionType = kPresentSelectionTypes[i];
+    if (ToSelectionTypeMask(selectionType) & allSelectionTypeMask) {
       // There is some selection of this selectionType. Try to paint its
       // decorations (there might not be any for this type but that's OK,
       // PaintTextSelectionDecorations will exit early).
       PaintTextSelectionDecorations(aParams, details, selectionType);
     }
   }
 
   return true;
@@ -7696,17 +7698,19 @@ nsTextFrame::CombineSelectionUnderlineRe
   params.offset = fontGroup->GetUnderlineOffset();
   params.descentLimit =
     ComputeDescentLimitForSelectionUnderline(aPresContext, metrics);
   params.vertical = verticalRun;
 
   UniquePtr<SelectionDetails> details = GetSelectionDetails();
   for (SelectionDetails* sd = details.get(); sd; sd = sd->mNext.get()) {
     if (sd->mStart == sd->mEnd ||
-        !(sd->mSelectionType & kRawSelectionTypesWithDecorations) ||
+        sd->mSelectionType == SelectionType::eInvalid ||
+        !(ToSelectionTypeMask(sd->mSelectionType) &
+            kSelectionTypesWithDecorations) ||
         // URL strikeout does not use underline.
         sd->mSelectionType == SelectionType::eURLStrikeout) {
       continue;
     }
 
     float relativeSize;
     int32_t index =
       nsTextPaintStyle::GetUnderlineStyleIndexForSelectionType(
@@ -7774,17 +7778,17 @@ nsTextFrame::SetSelectedRange(uint32_t a
     f = f->GetNextContinuation();
   }
 
   nsPresContext* presContext = PresContext();
   while (f && f->GetContentOffset() < int32_t(aEnd)) {
     // We may need to reflow to recompute the overflow area for
     // spellchecking or IME underline if their underline is thicker than
     // the normal decoration line.
-    if (aSelectionType & kRawSelectionTypesWithDecorations) {
+    if (ToSelectionTypeMask(aSelectionType) & kSelectionTypesWithDecorations) {
       bool didHaveOverflowingSelection =
         (f->GetStateBits() & TEXT_SELECTION_UNDERLINE_OVERFLOWED) != 0;
       nsRect r(nsPoint(0, 0), GetSize());
       bool willHaveOverflowingSelection =
         aSelected && f->CombineSelectionUnderlineRect(presContext, r);
       if (didHaveOverflowingSelection || willHaveOverflowingSelection) {
         presContext->PresShell()->FrameNeedsReflow(f,
                                                    nsIPresShell::eStyleChange,
diff --git a/layout/generic/nsTextFrame.h b/layout/generic/nsTextFrame.h
--- a/layout/generic/nsTextFrame.h
+++ b/layout/generic/nsTextFrame.h
@@ -37,17 +37,17 @@ class nsDisplayText;
 
 namespace mozilla {
 class SVGContextPaint;
 };
 
 class nsTextFrame : public nsFrame
 {
   typedef mozilla::LayoutDeviceRect LayoutDeviceRect;
-  typedef mozilla::RawSelectionType RawSelectionType;
+  typedef mozilla::SelectionTypeMask SelectionTypeMask;
   typedef mozilla::SelectionType SelectionType;
   typedef mozilla::TextRangeStyle TextRangeStyle;
   typedef mozilla::gfx::DrawTarget DrawTarget;
   typedef mozilla::gfx::Point Point;
   typedef mozilla::gfx::Rect Rect;
   typedef mozilla::gfx::Size Size;
   typedef mozilla::layout::TextDrawTarget TextDrawTarget;
   typedef gfxTextRun::Range Range;
@@ -515,23 +515,23 @@ public:
   // helper: paint text frame when we're impacted by at least one selection.
   // Return false if the text was not painted and we should continue with
   // the fast path.
   bool PaintTextWithSelection(
     const PaintTextSelectionParams& aParams,
     const nsCharClipDisplayItem::ClipEdges& aClipEdges);
   // helper: paint text with foreground and background colors determined
   // by selection(s). Also computes a mask of all selection types applying to
-  // our text, returned in aAllTypes.
+  // our text, returned in aAllSelectionTypeMask.
   // Return false if the text was not painted and we should continue with
   // the fast path.
   bool PaintTextWithSelectionColors(
     const PaintTextSelectionParams& aParams,
     const mozilla::UniquePtr<SelectionDetails>& aDetails,
-    RawSelectionType* aAllRawSelectionTypes,
+    SelectionTypeMask* aAllSelectionTypeMask,
     const nsCharClipDisplayItem::ClipEdges& aClipEdges);
   // helper: paint text decorations for text selected by aSelectionType
   void PaintTextSelectionDecorations(const PaintTextSelectionParams& aParams,
                                      const mozilla::UniquePtr<SelectionDetails>& aDetails,
                                      SelectionType aSelectionType);
 
   void DrawEmphasisMarks(gfxContext* aContext,
                          mozilla::WritingMode aWM,
