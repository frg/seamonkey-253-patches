# HG changeset patch
# User David Anderson <danderson@mozilla.com>
# Date 1506098892 25200
# Node ID 4cda16f44d0f59a079ae1807aaf2a7c177c6d141
# Parent  ddfa4807c4147fc9b035d96a86f59620b4a182a0
Fix shutdown leak in TextureSourceProviderMLGPU. (bug 1398304, r=bas)

diff --git a/gfx/layers/TextureSourceProvider.h b/gfx/layers/TextureSourceProvider.h
--- a/gfx/layers/TextureSourceProvider.h
+++ b/gfx/layers/TextureSourceProvider.h
@@ -57,17 +57,17 @@ public:
   virtual bool SupportsEffect(EffectTypes aEffect) { return true; }
 
   /// Most compositor backends operate asynchronously under the hood. This
   /// means that when a layer stops using a texture it is often desirable to
   /// wait for the end of the next composition before releasing the texture's
   /// ReadLock.
   /// This function provides a convenient way to do this delayed unlocking, if
   /// the texture itself requires it.
-  void UnlockAfterComposition(TextureHost* aTexture);
+  virtual void UnlockAfterComposition(TextureHost* aTexture);
 
   /// Most compositor backends operate asynchronously under the hood. This
   /// means that when a layer stops using a texture it is often desirable to
   /// wait for the end of the next composition before NotifyNotUsed() call.
   /// This function provides a convenient way to do this delayed NotifyNotUsed()
   /// call, if the texture itself requires it.
   /// See bug 1260611 and bug 1252835
   ///
diff --git a/gfx/layers/mlgpu/TextureSourceProviderMLGPU.cpp b/gfx/layers/mlgpu/TextureSourceProviderMLGPU.cpp
--- a/gfx/layers/mlgpu/TextureSourceProviderMLGPU.cpp
+++ b/gfx/layers/mlgpu/TextureSourceProviderMLGPU.cpp
@@ -88,19 +88,31 @@ TextureSourceProviderMLGPU::CreateDataTe
 
 already_AddRefed<DataTextureSource>
 TextureSourceProviderMLGPU::CreateDataTextureSourceAround(gfx::DataSourceSurface* aSurface)
 {
   MOZ_ASSERT_UNREACHABLE("NYI");
   return nullptr;
 }
 
+void
+TextureSourceProviderMLGPU::UnlockAfterComposition(TextureHost* aTexture)
+{
+  TextureSourceProvider::UnlockAfterComposition(aTexture);
+
+  // If this is being called after we shutdown the compositor, we must finish
+  // read unlocking now to prevent a cycle.
+  if (!IsValid()) {
+    ReadUnlockTextures();
+  }
+}
+
 bool
 TextureSourceProviderMLGPU::NotifyNotUsedAfterComposition(TextureHost* aTextureHost)
 {
-  if (!mDevice) {
+  if (!IsValid()) {
     return false;
   }
   return TextureSourceProvider::NotifyNotUsedAfterComposition(aTextureHost);
 }
 
 } // namespace layers
 } // namespace mozilla
diff --git a/gfx/layers/mlgpu/TextureSourceProviderMLGPU.h b/gfx/layers/mlgpu/TextureSourceProviderMLGPU.h
--- a/gfx/layers/mlgpu/TextureSourceProviderMLGPU.h
+++ b/gfx/layers/mlgpu/TextureSourceProviderMLGPU.h
@@ -22,16 +22,17 @@ public:
   ~TextureSourceProviderMLGPU() override;
 
   already_AddRefed<DataTextureSource>
   CreateDataTextureSource(TextureFlags aFlags) override;
 
   already_AddRefed<DataTextureSource>
   CreateDataTextureSourceAround(gfx::DataSourceSurface* aSurface) override;
 
+  void UnlockAfterComposition(TextureHost* aTexture) override;
   bool NotifyNotUsedAfterComposition(TextureHost* aTextureHost) override;
 
   int32_t GetMaxTextureSize() const override;
   TimeStamp GetLastCompositionEndTime() const override;
   bool SupportsEffect(EffectTypes aEffect) override;
   bool IsValid() const override;
 
 #ifdef XP_WIN
