# HG changeset patch
# User Michael Layzell <michael@thelayzells.com>
# Date 1502299900 14400
#      Wed Aug 09 13:31:40 2017 -0400
# Node ID a0376b2650a813b13b6440a6874b5887bd482719
# Parent  5ae01b3d6dcc0b13eb73cdcb34a0dbe34a31f5f1
Bug 1388818 - Remove old profiler_suspend_and_sample_thread, r=njn

MozReview-Commit-ID: 4kjGbNi3c7g

diff --git a/tools/profiler/core/platform.cpp b/tools/profiler/core/platform.cpp
--- a/tools/profiler/core/platform.cpp
+++ b/tools/profiler/core/platform.cpp
@@ -3326,67 +3326,16 @@ profiler_clear_js_context()
   }
 
   // We don't call info->StopJSSampling() here; there's no point doing that for
   // a JS thread that is in the process of disappearing.
 
   info->mContext = nullptr;
 }
 
-// NOTE: The callback function passed in will be called while the target thread
-// is paused. Doing stuff in this function like allocating which may try to
-// claim locks is a surefire way to deadlock.
-void
-profiler_suspend_and_sample_thread(
-  int aThreadId,
-  const std::function<ProfilerStackCallback>& aCallback,
-  bool aSampleNative /* = true */)
-{
-  // Allocate the space for the native stack
-  NativeStack nativeStack;
-
-  // Lock the profiler mutex
-  PSAutoLock lock(gPSMutex);
-
-  const CorePS::ThreadVector& liveThreads = CorePS::LiveThreads(lock);
-  for (uint32_t i = 0; i < liveThreads.size(); i++) {
-    ThreadInfo* info = liveThreads.at(i);
-
-    if (info->ThreadId() == aThreadId) {
-      // Suspend, sample, and then resume the target thread.
-      Sampler sampler(lock);
-      sampler.SuspendAndSampleAndResumeThread(lock, *info,
-                                              [&](const Registers& aRegs) {
-        // The target thread is now suspended. Collect a native backtrace, and
-        // call the callback.
-#if defined(HAVE_FASTINIT_NATIVE_UNWIND)
-        if (aSampleNative) {
-          // We can only use FramePointerStackWalk or MozStackWalk from
-          // suspend_and_sample_thread as other stackwalking methods may not be
-          // initialized.
-# if defined(USE_FRAME_POINTER_STACK_WALK)
-          DoFramePointerBacktrace(lock, *info, aRegs, nativeStack);
-# elif defined(USE_MOZ_STACK_WALK)
-          DoMozStackWalkBacktrace(lock, *info, aRegs, nativeStack);
-# else
-#  error "Invalid configuration"
-# endif
-        }
-#endif
-        aCallback(nativeStack.mPCs, nativeStack.mCount, info->IsMainThread());
-      });
-
-      // NOTE: Make sure to disable the sampler before it is destroyed, in case
-      // the profiler is running at the same time.
-      sampler.Disable(lock);
-      break;
-    }
-  }
-}
-
 // NOTE: aCollector's methods will be called while the target thread is paused.
 // Doing things in those methods like allocating -- which may try to claim
 // locks -- is a surefire way to deadlock.
 void
 profiler_suspend_and_sample_thread(int aThreadId,
                                    uint32_t aFeatures,
                                    ProfilerStackCollector& aCollector,
                                    bool aSampleNative /* = true */)
diff --git a/tools/profiler/public/GeckoProfiler.h b/tools/profiler/public/GeckoProfiler.h
--- a/tools/profiler/public/GeckoProfiler.h
+++ b/tools/profiler/public/GeckoProfiler.h
@@ -263,37 +263,16 @@ PROFILER_FUNC_VOID(
 PROFILER_FUNC(double profiler_time(), 0)
 
 // Get the current process's ID.
 PROFILER_FUNC(int profiler_current_process_id(), 0)
 
 // Get the current thread's ID.
 PROFILER_FUNC(int profiler_current_thread_id(), 0)
 
-// This is the function type of the callback passed to profiler_suspend_and_sample_thread.
-//
-// The callback is passed the following arguments:
-//   void** aPCs         The program counters for the target thread's stack.
-//   size_t aCount       The number of program counters in the aPCs array.
-//   bool aIsMainThread  Whether the target thread was the main thread.
-typedef void ProfilerStackCallback(void** aPCs, size_t aCount, bool aIsMainThread);
-
-// This method suspends the thread identified by aThreadId, optionally samples
-// it for its native stack, and then calls the callback.
-//
-// WARNING: The target thread is suspended during the callback. Do not try to
-// allocate or acquire any locks, or you could deadlock. The target thread will
-// have resumed by the time this function returns.
-//
-// XXX: this function is in the process of being replaced with the other profiler_suspend_and_sample_thread() function.
-PROFILER_FUNC_VOID(
-  profiler_suspend_and_sample_thread(int aThreadId,
-                                     const std::function<ProfilerStackCallback>& aCallback,
-                                     bool aSampleNative = true))
-
 // An object of this class is passed to profiler_suspend_and_sample_thread().
 // For each stack frame, one of the Collect methods will be called.
 class ProfilerStackCollector
 {
 public:
   // Some collectors need to worry about possibly overwriting previous
   // generations of data. If that's not an issue, this can return Nothing,
   // which is the default behaviour.
