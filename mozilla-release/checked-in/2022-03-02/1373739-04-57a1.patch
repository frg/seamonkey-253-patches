# HG changeset patch
# User Michael Smith <michael@spinda.net>
# Date 1498060299 25200
# Node ID 6ba8cca5631fafd4cbe0d13813b832ec1d4ebd80
# Parent  349c74ed0bd1e3d57a6182d7dbce48cc2fa294a3
Bug 1373739 - Simulate window activation events in headless mode. r=jrmuizel

This is necessary for focus events to propagate correctly. Otherwise,
/2dcontext/drawing-paths-to-the-canvas/drawFocusIfNeeded_004.html in the
Web Platform Tests fails, for example.

MozReview-Commit-ID: 69GItIaQWfr

diff --git a/widget/headless/HeadlessWidget.cpp b/widget/headless/HeadlessWidget.cpp
--- a/widget/headless/HeadlessWidget.cpp
+++ b/widget/headless/HeadlessWidget.cpp
@@ -29,76 +29,120 @@ CreateDefaultTarget(IntSize aSize)
   IntSize size = (aSize.width <= 0 || aSize.height <= 0) ? gfx::IntSize(1, 1) : aSize;
   RefPtr<DrawTarget> target = Factory::CreateDrawTarget(gfxVars::ContentBackend(), size, SurfaceFormat::B8G8R8A8);
   RefPtr<gfxContext> ctx = gfxContext::CreatePreservingTransformOrNull(target);
   return ctx.forget();
 }
 
 NS_IMPL_ISUPPORTS_INHERITED0(HeadlessWidget, nsBaseWidget)
 
+HeadlessWidget* HeadlessWidget::sActiveWindow = nullptr;
+
+HeadlessWidget::~HeadlessWidget()
+{
+  if (sActiveWindow == this)
+    sActiveWindow = nullptr;
+}
+
 nsresult
 HeadlessWidget::Create(nsIWidget* aParent,
                        nsNativeWidget aNativeParent,
                        const LayoutDeviceIntRect& aRect,
                        nsWidgetInitData* aInitData)
 {
   MOZ_ASSERT(!aNativeParent, "No native parents for headless widgets.");
 
   BaseCreate(nullptr, aInitData);
+
   mBounds = aRect;
   mRestoreBounds = aRect;
   mVisible = true;
   mEnabled = true;
+
+  if (aParent) {
+    mTopLevel = aParent->GetTopLevelWidget();
+  } else {
+    mTopLevel = this;
+  }
+
   return NS_OK;
 }
 
 already_AddRefed<nsIWidget>
 HeadlessWidget::CreateChild(const LayoutDeviceIntRect& aRect,
                             nsWidgetInitData* aInitData,
                             bool aForceUseIWidgetParent)
 {
   nsCOMPtr<nsIWidget> widget = nsIWidget::CreateHeadlessWidget();
   if (!widget) {
     return nullptr;
   }
-  if (NS_FAILED(widget->Create(nullptr, nullptr, aRect, aInitData))) {
+  if (NS_FAILED(widget->Create(this, nullptr, aRect, aInitData))) {
     return nullptr;
   }
   return widget.forget();
 }
 
+nsIWidget*
+HeadlessWidget::GetTopLevelWidget()
+{
+  return mTopLevel;
+}
+
 void
-HeadlessWidget::SendSetZLevelEvent()
+HeadlessWidget::RaiseWindow()
 {
+  MOZ_ASSERT(mTopLevel == this, "Raising a non-toplevel window.");
+
+  if (sActiveWindow == this)
+    return;
+
+  // Raise the window to the top of the stack.
   nsWindowZ placement = nsWindowZTop;
   nsCOMPtr<nsIWidget> actualBelow;
   if (mWidgetListener)
     mWidgetListener->ZLevelChanged(true, &placement, nullptr, getter_AddRefs(actualBelow));
+
+  // Deactivate the last active window.
+  if (sActiveWindow && sActiveWindow->mWidgetListener)
+    sActiveWindow->mWidgetListener->WindowDeactivated();
+
+  // Activate this window.
+  sActiveWindow = this;
+  if (mWidgetListener)
+    mWidgetListener->WindowActivated();
 }
 
 void
 HeadlessWidget::Show(bool aState)
 {
   mVisible = aState;
-  if (aState) {
-    SendSetZLevelEvent();
-  }
+
+  // Top-level windows are activated/raised when shown.
+  if (aState && mTopLevel == this)
+    RaiseWindow();
 }
 
 bool
 HeadlessWidget::IsVisible() const
 {
   return mVisible;
 }
 
 nsresult
 HeadlessWidget::SetFocus(bool aRaise)
 {
-  if (aRaise && mVisible) {
-    SendSetZLevelEvent();
+  // aRaise == true means we request activation of our toplevel window.
+  if (aRaise) {
+    HeadlessWidget* topLevel = (HeadlessWidget*) GetTopLevelWidget();
+
+    // The toplevel only becomes active if it's currently visible; otherwise, it
+    // will be activated anyway when it's shown.
+    if (topLevel->IsVisible())
+      topLevel->RaiseWindow();
   }
   return NS_OK;
 }
 
 void
 HeadlessWidget::Enable(bool aState)
 {
   mEnabled = aState;
diff --git a/widget/headless/HeadlessWidget.h b/widget/headless/HeadlessWidget.h
--- a/widget/headless/HeadlessWidget.h
+++ b/widget/headless/HeadlessWidget.h
@@ -30,16 +30,18 @@ public:
                           nsNativeWidget aNativeParent,
                           const LayoutDeviceIntRect& aRect,
                           nsWidgetInitData* aInitData = nullptr) override;
   using nsBaseWidget::Create; // for Create signature not overridden here
   virtual already_AddRefed<nsIWidget> CreateChild(const LayoutDeviceIntRect& aRect,
                                                   nsWidgetInitData* aInitData = nullptr,
                                                   bool aForceUseIWidgetParent = false) override;
 
+  virtual nsIWidget* GetTopLevelWidget() override;
+
   virtual void Show(bool aState) override;
   virtual bool IsVisible() const override;
   virtual void Move(double aX, double aY) override;
   virtual void Resize(double aWidth,
                       double aHeight,
                       bool   aRepaint) override;
   virtual void Resize(double aX,
                       double aY,
@@ -80,24 +82,28 @@ public:
   GetLayerManager(PLayerTransactionChild* aShadowManager = nullptr,
                   LayersBackend aBackendHint = mozilla::layers::LayersBackend::LAYERS_NONE,
                   LayerManagerPersistence aPersistence = LAYER_MANAGER_CURRENT) override;
 
   virtual nsresult DispatchEvent(WidgetGUIEvent* aEvent,
                                  nsEventStatus& aStatus) override;
 
 private:
-  ~HeadlessWidget() {}
+  ~HeadlessWidget();
   bool mEnabled;
   bool mVisible;
+  nsIWidget* mTopLevel;
   // The size mode before entering fullscreen mode.
   nsSizeMode mLastSizeMode;
   InputContext mInputContext;
   // In headless there is no window manager to track window bounds
   // across size mode changes, so we must track it to emulate.
   LayoutDeviceIntRect mRestoreBounds;
-  void SendSetZLevelEvent();
+  // Similarly, we must track the active window ourselves in order
+  // to dispatch (de)activation events properly.
+  void RaiseWindow();
+  static HeadlessWidget* sActiveWindow;
 };
 
 } // namespace widget
 } // namespace mozilla
 
 #endif

