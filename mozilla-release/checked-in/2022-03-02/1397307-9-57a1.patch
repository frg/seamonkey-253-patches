# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1505233780 -7200
# Node ID 7558fb4b16997621dab4b82e3dd0a5d81e0c4488
# Parent  197681846178cbcbcb87bcf51b90971bcfbf9241
Bug 1397307 - P9. Pass video frame rate to RemoteVideoDecoder and GPU process. r=mattwoodrow

MozReview-Commit-ID: BDSO332f3B6

diff --git a/dom/media/ipc/PVideoDecoderManager.ipdl b/dom/media/ipc/PVideoDecoderManager.ipdl
--- a/dom/media/ipc/PVideoDecoderManager.ipdl
+++ b/dom/media/ipc/PVideoDecoderManager.ipdl
@@ -12,18 +12,19 @@ using struct mozilla::layers::TextureFac
 
 namespace mozilla {
 namespace dom {
 
 sync protocol PVideoDecoderManager
 {
   manages PVideoDecoder;
 parent:
-  sync PVideoDecoder(VideoInfo info, TextureFactoryIdentifier identifier) returns (bool success,
-                                                                                   nsCString aErrorDescription);
+  sync PVideoDecoder(VideoInfo info, float framerate, TextureFactoryIdentifier identifier)
+         returns (bool success,
+         nsCString aErrorDescription);
 
   sync Readback(SurfaceDescriptorGPUVideo sd) returns (SurfaceDescriptor aResult);
 
   async DeallocateSurfaceDescriptorGPUVideo(SurfaceDescriptorGPUVideo sd);
 };
 
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/media/ipc/RemoteVideoDecoder.cpp b/dom/media/ipc/RemoteVideoDecoder.cpp
--- a/dom/media/ipc/RemoteVideoDecoder.cpp
+++ b/dom/media/ipc/RemoteVideoDecoder.cpp
@@ -183,16 +183,17 @@ RemoteDecoderModule::CreateVideoDecoder(
   MediaResult result(NS_OK);
   VideoDecoderManagerChild::GetManagerThread()->Dispatch(
     NS_NewRunnableFunction(
       "dom::RemoteDecoderModule::CreateVideoDecoder",
       [&]() {
         AutoCompleteTask complete(&task);
         result = object->mActor->InitIPDL(
           aParams.VideoConfig(),
+          aParams.mRate.mValue,
           aParams.mKnowsCompositor->GetTextureFactoryIdentifier());
       }),
     NS_DISPATCH_NORMAL);
   task.Wait();
 
   if (NS_FAILED(result)) {
     if (aParams.mError) {
       *aParams.mError = result;
diff --git a/dom/media/ipc/VideoDecoderChild.cpp b/dom/media/ipc/VideoDecoderChild.cpp
--- a/dom/media/ipc/VideoDecoderChild.cpp
+++ b/dom/media/ipc/VideoDecoderChild.cpp
@@ -145,16 +145,17 @@ VideoDecoderChild::ActorDestroy(ActorDes
         }
       }));
   }
   mCanSend = false;
 }
 
 MediaResult
 VideoDecoderChild::InitIPDL(const VideoInfo& aVideoInfo,
+                            float aFramerate,
                             const layers::TextureFactoryIdentifier& aIdentifier)
 {
   RefPtr<VideoDecoderManagerChild> manager =
     VideoDecoderManagerChild::GetSingleton();
 
   // The manager isn't available because VideoDecoderManagerChild has been
   // initialized with null end points and we don't want to decode video on GPU
   // process anymore. Return false here so that we can fallback to other PDMs.
@@ -171,17 +172,20 @@ VideoDecoderChild::InitIPDL(const VideoI
   // available. If not, then the cycle repeats until we're ready.
   if (!manager->CanSend()) {
     return NS_OK;
   }
 
   mIPDLSelfRef = this;
   bool success = false;
   nsCString errorDescription;
-  if (manager->SendPVideoDecoderConstructor(this, aVideoInfo, aIdentifier,
+  if (manager->SendPVideoDecoderConstructor(this,
+                                            aVideoInfo,
+                                            aFramerate,
+                                            aIdentifier,
                                             &success,
                                             &errorDescription)) {
     mCanSend = true;
   }
 
   return success ? MediaResult(NS_OK) :
                    MediaResult(NS_ERROR_DOM_MEDIA_FATAL_ERR, errorDescription);
 }
diff --git a/dom/media/ipc/VideoDecoderChild.h b/dom/media/ipc/VideoDecoderChild.h
--- a/dom/media/ipc/VideoDecoderChild.h
+++ b/dom/media/ipc/VideoDecoderChild.h
@@ -45,16 +45,17 @@ public:
   void Shutdown();
   bool IsHardwareAccelerated(nsACString& aFailureReason) const;
   nsCString GetDescriptionName() const;
   void SetSeekThreshold(const media::TimeUnit& aTime);
   MediaDataDecoder::ConversionRequired NeedsConversion() const;
 
   MOZ_IS_CLASS_INIT
   MediaResult InitIPDL(const VideoInfo& aVideoInfo,
+                       float aFramerate,
                        const layers::TextureFactoryIdentifier& aIdentifier);
   void DestroyIPDL();
 
   // Called from IPDL when our actor has been destroyed
   void IPDLActorDestroyed();
 
   VideoDecoderManagerChild* GetManager();
 
diff --git a/dom/media/ipc/VideoDecoderManagerChild.cpp b/dom/media/ipc/VideoDecoderManagerChild.cpp
--- a/dom/media/ipc/VideoDecoderManagerChild.cpp
+++ b/dom/media/ipc/VideoDecoderManagerChild.cpp
@@ -111,16 +111,17 @@ VideoDecoderManagerChild::GetManagerThre
 /* static */ AbstractThread*
 VideoDecoderManagerChild::GetManagerAbstractThread()
 {
   return sVideoDecoderChildAbstractThread;
 }
 
 PVideoDecoderChild*
 VideoDecoderManagerChild::AllocPVideoDecoderChild(const VideoInfo& aVideoInfo,
+                                                  const float& aFramerate,
                                                   const layers::TextureFactoryIdentifier& aIdentifier,
                                                   bool* aSuccess,
                                                   nsCString* /* not used */)
 {
   return new VideoDecoderChild();
 }
 
 bool
diff --git a/dom/media/ipc/VideoDecoderManagerChild.h b/dom/media/ipc/VideoDecoderManagerChild.h
--- a/dom/media/ipc/VideoDecoderManagerChild.h
+++ b/dom/media/ipc/VideoDecoderManagerChild.h
@@ -66,16 +66,17 @@ protected:
   void InitIPDL();
 
   void ActorDestroy(ActorDestroyReason aWhy) override;
   void DeallocPVideoDecoderManagerChild() override;
 
   void HandleFatalError(const char* aName, const char* aMsg) const override;
 
   PVideoDecoderChild* AllocPVideoDecoderChild(const VideoInfo& aVideoInfo,
+                                              const float& aFramerate,
                                               const layers::TextureFactoryIdentifier& aIdentifier,
                                               bool* aSuccess,
                                               nsCString* aErrorDescription) override;
   bool DeallocPVideoDecoderChild(PVideoDecoderChild* actor) override;
 
 private:
   // Main thread only
   static void InitializeThread();
diff --git a/dom/media/ipc/VideoDecoderManagerParent.cpp b/dom/media/ipc/VideoDecoderManagerParent.cpp
--- a/dom/media/ipc/VideoDecoderManagerParent.cpp
+++ b/dom/media/ipc/VideoDecoderManagerParent.cpp
@@ -44,35 +44,36 @@ VideoDecoderManagerParent::StoreImage(Im
 StaticRefPtr<nsIThread> sVideoDecoderManagerThread;
 StaticRefPtr<TaskQueue> sManagerTaskQueue;
 
 class VideoDecoderManagerThreadHolder
 {
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(VideoDecoderManagerThreadHolder)
 
 public:
-  VideoDecoderManagerThreadHolder() {}
+  VideoDecoderManagerThreadHolder() { }
 
 private:
-  ~VideoDecoderManagerThreadHolder() {
+  ~VideoDecoderManagerThreadHolder()
+  {
     NS_DispatchToMainThread(NS_NewRunnableFunction(
       "dom::VideoDecoderManagerThreadHolder::~VideoDecoderManagerThreadHolder",
       []() -> void {
         sVideoDecoderManagerThread->Shutdown();
         sVideoDecoderManagerThread = nullptr;
       }));
   }
 };
 StaticRefPtr<VideoDecoderManagerThreadHolder> sVideoDecoderManagerThreadHolder;
 
 class ManagerThreadShutdownObserver : public nsIObserver
 {
   virtual ~ManagerThreadShutdownObserver() = default;
 public:
-  ManagerThreadShutdownObserver() {}
+  ManagerThreadShutdownObserver() { }
 
   NS_DECL_ISUPPORTS
 
   NS_IMETHOD Observe(nsISupports* aSubject, const char* aTopic,
                      const char16_t* aData) override
   {
     MOZ_ASSERT(strcmp(aTopic, NS_XPCOM_SHUTDOWN_OBSERVER_ID) == 0);
 
@@ -190,26 +191,27 @@ VideoDecoderManagerParent::~VideoDecoder
 void
 VideoDecoderManagerParent::ActorDestroy(mozilla::ipc::IProtocol::ActorDestroyReason)
 {
   mThreadHolder = nullptr;
 }
 
 PVideoDecoderParent*
 VideoDecoderManagerParent::AllocPVideoDecoderParent(const VideoInfo& aVideoInfo,
+                                                    const float& aFramerate,
                                                     const layers::TextureFactoryIdentifier& aIdentifier,
                                                     bool* aSuccess,
                                                     nsCString* aErrorDescription)
 {
   RefPtr<TaskQueue> decodeTaskQueue = new TaskQueue(
     SharedThreadPool::Get(NS_LITERAL_CSTRING("VideoDecoderParent"), 4),
     "VideoDecoderParent::mDecodeTaskQueue");
 
   return new VideoDecoderParent(
-    this, aVideoInfo, aIdentifier,
+    this, aVideoInfo, aFramerate, aIdentifier,
     sManagerTaskQueue, decodeTaskQueue, aSuccess, aErrorDescription);
 }
 
 bool
 VideoDecoderManagerParent::DeallocPVideoDecoderParent(PVideoDecoderParent* actor)
 {
   VideoDecoderParent* parent = static_cast<VideoDecoderParent*>(actor);
   parent->Destroy();
diff --git a/dom/media/ipc/VideoDecoderManagerParent.h b/dom/media/ipc/VideoDecoderManagerParent.h
--- a/dom/media/ipc/VideoDecoderManagerParent.h
+++ b/dom/media/ipc/VideoDecoderManagerParent.h
@@ -27,16 +27,17 @@ public:
   static void ShutdownThreads();
 
   static void ShutdownVideoBridge();
 
   bool OnManagerThread();
 
 protected:
   PVideoDecoderParent* AllocPVideoDecoderParent(const VideoInfo& aVideoInfo,
+                                                const float& aFramerate,
                                                 const layers::TextureFactoryIdentifier& aIdentifier,
                                                 bool* aSuccess,
                                                 nsCString* aErrorDescription) override;
   bool DeallocPVideoDecoderParent(PVideoDecoderParent* actor) override;
 
   mozilla::ipc::IPCResult RecvReadback(const SurfaceDescriptorGPUVideo& aSD, SurfaceDescriptor* aResult) override;
   mozilla::ipc::IPCResult RecvDeallocateSurfaceDescriptorGPUVideo(const SurfaceDescriptorGPUVideo& aSD) override;
 
diff --git a/dom/media/ipc/VideoDecoderParent.cpp b/dom/media/ipc/VideoDecoderParent.cpp
--- a/dom/media/ipc/VideoDecoderParent.cpp
+++ b/dom/media/ipc/VideoDecoderParent.cpp
@@ -39,16 +39,17 @@ public:
     return VideoBridgeChild::GetSingleton();
   }
 private:
   virtual ~KnowsCompositorVideo() = default;
 };
 
 VideoDecoderParent::VideoDecoderParent(VideoDecoderManagerParent* aParent,
                                        const VideoInfo& aVideoInfo,
+                                       float aFramerate,
                                        const layers::TextureFactoryIdentifier& aIdentifier,
                                        TaskQueue* aManagerTaskQueue,
                                        TaskQueue* aDecodeTaskQueue,
                                        bool* aSuccess,
                                        nsCString* aErrorDescription)
   : mParent(aParent)
   , mManagerTaskQueue(aManagerTaskQueue)
   , mDecodeTaskQueue(aDecodeTaskQueue)
@@ -71,16 +72,17 @@ VideoDecoderParent::VideoDecoderParent(V
   WMFDecoderModule::Init();
   RefPtr<WMFDecoderModule> pdm(new WMFDecoderModule());
   pdm->Startup();
 
   CreateDecoderParams params(aVideoInfo);
   params.mTaskQueue = mDecodeTaskQueue;
   params.mKnowsCompositor = mKnowsCompositor;
   params.mImageContainer = new layers::ImageContainer();
+  params.mRate = CreateDecoderParams::VideoFrameRate(aFramerate);
   MediaResult error(NS_OK);
   params.mError = &error;
 
   mDecoder = pdm->CreateVideoDecoder(params);
 
   if (NS_FAILED(error)) {
     MOZ_ASSERT(aErrorDescription);
     *aErrorDescription = error.Description();
diff --git a/dom/media/ipc/VideoDecoderParent.h b/dom/media/ipc/VideoDecoderParent.h
--- a/dom/media/ipc/VideoDecoderParent.h
+++ b/dom/media/ipc/VideoDecoderParent.h
@@ -23,16 +23,17 @@ class VideoDecoderParent final : public 
 {
 public:
   // We refcount this class since the task queue can have runnables
   // that reference us.
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(VideoDecoderParent)
 
   VideoDecoderParent(VideoDecoderManagerParent* aParent,
                      const VideoInfo& aVideoInfo,
+                     float aFramerate,
                      const layers::TextureFactoryIdentifier& aIdentifier,
                      TaskQueue* aManagerTaskQueue,
                      TaskQueue* aDecodeTaskQueue,
                      bool* aSuccess,
                      nsCString* aErrorDescription);
 
   void Destroy();
 
