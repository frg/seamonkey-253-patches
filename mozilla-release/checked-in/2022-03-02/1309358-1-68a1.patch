# HG changeset patch
# User Kershaw Chang <kershaw@mozilla.com>
# Date 1563267014 0
# Node ID 4ed9a5ede7fb9dc2c4711b05a87b54fd760770fd
# Parent  634166a9abaeceb8ff65ae84c8dd453f7298ac29
Bug 1309358 - P1: Add wildcard to Access-Control-Expose-Headers. r=baku, a=RyanVM

For requests without credentials, add wildcard to Access-Control-Expose-Headers.

Differential Revision: https://phabricator.services.mozilla.com/D36624

diff --git a/dom/fetch/FetchDriver.cpp b/dom/fetch/FetchDriver.cpp
--- a/dom/fetch/FetchDriver.cpp
+++ b/dom/fetch/FetchDriver.cpp
@@ -530,32 +530,34 @@ FetchDriver::OnStartRequest(nsIRequest* 
         foundOpaqueRedirect = true;
       }
     }
 
     nsAutoCString statusText;
     rv = httpChannel->GetResponseStatusText(statusText);
     MOZ_ASSERT(NS_SUCCEEDED(rv));
 
-    response = new InternalResponse(responseStatus, statusText);
+    response = new InternalResponse(responseStatus, statusText,
+                                    mRequest->GetCredentialsMode());
 
     response->Headers()->FillResponseHeaders(httpChannel);
 
     // If Content-Encoding or Transfer-Encoding headers are set, then the actual
     // Content-Length (which refer to the decoded data) is obscured behind the encodings.
     ErrorResult result;
     if (response->Headers()->Has(NS_LITERAL_CSTRING("content-encoding"), result) ||
         response->Headers()->Has(NS_LITERAL_CSTRING("transfer-encoding"), result)) {
       NS_WARNING("Cannot know response Content-Length due to presence of Content-Encoding "
                  "or Transfer-Encoding headers.");
       contentLength = InternalResponse::UNKNOWN_BODY_SIZE;
     }
     MOZ_ASSERT(!result.Failed());
   } else {
-    response = new InternalResponse(200, NS_LITERAL_CSTRING("OK"));
+    response = new InternalResponse(200, NS_LITERAL_CSTRING("OK"),
+                                    mRequest->GetCredentialsMode());
 
     ErrorResult result;
     nsAutoCString contentType;
     rv = channel->GetContentType(contentType);
     if (NS_SUCCEEDED(rv) && !contentType.IsEmpty()) {
       nsAutoCString contentCharset;
       channel->GetContentCharset(contentCharset);
       if (NS_SUCCEEDED(rv) && !contentCharset.IsEmpty()) {
diff --git a/dom/fetch/InternalHeaders.cpp b/dom/fetch/InternalHeaders.cpp
--- a/dom/fetch/InternalHeaders.cpp
+++ b/dom/fetch/InternalHeaders.cpp
@@ -408,54 +408,62 @@ InternalHeaders::BasicHeaders(InternalHe
   basic->Delete(NS_LITERAL_CSTRING("Set-Cookie"), result);
   MOZ_ASSERT(!result.Failed());
   basic->Delete(NS_LITERAL_CSTRING("Set-Cookie2"), result);
   MOZ_ASSERT(!result.Failed());
   return basic.forget();
 }
 
 // static
-already_AddRefed<InternalHeaders>
-InternalHeaders::CORSHeaders(InternalHeaders* aHeaders)
-{
+already_AddRefed<InternalHeaders> InternalHeaders::CORSHeaders(
+    InternalHeaders* aHeaders, RequestCredentials aCredentialsMode) {
   RefPtr<InternalHeaders> cors = new InternalHeaders(aHeaders->mGuard);
   ErrorResult result;
 
   nsAutoCString acExposedNames;
   aHeaders->GetFirst(NS_LITERAL_CSTRING("Access-Control-Expose-Headers"), acExposedNames, result);
   MOZ_ASSERT(!result.Failed());
 
+  bool allowAllHeaders = false;
   AutoTArray<nsCString, 5> exposeNamesArray;
   nsCCharSeparatedTokenizer exposeTokens(acExposedNames, ',');
   while (exposeTokens.hasMoreTokens()) {
     const nsDependentCSubstring& token = exposeTokens.nextToken();
     if (token.IsEmpty()) {
       continue;
     }
 
     if (!NS_IsValidHTTPToken(token)) {
       NS_WARNING("Got invalid HTTP token in Access-Control-Expose-Headers. Header value is:");
       NS_WARNING(acExposedNames.get());
       exposeNamesArray.Clear();
       break;
     }
 
+    if (token.EqualsLiteral("*") &&
+        aCredentialsMode != RequestCredentials::Include) {
+      allowAllHeaders = true;
+    }
+
     exposeNamesArray.AppendElement(token);
   }
 
   nsCaseInsensitiveCStringArrayComparator comp;
   for (uint32_t i = 0; i < aHeaders->mList.Length(); ++i) {
     const Entry& entry = aHeaders->mList[i];
-    if (entry.mName.EqualsIgnoreCase("cache-control") ||
-        entry.mName.EqualsIgnoreCase("content-language") ||
-        entry.mName.EqualsIgnoreCase("content-type") ||
-        entry.mName.EqualsIgnoreCase("expires") ||
-        entry.mName.EqualsIgnoreCase("last-modified") ||
-        entry.mName.EqualsIgnoreCase("pragma") ||
-        exposeNamesArray.Contains(entry.mName, comp)) {
+    if (allowAllHeaders) {
+      cors->Append(entry.mName, entry.mValue, result);
+      MOZ_ASSERT(!result.Failed());
+    } else if (entry.mName.EqualsIgnoreCase("cache-control") ||
+               entry.mName.EqualsIgnoreCase("content-language") ||
+               entry.mName.EqualsIgnoreCase("content-type") ||
+               entry.mName.EqualsIgnoreCase("expires") ||
+               entry.mName.EqualsIgnoreCase("last-modified") ||
+               entry.mName.EqualsIgnoreCase("pragma") ||
+               exposeNamesArray.Contains(entry.mName, comp)) {
       cors->Append(entry.mName, entry.mValue, result);
       MOZ_ASSERT(!result.Failed());
     }
   }
 
   return cors.forget();
 }
 
diff --git a/dom/fetch/InternalHeaders.h b/dom/fetch/InternalHeaders.h
--- a/dom/fetch/InternalHeaders.h
+++ b/dom/fetch/InternalHeaders.h
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_InternalHeaders_h
 #define mozilla_dom_InternalHeaders_h
 
 // needed for HeadersGuardEnum.
 #include "mozilla/dom/HeadersBinding.h"
+#include "mozilla/dom/RequestBinding.h"
 #include "mozilla/dom/UnionTypes.h"
 
 #include "nsClassHashtable.h"
 #include "nsWrapperCache.h"
 
 namespace mozilla {
 
 class ErrorResult;
@@ -118,24 +119,24 @@ public:
 
   bool HasOnlySimpleHeaders() const;
 
   bool HasRevalidationHeaders() const;
 
   static already_AddRefed<InternalHeaders>
   BasicHeaders(InternalHeaders* aHeaders);
 
-  static already_AddRefed<InternalHeaders>
-  CORSHeaders(InternalHeaders* aHeaders);
+  static already_AddRefed<InternalHeaders> CORSHeaders(
+      InternalHeaders* aHeaders,
+      RequestCredentials mCredentialsMode = RequestCredentials::Omit);
 
-  void
-  GetEntries(nsTArray<InternalHeaders::Entry>& aEntries) const;
+  void GetEntries(nsTArray<InternalHeaders::Entry>& aEntries) const;
 
-  void
-  GetUnsafeHeaders(nsTArray<nsCString>& aNames) const;
+  void GetUnsafeHeaders(nsTArray<nsCString>& aNames) const;
+
 private:
   virtual ~InternalHeaders();
 
   static bool IsInvalidName(const nsACString& aName, ErrorResult& aRv);
   static bool IsInvalidValue(const nsACString& aValue, ErrorResult& aRv);
   bool IsImmutable(ErrorResult& aRv) const;
   bool IsForbiddenRequestHeader(const nsCString& aName) const;
   bool IsForbiddenRequestNoCorsHeader(const nsCString& aName) const;
diff --git a/dom/fetch/InternalResponse.cpp b/dom/fetch/InternalResponse.cpp
--- a/dom/fetch/InternalResponse.cpp
+++ b/dom/fetch/InternalResponse.cpp
@@ -12,24 +12,25 @@
 #include "mozilla/ipc/PBackgroundSharedTypes.h"
 #include "mozilla/ipc/IPCStreamUtils.h"
 #include "nsIURI.h"
 #include "nsStreamUtils.h"
 
 namespace mozilla {
 namespace dom {
 
-InternalResponse::InternalResponse(uint16_t aStatus, const nsACString& aStatusText)
+InternalResponse::InternalResponse(uint16_t aStatus,
+                                   const nsACString& aStatusText,
+                                   RequestCredentials aCredentialsMode)
   : mType(ResponseType::Default)
   , mStatus(aStatus)
   , mStatusText(aStatusText)
   , mHeaders(new InternalHeaders(HeadersGuardEnum::Response))
   , mBodySize(UNKNOWN_BODY_SIZE)
-{
-}
+  , mCredentialsMode(aCredentialsMode) {}
 
 already_AddRefed<InternalResponse>
 InternalResponse::FromIPC(const IPCInternalResponse& aIPCResponse)
 {
   if (aIPCResponse.type() == ResponseType::Error) {
     return InternalResponse::NetworkError();
   }
 
@@ -179,17 +180,17 @@ InternalResponse::BasicResponse()
 }
 
 already_AddRefed<InternalResponse>
 InternalResponse::CORSResponse()
 {
   MOZ_ASSERT(!mWrappedResponse, "Can't CORSResponse a already wrapped response");
   RefPtr<InternalResponse> cors = CreateIncompleteCopy();
   cors->mType = ResponseType::Cors;
-  cors->mHeaders = InternalHeaders::CORSHeaders(Headers());
+  cors->mHeaders = InternalHeaders::CORSHeaders(Headers(), mCredentialsMode);
   cors->mWrappedResponse = this;
   return cors.forget();
 }
 
 void
 InternalResponse::SetPrincipalInfo(UniquePtr<mozilla::ipc::PrincipalInfo> aPrincipalInfo)
 {
   mPrincipalInfo = Move(aPrincipalInfo);
diff --git a/dom/fetch/InternalResponse.h b/dom/fetch/InternalResponse.h
--- a/dom/fetch/InternalResponse.h
+++ b/dom/fetch/InternalResponse.h
@@ -6,16 +6,17 @@
 
 #ifndef mozilla_dom_InternalResponse_h
 #define mozilla_dom_InternalResponse_h
 
 #include "nsIInputStream.h"
 #include "nsISupportsImpl.h"
 
 #include "mozilla/dom/InternalHeaders.h"
+#include "mozilla/dom/RequestBinding.h"
 #include "mozilla/dom/ResponseBinding.h"
 #include "mozilla/dom/ChannelInfo.h"
 #include "mozilla/UniquePtr.h"
 
 namespace mozilla {
 namespace ipc {
 class PrincipalInfo;
 class AutoIPCStream;
@@ -28,17 +29,19 @@ class IPCInternalResponse;
 
 class InternalResponse final
 {
   friend class FetchDriver;
 
 public:
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(InternalResponse)
 
-  InternalResponse(uint16_t aStatus, const nsACString& aStatusText);
+  InternalResponse(
+      uint16_t aStatus, const nsACString& aStatusText,
+      RequestCredentials aCredentialsMode = RequestCredentials::Omit);
 
   static already_AddRefed<InternalResponse>
   FromIPC(const IPCInternalResponse& aIPCResponse);
 
   template<typename M>
   void
   ToIPC(IPCInternalResponse* aIPCResponse,
         M* aManager,
@@ -296,16 +299,19 @@ private:
   // Unless stated otherwise, it is the empty list. The current url is the last
   // element in mURLlist
   nsTArray<nsCString> mURLList;
   const uint16_t mStatus;
   const nsCString mStatusText;
   RefPtr<InternalHeaders> mHeaders;
   nsCOMPtr<nsIInputStream> mBody;
   int64_t mBodySize;
+
+  RequestCredentials mCredentialsMode;
+
 public:
   static const int64_t UNKNOWN_BODY_SIZE = -1;
 private:
   ChannelInfo mChannelInfo;
   UniquePtr<mozilla::ipc::PrincipalInfo> mPrincipalInfo;
 
   // For filtered responses.
   // Cache, and SW interception should always serialize/access the underlying
diff --git a/dom/xhr/XMLHttpRequestMainThread.cpp b/dom/xhr/XMLHttpRequestMainThread.cpp
--- a/dom/xhr/XMLHttpRequestMainThread.cpp
+++ b/dom/xhr/XMLHttpRequestMainThread.cpp
@@ -1208,17 +1208,20 @@ XMLHttpRequestMainThread::IsSafeHeader(c
   while (exposeTokens.hasMoreTokens()) {
     const nsDependentCSubstring& token = exposeTokens.nextToken();
     if (token.IsEmpty()) {
       continue;
     }
     if (!NS_IsValidHTTPToken(token)) {
       return false;
     }
-    if (aHeader.Equals(token, nsCaseInsensitiveCStringComparator())) {
+
+    if (token.EqualsLiteral("*") && !mFlagACwithCredentials) {
+      isSafe = true;
+    } else if (aHeader.Equals(token, nsCaseInsensitiveCStringComparator())) {
       isSafe = true;
     }
   }
   return isSafe;
 }
 
 NS_IMETHODIMP
 XMLHttpRequestMainThread::GetAllResponseHeaders(nsACString& aOut)
