# HG changeset patch
# User Tom Tung <shes050117@gmail.com>
# Date 1499677889 -28800
#      Mon Jul 10 17:11:29 2017 +0800
# Node ID 9e149be931c1c30c8e6ae05292f2862f2c645259
# Parent  ad17384b5552207f2fa82cd694fc268406543669
Bug 1290481 - P1: Make InternalResponse and CacheResponse be able to keep padding size. r=bkelly

MozReview-Commit-ID: LIxkSdfPZtf

diff --git a/dom/cache/CacheTypes.ipdlh b/dom/cache/CacheTypes.ipdlh
--- a/dom/cache/CacheTypes.ipdlh
+++ b/dom/cache/CacheTypes.ipdlh
@@ -81,16 +81,17 @@ struct CacheResponse
   nsCString[] urlList;
   uint32_t status;
   nsCString statusText;
   HeadersEntry[] headers;
   HeadersGuardEnum headersGuard;
   CacheReadStreamOrVoid body;
   IPCChannelInfo channelInfo;
   OptionalPrincipalInfo principalInfo;
+  int64_t paddingSize;
 };
 
 union CacheResponseOrVoid
 {
   void_t;
   CacheResponse;
 };
 
diff --git a/dom/cache/TypeUtils.cpp b/dom/cache/TypeUtils.cpp
--- a/dom/cache/TypeUtils.cpp
+++ b/dom/cache/TypeUtils.cpp
@@ -196,16 +196,18 @@ TypeUtils::ToCacheResponseWithoutBody(Ca
   ToHeadersEntryList(aOut.headers(), headers);
   aOut.headersGuard() = headers->Guard();
   aOut.channelInfo() = aIn.GetChannelInfo().AsIPCChannelInfo();
   if (aIn.GetPrincipalInfo()) {
     aOut.principalInfo() = *aIn.GetPrincipalInfo();
   } else {
     aOut.principalInfo() = void_t();
   }
+
+  aOut.paddingSize() = aIn.GetPaddingSize();
 }
 
 void
 TypeUtils::ToCacheResponse(JSContext* aCx, CacheResponse& aOut, Response& aIn,
                            nsTArray<UniquePtr<AutoIPCStream>>& aStreamCleanupList,
                            ErrorResult& aRv)
 {
   if (aIn.BodyUsed()) {
@@ -299,16 +301,18 @@ TypeUtils::ToResponse(const CacheRespons
     case ResponseType::Opaqueredirect:
       ir = ir->OpaqueRedirectResponse();
       break;
     default:
       MOZ_CRASH("Unexpected ResponseType!");
   }
   MOZ_DIAGNOSTIC_ASSERT(ir);
 
+  ir->SetPaddingSize(aIn.paddingSize());
+
   RefPtr<Response> ref = new Response(GetGlobalObject(), ir, nullptr);
   return ref.forget();
 }
 already_AddRefed<InternalRequest>
 TypeUtils::ToInternalRequest(const CacheRequest& aIn)
 {
   nsAutoCString url(aIn.urlWithoutQuery());
   url.Append(aIn.urlQuery());
diff --git a/dom/fetch/InternalResponse.cpp b/dom/fetch/InternalResponse.cpp
--- a/dom/fetch/InternalResponse.cpp
+++ b/dom/fetch/InternalResponse.cpp
@@ -20,16 +20,17 @@ namespace dom {
 InternalResponse::InternalResponse(uint16_t aStatus,
                                    const nsACString& aStatusText,
                                    RequestCredentials aCredentialsMode)
   : mType(ResponseType::Default)
   , mStatus(aStatus)
   , mStatusText(aStatusText)
   , mHeaders(new InternalHeaders(HeadersGuardEnum::Response))
   , mBodySize(UNKNOWN_BODY_SIZE)
+  , mPaddingSize(UNKNOWN_PADDING_SIZE)
   , mCredentialsMode(aCredentialsMode) {}
 
 already_AddRefed<InternalResponse>
 InternalResponse::FromIPC(const IPCInternalResponse& aIPCResponse)
 {
   if (aIPCResponse.type() == ResponseType::Error) {
     return InternalResponse::NetworkError();
   }
@@ -138,16 +139,20 @@ InternalResponse::ToIPC(IPCInternalRespo
 }
 
 already_AddRefed<InternalResponse>
 InternalResponse::Clone(CloneType aCloneType)
 {
   RefPtr<InternalResponse> clone = CreateIncompleteCopy();
 
   clone->mHeaders = new InternalHeaders(*mHeaders);
+
+  // Make sure the clone response will have the same padding size.
+  clone->mPaddingSize = mPaddingSize;
+
   if (mWrappedResponse) {
     clone->mWrappedResponse = mWrappedResponse->Clone(aCloneType);
     MOZ_ASSERT(!mBody);
     return clone.forget();
   }
 
   if (!mBody || aCloneType == eDontCloneInputStream) {
     return clone.forget();
@@ -185,16 +190,43 @@ InternalResponse::CORSResponse()
   MOZ_ASSERT(!mWrappedResponse, "Can't CORSResponse a already wrapped response");
   RefPtr<InternalResponse> cors = CreateIncompleteCopy();
   cors->mType = ResponseType::Cors;
   cors->mHeaders = InternalHeaders::CORSHeaders(Headers(), mCredentialsMode);
   cors->mWrappedResponse = this;
   return cors.forget();
 }
 
+int64_t
+InternalResponse::GetPaddingSize()
+{
+  // We initialize padding size to an unknown size (-1). After cached, we only
+  // pad opaque response. Opaque response's padding size might be unknown before
+  // cached.
+  MOZ_DIAGNOSTIC_ASSERT(mType == ResponseType::Opaque ||
+                        mPaddingSize == UNKNOWN_PADDING_SIZE);
+  MOZ_DIAGNOSTIC_ASSERT(mPaddingSize == UNKNOWN_PADDING_SIZE ||
+                        mPaddingSize >= 0);
+
+  return mPaddingSize;
+}
+
+void
+InternalResponse::SetPaddingSize(int64_t aPaddingSize)
+{
+  // We should only pad the opaque response.
+  MOZ_DIAGNOSTIC_ASSERT((mType == ResponseType::Opaque) !=
+                        (aPaddingSize ==
+                         InternalResponse::UNKNOWN_PADDING_SIZE));
+  MOZ_DIAGNOSTIC_ASSERT(aPaddingSize == UNKNOWN_PADDING_SIZE ||
+                        aPaddingSize >= 0);
+
+  mPaddingSize = aPaddingSize;
+}
+
 void
 InternalResponse::SetPrincipalInfo(UniquePtr<mozilla::ipc::PrincipalInfo> aPrincipalInfo)
 {
   mPrincipalInfo = Move(aPrincipalInfo);
 }
 
 LoadTainting
 InternalResponse::GetTainting() const
diff --git a/dom/fetch/InternalResponse.h b/dom/fetch/InternalResponse.h
--- a/dom/fetch/InternalResponse.h
+++ b/dom/fetch/InternalResponse.h
@@ -231,16 +231,22 @@ public:
     MOZ_ASSERT(aBodySize == UNKNOWN_BODY_SIZE || aBodySize >= 0);
     // If body is not given, then size must be unknown.
     MOZ_ASSERT_IF(!aBody, aBodySize == UNKNOWN_BODY_SIZE);
 
     mBody = aBody;
     mBodySize = aBodySize;
   }
 
+  int64_t
+  GetPaddingSize();
+
+  void
+  SetPaddingSize(int64_t aPaddingSize);
+
   void
   InitChannelInfo(nsIChannel* aChannel)
   {
     mChannelInfo.InitFromChannel(aChannel);
   }
 
   void
   InitChannelInfo(const mozilla::ipc::IPCChannelInfo& aChannelInfo)
@@ -299,21 +305,23 @@ private:
   // Unless stated otherwise, it is the empty list. The current url is the last
   // element in mURLlist
   nsTArray<nsCString> mURLList;
   const uint16_t mStatus;
   const nsCString mStatusText;
   RefPtr<InternalHeaders> mHeaders;
   nsCOMPtr<nsIInputStream> mBody;
   int64_t mBodySize;
+  int64_t mPaddingSize;
 
   RequestCredentials mCredentialsMode;
 
 public:
   static const int64_t UNKNOWN_BODY_SIZE = -1;
+  static const int64_t UNKNOWN_PADDING_SIZE = -1;
 private:
   ChannelInfo mChannelInfo;
   UniquePtr<mozilla::ipc::PrincipalInfo> mPrincipalInfo;
 
   // For filtered responses.
   // Cache, and SW interception should always serialize/access the underlying
   // unfiltered headers and when deserializing, create an InternalResponse
   // with the unfiltered headers followed by wrapping it.
