# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1504161112 18000
#      Thu Aug 31 01:31:52 2017 -0500
# Node ID 52f8d97719d26448eb089f198b199e398b135f4b
# Parent  2c541bf078a5640dee47460c5f1323e105ba6e54
servo: Merge #18320 - Rewrite CounterStyleOrNone::from_gecko_value to use fewer binding functions (from upsuper:counter-style-clone); r=heycam

This is the Servo side change of [bug 1393189](https://bugzilla.mozilla.org/show_bug.cgi?id=1393189).

Source-Repo: https://github.com/servo/servo
Source-Revision: 12ca7d9e96ebeddd040b1eceb82f80ddd780c294

diff --git a/servo/components/style/gecko/values.rs b/servo/components/style/gecko/values.rs
--- a/servo/components/style/gecko/values.rs
+++ b/servo/components/style/gecko/values.rs
@@ -1,18 +1,19 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #![allow(unsafe_code)]
 
 //! Different kind of helpers to interact with Gecko values.
 
+use Atom;
 use app_units::Au;
-use counter_style::Symbol;
+use counter_style::{Symbol, Symbols};
 use cssparser::RGBA;
 use gecko_bindings::structs::{CounterStylePtr, nsStyleCoord};
 use gecko_bindings::structs::{StyleGridTrackBreadth, StyleShapeRadius};
 use gecko_bindings::sugar::ns_style_coord::{CoordData, CoordDataMut, CoordDataValue};
 use media_queries::Device;
 use nsstring::{nsACString, nsCString};
 use std::cmp::max;
 use values::{Auto, Either, ExtremumLength, None_, Normal};
@@ -471,38 +472,40 @@ impl CounterStyleOrNone {
                     .map(|symbol| symbol as &nsACString as *const _)
                     .collect();
                 unsafe { set_symbols(gecko_value, symbols_type.to_gecko_keyword(),
                                      symbols.as_ptr(), symbols.len() as u32) };
             }
         }
     }
 
-    /// Convert Gecko CounterStylePtr to CounterStyleOrNone.
-    pub fn from_gecko_value(gecko_value: &CounterStylePtr) -> Self {
-        use counter_style::{Symbol, Symbols};
-        use gecko_bindings::bindings::Gecko_CounterStyle_GetName;
-        use gecko_bindings::bindings::Gecko_CounterStyle_GetSymbols;
-        use gecko_bindings::bindings::Gecko_CounterStyle_GetSystem;
-        use gecko_bindings::bindings::Gecko_CounterStyle_IsName;
-        use gecko_bindings::bindings::Gecko_CounterStyle_IsNone;
+    /// Convert Gecko CounterStylePtr to CounterStyleOrNone or String.
+    pub fn from_gecko_value(gecko_value: &CounterStylePtr) -> Either<Self, String> {
+        use gecko_bindings::bindings;
         use values::CustomIdent;
         use values::generics::SymbolsType;
 
-        if unsafe { Gecko_CounterStyle_IsNone(gecko_value) } {
-            CounterStyleOrNone::None
-        } else if unsafe { Gecko_CounterStyle_IsName(gecko_value) } {
-            ns_auto_string!(name);
-            unsafe { Gecko_CounterStyle_GetName(gecko_value, &mut *name) };
-            CounterStyleOrNone::Name(CustomIdent((&*name).into()))
+        let name = unsafe { bindings::Gecko_CounterStyle_GetName(gecko_value) };
+        if !name.is_null() {
+            let name = Atom::from(name);
+            if name == atom!("none") {
+                Either::First(CounterStyleOrNone::None)
+            } else {
+                Either::First(CounterStyleOrNone::Name(CustomIdent(name)))
+            }
         } else {
-            let system = unsafe { Gecko_CounterStyle_GetSystem(gecko_value) };
-            let symbol_type = SymbolsType::from_gecko_keyword(system as u32);
-            let symbols = unsafe {
-                let ref gecko_symbols = *Gecko_CounterStyle_GetSymbols(gecko_value);
-                gecko_symbols.iter().map(|gecko_symbol| {
+            let anonymous = unsafe {
+                bindings::Gecko_CounterStyle_GetAnonymous(gecko_value).as_ref()
+            }.unwrap();
+            let symbols = &anonymous.mSymbols;
+            if anonymous.mSingleString {
+                debug_assert_eq!(symbols.len(), 1);
+                Either::Second(symbols[0].to_string())
+            } else {
+                let symbol_type = SymbolsType::from_gecko_keyword(anonymous.mSystem as u32);
+                let symbols = symbols.iter().map(|gecko_symbol| {
                     Symbol::String(gecko_symbol.to_string())
-                }).collect()
-            };
-            CounterStyleOrNone::Symbols(symbol_type, Symbols(symbols))
+                }).collect();
+                Either::First(CounterStyleOrNone::Symbols(symbol_type, Symbols(symbols)))
+            }
         }
     }
 }
diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -4154,29 +4154,24 @@ fn static_assert() {
         }
     }
 
     pub fn reset_list_style_type(&mut self, other: &Self) {
         self.copy_list_style_type_from(other)
     }
 
     pub fn clone_list_style_type(&self) -> longhands::list_style_type::computed_value::T {
-        use gecko_bindings::bindings::Gecko_CounterStyle_IsSingleString;
-        use gecko_bindings::bindings::Gecko_CounterStyle_GetSingleString;
         use self::longhands::list_style_type::computed_value::T;
+        use values::Either;
         use values::generics::CounterStyleOrNone;
 
-        if unsafe { Gecko_CounterStyle_IsSingleString(&self.gecko.mCounterStyle) } {
-            ns_auto_string!(single_string);
-            unsafe {
-                Gecko_CounterStyle_GetSingleString(&self.gecko.mCounterStyle, &mut *single_string)
-            };
-            T::String(single_string.to_string())
-        } else {
-            T::CounterStyle(CounterStyleOrNone::from_gecko_value(&self.gecko.mCounterStyle))
+        let result = CounterStyleOrNone::from_gecko_value(&self.gecko.mCounterStyle);
+        match result {
+            Either::First(counter_style) => T::CounterStyle(counter_style),
+            Either::Second(string) => T::String(string),
         }
     }
 
     pub fn set_quotes(&mut self, other: longhands::quotes::computed_value::T) {
         use gecko_bindings::bindings::Gecko_NewStyleQuoteValues;
         use gecko_bindings::sugar::refptr::UniqueRefPtr;
 
         let mut refptr = unsafe {
@@ -5612,16 +5607,17 @@ clip-path
     pub fn reset_content(&mut self, other: &Self) {
         self.copy_content_from(other)
     }
 
     pub fn clone_content(&self) -> longhands::content::computed_value::T {
         use gecko::conversions::string_from_chars_pointer;
         use gecko_bindings::structs::nsStyleContentType::*;
         use properties::longhands::content::computed_value::{T, ContentItem};
+        use values::Either;
         use values::generics::CounterStyleOrNone;
         use values::specified::url::SpecifiedUrl;
         use values::specified::Attr;
 
         if self.gecko.mContents.is_empty() {
             return T::Normal;
         }
 
@@ -5659,16 +5655,21 @@ clip-path
                         ContentItem::Attr(Attr { namespace, attribute })
                     },
                     eStyleContentType_Counter | eStyleContentType_Counters => {
                         let gecko_function =
                             unsafe { &**gecko_content.mContent.mCounters.as_ref() };
                         let ident = gecko_function.mIdent.to_string();
                         let style =
                             CounterStyleOrNone::from_gecko_value(&gecko_function.mCounterStyle);
+                        let style = match style {
+                            Either::First(counter_style) => counter_style,
+                            Either::Second(_) =>
+                                unreachable!("counter function shouldn't have single string type"),
+                        };
                         if gecko_content.mType == eStyleContentType_Counter {
                             ContentItem::Counter(ident, style)
                         } else {
                             let separator = gecko_function.mSeparator.to_string();
                             ContentItem::Counters(ident, separator, style)
                         }
                     },
                     eStyleContentType_Image => {
