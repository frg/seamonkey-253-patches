# HG changeset patch
# User Tom Tromey <tom@tromey.com>
# Date 1504038118 21600
#      Tue Aug 29 14:21:58 2017 -0600
# Node ID fcd4aabcec11eda08077085ea1ca6aa1bbac6c8a
# Parent  e96f86e8071aff4f07ac4e872738591e39063fa6
Bug 1391768 - register existing source actors in source map service init; r=bgrins

MozReview-Commit-ID: Ao1viu9sk5O

diff --git a/devtools/client/framework/source-map-url-service.js b/devtools/client/framework/source-map-url-service.js
--- a/devtools/client/framework/source-map-url-service.js
+++ b/devtools/client/framework/source-map-url-service.js
@@ -31,21 +31,24 @@ function SourceMapURLService(target, thr
   this._onPrefChanged = this._onPrefChanged.bind(this);
 
   target.on("source-updated", this._onSourceUpdated);
   target.on("will-navigate", this.reset);
 
   Services.prefs.addObserver(SOURCE_MAP_PREF, this._onPrefChanged);
 
   // Start fetching the sources now.
-  this._loadingPromise = new Promise(resolve => {
-    threadClient.getSources(({sources}) => {
-      // Just ignore errors.
-      resolve(sources);
-    });
+  this._loadingPromise = threadClient.getSources().then(({sources}) => {
+    // Ignore errors.  Register the sources we got; we can't rely on
+    // an event to arrive if the source actor already existed.
+    for (let source of sources) {
+      this._onSourceUpdated(null, {source});
+    }
+  }, e => {
+    // Also ignore any protocol-based errors.
   });
 }
 
 /**
  * Reset the service.  This flushes the internal cache.
  */
 SourceMapURLService.prototype.reset = function () {
   this._sourceMapService.clearSourceMaps();
diff --git a/devtools/client/framework/test/browser.ini b/devtools/client/framework/test/browser.ini
--- a/devtools/client/framework/test/browser.ini
+++ b/devtools/client/framework/test/browser.ini
@@ -58,16 +58,17 @@ skip-if = debug # Bug 1282269
 [browser_keybindings_01.js]
 [browser_keybindings_02.js]
 [browser_keybindings_03.js]
 [browser_menu_api.js]
 [browser_new_activation_workflow.js]
 [browser_source_map-01.js]
 [browser_source_map-absolute.js]
 [browser_source_map-cross-domain.js]
+[browser_source_map-init.js]
 [browser_source_map-inline.js]
 [browser_source_map-no-race.js]
 [browser_source_map-reload.js]
 [browser_target_from_url.js]
 [browser_target_events.js]
 [browser_target_remote.js]
 [browser_target_support.js]
 [browser_toolbox_custom_host.js]
diff --git a/devtools/client/framework/test/browser_source_map-init.js b/devtools/client/framework/test/browser_source_map-init.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/framework/test/browser_source_map-init.js
@@ -0,0 +1,43 @@
+/* Any copyright is dedicated to the Public Domain.
+ http://creativecommons.org/publicdomain/zero/1.0/ */
+
+// Test that the source map service initializes properly when source
+// actors have already been created.  Regression test for bug 1391768.
+
+"use strict";
+
+const JS_URL = URL_ROOT + "code_bundle_no_race.js";
+
+const PAGE_URL = `data:text/html,
+<!doctype html>
+
+<html>
+  <head>
+    <meta charset="utf-8"/>
+    <title>Empty test page to test race case</title>
+  </head>
+
+  <body>
+    <script src="${JS_URL}"></script>
+  </body>
+
+</html>`;
+
+const ORIGINAL_URL = "webpack:///code_no_race.js";
+
+const GENERATED_LINE = 84;
+const ORIGINAL_LINE = 11;
+
+add_task(function* () {
+  // Opening the debugger causes the source actors to be created.
+  const toolbox = yield openNewTabAndToolbox(PAGE_URL, "jsdebugger");
+  // In bug 1391768, when the sourceMapURLService was created, it was
+  // ignoring any source actors that already existed, leading to
+  // source-mapping failures for those.
+  const service = toolbox.sourceMapURLService;
+
+  info(`checking original location for ${JS_URL}:${GENERATED_LINE}`);
+  let newLoc = yield service.originalPositionFor(JS_URL, GENERATED_LINE);
+  is(newLoc.sourceUrl, ORIGINAL_URL, "check mapped URL");
+  is(newLoc.line, ORIGINAL_LINE, "check mapped line number");
+});
