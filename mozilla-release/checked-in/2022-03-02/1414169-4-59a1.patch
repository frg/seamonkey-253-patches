# HG changeset patch
# User Michael Froman <mfroman@mozilla.com>
# Date 1511382482 21600
# Node ID b907280a1894164ab9a7faa8da62045b01a795f4
# Parent  ca8c614eba18ac4685364abf0c3848c7708442e1
Bug 1414169 - pt 4 - Trickled ICE candidates are highlighted with a light blue background. r=ng

MozReview-Commit-ID: JEMmf9LjvYd

diff --git a/dom/media/webrtc/WebrtcGlobal.h b/dom/media/webrtc/WebrtcGlobal.h
--- a/dom/media/webrtc/WebrtcGlobal.h
+++ b/dom/media/webrtc/WebrtcGlobal.h
@@ -76,16 +76,17 @@ struct ParamTraits<mozilla::dom::RTCStat
     WriteParam(aMsg, aParam.mPcid);
     WriteParam(aMsg, aParam.mRemoteSdp);
     WriteParam(aMsg, aParam.mTimestamp);
     WriteParam(aMsg, aParam.mIceRestarts);
     WriteParam(aMsg, aParam.mIceRollbacks);
     WriteParam(aMsg, aParam.mTransportStats);
     WriteParam(aMsg, aParam.mRtpContributingSourceStats);
     WriteParam(aMsg, aParam.mOfferer);
+    WriteParam(aMsg, aParam.mTrickledIceCandidateStats);
   }
 
   static bool Read(const Message* aMsg, PickleIterator* aIter, paramType* aResult)
   {
     if (!ReadParam(aMsg, aIter, &(aResult->mClosed)) ||
         !ReadParam(aMsg, aIter, &(aResult->mCodecStats)) ||
         !ReadParam(aMsg, aIter, &(aResult->mIceCandidatePairStats)) ||
         !ReadParam(aMsg, aIter, &(aResult->mIceCandidateStats)) ||
@@ -97,17 +98,18 @@ struct ParamTraits<mozilla::dom::RTCStat
         !ReadParam(aMsg, aIter, &(aResult->mOutboundRTPStreamStats)) ||
         !ReadParam(aMsg, aIter, &(aResult->mPcid)) ||
         !ReadParam(aMsg, aIter, &(aResult->mRemoteSdp)) ||
         !ReadParam(aMsg, aIter, &(aResult->mTimestamp)) ||
         !ReadParam(aMsg, aIter, &(aResult->mIceRestarts)) ||
         !ReadParam(aMsg, aIter, &(aResult->mIceRollbacks)) ||
         !ReadParam(aMsg, aIter, &(aResult->mTransportStats)) ||
         !ReadParam(aMsg, aIter, &(aResult->mRtpContributingSourceStats)) ||
-        !ReadParam(aMsg, aIter, &(aResult->mOfferer))) {
+        !ReadParam(aMsg, aIter, &(aResult->mOfferer)) ||
+        !ReadParam(aMsg, aIter, &(aResult->mTrickledIceCandidateStats))) {
       return false;
     }
 
     return true;
   }
 };
 
 typedef mozilla::dom::RTCStats RTCStats;
diff --git a/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp b/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp
--- a/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp
+++ b/media/webrtc/signaling/src/peerconnection/PeerConnectionImpl.cpp
@@ -2092,16 +2092,17 @@ public:
     mMediaStreamTrackStats.Construct();
     mMediaStreamStats.Construct();
     mTransportStats.Construct();
     mIceComponentStats.Construct();
     mIceCandidatePairStats.Construct();
     mIceCandidateStats.Construct();
     mCodecStats.Construct();
     mTimestamp.Construct(now);
+    mTrickledIceCandidateStats.Construct();
   }
 };
 
 NS_IMETHODIMP
 PeerConnectionImpl::GetStats(MediaStreamTrack *aSelector) {
   PC_AUTO_ENTER_API_CALL(true);
 
   if (!mMedia) {
@@ -3596,16 +3597,19 @@ static void ToRTCIceCandidateStats(
     cand.mPortNumber.Construct(candidate.cand_addr.port);
     cand.mTransport.Construct(
         NS_ConvertASCIItoUTF16(candidate.cand_addr.transport.c_str()));
     if (candidateType == RTCStatsType::Local_candidate) {
       cand.mMozLocalTransport.Construct(
           NS_ConvertASCIItoUTF16(candidate.local_addr.transport.c_str()));
     }
     report->mIceCandidateStats.Value().AppendElement(cand, fallible);
+    if (candidate.trickled) {
+      report->mTrickledIceCandidateStats.Value().AppendElement(cand, fallible);
+    }
   }
 }
 
 static void RecordIceStats_s(
     NrIceMediaStream& mediaStream,
     bool internalStats,
     DOMHighResTimeStamp now,
     RTCStatsReportInternal* report) {
diff --git a/toolkit/content/aboutwebrtc/aboutWebrtc.css b/toolkit/content/aboutwebrtc/aboutWebrtc.css
--- a/toolkit/content/aboutwebrtc/aboutWebrtc.css
+++ b/toolkit/content/aboutwebrtc/aboutWebrtc.css
@@ -74,16 +74,20 @@ html {
 .peer-connection table tr:nth-child(even) {
   background-color: #ddd;
 }
 
 .peer-connection table caption {
   text-align: left;
 }
 
+.trickled {
+  background-color: #bdf;
+}
+
 .info-label {
   font-weight: bold;
 }
 
 .section-ctrl {
   margin: 1em 1.5em;
 }
 
diff --git a/toolkit/content/aboutwebrtc/aboutWebrtc.js b/toolkit/content/aboutwebrtc/aboutWebrtc.js
--- a/toolkit/content/aboutwebrtc/aboutWebrtc.js
+++ b/toolkit/content/aboutwebrtc/aboutWebrtc.js
@@ -655,51 +655,81 @@ RTPStats.prototype = {
 };
 
 function ICEStats(report) {
   this._report = report;
 }
 
 ICEStats.prototype = {
   render() {
-    let tbody = [];
-    for (let stat of this.generateICEStats()) {
-      tbody.push([
-        stat["local-candidate"] || "",
-        stat["remote-candidate"] || "",
-        stat.state || "",
-        stat.priority || "",
-        stat.nominated || "",
-        stat.selected || "",
-        stat.bytesSent || "",
-        stat.bytesReceived || ""
-      ]);
-    }
-
-    let statsTable = new SimpleTable(
-      [getString("local_candidate"), getString("remote_candidate"), getString("ice_state"),
-       getString("priority"), getString("nominated"), getString("selected"),
-       getString("ice_pair_bytes_sent"), getString("ice_pair_bytes_received")],
-      tbody);
-
     let div = document.createElement("div");
     let heading = document.createElement("h4");
 
     heading.textContent = getString("ice_stats_heading");
     div.appendChild(heading);
 
-    div.appendChild(statsTable.render());
+    div.appendChild(this.renderICECandidateTable());
+    // add just a bit of vertical space between the restart/rollback
+    // counts and the ICE candidate pair table above.
+    div.appendChild(document.createElement("br"));
     div.appendChild(this.renderIceMetric("ice_restart_count_label",
                                          this._report.iceRestarts));
     div.appendChild(this.renderIceMetric("ice_rollback_count_label",
                                          this._report.iceRollbacks));
 
     return div;
   },
 
+  renderICECandidateTable() {
+    let caption = document.createElement("caption");
+    let captionSpan1 = document.createElement("span");
+    captionSpan1.textContent = `${getString("trickle_caption_msg")} `;
+    let captionSpan2 = document.createElement("span");
+    captionSpan2.textContent = getString("trickle_highlight_color_name");
+    captionSpan2.className = "trickled";
+    caption.appendChild(captionSpan1);
+    caption.appendChild(captionSpan2);
+    caption.className = "no-print";
+
+    let stats = this.generateICEStats();
+    // don't use |stat.x || ""| here because it hides 0 values
+    let tbody = stats.map(stat => [
+      stat["local-candidate"],
+      stat["remote-candidate"],
+      stat.state,
+      stat.priority,
+      stat.nominated,
+      stat.selected,
+      stat.bytesSent,
+      stat.bytesReceived
+    ].map(entry => Object.is(entry, undefined) ? "" : entry));
+
+    let statsTable = new SimpleTable(
+      ["local_candidate", "remote_candidate", "ice_state",
+       "priority", "nominated", "selected",
+       "ice_pair_bytes_sent", "ice_pair_bytes_received"
+      ].map(columnName => getString(columnName)),
+      tbody, caption).render();
+
+    // after rendering the table, we need to change the class name for each
+    // candidate pair's local or remote candidate if it was trickled.
+    stats.forEach((stat, index) => {
+      // look at statsTable row index + 1 to skip column headers
+      let rowIndex = index + 1;
+      if (stat["remote-trickled"]) {
+        statsTable.rows[rowIndex].cells[1].className = "trickled";
+      }
+      if (stat["local-trickled"]) {
+        statsTable.rows[rowIndex].cells[0].className = "trickled";
+      }
+    });
+
+    return statsTable;
+  },
+
   renderIceMetric(labelName, value) {
     let info = document.createElement("div");
     let label = document.createElement("span");
     let body = document.createElement("span");
 
     label.className = "info-label";
     label.textContent = `${getString(labelName)}: `;
     info.appendChild(label);
@@ -714,16 +744,21 @@ ICEStats.prototype = {
     // Create an index based on candidate ID for each element in the
     // iceCandidateStats array.
     let candidates = new Map();
 
     for (let candidate of this._report.iceCandidateStats) {
       candidates.set(candidate.id, candidate);
     }
 
+    // a method to see if a given candidate id is in the array of tickled
+    // candidates.
+    let isTrickled = id => [...this._report.trickledIceCandidateStats].some(
+      candidate => candidate.id == id);
+
     // A component may have a remote or local candidate address or both.
     // Combine those with both; these will be the peer candidates.
     let matched = {};
     let stats = [];
     let stat;
 
     for (let pair of this._report.iceCandidatePairStats) {
       let local = candidates.get(pair.localCandidateId);
@@ -735,20 +770,26 @@ ICEStats.prototype = {
           state: pair.state,
           priority: pair.priority,
           nominated: pair.nominated,
           selected: pair.selected,
           bytesSent: pair.bytesSent,
           bytesReceived: pair.bytesReceived
         };
         matched[local.id] = true;
+        if (isTrickled(local.id)) {
+            stat["local-trickled"] = true;
+        }
 
         if (remote) {
           stat["remote-candidate"] = this.candidateToString(remote);
           matched[remote.id] = true;
+          if (isTrickled(remote.id)) {
+            stat["remote-trickled"] = true;
+          }
         }
         stats.push(stat);
       }
     }
 
     // add the unmatched candidates to the end of the table
     [...candidates.values()].filter(cand => !matched[cand.id]).forEach(
       cand => stats.push({[cand.type]: this.candidateToString(cand)})
diff --git a/toolkit/locales/en-US/chrome/global/aboutWebrtc.properties b/toolkit/locales/en-US/chrome/global/aboutWebrtc.properties
--- a/toolkit/locales/en-US/chrome/global/aboutWebrtc.properties
+++ b/toolkit/locales/en-US/chrome/global/aboutWebrtc.properties
@@ -98,16 +98,25 @@ typeRemote = Remote
 # or are left blank.
 nominated = Nominated
 
 # LOCALIZATION NOTE (selected): This adjective is used to label a table column.
 # Cells in this column contain the localized javascript string representation of "true"
 # or are left blank. This represents an attribute of an ICE candidate.
 selected = Selected
 
+# LOCALIZATION NOTE (trickle_caption_msg, trickle_highlight_color_name):
+# ICE candidates arriving after the remote answer arrives are considered
+# trickled (an attribute of an ICE candidate).  These are highlighted in
+# the ICE stats table with light blue background.  This message is split
+# into two in order to highlight the word "blue" with a light blue
+# background to visually match the trickled ICE candidates.
+trickle_caption_msg = trickled candidates (arriving after answer) are highlighted in
+trickle_highlight_color_name = blue
+
 save_page_label = Save Page
 debug_mode_msg_label = Debug Mode
 debug_mode_off_state_label = Start Debug Mode
 debug_mode_on_state_label = Stop Debug Mode
 stats_heading = Session Statistics
 stats_clear = Clear History
 log_heading = Connection Log
 log_clear = Clear Log
