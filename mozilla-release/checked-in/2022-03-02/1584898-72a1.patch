# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1571915323 0
# Node ID d68244d4b2dc69acbd0d6f0d4620089a6ffeef8c
# Parent  99d6525620bcb0d5c09221590b7fd7bcad88a6eb
Bug 1584898 - Free nsPipe memory on a separate thread, r=Ehsan

Differential Revision: https://phabricator.services.mozilla.com/D50278

diff --git a/xpcom/io/nsSegmentedBuffer.cpp b/xpcom/io/nsSegmentedBuffer.cpp
--- a/xpcom/io/nsSegmentedBuffer.cpp
+++ b/xpcom/io/nsSegmentedBuffer.cpp
@@ -59,32 +59,32 @@ char* nsSegmentedBuffer::AppendNewSegmen
   mSegmentArray[mLastSegmentIndex] = seg;
   mLastSegmentIndex = ModSegArraySize(mLastSegmentIndex + 1);
   return seg;
 }
 
 bool nsSegmentedBuffer::DeleteFirstSegment() {
   NS_ASSERTION(mSegmentArray[mFirstSegmentIndex] != nullptr,
                "deleting bad segment");
-  free(mSegmentArray[mFirstSegmentIndex]);
+  FreeOMT(mSegmentArray[mFirstSegmentIndex]);
   mSegmentArray[mFirstSegmentIndex] = nullptr;
   int32_t last = ModSegArraySize(mLastSegmentIndex - 1);
   if (mFirstSegmentIndex == last) {
     mLastSegmentIndex = last;
     return true;
   } else {
     mFirstSegmentIndex = ModSegArraySize(mFirstSegmentIndex + 1);
     return false;
   }
 }
 
 bool nsSegmentedBuffer::DeleteLastSegment() {
   int32_t last = ModSegArraySize(mLastSegmentIndex - 1);
   NS_ASSERTION(mSegmentArray[last] != nullptr, "deleting bad segment");
-  free(mSegmentArray[last]);
+  FreeOMT(mSegmentArray[last]);
   mSegmentArray[last] = nullptr;
   mLastSegmentIndex = last;
   return (bool)(mLastSegmentIndex == mFirstSegmentIndex);
 }
 
 bool nsSegmentedBuffer::ReallocLastSegment(size_t aNewSize) {
   int32_t last = ModSegArraySize(mLastSegmentIndex - 1);
   NS_ASSERTION(mSegmentArray[last] != nullptr, "realloc'ing bad segment");
@@ -95,20 +95,20 @@ bool nsSegmentedBuffer::ReallocLastSegme
   }
   return false;
 }
 
 void nsSegmentedBuffer::Empty() {
   if (mSegmentArray) {
     for (uint32_t i = 0; i < mSegmentArrayCount; i++) {
       if (mSegmentArray[i]) {
-        free(mSegmentArray[i]);
+        FreeOMT(mSegmentArray[i]);
       }
     }
-    free(mSegmentArray);
+    FreeOMT(mSegmentArray);
     mSegmentArray = nullptr;
   }
   mSegmentArrayCount = NS_SEGMENTARRAY_INITIAL_COUNT;
   mFirstSegmentIndex = mLastSegmentIndex = 0;
 }
 
 #if 0
 void
@@ -142,9 +142,30 @@ TestSegmentedBuffer()
   empty = buf->DeleteFirstSegment();
   NS_ASSERTION(!empty, "DeleteFirstSegment failed");
   empty = buf->DeleteFirstSegment();
   NS_ASSERTION(empty, "DeleteFirstSegment failed");
   delete buf;
 }
 #endif
 
+void nsSegmentedBuffer::FreeOMT(void* aPtr) {
+  if (!NS_IsMainThread()) {
+    free(aPtr);
+    return;
+  }
+
+  nsCOMPtr<nsIRunnable> r = NS_NewRunnableFunction("nsSegmentedBuffer::FreeOMT",
+                                                   [aPtr]() { free(aPtr); });
+
+  if (!mIOThread) {
+    mIOThread = do_GetService(NS_STREAMTRANSPORTSERVICE_CONTRACTID);
+  }
+
+  // During the shutdown we are not able to obtain the IOThread and/or the
+  // dispatching of runnable fails.
+  if (NS_WARN_IF(!mIOThread) ||
+      NS_WARN_IF(NS_FAILED(mIOThread->Dispatch(r.forget())))) {
+    free(aPtr);
+  }
+}
+
 ////////////////////////////////////////////////////////////////////////////////
diff --git a/xpcom/io/nsSegmentedBuffer.h b/xpcom/io/nsSegmentedBuffer.h
--- a/xpcom/io/nsSegmentedBuffer.h
+++ b/xpcom/io/nsSegmentedBuffer.h
@@ -4,16 +4,18 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef nsSegmentedBuffer_h__
 #define nsSegmentedBuffer_h__
 
 #include "nsIMemory.h"
 
+class nsIEventTarget;
+
 class nsSegmentedBuffer {
  public:
   nsSegmentedBuffer()
       : mSegmentSize(0),
         mMaxSize(0),
         mSegmentArray(nullptr),
         mSegmentArrayCount(0),
         mFirstSegmentIndex(0),
@@ -69,16 +71,21 @@ class nsSegmentedBuffer {
 
  protected:
   uint32_t mSegmentSize;
   uint32_t mMaxSize;
   char** mSegmentArray;
   uint32_t mSegmentArrayCount;
   int32_t mFirstSegmentIndex;
   int32_t mLastSegmentIndex;
+
+ private:
+  void FreeOMT(void* aPtr);
+
+  nsCOMPtr<nsIEventTarget> mIOThread;
 };
 
 // NS_SEGMENTARRAY_INITIAL_SIZE: This number needs to start out as a
 // power of 2 given how it gets used. We double the segment array
 // when we overflow it, and use that fact that it's a power of 2
 // to compute a fast modulus operation in IsFull.
 //
 // 32 segment array entries can accommodate 128k of data if segments
