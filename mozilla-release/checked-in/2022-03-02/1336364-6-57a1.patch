# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1505924647 25200
#      Wed Sep 20 09:24:07 2017 -0700
# Node ID ea1993fd683d83fc287f02232e59c331efc4b956
# Parent  9990f1f2e75ade4d47ec2c21398d6b4182eeab53
Bug 1336364 P6 Ensure that we don't control a document if its window cannot access storage. r=asuth

diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -9164,16 +9164,30 @@ nsContentUtils::StorageAllowedForWindow(
     return InternalStorageAllowedForPrincipal(principal, aWindow);
   }
 
   return StorageAccess::eDeny;
 }
 
 // static, public
 nsContentUtils::StorageAccess
+nsContentUtils::StorageAllowedForDocument(nsIDocument* aDoc)
+{
+  MOZ_ASSERT(aDoc);
+
+  if (nsPIDOMWindowInner* inner = aDoc->GetInnerWindow()) {
+    nsCOMPtr<nsIPrincipal> principal = aDoc->NodePrincipal();
+    return InternalStorageAllowedForPrincipal(principal, inner);
+  }
+
+  return StorageAccess::eDeny;
+}
+
+// static, public
+nsContentUtils::StorageAccess
 nsContentUtils::StorageAllowedForPrincipal(nsIPrincipal* aPrincipal)
 {
   return InternalStorageAllowedForPrincipal(aPrincipal, nullptr);
 }
 
 // static, private
 void
 nsContentUtils::GetCookieBehaviorForPrincipal(nsIPrincipal* aPrincipal,
@@ -9241,17 +9255,17 @@ nsContentUtils::InternalStorageAllowedFo
   // calling context is chrome.
   if (aPrincipal->GetIsNullPrincipal()) {
     return StorageAccess::eDeny;
   }
 
   if (aWindow) {
     // If the document is sandboxed, then it is not permitted to use storage
     nsIDocument* document = aWindow->GetExtantDoc();
-    if (document->GetSandboxFlags() & SANDBOXED_ORIGIN) {
+    if (document && document->GetSandboxFlags() & SANDBOXED_ORIGIN) {
       return StorageAccess::eDeny;
     }
 
     // Check if we are in private browsing, and record that fact
     if (IsInPrivateBrowsing(document)) {
       access = StorageAccess::ePrivateBrowsing;
     }
   }
diff --git a/dom/base/nsContentUtils.h b/dom/base/nsContentUtils.h
--- a/dom/base/nsContentUtils.h
+++ b/dom/base/nsContentUtils.h
@@ -2953,16 +2953,27 @@ public:
    *
    * This logic is intended to be shared between the different forms of
    * persistent storage which are available to web pages. Cookies don't use
    * this logic, and security logic related to them must be updated separately.
    */
   static StorageAccess StorageAllowedForWindow(nsPIDOMWindowInner* aWindow);
 
   /*
+   * Checks if storage for the given document is permitted by a combination of
+   * the user's preferences, and whether the document's window is a third-party
+   * iframe.
+   *
+   * Note, this may be used on documents during the loading process where
+   * the window's extant document has not been set yet.  The code in
+   * StorageAllowedForWindow(), however, will not work in these cases.
+   */
+  static StorageAccess StorageAllowedForDocument(nsIDocument* aDoc);
+
+  /*
    * Checks if storage for the given principal is permitted by the user's
    * preferences. The caller is assumed to not be a third-party iframe.
    * (if that is possible, the caller should use StorageAllowedForWindow)
    */
   static StorageAccess StorageAllowedForPrincipal(nsIPrincipal* aPrincipal);
 
   /*
    * Serializes a HTML nsINode into its markup representation.
diff --git a/dom/workers/ServiceWorkerManager.cpp b/dom/workers/ServiceWorkerManager.cpp
--- a/dom/workers/ServiceWorkerManager.cpp
+++ b/dom/workers/ServiceWorkerManager.cpp
@@ -2196,17 +2196,25 @@ ServiceWorkerManager::GetServiceWorkerRe
 }
 
 already_AddRefed<ServiceWorkerRegistrationInfo>
 ServiceWorkerManager::GetServiceWorkerRegistrationInfo(nsIDocument* aDoc)
 {
   MOZ_ASSERT(aDoc);
   nsCOMPtr<nsIURI> documentURI = aDoc->GetDocumentURI();
   nsCOMPtr<nsIPrincipal> principal = aDoc->NodePrincipal();
-  return GetServiceWorkerRegistrationInfo(principal, documentURI);
+  RefPtr<ServiceWorkerRegistrationInfo> reg =
+    GetServiceWorkerRegistrationInfo(principal, documentURI);
+  if (reg) {
+    auto storageAllowed = nsContentUtils::StorageAllowedForDocument(aDoc);
+    if (storageAllowed != nsContentUtils::StorageAccess::eAllow) {
+      reg = nullptr;
+    }
+  }
+  return reg.forget();
 }
 
 already_AddRefed<ServiceWorkerRegistrationInfo>
 ServiceWorkerManager::GetServiceWorkerRegistrationInfo(nsIPrincipal* aPrincipal,
                                                        nsIURI* aURI)
 {
   MOZ_ASSERT(aPrincipal);
   MOZ_ASSERT(aURI);
@@ -2489,16 +2497,21 @@ ServiceWorkerManager::MaybeCheckNavigati
 void
 ServiceWorkerManager::StartControllingADocument(ServiceWorkerRegistrationInfo* aRegistration,
                                                 nsIDocument* aDoc,
                                                 const nsAString& aDocumentId)
 {
   MOZ_ASSERT(aRegistration);
   MOZ_ASSERT(aDoc);
 
+#ifdef MOZ_DIAGNOSTIC_ASSERT_ENABLED
+  auto storageAllowed = nsContentUtils::StorageAllowedForDocument(aDoc);
+  MOZ_DIAGNOSTIC_ASSERT(storageAllowed == nsContentUtils::StorageAccess::eAllow);
+#endif // MOZ_DIAGNOSTIC_ASSERT_ENABLED
+
   aRegistration->StartControllingADocument();
   mControlledDocuments.Put(aDoc, aRegistration);
   if (!aDocumentId.IsEmpty()) {
     aDoc->SetId(aDocumentId);
   }
   Telemetry::Accumulate(Telemetry::SERVICE_WORKER_CONTROLLED_DOCUMENTS, 1);
 }
 
diff --git a/dom/workers/test/serviceworkers/test_third_party_iframes.html b/dom/workers/test/serviceworkers/test_third_party_iframes.html
--- a/dom/workers/test/serviceworkers/test_third_party_iframes.html
+++ b/dom/workers/test/serviceworkers/test_third_party_iframes.html
@@ -98,16 +98,18 @@ function testShouldIntercept(policy, don
       status: "networkresponse",
       next: loadThirdPartyIframe
     }, {
       status: "swresponse",
       next: function() {
         iframe.src = thirdPartyOrigin + basePath + "unregister.html";
       }
     }, {
+      status: "controlled",
+    }, {
       status: "unregistrationdone",
       next: function() {
         window.onmessage = null;
         ok(true, "Test finished successfully");
         done();
       }
     }]);
   });
@@ -162,25 +164,29 @@ function testShouldNotIntercept(policy, 
       status: "networkresponse",
       next: loadThirdPartyIframe
     }, {
       status: "networkresponse",
       next: function() {
         iframe.src = thirdPartyOrigin + basePath + "unregister.html";
       }
     }, {
+      status: "uncontrolled",
+    }, {
       status: "getregistrationfailed",
       next: function() {
         SpecialPowers.pushPrefEnv({"set": [
             ["network.cookie.cookieBehavior", COOKIE_BEHAVIOR_ACCEPT],
           ]}, function() {
             iframe.src = thirdPartyOrigin + basePath + "unregister.html";
           });
       }
     }, {
+      status: "controlled",
+    }, {
       status: "unregistrationdone",
       next: function() {
         window.onmessage = null;
         ok(true, "Test finished successfully");
         done();
       }
     }]);
   });
diff --git a/dom/workers/test/serviceworkers/thirdparty/unregister.html b/dom/workers/test/serviceworkers/thirdparty/unregister.html
--- a/dom/workers/test/serviceworkers/thirdparty/unregister.html
+++ b/dom/workers/test/serviceworkers/thirdparty/unregister.html
@@ -1,10 +1,16 @@
 <!DOCTYPE html>
 <script>
+  if (navigator.serviceWorker.controller) {
+    window.parent.postMessage({status: "controlled"}, "*");
+  } else {
+    window.parent.postMessage({status: "uncontrolled"}, "*");
+  }
+
   navigator.serviceWorker.getRegistration(".").then(function(registration) {
     if(!registration) {
       return;
     }
     registration.unregister().then(() => {
       window.parent.postMessage({status: "unregistrationdone"}, "*");
     });
   }).catch(function(e) {
