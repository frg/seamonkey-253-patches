# HG changeset patch
# User Chris Pearce <cpearce@mozilla.com>
# Date 1502776337 -43200
# Node ID 557168f0da997fd84ff16d02f18c2c5ffbdba5b4
# Parent  07558ee96cd1147657a983022d5ef0854acc2495
Bug 1390748 - Remove ChannelMediaDecoder::CloneImpl() and remove subclasses of ChannelMediaDecoder. r=jwwang

MozReview-Commit-ID: 6nlBArYgwEJ

diff --git a/dom/media/ADTSDecoder.cpp b/dom/media/ADTSDecoder.cpp
--- a/dom/media/ADTSDecoder.cpp
+++ b/dom/media/ADTSDecoder.cpp
@@ -6,25 +6,16 @@
 
 #include "ADTSDecoder.h"
 #include "ADTSDemuxer.h"
 #include "MediaContainerType.h"
 #include "PDMFactory.h"
 
 namespace mozilla {
 
-ChannelMediaDecoder*
-ADTSDecoder::CloneImpl(MediaDecoderInit& aInit)
-{
-  if (!IsEnabled())
-    return nullptr;
-
-  return new ADTSDecoder(aInit);
-}
-
 /* static */ bool
 ADTSDecoder::IsEnabled()
 {
   RefPtr<PDMFactory> platform = new PDMFactory();
   return platform->SupportsMimeType(NS_LITERAL_CSTRING("audio/mp4a-latm"),
                                     /* DecoderDoctorDiagnostics* */ nullptr);
 }
 
diff --git a/dom/media/ADTSDecoder.h b/dom/media/ADTSDecoder.h
--- a/dom/media/ADTSDecoder.h
+++ b/dom/media/ADTSDecoder.h
@@ -8,29 +8,20 @@
 #define ADTS_DECODER_H_
 
 #include "ChannelMediaDecoder.h"
 
 namespace mozilla {
 
 class MediaContainerType;
 
-class ADTSDecoder : public ChannelMediaDecoder
+class ADTSDecoder
 {
 public:
-  // MediaDecoder interface.
-  explicit ADTSDecoder(MediaDecoderInit& aInit)
-    : ChannelMediaDecoder(aInit)
-  {
-  }
-
   // Returns true if the ADTS backend is pref'ed on, and we're running on a
   // platform that is likely to have decoders for the format.
   static bool IsEnabled();
   static bool IsSupportedType(const MediaContainerType& aContainerType);
-
-private:
-  ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) override;
 };
 
 } // namespace mozilla
 
 #endif // !ADTS_DECODER_H_
diff --git a/dom/media/ChannelMediaDecoder.cpp b/dom/media/ChannelMediaDecoder.cpp
--- a/dom/media/ChannelMediaDecoder.cpp
+++ b/dom/media/ChannelMediaDecoder.cpp
@@ -169,20 +169,20 @@ ChannelMediaDecoder::CanClone()
 {
   MOZ_ASSERT(NS_IsMainThread());
   return mResource && mResource->CanClone();
 }
 
 already_AddRefed<ChannelMediaDecoder>
 ChannelMediaDecoder::Clone(MediaDecoderInit& aInit)
 {
-  if (!mResource) {
+  if (!mResource || !DecoderTraits::IsSupportedType(aInit.mContainerType)) {
     return nullptr;
   }
-  RefPtr<ChannelMediaDecoder> decoder = CloneImpl(aInit);
+  RefPtr<ChannelMediaDecoder> decoder = new ChannelMediaDecoder(aInit);
   if (!decoder) {
     return nullptr;
   }
   nsresult rv = decoder->Load(mResource);
   if (NS_FAILED(rv)) {
     decoder->Shutdown();
     return nullptr;
   }
diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -79,17 +79,16 @@ public:
   void SetLoadInBackground(bool aLoadInBackground) override;
   void Suspend() override;
   void Resume() override;
 
 private:
   // Create a new state machine to run this decoder.
   MediaDecoderStateMachine* CreateStateMachine();
 
-  virtual ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) = 0;
   nsresult OpenResource(nsIStreamListener** aStreamListener);
   nsresult Load(BaseMediaResource* aOriginal);
 
   // Called by MediaResource when the download has ended.
   // Called on the main thread only. aStatus is the result from OnStopRequest.
   void NotifyDownloadEnded(nsresult aStatus);
 
   // Called by the MediaResource to keep track of the number of bytes read
diff --git a/dom/media/DecoderTraits.cpp b/dom/media/DecoderTraits.cpp
--- a/dom/media/DecoderTraits.cpp
+++ b/dom/media/DecoderTraits.cpp
@@ -228,46 +228,18 @@ bool DecoderTraits::ShouldHandleMediaTyp
 static already_AddRefed<ChannelMediaDecoder>
 InstantiateDecoder(MediaDecoderInit& aInit,
                    DecoderDoctorDiagnostics* aDiagnostics)
 {
   MOZ_ASSERT(NS_IsMainThread());
   RefPtr<ChannelMediaDecoder> decoder;
 
   const MediaContainerType& type = aInit.mContainerType;
-
-#ifdef MOZ_FMP4
-  if (MP4Decoder::IsSupportedType(type, aDiagnostics)) {
-    decoder = new MP4Decoder(aInit);
-    return decoder.forget();
-  }
-#endif
-  if (MP3Decoder::IsSupportedType(type)) {
-    decoder = new MP3Decoder(aInit);
-    return decoder.forget();
-  }
-  if (ADTSDecoder::IsSupportedType(type)) {
-    decoder = new ADTSDecoder(aInit);
-    return decoder.forget();
-  }
-  if (OggDecoder::IsSupportedType(type)) {
-    decoder = new OggDecoder(aInit);
-    return decoder.forget();
-  }
-  if (WaveDecoder::IsSupportedType(type)) {
-    decoder = new WaveDecoder(aInit);
-    return decoder.forget();
-  }
-  if (FlacDecoder::IsSupportedType(type)) {
-    decoder = new FlacDecoder(aInit);
-    return decoder.forget();
-  }
-
-  if (WebMDecoder::IsSupportedType(type)) {
-    decoder = new WebMDecoder(aInit);
+  if (DecoderTraits::IsSupportedType(type)) {
+    decoder = new ChannelMediaDecoder(aInit);
     return decoder.forget();
   }
 
   if (DecoderTraits::IsHttpLiveStreamingType(type)) {
     // We don't have an HLS decoder.
     Telemetry::Accumulate(Telemetry::MEDIA_HLS_DECODER_SUCCESS, false);
   }
 
diff --git a/dom/media/flac/FlacDecoder.cpp b/dom/media/flac/FlacDecoder.cpp
--- a/dom/media/flac/FlacDecoder.cpp
+++ b/dom/media/flac/FlacDecoder.cpp
@@ -6,26 +6,16 @@
 
 #include "FlacDecoder.h"
 #include "FlacDemuxer.h"
 #include "MediaContainerType.h"
 #include "MediaPrefs.h"
 
 namespace mozilla {
 
-ChannelMediaDecoder*
-FlacDecoder::CloneImpl(MediaDecoderInit& aInit)
-{
-  if (!IsEnabled()) {
-    return nullptr;
-  }
-
-  return new FlacDecoder(aInit);
-}
-
 /* static */ bool
 FlacDecoder::IsEnabled()
 {
 #ifdef MOZ_FFVPX
   return MediaPrefs::FlacEnabled();
 #else
   // Until bug 1295886 is fixed.
   return false;
diff --git a/dom/media/flac/FlacDecoder.h b/dom/media/flac/FlacDecoder.h
--- a/dom/media/flac/FlacDecoder.h
+++ b/dom/media/flac/FlacDecoder.h
@@ -8,29 +8,20 @@
 #define FLAC_DECODER_H_
 
 #include "ChannelMediaDecoder.h"
 
 namespace mozilla {
 
 class MediaContainerType;
 
-class FlacDecoder : public ChannelMediaDecoder
+class FlacDecoder
 {
 public:
-  // MediaDecoder interface.
-  explicit FlacDecoder(MediaDecoderInit& aInit)
-    : ChannelMediaDecoder(aInit)
-  {
-  }
-
   // Returns true if the Flac backend is pref'ed on, and we're running on a
   // platform that is likely to have decoders for the format.
   static bool IsEnabled();
   static bool IsSupportedType(const MediaContainerType& aContainerType);
-
-private:
-  ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) override;
 };
 
 } // namespace mozilla
 
 #endif // !FLAC_DECODER_H_
diff --git a/dom/media/fmp4/MP4Decoder.cpp b/dom/media/fmp4/MP4Decoder.cpp
--- a/dom/media/fmp4/MP4Decoder.cpp
+++ b/dom/media/fmp4/MP4Decoder.cpp
@@ -20,21 +20,16 @@
 #include "nsIGfxInfo.h"
 #endif
 #include "mozilla/layers/LayersTypes.h"
 
 #include "PDMFactory.h"
 
 namespace mozilla {
 
-MP4Decoder::MP4Decoder(MediaDecoderInit& aInit)
-  : ChannelMediaDecoder(aInit)
-{
-}
-
 static bool
 IsWhitelistedH264Codec(const nsAString& aCodec)
 {
   int16_t profile = 0, level = 0;
 
   if (!ExtractH264CodecDetails(aCodec, profile, level)) {
     return false;
   }
diff --git a/dom/media/fmp4/MP4Decoder.h b/dom/media/fmp4/MP4Decoder.h
--- a/dom/media/fmp4/MP4Decoder.h
+++ b/dom/media/fmp4/MP4Decoder.h
@@ -11,21 +11,19 @@
 #include "mozilla/layers/KnowsCompositor.h"
 #include "nsStringFwd.h"
 
 namespace mozilla {
 
 class MediaContainerType;
 
 // Decoder that uses a bundled MP4 demuxer and platform decoders to play MP4.
-class MP4Decoder : public ChannelMediaDecoder
+class MP4Decoder
 {
 public:
-  explicit MP4Decoder(MediaDecoderInit& aInit);
-
   // Returns true if aContainerType is an MP4 type that we think we can render
   // with the a platform decoder backend.
   // If provided, codecs are checked for support.
   static bool IsSupportedType(const MediaContainerType& aContainerType,
                               DecoderDoctorDiagnostics* aDiagnostics);
 
   static bool IsSupportedTypeWithoutDiagnostics(
     const MediaContainerType& aContainerType);
@@ -41,21 +39,13 @@ public:
   static bool IsAAC(const nsACString& aMimeType);
 
   // Returns true if the MP4 backend is preffed on.
   static bool IsEnabled();
 
   static already_AddRefed<dom::Promise>
   IsVideoAccelerated(layers::KnowsCompositor* aKnowsCompositor, nsIGlobalObject* aParent);
 
-private:
-  ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) override
-  {
-    if (!IsEnabled()) {
-      return nullptr;
-    }
-    return new MP4Decoder(aInit);
-  }
 };
 
 } // namespace mozilla
 
 #endif
diff --git a/dom/media/hls/HLSDecoder.h b/dom/media/hls/HLSDecoder.h
--- a/dom/media/hls/HLSDecoder.h
+++ b/dom/media/hls/HLSDecoder.h
@@ -20,18 +20,16 @@ public:
     : MediaDecoder(aInit)
   {
   }
 
   MediaResource* GetResource() const override final;
 
   void Shutdown() override;
 
-  MediaDecoderStateMachine* CreateStateMachine() override;
-
   // Returns true if the HLS backend is pref'ed on.
   static bool IsEnabled();
 
   // Returns true if aContainerType is an HLS type that we think we can render
   // with the a platform decoder backend.
   // If provided, codecs are checked for support.
   static bool IsSupportedType(const MediaContainerType& aContainerType);
 
@@ -40,16 +38,18 @@ public:
   nsresult Play() override;
 
   void Pause() override;
 
   void Suspend() override;
   void Resume() override;
 
 private:
+  MediaDecoderStateMachine* CreateStateMachine();
+
   bool CanPlayThroughImpl() override final
   {
     // TODO: We don't know how to estimate 'canplaythrough' for this decoder.
     // For now we just return true for 'autoplay' can work.
     return true;
   }
 
   RefPtr<HLSResource> mResource;
diff --git a/dom/media/mp3/MP3Decoder.cpp b/dom/media/mp3/MP3Decoder.cpp
--- a/dom/media/mp3/MP3Decoder.cpp
+++ b/dom/media/mp3/MP3Decoder.cpp
@@ -8,25 +8,16 @@
 #include "MP3Decoder.h"
 
 #include "MediaContainerType.h"
 #include "MP3Demuxer.h"
 #include "PDMFactory.h"
 
 namespace mozilla {
 
-ChannelMediaDecoder*
-MP3Decoder::CloneImpl(MediaDecoderInit& aInit)
-{
-  if (!IsEnabled()) {
-    return nullptr;
-  }
-  return new MP3Decoder(aInit);
-}
-
 /* static */
 bool
 MP3Decoder::IsEnabled() {
   RefPtr<PDMFactory> platform = new PDMFactory();
   return platform->SupportsMimeType(NS_LITERAL_CSTRING("audio/mpeg"),
                                     /* DecoderDoctorDiagnostics* */ nullptr);
 }
 
diff --git a/dom/media/mp3/MP3Decoder.h b/dom/media/mp3/MP3Decoder.h
--- a/dom/media/mp3/MP3Decoder.h
+++ b/dom/media/mp3/MP3Decoder.h
@@ -7,29 +7,20 @@
 #define MP3Decoder_h_
 
 #include "ChannelMediaDecoder.h"
 
 namespace mozilla {
 
 class MediaContainerType;
 
-class MP3Decoder : public ChannelMediaDecoder
+class MP3Decoder
 {
 public:
-  // MediaDecoder interface.
-  explicit MP3Decoder(MediaDecoderInit& aInit)
-    : ChannelMediaDecoder(aInit)
-  {
-  }
-
   // Returns true if the MP3 backend is preffed on, and we're running on a
   // platform that is likely to have decoders for the format.
   static bool IsEnabled();
   static bool IsSupportedType(const MediaContainerType& aContainerType);
-
-private:
-  ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) override;
 };
 
 } // namespace mozilla
 
 #endif
diff --git a/dom/media/ogg/OggDecoder.cpp b/dom/media/ogg/OggDecoder.cpp
--- a/dom/media/ogg/OggDecoder.cpp
+++ b/dom/media/ogg/OggDecoder.cpp
@@ -32,17 +32,17 @@ OggDecoder::IsSupportedType(const MediaC
   const MediaCodecs& codecs = aContainerType.ExtendedType().Codecs();
   if (codecs.IsEmpty()) {
     // WebM guarantees that the only codecs it contained are vp8, vp9, opus or vorbis.
     return true;
   }
   // Verify that all the codecs specified are ones that we expect that
   // we can play.
   for (const auto& codec : codecs.Range()) {
-    if ((IsOpusEnabled() && codec.EqualsLiteral("opus")) ||
+    if ((MediaDecoder::IsOpusEnabled() && codec.EqualsLiteral("opus")) ||
         codec.EqualsLiteral("vorbis") ||
         (MediaPrefs::FlacInOgg() && codec.EqualsLiteral("flac"))) {
       continue;
     }
     // Note: Only accept Theora in a video container type, not in an audio
     // container type.
     if (isOggVideo && codec.EqualsLiteral("theora")) {
       continue;
diff --git a/dom/media/ogg/OggDecoder.h b/dom/media/ogg/OggDecoder.h
--- a/dom/media/ogg/OggDecoder.h
+++ b/dom/media/ogg/OggDecoder.h
@@ -7,33 +7,20 @@
 #define OggDecoder_h_
 
 #include "ChannelMediaDecoder.h"
 
 namespace mozilla {
 
 class MediaContainerType;
 
-class OggDecoder : public ChannelMediaDecoder
+class OggDecoder
 {
 public:
-  explicit OggDecoder(MediaDecoderInit& aInit)
-    : ChannelMediaDecoder(aInit)
-  {}
-
   // Returns true if aContainerType is an Ogg type that we think we can render
   // with an enabled platform decoder backend.
   // If provided, codecs are checked for support.
   static bool IsSupportedType(const MediaContainerType& aContainerType);
-
-private:
-  ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) override
-  {
-    if (!IsOggEnabled()) {
-      return nullptr;
-    }
-    return new OggDecoder(aInit);
-  }
 };
 
 } // namespace mozilla
 
 #endif
diff --git a/dom/media/wave/WaveDecoder.cpp b/dom/media/wave/WaveDecoder.cpp
--- a/dom/media/wave/WaveDecoder.cpp
+++ b/dom/media/wave/WaveDecoder.cpp
@@ -6,26 +6,20 @@
 
 #include "WaveDemuxer.h"
 #include "MediaContainerType.h"
 #include "WaveDecoder.h"
 #include "PDMFactory.h"
 
 namespace mozilla {
 
-ChannelMediaDecoder*
-WaveDecoder::CloneImpl(MediaDecoderInit& aInit)
-{
-  return new WaveDecoder(aInit);
-}
-
 /* static */ bool
 WaveDecoder::IsSupportedType(const MediaContainerType& aContainerType)
 {
-  if (!IsWaveEnabled()) {
+  if (!MediaDecoder::IsWaveEnabled()) {
     return false;
   }
   if (aContainerType.Type() == MEDIAMIMETYPE("audio/wave")
       || aContainerType.Type() == MEDIAMIMETYPE("audio/x-wav")
       || aContainerType.Type() == MEDIAMIMETYPE("audio/wav")
       || aContainerType.Type() == MEDIAMIMETYPE("audio/x-pn-wav")) {
     return (aContainerType.ExtendedType().Codecs().IsEmpty()
             || aContainerType.ExtendedType().Codecs() == "1"
diff --git a/dom/media/wave/WaveDecoder.h b/dom/media/wave/WaveDecoder.h
--- a/dom/media/wave/WaveDecoder.h
+++ b/dom/media/wave/WaveDecoder.h
@@ -7,28 +7,19 @@
 #define WaveDecoder_h_
 
 #include "ChannelMediaDecoder.h"
 
 namespace mozilla {
 
 class MediaContainerType;
 
-class WaveDecoder : public ChannelMediaDecoder
+class WaveDecoder
 {
 public:
-  // MediaDecoder interface.
-  explicit WaveDecoder(MediaDecoderInit& aInit)
-    : ChannelMediaDecoder(aInit)
-  {
-  }
-
   // Returns true if the Wave backend is pref'ed on, and we're running on a
   // platform that is likely to have decoders for the format.
   static bool IsSupportedType(const MediaContainerType& aContainerType);
-
-private:
-  ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) override;
 };
 
 } // namespace mozilla
 
 #endif
diff --git a/dom/media/webm/WebMDecoder.h b/dom/media/webm/WebMDecoder.h
--- a/dom/media/webm/WebMDecoder.h
+++ b/dom/media/webm/WebMDecoder.h
@@ -7,34 +7,22 @@
 #define WebMDecoder_h_
 
 #include "ChannelMediaDecoder.h"
 
 namespace mozilla {
 
 class MediaContainerType;
 
-class WebMDecoder : public ChannelMediaDecoder
+class WebMDecoder
 {
 public:
-  explicit WebMDecoder(MediaDecoderInit& aInit)
-    : ChannelMediaDecoder(aInit)
-  {
-  }
 
   // Returns true if aContainerType is a WebM type that we think we can render
   // with an enabled platform decoder backend.
   // If provided, codecs are checked for support.
   static bool IsSupportedType(const MediaContainerType& aContainerType);
 
-private:
-  ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) override
-  {
-    if (!IsWebMEnabled()) {
-      return nullptr;
-    }
-    return new WebMDecoder(aInit);
-  }
 };
 
 } // namespace mozilla
 
 #endif
