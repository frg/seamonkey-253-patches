# HG changeset patch
# User Aryeh Gregor <ayg@aryeh.name>
# Date 1502289546 -10800
#      Wed Aug 09 17:39:06 2017 +0300
# Node ID d55ad0a1396d142848dc0e79c0eb015aa29e14ff
# Parent  a18386d46d967269207aa01a3ffe40611d37de4a
Bug 1388746 - Do not split start/end text nodes in deleteContents/extractContents; r=smaug

If a range endpoint is in the middle of a text node, and you call
deleteContents() or extractContents(), the spec says to delete the data
from the node.  In the case of extractContents(), the new text node
that's inserted into the DocumentFragment is a clone with its data set
to the bit that was deleted.
<https://dom.spec.whatwg.org/#dom-range-deletecontents>
<https://dom.spec.whatwg.org/#dom-range-extractcontents>

We don't do this.  Instead, we split the text node.  Then the bit to
delete is deleted naturally at a later stage together with all the other
nodes.

The result is the same, but on the way there we do a bunch more node
mutations.  This causes extra mutation records, which cause us to fail a
WPT test.  Chrome passes.  Changing to match the spec actually reduces
our lines of code anyway.

MozReview-Commit-ID: FTTV5yNSj71

diff --git a/dom/base/nsRange.cpp b/dom/base/nsRange.cpp
--- a/dom/base/nsRange.cpp
+++ b/dom/base/nsRange.cpp
@@ -2053,42 +2053,16 @@ CollapseRangeAfterDelete(nsRange* aRange
     return NS_ERROR_FAILURE; // This should never happen!
 
   aRange->SelectNode(*nodeToSelect, rv);
   if (rv.Failed()) return rv.StealNSResult();
 
   return aRange->Collapse(false);
 }
 
-/**
- * Split a data node into two parts.
- *
- * @param aStartContainer     The original node we are trying to split.
- * @param aStartOffset        The offset at which to split.
- * @param aEndContainer       The second node.
- * @param aCloneAfterOriginal Set false if the original node should be the
- *                            latter one after split.
- */
-static nsresult SplitDataNode(nsIDOMCharacterData* aStartContainer,
-                              uint32_t aStartOffset,
-                              nsIDOMCharacterData** aEndContainer,
-                              bool aCloneAfterOriginal = true)
-{
-  nsresult rv;
-  nsCOMPtr<nsINode> node = do_QueryInterface(aStartContainer);
-  NS_ENSURE_STATE(node && node->IsNodeOfType(nsINode::eDATA_NODE));
-  nsGenericDOMDataNode* dataNode = static_cast<nsGenericDOMDataNode*>(node.get());
-
-  nsCOMPtr<nsIContent> newData;
-  rv = dataNode->SplitData(aStartOffset, getter_AddRefs(newData),
-                           aCloneAfterOriginal);
-  NS_ENSURE_SUCCESS(rv, rv);
-  return CallQueryInterface(newData, aEndContainer);
-}
-
 NS_IMETHODIMP
 PrependChild(nsINode* aContainer, nsINode* aChild)
 {
   nsCOMPtr<nsINode> first = aContainer->GetFirstChild();
   ErrorResult rv;
   aContainer->InsertBefore(*aChild, first, rv);
   return rv.StealNSResult();
 }
@@ -2262,42 +2236,56 @@ nsRange::CutContents(DocumentFragment** 
         else
         {
           // Delete or extract everything after startOffset.
 
           rv = charData->GetLength(&dataLength);
           NS_ENSURE_SUCCESS(rv, rv);
 
           if (dataLength >= startOffset) {
+            if (retval) {
+              nsAutoString cutValue;
+              rv = charData->SubstringData(startOffset, dataLength, cutValue);
+              NS_ENSURE_SUCCESS(rv, rv);
+              nsCOMPtr<nsIDOMNode> clone;
+              rv = charData->CloneNode(false, 1, getter_AddRefs(clone));
+              NS_ENSURE_SUCCESS(rv, rv);
+              clone->SetNodeValue(cutValue);
+              nodeToResult = do_QueryInterface(clone);
+            }
+
             nsMutationGuard guard;
-            nsCOMPtr<nsIDOMCharacterData> cutNode;
-            rv = SplitDataNode(charData, startOffset, getter_AddRefs(cutNode));
+            rv = charData->DeleteData(startOffset, dataLength);
             NS_ENSURE_SUCCESS(rv, rv);
-            NS_ENSURE_STATE(!guard.Mutated(1) ||
+            NS_ENSURE_STATE(!guard.Mutated(0) ||
                             ValidateCurrentNode(this, iter));
-            nodeToResult = do_QueryInterface(cutNode);
           }
 
           handled = true;
         }
       }
       else if (node == endContainer)
       {
         // Delete or extract everything before endOffset.
+        if (retval) {
+          nsAutoString cutValue;
+          rv = charData->SubstringData(0, endOffset, cutValue);
+          NS_ENSURE_SUCCESS(rv, rv);
+          nsCOMPtr<nsIDOMNode> clone;
+          rv = charData->CloneNode(false, 1, getter_AddRefs(clone));
+          NS_ENSURE_SUCCESS(rv, rv);
+          clone->SetNodeValue(cutValue);
+          nodeToResult = do_QueryInterface(clone);
+        }
+
         nsMutationGuard guard;
-        nsCOMPtr<nsIDOMCharacterData> cutNode;
-        /* The Range spec clearly states clones get cut and original nodes
-           remain behind, so use false as the last parameter.
-        */
-        rv = SplitDataNode(charData, endOffset, getter_AddRefs(cutNode),
-                           false);
+        rv = charData->DeleteData(0, endOffset);
         NS_ENSURE_SUCCESS(rv, rv);
-        NS_ENSURE_STATE(!guard.Mutated(1) ||
+        NS_ENSURE_STATE(!guard.Mutated(0) ||
                         ValidateCurrentNode(this, iter));
-        nodeToResult = do_QueryInterface(cutNode);
         handled = true;
       }
     }
 
     if (!handled && (node == endContainer || node == startContainer))
     {
       if (node && node->IsElement() &&
           ((node == endContainer && endOffset == 0) ||
diff --git a/testing/web-platform/meta/dom/nodes/MutationObserver-childList.html.ini b/testing/web-platform/meta/dom/nodes/MutationObserver-childList.html.ini
--- a/testing/web-platform/meta/dom/nodes/MutationObserver-childList.html.ini
+++ b/testing/web-platform/meta/dom/nodes/MutationObserver-childList.html.ini
@@ -1,11 +1,5 @@
 [MutationObserver-childList.html]
   type: testharness
-  [childList Range.deleteContents: child and data removal mutation]
-    expected: FAIL
-
-  [childList Range.extractContents: child and data removal mutation]
-    expected: FAIL
-
   [childList Range.surroundContents: children removal and addition mutation]
     expected: FAIL
 
