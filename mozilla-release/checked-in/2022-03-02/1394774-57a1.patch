# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1504009340 -7200
#      Tue Aug 29 14:22:20 2017 +0200
# Node ID 053e0c2c51ec52285119624ad471a0dbaf6e2f85
# Parent  2f49314a19e376087686d564b1d4d797e129a6ff
Bug 1394774 - wasm, read u8 fields as u8, not as varu32. r=luke

diff --git a/js/src/wasm/WasmBinaryIterator.h b/js/src/wasm/WasmBinaryIterator.h
--- a/js/src/wasm/WasmBinaryIterator.h
+++ b/js/src/wasm/WasmBinaryIterator.h
@@ -1282,40 +1282,40 @@ template <typename Policy>
 inline bool
 OpIter<Policy>::readCurrentMemory()
 {
     MOZ_ASSERT(Classify(op_) == OpKind::CurrentMemory);
 
     if (!env_.usesMemory())
         return fail("can't touch memory without memory");
 
-    uint32_t flags;
-    if (!readVarU32(&flags))
+    uint8_t flags;
+    if (!readFixedU8(&flags))
         return false;
 
-    if (flags != uint32_t(MemoryTableFlags::Default))
+    if (flags != uint8_t(MemoryTableFlags::Default))
         return fail("unexpected flags");
 
     return push(ValType::I32);
 }
 
 template <typename Policy>
 inline bool
 OpIter<Policy>::readGrowMemory(Value* input)
 {
     MOZ_ASSERT(Classify(op_) == OpKind::GrowMemory);
 
     if (!env_.usesMemory())
         return fail("can't touch memory without memory");
 
-    uint32_t flags;
-    if (!readVarU32(&flags))
+    uint8_t flags;
+    if (!readFixedU8(&flags))
         return false;
 
-    if (flags != uint32_t(MemoryTableFlags::Default))
+    if (flags != uint8_t(MemoryTableFlags::Default))
         return fail("unexpected flags");
 
     if (!popWithType(ValType::I32, input))
         return false;
 
     infalliblePush(ValType::I32);
 
     return true;
@@ -1599,21 +1599,21 @@ OpIter<Policy>::readCallIndirect(uint32_
         return fail("can't call_indirect without a table");
 
     if (!readVarU32(sigIndex))
         return fail("unable to read call_indirect signature index");
 
     if (*sigIndex >= env_.numSigs())
         return fail("signature index out of range");
 
-    uint32_t flags;
-    if (!readVarU32(&flags))
+    uint8_t flags;
+    if (!readFixedU8(&flags))
         return false;
 
-    if (flags != uint32_t(MemoryTableFlags::Default))
+    if (flags != uint8_t(MemoryTableFlags::Default))
         return fail("unexpected flags");
 
     if (!popWithType(ValType::I32, callee))
         return false;
 
     const Sig& sig = env_.sigs[*sigIndex];
 
     if (!popCallArgs(sig.args(), argValues))
diff --git a/js/src/wasm/WasmValidate.cpp b/js/src/wasm/WasmValidate.cpp
--- a/js/src/wasm/WasmValidate.cpp
+++ b/js/src/wasm/WasmValidate.cpp
@@ -69,34 +69,34 @@ Decoder::startSection(SectionId id, Modu
     const size_t initialCustomSectionsLength = env->customSections.length();
 
     // Maintain a pointer to the current section that gets updated as custom
     // sections are skipped.
     const uint8_t* currentSectionStart = cur_;
 
     // Only start a section with 'id', skipping any custom sections before it.
 
-    uint32_t idValue;
-    if (!readVarU32(&idValue))
+    uint8_t idValue;
+    if (!readFixedU8(&idValue))
         goto rewind;
 
-    while (idValue != uint32_t(id)) {
-        if (idValue != uint32_t(SectionId::Custom))
+    while (idValue != uint8_t(id)) {
+        if (idValue != uint8_t(SectionId::Custom))
             goto rewind;
 
         // Rewind to the beginning of the current section since this is what
         // skipCustomSection() assumes.
         cur_ = currentSectionStart;
         if (!skipCustomSection(env))
             return false;
 
         // Having successfully skipped a custom section, consider the next
         // section.
         currentSectionStart = cur_;
-        if (!readVarU32(&idValue))
+        if (!readFixedU8(&idValue))
             goto rewind;
     }
 
     // Found it, now start the section.
 
     if (!readVarU32(sectionSize) || bytesRemain() < *sectionSize)
         goto fail;
 
@@ -202,18 +202,18 @@ Decoder::skipCustomSection(ModuleEnviron
     return true;
 }
 
 bool
 Decoder::startNameSubsection(NameType nameType, uint32_t* endOffset)
 {
     const uint8_t* initialPosition = cur_;
 
-    uint32_t nameTypeValue;
-    if (!readVarU32(&nameTypeValue))
+    uint8_t nameTypeValue;
+    if (!readFixedU8(&nameTypeValue))
         return false;
 
     if (nameTypeValue != uint8_t(nameType)) {
         cur_ = initialPosition;
         *endOffset = NotStarted;
         return true;
     }
 
@@ -755,18 +755,18 @@ DecodeTypeSection(Decoder& d, ModuleEnvi
 
     if (numSigs > MaxTypes)
         return d.fail("too many signatures");
 
     if (!env->sigs.resize(numSigs))
         return false;
 
     for (uint32_t sigIndex = 0; sigIndex < numSigs; sigIndex++) {
-        uint32_t form;
-        if (!d.readVarU32(&form) || form != uint32_t(TypeCode::Func))
+        uint8_t form;
+        if (!d.readFixedU8(&form) || form != uint8_t(TypeCode::Func))
             return d.fail("expected function form");
 
         uint32_t numArgs;
         if (!d.readVarU32(&numArgs))
             return d.fail("bad number of function args");
 
         if (numArgs > MaxParams)
             return d.fail("too many arguments in signature");
@@ -840,22 +840,22 @@ DecodeSignatureIndex(Decoder& d, const S
         return d.fail("signature index out of range");
 
     return true;
 }
 
 static bool
 DecodeLimits(Decoder& d, Limits* limits)
 {
-    uint32_t flags;
-    if (!d.readVarU32(&flags))
+    uint8_t flags;
+    if (!d.readFixedU8(&flags))
         return d.fail("expected flags");
 
-    if (flags & ~uint32_t(0x1))
-        return d.failf("unexpected bits set in flags: %" PRIu32, (flags & ~uint32_t(0x1)));
+    if (flags & ~uint8_t(0x1))
+        return d.failf("unexpected bits set in flags: %" PRIu32, (flags & ~uint8_t(0x1)));
 
     if (!d.readVarU32(&limits->initial))
         return d.fail("expected initial length");
 
     if (flags & 0x1) {
         uint32_t maximum;
         if (!d.readVarU32(&maximum))
             return d.fail("expected maximum length");
@@ -870,21 +870,21 @@ DecodeLimits(Decoder& d, Limits* limits)
     }
 
     return true;
 }
 
 static bool
 DecodeTableLimits(Decoder& d, TableDescVector* tables)
 {
-    uint32_t elementType;
-    if (!d.readVarU32(&elementType))
+    uint8_t elementType;
+    if (!d.readFixedU8(&elementType))
         return d.fail("expected table element type");
 
-    if (elementType != uint32_t(TypeCode::AnyFunc))
+    if (elementType != uint8_t(TypeCode::AnyFunc))
         return d.fail("expected 'anyfunc' element type");
 
     Limits limits;
     if (!DecodeLimits(d, &limits))
         return false;
 
     if (limits.initial > MaxTableInitialLength)
         return d.fail("too many table elements");
@@ -918,24 +918,24 @@ GlobalIsJSCompatible(Decoder& d, ValType
 }
 
 static bool
 DecodeGlobalType(Decoder& d, ValType* type, bool* isMutable)
 {
     if (!DecodeValType(d, ModuleKind::Wasm, type))
         return false;
 
-    uint32_t flags;
-    if (!d.readVarU32(&flags))
+    uint8_t flags;
+    if (!d.readFixedU8(&flags))
         return d.fail("expected global flags");
 
-    if (flags & ~uint32_t(GlobalTypeImmediate::AllowedMask))
+    if (flags & ~uint8_t(GlobalTypeImmediate::AllowedMask))
         return d.fail("unexpected bits set in global flags");
 
-    *isMutable = flags & uint32_t(GlobalTypeImmediate::IsMutable);
+    *isMutable = flags & uint8_t(GlobalTypeImmediate::IsMutable);
     return true;
 }
 
 static bool
 DecodeMemoryLimits(Decoder& d, ModuleEnvironment* env)
 {
     if (env->usesMemory())
         return d.fail("already have default memory");
@@ -976,18 +976,18 @@ DecodeImport(Decoder& d, ModuleEnvironme
     UniqueChars moduleName = DecodeName(d);
     if (!moduleName)
         return d.fail("expected valid import module name");
 
     UniqueChars funcName = DecodeName(d);
     if (!funcName)
         return d.fail("expected valid import func name");
 
-    uint32_t rawImportKind;
-    if (!d.readVarU32(&rawImportKind))
+    uint8_t rawImportKind;
+    if (!d.readFixedU8(&rawImportKind))
         return d.fail("failed to read import kind");
 
     DefinitionKind importKind = DefinitionKind(rawImportKind);
 
     switch (importKind) {
       case DefinitionKind::Function: {
         uint32_t sigIndex;
         if (!DecodeSignatureIndex(d, env->sigs, &sigIndex))
@@ -1276,18 +1276,18 @@ DecodeExportName(Decoder& d, CStringSet*
 
 static bool
 DecodeExport(Decoder& d, ModuleEnvironment* env, CStringSet* dupSet)
 {
     UniqueChars fieldName = DecodeExportName(d, dupSet);
     if (!fieldName)
         return false;
 
-    uint32_t exportKind;
-    if (!d.readVarU32(&exportKind))
+    uint8_t exportKind;
+    if (!d.readFixedU8(&exportKind))
         return d.fail("failed to read export kind");
 
     switch (DefinitionKind(exportKind)) {
       case DefinitionKind::Function: {
         uint32_t funcIndex;
         if (!d.readVarU32(&funcIndex))
             return d.fail("expected function index");
 
