# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1529522674 0
#      Wed Jun 20 19:24:34 2018 +0000
# Node ID 42232ab8fa6d453c4a30b2f245142646ca20e74f
# Parent  2bbea07a6d67f5854b10d736961cd9be003de065
Bug 1469257 - [H264] Only check for SPS changes on keyframe. r=bryce, a=RyanVM

Some invalid streams contain SPS changes and those appear to only occur on non-keyframe, this cause all frames to be dropped until the next keyframe is found. This result in apparent freezes.

While it is theoretically possible to have SPS changes inband on non-keyframe those should be very rare (I've never seen one). The content would have been invalid anyway in an non-fragmented mp4.

So we now only check for a SPS change on keyframe. This would cause no affect on either windows, android or ffmpeg as those decoders handle format change fine. The mac decoder could however show garbled frames temporarily.

Differential Revision: https://phabricator.services.mozilla.com/D1733

diff --git a/dom/media/platforms/wrappers/H264Converter.cpp b/dom/media/platforms/wrappers/H264Converter.cpp
--- a/dom/media/platforms/wrappers/H264Converter.cpp
+++ b/dom/media/platforms/wrappers/H264Converter.cpp
@@ -404,38 +404,36 @@ H264Converter::DecodeFirstSample(MediaRa
            })
     ->Track(mDecodePromiseRequest);
 }
 
 MediaResult
 H264Converter::CheckForSPSChange(MediaRawData* aSample)
 {
   RefPtr<MediaByteBuffer> extra_data =
-    H264::ExtractExtraData(aSample);
+    aSample->mKeyframe ? H264::ExtractExtraData(aSample) : nullptr;
   if (!H264::HasSPS(extra_data)) {
     MOZ_ASSERT(mCanRecycleDecoder.isSome());
     if (!*mCanRecycleDecoder) {
       // If the decoder can't be recycled, the out of band extradata will never
       // change as the H264Converter will be recreated by the MediaFormatReader
       // instead. So there's no point in testing for changes.
       return NS_OK;
     }
     // This sample doesn't contain inband SPS/PPS
     // We now check if the out of band one has changed.
     // This scenario can only occur on Android with devices that can recycle a
     // decoder.
     if (!H264::HasSPS(aSample->mExtraData) ||
-        H264::CompareExtraData(aSample->mExtraData,
-                                            mOriginalExtraData)) {
+        H264::CompareExtraData(aSample->mExtraData, mOriginalExtraData)) {
       return NS_OK;
     }
     extra_data = mOriginalExtraData = aSample->mExtraData;
   }
-  if (H264::CompareExtraData(extra_data,
-                                          mCurrentConfig.mExtraData)) {
+  if (H264::CompareExtraData(extra_data, mCurrentConfig.mExtraData)) {
     return NS_OK;
   }
 
   MOZ_ASSERT(mCanRecycleDecoder.isSome());
   if (*mCanRecycleDecoder) {
     // Do not recreate the decoder, reuse it.
     UpdateConfigFromExtraData(extra_data);
     if (!aSample->mTrackInfo) {
