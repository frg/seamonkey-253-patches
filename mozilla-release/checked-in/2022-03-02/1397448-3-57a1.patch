# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1505063125 25200
#      Sun Sep 10 10:05:25 2017 -0700
# Node ID bc7cd6e70a267dcca38e50450ded275e48cbc788
# Parent  31fe74ccc30ddea465f5772dd7170054f8e553e0
Bug 1397448: Part 3 - Reduce the number of promise callbacks created in MessageChannel. r=zombie

MozReview-Commit-ID: 2A4P9eaWnKx

diff --git a/toolkit/components/extensions/MessageChannel.jsm b/toolkit/components/extensions/MessageChannel.jsm
--- a/toolkit/components/extensions/MessageChannel.jsm
+++ b/toolkit/components/extensions/MessageChannel.jsm
@@ -276,22 +276,47 @@ this.MessageChannel = {
 
     this.messageManagers = new FilteringMessageManagerMap(
       MESSAGE_MESSAGE, this._handleMessage.bind(this));
 
     this.responseManagers = new FilteringMessageManagerMap(
       MESSAGE_RESPONSE, this._handleResponse.bind(this));
 
     /**
-     * Contains a list of pending responses, either waiting to be
-     * received or waiting to be sent. @see _addPendingResponse
+     * @property {Set<Deferred>} pendingResponses
+     * Contains a set of pending responses, either waiting to be
+     * received or waiting to be sent.
+     *
+     * The response object must be a deferred promise with the following
+     * properties:
+     *
+     *  promise:
+     *    The promise object which resolves or rejects when the response
+     *    is no longer pending.
+     *
+     *  reject:
+     *    A function which, when called, causes the `promise` object to be
+     *    rejected.
+     *
+     *  sender:
+     *    A sender object, as passed to `sendMessage.
+     *
+     *  messageManager:
+     *    The message manager the response will be sent or received on.
+     *
+     * When the promise resolves or rejects, it must be removed from the
+     * list.
+     *
+     * These values are used to clear pending responses when execution
+     * contexts are destroyed.
      */
     this.pendingResponses = new Set();
 
     /**
+     * @property {LimitedSet<string>} abortedResponses
      * Contains the message name of a limited number of aborted response
      * handlers, the responses for which will be ignored.
      */
     this.abortedResponses = new ExtensionUtils.LimitedSet(30);
   },
 
   RESULT_SUCCESS: 0,
   RESULT_DISCONNECTED: 1,
@@ -534,26 +559,27 @@ this.MessageChannel = {
     deferred.promise = new Promise((resolve, reject) => {
       deferred.resolve = resolve;
       deferred.reject = reject;
     });
     deferred.sender = recipient;
     deferred.messageManager = target;
     deferred.channelId = channelId;
 
-    this._addPendingResponse(deferred);
-
     // The channel ID is used as the message name when routing responses.
     // Add a message listener to the response broker, and remove it once
     // we've gotten (or canceled) a response.
     let broker = this.responseManagers.get(target);
     broker.addHandler(channelId, deferred);
 
+    this.pendingResponses.add(deferred);
+
     let cleanup = () => {
       broker.removeHandler(channelId, deferred);
+      this.pendingResponses.delete(deferred);
     };
     deferred.promise.then(cleanup, cleanup);
 
     try {
       target.sendAsyncMessage(MESSAGE_MESSAGE, message);
     } catch (e) {
       deferred.reject(e);
     }
@@ -640,16 +666,23 @@ this.MessageChannel = {
     let target = new MessageManagerProxy(data.target);
 
     let deferred = {
       sender: data.sender,
       messageManager: target,
       channelId: data.channelId,
       respondingSide: true,
     };
+
+    let cleanup = () => {
+      this.pendingResponses.delete(deferred);
+      target.dispose();
+    };
+    this.pendingResponses.add(deferred);
+
     deferred.promise = new Promise((resolve, reject) => {
       deferred.reject = reject;
 
       this._callHandlers(handlers, data).then(resolve, reject);
       data = null;
     }).then(
       value => {
         let response = {
@@ -689,23 +722,20 @@ this.MessageChannel = {
                            "columnNumber", "message", "stack", "result"]) {
             if (key in error) {
               response.error[key] = error[key];
             }
           }
         }
 
         target.sendAsyncMessage(MESSAGE_RESPONSE, response);
-      }).catch(e => {
+      }).then(cleanup, e => {
+        cleanup();
         Cu.reportError(e);
-      }).then(() => {
-        target.dispose();
       });
-
-    this._addPendingResponse(deferred);
   },
 
   /**
    * Handles message callbacks from the response brokers.
    *
    * Each handler object is a deferred object created by `sendMessage`, and
    * should be resolved or rejected based on the contents of the response.
    *
@@ -728,52 +758,16 @@ this.MessageChannel = {
     } else if (data.result === this.RESULT_SUCCESS) {
       handlers[0].resolve(data.value);
     } else {
       handlers[0].reject(data.error);
     }
   },
 
   /**
-   * Adds a pending response to the the `pendingResponses` list.
-   *
-   * The response object must be a deferred promise with the following
-   * properties:
-   *
-   *  promise:
-   *    The promise object which resolves or rejects when the response
-   *    is no longer pending.
-   *
-   *  reject:
-   *    A function which, when called, causes the `promise` object to be
-   *    rejected.
-   *
-   *  sender:
-   *    A sender object, as passed to `sendMessage.
-   *
-   *  messageManager:
-   *    The message manager the response will be sent or received on.
-   *
-   * When the promise resolves or rejects, it will be removed from the
-   * list.
-   *
-   * These values are used to clear pending responses when execution
-   * contexts are destroyed.
-   *
-   * @param {Deferred} deferred
-   */
-  _addPendingResponse(deferred) {
-    let cleanup = () => {
-      this.pendingResponses.delete(deferred);
-    };
-    this.pendingResponses.add(deferred);
-    deferred.promise.then(cleanup, cleanup);
-  },
-
-  /**
    * Aborts pending message response for the specific channel.
    *
    * @param {string} channelId
    *    A string for channelId of the response.
    * @param {object} reason
    *    An object describing the reason the response was aborted.
    *    Will be passed to the promise rejection handler of the aborted
    *    response.
