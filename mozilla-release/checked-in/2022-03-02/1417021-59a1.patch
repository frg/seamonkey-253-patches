# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1508883775 -3600
# Node ID 7159964287fe161b0dda9cdfa59d74bc7bead348
# Parent  161e24f1fe017da9fd66624c67425c221b793bde
Bug 1417021 - Fix various non-unified build errors in imagelib. r=aosmond

diff --git a/image/Decoder.cpp b/image/Decoder.cpp
--- a/image/Decoder.cpp
+++ b/image/Decoder.cpp
@@ -1,26 +1,29 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "Decoder.h"
 
-#include "mozilla/gfx/2D.h"
 #include "DecodePool.h"
 #include "GeckoProfiler.h"
 #include "IDecodingTask.h"
 #include "ISurfaceProvider.h"
+#include "mozilla/gfx/2D.h"
+#include "mozilla/gfx/Point.h"
+#include "mozilla/Telemetry.h"
+#include "nsComponentManagerUtils.h"
 #include "nsProxyRelease.h"
 #include "nsServiceManagerUtils.h"
-#include "nsComponentManagerUtils.h"
-#include "mozilla/Telemetry.h"
 
+using mozilla::gfx::IntPoint;
 using mozilla::gfx::IntSize;
+using mozilla::gfx::IntRect;
 using mozilla::gfx::SurfaceFormat;
 
 namespace mozilla {
 namespace image {
 
 class MOZ_STACK_CLASS AutoRecordDecoderTelemetry final
 {
 public:
diff --git a/image/DecoderFactory.cpp b/image/DecoderFactory.cpp
--- a/image/DecoderFactory.cpp
+++ b/image/DecoderFactory.cpp
@@ -5,17 +5,19 @@
 
 #include "DecoderFactory.h"
 
 #include "nsMimeTypes.h"
 #include "mozilla/RefPtr.h"
 
 #include "AnimationSurfaceProvider.h"
 #include "Decoder.h"
+#include "DecodedSurfaceProvider.h"
 #include "IDecodingTask.h"
+#include "ImageOps.h"
 #include "nsPNGDecoder.h"
 #include "nsGIFDecoder2.h"
 #include "nsJPEGDecoder.h"
 #include "nsBMPDecoder.h"
 #include "nsICODecoder.h"
 #include "nsIconDecoder.h"
 
 namespace mozilla {
diff --git a/image/IDecodingTask.h b/image/IDecodingTask.h
--- a/image/IDecodingTask.h
+++ b/image/IDecodingTask.h
@@ -6,20 +6,20 @@
 /**
  * An interface for tasks which can execute on the ImageLib DecodePool, and
  * various implementations.
  */
 
 #ifndef mozilla_image_IDecodingTask_h
 #define mozilla_image_IDecodingTask_h
 
+#include "imgFrame.h"
 #include "mozilla/NotNull.h"
 #include "mozilla/RefPtr.h"
-
-#include "imgFrame.h"
+#include "nsIEventTarget.h"
 #include "SourceBuffer.h"
 
 namespace mozilla {
 namespace image {
 
 class Decoder;
 class RasterImage;
 
diff --git a/image/ImageCacheKey.h b/image/ImageCacheKey.h
--- a/image/ImageCacheKey.h
+++ b/image/ImageCacheKey.h
@@ -8,16 +8,17 @@
  */
 
 #ifndef mozilla_image_src_ImageCacheKey_h
 #define mozilla_image_src_ImageCacheKey_h
 
 #include "mozilla/BasePrincipal.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/RefPtr.h"
+#include "PLDHashTable.h"
 
 class nsIDocument;
 class nsIURI;
 
 namespace mozilla {
 namespace image {
 
 class ImageURL;
diff --git a/image/ImageMetadata.h b/image/ImageMetadata.h
--- a/image/ImageMetadata.h
+++ b/image/ImageMetadata.h
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_image_ImageMetadata_h
 #define mozilla_image_ImageMetadata_h
 
 #include <stdint.h>
 #include "mozilla/Maybe.h"
 #include "nsSize.h"
+#include "nsTArray.h"
 #include "Orientation.h"
 #include "FrameTimeout.h"
 
 namespace mozilla {
 namespace image {
 
 // The metadata about an image that decoders accumulate as they decode.
 class ImageMetadata
diff --git a/image/StreamingLexer.h b/image/StreamingLexer.h
--- a/image/StreamingLexer.h
+++ b/image/StreamingLexer.h
@@ -16,16 +16,17 @@
 #include <algorithm>
 #include <cstdint>
 #include "mozilla/Assertions.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/Move.h"
 #include "mozilla/Variant.h"
 #include "mozilla/Vector.h"
+#include "SourceBuffer.h"
 
 namespace mozilla {
 namespace image {
 
 /// Buffering behaviors for StreamingLexer transitions.
 enum class BufferingStrategy
 {
   BUFFERED,   // Data will be buffered and processed in one chunk.
diff --git a/image/decoders/nsJPEGDecoder.cpp b/image/decoders/nsJPEGDecoder.cpp
--- a/image/decoders/nsJPEGDecoder.cpp
+++ b/image/decoders/nsJPEGDecoder.cpp
@@ -19,30 +19,33 @@
 #include "nspr.h"
 #include "nsCRT.h"
 #include "gfxColor.h"
 
 #include "jerror.h"
 
 #include "gfxPlatform.h"
 #include "mozilla/EndianUtils.h"
+#include "mozilla/gfx/Types.h"
 #include "mozilla/Telemetry.h"
 
 extern "C" {
 #include "iccjpeg.h"
 }
 
 #if MOZ_BIG_ENDIAN
 #define MOZ_JCS_EXT_NATIVE_ENDIAN_XRGB JCS_EXT_XRGB
 #else
 #define MOZ_JCS_EXT_NATIVE_ENDIAN_XRGB JCS_EXT_BGRX
 #endif
 
 static void cmyk_convert_rgb(JSAMPROW row, JDIMENSION width);
 
+using mozilla::gfx::SurfaceFormat;
+
 namespace mozilla {
 namespace image {
 
 static mozilla::LazyLogModule sJPEGLog("JPEGDecoder");
 
 static mozilla::LazyLogModule sJPEGDecoderAccountingLog("JPEGDecoderAccounting");
 
 static qcms_profile*
diff --git a/image/imgRequestProxy.cpp b/image/imgRequestProxy.cpp
--- a/image/imgRequestProxy.cpp
+++ b/image/imgRequestProxy.cpp
@@ -1,27 +1,30 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*-
  *
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
+#include "imgRequestProxy.h"
+
 #include "ImageLogging.h"
-#include "imgRequestProxy.h"
 #include "imgIOnloadBlocker.h"
 #include "imgLoader.h"
 #include "Image.h"
 #include "ImageOps.h"
 #include "nsError.h"
 #include "nsCRTGlue.h"
 #include "imgINotificationObserver.h"
 #include "mozilla/dom/TabGroup.h"       // for TabGroup
 #include "mozilla/dom/DocGroup.h"       // for DocGroup
+#include "mozilla/Move.h"
 #include "mozilla/Telemetry.h"          // for Telemetry
 
+using namespace mozilla;
 using namespace mozilla::image;
 
 // The split of imgRequestProxy and imgRequestProxyStatic means that
 // certain overridden functions need to be usable in the destructor.
 // Since virtual functions can't be used in that way, this class
 // provides a behavioural trait for each class to use instead.
 class ProxyBehaviour
 {
diff --git a/image/test/gtest/Common.h b/image/test/gtest/Common.h
--- a/image/test/gtest/Common.h
+++ b/image/test/gtest/Common.h
@@ -388,16 +388,17 @@ ImageTestCase CorruptTestCase();
 ImageTestCase CorruptBMPWithTruncatedHeader();
 ImageTestCase CorruptICOWithBadBMPWidthTestCase();
 ImageTestCase CorruptICOWithBadBMPHeightTestCase();
 ImageTestCase CorruptICOWithBadBppTestCase();
 
 ImageTestCase TransparentPNGTestCase();
 ImageTestCase TransparentGIFTestCase();
 ImageTestCase FirstFramePaddingGIFTestCase();
+ImageTestCase TransparentIfWithinICOBMPTestCase(TestCaseFlags aFlags);
 ImageTestCase NoFrameDelayGIFTestCase();
 ImageTestCase ExtraImageSubBlocksAnimatedGIFTestCase();
 
 ImageTestCase TransparentBMPWhenBMPAlphaEnabledTestCase();
 ImageTestCase RLE4BMPTestCase();
 ImageTestCase RLE8BMPTestCase();
 
 ImageTestCase DownscaledPNGTestCase();
diff --git a/image/test/gtest/TestDecoders.cpp b/image/test/gtest/TestDecoders.cpp
--- a/image/test/gtest/TestDecoders.cpp
+++ b/image/test/gtest/TestDecoders.cpp
@@ -4,16 +4,17 @@
 
 #include "gtest/gtest.h"
 
 #include "Common.h"
 #include "Decoder.h"
 #include "DecoderFactory.h"
 #include "decoders/nsBMPDecoder.h"
 #include "IDecodingTask.h"
+#include "ImageOps.h"
 #include "imgIContainer.h"
 #include "imgITools.h"
 #include "ImageFactory.h"
 #include "mozilla/gfx/2D.h"
 #include "nsComponentManagerUtils.h"
 #include "nsCOMPtr.h"
 #include "nsIInputStream.h"
 #include "nsIRunnable.h"
diff --git a/image/test/gtest/TestSourceBuffer.cpp b/image/test/gtest/TestSourceBuffer.cpp
--- a/image/test/gtest/TestSourceBuffer.cpp
+++ b/image/test/gtest/TestSourceBuffer.cpp
@@ -2,17 +2,19 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "gtest/gtest.h"
 
 #include <algorithm>
 #include <cstdint>
 
+#include "Common.h"
 #include "mozilla/Move.h"
+#include "nsIInputStream.h"
 #include "SourceBuffer.h"
 #include "SurfaceCache.h"
 
 using namespace mozilla;
 using namespace mozilla::image;
 
 using std::min;
 
diff --git a/image/test/gtest/TestStreamingLexer.cpp b/image/test/gtest/TestStreamingLexer.cpp
--- a/image/test/gtest/TestStreamingLexer.cpp
+++ b/image/test/gtest/TestStreamingLexer.cpp
@@ -1,14 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "gtest/gtest.h"
 
+#include "Common.h"
 #include "mozilla/Vector.h"
 #include "StreamingLexer.h"
 
 using namespace mozilla;
 using namespace mozilla::image;
 
 enum class TestState
 {
