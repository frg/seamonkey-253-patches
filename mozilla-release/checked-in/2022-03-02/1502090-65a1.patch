# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1540497002 0
# Node ID 1c4bf766a99a657e2f88183afbef240e9e8e38ac
# Parent  428742bd8237eef0d27e71c30fd37bc0a2b17e80
Bug 1502090 - Fix bailout tracking with fun.call. r=nbp

NOTE: Multi-arg array.push is still disabled in Ion.

Differential Revision: https://phabricator.services.mozilla.com/D9803

diff --git a/js/src/jit-test/tests/ion/bug1502090.js b/js/src/jit-test/tests/ion/bug1502090.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/ion/bug1502090.js
@@ -0,0 +1,13 @@
+function f(o) {
+   var a = [o];
+   a.length = a[0];
+   var useless = function() {}
+   var sz = Array.prototype.push.call(a, 42, 43);
+   (function(){
+       sz;
+   })(new Boolean(false));
+}
+for (var i = 0; i < 2; i++) {
+   f(1);
+}
+f(2);
diff --git a/js/src/jit/IonBuilder.cpp b/js/src/jit/IonBuilder.cpp
--- a/js/src/jit/IonBuilder.cpp
+++ b/js/src/jit/IonBuilder.cpp
@@ -5053,32 +5053,38 @@ IonBuilder::jsop_funcall(uint32_t argc)
         return makeCall(native, callInfo);
     }
     current->peek(calleeDepth)->setImplicitlyUsedUnchecked();
 
     // Extract call target.
     TemporaryTypeSet* funTypes = current->peek(funcDepth)->resultTypeSet();
     JSFunction* target = getSingleCallTarget(funTypes);
 
+    CallInfo callInfo(alloc(), pc, /* constructing = */ false,
+                      /* ignoresReturnValue = */ BytecodeIsPopped(pc));
+
+    // Save prior call stack in case we need to resolve during bailout
+    // recovery of inner inlined function. This includes the JSFunction and the
+    // 'call' native function.
+    MOZ_TRY(callInfo.savePriorCallStack(this, current, argc + 2));
+
     // Shimmy the slots down to remove the native 'call' function.
     current->shimmySlots(funcDepth - 1);
 
     bool zeroArguments = (argc == 0);
 
     // If no |this| argument was provided, explicitly pass Undefined.
     // Pushing is safe here, since one stack slot has been removed.
     if (zeroArguments) {
         pushConstant(UndefinedValue());
     } else {
         // |this| becomes implicit in the call.
         argc -= 1;
     }
 
-    CallInfo callInfo(alloc(), pc, /* constructing = */ false,
-                      /* ignoresReturnValue = */ BytecodeIsPopped(pc));
     if (!callInfo.init(current, argc))
         return abort(AbortReason::Alloc);
 
     // Try to inline the call.
     if (!zeroArguments) {
         InliningDecision decision = makeInliningDecision(target, callInfo);
         switch (decision) {
           case InliningDecision_Error:
