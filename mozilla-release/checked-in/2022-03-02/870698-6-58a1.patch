# HG changeset patch
# User Chris Peterson <cpeterson@mozilla.com>
# Date 1504833925 25200
# Node ID 2725a1e0b35c2bc4b1e69e68c42095d1f6480e1f
# Parent  5d5286c32b5d0a66d8d699405c1f5dcef68624c4
Bug 870698 - Part 6: Replace Append(NS_LITERAL_CSTRING("")) with AppendLiteral(""). r=erahm

The NS_LITERAL_CSTRING macro creates a temporary nsLiteralCString to encapsulate the string literal and its length, but AssignLiteral() can determine the string literal's length at compile-time without nsLiteralCString.

MozReview-Commit-ID: F750v6NN81s

diff --git a/dom/fetch/InternalRequest.h b/dom/fetch/InternalRequest.h
--- a/dom/fetch/InternalRequest.h
+++ b/dom/fetch/InternalRequest.h
@@ -136,17 +136,17 @@ public:
   // request's url list.
   void
   GetURL(nsACString& aURL) const
   {
     aURL.Assign(GetURLWithoutFragment());
     if (GetFragment().IsEmpty()) {
       return;
     }
-    aURL.Append(NS_LITERAL_CSTRING("#"));
+    aURL.AppendLiteral("#");
     aURL.Append(GetFragment());
   }
 
   const nsCString&
   GetURLWithoutFragment() const
   {
     MOZ_RELEASE_ASSERT(!mURLList.IsEmpty(),
                        "Internal Request's urlList should not be empty.");
diff --git a/dom/media/platforms/wmf/WMFVideoMFTManager.cpp b/dom/media/platforms/wmf/WMFVideoMFTManager.cpp
--- a/dom/media/platforms/wmf/WMFVideoMFTManager.cpp
+++ b/dom/media/platforms/wmf/WMFVideoMFTManager.cpp
@@ -446,17 +446,17 @@ public:
           DXVA2Manager::CreateD3D11DXVA(mKnowsCompositor, *failureReason);
         if (mDXVA2Manager) {
           return NS_OK;
         }
       }
       // Try again with d3d9, but record the failure reason
       // into a new var to avoid overwriting the d3d11 failure.
       failureReason = &secondFailureReason;
-      mFailureReason.Append(NS_LITERAL_CSTRING("; "));
+      mFailureReason.AppendLiteral("; ");
     }
 
     const nsCString& blacklistedDLL = FindD3D9BlacklistedDLL();
     if (!blacklistedDLL.IsEmpty()) {
       mFailureReason.AppendPrintf("D3D9 blacklisted with DLL %s",
                                   blacklistedDLL.get());
     } else {
       mDXVA2Manager =
@@ -544,19 +544,19 @@ WMFVideoMFTManager::Init()
     return result;
   }
 
     result = InitInternal();
     if (NS_SUCCEEDED(result) && mDXVA2Manager) {
     // If we had some failures but eventually made it work,
     // make sure we preserve the messages.
     if (mDXVA2Manager->IsD3D11()) {
-      mDXVAFailureReason.Append(NS_LITERAL_CSTRING("Using D3D11 API"));
+      mDXVAFailureReason.AppendLiteral("Using D3D11 API");
     } else {
-      mDXVAFailureReason.Append(NS_LITERAL_CSTRING("Using D3D9 API"));
+      mDXVAFailureReason.AppendLiteral("Using D3D9 API");
     }
   }
 
   return result;
 }
 
 MediaResult
 WMFVideoMFTManager::InitInternal()
diff --git a/dom/quota/ActorsParent.cpp b/dom/quota/ActorsParent.cpp
--- a/dom/quota/ActorsParent.cpp
+++ b/dom/quota/ActorsParent.cpp
@@ -8088,17 +8088,17 @@ OriginParser::Parse(nsACString& aSpec, O
 
     HandleToken(token);
 
     if (mError) {
       break;
     }
 
     if (!mHandledTokens.IsEmpty()) {
-      mHandledTokens.Append(NS_LITERAL_CSTRING(", "));
+      mHandledTokens.AppendLiteral(", ");
     }
     mHandledTokens.Append('\'');
     mHandledTokens.Append(token);
     mHandledTokens.Append('\'');
   }
 
   if (!mError && mTokenizer.separatorAfterCurrentToken()) {
     HandleTrailingSeparator();
diff --git a/dom/security/nsCSPContext.cpp b/dom/security/nsCSPContext.cpp
--- a/dom/security/nsCSPContext.cpp
+++ b/dom/security/nsCSPContext.cpp
@@ -77,29 +77,29 @@ CreateCacheKey_Internal(nsIURI* aContent
 
   bool isDataScheme = false;
   nsresult rv = aContentLocation->SchemeIs("data", &isDataScheme);
   NS_ENSURE_SUCCESS(rv, rv);
 
   outCacheKey.Truncate();
   if (aContentType != nsIContentPolicy::TYPE_SCRIPT && isDataScheme) {
     // For non-script data: URI, use ("data:", aContentType) as the cache key.
-    outCacheKey.Append(NS_LITERAL_CSTRING("data:"));
+    outCacheKey.AppendLiteral("data:");
     outCacheKey.AppendInt(aContentType);
     return NS_OK;
   }
 
   nsAutoCString spec;
   rv = aContentLocation->GetSpec(spec);
   NS_ENSURE_SUCCESS(rv, rv);
 
   // Don't cache for a URI longer than the cutoff size.
   if (spec.Length() <= CSP_CACHE_URI_CUTOFF_SIZE) {
     outCacheKey.Append(spec);
-    outCacheKey.Append(NS_LITERAL_CSTRING("!"));
+    outCacheKey.AppendLiteral("!");
     outCacheKey.AppendInt(aContentType);
   }
 
   return NS_OK;
 }
 
 /* =====  nsIContentSecurityPolicy impl ====== */
 
diff --git a/dom/storage/StorageDBThread.cpp b/dom/storage/StorageDBThread.cpp
--- a/dom/storage/StorageDBThread.cpp
+++ b/dom/storage/StorageDBThread.cpp
@@ -87,18 +87,19 @@ Scheme0Scope(LocalStorageCacheBridge* aC
   oa.mInIsolatedMozBrowser = false;
   oa.CreateSuffix(remaining);
   if (!remaining.IsEmpty()) {
     MOZ_ASSERT(!suffix.IsEmpty());
 
     if (result.IsEmpty()) {
       // Must contain the old prefix, otherwise we won't search for the whole
       // origin attributes suffix.
-      result.Append(NS_LITERAL_CSTRING("0:f:"));
+      result.AppendLiteral("0:f:");
     }
+
     // Append the whole origin attributes suffix despite we have already stored
     // appid and inbrowser.  We are only looking for it when the scope string
     // starts with "$appid:$inbrowser:" (with whatever valid values).
     //
     // The OriginAttributes suffix is a string in a form like:
     // "^addonId=101&userContextId=5" and it's ensured it always starts with '^'
     // and never contains ':'.  See OriginAttributes::CreateSuffix.
     result.Append(suffix);
diff --git a/netwerk/protocol/about/nsAboutCache.cpp b/netwerk/protocol/about/nsAboutCache.cpp
--- a/netwerk/protocol/about/nsAboutCache.cpp
+++ b/netwerk/protocol/about/nsAboutCache.cpp
@@ -497,19 +497,19 @@ nsAboutCache::Channel::OnCacheEntryInfo(
     } else {
         mBuffer.AppendLiteral("No expiration time");
     }
     mBuffer.AppendLiteral("</td>\n");
 
     // Pinning
     mBuffer.AppendLiteral("    <td>");
     if (aPinned) {
-      mBuffer.Append(NS_LITERAL_CSTRING("Pinned"));
+      mBuffer.AppendLiteral("Pinned");
     } else {
-      mBuffer.Append(NS_LITERAL_CSTRING("&nbsp;"));
+      mBuffer.AppendLiteral("&nbsp;");
     }
     mBuffer.AppendLiteral("</td>\n");
 
     // Entry is done...
     mBuffer.AppendLiteral("  </tr>\n");
 
     return FlushBuffer();
 }
diff --git a/netwerk/protocol/http/AlternateServices.cpp b/netwerk/protocol/http/AlternateServices.cpp
--- a/netwerk/protocol/http/AlternateServices.cpp
+++ b/netwerk/protocol/http/AlternateServices.cpp
@@ -915,17 +915,17 @@ AltSvcCache::UpdateAltServiceMapping(Alt
     nsAutoCString origin (NS_LITERAL_CSTRING("http://") + map->OriginHost());
     if (map->OriginPort() != NS_HTTP_DEFAULT_PORT) {
       origin.Append(':');
       origin.AppendInt(map->OriginPort());
     }
 
     nsCOMPtr<nsIURI> wellKnown;
     nsAutoCString uri(origin);
-    uri.Append(NS_LITERAL_CSTRING("/.well-known/http-opportunistic"));
+    uri.AppendLiteral("/.well-known/http-opportunistic");
     NS_NewURI(getter_AddRefs(wellKnown), uri);
 
     auto *checker = new WellKnownChecker(wellKnown, origin, caps, ci, map);
     if (NS_FAILED(checker->Start())) {
       LOG(("AltSvcCache::UpdateAltServiceMapping %p .wk checker failed to start\n", this));
       map->SetExpired();
       delete checker;
       checker = nullptr;
diff --git a/netwerk/protocol/http/nsHttpRequestHead.cpp b/netwerk/protocol/http/nsHttpRequestHead.cpp
--- a/netwerk/protocol/http/nsHttpRequestHead.cpp
+++ b/netwerk/protocol/http/nsHttpRequestHead.cpp
@@ -324,20 +324,20 @@ nsHttpRequestHead::SetMethod(const nsACS
 }
 
 void
 nsHttpRequestHead::SetOrigin(const nsACString &scheme, const nsACString &host,
                              int32_t port)
 {
     RecursiveMutexAutoLock mon(mRecursiveMutex);
     mOrigin.Assign(scheme);
-    mOrigin.Append(NS_LITERAL_CSTRING("://"));
+    mOrigin.AppendLiteral("://");
     mOrigin.Append(host);
     if (port >= 0) {
-        mOrigin.Append(NS_LITERAL_CSTRING(":"));
+        mOrigin.AppendLiteral(":");
         mOrigin.AppendInt(port);
     }
 }
 
 bool
 nsHttpRequestHead::IsSafeMethod()
 {
     RecursiveMutexAutoLock mon(mRecursiveMutex);
diff --git a/toolkit/components/places/nsNavHistory.cpp b/toolkit/components/places/nsNavHistory.cpp
--- a/toolkit/components/places/nsNavHistory.cpp
+++ b/toolkit/components/places/nsNavHistory.cpp
@@ -1756,24 +1756,22 @@ PlacesSQLQueryBuilder::SelectAsDay()
         else {
           nsNavHistory::GetMonthName(tm, dateName);
         }
 
         // From start of MonthIndex + 1 months ago
         sqlFragmentContainerBeginTime = NS_LITERAL_CSTRING(
           "(strftime('%s','now','localtime','start of month','-");
         sqlFragmentContainerBeginTime.AppendInt(MonthIndex);
-        sqlFragmentContainerBeginTime.Append(NS_LITERAL_CSTRING(
-            " months','utc')*1000000)"));
+        sqlFragmentContainerBeginTime.AppendLiteral(" months','utc')*1000000)");
         // To start of MonthIndex months ago
         sqlFragmentContainerEndTime = NS_LITERAL_CSTRING(
           "(strftime('%s','now','localtime','start of month','-");
         sqlFragmentContainerEndTime.AppendInt(MonthIndex - 1);
-        sqlFragmentContainerEndTime.Append(NS_LITERAL_CSTRING(
-            " months','utc')*1000000)"));
+        sqlFragmentContainerEndTime.AppendLiteral(" months','utc')*1000000)");
         // Search for the same timeframe.
         sqlFragmentSearchBeginTime = sqlFragmentContainerBeginTime;
         sqlFragmentSearchEndTime = sqlFragmentContainerEndTime;
         break;
     }
 
     nsPrintfCString dateParam("dayTitle%d", i);
     mAddParams.Put(dateParam, dateName);
