# HG changeset patch
# User Matthew Gregan <kinetik@flim.org>
# Date 1511316951 -46800
# Node ID 7beea3063907269c70c04ad030b65fb1712169df
# Parent  564b3e17264466fe2994addb4098c389944eb00a
Bug 1419627 - Update mp4parse-rust callers in cheddar->cbindgen migration.  r=rillian

MozReview-Commit-ID: AbyXNRrqr4X

diff --git a/dom/media/gtest/mp4_demuxer/TestMP4.cpp b/dom/media/gtest/mp4_demuxer/TestMP4.cpp
--- a/dom/media/gtest/mp4_demuxer/TestMP4.cpp
+++ b/dom/media/gtest/mp4_demuxer/TestMP4.cpp
@@ -56,87 +56,87 @@ vector_reader(uint8_t* buffer, uintptr_t
   uintptr_t length = std::min(available, size);
   memcpy(buffer, source->buffer.data() + source->location, length);
   source->location += length;
   return length;
 }
 
 TEST(rust, MP4MetadataEmpty)
 {
-  mp4parse_status rv;
-  mp4parse_io io;
+  Mp4parseStatus rv;
+  Mp4parseIo io;
 
   // Shouldn't be able to read with no context.
   rv = mp4parse_read(nullptr);
-  EXPECT_EQ(rv, mp4parse_status_BAD_ARG);
+  EXPECT_EQ(rv, MP4PARSE_STATUS_BAD_ARG);
 
-  // Shouldn't be able to wrap an mp4parse_io with null members.
+  // Shouldn't be able to wrap an Mp4parseIo with null members.
   io = { nullptr, nullptr };
-  mp4parse_parser* context = mp4parse_new(&io);
+  Mp4parseParser* context = mp4parse_new(&io);
   EXPECT_EQ(context, nullptr);
 
   io = { nullptr, &io };
   context = mp4parse_new(&io);
   EXPECT_EQ(context, nullptr);
 
   // FIXME: this should probably be accepted.
   io = { error_reader, nullptr };
   context = mp4parse_new(&io);
   EXPECT_EQ(context, nullptr);
 
   // Read method errors should propagate.
   io = { error_reader, &io };
   context = mp4parse_new(&io);
   ASSERT_NE(context, nullptr);
   rv = mp4parse_read(context);
-  EXPECT_EQ(rv, mp4parse_status_IO);
+  EXPECT_EQ(rv, MP4PARSE_STATUS_IO);
   mp4parse_free(context);
 
   // Short buffers should fail.
   read_vector buf(0);
   io = { vector_reader, &buf };
   context = mp4parse_new(&io);
   ASSERT_NE(context, nullptr);
   rv = mp4parse_read(context);
-  EXPECT_EQ(rv, mp4parse_status_INVALID);
+  EXPECT_EQ(rv, MP4PARSE_STATUS_INVALID);
   mp4parse_free(context);
 
   buf.buffer.reserve(4097);
   context = mp4parse_new(&io);
   ASSERT_NE(context, nullptr);
   rv = mp4parse_read(context);
-  EXPECT_EQ(rv, mp4parse_status_INVALID);
+  EXPECT_EQ(rv, MP4PARSE_STATUS_INVALID);
   mp4parse_free(context);
 
   // Empty buffers should fail.
   buf.buffer.resize(4097, 0);
   context = mp4parse_new(&io);
   rv = mp4parse_read(context);
-  EXPECT_EQ(rv, mp4parse_status_UNSUPPORTED);
+  EXPECT_EQ(rv, MP4PARSE_STATUS_UNSUPPORTED);
   mp4parse_free(context);
 }
 
 TEST(rust, MP4Metadata)
 {
   FILE* f = fopen("street.mp4", "rb");
   ASSERT_TRUE(f != nullptr);
   // Read just the moov header to work around the parser
   // treating mid-box eof as an error.
   //read_vector reader = read_vector(f, 1061);
   struct stat s;
   ASSERT_EQ(0, fstat(fileno(f), &s));
   read_vector reader = read_vector(f, s.st_size);
   fclose(f);
 
-  mp4parse_io io = { vector_reader, &reader };
-  mp4parse_parser* context = mp4parse_new(&io);
+  Mp4parseIo io = { vector_reader, &reader };
+  Mp4parseParser* context = mp4parse_new(&io);
   ASSERT_NE(nullptr, context);
 
-  mp4parse_status rv = mp4parse_read(context);
-  EXPECT_EQ(mp4parse_status_OK, rv);
+  Mp4parseStatus rv = mp4parse_read(context);
+  EXPECT_EQ(MP4PARSE_STATUS_OK, rv);
 
   uint32_t tracks = 0;
   rv = mp4parse_get_track_count(context, &tracks);
-  EXPECT_EQ(mp4parse_status_OK, rv);
+  EXPECT_EQ(MP4PARSE_STATUS_OK, rv);
   EXPECT_EQ(2U, tracks);
 
   mp4parse_free(context);
 }
diff --git a/dom/media/mp4/DecoderData.cpp b/dom/media/mp4/DecoderData.cpp
--- a/dom/media/mp4/DecoderData.cpp
+++ b/dom/media/mp4/DecoderData.cpp
@@ -48,48 +48,48 @@ MP4AudioInfo::IsValid() const
   return mChannels > 0 && mRate > 0 &&
          // Accept any mime type here, but if it's aac, validate the profile.
          (!mMimeType.EqualsLiteral("audio/mp4a-latm") ||
           mProfile > 0 || mExtendedProfile > 0);
 }
 
 static void
 UpdateTrackProtectedInfo(mozilla::TrackInfo& aConfig,
-                         const mp4parse_sinf_info& aSinf)
+                         const Mp4parseSinfInfo& aSinf)
 {
   if (aSinf.is_encrypted != 0) {
     aConfig.mCrypto.mValid = true;
     aConfig.mCrypto.mMode = aSinf.is_encrypted;
     aConfig.mCrypto.mIVSize = aSinf.iv_size;
     aConfig.mCrypto.mKeyId.AppendElements(aSinf.kid.data, aSinf.kid.length);
   }
 }
 
 void
-MP4AudioInfo::Update(const mp4parse_track_info* track,
-                     const mp4parse_track_audio_info* audio)
+MP4AudioInfo::Update(const Mp4parseTrackInfo* track,
+                     const Mp4parseTrackAudioInfo* audio)
 {
   UpdateTrackProtectedInfo(*this, audio->protected_data);
 
-  if (track->codec == mp4parse_codec_OPUS) {
+  if (track->codec == MP4PARSE_CODEC_OPUS) {
     mMimeType = NS_LITERAL_CSTRING("audio/opus");
     // The Opus decoder expects the container's codec delay or
     // pre-skip value, in microseconds, as a 64-bit int at the
     // start of the codec-specific config blob.
     MOZ_ASSERT(audio->extra_data.data);
     MOZ_ASSERT(audio->extra_data.length >= 12);
     uint16_t preskip =
       mozilla::LittleEndian::readUint16(audio->extra_data.data + 10);
     mozilla::OpusDataDecoder::AppendCodecDelay(mCodecSpecificConfig,
         mozilla::FramesToUsecs(preskip, 48000).value());
-  } else if (track->codec == mp4parse_codec_AAC) {
+  } else if (track->codec == MP4PARSE_CODEC_AAC) {
     mMimeType = NS_LITERAL_CSTRING("audio/mp4a-latm");
-  } else if (track->codec == mp4parse_codec_FLAC) {
+  } else if (track->codec == MP4PARSE_CODEC_FLAC) {
     mMimeType = NS_LITERAL_CSTRING("audio/flac");
-  } else if (track->codec == mp4parse_codec_MP3) {
+  } else if (track->codec == MP4PARSE_CODEC_MP3) {
     mMimeType = NS_LITERAL_CSTRING("audio/mpeg");
   }
 
   mRate = audio->sample_rate;
   mChannels = audio->channels;
   mBitDepth = audio->bit_depth;
   mExtendedProfile = audio->profile;
   mDuration = TimeUnit::FromMicroseconds(track->duration);
@@ -109,25 +109,25 @@ MP4AudioInfo::Update(const mp4parse_trac
 
   if (audio->codec_specific_config.length > 0) {
     mCodecSpecificConfig->AppendElements(audio->codec_specific_config.data,
                                          audio->codec_specific_config.length);
   }
 }
 
 void
-MP4VideoInfo::Update(const mp4parse_track_info* track,
-                     const mp4parse_track_video_info* video)
+MP4VideoInfo::Update(const Mp4parseTrackInfo* track,
+                     const Mp4parseTrackVideoInfo* video)
 {
   UpdateTrackProtectedInfo(*this, video->protected_data);
-  if (track->codec == mp4parse_codec_AVC) {
+  if (track->codec == MP4PARSE_CODEC_AVC) {
     mMimeType = NS_LITERAL_CSTRING("video/avc");
-  } else if (track->codec == mp4parse_codec_VP9) {
+  } else if (track->codec == MP4PARSE_CODEC_VP9) {
     mMimeType = NS_LITERAL_CSTRING("video/vp9");
-  } else if (track->codec == mp4parse_codec_MP4V) {
+  } else if (track->codec == MP4PARSE_CODEC_MP4V) {
     mMimeType = NS_LITERAL_CSTRING("video/mp4v-es");
   }
   mTrackId = track->track_id;
   mDuration = TimeUnit::FromMicroseconds(track->duration);
   mMediaTime = TimeUnit::FromMicroseconds(track->media_time);
   mDisplay.width = video->display_width;
   mDisplay.height = video->display_height;
   mImage.width = video->image_width;
diff --git a/dom/media/mp4/DecoderData.h b/dom/media/mp4/DecoderData.h
--- a/dom/media/mp4/DecoderData.h
+++ b/dom/media/mp4/DecoderData.h
@@ -8,22 +8,17 @@
 #include "MediaInfo.h"
 #include "mozilla/RefPtr.h"
 #include "mozilla/Result.h"
 #include "mozilla/Types.h"
 #include "mozilla/Vector.h"
 #include "nsString.h"
 #include "nsTArray.h"
 #include "nsString.h"
-
-extern "C" {
-typedef struct mp4parse_track_info mp4parse_track_info;
-typedef struct mp4parse_track_audio_info mp4parse_track_audio_info;
-typedef struct mp4parse_track_video_info mp4parse_track_video_info;
-}
+#include "mp4parse.h"
 
 namespace mozilla
 {
 
 class MP4Demuxer;
 
 struct PsshInfo
 {
@@ -58,28 +53,28 @@ private:
   mozilla::Result<mozilla::Ok, nsresult> DoUpdate(const uint8_t* aData, size_t aLength);
 };
 
 class MP4AudioInfo : public mozilla::AudioInfo
 {
 public:
   MP4AudioInfo() = default;
 
-  void Update(const mp4parse_track_info* track,
-              const mp4parse_track_audio_info* audio);
+  void Update(const Mp4parseTrackInfo* track,
+              const Mp4parseTrackAudioInfo* audio);
 
   virtual bool IsValid() const override;
 };
 
 class MP4VideoInfo : public mozilla::VideoInfo
 {
 public:
   MP4VideoInfo() = default;
 
-  void Update(const mp4parse_track_info* track,
-              const mp4parse_track_video_info* video);
+  void Update(const Mp4parseTrackInfo* track,
+              const Mp4parseTrackVideoInfo* video);
 
   virtual bool IsValid() const override;
 };
 
 }
 
 #endif
diff --git a/dom/media/mp4/MP4Metadata.cpp b/dom/media/mp4/MP4Metadata.cpp
--- a/dom/media/mp4/MP4Metadata.cpp
+++ b/dom/media/mp4/MP4Metadata.cpp
@@ -20,17 +20,17 @@
 #include <stdint.h>
 #include <vector>
 
 using mozilla::media::TimeUnit;
 
 namespace mozilla {
 LazyLogModule gMP4MetadataLog("MP4Metadata");
 
-IndiceWrapper::IndiceWrapper(mp4parse_byte_data& aIndice)
+IndiceWrapper::IndiceWrapper(Mp4parseByteData& aIndice)
 {
   mIndice.length = aIndice.length;
   mIndice.indices = aIndice.indices;
 }
 
 size_t
 IndiceWrapper::Length() const
 {
@@ -40,17 +40,17 @@ IndiceWrapper::Length() const
 bool
 IndiceWrapper::GetIndice(size_t aIndex, Index::Indice& aIndice) const
 {
   if (aIndex >= mIndice.length) {
     MOZ_LOG(gMP4MetadataLog, LogLevel::Error, ("Index overflow in indice"));
    return false;
   }
 
-  const mp4parse_indice* indice = &mIndice.indices[aIndex];
+  const Mp4parseIndice* indice = &mIndice.indices[aIndex];
   aIndice.start_offset = indice->start_offset;
   aIndice.end_offset = indice->end_offset;
   aIndice.start_composition = indice->start_composition;
   aIndice.end_composition = indice->end_composition;
   aIndice.start_decode = indice->start_decode;
   aIndice.sync = indice->sync;
   return true;
 }
@@ -98,93 +98,93 @@ read_source(uint8_t* buffer, uintptr_t s
   }
   return bytes_read;
 }
 
 MP4Metadata::MP4Metadata(ByteStream* aSource)
   : mSource(aSource)
   , mSourceAdaptor(aSource)
 {
-  mp4parse_io io = { read_source, &mSourceAdaptor };
+  Mp4parseIo io = { read_source, &mSourceAdaptor };
   mParser.reset(mp4parse_new(&io));
   MOZ_ASSERT(mParser);
 
   if (MOZ_LOG_TEST(gMP4MetadataLog, LogLevel::Debug)) {
     mp4parse_log(true);
   }
 }
 
 MP4Metadata::~MP4Metadata()
 {
 }
 
 nsresult
 MP4Metadata::Parse()
 {
-  mp4parse_status rv = mp4parse_read(mParser.get());
-  if (rv != mp4parse_status_OK) {
+  Mp4parseStatus rv = mp4parse_read(mParser.get());
+  if (rv != MP4PARSE_STATUS_OK) {
     MOZ_LOG(gMP4MetadataLog, LogLevel::Debug, ("Parse failed, return code %d\n", rv));
-    return rv == mp4parse_status_OOM ? NS_ERROR_OUT_OF_MEMORY
+    return rv == MP4PARSE_STATUS_OOM ? NS_ERROR_OUT_OF_MEMORY
                                      : NS_ERROR_DOM_MEDIA_METADATA_ERR;
   }
 
   UpdateCrypto();
 
   return NS_OK;
 }
 
 void
 MP4Metadata::UpdateCrypto()
 {
-  mp4parse_pssh_info info = {};
-  if (mp4parse_get_pssh_info(mParser.get(), &info) != mp4parse_status_OK) {
+  Mp4parsePsshInfo info = {};
+  if (mp4parse_get_pssh_info(mParser.get(), &info) != MP4PARSE_STATUS_OK) {
     return;
   }
 
   if (info.data.length == 0) {
     return;
   }
 
   mCrypto.Update(info.data.data, info.data.length);
 }
 
 bool
-TrackTypeEqual(TrackInfo::TrackType aLHS, mp4parse_track_type aRHS)
+TrackTypeEqual(TrackInfo::TrackType aLHS, Mp4parseTrackType aRHS)
 {
   switch (aLHS) {
   case TrackInfo::kAudioTrack:
-    return aRHS == mp4parse_track_type_AUDIO;
+    return aRHS == MP4PARSE_TRACK_TYPE_AUDIO;
   case TrackInfo::kVideoTrack:
-    return aRHS == mp4parse_track_type_VIDEO;
+    return aRHS == MP4PARSE_TRACK_TYPE_VIDEO;
   default:
     return false;
   }
 }
 
 MP4Metadata::ResultAndTrackCount
 MP4Metadata::GetNumberTracks(mozilla::TrackInfo::TrackType aType) const
 {
   uint32_t tracks;
   auto rv = mp4parse_get_track_count(mParser.get(), &tracks);
-  if (rv != mp4parse_status_OK) {
+  if (rv != MP4PARSE_STATUS_OK) {
     MOZ_LOG(gMP4MetadataLog, LogLevel::Warning,
         ("rust parser error %d counting tracks", rv));
     return {MediaResult(NS_ERROR_DOM_MEDIA_METADATA_ERR,
                         RESULT_DETAIL("Rust parser error %d", rv)),
             MP4Metadata::NumberTracksError()};
   }
 
   uint32_t total = 0;
   for (uint32_t i = 0; i < tracks; ++i) {
-    mp4parse_track_info track_info;
+    Mp4parseTrackInfo track_info;
     rv = mp4parse_get_track_info(mParser.get(), i, &track_info);
-    if (rv != mp4parse_status_OK) {
+    if (rv != MP4PARSE_STATUS_OK) {
       continue;
     }
-    if (track_info.codec == mp4parse_codec::mp4parse_codec_UNKNOWN) {
+    if (track_info.codec == MP4PARSE_CODEC_UNKNOWN) {
       continue;
     }
     if (TrackTypeEqual(aType, track_info.track_type)) {
         total += 1;
     }
   }
 
   MOZ_LOG(gMP4MetadataLog, LogLevel::Info, ("%s tracks found: %u",
@@ -194,29 +194,29 @@ MP4Metadata::GetNumberTracks(mozilla::Tr
   return {NS_OK, total};
 }
 
 Maybe<uint32_t>
 MP4Metadata::TrackTypeToGlobalTrackIndex(mozilla::TrackInfo::TrackType aType, size_t aTrackNumber) const
 {
   uint32_t tracks;
   auto rv = mp4parse_get_track_count(mParser.get(), &tracks);
-  if (rv != mp4parse_status_OK) {
+  if (rv != MP4PARSE_STATUS_OK) {
     return Nothing();
   }
 
   /* The MP4Metadata API uses a per-TrackType index of tracks, but mp4parse
      (and libstagefright) use a global track index.  Convert the index by
      counting the tracks of the requested type and returning the global
      track index when a match is found. */
   uint32_t perType = 0;
   for (uint32_t i = 0; i < tracks; ++i) {
-    mp4parse_track_info track_info;
+    Mp4parseTrackInfo track_info;
     rv = mp4parse_get_track_info(mParser.get(), i, &track_info);
-    if (rv != mp4parse_status_OK) {
+    if (rv != MP4PARSE_STATUS_OK) {
       continue;
     }
     if (TrackTypeEqual(aType, track_info.track_type)) {
       if (perType == aTrackNumber) {
         return Some(i);
       }
       perType += 1;
     }
@@ -232,68 +232,68 @@ MP4Metadata::GetTrackInfo(mozilla::Track
   Maybe<uint32_t> trackIndex = TrackTypeToGlobalTrackIndex(aType, aTrackNumber);
   if (trackIndex.isNothing()) {
     return {MediaResult(NS_ERROR_DOM_MEDIA_METADATA_ERR,
                         RESULT_DETAIL("No %s tracks",
                                       TrackTypeToStr(aType))),
             nullptr};
   }
 
-  mp4parse_track_info info;
+  Mp4parseTrackInfo info;
   auto rv = mp4parse_get_track_info(mParser.get(), trackIndex.value(), &info);
-  if (rv != mp4parse_status_OK) {
+  if (rv != MP4PARSE_STATUS_OK) {
     MOZ_LOG(gMP4MetadataLog, LogLevel::Warning, ("mp4parse_get_track_info returned %d", rv));
     return {MediaResult(NS_ERROR_DOM_MEDIA_METADATA_ERR,
                         RESULT_DETAIL("Cannot find %s track #%zu",
                                       TrackTypeToStr(aType),
                                       aTrackNumber)),
             nullptr};
   }
 #ifdef DEBUG
   const char* codec_string = "unrecognized";
   switch (info.codec) {
-    case mp4parse_codec_UNKNOWN: codec_string = "unknown"; break;
-    case mp4parse_codec_AAC: codec_string = "aac"; break;
-    case mp4parse_codec_OPUS: codec_string = "opus"; break;
-    case mp4parse_codec_FLAC: codec_string = "flac"; break;
-    case mp4parse_codec_AVC: codec_string = "h.264"; break;
-    case mp4parse_codec_VP9: codec_string = "vp9"; break;
-    case mp4parse_codec_MP3: codec_string = "mp3"; break;
-    case mp4parse_codec_MP4V: codec_string = "mp4v"; break;
-    case mp4parse_codec_JPEG: codec_string = "jpeg"; break;
-    case mp4parse_codec_AC3: codec_string = "ac-3"; break;
-    case mp4parse_codec_EC3: codec_string = "ec-3"; break;
+    case MP4PARSE_CODEC_UNKNOWN: codec_string = "unknown"; break;
+    case MP4PARSE_CODEC_AAC: codec_string = "aac"; break;
+    case MP4PARSE_CODEC_OPUS: codec_string = "opus"; break;
+    case MP4PARSE_CODEC_FLAC: codec_string = "flac"; break;
+    case MP4PARSE_CODEC_AVC: codec_string = "h.264"; break;
+    case MP4PARSE_CODEC_VP9: codec_string = "vp9"; break;
+    case MP4PARSE_CODEC_MP3: codec_string = "mp3"; break;
+    case MP4PARSE_CODEC_MP4V: codec_string = "mp4v"; break;
+    case MP4PARSE_CODEC_JPEG: codec_string = "jpeg"; break;
+    case MP4PARSE_CODEC_AC3: codec_string = "ac-3"; break;
+    case MP4PARSE_CODEC_EC3: codec_string = "ec-3"; break;
   }
   MOZ_LOG(gMP4MetadataLog, LogLevel::Debug,
     ("track codec %s (%u)\n", codec_string, info.codec));
 #endif
 
   // This specialization interface is crazy.
   UniquePtr<mozilla::TrackInfo> e;
   switch (aType) {
     case TrackInfo::TrackType::kAudioTrack: {
-      mp4parse_track_audio_info audio;
+      Mp4parseTrackAudioInfo audio;
       auto rv = mp4parse_get_track_audio_info(mParser.get(), trackIndex.value(), &audio);
-      if (rv != mp4parse_status_OK) {
+      if (rv != MP4PARSE_STATUS_OK) {
         MOZ_LOG(gMP4MetadataLog, LogLevel::Warning, ("mp4parse_get_track_audio_info returned error %d", rv));
         return {MediaResult(NS_ERROR_DOM_MEDIA_METADATA_ERR,
                             RESULT_DETAIL("Cannot parse %s track #%zu",
                                           TrackTypeToStr(aType),
                                           aTrackNumber)),
                 nullptr};
       }
       auto track = mozilla::MakeUnique<MP4AudioInfo>();
       track->Update(&info, &audio);
       e = Move(track);
     }
     break;
     case TrackInfo::TrackType::kVideoTrack: {
-      mp4parse_track_video_info video;
+      Mp4parseTrackVideoInfo video;
       auto rv = mp4parse_get_track_video_info(mParser.get(), trackIndex.value(), &video);
-      if (rv != mp4parse_status_OK) {
+      if (rv != MP4PARSE_STATUS_OK) {
         MOZ_LOG(gMP4MetadataLog, LogLevel::Warning, ("mp4parse_get_track_video_info returned error %d", rv));
         return {MediaResult(NS_ERROR_DOM_MEDIA_METADATA_ERR,
                             RESULT_DETAIL("Cannot parse %s track #%zu",
                                           TrackTypeToStr(aType),
                                           aTrackNumber)),
                 nullptr};
       }
       auto track = mozilla::MakeUnique<MP4VideoInfo>();
@@ -307,19 +307,19 @@ MP4Metadata::GetTrackInfo(mozilla::Track
                           RESULT_DETAIL("Cannot handle %s track #%zu",
                                         TrackTypeToStr(aType),
                                         aTrackNumber)),
               nullptr};
   }
 
   // No duration in track, use fragment_duration.
   if (e && !e->mDuration.IsPositive()) {
-    mp4parse_fragment_info info;
+    Mp4parseFragmentInfo info;
     auto rv = mp4parse_get_fragment_info(mParser.get(), &info);
-    if (rv == mp4parse_status_OK) {
+    if (rv == MP4PARSE_STATUS_OK) {
       e->mDuration = TimeUnit::FromMicroseconds(info.fragment_duration);
     }
   }
 
   if (e && e->IsValid()) {
     return {NS_OK, Move(e)};
   }
   MOZ_LOG(gMP4MetadataLog, LogLevel::Debug, ("TrackInfo didn't validate"));
@@ -341,31 +341,31 @@ MP4Metadata::ResultAndCryptoFile
 MP4Metadata::Crypto() const
 {
   return {NS_OK, &mCrypto};
 }
 
 MP4Metadata::ResultAndIndice
 MP4Metadata::GetTrackIndice(mozilla::TrackID aTrackID)
 {
-  mp4parse_byte_data indiceRawData = {};
+  Mp4parseByteData indiceRawData = {};
 
   uint8_t fragmented = false;
   auto rv = mp4parse_is_fragmented(mParser.get(), aTrackID, &fragmented);
-  if (rv != mp4parse_status_OK) {
+  if (rv != MP4PARSE_STATUS_OK) {
     return {MediaResult(NS_ERROR_DOM_MEDIA_METADATA_ERR,
                         RESULT_DETAIL("Cannot parse whether track id %d is "
                                       "fragmented, mp4parse_error=%d",
                                       int(aTrackID), int(rv))),
             nullptr};
   }
 
   if (!fragmented) {
     rv = mp4parse_get_indice_table(mParser.get(), aTrackID, &indiceRawData);
-    if (rv != mp4parse_status_OK) {
+    if (rv != MP4PARSE_STATUS_OK) {
       return {MediaResult(NS_ERROR_DOM_MEDIA_METADATA_ERR,
                           RESULT_DETAIL("Cannot parse index table in track id %d, "
                                         "mp4parse_error=%d",
                                         int(aTrackID), int(rv))),
               nullptr};
     }
   }
 
diff --git a/dom/media/mp4/MP4Metadata.h b/dom/media/mp4/MP4Metadata.h
--- a/dom/media/mp4/MP4Metadata.h
+++ b/dom/media/mp4/MP4Metadata.h
@@ -21,23 +21,23 @@ namespace mozilla {
 // class SHOULD NOT longer than rust parser.
 class IndiceWrapper
 {
 public:
   size_t Length() const;
 
   bool GetIndice(size_t aIndex, Index::Indice& aIndice) const;
 
-  explicit IndiceWrapper(mp4parse_byte_data& aRustIndice);
+  explicit IndiceWrapper(Mp4parseByteData& aRustIndice);
 
 protected:
-  mp4parse_byte_data mIndice;
+  Mp4parseByteData mIndice;
 };
 
-struct FreeMP4Parser { void operator()(mp4parse_parser* aPtr) { mp4parse_free(aPtr); } };
+struct FreeMP4Parser { void operator()(Mp4parseParser* aPtr) { mp4parse_free(aPtr); } };
 
 // Wrap an Stream to remember the read offset.
 class StreamAdaptor {
 public:
   explicit StreamAdaptor(ByteStream* aSource)
     : mSource(aSource)
     , mOffset(0)
   {
@@ -105,14 +105,14 @@ public:
 
 private:
   void UpdateCrypto();
   Maybe<uint32_t> TrackTypeToGlobalTrackIndex(mozilla::TrackInfo::TrackType aType, size_t aTrackNumber) const;
 
   CryptoFile mCrypto;
   RefPtr<ByteStream> mSource;
   StreamAdaptor mSourceAdaptor;
-  mozilla::UniquePtr<mp4parse_parser, FreeMP4Parser> mParser;
+  mozilla::UniquePtr<Mp4parseParser, FreeMP4Parser> mParser;
 };
 
 } // namespace mozilla
 
 #endif // MP4METADATA_H_
