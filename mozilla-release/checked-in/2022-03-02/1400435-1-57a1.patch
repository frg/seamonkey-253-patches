# HG changeset patch
# User Bobby Holley <bobbyholley@gmail.com>
# Date 1505784389 25200
#      Mon Sep 18 18:26:29 2017 -0700
# Node ID e5bd13138738491c93fe3b1351f670b7202050c3
# Parent  38744471d4388b9460693ba37c0ea34ba58b215d
Bug 1400435 - Use a more precise check in the nsCSSValue destructor. r=xidorn

MozReview-Commit-ID: KFdgtxyOZ01

diff --git a/layout/style/ServoBindingList.h b/layout/style/ServoBindingList.h
--- a/layout/style/ServoBindingList.h
+++ b/layout/style/ServoBindingList.h
@@ -610,16 +610,19 @@ SERVO_BINDING_FUNC(Servo_TraverseSubtree
                    RawGeckoElementBorrowed root,
                    RawServoStyleSetBorrowed set,
                    const mozilla::ServoElementSnapshotTable* snapshots,
                    mozilla::ServoTraversalFlags flags)
 
 // Assert that the tree has no pending or unconsumed restyles.
 SERVO_BINDING_FUNC(Servo_AssertTreeIsClean, void, RawGeckoElementBorrowed root)
 
+// Returns true if the current thread is a Servo parallel worker thread.
+SERVO_BINDING_FUNC(Servo_IsWorkerThread, bool, )
+
 // Checks whether the rule tree has crossed its threshold for unused rule nodes,
 // and if so, frees them.
 SERVO_BINDING_FUNC(Servo_MaybeGCRuleTree, void, RawServoStyleSetBorrowed set)
 
 // Returns computed values for the given element without any animations rules.
 SERVO_BINDING_FUNC(Servo_StyleSet_GetBaseComputedValuesForElement,
                    ServoStyleContextStrong,
                    RawServoStyleSetBorrowed set,
diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -35,16 +35,24 @@
 #include "nsStyleSet.h"
 #include "gfxUserFontSet.h"
 
 using namespace mozilla;
 using namespace mozilla::dom;
 
 ServoStyleSet* ServoStyleSet::sInServoTraversal = nullptr;
 
+#ifdef DEBUG
+bool
+ServoStyleSet::IsCurrentThreadInServoTraversal()
+{
+  return sInServoTraversal && (NS_IsMainThread() || Servo_IsWorkerThread());
+}
+#endif
+
 namespace mozilla {
 // On construction, sets sInServoTraversal to the given ServoStyleSet.
 // On destruction, clears sInServoTraversal and calls RunPostTraversalTasks.
 class MOZ_RAII AutoSetInServoTraversal
 {
 public:
   explicit AutoSetInServoTraversal(ServoStyleSet* aSet)
     : mSet(aSet)
diff --git a/layout/style/ServoStyleSet.h b/layout/style/ServoStyleSet.h
--- a/layout/style/ServoStyleSet.h
+++ b/layout/style/ServoStyleSet.h
@@ -92,16 +92,23 @@ public:
     // maintain this static boolean. However, the danger is that those callers
     // are generally unprepared to deal with non-Servo-but-also-non-main-thread
     // callers, and are likely to take the main-thread codepath if this function
     // returns false. So we assert against other non-main-thread callers here.
     MOZ_ASSERT(sInServoTraversal || NS_IsMainThread());
     return sInServoTraversal;
   }
 
+#ifdef DEBUG
+  // Used for debug assertions. We make this debug-only to prevent callers from
+  // accidentally using it instead of IsInServoTraversal, which is cheaper. We
+  // can change this if a use-case arises.
+  static bool IsCurrentThreadInServoTraversal();
+#endif
+
   static ServoStyleSet* Current()
   {
     return sInServoTraversal;
   }
 
   // The kind of styleset we have.
   //
   // We use ServoStyleSet also from XBL bindings, and some stuff needs to be
diff --git a/layout/style/nsCSSValue.cpp b/layout/style/nsCSSValue.cpp
--- a/layout/style/nsCSSValue.cpp
+++ b/layout/style/nsCSSValue.cpp
@@ -412,20 +412,21 @@ nscoord nsCSSValue::GetPixelLength() con
   }
   return nsPresContext::CSSPixelsToAppUnits(float(mValue.mFloat*scaleFactor));
 }
 
 // Assert against resetting non-trivial CSS values from the parallel Servo
 // traversal, since the refcounts aren't thread-safe.
 // Note that the caller might be an OMTA thread, which is allowed to operate off
 // main thread because it owns all of the corresponding nsCSSValues and any that
-// they might be sharing members with.
-#define DO_RELEASE(member) {                                                     \
-  MOZ_ASSERT(NS_IsInCompositorThread() || !ServoStyleSet::IsInServoTraversal()); \
-  mValue.member->Release();                                                      \
+// they might be sharing members with. Since this can happen concurrently with
+// the servo traversal, we have to use a more-precise (but slower) test.
+#define DO_RELEASE(member) {                                     \
+  MOZ_ASSERT(!ServoStyleSet::IsCurrentThreadInServoTraversal()); \
+  mValue.member->Release();                                      \
 }
 
 void nsCSSValue::DoReset()
 {
   if (UnitHasStringValue()) {
     mValue.mString->Release();
   } else if (IsFloatColorUnit()) {
     DO_RELEASE(mFloatColor);
