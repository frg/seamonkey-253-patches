# HG changeset patch
# User Aaron Klotz <aklotz@mozilla.com>
# Date 1503420492 21600
#      Tue Aug 22 10:48:12 2017 -0600
# Node ID 7ca1a9de85b58c032fb15b2ea8ecadedacdcdfbe
# Parent  5e9673299f816e901e10e15c582d9a9b1ce95c66
Bug 1392681: Move IStream creation out of mscom::ProxyStream into its own utility functions; r=jimm

MozReview-Commit-ID: EucWtw0YeBI

diff --git a/ipc/mscom/Objref.cpp b/ipc/mscom/Objref.cpp
--- a/ipc/mscom/Objref.cpp
+++ b/ipc/mscom/Objref.cpp
@@ -3,20 +3,22 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/mscom/Objref.h"
 
 #include "mozilla/Assertions.h"
 #include "mozilla/mscom/Utils.h"
+#include "mozilla/RefPtr.h"
 #include "mozilla/UniquePtr.h"
 
 #include <guiddef.h>
 #include <objidl.h>
+#include <winnt.h>
 
 namespace {
 
 #pragma pack(push, 1)
 
 typedef uint64_t OID;
 typedef uint64_t OXID;
 typedef GUID IPID;
@@ -51,37 +53,60 @@ struct DUALSTRINGARRAY
 
   uint16_t  mNumEntries;
   uint16_t  mSecurityOffset;
   uint16_t  mStringArray[1]; // Length is mNumEntries
 };
 
 struct OBJREF_STANDARD
 {
+  static size_t SizeOfFixedLenHeader()
+  {
+    return sizeof(mStd);
+  }
+
   size_t SizeOf() const
   {
-    return sizeof(mStd) + mResAddr.SizeOf();
+    return SizeOfFixedLenHeader() + mResAddr.SizeOf();
   }
 
   STDOBJREF       mStd;
   DUALSTRINGARRAY mResAddr;
 };
 
 struct OBJREF_HANDLER
 {
+  static size_t SizeOfFixedLenHeader()
+  {
+    return sizeof(mStd) + sizeof(mClsid);
+  }
+
   size_t SizeOf() const
   {
-    return sizeof(mStd) + sizeof(mClsid) + mResAddr.SizeOf();
+    return SizeOfFixedLenHeader() + mResAddr.SizeOf();
   }
 
   STDOBJREF       mStd;
   CLSID           mClsid;
   DUALSTRINGARRAY mResAddr;
 };
 
+struct OBJREF_CUSTOM
+{
+  static size_t SizeOfFixedLenHeader()
+  {
+    return sizeof(mClsid) + sizeof(mCbExtension) + sizeof(mReserved);
+  }
+
+  CLSID           mClsid;
+  uint32_t        mCbExtension;
+  uint32_t        mReserved;
+  uint8_t         mPayload[1];
+};
+
 enum OBJREF_FLAGS
 {
   OBJREF_TYPE_STANDARD = 0x00000001UL,
   OBJREF_TYPE_HANDLER = 0x00000002UL,
   OBJREF_TYPE_CUSTOM = 0x00000004UL,
   OBJREF_TYPE_EXTENDED = 0x00000008UL,
 };
 
@@ -107,16 +132,17 @@ struct OBJREF
   }
 
   uint32_t  mSignature;
   uint32_t  mFlags;
   IID       mIid;
   union {
     OBJREF_STANDARD mObjRefStd;
     OBJREF_HANDLER  mObjRefHandler;
+    OBJREF_CUSTOM   mObjRefCustom;
     // There are others but we're not supporting them here
   };
 };
 
 enum OBJREF_SIGNATURES
 {
   OBJREF_SIGNATURE = 0x574F454DUL
 };
@@ -178,17 +204,17 @@ StripHandlerFromOBJREF(NotNull<IStream*>
   if (FAILED(hr) || bytesRead != sizeof(iid) || !IsValidGUID(iid)) {
     return false;
   }
 
   // Seek past fixed-size STDOBJREF and CLSID
   seekTo.QuadPart = sizeof(STDOBJREF) + sizeof(CLSID);
   hr = aStream->Seek(seekTo, STREAM_SEEK_CUR, nullptr);
   if (FAILED(hr)) {
-    return hr;
+    return false;
   }
 
   uint16_t numEntries;
   hr = aStream->Read(&numEntries, sizeof(numEntries), &bytesRead);
   if (FAILED(hr) || bytesRead != sizeof(numEntries)) {
     return false;
   }
 
@@ -254,10 +280,113 @@ StripHandlerFromOBJREF(NotNull<IStream*>
     return false;
   }
 
   // Back up to just before the zeros we just wrote
   seekTo.QuadPart = -static_cast<int64_t>(sizeof(CLSID));
   return SUCCEEDED(aStream->Seek(seekTo, STREAM_SEEK_CUR, nullptr));
 }
 
+uint32_t
+GetOBJREFSize(NotNull<IStream*> aStream)
+{
+  // Make a clone so that we don't manipulate aStream's seek pointer
+  RefPtr<IStream> cloned;
+  HRESULT hr = aStream->Clone(getter_AddRefs(cloned));
+  if (FAILED(hr)) {
+    return 0;
+  }
+
+  uint32_t accumulatedSize = 0;
+
+  ULONG bytesRead;
+
+  uint32_t signature;
+  hr = cloned->Read(&signature, sizeof(signature), &bytesRead);
+  if (FAILED(hr) || bytesRead != sizeof(signature) ||
+      signature != OBJREF_SIGNATURE) {
+    return 0;
+  }
+
+  accumulatedSize += bytesRead;
+
+  uint32_t type;
+  hr = cloned->Read(&type, sizeof(type), &bytesRead);
+  if (FAILED(hr) || bytesRead != sizeof(type)) {
+    return 0;
+  }
+
+  accumulatedSize += bytesRead;
+
+  IID iid;
+  hr = cloned->Read(&iid, sizeof(iid), &bytesRead);
+  if (FAILED(hr) || bytesRead != sizeof(iid) || !IsValidGUID(iid)) {
+    return 0;
+  }
+
+  accumulatedSize += bytesRead;
+
+  LARGE_INTEGER seekTo;
+
+  if (type == OBJREF_TYPE_CUSTOM) {
+    CLSID clsid;
+    hr = cloned->Read(&clsid, sizeof(CLSID), &bytesRead);
+    if (FAILED(hr) || bytesRead != sizeof(CLSID)) {
+      return 0;
+    }
+
+    MOZ_ASSERT(clsid == CLSID_StdMarshal);
+    if (clsid != CLSID_StdMarshal) {
+      // We can only calulate the size if the payload is a standard OBJREF as
+      // identified by clsid == CLSID_StdMarshal.
+      return 0;
+    }
+
+    accumulatedSize += bytesRead;
+
+    seekTo.QuadPart = sizeof(OBJREF_CUSTOM::mCbExtension) +
+                      sizeof(OBJREF_CUSTOM::mReserved);
+    hr = cloned->Seek(seekTo, STREAM_SEEK_CUR, nullptr);
+    if (FAILED(hr)) {
+      return 0;
+    }
+
+    accumulatedSize += seekTo.LowPart;
+
+    uint32_t payloadLen = GetOBJREFSize(WrapNotNull(cloned.get()));
+    if (!payloadLen) {
+      return 0;
+    }
+
+    accumulatedSize += payloadLen;
+    return accumulatedSize;
+  }
+
+  switch (type) {
+    case OBJREF_TYPE_STANDARD:
+      seekTo.QuadPart = OBJREF_STANDARD::SizeOfFixedLenHeader();
+      break;
+    case OBJREF_TYPE_HANDLER:
+      seekTo.QuadPart = OBJREF_HANDLER::SizeOfFixedLenHeader();
+      break;
+    default:
+      return 0;
+  }
+
+  hr = cloned->Seek(seekTo, STREAM_SEEK_CUR, nullptr);
+  if (FAILED(hr)) {
+    return 0;
+  }
+
+  accumulatedSize += seekTo.LowPart;
+
+  uint16_t numEntries;
+  hr = cloned->Read(&numEntries, sizeof(numEntries), &bytesRead);
+  if (FAILED(hr) || bytesRead != sizeof(numEntries)) {
+    return 0;
+  }
+
+  accumulatedSize += DUALSTRINGARRAY::SizeFromNumEntries(numEntries);
+  return accumulatedSize;
+}
+
 } // namespace mscom
 } // namespace mozilla
diff --git a/ipc/mscom/Objref.h b/ipc/mscom/Objref.h
--- a/ipc/mscom/Objref.h
+++ b/ipc/mscom/Objref.h
@@ -24,13 +24,23 @@ namespace mscom {
  * @param aEnd    Absolute position of the end of the OBJREF.
  * @return true if the handler was successfully stripped, otherwise false.
  */
 bool
 StripHandlerFromOBJREF(NotNull<IStream*> aStream,
                        const uint64_t aStart,
                        const uint64_t aEnd);
 
+/**
+ * Given a buffer containing a serialized proxy to an interface, this function
+ * returns the length of the serialized data.
+ * @param aStream IStream containing a serialized proxy. The stream pointer
+ *                must be positioned at the beginning of the OBJREF.
+ * @return The size of the serialized proxy, or 0 on error.
+ */
+uint32_t
+GetOBJREFSize(NotNull<IStream*> aStream);
+
 } // namespace mscom
 } // namespace mozilla
 
 #endif // mozilla_mscom_Objref_h
 
diff --git a/ipc/mscom/ProxyStream.cpp b/ipc/mscom/ProxyStream.cpp
--- a/ipc/mscom/ProxyStream.cpp
+++ b/ipc/mscom/ProxyStream.cpp
@@ -3,17 +3,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/Move.h"
 #include "mozilla/mscom/EnsureMTA.h"
 #include "mozilla/mscom/ProxyStream.h"
 #include "mozilla/mscom/Utils.h"
-#include "mozilla/WindowsVersion.h"
 
 #if defined(MOZ_CRASHREPORTER)
 #include "InterfaceRegistrationAnnotator.h"
 #include "nsExceptionHandler.h"
 #include "nsPrintfCString.h"
 #endif
 
 #include <windows.h>
@@ -29,25 +28,34 @@ ProxyStream::ProxyStream()
   , mBufSize(0)
 {
 }
 
 // GetBuffer() fails with this variant, but that's okay because we're just
 // reconstructing the stream from a buffer anyway.
 ProxyStream::ProxyStream(REFIID aIID, const BYTE* aInitBuf,
                          const int aInitBufSize)
-  : mStream(InitStream(aInitBuf, static_cast<const UINT>(aInitBufSize)))
-  , mGlobalLockedBuf(nullptr)
+  : mGlobalLockedBuf(nullptr)
   , mHGlobal(nullptr)
   , mBufSize(aInitBufSize)
 {
 #if defined(MOZ_CRASHREPORTER)
   NS_NAMED_LITERAL_CSTRING(kCrashReportKey, "ProxyStreamUnmarshalStatus");
 #endif
 
+  HRESULT createStreamResult = CreateStream(aInitBuf, aInitBufSize,
+                                            getter_AddRefs(mStream));
+  if (FAILED(createStreamResult)) {
+#if defined(MOZ_CRASHREPORTER)
+    nsPrintfCString hrAsStr("0x%08X", createStreamResult);
+    CrashReporter::AnnotateCrashReport(kCrashReportKey, hrAsStr);
+#endif // defined(MOZ_CRASHREPORTER)
+    return;
+  }
+
   if (!aInitBufSize) {
 #if defined(MOZ_CRASHREPORTER)
     CrashReporter::AnnotateCrashReport(kCrashReportKey,
                                        NS_LITERAL_CSTRING("!aInitBufSize"));
 #endif // defined(MOZ_CRASHREPORTER)
     // We marshaled a nullptr. Nothing else to do here.
     return;
   }
@@ -93,77 +101,16 @@ ProxyStream::ProxyStream(REFIID aIID, co
     nsPrintfCString hrAsStr("0x%08X", unmarshalResult);
     CrashReporter::AnnotateCrashReport(
         NS_LITERAL_CSTRING("CoGetInterfaceAndReleaseStreamFailure"), hrAsStr);
     AnnotateInterfaceRegistration(aIID);
   }
 #endif // defined(MOZ_CRASHREPORTER)
 }
 
-/* static */
-already_AddRefed<IStream>
-ProxyStream::InitStream(const BYTE* aInitBuf, const UINT aInitBufSize)
-{
-  if (!aInitBuf || !aInitBufSize) {
-    return nullptr;
-  }
-
-  HRESULT hr;
-  RefPtr<IStream> stream;
-
-  if (IsWin8OrLater()) {
-    // This function is not safe for us to use until Windows 8
-    stream = already_AddRefed<IStream>(::SHCreateMemStream(aInitBuf, aInitBufSize));
-    if (!stream) {
-      return nullptr;
-    }
-  } else {
-    HGLOBAL hglobal = ::GlobalAlloc(GMEM_MOVEABLE, aInitBufSize);
-    if (!hglobal) {
-      return nullptr;
-    }
-
-    // stream takes ownership of hglobal if this call is successful
-    hr = ::CreateStreamOnHGlobal(hglobal, TRUE, getter_AddRefs(stream));
-    if (FAILED(hr)) {
-      ::GlobalFree(hglobal);
-      return nullptr;
-    }
-
-    // The default stream size is derived from ::GlobalSize(hglobal), which due
-    // to rounding may be larger than aInitBufSize. We forcibly set the correct
-    // stream size here.
-    ULARGE_INTEGER streamSize;
-    streamSize.QuadPart = aInitBufSize;
-    hr = stream->SetSize(streamSize);
-    if (FAILED(hr)) {
-      return nullptr;
-    }
-
-    void* streamBuf = ::GlobalLock(hglobal);
-    if (!streamBuf) {
-      return nullptr;
-    }
-
-    memcpy(streamBuf, aInitBuf, aInitBufSize);
-
-    ::GlobalUnlock(hglobal);
-  }
-
-  // Ensure that the stream is rewound
-  LARGE_INTEGER streamOffset;
-  streamOffset.QuadPart = 0;
-  hr = stream->Seek(streamOffset, STREAM_SEEK_SET, nullptr);
-  if (FAILED(hr)) {
-    return nullptr;
-  }
-
-  return stream.forget();
-}
-
 ProxyStream::ProxyStream(ProxyStream&& aOther)
   : mGlobalLockedBuf(nullptr)
   , mHGlobal(nullptr)
   , mBufSize(0)
 {
   *this = mozilla::Move(aOther);
 }
 
diff --git a/ipc/mscom/ProxyStream.h b/ipc/mscom/ProxyStream.h
--- a/ipc/mscom/ProxyStream.h
+++ b/ipc/mscom/ProxyStream.h
@@ -42,20 +42,16 @@ public:
   RefPtr<IStream> GetStream() const;
 
   bool operator==(const ProxyStream& aOther) const
   {
     return this == &aOther;
   }
 
 private:
-  static already_AddRefed<IStream> InitStream(const BYTE* aInitBuf,
-                                              const UINT aInitBufSize);
-
-private:
   RefPtr<IStream> mStream;
   BYTE*           mGlobalLockedBuf;
   HGLOBAL         mHGlobal;
   int             mBufSize;
   ProxyUniquePtr<IUnknown> mUnmarshaledProxy;
 };
 
 } // namespace mscom
diff --git a/ipc/mscom/Utils.cpp b/ipc/mscom/Utils.cpp
--- a/ipc/mscom/Utils.cpp
+++ b/ipc/mscom/Utils.cpp
@@ -6,21 +6,24 @@
 
 #if defined(ACCESSIBILITY)
 #include "mozilla/mscom/Registration.h"
 #if defined(MOZILLA_INTERNAL_API)
 #include "nsTArray.h"
 #endif
 #endif
 
+#include "mozilla/mscom/Objref.h"
 #include "mozilla/mscom/Utils.h"
 #include "mozilla/RefPtr.h"
+#include "mozilla/WindowsVersion.h"
 
 #include <objbase.h>
 #include <objidl.h>
+#include <shlwapi.h>
 #include <winnt.h>
 
 #if defined(_MSC_VER)
 extern "C" IMAGE_DOS_HEADER __ImageBase;
 #endif
 
 namespace mozilla {
 namespace mscom {
@@ -93,16 +96,136 @@ GetContainingModuleHandle()
                          reinterpret_cast<LPCTSTR>(&GetContainingModuleHandle),
                          &thisModule)) {
     return 0;
   }
 #endif
   return reinterpret_cast<uintptr_t>(thisModule);
 }
 
+uint32_t
+CreateStream(const uint8_t* aInitBuf, const uint32_t aInitBufSize,
+             IStream** aOutStream)
+{
+  if (!aInitBufSize || !aOutStream) {
+    return E_INVALIDARG;
+  }
+
+  *aOutStream = nullptr;
+
+  HRESULT hr;
+  RefPtr<IStream> stream;
+
+  if (IsWin8OrLater()) {
+    // SHCreateMemStream is not safe for us to use until Windows 8. On older
+    // versions of Windows it is not thread-safe and it creates IStreams that do
+    // not support the full IStream API.
+
+    // If aInitBuf is null then initSize must be 0.
+    UINT initSize = aInitBuf ? aInitBufSize : 0;
+    stream = already_AddRefed<IStream>(::SHCreateMemStream(aInitBuf, initSize));
+    if (!stream) {
+      return E_OUTOFMEMORY;
+    }
+
+    if (!aInitBuf) {
+      // Now we'll set the required size
+      ULARGE_INTEGER newSize;
+      newSize.QuadPart = aInitBufSize;
+      hr = stream->SetSize(newSize);
+      if (FAILED(hr)) {
+        return hr;
+      }
+    }
+  } else {
+    HGLOBAL hglobal = ::GlobalAlloc(GMEM_MOVEABLE, aInitBufSize);
+    if (!hglobal) {
+      return HRESULT_FROM_WIN32(::GetLastError());
+    }
+
+    // stream takes ownership of hglobal if this call is successful
+    hr = ::CreateStreamOnHGlobal(hglobal, TRUE, getter_AddRefs(stream));
+    if (FAILED(hr)) {
+      ::GlobalFree(hglobal);
+      return hr;
+    }
+
+    // The default stream size is derived from ::GlobalSize(hglobal), which due
+    // to rounding may be larger than aInitBufSize. We forcibly set the correct
+    // stream size here.
+    ULARGE_INTEGER streamSize;
+    streamSize.QuadPart = aInitBufSize;
+    hr = stream->SetSize(streamSize);
+    if (FAILED(hr)) {
+      return hr;
+    }
+
+    if (aInitBuf) {
+      ULONG bytesWritten;
+      hr = stream->Write(aInitBuf, aInitBufSize, &bytesWritten);
+      if (FAILED(hr)) {
+        return hr;
+      }
+
+      if (bytesWritten != aInitBufSize) {
+        return E_UNEXPECTED;
+      }
+    }
+  }
+
+  // Ensure that the stream is rewound
+  LARGE_INTEGER streamOffset;
+  streamOffset.QuadPart = 0LL;
+  hr = stream->Seek(streamOffset, STREAM_SEEK_SET, nullptr);
+  if (FAILED(hr)) {
+    return hr;
+  }
+
+  stream.forget(aOutStream);
+  return S_OK;
+}
+
+uint32_t
+CopySerializedProxy(IStream* aInStream, IStream** aOutStream)
+{
+  if (!aInStream || !aOutStream) {
+    return E_INVALIDARG;
+  }
+
+  *aOutStream = nullptr;
+
+  uint32_t desiredStreamSize = GetOBJREFSize(WrapNotNull(aInStream));
+  if (!desiredStreamSize) {
+    return E_INVALIDARG;
+  }
+
+  RefPtr<IStream> stream;
+  HRESULT hr = CreateStream(nullptr, desiredStreamSize, getter_AddRefs(stream));
+  if (FAILED(hr)) {
+    return hr;
+  }
+
+  ULARGE_INTEGER numBytesToCopy;
+  numBytesToCopy.QuadPart = desiredStreamSize;
+  hr = aInStream->CopyTo(stream, numBytesToCopy, nullptr, nullptr);
+  if (FAILED(hr)) {
+    return hr;
+  }
+
+  LARGE_INTEGER seekTo;
+  seekTo.QuadPart = 0LL;
+  hr = stream->Seek(seekTo, STREAM_SEEK_SET, nullptr);
+  if (FAILED(hr)) {
+    return hr;
+  }
+
+  stream.forget(aOutStream);
+  return S_OK;
+}
+
 #if defined(MOZILLA_INTERNAL_API)
 
 void
 GUIDToString(REFGUID aGuid, nsAString& aOutString)
 {
   // This buffer length is long enough to hold a GUID string that is formatted
   // to include curly braces and dashes.
   const int kBufLenWithNul = 39;
diff --git a/ipc/mscom/Utils.h b/ipc/mscom/Utils.h
--- a/ipc/mscom/Utils.h
+++ b/ipc/mscom/Utils.h
@@ -9,26 +9,49 @@
 
 #if defined(MOZILLA_INTERNAL_API)
 #include "nsString.h"
 #endif // defined(MOZILLA_INTERNAL_API)
 
 #include "mozilla/Attributes.h"
 #include <guiddef.h>
 
+struct IStream;
 struct IUnknown;
 
 namespace mozilla {
 namespace mscom {
 
 bool IsCurrentThreadMTA();
 bool IsProxy(IUnknown* aUnknown);
 bool IsValidGUID(REFGUID aCheckGuid);
 uintptr_t GetContainingModuleHandle();
 
+/**
+ * Given a buffer, create a new IStream object.
+ * @param aBuf Buffer containing data to initialize the stream. This parameter
+ *             may be nullptr, causing the stream to be created with aBufLen
+ *             bytes of uninitialized data.
+ * @param aBufLen Length of data in aBuf, or desired stream size if aBuf is
+ *                nullptr.
+ * @param aOutStream Outparam to receive the newly created stream.
+ * @return HRESULT error code.
+ */
+uint32_t CreateStream(const uint8_t* aBuf, const uint32_t aBufLen,
+                      IStream** aOutStream);
+
+/**
+ * Creates a deep copy of a proxy contained in a stream.
+ * @param aInStream Stream containing the proxy to copy. Its seek pointer must
+ *                  be positioned to point at the beginning of the proxy data.
+ * @param aOutStream Outparam to receive the newly created stream.
+ * @return HRESULT error code.
+ */
+uint32_t CopySerializedProxy(IStream* aInStream, IStream** aOutStream);
+
 #if defined(MOZILLA_INTERNAL_API)
 void GUIDToString(REFGUID aGuid, nsAString& aOutString);
 #endif // defined(MOZILLA_INTERNAL_API)
 
 #if defined(ACCESSIBILITY)
 bool IsVtableIndexFromParentInterface(REFIID aInterface,
                                       unsigned long aVtableIndex);
 
