# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1501753643 -28800
# Node ID e1710efb9b709d69b8e53c26262390702873cce8
# Parent  3644f6f7889cbd3b0586f7513096e6019d7d6ae6
Bug 1383628. P2 - move the call to Load() into Clone(). r=gerald

MozReview-Commit-ID: 9k8BXLwAepI

diff --git a/dom/html/HTMLMediaElement.cpp b/dom/html/HTMLMediaElement.cpp
--- a/dom/html/HTMLMediaElement.cpp
+++ b/dom/html/HTMLMediaElement.cpp
@@ -4719,44 +4719,32 @@ HTMLMediaElement::CanPlayType(const nsAS
 
 nsresult
 HTMLMediaElement::InitializeDecoderAsClone(ChannelMediaDecoder* aOriginal)
 {
   NS_ASSERTION(mLoadingSrc, "mLoadingSrc must already be set");
   NS_ASSERTION(mDecoder == nullptr, "Shouldn't have a decoder");
   MOZ_DIAGNOSTIC_ASSERT(mReadyState == nsIDOMHTMLMediaElement::HAVE_NOTHING);
 
-  MediaResource* originalResource = aOriginal->GetResource();
-  if (!originalResource)
-    return NS_ERROR_FAILURE;
-
   MediaDecoderInit decoderInit(this,
                                mMuted ? 0.0 : mVolume,
                                mPreservesPitch,
                                mPlaybackRate,
                                mPreloadAction ==
                                  HTMLMediaElement::PRELOAD_METADATA,
                                mHasSuspendTaint,
                                HasAttr(kNameSpaceID_None, nsGkAtoms::loop),
                                aOriginal->ContainerType());
 
   RefPtr<ChannelMediaDecoder> decoder = aOriginal->Clone(decoderInit);
   if (!decoder)
     return NS_ERROR_FAILURE;
 
   LOG(LogLevel::Debug, ("%p Cloned decoder %p from %p", this, decoder.get(), aOriginal));
 
-  nsresult rv = decoder->Load(originalResource);
-  if (NS_FAILED(rv)) {
-    decoder->Shutdown();
-    LOG(LogLevel::Debug,
-        ("%p Failed to load for decoder %p", this, decoder.get()));
-    return rv;
-  }
-
   return FinishDecoderSetup(decoder);
 }
 
 template<typename DecoderType, typename... LoadArgs>
 nsresult
 HTMLMediaElement::SetupDecoder(DecoderType* aDecoder, LoadArgs&&... aArgs)
 {
   LOG(LogLevel::Debug,
diff --git a/dom/media/ChannelMediaDecoder.cpp b/dom/media/ChannelMediaDecoder.cpp
--- a/dom/media/ChannelMediaDecoder.cpp
+++ b/dom/media/ChannelMediaDecoder.cpp
@@ -153,17 +153,28 @@ ChannelMediaDecoder::ChannelMediaDecoder
   , mResourceCallback(new ResourceCallback(aInit.mOwner->AbstractMainThread()))
 {
   mResourceCallback->Connect(this);
 }
 
 already_AddRefed<ChannelMediaDecoder>
 ChannelMediaDecoder::Clone(MediaDecoderInit& aInit)
 {
+  if (!mResource) {
+    return nullptr;
+  }
   RefPtr<ChannelMediaDecoder> decoder = CloneImpl(aInit);
+  if (!decoder) {
+    return nullptr;
+  }
+  nsresult rv = decoder->Load(mResource);
+  if (NS_FAILED(rv)) {
+    decoder->Shutdown();
+    return nullptr;
+  }
   return decoder.forget();
 }
 
 MediaResource*
 ChannelMediaDecoder::GetResource() const
 {
   return mResource;
 }
diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -65,18 +65,18 @@ public:
   void Shutdown() override;
 
   // Create a new decoder of the same type as this one.
   already_AddRefed<ChannelMediaDecoder> Clone(MediaDecoderInit& aInit);
 
   virtual nsresult Load(nsIChannel* aChannel,
                         bool aIsPrivateBrowsing,
                         nsIStreamListener** aStreamListener);
-  virtual nsresult Load(MediaResource* aOriginal);
 
 private:
   virtual ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) = 0;
   nsresult OpenResource(nsIStreamListener** aStreamListener);
+  virtual nsresult Load(MediaResource* aOriginal);
 };
 
 } // namespace mozilla
 
 #endif // ChannelMediaDecoder_h_
