# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1504034338 18000
#      Tue Aug 29 14:18:58 2017 -0500
# Node ID 82febb01ec1d779da429a6b6c1f67d89ea943b58
# Parent  3f5e9d3161956600260f023aff93b201d2304583
servo: Merge #18268 - style: Recascade the document instead of using the dirty_on_viewport_size_change bit (from emilio:dirty-viewport-followup); r=SimonSapin

This allows us to simplify a lot of code.

On top of #18267.

Source-Repo: https://github.com/servo/servo
Source-Revision: 473934c989c94773c38f585b1c4500cd8c811738

diff --git a/servo/components/style/animation.rs b/servo/components/style/animation.rs
--- a/servo/components/style/animation.rs
+++ b/servo/components/style/animation.rs
@@ -497,17 +497,16 @@ fn compute_style_for_animation_step(cont
             let computed =
                 properties::apply_declarations(context.stylist.device(),
                                                /* pseudo = */ None,
                                                previous_style.rules(),
                                                iter,
                                                Some(previous_style),
                                                Some(previous_style),
                                                Some(previous_style),
-                                               /* cascade_info = */ None,
                                                /* visited_style = */ None,
                                                font_metrics_provider,
                                                CascadeFlags::empty(),
                                                context.quirks_mode());
             computed
         }
     }
 }
diff --git a/servo/components/style/cascade_info.rs b/servo/components/style/cascade_info.rs
deleted file mode 100644
--- a/servo/components/style/cascade_info.rs
+++ /dev/null
@@ -1,95 +0,0 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-//! A structure to collect information about the cascade.
-
-#![deny(missing_docs)]
-
-use dom::TNode;
-use properties::{DeclaredValue, PropertyDeclaration};
-use style_traits::HasViewportPercentage;
-
-/// A structure to collect information about the cascade.
-///
-/// This is useful to gather information about what an element is affected by,
-/// and can be used in the future to track optimisations like when a
-/// non-inherited property is explicitly inherited, in order to cut-off the
-/// traversal.
-pub struct CascadeInfo {
-    /// Whether we've seen viewport units so far.
-    pub saw_viewport_units: bool,
-    /// Whether the cascade has been marked as finished. This is a debug-only
-    /// flag to ensure `finish` is called, given it's optional to not pass a
-    /// `CascadeInfo`.
-    #[cfg(debug_assertions)]
-    finished: bool,
-}
-
-impl CascadeInfo {
-    /// Construct a new `CascadeInfo`.
-    #[cfg(debug_assertions)]
-    pub fn new() -> Self {
-        CascadeInfo {
-            saw_viewport_units: false,
-            finished: false,
-        }
-    }
-
-    /// Construct a new `CascadeInfo`.
-    #[cfg(not(debug_assertions))]
-    pub fn new() -> Self {
-        CascadeInfo {
-            saw_viewport_units: false,
-        }
-    }
-
-    /// Called when a property is cascaded.
-    ///
-    /// NOTE: We can add a vast amount of information here.
-    #[inline]
-    pub fn on_cascade_property<T>(&mut self,
-                                  _property_declaration: &PropertyDeclaration,
-                                  value: &DeclaredValue<T>)
-        where T: HasViewportPercentage,
-    {
-        // TODO: we can be smarter and keep a property bitfield to keep track of
-        // the last applying rule.
-        if value.has_viewport_percentage() {
-            self.saw_viewport_units = true;
-        }
-    }
-
-    #[cfg(debug_assertions)]
-    fn mark_as_finished_if_appropriate(&mut self) {
-        self.finished = true;
-    }
-
-    #[cfg(not(debug_assertions))]
-    fn mark_as_finished_if_appropriate(&mut self) {}
-
-    /// Called when the cascade is finished, in order to use the information
-    /// we've collected.
-    ///
-    /// Currently used for styling to mark a node as needing restyling when the
-    /// viewport size changes.
-    #[allow(unsafe_code)]
-    pub fn finish<N: TNode>(mut self, node: &N) {
-        self.mark_as_finished_if_appropriate();
-
-        if self.saw_viewport_units {
-            unsafe {
-                node.set_dirty_on_viewport_size_changed();
-            }
-        }
-    }
-}
-
-#[cfg(debug_assertions)]
-impl Drop for CascadeInfo {
-    fn drop(&mut self) {
-        debug_assert!(self.finished,
-                      "Didn't use the result of CascadeInfo, if you don't need \
-                      it, consider passing None");
-    }
-}
diff --git a/servo/components/style/dom.rs b/servo/components/style/dom.rs
--- a/servo/components/style/dom.rs
+++ b/servo/components/style/dom.rs
@@ -133,22 +133,16 @@ pub trait TNode : Sized + Copy + Clone +
     fn opaque(&self) -> OpaqueNode;
 
     /// A debug id, only useful, mm... for debugging.
     fn debug_id(self) -> usize;
 
     /// Get this node as an element, if it's one.
     fn as_element(&self) -> Option<Self::ConcreteElement>;
 
-    /// Whether this node needs to be laid out on viewport size change.
-    fn needs_dirty_on_viewport_size_changed(&self) -> bool;
-
-    /// Mark this node as needing layout on viewport size change.
-    unsafe fn set_dirty_on_viewport_size_changed(&self);
-
     /// Whether this node can be fragmented. This is used for multicol, and only
     /// for Servo.
     fn can_be_fragmented(&self) -> bool;
 
     /// Set whether this node can be fragmented.
     unsafe fn set_can_be_fragmented(&self, value: bool);
 
     /// Whether this node is in the document right now needed to clear the
diff --git a/servo/components/style/gecko/wrapper.rs b/servo/components/style/gecko/wrapper.rs
--- a/servo/components/style/gecko/wrapper.rs
+++ b/servo/components/style/gecko/wrapper.rs
@@ -313,27 +313,16 @@ impl<'ln> TNode for GeckoNode<'ln> {
     unsafe fn set_can_be_fragmented(&self, _value: bool) {
         // FIXME(SimonSapin): Servo uses this to implement CSS multicol / fragmentation
         // Maybe this isn’t useful for Gecko?
     }
 
     fn is_in_doc(&self) -> bool {
         unsafe { bindings::Gecko_IsInDocument(self.0) }
     }
-
-    fn needs_dirty_on_viewport_size_changed(&self) -> bool {
-        // Gecko's node doesn't have the DIRTY_ON_VIEWPORT_SIZE_CHANGE flag,
-        // so we force them to be dirtied on viewport size change, regardless if
-        // they use viewport percentage size or not.
-        // TODO(shinglyu): implement this in Gecko: https://github.com/servo/servo/pull/11890
-        true
-    }
-
-    // TODO(shinglyu): implement this in Gecko: https://github.com/servo/servo/pull/11890
-    unsafe fn set_dirty_on_viewport_size_changed(&self) {}
 }
 
 /// A wrapper on top of two kind of iterators, depending on the parent being
 /// iterated.
 ///
 /// We generally iterate children by traversing the light-tree siblings of the
 /// first child like Servo does.
 ///
diff --git a/servo/components/style/lib.rs b/servo/components/style/lib.rs
--- a/servo/components/style/lib.rs
+++ b/servo/components/style/lib.rs
@@ -94,17 +94,16 @@ mod macros;
 
 #[cfg(feature = "servo")] pub mod animation;
 pub mod applicable_declarations;
 #[allow(missing_docs)] // TODO.
 #[cfg(feature = "servo")] pub mod attr;
 pub mod bezier;
 pub mod bloom;
 pub mod cache;
-pub mod cascade_info;
 pub mod context;
 pub mod counter_style;
 pub mod custom_properties;
 pub mod data;
 pub mod dom;
 pub mod driver;
 pub mod element_state;
 #[cfg(feature = "servo")] mod encoding_support;
diff --git a/servo/components/style/properties/helpers.mako.rs b/servo/components/style/properties/helpers.mako.rs
--- a/servo/components/style/properties/helpers.mako.rs
+++ b/servo/components/style/properties/helpers.mako.rs
@@ -270,18 +270,16 @@
             #[allow(unused_imports)]
             use parser::{Parse, ParserContext};
             #[allow(unused_imports)]
             use properties::{UnparsedValue, ShorthandId};
         % endif
         #[allow(unused_imports)]
         use values::{Auto, Either, None_, Normal};
         #[allow(unused_imports)]
-        use cascade_info::CascadeInfo;
-        #[allow(unused_imports)]
         use error_reporting::ParseErrorReporter;
         #[allow(unused_imports)]
         use properties::longhands;
         #[allow(unused_imports)]
         use properties::{DeclaredValue, LonghandId, LonghandIdSet};
         #[allow(unused_imports)]
         use properties::{CSSWideKeyword, ComputedValues, PropertyDeclaration};
         #[allow(unused_imports)]
@@ -298,36 +296,32 @@
         use values::{computed, generics, specified};
         #[allow(unused_imports)]
         use Atom;
         ${caller.body()}
         #[allow(unused_variables)]
         pub fn cascade_property(
             declaration: &PropertyDeclaration,
             context: &mut computed::Context,
-            cascade_info: &mut Option<<&mut CascadeInfo>,
         ) {
             let value = match *declaration {
                 PropertyDeclaration::${property.camel_case}(ref value) => {
                     DeclaredValue::Value(value)
                 },
                 PropertyDeclaration::CSSWideKeyword(id, value) => {
                     debug_assert!(id == LonghandId::${property.camel_case});
                     DeclaredValue::CSSWideKeyword(value)
                 },
                 PropertyDeclaration::WithVariables(..) => {
                     panic!("variables should already have been substituted")
                 }
                 _ => panic!("entered the wrong cascade_property() implementation"),
             };
 
             % if not property.derived_from:
-                if let Some(ref mut cascade_info) = *cascade_info {
-                    cascade_info.on_cascade_property(&declaration, &value);
-                }
                 match value {
                     DeclaredValue::Value(ref specified_value) => {
                         % if property.ident in SYSTEM_FONT_LONGHANDS and product == "gecko":
                             if let Some(sf) = specified_value.get_system() {
                                 longhands::system_font::resolve_system_font(sf, context);
                             }
                         % endif
                         % if property.is_vector:
diff --git a/servo/components/style/properties/properties.mako.rs b/servo/components/style/properties/properties.mako.rs
--- a/servo/components/style/properties/properties.mako.rs
+++ b/servo/components/style/properties/properties.mako.rs
@@ -38,17 +38,16 @@ use selectors::parser::SelectorParseErro
 use shared_lock::StylesheetGuards;
 use style_traits::{PARSING_MODE_DEFAULT, HasViewportPercentage, ToCss, ParseError};
 use style_traits::{PropertyDeclarationParseError, StyleParseError, ValueParseError};
 use stylesheets::{CssRuleType, MallocSizeOf, MallocSizeOfFn, Origin, UrlExtraData};
 #[cfg(feature = "servo")] use values::Either;
 use values::generics::text::LineHeight;
 use values::computed;
 use values::computed::NonNegativeAu;
-use cascade_info::CascadeInfo;
 use rule_tree::{CascadeLevel, StrongRuleNode};
 use self::computed_value_flags::ComputedValueFlags;
 use style_adjuster::StyleAdjuster;
 #[cfg(feature = "servo")] use values::specified::BorderStyle;
 
 pub use self::declaration_block::*;
 
 #[cfg(feature = "gecko")]
@@ -2948,19 +2947,20 @@ mod lazy_static_module {
                 flags: ComputedValueFlags::empty(),
             }
         };
     }
 }
 
 /// A per-longhand function that performs the CSS cascade for that longhand.
 pub type CascadePropertyFn =
-    extern "Rust" fn(declaration: &PropertyDeclaration,
-                     context: &mut computed::Context,
-                     cascade_info: &mut Option<<&mut CascadeInfo>);
+    extern "Rust" fn(
+        declaration: &PropertyDeclaration,
+        context: &mut computed::Context,
+    );
 
 /// A per-longhand array of functions to perform the CSS cascade on each of
 /// them, effectively doing virtual dispatch.
 static CASCADE_PROPERTY: [CascadePropertyFn; ${len(data.longhands)}] = [
     % for property in data.longhands:
         longhands::${property.ident}::cascade_property,
     % endfor
 ];
@@ -3025,17 +3025,16 @@ pub fn cascade(
     device: &Device,
     pseudo: Option<<&PseudoElement>,
     rule_node: &StrongRuleNode,
     guards: &StylesheetGuards,
     parent_style: Option<<&ComputedValues>,
     parent_style_ignoring_first_line: Option<<&ComputedValues>,
     layout_parent_style: Option<<&ComputedValues>,
     visited_style: Option<Arc<ComputedValues>>,
-    cascade_info: Option<<&mut CascadeInfo>,
     font_metrics_provider: &FontMetricsProvider,
     flags: CascadeFlags,
     quirks_mode: QuirksMode
 ) -> Arc<ComputedValues> {
     debug_assert_eq!(parent_style.is_some(), parent_style_ignoring_first_line.is_some());
     #[cfg(feature = "gecko")]
     debug_assert!(parent_style.is_none() ||
                   ptr::eq(parent_style.unwrap(),
@@ -3084,17 +3083,16 @@ pub fn cascade(
         device,
         pseudo,
         rule_node,
         iter_declarations,
         parent_style,
         parent_style_ignoring_first_line,
         layout_parent_style,
         visited_style,
-        cascade_info,
         font_metrics_provider,
         flags,
         quirks_mode,
     )
 }
 
 /// NOTE: This function expects the declaration with more priority to appear
 /// first.
@@ -3103,17 +3101,16 @@ pub fn apply_declarations<'a, F, I>(
     device: &Device,
     pseudo: Option<<&PseudoElement>,
     rules: &StrongRuleNode,
     iter_declarations: F,
     parent_style: Option<<&ComputedValues>,
     parent_style_ignoring_first_line: Option<<&ComputedValues>,
     layout_parent_style: Option<<&ComputedValues>,
     visited_style: Option<Arc<ComputedValues>>,
-    mut cascade_info: Option<<&mut CascadeInfo>,
     font_metrics_provider: &FontMetricsProvider,
     flags: CascadeFlags,
     quirks_mode: QuirksMode,
 ) -> Arc<ComputedValues>
 where
     F: Fn() -> I,
     I: Iterator<Item = (&'a PropertyDeclaration, CascadeLevel)>,
 {
@@ -3275,19 +3272,17 @@ where
                 }
                 if LonghandId::FontFamily == longhand_id {
                     font_family = Some(declaration.clone());
                     continue;
                 }
             % endif
 
             let discriminant = longhand_id as usize;
-            (CASCADE_PROPERTY[discriminant])(&*declaration,
-                                             &mut context,
-                                             &mut cascade_info);
+            (CASCADE_PROPERTY[discriminant])(&*declaration, &mut context);
         }
         % if category_to_cascade_now == "early":
             let writing_mode = get_writing_mode(context.builder.get_inheritedbox());
             context.builder.writing_mode = writing_mode;
 
             let mut _skip_font_family = false;
 
             % if product == "gecko":
@@ -3358,19 +3353,17 @@ where
             //
             // To avoid an extra iteration, we just pull out the property
             // during the early iteration and cascade them in order
             // after it.
             if !_skip_font_family {
                 if let Some(ref declaration) = font_family {
 
                     let discriminant = LonghandId::FontFamily as usize;
-                    (CASCADE_PROPERTY[discriminant])(declaration,
-                                                     &mut context,
-                                                     &mut cascade_info);
+                    (CASCADE_PROPERTY[discriminant])(declaration, &mut context);
                     % if product == "gecko":
                         let device = context.builder.device;
                         if let PropertyDeclaration::FontFamily(ref val) = **declaration {
                             if val.get_system().is_some() {
                                 let default = context.cached_system_font
                                                      .as_ref().unwrap().default_font_type;
                                 context.builder.mutate_font().fixup_system(default);
                             } else {
@@ -3378,33 +3371,29 @@ where
                             }
                         }
                     % endif
                 }
             }
 
             if let Some(ref declaration) = font_size {
                 let discriminant = LonghandId::FontSize as usize;
-                (CASCADE_PROPERTY[discriminant])(declaration,
-                                                 &mut context,
-                                                 &mut cascade_info);
+                (CASCADE_PROPERTY[discriminant])(declaration, &mut context);
             % if product == "gecko":
             // Font size must be explicitly inherited to handle lang changes and
             // scriptlevel changes.
             } else if seen.contains(LonghandId::XLang) ||
                       seen.contains(LonghandId::MozScriptLevel) ||
                       seen.contains(LonghandId::MozMinFontSizeRatio) ||
                       font_family.is_some() {
                 let discriminant = LonghandId::FontSize as usize;
                 let size = PropertyDeclaration::CSSWideKeyword(
                     LonghandId::FontSize, CSSWideKeyword::Inherit);
 
-                (CASCADE_PROPERTY[discriminant])(&size,
-                                                 &mut context,
-                                                 &mut cascade_info);
+                (CASCADE_PROPERTY[discriminant])(&size, &mut context);
             % endif
             }
         % endif
     % endfor
 
     let mut builder = context.builder;
 
     {
diff --git a/servo/components/style/style_resolver.rs b/servo/components/style/style_resolver.rs
--- a/servo/components/style/style_resolver.rs
+++ b/servo/components/style/style_resolver.rs
@@ -1,16 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Style resolution for a given element or pseudo-element.
 
 use applicable_declarations::ApplicableDeclarationList;
-use cascade_info::CascadeInfo;
 use context::{CascadeInputs, ElementCascadeInputs, StyleContext};
 use data::{ElementStyles, EagerPseudoStyles};
 use dom::TElement;
 use log::LogLevel::Trace;
 use matching::{CascadeVisitedMode, MatchMethods};
 use properties::{AnimationRules, CascadeFlags, ComputedValues};
 use properties::{IS_LINK, IS_ROOT_ELEMENT, IS_VISITED_LINK};
 use properties::{PROHIBIT_DISPLAY_CONTENTS, SKIP_ROOT_AND_ITEM_BASED_DISPLAY_FIXUP};
@@ -481,17 +480,16 @@ where
         &mut self,
         rules: Option<&StrongRuleNode>,
         style_if_visited: Option<Arc<ComputedValues>>,
         mut parent_style: Option<&ComputedValues>,
         layout_parent_style: Option<&ComputedValues>,
         cascade_visited: CascadeVisitedMode,
         pseudo: Option<&PseudoElement>,
     ) -> Arc<ComputedValues> {
-        let mut cascade_info = CascadeInfo::new();
         let mut cascade_flags = CascadeFlags::empty();
 
         if self.element.skip_root_and_item_based_display_fixup() ||
            pseudo.map_or(false, |p| p.skip_item_based_display_fixup()) {
             cascade_flags.insert(SKIP_ROOT_AND_ITEM_BASED_DISPLAY_FIXUP);
         }
 
         if pseudo.is_none() && self.element.is_link() {
@@ -525,19 +523,16 @@ where
                 self.context.shared.stylist.device(),
                 pseudo.or(implemented_pseudo.as_ref()),
                 rules.unwrap_or(self.context.shared.stylist.rule_tree().root()),
                 &self.context.shared.guards,
                 parent_style,
                 parent_style,
                 layout_parent_style,
                 style_if_visited,
-                Some(&mut cascade_info),
                 &self.context.thread_local.font_metrics_provider,
                 cascade_flags,
                 self.context.shared.quirks_mode(),
             );
 
-        cascade_info.finish(&self.element.as_node());
-
         values
     }
 }
diff --git a/servo/components/style/stylist.rs b/servo/components/style/stylist.rs
--- a/servo/components/style/stylist.rs
+++ b/servo/components/style/stylist.rs
@@ -716,17 +716,16 @@ impl Stylist {
             &self.device,
             Some(pseudo),
             &rule_node,
             guards,
             parent,
             parent,
             parent,
             None,
-            None,
             font_metrics,
             cascade_flags,
             self.quirks_mode,
         )
     }
 
     /// Returns the style for an anonymous box of the given type.
     #[cfg(feature = "servo")]
@@ -896,17 +895,16 @@ impl Stylist {
                 &self.device,
                 pseudo,
                 rule_node,
                 guards,
                 Some(inherited_style),
                 Some(inherited_style_ignoring_first_line),
                 Some(layout_parent_style_for_visited),
                 None,
-                None,
                 font_metrics,
                 cascade_flags,
                 self.quirks_mode,
             ))
         } else {
             None
         };
 
@@ -922,17 +920,16 @@ impl Stylist {
             &self.device,
             pseudo,
             rules,
             guards,
             Some(parent_style),
             Some(parent_style_ignoring_first_line),
             Some(layout_parent_style),
             visited_values,
-            None,
             font_metrics,
             cascade_flags,
             self.quirks_mode,
         )
     }
 
     fn has_rules_for_pseudo(&self, pseudo: &PseudoElement) -> bool {
         self.cascade_data
@@ -1538,17 +1535,16 @@ impl Stylist {
             &self.device,
             /* pseudo = */ None,
             &rule_node,
             guards,
             Some(parent_style),
             Some(parent_style),
             Some(parent_style),
             None,
-            None,
             &metrics,
             CascadeFlags::empty(),
             self.quirks_mode,
         )
     }
 
     /// Accessor for a shared reference to the device.
     pub fn device(&self) -> &Device {
