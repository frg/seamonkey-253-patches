# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1541104576 14400
# Node ID 5223058ac4fffd00aad34eeb29998c3264b250e8
# Parent  96783175f79a4c34ca2529f09a04ff706c482ad3
Bug 1503935 - Fix some WebP decoder implementation bugs. r=tnikkel

First we did not handle the SourceBufferIterator::WAITING state which
can happen when we get woken up but there is no data to read from the
SourceBufferIterator. StreamingLexer handled this properly by yielding
with NEED_MORE_DATA, and properly scheduling the decoder to resume. This
patch does the same in the WebP decoder.

Second nsWebPDecoder::GetType was not implemented. This meant it would
return DecoderType::UNKNOWN, and would fail to recreate the decoder if
we are discarding frames and need to restart from the beginning. In
addition to implementing that method, this patch also corrects an assert
in DecoderFactory::CloneAnimationDecoder which failed to check for WebP
as a supported animated decoder.

This patch also modestly improves the logging output and library method
checks.

Differential Revision: https://phabricator.services.mozilla.com/D10624

diff --git a/image/DecoderFactory.cpp.1503935.later b/image/DecoderFactory.cpp.1503935.later
new file mode 100644
--- /dev/null
+++ b/image/DecoderFactory.cpp.1503935.later
@@ -0,0 +1,22 @@
+--- DecoderFactory.cpp
++++ DecoderFactory.cpp
+@@ -241,17 +241,18 @@ DecoderFactory::CloneAnimationDecoder(De
+ {
+   MOZ_ASSERT(aDecoder);
+ 
+   // In an ideal world, we would assert aDecoder->HasAnimation() but we cannot.
+   // The decoder may not have detected it is animated yet (e.g. it did not even
+   // get scheduled yet, or it has only decoded the first frame and has yet to
+   // rediscover it is animated).
+   DecoderType type = aDecoder->GetType();
+-  MOZ_ASSERT(type == DecoderType::GIF || type == DecoderType::PNG,
++  MOZ_ASSERT(type == DecoderType::GIF || type == DecoderType::PNG ||
++             type == DecoderType::WEBP,
+              "Calling CloneAnimationDecoder for non-animating DecoderType");
+ 
+   RefPtr<Decoder> decoder = GetDecoder(type, nullptr, /* aIsRedecode = */ true);
+   MOZ_ASSERT(decoder, "Should have a decoder now");
+ 
+   // Initialize the decoder.
+   decoder->SetMetadataDecode(false);
+   decoder->SetIterator(aDecoder->GetSourceBuffer()->Iterator());
diff --git a/image/IDecodingTask.cpp b/image/IDecodingTask.cpp
--- a/image/IDecodingTask.cpp
+++ b/image/IDecodingTask.cpp
@@ -180,18 +180,20 @@ MetadataDecodingTask::Run()
   MOZ_ASSERT_UNREACHABLE("Metadata decode yielded for an unexpected reason");
 }
 
 
 ///////////////////////////////////////////////////////////////////////////////
 // AnonymousDecodingTask implementation.
 ///////////////////////////////////////////////////////////////////////////////
 
-AnonymousDecodingTask::AnonymousDecodingTask(NotNull<Decoder*> aDecoder)
+AnonymousDecodingTask::AnonymousDecodingTask(NotNull<Decoder*> aDecoder,
+                                             bool aResumable)
   : mDecoder(aDecoder)
+  , mResumable(aResumable)
 { }
 
 void
 AnonymousDecodingTask::Run()
 {
   while (true) {
     LexerResult result = mDecoder->Decode(WrapNotNull(this));
 
@@ -206,10 +208,25 @@ AnonymousDecodingTask::Run()
     }
 
     // Right now we don't do anything special for other kinds of yields, so just
     // keep working.
     MOZ_ASSERT(result.is<Yield>());
   }
 }
 
+void
+AnonymousDecodingTask::Resume()
+{
+  // Anonymous decoders normally get all their data at once. We have tests
+  // where they don't; typically in these situations, the test re-runs them
+  // manually. However some tests want to verify Resume works, so they will
+  // explicitly request this behaviour.
+  if (mResumable) {
+    RefPtr<AnonymousDecodingTask> self(this);
+    NS_DispatchToMainThread(NS_NewRunnableFunction(
+      "image::AnonymousDecodingTask::Resume",
+      [self]() -> void { self->Run(); }));
+  }
+}
+
 } // namespace image
 } // namespace mozilla
diff --git a/image/IDecodingTask.h b/image/IDecodingTask.h
--- a/image/IDecodingTask.h
+++ b/image/IDecodingTask.h
@@ -104,30 +104,29 @@ private:
  * An IDecodingTask implementation for anonymous decoders - that is, decoders
  * with no associated Image object.
  */
 class AnonymousDecodingTask final : public IDecodingTask
 {
 public:
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(AnonymousDecodingTask, override)
 
-  explicit AnonymousDecodingTask(NotNull<Decoder*> aDecoder);
+  explicit AnonymousDecodingTask(NotNull<Decoder*> aDecoder,
+                                 bool aResumable);
 
   void Run() override;
 
   bool ShouldPreferSyncRun() const override { return true; }
   TaskPriority Priority() const override { return TaskPriority::eLow; }
 
-  // Anonymous decoders normally get all their data at once. We have tests where
-  // they don't; in these situations, the test re-runs them manually. So no
-  // matter what, we don't want to resume by posting a task to the DecodePool.
-  void Resume() override { }
+  void Resume() override;
 
 private:
   virtual ~AnonymousDecodingTask() { }
 
   NotNull<RefPtr<Decoder>> mDecoder;
+  bool mResumable;
 };
 
 } // namespace image
 } // namespace mozilla
 
 #endif // mozilla_image_IDecodingTask_h
diff --git a/image/ImageOps.cpp b/image/ImageOps.cpp
--- a/image/ImageOps.cpp
+++ b/image/ImageOps.cpp
@@ -173,17 +173,18 @@ ImageOps::DecodeMetadata(ImageBuffer* aB
   RefPtr<Decoder> decoder =
     DecoderFactory::CreateAnonymousMetadataDecoder(decoderType,
                                                    WrapNotNull(sourceBuffer));
   if (!decoder) {
     return NS_ERROR_FAILURE;
   }
 
   // Run the decoder synchronously.
-  RefPtr<IDecodingTask> task = new AnonymousDecodingTask(WrapNotNull(decoder));
+  RefPtr<IDecodingTask> task =
+    new AnonymousDecodingTask(WrapNotNull(decoder), /* aResumable */ false);
   task->Run();
   if (!decoder->GetDecodeDone() || decoder->HasError()) {
     return NS_ERROR_FAILURE;
   }
 
   aMetadata = decoder->GetImageMetadata();
   if (aMetadata.GetNativeSizes().IsEmpty() && aMetadata.HasSize()) {
     aMetadata.AddNativeSize(aMetadata.GetSize());
@@ -224,17 +225,18 @@ ImageOps::DecodeToSurface(ImageBuffer* a
     DecoderFactory::CreateAnonymousDecoder(decoderType,
                                            WrapNotNull(sourceBuffer),
                                            aSize, ToSurfaceFlags(aFlags));
   if (!decoder) {
     return nullptr;
   }
 
   // Run the decoder synchronously.
-  RefPtr<IDecodingTask> task = new AnonymousDecodingTask(WrapNotNull(decoder));
+  RefPtr<IDecodingTask> task =
+    new AnonymousDecodingTask(WrapNotNull(decoder), /* aResumable */ false);
   task->Run();
   if (!decoder->GetDecodeDone() || decoder->HasError()) {
     return nullptr;
   }
 
   // Pull out the surface.
   RawAccessFrameRef frame = decoder->GetCurrentFrameRef();
   if (!frame) {
diff --git a/image/decoders/nsWebPDecoder.cpp b/image/decoders/nsWebPDecoder.cpp
--- a/image/decoders/nsWebPDecoder.cpp
+++ b/image/decoders/nsWebPDecoder.cpp
@@ -105,17 +105,17 @@ nsWebPDecoder::ReadData()
 
 LexerResult
 nsWebPDecoder::DoDecode(SourceBufferIterator& aIterator, IResumable* aOnResume)
 {
   MOZ_ASSERT(!HasError(), "Shouldn't call DoDecode after error!");
 
   SourceBufferIterator::State state = SourceBufferIterator::COMPLETE;
   if (!mIteratorComplete) {
-    state = aIterator.Advance(SIZE_MAX);
+    state = aIterator.AdvanceOrScheduleResume(SIZE_MAX, aOnResume);
 
     // We need to remember since we can't advance a complete iterator.
     mIteratorComplete = state == SourceBufferIterator::COMPLETE;
   }
 
   switch (state) {
     case SourceBufferIterator::READY:
       if (!aIterator.IsContiguous()) {
@@ -128,16 +128,18 @@ nsWebPDecoder::DoDecode(SourceBufferIter
         // the first block.
         MOZ_ASSERT(mLength == 0);
         mData = reinterpret_cast<const uint8_t*>(aIterator.Data());
       }
       mLength += aIterator.Length();
       return ReadData();
     case SourceBufferIterator::COMPLETE:
       return ReadData();
+    case SourceBufferIterator::WAITING:
+      return LexerResult(Yield::NEED_MORE_DATA);
     default:
       MOZ_LOG(sWebPLog, LogLevel::Error,
           ("[this=%p] nsWebPDecoder::DoDecode -- bad state\n", this));
       return LexerResult(TerminalState::FAILURE);
   }
 
   // We need to buffer. If we have no data buffered, we need to get everything
   // from the first chunk of the source buffer before appending the new data.
@@ -175,18 +177,26 @@ nsWebPDecoder::DoDecode(SourceBufferIter
 
 nsresult
 nsWebPDecoder::CreateFrame(const nsIntRect& aFrameRect)
 {
   MOZ_ASSERT(HasSize());
   MOZ_ASSERT(!mDecoder);
 
   MOZ_LOG(sWebPLog, LogLevel::Debug,
-      ("[this=%p] nsWebPDecoder::CreateFrame -- frame %u, %d x %d\n",
-       this, mCurrentFrame, aFrameRect.width, aFrameRect.height));
+      ("[this=%p] nsWebPDecoder::CreateFrame -- frame %u, (%d, %d) %d x %d\n",
+       this, mCurrentFrame, aFrameRect.x, aFrameRect.y,
+       aFrameRect.width, aFrameRect.height));
+
+  if (aFrameRect.width <= 0 || aFrameRect.height <= 0) {
+    MOZ_LOG(sWebPLog, LogLevel::Error,
+        ("[this=%p] nsWebPDecoder::CreateFrame -- bad frame rect\n",
+         this));
+    return NS_ERROR_FAILURE;
+  }
 
   // If this is our first frame in an animation and it doesn't cover the
   // full frame, then we are transparent even if there is no alpha
   if (mCurrentFrame == 0 && !aFrameRect.IsEqualEdges(FullFrame())) {
     MOZ_ASSERT(HasAnimation());
     mFormat = SurfaceFormat::B8G8R8A8;
     PostHasTransparency();
   }
@@ -215,16 +225,17 @@ nsWebPDecoder::CreateFrame(const nsIntRe
   Maybe<SurfacePipe> pipe = SurfacePipeFactory::CreateSurfacePipe(this,
       Size(), OutputSize(), aFrameRect, mFormat, Some(animParams), pipeFlags);
   if (!pipe) {
     MOZ_LOG(sWebPLog, LogLevel::Error,
         ("[this=%p] nsWebPDecoder::CreateFrame -- no pipe\n", this));
     return NS_ERROR_FAILURE;
   }
 
+  mFrameRect = aFrameRect;
   mPipe = std::move(*pipe);
   return NS_OK;
 }
 
 void
 nsWebPDecoder::EndFrame()
 {
   MOZ_ASSERT(HasSize());
@@ -414,17 +425,19 @@ nsWebPDecoder::ReadSingle(const uint8_t*
   int width = 0;
   int height = 0;
   int stride = 0;
   uint8_t* rowStart = WebPIDecGetRGB(mDecoder, &lastRow, &width, &height, &stride);
   if (!rowStart || lastRow == -1) {
     return LexerResult(Yield::NEED_MORE_DATA);
   }
 
-  if (width <= 0 || height <= 0 || stride <= 0) {
+  if (width != mFrameRect.width || height != mFrameRect.height ||
+      stride < mFrameRect.width * 4 ||
+      lastRow > mFrameRect.height) {
     MOZ_LOG(sWebPLog, LogLevel::Error,
         ("[this=%p] nsWebPDecoder::ReadSingle -- bad (w,h,s) = (%d, %d, %d)\n",
          this, width, height, stride));
     return LexerResult(TerminalState::FAILURE);
   }
 
   const bool noPremultiply =
     bool(GetSurfaceFlags() & SurfaceFlags::NO_PREMULTIPLY_ALPHA);
diff --git a/image/decoders/nsWebPDecoder.h b/image/decoders/nsWebPDecoder.h
--- a/image/decoders/nsWebPDecoder.h
+++ b/image/decoders/nsWebPDecoder.h
@@ -16,16 +16,18 @@ namespace mozilla {
 namespace image {
 class RasterImage;
 
 class nsWebPDecoder final : public Decoder
 {
 public:
   virtual ~nsWebPDecoder();
 
+  DecoderType GetType() const override { return DecoderType::WEBP; }
+
 protected:
   LexerResult DoDecode(SourceBufferIterator& aIterator,
                        IResumable* aOnResume) override;
 
 private:
   friend class DecoderFactory;
 
   // Decoders should only be instantiated via DecoderFactory.
@@ -71,16 +73,19 @@ private:
   DisposalMethod mDisposal;
 
   /// Frame timeout for the current frame;
   FrameTimeout mTimeout;
 
   /// Surface format for the current frame.
   gfx::SurfaceFormat mFormat;
 
+  /// Frame rect for the current frame.
+  IntRect mFrameRect;
+
   /// The last row of decoded pixels written to mPipe.
   int mLastRow;
 
   /// Number of decoded frames.
   uint32_t mCurrentFrame;
 
   /// Pointer to the start of the contiguous encoded image data.
   const uint8_t* mData;
diff --git a/image/test/gtest/TestDecoders.cpp b/image/test/gtest/TestDecoders.cpp
--- a/image/test/gtest/TestDecoders.cpp
+++ b/image/test/gtest/TestDecoders.cpp
@@ -112,17 +112,18 @@ void WithSingleChunkDecode(const ImageTe
 
   // Create a decoder.
   DecoderType decoderType =
     DecoderFactory::GetDecoderType(aTestCase.mMimeType);
   RefPtr<Decoder> decoder =
     DecoderFactory::CreateAnonymousDecoder(decoderType, sourceBuffer, aOutputSize,
                                            DefaultSurfaceFlags());
   ASSERT_TRUE(decoder != nullptr);
-  RefPtr<IDecodingTask> task = new AnonymousDecodingTask(WrapNotNull(decoder));
+  RefPtr<IDecodingTask> task =
+    new AnonymousDecodingTask(WrapNotNull(decoder), /* aResumable */ false);
 
   // Run the full decoder synchronously.
   task->Run();
 
   // Call the lambda to verify the expected results.
   aResultChecker(decoder);
 }
 
@@ -149,17 +150,18 @@ CheckDecoderMultiChunk(const ImageTestCa
   auto sourceBuffer = MakeNotNull<RefPtr<SourceBuffer>>();
   sourceBuffer->ExpectLength(length);
   DecoderType decoderType =
     DecoderFactory::GetDecoderType(aTestCase.mMimeType);
   RefPtr<Decoder> decoder =
     DecoderFactory::CreateAnonymousDecoder(decoderType, sourceBuffer, Nothing(),
                                            DefaultSurfaceFlags());
   ASSERT_TRUE(decoder != nullptr);
-  RefPtr<IDecodingTask> task = new AnonymousDecodingTask(WrapNotNull(decoder));
+  RefPtr<IDecodingTask> task =
+    new AnonymousDecodingTask(WrapNotNull(decoder), /* aResumable */ false);
 
   for (uint64_t read = 0; read < length ; ++read) {
     uint64_t available = 0;
     rv = inputStream->Available(&available);
     ASSERT_TRUE(available > 0);
     ASSERT_TRUE(NS_SUCCEEDED(rv));
 
     rv = sourceBuffer->AppendFromInputStream(inputStream, 1);
diff --git a/image/test/gtest/TestMetadata.cpp b/image/test/gtest/TestMetadata.cpp
--- a/image/test/gtest/TestMetadata.cpp
+++ b/image/test/gtest/TestMetadata.cpp
@@ -55,17 +55,18 @@ CheckMetadata(const ImageTestCase& aTest
   sourceBuffer->Complete(NS_OK);
 
   // Create a metadata decoder.
   DecoderType decoderType =
     DecoderFactory::GetDecoderType(aTestCase.mMimeType);
   RefPtr<Decoder> decoder =
     DecoderFactory::CreateAnonymousMetadataDecoder(decoderType, sourceBuffer);
   ASSERT_TRUE(decoder != nullptr);
-  RefPtr<IDecodingTask> task = new AnonymousDecodingTask(WrapNotNull(decoder));
+  RefPtr<IDecodingTask> task =
+    new AnonymousDecodingTask(WrapNotNull(decoder), /* aResumable */ false);
 
   if (aBMPWithinICO == BMPWithinICO::YES) {
     static_cast<nsBMPDecoder*>(decoder.get())->SetIsWithinICO();
   }
 
   // Run the metadata decoder synchronously.
   task->Run();
 
@@ -105,17 +106,17 @@ CheckMetadata(const ImageTestCase& aTest
   EXPECT_EQ(bool(aTestCase.mFlags & TEST_CASE_IS_ANIMATED),
             bool(metadataProgress & FLAG_IS_ANIMATED));
 
   // Create a full decoder, so we can compare the result.
   decoder =
     DecoderFactory::CreateAnonymousDecoder(decoderType, sourceBuffer, Nothing(),
                                            DefaultSurfaceFlags());
   ASSERT_TRUE(decoder != nullptr);
-  task = new AnonymousDecodingTask(WrapNotNull(decoder));
+  task = new AnonymousDecodingTask(WrapNotNull(decoder), /* aResumable */ false);
 
   if (aBMPWithinICO == BMPWithinICO::YES) {
     static_cast<nsBMPDecoder*>(decoder.get())->SetIsWithinICO();
   }
 
   // Run the full decoder synchronously.
   task->Run();
 
