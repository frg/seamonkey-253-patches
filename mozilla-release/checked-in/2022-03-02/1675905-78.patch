# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1604727391 0
# Node ID 22b8bef3c436a4d36b586804f342928e1ab11e51
# Parent  e69269760fcecd99683a179d5ed32322069f6a1c
Bug 1675905 - Simplify IonBuilder::createThisScripted. r=jandem,iain a=RyanVM

Differential Revision: https://phabricator.services.mozilla.com/D96309

diff --git a/js/src/jit/IonBuilder.cpp b/js/src/jit/IonBuilder.cpp
--- a/js/src/jit/IonBuilder.cpp
+++ b/js/src/jit/IonBuilder.cpp
@@ -4850,38 +4850,28 @@ IonBuilder::createCallObject(MDefinition
 MDefinition*
 IonBuilder::createThisScripted(MDefinition* callee, MDefinition* newTarget)
 {
     // Get callee.prototype.
     //
     // This instruction MUST be idempotent: since it does not correspond to an
     // explicit operation in the bytecode, we cannot use resumeAfter().
     // Getters may not override |prototype| fetching, so this operation is indeed idempotent.
-    // - First try an idempotent property cache.
-    // - Upon failing idempotent property cache, we can't use a non-idempotent cache,
-    //   therefore we fallback to CallGetProperty
     //
-    // Note: both CallGetProperty and GetPropertyCache can trigger a GC,
-    //       and thus invalidation.
-    MInstruction* getProto;
-    if (!invalidatedIdempotentCache()) {
-        MConstant* id = constant(StringValue(names().prototype));
-        MGetPropertyCache* getPropCache = MGetPropertyCache::New(alloc(), newTarget, id,
-                                                                 /* monitored = */ false);
-        getPropCache->setIdempotent();
-        getProto = getPropCache;
-    } else {
-        MCallGetProperty* callGetProp = MCallGetProperty::New(alloc(), newTarget, names().prototype);
-        callGetProp->setIdempotent();
-        getProto = callGetProp;
-    }
-    current->add(getProto);
+    // Note: GetPropertyCache can trigger a GC, and thus invalidation.
+    MConstant* id = constant(StringValue(names().prototype));
+    MGetPropertyCache* getPropCache =
+        MGetPropertyCache::New(alloc(), newTarget, id,
+                                /* monitored = */ false);
+    getPropCache->setIdempotent();
+    current->add(getPropCache);
 
     // Create this from prototype
-    MCreateThisWithProto* createThis = MCreateThisWithProto::New(alloc(), callee, newTarget, getProto);
+    MCreateThisWithProto* createThis =
+        MCreateThisWithProto::New(alloc(), callee, newTarget, getPropCache);
     current->add(createThis);
 
     return createThis;
 }
 
 JSObject*
 IonBuilder::getSingletonPrototype(JSFunction* target)
 {
diff --git a/js/src/jit/IonIC.cpp b/js/src/jit/IonIC.cpp
--- a/js/src/jit/IonIC.cpp
+++ b/js/src/jit/IonIC.cpp
@@ -157,16 +157,25 @@ IonGetPropertyIC::update(JSContext* cx, 
                 outerScript->filename(), outerScript->lineno());
 
         outerScript->setInvalidatedIdempotentCache();
 
         // Do not re-invalidate if the lookup already caused invalidation.
         if (outerScript->hasIonScript())
             Invalidate(cx, outerScript);
 
+        // IonBuilder::createScriptedThis does not use InvalidedIdempotentCache
+        // flag so prevent bailout-loop by disabling Ion for the script.
+        MOZ_ASSERT(ic->kind() == CacheKind::GetProp);
+        if (idVal.toString()->asAtom().asPropertyName() == cx->names().prototype) {
+          if (val.isObject() && val.toObject().is<JSFunction>()) {
+            outerScript->setIonScript(cx->runtime(), ION_DISABLED_SCRIPT);
+          }
+        }
+
         // We will redo the potentially effectful lookup in Baseline.
         return true;
     }
 
     if (ic->kind() == CacheKind::GetProp) {
         RootedPropertyName name(cx, idVal.toString()->asAtom().asPropertyName());
         if (!GetProperty(cx, val, name, res))
             return false;
