# HG changeset patch
# User Ehsan Akhgari <ehsan@mozilla.com>
# Date 1501631450 14400
# Node ID 17155d1c2ca71ef2e3a21254f4dbeae52a0b99e8
# Parent  41611b61d538cef3fd5fc8259539ddb39589735e
Bug 1386411 - Part 6: Add a more efficient nsISelectionController::GetSelection() API for retrieving native Selection objects; r=bzbarsky

This API avoids needless refcounting and QueryInterface overhead.

diff --git a/dom/base/nsISelectionController.idl b/dom/base/nsISelectionController.idl
--- a/dom/base/nsISelectionController.idl
+++ b/dom/base/nsISelectionController.idl
@@ -3,16 +3,21 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 
 #include "nsISelectionDisplay.idl"
 
 %{C++
 typedef short SelectionRegion;
+namespace mozilla {
+namespace dom {
+class Selection;
+} // namespace dom
+} // namespace mozilla
 %}
 
 interface nsIContent;
 interface nsIDOMNode;
 interface nsISelection;
 interface nsISelectionDisplay;
 
 [scriptable, uuid(3801c9d4-8e69-4bfc-9edb-b58278621f8f)]
@@ -268,16 +273,23 @@ interface nsISelectionController : nsISe
    *  in the current precontext.
    *  @param aNode textNode to test
    *  @param aStartOffset  offset in dom to first char of textnode to test
    *  @param aEndOffset    offset in dom to last char of textnode to test
    *  @param aReturnBool   boolean returned TRUE if visible FALSE if not
    */
     boolean checkVisibility(in nsIDOMNode node, in short startOffset, in short endOffset);
     [noscript,nostdcall] boolean checkVisibilityContent(in nsIContent node, in short startOffset, in short endOffset);
+
+%{C++
+    /**
+     * Return the selection object corresponding to a selection type.
+     */
+    NS_IMETHOD_(mozilla::dom::Selection*) GetSelection(int16_t aType) = 0;
+%}
 };
 %{ C++
    #define NS_ISELECTIONCONTROLLER_CID \
    { 0x513b9460, 0xd56a, 0x4c4e, \
    { 0xb6, 0xf9, 0x0b, 0x8a, 0xe4, 0x37, 0x2a, 0x3b }}
 
 namespace mozilla {
 
diff --git a/dom/html/nsTextEditorState.cpp b/dom/html/nsTextEditorState.cpp
--- a/dom/html/nsTextEditorState.cpp
+++ b/dom/html/nsTextEditorState.cpp
@@ -289,16 +289,17 @@ public:
 
   //NSISELECTIONCONTROLLER INTERFACES
   NS_IMETHOD SetDisplaySelection(int16_t toggle) override;
   NS_IMETHOD GetDisplaySelection(int16_t* _retval) override;
   NS_IMETHOD SetSelectionFlags(int16_t aInEnable) override;
   NS_IMETHOD GetSelectionFlags(int16_t *aOutEnable) override;
   NS_IMETHOD GetSelection(RawSelectionType aRawSelectionType,
                           nsISelection** aSelection) override;
+  NS_IMETHODIMP_(Selection*) GetSelection(RawSelectionType aRawSelectionType) override;
   NS_IMETHOD ScrollSelectionIntoView(RawSelectionType aRawSelectionType,
                                      int16_t aRegion, int16_t aFlags) override;
   NS_IMETHOD RepaintSelection(RawSelectionType aRawSelectionType) override;
   nsresult RepaintSelection(nsPresContext* aPresContext,
                             SelectionType aSelectionType);
   NS_IMETHOD SetCaretEnabled(bool enabled) override;
   NS_IMETHOD SetCaretReadOnly(bool aReadOnly) override;
   NS_IMETHOD GetCaretEnabled(bool* _retval) override;
@@ -425,16 +426,22 @@ nsTextInputSelectionImpl::GetSelection(R
   if (!(*aSelection)) {
     return NS_ERROR_INVALID_ARG;
   }
 
   NS_ADDREF(*aSelection);
   return NS_OK;
 }
 
+NS_IMETHODIMP_(Selection*)
+nsTextInputSelectionImpl::GetSelection(RawSelectionType aRawSelectionType)
+{
+  return GetSelection(ToSelectionType(aRawSelectionType));
+}
+
 NS_IMETHODIMP
 nsTextInputSelectionImpl::ScrollSelectionIntoView(
                             RawSelectionType aRawSelectionType,
                             int16_t aRegion,
                             int16_t aFlags)
 {
   if (!mFrameSelection)
     return NS_ERROR_FAILURE;
diff --git a/layout/base/PresShell.cpp b/layout/base/PresShell.cpp
--- a/layout/base/PresShell.cpp
+++ b/layout/base/PresShell.cpp
@@ -1581,16 +1581,30 @@ PresShell::GetSelection(RawSelectionType
   if (!selection) {
     return NS_ERROR_INVALID_ARG;
   }
 
   selection.forget(aSelection);
   return NS_OK;
 }
 
+NS_IMETHODIMP_(Selection*)
+PresShell::GetSelection(RawSelectionType aRawSelectionType)
+{
+  if (!mSelection) {
+    return nullptr;
+  }
+
+  RefPtr<nsFrameSelection> frameSelection = mSelection;
+  nsISelection* selection =
+    frameSelection->GetSelection(ToSelectionType(aRawSelectionType));
+
+  return static_cast<Selection*>(selection);
+}
+
 Selection*
 PresShell::GetCurrentSelection(SelectionType aSelectionType)
 {
   if (!mSelection)
     return nullptr;
 
   RefPtr<nsFrameSelection> frameSelection = mSelection;
   return frameSelection->GetSelection(aSelectionType);
diff --git a/layout/base/PresShell.h b/layout/base/PresShell.h
--- a/layout/base/PresShell.h
+++ b/layout/base/PresShell.h
@@ -54,16 +54,17 @@ class ReflowCountMgr;
 
 class nsPresShellEventCB;
 class nsAutoCauseReflowNotifier;
 
 namespace mozilla {
 
 namespace dom {
 class Element;
+class Selection;
 }  // namespace dom
 
 class EventDispatchingCallback;
 
 // A set type for tracking visible frames, for use by the visibility code in
 // PresShell. The set contains nsIFrame* pointers.
 typedef nsTHashtable<nsPtrHashKey<nsIFrame>> VisibleFrames;
 
@@ -95,16 +96,17 @@ public:
   void Init(nsIDocument* aDocument, nsPresContext* aPresContext,
             nsViewManager* aViewManager, mozilla::StyleSetHandle aStyleSet);
   virtual void Destroy() override;
 
   virtual void UpdatePreferenceStyles() override;
 
   NS_IMETHOD GetSelection(RawSelectionType aRawSelectionType,
                           nsISelection** aSelection) override;
+  NS_IMETHODIMP_(dom::Selection*) GetSelection(RawSelectionType aRawSelectionType) override;
   virtual mozilla::dom::Selection*
     GetCurrentSelection(SelectionType aSelectionType) override;
   virtual already_AddRefed<nsISelectionController>
             GetSelectionControllerForFocusedContent(
               nsIContent** aFocusedContent = nullptr) override;
 
   NS_IMETHOD SetDisplaySelection(int16_t aToggle) override;
   NS_IMETHOD GetDisplaySelection(int16_t *aToggle) override;
