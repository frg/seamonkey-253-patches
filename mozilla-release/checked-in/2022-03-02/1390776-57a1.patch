# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1502871428 -32400
# Node ID 112a2a2a66b707fc9691e572b8b06d579dcdb38b
# Parent  6c0bbcd96c2cacac7de0786ee66c5959bde69c99
Bug 1390776 - Put definition and uses of gProgname under #ifdef CRAWL_STACK_ON_SIGSEGV. r=njn

gProgname is actually never used outside #ifdef CRAWL_STACK_ON_SIGSEGV,
and it being defined and filled with a strdup even when not used leads
to a (purposeful, but useless) leak.

The leak was not detected so far because GCC 4.9 generated a call to
PL_strdup followed by a store to gProgname, while GCC 6 keeps the
PL_strdup but is smart enough to figure out it doesn't need to store to
gProgname (and to not keep space for that variable).

So with GCC 4.9, the leak was not considered a leak because it was still
reachable, but with GCC 6, it's not reachable anymore, and thus
considered as a leak.

diff --git a/toolkit/xre/nsSigHandlers.cpp b/toolkit/xre/nsSigHandlers.cpp
--- a/toolkit/xre/nsSigHandlers.cpp
+++ b/toolkit/xre/nsSigHandlers.cpp
@@ -32,18 +32,16 @@
 #endif
 #endif
 
 #if defined(SOLARIS)
 #include <sys/resource.h>
 #include <ucontext.h>
 #endif
 
-static const char* gProgname = "huh?";
-
 // Note: some tests manipulate this value.
 unsigned int _gdb_sleep_duration = 300;
 
 #if defined(LINUX) && defined(DEBUG) && \
       (defined(__i386) || defined(__x86_64) || defined(PPC))
 #define CRAWL_STACK_ON_SIGSEGV
 #endif
 
@@ -55,16 +53,18 @@ unsigned int _gdb_sleep_duration = 300;
 #endif
 
 #if defined(CRAWL_STACK_ON_SIGSEGV)
 
 #include <unistd.h>
 #include "nsISupportsUtils.h"
 #include "mozilla/StackWalk.h"
 
+static const char* gProgname = "huh?";
+
 // NB: keep me up to date with the same variable in
 // ipc/chromium/chrome/common/ipc_channel_posix.cc
 static const int kClientChannelFd = 3;
 
 extern "C" {
 
 static void PrintStackFrame(uint32_t aFrameNumber, void *aPC, void *aSP,
                             void *aClosure)
@@ -224,20 +224,22 @@ static void fpehandler(int signum, sigin
   *mxcsr &= ~SSE_STATUS_FLAGS; /* clear all pending SSE exceptions */
 #endif
 #endif
 }
 #endif
 
 void InstallSignalHandlers(const char *aProgname)
 {
+#if defined(CRAWL_STACK_ON_SIGSEGV)
   const char* tmp = PL_strdup(aProgname);
   if (tmp) {
     gProgname = tmp;
   }
+#endif // CRAWL_STACK_ON_SIGSEGV
 
   const char *gdbSleep = PR_GetEnv("MOZ_GDB_SLEEP");
   if (gdbSleep && *gdbSleep)
   {
     unsigned int s;
     if (1 == sscanf(gdbSleep, "%u", &s)) {
       _gdb_sleep_duration = s;
     }
