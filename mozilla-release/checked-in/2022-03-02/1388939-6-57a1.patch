# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1501603387 -3600
# Node ID 6f02e4c2fc42fd97a4c31ab268424bd4aa6ed9b0
# Parent  4d13c2c742628dbd497e6a5d6248882e7bae04e5
Bug 1388939, part 6 - Give clear names to the nsFrameManager methods for obtaining the linked list of undisplayed style contexts for a node. r=dholbert

MozReview-Commit-ID: 5EuaFzCW4Lh

diff --git a/layout/base/GeckoRestyleManager.cpp b/layout/base/GeckoRestyleManager.cpp
--- a/layout/base/GeckoRestyleManager.cpp
+++ b/layout/base/GeckoRestyleManager.cpp
@@ -1512,20 +1512,20 @@ ElementRestyler::ConditionallyRestyleUnd
 // The structure of this method parallels DoRestyleUndisplayedDescendants.
 // If you update this method, you probably want to update that one too.
 void
 ElementRestyler::DoConditionallyRestyleUndisplayedDescendants(
     nsIContent* aParent,
     Element* aRestyleRoot)
 {
   nsCSSFrameConstructor* fc = mPresContext->FrameConstructor();
-  UndisplayedNode* nodes = fc->GetAllUndisplayedContentIn(aParent);
+  UndisplayedNode* nodes = fc->GetAllRegisteredDisplayNoneStylesIn(aParent);
   ConditionallyRestyleUndisplayedNodes(nodes, aParent,
                                        StyleDisplay::None, aRestyleRoot);
-  nodes = fc->GetAllDisplayContentsIn(aParent);
+  nodes = fc->GetAllRegisteredDisplayContentsStylesIn(aParent);
   ConditionallyRestyleUndisplayedNodes(nodes, aParent,
                                        StyleDisplay::Contents, aRestyleRoot);
 }
 
 // The structure of this method parallels RestyleUndisplayedNodes.
 // If you update this method, you probably want to update that one too.
 void
 ElementRestyler::ConditionallyRestyleUndisplayedNodes(
@@ -1719,18 +1719,18 @@ ElementRestyler::MoveStyleContextsForCon
 bool
 ElementRestyler::MoveStyleContextsForChildren(GeckoStyleContext* aOldContext)
 {
   // Bail out if there are undisplayed or display:contents children.
   // FIXME: We could get this to work if we need to.
   nsIContent* undisplayedParent;
   if (MustCheckUndisplayedContent(mFrame, undisplayedParent)) {
     nsCSSFrameConstructor* fc = mPresContext->FrameConstructor();
-    if (fc->GetAllUndisplayedContentIn(undisplayedParent) ||
-        fc->GetAllDisplayContentsIn(undisplayedParent)) {
+    if (fc->GetAllRegisteredDisplayNoneStylesIn(undisplayedParent) ||
+        fc->GetAllRegisteredDisplayContentsStylesIn(undisplayedParent)) {
       return false;
     }
   }
 
   nsTArray<GeckoStyleContext*> contextsToMove;
 
   MOZ_ASSERT(!MustReframeForBeforePseudo(),
              "shouldn't need to reframe ::before as we would have had "
@@ -3186,20 +3186,20 @@ ElementRestyler::RestyleUndisplayedDesce
 // The structure of this method parallels DoConditionallyRestyleUndisplayedDescendants.
 // If you update this method, you probably want to update that one too.
 void
 ElementRestyler::DoRestyleUndisplayedDescendants(nsRestyleHint aChildRestyleHint,
                                                  nsIContent* aParent,
                                                  GeckoStyleContext* aParentContext)
 {
   nsCSSFrameConstructor* fc = mPresContext->FrameConstructor();
-  UndisplayedNode* nodes = fc->GetAllUndisplayedContentIn(aParent);
+  UndisplayedNode* nodes = fc->GetAllRegisteredDisplayNoneStylesIn(aParent);
   RestyleUndisplayedNodes(aChildRestyleHint, nodes, aParent,
                           aParentContext, StyleDisplay::None);
-  nodes = fc->GetAllDisplayContentsIn(aParent);
+  nodes = fc->GetAllRegisteredDisplayContentsStylesIn(aParent);
   RestyleUndisplayedNodes(aChildRestyleHint, nodes, aParent,
                           aParentContext, StyleDisplay::Contents);
 }
 
 // The structure of this method parallels ConditionallyRestyleUndisplayedNodes.
 // If you update this method, you probably want to update that one too.
 void
 ElementRestyler::RestyleUndisplayedNodes(nsRestyleHint      aChildRestyleHint,
diff --git a/layout/base/nsFrameManager.cpp b/layout/base/nsFrameManager.cpp
--- a/layout/base/nsFrameManager.cpp
+++ b/layout/base/nsFrameManager.cpp
@@ -172,17 +172,17 @@ nsFrameManager::GetUndisplayedNodeInMapF
 /* static */ UndisplayedNode*
 nsFrameManager::GetAllUndisplayedNodesInMapFor(UndisplayedMap* aMap,
                                                nsIContent* aParentContent)
 {
   return aMap ? aMap->GetFirstNode(aParentContent) : nullptr;
 }
 
 UndisplayedNode*
-nsFrameManager::GetAllUndisplayedContentIn(nsIContent* aParentContent)
+nsFrameManager::GetAllRegisteredDisplayNoneStylesIn(nsIContent* aParentContent)
 {
   return GetAllUndisplayedNodesInMapFor(mDisplayNoneMap, aParentContent);
 }
 
 /* static */ void
 nsFrameManager::SetStyleContextInMap(UndisplayedMap* aMap,
                                      nsIContent* aContent,
                                      nsStyleContext* aStyleContext)
@@ -319,17 +319,17 @@ nsFrameManager::RegisterDisplayContentsS
 {
   if (!mDisplayContentsMap) {
     mDisplayContentsMap = new UndisplayedMap;
   }
   SetStyleContextInMap(mDisplayContentsMap, aContent, aStyleContext);
 }
 
 UndisplayedNode*
-nsFrameManager::GetAllDisplayContentsIn(nsIContent* aParentContent)
+nsFrameManager::GetAllRegisteredDisplayContentsStylesIn(nsIContent* aParentContent)
 {
   return GetAllUndisplayedNodesInMapFor(mDisplayContentsMap, aParentContent);
 }
 
 void
 nsFrameManager::ClearDisplayContentsIn(nsIContent* aContent,
                                        nsIContent* aParentContent)
 {
diff --git a/layout/base/nsFrameManager.h b/layout/base/nsFrameManager.h
--- a/layout/base/nsFrameManager.h
+++ b/layout/base/nsFrameManager.h
@@ -137,24 +137,26 @@ public:
     return GetStyleContextInMap(mDisplayContentsMap, aContent);
   }
 
   /**
    * Return the linked list of UndisplayedNodes that contain the style contexts
    * that have been registered for the display:none children of
    * aParentContent.
    */
-  UndisplayedNode* GetAllUndisplayedContentIn(nsIContent* aParentContent);
+  UndisplayedNode*
+  GetAllRegisteredDisplayNoneStylesIn(nsIContent* aParentContent);
 
   /**
    * Return the linked list of UndisplayedNodes that contain the style contexts
    * that have been registered for the display:contents children of
    * aParentContent.
    */
-  UndisplayedNode* GetAllDisplayContentsIn(nsIContent* aParentContent);
+  UndisplayedNode*
+  GetAllRegisteredDisplayContentsStylesIn(nsIContent* aParentContent);
 
   /**
    * Return the relevant undisplayed node for a given content with display:
    * contents style.
    */
   UndisplayedNode* GetDisplayContentsNodeFor(const nsIContent* aContent)
   {
     if (!mDisplayContentsMap) {

