# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1502946991 25200
# Node ID 4362abd25f538e5c39dfe49027cfd7b784c2d2fb
# Parent  bb16787cdf18aea2a76384116624217847260148
Bug 1391153: Get rid of Sandbox clone for cross-process API calls. r=mixedpuppy

MozReview-Commit-ID: A9g6s5jqd78

diff --git a/toolkit/components/extensions/ExtensionChild.jsm b/toolkit/components/extensions/ExtensionChild.jsm
--- a/toolkit/components/extensions/ExtensionChild.jsm
+++ b/toolkit/components/extensions/ExtensionChild.jsm
@@ -803,33 +803,29 @@ class ChildAPIManager {
    * Calls a function in the parent process and returns its result
    * asynchronously.
    *
    * @param {string} path The full name of the method, e.g. "tabs.create".
    * @param {Array} args The parameters for the function.
    * @param {function(*)} [callback] The callback to be called when the function
    *     completes.
    * @param {object} [options] Extra options.
-   * @param {boolean} [options.noClone = false] If true, do not clone
-   *     the arguments into an extension sandbox before calling the API
-   *     method.
    * @returns {Promise|undefined} Must be void if `callback` is set, and a
    *     promise otherwise. The promise is resolved when the function completes.
    */
   callParentAsyncFunction(path, args, callback, options = {}) {
     let callId = getUniqueId();
     let deferred = PromiseUtils.defer();
     this.callPromises.set(callId, deferred);
 
     this.messageManager.sendAsyncMessage("API:Call", {
       childId: this.id,
       callId,
       path,
       args,
-      noClone: options.noClone || false,
     });
 
     return this.context.wrapPromise(deferred.promise, callback);
   }
 
   /**
    * Create a proxy for an event in the parent process. The returned event
    * object shares its internal state with other instances. For instance, if
diff --git a/toolkit/components/extensions/ExtensionParent.jsm b/toolkit/components/extensions/ExtensionParent.jsm
--- a/toolkit/components/extensions/ExtensionParent.jsm
+++ b/toolkit/components/extensions/ExtensionParent.jsm
@@ -400,16 +400,22 @@ class ProxyContextParent extends BaseCon
       this.pendingEventBrowser = savedBrowser;
     }
   }
 
   get cloneScope() {
     return this.sandbox;
   }
 
+  runSafe(...args) {
+    // There's no need to clone when calling listeners for a proxied
+    // context.
+    return this.runSafeWithoutClone(...args);
+  }
+
   get xulBrowser() {
     return this.messageManagerProxy.eventTarget;
   }
 
   get parentMessageManager() {
     return this.messageManagerProxy.messageManager;
   }
 
@@ -686,17 +692,17 @@ ParentAPIManager = {
         "API:CallResult",
         Object.assign({
           childId: data.childId,
           callId: data.callId,
         }, result));
     };
 
     try {
-      let args = data.noClone ? data.args : Cu.cloneInto(data.args, context.sandbox);
+      let args = data.args;
       let pendingBrowser = context.pendingEventBrowser;
       let fun = await context.apiCan.asyncFindAPIPath(data.path);
       let result = context.withPendingBrowser(pendingBrowser,
                                               () => fun(...args));
       if (data.callId) {
         result = result || Promise.resolve();
 
         result.then(result => {
@@ -742,17 +748,17 @@ ParentAPIManager = {
         },
         {
           recipient: {childId},
         });
     }
 
     context.listenerProxies.set(data.listenerId, listener);
 
-    let args = Cu.cloneInto(data.args, context.sandbox);
+    let args = data.args;
     let promise = context.apiCan.asyncFindAPIPath(data.path);
 
     // Store pending listener additions so we can be sure they're all
     // fully initialize before we consider extension startup complete.
     if (context.viewType === "background" && context.listenerPromises) {
       const {listenerPromises} = context;
       listenerPromises.add(promise);
       let remove = () => { listenerPromises.delete(promise); };
diff --git a/toolkit/components/extensions/ext-c-storage.js b/toolkit/components/extensions/ext-c-storage.js
--- a/toolkit/components/extensions/ext-c-storage.js
+++ b/toolkit/components/extensions/ext-c-storage.js
@@ -89,31 +89,31 @@ this.storage = class extends ExtensionAP
       storage: {
         local: {
           get: async function(keys) {
             const stopwatchKey = {};
             TelemetryStopwatch.start(storageGetHistogram, stopwatchKey);
             try {
               let result = await context.childManager.callParentAsyncFunction("storage.local.get", [
                 serialize(keys),
-              ], null, {noClone: true}).then(deserialize);
+              ]).then(deserialize);
               TelemetryStopwatch.finish(storageGetHistogram, stopwatchKey);
               return result;
             } catch (e) {
               TelemetryStopwatch.cancel(storageGetHistogram, stopwatchKey);
               throw e;
             }
           },
           set: async function(items) {
             const stopwatchKey = {};
             TelemetryStopwatch.start(storageSetHistogram, stopwatchKey);
             try {
               let result = await context.childManager.callParentAsyncFunction("storage.local.set", [
                 serialize(items),
-              ], null, {noClone: true});
+              ]);
               TelemetryStopwatch.finish(storageSetHistogram, stopwatchKey);
               return result;
             } catch (e) {
               TelemetryStopwatch.cancel(storageSetHistogram, stopwatchKey);
               throw e;
             }
           },
         },
