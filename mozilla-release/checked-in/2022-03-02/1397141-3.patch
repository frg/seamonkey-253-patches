# HG changeset patch
# User Alastor Wu <alwu@mozilla.com>
# Date 1505286345 -28800
# Node ID 81a7bf2ee0988cdd98cd9a506071ab4b5ba2f0d3
# Parent  8a9625f9bfd64ebb624d3e9f8466c10f59779e7a
Bug 1397141 - part3 : remove the minimum resolution check. r=jya

After bug 1392143, we won't enable HW decoding for the resolution < 132 pixels.
In addition, software decoder doesn't have the minimum resolution limit, so
we can remove the minimum resolution check.

MozReview-Commit-ID: 7MiLpwjiq3s

diff --git a/dom/media/platforms/wmf/WMFVideoMFTManager.cpp b/dom/media/platforms/wmf/WMFVideoMFTManager.cpp
--- a/dom/media/platforms/wmf/WMFVideoMFTManager.cpp
+++ b/dom/media/platforms/wmf/WMFVideoMFTManager.cpp
@@ -505,35 +505,26 @@ WMFVideoMFTManager::InitializeDXVA()
 
 MediaResult
 WMFVideoMFTManager::ValidateVideoInfo()
 {
   if (mStreamType != H264 ||
       gfxPrefs::PDMWMFAllowUnsupportedResolutions()) {
     return NS_OK;
   }
-  // The WMF H.264 decoder is documented to have a minimum resolution
-  // 48x48 pixels. We've observed the decoder working for output smaller than
-  // that, but on some output it hangs in IMFTransform::ProcessOutput(), so
-  // we just reject streams which are less than the documented minimum.
+  // The WMF H.264 decoder is documented to have a minimum resolution 48x48 pixels
+  // for resolution, but we won't enable hw decoding for the resolution < 132 pixels.
+  // It's assumed the software decoder doesn't have this limitation, but it still
+  // might have maximum resolution limitation.
   // https://msdn.microsoft.com/en-us/library/windows/desktop/dd797815(v=vs.85).aspx
   const bool Is4KCapable = IsWin8OrLater() || IsWin7H264Decoder4KCapable();
   static const int32_t MIN_H264_FRAME_DIMENSION = 48;
   static const int32_t MAX_H264_FRAME_WIDTH = Is4KCapable ? 4096 : 1920;
   static const int32_t MAX_H264_FRAME_HEIGHT = Is4KCapable ? 2304 : 1088;
 
-  if (mStreamType == H264
-      && (mVideoInfo.mImage.width < MIN_H264_FRAME_DIMENSION
-          || mVideoInfo.mImage.height < MIN_H264_FRAME_DIMENSION)) {
-    mIsValid = false;
-    return MediaResult(NS_ERROR_DOM_MEDIA_FATAL_ERR,
-                       RESULT_DETAIL("Can't decode H.264 stream with width or "
-                                     "height less than 48 pixels."));
-  }
-
   if (mVideoInfo.mImage.width > MAX_H264_FRAME_WIDTH  ||
       mVideoInfo.mImage.height > MAX_H264_FRAME_HEIGHT) {
     mIsValid = false;
     return MediaResult(NS_ERROR_DOM_MEDIA_FATAL_ERR,
                        RESULT_DETAIL("Can't decode H.264 stream because its "
                                      "resolution is out of the maximum limitation"));
   }
 
