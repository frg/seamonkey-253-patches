# HG changeset patch
# User Valentin Gosu <valentin.gosu@gmail.com>
# Date 1606378503 0
#      Thu Nov 26 08:15:03 2020 +0000
# Node ID cc7a574fb4e00f834f0c95a12154f929ba14898f
# Parent  d5f71b58d0116e4a1063136e310f7e18d7e8c49c
Bug 1667581 - Ensure there's only one nsSegmentedBuffer::FreeOMT pending at any time. r=sg a=RyanVM

Differential Revision: https://phabricator.services.mozilla.com/D97368

diff --git a/xpcom/io/nsSegmentedBuffer.cpp b/xpcom/io/nsSegmentedBuffer.cpp
--- a/xpcom/io/nsSegmentedBuffer.cpp
+++ b/xpcom/io/nsSegmentedBuffer.cpp
@@ -129,21 +129,47 @@ void nsSegmentedBuffer::FreeOMT(void* aP
 }
 
 void nsSegmentedBuffer::FreeOMT(std::function<void()>&& aTask) {
   if (!NS_IsMainThread()) {
     aTask();
     return;
   }
 
+  if (mFreeOMT) {
+    // There is a runnable pending which will handle this object
+    if (mFreeOMT->AddTask(std::move(aTask)) > 1) {
+      return;
+    }
+  } else {
+    mFreeOMT = MakeRefPtr<FreeOMTPointers>();
+    mFreeOMT->AddTask(std::move(aTask));
+  }
+
   if (!mIOThread) {
     mIOThread = do_GetService(NS_STREAMTRANSPORTSERVICE_CONTRACTID);
   }
 
   // During the shutdown we are not able to obtain the IOThread and/or the
   // dispatching of runnable fails.
   if (!mIOThread || NS_FAILED(mIOThread->Dispatch(NS_NewRunnableFunction(
-                        "nsSegmentedBuffer::FreeOMT", aTask)))) {
-    aTask();
+                        "nsSegmentedBuffer::FreeOMT",
+                        [obj = mFreeOMT]() { obj->FreeAll(); })))) {
+    mFreeOMT->FreeAll();
+  }
+}
+
+void nsSegmentedBuffer::FreeOMTPointers::FreeAll() {
+  // Take all the tasks from the object. If AddTask is called after we release
+  // the lock, then another runnable will be dispatched for that task. This is
+  // necessary to avoid blocking the main thread while memory is being freed.
+  nsTArray<std::function<void()>> tasks = [this]() {
+    auto t = mTasks.Lock();
+    return std::move(*t);
+  }();
+
+  // Finally run all the tasks to free memory.
+  for (auto& task : tasks) {
+    task();
   }
 }
 
 ////////////////////////////////////////////////////////////////////////////////
diff --git a/xpcom/io/nsSegmentedBuffer.h b/xpcom/io/nsSegmentedBuffer.h
--- a/xpcom/io/nsSegmentedBuffer.h
+++ b/xpcom/io/nsSegmentedBuffer.h
@@ -8,17 +8,17 @@
 #define nsSegmentedBuffer_h__
 
 #include "nsIMemory.h"
 #include <stddef.h>
 
 #include "nsCOMPtr.h"
 #include "nsDebug.h"
 #include "nsError.h"
-#include "mozilla/Mutex.h"
+#include "mozilla/DataMutex.h"
 
 class nsIEventTarget;
 
 class nsSegmentedBuffer {
  public:
   nsSegmentedBuffer()
       : mSegmentSize(0),
         mMaxSize(0),
@@ -79,20 +79,46 @@ class nsSegmentedBuffer {
   uint32_t mSegmentSize;
   uint32_t mMaxSize;
   char** mSegmentArray;
   uint32_t mSegmentArrayCount;
   int32_t mFirstSegmentIndex;
   int32_t mLastSegmentIndex;
 
  private:
+  class FreeOMTPointers {
+    NS_INLINE_DECL_THREADSAFE_REFCOUNTING(FreeOMTPointers)
+
+   public:
+    FreeOMTPointers() : mTasks("nsSegmentedBuffer::FreeOMTPointers") {}
+
+    void FreeAll();
+
+    // Adds a task to the array. Returns the size of the array.
+    size_t AddTask(std::function<void()>&& aTask) {
+      auto tasks = mTasks.Lock();
+      tasks->AppendElement(std::move(aTask));
+      return tasks->Length();
+    }
+
+   private:
+    ~FreeOMTPointers() = default;
+
+    mozilla::DataMutex<nsTArray<std::function<void()>>> mTasks;
+  };
+
   void FreeOMT(void* aPtr);
   void FreeOMT(std::function<void()>&& aTask);
 
   nsCOMPtr<nsIEventTarget> mIOThread;
+
+  // This object is created the first time we need to dispatch to another thread
+  // to free segments. It is only freed when the nsSegmentedBufer is destroyed
+  // or when the runnable is finally handled and its refcount goes to 0.
+  RefPtr<FreeOMTPointers> mFreeOMT;
 };
 
 // NS_SEGMENTARRAY_INITIAL_SIZE: This number needs to start out as a
 // power of 2 given how it gets used. We double the segment array
 // when we overflow it, and use that fact that it's a power of 2
 // to compute a fast modulus operation in IsFull.
 //
 // 32 segment array entries can accommodate 128k of data if segments
