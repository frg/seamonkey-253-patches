# HG changeset patch
# User Nathan Moos <moosingin3space@gmail.com>
# Date 1539631510 0
# Node ID 2fd7d3ae070e853281fa49408996ecc9308990a9
# Parent  0ee3f6f335010458bb031474fa3d2c1c1eb83df3
Bug 1490186 - Add GtkFileChooserNative support to Firefox. r=stransky

This patch makes Firefox's GTK3 platform support use GtkFileChooserNative when
available. GtkFileChooserNative transparently uses the desktop portals
interface, which enables Firefox to use native Qt file dialogs on KDE, or
sandboxed file dialogs in Flatpak.

Differential Revision: https://phabricator.services.mozilla.com/D7033

diff --git a/widget/gtk/nsFilePicker.cpp b/widget/gtk/nsFilePicker.cpp
--- a/widget/gtk/nsFilePicker.cpp
+++ b/widget/gtk/nsFilePicker.cpp
@@ -192,17 +192,17 @@ ReadMultipleFiles(gpointer filename, gpo
     nsCOMArray<nsIFile>& files = *static_cast<nsCOMArray<nsIFile>*>(array);
     files.AppendObject(localfile);
   }
 
   g_free(filename);
 }
 
 void
-nsFilePicker::ReadValuesFromFileChooser(GtkWidget *file_chooser)
+nsFilePicker::ReadValuesFromFileChooser(void *file_chooser)
 {
   mFiles.Clear();
 
   if (mMode == nsIFilePicker::modeOpenMultiple) {
     mFileURL.Truncate();
 
     GSList *list = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(file_chooser));
     g_slist_foreach(list, ReadMultipleFiles, static_cast<gpointer>(&mFiles));
@@ -384,44 +384,32 @@ nsFilePicker::Open(nsIFilePickerShownCal
 
   GtkFileChooserAction action = GetGtkFileChooserAction(mMode);
 
   const gchar* accept_button;
   NS_ConvertUTF16toUTF8 buttonLabel(mOkButtonLabel);
   if (!mOkButtonLabel.IsEmpty()) {
     accept_button = buttonLabel.get();
   } else {
-    accept_button = (action == GTK_FILE_CHOOSER_ACTION_SAVE) ?
-                    GTK_STOCK_SAVE : GTK_STOCK_OPEN;
+    accept_button = nullptr;
   }
 
-  GtkWidget *file_chooser =
-      gtk_file_chooser_dialog_new(title.get(), parent_widget, action,
-                                  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
-                                  accept_button, GTK_RESPONSE_ACCEPT,
-                                  nullptr);
-  gtk_dialog_set_alternative_button_order(GTK_DIALOG(file_chooser),
-                                          GTK_RESPONSE_ACCEPT,
-                                          GTK_RESPONSE_CANCEL,
-                                          -1);
+  void *file_chooser = GtkFileChooserNew(title.get(), parent_widget, action, accept_button);
+
   if (mAllowURLs) {
     gtk_file_chooser_set_local_only(GTK_FILE_CHOOSER(file_chooser), FALSE);
   }
 
   if (action == GTK_FILE_CHOOSER_ACTION_OPEN || action == GTK_FILE_CHOOSER_ACTION_SAVE) {
     GtkWidget *img_preview = gtk_image_new();
     gtk_file_chooser_set_preview_widget(GTK_FILE_CHOOSER(file_chooser), img_preview);
     g_signal_connect(file_chooser, "update-preview", G_CALLBACK(UpdateFilePreviewWidget), img_preview);
   }
 
-  GtkWindow *window = GTK_WINDOW(file_chooser);
-  gtk_window_set_modal(window, TRUE);
-  if (parent_widget) {
-    gtk_window_set_destroy_with_parent(window, TRUE);
-  }
+  GtkFileChooserSetModal(file_chooser, parent_widget, TRUE);
 
   NS_ConvertUTF16toUTF8 defaultName(mDefault);
   switch (mMode) {
     case nsIFilePicker::modeOpenMultiple:
       gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(file_chooser), TRUE);
       break;
     case nsIFilePicker::modeSave:
       gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(file_chooser),
@@ -449,36 +437,41 @@ nsFilePicker::Open(nsIFilePickerShownCal
       defaultPath->GetNativePath(directory);
 
 #ifdef MOZ_WIDGET_GTK
       // Workaround for problematic refcounting in GTK3 before 3.16.
       // We need to keep a reference to the dialog's internal delegate.
       // Otherwise, if our dialog gets destroyed, we'll lose the dialog's
       // delegate by the time this gets processed in the event loop.
       // See: https://bugzilla.mozilla.org/show_bug.cgi?id=1166741
-      GtkDialog *dialog = GTK_DIALOG(file_chooser);
-      GtkContainer *area = GTK_CONTAINER(gtk_dialog_get_content_area(dialog));
-      gtk_container_forall(area, [](GtkWidget *widget,
-                                    gpointer data) {
-          if (GTK_IS_FILE_CHOOSER_WIDGET(widget)) {
-            auto result = static_cast<GtkFileChooserWidget**>(data);
-            *result = GTK_FILE_CHOOSER_WIDGET(widget);
-          }
-      }, &mFileChooserDelegate);
+      if (GTK_IS_DIALOG(file_chooser)) {
+        GtkDialog *dialog = GTK_DIALOG(file_chooser);
+        GtkContainer *area = GTK_CONTAINER(gtk_dialog_get_content_area(dialog));
+        gtk_container_forall(area, [](GtkWidget *widget,
+                                      gpointer data) {
+            if (GTK_IS_FILE_CHOOSER_WIDGET(widget)) {
+              auto result = static_cast<GtkFileChooserWidget**>(data);
+              *result = GTK_FILE_CHOOSER_WIDGET(widget);
+            }
+        }, &mFileChooserDelegate);
 
-      if (mFileChooserDelegate)
-        g_object_ref(mFileChooserDelegate);
+        if (mFileChooserDelegate != nullptr) {
+          g_object_ref(mFileChooserDelegate);
+        }
+      }
 #endif
 
       gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(file_chooser),
                                           directory.get());
     }
   }
 
-  gtk_dialog_set_default_response(GTK_DIALOG(file_chooser), GTK_RESPONSE_ACCEPT);
+  if (GTK_IS_DIALOG(file_chooser)) {
+    gtk_dialog_set_default_response(GTK_DIALOG(file_chooser), GTK_RESPONSE_ACCEPT);
+  }
 
   int32_t count = mFilters.Length();
   for (int32_t i = 0; i < count; ++i) {
     // This is fun... the GTK file picker does not accept a list of filters
     // so we need to split out each string, and add it manually.
 
     char **patterns = g_strsplit(mFilters[i].get(), ";", -1);
     if (!patterns) {
@@ -512,39 +505,38 @@ nsFilePicker::Open(nsIFilePickerShownCal
   }
 
   gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(file_chooser), TRUE);
 
   mRunning = true;
   mCallback = aCallback;
   NS_ADDREF_THIS();
   g_signal_connect(file_chooser, "response", G_CALLBACK(OnResponse), this);
-  g_signal_connect(file_chooser, "destroy", G_CALLBACK(OnDestroy), this);
-  gtk_widget_show(file_chooser);
+  GtkFileChooserShow(file_chooser);
 
   return NS_OK;
 }
 
 /* static */ void
-nsFilePicker::OnResponse(GtkWidget* file_chooser, gint response_id,
+nsFilePicker::OnResponse(void* file_chooser, gint response_id,
                          gpointer user_data)
 {
   static_cast<nsFilePicker*>(user_data)->
     Done(file_chooser, response_id);
 }
 
 /* static */ void
 nsFilePicker::OnDestroy(GtkWidget* file_chooser, gpointer user_data)
 {
   static_cast<nsFilePicker*>(user_data)->
     Done(file_chooser, GTK_RESPONSE_CANCEL);
 }
 
 void
-nsFilePicker::Done(GtkWidget* file_chooser, gint response)
+nsFilePicker::Done(void* file_chooser, gint response)
 {
   mRunning = false;
 
   int16_t result;
   switch (response) {
     case GTK_RESPONSE_OK:
     case GTK_RESPONSE_ACCEPT:
     ReadValuesFromFileChooser(file_chooser);
@@ -578,17 +570,17 @@ nsFilePicker::Done(GtkWidget* file_choos
                                        FuncToGpointer(OnDestroy), this);
 
   // When response_id is GTK_RESPONSE_DELETE_EVENT or when called from
   // OnDestroy, the widget would be destroyed anyway but it is fine if
   // gtk_widget_destroy is called more than once.  gtk_widget_destroy has
   // requests that any remaining references be released, but the reference
   // count will not be decremented again if GtkWindow's reference has already
   // been released.
-  gtk_widget_destroy(file_chooser);
+  GtkFileChooserDestroy(file_chooser);
 
 #ifdef MOZ_WIDGET_GTK
       if (mFileChooserDelegate) {
         // Properly deref our acquired reference. We call this after
         // gtk_widget_destroy() to try and ensure that pending file info
         // queries caused by updating the current folder have been cancelled.
         // However, we do not know for certain when the callback will run after
         // cancelled.
@@ -603,8 +595,78 @@ nsFilePicker::Done(GtkWidget* file_choos
   if (mCallback) {
     mCallback->Done(result);
     mCallback = nullptr;
   } else {
     mResult = result;
   }
   NS_RELEASE_THIS();
 }
+
+// All below functions available as of GTK 3.20+
+
+void *
+nsFilePicker::GtkFileChooserNew(
+        const gchar *title, GtkWindow *parent,
+        GtkFileChooserAction action,
+        const gchar *accept_label)
+{
+  static auto sGtkFileChooserNativeNewPtr = (void * (*)(
+        const gchar *, GtkWindow *,
+        GtkFileChooserAction,
+        const gchar *, const gchar *))
+      dlsym(RTLD_DEFAULT, "gtk_file_chooser_native_new");
+  if (sGtkFileChooserNativeNewPtr != nullptr) {
+    return (*sGtkFileChooserNativeNewPtr)(title, parent, action, accept_label, nullptr);
+  }
+  if (accept_label == nullptr) {
+    accept_label = (action == GTK_FILE_CHOOSER_ACTION_SAVE) 
+        ? GTK_STOCK_SAVE : GTK_STOCK_OPEN;
+  }
+  GtkWidget *file_chooser = gtk_file_chooser_dialog_new(title, parent, action,
+      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
+      accept_label, GTK_RESPONSE_ACCEPT, nullptr);
+  gtk_dialog_set_alternative_button_order(GTK_DIALOG(file_chooser),
+          GTK_RESPONSE_ACCEPT, GTK_RESPONSE_CANCEL, -1);
+  return file_chooser;
+}
+
+void
+nsFilePicker::GtkFileChooserShow(void *file_chooser)
+{
+  static auto sGtkNativeDialogShowPtr = (void (*)(void *))
+      dlsym(RTLD_DEFAULT, "gtk_native_dialog_show");
+  if (sGtkNativeDialogShowPtr != nullptr) {
+    (*sGtkNativeDialogShowPtr)(file_chooser);
+  } else {
+    g_signal_connect(file_chooser, "destroy", G_CALLBACK(OnDestroy), this);
+    gtk_widget_show(GTK_WIDGET(file_chooser));
+  }
+}
+
+void
+nsFilePicker::GtkFileChooserDestroy(void *file_chooser)
+{
+  static auto sGtkNativeDialogDestroyPtr = (void (*)(void *))
+    dlsym(RTLD_DEFAULT, "gtk_native_dialog_destroy");
+  if (sGtkNativeDialogDestroyPtr != nullptr) {
+    (*sGtkNativeDialogDestroyPtr)(file_chooser);
+  } else {
+    gtk_widget_destroy(GTK_WIDGET(file_chooser));
+  }
+}
+
+void
+nsFilePicker::GtkFileChooserSetModal(void *file_chooser,
+        GtkWindow *parent_widget, gboolean modal)
+{
+  static auto sGtkNativeDialogSetModalPtr = (void (*)(void *, gboolean))
+    dlsym(RTLD_DEFAULT, "gtk_native_dialog_set_modal");
+  if (sGtkNativeDialogSetModalPtr != nullptr) {
+    (*sGtkNativeDialogSetModalPtr)(file_chooser, modal);
+  } else {
+    GtkWindow *window = GTK_WINDOW(file_chooser);
+    gtk_window_set_modal(window, modal);
+    if (parent_widget != nullptr) {
+      gtk_window_set_destroy_with_parent(window, modal);
+    }
+  }
+}
diff --git a/widget/gtk/nsFilePicker.h b/widget/gtk/nsFilePicker.h
--- a/widget/gtk/nsFilePicker.h
+++ b/widget/gtk/nsFilePicker.h
@@ -43,22 +43,23 @@ public:
   virtual void InitNative(nsIWidget *aParent,
                           const nsAString& aTitle) override;
 
   static void Shutdown();
 
 protected:
   virtual ~nsFilePicker();
 
-  void ReadValuesFromFileChooser(GtkWidget *file_chooser);
+  void ReadValuesFromFileChooser(void *file_chooser);
 
-  static void OnResponse(GtkWidget* dialog, gint response_id,
+  static void OnResponse(void* file_chooser, gint response_id,
                          gpointer user_data);
-  static void OnDestroy(GtkWidget* dialog, gpointer user_data);
-  void Done(GtkWidget* dialog, gint response_id);
+  static void OnDestroy(GtkWidget* file_chooser, gpointer user_data);
+  void Done(void* file_chooser, gint response_id);
+
 
   nsCOMPtr<nsIWidget>    mParentWidget;
   nsCOMPtr<nsIFilePickerShownCallback> mCallback;
   nsCOMArray<nsIFile> mFiles;
 
   int16_t   mSelectedType;
   int16_t   mResult;
   bool      mRunning;
@@ -69,14 +70,23 @@ protected:
   nsString  mDefaultExtension;
 
   nsTArray<nsCString> mFilters;
   nsTArray<nsCString> mFilterNames;
 
 private:
   static nsIFile *mPrevDisplayDirectory;
 
+  void *GtkFileChooserNew(
+          const gchar *title, GtkWindow *parent,
+          GtkFileChooserAction action,
+          const gchar *accept_label);
+  void GtkFileChooserShow(void *file_chooser);
+  void GtkFileChooserDestroy(void *file_chooser);
+  void GtkFileChooserSetModal(void *file_chooser, GtkWindow* parent_widget,
+          gboolean modal);
+
 #ifdef MOZ_WIDGET_GTK
   GtkFileChooserWidget *mFileChooserDelegate;
 #endif
 };
 
 #endif
