# HG changeset patch
# User Andreas Farre <farre@mozilla.com>
# Date 1503666388 -7200
#      Fri Aug 25 15:06:28 2017 +0200
# Node ID 12d69143d17f1887ba480375542ade2974411805
# Parent  d06a801a9476843e57d0a60b072b9f3b9642d339
Bug 1393764 - Reset execution budget if BudgetThrottlingEnabled returns false. r=bkelly

When BudgetThrottlingEnabled transitions from true to false, the
execution budget can be negative which will have the unfortunate
effect of limiting the number of timeouts run by
TimeoutManager::RunTimeout to zero. By resetting the execution budget
in UpdateBudget, when BudgetThrottlingEnabled returns false, we
prevent this loop from happening.

diff --git a/dom/base/TimeoutManager.cpp b/dom/base/TimeoutManager.cpp
--- a/dom/base/TimeoutManager.cpp
+++ b/dom/base/TimeoutManager.cpp
@@ -344,17 +344,27 @@ TimeoutManager::UpdateBudget(const TimeS
   if (BudgetThrottlingEnabled(isBackground)) {
     double factor = GetRegenerationFactor(isBackground);
     TimeDuration regenerated = (aNow - mLastBudgetUpdate).MultDouble(factor);
     // Clamp the budget to the range of minimum and maximum allowed budget.
     mExecutionBudget = TimeDuration::Max(
       GetMinBudget(isBackground),
       TimeDuration::Min(GetMaxBudget(isBackground),
                         mExecutionBudget - aDuration + regenerated));
+  } else {
+    // If budget throttling isn't enabled, reset the execution budget
+    // to the max budget specified in preferences. Always doing this
+    // will catch the case of BudgetThrottlingEnabled going from
+    // returning true to returning false. This prevent us from looping
+    // in RunTimeout, due to totalTimeLimit being set to zero and no
+    // timeouts being executed, even though budget throttling isn't
+    // active at the moment.
+    mExecutionBudget = GetMaxBudget(isBackground);
   }
+
   mLastBudgetUpdate = aNow;
 }
 
 #define TRACKING_SEPARATE_TIMEOUT_BUCKETING_STRATEGY 0 // Consider all timeouts coming from tracking scripts as tracking
 // These strategies are useful for testing.
 #define ALL_NORMAL_TIMEOUT_BUCKETING_STRATEGY        1 // Consider all timeouts as normal
 #define ALTERNATE_TIMEOUT_BUCKETING_STRATEGY         2 // Put every other timeout in the list of tracking timeouts
 #define RANDOM_TIMEOUT_BUCKETING_STRATEGY            3 // Put timeouts into either the normal or tracking timeouts list randomly
