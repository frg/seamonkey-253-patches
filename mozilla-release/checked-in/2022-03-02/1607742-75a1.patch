# HG changeset patch
# User Jan Odvarko <odvarko@gmail.com>
# Date 1581508350 0
#      Wed Feb 12 11:52:30 2020 +0000
# Node ID 4e6cf4c65e2153dd212512aae8b43eea39ce4f36
# Parent  d20df530f09fa1e569b80d94511e2631b79b63eb
Bug 1607742 - Escape method argument r=Gijs

Differential Revision: https://phabricator.services.mozilla.com/D60413

diff --git a/devtools/client/shared/curl.js b/devtools/client/shared/curl.js
--- a/devtools/client/shared/curl.js
+++ b/devtools/client/shared/curl.js
@@ -54,85 +54,97 @@ const Curl = {
    *
    * @return string
    *         A cURL command.
    */
   generateCommand: function (data) {
     let utils = CurlUtils;
 
     let command = ["curl"];
+
+    // Make sure to use the following helpers to sanitize arguments before execution.
+    const addParam = value => {
+      const safe = /^[a-zA-Z-]+$/.test(value) ? value : escapeString(value);
+      command.push(safe);
+    };
+
+    const addPostData = value => {
+      const safe = /^[a-zA-Z-]+$/.test(value) ? value : escapeString(value);
+      postData.push(safe);
+    };
+
     let ignoredHeaders = new Set();
 
     // The cURL command is expected to run on the same platform that Firefox runs
     // (it may be different from the inspected page platform).
     let escapeString = Services.appinfo.OS == "WINNT" ?
                        utils.escapeStringWin : utils.escapeStringPosix;
 
     // Add URL.
-    command.push(escapeString(data.url));
+    addParam(data.url);
 
     let postDataText = null;
     let multipartRequest = utils.isMultipartRequest(data);
 
     // Create post data.
     let postData = [];
     if (multipartRequest) {
       // WINDOWS KNOWN LIMITATIONS: Due to the specificity of running curl on
       // cmd.exe even correctly escaped windows newline \r\n will be
       // treated by curl as plain local newline. It corresponds in unix
       // to single \n and that's what curl will send in payload.
       // It may be particularly hurtful for multipart/form-data payloads
       // which composed using \n only, not \r\n, may be not parsable for
       // peers which split parts of multipart payload using \r\n.
       postDataText = data.postDataText;
-      postData.push("--data-binary");
+      addPostData("--data-binary");
       let boundary = utils.getMultipartBoundary(data);
       let text = utils.removeBinaryDataFromMultipartText(postDataText, boundary);
-      postData.push(escapeString(text));
+      addPostData(text);
       ignoredHeaders.add("content-length");
     } else if (utils.isUrlEncodedRequest(data) ||
           ["PUT", "POST", "PATCH"].includes(data.method)) {
       postDataText = data.postDataText;
-      postData.push("--data");
-      postData.push(escapeString(utils.writePostDataTextParams(postDataText)));
+      addPostData("--data");
+      addPostData(utils.writePostDataTextParams(postDataText));
       ignoredHeaders.add("content-length");
     }
     // curl generates the host header itself based on the given URL
     ignoredHeaders.add("host");
 
     // Add -I (HEAD)
     // For servers that supports HEAD.
     // This will fetch the header of a document only.
     if (data.method == "HEAD") {
-      command.push("-I");
+      addParam("-I");
     } else if (!(data.method == "GET" || data.method == "POST")) {
       // Add method.
       // For HEAD, GET and POST requests this is not necessary. GET is the
       // default, if --data or --binary is added POST is used, -I implies HEAD.
-      command.push("-X");
-      command.push(data.method);
+      addParam("-X");
+      addParam(data.method);
     }
 
     // Add request headers.
     let headers = data.headers;
     if (multipartRequest) {
       let multipartHeaders = utils.getHeadersFromMultipartText(postDataText);
       headers = headers.concat(multipartHeaders);
     }
     for (let i = 0; i < headers.length; i++) {
       let header = headers[i];
       if (header.name.toLowerCase() === "accept-encoding") {
-        command.push("--compressed");
+        addParam("--compressed");
         continue;
       }
       if (ignoredHeaders.has(header.name.toLowerCase())) {
         continue;
       }
-      command.push("-H");
-      command.push(escapeString(header.name + ": " + header.value));
+      addParam("-H");
+      addParam(header.name + ": " + header.value);
     }
 
     // Add post data.
     command = command.concat(postData);
 
     return command.join(" ");
   }
 };
