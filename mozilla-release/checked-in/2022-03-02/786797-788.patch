# HG changeset patch
# User Henri Sivonen <hsivonen@hsivonen.fi>
# Date 1611248578 0
#      Thu Jan 21 17:02:58 2021 +0000
# Node ID 4d73b6090b1154f17536c26acc461ff519152e43
# Parent  f317d41c233b4602e1383ff2a9943356778cbe4a
Bug 786797 - Check for integer overflow when computing new buffer sizes. r=smaug, a=RyanVM

Differential Revision: https://phabricator.services.mozilla.com/D102592

diff --git a/parser/html/javasrc/MetaScanner.java b/parser/html/javasrc/MetaScanner.java
--- a/parser/html/javasrc/MetaScanner.java
+++ b/parser/html/javasrc/MetaScanner.java
@@ -747,17 +747,17 @@ public abstract class MetaScanner {
                                 continue;
                         }
                     }
             }
         }
         stateSave  = state;
     }
 
-    private void handleCharInAttributeValue(int c) {
+    private void handleCharInAttributeValue(int c) throws SAXException {
         if (metaState == A) {
             if (contentIndex == CONTENT.length || charsetIndex == CHARSET.length) {
                 addToBuffer(c);
             } else if (httpEquivIndex == HTTP_EQUIV.length) {
                 if (contentTypeIndex < CONTENT_TYPE.length && toAsciiLowerCase(c) == CONTENT_TYPE[contentTypeIndex]) {
                     ++contentTypeIndex;
                 } else {
                     contentTypeIndex = Integer.MAX_VALUE;
@@ -771,20 +771,21 @@ public abstract class MetaScanner {
             return c + 0x20;
         }
         return c;
     }
 
     /**
      * Adds a character to the accumulation buffer.
      * @param c the character to add
+     * @throws SAXException
      */
-    private void addToBuffer(int c) {
+    private void addToBuffer(int c) throws SAXException {
         if (strBufLen == strBuf.length) {
-            char[] newBuf = new char[strBuf.length + (strBuf.length << 1)];
+            char[] newBuf = new char[Portability.checkedAdd(strBuf.length, (strBuf.length << 1))];
             System.arraycopy(strBuf, 0, newBuf, 0, strBuf.length);
             strBuf = newBuf;
         }
         strBuf[strBufLen++] = (char)c;
     }
 
     /**
      * Attempts to extract a charset name from the accumulation buffer.
diff --git a/parser/html/javasrc/Portability.java b/parser/html/javasrc/Portability.java
--- a/parser/html/javasrc/Portability.java
+++ b/parser/html/javasrc/Portability.java
@@ -17,23 +17,36 @@
  * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
  * DEALINGS IN THE SOFTWARE.
  */
 
 package nu.validator.htmlparser.impl;
 
+import org.xml.sax.SAXException;
+
 import nu.validator.htmlparser.annotation.Literal;
 import nu.validator.htmlparser.annotation.Local;
 import nu.validator.htmlparser.annotation.NoLength;
 import nu.validator.htmlparser.common.Interner;
 
 public final class Portability {
 
+    public static int checkedAdd(int a, int b) throws SAXException {
+        // This can't be translated code, because in C++ signed integer overflow is UB, so the below code would be wrong.
+        assert a >= 0;
+        assert b >= 0;
+        int sum = a + b;
+        if (sum < a || sum < b) {
+            throw new SAXException("Integer overflow");
+        }
+        return sum;
+    }
+
     // Allocating methods
 
     /**
      * Allocates a new local name object. In C++, the refcount must be set up in such a way that
      * calling <code>releaseLocal</code> on the return value balances the refcount set by this method.
      */
     public static @Local String newLocalNameFromBuffer(@NoLength char[] buf, int offset, int length, Interner interner) {
         return new String(buf, offset, length).intern();
diff --git a/parser/html/javasrc/Tokenizer.java b/parser/html/javasrc/Tokenizer.java
--- a/parser/html/javasrc/Tokenizer.java
+++ b/parser/html/javasrc/Tokenizer.java
@@ -984,32 +984,32 @@ public class Tokenizer implements Locato
                 break;
             case FATAL:
                 fatal("The document is not mappable to XML 1.0 due to two consecutive hyphens in a comment.");
                 break;
         }
         // ]NOCPP]
     }
 
-    private void appendStrBuf(@NoLength char[] buffer, int offset, int length) {
-        int newLen = strBufLen + length;
+    private void appendStrBuf(@NoLength char[] buffer, int offset, int length) throws SAXException {
+        int newLen = Portability.checkedAdd(strBufLen, length);
         // CPPONLY: assert newLen <= strBuf.length: "Previous buffer length insufficient.";
         // CPPONLY: if (strBuf.length < newLen) {
         // CPPONLY:     if (!EnsureBufferSpace(length)) {
         // CPPONLY:         assert false: "RELEASE: Unable to recover from buffer reallocation failure";
         // CPPONLY:     } // TODO: Add telemetry when outer if fires but inner does not
         // CPPONLY: }
         System.arraycopy(buffer, offset, strBuf, strBufLen, length);
         strBufLen = newLen;
     }
 
     /**
      * Append the contents of the char reference buffer to the main one.
      */
-    @Inline private void appendCharRefBufToStrBuf() {
+    @Inline private void appendCharRefBufToStrBuf() throws SAXException {
         appendStrBuf(charRefBuf, 0, charRefBufLen);
         charRefBufLen = 0;
     }
 
     /**
      * Emits the current comment token.
      *
      * @param pos
diff --git a/parser/html/nsHtml5MetaScanner.cpp b/parser/html/nsHtml5MetaScanner.cpp
--- a/parser/html/nsHtml5MetaScanner.cpp
+++ b/parser/html/nsHtml5MetaScanner.cpp
@@ -736,17 +736,18 @@ nsHtml5MetaScanner::handleCharInAttribut
     }
   }
 }
 
 void 
 nsHtml5MetaScanner::addToBuffer(int32_t c)
 {
   if (strBufLen == strBuf.length) {
-    jArray<char16_t,int32_t> newBuf = jArray<char16_t,int32_t>::newJArray(strBuf.length + (strBuf.length << 1));
+    jArray<char16_t, int32_t> newBuf = jArray<char16_t, int32_t>::newJArray(
+        nsHtml5Portability::checkedAdd(strBuf.length, (strBuf.length << 1)));
     nsHtml5ArrayCopy::arraycopy(strBuf, newBuf, strBuf.length);
     strBuf = newBuf;
   }
   strBuf[strBufLen++] = (char16_t) c;
 }
 
 void 
 nsHtml5MetaScanner::handleAttributeValue()
diff --git a/parser/html/nsHtml5Portability.cpp b/parser/html/nsHtml5Portability.cpp
--- a/parser/html/nsHtml5Portability.cpp
+++ b/parser/html/nsHtml5Portability.cpp
@@ -2,16 +2,25 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsIAtom.h"
 #include "nsString.h"
 #include "jArray.h"
 #include "nsHtml5Portability.h"
 #include "nsHtml5TreeBuilder.h"
+#include "mozilla/CheckedInt.h"
+
+int32_t nsHtml5Portability::checkedAdd(int32_t a, int32_t b) {
+  mozilla::CheckedInt<int32_t> sum(a);
+  sum += b;
+  MOZ_RELEASE_ASSERT(sum.isValid(),
+                     "HTML input too large for signed 32-bit integer.");
+  return sum.value();
+}
 
 nsIAtom*
 nsHtml5Portability::newLocalNameFromBuffer(char16_t* buf, int32_t offset, int32_t length, nsHtml5AtomTable* interner)
 {
   NS_ASSERTION(!offset, "The offset should always be zero here.");
   NS_ASSERTION(interner, "Didn't get an atom service.");
   return interner->GetAtom(nsDependentSubstring(buf, buf + length));
 }
diff --git a/parser/html/nsHtml5Portability.h b/parser/html/nsHtml5Portability.h
--- a/parser/html/nsHtml5Portability.h
+++ b/parser/html/nsHtml5Portability.h
@@ -49,19 +49,19 @@ class nsHtml5AttributeName;
 class nsHtml5ElementName;
 class nsHtml5Tokenizer;
 class nsHtml5TreeBuilder;
 class nsHtml5MetaScanner;
 class nsHtml5UTF16Buffer;
 class nsHtml5StateSnapshot;
 
 
-class nsHtml5Portability
-{
-  public:
+class nsHtml5Portability {
+ public:
+  static int32_t checkedAdd(int32_t a, int32_t b);
     static nsIAtom* newLocalNameFromBuffer(char16_t* buf, int32_t offset, int32_t length, nsHtml5AtomTable* interner);
     static nsHtml5String newStringFromBuffer(char16_t* buf,
                                              int32_t offset,
                                              int32_t length,
                                              nsHtml5TreeBuilder* treeBuilder,
                                              bool maybeAtomize);
     static nsHtml5String newEmptyString();
     static nsHtml5String newStringFromLiteral(const char* literal);
diff --git a/parser/html/nsHtml5Tokenizer.cpp b/parser/html/nsHtml5Tokenizer.cpp
--- a/parser/html/nsHtml5Tokenizer.cpp
+++ b/parser/html/nsHtml5Tokenizer.cpp
@@ -253,20 +253,19 @@ void
 nsHtml5Tokenizer::emitStrBuf()
 {
   if (strBufLen > 0) {
     tokenHandler->characters(strBuf, 0, strBufLen);
     clearStrBufAfterUse();
   }
 }
 
-void 
-nsHtml5Tokenizer::appendStrBuf(char16_t* buffer, int32_t offset, int32_t length)
-{
-  int32_t newLen = strBufLen + length;
+void nsHtml5Tokenizer::appendStrBuf(char16_t* buffer, int32_t offset,
+                                    int32_t length) {
+  int32_t newLen = nsHtml5Portability::checkedAdd(strBufLen, length);
   MOZ_ASSERT(newLen <= strBuf.length, "Previous buffer length insufficient.");
   if (MOZ_UNLIKELY(strBuf.length < newLen)) {
     if (MOZ_UNLIKELY(!EnsureBufferSpace(length))) {
       MOZ_CRASH("Unable to recover from buffer reallocation failure");
     }
   }
   nsHtml5ArrayCopy::arraycopy(buffer, offset, strBuf, strBufLen, length);
   strBufLen = newLen;
