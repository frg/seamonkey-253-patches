# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1504110138 14400
# Node ID 99de93a9896b7e3075e488b4e33cc277e322a4d2
# Parent  78f2c43be4ebcf396d307979bd6a95f307062ba5
Bug 1395218 - Make the bevel side parameters more strongly-typed. r=dholbert

MozReview-Commit-ID: 5rymg5uTMC1

diff --git a/layout/painting/nsCSSRendering.cpp b/layout/painting/nsCSSRendering.cpp
--- a/layout/painting/nsCSSRendering.cpp
+++ b/layout/painting/nsCSSRendering.cpp
@@ -3370,19 +3370,19 @@ DrawDashedSegment(DrawTarget&          a
   }
 }
 
 static void
 DrawSolidBorderSegment(DrawTarget&          aDrawTarget,
                        nsRect               aRect,
                        nscolor              aColor,
                        int32_t              aAppUnitsPerDevPixel,
-                       uint8_t              aStartBevelSide = 0,
+                       mozilla::Side        aStartBevelSide = mozilla::eSideTop,
                        nscoord              aStartBevelOffset = 0,
-                       uint8_t              aEndBevelSide = 0,
+                       mozilla::Side        aEndBevelSide = mozilla::eSideTop,
                        nscoord              aEndBevelOffset = 0)
 {
   ColorPattern color(ToDeviceColor(aColor));
   DrawOptions drawOptions(1.f, CompositionOp::OP_OVER, AntialiasMode::NONE);
 
   nscoord oneDevPixel = NSIntPixelsToAppUnits(1, aAppUnitsPerDevPixel);
   // We don't need to bevel single pixel borders
   if ((aRect.width == oneDevPixel) || (aRect.height == oneDevPixel) ||
@@ -3467,19 +3467,19 @@ GetDashInfo(nscoord  aBorderLength,
 
 void
 nsCSSRendering::DrawTableBorderSegment(DrawTarget&   aDrawTarget,
                                        uint8_t       aBorderStyle,
                                        nscolor       aBorderColor,
                                        nscolor       aBGColor,
                                        const nsRect& aBorder,
                                        int32_t       aAppUnitsPerDevPixel,
-                                       uint8_t       aStartBevelSide,
+                                       mozilla::Side aStartBevelSide,
                                        nscoord       aStartBevelOffset,
-                                       uint8_t       aEndBevelSide,
+                                       mozilla::Side aEndBevelSide,
                                        nscoord       aEndBevelOffset)
 {
   bool horizontal = ((eSideTop == aStartBevelSide) || (eSideBottom == aStartBevelSide));
   nscoord oneDevPixel = NSIntPixelsToAppUnits(1, aAppUnitsPerDevPixel);
   uint8_t ridgeGroove = NS_STYLE_BORDER_STYLE_RIDGE;
 
   if ((oneDevPixel >= aBorder.width) || (oneDevPixel >= aBorder.height) ||
       (NS_STYLE_BORDER_STYLE_DASHED == aBorderStyle) || (NS_STYLE_BORDER_STYLE_DOTTED == aBorderStyle)) {
diff --git a/layout/painting/nsCSSRendering.h b/layout/painting/nsCSSRendering.h
--- a/layout/painting/nsCSSRendering.h
+++ b/layout/painting/nsCSSRendering.h
@@ -545,19 +545,19 @@ struct nsCSSRendering {
   // Draw a border segment in the table collapsing border model without
   // beveling corners
   static void DrawTableBorderSegment(DrawTarget&   aDrawTarget,
                                      uint8_t       aBorderStyle,
                                      nscolor       aBorderColor,
                                      nscolor       aBGColor,
                                      const nsRect& aBorderRect,
                                      int32_t       aAppUnitsPerDevPixel,
-                                     uint8_t       aStartBevelSide = 0,
+                                     mozilla::Side aStartBevelSide = mozilla::eSideTop,
                                      nscoord       aStartBevelOffset = 0,
-                                     uint8_t       aEndBevelSide = 0,
+                                     mozilla::Side aEndBevelSide = mozilla::eSideTop,
                                      nscoord       aEndBevelOffset = 0);
 
   // NOTE: pt, dirtyRect, lineSize, ascent, offset in the following
   //       structs are non-rounded device pixels, not app units.
   struct DecorationRectParams
   {
     // The width [length] and the height [thickness] of the decoration
     // line. This is a "logical" size in textRun orientation, so that
diff --git a/layout/tables/nsTableFrame.cpp b/layout/tables/nsTableFrame.cpp
--- a/layout/tables/nsTableFrame.cpp
+++ b/layout/tables/nsTableFrame.cpp
@@ -6475,19 +6475,19 @@ class BCPaintBorderIterator;
 
 struct BCBorderParameters
 {
   uint8_t mBorderStyle;
   nscolor mBorderColor;
   nscolor mBGColor;
   nsRect mBorderRect;
   int32_t mAppUnitsPerDevPixel;
-  uint8_t mStartBevelSide;
+  mozilla::Side mStartBevelSide;
   nscoord mStartBevelOffset;
-  uint8_t mEndBevelSide;
+  mozilla::Side mEndBevelSide;
   nscoord mEndBevelOffset;
 };
 
 struct BCBlockDirSeg
 {
   BCBlockDirSeg();
 
   void Start(BCPaintBorderIterator& aIter,
