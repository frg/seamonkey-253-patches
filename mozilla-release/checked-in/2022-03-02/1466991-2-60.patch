# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1529018549 14400
#      Thu Jun 14 19:22:29 2018 -0400
# Node ID 0915c8e08aa926e892c22d7d6c4944945b1828f8
# Parent  91bf37bcf8b2e25caa10fc691f3426d5036dce15
Bug 1466991 - Part 2: Reparent nodes when they start being in the XBL scope. r=smaug, a=RyanVM

diff --git a/dom/base/nsINode.cpp b/dom/base/nsINode.cpp
--- a/dom/base/nsINode.cpp
+++ b/dom/base/nsINode.cpp
@@ -23,16 +23,17 @@
 #include "mozilla/ServoBindings.h"
 #include "mozilla/Telemetry.h"
 #include "mozilla/TextEditor.h"
 #include "mozilla/TimeStamp.h"
 #include "mozilla/css/StyleRule.h"
 #include "mozilla/dom/Element.h"
 #include "mozilla/dom/Event.h"
 #include "mozilla/dom/ShadowRoot.h"
+#include "mozilla/dom/ScriptSettings.h"
 #include "nsAttrValueOrString.h"
 #include "nsBindingManager.h"
 #include "nsCCUncollectableMarker.h"
 #include "nsContentCreatorFunctions.h"
 #include "nsContentList.h"
 #include "nsContentUtils.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsDocument.h"
@@ -1571,16 +1572,56 @@ CheckForOutdatedParent(nsINode* aParent,
       nsresult rv = ReparentWrapper(cx, existingObj);
       NS_ENSURE_SUCCESS(rv, rv);
     }
   }
 
   return NS_OK;
 }
 
+static nsresult
+ReparentWrappersInSubtree(nsIContent* aRoot)
+{
+  MOZ_ASSERT(ShouldUseXBLScope(aRoot));
+  // Start off with no global so we don't fire any error events on failure.
+  AutoJSAPI jsapi;
+  jsapi.Init();
+
+  JSContext* cx = jsapi.cx();
+
+  nsIGlobalObject* docGlobal = aRoot->OwnerDoc()->GetScopeObject();
+  if (NS_WARN_IF(!docGlobal)) {
+    return NS_ERROR_UNEXPECTED;
+  }
+
+  JS::Rooted<JSObject*> rootedGlobal(cx, docGlobal->GetGlobalJSObject());
+  if (NS_WARN_IF(!rootedGlobal)) {
+    return NS_ERROR_UNEXPECTED;
+  }
+
+  rootedGlobal = xpc::GetXBLScope(cx, rootedGlobal);
+
+  nsresult rv;
+  JS::Rooted<JSObject*> reflector(cx);
+  for (nsIContent* cur = aRoot; cur; cur = cur->GetNextNode(aRoot)) {
+    if ((reflector = cur->GetWrapper())) {
+      JSAutoCompartment ac(cx, reflector);
+      rv = ReparentWrapper(cx, reflector);
+      // We _could_ consider BlastSubtreeToPieces here, but it's not really
+      // needed.  Having some nodes in here accessible to content while others
+      // are not is probably OK.  We just need to fail out of the actual
+      // insertion, so they're not in the DOM.  Returning a failure here will
+      // do that.
+      NS_ENSURE_SUCCESS(rv, rv);
+    }
+  }
+
+  return NS_OK;
+}
+
 nsresult
 nsINode::doInsertChildAt(nsIContent* aKid, uint32_t aIndex,
                          bool aNotify, nsAttrAndChildArray& aChildArray)
 {
   NS_PRECONDITION(!aKid->GetParentNode(),
                   "Inserting node that already has parent");
   nsresult rv;
 
@@ -1608,19 +1649,25 @@ nsINode::doInsertChildAt(nsIContent* aKi
   NS_ENSURE_SUCCESS(rv, rv);
   if (aIndex == 0) {
     mFirstChild = aKid;
   }
 
   nsIContent* parent =
     IsNodeOfType(eDOCUMENT) ? nullptr : static_cast<nsIContent*>(this);
 
+  bool wasInXBLScope = ShouldUseXBLScope(aKid);
   rv = aKid->BindToTree(doc, parent,
                         parent ? parent->GetBindingParent() : nullptr,
                         true);
+  if (NS_SUCCEEDED(rv) && !wasInXBLScope && ShouldUseXBLScope(aKid)) {
+    MOZ_ASSERT(ShouldUseXBLScope(this),
+               "Why does the kid need to use an XBL scope?");
+    rv = ReparentWrappersInSubtree(aKid);
+  }
   if (NS_FAILED(rv)) {
     if (GetFirstChild() == aKid) {
       mFirstChild = aKid->GetNextSibling();
     }
     aChildArray.RemoveChildAt(aIndex);
     aKid->UnbindFromTree();
     return rv;
   }
