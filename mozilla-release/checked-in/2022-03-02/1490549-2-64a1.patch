# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1536946852 0
# Node ID 394ff9e067c89cb615b304423f85b081686f5ca1
# Parent  da6045abbf07360eaa0906c5e778beea1cc23d88
Bug 1490549 - Change how host compilers are found r=froydnj

Before this change, we'd derive a host compiler and handle things as if
HOST_CC/HOST_CXX had been passed. With this change, we change the list
of compilers that are tested with check_prog instead.

Depends on D5828

Differential Revision: https://phabricator.services.mozilla.com/D5829

diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -457,16 +457,24 @@ def get_compiler_info(compiler, language
         cpu=data.get('CPU'),
         kernel=data.get('KERNEL'),
         endianness=data.get('ENDIANNESS'),
         language='C++' if cplusplus else 'C',
         language_version=cplusplus if cplusplus else stdc_version,
     )
 
 
+def same_arch_different_bits():
+    return (
+        ('x86', 'x86_64'),
+        ('ppc', 'ppc64'),
+        ('sparc', 'sparc64'),
+    )
+
+
 @imports(_from='mozbuild.shellutil', _import='quote')
 def check_compiler(compiler, language, target):
     info = get_compiler_info(compiler, language)
 
     flags = []
 
     def append_flag(flag):
         if flag not in flags:
@@ -507,24 +515,20 @@ def check_compiler(compiler, language, t
             append_flag('--target=%s' % target.toolchain)
         elif info.type == 'clang-cl':
             # Ideally this would share the 'clang' branch above, but on Windows
             # the --target needs additional data like ms-compatibility-version.
             if (info.cpu, target.cpu) == ('x86_64', 'x86'):
                 # -m32 does not use -Xclang, so add it directly.
                 flags.append('-m32')
         elif info.type == 'gcc':
-            same_arch_different_bits = (
-                ('x86', 'x86_64'),
-                ('ppc', 'ppc64'),
-                ('sparc', 'sparc64'),
-            )
-            if (target.cpu, info.cpu) in same_arch_different_bits:
+            same_arch = same_arch_different_bits()
+            if (target.cpu, info.cpu) in same_arch:
                 append_flag('-m32')
-            elif (info.cpu, target.cpu) in same_arch_different_bits:
+            elif (info.cpu, target.cpu) in same_arch:
                 append_flag('-m64')
 
     if not info.kernel or info.kernel != target.kernel:
         if info.type == 'clang':
             append_flag('--target=%s' % target.toolchain)
 
     if not info.endianness or info.endianness != target.endianness:
         if info.type == 'clang':
@@ -627,53 +631,112 @@ def toolchain_search_path(vc_compiler_pa
         result.extend(vc_compiler_path)
         # We're going to alter PATH for good in windows.configure, but we also
         # need to do it for the valid_compiler() check below.
         os.environ['PATH'] = os.pathsep.join(result)
         return result
 
 
 @template
-def default_c_compilers(host_or_target):
+def default_c_compilers(host_or_target, other_c_compiler=None):
     '''Template defining the set of default C compilers for the host and
     target platforms.
     `host_or_target` is either `host` or `target` (the @depends functions
     from init.configure.
+    `other_c_compiler` is the `target` C compiler when `host_or_target` is `host`.
     '''
     assert host_or_target in {host, target}
 
-    @depends(host_or_target, target, toolchain_prefix)
-    def default_c_compilers(host_or_target, target, toolchain_prefix):
+    other_c_compiler = () if other_c_compiler is None else (other_c_compiler,)
+
+    @depends(host_or_target, target, toolchain_prefix, *other_c_compiler)
+    def default_c_compilers(host_or_target, target, toolchain_prefix,
+                            *other_c_compiler):
+        if host_or_target.kernel == 'WINNT':
+            supported = types = ('clang-cl', 'msvc', 'gcc', 'clang')
+        elif host_or_target.kernel == 'Darwin':
+            types = ('clang',)
+            supported = ('clang', 'gcc')
+        else:
+            supported = types = ('gcc', 'clang')
+
+        info = other_c_compiler[0] if other_c_compiler else None
+        if info and info.type in supported:
+            # When getting default C compilers for the host, we prioritize the
+            # same compiler as the target C compiler.
+            prioritized = info.compiler
+            if info.type == 'gcc':
+                same_arch = same_arch_different_bits()
+                if (target.cpu != host_or_target.cpu and
+                        (target.cpu, host_or_target.cpu) not in same_arch and
+                        (host_or_target.cpu, target.cpu) not in same_arch):
+                    # If the target C compiler if GCC, and it can't be used with
+                    # -m32/-m64 for the host, it's probably toolchain-prefixed,
+                    # so we prioritize a raw 'gcc' instead.
+                    prioritized = info.type
+            # elif info.type == 'clang' and android_clang_compiler:
+            # Android NDK clangs do not function as host compiler, so
+            # prioritize a raw 'clang' instead.
+            # prioritized = info.type
+
+            types = [prioritized] + [t for t in types if t != info.type]
+
         gcc = ('gcc',)
         if toolchain_prefix and host_or_target is target:
             gcc = tuple('%sgcc' % p for p in toolchain_prefix) + gcc
 
-        if host_or_target.kernel == 'WINNT':
-            return ('clang-cl', 'cl') + gcc + ('clang',)
-        if host_or_target.kernel == 'Darwin':
-            return ('clang',)
-        return gcc + ('clang',)
+        result = []
+        for type in types:
+            # Android sets toolchain_prefix and android_clang_compiler, but
+            # we want the latter to take precedence, because the latter can
+            # point at clang, which is what we want to use.
+            # if type == 'clang' and android_clang_compiler and host_or_target is target:
+            #    result.append(android_clang_compiler)
+            # elif type == 'gcc':
+            if type == 'gcc':
+                result.extend(gcc)
+            elif type == 'msvc':
+                result.append('cl')
+            else:
+                result.append(type)
+
+        return tuple(result)
 
     return default_c_compilers
 
 
 @template
-def default_cxx_compilers(c_compiler):
+def default_cxx_compilers(c_compiler, other_c_compiler=None, other_cxx_compiler=None):
     '''Template defining the set of default C++ compilers for the host and
     target platforms.
     `c_compiler` is the @depends function returning a Compiler instance for
     the desired platform.
 
     Because the build system expects the C and C++ compilers to be from the
     same compiler suite, we derive the default C++ compilers from the C
     compiler that was found if none was provided.
+
+    We also factor the target C++ compiler when getting the default host
+    C++ compiler, using the same if the corresponding C compilers are the
+    same.
     '''
 
-    @depends(c_compiler)
-    def default_cxx_compilers(c_compiler):
+    assert (other_c_compiler is None) == (other_cxx_compiler is None)
+    if other_c_compiler is not None:
+        other_compilers = (other_c_compiler, other_cxx_compiler)
+    else:
+        other_compilers = ()
+
+    @depends(c_compiler, *other_compilers)
+    def default_cxx_compilers(c_compiler, *other_compilers):
+        if other_compilers:
+            other_c_compiler, other_cxx_compiler = other_compilers
+            if other_c_compiler.compiler == c_compiler.compiler:
+                return (other_cxx_compiler.compiler,)
+
         dir = os.path.dirname(c_compiler.compiler)
         file = os.path.basename(c_compiler.compiler)
 
         if c_compiler.type == 'gcc':
             return (os.path.join(dir, file.replace('gcc', 'g++')),)
 
         if c_compiler.type == 'clang':
             return (os.path.join(dir, file.replace('clang', 'clang++')),)
@@ -713,18 +776,18 @@ def compiler(language, host_or_target, c
     var = {
         ('C', target): 'CC',
         ('C++', target): 'CXX',
         ('C', host): 'HOST_CC',
         ('C++', host): 'HOST_CXX',
     }[language, host_or_target]
 
     default_compilers = {
-        'C': lambda: default_c_compilers(host_or_target),
-        'C++': lambda: default_cxx_compilers(c_compiler),
+        'C': lambda: default_c_compilers(host_or_target, other_compiler),
+        'C++': lambda: default_cxx_compilers(c_compiler, other_c_compiler, other_compiler),
     }[language]()
 
     what = 'the %s %s compiler' % (host_or_target_str, language)
 
     option(env=var, nargs=1, help='Path to %s' % what)
 
     # Handle the compiler given by the user through one of the CC/CXX/HOST_CC/
     # HOST_CXX variables.
@@ -742,42 +805,16 @@ def compiler(language, host_or_target, c
         without_flags = list(takewhile(lambda x: not x.startswith('-'), cmd))
 
         return namespace(
             wrapper=without_flags[:-1],
             compiler=without_flags[-1],
             flags=cmd[len(without_flags):],
         )
 
-    # Derive the host compiler from the corresponding target compiler when no
-    # explicit compiler was given and we're not cross compiling. For the C++
-    # compiler, though, prefer to derive from the host C compiler when it
-    # doesn't match the target C compiler.
-    # As a special case, since clang supports all kinds of targets in the same
-    # executable, when cross compiling with clang, default to the same compiler
-    # as the target compiler, resetting flags.
-    if host_or_target is host:
-        if other_c_compiler is not None:
-            args = (c_compiler, other_c_compiler)
-        else:
-            args = ()
-
-        @depends(provided_compiler, other_compiler, cross_compiling, *args)
-        def provided_compiler(value, other_compiler, cross_compiling, *args):
-            if value:
-                return value
-            c_compiler, other_c_compiler = args if args else (None, None)
-            if not cross_compiling and c_compiler == other_c_compiler:
-                return other_compiler
-            if cross_compiling and other_compiler.type == 'clang':
-                return namespace(**{
-                    k: [] if k == 'flags' else v
-                    for k, v in other_compiler.__dict__.iteritems()
-                })
-
     # Normally, we'd use `var` instead of `_var`, but the interaction with
     # old-configure complicates things, and for now, we a) can't take the plain
     # result from check_prog as CC/CXX/HOST_CC/HOST_CXX and b) have to let
     # old-configure AC_SUBST it (because it's autoconf doing it, not us)
     compiler = check_prog('_%s' % var, what=what, progs=default_compilers,
                           input=provided_compiler.compiler,
                           paths=toolchain_search_path)
 
