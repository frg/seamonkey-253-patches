# HG changeset patch
# User L. David Baron <dbaron@dbaron.org>
# Date 1507227357 25200
# Node ID 3d7bbd9c6c096c50fb068a2f0ab68111d38da0f1
# Parent  254559968e7a3b585e1cff3e690cda4bbafe1363
Bug 1405875 - Remove debug file mechanism from printing.  r=mats

This was used only by nsRegressionTester::DumpFrameModel, which was
removed in the previous patch.

MozReview-Commit-ID: I8fS7vzlFQw

diff --git a/docshell/base/nsIContentViewerFile.idl b/docshell/base/nsIContentViewerFile.idl
--- a/docshell/base/nsIContentViewerFile.idl
+++ b/docshell/base/nsIContentViewerFile.idl
@@ -21,11 +21,10 @@ interface nsIWebProgressListener;
  */
 
 [scriptable, uuid(564a3276-6228-401e-9b5c-d82cb382a60f)]
 interface nsIContentViewerFile : nsISupports
 {
   readonly attribute boolean printable;
 
   [noscript] void print(in boolean aSilent,
-                        in FILE    aDebugFile, 
                         in nsIPrintSettings aPrintSettings);
 };
diff --git a/layout/base/nsDocumentViewer.cpp b/layout/base/nsDocumentViewer.cpp
--- a/layout/base/nsDocumentViewer.cpp
+++ b/layout/base/nsDocumentViewer.cpp
@@ -388,19 +388,16 @@ protected:
   nsCOMPtr<nsIWebProgressListener> mCachedPrintWebProgressListner;
 
   RefPtr<nsPrintEngine>          mPrintEngine;
   float                            mOriginalPrintPreviewScale;
   float                            mPrintPreviewZoom;
   nsAutoPtr<AutoPrintEventDispatcher> mAutoBeforeAndAfterPrint;
 #endif // NS_PRINT_PREVIEW
 
-#ifdef DEBUG
-  FILE* mDebugFile;
-#endif // DEBUG
 #endif // NS_PRINTING
 
   /* character set member data */
   int32_t mHintCharsetSource;
   const Encoding* mHintCharset;
   const Encoding* mForceCharacterSet;
 
   bool mIsPageMode;
@@ -496,20 +493,16 @@ void nsDocumentViewer::PrepareToStartLoa
   if (mPrintEngine) {
     mPrintEngine->Destroy();
     mPrintEngine = nullptr;
 #ifdef NS_PRINT_PREVIEW
     SetIsPrintPreview(false);
 #endif
   }
 
-#ifdef DEBUG
-  mDebugFile = nullptr;
-#endif
-
 #endif // NS_PRINTING
 }
 
 nsDocumentViewer::nsDocumentViewer()
   : mParentWidget(nullptr),
     mAttachedToParent(false),
     mTextZoom(1.0),
     mPageZoom(1.0),
@@ -527,19 +520,16 @@ nsDocumentViewer::nsDocumentViewer()
     mClosingWhilePrinting(false),
 #if NS_PRINT_PREVIEW
     mPrintPreviewZoomed(false),
     mPrintIsPending(false),
     mPrintDocIsFullyLoaded(false),
     mOriginalPrintPreviewScale(0.0),
     mPrintPreviewZoom(1.0),
 #endif // NS_PRINT_PREVIEW
-#ifdef DEBUG
-    mDebugFile(nullptr),
-#endif // DEBUG
 #endif // NS_PRINTING
     mHintCharsetSource(kCharsetUninitialized),
     mHintCharset(nullptr),
     mForceCharacterSet(nullptr),
     mIsPageMode(false),
     mInitializedForPrintPreview(false),
     mHidden(false)
 {
@@ -2840,26 +2830,24 @@ NS_IMETHODIMP nsDocumentViewer::SetComma
  * nsIContentViewerFile
  * ======================================================================================== */
 /** ---------------------------------------------------
  *  See documentation above in the nsIContentViewerfile class definition
  *	@update 01/24/00 dwc
  */
 NS_IMETHODIMP
 nsDocumentViewer::Print(bool              aSilent,
-                          FILE *            aDebugFile,
-                          nsIPrintSettings* aPrintSettings)
+                        nsIPrintSettings* aPrintSettings)
 {
 #ifdef NS_PRINTING
   nsCOMPtr<nsIPrintSettings> printSettings;
 
 #ifdef DEBUG
   nsresult rv = NS_ERROR_FAILURE;
 
-  mDebugFile = aDebugFile;
   // if they don't pass in a PrintSettings, then make one
   // it will have all the default values
   printSettings = aPrintSettings;
   nsCOMPtr<nsIPrintSettingsService> printSettingsSvc
     = do_GetService("@mozilla.org/gfx/printsettings-service;1", &rv);
   if (NS_SUCCEEDED(rv)) {
     // if they don't pass in a PrintSettings, then make one
     if (printSettings == nullptr) {
@@ -3991,23 +3979,17 @@ nsDocumentViewer::Print(nsIPrintSettings
   RefPtr<nsPrintEngine> printEngine = mPrintEngine;
   if (!printEngine) {
     NS_ENSURE_STATE(mDeviceContext);
     printEngine = new nsPrintEngine();
 
     rv = printEngine->Initialize(this, mContainer, mDocument,
                                  float(mDeviceContext->AppUnitsPerCSSInch()) /
                                  float(mDeviceContext->AppUnitsPerDevPixel()) /
-                                 mPageZoom,
-#ifdef DEBUG
-                                 mDebugFile
-#else
-                                 nullptr
-#endif
-                                 );
+                                 mPageZoom);
     if (NS_FAILED(rv)) {
       printEngine->Destroy();
       return rv;
     }
     mPrintEngine = printEngine;
   }
   if (printEngine->HasPrintCallbackCanvas()) {
     // Postpone the 'afterprint' event until after the mozPrintCallback
@@ -4083,23 +4065,17 @@ nsDocumentViewer::PrintPreview(nsIPrintS
   // a local variable, so that it won't be deleted during its own method.
   RefPtr<nsPrintEngine> printEngine = mPrintEngine;
   if (!printEngine) {
     printEngine = new nsPrintEngine();
 
     rv = printEngine->Initialize(this, mContainer, doc,
                                  float(mDeviceContext->AppUnitsPerCSSInch()) /
                                  float(mDeviceContext->AppUnitsPerDevPixel()) /
-                                 mPageZoom,
-#ifdef DEBUG
-                                 mDebugFile
-#else
-                                 nullptr
-#endif
-                                 );
+                                 mPageZoom);
     if (NS_FAILED(rv)) {
       printEngine->Destroy();
       return rv;
     }
     mPrintEngine = printEngine;
   }
   if (autoBeforeAndAfterPrint &&
       printEngine->HasPrintCallbackCanvas()) {
diff --git a/layout/printing/nsPrintData.cpp b/layout/printing/nsPrintData.cpp
--- a/layout/printing/nsPrintData.cpp
+++ b/layout/printing/nsPrintData.cpp
@@ -21,17 +21,16 @@ static mozilla::LazyLogModule gPrintingL
 
 #define PR_PL(_p1)  MOZ_LOG(gPrintingLog, mozilla::LogLevel::Debug, _p1);
 
 //---------------------------------------------------
 //-- nsPrintData Class Impl
 //---------------------------------------------------
 nsPrintData::nsPrintData(ePrintDataType aType)
   : mType(aType)
-  , mDebugFilePtr(nullptr)
   , mPrintDocList(0)
   , mIsIFrameSelected(false)
   , mIsParentAFrameSet(false)
   , mOnStartSent(false)
   , mIsAborted(false)
   , mPreparingForPrint(false)
   , mDocWasToBeDestroyed(false)
   , mShrinkToFit(false)
@@ -65,17 +64,17 @@ nsPrintData::~nsPrintData()
     NS_RELEASE(mPPEventListeners);
   }
 
   // Only Send an OnEndPrinting if we have started printing
   if (mOnStartSent && mType != eIsPrintPreview) {
     OnEndPrinting();
   }
 
-  if (mPrintDC && !mDebugFilePtr) {
+  if (mPrintDC) {
     PR_PL(("****************** End Document ************************\n"));
     PR_PL(("\n"));
     bool isCancelled = false;
     mPrintSettings->GetIsCancelled(&isCancelled);
 
     nsresult rv = NS_OK;
     if (mType == eIsPrinting &&
         mPrintDC->IsCurrentlyPrintingDocument()) {
diff --git a/layout/printing/nsPrintData.h b/layout/printing/nsPrintData.h
--- a/layout/printing/nsPrintData.h
+++ b/layout/printing/nsPrintData.h
@@ -52,17 +52,16 @@ public:
                           bool         aDoStartStop,
                           int32_t      aFlag);
 
   void DoOnStatusChange(nsresult aStatus);
 
 
   ePrintDataType               mType;            // the type of data this is (Printing or Print Preview)
   RefPtr<nsDeviceContext>   mPrintDC;
-  FILE                        *mDebugFilePtr;    // a file where information can go to when printing
 
   mozilla::UniquePtr<nsPrintObject> mPrintObject;
 
   nsCOMArray<nsIWebProgressListener> mPrintProgressListeners;
   nsCOMPtr<nsIPrintProgressParams> mPrintProgressParams;
 
   nsCOMPtr<nsPIDOMWindowOuter> mCurrentFocusWin; // cache a pointer to the currently focused window
 
diff --git a/layout/printing/nsPrintEngine.cpp b/layout/printing/nsPrintEngine.cpp
--- a/layout/printing/nsPrintEngine.cpp
+++ b/layout/printing/nsPrintEngine.cpp
@@ -212,17 +212,16 @@ NS_IMPL_ISUPPORTS(nsPrintEngine, nsIWebP
 //---------------------------------------------------
 nsPrintEngine::nsPrintEngine()
   : mIsCreatingPrintPreview(false)
   , mIsDoingPrinting(false)
   , mIsDoingPrintPreview(false)
   , mProgressDialogIsShown(false)
   , mScreenDPI(115.0f)
   , mPagePrintTimer(nullptr)
-  , mDebugFile(nullptr)
   , mLoadCounter(0)
   , mDidLoadDataForPrinting(false)
   , mIsDestroying(false)
   , mDisallowSelectionPrint(false)
 {
 }
 
 //-------------------------------------------------------
@@ -258,30 +257,27 @@ void nsPrintEngine::DestroyPrintingData(
 //---------------------------------------------------------------------------------
 //-- Section: Methods needed by the DocViewer
 //---------------------------------------------------------------------------------
 
 //--------------------------------------------------------
 nsresult nsPrintEngine::Initialize(nsIDocumentViewerPrint* aDocViewerPrint,
                                    nsIDocShell*            aContainer,
                                    nsIDocument*            aDocument,
-                                   float                   aScreenDPI,
-                                   FILE*                   aDebugFile)
+                                   float                   aScreenDPI)
 {
   NS_ENSURE_ARG_POINTER(aDocViewerPrint);
   NS_ENSURE_ARG_POINTER(aContainer);
   NS_ENSURE_ARG_POINTER(aDocument);
 
   mDocViewerPrint = aDocViewerPrint;
   mContainer      = do_GetWeakReference(aContainer);
   mDocument       = aDocument;
   mScreenDPI      = aScreenDPI;
 
-  mDebugFile      = aDebugFile;      // ok to be nullptr
-
   return NS_OK;
 }
 
 //-------------------------------------------------------
 bool
 nsPrintEngine::CheckBeforeDestroy()
 {
   if (mPrt && mPrt->mPreparingForPrint) {
@@ -592,20 +588,16 @@ nsPrintEngine::DoCommonPrint(bool       
   }
 
   nsScriptSuppressor scriptSuppressor(this);
   // If printing via parent we still call ShowPrintDialog even for print preview
   // because we use that to retrieve the print settings from the printer.
   // The dialog is not shown, but this means we don't need to access the printer
   // driver from the child, which causes sandboxing issues.
   if (!aIsPrintPreview || printingViaParent) {
-#ifdef DEBUG
-    printData->mDebugFilePtr = mDebugFile;
-#endif
-
     scriptSuppressor.Suppress();
     bool printSilently;
     printData->mPrintSettings->GetPrintSilent(&printSilently);
 
     // Check prefs for a default setting as to whether we should print silently
     printSilently =
       Preferences::GetBool("print.always_print_silent", printSilently);
 
@@ -1857,17 +1849,17 @@ nsPrintEngine::SetupToPrintContent()
     }
   }
 
   nsresult rv = NS_OK;
   // BeginDocument may pass back a FAILURE code
   // i.e. On Windows, if you are printing to a file and hit "Cancel"
   //      to the "File Name" dialog, this comes back as an error
   // Don't start printing when regression test are executed
-  if (!printData->mDebugFilePtr && mIsDoingPrinting) {
+  if (mIsDoingPrinting) {
     rv = printData->mPrintDC->BeginDocument(docTitleStr, fileNameStr, startPage,
                                             endPage);
   }
 
   if (mIsCreatingPrintPreview) {
     // Copy docTitleStr and docURLStr to the pageSequenceFrame, to be displayed
     // in the header
     nsIPageSequenceFrame* seqFrame =
@@ -2569,133 +2561,121 @@ nsPrintEngine::DoPrint(const UniquePtr<n
 
     // Ask the page sequence frame to print all the pages
     nsIPageSequenceFrame* pageSequence = poPresShell->GetPageSequenceFrame();
     NS_ASSERTION(nullptr != pageSequence, "no page sequence frame");
 
     // We are done preparing for printing, so we can turn this off
     printData->mPreparingForPrint = false;
 
-    // printData->mDebugFilePtr this is onlu non-null when compiled for
-    // debugging
-    if (printData->mDebugFilePtr) {
-#ifdef DEBUG
-      // output the regression test
-      nsIFrame* root = poPresShell->FrameManager()->GetRootFrame();
-      root->DumpRegressionData(poPresContext, printData->mDebugFilePtr, 0);
-      fclose(printData->mDebugFilePtr);
-      SetIsPrinting(false);
-#endif
-    } else {
 #ifdef EXTENDED_DEBUG_PRINTING
-      nsIFrame* rootFrame = poPresShell->FrameManager()->GetRootFrame();
-      if (aPO->IsPrintable()) {
-        nsAutoCString docStr;
-        nsAutoCString urlStr;
-        GetDocTitleAndURL(aPO, docStr, urlStr);
-        DumpLayoutData(docStr.get(), urlStr.get(), poPresContext,
-                       printData->mPrintDocDC, rootFrame, docShell, nullptr);
-      }
+    nsIFrame* rootFrame = poPresShell->FrameManager()->GetRootFrame();
+    if (aPO->IsPrintable()) {
+      nsAutoCString docStr;
+      nsAutoCString urlStr;
+      GetDocTitleAndURL(aPO, docStr, urlStr);
+      DumpLayoutData(docStr.get(), urlStr.get(), poPresContext,
+                     printData->mPrintDocDC, rootFrame, docShell, nullptr);
+    }
 #endif
 
-      if (!printData->mPrintSettings) {
-        // not sure what to do here!
-        SetIsPrinting(false);
-        return NS_ERROR_FAILURE;
-      }
-
-      nsAutoString docTitleStr;
-      nsAutoString docURLStr;
-      GetDisplayTitleAndURL(aPO, docTitleStr, docURLStr, eDocTitleDefBlank);
-
-      if (nsIPrintSettings::kRangeSelection == printRangeType) {
-        CloneSelection(aPO->mDocument->GetOriginalDocument(), aPO->mDocument);
-
-        poPresContext->SetIsRenderingOnlySelection(true);
-        // temporarily creating rendering context
-        // which is needed to find the selection frames
-        // mPrintDC must have positive width and height for this call
-
-        // find the starting and ending page numbers
-        // via the selection
-        nsIFrame* startFrame;
-        nsIFrame* endFrame;
-        int32_t   startPageNum;
-        int32_t   endPageNum;
-        nsRect    startRect;
-        nsRect    endRect;
-
-        rv = GetPageRangeForSelection(pageSequence,
-                                      &startFrame, startPageNum, startRect,
-                                      &endFrame, endPageNum, endRect);
-        if (NS_SUCCEEDED(rv)) {
-          printData->mPrintSettings->SetStartPageRange(startPageNum);
-          printData->mPrintSettings->SetEndPageRange(endPageNum);
-          nsIntMargin marginTwips(0,0,0,0);
-          nsIntMargin unwrtMarginTwips(0,0,0,0);
-          printData->mPrintSettings->GetMarginInTwips(marginTwips);
-          printData->mPrintSettings->GetUnwriteableMarginInTwips(
-                                       unwrtMarginTwips);
-          nsMargin totalMargin = poPresContext->CSSTwipsToAppUnits(marginTwips +
-                                                                   unwrtMarginTwips);
-          if (startPageNum == endPageNum) {
-            startRect.y -= totalMargin.top;
-            endRect.y   -= totalMargin.top;
-
-            // Clip out selection regions above the top of the first page
-            if (startRect.y < 0) {
-              // Reduce height to be the height of the positive-territory
-              // region of original rect
-              startRect.height = std::max(0, startRect.YMost());
-              startRect.y = 0;
-            }
-            if (endRect.y < 0) {
-              // Reduce height to be the height of the positive-territory
-              // region of original rect
-              endRect.height = std::max(0, endRect.YMost());
-              endRect.y = 0;
-            }
-            NS_ASSERTION(endRect.y >= startRect.y,
-                         "Selection end point should be after start point");
-            NS_ASSERTION(startRect.height >= 0,
-                         "rect should have non-negative height.");
-            NS_ASSERTION(endRect.height >= 0,
-                         "rect should have non-negative height.");
-
-            nscoord selectionHgt = endRect.y + endRect.height - startRect.y;
-            // XXX This is temporary fix for printing more than one page of a selection
-            pageSequence->SetSelectionHeight(startRect.y * aPO->mZoomRatio,
-                                             selectionHgt * aPO->mZoomRatio);
-
-            // calc total pages by getting calculating the selection's height
-            // and then dividing it by how page content frames will fit.
-            nscoord pageWidth, pageHeight;
-            printData->mPrintDC->GetDeviceSurfaceDimensions(pageWidth,
-                                                            pageHeight);
-            pageHeight -= totalMargin.top + totalMargin.bottom;
-            int32_t totalPages = NSToIntCeil(float(selectionHgt) * aPO->mZoomRatio / float(pageHeight));
-            pageSequence->SetTotalNumPages(totalPages);
+    if (!printData->mPrintSettings) {
+      // not sure what to do here!
+      SetIsPrinting(false);
+      return NS_ERROR_FAILURE;
+    }
+
+    nsAutoString docTitleStr;
+    nsAutoString docURLStr;
+    GetDisplayTitleAndURL(aPO, docTitleStr, docURLStr, eDocTitleDefBlank);
+
+    if (nsIPrintSettings::kRangeSelection == printRangeType) {
+      CloneSelection(aPO->mDocument->GetOriginalDocument(), aPO->mDocument);
+
+      poPresContext->SetIsRenderingOnlySelection(true);
+      // temporarily creating rendering context
+      // which is needed to find the selection frames
+      // mPrintDC must have positive width and height for this call
+
+      // find the starting and ending page numbers
+      // via the selection
+      nsIFrame* startFrame;
+      nsIFrame* endFrame;
+      int32_t   startPageNum;
+      int32_t   endPageNum;
+      nsRect    startRect;
+      nsRect    endRect;
+
+      rv = GetPageRangeForSelection(pageSequence,
+                                    &startFrame, startPageNum, startRect,
+                                    &endFrame, endPageNum, endRect);
+      if (NS_SUCCEEDED(rv)) {
+        printData->mPrintSettings->SetStartPageRange(startPageNum);
+        printData->mPrintSettings->SetEndPageRange(endPageNum);
+        nsIntMargin marginTwips(0,0,0,0);
+        nsIntMargin unwrtMarginTwips(0,0,0,0);
+        printData->mPrintSettings->GetMarginInTwips(marginTwips);
+        printData->mPrintSettings->GetUnwriteableMarginInTwips(
+                                     unwrtMarginTwips);
+        nsMargin totalMargin = poPresContext->CSSTwipsToAppUnits(marginTwips +
+                                                                 unwrtMarginTwips);
+        if (startPageNum == endPageNum) {
+          startRect.y -= totalMargin.top;
+          endRect.y   -= totalMargin.top;
+
+          // Clip out selection regions above the top of the first page
+          if (startRect.y < 0) {
+            // Reduce height to be the height of the positive-territory
+            // region of original rect
+            startRect.height = std::max(0, startRect.YMost());
+            startRect.y = 0;
           }
+          if (endRect.y < 0) {
+            // Reduce height to be the height of the positive-territory
+            // region of original rect
+            endRect.height = std::max(0, endRect.YMost());
+            endRect.y = 0;
+          }
+          NS_ASSERTION(endRect.y >= startRect.y,
+                       "Selection end point should be after start point");
+          NS_ASSERTION(startRect.height >= 0,
+                       "rect should have non-negative height.");
+          NS_ASSERTION(endRect.height >= 0,
+                       "rect should have non-negative height.");
+
+          nscoord selectionHgt = endRect.y + endRect.height - startRect.y;
+          // XXX This is temporary fix for printing more than one page of a selection
+          pageSequence->SetSelectionHeight(startRect.y * aPO->mZoomRatio,
+                                           selectionHgt * aPO->mZoomRatio);
+
+          // calc total pages by getting calculating the selection's height
+          // and then dividing it by how page content frames will fit.
+          nscoord pageWidth, pageHeight;
+          printData->mPrintDC->GetDeviceSurfaceDimensions(pageWidth,
+                                                          pageHeight);
+          pageHeight -= totalMargin.top + totalMargin.bottom;
+          int32_t totalPages = NSToIntCeil(float(selectionHgt) * aPO->mZoomRatio / float(pageHeight));
+          pageSequence->SetTotalNumPages(totalPages);
         }
       }
-
-      nsIFrame * seqFrame = do_QueryFrame(pageSequence);
-      if (!seqFrame) {
-        SetIsPrinting(false);
-        return NS_ERROR_FAILURE;
-      }
-
-      mPageSeqFrame = seqFrame;
-      pageSequence->StartPrint(poPresContext, printData->mPrintSettings,
-                               docTitleStr, docURLStr);
-
-      // Schedule Page to Print
-      PR_PL(("Scheduling Print of PO: %p (%s) \n", aPO.get(), gFrameTypesStr[aPO->mFrameType]));
-      StartPagePrintTimer(aPO);
+    }
+
+    nsIFrame * seqFrame = do_QueryFrame(pageSequence);
+    if (!seqFrame) {
+      SetIsPrinting(false);
+      return NS_ERROR_FAILURE;
     }
+
+    mPageSeqFrame = seqFrame;
+    pageSequence->StartPrint(poPresContext, printData->mPrintSettings,
+                             docTitleStr, docURLStr);
+
+    // Schedule Page to Print
+    PR_PL(("Scheduling Print of PO: %p (%s) \n", aPO.get(), gFrameTypesStr[aPO->mFrameType]));
+    StartPagePrintTimer(aPO);
   }
 
   return NS_OK;
 }
 
 //---------------------------------------------------------------------
 void
 nsPrintEngine::SetDocAndURLIntoProgress(const UniquePtr<nsPrintObject>& aPO,
diff --git a/layout/printing/nsPrintEngine.h b/layout/printing/nsPrintEngine.h
--- a/layout/printing/nsPrintEngine.h
+++ b/layout/printing/nsPrintEngine.h
@@ -76,18 +76,17 @@ public:
   nsPrintEngine();
 
   void Destroy();
   void DestroyPrintingData();
 
   nsresult Initialize(nsIDocumentViewerPrint* aDocViewerPrint,
                       nsIDocShell*            aContainer,
                       nsIDocument*            aDocument,
-                      float                   aScreenDPI,
-                      FILE*                   aDebugFile);
+                      float                   aScreenDPI);
 
   nsresult GetSeqFrameAndCountPages(nsIFrame*& aSeqFrame, int32_t& aCount);
 
   //
   // The following three methods are used for printing...
   //
   nsresult DocumentReadyForPrinting();
   nsresult GetSelectionDocument(nsIDeviceContextSpec * aDevSpec,
@@ -276,18 +275,16 @@ protected:
   WeakFrame               mPageSeqFrame;
 
   // Print Preview
   RefPtr<nsPrintData> mPrtPreview;
   RefPtr<nsPrintData> mOldPrtPreview;
 
   nsCOMPtr<nsIDocument>   mDocument;
 
-  FILE* mDebugFile;
-
   int32_t mLoadCounter;
   bool mDidLoadDataForPrinting;
   bool mIsDestroying;
   bool mDisallowSelectionPrint;
 
   nsresult AfterNetworkPrint(bool aHandleError);
 
   nsresult SetRootView(nsPrintObject* aPO,
