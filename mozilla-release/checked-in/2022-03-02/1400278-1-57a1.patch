# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1505909971 -3600
#      Wed Sep 20 13:19:31 2017 +0100
# Node ID bfe046d689851b488e73be0a8af93ad624dbde86
# Parent  ed75cf71ef3069ed7c1346186deb6355be5ed2b2
Bug 1400278 - Move definition of ZoneAllocPolicy to jsalloc.h r=sfink

diff --git a/js/src/builtin/ModuleObject.h b/js/src/builtin/ModuleObject.h
--- a/js/src/builtin/ModuleObject.h
+++ b/js/src/builtin/ModuleObject.h
@@ -8,17 +8,16 @@
 #define builtin_ModuleObject_h
 
 #include "mozilla/Maybe.h"
 
 #include "jsapi.h"
 #include "jsatom.h"
 
 #include "builtin/SelfHostingDefines.h"
-#include "gc/Zone.h"
 #include "js/GCVector.h"
 #include "js/Id.h"
 #include "js/UniquePtr.h"
 #include "vm/NativeObject.h"
 #include "vm/ProxyObject.h"
 
 namespace js {
 
diff --git a/js/src/gc/Zone.h b/js/src/gc/Zone.h
--- a/js/src/gc/Zone.h
+++ b/js/src/gc/Zone.h
@@ -890,71 +890,57 @@ class CompartmentsIterT
     }
 
     operator JSCompartment*() const { return get(); }
     JSCompartment* operator->() const { return get(); }
 };
 
 typedef CompartmentsIterT<ZonesIter> CompartmentsIter;
 
-/*
- * Allocation policy that uses Zone::pod_malloc and friends, so that memory
- * pressure is accounted for on the zone. This is suitable for memory associated
- * with GC things allocated in the zone.
- *
- * Since it doesn't hold a JSContext (those may not live long enough), it can't
- * report out-of-memory conditions itself; the caller must check for OOM and
- * take the appropriate action.
- *
- * FIXME bug 647103 - replace these *AllocPolicy names.
- */
-class ZoneAllocPolicy
+template <typename T>
+inline T*
+ZoneAllocPolicy::maybe_pod_malloc(size_t numElems)
 {
-    Zone* const zone;
-
-  public:
-    MOZ_IMPLICIT ZoneAllocPolicy(Zone* zone) : zone(zone) {}
+    return zone->maybe_pod_malloc<T>(numElems);
+}
 
-    template <typename T>
-    T* maybe_pod_malloc(size_t numElems) {
-        return zone->maybe_pod_malloc<T>(numElems);
-    }
+template <typename T>
+inline T*
+ZoneAllocPolicy::maybe_pod_calloc(size_t numElems)
+{
+    return zone->maybe_pod_calloc<T>(numElems);
+}
 
-    template <typename T>
-    T* maybe_pod_calloc(size_t numElems) {
-        return zone->maybe_pod_calloc<T>(numElems);
-    }
+template <typename T>
+inline T*
+ZoneAllocPolicy::maybe_pod_realloc(T* p, size_t oldSize, size_t newSize)
+{
+    return zone->maybe_pod_realloc<T>(p, oldSize, newSize);
+}
 
-    template <typename T>
-    T* maybe_pod_realloc(T* p, size_t oldSize, size_t newSize) {
-        return zone->maybe_pod_realloc<T>(p, oldSize, newSize);
-    }
-
-    template <typename T>
-    T* pod_malloc(size_t numElems) {
-        return zone->pod_malloc<T>(numElems);
-    }
+template <typename T>
+inline T*
+ZoneAllocPolicy::pod_malloc(size_t numElems)
+{
+    return zone->pod_malloc<T>(numElems);
+}
 
-    template <typename T>
-    T* pod_calloc(size_t numElems) {
-        return zone->pod_calloc<T>(numElems);
-    }
+template <typename T>
+inline T*
+ZoneAllocPolicy::pod_calloc(size_t numElems)
+{
+    return zone->pod_calloc<T>(numElems);
+}
 
-    template <typename T>
-    T* pod_realloc(T* p, size_t oldSize, size_t newSize) {
-        return zone->pod_realloc<T>(p, oldSize, newSize);
-    }
-
-    void free_(void* p) { js_free(p); }
-    void reportAllocOverflow() const {}
-
-    MOZ_MUST_USE bool checkSimulatedOOM() const {
-        return !js::oom::ShouldFailWithOOM();
-    }
-};
+template <typename T>
+inline T*
+ZoneAllocPolicy::pod_realloc(T* p, size_t oldSize, size_t newSize)
+{
+    return zone->pod_realloc<T>(p, oldSize, newSize);
+}
 
 /*
  * Provides a delete policy that can be used for objects which have their
  * lifetime managed by the GC so they can be safely destroyed outside of GC.
  *
  * This is necessary for example when initializing such an object may fail after
  * the initial allocation. The partially-initialized object must be destroyed,
  * but it may not be safe to do so at the current time as the store buffer may
diff --git a/js/src/jsalloc.h b/js/src/jsalloc.h
--- a/js/src/jsalloc.h
+++ b/js/src/jsalloc.h
@@ -14,16 +14,20 @@
 #ifndef jsalloc_h
 #define jsalloc_h
 
 #include "js/TypeDecls.h"
 #include "js/Utility.h"
 
 extern JS_PUBLIC_API(void) JS_ReportOutOfMemory(JSContext* cx);
 
+namespace JS {
+struct Zone;
+} // namespace JS
+
 namespace js {
 
 enum class AllocFunction {
     Malloc,
     Calloc,
     Realloc
 };
 /* Policy for using system memory functions and doing no error reporting. */
@@ -130,11 +134,45 @@ class TempAllocPolicy
             ReportOutOfMemory(cx_);
             return false;
         }
 
         return true;
     }
 };
 
+/*
+ * Allocation policy that uses Zone::pod_malloc and friends, so that memory
+ * pressure is accounted for on the zone. This is suitable for memory associated
+ * with GC things allocated in the zone.
+ *
+ * Since it doesn't hold a JSContext (those may not live long enough), it can't
+ * report out-of-memory conditions itself; the caller must check for OOM and
+ * take the appropriate action.
+ *
+ * FIXME bug 647103 - replace these *AllocPolicy names.
+ */
+class ZoneAllocPolicy
+{
+    JS::Zone* const zone;
+
+  public:
+    MOZ_IMPLICIT ZoneAllocPolicy(JS::Zone* z) : zone(z) {}
+
+    // These methods are defined in gc/Zone.h.
+    template <typename T> inline T* maybe_pod_malloc(size_t numElems);
+    template <typename T> inline T* maybe_pod_calloc(size_t numElems);
+    template <typename T> inline T* maybe_pod_realloc(T* p, size_t oldSize, size_t newSize);
+    template <typename T> inline T* pod_malloc(size_t numElems);
+    template <typename T> inline T* pod_calloc(size_t numElems);
+    template <typename T> inline T* pod_realloc(T* p, size_t oldSize, size_t newSize);
+
+    void free_(void* p) { js_free(p); }
+    void reportAllocOverflow() const {}
+
+    MOZ_MUST_USE bool checkSimulatedOOM() const {
+        return !js::oom::ShouldFailWithOOM();
+    }
+};
+
 } /* namespace js */
 
 #endif /* jsalloc_h */
diff --git a/js/src/vm/Probes-inl.h b/js/src/vm/Probes-inl.h
--- a/js/src/vm/Probes-inl.h
+++ b/js/src/vm/Probes-inl.h
@@ -6,18 +6,16 @@
 
 #ifndef vm_Probes_inl_h
 #define vm_Probes_inl_h
 
 #include "vm/Probes.h"
 
 #include "jscntxt.h"
 
-#include "gc/Zone.h"
-
 namespace js {
 
 /*
  * Many probe handlers are implemented inline for minimal performance impact,
  * especially important when no backends are enabled.
  */
 
 inline bool
diff --git a/js/src/vm/Stopwatch.cpp b/js/src/vm/Stopwatch.cpp
--- a/js/src/vm/Stopwatch.cpp
+++ b/js/src/vm/Stopwatch.cpp
@@ -12,20 +12,18 @@
 
 #if defined(XP_WIN)
 #include <processthreadsapi.h>
 #endif // defined(XP_WIN)
 
 #include "jscompartment.h"
 #include "jswin.h"
 
-#include "gc/Zone.h"
 #include "vm/Runtime.h"
 
-
 namespace js {
 
 bool
 PerformanceMonitoring::addRecentGroup(PerformanceGroup* group)
 {
     if (group->isUsedInThisIteration())
         return true;
 
diff --git a/js/src/vm/TaggedProto.cpp b/js/src/vm/TaggedProto.cpp
--- a/js/src/vm/TaggedProto.cpp
+++ b/js/src/vm/TaggedProto.cpp
@@ -5,19 +5,16 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "vm/TaggedProto.h"
 
 #include "jsfun.h"
 #include "jsobj.h"
 
 #include "gc/Barrier.h"
-#include "gc/Zone.h"
-
-#include "vm/Caches-inl.h"
 
 namespace js {
 
 /* static */ void
 InternalBarrierMethods<TaggedProto>::preBarrier(TaggedProto& proto)
 {
     InternalBarrierMethods<JSObject*>::preBarrier(proto.toObjectOrNull());
 }
diff --git a/js/src/vm/TypedArrayObject-inl.h b/js/src/vm/TypedArrayObject-inl.h
--- a/js/src/vm/TypedArrayObject-inl.h
+++ b/js/src/vm/TypedArrayObject-inl.h
@@ -14,16 +14,18 @@
 #include "mozilla/Assertions.h"
 #include "mozilla/FloatingPoint.h"
 #include "mozilla/PodOperations.h"
 
 #include "jsarray.h"
 #include "jscntxt.h"
 #include "jsnum.h"
 
+#include "gc/Zone.h"
+
 #include "jit/AtomicOperations.h"
 
 #include "js/Conversions.h"
 #include "js/Value.h"
 
 #include "vm/NativeObject.h"
 
 namespace js {
diff --git a/js/src/vm/TypedArrayObject.h b/js/src/vm/TypedArrayObject.h
--- a/js/src/vm/TypedArrayObject.h
+++ b/js/src/vm/TypedArrayObject.h
@@ -7,17 +7,16 @@
 #ifndef vm_TypedArrayObject_h
 #define vm_TypedArrayObject_h
 
 #include "mozilla/Attributes.h"
 
 #include "jsobj.h"
 
 #include "gc/Barrier.h"
-#include "gc/Zone.h"
 #include "js/Class.h"
 #include "vm/ArrayBufferObject.h"
 #include "vm/SharedArrayObject.h"
 
 #define JS_FOR_EACH_TYPED_ARRAY(macro) \
     macro(int8_t, Int8) \
     macro(uint8_t, Uint8) \
     macro(int16_t, Int16) \
