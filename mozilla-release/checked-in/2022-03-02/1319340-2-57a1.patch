# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1501828902 -32400
# Node ID c612ed5072bdd989f1f60cd7e0f9de1e1f6cdca8
# Parent  83010973e7a7e6e9e99e763cb9512f0eb9a92b50
Bug 1319340 - part2: GetCurrentState() of the classes in nsComposerCommands should take HTMLEditor instead of nsIEditor r=m_kato

All GetCurrentState() methods in nsComposerCommands require HTMLEditor but its argument is nsIEditor*.  So, it should take HTMLEditor* and it shouldn't be called if given editor isn't HTMLEditor since it's virtual method.

MozReview-Commit-ID: HsvYJN8hIxN

diff --git a/editor/composer/nsComposerCommands.cpp b/editor/composer/nsComposerCommands.cpp
--- a/editor/composer/nsComposerCommands.cpp
+++ b/editor/composer/nsComposerCommands.cpp
@@ -2,16 +2,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 
 #include <stdio.h>                      // for printf
 
 #include "mozilla/Assertions.h"         // for MOZ_ASSERT, etc
+#include "mozilla/HTMLEditor.h"         // for HTMLEditor
 #include "nsAString.h"
 #include "nsCOMPtr.h"                   // for nsCOMPtr, do_QueryInterface, etc
 #include "nsComponentManagerUtils.h"    // for do_CreateInstance
 #include "nsComposerCommands.h"
 #include "nsDebug.h"                    // for NS_ENSURE_TRUE, etc
 #include "nsError.h"                    // for NS_OK, NS_ERROR_FAILURE, etc
 #include "nsGkAtoms.h"                  // for nsGkAtoms, nsGkAtoms::font, etc
 #include "nsIAtom.h"                    // for nsIAtom, etc
@@ -25,17 +26,18 @@
 #include "nsLiteralString.h"            // for NS_LITERAL_STRING
 #include "nsReadableUtils.h"            // for EmptyString
 #include "nsString.h"                   // for nsAutoString, nsString, etc
 #include "nsStringFwd.h"                // for nsString
 
 class nsISupports;
 
 //prototype
-nsresult GetListState(nsIHTMLEditor* aEditor, bool* aMixed,
+nsresult GetListState(mozilla::HTMLEditor* aHTMLEditor,
+                      bool* aMixed,
                       nsAString& aLocalName);
 nsresult RemoveOneProperty(nsIHTMLEditor* aEditor, const nsAString& aProp);
 nsresult RemoveTextProperty(nsIHTMLEditor* aEditor, const nsAString& aProp);
 nsresult SetTextProperty(nsIHTMLEditor *aEditor, const nsAString& aProp);
 
 
 //defines
 #define STATE_ENABLED  "state_enabled"
@@ -101,20 +103,24 @@ nsBaseStateUpdatingCommand::DoCommandPar
 }
 
 NS_IMETHODIMP
 nsBaseStateUpdatingCommand::GetCommandStateParams(const char *aCommandName,
                                                   nsICommandParams *aParams,
                                                   nsISupports *refCon)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
-  if (editor)
-    return GetCurrentState(editor, aParams);
-
-  return NS_OK;
+  if (!editor) {
+    return NS_OK;
+  }
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
+  return GetCurrentState(htmlEditor, aParams);
 }
 
 NS_IMETHODIMP
 nsPasteNoFormattingCommand::IsCommandEnabled(const char * aCommandName,
                                              nsISupports *refCon,
                                              bool *outCmdEnabled)
 {
   NS_ENSURE_ARG_POINTER(outCmdEnabled);
@@ -166,64 +172,69 @@ nsPasteNoFormattingCommand::GetCommandSt
 }
 
 nsStyleUpdatingCommand::nsStyleUpdatingCommand(nsIAtom* aTagName)
 : nsBaseStateUpdatingCommand(aTagName)
 {
 }
 
 nsresult
-nsStyleUpdatingCommand::GetCurrentState(nsIEditor *aEditor,
+nsStyleUpdatingCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
                                         nsICommandParams *aParams)
 {
-  NS_ASSERTION(aEditor, "Need editor here");
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_NOT_INITIALIZED);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool firstOfSelectionHasProp = false;
   bool anyOfSelectionHasProp = false;
   bool allOfSelectionHasProp = false;
 
-  nsresult rv = htmlEditor->GetInlineProperty(mTagName, EmptyString(),
-                                              EmptyString(),
-                                              &firstOfSelectionHasProp,
-                                              &anyOfSelectionHasProp,
-                                              &allOfSelectionHasProp);
+  nsresult rv = aHTMLEditor->GetInlineProperty(mTagName, EmptyString(),
+                                               EmptyString(),
+                                               &firstOfSelectionHasProp,
+                                               &anyOfSelectionHasProp,
+                                               &allOfSelectionHasProp);
 
   aParams->SetBooleanValue(STATE_ENABLED, NS_SUCCEEDED(rv));
   aParams->SetBooleanValue(STATE_ALL, allOfSelectionHasProp);
   aParams->SetBooleanValue(STATE_ANY, anyOfSelectionHasProp);
   aParams->SetBooleanValue(STATE_MIXED, anyOfSelectionHasProp
            && !allOfSelectionHasProp);
   aParams->SetBooleanValue(STATE_BEGIN, firstOfSelectionHasProp);
   aParams->SetBooleanValue(STATE_END, allOfSelectionHasProp);//not completely accurate
   return NS_OK;
 }
 
 nsresult
 nsStyleUpdatingCommand::ToggleState(nsIEditor *aEditor)
 {
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_NO_INTERFACE);
+  if (NS_WARN_IF(!aEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
+  mozilla::HTMLEditor* htmlEditor = aEditor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   //create some params now...
   nsresult rv;
   nsCOMPtr<nsICommandParams> params =
       do_CreateInstance(NS_COMMAND_PARAMS_CONTRACTID,&rv);
   if (NS_FAILED(rv) || !params)
     return rv;
 
   // tags "href" and "name" are special cases in the core editor
   // they are used to remove named anchor/link and shouldn't be used for insertion
   bool doTagRemoval;
   if (mTagName == nsGkAtoms::href || mTagName == nsGkAtoms::name) {
     doTagRemoval = true;
   } else {
     // check current selection; set doTagRemoval if formatting should be removed
-    rv = GetCurrentState(aEditor, params);
+    rv = GetCurrentState(htmlEditor, params);
     NS_ENSURE_SUCCESS(rv, rv);
     rv = params->GetBooleanValue(STATE_ALL, &doTagRemoval);
     NS_ENSURE_SUCCESS(rv, rv);
   }
 
   if (doTagRemoval) {
     // Also remove equivalent properties (bug 317093)
     if (mTagName == nsGkAtoms::b) {
@@ -256,79 +267,84 @@ nsStyleUpdatingCommand::ToggleState(nsIE
 }
 
 nsListCommand::nsListCommand(nsIAtom* aTagName)
 : nsBaseStateUpdatingCommand(aTagName)
 {
 }
 
 nsresult
-nsListCommand::GetCurrentState(nsIEditor* aEditor, nsICommandParams* aParams)
+nsListCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                               nsICommandParams* aParams)
 {
-  NS_ASSERTION(aEditor, "Need editor here");
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_NO_INTERFACE);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool bMixed;
   nsAutoString localName;
-  nsresult rv = GetListState(htmlEditor, &bMixed, localName);
+  nsresult rv = GetListState(aHTMLEditor, &bMixed, localName);
   NS_ENSURE_SUCCESS(rv, rv);
 
   bool inList = mTagName->Equals(localName);
   aParams->SetBooleanValue(STATE_ALL, !bMixed && inList);
   aParams->SetBooleanValue(STATE_MIXED, bMixed);
   aParams->SetBooleanValue(STATE_ENABLED, true);
   return NS_OK;
 }
 
 nsresult
 nsListCommand::ToggleState(nsIEditor *aEditor)
 {
-  nsCOMPtr<nsIHTMLEditor> editor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(editor, NS_NOINTERFACE);
+  if (NS_WARN_IF(!aEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
+  mozilla::HTMLEditor* htmlEditor = aEditor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   nsresult rv;
   nsCOMPtr<nsICommandParams> params =
       do_CreateInstance(NS_COMMAND_PARAMS_CONTRACTID,&rv);
   if (NS_FAILED(rv) || !params)
     return rv;
 
-  rv = GetCurrentState(aEditor, params);
+  rv = GetCurrentState(htmlEditor, params);
   NS_ENSURE_SUCCESS(rv, rv);
 
   bool inList;
   rv = params->GetBooleanValue(STATE_ALL,&inList);
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsDependentAtomString listType(mTagName);
   if (inList) {
-    rv = editor->RemoveList(listType);
+    rv = htmlEditor->RemoveList(listType);
   } else {
-    rv = editor->MakeOrChangeList(listType, false, EmptyString());
+    rv = htmlEditor->MakeOrChangeList(listType, false, EmptyString());
   }
 
   return rv;
 }
 
 nsListItemCommand::nsListItemCommand(nsIAtom* aTagName)
 : nsBaseStateUpdatingCommand(aTagName)
 {
 }
 
 nsresult
-nsListItemCommand::GetCurrentState(nsIEditor* aEditor,
+nsListItemCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
                                    nsICommandParams *aParams)
 {
-  NS_ASSERTION(aEditor, "Need editor here");
-  // 39584
-  nsCOMPtr<nsIHTMLEditor>  htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_NOINTERFACE);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool bMixed, bLI, bDT, bDD;
-  nsresult rv = htmlEditor->GetListItemState(&bMixed, &bLI, &bDT, &bDD);
+  nsresult rv = aHTMLEditor->GetListItemState(&bMixed, &bLI, &bDT, &bDD);
   NS_ENSURE_SUCCESS(rv, rv);
 
   bool inList = false;
   if (!bMixed) {
     if (bLI) {
       inList = mTagName == nsGkAtoms::li;
     } else if (bDT) {
       inList = mTagName == nsGkAtoms::dt;
@@ -341,28 +357,32 @@ nsListItemCommand::GetCurrentState(nsIEd
   aParams->SetBooleanValue(STATE_MIXED, bMixed);
 
   return NS_OK;
 }
 
 nsresult
 nsListItemCommand::ToggleState(nsIEditor *aEditor)
 {
-  NS_ASSERTION(aEditor, "Need editor here");
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_NOT_INITIALIZED);
+  if (NS_WARN_IF(!aEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
+  mozilla::HTMLEditor* htmlEditor = aEditor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool inList;
   // Need to use mTagName????
   nsresult rv;
   nsCOMPtr<nsICommandParams> params =
       do_CreateInstance(NS_COMMAND_PARAMS_CONTRACTID,&rv);
   if (NS_FAILED(rv) || !params)
     return rv;
-  rv = GetCurrentState(aEditor, params);
+  rv = GetCurrentState(htmlEditor, params);
   rv = params->GetBooleanValue(STATE_ALL,&inList);
   NS_ENSURE_SUCCESS(rv, rv);
   NS_ENSURE_SUCCESS(rv, rv);
 
   if (inList) {
     // To remove a list, first get what kind of list we're in
     bool bMixed;
     nsAutoString localName;
@@ -393,18 +413,20 @@ nsRemoveListCommand::IsCommandEnabled(co
   bool isEditable = false;
   nsresult rv = editor->GetIsSelectionEditable(&isEditable);
   NS_ENSURE_SUCCESS(rv, rv);
   if (!isEditable) {
     return NS_OK;
   }
 
   // It is enabled if we are in any list type
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(refCon);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_NO_INTERFACE);
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool bMixed;
   nsAutoString localName;
   rv = GetListState(htmlEditor, &bMixed, localName);
   NS_ENSURE_SUCCESS(rv, rv);
 
   *outCmdEnabled = bMixed || !localName.IsEmpty();
   return NS_OK;
@@ -599,40 +621,42 @@ nsMultiStateCommand::DoCommandParams(con
 }
 
 NS_IMETHODIMP
 nsMultiStateCommand::GetCommandStateParams(const char *aCommandName,
                                            nsICommandParams *aParams,
                                            nsISupports *refCon)
 {
   nsCOMPtr<nsIEditor> editor = do_QueryInterface(refCon);
-  nsresult rv = NS_OK;
-  if (editor) {
-      rv = GetCurrentState(editor, aParams);
+  if (!editor) {
+    return NS_OK;
   }
-  return rv;
+  mozilla::HTMLEditor* htmlEditor = editor->AsHTMLEditor();
+  if (NS_WARN_IF(!htmlEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
+  return GetCurrentState(htmlEditor, aParams);
 }
 
 nsParagraphStateCommand::nsParagraphStateCommand()
 : nsMultiStateCommand()
 {
 }
 
 nsresult
-nsParagraphStateCommand::GetCurrentState(nsIEditor *aEditor,
-                                         nsICommandParams *aParams)
+nsParagraphStateCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                         nsICommandParams* aParams)
 {
-  NS_ASSERTION(aEditor, "Need an editor here");
-
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool outMixed;
   nsAutoString outStateString;
-  nsresult rv = htmlEditor->GetParagraphState(&outMixed, outStateString);
+  nsresult rv = aHTMLEditor->GetParagraphState(&outMixed, outStateString);
   if (NS_SUCCEEDED(rv)) {
     nsAutoCString tOutStateString;
     LossyCopyUTF16toASCII(outStateString, tOutStateString);
     aParams->SetBooleanValue(STATE_MIXED,outMixed);
     aParams->SetCStringValue(STATE_ATTRIBUTE, tOutStateString.get());
   }
   return rv;
 }
@@ -649,34 +673,33 @@ nsParagraphStateCommand::SetState(nsIEdi
 }
 
 nsFontFaceStateCommand::nsFontFaceStateCommand()
 : nsMultiStateCommand()
 {
 }
 
 nsresult
-nsFontFaceStateCommand::GetCurrentState(nsIEditor *aEditor,
-                                        nsICommandParams *aParams)
+nsFontFaceStateCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                        nsICommandParams* aParams)
 {
-  NS_ASSERTION(aEditor, "Need an editor here");
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   nsAutoString outStateString;
   bool outMixed;
-  nsresult rv = htmlEditor->GetFontFaceState(&outMixed, outStateString);
+  nsresult rv = aHTMLEditor->GetFontFaceState(&outMixed, outStateString);
   if (NS_SUCCEEDED(rv)) {
     aParams->SetBooleanValue(STATE_MIXED,outMixed);
     aParams->SetCStringValue(STATE_ATTRIBUTE, NS_ConvertUTF16toUTF8(outStateString).get());
   }
   return rv;
 }
 
-
 nsresult
 nsFontFaceStateCommand::SetState(nsIEditor *aEditor, nsString& newState)
 {
   NS_ASSERTION(aEditor, "Need an editor here");
   nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
   NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
 
   if (newState.EqualsLiteral("tt")) {
@@ -703,31 +726,31 @@ nsFontFaceStateCommand::SetState(nsIEdit
 }
 
 nsFontSizeStateCommand::nsFontSizeStateCommand()
   : nsMultiStateCommand()
 {
 }
 
 nsresult
-nsFontSizeStateCommand::GetCurrentState(nsIEditor *aEditor,
-                                        nsICommandParams *aParams)
+nsFontSizeStateCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                        nsICommandParams* aParams)
 {
-  NS_ASSERTION(aEditor, "Need an editor here");
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_INVALID_ARG);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   nsAutoString outStateString;
-  nsCOMPtr<nsIAtom> fontAtom = NS_Atomize("font");
   bool firstHas, anyHas, allHas;
-  nsresult rv = htmlEditor->GetInlinePropertyWithAttrValue(fontAtom,
-                                         NS_LITERAL_STRING("size"),
-                                         EmptyString(),
-                                         &firstHas, &anyHas, &allHas,
-                                         outStateString);
+  nsresult rv = aHTMLEditor->GetInlinePropertyWithAttrValue(
+                               nsGkAtoms::font,
+                               NS_LITERAL_STRING("size"),
+                               EmptyString(),
+                               &firstHas, &anyHas, &allHas,
+                               outStateString);
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsAutoCString tOutStateString;
   LossyCopyUTF16toASCII(outStateString, tOutStateString);
   aParams->SetBooleanValue(STATE_MIXED, anyHas && !allHas);
   aParams->SetCStringValue(STATE_ATTRIBUTE, tOutStateString.get());
   aParams->SetBooleanValue(STATE_ENABLED, true);
 
@@ -770,27 +793,26 @@ nsFontSizeStateCommand::SetState(nsIEdit
 }
 
 nsFontColorStateCommand::nsFontColorStateCommand()
 : nsMultiStateCommand()
 {
 }
 
 nsresult
-nsFontColorStateCommand::GetCurrentState(nsIEditor *aEditor,
-                                         nsICommandParams *aParams)
+nsFontColorStateCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                         nsICommandParams* aParams)
 {
-  NS_ASSERTION(aEditor, "Need an editor here");
-
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool outMixed;
   nsAutoString outStateString;
-  nsresult rv = htmlEditor->GetFontColorState(&outMixed, outStateString);
+  nsresult rv = aHTMLEditor->GetFontColorState(&outMixed, outStateString);
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsAutoCString tOutStateString;
   LossyCopyUTF16toASCII(outStateString, tOutStateString);
   aParams->SetBooleanValue(STATE_MIXED, outMixed);
   aParams->SetCStringValue(STATE_ATTRIBUTE, tOutStateString.get());
   return NS_OK;
 }
@@ -812,26 +834,26 @@ nsFontColorStateCommand::SetState(nsIEdi
 }
 
 nsHighlightColorStateCommand::nsHighlightColorStateCommand()
 : nsMultiStateCommand()
 {
 }
 
 nsresult
-nsHighlightColorStateCommand::GetCurrentState(nsIEditor *aEditor,
-                                              nsICommandParams *aParams)
+nsHighlightColorStateCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                              nsICommandParams* aParams)
 {
-  NS_ASSERTION(aEditor, "Need an editor here");
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool outMixed;
   nsAutoString outStateString;
-  nsresult rv = htmlEditor->GetHighlightColorState(&outMixed, outStateString);
+  nsresult rv = aHTMLEditor->GetHighlightColorState(&outMixed, outStateString);
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsAutoCString tOutStateString;
   LossyCopyUTF16toASCII(outStateString, tOutStateString);
   aParams->SetBooleanValue(STATE_MIXED, outMixed);
   aParams->SetCStringValue(STATE_ATTRIBUTE, tOutStateString.get());
   return NS_OK;
 }
@@ -868,27 +890,26 @@ nsHighlightColorStateCommand::IsCommandE
 
 
 nsBackgroundColorStateCommand::nsBackgroundColorStateCommand()
 : nsMultiStateCommand()
 {
 }
 
 nsresult
-nsBackgroundColorStateCommand::GetCurrentState(nsIEditor *aEditor,
-                                               nsICommandParams *aParams)
+nsBackgroundColorStateCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                               nsICommandParams* aParams)
 {
-  NS_ASSERTION(aEditor, "Need an editor here");
-
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool outMixed;
   nsAutoString outStateString;
-  nsresult rv =  htmlEditor->GetBackgroundColorState(&outMixed, outStateString);
+  nsresult rv = aHTMLEditor->GetBackgroundColorState(&outMixed, outStateString);
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsAutoCString tOutStateString;
   LossyCopyUTF16toASCII(outStateString, tOutStateString);
   aParams->SetBooleanValue(STATE_MIXED, outMixed);
   aParams->SetCStringValue(STATE_ATTRIBUTE, tOutStateString.get());
   return NS_OK;
 }
@@ -905,26 +926,26 @@ nsBackgroundColorStateCommand::SetState(
 }
 
 nsAlignCommand::nsAlignCommand()
 : nsMultiStateCommand()
 {
 }
 
 nsresult
-nsAlignCommand::GetCurrentState(nsIEditor *aEditor, nsICommandParams *aParams)
+nsAlignCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                nsICommandParams* aParams)
 {
-  NS_ASSERTION(aEditor, "Need an editor here");
-
-  nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   nsIHTMLEditor::EAlignment firstAlign;
   bool outMixed;
-  nsresult rv = htmlEditor->GetAlignment(&outMixed, &firstAlign);
+  nsresult rv = aHTMLEditor->GetAlignment(&outMixed, &firstAlign);
 
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsAutoString outStateString;
   switch (firstAlign) {
     default:
     case nsIHTMLEditor::eLeft:
       outStateString.AssignLiteral("left");
@@ -980,33 +1001,34 @@ nsAbsolutePositioningCommand::IsCommandE
       return htmlEditor->GetAbsolutePositioningEnabled(outCmdEnabled);
   }
 
   *outCmdEnabled = false;
   return NS_OK;
 }
 
 nsresult
-nsAbsolutePositioningCommand::GetCurrentState(nsIEditor *aEditor, nsICommandParams *aParams)
+nsAbsolutePositioningCommand::GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                              nsICommandParams* aParams)
 {
-  NS_ASSERTION(aEditor, "Need an editor here");
-
-  nsCOMPtr<nsIHTMLAbsPosEditor> htmlEditor = do_QueryInterface(aEditor);
-  NS_ENSURE_TRUE(htmlEditor, NS_ERROR_FAILURE);
+  if (NS_WARN_IF(!aHTMLEditor)) {
+    return NS_ERROR_INVALID_ARG;
+  }
 
   bool isEnabled;
-  htmlEditor->GetAbsolutePositioningEnabled(&isEnabled);
+  aHTMLEditor->GetAbsolutePositioningEnabled(&isEnabled);
   if (!isEnabled) {
     aParams->SetBooleanValue(STATE_MIXED,false);
     aParams->SetCStringValue(STATE_ATTRIBUTE, "");
     return NS_OK;
   }
 
   nsCOMPtr<nsIDOMElement>  elt;
-  nsresult rv = htmlEditor->GetAbsolutelyPositionedSelectionContainer(getter_AddRefs(elt));
+  nsresult rv =
+    aHTMLEditor->GetAbsolutelyPositionedSelectionContainer(getter_AddRefs(elt));
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsAutoString outStateString;
   if (elt)
     outStateString.AssignLiteral("absolute");
 
   aParams->SetBooleanValue(STATE_MIXED,false);
   aParams->SetCStringValue(STATE_ATTRIBUTE, NS_ConvertUTF16toUTF8(outStateString).get());
@@ -1451,26 +1473,28 @@ nsInsertTagCommand::GetCommandStateParam
 }
 
 
 /****************************/
 //HELPER METHODS
 /****************************/
 
 nsresult
-GetListState(nsIHTMLEditor* aEditor, bool* aMixed, nsAString& aLocalName)
+GetListState(mozilla::HTMLEditor* aHTMLEditor,
+             bool* aMixed,
+             nsAString& aLocalName)
 {
-  MOZ_ASSERT(aEditor);
+  MOZ_ASSERT(aHTMLEditor);
   MOZ_ASSERT(aMixed);
 
   *aMixed = false;
   aLocalName.Truncate();
 
   bool bOL, bUL, bDL;
-  nsresult rv = aEditor->GetListState(aMixed, &bOL, &bUL, &bDL);
+  nsresult rv = aHTMLEditor->GetListState(aMixed, &bOL, &bUL, &bDL);
   NS_ENSURE_SUCCESS(rv, rv);
 
   if (*aMixed) {
     return NS_OK;
   }
 
   if (bOL) {
     aLocalName.AssignLiteral("ol");
diff --git a/editor/composer/nsComposerCommands.h b/editor/composer/nsComposerCommands.h
--- a/editor/composer/nsComposerCommands.h
+++ b/editor/composer/nsComposerCommands.h
@@ -11,16 +11,20 @@
 #include "nsStringFwd.h"
 #include "nscore.h"                     // for nsresult, NS_IMETHOD
 
 class nsIAtom;
 class nsICommandParams;
 class nsIEditor;
 class nsISupports;
 
+namespace mozilla {
+class HTMLEditor;
+} // namespace mozilla
+
 // This is a virtual base class for commands registered with the composer controller.
 // Note that such commands are instantiated once per composer, so can store state.
 // Also note that IsCommandEnabled can be called with an editor that may not
 // have an editor yet (because the document is loading). Most commands will want
 // to return false in this case.
 // Don't hold on to any references to the editor or document from
 // your command. This will cause leaks. Also, be aware that the document the
 // editor is editing can change under you (if the user Reverts the file, for
@@ -60,37 +64,39 @@ public:
   NS_DECL_ISUPPORTS_INHERITED
 
   NS_DECL_NSICONTROLLERCOMMAND
 
 protected:
   virtual ~nsBaseStateUpdatingCommand();
 
   // get the current state (on or off) for this style or block format
-  virtual nsresult  GetCurrentState(nsIEditor* aEditor, nsICommandParams* aParams) = 0;
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) = 0;
 
   // add/remove the style
   virtual nsresult  ToggleState(nsIEditor* aEditor) = 0;
 
 protected:
   nsIAtom* mTagName;
 };
 
 
 // Shared class for the various style updating commands like bold, italics etc.
 // Suitable for commands whose state is either 'on' or 'off'.
-class nsStyleUpdatingCommand : public nsBaseStateUpdatingCommand
+class nsStyleUpdatingCommand final : public nsBaseStateUpdatingCommand
 {
 public:
   explicit nsStyleUpdatingCommand(nsIAtom* aTagName);
 
 protected:
 
   // get the current state (on or off) for this style or block format
-  virtual nsresult  GetCurrentState(nsIEditor* aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
 
   // add/remove the style
   virtual nsresult  ToggleState(nsIEditor* aEditor);
 };
 
 
 class nsInsertTagCommand : public nsBaseComposerCommand
 {
@@ -103,39 +109,41 @@ public:
 
 protected:
   virtual ~nsInsertTagCommand();
 
   nsIAtom* mTagName;
 };
 
 
-class nsListCommand : public nsBaseStateUpdatingCommand
+class nsListCommand final : public nsBaseStateUpdatingCommand
 {
 public:
   explicit nsListCommand(nsIAtom* aTagName);
 
 protected:
 
   // get the current state (on or off) for this style or block format
-  virtual nsresult  GetCurrentState(nsIEditor* aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
 
   // add/remove the style
   virtual nsresult  ToggleState(nsIEditor* aEditor);
 };
 
-class nsListItemCommand : public nsBaseStateUpdatingCommand
+class nsListItemCommand final : public nsBaseStateUpdatingCommand
 {
 public:
   explicit nsListItemCommand(nsIAtom* aTagName);
 
 protected:
 
   // get the current state (on or off) for this style or block format
-  virtual nsresult  GetCurrentState(nsIEditor* aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
 
   // add/remove the style
   virtual nsresult  ToggleState(nsIEditor* aEditor);
 };
 
 // Base class for commands whose state consists of a string (e.g. para format)
 class nsMultiStateCommand : public nsBaseComposerCommand
 {
@@ -144,111 +152,119 @@ public:
   nsMultiStateCommand();
 
   NS_DECL_ISUPPORTS_INHERITED
   NS_DECL_NSICONTROLLERCOMMAND
 
 protected:
   virtual ~nsMultiStateCommand();
 
-  virtual nsresult GetCurrentState(nsIEditor *aEditor, nsICommandParams* aParams) =0;
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) = 0;
   virtual nsresult SetState(nsIEditor *aEditor, nsString& newState) = 0;
 
 };
 
 
-class nsParagraphStateCommand : public nsMultiStateCommand
+class nsParagraphStateCommand final : public nsMultiStateCommand
 {
 public:
                    nsParagraphStateCommand();
 
 protected:
 
-  virtual nsresult GetCurrentState(nsIEditor *aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
   virtual nsresult SetState(nsIEditor *aEditor, nsString& newState);
 };
 
-class nsFontFaceStateCommand : public nsMultiStateCommand
+class nsFontFaceStateCommand final : public nsMultiStateCommand
 {
 public:
                    nsFontFaceStateCommand();
 
 protected:
 
-  virtual nsresult GetCurrentState(nsIEditor *aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
   virtual nsresult SetState(nsIEditor *aEditor, nsString& newState);
 };
 
-class nsFontSizeStateCommand : public nsMultiStateCommand
+class nsFontSizeStateCommand final : public nsMultiStateCommand
 {
 public:
                    nsFontSizeStateCommand();
 
 protected:
 
-  virtual nsresult GetCurrentState(nsIEditor *aEditor,
-                                   nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
   virtual nsresult SetState(nsIEditor *aEditor, nsString& newState);
 };
 
-class nsHighlightColorStateCommand : public nsMultiStateCommand
+class nsHighlightColorStateCommand final : public nsMultiStateCommand
 {
 public:
                    nsHighlightColorStateCommand();
 
 protected:
 
   NS_IMETHOD IsCommandEnabled(const char *aCommandName, nsISupports *aCommandRefCon, bool *_retval);
-  virtual nsresult GetCurrentState(nsIEditor *aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
   virtual nsresult SetState(nsIEditor *aEditor, nsString& newState);
 
 };
 
-class nsFontColorStateCommand : public nsMultiStateCommand
+class nsFontColorStateCommand final : public nsMultiStateCommand
 {
 public:
                    nsFontColorStateCommand();
 
 protected:
 
-  virtual nsresult GetCurrentState(nsIEditor *aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
   virtual nsresult SetState(nsIEditor *aEditor, nsString& newState);
 };
 
-class nsAlignCommand : public nsMultiStateCommand
+class nsAlignCommand final : public nsMultiStateCommand
 {
 public:
                    nsAlignCommand();
 
 protected:
 
-  virtual nsresult GetCurrentState(nsIEditor *aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
   virtual nsresult SetState(nsIEditor *aEditor, nsString& newState);
 };
 
-class nsBackgroundColorStateCommand : public nsMultiStateCommand
+class nsBackgroundColorStateCommand final : public nsMultiStateCommand
 {
 public:
                    nsBackgroundColorStateCommand();
 
 protected:
 
-  virtual nsresult GetCurrentState(nsIEditor *aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
   virtual nsresult SetState(nsIEditor *aEditor, nsString& newState);
 };
 
-class nsAbsolutePositioningCommand : public nsBaseStateUpdatingCommand
+class nsAbsolutePositioningCommand final : public nsBaseStateUpdatingCommand
 {
 public:
   nsAbsolutePositioningCommand();
 
 protected:
 
   NS_IMETHOD IsCommandEnabled(const char *aCommandName, nsISupports *aCommandRefCon, bool *_retval);
-  virtual nsresult  GetCurrentState(nsIEditor* aEditor, nsICommandParams* aParams);
+  virtual nsresult GetCurrentState(mozilla::HTMLEditor* aHTMLEditor,
+                                   nsICommandParams* aParams) override final;
   virtual nsresult  ToggleState(nsIEditor* aEditor);
 };
 
 // composer commands
 
 NS_DECL_COMPOSER_COMMAND(nsCloseCommand)
 NS_DECL_COMPOSER_COMMAND(nsDocumentStateCommand)
 NS_DECL_COMPOSER_COMMAND(nsSetDocumentStateCommand)
