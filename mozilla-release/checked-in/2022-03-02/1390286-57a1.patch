# HG changeset patch
# User Karl Tomlinson <karlt+@karlt.net>
# Date 1502667233 -43200
#      Mon Aug 14 11:33:53 2017 +1200
# Node ID a336665e89aaa0adcdeb151fde1bf85824a00612
# Parent  eb231e02ebd14b1077a4ca3eaf544418c02ff5b1
bug 1390286 receive WrapRunnable*() parameters by reference r=jesup

This permits using these functions with non-param types such as AutoTArray.

MozReview-Commit-ID: 3J1bLjgwB9M

diff --git a/media/mtransport/runnable_utils.h b/media/mtransport/runnable_utils.h
--- a/media/mtransport/runnable_utils.h
+++ b/media/mtransport/runnable_utils.h
@@ -133,89 +133,92 @@ WrapRunnableNM(FunType f, Args&&... args
 {
   return new runnable_args_func<FunType, typename mozilla::Decay<Args>::Type...>(f, Forward<Args>(args)...);
 }
 
 template<typename Ret, typename FunType, typename... Args>
 class runnable_args_func_ret : public detail::runnable_args_base<detail::ReturnsResult>
 {
 public:
-  runnable_args_func_ret(Ret* ret, FunType f, Args&&... args)
-    : mReturn(ret), mFunc(f), mArgs(Forward<Args>(args)...)
+  template<typename... Arguments>
+  runnable_args_func_ret(Ret* ret, FunType f, Arguments&&... args)
+    : mReturn(ret), mFunc(f), mArgs(Forward<Arguments>(args)...)
   {}
 
   NS_IMETHOD Run() {
     *mReturn = detail::RunnableFunctionCallHelper<Ret>::apply(mFunc, mArgs, typename IndexSequenceFor<Args...>::Type());
     return NS_OK;
   }
 
 private:
   Ret* mReturn;
   FunType mFunc;
   Tuple<Args...> mArgs;
 };
 
 template<typename R, typename FunType, typename... Args>
-runnable_args_func_ret<R, FunType, Args...>*
-WrapRunnableNMRet(R* ret, FunType f, Args... args)
+runnable_args_func_ret<R, FunType, typename mozilla::Decay<Args>::Type...>*
+WrapRunnableNMRet(R* ret, FunType f, Args&&... args)
 {
-  return new runnable_args_func_ret<R, FunType, Args...>(ret, f, Move(args)...);
+  return new runnable_args_func_ret<R, FunType, typename mozilla::Decay<Args>::Type...>(ret, f, Forward<Args>(args)...);
 }
 
 template<typename Class, typename M, typename... Args>
 class runnable_args_memfn : public detail::runnable_args_base<detail::NoResult>
 {
 public:
-  runnable_args_memfn(Class obj, M method, Args&&... args)
-    : mObj(obj), mMethod(method), mArgs(Forward<Args>(args)...)
+  template<typename... Arguments>
+  runnable_args_memfn(Class obj, M method, Arguments&&... args)
+    : mObj(obj), mMethod(method), mArgs(Forward<Arguments>(args)...)
   {}
 
   NS_IMETHOD Run() {
     detail::RunnableMethodCallHelper<void>::apply(mObj, mMethod, mArgs, typename IndexSequenceFor<Args...>::Type());
     return NS_OK;
   }
 
 private:
   Class mObj;
   M mMethod;
   Tuple<Args...> mArgs;
 };
 
 template<typename Class, typename M, typename... Args>
-runnable_args_memfn<Class, M, Args...>*
-WrapRunnable(Class obj, M method, Args... args)
+runnable_args_memfn<Class, M, typename mozilla::Decay<Args>::Type...>*
+WrapRunnable(Class obj, M method, Args&&... args)
 {
-  return new runnable_args_memfn<Class, M, Args...>(obj, method, Move(args)...);
+  return new runnable_args_memfn<Class, M, typename mozilla::Decay<Args>::Type...>(obj, method, Forward<Args>(args)...);
 }
 
 template<typename Ret, typename Class, typename M, typename... Args>
 class runnable_args_memfn_ret : public detail::runnable_args_base<detail::ReturnsResult>
 {
 public:
-  runnable_args_memfn_ret(Ret* ret, Class obj, M method, Args... args)
-    : mReturn(ret), mObj(obj), mMethod(method), mArgs(Forward<Args>(args)...)
+  template<typename... Arguments>
+  runnable_args_memfn_ret(Ret* ret, Class obj, M method, Arguments... args)
+    : mReturn(ret), mObj(obj), mMethod(method), mArgs(Forward<Arguments>(args)...)
   {}
 
   NS_IMETHOD Run() {
     *mReturn = detail::RunnableMethodCallHelper<Ret>::apply(mObj, mMethod, mArgs, typename IndexSequenceFor<Args...>::Type());
     return NS_OK;
   }
 
 private:
   Ret* mReturn;
   Class mObj;
   M mMethod;
   Tuple<Args...> mArgs;
 };
 
 template<typename R, typename Class, typename M, typename... Args>
-runnable_args_memfn_ret<R, Class, M, Args...>*
-WrapRunnableRet(R* ret, Class obj, M method, Args... args)
+runnable_args_memfn_ret<R, Class, M, typename mozilla::Decay<Args>::Type...>*
+WrapRunnableRet(R* ret, Class obj, M method, Args&&... args)
 {
-  return new runnable_args_memfn_ret<R, Class, M, Args...>(ret, obj, method, Move(args)...);
+  return new runnable_args_memfn_ret<R, Class, M, typename mozilla::Decay<Args>::Type...>(ret, obj, method, Forward<Args>(args)...);
 }
 
 static inline nsresult RUN_ON_THREAD(nsIEventTarget *thread, detail::runnable_args_base<detail::NoResult> *runnable, uint32_t flags) {
   return detail::RunOnThreadInternal(thread, static_cast<nsIRunnable *>(runnable), flags);
 }
 
 static inline nsresult
 RUN_ON_THREAD(nsIEventTarget *thread, detail::runnable_args_base<detail::ReturnsResult> *runnable)
