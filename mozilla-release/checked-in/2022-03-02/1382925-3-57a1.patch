# HG changeset patch
# User Cameron McCormack <cam@mcc.id.au>
# Date 1502534941 -28800
#      Sat Aug 12 18:49:01 2017 +0800
# Node ID 1ff9809cc7aa260059efbe021cb5ded48eabeb5b
# Parent  b8833bd9d99603aab2da73d6c382b1f422355f95
Bug 1382925 - Part 3: Pass in relevant origin when style sheet rules change and when author styles are toggled. r=emilio

MozReview-Commit-ID: EGEnUBAvVyX

diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -166,17 +166,19 @@ ServoStyleSet::InvalidateStyleForCSSRule
 nsRestyleHint
 ServoStyleSet::MediumFeaturesChanged(bool aViewportChanged)
 {
   bool viewportUnitsUsed = false;
   const bool rulesChanged =
     Servo_StyleSet_MediumFeaturesChanged(mRawSet.get(), &viewportUnitsUsed);
 
   if (rulesChanged) {
-    ForceAllStyleDirty();
+    // XXXheycam Should be able to tell which origin to pass in here
+    // (bug 1389871).
+    MarkOriginsDirty(OriginFlags::All);
     return eRestyle_Subtree;
   }
 
   if (viewportUnitsUsed && aViewportChanged) {
     return eRestyle_ForceDescendants;
   }
 
   return nsRestyleHint(0);
@@ -212,17 +214,17 @@ ServoStyleSet::GetAuthorStyleDisabled() 
 nsresult
 ServoStyleSet::SetAuthorStyleDisabled(bool aStyleDisabled)
 {
   if (mAuthorStyleDisabled == aStyleDisabled) {
     return NS_OK;
   }
 
   mAuthorStyleDisabled = aStyleDisabled;
-  ForceAllStyleDirty();
+  MarkOriginsDirty(OriginFlags::Author);
 
   return NS_OK;
 }
 
 void
 ServoStyleSet::BeginUpdate()
 {
 }
@@ -1035,36 +1037,35 @@ ServoStyleSet::StyleSubtreeForReconstruc
   if (mPresContext->EffectCompositor()->PreTraverseInSubtree(flags, aRoot)) {
     postTraversalRequired =
       Servo_TraverseSubtree(aRoot, mRawSet.get(), &snapshots, flags);
     MOZ_ASSERT(!postTraversalRequired);
   }
 }
 
 void
-ServoStyleSet::ForceAllStyleDirty()
+ServoStyleSet::MarkOriginsDirty(OriginFlags aChangedOrigins)
 {
   SetStylistStyleSheetsDirty();
   Servo_StyleSet_NoteStyleSheetsChanged(mRawSet.get(),
                                         mAuthorStyleDisabled,
-                                        OriginFlags::All);
+                                        aChangedOrigins);
 }
 
 void
 ServoStyleSet::RecordStyleSheetChange(
     ServoStyleSheet* aSheet,
     StyleSheet::ChangeType aChangeType)
 {
-  SetStylistStyleSheetsDirty();
   switch (aChangeType) {
     case StyleSheet::ChangeType::RuleAdded:
     case StyleSheet::ChangeType::RuleRemoved:
     case StyleSheet::ChangeType::RuleChanged:
       // FIXME(emilio): We can presumably do better in a bunch of these.
-      return ForceAllStyleDirty();
+      return MarkOriginsDirty(aSheet->GetOrigin());
     case StyleSheet::ChangeType::ApplicableStateChanged:
     case StyleSheet::ChangeType::Added:
     case StyleSheet::ChangeType::Removed:
       // Do nothing, we've already recorded the change in the
       // Append/Remove/Replace methods, etc, and will act consequently.
       return;
   }
 }
diff --git a/layout/style/ServoStyleSet.h b/layout/style/ServoStyleSet.h
--- a/layout/style/ServoStyleSet.h
+++ b/layout/style/ServoStyleSet.h
@@ -104,17 +104,17 @@ public:
   void BeginShutdown();
   void Shutdown();
 
   void RecordStyleSheetChange(mozilla::ServoStyleSheet*, StyleSheet::ChangeType);
 
   void RecordShadowStyleChange(mozilla::dom::ShadowRoot* aShadowRoot) {
     // FIXME(emilio): When we properly support shadow dom we'll need to do
     // better.
-    ForceAllStyleDirty();
+    MarkOriginsDirty(OriginFlags::All);
   }
 
   bool StyleSheetsHaveChanged() const
   {
     return StylistNeedsUpdate();
   }
 
   nsRestyleHint MediumFeaturesChanged(bool aViewportChanged);
@@ -294,23 +294,16 @@ public:
    * styles.  This will leave the subtree in a state just like after an initial
    * styling, i.e. with new styles, no change hints, and with the dirty
    * descendants bits cleared.  No comparison of old and new styles is done,
    * so no change hints will be processed.
    */
   void StyleSubtreeForReconstruct(dom::Element* aRoot);
 
   /**
-   * Records that the contents of style sheets have changed since the last
-   * restyle.  Calling this will ensure that the Stylist rebuilds its
-   * selector maps.
-   */
-  void ForceAllStyleDirty();
-
-  /**
    * Helper for correctly calling UpdateStylist without paying the cost of an
    * extra function call in the common no-rebuild-needed case.
    */
   void UpdateStylistIfNeeded()
   {
     if (StylistNeedsUpdate()) {
       UpdateStylist();
     }
@@ -502,16 +495,23 @@ private:
    */
   void PreTraverse(ServoTraversalFlags aFlags,
                    dom::Element* aRoot = nullptr);
 
   // Subset of the pre-traverse steps that involve syncing up data
   void PreTraverseSync();
 
   /**
+   * Records that the contents of style sheets at the specified origin have
+   * changed since the last.  Calling this will ensure that the Stylist
+   * rebuilds its selector maps.
+   */
+  void MarkOriginsDirty(OriginFlags aChangedOrigins);
+
+  /**
    * Note that the stylist needs a style flush due to style sheet changes.
    */
   void SetStylistStyleSheetsDirty()
   {
     mStylistState = StylistState::StyleSheetsDirty;
   }
 
   bool StylistNeedsUpdate() const
