# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1503443594 25200
# Node ID 2c1d5bec1bd09991f6e233282bfbe3ba399a315b
# Parent  0c1214b63207ab23632fe20350d80e60b464c280
Bug 1322746 - Add extern decls for DrawBlitProg::Key. - r=daoshengmu

MozReview-Commit-ID: CZSOmThJSbj


diff --git a/gfx/gl/GLBlitHelper.cpp b/gfx/gl/GLBlitHelper.cpp
--- a/gfx/gl/GLBlitHelper.cpp
+++ b/gfx/gl/GLBlitHelper.cpp
@@ -32,77 +32,77 @@
 using mozilla::layers::PlanarYCbCrImage;
 using mozilla::layers::PlanarYCbCrData;
 
 namespace mozilla {
 namespace gl {
 
 // --
 
-const char kFragHeader_Tex2D[] = "\
+const char* const kFragHeader_Tex2D = "\
     #define SAMPLER sampler2D                                                \n\
     #if __VERSION__ >= 130                                                   \n\
         #define TEXTURE texture                                              \n\
     #else                                                                    \n\
         #define TEXTURE texture2D                                            \n\
     #endif                                                                   \n\
 ";
-const char kFragHeader_Tex2DRect[] = "\
+const char* const kFragHeader_Tex2DRect = "\
     #define SAMPLER sampler2DRect                                            \n\
     #if __VERSION__ >= 130                                                   \n\
         #define TEXTURE texture                                              \n\
     #else                                                                    \n\
         #define TEXTURE texture2DRect                                        \n\
     #endif                                                                   \n\
 ";
-const char kFragHeader_TexExt[] = "\
+const char* const kFragHeader_TexExt = "\
     #extension GL_OES_EGL_image_external : require                           \n\
     #define SAMPLER samplerExternalOES                                       \n\
     #define TEXTURE texture2D                                                \n\
 ";
 
-const char kFragBody_RGBA[] = "\
+const char* const kFragBody_RGBA = "\
     VARYING vec2 vTexCoord0;                                                 \n\
     uniform SAMPLER uTex0;                                                   \n\
                                                                              \n\
     void main(void)                                                          \n\
     {                                                                        \n\
         FRAG_COLOR = TEXTURE(uTex0, vTexCoord0);                             \n\
     }                                                                        \n\
 ";
-const char kFragBody_CrYCb[] = "\
+const char* const kFragBody_CrYCb = "\
     VARYING vec2 vTexCoord0;                                                 \n\
     uniform SAMPLER uTex0;                                                   \n\
     uniform mat4 uColorMatrix;                                               \n\
                                                                              \n\
     void main(void)                                                          \n\
     {                                                                        \n\
         vec4 yuv = vec4(TEXTURE(uTex0, vTexCoord0).gbr,                      \n\
                         1.0);                                                \n\
         vec4 rgb = uColorMatrix * yuv;                                       \n\
         FRAG_COLOR = vec4(rgb.rgb, 1.0);                                     \n\
     }                                                                        \n\
 ";
-const char kFragBody_NV12[] = "\
+const char* const kFragBody_NV12 = "\
     VARYING vec2 vTexCoord0;                                                 \n\
     VARYING vec2 vTexCoord1;                                                 \n\
     uniform SAMPLER uTex0;                                                   \n\
     uniform SAMPLER uTex1;                                                   \n\
     uniform mat4 uColorMatrix;                                               \n\
                                                                              \n\
     void main(void)                                                          \n\
     {                                                                        \n\
         vec4 yuv = vec4(TEXTURE(uTex0, vTexCoord0).x,                        \n\
                         TEXTURE(uTex1, vTexCoord1).xy,                       \n\
                         1.0);                                                \n\
         vec4 rgb = uColorMatrix * yuv;                                       \n\
         FRAG_COLOR = vec4(rgb.rgb, 1.0);                                     \n\
     }                                                                        \n\
 ";
-const char kFragBody_PlanarYUV[] = "\
+const char* const kFragBody_PlanarYUV = "\
     VARYING vec2 vTexCoord0;                                                 \n\
     VARYING vec2 vTexCoord1;                                                 \n\
     uniform SAMPLER uTex0;                                                   \n\
     uniform SAMPLER uTex1;                                                   \n\
     uniform SAMPLER uTex2;                                                   \n\
     uniform mat4 uColorMatrix;                                               \n\
                                                                              \n\
     void main(void)                                                          \n\
diff --git a/gfx/gl/GLBlitHelper.h b/gfx/gl/GLBlitHelper.h
--- a/gfx/gl/GLBlitHelper.h
+++ b/gfx/gl/GLBlitHelper.h
@@ -185,12 +185,20 @@ private:
                         const gfx::IntSize& destSize, OriginPos destOrigin) const;
 
     bool BlitAnglePlanes(uint8_t numPlanes, const RefPtr<ID3D11Texture2D>* texD3DList,
                          const DrawBlitProg* prog, const DrawBlitProg::BaseArgs& baseArgs,
                          const DrawBlitProg::YUVArgs* const yuvArgs) const;
 #endif
 };
 
+extern const char* const kFragHeader_Tex2D;
+extern const char* const kFragHeader_Tex2DRect;
+extern const char* const kFragHeader_TexExt;
+extern const char* const kFragBody_RGBA;
+extern const char* const kFragBody_CrYCb;
+extern const char* const kFragBody_NV12;
+extern const char* const kFragBody_PlanarYUV;
+
 } // namespace gl
 } // namespace mozilla
 
 #endif // GLBLITHELPER_H_
diff --git a/gfx/gl/GLBlitHelperD3D.cpp b/gfx/gl/GLBlitHelperD3D.cpp
--- a/gfx/gl/GLBlitHelperD3D.cpp
+++ b/gfx/gl/GLBlitHelperD3D.cpp
@@ -283,17 +283,17 @@ GLBlitHelper::BlitDescriptor(const layer
     MOZ_ASSERT(ySize.height % divisors.height == 0);
     const gfx::IntSize uvSize(ySize.width / divisors.width,
                               ySize.height / divisors.height);
 
     const bool yFlip = destOrigin != srcOrigin;
     const DrawBlitProg::BaseArgs baseArgs = { destSize, yFlip, clipRect, ySize };
     const DrawBlitProg::YUVArgs yuvArgs = { uvSize, divisors, colorSpace };
 
-    const auto& prog = GetDrawBlitProg(DrawBlitType::TexExtNV12);
+    const auto& prog = GetDrawBlitProg({kFragHeader_TexExt, kFragBody_NV12});
     MOZ_RELEASE_ASSERT(prog);
     prog->Draw(baseArgs, &yuvArgs);
     return true;
 }
 
 // --
 
 bool
@@ -319,16 +319,16 @@ GLBlitHelper::BlitAngleYCbCr(const Windo
         OpenSharedTexture(d3d, handleList[2])
     };
     const BindAnglePlanes bindPlanes(this, 3, texList);
 
     const bool yFlip = destOrigin != srcOrigin;
     const DrawBlitProg::BaseArgs baseArgs = { destSize, yFlip, clipRect, ySize };
     const DrawBlitProg::YUVArgs yuvArgs = { uvSize, divisors, colorSpace };
 
-    const auto& prog = GetDrawBlitProg(DrawBlitType::TexExtPlanarYUV);
+    const auto& prog = GetDrawBlitProg({kFragHeader_TexExt, kFragBody_PlanarYUV});
     MOZ_RELEASE_ASSERT(prog);
     prog->Draw(baseArgs, &yuvArgs);
     return true;
 }
 
 } // namespace gl
 } // namespace mozilla
diff --git a/gfx/gl/SharedSurface.cpp b/gfx/gl/SharedSurface.cpp
--- a/gfx/gl/SharedSurface.cpp
+++ b/gfx/gl/SharedSurface.cpp
@@ -65,27 +65,25 @@ SharedSurface::ProdCopy(SharedSurface* s
             GLuint destTex = dest->ProdTexture();
             GLenum destTarget = dest->ProdTextureTarget();
 
             const ScopedBindFramebuffer bindFB(gl, 0);
 
             gl->BlitHelper()->BlitFramebufferToTexture(destTex,
                                                        src->mSize,
                                                        dest->mSize,
-                                                       destTarget,
-                                                       true);
+                                                       destTarget);
         } else if (dest->mAttachType == AttachmentType::GLRenderbuffer) {
             GLuint destRB = dest->ProdRenderbuffer();
             ScopedFramebufferForRenderbuffer destWrapper(gl, destRB);
 
             gl->BlitHelper()->BlitFramebufferToFramebuffer(0,
                                                            destWrapper.FB(),
                                                            src->mSize,
-                                                           dest->mSize,
-                                                           true);
+                                                           dest->mSize);
         } else {
             MOZ_CRASH("GFX: Unhandled dest->mAttachType 1.");
         }
 
         if (srcNeedsUnlock)
             src->UnlockProd();
 
         if (origNeedsRelock)
@@ -112,27 +110,25 @@ SharedSurface::ProdCopy(SharedSurface* s
             GLuint srcTex = src->ProdTexture();
             GLenum srcTarget = src->ProdTextureTarget();
 
             const ScopedBindFramebuffer bindFB(gl, 0);
 
             gl->BlitHelper()->BlitTextureToFramebuffer(srcTex,
                                                        src->mSize,
                                                        dest->mSize,
-                                                       srcTarget,
-                                                       !!gl->Screen());
+                                                       srcTarget);
         } else if (src->mAttachType == AttachmentType::GLRenderbuffer) {
             GLuint srcRB = src->ProdRenderbuffer();
             ScopedFramebufferForRenderbuffer srcWrapper(gl, srcRB);
 
             gl->BlitHelper()->BlitFramebufferToFramebuffer(srcWrapper.FB(),
                                                            0,
                                                            src->mSize,
-                                                           dest->mSize,
-                                                           true);
+                                                           dest->mSize);
         } else {
             MOZ_CRASH("GFX: Unhandled src->mAttachType 2.");
         }
 
         if (destNeedsUnlock)
             dest->UnlockProd();
 
         if (origNeedsRelock)

