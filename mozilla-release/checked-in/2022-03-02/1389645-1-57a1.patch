# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1503784768 -7200
# Node ID 3311859df58878fbae16c76cec0ecdc7b61d681a
# Parent  4fad8da62099123db8f0ef06e5adccdb03513a3e
Bug 1389645: Don't incorrectly set lazy frame construction bits in ContentAppended and ContentRangeInserted. r=bholley

Before this patch, we may get into that piece of code reentrantly from lazy
frame construction itself leaving stale dirty bits around that we really don't
want.

MozReview-Commit-ID: 2wtKeF3o0Gr

diff --git a/layout/base/nsCSSFrameConstructor.cpp b/layout/base/nsCSSFrameConstructor.cpp
--- a/layout/base/nsCSSFrameConstructor.cpp
+++ b/layout/base/nsCSSFrameConstructor.cpp
@@ -7634,18 +7634,19 @@ nsCSSFrameConstructor::ContentAppended(n
     !aFirstNewContent->IsInNativeAnonymousSubtree();
 
   if (!isNewShadowTreeContent) {
     // See comment in ContentRangeInserted for why this is necessary.
     if (!GetContentInsertionFrameFor(aContainer) &&
         !aContainer->IsActiveChildrenElement()) {
       // We're punting on frame construction because there's no container frame.
       // The Servo-backed style system handles this case like the lazy frame
-      // construction case.
-      if (isNewlyAddedContentForServo) {
+      // construction case, except when we're already constructing frames, in
+      // which case we shouldn't need to do anything else.
+      if (isNewlyAddedContentForServo && aAllowLazyConstruction) {
         LazilyStyleNewChildRange(aFirstNewContent, nullptr);
       }
       return;
     }
 
     if (aAllowLazyConstruction &&
         MaybeConstructLazily(CONTENTAPPEND, aContainer, aFirstNewContent)) {
       if (isNewlyAddedContentForServo) {
@@ -8101,18 +8102,19 @@ nsCSSFrameConstructor::ContentRangeInser
   if (!isNewShadowTreeContent) {
     nsContainerFrame* parentFrame = GetContentInsertionFrameFor(aContainer);
     // The xbl:children element won't have a frame, but default content can have the children as
     // a parent. While its uncommon to change the structure of the default content itself, a label,
     // for example, can be reframed by having its value attribute set or removed.
     if (!parentFrame && !aContainer->IsActiveChildrenElement()) {
       // We're punting on frame construction because there's no container frame.
       // The Servo-backed style system handles this case like the lazy frame
-      // construction case.
-      if (isNewlyAddedContentForServo) {
+      // construction case, except when we're already constructing frames, in
+      // which case we shouldn't need to do anything else.
+      if (isNewlyAddedContentForServo && aAllowLazyConstruction) {
         LazilyStyleNewChildRange(aStartChild, aEndChild);
       }
       return;
     }
 
     // Otherwise, we've got parent content. Find its frame.
     NS_ASSERTION(!parentFrame || parentFrame->GetContent() == aContainer ||
                  GetDisplayContentsStyleFor(aContainer), "New XBL code is possibly wrong!");
