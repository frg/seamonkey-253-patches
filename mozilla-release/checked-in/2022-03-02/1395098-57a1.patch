# HG changeset patch
# User Alexis Beingessner <a.beingessner@gmail.com>
# Date 1504113489 14400
# Node ID 7663f58bcf9696d17563197e3de9883f11a75231
# Parent  145bcb899801891b68c3797f0f0c8881746b084c
Bug 1395098 - Apply folded alpha to text when using advanced layers r=jrmuizel

MozReview-Commit-ID: 6crHThEP6Ha

diff --git a/layout/generic/nsTextFrame.cpp b/layout/generic/nsTextFrame.cpp
--- a/layout/generic/nsTextFrame.cpp
+++ b/layout/generic/nsTextFrame.cpp
@@ -5226,18 +5226,24 @@ nsDisplayText::CreateWebRenderCommands(m
     aBuilder.PushTextShadow(wrBoundsRect, wrClipRect, shadow);
   }
 
   for (const wr::Line& decoration: mTextDrawer->GetBeforeDecorations()) {
     aBuilder.PushLine(wrClipRect, decoration);
   }
 
   for (const mozilla::layout::TextRunFragment& text: mTextDrawer->GetText()) {
+    // mOpacity is set after we do our analysis, so we need to apply it here.
+    // mOpacity is only non-trivial when we have "pure" text, so we don't
+    // ever need to apply it to shadows or decorations.
+    auto color = text.color;
+    color.a *= mOpacity;
+
     aManager->WrBridge()->PushGlyphs(aBuilder, text.glyphs, text.font,
-                                     text.color, aSc, boundsRect, clipRect);
+                                     color, aSc, boundsRect, clipRect);
   }
 
   for (const wr::Line& decoration: mTextDrawer->GetAfterDecorations()) {
     aBuilder.PushLine(wrClipRect, decoration);
   }
 
   for (size_t i = 0; i < mTextDrawer->GetShadows().Length(); ++i) {
     aBuilder.PopTextShadow();
@@ -5273,17 +5279,21 @@ nsDisplayText::BuildLayer(nsDisplayListB
   allGlyphs.SetCapacity(mTextDrawer->GetText().Length());
   for (const mozilla::layout::TextRunFragment& text : mTextDrawer->GetText()) {
     if (!font) {
       font = text.font;
     }
 
     GlyphArray* glyphs = allGlyphs.AppendElement();
     glyphs->glyphs() = text.glyphs;
-    glyphs->color() = text.color;
+
+    // Apply folded alpha (only applies to glyphs)
+    auto color = text.color;
+    color.a *= mOpacity;
+    glyphs->color() = color;
   }
 
   MOZ_ASSERT(font);
 
   layer->SetGlyphs(Move(allGlyphs));
   layer->SetScaledFont(font);
 
   auto A2D = mFrame->PresContext()->AppUnitsPerDevPixel();
