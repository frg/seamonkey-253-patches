# HG changeset patch
# User Frank-Rainer Grahl <frgrahl@gmx.net>
# Date 1571161560 -7200
# Parent  225b6fc2bc4a9a488d48d728c831fd38dba7f1c7
Bug 1602256 - Part 3: Kill telemetry in other toolkit components. r=IanN a=IanN

diff --git a/toolkit/components/addoncompat/CompatWarning.jsm b/toolkit/components/addoncompat/CompatWarning.jsm
--- a/toolkit/components/addoncompat/CompatWarning.jsm
+++ b/toolkit/components/addoncompat/CompatWarning.jsm
@@ -37,21 +37,16 @@ var CompatWarning = {
     let alreadyWarned = false;
 
     return function() {
       if (alreadyWarned) {
         return;
       }
       alreadyWarned = true;
 
-      if (addon) {
-        let histogram = Services.telemetry.getKeyedHistogramById("ADDON_SHIM_USAGE");
-        histogram.add(addon, warning ? warning.number : 0);
-      }
-
       if (!Preferences.get("dom.ipc.shims.enabledWarnings", false))
         return;
 
       let error = Cc["@mozilla.org/scripterror;1"].createInstance(Ci.nsIScriptError);
       if (!error || !Services.console) {
         // Too late during shutdown to use the nsIConsole
         return;
       }
diff --git a/toolkit/components/addoncompat/RemoteAddonsChild.jsm b/toolkit/components/addoncompat/RemoteAddonsChild.jsm
--- a/toolkit/components/addoncompat/RemoteAddonsChild.jsm
+++ b/toolkit/components/addoncompat/RemoteAddonsChild.jsm
@@ -14,19 +14,16 @@ ChromeUtils.defineModuleGetter(this, "Pr
 
 XPCOMUtils.defineLazyServiceGetter(this, "SystemPrincipal",
                                    "@mozilla.org/systemprincipal;1", "nsIPrincipal");
 
 XPCOMUtils.defineLazyServiceGetter(this, "contentSecManager",
                                    "@mozilla.org/contentsecuritymanager;1",
                                    "nsIContentSecurityManager");
 
-const TELEMETRY_SHOULD_LOAD_LOADING_KEY = "ADDON_CONTENT_POLICY_SHIM_BLOCKING_LOADING_MS";
-const TELEMETRY_SHOULD_LOAD_LOADED_KEY = "ADDON_CONTENT_POLICY_SHIM_BLOCKING_LOADED_MS";
-
 // Similar to Python. Returns dict[key] if it exists. Otherwise,
 // sets dict[key] to default_ and returns default_.
 function setDefault(dict, key, default_) {
   if (key in dict) {
     return dict[key];
   }
   dict[key] = default_;
   return default_;
@@ -148,132 +145,54 @@ var NotificationTracker = {
 // it runs, it notifies the parent that it needs to run its own
 // nsIContentPolicy list. If any policy in the parent rejects a
 // resource load, that answer is returned to the child.
 var ContentPolicyChild = {
   _classDescription: "Addon shim content policy",
   _classID: Components.ID("6e869130-635c-11e2-bcfd-0800200c9a66"),
   _contractID: "@mozilla.org/addon-child/policy;1",
 
-  // A weak map of time spent blocked in hooks for a given document.
-  // WeakMap[document -> Map[addonId -> timeInMS]]
-  timings: new WeakMap(),
-
   init() {
     let registrar = Components.manager.QueryInterface(Ci.nsIComponentRegistrar);
     registrar.registerFactory(this._classID, this._classDescription, this._contractID, this);
 
-    this.loadingHistogram = Services.telemetry.getKeyedHistogramById(TELEMETRY_SHOULD_LOAD_LOADING_KEY);
-    this.loadedHistogram = Services.telemetry.getKeyedHistogramById(TELEMETRY_SHOULD_LOAD_LOADED_KEY);
-
     NotificationTracker.watch("content-policy", this);
   },
 
   QueryInterface: XPCOMUtils.generateQI([Ci.nsIContentPolicy, Ci.nsIObserver,
                                          Ci.nsIChannelEventSink, Ci.nsIFactory,
                                          Ci.nsISupportsWeakReference]),
 
   track(path, register) {
     let catMan = Cc["@mozilla.org/categorymanager;1"].getService(Ci.nsICategoryManager);
     if (register) {
       catMan.addCategoryEntry("content-policy", this._contractID, this._contractID, false, true);
     } else {
       catMan.deleteCategoryEntry("content-policy", this._contractID, false);
     }
   },
 
-  // Returns a map of cumulative time spent in shouldLoad hooks for a
-  // given add-on in the given node's document. May return null if
-  // telemetry recording is disabled, or the given context does not
-  // point to a document.
-  getTimings(context) {
-    if (!Services.telemetry.canRecordExtended) {
-      return null;
-    }
-
-    let doc;
-    if (context instanceof Ci.nsIDOMNode) {
-      doc = context.ownerDocument;
-    } else if (context instanceof Ci.nsIDOMDocument) {
-      doc = context;
-    } else if (context instanceof Ci.nsIDOMWindow) {
-      doc = context.document;
-    }
-
-    if (!doc) {
-      return null;
-    }
-
-    let map = this.timings.get(doc);
-    if (!map) {
-      // No timing object exists for this document yet. Create one, and
-      // set up a listener to record the final values at the right time.
-      map = new Map();
-      this.timings.set(doc, map);
-
-      // If the document is still loading, record aggregate pre-load
-      // timings when the load event fires. If it's already loaded,
-      // record aggregate post-load timings when the page is hidden.
-      let eventName = doc.readyState == "complete" ? "pagehide" : "load";
-
-      let listener = event => {
-        if (event.target == doc) {
-          event.currentTarget.removeEventListener(eventName, listener, true);
-          this.logTelemetry(doc, eventName);
-        }
-      };
-      doc.defaultView.addEventListener(eventName, listener, true);
-    }
-    return map;
-  },
-
-  // Logs the accumulated telemetry for the given document, into the
-  // appropriate telemetry histogram based on the DOM event name that
-  // triggered it.
-  logTelemetry(doc, eventName) {
-    let map = this.timings.get(doc);
-    this.timings.delete(doc);
-
-    let histogram = eventName == "load" ? this.loadingHistogram : this.loadedHistogram;
-
-    for (let [addon, time] of map.entries()) {
-      histogram.add(addon, time);
-    }
-  },
-
   shouldLoad(contentType, contentLocation, requestOrigin,
                        node, mimeTypeGuess, extra, requestPrincipal) {
-    let startTime = Cu.now();
-
     let addons = NotificationTracker.findSuffixes(["content-policy"]);
     let [prefetched, cpows] = Prefetcher.prefetch("ContentPolicy.shouldLoad",
                                                   addons, {InitNode: node});
     cpows.node = node;
 
     let cpmm = Cc["@mozilla.org/childprocessmessagemanager;1"]
                .getService(Ci.nsISyncMessageSender);
     let rval = cpmm.sendRpcMessage("Addons:ContentPolicy:Run", {
       contentType,
       contentLocation: contentLocation.spec,
       requestOrigin: requestOrigin ? requestOrigin.spec : null,
       mimeTypeGuess,
       requestPrincipal,
       prefetched,
     }, cpows);
 
-    let timings = this.getTimings(node);
-    if (timings) {
-      let delta = Cu.now() - startTime;
-
-      for (let addon of addons) {
-        let old = timings.get(addon) || 0;
-        timings.set(addon, old + delta);
-      }
-    }
-
     if (rval.length != 1) {
       return Ci.nsIContentPolicy.ACCEPT;
     }
 
     return rval[0];
   },
 
   shouldProcess(contentType, contentLocation, requestOrigin, insecNode, mimeType, extra) {
diff --git a/toolkit/components/alerts/nsAlertsService.cpp b/toolkit/components/alerts/nsAlertsService.cpp
--- a/toolkit/components/alerts/nsAlertsService.cpp
+++ b/toolkit/components/alerts/nsAlertsService.cpp
@@ -1,17 +1,16 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode:nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/dom/ContentChild.h"
 #include "mozilla/dom/PermissionMessageUtils.h"
 #include "mozilla/Preferences.h"
-#include "mozilla/Telemetry.h"
 #include "nsXULAppAPI.h"
 
 #include "nsAlertsService.h"
 
 #include "nsXPCOM.h"
 #include "nsIServiceManager.h"
 #include "nsIDOMWindow.h"
 #include "nsPromiseFlatString.h"
@@ -287,19 +286,16 @@ NS_IMETHODIMP nsAlertsService::SetManual
 {
 #ifdef MOZ_WIDGET_ANDROID
   return NS_ERROR_NOT_IMPLEMENTED;
 #else
   nsCOMPtr<nsIAlertsDoNotDisturb> alertsDND(GetDNDBackend());
   NS_ENSURE_TRUE(alertsDND, NS_ERROR_NOT_IMPLEMENTED);
 
   nsresult rv = alertsDND->SetManualDoNotDisturb(aDoNotDisturb);
-  if (NS_SUCCEEDED(rv)) {
-    Telemetry::Accumulate(Telemetry::ALERTS_SERVICE_DND_ENABLED, 1);
-  }
   return rv;
 #endif
 }
 
 already_AddRefed<nsIAlertsDoNotDisturb>
 nsAlertsService::GetDNDBackend()
 {
   // Try the system notification service.
diff --git a/toolkit/components/narrate/NarrateControls.jsm b/toolkit/components/narrate/NarrateControls.jsm
--- a/toolkit/components/narrate/NarrateControls.jsm
+++ b/toolkit/components/narrate/NarrateControls.jsm
@@ -3,17 +3,16 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 ChromeUtils.import("resource://gre/modules/narrate/VoiceSelect.jsm");
 ChromeUtils.import("resource://gre/modules/narrate/Narrator.jsm");
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/AsyncPrefs.jsm");
-ChromeUtils.import("resource://gre/modules/TelemetryStopwatch.jsm");
 
 this.EXPORTED_SYMBOLS = ["NarrateControls"];
 
 var gStrings = Services.strings.createBundle("chrome://global/locale/narrate.properties");
 
 function NarrateControls(mm, win, languagePromise) {
   this._mm = mm;
   this._winRef = Cu.getWeakReference(win);
@@ -147,19 +146,16 @@ NarrateControls.prototype = {
         break;
       case "click":
         this._onButtonClick(evt);
         break;
       case "voiceschanged":
         this._setupVoices();
         break;
       case "unload":
-        if (this.narrator.speaking) {
-          TelemetryStopwatch.finish("NARRATE_CONTENT_SPEAKTIME_MS", this);
-        }
         break;
     }
   },
 
   /**
    * Returns true if synth voices are available.
    */
   _setupVoices() {
@@ -185,30 +181,19 @@ NarrateControls.prototype = {
           label: gStrings.GetStringFromName("defaultvoice"),
           value: "automatic",
           selected: selectedVoice == "automatic"
         });
         this.voiceSelect.addOptions(options);
       }
 
       let narrateToggle = win.document.getElementById("narrate-toggle");
-      let histogram = Services.telemetry.getKeyedHistogramById(
-        "NARRATE_CONTENT_BY_LANGUAGE_2");
       let initial = !this._voicesInitialized;
       this._voicesInitialized = true;
 
-      if (initial) {
-        histogram.add(language, 0);
-      }
-
-      if (options.length && narrateToggle.hidden) {
-        // About to show for the first time..
-        histogram.add(language, 1);
-      }
-
       // We disable this entire feature if there are no available voices.
       narrateToggle.hidden = !options.length;
     });
   },
 
   _getVoicePref() {
     let voicePref = Services.prefs.getCharPref("narrate.voice");
     try {
@@ -266,22 +251,16 @@ NarrateControls.prototype = {
     dropdown.classList.toggle("speaking", speaking);
 
     let startStopButton = this._doc.getElementById("narrate-start-stop");
     startStopButton.title =
       gStrings.GetStringFromName(speaking ? "stop" : "start");
 
     this._doc.getElementById("narrate-skip-previous").disabled = !speaking;
     this._doc.getElementById("narrate-skip-next").disabled = !speaking;
-
-    if (speaking) {
-      TelemetryStopwatch.start("NARRATE_CONTENT_SPEAKTIME_MS", this);
-    } else {
-      TelemetryStopwatch.finish("NARRATE_CONTENT_SPEAKTIME_MS", this);
-    }
   },
 
   _createVoiceLabel(voice) {
     // This is a highly imperfect method of making human-readable labels
     // for system voices. Because each platform has a different naming scheme
     // for voices, we use a different method for each platform.
     switch (Services.appinfo.OS) {
       case "WINNT":
diff --git a/toolkit/components/processsingleton/ContentProcessSingleton.js b/toolkit/components/processsingleton/ContentProcessSingleton.js
--- a/toolkit/components/processsingleton/ContentProcessSingleton.js
+++ b/toolkit/components/processsingleton/ContentProcessSingleton.js
@@ -6,18 +6,16 @@
 
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 XPCOMUtils.defineLazyServiceGetter(this, "cpmm",
                                    "@mozilla.org/childprocessmessagemanager;1",
                                    "nsIMessageSender");
 
-ChromeUtils.defineModuleGetter(this, "TelemetryController",
-                               "resource://gre/modules/TelemetryController.jsm");
 ChromeUtils.defineModuleGetter(this, "E10SUtils",
                                "resource://gre/modules/E10SUtils.jsm");
 
 /*
  * The message manager has an upper limit on message sizes that it can
  * reliably forward to the parent so we limit the size of console log event
  * messages that we forward here. The web console is local and receives the
  * full console message, but addons subscribed to console event messages
@@ -43,17 +41,16 @@ ContentProcessSingleton.prototype = {
   QueryInterface: XPCOMUtils.generateQI([Ci.nsIObserver,
                                          Ci.nsISupportsWeakReference]),
 
   observe(subject, topic, data) {
     switch (topic) {
     case "app-startup": {
       Services.obs.addObserver(this, "console-api-log-event");
       Services.obs.addObserver(this, "xpcom-shutdown");
-      TelemetryController.observe(null, topic, null);
       break;
     }
     case "console-api-log-event": {
       let consoleMsg = subject.wrappedJSObject;
 
       let msgData = {
         level: consoleMsg.level,
         filename: consoleMsg.filename.substring(0, MSG_MGR_CONSOLE_INFO_MAX),
diff --git a/toolkit/components/telemetry/Histograms.json b/toolkit/components/telemetry/Histograms.json
--- a/toolkit/components/telemetry/Histograms.json
+++ b/toolkit/components/telemetry/Histograms.json
@@ -58,24 +58,16 @@
     "record_in_processes": ["main", "content"],
     "expires_in_version": "53",
     "kind": "count",
     "description": "Recorded when the addon manager shows the modal upgrade UI. Should only be recorded once per upgrade.",
     "releaseChannelCollection": "opt-out",
     "bug_numbers": [1268548],
     "alert_emails": ["kev@mozilla.com"]
   },
-  "ADDON_SHIM_USAGE": {
-    "record_in_processes": ["main", "content"],
-    "expires_in_version": "never",
-    "kind": "enumerated",
-    "n_values": 15,
-    "keyed": true,
-    "description": "Reasons why add-on shims were used, keyed by add-on ID."
-  },
   "ADDON_FORBIDDEN_CPOW_USAGE": {
     "record_in_processes": ["main", "content"],
     "expires_in_version": "never",
     "kind": "count",
     "keyed": true,
     "description": "Counts the number of times a given add-on used CPOWs when it was marked as e10s compatible.",
     "bug_numbers": [1214824],
     "alert_emails": ["wmccloskey@mozilla.com"]
@@ -9721,24 +9713,16 @@
   "WEBRTC_ICE_CHECKING_RATE": {
     "record_in_processes": ["main", "content"],
     "alert_emails": ["webrtc-ice-telemetry-alerts@mozilla.com"],
     "bug_numbers": [1188391,1319268],
     "expires_in_version": "58",
     "kind": "boolean",
     "description": "The number of ICE connections which immediately failed (0) vs. reached at least checking state (1)."
   },
-  "ALERTS_SERVICE_DND_ENABLED": {
-    "record_in_processes": ["main", "content"],
-    "alert_emails": ["firefox-dev@mozilla.org"],
-    "bug_numbers": [1219030],
-    "expires_in_version": "50",
-    "kind": "boolean",
-    "description": "XUL-only: whether the user has toggled do not disturb."
-  },
   "PLUGIN_DRAWING_MODEL": {
     "record_in_processes": ["main", "content"],
     "alert_emails": ["danderson@mozilla.com"],
     "expires_in_version": "never",
     "kind": "enumerated",
     "bug_numbers": [1229961],
     "n_values": 12,
     "description": "Plugin drawing model. 0 when windowed, otherwise NPDrawingModel + 1."
diff --git a/toolkit/components/telemetry/Telemetry.cpp b/toolkit/components/telemetry/Telemetry.cpp
--- a/toolkit/components/telemetry/Telemetry.cpp
+++ b/toolkit/components/telemetry/Telemetry.cpp
@@ -1883,26 +1883,16 @@ TelemetryImpl::SizeOfIncludingThis(mozil
 ////////////////////////////////////////////////////////////////////////
 ////////////////////////////////////////////////////////////////////////
 //
 // EXTERNALLY VISIBLE FUNCTIONS in no name space
 // These are NOT listed in Telemetry.h
 
 NSMODULE_DEFN(nsTelemetryModule) = &kTelemetryModule;
 
-/**
- * The XRE_TelemetryAdd function is to be used by embedding applications
- * that can't use mozilla::Telemetry::Accumulate() directly.
- */
-void
-XRE_TelemetryAccumulate(int aID, uint32_t aSample)
-{
-  mozilla::Telemetry::Accumulate((mozilla::Telemetry::HistogramID) aID, aSample);
-}
-
 
 ////////////////////////////////////////////////////////////////////////
 ////////////////////////////////////////////////////////////////////////
 //
 // EXTERNALLY VISIBLE FUNCTIONS in mozilla::
 // These are NOT listed in Telemetry.h
 
 namespace mozilla {
diff --git a/toolkit/components/telemetry/TelemetrySession.jsm b/toolkit/components/telemetry/TelemetrySession.jsm
--- a/toolkit/components/telemetry/TelemetrySession.jsm
+++ b/toolkit/components/telemetry/TelemetrySession.jsm
@@ -156,24 +156,17 @@ function getPingType(aPayload) {
   return PING_TYPE_MAIN;
 }
 
 /**
  * Annotate the current session ID with the crash reporter to map potential
  * crash pings with the related main ping.
  */
 function annotateCrashReport(sessionId) {
-  try {
-    const cr = Cc["@mozilla.org/toolkit/crash-reporter;1"];
-    if (cr) {
-      cr.getService(Ci.nsICrashReporter).setTelemetrySessionId(sessionId);
-    }
-  } catch (e) {
-    // Ignore errors when crash reporting is disabled
-  }
+  // Go away nothing to see here any longer
 }
 
 /**
  * Read current process I/O counters.
  */
 var processInfo = {
   _initialized: false,
   _IO_COUNTERS: null,
diff --git a/toolkit/components/telemetry/histogram-whitelists.json b/toolkit/components/telemetry/histogram-whitelists.json
--- a/toolkit/components/telemetry/histogram-whitelists.json
+++ b/toolkit/components/telemetry/histogram-whitelists.json
@@ -1,11 +1,10 @@
 {
   "alert_emails": [
-    "ADDON_SHIM_USAGE",
     "AUDIOSTREAM_FIRST_OPEN_MS",
     "AUDIOSTREAM_LATER_OPEN_MS",
     "AUTO_REJECTED_TRANSLATION_OFFERS",
     "BAD_FALLBACK_FONT",
     "BROWSERPROVIDER_XUL_IMPORT_BOOKMARKS",
     "BROWSER_IS_ASSIST_DEFAULT",
     "BROWSER_IS_USER_DEFAULT",
     "BROWSER_IS_USER_DEFAULT_ERROR",
@@ -406,17 +405,16 @@
     "WEBCRYPTO_METHOD",
     "WEBCRYPTO_RESOLVED",
     "XMLHTTPREQUEST_ASYNC_OR_SYNC",
     "XUL_CACHE_DISABLED"
   ],
   "bug_numbers": [
     "A11Y_IATABLE_USAGE_FLAG",
     "A11Y_ISIMPLEDOM_USAGE_FLAG",
-    "ADDON_SHIM_USAGE",
     "APPLICATION_REPUTATION_COUNT",
     "APPLICATION_REPUTATION_LOCAL",
     "APPLICATION_REPUTATION_SERVER",
     "APPLICATION_REPUTATION_SHOULD_BLOCK",
     "AUDIOSTREAM_FIRST_OPEN_MS",
     "AUDIOSTREAM_LATER_OPEN_MS",
     "AUTO_REJECTED_TRANSLATION_OFFERS",
     "BAD_FALLBACK_FONT",
diff --git a/toolkit/crashreporter/nsExceptionHandler.cpp b/toolkit/crashreporter/nsExceptionHandler.cpp
--- a/toolkit/crashreporter/nsExceptionHandler.cpp
+++ b/toolkit/crashreporter/nsExceptionHandler.cpp
@@ -2914,28 +2914,16 @@ GetDefaultMemoryReportFile(nsIFile** aFi
     if (!defaultMemoryReportFile) {
       return NS_ERROR_FAILURE;
     }
   }
   defaultMemoryReportFile.forget(aFile);
   return NS_OK;
 }
 
-void
-SetTelemetrySessionId(const nsACString& id)
-{
-  if (!gExceptionHandler) {
-    return;
-  }
-  if (currentSessionId) {
-    free(currentSessionId);
-  }
-  currentSessionId = ToNewCString(id);
-}
-
 static void
 FindPendingDir()
 {
   if (pendingDirectory)
     return;
 
   nsCOMPtr<nsIFile> pendingDir;
   nsresult rv = NS_GetSpecialDirectory("UAppData", getter_AddRefs(pendingDir));
diff --git a/toolkit/crashreporter/nsExceptionHandler.h b/toolkit/crashreporter/nsExceptionHandler.h
--- a/toolkit/crashreporter/nsExceptionHandler.h
+++ b/toolkit/crashreporter/nsExceptionHandler.h
@@ -50,17 +50,16 @@ nsresult UnsetExceptionHandler();
  * 2. <profile>/crashes/events
  * 3. <UAppData>/Crash Reports/events
  */
 void SetUserAppDataDirectory(nsIFile* aDir);
 void SetProfileDirectory(nsIFile* aDir);
 void UpdateCrashEventsDir();
 void SetMemoryReportFile(nsIFile* aFile);
 nsresult GetDefaultMemoryReportFile(nsIFile** aFile);
-void SetTelemetrySessionId(const nsACString& id);
 
 /**
  * Get the path where crash event files should be written.
  */
 bool     GetCrashEventsDir(nsAString& aPath);
 
 bool     GetEnabled();
 bool     GetServerURL(nsACString& aServerURL);
diff --git a/toolkit/xre/Bootstrap.cpp b/toolkit/xre/Bootstrap.cpp
--- a/toolkit/xre/Bootstrap.cpp
+++ b/toolkit/xre/Bootstrap.cpp
@@ -32,20 +32,16 @@ public:
   virtual void NS_LogInit() override {
     ::NS_LogInit();
   }
 
   virtual void NS_LogTerm() override {
     ::NS_LogTerm();
   }
 
-  virtual void XRE_TelemetryAccumulate(int aID, uint32_t aSample) override {
-    ::XRE_TelemetryAccumulate(aID, aSample);
-  }
-
   virtual void XRE_StartupTimelineRecord(int aEvent, mozilla::TimeStamp aWhen) override {
     ::XRE_StartupTimelineRecord(aEvent, aWhen);
   }
 
   virtual int XRE_main(int argc, char* argv[], const BootstrapConfig& aConfig) override {
     return ::XRE_main(argc, argv, aConfig);
   }
 
diff --git a/toolkit/xre/Bootstrap.h b/toolkit/xre/Bootstrap.h
--- a/toolkit/xre/Bootstrap.h
+++ b/toolkit/xre/Bootstrap.h
@@ -87,18 +87,16 @@ protected:
 
 public:
   typedef mozilla::UniquePtr<Bootstrap, BootstrapDelete> UniquePtr;
 
   virtual void NS_LogInit() = 0;
 
   virtual void NS_LogTerm() = 0;
 
-  virtual void XRE_TelemetryAccumulate(int aID, uint32_t aSample) = 0;
-
   virtual void XRE_StartupTimelineRecord(int aEvent, mozilla::TimeStamp aWhen) = 0;
 
   virtual int XRE_main(int argc, char* argv[], const BootstrapConfig& aConfig) = 0;
 
   virtual void XRE_StopLateWriteChecks() = 0;
 
   virtual int XRE_XPCShellMain(int argc, char** argv, char** envp, const XREShellData* aShellData) = 0;
 
diff --git a/toolkit/xre/nsAppRunner.cpp b/toolkit/xre/nsAppRunner.cpp
--- a/toolkit/xre/nsAppRunner.cpp
+++ b/toolkit/xre/nsAppRunner.cpp
@@ -14,17 +14,16 @@
 #include "mozilla/IOInterposer.h"
 #include "mozilla/Likely.h"
 #include "mozilla/MemoryChecking.h"
 #include "mozilla/Poison.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/Printf.h"
 #include "mozilla/ScopeExit.h"
 #include "mozilla/Services.h"
-#include "mozilla/Telemetry.h"
 #include "mozilla/intl/LocaleService.h"
 
 #include "nsAppRunner.h"
 #include "mozilla/XREAppData.h"
 #include "mozilla/Bootstrap.h"
 #if defined(MOZ_UPDATER) && !defined(MOZ_WIDGET_ANDROID)
 #include "nsUpdateDriver.h"
 #endif
@@ -1385,23 +1384,16 @@ nsXULAppInfo::SaveMemoryReport()
 
   rv = dumper->DumpMemoryReportsToNamedFile(path, this, file, true /* anonymize */);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
   return NS_OK;
 }
 
-NS_IMETHODIMP
-nsXULAppInfo::SetTelemetrySessionId(const nsACString& id)
-{
-  CrashReporter::SetTelemetrySessionId(id);
-  return NS_OK;
-}
-
 // This method is from nsIFInishDumpingCallback.
 NS_IMETHODIMP
 nsXULAppInfo::Callback(nsISupports* aData)
 {
   nsCOMPtr<nsIFile> file = do_QueryInterface(aData);
   MOZ_ASSERT(file);
 
   CrashReporter::SetMemoryReportFile(file);
@@ -1950,18 +1942,16 @@ ProfileLockedDialog(nsIFile* aProfileDir
                     nsINativeAppSupport* aNative, nsIProfileLock* *aResult)
 {
   nsresult rv;
 
   ScopedXPCOMStartup xpcom;
   rv = xpcom.Initialize();
   NS_ENSURE_SUCCESS(rv, rv);
 
-  mozilla::Telemetry::WriteFailedProfileLock(aProfileDir);
-
   rv = xpcom.SetWindowCreator(aNative);
   NS_ENSURE_SUCCESS(rv, NS_ERROR_FAILURE);
 
   { //extra scoping is needed so we release these components before xpcom shutdown
     nsCOMPtr<nsIStringBundleService> sbs =
       mozilla::services::GetStringBundleService();
     NS_ENSURE_TRUE(sbs, NS_ERROR_FAILURE);
 
@@ -3759,19 +3749,17 @@ AnnotateLSBRelease(void*)
 #endif
 
 namespace mozilla {
   ShutdownChecksMode gShutdownChecks = SCM_NOTHING;
 } // namespace mozilla
 
 static void SetShutdownChecks() {
   // Set default first. On debug builds we crash. On nightly and local
-  // builds we record. Nightlies will then send the info via telemetry,
-  // but it is usefull to have the data in about:telemetry in local builds
-  // too.
+  // builds we record.
 
 #ifdef DEBUG
   gShutdownChecks = SCM_CRASH;
 #else
   const char* releaseChannel = NS_STRINGIFY(MOZ_UPDATE_CHANNEL);
   if (strcmp(releaseChannel, "nightly") == 0 ||
       strcmp(releaseChannel, "default") == 0) {
     gShutdownChecks = SCM_RECORD;
@@ -3806,29 +3794,16 @@ XREMain::XRE_mainStartup(bool* aExitFlag
   nsresult rv;
 
   if (!aExitFlag)
     return 1;
   *aExitFlag = false;
 
   SetShutdownChecks();
 
-  // Enable Telemetry IO Reporting on DEBUG, nightly and local builds
-#ifdef DEBUG
-  mozilla::Telemetry::InitIOReporting(gAppData->xreDirectory);
-#else
-  {
-    const char* releaseChannel = NS_STRINGIFY(MOZ_UPDATE_CHANNEL);
-    if (strcmp(releaseChannel, "nightly") == 0 ||
-        strcmp(releaseChannel, "default") == 0) {
-      mozilla::Telemetry::InitIOReporting(gAppData->xreDirectory);
-    }
-  }
-#endif /* DEBUG */
-
 #if defined(XP_WIN)
   // Enable the HeapEnableTerminationOnCorruption exploit mitigation. We ignore
   // the return code because it always returns success, although it has no
   // effect on Windows older than XP SP3.
   HeapSetInformation(NULL, HeapEnableTerminationOnCorruption, NULL, 0);
 #endif /* XP_WIN */
 
 #if defined(MOZ_WIDGET_GTK) || defined(MOZ_ENABLE_XREMOTE)
@@ -4153,18 +4128,16 @@ XREMain::XRE_mainStartup(bool* aExitFlag
   rv = mProfileLock->GetLocalDirectory(getter_AddRefs(mProfLD));
   NS_ENSURE_SUCCESS(rv, 1);
 
   rv = mDirProvider.SetProfile(mProfD, mProfLD);
   NS_ENSURE_SUCCESS(rv, 1);
 
   //////////////////////// NOW WE HAVE A PROFILE ////////////////////////
 
-  mozilla::Telemetry::SetProfileDir(mProfD);
-
 #ifdef MOZ_CRASHREPORTER
   if (mAppData->flags & NS_XRE_ENABLE_CRASH_REPORTER)
       MakeOrSetMinidumpPath(mProfD);
 
   CrashReporter::SetProfileDirectory(mProfD);
 #endif
 
   nsAutoCString version;
@@ -4611,28 +4584,16 @@ XREMain::XRE_mainRun()
     mozilla::InitEventTracing(logToConsole);
   }
 #endif /* MOZ_INSTRUMENT_EVENT_LOOP */
 
 #if defined(MOZ_SANDBOX) && defined(XP_LINUX)
   // If we're on Linux, we now have information about the OS capabilities
   // available to us.
   SandboxInfo sandboxInfo = SandboxInfo::Get();
-  Telemetry::Accumulate(Telemetry::SANDBOX_HAS_SECCOMP_BPF,
-                        sandboxInfo.Test(SandboxInfo::kHasSeccompBPF));
-  Telemetry::Accumulate(Telemetry::SANDBOX_HAS_SECCOMP_TSYNC,
-                        sandboxInfo.Test(SandboxInfo::kHasSeccompTSync));
-  Telemetry::Accumulate(Telemetry::SANDBOX_HAS_USER_NAMESPACES_PRIVILEGED,
-                        sandboxInfo.Test(SandboxInfo::kHasPrivilegedUserNamespaces));
-  Telemetry::Accumulate(Telemetry::SANDBOX_HAS_USER_NAMESPACES,
-                        sandboxInfo.Test(SandboxInfo::kHasUserNamespaces));
-  Telemetry::Accumulate(Telemetry::SANDBOX_CONTENT_ENABLED,
-                        sandboxInfo.Test(SandboxInfo::kEnabledForContent));
-  Telemetry::Accumulate(Telemetry::SANDBOX_MEDIA_ENABLED,
-                        sandboxInfo.Test(SandboxInfo::kEnabledForMedia));
 #if defined(MOZ_CRASHREPORTER)
   nsAutoCString flagsString;
   flagsString.AppendInt(sandboxInfo.AsInteger());
 
   CrashReporter::AnnotateCrashReport(
     NS_LITERAL_CSTRING("ContentSandboxCapabilities"), flagsString);
 #endif /* MOZ_CRASHREPORTER */
 #endif /* MOZ_SANDBOX && XP_LINUX */
@@ -5166,21 +5127,16 @@ BrowserTabsRemoteAutostart()
       (Preferences::GetBool(kForceDisableE10sPref, false) ||
        EnvHasValue("MOZ_FORCE_DISABLE_E10S"))) {
     gBrowserTabsRemoteAutostart = false;
     status = kE10sForceDisabled;
   }
 
   gBrowserTabsRemoteStatus = status;
 
-  mozilla::Telemetry::Accumulate(mozilla::Telemetry::E10S_STATUS, status);
-  if (prefEnabled) {
-    mozilla::Telemetry::Accumulate(mozilla::Telemetry::E10S_BLOCKED_FROM_RUNNING,
-                                    !gBrowserTabsRemoteAutostart);
-  }
   return gBrowserTabsRemoteAutostart;
 }
 
 uint32_t
 GetMaxWebProcessCount()
 {
   // multiOptOut is in int to allow us to run multiple experiments without
   // introducing multiple prefs a la the autostart.N prefs.
diff --git a/toolkit/xre/nsXREDirProvider.cpp b/toolkit/xre/nsXREDirProvider.cpp
--- a/toolkit/xre/nsXREDirProvider.cpp
+++ b/toolkit/xre/nsXREDirProvider.cpp
@@ -36,17 +36,16 @@
 
 #include "SpecialSystemDirectory.h"
 
 #include "mozilla/dom/ScriptSettings.h"
 
 #include "mozilla/Services.h"
 #include "mozilla/Omnijar.h"
 #include "mozilla/Preferences.h"
-#include "mozilla/Telemetry.h"
 
 #include <stdlib.h>
 
 #ifdef XP_WIN
 #include "city.h"
 #include <windows.h>
 #include <shlobj.h>
 #endif
@@ -1039,46 +1038,16 @@ nsXREDirProvider::DoStartup()
     (void)NS_CreateServicesFromCategory("profile-after-change", nullptr,
                                         "profile-after-change");
 
     if (gSafeMode && safeModeNecessary) {
       static const char16_t kCrashed[] = {'c','r','a','s','h','e','d','\0'};
       obsSvc->NotifyObservers(nullptr, "safemode-forced", kCrashed);
     }
 
-    // 1 = Regular mode, 2 = Safe mode, 3 = Safe mode forced
-    int mode = 1;
-    if (gSafeMode) {
-      if (safeModeNecessary)
-        mode = 3;
-      else
-        mode = 2;
-    }
-    mozilla::Telemetry::Accumulate(mozilla::Telemetry::SAFE_MODE_USAGE, mode);
-
-    // Telemetry about number of profiles.
-    nsCOMPtr<nsIToolkitProfileService> profileService =
-      do_GetService("@mozilla.org/toolkit/profile-service;1");
-    if (profileService) {
-      nsCOMPtr<nsISimpleEnumerator> profiles;
-      rv = profileService->GetProfiles(getter_AddRefs(profiles));
-      if (NS_WARN_IF(NS_FAILED(rv))) {
-        return rv;
-      }
-
-      uint32_t count = 0;
-      nsCOMPtr<nsISupports> profile;
-      while (NS_SUCCEEDED(profiles->GetNext(getter_AddRefs(profile)))) {
-        ++count;
-      }
-
-      mozilla::Telemetry::Accumulate(mozilla::Telemetry::NUMBER_OF_PROFILES,
-                                     count);
-    }
-
     obsSvc->NotifyObservers(nullptr, "profile-initial-state", nullptr);
 
 #if (defined(XP_WIN) || defined(XP_MACOSX)) && defined(MOZ_CONTENT_SANDBOX)
     // Makes sure the content temp dir has been loaded if it hasn't been
     // already. In the parent this ensures it has been created before we attempt
     // to start any content processes.
     if (!mContentTempDir) {
       mozilla::Unused << NS_WARN_IF(NS_FAILED(LoadContentProcessTempDir()));
diff --git a/xpcom/build/nsXULAppAPI.h b/xpcom/build/nsXULAppAPI.h
--- a/xpcom/build/nsXULAppAPI.h
+++ b/xpcom/build/nsXULAppAPI.h
@@ -477,19 +477,16 @@ XRE_API(bool,
                                    void* aCallback))
 XRE_API(bool,
         XRE_ShutdownTestShell, ())
 
 XRE_API(void,
         XRE_InstallX11ErrorHandler, ())
 
 XRE_API(void,
-        XRE_TelemetryAccumulate, (int aID, uint32_t aSample))
-
-XRE_API(void,
         XRE_StartupTimelineRecord, (int aEvent, mozilla::TimeStamp aWhen))
 
 XRE_API(void,
         XRE_InitOmnijar, (nsIFile* aGreOmni,
                           nsIFile* aAppOmni))
 XRE_API(void,
         XRE_StopLateWriteChecks, (void))
 
diff --git a/xpcom/system/nsICrashReporter.idl b/xpcom/system/nsICrashReporter.idl
--- a/xpcom/system/nsICrashReporter.idl
+++ b/xpcom/system/nsICrashReporter.idl
@@ -144,16 +144,9 @@ interface nsICrashReporter : nsISupports
 
   /**
    * Save an anonymized memory report file for inclusion in a future crash
    * report in this session.
    *
    * @throws NS_ERROR_NOT_INITIALIZED if crash reporting is disabled.
    */
   void saveMemoryReport();
-
-  /**
-   * Set the telemetry session ID which is recorded in crash metadata. This is
-   * saved in the crash manager and telemetry but is not submitted as a
-   * crash-stats annotation.
-   */
-  void setTelemetrySessionId(in AUTF8String id);
 };
