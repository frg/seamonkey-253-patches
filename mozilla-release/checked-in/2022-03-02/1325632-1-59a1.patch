# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1506015602 18000
# Node ID 6ac9cbe03c9d1ad4317308f79734b9dd88886f92
# Parent  36207a27825f387137bb62dd7b4b542a1f22cde7
Bug 1325632 - part 1 - modify re-checking the compiler with flags; r=glandium

toolchain.configure checks for information about the compilers we're
using and accumulates additional flags that we might need to
pass (e.g. switches for C/C++ versions, proper compiler targets, etc.),
and then rechecks the compilers with those additional flags to verify
that those flags are sufficient to configure the compiler properly.
Only after we've checked for the proper flags do we move on to verifying
the compiler versions are sufficient.

It's possible, however, that the flags we try to add might not be
supported by the compiler being tested, in which case we'd get an
unfriendly error about an "unknown compiler", rather than something like
"version X of Y not supported".  In this case, we'd rather use the
information we gathered from the first run to provide information
messages.  So we modify the second check to ignore any thrown exceptions
during the check itself.

This change results in another problem: the check for whether we had any
extraneous flags is done prior to version checks for the compilers we
support, which is also unhelpful.  We choose to move this check after
the version checks themselves.

diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -759,21 +759,27 @@ def compiler(language, host_or_target, c
                     'Please reorder your $PATH.',
                     quote(os.path.dirname(found_compiler)),
                     quote(os.path.dirname(full_path)))
 
         info = check_compiler(wrapper + [compiler] + flags, language,
                               host_or_target)
 
         # Check that the additional flags we got are enough to not require any
-        # more flags.
-        if info.flags:
-            flags += info.flags
-            info = check_compiler(wrapper + [compiler] + flags, language,
-                                  host_or_target)
+        # more flags. If we get an exception, just ignore it; it's liable to be
+        # invalid command-line flags, which means the compiler we're checking
+        # doesn't support those command-line flags and will fail one or more of
+        # the checks below.
+        try:
+            if info.flags:
+                flags += info.flags
+                info = check_compiler(wrapper + [compiler] + flags, language,
+                                      host_or_target)
+        except FatalCheckError:
+            pass
 
         if not info.target_cpu or info.target_cpu != host_or_target.cpu:
             raise FatalCheckError(
                 '%s %s compiler target CPU (%s) does not match --%s CPU (%s)'
                 % (host_or_target_str.capitalize(), language,
                    info.target_cpu or 'unknown', host_or_target_str,
                    host_or_target.raw_cpu))
 
@@ -789,46 +795,46 @@ def compiler(language, host_or_target, c
                                           host_or_target.endianness):
             raise FatalCheckError(
                 '%s %s compiler target endianness (%s) does not match --%s '
                 'endianness (%s)'
                 % (host_or_target_str.capitalize(), language,
                    info.target_endianness or 'unknown', host_or_target_str,
                    host_or_target.endianness))
 
-        if info.flags:
-            raise FatalCheckError(
-                'Unknown compiler or compiler not supported.')
-
         # Compiler version checks
         # ===================================================
         # Check the compiler version here instead of in `compiler_version` so
         # that the `checking` message doesn't pretend the compiler can be used
         # to then bail out one line later.
         if info.type == 'gcc' and info.version < '4.9.0':
             raise FatalCheckError(
                 'Only GCC 4.9 or newer is supported (found version %s).'
                 % info.version)
 
         # If you want to bump the version check here search for
-        # __cpp_static_assert above, and see the associated comment.
+        # cxx_alignof above, and see the associated comment.
         if info.type == 'clang' and not info.version:
             raise FatalCheckError(
                 'Only clang/llvm 3.6 or newer is supported.')
 
         if info.type == 'msvc':
             if info.version < '19.00.24213':
                 raise FatalCheckError(
                     'This version (%s) of the MSVC compiler is not '
                     'supported.\n'
                     'You must install Visual C++ 2015 Update 3 or newer in '
                     'order to build.\n'
                     'See https://developer.mozilla.org/en/'
                     'Windows_Build_Prerequisites' % info.version)
 
+        if info.flags:
+            raise FatalCheckError(
+                'Unknown compiler or compiler not supported.')
+
         return namespace(
             wrapper=wrapper,
             compiler=compiler,
             flags=flags,
             type=info.type,
             version=info.version,
             language=language,
         )
