# HG changeset patch
# User Chih-Yi Leu <subsevenx2001@gmail.com>
# Date 1507080463 -28800
#      Wed Oct 04 09:27:43 2017 +0800
# Node ID 9f5ac1ae700103bc8f5aad0633504c1b34447e8a
# Parent  ed85185ff27e2236f528116011e552a59ac33bc9
Bug 1371190 - Part 8: Retry D3D11CreateDevice with feature level 11_0 if 11_1 is not supported. r=jgilbert

MozReview-Commit-ID: 1PGa6CuY7GZ

diff --git a/gfx/angle/src/libANGLE/renderer/d3d/d3d11/Renderer11.cpp b/gfx/angle/src/libANGLE/renderer/d3d/d3d11/Renderer11.cpp
--- a/gfx/angle/src/libANGLE/renderer/d3d/d3d11/Renderer11.cpp
+++ b/gfx/angle/src/libANGLE/renderer/d3d/d3d11/Renderer11.cpp
@@ -772,32 +772,55 @@ egl::Error Renderer11::initializeD3DDevi
         {
             TRACE_EVENT0("gpu.angle", "D3D11CreateDevice (Debug)");
             result = D3D11CreateDevice(nullptr, mRequestedDriverType, nullptr,
                                        D3D11_CREATE_DEVICE_DEBUG, mAvailableFeatureLevels.data(),
                                        static_cast<unsigned int>(mAvailableFeatureLevels.size()),
                                        D3D11_SDK_VERSION, &mDevice,
                                        &(mRenderer11DeviceCaps.featureLevel), &mDeviceContext);
 
+            if (result == E_INVALIDARG &&
+                mAvailableFeatureLevels[0] == D3D_FEATURE_LEVEL_11_1)
+            {
+                // In some older Windows platform, D3D11.1 is not supported which returns E_INVALIDARG
+                // so we omit the 11.1 feature level flag and try again
+                result = D3D11CreateDevice(nullptr, mRequestedDriverType, nullptr,
+                                           D3D11_CREATE_DEVICE_DEBUG, mAvailableFeatureLevels.data()+1,
+                                           static_cast<unsigned int>(mAvailableFeatureLevels.size())-1,
+                                           D3D11_SDK_VERSION, &mDevice,
+                                           &(mRenderer11DeviceCaps.featureLevel), &mDeviceContext);
+            }
+
             if (!mDevice || FAILED(result))
             {
                 WARN() << "Failed creating Debug D3D11 device - falling back to release runtime.";
             }
         }
 
         if (!mDevice || FAILED(result))
         {
             SCOPED_ANGLE_HISTOGRAM_TIMER("GPU.ANGLE.D3D11CreateDeviceMS");
             TRACE_EVENT0("gpu.angle", "D3D11CreateDevice");
 
             result = D3D11CreateDevice(
                 nullptr, mRequestedDriverType, nullptr, 0, mAvailableFeatureLevels.data(),
                 static_cast<unsigned int>(mAvailableFeatureLevels.size()), D3D11_SDK_VERSION,
                 &mDevice, &(mRenderer11DeviceCaps.featureLevel), &mDeviceContext);
 
+            if (result == E_INVALIDARG &&
+                mAvailableFeatureLevels[0] == D3D_FEATURE_LEVEL_11_1)
+            {
+                // In some older Windows platform, D3D11.1 is not supported which returns E_INVALIDARG
+                // so we omit the 11.1 feature level flag and try again
+                result = D3D11CreateDevice(
+                    nullptr, mRequestedDriverType, nullptr, 0, mAvailableFeatureLevels.data()+1,
+                    static_cast<unsigned int>(mAvailableFeatureLevels.size())-1, D3D11_SDK_VERSION,
+                    &mDevice, &(mRenderer11DeviceCaps.featureLevel), &mDeviceContext);
+            }
+
             // Cleanup done by destructor
             if (!mDevice || FAILED(result))
             {
                 ANGLE_HISTOGRAM_SPARSE_SLOWLY("GPU.ANGLE.D3D11CreateDeviceError",
                                               static_cast<int>(result));
                 return egl::EglNotInitialized(D3D11_INIT_CREATEDEVICE_ERROR)
                        << "Could not create D3D11 device.";
             }
