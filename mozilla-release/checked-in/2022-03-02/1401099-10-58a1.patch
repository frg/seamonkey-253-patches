# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1505466672 -32400
#      Fri Sep 15 18:11:12 2017 +0900
# Node ID b73ccc81201f5782935bea97d105786b675ac1d8
# Parent  879970683e882ad9a079b96fc412be0a02330744
Bug 1401099 - Move arena_run_trim_head to a method of arena_t. r=njn

diff --git a/memory/build/mozjemalloc.cpp b/memory/build/mozjemalloc.cpp
--- a/memory/build/mozjemalloc.cpp
+++ b/memory/build/mozjemalloc.cpp
@@ -787,16 +787,18 @@ private:
 
 public:
   arena_run_t* AllocRun(arena_bin_t* aBin, size_t aSize, bool aLarge, bool aZero);
 
   void DallocRun(arena_run_t* aRun, bool aDirty);
 
   void SplitRun(arena_run_t* aRun, size_t aSize, bool aLarge, bool aZero);
 
+  void TrimRunHead(arena_chunk_t* aChunk, arena_run_t* aRun, size_t aOldSize, size_t aNewSize);
+
   void Purge(bool aAll);
 
   void HardPurge();
 };
 
 /******************************************************************************/
 /*
  * Data.
@@ -3004,35 +3006,35 @@ arena_t::DallocRun(arena_run_t* aRun, bo
   }
 
   /* Enforce mMaxDirty. */
   if (mNumDirty > mMaxDirty) {
     Purge(false);
   }
 }
 
-static void
-arena_run_trim_head(arena_t *arena, arena_chunk_t *chunk, arena_run_t *run,
-    size_t oldsize, size_t newsize)
+void
+arena_t::TrimRunHead(arena_chunk_t* aChunk, arena_run_t* aRun, size_t aOldSize,
+                     size_t aNewSize)
 {
-	size_t pageind = ((uintptr_t)run - (uintptr_t)chunk) >> pagesize_2pow;
-	size_t head_npages = (oldsize - newsize) >> pagesize_2pow;
-
-	MOZ_ASSERT(oldsize > newsize);
-
-	/*
-	 * Update the chunk map so that arena_t::DallocRun() can treat the
-	 * leading run as separately allocated.
-	 */
-	chunk->map[pageind].bits = (oldsize - newsize) | CHUNK_MAP_LARGE |
-	    CHUNK_MAP_ALLOCATED;
-	chunk->map[pageind+head_npages].bits = newsize | CHUNK_MAP_LARGE |
-	    CHUNK_MAP_ALLOCATED;
-
-	arena->DallocRun(run, false);
+  size_t pageind = (uintptr_t(aRun) - uintptr_t(aChunk)) >> pagesize_2pow;
+  size_t head_npages = (aOldSize - aNewSize) >> pagesize_2pow;
+
+  MOZ_ASSERT(aOldSize > aNewSize);
+
+  /*
+   * Update the chunk map so that arena_t::RunDalloc() can treat the
+   * leading run as separately allocated.
+   */
+  aChunk->map[pageind].bits = (aOldSize - aNewSize) | CHUNK_MAP_LARGE |
+      CHUNK_MAP_ALLOCATED;
+  aChunk->map[pageind+head_npages].bits = aNewSize | CHUNK_MAP_LARGE |
+      CHUNK_MAP_ALLOCATED;
+
+  DallocRun(aRun, false);
 }
 
 static void
 arena_run_trim_tail(arena_t *arena, arena_chunk_t *chunk, arena_run_t *run,
     size_t oldsize, size_t newsize, bool dirty)
 {
 	size_t pageind = ((uintptr_t)run - (uintptr_t)chunk) >> pagesize_2pow;
 	size_t npages = newsize >> pagesize_2pow;
@@ -3361,17 +3363,17 @@ arena_palloc(arena_t *arena, size_t alig
 	MOZ_ASSERT(offset < alloc_size);
 	if (offset == 0)
 		arena_run_trim_tail(arena, chunk, (arena_run_t*)ret, alloc_size, size, false);
 	else {
 		size_t leadsize, trailsize;
 
 		leadsize = alignment - offset;
 		if (leadsize > 0) {
-			arena_run_trim_head(arena, chunk, (arena_run_t*)ret, alloc_size,
+			arena->TrimRunHead(chunk, (arena_run_t*)ret, alloc_size,
 			    alloc_size - leadsize);
 			ret = (void *)((uintptr_t)ret + leadsize);
 		}
 
 		trailsize = alloc_size - leadsize - size;
 		if (trailsize != 0) {
 			/* Trim trailing space. */
 			MOZ_ASSERT(trailsize < alloc_size);
