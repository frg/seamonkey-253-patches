# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1504079730 -32400
# Node ID a49266235c5aefff829bb5ee99a507fbd905540b
# Parent  7c9ff8c1c4af7d3afcf0b0f2378ad10f1b96ac75
Bug 1395070 - Replace MOZ_MEMORY_LINUX with XP_LINUX. r=njn

diff --git a/memory/mozjemalloc/mozjemalloc.cpp b/memory/mozjemalloc/mozjemalloc.cpp
--- a/memory/mozjemalloc/mozjemalloc.cpp
+++ b/memory/mozjemalloc/mozjemalloc.cpp
@@ -251,17 +251,17 @@ typedef long ssize_t;
  * On these systems, we prefer to directly use the system call.
  * We do that for Linux systems and kfreebsd with GNU userland.
  * Note sanity checks are not done (alignment of offset, ...) because
  * the uses of mmap are pretty limited, in jemalloc.
  *
  * On Alpha, glibc has a bug that prevents syscall() to work for system
  * calls with 6 arguments
  */
-#if (defined(MOZ_MEMORY_LINUX) && !defined(__alpha__)) || \
+#if (defined(XP_LINUX) && !defined(__alpha__)) || \
     (defined(MOZ_MEMORY_BSD) && defined(__GLIBC__))
 #include <sys/syscall.h>
 #if defined(SYS_mmap) || defined(SYS_mmap2)
 static inline
 void *_mmap(void *addr, size_t length, int prot, int flags,
             int fd, off_t offset)
 {
 /* S390 only passes one argument to the mmap system call, which is a
@@ -423,17 +423,17 @@ typedef pthread_mutex_t malloc_spinlock_
 
 /* Set to true once the allocator has been initialized. */
 static bool malloc_initialized = false;
 
 #if defined(XP_WIN)
 /* No init lock for Windows. */
 #elif defined(XP_DARWIN)
 static malloc_mutex_t init_lock = {OS_SPINLOCK_INIT};
-#elif defined(MOZ_MEMORY_LINUX) && !defined(MOZ_MEMORY_ANDROID)
+#elif defined(XP_LINUX) && !defined(MOZ_MEMORY_ANDROID)
 static malloc_mutex_t init_lock = PTHREAD_ADAPTIVE_MUTEX_INITIALIZER_NP;
 #else
 static malloc_mutex_t init_lock = PTHREAD_MUTEX_INITIALIZER;
 #endif
 
 /******************************************************************************/
 /*
  * Statistics data structures.
@@ -1079,17 +1079,17 @@ int pthread_atfork(void (*)(void), void 
 static bool
 malloc_mutex_init(malloc_mutex_t *mutex)
 {
 #if defined(XP_WIN)
 	if (!InitializeCriticalSectionAndSpinCount(mutex, _CRT_SPINCOUNT))
 		return (true);
 #elif defined(XP_DARWIN)
 	mutex->lock = OS_SPINLOCK_INIT;
-#elif defined(MOZ_MEMORY_LINUX) && !defined(MOZ_MEMORY_ANDROID)
+#elif defined(XP_LINUX) && !defined(MOZ_MEMORY_ANDROID)
 	pthread_mutexattr_t attr;
 	if (pthread_mutexattr_init(&attr) != 0)
 		return (true);
 	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ADAPTIVE_NP);
 	if (pthread_mutex_init(mutex, &attr) != 0) {
 		pthread_mutexattr_destroy(&attr);
 		return (true);
 	}
@@ -1133,17 +1133,17 @@ malloc_mutex_unlock(malloc_mutex_t *mute
 static bool
 malloc_spin_init(malloc_spinlock_t *lock)
 {
 #if defined(XP_WIN)
 	if (!InitializeCriticalSectionAndSpinCount(lock, _CRT_SPINCOUNT))
 			return (true);
 #elif defined(XP_DARWIN)
 	lock->lock = OS_SPINLOCK_INIT;
-#elif defined(MOZ_MEMORY_LINUX) && !defined(MOZ_MEMORY_ANDROID)
+#elif defined(XP_LINUX) && !defined(MOZ_MEMORY_ANDROID)
 	pthread_mutexattr_t attr;
 	if (pthread_mutexattr_init(&attr) != 0)
 		return (true);
 	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ADAPTIVE_NP);
 	if (pthread_mutex_init(lock, &attr) != 0) {
 		pthread_mutexattr_destroy(&attr);
 		return (true);
 	}
@@ -1890,17 +1890,17 @@ chunk_alloc_mmap(size_t size, size_t ali
  */
 static bool
 pages_purge(void *addr, size_t length, bool force_zero)
 {
 #ifdef MALLOC_DECOMMIT
 	pages_decommit(addr, length);
 	return true;
 #else
-#  ifndef MOZ_MEMORY_LINUX
+#  ifndef XP_LINUX
 	if (force_zero)
 		memset(addr, 0, length);
 #  endif
 #  ifdef XP_WIN
 	/*
 	* The region starting at addr may have been allocated in multiple calls
 	* to VirtualAlloc and recycled, so resetting the entire region in one
 	* go may not be valid. However, since we allocate at least a chunk at a
@@ -1911,17 +1911,17 @@ pages_purge(void *addr, size_t length, b
 	while (length > 0) {
 		VirtualAlloc(addr, pages_size, MEM_RESET, PAGE_READWRITE);
 		addr = (void *)((uintptr_t)addr + pages_size);
 		length -= pages_size;
 		pages_size = std::min(length, chunksize);
 	}
 	return force_zero;
 #  else
-#    ifdef MOZ_MEMORY_LINUX
+#    ifdef XP_LINUX
 #      define JEMALLOC_MADV_PURGE MADV_DONTNEED
 #      define JEMALLOC_MADV_ZEROS true
 #    else /* FreeBSD and Darwin. */
 #      define JEMALLOC_MADV_PURGE MADV_FREE
 #      define JEMALLOC_MADV_ZEROS force_zero
 #    endif
 	int err = madvise(addr, length, JEMALLOC_MADV_PURGE);
 	return JEMALLOC_MADV_ZEROS && err == 0;
