# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1507774124 -32400
# Node ID 4fe5de05cabab5de891c43eb08478535f62afd07
# Parent  3678c68d93f6248f23ef422d8cc6906800005179
Bug 1407838 - Remove the remains of NS_Alloc/NS_Realloc/NS_Free. r=njn

Bug 1134923 removed the use of those functions in gecko, and left some
for the XPCOM standalone glue. The XPCOM standalone glue was severely
stripped down in bug 1306327, with the effect of removing the
implementation for those functions.

The remains in nsXPCOM.h are:
XPCOM_API(void*) NS_Alloc(size_t aSize);
XPCOM_API(void*) NS_Realloc(void* aPtr, size_t aSize);
XPCOM_API(void) NS_Free(void* aPtr);

With no implementation left, the first arm is never actually used, and
the second arm means every remaining use of those functions in the tree
is a macro expansion to one of moz_xmalloc, moz_xrealloc or free.

diff --git a/devtools/shared/heapsnapshot/HeapSnapshot.h b/devtools/shared/heapsnapshot/HeapSnapshot.h
--- a/devtools/shared/heapsnapshot/HeapSnapshot.h
+++ b/devtools/shared/heapsnapshot/HeapSnapshot.h
@@ -28,17 +28,17 @@
 
 namespace mozilla {
 namespace devtools {
 
 class DominatorTree;
 
 struct NSFreePolicy {
   void operator()(void* ptr) {
-    NS_Free(ptr);
+    free(ptr);
   }
 };
 
 using UniqueTwoByteString = UniquePtr<char16_t[], NSFreePolicy>;
 using UniqueOneByteString = UniquePtr<char[], NSFreePolicy>;
 
 class HeapSnapshot final : public nsISupports
                          , public nsWrapperCache
diff --git a/dom/push/PushManager.cpp b/dom/push/PushManager.cpp
--- a/dom/push/PushManager.cpp
+++ b/dom/push/PushManager.cpp
@@ -78,17 +78,17 @@ public:
   explicit AutoFreeKeyBuffer(uint8_t** aKeyBuffer)
     : mKeyBuffer(aKeyBuffer)
   {
     MOZ_ASSERT(mKeyBuffer);
   }
 
   ~AutoFreeKeyBuffer()
   {
-    NS_Free(*mKeyBuffer);
+    free(*mKeyBuffer);
   }
 };
 
 // Copies a subscription key buffer into an array.
 nsresult
 CopySubscriptionKeyToArray(nsIPushSubscription* aSubscription,
                            const nsAString& aKeyName,
                            nsTArray<uint8_t>& aKey)
diff --git a/dom/push/PushNotifier.cpp b/dom/push/PushNotifier.cpp
--- a/dom/push/PushNotifier.cpp
+++ b/dom/push/PushNotifier.cpp
@@ -219,17 +219,17 @@ PushData::Binary(uint32_t* aDataLen, uin
   NS_ENSURE_ARG_POINTER(aData);
 
   *aData = nullptr;
   if (mData.IsEmpty()) {
     *aDataLen = 0;
     return NS_OK;
   }
   uint32_t length = mData.Length();
-  uint8_t* data = static_cast<uint8_t*>(NS_Alloc(length * sizeof(uint8_t)));
+  uint8_t* data = static_cast<uint8_t*>(moz_xmalloc(length * sizeof(uint8_t)));
   if (!data) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
   memcpy(data, mData.Elements(), length * sizeof(uint8_t));
   *aDataLen = length;
   *aData = data;
   return NS_OK;
 }
diff --git a/gfx/thebes/gfxFcPlatformFontList.cpp b/gfx/thebes/gfxFcPlatformFontList.cpp
--- a/gfx/thebes/gfxFcPlatformFontList.cpp
+++ b/gfx/thebes/gfxFcPlatformFontList.cpp
@@ -1502,22 +1502,22 @@ gfxFcPlatformFontList::MakePlatformFont(
                                         uint16_t aWeight,
                                         int16_t aStretch,
                                         uint8_t aStyle,
                                         const uint8_t* aFontData,
                                         uint32_t aLength)
 {
     FT_Face face = Factory::NewFTFaceFromData(nullptr, aFontData, aLength, 0);
     if (!face) {
-        NS_Free((void*)aFontData);
+        free((void*)aFontData);
         return nullptr;
     }
     if (FT_Err_Ok != FT_Select_Charmap(face, FT_ENCODING_UNICODE)) {
         Factory::ReleaseFTFace(face);
-        NS_Free((void*)aFontData);
+        free((void*)aFontData);
         return nullptr;
     }
 
     return new gfxFontconfigFontEntry(aFontName, aWeight, aStretch,
                                       aStyle, aFontData, face);
 }
 
 bool
diff --git a/gfx/thebes/gfxFcPlatformFontList.h b/gfx/thebes/gfxFcPlatformFontList.h
--- a/gfx/thebes/gfxFcPlatformFontList.h
+++ b/gfx/thebes/gfxFcPlatformFontList.h
@@ -55,17 +55,17 @@ public:
 
     const uint8_t *FontData() const { return mFontData; }
 
 private:
     ~FTUserFontData()
     {
         mozilla::gfx::Factory::ReleaseFTFace(mFace);
         if (mFontData) {
-            NS_Free((void*)mFontData);
+            free((void*)mFontData);
         }
     }
 
     FT_Face        mFace;
     const uint8_t *mFontData;
 };
 
 // The names for the font entry and font classes should really
diff --git a/xpcom/base/nsCRTGlue.cpp b/xpcom/base/nsCRTGlue.cpp
--- a/xpcom/base/nsCRTGlue.cpp
+++ b/xpcom/base/nsCRTGlue.cpp
@@ -126,32 +126,32 @@ NS_strdup(const char16_t* aString)
   uint32_t len = NS_strlen(aString);
   return NS_strndup(aString, len);
 }
 
 template<typename CharT>
 CharT*
 NS_strndup(const CharT* aString, uint32_t aLen)
 {
-  auto newBuf = (CharT*)NS_Alloc((aLen + 1) * sizeof(CharT));
+  auto newBuf = (CharT*)moz_xmalloc((aLen + 1) * sizeof(CharT));
   if (newBuf) {
     memcpy(newBuf, aString, aLen * sizeof(CharT));
     newBuf[aLen] = '\0';
   }
   return newBuf;
 }
 
 template char16_t* NS_strndup<char16_t>(const char16_t* aString, uint32_t aLen);
 template char* NS_strndup<char>(const char* aString, uint32_t aLen);
 
 char*
 NS_strdup(const char* aString)
 {
   uint32_t len = strlen(aString);
-  char* str = (char*)NS_Alloc(len + 1);
+  char* str = (char*)moz_xmalloc(len + 1);
   if (str) {
     memcpy(str, aString, len);
     str[len] = '\0';
   }
   return str;
 }
 
 // This table maps uppercase characters to lower case characters;
diff --git a/xpcom/base/nsCRTGlue.h b/xpcom/base/nsCRTGlue.h
--- a/xpcom/base/nsCRTGlue.h
+++ b/xpcom/base/nsCRTGlue.h
@@ -48,29 +48,29 @@ uint32_t NS_strlen(const char16_t* aStri
 int NS_strcmp(const char16_t* aStrA, const char16_t* aStrB);
 
 /**
  * "strncmp" for char16_t strings
  */
 int NS_strncmp(const char16_t* aStrA, const char16_t* aStrB, size_t aLen);
 
 /**
- * "strdup" for char16_t strings, uses the NS_Alloc allocator.
+ * "strdup" for char16_t strings, uses the moz_xmalloc allocator.
  */
 char16_t* NS_strdup(const char16_t* aString);
 
 /**
- * "strdup", but using the NS_Alloc allocator.
+ * "strdup", but using the moz_xmalloc allocator.
  */
 char* NS_strdup(const char* aString);
 
 /**
  * strndup for char16_t or char strings (normal strndup is not available on
  * windows). This function will ensure that the new string is
- * null-terminated. Uses the NS_Alloc allocator.
+ * null-terminated. Uses the moz_xmalloc allocator.
  *
  * CharT may be either char16_t or char.
  */
 template<typename CharT>
 CharT* NS_strndup(const CharT* aString, uint32_t aLen);
 
 // The following case-conversion methods only deal in the ascii repertoire
 // A-Z and a-z
diff --git a/xpcom/base/nsID.cpp b/xpcom/base/nsID.cpp
--- a/xpcom/base/nsID.cpp
+++ b/xpcom/base/nsID.cpp
@@ -94,24 +94,24 @@ nsID::Parse(const char* aIDStr)
 
 #ifndef XPCOM_GLUE_AVOID_NSPR
 
 static const char gIDFormat[] =
   "{%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x}";
 
 /*
  * Returns an allocated string in {xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}
- * format. The string is allocated with NS_Alloc and should be freed by
+ * format. The string is allocated with moz_xmalloc and should be freed by
  * the caller.
  */
 
 char*
 nsID::ToString() const
 {
-  char* res = (char*)NS_Alloc(NSID_LENGTH);
+  char* res = (char*)moz_xmalloc(NSID_LENGTH);
 
   if (res) {
     snprintf(res, NSID_LENGTH, gIDFormat,
              m0, (uint32_t)m1, (uint32_t)m2,
              (uint32_t)m3[0], (uint32_t)m3[1], (uint32_t)m3[2],
              (uint32_t)m3[3], (uint32_t)m3[4], (uint32_t)m3[5],
              (uint32_t)m3[6], (uint32_t)m3[7]);
   }
diff --git a/xpcom/base/nsMemory.cpp b/xpcom/base/nsMemory.cpp
--- a/xpcom/base/nsMemory.cpp
+++ b/xpcom/base/nsMemory.cpp
@@ -25,17 +25,17 @@ nsMemory::HeapMinimize(bool aImmediate)
   }
 
   return mem->HeapMinimize(aImmediate);
 }
 
 void*
 nsMemory::Clone(const void* aPtr, size_t aSize)
 {
-  void* newPtr = NS_Alloc(aSize);
+  void* newPtr = moz_xmalloc(aSize);
   if (newPtr) {
     memcpy(newPtr, aPtr, aSize);
   }
   return newPtr;
 }
 
 nsIMemory*
 nsMemory::GetGlobalMemoryService()
diff --git a/xpcom/base/nsMemory.h b/xpcom/base/nsMemory.h
--- a/xpcom/base/nsMemory.h
+++ b/xpcom/base/nsMemory.h
@@ -65,33 +65,33 @@ public:
  *                  types (including XPCOM strings and wstrings),
  *                  free should be used.
  */
 #define NS_FREE_XPCOM_POINTER_ARRAY(size, array, freeFunc)                    \
     do {                                                                      \
         int32_t iter_ = int32_t(size);                                        \
         while (--iter_ >= 0)                                                  \
             freeFunc((array)[iter_]);                                         \
-        NS_Free((array));                                                     \
+        free((array));                                                     \
     } while(0)
 
 // convenience macros for commonly used calls.  mmmmm.  syntactic sugar.
 
 /**
  * Macro to free arrays of non-refcounted objects allocated by the
  * shared allocator (nsMemory) such as strings and wstrings.  A
  * convenience wrapper around NS_FREE_XPCOM_POINTER_ARRAY.
  *
  * @param size      Number of elements in the array.  If not a constant, this
  *                  should be a int32_t.  Note that this means this macro
  *                  will not work if size >= 2^31.
  * @param array     The array to be freed.
  */
 #define NS_FREE_XPCOM_ALLOCATED_POINTER_ARRAY(size, array)                    \
-    NS_FREE_XPCOM_POINTER_ARRAY((size), (array), NS_Free)
+    NS_FREE_XPCOM_POINTER_ARRAY((size), (array), free)
 
 /**
  * Macro to free an array of pointers to nsISupports (or classes
  * derived from it).  A convenience wrapper around
  * NS_FREE_XPCOM_POINTER_ARRAY.
  *
  * Note that if you know that none of your nsISupports pointers are
  * going to be 0, you can gain a bit of speed by calling
diff --git a/xpcom/build/XREAppData.h b/xpcom/build/XREAppData.h
--- a/xpcom/build/XREAppData.h
+++ b/xpcom/build/XREAppData.h
@@ -48,17 +48,17 @@ public:
 
   XREAppData& operator=(const StaticXREAppData& aOther);
   XREAppData& operator=(const XREAppData& aOther);
   XREAppData& operator=(XREAppData&& aOther) = default;
 
   struct NSFreePolicy
   {
     void operator()(const void* ptr) {
-      NS_Free(const_cast<void*>(ptr));
+      free(const_cast<void*>(ptr));
     }
   };
 
   // Lots of code reads these fields directly like a struct, so rather
   // than using UniquePtr directly, use an auto-converting wrapper.
   class CharPtr
   {
   public:
diff --git a/xpcom/build/nsXPCOM.h b/xpcom/build/nsXPCOM.h
--- a/xpcom/build/nsXPCOM.h
+++ b/xpcom/build/nsXPCOM.h
@@ -171,64 +171,16 @@ XPCOM_API(nsresult) NS_NewNativeLocalFil
 class NS_ConvertUTF16toUTF8;
 nsresult NS_NewNativeLocalFile(const NS_ConvertUTF16toUTF8& aPath,
                                bool aFollowLinks,
                                nsIFile** aResult) = delete;
 
 #endif
 
 /**
- * Allocator functions for the standalone glue.
- * Do not use outside the xpcom glue code.
- * Use moz_xmalloc/moz_xrealloc/free, or new/delete instead.
- */
-#ifdef XPCOM_GLUE
-/**
- * Allocates a block of memory of a particular size. If the memory cannot
- * be allocated (because of an out-of-memory condition), the process aborts.
- *
- * @param aSize  The size of the block to allocate
- * @result       The block of memory
- * @note         This function is thread-safe.
- */
-XPCOM_API(void*) NS_Alloc(size_t aSize);
-
-/**
- * Reallocates a block of memory to a new size.
- *
- * @param aPtr    The block of memory to reallocate. This block must originally
-                  have been allocated by NS_Alloc or NS_Realloc
- * @param aSize   The new size. If 0, frees the block like NS_Free
- * @result        The reallocated block of memory
- * @note          This function is thread-safe.
- *
- * If aPtr is null, this function behaves like NS_Alloc.
- * If s is the size of the block to which aPtr points, the first min(s, size)
- * bytes of aPtr's block are copied to the new block. If the allocation
- * succeeds, aPtr is freed and a pointer to the new block is returned. If the
- * allocation fails, the process aborts.
- */
-XPCOM_API(void*) NS_Realloc(void* aPtr, size_t aSize);
-
-/**
- * Frees a block of memory. Null is a permissible value, in which case no
- * action is taken.
- *
- * @param aPtr  The block of memory to free. This block must originally have
- *              been allocated by NS_Alloc or NS_Realloc
- * @note        This function is thread-safe.
- */
-XPCOM_API(void) NS_Free(void* aPtr);
-#else
-#define NS_Alloc moz_xmalloc
-#define NS_Realloc moz_xrealloc
-#define NS_Free free
-#endif
-
-/**
  * Support for warnings, assertions, and debugging breaks.
  */
 
 enum
 {
   NS_DEBUG_WARNING = 0,
   NS_DEBUG_ASSERTION = 1,
   NS_DEBUG_BREAK = 2,
diff --git a/xpcom/ds/nsCOMArray.cpp b/xpcom/ds/nsCOMArray.cpp
--- a/xpcom/ds/nsCOMArray.cpp
+++ b/xpcom/ds/nsCOMArray.cpp
@@ -299,25 +299,25 @@ nsCOMArray_base::SetCount(int32_t aNewCo
 
 void
 nsCOMArray_base::Adopt(nsISupports** aElements, uint32_t aSize)
 {
   Clear();
   mArray.AppendElements(aElements, aSize);
 
   // Free the allocated array as well.
-  NS_Free(aElements);
+  free(aElements);
 }
 
 uint32_t
 nsCOMArray_base::Forget(nsISupports*** aElements)
 {
   uint32_t length = Length();
   size_t array_size = sizeof(nsISupports*) * length;
-  nsISupports** array = static_cast<nsISupports**>(NS_Alloc(array_size));
+  nsISupports** array = static_cast<nsISupports**>(moz_xmalloc(array_size));
   memmove(array, Elements(), array_size);
   *aElements = array;
   // Don't Release the contained pointers; the caller of the method will
   // do this eventually.
   mArray.Clear();
 
   return length;
 }
diff --git a/xpcom/ds/nsHashKeys.h b/xpcom/ds/nsHashKeys.h
--- a/xpcom/ds/nsHashKeys.h
+++ b/xpcom/ds/nsHashKeys.h
@@ -547,17 +547,17 @@ public:
     : mKey(aOther.mKey)
   {
     aOther.mKey = nullptr;
   }
 
   ~nsUnicharPtrHashKey()
   {
     if (mKey) {
-      NS_Free(const_cast<char16_t*>(mKey));
+      free(const_cast<char16_t*>(mKey));
     }
   }
 
   const char16_t* GetKey() const { return mKey; }
   bool KeyEquals(KeyTypePointer aKey) const { return !NS_strcmp(mKey, aKey); }
 
   static KeyTypePointer KeyToPointer(KeyType aKey) { return aKey; }
   static PLDHashNumber HashKey(KeyTypePointer aKey)
