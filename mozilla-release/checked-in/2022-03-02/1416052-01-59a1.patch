# HG changeset patch
# User Gregory Szorc <gps@mozilla.com>
# Date 1510271437 28800
#      Thu Nov 09 15:50:37 2017 -0800
# Node ID 13e44d942d707d7bf7a2de4ebed8da9f6759a347
# Parent  b0d4c34c46cc94f91010838c42bb1ce0fe20b74c
Bug 1416052 - Add a wrapper to invoke client.mk; r=nalexander

We're about to start moving logic from client.mk into Python.

The transition will require doing things like setting
environment variables when we invoke client.mk. Prepare for
this by factoring the "call client.mk" logic into a reusable
function.

The function will obviously be temporary until client.mk is
no more. I'm not sure what the end state will be yet. But surely
having equivalent code in Python is better than having it in
make.

MozReview-Commit-ID: F15DxXSV8V3

diff --git a/python/mozbuild/mozbuild/controller/building.py b/python/mozbuild/mozbuild/controller/building.py
--- a/python/mozbuild/mozbuild/controller/building.py
+++ b/python/mozbuild/mozbuild/controller/building.py
@@ -1123,20 +1123,20 @@ class BuildDriver(MozbuildObject):
                     active_backend = config.substs.get('BUILD_BACKENDS', [None])[0]
                     if active_backend:
                         backend_cls = get_backend_class(active_backend)(config)
                         status = backend_cls.build(self, output, jobs, verbose)
 
                 # If the backend doesn't specify a build() method, then just
                 # call client.mk directly.
                 if status is None:
-                    status = self._run_make(srcdir=True, filename='client.mk',
-                        line_handler=output.on_line, log=False, print_directory=False,
-                        allow_parallel=False, ensure_exit_code=False, num_jobs=jobs,
-                        silent=not verbose, keep_going=keep_going)
+                    status = self._run_client_mk(line_handler=output.on_line,
+                                                 jobs=jobs,
+                                                 verbose=verbose,
+                                                 keep_going=keep_going)
 
                 self.log(logging.WARNING, 'warning_summary',
                     {'count': len(monitor.warnings_database)},
                     '{count} compiler warnings present.')
 
             monitor.finish(record_usage=status == 0)
 
         # Print the collected compiler warnings. This is redundant with
@@ -1266,20 +1266,19 @@ class BuildDriver(MozbuildObject):
 
         options = ' '.join(shell_quote(o) for o in options or ())
         append_env = {b'CONFIGURE_ARGS': options.encode('utf-8')}
 
         # Only print build status messages when we have an active
         # monitor.
         if not buildstatus_messages:
             append_env[b'NO_BUILDSTATUS_MESSAGES'] =  b'1'
-        status = self._run_make(srcdir=True, filename='client.mk',
-            target='configure', line_handler=line_handler, log=False,
-            print_directory=False, allow_parallel=False, ensure_exit_code=False,
-            append_env=append_env)
+        status = self._run_client_mk(target='configure',
+                                     line_handler=line_handler,
+                                     append_env=append_env)
 
         if not status:
             print('Configure complete!')
             print('Be sure to run |mach build| to pick up any changes');
 
         return status
 
     def install_tests(self, test_objs):
@@ -1293,8 +1292,23 @@ class BuildDriver(MozbuildObject):
         if not test_objs:
             # If we don't actually have a list of tests to install we install
             # test and support files wholesale.
             self._run_make(target='install-test-files', pass_thru=True,
                            print_directory=False)
         else:
             install_test_files(mozpath.normpath(self.topsrcdir), self.topobjdir,
                                '_tests', test_objs)
+
+    def _run_client_mk(self, target=None, line_handler=None, jobs=0,
+                       verbose=None, keep_going=False, append_env=None):
+        return self._run_make(srcdir=True,
+                              filename='client.mk',
+                              allow_parallel=False,
+                              ensure_exit_code=False,
+                              print_directory=False,
+                              target=target,
+                              line_handler=line_handler,
+                              log=False,
+                              num_jobs=jobs,
+                              silent=not verbose,
+                              keep_going=keep_going,
+                              append_env=append_env)
