# HG changeset patch
# User Mason Chang <mchang@mozilla.com>
# Date 1501775432 25200
# Node ID fc1d502dc1eafd53d5aecec1feac663e25104c74
# Parent  0a1cf3b8dbeaf6b837304f087551f7c37107c621
Bug 1383916 Prep a DrawTarget to be drawn to on the paint thread. r=dvander

diff --git a/gfx/layers/PaintThread.cpp b/gfx/layers/PaintThread.cpp
--- a/gfx/layers/PaintThread.cpp
+++ b/gfx/layers/PaintThread.cpp
@@ -139,50 +139,76 @@ PaintThread::ShutdownOnPaintThread()
 PaintThread::IsOnPaintThread()
 {
   return sThreadId == PlatformThread::CurrentId();
 }
 
 void
 PaintThread::PaintContentsAsync(CompositorBridgeChild* aBridge,
                                 gfx::DrawTargetCapture* aCapture,
-                                gfx::DrawTarget* aTarget)
+                                CapturedPaintState* aState,
+                                PrepDrawTargetForPaintingCallback aCallback)
 {
   MOZ_ASSERT(IsOnPaintThread());
+  MOZ_ASSERT(aCapture);
+  MOZ_ASSERT(aState);
+
+  DrawTarget* target = aState->mTarget;
+
+  Matrix oldTransform = target->GetTransform();
+  target->SetTransform(aState->mTargetTransform);
+
+  if (!aCallback(aState)) {
+    return;
+  }
 
   // Draw all the things into the actual dest target.
-  aTarget->DrawCapturedDT(aCapture, Matrix());
+  target->DrawCapturedDT(aCapture, Matrix());
+  target->SetTransform(oldTransform);
+
+  // Textureclient forces a flush once we "end paint", so
+  // users of this texture expect all the drawing to be complete.
+  // Force a flush now.
+  // TODO: This might be a performance bottleneck because
+  // main thread painting only does one flush at the end of all paints
+  // whereas we force a flush after each draw target paint.
+  target->Flush();
 
   if (aBridge) {
     aBridge->NotifyFinishedAsyncPaint();
   }
 }
 
 void
 PaintThread::PaintContents(DrawTargetCapture* aCapture,
-                           DrawTarget* aTarget)
+                           CapturedPaintState* aState,
+                           PrepDrawTargetForPaintingCallback aCallback)
 {
   MOZ_ASSERT(NS_IsMainThread());
+  MOZ_ASSERT(aCapture);
+  MOZ_ASSERT(aState);
 
   // If painting asynchronously, we need to acquire the compositor bridge which
   // owns the underlying MessageChannel. Otherwise we leave it null and use
   // synchronous dispatch.
   RefPtr<CompositorBridgeChild> cbc;
   if (!gfxPrefs::LayersOMTPForceSync()) {
     cbc = CompositorBridgeChild::Get();
     cbc->NotifyBeginAsyncPaint();
   }
   RefPtr<DrawTargetCapture> capture(aCapture);
-  RefPtr<DrawTarget> target(aTarget);
+  RefPtr<CapturedPaintState> state(aState);
 
   RefPtr<PaintThread> self = this;
   RefPtr<Runnable> task = NS_NewRunnableFunction("PaintThread::PaintContents",
-    [self, cbc, capture, target]() -> void
+    [self, cbc, capture, state, aCallback]() -> void
   {
-    self->PaintContentsAsync(cbc, capture, target);
+    self->PaintContentsAsync(cbc, capture,
+                             state,
+                             aCallback);
   });
 
   if (cbc) {
     sThread->Dispatch(task.forget());
   } else {
     SyncRunnable::DispatchToThread(sThread, task);
   }
 }
diff --git a/gfx/layers/PaintThread.h b/gfx/layers/PaintThread.h
--- a/gfx/layers/PaintThread.h
+++ b/gfx/layers/PaintThread.h
@@ -3,40 +3,74 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef MOZILLA_LAYERS_PAINTTHREAD_H
 #define MOZILLA_LAYERS_PAINTTHREAD_H
 
 #include "base/platform_thread.h"
+#include "mozilla/RefPtr.h"
 #include "mozilla/StaticPtr.h"
 #include "mozilla/UniquePtr.h"
 #include "nsThreadUtils.h"
 
 namespace mozilla {
 namespace gfx {
 class DrawTarget;
 class DrawTargetCapture;
 };
 
 namespace layers {
 
+// Holds the key parts from a RotatedBuffer::PaintState
+// required to draw the captured paint state
+class CapturedPaintState {
+  NS_INLINE_DECL_THREADSAFE_REFCOUNTING(CapturedPaintState)
+public:
+  CapturedPaintState(nsIntRegion& aRegionToDraw,
+                     gfx::DrawTarget* aTarget,
+                     gfx::DrawTarget* aTargetOnWhite,
+                     gfx::Matrix aTargetTransform,
+                     SurfaceMode aSurfaceMode,
+                     gfxContentType aContentType)
+  : mRegionToDraw(aRegionToDraw)
+  , mTarget(aTarget)
+  , mTargetOnWhite(aTargetOnWhite)
+  , mTargetTransform(aTargetTransform)
+  , mSurfaceMode(aSurfaceMode)
+  , mContentType(aContentType)
+  {}
+
+  nsIntRegion mRegionToDraw;
+  RefPtr<gfx::DrawTarget> mTarget;
+  RefPtr<gfx::DrawTarget> mTargetOnWhite;
+  gfx::Matrix mTargetTransform;
+  SurfaceMode mSurfaceMode;
+  gfxContentType mContentType;
+
+protected:
+  virtual ~CapturedPaintState() {}
+};
+
+typedef bool (*PrepDrawTargetForPaintingCallback)(CapturedPaintState* aPaintState);
+
 class CompositorBridgeChild;
 
 class PaintThread final
 {
   friend void DestroyPaintThread(UniquePtr<PaintThread>&& aPaintThread);
 
 public:
   static void Start();
   static void Shutdown();
   static PaintThread* Get();
   void PaintContents(gfx::DrawTargetCapture* aCapture,
-                     gfx::DrawTarget* aTarget);
+                     CapturedPaintState* aState,
+                     PrepDrawTargetForPaintingCallback aCallback);
 
   // Sync Runnables need threads to be ref counted,
   // But this thread lives through the whole process.
   // We're only temporarily using sync runnables so
   // Override release/addref but don't do anything.
   void Release();
   void AddRef();
 
@@ -44,17 +78,18 @@ public:
   static bool IsOnPaintThread();
 
 private:
   bool Init();
   void ShutdownOnPaintThread();
   void InitOnPaintThread();
   void PaintContentsAsync(CompositorBridgeChild* aBridge,
                           gfx::DrawTargetCapture* aCapture,
-                          gfx::DrawTarget* aTarget);
+                          CapturedPaintState* aState,
+                          PrepDrawTargetForPaintingCallback aCallback);
 
   static StaticAutoPtr<PaintThread> sSingleton;
   static StaticRefPtr<nsIThread> sThread;
   static PlatformThreadId sThreadId;
 };
 
 } // namespace layers
 } // namespace mozilla
diff --git a/gfx/layers/RotatedBuffer.cpp b/gfx/layers/RotatedBuffer.cpp
--- a/gfx/layers/RotatedBuffer.cpp
+++ b/gfx/layers/RotatedBuffer.cpp
@@ -22,16 +22,17 @@
 #include "mozilla/gfx/Point.h"          // for Point, IntPoint
 #include "mozilla/gfx/Rect.h"           // for Rect, IntRect
 #include "mozilla/gfx/Types.h"          // for ExtendMode::ExtendMode::CLAMP, etc
 #include "mozilla/layers/ShadowLayers.h"  // for ShadowableLayer
 #include "mozilla/layers/TextureClient.h"  // for TextureClient
 #include "mozilla/gfx/Point.h"          // for IntSize
 #include "gfx2DGlue.h"
 #include "nsLayoutUtils.h"              // for invalidation debugging
+#include "PaintThread.h"
 
 namespace mozilla {
 
 using namespace gfx;
 
 namespace layers {
 
 IntRect
@@ -726,64 +727,116 @@ RotatedContentBuffer::BeginPaint(Painted
   result.mRegionToInvalidate.Or(result.mRegionToInvalidate, invalidate);
   result.mClip = DrawRegionClip::DRAW;
   result.mMode = mode;
 
   return result;
 }
 
 DrawTarget*
+RotatedContentBuffer::BorrowDrawTargetForRecording(PaintState& aPaintState,
+                                                   DrawIterator* aIter /* = nullptr */)
+{
+  if (aPaintState.mMode == SurfaceMode::SURFACE_NONE) {
+    return nullptr;
+  }
+
+  DrawTarget* result = BorrowDrawTargetForQuadrantUpdate(aPaintState.mRegionToDraw.GetBounds(),
+                                                         BUFFER_BOTH, aIter);
+  if (!result) {
+    return nullptr;
+  }
+
+  ExpandDrawRegion(aPaintState, aIter, result->GetBackendType());
+  return result;
+}
+
+/*static */ bool
+RotatedContentBuffer::PrepareDrawTargetForPainting(CapturedPaintState* aState)
+{
+  RefPtr<DrawTarget> target = aState->mTarget;
+  RefPtr<DrawTarget> whiteTarget = aState->mTargetOnWhite;
+
+  if (aState->mSurfaceMode == SurfaceMode::SURFACE_COMPONENT_ALPHA) {
+    if (!target || !target->IsValid() ||
+        !aState->mTargetOnWhite || !aState->mTargetOnWhite->IsValid()) {
+      // This can happen in release builds if allocating one of the two buffers
+      // failed. This in turn can happen if unreasonably large textures are
+      // requested.
+      return false;
+    }
+    for (auto iter = aState->mRegionToDraw.RectIter(); !iter.Done(); iter.Next()) {
+      const IntRect& rect = iter.Get();
+      target->FillRect(Rect(rect.x, rect.y, rect.width, rect.height),
+                            ColorPattern(Color(0.0, 0.0, 0.0, 1.0)));
+      whiteTarget->FillRect(Rect(rect.x, rect.y, rect.width, rect.height),
+                                 ColorPattern(Color(1.0, 1.0, 1.0, 1.0)));
+    }
+  } else if (aState->mContentType == gfxContentType::COLOR_ALPHA &&
+             target->IsValid()) {
+    // HaveBuffer() => we have an existing buffer that we must clear
+    for (auto iter = aState->mRegionToDraw.RectIter(); !iter.Done(); iter.Next()) {
+      const IntRect& rect = iter.Get();
+      target->ClearRect(Rect(rect.x, rect.y, rect.width, rect.height));
+    }
+  }
+
+  return true;
+}
+
+void
+RotatedContentBuffer::ExpandDrawRegion(PaintState& aPaintState,
+                                       DrawIterator* aIter,
+                                       BackendType aBackendType)
+{
+  nsIntRegion* drawPtr = &aPaintState.mRegionToDraw;
+  if (aIter) {
+    // The iterators draw region currently only contains the bounds of the region,
+    // this makes it the precise region.
+    aIter->mDrawRegion.And(aIter->mDrawRegion, aPaintState.mRegionToDraw);
+    drawPtr = &aIter->mDrawRegion;
+  }
+  if (aBackendType == BackendType::DIRECT2D ||
+      aBackendType == BackendType::DIRECT2D1_1) {
+    // Simplify the draw region to avoid hitting expensive drawing paths
+    // for complex regions.
+    drawPtr->SimplifyOutwardByArea(100 * 100);
+  }
+}
+
+DrawTarget*
 RotatedContentBuffer::BorrowDrawTargetForPainting(PaintState& aPaintState,
                                                   DrawIterator* aIter /* = nullptr */)
 {
   if (aPaintState.mMode == SurfaceMode::SURFACE_NONE) {
     return nullptr;
   }
 
   DrawTarget* result = BorrowDrawTargetForQuadrantUpdate(aPaintState.mRegionToDraw.GetBounds(),
                                                          BUFFER_BOTH, aIter);
   if (!result) {
     return nullptr;
   }
 
-  nsIntRegion* drawPtr = &aPaintState.mRegionToDraw;
-  if (aIter) {
-    // The iterators draw region currently only contains the bounds of the region,
-    // this makes it the precise region.
-    aIter->mDrawRegion.And(aIter->mDrawRegion, aPaintState.mRegionToDraw);
-    drawPtr = &aIter->mDrawRegion;
-  }
-  if (result->GetBackendType() == BackendType::DIRECT2D ||
-      result->GetBackendType() == BackendType::DIRECT2D1_1) {
-    // Simplify the draw region to avoid hitting expensive drawing paths
-    // for complex regions.
-    drawPtr->SimplifyOutwardByArea(100 * 100);
-  }
+  ExpandDrawRegion(aPaintState, aIter, result->GetBackendType());
+
+  nsIntRegion regionToDraw = aIter ? aIter->mDrawRegion
+                                   : aPaintState.mRegionToDraw;
 
-  if (aPaintState.mMode == SurfaceMode::SURFACE_COMPONENT_ALPHA) {
-    if (!mDTBuffer || !mDTBuffer->IsValid() ||
-        !mDTBufferOnWhite || !mDTBufferOnWhite->IsValid()) {
-      // This can happen in release builds if allocating one of the two buffers
-      // failed. This in turn can happen if unreasonably large textures are
-      // requested.
-      return nullptr;
-    }
-    for (auto iter = drawPtr->RectIter(); !iter.Done(); iter.Next()) {
-      const IntRect& rect = iter.Get();
-      mDTBuffer->FillRect(Rect(rect.x, rect.y, rect.width, rect.height),
-                          ColorPattern(Color(0.0, 0.0, 0.0, 1.0)));
-      mDTBufferOnWhite->FillRect(Rect(rect.x, rect.y, rect.width, rect.height),
-                                 ColorPattern(Color(1.0, 1.0, 1.0, 1.0)));
-    }
-  } else if (aPaintState.mContentType == gfxContentType::COLOR_ALPHA && HaveBuffer()) {
-    // HaveBuffer() => we have an existing buffer that we must clear
-    for (auto iter = drawPtr->RectIter(); !iter.Done(); iter.Next()) {
-      const IntRect& rect = iter.Get();
-      result->ClearRect(Rect(rect.x, rect.y, rect.width, rect.height));
-    }
+  // Can't stack allocate refcounted objects.
+  RefPtr<CapturedPaintState> capturedPaintState =
+    MakeAndAddRef<CapturedPaintState>(regionToDraw,
+                                      mDTBuffer,
+                                      mDTBufferOnWhite,
+                                      Matrix(),
+                                      aPaintState.mMode,
+                                      aPaintState.mContentType);
+
+  if (!RotatedContentBuffer::PrepareDrawTargetForPainting(capturedPaintState)) {
+    return nullptr;
   }
 
   return result;
 }
 
 already_AddRefed<SourceSurface>
 RotatedContentBuffer::GetSourceSurface(ContextSource aSource) const
 {
diff --git a/gfx/layers/RotatedBuffer.h b/gfx/layers/RotatedBuffer.h
--- a/gfx/layers/RotatedBuffer.h
+++ b/gfx/layers/RotatedBuffer.h
@@ -17,16 +17,20 @@
 #include "nsDebug.h"                    // for NS_RUNTIMEABORT
 #include "nsISupportsImpl.h"            // for MOZ_COUNT_CTOR, etc
 #include "nsRegion.h"                   // for nsIntRegion
 #include "LayersTypes.h"
 
 namespace mozilla {
 namespace layers {
 
+class CapturedPaintState;
+
+typedef bool (*PrepDrawTargetForPaintingCallback)(CapturedPaintState*);
+
 class TextureClient;
 class PaintedLayer;
 
 /**
  * This is a cairo/Thebes surface, but with a literal twist. Scrolling
  * causes the layer's visible region to move. We want to keep
  * reusing the same surface if the region size hasn't changed, but we don't
  * want to keep moving the contents of the surface around in memory. So
@@ -288,16 +292,24 @@ public:
    *
    * @param aPaintState Paint state data returned by a call to BeginPaint
    * @param aIter Paint state iterator. Only required if PAINT_CAN_DRAW_ROTATED
    * was specified to BeginPaint.
    */
   gfx::DrawTarget* BorrowDrawTargetForPainting(PaintState& aPaintState,
                                                DrawIterator* aIter = nullptr);
 
+  gfx::DrawTarget* BorrowDrawTargetForRecording(PaintState& aPaintState,
+                                                DrawIterator* aIter = nullptr);
+
+  void ExpandDrawRegion(PaintState& aPaintState,
+                        DrawIterator* aIter,
+                        gfx::BackendType aBackendType);
+
+  static bool PrepareDrawTargetForPainting(CapturedPaintState*);
   enum {
     BUFFER_COMPONENT_ALPHA = 0x02 // Dual buffers should be created for drawing with
                                   // component alpha.
   };
   /**
    * Return a new surface of |aSize| and |aType|.
    *
    * If the created buffer supports azure content, then the result(s) will
diff --git a/gfx/layers/client/ClientPaintedLayer.cpp b/gfx/layers/client/ClientPaintedLayer.cpp
--- a/gfx/layers/client/ClientPaintedLayer.cpp
+++ b/gfx/layers/client/ClientPaintedLayer.cpp
@@ -20,16 +20,17 @@
 #include "mozilla/gfx/Types.h"          // for Float, etc
 #include "mozilla/layers/LayersTypes.h"
 #include "mozilla/Preferences.h"
 #include "nsCOMPtr.h"                   // for already_AddRefed
 #include "nsISupportsImpl.h"            // for Layer::AddRef, etc
 #include "nsRect.h"                     // for mozilla::gfx::IntRect
 #include "PaintThread.h"
 #include "ReadbackProcessor.h"
+#include "RotatedBuffer.h"
 
 namespace mozilla {
 namespace layers {
 
 using namespace mozilla::gfx;
 
 bool
 ClientPaintedLayer::EnsureContentClient()
@@ -183,68 +184,104 @@ ClientPaintedLayer::PaintThebes(nsTArray
 
   mContentClient->EndPaint(aReadbackUpdates);
 
   if (didUpdate) {
     UpdateContentClient(state);
   }
 }
 
+/***
+ * If we can, let's paint this ClientPaintedLayer's contents off the main thread.
+ * The essential idea is that we ask the ContentClient for a DrawTarget and record
+ * the moz2d commands. On the Paint Thread, we replay those commands to the
+ * destination draw target. There are a couple of lifetime issues here though:
+ *
+ * 1) TextureClient owns the underlying buffer and DrawTarget. Because of this
+ *    we have to keep the TextureClient and DrawTarget alive but trick the
+ *    TextureClient into thinking it's already returned the DrawTarget
+ *    since we iterate through different Rects to get DrawTargets*. If
+ *    the TextureClient goes away, the DrawTarget and thus buffer can too.
+ * 2) When ContentClient::EndPaint happens, it flushes the DrawTarget. We have
+ *    to Reflush on the Paint Thread
+ * 3) DrawTarget API is NOT thread safe. We get around this by recording
+ *    on the main thread and painting on the paint thread. Logically,
+ *    ClientLayerManager will force a flushed paint and block the main thread
+ *    if we have another transaction. Thus we have a gap between when the main
+ *    thread records, the paint thread paints, and we block the main thread
+ *    from trying to paint again. The underlying API however is NOT thread safe.
+ *  4) We have both "sync" and "async" OMTP. Sync OMTP means we paint on the main thread
+ *     but block the main thread while the paint thread paints. Async OMTP doesn't block
+ *     the main thread. Sync OMTP is only meant to be used as a debugging tool.
+ */
 bool
 ClientPaintedLayer::PaintOffMainThread()
 {
   mContentClient->BeginAsyncPaint();
 
   uint32_t flags = GetPaintFlags();
 
   PaintState state = mContentClient->BeginPaintBuffer(this, flags);
   if (!UpdatePaintRegion(state)) {
     return false;
   }
 
   bool didUpdate = false;
   RotatedContentBuffer::DrawIterator iter;
-  while (DrawTarget* target = mContentClient->BorrowDrawTargetForPainting(state, &iter)) {
+  // Debug Protip: Change to BorrowDrawTargetForPainting if using sync OMTP.
+  while (DrawTarget* target = mContentClient->BorrowDrawTargetForRecording(state, &iter)) {
     if (!target || !target->IsValid()) {
       if (target) {
         mContentClient->ReturnDrawTargetToBuffer(target);
       }
       continue;
     }
 
-    // We don't clear the rect here like WRPaintedBlobLayers do
-    // because ContentClient already clears the surface for us during BeginPaint.
     RefPtr<DrawTargetCapture> captureDT =
       Factory::CreateCaptureDrawTarget(target->GetBackendType(),
                                        target->GetSize(),
                                        target->GetFormat());
-    captureDT->SetTransform(target->GetTransform());
 
+    Matrix capturedTransform = target->GetTransform();
+    captureDT->SetTransform(capturedTransform);
+
+    // TODO: Capture AA Flags and reset them in PaintThread
     SetAntialiasingFlags(this, captureDT);
     SetAntialiasingFlags(this, target);
 
     RefPtr<gfxContext> ctx = gfxContext::CreatePreservingTransformOrNull(captureDT);
     MOZ_ASSERT(ctx); // already checked the target above
 
     ClientManager()->GetPaintedLayerCallback()(this,
                                               ctx,
                                               iter.mDrawRegion,
                                               iter.mDrawRegion,
                                               state.mClip,
                                               state.mRegionToInvalidate,
                                               ClientManager()->GetPaintedLayerCallbackData());
 
     ctx = nullptr;
 
-    PaintThread::Get()->PaintContents(captureDT, target);
+    // TODO: Fixup component alpha
+    DrawTarget* targetOnWhite = nullptr;
+    RefPtr<CapturedPaintState> capturedState
+      = MakeAndAddRef<CapturedPaintState>(state.mRegionToDraw,
+                                          target, targetOnWhite,
+                                          capturedTransform,
+                                          state.mMode,
+                                          state.mContentType);
+
+    PaintThread::Get()->PaintContents(captureDT,
+                                      capturedState,
+                                      RotatedContentBuffer::PrepareDrawTargetForPainting);
 
     mContentClient->ReturnDrawTargetToBuffer(target);
+
     didUpdate = true;
   }
-
   mContentClient->EndPaint(nullptr);
 
   if (didUpdate) {
     UpdateContentClient(state);
   }
   return true;
 }
 
diff --git a/gfx/layers/client/ContentClient.h b/gfx/layers/client/ContentClient.h
--- a/gfx/layers/client/ContentClient.h
+++ b/gfx/layers/client/ContentClient.h
@@ -91,17 +91,18 @@ public:
   virtual void PrintInfo(std::stringstream& aStream, const char* aPrefix);
 
   virtual void Clear() = 0;
   virtual RotatedContentBuffer::PaintState BeginPaintBuffer(PaintedLayer* aLayer,
                                                             uint32_t aFlags) = 0;
   virtual gfx::DrawTarget* BorrowDrawTargetForPainting(RotatedContentBuffer::PaintState& aPaintState,
                                                        RotatedContentBuffer::DrawIterator* aIter = nullptr) = 0;
   virtual void ReturnDrawTargetToBuffer(gfx::DrawTarget*& aReturned) = 0;
-
+  virtual gfx::DrawTarget* BorrowDrawTargetForRecording(RotatedContentBuffer::PaintState& aPaintState,
+                                                        RotatedContentBuffer::DrawIterator* aIter = nullptr) = 0;
   // Called as part of the layers transation reply. Conveys data about our
   // buffer(s) from the compositor. If appropriate we should swap references
   // to our buffers.
   virtual void SwapBuffers(const nsIntRegion& aFrontUpdatedRegion) {}
 
   // call before and after painting into this content client
   virtual void BeginPaint() {}
   virtual void BeginAsyncPaint();
@@ -146,16 +147,21 @@ public:
   {
     return RotatedContentBuffer::BeginPaint(aLayer, aFlags);
   }
   virtual gfx::DrawTarget* BorrowDrawTargetForPainting(PaintState& aPaintState,
                                                        RotatedContentBuffer::DrawIterator* aIter = nullptr) override
   {
     return RotatedContentBuffer::BorrowDrawTargetForPainting(aPaintState, aIter);
   }
+  virtual gfx::DrawTarget* BorrowDrawTargetForRecording(PaintState& aPaintState,
+                                                       RotatedContentBuffer::DrawIterator* aIter = nullptr) override
+  {
+    return RotatedContentBuffer::BorrowDrawTargetForRecording(aPaintState, aIter);
+  }
   virtual void ReturnDrawTargetToBuffer(gfx::DrawTarget*& aReturned) override
   {
     BorrowDrawTarget::ReturnDrawTarget(aReturned);
   }
 
   void DrawTo(PaintedLayer* aLayer,
               gfx::DrawTarget* aTarget,
               float aOpacity,
@@ -229,16 +235,21 @@ public:
   {
     return RotatedContentBuffer::BeginPaint(aLayer, aFlags);
   }
   virtual gfx::DrawTarget* BorrowDrawTargetForPainting(PaintState& aPaintState,
                                                        RotatedContentBuffer::DrawIterator* aIter = nullptr) override
   {
     return RotatedContentBuffer::BorrowDrawTargetForPainting(aPaintState, aIter);
   }
+  virtual gfx::DrawTarget* BorrowDrawTargetForRecording(PaintState& aPaintState,
+                                                        RotatedContentBuffer::DrawIterator* aIter = nullptr) override
+  {
+    return RotatedContentBuffer::BorrowDrawTargetForRecording(aPaintState, aIter);
+  }
   virtual void ReturnDrawTargetToBuffer(gfx::DrawTarget*& aReturned) override
   {
     BorrowDrawTarget::ReturnDrawTarget(aReturned);
   }
 
   /**
    * Begin/End Paint map a gfxASurface from the texture client
    * into the buffer of RotatedBuffer. The surface is only
diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -5849,8 +5849,11 @@ pref("dom.xhr.lowercase_header.enabled",
 #ifdef RELEASE_OR_BETA
 pref("toolkit.crashreporter.include_context_heap", false);
 #else
 pref("toolkit.crashreporter.include_context_heap", true);
 #endif
 
 // Open noopener links in a new process
 pref("dom.noopener.newprocess.enabled", true);
+
+pref("layers.omtp.enabled", false);
+pref("layers.omtp.force-sync", false);
\ No newline at end of file
