# HG changeset patch
# User Gregory Szorc <gps@mozilla.com>
# Date 1510096106 28800
# Node ID 8c8777aae67f5f6eba7686d73bc195e155c017f6
# Parent  818df7ceccbf42fdebb7ab05853d1f7351430832
Bug 1412932 - Remove realbuild target from client.mk; r=ted

Before, the default "build" target either invoked "realbuild" or
"profiledbuild" depending on whether MOZ_PGO was set. Now that
PGO is handled by Makefile.in, we only have "realbuild" and there
is no need to have a recursive invocation of client.mk. So we
merge "realbuild" into "build."

MozReview-Commit-ID: IIX8iAXlsD1

diff --git a/client.mk b/client.mk
--- a/client.mk
+++ b/client.mk
@@ -134,17 +134,16 @@ CONFIGURES += $(TOPSRCDIR)/js/src/config
 # Make targets that are going to be passed to the real build system
 OBJDIR_TARGETS = install export libs clean realclean distclean upload sdk installer package package-compare stage-package source-package l10n-check automation/build
 
 #######################################################################
 # Rules
 
 # The default rule is build
 build::
-	$(MAKE) -f $(TOPSRCDIR)/client.mk realbuild CREATE_MOZCONFIG_JSON=
 
 # Include baseconfig.mk for its $(MAKE) validation.
 include $(TOPSRCDIR)/config/baseconfig.mk
 
 # Define mkdir
 include $(TOPSRCDIR)/config/makefiles/makeutils.mk
 include $(TOPSRCDIR)/config/makefiles/autotargets.mk
 
@@ -155,34 +154,34 @@ MOZCONFIG_MK_LINES := $(filter export||%
 	$(if $(MOZCONFIG_MK_LINES),( $(foreach line,$(MOZCONFIG_MK_LINES), echo '$(subst ||, ,$(line))';) )) > $@
 
 # Include that makefile so that it is created. This should not actually change
 # the environment since MOZCONFIG_CONTENT, which MOZCONFIG_OUT_LINES derives
 # from, has already been eval'ed.
 include $(OBJDIR)/.mozconfig.mk
 
 # Print out any options loaded from mozconfig.
-all realbuild clean distclean export libs install realclean::
+all build clean distclean export libs install realclean::
 ifneq (,$(strip $(MOZCONFIG_OUT_FILTERED)))
 	$(info Adding client.mk options from $(FOUND_MOZCONFIG):)
 	$(foreach line,$(MOZCONFIG_OUT_FILTERED),$(info $(NULL) $(NULL) $(NULL) $(NULL) $(subst ||, ,$(line))))
 endif
 
 # Windows equivalents
 build_all: build
 clobber clobber_all: clean
 
 # helper target for mobile
 build_and_deploy: build package install
 
 #####################################################
 # Preflight, before building any project
 
 ifdef MOZ_PREFLIGHT_ALL
-realbuild preflight_all::
+build preflight_all::
 	set -e; \
 	for mkfile in $(MOZ_PREFLIGHT_ALL); do \
 	  $(MAKE) -f $(TOPSRCDIR)/$$mkfile preflight_all TOPSRCDIR=$(TOPSRCDIR) OBJDIR=$(OBJDIR) MOZ_OBJDIR=$(MOZ_OBJDIR); \
 	done
 endif
 
 ####################################
 # Configure
@@ -285,52 +284,51 @@ endif
 ifneq (,$(CONFIG_STATUS))
 $(OBJDIR)/config/autoconf.mk: $(TOPSRCDIR)/config/autoconf.mk.in
 	$(PYTHON) $(OBJDIR)/config.status -n --file=$(OBJDIR)/config/autoconf.mk
 endif
 
 ####################################
 # Build it
 
-realbuild::  $(OBJDIR)/Makefile $(OBJDIR)/config.status
+build::  $(OBJDIR)/Makefile $(OBJDIR)/config.status
 	+$(MOZ_MAKE) $(if $(MOZ_PGO),profiledbuild)
 
 ####################################
 # Other targets
 
 # Pass these target onto the real build system
 $(OBJDIR_TARGETS):: $(OBJDIR)/Makefile $(OBJDIR)/config.status
 	+$(MOZ_MAKE) $@
 
 ####################################
 # Postflight, after building all projects
 
 ifdef MOZ_AUTOMATION
-realbuild::
+build::
 	$(MAKE) -f $(TOPSRCDIR)/client.mk automation/build
 endif
 
 ifdef MOZ_POSTFLIGHT_ALL
-realbuild postflight_all::
+build postflight_all::
 	set -e; \
 	for mkfile in $(MOZ_POSTFLIGHT_ALL); do \
 	  $(MAKE) -f $(TOPSRCDIR)/$$mkfile postflight_all TOPSRCDIR=$(TOPSRCDIR) OBJDIR=$(OBJDIR) MOZ_OBJDIR=$(MOZ_OBJDIR); \
 	done
 endif
 
 echo-variable-%:
 	@echo $($*)
 
 # This makefile doesn't support parallel execution. It does pass
 # MOZ_MAKE_FLAGS to sub-make processes, so they will correctly execute
 # in parallel.
 .NOTPARALLEL:
 
 .PHONY: \
-    realbuild \
     build \
     build_all \
     clobber \
     clobber_all \
     configure \
     preflight_all \
     postflight_all \
     $(OBJDIR_TARGETS)

