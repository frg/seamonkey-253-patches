# HG changeset patch
# User Stephen A Pohl <spohl.mozilla.bugs@gmail.com>
# Date 1505837825 14400
# Node ID e4ac2e4268c75afeb6c3428bf0e7957287f2bd9f
# Parent  6b1f5a72c2e28dc3a42d6d0f4708e86dfca912db
Bug 1398582: Prevent drawing titles in title bars on macOS 10.13 when we don't want them. r=mstange

diff --git a/widget/cocoa/nsCocoaWindow.mm b/widget/cocoa/nsCocoaWindow.mm
--- a/widget/cocoa/nsCocoaWindow.mm
+++ b/widget/cocoa/nsCocoaWindow.mm
@@ -77,16 +77,30 @@ enum NSWindowOcclusionState {
 };
 
 @interface NSWindow(OcclusionState)
 - (NSWindowOcclusionState) occlusionState;
 @end
 
 #endif
 
+#if !defined(MAC_OS_X_VERSION_10_10) || \
+    MAC_OS_X_VERSION_MAX_ALLOWED < MAC_OS_X_VERSION_10_10
+
+enum NSWindowTitleVisibility {
+  NSWindowTitleVisible = 0,
+  NSWindowTitleHidden  = 1
+};
+
+@interface NSWindow(TitleVisibility)
+- (void)setTitleVisibility:(NSWindowTitleVisibility)visibility;
+@end
+
+#endif
+
 #if !defined(MAC_OS_X_VERSION_10_12) || \
     MAC_OS_X_VERSION_MAX_ALLOWED < MAC_OS_X_VERSION_10_12
 
 @interface NSWindow(AutomaticWindowTabbing)
 + (void)setAllowsAutomaticWindowTabbing:(BOOL)allow;
 @end
 
 #endif
@@ -467,16 +481,21 @@ nsresult nsCocoaWindow::CreateNativeWind
   // BorderlessWindow class.
   else if (features == NSBorderlessWindowMask)
     windowClass = [BorderlessWindow class];
 
   // Create the window
   mWindow = [[windowClass alloc] initWithContentRect:contentRect styleMask:features 
                                  backing:NSBackingStoreBuffered defer:YES];
 
+  if ([mWindow respondsToSelector:@selector(setTitleVisibility)]) {
+    // By default, hide window titles.
+    [mWindow setTitleVisibility:NSWindowTitleHidden];
+  }
+
   // setup our notification delegate. Note that setDelegate: does NOT retain.
   mDelegate = [[WindowDelegate alloc] initWithGeckoWindow:this];
   [mWindow setDelegate:mDelegate];
 
   // Make sure that the content rect we gave has been honored.
   NSRect wantedFrame = [mWindow frameRectForContentRect:contentRect];
   if (!NSEqualRects([mWindow frame], wantedFrame)) {
     // This can happen when the window is not on the primary screen.
@@ -1825,31 +1844,25 @@ nsCocoaWindow::SetCursor(imgIContainer* 
   return NS_OK;
 }
 
 nsresult
 nsCocoaWindow::SetTitle(const nsAString& aTitle)
 {
   NS_OBJC_BEGIN_TRY_ABORT_BLOCK_NSRESULT;
 
-  if (!mWindow)
+  if (!mWindow) {
     return NS_OK;
+  }
 
   const nsString& strTitle = PromiseFlatString(aTitle);
-  NSString* title = [NSString stringWithCharacters:reinterpret_cast<const unichar*>(strTitle.get())
+  const unichar* uniTitle = reinterpret_cast<const unichar*>(strTitle.get());
+  NSString* title = [NSString stringWithCharacters:uniTitle
                                             length:strTitle.Length()];
-
-  if ([mWindow drawsContentsIntoWindowFrame] && ![mWindow wantsTitleDrawn]) {
-    // Don't cause invalidations.
-    [mWindow disableSetNeedsDisplay];
-    [mWindow setTitle:title];
-    [mWindow enableSetNeedsDisplay];
-  } else {
-    [mWindow setTitle:title];
-  }
+  [mWindow setTitle:title];
 
   return NS_OK;
 
   NS_OBJC_END_TRY_ABORT_BLOCK_NSRESULT;
 }
 
 void
 nsCocoaWindow::Invalidate(const LayoutDeviceIntRect& aRect)
@@ -3135,16 +3148,20 @@ static const NSString* kStateCollectionB
 
 - (void)setDrawsContentsIntoWindowFrame:(BOOL)aState
 {
   bool changed = (aState != mDrawsIntoWindowFrame);
   mDrawsIntoWindowFrame = aState;
   if (changed) {
     [self updateContentViewSize];
     [self reflowTitlebarElements];
+    if ([self respondsToSelector:@selector(setTitleVisibility)]) {
+      [self setTitleVisibility:mDrawsIntoWindowFrame ? NSWindowTitleHidden :
+                                                       NSWindowTitleVisible];
+    }
   }
 }
 
 - (BOOL)drawsContentsIntoWindowFrame
 {
   return mDrawsIntoWindowFrame;
 }
 
