# HG changeset patch
# User Gerald Squelart <gsquelart@mozilla.com>
# Date 1507765503 -39600
# Node ID 5a46043493c82f561b03b2aecb69160b9ce7a2df
# Parent  c9b5ab89d7fea5646c97e24e9a0dd3abee92be72
Bug 1407810 - Make DecoderDoctorLogger::sLogState 'ReleaseAcquire' - r=jwwang

sLogState is accessed at least once for every DDLog call, even when logging is
disabled, so we want it to be as quick as possible for minimal impact on most
users.

MozReview-Commit-ID: AMstgXmixrv

diff --git a/dom/media/doctor/DecoderDoctorLogger.cpp b/dom/media/doctor/DecoderDoctorLogger.cpp
--- a/dom/media/doctor/DecoderDoctorLogger.cpp
+++ b/dom/media/doctor/DecoderDoctorLogger.cpp
@@ -9,17 +9,17 @@
 #include "DDLogUtils.h"
 #include "DDMediaLogs.h"
 #include "mozilla/ClearOnShutdown.h"
 #include "mozilla/SystemGroup.h"
 #include "mozilla/Unused.h"
 
 namespace mozilla {
 
-/* static */ Atomic<DecoderDoctorLogger::LogState>
+/* static */ Atomic<DecoderDoctorLogger::LogState, ReleaseAcquire>
   DecoderDoctorLogger::sLogState{ DecoderDoctorLogger::scDisabled };
 
 /* static */ const char* DecoderDoctorLogger::sShutdownReason = nullptr;
 
 static DDMediaLogs* sMediaLogs;
 
 // First DDLogShutdowner sets sLogState to scShutdown, to prevent further
 // logging.
diff --git a/dom/media/doctor/DecoderDoctorLogger.h b/dom/media/doctor/DecoderDoctorLogger.h
--- a/dom/media/doctor/DecoderDoctorLogger.h
+++ b/dom/media/doctor/DecoderDoctorLogger.h
@@ -368,17 +368,19 @@ private:
   // Currently enabled (logging happens), may be shutdown.
   static constexpr LogState scEnabled = 1;
   // Still disabled, but one thread is working on enabling it, nobody else
   // should interfere during this time.
   static constexpr LogState scEnabling = 2;
   // Shutdown, cannot be re-enabled.
   static constexpr LogState scShutdown = 3;
   // Current state.
-  static Atomic<LogState> sLogState;
+  // "ReleaseAcquire" because when changing to scEnabled, the just-created
+  // sMediaLogs must be accessible to consumers that see scEnabled.
+  static Atomic<LogState, ReleaseAcquire> sLogState;
 
   // If non-null, reason for an abnormal shutdown.
   static const char* sShutdownReason;
 };
 
 // Base class to automatically record a class lifetime. Usage:
 //   class SomeClass : public DecoderDoctorLifeLogger<SomeClass>
 //   {
