# HG changeset patch
# User Michael Froman <mfroman@mozilla.com>
# Date 1512580982 21600
# Node ID af9b67f94aa9d34f0d1e2c8eda087aa468685022
# Parent  17c5803bf8332dde443c68d21b65f2c43e54ffff
Bug 1414169 - pt 8 - Refactor creating html elements for conciseness. r=ng

MozReview-Commit-ID: 5naesMTOmtI

diff --git a/toolkit/content/aboutwebrtc/aboutWebrtc.js b/toolkit/content/aboutwebrtc/aboutWebrtc.js
--- a/toolkit/content/aboutwebrtc/aboutWebrtc.js
+++ b/toolkit/content/aboutwebrtc/aboutWebrtc.js
@@ -72,28 +72,23 @@ function onClearStats() {
   WebrtcGlobalInformation.clearAllStats();
   getStats()
     .then(stats => AboutWebRTC.refresh({reports: stats.reports}))
     .catch(error => AboutWebRTC.refresh({reportError: error}));
 }
 
 var ControlSet = {
   render() {
-    let controls = document.createElement("div");
-    let control = document.createElement("div");
-    let message = document.createElement("div");
+    let controls = renderElement("div", null, {className: "controls"});
+    this.controlSection = renderElement("div", null, {className: "control"});
+    this.messageSection = renderElement("div", null, {className: "message"});
 
-    controls.className = "controls";
-    control.className = "control";
-    message.className = "message";
-    controls.appendChild(control);
-    controls.appendChild(message);
+    controls.appendChild(this.controlSection);
+    controls.appendChild(this.messageSection);
 
-    this.controlSection = control;
-    this.messageSection = message;
     return controls;
   },
 
   add(controlObj) {
     let [controlElem, messageElem] = controlObj.render();
     this.controlSection.appendChild(controlElem);
     this.messageSection.appendChild(messageElem);
   }
@@ -296,22 +291,19 @@ var AboutWebRTC = {
     this._onClearLog = onClearLog;
   },
 
   render(parent, data) {
     this._content = parent;
     this._setData(data);
 
     if (data.error) {
-      let msg = document.createElement("h3");
-      msg.textContent = getString("cannot_retrieve_log");
-      parent.appendChild(msg);
-      msg = document.createElement("p");
-      msg.textContent = `${data.error.name}: ${data.error.message}`;
-      parent.appendChild(msg);
+      parent.appendChild(renderElement("h3", getString("cannot_retrieve_log")));
+      parent.appendChild(
+          renderElement("p", `${data.error.name}: ${data.error.message}`));
       return;
     }
 
     this._peerConnections = this.renderPeerConnections();
     this._connectionLog = this.renderConnectionLog();
     this._content.appendChild(this._peerConnections);
     this._content.appendChild(this._connectionLog);
   },
@@ -332,30 +324,25 @@ var AboutWebRTC = {
     this._peerConnections = this.renderPeerConnections();
     let log = this._connectionLog;
     this._connectionLog = this.renderConnectionLog();
     this._content.replaceChild(this._peerConnections, pc);
     this._content.replaceChild(this._connectionLog, log);
   },
 
   renderPeerConnections() {
-    let connections = document.createElement("div");
-    connections.className = "stats";
+    let connections = renderElement("div", null, {className: "stats"});
+
+    let heading = renderElement("span", null, {className: "section-heading"});
+    heading.appendChild(renderElement("h3", getString("stats_heading")));
 
-    let heading = document.createElement("span");
-    heading.className = "section-heading";
-    let elem = document.createElement("h3");
-    elem.textContent = getString("stats_heading");
-    heading.appendChild(elem);
-
-    elem = document.createElement("button");
-    elem.textContent = getString("stats_clear");
-    elem.className = "no-print";
-    elem.onclick = this._onClearStats;
-    heading.appendChild(elem);
+    heading.appendChild(renderElement("button", getString("stats_clear"), {
+      className: "no-print",
+      onclick: this._onClearStats
+    }));
     connections.appendChild(heading);
 
     if (!this._reports || !this._reports.length) {
       return connections;
     }
 
     let reports = [...this._reports];
     reports.sort((a, b) => b.timestamp - a.timestamp);
@@ -363,59 +350,51 @@ var AboutWebRTC = {
       let peerConnection = new PeerConnection(report);
       connections.appendChild(peerConnection.render());
     }
 
     return connections;
   },
 
   renderConnectionLog() {
-    let content = document.createElement("div");
-    content.className = "log";
+    let content = renderElement("div", null, {className: "log"});
 
-    let heading = document.createElement("span");
-    heading.className = "section-heading";
-    let elem = document.createElement("h3");
-    elem.textContent = getString("log_heading");
-    heading.appendChild(elem);
-    elem = document.createElement("button");
-    elem.textContent = getString("log_clear");
-    elem.className = "no-print";
-    elem.onclick = this._onClearLog;
-    heading.appendChild(elem);
+    let heading = renderElement("span", null, {className: "section-heading"});
+    heading.appendChild(renderElement("h3", getString("log_heading")));
+    heading.appendChild(renderElement("button", getString("log_clear"), {
+      className: "no-print",
+      onclick: this._onClearLog
+    }));
     content.appendChild(heading);
 
     if (!this._log || !this._log.length) {
       return content;
     }
 
     let div = new FoldableSection(content, {
       showMsg: getString("log_show_msg"),
       hideMsg: getString("log_hide_msg")
     }).render();
 
     for (let line of this._log) {
-      elem = document.createElement("p");
-      elem.textContent = line;
-      div.appendChild(elem);
+      div.appendChild(renderElement("p", line));
     }
 
     content.appendChild(div);
     return content;
   }
 };
 
 function PeerConnection(report) {
   this._report = report;
 }
 
 PeerConnection.prototype = {
   render() {
-    let pc = document.createElement("div");
-    pc.className = "peer-connection";
+    let pc = renderElement("div", null, {className: "peer-connection"});
     pc.appendChild(this.renderHeading());
 
     let div = new FoldableSection(pc).render();
 
     div.appendChild(this.renderDesc());
     div.appendChild(new ICEStats(this._report).render());
     div.appendChild(new SDPStats(this._report).render());
     div.appendChild(new RTPStats(this._report).render());
@@ -430,90 +409,80 @@ PeerConnection.prototype = {
     let now = new Date(this._report.timestamp).toTimeString();
     heading.textContent =
       `[ ${pcInfo.id} ] ${pcInfo.url} ${pcInfo.closed ? `(${getString("connection_closed")})` : ""} ${now}`;
     return heading;
   },
 
   renderDesc() {
     let info = document.createElement("div");
-    let label = document.createElement("span");
-    let body = document.createElement("span");
 
-    label.className = "info-label";
-    label.textContent = `${getString("peer_connection_id_label")}: `;
-    info.appendChild(label);
+    info.appendChild(
+        renderElement("span", `${getString("peer_connection_id_label")}: `), {
+          className: "info-label"
+        });
 
-    body.className = "info-body";
-    body.textContent = this._report.pcid;
-    info.appendChild(body);
+    info.appendChild(renderElement("span", this._report.pcid, {
+      className: "info-body"
+    }));
 
     return info;
   },
 
   getPCInfo(report) {
     return {
       id: report.pcid.match(/id=(\S+)/)[1],
       url: report.pcid.match(/url=([^)]+)/)[1],
       closed: report.closed
     };
   }
 };
 
+function renderElement(elemName, elemText, options = {}) {
+  let elem = document.createElement(elemName);
+  elem.textContent = elemText || "";
+  Object.assign(elem, options);
+  return elem;
+}
+
 function SDPStats(report) {
   this._report = report;
 }
 
 SDPStats.prototype = {
   render() {
     let div = document.createElement("div");
-    let elem = document.createElement("h4");
+    div.appendChild(renderElement("h4", getString("sdp_heading")));
 
-    let localSdpHeading = getString("local_sdp_heading");
-    let remoteSdpHeading = getString("remote_sdp_heading");
     let offerLabel = `(${getString("offer")})`;
     let answerLabel = `(${getString("answer")})`;
-
-    elem.textContent = getString("sdp_heading");
-    div.appendChild(elem);
-
-    elem = document.createElement("h5");
-    elem.textContent =
-      `${localSdpHeading} ${this._report.offerer ? offerLabel : answerLabel}`;
-    div.appendChild(elem);
+    let localSdpHeading =
+      `${getString("local_sdp_heading")} ${this._report.offerer ? offerLabel : answerLabel}`;
+    let remoteSdpHeading =
+      `${getString("remote_sdp_heading")} ${this._report.offerer ? answerLabel : offerLabel}`;
 
-    elem = document.createElement("pre");
-    elem.textContent = this._report.localSdp;
-    div.appendChild(elem);
+    div.appendChild(renderElement("h5", localSdpHeading));
+    div.appendChild(renderElement("pre", this._report.localSdp));
 
-    elem = document.createElement("h5");
-    elem.textContent =
-      `${remoteSdpHeading} ${this._report.offerer ? answerLabel : offerLabel}`;
-    div.appendChild(elem);
-
-    elem = document.createElement("pre");
-    elem.textContent = this._report.remoteSdp;
-    div.appendChild(elem);
+    div.appendChild(renderElement("h5", remoteSdpHeading));
+    div.appendChild(renderElement("pre", this._report.remoteSdp));
 
     return div;
   }
 };
 
 function RTPStats(report) {
   this._report = report;
   this._stats = [];
 }
 
 RTPStats.prototype = {
   render() {
     let div = document.createElement("div");
-    let heading = document.createElement("h4");
-
-    heading.textContent = getString("rtp_stats_heading");
-    div.appendChild(heading);
+    div.appendChild(renderElement("h4", getString("rtp_stats_heading")));
 
     this.generateRTPStats();
 
     for (let statSet of this._stats) {
       div.appendChild(this.renderRTPStatSet(statSet));
     }
 
     return div;
@@ -549,19 +518,17 @@ RTPStats.prototype = {
 
     if (stats.mozAvSyncDelay) {
       statsString += `${getString("av_sync_label")}: ${stats.mozAvSyncDelay} ms `;
     }
     if (stats.mozJitterBufferDelay) {
       statsString += `${getString("jitter_buffer_delay_label")}: ${stats.mozJitterBufferDelay} ms`;
     }
 
-    let line = document.createElement("p");
-    line.textContent = statsString;
-    return line;
+    return renderElement("p", statsString);
   },
 
   renderCoderStats(stats) {
     let statsString = "";
     let label;
 
     if (stats.bitrateMean) {
       statsString += ` ${getString("avg_bitrate_label")}: ${(stats.bitrateMean / 1000000).toFixed(2)} Mbps`;
@@ -584,19 +551,17 @@ RTPStats.prototype = {
       statsString += ` ${getString("discarded_packets_label")}: ${stats.discardedPackets}`;
     }
 
     if (statsString) {
       label = (stats.packetsReceived ? ` ${getString("decoder_label")}:` : ` ${getString("encoder_label")}:`);
       statsString = label + statsString;
     }
 
-    let line = document.createElement("p");
-    line.textContent = statsString;
-    return line;
+    return renderElement("p", statsString);
   },
 
   renderTransportStats(stats, typeLabel) {
     let time  = new Date(stats.timestamp).toTimeString();
     let statsString = `${typeLabel}: ${time} ${stats.type} SSRC: ${stats.ssrc}`;
 
     if (stats.packetsReceived) {
       statsString += ` ${getString("received_label")}: ${stats.packetsReceived} ${getString("packets")}`;
@@ -612,27 +577,22 @@ RTPStats.prototype = {
       }
     } else if (stats.packetsSent) {
       statsString += ` ${getString("sent_label")}: ${stats.packetsSent} ${getString("packets")}`;
       if (stats.bytesSent) {
         statsString += ` (${(stats.bytesSent / 1024).toFixed(2)} Kb)`;
       }
     }
 
-    let line = document.createElement("p");
-    line.textContent = statsString;
-    return line;
+    return renderElement("p", statsString);
   },
 
   renderRTPStatSet(stats) {
     let div = document.createElement("div");
-    let heading = document.createElement("h5");
-
-    heading.textContent = stats.id;
-    div.appendChild(heading);
+    div.appendChild(renderElement("h5", stats.id));
 
     if (stats.MozAvSyncDelay || stats.mozJitterBufferDelay) {
       div.appendChild(this.renderAvStats(stats));
     }
 
     div.appendChild(this.renderCoderStats(stats));
     div.appendChild(this.renderTransportStats(stats, getString("typeLocal")));
 
@@ -646,45 +606,40 @@ RTPStats.prototype = {
 
 function ICEStats(report) {
   this._report = report;
 }
 
 ICEStats.prototype = {
   render() {
     let div = document.createElement("div");
-    let heading = document.createElement("h4");
-
-    heading.textContent = getString("ice_stats_heading");
-    div.appendChild(heading);
+    div.appendChild(renderElement("h4", getString("ice_stats_heading")));
 
     div.appendChild(this.renderICECandidateTable());
     // add just a bit of vertical space between the restart/rollback
     // counts and the ICE candidate pair table above.
     div.appendChild(document.createElement("br"));
     div.appendChild(this.renderIceMetric("ice_restart_count_label",
                                          this._report.iceRestarts));
     div.appendChild(this.renderIceMetric("ice_rollback_count_label",
                                          this._report.iceRollbacks));
 
     div.appendChild(this.renderRawICECandidateSection());
 
     return div;
   },
 
   renderICECandidateTable() {
-    let caption = document.createElement("caption");
-    let captionSpan1 = document.createElement("span");
-    captionSpan1.textContent = `${getString("trickle_caption_msg")} `;
-    let captionSpan2 = document.createElement("span");
-    captionSpan2.textContent = getString("trickle_highlight_color_name");
-    captionSpan2.className = "trickled";
-    caption.appendChild(captionSpan1);
-    caption.appendChild(captionSpan2);
-    caption.className = "no-print";
+    let caption = renderElement("caption", null, {className: "no-print"});
+    caption.appendChild(
+        renderElement("span", `${getString("trickle_caption_msg")} `));
+    caption.appendChild(
+        renderElement("span", getString("trickle_highlight_color_name"), {
+          className: "trickled"
+        }));
 
     let stats = this.generateICEStats();
     // don't use |stat.x || ""| here because it hides 0 values
     let tbody = stats.map(stat => [
       stat["local-candidate"],
       stat["remote-candidate"],
       stat.state,
       stat.priority,
@@ -734,19 +689,18 @@ ICEStats.prototype = {
     statsTable.className = "raw-candidate";
     div.appendChild(statsTable);
 
     return div;
   },
 
   renderRawICECandidateSection() {
     let section = document.createElement("div");
-    let heading = document.createElement("h4");
-    heading.textContent = getString("raw_candidates_heading");
-    section.appendChild(heading);
+    section.appendChild(
+        renderElement("h4", getString("raw_candidates_heading")));
 
     let div = new FoldableSection(section, {
       showMsg: getString("raw_cand_show_msg"),
       hideMsg: getString("raw_cand_hide_msg")
     }).render();
 
     div.appendChild(this.renderRawICECandidates());
 
@@ -775,26 +729,24 @@ ICEStats.prototype = {
       };
       rows.push(row);
     }
     return rows;
   },
 
   renderIceMetric(labelName, value) {
     let info = document.createElement("div");
-    let label = document.createElement("span");
-    let body = document.createElement("span");
 
-    label.className = "info-label";
-    label.textContent = `${getString(labelName)}: `;
-    info.appendChild(label);
+    info.appendChild(
+        renderElement("span", `${getString(labelName)}: `, {
+          className: "info-label"
+        }));
+    info.appendChild(
+        renderElement("span", value, {className: "info-body"}));
 
-    body.className = "info-body";
-    body.textContent = value;
-    info.appendChild(body);
     return info;
   },
 
   generateICEStats() {
     // Create an index based on candidate ID for each element in the
     // iceCandidateStats array.
     let candidates = new Map();
 
@@ -862,18 +814,19 @@ ICEStats.prototype = {
 
     return `${c.ipAddress}:${c.portNumber}/${c.transport}(${type})`;
   }
 };
 
 function FoldableSection(parentElement, options = {}) {
   this._foldableElement = document.createElement("div");
   if (parentElement) {
-    let sectionCtrl = document.createElement("div");
-    sectionCtrl.className = "section-ctrl no-print";
+    let sectionCtrl = renderElement("div", null, {
+      className: "section-ctrl no-print"
+    });
     let foldEffect = new FoldEffect(this._foldableElement, options);
     sectionCtrl.appendChild(foldEffect.render());
     parentElement.appendChild(sectionCtrl);
   }
 }
 
 FoldableSection.prototype = {
   render() {
@@ -888,19 +841,17 @@ function SimpleTable(heading, data, capt
 }
 
 SimpleTable.prototype = {
   renderRow(list, header) {
     let row = document.createElement("tr");
     let elemType = (header ? "th" : "td");
 
     for (let elem of list) {
-      let cell = document.createElement(elemType);
-      cell.textContent = elem;
-      row.appendChild(cell);
+      row.appendChild(renderElement(elemType, elem));
     }
 
     return row;
   },
 
   render() {
     let table = document.createElement("table");
 
@@ -929,19 +880,18 @@ function FoldEffect(targetElem, options 
     this._target = targetElem;
   }
 }
 
 FoldEffect.prototype = {
   render() {
     this._target.classList.add("fold-target");
 
-    let ctrl = document.createElement("div");
+    let ctrl = renderElement("div", null, {className: "fold-trigger"});
     this._trigger = ctrl;
-    ctrl.className = "fold-trigger";
     ctrl.addEventListener("click", this.onClick.bind(this));
     this.close();
 
     FoldEffect._sections.push(this);
     return ctrl;
   },
 
   onClick() {
