# HG changeset patch
# User Mike Shal <mshal@mozilla.com>
# Date 1512676370 18000
# Node ID 271c923ff4f663d6db85c51db1701c11964470b5
# Parent  4868ad128f71ceaf7cca5f5ab09c326c30cf0477
Bug 1421038 - Rename 'relativedir' to 'relsrcdir'; r=nalexander

The name 'relativedir' is ambiguous - it is unclear whether it is a
relative srcdir or objdir. Rename it to 'relsrcdir' in the
ContextDerived() object to match the naming used in Context() so it is
obvious that it is a relative srcdir.

Most of these are a straight text replacement from relativedir to
relsrcdir, except for tup.py:_get_backend_file(), which was supposed to
be using an objdir in the first place.

MozReview-Commit-ID: 9eFHOCMofq5

diff --git a/python/mozbuild/mozbuild/backend/cpp_eclipse.py b/python/mozbuild/mozbuild/backend/cpp_eclipse.py
--- a/python/mozbuild/mozbuild/backend/cpp_eclipse.py
+++ b/python/mozbuild/mozbuild/backend/cpp_eclipse.py
@@ -66,17 +66,17 @@ class CppEclipseBackend(CommonBackend):
         # Eclipse doesn't support having the workspace inside the srcdir.
         # Since most people have their objdir inside their srcdir it's easier
         # and more consistent to just put the workspace along side the srcdir
         srcdir_parent = os.path.dirname(topsrcdir)
         workspace_dirname = "eclipse_" + os.path.basename(topobjdir)
         return os.path.join(srcdir_parent, workspace_dirname)
 
     def consume_object(self, obj):
-        reldir = getattr(obj, 'relativedir', None)
+        reldir = getattr(obj, 'relsrcdir', None)
 
         # Note that unlike VS, Eclipse' indexer seem to crawl the headers and
         # isn't picky about the local includes.
         if isinstance(obj, Defines):
             self._paths_to_defines.setdefault(reldir, {}).update(obj.defines)
 
         return True
 
diff --git a/python/mozbuild/mozbuild/backend/recursivemake.py b/python/mozbuild/mozbuild/backend/recursivemake.py
--- a/python/mozbuild/mozbuild/backend/recursivemake.py
+++ b/python/mozbuild/mozbuild/backend/recursivemake.py
@@ -1552,17 +1552,17 @@ class RecursiveMakeBackend(CommonBackend
                                ' '.join(args)])
         args = [
             mozpath.join('$(DEPTH)', obj.path),
             make_quote(shell_quote(str(obj.entry))),
         ]
         rule.add_commands(['$(call py_action,buildlist,%s)' % ' '.join(args)])
         fragment.dump(backend_file.fh, removal_guard=False)
 
-        self._no_skip['misc'].add(obj.relativedir)
+        self._no_skip['misc'].add(obj.relsrcdir)
 
     def _write_manifests(self, dest, manifests):
         man_dir = mozpath.join(self.environment.topobjdir, '_build_manifests',
             dest)
 
         for k, manifest in manifests.items():
             with self._write_file(mozpath.join(man_dir, k)) as fh:
                 manifest.write(fileobj=fh)
diff --git a/python/mozbuild/mozbuild/backend/visualstudio.py b/python/mozbuild/mozbuild/backend/visualstudio.py
--- a/python/mozbuild/mozbuild/backend/visualstudio.py
+++ b/python/mozbuild/mozbuild/backend/visualstudio.py
@@ -86,17 +86,17 @@ class VisualStudioBackend(CommonBackend)
     def summary(self):
         return ExecutionSummary(
             'VisualStudio backend executed in {execution_time:.2f}s\n'
             'Generated Visual Studio solution at {path:s}',
             execution_time=self._execution_time,
             path=os.path.join(self._out_dir, 'mozilla.sln'))
 
     def consume_object(self, obj):
-        reldir = getattr(obj, 'relativedir', None)
+        reldir = getattr(obj, 'relsrcdir', None)
 
         if hasattr(obj, 'config') and reldir not in self._paths_to_configs:
             self._paths_to_configs[reldir] = obj.config
 
         if isinstance(obj, Sources):
             self._add_sources(reldir, obj)
 
         elif isinstance(obj, HostSources):
@@ -126,17 +126,17 @@ class VisualStudioBackend(CommonBackend)
         # Just acknowledge everything.
         return True
 
     def _add_sources(self, reldir, obj):
         s = self._paths_to_sources.setdefault(reldir, set())
         s.update(obj.files)
 
     def _process_unified_sources(self, obj):
-        reldir = getattr(obj, 'relativedir', None)
+        reldir = getattr(obj, 'relsrcdir', None)
 
         s = self._paths_to_sources.setdefault(reldir, set())
         s.update(obj.files)
 
     def consume_finished(self):
         out_dir = self._out_dir
         out_proj_dir = os.path.join(self._out_dir, self._projsubdir)
 
diff --git a/python/mozbuild/mozbuild/compilation/database.py b/python/mozbuild/mozbuild/compilation/database.py
--- a/python/mozbuild/mozbuild/compilation/database.py
+++ b/python/mozbuild/mozbuild/compilation/database.py
@@ -44,17 +44,17 @@ class CompileDBBackend(CommonBackend):
         self._flags = {}
 
         self._envs = {}
         self._local_flags = defaultdict(dict)
         self._per_source_flags = defaultdict(list)
 
     def consume_object(self, obj):
         # Those are difficult directories, that will be handled later.
-        if obj.relativedir in (
+        if obj.relsrcdir in (
                 'build/unix/elfhack',
                 'build/unix/elfhack/inject',
                 'build/clang-plugin',
                 'build/clang-plugin/tests'):
             return True
 
         consumed = CommonBackend.consume_object(self, obj)
 
@@ -62,17 +62,17 @@ class CompileDBBackend(CommonBackend):
             return True
 
         if isinstance(obj, DirectoryTraversal):
             self._envs[obj.objdir] = obj.config
 
         elif isinstance(obj, (Sources, GeneratedSources)):
             # For other sources, include each source file.
             for f in obj.files:
-                self._build_db_line(obj.objdir, obj.relativedir, obj.config, f,
+                self._build_db_line(obj.objdir, obj.relsrcdir, obj.config, f,
                                     obj.canonical_suffix)
 
         elif isinstance(obj, VariablePassthru):
             for var in ('MOZBUILD_CMFLAGS', 'MOZBUILD_CMMFLAGS'):
                 if var in obj.variables:
                     self._local_flags[obj.objdir][var] = obj.variables[var]
 
         elif isinstance(obj, PerSourceFlag):
@@ -127,20 +127,20 @@ class CompileDBBackend(CommonBackend):
         outputfile = os.path.join(self.environment.topobjdir, 'compile_commands.json')
         with self._write_file(outputfile) as jsonout:
             json.dump(db, jsonout, indent=0)
 
     def _process_unified_sources(self, obj):
         # For unified sources, only include the unified source file.
         # Note that unified sources are never used for host sources.
         for f in obj.unified_source_mapping:
-            self._build_db_line(obj.objdir, obj.relativedir, obj.config, f[0],
+            self._build_db_line(obj.objdir, obj.relsrcdir, obj.config, f[0],
                                 obj.canonical_suffix)
             for entry in f[1]:
-                self._build_db_line(obj.objdir, obj.relativedir, obj.config,
+                self._build_db_line(obj.objdir, obj.relsrcdir, obj.config,
                                     entry, obj.canonical_suffix, unified=f[0])
 
     def _handle_idl_manager(self, idl_manager):
         pass
 
     def _handle_ipdl_sources(self, ipdl_dir, sorted_ipdl_sources, sorted_nonstatic_ipdl_sources,
                              sorted_static_ipdl_sources, unified_ipdl_cppsrcs_mapping):
         for f in unified_ipdl_cppsrcs_mapping:
diff --git a/python/mozbuild/mozbuild/frontend/data.py b/python/mozbuild/mozbuild/frontend/data.py
--- a/python/mozbuild/mozbuild/frontend/data.py
+++ b/python/mozbuild/mozbuild/frontend/data.py
@@ -49,17 +49,17 @@ class ContextDerived(TreeMetadata):
     never instantiated directly but is instead derived from.
     """
 
     __slots__ = (
         'context_main_path',
         'context_all_paths',
         'topsrcdir',
         'topobjdir',
-        'relativedir',
+        'relsrcdir',
         'srcdir',
         'objdir',
         'config',
         '_context',
     )
 
     def __init__(self, context):
         TreeMetadata.__init__(self)
@@ -67,17 +67,17 @@ class ContextDerived(TreeMetadata):
         # Capture the files that were evaluated to fill this context.
         self.context_main_path = context.main_path
         self.context_all_paths = context.all_paths
 
         # Basic directory state.
         self.topsrcdir = context.config.topsrcdir
         self.topobjdir = context.config.topobjdir
 
-        self.relativedir = context.relsrcdir
+        self.relsrcdir = context.relsrcdir
         self.srcdir = context.srcdir
         self.objdir = context.objdir
 
         self.config = context.config
 
         self._context = context
 
     @property
diff --git a/python/mozbuild/mozbuild/frontend/emitter.py b/python/mozbuild/mozbuild/frontend/emitter.py
--- a/python/mozbuild/mozbuild/frontend/emitter.py
+++ b/python/mozbuild/mozbuild/frontend/emitter.py
@@ -362,17 +362,17 @@ class TreeMetadataEmitter(LoggingMixin):
 
             if not candidates:
                 raise SandboxValidationError(
                     '%s contains "%s", which does not match any %s in the tree.'
                     % (variable, path, self.LIBRARY_NAME_VAR[obj.KIND]),
                     context)
 
             elif len(candidates) > 1:
-                paths = (mozpath.join(l.relativedir, 'moz.build')
+                paths = (mozpath.join(l.relsrcdir, 'moz.build')
                     for l in candidates)
                 raise SandboxValidationError(
                     '%s contains "%s", which matches a %s defined in multiple '
                     'places:\n    %s' % (variable, path,
                     self.LIBRARY_NAME_VAR[obj.KIND],
                     '\n    '.join(paths)), context)
 
             elif force_static and not isinstance(candidates[0], StaticLibrary):
@@ -560,17 +560,17 @@ class TreeMetadataEmitter(LoggingMixin):
             else:
                 linkables.append(prog)
 
         def check_unique_binary(program, kind):
             if program in self._binaries:
                 raise SandboxValidationError(
                     'Cannot use "%s" as %s name, '
                     'because it is already used in %s' % (program, kind,
-                    self._binaries[program].relativedir), context)
+                    self._binaries[program].relsrcdir), context)
         for kind, cls in [('PROGRAM', Program), ('HOST_PROGRAM', HostProgram)]:
             program = context.get(kind)
             if program:
                 check_unique_binary(program, kind)
                 self._binaries[program] = cls(context, program)
                 self._linkage.append((context, self._binaries[program],
                     kind.replace('PROGRAM', 'USE_LIBS')))
                 add_program(self._binaries[program], kind)
@@ -609,17 +609,17 @@ class TreeMetadataEmitter(LoggingMixin):
                 ('SIMPLE_PROGRAMS', SimpleProgram),
                 ('CPP_UNIT_TESTS', SimpleProgram),
                 ('HOST_SIMPLE_PROGRAMS', HostSimpleProgram)]:
             for program in context[kind]:
                 if program in self._binaries:
                     raise SandboxValidationError(
                         'Cannot use "%s" in %s, '
                         'because it is already used in %s' % (program, kind,
-                        self._binaries[program].relativedir), context)
+                        self._binaries[program].relsrcdir), context)
                 self._binaries[program] = cls(context, program,
                     is_unit_test=kind == 'CPP_UNIT_TESTS')
                 self._linkage.append((context, self._binaries[program],
                     'HOST_USE_LIBS' if kind == 'HOST_SIMPLE_PROGRAMS'
                     else 'USE_LIBS'))
                 add_program(self._binaries[program], kind)
 
         host_libname = context.get('HOST_LIBRARY_NAME')
diff --git a/python/mozbuild/mozbuild/test/frontend/test_emitter.py b/python/mozbuild/mozbuild/test/frontend/test_emitter.py
--- a/python/mozbuild/mozbuild/test/frontend/test_emitter.py
+++ b/python/mozbuild/mozbuild/test/frontend/test_emitter.py
@@ -108,17 +108,17 @@ class TestEmitterBasic(unittest.TestCase
         objs = self.read_topsrcdir(reader, filter_common=False)
         self.assertEqual(len(objs), 4)
 
         for o in objs:
             self.assertIsInstance(o, DirectoryTraversal)
             self.assertTrue(os.path.isabs(o.context_main_path))
             self.assertEqual(len(o.context_all_paths), 1)
 
-        reldirs = [o.relativedir for o in objs]
+        reldirs = [o.relsrcdir for o in objs]
         self.assertEqual(reldirs, ['', 'foo', 'foo/biz', 'bar'])
 
         dirs = [[d.full_path for d in o.dirs] for o in objs]
         self.assertEqual(dirs, [
             [
                 mozpath.join(reader.config.topsrcdir, 'foo'),
                 mozpath.join(reader.config.topsrcdir, 'bar')
             ], [
@@ -128,39 +128,39 @@ class TestEmitterBasic(unittest.TestCase
     def test_traversal_all_vars(self):
         reader = self.reader('traversal-all-vars')
         objs = self.read_topsrcdir(reader, filter_common=False)
         self.assertEqual(len(objs), 2)
 
         for o in objs:
             self.assertIsInstance(o, DirectoryTraversal)
 
-        reldirs = set([o.relativedir for o in objs])
+        reldirs = set([o.relsrcdir for o in objs])
         self.assertEqual(reldirs, set(['', 'regular']))
 
         for o in objs:
-            reldir = o.relativedir
+            reldir = o.relsrcdir
 
             if reldir == '':
                 self.assertEqual([d.full_path for d in o.dirs], [
                     mozpath.join(reader.config.topsrcdir, 'regular')])
 
     def test_traversal_all_vars_enable_tests(self):
         reader = self.reader('traversal-all-vars', enable_tests=True)
         objs = self.read_topsrcdir(reader, filter_common=False)
         self.assertEqual(len(objs), 3)
 
         for o in objs:
             self.assertIsInstance(o, DirectoryTraversal)
 
-        reldirs = set([o.relativedir for o in objs])
+        reldirs = set([o.relsrcdir for o in objs])
         self.assertEqual(reldirs, set(['', 'regular', 'test']))
 
         for o in objs:
-            reldir = o.relativedir
+            reldir = o.relsrcdir
 
             if reldir == '':
                 self.assertEqual([d.full_path for d in o.dirs], [
                     mozpath.join(reader.config.topsrcdir, 'regular'),
                     mozpath.join(reader.config.topsrcdir, 'test')])
 
     def test_config_file_substitution(self):
         reader = self.reader('config-file-substitution')
@@ -926,19 +926,19 @@ class TestEmitterBasic(unittest.TestCase
     def test_ipdl_sources(self):
         reader = self.reader('ipdl_sources')
         objs = self.read_topsrcdir(reader)
 
         ipdls = []
         nonstatic_ipdls = []
         for o in objs:
             if isinstance(o, IPDLFile):
-                ipdls.append('%s/%s' % (o.relativedir, o.basename))
+                ipdls.append('%s/%s' % (o.relsrcdir, o.basename))
             elif isinstance(o, PreprocessedIPDLFile):
-                nonstatic_ipdls.append('%s/%s' % (o.relativedir, o.basename))
+                nonstatic_ipdls.append('%s/%s' % (o.relsrcdir, o.basename))
 
         expected = [
             'bar/bar.ipdl',
             'bar/bar2.ipdlh',
             'foo/foo.ipdl',
             'foo/foo2.ipdlh',
         ]
 
