# HG changeset patch
# User Lee Salzman <lsalzman@mozilla.com>
# Date 1523393192 14400
# Node ID 2a0555131c04bffedb7a71be27ca97a3f8303d92
# Parent  43c4ad601a5c3364f6fd64486adf10e323766d74
Bug 1450814 - generic implementation of DrawTarget::Blur using LockBits. r=rhunt a=jcristau

MozReview-Commit-ID: 2jROASoiPQd

diff --git a/gfx/2d/2D.h b/gfx/2d/2D.h
--- a/gfx/2d/2D.h
+++ b/gfx/2d/2D.h
@@ -1200,19 +1200,17 @@ public:
    * onto the target in device space using POINT sampling and operator over.
    */
   virtual void PopLayer() { MOZ_CRASH("GFX: PopLayer"); }
 
   /**
    * Perform an in-place blur operation. This is only supported on data draw
    * targets.
    */
-  virtual void Blur(const AlphaBoxBlur& aBlur) {
-    MOZ_CRASH("GFX: DoBlur");
-  }
+  virtual void Blur(const AlphaBoxBlur& aBlur);
 
   /**
    * Create a SourceSurface optimized for use with this DrawTarget from
    * existing bitmap data in memory.
    *
    * The SourceSurface does not take ownership of aData, and may be freed at any time.
    */
   virtual already_AddRefed<SourceSurface> CreateSourceSurfaceFromData(unsigned char *aData,
diff --git a/gfx/2d/DrawTarget.cpp b/gfx/2d/DrawTarget.cpp
--- a/gfx/2d/DrawTarget.cpp
+++ b/gfx/2d/DrawTarget.cpp
@@ -264,10 +264,30 @@ DrawTarget::IntoLuminanceSource(Luminanc
   }
 
   maskSurface->Unmap();
   destMaskSurface->Unmap();
 
   return destMaskSurface.forget();
 }
 
+void
+DrawTarget::Blur(const AlphaBoxBlur& aBlur)
+{
+  uint8_t* data;
+  IntSize size;
+  int32_t stride;
+  SurfaceFormat format;
+  if (!LockBits(&data, &size, &stride, &format)) {
+    gfxWarning() << "Cannot perform in-place blur on non-data DrawTarget";
+    return;
+  }
+
+  // Sanity check that the blur size matches the draw target.
+  MOZ_ASSERT(size == aBlur.GetSize());
+  MOZ_ASSERT(stride == aBlur.GetStride());
+  aBlur.Blur(data);
+
+  ReleaseBits(data);
+}
+
 } // namespace gfx
 } // namespace mozilla
diff --git a/gfx/2d/DrawTargetSkia.cpp b/gfx/2d/DrawTargetSkia.cpp
--- a/gfx/2d/DrawTargetSkia.cpp
+++ b/gfx/2d/DrawTargetSkia.cpp
@@ -2206,35 +2206,16 @@ DrawTargetSkia::PopLayer()
   mPushedLayers.pop_back();
 
 #ifdef MOZ_WIDGET_COCOA
   CGContextRelease(mCG);
   mCG = nullptr;
 #endif
 }
 
-void
-DrawTargetSkia::Blur(const AlphaBoxBlur& aBlur)
-{
-  MarkChanged();
-  Flush();
-
-  SkPixmap pixmap;
-  if (!mCanvas->peekPixels(&pixmap)) {
-    gfxWarning() << "Cannot perform in-place blur on non-raster Skia surface";
-    return;
-  }
-
-  // Sanity check that the blur size matches the draw target.
-  MOZ_ASSERT(pixmap.width() == aBlur.GetSize().width);
-  MOZ_ASSERT(pixmap.height() == aBlur.GetSize().height);
-  MOZ_ASSERT(size_t(aBlur.GetStride()) == pixmap.rowBytes());
-  aBlur.Blur(static_cast<uint8_t*>(pixmap.writable_addr()));
-}
-
 already_AddRefed<GradientStops>
 DrawTargetSkia::CreateGradientStops(GradientStop *aStops, uint32_t aNumStops, ExtendMode aExtendMode) const
 {
   std::vector<GradientStop> stops;
   stops.resize(aNumStops);
   for (uint32_t i = 0; i < aNumStops; i++) {
     stops[i] = aStops[i];
   }
diff --git a/gfx/2d/DrawTargetSkia.h b/gfx/2d/DrawTargetSkia.h
--- a/gfx/2d/DrawTargetSkia.h
+++ b/gfx/2d/DrawTargetSkia.h
@@ -107,17 +107,16 @@ public:
   virtual void PushDeviceSpaceClipRects(const IntRect* aRects, uint32_t aCount) override;
   virtual void PopClip() override;
   virtual void PushLayer(bool aOpaque, Float aOpacity,
                          SourceSurface* aMask,
                          const Matrix& aMaskTransform,
                          const IntRect& aBounds = IntRect(),
                          bool aCopyBackground = false) override;
   virtual void PopLayer() override;
-  virtual void Blur(const AlphaBoxBlur& aBlur) override;
   virtual already_AddRefed<SourceSurface> CreateSourceSurfaceFromData(unsigned char *aData,
                                                             const IntSize &aSize,
                                                             int32_t aStride,
                                                             SurfaceFormat aFormat) const override;
   virtual already_AddRefed<SourceSurface> OptimizeSourceSurface(SourceSurface *aSurface) const override;
   virtual already_AddRefed<SourceSurface> OptimizeSourceSurfaceForUnknownAlpha(SourceSurface *aSurface) const override;
   virtual already_AddRefed<SourceSurface>
     CreateSourceSurfaceFromNativeSurface(const NativeSurface &aSurface) const override;
