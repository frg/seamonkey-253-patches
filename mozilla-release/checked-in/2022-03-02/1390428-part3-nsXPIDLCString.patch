# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1502855935 -36000
# Node ID 009055acb0b72b06f64570bd4c91eb6742975307
# Parent  1673c8455fcbec6d3ee6cb969b00c6c04c036dd4
Bug 1390428 (part 3) - Remove nsXPIDLCString use in nsHttpHandler. r=erahm.

Most of these fields are not using any nsXPIDLCString-specific features.
The exceptions are mUserAgentOverride and mDefaultSocketType, which require a
little more care.

diff --git a/netwerk/protocol/http/nsHttpHandler.cpp b/netwerk/protocol/http/nsHttpHandler.cpp
--- a/netwerk/protocol/http/nsHttpHandler.cpp
+++ b/netwerk/protocol/http/nsHttpHandler.cpp
@@ -257,16 +257,18 @@ nsHttpHandler::nsHttpHandler()
     , mFastOpenConsecutiveFailureLimit(5)
     , mFastOpenConsecutiveFailureCounter(0)
     , mActiveTabPriority(true)
     , mProcessId(0)
     , mNextChannelId(1)
 {
     LOG(("Creating nsHttpHandler [this=%p].\n", this));
 
+    mUserAgentOverride.SetIsVoid(true);
+
     MOZ_ASSERT(!gHttpHandler, "HTTP handler already created!");
     gHttpHandler = this;
     nsCOMPtr<nsIXULRuntime> runtime = do_GetService("@mozilla.org/xre/runtime;1");
     if (runtime) {
         runtime->GetProcessID(&mProcessId);
     }
     SetFastOpenOSSupport();
 }
@@ -823,17 +825,17 @@ const nsCString&
 nsHttpHandler::UserAgent()
 {
     if (nsContentUtils::ShouldResistFingerprinting() &&
         !mSpoofedUserAgent.IsEmpty()) {
         LOG(("using spoofed userAgent : %s\n", mSpoofedUserAgent.get()));
         return mSpoofedUserAgent;
     }
 
-    if (mUserAgentOverride) {
+    if (!mUserAgentOverride.IsVoid()) {
         LOG(("using general.useragent.override : %s\n", mUserAgentOverride.get()));
         return mUserAgentOverride;
     }
 
     if (mUserAgentIsDirty) {
         BuildUserAgent();
         mUserAgentIsDirty = false;
     }
@@ -1382,17 +1384,17 @@ nsHttpHandler::PrefsChanged(nsIPrefBranc
     }
 
     if (PREF_CHANGED(HTTP_PREF("default-socket-type"))) {
         nsCString sval;
         rv = prefs->GetCharPref(HTTP_PREF("default-socket-type"),
                                 getter_Copies(sval));
         if (NS_SUCCEEDED(rv)) {
             if (sval.IsEmpty())
-                mDefaultSocketType.Adopt(nullptr);
+                mDefaultSocketType.SetIsVoid(true);
             else {
                 // verify that this socket type is actually valid
                 nsCOMPtr<nsISocketProviderService> sps(
                         do_GetService(NS_SOCKETPROVIDERSERVICE_CONTRACTID));
                 if (sps) {
                     nsCOMPtr<nsISocketProvider> sp;
                     rv = sps->GetSocketProvider(sval.get(), getter_AddRefs(sp));
                     if (NS_SUCCEEDED(rv)) {
diff --git a/netwerk/protocol/http/nsHttpHandler.h b/netwerk/protocol/http/nsHttpHandler.h
--- a/netwerk/protocol/http/nsHttpHandler.h
+++ b/netwerk/protocol/http/nsHttpHandler.h
@@ -90,17 +90,17 @@ public:
     PRIntervalTime IdleTimeout()             { return mIdleTimeout; }
     PRIntervalTime SpdyTimeout()             { return mSpdyTimeout; }
     PRIntervalTime ResponseTimeout() {
       return mResponseTimeoutEnabled ? mResponseTimeout : 0;
     }
     PRIntervalTime ResponseTimeoutEnabled()  { return mResponseTimeoutEnabled; }
     uint32_t       NetworkChangedTimeout()   { return mNetworkChangedTimeout; }
     uint16_t       MaxRequestAttempts()      { return mMaxRequestAttempts; }
-    const char    *DefaultSocketType()       { return mDefaultSocketType.get(); /* ok to return null */ }
+    const char    *DefaultSocketType()       { return mDefaultSocketType.IsVoid() ? nullptr : mDefaultSocketType.get(); }
     uint32_t       PhishyUserPassLength()    { return mPhishyUserPassLength; }
     uint8_t        GetQoSBits()              { return mQoSBits; }
     uint16_t       GetIdleSynTimeout()       { return mIdleSynTimeout; }
     bool           FastFallbackToIPv4()      { return mFastFallbackToIPv4; }
     uint32_t       MaxSocketCount();
     bool           EnforceAssocReq()         { return mEnforceAssocReq; }
 
     bool           IsPersistentHttpsCachingEnabled() { return mEnablePersistentHttpsCaching; }
@@ -476,40 +476,40 @@ private:
 
     bool mEnforceAssocReq;
 
     nsCString mAccept;
     nsCString mAcceptLanguages;
     nsCString mHttpAcceptEncodings;
     nsCString mHttpsAcceptEncodings;
 
-    nsXPIDLCString mDefaultSocketType;
+    nsCString mDefaultSocketType;
 
     // cache support
     uint32_t                  mLastUniqueID;
     uint32_t                  mSessionStartTime;
 
     // useragent components
     nsCString      mLegacyAppName;
     nsCString      mLegacyAppVersion;
     nsCString      mPlatform;
     nsCString      mOscpu;
     nsCString      mMisc;
     nsCString      mProduct;
-    nsXPIDLCString mProductSub;
-    nsXPIDLCString mAppName;
-    nsXPIDLCString mAppVersion;
+    nsCString      mProductSub;
+    nsCString      mAppName;
+    nsCString      mAppVersion;
     nsCString      mCompatFirefox;
     bool           mCompatFirefoxEnabled;
-    nsXPIDLCString mCompatDevice;
+    nsCString      mCompatDevice;
     nsCString      mDeviceModelId;
 
     nsCString      mUserAgent;
     nsCString      mSpoofedUserAgent;
-    nsXPIDLCString mUserAgentOverride;
+    nsCString      mUserAgentOverride;
     bool           mUserAgentIsDirty; // true if mUserAgent should be rebuilt
     bool           mAcceptLanguagesIsDirty;
 
 
     bool           mPromptTempRedirect;
 
     // Persistent HTTPS caching flag
     bool           mEnablePersistentHttpsCaching;
