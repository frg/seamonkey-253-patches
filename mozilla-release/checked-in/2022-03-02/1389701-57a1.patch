# HG changeset patch
# User Ed Lee <edilee@mozilla.com>
# Date 1502986093 21600
#      Thu Aug 17 10:08:13 2017 -0600
# Node ID e81a6f33863a952d1cad99d0a3cfd2f0aeaf8b35
# Parent  aa7ae0ecaa87b3650252517b664ed08e9d5036b1
Bug 1389701 - Newly loaded child RemotePageManager can register pages too late to properly manage created documents. r=mossop

MozReview-Commit-ID: HSxuywFhqmb

diff --git a/toolkit/modules/RemotePageManager.jsm b/toolkit/modules/RemotePageManager.jsm
--- a/toolkit/modules/RemotePageManager.jsm
+++ b/toolkit/modules/RemotePageManager.jsm
@@ -444,32 +444,37 @@ ChildMessagePort.prototype.destroy = fun
 // Allows callers to register to connect to specific content pages. Registration
 // is done through the addRemotePageListener method
 var RemotePageManagerInternal = {
   // The currently registered remote pages
   pages: new Map(),
 
   // Initialises all the needed listeners
   init() {
-    Services.ppmm.addMessageListener("RemotePage:InitListener", this.initListener.bind(this));
     Services.mm.addMessageListener("RemotePage:InitPort", this.initPort.bind(this));
+    this.updateProcessUrls();
+  },
+
+  updateProcessUrls() {
+    Services.ppmm.initialProcessData["RemotePageManager:urls"] = Array.from(this.pages.keys());
   },
 
   // Registers interest in a remote page. A callback is called with a port for
   // the new page when loading begins (i.e. the page hasn't actually loaded yet).
   // Only one callback can be registered per URL.
   addRemotePageListener(url, callback) {
     if (Services.appinfo.processType != Ci.nsIXULRuntime.PROCESS_TYPE_DEFAULT)
       throw new Error("RemotePageManager can only be used in the main process.");
 
     if (this.pages.has(url)) {
       throw new Error("Remote page already registered: " + url);
     }
 
     this.pages.set(url, callback);
+    this.updateProcessUrls();
 
     // Notify all the frame scripts of the new registration
     Services.ppmm.broadcastAsyncMessage("RemotePage:Register", { urls: [url] });
   },
 
   // Removes any interest in a remote page.
   removeRemotePageListener(url) {
     if (Services.appinfo.processType != Ci.nsIXULRuntime.PROCESS_TYPE_DEFAULT)
@@ -477,21 +482,17 @@ var RemotePageManagerInternal = {
 
     if (!this.pages.has(url)) {
       throw new Error("Remote page is not registered: " + url);
     }
 
     // Notify all the frame scripts of the removed registration
     Services.ppmm.broadcastAsyncMessage("RemotePage:Unregister", { urls: [url] });
     this.pages.delete(url);
-  },
-
-  // A listener is requesting the list of currently registered urls
-  initListener({ target: messageManager }) {
-    messageManager.sendAsyncMessage("RemotePage:Register", { urls: Array.from(this.pages.keys()) })
+    this.updateProcessUrls();
   },
 
   // A remote page has been created and a port is ready in the content side
   initPort({ target: browser, data: { url, portID } }) {
     let callback = this.pages.get(url);
     if (!callback) {
       Cu.reportError("Unexpected remote page load: " + url);
       return;
@@ -507,17 +508,17 @@ if (Services.appinfo.processType == Ci.n
 
 // The public API for the above object
 this.RemotePageManager = {
   addRemotePageListener: RemotePageManagerInternal.addRemotePageListener.bind(RemotePageManagerInternal),
   removeRemotePageListener: RemotePageManagerInternal.removeRemotePageListener.bind(RemotePageManagerInternal),
 };
 
 // Listen for pages in any process we're loaded in
-var registeredURLs = new Set();
+var registeredURLs = new Set(Services.cpmm.initialProcessData["RemotePageManager:urls"]);
 
 var observer = (window) => {
   // Strip the hash from the URL, because it's not part of the origin.
   let url = window.document.documentURI.replace(/[\#|\?].*$/, "");
   if (!registeredURLs.has(url))
     return;
 
   // Get the frame message manager for this window so we can associate this
@@ -538,10 +539,8 @@ Services.cpmm.addMessageListener("Remote
     registeredURLs.add(url);
 });
 
 // A message from chrome telling us what pages to stop listening for
 Services.cpmm.addMessageListener("RemotePage:Unregister", ({ data }) => {
   for (let url of data.urls)
     registeredURLs.delete(url);
 });
-
-Services.cpmm.sendAsyncMessage("RemotePage:InitListener");
diff --git a/toolkit/modules/tests/browser/browser_RemotePageManager.js b/toolkit/modules/tests/browser/browser_RemotePageManager.js
--- a/toolkit/modules/tests/browser/browser_RemotePageManager.js
+++ b/toolkit/modules/tests/browser/browser_RemotePageManager.js
@@ -54,16 +54,30 @@ function swapDocShells(browser1, browser
   browser1.swapDocShells(browser2);
 
   // Swap permanentKeys.
   let tmp = browser1.permanentKey;
   browser1.permanentKey = browser2.permanentKey;
   browser2.permanentKey = tmp;
 }
 
+add_task(async function initialProcessData() {
+  const includesTest = () => Services.cpmm.
+    initialProcessData["RemotePageManager:urls"].includes(TEST_URL);
+  is(includesTest(), false, "Shouldn't have test url in initial process data yet");
+
+  const loadedPort = waitForPort(TEST_URL);
+  is(includesTest(), true, "Should have test url when waiting for it to load");
+
+  await loadedPort;
+  is(includesTest(), false, "Should have test url removed when done listening");
+
+  gBrowser.removeCurrentTab();
+});
+
 // Test that opening a page creates a port, sends the load event and then
 // navigating to a new page sends the unload event. Going back should create a
 // new port
 add_task(async function init_navigate() {
   let port = await waitForPort(TEST_URL);
   is(port.browser, gBrowser.selectedBrowser, "Port is for the correct browser");
 
   let loaded = new Promise(resolve => {
