# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1504731517 14400
# Node ID 9080c5fb07696d23b40d94c1100ec188cc99212d
# Parent  837aaf616d8543ced7aed4ffc98d742c835d30cf
Bug 1362449 - part 5 - add tables for normal base64 decoding; r=erahm

These tables are nearly identical to the ones for base64url decoding,
but ideally will be slightly more readable, since things are broken up
into sets of eight entries at a time.

diff --git a/xpcom/io/Base64.cpp b/xpcom/io/Base64.cpp
--- a/xpcom/io/Base64.cpp
+++ b/xpcom/io/Base64.cpp
@@ -1,16 +1,17 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "Base64.h"
 
+#include "mozilla/ArrayUtils.h"
 #include "mozilla/ScopeExit.h"
 #include "mozilla/UniquePtrExtensions.h"
 #include "nsIInputStream.h"
 #include "nsString.h"
 #include "nsTArray.h"
 
 #include "plbase64.h"
 
@@ -236,16 +237,59 @@ EncodeInputStream(nsIInputStream* aInput
     // May belong to an nsCString with an unallocated buffer, so only null
     // terminate if there is a need to.
     *aDest.EndWriting() = '\0';
   }
 
   return NS_OK;
 }
 
+// Maps an encoded character to a value in the Base64 alphabet, per
+// RFC 4648, Table 1. Invalid input characters map to UINT8_MAX.
+static const uint8_t kBase64DecodeTable[] = {
+  /* 0 */  255, 255, 255, 255, 255, 255, 255, 255,
+  /* 8 */  255, 255, 255, 255, 255, 255, 255, 255,
+  /* 16 */ 255, 255, 255, 255, 255, 255, 255, 255,
+  /* 24 */ 255, 255, 255, 255, 255, 255, 255, 255,
+  /* 32 */ 255, 255, 255, 255, 255, 255, 255, 255,
+  /* 40 */ 255, 255, 255,
+  62 /* + */,
+  255, 255, 255,
+  63 /* / */,
+
+  /* 48 */ /* 0 - 9 */ 52, 53, 54, 55, 56, 57, 58, 59,
+  /* 56 */ 60, 61, 255, 255, 255, 255, 255, 255,
+
+  /* 64 */ 255, /* A - Z */ 0, 1, 2, 3, 4, 5, 6,
+  /* 72 */ 7, 8, 9, 10, 11, 12, 13, 14,
+  /* 80 */ 15, 16, 17, 18, 19, 20, 21, 22,
+  /* 88 */ 23, 24, 25, 255, 255, 255, 255, 255,
+  /* 96 */ 255, /* a - z */ 26, 27, 28, 29, 30, 31, 32,
+  /* 104 */ 33, 34, 35, 36, 37, 38, 39, 40,
+  /* 112 */ 41, 42, 43, 44, 45, 46, 47, 48,
+  /* 120 */ 49, 50, 51, 255, 255, 255, 255, 255,
+};
+
+template<typename T>
+MOZ_MUST_USE bool
+Base64CharToValue(T aChar, uint8_t* aValue)
+{
+  static const size_t mask = 0x7f;
+  static_assert((mask + 1) == sizeof(kBase64DecodeTable)/sizeof(kBase64DecodeTable[0]),
+                "wrong mask");
+  size_t index = static_cast<uint8_t>(aChar);
+
+  if (index & ~mask) {
+    return false;
+  }
+  *aValue = kBase64DecodeTable[index & mask];
+
+  return *aValue != 255;
+}
+
 static const char kBase64URLAlphabet[] =
   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
 
 // Maps an encoded character to a value in the Base64 URL alphabet, per
 // RFC 4648, Table 2. Invalid input characters map to UINT8_MAX.
 static const uint8_t kBase64URLDecodeTable[] = {
   255, 255, 255, 255, 255, 255, 255, 255,
   255, 255, 255, 255, 255, 255, 255, 255,

