# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1520458134 28800
#      Wed Mar 07 13:28:54 2018 -0800
# Node ID e1641b8d914d5cadcf9716b8dcaa6a08745fed6c
# Parent  139a283ba81046e94db75f49c56b16e55eb98219
Bug 1444129, part 1 - Rename fields of XPTHeader. r=njn

MozReview-Commit-ID: COPpBBEbVbC

diff --git a/xpcom/reflect/xptinfo/xptiInterfaceInfoManager.cpp b/xpcom/reflect/xptinfo/xptiInterfaceInfoManager.cpp
--- a/xpcom/reflect/xptinfo/xptiInterfaceInfoManager.cpp
+++ b/xpcom/reflect/xptinfo/xptiInterfaceInfoManager.cpp
@@ -113,25 +113,25 @@ XPTInterfaceInfoManager::RegisterBuffer(
     if (XPT_DoHeader(gXPTIStructArena, cursor, &header)) {
         RegisterXPTHeader(header);
     }
 }
 
 void
 XPTInterfaceInfoManager::RegisterXPTHeader(const XPTHeader* aHeader)
 {
-    if (aHeader->major_version >= XPT_MAJOR_INCOMPATIBLE_VERSION) {
-        NS_ASSERTION(!aHeader->num_interfaces,"bad libxpt");
+    if (aHeader->mMajorVersion >= XPT_MAJOR_INCOMPATIBLE_VERSION) {
+        MOZ_ASSERT(!aHeader->mNumInterfaces, "bad libxpt");
     }
 
     xptiTypelibGuts* typelib = xptiTypelibGuts::Create(aHeader);
 
     ReentrantMonitorAutoEnter monitor(mWorkingSet.mTableReentrantMonitor);
-    for(uint16_t k = 0; k < aHeader->num_interfaces; k++)
-        VerifyAndAddEntryIfNew(aHeader->interface_directory + k, k, typelib);
+    for(uint16_t k = 0; k < aHeader->mNumInterfaces; k++)
+        VerifyAndAddEntryIfNew(aHeader->mInterfaceDirectory + k, k, typelib);
 }
 
 void
 XPTInterfaceInfoManager::VerifyAndAddEntryIfNew(const XPTInterfaceDirectoryEntry* iface,
                                                 uint16_t idx,
                                                 xptiTypelibGuts* typelib)
 {
     if (!iface->interface_descriptor)
diff --git a/xpcom/reflect/xptinfo/xptiTypelibGuts.cpp b/xpcom/reflect/xptinfo/xptiTypelibGuts.cpp
--- a/xpcom/reflect/xptinfo/xptiTypelibGuts.cpp
+++ b/xpcom/reflect/xptinfo/xptiTypelibGuts.cpp
@@ -19,17 +19,17 @@ class MOZ_NEEDS_NO_VTABLE_TYPE CheckNoVT
 CheckNoVTable<xptiTypelibGuts> gChecker;
 
 // static
 xptiTypelibGuts*
 xptiTypelibGuts::Create(const XPTHeader* aHeader)
 {
     NS_ASSERTION(aHeader, "bad param");
     size_t n = sizeof(xptiTypelibGuts) +
-               sizeof(xptiInterfaceEntry*) * (aHeader->num_interfaces - 1);
+               sizeof(xptiInterfaceEntry*) * (aHeader->mNumInterfaces - 1);
     void* place = XPT_CALLOC8(gXPTIStructArena, n);
     if (!place)
         return nullptr;
     return new(place) xptiTypelibGuts(aHeader);
 }
 
 xptiInterfaceEntry*
 xptiTypelibGuts::GetEntryAt(uint16_t i)
@@ -39,17 +39,17 @@ xptiTypelibGuts::GetEntryAt(uint16_t i)
 
     NS_ASSERTION(mHeader, "bad state");
     NS_ASSERTION(i < GetEntryCount(), "bad index");
 
     xptiInterfaceEntry* r = mEntryArray[i];
     if (r)
         return r;
 
-    const XPTInterfaceDirectoryEntry* iface = mHeader->interface_directory + i;
+    const XPTInterfaceDirectoryEntry* iface = mHeader->mInterfaceDirectory + i;
 
     XPTInterfaceInfoManager::xptiWorkingSet& set =
         XPTInterfaceInfoManager::GetSingleton()->mWorkingSet;
 
     {
         ReentrantMonitorAutoEnter monitor(set.mTableReentrantMonitor);
         if (iface->iid.Equals(zeroIID))
             r = set.mNameTable.Get(iface->name);
@@ -64,12 +64,12 @@ xptiTypelibGuts::GetEntryAt(uint16_t i)
 }
 
 const char*
 xptiTypelibGuts::GetEntryNameAt(uint16_t i)
 {
     NS_ASSERTION(mHeader, "bad state");
     NS_ASSERTION(i < GetEntryCount(), "bad index");
 
-    const XPTInterfaceDirectoryEntry* iface = mHeader->interface_directory + i;
+    const XPTInterfaceDirectoryEntry* iface = mHeader->mInterfaceDirectory + i;
 
     return iface->name;
 }
diff --git a/xpcom/reflect/xptinfo/xptiprivate.h b/xpcom/reflect/xptinfo/xptiprivate.h
--- a/xpcom/reflect/xptinfo/xptiprivate.h
+++ b/xpcom/reflect/xptinfo/xptiprivate.h
@@ -72,17 +72,17 @@ extern XPTArena* gXPTIStructArena;
 // These are always constructed in the struct arena using placement new.
 // dtor need not be called.
 
 class xptiTypelibGuts
 {
 public:
     static xptiTypelibGuts* Create(const XPTHeader* aHeader);
 
-    uint16_t            GetEntryCount() const {return mHeader->num_interfaces;}
+    uint16_t GetEntryCount() const {return mHeader->mNumInterfaces;}
 
     void                SetEntryAt(uint16_t i, xptiInterfaceEntry* ptr)
     {
         NS_ASSERTION(mHeader,"bad state!");
         NS_ASSERTION(i < GetEntryCount(),"bad param!");
         mEntryArray[i] = ptr;
     }
 
diff --git a/xpcom/typelib/xpt/xpt_struct.cpp b/xpcom/typelib/xpt/xpt_struct.cpp
--- a/xpcom/typelib/xpt/xpt_struct.cpp
+++ b/xpcom/typelib/xpt/xpt_struct.cpp
@@ -105,30 +105,30 @@ XPT_DoHeader(XPTArena *arena, NotNull<XP
         fprintf(stderr,
                 "libxpt: bad magic header in input file; "
                 "found '%s', expected '%s'\n",
                 magic, XPT_MAGIC_STRING);
         return false;
     }
 
     uint8_t minor_version;
-    if (!XPT_Do8(cursor, &header->major_version) ||
+    if (!XPT_Do8(cursor, &header->mMajorVersion) ||
         !XPT_Do8(cursor, &minor_version)) {
         return false;
     }
 
-    if (header->major_version >= XPT_MAJOR_INCOMPATIBLE_VERSION) {
+    if (header->mMajorVersion >= XPT_MAJOR_INCOMPATIBLE_VERSION) {
         /* This file is newer than we are and set to an incompatible version
          * number. We must set the header state thusly and return.
          */
-        header->num_interfaces = 0;
+        header->mNumInterfaces = 0;
         return true;
     }
 
-    if (!XPT_Do16(cursor, &header->num_interfaces) ||
+    if (!XPT_Do16(cursor, &header->mNumInterfaces) ||
         !XPT_Do32(cursor, &file_length) ||
         !XPT_Do32(cursor, &ide_offset)) {
         return false;
     }
 
     /*
      * Make sure the file length reported in the header is the same size as
      * as our buffer unless it is zero (not set)
@@ -143,18 +143,18 @@ XPT_DoHeader(XPTArena *arena, NotNull<XP
     uint32_t data_pool;
     if (!XPT_Do32(cursor, &data_pool))
         return false;
 
     XPT_SetDataOffset(cursor->state, data_pool);
 
     XPTInterfaceDirectoryEntry* interface_directory = nullptr;
 
-    if (header->num_interfaces) {
-        size_t n = header->num_interfaces * sizeof(XPTInterfaceDirectoryEntry);
+    if (header->mNumInterfaces) {
+        size_t n = header->mNumInterfaces * sizeof(XPTInterfaceDirectoryEntry);
         interface_directory =
             static_cast<XPTInterfaceDirectoryEntry*>(XPT_CALLOC8(arena, n));
         if (!interface_directory)
             return false;
     }
 
     /*
      * Iterate through the annotations rather than recurring, to avoid blowing
@@ -165,23 +165,23 @@ XPT_DoHeader(XPTArena *arena, NotNull<XP
     do {
         if (!SkipAnnotation(cursor, &isLast))
             return false;
     } while (!isLast);
 
     /* shouldn't be necessary now, but maybe later */
     XPT_SeekTo(cursor, ide_offset);
 
-    for (i = 0; i < header->num_interfaces; i++) {
+    for (i = 0; i < header->mNumInterfaces; i++) {
         if (!DoInterfaceDirectoryEntry(arena, cursor,
                                        &interface_directory[i]))
             return false;
     }
 
-    header->interface_directory = interface_directory;
+    header->mInterfaceDirectory = interface_directory;
 
     return true;
 }
 
 /* InterfaceDirectoryEntry records go in the header */
 bool
 DoInterfaceDirectoryEntry(XPTArena *arena, NotNull<XPTCursor*> cursor,
                           XPTInterfaceDirectoryEntry *ide)
diff --git a/xpcom/typelib/xpt/xpt_struct.h b/xpcom/typelib/xpt/xpt_struct.h
--- a/xpcom/typelib/xpt/xpt_struct.h
+++ b/xpcom/typelib/xpt/xpt_struct.h
@@ -38,40 +38,40 @@ struct XPTTypeDescriptorPrefix;
 /*
  * Every XPCOM typelib file begins with a header.
  */
 struct XPTHeader {
   // Some of these fields exists in the on-disk format but don't need to be
   // stored in memory (other than very briefly, which can be done with local
   // variables).
 
-  //uint8_t magic[16];
-  uint8_t major_version;
-  //uint8_t minor_version;
-  uint16_t num_interfaces;
-  //uint32_t file_length;
-  const XPTInterfaceDirectoryEntry* interface_directory;
-  //uint32_t data_pool;
+  //uint8_t mMagic[16];
+  uint8_t mMajorVersion;
+  //uint8_t mMinorVersion;
+  uint16_t mNumInterfaces;
+  //uint32_t mFileLength;
+  const XPTInterfaceDirectoryEntry* mInterfaceDirectory;
+  //uint32_t mDataPool;
 };
 
 /*
  * Any file with a major version number of XPT_MAJOR_INCOMPATIBLE_VERSION
  * or higher is to be considered incompatible by this version of xpt and
  * we will refuse to read it. We will return a header with magic, major and
  * minor versions set from the file. num_interfaces will be set to zero to
  * confirm our inability to read the file; i.e. even if some client of this
  * library gets out of sync with us regarding the agreed upon value for
  * XPT_MAJOR_INCOMPATIBLE_VERSION, anytime num_interfaces is zero we *know*
  * that this library refused to read the file due to version incompatibility.
  */
 #define XPT_MAJOR_INCOMPATIBLE_VERSION 0x02
 
 /*
  * A contiguous array of fixed-size InterfaceDirectoryEntry records begins at
- * the byte offset identified by the interface_directory field in the file
+ * the byte offset identified by the mInterfaceDirectory field in the file
  * header.  The array is used to quickly locate an interface description
  * using its IID.  No interface should appear more than once in the array.
  */
 struct XPTInterfaceDirectoryEntry {
   nsID iid;
   const char* name;
 
   // This field exists in the on-disk format. But it isn't used so we don't
