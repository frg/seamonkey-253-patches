# HG changeset patch
# User Eden Chuang <echuang@mozilla.com>
# Date 1504679784 -28800
#      Wed Sep 06 14:36:24 2017 +0800
# Node ID f18ee8840162b9e1a19946eac2735d6bc30f1efd
# Parent  b7b9092b107de8f4aa59cb3933a19c2fca97c52d
Bug 1388661 - Support currency validation in PaymentRequet API. r=baku

    This patch implements currency validation algorithm according to the spec
    https://w3c.github.io/payment-request/#validity-checkers.

    1. amount.currencySystem must be "urn:iso:std:iso:4217".
    2. amount.currency is valid with following criteria
       1. The currency length must be 3.
       2. The currency contains any character that must be in the range "A" to
          "Z"(U+0041 to U+005A) or the range "a" to "z"(U+0061 to U+007A).

    According to the spec, converting the currency to upper case and save it in
    nsIPaymentRequest.

diff --git a/dom/bindings/Errors.msg b/dom/bindings/Errors.msg
--- a/dom/bindings/Errors.msg
+++ b/dom/bindings/Errors.msg
@@ -28,17 +28,18 @@ MSG_DEF(MSG_NOT_OBJECT, 1, JSEXN_TYPEERR
 MSG_DEF(MSG_NOT_CALLABLE, 1, JSEXN_TYPEERR, "{0} is not callable.")
 MSG_DEF(MSG_NOT_CONSTRUCTOR, 1, JSEXN_TYPEERR, "{0} is not a constructor.")
 MSG_DEF(MSG_DOES_NOT_IMPLEMENT_INTERFACE, 2, JSEXN_TYPEERR, "{0} does not implement interface {1}.")
 MSG_DEF(MSG_METHOD_THIS_DOES_NOT_IMPLEMENT_INTERFACE, 2, JSEXN_TYPEERR, "'{0}' called on an object that does not implement interface {1}.")
 MSG_DEF(MSG_METHOD_THIS_UNWRAPPING_DENIED, 1, JSEXN_TYPEERR, "Permission to call '{0}' denied.")
 MSG_DEF(MSG_THIS_DOES_NOT_IMPLEMENT_INTERFACE, 1, JSEXN_TYPEERR, "\"this\" object does not implement interface {0}.")
 MSG_DEF(MSG_NOT_IN_UNION, 2, JSEXN_TYPEERR, "{0} could not be converted to any of: {1}.")
 MSG_DEF(MSG_ILLEGAL_CONSTRUCTOR, 0, JSEXN_TYPEERR, "Illegal constructor.")
-MSG_DEF(MSG_ILLEGAL_PR_CONSTRUCTOR, 1, JSEXN_TYPEERR, "TypeError:{0}")
+MSG_DEF(MSG_ILLEGAL_TYPE_PR_CONSTRUCTOR, 1, JSEXN_TYPEERR, "TypeError:{0}")
+MSG_DEF(MSG_ILLEGAL_RANGE_PR_CONSTRUCTOR, 1, JSEXN_RANGEERR, "RangeError:{0}")
 MSG_DEF(MSG_CONSTRUCTOR_WITHOUT_NEW, 1, JSEXN_TYPEERR, "Constructor {0} requires 'new'")
 MSG_DEF(MSG_ENFORCE_RANGE_NON_FINITE, 1, JSEXN_TYPEERR, "Non-finite value is out of range for {0}.")
 MSG_DEF(MSG_ENFORCE_RANGE_OUT_OF_RANGE, 1, JSEXN_TYPEERR, "Value is out of range for {0}.")
 MSG_DEF(MSG_NOT_SEQUENCE, 1, JSEXN_TYPEERR, "{0} can't be converted to a sequence.")
 MSG_DEF(MSG_NOT_DICTIONARY, 1, JSEXN_TYPEERR, "{0} can't be converted to a dictionary.")
 MSG_DEF(MSG_OVERLOAD_RESOLUTION_FAILED, 3, JSEXN_TYPEERR, "Argument {0} is not valid for any of the {1}-argument overloads of {2}.")
 MSG_DEF(MSG_GLOBAL_NOT_NATIVE, 0, JSEXN_TYPEERR, "Global is not a native object.")
 MSG_DEF(MSG_ENCODING_NOT_SUPPORTED, 1, JSEXN_RANGEERR, "The given encoding '{0}' is not supported.")
diff --git a/dom/payments/PaymentRequest.cpp b/dom/payments/PaymentRequest.cpp
--- a/dom/payments/PaymentRequest.cpp
+++ b/dom/payments/PaymentRequest.cpp
@@ -47,51 +47,51 @@ NS_IMPL_ADDREF_INHERITED(PaymentRequest,
 NS_IMPL_RELEASE_INHERITED(PaymentRequest, DOMEventTargetHelper)
 
 bool
 PaymentRequest::PrefEnabled(JSContext* aCx, JSObject* aObj)
 {
   return Preferences::GetBool("dom.payments.request.enabled");
 }
 
-bool
+nsresult
 PaymentRequest::IsValidMethodData(JSContext* aCx,
                                   const Sequence<PaymentMethodData>& aMethodData,
                                   nsAString& aErrorMsg)
 {
   if (!aMethodData.Length()) {
     aErrorMsg.AssignLiteral("At least one payment method is required.");
-    return false;
+    return NS_ERROR_TYPE_ERR;
   }
 
   for (const PaymentMethodData& methodData : aMethodData) {
     if (methodData.mSupportedMethods.IsEmpty()) {
       aErrorMsg.AssignLiteral(
         "Payment method identifier is required.");
-      return false;
+      return NS_ERROR_TYPE_ERR;
     }
     RefPtr<BasicCardService> service = BasicCardService::GetService();
     MOZ_ASSERT(service);
     if (service->IsBasicCardPayment(methodData.mSupportedMethods)) {
       if (!methodData.mData.WasPassed()) {
         continue;
       }
       MOZ_ASSERT(aCx);
       if (!service->IsValidBasicCardRequest(aCx,
                                             methodData.mData.Value(),
                                             aErrorMsg)) {
-        return false;
+        return NS_ERROR_TYPE_ERR;
       }
     }
   }
 
-  return true;
+  return NS_OK;
 }
 
-bool
+nsresult
 PaymentRequest::IsValidNumber(const nsAString& aItem,
                               const nsAString& aStr,
                               nsAString& aErrorMsg)
 {
   nsresult error = NS_ERROR_FAILURE;
 
   if (!aStr.IsEmpty()) {
     nsAutoString aValue(aStr);
@@ -115,22 +115,22 @@ PaymentRequest::IsValidNumber(const nsAS
   }
 
   if (NS_FAILED(error)) {
     aErrorMsg.AssignLiteral("The amount.value of \"");
     aErrorMsg.Append(aItem);
     aErrorMsg.AppendLiteral("\"(");
     aErrorMsg.Append(aStr);
     aErrorMsg.AppendLiteral(") must be a valid decimal monetary value.");
-    return false;
+    return NS_ERROR_TYPE_ERR;
   }
-  return true;
+  return NS_OK;
 }
 
-bool
+nsresult
 PaymentRequest::IsNonNegativeNumber(const nsAString& aItem,
                                     const nsAString& aStr,
                                     nsAString& aErrorMsg)
 {
   nsresult error = NS_ERROR_FAILURE;
 
   if (!aStr.IsEmpty()) {
     nsAutoString aValue(aStr);
@@ -144,93 +144,181 @@ PaymentRequest::IsNonNegativeNumber(cons
     }
   }
 
   if (NS_FAILED(error)) {
     aErrorMsg.AssignLiteral("The amount.value of \"");
     aErrorMsg.Append(aItem);
     aErrorMsg.AppendLiteral("\"(");
     aErrorMsg.Append(aStr);
-    aErrorMsg.AppendLiteral(") must be a valid and non-negative decimal monetaryvalue.");
-    return false;
+    aErrorMsg.AppendLiteral(") must be a valid and non-negative decimal monetary value.");
+    return NS_ERROR_TYPE_ERR;
   }
-  return true;
+  return NS_OK;
+}
+
+nsresult
+PaymentRequest::IsValidCurrency(const nsAString& aItem,
+                                const nsAString& aCurrency,
+                                nsAString& aErrorMsg)
+{
+   /*
+    *  According to spec in https://w3c.github.io/payment-request/#validity-checkers,
+    *  perform currency validation with following criteria
+    *  1. The currency length must be 3.
+    *  2. The currency contains any character that must be in the range "A" to "Z"
+    *     (U+0041 to U+005A) or the range "a" to "z" (U+0061 to U+007A)
+    */
+   if (aCurrency.Length() != 3) {
+     aErrorMsg.AssignLiteral("The length amount.currency of \"");
+     aErrorMsg.Append(aItem);
+     aErrorMsg.AppendLiteral("\"(");
+     aErrorMsg.Append(aCurrency);
+     aErrorMsg.AppendLiteral(") must be 3.");
+     return NS_ERROR_RANGE_ERR;
+   }
+   // Don't use nsUnicharUtils::ToUpperCase, it converts the invalid "ınr" PMI to
+   // to the valid one "INR".
+   for (uint32_t idx = 0; idx < aCurrency.Length(); ++idx) {
+     if ((aCurrency.CharAt(idx) >= 'A' && aCurrency.CharAt(idx) <= 'Z') ||
+         (aCurrency.CharAt(idx) >= 'a' && aCurrency.CharAt(idx) <= 'z')) {
+       continue;
+     }
+     aErrorMsg.AssignLiteral("The character amount.currency of \"");
+     aErrorMsg.Append(aItem);
+     aErrorMsg.AppendLiteral("\"(");
+     aErrorMsg.Append(aCurrency);
+     aErrorMsg.AppendLiteral(") must be in the range 'A' to 'Z'(U+0041 to U+005A) or 'a' to 'z'(U+0061 to U+007A).");
+     return NS_ERROR_RANGE_ERR;
+   }
+   return NS_OK;
 }
 
-bool
+nsresult
+PaymentRequest::IsValidCurrencyAmount(const nsAString& aItem,
+                                      const PaymentCurrencyAmount& aAmount,
+                                      const bool aIsTotalItem,
+                                      nsAString& aErrorMsg)
+{
+  nsresult rv;
+  if (aIsTotalItem) {
+    rv = IsNonNegativeNumber(aItem, aAmount.mValue, aErrorMsg);
+    if (NS_FAILED(rv)) {
+      return rv;
+    }
+  } else {
+    rv = IsValidNumber(aItem, aAmount.mValue, aErrorMsg);
+    if (NS_FAILED(rv)) {
+      return rv;
+    }
+  }
+  // currencySystem must equal urn:iso:std:iso:4217
+  if (!aAmount.mCurrencySystem.EqualsASCII("urn:iso:std:iso:4217")) {
+    aErrorMsg.AssignLiteral("The amount.currencySystem of \"");
+    aErrorMsg.Append(aItem);
+    aErrorMsg.AppendLiteral("\"(");
+    aErrorMsg.Append(aAmount.mCurrencySystem);
+    aErrorMsg.AppendLiteral(") must equal urn:iso:std:iso:4217.");
+    return NS_ERROR_RANGE_ERR;
+  }
+  rv = IsValidCurrency(aItem, aAmount.mCurrency, aErrorMsg);
+  if (NS_FAILED(rv)) {
+    return rv;
+  }
+  return NS_OK;
+}
+
+nsresult
 PaymentRequest::IsValidDetailsInit(const PaymentDetailsInit& aDetails, nsAString& aErrorMsg)
 {
-  // Check the amount.value of detail.total
-  if (!IsNonNegativeNumber(NS_LITERAL_STRING("details.total"),
-                           aDetails.mTotal.mAmount.mValue, aErrorMsg)) {
-    return false;
+  // Check the amount.value and amount.currency of detail.total
+  nsresult rv = IsValidCurrencyAmount(NS_LITERAL_STRING("details.total"),
+                                      aDetails.mTotal.mAmount,
+                                      true, // isTotalItem
+                                      aErrorMsg);
+  if (NS_FAILED(rv)) {
+    return rv;
   }
-
   return IsValidDetailsBase(aDetails, aErrorMsg);
 }
 
-bool
+nsresult
 PaymentRequest::IsValidDetailsUpdate(const PaymentDetailsUpdate& aDetails)
 {
   nsAutoString message;
-  // Check the amount.value of detail.total
-  if (!IsNonNegativeNumber(NS_LITERAL_STRING("details.total"),
-                           aDetails.mTotal.mAmount.mValue, message)) {
-    return false;
+  // Check the amount.value and amount.currency of detail.total
+  nsresult rv = IsValidCurrencyAmount(NS_LITERAL_STRING("details.total"),
+                                      aDetails.mTotal.mAmount,
+                                      true, // isTotalItem
+                                      message);
+  if (NS_FAILED(rv)) {
+    return rv;
   }
-
   return IsValidDetailsBase(aDetails, message);
 }
 
-bool
+nsresult
 PaymentRequest::IsValidDetailsBase(const PaymentDetailsBase& aDetails, nsAString& aErrorMsg)
 {
+  nsresult rv;
   // Check the amount.value of each item in the display items
   if (aDetails.mDisplayItems.WasPassed()) {
     const Sequence<PaymentItem>& displayItems = aDetails.mDisplayItems.Value();
     for (const PaymentItem& displayItem : displayItems) {
-      if (!IsValidNumber(displayItem.mLabel,
-                         displayItem.mAmount.mValue, aErrorMsg)) {
-        return false;
+      rv = IsValidCurrencyAmount(displayItem.mLabel,
+                                 displayItem.mAmount,
+                                 false,  // isTotalItem
+                                 aErrorMsg);
+      if (NS_FAILED(rv)) {
+        return rv;
       }
     }
   }
 
   // Check the shipping option
   if (aDetails.mShippingOptions.WasPassed()) {
     const Sequence<PaymentShippingOption>& shippingOptions = aDetails.mShippingOptions.Value();
     for (const PaymentShippingOption& shippingOption : shippingOptions) {
-      if (!IsValidNumber(NS_LITERAL_STRING("details.shippingOptions"),
-                         shippingOption.mAmount.mValue, aErrorMsg)) {
-        return false;
+      rv = IsValidCurrencyAmount(NS_LITERAL_STRING("details.shippingOptions"),
+                                 shippingOption.mAmount,
+                                 false,  // isTotalItem
+                                 aErrorMsg);
+      if (NS_FAILED(rv)) {
+        return rv;
       }
     }
   }
 
   // Check payment details modifiers
   if (aDetails.mModifiers.WasPassed()) {
     const Sequence<PaymentDetailsModifier>& modifiers = aDetails.mModifiers.Value();
     for (const PaymentDetailsModifier& modifier : modifiers) {
-      if (!IsNonNegativeNumber(NS_LITERAL_STRING("details.modifiers.total"),
-                               modifier.mTotal.mAmount.mValue, aErrorMsg)) {
-        return false;
+      rv = IsValidCurrencyAmount(NS_LITERAL_STRING("details.modifiers.total"),
+                                 modifier.mTotal.mAmount,
+                                 true, // isTotalItem
+                                 aErrorMsg);
+      if (NS_FAILED(rv)) {
+        return rv;
       }
       if (modifier.mAdditionalDisplayItems.WasPassed()) {
         const Sequence<PaymentItem>& displayItems = modifier.mAdditionalDisplayItems.Value();
         for (const PaymentItem& displayItem : displayItems) {
-          if (!IsValidNumber(displayItem.mLabel,
-                             displayItem.mAmount.mValue, aErrorMsg)) {
-            return false;
+          rv = IsValidCurrencyAmount(displayItem.mLabel,
+                                     displayItem.mAmount,
+                                     false,  // isTotalItem
+                                     aErrorMsg);
+          if (NS_FAILED(rv)) {
+            return rv;
           }
         }
       }
     }
   }
 
-  return true;
+  return NS_OK;
 }
 
 already_AddRefed<PaymentRequest>
 PaymentRequest::Constructor(const GlobalObject& aGlobal,
                             const Sequence<PaymentMethodData>& aMethodData,
                             const PaymentDetailsInit& aDetails,
                             const PaymentOptions& aOptions,
                             ErrorResult& aRv)
@@ -272,33 +360,42 @@ PaymentRequest::Constructor(const Global
       }
     }
     topLevelPrincipal = node->NodePrincipal();
     node = parentNode;
   } while (node);
 
   // Check payment methods and details
   nsAutoString message;
-  if (!IsValidMethodData(nsContentUtils::GetCurrentJSContext(),
-                         aMethodData,
-                         message) ||
-      !IsValidDetailsInit(aDetails, message)) {
-    aRv.ThrowTypeError<MSG_ILLEGAL_PR_CONSTRUCTOR>(message);
+  nsresult rv = IsValidMethodData(aGlobal.Context(),
+                                  aMethodData,
+                                  message);
+  if (NS_FAILED(rv)) {
+    aRv.ThrowTypeError<MSG_ILLEGAL_TYPE_PR_CONSTRUCTOR>(message);
+    return nullptr;
+  }
+  rv = IsValidDetailsInit(aDetails, message);
+  if (NS_FAILED(rv)) {
+    if (rv == NS_ERROR_TYPE_ERR) {
+      aRv.ThrowTypeError<MSG_ILLEGAL_TYPE_PR_CONSTRUCTOR>(message);
+    } else if (rv == NS_ERROR_RANGE_ERR) {
+      aRv.ThrowRangeError<MSG_ILLEGAL_RANGE_PR_CONSTRUCTOR>(message);
+    }
     return nullptr;
   }
 
   RefPtr<PaymentRequestManager> manager = PaymentRequestManager::GetSingleton();
   if (NS_WARN_IF(!manager)) {
     return nullptr;
   }
 
   // Create PaymentRequest and set its |mId|
   RefPtr<PaymentRequest> request;
-  nsresult rv = manager->CreatePayment(window, topLevelPrincipal, aMethodData,
-                                       aDetails, aOptions, getter_AddRefs(request));
+  rv = manager->CreatePayment(aGlobal.Context(), window, topLevelPrincipal, aMethodData,
+                              aDetails, aOptions, getter_AddRefs(request));
   if (NS_WARN_IF(NS_FAILED(rv))) {
     aRv.Throw(NS_ERROR_DOM_TYPE_ERR);
     return nullptr;
   }
 
   return request.forget();
 }
 
@@ -509,16 +606,18 @@ PaymentRequest::RespondAbortPayment(bool
   // - If |mUpdateError| is not NS_OK, we are aborting the update as
   //   |mUpdateError| was set in method |AbortUpdate|.
   //   => Reject |mAcceptPromise| and reset |mUpdateError| to complete
   //      the action, regardless of |aSuccess|.
   //
   // - Otherwise, we are handling |Abort| method call from merchant.
   //   => Resolve/Reject |mAbortPromise| based on |aSuccess|.
   if (NS_FAILED(mUpdateError)) {
+    // Respond show with mUpdateError, set mUpdating to false.
+    mUpdating = false;
     RespondShowPayment(false, EmptyString(), EmptyString(), EmptyString(),
                        EmptyString(), EmptyString(), mUpdateError);
     mUpdateError = NS_OK;
     return;
   }
 
   MOZ_ASSERT(mAbortPromise);
   MOZ_ASSERT(mState == eInteractive);
@@ -529,23 +628,24 @@ PaymentRequest::RespondAbortPayment(bool
     RejectShowPayment(NS_ERROR_DOM_ABORT_ERR);
   } else {
     mAbortPromise->MaybeReject(NS_ERROR_DOM_INVALID_STATE_ERR);
     mAbortPromise = nullptr;
   }
 }
 
 nsresult
-PaymentRequest::UpdatePayment(const PaymentDetailsUpdate& aDetails)
+PaymentRequest::UpdatePayment(JSContext* aCx, const PaymentDetailsUpdate& aDetails)
 {
+  NS_ENSURE_ARG_POINTER(aCx);
   RefPtr<PaymentRequestManager> manager = PaymentRequestManager::GetSingleton();
   if (NS_WARN_IF(!manager)) {
     return NS_ERROR_FAILURE;
   }
-  nsresult rv = manager->UpdatePayment(mInternalId, aDetails);
+  nsresult rv = manager->UpdatePayment(aCx, mInternalId, aDetails);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
   return NS_OK;
 }
 
 void
 PaymentRequest::AbortUpdate(nsresult aRv)
diff --git a/dom/payments/PaymentRequest.h b/dom/payments/PaymentRequest.h
--- a/dom/payments/PaymentRequest.h
+++ b/dom/payments/PaymentRequest.h
@@ -31,37 +31,48 @@ public:
   virtual JSObject* WrapObject(JSContext* aCx,
                                JS::Handle<JSObject*> aGivenProto) override;
 
   static already_AddRefed<PaymentRequest>
   CreatePaymentRequest(nsPIDOMWindowInner* aWindow, nsresult& aRv);
 
   static bool PrefEnabled(JSContext* aCx, JSObject* aObj);
 
-  static bool IsValidMethodData(JSContext* aCx,
-                                const Sequence<PaymentMethodData>& aMethodData,
-                                nsAString& aErrorMsg);
+  static nsresult IsValidMethodData(JSContext* aCx,
+                                    const Sequence<PaymentMethodData>& aMethodData,
+                                    nsAString& aErrorMsg);
 
-  static bool
+  static nsresult
   IsValidNumber(const nsAString& aItem,
                 const nsAString& aStr,
                 nsAString& aErrorMsg);
-  static bool
+  static nsresult
   IsNonNegativeNumber(const nsAString& aItem,
                       const nsAString& aStr,
                       nsAString& aErrorMsg);
 
-  static bool
+  static nsresult
+  IsValidCurrencyAmount(const nsAString& aItem,
+                        const PaymentCurrencyAmount& aAmount,
+                        const bool aIsTotalItem,
+                        nsAString& aErrorMsg);
+
+  static nsresult
+  IsValidCurrency(const nsAString& aItem,
+                  const nsAString& aCurrency,
+                  nsAString& aErrorMsg);
+
+  static nsresult
   IsValidDetailsInit(const PaymentDetailsInit& aDetails,
                      nsAString& aErrorMsg);
 
-  static bool
+  static nsresult
   IsValidDetailsUpdate(const PaymentDetailsUpdate& aDetails);
 
-  static bool
+  static nsresult
   IsValidDetailsBase(const PaymentDetailsBase& aDetails,
                      nsAString& aErrorMsg);
 
   static already_AddRefed<PaymentRequest>
   Constructor(const GlobalObject& aGlobal,
               const Sequence<PaymentMethodData>& aMethodData,
               const PaymentDetailsInit& aDetails,
               const PaymentOptions& aOptions,
@@ -107,17 +118,17 @@ public:
                                  const nsAString& aRecipient,
                                  const nsAString& aPhone);
 
 
   void SetShippingOption(const nsAString& aShippingOption);
   void GetShippingOption(nsAString& aRetVal) const;
   nsresult UpdateShippingOption(const nsAString& aShippingOption);
 
-  nsresult UpdatePayment(const PaymentDetailsUpdate& aDetails);
+  nsresult UpdatePayment(JSContext* aCx, const PaymentDetailsUpdate& aDetails);
   void AbortUpdate(nsresult aRv);
 
   void SetShippingType(const Nullable<PaymentShippingType>& aShippingType);
   Nullable<PaymentShippingType> GetShippingType() const;
 
   IMPL_EVENT_HANDLER(shippingaddresschange);
   IMPL_EVENT_HANDLER(shippingoptionchange);
 
diff --git a/dom/payments/PaymentRequestData.cpp b/dom/payments/PaymentRequestData.cpp
--- a/dom/payments/PaymentRequestData.cpp
+++ b/dom/payments/PaymentRequestData.cpp
@@ -1,16 +1,17 @@
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsArrayUtils.h"
 #include "nsIMutableArray.h"
 #include "nsISupportsPrimitives.h"
+#include "nsUnicharUtils.h"
 #include "PaymentRequestData.h"
 #include "PaymentRequestUtils.h"
 
 namespace mozilla {
 namespace dom {
 namespace payments {
 
 /* PaymentMethodData */
@@ -59,19 +60,24 @@ PaymentMethodData::GetData(JSContext* aC
 
 /* PaymentCurrencyAmount */
 
 NS_IMPL_ISUPPORTS(PaymentCurrencyAmount,
                   nsIPaymentCurrencyAmount)
 
 PaymentCurrencyAmount::PaymentCurrencyAmount(const nsAString& aCurrency,
                                              const nsAString& aValue)
-  : mCurrency(aCurrency)
-  , mValue(aValue)
+  : mValue(aValue)
 {
+  /*
+   *  According to the spec
+   *  https://w3c.github.io/payment-request/#validity-checkers
+   *  Set amount.currency to the result of ASCII uppercasing amount.currency.
+   */
+  ToUpperCase(aCurrency, mCurrency);
 }
 
 nsresult
 PaymentCurrencyAmount::Create(const IPCPaymentCurrencyAmount& aIPCAmount,
                               nsIPaymentCurrencyAmount** aAmount)
 {
   NS_ENSURE_ARG_POINTER(aAmount);
   nsCOMPtr<nsIPaymentCurrencyAmount> amount =
diff --git a/dom/payments/PaymentRequestManager.cpp b/dom/payments/PaymentRequestManager.cpp
--- a/dom/payments/PaymentRequestManager.cpp
+++ b/dom/payments/PaymentRequestManager.cpp
@@ -18,26 +18,26 @@ namespace mozilla {
 namespace dom {
 namespace {
 
 /*
  *  Following Convert* functions are used for convert PaymentRequest structs
  *  to transferable structs for IPC.
  */
 nsresult
-ConvertMethodData(const PaymentMethodData& aMethodData,
+ConvertMethodData(JSContext* aCx,
+                  const PaymentMethodData& aMethodData,
                   IPCPaymentMethodData& aIPCMethodData)
 {
+  NS_ENSURE_ARG_POINTER(aCx);
   // Convert JSObject to a serialized string
   nsAutoString serializedData;
   if (aMethodData.mData.WasPassed()) {
-    JSContext* cx = nsContentUtils::GetCurrentJSContext();
-    MOZ_ASSERT(cx);
-    JS::RootedObject object(cx, aMethodData.mData.Value());
-    nsresult rv = SerializeFromJSObject(cx, object, serializedData);
+    JS::RootedObject object(aCx, aMethodData.mData.Value());
+    nsresult rv = SerializeFromJSObject(aCx, object, serializedData);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
     }
   }
   aIPCMethodData = IPCPaymentMethodData(aMethodData.mSupportedMethods, serializedData);
   return NS_OK;
 }
 
@@ -52,26 +52,26 @@ void
 ConvertItem(const PaymentItem& aItem, IPCPaymentItem& aIPCItem)
 {
   IPCPaymentCurrencyAmount amount;
   ConvertCurrencyAmount(aItem.mAmount, amount);
   aIPCItem = IPCPaymentItem(aItem.mLabel, amount, aItem.mPending);
 }
 
 nsresult
-ConvertModifier(const PaymentDetailsModifier& aModifier,
+ConvertModifier(JSContext* aCx,
+                const PaymentDetailsModifier& aModifier,
                 IPCPaymentDetailsModifier& aIPCModifier)
 {
+  NS_ENSURE_ARG_POINTER(aCx);
   // Convert JSObject to a serialized string
   nsAutoString serializedData;
   if (aModifier.mData.WasPassed()) {
-    JSContext* cx = nsContentUtils::GetCurrentJSContext();
-    MOZ_ASSERT(cx);
-    JS::RootedObject object(cx, aModifier.mData.Value());
-    nsresult rv = SerializeFromJSObject(cx, object, serializedData);
+    JS::RootedObject object(aCx, aModifier.mData.Value());
+    nsresult rv = SerializeFromJSObject(aCx, object, serializedData);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
     }
   }
 
   IPCPaymentItem total;
   ConvertItem(aModifier.mTotal, total);
 
@@ -96,22 +96,24 @@ ConvertShippingOption(const PaymentShipp
                       IPCPaymentShippingOption& aIPCOption)
 {
   IPCPaymentCurrencyAmount amount;
   ConvertCurrencyAmount(aOption.mAmount, amount);
   aIPCOption = IPCPaymentShippingOption(aOption.mId, aOption.mLabel, amount, aOption.mSelected);
 }
 
 nsresult
-ConvertDetailsBase(const PaymentDetailsBase& aDetails,
+ConvertDetailsBase(JSContext* aCx,
+                   const PaymentDetailsBase& aDetails,
                    nsTArray<IPCPaymentItem>& aDisplayItems,
                    nsTArray<IPCPaymentShippingOption>& aShippingOptions,
                    nsTArray<IPCPaymentDetailsModifier>& aModifiers,
                    bool aResetShippingOptions)
 {
+  NS_ENSURE_ARG_POINTER(aCx);
   if (aDetails.mDisplayItems.WasPassed()) {
     for (const PaymentItem& item : aDetails.mDisplayItems.Value()) {
       IPCPaymentItem displayItem;
       ConvertItem(item, displayItem);
       aDisplayItems.AppendElement(displayItem);
     }
   }
   if (aDetails.mShippingOptions.WasPassed() && !aResetShippingOptions) {
@@ -119,36 +121,39 @@ ConvertDetailsBase(const PaymentDetailsB
       IPCPaymentShippingOption shippingOption;
       ConvertShippingOption(option, shippingOption);
       aShippingOptions.AppendElement(shippingOption);
     }
   }
   if (aDetails.mModifiers.WasPassed()) {
     for (const PaymentDetailsModifier& modifier : aDetails.mModifiers.Value()) {
       IPCPaymentDetailsModifier detailsModifier;
-      nsresult rv = ConvertModifier(modifier, detailsModifier);
+      nsresult rv = ConvertModifier(aCx, modifier, detailsModifier);
       if (NS_WARN_IF(NS_FAILED(rv))) {
         return rv;
       }
       aModifiers.AppendElement(detailsModifier);
     }
   }
   return NS_OK;
 }
 
 nsresult
-ConvertDetailsInit(const PaymentDetailsInit& aDetails,
+ConvertDetailsInit(JSContext* aCx,
+                   const PaymentDetailsInit& aDetails,
                    IPCPaymentDetails& aIPCDetails,
                    bool aResetShippingOptions)
 {
+  NS_ENSURE_ARG_POINTER(aCx);
   // Convert PaymentDetailsBase members
   nsTArray<IPCPaymentItem> displayItems;
   nsTArray<IPCPaymentShippingOption> shippingOptions;
   nsTArray<IPCPaymentDetailsModifier> modifiers;
-  nsresult rv = ConvertDetailsBase(aDetails, displayItems, shippingOptions, modifiers, aResetShippingOptions);
+  nsresult rv = ConvertDetailsBase(aCx, aDetails, displayItems, shippingOptions,
+                                   modifiers, aResetShippingOptions);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   // Convert |id|
   nsString id(EmptyString());
   if (aDetails.mId.WasPassed()) {
     id = aDetails.mId.Value();
@@ -166,27 +171,29 @@ ConvertDetailsInit(const PaymentDetailsI
                                   EmptyString(), // error message
                                   aDetails.mDisplayItems.WasPassed(),
                                   aDetails.mShippingOptions.WasPassed(),
                                   aDetails.mModifiers.WasPassed());
   return NS_OK;
 }
 
 nsresult
-ConvertDetailsUpdate(const PaymentDetailsUpdate& aDetails,
+ConvertDetailsUpdate(JSContext* aCx,
+                     const PaymentDetailsUpdate& aDetails,
                      IPCPaymentDetails& aIPCDetails)
 {
+  NS_ENSURE_ARG_POINTER(aCx);
   // Convert PaymentDetailsBase members
   nsTArray<IPCPaymentItem> displayItems;
   nsTArray<IPCPaymentShippingOption> shippingOptions;
   nsTArray<IPCPaymentDetailsModifier> modifiers;
   // [TODO] Populate a boolean flag as aResetShippingOptions based on the
   // result of processing details.shippingOptions in UpdatePayment method.
-  nsresult rv = ConvertDetailsBase(
-    aDetails, displayItems, shippingOptions, modifiers, false);
+  nsresult rv = ConvertDetailsBase(aCx, aDetails, displayItems, shippingOptions,
+                                   modifiers, false);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   // Convert required |total|
   IPCPaymentItem total;
   ConvertItem(aDetails.mTotal, total);
 
@@ -385,24 +392,26 @@ GetSelectedShippingOption(const PaymentD
     // set aOption to last selected option's ID
     if (shippingOption.mSelected) {
       aOption = shippingOption.mId;
     }
   }
 }
 
 nsresult
-PaymentRequestManager::CreatePayment(nsPIDOMWindowInner* aWindow,
+PaymentRequestManager::CreatePayment(JSContext* aCx,
+                                     nsPIDOMWindowInner* aWindow,
                                      nsIPrincipal* aTopLevelPrincipal,
                                      const Sequence<PaymentMethodData>& aMethodData,
                                      const PaymentDetailsInit& aDetails,
                                      const PaymentOptions& aOptions,
                                      PaymentRequest** aRequest)
 {
   MOZ_ASSERT(NS_IsMainThread());
+  NS_ENSURE_ARG_POINTER(aCx);
   NS_ENSURE_ARG_POINTER(aRequest);
   NS_ENSURE_ARG_POINTER(aTopLevelPrincipal);
   *aRequest = nullptr;
   nsresult rv;
 
   RefPtr<PaymentRequest> request = PaymentRequest::CreatePaymentRequest(aWindow, rv);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
@@ -441,25 +450,25 @@ PaymentRequestManager::CreatePayment(nsP
   }
 
   nsAutoString internalId;
   request->GetInternalId(internalId);
 
   nsTArray<IPCPaymentMethodData> methodData;
   for (const PaymentMethodData& data : aMethodData) {
     IPCPaymentMethodData ipcMethodData;
-    rv = ConvertMethodData(data, ipcMethodData);
+    rv = ConvertMethodData(aCx, data, ipcMethodData);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
     }
     methodData.AppendElement(ipcMethodData);
   }
 
   IPCPaymentDetails details;
-  rv = ConvertDetailsInit(aDetails, details, resetShippingOptions);
+  rv = ConvertDetailsInit(aCx, aDetails, details, resetShippingOptions);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   IPCPaymentOptions options;
   ConvertOptions(aOptions, options);
 
   IPCPaymentCreateActionRequest action(internalId,
@@ -541,32 +550,34 @@ PaymentRequestManager::CompletePayment(c
 
   nsAutoString requestId(aRequestId);
   IPCPaymentCompleteActionRequest action(requestId, completeStatusString);
 
   return SendRequestPayment(request, action);
 }
 
 nsresult
-PaymentRequestManager::UpdatePayment(const nsAString& aRequestId,
+PaymentRequestManager::UpdatePayment(JSContext* aCx,
+                                     const nsAString& aRequestId,
                                      const PaymentDetailsUpdate& aDetails)
 {
+  NS_ENSURE_ARG_POINTER(aCx);
   RefPtr<PaymentRequest> request = GetPaymentRequestById(aRequestId);
   if (!request) {
     return NS_ERROR_UNEXPECTED;
   }
 
   // [TODO] Process details.shippingOptions if presented.
   //        1) Check if there are duplicate IDs in details.shippingOptions,
   //           if so, reset details.shippingOptions to an empty sequence.
   //        2) Set request's selectedShippingOption to the ID of last selected
   //           option.
 
   IPCPaymentDetails details;
-  nsresult rv = ConvertDetailsUpdate(aDetails, details);
+  nsresult rv = ConvertDetailsUpdate(aCx, aDetails, details);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   nsAutoString requestId(aRequestId);
   IPCPaymentUpdateActionRequest action(requestId, details);
   return SendRequestPayment(request, action);
 }
diff --git a/dom/payments/PaymentRequestManager.h b/dom/payments/PaymentRequestManager.h
--- a/dom/payments/PaymentRequestManager.h
+++ b/dom/payments/PaymentRequestManager.h
@@ -36,29 +36,31 @@ public:
   GetPaymentRequestById(const nsAString& aRequestId);
 
   /*
    *  This method is used to create PaymentRequest object and send corresponding
    *  data to chrome process for internal payment creation, such that content
    *  process can ask specific task by sending requestId only.
    */
   nsresult
-  CreatePayment(nsPIDOMWindowInner* aWindow,
+  CreatePayment(JSContext* aCx,
+                nsPIDOMWindowInner* aWindow,
                 nsIPrincipal* aTopLevelPrincipal,
                 const Sequence<PaymentMethodData>& aMethodData,
                 const PaymentDetailsInit& aDetails,
                 const PaymentOptions& aOptions,
                 PaymentRequest** aRequest);
 
   nsresult CanMakePayment(const nsAString& aRequestId);
   nsresult ShowPayment(const nsAString& aRequestId);
   nsresult AbortPayment(const nsAString& aRequestId);
   nsresult CompletePayment(const nsAString& aRequestId,
                            const PaymentComplete& aComplete);
-  nsresult UpdatePayment(const nsAString& aRequestId,
+  nsresult UpdatePayment(JSContext* aCx,
+                         const nsAString& aRequestId,
                          const PaymentDetailsUpdate& aDetails);
 
   nsresult RespondPayment(const IPCPaymentActionResponse& aResponse);
   nsresult ChangeShippingAddress(const nsAString& aRequestId,
                                  const IPCPaymentAddress& aAddress);
   nsresult ChangeShippingOption(const nsAString& aRequestId,
                                 const nsAString& aOption);
 
diff --git a/dom/payments/PaymentRequestUpdateEvent.cpp b/dom/payments/PaymentRequestUpdateEvent.cpp
--- a/dom/payments/PaymentRequestUpdateEvent.cpp
+++ b/dom/payments/PaymentRequestUpdateEvent.cpp
@@ -49,41 +49,43 @@ PaymentRequestUpdateEvent::PaymentReques
   , mRequest(nullptr)
 {
   MOZ_ASSERT(aOwner);
 }
 
 void
 PaymentRequestUpdateEvent::ResolvedCallback(JSContext* aCx, JS::Handle<JS::Value> aValue)
 {
+  MOZ_ASSERT(aCx);
   MOZ_ASSERT(mRequest);
 
   if (NS_WARN_IF(!aValue.isObject()) || !mWaitForUpdate) {
     return;
   }
 
   // Converting value to a PaymentDetailsUpdate dictionary
   PaymentDetailsUpdate details;
   if (!details.Init(aCx, aValue)) {
     return;
   }
 
   // Validate and canonicalize the details
-  if (!mRequest->IsValidDetailsUpdate(details)) {
-    mRequest->AbortUpdate(NS_ERROR_TYPE_ERR);
+  nsresult rv = mRequest->IsValidDetailsUpdate(details);
+  if (NS_FAILED(rv)) {
+    mRequest->AbortUpdate(rv);
     return;
   }
 
   // [TODO]
   // If the data member of modifier is present,
   // let serializedData be the result of JSON-serializing modifier.data into a string.
   // null if it is not.
 
   // Update the PaymentRequest with the new details
-  if (NS_FAILED(mRequest->UpdatePayment(details))) {
+  if (NS_FAILED(mRequest->UpdatePayment(aCx, details))) {
     mRequest->AbortUpdate(NS_ERROR_DOM_ABORT_ERR);
     return;
   }
   mWaitForUpdate = false;
   mRequest->SetUpdating(false);
 }
 
 void
