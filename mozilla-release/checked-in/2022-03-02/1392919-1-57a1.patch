# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1503469971 -28800
# Node ID 1d196a124e475b19735b6710d3e66083d4006760
# Parent  e18486024d1ef63f36563ef3f7dfef06b1b549bc
Bug 1392919. P1 - include the suspend status when notifying NotifySuspendedStatusChanged. r=gerald

MozReview-Commit-ID: GwzIBNLjcZr

diff --git a/dom/media/ChannelMediaDecoder.cpp b/dom/media/ChannelMediaDecoder.cpp
--- a/dom/media/ChannelMediaDecoder.cpp
+++ b/dom/media/ChannelMediaDecoder.cpp
@@ -109,37 +109,40 @@ ChannelMediaDecoder::ResourceCallback::N
     if (NS_SUCCEEDED(aStatus)) {
       MediaDecoderOwner* owner = self->GetMediaOwner();
       MOZ_ASSERT(owner);
       owner->DownloadSuspended();
 
       // NotifySuspendedStatusChanged will tell the element that download
       // has been suspended "by the cache", which is true since we never
       // download anything. The element can then transition to HAVE_ENOUGH_DATA.
-      self->mDecoder->NotifySuspendedStatusChanged();
+      owner->NotifySuspendedByCache(true);
     }
   });
   mAbstractMainThread->Dispatch(r.forget());
 }
 
 void
 ChannelMediaDecoder::ResourceCallback::NotifyPrincipalChanged()
 {
   MOZ_ASSERT(NS_IsMainThread());
   if (mDecoder) {
     mDecoder->NotifyPrincipalChanged();
   }
 }
 
 void
-ChannelMediaDecoder::ResourceCallback::NotifySuspendedStatusChanged()
+ChannelMediaDecoder::ResourceCallback::NotifySuspendedStatusChanged(
+  bool aSuspendedByCache)
 {
   MOZ_ASSERT(NS_IsMainThread());
-  if (mDecoder) {
-    mDecoder->NotifySuspendedStatusChanged();
+  MediaDecoderOwner* owner = GetMediaOwner();
+  if (owner) {
+    AbstractThread::AutoEnter context(owner->AbstractMainThread());
+    owner->NotifySuspendedByCache(aSuspendedByCache);
   }
 }
 
 void
 ChannelMediaDecoder::ResourceCallback::NotifyBytesConsumed(int64_t aBytes,
                                                            int64_t aOffset)
 {
   RefPtr<ResourceCallback> self = this;
diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -36,17 +36,17 @@ class ChannelMediaDecoder : public Media
 
   private:
     /* MediaResourceCallback functions */
     MediaDecoderOwner* GetMediaOwner() const override;
     void NotifyNetworkError() override;
     void NotifyDataArrived() override;
     void NotifyDataEnded(nsresult aStatus) override;
     void NotifyPrincipalChanged() override;
-    void NotifySuspendedStatusChanged() override;
+    void NotifySuspendedStatusChanged(bool aSuspendedByCache) override;
     void NotifyBytesConsumed(int64_t aBytes, int64_t aOffset) override;
 
     static void TimerCallback(nsITimer* aTimer, void* aClosure);
 
     // The decoder to send notifications. Main-thread only.
     ChannelMediaDecoder* mDecoder = nullptr;
     nsCOMPtr<nsITimer> mTimer;
     bool mTimerArmed = false;
diff --git a/dom/media/MediaResource.cpp b/dom/media/MediaResource.cpp
--- a/dom/media/MediaResource.cpp
+++ b/dom/media/MediaResource.cpp
@@ -896,17 +896,17 @@ ChannelMediaResource::CacheClientNotifyP
 
   mCallback->NotifyPrincipalChanged();
 }
 
 void
 ChannelMediaResource::CacheClientNotifySuspendedStatusChanged()
 {
   NS_ASSERTION(NS_IsMainThread(), "Don't call on non-main thread");
-  mCallback->NotifySuspendedStatusChanged();
+  mCallback->NotifySuspendedStatusChanged(IsSuspendedByCache());
 }
 
 nsresult
 ChannelMediaResource::CacheClientSeek(int64_t aOffset, bool aResume)
 {
   NS_ASSERTION(NS_IsMainThread(), "Don't call on non-main thread");
 
   LOG("CacheClientSeek requested for aOffset [%" PRId64 "] for decoder [%p]",
diff --git a/dom/media/MediaResourceCallback.h b/dom/media/MediaResourceCallback.h
--- a/dom/media/MediaResourceCallback.h
+++ b/dom/media/MediaResourceCallback.h
@@ -41,17 +41,17 @@ public:
   // NOTE: this can be called with the media cache lock held, so don't
   // block or do anything which might try to acquire a lock!
   virtual void NotifyDataEnded(nsresult aStatus) {}
 
   // Notify that the principal of MediaResource has changed.
   virtual void NotifyPrincipalChanged() {}
 
   // Notify that the "cache suspended" status of MediaResource changes.
-  virtual void NotifySuspendedStatusChanged() {}
+  virtual void NotifySuspendedStatusChanged(bool aSuspendedByCache) {}
 
   // Notify the number of bytes read from the resource.
   virtual void NotifyBytesConsumed(int64_t aBytes, int64_t aOffset) {}
 
 protected:
   virtual ~MediaResourceCallback() {}
 };
 
