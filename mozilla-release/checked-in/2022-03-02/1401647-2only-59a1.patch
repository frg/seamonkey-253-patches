# HG changeset patch
# User Ted Mielczarek <ted@mielczarek.org>
# Date 1513268433 21600
# Node ID 273a99be71914167664482c2bdb26c840ec6867b
# Parent  cbbcabb3f95c09ab9433d5af88b3353fb8d904a2
bug 1401647 - use a 64-bit Rust toolchain for win32 builds. r=nalexander,rillian

We currently use a 32-bit Rust toolchain for win32 builds, but this can lead
to OOM situations. This patch makes win32 builds use a 64-bit Rust toolchain,
which requires a little bit of extra configuration because rustc needs to
be able to find a link.exe that produces 64-bit binaries for building
things like build scripts, which are host binaries.

We will now generate a batch file that sets LIB to the paths to 64-bit
libraries and invokes the x64-targeting link.exe, and add a section to the
.cargo/config file to instruct cargo to use that batch file as the linker
when producing 64-bit binaries.

MozReview-Commit-ID: 9vKBbm7Gvra

diff --git a/.cargo/config.in b/.cargo/config.in
--- a/.cargo/config.in
+++ b/.cargo/config.in
@@ -1,6 +1,8 @@
 [source.crates-io]
 registry = 'https://github.com/rust-lang/crates.io-index'
 replace-with = 'vendored-sources'
 
 [source.vendored-sources]
 directory = '@top_srcdir@/third_party/rust'
+
+@WIN64_CARGO_LINKER_CONFIG@
diff --git a/build/moz.build b/build/moz.build
--- a/build/moz.build
+++ b/build/moz.build
@@ -7,16 +7,18 @@
 with Files('**'):
     BUG_COMPONENT = ('Core', 'Build Config')
 
 # This cannot be named "build" because of bug 922191.
 SPHINX_TREES['buildsystem'] = 'docs'
 
 if CONFIG['OS_ARCH'] == 'WINNT':
     DIRS += ['win32']
+    if CONFIG['WIN64_CARGO_LINKER']:
+        CONFIGURE_SUBST_FILES += ['win64/cargo-linker.bat']
 else:
     DIRS += ['unix']
 
 CRAMTEST_MANIFESTS += [
     'tests/cram/cram.ini',
 ]
 
 if CONFIG['MOZ_WIDGET_TOOLKIT'] == 'android':
diff --git a/build/moz.configure/rust.configure b/build/moz.configure/rust.configure
--- a/build/moz.configure/rust.configure
+++ b/build/moz.configure/rust.configure
@@ -13,16 +13,17 @@ cargo = check_prog('CARGO', ['cargo'], a
 @depends_if(rustc)
 @checking('rustc version', lambda info: info.version)
 def rustc_info(rustc):
     out = check_cmd_output(rustc, '--version', '--verbose').splitlines()
     info = dict((s.strip() for s in line.split(':', 1)) for line in out[1:])
     return namespace(
         version=Version(info.get('release', '0')),
         commit=info.get('commit-hash', 'unknown'),
+        host=info['host'],
     )
 
 
 @depends_if(cargo)
 @checking('cargo version', lambda info: info.version)
 @imports('re')
 def cargo_info(cargo):
     out = check_cmd_output(cargo, '--version', '--verbose').splitlines()
@@ -241,8 +242,44 @@ def rust_target_env_name(triple):
 set_config('RUST_TARGET_ENV_NAME', rust_target_env_name)
 
 # This is used for putting source info into symbol files.
 set_config('RUSTC_COMMIT', depends(rustc_info)(lambda i: i.commit))
 
 # Until we remove all the other Rust checks in old-configure.
 add_old_configure_assignment('RUSTC', rustc)
 add_old_configure_assignment('RUST_TARGET', rust_target_triple)
+
+option(env='WIN64_LINK', nargs=1, help='Path to link.exe that targets win64')
+option(env='WIN64_LIB', nargs=1, help='Paths to libraries for the win64 linker')
+
+set_config('WIN64_LINK', depends('WIN64_LINK')(lambda x: x))
+set_config('WIN64_LIB', depends('WIN64_LIB')(lambda x: x))
+
+
+@depends(target, rustc_info, c_compiler, 'WIN64_LINK', 'WIN64_LIB')
+def win64_cargo_linker(target, rustc_info, compiler_info, link, lib):
+    # When we're building a 32-bit Windows build with a 64-bit rustc, we
+    # need to configure the linker it will use for host binaries (build scripts)
+    # specially because the compiler configuration we use for the build is for
+    # MSVC targeting 32-bit binaries.
+    if target.kernel == 'WINNT' and target.cpu == 'x86' and \
+       compiler_info.type in ('msvc', 'clang-cl') and \
+       rustc_info.host == 'x86_64-pc-windows-msvc' and link and lib:
+        return True
+
+
+set_config('WIN64_CARGO_LINKER', win64_cargo_linker)
+
+
+@depends(win64_cargo_linker, check_build_environment)
+@imports(_from='textwrap', _import='dedent')
+def win64_cargo_linker_config(linker, env):
+    if linker:
+        return dedent('''\
+        [target.x86_64-pc-windows-msvc]
+        linker = "{objdir}/build/win64/cargo-linker.bat"
+        '''.format(objdir=env.topobjdir))
+    # We want an empty string here so we don't leave the @ variable in the config file.
+    return ''
+
+
+set_config('WIN64_CARGO_LINKER_CONFIG', win64_cargo_linker_config)
diff --git a/build/win32/mozconfig.vs2017 b/build/win32/mozconfig.vs2017
--- a/build/win32/mozconfig.vs2017
+++ b/build/win32/mozconfig.vs2017
@@ -11,16 +11,19 @@ if [ -d "${VSPATH}" ]; then
     export WIN_UCRT_REDIST_DIR="${VSPATH}/SDK/Redist/ucrt/DLLs/x86"
     export WIN_DIA_SDK_BIN_DIR="${VSPATH}/DIA SDK/bin"
 
     export PATH="${VSPATH}/VC/bin/Hostx86/x86:${VSPATH}/VC/bin/Hostx64/x86:${VSPATH}/VC/bin/Hostx64/x64:${VSPATH}/SDK/bin/10.0.15063.0/x64:${WIN_DIA_SDK_BIN_DIR}:${PATH}"
     export PATH="${VSPATH}/VC/redist/x86/Microsoft.VC141.CRT:${VSPATH}/SDK/Redist/ucrt/DLLs/x86:${PATH}"
 
     export INCLUDE="${VSPATH}/VC/include:${VSPATH}/VC/atlmfc/include:${VSPATH}/SDK/Include/10.0.15063.0/ucrt:${VSPATH}/SDK/Include/10.0.15063.0/shared:${VSPATH}/SDK/Include/10.0.15063.0/um:${VSPATH}/SDK/Include/10.0.15063.0/winrt:${VSPATH}/DIA SDK/include"
     export LIB="${VSPATH}/VC/lib/x86:${VSPATH}/VC/atlmfc/lib/x86:${VSPATH}/SDK/Lib/10.0.15063.0/ucrt/x86:${VSPATH}/SDK/Lib/10.0.15063.0/um/x86:${VSPATH}/DIA SDK/lib"
+
+    export WIN64_LINK="${VSPATH}/VC/bin/Hostx64/x64/link.exe"
+    export WIN64_LIB="${VSPATH}/VC/lib/x64:${VSPATH}/VC/atlmfc/lib/x64:${VSPATH}/SDK/Lib/10.0.15063.0/ucrt/x64:${VSPATH}/SDK/Lib/10.0.15063.0/um/x64:${VSPATH}/DIA SDK/lib/amd64"
 fi
 
 . $topsrcdir/build/mozconfig.vs-common
 
 mk_export_correct_style WINDOWSSDKDIR
 mk_export_correct_style WIN32_REDIST_DIR
 mk_export_correct_style WIN_UCRT_REDIST_DIR
 mk_export_correct_style PATH
diff --git a/build/win64/cargo-linker.bat.in b/build/win64/cargo-linker.bat.in
new file mode 100644
--- /dev/null
+++ b/build/win64/cargo-linker.bat.in
@@ -0,0 +1,3 @@
+set LIB=@WIN64_LIB@
+
+@WIN64_LINK@ %*
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
@@ -1472,17 +1472,17 @@ class OpenBSDToolchainTest(BaseToolchain
 class RustTest(BaseConfigureTest):
     def invoke_cargo(self, stdin, args):
         if args == ('--version', '--verbose'):
             return 0, 'cargo 2.0\nrelease: 2.0', ''
         raise NotImplementedError('unsupported arguments')
 
     def invoke_rustc(self, stdin, args):
         if args == ('--version', '--verbose'):
-            return 0, 'rustc 2.0\nrelease: 2.0', ''
+            return 0, 'rustc 2.0\nrelease: 2.0\nhost: x86_64-unknown-linux-gnu', ''
         if args == ('--print', 'target-list'):
             # Raw list returned by rustc version 1.19, + ios, which somehow
             # don't appear in the default list.
             # https://github.com/rust-lang/rust/issues/36156
             rust_targets = [
                 'aarch64-apple-ios',
                 'aarch64-linux-android',
                 'aarch64-unknown-freebsd',
