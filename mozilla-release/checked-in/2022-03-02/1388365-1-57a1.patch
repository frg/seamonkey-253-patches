# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1502201385 -3600
# Node ID 535e762772079db364e730f336a068bfd2d6f875
# Parent  263f449f2e8d6b4b183c22ac787480636a35c1ee
Bug 1388365 - Upgrade to webdriver 0.29.0. r=whimboo

Upgrades the webdriver crate dependency to 0.29.0, which contains some
backwards incompatible changes for RectResponse.  This type has been
split in two, WindowRectResponse and ElementRectResponse.

The former type contains a new "state" field which is already implemented
by Marionette.  Because geckodriver is used with a range of earlier
Firefoxen, it defaults to "normal" window state if the field is not
returned from Marionette.  This is acceptable.

MozReview-Commit-ID: FRxppRVmiZl

diff --git a/testing/geckodriver/CHANGES.md b/testing/geckodriver/CHANGES.md
--- a/testing/geckodriver/CHANGES.md
+++ b/testing/geckodriver/CHANGES.md
@@ -1,19 +1,27 @@
 # Change log
 
 All notable changes to this program is documented in this file.
 
 ## Unreleased
 
 ### Added
 - Added preference `dom.file.createInChild` set to true to allow file object creation in content processes.
+- New window `state` field on the window rect response object, returned from [`GetWindowRect`], [`SetWindowRect`], [`MinimizeWindow`], [`MaximizeWindow`], and [`FullscreenWindow`] commands
+
+[`FullscreenWindow`]: https://docs.rs/webdriver/0.29.0/webdriver/command/enum.WebDriverCommand.html#variant.FullscreenWindow
+[`GetWindowRect`]: https://docs.rs/webdriver/0.29.0/webdriver/command/enum.WebDriverCommand.html#variant.GetWindowRect
+[`MaximizeWindow`]: https://docs.rs/webdriver/0.29.0/webdriver/command/enum.WebDriverCommand.html#variant.MaximizeWindow
+[`MinimizeWindow`]: https://docs.rs/webdriver/0.29.0/webdriver/command/enum.WebDriverCommand.html#variant.MinimizeWindow
+[`SetWindowRect`]: https://docs.rs/webdriver/0.29.0/webdriver/command/enum.WebDriverCommand.html#variant.SetWindowRect
 
 ### Changed
-- `/moz/addon/install` command accepts an `addon` parameter, in lieu of `path`, containing an add-on as a base64 string.
+- The `proxyType` `noproxy` has been replaced with `direct` in accordance with recent WebDriver specification changes
+- `/moz/addon/install` command accepts an `addon` parameter, in lieu of `path`, containing an addon as a Base64 string
 
 ## 0.18.0 (2017-07-10)
 
 ### Changed
 - [`RectResponse`](https://docs.rs/webdriver/0.27.0/webdriver/response/struct.RectResponse.html) permits returning floats for `width` and `height` fields
 - New type [`CookieResponse`](https://docs.rs/webdriver/0.27.0/webdriver/response/struct.CookieResponse.html) for the [`GetNamedCookie` command](https://docs.rs/webdriver/0.27.0/webdriver/command/enum.WebDriverCommand.html#variant.GetNamedCookie) returns a single cookie, as opposed to an array of a single cookie
 - To pick up a prepared profile from the filesystem, it is now possible to pass `["-profile", "/path/to/profile"]` in the `args` array on `moz:firefoxOptions`
 - geckodriver now recommends Firefox 53 and greater
diff --git a/testing/geckodriver/Cargo.toml b/testing/geckodriver/Cargo.toml
--- a/testing/geckodriver/Cargo.toml
+++ b/testing/geckodriver/Cargo.toml
@@ -22,13 +22,13 @@ mozrunner = "0.4.1"
 mozversion = "0.1.2"
 regex = "0.2"
 rustc-serialize = "0.3"
 slog = "1"
 slog-atomic = "0.4"
 slog-stdlog = "1"
 slog-stream = "1"
 uuid = "0.1.18"
-webdriver = "0.28.0"
+webdriver = "0.29.0"
 zip = "0.1"
 
 [[bin]]
 name = "geckodriver"
diff --git a/testing/geckodriver/src/marionette.rs b/testing/geckodriver/src/marionette.rs
--- a/testing/geckodriver/src/marionette.rs
+++ b/testing/geckodriver/src/marionette.rs
@@ -40,19 +40,19 @@ use webdriver::command::WebDriverCommand
     AcceptAlert, GetAlertText, SendAlertText, TakeScreenshot, TakeElementScreenshot,
     Extension, PerformActions, ReleaseActions};
 use webdriver::command::{
     NewSessionParameters, GetParameters, WindowRectParameters, SwitchToWindowParameters,
     SwitchToFrameParameters, LocatorParameters, JavascriptCommandParameters,
     GetNamedCookieParameters, AddCookieParameters, TimeoutsParameters,
     ActionsParameters, TakeScreenshotParameters};
 use webdriver::response::{CloseWindowResponse, Cookie, CookieResponse, CookiesResponse,
-                          NewSessionResponse, RectResponse, TimeoutsResponse, ValueResponse,
-                          WebDriverResponse};
-use webdriver::common::{Date, ELEMENT_KEY, FrameId, Nullable, WebElement};
+                          ElementRectResponse, NewSessionResponse, TimeoutsResponse,
+                          ValueResponse, WebDriverResponse, WindowRectResponse};
+use webdriver::common::{Date, ELEMENT_KEY, FrameId, Nullable, WebElement, WindowState};
 use webdriver::error::{ErrorStatus, WebDriverError, WebDriverResult};
 use webdriver::server::{WebDriverHandler, Session};
 use webdriver::httpapi::{WebDriverExtensionRoute};
 
 use capabilities::{FirefoxCapabilities, FirefoxOptions};
 use prefs;
 
 const DEFAULT_HOST: &'static str = "localhost";
@@ -755,17 +755,18 @@ impl MarionetteSession {
 
                 let height = try_opt!(
                     try_opt!(resp.result.find("height"),
                              ErrorStatus::UnknownError,
                              "Failed to find height field").as_f64(),
                     ErrorStatus::UnknownError,
                     "Failed to interpret width as float");
 
-                WebDriverResponse::ElementRect(RectResponse::new(x, y, width, height))
+                let rect = ElementRectResponse { x, y, width, height };
+                WebDriverResponse::ElementRect(rect)
             },
             FullscreenWindow | MinimizeWindow | MaximizeWindow | GetWindowRect |
             SetWindowRect(_) => {
                 let width = try_opt!(
                     try_opt!(resp.result.find("width"),
                              ErrorStatus::UnknownError,
                              "Failed to find width field").as_f64(),
                     ErrorStatus::UnknownError,
@@ -787,17 +788,23 @@ impl MarionetteSession {
 
                 let y = try_opt!(
                     try_opt!(resp.result.find("y"),
                              ErrorStatus::UnknownError,
                              "Failed to find y field").as_f64(),
                     ErrorStatus::UnknownError,
                     "Failed to interpret y as float");
 
-                WebDriverResponse::WindowRect(RectResponse::new(x, y, width, height))
+                let state = match resp.result.find("state") {
+                    Some(json) => WindowState::from_json(json)?,
+                    None => WindowState::Normal,
+                };
+
+                let rect = WindowRectResponse { x, y, width, height, state };
+                WebDriverResponse::WindowRect(rect)
             },
             GetCookies => {
                 let cookies = try!(self.process_cookies(&resp.result));
                 WebDriverResponse::Cookies(CookiesResponse { value: cookies })
             },
             GetNamedCookie(ref name) => {
                 let mut cookies = try!(self.process_cookies(&resp.result));
                 cookies.retain(|x| x.name == *name);
