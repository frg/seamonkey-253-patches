# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1504171836 14400
#      Thu Aug 31 05:30:36 2017 -0400
# Node ID f74e7808e0cbaede41af61ae8e6934235d148c6a
# Parent  f712987c1d28e6e8b7eb4f9f6ab3ba4df9cca336
Bug 1391793 - Save the given ImageContainer in WebRenderImageData so that it is not recreated. r=jrmuizel

A RasterImage's ImageContainer is only retained as a weak pointer in the
RasterImage itself. With WebRender and display items, we no longer kept
a strong reference to said image container, and as such it got recreated
every time it had to repaint (e.g. after a scroll). This would
unnecessarily reset/update the generation counter such that the
ImageClient would decide it needs to retransmit the image data to the
GPU process. By keeping a strong reference to the container that we
don't otherwise use, we avoid this situation.

diff --git a/gfx/layers/wr/WebRenderUserData.cpp b/gfx/layers/wr/WebRenderUserData.cpp
--- a/gfx/layers/wr/WebRenderUserData.cpp
+++ b/gfx/layers/wr/WebRenderUserData.cpp
@@ -58,16 +58,20 @@ WebRenderImageData::~WebRenderImageData(
 }
 
 Maybe<wr::ImageKey>
 WebRenderImageData::UpdateImageKey(ImageContainer* aContainer, bool aForceUpdate)
 {
   CreateImageClientIfNeeded();
   CreateExternalImageIfNeeded();
 
+  if (mContainer != aContainer) {
+    mContainer = aContainer;
+  }
+
   if (!mImageClient || !mExternalImageId) {
     return Nothing();
   }
 
   MOZ_ASSERT(mImageClient->AsImageClientSingle());
   MOZ_ASSERT(aContainer);
 
   ImageClientSingle* imageClient = mImageClient->AsImageClientSingle();
