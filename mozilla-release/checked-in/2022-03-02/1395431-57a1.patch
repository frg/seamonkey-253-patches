# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1504065523 -28800
# Node ID 670f548e668947054c77d3c1614b0cac2636411b
# Parent  f1bbc5bcef2cd56135891aab5cdf47a91eb284cd
Bug 1395431 - dispatch a task to call MediaResourceCallback::NotifyDataArrived() in CacheClientNotifyDataReceived(). r=cpearce

Since MediaResourceCallback is ref-counted, we don't need nsRevocableEventPtr
to handle the case ChannelMediaResource might go away before mCallback->NotifyDataArrived()
is run. This also avoid the data race in accessing mDataReceivedEvent on different threads
when OMT data delivery is enabled.

MozReview-Commit-ID: 1Tbjxrm1ar5

diff --git a/dom/media/MediaResource.cpp b/dom/media/MediaResource.cpp
--- a/dom/media/MediaResource.cpp
+++ b/dom/media/MediaResource.cpp
@@ -866,39 +866,23 @@ ChannelMediaResource::RecreateChannel()
 
   // Tell the cache to reset the download status when the channel is reopened.
   mCacheStream.NotifyChannelRecreated();
 
   return rv;
 }
 
 void
-ChannelMediaResource::DoNotifyDataReceived()
-{
-  mDataReceivedEvent.Revoke();
-  mCallback->NotifyDataArrived();
-}
-
-void
 ChannelMediaResource::CacheClientNotifyDataReceived()
 {
-  NS_ASSERTION(NS_IsMainThread(), "Don't call on non-main thread");
-  // NOTE: this can be called with the media cache lock held, so don't
-  // block or do anything which might try to acquire a lock!
-
-  if (mDataReceivedEvent.IsPending())
-    return;
-
-  mDataReceivedEvent =
-    NewNonOwningRunnableMethod("ChannelMediaResource::DoNotifyDataReceived",
-                               this, &ChannelMediaResource::DoNotifyDataReceived);
-
-  nsCOMPtr<nsIRunnable> event = mDataReceivedEvent.get();
-
-  SystemGroup::AbstractMainThreadFor(TaskCategory::Other)->Dispatch(event.forget());
+  SystemGroup::Dispatch(
+    TaskCategory::Other,
+    NewRunnableMethod("MediaResourceCallback::NotifyDataArrived",
+                      mCallback.get(),
+                      &MediaResourceCallback::NotifyDataArrived));
 }
 
 void
 ChannelMediaResource::CacheClientNotifyDataEnded(nsresult aStatus)
 {
   MOZ_ASSERT(NS_IsMainThread());
   mCallback->NotifyDataEnded(aStatus);
 }
diff --git a/dom/media/MediaResource.h b/dom/media/MediaResource.h
--- a/dom/media/MediaResource.h
+++ b/dom/media/MediaResource.h
@@ -488,17 +488,16 @@ public:
   int64_t GetCachedDataEnd(int64_t aOffset) override;
   bool    IsDataCachedToEndOfResource(int64_t aOffset) override;
   bool    IsTransportSeekable() override;
 
   size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const override {
     // Might be useful to track in the future:
     //   - mListener (seems minor)
     //   - mChannelStatistics (seems minor)
-    //   - mDataReceivedEvent (seems minor)
     size_t size = BaseMediaResource::SizeOfExcludingThis(aMallocSizeOf);
     size += mCacheStream.SizeOfExcludingThis(aMallocSizeOf);
 
     return size;
   }
 
   size_t SizeOfIncludingThis(MallocSizeOf aMallocSizeOf) const override {
     return aMallocSizeOf(this) + SizeOfExcludingThis(aMallocSizeOf);
@@ -552,36 +551,31 @@ protected:
   // Parses 'Content-Range' header and returns results via parameters.
   // Returns error if header is not available, values are not parse-able or
   // values are out of range.
   nsresult ParseContentRangeHeader(nsIHttpChannel * aHttpChan,
                                    int64_t& aRangeStart,
                                    int64_t& aRangeEnd,
                                    int64_t& aRangeTotal);
 
-  void DoNotifyDataReceived();
-
   static nsresult CopySegmentToCache(nsIInputStream* aInStream,
                                      void* aClosure,
                                      const char* aFromSegment,
                                      uint32_t aToOffset,
                                      uint32_t aCount,
                                      uint32_t* aWriteCount);
 
   nsresult CopySegmentToCache(nsIPrincipal* aPrincipal,
                               const char* aFromSegment,
                               uint32_t aCount,
                               uint32_t* aWriteCount);
 
   // Main thread access only
   int64_t            mOffset;
   RefPtr<Listener> mListener;
-  // A data received event for the decoder that has been dispatched but has
-  // not yet been processed.
-  nsRevocableEventPtr<nsRunnableMethod<ChannelMediaResource, void, false> > mDataReceivedEvent;
   // When this flag is set, if we get a network error we should silently
   // reopen the stream.
   bool               mReopenOnError;
   // When this flag is set, we should not report the next close of the
   // channel.
   bool               mIgnoreClose;
 
   // Any thread access
