# HG changeset patch
# User Simon Sapin <simon.sapin@exyr.org>
# Date 1503710698 18000
#      Fri Aug 25 20:24:58 2017 -0500
# Node ID 75a20ebcd6e8c43b2f0c332b6d5ce947ba1f5ad7
# Parent  6726008af0fc05a3afeb9de6041c40d49f6d598c
servo: Merge #18171 - Use Parser::skip_whitespace in a few places to make Parser::try rewind less (from servo:skip_whitespace); r=emilio

**Do not merge yet.** This pulls in unrelated cssparser changes which add a dependency to 'dota', and we'd like to resolve https://github.com/dtolnay/dtoa/pull/9 before landing dtoa in mozilla-central. Also, the dependency change may require manually revendoring in mozilla-central.

Gecko's CSS parsing microbenchmarks before:

'''
  43.437 +-  0.391 ms    Stylo.Servo_StyleSheet_FromUTF8Bytes_Bench
  29.244 +-  0.042 ms    Stylo.Gecko_nsCSSParser_ParseSheet_Bench
 281.884 +-  0.028 ms    Stylo.Servo_DeclarationBlock_SetPropertyById_Bench
 426.242 +-  0.008 ms    Stylo.Servo_DeclarationBlock_SetPropertyById_WithInitialSpace_Bench
'''

After:

'''
  29.779 +-  0.254 ms    Stylo.Servo_StyleSheet_FromUTF8Bytes_Bench
  28.841 +-  0.031 ms    Stylo.Gecko_nsCSSParser_ParseSheet_Bench
 296.240 +-  4.744 ms    Stylo.Servo_DeclarationBlock_SetPropertyById_Bench
 293.855 +-  4.304 ms    Stylo.Servo_DeclarationBlock_SetPropertyById_WithInitialSpace_Bench
'''

Source-Repo: https://github.com/servo/servo
Source-Revision: 8812422bfa504633a7011b0b002e9a2682c7d045

diff --git a/servo/components/selectors/Cargo.toml b/servo/components/selectors/Cargo.toml
--- a/servo/components/selectors/Cargo.toml
+++ b/servo/components/selectors/Cargo.toml
@@ -20,17 +20,17 @@ doctest = false
 
 [features]
 gecko_like_types = []
 unstable = []
 
 [dependencies]
 bitflags = "0.7"
 matches = "0.1"
-cssparser = "0.19"
+cssparser = "0.19.3"
 log = "0.3"
 fnv = "1.0"
 phf = "0.7.18"
 precomputed-hash = "0.1"
 servo_arc = { path = "../servo_arc" }
 smallvec = "0.4"
 
 [dev-dependencies]
diff --git a/servo/components/selectors/parser.rs b/servo/components/selectors/parser.rs
--- a/servo/components/selectors/parser.rs
+++ b/servo/components/selectors/parser.rs
@@ -1420,24 +1420,17 @@ fn parse_negation<'i, 't, P, E, Impl>(pa
                                       input: &mut CssParser<'i, 't>)
                                       -> Result<Component<Impl>,
                                                 ParseError<'i, SelectorParseError<'i, E>>>
     where P: Parser<'i, Impl=Impl, Error=E>, Impl: SelectorImpl
 {
     // We use a sequence because a type selector may be represented as two Components.
     let mut sequence = SmallVec::<[Component<Impl>; 2]>::new();
 
-    // Consume any leading whitespace.
-    loop {
-        let before_this_token = input.state();
-        if !matches!(input.next_including_whitespace(), Ok(&Token::WhiteSpace(_))) {
-            input.reset(&before_this_token);
-            break
-        }
-    }
+    input.skip_whitespace();
 
     // Get exactly one simple selector. The parse logic in the caller will verify
     // that there are no trailing tokens after we're done.
     if !parse_type_selector(parser, input, &mut sequence)? {
         match parse_one_simple_selector(parser, input, /* inside_negation = */ true)? {
             Some(SimpleSelectorParseResult::SimpleSelector(s)) => {
                 sequence.push(s);
             },
@@ -1463,24 +1456,18 @@ fn parse_negation<'i, 't, P, E, Impl>(pa
 /// The boolean represent whether a pseudo-element has been parsed.
 fn parse_compound_selector<'i, 't, P, E, Impl>(
     parser: &P,
     input: &mut CssParser<'i, 't>,
     builder: &mut SelectorBuilder<Impl>)
     -> Result<bool, ParseError<'i, SelectorParseError<'i, E>>>
     where P: Parser<'i, Impl=Impl, Error=E>, Impl: SelectorImpl
 {
-    // Consume any leading whitespace.
-    loop {
-        let before_this_token = input.state();
-        if !matches!(input.next_including_whitespace(), Ok(&Token::WhiteSpace(_))) {
-            input.reset(&before_this_token);
-            break
-        }
-    }
+    input.skip_whitespace();
+
     let mut empty = true;
     if !parse_type_selector(parser, input, builder)? {
         if let Some(url) = parser.default_namespace() {
             // If there was no explicit type selector, but there is a
             // default namespace, there is an implicit "<defaultns>|*" type
             // selector.
             builder.push_simple_selector(Component::DefaultNamespace(url))
         }
diff --git a/servo/components/style/Cargo.toml b/servo/components/style/Cargo.toml
--- a/servo/components/style/Cargo.toml
+++ b/servo/components/style/Cargo.toml
@@ -32,17 +32,17 @@ gecko_debug = ["nsstring_vendor/gecko_de
 app_units = "0.5.6"
 arrayvec = "0.3.20"
 arraydeque = "0.2.3"
 atomic_refcell = "0.1"
 bitflags = "0.7"
 bit-vec = "0.4.3"
 byteorder = "1.0"
 cfg-if = "0.1.0"
-cssparser = "0.19"
+cssparser = "0.19.3"
 encoding = {version = "0.2", optional = true}
 euclid = "0.15"
 fnv = "1.0"
 heapsize = {version = "0.4", optional = true}
 heapsize_derive = {version = "0.1", optional = true}
 itertools = "0.5"
 itoa = "0.3"
 html5ever = {version = "0.19", optional = true}
diff --git a/servo/components/style/properties/properties.mako.rs b/servo/components/style/properties/properties.mako.rs
--- a/servo/components/style/properties/properties.mako.rs
+++ b/servo/components/style/properties/properties.mako.rs
@@ -1542,35 +1542,39 @@ impl PropertyDeclaration {
     ///
     /// This will not actually parse Importance values, and will always set things
     /// to Importance::Normal. Parsing Importance values is the job of PropertyDeclarationParser,
     /// we only set them here so that we don't have to reallocate
     pub fn parse_into<'i, 't>(declarations: &mut SourcePropertyDeclaration,
                               id: PropertyId, context: &ParserContext, input: &mut Parser<'i, 't>)
                               -> Result<(), PropertyDeclarationParseError<'i>> {
         assert!(declarations.is_empty());
+        let start = input.state();
         match id {
             PropertyId::Custom(name) => {
+                // FIXME: fully implement https://github.com/w3c/csswg-drafts/issues/774
+                // before adding skip_whitespace here.
+                // This probably affects some test results.
                 let value = match input.try(|i| CSSWideKeyword::parse(i)) {
                     Ok(keyword) => DeclaredValueOwned::CSSWideKeyword(keyword),
                     Err(()) => match ::custom_properties::SpecifiedValue::parse(context, input) {
                         Ok(value) => DeclaredValueOwned::Value(value),
                         Err(e) => return Err(PropertyDeclarationParseError::InvalidValue(name.to_string().into(),
                         ValueParseError::from_parse_error(e))),
                     }
                 };
                 declarations.push(PropertyDeclaration::Custom(name, value));
                 Ok(())
             }
             PropertyId::Longhand(id) => {
+                input.skip_whitespace();  // Unnecessary for correctness, but may help try() rewind less.
                 input.try(|i| CSSWideKeyword::parse(i)).map(|keyword| {
                     PropertyDeclaration::CSSWideKeyword(id, keyword)
                 }).or_else(|()| {
                     input.look_for_var_functions();
-                    let start = input.state();
                     input.parse_entirely(|input| id.parse_value(context, input))
                     .or_else(|err| {
                         while let Ok(_) = input.next() {}  // Look for var() after the error.
                         if input.seen_var_functions() {
                             input.reset(&start);
                             let (first_token_type, css) =
                                 ::custom_properties::parse_non_custom_with_var(input).map_err(|e| {
                                     PropertyDeclarationParseError::InvalidValue(id.name().into(),
@@ -1587,28 +1591,28 @@ impl PropertyDeclaration {
                                 ValueParseError::from_parse_error(err)))
                         }
                     })
                 }).map(|declaration| {
                     declarations.push(declaration)
                 })
             }
             PropertyId::Shorthand(id) => {
+                input.skip_whitespace();  // Unnecessary for correctness, but may help try() rewind less.
                 if let Ok(keyword) = input.try(|i| CSSWideKeyword::parse(i)) {
                     if id == ShorthandId::All {
                         declarations.all_shorthand = AllShorthand::CSSWideKeyword(keyword)
                     } else {
                         for &longhand in id.longhands() {
                             declarations.push(PropertyDeclaration::CSSWideKeyword(longhand, keyword))
                         }
                     }
                     Ok(())
                 } else {
                     input.look_for_var_functions();
-                    let start = input.state();
                     // Not using parse_entirely here: each ${shorthand.ident}::parse_into function
                     // needs to do so *before* pushing to `declarations`.
                     id.parse_into(declarations, context, input).or_else(|err| {
                         while let Ok(_) = input.next() {}  // Look for var() after the error.
                         if input.seen_var_functions() {
                             input.reset(&start);
                             let (first_token_type, css) =
                                 ::custom_properties::parse_non_custom_with_var(input).map_err(|e| {
diff --git a/servo/components/style_traits/values.rs b/servo/components/style_traits/values.rs
--- a/servo/components/style_traits/values.rs
+++ b/servo/components/style_traits/values.rs
@@ -241,39 +241,47 @@ impl Separator for Space {
 
     fn parse<'i, 't, F, T, E>(
         input: &mut Parser<'i, 't>,
         mut parse_one: F,
     ) -> Result<Vec<T>, ParseError<'i, E>>
     where
         F: for<'tt> FnMut(&mut Parser<'i, 'tt>) -> Result<T, ParseError<'i, E>>
     {
+        input.skip_whitespace();  // Unnecessary for correctness, but may help try() rewind less.
         let mut results = vec![parse_one(input)?];
-        while let Ok(item) = input.try(&mut parse_one) {
-            results.push(item);
+        loop {
+            input.skip_whitespace();  // Unnecessary for correctness, but may help try() rewind less.
+            if let Ok(item) = input.try(&mut parse_one) {
+                results.push(item);
+            } else {
+                return Ok(results)
+            }
         }
-        Ok(results)
     }
 }
 
 impl Separator for CommaWithSpace {
     fn separator() -> &'static str {
         ", "
     }
 
     fn parse<'i, 't, F, T, E>(
         input: &mut Parser<'i, 't>,
         mut parse_one: F,
     ) -> Result<Vec<T>, ParseError<'i, E>>
     where
         F: for<'tt> FnMut(&mut Parser<'i, 'tt>) -> Result<T, ParseError<'i, E>>
     {
+        input.skip_whitespace();  // Unnecessary for correctness, but may help try() rewind less.
         let mut results = vec![parse_one(input)?];
         loop {
+            input.skip_whitespace();  // Unnecessary for correctness, but may help try() rewind less.
             let comma = input.try(|i| i.expect_comma()).is_ok();
+            input.skip_whitespace();  // Unnecessary for correctness, but may help try() rewind less.
             if let Ok(item) = input.try(&mut parse_one) {
                 results.push(item);
             } else if comma {
                 return Err(BasicParseError::UnexpectedToken(Token::Comma).into());
             } else {
                 break;
             }
         }
