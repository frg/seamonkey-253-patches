# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1519773783 28800
#      Tue Feb 27 15:23:03 2018 -0800
# Node ID 4a4ea07f646712675cc96ce5bd123099e2ecde20
# Parent  6252ddb1aa426f7cd8b8685dac55ccc66bde18c7
Bug 1442363, part 4 - Constify XPTInterfaceDirectoryEntry::interface_descriptor. r=njn

MozReview-Commit-ID: E4DFA4dodK3

diff --git a/xpcom/reflect/xptinfo/xptiInterfaceInfo.cpp b/xpcom/reflect/xptinfo/xptiInterfaceInfo.cpp
--- a/xpcom/reflect/xptinfo/xptiInterfaceInfo.cpp
+++ b/xpcom/reflect/xptinfo/xptiInterfaceInfo.cpp
@@ -13,29 +13,29 @@
 #include "mozilla/PodOperations.h"
 #include "jsapi.h"
 
 using namespace mozilla;
 
 /* static */ xptiInterfaceEntry*
 xptiInterfaceEntry::Create(const char* aName,
                            const nsID& aIID,
-                           XPTInterfaceDescriptor* aDescriptor,
+                           const XPTInterfaceDescriptor* aDescriptor,
                            xptiTypelibGuts* aTypelib)
 {
     void* place = XPT_CALLOC8(gXPTIStructArena, sizeof(xptiInterfaceEntry));
     if (!place) {
         return nullptr;
     }
     return new (place) xptiInterfaceEntry(aName, aIID, aDescriptor, aTypelib);
 }
 
 xptiInterfaceEntry::xptiInterfaceEntry(const char* aName,
                                        const nsID& aIID,
-                                       XPTInterfaceDescriptor* aDescriptor,
+                                       const XPTInterfaceDescriptor* aDescriptor,
                                        xptiTypelibGuts* aTypelib)
     : mIID(aIID)
     , mDescriptor(aDescriptor)
     , mTypelib(aTypelib)
     , mParent(nullptr)
     , mInfo(nullptr)
     , mMethodBaseIndex(0)
     , mConstantBaseIndex(0)
diff --git a/xpcom/reflect/xptinfo/xptiprivate.h b/xpcom/reflect/xptinfo/xptiprivate.h
--- a/xpcom/reflect/xptinfo/xptiprivate.h
+++ b/xpcom/reflect/xptinfo/xptiprivate.h
@@ -164,17 +164,17 @@ private:
 // We always create in the struct arena and construct using "placement new".
 // No members need dtor calls.
 
 class xptiInterfaceEntry
 {
 public:
     static xptiInterfaceEntry* Create(const char* aName,
                                       const nsID& aIID,
-                                      XPTInterfaceDescriptor* aDescriptor,
+                                      const XPTInterfaceDescriptor* aDescriptor,
                                       xptiTypelibGuts* aTypelib);
 
     enum {
         PARTIALLY_RESOLVED    = 1,
         FULLY_RESOLVED        = 2,
         RESOLVE_FAILED        = 3
     };
 
@@ -262,17 +262,17 @@ public:
     nsresult GetIIDShared(const nsIID * *iid);
     nsresult IsFunction(bool *_retval);
     nsresult HasAncestor(const nsIID * iid, bool *_retval);
     nsresult GetIIDForParamNoAlloc(uint16_t methodIndex, const nsXPTParamInfo * param, nsIID *iid);
 
 private:
     xptiInterfaceEntry(const char* aName,
                        const nsID& aIID,
-                       XPTInterfaceDescriptor* aDescriptor,
+                       const XPTInterfaceDescriptor* aDescriptor,
                        xptiTypelibGuts* aTypelib);
     ~xptiInterfaceEntry();
 
     void SetResolvedState(int state)
         {mFlags.SetState(uint8_t(state));}
 
     bool Resolve();
 
@@ -298,17 +298,17 @@ private:
                                        const nsXPTParamInfo* param,
                                        uint16_t* interfaceIndex);
 
     already_AddRefed<ShimInterfaceInfo>
     GetShimForParam(uint16_t methodIndex, const nsXPTParamInfo* param);
 
 private:
     nsID                    mIID;
-    XPTInterfaceDescriptor* mDescriptor;
+    const XPTInterfaceDescriptor* mDescriptor;
 
     xptiTypelibGuts* mTypelib;
 
     xptiInterfaceEntry*     mParent;      // Valid only when fully resolved
 
     xptiInterfaceInfo* MOZ_UNSAFE_REF("The safety of this pointer is ensured "
                                       "by the semantics of xptiWorkingSet.")
                             mInfo;        // May come and go.
diff --git a/xpcom/typelib/xpt/xpt_struct.cpp b/xpcom/typelib/xpt/xpt_struct.cpp
--- a/xpcom/typelib/xpt/xpt_struct.cpp
+++ b/xpcom/typelib/xpt/xpt_struct.cpp
@@ -61,17 +61,17 @@ static bool
 DoMethodDescriptor(XPTArena *arena, NotNull<XPTCursor*> cursor,
                    XPTMethodDescriptor *md, XPTInterfaceDescriptor *id);
 
 static bool
 SkipAnnotation(NotNull<XPTCursor*> cursor, bool *isLast);
 
 static bool
 DoInterfaceDescriptor(XPTArena *arena, NotNull<XPTCursor*> outer,
-                      XPTInterfaceDescriptor **idp);
+                      const XPTInterfaceDescriptor **idp);
 
 static bool
 DoTypeDescriptorPrefix(XPTArena *arena, NotNull<XPTCursor*> cursor,
                        XPTTypeDescriptorPrefix *tdp);
 
 static bool
 DoTypeDescriptor(XPTArena *arena, NotNull<XPTCursor*> cursor,
                  XPTTypeDescriptor *td, XPTInterfaceDescriptor *id);
@@ -228,17 +228,17 @@ InterfaceDescriptorAddType(XPTArena *are
         return false;
 
     id->num_additional_types += 1;
     return true;
 }
 
 bool
 DoInterfaceDescriptor(XPTArena *arena, NotNull<XPTCursor*> outer,
-                      XPTInterfaceDescriptor **idp)
+                      const XPTInterfaceDescriptor **idp)
 {
     XPTInterfaceDescriptor *id;
     XPTCursor curs;
     NotNull<XPTCursor*> cursor = WrapNotNull(&curs);
     uint32_t i, id_sz = 0;
 
     id = XPT_NEWZAP(arena, XPTInterfaceDescriptor);
     if (!id)
diff --git a/xpcom/typelib/xpt/xpt_struct.h b/xpcom/typelib/xpt/xpt_struct.h
--- a/xpcom/typelib/xpt/xpt_struct.h
+++ b/xpcom/typelib/xpt/xpt_struct.h
@@ -73,17 +73,17 @@ struct XPTHeader {
 struct XPTInterfaceDirectoryEntry {
   nsID iid;
   const char* name;
 
   // This field exists in the on-disk format. But it isn't used so we don't
   // allocate space for it in memory.
   //const char* name_space;
 
-  XPTInterfaceDescriptor* interface_descriptor;
+  const XPTInterfaceDescriptor* interface_descriptor;
 };
 
 /*
  * An InterfaceDescriptor describes a single XPCOM interface, including all of
  * its methods.
  */
 struct XPTInterfaceDescriptor {
   static const uint8_t kScriptableMask =                0x80;
