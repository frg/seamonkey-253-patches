# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1505324095 14400
#      Wed Sep 13 13:34:55 2017 -0400
# Node ID 3105c934288537f39fc0a29bfc941d21bc1c3e8c
# Parent  afdc5b2921b7e3be8aac976ba7c9e7c9af2ecd3d
Bug 1393806 part 1.  Change nsNodeUtils cloning/adopting stuff to use an ErrorResult for errors.  r=peterv

This will allow us to propagate out more informative errors in some cases.

MozReview-Commit-ID: 9FbzV5VRMqN

diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -6780,23 +6780,17 @@ nsIDocument::ImportNode(nsINode& aNode, 
     case nsIDOMNode::ATTRIBUTE_NODE:
     case nsIDOMNode::ELEMENT_NODE:
     case nsIDOMNode::PROCESSING_INSTRUCTION_NODE:
     case nsIDOMNode::TEXT_NODE:
     case nsIDOMNode::CDATA_SECTION_NODE:
     case nsIDOMNode::COMMENT_NODE:
     case nsIDOMNode::DOCUMENT_TYPE_NODE:
     {
-      nsCOMPtr<nsINode> newNode;
-      rv = nsNodeUtils::Clone(imported, aDeep, mNodeInfoManager, nullptr,
-                              getter_AddRefs(newNode));
-      if (rv.Failed()) {
-        return nullptr;
-      }
-      return newNode.forget();
+      return nsNodeUtils::Clone(imported, aDeep, mNodeInfoManager, nullptr, rv);
     }
     default:
     {
       NS_WARNING("Don't know how to clone this nodetype for importNode.");
     }
   }
 
   rv.Throw(NS_ERROR_DOM_NOT_SUPPORTED_ERR);
@@ -7967,18 +7961,18 @@ nsIDocument::AdoptNode(nsINode& aAdopted
                                       /* aAllowWrapping = */ false);
       if (rv.Failed())
         return nullptr;
       newScope = &v.toObject();
     }
   }
 
   nsCOMArray<nsINode> nodesWithProperties;
-  rv = nsNodeUtils::Adopt(adoptedNode, sameDocument ? nullptr : mNodeInfoManager,
-                          newScope, nodesWithProperties);
+  nsNodeUtils::Adopt(adoptedNode, sameDocument ? nullptr : mNodeInfoManager,
+                     newScope, nodesWithProperties, rv);
   if (rv.Failed()) {
     // Disconnect all nodes from their parents, since some have the old document
     // as their ownerDocument and some have this as their ownerDocument.
     nsDOMAttributeMap::BlastSubtreeToPieces(adoptedNode);
 
     if (!sameDocument && oldDocument) {
       uint32_t count = nodesWithProperties.Count();
       for (uint32_t j = 0; j < oldDocument->GetPropertyTableCount(); ++j) {
diff --git a/dom/base/nsINode.cpp b/dom/base/nsINode.cpp
--- a/dom/base/nsINode.cpp
+++ b/dom/base/nsINode.cpp
@@ -2971,19 +2971,17 @@ nsINode::WrapObject(JSContext *aCx, JS::
                 xpc::IsInContentXBLScope(obj) ||
                 !xpc::UseContentXBLScope(JS::GetObjectRealmOrNull(obj)));
   return obj;
 }
 
 already_AddRefed<nsINode>
 nsINode::CloneNode(bool aDeep, ErrorResult& aError)
 {
-  nsCOMPtr<nsINode> result;
-  aError = nsNodeUtils::CloneNodeImpl(this, aDeep, getter_AddRefs(result));
-  return result.forget();
+  return nsNodeUtils::CloneNodeImpl(this, aDeep, aError);
 }
 
 nsDOMAttributeMap*
 nsINode::GetAttributes()
 {
   if (!IsElement()) {
     return nullptr;
   }
diff --git a/dom/base/nsNodeUtils.cpp b/dom/base/nsNodeUtils.cpp
--- a/dom/base/nsNodeUtils.cpp
+++ b/dom/base/nsNodeUtils.cpp
@@ -21,16 +21,17 @@
 #include "nsDocument.h"
 #ifdef MOZ_XUL
 #include "nsXULElement.h"
 #endif
 #include "nsBindingManager.h"
 #include "nsGenericHTMLElement.h"
 #include "mozilla/AnimationTarget.h"
 #include "mozilla/Assertions.h"
+#include "mozilla/ErrorResult.h"
 #include "mozilla/dom/Animation.h"
 #include "mozilla/dom/HTMLImageElement.h"
 #include "mozilla/dom/HTMLMediaElement.h"
 #include "mozilla/dom/KeyframeEffectReadOnly.h"
 #include "nsWrapperCacheInlines.h"
 #include "nsObjectLoadingContent.h"
 #include "nsDOMMutationObserver.h"
 #include "mozilla/dom/BindingUtils.h"
@@ -391,87 +392,86 @@ void
 nsNodeUtils::TraverseUserData(nsINode* aNode,
                               nsCycleCollectionTraversalCallback &aCb)
 {
   nsIDocument* ownerDoc = aNode->OwnerDoc();
   ownerDoc->PropertyTable(DOM_USER_DATA)->Enumerate(aNode, NoteUserData, &aCb);
 }
 
 /* static */
-nsresult
-nsNodeUtils::CloneNodeImpl(nsINode *aNode, bool aDeep, nsINode **aResult)
+already_AddRefed<nsINode>
+nsNodeUtils::CloneNodeImpl(nsINode *aNode, bool aDeep, ErrorResult& aError)
 {
-  *aResult = nullptr;
-
-  nsCOMPtr<nsINode> newNode;
-  nsresult rv = Clone(aNode, aDeep, nullptr, nullptr, getter_AddRefs(newNode));
-  NS_ENSURE_SUCCESS(rv, rv);
-
-  newNode.forget(aResult);
-  return NS_OK;
+  return Clone(aNode, aDeep, nullptr, nullptr, aError);
 }
 
 /* static */
-nsresult
+already_AddRefed<nsINode>
 nsNodeUtils::CloneAndAdopt(nsINode *aNode, bool aClone, bool aDeep,
                            nsNodeInfoManager *aNewNodeInfoManager,
                            JS::Handle<JSObject*> aReparentScope,
                            nsCOMArray<nsINode> *aNodesWithProperties,
-                           nsINode *aParent, nsINode **aResult)
+                           nsINode* aParent, ErrorResult& aError)
 {
   NS_PRECONDITION((!aClone && aNewNodeInfoManager) || !aReparentScope,
                   "If cloning or not getting a new nodeinfo we shouldn't "
                   "rewrap");
   NS_PRECONDITION(!aParent || aNode->IsNodeOfType(nsINode::eCONTENT),
                   "Can't insert document or attribute nodes into a parent");
 
-  *aResult = nullptr;
-
   // First deal with aNode and walk its attributes (and their children). Then,
   // if aDeep is true, deal with aNode's children (and recurse into their
   // attributes and children).
 
   nsAutoScriptBlocker scriptBlocker;
-  nsresult rv;
 
   nsNodeInfoManager *nodeInfoManager = aNewNodeInfoManager;
 
   // aNode.
   NodeInfo *nodeInfo = aNode->mNodeInfo;
   RefPtr<NodeInfo> newNodeInfo;
   if (nodeInfoManager) {
 
     // Don't allow importing/adopting nodes from non-privileged "scriptable"
     // documents to "non-scriptable" documents.
     nsIDocument* newDoc = nodeInfoManager->GetDocument();
-    NS_ENSURE_STATE(newDoc);
+    if (NS_WARN_IF(!newDoc)) {
+      aError.Throw(NS_ERROR_UNEXPECTED);
+      return nullptr;
+    }
     bool hasHadScriptHandlingObject = false;
     if (!newDoc->GetScriptHandlingObject(hasHadScriptHandlingObject) &&
         !hasHadScriptHandlingObject) {
       nsIDocument* currentDoc = aNode->OwnerDoc();
-      NS_ENSURE_STATE((nsContentUtils::IsChromeDoc(currentDoc) ||
-                       (!currentDoc->GetScriptHandlingObject(hasHadScriptHandlingObject) &&
-                        !hasHadScriptHandlingObject)));
+      if (NS_WARN_IF(!nsContentUtils::IsChromeDoc(currentDoc) &&
+                     (currentDoc->GetScriptHandlingObject(hasHadScriptHandlingObject) ||
+                      hasHadScriptHandlingObject))) {
+        aError.Throw(NS_ERROR_UNEXPECTED);
+        return nullptr;
+      }
     }
 
     newNodeInfo = nodeInfoManager->GetNodeInfo(nodeInfo->NameAtom(),
                                                nodeInfo->GetPrefixAtom(),
                                                nodeInfo->NamespaceID(),
                                                nodeInfo->NodeType(),
                                                nodeInfo->GetExtraName());
 
     nodeInfo = newNodeInfo;
   }
 
   Element *elem = aNode->IsElement() ? aNode->AsElement() : nullptr;
 
   nsCOMPtr<nsINode> clone;
   if (aClone) {
-    rv = aNode->Clone(nodeInfo, getter_AddRefs(clone), aDeep);
-    NS_ENSURE_SUCCESS(rv, rv);
+    nsresult rv = aNode->Clone(nodeInfo, getter_AddRefs(clone), aDeep);
+    if (NS_WARN_IF(NS_FAILED(rv))) {
+      aError.Throw(rv);
+      return nullptr;
+    }
 
     if (clone->IsElement()) {
       // The cloned node may be a custom element that may require
       // enqueing created callback and prototype swizzling.
       Element* elem = clone->AsElement();
       if (nsContentUtils::IsCustomElementName(nodeInfo->NameAtom())) {
         nsContentUtils::SetupCustomElement(elem);
       } else {
@@ -485,17 +485,20 @@ nsNodeUtils::CloneAndAdopt(nsINode *aNod
       }
     }
 
     if (aParent) {
       // If we're cloning we need to insert the cloned children into the cloned
       // parent.
       rv = aParent->AppendChildTo(static_cast<nsIContent*>(clone.get()),
                                   false);
-      NS_ENSURE_SUCCESS(rv, rv);
+      if (NS_WARN_IF(NS_FAILED(rv))) {
+        aError.Throw(rv);
+        return nullptr;
+      }
     }
     else if (aDeep && clone->IsNodeOfType(nsINode::eDOCUMENT)) {
       // After cloning the document itself, we want to clone the children into
       // the cloned document (somewhat like cloning and importing them into the
       // cloned document).
       nodeInfoManager = clone->mNodeInfo->NodeInfoManager();
     }
   }
@@ -570,26 +573,27 @@ nsNodeUtils::CloneAndAdopt(nsINode *aNod
     }
 
     if (aReparentScope) {
       AutoJSContext cx;
       JS::Rooted<JSObject*> wrapper(cx);
       if ((wrapper = aNode->GetWrapper())) {
         MOZ_ASSERT(IsDOMObject(wrapper));
         JSAutoCompartment ac(cx, wrapper);
-        rv = ReparentWrapper(cx, wrapper);
+        nsresult rv = ReparentWrapper(cx, wrapper);
         if (NS_FAILED(rv)) {
           if (wasRegistered) {
             aNode->OwnerDoc()->UnregisterActivityObserver(aNode->AsElement());
           }
           aNode->mNodeInfo.swap(newNodeInfo);
           if (wasRegistered) {
             aNode->OwnerDoc()->RegisterActivityObserver(aNode->AsElement());
           }
-          return rv;
+          aError.Throw(rv);
+          return nullptr;
         }
       }
     }
   }
 
   if (aNodesWithProperties && aNode->HasProperties()) {
     bool ok = aNodesWithProperties->AppendObject(aNode);
     MOZ_RELEASE_ASSERT(ok, "Out of memory");
@@ -599,21 +603,23 @@ nsNodeUtils::CloneAndAdopt(nsINode *aNod
     }
   }
 
   if (aDeep && (!aClone || !aNode->IsNodeOfType(nsINode::eATTRIBUTE))) {
     // aNode's children.
     for (nsIContent* cloneChild = aNode->GetFirstChild();
          cloneChild;
          cloneChild = cloneChild->GetNextSibling()) {
-      nsCOMPtr<nsINode> child;
-      rv = CloneAndAdopt(cloneChild, aClone, true, nodeInfoManager,
-                         aReparentScope, aNodesWithProperties, clone,
-                         getter_AddRefs(child));
-      NS_ENSURE_SUCCESS(rv, rv);
+      nsCOMPtr<nsINode> child =
+        CloneAndAdopt(cloneChild, aClone, true, nodeInfoManager,
+                      aReparentScope, aNodesWithProperties, clone,
+                      aError);
+      if (NS_WARN_IF(aError.Failed())) {
+        return nullptr;
+      }
     }
   }
 
   // Cloning template element.
   if (aDeep && aClone && IsTemplateElement(aNode)) {
     DocumentFragment* origContent =
       static_cast<HTMLTemplateElement*>(aNode)->Content();
     DocumentFragment* cloneContent =
@@ -622,21 +628,23 @@ nsNodeUtils::CloneAndAdopt(nsINode *aNod
     // Clone the children into the clone's template content owner
     // document's nodeinfo manager.
     nsNodeInfoManager* ownerNodeInfoManager =
       cloneContent->mNodeInfo->NodeInfoManager();
 
     for (nsIContent* cloneChild = origContent->GetFirstChild();
          cloneChild;
          cloneChild = cloneChild->GetNextSibling()) {
-      nsCOMPtr<nsINode> child;
-      rv = CloneAndAdopt(cloneChild, aClone, aDeep, ownerNodeInfoManager,
-                         aReparentScope, aNodesWithProperties, cloneContent,
-                         getter_AddRefs(child));
-      NS_ENSURE_SUCCESS(rv, rv);
+      nsCOMPtr<nsINode> child =
+        CloneAndAdopt(cloneChild, aClone, aDeep, ownerNodeInfoManager,
+                      aReparentScope, aNodesWithProperties, cloneContent,
+                      aError);
+      if (NS_WARN_IF(aError.Failed())) {
+        return nullptr;
+      }
     }
   }
 
   // XXX setting document on some nodes not in a document so XBL will bind
   // and chrome won't break. Make XBL bind to document-less nodes!
   // XXXbz Once this is fixed, fix up the asserts in all implementations of
   // BindToTree to assert what they would like to assert, and fix the
   // ChangeDocumentFor() call in nsXULElement::BindToTree as well.  Also,
@@ -649,19 +657,17 @@ nsNodeUtils::CloneAndAdopt(nsINode *aNod
 #ifdef MOZ_XUL
   if (aClone && !aParent && aNode->IsXULElement()) {
     if (!aNode->OwnerDoc()->IsLoadedAsInteractiveData()) {
       clone->SetFlags(NODE_FORCE_XBL_BINDINGS);
     }
   }
 #endif
 
-  clone.forget(aResult);
-
-  return NS_OK;
+  return clone.forget();
 }
 
 
 /* static */
 void
 nsNodeUtils::UnlinkUserData(nsINode *aNode)
 {
   NS_ASSERTION(aNode->HasProperties(), "Call to UnlinkUserData not needed.");
diff --git a/dom/base/nsNodeUtils.h b/dom/base/nsNodeUtils.h
--- a/dom/base/nsNodeUtils.h
+++ b/dom/base/nsNodeUtils.h
@@ -13,16 +13,17 @@
 #include "js/TypeDecls.h"
 #include "nsCOMArray.h"
 
 struct CharacterDataChangeInfo;
 template<class E> class nsCOMArray;
 class nsCycleCollectionTraversalCallback;
 namespace mozilla {
 struct NonOwningAnimationTarget;
+class ErrorResult;
 namespace dom {
 class Animation;
 } // namespace dom
 } // namespace mozilla
 
 class nsNodeUtils
 {
 public:
@@ -178,34 +179,27 @@ public:
    * @param aNewNodeInfoManager The nodeinfo manager to use to create new
    *                            nodeinfos for aNode and its attributes and
    *                            descendants. May be null if the nodeinfos
    *                            shouldn't be changed.
    * @param aNodesWithProperties All nodes (from amongst aNode and its
    *                             descendants) with properties. Every node will
    *                             be followed by its clone. Null can be passed to
    *                             prevent this from being used.
-   * @param aResult *aResult will contain the cloned node.
+   * @param aError The error, if any.
+   *
+   * @return The newly created node.  Null in error conditions.
    */
-  static nsresult Clone(nsINode *aNode, bool aDeep,
-                        nsNodeInfoManager *aNewNodeInfoManager,
-                        nsCOMArray<nsINode> *aNodesWithProperties,
-                        nsINode **aResult)
+  static already_AddRefed<nsINode> Clone(nsINode *aNode, bool aDeep,
+                                         nsNodeInfoManager *aNewNodeInfoManager,
+                                         nsCOMArray<nsINode> *aNodesWithProperties,
+                                         mozilla::ErrorResult& aError)
   {
     return CloneAndAdopt(aNode, true, aDeep, aNewNodeInfoManager,
-                         nullptr, aNodesWithProperties, nullptr, aResult);
-  }
-
-  /**
-   * Clones aNode, its attributes and, if aDeep is true, its descendant nodes
-   */
-  static nsresult Clone(nsINode *aNode, bool aDeep, nsINode **aResult)
-  {
-    return CloneAndAdopt(aNode, true, aDeep, nullptr, nullptr, nullptr,
-                         aNode->GetParent(), aResult);
+                         nullptr, aNodesWithProperties, nullptr, aError);
   }
 
   /**
    * Walks aNode, its attributes and descendant nodes. If aNewNodeInfoManager is
    * not null, it is used to create new nodeinfos for the nodes. Also reparents
    * the XPConnect wrappers for the nodes into aReparentScope if non-null.
    * aNodesWithProperties will be filled with all the nodes that have
    * properties.
@@ -214,29 +208,30 @@ public:
    * @param aNewNodeInfoManager The nodeinfo manager to use to create new
    *                            nodeinfos for aNode and its attributes and
    *                            descendants. May be null if the nodeinfos
    *                            shouldn't be changed.
    * @param aReparentScope New scope for the wrappers, or null if no reparenting
    *                       should be done.
    * @param aNodesWithProperties All nodes (from amongst aNode and its
    *                             descendants) with properties.
+   * @param aError The error, if any.
    */
-  static nsresult Adopt(nsINode *aNode, nsNodeInfoManager *aNewNodeInfoManager,
-                        JS::Handle<JSObject*> aReparentScope,
-                        nsCOMArray<nsINode> &aNodesWithProperties)
+  static void Adopt(nsINode *aNode, nsNodeInfoManager *aNewNodeInfoManager,
+                    JS::Handle<JSObject*> aReparentScope,
+                    nsCOMArray<nsINode>& aNodesWithProperties,
+                    mozilla::ErrorResult& aError)
   {
-    nsCOMPtr<nsINode> node;
-    nsresult rv = CloneAndAdopt(aNode, false, true, aNewNodeInfoManager,
-                                aReparentScope, &aNodesWithProperties,
-                                nullptr, getter_AddRefs(node));
+    // Just need to store the return value of CloneAndAdopt in a
+    // temporary nsCOMPtr to make sure we release it.
+    nsCOMPtr<nsINode> node = CloneAndAdopt(aNode, false, true, aNewNodeInfoManager,
+                                           aReparentScope, &aNodesWithProperties,
+                                           nullptr, aError);
 
     nsMutationGuard::DidMutate();
-
-    return rv;
   }
 
   /**
    * Helper for the cycle collector to traverse the DOM UserData for aNode.
    *
    * @param aNode the node to traverse UserData for
    * @param aCb the cycle collection callback
    */
@@ -244,19 +239,22 @@ public:
                                nsCycleCollectionTraversalCallback &aCb);
 
   /**
    * A basic implementation of the DOM cloneNode method. Calls nsINode::Clone to
    * do the actual cloning of the node.
    *
    * @param aNode the node to clone
    * @param aDeep if true all descendants will be cloned too
-   * @param aResult the clone
+   * @param aError the error, if any.
+   *
+   * @return the clone, or null if an error occurs.
    */
-  static nsresult CloneNodeImpl(nsINode *aNode, bool aDeep, nsINode **aResult);
+  static already_AddRefed<nsINode> CloneNodeImpl(nsINode *aNode, bool aDeep,
+                                                 mozilla::ErrorResult& aError);
 
   /**
    * Release the UserData for aNode.
    *
    * @param aNode the node to release the UserData for
    */
   static void UnlinkUserData(nsINode *aNode);
 
@@ -282,41 +280,45 @@ private:
    * If aClone is true the nodes will be cloned. If aNewNodeInfoManager is
    * not null, it is used to create new nodeinfos for the nodes. Also reparents
    * the XPConnect wrappers for the nodes into aReparentScope if non-null.
    * aNodesWithProperties will be filled with all the nodes that have
    * properties.
    *
    * @param aNode Node to adopt/clone.
    * @param aClone If true the node will be cloned and the cloned node will
-   *               be in aResult.
+   *               be returned.
    * @param aDeep If true the function will be called recursively on
    *              descendants of the node
    * @param aNewNodeInfoManager The nodeinfo manager to use to create new
    *                            nodeinfos for aNode and its attributes and
    *                            descendants. May be null if the nodeinfos
    *                            shouldn't be changed.
    * @param aReparentScope Scope into which wrappers should be reparented, or
    *                             null if no reparenting should be done.
    * @param aNodesWithProperties All nodes (from amongst aNode and its
    *                             descendants) with properties. If aClone is
    *                             true every node will be followed by its
    *                             clone. Null can be passed to prevent this from
    *                             being populated.
    * @param aParent If aClone is true the cloned node will be appended to
    *                aParent's children. May be null. If not null then aNode
    *                must be an nsIContent.
-   * @param aResult If aClone is true then *aResult will contain the cloned
-   *                node.
+   * @param aError The error, if any.
+   *
+   * @return If aClone is true then the cloned node will be returned,
+   *          unless an error occurred.  In error conditions, null
+   *          will be returned.
    */
-  static nsresult CloneAndAdopt(nsINode *aNode, bool aClone, bool aDeep,
-                                nsNodeInfoManager *aNewNodeInfoManager,
-                                JS::Handle<JSObject*> aReparentScope,
-                                nsCOMArray<nsINode> *aNodesWithProperties,
-                                nsINode *aParent, nsINode **aResult);
+  static already_AddRefed<nsINode>
+    CloneAndAdopt(nsINode *aNode, bool aClone, bool aDeep,
+                  nsNodeInfoManager* aNewNodeInfoManager,
+                  JS::Handle<JSObject*> aReparentScope,
+                  nsCOMArray<nsINode>* aNodesWithProperties,
+                  nsINode *aParent, mozilla::ErrorResult& aError);
 
   enum class AnimationMutationType
   {
     Added,
     Changed,
     Removed
   };
   /**
diff --git a/dom/svg/SVGUseElement.cpp b/dom/svg/SVGUseElement.cpp
--- a/dom/svg/SVGUseElement.cpp
+++ b/dom/svg/SVGUseElement.cpp
@@ -1,15 +1,16 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/ArrayUtils.h"
+#include "mozilla/ErrorResult.h"
 
 #include "mozilla/dom/SVGUseElement.h"
 #include "mozilla/dom/SVGUseElementBinding.h"
 #include "nsGkAtoms.h"
 #include "mozilla/dom/SVGSVGElement.h"
 #include "nsIDocument.h"
 #include "nsIPresShell.h"
 #include "mozilla/dom/Element.h"
@@ -254,23 +255,22 @@ SVGUseElement::CreateAnonymousContent()
          content = content->GetParent()) {
       if (content->IsSVGElement(nsGkAtoms::use) &&
           static_cast<SVGUseElement*>(content.get())->mOriginal == mOriginal) {
         return nullptr;
       }
     }
   }
 
-  nsCOMPtr<nsINode> newnode;
   nsNodeInfoManager* nodeInfoManager =
     targetContent->OwnerDoc() == OwnerDoc() ?
       nullptr : OwnerDoc()->NodeInfoManager();
-  nsNodeUtils::Clone(targetContent, true, nodeInfoManager, nullptr,
-                     getter_AddRefs(newnode));
-
+  IgnoredErrorResult rv;
+  nsCOMPtr<nsINode> newnode =
+    nsNodeUtils::Clone(targetContent, true, nodeInfoManager, nullptr, rv);
   nsCOMPtr<nsIContent> newcontent = do_QueryInterface(newnode);
 
   if (!newcontent)
     return nullptr;
 
   if (newcontent->IsAnyOfSVGElements(nsGkAtoms::svg, nsGkAtoms::symbol)) {
     nsSVGElement *newElement = static_cast<nsSVGElement*>(newcontent.get());
 
diff --git a/dom/xbl/nsXBLBinding.cpp b/dom/xbl/nsXBLBinding.cpp
--- a/dom/xbl/nsXBLBinding.cpp
+++ b/dom/xbl/nsXBLBinding.cpp
@@ -316,19 +316,21 @@ nsXBLBinding::GenerateAnonymousContent()
   // using the attribute-setting shorthand hack.
   uint32_t contentCount = content->GetChildCount();
 
   // Plan to build the content by default.
   bool hasContent = (contentCount > 0);
   if (hasContent) {
     nsIDocument* doc = mBoundElement->OwnerDoc();
 
-    nsCOMPtr<nsINode> clonedNode;
-    nsNodeUtils::Clone(content, true, doc->NodeInfoManager(), nullptr,
-                       getter_AddRefs(clonedNode));
+    IgnoredErrorResult rv;
+    nsCOMPtr<nsINode> clonedNode =
+      nsNodeUtils::Clone(content, true, doc->NodeInfoManager(), nullptr, rv);
+    // FIXME: Bug 1399558, Why is this code OK assuming that nsNodeUtils::Clone
+    // never fails?
     mContent = clonedNode->AsElement();
 
     // Search for <xbl:children> elements in the XBL content. In the presence
     // of multiple default insertion points, we use the last one in document
     // order.
     for (nsIContent* child = mContent; child; child = child->GetNextNode(mContent)) {
       if (child->NodeInfo()->Equals(nsGkAtoms::children, kNameSpaceID_XBL)) {
         XBLChildrenElement* point = static_cast<XBLChildrenElement*>(child);
