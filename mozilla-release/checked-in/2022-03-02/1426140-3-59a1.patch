# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1514984878 0
# Node ID a8f97458d0cc43a62b048a5ebb4a8b1663d18b20
# Parent  9402033e9bee8b818b506aed495fb2567bee07cb
Bug 1426140 - Handle errors for inline module scripts and ensure we update the module map after fetch errors r=baku

diff --git a/dom/script/ScriptLoader.cpp b/dom/script/ScriptLoader.cpp
--- a/dom/script/ScriptLoader.cpp
+++ b/dom/script/ScriptLoader.cpp
@@ -437,25 +437,27 @@ void ScriptLoader::ClearModuleMap() {
 }
 
 nsresult ScriptLoader::ProcessFetchedModuleSource(ModuleLoadRequest* aRequest) {
   MOZ_ASSERT(!aRequest->mModuleScript);
 
   nsresult rv = CreateModuleScript(aRequest);
   MOZ_ASSERT(NS_FAILED(rv) == !aRequest->mModuleScript);
 
-  SetModuleFetchFinishedAndResumeWaitingRequests(aRequest, rv);
-
   aRequest->mScriptText.clearAndFree();
 
   if (NS_FAILED(rv)) {
     aRequest->LoadFailed();
     return rv;
   }
 
+  if (!aRequest->mIsInline) {
+    SetModuleFetchFinishedAndResumeWaitingRequests(aRequest, rv);
+  }
+
   if (!aRequest->mModuleScript->HasParseError()) {
     StartFetchingModuleDependencies(aRequest);
   }
 
   return NS_OK;
 }
 
 static nsresult
@@ -1513,17 +1515,17 @@ ScriptLoader::ProcessScriptElement(nsISc
     return false;
   }
 
   // Does CSP allow this inline script to run?
   if (!CSPAllowsInlineScript(aElement, mDocument)) {
     return false;
   }
 
-  // Inline classic scripts ignore ther CORS mode and are always CORS_NONE.
+  // Inline classic scripts ignore their CORS mode and are always CORS_NONE.
   CORSMode corsMode = CORS_NONE;
   if (scriptKind == ScriptKind::Module) {
     corsMode = aElement->GetCORSMode();
   }
 
   request = CreateLoadRequest(scriptKind, mDocument->GetDocumentURI(), aElement,
                               version, corsMode,
                               SRIMetadata(), // SRI doesn't apply
@@ -1537,30 +1539,30 @@ ScriptLoader::ProcessScriptElement(nsISc
   CollectScriptTelemetry(nullptr, request);
 
   LOG(("ScriptLoadRequest (%p): Created request for inline script",
        request.get()));
 
   if (request->IsModuleRequest()) {
     ModuleLoadRequest* modReq = request->AsModuleRequest();
     modReq->mBaseURL = mDocument->GetDocBaseURI();
-    rv = CreateModuleScript(modReq);
-    MOZ_ASSERT(NS_FAILED(rv) == !modReq->mModuleScript);
-    if (NS_FAILED(rv)) {
-      modReq->LoadFailed();
-      return false;
-    }
+
     if (aElement->GetScriptAsync()) {
-      mLoadingAsyncRequests.AppendElement(request);
+      modReq->mIsAsync = true;
+      mLoadingAsyncRequests.AppendElement(modReq);
     } else {
-      AddDeferRequest(request);
+      AddDeferRequest(modReq);
     }
-    if (!modReq->mModuleScript->HasParseError()) {
-      StartFetchingModuleDependencies(modReq);
+
+    nsresult rv = ProcessFetchedModuleSource(modReq);
+    if (NS_FAILED(rv)) {
+      ReportErrorToConsole(modReq, rv);
+      HandleLoadError(modReq, rv);
     }
+
     return false;
   }
   request->mProgress = ScriptLoadRequest::Progress::Ready;
   if (aElement->GetParserCreated() == FROM_PARSER_XSLT &&
       (!ReadyToExecuteParserBlockingScripts() || !mXSLTRequests.isEmpty())) {
     // Need to maintain order for XSLT-inserted scripts
     NS_ASSERTION(!mParserBlockingRequest,
         "Parser-blocking scripts and XSLT scripts in the same doc!");
@@ -2900,16 +2902,21 @@ ScriptLoader::HandleLoadError(ScriptLoad
    * We make a note of this script node by including it in a dedicated
    * array of blocked tracking nodes under its parent document.
    */
   if (aResult == NS_ERROR_TRACKING_URI) {
     nsCOMPtr<nsIContent> cont = do_QueryInterface(aRequest->mElement);
     mDocument->AddBlockedTrackingNode(cont);
   }
 
+  if (aRequest->IsModuleRequest() && !aRequest->mIsInline) {
+    auto request = aRequest->AsModuleRequest();
+    SetModuleFetchFinishedAndResumeWaitingRequests(request, aResult);
+  }
+
   if (aRequest->mIsDefer) {
     MOZ_ASSERT_IF(aRequest->IsModuleRequest(),
                   aRequest->AsModuleRequest()->IsTopLevel());
     if (aRequest->isInList()) {
       RefPtr<ScriptLoadRequest> req = mDeferRequests.Steal(aRequest);
       FireScriptAvailable(aResult, req);
     }
   } else if (aRequest->mIsAsync) {
diff --git a/testing/web-platform/meta/html/semantics/scripting-1/the-script-element/module/load-error-events-inline.html.ini b/testing/web-platform/meta/html/semantics/scripting-1/the-script-element/module/load-error-events-inline.html.ini
deleted file mode 100644
--- a/testing/web-platform/meta/html/semantics/scripting-1/the-script-element/module/load-error-events-inline.html.ini
+++ /dev/null
@@ -1,18 +0,0 @@
-[load-error-events-inline.html]
-  type: testharness
-  expected: TIMEOUT
-  [src, 200, parser-inserted, defer, no async]
-    expected: NOTRUN
-
-  [src, 200, parser-inserted, no defer, async]
-    expected: NOTRUN
-
-  [src, 404, parser-inserted, defer, no async]
-    expected: NOTRUN
-
-  [src, 404, parser-inserted, no defer, async]
-    expected: NOTRUN
-
-  [src, 404, not parser-inserted, no defer, async]
-    expected: NOTRUN
-
diff --git a/testing/web-platform/tests/html/semantics/scripting-1/the-script-element/module/load-error-events-inline.html b/testing/web-platform/tests/html/semantics/scripting-1/the-script-element/module/load-error-events-inline.html
--- a/testing/web-platform/tests/html/semantics/scripting-1/the-script-element/module/load-error-events-inline.html
+++ b/testing/web-platform/tests/html/semantics/scripting-1/the-script-element/module/load-error-events-inline.html
@@ -54,9 +54,8 @@ script4_dynamic_error.async = true;
 script4_dynamic_error.appendChild(document.createTextNode('import "./not_found.js";'));
 document.head.appendChild(script4_dynamic_error);
 </script>
 
 <script onload="onLoad(test1_load);" onerror="onError(test1_load);" type="module">"use strict";onExecute(test1_load);</script>
 <script onload="onLoad(test4_load);" onerror="onError(test4_load);" type="module" async>"use strict";onExecute(test4_load);</script>
 <script onload="onLoad(test1_error);" onerror="onError(test1_error);" type="module">"use strict";import "./not_found.js";</script>
 <script onload="onLoad(test4_error);" onerror="onError(test4_error);" type="module" async>"use strict";import "./not_found.js";</script>
-</script>
