# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1502383373 14400
#      Thu Aug 10 12:42:53 2017 -0400
# Node ID f631cb50bf5349db728bfa8ca39a82799ae8b592
# Parent  7bca41421dcf831c5e94b736437fa580adf95888
Bug 1388625 part 1.  During a stylo restyle, update the style of the trailing anonymous colgroup of a table, if any.  r=heycam

MozReview-Commit-ID: 4rz4AlPEsjR

diff --git a/layout/tables/nsTableFrame.cpp b/layout/tables/nsTableFrame.cpp
--- a/layout/tables/nsTableFrame.cpp
+++ b/layout/tables/nsTableFrame.cpp
@@ -8027,16 +8027,37 @@ void
 nsTableFrame::AppendDirectlyOwnedAnonBoxes(nsTArray<OwnedAnonBox>& aResult)
 {
   nsIFrame* wrapper = GetParent();
   MOZ_ASSERT(wrapper->StyleContext()->GetPseudo() ==
                nsCSSAnonBoxes::tableWrapper,
              "What happened to our parent?");
   aResult.AppendElement(
     OwnedAnonBox(wrapper, &UpdateStyleOfOwnedAnonBoxesForTableWrapper));
+
+  // We may also have an anonymous colgroup that we're responsible for.
+  // Specifically, we can have three types of colgroup frames: (A) corresponding
+  // to actual elements with "display: table-column-group", (B) wrapping runs of
+  // "display: table-column" kids, and (C) one colgroup frame added at the end
+  // to hold the anonymous colframes we need so each cell has an associated
+  // colframe.
+  //
+  // These types of colgroups are supposed to correspond to the values of the
+  // nsTableColGroupType enum: type (A) to eColGroupContent, type (B) to
+  // eColGroupAnonymousCol, and type (C) to eColGroupAnonymousCell.  But we
+  // never actually set eColGroupAnonymousCol on any colgroups right now; see
+  // bug 1387568.  In any case, eColGroupAnonymousCell works correctly to detect
+  // colgroups of type (C), which are the ones we want to restyle here.  Type
+  // (A) will be restyled via their element, and type (B) via the machinery for
+  // restyling wrapper anonymous frames.
+  auto colGroupFrame =
+    static_cast<nsTableColGroupFrame*>(mColGroups.LastChild());
+  if (colGroupFrame && colGroupFrame->GetColType() == eColGroupAnonymousCell) {
+    aResult.AppendElement(colGroupFrame);
+  }
 }
 
 /* static */ void
 nsTableFrame::UpdateStyleOfOwnedAnonBoxesForTableWrapper(
   nsIFrame* aOwningFrame,
   nsIFrame* aWrapperFrame,
   ServoRestyleState& aRestyleState)
 {
