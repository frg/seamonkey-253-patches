# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1504519299 18000
#      Mon Sep 04 05:01:39 2017 -0500
# Node ID 0360b6884f35868a4e413cfc51020f72e2c8c414
# Parent  5cdaee207483e27e4a4bec30cd8d7700cb506a25
servo: Merge #18359 - style: Move color to use predefined_type (from emilio:bye-color-mako); r=nox

I want to play with autogenerating style structs, and color is the smallest
struct out there.

However, moving it out of the mako file is a requirement. This patch does that.

Source-Repo: https://github.com/servo/servo
Source-Revision: bcddb19eb8642664386ed83d7734f16db517d0cf

diff --git a/servo/components/style/properties/longhand/color.mako.rs b/servo/components/style/properties/longhand/color.mako.rs
--- a/servo/components/style/properties/longhand/color.mako.rs
+++ b/servo/components/style/properties/longhand/color.mako.rs
@@ -3,139 +3,112 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 <%namespace name="helpers" file="/helpers.mako.rs" />
 
 <% data.new_style_struct("Color", inherited=True) %>
 
 <% from data import to_rust_ident %>
 
-<%helpers:longhand name="color" need_clone="True"
-                   animation_value_type="AnimatedRGBA"
-                   ignored_when_colors_disabled="True"
-                   flags="APPLIES_TO_FIRST_LETTER APPLIES_TO_FIRST_LINE APPLIES_TO_PLACEHOLDER"
-                   spec="https://drafts.csswg.org/css-color/#color">
-    use cssparser::RGBA;
-    use values::specified::{AllowQuirks, Color};
+${helpers.predefined_type(
+    "color",
+    "ColorPropertyValue",
+    "::cssparser::RGBA::new(0, 0, 0, 255)",
+    animation_value_type="AnimatedRGBA",
+    flags="APPLIES_TO_FIRST_LETTER APPLIES_TO_FIRST_LINE APPLIES_TO_PLACEHOLDER",
+    ignored_when_colors_disabled="True",
+    spec="https://drafts.csswg.org/css-color/#color",
+    need_clone="True"
+)}
 
-    impl ToComputedValue for SpecifiedValue {
-        type ComputedValue = computed_value::T;
+// FIXME(#15973): Add servo support for system colors
+//
+// FIXME(emilio): Move outside of mako.
+% if product == "gecko":
+pub mod system_colors {
+    <%
+        # These are actually parsed. See nsCSSProps::kColorKTable
+        system_colors = """activeborder activecaption appworkspace background buttonface
+                           buttonhighlight buttonshadow buttontext captiontext graytext highlight
+                           highlighttext inactiveborder inactivecaption inactivecaptiontext
+                           infobackground infotext menu menutext scrollbar threeddarkshadow
+                           threedface threedhighlight threedlightshadow threedshadow window
+                           windowframe windowtext -moz-buttondefault -moz-buttonhoverface
+                           -moz-buttonhovertext -moz-cellhighlight -moz-cellhighlighttext
+                           -moz-eventreerow -moz-field -moz-fieldtext -moz-dialog -moz-dialogtext
+                           -moz-dragtargetzone -moz-gtk-info-bar-text -moz-html-cellhighlight
+                           -moz-html-cellhighlighttext -moz-mac-buttonactivetext
+                           -moz-mac-chrome-active -moz-mac-chrome-inactive
+                           -moz-mac-defaultbuttontext -moz-mac-focusring -moz-mac-menuselect
+                           -moz-mac-menushadow -moz-mac-menutextdisable -moz-mac-menutextselect
+                           -moz-mac-disabledtoolbartext -moz-mac-secondaryhighlight
+                           -moz-menuhover -moz-menuhovertext -moz-menubartext -moz-menubarhovertext
+                           -moz-oddtreerow -moz-win-mediatext -moz-win-communicationstext
+                           -moz-win-accentcolor -moz-win-accentcolortext
+                           -moz-nativehyperlinktext -moz-comboboxtext -moz-combobox""".split()
 
-        #[inline]
-        fn to_computed_value(&self, context: &Context) -> computed_value::T {
-            self.0.to_computed_value(context)
-                .to_rgba(context.builder.get_parent_color().clone_color())
-        }
+        # These are not parsed but must be serialized
+        # They are only ever set directly by Gecko
+        extra_colors = """WindowBackground WindowForeground WidgetBackground WidgetForeground
+                          WidgetSelectBackground WidgetSelectForeground Widget3DHighlight Widget3DShadow
+                          TextBackground TextForeground TextSelectBackground TextSelectForeground
+                          TextSelectForegroundCustom TextSelectBackgroundDisabled TextSelectBackgroundAttention
+                          TextHighlightBackground TextHighlightForeground IMERawInputBackground
+                          IMERawInputForeground IMERawInputUnderline IMESelectedRawTextBackground
+                          IMESelectedRawTextForeground IMESelectedRawTextUnderline
+                          IMEConvertedTextBackground IMEConvertedTextForeground IMEConvertedTextUnderline
+                          IMESelectedConvertedTextBackground IMESelectedConvertedTextForeground
+                          IMESelectedConvertedTextUnderline SpellCheckerUnderline""".split()
+    %>
+    use cssparser::Parser;
+    use gecko_bindings::bindings::Gecko_GetLookAndFeelSystemColor;
+    use gecko_bindings::structs::root::mozilla::LookAndFeel_ColorID;
+    use std::fmt;
+    use style_traits::ToCss;
+    use values::computed::{Context, ToComputedValue};
 
-        #[inline]
-        fn from_computed_value(computed: &computed_value::T) -> Self {
-            SpecifiedValue(Color::rgba(*computed).into())
+    pub type SystemColor = LookAndFeel_ColorID;
+
+    impl ToCss for SystemColor {
+        fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
+            let s = match *self {
+                % for color in system_colors + extra_colors:
+                    LookAndFeel_ColorID::eColorID_${to_rust_ident(color)} => "${color}",
+                % endfor
+                LookAndFeel_ColorID::eColorID_LAST_COLOR => unreachable!(),
+            };
+            dest.write_str(s)
         }
     }
 
-    #[cfg_attr(feature = "servo", derive(HeapSizeOf))]
-    #[derive(Clone, Debug, PartialEq, ToCss)]
-    pub struct SpecifiedValue(pub Color);
-
-    pub mod computed_value {
-        use cssparser;
-        pub type T = cssparser::RGBA;
-    }
-    #[inline]
-    pub fn get_initial_value() -> computed_value::T {
-        RGBA::new(0, 0, 0, 255) // black
-    }
-    pub fn parse<'i, 't>(context: &ParserContext, input: &mut Parser<'i, 't>)
-                         -> Result<SpecifiedValue, ParseError<'i>> {
-        Color::parse_quirky(context, input, AllowQuirks::Yes).map(SpecifiedValue)
-    }
+    impl ToComputedValue for SystemColor {
+        type ComputedValue = u32; // nscolor
 
-    // FIXME(#15973): Add servo support for system colors
-    % if product == "gecko":
-        <%
-            # These are actually parsed. See nsCSSProps::kColorKTable
-            system_colors = """activeborder activecaption appworkspace background buttonface
-                               buttonhighlight buttonshadow buttontext captiontext graytext highlight
-                               highlighttext inactiveborder inactivecaption inactivecaptiontext
-                               infobackground infotext menu menutext scrollbar threeddarkshadow
-                               threedface threedhighlight threedlightshadow threedshadow window
-                               windowframe windowtext -moz-buttondefault -moz-buttonhoverface
-                               -moz-buttonhovertext -moz-cellhighlight -moz-cellhighlighttext
-                               -moz-eventreerow -moz-field -moz-fieldtext -moz-dialog -moz-dialogtext
-                               -moz-dragtargetzone -moz-gtk-info-bar-text -moz-html-cellhighlight
-                               -moz-html-cellhighlighttext -moz-mac-buttonactivetext
-                               -moz-mac-chrome-active -moz-mac-chrome-inactive
-                               -moz-mac-defaultbuttontext -moz-mac-focusring -moz-mac-menuselect
-                               -moz-mac-menushadow -moz-mac-menutextdisable -moz-mac-menutextselect
-                               -moz-mac-disabledtoolbartext -moz-mac-secondaryhighlight
-                               -moz-menuhover -moz-menuhovertext -moz-menubartext -moz-menubarhovertext
-                               -moz-oddtreerow -moz-win-mediatext -moz-win-communicationstext
-                               -moz-win-accentcolor -moz-win-accentcolortext
-                               -moz-nativehyperlinktext -moz-comboboxtext -moz-combobox""".split()
-
-            # These are not parsed but must be serialized
-            # They are only ever set directly by Gecko
-            extra_colors = """WindowBackground WindowForeground WidgetBackground WidgetForeground
-                              WidgetSelectBackground WidgetSelectForeground Widget3DHighlight Widget3DShadow
-                              TextBackground TextForeground TextSelectBackground TextSelectForeground
-                              TextSelectForegroundCustom TextSelectBackgroundDisabled TextSelectBackgroundAttention
-                              TextHighlightBackground TextHighlightForeground IMERawInputBackground
-                              IMERawInputForeground IMERawInputUnderline IMESelectedRawTextBackground
-                              IMESelectedRawTextForeground IMESelectedRawTextUnderline
-                              IMEConvertedTextBackground IMEConvertedTextForeground IMEConvertedTextUnderline
-                              IMESelectedConvertedTextBackground IMESelectedConvertedTextForeground
-                              IMESelectedConvertedTextUnderline SpellCheckerUnderline""".split()
-        %>
-        use gecko_bindings::bindings::Gecko_GetLookAndFeelSystemColor;
-        use gecko_bindings::structs::root::mozilla::LookAndFeel_ColorID;
-        use std::fmt;
-        use style_traits::ToCss;
-
-        pub type SystemColor = LookAndFeel_ColorID;
-
-        impl ToCss for SystemColor {
-            fn to_css<W>(&self, dest: &mut W) -> fmt::Result where W: fmt::Write {
-                let s = match *self {
-                    % for color in system_colors + extra_colors:
-                        LookAndFeel_ColorID::eColorID_${to_rust_ident(color)} => "${color}",
-                    % endfor
-                    LookAndFeel_ColorID::eColorID_LAST_COLOR => unreachable!(),
-                };
-                dest.write_str(s)
+        #[inline]
+        fn to_computed_value(&self, cx: &Context) -> Self::ComputedValue {
+            unsafe {
+                Gecko_GetLookAndFeelSystemColor(*self as i32,
+                                                cx.device().pres_context())
             }
         }
 
-        impl ToComputedValue for SystemColor {
-            type ComputedValue = u32; // nscolor
-            #[inline]
-            fn to_computed_value(&self, cx: &Context) -> Self::ComputedValue {
-                unsafe {
-                    Gecko_GetLookAndFeelSystemColor(*self as i32,
-                                                    cx.device().pres_context())
+        #[inline]
+        fn from_computed_value(_: &Self::ComputedValue) -> Self {
+            unreachable!()
+        }
+    }
+
+    impl SystemColor {
+        pub fn parse<'i, 't>(input: &mut Parser<'i, 't>,) -> Result<Self, ()> {
+            ascii_case_insensitive_phf_map! {
+                color_name -> SystemColor = {
+                    % for color in system_colors:
+                        "${color}" => LookAndFeel_ColorID::eColorID_${to_rust_ident(color)},
+                    % endfor
                 }
             }
 
-            #[inline]
-            fn from_computed_value(_: &Self::ComputedValue) -> Self {
-                unreachable!()
-            }
+            let ident = input.expect_ident().map_err(|_| ())?;
+            color_name(ident).cloned().ok_or(())
         }
-
-        impl SystemColor {
-            pub fn parse<'i, 't>(input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
-                ascii_case_insensitive_phf_map! {
-                    color_name -> SystemColor = {
-                        % for color in system_colors:
-                            "${color}" => LookAndFeel_ColorID::eColorID_${to_rust_ident(color)},
-                        % endfor
-                    }
-                }
-
-                let ident = input.expect_ident()?;
-                if let Some(color) = color_name(&ident) {
-                    Ok(*color)
-                } else {
-                    Err(SelectorParseError::UnexpectedIdent(ident.clone()).into())
-                }
-            }
-        }
-    % endif
-</%helpers:longhand>
+    }
+}
+% endif
diff --git a/servo/components/style/values/computed/color.rs b/servo/components/style/values/computed/color.rs
--- a/servo/components/style/values/computed/color.rs
+++ b/servo/components/style/values/computed/color.rs
@@ -21,16 +21,19 @@ pub struct Color {
     pub color: RGBA,
     /// The ratio of currentcolor in complex color.
     pub foreground_ratio: u8,
 }
 
 /// Computed value type for the specified RGBAColor.
 pub type RGBAColor = RGBA;
 
+/// The computed value of the `color` property.
+pub type ColorPropertyValue = RGBA;
+
 impl Color {
     /// Returns a numeric color representing the given RGBA value.
     pub fn rgba(rgba: RGBA) -> Color {
         Color {
             color: rgba,
             foreground_ratio: 0,
         }
     }
diff --git a/servo/components/style/values/computed/mod.rs b/servo/components/style/values/computed/mod.rs
--- a/servo/components/style/values/computed/mod.rs
+++ b/servo/components/style/values/computed/mod.rs
@@ -30,17 +30,17 @@ pub use app_units::Au;
 pub use properties::animated_properties::TransitionProperty;
 #[cfg(feature = "gecko")]
 pub use self::align::{AlignItems, AlignJustifyContent, AlignJustifySelf, JustifyItems};
 pub use self::angle::Angle;
 pub use self::background::BackgroundSize;
 pub use self::border::{BorderImageSlice, BorderImageWidth, BorderImageSideWidth};
 pub use self::border::{BorderRadius, BorderCornerRadius};
 pub use self::box_::VerticalAlign;
-pub use self::color::{Color, RGBAColor};
+pub use self::color::{Color, ColorPropertyValue, RGBAColor};
 pub use self::effects::{BoxShadow, Filter, SimpleShadow};
 pub use self::flex::FlexBasis;
 pub use self::image::{Gradient, GradientItem, Image, ImageLayer, LineDirection, MozImageRect};
 #[cfg(feature = "gecko")]
 pub use self::gecko::ScrollSnapPoint;
 pub use self::rect::LengthOrNumberRect;
 pub use super::{Auto, Either, None_};
 pub use super::specified::BorderStyle;
diff --git a/servo/components/style/values/specified/color.rs b/servo/components/style/values/specified/color.rs
--- a/servo/components/style/values/specified/color.rs
+++ b/servo/components/style/values/specified/color.rs
@@ -5,17 +5,17 @@
 //! Specified color values.
 
 use cssparser::{Color as CSSParserColor, Parser, RGBA, Token, BasicParseError};
 #[cfg(feature = "gecko")]
 use gecko_bindings::structs::nscolor;
 use itoa;
 use parser::{ParserContext, Parse};
 #[cfg(feature = "gecko")]
-use properties::longhands::color::SystemColor;
+use properties::longhands::system_colors::SystemColor;
 use std::fmt;
 use std::io::Write;
 use style_traits::{ToCss, ParseError, StyleParseError, ValueParseError};
 use super::AllowQuirks;
 use values::computed::{Color as ComputedColor, Context, ToComputedValue};
 
 /// Specified color value
 #[derive(Clone, Debug, PartialEq)]
@@ -324,8 +324,37 @@ impl ToComputedValue for RGBAColor {
     }
 }
 
 impl From<Color> for RGBAColor {
     fn from(color: Color) -> RGBAColor {
         RGBAColor(color)
     }
 }
+
+/// Specified value for the "color" property, which resolves the `currentcolor`
+/// keyword to the parent color instead of self's color.
+#[derive(Clone, Debug, PartialEq, ToCss)]
+pub struct ColorPropertyValue(pub Color);
+
+impl ToComputedValue for ColorPropertyValue {
+    type ComputedValue = RGBA;
+
+    #[inline]
+    fn to_computed_value(&self, context: &Context) -> RGBA {
+        self.0.to_computed_value(context)
+            .to_rgba(context.builder.get_parent_color().clone_color())
+    }
+
+    #[inline]
+    fn from_computed_value(computed: &RGBA) -> Self {
+        ColorPropertyValue(Color::rgba(*computed).into())
+    }
+}
+
+impl Parse for ColorPropertyValue {
+    fn parse<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<Self, ParseError<'i>> {
+        Color::parse_quirky(context, input, AllowQuirks::Yes).map(ColorPropertyValue)
+    }
+}
diff --git a/servo/components/style/values/specified/mod.rs b/servo/components/style/values/specified/mod.rs
--- a/servo/components/style/values/specified/mod.rs
+++ b/servo/components/style/values/specified/mod.rs
@@ -27,17 +27,17 @@ use values::specified::calc::CalcNode;
 pub use properties::animated_properties::TransitionProperty;
 pub use self::angle::Angle;
 #[cfg(feature = "gecko")]
 pub use self::align::{AlignItems, AlignJustifyContent, AlignJustifySelf, JustifyItems};
 pub use self::background::BackgroundSize;
 pub use self::border::{BorderCornerRadius, BorderImageSlice, BorderImageWidth};
 pub use self::border::{BorderImageSideWidth, BorderRadius, BorderSideWidth};
 pub use self::box_::VerticalAlign;
-pub use self::color::{Color, RGBAColor};
+pub use self::color::{Color, ColorPropertyValue, RGBAColor};
 pub use self::effects::{BoxShadow, Filter, SimpleShadow};
 pub use self::flex::FlexBasis;
 #[cfg(feature = "gecko")]
 pub use self::gecko::ScrollSnapPoint;
 pub use self::image::{ColorStop, EndingShape as GradientEndingShape, Gradient};
 pub use self::image::{GradientItem, GradientKind, Image, ImageLayer, MozImageRect};
 pub use self::length::{AbsoluteLength, CalcLengthOrPercentage, CharacterWidth};
 pub use self::length::{FontRelativeLength, Length, LengthOrNone, LengthOrNumber};
