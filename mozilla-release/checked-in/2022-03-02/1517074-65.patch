# HG changeset patch
# User Martin Stransky <stransky@redhat.com>
# Date 1547205687 0
# Node ID 25ba8700fe9477fc7091067e79a2b281aeb0aa29
# Parent  3ab925951e61d042d1a023e33f157151d69b704f
Bug 1517074 - [Linux/Gtk] Enable native (xdg-desktop based) file dialog on KDE only due to Gtk+ regression. r=jhorak, a=RyanVM

Also add widget.allow-gtk-native-file-chooser preference value to force enable on all systems.

Differential Revision: https://phabricator.services.mozilla.com/D16184

diff --git a/widget/gtk/nsFilePicker.cpp b/widget/gtk/nsFilePicker.cpp
--- a/widget/gtk/nsFilePicker.cpp
+++ b/widget/gtk/nsFilePicker.cpp
@@ -170,16 +170,18 @@ NS_IMPL_ISUPPORTS(nsFilePicker, nsIFileP
 nsFilePicker::nsFilePicker()
   : mSelectedType(0)
   , mRunning(false)
   , mAllowURLs(false)
 #ifdef MOZ_WIDGET_GTK
   , mFileChooserDelegate(nullptr)
 #endif
 {
+  nsCOMPtr<nsIGIOService> giovfs = do_GetService(NS_GIOSERVICE_CONTRACTID);
+  giovfs->ShouldUseFlatpakPortal(&mUseNativeFileChooser);
 }
 
 nsFilePicker::~nsFilePicker()
 {
 }
 
 void
 ReadMultipleFiles(gpointer filename, gpointer array)
@@ -597,29 +599,28 @@ nsFilePicker::Done(void* file_chooser, g
     mCallback = nullptr;
   } else {
     mResult = result;
   }
   NS_RELEASE_THIS();
 }
 
 // All below functions available as of GTK 3.20+
-
 void *
 nsFilePicker::GtkFileChooserNew(
         const gchar *title, GtkWindow *parent,
         GtkFileChooserAction action,
         const gchar *accept_label)
 {
   static auto sGtkFileChooserNativeNewPtr = (void * (*)(
         const gchar *, GtkWindow *,
         GtkFileChooserAction,
         const gchar *, const gchar *))
       dlsym(RTLD_DEFAULT, "gtk_file_chooser_native_new");
-  if (sGtkFileChooserNativeNewPtr != nullptr) {
+  if (mUseNativeFileChooser && sGtkFileChooserNativeNewPtr != nullptr) {
     return (*sGtkFileChooserNativeNewPtr)(title, parent, action, accept_label, nullptr);
   }
   if (accept_label == nullptr) {
     accept_label = (action == GTK_FILE_CHOOSER_ACTION_SAVE) 
         ? GTK_STOCK_SAVE : GTK_STOCK_OPEN;
   }
   GtkWidget *file_chooser = gtk_file_chooser_dialog_new(title, parent, action,
       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
@@ -629,43 +630,43 @@ nsFilePicker::GtkFileChooserNew(
   return file_chooser;
 }
 
 void
 nsFilePicker::GtkFileChooserShow(void *file_chooser)
 {
   static auto sGtkNativeDialogShowPtr = (void (*)(void *))
       dlsym(RTLD_DEFAULT, "gtk_native_dialog_show");
-  if (sGtkNativeDialogShowPtr != nullptr) {
+  if (mUseNativeFileChooser && sGtkNativeDialogShowPtr != nullptr) {
     (*sGtkNativeDialogShowPtr)(file_chooser);
   } else {
     g_signal_connect(file_chooser, "destroy", G_CALLBACK(OnDestroy), this);
     gtk_widget_show(GTK_WIDGET(file_chooser));
   }
 }
 
 void
 nsFilePicker::GtkFileChooserDestroy(void *file_chooser)
 {
   static auto sGtkNativeDialogDestroyPtr = (void (*)(void *))
     dlsym(RTLD_DEFAULT, "gtk_native_dialog_destroy");
-  if (sGtkNativeDialogDestroyPtr != nullptr) {
+  if (mUseNativeFileChooser && sGtkNativeDialogDestroyPtr != nullptr) {
     (*sGtkNativeDialogDestroyPtr)(file_chooser);
   } else {
     gtk_widget_destroy(GTK_WIDGET(file_chooser));
   }
 }
 
 void
 nsFilePicker::GtkFileChooserSetModal(void *file_chooser,
         GtkWindow *parent_widget, gboolean modal)
 {
   static auto sGtkNativeDialogSetModalPtr = (void (*)(void *, gboolean))
     dlsym(RTLD_DEFAULT, "gtk_native_dialog_set_modal");
-  if (sGtkNativeDialogSetModalPtr != nullptr) {
+  if (mUseNativeFileChooser && sGtkNativeDialogSetModalPtr != nullptr) {
     (*sGtkNativeDialogSetModalPtr)(file_chooser, modal);
   } else {
     GtkWindow *window = GTK_WINDOW(file_chooser);
     gtk_window_set_modal(window, modal);
     if (parent_widget != nullptr) {
       gtk_window_set_destroy_with_parent(window, modal);
     }
   }
diff --git a/widget/gtk/nsFilePicker.h b/widget/gtk/nsFilePicker.h
--- a/widget/gtk/nsFilePicker.h
+++ b/widget/gtk/nsFilePicker.h
@@ -82,11 +82,12 @@ private:
   void GtkFileChooserShow(void *file_chooser);
   void GtkFileChooserDestroy(void *file_chooser);
   void GtkFileChooserSetModal(void *file_chooser, GtkWindow* parent_widget,
           gboolean modal);
 
 #ifdef MOZ_WIDGET_GTK
   GtkFileChooserWidget *mFileChooserDelegate;
 #endif
+  bool mUseNativeFileChooser;
 };
 
 #endif
