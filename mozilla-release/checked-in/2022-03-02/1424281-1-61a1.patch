# HG changeset patch
# User David Major <dmajor@mozilla.com>
# Date 1520349334 18000
# Node ID 1187ae9020bef4e101696883feb3b41ffb8724db
# Parent  ccf6855e36e2963a5f9c886d141947fd5acb1a75
Bug 1424281 - De-optimize some functions to work around crashes during compilation. r=froydnj

diff --git a/layout/generic/nsContainerFrame.cpp b/layout/generic/nsContainerFrame.cpp
--- a/layout/generic/nsContainerFrame.cpp
+++ b/layout/generic/nsContainerFrame.cpp
@@ -1041,16 +1041,24 @@ nsContainerFrame::PositionChildViews(nsI
  *
  * Flags:
  * NS_FRAME_NO_MOVE_FRAME - don't move the frame. aX and aY are ignored in this
  *    case. Also implies NS_FRAME_NO_MOVE_VIEW
  * NS_FRAME_NO_MOVE_VIEW - don't position the frame's view. Set this if you
  *    don't want to automatically sync the frame and view
  * NS_FRAME_NO_SIZE_VIEW - don't size the frame's view
  */
+
+/**
+ * De-optimize function to work around a VC2017 15.5+ compiler bug:
+ * https://bugzil.la/1424281#c12
+ */
+#if defined(_MSC_VER) && !defined(__clang__) && defined(_M_AMD64)
+#pragma optimize("g", off)
+#endif
 void
 nsContainerFrame::FinishReflowChild(nsIFrame*                  aKidFrame,
                                     nsPresContext*             aPresContext,
                                     const ReflowOutput& aDesiredSize,
                                     const ReflowInput*   aReflowInput,
                                     const WritingMode&         aWM,
                                     const LogicalPoint&        aPos,
                                     const nsSize&              aContainerSize,
@@ -1087,16 +1095,19 @@ nsContainerFrame::FinishReflowChild(nsIF
       // If the frame has moved, then we need to make sure any child views are
       // correctly positioned
       PositionChildViews(aKidFrame);
     }
   }
 
   aKidFrame->DidReflow(aPresContext, aReflowInput, nsDidReflowStatus::FINISHED);
 }
+#if defined(_MSC_VER) && !defined(__clang__) && defined(_M_AMD64)
+#pragma optimize("", on)
+#endif
 
 //XXX temporary: hold on to a copy of the old physical version of
 //    FinishReflowChild so that we can convert callers incrementally.
 void
 nsContainerFrame::FinishReflowChild(nsIFrame*                  aKidFrame,
                                     nsPresContext*             aPresContext,
                                     const ReflowOutput& aDesiredSize,
                                     const ReflowInput*   aReflowInput,
diff --git a/third_party/aom/av1/encoder/x86/hybrid_fwd_txfm_avx2.c b/third_party/aom/av1/encoder/x86/hybrid_fwd_txfm_avx2.c
--- a/third_party/aom/av1/encoder/x86/hybrid_fwd_txfm_avx2.c
+++ b/third_party/aom/av1/encoder/x86/hybrid_fwd_txfm_avx2.c
@@ -166,16 +166,21 @@ static void right_shift_16x16(__m256i *i
   in[10] = _mm256_srai_epi16(in[10], 2);
   in[11] = _mm256_srai_epi16(in[11], 2);
   in[12] = _mm256_srai_epi16(in[12], 2);
   in[13] = _mm256_srai_epi16(in[13], 2);
   in[14] = _mm256_srai_epi16(in[14], 2);
   in[15] = _mm256_srai_epi16(in[15], 2);
 }
 
+// Work around bugs in Visual Studio 2017 15.6 profile-guided optimization.
+#if defined(_MSC_VER) && !defined(__clang__) && defined(_M_IX86)
+#pragma optimize("g", off)
+#endif
+
 static void fdct16_avx2(__m256i *in) {
   // sequence: cospi_L_H = pairs(L, H) and L first
   const __m256i cospi_p16_m16 = pair256_set_epi16(cospi_16_64, -cospi_16_64);
   const __m256i cospi_p16_p16 = pair256_set_epi16(cospi_16_64, cospi_16_64);
   const __m256i cospi_p24_p08 = pair256_set_epi16(cospi_24_64, cospi_8_64);
   const __m256i cospi_m08_p24 = pair256_set_epi16(-cospi_8_64, cospi_24_64);
   const __m256i cospi_m24_m08 = pair256_set_epi16(-cospi_24_64, -cospi_8_64);
 
@@ -355,16 +360,21 @@ static void fdct16_avx2(__m256i *in) {
   in[11] = butter_fly(&x0, &x1, &cospi_m10_p22);
 
   x0 = _mm256_unpacklo_epi16(u3, u4);
   x1 = _mm256_unpackhi_epi16(u3, u4);
   in[13] = butter_fly(&x0, &x1, &cospi_p06_p26);
   in[3] = butter_fly(&x0, &x1, &cospi_m26_p06);
 }
 
+// Work around bugs in Visual Studio 2017 15.6 profile-guided optimization.
+#if defined(_MSC_VER) && !defined(__clang__) && defined(_M_IX86)
+#pragma optimize("", on)
+#endif
+
 void fadst16_avx2(__m256i *in) {
   const __m256i cospi_p01_p31 = pair256_set_epi16(cospi_1_64, cospi_31_64);
   const __m256i cospi_p31_m01 = pair256_set_epi16(cospi_31_64, -cospi_1_64);
   const __m256i cospi_p05_p27 = pair256_set_epi16(cospi_5_64, cospi_27_64);
   const __m256i cospi_p27_m05 = pair256_set_epi16(cospi_27_64, -cospi_5_64);
   const __m256i cospi_p09_p23 = pair256_set_epi16(cospi_9_64, cospi_23_64);
   const __m256i cospi_p23_m09 = pair256_set_epi16(cospi_23_64, -cospi_9_64);
   const __m256i cospi_p13_p19 = pair256_set_epi16(cospi_13_64, cospi_19_64);
