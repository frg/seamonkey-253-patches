# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1505381292 -32400
# Node ID 849f008bce82bd547dd4e092efcce43c3f89f61c
# Parent  dbf8e2e9cfdcea77d8ede87266bd09bbfc7059d5
Bug 1400063 - Automatically declare jemalloc_* functions in mozmemory.h. r=njn

There is a lot of churn involved in adding new API surface to
mozjemalloc, and mozmemory.h is one. Instead of declaring
everything manually in there, "generate" the declarations through
malloc_decls.h.

diff --git a/memory/build/malloc_decls.h b/memory/build/malloc_decls.h
--- a/memory/build/malloc_decls.h
+++ b/memory/build/malloc_decls.h
@@ -48,19 +48,60 @@ MALLOC_DECL(realloc, void *, void *, siz
 MALLOC_DECL_VOID(free, void *)
 MALLOC_DECL(memalign, void *, size_t, size_t)
 MALLOC_DECL(valloc, void *, size_t)
 MALLOC_DECL(malloc_usable_size, size_t, usable_ptr_t)
 MALLOC_DECL(malloc_good_size, size_t, size_t)
 #  endif
 #  if MALLOC_FUNCS & MALLOC_FUNCS_JEMALLOC
 MALLOC_DECL_VOID(jemalloc_stats, jemalloc_stats_t *)
+/*
+ * On some operating systems (Mac), we use madvise(MADV_FREE) to hand pages
+ * back to the operating system.  On Mac, the operating system doesn't take
+ * this memory back immediately; instead, the OS takes it back only when the
+ * machine is running out of physical memory.
+ *
+ * This is great from the standpoint of efficiency, but it makes measuring our
+ * actual RSS difficult, because pages which we've MADV_FREE'd shouldn't count
+ * against our RSS.
+ *
+ * This function explicitly purges any MADV_FREE'd pages from physical memory,
+ * causing our reported RSS match the amount of memory we're actually using.
+ *
+ * Note that this call is expensive in two ways.  First, it may be slow to
+ * execute, because it may make a number of slow syscalls to free memory.  This
+ * function holds the big jemalloc locks, so basically all threads are blocked
+ * while this function runs.
+ *
+ * This function is also expensive in that the next time we go to access a page
+ * which we've just explicitly decommitted, the operating system has to attach
+ * to it a physical page!  If we hadn't run this function, the OS would have
+ * less work to do.
+ *
+ * If MALLOC_DOUBLE_PURGE is not defined, this function does nothing.
+ */
 MALLOC_DECL_VOID(jemalloc_purge_freed_pages)
+
+/*
+ * Free all unused dirty pages in all arenas. Calling this function will slow
+ * down subsequent allocations so it is recommended to use it only when
+ * memory needs to be reclaimed at all costs (see bug 805855). This function
+ * provides functionality similar to mallctl("arenas.purge") in jemalloc 3.
+ */
 MALLOC_DECL_VOID(jemalloc_free_dirty_pages)
+
+/*
+ * Opt in or out of a thread local arena (bool argument is whether to opt-in
+ * (true) or out (false)).
+ */
 MALLOC_DECL_VOID(jemalloc_thread_local_arena, bool)
+
+/*
+ * Provide information about any allocation enclosing the given address.
+ */
 MALLOC_DECL_VOID(jemalloc_ptr_info, const void*, jemalloc_ptr_info_t*)
 #  endif
 
 #  undef MALLOC_DECL_VOID
 #endif /* MALLOC_DECL */
 
 #undef MALLOC_DECL
 #undef MALLOC_FUNCS
diff --git a/memory/build/moz.build b/memory/build/moz.build
--- a/memory/build/moz.build
+++ b/memory/build/moz.build
@@ -1,26 +1,26 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 EXPORTS += [
+    'malloc_decls.h',
     'mozjemalloc_types.h',
     'mozmemory.h',
     'mozmemory_wrap.h',
 ]
 
 LIBRARY_DEFINES['MOZ_HAS_MOZGLUE'] = True
 DEFINES['MOZ_MEMORY_IMPL'] = True
 
 if CONFIG['MOZ_REPLACE_MALLOC']:
     EXPORTS += [
-        'malloc_decls.h',
         'replace_malloc.h',
         'replace_malloc_bridge.h',
     ]
 
 SOURCES += [
     'mozjemalloc.cpp',
     'mozmemory_wrap.c',
 ]
diff --git a/memory/build/mozmemory.h b/memory/build/mozmemory.h
--- a/memory/build/mozmemory.h
+++ b/memory/build/mozmemory.h
@@ -44,54 +44,14 @@ static inline size_t _malloc_good_size(s
     return size;
 #  endif
   return malloc_good_size_impl(size);
 }
 
 #  define malloc_good_size _malloc_good_size
 #endif
 
-MOZ_JEMALLOC_API void jemalloc_stats(jemalloc_stats_t *stats);
-
-/*
- * On some operating systems (Mac), we use madvise(MADV_FREE) to hand pages
- * back to the operating system.  On Mac, the operating system doesn't take
- * this memory back immediately; instead, the OS takes it back only when the
- * machine is running out of physical memory.
- *
- * This is great from the standpoint of efficiency, but it makes measuring our
- * actual RSS difficult, because pages which we've MADV_FREE'd shouldn't count
- * against our RSS.
- *
- * This function explicitly purges any MADV_FREE'd pages from physical memory,
- * causing our reported RSS match the amount of memory we're actually using.
- *
- * Note that this call is expensive in two ways.  First, it may be slow to
- * execute, because it may make a number of slow syscalls to free memory.  This
- * function holds the big jemalloc locks, so basically all threads are blocked
- * while this function runs.
- *
- * This function is also expensive in that the next time we go to access a page
- * which we've just explicitly decommitted, the operating system has to attach
- * to it a physical page!  If we hadn't run this function, the OS would have
- * less work to do.
- *
- * If MALLOC_DOUBLE_PURGE is not defined, this function does nothing.
- */
-MOZ_JEMALLOC_API void jemalloc_purge_freed_pages();
-
-/*
- * Free all unused dirty pages in all arenas. Calling this function will slow
- * down subsequent allocations so it is recommended to use it only when
- * memory needs to be reclaimed at all costs (see bug 805855). This function
- * provides functionality similar to mallctl("arenas.purge") in jemalloc 3.
- */
-MOZ_JEMALLOC_API void jemalloc_free_dirty_pages();
-
-MOZ_JEMALLOC_API void jemalloc_thread_local_arena(bool enabled);
-
-/*
- * Provide information about any allocation enclosing the given address.
- */
-MOZ_JEMALLOC_API void jemalloc_ptr_info(const void* ptr,
-                                        jemalloc_ptr_info_t* info);
+#define MALLOC_DECL(name, return_type, ...) \
+  MOZ_JEMALLOC_API return_type name(__VA_ARGS__);
+#define MALLOC_FUNCS MALLOC_FUNCS_JEMALLOC
+#include "malloc_decls.h"
 
 #endif /* mozmemory_h */
