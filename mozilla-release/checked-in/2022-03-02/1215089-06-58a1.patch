# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1507080405 -7200
# Node ID 84bcd57f2404a4fc48a42b36e5f553c8dec001ff
# Parent  c4c86bcdf3589af0988787943c72457d860d35ff
Bug 1215089 - P6: Remove dead code. r=mattwoodrow

The method isn't called and the comments referring to it are no longer applicable.

MozReview-Commit-ID: 2FFWhj7wzht

diff --git a/gfx/layers/ImageContainer.cpp b/gfx/layers/ImageContainer.cpp
--- a/gfx/layers/ImageContainer.cpp
+++ b/gfx/layers/ImageContainer.cpp
@@ -587,36 +587,24 @@ gfxImageFormat
 PlanarYCbCrImage::GetOffscreenFormat()
 {
   return mOffscreenFormat == SurfaceFormat::UNKNOWN ?
     gfxVars::OffscreenFormat() :
     mOffscreenFormat;
 }
 
 bool
-PlanarYCbCrImage::AdoptData(const Data &aData)
+PlanarYCbCrImage::AdoptData(const Data& aData)
 {
   mData = aData;
   mSize = aData.mPicSize;
   mOrigin = gfx::IntPoint(aData.mPicX, aData.mPicY);
   return true;
 }
 
-uint8_t*
-RecyclingPlanarYCbCrImage::AllocateAndGetNewBuffer(uint32_t aSize)
-{
-  // get new buffer
-  mBuffer = AllocateBuffer(aSize);
-  if (mBuffer) {
-    // update buffer size
-    mBufferSize = aSize;
-  }
-  return mBuffer.get();
-}
-
 already_AddRefed<gfx::SourceSurface>
 PlanarYCbCrImage::GetAsSourceSurface()
 {
   if (mSourceSurface) {
     RefPtr<gfx::SourceSurface> surface(mSourceSurface);
     return surface.forget();
   }
 
diff --git a/gfx/layers/ImageContainer.h b/gfx/layers/ImageContainer.h
--- a/gfx/layers/ImageContainer.h
+++ b/gfx/layers/ImageContainer.h
@@ -808,28 +808,19 @@ public:
 
   /**
    * This makes a copy of the data buffers, in order to support functioning
    * in all different layer managers.
    */
   virtual bool CopyData(const Data& aData) = 0;
 
   /**
-   * This doesn't make a copy of the data buffers. Can be used when mBuffer is
-   * pre allocated with AllocateAndGetNewBuffer(size) and then AdoptData is
-   * called to only update the picture size, planes etc. fields in mData.
-   * The GStreamer media backend uses this to decode into PlanarYCbCrImage(s)
-   * directly.
+   * This doesn't make a copy of the data buffers.
    */
-  virtual bool AdoptData(const Data &aData);
-
-  /**
-   * This allocates and returns a new buffer
-   */
-  virtual uint8_t* AllocateAndGetNewBuffer(uint32_t aSize) = 0;
+  virtual bool AdoptData(const Data& aData);
 
   /**
    * Ask this Image to not convert YUV to RGB during SetData, and make
    * the original data available through GetData. This is optional,
    * and not all PlanarYCbCrImages will support it.
    */
   virtual void SetDelayedConversion(bool aDelayed) { }
 
@@ -875,17 +866,16 @@ protected:
   uint32_t mBufferSize;
 };
 
 class RecyclingPlanarYCbCrImage: public PlanarYCbCrImage {
 public:
   explicit RecyclingPlanarYCbCrImage(BufferRecycleBin *aRecycleBin) : mRecycleBin(aRecycleBin) {}
   virtual ~RecyclingPlanarYCbCrImage() override;
   virtual bool CopyData(const Data& aData) override;
-  virtual uint8_t* AllocateAndGetNewBuffer(uint32_t aSize) override;
   virtual size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const override;
 protected:
 
   /**
    * Return a buffer to store image data in.
    */
   mozilla::UniquePtr<uint8_t[]> AllocateBuffer(uint32_t aSize);
 
diff --git a/gfx/layers/ipc/SharedPlanarYCbCrImage.cpp b/gfx/layers/ipc/SharedPlanarYCbCrImage.cpp
--- a/gfx/layers/ipc/SharedPlanarYCbCrImage.cpp
+++ b/gfx/layers/ipc/SharedPlanarYCbCrImage.cpp
@@ -100,57 +100,19 @@ SharedPlanarYCbCrImage::CopyData(const P
   if (!UpdateYCbCrTextureClient(mTextureClient, aData)) {
     MOZ_ASSERT(false, "Failed to copy YCbCr data into the TextureClient");
     return false;
   }
   mTextureClient->MarkImmutable();
   return true;
 }
 
-// needs to be overriden because the parent class sets mBuffer which we
-// do not want to happen.
-uint8_t*
-SharedPlanarYCbCrImage::AllocateAndGetNewBuffer(uint32_t aSize)
+bool
+SharedPlanarYCbCrImage::AdoptData(const Data& aData)
 {
-  MOZ_ASSERT(!mTextureClient, "This image already has allocated data");
-  size_t size = ImageDataSerializer::ComputeYCbCrBufferSize(aSize);
-  if (!size) {
-    return nullptr;
-  }
-
-  // XXX Add YUVColorSpace handling. Use YUVColorSpace::BT601 for now.
-  mTextureClient = TextureClient::CreateForYCbCrWithBufferSize(mCompositable->GetForwarder(),
-                                                               size,
-                                                               YUVColorSpace::BT601,
-                                                               mCompositable->GetTextureFlags());
-
-  // get new buffer _without_ setting mBuffer.
-  if (!mTextureClient) {
-    return nullptr;
-  }
-
-  // update buffer size
-  mBufferSize = size;
-
-  MappedYCbCrTextureData mapped;
-  if (mTextureClient->BorrowMappedYCbCrData(mapped)) {
-    // The caller expects a pointer to the beginning of the writable part of the
-    // buffer which is where the y channel starts by default.
-    return mapped.y.data;
-  } else {
-    MOZ_CRASH("GFX: Cannot borrow mapped YCbCr data");
-  }
-}
-
-bool
-SharedPlanarYCbCrImage::AdoptData(const Data &aData)
-{
-  // AdoptData is used to update YUV plane offsets without (re)allocating
-  // memory previously allocated with AllocateAndGetNewBuffer().
-
   MOZ_ASSERT(mTextureClient, "This Image should have already allocated data");
   if (!mTextureClient) {
     return false;
   }
   mData = aData;
   mSize = aData.mPicSize;
   mOrigin = gfx::IntPoint(aData.mPicX, aData.mPicY);
 
diff --git a/gfx/layers/ipc/SharedPlanarYCbCrImage.h b/gfx/layers/ipc/SharedPlanarYCbCrImage.h
--- a/gfx/layers/ipc/SharedPlanarYCbCrImage.h
+++ b/gfx/layers/ipc/SharedPlanarYCbCrImage.h
@@ -31,20 +31,19 @@ protected:
   ~SharedPlanarYCbCrImage();
 
 public:
   virtual TextureClient* GetTextureClient(KnowsCompositor* aForwarder) override;
   virtual uint8_t* GetBuffer() override;
 
   virtual already_AddRefed<gfx::SourceSurface> GetAsSourceSurface() override;
   virtual bool CopyData(const PlanarYCbCrData& aData) override;
-  virtual bool AdoptData(const Data &aData) override;
+  virtual bool AdoptData(const Data& aData) override;
 
   virtual bool Allocate(PlanarYCbCrData& aData);
-  virtual uint8_t* AllocateAndGetNewBuffer(uint32_t aSize) override;
 
   virtual bool IsValid() override;
 
   virtual size_t SizeOfIncludingThis(MallocSizeOf aMallocSizeOf) const override
   {
     return aMallocSizeOf(this) + SizeOfExcludingThis(aMallocSizeOf);
   }
 
