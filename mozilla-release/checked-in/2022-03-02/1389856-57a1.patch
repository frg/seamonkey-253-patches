# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1502651221 25200
#      Sun Aug 13 12:07:01 2017 -0700
# Node ID a39fe6811d82378e71031aa4d85529138a647d8a
# Parent  e3650535c076526e5e1d04b356272c230392711c
Bug 1389856: Optimize EventEmitter.emit for the common case. r=zombie

MozReview-Commit-ID: 3dbgZfG7l8T

diff --git a/toolkit/components/extensions/ExtensionUtils.jsm b/toolkit/components/extensions/ExtensionUtils.jsm
--- a/toolkit/components/extensions/ExtensionUtils.jsm
+++ b/toolkit/components/extensions/ExtensionUtils.jsm
@@ -234,35 +234,48 @@ class EventEmitter {
     };
     this[ONCE_MAP].set(listener, wrapper);
 
     this.on(event, wrapper);
   }
 
 
   /**
-   * Triggers all listeners for the given event, and returns a promise
-   * which resolves when all listeners have been called, and any
-   * promises they have returned have likewise resolved.
+   * Triggers all listeners for the given event. If any listeners return
+   * a value, returns a promise which resolves when all returned
+   * promises have resolved. Otherwise, returns undefined.
    *
    * @param {string} event
    *       The name of the event to emit.
    * @param {any} args
    *        Arbitrary arguments to pass to the listener functions, after
    *        the event name.
-   * @returns {Promise}
+   * @returns {Promise?}
    */
   emit(event, ...args) {
-    let listeners = this[LISTENERS].get(event) || new Set();
+    let listeners = this[LISTENERS].get(event);
+
+    if (listeners) {
+      let promises = [];
 
-    let promises = Array.from(listeners, listener => {
-      return runSafeSyncWithoutClone(listener, event, ...args);
-    });
+      for (let listener of listeners) {
+        try {
+          let result = listener(event, ...args);
+          if (result !== undefined) {
+            promises.push(result);
+          }
+        } catch (e) {
+          Cu.reportError(e);
+        }
+      }
 
-    return Promise.all(promises);
+      if (promises.length) {
+        return Promise.all(promises);
+      }
+    }
   }
 }
 
 /**
  * A set with a limited number of slots, which flushes older entries as
  * newer ones are added.
  *
  * @param {integer} limit
