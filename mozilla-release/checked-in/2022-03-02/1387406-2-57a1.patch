# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1501852323 -32400
#      Fri Aug 04 22:12:03 2017 +0900
# Node ID 17cce816b7a470247e24191640ad416effb714a4
# Parent  b68a965b3c897c164d119eee0207abb91ed4877d
Bug 1387406 - part2: nsIDocShell should have GetHTMLEditor() and SetHTMLEditor() r=smaug

C++ code should be accessible to editor of nsDocShell.  However, nsIDocShell needs to have the methods.  Therefore, this patch assumes that nsDocShell is the only subclass of nsIDocShell and creates 2 inline methods to access nsDocShell methods.

MozReview-Commit-ID: ByXtTB5X4cB

diff --git a/docshell/base/nsDocShell.cpp b/docshell/base/nsDocShell.cpp
--- a/docshell/base/nsDocShell.cpp
+++ b/docshell/base/nsDocShell.cpp
@@ -13181,46 +13181,51 @@ nsDocShell::ShouldDiscardLayoutState(nsI
   Unused << aChannel->IsNoStoreResponse(&noStore);
   return noStore;
 }
 
 NS_IMETHODIMP
 nsDocShell::GetEditor(nsIEditor** aEditor)
 {
   NS_ENSURE_ARG_POINTER(aEditor);
-
-  if (!mEditorData) {
-    *aEditor = nullptr;
-    return NS_OK;
-  }
-
-  RefPtr<HTMLEditor> htmlEditor = mEditorData->GetHTMLEditor();
+  RefPtr<HTMLEditor> htmlEditor = GetHTMLEditorInternal();
   htmlEditor.forget(aEditor);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsDocShell::SetEditor(nsIEditor* aEditor)
 {
-  if (!aEditor && !mEditorData) {
-    return NS_OK;
-  }
-
   HTMLEditor* htmlEditor = aEditor ? aEditor->AsHTMLEditor() : nullptr;
   // If TextEditor comes, throw an error.
   if (aEditor && !htmlEditor) {
     return NS_ERROR_INVALID_ARG;
   }
+  return SetHTMLEditorInternal(htmlEditor);
+}
+
+HTMLEditor*
+nsDocShell::GetHTMLEditorInternal()
+{
+  return mEditorData ? mEditorData->GetHTMLEditor() : nullptr;
+}
+
+nsresult
+nsDocShell::SetHTMLEditorInternal(HTMLEditor* aHTMLEditor)
+{
+  if (!aHTMLEditor && !mEditorData) {
+    return NS_OK;
+  }
 
   nsresult rv = EnsureEditorData();
   if (NS_FAILED(rv)) {
     return rv;
   }
 
-  return mEditorData->SetHTMLEditor(htmlEditor);
+  return mEditorData->SetHTMLEditor(aHTMLEditor);
 }
 
 NS_IMETHODIMP
 nsDocShell::GetEditable(bool* aEditable)
 {
   NS_ENSURE_ARG_POINTER(aEditable);
   *aEditable = mEditorData && mEditorData->GetEditable();
   return NS_OK;
@@ -15102,16 +15107,30 @@ nsDocShell::GetAwaitingLargeAlloc(bool* 
 }
 
 NS_IMETHODIMP_(void)
 nsDocShell::GetOriginAttributes(mozilla::OriginAttributes& aAttrs)
 {
   aAttrs = mOriginAttributes;
 }
 
+HTMLEditor*
+nsIDocShell::GetHTMLEditor()
+{
+  nsDocShell* docShell = static_cast<nsDocShell*>(this);
+  return docShell->GetHTMLEditorInternal();
+}
+
+nsresult
+nsIDocShell::SetHTMLEditor(HTMLEditor* aHTMLEditor)
+{
+  nsDocShell* docShell = static_cast<nsDocShell*>(this);
+  return docShell->SetHTMLEditorInternal(aHTMLEditor);
+}
+
 NS_IMETHODIMP
 nsDocShell::GetDisplayMode(uint32_t* aDisplayMode)
 {
   NS_ENSURE_ARG_POINTER(aDisplayMode);
   *aDisplayMode = mDisplayMode;
   return NS_OK;
 }
 
diff --git a/docshell/base/nsDocShell.h b/docshell/base/nsDocShell.h
--- a/docshell/base/nsDocShell.h
+++ b/docshell/base/nsDocShell.h
@@ -59,16 +59,17 @@
 #include "nsCRT.h"
 #include "prtime.h"
 #include "nsRect.h"
 #include "Units.h"
 #include "nsIDeprecationWarner.h"
 
 namespace mozilla {
 class Encoding;
+class HTMLEditor;
 enum class TaskCategory;
 namespace dom {
 class EventTarget;
 class PendingGlobalHistoryEntry;
 typedef uint32_t ScreenOrientationInternal;
 } // namespace dom
 } // namespace mozilla
 
@@ -286,16 +287,19 @@ public:
   void SetInFrameSwap(bool aInSwap)
   {
     mInFrameSwap = aInSwap;
   }
   bool InFrameSwap();
 
   const Encoding* GetForcedCharset() { return mForcedCharset; }
 
+  mozilla::HTMLEditor* GetHTMLEditorInternal();
+  nsresult SetHTMLEditorInternal(mozilla::HTMLEditor* aHTMLEditor);
+
 private:
   bool CanSetOriginAttributes();
 
 public:
   const mozilla::OriginAttributes&
   GetOriginAttributes()
   {
     return mOriginAttributes;
diff --git a/docshell/base/nsIDocShell.idl b/docshell/base/nsIDocShell.idl
--- a/docshell/base/nsIDocShell.idl
+++ b/docshell/base/nsIDocShell.idl
@@ -11,16 +11,17 @@
 %{ C++
 #include "js/TypeDecls.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/NotNull.h"
 class nsPresContext;
 class nsIPresShell;
 namespace mozilla {
 class Encoding;
+class HTMLEditor;
 }
 %}
 
 /**
  * The nsIDocShell interface.
  */
 
 [ptr] native nsPresContext(nsPresContext);
@@ -1158,16 +1159,25 @@ interface nsIDocShell : nsIDocShellTreeI
    */
   attribute boolean useTrackingProtection;
 
  /**
   * Fire a dummy location change event asynchronously.
   */
   [noscript] void dispatchLocationChangeEvent();
 
+%{C++
+  /**
+   * These methods call nsDocShell::GetHTMLEditorInternal() and
+   * nsDocShell::SetHTMLEditorInternal() with static_cast.
+   */
+  mozilla::HTMLEditor* GetHTMLEditor();
+  nsresult SetHTMLEditor(mozilla::HTMLEditor* aHTMLEditor);
+%}
+
   /**
    * Allowed CSS display modes. This needs to be kept in
    * sync with similar values in nsStyleConsts.h
    */
   const unsigned long DISPLAY_MODE_BROWSER = 0;
   const unsigned long DISPLAY_MODE_MINIMAL_UI = 1;
   const unsigned long DISPLAY_MODE_STANDALONE = 2;
   const unsigned long DISPLAY_MODE_FULLSCREEN = 3;
