# HG changeset patch
# User Matthew Gregan <kinetik@flim.org>
# Date 1502326808 -43200
# Node ID 8e8da5bc4301ee7dfde83a05a5567a97831608ef
# Parent  ddc09d80caba59ccd6dec22a2a725ef4566f84ca
Bug 1388236 - Remove B2G-only AudioChannel code from WebAudio.  r=padenot

diff --git a/dom/media/webaudio/AudioDestinationNode.cpp b/dom/media/webaudio/AudioDestinationNode.cpp
--- a/dom/media/webaudio/AudioDestinationNode.cpp
+++ b/dom/media/webaudio/AudioDestinationNode.cpp
@@ -323,17 +323,16 @@ NS_IMPL_RELEASE_INHERITED(AudioDestinati
 
 AudioDestinationNode::AudioDestinationNode(AudioContext* aContext,
                                            bool aIsOffline,
                                            uint32_t aNumberOfChannels,
                                            uint32_t aLength, float aSampleRate)
   : AudioNode(aContext, aIsOffline ? aNumberOfChannels : 2,
               ChannelCountMode::Explicit, ChannelInterpretation::Speakers)
   , mFramesToProduce(aLength)
-  , mAudioChannel(AudioChannel::Normal)
   , mIsOffline(aIsOffline)
   , mAudioChannelSuspended(false)
   , mCaptured(false)
   , mAudible(AudioChannelService::AudibleState::eAudible)
 {
   nsPIDOMWindowInner* window = aContext->GetParentObject();
   MediaStreamGraph* graph =
     aIsOffline
@@ -586,75 +585,16 @@ AudioDestinationNode::WindowAudioCapture
       mCaptureStreamPort->Destroy();
     }
     mCaptured = aCapture;
   }
 
   return NS_OK;
 }
 
-AudioChannel
-AudioDestinationNode::MozAudioChannelType() const
-{
-  return mAudioChannel;
-}
-
-void
-AudioDestinationNode::SetMozAudioChannelType(AudioChannel aValue, ErrorResult& aRv)
-{
-  if (Context()->IsOffline()) {
-    aRv.Throw(NS_ERROR_DOM_INVALID_STATE_ERR);
-    return;
-  }
-
-  if (aValue != mAudioChannel &&
-      CheckAudioChannelPermissions(aValue)) {
-    mAudioChannel = aValue;
-
-    if (mAudioChannelAgent) {
-      CreateAudioChannelAgent();
-    }
-  }
-}
-
-bool
-AudioDestinationNode::CheckAudioChannelPermissions(AudioChannel aValue)
-{
-  // Only normal channel doesn't need permission.
-  if (aValue == AudioChannel::Normal) {
-    return true;
-  }
-
-  // Maybe this audio channel is equal to the default one.
-  if (aValue == AudioChannelService::GetDefaultAudioChannel()) {
-    return true;
-  }
-
-  nsCOMPtr<nsIPermissionManager> permissionManager =
-    services::GetPermissionManager();
-  if (!permissionManager) {
-    return false;
-  }
-
-  nsCOMPtr<nsIScriptObjectPrincipal> sop = do_QueryInterface(GetOwner());
-  NS_ASSERTION(sop, "Window didn't QI to nsIScriptObjectPrincipal!");
-  nsCOMPtr<nsIPrincipal> principal = sop->GetPrincipal();
-
-  uint32_t perm = nsIPermissionManager::UNKNOWN_ACTION;
-
-  nsCString channel;
-  channel.AssignASCII(AudioChannelValues::strings[uint32_t(aValue)].value,
-                      AudioChannelValues::strings[uint32_t(aValue)].length);
-  permissionManager->TestExactPermissionFromPrincipal(principal,
-    nsCString(NS_LITERAL_CSTRING("audio-channel-") + channel).get(),
-    &perm);
-
-  return perm == nsIPermissionManager::ALLOW_ACTION;
-}
-
 nsresult
 AudioDestinationNode::CreateAudioChannelAgent()
 {
   if (mIsOffline) {
     return NS_OK;
   }
 
   nsresult rv = NS_OK;
@@ -662,17 +602,17 @@ AudioDestinationNode::CreateAudioChannel
     rv = mAudioChannelAgent->NotifyStoppedPlaying();
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
     }
   }
 
   mAudioChannelAgent = new AudioChannelAgent();
   rv = mAudioChannelAgent->InitWithWeakCallback(GetOwner(),
-                                           static_cast<int32_t>(mAudioChannel),
+                                           static_cast<int32_t>(AudioChannel::Normal),
                                            this);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   return NS_OK;
 }
 
diff --git a/dom/media/webaudio/AudioDestinationNode.h b/dom/media/webaudio/AudioDestinationNode.h
--- a/dom/media/webaudio/AudioDestinationNode.h
+++ b/dom/media/webaudio/AudioDestinationNode.h
@@ -54,18 +54,16 @@ public:
 
   void Suspend();
   void Resume();
 
   void StartRendering(Promise* aPromise);
 
   void OfflineShutdown();
 
-  AudioChannel MozAudioChannelType() const;
-
   void NotifyMainThreadStreamFinished() override;
   void FireOfflineCompletionEvent();
 
   nsresult CreateAudioChannelAgent();
   void DestroyAudioChannelAgent();
 
   const char* NodeType() const override
   {
@@ -83,29 +81,24 @@ public:
     MOZ_ASSERT(mIsOffline);
     return mFramesToProduce;
   }
 
 protected:
   virtual ~AudioDestinationNode();
 
 private:
-  void SetMozAudioChannelType(AudioChannel aValue, ErrorResult& aRv);
-  bool CheckAudioChannelPermissions(AudioChannel aValue);
-
   SelfReference<AudioDestinationNode> mOfflineRenderingRef;
   uint32_t mFramesToProduce;
 
   nsCOMPtr<nsIAudioChannelAgent> mAudioChannelAgent;
   RefPtr<MediaInputPort> mCaptureStreamPort;
 
   RefPtr<Promise> mOfflineRenderingPromise;
 
-  // Audio Channel Type.
-  AudioChannel mAudioChannel;
   bool mIsOffline;
   bool mAudioChannelSuspended;
 
   bool mCaptured;
   AudioChannelService::AudibleState mAudible;
 };
 
 } // namespace dom

