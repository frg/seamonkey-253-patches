# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1510800278 28800
# Node ID 8a49cb129f99e31bc5a4d3e05520d553fa9adcc3
# Parent  cf1136e67e09fbfb196d256104ba0986ded3364a
Bug 1417312 - Adjust visibility of some CacheMap.h members. - r=daoshengmu

MozReview-Commit-ID: fTsP6xbrMU

diff --git a/dom/canvas/CacheMap.h b/dom/canvas/CacheMap.h
--- a/dom/canvas/CacheMap.h
+++ b/dom/canvas/CacheMap.h
@@ -10,62 +10,65 @@
 #include "mozilla/UniquePtr.h"
 #include <map>
 #include <unordered_set>
 #include <vector>
 
 namespace mozilla {
 
 namespace detail {
-struct CacheMapUntypedEntry;
+class CacheMapUntypedEntry;
 }
 
 class CacheMapInvalidator
 {
-    friend struct detail::CacheMapUntypedEntry;
+    friend class detail::CacheMapUntypedEntry;
 
     mutable std::unordered_set<const detail::CacheMapUntypedEntry*> mCacheEntries;
 
 public:
     ~CacheMapInvalidator() {
         InvalidateCaches();
     }
 
     void InvalidateCaches() const;
 };
 
 namespace detail {
 
-struct CacheMapUntypedEntry
+class CacheMapUntypedEntry
 {
     template<typename, typename> friend class CacheMap;
 
+private:
     const std::vector<const CacheMapInvalidator*> mInvalidators;
 
+protected:
     CacheMapUntypedEntry(std::vector<const CacheMapInvalidator*>&& invalidators);
     ~CacheMapUntypedEntry();
 
 public:
     virtual void Invalidate() const = 0;
 };
 
-template<typename T>
 struct DerefLess final {
+    template<typename T>
     bool operator ()(const T* const a, const T* const b) const {
         return *a < *b;
     }
 };
 
 } // namespace detail
 
 
 template<typename KeyT, typename ValueT>
 class CacheMap final
 {
-    struct Entry final : public detail::CacheMapUntypedEntry {
+    class Entry final : public detail::CacheMapUntypedEntry {
+    public:
         CacheMap& mParent;
         const KeyT mKey;
         const ValueT mValue;
 
         Entry(std::vector<const CacheMapInvalidator*>&& invalidators, CacheMap& parent,
               KeyT&& key, ValueT&& value)
             : detail::CacheMapUntypedEntry(Move(invalidators))
             , mParent(parent)
@@ -78,17 +81,17 @@ class CacheMap final
             MOZ_ALWAYS_TRUE( erased == 1 );
         }
 
         bool operator <(const Entry& x) const {
             return mKey < x.mKey;
         }
     };
 
-    typedef std::map<const KeyT*, UniquePtr<const Entry>, detail::DerefLess<KeyT>> MapT;
+    typedef std::map<const KeyT*, UniquePtr<const Entry>, detail::DerefLess> MapT;
     MapT mMap;
 
 public:
     const ValueT* Insert(KeyT&& key, ValueT&& value,
                          std::vector<const CacheMapInvalidator*>&& invalidators)
     {
         UniquePtr<const Entry> entry( new Entry(Move(invalidators), *this, Move(key),
                                                 Move(value)) );
