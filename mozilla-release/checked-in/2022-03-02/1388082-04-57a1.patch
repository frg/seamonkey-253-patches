# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1502128589 -3600
#      Mon Aug 07 18:56:29 2017 +0100
# Node ID 4544d74567dfeb74cbc787ea5eba9354ae984b24
# Parent  757f65fac883dc958c32274e3d4ab7a8983d5227
Bug 1388082 - Switch to async/await in interaction module. r=automatedtester

MozReview-Commit-ID: 2aUUrT3KXRF

diff --git a/testing/marionette/interaction.js b/testing/marionette/interaction.js
--- a/testing/marionette/interaction.js
+++ b/testing/marionette/interaction.js
@@ -345,25 +345,25 @@ interaction.flushEventLoop = function* (
 /**
  * Appends <var>path</var> to an <tt>&lt;input type=file&gt;</tt>'s
  * file list.
  *
  * @param {HTMLInputElement} el
  *     An <tt>&lt;input type=file&gt;</tt> element.
  * @param {string} path
  *     Full path to file.
+ *
+ * @throws {InvalidArgumentError}
+ *     If <var>path</var> can not be found.
  */
-interaction.uploadFile = function* (el, path) {
-  let file = yield File.createFromFileName(path).then(file => {
-    return file;
-  }, () => {
-    return null;
-  });
-
-  if (!file) {
+interaction.uploadFile = async function(el, path) {
+  let file;
+  try {
+    file = await File.createFromFileName(path);
+  } catch (e) {
     throw new InvalidArgumentError("File not found: " + path);
   }
 
   let fs = Array.prototype.slice.call(el.files);
   fs.push(file);
 
   // <input type=file> opens OS widget dialogue
   // which means the mousedown/focus/mouseup/click events
@@ -386,17 +386,17 @@ interaction.uploadFile = function* (el, 
  * @param {DOMElement} el
  *     An form element, e.g. input, textarea, etc.
  * @param {string} value
  *     The value to be set.
  *
  * @throws {TypeError}
  *     If <var>el</var> is not an supported form element.
  */
-interaction.setFormControlValue = function* (el, value) {
+interaction.setFormControlValue = function(el, value) {
   if (!COMMON_FORM_CONTROLS.has(el.localName)) {
     throw new TypeError("This function is for form elements only");
   }
 
   el.value = value;
 
   if (INPUT_TYPES_NO_EVENT.has(el.type)) {
     return;
@@ -413,24 +413,23 @@ interaction.setFormControlValue = functi
  *     Element to send key events to.
  * @param {Array.<string>} value
  *     Sequence of keystrokes to send to the element.
  * @param {boolean} ignoreVisibility
  *     Flag to enable or disable element visibility tests.
  * @param {boolean=} [strict=false] strict
  *     Enforce strict accessibility tests.
  */
-interaction.sendKeysToElement = function(
+interaction.sendKeysToElement = async function(
     el, value, ignoreVisibility, strict = false) {
   let win = getWindow(el);
   let a11y = accessibility.get(strict);
-  return a11y.getAccessible(el, true).then(acc => {
-    a11y.assertActionable(acc, el);
-    event.sendKeysToElement(value, el, {ignoreVisibility: false}, win);
-  });
+  let acc = await a11y.getAccessible(el, true);
+  a11y.assertActionable(acc, el);
+  event.sendKeysToElement(value, el, {ignoreVisibility: false}, win);
 };
 
 /**
  * Determine the element displayedness of an element.
  *
  * @param {DOMElement|XULElement} el
  *     Element to determine displayedness of.
  * @param {boolean=} [strict=false] strict
