# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1518858256 0
#      Sat Feb 17 09:04:16 2018 +0000
# Node ID 2ad6019ed5ce8fda34be421de06b228ce085359c
# Parent  c3f16a179c93d9b75dd4581280967fa6ef364cc3
Bug 1438522 - Cherry-pick recent FreeType fixes for variation fonts and flex rendering. r=jrmuizel

diff --git a/modules/freetype2/README.moz-patches b/modules/freetype2/README.moz-patches
--- a/modules/freetype2/README.moz-patches
+++ b/modules/freetype2/README.moz-patches
@@ -1,6 +1,11 @@
 This directory contains FreeType v2.9 downloaded from
 https://download.savannah.gnu.org/releases/freetype/
 
-There are currently no local changes applied to the FreeType tree,
-except that the file docs/LICENSE.TXT has been copied to the top-level
-directory of the FreeType tree as LICENSE.TXT.
+The following post-2.9 commits have been cherry-picked from
+the upstream FreeType repository (see bug 1434697, bug 1438522):
+
+http://git.savannah.gnu.org/cgit/freetype/freetype2.git/commit/?id=994eb2b34934bc5face9f83b2d3b12cf7a9262ab
+http://git.savannah.gnu.org/cgit/freetype/freetype2.git/commit/?id=cc2f3cdecff5a351e7e8961b9f2e389ab740231a
+http://git.savannah.gnu.org/cgit/freetype/freetype2.git/commit/?id=4a03f17449ae45f0dacf4de4694ccd6e5e1b24d1
+http://git.savannah.gnu.org/cgit/freetype/freetype2.git/commit/?id=68dddcdcbe18a08d778026efc01b1369e35cbf6a
+http://git.savannah.gnu.org/cgit/freetype/freetype2.git/commit/?id=29c759284e305ec428703c9a5831d0b1fc3497ef
diff --git a/modules/freetype2/src/psaux/psintrp.c b/modules/freetype2/src/psaux/psintrp.c
--- a/modules/freetype2/src/psaux/psintrp.c
+++ b/modules/freetype2/src/psaux/psintrp.c
@@ -847,17 +847,18 @@
         /* width is defined or default after this */
         haveWidth = TRUE;
 
         if ( decoder->width_only )
           goto exit;
 
         curY = ADD_INT32( curY, cf2_stack_popFixed( opStack ) );
 
-        cf2_glyphpath_moveTo( &glyphPath, curX, curY );
+        if ( !decoder->flex_state )
+          cf2_glyphpath_moveTo( &glyphPath, curX, curY );
 
         break;
 
       case cf2_cmdRLINETO:
         {
           CF2_UInt  idx;
           CF2_UInt  count = cf2_stack_count( opStack );
 
@@ -2669,17 +2670,18 @@
         /* width is defined or default after this */
         haveWidth = TRUE;
 
         if ( decoder->width_only )
           goto exit;
 
         curX = ADD_INT32( curX, cf2_stack_popFixed( opStack ) );
 
-        cf2_glyphpath_moveTo( &glyphPath, curX, curY );
+        if ( !decoder->flex_state )
+          cf2_glyphpath_moveTo( &glyphPath, curX, curY );
 
         break;
 
       case cf2_cmdRLINECURVE:
         {
           CF2_UInt  count = cf2_stack_count( opStack );
           CF2_UInt  idx   = 0;
 
diff --git a/modules/freetype2/src/truetype/ttgxvar.c b/modules/freetype2/src/truetype/ttgxvar.c
--- a/modules/freetype2/src/truetype/ttgxvar.c
+++ b/modules/freetype2/src/truetype/ttgxvar.c
@@ -2757,18 +2757,19 @@
         if ( *c != a->def )
         {
           *c        = a->def;
           have_diff = 1;
         }
       }
     }
 
-    /* return value -1 indicates `no change' */
-    if ( !have_diff )
+    /* return value -1 indicates `no change';                      */
+    /* we can exit early if `normalizedcoords' is already computed */
+    if ( blend->normalizedcoords && !have_diff )
       return -1;
 
     if ( FT_NEW_ARRAY( normalized, mmvar->num_axis ) )
       goto Exit;
 
     if ( !face->blend->avar_loaded )
       ft_var_load_avar( face );
 
diff --git a/modules/freetype2/src/truetype/ttinterp.c b/modules/freetype2/src/truetype/ttinterp.c
--- a/modules/freetype2/src/truetype/ttinterp.c
+++ b/modules/freetype2/src/truetype/ttinterp.c
@@ -5777,16 +5777,17 @@
   static void
   Ins_MSIRP( TT_ExecContext  exc,
              FT_Long*        args )
   {
     FT_UShort   point = 0;
     FT_F26Dot6  distance;
 #ifdef TT_SUPPORT_SUBPIXEL_HINTING_INFINALITY
     FT_F26Dot6  control_value_cutin = 0;
+    FT_F26Dot6  delta;
 
 
     if ( SUBPIXEL_HINTING_INFINALITY )
     {
       control_value_cutin = exc->GS.control_value_cutin;
 
       if ( exc->ignore_x_mode                                 &&
            exc->GS.freeVector.x != 0                          &&
@@ -5812,21 +5813,25 @@
       exc->zp1.org[point] = exc->zp0.org[exc->GS.rp0];
       exc->func_move_orig( exc, &exc->zp1, point, args[1] );
       exc->zp1.cur[point] = exc->zp1.org[point];
     }
 
     distance = PROJECT( exc->zp1.cur + point, exc->zp0.cur + exc->GS.rp0 );
 
 #ifdef TT_SUPPORT_SUBPIXEL_HINTING_INFINALITY
+    delta = SUB_LONG( distance, args[1] );
+    if ( delta < 0 )
+      delta = NEG_LONG( delta );
+
     /* subpixel hinting - make MSIRP respect CVT cut-in; */
-    if ( SUBPIXEL_HINTING_INFINALITY                                    &&
-         exc->ignore_x_mode                                             &&
-         exc->GS.freeVector.x != 0                                      &&
-         FT_ABS( SUB_LONG( distance, args[1] ) ) >= control_value_cutin )
+    if ( SUBPIXEL_HINTING_INFINALITY  &&
+         exc->ignore_x_mode           &&
+         exc->GS.freeVector.x != 0    &&
+         delta >= control_value_cutin )
       distance = args[1];
 #endif /* TT_SUPPORT_SUBPIXEL_HINTING_INFINALITY */
 
     exc->func_move( exc,
                     &exc->zp1,
                     point,
                     SUB_LONG( args[1], distance ) );
 
@@ -5973,17 +5978,24 @@
          exc->GS.freeVector.y != 0                      )
       distance = 0;
 #endif /* TT_SUPPORT_SUBPIXEL_HINTING_INFINALITY */
 
     org_dist = FAST_PROJECT( &exc->zp0.cur[point] );
 
     if ( ( exc->opcode & 1 ) != 0 )   /* rounding and control cut-in flag */
     {
-      if ( FT_ABS( distance - org_dist ) > control_value_cutin )
+      FT_F26Dot6  delta;
+
+
+      delta = SUB_LONG( distance, org_dist );
+      if ( delta < 0 )
+        delta = NEG_LONG( delta );
+
+      if ( delta > control_value_cutin )
         distance = org_dist;
 
 #ifdef TT_SUPPORT_SUBPIXEL_HINTING_INFINALITY
       if ( SUBPIXEL_HINTING_INFINALITY &&
            exc->ignore_x_mode          &&
            exc->GS.freeVector.x != 0   )
         distance = Round_None( exc,
                                distance,
@@ -6254,29 +6266,36 @@
 
     if ( ( exc->opcode & 4 ) != 0 )
     {
       /* XXX: UNDOCUMENTED!  Only perform cut-in test when both points */
       /*      refer to the same zone.                                  */
 
       if ( exc->GS.gep0 == exc->GS.gep1 )
       {
+        FT_F26Dot6  delta;
+
+
         /* XXX: According to Greg Hitchcock, the following wording is */
         /*      the right one:                                        */
         /*                                                            */
         /*        When the absolute difference between the value in   */
         /*        the table [CVT] and the measurement directly from   */
         /*        the outline is _greater_ than the cut_in value, the */
         /*        outline measurement is used.                        */
         /*                                                            */
         /*      This is from `instgly.doc'.  The description in       */
         /*      `ttinst2.doc', version 1.66, is thus incorrect since  */
         /*      it implies `>=' instead of `>'.                       */
 
-        if ( FT_ABS( cvt_dist - org_dist ) > control_value_cutin )
+        delta = SUB_LONG( cvt_dist, org_dist );
+        if ( delta < 0 )
+          delta = NEG_LONG( delta );
+
+        if ( delta > control_value_cutin )
           cvt_dist = org_dist;
       }
 
       distance = exc->func_round(
                    exc,
                    cvt_dist,
                    exc->tt_metrics.compensations[exc->opcode & 3] );
     }
@@ -6284,17 +6303,24 @@
     {
 
 #ifdef TT_SUPPORT_SUBPIXEL_HINTING_INFINALITY
       /* do cvt cut-in always in MIRP for sph */
       if ( SUBPIXEL_HINTING_INFINALITY  &&
            exc->ignore_x_mode           &&
            exc->GS.gep0 == exc->GS.gep1 )
       {
-        if ( FT_ABS( cvt_dist - org_dist ) > control_value_cutin )
+        FT_F26Dot6  delta;
+
+
+        delta = SUB_LONG( cvt_dist, org_dist );
+        if ( delta < 0 )
+          delta = NEG_LONG( delta );
+
+        if ( delta > control_value_cutin )
           cvt_dist = org_dist;
       }
 #endif /* TT_SUPPORT_SUBPIXEL_HINTING_INFINALITY */
 
       distance = Round_None(
                    exc,
                    cvt_dist,
                    exc->tt_metrics.compensations[exc->opcode & 3] );
@@ -7527,18 +7553,26 @@
 
 
     if ( BOUNDS( num_axes, exc->stackSize + 1 - exc->top ) )
     {
       exc->error = FT_THROW( Stack_Overflow );
       return;
     }
 
-    for ( i = 0; i < num_axes; i++ )
-      args[i] = coords[i] >> 2; /* convert 16.16 to 2.14 format */
+    if ( coords )
+    {
+      for ( i = 0; i < num_axes; i++ )
+        args[i] = coords[i] >> 2; /* convert 16.16 to 2.14 format */
+    }
+    else
+    {
+      for ( i = 0; i < num_axes; i++ )
+        args[i] = 0;
+    }
   }
 
 
   /*************************************************************************/
   /*                                                                       */
   /* GETDATA[]:    no idea what this is good for                           */
   /* Opcode range: 0x92                                                    */
   /* Stack:        --> 17                                                  */
