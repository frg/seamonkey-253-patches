# HG changeset patch
# User Mantaroh Yoshinaga <mantaroh@gmail.com>
# Date 1504512128 18000
#      Mon Sep 04 03:02:08 2017 -0500
# Node ID cc1dd06b768883dd5d66b1486558a98f69c4fdff
# Parent  274bc195412ce8ba4baab9b2e7011b810bf14fb6
servo: Merge #18364 - Don't allow interpolating SVGPaintKind::None (from mantaroh:svgpaintkind-interpolation); r=birtles

This is a PR for https://bugzilla.mozilla.org/show_bug.cgi?id=1396483

PR #18103 disallowed interpolation between fill:none and fill:none, but that change was regressed by the refactoring in PR #18239.
This patch restores the intended behavior, disabling animation of SVGPaintKind::None.

<!-- Please describe your changes on the following line: -->

---
<!-- Thank you for contributing to Servo! Please replace each `[ ]` by `[X]` when the step is complete, and replace `__` with appropriate data: -->
- [ x ] `./mach build -d` does not report any errors
- [ x ] `./mach test-tidy` does not report any errors

<!-- Either: -->
There are already these tests in dom/smil/tests of gecko, this PR will enable these tests.
For detail, see https://bugzilla.mozilla.org/show_bug.cgi?id=1396483 .

<!-- Also, please make sure that "Allow edits from maintainers" checkbox is checked, so that we can help you if you get stuck somewhere along the way.-->

<!-- Pull requests that do not address these steps are welcome, but they will require additional verification as part of the review process. -->

Source-Repo: https://github.com/servo/servo
Source-Revision: e5efbeea6e1027931bf6d6f3cb429a00ea05bdec

diff --git a/servo/components/style/values/generics/svg.rs b/servo/components/style/values/generics/svg.rs
--- a/servo/components/style/values/generics/svg.rs
+++ b/servo/components/style/values/generics/svg.rs
@@ -30,16 +30,17 @@ pub struct SVGPaint<ColorType, UrlPaintS
 /// Whereas the spec only allows PaintServer
 /// to have a fallback, Gecko lets the context
 /// properties have a fallback as well.
 #[cfg_attr(feature = "servo", derive(HeapSizeOf))]
 #[derive(Animate, Clone, ComputeSquaredDistance, Debug, PartialEq)]
 #[derive(ToAnimatedValue, ToAnimatedZero, ToComputedValue, ToCss)]
 pub enum SVGPaintKind<ColorType, UrlPaintServer> {
     /// `none`
+    #[animation(error)]
     None,
     /// `<color>`
     Color(ColorType),
     /// `url(...)`
     #[animation(error)]
     PaintServer(UrlPaintServer),
     /// `context-fill`
     ContextFill,
