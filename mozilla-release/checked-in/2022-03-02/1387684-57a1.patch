# HG changeset patch
# User Christoph Kerschbaumer <ckerschb@christophkerschbaumer.com>
# Date 1503475512 -7200
#      Wed Aug 23 10:05:12 2017 +0200
# Node ID f96c5e184fbfc5e5b1712d867ccf1b5cd5f0cd5d
# Parent  0d6ca49f13e35a9c9d30ad9107c82916a2c10d3a
Bug 1387684 - CSP: Special case 'self' for unique opaque origins. r=dveditz

diff --git a/dom/security/nsCSPUtils.cpp b/dom/security/nsCSPUtils.cpp
--- a/dom/security/nsCSPUtils.cpp
+++ b/dom/security/nsCSPUtils.cpp
@@ -276,16 +276,25 @@ CSP_CreateHostSrcFromSelfURI(nsIURI* aSe
   nsCSPHostSrc *hostsrc = new nsCSPHostSrc(NS_ConvertUTF8toUTF16(host));
   hostsrc->setGeneratedFromSelfKeyword();
 
   // Add the scheme.
   nsCString scheme;
   aSelfURI->GetScheme(scheme);
   hostsrc->setScheme(NS_ConvertUTF8toUTF16(scheme));
 
+  // An empty host (e.g. for data:) indicates it's effectively a unique origin.
+  // Please note that we still need to set the scheme on hostsrc (see above),
+  // because it's used for reporting.
+  if (host.EqualsLiteral("")) {
+    hostsrc->setIsUniqueOrigin();
+    // no need to query the port in that case.
+    return hostsrc;
+  }
+
   int32_t port;
   aSelfURI->GetPort(&port);
   // Only add port if it's not default port.
   if (port > 0) {
     nsAutoString portStr;
     portStr.AppendInt(port);
     hostsrc->setPort(portStr);
   }
@@ -518,16 +527,17 @@ nsCSPSchemeSrc::toString(nsAString& outS
   outStr.AppendASCII(":");
 }
 
 /* ===== nsCSPHostSrc ======================== */
 
 nsCSPHostSrc::nsCSPHostSrc(const nsAString& aHost)
   : mHost(aHost)
   , mGeneratedFromSelfKeyword(false)
+  , mIsUniqueOrigin(false)
   , mWithinFrameAncstorsDir(false)
 {
   ToLowerCase(mHost);
 }
 
 nsCSPHostSrc::~nsCSPHostSrc()
 {
 }
@@ -619,17 +629,17 @@ bool
 nsCSPHostSrc::permits(nsIURI* aUri, const nsAString& aNonce, bool aWasRedirected,
                       bool aReportOnly, bool aUpgradeInsecure, bool aParserCreated) const
 {
   if (CSPUTILSLOGENABLED()) {
     CSPUTILSLOG(("nsCSPHostSrc::permits, aUri: %s",
                  aUri->GetSpecOrDefault().get()));
   }
 
-  if (mInvalidated) {
+  if (mInvalidated || mIsUniqueOrigin) {
     return false;
   }
 
   // we are following the enforcement rules from the spec, see:
   // http://www.w3.org/TR/CSP11/#match-source-expression
 
   // 4.3) scheme matching: Check if the scheme matches.
   if (!permitsScheme(mScheme, aUri, aReportOnly, aUpgradeInsecure, mGeneratedFromSelfKeyword)) {
diff --git a/dom/security/nsCSPUtils.h b/dom/security/nsCSPUtils.h
--- a/dom/security/nsCSPUtils.h
+++ b/dom/security/nsCSPUtils.h
@@ -255,16 +255,19 @@ class nsCSPHostSrc : public nsCSPBaseSrc
 
     void setScheme(const nsAString& aScheme);
     void setPort(const nsAString& aPort);
     void appendPath(const nsAString &aPath);
 
     inline void setGeneratedFromSelfKeyword() const
       { mGeneratedFromSelfKeyword = true; }
 
+    inline void setIsUniqueOrigin() const
+      { mIsUniqueOrigin = true; }
+
     inline void setWithinFrameAncestorsDir(bool aValue) const
       { mWithinFrameAncstorsDir = aValue; }
 
     inline void getScheme(nsAString& outStr) const
       { outStr.Assign(mScheme); };
 
     inline void getHost(nsAString& outStr) const
       { outStr.Assign(mHost); };
@@ -276,16 +279,17 @@ class nsCSPHostSrc : public nsCSPBaseSrc
       { outStr.Assign(mPath); };
 
   private:
     nsString mScheme;
     nsString mHost;
     nsString mPort;
     nsString mPath;
     mutable bool mGeneratedFromSelfKeyword;
+    mutable bool mIsUniqueOrigin;
     mutable bool mWithinFrameAncstorsDir;
 };
 
 /* =============== nsCSPKeywordSrc ============ */
 
 class nsCSPKeywordSrc : public nsCSPBaseSrc {
   public:
     explicit nsCSPKeywordSrc(CSPKeyword aKeyword);
diff --git a/testing/web-platform/meta/content-security-policy/frame-src/frame-src-self-unique-origin.html.ini b/testing/web-platform/meta/content-security-policy/frame-src/frame-src-self-unique-origin.html.ini
--- a/testing/web-platform/meta/content-security-policy/frame-src/frame-src-self-unique-origin.html.ini
+++ b/testing/web-platform/meta/content-security-policy/frame-src/frame-src-self-unique-origin.html.ini
@@ -1,5 +1,5 @@
 [frame-src-self-unique-origin.html]
   type: testharness
-  expected:
-    if not e10s: CRASH
-    if e10s: CRASH
+  expected: TIMEOUT
+  [Iframe's url must not match with 'self'. It must be blocked.]
+    expected: TIMEOUT
diff --git a/testing/web-platform/meta/content-security-policy/img-src/img-src-self-unique-origin.html.ini b/testing/web-platform/meta/content-security-policy/img-src/img-src-self-unique-origin.html.ini
--- a/testing/web-platform/meta/content-security-policy/img-src/img-src-self-unique-origin.html.ini
+++ b/testing/web-platform/meta/content-security-policy/img-src/img-src-self-unique-origin.html.ini
@@ -1,5 +1,5 @@
 [img-src-self-unique-origin.html]
   type: testharness
-  expected:
-    if not e10s: CRASH
-    if e10s: CRASH
+  expected: TIMEOUT
+  [Image's url must not match with 'self'. Image must be blocked.]
+    expected: TIMEOUT
