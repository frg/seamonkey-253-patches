# HG changeset patch
# User Boris Chiou <boris.chiou@gmail.com>
# Date 1502683683 18000
#      Sun Aug 13 23:08:03 2017 -0500
# Node ID 9c9e455241e5059f60dee88a16161ba042bd735d
# Parent  b061ddd698e2f270c646427ce1e7f57b91622bf8
servo: Merge #18068 - Fix the conversion from Perspective into ComputedMatrix (from BorisChiou:stylo/animation/perspective); r=birtles

We convert a perspective function into a ComputedMatrix, but we apply the
perspective length to a wrong matrix item. We should put it into m34,
instead of m43. m43 is for translate parameter Z.

---
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [X] These changes fix [Bug 1389023](https://bugzilla.mozilla.org/show_bug.cgi?id=1389023).
- [X] These changes do not require tests because we have many transform tests in Gecko already. However, those tests may not be enough, so we should add more in Gecko later.

Source-Repo: https://github.com/servo/servo
Source-Revision: cd1251082511a44ef9ad020b7196336bca812a49

diff --git a/servo/components/style/properties/helpers/animated_properties.mako.rs b/servo/components/style/properties/helpers/animated_properties.mako.rs
--- a/servo/components/style/properties/helpers/animated_properties.mako.rs
+++ b/servo/components/style/properties/helpers/animated_properties.mako.rs
@@ -1469,21 +1469,27 @@ fn add_weighted_transform_lists(from_lis
                         let matrix_t = rotate_to_matrix(tx, ty, tz, ta);
                         let sum = matrix_f.add_weighted(&matrix_t, self_portion, other_portion)
                                           .unwrap();
 
                         result.push(TransformOperation::Matrix(sum));
                     }
                 }
                 (&TransformOperation::Perspective(fd),
-                 &TransformOperation::Perspective(_td)) => {
+                 &TransformOperation::Perspective(td)) => {
                     let mut fd_matrix = ComputedMatrix::identity();
                     let mut td_matrix = ComputedMatrix::identity();
-                    fd_matrix.m43 = -1. / fd.to_f32_px();
-                    td_matrix.m43 = -1. / _td.to_f32_px();
+                    if fd.0 > 0 {
+                        fd_matrix.m34 = -1. / fd.to_f32_px();
+                    }
+
+                    if td.0 > 0 {
+                        td_matrix.m34 = -1. / td.to_f32_px();
+                    }
+
                     let sum = fd_matrix.add_weighted(&td_matrix, self_portion, other_portion)
                                        .unwrap();
                     result.push(TransformOperation::Matrix(sum));
                 }
                 _ => {
                     // This should be unreachable due to the can_interpolate_list() call.
                     unreachable!();
                 }
