# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1497375133 -7200
#      Tue Jun 13 19:32:13 2017 +0200
# Node ID b8b7771cce0d66e6fbfec285a84ef1b7e2e3ff0d
# Parent  0f4d52995594cc5c2d302c11b34088e5e5174fb2
Bug 1277562 - Part 10: Cancel background tier2 compilation correctly. r=luke

diff --git a/js/src/vm/HelperThreads.cpp b/js/src/vm/HelperThreads.cpp
--- a/js/src/vm/HelperThreads.cpp
+++ b/js/src/vm/HelperThreads.cpp
@@ -111,17 +111,58 @@ js::StartOffThreadWasmTier2Generator(was
 
     HelperThreadState().notifyOne(GlobalHelperThreadState::PRODUCER, lock);
     return true;
 }
 
 void
 js::CancelOffThreadWasmTier2Generator()
 {
-    // TODO: Implement this
+    AutoLockHelperThreadState lock;
+
+    if (!HelperThreadState().threads)
+        return;
+
+    // Remove pending tasks from the tier2 generator worklist and cancel and
+    // delete them.
+    {
+        wasm::Tier2GeneratorTaskPtrVector& worklist =
+            HelperThreadState().wasmTier2GeneratorWorklist(lock);
+        for (size_t i = 0; i < worklist.length(); i++) {
+            wasm::Tier2GeneratorTask* task = worklist[i];
+            HelperThreadState().remove(worklist, &i);
+            CancelTier2GeneratorTask(task);
+            DeleteTier2GeneratorTask(task);
+        }
+    }
+
+    // There is at most one running Tier2Generator task and we assume that
+    // below.
+    static_assert(GlobalHelperThreadState::MaxTier2GeneratorTasks == 1,
+                  "code must be generalized");
+
+    // If there is a running Tier2 generator task, shut it down in a predictable
+    // way.  The task will be deleted by the normal deletion logic.
+    for (auto& helper : *HelperThreadState().threads) {
+        if (helper.wasmTier2GeneratorTask()) {
+            // Set a flag that causes compilation to shortcut itself.
+            CancelTier2GeneratorTask(helper.wasmTier2GeneratorTask());
+
+            // Wait for the generator task to finish.  This avoids a shutdown race where
+            // the shutdown code is trying to shut down helper threads and the ongoing
+            // tier2 compilation is trying to finish, which requires it to have access
+            // to helper threads.
+            uint32_t oldFinishedCount = HelperThreadState().wasmTier2GeneratorsFinished(lock);
+            while (HelperThreadState().wasmTier2GeneratorsFinished(lock) == oldFinishedCount)
+                HelperThreadState().wait(lock, GlobalHelperThreadState::CONSUMER);
+
+            // At most one of these tasks.
+            break;
+        }
+    }
 }
 
 bool
 js::StartOffThreadIonCompile(JSContext* cx, jit::IonBuilder* builder)
 {
     AutoLockHelperThreadState lock;
 
     if (!HelperThreadState().ionWorklist(lock).append(builder))
@@ -880,16 +921,17 @@ GlobalHelperThreadState::ensureInitializ
 }
 
 GlobalHelperThreadState::GlobalHelperThreadState()
  : cpuCount(0),
    threadCount(0),
    threads(nullptr),
    wasmCompilationInProgress_tier1(false),
    wasmCompilationInProgress_tier2(false),
+   wasmTier2GeneratorsFinished_(0),
    numWasmFailedJobs_tier1(0),
    numWasmFailedJobs_tier2(0),
    helperLock(mutexid::GlobalHelperThreadState)
 {
     cpuCount = GetCPUCount();
     threadCount = ThreadCountForCPUCount(cpuCount);
 
     MOZ_ASSERT(cpuCount > 0, "GetCPUCount() seems broken");
@@ -1826,23 +1868,25 @@ HelperThread::handleWasmTier2GeneratorWo
     bool success = false;
 
     wasm::Tier2GeneratorTask* task = wasmTier2GeneratorTask();
     {
         AutoUnlockHelperThreadState unlock(locked);
         success = wasm::GenerateTier2(task);
     }
 
-    // We silently ignore failures.  Such failures must be resource exhaustion,
-    // because all error checking was performed by the initial compilation.
+    // We silently ignore failures.  Such failures must be resource exhaustion
+    // or cancellation, because all error checking was performed by the initial
+    // compilation.
     mozilla::Unused << success;
 
     // During shutdown the main thread will wait for any ongoing (cancelled)
     // tier-2 generation to shut down normally.  To do so, it waits on the
     // CONSUMER condition for the count of finished generators to rise.
+    HelperThreadState().incWasmTier2GeneratorsFinished(locked);
     HelperThreadState().notifyAll(GlobalHelperThreadState::CONSUMER, locked);
 
     wasm::DeleteTier2GeneratorTask(task);
     currentTask.reset();
 }
 
 void
 HelperThread::handlePromiseHelperTaskWorkload(AutoLockHelperThreadState& locked)
diff --git a/js/src/vm/HelperThreads.h b/js/src/vm/HelperThreads.h
--- a/js/src/vm/HelperThreads.h
+++ b/js/src/vm/HelperThreads.h
@@ -143,16 +143,19 @@ class GlobalHelperThreadState
     SourceCompressionTaskVector compressionFinishedList_;
 
     // Runtimes which have sweeping / allocating work to do.
     GCHelperStateVector gcHelperWorklist_;
 
     // GC tasks needing to be done in parallel.
     GCParallelTaskVector gcParallelWorklist_;
 
+    // Count of finished Tier2Generator tasks.
+    uint32_t wasmTier2GeneratorsFinished_;
+
     ParseTask* removeFinishedParseTask(ParseTaskKind kind, void* token);
 
   public:
     size_t maxIonCompilationThreads() const;
     size_t maxUnpausedIonCompilationThreads() const;
     size_t maxWasmCompilationThreads() const;
     size_t maxWasmTier2GeneratorThreads() const;
     size_t maxParseThreads() const;
@@ -236,16 +239,24 @@ class GlobalHelperThreadState
             MOZ_CRASH();
         }
     }
 
     wasm::Tier2GeneratorTaskPtrVector& wasmTier2GeneratorWorklist(const AutoLockHelperThreadState&) {
         return wasmTier2GeneratorWorklist_;
     }
 
+    void incWasmTier2GeneratorsFinished(const AutoLockHelperThreadState&) {
+        wasmTier2GeneratorsFinished_++;
+    }
+
+    uint32_t wasmTier2GeneratorsFinished(const AutoLockHelperThreadState&) const {
+        return wasmTier2GeneratorsFinished_;
+    }
+
     PromiseHelperTaskVector& promiseHelperTasks(const AutoLockHelperThreadState&) {
         return promiseHelperTasks_;
     }
 
     ParseTaskVector& parseWorklist(const AutoLockHelperThreadState&) {
         return parseWorklist_;
     }
     ParseTaskVector& parseFinishedList(const AutoLockHelperThreadState&) {
@@ -588,16 +599,19 @@ namespace wasm {
 // Performs MIR optimization and LIR generation on one or several functions.
 MOZ_MUST_USE bool
 CompileFunction(CompileTask* task, UniqueChars* error);
 
 MOZ_MUST_USE bool
 GenerateTier2(Tier2GeneratorTask* task);
 
 void
+CancelTier2GeneratorTask(Tier2GeneratorTask* task);
+
+void
 DeleteTier2GeneratorTask(Tier2GeneratorTask* task);
 }
 
 /*
  * If helper threads are available, call execute() then dispatchResolve() on the
  * given task in a helper thread. If no helper threads are available, the given
  * task is executed and resolved synchronously.
  */
diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -1725,17 +1725,17 @@ class MOZ_STACK_CLASS ModuleValidator
         functions_(cx),
         funcPtrTables_(cx),
         globalMap_(cx),
         sigMap_(cx),
         importMap_(cx),
         arrayViews_(cx),
         atomicsPresent_(false),
         simdPresent_(false),
-        mg_(nullptr),
+        mg_(nullptr, nullptr),
         errorString_(nullptr),
         errorOffset_(UINT32_MAX),
         errorOverRecursed_(false)
     {}
 
     ~ModuleValidator() {
         if (errorString_) {
             MOZ_ASSERT(errorOffset_ != UINT32_MAX);
diff --git a/js/src/wasm/WasmCompile.cpp b/js/src/wasm/WasmCompile.cpp
--- a/js/src/wasm/WasmCompile.cpp
+++ b/js/src/wasm/WasmCompile.cpp
@@ -177,25 +177,30 @@ wasm::GetTier(const CompileArgs& args, C
     }
 }
 
 namespace js {
 namespace wasm {
 
 struct Tier2GeneratorTask
 {
-    // The module that wants the results of the compilation
+    // The module that wants the results of the compilation.
     SharedModule            module;
 
-    // The arguments for the compilation
+    // The arguments for the compilation.
     SharedCompileArgs       compileArgs;
 
+    // A flag that is set by the cancellation code and checked by any running
+    // module generator to short-circuit compilation.
+    mozilla::Atomic<bool>   cancelled;
+
     Tier2GeneratorTask(Module& module, const CompileArgs& compileArgs)
       : module(&module),
-        compileArgs(&compileArgs)
+        compileArgs(&compileArgs),
+        cancelled(false)
     {}
 };
 
 }
 }
 
 static bool
 Compile(ModuleGenerator& mg, const ShareableBytes& bytecode, const CompileArgs& args,
@@ -222,17 +227,17 @@ Compile(ModuleGenerator& mg, const Share
 }
 
 
 SharedModule
 wasm::Compile(const ShareableBytes& bytecode, const CompileArgs& args, UniqueChars* error)
 {
     MOZ_RELEASE_ASSERT(wasm::HaveSignalHandlers());
 
-    ModuleGenerator mg(error);
+    ModuleGenerator mg(error, nullptr);
 
     CompileMode mode = GetInitialCompileMode(args);
     if (!::Compile(mg, bytecode, args, error, mode))
         return nullptr;
 
     MOZ_ASSERT(!*error, "unreported error in decoding");
 
     SharedModule module = mg.finishModule(bytecode);
@@ -259,24 +264,39 @@ wasm::Compile(const ShareableBytes& byte
     return module;
 }
 
 // This runs on a helper thread.
 bool
 wasm::GenerateTier2(Tier2GeneratorTask* task)
 {
     UniqueChars     error;
-    ModuleGenerator mg(&error);
+    ModuleGenerator mg(&error, &task->cancelled);
 
     bool res =
         ::Compile(mg, task->module->bytecode(), *task->compileArgs, &error, CompileMode::Tier2) &&
         mg.finishTier2(task->module->bytecode(), task->module);
 
+    // If the task was cancelled then res will be false.
     task->module->unblockOnTier2GeneratorFinished(res ? CompileMode::Tier2 : CompileMode::Once);
 
     return res;
 }
 
+// This runs on the main thread.  The task will be deleted in the HelperThread
+// system.
+void
+wasm::CancelTier2GeneratorTask(Tier2GeneratorTask* task)
+{
+    task->cancelled = true;
+
+    // GenerateTier2 will also unblock and set the mode to Once, after the
+    // compilation fails, if the compilation gets to run at all.  Do it here in
+    // any case - there's no reason to wait, and we won't be depending on
+    // anything else happening.
+    task->module->unblockOnTier2GeneratorFinished(CompileMode::Once);
+}
+
 void
 wasm::DeleteTier2GeneratorTask(Tier2GeneratorTask* task)
 {
     js_delete(task);
 }
diff --git a/js/src/wasm/WasmGenerator.cpp b/js/src/wasm/WasmGenerator.cpp
--- a/js/src/wasm/WasmGenerator.cpp
+++ b/js/src/wasm/WasmGenerator.cpp
@@ -40,20 +40,21 @@ using mozilla::MakeEnumeratedRange;
 
 // ****************************************************************************
 // ModuleGenerator
 
 static const unsigned GENERATOR_LIFO_DEFAULT_CHUNK_SIZE = 4 * 1024;
 static const unsigned COMPILATION_LIFO_DEFAULT_CHUNK_SIZE = 64 * 1024;
 static const uint32_t BAD_CODE_RANGE = UINT32_MAX;
 
-ModuleGenerator::ModuleGenerator(UniqueChars* error)
+ModuleGenerator::ModuleGenerator(UniqueChars* error, mozilla::Atomic<bool>* cancelled)
   : tier_(Tier(-1)),
     compileMode_(CompileMode::Once),
     error_(error),
+    cancelled_(cancelled),
     linkDataTier_(nullptr),
     metadataTier_(nullptr),
     numSigs_(0),
     numTables_(0),
     lifo_(GENERATOR_LIFO_DEFAULT_CHUNK_SIZE),
     masmAlloc_(&lifo_),
     masm_(MacroAssembler::WasmToken(), masmAlloc_),
     lastPatchedCallsite_(0),
@@ -949,16 +950,19 @@ ModuleGenerator::startFuncDef(uint32_t l
     return true;
 }
 
 bool
 ModuleGenerator::launchBatchCompile()
 {
     MOZ_ASSERT(currentTask_);
 
+    if (cancelled_ && *cancelled_)
+        return false;
+
     currentTask_->setDebugEnabled(metadata_->debugEnabled);
 
     size_t numBatchedFuncs = currentTask_->units().length();
     MOZ_ASSERT(numBatchedFuncs);
 
     if (parallel_) {
         if (!StartOffThreadWasmCompile(currentTask_, compileMode_))
             return false;
@@ -1259,16 +1263,19 @@ ModuleGenerator::finishModule(const Shar
                                        bytecode));
 }
 
 bool
 ModuleGenerator::finishTier2(const ShareableBytes& bytecode, SharedModule module)
 {
     MOZ_ASSERT(compileMode_ == CompileMode::Tier2);
 
+    if (cancelled_ && *cancelled_)
+        return false;
+
     if (!finishCommon(bytecode))
         return false;
 
     UniqueConstCodeSegment codeSegment = CodeSegment::create(tier_,
                                                              masm_,
                                                              bytecode,
                                                              *linkDataTier_,
                                                              *metadata_);
diff --git a/js/src/wasm/WasmGenerator.h b/js/src/wasm/WasmGenerator.h
--- a/js/src/wasm/WasmGenerator.h
+++ b/js/src/wasm/WasmGenerator.h
@@ -215,16 +215,17 @@ class MOZ_STACK_CLASS ModuleGenerator
     typedef Vector<CompileTask, 0, SystemAllocPolicy> CompileTaskVector;
     typedef Vector<CompileTask*, 0, SystemAllocPolicy> CompileTaskPtrVector;
     typedef EnumeratedArray<Trap, Trap::Limit, CallableOffsets> TrapExitOffsetArray;
 
     // Constant parameters
     Tier                            tier_;
     CompileMode                     compileMode_;
     UniqueChars*                    error_;
+    mozilla::Atomic<bool>*          cancelled_;
 
     // Data that is moved into the result of finish()
     Assumptions                     assumptions_;
     LinkDataTier*                   linkDataTier_; // Owned by linkData_
     LinkData                        linkData_;
     MetadataTier*                   metadataTier_; // Owned by metadata_
     MutableMetadata                 metadata_;
 
@@ -277,17 +278,17 @@ class MOZ_STACK_CLASS ModuleGenerator
     MOZ_MUST_USE bool allocateGlobal(GlobalDesc* global);
 
     MOZ_MUST_USE bool launchBatchCompile();
 
     MOZ_MUST_USE bool initAsmJS(Metadata* asmJSMetadata);
     MOZ_MUST_USE bool initWasm(const CompileArgs& args);
 
   public:
-    explicit ModuleGenerator(UniqueChars* error);
+    explicit ModuleGenerator(UniqueChars* error, mozilla::Atomic<bool>* cancelled);
     ~ModuleGenerator();
 
     MOZ_MUST_USE bool init(UniqueModuleEnvironment env, const CompileArgs& args,
                            CompileMode compileMode = CompileMode::Once,
                            Metadata* maybeAsmJSMetadata = nullptr);
 
     const ModuleEnvironment& env() const { return *env_; }
     ModuleEnvironment& mutableEnv();
