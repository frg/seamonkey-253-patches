# HG changeset patch
# User cku <cku@mozilla.com>
# Date 1504153299 -28800
#      Thu Aug 31 12:21:39 2017 +0800
# Node ID aa111604eaa34be601b91b32aa077939b4252c28
# Parent  b18cad4e3274cb2a6b92eb95779f099b79c4fd5d
Bug 1395439 - Clean out useless flags to reuse cache FillGeometry in nsSVGUtils::GetBBox. r=longsonr+218550

In nsSVGUtils::GetBBox, we cache fill-gemorty's bbox in ObjectBoundingBoxProperty
of the given frame[1] so that we can just return the cache value without
recomputing bbox. Some new added flags, such as eUseFrameBoundsForOuterSVG,
prevent storing and reusing this cache value. This patch is trying to make this
cache mechanism work again.

[1]
https://hg.mozilla.org/mozilla-central/file/ef585ac7c476/layout/svg/nsSVGUtils.cpp#l1119

MozReview-Commit-ID: 8vWDzFi8qCw

diff --git a/layout/svg/nsSVGUtils.cpp b/layout/svg/nsSVGUtils.cpp
--- a/layout/svg/nsSVGUtils.cpp
+++ b/layout/svg/nsSVGUtils.cpp
@@ -1108,33 +1108,40 @@ nsSVGUtils::GetBBox(nsIFrame* aFrame, ui
   MOZ_ASSERT(svg);
 
   nsIContent* content = aFrame->GetContent();
   if (content->IsSVGElement() &&
       !static_cast<const nsSVGElement*>(content)->HasValidDimensions()) {
     return gfxRect();
   }
 
+  // Clean out flags which have no effects on returning bbox from now, so that
+  // we can cache and reuse ObjectBoundingBoxProperty() in the code below.
+  aFlags &= ~eIncludeOnlyCurrentFrameForNonSVGElement;
+  aFlags &= ~eUseFrameBoundsForOuterSVG;
+  if (!aFrame->IsSVGUseFrame()) {
+    aFlags &= ~eUseUserSpaceOfUseElement;
+  }
+
   if (aFlags == eBBoxIncludeFillGeometry &&
       // We only cache bbox in element's own user space
       !aToBoundsSpace) {
     gfxRect* prop = aFrame->GetProperty(ObjectBoundingBoxProperty());
     if (prop) {
       return *prop;
     }
   }
 
   gfxMatrix matrix;
   if (aToBoundsSpace) {
     matrix = *aToBoundsSpace;
   }
 
   if (aFrame->IsSVGForeignObjectFrame() ||
-      (aFrame->IsSVGUseFrame() &&
-       (aFlags & nsSVGUtils::eUseUserSpaceOfUseElement))) {
+      aFlags & nsSVGUtils::eUseUserSpaceOfUseElement) {
     // The spec says getBBox "Returns the tight bounding box in *current user
     // space*". So we should really be doing this for all elements, but that
     // needs investigation to check that we won't break too much content.
     // NOTE: When changing this to apply to other frame types, make sure to
     // also update nsSVGUtils::FrameSpaceInCSSPxToUserSpaceOffset.
     MOZ_ASSERT(content->IsSVGElement(), "bad cast");
     nsSVGElement *element = static_cast<nsSVGElement*>(content);
     matrix = element->PrependLocalTransformsTo(matrix, eChildToUserSpace);
