# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1502271089 -7200
# Node ID cd1d769cabe8a2c3a54330f9c13f4c7f66a3b269
# Parent  60eacfb4bc96529ef311bd26ff95948fda50d1e3
Bug 1387968 - Part 2: Optimize array accesses and allocations in RegExpGetSubstitution. r=till

diff --git a/js/src/builtin/RegExp.cpp b/js/src/builtin/RegExp.cpp
--- a/js/src/builtin/RegExp.cpp
+++ b/js/src/builtin/RegExp.cpp
@@ -17,16 +17,17 @@
 #include "vm/RegExpStatics.h"
 #include "vm/SelfHosting.h"
 #include "vm/StringBuffer.h"
 #include "vm/Unicode.h"
 
 #include "jsobjinlines.h"
 
 #include "vm/NativeObject-inl.h"
+#include "vm/UnboxedObject-inl.h"
 
 using namespace js;
 using namespace js::unicode;
 
 using mozilla::ArrayLength;
 using mozilla::CheckedInt;
 using mozilla::Maybe;
 
@@ -1198,43 +1199,45 @@ js::regexp_test_no_statics(JSContext* cx
 
     size_t ignored = 0;
     RegExpRunStatus status = ExecuteRegExp(cx, regexp, string, 0,
                                            nullptr, &ignored, DontUpdateRegExpStatics);
     args.rval().setBoolean(status == RegExpRunStatus_Success);
     return status != RegExpRunStatus_Error;
 }
 
+using CapturesVector = GCVector<Value, 4>;
+
 static void
 GetParen(JSLinearString* matched, const JS::Value& capture, JSSubString* out)
 {
     if (capture.isUndefined()) {
         out->initEmpty(matched);
         return;
     }
     JSLinearString& captureLinear = capture.toString()->asLinear();
     out->init(&captureLinear, 0, captureLinear.length());
 }
 
 template <typename CharT>
 static bool
 InterpretDollar(JSLinearString* matched, JSLinearString* string, size_t position, size_t tailPos,
-                MutableHandle<GCVector<Value>> captures, JSLinearString* replacement,
+                Handle<CapturesVector> captures, JSLinearString* replacement,
                 const CharT* replacementBegin, const CharT* currentDollar,
                 const CharT* replacementEnd,
                 JSSubString* out, size_t* skip)
 {
     MOZ_ASSERT(*currentDollar == '$');
 
     /* If there is only a dollar, bail now. */
     if (currentDollar + 1 >= replacementEnd)
         return false;
 
     /* ES 2016 draft Mar 25, 2016 Table 46. */
-    char16_t c = currentDollar[1];
+    CharT c = currentDollar[1];
     if (JS7_ISDEC(c)) {
         /* $n, $nn */
         unsigned num = JS7_UNDEC(c);
         if (num > captures.length()) {
             // The result is implementation-defined, do not substitute.
             return false;
         }
 
@@ -1291,17 +1294,17 @@ InterpretDollar(JSLinearString* matched,
         break;
     }
     return true;
 }
 
 template <typename CharT>
 static bool
 FindReplaceLengthString(JSContext* cx, HandleLinearString matched, HandleLinearString string,
-                        size_t position, size_t tailPos, MutableHandle<GCVector<Value>> captures,
+                        size_t position, size_t tailPos, Handle<CapturesVector> captures,
                         HandleLinearString replacement, size_t firstDollarIndex, size_t* sizep)
 {
     CheckedInt<uint32_t> replen = replacement->length();
 
     JS::AutoCheckCannotGC nogc;
     MOZ_ASSERT(firstDollarIndex < replacement->length());
     const CharT* replacementBegin = replacement->chars<CharT>(nogc);
     const CharT* currentDollar = replacementBegin + firstDollarIndex;
@@ -1330,17 +1333,17 @@ FindReplaceLengthString(JSContext* cx, H
     }
 
     *sizep = replen.value();
     return true;
 }
 
 static bool
 FindReplaceLength(JSContext* cx, HandleLinearString matched, HandleLinearString string,
-                  size_t position, size_t tailPos, MutableHandle<GCVector<Value>> captures,
+                  size_t position, size_t tailPos, Handle<CapturesVector> captures,
                   HandleLinearString replacement, size_t firstDollarIndex, size_t* sizep)
 {
     return replacement->hasLatin1Chars()
            ? FindReplaceLengthString<Latin1Char>(cx, matched, string, position, tailPos, captures,
                                                  replacement, firstDollarIndex, sizep)
            : FindReplaceLengthString<char16_t>(cx, matched, string, position, tailPos, captures,
                                                replacement, firstDollarIndex, sizep);
 }
@@ -1348,17 +1351,17 @@ FindReplaceLength(JSContext* cx, HandleL
 /*
  * Precondition: |sb| already has necessary growth space reserved (as
  * derived from FindReplaceLength), and has been inflated to TwoByte if
  * necessary.
  */
 template <typename CharT>
 static void
 DoReplace(HandleLinearString matched, HandleLinearString string,
-          size_t position, size_t tailPos, MutableHandle<GCVector<Value>> captures,
+          size_t position, size_t tailPos, Handle<CapturesVector> captures,
           HandleLinearString replacement, size_t firstDollarIndex, StringBuffer &sb)
 {
     JS::AutoCheckCannotGC nogc;
     const CharT* replacementBegin = replacement->chars<CharT>(nogc);
     const CharT* currentChar = replacementBegin;
 
     MOZ_ASSERT(firstDollarIndex < replacement->length());
     const CharT* currentDollar = replacementBegin + firstDollarIndex;
@@ -1383,86 +1386,80 @@ DoReplace(HandleLinearString matched, Ha
 
         currentDollar = js_strchr_limit(currentDollar, '$', replacementEnd);
     } while (currentDollar);
     sb.infallibleAppend(currentChar, replacement->length() - (currentChar - replacementBegin));
 }
 
 static bool
 NeedTwoBytes(HandleLinearString string, HandleLinearString replacement,
-             HandleLinearString matched, Handle<GCVector<Value>> captures)
+             HandleLinearString matched, Handle<CapturesVector> captures)
 {
     if (string->hasTwoByteChars())
         return true;
     if (replacement->hasTwoByteChars())
         return true;
     if (matched->hasTwoByteChars())
         return true;
 
     for (size_t i = 0, len = captures.length(); i < len; i++) {
-        Value capture = captures[i];
+        const Value& capture = captures[i];
         if (capture.isUndefined())
             continue;
         if (capture.toString()->hasTwoByteChars())
             return true;
     }
 
     return false;
 }
 
 /* ES 2016 draft Mar 25, 2016 21.1.3.14.1. */
 bool
 js::RegExpGetSubstitution(JSContext* cx, HandleObject matchResult, HandleLinearString string,
                           size_t position, HandleLinearString replacement,
                           size_t firstDollarIndex, MutableHandleValue rval)
 {
     MOZ_ASSERT(firstDollarIndex < replacement->length());
+    MOZ_ASSERT(matchResult->is<ArrayObject>() || matchResult->is<UnboxedArrayObject>());
 
     // Step 1 (skipped).
 
     // Step 10 (reordered).
-    uint32_t matchResultLength;
-    if (!GetLengthProperty(cx, matchResult, &matchResultLength))
-        return false;
+    uint32_t matchResultLength = GetAnyBoxedOrUnboxedArrayLength(matchResult);
     MOZ_ASSERT(matchResultLength > 0);
+    MOZ_ASSERT(matchResultLength == GetAnyBoxedOrUnboxedInitializedLength(matchResult));
 
-    RootedValue matchedValue(cx);
-    if (!GetElement(cx, matchResult, matchResult, 0, &matchedValue))
-        return false;
-
+    const Value& matchedValue = GetAnyBoxedOrUnboxedDenseElement(matchResult, 0);
     RootedLinearString matched(cx, matchedValue.toString()->ensureLinear(cx));
     if (!matched)
         return false;
 
     // Step 2.
     size_t matchLength = matched->length();
 
     // Steps 3-5 (skipped).
 
     // Step 6.
     MOZ_ASSERT(position <= string->length());
 
     uint32_t nCaptures = matchResultLength - 1;
-    Rooted<GCVector<Value>> captures(cx, GCVector<Value>(cx));
+    Rooted<CapturesVector> captures(cx, CapturesVector(cx));
     if (!captures.reserve(nCaptures))
         return false;
 
     // Step 7.
-    RootedValue capture(cx);
     for (uint32_t i = 1; i <= nCaptures; i++) {
-        if (!GetElement(cx, matchResult, matchResult, i, &capture))
-            return false;
+        const Value& capture = GetAnyBoxedOrUnboxedDenseElement(matchResult, i);
 
         if (capture.isUndefined()) {
             captures.infallibleAppend(capture);
             continue;
         }
 
-        MOZ_ASSERT(capture.isString());
-        RootedLinearString captureLinear(cx, capture.toString()->ensureLinear(cx));
+        JSLinearString* captureLinear = capture.toString()->ensureLinear(cx);
         if (!captureLinear)
             return false;
         captures.infallibleAppend(StringValue(captureLinear));
     }
 
     // Step 8 (skipped).
 
     // Step 9.
@@ -1472,36 +1469,36 @@ js::RegExpGetSubstitution(JSContext* cx,
     if (!checkedTailPos.isValid()) {
         ReportAllocationOverflow(cx);
         return false;
     }
     uint32_t tailPos = checkedTailPos.value();
 
     // Step 11.
     size_t reserveLength;
-    if (!FindReplaceLength(cx, matched, string, position, tailPos, &captures, replacement,
+    if (!FindReplaceLength(cx, matched, string, position, tailPos, captures, replacement,
                            firstDollarIndex, &reserveLength))
     {
         return false;
     }
 
     StringBuffer result(cx);
     if (NeedTwoBytes(string, replacement, matched, captures)) {
         if (!result.ensureTwoByteChars())
             return false;
     }
 
     if (!result.reserve(reserveLength))
         return false;
 
     if (replacement->hasLatin1Chars()) {
-        DoReplace<Latin1Char>(matched, string, position, tailPos, &captures,
+        DoReplace<Latin1Char>(matched, string, position, tailPos, captures,
                               replacement, firstDollarIndex, result);
     } else {
-        DoReplace<char16_t>(matched, string, position, tailPos, &captures,
+        DoReplace<char16_t>(matched, string, position, tailPos, captures,
                             replacement, firstDollarIndex, result);
     }
 
     // Step 12.
     JSString* resultString = result.finishString();
     if (!resultString)
         return false;
 
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -1531,41 +1531,33 @@ intrinsic_RegExpCreate(JSContext* cx, un
 
 static bool
 intrinsic_RegExpGetSubstitution(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     MOZ_ASSERT(args.length() == 5);
 
     RootedObject matchResult(cx, &args[0].toObject());
-#ifdef DEBUG
-    bool isArray = false;
-    MOZ_ALWAYS_TRUE(IsArray(cx, matchResult, &isArray));
-    MOZ_ASSERT(isArray);
-#endif
-
-    RootedString string(cx, args[1].toString());
+
+    RootedLinearString string(cx, args[1].toString()->ensureLinear(cx));
+    if (!string)
+        return false;
 
     int32_t position = int32_t(args[2].toNumber());
     MOZ_ASSERT(position >= 0);
 
-    RootedString replacement(cx, args[3].toString());
+    RootedLinearString replacement(cx, args[3].toString()->ensureLinear(cx));
+    if (!replacement)
+        return false;
 
     int32_t firstDollarIndex = int32_t(args[4].toNumber());
     MOZ_ASSERT(firstDollarIndex >= 0);
 
-    RootedLinearString stringLinear(cx, string->ensureLinear(cx));
-    if (!stringLinear)
-        return false;
-    RootedLinearString replacementLinear(cx, replacement->ensureLinear(cx));
-    if (!replacementLinear)
-        return false;
-
-    return RegExpGetSubstitution(cx, matchResult, stringLinear, size_t(position),
-                                 replacementLinear, size_t(firstDollarIndex), args.rval());
+    return RegExpGetSubstitution(cx, matchResult, string, size_t(position), replacement,
+                                 size_t(firstDollarIndex), args.rval());
 }
 
 static bool
 intrinsic_StringReplaceString(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     MOZ_ASSERT(args.length() == 3);
 
