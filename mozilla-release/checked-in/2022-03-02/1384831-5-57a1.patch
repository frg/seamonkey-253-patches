# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1501095293 -28800
# Node ID d9c52bcac7974e2e8dfdd5bf0be0c1cd7e422a99
# Parent  057e2024020484315fa4b4a33b2e2761f7ef2bae
Bug 1384831. P5 - remove unused arguments from HLSDecoder::Load(). r=gerald

MozReview-Commit-ID: JsULuDcMiNa

diff --git a/dom/html/HTMLMediaElement.cpp b/dom/html/HTMLMediaElement.cpp
--- a/dom/html/HTMLMediaElement.cpp
+++ b/dom/html/HTMLMediaElement.cpp
@@ -4814,44 +4814,44 @@ nsresult HTMLMediaElement::InitializeDec
   });
 
   Maybe<MediaContainerType> containerType = MakeMediaContainerType(mimeType);
   if (!containerType) {
     reportCanPlay(false);
     return NS_ERROR_FAILURE;
   }
 
-  bool isPrivateBrowsing = NodePrincipal()->GetPrivateBrowsingId() > 0;
   MediaDecoderInit decoderInit(this,
                                mAudioChannel,
                                mMuted ? 0.0 : mVolume,
                                mPreservesPitch,
                                mPlaybackRate,
                                mPreloadAction ==
                                  HTMLMediaElement::PRELOAD_METADATA,
                                mHasSuspendTaint,
                                HasAttr(kNameSpaceID_None, nsGkAtoms::loop),
                                *containerType);
 
 #ifdef MOZ_ANDROID_HLS_SUPPORT
   if (HLSDecoder::IsSupportedType(*containerType)) {
     RefPtr<HLSDecoder> decoder = new HLSDecoder(decoderInit);
     reportCanPlay(true);
-    return SetupDecoder(decoder.get(), aChannel, isPrivateBrowsing, aListener);
+    return SetupDecoder(decoder.get(), aChannel);
   }
 #endif
 
   RefPtr<ChannelMediaDecoder> decoder =
     DecoderTraits::CreateDecoder(decoderInit, &diagnostics);
   if (!decoder) {
     reportCanPlay(false);
     return NS_ERROR_FAILURE;
   }
 
   reportCanPlay(true);
+  bool isPrivateBrowsing = NodePrincipal()->GetPrivateBrowsingId() > 0;
   return SetupDecoder(decoder.get(), aChannel, isPrivateBrowsing, aListener);
 }
 
 nsresult
 HTMLMediaElement::FinishDecoderSetup(MediaDecoder* aDecoder)
 {
   ChangeNetworkState(nsIDOMHTMLMediaElement::NETWORK_LOADING);
 
diff --git a/dom/media/hls/HLSDecoder.cpp b/dom/media/hls/HLSDecoder.cpp
--- a/dom/media/hls/HLSDecoder.cpp
+++ b/dom/media/hls/HLSDecoder.cpp
@@ -59,19 +59,17 @@ HLSDecoder::IsEnabled()
 bool
 HLSDecoder::IsSupportedType(const MediaContainerType& aContainerType)
 {
   return IsEnabled() &&
          DecoderTraits::IsHttpLiveStreamingType(aContainerType);
 }
 
 nsresult
-HLSDecoder::Load(nsIChannel* aChannel,
-                 bool aIsPrivateBrowsing,
-                 nsIStreamListener**)
+HLSDecoder::Load(nsIChannel* aChannel)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(!mResource);
 
   nsCOMPtr<nsIURI> uri;
   nsresult rv = NS_GetFinalChannelURI(aChannel, getter_AddRefs(uri));
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
diff --git a/dom/media/hls/HLSDecoder.h b/dom/media/hls/HLSDecoder.h
--- a/dom/media/hls/HLSDecoder.h
+++ b/dom/media/hls/HLSDecoder.h
@@ -27,19 +27,17 @@ public:
   // Returns true if the HLS backend is pref'ed on.
   static bool IsEnabled();
 
   // Returns true if aContainerType is an HLS type that we think we can render
   // with the a platform decoder backend.
   // If provided, codecs are checked for support.
   static bool IsSupportedType(const MediaContainerType& aContainerType);
 
-  nsresult Load(nsIChannel* aChannel,
-                bool aIsPrivateBrowsing,
-                nsIStreamListener**);
+  nsresult Load(nsIChannel* aChannel);
 
   nsresult Play() override;
 
   void Pause() override;
 };
 
 } // namespace mozilla
 
