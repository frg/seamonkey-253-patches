# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1504001913 18000
#      Tue Aug 29 05:18:33 2017 -0500
# Node ID 777eb5f15d2024b328b7f34a17a966f85f8f7590
# Parent  c01a936e38103a805367f0fd4e933801a2e480d8
servo: Merge #18292 - style: Prevent Animate to generate NaN float values (from emilio:animate-nan); r=nox

This should fix the assertions in https://bugzilla.mozilla.org/show_bug.cgi?id=1394558.

Source-Repo: https://github.com/servo/servo
Source-Revision: 1c4d0d2d5d3e34e62ad3585b96632f354dbbe208

diff --git a/servo/components/style/values/animated/mod.rs b/servo/components/style/values/animated/mod.rs
--- a/servo/components/style/values/animated/mod.rs
+++ b/servo/components/style/values/animated/mod.rs
@@ -120,26 +120,33 @@ impl Animate for i32 {
         Ok(((*self as f64).animate(&(*other as f64), procedure)? + 0.5).floor() as i32)
     }
 }
 
 /// https://drafts.csswg.org/css-transitions/#animtype-number
 impl Animate for f32 {
     #[inline]
     fn animate(&self, other: &Self, procedure: Procedure) -> Result<Self, ()> {
-        Ok((*self as f64).animate(&(*other as f64), procedure)? as f32)
+        use std::f32;
+
+        let ret = (*self as f64).animate(&(*other as f64), procedure)?;
+        Ok(ret.min(f32::MAX as f64).max(f32::MIN as f64) as f32)
     }
 }
 
 /// https://drafts.csswg.org/css-transitions/#animtype-number
 impl Animate for f64 {
     #[inline]
     fn animate(&self, other: &Self, procedure: Procedure) -> Result<Self, ()> {
+        use std::f64;
+
         let (self_weight, other_weight) = procedure.weights();
-        Ok(*self * self_weight + *other * other_weight)
+
+        let ret = *self * self_weight + *other * other_weight;
+        Ok(ret.min(f64::MAX).max(f64::MIN))
     }
 }
 
 impl<T> Animate for Option<T>
 where
     T: Animate,
 {
     #[inline]
