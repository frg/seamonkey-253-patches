# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1507124570 18000
#      Wed Oct 04 08:42:50 2017 -0500
# Node ID cfde890019df4823bd01567331dcc31506ff642f
# Parent  a0eeaeef88bf4801e01a7a2fb923c2ef337b0826
Bug 1405760 - Remove the JSOp argument from ParseHandler::newDeclarationList. r=Waldo

This changes these ParseNodes to have pn_op=JSOP_NOP, but the field is
already ignored downstream for these node kinds.

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -6211,16 +6211,17 @@ BytecodeEmitter::emitTemplateString(Pars
 
     return true;
 }
 
 bool
 BytecodeEmitter::emitDeclarationList(ParseNode* declList)
 {
     MOZ_ASSERT(declList->isArity(PN_LIST));
+    MOZ_ASSERT(declList->isOp(JSOP_NOP));
 
     ParseNode* next;
     for (ParseNode* decl = declList->pn_head; decl; decl = next) {
         if (!updateSourceCoordNotes(decl->pn_pos.begin))
             return false;
         next = decl->pn_next;
 
         if (decl->isKind(PNK_ASSIGN)) {
diff --git a/js/src/frontend/FullParseHandler.h b/js/src/frontend/FullParseHandler.h
--- a/js/src/frontend/FullParseHandler.h
+++ b/js/src/frontend/FullParseHandler.h
@@ -814,19 +814,19 @@ class FullParseHandler
     ParseNode* newList(ParseNodeKind kind, const T& begin, JSOp op = JSOP_NOP) = delete;
 
   public:
     ParseNode* newList(ParseNodeKind kind, ParseNode* kid, JSOp op = JSOP_NOP) {
         MOZ_ASSERT(!isDeclarationKind(kind));
         return new_<ListNode>(kind, op, kid);
     }
 
-    ParseNode* newDeclarationList(ParseNodeKind kind, const TokenPos& pos, JSOp op) {
+    ParseNode* newDeclarationList(ParseNodeKind kind, const TokenPos& pos) {
         MOZ_ASSERT(isDeclarationKind(kind));
-        return new_<ListNode>(kind, op, pos);
+        return new_<ListNode>(kind, JSOP_NOP, pos);
     }
 
     bool isDeclarationList(ParseNode* node) {
         return isDeclarationKind(node->getKind());
     }
 
     ParseNode* singleBindingFromDeclaration(ParseNode* decl) {
         MOZ_ASSERT(isDeclarationList(decl));
diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -4946,36 +4946,32 @@ template <class ParseHandler, typename C
 typename ParseHandler::Node
 Parser<ParseHandler, CharT>::declarationList(YieldHandling yieldHandling,
                                              ParseNodeKind kind,
                                              ParseNodeKind* forHeadKind /* = nullptr */,
                                              Node* forInOrOfExpression /* = nullptr */)
 {
     MOZ_ASSERT(kind == PNK_VAR || kind == PNK_LET || kind == PNK_CONST);
 
-    JSOp op;
     DeclarationKind declKind;
     switch (kind) {
       case PNK_VAR:
-        op = JSOP_DEFVAR;
         declKind = DeclarationKind::Var;
         break;
       case PNK_CONST:
-        op = JSOP_DEFCONST;
         declKind = DeclarationKind::Const;
         break;
       case PNK_LET:
-        op = JSOP_DEFLET;
         declKind = DeclarationKind::Let;
         break;
       default:
         MOZ_CRASH("Unknown declaration kind");
     }
 
-    Node decl = handler.newDeclarationList(kind, pos(), op);
+    Node decl = handler.newDeclarationList(kind, pos());
     if (!decl)
         return null();
 
     bool moreDeclarations;
     bool initialDeclaration = true;
     do {
         MOZ_ASSERT_IF(!initialDeclaration && forHeadKind,
                       *forHeadKind == PNK_FORHEAD);
diff --git a/js/src/frontend/SyntaxParseHandler.h b/js/src/frontend/SyntaxParseHandler.h
--- a/js/src/frontend/SyntaxParseHandler.h
+++ b/js/src/frontend/SyntaxParseHandler.h
@@ -425,17 +425,17 @@ class SyntaxParseHandler
     template<typename T>
     Node newList(ParseNodeKind kind, const T& begin, JSOp op = JSOP_NOP) = delete;
 
   public:
     Node newList(ParseNodeKind kind, Node kid, JSOp op = JSOP_NOP) {
         return newList(kind, TokenPos(), op);
     }
 
-    Node newDeclarationList(ParseNodeKind kind, const TokenPos& pos, JSOp op = JSOP_NOP) {
+    Node newDeclarationList(ParseNodeKind kind, const TokenPos& pos) {
         if (kind == PNK_VAR)
             return NodeVarDeclaration;
         MOZ_ASSERT(kind == PNK_LET || kind == PNK_CONST);
         return NodeLexicalDeclaration;
     }
 
     bool isDeclarationList(Node node) {
         return node == NodeVarDeclaration || node == NodeLexicalDeclaration;
