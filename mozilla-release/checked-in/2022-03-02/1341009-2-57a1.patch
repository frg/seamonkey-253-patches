# HG changeset patch
# User Ting-Yu Lin <tlin@mozilla.com>
# Date 1505288871 -28800
#      Wed Sep 13 15:47:51 2017 +0800
# Node ID 25757b404bb6823183dd8f67302752b4076875c3
# Parent  a4e21ea29716c6032dd63a15d789abc0816541c5
Bug 1341009 - Pass const reference instead of value for nsReflowStatus. r=dholbert

It's good to save a copy constructor call.

Also, make nsBlockFrame::SplitFloat() return void because the only
caller (BlockReflowInput::FlowAndPlaceFloat()) doesn't check its return
value, and (more importantly) because it only ever returns NS_OK.

MozReview-Commit-ID: 9LmDCXd7xGS

diff --git a/layout/generic/nsBlockFrame.cpp b/layout/generic/nsBlockFrame.cpp
--- a/layout/generic/nsBlockFrame.cpp
+++ b/layout/generic/nsBlockFrame.cpp
@@ -244,17 +244,17 @@ nsBlockFrame::LineReflowStatusToString(L
   }
   return "unknown";
 }
 
 #endif
 
 #ifdef REFLOW_STATUS_COVERAGE
 static void
-RecordReflowStatus(bool aChildIsBlock, nsReflowStatus aFrameReflowStatus)
+RecordReflowStatus(bool aChildIsBlock, const nsReflowStatus& aFrameReflowStatus)
 {
   static uint32_t record[2];
 
   // 0: child-is-block
   // 1: child-is-inline
   int index = 0;
   if (!aChildIsBlock) index |= 1;
 
@@ -4291,20 +4291,20 @@ nsBlockFrame::CreateContinuationFor(Bloc
     }
   }
 #ifdef DEBUG
   VerifyLines(false);
 #endif
   return !!newFrame;
 }
 
-nsresult
+void
 nsBlockFrame::SplitFloat(BlockReflowInput& aState,
-                         nsIFrame*           aFloat,
-                         nsReflowStatus      aFloatStatus)
+                         nsIFrame* aFloat,
+                         const nsReflowStatus& aFloatStatus)
 {
   MOZ_ASSERT(!aFloatStatus.IsFullyComplete(),
              "why split the frame if it's fully complete?");
   MOZ_ASSERT(aState.mBlock == this);
 
   nsIFrame* nextInFlow = aFloat->GetNextInFlow();
   if (nextInFlow) {
     nsContainerFrame *oldParent = nextInFlow->GetParent();
@@ -4330,17 +4330,16 @@ nsBlockFrame::SplitFloat(BlockReflowInpu
     aState.FloatManager()->SetSplitLeftFloatAcrossBreak();
   } else {
     MOZ_ASSERT(floatStyle == StyleFloat::Right, "Unexpected float side!");
     aState.FloatManager()->SetSplitRightFloatAcrossBreak();
   }
 
   aState.AppendPushedFloatChain(nextInFlow);
   aState.mReflowStatus.SetOverflowIncomplete();
-  return NS_OK;
 }
 
 static nsFloatCache*
 GetLastFloat(nsLineBox* aLine)
 {
   nsFloatCache* fc = aLine->GetFirstFloat();
   while (fc && fc->Next()) {
     fc = fc->Next();
diff --git a/layout/generic/nsBlockFrame.h b/layout/generic/nsBlockFrame.h
--- a/layout/generic/nsBlockFrame.h
+++ b/layout/generic/nsBlockFrame.h
@@ -385,19 +385,19 @@ public:
                            nsIFrame* aFrame);
 
   /**
    * Creates a contination for aFloat and adds it to the list of overflow floats.
    * Also updates aState.mReflowStatus to include the float's incompleteness.
    * Must only be called while this block frame is in reflow.
    * aFloatStatus must be the float's true, unmodified reflow status.
    */
-  nsresult SplitFloat(BlockReflowInput& aState,
-                      nsIFrame* aFloat,
-                      nsReflowStatus aFloatStatus);
+  void SplitFloat(BlockReflowInput& aState,
+                  nsIFrame* aFloat,
+                  const nsReflowStatus& aFloatStatus);
 
   /**
    * Walks up the frame tree, starting with aCandidate, and returns the first
    * block frame that it encounters.
    */
   static nsBlockFrame* GetNearestAncestorBlock(nsIFrame* aCandidate);
 
   struct FrameLines {
diff --git a/layout/generic/nsBlockReflowContext.cpp b/layout/generic/nsBlockReflowContext.cpp
--- a/layout/generic/nsBlockReflowContext.cpp
+++ b/layout/generic/nsBlockReflowContext.cpp
@@ -353,21 +353,21 @@ nsBlockReflowContext::ReflowBlock(const 
 
 /**
  * Attempt to place the block frame within the available space.  If
  * it fits, apply inline-dir ("horizontal") positioning (CSS 10.3.3),
  * collapse margins (CSS2 8.3.1). Also apply relative positioning.
  */
 bool
 nsBlockReflowContext::PlaceBlock(const ReflowInput&  aReflowInput,
-                                 bool                      aForceFit,
-                                 nsLineBox*                aLine,
-                                 nsCollapsingMargin&       aBEndMarginResult,
-                                 nsOverflowAreas&          aOverflowAreas,
-                                 nsReflowStatus            aReflowStatus)
+                                 bool aForceFit,
+                                 nsLineBox* aLine,
+                                 nsCollapsingMargin& aBEndMarginResult,
+                                 nsOverflowAreas& aOverflowAreas,
+                                 const nsReflowStatus& aReflowStatus)
 {
   // Compute collapsed block-end margin value.
   WritingMode wm = aReflowInput.GetWritingMode();
   WritingMode parentWM = mMetrics.GetWritingMode();
   if (aReflowStatus.IsComplete()) {
     aBEndMarginResult = mMetrics.mCarriedOutBEndMargin;
     aBEndMarginResult.Include(aReflowInput.ComputedLogicalMargin().
       ConvertTo(parentWM, wm).BEnd(parentWM));
diff --git a/layout/generic/nsBlockReflowContext.h b/layout/generic/nsBlockReflowContext.h
--- a/layout/generic/nsBlockReflowContext.h
+++ b/layout/generic/nsBlockReflowContext.h
@@ -37,21 +37,21 @@ public:
                    nscoord                     aClearance,
                    bool                        aIsAdjacentWithBStart,
                    nsLineBox*                  aLine,
                    ReflowInput&          aReflowInput,
                    nsReflowStatus&             aReflowStatus,
                    BlockReflowInput&         aState);
 
   bool PlaceBlock(const ReflowInput& aReflowInput,
-                  bool                     aForceFit,
-                  nsLineBox*               aLine,
-                  nsCollapsingMargin&      aBEndMarginResult /* out */,
-                  nsOverflowAreas&         aOverflowAreas,
-                  nsReflowStatus           aReflowStatus);
+                  bool aForceFit,
+                  nsLineBox* aLine,
+                  nsCollapsingMargin& aBEndMarginResult /* out */,
+                  nsOverflowAreas& aOverflowAreas,
+                  const nsReflowStatus& aReflowStatus);
 
   nsCollapsingMargin& GetCarriedOutBEndMargin() {
     return mMetrics.mCarriedOutBEndMargin;
   }
 
   const ReflowOutput& GetMetrics() const {
     return mMetrics;
   }
diff --git a/layout/generic/nsFrame.cpp b/layout/generic/nsFrame.cpp
--- a/layout/generic/nsFrame.cpp
+++ b/layout/generic/nsFrame.cpp
@@ -10781,17 +10781,17 @@ nsFrame::Trace(const char* aMethod, bool
   if (NS_FRAME_LOG_TEST(sFrameLogModule, NS_FRAME_TRACE_CALLS)) {
     char tagbuf[40];
     GetTagName(this, mContent, sizeof(tagbuf), tagbuf);
     printf_stderr("%s: %s %s", tagbuf, aEnter ? "enter" : "exit", aMethod);
   }
 }
 
 void
-nsFrame::Trace(const char* aMethod, bool aEnter, nsReflowStatus aStatus)
+nsFrame::Trace(const char* aMethod, bool aEnter, const nsReflowStatus& aStatus)
 {
   if (NS_FRAME_LOG_TEST(sFrameLogModule, NS_FRAME_TRACE_CALLS)) {
     char tagbuf[40];
     GetTagName(this, mContent, sizeof(tagbuf), tagbuf);
     printf_stderr("%s: %s %s, status=%scomplete%s",
                 tagbuf, aEnter ? "enter" : "exit", aMethod,
                 aStatus.IsIncomplete() ? "not" : "",
                 (aStatus.NextInFlowNeedsReflow()) ? "+reflow" : "");
diff --git a/layout/generic/nsFrame.h b/layout/generic/nsFrame.h
--- a/layout/generic/nsFrame.h
+++ b/layout/generic/nsFrame.h
@@ -482,17 +482,17 @@ public:
 #ifdef DEBUG
   /**
    * Tracing method that writes a method enter/exit routine to the
    * nspr log using the nsIFrame log module. The tracing is only
    * done when the NS_FRAME_TRACE_CALLS bit is set in the log module's
    * level field.
    */
   void Trace(const char* aMethod, bool aEnter);
-  void Trace(const char* aMethod, bool aEnter, nsReflowStatus aStatus);
+  void Trace(const char* aMethod, bool aEnter, const nsReflowStatus& aStatus);
   void TraceMsg(const char* fmt, ...) MOZ_FORMAT_PRINTF(2, 3);
 
   // Helper function that verifies that each frame in the list has the
   // NS_FRAME_IS_DIRTY bit set
   static void VerifyDirtyBitSet(const nsFrameList& aFrameList);
 
   // Display Reflow Debugging
   static void* DisplayReflowEnter(nsPresContext*          aPresContext,
