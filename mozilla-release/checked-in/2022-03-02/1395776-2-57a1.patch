# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1504146969 -32400
# Node ID 12b9309d675c40c7728f17c6dad299080c9142aa
# Parent  6ea188b815376634af0b182a873a76e74ae08129
Bug 1395776 - Uniformize valloc implementations in replace-malloc and mozjemalloc. r=njn

valloc is supposed to page-align data, but mozjemalloc's definition of
pagesize is statically compiled in and might not match the actual page
size at runtime (because of MALLOC_STATIC_SIZES). We change valloc in
mozjemalloc to use the runtime page size.

diff --git a/memory/mozjemalloc/mozjemalloc.cpp b/memory/mozjemalloc/mozjemalloc.cpp
--- a/memory/mozjemalloc/mozjemalloc.cpp
+++ b/memory/mozjemalloc/mozjemalloc.cpp
@@ -4373,16 +4373,33 @@ malloc_init(void)
 	return (false);
 }
 #endif
 
 #if defined(XP_DARWIN)
 extern "C" void register_zone(void);
 #endif
 
+static size_t
+GetKernelPageSize()
+{
+  static size_t kernel_page_size = ([]() {
+#ifdef XP_WIN
+    SYSTEM_INFO info;
+    GetSystemInfo(&info);
+    return info.dwPageSize;
+#else
+    long result = sysconf(_SC_PAGESIZE);
+    MOZ_ASSERT(result != -1);
+    return result;
+#endif
+  })();
+  return kernel_page_size;
+}
+
 #if !defined(XP_WIN)
 static
 #endif
 bool
 malloc_init_hard(void)
 {
 	unsigned i;
 	const char *opts;
@@ -4406,29 +4423,17 @@ malloc_init_hard(void)
 #ifdef XP_WIN
 	/* get a thread local storage index */
 	tlsIndex = TlsAlloc();
 #elif defined(XP_DARWIN)
 	pthread_key_create(&tlsIndex, nullptr);
 #endif
 
 	/* Get page size and number of CPUs */
-#ifdef XP_WIN
-	{
-		SYSTEM_INFO info;
-
-		GetSystemInfo(&info);
-		result = info.dwPageSize;
-
-	}
-#else
-	result = sysconf(_SC_PAGESIZE);
-	MOZ_ASSERT(result != -1);
-#endif
-
+	result = GetKernelPageSize();
 	/* We assume that the page size is a power of 2. */
 	MOZ_ASSERT(((result - 1) & result) == 0);
 #ifdef MALLOC_STATIC_SIZES
 	if (pagesize % (size_t) result) {
 		_malloc_message(_getprogname(),
 				"Compile-time page size does not divide the runtime one.\n");
 		MOZ_CRASH();
 	}
@@ -4761,17 +4766,17 @@ aligned_alloc_impl(size_t alignment, siz
 		return nullptr;
 	}
 	return MEMALIGN(alignment, size);
 }
 
 MOZ_MEMORY_API void *
 valloc_impl(size_t size)
 {
-	return (MEMALIGN(pagesize, size));
+	return (MEMALIGN(GetKernelPageSize(), size));
 }
 
 MOZ_MEMORY_API void *
 calloc_impl(size_t num, size_t size)
 {
 	void *ret;
 	size_t num_size;
 
