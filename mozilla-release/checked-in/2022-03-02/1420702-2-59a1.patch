# HG changeset patch
# User Thomas Nguyen <tnguyen@mozilla.com>
# Date 1512049380 -28800
# Node ID 0f6ee76c53931458b43a32f62adff085d9fe0cb7
# Parent  97dadcd05dda3dae004eec4e402e9dc00e0200b5
Bug 1420702 - Test referrer when clicking cross domain link from pinned tab r=ckerschb

MozReview-Commit-ID: DokwVqZcrx7

diff --git a/browser/base/content/test/referrer/browser.ini b/browser/base/content/test/referrer/browser.ini
--- a/browser/base/content/test/referrer/browser.ini
+++ b/browser/base/content/test/referrer/browser.ini
@@ -11,14 +11,15 @@ support-files =
 skip-if = os == 'linux' # Bug 1145199
 [browser_referrer_open_link_in_tab.js]
 skip-if = os == 'linux' # Bug 1144816
 [browser_referrer_open_link_in_window.js]
 skip-if = os == 'linux' # Bug 1145199
 [browser_referrer_open_link_in_window_in_container.js]
 skip-if = os == 'linux' # Bug 1145199
 [browser_referrer_simple_click.js]
+[browser_referrer_click_pinned_tab.js]
 [browser_referrer_open_link_in_container_tab.js]
 skip-if = os == 'linux' # Bug 1144816
 [browser_referrer_open_link_in_container_tab2.js]
 skip-if = os == 'linux' # Bug 1144816
 [browser_referrer_open_link_in_container_tab3.js]
 skip-if = os == 'linux' # Bug 1144816
diff --git a/browser/base/content/test/referrer/browser_referrer_click_pinned_tab.js b/browser/base/content/test/referrer/browser_referrer_click_pinned_tab.js
new file mode 100644
--- /dev/null
+++ b/browser/base/content/test/referrer/browser_referrer_click_pinned_tab.js
@@ -0,0 +1,69 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+// We will open a new tab if clicking on a cross domain link in pinned tab
+// So, override the tests data in head.js, adding "cross: true".
+
+_referrerTests = [
+  {
+    fromScheme: "http://",
+    toScheme: "http://",
+    cross: true,
+    result: "http://test1.example.com/browser"  // full referrer
+  },
+  {
+    fromScheme: "https://",
+    toScheme: "http://",
+    cross: true,
+    result: ""  // no referrer when downgrade
+  },
+  {
+    fromScheme: "https://",
+    toScheme: "http://",
+    policy: "origin",
+    cross: true,
+    result: "https://test1.example.com/"  // origin, even on downgrade
+  },
+  {
+    fromScheme: "https://",
+    toScheme: "http://",
+    policy: "origin",
+    rel: "noreferrer",
+    cross: true,
+    result: ""  // rel=noreferrer trumps meta-referrer
+  },
+  {
+    fromScheme: "https://",
+    toScheme: "https://",
+    policy: "no-referrer",
+    cross: true,
+    result: ""  // same origin https://test1.example.com/browser
+  },
+  {
+    fromScheme: "http://",
+    toScheme: "https://",
+    policy: "no-referrer",
+    cross: true,
+    result: ""  // cross origin http://test1.example.com
+  },
+];
+
+async function startClickPinnedTabTestCase(aTestNumber) {
+  info("browser_referrer_click_pinned_tab: " +
+       getReferrerTestDescription(aTestNumber));
+  let browser = gTestWindow.gBrowser;
+
+  browser.pinTab(browser.selectedTab);
+  someTabLoaded(gTestWindow).then(function(aNewTab) {
+    checkReferrerAndStartNextTest(aTestNumber, null, aNewTab,
+                                  startClickPinnedTabTestCase);
+  });
+
+  clickTheLink(gTestWindow, "testlink", {});
+}
+
+function test() {
+  requestLongerTimeout(10); // slowwww shutdown on e10s
+  startReferrerTest(startClickPinnedTabTestCase);
+}
diff --git a/browser/base/content/test/referrer/file_referrer_policyserver.sjs b/browser/base/content/test/referrer/file_referrer_policyserver.sjs
--- a/browser/base/content/test/referrer/file_referrer_policyserver.sjs
+++ b/browser/base/content/test/referrer/file_referrer_policyserver.sjs
@@ -6,19 +6,21 @@
 function handleRequest(request, response)
 {
   Components.utils.importGlobalProperties(["URLSearchParams"]);
   let query = new URLSearchParams(request.queryString);
 
   let scheme = query.get("scheme");
   let policy = query.get("policy");
   let rel = query.get("rel");
+  let cross = query.get("cross");
 
-  let linkUrl = scheme +
-                "test1.example.com/browser/browser/base/content/test/referrer/" +
+  let host = cross ? "example.com" : "test1.example.com";
+  let linkUrl = scheme + host +
+                "/browser/browser/base/content/test/referrer/" +
                 "file_referrer_testserver.sjs";
   let metaReferrerTag =
       policy ? `<meta name='referrer' content='${policy}'>` : "";
 
   let html = `<!DOCTYPE HTML>
               <html>
               <head>
               <meta charset='utf-8'>
diff --git a/browser/base/content/test/referrer/file_referrer_policyserver_attr.sjs b/browser/base/content/test/referrer/file_referrer_policyserver_attr.sjs
--- a/browser/base/content/test/referrer/file_referrer_policyserver_attr.sjs
+++ b/browser/base/content/test/referrer/file_referrer_policyserver_attr.sjs
@@ -6,20 +6,23 @@
 function handleRequest(request, response)
 {
   Components.utils.importGlobalProperties(["URLSearchParams"]);
   let query = new URLSearchParams(request.queryString);
 
   let scheme = query.get("scheme");
   let policy = query.get("policy");
   let rel = query.get("rel");
+  let cross = query.get("cross");
 
-  let linkUrl = scheme +
-                "test1.example.com/browser/browser/base/content/test/referrer/" +
+  let host = cross ? "example.com" : "test1.example.com";
+  let linkUrl = scheme + host +
+                "/browser/browser/base/content/test/referrer/" +
                 "file_referrer_testserver.sjs";
+
   let referrerPolicy =
       policy ? `referrerpolicy="${policy}"` : "";
 
   let html = `<!DOCTYPE HTML>
               <html>
               <head>
               <meta charset='utf-8'>
               <title>Test referrer</title>
diff --git a/browser/base/content/test/referrer/head.js b/browser/base/content/test/referrer/head.js
--- a/browser/base/content/test/referrer/head.js
+++ b/browser/base/content/test/referrer/head.js
@@ -186,17 +186,18 @@ function doContextMenuCommand(aWindow, a
  */
 function referrerTestCaseLoaded(aTestNumber, aParams) {
   let test = getReferrerTest(aTestNumber);
   let server = rounds == 0 ? REFERRER_POLICYSERVER_URL :
                              REFERRER_POLICYSERVER_URL_ATTRIBUTE;
   let url = test.fromScheme + server +
             "?scheme=" + escape(test.toScheme) +
             "&policy=" + escape(test.policy || "") +
-            "&rel=" + escape(test.rel || "");
+            "&rel=" + escape(test.rel || "") +
+            "&cross=" + escape(test.cross || "");
   let browser = gTestWindow.gBrowser;
   return BrowserTestUtils.openNewForegroundTab(browser, () => {
     browser.selectedTab = browser.addTab(url, aParams);
   }, false, true);
 }
 
 /**
  * Checks the result of the referrer test, and moves on to the next test.
