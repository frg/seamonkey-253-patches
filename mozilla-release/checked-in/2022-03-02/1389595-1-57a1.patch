# HG changeset patch
# User Perry Jiang <jiangperry@gmail.com>
# Date 1504301322 25200
#      Fri Sep 01 14:28:42 2017 -0700
# Node ID 06338b8f9ae16041db213f3cf3af500e8af28eb3
# Parent  0e0b792fef753b2184cba6f79af506768fa6f5fc
Bug 1389595 - cache recipes in LoginRecipesContent and use cache in LoginManagerContent._onFormSubmit. r=MattN

MozReview-Commit-ID: 708hDV8veQ9

diff --git a/toolkit/components/passwordmgr/LoginManagerContent.jsm b/toolkit/components/passwordmgr/LoginManagerContent.jsm
--- a/toolkit/components/passwordmgr/LoginManagerContent.jsm
+++ b/toolkit/components/passwordmgr/LoginManagerContent.jsm
@@ -31,16 +31,20 @@ XPCOMUtils.defineLazyServiceGetter(this,
                                    "@mozilla.org/network/util;1",
                                    "nsINetUtil");
 
 XPCOMUtils.defineLazyGetter(this, "log", () => {
   let logger = LoginHelper.createLogger("LoginManagerContent");
   return logger.log.bind(logger);
 });
 
+Services.cpmm.addMessageListener("clearRecipeCache", () => {
+  LoginRecipesContent._clearRecipeCache();
+});
+
 // These mirror signon.* prefs.
 var gEnabled, gAutofillForms, gStoreWhenAutocompleteOff;
 var gLastRightClickTimeStamp = Number.NEGATIVE_INFINITY;
 
 var observer = {
   QueryInterface: XPCOMUtils.generateQI([Ci.nsIObserver,
                                           Ci.nsIFormSubmitObserver,
                                           Ci.nsIWebProgressListener,
@@ -554,16 +558,19 @@ var LoginManagerContent = {
       userTriggered: true,
     });
   },
 
   loginsFound({ form, loginsFound, recipes }) {
     let doc = form.ownerDocument;
     let autofillForm = gAutofillForms && !PrivateBrowsingUtils.isContentWindowPrivate(doc.defaultView);
 
+    let formOrigin = LoginUtils._getPasswordOrigin(doc.documentURI);
+    LoginRecipesContent.cacheRecipes(formOrigin, doc.defaultView, recipes);
+
     this._fillForm(form, loginsFound, recipes, {autofillForm});
   },
 
   /**
    * Focus event handler for username fields to decide whether to show autocomplete.
    * @param {FocusEvent} event
    */
   _onUsernameFocus(event) {
@@ -922,19 +929,17 @@ var LoginManagerContent = {
     if (!hostname) {
       log("(form submission ignored -- invalid hostname)");
       return;
     }
 
     let formSubmitURL = LoginUtils._getActionOrigin(form);
     let messageManager = messageManagerFromWindow(win);
 
-    let recipes = messageManager.sendSyncMessage("RemoteLogins:findRecipes", {
-      formOrigin: hostname,
-    })[0];
+    let recipes = LoginRecipesContent.getRecipes(hostname, win);
 
     // Get the appropriate fields from the form.
     var [usernameField, newPasswordField, oldPasswordField] =
           this._getFormFields(form, true, recipes);
 
     // Need at least 1 valid password field to do anything.
     if (newPasswordField == null)
       return;
diff --git a/toolkit/components/passwordmgr/LoginRecipes.jsm b/toolkit/components/passwordmgr/LoginRecipes.jsm
--- a/toolkit/components/passwordmgr/LoginRecipes.jsm
+++ b/toolkit/components/passwordmgr/LoginRecipes.jsm
@@ -107,16 +107,17 @@ LoginRecipesParent.prototype = {
             if (!Components.isSuccessCode(result)) {
               throw new Error("Error fetching recipe file:" + result);
             }
             let count = stream.available();
             let data = NetUtil.readInputStreamToString(stream, count, { charset: "UTF-8" });
             resolve(JSON.parse(data));
           });
         }).then(recipes => {
+          Services.ppmm.broadcastAsyncMessage("clearRecipeCache");
           return this.load(recipes);
         }).then(resolve => {
           return this;
         });
       } catch (e) {
         throw new Error("Error reading recipe file:" + e);
       }
     } else {
@@ -183,16 +184,72 @@ LoginRecipesParent.prototype = {
     }
 
     return hostRecipes;
   },
 };
 
 
 var LoginRecipesContent = {
+  _recipeCache: new WeakMap(),
+
+  _clearRecipeCache() {
+    this._recipeCache = new WeakMap();
+  },
+
+  /**
+   * Locally caches recipes for a given host.
+   *
+   * @param {String} aHost (e.g. example.com:8080 [non-default port] or sub.example.com)
+   * @param {Object} win - the window of the host
+   * @param {Set} recipes - recipes that apply to the host
+   */
+  cacheRecipes(aHost, win, recipes) {
+    let recipeMap = this._recipeCache.get(win);
+
+    if (!recipeMap) {
+      recipeMap = new Map();
+      this._recipeCache.set(win, recipeMap);
+    }
+
+    recipeMap.set(aHost, recipes);
+  },
+
+  /**
+   * Tries to fetch recipes for a given host, using a local cache if possible.
+   * Otherwise, the recipes are cached for later use.
+   *
+   * @param {String} aHost (e.g. example.com:8080 [non-default port] or sub.example.com)
+   * @param {Object} win - the window of the host
+   * @return {Set} of recipes that apply to the host
+   */
+  getRecipes(aHost, win) {
+    let recipes;
+    let recipeMap = this._recipeCache.get(win);
+
+    if (recipeMap) {
+      recipes = recipeMap.get(aHost);
+
+      if (recipes) {
+        return recipes;
+      }
+    }
+
+    let mm = win.QueryInterface(Ci.nsIInterfaceRequestor)
+                .getInterface(Ci.nsIWebNavigation)
+                .QueryInterface(Ci.nsIDocShell)
+                .QueryInterface(Ci.nsIInterfaceRequestor)
+                .getInterface(Ci.nsIContentFrameMessageManager);
+
+    recipes = mm.sendSyncMessage("RemoteLogins:findRecipes", { formOrigin: aHost })[0];
+    this.cacheRecipes(aHost, win, recipes);
+
+    return recipes;
+  },
+
   /**
    * @param {Set} aRecipes - Possible recipes that could apply to the form
    * @param {FormLike} aForm - We use a form instead of just a URL so we can later apply
    * tests to the page contents.
    * @return {Set} a subset of recipes that apply to the form with the order preserved
    */
   _filterRecipesForForm(aRecipes, aForm) {
     let formDocURL = aForm.ownerDocument.location;
diff --git a/toolkit/components/passwordmgr/test/mochitest/test_basic_form_autocomplete.html b/toolkit/components/passwordmgr/test/mochitest/test_basic_form_autocomplete.html
--- a/toolkit/components/passwordmgr/test/mochitest/test_basic_form_autocomplete.html
+++ b/toolkit/components/passwordmgr/test/mochitest/test_basic_form_autocomplete.html
@@ -156,29 +156,29 @@ var setupScript = runInParent(function s
 
   <!-- test autocomplete dropdown -->
   <form id="form9" action="http://autocomplete5" onsubmit="return false;">
     <input  type="text"       name="uname">
     <input  type="password"   name="pword">
     <button type="submit">Submit</button>
   </form>
 
+  <!-- tests <form>-less autocomplete -->
+  <div id="form11">
+     <input  type="text"       name="uname" id="uname">
+     <input  type="password"   name="pword" id="pword">
+     <button type="submit">Submit</button>
+  </div>
+
   <!-- test for onUsernameInput recipe testing -->
-  <form id="form11" action="http://autocomplete7" onsubmit="return false;">
+  <form id="form12" action="http://autocomplete7" onsubmit="return false;">
     <input  type="text"   name="1">
     <input  type="text"   name="2">
     <button type="submit">Submit</button>
   </form>
-
-  <!-- tests <form>-less autocomplete -->
-  <div id="form12">
-     <input  type="text"       name="uname" id="uname">
-     <input  type="password"   name="pword" id="pword">
-     <button type="submit">Submit</button>
-   </div>
 </div>
 
 <pre id="test">
 <script class="testbody" type="text/javascript">
 
 /** Test for Login Manager: multiple login autocomplete. **/
 
 var uname = $_(1, "uname");
@@ -765,26 +765,72 @@ add_task(async function test_form9_autoc
   // check that empty results are cached - bug 496466
   promise0 = notifyMenuChanged(0);
   sendChar("z");
   await promise0;
   popupState = await getPopupState();
   is(popupState.open, false, "Check popup stays closed due to cached empty result");
 });
 
-add_task(async function test_form11_recipes() {
+add_task(async function test_form11_formless() {
+  // Test form-less autocomplete
+  uname = $_(11, "uname");
+  pword = $_(11, "pword");
+  restoreForm();
+  checkACForm("", "");
+  let shownPromise = promiseACShown();
+  doKey("down"); // open
+  await shownPromise;
+
+  // Trigger autocomplete
+  doKey("down");
+  checkACForm("", ""); // value shouldn't update
+  let processedPromise = promiseFormsProcessed();
+  doKey("return"); // not "enter"!
+  await processedPromise;
+  checkACForm("testuser", "testpass");
+});
+
+add_task(async function test_form11_open_on_trusted_focus() {
+  uname = $_(11, "uname");
+  pword = $_(11, "pword");
+  uname.value = "";
+  pword.value = "";
+
+  // Move focus to the password field so we can test the first click on the
+  // username field.
+  pword.focus();
+  checkACForm("", "");
+  const firePrivEventPromise = new Promise((resolve) => {
+    uname.addEventListener("click", (e) => {
+      ok(e.isTrusted, "Ensure event is trusted");
+      resolve();
+    });
+  });
+  const shownPromise = promiseACShown();
+  synthesizeMouseAtCenter(uname, {});
+  await firePrivEventPromise;
+  await shownPromise;
+  doKey("down");
+  const processedPromise = promiseFormsProcessed();
+  doKey("return"); // not "enter"!
+  await processedPromise;
+  checkACForm("testuser", "testpass");
+});
+
+add_task(async function test_form12_recipes() {
   await loadRecipes({
     siteRecipes: [{
       "hosts": ["mochi.test:8888"],
       "usernameSelector": "input[name='1']",
       "passwordSelector": "input[name='2']"
     }],
   });
-  uname = $_(11, "1");
-  pword = $_(11, "2");
+  uname = $_(12, "1");
+  pword = $_(12, "2");
 
   // First test DOMAutocomplete
   // Switch the password field to type=password so _fillForm marks the username
   // field for autocomplete.
   pword.type = "password";
   await promiseFormsProcessed();
   restoreForm();
   checkACForm("", "");
@@ -803,58 +849,12 @@ add_task(async function test_form11_reci
   checkACForm("", "");
   uname.value = "testuser10";
   checkACForm("testuser10", "");
   doKey("tab");
   await promiseFormsProcessed();
   checkACForm("testuser10", "testpass10");
   await resetRecipes();
 });
-
-add_task(async function test_form12_formless() {
-  // Test form-less autocomplete
-  uname = $_(12, "uname");
-  pword = $_(12, "pword");
-  restoreForm();
-  checkACForm("", "");
-  let shownPromise = promiseACShown();
-  doKey("down"); // open
-  await shownPromise;
-
-  // Trigger autocomplete
-  doKey("down");
-  checkACForm("", ""); // value shouldn't update
-  let processedPromise = promiseFormsProcessed();
-  doKey("return"); // not "enter"!
-  await processedPromise;
-  checkACForm("testuser", "testpass");
-});
-
-add_task(async function test_form12_open_on_trusted_focus() {
-  uname = $_(12, "uname");
-  pword = $_(12, "pword");
-  uname.value = "";
-  pword.value = "";
-
-  // Move focus to the password field so we can test the first click on the
-  // username field.
-  pword.focus();
-  checkACForm("", "");
-  const firePrivEventPromise = new Promise((resolve) => {
-    uname.addEventListener("click", (e) => {
-      ok(e.isTrusted, "Ensure event is trusted");
-      resolve();
-    });
-  });
-  const shownPromise = promiseACShown();
-  synthesizeMouseAtCenter(uname, {});
-  await firePrivEventPromise;
-  await shownPromise;
-  doKey("down");
-  const processedPromise = promiseFormsProcessed();
-  doKey("return"); // not "enter"!
-  await processedPromise;
-  checkACForm("testuser", "testpass");
-});
 </script>
 </pre>
 </body>
 </html>
