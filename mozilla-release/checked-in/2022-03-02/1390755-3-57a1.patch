# HG changeset patch
# User Ryan Hunt <rhunt@eqrion.net>
# Date 1502917333 18000
#      Wed Aug 16 16:02:13 2017 -0500
# Node ID 5b0eb3f25b7412e3be948bfa95c3d818410944af
# Parent  381bc678ca65f13cc80b46ae2973396723f0ef33
Bug 1390755 - Rename PaintThread and CompositorBridgeChild methods to be more unified. r=mchang

MozReview-Commit-ID: FpwIqEmmvbg

diff --git a/gfx/layers/PaintThread.cpp b/gfx/layers/PaintThread.cpp
--- a/gfx/layers/PaintThread.cpp
+++ b/gfx/layers/PaintThread.cpp
@@ -171,17 +171,17 @@ PaintThread::ShutdownOnPaintThread()
 
 /* static */ bool
 PaintThread::IsOnPaintThread()
 {
   return sThreadId == PlatformThread::CurrentId();
 }
 
 void
-PaintThread::PaintContentsAsync(CompositorBridgeChild* aBridge,
+PaintThread::AsyncPaintContents(CompositorBridgeChild* aBridge,
                                 CapturedPaintState* aState,
                                 PrepDrawTargetForPaintingCallback aCallback)
 {
   MOZ_ASSERT(IsOnPaintThread());
   MOZ_ASSERT(aState);
 
   DrawTarget* target = aState->mTarget;
   DrawTargetCapture* capture = aState->mCapture;
@@ -195,86 +195,86 @@ PaintThread::PaintContentsAsync(Composit
   // Draw all the things into the actual dest target.
   target->DrawCapturedDT(capture, Matrix());
   if (!mDrawTargetsToFlush.Contains(target)) {
     mDrawTargetsToFlush.AppendElement(target);
   }
 }
 
 void
-PaintThread::FinishedLayerBatch()
+PaintThread::EndLayer()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   RefPtr<PaintThread> self = this;
-  RefPtr<Runnable> task = NS_NewRunnableFunction("PaintThread::EndAsyncPaintingLayer",
+  RefPtr<Runnable> task = NS_NewRunnableFunction("PaintThread::AsyncEndLayer",
   [self]() -> void
   {
-    self->EndAsyncPaintingLayer();
+    self->AsyncEndLayer();
   });
 
   if (!gfxPrefs::LayersOMTPForceSync()) {
     sThread->Dispatch(task.forget());
   } else {
     SyncRunnable::DispatchToThread(sThread, task);
   }
 }
 
 void
-PaintThread::EndAsyncPaintingLayer()
+PaintThread::AsyncEndLayer()
 {
   MOZ_ASSERT(IsOnPaintThread());
   // Textureclient forces a flush once we "end paint", so
   // users of this texture expect all the drawing to be complete.
   // Force a flush now.
   for (size_t i = 0; i < mDrawTargetsToFlush.Length(); i++) {
     mDrawTargetsToFlush[i]->Flush();
   }
 
   mDrawTargetsToFlush.Clear();
 }
 
 void
-PaintThread::FinishedLayerTransaction(SyncObjectClient* aSyncObject)
+PaintThread::EndLayerTransaction(SyncObjectClient* aSyncObject)
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   RefPtr<CompositorBridgeChild> cbc;
   if (!gfxPrefs::LayersOMTPForceSync()) {
     cbc = CompositorBridgeChild::Get();
-    cbc->NotifyBeginAsyncPaintEndTransaction();
+    cbc->NotifyBeginAsyncEndLayerTransaction();
   }
 
   RefPtr<SyncObjectClient> syncObject(aSyncObject);
   RefPtr<PaintThread> self = this;
-  RefPtr<Runnable> task = NS_NewRunnableFunction("PaintThread::EndAsyncLayerTransaction",
+  RefPtr<Runnable> task = NS_NewRunnableFunction("PaintThread::AsyncEndLayerTransaction",
     [self, cbc, syncObject]() -> void
   {
-    self->EndAsyncLayerTransaction(cbc, syncObject);
+    self->AsyncEndLayerTransaction(cbc, syncObject);
   });
 
   if (cbc) {
     sThread->Dispatch(task.forget());
   } else {
     SyncRunnable::DispatchToThread(sThread, task);
   }
 }
 
 void
-PaintThread::EndAsyncLayerTransaction(CompositorBridgeChild* aBridge,
+PaintThread::AsyncEndLayerTransaction(CompositorBridgeChild* aBridge,
                                       SyncObjectClient* aSyncObject)
 {
   MOZ_ASSERT(IsOnPaintThread());
 
   if (aSyncObject) {
     aSyncObject->Synchronize();
   }
 
   if (aBridge) {
-    aBridge->NotifyFinishedAsyncPaintEndTransaction();
+    aBridge->NotifyFinishedAsyncEndLayerTransaction();
   }
 }
 
 void
 PaintThread::PaintContents(CapturedPaintState* aState,
                            PrepDrawTargetForPaintingCallback aCallback)
 {
   MOZ_ASSERT(NS_IsMainThread());
@@ -290,17 +290,17 @@ PaintThread::PaintContents(CapturedPaint
   }
   RefPtr<CapturedPaintState> state(aState);
   RefPtr<DrawTargetCapture> capture(aState->mCapture);
 
   RefPtr<PaintThread> self = this;
   RefPtr<Runnable> task = NS_NewRunnableFunction("PaintThread::PaintContents",
     [self, cbc, capture, state, aCallback]() -> void
   {
-    self->PaintContentsAsync(cbc,
+    self->AsyncPaintContents(cbc,
                              state,
                              aCallback);
   });
 
   if (cbc) {
     sThread->Dispatch(task.forget());
   } else {
     SyncRunnable::DispatchToThread(sThread, task);
diff --git a/gfx/layers/PaintThread.h b/gfx/layers/PaintThread.h
--- a/gfx/layers/PaintThread.h
+++ b/gfx/layers/PaintThread.h
@@ -65,48 +65,48 @@ class PaintThread final
 
 public:
   static void Start();
   static void Shutdown();
   static PaintThread* Get();
   void PaintContents(CapturedPaintState* aState,
                      PrepDrawTargetForPaintingCallback aCallback);
 
-  // To be called on the main thread. Signifies that the current
+  // Must be called on the main thread. Signifies that the current
   // batch of CapturedPaintStates* for PaintContents have been recorded
   // and the main thread is finished recording this layer.
-  void FinishedLayerBatch();
+  void EndLayer();
 
   // Must be called on the main thread. Signifies that the current
   // layer tree transaction has been finished and any async paints
   // for it have been queued on the paint thread. This MUST be called
   // at the end of a layer transaction as it will be used to do an optional
   // texture sync and then unblock the main thread if it is waiting to paint
   // a new frame.
-  void FinishedLayerTransaction(SyncObjectClient* aSyncObject);
+  void EndLayerTransaction(SyncObjectClient* aSyncObject);
 
   // Sync Runnables need threads to be ref counted,
   // But this thread lives through the whole process.
   // We're only temporarily using sync runnables so
   // Override release/addref but don't do anything.
   void Release();
   void AddRef();
 
   // Helper for asserts.
   static bool IsOnPaintThread();
 
 private:
   bool Init();
   void ShutdownOnPaintThread();
   void InitOnPaintThread();
-  void PaintContentsAsync(CompositorBridgeChild* aBridge,
+  void AsyncPaintContents(CompositorBridgeChild* aBridge,
                           CapturedPaintState* aState,
                           PrepDrawTargetForPaintingCallback aCallback);
-  void EndAsyncPaintingLayer();
-  void EndAsyncLayerTransaction(CompositorBridgeChild* aBridge,
+  void AsyncEndLayer();
+  void AsyncEndLayerTransaction(CompositorBridgeChild* aBridge,
                                 SyncObjectClient* aSyncObject);
 
   static StaticAutoPtr<PaintThread> sSingleton;
   static StaticRefPtr<nsIThread> sThread;
   static PlatformThreadId sThreadId;
 
   // This shouldn't be very many elements, so a list should be fine.
   // Should only be accessed on the paint thread.
diff --git a/gfx/layers/client/ClientLayerManager.cpp b/gfx/layers/client/ClientLayerManager.cpp
--- a/gfx/layers/client/ClientLayerManager.cpp
+++ b/gfx/layers/client/ClientLayerManager.cpp
@@ -738,17 +738,17 @@ ClientLayerManager::ForwardTransaction(b
   }
 
   // If there were async paints queued, then we need to notify the paint thread
   // that we finished queuing async paints so it can schedule a runnable after
   // all async painting is finished to do a texture sync and unblock the main
   // thread if it is waiting before doing a new layer transaction.
   if (mTextureSyncOnPaintThread) {
     MOZ_ASSERT(PaintThread::Get());
-    PaintThread::Get()->FinishedLayerTransaction(syncObject);
+    PaintThread::Get()->EndLayerTransaction(syncObject);
   } else if (syncObject) {
     syncObject->Synchronize();
   }
 
   mPhase = PHASE_FORWARD;
 
   mLatestTransactionId = mTransactionIdAllocator->GetTransactionId(!mIsRepeatTransaction);
   TimeStamp transactionStart;
diff --git a/gfx/layers/client/ClientPaintedLayer.cpp b/gfx/layers/client/ClientPaintedLayer.cpp
--- a/gfx/layers/client/ClientPaintedLayer.cpp
+++ b/gfx/layers/client/ClientPaintedLayer.cpp
@@ -259,17 +259,17 @@ ClientPaintedLayer::PaintOffMainThread()
     PaintThread::Get()->PaintContents(captureState,
                                       RotatedContentBuffer::PrepareDrawTargetForPainting);
 
     mContentClient->ReturnDrawTargetToBuffer(target);
 
     didUpdate = true;
   }
 
-  PaintThread::Get()->FinishedLayerBatch();
+  PaintThread::Get()->EndLayer();
   mContentClient->EndPaint(nullptr);
 
   if (didUpdate) {
     UpdateContentClient(state);
     ClientManager()->SetNeedTextureSyncOnPaintThread();
   }
   return true;
 }
diff --git a/gfx/layers/ipc/CompositorBridgeChild.cpp b/gfx/layers/ipc/CompositorBridgeChild.cpp
--- a/gfx/layers/ipc/CompositorBridgeChild.cpp
+++ b/gfx/layers/ipc/CompositorBridgeChild.cpp
@@ -1184,27 +1184,27 @@ CompositorBridgeChild::NotifyFinishedAsy
   aState->mTextureClient = nullptr;
   if (aState->mTextureClientOnWhite) {
     aState->mTextureClientOnWhite->DropPaintThreadRef();
     aState->mTextureClientOnWhite = nullptr;
   }
 }
 
 void
-CompositorBridgeChild::NotifyBeginAsyncPaintEndTransaction()
+CompositorBridgeChild::NotifyBeginAsyncEndLayerTransaction()
 {
   MOZ_ASSERT(NS_IsMainThread());
   MonitorAutoLock lock(mPaintLock);
 
   MOZ_ASSERT(!mOutstandingAsyncEndTransaction);
   mOutstandingAsyncEndTransaction = true;
 }
 
 void
-CompositorBridgeChild::NotifyFinishedAsyncPaintEndTransaction()
+CompositorBridgeChild::NotifyFinishedAsyncEndLayerTransaction()
 {
   MOZ_ASSERT(PaintThread::IsOnPaintThread());
   MonitorAutoLock lock(mPaintLock);
 
   // Since this should happen after ALL paints are done and
   // at the end of a transaction, this should always be true.
   MOZ_RELEASE_ASSERT(mOutstandingAsyncPaints == 0);
   MOZ_ASSERT(mOutstandingAsyncEndTransaction);
diff --git a/gfx/layers/ipc/CompositorBridgeChild.h b/gfx/layers/ipc/CompositorBridgeChild.h
--- a/gfx/layers/ipc/CompositorBridgeChild.h
+++ b/gfx/layers/ipc/CompositorBridgeChild.h
@@ -236,22 +236,22 @@ public:
   // Must only be called from the paint thread. Notifies the CompositorBridge
   // that the paint thread has finished an asynchronous paint request.
   void NotifyFinishedAsyncPaint(CapturedPaintState* aState);
 
   // Must only be called from the main thread. Notifies the CompositorBridge
   // that the paint thread is going to perform texture synchronization at the
   // end of async painting, and should postpone messages if needed until
   // finished.
-  void NotifyBeginAsyncPaintEndTransaction();
+  void NotifyBeginAsyncEndLayerTransaction();
 
   // Must only be called from the paint thread. Notifies the CompositorBridge
   // that the paint thread has finished all async paints and texture syncs from
   // a given transaction and may resume sending messages.
-  void NotifyFinishedAsyncPaintEndTransaction();
+  void NotifyFinishedAsyncEndLayerTransaction();
 
 private:
   // Private destructor, to discourage deletion outside of Release():
   virtual ~CompositorBridgeChild();
 
   // Must only be called from the paint thread. If the main thread is delaying
   // IPC messages, this forwards all such delayed IPC messages to the I/O thread
   // and resumes IPC.
