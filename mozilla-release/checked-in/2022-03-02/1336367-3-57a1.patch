# HG changeset patch
# User Bryce Van Dyk <bvandyk@mozilla.com>
# Date 1505352091 -43200
# Node ID 9438b1661ccd7048c015180a7c3ce652d0c6cf0b
# Parent  a5424b7d3df896b37636e5be27c9c139e75453ff
Bug 1336367 - Add gtest for new AudioTrackEncoder init method and behaviour. r=pehrsons

MozReview-Commit-ID: 1IWPu2lD2u6

diff --git a/dom/media/gtest/TestTrackEncoder.cpp b/dom/media/gtest/TestTrackEncoder.cpp
--- a/dom/media/gtest/TestTrackEncoder.cpp
+++ b/dom/media/gtest/TestTrackEncoder.cpp
@@ -17,25 +17,49 @@ public:
     if (Init(aChannels, aSamplingRate) == NS_OK) {
       if (GetPacketDuration()) {
         return true;
       }
     }
     return false;
   }
 
+  bool TestOpusTryCreation(const AudioSegment& aSegment, int aSamplingRate)
+  {
+    if (TryInit(aSegment, aSamplingRate) == NS_OK) {
+      if (GetPacketDuration()) {
+        return true;
+      }
+    }
+    return false;
+  }
+
   // Return the sample rate of data to be fed to the Opus encoder, could be
   // re-sampled if it was not one of the Opus supported sampling rates.
   // Init() is expected to be called first.
   int TestGetOutputSampleRate()
   {
     return mInitialized ? GetOutputSampleRate() : 0;
   }
 };
 
+static AudioSegment
+CreateTestSegment()
+{
+  RefPtr<SharedBuffer> dummyBuffer = SharedBuffer::Create(2);
+  AutoTArray<const int16_t*, 1> channels;
+  const int16_t* channelData = static_cast<const int16_t*>(dummyBuffer->Data());
+  channels.AppendElement(channelData);
+
+  AudioSegment testSegment;
+  testSegment.AppendFrames(
+    dummyBuffer.forget(), channels, 1 /* #samples */, PRINCIPAL_HANDLE_NONE);
+  return testSegment;
+}
+
 static bool
 TestOpusInit(int aChannels, int aSamplingRate)
 {
   TestOpusTrackEncoder encoder;
   return encoder.TestOpusCreation(aChannels, aSamplingRate);
 }
 
 static int
@@ -72,16 +96,79 @@ TEST(Media, OpusEncoder_Init)
   EXPECT_FALSE(TestOpusInit(2, 4000));
   EXPECT_FALSE(TestOpusInit(2, 7999));
   EXPECT_TRUE(TestOpusInit(2, 8000));
   EXPECT_TRUE(TestOpusInit(2, 192000));
   EXPECT_FALSE(TestOpusInit(2, 192001));
   EXPECT_FALSE(TestOpusInit(2, 200000));
 }
 
+TEST(Media, OpusEncoder_TryInit)
+{
+  {
+    // The encoder does not normally recieve enough info from null data to
+    // init. However, multiple attempts to do so, with sufficiently long
+    // duration segments, should result in a best effort attempt. The first
+    // attempt should never do this though, even if the duration is long:
+    TestOpusTrackEncoder encoder;
+    AudioSegment testSegment;
+    testSegment.AppendNullData(48000 * 100);
+    EXPECT_FALSE(encoder.TestOpusTryCreation(testSegment, 48000));
+
+    // Multiple init attempts should result in best effort init:
+    EXPECT_TRUE(encoder.TestOpusTryCreation(testSegment, 48000));
+  }
+
+  {
+    // If the duration of the segments given to the encoder is not long then
+    // we shouldn't try a best effort init:
+    TestOpusTrackEncoder encoder;
+    AudioSegment testSegment;
+    testSegment.AppendNullData(1);
+    EXPECT_FALSE(encoder.TestOpusTryCreation(testSegment, 48000));
+    EXPECT_FALSE(encoder.TestOpusTryCreation(testSegment, 48000));
+  }
+
+  {
+    // For non-null segments we should init immediately
+    TestOpusTrackEncoder encoder;
+    AudioSegment testSegment = CreateTestSegment();
+    EXPECT_TRUE(encoder.TestOpusTryCreation(testSegment, 48000));
+  }
+
+  {
+    // Test low sample rate bound
+    TestOpusTrackEncoder encoder;
+    AudioSegment testSegment = CreateTestSegment();
+    EXPECT_FALSE(encoder.TestOpusTryCreation(testSegment, 7999));
+  }
+
+  {
+    // Test low sample rate bound
+    TestOpusTrackEncoder encoder;
+    AudioSegment testSegment = CreateTestSegment();
+    EXPECT_FALSE(encoder.TestOpusTryCreation(testSegment, 7999));
+    EXPECT_TRUE(encoder.TestOpusTryCreation(testSegment, 8000));
+  }
+
+  {
+    // Test high sample rate bound
+    TestOpusTrackEncoder encoder;
+    AudioSegment testSegment = CreateTestSegment();
+    EXPECT_FALSE(encoder.TestOpusTryCreation(testSegment, 192001));
+  }
+
+  {
+    // Test high sample rate bound
+    TestOpusTrackEncoder encoder;
+    AudioSegment testSegment = CreateTestSegment();
+    EXPECT_TRUE(encoder.TestOpusTryCreation(testSegment, 192000));
+  }
+}
+
 TEST(Media, OpusEncoder_Resample)
 {
   // Sampling rates of data to be fed to Opus encoder, should remain unchanged
   // if it is one of Opus supported rates (8000, 12000, 16000, 24000 and 48000
   // (kHz)) at initialization.
   EXPECT_TRUE(TestOpusResampler(1, 8000) == 8000);
   EXPECT_TRUE(TestOpusResampler(1, 12000) == 12000);
   EXPECT_TRUE(TestOpusResampler(1, 16000) == 16000);
