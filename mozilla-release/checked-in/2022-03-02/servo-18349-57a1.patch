# HG changeset patch
# User Nazim Can Altinova <canaltinova@gmail.com>
# Date 1504363031 18000
#      Sat Sep 02 09:37:11 2017 -0500
# Node ID 83e27f8afa12874d6579490e98571efd26382b6a
# Parent  f7aef33a5715c447323c513390f3aa013711937a
servo: Merge #18349 - stylo: Remove calc support from media queries (from canaltinova:media-calc); r=emilio

Gecko currently doesn't support calc inside media queries. We should
also remove the calc support temporarily for parity with gecko. We can
add this support back in next releases.

Reviewed by emilio in Bugzilla.

---
- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [X] These changes fix [Bug 1390339](https://bugzilla.mozilla.org/show_bug.cgi?id=1390339)

Source-Repo: https://github.com/servo/servo
Source-Revision: f36580cb9187b99793f89cf60e5989979f83efca

diff --git a/servo/components/style/gecko/media_queries.rs b/servo/components/style/gecko/media_queries.rs
--- a/servo/components/style/gecko/media_queries.rs
+++ b/servo/components/style/gecko/media_queries.rs
@@ -27,18 +27,19 @@ use selectors::parser::SelectorParseErro
 use servo_arc::Arc;
 use std::fmt::{self, Write};
 use std::sync::atomic::{AtomicBool, AtomicIsize, Ordering};
 use str::starts_with_ignore_ascii_case;
 use string_cache::Atom;
 use style_traits::{CSSPixel, DevicePixel};
 use style_traits::{ToCss, ParseError, StyleParseError};
 use style_traits::viewport::ViewportConstraints;
-use values::{CSSFloat, specified, CustomIdent, serialize_dimension};
+use values::{CSSFloat, CustomIdent, serialize_dimension};
 use values::computed::{self, ToComputedValue};
+use values::specified::Length;
 
 /// The `Device` in Gecko wraps a pres context, has a default values computed,
 /// and contains all the viewport rule state.
 pub struct Device {
     /// NB: The pres context lifetime is tied to the styleset, who owns the
     /// stylist, and thus the `Device`, so having a raw pres context pointer
     /// here is fine.
     pres_context: RawGeckoPresContextOwned,
@@ -299,17 +300,17 @@ impl ToCss for Resolution {
         }
     }
 }
 
 /// A value found or expected in a media expression.
 #[derive(Clone, Debug, PartialEq)]
 pub enum MediaExpressionValue {
     /// A length.
-    Length(specified::Length),
+    Length(Length),
     /// A (non-negative) integer.
     Integer(u32),
     /// A floating point value.
     Float(CSSFloat),
     /// A boolean value, specified as an integer (i.e., either 0 or 1).
     BoolInteger(bool),
     /// Two integers separated by '/', with optional whitespace on either side
     /// of the '/'.
@@ -334,17 +335,17 @@ impl MediaExpressionValue {
         if css_value.mUnit == nsCSSUnit::eCSSUnit_Null {
             return None;
         }
 
         match for_expr.feature.mValueType {
             nsMediaFeature_ValueType::eLength => {
                 debug_assert!(css_value.mUnit == nsCSSUnit::eCSSUnit_Pixel);
                 let pixels = css_value.float_unchecked();
-                Some(MediaExpressionValue::Length(specified::Length::from_px(pixels)))
+                Some(MediaExpressionValue::Length(Length::from_px(pixels)))
             }
             nsMediaFeature_ValueType::eInteger => {
                 let i = css_value.integer_unchecked();
                 debug_assert!(i >= 0);
                 Some(MediaExpressionValue::Integer(i as u32))
             }
             nsMediaFeature_ValueType::eFloat => {
                 debug_assert!(css_value.mUnit == nsCSSUnit::eCSSUnit_Number);
@@ -550,20 +551,30 @@ impl Expression {
                 if range != nsMediaExpression_Range::eEqual {
                     return Err(StyleParseError::RangedExpressionWithNoValue.into())
                 }
                 return Ok(Expression::new(feature, None, range));
             }
 
             let value = match feature.mValueType {
                 nsMediaFeature_ValueType::eLength => {
-                    MediaExpressionValue::Length(
-                        specified::Length::parse_non_negative(context, input)?)
+                    let length = Length::parse_non_negative(context, input)?;
+                    // FIXME(canaltinova): See bug 1396057. Gecko doesn't support calc
+                    // inside media queries. This check is for temporarily remove it
+                    // for parity with gecko. We should remove this check when we want
+                    // to support it.
+                    if let Length::Calc(_) = length {
+                        return Err(StyleParseError::UnspecifiedError.into())
+                    }
+                    MediaExpressionValue::Length(length)
                 },
                 nsMediaFeature_ValueType::eInteger => {
+                    // FIXME(emilio): We should use `Integer::parse` to handle `calc`
+                    // properly in integer expressions. Note that calc is still not
+                    // supported in media queries per FIXME above.
                     let i = input.expect_integer()?;
                     if i < 0 {
                         return Err(StyleParseError::UnspecifiedError.into())
                     }
                     MediaExpressionValue::Integer(i as u32)
                 }
                 nsMediaFeature_ValueType::eBoolInteger => {
                     let i = input.expect_integer()?;
