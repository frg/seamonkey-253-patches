# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1499850268 -7200
#      Wed Jul 12 11:04:28 2017 +0200
# Node ID 7a0133d5ee1d9ca3c8ce3456d0ba83e5330ebf61
# Parent  80a0e81ef702090055d410da20544e9ba19b7d58
Bug 1374735 - use DevToolsShim to retrieve the WebExtensionInspectedWindowFront;r=ochameau,rpl

Expose an API to retrieve devtools' WebExtensionInspectedWindowFront which is needed
for webextensions. Exposed a new API on devtools/DevToolsShim

MozReview-Commit-ID: Dyc2UUJGsrs

diff --git a/browser/components/extensions/ext-devtools-inspectedWindow.js b/browser/components/extensions/ext-devtools-inspectedWindow.js
--- a/browser/components/extensions/ext-devtools-inspectedWindow.js
+++ b/browser/components/extensions/ext-devtools-inspectedWindow.js
@@ -1,33 +1,32 @@
 /* -*- Mode: indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set sts=2 sw=2 et tw=80: */
 "use strict";
 
 /* global getDevToolsTargetForContext */
 
+XPCOMUtils.defineLazyModuleGetter(this, "DevToolsShim",
+                                  "chrome://devtools-shim/content/DevToolsShim.jsm");
+
 var {
   SpreadArgs,
 } = ExtensionCommon;
 
 this.devtools_inspectedWindow = class extends ExtensionAPI {
   getAPI(context) {
-    const {
-      WebExtensionInspectedWindowFront,
-    } = require("devtools/shared/fronts/webextension-inspected-window");
-
     // Lazily retrieve and store an inspectedWindow actor front per child context.
     let waitForInspectedWindowFront;
     async function getInspectedWindowFront() {
       // If there is not yet a front instance, then a lazily cloned target for the context is
       // retrieved using the DevtoolsParentContextsManager helper (which is an asynchronous operation,
       // because the first time that the target has been cloned, it is not ready to be used to create
       // the front instance until it is connected to the remote debugger successfully).
       const clonedTarget = await getDevToolsTargetForContext(context);
-      return new WebExtensionInspectedWindowFront(clonedTarget.client, clonedTarget.form);
+      return DevToolsShim.createWebExtensionInspectedWindowFront(clonedTarget);
     }
 
     function getToolboxOptions() {
       const options = {};
       const toolbox = context.devToolsToolbox;
       const selectedNode = toolbox.selection;
 
       if (selectedNode && selectedNode.nodeFront) {
diff --git a/devtools/client/framework/devtools.js b/devtools/client/framework/devtools.js
--- a/devtools/client/framework/devtools.js
+++ b/devtools/client/framework/devtools.js
@@ -17,16 +17,19 @@ loader.lazyRequireGetter(this, "gDevTool
 loader.lazyRequireGetter(this, "HUDService", "devtools/client/webconsole/hudservice", true);
 loader.lazyImporter(this, "ScratchpadManager", "resource://devtools/client/scratchpad/scratchpad-manager.jsm");
 
 // Dependencies required for addon sdk compatibility layer.
 loader.lazyRequireGetter(this, "DebuggerServer", "devtools/server/main", true);
 loader.lazyRequireGetter(this, "DebuggerClient", "devtools/shared/client/main", true);
 loader.lazyImporter(this, "BrowserToolboxProcess", "resource://devtools/client/framework/ToolboxProcess.jsm");
 
+loader.lazyRequireGetter(this, "WebExtensionInspectedWindowFront",
+      "devtools/shared/fronts/webextension-inspected-window", true);
+
 const {defaultTools: DefaultTools, defaultThemes: DefaultThemes} =
   require("devtools/client/definitions");
 const EventEmitter = require("devtools/shared/old-event-emitter");
 const AboutDevTools = require("devtools/client/framework/about-devtools-toolbox");
 const {Task} = require("devtools/shared/task");
 const {getTheme, setTheme, addThemeObserver, removeThemeObserver} =
   require("devtools/client/shared/theme");
 
@@ -575,16 +578,24 @@ DevTools.prototype = {
    *
    * Create a BrowserToolbox process linked to the provided addon id.
    */
   initBrowserToolboxProcessForAddon: function (addonID) {
     BrowserToolboxProcess.init({ addonID });
   },
 
   /**
+   * Compatibility layer for web-extensions. Used by DevToolsShim for
+   * browser/components/extensions/ext-devtools-inspectedWindow.js
+   */
+  createWebExtensionInspectedWindowFront: function (tabTarget) {
+    return new WebExtensionInspectedWindowFront(tabTarget.client, tabTarget.form);
+  },
+
+  /**
    * Called from the DevToolsShim, used by nsContextMenu.js.
    *
    * @param {XULTab} tab
    *        The browser tab on which inspect node was used.
    * @param {Array} selectors
    *        An array of CSS selectors to find the target node. Several selectors can be
    *        needed if the element is nested in frames and not directly in the root
    *        document.
diff --git a/devtools/shim/DevToolsShim.jsm b/devtools/shim/DevToolsShim.jsm
--- a/devtools/shim/DevToolsShim.jsm
+++ b/devtools/shim/DevToolsShim.jsm
@@ -294,15 +294,16 @@ let addonSdkMethods = [
  * Compatibility layer for webextensions.
  *
  * Those methods are called only after a DevTools webextension was loaded in DevTools,
  * therefore DevTools should always be available when they are called.
  */
 let webExtensionsMethods = [
   "getTargetForTab",
   "getTheme",
+  "createWebExtensionInspectedWindowFront",
 ];
 
 for (let method of [...addonSdkMethods, ...webExtensionsMethods]) {
   this.DevToolsShim[method] = function () {
     return this.gDevTools[method].apply(this.gDevTools, arguments);
   };
 }
