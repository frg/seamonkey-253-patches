# HG changeset patch
# User Dragana Damjanovic <dd.mozilla@gmail.com>
# Date 1513071252 -3600
#      Tue Dec 12 10:34:12 2017 +0100
# Node ID c31b663b4dd2cad205e3a3bc2089fd45405daab1
# Parent  709bc591be5bc4beaffc2e1d1ddb22b6ecfa67ac
Bug 1409449 - Do not show auth-dialog for triggeringPrincipal==SystemPrincipal. r=ckerschb r=valentin r=francois

diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -2153,16 +2153,21 @@ pref("network.auth.subresource-http-auth
 
 // Sub-resources HTTP-authentication for cross-origin images:
 // true - it is allowed to present http auth. dialog for cross-origin images.
 // false - it is not allowed.
 // If network.auth.subresource-http-auth-allow has values 0 or 1 this pref does not
 // have any effect.
 pref("network.auth.subresource-img-cross-origin-http-auth-allow", true);
 
+// Resources that are triggered by some non-web-content:
+// true - they are allow to present http auth. dialog
+// false - they are not allow to present http auth. dialog.
+pref("network.auth.non-web-content-triggered-resources-http-auth-allow", false);
+
 // This preference controls whether to allow sending default credentials (SSO) to
 // NTLM/Negotiate servers allowed in the "trusted uri" list when navigating them
 // in a Private Browsing window.
 // If set to false, Private Browsing windows will not use default credentials and ask
 // for credentials from the user explicitly.
 // If set to true, and a server URL conforms other conditions for sending default
 // credentials, those will be sent automatically in Private Browsing windows.
 //
diff --git a/netwerk/protocol/http/nsHttpChannelAuthProvider.cpp b/netwerk/protocol/http/nsHttpChannelAuthProvider.cpp
--- a/netwerk/protocol/http/nsHttpChannelAuthProvider.cpp
+++ b/netwerk/protocol/http/nsHttpChannelAuthProvider.cpp
@@ -37,16 +37,17 @@ namespace net {
 
 #define SUBRESOURCE_AUTH_DIALOG_DISALLOW_ALL 0
 #define SUBRESOURCE_AUTH_DIALOG_DISALLOW_CROSS_ORIGIN 1
 #define SUBRESOURCE_AUTH_DIALOG_ALLOW_ALL 2
 
 #define HTTP_AUTH_DIALOG_TOP_LEVEL_DOC 29
 #define HTTP_AUTH_DIALOG_SAME_ORIGIN_SUBRESOURCE 30
 #define HTTP_AUTH_DIALOG_SAME_ORIGIN_XHR 31
+#define HTTP_AUTH_DIALOG_NON_WEB_CONTENT 32
 
 #define HTTP_AUTH_BASIC_INSECURE 0
 #define HTTP_AUTH_BASIC_SECURE 1
 #define HTTP_AUTH_DIGEST_INSECURE 2
 #define HTTP_AUTH_DIGEST_SECURE 3
 #define HTTP_AUTH_NTLM_INSECURE 4
 #define HTTP_AUTH_NTLM_SECURE 5
 #define HTTP_AUTH_NEGOTIATE_INSECURE 6
@@ -90,27 +91,31 @@ nsHttpChannelAuthProvider::~nsHttpChanne
 {
     MOZ_ASSERT(!mAuthChannel, "Disconnect wasn't called");
 }
 
 uint32_t nsHttpChannelAuthProvider::sAuthAllowPref =
     SUBRESOURCE_AUTH_DIALOG_ALLOW_ALL;
 
 bool nsHttpChannelAuthProvider::sImgCrossOriginAuthAllowPref = true;
+bool nsHttpChannelAuthProvider::sNonWebContentTriggeredAuthAllow = false;
 
 void
 nsHttpChannelAuthProvider::InitializePrefs()
 {
   MOZ_ASSERT(NS_IsMainThread());
   mozilla::Preferences::AddUintVarCache(&sAuthAllowPref,
                                         "network.auth.subresource-http-auth-allow",
                                         SUBRESOURCE_AUTH_DIALOG_ALLOW_ALL);
   mozilla::Preferences::AddBoolVarCache(&sImgCrossOriginAuthAllowPref,
                                         "network.auth.subresource-img-cross-origin-http-auth-allow",
                                         true);
+  mozilla::Preferences::AddBoolVarCache(&sNonWebContentTriggeredAuthAllow,
+                                        "network.auth.non-web-content-triggered-resources-http-auth-allow",
+                                        false);
 }
 
 NS_IMETHODIMP
 nsHttpChannelAuthProvider::Init(nsIHttpAuthenticableChannel *channel)
 {
     MOZ_ASSERT(channel, "channel expected!");
 
     mAuthChannel = channel;
@@ -907,18 +912,20 @@ nsHttpChannelAuthProvider::GetCredential
 
             // Depending on the pref setting, the authentication dialog may be
             // blocked for all sub-resources, blocked for cross-origin
             // sub-resources, or always allowed for sub-resources.
             // For more details look at the bug 647010.
             // BlockPrompt will set mCrossOrigin parameter as well.
             if (BlockPrompt(proxyAuth)) {
                 LOG(("nsHttpChannelAuthProvider::GetCredentialsForChallenge: "
-                     "Prompt is blocked [this=%p pref=%d img-pref=%d]\n",
-                      this, sAuthAllowPref, sImgCrossOriginAuthAllowPref));
+                     "Prompt is blocked [this=%p pref=%d img-pref=%d "
+                     "non-web-content-triggered-pref=%d]\n",
+                      this, sAuthAllowPref, sImgCrossOriginAuthAllowPref,
+                      sNonWebContentTriggeredAuthAllow));
                 return NS_ERROR_ABORT;
             }
 
             // at this point we are forced to interact with the user to get
             // their username and password for this domain.
             rv = PromptForIdentity(level, proxyAuth, realm.get(),
                                    authType, authFlags, *ident);
             if (NS_FAILED(rv)) return rv;
@@ -983,18 +990,25 @@ nsHttpChannelAuthProvider::BlockPrompt(b
 
     nsCOMPtr<nsIChannel> chan = do_QueryInterface(mAuthChannel);
     nsCOMPtr<nsILoadInfo> loadInfo;
     chan->GetLoadInfo(getter_AddRefs(loadInfo));
 
     // We will treat loads w/o loadInfo as a top level document.
     bool topDoc = true;
     bool xhr = false;
+    bool nonWebContent = false;
 
     if (loadInfo) {
+        nsCOMPtr<nsIPrincipal> triggeringPrinc =
+            loadInfo->TriggeringPrincipal();
+        if (nsContentUtils::IsSystemPrincipal(triggeringPrinc)) {
+            nonWebContent = true;
+        }
+
         if (loadInfo->GetExternalContentPolicyType() !=
             nsIContentPolicy::TYPE_DOCUMENT) {
             topDoc = false;
         }
         if (loadInfo->GetExternalContentPolicyType() ==
             nsIContentPolicy::TYPE_XMLHTTPREQUEST) {
             xhr = true;
         }
@@ -1013,33 +1027,40 @@ nsHttpChannelAuthProvider::BlockPrompt(b
 
             if (!NS_SecurityCompareURIs(topURI, mURI, true)) {
                 mCrossOrigin = true;
             }
         }
     }
 
     if (gHttpHandler->IsTelemetryEnabled()) {
-        if (topDoc) {
-            Telemetry::Accumulate(Telemetry::HTTP_AUTH_DIALOG_STATS_2,
+        if (nonWebContent) {
+            Telemetry::Accumulate(Telemetry::HTTP_AUTH_DIALOG_STATS_3,
+                                  HTTP_AUTH_DIALOG_NON_WEB_CONTENT);
+        } else if (topDoc) {
+            Telemetry::Accumulate(Telemetry::HTTP_AUTH_DIALOG_STATS_3,
                                   HTTP_AUTH_DIALOG_TOP_LEVEL_DOC);
         } else if (!mCrossOrigin) {
             if (xhr) {
-                Telemetry::Accumulate(Telemetry::HTTP_AUTH_DIALOG_STATS_2,
+                Telemetry::Accumulate(Telemetry::HTTP_AUTH_DIALOG_STATS_3,
                                       HTTP_AUTH_DIALOG_SAME_ORIGIN_XHR);
             } else {
-                Telemetry::Accumulate(Telemetry::HTTP_AUTH_DIALOG_STATS_2,
+                Telemetry::Accumulate(Telemetry::HTTP_AUTH_DIALOG_STATS_3,
                                       HTTP_AUTH_DIALOG_SAME_ORIGIN_SUBRESOURCE);
             }
         } else {
-            Telemetry::Accumulate(Telemetry::HTTP_AUTH_DIALOG_STATS_2,
+            Telemetry::Accumulate(Telemetry::HTTP_AUTH_DIALOG_STATS_3,
                                   loadInfo->GetExternalContentPolicyType());
         }
     }
 
+    if (!sNonWebContentTriggeredAuthAllow && nonWebContent) {
+        return true;
+    }
+
     switch (sAuthAllowPref) {
     case SUBRESOURCE_AUTH_DIALOG_DISALLOW_ALL:
         // Do not open the http-authentication credentials dialog for
         // the sub-resources.
         return !topDoc && !xhr;
     case SUBRESOURCE_AUTH_DIALOG_DISALLOW_CROSS_ORIGIN:
         // Open the http-authentication credentials dialog for
         // the sub-resources only if they are not cross-origin.
diff --git a/netwerk/protocol/http/nsHttpChannelAuthProvider.h b/netwerk/protocol/http/nsHttpChannelAuthProvider.h
--- a/netwerk/protocol/http/nsHttpChannelAuthProvider.h
+++ b/netwerk/protocol/http/nsHttpChannelAuthProvider.h
@@ -182,15 +182,16 @@ private:
 
     RefPtr<nsHttpHandler>           mHttpHandler;  // keep gHttpHandler alive
 
     // A variable holding the preference settings to whether to open HTTP
     // authentication credentials dialogs for sub-resources and cross-origin
     // sub-resources.
     static uint32_t                   sAuthAllowPref;
     static bool                       sImgCrossOriginAuthAllowPref;
+    static bool                       sNonWebContentTriggeredAuthAllow;
     nsCOMPtr<nsICancelable>           mGenerateCredentialsCancelable;
 };
 
 } // namespace net
 } // namespace mozilla
 
 #endif // nsHttpChannelAuthProvider_h__
diff --git a/toolkit/components/telemetry/Histograms.json b/toolkit/components/telemetry/Histograms.json
--- a/toolkit/components/telemetry/Histograms.json
+++ b/toolkit/components/telemetry/Histograms.json
@@ -2503,24 +2503,24 @@
     "record_in_processes": ["main"],
     "expires_in_version": "62",
     "alert_emails": ["necko@mozilla.com"],
     "bug_numbers": [1377223],
     "kind": "categorical",
     "labels": ["NotSent", "CachedContentUsed", "CachedContentNotUsed"],
     "description": "Stats for validation requests when cache won the race."
   },
-  "HTTP_AUTH_DIALOG_STATS_2": {
+  "HTTP_AUTH_DIALOG_STATS_3": {
     "record_in_processes": ["main", "content"],
     "expires_in_version": "61",
-    "alert_emails": ["necko@mozilla.com"],
+    "alert_emails": ["necko@mozilla.com", "ddamjanovic@mozilla.com"],
     "bug_numbers": [1357835],
     "kind": "enumerated",
-    "n_values": 32,
-    "description": "Stats about what kind of resource requested http authentication. (29=top-level doc, 30=same origin subresources, 31=same origin xhr, (nsIContentPolicy type)=cross-origin subresources per nsIContentPolicy type)"
+    "n_values": 64,
+    "description": "Stats about what kind of resource requested http authentication. (29=top-level doc, 30=same origin subresources, 31=same origin xhr, 32=non-web-content, (nsIContentPolicy type)=cross-origin subresources per nsIContentPolicy type)"
   },
   "HTTP_AUTH_TYPE_STATS": {
     "record_in_processes": ["main", "content"],
     "alert_emails": ["rbarnes@mozilla.com"],
     "bug_numbers": [1266571],
     "expires_in_version": "52",
     "kind": "enumerated",
     "n_values": 8,
