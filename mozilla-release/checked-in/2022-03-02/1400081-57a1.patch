# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1505436633 -36000
#      Fri Sep 15 10:50:33 2017 +1000
# Node ID aa14c5a7d4e15594030f9ecc95bd2addb5b72496
# Parent  30e109b0ad79d9bbb9ebc215f36201d71345a650
Bug 1400081 - Don't use -1 to represent an unset JSGCParamKey. r=sfink.

Because UBSan complains about casting -1:

> runtime error: load of value 4294967295, which is not a valid value for type 'JSGCParamKey'

diff --git a/dom/workers/RuntimeService.cpp b/dom/workers/RuntimeService.cpp
--- a/dom/workers/RuntimeService.cpp
+++ b/dom/workers/RuntimeService.cpp
@@ -848,19 +848,19 @@ InitJSContextForWorker(WorkerPrivate* aW
 
   JS::ContextOptionsRef(aWorkerCx) = settings.contextOptions;
 
   JSSettings::JSGCSettingsArray& gcSettings = settings.gcSettings;
 
   // This is the real place where we set the max memory for the runtime.
   for (uint32_t index = 0; index < ArrayLength(gcSettings); index++) {
     const JSSettings::JSGCSetting& setting = gcSettings[index];
-    if (setting.IsSet()) {
+    if (setting.key.isSome()) {
       NS_ASSERTION(setting.value, "Can't handle 0 values!");
-      JS_SetGCParameter(aWorkerCx, setting.key, setting.value);
+      JS_SetGCParameter(aWorkerCx, *setting.key, setting.value);
     }
   }
 
   JS_SetNativeStackQuota(aWorkerCx, WORKER_CONTEXT_NATIVE_STACK_LIMIT);
 
   // Security policy:
   static const JSSecurityCallbacks securityCallbacks = {
     ContentSecurityPolicyAllows
@@ -1909,17 +1909,17 @@ RuntimeService::Init()
   if (!BackgroundChild::GetForCurrentThread()) {
     RefPtr<BackgroundChildCallback> callback = new BackgroundChildCallback();
     if (!BackgroundChild::GetOrCreateForCurrentThread(callback)) {
       MOZ_CRASH("Unable to connect PBackground actor for the main thread!");
     }
   }
 
   // Initialize JSSettings.
-  if (!sDefaultJSSettings.gcSettings[0].IsSet()) {
+  if (sDefaultJSSettings.gcSettings[0].key.isNothing()) {
     sDefaultJSSettings.contextOptions = JS::ContextOptions();
     sDefaultJSSettings.chrome.maxScriptRuntime = -1;
     sDefaultJSSettings.chrome.compartmentOptions.behaviors().setVersion(JSVERSION_DEFAULT);
     sDefaultJSSettings.content.maxScriptRuntime = MAX_SCRIPT_RUN_TIME_SEC;
 #ifdef JS_GC_ZEAL
     sDefaultJSSettings.gcZealFrequency = JS_DEFAULT_ZEAL_FREQ;
     sDefaultJSSettings.gcZeal = 0;
 #endif
diff --git a/dom/workers/Workers.h b/dom/workers/Workers.h
--- a/dom/workers/Workers.h
+++ b/dom/workers/Workers.h
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_workers_workers_h__
 #define mozilla_dom_workers_workers_h__
 
 #include "jsapi.h"
 #include "mozilla/Attributes.h"
+#include "mozilla/Maybe.h"
 #include "mozilla/Mutex.h"
 #include <stdint.h>
 #include "nsAutoPtr.h"
 #include "nsCOMPtr.h"
 #include "nsDebug.h"
 #include "nsString.h"
 #include "nsTArray.h"
 
@@ -97,35 +98,22 @@ struct JSSettings
     // JSGC_MODE not supported
 
     // This must be last so that we get an accurate count.
     kGCSettingsArraySize
   };
 
   struct JSGCSetting
   {
-    JSGCParamKey key;
+    mozilla::Maybe<JSGCParamKey> key;
     uint32_t value;
 
     JSGCSetting()
-    : key(static_cast<JSGCParamKey>(-1)), value(0)
+    : key(), value(0)
     { }
-
-    bool
-    IsSet() const
-    {
-      return key != static_cast<JSGCParamKey>(-1);
-    }
-
-    void
-    Unset()
-    {
-      key = static_cast<JSGCParamKey>(-1);
-      value = 0;
-    }
   };
 
   // There are several settings that we know we need so it makes sense to
   // preallocate here.
   typedef JSGCSetting JSGCSettingsArray[kGCSettingsArraySize];
 
   // Settings that change based on chrome/content context.
   struct JSContentChromeSettings
@@ -161,40 +149,40 @@ struct JSSettings
   bool
   ApplyGCSetting(JSGCParamKey aKey, uint32_t aValue)
   {
     JSSettings::JSGCSetting* firstEmptySetting = nullptr;
     JSSettings::JSGCSetting* foundSetting = nullptr;
 
     for (uint32_t index = 0; index < ArrayLength(gcSettings); index++) {
       JSSettings::JSGCSetting& setting = gcSettings[index];
-      if (setting.key == aKey) {
+      if (setting.key.isSome() && *setting.key == aKey) {
         foundSetting = &setting;
         break;
       }
-      if (!firstEmptySetting && !setting.IsSet()) {
+      if (!firstEmptySetting && setting.key.isNothing()) {
         firstEmptySetting = &setting;
       }
     }
 
     if (aValue) {
       if (!foundSetting) {
         foundSetting = firstEmptySetting;
         if (!foundSetting) {
           NS_ERROR("Not enough space for this value!");
           return false;
         }
       }
-      foundSetting->key = aKey;
+      foundSetting->key = mozilla::Some(aKey);
       foundSetting->value = aValue;
       return true;
     }
 
     if (foundSetting) {
-      foundSetting->Unset();
+      foundSetting->key.reset();
       return true;
     }
 
     return false;
   }
 };
 
 enum WorkerPreference
