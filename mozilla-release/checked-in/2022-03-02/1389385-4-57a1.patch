# HG changeset patch
# User Bobby Holley <bobbyholley@gmail.com>
# Date 1500426274 25200
#      Tue Jul 18 18:04:34 2017 -0700
# Node ID 4a977fea2799d75b82dbd9cf4da8d0dabadee65d
# Parent  6e4320663c91c815d21f9fe3da4aac4604468e72
Bug 1389385 - Rearrange dirty noting to operate on the element rather than the parent. r=emilio

This will allow us to scope restyle roots more tightly.

MozReview-Commit-ID: 2t2lp5sKBHH

diff --git a/dom/base/Element.cpp b/dom/base/Element.cpp
--- a/dom/base/Element.cpp
+++ b/dom/base/Element.cpp
@@ -4338,39 +4338,41 @@ BitIsPropagated(const Element* aElement)
                   parentNode == aElement->OwnerDoc() ||
                   parentNode == parentNode->OwnerDoc()->GetRootElement());
   }
   return true;
 }
 #endif
 
 template<typename Traits>
-inline void NoteDescendants(Element* aElement)
+void
+NoteDirtyContent(nsIContent* aContent)
 {
-  if (!aElement->HasServoData()) {
+  if (nsIPresShell* shell = aContent->OwnerDoc()->GetShell()) {
+    shell->EnsureStyleFlush();
+  }
+
+  Element* parent = aContent->GetFlattenedTreeParentElementForStyle();
+  if (!parent || !parent->HasServoData()) {
     // The bits only apply to styled elements.
     return;
   }
 
-  Element* curr = aElement;
+  Element* curr = parent;
   while (curr && !Traits::HasBit(curr)) {
     Traits::SetBit(curr);
     curr = curr->GetFlattenedTreeParentElementForStyle();
   }
 
-  if (nsIPresShell* shell = aElement->OwnerDoc()->GetShell()) {
-    shell->EnsureStyleFlush();
-  }
-
-  MOZ_ASSERT(BitIsPropagated<Traits>(aElement));
+  MOZ_ASSERT(BitIsPropagated<Traits>(parent));
 }
 
 void
-Element::NoteDirtyDescendantsForServo()
+nsIContent::NoteDirtyForServo()
 {
-  NoteDescendants<DirtyDescendantsBit>(this);
+  NoteDirtyContent<DirtyDescendantsBit>(this);
 }
 
 void
-Element::NoteAnimationOnlyDirtyDescendantsForServo()
+nsIContent::NoteAnimationOnlyDirtyForServo()
 {
-  NoteDescendants<AnimationOnlyDirtyDescendantsBit>(this);
+  NoteDirtyContent<AnimationOnlyDirtyDescendantsBit>(this);
 }
diff --git a/dom/base/Element.h b/dom/base/Element.h
--- a/dom/base/Element.h
+++ b/dom/base/Element.h
@@ -463,38 +463,32 @@ public:
       UpdateState(true);
     }
   }
 
   bool GetBindingURL(nsIDocument* aDocument, css::URLValue **aResult);
 
   Directionality GetComputedDirectionality() const;
 
-  inline Element* GetFlattenedTreeParentElement() const;
-  inline Element* GetFlattenedTreeParentElementForStyle() const;
-
   bool HasDirtyDescendantsForServo() const
   {
     MOZ_ASSERT(IsStyledByServo());
     return HasFlag(ELEMENT_HAS_DIRTY_DESCENDANTS_FOR_SERVO);
   }
 
   void SetHasDirtyDescendantsForServo() {
     MOZ_ASSERT(IsStyledByServo());
     SetFlags(ELEMENT_HAS_DIRTY_DESCENDANTS_FOR_SERVO);
   }
 
   void UnsetHasDirtyDescendantsForServo() {
     MOZ_ASSERT(IsStyledByServo());
     UnsetFlags(ELEMENT_HAS_DIRTY_DESCENDANTS_FOR_SERVO);
   }
 
-  void NoteDirtyDescendantsForServo();
-  void NoteAnimationOnlyDirtyDescendantsForServo();
-
   bool HasAnimationOnlyDirtyDescendantsForServo() const {
     MOZ_ASSERT(IsStyledByServo());
     return HasFlag(ELEMENT_HAS_ANIMATION_ONLY_DIRTY_DESCENDANTS_FOR_SERVO);
   }
 
   void SetHasAnimationOnlyDirtyDescendantsForServo() {
     MOZ_ASSERT(IsStyledByServo());
     SetFlags(ELEMENT_HAS_ANIMATION_ONLY_DIRTY_DESCENDANTS_FOR_SERVO);
diff --git a/dom/base/ElementInlines.h b/dom/base/ElementInlines.h
--- a/dom/base/ElementInlines.h
+++ b/dom/base/ElementInlines.h
@@ -24,34 +24,34 @@ Element::RegisterActivityObserver()
 }
 
 inline void
 Element::UnregisterActivityObserver()
 {
   OwnerDoc()->UnregisterActivityObserver(this);
 }
 
+} // namespace dom
+} // namespace mozilla
+
 inline Element*
-Element::GetFlattenedTreeParentElement() const
+nsINode::GetFlattenedTreeParentElement() const
 {
   nsINode* parentNode = GetFlattenedTreeParentNode();
   if MOZ_LIKELY(parentNode && parentNode->IsElement()) {
     return parentNode->AsElement();
   }
 
   return nullptr;
 }
 
 inline Element*
-Element::GetFlattenedTreeParentElementForStyle() const
+nsINode::GetFlattenedTreeParentElementForStyle() const
 {
   nsINode* parentNode = GetFlattenedTreeParentNodeForStyle();
   if MOZ_LIKELY(parentNode && parentNode->IsElement()) {
     return parentNode->AsElement();
   }
 
   return nullptr;
 }
 
-} // namespace dom
-} // namespace mozilla
-
 #endif // mozilla_dom_ElementInlines_h
diff --git a/dom/base/nsIContent.h b/dom/base/nsIContent.h
--- a/dom/base/nsIContent.h
+++ b/dom/base/nsIContent.h
@@ -974,16 +974,20 @@ public:
 
   virtual nsresult GetEventTargetParent(
                      mozilla::EventChainPreVisitor& aVisitor) override;
 
   virtual bool IsPurple() = 0;
   virtual void RemovePurple() = 0;
 
   virtual bool OwnedOnlyByTheDOMTree() { return false; }
+
+  void NoteDirtyForServo();
+  void NoteAnimationOnlyDirtyForServo();
+
 protected:
   /**
    * Hook for implementing GetID.  This is guaranteed to only be
    * called if HasID() is true.
    */
   nsIAtom* DoGetID() const;
 
   // Returns base URI without considering xml:base.
diff --git a/dom/base/nsINode.h b/dom/base/nsINode.h
--- a/dom/base/nsINode.h
+++ b/dom/base/nsINode.h
@@ -946,16 +946,19 @@ public:
   /**
    * Like GetFlattenedTreeParentNode, but returns null for any native
    * anonymous content that was generated for ancestor frames of the
    * root element's primary frame, such as scrollbar elements created
    * by the root scroll frame.
    */
   inline nsINode* GetFlattenedTreeParentNodeForStyle() const;
 
+  inline mozilla::dom::Element* GetFlattenedTreeParentElement() const;
+  inline mozilla::dom::Element* GetFlattenedTreeParentElementForStyle() const;
+
   /**
    * Get the parent nsINode for this node if it is an Element.
    * @return the parent node
    */
   mozilla::dom::Element* GetParentElement() const
   {
     return mParent && mParent->IsElement() ? mParent->AsElement() : nullptr;
   }
diff --git a/layout/base/ServoRestyleManager.cpp b/layout/base/ServoRestyleManager.cpp
--- a/layout/base/ServoRestyleManager.cpp
+++ b/layout/base/ServoRestyleManager.cpp
@@ -1222,19 +1222,17 @@ ServoRestyleManager::ContentStateChanged
       !StyleSet()->HasStateDependency(*aElement, aChangedBits)) {
     return;
   }
 
   ServoElementSnapshot& snapshot = SnapshotFor(aElement);
   EventStates previousState = aElement->StyleState() ^ aChangedBits;
   snapshot.AddState(previousState);
 
-  if (Element* parent = aElement->GetFlattenedTreeParentElementForStyle()) {
-    parent->NoteDirtyDescendantsForServo();
-  }
+  aElement->NoteDirtyForServo();
 
   if (restyleHint || changeHint) {
     Servo_NoteExplicitHints(aElement, restyleHint, changeHint);
   }
 
   // Assuming we need to invalidate cached style in getComputedStyle for
   // undisplayed elements, since we don't know if it is needed.
   IncrementUndisplayedRestyleGeneration();
@@ -1344,19 +1342,17 @@ ServoRestyleManager::TakeSnapshotForAttr
 
   ServoElementSnapshot& snapshot = SnapshotFor(aElement);
   snapshot.AddAttrs(aElement, aNameSpaceID, aAttribute);
 
   if (influencesOtherPseudoClassState) {
     snapshot.AddOtherPseudoClassState(aElement);
   }
 
-  if (Element* parent = aElement->GetFlattenedTreeParentElementForStyle()) {
-    parent->NoteDirtyDescendantsForServo();
-  }
+  aElement->NoteDirtyForServo();
 }
 
 // For some attribute changes we must restyle the whole subtree:
 //
 // * <td> is affected by the cellpadding on its ancestor table
 // * lang="" and xml:lang="" can affect all descendants due to :lang()
 //
 static inline bool
diff --git a/layout/base/nsCSSFrameConstructor.cpp b/layout/base/nsCSSFrameConstructor.cpp
--- a/layout/base/nsCSSFrameConstructor.cpp
+++ b/layout/base/nsCSSFrameConstructor.cpp
@@ -7547,20 +7547,17 @@ nsCSSFrameConstructor::MaybeRecreateForF
 }
 
 void
 nsCSSFrameConstructor::LazilyStyleNewChildRange(nsIContent* aStartChild,
                                                 nsIContent* aEndChild)
 {
   for (nsIContent* child = aStartChild; child != aEndChild;
        child = child->GetNextSibling()) {
-    nsINode* parent = child->GetFlattenedTreeParent();
-    if (MOZ_LIKELY(parent) && parent->IsElement()) {
-      parent->AsElement()->NoteDirtyDescendantsForServo();
-    }
+    child->NoteDirtyForServo();
   }
 }
 
 void
 nsCSSFrameConstructor::StyleNewChildRange(nsIContent* aStartChild,
                                           nsIContent* aEndChild)
 {
   ServoStyleSet* styleSet = mPresShell->StyleSet()->AsServo();
diff --git a/layout/style/ServoBindings.cpp b/layout/style/ServoBindings.cpp
--- a/layout/style/ServoBindings.cpp
+++ b/layout/style/ServoBindings.cpp
@@ -407,36 +407,27 @@ Gecko_SetNodeFlags(RawGeckoNodeBorrowed 
 
 void
 Gecko_UnsetNodeFlags(RawGeckoNodeBorrowed aNode, uint32_t aFlags)
 {
   const_cast<nsINode*>(aNode)->UnsetFlags(aFlags);
 }
 
 void
-Gecko_SetOwnerDocumentNeedsStyleFlush(RawGeckoElementBorrowed aElement)
+Gecko_NoteDirtyElement(RawGeckoElementBorrowed aElement)
 {
   MOZ_ASSERT(NS_IsMainThread());
-  if (nsIPresShell* shell = aElement->OwnerDoc()->GetShell()) {
-    shell->EnsureStyleFlush();
-  }
+  const_cast<Element*>(aElement)->NoteDirtyForServo();
 }
 
 void
-Gecko_NoteDirtyDescendants(RawGeckoElementBorrowed aElement)
+Gecko_NoteAnimationOnlyDirtyElement(RawGeckoElementBorrowed aElement)
 {
   MOZ_ASSERT(NS_IsMainThread());
-  const_cast<Element*>(aElement)->NoteDirtyDescendantsForServo();
-}
-
-void
-Gecko_NoteAnimationOnlyDirtyDescendants(RawGeckoElementBorrowed aElement)
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  const_cast<Element*>(aElement)->NoteAnimationOnlyDirtyDescendantsForServo();
+  const_cast<Element*>(aElement)->NoteAnimationOnlyDirtyForServo();
 }
 
 nsStyleContext*
 Gecko_GetStyleContext(RawGeckoElementBorrowed aElement,
                       nsIAtom* aPseudoTagOrNull)
 {
   nsIFrame* relevantFrame =
     ServoRestyleManager::FrameForPseudoElement(aElement, aPseudoTagOrNull);
diff --git a/layout/style/ServoBindings.h b/layout/style/ServoBindings.h
--- a/layout/style/ServoBindings.h
+++ b/layout/style/ServoBindings.h
@@ -387,19 +387,18 @@ void Gecko_SetContentDataImageValue(nsSt
                                     mozilla::css::ImageValue* aImageValue);
 nsStyleContentData::CounterFunction* Gecko_SetCounterFunction(
     nsStyleContentData* content_data, nsStyleContentType type);
 
 // Dirtiness tracking.
 uint32_t Gecko_GetNodeFlags(RawGeckoNodeBorrowed node);
 void Gecko_SetNodeFlags(RawGeckoNodeBorrowed node, uint32_t flags);
 void Gecko_UnsetNodeFlags(RawGeckoNodeBorrowed node, uint32_t flags);
-void Gecko_SetOwnerDocumentNeedsStyleFlush(RawGeckoElementBorrowed element);
-void Gecko_NoteDirtyDescendants(RawGeckoElementBorrowed element);
-void Gecko_NoteAnimationOnlyDirtyDescendants(RawGeckoElementBorrowed element);
+void Gecko_NoteDirtyElement(RawGeckoElementBorrowed element);
+void Gecko_NoteAnimationOnlyDirtyElement(RawGeckoElementBorrowed element);
 
 // Incremental restyle.
 // Also, we might want a ComputedValues to ComputedValues API for animations?
 // Not if we do them in Gecko...
 nsStyleContext* Gecko_GetStyleContext(RawGeckoElementBorrowed element,
                                       nsIAtom* aPseudoTagOrNull);
 mozilla::CSSPseudoElementType Gecko_GetImplementedPseudo(RawGeckoElementBorrowed element);
 nsChangeHint Gecko_CalcStyleDifference(ServoStyleContextBorrowed old_style,
