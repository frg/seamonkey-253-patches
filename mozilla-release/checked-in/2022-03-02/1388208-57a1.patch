# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1502152933 25200
#      Mon Aug 07 17:42:13 2017 -0700
# Node ID 903db0ac80c6a27b61b68e7df699782acc7f8842
# Parent  bc2706eb3c9f0e352a3ef0e5d08f8318d1aa280d
Bug 1388208: Stop using FileUtils.getFile. r=zombie

This performs main thread IO to make sure that directories exist, which is not
something we should be doing on the startup path.

MozReview-Commit-ID: 2NrgRgY5ua6

diff --git a/toolkit/components/extensions/ExtensionParent.jsm b/toolkit/components/extensions/ExtensionParent.jsm
--- a/toolkit/components/extensions/ExtensionParent.jsm
+++ b/toolkit/components/extensions/ExtensionParent.jsm
@@ -15,19 +15,19 @@ this.EXPORTED_SYMBOLS = ["ExtensionParen
 
 ChromeUtils.import("resource://gre/modules/Services.jsm");
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 XPCOMUtils.defineLazyModuleGetters(this, {
   AppConstants: "resource://gre/modules/AppConstants.jsm",
   DeferredSave: "resource://gre/modules/DeferredSave.jsm",
   E10SUtils: "resource://gre/modules/E10SUtils.jsm",
-  FileUtils: "resource://gre/modules/FileUtils.jsm",
   IndexedDB: "resource://gre/modules/IndexedDB.jsm",
   MessageChannel: "resource://gre/modules/MessageChannel.jsm",
+  OS: "resource://gre/modules/osfile.jsm",
   NativeApp: "resource://gre/modules/NativeMessaging.jsm",
   PrivateBrowsingUtils: "resource://gre/modules/PrivateBrowsingUtils.jsm",
   Schemas: "resource://gre/modules/Schemas.jsm",
 });
 
 XPCOMUtils.defineLazyServiceGetters(this, {
   gAddonPolicyService: ["@mozilla.org/addons/policy-service;1", "nsIAddonPolicyService"],
   aomStartup: ["@mozilla.org/addons/addon-manager-startup;1", "amIAddonManagerStartup"],
@@ -1373,23 +1373,26 @@ let IconDetails = {
   },
 };
 
 StartupCache = {
   DB_NAME: "ExtensionStartupCache",
 
   STORE_NAMES: Object.freeze(["general", "locales", "manifests", "other", "permissions", "schemas"]),
 
-  get file() {
-    return FileUtils.getFile("ProfLD", ["startupCache", "webext.sc.lz4"]);
-  },
+  file: OS.Path.join(OS.Constants.Path.localProfileDir, "startupCache", "webext.sc.lz4"),
 
   get saver() {
     if (!this._saver) {
-      this._saver = new DeferredSave(this.file.path,
+      OS.File.makeDir(OS.Path.dirname(this.file), {
+        ignoreExisting: true,
+        from: OS.Constants.Path.localProfileDir,
+      });
+
+      this._saver = new DeferredSave(this.file,
                                      () => this.getBlob(),
                                      {delay: 5000});
     }
     return this._saver;
   },
 
   async save() {
     return this.saver.saveChanges();
diff --git a/toolkit/components/extensions/ExtensionPermissions.jsm b/toolkit/components/extensions/ExtensionPermissions.jsm
--- a/toolkit/components/extensions/ExtensionPermissions.jsm
+++ b/toolkit/components/extensions/ExtensionPermissions.jsm
@@ -1,36 +1,36 @@
 "use strict";
 
 ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm");
 
 XPCOMUtils.defineLazyModuleGetters(this, {
   ExtensionParent: "resource://gre/modules/ExtensionParent.jsm",
   ExtensionUtils: "resource://gre/modules/ExtensionUtils.jsm",
-  FileUtils: "resource://gre/modules/FileUtils.jsm",
   JSONFile: "resource://gre/modules/JSONFile.jsm",
+  OS: "resource://gre/modules/osfile.jsm",
 });
 
 XPCOMUtils.defineLazyGetter(this, "StartupCache", () => ExtensionParent.StartupCache);
 
 this.EXPORTED_SYMBOLS = ["ExtensionPermissions"];
 
 const FILE_NAME = "extension-preferences.json";
 
 let prefs;
 let _initPromise;
 
 async function _lazyInit() {
-  let file = FileUtils.getFile("ProfD", [FILE_NAME]);
+  let path = OS.Path.join(OS.Constants.Path.profileDir, FILE_NAME);
 
-  prefs = new JSONFile({path: file.path});
+  prefs = new JSONFile({path});
   prefs.data = {};
 
   try {
-    let blob = await ExtensionUtils.promiseFileContents(file);
+    let blob = await ExtensionUtils.promiseFileContents(path);
     prefs.data = JSON.parse(new TextDecoder().decode(blob));
   } catch (e) {
     if (!e.becauseNoSuchFile) {
       Cu.reportError(e);
     }
   }
 }
 
diff --git a/toolkit/components/extensions/ExtensionUtils.jsm b/toolkit/components/extensions/ExtensionUtils.jsm
--- a/toolkit/components/extensions/ExtensionUtils.jsm
+++ b/toolkit/components/extensions/ExtensionUtils.jsm
@@ -31,18 +31,18 @@ const appinfo = Cc["@mozilla.org/xre/app
 
 let nextId = 0;
 XPCOMUtils.defineLazyGetter(this, "uniqueProcessID", () => appinfo.uniqueProcessID);
 
 function getUniqueId() {
   return `${nextId++}-${uniqueProcessID}`;
 }
 
-async function promiseFileContents(file) {
-  let res = await OS.File.read(file.path);
+async function promiseFileContents(path) {
+  let res = await OS.File.read(path);
   return res.buffer;
 }
 
 
 /**
  * An Error subclass for which complete error messages are always passed
  * to extensions, rather than being interpreted as an unknown error.
  */
