# HG changeset patch
# User Anthony Ramine <n.oxyde@gmail.com>
# Date 1503954551 18000
#      Mon Aug 28 16:09:11 2017 -0500
# Node ID 0097ec57473ba8cf654db9151edadf0a30027991
# Parent  f4da55f291cb485dcc2d5c638a5bb7826706b5f7
servo: Merge #18287 - Fix impl of ToComputedValue for TrackSize<T> (from servo:derive-all-the-things); r=emilio

Source-Repo: https://github.com/servo/servo
Source-Revision: ffc4ed187405f3fe654abeb682a61495f03cb687

diff --git a/servo/components/style/values/generics/grid.rs b/servo/components/style/values/generics/grid.rs
--- a/servo/components/style/values/generics/grid.rs
+++ b/servo/components/style/values/generics/grid.rs
@@ -5,17 +5,17 @@
 //! Generic types for the handling of
 //! [grids](https://drafts.csswg.org/css-grid/).
 
 use cssparser::Parser;
 use parser::{Parse, ParserContext};
 use std::{fmt, mem, usize};
 use style_traits::{ToCss, ParseError, StyleParseError};
 use values::{CSSFloat, CustomIdent, serialize_dimension};
-use values::computed::ComputedValueAsSpecified;
+use values::computed::{ComputedValueAsSpecified, Context, ToComputedValue};
 use values::specified::Integer;
 use values::specified::grid::parse_line_names;
 
 #[derive(Clone, Debug, PartialEq)]
 #[cfg_attr(feature = "servo", derive(HeapSizeOf))]
 /// A `<grid-line>` type.
 ///
 /// https://drafts.csswg.org/css-grid/#typedef-grid-row-start-grid-line
@@ -177,17 +177,17 @@ impl<L: ToCss> ToCss for TrackBreadth<L>
     }
 }
 
 /// A `<track-size>` type for explicit grid track sizing. Like `<track-breadth>`, this is
 /// generic only to avoid code bloat. It only takes `<length-percentage>`
 ///
 /// https://drafts.csswg.org/css-grid/#typedef-track-size
 #[cfg_attr(feature = "servo", derive(HeapSizeOf))]
-#[derive(Clone, Debug, HasViewportPercentage, PartialEq, ToComputedValue)]
+#[derive(Clone, Debug, HasViewportPercentage, PartialEq)]
 pub enum TrackSize<L> {
     /// A flexible `<track-breadth>`
     Breadth(TrackBreadth<L>),
     /// A `minmax` function for a range over an inflexible `<track-breadth>`
     /// and a flexible `<track-breadth>`
     ///
     /// https://drafts.csswg.org/css-grid/#valdef-grid-template-columns-minmax
     Minmax(TrackBreadth<L>, TrackBreadth<L>),
@@ -259,16 +259,67 @@ impl<L: ToCss> ToCss for TrackSize<L> {
                 dest.write_str("fit-content(")?;
                 lop.to_css(dest)?;
                 dest.write_str(")")
             },
         }
     }
 }
 
+impl<L: ToComputedValue> ToComputedValue for TrackSize<L> {
+    type ComputedValue = TrackSize<L::ComputedValue>;
+
+    #[inline]
+    fn to_computed_value(&self, context: &Context) -> Self::ComputedValue {
+        match *self {
+            TrackSize::Breadth(TrackBreadth::Flex(ref f)) => {
+                // <flex> outside `minmax()` expands to `mimmax(auto, <flex>)`
+                // https://drafts.csswg.org/css-grid/#valdef-grid-template-columns-flex
+                // FIXME(nox): This sounds false, the spec just says that <flex>
+                // implies `minmax(auto, <flex>)`, not that it should be changed
+                // into `minmax` at computed value time.
+                TrackSize::Minmax(
+                    TrackBreadth::Keyword(TrackKeyword::Auto),
+                    TrackBreadth::Flex(f.to_computed_value(context)),
+                )
+            },
+            TrackSize::Breadth(ref b) => {
+                TrackSize::Breadth(b.to_computed_value(context))
+            },
+            TrackSize::Minmax(ref b1, ref b2) => {
+                TrackSize::Minmax(
+                    b1.to_computed_value(context),
+                    b2.to_computed_value(context),
+                )
+            }
+            TrackSize::FitContent(ref lop) => {
+                TrackSize::FitContent(lop.to_computed_value(context))
+            },
+        }
+    }
+
+    #[inline]
+    fn from_computed_value(computed: &Self::ComputedValue) -> Self {
+        match *computed {
+            TrackSize::Breadth(ref b) => {
+                TrackSize::Breadth(ToComputedValue::from_computed_value(b))
+            },
+            TrackSize::Minmax(ref b1, ref b2) => {
+                TrackSize::Minmax(
+                    ToComputedValue::from_computed_value(b1),
+                    ToComputedValue::from_computed_value(b2),
+                )
+            },
+            TrackSize::FitContent(ref lop) => {
+                TrackSize::FitContent(ToComputedValue::from_computed_value(lop))
+            },
+        }
+    }
+}
+
 /// Helper function for serializing identifiers with a prefix and suffix, used
 /// for serializing <line-names> (in grid).
 pub fn concat_serialize_idents<W>(prefix: &str, suffix: &str,
                                   slice: &[CustomIdent], sep: &str, dest: &mut W) -> fmt::Result
     where W: fmt::Write
 {
     if let Some((ref first, rest)) = slice.split_first() {
         dest.write_str(prefix)?;
