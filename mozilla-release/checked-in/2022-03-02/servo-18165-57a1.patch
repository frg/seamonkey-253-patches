# HG changeset patch
# User Brian Birtles <birtles@gmail.com>
# Date 1503288357 18000
#      Sun Aug 20 23:05:57 2017 -0500
# Node ID 3a458c0fd90c4bdb3754c65d5aa2b5d3fb5a8c03
# Parent  e72041274e6ee63a3daa5bf2fa1b3f67c7be1f75
servo: Merge #18165 - Expand var() references in single_value_to_css (from birtles:expand-variables-in-serialize); r=hiro

These are the Servo-side changes for [Gecko bug 1385139](https://bugzilla.mozilla.org/show_bug.cgi?id=1385139).

This is a temporary step needed to support Gecko's getKeyframes() API until we implement bug 1391537. It only takes effect when a ComputedValues object is supplied and only for longhand declarations.

Source-Repo: https://github.com/servo/servo
Source-Revision: 9bb21e6cab637380a544f941ab742dd47b8133b4

diff --git a/servo/components/style/properties/declaration_block.rs b/servo/components/style/properties/declaration_block.rs
--- a/servo/components/style/properties/declaration_block.rs
+++ b/servo/components/style/properties/declaration_block.rs
@@ -513,23 +513,45 @@ impl PropertyDeclarationBlock {
 
         if let PropertyId::Longhand(_) = *property {
             debug_assert!(removed_at_least_one);
         }
         removed_at_least_one
     }
 
     /// Take a declaration block known to contain a single property and serialize it.
-    pub fn single_value_to_css<W>(&self, property: &PropertyId, dest: &mut W) -> fmt::Result
-        where W: fmt::Write,
+    pub fn single_value_to_css<W>(
+        &self,
+        property: &PropertyId,
+        dest: &mut W,
+        computed_values: Option<&ComputedValues>,
+    ) -> fmt::Result
+    where
+        W: fmt::Write,
     {
         match property.as_shorthand() {
             Err(_longhand_or_custom) => {
                 if self.declarations.len() == 1 {
-                    self.declarations[0].0.to_css(dest)
+                    let declaration = &self.declarations[0].0;
+                    // If we have a longhand declaration with variables, those variables will be
+                    // stored as unparsed values. As a temporary measure to produce sensible results
+                    // in Gecko's getKeyframes() implementation for CSS animations, if
+                    // |computed_values| is supplied, we use it to expand such variable
+                    // declarations. This will be fixed properly in Gecko bug 1391537.
+                    match (declaration, computed_values) {
+                        (&PropertyDeclaration::WithVariables(id, ref unparsed),
+                         Some(ref computed_values)) => unparsed
+                            .substitute_variables(
+                                id,
+                                &computed_values.custom_properties(),
+                                QuirksMode::NoQuirks,
+                            )
+                            .to_css(dest),
+                        (ref d, _) => d.to_css(dest),
+                    }
                 } else {
                     Err(fmt::Error)
                 }
             }
             Ok(shorthand) => {
                 if !self.declarations.iter().all(|decl| decl.0.shorthands().contains(&shorthand)) {
                     return Err(fmt::Error)
                 }
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -559,17 +559,18 @@ macro_rules! get_property_id_from_nscssp
 #[no_mangle]
 pub extern "C" fn Servo_AnimationValue_Serialize(value: RawServoAnimationValueBorrowed,
                                                  property: nsCSSPropertyID,
                                                  buffer: *mut nsAString)
 {
     let uncomputed_value = AnimationValue::as_arc(&value).uncompute();
     let mut string = String::new();
     let rv = PropertyDeclarationBlock::with_one(uncomputed_value, Importance::Normal)
-        .single_value_to_css(&get_property_id_from_nscsspropertyid!(property, ()), &mut string);
+        .single_value_to_css(&get_property_id_from_nscsspropertyid!(property, ()), &mut string,
+                             None);
     debug_assert!(rv.is_ok());
 
     let buffer = unsafe { buffer.as_mut().unwrap() };
     buffer.assign_utf8(&string);
 }
 
 #[no_mangle]
 pub extern "C" fn Servo_Shorthand_AnimationValues_Serialize(shorthand_property: nsCSSPropertyID,
@@ -2137,22 +2138,23 @@ pub extern "C" fn Servo_DeclarationBlock
     read_locked_arc(declarations, |decls: &PropertyDeclarationBlock| {
         decls.to_css(unsafe { result.as_mut().unwrap() }).unwrap();
     })
 }
 
 #[no_mangle]
 pub extern "C" fn Servo_DeclarationBlock_SerializeOneValue(
     declarations: RawServoDeclarationBlockBorrowed,
-    property_id: nsCSSPropertyID, buffer: *mut nsAString)
+    property_id: nsCSSPropertyID, buffer: *mut nsAString,
+    computed_values: ServoStyleContextBorrowedOrNull)
 {
     let property_id = get_property_id_from_nscsspropertyid!(property_id, ());
     read_locked_arc(declarations, |decls: &PropertyDeclarationBlock| {
         let mut string = String::new();
-        let rv = decls.single_value_to_css(&property_id, &mut string);
+        let rv = decls.single_value_to_css(&property_id, &mut string, computed_values);
         debug_assert!(rv.is_ok());
 
         let buffer = unsafe { buffer.as_mut().unwrap() };
         buffer.assign_utf8(&string);
     })
 }
 
 #[no_mangle]
