# HG changeset patch
# User Dustin J. Mitchell <dustin@mozilla.com>
# Date 1516141988 0
# Node ID ce4e64aa7ea0452877bfbd597a5db413e476c1c2
# Parent  3721027ad10fccd89df9d666523bb7f5eaa99048
Bug 1403519: reset SCHEDULES.exclusive if set multiple times; r=gps

MozReview-Commit-ID: Kycd9i5f19P

diff --git a/python/mozbuild/mozbuild/frontend/context.py b/python/mozbuild/mozbuild/frontend/context.py
--- a/python/mozbuild/mozbuild/frontend/context.py
+++ b/python/mozbuild/mozbuild/frontend/context.py
@@ -849,20 +849,19 @@ class Schedules(object):
         rv._inclusive = self._inclusive + other._inclusive
         if other._exclusive == self._exclusive:
             rv._exclusive = self._exclusive
         elif self._exclusive == schedules.EXCLUSIVE_COMPONENTS:
             rv._exclusive = other._exclusive
         elif other._exclusive == schedules.EXCLUSIVE_COMPONENTS:
             rv._exclusive = self._exclusive
         else:
-            msg = 'Two Files sections have set SCHEDULES.exclusive to different' \
-                'values; these cannot be combined: {} and {}'
-            msg = msg.format(self._exclusive, other._exclusive)
-            raise ValueError(msg)
+            # in a case where two SCHEDULES.exclusive set different values, take
+            # the later one; this acts the way we expect assignment to work.
+            rv._exclusive = other._exclusive
         return rv
 
 
 @memoize
 def ContextDerivedTypedHierarchicalStringList(type):
     """Specialized HierarchicalStringList for use with ContextDerivedValue
     types."""
     class _TypedListWithItems(ContextDerivedValue, HierarchicalStringList):
diff --git a/python/mozbuild/mozbuild/test/frontend/data/schedules/moz.build b/python/mozbuild/mozbuild/test/frontend/data/schedules/moz.build
--- a/python/mozbuild/mozbuild/test/frontend/data/schedules/moz.build
+++ b/python/mozbuild/mozbuild/test/frontend/data/schedules/moz.build
@@ -2,17 +2,17 @@
 # http://creativecommons.org/publicdomain/zero/1.0/
 
 with Files('*.win'):
     SCHEDULES.exclusive = ['windows']
 
 with Files('*.osx'):
     SCHEDULES.exclusive = ['macosx']
 
-with Files('bad.osx'):
+with Files('win.and.osx'):
     # this conflicts with the previous clause and will cause an error
     # when read
     SCHEDULES.exclusive = ['macosx', 'windows']
 
 with Files('subd/**.py'):
     SCHEDULES.inclusive += ['py-lint']
 
 with Files('**/*.js'):
diff --git a/python/mozbuild/mozbuild/test/frontend/test_reader.py b/python/mozbuild/mozbuild/test/frontend/test_reader.py
--- a/python/mozbuild/mozbuild/test/frontend/test_reader.py
+++ b/python/mozbuild/mozbuild/test/frontend/test_reader.py
@@ -478,16 +478,17 @@ class TestBuildReader(unittest.TestCase)
         reader = self.reader('invalid-files-flavor')
 
         with self.assertRaises(BuildReaderError):
             reader.files_info(['foo.js'])
 
     def test_schedules(self):
         reader = self.reader('schedules')
         info = reader.files_info([
+            'win.and.osx',
             'somefile',
             'foo.win',
             'foo.osx',
             'subd/aa.py',
             'subd/yaml.py',
             'subd/win.js',
         ])
         # default: all exclusive, no inclusive
@@ -507,18 +508,15 @@ class TestBuildReader(unittest.TestCase)
         self.assertEqual(info['subd/yaml.py']['SCHEDULES'].exclusive, ['android', 'linux', 'macosx', 'windows'])
         # .. but exlusive does not override inclusive
         self.assertEqual(info['subd/win.js']['SCHEDULES'].inclusive, ['js-lint'])
         self.assertEqual(info['subd/win.js']['SCHEDULES'].exclusive, ['windows'])
 
         self.assertEqual(info['subd/yaml.py']['SCHEDULES'].components,
                 ['android', 'linux', 'macosx', 'windows', 'py-lint', 'yaml-lint'])
 
-    def test_schedules_conflicting_excludes(self):
-        reader = self.reader('schedules')
-
-        # bad.osx is defined explicitly, and matches *.osx, and the two have
-        # conflicting SCHEDULES.exclusive settings
-        with self.assertRaisesRegexp(ValueError, r"Two Files sections"):
-            reader.files_info(['bad.osx'])
+        # win.and.osx is defined explicitly, and matches *.osx, and the two have
+        # conflicting SCHEDULES.exclusive settings, so the later one is used
+        self.assertEqual(set(info['win.and.osx']['SCHEDULES'].exclusive),
+                         set(['macosx', 'windows']))
 
 if __name__ == '__main__':
     main()
