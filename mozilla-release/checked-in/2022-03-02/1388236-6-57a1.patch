# HG changeset patch
# User Matthew Gregan <kinetik@flim.org>
# Date 1502326809 -43200
# Node ID bbedde5d27b4a025e0fd01eeced352511d99367c
# Parent  9cabb3951e654a31b1751870d3b0d51e2d20e1f4
Bug 1388236 - Remove B2G-only AudioChannel code from dom/audiochannel.  r=alwu

diff --git a/dom/audiochannel/AudioChannelAgent.cpp b/dom/audiochannel/AudioChannelAgent.cpp
--- a/dom/audiochannel/AudioChannelAgent.cpp
+++ b/dom/audiochannel/AudioChannelAgent.cpp
@@ -32,18 +32,17 @@ NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(
   NS_INTERFACE_MAP_ENTRY(nsIAudioChannelAgent)
   NS_INTERFACE_MAP_ENTRY(nsISupports)
 NS_INTERFACE_MAP_END
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(AudioChannelAgent)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(AudioChannelAgent)
 
 AudioChannelAgent::AudioChannelAgent()
-  : mAudioChannelType(AUDIO_AGENT_CHANNEL_ERROR)
-  , mInnerWindowID(0)
+  : mInnerWindowID(0)
   , mIsRegToService(false)
 {
   // Init service in the begining, it can help us to know whether there is any
   // created media component via AudioChannelService::IsServiceStarted().
   RefPtr<AudioChannelService> service = AudioChannelService::GetOrCreate();
 }
 
 AudioChannelAgent::~AudioChannelAgent()
@@ -54,36 +53,29 @@ AudioChannelAgent::~AudioChannelAgent()
 void
 AudioChannelAgent::Shutdown()
 {
   if (mIsRegToService) {
     NotifyStoppedPlaying();
   }
 }
 
-NS_IMETHODIMP AudioChannelAgent::GetAudioChannelType(int32_t *aAudioChannelType)
-{
-  *aAudioChannelType = mAudioChannelType;
-  return NS_OK;
-}
-
 NS_IMETHODIMP
-AudioChannelAgent::Init(mozIDOMWindow* aWindow, int32_t aChannelType,
+AudioChannelAgent::Init(mozIDOMWindow* aWindow,
                         nsIAudioChannelAgentCallback *aCallback)
 {
-  return InitInternal(nsPIDOMWindowInner::From(aWindow), aChannelType,
+  return InitInternal(nsPIDOMWindowInner::From(aWindow),
                       aCallback, /* useWeakRef = */ false);
 }
 
 NS_IMETHODIMP
 AudioChannelAgent::InitWithWeakCallback(mozIDOMWindow* aWindow,
-                                        int32_t aChannelType,
                                         nsIAudioChannelAgentCallback *aCallback)
 {
-  return InitInternal(nsPIDOMWindowInner::From(aWindow), aChannelType,
+  return InitInternal(nsPIDOMWindowInner::From(aWindow),
                       aCallback, /* useWeakRef = */ true);
 }
 
 nsresult
 AudioChannelAgent::FindCorrectWindow(nsPIDOMWindowInner* aWindow)
 {
   MOZ_ASSERT(aWindow->IsInnerWindow());
 
@@ -138,106 +130,82 @@ AudioChannelAgent::FindCorrectWindow(nsP
     }
   }
 
   return FindCorrectWindow(parent);
 }
 
 nsresult
 AudioChannelAgent::InitInternal(nsPIDOMWindowInner* aWindow,
-                                int32_t aChannelType,
                                 nsIAudioChannelAgentCallback *aCallback,
                                 bool aUseWeakRef)
 {
-  // We syncd the enum of channel type between nsIAudioChannelAgent.idl and
-  // AudioChannelBinding.h the same.
-  MOZ_ASSERT(int(AUDIO_AGENT_CHANNEL_NORMAL) == int(AudioChannel::Normal) &&
-             int(AUDIO_AGENT_CHANNEL_CONTENT) == int(AudioChannel::Content) &&
-             int(AUDIO_AGENT_CHANNEL_NOTIFICATION) == int(AudioChannel::Notification) &&
-             int(AUDIO_AGENT_CHANNEL_ALARM) == int(AudioChannel::Alarm) &&
-             int(AUDIO_AGENT_CHANNEL_TELEPHONY) == int(AudioChannel::Telephony) &&
-             int(AUDIO_AGENT_CHANNEL_RINGER) == int(AudioChannel::Ringer) &&
-             int(AUDIO_AGENT_CHANNEL_SYSTEM) == int(AudioChannel::System) &&
-             int(AUDIO_AGENT_CHANNEL_PUBLICNOTIFICATION) == int(AudioChannel::Publicnotification),
-             "Enum of channel on nsIAudioChannelAgent.idl should be the same with AudioChannelBinding.h");
-
-  if (mAudioChannelType != AUDIO_AGENT_CHANNEL_ERROR ||
-      aChannelType > AUDIO_AGENT_CHANNEL_SYSTEM ||
-      aChannelType < AUDIO_AGENT_CHANNEL_NORMAL) {
-    return NS_ERROR_FAILURE;
-  }
-
   if (NS_WARN_IF(!aWindow)) {
     return NS_ERROR_FAILURE;
   }
 
   MOZ_ASSERT(aWindow->IsInnerWindow());
   mInnerWindowID = aWindow->WindowID();
 
   nsresult rv = FindCorrectWindow(aWindow);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
-  mAudioChannelType = aChannelType;
-
   if (aUseWeakRef) {
     mWeakCallback = do_GetWeakReference(aCallback);
   } else {
     mCallback = aCallback;
   }
 
   MOZ_LOG(AudioChannelService::GetAudioChannelLog(), LogLevel::Debug,
-         ("AudioChannelAgent, InitInternal, this = %p, type = %d, "
-          "owner = %p, hasCallback = %d\n", this, mAudioChannelType,
+         ("AudioChannelAgent, InitInternal, this = %p, "
+          "owner = %p, hasCallback = %d\n", this,
           mWindow.get(), (!!mCallback || !!mWeakCallback)));
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
 AudioChannelAgent::NotifyStartedPlaying(AudioPlaybackConfig* aConfig,
                                         uint8_t aAudible)
 {
   if (NS_WARN_IF(!aConfig)) {
     return NS_ERROR_FAILURE;
   }
 
   RefPtr<AudioChannelService> service = AudioChannelService::GetOrCreate();
-  if (mAudioChannelType == AUDIO_AGENT_CHANNEL_ERROR ||
-      service == nullptr || mIsRegToService) {
+  if (service == nullptr || mIsRegToService) {
     return NS_ERROR_FAILURE;
   }
 
   MOZ_ASSERT(AudioChannelService::AudibleState::eNotAudible == 0 &&
              AudioChannelService::AudibleState::eMaybeAudible == 1 &&
              AudioChannelService::AudibleState::eAudible == 2);
   service->RegisterAudioChannelAgent(this,
     static_cast<AudioChannelService::AudibleState>(aAudible));
 
-  AudioPlaybackConfig config = service->GetMediaConfig(mWindow,
-                                                       mAudioChannelType);
+  AudioPlaybackConfig config = service->GetMediaConfig(mWindow);
 
   MOZ_LOG(AudioChannelService::GetAudioChannelLog(), LogLevel::Debug,
          ("AudioChannelAgent, NotifyStartedPlaying, this = %p, "
           "audible = %s, mute = %s, volume = %f, suspend = %s\n", this,
           AudibleStateToStr(static_cast<AudioChannelService::AudibleState>(aAudible)),
           config.mMuted ? "true" : "false", config.mVolume,
           SuspendTypeToStr(config.mSuspend)));
 
   aConfig->SetConfig(config.mVolume, config.mMuted, config.mSuspend);
   mIsRegToService = true;
   return NS_OK;
 }
 
 NS_IMETHODIMP
 AudioChannelAgent::NotifyStoppedPlaying()
 {
-  if (mAudioChannelType == AUDIO_AGENT_CHANNEL_ERROR ||
-      !mIsRegToService) {
+  if (!mIsRegToService) {
     return NS_ERROR_FAILURE;
   }
 
   MOZ_LOG(AudioChannelService::GetAudioChannelLog(), LogLevel::Debug,
          ("AudioChannelAgent, NotifyStoppedPlaying, this = %p\n", this));
 
   RefPtr<AudioChannelService> service = AudioChannelService::GetOrCreate();
   if (service) {
@@ -316,17 +284,17 @@ AudioChannelAgent::WindowSuspendChanged(
 }
 
 AudioPlaybackConfig
 AudioChannelAgent::GetMediaConfig()
 {
   RefPtr<AudioChannelService> service = AudioChannelService::GetOrCreate();
   AudioPlaybackConfig config(1.0, false, nsISuspendedTypes::NONE_SUSPENDED);
   if (service) {
-    config = service->GetMediaConfig(mWindow, mAudioChannelType);
+    config = service->GetMediaConfig(mWindow);
   }
   return config;
 }
 
 bool
 AudioChannelAgent::IsDisposableSuspend(nsSuspendedTypes aSuspend) const
 {
   return (aSuspend == nsISuspendedTypes::SUSPENDED_PAUSE_DISPOSABLE ||
diff --git a/dom/audiochannel/AudioChannelAgent.h b/dom/audiochannel/AudioChannelAgent.h
--- a/dom/audiochannel/AudioChannelAgent.h
+++ b/dom/audiochannel/AudioChannelAgent.h
@@ -56,30 +56,29 @@ private:
 
   AudioPlaybackConfig GetMediaConfig();
   bool IsDisposableSuspend(nsSuspendedTypes aSuspend) const;
 
   // Returns mCallback if that's non-null, or otherwise tries to get an
   // nsIAudioChannelAgentCallback out of mWeakCallback.
   already_AddRefed<nsIAudioChannelAgentCallback> GetCallback();
 
-  nsresult InitInternal(nsPIDOMWindowInner* aWindow, int32_t aAudioAgentType,
+  nsresult InitInternal(nsPIDOMWindowInner* aWindow,
                         nsIAudioChannelAgentCallback* aCallback,
                         bool aUseWeakRef);
 
   void Shutdown();
 
   nsresult FindCorrectWindow(nsPIDOMWindowInner* aWindow);
 
   nsCOMPtr<nsPIDOMWindowOuter> mWindow;
   nsCOMPtr<nsIAudioChannelAgentCallback> mCallback;
 
   nsWeakPtr mWeakCallback;
 
-  int32_t mAudioChannelType;
   uint64_t mInnerWindowID;
   bool mIsRegToService;
 };
 
 } // namespace dom
 } // namespace mozilla
 
 
diff --git a/dom/audiochannel/AudioChannelService.cpp b/dom/audiochannel/AudioChannelService.cpp
--- a/dom/audiochannel/AudioChannelService.cpp
+++ b/dom/audiochannel/AudioChannelService.cpp
@@ -212,29 +212,16 @@ AudibleChangedReasonToStr(const AudioCha
       return "pause-state";
     default:
       return "unknown";
   }
 }
 
 StaticRefPtr<AudioChannelService> gAudioChannelService;
 
-// Mappings from 'mozaudiochannel' attribute strings to an enumeration.
-static const nsAttrValue::EnumTable kMozAudioChannelAttributeTable[] = {
-  { "normal",             (int16_t)AudioChannel::Normal },
-  { "content",            (int16_t)AudioChannel::Content },
-  { "notification",       (int16_t)AudioChannel::Notification },
-  { "alarm",              (int16_t)AudioChannel::Alarm },
-  { "telephony",          (int16_t)AudioChannel::Telephony },
-  { "ringer",             (int16_t)AudioChannel::Ringer },
-  { "publicnotification", (int16_t)AudioChannel::Publicnotification },
-  { "system",             (int16_t)AudioChannel::System },
-  { nullptr,              0 }
-};
-
 /* static */ void
 AudioChannelService::CreateServiceIfNeeded()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   if (!gAudioChannelService) {
     gAudioChannelService = new AudioChannelService();
   }
@@ -351,21 +338,19 @@ AudioChannelService::UnregisterAudioChan
   // To make sure agent would be alive because AppendAgent() would trigger the
   // callback function of AudioChannelAgentOwner that means the agent might be
   // released in their callback.
   RefPtr<AudioChannelAgent> kungFuDeathGrip(aAgent);
   winData->RemoveAgent(aAgent);
 }
 
 AudioPlaybackConfig
-AudioChannelService::GetMediaConfig(nsPIDOMWindowOuter* aWindow,
-                                    uint32_t aAudioChannel) const
+AudioChannelService::GetMediaConfig(nsPIDOMWindowOuter* aWindow) const
 {
   MOZ_ASSERT(!aWindow || aWindow->IsOuterWindow());
-  MOZ_ASSERT(aAudioChannel < NUMBER_OF_AUDIO_CHANNELS);
 
   AudioPlaybackConfig config(1.0, false,
                              nsISuspendedTypes::NONE_SUSPENDED);
 
   if (!aWindow || !aWindow->IsOuterWindow()) {
     config.SetConfig(0.0, true,
                      nsISuspendedTypes::SUSPENDED_BLOCK);
     return config;
@@ -374,18 +359,18 @@ AudioChannelService::GetMediaConfig(nsPI
   AudioChannelWindow* winData = nullptr;
   nsCOMPtr<nsPIDOMWindowOuter> window = aWindow;
 
   // The volume must be calculated based on the window hierarchy. Here we go up
   // to the top window and we calculate the volume and the muted flag.
   do {
     winData = GetWindowData(window->WindowID());
     if (winData) {
-      config.mVolume *= winData->mChannels[aAudioChannel].mVolume;
-      config.mMuted = config.mMuted || winData->mChannels[aAudioChannel].mMuted;
+      config.mVolume *= winData->mChannelConfig.mVolume;
+      config.mMuted = config.mMuted || winData->mChannelConfig.mMuted;
       config.mSuspend = winData->mOwningAudioFocus ?
         config.mSuspend : nsISuspendedTypes::SUSPENDED_STOP_DISPOSABLE;
     }
 
     config.mVolume *= window->GetAudioVolume();
     config.mMuted = config.mMuted || window->GetAudioMuted();
     if (window->GetMediaSuspend() != nsISuspendedTypes::NONE_SUSPENDED) {
       config.mSuspend = window->GetMediaSuspend();
@@ -537,52 +522,16 @@ AudioChannelService::SetWindowAudioCaptu
     nsTObserverArray<AudioChannelAgent*>::ForwardIterator
       iter(winData->mAgents);
     while (iter.HasMore()) {
       iter.GetNext()->WindowAudioCaptureChanged(aInnerWindowID, aCapture);
     }
   }
 }
 
-/* static */ const nsAttrValue::EnumTable*
-AudioChannelService::GetAudioChannelTable()
-{
-  return kMozAudioChannelAttributeTable;
-}
-
-/* static */ AudioChannel
-AudioChannelService::GetAudioChannel(const nsAString& aChannel)
-{
-  for (uint32_t i = 0; kMozAudioChannelAttributeTable[i].tag; ++i) {
-    if (aChannel.EqualsASCII(kMozAudioChannelAttributeTable[i].tag)) {
-      return static_cast<AudioChannel>(kMozAudioChannelAttributeTable[i].value);
-    }
-  }
-
-  return AudioChannel::Normal;
-}
-
-/* static */ AudioChannel
-AudioChannelService::GetDefaultAudioChannel()
-{
-  nsAutoString audioChannel;
-  Preferences::GetString("media.defaultAudioChannel", audioChannel);
-  if (audioChannel.IsEmpty()) {
-    return AudioChannel::Normal;
-  }
-
-  for (uint32_t i = 0; kMozAudioChannelAttributeTable[i].tag; ++i) {
-    if (audioChannel.EqualsASCII(kMozAudioChannelAttributeTable[i].tag)) {
-      return static_cast<AudioChannel>(kMozAudioChannelAttributeTable[i].value);
-    }
-  }
-
-  return AudioChannel::Normal;
-}
-
 AudioChannelService::AudioChannelWindow*
 AudioChannelService::GetOrCreateWindowData(nsPIDOMWindowOuter* aWindow)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(aWindow);
   MOZ_ASSERT(aWindow->IsOuterWindow());
 
   AudioChannelWindow* winData = GetWindowData(aWindow->WindowID());
@@ -764,18 +713,17 @@ AudioChannelService::AudioChannelWindow:
       AudioChannelAgent* agent = iter.GetNext();
       MOZ_ASSERT(agent);
 
       // Don't need to update the playing state of new playing agent.
       if (agent == aNewPlayingAgent) {
         continue;
       }
 
-      uint32_t type = GetCompetingBehavior(agent,
-                                           aNewPlayingAgent->AudioChannelType());
+      uint32_t type = GetCompetingBehavior(agent);
 
       // If window will be suspended, it needs to abandon the audio focus
       // because only one window can own audio focus at a time. However, we
       // would support multiple audio focus at the same time in the future.
       mOwningAudioFocus = (type == nsISuspendedTypes::NONE_SUSPENDED);
 
       // TODO : support other behaviors which are definded in MediaSession API.
       switch (type) {
@@ -794,37 +742,28 @@ AudioChannelService::AudioChannelWindow:
 
 bool
 AudioChannelService::AudioChannelWindow::IsContainingPlayingAgent(AudioChannelAgent* aAgent) const
 {
   return (aAgent->WindowID() == mWindowID);
 }
 
 uint32_t
-AudioChannelService::AudioChannelWindow::GetCompetingBehavior(AudioChannelAgent* aAgent,
-                                                              int32_t aIncomingChannelType) const
+AudioChannelService::AudioChannelWindow::GetCompetingBehavior(AudioChannelAgent* aAgent) const
 {
   MOZ_ASSERT(aAgent);
   MOZ_ASSERT(IsEnableAudioCompetingForAllAgents() ?
     mAgents.Contains(aAgent) : mAudibleAgents.Contains(aAgent));
 
-  uint32_t competingBehavior = nsISuspendedTypes::NONE_SUSPENDED;
-  int32_t presentChannelType = aAgent->AudioChannelType();
-
-  // TODO : add other competing cases for MediaSession API
-  if (presentChannelType == int32_t(AudioChannel::Normal) &&
-      aIncomingChannelType == int32_t(AudioChannel::Normal)) {
-    competingBehavior = nsISuspendedTypes::SUSPENDED_STOP_DISPOSABLE;
-  }
+  uint32_t competingBehavior = nsISuspendedTypes::SUSPENDED_STOP_DISPOSABLE;
 
   MOZ_LOG(AudioChannelService::GetAudioChannelLog(), LogLevel::Debug,
          ("AudioChannelWindow, GetCompetingBehavior, this = %p, "
-          "present type = %d, incoming channel = %d, behavior = %s\n",
-          this, presentChannelType, aIncomingChannelType,
-          SuspendTypeToStr(competingBehavior)));
+          "behavior = %s\n",
+          this, SuspendTypeToStr(competingBehavior)));
 
   return competingBehavior;
 }
 
 void
 AudioChannelService::AudioChannelWindow::AppendAgent(AudioChannelAgent* aAgent,
                                                      AudibleState aAudible)
 {
@@ -895,40 +834,38 @@ AudioChannelService::AudioChannelWindow:
 }
 
 void
 AudioChannelService::AudioChannelWindow::AppendAgentAndIncreaseAgentsNum(AudioChannelAgent* aAgent)
 {
   MOZ_ASSERT(aAgent);
   MOZ_ASSERT(!mAgents.Contains(aAgent));
 
-  int32_t channel = aAgent->AudioChannelType();
   mAgents.AppendElement(aAgent);
 
-  ++mChannels[channel].mNumberOfAgents;
+  ++mChannelConfig.mNumberOfAgents;
 
   // TODO: Make NotifyChannelActiveRunnable irrelevant to BrowserElementAudioChannel
-  if (mChannels[channel].mNumberOfAgents == 1) {
+  if (mChannelConfig.mNumberOfAgents == 1) {
     NotifyChannelActive(aAgent->WindowID(), true);
   }
 }
 
 void
 AudioChannelService::AudioChannelWindow::RemoveAgentAndReduceAgentsNum(AudioChannelAgent* aAgent)
 {
   MOZ_ASSERT(aAgent);
   MOZ_ASSERT(mAgents.Contains(aAgent));
 
-  int32_t channel = aAgent->AudioChannelType();
   mAgents.RemoveElement(aAgent);
 
-  MOZ_ASSERT(mChannels[channel].mNumberOfAgents > 0);
-  --mChannels[channel].mNumberOfAgents;
+  MOZ_ASSERT(mChannelConfig.mNumberOfAgents > 0);
+  --mChannelConfig.mNumberOfAgents;
 
-  if (mChannels[channel].mNumberOfAgents == 0) {
+  if (mChannelConfig.mNumberOfAgents == 0) {
     NotifyChannelActive(aAgent->WindowID(), false);
   }
 }
 
 void
 AudioChannelService::AudioChannelWindow::AudioCapturedChanged(AudioChannelAgent* aAgent,
                                                               AudioCaptureState aCapture)
 {
diff --git a/dom/audiochannel/AudioChannelService.h b/dom/audiochannel/AudioChannelService.h
--- a/dom/audiochannel/AudioChannelService.h
+++ b/dom/audiochannel/AudioChannelService.h
@@ -9,29 +9,26 @@
 
 #include "nsAutoPtr.h"
 #include "nsIObserver.h"
 #include "nsTObserverArray.h"
 #include "nsTArray.h"
 
 #include "AudioChannelAgent.h"
 #include "nsAttrValue.h"
-#include "mozilla/dom/AudioChannelBinding.h"
 #include "mozilla/Logging.h"
 
 #include <functional>
 
 class nsPIDOMWindowOuter;
 struct PRLogModuleInfo;
 
 namespace mozilla {
 namespace dom {
 
-#define NUMBER_OF_AUDIO_CHANNELS (uint32_t)AudioChannel::EndGuard_
-
 class AudioPlaybackConfig
 {
 public:
   AudioPlaybackConfig()
     : mVolume(1.0)
     , mMuted(false)
     , mSuspend(nsISuspendedTypes::NONE_SUSPENDED)
   {}
@@ -111,18 +108,17 @@ public:
    * this service.
    */
   void UnregisterAudioChannelAgent(AudioChannelAgent* aAgent);
 
   /**
    * Return the state to indicate this audioChannel for his window should keep
    * playing/muted/suspended.
    */
-  AudioPlaybackConfig GetMediaConfig(nsPIDOMWindowOuter* aWindow,
-                                     uint32_t aAudioChannel) const;
+  AudioPlaybackConfig GetMediaConfig(nsPIDOMWindowOuter* aWindow) const;
 
   /**
    * Called this method when the audible state of the audio playback changed,
    * it would dispatch the playback event to observers which want to know the
    * actual audible state of the window.
    */
   void AudioAudibleChanged(AudioChannelAgent* aAgent,
                            AudibleState aAudible,
@@ -137,20 +133,16 @@ public:
   // This method needs to know the inner window that wants to capture audio. We
   // group agents per top outer window, but we can have multiple innerWindow per
   // top outerWindow (subiframes, etc.) and we have to identify all the agents
   // just for a particular innerWindow.
   void SetWindowAudioCaptured(nsPIDOMWindowOuter* aWindow,
                               uint64_t aInnerWindowID,
                               bool aCapture);
 
-  static const nsAttrValue::EnumTable* GetAudioChannelTable();
-  static AudioChannel GetAudioChannel(const nsAString& aString);
-  static AudioChannel GetDefaultAudioChannel();
-
   void NotifyMediaResumedFromBlock(nsPIDOMWindowOuter* aWindow);
 
 private:
   AudioChannelService();
   ~AudioChannelService();
 
   void RefreshAgents(nsPIDOMWindowOuter* aWindow,
                      const std::function<void(AudioChannelAgent*)>& aFunc);
@@ -192,17 +184,17 @@ private:
 
     void AppendAgent(AudioChannelAgent* aAgent, AudibleState aAudible);
     void RemoveAgent(AudioChannelAgent* aAgent);
 
     void NotifyMediaBlockStop(nsPIDOMWindowOuter* aWindow);
 
     uint64_t mWindowID;
     bool mIsAudioCaptured;
-    AudioChannelConfig mChannels[NUMBER_OF_AUDIO_CHANNELS];
+    AudioChannelConfig mChannelConfig;
 
     // Raw pointer because the AudioChannelAgent must unregister itself.
     nsTObserverArray<AudioChannelAgent*> mAgents;
     nsTObserverArray<AudioChannelAgent*> mAudibleAgents;
 
     // Owning audio focus when the window starts playing audible sound, and
     // lose audio focus when other windows starts playing.
     bool mOwningAudioFocus;
@@ -233,18 +225,17 @@ private:
     void NotifyChannelActive(uint64_t aWindowID, bool aActive);
     void MaybeNotifyMediaBlockStart(AudioChannelAgent* aAgent);
 
     void RequestAudioFocus(AudioChannelAgent* aAgent);
 
     // We need to do audio competing only when the new incoming agent started.
     void NotifyAudioCompetingChanged(AudioChannelAgent* aAgent);
 
-    uint32_t GetCompetingBehavior(AudioChannelAgent* aAgent,
-                                  int32_t aIncomingChannelType) const;
+    uint32_t GetCompetingBehavior(AudioChannelAgent* aAgent) const;
     bool IsAgentInvolvingInAudioCompeting(AudioChannelAgent* aAgent) const;
     bool IsAudioCompetingInSameTab() const;
     bool IsContainingPlayingAgent(AudioChannelAgent* aAgent) const;
 
     bool IsInactiveWindow() const;
   };
 
   AudioChannelWindow*
diff --git a/dom/audiochannel/nsIAudioChannelAgent.idl b/dom/audiochannel/nsIAudioChannelAgent.idl
--- a/dom/audiochannel/nsIAudioChannelAgent.idl
+++ b/dom/audiochannel/nsIAudioChannelAgent.idl
@@ -82,82 +82,54 @@ interface nsIAudioChannelAgentCallback :
    * Notified when the capture state is changed.
    */
   void windowAudioCaptureChanged(in bool aCapture);
 };
 
 /**
  * This interface provides an agent for gecko components to participate
  * in the audio channel service. Gecko components are responsible for
- *   1. Indicating what channel type they are using (via the init() member
- *      function).
- *   2. Notifying the agent when they start/stop using this channel.
- *   3. Notifying the agent when they are audible.
+ *   1. Notifying the agent when they start/stop using this channel.
+ *   2. Notifying the agent when they are audible.
  *
  * The agent will invoke a callback to notify Gecko components of
  *   1. Changes to the playable status of this channel.
  */
 
-[uuid(ab7e21c0-970c-11e5-a837-0800200c9a66)]
+[uuid(4d212770-5d7b-446f-9394-632e351d96ee)]
 interface nsIAudioChannelAgent : nsISupports
 {
-  const long AUDIO_AGENT_CHANNEL_NORMAL             = 0;
-  const long AUDIO_AGENT_CHANNEL_CONTENT            = 1;
-  const long AUDIO_AGENT_CHANNEL_NOTIFICATION       = 2;
-  const long AUDIO_AGENT_CHANNEL_ALARM              = 3;
-  const long AUDIO_AGENT_CHANNEL_TELEPHONY          = 4;
-  const long AUDIO_AGENT_CHANNEL_RINGER             = 5;
-  const long AUDIO_AGENT_CHANNEL_PUBLICNOTIFICATION = 6;
-  const long AUDIO_AGENT_CHANNEL_SYSTEM             = 7;
-
-  const long AUDIO_AGENT_CHANNEL_ERROR              = 1000;
-
   const long AUDIO_AGENT_STATE_NORMAL               = 0;
   const long AUDIO_AGENT_STATE_MUTED                = 1;
   const long AUDIO_AGENT_STATE_FADED                = 2;
 
   /**
-   * Before init() is called, this returns AUDIO_AGENT_CHANNEL_ERROR.
-   */
-  readonly attribute long audioChannelType;
-
-  %{C++
-  inline int32_t AudioChannelType() {
-    int32_t channel;
-    return NS_SUCCEEDED(GetAudioChannelType(&channel)) ? channel : AUDIO_AGENT_CHANNEL_ERROR;
-  }
-  %}
-
-  /**
    * Initialize the agent with a channel type.
    * Note: This function should only be called once.
    *
    * @param window
    *    The window
-   * @param channelType
-   *    Audio Channel Type listed as above
    * @param callback
    *    1. Once the playable status changes, agent uses this callback function
    *       to notify Gecko component.
    *    2. The callback is allowed to be null. Ex: telephony doesn't need to
    *       listen change of the playable status.
    *    3. The AudioChannelAgent keeps a strong reference to the callback
    *       object.
    */
-  void init(in mozIDOMWindow window, in long channelType,
-            in nsIAudioChannelAgentCallback callback);
+  void init(in mozIDOMWindow window, in nsIAudioChannelAgentCallback callback);
 
   /**
    * This method is just like init(), except the audio channel agent keeps a
    * weak reference to the callback object.
    *
    * In order for this to work, |callback| must implement
    * nsISupportsWeakReference.
    */
-  void initWithWeakCallback(in mozIDOMWindow window, in long channelType,
+  void initWithWeakCallback(in mozIDOMWindow window,
                             in nsIAudioChannelAgentCallback callback);
 
   /**
    * Notify the agent that we want to start playing.
    * Note: Gecko component SHOULD call this function first then start to
    *          play audio stream only when return value is true.
    *
    * @param config
diff --git a/dom/html/HTMLMediaElement.cpp b/dom/html/HTMLMediaElement.cpp
--- a/dom/html/HTMLMediaElement.cpp
+++ b/dom/html/HTMLMediaElement.cpp
@@ -860,19 +860,17 @@ private:
   bool
   MaybeCreateAudioChannelAgent()
   {
     if (mAudioChannelAgent) {
       return true;
     }
 
     mAudioChannelAgent = new AudioChannelAgent();
-    nsresult rv = mAudioChannelAgent->Init(mOwner->OwnerDoc()->GetInnerWindow(),
-                                           static_cast<int32_t>(AudioChannelService::GetDefaultAudioChannel()),
-                                           this);
+    nsresult rv = mAudioChannelAgent->Init(mOwner->OwnerDoc()->GetInnerWindow(), this);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       mAudioChannelAgent = nullptr;
       MOZ_LOG(AudioChannelService::GetAudioChannelLog(), LogLevel::Debug,
              ("HTMLMediaElement::AudioChannelAgentCallback, Fail to initialize "
               "the audio channel agent, this = %p\n", this));
       return false;
     }
 
diff --git a/dom/media/webaudio/AudioDestinationNode.cpp b/dom/media/webaudio/AudioDestinationNode.cpp
--- a/dom/media/webaudio/AudioDestinationNode.cpp
+++ b/dom/media/webaudio/AudioDestinationNode.cpp
@@ -601,19 +601,17 @@ AudioDestinationNode::CreateAudioChannel
   if (mAudioChannelAgent) {
     rv = mAudioChannelAgent->NotifyStoppedPlaying();
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
     }
   }
 
   mAudioChannelAgent = new AudioChannelAgent();
-  rv = mAudioChannelAgent->InitWithWeakCallback(GetOwner(),
-                                           static_cast<int32_t>(AudioChannel::Normal),
-                                           this);
+  rv = mAudioChannelAgent->InitWithWeakCallback(GetOwner(), this);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   return NS_OK;
 }
 
 void
diff --git a/dom/media/webspeech/synth/nsSpeechTask.cpp b/dom/media/webspeech/synth/nsSpeechTask.cpp
--- a/dom/media/webspeech/synth/nsSpeechTask.cpp
+++ b/dom/media/webspeech/synth/nsSpeechTask.cpp
@@ -376,19 +376,17 @@ nsSpeechTask::CreateAudioChannelAgent()
     return;
   }
 
   if (mAudioChannelAgent) {
     mAudioChannelAgent->NotifyStoppedPlaying();
   }
 
   mAudioChannelAgent = new AudioChannelAgent();
-  mAudioChannelAgent->InitWithWeakCallback(mUtterance->GetOwner(),
-                                           static_cast<int32_t>(AudioChannelService::GetDefaultAudioChannel()),
-                                           this);
+  mAudioChannelAgent->InitWithWeakCallback(mUtterance->GetOwner(), this);
 
   AudioPlaybackConfig config;
   nsresult rv = mAudioChannelAgent->NotifyStartedPlaying(&config,
                                                          AudioChannelService::AudibleState::eAudible);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return;
   }
 
diff --git a/dom/media/webspeech/synth/nsSynthVoiceRegistry.cpp b/dom/media/webspeech/synth/nsSynthVoiceRegistry.cpp
--- a/dom/media/webspeech/synth/nsSynthVoiceRegistry.cpp
+++ b/dom/media/webspeech/synth/nsSynthVoiceRegistry.cpp
@@ -667,19 +667,17 @@ nsSynthVoiceRegistry::SpeakUtterance(Spe
   }
 
   // Get current audio volume to apply speech call
   float volume = aUtterance.Volume();
   RefPtr<AudioChannelService> service = AudioChannelService::GetOrCreate();
   if (service) {
     if (nsCOMPtr<nsPIDOMWindowInner> topWindow = aUtterance.GetOwner()) {
       // TODO : use audio channel agent, open new bug to fix it.
-      uint32_t channel = static_cast<uint32_t>(AudioChannelService::GetDefaultAudioChannel());
-      AudioPlaybackConfig config = service->GetMediaConfig(topWindow->GetOuterWindow(),
-                                                           channel);
+      AudioPlaybackConfig config = service->GetMediaConfig(topWindow->GetOuterWindow());
       volume = config.mMuted ? 0.0f : config.mVolume * volume;
     }
   }
 
   nsCOMPtr<nsPIDOMWindowInner> window = aUtterance.GetOwner();
   nsCOMPtr<nsIDocument> doc = window ? window->GetDoc() : nullptr;
 
   bool isChrome = nsContentUtils::IsChromeDoc(doc);
diff --git a/dom/plugins/base/nsNPAPIPluginInstance.cpp b/dom/plugins/base/nsNPAPIPluginInstance.cpp
--- a/dom/plugins/base/nsNPAPIPluginInstance.cpp
+++ b/dom/plugins/base/nsNPAPIPluginInstance.cpp
@@ -1258,19 +1258,17 @@ nsNPAPIPluginInstance::CreateAudioChanne
     return NS_ERROR_FAILURE;
   }
 
   nsCOMPtr<nsPIDOMWindowOuter> window = GetDOMWindow();
   if (NS_WARN_IF(!window)) {
     return NS_ERROR_FAILURE;
   }
 
-  rv = mAudioChannelAgent->Init(window->GetCurrentInnerWindow(),
-                               (int32_t)AudioChannelService::GetDefaultAudioChannel(),
-                               this);
+  rv = mAudioChannelAgent->Init(window->GetCurrentInnerWindow(), this);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
   return NS_OK;
 }
 
 void
 nsNPAPIPluginInstance::NotifyStartedPlaying()
