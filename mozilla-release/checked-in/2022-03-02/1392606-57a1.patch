# HG changeset patch
# User Miran.Karic <Miran.Karic@imgtec.com>
# Date 1503563100 14400
#      Thu Aug 24 04:25:00 2017 -0400
# Node ID 34a8455c373ccd53d11dd881c5177a6937cdadf0
# Parent  1284ab7cbcfad25f942224883b487ad29431da5b
Bug 1392606 - Fix MIPS64 simulator build compilation failures. r=bbouvier

diff --git a/js/src/jit/mips-shared/MacroAssembler-mips-shared.cpp b/js/src/jit/mips-shared/MacroAssembler-mips-shared.cpp
--- a/js/src/jit/mips-shared/MacroAssembler-mips-shared.cpp
+++ b/js/src/jit/mips-shared/MacroAssembler-mips-shared.cpp
@@ -1484,68 +1484,68 @@ MacroAssembler::flush()
 
 // ===============================================================
 // Stack manipulation functions.
 
 void
 MacroAssembler::Push(Register reg)
 {
     ma_push(reg);
-    adjustFrame(sizeof(intptr_t));
+    adjustFrame(int32_t(sizeof(intptr_t)));
 }
 
 void
 MacroAssembler::Push(const Imm32 imm)
 {
     ma_li(ScratchRegister, imm);
     ma_push(ScratchRegister);
-    adjustFrame(sizeof(intptr_t));
+    adjustFrame(int32_t(sizeof(intptr_t)));
 }
 
 void
 MacroAssembler::Push(const ImmWord imm)
 {
     ma_li(ScratchRegister, imm);
     ma_push(ScratchRegister);
-    adjustFrame(sizeof(intptr_t));
+    adjustFrame(int32_t(sizeof(intptr_t)));
 }
 
 void
 MacroAssembler::Push(const ImmPtr imm)
 {
     Push(ImmWord(uintptr_t(imm.value)));
 }
 
 void
 MacroAssembler::Push(const ImmGCPtr ptr)
 {
     ma_li(ScratchRegister, ptr);
     ma_push(ScratchRegister);
-    adjustFrame(sizeof(intptr_t));
+    adjustFrame(int32_t(sizeof(intptr_t)));
 }
 
 void
 MacroAssembler::Push(FloatRegister f)
 {
     ma_push(f);
-    adjustFrame(sizeof(double));
+    adjustFrame(int32_t(sizeof(double)));
 }
 
 void
 MacroAssembler::Pop(Register reg)
 {
     ma_pop(reg);
-    adjustFrame(-sizeof(intptr_t));
+    adjustFrame(-int32_t(sizeof(intptr_t)));
 }
 
 void
 MacroAssembler::Pop(FloatRegister f)
 {
     ma_pop(f);
-    adjustFrame(-sizeof(double));
+    adjustFrame(-int32_t(sizeof(double)));
 }
 
 void
 MacroAssembler::Pop(const ValueOperand& val)
 {
     popValue(val);
     framePushed_ -= sizeof(Value);
 }
diff --git a/js/src/jit/mips64/Architecture-mips64.h b/js/src/jit/mips64/Architecture-mips64.h
--- a/js/src/jit/mips64/Architecture-mips64.h
+++ b/js/src/jit/mips64/Architecture-mips64.h
@@ -105,16 +105,24 @@ class FloatRegister : public FloatRegist
   public:
     constexpr FloatRegister(uint32_t r, ContentType kind = Codes::Double)
       : reg_(Encoding(r)), kind_(kind)
     { }
     constexpr FloatRegister()
       : reg_(Encoding(FloatRegisters::invalid_freg)), kind_(Codes::Double)
     { }
 
+    static uint32_t SetSize(SetType x) {
+        // Count the number of non-aliased registers.
+        x |= x >> Codes::TotalPhys;
+        x &= Codes::AllPhysMask;
+        static_assert(Codes::AllPhysMask <= 0xffffffff, "We can safely use CountPopulation32");
+        return mozilla::CountPopulation32(x);
+    }
+
     bool operator==(const FloatRegister& other) const {
         MOZ_ASSERT(!isInvalid());
         MOZ_ASSERT(!other.isInvalid());
         return kind_ == other.kind_ && reg_ == other.reg_;
     }
     bool equiv(const FloatRegister& other) const { return other.kind_ == kind_; }
     size_t size() const { return (kind_ == Codes::Double) ? sizeof(double) : sizeof (float); }
     bool isInvalid() const {
diff --git a/js/src/jit/mips64/Simulator-mips64.cpp b/js/src/jit/mips64/Simulator-mips64.cpp
--- a/js/src/jit/mips64/Simulator-mips64.cpp
+++ b/js/src/jit/mips64/Simulator-mips64.cpp
@@ -35,16 +35,18 @@
 #include "mozilla/Likely.h"
 #include "mozilla/MathAlgorithms.h"
 
 #include <float.h>
 
 #include "jit/mips64/Assembler-mips64.h"
 #include "threading/LockGuard.h"
 #include "vm/Runtime.h"
+#include "wasm/WasmInstance.h"
+#include "wasm/WasmSignalHandlers.h"
 
 #define I8(v)   static_cast<int8_t>(v)
 #define I16(v)  static_cast<int16_t>(v)
 #define U16(v)  static_cast<uint16_t>(v)
 #define I32(v)  static_cast<int32_t>(v)
 #define U32(v)  static_cast<uint32_t>(v)
 #define I64(v)  static_cast<int64_t>(v)
 #define U64(v)  static_cast<uint64_t>(v)
@@ -1262,17 +1264,17 @@ Simulator::Simulator()
     // Note, allocation and anything that depends on allocated memory is
     // deferred until init(), in order to handle OOM properly.
 
     stack_ = nullptr;
     stackLimit_ = 0;
     pc_modified_ = false;
     icount_ = 0;
     break_count_ = 0;
-    resume_pc_ = 0;
+    wasm_interrupt_ = false;
     break_pc_ = nullptr;
     break_instr_ = 0;
     single_stepping_ = false;
     single_step_callback_ = nullptr;
     single_step_callback_arg_ = nullptr;
 
     // Set up architecture state.
     // All registers are initialized to zero to start with.
@@ -1605,16 +1607,28 @@ Simulator::has_bad_pc() const
 
 // Raw access to the PC register without the special adjustment when reading.
 int64_t
 Simulator::get_pc() const
 {
     return registers_[pc];
 }
 
+void
+Simulator::startInterrupt(WasmActivation* activation)
+{
+    MOZ_CRASH("NIY");
+}
+
+void
+Simulator::handleWasmInterrupt()
+{
+    MOZ_CRASH("NIY");
+}
+
 // The MIPS cannot do unaligned reads and writes.  On some MIPS platforms an
 // interrupt is caused.  On others it does a funky rotation thing.  For now we
 // simply disallow unaligned reads, but at some point we may want to move to
 // emulating the rotate behaviour.  Note that simulator runs have the runtime
 // system running directly on the host system and only generated code is
 // executed in the simulator.  Since the host is typically IA32 we will not
 // get the correct MIPS-like behaviour on unaligned accesses.
 
@@ -2715,16 +2729,17 @@ Simulator::decodeTypeRegister(SimInstruc
     switch (op) {
       case op_cop1:
         switch (instr->rsFieldRaw()) {
           case rs_bc1:   // Branch on coprocessor condition.
             MOZ_CRASH();
             break;
           case rs_cfc1:
             setRegister(rt_reg, alu_out);
+            MOZ_FALLTHROUGH;
           case rs_mfc1:
             setRegister(rt_reg, alu_out);
             break;
           case rs_dmfc1:
             setRegister(rt_reg, alu_out);
             break;
           case rs_mfhc1:
             setRegister(rt_reg, alu_out);
@@ -2803,16 +2818,17 @@ Simulator::decodeTypeRegister(SimInstruc
               case ff_cvt_d_fmt:
                 f = getFpuRegisterFloat(fs_reg);
                 setFpuRegisterDouble(fd_reg, static_cast<double>(f));
                 break;
               case ff_cvt_w_fmt:   // Convert float to word.
                 // Rounding modes are not yet supported.
                 MOZ_ASSERT((FCSR_ & 3) == 0);
                 // In rounding mode 0 it should behave like ROUND.
+                MOZ_FALLTHROUGH;
               case ff_round_w_fmt: { // Round double to word (round half to even).
                 float rounded = std::floor(fs_value + 0.5);
                 int32_t result = I32(rounded);
                 if ((result & 1) != 0 && result - fs_value == 0.5) {
                     // If the number is halfway between two integers,
                     // round to the even one.
                     result--;
                 }
@@ -2938,16 +2954,17 @@ Simulator::decodeTypeRegister(SimInstruc
               case ff_c_ule_fmt:
                 setFCSRBit(fcsr_cc,
                            (ds_value <= dt_value) || (mozilla::IsNaN(ds_value) || mozilla::IsNaN(dt_value)));
                 break;
               case ff_cvt_w_fmt:   // Convert double to word.
                 // Rounding modes are not yet supported.
                 MOZ_ASSERT((FCSR_ & 3) == 0);
                 // In rounding mode 0 it should behave like ROUND.
+                MOZ_FALLTHROUGH;
               case ff_round_w_fmt: { // Round double to word (round half to even).
                 double rounded = std::floor(ds_value + 0.5);
                 int32_t result = I32(rounded);
                 if ((result & 1) != 0 && result - ds_value == 0.5) {
                     // If the number is halfway between two integers,
                     // round to the even one.
                     result--;
                 }
@@ -3309,16 +3326,17 @@ Simulator::decodeTypeImmediate(SimInstru
             // Set next_pc.
             if (do_branch) {
                 next_pc = current_pc + (imm16 << 2) + SimInstruction::kInstrSize;
                 if (instr->isLinkingInstruction())
                     setRegister(31, current_pc + kBranchReturnOffset);
             } else {
                 next_pc = current_pc + kBranchReturnOffset;
             }
+            break;
           default:
             break;
         };
         break;  // case op_regimm.
         // ------------- Branch instructions.
         // When comparing to zero, the encoding of rt field is always 0, so we don't
         // need to replace rt with zero.
       case op_beq:
@@ -3717,35 +3735,31 @@ void
 Simulator::execute()
 {
     if (single_stepping_)
         single_step_callback_(single_step_callback_arg_, this, nullptr);
 
     // Get the PC to simulate. Cannot use the accessor here as we need the
     // raw PC value and not the one used as input to arithmetic instructions.
     int64_t program_counter = get_pc();
-    WasmActivation* activation = TlsContext.get()->wasmActivationStack();
 
     while (program_counter != end_sim_pc) {
         if (enableStopSimAt && (icount_ == Simulator::StopSimAt)) {
             MipsDebugger dbg(this);
             dbg.debug();
         } else {
             if (single_stepping_)
                 single_step_callback_(single_step_callback_arg_, this, (void*)program_counter);
             SimInstruction* instr = reinterpret_cast<SimInstruction *>(program_counter);
             instructionDecode(instr);
             icount_++;
 
-            int64_t rpc = resume_pc_;
-            if (MOZ_UNLIKELY(rpc != 0)) {
-                // wasm signal handler ran and we have to adjust the pc.
-                activation->setResumePC((void*)get_pc());
-                set_pc(rpc);
-                resume_pc_ = 0;
+            if (MOZ_UNLIKELY(wasm_interrupt_)) {
+                handleWasmInterrupt();
+                wasm_interrupt_ = false;
             }
         }
         program_counter = get_pc();
     }
 
     if (single_stepping_)
         single_step_callback_(single_step_callback_arg_, this, nullptr);
 }
diff --git a/js/src/jit/mips64/Simulator-mips64.h b/js/src/jit/mips64/Simulator-mips64.h
--- a/js/src/jit/mips64/Simulator-mips64.h
+++ b/js/src/jit/mips64/Simulator-mips64.h
@@ -34,16 +34,19 @@
 
 #include "mozilla/Atomics.h"
 
 #include "jit/IonTypes.h"
 #include "threading/Thread.h"
 #include "vm/MutexIDs.h"
 
 namespace js {
+
+class WasmActivation;
+
 namespace jit {
 
 class Simulator;
 class Redirection;
 class CachePage;
 class AutoLockSimulator;
 
 // When the SingleStepCallback is called, the simulator is about to execute
@@ -188,19 +191,22 @@ class Simulator {
 
     // Special case of set_register and get_register to access the raw PC value.
     void set_pc(int64_t value);
     int64_t get_pc() const;
 
     template <typename T>
     T get_pc_as() const { return reinterpret_cast<T>(get_pc()); }
 
-    void set_resume_pc(void* value) {
-        resume_pc_ = int64_t(value);
-     }
+    void trigger_wasm_interrupt() {
+        // This can be called several times if a single interrupt isn't caught
+        // and handled by the simulator, but this is fine; once the current
+        // instruction is done executing, the interrupt will be handled anyhow.
+        wasm_interrupt_ = true;
+    }
 
     void enable_single_stepping(SingleStepCallback cb, void* arg);
     void disable_single_stepping();
 
     // Accessor to the internal simulator stack area.
     uintptr_t stackLimit() const;
     bool overRecursed(uintptr_t newsp = 0) const;
     bool overRecursedWithExtra(uint32_t extra) const;
@@ -292,16 +298,19 @@ class Simulator {
     void handleStop(uint32_t code, SimInstruction* instr);
     bool isStopInstruction(SimInstruction* instr);
     bool isEnabledStop(uint32_t code);
     void enableStop(uint32_t code);
     void disableStop(uint32_t code);
     void increaseStopCounter(uint32_t code);
     void printStopInfo(uint32_t code);
 
+    // Handle a wasm interrupt triggered by an async signal handler.
+    void handleWasmInterrupt();
+    void startInterrupt(WasmActivation* act);
 
     // Executes one instruction.
     void instructionDecode(SimInstruction* instr);
     // Execute one instruction placed in a branch delay slot.
     void branchDelayInstructionDecode(SimInstruction* instr);
 
   public:
     static int64_t StopSimAt;
@@ -340,17 +349,18 @@ class Simulator {
 
     // Simulator support.
     char* stack_;
     uintptr_t stackLimit_;
     bool pc_modified_;
     int64_t icount_;
     int64_t break_count_;
 
-    int64_t resume_pc_;
+    // wasm async interrupt support
+    bool wasm_interrupt_;
 
     // Debugger input.
     char* lastDebuggerInput_;
 
     // Registered breakpoints.
     SimInstruction* break_pc_;
     Instr break_instr_;
 
diff --git a/js/src/wasm/WasmStubs.cpp b/js/src/wasm/WasmStubs.cpp
--- a/js/src/wasm/WasmStubs.cpp
+++ b/js/src/wasm/WasmStubs.cpp
@@ -1173,17 +1173,17 @@ wasm::GenerateInterruptExit(MacroAssembl
     masm.PopRegsInMask(AllRegsExceptSP);
 
     // Pop resumePC into PC. Clobber HeapReg to make the jump and restore it
     // during jump delay slot.
     masm.loadPtr(Address(StackPointer, 0), HeapReg);
     // Reclaim the reserve space.
     masm.addToStackPtr(Imm32(2 * sizeof(intptr_t)));
     masm.as_jr(HeapReg);
-    masm.loadPtr(Address(StackPointer, -sizeof(intptr_t)), HeapReg);
+    masm.loadPtr(Address(StackPointer, -int32_t(sizeof(intptr_t))), HeapReg);
 #elif defined(JS_CODEGEN_ARM)
     {
         // Be careful not to clobber scratch registers before they are saved.
         ScratchRegisterScope scratch(masm);
         SecondScratchRegisterScope secondScratch(masm);
 
         // Reserve a word to receive the return address.
         masm.as_alu(StackPointer, StackPointer, Imm8(4), OpSub);
