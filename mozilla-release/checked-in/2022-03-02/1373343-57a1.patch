# HG changeset patch
# User Bobby Holley <bobbyholley@gmail.com>
# Date 1502985896 25200
#      Thu Aug 17 09:04:56 2017 -0700
# Node ID f32be31f0998a4c3050a873a42599efd9a440e09
# Parent  190c615916eb434c5bfc30896c057e9f2637c3a7
Bug 1373343 - Skip the parallel traversal when the presshell isn't active. r=heycam

I've verified with the profiler that we use the parallel traversal for
loading a foreground tab, but not for a tab opened with ctrl-click.

MozReview-Commit-ID: 2SiVDlLLyah

diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -876,17 +876,19 @@ ServoStyleSet::StyleDocument(ServoTraver
 
     // If there were text nodes inserted into the document (but not elements),
     // there may be lazy frame construction to do even if no styling is required.
     postTraversalRequired |= root->HasFlag(NODE_DESCENDANTS_NEED_FRAMES);
 
     // Allow the parallel traversal, unless we're traversing traversing one of
     // the native anonymous document style roots, which are tiny and not worth
     // parallelizing over.
-    if (!root->IsInNativeAnonymousSubtree()) {
+    //
+    // We only allow the parallel traversal in active (foreground) tabs.
+    if (!root->IsInNativeAnonymousSubtree() && mPresContext->PresShell()->IsActive()) {
       flags |= ServoTraversalFlags::ParallelTraversal;
     }
 
     // Do the first traversal.
     bool required = Servo_TraverseSubtree(root, mRawSet.get(), &snapshots, flags);
     MOZ_ASSERT_IF(isInitial, !required);
     postTraversalRequired |= required;
 
@@ -978,17 +980,18 @@ ServoStyleSet::StyleNewChildren(Element*
   aParent->SetHasDirtyDescendantsForServo();
 
   auto flags = ServoTraversalFlags::UnstyledOnly;
 
   // If there is an XBL binding on the root element, we do the initial document
   // styling with this API. Not clear how common that is, but we allow parallel
   // traversals in this case to preserve the old behavior (where Servo would
   // use the parallel traversal i.f.f. the traversal root was the document root).
-  if (aParent == aParent->OwnerDoc()->GetRootElement()) {
+  if (aParent == aParent->OwnerDoc()->GetRootElement() &&
+      mPresContext->PresShell()->IsActive()) {
     flags |= ServoTraversalFlags::ParallelTraversal;
   }
 
   // Do the traversal. The snapshots will be ignored.
   const SnapshotTable& snapshots = Snapshots();
   Servo_TraverseSubtree(aParent, mRawSet.get(), &snapshots, flags);
 
   // Restore the old state of the dirty descendants bit.
