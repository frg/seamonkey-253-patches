# HG changeset patch
# User James Teh <jteh@mozilla.com>
# Date 1595503137 0
# Node ID 92582efe4621cf2b7a2feb5bd101a9c1ce70206c
# Parent  2aaa81ab38be985918019e314076237e03e4fbdc
Bug 1368270: When shutting down Windows AccessibleWraps, don't clear the id. r=MarcoZ, a=jcristau

When an Accessible is shut down, there might still be external references to it, so we keep the instance alive.
We don't want to reuse the id until all references are released, so we only release the id in the destructor (which runs when all references are released).
Previously, we were clearing the id variable in Shutdown, which meant we couldn't release the id in the destructor!
This meant we always leaked ids, never reusing them.
Now, we don't clear the id in Shutdown.

Differential Revision: https://phabricator.services.mozilla.com/D84653

diff --git a/accessible/windows/msaa/AccessibleWrap.cpp b/accessible/windows/msaa/AccessibleWrap.cpp
--- a/accessible/windows/msaa/AccessibleWrap.cpp
+++ b/accessible/windows/msaa/AccessibleWrap.cpp
@@ -90,20 +90,20 @@ ITypeInfo* AccessibleWrap::gTypeInfo = n
 
 NS_IMPL_ISUPPORTS_INHERITED0(AccessibleWrap, Accessible)
 
 void
 AccessibleWrap::Shutdown()
 {
   if (mID != kNoID) {
     auto doc = static_cast<DocAccessibleWrap*>(mDoc.get());
-    MOZ_ASSERT(doc);
+    // Accessibles can be shut down twice in some cases. When this happens,
+    // doc will be null.
     if (doc) {
       doc->RemoveID(mID);
-      mID = kNoID;
     }
   }
 
   Accessible::Shutdown();
 }
 
 //-----------------------------------------------------
 // IUnknown interface methods - see iunknown.h for documentation
