# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1504931760 25200
#      Fri Sep 08 21:36:00 2017 -0700
# Node ID 10629cc177b4a3925d5547bf94df550c36b1b155
# Parent  0dba076000234ab94da120c799841e53cef1c1cd
Bug 1397448: Part 6 - Cache messageManager for MessageManagerProxy. r=mixedpuppy

MozReview-Commit-ID: 7eqSgBkrj4f

diff --git a/toolkit/components/extensions/ExtensionUtils.jsm b/toolkit/components/extensions/ExtensionUtils.jsm
--- a/toolkit/components/extensions/ExtensionUtils.jsm
+++ b/toolkit/components/extensions/ExtensionUtils.jsm
@@ -487,21 +487,17 @@ function defineLazyGetter(object, prop, 
  *        The target message manager on which to send messages, or the
  *        <browser> element which owns it.
  */
 class MessageManagerProxy {
   constructor(target) {
     this.listeners = new DefaultMap(() => new Map());
 
     if (target instanceof Ci.nsIMessageSender) {
-      Object.defineProperty(this, "messageManager", {
-        value: target,
-        configurable: true,
-        writable: true,
-      });
+      this.messageManager = target;
     } else {
       this.addListeners(target);
     }
   }
 
   /**
    * Disposes of the proxy object, removes event listeners, and drops
    * all references to the underlying message manager.
@@ -509,19 +505,18 @@ class MessageManagerProxy {
    * Must be called before the last reference to the proxy is dropped,
    * unless the underlying message manager or <browser> is also being
    * destroyed.
    */
   dispose() {
     if (this.eventTarget) {
       this.removeListeners(this.eventTarget);
       this.eventTarget = null;
-    } else {
-      this.messageManager = null;
     }
+    this.messageManager = null;
   }
 
   /**
    * Returns true if the given target is the same as, or owns, the given
    * message manager.
    *
    * @param {nsIMessageSender|MessageManagerProxy|Element} target
    *        The message manager, MessageManagerProxy, or <browser>
@@ -539,19 +534,16 @@ class MessageManagerProxy {
   }
 
   /**
    * @property {nsIMessageSender|null} messageManager
    *        The message manager that is currently being proxied. This
    *        may change during the life of the proxy object, so should
    *        not be stored elsewhere.
    */
-  get messageManager() {
-    return this.eventTarget && this.eventTarget.messageManager;
-  }
 
   /**
    * Sends a message on the proxied message manager.
    *
    * @param {array} args
    *        Arguments to be passed verbatim to the underlying
    *        sendAsyncMessage method.
    * @returns {undefined}
@@ -620,35 +612,36 @@ class MessageManagerProxy {
    * Adds docShell swap listeners to the message manager owner.
    *
    * @param {Element} target
    *        The target element.
    */
   addListeners(target) {
     target.addEventListener("SwapDocShells", this);
 
+    this.eventTarget = target;
+    this.messageManager = target.messageManager;
+
     for (let {message, listener, listenWhenClosed} of this.iterListeners()) {
-      target.addMessageListener(message, listener, listenWhenClosed);
+      this.messageManager.addMessageListener(message, listener, listenWhenClosed);
     }
-
-    this.eventTarget = target;
   }
 
   /**
    * @private
    * Removes docShell swap listeners to the message manager owner.
    *
    * @param {Element} target
    *        The target element.
    */
   removeListeners(target) {
     target.removeEventListener("SwapDocShells", this);
 
     for (let {message, listener} of this.iterListeners()) {
-      target.removeMessageListener(message, listener);
+      this.messageManager.removeMessageListener(message, listener);
     }
   }
 
   handleEvent(event) {
     if (event.type == "SwapDocShells") {
       this.removeListeners(this.eventTarget);
       this.addListeners(event.detail);
     }
