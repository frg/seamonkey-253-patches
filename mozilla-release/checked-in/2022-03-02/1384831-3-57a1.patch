# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1501082777 -28800
# Node ID 34906a2a7b4ca5fb3cf172922e960fb88381bc2b
# Parent  874b5a4dccac94d90dcc5848af93ab9f5da086a5
Bug 1384831. P3 - let HLSResource notify HLSDecoder directly without going through MediaResourceCallback. r=kikuo

We can't use MediaResourceCallback since HLSDecoder will not inherit ChannelMediaDecoder.

MozReview-Commit-ID: BzKatvYU90Y

diff --git a/dom/media/hls/HLSDecoder.cpp b/dom/media/hls/HLSDecoder.cpp
--- a/dom/media/hls/HLSDecoder.cpp
+++ b/dom/media/hls/HLSDecoder.cpp
@@ -14,16 +14,27 @@
 #include "MediaDecoderStateMachine.h"
 #include "MediaFormatReader.h"
 #include "MediaPrefs.h"
 #include "MediaShutdownManager.h"
 #include "nsNetUtil.h"
 
 namespace mozilla {
 
+void
+HLSDecoder::Shutdown()
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  auto resource = static_cast<HLSResource*>(mResource.get());
+  if (resource) {
+    resource->Detach();
+  }
+  ChannelMediaDecoder::Shutdown();
+}
+
 MediaDecoderStateMachine*
 HLSDecoder::CreateStateMachine()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   MediaResource* resource = GetResource();
   MOZ_ASSERT(resource);
   auto resourceWrapper = static_cast<HLSResource*>(resource)->GetResourceWrapper();
@@ -68,17 +79,17 @@ HLSDecoder::Load(nsIChannel* aChannel,
   MOZ_ASSERT(!mResource);
 
   nsCOMPtr<nsIURI> uri;
   nsresult rv = NS_GetFinalChannelURI(aChannel, getter_AddRefs(uri));
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
-  mResource = new HLSResource(mResourceCallback, aChannel, uri);
+  mResource = new HLSResource(this, aChannel, uri);
 
   rv = MediaShutdownManager::Instance().Register(this);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   SetStateMachine(CreateStateMachine());
   NS_ENSURE_TRUE(GetStateMachine(), NS_ERROR_FAILURE);
diff --git a/dom/media/hls/HLSDecoder.h b/dom/media/hls/HLSDecoder.h
--- a/dom/media/hls/HLSDecoder.h
+++ b/dom/media/hls/HLSDecoder.h
@@ -16,16 +16,18 @@ class HLSDecoder final : public ChannelM
 {
 public:
   // MediaDecoder interface.
   explicit HLSDecoder(MediaDecoderInit& aInit)
     : ChannelMediaDecoder(aInit)
   {
   }
 
+  void Shutdown() override;
+
   ChannelMediaDecoder* Clone(MediaDecoderInit& aInit) override;
 
   MediaDecoderStateMachine* CreateStateMachine() override;
 
   // Returns true if the HLS backend is pref'ed on.
   static bool IsEnabled();
 
   // Returns true if aContainerType is an HLS type that we think we can render
diff --git a/dom/media/hls/HLSResource.cpp b/dom/media/hls/HLSResource.cpp
--- a/dom/media/hls/HLSResource.cpp
+++ b/dom/media/hls/HLSResource.cpp
@@ -1,14 +1,15 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
+#include "HLSDecoder.h"
 #include "HLSResource.h"
 #include "HLSUtils.h"
 
 using namespace mozilla::java;
 
 namespace mozilla {
 
 HLSResourceCallbacksSupport::HLSResourceCallbacksSupport(HLSResource* aResource)
@@ -37,20 +38,20 @@ void
 HLSResourceCallbacksSupport::OnError(int aErrorCode)
 {
   MOZ_ASSERT(NS_IsMainThread());
   if (mResource) {
     mResource->onError(aErrorCode);
   }
 }
 
-HLSResource::HLSResource(MediaResourceCallback* aCallback,
+HLSResource::HLSResource(HLSDecoder* aDecoder,
                          nsIChannel* aChannel,
                          nsIURI* aURI)
-  : mCallback(aCallback)
+  : mDecoder(aDecoder)
   , mChannel(aChannel)
   , mURI(aURI)
 {
   nsCString spec;
   nsresult rv = aURI->GetSpec(spec);
   (void)rv;
   HLSResourceCallbacksSupport::Init();
   mJavaCallbacks = GeckoHLSResourceWrapper::Callbacks::New();
@@ -59,29 +60,31 @@ HLSResource::HLSResource(MediaResourceCa
   mHLSResourceWrapper = java::GeckoHLSResourceWrapper::Create(NS_ConvertUTF8toUTF16(spec),
                                                               mJavaCallbacks);
   MOZ_ASSERT(mHLSResourceWrapper);
 }
 
 void
 HLSResource::onDataAvailable()
 {
-  MOZ_ASSERT(mCallback);
   HLS_DEBUG("HLSResource", "onDataAvailable");
-  mCallback->NotifyDataArrived();
+  if (mDecoder) {
+    mDecoder->NotifyDataArrived();
+  }
 }
 
 void
 HLSResource::onError(int aErrorCode)
 {
-  MOZ_ASSERT(mCallback);
   HLS_DEBUG("HLSResource", "onError(%d)", aErrorCode);
   // Since HLS source should be from the Internet, we treat all resource errors
   // from GeckoHlsPlayer as network errors.
-  mCallback->NotifyNetworkError();
+  if (mDecoder) {
+    mDecoder->NetworkError();
+  }
 }
 
 void HLSResource::Suspend(bool aCloseImmediately)
 {
   MOZ_ASSERT(NS_IsMainThread(), "Don't call on non-main thread");
   HLS_DEBUG("HLSResource", "Should suspend the resource fetching.");
   mHLSResourceWrapper->Suspend();
 }
diff --git a/dom/media/hls/HLSResource.h b/dom/media/hls/HLSResource.h
--- a/dom/media/hls/HLSResource.h
+++ b/dom/media/hls/HLSResource.h
@@ -13,16 +13,17 @@
 #include "nsContentUtils.h"
 
 #define UNIMPLEMENTED() HLS_DEBUG("HLSResource", "UNIMPLEMENTED FUNCTION")
 
 using namespace mozilla::java;
 
 namespace mozilla {
 
+class HLSDecoder;
 class HLSResource;
 
 class HLSResourceCallbacksSupport
   : public GeckoHLSResourceWrapper::Callbacks::Natives<HLSResourceCallbacksSupport>
 {
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(HLSResourceCallbacksSupport)
 public:
   typedef GeckoHLSResourceWrapper::Callbacks::Natives<HLSResourceCallbacksSupport> NativeCallbacks;
@@ -37,19 +38,17 @@ public:
 private:
   ~HLSResourceCallbacksSupport() {}
   HLSResource* mResource;
 };
 
 class HLSResource final : public MediaResource
 {
 public:
-  HLSResource(MediaResourceCallback* aCallback,
-              nsIChannel* aChannel,
-              nsIURI* aURI);
+  HLSResource(HLSDecoder* aDecoder, nsIChannel* aChannel, nsIURI* aURI);
   ~HLSResource();
   nsresult Close() override { return NS_OK; }
   void Suspend(bool aCloseImmediately) override;
   void Resume() override;
   void SetReadMode(MediaCacheStream::ReadMode aMode) override { UNIMPLEMENTED(); }
   void SetPlaybackRate(uint32_t aBytesPerSecond) override  { UNIMPLEMENTED(); }
   nsresult ReadAt(int64_t aOffset, char* aBuffer, uint32_t aCount, uint32_t* aBytes) override { UNIMPLEMENTED(); return NS_ERROR_FAILURE; }
   bool ShouldCacheReads() override { UNIMPLEMENTED(); return false; }
@@ -95,16 +94,18 @@ public:
   {
     return false;
   }
 
   java::GeckoHLSResourceWrapper::GlobalRef GetResourceWrapper() {
     return mHLSResourceWrapper;
   }
 
+  void Detach() { mDecoder = nullptr; }
+
 private:
   friend class HLSResourceCallbacksSupport;
 
   void onDataAvailable();
   void onError(int aErrorCode);
 
   size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const override
   {
@@ -112,17 +113,17 @@ private:
     return size;
   }
 
   size_t SizeOfIncludingThis(MallocSizeOf aMallocSizeOf) const override
   {
     return aMallocSizeOf(this) + SizeOfExcludingThis(aMallocSizeOf);
   }
 
-  RefPtr<MediaResourceCallback> mCallback;
+  HLSDecoder* mDecoder;
   nsCOMPtr<nsIChannel> mChannel;
   nsCOMPtr<nsIURI> mURI;
   java::GeckoHLSResourceWrapper::GlobalRef mHLSResourceWrapper;
   java::GeckoHLSResourceWrapper::Callbacks::GlobalRef mJavaCallbacks;
   RefPtr<HLSResourceCallbacksSupport> mCallbackSupport;
 };
 
 } // namespace mozilla

