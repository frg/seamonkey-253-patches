# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1501829542 -28800
# Node ID 630ab1a8629dd171ecd87e54d28105264cdc9e2b
# Parent  aaa1e8517b28e1c37ceb5e78b1598f49b62ea509
Bug 1383628. P4 - move CloneData() and Create() from MediaResource to BaseMediaResource. r=gerald

Now we can store a more specific resource type in ChannelMediaDecoder.

MozReview-Commit-ID: EzXC547O7aR

diff --git a/dom/media/ChannelMediaDecoder.cpp b/dom/media/ChannelMediaDecoder.cpp
--- a/dom/media/ChannelMediaDecoder.cpp
+++ b/dom/media/ChannelMediaDecoder.cpp
@@ -213,17 +213,17 @@ nsresult
 ChannelMediaDecoder::Load(nsIChannel* aChannel,
                           bool aIsPrivateBrowsing,
                           nsIStreamListener** aStreamListener)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(!mResource);
 
   mResource =
-    MediaResource::Create(mResourceCallback, aChannel, aIsPrivateBrowsing);
+    BaseMediaResource::Create(mResourceCallback, aChannel, aIsPrivateBrowsing);
   if (!mResource) {
     return NS_ERROR_FAILURE;
   }
 
   nsresult rv = MediaShutdownManager::Instance().Register(this);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
@@ -233,17 +233,17 @@ ChannelMediaDecoder::Load(nsIChannel* aC
 
   SetStateMachine(CreateStateMachine());
   NS_ENSURE_TRUE(GetStateMachine(), NS_ERROR_FAILURE);
 
   return InitializeStateMachine();
 }
 
 nsresult
-ChannelMediaDecoder::Load(MediaResource* aOriginal)
+ChannelMediaDecoder::Load(BaseMediaResource* aOriginal)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(!mResource);
 
   mResource = aOriginal->CloneData(mResourceCallback);
   if (!mResource) {
     return NS_ERROR_FAILURE;
   }
diff --git a/dom/media/ChannelMediaDecoder.h b/dom/media/ChannelMediaDecoder.h
--- a/dom/media/ChannelMediaDecoder.h
+++ b/dom/media/ChannelMediaDecoder.h
@@ -10,16 +10,18 @@
 #include "MediaDecoder.h"
 #include "MediaResourceCallback.h"
 
 class nsIChannel;
 class nsIStreamListener;
 
 namespace mozilla {
 
+class BaseMediaResource;
+
 class ChannelMediaDecoder : public MediaDecoder
 {
   // Used to register with MediaResource to receive notifications which will
   // be forwarded to MediaDecoder.
   class ResourceCallback : public MediaResourceCallback
   {
     // Throttle calls to MediaDecoder::NotifyDataArrived()
     // to be at most once per 500ms.
@@ -48,17 +50,17 @@ class ChannelMediaDecoder : public Media
     ChannelMediaDecoder* mDecoder = nullptr;
     nsCOMPtr<nsITimer> mTimer;
     bool mTimerArmed = false;
     const RefPtr<AbstractThread> mAbstractMainThread;
   };
 
 protected:
   RefPtr<ResourceCallback> mResourceCallback;
-  RefPtr<MediaResource> mResource;
+  RefPtr<BaseMediaResource> mResource;
 
 public:
   explicit ChannelMediaDecoder(MediaDecoderInit& aInit);
 
   MediaDecoderStateMachine* CreateStateMachine() override;
 
   MediaResource* GetResource() const override final;
 
@@ -69,14 +71,14 @@ public:
 
   nsresult Load(nsIChannel* aChannel,
                 bool aIsPrivateBrowsing,
                 nsIStreamListener** aStreamListener);
 
 private:
   virtual ChannelMediaDecoder* CloneImpl(MediaDecoderInit& aInit) = 0;
   nsresult OpenResource(nsIStreamListener** aStreamListener);
-  nsresult Load(MediaResource* aOriginal);
+  nsresult Load(BaseMediaResource* aOriginal);
 };
 
 } // namespace mozilla
 
 #endif // ChannelMediaDecoder_h_
diff --git a/dom/media/MediaResource.cpp b/dom/media/MediaResource.cpp
--- a/dom/media/MediaResource.cpp
+++ b/dom/media/MediaResource.cpp
@@ -637,17 +637,18 @@ already_AddRefed<nsIPrincipal> ChannelMe
   return principal.forget();
 }
 
 bool ChannelMediaResource::CanClone()
 {
   return mCacheStream.IsAvailableForSharing();
 }
 
-already_AddRefed<MediaResource> ChannelMediaResource::CloneData(MediaResourceCallback* aCallback)
+already_AddRefed<BaseMediaResource>
+ChannelMediaResource::CloneData(MediaResourceCallback* aCallback)
 {
   NS_ASSERTION(NS_IsMainThread(), "Only call on main thread");
   NS_ASSERTION(mCacheStream.IsAvailableForSharing(), "Stream can't be cloned");
 
   RefPtr<ChannelMediaResource> resource =
     new ChannelMediaResource(aCallback, nullptr, mURI, mChannelStatistics);
   if (resource) {
     // Initially the clone is treated as suspended by the cache, because
@@ -1408,19 +1409,20 @@ int64_t FileMediaResource::Tell()
 
   int64_t offset = 0;
   // Return mSize as offset (end of stream) in case of error
   if (!mSeekable || NS_FAILED(mSeekable->Tell(&offset)))
     return mSize;
   return offset;
 }
 
-already_AddRefed<MediaResource>
-MediaResource::Create(MediaResourceCallback* aCallback,
-                      nsIChannel* aChannel, bool aIsPrivateBrowsing)
+already_AddRefed<BaseMediaResource>
+BaseMediaResource::Create(MediaResourceCallback* aCallback,
+                          nsIChannel* aChannel,
+                          bool aIsPrivateBrowsing)
 {
   NS_ASSERTION(NS_IsMainThread(),
                "MediaResource::Open called on non-main thread");
 
   // If the channel was redirected, we want the post-redirect URI;
   // but if the URI scheme was expanded, say from chrome: to jar:file:,
   // we want the original URI.
   nsCOMPtr<nsIURI> uri;
@@ -1429,17 +1431,17 @@ MediaResource::Create(MediaResourceCallb
 
   nsAutoCString contentTypeString;
   aChannel->GetContentType(contentTypeString);
   Maybe<MediaContainerType> containerType = MakeMediaContainerType(contentTypeString);
   if (!containerType) {
     return nullptr;
   }
 
-  RefPtr<MediaResource> resource;
+  RefPtr<BaseMediaResource> resource;
 
   // Let's try to create a FileMediaResource in case the channel is a nsIFile
   nsCOMPtr<nsIFileChannel> fc = do_QueryInterface(aChannel);
   if (fc) {
     resource = new FileMediaResource(aCallback, aChannel, uri);
   }
 
   // If the URL is blobURL with a seekable inputStream, we can still use a
diff --git a/dom/media/MediaResource.h b/dom/media/MediaResource.h
--- a/dom/media/MediaResource.h
+++ b/dom/media/MediaResource.h
@@ -174,24 +174,16 @@ public:
   // Get the current principal for the channel
   virtual already_AddRefed<nsIPrincipal> GetCurrentPrincipal() = 0;
   // If this returns false, then we shouldn't try to clone this MediaResource
   // because its underlying resources are not suitable for reuse (e.g.
   // because the underlying connection has been lost, or this resource
   // just can't be safely cloned). If this returns true, CloneData could
   // still fail. If this returns false, CloneData should not be called.
   virtual bool CanClone() { return false; }
-  // Create a new stream of the same type that refers to the same URI
-  // with a new channel. Any cached data associated with the original
-  // stream should be accessible in the new stream too.
-  virtual already_AddRefed<MediaResource> CloneData(
-    MediaResourceCallback* aCallback)
-  {
-    return nullptr;
-  }
 
   // These methods are called off the main thread.
   // The mode is initially MODE_PLAYBACK.
   virtual void SetReadMode(MediaCacheStream::ReadMode aMode) = 0;
   // This is the client's estimate of the playback rate assuming
   // the media plays continuously. The cache can't guess this itself
   // because it doesn't know when the decoder was paused, buffering, etc.
   virtual void SetPlaybackRate(uint32_t aBytesPerSecond) = 0;
@@ -289,24 +281,16 @@ public:
                                  int64_t aOffset,
                                  uint32_t aCount) = 0;
   // Returns true if the resource can be seeked to unbuffered ranges, i.e.
   // for an HTTP network stream this returns true if HTTP1.1 Byte Range
   // requests are supported by the connection/server.
   virtual bool IsTransportSeekable() = 0;
 
   /**
-   * Create a resource, reading data from the channel. Call on main thread only.
-   * The caller must follow up by calling resource->Open().
-   */
-  static already_AddRefed<MediaResource>
-  Create(MediaResourceCallback* aCallback,
-         nsIChannel* aChannel, bool aIsPrivateBrowsing);
-
-  /**
    * Open the stream. This creates a stream listener and returns it in
    * aStreamListener; this listener needs to be notified of incoming data.
    */
   virtual nsresult Open(nsIStreamListener** aStreamListener) = 0;
 
   /**
    * Fills aRanges with MediaByteRanges representing the data which is cached
    * in the media cache. Stream should be pinned during call and while
@@ -332,16 +316,34 @@ protected:
   virtual ~MediaResource() {};
 
 private:
   void Destroy();
 };
 
 class BaseMediaResource : public MediaResource {
 public:
+  /**
+   * Create a resource, reading data from the channel. Call on main thread only.
+   * The caller must follow up by calling resource->Open().
+   */
+  static already_AddRefed<BaseMediaResource> Create(
+    MediaResourceCallback* aCallback,
+    nsIChannel* aChannel,
+    bool aIsPrivateBrowsing);
+
+  // Create a new stream of the same type that refers to the same URI
+  // with a new channel. Any cached data associated with the original
+  // stream should be accessible in the new stream too.
+  virtual already_AddRefed<BaseMediaResource> CloneData(
+    MediaResourceCallback* aCallback)
+  {
+    return nullptr;
+  }
+
   void SetLoadInBackground(bool aLoadInBackground) override;
 
   size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const override
   {
     // Might be useful to track in the future:
     // - mChannel
     // - mURI (possibly owned, looks like just a ref from mChannel)
     // Not owned:
@@ -491,17 +493,18 @@ public:
   nsresult Open(nsIStreamListener** aStreamListener) override;
   nsresult Close() override;
   void     Suspend(bool aCloseImmediately) override;
   void     Resume() override;
   already_AddRefed<nsIPrincipal> GetCurrentPrincipal() override;
   // Return true if the stream has been closed.
   bool     IsClosed() const { return mCacheStream.IsClosed(); }
   bool     CanClone() override;
-  already_AddRefed<MediaResource> CloneData(MediaResourceCallback* aDecoder) override;
+  already_AddRefed<BaseMediaResource> CloneData(
+    MediaResourceCallback* aDecoder) override;
   nsresult ReadFromCache(char* aBuffer, int64_t aOffset, uint32_t aCount) override;
   void     EnsureCacheUpToDate() override;
 
   // Other thread
   void     SetReadMode(MediaCacheStream::ReadMode aMode) override;
   void     SetPlaybackRate(uint32_t aBytesPerSecond) override;
   nsresult ReadAt(int64_t offset, char* aBuffer,
                   uint32_t aCount, uint32_t* aBytes) override;
