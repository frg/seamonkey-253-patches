# HG changeset patch
# User Ya-Chieh Wu <ywu@mozilla.com>
# Date 1505260140 14400
# Node ID 405957d41476f08d4e8a0468fd073b1040759ffe
# Parent  3496427ab6252d7960513231c1db3669f5baa267
Bug 895096 - Part 3: Clean up twipsPerPixel to oneDevPixel. r=dbaron

MozReview-Commit-ID: 70aRd6KFpdd

diff --git a/layout/painting/nsCSSRendering.cpp b/layout/painting/nsCSSRendering.cpp
--- a/layout/painting/nsCSSRendering.cpp
+++ b/layout/painting/nsCSSRendering.cpp
@@ -745,23 +745,23 @@ ConstructBorderRenderer(nsPresContext* a
                "Should not skip sides for box-decoration-break:clone except "
                "::first-letter/line continuations or other frame types that "
                "don't have borders but those shouldn't reach this point. "
                "Overflow containers do reach this point though.");
     border.ApplySkipSides(aSkipSides);
   }
 
   // Convert to dev pixels.
-  nscoord twipsPerPixel = aPresContext->DevPixelsToAppUnits(1);
-  Rect joinedBorderAreaPx = NSRectToRect(joinedBorderArea, twipsPerPixel);
-  Float borderWidths[4] = { Float(border.top) / twipsPerPixel,
-                                   Float(border.right) / twipsPerPixel,
-                                   Float(border.bottom) / twipsPerPixel,
-                                   Float(border.left) / twipsPerPixel };
-  Rect dirtyRect = NSRectToRect(aDirtyRect, twipsPerPixel);
+  nscoord oneDevPixel = aPresContext->DevPixelsToAppUnits(1);
+  Rect joinedBorderAreaPx = NSRectToRect(joinedBorderArea, oneDevPixel);
+  Float borderWidths[4] = { Float(border.top) / oneDevPixel,
+                                   Float(border.right) / oneDevPixel,
+                                   Float(border.bottom) / oneDevPixel,
+                                   Float(border.left) / oneDevPixel };
+  Rect dirtyRect = NSRectToRect(aDirtyRect, oneDevPixel);
 
   uint8_t borderStyles[4];
   nscolor borderColors[4];
   nsBorderColors* compositeColors[4];
 
   // pull out styles, colors, composite colors
   NS_FOR_CSS_SIDES (i) {
     borderStyles[i] = aStyleBorder.GetBorderStyle(i);
@@ -995,25 +995,25 @@ nsCSSRendering::CreateBorderRendererForO
   nsRect outerRect = innerRect;
   outerRect.Inflate(width, width);
 
   // get the radius for our outline
   nsIFrame::ComputeBorderRadii(ourOutline->mOutlineRadius, aBorderArea.Size(),
                                outerRect.Size(), Sides(), twipsRadii);
 
   // Get our conversion values
-  nscoord twipsPerPixel = aPresContext->DevPixelsToAppUnits(1);
+  nscoord oneDevPixel = aPresContext->DevPixelsToAppUnits(1);
 
   // get the outer rectangles
-  Rect oRect(NSRectToRect(outerRect, twipsPerPixel));
+  Rect oRect(NSRectToRect(outerRect, oneDevPixel));
 
   // convert the radii
   nsMargin outlineMargin(width, width, width, width);
   RectCornerRadii outlineRadii;
-  ComputePixelRadii(twipsRadii, twipsPerPixel, &outlineRadii);
+  ComputePixelRadii(twipsRadii, oneDevPixel, &outlineRadii);
 
   uint8_t outlineStyle = ourOutline->mOutlineStyle;
   if (outlineStyle == NS_STYLE_BORDER_STYLE_AUTO) {
     if (nsLayoutUtils::IsOutlineStyleAutoEnabled()) {
       nsITheme* theme = aPresContext->GetTheme();
       if (theme && theme->ThemeSupportsWidget(aPresContext, aForFrame,
                                               NS_THEME_FOCUS_OUTLINE)) {
         theme->DrawWidgetBackground(aRenderingContext, aForFrame,
@@ -1038,21 +1038,21 @@ nsCSSRendering::CreateBorderRendererForO
   nscolor outlineColor =
     aStyleContext->GetVisitedDependentColor(&nsStyleOutline::mOutlineColor);
   nscolor outlineColors[4] = { outlineColor,
                                outlineColor,
                                outlineColor,
                                outlineColor };
 
   // convert the border widths
-  Float outlineWidths[4] = { Float(width) / twipsPerPixel,
-                             Float(width) / twipsPerPixel,
-                             Float(width) / twipsPerPixel,
-                             Float(width) / twipsPerPixel };
-  Rect dirtyRect = NSRectToRect(aDirtyRect, twipsPerPixel);
+  Float outlineWidths[4] = { Float(width) / oneDevPixel,
+                             Float(width) / oneDevPixel,
+                             Float(width) / oneDevPixel,
+                             Float(width) / oneDevPixel };
+  Rect dirtyRect = NSRectToRect(aDirtyRect, oneDevPixel);
 
   nsIDocument* document = nullptr;
   nsIContent* content = aForFrame->GetContent();
   if (content) {
     document = content->OwnerDoc();
   }
 
   DrawTarget* dt = aRenderingContext ? aRenderingContext->GetDrawTarget() : nullptr;
@@ -1436,24 +1436,24 @@ nsCSSRendering::GetShadowRect(const nsRe
 }
 
 bool
 nsCSSRendering::GetBorderRadii(const nsRect& aFrameRect,
                                const nsRect& aBorderRect,
                                nsIFrame* aFrame,
                                RectCornerRadii& aOutRadii)
 {
-  const nscoord twipsPerPixel = aFrame->PresContext()->DevPixelsToAppUnits(1);
+  const nscoord oneDevPixel = aFrame->PresContext()->DevPixelsToAppUnits(1);
   nscoord twipsRadii[8];
   NS_ASSERTION(aBorderRect.Size() == aFrame->VisualBorderRectRelativeToSelf().Size(),
               "unexpected size");
   nsSize sz = aFrameRect.Size();
   bool hasBorderRadius = aFrame->GetBorderRadii(sz, sz, Sides(), twipsRadii);
   if (hasBorderRadius) {
-    ComputePixelRadii(twipsRadii, twipsPerPixel, &aOutRadii);
+    ComputePixelRadii(twipsRadii, oneDevPixel, &aOutRadii);
   }
 
   return hasBorderRadius;
 }
 
 void
 nsCSSRendering::PaintBoxShadowOuter(nsPresContext* aPresContext,
                                     gfxContext& aRenderingContext,
@@ -1472,44 +1472,44 @@ nsCSSRendering::PaintBoxShadowOuter(nsPr
   bool nativeTheme = HasBoxShadowNativeTheme(aForFrame, hasBorderRadius);
   const nsStyleDisplay* styleDisplay = aForFrame->StyleDisplay();
 
   nsRect frameRect = GetShadowRect(aFrameArea, nativeTheme, aForFrame);
 
   // Get any border radius, since box-shadow must also have rounded corners if
   // the frame does.
   RectCornerRadii borderRadii;
-  const nscoord twipsPerPixel = aPresContext->DevPixelsToAppUnits(1);
+  const nscoord oneDevPixel = aPresContext->DevPixelsToAppUnits(1);
   if (hasBorderRadius) {
     nscoord twipsRadii[8];
     NS_ASSERTION(aFrameArea.Size() == aForFrame->VisualBorderRectRelativeToSelf().Size(),
                  "unexpected size");
     nsSize sz = frameRect.Size();
     hasBorderRadius = aForFrame->GetBorderRadii(sz, sz, Sides(), twipsRadii);
     if (hasBorderRadius) {
-      ComputePixelRadii(twipsRadii, twipsPerPixel, &borderRadii);
+      ComputePixelRadii(twipsRadii, oneDevPixel, &borderRadii);
     }
   }
 
 
   // We don't show anything that intersects with the frame we're blurring on. So tell the
   // blurrer not to do unnecessary work there.
-  gfxRect skipGfxRect = ThebesRect(NSRectToRect(frameRect, twipsPerPixel));
+  gfxRect skipGfxRect = ThebesRect(NSRectToRect(frameRect, oneDevPixel));
   skipGfxRect.Round();
   bool useSkipGfxRect = true;
   if (nativeTheme) {
     // Optimize non-leaf native-themed frames by skipping computing pixels
     // in the padding-box. We assume the padding-box is going to be painted
     // opaquely for non-leaf frames.
     // XXX this may not be a safe assumption; we should make this go away
     // by optimizing box-shadow drawing more for the cases where we don't have a skip-rect.
     useSkipGfxRect = !aForFrame->IsLeaf();
     nsRect paddingRect =
       aForFrame->GetPaddingRect() - aForFrame->GetPosition() + aFrameArea.TopLeft();
-    skipGfxRect = nsLayoutUtils::RectToGfxRect(paddingRect, twipsPerPixel);
+    skipGfxRect = nsLayoutUtils::RectToGfxRect(paddingRect, oneDevPixel);
   } else if (hasBorderRadius) {
     skipGfxRect.Deflate(gfxMargin(
         std::max(borderRadii[C_TL].height, borderRadii[C_TR].height), 0,
         std::max(borderRadii[C_BL].height, borderRadii[C_BR].height), 0));
   }
 
 
   for (uint32_t i = shadows->Length(); i > 0; --i) {
@@ -1523,35 +1523,35 @@ nsCSSRendering::PaintBoxShadowOuter(nsPr
       shadowRect.Inflate(shadowItem->mSpread, shadowItem->mSpread);
     }
 
     // shadowRect won't include the blur, so make an extra rect here that includes the blur
     // for use in the even-odd rule below.
     nsRect shadowRectPlusBlur = shadowRect;
     nscoord blurRadius = shadowItem->mRadius;
     shadowRectPlusBlur.Inflate(
-      nsContextBoxBlur::GetBlurRadiusMargin(blurRadius, twipsPerPixel));
+      nsContextBoxBlur::GetBlurRadiusMargin(blurRadius, oneDevPixel));
 
     Rect shadowGfxRectPlusBlur =
-      NSRectToRect(shadowRectPlusBlur, twipsPerPixel);
+      NSRectToRect(shadowRectPlusBlur, oneDevPixel);
     shadowGfxRectPlusBlur.RoundOut();
     MaybeSnapToDevicePixels(shadowGfxRectPlusBlur, aDrawTarget, true);
 
     Color gfxShadowColor = GetShadowColor(shadowItem, aForFrame, aOpacity);
 
     if (nativeTheme) {
       nsContextBoxBlur blurringArea;
 
       // When getting the widget shape from the native theme, we're going
       // to draw the widget into the shadow surface to create a mask.
       // We need to ensure that there actually *is* a shadow surface
       // and that we're not going to draw directly into aRenderingContext.
       gfxContext* shadowContext =
         blurringArea.Init(shadowRect, shadowItem->mSpread, blurRadius,
-                          twipsPerPixel, &aRenderingContext, aDirtyRect,
+                          oneDevPixel, &aRenderingContext, aDirtyRect,
                           useSkipGfxRect ? &skipGfxRect : nullptr,
                           nsContextBoxBlur::FORCE_MASK);
       if (!shadowContext)
         continue;
 
       MOZ_ASSERT(shadowContext == blurringArea.GetContext());
 
       aRenderingContext.Save();
@@ -1582,17 +1582,17 @@ nsCSSRendering::PaintBoxShadowOuter(nsPr
           styleDisplay->mAppearance, aFrameArea, nativeRect);
 
       blurringArea.DoPaint();
       aRenderingContext.Restore();
     } else {
       aRenderingContext.Save();
 
       {
-        Rect innerClipRect = NSRectToRect(frameRect, twipsPerPixel);
+        Rect innerClipRect = NSRectToRect(frameRect, oneDevPixel);
         if (!MaybeSnapToDevicePixels(innerClipRect, aDrawTarget, true)) {
           innerClipRect.Round();
         }
 
         // Clip out the interior of the frame's border edge so that the shadow
         // is only painted outside that area.
         RefPtr<PathBuilder> builder =
           aDrawTarget.CreatePathBuilder(FillRule::FILL_EVEN_ODD);
@@ -1638,32 +1638,32 @@ nsCSSRendering::PaintBoxShadowOuter(nsPr
       fragmentClip = fragmentClip.Intersect(aDirtyRect);
       aRenderingContext.
         Clip(NSRectToSnappedRect(fragmentClip,
                                  aForFrame->PresContext()->AppUnitsPerDevPixel(),
                                  aDrawTarget));
 
       RectCornerRadii clipRectRadii;
       if (hasBorderRadius) {
-        Float spreadDistance = Float(shadowItem->mSpread) / twipsPerPixel;
+        Float spreadDistance = Float(shadowItem->mSpread) / oneDevPixel;
 
         Float borderSizes[4];
 
         borderSizes[eSideLeft] = spreadDistance;
         borderSizes[eSideTop] = spreadDistance;
         borderSizes[eSideRight] = spreadDistance;
         borderSizes[eSideBottom] = spreadDistance;
 
         nsCSSBorderRenderer::ComputeOuterRadii(borderRadii, borderSizes,
             &clipRectRadii);
 
       }
       nsContextBoxBlur::BlurRectangle(&aRenderingContext,
                                       shadowRect,
-                                      twipsPerPixel,
+                                      oneDevPixel,
                                       hasBorderRadius ? &clipRectRadii : nullptr,
                                       blurRadius,
                                       gfxShadowColor,
                                       aDirtyRect,
                                       skipGfxRect);
       aRenderingContext.Restore();
     }
 
@@ -1711,29 +1711,29 @@ nsCSSRendering::GetShadowInnerRadii(nsIF
   // Get any border radius, since box-shadow must also have rounded corners
   // if the frame does.
   nscoord twipsRadii[8];
   nsRect frameRect =
     ::BoxDecorationRectForBorder(aFrame, aFrameArea, aFrame->GetSkipSides());
   nsSize sz = frameRect.Size();
   nsMargin border = aFrame->GetUsedBorder();
   bool hasBorderRadius = aFrame->GetBorderRadii(sz, sz, Sides(), twipsRadii);
-  const nscoord twipsPerPixel = aFrame->PresContext()->DevPixelsToAppUnits(1);
+  const nscoord oneDevPixel = aFrame->PresContext()->DevPixelsToAppUnits(1);
 
   RectCornerRadii borderRadii;
 
   hasBorderRadius = GetBorderRadii(frameRect, aFrameArea, aFrame, borderRadii);
   if (hasBorderRadius) {
-    ComputePixelRadii(twipsRadii, twipsPerPixel, &borderRadii);
+    ComputePixelRadii(twipsRadii, oneDevPixel, &borderRadii);
 
     Float borderSizes[4] = {
-      Float(border.top) / twipsPerPixel,
-      Float(border.right) / twipsPerPixel,
-      Float(border.bottom) / twipsPerPixel,
-      Float(border.left) / twipsPerPixel
+      Float(border.top) / oneDevPixel,
+      Float(border.right) / oneDevPixel,
+      Float(border.bottom) / oneDevPixel,
+      Float(border.left) / oneDevPixel
     };
     nsCSSBorderRenderer::ComputeInnerRadii(borderRadii,
                                            borderSizes,
                                            &aOutInnerRadii);
   }
 
   return hasBorderRadius;
 }
@@ -1754,45 +1754,45 @@ nsCSSRendering::PaintBoxShadowInner(nsPr
 
   nsRect paddingRect = GetBoxShadowInnerPaddingRect(aForFrame, aFrameArea);
 
   RectCornerRadii innerRadii;
   bool hasBorderRadius = GetShadowInnerRadii(aForFrame,
                                              aFrameArea,
                                              innerRadii);
 
-  const nscoord twipsPerPixel = aPresContext->DevPixelsToAppUnits(1);
+  const nscoord oneDevPixel = aPresContext->DevPixelsToAppUnits(1);
 
   for (uint32_t i = shadows->Length(); i > 0; --i) {
     nsCSSShadowItem* shadowItem = shadows->ShadowAt(i - 1);
     if (!shadowItem->mInset)
       continue;
 
     // shadowPaintRect: the area to paint on the temp surface
     // shadowClipRect: the area on the temporary surface within shadowPaintRect
     //                 that we will NOT paint in
     nscoord blurRadius = shadowItem->mRadius;
     nsMargin blurMargin =
-      nsContextBoxBlur::GetBlurRadiusMargin(blurRadius, twipsPerPixel);
+      nsContextBoxBlur::GetBlurRadiusMargin(blurRadius, oneDevPixel);
     nsRect shadowPaintRect = paddingRect;
     shadowPaintRect.Inflate(blurMargin);
 
     // Round the spread radius to device pixels (by truncation).
     // This mostly matches what we do for borders, except that we don't round
     // up values between zero and one device pixels to one device pixel.
     // This way of rounding is symmetric around zero, which makes sense for
     // the spread radius.
-    int32_t spreadDistance = shadowItem->mSpread / twipsPerPixel;
+    int32_t spreadDistance = shadowItem->mSpread / oneDevPixel;
     nscoord spreadDistanceAppUnits = aPresContext->DevPixelsToAppUnits(spreadDistance);
 
     nsRect shadowClipRect = paddingRect;
     shadowClipRect.MoveBy(shadowItem->mXOffset, shadowItem->mYOffset);
     shadowClipRect.Deflate(spreadDistanceAppUnits, spreadDistanceAppUnits);
 
-    Rect shadowClipGfxRect = NSRectToRect(shadowClipRect, twipsPerPixel);
+    Rect shadowClipGfxRect = NSRectToRect(shadowClipRect, oneDevPixel);
     shadowClipGfxRect.Round();
 
     RectCornerRadii clipRectRadii;
     if (hasBorderRadius) {
       // Calculate the radii the inner clipping rect will have
       Float borderSizes[4] = {0, 0, 0, 0};
 
       // See PaintBoxShadowOuter and bug 514670
@@ -1815,57 +1815,57 @@ nsCSSRendering::PaintBoxShadowInner(nsPr
       nsCSSBorderRenderer::ComputeInnerRadii(innerRadii, borderSizes,
                                              &clipRectRadii);
     }
 
     // Set the "skip rect" to the area within the frame that we don't paint in,
     // including after blurring.
     nsRect skipRect = shadowClipRect;
     skipRect.Deflate(blurMargin);
-    gfxRect skipGfxRect = nsLayoutUtils::RectToGfxRect(skipRect, twipsPerPixel);
+    gfxRect skipGfxRect = nsLayoutUtils::RectToGfxRect(skipRect, oneDevPixel);
     if (hasBorderRadius) {
       skipGfxRect.Deflate(gfxMargin(
           std::max(clipRectRadii[C_TL].height, clipRectRadii[C_TR].height), 0,
           std::max(clipRectRadii[C_BL].height, clipRectRadii[C_BR].height), 0));
     }
 
     // When there's a blur radius, gfxAlphaBoxBlur leaves the skiprect area
     // unchanged. And by construction the gfxSkipRect is not touched by the
     // rendered shadow (even after blurring), so those pixels must be completely
     // transparent in the shadow, so drawing them changes nothing.
     DrawTarget* drawTarget = aRenderingContext.GetDrawTarget();
 
     // Clip the context to the area of the frame's padding rect, so no part of the
     // shadow is painted outside. Also cut out anything beyond where the inset shadow
     // will be.
-    Rect shadowGfxRect = NSRectToRect(paddingRect, twipsPerPixel);
+    Rect shadowGfxRect = NSRectToRect(paddingRect, oneDevPixel);
     shadowGfxRect.Round();
 
     Color shadowColor = GetShadowColor(shadowItem, aForFrame, 1.0);
     aRenderingContext.Save();
 
     // This clips the outside border radius.
     // clipRectRadii is the border radius inside the inset shadow.
     if (hasBorderRadius) {
       RefPtr<Path> roundedRect =
         MakePathForRoundedRect(*drawTarget, shadowGfxRect, innerRadii);
       aRenderingContext.Clip(roundedRect);
     } else {
       aRenderingContext.Clip(shadowGfxRect);
     }
 
     nsContextBoxBlur insetBoxBlur;
-    gfxRect destRect = nsLayoutUtils::RectToGfxRect(shadowPaintRect, twipsPerPixel);
-    Point shadowOffset(shadowItem->mXOffset / twipsPerPixel,
-                       shadowItem->mYOffset / twipsPerPixel);
+    gfxRect destRect = nsLayoutUtils::RectToGfxRect(shadowPaintRect, oneDevPixel);
+    Point shadowOffset(shadowItem->mXOffset / oneDevPixel,
+                       shadowItem->mYOffset / oneDevPixel);
 
     insetBoxBlur.InsetBoxBlur(&aRenderingContext, ToRect(destRect),
                               shadowClipGfxRect, shadowColor,
                               blurRadius, spreadDistanceAppUnits,
-                              twipsPerPixel, hasBorderRadius,
+                              oneDevPixel, hasBorderRadius,
                               clipRectRadii, ToRect(skipGfxRect),
                               shadowOffset);
     aRenderingContext.Restore();
   }
 }
 
 /* static */
 nsCSSRendering::PaintBGParams
@@ -3294,36 +3294,37 @@ nsCSSRendering::GetBackgroundLayerRect(n
 }
 
 // Begin table border-collapsing section
 // These functions were written to not disrupt the normal ones and yet satisfy some additional requirements
 // At some point, all functions should be unified to include the additional functionality that these provide
 
 static nscoord
 RoundIntToPixel(nscoord aValue,
-                nscoord aTwipsPerPixel,
+                nscoord aOneDevPixel,
                 bool    aRoundDown = false)
 {
-  if (aTwipsPerPixel <= 0)
-    // We must be rendering to a device that has a resolution greater than Twips!
+  if (aOneDevPixel <= 0)
+    // We must be rendering to a device that has a resolution greater than
+    // one device pixel!
     // In that case, aValue is as accurate as it's going to get.
     return aValue;
 
-  nscoord halfPixel = NSToCoordRound(aTwipsPerPixel / 2.0f);
-  nscoord extra = aValue % aTwipsPerPixel;
-  nscoord finalValue = (!aRoundDown && (extra >= halfPixel)) ? aValue + (aTwipsPerPixel - extra) : aValue - extra;
+  nscoord halfPixel = NSToCoordRound(aOneDevPixel / 2.0f);
+  nscoord extra = aValue % aOneDevPixel;
+  nscoord finalValue = (!aRoundDown && (extra >= halfPixel)) ? aValue + (aOneDevPixel - extra) : aValue - extra;
   return finalValue;
 }
 
 static nscoord
 RoundFloatToPixel(float   aValue,
-                  nscoord aTwipsPerPixel,
+                  nscoord aOneDevPixel,
                   bool    aRoundDown = false)
 {
-  return RoundIntToPixel(NSToCoordRound(aValue), aTwipsPerPixel, aRoundDown);
+  return RoundIntToPixel(NSToCoordRound(aValue), aOneDevPixel, aRoundDown);
 }
 
 static void SetPoly(const Rect& aRect, Point* poly)
 {
   poly[0].x = aRect.x;
   poly[0].y = aRect.y;
   poly[1].x = aRect.x + aRect.width;
   poly[1].y = aRect.y;
@@ -3438,31 +3439,31 @@ DrawSolidBorderSegment(DrawTarget&      
     RefPtr<Path> path = builder->Finish();
     aDrawTarget.Fill(path, color, drawOptions);
   }
 }
 
 static void
 GetDashInfo(nscoord  aBorderLength,
             nscoord  aDashLength,
-            nscoord  aTwipsPerPixel,
+            nscoord  aOneDevPixel,
             int32_t& aNumDashSpaces,
             nscoord& aStartDashLength,
             nscoord& aEndDashLength)
 {
   aNumDashSpaces = 0;
   if (aStartDashLength + aDashLength + aEndDashLength >= aBorderLength) {
     aStartDashLength = aBorderLength;
     aEndDashLength = 0;
   }
   else {
     aNumDashSpaces = (aBorderLength - aDashLength)/ (2 * aDashLength); // round down
     nscoord extra = aBorderLength - aStartDashLength - aEndDashLength - (((2 * aNumDashSpaces) - 1) * aDashLength);
     if (extra > 0) {
-      nscoord half = RoundIntToPixel(extra / 2, aTwipsPerPixel);
+      nscoord half = RoundIntToPixel(extra / 2, aOneDevPixel);
       aStartDashLength += half;
       aEndDashLength += (extra - half);
     }
   }
 }
 
 void
 nsCSSRendering::DrawTableBorderSegment(DrawTarget&   aDrawTarget,
