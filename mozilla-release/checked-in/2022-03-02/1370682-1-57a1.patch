# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1502388001 14400
#      Thu Aug 10 14:00:01 2017 -0400
# Node ID d403daf53feaf699906c9248b03e8994fc5f8b44
# Parent  741824215543cbc50052f67a9c99decb1d3e7606
Bug 1370682 - Allow passing in LayoutDeviceSize to wr::ToBorderRadius for convenience. r=jrmuizel,mstange

There's no real reason to restrict this to LayerSize, since we use
use Layer and LayoutDevice coordinate spaces interchangeably for
webrender. With the templating of the function the compiler doesn't know
what type to use for {0, 0} so I added a new function to deal with that.

MozReview-Commit-ID: BA83Sy9UjYH

diff --git a/gfx/webrender_bindings/WebRenderTypes.h b/gfx/webrender_bindings/WebRenderTypes.h
--- a/gfx/webrender_bindings/WebRenderTypes.h
+++ b/gfx/webrender_bindings/WebRenderTypes.h
@@ -8,16 +8,17 @@
 #define GFX_WEBRENDERTYPES_H
 
 #include "mozilla/webrender/webrender_ffi.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/gfx/Matrix.h"
 #include "mozilla/gfx/Types.h"
 #include "mozilla/gfx/Tools.h"
 #include "mozilla/layers/LayersTypes.h"
+#include "mozilla/PodOperations.h"
 #include "mozilla/Range.h"
 #include "Units.h"
 #include "RoundedRect.h"
 #include "nsStyleConsts.h"
 
 //#define ENABLE_FRAME_LATENCY_LOG
 
 namespace mozilla {
@@ -350,18 +351,26 @@ static inline wr::BorderRadius ToUniform
   wr::BorderRadius br;
   br.top_left = ToLayoutSize(aSize);
   br.top_right = ToLayoutSize(aSize);
   br.bottom_left = ToLayoutSize(aSize);
   br.bottom_right = ToLayoutSize(aSize);
   return br;
 }
 
-static inline wr::BorderRadius ToBorderRadius(const mozilla::LayerSize& topLeft, const mozilla::LayerSize& topRight,
-                                              const mozilla::LayerSize& bottomLeft, const mozilla::LayerSize& bottomRight)
+static inline wr::BorderRadius EmptyBorderRadius()
+{
+  wr::BorderRadius br;
+  PodZero(&br);
+  return br;
+}
+
+template<class T>
+static inline wr::BorderRadius ToBorderRadius(const gfx::SizeTyped<T>& topLeft, const gfx::SizeTyped<T>& topRight,
+                                              const gfx::SizeTyped<T>& bottomLeft, const gfx::SizeTyped<T>& bottomRight)
 {
   wr::BorderRadius br;
   br.top_left = ToLayoutSize(topLeft);
   br.top_right = ToLayoutSize(topRight);
   br.bottom_left = ToLayoutSize(bottomLeft);
   br.bottom_right = ToLayoutSize(bottomRight);
   return br;
 }
diff --git a/layout/tables/nsTableFrame.cpp b/layout/tables/nsTableFrame.cpp
--- a/layout/tables/nsTableFrame.cpp
+++ b/layout/tables/nsTableFrame.cpp
@@ -7455,17 +7455,17 @@ BCBlockDirSeg::CreateWebRenderCommands(B
                                                                                param->mAppUnitsPerDevPixel));
   wr::LayoutRect transformedRect = aSc.ToRelativeLayoutRect(borderRect);
   wr::BorderSide wrSide[4];
   NS_FOR_CSS_SIDES(i) {
     wrSide[i] = wr::ToBorderSide(ToDeviceColor(param->mBorderColor), NS_STYLE_BORDER_STYLE_NONE);
   }
   wrSide[eSideLeft] = wr::ToBorderSide(ToDeviceColor(param->mBorderColor), param->mBorderStyle);
 
-  wr::BorderRadius borderRadii = wr::ToBorderRadius( {0, 0}, {0, 0}, {0, 0}, {0, 0} );
+  wr::BorderRadius borderRadii = wr::EmptyBorderRadius();
 
   // All border style is set to none except left side. So setting the widths of
   // each side to width of rect is fine.
   wr::BorderWidths borderWidths = wr::ToBorderWidths(transformedRect.size.width,
                                                      transformedRect.size.width,
                                                      transformedRect.size.width,
                                                      transformedRect.size.width);
   transformedRect.size.width *= 2.0f;
@@ -7715,17 +7715,17 @@ BCInlineDirSeg::CreateWebRenderCommands(
                                                                                param->mAppUnitsPerDevPixel));
   wr::LayoutRect transformedRect = aSc.ToRelativeLayoutRect(borderRect);
   wr::BorderSide wrSide[4];
   NS_FOR_CSS_SIDES(i) {
     wrSide[i] = wr::ToBorderSide(ToDeviceColor(param->mBorderColor), NS_STYLE_BORDER_STYLE_NONE);
   }
   wrSide[eSideTop] = wr::ToBorderSide(ToDeviceColor(param->mBorderColor), param->mBorderStyle);
 
-  wr::BorderRadius borderRadii = wr::ToBorderRadius( {0, 0}, {0, 0}, {0, 0}, {0, 0} );
+  wr::BorderRadius borderRadii = wr::EmptyBorderRadius();
 
   // All border style is set to none except top side. So setting the widths of
   // each side to height of rect is fine.
   wr::BorderWidths borderWidths = wr::ToBorderWidths(transformedRect.size.height,
                                                      transformedRect.size.height,
                                                      transformedRect.size.height,
                                                      transformedRect.size.height);
   transformedRect.size.height *= 2.0f;
