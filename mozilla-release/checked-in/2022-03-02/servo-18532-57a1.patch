# HG changeset patch
# User Bobby Holley <bobbyholley@gmail.com>
# Date 1505545760 18000
#      Sat Sep 16 02:09:20 2017 -0500
# Node ID b1e13cff22b9fa9ed414cf60f44b850e95603e1f
# Parent  55029d194eec2543a7df9f2626d802ab2338d7cd
servo: Merge #18532 - Undo #18497 in the MatchAndCascade case (from bholley:more_cache_insertion); r=emilio

It's easy to construct examples where not inserting in those cases causes performance
to get worse (for example, any long list of siblings that match the same selectors
while having some non-effectual differences in LocalName/Class/Id/etc). And the LRU
nature of the cache already does the right thing of pruning non-useful entries.

Fixing this causes several hundred more sharing hits on wikipedia.

Source-Repo: https://github.com/servo/servo
Source-Revision: da64abc4298549362debc96e5843bd36e6d23b7f

diff --git a/servo/components/style/sharing/mod.rs b/servo/components/style/sharing/mod.rs
--- a/servo/components/style/sharing/mod.rs
+++ b/servo/components/style/sharing/mod.rs
@@ -549,21 +549,16 @@ impl<E: TElement> StyleSharingCache<E> {
     /// to avoid memmoving at each function call. See rust issue #42763.
     pub fn insert_if_possible(
         &mut self,
         element: &E,
         style: &PrimaryStyle,
         validation_data_holder: Option<&mut StyleSharingTarget<E>>,
         dom_depth: usize,
     ) {
-        if style.0.reused_via_rule_node {
-            debug!("Failing to insert into the cached: this was a cached style");
-            return;
-        }
-
         let parent = match element.traversal_parent() {
             Some(element) => element,
             None => {
                 debug!("Failing to insert to the cache: no parent element");
                 return;
             }
         };
 
diff --git a/servo/components/style/traversal.rs b/servo/components/style/traversal.rs
--- a/servo/components/style/traversal.rs
+++ b/servo/components/style/traversal.rs
@@ -718,22 +718,39 @@ where
                         context,
                         RuleInclusion::All,
                         PseudoElementResolution::IfApplicable
                     );
 
                 resolver.cascade_styles_with_default_parents(cascade_inputs)
             };
 
-            context.thread_local.sharing_cache.insert_if_possible(
-                &element,
-                &new_styles.primary,
-                None,
-                traversal_data.current_dom_depth,
-            );
+            // Insert into the cache, but only if this style isn't reused from a
+            // sibling or cousin. Otherwise, recascading a bunch of identical
+            // elements would unnecessarily flood the cache with identical entries.
+            //
+            // This is analagous to the obvious "don't insert an element that just
+            // got a hit in the style sharing cache" behavior in the MatchAndCascade
+            // handling above.
+            //
+            // Note that, for the MatchAndCascade path, we still insert elements that
+            // shared styles via the rule node, because we know that there's something
+            // different about them that caused them to miss the sharing cache before
+            // selector matching. If we didn't, we would still end up with the same
+            // number of eventual styles, but would potentially miss out on various
+            // opportunities for skipping selector matching, which could hurt
+            // performance.
+            if !new_styles.primary.0.reused_via_rule_node {
+                context.thread_local.sharing_cache.insert_if_possible(
+                    &element,
+                    &new_styles.primary,
+                    None,
+                    traversal_data.current_dom_depth,
+                );
+            }
 
             new_styles
         }
     };
 
     element.finish_restyle(
         context,
         data,
