# HG changeset patch
# User Boris Chiou <boris.chiou@gmail.com>
# Date 1558462636 0
# Node ID b9ce11f1880ab7650c4ed80b097a6a440a9d5453
# Parent  cff730336f214b55fd62f2ff1abc8a5931d7cdac
Bug 1552911 - Drop the constructor of ResizeObserverEntry. r=dholbert,smaug

There is a spec issue about should we expose this API:
https://github.com/w3c/csswg-drafts/issues/3946

It's no clear that should we really need this API, so let's match
Chromium for now.

Differential Revision: https://phabricator.services.mozilla.com/D31891

diff --git a/dom/base/ResizeObserver.cpp b/dom/base/ResizeObserver.cpp
--- a/dom/base/ResizeObserver.cpp
+++ b/dom/base/ResizeObserver.cpp
@@ -1,16 +1,17 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim:set ts=2 sw=2 sts=2 et cindent: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/dom/ResizeObserver.h"
 
+#include "mozilla/dom/DOMRect.h"
 #include "nsIDocument.h"
 #include "nsIContentInlines.h"
 #include "nsSVGUtils.h"
 #include <limits>
 
 namespace mozilla {
 namespace dom {
 
@@ -266,38 +267,31 @@ NS_IMPL_CYCLE_COLLECTION_WRAPPERCACHE(Re
                                       mContentBoxSize)
 NS_IMPL_CYCLE_COLLECTING_ADDREF(ResizeObserverEntry)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(ResizeObserverEntry)
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(ResizeObserverEntry)
   NS_WRAPPERCACHE_INTERFACE_MAP_ENTRY
   NS_INTERFACE_MAP_ENTRY(nsISupports)
 NS_INTERFACE_MAP_END
 
-already_AddRefed<ResizeObserverEntry> ResizeObserverEntry::Constructor(
-    const GlobalObject& aGlobal, Element& aTarget, ErrorResult& aRv) {
-  RefPtr<ResizeObserverEntry> observerEntry =
-      new ResizeObserverEntry(aGlobal.GetAsSupports(), aTarget);
-  return observerEntry.forget();
-}
-
 void ResizeObserverEntry::SetBorderBoxSize(const nsSize& aSize) {
   nsIFrame* frame = mTarget->GetPrimaryFrame();
   const WritingMode wm = frame ? frame->GetWritingMode() : WritingMode();
   mBorderBoxSize = new ResizeObserverSize(this, aSize, wm);
 }
 
 void ResizeObserverEntry::SetContentRectAndSize(const nsSize& aSize) {
   nsIFrame* frame = mTarget->GetPrimaryFrame();
 
   // 1. Update mContentRect.
   nsMargin padding = frame ? frame->GetUsedPadding(): nsMargin();
   // Per the spec, we need to use the top-left padding offset as the origin of
   // our contentRect.
   nsRect rect(nsPoint(padding.left, padding.top), aSize);
-  RefPtr<DOMRect> contentRect = new DOMRect(mTarget);
+  RefPtr<DOMRect> contentRect = new DOMRect(this);
   contentRect->SetLayoutRect(rect);
   mContentRect = contentRect.forget();
 
   // 2. Update mContentBoxSize.
   const WritingMode wm = frame ? frame->GetWritingMode() : WritingMode();
   mContentBoxSize = new ResizeObserverSize(this, aSize, wm);
 }
 
diff --git a/dom/base/ResizeObserver.h b/dom/base/ResizeObserver.h
--- a/dom/base/ResizeObserver.h
+++ b/dom/base/ResizeObserver.h
@@ -183,19 +183,16 @@ class ResizeObserverEntry final : public
 
   nsISupports* GetParentObject() const { return mOwner; }
 
   JSObject* WrapObject(JSContext* aCx,
                        JS::Handle<JSObject*> aGivenProto) override {
     return ResizeObserverEntryBinding::Wrap(aCx, this, aGivenProto);
   }
 
-  static already_AddRefed<ResizeObserverEntry> Constructor(
-      const GlobalObject& global, Element& target, ErrorResult& aRv);
-
   Element* Target() const { return mTarget; }
 
   /**
    * Returns the DOMRectReadOnly of target's content rect so it can be
    * accessed from JavaScript in callback function of ResizeObserver.
    */
   DOMRectReadOnly* ContentRect() const { return mContentRect; }
 
diff --git a/dom/webidl/ResizeObserver.webidl b/dom/webidl/ResizeObserver.webidl
--- a/dom/webidl/ResizeObserver.webidl
+++ b/dom/webidl/ResizeObserver.webidl
@@ -24,18 +24,17 @@ interface ResizeObserver {
     void observe(Element target, optional ResizeObserverOptions options);
     [Throws]
     void unobserve(Element target);
     void disconnect();
 };
 
 callback ResizeObserverCallback = void (sequence<ResizeObserverEntry> entries, ResizeObserver observer);
 
-[Constructor(Element target),
- Pref="layout.css.resizeobserver.enabled"]
+[Pref="layout.css.resizeobserver.enabled"]
 interface ResizeObserverEntry {
     readonly attribute Element target;
     readonly attribute DOMRectReadOnly contentRect;
     readonly attribute ResizeObserverSize borderBoxSize;
     readonly attribute ResizeObserverSize contentBoxSize;
 };
 
 [Pref="layout.css.resizeobserver.enabled"]
diff --git a/layout/style/crashtests/1552911.html b/layout/style/crashtests/1552911.html
new file mode 100644
--- /dev/null
+++ b/layout/style/crashtests/1552911.html
@@ -0,0 +1,15 @@
+<html>
+<head>
+  <script>
+    function start() {
+      const o1 =
+        document.createElementNS('http://www.w3.org/1999/xhtml', 'slot');
+      const observer = new ResizeObserverEntry(o1);
+      typeof observer.borderBoxSize;
+      typeof observer.contentBoxSize;
+    }
+
+    window.addEventListener('load', start);
+  </script>
+</head>
+</html>
diff --git a/layout/style/crashtests/crashtests.list b/layout/style/crashtests/crashtests.list
--- a/layout/style/crashtests/crashtests.list
+++ b/layout/style/crashtests/crashtests.list
@@ -197,8 +197,9 @@ load long-url-list-stack-overflow.html
 skip-if(Android&&AndroidVersion<21) load 1383981.html
 skip-if(Android&&AndroidVersion<21) load 1383981-2.html
 # skip-if(Android&&AndroidVersion<21) load 1383981-3.html
 load 1384824-1.html
 load 1384824-2.html
 load 1387481-1.html
 load 1387499.html
 load 1391577.html
+pref(layout.css.resizeobserver.enabled,true) load 1552911.html
\ No newline at end of file
diff --git a/testing/web-platform/meta/resize-observer/idlharness.window.js.ini.1552911.later b/testing/web-platform/meta/resize-observer/idlharness.window.js.ini.1552911.later
new file mode 100644
--- /dev/null
+++ b/testing/web-platform/meta/resize-observer/idlharness.window.js.ini.1552911.later
@@ -0,0 +1,15 @@
+--- idlharness.window.js.ini
++++ idlharness.window.js.ini
+@@ -41,8 +41,12 @@
+ 
+   [ResizeObservation interface: attribute lastReportedSize]
+     expected: FAIL
+     bug: https://github.com/w3c/csswg-drafts/issues/3839
+ 
+   [ResizeObservation interface: attribute observedBox]
+     expected: FAIL
+     bug: https://github.com/w3c/csswg-drafts/issues/3839
++
++  [ResizeObserverEntry interface object length]
++    expected: FAIL
++    bug: https://github.com/w3c/csswg-drafts/issues/3946
