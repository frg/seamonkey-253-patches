# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1503315971 18000
#      Mon Aug 21 06:46:11 2017 -0500
# Node ID e1375528315fc63622fddec845cb178f7702392e
# Parent  36654023b55875ef17dac922f7e952dcf66626cf
servo: Merge #18139 - Update parser state only after rule is successfully parsed (from upsuper:parser-state); r=SimonSapin

This is expected to fix several web-platform tests mentioned in [bug 1389187](https://bugzilla.mozilla.org/show_bug.cgi?id=1389187).

Source-Repo: https://github.com/servo/servo
Source-Revision: 430b171f88c91141a8e4c76348a983e079594739

diff --git a/servo/components/style/stylesheets/rule_parser.rs b/servo/components/style/stylesheets/rule_parser.rs
--- a/servo/components/style/stylesheets/rule_parser.rs
+++ b/servo/components/style/stylesheets/rule_parser.rs
@@ -164,17 +164,16 @@ impl<'a, 'i> AtRuleParser<'i> for TopLev
         match_ignore_ascii_case! { &*name,
             "import" => {
                 if self.state > State::Imports {
                     // "@import must be before any rule but @charset"
                     self.had_hierarchy_error = true;
                     return Err(StyleParseError::UnexpectedImportRule.into())
                 }
 
-                self.state = State::Imports;
                 let url_string = input.expect_url_or_string()?.as_ref().to_owned();
                 let specified_url = SpecifiedUrl::parse_from_string(url_string, &self.context)?;
 
                 let media = parse_media_query_list(&self.context, input);
                 let media = Arc::new(self.shared_lock.wrap(media));
 
                 let loader =
                     self.loader.expect("Expected a stylesheet loader for @import");
@@ -182,25 +181,25 @@ impl<'a, 'i> AtRuleParser<'i> for TopLev
                 let import_rule = loader.request_stylesheet(
                     specified_url,
                     location,
                     &self.context,
                     &self.shared_lock,
                     media,
                 );
 
+                self.state = State::Imports;
                 return Ok(AtRuleType::WithoutBlock(CssRule::Import(import_rule)))
             },
             "namespace" => {
                 if self.state > State::Namespaces {
                     // "@namespace must be before any rule but @charset and @import"
                     self.had_hierarchy_error = true;
                     return Err(StyleParseError::UnexpectedNamespaceRule.into())
                 }
-                self.state = State::Namespaces;
 
                 let prefix_result = input.try(|i| i.expect_ident_cloned());
                 let maybe_namespace = match input.expect_url_or_string() {
                     Ok(url_or_string) => url_or_string,
                     Err(BasicParseError::UnexpectedToken(t)) =>
                         return Err(StyleParseError::UnexpectedTokenWithinNamespace(t).into()),
                     Err(e) => return Err(e.into()),
                 };
@@ -215,44 +214,45 @@ impl<'a, 'i> AtRuleParser<'i> for TopLev
                         .prefixes
                         .insert(prefix.clone(), (url.clone(), id));
                     Some(prefix)
                 } else {
                     self.namespaces.default = Some((url.clone(), id));
                     None
                 };
 
+                self.state = State::Namespaces;
                 return Ok(AtRuleType::WithoutBlock(CssRule::Namespace(Arc::new(
                     self.shared_lock.wrap(NamespaceRule {
                         prefix: opt_prefix,
                         url: url,
                         source_location: location,
                     })
                 ))))
             },
             // @charset is removed by rust-cssparser if it’s the first rule in the stylesheet
             // anything left is invalid.
             "charset" => {
                 self.had_hierarchy_error = true;
                 return Err(StyleParseError::UnexpectedCharsetRule.into())
             }
             _ => {}
         }
-        self.state = State::Body;
 
         AtRuleParser::parse_prelude(&mut self.nested(), name, input)
     }
 
     #[inline]
     fn parse_block<'t>(
         &mut self,
         prelude: AtRulePrelude,
         input: &mut Parser<'i, 't>
     ) -> Result<CssRule, ParseError<'i>> {
         AtRuleParser::parse_block(&mut self.nested(), prelude, input)
+            .map(|rule| { self.state = State::Body; rule })
     }
 }
 
 pub struct QualifiedRuleParserPrelude {
     selectors: SelectorList<SelectorImpl>,
     source_location: SourceLocation,
 }
 
@@ -261,27 +261,27 @@ impl<'a, 'i> QualifiedRuleParser<'i> for
     type QualifiedRule = CssRule;
     type Error = SelectorParseError<'i, StyleParseError<'i>>;
 
     #[inline]
     fn parse_prelude<'t>(
         &mut self,
         input: &mut Parser<'i, 't>,
     ) -> Result<QualifiedRuleParserPrelude, ParseError<'i>> {
-        self.state = State::Body;
         QualifiedRuleParser::parse_prelude(&mut self.nested(), input)
     }
 
     #[inline]
     fn parse_block<'t>(
         &mut self,
         prelude: QualifiedRuleParserPrelude,
         input: &mut Parser<'i, 't>
     ) -> Result<CssRule, ParseError<'i>> {
         QualifiedRuleParser::parse_block(&mut self.nested(), prelude, input)
+            .map(|result| { self.state = State::Body; result })
     }
 }
 
 #[derive(Clone)]  // shallow, relatively cheap .clone
 struct NestedRuleParser<'a, 'b: 'a> {
     stylesheet_origin: Origin,
     shared_lock: &'a SharedRwLock,
     context: &'a ParserContext<'b>,
