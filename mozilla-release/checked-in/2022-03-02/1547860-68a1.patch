# HG changeset patch
# User J.C. Jones <jc@mozilla.com>
# Date 1556659971 0
# Node ID 54d6029f69a58769752692246ae768701580defb
# Parent  1af2d10684bfa8005267cbcdea473fda010f8cd7
Bug 1547860 - Update test_tls_server to use TLS 1.3 client cert alert logic r=keeler

Differential Revision: https://phabricator.services.mozilla.com/D29384

diff --git a/netwerk/test/unit/test_tls_server.js b/netwerk/test/unit/test_tls_server.js
--- a/netwerk/test/unit/test_tls_server.js
+++ b/netwerk/test/unit/test_tls_server.js
@@ -103,19 +103,20 @@ function startServer(cert, expectingPeer
 
 function storeCertOverride(port, cert) {
   let overrideBits = Ci.nsICertOverrideService.ERROR_UNTRUSTED |
                      Ci.nsICertOverrideService.ERROR_MISMATCH;
   certOverrideService.rememberValidityOverride("127.0.0.1", port, cert,
                                                overrideBits, true);
 }
 
-function startClient(port, cert, expectingBadCertAlert) {
+function startClient(port, cert, expectingAlert, tlsVersion) {
   let SSL_ERROR_BASE = Ci.nsINSSErrorsService.NSS_SSL_ERROR_BASE;
   let SSL_ERROR_BAD_CERT_ALERT = SSL_ERROR_BASE + 17;
+  let SSL_ERROR_RX_CERTIFICATE_REQUIRED_ALERT = SSL_ERROR_BASE + 181;
   let transport =
     socketTransportService.createTransport(["ssl"], 1, "127.0.0.1", port, null);
   let input;
   let output;
 
   let inputDeferred = PromiseUtils.defer();
   let outputDeferred = PromiseUtils.defer();
 
@@ -128,28 +129,39 @@ function startClient(port, cert, expecti
     },
 
     onInputStreamReady: function(input) {
       try {
         let data = NetUtil.readInputStreamToString(input, input.available());
         equal(data, "HELLO", "Echoed data received");
         input.close();
         output.close();
-        ok(!expectingBadCertAlert, "No bad cert alert expected");
+        ok(!expectingAlert, "No cert alert expected");
         inputDeferred.resolve();
       } catch (e) {
         let errorCode = -1 * (e.result & 0xFFFF);
-        if (expectingBadCertAlert && errorCode == SSL_ERROR_BAD_CERT_ALERT) {
-          do_print("Got bad cert alert as expected");
-          input.close();
-          output.close();
-          inputDeferred.resolve();
-        } else {
-          inputDeferred.reject(e);
+        if (expectingAlert) {
+          if (tlsVersion == Ci.nsITLSClientStatus.TLS_VERSION_1_2 &&
+              errorCode == SSL_ERROR_BAD_CERT_ALERT) {
+            info("Got bad cert alert as expected for tls 1.2");
+            input.close();
+            output.close();
+            inputDeferred.resolve();
+            return;
+          }
+          if (tlsVersion == Ci.nsITLSClientStatus.TLS_VERSION_1_3 &&
+              errorCode == SSL_ERROR_RX_CERTIFICATE_REQUIRED_ALERT) {
+            info("Got cert required alert as expected for tls 1.3");
+            input.close();
+            output.close();
+            inputDeferred.resolve();
+            return;
+          }
         }
+        inputDeferred.reject(e);
       }
     },
 
     onOutputStreamReady: function(output) {
       try {
         // Set the client certificate as appropriate.
         if (cert) {
           let clientSecInfo = transport.securityInfo;
@@ -181,42 +193,42 @@ function startClient(port, cert, expecti
 
 // Replace the UI dialog that prompts the user to pick a client certificate.
 do_load_manifest("client_cert_chooser.manifest");
 
 const tests = [{
   expectingPeerCert: true,
   clientCertificateConfig: Ci.nsITLSServerSocket.REQUIRE_ALWAYS,
   sendClientCert: true,
-  expectingBadCertAlert: false
+  expectingAlert: false
 }, {
   expectingPeerCert: true,
   clientCertificateConfig: Ci.nsITLSServerSocket.REQUIRE_ALWAYS,
   sendClientCert: false,
-  expectingBadCertAlert: true
+  expectingAlert: true
 }, {
   expectingPeerCert: true,
   clientCertificateConfig: Ci.nsITLSServerSocket.REQUEST_ALWAYS,
   sendClientCert: true,
-  expectingBadCertAlert: false
+  expectingAlert: false
 }, {
   expectingPeerCert: false,
   clientCertificateConfig: Ci.nsITLSServerSocket.REQUEST_ALWAYS,
   sendClientCert: false,
-  expectingBadCertAlert: false
+  expectingAlert: false
 }, {
   expectingPeerCert: false,
   clientCertificateConfig: Ci.nsITLSServerSocket.REQUEST_NEVER,
   sendClientCert: true,
-  expectingBadCertAlert: false
+  expectingAlert: false
 }, {
   expectingPeerCert: false,
   clientCertificateConfig: Ci.nsITLSServerSocket.REQUEST_NEVER,
   sendClientCert: false,
-  expectingBadCertAlert: false
+  expectingAlert: false
 }];
 
 const versions = [{
   prefValue: 3, version: Ci.nsITLSClientStatus.TLS_VERSION_1_2, versionStr: "TLS 1.2"
 }, {
   prefValue: 4, version: Ci.nsITLSClientStatus.TLS_VERSION_1_3, versionStr: "TLS 1.3"
 }];
 
@@ -228,17 +240,17 @@ add_task(async function() {
     for (let t of tests) {
       let server = startServer(cert,
                                t.expectingPeerCert,
                                t.clientCertificateConfig,
                                v.version,
                                v.versionStr);
       storeCertOverride(server.port, cert);
       await startClient(server.port, t.sendClientCert ? cert : null,
-                        t.expectingBadCertAlert);
+                        t.expectingAlert, v.version);
       server.close();
     }
   }
 });
 
 do_register_cleanup(function() {
   prefs.clearUserPref("security.tls.version.max");
 });
