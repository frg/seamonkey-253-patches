# HG changeset patch
# User Bobby Holley <bobbyholley@gmail.com>
# Date 1502387920 25200
#      Thu Aug 10 10:58:40 2017 -0700
# Node ID a299e11ece7324e0bf45c19ca6406cfe8bc0181d
# Parent  89ab9c5fc83d82ae126e967b5dad169a0db22eda
Bug 1389347 - Hoist various bits of PrepareAndTraverse functionality into an RAII class. r=emilio

MozReview-Commit-ID: CPVnZjSwRpN

diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -32,16 +32,64 @@
 #include "nsSMILAnimationController.h"
 #include "nsStyleContext.h"
 #include "nsStyleSet.h"
 #include "gfxUserFontSet.h"
 
 using namespace mozilla;
 using namespace mozilla::dom;
 
+ServoStyleSet* ServoStyleSet::sInServoTraversal = nullptr;
+
+namespace mozilla {
+// On construction, sets sInServoTraversal to the given ServoStyleSet.
+// On destruction, clears sInServoTraversal and calls RunPostTraversalTasks.
+class MOZ_RAII AutoSetInServoTraversal
+{
+public:
+  explicit AutoSetInServoTraversal(ServoStyleSet* aSet)
+    : mSet(aSet)
+  {
+    MOZ_ASSERT(!ServoStyleSet::sInServoTraversal);
+    MOZ_ASSERT(aSet);
+    ServoStyleSet::sInServoTraversal = aSet;
+  }
+
+  ~AutoSetInServoTraversal()
+  {
+    MOZ_ASSERT(ServoStyleSet::sInServoTraversal);
+    ServoStyleSet::sInServoTraversal = nullptr;
+    mSet->RunPostTraversalTasks();
+  }
+
+private:
+  ServoStyleSet* mSet;
+};
+
+// Sets up for one or more calls to Servo_TraverseSubtree.
+class MOZ_RAII AutoPrepareTraversal
+{
+public:
+  explicit AutoPrepareTraversal(ServoStyleSet* aSet)
+    // For markers for animations, we have already set the markers in
+    // ServoRestyleManager::PostRestyleEventForAnimations so that we don't need
+    // to care about animation restyles here.
+    : mTimelineMarker(aSet->mPresContext->GetDocShell(), false)
+    , mSetInServoTraversal(aSet)
+  {
+    MOZ_ASSERT(!aSet->StylistNeedsUpdate());
+  }
+
+private:
+  AutoRestyleTimelineMarker mTimelineMarker;
+  AutoSetInServoTraversal mSetInServoTraversal;
+};
+
+} // namespace mozilla
+
 ServoStyleSet::ServoStyleSet()
   : mPresContext(nullptr)
   , mAuthorStyleDisabled(false)
   , mStylistState(StylistState::NotDirty)
   , mUserFontSetUpdateGeneration(0)
   , mUserFontCacheUpdateGeneration(0)
   , mNeedsRestyleAfterEnsureUniqueInner(false)
 {
@@ -330,24 +378,17 @@ ServoStyleSet::PreTraverse(ServoTraversa
 }
 
 bool
 ServoStyleSet::PrepareAndTraverseSubtree(
   RawGeckoElementBorrowed aRoot,
   ServoTraversalFlags aFlags)
 {
   MOZ_ASSERT(MayTraverseFrom(const_cast<Element*>(aRoot)));
-  // For markers for animations, we have already set the markers in
-  // ServoRestyleManager::PostRestyleEventForAnimations so that we don't need
-  // to care about animation restyles here.
-  AutoRestyleTimelineMarker marker(mPresContext->GetDocShell(),
-                                   false /* animation-only */);
-
-  MOZ_ASSERT(!StylistNeedsUpdate());
-  AutoSetInServoTraversal guard(this);
+  AutoPrepareTraversal guard(this);
 
   const SnapshotTable& snapshots = Snapshots();
 
   bool isInitial = !aRoot->HasServoData();
   bool forReconstruct = !!(aFlags & ServoTraversalFlags::AggressivelyForgetful);
   bool postTraversalRequired =
     Servo_TraverseSubtree(aRoot, mRawSet.get(), &snapshots, aFlags);
   MOZ_ASSERT(!(isInitial || forReconstruct) || !postTraversalRequired);
@@ -1389,10 +1430,8 @@ ServoStyleSet::ReparentStyleContext(Serv
                                     ServoStyleContext* aNewParentIgnoringFirstLine,
                                     ServoStyleContext* aNewLayoutParent,
                                     Element* aElement)
 {
   return Servo_ReparentStyle(aStyleContext, aNewParent,
                              aNewParentIgnoringFirstLine, aNewLayoutParent,
                              aElement, mRawSet.get()).Consume();
 }
-
-ServoStyleSet* ServoStyleSet::sInServoTraversal = nullptr;
diff --git a/layout/style/ServoStyleSet.h b/layout/style/ServoStyleSet.h
--- a/layout/style/ServoStyleSet.h
+++ b/layout/style/ServoStyleSet.h
@@ -454,39 +454,18 @@ public:
   already_AddRefed<ServoStyleContext>
   ReparentStyleContext(ServoStyleContext* aStyleContext,
                        ServoStyleContext* aNewParent,
                        ServoStyleContext* aNewParentIgnoringFirstLine,
                        ServoStyleContext* aNewLayoutParent,
                        Element* aElement);
 
 private:
-  // On construction, sets sInServoTraversal to the given ServoStyleSet.
-  // On destruction, clears sInServoTraversal and calls RunPostTraversalTasks.
-  class MOZ_STACK_CLASS AutoSetInServoTraversal
-  {
-  public:
-    explicit AutoSetInServoTraversal(ServoStyleSet* aSet)
-      : mSet(aSet)
-    {
-      MOZ_ASSERT(!sInServoTraversal);
-      MOZ_ASSERT(aSet);
-      sInServoTraversal = aSet;
-    }
-
-    ~AutoSetInServoTraversal()
-    {
-      MOZ_ASSERT(sInServoTraversal);
-      sInServoTraversal = nullptr;
-      mSet->RunPostTraversalTasks();
-    }
-
-  private:
-    ServoStyleSet* mSet;
-  };
+  friend class AutoSetInServoTraversal;
+  friend class AutoPrepareTraversal;
 
   /**
    * Gets the pending snapshots to handle from the restyle manager.
    */
   const SnapshotTable& Snapshots();
 
   /**
    * Resolve all ServoDeclarationBlocks attached to mapped
