# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1495575331 18000
# Node ID 1e13a2a2660398e5eae0595fdaa696c9f5987e9c
# Parent  687a55549ca080f579c0136c96ebff2b52fb6470
Bug 1363200 - JSAPI for realms: JS::SetDestroyRealmCallback. r=sfink

diff --git a/js/public/Realm.h b/js/public/Realm.h
--- a/js/public/Realm.h
+++ b/js/public/Realm.h
@@ -64,16 +64,36 @@ GetRealmForCompartment(JSCompartment* co
 // It's a pointer to embeddding-specific data that SpiderMonkey never uses.
 extern JS_PUBLIC_API(void*)
 GetRealmPrivate(Realm* realm);
 
 // Set the "private data" internal field of the given Realm.
 extern JS_PUBLIC_API(void)
 SetRealmPrivate(Realm* realm, void* data);
 
+typedef void
+(* DestroyRealmCallback)(JSFreeOp* fop, Realm* realm);
+
+// Set the callback SpiderMonkey calls just before garbage-collecting a realm.
+// Embeddings can use this callback to free private data associated with the
+// realm via SetRealmPrivate.
+//
+// By the time this is called, the global object for the realm has already been
+// collected.
+extern JS_PUBLIC_API(void)
+SetDestroyRealmCallback(JSContext* cx, DestroyRealmCallback callback);
+
+typedef void
+(* RealmNameCallback)(JSContext* cx, Handle<Realm*> realm, char* buf, size_t bufsize);
+
+// Set the callback SpiderMonkey calls to get the name of a realm, for
+// diagnostic output.
+extern JS_PUBLIC_API(void)
+SetRealmNameCallback(JSContext* cx, RealmNameCallback callback);
+
 extern JS_PUBLIC_API(JSObject*)
 GetRealmObjectPrototype(JSContext* cx);
 
 extern JS_PUBLIC_API(JSObject*)
 GetRealmFunctionPrototype(JSContext* cx);
 
 extern JS_PUBLIC_API(JSObject*)
 GetRealmArrayPrototype(JSContext* cx);
diff --git a/js/public/TypeDecls.h b/js/public/TypeDecls.h
--- a/js/public/TypeDecls.h
+++ b/js/public/TypeDecls.h
@@ -23,16 +23,17 @@
 #include "js-config.h"
 
 struct JSContext;
 class JSFunction;
 class JSObject;
 class JSScript;
 class JSString;
 class JSAddonId;
+struct JSFreeOp;
 
 struct jsid;
 
 namespace JS {
 
 typedef unsigned char Latin1Char;
 
 class Symbol;
diff --git a/js/src/jsapi.cpp b/js/src/jsapi.cpp
--- a/js/src/jsapi.cpp
+++ b/js/src/jsapi.cpp
@@ -679,22 +679,16 @@ JS_SetSizeOfIncludingThisCompartmentCall
 
 JS_PUBLIC_API(void)
 JS_SetCompartmentNameCallback(JSContext* cx, JSCompartmentNameCallback callback)
 {
     cx->runtime()->compartmentNameCallback = callback;
 }
 
 JS_PUBLIC_API(void)
-JS_SetRealmNameCallback(JSContext* cx, JSRealmNameCallback callback)
-{
-    cx->runtime()->realmNameCallback = callback;
-}
-
-JS_PUBLIC_API(void)
 JS_SetWrapObjectCallbacks(JSContext* cx, const JSWrapObjectCallbacks* callbacks)
 {
     cx->runtime()->wrapObjectCallbacks = callbacks;
 }
 
 JS_PUBLIC_API(void)
 JS_SetExternalStringSizeofCallback(JSContext* cx, JSExternalStringSizeofCallback callback)
 {
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -711,19 +711,16 @@ typedef void
 typedef size_t
 (* JSSizeOfIncludingThisCompartmentCallback)(mozilla::MallocSizeOf mallocSizeOf,
                                              JSCompartment* compartment);
 
 typedef void
 (* JSCompartmentNameCallback)(JSContext* cx, JSCompartment* compartment,
                               char* buf, size_t bufsize);
 
-typedef void
-(* JSRealmNameCallback)(JSContext* cx, JS::Handle<JS::Realm*> realm, char* buf, size_t bufsize);
-
 /**
  * Callback used by memory reporting to ask the embedder how much memory an
  * external string is keeping alive.  The embedder is expected to return a value
  * that corresponds to the size of the allocation that will be released by the
  * JSStringFinalizer passed to JS_NewExternalString for this string.
  *
  * Implementations of this callback MUST NOT do anything that can cause GC.
  */
@@ -1367,19 +1364,16 @@ JS_SetDestroyCompartmentCallback(JSConte
 extern JS_PUBLIC_API(void)
 JS_SetSizeOfIncludingThisCompartmentCallback(JSContext* cx,
                                              JSSizeOfIncludingThisCompartmentCallback callback);
 
 extern JS_PUBLIC_API(void)
 JS_SetCompartmentNameCallback(JSContext* cx, JSCompartmentNameCallback callback);
 
 extern JS_PUBLIC_API(void)
-JS_SetRealmNameCallback(JSContext* cx, JSRealmNameCallback callback);
-
-extern JS_PUBLIC_API(void)
 JS_SetWrapObjectCallbacks(JSContext* cx, const JSWrapObjectCallbacks* callbacks);
 
 extern JS_PUBLIC_API(void)
 JS_SetExternalStringSizeofCallback(JSContext* cx, JSExternalStringSizeofCallback callback);
 
 extern JS_PUBLIC_API(void)
 JS_SetCompartmentPrivate(JSCompartment* compartment, void* data);
 
diff --git a/js/src/jsgc.cpp b/js/src/jsgc.cpp
--- a/js/src/jsgc.cpp
+++ b/js/src/jsgc.cpp
@@ -3439,16 +3439,18 @@ JS::Zone::sweepUniqueIds(js::FreeOp* fop
 {
     uniqueIds().sweep();
 }
 
 void
 JSCompartment::destroy(FreeOp* fop)
 {
     JSRuntime* rt = fop->runtime();
+    if (auto callback = rt->destroyRealmCallback)
+        callback(fop, JS::GetRealmForCompartment(this));
     if (auto callback = rt->destroyCompartmentCallback)
         callback(fop, this);
     if (principals())
         JS_DropPrincipals(TlsContext.get(), principals());
     fop->delete_(this);
     rt->gc.stats().sweptCompartment();
 }
 
diff --git a/js/src/vm/Realm.cpp b/js/src/vm/Realm.cpp
--- a/js/src/vm/Realm.cpp
+++ b/js/src/vm/Realm.cpp
@@ -41,16 +41,28 @@ JS::GetRealmPrivate(JS::Realm* realm)
 }
 
 JS_PUBLIC_API(void)
 JS::SetRealmPrivate(JS::Realm* realm, void* data)
 {
     GetCompartmentForRealm(realm)->realmData = data;
 }
 
+JS_PUBLIC_API(void)
+JS::SetDestroyRealmCallback(JSContext* cx, JS::DestroyRealmCallback callback)
+{
+    cx->runtime()->destroyRealmCallback = callback;
+}
+
+JS_PUBLIC_API(void)
+JS::SetRealmNameCallback(JSContext* cx, JS::RealmNameCallback callback)
+{
+    cx->runtime()->realmNameCallback = callback;
+}
+
 JS_PUBLIC_API(JSObject*)
 JS::GetRealmObjectPrototype(JSContext* cx)
 {
     CHECK_REQUEST(cx);
     return GlobalObject::getOrCreateObjectPrototype(cx, cx->global());
 }
 
 JS_PUBLIC_API(JSObject*)
diff --git a/js/src/vm/Runtime.cpp b/js/src/vm/Runtime.cpp
--- a/js/src/vm/Runtime.cpp
+++ b/js/src/vm/Runtime.cpp
@@ -114,16 +114,17 @@ JSRuntime::JSRuntime(JSRuntime* parentRu
     readableStreamClosedCallback(nullptr),
     readableStreamErroredCallback(nullptr),
     readableStreamFinalizeCallback(nullptr),
     hadOutOfMemory(false),
     allowRelazificationForTesting(false),
     destroyCompartmentCallback(nullptr),
     sizeOfIncludingThisCompartmentCallback(nullptr),
     compartmentNameCallback(nullptr),
+    destroyRealmCallback(nullptr),
     realmNameCallback(nullptr),
     externalStringSizeofCallback(nullptr),
     securityCallbacks(&NullSecurityCallbacks),
     DOMcallbacks(nullptr),
     destroyPrincipals(nullptr),
     readPrincipals(nullptr),
     warningReporter(nullptr),
     geckoProfiler_(thisFromCtor()),
diff --git a/js/src/vm/Runtime.h b/js/src/vm/Runtime.h
--- a/js/src/vm/Runtime.h
+++ b/js/src/vm/Runtime.h
@@ -496,18 +496,21 @@ struct JSRuntime : public js::MallocProv
     js::ActiveThreadData<JSDestroyCompartmentCallback> destroyCompartmentCallback;
 
     /* Compartment memory reporting callback. */
     js::ActiveThreadData<JSSizeOfIncludingThisCompartmentCallback> sizeOfIncludingThisCompartmentCallback;
 
     /* Call this to get the name of a compartment. */
     js::ActiveThreadData<JSCompartmentNameCallback> compartmentNameCallback;
 
+    /* Realm destroy callback. */
+    js::ActiveThreadData<JS::DestroyRealmCallback> destroyRealmCallback;
+
     /* Call this to get the name of a realm. */
-    js::ActiveThreadData<JSRealmNameCallback> realmNameCallback;
+    js::ActiveThreadData<JS::RealmNameCallback> realmNameCallback;
 
     /* Callback for doing memory reporting on external strings. */
     js::ActiveThreadData<JSExternalStringSizeofCallback> externalStringSizeofCallback;
 
     js::ActiveThreadData<mozilla::UniquePtr<js::SourceHook>> sourceHook;
 
     js::ActiveThreadData<const JSSecurityCallbacks*> securityCallbacks;
     js::ActiveThreadData<const js::DOMCallbacks*> DOMcallbacks;
diff --git a/js/xpconnect/src/XPCJSRuntime.cpp b/js/xpconnect/src/XPCJSRuntime.cpp
--- a/js/xpconnect/src/XPCJSRuntime.cpp
+++ b/js/xpconnect/src/XPCJSRuntime.cpp
@@ -2659,17 +2659,17 @@ CompartmentNameCallback(JSContext* cx, J
     int anonymizeID = 0;
     GetCompartmentName(comp, name, &anonymizeID, /* replaceSlashes = */ false);
     if (name.Length() >= bufsize)
         name.Truncate(bufsize - 1);
     memcpy(buf, name.get(), name.Length() + 1);
 }
 
 static void
-RealmNameCallback(JSContext* cx, JS::Handle<JS::Realm*> realm, char* buf, size_t bufsize)
+GetRealmName(JSContext* cx, JS::Handle<JS::Realm*> realm, char* buf, size_t bufsize)
 {
     JSCompartment* comp = JS::GetCompartmentForRealm(realm);
     CompartmentNameCallback(cx, comp, buf, bufsize);
 }
 
 static bool
 PreserveWrapper(JSContext* cx, JSObject* obj)
 {
@@ -2836,17 +2836,17 @@ XPCJSRuntime::Initialize(JSContext* cx)
     // This leaves the maximum-JS_malloc-bytes threshold still in effect
     // to cause period, and we hope hygienic, last-ditch GCs from within
     // the GC's allocator.
     JS_SetGCParameter(cx, JSGC_MAX_BYTES, 0xffffffff);
 
     JS_SetDestroyCompartmentCallback(cx, CompartmentDestroyedCallback);
     JS_SetSizeOfIncludingThisCompartmentCallback(cx, CompartmentSizeOfIncludingThisCallback);
     JS_SetCompartmentNameCallback(cx, CompartmentNameCallback);
-    JS_SetRealmNameCallback(cx, RealmNameCallback);
+    JS::SetRealmNameCallback(cx, GetRealmName);
     mPrevGCSliceCallback = JS::SetGCSliceCallback(cx, GCSliceCallback);
     mPrevDoCycleCollectionCallback = JS::SetDoCycleCollectionCallback(cx,
             DoCycleCollectionCallback);
     JS_AddFinalizeCallback(cx, FinalizeCallback, nullptr);
     JS_AddWeakPointerZonesCallback(cx, WeakPointerZonesCallback, this);
     JS_AddWeakPointerCompartmentCallback(cx, WeakPointerCompartmentCallback, this);
     JS_SetWrapObjectCallbacks(cx, &WrapObjectCallbacks);
     js::SetPreserveWrapperCallback(cx, PreserveWrapper);

