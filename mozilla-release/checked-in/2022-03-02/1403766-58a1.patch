# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1506554932 -32400
# Node ID 007e7f04bab6dedd202edcb46788a4f68958266e
# Parent  011f138f28f4da74715fddb3901c21eb88220a30
Bug 1403766 - Always use some sort of thread local storage in the allocator. r=njn

As we're going to enable stylo on Android at some point, we'll have to
have thread local arenas there, which means Android needs to be using
thread local storage. Since Android is the last use of NO_TLS in the
allocator code base, remove it.

diff --git a/memory/build/mozjemalloc.cpp b/memory/build/mozjemalloc.cpp
--- a/memory/build/mozjemalloc.cpp
+++ b/memory/build/mozjemalloc.cpp
@@ -108,20 +108,16 @@
  */
 
 #include "mozmemory_wrap.h"
 #include "mozjemalloc.h"
 #include "mozilla/Sprintf.h"
 #include "mozilla/Likely.h"
 #include "mozilla/DoublyLinkedList.h"
 
-#ifdef ANDROID
-#define NO_TLS
-#endif
-
 /*
  * On Linux, we use madvise(MADV_DONTNEED) to release memory back to the
  * operating system.  If we release 1MB of live pages with MADV_DONTNEED, our
  * RSS will decrease by 1MB (almost) immediately.
  *
  * On Mac, we use madvise(MADV_FREE).  Unlike MADV_DONTNEED on Linux, MADV_FREE
  * on Mac doesn't cause the OS to release the specified pages immediately; the
  * OS keeps them in our process until the machine comes under memory pressure.
@@ -1107,29 +1103,27 @@ static size_t		base_committed;
 static arena_t** arenas;
 // A tree of arenas, arranged by id.
 // TODO: Move into arena_t as a static member when rb_tree doesn't depend on
 // the type being defined anymore.
 static RedBlackTree<arena_t, ArenaTreeTrait> gArenaTree;
 static unsigned narenas;
 static malloc_spinlock_t arenas_lock; /* Protects arenas initialization. */
 
-#ifndef NO_TLS
 /*
  * The arena associated with the current thread (per jemalloc_thread_local_arena)
  * On OSX, __thread/thread_local circles back calling malloc to allocate storage
  * on first access on each thread, which leads to an infinite loop, but
  * pthread-based TLS somehow doesn't have this problem.
  */
 #if !defined(XP_DARWIN)
 static MOZ_THREAD_LOCAL(arena_t*) thread_arena;
 #else
 static mozilla::detail::ThreadLocal<arena_t*, mozilla::detail::ThreadLocalKeyStorage> thread_arena;
 #endif
-#endif
 
 /*******************************/
 /*
  * Runtime configuration options.
  */
 const uint8_t kAllocJunk = 0xe4;
 const uint8_t kAllocPoison = 0xe5;
 
@@ -2334,35 +2328,31 @@ chunk_dealloc(void *chunk, size_t size, 
 /******************************************************************************/
 /*
  * Begin arena.
  */
 
 static inline arena_t *
 thread_local_arena(bool enabled)
 {
-#ifndef NO_TLS
   arena_t *arena;
 
   if (enabled) {
     /* The arena will essentially be leaked if this function is
      * called with `false`, but it doesn't matter at the moment.
      * because in practice nothing actually calls this function
      * with `false`, except maybe at shutdown. */
     arena = arenas_extend();
   } else {
     malloc_spin_lock(&arenas_lock);
     arena = arenas[0];
     malloc_spin_unlock(&arenas_lock);
   }
   thread_arena.set(arena);
   return arena;
-#else
-  return arenas[0];
-#endif
 }
 
 template<> inline void
 MozJemalloc::jemalloc_thread_local_arena(bool aEnabled)
 {
   thread_local_arena(aEnabled);
 }
 
@@ -2374,28 +2364,24 @@ choose_arena(size_t size)
 {
   arena_t *ret = nullptr;
 
   /*
    * We can only use TLS if this is a PIC library, since for the static
    * library version, libc's malloc is used by TLS allocation, which
    * introduces a bootstrapping issue.
    */
-#ifndef NO_TLS
   // Only use a thread local arena for small sizes.
   if (size <= small_max) {
     ret = thread_arena.get();
   }
 
   if (!ret) {
     ret = thread_local_arena(false);
   }
-#else
-  ret = arenas[0];
-#endif
   MOZ_DIAGNOSTIC_ASSERT(ret);
   return (ret);
 }
 
 static inline void *
 arena_run_reg_alloc(arena_run_t *run, arena_bin_t *bin)
 {
 	void *ret;
@@ -4413,21 +4399,19 @@ malloc_init_hard(void)
      * acquired init_lock.
      */
 #ifndef XP_WIN
     malloc_mutex_unlock(&init_lock);
 #endif
     return false;
   }
 
-#ifndef NO_TLS
   if (!thread_arena.init()) {
     return false;
   }
-#endif
 
   /* Get page size and number of CPUs */
   result = GetKernelPageSize();
   /* We assume that the page size is a power of 2. */
   MOZ_ASSERT(((result - 1) & result) == 0);
 #ifdef MALLOC_STATIC_SIZES
   if (pagesize % (size_t) result) {
     _malloc_message(_getprogname(),
@@ -4617,22 +4601,20 @@ MALLOC_OUT:
     malloc_mutex_unlock(&init_lock);
 #endif
     return true;
   }
   /* arena_t::Init() sets this to a lower value for thread local arenas;
    * reset to the default value for the main arenas */
   arenas[0]->mMaxDirty = opt_dirty_max;
 
-#ifndef NO_TLS
   /*
    * Assign the initial arena to the initial thread.
    */
   thread_arena.set(arenas[0]);
-#endif
 
   chunk_rtree = malloc_rtree_new((SIZEOF_PTR << 3) - opt_chunk_2pow);
   if (!chunk_rtree) {
     return true;
   }
 
   malloc_initialized = true;
 
