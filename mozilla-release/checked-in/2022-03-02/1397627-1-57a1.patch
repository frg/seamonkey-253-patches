# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1504879585 -7200
# Node ID b4f8fad33b2f8013edb3aa265ef63629c8f18816
# Parent  dce4d3db04bf1eb303a4e05f4d3cf9f1baf0f110
Bug 1397627 - IPCBlobInputStream doesn't need to be seekable, r=smaug

diff --git a/dom/file/ipc/IPCBlobInputStream.cpp b/dom/file/ipc/IPCBlobInputStream.cpp
--- a/dom/file/ipc/IPCBlobInputStream.cpp
+++ b/dom/file/ipc/IPCBlobInputStream.cpp
@@ -64,17 +64,16 @@ NS_IMPL_ADDREF(IPCBlobInputStream);
 NS_IMPL_RELEASE(IPCBlobInputStream);
 
 NS_INTERFACE_MAP_BEGIN(IPCBlobInputStream)
   NS_INTERFACE_MAP_ENTRY(nsIInputStream)
   NS_INTERFACE_MAP_ENTRY(nsIAsyncInputStream)
   NS_INTERFACE_MAP_ENTRY(nsIInputStreamCallback)
   NS_INTERFACE_MAP_ENTRY(nsICloneableInputStream)
   NS_INTERFACE_MAP_ENTRY(nsIIPCSerializableInputStream)
-  NS_INTERFACE_MAP_ENTRY_CONDITIONAL(nsISeekableStream, IsSeekableStream())
   NS_INTERFACE_MAP_ENTRY_CONDITIONAL(nsIFileMetadata, IsFileMetadata())
   NS_INTERFACE_MAP_ENTRY_AMBIGUOUS(nsISupports, nsIInputStream)
 NS_INTERFACE_MAP_END
 
 IPCBlobInputStream::IPCBlobInputStream(IPCBlobInputStreamChild* aActor)
   : mActor(aActor)
   , mState(eInit)
 {
@@ -410,50 +409,10 @@ IPCBlobInputStream::GetFileDescriptor(PR
   nsCOMPtr<nsIFileMetadata> fileMetadata = do_QueryInterface(mRemoteStream);
   if (!fileMetadata) {
     return mState == eClosed ? NS_BASE_STREAM_CLOSED : NS_ERROR_FAILURE;
   }
 
   return fileMetadata->GetFileDescriptor(aRetval);
 }
 
-// nsISeekableStream
-
-bool
-IPCBlobInputStream::IsSeekableStream() const
-{
-  // We are nsISeekableStream only if we have the remote stream and that is a
-  // nsISeekableStream.
-  nsCOMPtr<nsISeekableStream> seekableStream = do_QueryInterface(mRemoteStream);
-  return !!seekableStream;
-}
-
-NS_IMETHODIMP
-IPCBlobInputStream::Seek(int32_t aWhence, int64_t aOffset)
-{
-  nsCOMPtr<nsISeekableStream> seekableStream = do_QueryInterface(mRemoteStream);
-  if (!seekableStream) {
-    return mState == eClosed ? NS_BASE_STREAM_CLOSED : NS_ERROR_FAILURE;
-  }
-
-  return seekableStream->Seek(aWhence, aOffset);
-}
-
-NS_IMETHODIMP
-IPCBlobInputStream::Tell(int64_t *aResult)
-{
-  nsCOMPtr<nsISeekableStream> seekableStream = do_QueryInterface(mRemoteStream);
-  if (!seekableStream) {
-    return mState == eClosed ? NS_BASE_STREAM_CLOSED : NS_ERROR_FAILURE;
-  }
-
-  return seekableStream->Tell(aResult);
-}
-
-NS_IMETHODIMP
-IPCBlobInputStream::SetEOF()
-{
-  // This is a read-only stream.
-  return NS_ERROR_FAILURE;
-}
-
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/file/ipc/IPCBlobInputStream.h b/dom/file/ipc/IPCBlobInputStream.h
--- a/dom/file/ipc/IPCBlobInputStream.h
+++ b/dom/file/ipc/IPCBlobInputStream.h
@@ -6,57 +6,51 @@
 
 #ifndef mozilla_dom_ipc_IPCBlobInputStream_h
 #define mozilla_dom_ipc_IPCBlobInputStream_h
 
 #include "nsIAsyncInputStream.h"
 #include "nsICloneableInputStream.h"
 #include "nsIFileStreams.h"
 #include "nsIIPCSerializableInputStream.h"
-#include "nsISeekableStream.h"
 #include "nsCOMPtr.h"
 
 namespace mozilla {
 namespace dom {
 
 class IPCBlobInputStreamChild;
 
 class IPCBlobInputStream final : public nsIAsyncInputStream
                                , public nsIInputStreamCallback
                                , public nsICloneableInputStream
                                , public nsIIPCSerializableInputStream
-                               , public nsISeekableStream
                                , public nsIFileMetadata
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIINPUTSTREAM
   NS_DECL_NSIASYNCINPUTSTREAM
   NS_DECL_NSIINPUTSTREAMCALLBACK
   NS_DECL_NSICLONEABLEINPUTSTREAM
   NS_DECL_NSIIPCSERIALIZABLEINPUTSTREAM
-  NS_DECL_NSISEEKABLESTREAM
   NS_DECL_NSIFILEMETADATA
 
   explicit IPCBlobInputStream(IPCBlobInputStreamChild* aActor);
 
   void
   StreamReady(nsIInputStream* aInputStream);
 
 private:
   ~IPCBlobInputStream();
 
   nsresult
   MaybeExecuteCallback(nsIInputStreamCallback* aCallback,
                        nsIEventTarget* aEventTarget);
 
   bool
-  IsSeekableStream() const;
-
-  bool
   IsFileMetadata() const;
 
   RefPtr<IPCBlobInputStreamChild> mActor;
 
   // This is the list of possible states.
   enum {
     // The initial state. Only ::Available() can be used without receiving an
     // error. The available size is known by the actor.

