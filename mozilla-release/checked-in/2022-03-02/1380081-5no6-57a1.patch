# HG changeset patch
# User Michael Layzell <michael@thelayzells.com>
# Date 1500575239 14400
#      Thu Jul 20 14:27:19 2017 -0400
# Node ID 3afb52d2db564f31a9876f5ba1848b9a269571d0
# Parent  aabef37ae8242b4af50a57ab27a9463eb67c9bb1
Bug 1380081 - Part 5: Add some helper methods to HangAnnotations, r=froydnj

These will be used to implement IPC serialization and deserialization of the
HangDetails object to send over IPC. This is a temporary measure as
HangAnnotations is rewritten in part 11.

MozReview-Commit-ID: 1WHNvhDrMF5

diff --git a/xpcom/threads/HangAnnotations.cpp b/xpcom/threads/HangAnnotations.cpp
--- a/xpcom/threads/HangAnnotations.cpp
+++ b/xpcom/threads/HangAnnotations.cpp
@@ -28,16 +28,17 @@ public:
 
   void AddAnnotation(const nsAString& aName, const int32_t aData) override;
   void AddAnnotation(const nsAString& aName, const double aData) override;
   void AddAnnotation(const nsAString& aName, const nsAString& aData) override;
   void AddAnnotation(const nsAString& aName, const nsACString& aData) override;
   void AddAnnotation(const nsAString& aName, const bool aData) override;
 
   size_t SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) const override;
+  size_t Count() const override;
   bool IsEmpty() const override;
   UniquePtr<Enumerator> GetEnumerator() override;
 
   typedef std::pair<nsString, nsString> AnnotationType;
   typedef std::vector<AnnotationType> VectorType;
   typedef VectorType::const_iterator IteratorType;
 
 private:
@@ -137,16 +138,22 @@ ChromeHangAnnotationEnumerator::Next(nsA
     return false;
   }
   aOutName = mIterator->first;
   aOutValue = mIterator->second;
   ++mIterator;
   return true;
 }
 
+size_t
+BrowserHangAnnotations::Count() const
+{
+  return mAnnotations.size();
+}
+
 bool
 BrowserHangAnnotations::IsEmpty() const
 {
   return mAnnotations.empty();
 }
 
 size_t
 BrowserHangAnnotations::SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) const
@@ -254,10 +261,16 @@ UniquePtr<HangAnnotations>
 ChromeHangAnnotatorCallout()
 {
   if (!gChromehangAnnotators) {
     return nullptr;
   }
   return gChromehangAnnotators->GatherAnnotations();
 }
 
+UniquePtr<HangAnnotations>
+CreateEmptyHangAnnotations()
+{
+  return MakeUnique<BrowserHangAnnotations>();
+}
+
 } // namespace HangMonitor
 } // namespace mozilla
diff --git a/xpcom/threads/HangAnnotations.h b/xpcom/threads/HangAnnotations.h
--- a/xpcom/threads/HangAnnotations.h
+++ b/xpcom/threads/HangAnnotations.h
@@ -36,16 +36,17 @@ public:
   class Enumerator
   {
   public:
     virtual ~Enumerator() {}
     virtual bool Next(nsAString& aOutName, nsAString& aOutValue) = 0;
   };
 
   virtual size_t SizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf) const = 0;
+  virtual size_t Count() const = 0;
   virtual bool IsEmpty() const = 0;
   virtual UniquePtr<Enumerator> GetEnumerator() = 0;
 };
 
 typedef UniquePtr<HangAnnotations> HangAnnotationsPtr;
 typedef Vector<HangAnnotationsPtr> HangAnnotationsVector;
 
 class Annotator
@@ -73,16 +74,21 @@ void RegisterAnnotator(Annotator& aAnnot
 void UnregisterAnnotator(Annotator& aAnnotator);
 
 /**
  * Gathers annotations. This function should be called by ChromeHangs.
  * @return UniquePtr to HangAnnotations object or nullptr if none.
  */
 HangAnnotationsPtr ChromeHangAnnotatorCallout();
 
+/**
+ * Creates an empty UniquePtr<HangAnnotations>.
+ */
+HangAnnotationsPtr CreateEmptyHangAnnotations();
+
 namespace Observer {
 
 class Annotators
 {
 public:
   Annotators();
   ~Annotators();
 
