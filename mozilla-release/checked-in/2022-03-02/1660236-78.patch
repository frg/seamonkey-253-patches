# HG changeset patch
# User Matt Woodrow <mwoodrow@mozilla.com>
# Date 1603991992 0
# Node ID cd926b0711d5e791a92b65a2b09eaa1f330ab082
# Parent  bce7ca05778b10ce4086650c82737c50dd1d9c26
Bug 1660236 - Hold mutex while accessing shared SkImage data. r=lsalzman, a=pascal

Differential Revision: https://phabricator.services.mozilla.com/D93442

diff --git a/gfx/2d/DrawTargetSkia.cpp b/gfx/2d/DrawTargetSkia.cpp
--- a/gfx/2d/DrawTargetSkia.cpp
+++ b/gfx/2d/DrawTargetSkia.cpp
@@ -227,36 +227,37 @@ VerifyRGBXCorners(uint8_t* aData, const 
                            << int(aData[offset+3]);
     }
   }
 
   return true;
 }
 #endif
 
-static sk_sp<SkImage>
-GetSkImageForSurface(SourceSurface* aSurface, const Rect* aBounds = nullptr, const Matrix* aMatrix = nullptr)
-{
+static sk_sp<SkImage> GetSkImageForSurface(SourceSurface* aSurface,
+                                           Maybe<MutexAutoLock>* aLock,
+                                           const Rect* aBounds = nullptr,
+                                           const Matrix* aMatrix = nullptr) {
   if (!aSurface) {
     gfxDebug() << "Creating null Skia image from null SourceSurface";
     return nullptr;
   }
 
   if (aSurface->GetType() == SurfaceType::CAPTURE) {
     SourceSurfaceCapture* capture = static_cast<SourceSurfaceCapture*>(aSurface);
     RefPtr<SourceSurface> resolved = capture->Resolve(BackendType::SKIA);
     if (!resolved) {
       return nullptr;
     }
     MOZ_ASSERT(resolved->GetType() != SurfaceType::CAPTURE);
-    return GetSkImageForSurface(resolved, aBounds, aMatrix);
+    return GetSkImageForSurface(resolved, aLock, aBounds, aMatrix);
   }
 
   if (aSurface->GetType() == SurfaceType::SKIA) {
-    return static_cast<SourceSurfaceSkia*>(aSurface)->GetImage();
+    return static_cast<SourceSurfaceSkia*>(aSurface)->GetImage(aLock);
   }
 
   DataSourceSurface* surf = aSurface->GetDataSurface().take();
   if (!surf) {
     gfxWarning() << "Failed getting DataSourceSurface for Skia image";
     return nullptr;
   }
 
@@ -419,20 +420,19 @@ ExtractAlphaBitmap(const sk_sp<SkImage>&
       }
     }
   }
 
   gfxWarning() << "Failed reading alpha pixels for Skia bitmap";
   return false;
 }
 
-static sk_sp<SkImage>
-ExtractAlphaForSurface(SourceSurface* aSurface)
-{
-  sk_sp<SkImage> image = GetSkImageForSurface(aSurface);
+static sk_sp<SkImage> ExtractAlphaForSurface(SourceSurface* aSurface,
+                                             Maybe<MutexAutoLock>& aLock) {
+  sk_sp<SkImage> image = GetSkImageForSurface(aSurface, &aLock);
   if (!image) {
     return nullptr;
   }
   if (image->isAlphaOnly()) {
     return image;
   }
 
   SkBitmap bitmap;
@@ -440,19 +440,20 @@ ExtractAlphaForSurface(SourceSurface* aS
     return nullptr;
   }
 
   // Mark the bitmap immutable so that it will be shared rather than copied.
   bitmap.setImmutable();
   return SkImage::MakeFromBitmap(bitmap);
 }
 
-static void
-SetPaintPattern(SkPaint& aPaint, const Pattern& aPattern, Float aAlpha = 1.0, const SkMatrix* aMatrix = nullptr, const Rect* aBounds = nullptr)
-{
+static void SetPaintPattern(SkPaint& aPaint, const Pattern& aPattern,
+                            Maybe<MutexAutoLock>& aLock, Float aAlpha = 1.0,
+                            const SkMatrix* aMatrix = nullptr,
+                            const Rect* aBounds = nullptr) {
   switch (aPattern.GetType()) {
     case PatternType::COLOR: {
       Color color = static_cast<const ColorPattern&>(aPattern).mColor;
       aPaint.setColor(ColorToSkColor(color, aAlpha));
       break;
     }
     case PatternType::LINEAR_GRADIENT: {
       const LinearGradientPattern& pat = static_cast<const LinearGradientPattern&>(aPattern);
@@ -515,17 +516,18 @@ SetPaintPattern(SkPaint& aPaint, const P
         } else {
           aPaint.setColor(SK_ColorTRANSPARENT);
         }
       }
       break;
     }
     case PatternType::SURFACE: {
       const SurfacePattern& pat = static_cast<const SurfacePattern&>(aPattern);
-      sk_sp<SkImage> image = GetSkImageForSurface(pat.mSurface, aBounds, &pat.mMatrix);
+      sk_sp<SkImage> image =
+          GetSkImageForSurface(pat.mSurface, &aLock, aBounds, &pat.mMatrix);
       if (!image) {
         aPaint.setColor(SK_ColorTRANSPARENT);
         break;
       }
 
       SkMatrix mat;
       GfxMatrixToSkiaMatrix(pat.mMatrix, mat);
       if (aMatrix) {
@@ -569,17 +571,17 @@ GetClipBounds(SkCanvas *aCanvas)
   return SkRectToRect(localBounds);
 }
 
 struct AutoPaintSetup {
   AutoPaintSetup(SkCanvas *aCanvas, const DrawOptions& aOptions, const Pattern& aPattern, const Rect* aMaskBounds = nullptr, const SkMatrix* aMatrix = nullptr, const Rect* aSourceBounds = nullptr)
     : mNeedsRestore(false), mAlpha(1.0)
   {
     Init(aCanvas, aOptions, aMaskBounds, false);
-    SetPaintPattern(mPaint, aPattern, mAlpha, aMatrix, aSourceBounds);
+    SetPaintPattern(mPaint, aPattern, mLock, mAlpha, aMatrix, aSourceBounds);
   }
 
   AutoPaintSetup(SkCanvas *aCanvas, const DrawOptions& aOptions, const Rect* aMaskBounds = nullptr, bool aForceGroup = false)
     : mNeedsRestore(false), mAlpha(1.0)
   {
     Init(aCanvas, aOptions, aMaskBounds, aForceGroup);
   }
 
@@ -623,16 +625,17 @@ struct AutoPaintSetup {
     }
     mPaint.setFilterQuality(kLow_SkFilterQuality);
   }
 
   // TODO: Maybe add an operator overload to access this easier?
   SkPaint mPaint;
   bool mNeedsRestore;
   SkCanvas* mCanvas;
+  Maybe<MutexAutoLock> mLock;
   Float mAlpha;
 };
 
 void
 DrawTargetSkia::Flush()
 {
   mCanvas->flush();
 }
@@ -645,17 +648,18 @@ DrawTargetSkia::DrawSurface(SourceSurfac
                             const DrawOptions &aOptions)
 {
   if (aSource.IsEmpty()) {
     return;
   }
 
   MarkChanged();
 
-  sk_sp<SkImage> image = GetSkImageForSurface(aSurface);
+  Maybe<MutexAutoLock> lock;
+  sk_sp<SkImage> image = GetSkImageForSurface(aSurface, &lock);
   if (!image) {
     return;
   }
 
   SkRect destRect = RectToSkRect(aDest);
   SkRect sourceRect = RectToSkRect(aSource);
   bool forceGroup = image->isAlphaOnly() &&
                     aOptions.mCompositionOp != CompositionOp::OP_OVER;
@@ -698,17 +702,18 @@ DrawTargetSkia::DrawSurfaceWithShadow(So
                                       CompositionOp aOperator)
 {
   if (aSurface->GetSize().IsEmpty()) {
     return;
   }
 
   MarkChanged();
 
-  sk_sp<SkImage> image = GetSkImageForSurface(aSurface);
+  Maybe<MutexAutoLock> lock;
+  sk_sp<SkImage> image = GetSkImageForSurface(aSurface, &lock);
   if (!image) {
     return;
   }
 
   mCanvas->save();
   mCanvas->resetMatrix();
 
   SkPaint paint;
@@ -1528,18 +1533,19 @@ DrawTargetSkia::Mask(const Pattern &aSou
   maskOrigin.iset(maskBounds.fLeft, maskBounds.fTop);
 
   SkMatrix maskMatrix = mCanvas->getTotalMatrix();
   maskMatrix.postTranslate(-maskOrigin.fX, -maskOrigin.fY);
 
   MarkChanged();
   AutoPaintSetup paint(mCanvas, aOptions, aSource, nullptr, &maskMatrix);
 
+  Maybe<MutexAutoLock> lock;
   SkPaint maskPaint;
-  SetPaintPattern(maskPaint, aMask);
+  SetPaintPattern(maskPaint, aMask, lock);
 
   SkBitmap maskBitmap;
   if (!maskBitmap.tryAllocPixelsFlags(
         SkImageInfo::MakeA8(maskBounds.width(), maskBounds.height()),
         SkBitmap::kZeroPixels_AllocFlag)) {
     return;
   }
 
@@ -1561,17 +1567,18 @@ DrawTargetSkia::MaskSurface(const Patter
                             Point aOffset,
                             const DrawOptions &aOptions)
 {
   MarkChanged();
 
   SkMatrix invOffset = SkMatrix::MakeTrans(SkFloatToScalar(-aOffset.x), SkFloatToScalar(-aOffset.y));
   AutoPaintSetup paint(mCanvas, aOptions, aSource, nullptr, &invOffset);
 
-  sk_sp<SkImage> alphaMask = ExtractAlphaForSurface(aMask);
+  Maybe<MutexAutoLock> lock;
+  sk_sp<SkImage> alphaMask = ExtractAlphaForSurface(aMask, lock);
   if (!alphaMask) {
     gfxDebug() << *this << ": MaskSurface() failed to extract alpha for mask";
     return;
   }
 
   mCanvas->drawImage(alphaMask, aOffset.x, aOffset.y, &paint.mPaint);
 }
 
@@ -1590,17 +1597,18 @@ DrawTarget::Draw3DTransformedSurface(Sou
                                      Rect(Point(0, 0), Size(GetSize()))));
   if (xformBounds.IsEmpty()) {
     return true;
   }
   // Offset the matrix by the transformed origin.
   fullMat.PostTranslate(-xformBounds.x, -xformBounds.y, 0);
 
   // Read in the source data.
-  sk_sp<SkImage> srcImage = GetSkImageForSurface(aSurface);
+  Maybe<MutexAutoLock> lock;
+  sk_sp<SkImage> srcImage = GetSkImageForSurface(aSurface, &lock);
   if (!srcImage) {
     return true;
   }
 
   // Set up an intermediate destination surface only the size of the transformed bounds.
   // Try to pass through the source's format unmodified in both the BGRA and ARGB cases.
   RefPtr<DataSourceSurface> dstSurf =
     Factory::CreateDataSourceSurface(xformBounds.Size(),
@@ -1651,17 +1659,18 @@ bool
 DrawTargetSkia::Draw3DTransformedSurface(SourceSurface* aSurface, const Matrix4x4& aMatrix)
 {
   if (aMatrix.IsSingular()) {
     return false;
   }
 
   MarkChanged();
 
-  sk_sp<SkImage> image = GetSkImageForSurface(aSurface);
+  Maybe<MutexAutoLock> lock;
+  sk_sp<SkImage> image = GetSkImageForSurface(aSurface, &lock);
   if (!image) {
     return true;
   }
 
   mCanvas->save();
 
   SkPaint paint;
   paint.setAntiAlias(true);
@@ -1735,17 +1744,18 @@ DrawTargetSkia::UsingSkiaGPU() const
 #endif
 }
 
 #ifdef USE_SKIA_GPU
 already_AddRefed<SourceSurface>
 DrawTargetSkia::OptimizeGPUSourceSurface(SourceSurface *aSurface) const
 {
   // Check if the underlying SkImage already has an associated GrTexture.
-  sk_sp<SkImage> image = GetSkImageForSurface(aSurface);
+  Maybe<MutexAutoLock> lock;
+  sk_sp<SkImage> image = GetSkImageForSurface(aSurface, &lock);
   if (!image || image->isTextureBacked()) {
     RefPtr<SourceSurface> surface(aSurface);
     return surface.forget();
   }
 
   // Upload the SkImage to a GrTexture otherwise.
   sk_sp<SkImage> texture = image->makeTextureImage(mGrContext.get(), nullptr);
   if (texture) {
@@ -1876,17 +1886,18 @@ DrawTargetSkia::CreateSourceSurfaceFromN
 
 void
 DrawTargetSkia::CopySurface(SourceSurface *aSurface,
                             const IntRect& aSourceRect,
                             const IntPoint &aDestination)
 {
   MarkChanged();
 
-  sk_sp<SkImage> image = GetSkImageForSurface(aSurface);
+  Maybe<MutexAutoLock> lock;
+  sk_sp<SkImage> image = GetSkImageForSurface(aSurface, &lock);
   if (!image) {
     return;
   }
 
   mCanvas->save();
   mCanvas->setMatrix(SkMatrix::MakeTrans(SkIntToScalar(aDestination.x), SkIntToScalar(aDestination.y)));
   mCanvas->clipRect(SkRect::MakeIWH(aSourceRect.Width(), aSourceRect.Height()), SkClipOp::kReplace_deprecated);
 
@@ -2188,17 +2199,19 @@ DrawTargetSkia::PopLayer()
       layerMat.preTranslate(layerBounds.x(), layerBounds.y());
 
       if (layerImage) {
         paint.setShader(layerImage->makeShader(SkShader::kClamp_TileMode, SkShader::kClamp_TileMode, &layerMat));
       } else {
         paint.setColor(SK_ColorTRANSPARENT);
       }
 
-      sk_sp<SkImage> alphaMask = ExtractAlphaForSurface(layer.mMask);
+      Maybe<MutexAutoLock> lock;
+      sk_sp<SkImage> alphaMask = ExtractAlphaForSurface(layer.mMask, lock);
+
       if (!alphaMask) {
         gfxDebug() << *this << ": PopLayer() failed to extract alpha for mask";
       } else {
         mCanvas->save();
 
         // The layer may be smaller than the canvas size, so make sure drawing is
         // clipped to within the bounds of the layer.
         mCanvas->resetMatrix();
diff --git a/gfx/2d/DrawTargetSkia.cpp.1660236.later b/gfx/2d/DrawTargetSkia.cpp.1660236.later
new file mode 100644
--- /dev/null
+++ b/gfx/2d/DrawTargetSkia.cpp.1660236.later
@@ -0,0 +1,27 @@
+--- DrawTargetSkia.cpp
++++ DrawTargetSkia.cpp
+
+@@ -1883,17 +1893,22 @@ void DrawTargetSkia::PushLayerWithBlend(
+     SkMatrix inverseCTM;
+     if (mCanvas->getTotalMatrix().invert(&inverseCTM)) {
+       inverseCTM.mapRect(&bounds);
+     } else {
+       bounds.setEmpty();
+     }
+   }
+ 
+-  sk_sp<SkImage> clipImage = aMask ? GetSkImageForSurface(aMask) : nullptr;
++  // We don't pass a lock object to GetSkImageForSurface here, to force a
++  // copy of the data if this is a copy-on-write snapshot. If we instead held
++  // the lock until the corresponding PopLayer, we'd risk deadlocking if someone
++  // tried to touch the originating DrawTarget while the layer was pushed.
++  sk_sp<SkImage> clipImage =
++      aMask ? GetSkImageForSurface(aMask, nullptr) : nullptr;
+   SkMatrix clipMatrix;
+   GfxMatrixToSkiaMatrix(aMaskTransform, clipMatrix);
+   if (aMask) {
+     clipMatrix.preTranslate(aMask->GetRect().X(), aMask->GetRect().Y());
+   }
+ 
+   SkCanvas::SaveLayerRec saveRec(
+       aBounds.IsEmpty() ? nullptr : &bounds, &paint, nullptr, clipImage.get(),
diff --git a/gfx/2d/SourceSurfaceSkia.cpp b/gfx/2d/SourceSurfaceSkia.cpp
--- a/gfx/2d/SourceSurfaceSkia.cpp
+++ b/gfx/2d/SourceSurfaceSkia.cpp
@@ -35,20 +35,33 @@ SourceSurfaceSkia::GetSize() const
 }
 
 SurfaceFormat
 SourceSurfaceSkia::GetFormat() const
 {
   return mFormat;
 }
 
-sk_sp<SkImage>
-SourceSurfaceSkia::GetImage()
-{
-  MutexAutoLock lock(mChangeMutex);
+sk_sp<SkImage> SourceSurfaceSkia::GetImage(Maybe<MutexAutoLock>* aLock) {
+  // If we were provided a lock object, we can let the caller access
+  // a shared SkImage and we know it won't go away while the lock is held.
+  // Otherwise we need to call DrawTargetWillChange to ensure we have our
+  // own SkImage.
+  if (aLock) {
+    MOZ_ASSERT(aLock->isNothing());
+    aLock->emplace(mChangeMutex);
+
+    // Now that we are locked, we can check mDrawTarget. If it's null, then
+    // we're not shared and we can unlock eagerly.
+    if (!mDrawTarget) {
+      aLock->reset();
+    }
+  } else {
+    DrawTargetWillChange();
+  }
   sk_sp<SkImage> image = mImage;
   return image;
 }
 
 static sk_sp<SkData>
 MakeSkData(void* aData, int32_t aHeight, size_t aStride)
 {
   CheckedInt<size_t> size = aStride;
diff --git a/gfx/2d/SourceSurfaceSkia.h b/gfx/2d/SourceSurfaceSkia.h
--- a/gfx/2d/SourceSurfaceSkia.h
+++ b/gfx/2d/SourceSurfaceSkia.h
@@ -32,17 +32,17 @@ public:
   virtual SurfaceFormat GetFormat() const;
 
   // This is only ever called by the DT destructor, which can only ever happen
   // from one place at a time. Therefore it doesn't need to hold the ChangeMutex
   // as mSurface is never read to directly and is just there to keep the object
   // alive, which itself is refcounted in a thread-safe manner.
   void GiveSurface(sk_sp<SkSurface> &aSurface) { mSurface = aSurface; mDrawTarget = nullptr; }
 
-  sk_sp<SkImage> GetImage();
+  sk_sp<SkImage> GetImage(Maybe<MutexAutoLock>* aLock);
 
   bool InitFromData(unsigned char* aData,
                     const IntSize &aSize,
                     int32_t aStride,
                     SurfaceFormat aFormat);
 
   bool InitFromImage(const sk_sp<SkImage>& aImage,
                      SurfaceFormat aFormat = SurfaceFormat::UNKNOWN,
