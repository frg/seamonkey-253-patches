# HG changeset patch
# User Ting-Yu Lin <tlin@mozilla.com>
# Date 1504077751 -28800
#      Wed Aug 30 15:22:31 2017 +0800
# Node ID af60919d25dc5109e61c4aed6af2cb1cf560b0b8
# Parent  5b432a6db0650cf40c3bb4c2438e781d6fca7bc2
Bug 1382078 Part 2 - Make nsBindingManager::MediumFeaturesChanged() return bool directly. r=emilio

The method always returns NS_OK, and no other caller checks the nsresult.
Hence the patch.

MozReview-Commit-ID: CnYCZ8VchG

diff --git a/dom/xbl/nsBindingManager.cpp b/dom/xbl/nsBindingManager.cpp
--- a/dom/xbl/nsBindingManager.cpp
+++ b/dom/xbl/nsBindingManager.cpp
@@ -735,33 +735,32 @@ nsBindingManager::WalkAllRules(nsIStyleR
     nsIStyleRuleProcessor* ruleProcessor =
       aBinding->PrototypeBinding()->GetRuleProcessor();
     if (ruleProcessor) {
       (*(aFunc))(ruleProcessor, aData);
     }
   });
 }
 
-nsresult
-nsBindingManager::MediumFeaturesChanged(nsPresContext* aPresContext,
-                                        bool* aRulesChanged)
+bool
+nsBindingManager::MediumFeaturesChanged(nsPresContext* aPresContext)
 {
-  *aRulesChanged = false;
+  bool rulesChanged = false;
   RefPtr<nsPresContext> presContext = aPresContext;
 
-  EnumerateBoundContentBindings([=](nsXBLBinding* aBinding) {
+  EnumerateBoundContentBindings([=, &rulesChanged](nsXBLBinding* aBinding) {
     nsIStyleRuleProcessor* ruleProcessor =
       aBinding->PrototypeBinding()->GetRuleProcessor();
     if (ruleProcessor) {
       bool thisChanged = ruleProcessor->MediumFeaturesChanged(presContext);
-      *aRulesChanged = *aRulesChanged || thisChanged;
+      rulesChanged = rulesChanged || thisChanged;
     }
   });
 
-  return NS_OK;
+  return rulesChanged;
 }
 
 void
 nsBindingManager::AppendAllSheets(nsTArray<StyleSheet*>& aArray)
 {
   EnumerateBoundContentBindings([&aArray](nsXBLBinding* aBinding) {
     aBinding->PrototypeBinding()->AppendStyleSheetsTo(aArray);
   });
diff --git a/dom/xbl/nsBindingManager.h b/dom/xbl/nsBindingManager.h
--- a/dom/xbl/nsBindingManager.h
+++ b/dom/xbl/nsBindingManager.h
@@ -130,18 +130,17 @@ public:
 
   void WalkAllRules(nsIStyleRuleProcessor::EnumFunc aFunc,
                     ElementDependentRuleProcessorData* aData);
   /**
    * Do any processing that needs to happen as a result of a change in
    * the characteristics of the medium, and return whether this rule
    * processor's rules have changed (e.g., because of media queries).
    */
-  nsresult MediumFeaturesChanged(nsPresContext* aPresContext,
-                                 bool* aRulesChanged);
+  bool MediumFeaturesChanged(nsPresContext* aPresContext);
 
   void AppendAllSheets(nsTArray<mozilla::StyleSheet*>& aArray);
 
   void Traverse(nsIContent *aContent,
                             nsCycleCollectionTraversalCallback &cb);
 
   NS_DECL_CYCLE_COLLECTION_CLASS(nsBindingManager)
 
diff --git a/layout/style/nsStyleSet.cpp b/layout/style/nsStyleSet.cpp
--- a/layout/style/nsStyleSet.cpp
+++ b/layout/style/nsStyleSet.cpp
@@ -2681,18 +2681,18 @@ nsStyleSet::MediumFeaturesChanged(bool a
     stylesChanged = stylesChanged || thisChanged;
   }
   for (nsIStyleRuleProcessor* processor : mScopedDocSheetRuleProcessors) {
     bool thisChanged = processor->MediumFeaturesChanged(presContext);
     stylesChanged = stylesChanged || thisChanged;
   }
 
   if (mBindingManager) {
-    bool thisChanged = false;
-    mBindingManager->MediumFeaturesChanged(presContext, &thisChanged);
+    bool thisChanged =
+      mBindingManager->MediumFeaturesChanged(presContext);
     stylesChanged = stylesChanged || thisChanged;
   }
 
   if (stylesChanged) {
     return eRestyle_Subtree;
   }
   if (aViewportChanged && mUsesViewportUnits) {
     // Rebuild all style data without rerunning selector matching.
