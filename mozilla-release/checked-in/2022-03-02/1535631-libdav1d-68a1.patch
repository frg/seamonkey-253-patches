# HG changeset patch
# User Alex Chronopoulos <achronop@gmail.com>
# Date 1553197395 0
#      Thu Mar 21 19:43:15 2019 +0000
# Node ID 2b9f4f5a278652fcae03bfad21e415a5a934a5d1
# Parent  8693cb66569617c4faaefa4d076f947779bb1ff3
Bug 1535631 - Use 16 byte stack alignment on dav1d in OSX. r=TD-Linux

Differential Revision: https://phabricator.services.mozilla.com/D24382

diff --git a/media/libdav1d/asm/moz.build b/media/libdav1d/asm/moz.build
--- a/media/libdav1d/asm/moz.build
+++ b/media/libdav1d/asm/moz.build
@@ -20,16 +20,19 @@ LOCAL_INCLUDES += [
 # see: third_party/dav1d/include/dav1d/common.h
 DEFINES['DAV1D_API'] = ''
 
 CFLAGS += [
     # find the config.h file.
     '-I%s/dist/include/dav1d/' % TOPOBJDIR,
 ]
 
+# Default stack aligment is 16 bytes.
+stack_alignment = 16
+
 # Attaching config.asm file
 if CONFIG['CPU_ARCH'] == 'x86':
     if CONFIG['OS_TARGET'] == 'WINNT':
         ASFLAGS += ['-I%s/media/libdav1d/asm/x86_32/win/' % TOPSRCDIR]
     else:
         ASFLAGS += ['-I%s/media/libdav1d/asm/x86_32/' % TOPSRCDIR]
 
 if CONFIG['CPU_ARCH'] == 'x86_64':
@@ -37,23 +40,27 @@ if CONFIG['CPU_ARCH'] == 'x86_64':
         ASFLAGS += ['-I%s/media/libdav1d/asm/x86_64/linux/' % TOPSRCDIR]
     elif CONFIG['OS_TARGET'] == 'Darwin':
         ASFLAGS += ['-I%s/media/libdav1d/asm/x86_64/osx/' % TOPSRCDIR]
     elif CONFIG['OS_TARGET'] == 'WINNT':
         ASFLAGS += ['-I%s/media/libdav1d/asm/x86_64/' % TOPSRCDIR]
     else:
         error('Platform %s is not expected' % CONFIG['OS_TARGET'])
 
-    if CONFIG['OS_TARGET'] in ('Darwin', 'WINNT'):
-        # Change the default stack alignment (16) to 32
+    if CONFIG['OS_TARGET'] == 'WINNT':
+        # Change the default stack alignment (16) to 32 bytes.
+        stack_alignment = 32
         if CONFIG['CC_TYPE'] == 'clang':
             CFLAGS += ['-mstack-alignment=32']
         elif CONFIG['CC_TYPE'] == 'gcc':
             CFLAGS += ['-mpreferred-stack-boundary=5']
 
+# Set the macro here instead of config.h
+DEFINES['STACK_ALIGNMENT'] = stack_alignment
+
 if CONFIG['CPU_ARCH'] in ('x86', 'x86_64'):
     SOURCES += [
         '../../../third_party/dav1d/src/x86/cpu.c',
     ]
 
     EXPORTS.dav1d += [
         '../../../third_party/dav1d/src/x86/cpu.h',
     ]
diff --git a/media/libdav1d/asm/x86_64/osx/config.asm b/media/libdav1d/asm/x86_64/osx/config.asm
--- a/media/libdav1d/asm/x86_64/osx/config.asm
+++ b/media/libdav1d/asm/x86_64/osx/config.asm
@@ -4,10 +4,10 @@
 %define ARCH_X86_32 0
 
 %define ARCH_X86_64 1
 
 %define PIC 1
 
 %define PREFIX 1
 
-%define STACK_ALIGNMENT 32
+%define STACK_ALIGNMENT 16
 
diff --git a/media/libdav1d/config.h b/media/libdav1d/config.h
--- a/media/libdav1d/config.h
+++ b/media/libdav1d/config.h
@@ -47,21 +47,14 @@
 // (HAVE_MEMALIGN | HAVE_ALIGNED_MALLOC | HAVE_POSIX_MEMALIGN)
 // HAVE_UNISTD_H
 
 // Important when asm is enabled
 #if defined(__APPLE__) || (ARCH_x86_32 == 1 && defined(_WIN32))
 #  define PREFIX 1
 #endif
 
-#if ARCH_x86_32 == 1 || \
-    (ARCH_X86_64 == 1 && defined(__linux__) && !defined(__ANDROID__))
-#  define STACK_ALIGNMENT 16
-#else
-#  define STACK_ALIGNMENT 32
-#endif
-
 #if defined(_WIN32) || defined(__CYGWIN__)
 // _WIN32_WINNT 0x0601 is set in global macros
 #  define UNICODE 1
 #  define _UNICODE 1
 #  define __USE_MINGW_ANSI_STDIO 1
 #endif
diff --git a/media/libdav1d/moz.build b/media/libdav1d/moz.build
--- a/media/libdav1d/moz.build
+++ b/media/libdav1d/moz.build
@@ -25,28 +25,32 @@ entrypoint_source_files = [
 ]
 SOURCES += [f for f in entrypoint_source_files]
 
 # Don't export DAV1D_API symbols from libxul
 # see: third_party/dav1d/include/dav1d/common.h
 DEFINES['DAV1D_API'] = ''
 
 if CONFIG['MOZ_DAV1D_ASM']:
-    # Default stack alignment is 16 bytes
+    # Default stack alignment is 16 bytes.
+    stack_alignment = 16
     DIRS += ['asm']
-    if CONFIG['OS_TARGET'] in ('WINNT', 'Darwin') and CONFIG['CPU_ARCH'] == 'x86_64':
-         # Update stack alignment to 32 bytes
-         if CONFIG['CC_TYPE'] == 'clang':
-             CFLAGS += ['-mstack-alignment=32']
-             for ep in entrypoint_source_files:
-                 SOURCES[ep].flags += ['-mstackrealign']
-         elif CONFIG['CC_TYPE'] == 'gcc':
-             CFLAGS += ['-mpreferred-stack-boundary=5']
-             for ep in entrypoint_source_files:
-                 SOURCES[ep].flags += ['-mincoming-stack-boundary=4']
+    if CONFIG['OS_TARGET'] == 'WINNT' and CONFIG['CPU_ARCH'] == 'x86_64':
+        stack_alignment = 32
+        # Update stack alignment to 32 bytes.
+        if CONFIG['CC_TYPE'] == 'clang':
+            CFLAGS += ['-mstack-alignment=32']
+            for ep in entrypoint_source_files:
+                SOURCES[ep].flags += ['-mstackrealign']
+        elif CONFIG['CC_TYPE'] == 'gcc':
+            CFLAGS += ['-mpreferred-stack-boundary=5']
+            for ep in entrypoint_source_files:
+                SOURCES[ep].flags += ['-mincoming-stack-boundary=4']
+    # Set the macro here instead of config.h
+    DEFINES['STACK_ALIGNMENT'] = stack_alignment
 
 if CONFIG['OS_TARGET'] == 'Linux':
     # For fuzzing, We only support building on Linux currently.
     include('/tools/fuzzing/libfuzzer-config.mozbuild')
     if CONFIG['FUZZING_INTERFACES']:
         TEST_DIRS += [
             'test/fuzztest'
         ]
