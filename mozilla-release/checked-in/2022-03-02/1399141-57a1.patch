# HG changeset patch
# User Jon Coppeard <jcoppeard@mozilla.com>
# Date 1505298072 -3600
# Node ID ff90282974cef1e1b73b826f165aa7be26246a6a
# Parent  9e0e3b845834ffcee026680729bcc31e006ed4bb
Bug 1399141 - Attribute malloc memory when creating reflector object r=baku

diff --git a/dom/bindings/BindingUtils.h b/dom/bindings/BindingUtils.h
--- a/dom/bindings/BindingUtils.h
+++ b/dom/bindings/BindingUtils.h
@@ -2825,17 +2825,17 @@ ToSupportsIsOnPrimaryInheritanceChain(T*
 }
 
 // Get the size of allocated memory to associate with a binding JSObject for a
 // native object. This is supplied to the JS engine to allow it to schedule GC
 // when necessary.
 //
 // This function supplies a default value and is overloaded for specific native
 // object types.
-inline uint64_t
+inline size_t
 BindingJSObjectMallocBytes(void *aNativePtr)
 {
   return 0;
 }
 
 // The BindingJSObjectCreator class is supposed to be used by a caller that
 // wants to create and initialise a binding JSObject. After initialisation has
 // been successfully completed it should call ForgetObject().
@@ -2874,34 +2874,34 @@ public:
     aReflector.set(js::NewProxyObject(aCx, aHandler, aExpandoValue, aProto,
                                       options));
     if (aReflector) {
       js::SetProxyReservedSlot(aReflector, DOM_OBJECT_SLOT, JS::PrivateValue(aNative));
       mNative = aNative;
       mReflector = aReflector;
     }
 
-    if (uint64_t mallocBytes = BindingJSObjectMallocBytes(aNative)) {
+    if (size_t mallocBytes = BindingJSObjectMallocBytes(aNative)) {
       JS_updateMallocCounter(aCx, mallocBytes);
     }
   }
 
   void
   CreateObject(JSContext* aCx, const JSClass* aClass,
                JS::Handle<JSObject*> aProto,
                T* aNative, JS::MutableHandle<JSObject*> aReflector)
   {
     aReflector.set(JS_NewObjectWithGivenProto(aCx, aClass, aProto));
     if (aReflector) {
       js::SetReservedSlot(aReflector, DOM_OBJECT_SLOT, JS::PrivateValue(aNative));
       mNative = aNative;
       mReflector = aReflector;
     }
 
-    if (uint64_t mallocBytes = BindingJSObjectMallocBytes(aNative)) {
+    if (size_t mallocBytes = BindingJSObjectMallocBytes(aNative)) {
       JS_updateMallocCounter(aCx, mallocBytes);
     }
   }
 
   void
   InitializationSucceeded()
   {
     void* dummy;
diff --git a/dom/canvas/CanvasRenderingContext2D.cpp b/dom/canvas/CanvasRenderingContext2D.cpp
--- a/dom/canvas/CanvasRenderingContext2D.cpp
+++ b/dom/canvas/CanvasRenderingContext2D.cpp
@@ -1782,22 +1782,16 @@ CanvasRenderingContext2D::RegisterAlloca
   // PeristentBufferProvider, rather than here.
   static bool registered = false;
   // FIXME: Disable the reporter for now, see bug 1241865
   if (!registered && false) {
     registered = true;
     RegisterStrongMemoryReporter(new Canvas2dPixelsReporter());
   }
 
-  gCanvasAzureMemoryUsed += mWidth * mHeight * 4;
-  JSContext* context = nsContentUtils::GetCurrentJSContext();
-  if (context) {
-    JS_updateMallocCounter(context, mWidth * mHeight * 4);
-  }
-
   JSObject* wrapper = GetWrapperPreserveColor();
   if (wrapper) {
     CycleCollectedJSRuntime::Get()->
       AddZoneWaitingForGC(JS::GetObjectZone(wrapper));
   }
 }
 
 static already_AddRefed<LayerManager>
@@ -6560,10 +6554,16 @@ CanvasPath::EnsurePathBuilder() const
   }
 
   // if there is not pathbuilder, there must be a path
   MOZ_ASSERT(mPath);
   mPathBuilder = mPath->CopyToBuilder();
   mPath = nullptr;
 }
 
+size_t
+BindingJSObjectMallocBytes(CanvasRenderingContext2D* aContext)
+{
+  return aContext->GetWidth() * aContext->GetHeight() * 4;
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/canvas/CanvasRenderingContext2D.h b/dom/canvas/CanvasRenderingContext2D.h
--- a/dom/canvas/CanvasRenderingContext2D.h
+++ b/dom/canvas/CanvasRenderingContext2D.h
@@ -1166,12 +1166,14 @@ protected:
     if (aPerCSSPixel)
       *aPerCSSPixel = cssPixel;
   }
 
   friend struct CanvasBidiProcessor;
   friend class CanvasDrawObserver;
 };
 
+size_t BindingJSObjectMallocBytes(CanvasRenderingContext2D* aContext);
+
 } // namespace dom
 } // namespace mozilla
 
 #endif /* CanvasRenderingContext2D_h */
diff --git a/dom/canvas/ImageBitmap.cpp b/dom/canvas/ImageBitmap.cpp
--- a/dom/canvas/ImageBitmap.cpp
+++ b/dom/canvas/ImageBitmap.cpp
@@ -30,55 +30,16 @@ NS_IMPL_CYCLE_COLLECTION_WRAPPERCACHE(Im
 NS_IMPL_CYCLE_COLLECTING_ADDREF(ImageBitmap)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(ImageBitmap)
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(ImageBitmap)
   NS_WRAPPERCACHE_INTERFACE_MAP_ENTRY
   NS_INTERFACE_MAP_ENTRY(nsISupports)
 NS_INTERFACE_MAP_END
 
 /*
- * This helper function is used to notify DOM that aBytes memory is allocated
- * here so that we could trigger GC appropriately.
- */
-static void
-RegisterAllocation(nsIGlobalObject* aGlobal, size_t aBytes)
-{
-  AutoJSAPI jsapi;
-  if (jsapi.Init(aGlobal)) {
-    JS_updateMallocCounter(jsapi.cx(), aBytes);
-  }
-}
-
-static void
-RegisterAllocation(nsIGlobalObject* aGlobal, SourceSurface* aSurface)
-{
-  // Calculate how many bytes are used.
-  const int bytesPerPixel = BytesPerPixel(aSurface->GetFormat());
-  const size_t bytes =
-    aSurface->GetSize().height * aSurface->GetSize().width * bytesPerPixel;
-
-  // Register.
-  RegisterAllocation(aGlobal, bytes);
-}
-
-static void
-RegisterAllocation(nsIGlobalObject* aGlobal, layers::Image* aImage)
-{
-  // Calculate how many bytes are used.
-  if (aImage->GetFormat() == mozilla::ImageFormat::PLANAR_YCBCR) {
-    RegisterAllocation(aGlobal, aImage->AsPlanarYCbCrImage()->GetDataSize());
-  } else if (aImage->GetFormat() == mozilla::ImageFormat::NV_IMAGE) {
-    RegisterAllocation(aGlobal, aImage->AsNVImage()->GetBufferSize());
-  } else {
-    RefPtr<SourceSurface> surface = aImage->GetAsSourceSurface();
-    RegisterAllocation(aGlobal, surface);
-  }
-}
-
-/*
  * If either aRect.width or aRect.height are negative, then return a new IntRect
  * which represents the same rectangle as the aRect does but with positive width
  * and height.
  */
 static IntRect
 FixUpNegativeDimension(const IntRect& aRect, ErrorResult& aRv)
 {
   gfx::IntRect rect = aRect;
@@ -441,16 +402,17 @@ ImageBitmap::ImageBitmap(nsIGlobalObject
                          gfxAlphaType aAlphaType)
   : mParent(aGlobal)
   , mData(aData)
   , mSurface(nullptr)
   , mDataWrapper(new ImageUtils(mData))
   , mPictureRect(0, 0, aData->GetSize().width, aData->GetSize().height)
   , mAlphaType(aAlphaType)
   , mIsCroppingAreaOutSideOfSourceImage(false)
+  , mAllocatedImageData(false)
 {
   MOZ_ASSERT(aData, "aData is null in ImageBitmap constructor.");
 }
 
 ImageBitmap::~ImageBitmap()
 {
 }
 
@@ -708,18 +670,17 @@ ImageBitmap::ToCloneData() const
 /* static */ already_AddRefed<ImageBitmap>
 ImageBitmap::CreateFromCloneData(nsIGlobalObject* aGlobal,
                                  ImageBitmapCloneData* aData)
 {
   RefPtr<layers::Image> data = CreateImageFromSurface(aData->mSurface);
 
   RefPtr<ImageBitmap> ret = new ImageBitmap(aGlobal, data, aData->mAlphaType);
 
-  // Report memory allocation.
-  RegisterAllocation(aGlobal, aData->mSurface);
+  ret->mAllocatedImageData = true;
 
   ret->mIsCroppingAreaOutSideOfSourceImage =
     aData->mIsCroppingAreaOutSideOfSourceImage;
 
   ErrorResult rv;
   ret->SetPictureRect(aData->mPictureRect, rv);
   return ret.forget();
 }
@@ -746,18 +707,17 @@ ImageBitmap::CreateFromOffscreenCanvas(n
     return nullptr;
   }
 
   RefPtr<layers::Image> data =
     CreateImageFromSurface(surface);
 
   RefPtr<ImageBitmap> ret = new ImageBitmap(aGlobal, data);
 
-  // Report memory allocation.
-  RegisterAllocation(aGlobal, surface);
+  ret->mAllocatedImageData = true;
 
   return ret.forget();
 }
 
 /* static */ already_AddRefed<ImageBitmap>
 ImageBitmap::CreateInternal(nsIGlobalObject* aGlobal, HTMLImageElement& aImageEl,
                             const Maybe<IntRect>& aCropRect, ErrorResult& aRv)
 {
@@ -893,19 +853,18 @@ ImageBitmap::CreateInternal(nsIGlobalObj
 
   if (NS_WARN_IF(!data)) {
     aRv.Throw(NS_ERROR_NOT_AVAILABLE);
     return nullptr;
   }
 
   RefPtr<ImageBitmap> ret = new ImageBitmap(aGlobal, data);
 
-  // Report memory allocation if needed.
   if (needToReportMemoryAllocation) {
-    RegisterAllocation(aGlobal, croppedSurface);
+    ret->mAllocatedImageData = true;
   }
 
   // Set the picture rectangle.
   if (ret && aCropRect.isSome()) {
     ret->SetPictureRect(cropRect, aRv);
   }
 
   // Set mIsCroppingAreaOutSideOfSourceImage.
@@ -962,18 +921,17 @@ ImageBitmap::CreateInternal(nsIGlobalObj
   if (NS_WARN_IF(!data)) {
     aRv.Throw(NS_ERROR_NOT_AVAILABLE);
     return nullptr;
   }
 
   // Create an ImageBimtap.
   RefPtr<ImageBitmap> ret = new ImageBitmap(aGlobal, data, alphaType);
 
-  // Report memory allocation.
-  RegisterAllocation(aGlobal, data);
+  ret->mAllocatedImageData = true;
 
   // The cropping information has been handled in the CreateImageFromRawData()
   // function.
 
   // Set mIsCroppingAreaOutSideOfSourceImage.
   ret->SetIsCroppingAreaOutSideOfSourceImage(imageSize, aCropRect);
 
   return ret.forget();
@@ -1006,18 +964,17 @@ ImageBitmap::CreateInternal(nsIGlobalObj
 
   if (NS_WARN_IF(!data)) {
     aRv.Throw(NS_ERROR_NOT_AVAILABLE);
     return nullptr;
   }
 
   RefPtr<ImageBitmap> ret = new ImageBitmap(aGlobal, data);
 
-  // Report memory allocation.
-  RegisterAllocation(aGlobal, surface);
+  ret->mAllocatedImageData = true;
 
   // Set the picture rectangle.
   if (ret && aCropRect.isSome()) {
     ret->SetPictureRect(aCropRect.ref(), aRv);
   }
 
   // Set mIsCroppingAreaOutSideOfSourceImage.
   ret->SetIsCroppingAreaOutSideOfSourceImage(surface->GetSize(), aCropRect);
@@ -1251,18 +1208,17 @@ protected:
       imageBitmap->SetPictureRect(mCropRect.ref(), rv);
 
       if (rv.Failed()) {
         mPromise->MaybeReject(rv);
         return false;
       }
     }
 
-    // Report memory allocation.
-    RegisterAllocation(mGlobalObject, imageBitmap->mData);
+    imageBitmap->mAllocatedImageData = true;
 
     mPromise->MaybeResolve(imageBitmap);
     return true;
   }
 
   // Will return null on failure.  In that case, mPromise will already
   // be rejected with the right thing.
   virtual already_AddRefed<ImageBitmap> CreateImageBitmap() = 0;
@@ -1541,18 +1497,17 @@ ImageBitmap::ReadStructuredClone(JSConte
       error.SuppressException();
       return nullptr;
     }
 
     if (!GetOrCreateDOMReflector(aCx, imageBitmap, &value)) {
       return nullptr;
     }
 
-    // Report memory allocation.
-    RegisterAllocation(aParent, aClonedSurfaces[aIndex]);
+    imageBitmap->mAllocatedImageData = true;
   }
 
   return &(value.toObject());
 }
 
 /*static*/ bool
 ImageBitmap::WriteStructuredClone(JSStructuredCloneWriter* aWriter,
                                   nsTArray<RefPtr<DataSourceSurface>>& aClonedSurfaces,
@@ -2141,26 +2096,52 @@ ImageBitmap::Create(nsIGlobalObject* aGl
     return promise.forget();
   }
 
   // Create an ImageBimtap.
   // Assume the data from an external buffer is not alpha-premultiplied.
   RefPtr<ImageBitmap> imageBitmap = new ImageBitmap(aGlobal, data,
                                                     gfxAlphaType::NonPremult);
 
-  // Report memory allocation.
-  RegisterAllocation(aGlobal, data);
+  imageBitmap->mAllocatedImageData = true;
 
   // We don't need to call SetPictureRect() here because there is no cropping
   // supported and the ImageBitmap's mPictureRect is the size of the source
   // image in default
 
   // We don't need to set mIsCroppingAreaOutSideOfSourceImage here because there
   // is no cropping supported and the mIsCroppingAreaOutSideOfSourceImage is
   // false in default.
 
   AsyncFulfillImageBitmapPromise(promise, imageBitmap);
 
   return promise.forget();
 }
 
+size_t
+ImageBitmap::GetAllocatedSize() const
+{
+  if (!mAllocatedImageData) {
+    return 0;
+  }
+
+  // Calculate how many bytes are used.
+  if (mData->GetFormat() == mozilla::ImageFormat::PLANAR_YCBCR) {
+    return mData->AsPlanarYCbCrImage()->GetDataSize();
+  }
+
+  if (mData->GetFormat() == mozilla::ImageFormat::NV_IMAGE) {
+    return mData->AsNVImage()->GetBufferSize();
+  }
+
+  RefPtr<SourceSurface> surface = mData->GetAsSourceSurface();
+  const int bytesPerPixel = BytesPerPixel(surface->GetFormat());
+  return surface->GetSize().height * surface->GetSize().width * bytesPerPixel;
+}
+
+size_t
+BindingJSObjectMallocBytes(ImageBitmap* aBitmap)
+{
+  return aBitmap->GetAllocatedSize();
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/canvas/ImageBitmap.h b/dom/canvas/ImageBitmap.h
--- a/dom/canvas/ImageBitmap.h
+++ b/dom/canvas/ImageBitmap.h
@@ -174,16 +174,18 @@ public:
   MappedDataLength(ImageBitmapFormat aFormat, ErrorResult& aRv);
 
   already_AddRefed<Promise>
   MapDataInto(JSContext* aCx,
               ImageBitmapFormat aFormat,
               const ArrayBufferViewOrArrayBuffer& aBuffer,
               int32_t aOffset, ErrorResult& aRv);
 
+  size_t GetAllocatedSize() const;
+
 protected:
 
   /*
    * The default value of aIsPremultipliedAlpha is TRUE because that the
    * data stored in HTMLImageElement, HTMLVideoElement, HTMLCanvasElement,
    * CanvasRenderingContext2D are alpha-premultiplied in default.
    *
    * Actually, if one HTMLCanvasElement's rendering context is WebGLContext, it
@@ -279,16 +281,20 @@ protected:
    * Set mIsCroppingAreaOutSideOfSourceImage if image bitmap was cropped to the
    * source rectangle so that it contains any transparent black pixels (cropping
    * area is outside of the source image).
    * This is used in mapDataInto() to check if we should reject promise with
    * IndexSizeError.
    */
   bool mIsCroppingAreaOutSideOfSourceImage;
 
+  /*
+   * Whether this object allocated allocated and owns the image data.
+   */
+  bool mAllocatedImageData;
 };
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // mozilla_dom_ImageBitmap_h
 
 
diff --git a/dom/file/Blob.cpp b/dom/file/Blob.cpp
--- a/dom/file/Blob.cpp
+++ b/dom/file/Blob.cpp
@@ -287,17 +287,17 @@ Blob::IsMemoryFile() const
 }
 
 void
 Blob::CreateInputStream(nsIInputStream** aStream, ErrorResult& aRv)
 {
   mImpl->CreateInputStream(aStream, aRv);
 }
 
-uint64_t
+size_t
 BindingJSObjectMallocBytes(Blob* aBlob)
 {
   MOZ_ASSERT(aBlob);
   return aBlob->GetAllocationSize();
 }
 
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/file/Blob.h b/dom/file/Blob.h
--- a/dom/file/Blob.h
+++ b/dom/file/Blob.h
@@ -146,14 +146,14 @@ protected:
   RefPtr<BlobImpl> mImpl;
 
 private:
   nsCOMPtr<nsISupports> mParent;
 };
 
 // Override BindingJSObjectMallocBytes for blobs to tell the JS GC how much
 // memory is held live by the binding object.
-uint64_t BindingJSObjectMallocBytes(Blob* aBlob);
+size_t BindingJSObjectMallocBytes(Blob* aBlob);
 
 } // namespace dom
 } // namespace mozilla
 
 #endif // mozilla_dom_Blob_h
