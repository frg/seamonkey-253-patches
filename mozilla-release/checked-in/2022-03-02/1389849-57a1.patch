# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1502578082 25200
#      Sat Aug 12 15:48:02 2017 -0700
# Node ID 0db59c9c886ff64518d28a1c6815b98386895dd2
# Parent  c31b54c90a2ab3b6e9c13c0e8b60380331c38f07
Bug 1389849: Remove ext-geolocation.js. r=mixedpuppy

MozReview-Commit-ID: BJTAIsxVlKL

diff --git a/toolkit/components/extensions/Extension.jsm b/toolkit/components/extensions/Extension.jsm
--- a/toolkit/components/extensions/Extension.jsm
+++ b/toolkit/components/extensions/Extension.jsm
@@ -1116,38 +1116,54 @@ this.Extension = class extends Extension
         this.defaultLocale);
 
       locale = matches[0];
     }
 
     return super.initLocale(locale);
   }
 
-  initUnlimitedStoragePermission() {
-    const principal = this.principal;
+  updatePermissions(reason) {
+    const {principal} = this;
 
-    // Check if the site permission has already been set for the extension by the WebExtensions
-    // internals (instead of being manually allowed by the user).
-    const hasSitePermission = Services.perms.testPermissionFromPrincipal(
-      principal, "WebExtensions-unlimitedStorage"
-    );
+    const testPermission = perm =>
+      Services.perms.testPermissionFromPrincipal(principal, perm);
 
-    if (this.hasPermission("unlimitedStorage")) {
-      // Set the indexedDB permission and a custom "WebExtensions-unlimitedStorage" to remember
-      // that the permission hasn't been selected manually by the user.
-      Services.perms.addFromPrincipal(principal, "WebExtensions-unlimitedStorage",
-                                      Services.perms.ALLOW_ACTION);
-      Services.perms.addFromPrincipal(principal, "indexedDB", Services.perms.ALLOW_ACTION);
-      Services.perms.addFromPrincipal(principal, "persistent-storage", Services.perms.ALLOW_ACTION);
-    } else if (hasSitePermission) {
-      // Remove the indexedDB permission if it has been enabled using the
-      // unlimitedStorage WebExtensions permissions.
-      Services.perms.removeFromPrincipal(principal, "WebExtensions-unlimitedStorage");
-      Services.perms.removeFromPrincipal(principal, "indexedDB");
-      Services.perms.removeFromPrincipal(principal, "persistent-storage");
+    // Only update storage permissions when the extension changes in
+    // some way.
+    if (reason !== "APP_STARTUP" && reason !== "APP_SHUTDOWN") {
+      if (this.hasPermission("unlimitedStorage")) {
+        // Set the indexedDB permission and a custom "WebExtensions-unlimitedStorage" to remember
+        // that the permission hasn't been selected manually by the user.
+        Services.perms.addFromPrincipal(principal, "WebExtensions-unlimitedStorage",
+                                        Services.perms.ALLOW_ACTION);
+        Services.perms.addFromPrincipal(principal, "indexedDB", Services.perms.ALLOW_ACTION);
+        Services.perms.addFromPrincipal(principal, "persistent-storage", Services.perms.ALLOW_ACTION);
+      } else {
+        // Remove the indexedDB permission if it has been enabled using the
+        // unlimitedStorage WebExtensions permissions.
+        Services.perms.removeFromPrincipal(principal, "WebExtensions-unlimitedStorage");
+        Services.perms.removeFromPrincipal(principal, "indexedDB");
+        Services.perms.removeFromPrincipal(principal, "persistent-storage");
+      }
+    }
+
+    // Never change geolocation permissions at shutdown, since it uses a
+    // session-only permission.
+    if (reason !== "APP_SHUTDOWN") {
+      if (this.hasPermission("geolocation")) {
+        if (testPermission("geo") === Services.perms.UNKNOWN_ACTION) {
+          Services.perms.addFromPrincipal(principal, "geo",
+                                          Services.perms.ALLOW_ACTION,
+                                          Services.perms.EXPIRE_SESSION);
+        }
+      } else if (reason !== "APP_STARTUP" &&
+                 testPermission("geo") === Services.perms.ALLOW_ACTION) {
+        Services.perms.removeFromPrincipal(principal, "geo");
+      }
     }
   }
 
   startup() {
     this.startupPromise = this._startup();
 
     return this.startupPromise;
   }
@@ -1190,17 +1206,17 @@ this.Extension = class extends Extension
         return;
       }
 
       GlobalManager.init(this);
 
       this.policy.active = false;
       this.policy = processScript.initExtension(this.serialize(), this);
 
-      this.initUnlimitedStoragePermission();
+      this.updatePermissions(this.startupReason);
 
       // The "startup" Management event sent on the extension instance itself
       // is emitted just before the Management "startup" event,
       // and it is used to run code that needs to be executed before
       // any of the "startup" listeners.
       this.emit("startup", this);
       Management.emit("startup", this);
 
@@ -1301,16 +1317,18 @@ this.Extension = class extends Extension
       StartupCache.clearAddonData(this.id);
     }
 
     let data = Services.ppmm.initialProcessData;
     data["Extension:Extensions"] = data["Extension:Extensions"].filter(e => e.id !== this.id);
 
     Services.ppmm.removeMessageListener(this.MESSAGE_EMIT_EVENT, this);
 
+    this.updatePermissions(this.shutdownReason);
+
     if (!this.manifest) {
       this.policy.active = false;
 
       return this.cleanupGeneratedFile();
     }
 
     GlobalManager.uninit(this);
 
diff --git a/toolkit/components/extensions/ext-geolocation.js b/toolkit/components/extensions/ext-geolocation.js
deleted file mode 100644
--- a/toolkit/components/extensions/ext-geolocation.js
+++ /dev/null
@@ -1,31 +0,0 @@
-"use strict";
-
-ChromeUtils.defineModuleGetter(this, "Services",
-                               "resource://gre/modules/Services.jsm");
-
-// If the user has changed the permission on the addon to something other than
-// always allow, then we want to preserve that choice.  We only set the
-// permission if it is not set (unknown_action), and we only remove the
-// permission on shutdown if it is always allow.
-
-this.geolocation = class extends ExtensionAPI {
-  onStartup() {
-    let {extension} = this;
-
-    if (extension.hasPermission("geolocation") &&
-        Services.perms.testPermission(extension.principal.URI, "geo") == Services.perms.UNKNOWN_ACTION) {
-      Services.perms.add(extension.principal.URI, "geo",
-                         Services.perms.ALLOW_ACTION,
-                         Services.perms.EXPIRE_SESSION);
-    }
-  }
-
-  onShutdown() {
-    let {extension} = this;
-
-    if (extension.hasPermission("geolocation") &&
-        Services.perms.testPermission(extension.principal.URI, "geo") == Services.perms.ALLOW_ACTION) {
-      Services.perms.remove(extension.principal.URI, "geo");
-    }
-  }
-};
diff --git a/toolkit/components/extensions/ext-toolkit.json b/toolkit/components/extensions/ext-toolkit.json
--- a/toolkit/components/extensions/ext-toolkit.json
+++ b/toolkit/components/extensions/ext-toolkit.json
@@ -52,20 +52,16 @@
   "extension": {
     "url": "chrome://extensions/content/ext-extension.js",
     "schema": "chrome://extensions/content/schemas/extension.json",
     "scopes": ["addon_parent"],
     "paths": [
       ["extension"]
     ]
   },
-  "geolocation": {
-    "url": "chrome://extensions/content/ext-geolocation.js",
-    "events": ["startup"]
-  },
   "i18n": {
     "url": "chrome://extensions/content/ext-i18n.js",
     "schema": "chrome://extensions/content/schemas/i18n.json",
     "scopes": ["addon_parent", "content_child", "devtools_child"],
     "paths": [
       ["i18n"]
     ]
   },
diff --git a/toolkit/components/extensions/jar.mn b/toolkit/components/extensions/jar.mn
--- a/toolkit/components/extensions/jar.mn
+++ b/toolkit/components/extensions/jar.mn
@@ -8,17 +8,16 @@ toolkit.jar:
     content/extensions/ext-alarms.js
     content/extensions/ext-backgroundPage.js
     content/extensions/ext-browser-content.js
     content/extensions/ext-browserSettings.js
     content/extensions/ext-contextualIdentities.js
     content/extensions/ext-cookies.js
     content/extensions/ext-downloads.js
     content/extensions/ext-extension.js
-    content/extensions/ext-geolocation.js
     content/extensions/ext-i18n.js
 #ifndef ANDROID
     content/extensions/ext-identity.js
 #endif
     content/extensions/ext-idle.js
     content/extensions/ext-management.js
     content/extensions/ext-notifications.js
     content/extensions/ext-permissions.js
