# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1500908249 -3600
# Node ID 6db4b0af686c536a14435651d41cebca259a6beb
# Parent  aa0d6c7d80f2a5f4963806b5fd1cc93ca6266876
Bug 1379269, part 2 - Invalidate and notify when clearing cached media features for Windows accent color changes. r=dholbert

MozReview-Commit-ID: 66tP4la4xy9

diff --git a/layout/base/nsPresContext.cpp b/layout/base/nsPresContext.cpp
--- a/layout/base/nsPresContext.cpp
+++ b/layout/base/nsPresContext.cpp
@@ -1844,26 +1844,17 @@ nsPresContext::ThemeChangedInternal()
     sLookAndFeelChanged = false;
 
     // Vector images (SVG) may be using theme colors so we discard all cached
     // surfaces. (We could add a vector image only version of DiscardAll, but
     // in bug 940625 we decided theme changes are rare enough not to bother.)
     image::SurfaceCacheUtils::DiscardAll();
   }
 
-  // This will force the system metrics to be generated the next time they're used
-  nsCSSRuleProcessor::FreeSystemMetrics();
-
-  // Changes to system metrics can change media queries on them, or
-  // :-moz-system-metric selectors (which requires eRestyle_Subtree).
-  // Changes in theme can change system colors (whose changes are
-  // properly reflected in computed style data), system fonts (whose
-  // changes are not), and -moz-appearance (whose changes likewise are
-  // not), so we need to reflow.
-  MediaFeatureValuesChanged(eRestyle_Subtree, NS_STYLE_HINT_REFLOW);
+  RefreshSystemMetrics();
 
   // Recursively notify all remote leaf descendants that the
   // system theme has changed.
   nsContentUtils::CallOnAllRemoteChildren(mDocument->GetWindow(),
                                           NotifyThemeChanged, nullptr);
 }
 
 void
@@ -1890,28 +1881,43 @@ nsPresContext::SysColorChangedInternal()
 
   if (sLookAndFeelChanged) {
      // Don't use the cached values for the system colors
     LookAndFeel::Refresh();
     sLookAndFeelChanged = false;
   }
 
   // Invalidate cached '-moz-windows-accent-color-applies' media query:
-  nsCSSRuleProcessor::FreeSystemMetrics();
+  RefreshSystemMetrics();
 
   // Reset default background and foreground colors for the document since
   // they may be using system colors
   GetDocumentColorPreferences();
 
   // The system color values are computed to colors in the style data,
   // so normal style data comparison is sufficient here.
   RebuildAllStyleData(nsChangeHint(0), nsRestyleHint(0));
 }
 
 void
+nsPresContext::RefreshSystemMetrics()
+{
+  // This will force the system metrics to be generated the next time they're used
+  nsCSSRuleProcessor::FreeSystemMetrics();
+
+  // Changes to system metrics can change media queries on them, or
+  // :-moz-system-metric selectors (which requires eRestyle_Subtree).
+  // Changes in theme can change system colors (whose changes are
+  // properly reflected in computed style data), system fonts (whose
+  // changes are not), and -moz-appearance (whose changes likewise are
+  // not), so we need to reflow.
+  MediaFeatureValuesChanged(eRestyle_Subtree, NS_STYLE_HINT_REFLOW);
+}
+
+void
 nsPresContext::UIResolutionChanged()
 {
   if (!mPendingUIResolutionChanged) {
     nsCOMPtr<nsIRunnable> ev =
       NewRunnableMethod("nsPresContext::UIResolutionChangedInternal",
                         this,
                         &nsPresContext::UIResolutionChangedInternal);
     nsresult rv =
diff --git a/layout/base/nsPresContext.h b/layout/base/nsPresContext.h
--- a/layout/base/nsPresContext.h
+++ b/layout/base/nsPresContext.h
@@ -1183,16 +1183,17 @@ public:
   }
 
   nsBidi& GetBidiEngine();
 
 protected:
   friend class nsRunnableMethod<nsPresContext>;
   void ThemeChangedInternal();
   void SysColorChangedInternal();
+  void RefreshSystemMetrics();
 
   // update device context's resolution from the widget
   void UIResolutionChangedInternal();
 
   // if aScale > 0.0, use it as resolution scale factor to the device context
   // (otherwise get it from the widget)
   void UIResolutionChangedInternalScale(double aScale);
 
