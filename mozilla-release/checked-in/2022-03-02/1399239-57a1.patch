# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1498149868 -7200
# Node ID 9367b7801b706b9928e7e241e8ffb64ca871e741
# Parent  44c6610e6d476c44e625bc5fc077caff7c3e28b9
Bug 1399239 - Remove unused srcBodyStart. r=luke

MozReview-Commit-ID: 6p4OIQz7dPb

diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -322,32 +322,30 @@ struct js::AsmJSMetadata : Metadata, Asm
     //
     // srcStart refers to the offset in the ScriptSource to the beginning of
     // the asm.js module function. If the function has been created with the
     // Function constructor, this will be the first character in the function
     // source. Otherwise, it will be the opening parenthesis of the arguments
     // list.
     uint32_t                toStringStart;
     uint32_t                srcStart;
-    uint32_t                srcBodyStart;
     bool                    strict;
     ScriptSourceHolder      scriptSource;
 
     uint32_t srcEndBeforeCurly() const {
         return srcStart + srcLength;
     }
     uint32_t srcEndAfterCurly() const {
         return srcStart + srcLengthWithRightBrace;
     }
 
     explicit AsmJSMetadata(UniqueMetadataTier tier)
       : Metadata(Move(tier), ModuleKind::AsmJS),
         cacheResult(CacheResult::Miss),
         srcStart(0),
-        srcBodyStart(0),
         strict(false)
     {}
     ~AsmJSMetadata() override {}
 
     const AsmJSExport& lookupAsmJSExport(uint32_t funcIndex) const {
         // The AsmJSExportVector isn't stored in sorted order so do a linear
         // search. This is for the super-cold and already-expensive toString()
         // path and the number of exports is generally small.
@@ -1783,17 +1781,16 @@ class MOZ_STACK_CLASS ModuleValidator
             return false;
 
         asmJSMetadata_ = cx_->new_<AsmJSMetadata>(Move(tierMetadata));
         if (!asmJSMetadata_)
             return false;
 
         asmJSMetadata_->toStringStart = moduleFunctionNode_->pn_funbox->toStringStart;
         asmJSMetadata_->srcStart = moduleFunctionNode_->pn_body->pn_pos.begin;
-        asmJSMetadata_->srcBodyStart = parser_.tokenStream.currentToken().pos.end;
         asmJSMetadata_->strict = parser_.pc->sc()->strict() &&
                                  !parser_.pc->sc()->hasExplicitUseStrict();
         asmJSMetadata_->scriptSource.reset(parser_.ss);
 
         if (!globalMap_.init() || !sigMap_.init() || !importMap_.init())
             return false;
 
         if (!standardLibraryMathNames_.init() ||
@@ -8547,17 +8544,16 @@ LookupAsmJSModuleInCache(JSContext* cx, 
     MOZ_RELEASE_ASSERT(cursor == entry.memory + entry.serializedSize);
 #endif
     if (cursor != entry.memory + entry.serializedSize)
         return true;
 
     // See AsmJSMetadata comment as well as ModuleValidator::init().
     asmJSMetadata->toStringStart = parser.pc->functionBox()->toStringStart;
     asmJSMetadata->srcStart = parser.pc->functionBox()->functionNode->pn_body->pn_pos.begin;
-    asmJSMetadata->srcBodyStart = parser.tokenStream.currentToken().pos.end;
     asmJSMetadata->strict = parser.pc->sc()->strict() && !parser.pc->sc()->hasExplicitUseStrict();
     asmJSMetadata->scriptSource.reset(parser.ss);
 
     if (!parser.tokenStream.advance(asmJSMetadata->srcEndBeforeCurly()))
         return false;
 
     int64_t after = PRMJ_Now();
     int ms = (after - before) / PRMJ_USEC_PER_MSEC;
