# HG changeset patch
# User JW Wang <jwwang@mozilla.com>
# Date 1503504521 -28800
# Node ID 4c5be1a3ee2cb53598f25f249f57cf9412c3b180
# Parent  5beea3ee427bc0513ff358bcddb6827ad1cd6524
Bug 1393346. P2 - move IsTransportSeekable() from MediaResource to BaseMediaResource. r=gerald

MozReview-Commit-ID: KqpzIRH31gz

diff --git a/dom/media/BufferMediaResource.h b/dom/media/BufferMediaResource.h
--- a/dom/media/BufferMediaResource.h
+++ b/dom/media/BufferMediaResource.h
@@ -82,18 +82,16 @@ private:
   }
 
   nsresult GetCachedRanges(MediaByteRangeSet& aRanges) override
   {
     aRanges += MediaByteRange(0, int64_t(mLength));
     return NS_OK;
   }
 
-  bool IsTransportSeekable() override { return true; }
-
   size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const override
   {
     // Not owned:
     // - mBuffer
     // - mPrincipal
     size_t size = MediaResource::SizeOfExcludingThis(aMallocSizeOf);
     return size;
   }
diff --git a/dom/media/MediaDecoder.cpp b/dom/media/MediaDecoder.cpp
--- a/dom/media/MediaDecoder.cpp
+++ b/dom/media/MediaDecoder.cpp
@@ -810,17 +810,17 @@ MediaDecoder::FirstFrameLoaded(nsAutoPtr
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_DIAGNOSTIC_ASSERT(!IsShutdown());
   AbstractThread::AutoEnter context(AbstractMainThread());
 
   LOG("FirstFrameLoaded, channels=%u rate=%u hasAudio=%d hasVideo=%d "
       "mPlayState=%s transportSeekable=%d",
       aInfo->mAudio.mChannels, aInfo->mAudio.mRate, aInfo->HasAudio(),
-      aInfo->HasVideo(), PlayStateStr(), GetResource()->IsTransportSeekable());
+      aInfo->HasVideo(), PlayStateStr(), IsTransportSeekable());
 
   mInfo = aInfo.forget();
 
   Invalidate();
 
   // The element can run javascript via events
   // before reaching here, so only change the
   // state if we're still set to the original
diff --git a/dom/media/MediaResource.h b/dom/media/MediaResource.h
--- a/dom/media/MediaResource.h
+++ b/dom/media/MediaResource.h
@@ -206,20 +206,16 @@ public:
   // Reads only data which is cached in the media cache. If you try to read
   // any data which overlaps uncached data, or if aCount bytes otherwise can't
   // be read, this function will return failure. This function be called from
   // any thread, and it is the only read operation which is safe to call on
   // the main thread, since it's guaranteed to be non blocking.
   virtual nsresult ReadFromCache(char* aBuffer,
                                  int64_t aOffset,
                                  uint32_t aCount) = 0;
-  // Returns true if the resource can be seeked to unbuffered ranges, i.e.
-  // for an HTTP network stream this returns true if HTTP1.1 Byte Range
-  // requests are supported by the connection/server.
-  virtual bool IsTransportSeekable() = 0;
 
   /**
    * Fills aRanges with MediaByteRanges representing the data which is cached
    * in the media cache. Stream should be pinned during call and while
    * aRanges is being used.
    */
   virtual nsresult GetCachedRanges(MediaByteRangeSet& aRanges) = 0;
 
@@ -284,16 +280,21 @@ public:
   virtual void Suspend(bool aCloseImmediately) = 0;
 
   // Resume any downloads that have been suspended.
   virtual void Resume() = 0;
 
   // The mode is initially MODE_PLAYBACK.
   virtual void SetReadMode(MediaCacheStream::ReadMode aMode) = 0;
 
+  // Returns true if the resource can be seeked to unbuffered ranges, i.e.
+  // for an HTTP network stream this returns true if HTTP1.1 Byte Range
+  // requests are supported by the connection/server.
+  virtual bool IsTransportSeekable() = 0;
+
   /**
    * Open the stream. This creates a stream listener and returns it in
    * aStreamListener; this listener needs to be notified of incoming data.
    */
   virtual nsresult Open(nsIStreamListener** aStreamListener) = 0;
 
   // If this returns false, then we shouldn't try to clone this MediaResource
   // because its underlying resources are not suitable for reuse (e.g.
diff --git a/dom/media/gtest/MockMediaResource.h b/dom/media/gtest/MockMediaResource.h
--- a/dom/media/gtest/MockMediaResource.h
+++ b/dom/media/gtest/MockMediaResource.h
@@ -38,17 +38,16 @@ public:
                          uint32_t aCount) override
   {
     uint32_t bytesRead = 0;
     nsresult rv = ReadAt(aOffset, aBuffer, aCount, &bytesRead);
     NS_ENSURE_SUCCESS(rv, rv);
     return bytesRead == aCount ? NS_OK : NS_ERROR_FAILURE;
   }
 
-  bool IsTransportSeekable() override { return true; }
   nsresult Open();
   nsresult GetCachedRanges(MediaByteRangeSet& aRanges) override;
 
   void MockClearBufferedRanges();
   void MockAddBufferedRange(int64_t aStart, int64_t aEnd);
 
 protected:
   virtual ~MockMediaResource();
diff --git a/dom/media/hls/HLSResource.h b/dom/media/hls/HLSResource.h
--- a/dom/media/hls/HLSResource.h
+++ b/dom/media/hls/HLSResource.h
@@ -72,18 +72,16 @@ public:
   }
 
   nsresult GetCachedRanges(MediaByteRangeSet& aRanges) override
   {
     UNIMPLEMENTED();
     return NS_OK;
   }
 
-  bool IsTransportSeekable() override { return true; }
-
   java::GeckoHLSResourceWrapper::GlobalRef GetResourceWrapper() {
     return mHLSResourceWrapper;
   }
 
   void Detach() { mDecoder = nullptr; }
 
 private:
   friend class HLSResourceCallbacksSupport;
diff --git a/dom/media/mediasource/MediaSourceResource.h b/dom/media/mediasource/MediaSourceResource.h
--- a/dom/media/mediasource/MediaSourceResource.h
+++ b/dom/media/mediasource/MediaSourceResource.h
@@ -50,18 +50,16 @@ public:
 
   nsresult GetCachedRanges(MediaByteRangeSet& aRanges) override
   {
     UNIMPLEMENTED();
     aRanges += MediaByteRange(0, GetLength());
     return NS_OK;
   }
 
-  bool IsTransportSeekable() override { return true; }
-
   void SetEnded(bool aEnded)
   {
     MonitorAutoLock mon(mMonitor);
     mEnded = aEnded;
   }
 
 private:
   size_t SizeOfExcludingThis(MallocSizeOf aMallocSizeOf) const override
diff --git a/dom/media/mediasource/SourceBufferResource.h b/dom/media/mediasource/SourceBufferResource.h
--- a/dom/media/mediasource/SourceBufferResource.h
+++ b/dom/media/mediasource/SourceBufferResource.h
@@ -67,21 +67,16 @@ public:
       return aOffset;
     }
     return GetLength();
   }
   bool IsDataCachedToEndOfResource(int64_t aOffset) override { return false; }
   nsresult ReadFromCache(char* aBuffer,
                          int64_t aOffset,
                          uint32_t aCount) override;
-  bool IsTransportSeekable() override
-  {
-    UNIMPLEMENTED();
-    return true;
-  }
 
   nsresult GetCachedRanges(MediaByteRangeSet& aRanges) override
   {
     MOZ_ASSERT(OnTaskQueue());
     if (mInputBuffer.GetLength()) {
       aRanges += MediaByteRange(mInputBuffer.GetOffset(),
                                 mInputBuffer.GetLength());
     }
