# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1494626282 25200
#      Fri May 12 14:58:02 2017 -0700
# Node ID a9a24a28013bb346dc75044d02d4266842349e6c
# Parent  da5bb5e0ea57c3e863c86b15227311d4e285cbdc
Bug 1363482: Part 10 - Preload addonStartup.json off-thread during startup. r=rhelmer

MozReview-Commit-ID: 4jYpr9kXKq9

diff --git a/toolkit/mozapps/extensions/AddonManagerStartup.cpp b/toolkit/mozapps/extensions/AddonManagerStartup.cpp
--- a/toolkit/mozapps/extensions/AddonManagerStartup.cpp
+++ b/toolkit/mozapps/extensions/AddonManagerStartup.cpp
@@ -15,16 +15,17 @@
 #include "mozilla/EndianUtils.h"
 #include "mozilla/Compression.h"
 #include "mozilla/LinkedList.h"
 #include "mozilla/FileUtils.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/ResultExtensions.h"
 #include "mozilla/ScopeExit.h"
 #include "mozilla/Services.h"
+#include "mozilla/URLPreloader.h"
 #include "mozilla/Unused.h"
 #include "mozilla/ErrorResult.h"
 #include "mozilla/dom/ipc/StructuredCloneData.h"
 
 #include "nsAppDirectoryServiceDefs.h"
 #include "nsAppRunner.h"
 #include "nsContentUtils.h"
 #include "nsChromeRegistry.h"
@@ -100,38 +101,16 @@ CloneAndAppend(nsIFile* aFile, const cha
 
 static bool
 IsNormalFile(nsIFile* file)
 {
   bool result;
   return NS_SUCCEEDED(file->IsFile(&result)) && result;
 }
 
-static Result<nsCString, nsresult>
-ReadFile(nsIFile* file)
-{
-  nsCString result;
-
-  AutoFDClose fd;
-  MOZ_TRY(file->OpenNSPRFileDesc(PR_RDONLY, 0, &fd.rwget()));
-
-  auto size = PR_Seek64(fd, 0, PR_SEEK_END);
-  PR_Seek64(fd, 0, PR_SEEK_SET);
-
-  result.SetLength(size);
-
-  auto len = PR_Read(fd, result.BeginWriting(), size);
-
-  if (size != len) {
-    return Err(NS_ERROR_FAILURE);
-  }
-
-  return result;
-}
-
 static const char STRUCTURED_CLONE_MAGIC[] = "mozJSSCLz40v001";
 
 template <typename T>
 static Result<nsCString, nsresult>
 DecodeLZ4(const nsACString& lz4, const T& magicNumber)
 {
   constexpr auto HEADER_SIZE = sizeof(magicNumber) + 4;
 
@@ -191,29 +170,24 @@ EncodeLZ4(const nsACString& data, const 
 }
 
 static_assert(sizeof STRUCTURED_CLONE_MAGIC % 8 == 0,
               "Magic number should be an array of uint64_t");
 
 /**
  * Reads the contents of a LZ4-compressed file, as stored by the OS.File
  * module, and returns the decompressed contents on success.
- *
- * A nonexistent or empty file is treated as success. A corrupt or non-LZ4
- * file is treated as failure.
  */
 static Result<nsCString, nsresult>
 ReadFileLZ4(nsIFile* file)
 {
   static const char MAGIC_NUMBER[] = "mozLz40";
 
-  nsCString result;
-
   nsCString lz4;
-  MOZ_TRY_VAR(lz4, ReadFile(file));
+  MOZ_TRY_VAR(lz4, URLPreloader::ReadFile(file));
 
   if (lz4.IsEmpty()) {
     return lz4;
   }
 
   return DecodeLZ4(lz4, MAGIC_NUMBER);
 }
 
@@ -588,17 +562,22 @@ AddonManagerStartup::AddInstallLocation(
 nsresult
 AddonManagerStartup::ReadStartupData(JSContext* cx, JS::MutableHandleValue locations)
 {
   locations.set(JS::UndefinedValue());
 
   nsCOMPtr<nsIFile> file = CloneAndAppend(ProfileDir(), "addonStartup.json.lz4");
 
   nsCString data;
-  MOZ_TRY_VAR(data, ReadFileLZ4(file));
+  auto res = ReadFileLZ4(file);
+  if (res.isOk()) {
+    data = res.unwrap();
+  } else if (res.unwrapErr() != NS_ERROR_FILE_NOT_FOUND) {
+    return res.unwrapErr();
+  }
 
   if (data.IsEmpty() || !ParseJSON(cx, data, locations)) {
     return NS_OK;
   }
 
   if (!locations.isObject()) {
     return NS_ERROR_UNEXPECTED;
   }
