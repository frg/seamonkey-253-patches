# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1502148943 25200
#      Mon Aug 07 16:35:43 2017 -0700
# Node ID 40aafceb5e48a7139e934bf698c32c491b0936a1
# Parent  13cbf982fb50a16769a069214a896326eb26412e
Bug 1388191 - Add way to test evaluation with envChain in the shell. r=jorendorff

This is used by Gecko, but it isn't directly testable in the shell.

The new test has to be disabled for jstestbrowser because the special
option is shell-only.

MozReview-Commit-ID: 6gckRSkD4fQ

diff --git a/js/src/shell/js.cpp b/js/src/shell/js.cpp
--- a/js/src/shell/js.cpp
+++ b/js/src/shell/js.cpp
@@ -1521,16 +1521,17 @@ Evaluate(JSContext* cx, unsigned argc, V
     RootedString displayURL(cx);
     RootedString sourceMapURL(cx);
     RootedObject global(cx, nullptr);
     bool catchTermination = false;
     bool loadBytecode = false;
     bool saveBytecode = false;
     bool saveIncrementalBytecode = false;
     bool assertEqBytecode = false;
+    JS::AutoObjectVector envChain(cx);
     RootedObject callerGlobal(cx, cx->global());
 
     options.setIntroductionType("js shell evaluate")
            .setFileAndLine("@evaluate", 1);
 
     global = JS_GetGlobalForObject(cx, &args.callee());
     MOZ_ASSERT(global);
 
@@ -1627,16 +1628,36 @@ Evaluate(JSContext* cx, unsigned argc, V
         if (!v.isUndefined())
             saveIncrementalBytecode = ToBoolean(v);
 
         if (!JS_GetProperty(cx, opts, "assertEqBytecode", &v))
             return false;
         if (!v.isUndefined())
             assertEqBytecode = ToBoolean(v);
 
+        if (!JS_GetProperty(cx, opts, "envChainObject", &v))
+            return false;
+        if (!v.isUndefined()) {
+            if (loadBytecode) {
+                JS_ReportErrorASCII(cx, "Can't use both loadBytecode and envChainObject");
+                return false;
+            }
+
+            if (v.isObject()) {
+                if (!envChain.append(&v.toObject())) {
+                    JS_ReportOutOfMemory(cx);
+                    return false;
+                }
+            } else {
+                JS_ReportErrorNumberASCII(cx, GetErrorMessage, nullptr, JSMSG_UNEXPECTED_TYPE,
+                                          "\"envChainObject\" passed to evaluate()", "not an object");
+                return false;
+            }
+        }
+
         // We cannot load or save the bytecode if we have no object where the
         // bytecode cache is stored.
         if (loadBytecode || saveBytecode || saveIncrementalBytecode) {
             if (!cacheEntry) {
                 JS_ReportErrorNumberASCII(cx, my_GetErrorMessage, nullptr, JSSMSG_INVALID_ARGS,
                                           "evaluate");
                 return false;
             }
@@ -1684,17 +1705,21 @@ Evaluate(JSContext* cx, unsigned argc, V
             }
 
             if (loadBytecode) {
                 JS::TranscodeResult rv = JS::DecodeScript(cx, loadBuffer, &script);
                 if (!ConvertTranscodeResultToJSException(cx, rv))
                     return false;
             } else {
                 mozilla::Range<const char16_t> chars = codeChars.twoByteRange();
-                (void) JS::Compile(cx, options, chars.begin().get(), chars.length(), &script);
+                if (envChain.length() == 0) {
+                    (void) JS::Compile(cx, options, chars.begin().get(), chars.length(), &script);
+                } else {
+                    (void) JS::CompileForNonSyntacticScope(cx, options, chars.begin().get(), chars.length(), &script);
+                }
             }
 
             if (!script)
                 return false;
         }
 
         if (displayURL && !script->scriptSource()->hasDisplayURL()) {
             JSFlatString* flat = displayURL->ensureFlat(cx);
@@ -1726,17 +1751,17 @@ Evaluate(JSContext* cx, unsigned argc, V
         // If we want to save the bytecode incrementally, then we should
         // register ahead the fact that every JSFunction which is being
         // delazified should be encoded at the end of the delazification.
         if (saveIncrementalBytecode) {
             if (!StartIncrementalEncoding(cx, script))
                 return false;
         }
 
-        if (!JS_ExecuteScript(cx, script, args.rval())) {
+        if (!JS_ExecuteScript(cx, envChain, script, args.rval())) {
             if (catchTermination && !JS_IsExceptionPending(cx)) {
                 JSAutoCompartment ac1(cx, callerGlobal);
                 JSString* str = JS_NewStringCopyZ(cx, "terminated");
                 if (!str)
                     return false;
                 args.rval().setString(str);
                 return true;
             }
@@ -6258,16 +6283,19 @@ static const JSFunctionSpecWithHelp shel
 "         the bytecode would be loaded and decoded from the cache entry instead\n"
 "         of being parsed, then it would be executed as usual.\n"
 "      saveBytecode: if true, and if the source is a CacheEntryObject,\n"
 "         the bytecode would be encoded and saved into the cache entry after\n"
 "         the script execution.\n"
 "      assertEqBytecode: if true, and if both loadBytecode and saveBytecode are \n"
 "         true, then the loaded bytecode and the encoded bytecode are compared.\n"
 "         and an assertion is raised if they differ.\n"
+"      envChainObject: object to put on the scope chain, with its fields added\n"
+"         as var bindings, akin to how elements are added to the environment in\n"
+"         event handlers in Gecko.\n"
 ),
 
     JS_FN_HELP("run", Run, 1, 0,
 "run('foo.js')",
 "  Run the file named by the first argument, returning the number of\n"
 "  of milliseconds spent compiling and executing it."),
 
     JS_FN_HELP("readline", ReadLine, 0, 0,
diff --git a/js/src/tests/js1_8_5/extensions/non_syntactic.js b/js/src/tests/js1_8_5/extensions/non_syntactic.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/js1_8_5/extensions/non_syntactic.js
@@ -0,0 +1,36 @@
+// |reftest| skip-if(!xulRuntime.shell) -- needs evaluate()
+// Any copyright is dedicated to the Public Domain.
+// http://creativecommons.org/licenses/publicdomain/
+
+// Check references to someVar, both as a variable and on |this|, in
+// various evaluation contexts.
+var someVar = 1;
+
+// Top level.
+assertEq(someVar, 1);
+assertEq(this.someVar, 1);
+
+// Inside evaluate.
+evaluate("assertEq(someVar, 1);");
+evaluate("assertEq(this.someVar, 1);");
+
+// With an object on the scope, no shadowing.
+var someObject = { someOtherField: 2 };
+var evalOpt = { envChainObject: someObject };
+evaluate("assertEq(someVar, 1);", evalOpt);
+evaluate("assertEq(this.someVar, undefined);", evalOpt);
+
+// With an object on the scope, shadowing global.
+someObject = { someVar: 2 };
+evalOpt = { envChainObject: someObject };
+var alsoSomeObject = someObject;
+evaluate("assertEq(someVar, 2);", evalOpt);
+evaluate("assertEq(this.someVar, 2);", evalOpt);
+evaluate("assertEq(this, alsoSomeObject);", evalOpt);
+
+// With an object on the scope, inside a function.
+evaluate("(function() { assertEq(someVar, 2);})()", evalOpt);
+evaluate("(function() { assertEq(this !== alsoSomeObject, true);})()", evalOpt);
+evaluate("(function() { assertEq(this.someVar, 1);})()", evalOpt);
+
+reportCompare(true, true);
