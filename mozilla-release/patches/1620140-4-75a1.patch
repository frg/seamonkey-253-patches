# HG changeset patch
# User Mike Shal <mshal@mozilla.com>
# Date 1583510575 0
# Node ID 07adb480d013c1c4b4a042c6015b12546d34385b
# Parent  51bb25d374f0f7a2ee96adc3931cb660bb9bfac1
Bug 1620140 - Convert gen_scalar_data.py to py3; r=firefox-build-system-reviewers,rstewart

Differential Revision: https://phabricator.services.mozilla.com/D65460

diff --git a/toolkit/components/telemetry/build_scripts/mozparsers/parse_scalars.py b/toolkit/components/telemetry/build_scripts/mozparsers/parse_scalars.py
--- a/toolkit/components/telemetry/build_scripts/mozparsers/parse_scalars.py
+++ b/toolkit/components/telemetry/build_scripts/mozparsers/parse_scalars.py
@@ -1,17 +1,19 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+import io
 import re
+import six
 import yaml
-import shared_telemetry_utils as utils
+from . import shared_telemetry_utils as utils
 
-from shared_telemetry_utils import ParserError
+from .shared_telemetry_utils import ParserError
 
 # The map of containing the allowed scalar types and their mapping to
 # nsITelemetry::SCALAR_* type constants.
 
 BASE_DOC_URL = 'https://firefox-source-docs.mozilla.org/toolkit/components/' + \
                'telemetry/telemetry/collection/scalars.html'
 
 SCALAR_TYPES_MAP = {
@@ -86,34 +88,34 @@ class ScalarType:
         """
 
         if not self._strict_type_checks:
             return
 
         # The required and optional fields in a scalar type definition.
         REQUIRED_FIELDS = {
             'bug_numbers': list,  # This contains ints. See LIST_FIELDS_CONTENT.
-            'description': basestring,
-            'expires': basestring,
-            'kind': basestring,
+            'description': six.string_types,
+            'expires': six.string_types,
+            'kind': six.string_types,
             'notification_emails': list,  # This contains strings. See LIST_FIELDS_CONTENT.
             'record_in_processes': list,
         }
 
         OPTIONAL_FIELDS = {
-            'cpp_guard': basestring,
-            'release_channel_collection': basestring,
+            'cpp_guard': six.string_types,
+            'release_channel_collection': six.string_types,
             'keyed': bool,
         }
 
         # The types for the data within the fields that hold lists.
         LIST_FIELDS_CONTENT = {
             'bug_numbers': int,
-            'notification_emails': basestring,
-            'record_in_processes': basestring,
+            'notification_emails': six.string_types,
+            'record_in_processes': six.string_types,
         }
 
         # Concatenate the required and optional field definitions.
         ALL_FIELDS = REQUIRED_FIELDS.copy()
         ALL_FIELDS.update(OPTIONAL_FIELDS)
 
         # Checks that all the required fields are available.
         missing_fields = [f for f in REQUIRED_FIELDS.keys() if f not in definition]
@@ -287,35 +289,35 @@ def load_scalars(filename, strict_type_c
 
     :param filename: the YAML file containing the scalars definition.
     :raises ParserError: if the scalar file cannot be opened or parsed.
     """
 
     # Parse the scalar definitions from the YAML file.
     scalars = None
     try:
-        with open(filename, 'r') as f:
+        with io.open(filename, 'r', encoding='utf-8') as f:
             scalars = yaml.safe_load(f)
     except IOError as e:
         raise ParserError('Error opening ' + filename + ': ' + e.message)
     except ValueError as e:
         raise ParserError('Error parsing scalars in {}: {}'
                           '.\nSee: {}'.format(filename, e.message, BASE_DOC_URL))
 
     scalar_list = []
 
     # Scalars are defined in a fixed two-level hierarchy within the definition file.
     # The first level contains the group name, while the second level contains the
     # probe name (e.g. "group.name: probe: ...").
-    for group_name in scalars:
+    for group_name in sorted(scalars):
         group = scalars[group_name]
 
         # Make sure that the group has at least one probe in it.
         if not group or len(group) == 0:
             raise ParserError('Category "{}" must have at least one probe in it' +
                               '.\nSee: {}'.format(group_name, BASE_DOC_URL))
 
-        for probe_name in group:
+        for probe_name in sorted(group):
             # We found a scalar type. Go ahead and parse it.
             scalar_info = group[probe_name]
             scalar_list.append(ScalarType(group_name, probe_name, scalar_info, strict_type_checks))
 
     return scalar_list
