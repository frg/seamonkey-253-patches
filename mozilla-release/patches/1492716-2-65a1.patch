# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1539689292 -32400
#      Tue Oct 16 20:28:12 2018 +0900
# Node ID e22dc234a52fe16f8ce2ae86b1b63b86211c1572
# Parent  220ff89833e8fe5bc65d6f85db1fff0cc4e3f8dd
Bug 1492716 - Part 2: Add formatting rule to help text for --{enable,disable,with,without}. r=glandium

Differential Revision: https://phabricator.services.mozilla.com/D8834

diff --git a/build/moz.configure/warnings.configure b/build/moz.configure/warnings.configure
--- a/build/moz.configure/warnings.configure
+++ b/build/moz.configure/warnings.configure
@@ -1,17 +1,17 @@
 # -*- Mode: python; c-basic-offset: 4; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 js_option('--enable-warnings-as-errors', env='MOZ_ENABLE_WARNINGS_AS_ERRORS',
           default=depends('MOZ_AUTOMATION', '--help')(lambda x, _: bool(x)),
-          help='Enable treating warnings as errors')
+          help='{Enable|Disable} treating warnings as errors')
 
 add_old_configure_assignment(
     'MOZ_ENABLE_WARNINGS_AS_ERRORS',
     depends('--enable-warnings-as-errors')(lambda x: bool(x)))
 
 
 # GCC/Clang warnings:
 # https://gcc.gnu.org/onlinedocs/gcc-4.7.2/gcc/Warning-Options.html
diff --git a/js/moz.configure b/js/moz.configure
--- a/js/moz.configure
+++ b/js/moz.configure
@@ -24,17 +24,17 @@ include('../build/moz.configure/rust.con
 @depends('JS_STANDALONE')
 def js_standalone(value):
     if value:
         return True
 set_config('JS_STANDALONE', js_standalone)
 set_define('JS_STANDALONE', js_standalone)
 add_old_configure_assignment('JS_STANDALONE', js_standalone)
 js_option('--disable-js-shell', default=building_js,
-          help='Do not build the JS shell')
+          help='{Build|Do not build} the JS shell')
 
 @depends('--disable-js-shell')
 def js_disable_shell(value):
     if not value:
         return True
 
 set_config('JS_DISABLE_SHELL', js_disable_shell)
 
@@ -42,20 +42,20 @@ set_define('JS_64BIT', depends(target)(l
 
 set_define('JS_PUNBOX64', depends(target)(lambda t: t.bitness == 64 or None))
 set_define('JS_NUNBOX32', depends(target)(lambda t: t.bitness == 32 or None))
 
 
 # SpiderMonkey as a shared library, and how its symbols are exported
 # ==================================================================
 js_option('--disable-shared-js', default=building_js,
-          help='Do not create a shared library')
+          help='{Create|Do not create} a shared library')
 
 js_option('--disable-export-js', default=building_js,
-          help='Do not mark JS symbols as DLL exported/visible')
+          help='{Mark|Do not mark} JS symbols as DLL exported/visible')
 
 @depends('--disable-shared-js', '--disable-export-js')
 def shared_js(shared_js, export_js):
     if shared_js:
         if not export_js:
             die('Must export JS symbols when building a shared library.')
         return True
 
@@ -110,17 +110,17 @@ def disable_export_js(value):
 # =======================================================
 @depends(target)
 def ion_default(target):
     if target.cpu in ('x86', 'x86_64', 'arm', 'aarch64', 'mips32', 'mips64'):
         return True
 
 js_option('--enable-ion',
           default=ion_default,
-          help='Enable use of the IonMonkey JIT')
+          help='{Enable|Disable} use of the IonMonkey JIT')
 
 set_config('ENABLE_ION', depends_if('--enable-ion')(lambda x: True))
 
 # JIT code simulator for cross compiles
 # =======================================================
 js_option('--enable-simulator', choices=('arm', 'arm64', 'mips32', 'mips64'),
           nargs=1,
           help='Enable a JIT code simulator for the specified architecture')
@@ -242,18 +242,18 @@ def callgrind(value):
 set_define('MOZ_CALLGRIND', callgrind)
 imply_option('--enable-profiling', callgrind)
 
 @depends(milestone, '--help')
 def enable_profiling(milestone, help):
     return milestone.is_nightly
 
 js_option('--enable-profiling', env='MOZ_PROFILING', default=enable_profiling,
-          help='Set compile flags necessary for using sampling profilers '
-               '(e.g. shark, perf)')
+          help='{Set|Do not set} compile flags necessary for using sampling '
+                'profilers (e.g. shark, perf)')
 
 @depends('--enable-profiling')
 def profiling(value):
     if value:
         return True
 
 add_old_configure_assignment('MOZ_PROFILING', profiling)
 
@@ -290,17 +290,17 @@ def gc_trace(value):
     if value:
         return True
 
 set_define('JS_GC_TRACE', gc_trace)
 
 
 js_option('--enable-gczeal',
           default=depends(when=moz_debug)(lambda: True),
-          help='Enable zealous GCing')
+          help='{Enable|Disable} zealous GCing')
 
 set_define('JS_GC_ZEAL',
            depends_if('--enable-gczeal')(lambda _: True))
 
 
 # Use a smaller chunk size for GC chunks
 # ========================================================
 # Use large (1MB) chunks by default.  This option can be used to give
@@ -311,17 +311,17 @@ js_option('--enable-small-chunk-size',
 set_define('JS_GC_SMALL_CHUNK_SIZE',
            depends(when='--enable-small-chunk-size')(lambda: True))
 
 
 # Trace logging.
 # =======================================================
 js_option('--enable-trace-logging',
           default=depends(when=moz_debug)(lambda: True),
-          help='Enable trace logging')
+          help='{Enable|Disable} trace logging')
 
 set_config('ENABLE_TRACE_LOGGING',
            depends_if('--enable-trace-logging')(lambda x: True))
 set_define('JS_TRACE_LOGGING',
            depends_if('--enable-trace-logging')(lambda x: True))
 
 
 # Enable breakpoint for artificial OOMs
@@ -341,17 +341,18 @@ def ion_perf(value):
     if value:
         return True
 
 set_define('JS_ION_PERF', ion_perf)
 
 
 js_option('--enable-jitspew',
           default=depends(when=moz_debug)(lambda: True),
-          help='Enable the Jit spew and IONFLAGS environment variable.')
+          help='{Enable|Disable} the Jit spew and IONFLAGS environment '
+               'variable')
 
 set_define('JS_JITSPEW',
            depends_if('--enable-jitspew')(lambda _: True))
 set_config('JS_JITSPEW',
            depends_if('--enable-jitspew')(lambda _: True))
 
 
 js_option('--enable-more-deterministic', env='JS_MORE_DETERMINISTIC',
@@ -366,18 +367,19 @@ set_define('JS_MORE_DETERMINISTIC', more
 
 
 # CTypes
 # =======================================================
 @depends(building_js, '--help')
 def ctypes_default(building_js, _):
     return not building_js
 
-js_option('--enable-ctypes', help='Enable js-ctypes',
-          default=ctypes_default)
+js_option('--enable-ctypes',
+          default=ctypes_default,
+          help='{Enable|Disable} js-ctypes')
 
 build_ctypes = depends_if('--enable-ctypes')(lambda _: True)
 
 set_config('BUILD_CTYPES', build_ctypes)
 set_define('BUILD_CTYPES', build_ctypes)
 add_old_configure_assignment('BUILD_CTYPES', build_ctypes)
 
 @depends(build_ctypes, building_js)
diff --git a/js/moz.configure.1492716-2.later b/js/moz.configure.1492716-2.later
new file mode 100644
--- /dev/null
+++ b/js/moz.configure.1492716-2.later
@@ -0,0 +1,34 @@
+--- moz.configure
++++ moz.configure
+@@ -357,30 +357,31 @@ def ion_perf(value):
+ 
+ # When enabled, masm will generate assumeUnreachable calls that act as
+ # assertions in the generated code. This option is worth disabling when you
+ # have to track mutated values through the generated code, to avoid constantly
+ # dumping registers on and off the stack.
+ js_option('--enable-masm-verbose',
+           default=depends(when=moz_debug)(lambda: True),
+-          help='Enable MacroAssembler verbosity of generated code.')
++          help='{Enable|Disable} MacroAssembler verbosity of generated code.')
+ set_define('JS_MASM_VERBOSE',
+            depends_if('--enable-masm-verbose')(lambda _: True))
+ set_config('JS_MASM_VERBOSE',
+            depends_if('--enable-masm-verbose')(lambda _: True))
+ 
+ 
+ js_option('--enable-more-deterministic', env='JS_MORE_DETERMINISTIC',
+           help='Enable changes that make the shell more deterministic')
+@@ -485,12 +487,12 @@ set_define('ENABLE_PIPELINE_OPERATOR', e
+ @depends(milestone.is_nightly)
+ def default_cranelift(is_nightly):
+     # return is_nightly
+     # Lets test it first
+     return False
+ 
+ js_option('--enable-cranelift',
+           default=default_cranelift,
+-          help='Enable Cranelift code generator for wasm')
++          help='{Enable|Disable} Cranelift code generator for wasm')
+ 
+ set_config('ENABLE_WASM_CRANELIFT', depends_if('--enable-cranelift')(lambda x: True))
+ set_define('ENABLE_WASM_CRANELIFT', depends_if('--enable-cranelift')(lambda x: True))
diff --git a/moz.configure b/moz.configure
--- a/moz.configure
+++ b/moz.configure
@@ -109,17 +109,18 @@ def moz_debug(debug):
 set_config('MOZ_DEBUG', moz_debug)
 set_define('MOZ_DEBUG', moz_debug)
 # Override any value MOZ_DEBUG may have from the environment when passing it
 # down to old-configure.
 add_old_configure_assignment('MOZ_DEBUG',
                              depends('--enable-debug')(lambda x: bool(x)))
 
 js_option('--enable-rust-debug',
-          help='Build Rust code with debug assertions turned on.')
+          help='{Build|Do not build} Rust code with debug assertions turned '
+               'on.')
 
 @depends('--enable-rust-debug', '--enable-debug')
 def debug_rust(value, debug):
     if value.origin == 'default':
         return bool(debug) or None
     elif bool(value):
         return True
 
diff --git a/python/mozbuild/mozbuild/configure/help.py b/python/mozbuild/mozbuild/configure/help.py
--- a/python/mozbuild/mozbuild/configure/help.py
+++ b/python/mozbuild/mozbuild/configure/help.py
@@ -1,15 +1,16 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import os
+import re
 from mozbuild.configure.options import Option
 
 
 class HelpFormatter(object):
     def __init__(self, argv0):
         self.intro = ['Usage: %s [options]' % os.path.basename(argv0)]
         self.options = ['Options: [defaults in brackets after descriptions]']
         self.env = ['Environment variables:']
@@ -21,25 +22,49 @@ class HelpFormatter(object):
             # Don't display help if our option can only be implied.
             return
 
         # TODO: improve formatting
         target = self.options if option.name else self.env
         opt = option.option
         if option.choices:
             opt += '={%s}' % ','.join(option.choices)
-        help = option.help or ''
+        help = self.format_help(option)
         if len(option.default):
             if help:
                 help += ' '
             help += '[%s]' % ','.join(option.default)
 
         if len(opt) > 24 or not help:
             target.append('  %s' % opt)
             if help:
                 target.append('%s%s' % (' ' * 28, help))
         else:
             target.append('  %-24s  %s' % (opt, help))
 
+    RE_FORMAT = re.compile(r'{([^|}]+)\|([^|}]+)}')
+
+    # Return formatted help text for --{enable,disable,with,without}-* options.
+    #
+    # Format is the following syntax:
+    #   {String for --enable or --with|String for --disable or --without}
+    #
+    # For example, '{Enable|Disable} optimizations' will be formatted to
+    # 'Enable optimizations' if the options's prefix is 'enable' or 'with',
+    # and formatted to 'Disable optimizations' if the options's prefix is
+    # 'disable' or 'without'.
+    def format_help(self, option):
+        if not option.help:
+            return ''
+
+        if option.prefix in ('enable', 'with'):
+            replacement = r'\1'
+        elif option.prefix in ('disable', 'without'):
+            replacement = r'\2'
+        else:
+            return option.help
+
+        return self.RE_FORMAT.sub(replacement, option.help)
+
     def usage(self, out):
         print('\n\n'.join('\n'.join(t)
                           for t in (self.intro, self.options, self.env)),
               file=out)
diff --git a/toolkit/moz.configure b/toolkit/moz.configure
--- a/toolkit/moz.configure
+++ b/toolkit/moz.configure
@@ -77,17 +77,17 @@ set_define('MOZ_JACK', depends_if(jack)(
 
 # PulseAudio cubeb backend
 # ==============================================================
 @depends(target)
 def pulseaudio_default(target):
     return target.os not in ('WINNT', 'OSX', 'Android', 'OpenBSD')
 
 option('--enable-pulseaudio', env='MOZ_PULSEAUDIO', default=pulseaudio_default,
-       help='Enable PulseAudio audio backend.')
+       help='{Enable|Disable} PulseAudio audio backend.')
 
 pulseaudio = pkg_check_modules('MOZ_PULSEAUDIO', 'libpulse', when='--enable-pulseaudio')
 
 set_config('MOZ_PULSEAUDIO', depends_if(pulseaudio)(lambda _: True))
 set_define('MOZ_PULSEAUDIO', depends_if(pulseaudio)(lambda _: True))
 
 # Javascript engine
 # ==============================================================
@@ -758,17 +758,17 @@ def webspeech(value, _):
 
 set_config('MOZ_WEBSPEECH', webspeech)
 set_define('MOZ_WEBSPEECH', webspeech)
 add_old_configure_assignment('MOZ_WEBSPEECH', webspeech)
 
 # Speech API test backend
 # ==============================================================
 option('--enable-webspeechtestbackend', default=webspeech,
-       help='Enable support for HTML Speech API Test Backend')
+       help='{Enable|Disable} support for HTML Speech API Test Backend')
 
 @depends_if('--enable-webspeechtestbackend')
 def webspeech_test_backend(value):
     return True
 
 set_config('MOZ_WEBSPEECH_TEST_BACKEND', webspeech_test_backend)
 set_define('MOZ_WEBSPEECH_TEST_BACKEND', webspeech_test_backend)
 
@@ -1005,17 +1005,17 @@ def webrtc_default(target):
         target.cpu.startswith('ppc')):
         cpu_match = True
 
     if os_match and cpu_match:
         return True
     return False
 
 option('--disable-webrtc', default=webrtc_default,
-       help='Disable support for WebRTC')
+       help='{Enable|Disable} support for WebRTC')
 
 @depends('--disable-webrtc')
 def webrtc(enabled):
     if enabled:
         return True
 
 set_config('MOZ_WEBRTC', webrtc)
 set_define('MOZ_WEBRTC', webrtc)
@@ -1046,18 +1046,19 @@ set_define('MOZ_WEBRTC_HARDWARE_AEC_NS',
 
 @depends(target, webrtc)
 def raw_media_default(target, webrtc):
     if target.os == 'Android':
         return True
     if webrtc:
         return True
 
-option('--enable-raw', help='Enable support for RAW media',
-       default=raw_media_default)
+option('--enable-raw',
+       default=raw_media_default,
+       help='{Enable|Disable} support for RAW media')
 
 set_config('MOZ_RAW', depends_if('--enable-raw')(lambda _: True))
 set_define('MOZ_RAW', depends_if('--enable-raw')(lambda _: True))
 
 # Elfhack
 # ==============================================================
 with only_when('--enable-compile-environment'):
     @depends(host, target)
@@ -1066,17 +1067,17 @@ with only_when('--enable-compile-environ
                target.cpu in ('arm', 'x86', 'x86_64')
 
     @depends('--enable-release')
     def default_elfhack(release):
         return bool(release)
 
     with only_when(has_elfhack):
         option('--disable-elf-hack', default=default_elfhack,
-               help='Disable elf hacks')
+               help='{Enable|Disable} elf hacks')
 
         set_config('USE_ELF_HACK',
                    depends_if('--enable-elf-hack')(lambda _: True))
 
 
 @depends(check_build_environment)
 def idl_roots(build_env):
     return namespace(ipdl_root=os.path.join(build_env.topobjdir, 'ipc', 'ipdl'),
@@ -1175,18 +1176,19 @@ def unsigned_addon_scopes(scopes):
     )
 
 set_config('MOZ_UNSIGNED_APP_SCOPE', unsigned_addon_scopes.app)
 set_config('MOZ_UNSIGNED_SYSTEM_SCOPE', unsigned_addon_scopes.system)
 
 # Maintenance service (Windows only)
 # ==============================================================
 
-option('--enable-maintenance-service', help='Enable building of maintenance service',
-       when=target_is_windows, default=target_is_windows)
+option('--enable-maintenance-service',
+       when=target_is_windows, default=target_is_windows,
+       help='{Enable|Disable} building of maintenance service')
 
 set_define('MOZ_MAINTENANCE_SERVICE',
            depends_if('--enable-maintenance-service',
                       when=target_is_windows)(lambda _: True))
 set_config('MOZ_MAINTENANCE_SERVICE',
            depends_if('--enable-maintenance-service',
                       when=target_is_windows)(lambda _: True))
 
@@ -1198,17 +1200,17 @@ def bundled_fonts_default(target):
     return target.os == 'WINNT' or target.kernel == 'Linux'
 
 @depends(build_project)
 def allow_bundled_fonts(project):
     return project == 'browser'
 
 option('--enable-bundled-fonts', default=bundled_fonts_default,
        when=allow_bundled_fonts,
-       help='Enable support for bundled fonts on desktop platforms')
+       help='{Enable|Disable} support for bundled fonts on desktop platforms')
 
 set_define('MOZ_BUNDLED_FONTS',
            depends_if('--enable-bundled-fonts', when=allow_bundled_fonts)(lambda _: True))
 
 # Verify MAR signatures
 # ==============================================================
 
 option('--enable-verify-mar', help='Enable verifying MAR signatures')
@@ -1229,18 +1231,19 @@ set_config('MOZ_TASK_TRACER', depends_if
 # Reflow counting
 # ==============================================================
 
 @depends(moz_debug, '--help')
 def reflow_perf(debug, _):
     if debug:
         return True
 
-option('--enable-reflow-perf', help='Enable reflow performance tracing',
-       default=reflow_perf)
+option('--enable-reflow-perf',
+       default=reflow_perf,
+       help='{Enable|Disable} reflow performance tracing')
 
 # The difference in conditions here comes from the initial implementation
 # in old-configure, which was unexplained there as well.
 set_define('MOZ_REFLOW_PERF', depends_if('--enable-reflow-perf')(lambda _: True))
 set_define('MOZ_REFLOW_PERF_DSP', reflow_perf)
 
 # Alternative Crashreporter setting
 option("--with-crashreporter-url", env="MOZ_CRASHREPORTER_URL",
diff --git a/toolkit/moz.configure.1492716-2.later b/toolkit/moz.configure.1492716-2.later
new file mode 100644
--- /dev/null
+++ b/toolkit/moz.configure.1492716-2.later
@@ -0,0 +1,40 @@
+--- moz.configure
++++ moz.configure
+@@ -922,17 +922,17 @@ def geckodriver_default(enable_tests, ta
+     if target.os == 'WINNT' and target.cpu == 'aarch64':
+         return False
+     if hazard or target.os == 'Android' or (asan and cross_compile):
+         return False
+     return True
+ 
+ option('--enable-geckodriver', default=geckodriver_default,
+        when='--enable-compile-environment',
+-       help='Build geckodriver')
++       help='{Build|Do not build} geckodriver')
+ 
+ @depends('--enable-geckodriver', when='--enable-compile-environment')
+ def geckodriver(enabled):
+     if enabled:
+         return True
+ 
+ set_config('ENABLE_GECKODRIVER', geckodriver)
+ 
+@@ -1156,17 +1157,17 @@ set_config('MOZ_UNSIGNED_SYSTEM_SCOPE', 
+ # Launcher process (Windows only)
+ # ==============================================================
+ 
+ @depends(target, milestone)
+ def launcher_process_default(target, milestone):
+     return target.os == 'WINNT' and milestone.is_nightly
+ 
+ option('--enable-launcher-process', default=launcher_process_default,
+-       help='Enable launcher process by default')
++       help='{Enable|Disable} launcher process by default')
+ 
+ @depends('--enable-launcher-process', target)
+ def launcher(value, target):
+     enabled = bool(value)
+     if enabled and target.os != 'WINNT':
+         die('Cannot enable launcher process on %s', target.os)
+     if enabled:
+         return True
diff --git a/toolkit/nss.configure b/toolkit/nss.configure
--- a/toolkit/nss.configure
+++ b/toolkit/nss.configure
@@ -6,11 +6,12 @@
 
 
 # DBM support in NSS
 # ==============================================================
 @depends(build_project, '--help')
 def dbm_default(build_project, _):
     return build_project != 'mobile/android'
 
-option('--enable-dbm', default=dbm_default, help='Enable building DBM')
+option('--enable-dbm', default=dbm_default,
+       help='{Enable|Disable} building DBM')
 
 set_config('NSS_DISABLE_DBM', depends('--enable-dbm')(lambda x: not x))
