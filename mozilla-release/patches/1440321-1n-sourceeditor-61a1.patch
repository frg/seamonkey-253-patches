# HG changeset patch
# User Alexandre Poirot <poirot.alex@gmail.com>
# Date 1520762700 -7200
# Node ID 8e5e21375673e0be523fb43205ba07dfba937617
# Parent  7bff5e08e3f45ecfce4fab700ab9c8380ccbfdd8
Bug 1440321 - Convert Task.jsm to async/await in devtools/client - part 1n. r=jryans

MozReview-Commit-ID: HaGOC5cn3JD

diff --git a/devtools/client/sourceeditor/test/browser_editor_autocomplete_events.js b/devtools/client/sourceeditor/test/browser_editor_autocomplete_events.js
--- a/devtools/client/sourceeditor/test/browser_editor_autocomplete_events.js
+++ b/devtools/client/sourceeditor/test/browser_editor_autocomplete_events.js
@@ -3,125 +3,125 @@
    http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
 const {InspectorFront} = require("devtools/shared/fronts/inspector");
 const TEST_URI = "data:text/html;charset=UTF-8,<html><body><bar></bar>" +
                  "<div id='baz'></div><body></html>";
 
-add_task(function* () {
-  yield addTab(TEST_URI);
-  yield runTests();
+add_task(async function() {
+  await addTab(TEST_URI);
+  await runTests();
 });
 
-function* runTests() {
+async function runTests() {
   let target = TargetFactory.forTab(gBrowser.selectedTab);
-  yield target.makeRemote();
+  await target.makeRemote();
   let inspector = InspectorFront(target.client, target.form);
-  let walker = yield inspector.getWalker();
-  let {ed, win, edWin} = yield setup(null, {
+  let walker = await inspector.getWalker();
+  let {ed, win, edWin} = await setup(null, {
     autocomplete: true,
     mode: Editor.modes.css,
     autocompleteOpts: {walker: walker, cssProperties: getClientCssProperties()}
   });
-  yield testMouse(ed, edWin);
-  yield testKeyboard(ed, edWin);
-  yield testKeyboardCycle(ed, edWin);
-  yield testKeyboardCycleForPrefixedString(ed, edWin);
-  yield testKeyboardCSSComma(ed, edWin);
+  await testMouse(ed, edWin);
+  await testKeyboard(ed, edWin);
+  await testKeyboardCycle(ed, edWin);
+  await testKeyboardCycleForPrefixedString(ed, edWin);
+  await testKeyboardCSSComma(ed, edWin);
   teardown(ed, win);
 }
 
-function* testKeyboard(ed, win) {
+async function testKeyboard(ed, win) {
   ed.focus();
   ed.setText("b");
   ed.setCursor({line: 1, ch: 1});
 
   let popupOpened = ed.getAutocompletionPopup().once("popup-opened");
 
   let autocompleteKey =
     Editor.keyFor("autocompletion", { noaccel: true }).toUpperCase();
   EventUtils.synthesizeKey("VK_" + autocompleteKey, { ctrlKey: true }, win);
 
   info("Waiting for popup to be opened");
-  yield popupOpened;
+  await popupOpened;
 
   EventUtils.synthesizeKey("VK_RETURN", { }, win);
   is(ed.getText(), "bar", "Editor text has been updated");
 }
 
-function* testKeyboardCycle(ed, win) {
+async function testKeyboardCycle(ed, win) {
   ed.focus();
   ed.setText("b");
   ed.setCursor({line: 1, ch: 1});
 
   let popupOpened = ed.getAutocompletionPopup().once("popup-opened");
 
   let autocompleteKey =
     Editor.keyFor("autocompletion", { noaccel: true }).toUpperCase();
   EventUtils.synthesizeKey("VK_" + autocompleteKey, { ctrlKey: true }, win);
 
   info("Waiting for popup to be opened");
-  yield popupOpened;
+  await popupOpened;
 
   EventUtils.synthesizeKey("VK_DOWN", { }, win);
   is(ed.getText(), "bar", "Editor text has been updated");
 
   EventUtils.synthesizeKey("VK_DOWN", { }, win);
   is(ed.getText(), "body", "Editor text has been updated");
 
   EventUtils.synthesizeKey("VK_DOWN", { }, win);
   is(ed.getText(), "#baz", "Editor text has been updated");
 }
 
-function* testKeyboardCycleForPrefixedString(ed, win) {
+async function testKeyboardCycleForPrefixedString(ed, win) {
   ed.focus();
   ed.setText("#b");
   ed.setCursor({line: 1, ch: 2});
 
   let popupOpened = ed.getAutocompletionPopup().once("popup-opened");
 
   let autocompleteKey =
     Editor.keyFor("autocompletion", { noaccel: true }).toUpperCase();
   EventUtils.synthesizeKey("VK_" + autocompleteKey, { ctrlKey: true }, win);
 
   info("Waiting for popup to be opened");
-  yield popupOpened;
+  await popupOpened;
 
   EventUtils.synthesizeKey("VK_DOWN", { }, win);
   is(ed.getText(), "#baz", "Editor text has been updated");
 }
 
-function* testKeyboardCSSComma(ed, win) {
+async function testKeyboardCSSComma(ed, win) {
   ed.focus();
   ed.setText("b");
   ed.setCursor({line: 1, ch: 1});
 
   let isPopupOpened = false;
   let popupOpened = ed.getAutocompletionPopup().once("popup-opened");
   popupOpened.then(() => {
     isPopupOpened = true;
   });
 
   EventUtils.synthesizeKey(",", { }, win);
 
-  yield wait(500);
+  await wait(500);
 
   ok(!isPopupOpened, "Autocompletion shouldn't be opened");
 }
 
-function* testMouse(ed, win) {
+async function testMouse(ed, win) {
   ed.focus();
   ed.setText("b");
   ed.setCursor({line: 1, ch: 1});
 
   let popupOpened = ed.getAutocompletionPopup().once("popup-opened");
 
   let autocompleteKey =
     Editor.keyFor("autocompletion", { noaccel: true }).toUpperCase();
   EventUtils.synthesizeKey("VK_" + autocompleteKey, { ctrlKey: true }, win);
 
   info("Waiting for popup to be opened");
-  yield popupOpened;
+  await popupOpened;
   ed.getAutocompletionPopup()._list.children[2].click();
   is(ed.getText(), "#baz", "Editor text has been updated");
 }
diff --git a/devtools/client/sourceeditor/test/browser_editor_find_again.js b/devtools/client/sourceeditor/test/browser_editor_find_again.js
--- a/devtools/client/sourceeditor/test/browser_editor_find_again.js
+++ b/devtools/client/sourceeditor/test/browser_editor_find_again.js
@@ -65,17 +65,17 @@ function testFindAgain(ed, inputLine, ex
   } else {
     synthesizeKeyShortcut(FINDNEXT_KEY, edWin);
   }
 
   ch(ed.getCursor(), expectCursor,
     "find: " + inputLine + " expects cursor: " + expectCursor.toSource());
 }
 
-const testSearchBoxTextIsSelected = Task.async(function* (ed) {
+const testSearchBoxTextIsSelected = async function(ed) {
   let edDoc = ed.container.contentDocument;
   let edWin = edDoc.defaultView;
 
   let input = edDoc.querySelector("input[type=search]");
   ok(input, "search box is opened");
 
   // Ensure the input has the focus before send the key – necessary on Linux,
   // it seems that during the tests can be lost
@@ -88,17 +88,17 @@ const testSearchBoxTextIsSelected = Task
   ok(!input, "search box is closed");
 
   // Re-open the search box
   synthesizeKeyShortcut(FIND_KEY, edWin);
 
   input = edDoc.querySelector("input[type=search]");
   ok(input, "find command key opens the search box");
 
-  yield dispatchAndWaitForFocus(input);
+  await dispatchAndWaitForFocus(input);
 
   let { selectionStart, selectionEnd, value } = input;
 
   ok(selectionStart === 0 && selectionEnd === value.length,
     "search box's text is selected when re-opened");
 
   // Removing selection
   input.setSelectionRange(0, 0);
@@ -107,19 +107,19 @@ const testSearchBoxTextIsSelected = Task
 
   ({ selectionStart, selectionEnd } = input);
 
   ok(selectionStart === 0 && selectionEnd === value.length,
     "search box's text is selected when find key is pressed");
 
   // Close search box
   EventUtils.synthesizeKey("VK_ESCAPE", {}, edWin);
-});
+};
 
-const testReplaceBoxTextIsSelected = Task.async(function* (ed) {
+const testReplaceBoxTextIsSelected = async function(ed) {
   let edDoc = ed.container.contentDocument;
   let edWin = edDoc.defaultView;
 
   let input = edDoc.querySelector(".CodeMirror-dialog > input");
   ok(!input, "dialog box with replace is closed");
 
   // The editor needs the focus to properly receive the `synthesizeKey`
   ed.focus();
@@ -130,46 +130,46 @@ const testReplaceBoxTextIsSelected = Tas
   ok(input, "dialog box with replace is opened");
 
   input.value = "line 5";
 
   // Ensure the input has the focus before send the key – necessary on Linux,
   // it seems that during the tests can be lost
   input.focus();
 
-  yield dispatchAndWaitForFocus(input);
+  await dispatchAndWaitForFocus(input);
 
   let { selectionStart, selectionEnd, value } = input;
 
   ok(!(selectionStart === 0 && selectionEnd === value.length),
     "Text in dialog box is not selected");
 
   synthesizeKeyShortcut(REPLACE_KEY, edWin);
 
   ({ selectionStart, selectionEnd } = input);
 
   ok(selectionStart === 0 && selectionEnd === value.length,
     "dialog box's text is selected when replace key is pressed");
 
   // Close dialog box
   EventUtils.synthesizeKey("VK_ESCAPE", {}, edWin);
-});
+};
 
-add_task(function* () {
-  let { ed, win } = yield setup();
+add_task(async function() {
+  let { ed, win } = await setup();
 
   ed.setText([
     "// line 1",
     "//  line 2",
     "//   line 3",
     "//    line 4",
     "//     line 5"
   ].join("\n"));
 
-  yield promiseWaitForFocus();
+  await promiseWaitForFocus();
 
   openSearchBox(ed);
 
   let testVectors = [
     // Starting here expect data needs to get updated for length changes to
     // "textLines" above.
     ["line",
      {line: 0, ch: 7}],
@@ -199,17 +199,17 @@ add_task(function* () {
      {line: 1, ch: 8},
      true],
     ["line",
      {line: 0, ch: 7},
      true]
   ];
 
   for (let v of testVectors) {
-    yield testFindAgain(ed, ...v);
+    await testFindAgain(ed, ...v);
   }
 
-  yield testSearchBoxTextIsSelected(ed);
+  await testSearchBoxTextIsSelected(ed);
 
-  yield testReplaceBoxTextIsSelected(ed);
+  await testReplaceBoxTextIsSelected(ed);
 
   teardown(ed, win);
 });
diff --git a/devtools/client/sourceeditor/test/browser_editor_script_injection.js b/devtools/client/sourceeditor/test/browser_editor_script_injection.js
--- a/devtools/client/sourceeditor/test/browser_editor_script_injection.js
+++ b/devtools/client/sourceeditor/test/browser_editor_script_injection.js
@@ -1,26 +1,26 @@
 /* Any copyright is dedicated to the Public Domain.
    http://creativecommons.org/publicdomain/zero/1.0/ */
 
 // Test the externalScripts option, which allows custom language modes or
 // other scripts to be injected into the editor window.  See Bug 1089428.
 
 "use strict";
 
-add_task(function* () {
-  yield runTest();
+add_task(async function() {
+  await runTest();
 });
 
-function* runTest() {
+async function runTest() {
   const baseURL =
     "chrome://mochitests/content/browser/devtools/client/sourceeditor/test";
   const injectedText = "Script successfully injected!";
 
-  let {ed, win} = yield setup(null, {
+  let {ed, win} = await setup(null, {
     mode: "ruby",
     externalScripts: [`${baseURL}/cm_script_injection_test.js`,
                       `${baseURL}/cm_mode_ruby.js`]
   });
 
   is(ed.getText(), injectedText, "The text has been injected");
   is(ed.getOption("mode"), "ruby", "The ruby mode is correctly set");
   teardown(ed, win);
