# HG changeset patch
# User Andi-Bogdan Postelnicu <bpostelnicu@mozilla.com>
# Date 1535544667 0
# Node ID 19ec31a683cc6c161e8312b1126d130b69ea0892
# Parent  61268e5157284de3230e771bd62e5a39739bd0eb
Bug 1486452 - mach static analysis autotest - display the error if we encounter clang-diagnostic-error. r=janx

Differential Revision: https://phabricator.services.mozilla.com/D4425

diff --git a/python/mozbuild/mozbuild/mach_commands.py b/python/mozbuild/mozbuild/mach_commands.py
--- a/python/mozbuild/mozbuild/mach_commands.py
+++ b/python/mozbuild/mozbuild/mach_commands.py
@@ -1887,16 +1887,17 @@ class StaticAnalysis(MachCommandBase):
         self.TOOLS_SUCCESS = 0
         self.TOOLS_FAILED_DOWNLOAD = 1
         self.TOOLS_UNSUPORTED_PLATFORM = 2
         self.TOOLS_CHECKER_NO_TEST_FILE = 3
         self.TOOLS_CHECKER_RETURNED_NO_ISSUES = 4
         self.TOOLS_CHECKER_RESULT_FILE_NOT_FOUND = 5
         self.TOOLS_CHECKER_DIFF_FAILED = 6
         self.TOOLS_CHECKER_NOT_FOUND = 7
+        self.TOOLS_CHECKER_FAILED_FILE = 8
 
         # Configure the tree or download clang-tidy package, depending on the option that we choose
         if intree_tool:
             _, config, _ = self._get_config_environment()
             clang_tools_path = self.topsrcdir
             self._clang_tidy_path = mozpath.join(
                 clang_tools_path, "clang", "bin",
                 "clang-tidy" + config.substs.get('BIN_SUFFIX', ''))
@@ -1969,22 +1970,31 @@ class StaticAnalysis(MachCommandBase):
                 checker_not_in_list = checker_names and (item['name'] not in checker_names or not_published)
                 if not_published or \
                    ignored_platform or \
                    ignored_checker or \
                    checker_not_in_list:
                     continue
                 futures.append(executor.submit(self._verify_checker, item))
 
+            error_code = self.TOOLS_SUCCESS
             for future in concurrent.futures.as_completed(futures):
+                # Wait for every task to finish
                 ret_val = future.result()
                 if ret_val != self.TOOLS_SUCCESS:
-                    # Also delete the tmp folder
-                    shutil.rmtree(self._compilation_commands_path)
-                    return ret_val
+                    # We are interested only in one error and we don't break
+                    # the execution of for loop since we want to make sure that all
+                    # tasks finished.
+                    error_code = ret_val
+
+            if error_code != self.TOOLS_SUCCESS:
+                self.log(logging.INFO, 'static-analysis', {}, "FAIL: clang-tidy some tests failed.")
+                # Also delete the tmp folder
+                shutil.rmtree(self._compilation_commands_path)
+                return error_code
 
         self.log(logging.INFO, 'static-analysis', {}, "SUCCESS: clang-tidy all tests passed.")
         # Also delete the tmp folder
         shutil.rmtree(self._compilation_commands_path)
         return self.TOOLS_SUCCESS
 
     def _create_temp_compilation_db(self, config):
         directory = tempfile.mkdtemp(prefix='cc')
@@ -2098,52 +2108,55 @@ class StaticAnalysis(MachCommandBase):
 
         # Verify if the test file exists for this checker
         if not os.path.exists(test_file_path_cpp):
             self.log(logging.ERROR, 'static-analysis', {}, "ERROR: clang-tidy checker {} doesn't have a test file.".format(check))
             return self.TOOLS_CHECKER_NO_TEST_FILE
 
         cmd = self._get_clang_tidy_command(
             checks='-*,' + check, header_filter='', sources=[test_file_path_cpp], jobs=1, fix=False)
-
-        clang_output = subprocess.check_output(
-            cmd, stderr=subprocess.STDOUT).decode('utf-8')
-
-        issues = self._parse_issues(clang_output)
-
-        # Verify to see if we got any issues, if not raise exception
-        if not issues:
-            self.log(
-                logging.ERROR, 'static-analysis', {},
-                "ERROR: clang-tidy checker {0} did not find any issues in it\'s associated test suite.".
-                format(check))
-            return self.CHECKER_RETURNED_NO_ISSUES
-
-        if self._dump_results:
-            self._build_autotest_result(test_file_path_json, issues)
+        try:
+            clang_output = subprocess.check_output(
+                cmd, stderr=subprocess.STDOUT).decode('utf-8')
+        except subprocess.CalledProcessError as e:
+            print(e.output)
+            return self.TOOLS_CHECKER_FAILED_FILE
         else:
-            if not os.path.exists(test_file_path_json):
-                # Result file for test not found maybe regenerate it?
+            issues = self._parse_issues(clang_output)
+
+            # Verify to see if we got any issues, if not raise exception
+            if not issues:
                 self.log(
                     logging.ERROR, 'static-analysis', {},
-                    "ERROR: clang-tidy result file not found for checker {0}".format(
-                        check))
-                return self.TOOLS_CHECKER_RESULT_FILE_NOT_FOUND
-            # Read the pre-determined issues
-            baseline_issues = self._get_autotest_stored_issues(test_file_path_json)
-
-            # Compare the two lists
-            if issues != baseline_issues:
-                print("Clang output: {}".format(clang_output))
-                self.log(
-                    logging.ERROR, 'static-analysis', {},
-                    "ERROR: clang-tidy auto-test failed for checker {0} Expected: {1} Got: {2}".
-                    format(check, baseline_issues, issues))
-                return self.TOOLS_CHECKER_DIFF_FAILED
-        return self.TOOLS_SUCCESS
+                    "ERROR: clang-tidy checker {0} did not find any issues in its associated test file.".
+                    format(check))
+                return self.CHECKER_RETURNED_NO_ISSUES
+
+            if self._dump_results:
+                self._build_autotest_result(test_file_path_json, issues)
+            else:
+                if not os.path.exists(test_file_path_json):
+                    # Result file for test not found maybe regenerate it?
+                    self.log(
+                        logging.ERROR, 'static-analysis', {},
+                        "ERROR: clang-tidy result file not found for checker {0}".format(
+                            check))
+                    return self.TOOLS_CHECKER_RESULT_FILE_NOT_FOUND
+                # Read the pre-determined issues
+                baseline_issues = self._get_autotest_stored_issues(test_file_path_json)
+
+                # Compare the two lists
+                if issues != baseline_issues:
+                    print("Clang output: {}".format(clang_output))
+                    self.log(
+                        logging.ERROR, 'static-analysis', {},
+                        "ERROR: clang-tidy auto-test failed for checker {0} Expected: {1} Got: {2}".
+                        format(check, baseline_issues, issues))
+                    return self.TOOLS_CHECKER_DIFF_FAILED
+            return self.TOOLS_SUCCESS
 
     def _build_autotest_result(self, file, issues):
         with open(file, 'w') as f:
             json.dump(issues, f, indent=4, sort_keys=True)
 
     def _get_autotest_stored_issues(self, file):
         with open(file) as f:
             return json.load(f)
