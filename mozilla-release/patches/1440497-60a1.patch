# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1519766630 28800
# Node ID f68825f0495dba102d4f1d73d246095e7c82e214
# Parent  b663ebf056c58201962c9dd93923293247bdde87
Bug 1440497 - When syntax-parsing, properly handle an ArrowFunction at the end of a ConditionalExpression that forms the body of a SpiderMonkey-proprietary expression closure.  r=arai

diff --git a/js/src/frontend/FullParseHandler.h b/js/src/frontend/FullParseHandler.h
--- a/js/src/frontend/FullParseHandler.h
+++ b/js/src/frontend/FullParseHandler.h
@@ -684,16 +684,22 @@ class FullParseHandler
     }
 
     bool isExpressionClosure(ParseNode* node) const {
         return node->isKind(ParseNodeKind::Function) &&
                node->pn_funbox->isExprBody() &&
                !node->pn_funbox->isArrow();
     }
 
+    void noteExpressionClosure(Node* funcNode) const {
+        // No need to do anything: |funcNode->pn_funbox| modifications
+        // performed elsewhere in the relevant code path will assure
+        // |isExpressionClosure| above tests true on |*funcNode|.
+    }
+
     ParseNode* newObjectMethodOrPropertyDefinition(ParseNode* key, ParseNode* fn, AccessorType atype) {
         MOZ_ASSERT(isUsableAsObjectPropertyName(key));
 
         return newBinary(ParseNodeKind::Colon, key, fn, AccessorTypeToJSOp(atype));
     }
 
     void setFunctionFormalParametersAndBody(ParseNode* funcNode, ParseNode* kid) {
         MOZ_ASSERT_IF(kid, kid->isKind(ParseNodeKind::ParamsBody));
diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -2592,17 +2592,17 @@ Parser<FullParseHandler, CharT>::standal
     SourceParseContext funpc(this, funbox, newDirectives);
     if (!funpc.init())
         return null();
     funpc.setIsStandaloneFunctionBody();
 
     YieldHandling yieldHandling = GetYieldHandling(generatorKind);
     AwaitHandling awaitHandling = GetAwaitHandling(asyncKind);
     AutoAwaitIsKeyword<FullParseHandler, CharT> awaitIsKeyword(this, awaitHandling);
-    if (!functionFormalParametersAndBody(InAllowed, yieldHandling, fn, Statement,
+    if (!functionFormalParametersAndBody(InAllowed, yieldHandling, &fn, Statement,
                                          parameterListEnd, /* isStandaloneFunction = */ true))
     {
         return null();
     }
 
     if (!tokenStream.getToken(&tt, TokenStream::Operand))
         return null();
     if (tt != TokenKind::Eof) {
@@ -3411,17 +3411,17 @@ GeneralParser<ParseHandler, CharT>::func
 
     Position start(keepAtoms);
     tokenStream.tell(&start);
 
     // Parse the inner function. The following is a loop as we may attempt to
     // reparse a function due to failed syntax parsing and encountering new
     // "use foo" directives.
     while (true) {
-        if (trySyntaxParseInnerFunction(funcNode, fun, toStringStart, inHandling, yieldHandling,
+        if (trySyntaxParseInnerFunction(&funcNode, fun, toStringStart, inHandling, yieldHandling,
                                         kind, generatorKind, asyncKind, tryAnnexB, directives,
                                         &newDirectives))
         {
             break;
         }
 
         // Return on error.
         if (anyChars.hadError() || directives == newDirectives)
@@ -3439,17 +3439,17 @@ GeneralParser<ParseHandler, CharT>::func
         handler.setFunctionFormalParametersAndBody(funcNode, null());
     }
 
     return funcNode;
 }
 
 template <typename CharT>
 bool
-Parser<FullParseHandler, CharT>::trySyntaxParseInnerFunction(ParseNode* funcNode,
+Parser<FullParseHandler, CharT>::trySyntaxParseInnerFunction(ParseNode** funcNode,
                                                              HandleFunction fun,
                                                              uint32_t toStringStart,
                                                              InHandling inHandling,
                                                              YieldHandling yieldHandling,
                                                              FunctionSyntaxKind kind,
                                                              GeneratorKind generatorKind,
                                                              FunctionAsyncKind asyncKind,
                                                              bool tryAnnexB,
@@ -3457,17 +3457,17 @@ Parser<FullParseHandler, CharT>::trySynt
                                                              Directives* newDirectives)
 {
     // Try a syntax parse for this inner function.
     do {
         // If we're assuming this function is an IIFE, always perform a full
         // parse to avoid the overhead of a lazy syntax-only parse. Although
         // the prediction may be incorrect, IIFEs are common enough that it
         // pays off for lots of code.
-        if (funcNode->isLikelyIIFE() &&
+        if ((*funcNode)->isLikelyIIFE() &&
             generatorKind == GeneratorKind::NotGenerator &&
             asyncKind == FunctionAsyncKind::SyncFunction)
         {
             break;
         }
 
         SyntaxParser* syntaxParser = getSyntaxParser();
         if (!syntaxParser)
@@ -3479,26 +3479,27 @@ Parser<FullParseHandler, CharT>::trySynt
         Position position(keepAtoms);
         tokenStream.tell(&position);
         if (!syntaxParser->tokenStream.seek(position, anyChars))
             return false;
 
         // Make a FunctionBox before we enter the syntax parser, because |pn|
         // still expects a FunctionBox to be attached to it during BCE, and
         // the syntax parser cannot attach one to it.
-        FunctionBox* funbox = newFunctionBox(funcNode, fun, toStringStart, inheritedDirectives,
+        FunctionBox* funbox = newFunctionBox(*funcNode, fun, toStringStart, inheritedDirectives,
                                              generatorKind, asyncKind);
         if (!funbox)
             return false;
         funbox->initWithEnclosingParseContext(pc, kind);
 
-        if (!syntaxParser->innerFunctionForFunctionBox(SyntaxParseHandler::NodeGeneric,
-                                                       pc, funbox, inHandling, yieldHandling,
-                                                       kind, newDirectives))
-        {
+        SyntaxParseHandler::Node syntaxNode =
+            syntaxParser->innerFunctionForFunctionBox(SyntaxParseHandler::NodeGeneric, pc, funbox,
+                                                      inHandling, yieldHandling, kind,
+                                                      newDirectives);
+        if (!syntaxNode) {
             if (syntaxParser->hadAbortedSyntaxParse()) {
                 // Try again with a full parse. UsedNameTracker needs to be
                 // rewound to just before we tried the syntax parse for
                 // correctness.
                 syntaxParser->clearAbortedSyntaxParse();
                 usedNames.rewind(token);
                 MOZ_ASSERT_IF(!syntaxParser->context->helperThread(),
                               !syntaxParser->context->isExceptionPending());
@@ -3508,53 +3509,66 @@ Parser<FullParseHandler, CharT>::trySynt
         }
 
         // Advance this parser over tokens processed by the syntax parser.
         syntaxParser->tokenStream.tell(&position);
         if (!tokenStream.seek(position, syntaxParser->anyChars))
             return false;
 
         // Update the end position of the parse node.
-        funcNode->pn_pos.end = anyChars.currentToken().pos.end;
+        (*funcNode)->pn_pos.end = anyChars.currentToken().pos.end;
 
         // Append possible Annex B function box only upon successfully parsing.
         if (tryAnnexB) {
             if (!pc->innermostScope()->addPossibleAnnexBFunctionBox(pc, funbox))
                 return false;
         }
 
         return true;
     } while (false);
 
     // We failed to do a syntax parse above, so do the full parse.
-    return innerFunction(funcNode, pc, fun, toStringStart, inHandling, yieldHandling, kind,
-                         generatorKind, asyncKind, tryAnnexB, inheritedDirectives, newDirectives);
+    Node innerFunc =
+        innerFunction(*funcNode, pc, fun, toStringStart, inHandling, yieldHandling, kind,
+                      generatorKind, asyncKind, tryAnnexB, inheritedDirectives, newDirectives);
+    if (!innerFunc)
+        return false;
+
+    *funcNode = innerFunc;
+    return true;
 }
 
 template <typename CharT>
 bool
-Parser<SyntaxParseHandler, CharT>::trySyntaxParseInnerFunction(Node funcNode, HandleFunction fun,
+Parser<SyntaxParseHandler, CharT>::trySyntaxParseInnerFunction(Node* funcNode, HandleFunction fun,
                                                                uint32_t toStringStart,
                                                                InHandling inHandling,
                                                                YieldHandling yieldHandling,
                                                                FunctionSyntaxKind kind,
                                                                GeneratorKind generatorKind,
                                                                FunctionAsyncKind asyncKind,
                                                                bool tryAnnexB,
                                                                Directives inheritedDirectives,
                                                                Directives* newDirectives)
 {
     // This is already a syntax parser, so just parse the inner function.
-    return innerFunction(funcNode, pc, fun, toStringStart, inHandling, yieldHandling, kind,
-                         generatorKind, asyncKind, tryAnnexB, inheritedDirectives, newDirectives);
+    Node innerFunc =
+        innerFunction(*funcNode, pc, fun, toStringStart, inHandling, yieldHandling, kind,
+                      generatorKind, asyncKind, tryAnnexB, inheritedDirectives, newDirectives);
+
+    if (!innerFunc)
+        return false;
+
+    *funcNode = innerFunc;
+    return true;
 }
 
 template <class ParseHandler, typename CharT>
 inline bool
-GeneralParser<ParseHandler, CharT>::trySyntaxParseInnerFunction(Node funcNode, HandleFunction fun,
+GeneralParser<ParseHandler, CharT>::trySyntaxParseInnerFunction(Node* funcNode, HandleFunction fun,
                                                                 uint32_t toStringStart,
                                                                 InHandling inHandling,
                                                                 YieldHandling yieldHandling,
                                                                 FunctionSyntaxKind kind,
                                                                 GeneratorKind generatorKind,
                                                                 FunctionAsyncKind asyncKind,
                                                                 bool tryAnnexB,
                                                                 Directives inheritedDirectives,
@@ -3562,43 +3576,46 @@ GeneralParser<ParseHandler, CharT>::tryS
 {
     return asFinalParser()->trySyntaxParseInnerFunction(funcNode, fun, toStringStart, inHandling,
                                                         yieldHandling, kind, generatorKind,
                                                         asyncKind, tryAnnexB, inheritedDirectives,
                                                         newDirectives);
 }
 
 template <class ParseHandler, typename CharT>
-bool
+typename ParseHandler::Node
 GeneralParser<ParseHandler, CharT>::innerFunctionForFunctionBox(Node funcNode,
                                                                 ParseContext* outerpc,
                                                                 FunctionBox* funbox,
                                                                 InHandling inHandling,
                                                                 YieldHandling yieldHandling,
                                                                 FunctionSyntaxKind kind,
                                                                 Directives* newDirectives)
 {
     // Note that it is possible for outerpc != this->pc, as we may be
     // attempting to syntax parse an inner function from an outer full
     // parser. In that case, outerpc is a SourceParseContext from the full parser
     // instead of the current top of the stack of the syntax parser.
 
     // Push a new ParseContext.
     SourceParseContext funpc(this, funbox, newDirectives);
     if (!funpc.init())
-        return false;
-
-    if (!functionFormalParametersAndBody(inHandling, yieldHandling, funcNode, kind))
-        return false;
-
-    return leaveInnerFunction(outerpc);
-}
-
-template <class ParseHandler, typename CharT>
-bool
+        return null();
+
+    if (!functionFormalParametersAndBody(inHandling, yieldHandling, &funcNode, kind))
+        return null();
+
+    if (!leaveInnerFunction(outerpc))
+        return null();
+
+    return funcNode;
+}
+
+template <class ParseHandler, typename CharT>
+typename ParseHandler::Node
 GeneralParser<ParseHandler, CharT>::innerFunction(Node funcNode, ParseContext* outerpc,
                                                   HandleFunction fun, uint32_t toStringStart,
                                                   InHandling inHandling,
                                                   YieldHandling yieldHandling,
                                                   FunctionSyntaxKind kind,
                                                   GeneratorKind generatorKind,
                                                   FunctionAsyncKind asyncKind, bool tryAnnexB,
                                                   Directives inheritedDirectives,
@@ -3607,32 +3624,32 @@ GeneralParser<ParseHandler, CharT>::inne
     // Note that it is possible for outerpc != this->pc, as we may be
     // attempting to syntax parse an inner function from an outer full
     // parser. In that case, outerpc is a SourceParseContext from the full parser
     // instead of the current top of the stack of the syntax parser.
 
     FunctionBox* funbox = newFunctionBox(funcNode, fun, toStringStart, inheritedDirectives,
                                          generatorKind, asyncKind);
     if (!funbox)
-        return false;
+        return null();
     funbox->initWithEnclosingParseContext(outerpc, kind);
 
-    if (!innerFunctionForFunctionBox(funcNode, outerpc, funbox, inHandling, yieldHandling, kind,
-                                     newDirectives))
-    {
-        return false;
-    }
+    Node innerFunc =
+        innerFunctionForFunctionBox(funcNode, outerpc, funbox, inHandling, yieldHandling, kind,
+                                     newDirectives);
+    if (!innerFunc)
+        return null();
 
     // Append possible Annex B function box only upon successfully parsing.
     if (tryAnnexB) {
         if (!pc->innermostScope()->addPossibleAnnexBFunctionBox(pc, funbox))
-            return false;
-    }
-
-    return true;
+            return null();
+    }
+
+    return innerFunc;
 }
 
 template <class ParseHandler, typename CharT>
 bool
 GeneralParser<ParseHandler, CharT>::appendToCallSiteObj(Node callSiteObj)
 {
     Node cookedNode = noSubstitutionTaggedTemplate();
     if (!cookedNode)
@@ -3693,32 +3710,32 @@ Parser<FullParseHandler, CharT>::standal
         syntaxKind = Method;
     else if (fun->isGetter())
         syntaxKind = Getter;
     else if (fun->isSetter())
         syntaxKind = Setter;
     else if (fun->isArrow())
         syntaxKind = Arrow;
 
-    if (!functionFormalParametersAndBody(InAllowed, yieldHandling, pn, syntaxKind)) {
+    if (!functionFormalParametersAndBody(InAllowed, yieldHandling, &pn, syntaxKind)) {
         MOZ_ASSERT(directives == newDirectives);
         return null();
     }
 
     if (!FoldConstants(context, &pn, this))
         return null();
 
     return pn;
 }
 
 template <class ParseHandler, typename CharT>
 bool
 GeneralParser<ParseHandler, CharT>::functionFormalParametersAndBody(InHandling inHandling,
                                                                     YieldHandling yieldHandling,
-                                                                    Node pn,
+                                                                    Node* pn,
                                                                     FunctionSyntaxKind kind,
                                                                     const Maybe<uint32_t>& parameterListEnd /* = Nothing() */,
                                                                     bool isStandaloneFunction /* = false */)
 {
     // Given a properly initialized parse context, try to parse an actual
     // function without concern for conversion to strict mode, use of lazy
     // parsing and such.
 
@@ -3727,17 +3744,17 @@ GeneralParser<ParseHandler, CharT>::func
 
     // See below for an explanation why arrow function parameters and arrow
     // function bodies are parsed with different yield/await settings.
     {
         AwaitHandling awaitHandling = funbox->isAsync() || (kind == Arrow && awaitIsKeyword())
                                       ? AwaitIsKeyword
                                       : AwaitIsName;
         AutoAwaitIsKeyword<ParseHandler, CharT> awaitIsKeyword(this, awaitHandling);
-        if (!functionArguments(yieldHandling, kind, pn))
+        if (!functionArguments(yieldHandling, kind, *pn))
             return false;
     }
 
     Maybe<ParseContext::VarScope> varScope;
     if (funbox->hasParameterExprs) {
         varScope.emplace(this);
         if (!varScope->init(pc))
             return false;
@@ -3777,16 +3794,17 @@ GeneralParser<ParseHandler, CharT>::func
                 error(JSMSG_CURLY_BEFORE_BODY);
                 return false;
             }
 
 #if JS_HAS_EXPR_CLOSURES
             this->addTelemetry(DeprecatedLanguageExtension::ExpressionClosure);
             if (!warnOnceAboutExprClosure())
                 return false;
+            handler.noteExpressionClosure(pn);
 #else
             error(JSMSG_CURLY_BEFORE_BODY);
             return false;
 #endif
         }
 
         anyChars.ungetToken();
         bodyType = ExpressionBody;
@@ -3831,17 +3849,17 @@ GeneralParser<ParseHandler, CharT>::func
             // because of different context.
             // It should already be checked before this point.
             nameYieldHandling = YieldIsName;
         }
 
         // We already use the correct await-handling at this point, therefore
         // we don't need call AutoAwaitIsKeyword here.
 
-        uint32_t nameOffset = handler.getFunctionNameOffset(pn, anyChars);
+        uint32_t nameOffset = handler.getFunctionNameOffset(*pn, anyChars);
         if (!checkBindingIdentifier(propertyName, nameOffset, nameYieldHandling))
             return false;
     }
 
     if (bodyType == StatementListBody) {
         MUST_MATCH_TOKEN_MOD_WITH_REPORT(TokenKind::Rc, TokenStream::Operand,
                                          reportMissingClosing(JSMSG_CURLY_AFTER_BODY,
                                                               JSMSG_CURLY_OPENED, openedPos));
@@ -3859,18 +3877,18 @@ GeneralParser<ParseHandler, CharT>::func
 
     if (IsMethodDefinitionKind(kind) && pc->superScopeNeedsHomeObject())
         funbox->setNeedsHomeObject();
 
     if (!finishFunction(isStandaloneFunction))
         return false;
 
     handler.setEndPosition(body, pos().begin);
-    handler.setEndPosition(pn, pos().end);
-    handler.setFunctionBody(pn, body);
+    handler.setEndPosition(*pn, pos().end);
+    handler.setFunctionBody(*pn, body);
 
     return true;
 }
 
 template <class ParseHandler, typename CharT>
 typename ParseHandler::Node
 GeneralParser<ParseHandler, CharT>::functionStmt(uint32_t toStringStart,
                                                  YieldHandling yieldHandling,
diff --git a/js/src/frontend/Parser.h b/js/src/frontend/Parser.h
--- a/js/src/frontend/Parser.h
+++ b/js/src/frontend/Parser.h
@@ -933,41 +933,42 @@ class GeneralParser
 
     Node noSubstitutionUntaggedTemplate();
     Node templateLiteral(YieldHandling yieldHandling);
     bool taggedTemplate(YieldHandling yieldHandling, Node nodeList, TokenKind tt);
     bool appendToCallSiteObj(Node callSiteObj);
     bool addExprAndGetNextTemplStrToken(YieldHandling yieldHandling, Node nodeList,
                                         TokenKind* ttp);
 
-    inline bool trySyntaxParseInnerFunction(Node funcNode, HandleFunction fun,
+    inline bool trySyntaxParseInnerFunction(Node* funcNode, HandleFunction fun,
                                             uint32_t toStringStart, InHandling inHandling,
                                             YieldHandling yieldHandling, FunctionSyntaxKind kind,
                                             GeneratorKind generatorKind,
                                             FunctionAsyncKind asyncKind, bool tryAnnexB,
                                             Directives inheritedDirectives,
                                             Directives* newDirectives);
 
     inline bool skipLazyInnerFunction(Node funcNode, uint32_t toStringStart,
                                       FunctionSyntaxKind kind, bool tryAnnexB);
 
   public:
     /* Public entry points for parsing. */
     Node statementListItem(YieldHandling yieldHandling, bool canHaveDirectives = false);
 
     // Parse an inner function given an enclosing ParseContext and a
     // FunctionBox for the inner function.
-    bool innerFunctionForFunctionBox(Node funcNode, ParseContext* outerpc, FunctionBox* funbox,
-                                     InHandling inHandling, YieldHandling yieldHandling,
-                                     FunctionSyntaxKind kind, Directives* newDirectives);
+    MOZ_MUST_USE Node
+    innerFunctionForFunctionBox(Node funcNode, ParseContext* outerpc, FunctionBox* funbox,
+                                InHandling inHandling, YieldHandling yieldHandling,
+                                FunctionSyntaxKind kind, Directives* newDirectives);
 
     // Parse a function's formal parameters and its body assuming its function
     // ParseContext is already on the stack.
     bool functionFormalParametersAndBody(InHandling inHandling, YieldHandling yieldHandling,
-                                         Node pn, FunctionSyntaxKind kind,
+                                         Node* pn, FunctionSyntaxKind kind,
                                          const mozilla::Maybe<uint32_t>& parameterListEnd = mozilla::Nothing(),
                                          bool isStandaloneFunction = false);
 
   private:
     /*
      * JS parsers, from lowest to highest precedence.
      *
      * Each parser must be called during the dynamic scope of a ParseContext
@@ -1238,21 +1239,22 @@ class GeneralParser
     PropertyName* bindingIdentifier(YieldHandling yieldHandling);
 
     bool checkLabelOrIdentifierReference(PropertyName* ident, uint32_t offset,
                                          YieldHandling yieldHandling,
                                          TokenKind hint = TokenKind::Limit);
 
     Node statementList(YieldHandling yieldHandling);
 
-    bool innerFunction(Node funcNode, ParseContext* outerpc, HandleFunction fun,
-                       uint32_t toStringStart, InHandling inHandling, YieldHandling yieldHandling,
-                       FunctionSyntaxKind kind, GeneratorKind generatorKind,
-                       FunctionAsyncKind asyncKind, bool tryAnnexB, Directives inheritedDirectives,
-                       Directives* newDirectives);
+    MOZ_MUST_USE Node
+    innerFunction(Node funcNode, ParseContext* outerpc, HandleFunction fun,
+                  uint32_t toStringStart, InHandling inHandling, YieldHandling yieldHandling,
+                  FunctionSyntaxKind kind, GeneratorKind generatorKind,
+                  FunctionAsyncKind asyncKind, bool tryAnnexB, Directives inheritedDirectives,
+                  Directives* newDirectives);
 
     bool matchOrInsertSemicolon();
 
     bool noteDeclaredName(HandlePropertyName name, DeclarationKind kind, TokenPos pos);
 
   private:
     inline bool asmJS(Node list);
 };
@@ -1345,17 +1347,17 @@ class Parser<SyntaxParseHandler, CharT> 
     inline Node importDeclaration();
     inline bool checkLocalExportNames(Node node);
     inline bool checkExportedName(JSAtom* exportName);
     inline bool checkExportedNamesForDeclaration(Node node);
     inline bool checkExportedNameForFunction(Node node);
     inline bool checkExportedNameForClass(Node node);
     inline bool checkExportedNameForClause(Node node);
 
-    bool trySyntaxParseInnerFunction(Node funcNode, HandleFunction fun, uint32_t toStringStart,
+    bool trySyntaxParseInnerFunction(Node* funcNode, HandleFunction fun, uint32_t toStringStart,
                                      InHandling inHandling, YieldHandling yieldHandling,
                                      FunctionSyntaxKind kind, GeneratorKind generatorKind,
                                      FunctionAsyncKind asyncKind, bool tryAnnexB,
                                      Directives inheritedDirectives, Directives* newDirectives);
 
     bool skipLazyInnerFunction(Node funcNode, uint32_t toStringStart, FunctionSyntaxKind kind,
                                bool tryAnnexB);
 
@@ -1460,17 +1462,17 @@ class Parser<FullParseHandler, CharT> fi
     Node importDeclaration();
     bool checkLocalExportNames(Node node);
     bool checkExportedName(JSAtom* exportName);
     bool checkExportedNamesForDeclaration(Node node);
     bool checkExportedNameForFunction(Node node);
     bool checkExportedNameForClass(Node node);
     inline bool checkExportedNameForClause(Node node);
 
-    bool trySyntaxParseInnerFunction(Node funcNode, HandleFunction fun, uint32_t toStringStart,
+    bool trySyntaxParseInnerFunction(Node* funcNode, HandleFunction fun, uint32_t toStringStart,
                                      InHandling inHandling, YieldHandling yieldHandling,
                                      FunctionSyntaxKind kind, GeneratorKind generatorKind,
                                      FunctionAsyncKind asyncKind, bool tryAnnexB,
                                      Directives inheritedDirectives, Directives* newDirectives);
 
     bool skipLazyInnerFunction(Node funcNode, uint32_t toStringStart, FunctionSyntaxKind kind,
                                bool tryAnnexB);
 
diff --git a/js/src/frontend/SyntaxParseHandler.h b/js/src/frontend/SyntaxParseHandler.h
--- a/js/src/frontend/SyntaxParseHandler.h
+++ b/js/src/frontend/SyntaxParseHandler.h
@@ -355,16 +355,20 @@ class SyntaxParseHandler
     }
 
     Node newArrowFunction(const TokenPos& pos) { return NodeFunctionArrow; }
 
     bool isExpressionClosure(Node node) const {
         return node == NodeFunctionExpressionClosure;
     }
 
+    void noteExpressionClosure(Node* funcNode) const {
+        *funcNode = NodeFunctionExpressionClosure;
+    }
+
     void setFunctionFormalParametersAndBody(Node funcNode, Node kid) {}
     void setFunctionBody(Node pn, Node kid) {}
     void setFunctionBox(Node pn, FunctionBox* funbox) {}
     void addFunctionFormalParameter(Node pn, Node argpn) {}
 
     Node newForStatement(uint32_t begin, Node forHead, Node body, unsigned iflags) {
         return NodeGeneric;
     }
diff --git a/js/src/tests/non262/extensions/arrow-as-end-of-expression-closure.js b/js/src/tests/non262/extensions/arrow-as-end-of-expression-closure.js
--- a/js/src/tests/non262/extensions/arrow-as-end-of-expression-closure.js
+++ b/js/src/tests/non262/extensions/arrow-as-end-of-expression-closure.js
@@ -11,31 +11,74 @@
 if (getBuildConfiguration().release_or_beta)
 {
   eval(`
   var ec1 = function() 0 ? 1 : a => {};
   assertEq(typeof ec1, "function");
   assertEq(typeof ec1(), "function");
   assertEq(ec1()(), undefined);
 
+  function inFunction1()
+  {
+    var ec1f = function() 0 ? 1 : a => {};
+    assertEq(typeof ec1f, "function");
+    assertEq(typeof ec1f(), "function");
+    assertEq(ec1f()(), undefined);
+  }
+  inFunction1();
+
   var ec2 = function() 0 ? 1 : a => {} // deliberately exercise ASI here
   assertEq(typeof ec2, "function");
   assertEq(typeof ec2(), "function");
   assertEq(ec2()(), undefined);
 
+  function inFunction2()
+  {
+    var ec2f = function() 0 ? 1 : a => {} // deliberately exercise ASI here
+    assertEq(typeof ec2f, "function");
+    assertEq(typeof ec2f(), "function");
+    assertEq(ec2f()(), undefined);
+  }
+  inFunction2();
+
   function ec3() 0 ? 1 : a => {} // exercise ASI here
   assertEq(typeof ec3(), "function");
 
+  function inFunction3()
+  {
+    function ec3f() 0 ? 1 : a => {} // exercise ASI here
+    assertEq(typeof ec3f(), "function");
+  }
+  inFunction3();
+
   function ec4() 0 ? 1 : a => {};
   assertEq(typeof ec4(), "function");
 
+  function inFunction4()
+  {
+    function ec4f() 0 ? 1 : a => {};
+    assertEq(typeof ec4f(), "function");
+  }
+
   var needle = "@";
   var x = 42;
   var g = { test() { assertEq(true, false, "shouldn't be called"); } };
 
   function ec5() 0 ? 1 : a => {} // ASI
   /x/g.test((needle = "x"));
   assertEq(needle, "x");
+
+  function inFunction5()
+  {
+    var needle = "@";
+    var x = 42;
+    var g = { test() { assertEq(true, false, "shouldn't be called"); } };
+  
+    function ec5f() 0 ? 1 : a => {} // ASI
+    /x/g.test((needle = "x"));
+    assertEq(needle, "x");
+  }
+  inFunction5();
   `);
 }
 
 if (typeof reportCompare === "function")
   reportCompare(true, true);
diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -7208,17 +7208,17 @@ ParseFunction(ModuleValidator& m, ParseN
         return false;
     funbox->initWithEnclosingParseContext(outerpc, frontend::Statement);
 
     Directives newDirectives = directives;
     SourceParseContext funpc(&m.parser(), funbox, &newDirectives);
     if (!funpc.init())
         return false;
 
-    if (!m.parser().functionFormalParametersAndBody(InAllowed, YieldIsName, fn, Statement)) {
+    if (!m.parser().functionFormalParametersAndBody(InAllowed, YieldIsName, &fn, Statement)) {
         if (anyChars.hadError() || directives == newDirectives)
             return false;
 
         return m.fail(fn, "encountered new directive in function");
     }
 
     MOZ_ASSERT(!anyChars.hadError());
     MOZ_ASSERT(directives == newDirectives);
