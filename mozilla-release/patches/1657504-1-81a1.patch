# HG changeset patch
# User Andrew McCreight <continuation@gmail.com>
# Date 1596735733 0
# Node ID 3dd114ed0912358015d858b32d755180f1a87b69
# Parent  be5b134276bde5db0fb6dadd1031deebb6715ca0
Bug 1657504, part 1 - Don't allow a protocol to be defined in two different files. r=nika

This changes the duplicate checking/caching implemented by |parsed| to be
based on the name of the protocol etc. we're loading, not the file name.
This lets us detect when a protocol is being defined in two different
files.

This can happen if one file is included earlier in the resolve path than
a file explicitly specified on the command line. Any includes will resolve
to the former, and then we'll attempt to parse the latter. Before this
patch, this would result in weird errors, because there would be multiple
protocol types with the same name.

Differential Revision: https://phabricator.services.mozilla.com/D86113

diff --git a/ipc/ipdl/ipdl/parser.py b/ipc/ipdl/ipdl/parser.py
--- a/ipc/ipdl/ipdl/parser.py
+++ b/ipc/ipdl/ipdl/parser.py
@@ -53,26 +53,33 @@ class Parser:
         self.lexer = None
         self.parser = None
         self.tu = TranslationUnit(type, name)
         self.direction = None
 
     def parse(self, input, filename, includedirs):
         assert os.path.isabs(filename)
 
-        if filename in Parser.parsed:
-            return Parser.parsed[filename].tu
+        if self.tu.name in Parser.parsed:
+            priorTU = Parser.parsed[self.tu.name].tu
+            if priorTU.filename != filename:
+                _error(Loc(filename),
+                       "Trying to load `%s' from a file when we'd already seen it in file `%s'" % (
+                           self.tu.name,
+                           priorTU.filename))
+
+            return priorTU
 
         self.lexer = lex.lex(debug=self.debug)
         self.parser = yacc.yacc(debug=self.debug, write_tables=False)
         self.filename = filename
         self.includedirs = includedirs
         self.tu.filename = filename
 
-        Parser.parsed[filename] = self
+        Parser.parsed[self.tu.name] = self
         Parser.parseStack.append(Parser.current)
         Parser.current = self
 
         try:
             ast = self.parser.parse(input=input, lexer=self.lexer,
                                     debug=self.debug)
         finally:
             Parser.current = Parser.parseStack.pop()
diff --git a/ipc/ipdl/test/ipdl/error/PDouble.ipdl b/ipc/ipdl/test/ipdl/error/PDouble.ipdl
new file mode 100644
--- /dev/null
+++ b/ipc/ipdl/test/ipdl/error/PDouble.ipdl
@@ -0,0 +1,11 @@
+//error: Trying to load `PDouble' from a file when we'd already seen it in file
+
+// This will load extra/PDouble.ipdl because extra/ is earlier
+// in the list of include directories than the current working
+// directory. Loading the same protocol from two files is
+// obviously bad.
+include protocol PDouble;
+
+protocol PDouble {
+child: async Msg();
+};
diff --git a/ipc/ipdl/test/ipdl/error/extra/PDouble.ipdl b/ipc/ipdl/test/ipdl/error/extra/PDouble.ipdl
new file mode 100644
--- /dev/null
+++ b/ipc/ipdl/test/ipdl/error/extra/PDouble.ipdl
@@ -0,0 +1,5 @@
+// This is a valid file.
+
+protocol PDouble {
+child: async Msg();
+};
diff --git a/ipc/ipdl/test/ipdl/runtests.py b/ipc/ipdl/test/ipdl/runtests.py
--- a/ipc/ipdl/test/ipdl/runtests.py
+++ b/ipc/ipdl/test/ipdl/runtests.py
@@ -80,17 +80,21 @@ if __name__ == '__main__':
 
     ipdlargv = []
     oksuite = unittest.TestSuite()
     errorsuite = unittest.TestSuite()
 
     oktests, errortests = False, False
     for arg in sys.argv[3:]:
         if errortests:
-            errorsuite.addTest(ErrorTestCase(ipdlargv + ['-I', errordir],
+            # The extra subdirectory is used for non-failing files we want
+            # to include from failing files.
+            errorIncludes = ['-I', os.path.join(errordir, 'extra'),
+                             '-I', errordir]
+            errorsuite.addTest(ErrorTestCase(ipdlargv + errorIncludes,
                                              arg))
         elif oktests:
             if 'ERRORTESTS' == arg:
                 errortests = True
                 continue
             oksuite.addTest(OkTestCase(ipdlargv + ['-I', okdir],
                                        arg))
         else:

