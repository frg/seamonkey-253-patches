# HG changeset patch
# User Matthew Gaudet <mgaudet@mozilla.com>
# Date 1524509514 14400
#      Mon Apr 23 14:51:54 2018 -0400
# Node ID 314bb18aab5237c69deeb216196eb165790c7dfb
# Parent  75fbdbe514f048b0bb57423ce5d0d9baa52a1033
Bug 1437842: [Part 9] Convert IsRelativeTimeFormat to GuardToRelativeTimeFormat r=jandem

diff --git a/js/src/builtin/intl/CommonFunctions.js b/js/src/builtin/intl/CommonFunctions.js
--- a/js/src/builtin/intl/CommonFunctions.js
+++ b/js/src/builtin/intl/CommonFunctions.js
@@ -1467,18 +1467,18 @@ function intlFallbackSymbol() {
 /**
  * Initializes the INTL_INTERNALS_OBJECT_SLOT of the given object.
  */
 function initializeIntlObject(obj, type, lazyData) {
     assert(IsObject(obj), "Non-object passed to initializeIntlObject");
     assert((type === "Collator" && GuardToCollator(obj) !== null) ||
            (type === "DateTimeFormat" && GuardToDateTimeFormat(obj) !== null) ||
            (type === "NumberFormat" && GuardToNumberFormat(obj) !== null) ||
-           (type === "RelativeTimeFormat" && IsRelativeTimeFormat(obj)),
            (type === "PluralRules" && GuardToPluralRules(obj) !== null) ||
+           (type === "RelativeTimeFormat" && GuardToRelativeTimeFormat(obj) !== null),
            "type must match the object's class");
     assert(IsObject(lazyData), "non-object lazy data");
 
     // The meaning of an internals object for an object |obj| is as follows.
     //
     // The .type property indicates the type of Intl object that |obj| is:
     // "Collator", "DateTimeFormat", "NumberFormat", or "PluralRules" (likely
     // with more coming in future Intl specs).
@@ -1533,29 +1533,29 @@ function maybeInternalProperties(interna
  *
  * Spec: ECMAScript Internationalization API Specification, 10.3.
  * Spec: ECMAScript Internationalization API Specification, 11.3.
  * Spec: ECMAScript Internationalization API Specification, 12.3.
  */
 function getIntlObjectInternals(obj) {
     assert(IsObject(obj), "getIntlObjectInternals called with non-Object");
     assert(GuardToCollator(obj) !== null || GuardToDateTimeFormat(obj) !== null ||
-           IsRelativeTimeFormat(obj),
            GuardToNumberFormat(obj) !== null || GuardToPluralRules(obj) !== null ||
+           GuardToRelativeTimeFormat(obj) !== null,
            "getIntlObjectInternals called with non-Intl object");
 
     var internals = UnsafeGetReservedSlot(obj, INTL_INTERNALS_OBJECT_SLOT);
 
     assert(IsObject(internals), "internals not an object");
     assert(hasOwn("type", internals), "missing type");
     assert((internals.type === "Collator" && GuardToCollator(obj) !== null) ||
            (internals.type === "DateTimeFormat" && GuardToDateTimeFormat(obj) !== null) ||
            (internals.type === "NumberFormat" && GuardToNumberFormat(obj) !== null) ||
-           (internals.type === "RelativeTimeFormat" && IsRelativeTimeFormat(obj)),
            (internals.type === "PluralRules" && GuardToPluralRules(obj) !== null) ||
+           (internals.type === "RelativeTimeFormat" && GuardToRelativeTimeFormat(obj) !== null),
            "type must match the object's class");
     assert(hasOwn("lazyData", internals), "missing lazyData");
     assert(hasOwn("internalProps", internals), "missing internalProps");
 
     return internals;
 }
 
 /**
diff --git a/js/src/builtin/intl/RelativeTimeFormat.js b/js/src/builtin/intl/RelativeTimeFormat.js
--- a/js/src/builtin/intl/RelativeTimeFormat.js
+++ b/js/src/builtin/intl/RelativeTimeFormat.js
@@ -58,17 +58,17 @@ function resolveRelativeTimeFormatIntern
 }
 
 /**
  * Returns an object containing the RelativeTimeFormat internal properties of |obj|,
  * or throws a TypeError if |obj| isn't RelativeTimeFormat-initialized.
  */
 function getRelativeTimeFormatInternals(obj, methodName) {
     assert(IsObject(obj), "getRelativeTimeFormatInternals called with non-object");
-    assert(IsRelativeTimeFormat(obj), "getRelativeTimeFormatInternals called with non-RelativeTimeFormat");
+    assert(GuardToRelativeTimeFormat(obj) !== null, "getRelativeTimeFormatInternals called with non-RelativeTimeFormat");
 
     var internals = getIntlObjectInternals(obj);
     assert(internals.type === "RelativeTimeFormat", "bad type escaped getIntlObjectInternals");
 
     var internalProps = maybeInternalProperties(internals);
     if (internalProps)
         return internalProps;
 
@@ -86,17 +86,17 @@ function getRelativeTimeFormatInternals(
  * This later work occurs in |resolveRelativeTimeFormatInternals|; steps not noted
  * here occur there.
  *
  * Spec: ECMAScript 402 API, RelativeTimeFormat, 1.1.1.
  */
 function InitializeRelativeTimeFormat(relativeTimeFormat, locales, options) {
     assert(IsObject(relativeTimeFormat),
            "InitializeRelativeimeFormat called with non-object");
-    assert(IsRelativeTimeFormat(relativeTimeFormat),
+    assert(GuardToRelativeTimeFormat(relativeTimeFormat) !== null,
            "InitializeRelativeTimeFormat called with non-RelativeTimeFormat");
 
     // Lazy RelativeTimeFormat data has the following structure:
     //
     //   {
     //     requestedLocales: List of locales,
     //     style: "long" / "short" / "narrow",
     //     numeric: "always" / "auto",
@@ -169,17 +169,17 @@ function Intl_RelativeTimeFormat_support
  *
  * Spec: ECMAScript 402 API, RelativeTImeFormat, 1.4.3.
  */
 function Intl_RelativeTimeFormat_format(value, unit) {
     // Step 1.
     let relativeTimeFormat = this;
 
     // Step 2.
-    if (!IsObject(relativeTimeFormat) || !IsRelativeTimeFormat(relativeTimeFormat))
+    if (!IsObject(relativeTimeFormat) || (relativeTimeFormat = GuardToRelativeTimeFormat(relativeTimeFormat)) === null)
         ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "RelativeTimeFormat", "format", "RelativeTimeFormat");
 
     // Ensure the RelativeTimeFormat internals are resolved.
     var internals = getRelativeTimeFormatInternals(relativeTimeFormat);
 
     // Step 3.
     let t = ToNumber(value);
 
@@ -205,23 +205,24 @@ function Intl_RelativeTimeFormat_format(
 }
 
 /**
  * Returns the resolved options for a PluralRules object.
  *
  * Spec: ECMAScript 402 API, RelativeTimeFormat, 1.4.4.
  */
 function Intl_RelativeTimeFormat_resolvedOptions() {
+    var relativeTimeFormat;
     // Check "this RelativeTimeFormat object" per introduction of section 1.4.
-    if (!IsObject(this) || !IsRelativeTimeFormat(this)) {
+    if (!IsObject(this) || (relativeTimeFormat = GuardToRelativeTimeFormat(this)) === null) {
         ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "RelativeTimeFormat", "resolvedOptions",
                        "RelativeTimeFormat");
     }
 
-    var internals = getRelativeTimeFormatInternals(this, "resolvedOptions");
+    var internals = getRelativeTimeFormatInternals(relativeTimeFormat, "resolvedOptions");
 
     var result = {
         locale: internals.locale,
         style: internals.style,
         numeric: internals.numeric,
     };
 
     return result;
diff --git a/js/src/jit/InlinableNatives.h b/js/src/jit/InlinableNatives.h
--- a/js/src/jit/InlinableNatives.h
+++ b/js/src/jit/InlinableNatives.h
@@ -27,18 +27,18 @@
     _(AtomicsXor)                   \
     _(AtomicsIsLockFree)            \
                                     \
     _(Boolean)                      \
                                     \
     _(IntlGuardToCollator)          \
     _(IntlGuardToDateTimeFormat)    \
     _(IntlGuardToNumberFormat)      \
-    _(IntlIsRelativeTimeFormat)     \
     _(IntlGuardToPluralRules)       \
+    _(IntlGuardToRelativeTimeFormat) \
                                     \
     _(MathAbs)                      \
     _(MathFloor)                    \
     _(MathCeil)                     \
     _(MathRound)                    \
     _(MathClz32)                    \
     _(MathSqrt)                     \
     _(MathATan2)                    \
diff --git a/js/src/jit/MCallOptimize.cpp b/js/src/jit/MCallOptimize.cpp
--- a/js/src/jit/MCallOptimize.cpp
+++ b/js/src/jit/MCallOptimize.cpp
@@ -123,20 +123,20 @@ IonBuilder::inlineNativeCall(CallInfo& c
 
       // Intl natives.
       case InlinableNative::IntlGuardToCollator:
         return inlineGuardToClass(callInfo, &CollatorObject::class_);
       case InlinableNative::IntlGuardToDateTimeFormat:
         return inlineGuardToClass(callInfo, &DateTimeFormatObject::class_);
       case InlinableNative::IntlGuardToNumberFormat:
         return inlineGuardToClass(callInfo, &NumberFormatObject::class_);
-      case InlinableNative::IntlIsRelativeTimeFormat:
-        return inlineHasClass(callInfo, &RelativeTimeFormatObject::class_);
       case InlinableNative::IntlGuardToPluralRules:
         return inlineGuardToClass(callInfo, &PluralRulesObject::class_);
+      case InlinableNative::IntlGuardToRelativeTimeFormat:
+        return inlineGuardToClass(callInfo, &RelativeTimeFormatObject::class_);
 
       // Math natives.
       case InlinableNative::MathAbs:
         return inlineMathAbs(callInfo);
       case InlinableNative::MathFloor:
         return inlineMathFloor(callInfo);
       case InlinableNative::MathCeil:
         return inlineMathCeil(callInfo);
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -2523,22 +2523,22 @@ static const JSFunctionSpec intrinsic_fu
                     intrinsic_GuardToBuiltin<CollatorObject>, 1,0,
                     IntlGuardToCollator),
     JS_INLINABLE_FN("GuardToDateTimeFormat",
                     intrinsic_GuardToBuiltin<DateTimeFormatObject>, 1,0,
                     IntlGuardToDateTimeFormat),
     JS_INLINABLE_FN("GuardToNumberFormat",
                     intrinsic_GuardToBuiltin<NumberFormatObject>, 1,0,
                     IntlGuardToNumberFormat),
-    JS_INLINABLE_FN("IsRelativeTimeFormat",
-                    intrinsic_IsInstanceOfBuiltin<RelativeTimeFormatObject>, 1,0,
-                    IntlIsRelativeTimeFormat),
     JS_INLINABLE_FN("GuardToPluralRules",
                     intrinsic_GuardToBuiltin<PluralRulesObject>, 1,0,
                     IntlGuardToPluralRules),
+    JS_INLINABLE_FN("GuardToRelativeTimeFormat",
+                    intrinsic_GuardToBuiltin<RelativeTimeFormatObject>, 1,0,
+                    IntlGuardToRelativeTimeFormat),
     JS_FN("GetDateTimeFormatConstructor",
           intrinsic_GetBuiltinIntlConstructor<GlobalObject::getOrCreateDateTimeFormatConstructor>,
           0,0),
     JS_FN("GetNumberFormatConstructor",
           intrinsic_GetBuiltinIntlConstructor<GlobalObject::getOrCreateNumberFormatConstructor>,
           0,0),
 
     JS_FN("GetOwnPropertyDescriptorToArray", GetOwnPropertyDescriptorToArray, 2,0),
