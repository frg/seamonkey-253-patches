# HG changeset patch
# User Jed Davis <jld@mozilla.com>
# Date 1523392560 21600
# Node ID 48d1719348251e2406619adee9dc4bee5bcd9db8
# Parent  5c98a8b877b0b3336ea9686c662b3a9d32bdd21b
Bug 1278361 - Step 2: Search-and-replace HANDLE_EINTR(close(...)) to use IGNORE_EINTR. r=froydnj

I've also manually verified that no other references to HANDLE_EINTR are
wrapping a close() in any less syntactically obvious way.

MozReview-Commit-ID: 3KkBwFIhEIq

diff --git a/ipc/chromium/src/base/dir_reader_bsd.h b/ipc/chromium/src/base/dir_reader_bsd.h
--- a/ipc/chromium/src/base/dir_reader_bsd.h
+++ b/ipc/chromium/src/base/dir_reader_bsd.h
@@ -33,17 +33,17 @@ class DirReaderBSD {
 #endif
         offset_(0),
         size_(0) {
     memset(buf_, 0, sizeof(buf_));
   }
 
   ~DirReaderBSD() {
     if (fd_ >= 0) {
-      if (HANDLE_EINTR(close(fd_)))
+      if (IGNORE_EINTR(close(fd_)))
         DLOG(ERROR) << "Failed to close directory handle";
     }
   }
 
   bool IsValid() const {
     return fd_ >= 0;
   }
 
diff --git a/ipc/chromium/src/base/dir_reader_linux.h b/ipc/chromium/src/base/dir_reader_linux.h
--- a/ipc/chromium/src/base/dir_reader_linux.h
+++ b/ipc/chromium/src/base/dir_reader_linux.h
@@ -35,17 +35,17 @@ class DirReaderLinux {
       : fd_(open(directory_path, O_RDONLY | O_DIRECTORY)),
         offset_(0),
         size_(0) {
     memset(buf_, 0, sizeof(buf_));
   }
 
   ~DirReaderLinux() {
     if (fd_ >= 0) {
-      if (HANDLE_EINTR(close(fd_)))
+      if (IGNORE_EINTR(close(fd_)))
         DLOG(ERROR) << "Failed to close directory handle";
     }
   }
 
   bool IsValid() const {
     return fd_ >= 0;
   }
 
diff --git a/ipc/chromium/src/base/file_descriptor_shuffle.cc b/ipc/chromium/src/base/file_descriptor_shuffle.cc
--- a/ipc/chromium/src/base/file_descriptor_shuffle.cc
+++ b/ipc/chromium/src/base/file_descriptor_shuffle.cc
@@ -85,12 +85,12 @@ bool FileDescriptorTableInjection::Dupli
   return *result >= 0;
 }
 
 bool FileDescriptorTableInjection::Move(int src, int dest) {
   return HANDLE_EINTR(dup2(src, dest)) != -1;
 }
 
 void FileDescriptorTableInjection::Close(int fd) {
-  HANDLE_EINTR(close(fd));
+  IGNORE_EINTR(close(fd));
 }
 
 }  // namespace base
diff --git a/ipc/chromium/src/base/file_util_posix.cc b/ipc/chromium/src/base/file_util_posix.cc
--- a/ipc/chromium/src/base/file_util_posix.cc
+++ b/ipc/chromium/src/base/file_util_posix.cc
@@ -220,39 +220,39 @@ FILE* OpenFile(const FilePath& filename,
 }
 
 int ReadFile(const FilePath& filename, char* data, int size) {
   int fd = open(filename.value().c_str(), O_RDONLY);
   if (fd < 0)
     return -1;
 
   int ret_value = HANDLE_EINTR(read(fd, data, size));
-  HANDLE_EINTR(close(fd));
+  IGNORE_EINTR(close(fd));
   return ret_value;
 }
 
 int WriteFile(const FilePath& filename, const char* data, int size) {
   int fd = creat(filename.value().c_str(), 0666);
   if (fd < 0)
     return -1;
 
   // Allow for partial writes
   ssize_t bytes_written_total = 0;
   do {
     ssize_t bytes_written_partial =
       HANDLE_EINTR(write(fd, data + bytes_written_total,
                          size - bytes_written_total));
     if (bytes_written_partial < 0) {
-      HANDLE_EINTR(close(fd));
+      IGNORE_EINTR(close(fd));
       return -1;
     }
     bytes_written_total += bytes_written_partial;
   } while (bytes_written_total < size);
 
-  HANDLE_EINTR(close(fd));
+  IGNORE_EINTR(close(fd));
   return bytes_written_total;
 }
 
 // Gets the current working directory for the process.
 bool GetCurrentDirectory(FilePath* dir) {
   char system_buffer[PATH_MAX] = "";
   if (!getcwd(system_buffer, sizeof(system_buffer))) {
     NOTREACHED();
@@ -320,18 +320,18 @@ bool CopyFile(const FilePath& from_path,
       if (bytes_written_partial < 0) {
         result = false;
         break;
       }
       bytes_written_per_read += bytes_written_partial;
     } while (bytes_written_per_read < bytes_read);
   }
 
-  if (HANDLE_EINTR(close(infile)) < 0)
+  if (IGNORE_EINTR(close(infile)) < 0)
     result = false;
-  if (HANDLE_EINTR(close(outfile)) < 0)
+  if (IGNORE_EINTR(close(outfile)) < 0)
     result = false;
 
   return result;
 }
 #endif // !defined(OS_MACOSX)
 
 } // namespace file_util
diff --git a/ipc/chromium/src/base/process_util_posix.cc b/ipc/chromium/src/base/process_util_posix.cc
--- a/ipc/chromium/src/base/process_util_posix.cc
+++ b/ipc/chromium/src/base/process_util_posix.cc
@@ -167,17 +167,17 @@ void CloseSuperfluousFds(const base::Inj
         if (fd == j->dest)
           break;
       }
       if (j != saved_mapping.end())
         continue;
 
       // Since we're just trying to close anything we can find,
       // ignore any error return values of close().
-      HANDLE_EINTR(close(fd));
+      IGNORE_EINTR(close(fd));
     }
     return;
   }
 
   const int dir_fd = fd_dir.fd();
 
   for ( ; fd_dir.Next(); ) {
     // Skip . and .. entries.
@@ -201,17 +201,17 @@ void CloseSuperfluousFds(const base::Inj
     if (fd == dir_fd)
       continue;
 
     // When running under Valgrind, Valgrind opens several FDs for its
     // own use and will complain if we try to close them.  All of
     // these FDs are >= |max_fds|, so we can check against that here
     // before closing.  See https://bugs.kde.org/show_bug.cgi?id=191758
     if (fd < static_cast<int>(max_fds)) {
-      int ret = HANDLE_EINTR(close(fd));
+      int ret = IGNORE_EINTR(close(fd));
       if (ret != 0) {
         DLOG(ERROR) << "Problem closing fd";
       }
     }
   }
 }
 
 // Sets all file descriptors to close on exec except for stdin, stdout
diff --git a/ipc/chromium/src/chrome/common/file_descriptor_set_posix.cc b/ipc/chromium/src/chrome/common/file_descriptor_set_posix.cc
--- a/ipc/chromium/src/chrome/common/file_descriptor_set_posix.cc
+++ b/ipc/chromium/src/chrome/common/file_descriptor_set_posix.cc
@@ -26,17 +26,17 @@ FileDescriptorSet::~FileDescriptorSet() 
   //
   // If this message was received with more descriptors than expected
   // (which could a DOS against the browser by a rogue renderer) then all
   // the descriptors have their close flag set and we free all the extra
   // kernel resources.
   for (unsigned i = consumed_descriptor_highwater_;
        i < descriptors_.size(); ++i) {
     if (descriptors_[i].auto_close)
-      HANDLE_EINTR(close(descriptors_[i].fd));
+      IGNORE_EINTR(close(descriptors_[i].fd));
   }
 }
 
 bool FileDescriptorSet::Add(int fd) {
   if (descriptors_.size() == MAX_DESCRIPTORS_PER_MESSAGE)
     return false;
 
   struct base::FileDescriptor sd;
@@ -97,17 +97,17 @@ void FileDescriptorSet::GetDescriptors(i
     *(buffer++) = i->fd;
   }
 }
 
 void FileDescriptorSet::CommitAll() {
   for (std::vector<base::FileDescriptor>::iterator
        i = descriptors_.begin(); i != descriptors_.end(); ++i) {
     if (i->auto_close)
-      HANDLE_EINTR(close(i->fd));
+      IGNORE_EINTR(close(i->fd));
   }
   descriptors_.clear();
   consumed_descriptor_highwater_ = 0;
 }
 
 void FileDescriptorSet::SetDescriptors(const int* buffer, unsigned count) {
   DCHECK_LE(count, MAX_DESCRIPTORS_PER_MESSAGE);
   DCHECK_EQ(descriptors_.size(), 0u);
diff --git a/ipc/chromium/src/chrome/common/ipc_channel_posix.cc b/ipc/chromium/src/chrome/common/ipc_channel_posix.cc
--- a/ipc/chromium/src/chrome/common/ipc_channel_posix.cc
+++ b/ipc/chromium/src/chrome/common/ipc_channel_posix.cc
@@ -250,26 +250,26 @@ bool Channel::ChannelImpl::CreatePipe(co
     if (socketpair(AF_UNIX, SOCK_STREAM, 0, pipe_fds) != 0) {
       mozilla::ipc::AnnotateCrashReportWithErrno("IpcCreatePipeSocketPairErrno", errno);
       return false;
     }
     // Set both ends to be non-blocking.
     if (fcntl(pipe_fds[0], F_SETFL, O_NONBLOCK) == -1 ||
         fcntl(pipe_fds[1], F_SETFL, O_NONBLOCK) == -1) {
       mozilla::ipc::AnnotateCrashReportWithErrno("IpcCreatePipeFcntlErrno", errno);
-      HANDLE_EINTR(close(pipe_fds[0]));
-      HANDLE_EINTR(close(pipe_fds[1]));
+      IGNORE_EINTR(close(pipe_fds[0]));
+      IGNORE_EINTR(close(pipe_fds[1]));
       return false;
     }
 
     if (!SetCloseOnExec(pipe_fds[0]) ||
         !SetCloseOnExec(pipe_fds[1])) {
       mozilla::ipc::AnnotateCrashReportWithErrno("IpcCreatePipeCloExecErrno", errno);
-      HANDLE_EINTR(close(pipe_fds[0]));
-      HANDLE_EINTR(close(pipe_fds[1]));
+      IGNORE_EINTR(close(pipe_fds[0]));
+      IGNORE_EINTR(close(pipe_fds[1]));
       return false;
     }
 
     pipe_ = pipe_fds[0];
     client_pipe_ = pipe_fds[1];
 
     if (pipe_name_.length()) {
       Singleton<PipeMap>()->Insert(pipe_name_, client_pipe_);
@@ -357,17 +357,17 @@ bool Channel::ChannelImpl::ProcessIncomi
       // The pipe has closed...
       Close();
       return false;
     }
     DCHECK(bytes_read);
 
     if (client_pipe_ != -1) {
       Singleton<PipeMap>()->Remove(pipe_name_);
-      HANDLE_EINTR(close(client_pipe_));
+      IGNORE_EINTR(close(client_pipe_));
       client_pipe_ = -1;
     }
 
     // a pointer to an array of |num_wire_fds| file descriptors from the read
     const int* wire_fds = NULL;
     unsigned num_wire_fds = 0;
 
     // walk the list of control messages and, if we find an array of file
@@ -396,17 +396,17 @@ bool Channel::ChannelImpl::ProcessIncomi
           wire_fds = reinterpret_cast<int*>(CMSG_DATA(cmsg));
           num_wire_fds = payload_len / 4;
 
           if (msg.msg_flags & MSG_CTRUNC) {
             CHROMIUM_LOG(ERROR) << "SCM_RIGHTS message was truncated"
                                 << " cmsg_len:" << cmsg->cmsg_len
                                 << " fd:" << pipe_;
             for (unsigned i = 0; i < num_wire_fds; ++i)
-              HANDLE_EINTR(close(wire_fds[i]));
+              IGNORE_EINTR(close(wire_fds[i]));
             return false;
           }
           break;
         }
       }
     }
 
     // Process messages from input buffer.
@@ -532,17 +532,17 @@ bool Channel::ChannelImpl::ProcessIncomi
           CHROMIUM_LOG(WARNING) << error
                                 << " channel:" << this
                                 << " message-type:" << m.type()
                                 << " header()->num_fds:" << m.header()->num_fds
                                 << " num_fds:" << num_fds
                                 << " fds_i:" << fds_i;
           // close the existing file descriptors so that we don't leak them
           for (unsigned i = fds_i; i < num_fds; ++i)
-            HANDLE_EINTR(close(fds[i]));
+            IGNORE_EINTR(close(fds[i]));
           input_overflow_fds_.clear();
           // abort the connection
           return false;
         }
 
 #if defined(OS_MACOSX)
         // Send a message to the other side, indicating that we are now
         // responsible for closing the descriptor.
@@ -801,17 +801,17 @@ void Channel::ChannelImpl::GetClientFile
   DCHECK(mode_ == MODE_SERVER);
   *src_fd = client_pipe_;
   *dest_fd = gClientChannelFd;
 }
 
 void Channel::ChannelImpl::CloseClientFileDescriptor() {
   if (client_pipe_ != -1) {
     Singleton<PipeMap>()->Remove(pipe_name_);
-    HANDLE_EINTR(close(client_pipe_));
+    IGNORE_EINTR(close(client_pipe_));
     client_pipe_ = -1;
   }
 }
 
 // Called by libevent when we can read from th pipe without blocking.
 void Channel::ChannelImpl::OnFileCanReadWithoutBlocking(int fd) {
   if (!waiting_connect_ && fd == pipe_) {
     if (!ProcessIncomingMessages()) {
@@ -863,43 +863,43 @@ void Channel::ChannelImpl::OnFileCanWrit
 void Channel::ChannelImpl::Close() {
   // Close can be called multiple times, so we need to make sure we're
   // idempotent.
 
   // Unregister libevent for the listening socket and close it.
   server_listen_connection_watcher_.StopWatchingFileDescriptor();
 
   if (server_listen_pipe_ != -1) {
-    HANDLE_EINTR(close(server_listen_pipe_));
+    IGNORE_EINTR(close(server_listen_pipe_));
     server_listen_pipe_ = -1;
   }
 
   // Unregister libevent for the FIFO and close it.
   read_watcher_.StopWatchingFileDescriptor();
   write_watcher_.StopWatchingFileDescriptor();
   if (pipe_ != -1) {
-    HANDLE_EINTR(close(pipe_));
+    IGNORE_EINTR(close(pipe_));
     pipe_ = -1;
   }
   if (client_pipe_ != -1) {
     Singleton<PipeMap>()->Remove(pipe_name_);
-    HANDLE_EINTR(close(client_pipe_));
+    IGNORE_EINTR(close(client_pipe_));
     client_pipe_ = -1;
   }
 
   while (!output_queue_.empty()) {
     Message* m = output_queue_.front();
     OutputQueuePop();
     delete m;
   }
 
   // Close any outstanding, received file descriptors
   for (std::vector<int>::iterator
        i = input_overflow_fds_.begin(); i != input_overflow_fds_.end(); ++i) {
-    HANDLE_EINTR(close(*i));
+    IGNORE_EINTR(close(*i));
   }
   input_overflow_fds_.clear();
 
 #if defined(OS_MACOSX)
   for (std::list<PendingDescriptors>::iterator i = pending_fds_.begin();
        i != pending_fds_.end();
        i++) {
     (*i).fds->CommitAll();
diff --git a/ipc/glue/FileDescriptor.cpp b/ipc/glue/FileDescriptor.cpp
--- a/ipc/glue/FileDescriptor.cpp
+++ b/ipc/glue/FileDescriptor.cpp
@@ -182,17 +182,17 @@ void
 FileDescriptor::Close(PlatformHandleType aHandle)
 {
   if (IsValid(aHandle)) {
 #ifdef XP_WIN
     if (!CloseHandle(aHandle)) {
       NS_WARNING("Failed to close file handle for current process!");
     }
 #else // XP_WIN
-    HANDLE_EINTR(close(aHandle));
+    IGNORE_EINTR(close(aHandle));
 #endif
   }
 }
 
 FileDescriptor::PlatformHandleHelper::PlatformHandleHelper(FileDescriptor::PlatformHandleType aHandle)
   :mHandle(aHandle)
 {
 }
diff --git a/ipc/glue/Transport_posix.cpp b/ipc/glue/Transport_posix.cpp
--- a/ipc/glue/Transport_posix.cpp
+++ b/ipc/glue/Transport_posix.cpp
@@ -43,18 +43,18 @@ CreateTransport(base::ProcessId aProcIdO
     AnnotateCrashReportWithErrno("IpcCreateTransportDupErrno", errno);
   }
   fd2 = dup(fd2);
   if (fd2 < 0) {
     AnnotateCrashReportWithErrno("IpcCreateTransportDupErrno", errno);
   }
 
   if (fd1 < 0 || fd2 < 0) {
-    HANDLE_EINTR(close(fd1));
-    HANDLE_EINTR(close(fd2));
+    IGNORE_EINTR(close(fd1));
+    IGNORE_EINTR(close(fd2));
     return NS_ERROR_DUPLICATE_HANDLE;
   }
 
   aOne->mFd = base::FileDescriptor(fd1, true/*close after sending*/);
   aTwo->mFd = base::FileDescriptor(fd2, true/*close after sending*/);
   return NS_OK;
 }
 
