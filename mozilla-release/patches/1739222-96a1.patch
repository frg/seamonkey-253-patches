# HG changeset patch
# User Ryan VanderMeulen <ryanvm@gmail.com>
# Date 1635982599 0
#      Wed Nov 03 23:36:39 2021 +0000
# Node ID 427c15f9b1f5ab2b7af9ff72d743bb9f88f6e9a1
# Parent  dfd297590078a5e72f39a55fd220e5cefa872ff8
Bug 1739222 - Update jsoncpp to version 1.9.5. r=gsvelto

Differential Revision: https://phabricator.services.mozilla.com/D130307

diff --git a/toolkit/components/jsoncpp/GIT-INFO b/toolkit/components/jsoncpp/GIT-INFO
--- a/toolkit/components/jsoncpp/GIT-INFO
+++ b/toolkit/components/jsoncpp/GIT-INFO
@@ -1,1 +1,1 @@
-9059f5cad030ba11d37818847443a53918c327b1
+5defb4ed1a4293b8e2bf641e16b156fb9de498cc
diff --git a/toolkit/components/jsoncpp/LICENSE b/toolkit/components/jsoncpp/LICENSE
--- a/toolkit/components/jsoncpp/LICENSE
+++ b/toolkit/components/jsoncpp/LICENSE
@@ -1,30 +1,30 @@
-The JsonCpp library's source code, including accompanying documentation, 
+The JsonCpp library's source code, including accompanying documentation,
 tests and demonstration applications, are licensed under the following
 conditions...
 
-Baptiste Lepilleur and The JsonCpp Authors explicitly disclaim copyright in all 
-jurisdictions which recognize such a disclaimer. In such jurisdictions, 
+Baptiste Lepilleur and The JsonCpp Authors explicitly disclaim copyright in all
+jurisdictions which recognize such a disclaimer. In such jurisdictions,
 this software is released into the Public Domain.
 
 In jurisdictions which do not recognize Public Domain property (e.g. Germany as of
 2010), this software is Copyright (c) 2007-2010 by Baptiste Lepilleur and
 The JsonCpp Authors, and is released under the terms of the MIT License (see below).
 
-In jurisdictions which recognize Public Domain property, the user of this 
-software may choose to accept it either as 1) Public Domain, 2) under the 
-conditions of the MIT License (see below), or 3) under the terms of dual 
+In jurisdictions which recognize Public Domain property, the user of this
+software may choose to accept it either as 1) Public Domain, 2) under the
+conditions of the MIT License (see below), or 3) under the terms of dual
 Public Domain/MIT License conditions described here, as they choose.
 
 The MIT License is about as close to Public Domain as a license can get, and is
 described in clear, concise terms at:
 
    http://en.wikipedia.org/wiki/MIT_License
-   
+
 The full text of the MIT License follows:
 
 ========================================================================
 Copyright (c) 2007-2010 Baptiste Lepilleur and The JsonCpp Authors
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
diff --git a/toolkit/components/jsoncpp/include/json/allocator.h b/toolkit/components/jsoncpp/include/json/allocator.h
--- a/toolkit/components/jsoncpp/include/json/allocator.h
+++ b/toolkit/components/jsoncpp/include/json/allocator.h
@@ -30,21 +30,20 @@ public:
     // allocate using "global operator new"
     return static_cast<pointer>(::operator new(n * sizeof(T)));
   }
 
   /**
    * Release memory which was allocated for N items at pointer P.
    *
    * The memory block is filled with zeroes before being released.
-   * The pointer argument is tagged as "volatile" to prevent the
-   * compiler optimizing out this critical step.
    */
-  void deallocate(volatile pointer p, size_type n) {
-    std::memset(p, 0, n * sizeof(T));
+  void deallocate(pointer p, size_type n) {
+    // memset_s is used because memset may be optimized away by the compiler
+    memset_s(p, n * sizeof(T), 0, n * sizeof(T));
     // free using "global operator delete"
     ::operator delete(p);
   }
 
   /**
    * Construct an item in-place at pointer P.
    */
   template <typename... Args> void construct(pointer p, Args&&... args) {
diff --git a/toolkit/components/jsoncpp/include/json/reader.h b/toolkit/components/jsoncpp/include/json/reader.h
--- a/toolkit/components/jsoncpp/include/json/reader.h
+++ b/toolkit/components/jsoncpp/include/json/reader.h
@@ -28,41 +28,40 @@
 namespace Json {
 
 /** \brief Unserialize a <a HREF="http://www.json.org">JSON</a> document into a
  * Value.
  *
  * \deprecated Use CharReader and CharReaderBuilder.
  */
 
-class JSONCPP_DEPRECATED(
-    "Use CharReader and CharReaderBuilder instead.") JSON_API Reader {
+class JSON_API Reader {
 public:
   using Char = char;
   using Location = const Char*;
 
   /** \brief An error tagged with where in the JSON text it was encountered.
    *
    * The offsets give the [start, limit) range of bytes within the text. Note
    * that this is bytes, not codepoints.
    */
   struct StructuredError {
     ptrdiff_t offset_start;
     ptrdiff_t offset_limit;
     String message;
   };
 
   /** \brief Constructs a Reader allowing all features for parsing.
+    * \deprecated Use CharReader and CharReaderBuilder.
    */
-  JSONCPP_DEPRECATED("Use CharReader and CharReaderBuilder instead")
   Reader();
 
   /** \brief Constructs a Reader allowing the specified feature set for parsing.
+    * \deprecated Use CharReader and CharReaderBuilder.
    */
-  JSONCPP_DEPRECATED("Use CharReader and CharReaderBuilder instead")
   Reader(const Features& features);
 
   /** \brief Read a Value from a <a HREF="http://www.json.org">JSON</a>
    * document.
    *
    * \param      document        UTF-8 encoded string containing the document
    *                             to read.
    * \param[out] root            Contains the root value of the document if it
@@ -319,16 +318,19 @@ public:
    *   - If true, `parse()` returns false when extra non-whitespace trails the
    *     JSON value in the input string.
    * - `"rejectDupKeys": false or true`
    *   - If true, `parse()` returns false when a key is duplicated within an
    *     object.
    * - `"allowSpecialFloats": false or true`
    *   - If true, special float values (NaNs and infinities) are allowed and
    *     their values are lossfree restorable.
+   * - `"skipBom": false or true`
+   *   - If true, if the input starts with the Unicode byte order mark (BOM),
+   *     it is skipped.
    *
    * You can examine 'settings_` yourself to see the defaults. You can also
    * write and read them just like any JSON Value.
    * \sa setDefaults()
    */
   Json::Value settings_;
 
   CharReaderBuilder();
diff --git a/toolkit/components/jsoncpp/include/json/value.h b/toolkit/components/jsoncpp/include/json/value.h
--- a/toolkit/components/jsoncpp/include/json/value.h
+++ b/toolkit/components/jsoncpp/include/json/value.h
@@ -45,17 +45,17 @@
 #include <memory>
 #include <string>
 #include <vector>
 
 // Disable warning C4251: <data member>: <type> needs to have dll-interface to
 // be used by...
 #if defined(JSONCPP_DISABLE_DLL_INTERFACE_WARNING)
 #pragma warning(push)
-#pragma warning(disable : 4251)
+#pragma warning(disable : 4251 4275)
 #endif // if defined(JSONCPP_DISABLE_DLL_INTERFACE_WARNING)
 
 #pragma pack(push, 8)
 
 /** \brief JSON (JavaScript Object Notation).
  */
 namespace Json {
 
@@ -258,20 +258,20 @@ private:
 #endif
 #ifndef JSONCPP_DOC_EXCLUDE_IMPLEMENTATION
   class CZString {
   public:
     enum DuplicationPolicy { noDuplication = 0, duplicate, duplicateOnCopy };
     CZString(ArrayIndex index);
     CZString(char const* str, unsigned length, DuplicationPolicy allocate);
     CZString(CZString const& other);
-    CZString(CZString&& other);
+    CZString(CZString&& other) noexcept;
     ~CZString();
     CZString& operator=(const CZString& other);
-    CZString& operator=(CZString&& other);
+    CZString& operator=(CZString&& other) noexcept;
 
     bool operator<(CZString const& other) const;
     bool operator==(CZString const& other) const;
     ArrayIndex index() const;
     // const char* c_str() const; ///< \deprecated
     char const* data() const;
     unsigned length() const;
     bool isStaticString() const;
@@ -339,23 +339,23 @@ public:
    *   Json::Value aValue(foo);
    *   \endcode
    */
   Value(const StaticString& value);
   Value(const String& value);
   Value(bool value);
   Value(std::nullptr_t ptr) = delete;
   Value(const Value& other);
-  Value(Value&& other);
+  Value(Value&& other) noexcept;
   ~Value();
 
   /// \note Overwrite existing comments. To preserve comments, use
   /// #swapPayload().
   Value& operator=(const Value& other);
-  Value& operator=(Value&& other);
+  Value& operator=(Value&& other) noexcept;
 
   /// Swap everything.
   void swap(Value& other);
   /// Swap values but leave comments and source offsets in place.
   void swapPayload(Value& other);
 
   /// copy everything.
   void copy(const Value& other);
@@ -630,19 +630,19 @@ private:
     // Unless allocated_, string_ must be null-terminated.
     unsigned int allocated_ : 1;
   } bits_;
 
   class Comments {
   public:
     Comments() = default;
     Comments(const Comments& that);
-    Comments(Comments&& that);
+    Comments(Comments&& that) noexcept;
     Comments& operator=(const Comments& that);
-    Comments& operator=(Comments&& that);
+    Comments& operator=(Comments&& that) noexcept;
     bool has(CommentPlacement slot) const;
     String get(CommentPlacement slot) const;
     void set(CommentPlacement slot, String comment);
 
   private:
     using Array = std::array<String, numberOfCommentPlacement>;
     std::unique_ptr<Array> ptr_;
   };
@@ -913,18 +913,18 @@ public:
     return *this;
   }
 
   /*! The return value of non-const iterators can be
    *  changed, so the these functions are not const
    *  because the returned references/pointers can be used
    *  to change state of the base class.
    */
-  reference operator*() { return deref(); }
-  pointer operator->() { return &deref(); }
+  reference operator*() const { return const_cast<reference>(deref()); }
+  pointer operator->() const { return const_cast<pointer>(&deref()); }
 };
 
 inline void swap(Value& a, Value& b) { a.swap(b); }
 
 } // namespace Json
 
 #pragma pack(pop)
 
diff --git a/toolkit/components/jsoncpp/include/json/version.h b/toolkit/components/jsoncpp/include/json/version.h
--- a/toolkit/components/jsoncpp/include/json/version.h
+++ b/toolkit/components/jsoncpp/include/json/version.h
@@ -4,20 +4,20 @@
 // Note: version must be updated in three places when doing a release. This
 // annoying process ensures that amalgamate, CMake, and meson all report the
 // correct version.
 // 1. /meson.build
 // 2. /include/json/version.h
 // 3. /CMakeLists.txt
 // IMPORTANT: also update the SOVERSION!!
 
-#define JSONCPP_VERSION_STRING "1.9.4"
+#define JSONCPP_VERSION_STRING "1.9.5"
 #define JSONCPP_VERSION_MAJOR 1
 #define JSONCPP_VERSION_MINOR 9
-#define JSONCPP_VERSION_PATCH 3
+#define JSONCPP_VERSION_PATCH 5
 #define JSONCPP_VERSION_QUALIFIER
 #define JSONCPP_VERSION_HEXA                                                   \
   ((JSONCPP_VERSION_MAJOR << 24) | (JSONCPP_VERSION_MINOR << 16) |             \
    (JSONCPP_VERSION_PATCH << 8))
 
 #ifdef JSONCPP_USING_SECURE_MEMORY
 #undef JSONCPP_USING_SECURE_MEMORY
 #endif
diff --git a/toolkit/components/jsoncpp/include/json/writer.h b/toolkit/components/jsoncpp/include/json/writer.h
--- a/toolkit/components/jsoncpp/include/json/writer.h
+++ b/toolkit/components/jsoncpp/include/json/writer.h
@@ -105,16 +105,18 @@ public:
    *  - "useSpecialFloats": false or true
    *  - If true, outputs non-finite floating point values in the following way:
    *    NaN values as "NaN", positive infinity as "Infinity", and negative
    *  infinity as "-Infinity".
    *  - "precision": int
    *  - Number of precision digits for formatting of real values.
    *  - "precisionType": "significant"(default) or "decimal"
    *  - Type of precision for formatting of real values.
+   *  - "emitUTF8": false or true
+   *  - If true, outputs raw UTF8 strings instead of escaping them.
 
    *  You can examine 'settings_` yourself
    *  to see the defaults. You can also write and read them just like any
    *  JSON Value.
    *  \sa setDefaults()
    */
   Json::Value settings_;
 
@@ -140,17 +142,17 @@ public:
    * \snippet src/lib_json/json_writer.cpp StreamWriterBuilderDefaults
    */
   static void setDefaults(Json::Value* settings);
 };
 
 /** \brief Abstract class for writers.
  * \deprecated Use StreamWriter. (And really, this is an implementation detail.)
  */
-class JSONCPP_DEPRECATED("Use StreamWriter instead") JSON_API Writer {
+class JSON_API Writer {
 public:
   virtual ~Writer();
 
   virtual String write(const Value& root) = 0;
 };
 
 /** \brief Outputs a Value in <a HREF="http://www.json.org">JSON</a> format
  *without formatting (not human friendly).
@@ -160,17 +162,17 @@ public:
  * but may be useful to support feature such as RPC where bandwidth is limited.
  * \sa Reader, Value
  * \deprecated Use StreamWriterBuilder.
  */
 #if defined(_MSC_VER)
 #pragma warning(push)
 #pragma warning(disable : 4996) // Deriving from deprecated class
 #endif
-class JSONCPP_DEPRECATED("Use StreamWriterBuilder instead") JSON_API FastWriter
+class JSON_API FastWriter
     : public Writer {
 public:
   FastWriter();
   ~FastWriter() override = default;
 
   void enableYAMLCompatibility();
 
   /** \brief Drop the "null" string from the writer's output for nullValues.
@@ -220,17 +222,17 @@ private:
  *
  * \sa Reader, Value, Value::setComment()
  * \deprecated Use StreamWriterBuilder.
  */
 #if defined(_MSC_VER)
 #pragma warning(push)
 #pragma warning(disable : 4996) // Deriving from deprecated class
 #endif
-class JSONCPP_DEPRECATED("Use StreamWriterBuilder instead") JSON_API
+class JSON_API
     StyledWriter : public Writer {
 public:
   StyledWriter();
   ~StyledWriter() override = default;
 
 public: // overridden from Writer
   /** \brief Serialize a Value in <a HREF="http://www.json.org">JSON</a> format.
    * \param root Value to serialize.
@@ -289,17 +291,17 @@ private:
  *
  * \sa Reader, Value, Value::setComment()
  * \deprecated Use StreamWriterBuilder.
  */
 #if defined(_MSC_VER)
 #pragma warning(push)
 #pragma warning(disable : 4996) // Deriving from deprecated class
 #endif
-class JSONCPP_DEPRECATED("Use StreamWriterBuilder instead") JSON_API
+class JSON_API
     StyledStreamWriter {
 public:
   /**
    * \param indentation Each level will be indented by this amount extra.
    */
   StyledStreamWriter(String indentation = "\t");
   ~StyledStreamWriter() = default;
 
diff --git a/toolkit/components/jsoncpp/src/lib_json/json_reader.cpp b/toolkit/components/jsoncpp/src/lib_json/json_reader.cpp
--- a/toolkit/components/jsoncpp/src/lib_json/json_reader.cpp
+++ b/toolkit/components/jsoncpp/src/lib_json/json_reader.cpp
@@ -99,18 +99,17 @@ bool Reader::parse(const std::string& do
 bool Reader::parse(std::istream& is, Value& root, bool collectComments) {
   // std::istream_iterator<char> begin(is);
   // std::istream_iterator<char> end;
   // Those would allow streamed input from a file, if parse() were a
   // template function.
 
   // Since String is reference-counted, this at least does not
   // create an extra copy.
-  String doc;
-  std::getline(is, doc, static_cast<char> EOF);
+  String doc(std::istreambuf_iterator<char>(is), {});
   return parse(doc.data(), doc.data() + doc.size(), root, collectComments);
 }
 
 bool Reader::parse(const char* beginDoc, const char* endDoc, Value& root,
                    bool collectComments) {
   if (!features_.allowComments_) {
     collectComments = false;
   }
@@ -1916,17 +1915,17 @@ bool CharReaderBuilder::validate(Json::V
       "allowSpecialFloats",
       "skipBom",
   };
   for (auto si = settings_.begin(); si != settings_.end(); ++si) {
     auto key = si.name();
     if (valid_keys.count(key))
       continue;
     if (invalid)
-      (*invalid)[std::move(key)] = *si;
+      (*invalid)[key] = *si;
     else
       return false;
   }
   return invalid ? invalid->empty() : true;
 }
 
 Value& CharReaderBuilder::operator[](const String& key) {
   return settings_[key];
diff --git a/toolkit/components/jsoncpp/src/lib_json/json_tool.h b/toolkit/components/jsoncpp/src/lib_json/json_tool.h
--- a/toolkit/components/jsoncpp/src/lib_json/json_tool.h
+++ b/toolkit/components/jsoncpp/src/lib_json/json_tool.h
@@ -111,24 +111,28 @@ template <typename Iter> void fixNumeric
     }
   }
 }
 
 /**
  * Return iterator that would be the new end of the range [begin,end), if we
  * were to delete zeros in the end of string, but not the last zero before '.'.
  */
-template <typename Iter> Iter fixZerosInTheEnd(Iter begin, Iter end) {
+template <typename Iter>
+Iter fixZerosInTheEnd(Iter begin, Iter end, unsigned int precision) {
   for (; begin != end; --end) {
     if (*(end - 1) != '0') {
       return end;
     }
     // Don't delete the last zero before the decimal point.
-    if (begin != (end - 1) && *(end - 2) == '.') {
-      return end;
+    if (begin != (end - 1) && begin != (end - 2) && *(end - 2) == '.') {
+      if (precision) {
+        return end;
+      }
+      return end - 2;
     }
   }
   return end;
 }
 
 } // namespace Json
 
 #endif // LIB_JSONCPP_JSON_TOOL_H_INCLUDED
diff --git a/toolkit/components/jsoncpp/src/lib_json/json_value.cpp b/toolkit/components/jsoncpp/src/lib_json/json_value.cpp
--- a/toolkit/components/jsoncpp/src/lib_json/json_value.cpp
+++ b/toolkit/components/jsoncpp/src/lib_json/json_value.cpp
@@ -254,17 +254,17 @@ Value::CZString::CZString(const CZString
                          noDuplication
                      ? noDuplication
                      : duplicate)
               : static_cast<DuplicationPolicy>(other.storage_.policy_)) &
       3U;
   storage_.length_ = other.storage_.length_;
 }
 
-Value::CZString::CZString(CZString&& other)
+Value::CZString::CZString(CZString&& other) noexcept
     : cstr_(other.cstr_), index_(other.index_) {
   other.cstr_ = nullptr;
 }
 
 Value::CZString::~CZString() {
   if (cstr_ && storage_.policy_ == duplicate) {
     releaseStringValue(const_cast<char*>(cstr_),
                        storage_.length_ + 1U); // +1 for null terminating
@@ -280,17 +280,17 @@ void Value::CZString::swap(CZString& oth
 }
 
 Value::CZString& Value::CZString::operator=(const CZString& other) {
   cstr_ = other.cstr_;
   index_ = other.index_;
   return *this;
 }
 
-Value::CZString& Value::CZString::operator=(CZString&& other) {
+Value::CZString& Value::CZString::operator=(CZString&& other) noexcept {
   cstr_ = other.cstr_;
   index_ = other.index_;
   other.cstr_ = nullptr;
   return *this;
 }
 
 bool Value::CZString::operator<(const CZString& other) const {
   if (!cstr_)
@@ -428,32 +428,32 @@ Value::Value(bool value) {
   value_.bool_ = value;
 }
 
 Value::Value(const Value& other) {
   dupPayload(other);
   dupMeta(other);
 }
 
-Value::Value(Value&& other) {
+Value::Value(Value&& other) noexcept {
   initBasic(nullValue);
   swap(other);
 }
 
 Value::~Value() {
   releasePayload();
   value_.uint_ = 0;
 }
 
 Value& Value::operator=(const Value& other) {
   Value(other).swap(*this);
   return *this;
 }
 
-Value& Value::operator=(Value&& other) {
+Value& Value::operator=(Value&& other) noexcept {
   other.swap(*this);
   return *this;
 }
 
 void Value::swapPayload(Value& other) {
   std::swap(bits_, other.bits_);
   std::swap(value_, other.value_);
 }
@@ -907,17 +907,18 @@ void Value::resize(ArrayIndex newSize) {
   JSON_ASSERT_MESSAGE(type() == nullValue || type() == arrayValue,
                       "in Json::Value::resize(): requires arrayValue");
   if (type() == nullValue)
     *this = Value(arrayValue);
   ArrayIndex oldSize = size();
   if (newSize == 0)
     clear();
   else if (newSize > oldSize)
-    this->operator[](newSize - 1);
+    for (ArrayIndex i = oldSize; i < newSize; ++i)
+      (*this)[i];
   else {
     for (ArrayIndex index = newSize; index < oldSize; ++index) {
       value_.map_->erase(index);
     }
     JSON_ASSERT(size() == newSize);
   }
 }
 
@@ -1368,46 +1369,45 @@ bool Value::isString() const { return ty
 
 bool Value::isArray() const { return type() == arrayValue; }
 
 bool Value::isObject() const { return type() == objectValue; }
 
 Value::Comments::Comments(const Comments& that)
     : ptr_{cloneUnique(that.ptr_)} {}
 
-Value::Comments::Comments(Comments&& that) : ptr_{std::move(that.ptr_)} {}
+Value::Comments::Comments(Comments&& that) noexcept
+    : ptr_{std::move(that.ptr_)} {}
 
 Value::Comments& Value::Comments::operator=(const Comments& that) {
   ptr_ = cloneUnique(that.ptr_);
   return *this;
 }
 
-Value::Comments& Value::Comments::operator=(Comments&& that) {
+Value::Comments& Value::Comments::operator=(Comments&& that) noexcept {
   ptr_ = std::move(that.ptr_);
   return *this;
 }
 
 bool Value::Comments::has(CommentPlacement slot) const {
   return ptr_ && !(*ptr_)[slot].empty();
 }
 
 String Value::Comments::get(CommentPlacement slot) const {
   if (!ptr_)
     return {};
   return (*ptr_)[slot];
 }
 
 void Value::Comments::set(CommentPlacement slot, String comment) {
-  if (!ptr_) {
+  if (slot >= CommentPlacement::numberOfCommentPlacement)
+    return;
+  if (!ptr_)
     ptr_ = std::unique_ptr<Array>(new Array());
-  }
-  // check comments array boundry.
-  if (slot < CommentPlacement::numberOfCommentPlacement) {
-    (*ptr_)[slot] = std::move(comment);
-  }
+  (*ptr_)[slot] = std::move(comment);
 }
 
 void Value::setComment(String comment, CommentPlacement placement) {
   if (!comment.empty() && (comment.back() == '\n')) {
     // Always discard trailing newline, to aid indentation.
     comment.pop_back();
   }
   JSON_ASSERT(!comment.empty());
diff --git a/toolkit/components/jsoncpp/src/lib_json/json_writer.cpp b/toolkit/components/jsoncpp/src/lib_json/json_writer.cpp
--- a/toolkit/components/jsoncpp/src/lib_json/json_writer.cpp
+++ b/toolkit/components/jsoncpp/src/lib_json/json_writer.cpp
@@ -63,17 +63,17 @@
 #define isfinite(x)                                                            \
   ((sizeof(x) == sizeof(float) ? _Isfinitef(x) : _IsFinite(x)))
 #endif
 #endif
 #endif
 
 #if !defined(isnan)
 // IEEE standard states that NaN values will not compare to themselves
-#define isnan(x) (x != x)
+#define isnan(x) ((x) != (x))
 #endif
 
 #if !defined(__APPLE__)
 #if !defined(isfinite)
 #define isfinite finite
 #endif
 #endif
 #endif
@@ -149,26 +149,28 @@ String valueToString(double value, bool 
       continue;
     }
     buffer.resize(wouldPrint);
     break;
   }
 
   buffer.erase(fixNumericLocale(buffer.begin(), buffer.end()), buffer.end());
 
-  // strip the zero padding from the right
-  if (precisionType == PrecisionType::decimalPlaces) {
-    buffer.erase(fixZerosInTheEnd(buffer.begin(), buffer.end()), buffer.end());
-  }
-
   // try to ensure we preserve the fact that this was given to us as a double on
   // input
   if (buffer.find('.') == buffer.npos && buffer.find('e') == buffer.npos) {
     buffer += ".0";
   }
+
+  // strip the zero padding from the right
+  if (precisionType == PrecisionType::decimalPlaces) {
+    buffer.erase(fixZerosInTheEnd(buffer.begin(), buffer.end(), precision),
+                 buffer.end());
+  }
+
   return buffer;
 }
 } // namespace
 
 String valueToString(double value, unsigned int precision,
                      PrecisionType precisionType) {
   return valueToString(value, false, precision, precisionType);
 }
@@ -265,17 +267,17 @@ static String toHex16Bit(unsigned int x)
 static void appendRaw(String& result, unsigned ch) {
   result += static_cast<char>(ch);
 }
 
 static void appendHex(String& result, unsigned ch) {
   result.append("\\u").append(toHex16Bit(ch));
 }
 
-static String valueToQuotedStringN(const char* value, unsigned length,
+static String valueToQuotedStringN(const char* value, size_t length,
                                    bool emitUTF8 = false) {
   if (value == nullptr)
     return "";
 
   if (!doesAnyCharRequireEscaping(value, length))
     return String("\"") + value + "\"";
   // We have to walk value and escape any special characters.
   // Appending to String is not efficient, but this should be rare.
@@ -343,17 +345,17 @@ static String valueToQuotedStringN(const
     } break;
     }
   }
   result += "\"";
   return result;
 }
 
 String valueToQuotedString(const char* value) {
-  return valueToQuotedStringN(value, static_cast<unsigned int>(strlen(value)));
+  return valueToQuotedStringN(value, strlen(value));
 }
 
 // Class Writer
 // //////////////////////////////////////////////////////////////////
 Writer::~Writer() = default;
 
 // Class FastWriter
 // //////////////////////////////////////////////////////////////////
@@ -392,17 +394,17 @@ void FastWriter::writeValue(const Value&
     document_ += valueToString(value.asDouble());
     break;
   case stringValue: {
     // Is NULL possible for value.string_? No.
     char const* str;
     char const* end;
     bool ok = value.getString(&str, &end);
     if (ok)
-      document_ += valueToQuotedStringN(str, static_cast<unsigned>(end - str));
+      document_ += valueToQuotedStringN(str, static_cast<size_t>(end - str));
     break;
   }
   case booleanValue:
     document_ += valueToString(value.asBool());
     break;
   case arrayValue: {
     document_ += '[';
     ArrayIndex size = value.size();
@@ -415,18 +417,17 @@ void FastWriter::writeValue(const Value&
   } break;
   case objectValue: {
     Value::Members members(value.getMemberNames());
     document_ += '{';
     for (auto it = members.begin(); it != members.end(); ++it) {
       const String& name = *it;
       if (it != members.begin())
         document_ += ',';
-      document_ += valueToQuotedStringN(name.data(),
-                                        static_cast<unsigned>(name.length()));
+      document_ += valueToQuotedStringN(name.data(), name.length());
       document_ += yamlCompatibilityEnabled_ ? ": " : ":";
       writeValue(value[name]);
     }
     document_ += '}';
   } break;
   }
 }
 
@@ -461,17 +462,17 @@ void StyledWriter::writeValue(const Valu
     pushValue(valueToString(value.asDouble()));
     break;
   case stringValue: {
     // Is NULL possible for value.string_? No.
     char const* str;
     char const* end;
     bool ok = value.getString(&str, &end);
     if (ok)
-      pushValue(valueToQuotedStringN(str, static_cast<unsigned>(end - str)));
+      pushValue(valueToQuotedStringN(str, static_cast<size_t>(end - str)));
     else
       pushValue("");
     break;
   }
   case booleanValue:
     pushValue(valueToString(value.asBool()));
     break;
   case arrayValue:
@@ -502,26 +503,26 @@ void StyledWriter::writeValue(const Valu
       unindent();
       writeWithIndent("}");
     }
   } break;
   }
 }
 
 void StyledWriter::writeArrayValue(const Value& value) {
-  unsigned size = value.size();
+  size_t size = value.size();
   if (size == 0)
     pushValue("[]");
   else {
     bool isArrayMultiLine = isMultilineArray(value);
     if (isArrayMultiLine) {
       writeWithIndent("[");
       indent();
       bool hasChildValue = !childValues_.empty();
-      unsigned index = 0;
+      ArrayIndex index = 0;
       for (;;) {
         const Value& childValue = value[index];
         writeCommentBeforeValue(childValue);
         if (hasChildValue)
           writeWithIndent(childValues_[index]);
         else {
           writeIndent();
           writeValue(childValue);
@@ -534,17 +535,17 @@ void StyledWriter::writeArrayValue(const
         writeCommentAfterValueOnSameLine(childValue);
       }
       unindent();
       writeWithIndent("]");
     } else // output on a single line
     {
       assert(childValues_.size() == size);
       document_ += "[ ";
-      for (unsigned index = 0; index < size; ++index) {
+      for (size_t index = 0; index < size; ++index) {
         if (index > 0)
           document_ += ", ";
         document_ += childValues_[index];
       }
       document_ += " ]";
     }
   }
 }
@@ -679,17 +680,17 @@ void StyledStreamWriter::writeValue(cons
     pushValue(valueToString(value.asDouble()));
     break;
   case stringValue: {
     // Is NULL possible for value.string_? No.
     char const* str;
     char const* end;
     bool ok = value.getString(&str, &end);
     if (ok)
-      pushValue(valueToQuotedStringN(str, static_cast<unsigned>(end - str)));
+      pushValue(valueToQuotedStringN(str, static_cast<size_t>(end - str)));
     else
       pushValue("");
     break;
   }
   case booleanValue:
     pushValue(valueToString(value.asBool()));
     break;
   case arrayValue:
@@ -953,18 +954,18 @@ void BuiltStyledStreamWriter::writeValue
                             precisionType_));
     break;
   case stringValue: {
     // Is NULL is possible for value.string_? No.
     char const* str;
     char const* end;
     bool ok = value.getString(&str, &end);
     if (ok)
-      pushValue(valueToQuotedStringN(str, static_cast<unsigned>(end - str),
-                                     emitUTF8_));
+      pushValue(
+          valueToQuotedStringN(str, static_cast<size_t>(end - str), emitUTF8_));
     else
       pushValue("");
     break;
   }
   case booleanValue:
     pushValue(valueToString(value.asBool()));
     break;
   case arrayValue:
@@ -977,18 +978,18 @@ void BuiltStyledStreamWriter::writeValue
     else {
       writeWithIndent("{");
       indent();
       auto it = members.begin();
       for (;;) {
         String const& name = *it;
         Value const& childValue = value[name];
         writeCommentBeforeValue(childValue);
-        writeWithIndent(valueToQuotedStringN(
-            name.data(), static_cast<unsigned>(name.length()), emitUTF8_));
+        writeWithIndent(
+            valueToQuotedStringN(name.data(), name.length(), emitUTF8_));
         *sout_ << colonSymbol_;
         writeValue(childValue);
         if (++it == members.end()) {
           writeCommentAfterValueOnSameLine(childValue);
           break;
         }
         *sout_ << ",";
         writeCommentAfterValueOnSameLine(childValue);
@@ -1212,17 +1213,17 @@ bool StreamWriterBuilder::validate(Json:
       "precision",
       "precisionType",
   };
   for (auto si = settings_.begin(); si != settings_.end(); ++si) {
     auto key = si.name();
     if (valid_keys.count(key))
       continue;
     if (invalid)
-      (*invalid)[std::move(key)] = *si;
+      (*invalid)[key] = *si;
     else
       return false;
   }
   return invalid ? invalid->empty() : true;
 }
 
 Value& StreamWriterBuilder::operator[](const String& key) {
   return settings_[key];
diff --git a/toolkit/components/jsoncpp/version.txt b/toolkit/components/jsoncpp/version.txt
--- a/toolkit/components/jsoncpp/version.txt
+++ b/toolkit/components/jsoncpp/version.txt
@@ -1,1 +1,1 @@
-1.9.4
+1.9.5
