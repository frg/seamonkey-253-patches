# HG changeset patch
# User Kelsey Gilbert <kelsey.gilbert@mozilla.com>
# Date 1680210279 0
# Node ID 5d7bc68dcd6a6b4b2d3f07912d49ee49bdf20370
# Parent  11464793ea3943610cdaf40b41ace0128d6179ba
Bug 1794292 - [ANGLE] cherry-pick init-gl-point-size. r=bradwerth, a=RyanVM

Differential Revision: https://phabricator.services.mozilla.com/D174175

diff --git a/gfx/angle/checkout/LICENSE b/gfx/angle/checkout/LICENSE
--- a/gfx/angle/checkout/LICENSE
+++ b/gfx/angle/checkout/LICENSE
@@ -24,9 +24,9 @@
 // FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 // COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 // INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 // BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 // LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 // CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 // LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 // ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-// POSSIBILITY OF SUCH DAMAGE.
\ No newline at end of file
+// POSSIBILITY OF SUCH DAMAGE.
diff --git a/gfx/angle/checkout/include/GLSLANG/ShaderLang.h b/gfx/angle/checkout/include/GLSLANG/ShaderLang.h
--- a/gfx/angle/checkout/include/GLSLANG/ShaderLang.h
+++ b/gfx/angle/checkout/include/GLSLANG/ShaderLang.h
@@ -332,16 +332,21 @@ const ShCompileOptions SH_ADD_VULKAN_XFB
 // VK_EXT_transform_feedback extension.
 const ShCompileOptions SH_ADD_VULKAN_XFB_EXTENSION_SUPPORT_CODE = UINT64_C(1) << 56;
 
 // This flag initializes fragment shader's output variables to zero at the beginning of the fragment
 // shader's main(). It is intended as a workaround for drivers which get context lost if
 // gl_FragColor is not written.
 const ShCompileOptions SH_INIT_FRAGMENT_OUTPUT_VARIABLES = UINT64_C(1) << 57;
 
+// This flag initializes gl_PointSize to 0.0f at the beginning of the vertex shader's
+// main(), and has no effect in the fragment shader. It is intended to assist in working around
+// bugs on some drivers when this is left written and then POINTS are drawn.
+const ShCompileOptions SH_INIT_GL_POINT_SIZE = UINT64_C(1) << 58;
+
 // Defines alternate strategies for implementing array index clamping.
 enum ShArrayIndexClampingStrategy
 {
     // Use the clamp intrinsic for array index clamping.
     SH_CLAMP_WITH_CLAMP_INTRINSIC = 1,
 
     // Use a user-defined function for array index clamping.
     SH_CLAMP_WITH_USER_DEFINED_INT_CLAMP_FUNCTION
diff --git a/gfx/angle/checkout/out/gen/angle/angle_commit.h b/gfx/angle/checkout/out/gen/angle/angle_commit.h
--- a/gfx/angle/checkout/out/gen/angle/angle_commit.h
+++ b/gfx/angle/checkout/out/gen/angle/angle_commit.h
@@ -1,4 +1,4 @@
-#define ANGLE_COMMIT_HASH "23851a53779d"
+#define ANGLE_COMMIT_HASH "568b727a61e6"
 #define ANGLE_COMMIT_HASH_SIZE 12
-#define ANGLE_COMMIT_DATE "2021-09-14 18:27:26 -0700"
-#define ANGLE_COMMIT_POSITION 15727
+#define ANGLE_COMMIT_DATE "2023-03-29 16:46:03 -0700"
+#define ANGLE_COMMIT_POSITION 15728
diff --git a/gfx/angle/checkout/src/compiler/translator/Compiler.cpp b/gfx/angle/checkout/src/compiler/translator/Compiler.cpp
--- a/gfx/angle/checkout/src/compiler/translator/Compiler.cpp
+++ b/gfx/angle/checkout/src/compiler/translator/Compiler.cpp
@@ -912,16 +912,27 @@ bool TCompiler::checkAndSimplifyAST(TInt
     {
         if (!initializeGLPosition(root))
         {
             return false;
         }
         mGLPositionInitialized = true;
     }
 
+    if (mShaderType == GL_VERTEX_SHADER && (compileOptions & SH_INIT_GL_POINT_SIZE) != 0)
+    {
+        sh::ShaderVariable var(GL_FLOAT);
+        var.name = "gl_PointSize";
+        if (!InitializeVariables(this, root, {var}, &mSymbolTable, mShaderVersion,
+                                 mExtensionBehavior, false, false))
+        {
+            return false;
+        }
+    }
+
     // DeferGlobalInitializers needs to be run before other AST transformations that generate new
     // statements from expressions. But it's fine to run DeferGlobalInitializers after the above
     // SplitSequenceOperator and RemoveArrayLengthMethod since they only have an effect on the AST
     // on ESSL >= 3.00, and the initializers that need to be deferred can only exist in ESSL < 3.00.
     // Exception: if EXT_shader_non_constant_global_initializers is enabled, we must generate global
     // initializers before we generate the DAG, since initializers may call functions which must not
     // be optimized out
     if (!enableNonConstantInitializers &&
diff --git a/gfx/angle/cherry_picks.txt b/gfx/angle/cherry_picks.txt
--- a/gfx/angle/cherry_picks.txt
+++ b/gfx/angle/cherry_picks.txt
@@ -1,8 +1,16 @@
+commit 568b727a61e6a78702a0deaab5efe68b6cd061ff
+Author: Kelsey Gilbert <kelsey.gilbert@mozilla.com>
+Date:   Wed Mar 29 16:13:39 2023 -0700
+
+    Add ShCompileOptions.initGLPointSize.
+    
+    Change-Id: I1d998867f3e829ed0dc8181fa76be5fe701dde70
+
 commit 23851a53779dfd3239b4cceb7f93df9ea6efb9c3
 Author: Jeff Gilbert <jgilbert@mozilla.com>
 Date:   Tue Sep 14 18:11:05 2021 -0700
 
     If RoUninitialize iff SUCCEEDED(RoInitialize).
     
     S_FALSE is success, so `SUCCEEDED(result) || result == S_FALSE` == `SUCCEEDED(result)`.
     RPC_E_CHANGED_MODE is an Error, so we should not call RoUninitialize in that case.
diff --git a/gfx/angle/targets/angle_common/moz.build b/gfx/angle/targets/angle_common/moz.build
--- a/gfx/angle/targets/angle_common/moz.build
+++ b/gfx/angle/targets/angle_common/moz.build
@@ -1,15 +1,15 @@
 # Generated by update-angle.py
 
 include("../../moz.build.common")
 
 # DEFINES["ANGLE_IS_WIN"] = True
 # DEFINES["CERT_CHAIN_PARA_HAS_EXTRA_FIELDS"] = True
-DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-3462-gfe5c2c3c-2"'
+DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-9302-g897d7bce-1"'
 DEFINES["DYNAMIC_ANNOTATIONS_ENABLED"] = "0"
 # DEFINES["NDEBUG"] = True
 DEFINES["NOMINMAX"] = True
 # DEFINES["NTDDI_VERSION"] = "NTDDI_WIN10_VB"
 # DEFINES["NVALGRIND"] = True
 # DEFINES["PSAPI_VERSION"] = "2"
 DEFINES["UNICODE"] = True
 # DEFINES["USE_AURA"] = "1"
@@ -35,17 +35,16 @@ LOCAL_INCLUDES += [
     "../../checkout/src/",
     "../../checkout/src/common/third_party/base/",
 ]
 
 # CXXFLAGS += [
 #     "-D__DATE__=",
 #     "-D__TIME__=",
 #     "-D__TIMESTAMP__=",
-#     "-debug-info-kind=constructor",
 #     "-fcolor-diagnostics",
 #     "-fcomplete-member-pointers",
 #     "-fcrash-diagnostics-dir=../tools/clang/crashreports",
 #     "-fdebug-compilation-dir",
 #     "-fmerge-all-constants",
 #     "-fmsc-version=1916",
 #     "-fno-delete-null-pointer-checks",
 #     "-fno-ident",
@@ -230,13 +229,13 @@ if CONFIG["OS_ARCH"] not in ("Darwin", "
 #     "/INCREMENTAL:NO",
 #     "/lldignoreenv",
 #     "/OPT:ICF",
 #     "/OPT:NOLLDTAILMERGE",
 #     "/OPT:REF",
 #     "/pdbaltpath:%_PDB%",
 #     "/PDBSourcePath:C:/dev/angle/out",
 #     "/PROFILE",
-#     "/TIMESTAMP:1615093200",
+#     "/TIMESTAMP:1619931600",
 #     "/WX",
 # ]
 
 Library("angle_common")
diff --git a/gfx/angle/targets/angle_gpu_info_util/moz.build b/gfx/angle/targets/angle_gpu_info_util/moz.build
--- a/gfx/angle/targets/angle_gpu_info_util/moz.build
+++ b/gfx/angle/targets/angle_gpu_info_util/moz.build
@@ -3,17 +3,17 @@
 include("../../moz.build.common")
 
 DEFINES["ANGLE_ENABLE_D3D11"] = True
 DEFINES["ANGLE_ENABLE_D3D11_COMPOSITOR_NATIVE_WINDOW"] = True
 DEFINES["ANGLE_ENABLE_D3D9"] = True
 # DEFINES["ANGLE_HAS_VULKAN_SYSTEM_INFO"] = True
 # DEFINES["ANGLE_IS_WIN"] = True
 # DEFINES["CERT_CHAIN_PARA_HAS_EXTRA_FIELDS"] = True
-DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-3462-gfe5c2c3c-2"'
+DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-9302-g897d7bce-1"'
 DEFINES["DYNAMIC_ANNOTATIONS_ENABLED"] = "0"
 # DEFINES["NDEBUG"] = True
 DEFINES["NOMINMAX"] = True
 # DEFINES["NTDDI_VERSION"] = "NTDDI_WIN10_VB"
 # DEFINES["NVALGRIND"] = True
 # DEFINES["PSAPI_VERSION"] = "2"
 DEFINES["UNICODE"] = True
 # DEFINES["USE_AURA"] = "1"
@@ -44,17 +44,16 @@ LOCAL_INCLUDES += [
     "../../checkout/third_party/vulkan-deps/vulkan-headers/src/include/",
 ]
 
 # CXXFLAGS += [
 #     "-add-plugin",
 #     "-D__DATE__=",
 #     "-D__TIME__=",
 #     "-D__TIMESTAMP__=",
-#     "-debug-info-kind=constructor",
 #     "-fcolor-diagnostics",
 #     "-fcomplete-member-pointers",
 #     "-fcrash-diagnostics-dir=../tools/clang/crashreports",
 #     "-fdebug-compilation-dir",
 #     "-fmerge-all-constants",
 #     "-fmsc-version=1916",
 #     "-fno-delete-null-pointer-checks",
 #     "-fno-ident",
@@ -219,13 +218,13 @@ OS_LIBS += [
 #     "/INCREMENTAL:NO",
 #     "/lldignoreenv",
 #     "/OPT:ICF",
 #     "/OPT:NOLLDTAILMERGE",
 #     "/OPT:REF",
 #     "/pdbaltpath:%_PDB%",
 #     "/PDBSourcePath:C:/dev/angle/out",
 #     "/PROFILE",
-#     "/TIMESTAMP:1615093200",
+#     "/TIMESTAMP:1619931600",
 #     "/WX",
 # ]
 
 Library("angle_gpu_info_util")
diff --git a/gfx/angle/targets/angle_image_util/moz.build b/gfx/angle/targets/angle_image_util/moz.build
--- a/gfx/angle/targets/angle_image_util/moz.build
+++ b/gfx/angle/targets/angle_image_util/moz.build
@@ -1,15 +1,15 @@
 # Generated by update-angle.py
 
 include("../../moz.build.common")
 
 # DEFINES["ANGLE_IS_WIN"] = True
 # DEFINES["CERT_CHAIN_PARA_HAS_EXTRA_FIELDS"] = True
-DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-3462-gfe5c2c3c-2"'
+DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-9302-g897d7bce-1"'
 DEFINES["DYNAMIC_ANNOTATIONS_ENABLED"] = "0"
 # DEFINES["NDEBUG"] = True
 DEFINES["NOMINMAX"] = True
 # DEFINES["NTDDI_VERSION"] = "NTDDI_WIN10_VB"
 # DEFINES["NVALGRIND"] = True
 # DEFINES["PSAPI_VERSION"] = "2"
 DEFINES["UNICODE"] = True
 # DEFINES["USE_AURA"] = "1"
@@ -35,17 +35,16 @@ LOCAL_INCLUDES += [
     "../../checkout/src/",
     "../../checkout/src/common/third_party/base/",
 ]
 
 # CXXFLAGS += [
 #     "-D__DATE__=",
 #     "-D__TIME__=",
 #     "-D__TIMESTAMP__=",
-#     "-debug-info-kind=constructor",
 #     "-fcolor-diagnostics",
 #     "-fcomplete-member-pointers",
 #     "-fcrash-diagnostics-dir=../tools/clang/crashreports",
 #     "-fdebug-compilation-dir",
 #     "-fmerge-all-constants",
 #     "-fmsc-version=1916",
 #     "-fno-delete-null-pointer-checks",
 #     "-fno-ident",
@@ -198,13 +197,13 @@ DIRS += [
 #     "/INCREMENTAL:NO",
 #     "/lldignoreenv",
 #     "/OPT:ICF",
 #     "/OPT:NOLLDTAILMERGE",
 #     "/OPT:REF",
 #     "/pdbaltpath:%_PDB%",
 #     "/PDBSourcePath:C:/dev/angle/out",
 #     "/PROFILE",
-#     "/TIMESTAMP:1615093200",
+#     "/TIMESTAMP:1619931600",
 #     "/WX",
 # ]
 
 Library("angle_image_util")
diff --git a/gfx/angle/targets/compression_utils_portable/moz.build b/gfx/angle/targets/compression_utils_portable/moz.build
--- a/gfx/angle/targets/compression_utils_portable/moz.build
+++ b/gfx/angle/targets/compression_utils_portable/moz.build
@@ -1,14 +1,14 @@
 # Generated by update-angle.py
 
 include("../../moz.build.common")
 
 # DEFINES["CERT_CHAIN_PARA_HAS_EXTRA_FIELDS"] = True
-DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-3462-gfe5c2c3c-2"'
+DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-9302-g897d7bce-1"'
 DEFINES["DYNAMIC_ANNOTATIONS_ENABLED"] = "0"
 # DEFINES["NDEBUG"] = True
 DEFINES["NOMINMAX"] = True
 # DEFINES["NTDDI_VERSION"] = "NTDDI_WIN10_VB"
 # DEFINES["NVALGRIND"] = True
 # DEFINES["PSAPI_VERSION"] = "2"
 DEFINES["UNICODE"] = True
 # DEFINES["USE_AURA"] = "1"
@@ -34,17 +34,16 @@ LOCAL_INCLUDES += [
     "../../checkout/third_party/zlib/",
 ]
 
 # CXXFLAGS += [
 #     "-add-plugin",
 #     "-D__DATE__=",
 #     "-D__TIME__=",
 #     "-D__TIMESTAMP__=",
-#     "-debug-info-kind=constructor",
 #     "-fcolor-diagnostics",
 #     "-fcomplete-member-pointers",
 #     "-fcrash-diagnostics-dir=../tools/clang/crashreports",
 #     "-fdebug-compilation-dir",
 #     "-fmerge-all-constants",
 #     "-fmsc-version=1916",
 #     "-fno-delete-null-pointer-checks",
 #     "-fno-ident",
@@ -163,13 +162,13 @@ USE_LIBS += [
 #     "/INCREMENTAL:NO",
 #     "/lldignoreenv",
 #     "/OPT:ICF",
 #     "/OPT:NOLLDTAILMERGE",
 #     "/OPT:REF",
 #     "/pdbaltpath:%_PDB%",
 #     "/PDBSourcePath:C:/dev/angle/out",
 #     "/PROFILE",
-#     "/TIMESTAMP:1615093200",
+#     "/TIMESTAMP:1619931600",
 #     "/WX",
 # ]
 
 Library("compression_utils_portable")
diff --git a/gfx/angle/targets/libEGL/moz.build b/gfx/angle/targets/libEGL/moz.build
--- a/gfx/angle/targets/libEGL/moz.build
+++ b/gfx/angle/targets/libEGL/moz.build
@@ -2,17 +2,17 @@
 
 include("../../moz.build.common")
 
 # DEFINES["ANGLE_EGL_LIBRARY_NAME"] = '"libEGL"'
 DEFINES["ANGLE_GLESV2_LIBRARY_NAME"] = '"libGLESv2"'
 # DEFINES["ANGLE_IS_WIN"] = True
 DEFINES["ANGLE_USE_EGL_LOADER"] = True
 # DEFINES["CERT_CHAIN_PARA_HAS_EXTRA_FIELDS"] = True
-DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-3462-gfe5c2c3c-2"'
+DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-9302-g897d7bce-1"'
 DEFINES["DYNAMIC_ANNOTATIONS_ENABLED"] = "0"
 DEFINES["EGLAPI"] = ""
 DEFINES["EGL_EGLEXT_PROTOTYPES"] = True
 DEFINES["EGL_EGL_PROTOTYPES"] = "1"
 DEFINES["GL_GLES_PROTOTYPES"] = "1"
 DEFINES["GL_GLEXT_PROTOTYPES"] = True
 DEFINES["LIBEGL_IMPLEMENTATION"] = True
 # DEFINES["NDEBUG"] = True
@@ -44,17 +44,16 @@ LOCAL_INCLUDES += [
     "../../checkout/src/",
     "../../checkout/src/common/third_party/base/",
 ]
 
 # CXXFLAGS += [
 #     "-D__DATE__=",
 #     "-D__TIME__=",
 #     "-D__TIMESTAMP__=",
-#     "-debug-info-kind=constructor",
 #     "-fcolor-diagnostics",
 #     "-fcomplete-member-pointers",
 #     "-fcrash-diagnostics-dir=../tools/clang/crashreports",
 #     "-fdebug-compilation-dir",
 #     "-fmerge-all-constants",
 #     "-fmsc-version=1916",
 #     "-fno-delete-null-pointer-checks",
 #     "-fno-ident",
@@ -239,15 +238,15 @@ OS_LIBS += [
 #     "/NXCOMPAT",
 #     "/OPT:ICF",
 #     "/OPT:NOLLDTAILMERGE",
 #     "/OPT:REF",
 #     "/pdbaltpath:%_PDB%",
 #     "/PDBSourcePath:C:/dev/angle/out",
 #     "/PROFILE",
 #     "/SUBSYSTEM:CONSOLE,5.02",
-#     "/TIMESTAMP:1615093200",
+#     "/TIMESTAMP:1619931600",
 #     "/WX",
 # ]
 
 DEFFILE = "../../checkout/src/libEGL/libEGL_autogen.def"
 RCFILE = "../../checkout/src/libEGL/libEGL.rc"
 GeckoSharedLibrary("libEGL", linkage=None)
diff --git a/gfx/angle/targets/libGLESv2/moz.build b/gfx/angle/targets/libGLESv2/moz.build
--- a/gfx/angle/targets/libGLESv2/moz.build
+++ b/gfx/angle/targets/libGLESv2/moz.build
@@ -4,17 +4,17 @@ include("../../moz.build.common")
 
 DEFINES["ANGLE_CAPTURE_ENABLED"] = "0"
 DEFINES["ANGLE_ENABLE_D3D11"] = True
 DEFINES["ANGLE_ENABLE_D3D11_COMPOSITOR_NATIVE_WINDOW"] = True
 DEFINES["ANGLE_ENABLE_D3D9"] = True
 # DEFINES["ANGLE_IS_WIN"] = True
 # DEFINES["ANGLE_PRELOADED_D3DCOMPILER_MODULE_NAMES"] = "{ "d3dcompiler_47.dll", "d3dcompiler_46.dll", "d3dcompiler_43.dll" }"
 # DEFINES["CERT_CHAIN_PARA_HAS_EXTRA_FIELDS"] = True
-DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-3462-gfe5c2c3c-2"'
+DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-9302-g897d7bce-1"'
 DEFINES["DYNAMIC_ANNOTATIONS_ENABLED"] = "0"
 DEFINES["EGL_EGLEXT_PROTOTYPES"] = True
 DEFINES["EGL_EGL_PROTOTYPES"] = "1"
 DEFINES["GL_API"] = ""
 DEFINES["GL_APICALL"] = ""
 DEFINES["GL_GLES_PROTOTYPES"] = "1"
 DEFINES["GL_GLEXT_PROTOTYPES"] = True
 DEFINES["LIBANGLE_IMPLEMENTATION"] = True
@@ -51,17 +51,16 @@ LOCAL_INCLUDES += [
     "../../checkout/third_party/zlib/google/",
 ]
 
 # CXXFLAGS += [
 #     "-add-plugin",
 #     "-D__DATE__=",
 #     "-D__TIME__=",
 #     "-D__TIMESTAMP__=",
-#     "-debug-info-kind=constructor",
 #     "-fcolor-diagnostics",
 #     "-fcomplete-member-pointers",
 #     "-fcrash-diagnostics-dir=../tools/clang/crashreports",
 #     "-fdebug-compilation-dir",
 #     "-fmerge-all-constants",
 #     "-fmsc-version=1916",
 #     "-fno-delete-null-pointer-checks",
 #     "-fno-ident",
@@ -445,15 +444,15 @@ OS_LIBS += [
 #     "/NXCOMPAT",
 #     "/OPT:ICF",
 #     "/OPT:NOLLDTAILMERGE",
 #     "/OPT:REF",
 #     "/pdbaltpath:%_PDB%",
 #     "/PDBSourcePath:C:/dev/angle/out",
 #     "/PROFILE",
 #     "/SUBSYSTEM:CONSOLE,5.02",
-#     "/TIMESTAMP:1615093200",
+#     "/TIMESTAMP:1619931600",
 #     "/WX",
 # ]
 
 DEFFILE = "../../checkout/src/libGLESv2/libGLESv2_autogen.def"
 RCFILE = "../../checkout/src/libGLESv2/libGLESv2.rc"
 GeckoSharedLibrary("libGLESv2", linkage=None)
diff --git a/gfx/angle/targets/preprocessor/moz.build b/gfx/angle/targets/preprocessor/moz.build
--- a/gfx/angle/targets/preprocessor/moz.build
+++ b/gfx/angle/targets/preprocessor/moz.build
@@ -1,15 +1,15 @@
 # Generated by update-angle.py
 
 include("../../moz.build.common")
 
 # DEFINES["ANGLE_IS_WIN"] = True
 # DEFINES["CERT_CHAIN_PARA_HAS_EXTRA_FIELDS"] = True
-DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-3462-gfe5c2c3c-2"'
+DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-9302-g897d7bce-1"'
 DEFINES["DYNAMIC_ANNOTATIONS_ENABLED"] = "0"
 # DEFINES["NDEBUG"] = True
 DEFINES["NOMINMAX"] = True
 # DEFINES["NTDDI_VERSION"] = "NTDDI_WIN10_VB"
 # DEFINES["NVALGRIND"] = True
 # DEFINES["PSAPI_VERSION"] = "2"
 DEFINES["UNICODE"] = True
 # DEFINES["USE_AURA"] = "1"
@@ -35,17 +35,16 @@ LOCAL_INCLUDES += [
     "../../checkout/src/",
     "../../checkout/src/common/third_party/base/",
 ]
 
 # CXXFLAGS += [
 #     "-D__DATE__=",
 #     "-D__TIME__=",
 #     "-D__TIMESTAMP__=",
-#     "-debug-info-kind=constructor",
 #     "-fcolor-diagnostics",
 #     "-fcomplete-member-pointers",
 #     "-fcrash-diagnostics-dir=../tools/clang/crashreports",
 #     "-fdebug-compilation-dir",
 #     "-fmerge-all-constants",
 #     "-fmsc-version=1916",
 #     "-fno-delete-null-pointer-checks",
 #     "-fno-ident",
@@ -205,13 +204,13 @@ DIRS += [
 #     "/INCREMENTAL:NO",
 #     "/lldignoreenv",
 #     "/OPT:ICF",
 #     "/OPT:NOLLDTAILMERGE",
 #     "/OPT:REF",
 #     "/pdbaltpath:%_PDB%",
 #     "/PDBSourcePath:C:/dev/angle/out",
 #     "/PROFILE",
-#     "/TIMESTAMP:1615093200",
+#     "/TIMESTAMP:1619931600",
 #     "/WX",
 # ]
 
 Library("preprocessor")
diff --git a/gfx/angle/targets/translator/moz.build b/gfx/angle/targets/translator/moz.build
--- a/gfx/angle/targets/translator/moz.build
+++ b/gfx/angle/targets/translator/moz.build
@@ -2,17 +2,17 @@
 
 include("../../moz.build.common")
 
 DEFINES["ANGLE_ENABLE_ESSL"] = True
 DEFINES["ANGLE_ENABLE_GLSL"] = True
 DEFINES["ANGLE_ENABLE_HLSL"] = True
 # DEFINES["ANGLE_IS_WIN"] = True
 # DEFINES["CERT_CHAIN_PARA_HAS_EXTRA_FIELDS"] = True
-DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-3462-gfe5c2c3c-2"'
+DEFINES["CR_CLANG_REVISION"] = '"llvmorg-13-init-9302-g897d7bce-1"'
 DEFINES["DYNAMIC_ANNOTATIONS_ENABLED"] = "0"
 # DEFINES["NDEBUG"] = True
 DEFINES["NOMINMAX"] = True
 # DEFINES["NTDDI_VERSION"] = "NTDDI_WIN10_VB"
 # DEFINES["NVALGRIND"] = True
 # DEFINES["PSAPI_VERSION"] = "2"
 DEFINES["UNICODE"] = True
 # DEFINES["USE_AURA"] = "1"
@@ -38,17 +38,16 @@ LOCAL_INCLUDES += [
     "../../checkout/src/",
     "../../checkout/src/common/third_party/base/",
 ]
 
 # CXXFLAGS += [
 #     "-D__DATE__=",
 #     "-D__TIME__=",
 #     "-D__TIMESTAMP__=",
-#     "-debug-info-kind=constructor",
 #     "-fcolor-diagnostics",
 #     "-fcomplete-member-pointers",
 #     "-fcrash-diagnostics-dir=../tools/clang/crashreports",
 #     "-fdebug-compilation-dir",
 #     "-fmerge-all-constants",
 #     "-fmsc-version=1916",
 #     "-fno-delete-null-pointer-checks",
 #     "-fno-ident",
@@ -333,13 +332,13 @@ DIRS += [
 #     "/INCREMENTAL:NO",
 #     "/lldignoreenv",
 #     "/OPT:ICF",
 #     "/OPT:NOLLDTAILMERGE",
 #     "/OPT:REF",
 #     "/pdbaltpath:%_PDB%",
 #     "/PDBSourcePath:C:/dev/angle/out",
 #     "/PROFILE",
-#     "/TIMESTAMP:1615093200",
+#     "/TIMESTAMP:1619931600",
 #     "/WX",
 # ]
 
 Library("translator")
diff --git a/gfx/angle/update-angle.py b/gfx/angle/update-angle.py
--- a/gfx/angle/update-angle.py
+++ b/gfx/angle/update-angle.py
@@ -331,17 +331,17 @@ REGISTERED_DEFINES = {
 }
 
 # -
 
 print("\nRun actions")
 required_files: Set[str] = set()
 required_files.add("//LICENSE")
 
-run_checked("ninja", "-C", str(OUT_DIR), ":angle_commit_id")
+run_checked("ninja", "-C", str(OUT_DIR), ":angle_commit_id", shell=True)
 required_files.add("//out/gen/angle/angle_commit.h")
 
 # -
 
 # Export our targets
 print("\nExport targets")
 
 # Clear our dest directories

