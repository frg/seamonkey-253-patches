# HG changeset patch
# User Mike de Boer <mdeboer@mozilla.com>
# Date 1516711454 -3600
# Node ID 59ece44fa56e808f25ca45649c937742bc31eb9f
# Parent  08495fc79fff713bbdd582d0e1d1a224dfc8fb71
Bug 1411581 - Mochitest 'browser_615394-SSWindowState_events_undoCloseTab.js' leaks windows too frequently; rewrite the test and wait for the browser to load, before finishing up the test. r=mconley

MozReview-Commit-ID: 6HFJTZP8tLB

diff --git a/browser/components/sessionstore/test/browser_615394-SSWindowState_events_undoCloseTab.js b/browser/components/sessionstore/test/browser_615394-SSWindowState_events_undoCloseTab.js
--- a/browser/components/sessionstore/test/browser_615394-SSWindowState_events_undoCloseTab.js
+++ b/browser/components/sessionstore/test/browser_615394-SSWindowState_events_undoCloseTab.js
@@ -1,60 +1,57 @@
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+"use strict";
 
 const testState = {
   windows: [{
     tabs: [
       { entries: [{ url: "about:blank", triggeringPrincipal_base64 }] },
       { entries: [{ url: "about:rights", triggeringPrincipal_base64 }] }
     ]
   }]
 };
 
-function test() {
-  /** Test for Bug 615394 - Session Restore should notify when it is beginning and ending a restore **/
-  waitForExplicitFinish();
+// Test for Bug 615394 - Session Restore should notify when it is beginning and
+// ending a restore.
+add_task(async function test_undoCloseTab() {
+  await promiseBrowserState(testState);
 
-  waitForBrowserState(testState, test_undoCloseTab);
-}
-
-function test_undoCloseTab() {
-  let tab = gBrowser.tabs[1],
-      busyEventCount = 0,
-      readyEventCount = 0,
-      reopenedTab;
+  let tab = gBrowser.tabs[1];
+  let busyEventCount = 0;
+  let readyEventCount = 0;
+  // This will be set inside the `onSSWindowStateReady` method.
+  let lastTab;
 
   ss.setTabValue(tab, "foo", "bar");
 
   function onSSWindowStateBusy(aEvent) {
     busyEventCount++;
   }
 
   function onSSWindowStateReady(aEvent) {
-    reopenedTab = gBrowser.tabs[1];
+    Assert.equal(gBrowser.tabs.length, 2, "Should only have 2 tabs");
+    lastTab = gBrowser.tabs[1];
     readyEventCount++;
-    is(ss.getTabValue(reopenedTab, "foo"), "bar");
-    ss.setTabValue(reopenedTab, "baz", "qux");
-  }
-
-  function onSSTabRestored(aEvent) {
-    is(busyEventCount, 1);
-    is(readyEventCount, 1);
-    is(ss.getTabValue(reopenedTab, "baz"), "qux");
-    is(reopenedTab.linkedBrowser.currentURI.spec, "about:rights");
-
-    window.removeEventListener("SSWindowStateBusy", onSSWindowStateBusy);
-    window.removeEventListener("SSWindowStateReady", onSSWindowStateReady);
-
-    gBrowser.removeTab(gBrowser.tabs[1]);
-    finish();
+    Assert.equal(ss.getTabValue(lastTab, "foo"), "bar");
+    ss.setTabValue(lastTab, "baz", "qux");
   }
 
   window.addEventListener("SSWindowStateBusy", onSSWindowStateBusy);
   window.addEventListener("SSWindowStateReady", onSSWindowStateReady);
-  gBrowser.tabContainer.addEventListener("SSTabRestored", onSSTabRestored, { once: true });
+
+  let restoredPromise = BrowserTestUtils.waitForEvent(gBrowser.tabContainer, "SSTabRestored");
+
+  await BrowserTestUtils.removeTab(tab);
+  let reopenedTab = ss.undoCloseTab(window, 0);
+
+  await Promise.all([restoredPromise, BrowserTestUtils.browserLoaded(reopenedTab.linkedBrowser)]);
 
-  gBrowser.removeTab(tab);
-  reopenedTab = ss.undoCloseTab(window, 0);
-}
+  Assert.equal(reopenedTab, lastTab, "Tabs should be the same one.");
+  Assert.equal(busyEventCount, 1);
+  Assert.equal(readyEventCount, 1);
+  Assert.equal(ss.getTabValue(reopenedTab, "baz"), "qux");
+  Assert.equal(reopenedTab.linkedBrowser.currentURI.spec, "about:rights");
 
+  window.removeEventListener("SSWindowStateBusy", onSSWindowStateBusy);
+  window.removeEventListener("SSWindowStateReady", onSSWindowStateReady);
+
+  await BrowserTestUtils.removeTab(reopenedTab);
+});
