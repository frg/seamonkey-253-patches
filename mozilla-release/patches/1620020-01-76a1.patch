# HG changeset patch
# User Iain Ireland <iireland@mozilla.com>
# Date 1583425593 0
#      Thu Mar 05 16:26:33 2020 +0000
# Node ID cad9fd303a77f52836d7b754754d1deff3888ff5
# Parent  a1771d7b1b8f9aada3a762b8bd81f48f2affccb5
Bug 1620020: Prune some dead shim code r=mgaudet

This shim code was only used in irregexp code that we're factoring out into a separate file and not importing.

Differential Revision: https://phabricator.services.mozilla.com/D65530

diff --git a/js/src/new-regexp/regexp-shim.cc b/js/src/new-regexp/regexp-shim.cc
--- a/js/src/new-regexp/regexp-shim.cc
+++ b/js/src/new-regexp/regexp-shim.cc
@@ -45,19 +45,16 @@ std::ostream& operator<<(std::ostream& o
   if (v <= String::kMaxUtf16CodeUnit) {
     return os << AsUC16(v);
   }
   char buf[13];
   snprintf(buf, sizeof(buf), "\\u{%06x}", v);
   return os << buf;
 }
 
-DisallowJavascriptExecution::DisallowJavascriptExecution(Isolate* isolate)
-    : nojs_(isolate->cx()) {}
-
 // TODO: Map flags to jitoptions
 bool FLAG_correctness_fuzzer_suppressions = false;
 bool FLAG_enable_regexp_unaligned_accesses = false;
 bool FLAG_harmony_regexp_sequence = false;
 bool FLAG_regexp_interpret_all = false;
 bool FLAG_regexp_mode_modifiers = false;
 bool FLAG_regexp_optimization = false;
 bool FLAG_regexp_peephole_optimization = false;
diff --git a/js/src/new-regexp/regexp-shim.h b/js/src/new-regexp/regexp-shim.h
--- a/js/src/new-regexp/regexp-shim.h
+++ b/js/src/new-regexp/regexp-shim.h
@@ -429,24 +429,16 @@ class Object {
 
   // Used in regexp-macro-assembler.cc and regexp-interpreter.cc to
   // check the return value of isolate->stack_guard()->HandleInterrupts()
   // In V8, this will be either an exception object or undefined.
   // In SM, we store the exception in the context, so we can use our normal
   // idiom: return false iff we are throwing an exception.
   inline bool IsException(Isolate*) const { return !value_.toBoolean(); }
 
-  // SpiderMonkey tries to avoid leaking the internal representation of its
-  // objects. V8 is not so strict. These functions are used when calling /
-  // being called by native code: objects are converted to Addresses for the
-  // call, then cast back to objects on the other side.
-  // We might be able to upstream a patch that eliminates the need for these.
-  Object(Address bits);
-  Address ptr() const;
-
  protected:
   JS::Value value_;
 };
 
 class Smi : public Object {
  public:
   static Smi FromInt(int32_t value) {
     Smi smi;
@@ -585,24 +577,16 @@ class DisallowHeapAllocation {
 // - isolate->StackOverflow()
 // Those cases don't allocate in SpiderMonkey, so this can be a no-op.
 class AllowHeapAllocation {
  public:
   // Empty constructor to avoid unused_variable warnings
   AllowHeapAllocation() {}
 };
 
-class DisallowJavascriptExecution {
- public:
-  DisallowJavascriptExecution(Isolate* isolate);
-
- private:
-  js::AutoAssertNoContentJS nojs_;
-};
-
 // Origin:
 // https://github.com/v8/v8/blob/84f3877c15bc7f8956d21614da4311337525a3c8/src/objects/string.h#L83-L474
 class String : public HeapObject {
  private:
   JSString* str() const { return value_.toString(); }
 
  public:
   operator JSString*() const { return str(); }
@@ -851,19 +835,16 @@ class Isolate {
 
   Counters* counters() { return &counters_; }
 
   //********** Factory code **********//
   inline Factory* factory() { return this; }
 
   Handle<ByteArray> NewByteArray(
       int length, AllocationType allocation = AllocationType::kYoung);
-  MOZ_MUST_USE MaybeHandle<String> NewStringFromOneByte(
-      const Vector<const uint8_t>& str,
-      AllocationType allocation = AllocationType::kYoung);
 
   // Allocates a fixed array initialized with undefined values.
   Handle<FixedArray> NewFixedArray(
       int length, AllocationType allocation = AllocationType::kYoung);
 
   template <typename Char>
   Handle<String> InternalizeString(const Vector<const Char>& str);
 
@@ -908,48 +889,16 @@ class Code {
 
   Address raw_instruction_start();
   Address raw_instruction_end();
   Address address();
 
   static Code cast(Object object);
 };
 
-// GeneratedCode provides an interface for calling into jit code.
-// It will probably require additional work to hook this up to the
-// arm simulator.
-// Origin:
-// https://github.com/v8/v8/blob/abfbe7687edb5b2dffe0b33b24e0a41bb86a8214/src/execution/simulator.h#L96-L164
-template <typename Return, typename... Args>
-class GeneratedCode {
- public:
-  using Signature = Return(Args...);
-
-  static GeneratedCode FromCode(Code code);  // TODO: implement
-  Return Call(Args... args) { return fn_ptr_(args...); }
-
- private:
-  friend class GeneratedCode<Return(Args...)>;
-  Isolate* isolate_;
-  Signature* fn_ptr_;
-  GeneratedCode(Isolate* isolate, Signature* fn_ptr)
-      : isolate_(isolate), fn_ptr_(fn_ptr) {}
-};
-
-// Allow to use {GeneratedCode<ret(arg1, arg2)>} instead of
-// {GeneratedCode<ret, arg1, arg2>}.
-template <typename Return, typename... Args>
-class GeneratedCode<Return(Args...)> : public GeneratedCode<Return, Args...> {
- public:
-  // Automatically convert from {GeneratedCode<ret, arg1, arg2>} to
-  // {GeneratedCode<ret(arg1, arg2)>}.
-  GeneratedCode(GeneratedCode<Return, Args...> other)
-      : GeneratedCode<Return, Args...>(other.isolate_, other.fn_ptr_) {}
-};
-
 enum class MessageTemplate { kStackOverflow };
 
 class MessageFormatter {
  public:
   static const char* TemplateString(MessageTemplate index) {
     switch (index) {
       case MessageTemplate::kStackOverflow:
         return "too much recursion";
