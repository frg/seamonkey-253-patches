# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1554431456 0
# Node ID 60669a841a87644ba9b2f4d9f17f225e2eca0980
# Parent  0f0782a8fbfd5c6096731f871a059fea5f1bd9a2
Bug 1541792 - Replace linker magic with manual component registration. r=froydnj

Before bug 938437, we had a rather large and error-prone
nsStaticXULComponents.cpp used to register all modules. That was
replaced with clever use of the linker, which allowed to avoid the mess
that maintaining that file was.

Fast forward to now, where after bug 1524687 and other work that
preceded it, we have a much smaller number of remaining static xpcom
components, registered via this linker hack, and don't expect to add
any new ones. The list should eventually go down to zero.

Within that context, it seems to be the right time to get rid of the
magic, and with it the problems it causes on its own.

Some of those components could probably be trivially be converted to
static registration via .conf files, but I didn't want to deal with the
possible need to increase the number of dummy modules in XPCOMInit.cpp.
They can still be converted as a followup.

Differential Revision: https://phabricator.services.mozilla.com/D26076

diff --git a/toolkit/library/StaticXULComponents.ld b/toolkit/library/StaticXULComponents.ld
deleted file mode 100644
--- a/toolkit/library/StaticXULComponents.ld
+++ /dev/null
@@ -1,7 +0,0 @@
-SECTIONS {
-  .data.rel.ro : {
-    PROVIDE_HIDDEN(__start_kPStaticModules = .);
-    *(kPStaticModules)
-    PROVIDE_HIDDEN(__stop_kPStaticModules = .);
-  }
-}
diff --git a/toolkit/library/moz.build b/toolkit/library/moz.build
--- a/toolkit/library/moz.build
+++ b/toolkit/library/moz.build
@@ -51,27 +51,16 @@ def Libxul(name, output_category=None):
             '/xpcom/base',
         ]
         # config/version.mk says $(srcdir)/$(RCINCLUDE), and this needs to
         # be valid in both toolkit/library and toolkit/library/gtest.
         # Eventually, the make backend would do its own path canonicalization
         # and config/version.mk would lift the $(srcdir)
         RCINCLUDE = '$(DEPTH)/toolkit/library/xulrunner.rc'
 
-    # BFD ld doesn't create multiple PT_LOADs as usual when an unknown section
-    # exists. Using an implicit linker script to make it fold that section in
-    # .data.rel.ro makes it create multiple PT_LOADs. That implicit linker
-    # script however makes gold misbehave, first because it doesn't like that
-    # the linker script is given after crtbegin.o, and even past that, replaces
-    # the default section rules with those from the script instead of
-    # supplementing them. Which leads to a lib with a huge load of sections.
-    if CONFIG['OS_TARGET'] not in ('OpenBSD', 'WINNT') and \
-            CONFIG['LINKER_KIND'] == 'bfd':
-        LDFLAGS += [TOPSRCDIR + '/toolkit/library/StaticXULComponents.ld']
-
     Libxul_defines()
 
     if CONFIG['MOZ_NEEDS_LIBATOMIC']:
         OS_LIBS += ['atomic']
 
     # This option should go away in bug 1290972, but we need to wait until
     # Rust 1.12 has been released.
     # We're also linking against libresolv to solve bug 1367932.
