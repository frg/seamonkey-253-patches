# HG changeset patch
# User Eric Rahm <erahm@mozilla.com>
# Date 1559846133 0
# Node ID 33fbb6ff15cd2f14d801caf9de47d35c068d8131
# Parent  1fc5ee55c5e6ed7b5921104328c724e03eab13dd
Bug 1551368 - Part 3: Don't remove the rust incremental cache when clobbering. r=chmanchester

Skips over the incremental cache when performing a clobber. The incremental compilation cache is located at:
  `$(objdir)/$(rust_target)/$(rust_build_kind)/incremental`

When cross compiling there can be two caches, one for the host and one for the target so we handle both.

Differential Revision: https://phabricator.services.mozilla.com/D31310

diff --git a/python/mozbuild/mozbuild/controller/clobber.py b/python/mozbuild/mozbuild/controller/clobber.py
--- a/python/mozbuild/mozbuild/controller/clobber.py
+++ b/python/mozbuild/mozbuild/controller/clobber.py
@@ -131,30 +131,47 @@ class Clobberer(object):
 
     def remove_objdir(self, full=True):
         """Remove the object directory.
 
         ``full`` controls whether to fully delete the objdir. If False,
         some directories (e.g. Visual Studio Project Files) will not be
         deleted.
         """
+        # Determine where cargo build artifacts are stored
+        RUST_TARGET_VARS = ('RUST_HOST_TARGET', 'RUST_TARGET')
+        rust_targets = set([self.substs[x] for x in RUST_TARGET_VARS if x in self.substs])
+        rust_build_kind = 'release'
+        if self.substs.get('MOZ_DEBUG_RUST'):
+            rust_build_kind = 'debug'
+
         # Top-level files and directories to not clobber by default.
         no_clobber = {
             '.mozbuild',
             'msvc',
         }
 
+        # Hold off on clobbering cargo build artifacts
+        no_clobber |= rust_targets
+
         if full:
             # mozfile doesn't like unicode arguments (bug 818783).
             paths = [self.topobjdir.encode('utf-8')]
         else:
             paths = self.collect_subdirs(self.topobjdir, no_clobber)
 
         self.delete_dirs(self.topobjdir, paths)
 
+        # Now handle cargo's build artifacts and skip removing the incremental
+        # compilation cache.
+        for target in rust_targets:
+            cargo_path = os.path.join(self.topobjdir, target, rust_build_kind)
+            paths = self.collect_subdirs(cargo_path, {'incremental', })
+            self.delete_dirs(cargo_path, paths)
+
     def ensure_objdir_state(self):
         """Ensure the CLOBBER file in the objdir exists.
 
         This is called as part of the build to ensure the clobber information
         is configured properly for the objdir.
         """
         if not os.path.exists(self.topobjdir):
             os.makedirs(self.topobjdir)
