# HG changeset patch
# User Tom Schuster <evilpies@gmail.com>
# Date 1523040990 -7200
# Node ID 1b3203bbe43b4659681141da0f832853e84f7a4a
# Parent  cf6d4d042aef11c17e3b187cd58516b30b883b83
Bug 1453006 - Convert FunctionSyntaxKind to an enum class. r=jandem

diff --git a/js/src/frontend/ParseNode.h b/js/src/frontend/ParseNode.h
--- a/js/src/frontend/ParseNode.h
+++ b/js/src/frontend/ParseNode.h
@@ -1433,17 +1433,17 @@ AccessorTypeToJSOp(AccessorType atype)
         return JSOP_INITPROP_GETTER;
       case AccessorType::Setter:
         return JSOP_INITPROP_SETTER;
       default:
         MOZ_CRASH("unexpected accessor type");
     }
 }
 
-enum FunctionSyntaxKind
+enum class FunctionSyntaxKind
 {
     // A non-arrow function expression.
     Expression,
 
     // A named function appearing as a Statement.
     Statement,
 
     Arrow,
@@ -1452,24 +1452,27 @@ enum FunctionSyntaxKind
     DerivedClassConstructor,
     Getter,
     Setter,
 };
 
 static inline bool
 IsConstructorKind(FunctionSyntaxKind kind)
 {
-    return kind == ClassConstructor || kind == DerivedClassConstructor;
+    return kind == FunctionSyntaxKind::ClassConstructor ||
+           kind == FunctionSyntaxKind::DerivedClassConstructor;
 }
 
 static inline bool
 IsMethodDefinitionKind(FunctionSyntaxKind kind)
 {
-    return kind == Method || IsConstructorKind(kind) ||
-           kind == Getter || kind == Setter;
+    return IsConstructorKind(kind) ||
+           kind == FunctionSyntaxKind::Method ||
+           kind == FunctionSyntaxKind::Getter ||
+           kind == FunctionSyntaxKind::Setter;
 }
 
 static inline ParseNode*
 FunctionFormalParametersList(ParseNode* fn, unsigned* numFormals)
 {
     MOZ_ASSERT(fn->isKind(ParseNodeKind::Function));
     ParseNode* argsBody = fn->pn_body;
     MOZ_ASSERT(argsBody->isKind(ParseNodeKind::ParamsBody));
diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -536,22 +536,22 @@ FunctionBox::initWithEnclosingParseConte
         allowSuperProperty_ = sc->allowSuperProperty();
         allowSuperCall_ = sc->allowSuperCall();
         needsThisTDZChecks_ = sc->needsThisTDZChecks();
         thisBinding_ = sc->thisBinding();
     } else {
         allowNewTarget_ = true;
         allowSuperProperty_ = fun->allowSuperProperty();
 
-        if (kind == ClassConstructor || kind == DerivedClassConstructor) {
+        if (IsConstructorKind(kind)) {
             auto stmt = enclosing->findInnermostStatement<ParseContext::ClassStatement>();
             MOZ_ASSERT(stmt);
             stmt->constructorBox = this;
 
-            if (kind == DerivedClassConstructor) {
+            if (kind == FunctionSyntaxKind::DerivedClassConstructor) {
                 setDerivedClassConstructor();
                 allowSuperCall_ = true;
                 needsThisTDZChecks_ = true;
             }
         }
 
         thisBinding_ = ThisBinding::Function;
     }
@@ -2590,17 +2590,17 @@ Parser<FullParseHandler, CharT>::standal
     SourceParseContext funpc(this, funbox, newDirectives);
     if (!funpc.init())
         return null();
     funpc.setIsStandaloneFunctionBody();
 
     YieldHandling yieldHandling = GetYieldHandling(generatorKind);
     AwaitHandling awaitHandling = GetAwaitHandling(asyncKind);
     AutoAwaitIsKeyword<FullParseHandler, CharT> awaitIsKeyword(this, awaitHandling);
-    if (!functionFormalParametersAndBody(InAllowed, yieldHandling, &fn, Statement,
+    if (!functionFormalParametersAndBody(InAllowed, yieldHandling, &fn, FunctionSyntaxKind::Statement,
                                          parameterListEnd, /* isStandaloneFunction = */ true))
     {
         return null();
     }
 
     if (!tokenStream.getToken(&tt, TokenStream::Operand))
         return null();
     if (tt != TokenKind::Eof) {
@@ -2744,90 +2744,90 @@ GeneralParser<ParseHandler, CharT>::func
 
         if (pc->isAsync()) {
             handler.addStatementToList(stmtList, pn);
             pn = stmtList;
         }
     }
 
     MOZ_ASSERT_IF(!pc->isGenerator() && !pc->isAsync(), pc->lastYieldOffset == startYieldOffset);
-    MOZ_ASSERT_IF(pc->isGenerator(), kind != Arrow);
+    MOZ_ASSERT_IF(pc->isGenerator(), kind != FunctionSyntaxKind::Arrow);
     MOZ_ASSERT_IF(pc->isGenerator(), type == StatementListBody);
 
     if (pc->needsDotGeneratorName()) {
         MOZ_ASSERT_IF(!pc->isAsync(), type == StatementListBody);
         if (!declareDotGeneratorName())
             return null();
         Node generator = newDotGeneratorName();
         if (!generator)
             return null();
         if (!handler.prependInitialYield(pn, generator))
             return null();
     }
 
     // Declare the 'arguments' and 'this' bindings if necessary before
     // finishing up the scope so these special bindings get marked as closed
     // over if necessary. Arrow functions don't have these bindings.
-    if (kind != Arrow) {
+    if (kind != FunctionSyntaxKind::Arrow) {
         if (!declareFunctionArgumentsObject())
             return null();
         if (!declareFunctionThis())
             return null();
     }
 
     return finishLexicalScope(pc->varScope(), pn);
 }
 
 JSFunction*
 ParserBase::newFunction(HandleAtom atom, FunctionSyntaxKind kind,
                         GeneratorKind generatorKind, FunctionAsyncKind asyncKind,
                         HandleObject proto)
 {
-    MOZ_ASSERT_IF(kind == Statement, atom != nullptr);
+    MOZ_ASSERT_IF(kind == FunctionSyntaxKind::Statement, atom != nullptr);
 
     RootedFunction fun(context);
 
     gc::AllocKind allocKind = gc::AllocKind::FUNCTION;
     JSFunction::Flags flags;
 #ifdef DEBUG
     bool isGlobalSelfHostedBuiltin = false;
 #endif
     switch (kind) {
-      case Expression:
+      case FunctionSyntaxKind::Expression:
         flags = (generatorKind == GeneratorKind::NotGenerator &&
                  asyncKind == FunctionAsyncKind::SyncFunction
                  ? JSFunction::INTERPRETED_LAMBDA
                  : JSFunction::INTERPRETED_LAMBDA_GENERATOR_OR_ASYNC);
         break;
-      case Arrow:
+      case FunctionSyntaxKind::Arrow:
         flags = JSFunction::INTERPRETED_LAMBDA_ARROW;
         allocKind = gc::AllocKind::FUNCTION_EXTENDED;
         break;
-      case Method:
+      case FunctionSyntaxKind::Method:
         flags = (generatorKind == GeneratorKind::NotGenerator &&
                  asyncKind == FunctionAsyncKind::SyncFunction
                  ? JSFunction::INTERPRETED_METHOD
                  : JSFunction::INTERPRETED_METHOD_GENERATOR_OR_ASYNC);
         allocKind = gc::AllocKind::FUNCTION_EXTENDED;
         break;
-      case ClassConstructor:
-      case DerivedClassConstructor:
+      case FunctionSyntaxKind::ClassConstructor:
+      case FunctionSyntaxKind::DerivedClassConstructor:
         flags = JSFunction::INTERPRETED_CLASS_CONSTRUCTOR;
         allocKind = gc::AllocKind::FUNCTION_EXTENDED;
         break;
-      case Getter:
+      case FunctionSyntaxKind::Getter:
         flags = JSFunction::INTERPRETED_GETTER;
         allocKind = gc::AllocKind::FUNCTION_EXTENDED;
         break;
-      case Setter:
+      case FunctionSyntaxKind::Setter:
         flags = JSFunction::INTERPRETED_SETTER;
         allocKind = gc::AllocKind::FUNCTION_EXTENDED;
         break;
       default:
-        MOZ_ASSERT(kind == Statement);
+        MOZ_ASSERT(kind == FunctionSyntaxKind::Statement);
 #ifdef DEBUG
         if (options().selfHostingMode && !pc->isFunctionBox()) {
             isGlobalSelfHostedBuiltin = true;
             allocKind = gc::AllocKind::FUNCTION_EXTENDED;
         }
 #endif
         flags = (generatorKind == GeneratorKind::NotGenerator &&
                  asyncKind == FunctionAsyncKind::SyncFunction
@@ -2971,17 +2971,17 @@ GeneralParser<ParseHandler, CharT>::func
     //   ^
     Modifier firstTokenModifier = TokenStream::None;
 
     // Modifier for the the first token in each argument.
     // can be changed to TokenStream::None for the following case:
     //   async a => 1
     //         ^
     Modifier argModifier = TokenStream::Operand;
-    if (kind == Arrow) {
+    if (kind == FunctionSyntaxKind::Arrow) {
         TokenKind tt;
         // In async function, the first token after `async` is already gotten
         // with TokenStream::None.
         // In sync function, the first token is already gotten with
         // TokenStream::Operand.
         firstTokenModifier = funbox->isAsync() ? TokenStream::None : TokenStream::Operand;
         if (!tokenStream.peekToken(&tt, firstTokenModifier))
             return false;
@@ -2992,17 +2992,19 @@ GeneralParser<ParseHandler, CharT>::func
     }
 
     TokenPos firstTokenPos;
     if (!parenFreeArrow) {
         TokenKind tt;
         if (!tokenStream.getToken(&tt, firstTokenModifier))
             return false;
         if (tt != TokenKind::Lp) {
-            error(kind == Arrow ? JSMSG_BAD_ARROW_ARGS : JSMSG_PAREN_BEFORE_FORMAL);
+            error(kind == FunctionSyntaxKind::Arrow
+                  ? JSMSG_BAD_ARROW_ARGS
+                  : JSMSG_PAREN_BEFORE_FORMAL);
             return false;
         }
 
         firstTokenPos = pos();
 
         // Record the start of function source (for FunctionToString). If we
         // are parenFreeArrow, we will set this below, after consuming the NAME.
         funbox->setStart(anyChars);
@@ -3027,20 +3029,22 @@ GeneralParser<ParseHandler, CharT>::func
             return false;
         if (!matched)
             hasArguments = true;
     }
     if (hasArguments) {
         bool hasRest = false;
         bool hasDefault = false;
         bool duplicatedParam = false;
-        bool disallowDuplicateParams = kind == Arrow || kind == Method || kind == ClassConstructor;
+        bool disallowDuplicateParams = kind == FunctionSyntaxKind::Arrow ||
+                                       kind == FunctionSyntaxKind::Method ||
+                                       kind == FunctionSyntaxKind::ClassConstructor;
         AtomVector& positionalFormals = pc->positionalFormalParameterNames();
 
-        if (kind == Getter) {
+        if (kind == FunctionSyntaxKind::Getter) {
             error(JSMSG_ACCESSOR_WRONG_ARGS, "getter", "no", "s");
             return false;
         }
 
         while (true) {
             if (hasRest) {
                 error(JSMSG_PARAMETER_AFTER_REST);
                 return false;
@@ -3048,17 +3052,17 @@ GeneralParser<ParseHandler, CharT>::func
 
             TokenKind tt;
             if (!tokenStream.getToken(&tt, argModifier))
                 return false;
             argModifier = TokenStream::Operand;
             MOZ_ASSERT_IF(parenFreeArrow, TokenKindIsPossibleIdentifier(tt));
 
             if (tt == TokenKind::TripleDot) {
-                if (kind == Setter) {
+                if (kind == FunctionSyntaxKind::Setter) {
                     error(JSMSG_ACCESSOR_WRONG_ARGS, "setter", "one", "");
                     return false;
                 }
 
                 disallowDuplicateParams = true;
                 if (duplicatedParam) {
                     // Has duplicated args before the rest parameter.
                     error(JSMSG_BAD_DUP_ARGS);
@@ -3175,17 +3179,17 @@ GeneralParser<ParseHandler, CharT>::func
                 Node def_expr = assignExprWithoutYieldOrAwait(yieldHandling);
                 if (!def_expr)
                     return false;
                 if (!handler.setLastFunctionFormalParameterDefault(funcpn, def_expr))
                     return false;
             }
 
             // Setter syntax uniquely requires exactly one argument.
-            if (kind == Setter)
+            if (kind == FunctionSyntaxKind::Setter)
                 break;
 
             if (!tokenStream.matchToken(&matched, TokenKind::Comma, TokenStream::Operand))
                 return false;
             if (!matched)
                 break;
 
             if (!hasRest) {
@@ -3196,34 +3200,34 @@ GeneralParser<ParseHandler, CharT>::func
             }
         }
 
         if (!parenFreeArrow) {
             TokenKind tt;
             if (!tokenStream.getToken(&tt, TokenStream::Operand))
                 return false;
             if (tt != TokenKind::Rp) {
-                if (kind == Setter) {
+                if (kind == FunctionSyntaxKind::Setter) {
                     error(JSMSG_ACCESSOR_WRONG_ARGS, "setter", "one", "");
                     return false;
                 }
 
                 error(JSMSG_PAREN_AFTER_FORMAL);
                 return false;
             }
         }
 
         if (!hasDefault)
             funbox->length = positionalFormals.length() - hasRest;
 
         if (funbox->hasParameterExprs && funbox->hasDirectEval())
             funbox->hasDirectEvalInParameterExpr = true;
 
         funbox->function()->setArgCount(positionalFormals.length());
-    } else if (kind == Setter) {
+    } else if (kind == FunctionSyntaxKind::Setter) {
         error(JSMSG_ACCESSOR_WRONG_ARGS, "setter", "one", "");
         return false;
     }
 
     return true;
 }
 
 template <typename CharT>
@@ -3352,17 +3356,17 @@ template <class ParseHandler, typename C
 typename ParseHandler::Node
 GeneralParser<ParseHandler, CharT>::functionDefinition(Node funcNode, uint32_t toStringStart,
                                                        InHandling inHandling, YieldHandling yieldHandling,
                                                        HandleAtom funName, FunctionSyntaxKind kind,
                                                        GeneratorKind generatorKind,
                                                        FunctionAsyncKind asyncKind,
                                                        bool tryAnnexB /* = false */)
 {
-    MOZ_ASSERT_IF(kind == Statement, funName);
+    MOZ_ASSERT_IF(kind == FunctionSyntaxKind::Statement, funName);
 
     // When fully parsing a LazyScript, we do not fully reparse its inner
     // functions, which are also lazy. Instead, their free variables and
     // source extents are recorded and may be skipped.
     if (handler.canSkipLazyInnerFunctions()) {
         if (!skipLazyInnerFunction(funcNode, toStringStart, kind, tryAnnexB))
             return null();
 
@@ -3671,35 +3675,34 @@ Parser<FullParseHandler, CharT>::standal
     if (!funpc.init())
         return null();
 
     // Our tokenStream has no current token, so pn's position is garbage.
     // Substitute the position of the first token in our source. If the
     // function is a not-async arrow, use TokenStream::Operand to keep
     // verifyConsistentModifier from complaining (we will use
     // TokenStream::Operand in functionArguments).
-    Modifier modifier = (fun->isArrow() &&
-                         asyncKind == FunctionAsyncKind::SyncFunction)
+    Modifier modifier = (fun->isArrow() && asyncKind == FunctionAsyncKind::SyncFunction)
                         ? TokenStream::Operand
                         : TokenStream::None;
     if (!tokenStream.peekTokenPos(&pn->pn_pos, modifier))
         return null();
 
     YieldHandling yieldHandling = GetYieldHandling(generatorKind);
-    FunctionSyntaxKind syntaxKind = Statement;
+    FunctionSyntaxKind syntaxKind = FunctionSyntaxKind::Statement;
     if (fun->isClassConstructor())
-        syntaxKind = ClassConstructor;
+        syntaxKind = FunctionSyntaxKind::ClassConstructor;
     else if (fun->isMethod())
-        syntaxKind = Method;
+        syntaxKind = FunctionSyntaxKind::Method;
     else if (fun->isGetter())
-        syntaxKind = Getter;
+        syntaxKind = FunctionSyntaxKind::Getter;
     else if (fun->isSetter())
-        syntaxKind = Setter;
+        syntaxKind = FunctionSyntaxKind::Setter;
     else if (fun->isArrow())
-        syntaxKind = Arrow;
+        syntaxKind = FunctionSyntaxKind::Arrow;
 
     if (!functionFormalParametersAndBody(InAllowed, yieldHandling, &pn, syntaxKind)) {
         MOZ_ASSERT(directives == newDirectives);
         return null();
     }
 
     if (!FoldConstants(context, &pn, this))
         return null();
@@ -3721,34 +3724,35 @@ GeneralParser<ParseHandler, CharT>::func
     // parsing and such.
 
     FunctionBox* funbox = pc->functionBox();
     RootedFunction fun(context, funbox->function());
 
     // See below for an explanation why arrow function parameters and arrow
     // function bodies are parsed with different yield/await settings.
     {
-        AwaitHandling awaitHandling = funbox->isAsync() || (kind == Arrow && awaitIsKeyword())
-                                      ? AwaitIsKeyword
-                                      : AwaitIsName;
+        AwaitHandling awaitHandling =
+            (funbox->isAsync() || (kind == FunctionSyntaxKind::Arrow && awaitIsKeyword()))
+            ? AwaitIsKeyword
+            : AwaitIsName;
         AutoAwaitIsKeyword<ParseHandler, CharT> awaitIsKeyword(this, awaitHandling);
         if (!functionArguments(yieldHandling, kind, *pn))
             return false;
     }
 
     Maybe<ParseContext::VarScope> varScope;
     if (funbox->hasParameterExprs) {
         varScope.emplace(this);
         if (!varScope->init(pc))
             return false;
     } else {
         pc->functionScope().useAsVarScope(pc);
     }
 
-    if (kind == Arrow) {
+    if (kind == FunctionSyntaxKind::Arrow) {
         bool matched;
         if (!tokenStream.matchToken(&matched, TokenKind::Arrow))
             return false;
         if (!matched) {
             error(JSMSG_BAD_ARROW_ARGS);
             return false;
         }
     }
@@ -3762,17 +3766,17 @@ GeneralParser<ParseHandler, CharT>::func
 
     // Parse the function body.
     FunctionBodyType bodyType = StatementListBody;
     TokenKind tt;
     if (!tokenStream.getToken(&tt, TokenStream::Operand))
         return false;
     uint32_t openedPos = 0;
     if (tt != TokenKind::Lc) {
-        if (kind != Arrow) {
+        if (kind != FunctionSyntaxKind::Arrow) {
           error(JSMSG_CURLY_BEFORE_BODY);
           return false;
         }
 
         anyChars.ungetToken();
         bodyType = ExpressionBody;
         funbox->setHasExprBody();
     } else {
@@ -3792,27 +3796,27 @@ GeneralParser<ParseHandler, CharT>::func
     {
         AutoAwaitIsKeyword<ParseHandler, CharT> awaitIsKeyword(this, bodyAwaitHandling);
         body = functionBody(inHandling, bodyYieldHandling, kind, bodyType);
         if (!body)
             return false;
     }
 
     // Revalidate the function name when we transitioned to strict mode.
-    if ((kind == Statement || kind == Expression) &&
+    if ((kind == FunctionSyntaxKind::Statement || kind == FunctionSyntaxKind::Expression) &&
         fun->explicitName() &&
         !inheritedStrict &&
         pc->sc()->strict())
     {
         MOZ_ASSERT(pc->sc()->hasExplicitUseStrict(),
                    "strict mode should only change when a 'use strict' directive is present");
 
         PropertyName* propertyName = fun->explicitName()->asPropertyName();
         YieldHandling nameYieldHandling;
-        if (kind == Expression) {
+        if (kind == FunctionSyntaxKind::Expression) {
             // Named lambda has binding inside it.
             nameYieldHandling = bodyYieldHandling;
         } else {
             // Otherwise YieldHandling cannot be checked at this point
             // because of different context.
             // It should already be checked before this point.
             nameYieldHandling = YieldIsName;
         }
@@ -3826,22 +3830,22 @@ GeneralParser<ParseHandler, CharT>::func
     }
 
     if (bodyType == StatementListBody) {
         MUST_MATCH_TOKEN_MOD_WITH_REPORT(TokenKind::Rc, TokenStream::Operand,
                                          reportMissingClosing(JSMSG_CURLY_AFTER_BODY,
                                                               JSMSG_CURLY_OPENED, openedPos));
         funbox->setEnd(anyChars);
     } else {
-        MOZ_ASSERT(kind == Arrow);
+        MOZ_ASSERT(kind == FunctionSyntaxKind::Arrow);
 
         if (anyChars.hadError())
             return false;
         funbox->setEnd(anyChars);
-        if (kind == Statement && !matchOrInsertSemicolon())
+        if (kind == FunctionSyntaxKind::Statement && !matchOrInsertSemicolon())
             return false;
     }
 
     if (IsMethodDefinitionKind(kind) && pc->superScopeNeedsHomeObject())
         funbox->setNeedsHomeObject();
 
     if (!finishFunction(isStandaloneFunction))
         return false;
@@ -3935,18 +3939,18 @@ GeneralParser<ParseHandler, CharT>::func
     // This 'var' binding would be assigned the function object when its
     // declaration is reached, not at the start of the block.
     //
     // This semantics is implemented upon Scope exit in
     // Scope::propagateAndMarkAnnexBFunctionBoxes.
     bool tryAnnexB = kind == DeclarationKind::SloppyLexicalFunction;
 
     YieldHandling newYieldHandling = GetYieldHandling(generatorKind);
-    return functionDefinition(pn, toStringStart, InAllowed, newYieldHandling, name, Statement,
-                              generatorKind, asyncKind, tryAnnexB);
+    return functionDefinition(pn, toStringStart, InAllowed, newYieldHandling, name,
+                              FunctionSyntaxKind::Statement, generatorKind, asyncKind, tryAnnexB);
 }
 
 template <class ParseHandler, typename CharT>
 typename ParseHandler::Node
 GeneralParser<ParseHandler, CharT>::functionExpr(uint32_t toStringStart,
                                                  InvokedPrediction invoked,
                                                  FunctionAsyncKind asyncKind)
 {
@@ -8225,18 +8229,19 @@ GeneralParser<ParseHandler, CharT>::assi
             else
                 anyChars.ungetToken();
         }
 
         Node pn = handler.newArrowFunction(pos());
         if (!pn)
             return null();
 
-        return functionDefinition(pn, toStringStart, inHandling, yieldHandling, nullptr, Arrow,
-                                  GeneratorKind::NotGenerator, asyncKind);
+        return functionDefinition(pn, toStringStart, inHandling, yieldHandling, nullptr,
+                                  FunctionSyntaxKind::Arrow, GeneratorKind::NotGenerator,
+                                  asyncKind);
     }
 
     MOZ_ALWAYS_TRUE(tokenStream.getToken(&tokenAfterLHS, TokenStream::Operand));
 
     ParseNodeKind kind;
     switch (tokenAfterLHS) {
       case TokenKind::Assign:       kind = ParseNodeKind::Assign;       break;
       case TokenKind::AddAssign:    kind = ParseNodeKind::AddAssign;    break;
@@ -9735,36 +9740,36 @@ GeneralParser<ParseHandler, CharT>::obje
 template <class ParseHandler, typename CharT>
 typename ParseHandler::Node
 GeneralParser<ParseHandler, CharT>::methodDefinition(uint32_t toStringStart, PropertyType propType,
                                                      HandleAtom funName)
 {
     FunctionSyntaxKind kind;
     switch (propType) {
       case PropertyType::Getter:
-        kind = Getter;
+        kind = FunctionSyntaxKind::Getter;
         break;
 
       case PropertyType::Setter:
-        kind = Setter;
+        kind = FunctionSyntaxKind::Setter;
         break;
 
       case PropertyType::Method:
       case PropertyType::GeneratorMethod:
       case PropertyType::AsyncMethod:
       case PropertyType::AsyncGeneratorMethod:
-        kind = Method;
+        kind = FunctionSyntaxKind::Method;
         break;
 
       case PropertyType::Constructor:
-        kind = ClassConstructor;
+        kind = FunctionSyntaxKind::ClassConstructor;
         break;
 
       case PropertyType::DerivedConstructor:
-        kind = DerivedClassConstructor;
+        kind = FunctionSyntaxKind::DerivedClassConstructor;
         break;
 
       default:
         MOZ_CRASH("unexpected property type");
     }
 
     GeneratorKind generatorKind = (propType == PropertyType::GeneratorMethod ||
                                    propType == PropertyType::AsyncGeneratorMethod)
diff --git a/js/src/wasm/AsmJS.cpp b/js/src/wasm/AsmJS.cpp
--- a/js/src/wasm/AsmJS.cpp
+++ b/js/src/wasm/AsmJS.cpp
@@ -7201,24 +7201,26 @@ ParseFunction(ModuleValidator& m, ParseN
 
     ParseContext* outerpc = m.parser().pc;
     Directives directives(outerpc);
     FunctionBox* funbox = m.parser().newFunctionBox(fn, fun, toStringStart, directives,
                                                     GeneratorKind::NotGenerator,
                                                     FunctionAsyncKind::SyncFunction);
     if (!funbox)
         return false;
-    funbox->initWithEnclosingParseContext(outerpc, frontend::Statement);
+    funbox->initWithEnclosingParseContext(outerpc, FunctionSyntaxKind::Statement);
 
     Directives newDirectives = directives;
     SourceParseContext funpc(&m.parser(), funbox, &newDirectives);
     if (!funpc.init())
         return false;
 
-    if (!m.parser().functionFormalParametersAndBody(InAllowed, YieldIsName, &fn, Statement)) {
+    if (!m.parser().functionFormalParametersAndBody(InAllowed, YieldIsName, &fn,
+                                                    FunctionSyntaxKind::Statement))
+    {
         if (anyChars.hadError() || directives == newDirectives)
             return false;
 
         return m.fail(fn, "encountered new directive in function");
     }
 
     MOZ_ASSERT(!anyChars.hadError());
     MOZ_ASSERT(directives == newDirectives);
