# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1557843775 0
# Node ID 0a49234facf2496f920f6ba1ef9e506758466db3
# Parent  91652794ee91c5d508513948092d0e318427989c
Bug 1550868 - enforce a minimum version of clang-cl; r=glandium

We've not been checking the clang-cl version in use.  This lack of
checking is bad, for a couple of reasons:

* Released versions of clang-cl differ drastically in their robustness;
* Only the most recent version of clang-cl supports aarch64.

We should check for a minimum version of clang-cl, just like our other
supported compilers.  As a bonus, we can then start depending on
features that we know appear in the particular minimum clang-cl
version.  (The current patch is motivated by `/clang:` command-line
support, but one could pick other things.)

Differential Revision: https://phabricator.services.mozilla.com/D30723

diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -1035,16 +1035,22 @@ def compiler(language, host_or_target, c
             if host_or_target.os == 'Android':
                 raise FatalCheckError('GCC is not supported on Android.\n'
                                       'Please use clang from the Android NDK instead.')
             if info.version < '6.1.0':
                 raise FatalCheckError(
                     'Only GCC 6.1 or newer is supported (found version %s).'
                     % info.version)
 
+        if info.type == 'clang-cl':
+            if info.version < '8.0.0':
+                raise FatalCheckError(
+                    'Only clang-cl 8.0 or newer is supported (found version %s)'
+                    % info.version)
+
         # If you want to bump the version check here search for
         # diagnose_if above, and see the associated comment.
         if info.type == 'clang' and not info.version:
             raise FatalCheckError(
                 'Only clang/llvm 4.0 or newer is supported.')
 
         if info.type == 'msvc':
             # Require MSVC version 15.9.13 or higher
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
@@ -260,16 +260,24 @@ VS_PLATFORM_X86_64 = {
 CLANG_CL_3_9 = (CLANG_BASE('3.9.0') + VS('18.00.00000') + DEFAULT_C11 +
                 SUPPORTS_GNU99 + SUPPORTS_GNUXX11 + SUPPORTS_CXX14) + {
     '*.cpp': {
         '__STDC_VERSION__': False,
         '__cplusplus': '201103L',
     },
 }
 
+CLANG_CL_8_0 = (CLANG_BASE('8.0.0') + VS('18.00.00000') + DEFAULT_C11 +
+                SUPPORTS_GNU99 + SUPPORTS_GNUXX11 + SUPPORTS_CXX14) + {
+    '*.cpp': {
+        '__STDC_VERSION__': False,
+        '__cplusplus': '201103L',
+    },
+}
+
 CLANG_CL_PLATFORM_X86 = FakeCompiler(VS_PLATFORM_X86, GCC_PLATFORM_X86[None])
 CLANG_CL_PLATFORM_X86_64 = FakeCompiler(VS_PLATFORM_X86_64, GCC_PLATFORM_X86_64[None])
 
 LIBRARY_NAME_INFOS = {
     'linux-gnu': {
         'DLL_PREFIX': 'lib',
         'DLL_SUFFIX': '.so',
         'LIB_PREFIX': 'lib',
@@ -872,17 +880,18 @@ class WindowsToolchainTest(BaseToolchain
         '/opt/VS_2013u3/bin/cl': VS_2013u3 + VS_PLATFORM_X86,
         '/opt/VS_2015/bin/cl': VS_2015 + VS_PLATFORM_X86,
         '/opt/VS_2015u1/bin/cl': VS_2015u1 + VS_PLATFORM_X86,
         '/opt/VS_2015u2/bin/cl': VS_2015u2 + VS_PLATFORM_X86,
         '/opt/VS_2015u3/bin/cl': VS_2015u3 + VS_PLATFORM_X86,
         '/opt/VS_2017u4/bin/cl': VS_2017u4 + VS_PLATFORM_X86,
         '/opt/VS_2017u6/bin/cl': VS_2017u6 + VS_PLATFORM_X86,
         '/usr/bin/cl': VS_2017u8 + VS_PLATFORM_X86,
-        '/usr/bin/clang-cl': CLANG_CL_3_9 + CLANG_CL_PLATFORM_X86,
+        '/usr/bin/clang-cl-3.9': CLANG_CL_3_9 + CLANG_CL_PLATFORM_X86,
+        '/usr/bin/clang-cl': CLANG_CL_8_0 + CLANG_CL_PLATFORM_X86,
         '/usr/bin/gcc': DEFAULT_GCC + GCC_PLATFORM_X86_WIN,
         '/usr/bin/g++': DEFAULT_GXX + GCC_PLATFORM_X86_WIN,
         '/usr/bin/gcc-4.9': GCC_4_9 + GCC_PLATFORM_X86_WIN,
         '/usr/bin/g++-4.9': GXX_4_9 + GCC_PLATFORM_X86_WIN,
         '/usr/bin/gcc-5': GCC_5 + GCC_PLATFORM_X86_WIN,
         '/usr/bin/g++-5': GXX_5 + GCC_PLATFORM_X86_WIN,
         '/usr/bin/gcc-6': GCC_6 + GCC_PLATFORM_X86_WIN,
         '/usr/bin/g++-6': GXX_6 + GCC_PLATFORM_X86_WIN,
@@ -915,24 +924,26 @@ class WindowsToolchainTest(BaseToolchain
     )
     VSXX_2017u8_RESULT = CompilerResult(
         flags=[],
         version='19.15.26726',
         type='msvc',
         compiler='/usr/bin/cl',
         language='C++',
     )
-    CLANG_CL_3_9_RESULT = CompilerResult(
+    CLANG_CL_3_9_RESULT = 'Only clang-cl 8.0 or newer is supported (found version 3.9.0)'
+    CLANG_CL_8_0_RESULT = CompilerResult(
         version='18.00.00000',
         flags=['-Xclang', '-std=gnu99'],
         type='clang-cl',
         compiler='/usr/bin/clang-cl',
         language='C',
     )
-    CLANGXX_CL_3_9_RESULT = CompilerResult(
+    CLANGXX_CL_3_9_RESULT = 'Only clang-cl 8.0 or newer is supported (found version 3.9.0)'
+    CLANGXX_CL_8_0_RESULT = CompilerResult(
         version='18.00.00000',
         flags=['-Xclang', '-std=c++14'],
         type='clang-cl',
         compiler='/usr/bin/clang-cl',
         language='C++',
     )
     CLANG_3_3_RESULT = LinuxToolchainTest.CLANG_3_3_RESULT
     CLANGXX_3_3_RESULT = LinuxToolchainTest.CLANGXX_3_3_RESULT
@@ -1003,20 +1014,27 @@ class WindowsToolchainTest(BaseToolchain
         })
 
         self.do_toolchain_test(self.PATHS, {
             'c_compiler': self.VS_2013u2_RESULT,
         }, environ={
             'CC': '/opt/VS_2013u2/bin/cl',
         })
 
+    def test_unsupported_clang_cl(self):
+        self.do_toolchain_test(self.PATHS, {
+            'c_compiler': self.CLANG_CL_3_9_RESULT,
+        }, environ={
+            'CC': '/usr/bin/clang-cl-3.9',
+        })
+
     def test_clang_cl(self):
         self.do_toolchain_test(self.PATHS, {
-            'c_compiler': self.CLANG_CL_3_9_RESULT,
-            'cxx_compiler': self.CLANGXX_CL_3_9_RESULT,
+            'c_compiler': self.CLANG_CL_8_0_RESULT,
+            'cxx_compiler': self.CLANGXX_CL_8_0_RESULT,
         })
 
     def test_gcc(self):
         # We'll pick GCC if msvc and clang-cl can't be found.
         paths = {
             k: v for k, v in self.PATHS.iteritems()
             if os.path.basename(k) not in ('cl', 'clang-cl')
         }
@@ -1074,17 +1092,18 @@ class Windows64ToolchainTest(WindowsTool
         '/opt/VS_2013u3/bin/cl': VS_2013u3 + VS_PLATFORM_X86_64,
         '/opt/VS_2015/bin/cl': VS_2015 + VS_PLATFORM_X86_64,
         '/opt/VS_2015u1/bin/cl': VS_2015u1 + VS_PLATFORM_X86_64,
         '/opt/VS_2015u2/bin/cl': VS_2015u2 + VS_PLATFORM_X86_64,
         '/opt/VS_2015u3/bin/cl': VS_2015u3 + VS_PLATFORM_X86_64,
         '/opt/VS_2017u4/bin/cl': VS_2017u4 + VS_PLATFORM_X86_64,
         '/opt/VS_2017u6/bin/cl': VS_2017u6 + VS_PLATFORM_X86_64,
         '/usr/bin/cl': VS_2017u8 + VS_PLATFORM_X86_64,
-        '/usr/bin/clang-cl': CLANG_CL_3_9 + CLANG_CL_PLATFORM_X86_64,
+        '/usr/bin/clang-cl': CLANG_CL_8_0 + CLANG_CL_PLATFORM_X86_64,
+        '/usr/bin/clang-cl-3.9': CLANG_CL_3_9 + CLANG_CL_PLATFORM_X86_64,
         '/usr/bin/gcc': DEFAULT_GCC + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/g++': DEFAULT_GXX + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/gcc-4.9': GCC_4_9 + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/g++-4.9': GXX_4_9 + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/gcc-5': GCC_5 + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/g++-5': GXX_5 + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/gcc-6': GCC_6 + GCC_PLATFORM_X86_64_WIN,
         '/usr/bin/g++-6': GXX_6 + GCC_PLATFORM_X86_64_WIN,
@@ -1457,22 +1476,22 @@ class WindowsCrossToolchainTest(BaseTool
             'c_compiler': WindowsToolchainTest.VS_2017u6_RESULT,
             'cxx_compiler': WindowsToolchainTest.VSXX_2017u6_RESULT,
             'host_c_compiler': self.DEFAULT_CLANG_RESULT,
             'host_cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
         })
 
     def test_clang_cl_cross(self):
         paths = {
-            '/usr/bin/clang-cl': CLANG_CL_3_9 + CLANG_CL_PLATFORM_X86_64,
+            '/usr/bin/clang-cl': CLANG_CL_8_0 + CLANG_CL_PLATFORM_X86_64,
         }
         paths.update(LinuxToolchainTest.PATHS)
         self.do_toolchain_test(paths, {
-            'c_compiler': WindowsToolchainTest.CLANG_CL_3_9_RESULT,
-            'cxx_compiler': WindowsToolchainTest.CLANGXX_CL_3_9_RESULT,
+            'c_compiler': WindowsToolchainTest.CLANG_CL_8_0_RESULT,
+            'cxx_compiler': WindowsToolchainTest.CLANGXX_CL_8_0_RESULT,
             'host_c_compiler': self.DEFAULT_CLANG_RESULT,
             'host_cxx_compiler': self.DEFAULT_CLANGXX_RESULT,
         })
 
 
 class OpenBSDToolchainTest(BaseToolchainTest):
     HOST = 'x86_64-unknown-openbsd6.1'
     TARGET = 'x86_64-unknown-openbsd6.1'
