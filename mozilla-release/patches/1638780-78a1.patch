# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1589890067 0
# Node ID 8913deea254883ab20c50de795726a4f0f63224f
# Parent  85b6a44aa18c30c7aaae703123fe67b35402e0a0
Bug 1638780 - Don't import `virtualenv` unless necessary in `mach` commands r=froydnj

The module `distutils.sysconfig` isn't installed by default on Ubuntu -- the package `python3-distutils` exposes it. That's fine, except we unconditionally import it in `virtualenv.py`, which gets up indirectly being imported whenever you run any `mach` command, which will cause `bootstrap` to break before it even has the chance to install it.

`python3-pip` seems to rely on `python3-distutils` being installed so `bootstrap` will install it, so all we need to do is make sure it doesn't import `virtualenv` in any circumstance unless it's necessary (when surfacing an error in the module would be appropriate).

Differential Revision: https://phabricator.services.mozilla.com/D75833

diff --git a/python/mozbuild/mozbuild/base.py b/python/mozbuild/mozbuild/base.py
--- a/python/mozbuild/mozbuild/base.py
+++ b/python/mozbuild/mozbuild/base.py
@@ -38,17 +38,16 @@ from .mozconfig import (
     MozconfigLoadException,
     MozconfigLoader,
 )
 from .pythonutil import find_python3_executable
 from .util import (
     memoize,
     memoized_property,
 )
-from .virtualenv import VirtualenvManager
 
 
 def ancestors(path):
     """Emit the parent directories of a path."""
     while path:
         yield path
         newpath = os.path.dirname(path)
         if newpath == path:
@@ -256,16 +255,18 @@ class MozbuildObject(ProcessExecutionMix
         if self._topobjdir is None:
             self._topobjdir = self.resolve_mozconfig_topobjdir(
                 default='obj-@CONFIG_GUESS@')
 
         return self._topobjdir
 
     @property
     def virtualenv_manager(self):
+        from .virtualenv import VirtualenvManager
+
         if self._virtualenv_manager is None:
             name = "init"
             if six.PY3:
                 name += "_py3"
             self._virtualenv_manager = VirtualenvManager(
                 self.topsrcdir,
                 self.topobjdir,
                 os.path.join(self.topobjdir, '_virtualenvs', name),
