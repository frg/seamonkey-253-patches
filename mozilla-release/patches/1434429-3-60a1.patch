# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1516304067 28800
#      Thu Jan 18 11:34:27 2018 -0800
# Node ID d353d4a61cb0c01435225f77e5cec73348529409
# Parent  8a3135a1bce93c7403df54f4897efeaa9504e764
Bug 1434429 - Move TokenStreamSpecific::ungetCharIgnoreEOL into TokenStreamCharsBase.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -541,17 +541,17 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 
 // This gets the next char. It does nothing special with EOL sequences, not
 // even updating the line counters.  It can be used safely if (a) the
 // resulting char is guaranteed to be ungotten (by ungetCharIgnoreEOL()) if
 // it's an EOL, and (b) the line-related state (lineno, linebase) is not used
 // before it's ungotten.
 template<typename CharT, class AnyCharsAccess>
 int32_t
-TokenStreamSpecific<CharT, AnyCharsAccess>::getCharIgnoreEOL()
+GeneralTokenStreamChars<CharT, AnyCharsAccess>::getCharIgnoreEOL()
 {
     if (MOZ_LIKELY(userbuf.hasRawChars()))
         return userbuf.getRawChar();
 
     anyCharsAccess().flags.isEOF = true;
     return EOF;
 }
 
@@ -584,19 +584,19 @@ GeneralTokenStreamChars<CharT, AnyCharsA
             userbuf.matchRawCharBackwards('\r');
 
         anyCharsAccess().undoGetChar();
     } else {
         MOZ_ASSERT(userbuf.peekRawChar() == c);
     }
 }
 
-template<typename CharT, class AnyCharsAccess>
+template<typename CharT>
 void
-TokenStreamSpecific<CharT, AnyCharsAccess>::ungetCharIgnoreEOL(int32_t c)
+TokenStreamCharsBase<CharT>::ungetCharIgnoreEOL(int32_t c)
 {
     if (c == EOF)
         return;
 
     MOZ_ASSERT(!userbuf.atStart());
     userbuf.ungetRawChar();
 }
 
@@ -1272,21 +1272,19 @@ IsTokenSane(Token* tp)
 }
 #endif
 
 template<class AnyCharsAccess>
 bool
 TokenStreamChars<char16_t, AnyCharsAccess>::matchTrailForLeadSurrogate(char16_t lead,
                                                                        uint32_t* codePoint)
 {
-    TokenStreamSpecific* ts = asSpecific();
-
-    int32_t maybeTrail = ts->getCharIgnoreEOL();
+    int32_t maybeTrail = getCharIgnoreEOL();
     if (!unicode::IsTrailSurrogate(maybeTrail)) {
-        ts->ungetCharIgnoreEOL(maybeTrail);
+        ungetCharIgnoreEOL(maybeTrail);
         return false;
     }
 
     *codePoint = unicode::UTF16Decode(lead, maybeTrail);
     return true;
 }
 
 template<>
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -808,16 +808,19 @@ class TokenStreamAnyChars
     JSContext* const    cx;
     bool                mutedErrors;
     StrictModeGetter*   strictModeGetter;  // used to test for strict mode
 };
 
 template<typename CharT>
 class TokenStreamCharsBase
 {
+  protected:
+    void ungetCharIgnoreEOL(int32_t c);
+
   public:
     using CharBuffer = Vector<CharT, 32>;
 
     TokenStreamCharsBase(JSContext* cx, const CharT* chars, size_t length, size_t startOffset);
 
     static MOZ_ALWAYS_INLINE JSAtom*
     atomizeChars(JSContext* cx, const CharT* chars, size_t length);
 
@@ -1018,36 +1021,42 @@ class GeneralTokenStreamChars
 
     TokenStreamSpecific* asSpecific() {
         static_assert(mozilla::IsBaseOf<GeneralTokenStreamChars, TokenStreamSpecific>::value,
                       "static_cast below presumes an inheritance relationship");
 
         return static_cast<TokenStreamSpecific*>(this);
     }
 
+    int32_t getCharIgnoreEOL();
+
     void ungetChar(int32_t c);
 };
 
 template<typename CharT, class AnyCharsAccess> class TokenStreamChars;
 
 template<class AnyCharsAccess>
 class TokenStreamChars<char16_t, AnyCharsAccess>
   : public GeneralTokenStreamChars<char16_t, AnyCharsAccess>
 {
+  private:
     using Self = TokenStreamChars<char16_t, AnyCharsAccess>;
     using GeneralCharsBase = GeneralTokenStreamChars<char16_t, AnyCharsAccess>;
     using CharsSharedBase = TokenStreamCharsBase<char16_t>;
 
+    using GeneralCharsBase::asSpecific;
+
+    using typename GeneralCharsBase::TokenStreamSpecific;
+
     bool matchTrailForLeadSurrogate(char16_t lead, uint32_t* codePoint);
 
-  public:
-    using typename GeneralCharsBase::TokenStreamSpecific;
-
-    using GeneralCharsBase::asSpecific;
+  protected:
     using GeneralCharsBase::anyCharsAccess;
+    using GeneralCharsBase::getCharIgnoreEOL;
+    using CharsSharedBase::ungetCharIgnoreEOL;
 
   public:
     using GeneralCharsBase::GeneralCharsBase;
 
     MOZ_ALWAYS_INLINE bool isMultiUnitCodepoint(char16_t c, uint32_t* codepoint) {
         if (MOZ_LIKELY(!unicode::IsLeadSurrogate(c)))
             return false;
 
@@ -1127,19 +1136,21 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
   private:
     using typename CharsSharedBase::CharBuffer;
     using typename CharsSharedBase::TokenBuf;
 
   private:
     using CharsSharedBase::appendMultiUnitCodepointToTokenbuf;
     using CharsSharedBase::atomizeChars;
     using CharsSharedBase::copyTokenbufTo;
+    using GeneralCharsBase::getCharIgnoreEOL;
     using CharsBase::isMultiUnitCodepoint;
     using CharsSharedBase::tokenbuf;
     using GeneralCharsBase::ungetChar;
+    using CharsSharedBase::ungetCharIgnoreEOL;
     using CharsSharedBase::userbuf;
 
   public:
     TokenStreamSpecific(JSContext* cx, const ReadOnlyCompileOptions& options,
                         const CharT* base, size_t length);
 
     // If there is an invalid escape in a template, report it and return false,
     // otherwise return true.
@@ -1407,19 +1418,17 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
 
     MOZ_MUST_USE bool getStringOrTemplateToken(int untilChar, Token** tp);
 
     // Try to get the next character, normalizing '\r', '\r\n', and '\n' into
     // '\n'.  Also updates internal line-counter state.  Return true on success
     // and store the character in |*c|.  Return false and leave |*c| undefined
     // on failure.
     MOZ_MUST_USE bool getChar(int32_t* cp);
-    int32_t getCharIgnoreEOL();
 
-    void ungetCharIgnoreEOL(int32_t c);
     Token* newToken(ptrdiff_t adjust);
     uint32_t peekUnicodeEscape(uint32_t* codePoint);
     uint32_t peekExtendedUnicodeEscape(uint32_t* codePoint);
     uint32_t matchUnicodeEscapeIdStart(uint32_t* codePoint);
     bool matchUnicodeEscapeIdent(uint32_t* codePoint);
     bool peekChars(int n, CharT* cp);
 
     MOZ_MUST_USE bool getDirectives(bool isMultiline, bool shouldWarnDeprecated);
