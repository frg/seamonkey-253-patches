# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1586274909 0
# Node ID be8356bf09d78157bdd3d69c5548dea7be6bda29
# Parent  4a3b9cf93c39617e48cd347b2602f224c5ac2d56
Bug 1624670 - Cap ProcessPoolExecutor's max_workers to 60 on Windows. r=firefox-build-system-reviewers,rstewart

See https://bugs.python.org/issue26903#msg365886.

Differential Revision: https://phabricator.services.mozilla.com/D69921

diff --git a/python/mozbuild/mozbuild/frontend/reader.py b/python/mozbuild/mozbuild/frontend/reader.py
--- a/python/mozbuild/mozbuild/frontend/reader.py
+++ b/python/mozbuild/mozbuild/frontend/reader.py
@@ -29,16 +29,17 @@ import traceback
 import types
 
 from collections import (
     defaultdict,
     OrderedDict,
 )
 from io import StringIO
 from itertools import chain
+from multiprocessing import cpu_count
 import six
 from six import string_types
 
 from mozbuild.util import (
     EmptyValue,
     HierarchicalStringList,
     memoize,
     ReadOnlyDefaultDict,
@@ -847,19 +848,27 @@ class BuildReader(object):
         self._relevant_mozbuild_finder = FileFinder(self.config.topsrcdir,
                                                     ignore=ignores)
 
         # Also ignore any other directories that could be objdirs, they don't
         # necessarily start with the string 'obj'.
         for path, f in self._relevant_mozbuild_finder.find('*/config.status'):
             self._relevant_mozbuild_finder.ignore.add(os.path.dirname(path))
 
-        # ProcessPoolExecutor will naturally default to the number of CPUs
-        # on the machine and will also handle edge cases on Windows.
-        self._gyp_worker_pool = ProcessPoolExecutor()
+        max_workers = cpu_count()
+        if sys.platform.startswith('win'):
+            # In python 3, on Windows, ProcessPoolExecutor uses
+            # _winapi.WaitForMultipleObjects, which doesn't work on large
+            # number of objects. It also has some automatic capping to avoid
+            # _winapi.WaitForMultipleObjects being unhappy as a consequence,
+            # but that capping is actually insufficient in python 3.7 and 3.8
+            # (as well as inexistent in older versions). So we cap ourselves
+            # to 60, see https://bugs.python.org/issue26903#msg365886.
+            max_workers = min(max_workers, 60)
+        self._gyp_worker_pool = ProcessPoolExecutor(max_workers=max_workers)
         self._gyp_processors = []
         self._execution_time = 0.0
         self._file_count = 0
         self._gyp_execution_time = 0.0
         self._gyp_file_count = 0
 
     def summary(self):
         return ExecutionSummary(
