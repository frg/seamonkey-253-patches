# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1521031364 14400
# Node ID d2252a25471271ce473936fcfd6c689f9bce3273
# Parent  a60f73984bb766671845a11afe62d8e2021a083f
Bug 1445403 - Move FocusState method implementations into .cpp file. r=rhunt

MozReview-Commit-ID: FycCdfjdEWK

diff --git a/gfx/layers/apz/src/FocusState.cpp b/gfx/layers/apz/src/FocusState.cpp
--- a/gfx/layers/apz/src/FocusState.cpp
+++ b/gfx/layers/apz/src/FocusState.cpp
@@ -17,16 +17,22 @@ FocusState::FocusState()
   , mLastContentProcessedEvent(0)
   , mFocusHasKeyEventListeners(false)
   , mFocusLayersId(0)
   , mFocusHorizontalTarget(FrameMetrics::NULL_SCROLL_ID)
   , mFocusVerticalTarget(FrameMetrics::NULL_SCROLL_ID)
 {
 }
 
+uint64_t
+FocusState::LastAPZProcessedEvent() const
+{
+  return mLastAPZProcessedEvent;
+}
+
 bool
 FocusState::IsCurrent() const
 {
   FS_LOG("Checking IsCurrent() with cseq=%" PRIu64 ", aseq=%" PRIu64 "\n",
          mLastContentProcessedEvent,
          mLastAPZProcessedEvent);
 
   MOZ_ASSERT(mLastContentProcessedEvent <= mLastAPZProcessedEvent);
@@ -171,10 +177,16 @@ FocusState::GetVerticalTarget() const
   if (!IsCurrent() ||
       mFocusHasKeyEventListeners ||
       mFocusVerticalTarget == FrameMetrics::NULL_SCROLL_ID) {
     return Nothing();
   }
   return Some(ScrollableLayerGuid(mFocusLayersId, 0, mFocusVerticalTarget));
 }
 
+bool
+FocusState::CanIgnoreKeyboardShortcutMisses() const
+{
+  return IsCurrent() && !mFocusHasKeyEventListeners;
+}
+
 } // namespace layers
 } // namespace mozilla
diff --git a/gfx/layers/apz/src/FocusState.h b/gfx/layers/apz/src/FocusState.h
--- a/gfx/layers/apz/src/FocusState.h
+++ b/gfx/layers/apz/src/FocusState.h
@@ -72,17 +72,17 @@ public:
   FocusState();
 
   /**
    * The sequence number of the last potentially focus changing event processed
    * by APZ. This number starts at one and increases monotonically. This number
    * will never be zero as that is used to catch uninitialized focus sequence
    * numbers on input events.
    */
-  uint64_t LastAPZProcessedEvent() const { return mLastAPZProcessedEvent; }
+  uint64_t LastAPZProcessedEvent() const;
 
   /**
    * Whether the current focus state is known to be current or else if an event
    * has been processed that could change the focus but we have not received an
    * update with a new confirmed target.
    */
   bool IsCurrent() const;
 
@@ -126,20 +126,17 @@ public:
    * The same as GetHorizontalTarget() but for vertical scrolling.
    */
   Maybe<ScrollableLayerGuid> GetVerticalTarget() const;
 
   /**
    * Gets whether it is safe to not increment the focus sequence number for an
    * unmatched keyboard event.
    */
-  bool CanIgnoreKeyboardShortcutMisses() const
-  {
-    return IsCurrent() && !mFocusHasKeyEventListeners;
-  }
+  bool CanIgnoreKeyboardShortcutMisses() const;
 
 private:
   // The set of focus targets received indexed by their layer tree ID
   std::unordered_map<uint64_t, FocusTarget> mFocusTree;
 
   // The focus sequence number of the last potentially focus changing event
   // processed by APZ. This number starts at one and increases monotonically.
   // We don't worry about wrap around here because at a pace of 100 increments/sec,
