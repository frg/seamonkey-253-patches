# HG changeset patch
# User Benjamin Bouvier <benj@benj.me>
# Date 1518446703 -3600
# Node ID 0e1e3fb7d63a15281557e9ddf9d86b7b45766b87
# Parent  97f4a5cff8faa70dffdd9e95b6412cbe9e91d8e6
Bug 1437501: Ignore wasm::FailFP when unwinding jit->wasm frames; r=luke

MozReview-Commit-ID: 30x0ErsjDzj

diff --git a/js/src/jit-test/tests/wasm/ion-error-throw.js b/js/src/jit-test/tests/wasm/ion-error-throw.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/wasm/ion-error-throw.js
@@ -0,0 +1,44 @@
+const options = getJitCompilerOptions();
+
+// These tests need at least baseline to make sense.
+if (!options['baseline.enable'])
+    quit();
+
+const { assertStackTrace, startProfiling, endProfiling, assertEqPreciseStacks } = WasmHelpers;
+
+enableGeckoProfiling();
+
+let { add } = wasmEvalText(`(module
+    (func $add (export "add") (result i32) (param i32) (param i32)
+     get_local 0
+     i32.const 42
+     i32.eq
+     if
+         unreachable
+     end
+
+     get_local 0
+     get_local 1
+     i32.add
+    )
+)`).exports;
+
+const SLOW_ENTRY_STACK = ['', '!>', '0,!>', '!>', ''];
+const FAST_ENTRY_STACK = ['', '>', '0,>', '>', ''];
+
+function main() {
+    for (let i = 0; i < 50; i++) {
+        startProfiling();
+        try {
+            assertEq(add(i, i+1), 2*i+1);
+        } catch (e) {
+            assertEq(i, 42);
+            assertEq(e.message.includes("unreachable"), true);
+            assertStackTrace(e, ['wasm-function[0]', 'main', '']);
+        }
+        let stack = endProfiling();
+        assertEqPreciseStacks(stack, [FAST_ENTRY_STACK, SLOW_ENTRY_STACK]);
+    }
+}
+
+main();
diff --git a/js/src/wasm/WasmFrameIter.cpp b/js/src/wasm/WasmFrameIter.cpp
--- a/js/src/wasm/WasmFrameIter.cpp
+++ b/js/src/wasm/WasmFrameIter.cpp
@@ -935,16 +935,20 @@ js::wasm::StartUnwinding(const RegisterS
             // frame is incomplete. During profiling frame iteration, it means
             // that the jit profiling frame iterator won't be able to unwind
             // this frame; drop it.
             return false;
         }
 #endif
         fixedFP = offsetFromEntry < SetJitEntryFP ? (Frame*) sp : fp;
         fixedPC = nullptr;
+
+        // On the error return path, FP might be set to FailFP. Ignore these transient frames.
+        if (intptr_t(fixedFP) == (FailFP & ~JitActivation::ExitFpWasmBit))
+            return false;
         break;
       case CodeRange::Throw:
         // The throw stub executes a small number of instructions before popping
         // the entire activation. To simplify testing, we simply pretend throw
         // stubs have already popped the entire stack.
         return false;
       case CodeRange::Interrupt:
         // When the PC is in the async interrupt stub, the fp may be garbage and
