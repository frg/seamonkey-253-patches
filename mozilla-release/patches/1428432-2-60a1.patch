# HG changeset patch
# User Yura Zenevich <yura.zenevich@gmail.com>
# Date 1517948461 18000
# Node ID 034c72b76050e6a0e65567724e4f32b4f3c529db
# Parent  09f69241d0a895510228809479d8305022da596b
Bug 1428432 - further improve keyboard accessibility for VirtualizedTree. r=nchevobbe

MozReview-Commit-ID: 8Es17Q5J87a

diff --git a/devtools/client/shared/components/VirtualizedTree.js b/devtools/client/shared/components/VirtualizedTree.js
--- a/devtools/client/shared/components/VirtualizedTree.js
+++ b/devtools/client/shared/components/VirtualizedTree.js
@@ -190,16 +190,19 @@ class Tree extends Component {
       // Optional props
 
       // The currently focused item, if any such item exists.
       focused: PropTypes.any,
 
       // Handle when a new item is focused.
       onFocus: PropTypes.func,
 
+      // Handle when item is activated with a keyboard (using Space or Enter)
+      onActivate: PropTypes.func,
+
       // The depth to which we should automatically expand new items.
       autoExpandDepth: PropTypes.number,
 
       // Note: the two properties below are mutually exclusive. Only one of the
       // label properties is necessary.
       // ID of an element whose textual content serves as an accessible label for
       // a tree.
       labelledby: PropTypes.string,
@@ -238,16 +241,19 @@ class Tree extends Component {
     };
 
     this._onExpand = oncePerAnimationFrame(this._onExpand).bind(this);
     this._onCollapse = oncePerAnimationFrame(this._onCollapse).bind(this);
     this._onScroll = oncePerAnimationFrame(this._onScroll).bind(this);
     this._focusPrevNode = oncePerAnimationFrame(this._focusPrevNode).bind(this);
     this._focusNextNode = oncePerAnimationFrame(this._focusNextNode).bind(this);
     this._focusParentNode = oncePerAnimationFrame(this._focusParentNode).bind(this);
+    this._focusFirstNode = oncePerAnimationFrame(this._focusFirstNode).bind(this);
+    this._focusLastNode = oncePerAnimationFrame(this._focusLastNode).bind(this);
+    this._activateNode = oncePerAnimationFrame(this._activateNode).bind(this);
 
     this._autoExpand = this._autoExpand.bind(this);
     this._preventArrowKeyScrolling = this._preventArrowKeyScrolling.bind(this);
     this._updateHeight = this._updateHeight.bind(this);
     this._dfs = this._dfs.bind(this);
     this._dfsFromRoots = this._dfsFromRoots.bind(this);
     this._focus = this._focus.bind(this);
     this._onBlur = this._onBlur.bind(this);
@@ -492,19 +498,49 @@ class Tree extends Component {
 
       case "ArrowRight":
         if (!this.props.isExpanded(this.props.focused)) {
           this._onExpand(this.props.focused);
         } else {
           this._focusNextNode();
         }
         break;
+
+      case "Home":
+        this._focusFirstNode();
+        break;
+
+      case "End":
+        this._focusLastNode();
+        break;
+
+      case "Enter":
+      case " ":
+        this._activateNode();
+        break;
     }
   }
 
+  _activateNode() {
+    if (this.props.onActivate) {
+      this.props.onActivate(this.props.focused);
+    }
+  }
+
+  _focusFirstNode() {
+    const traversal = this._dfsFromRoots();
+    this._focus(0, traversal[0].item);
+  }
+
+  _focusLastNode() {
+    const traversal = this._dfsFromRoots();
+    const lastIndex = traversal.length - 1;
+    this._focus(lastIndex, traversal[lastIndex].item);
+  }
+
   /**
    * Sets the previous node relative to the currently focused item, to focused.
    */
   _focusPrevNode() {
     // Start a depth first search and keep going until we reach the currently
     // focused node. Focus the previous node in the DFS, if it exists. If it
     // doesn't exist, we're at the first node already.
 
diff --git a/devtools/client/shared/components/test/mochitest/chrome.ini b/devtools/client/shared/components/test/mochitest/chrome.ini
--- a/devtools/client/shared/components/test/mochitest/chrome.ini
+++ b/devtools/client/shared/components/test/mochitest/chrome.ini
@@ -21,8 +21,9 @@ support-files =
 [test_tree_04.html]
 [test_tree_05.html]
 [test_tree_06.html]
 [test_tree_07.html]
 [test_tree_08.html]
 [test_tree_09.html]
 [test_tree_10.html]
 [test_tree_11.html]
+[test_tree_12.html]
diff --git a/devtools/client/shared/components/test/mochitest/test_tree_12.html b/devtools/client/shared/components/test/mochitest/test_tree_12.html
new file mode 100644
--- /dev/null
+++ b/devtools/client/shared/components/test/mochitest/test_tree_12.html
@@ -0,0 +1,257 @@
+<!-- This Source Code Form is subject to the terms of the Mozilla Public
+   - License, v. 2.0. If a copy of the MPL was not distributed with this
+   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
+<!DOCTYPE HTML>
+<html>
+<!--
+Test keyboard navigation/activation with the VirtualizedTree component.
+-->
+<head>
+  <meta charset="utf-8">
+  <title>Tree component test</title>
+  <script type="application/javascript" src="chrome://mochikit/content/tests/SimpleTest/SimpleTest.js"></script>
+  <link rel="stylesheet" type="text/css" href="chrome://mochikit/content/tests/SimpleTest/test.css">
+</head>
+<body>
+<pre id="test">
+<script src="head.js" type="application/javascript"></script>
+<script type="application/javascript">
+
+"use strict";
+
+window.onload = async function () {
+  try {
+    const ReactDOM = browserRequire("devtools/client/shared/vendor/react-dom");
+    const { createFactory } = browserRequire("devtools/client/shared/vendor/react");
+    const { Simulate } = ReactDOM.TestUtils;
+    const Tree =
+      createFactory(browserRequire("devtools/client/shared/components/VirtualizedTree"));
+
+    function renderTree(props) {
+      const treeProps = {
+        ...TEST_TREE_INTERFACE,
+        onFocus: x => renderTree({ focused: x }),
+        ...props
+      };
+
+      return ReactDOM.render(Tree(treeProps), window.document.body);
+    }
+
+    const checker = Symbol();
+    let isActivated;
+    const mockFn = activated => {
+      isActivated = activated;
+    };
+
+    const tree = renderTree();
+
+    TEST_TREE.expanded = new Set("ABCDEFGHIJKLMNO".split(""));
+
+    // Test Home key -----------------------------------------------------------
+
+    info("Press Home to move to the first node.");
+    renderTree({ focused: "L" });
+    Simulate.keyDown(document.querySelector(".tree"), { key: "Home" });
+    await forceRender(tree);
+
+    isRenderedTree(document.body.textContent, [
+      "A:true",
+      "-B:false",
+      "--E:false",
+      "---K:false",
+      "---L:false",
+      "--F:false",
+      "--G:false",
+      "-C:false",
+      "--H:false",
+      "--I:false",
+      "-D:false",
+      "--J:false",
+      "M:false",
+      "-N:false",
+      "--O:false",
+    ], "After the Home key, A should be focused.");
+
+    info("Press Home again when already on first node.");
+    Simulate.keyDown(document.querySelector(".tree"), { key: "Home" });
+    await forceRender(tree);
+
+    isRenderedTree(document.body.textContent, [
+      "A:true",
+      "-B:false",
+      "--E:false",
+      "---K:false",
+      "---L:false",
+      "--F:false",
+      "--G:false",
+      "-C:false",
+      "--H:false",
+      "--I:false",
+      "-D:false",
+      "--J:false",
+      "M:false",
+      "-N:false",
+      "--O:false",
+    ], "After the Home key again, A should still be focused.");
+
+    // Test End key ------------------------------------------------------------
+
+    info("Press End to move to the last node.");
+    Simulate.keyDown(document.querySelector(".tree"), { key: "End" });
+    await forceRender(tree);
+
+    isRenderedTree(document.body.textContent, [
+      "A:false",
+      "-B:false",
+      "--E:false",
+      "---K:false",
+      "---L:false",
+      "--F:false",
+      "--G:false",
+      "-C:false",
+      "--H:false",
+      "--I:false",
+      "-D:false",
+      "--J:false",
+      "M:false",
+      "-N:false",
+      "--O:true",
+    ], "After the End key, O should be focused.");
+
+    info("Press End again when already on last node.");
+    Simulate.keyDown(document.querySelector(".tree"), { key: "End" });
+    await forceRender(tree);
+
+    isRenderedTree(document.body.textContent, [
+      "A:false",
+      "-B:false",
+      "--E:false",
+      "---K:false",
+      "---L:false",
+      "--F:false",
+      "--G:false",
+      "-C:false",
+      "--H:false",
+      "--I:false",
+      "-D:false",
+      "--J:false",
+      "M:false",
+      "-N:false",
+      "--O:true",
+    ], "After the End key again, O should still be focused.");
+
+    // Test Enter key ----------------------------------------------------------
+
+    info("Press Enter to activate node, when onActivate is not passed.");
+    isActivated = checker;
+    renderTree({ focused: "L" });
+    Simulate.keyDown(document.querySelector(".tree"), { key: "Enter" });
+    await forceRender(tree);
+
+    isRenderedTree(document.body.textContent, [
+      "A:false",
+      "-B:false",
+      "--E:false",
+      "---K:false",
+      "---L:true",
+      "--F:false",
+      "--G:false",
+      "-C:false",
+      "--H:false",
+      "--I:false",
+      "-D:false",
+      "--J:false",
+      "M:false",
+      "-N:false",
+      "--O:false",
+    ], "After the Enter, L should be focused and the tree remained unchanged.");
+    ok(isActivated === checker,
+       "Since onActivate was not specified, 'isActivated' should not be set.");
+
+    info("Press Enter to activate node, when onActivate is passed.");
+    isActivated = checker;
+    renderTree({ focused: "L", onActivate: mockFn });
+    Simulate.keyDown(document.querySelector(".tree"), { key: "Enter" });
+    await forceRender(tree);
+
+    isRenderedTree(document.body.textContent, [
+      "A:false",
+      "-B:false",
+      "--E:false",
+      "---K:false",
+      "---L:true",
+      "--F:false",
+      "--G:false",
+      "-C:false",
+      "--H:false",
+      "--I:false",
+      "-D:false",
+      "--J:false",
+      "M:false",
+      "-N:false",
+      "--O:false",
+    ], "After the Enter, L should be focused and the tree remained unchanged.");
+    is(isActivated, "L", "onActivate function was called with the right node.");
+
+    // Test Space key ----------------------------------------------------------
+
+    info("Press Space to activate node, when onActivate is not passed.");
+    isActivated = checker;
+    renderTree({ focused: "K" });
+    Simulate.keyDown(document.querySelector(".tree"), { key: " " });
+    await forceRender(tree);
+
+    isRenderedTree(document.body.textContent, [
+      "A:false",
+      "-B:false",
+      "--E:false",
+      "---K:true",
+      "---L:false",
+      "--F:false",
+      "--G:false",
+      "-C:false",
+      "--H:false",
+      "--I:false",
+      "-D:false",
+      "--J:false",
+      "M:false",
+      "-N:false",
+      "--O:false",
+    ], "After the Space, K should be focused and the tree remained unchanged.");
+    ok(isActivated === checker,
+       "Since onActivate was not specified, 'isActivated' should not be set.");
+
+    info("Press Space to activate node, when onActivate is passed.");
+    isActivated = checker;
+    renderTree({ focused: "K", onActivate: mockFn });
+    Simulate.keyDown(document.querySelector(".tree"), { key: " " });
+    await forceRender(tree);
+
+    isRenderedTree(document.body.textContent, [
+      "A:false",
+      "-B:false",
+      "--E:false",
+      "---K:true",
+      "---L:false",
+      "--F:false",
+      "--G:false",
+      "-C:false",
+      "--H:false",
+      "--I:false",
+      "-D:false",
+      "--J:false",
+      "M:false",
+      "-N:false",
+      "--O:false",
+    ], "After the Space, K should be focused and the tree remained unchanged.");
+    is(isActivated, "K", "onActivate function was called with the right node.");
+  } catch (e) {
+    ok(false, "Got an error: " + DevToolsUtils.safeErrorString(e));
+  } finally {
+    SimpleTest.finish();
+  }
+};
+</script>
+</pre>
+</body>
+</html>
