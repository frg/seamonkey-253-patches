# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1531515248 0
# Node ID a35b188d0e449dfd006d67c9422465600f9fb31e
# Parent  5bbbd85e49cbc9532c47963fc94a1d9730771dc8
Bug 1475649 - [mozversioncontrol] Make vcs.working_directory_clean() ignore untracked files by default r=jgraham

The doc string for the "working_directory_clean" function states:
> By default, untracked and ignored files are not considered.

But the git implementation for this function used to run:
git status --porcelain

Which *does* consider untracked files by default. Instead, we need to run:
git status --porcelain --untracked-files=no

Differential Revision: https://phabricator.services.mozilla.com/D2134

diff --git a/python/mozversioncontrol/mozversioncontrol/__init__.py b/python/mozversioncontrol/mozversioncontrol/__init__.py
--- a/python/mozversioncontrol/mozversioncontrol/__init__.py
+++ b/python/mozversioncontrol/mozversioncontrol/__init__.py
@@ -427,18 +427,18 @@ class GitRepository(Repository):
     def forget_add_remove_files(self, path):
         self._run('reset', path)
 
     def get_files_in_working_directory(self):
         return self._run('ls-files', '-z').split(b'\0')
 
     def working_directory_clean(self, untracked=False, ignored=False):
         args = ['status', '--porcelain']
-        if untracked:
-            args.append('--untracked-files')
+        if not untracked:
+            args.append('--untracked-files=no')
         if ignored:
             args.append('--ignored')
 
         return not len(self._run(*args).strip())
 
     def push_to_try(self, message):
         if not self.has_git_cinnabar:
             raise MissingVCSExtension('cinnabar')
diff --git a/python/mozversioncontrol/test/python.ini b/python/mozversioncontrol/test/python.ini
--- a/python/mozversioncontrol/test/python.ini
+++ b/python/mozversioncontrol/test/python.ini
@@ -1,6 +1,7 @@
 [DEFAULT]
 subsuite=mozversioncontrol
 skip-if = python == 3
 
 [test_context_manager.py]
 [test_workdir_outgoing.py]
+[test_working_directory.py]
diff --git a/python/mozversioncontrol/test/test_working_directory.py b/python/mozversioncontrol/test/test_working_directory.py
new file mode 100644
--- /dev/null
+++ b/python/mozversioncontrol/test/test_working_directory.py
@@ -0,0 +1,48 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+from __future__ import absolute_import
+
+import mozunit
+
+from mozversioncontrol import get_repository_object
+
+STEPS = {
+    'hg': [
+        """
+        echo "bar" >> bar
+        echo "baz" > baz
+        hg rm foo
+        """,
+        """
+        hg commit -m "Remove foo; modify bar; touch baz (but don't add it)"
+        """,
+    ],
+    'git': [
+        """
+        echo "bar" >> bar
+        echo "baz" > baz
+        git rm foo
+        """,
+        """
+        git commit -am "Remove foo; modify bar; touch baz (but don't add it)"
+        """
+    ]
+}
+
+
+def test_working_directory_clean_untracked_files(repo):
+    vcs = get_repository_object(repo.strpath)
+    assert vcs.working_directory_clean()
+
+    next(repo.step)
+    assert not vcs.working_directory_clean()
+
+    next(repo.step)
+    assert vcs.working_directory_clean()
+    assert not vcs.working_directory_clean(untracked=True)
+
+
+if __name__ == '__main__':
+    mozunit.main()
