# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1576707586 0
# Node ID e46b8e00efcec98a5a802e934a5415b7ac909e2a
# Parent  90198ce7847f3bb1556219e67883d4df8b278ceb
Bug 1604360 - [manifestparser] Convert 'test_read_ini' to the pytest format r=egao

Make it nicer to read and edit.

Depends on D57408

Differential Revision: https://phabricator.services.mozilla.com/D57409

diff --git a/testing/mozbase/manifestparser/tests/test_read_ini.py b/testing/mozbase/manifestparser/tests/test_read_ini.py
--- a/testing/mozbase/manifestparser/tests/test_read_ini.py
+++ b/testing/mozbase/manifestparser/tests/test_read_ini.py
@@ -7,58 +7,77 @@ ensure our .ini parser is doing what we 
 python's standard ConfigParser when 2.7 is reality so OrderedDict
 is the default:
 
 http://docs.python.org/2/library/configparser.html
 """
 
 from __future__ import absolute_import
 
-import unittest
-from manifestparser import read_ini
-from six import StringIO
+from textwrap import dedent
 
 import mozunit
+import pytest
+from six import StringIO
+
+from manifestparser import read_ini
 
 
-class IniParserTest(unittest.TestCase):
+@pytest.fixture(scope='module')
+def parse_manifest():
 
-    def parse_manifest(self, string):
+    def inner(string, **kwargs):
         buf = StringIO()
-        buf.write(string)
+        buf.write(dedent(string))
         buf.seek(0)
         return read_ini(buf)
 
-    def test_inline_comments(self):
-        result = self.parse_manifest("""
-[test_felinicity.py]
-kittens = true # This test requires kittens
-cats = false#but not cats
-""")[0][1]
+    return inner
+
+
+def test_inline_comments(parse_manifest):
+    result = parse_manifest("""
+    [test_felinicity.py]
+    kittens = true # This test requires kittens
+    cats = false#but not cats
+    """)[0][1]
 
-        # make sure inline comments get stripped out, but comments without a space in front don't
-        self.assertEqual(result['kittens'], 'true')
-        self.assertEqual(result['cats'], "false#but not cats")
+    # make sure inline comments get stripped out, but comments without a space in front don't
+    assert result['kittens'] == 'true'
+    assert result['cats'] == "false#but not cats"
+
+
+def test_line_continuation(parse_manifest):
+    result = parse_manifest("""
+    [test_caninicity.py]
+    breeds =
+      sheppard
+      retriever
+      terrier
 
-    def test_line_continuation(self):
-        result = self.parse_manifest("""
-[test_caninicity.py]
-breeds =
-  sheppard
-  retriever
-  terrier
+    [test_cats_and_dogs.py]
+      cats=yep
+      dogs=
+        yep
+          yep
+    birds=nope
+      fish=nope
+    """)
+    assert result[0][1]['breeds'].split() == ['sheppard', 'retriever', 'terrier']
+    assert result[1][1]['cats'] == 'yep'
+    assert result[1][1]['dogs'].split() == ['yep', 'yep']
+    assert result[1][1]['birds'].split() == ['nope', 'fish=nope']
 
-[test_cats_and_dogs.py]
-  cats=yep
-  dogs=
-    yep
-      yep
-birds=nope
-  fish=nope
-""")
-        self.assertEqual(result[0][1]['breeds'].split(), ['sheppard', 'retriever', 'terrier'])
-        self.assertEqual(result[1][1]['cats'], 'yep')
-        self.assertEqual(result[1][1]['dogs'].split(), ['yep', 'yep'])
-        self.assertEqual(result[1][1]['birds'].split(), ['nope', 'fish=nope'])
+def test_dupes_error(parse_manifest):
+    dupes = """
+    [test_dupes.py]
+    foo = bar
+    foo = baz
+    """
+    with pytest.raises(AssertionError):
+        parse_manifest(dupes, strict=True)
+
+    with pytest.raises(AssertionError):
+        parse_manifest(dupes, strict=False)
 
 
 if __name__ == '__main__':
     mozunit.main()
