# HG changeset patch
# User Axel Hecht <axel@pike.org>
# Date 1565265270 0
# Node ID b9febe64276d07324e8554f6dc22cd4d6fdff350
# Parent  26b016b0c4cb6f7cf2e4ae07e7744682e5b40b70
Bug 1321281, add test framework for Fluent migration recipes, r=flod,ahal

Basic test infrastructure for Fluent migration recipes, automate creating
a reference from recipe data, and running the recipe dry and wet.

There are no hard failures for diffs, as it might just be OK.

Differential Revision: https://phabricator.services.mozilla.com/D40200

diff --git a/intl/l10n/docs/fluent_migrations.rst b/intl/l10n/docs/fluent_migrations.rst
--- a/intl/l10n/docs/fluent_migrations.rst
+++ b/intl/l10n/docs/fluent_migrations.rst
@@ -107,17 +107,17 @@ This is how the migration recipe looks:
 
       ctx.add_transforms(
           "toolkit/toolkit/main-window/findbar.ftl",
           "toolkit/toolkit/main-window/findbar.ftl",
           transforms_from(
   """
   findbar-next =
       .tooltiptext = { COPY(from_path, "next.tooltip") }
-  """, from_path="toolkit/chrome/global/findbar.dtd")
+  """, from_path="toolkit/chrome/global/findbar.dtd"))
 
 
 The first important thing to notice is that the migration recipe needs file
 paths relative to a localization repository, losing :bash:`locales/en-US/`:
 
  - :bash:`toolkit/locales/en-US/chrome/global/findbar.dtd` becomes
    :bash:`toolkit/chrome/global/findbar.dtd`.
  - :bash:`toolkit/locales/en-US/toolkit/main-window/findbar.ftl` becomes
@@ -149,17 +149,17 @@ repeating the same path multiple times, 
 
   ctx.add_transforms(
       "toolkit/toolkit/main-window/findbar.ftl",
       "toolkit/toolkit/main-window/findbar.ftl",
       transforms_from(
   """
   findbar-next =
   .tooltiptext = { COPY("toolkit/chrome/global/findbar.dtd", "next.tooltip") }
-  """)
+  """))
 
 
 This method of writing migration recipes allows to take the original FTL
 strings, and simply replace the value of each message with a :python:`COPY`
 Transform. :python:`transforms_from` takes care of converting the FTL syntax
 into an array of Transforms describing how the legacy translations should be
 migrated. This manner of defining migrations is only suitable to simple strings
 where a copy operation is sufficient. For more complex use-cases which require
@@ -521,130 +521,42 @@ new elements are:
    :python:`REPLACE` evaluate to Fluent Patterns.
  - A :python:`SelectExpression` is defined, with an array of :python:`Variant`
    objects.
 
 
 How to Test Migration Recipes
 =============================
 
-Unfortunately, testing migration recipes requires several manual steps. We plan
-to `introduce automated testing`__ for patches including migration recipes, in
-the meantime this is how it’s possible to test migration recipes.
-
-__ https://bugzilla.mozilla.org/show_bug.cgi?id=1353680
-
-
-1. Install Fluent Migration
----------------------------
-
-The first step is to install the `Fluent Migration`_ Python library. It’s
-currently not available as a package, so the repository must be cloned locally
-and installed manually, e.g. with :bash:`pip install -e .`.
-
-Installing this package will make a :bash:`migrate-l10n` command available.
-
-
-2. Clone gecko-strings
-----------------------
-
-Migration recipes work against localization repositories, which means it’s not
-possible to test them directly against `mozilla-central`, unless the *source*
-path (the second argument) in :python:`ctx.add_transforms` is temporarily
-tweaked to match `mozilla-central` paths.
-
-To test the actual recipe that will land in the patch, it’s necessary to clone
-the `gecko-strings`_ repository on the system twice, in two separate folders.
-One will simulate the reference en-US repository after the patch has landed, and
-the other will simulate a target localization. For example, let’s call the two
-folders `en-US` and `test`.
-
-
-.. code-block:: bash
-
-  hg clone https://hg.mozilla.org/l10n/gecko-strings en-US
-  cp -r en-US test
-
-
-3. Add new FTL strings to the local en-US repository
-------------------------------------------------
-
-The changed (or brand new) FTL files from the patch need to be copied into the
-`en-US` repository. Remember that paths are slightly different, with
-localization repositories missing the :bash:`locales/en-US` portion. There’s no
-need to commit these changes locally.
-
-
-4. Run the migration recipe
----------------------------
-
-The system is all set to run the recipe with the following commands:
-
+To test migration recipes, use the following mach command:
 
 .. code-block:: bash
 
-  cd PATH/TO/recipes
+  ./mach fluent-migration-test python/l10n/fluent_migrations/bug_1485002_newtab.py
 
-  migrate-l10n \
-    --lang test
-    --reference-dir PATH/TO/en-US \
-    --localization-dir PATH/TO/test \
-    --dry-run \
-    name_of_the_recipe
+This will analyze your migration recipe to check that the :python:`migrate`
+function exists, and interacts correctly with the migration context. Once that
+passes, it clones :bash:`gecko-strings` into :bash:`$OBJDIR/python/l10n`, creates a
+reference localization by adding your local Fluent strings to the ones in
+:bash:`gecko-strings`. It then runs the migration recipe, both as dry run and
+as actual migration. Finally it analyzes the commits, and checks if any
+migrations were actually run and the bug number in the commit message matches
+the migration name.
 
-
-The name of the recipe needs to be specified without the :bash:`.py` extension,
-since it’s imported as a module.
+It will also show the diff between the migrated files and the reference, ignoring
+blank lines.
 
-Alternatively, before running :bash:`migrate-l10n`, it’s possible to update the
-value of :bash:`PYTHONPATH` to include the folder storing migration recipes.
-
+You can inspect the generated repository further by looking at
 
 .. code-block:: bash
 
-  export PYTHONPATH="${PYTHONPATH}:PATH/TO/recipes/"
-
-
-The :bash:`--dry-run` option allows to run the recipe without making changes,
-and it’s useful to spot syntax errors in the recipe. If there are no errors,
-it’s possible to run the migration without :bash:`--dry-run` and actually commit
-the changes locally.
-
-This is the output of a migration:
-
-
-.. code-block:: bash
+  ls $OBJDIR/python/l10n/bug_1485002_newtab/en-US
 
-  Running migration bug_1411707_findbar for test
-  WARNING:migrate:Plural rule for "'test'" is not defined in compare-locales
-  INFO:migrate:Localization file toolkit/toolkit/main-window/findbar.ftl does not exist and it will be created
-    Writing to test/toolkit/toolkit/main-window/findbar.ftl
-    Committing changeset: Bug 1411707 - Migrate the findbar XBL binding to a Custom Element, part 1.
-    Writing to test/toolkit/toolkit/main-window/findbar.ftl
-    Committing changeset: Bug 1411707 - Migrate the findbar XBL binding to a Custom Element, part 2.
-
-
-.. hint::
-
-  The warning about plural rules is expected, since `test` is not a valid locale
-  code. At this point, the result of migration is committed to the local `test`
-  folder.
-
-
-5. Compare the resulting files
-------------------------------
-
-Once the migration has run, the `test` repository includes the migrated files,
-and it’s possible to compare them with the files in `en-US`. Since the migration
-code strips empty line between strings, it’s recommended to use :bash:`diff -B`
-between the two files, or use a visual diff to compare their content.
-
-
-6. Caveats
-----------
+Caveats
+-------
 
 Be aware of hard-coded English context in migration. Consider for example:
 
 
 .. code-block:: python
 
   ctx.add_transforms(
           "browser/browser/preferences/siteDataSettings.ftl",
diff --git a/python/l10n/test_fluent_migrations/__init__.py b/python/l10n/test_fluent_migrations/__init__.py
new file mode 100644
diff --git a/python/l10n/test_fluent_migrations/fmt.py b/python/l10n/test_fluent_migrations/fmt.py
new file mode 100644
--- /dev/null
+++ b/python/l10n/test_fluent_migrations/fmt.py
@@ -0,0 +1,147 @@
+from __future__ import absolute_import, print_function
+import logging
+import os
+import re
+import shutil
+
+import hglib
+from mozboot.util import get_state_dir
+import mozpack.path as mozpath
+
+from compare_locales.merge import merge_channels
+from compare_locales.paths.files import ProjectFiles
+from compare_locales.paths.configparser import TOMLParser
+from fluent.migrate import validator
+
+
+def inspect_migration(path):
+    '''Validate recipe and extract some metadata.
+    '''
+    return validator.Validator.validate(path)
+
+
+def prepare_object_dir(cmd):
+    '''Prepare object dir to have an up-to-date clone of gecko-strings.
+
+    We run this once per mach invocation, for all tested migrations.
+    '''
+    obj_dir = mozpath.join(cmd.topobjdir, 'python', 'l10n')
+    if not os.path.exists(obj_dir):
+        os.makedirs(obj_dir)
+    state_dir = get_state_dir()
+    if os.path.exists(mozpath.join(state_dir, 'gecko-strings')):
+        cmd.run_process(
+            ['hg', 'pull', '-u'],
+            cwd=mozpath.join(state_dir, 'gecko-strings')
+        )
+    else:
+        cmd.run_process(
+            ['hg', 'clone', 'https://hg.mozilla.org/l10n/gecko-strings'],
+            cwd=state_dir,
+        )
+    return obj_dir
+
+
+def test_migration(cmd, obj_dir, to_test, references):
+    '''Test the given recipe.
+
+    This creates a workdir by l10n-merging gecko-strings and the m-c source,
+    to mimmic gecko-strings after the patch to test landed.
+    It then runs the recipe with a gecko-strings clone as localization, both
+    dry and wet.
+    It inspects the generated commits, and shows a diff between the merged
+    reference and the generated content.
+    The diff is intended to be visually inspected. Some changes might be
+    expected, in particular when formatting of the en-US strings is different.
+    '''
+    rv = 0
+    migration_name = os.path.splitext(os.path.split(to_test)[1])[0]
+    work_dir = mozpath.join(obj_dir, migration_name)
+    if os.path.exists(work_dir):
+        shutil.rmtree(work_dir)
+    os.makedirs(mozpath.join(work_dir, 'reference'))
+    l10n_toml = mozpath.join(cmd.topsrcdir, 'browser', 'locales', 'l10n.toml')
+    pc = TOMLParser().parse(l10n_toml, env={
+        'l10n_base': work_dir
+    })
+    pc.set_locales(['reference'])
+    files = ProjectFiles('reference', [pc])
+    for ref in references:
+        if ref != mozpath.normpath(ref):
+            cmd.log(logging.ERROR, 'fluent-migration-test', {
+                'file': to_test,
+                'ref': ref,
+            }, 'Reference path "{ref}" needs to be normalized for {file}')
+            rv = 1
+            continue
+        full_ref = mozpath.join(work_dir, 'reference', ref)
+        m = files.match(full_ref)
+        if m is None:
+            raise ValueError("Bad reference path: " + ref)
+        m_c_path = m[1]
+        g_s_path = mozpath.join(work_dir, 'gecko-strings', ref)
+        resources = [
+            b'' if not os.path.exists(f)
+            else open(f, 'rb').read()
+            for f in (g_s_path, m_c_path)
+        ]
+        ref_dir = os.path.dirname(full_ref)
+        if not os.path.exists(ref_dir):
+            os.makedirs(ref_dir)
+        open(full_ref, 'wb').write(merge_channels(ref, resources))
+    client = hglib.clone(
+        source=mozpath.join(get_state_dir(), 'gecko-strings'),
+        dest=mozpath.join(work_dir, 'en-US')
+    )
+    client.open()
+    old_tip = client.tip().node
+    run_migration = [
+        cmd._virtualenv_manager.python_path,
+        '-m', 'fluent.migrate.tool',
+        '--lang', 'en-US',
+        '--reference-dir', mozpath.join(work_dir, 'reference'),
+        '--localization-dir', mozpath.join(work_dir, 'en-US'),
+        '--dry-run',
+        'fluent_migrations.' + migration_name
+    ]
+    cmd.run_process(
+        run_migration,
+        cwd=work_dir,
+        line_handler=print,
+    )
+    # drop --dry-run
+    run_migration.pop(-2)
+    cmd.run_process(
+        run_migration,
+        cwd=work_dir,
+        line_handler=print,
+    )
+    tip = client.tip().node
+    if old_tip == tip:
+        cmd.log(logging.WARN, 'fluent-migration-test', {
+            'file': to_test,
+        }, 'No migration applied for {file}')
+        return rv
+    for ref in references:
+        cmd.run_process([
+            'diff', '-u', '-B',
+            mozpath.join(work_dir, 'reference', ref),
+            mozpath.join(work_dir, 'en-US', ref),
+        ], ensure_exit_code=False, line_handler=print)
+    messages = [l.desc for l in client.log('::{} - ::{}'.format(tip, old_tip))]
+    bug = re.search('[0-9]{5,}', migration_name).group()
+    # Just check first message for bug number, they're all following the same pattern
+    if bug not in messages[0]:
+        rv = 1
+        cmd.log(logging.ERROR, 'fluent-migration-test', {
+            'file': to_test,
+        }, 'Missing or wrong bug number for {file}')
+    if any(
+        'part {}'.format(n + 1) not in msg
+        for n, msg in enumerate(messages)
+    ):
+        rv = 1
+        cmd.log(logging.ERROR, 'fluent-migration-test', {
+            'file': to_test,
+        }, 'Commit messages should have "part {{index}}" for {file}')
+    return rv
diff --git a/testing/mach_commands.py b/testing/mach_commands.py
--- a/testing/mach_commands.py
+++ b/testing/mach_commands.py
@@ -891,8 +891,47 @@ class TestInfoCommand(MachCommandBase):
         response.raise_for_status()
         json_response = response.json()
         print("\nBugzilla quick search for '%s':" % search)
         if 'bugs' in json_response:
             for bug in json_response['bugs']:
                 print("Bug %s: %s" % (bug['id'], bug['summary']))
         else:
             print("No bugs found.")
+
+
+@CommandProvider
+class TestFluentMigration(MachCommandBase):
+    @Command('fluent-migration-test', category='testing',
+             description="Test Fluent migration recipes.")
+    @CommandArgument('test_paths', nargs='*', metavar='N',
+                     help="Recipe paths to test.")
+    def run_migration_tests(self, test_paths=None, **kwargs):
+        if not test_paths:
+            test_paths = []
+        self._activate_virtualenv()
+        from test_fluent_migrations import fmt
+        rv = 0
+        with_context = []
+        for to_test in test_paths:
+            try:
+                context = fmt.inspect_migration(to_test)
+                for issue in context['issues']:
+                    self.log(logging.ERROR, 'fluent-migration-test', {
+                        'error': issue['msg'],
+                        'file': to_test,
+                    }, 'ERROR in {file}: {error}')
+                if context['issues']:
+                    continue
+                with_context.append({
+                    'to_test': to_test,
+                    'references': context['references'],
+                })
+            except Exception as e:
+                self.log(logging.ERROR, 'fluent-migration-test', {
+                    'error': str(e),
+                    'file': to_test
+                }, 'ERROR in {file}: {error}')
+                rv |= 1
+        obj_dir = fmt.prepare_object_dir(self)
+        for context in with_context:
+            rv |= fmt.test_migration(self, obj_dir, **context)
+        return rv
