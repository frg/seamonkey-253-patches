# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1520676058 0
# Node ID 78e2a853d8f8837db876b58a9cd2b6b87bda1de0
# Parent  dd8202671cf14a9cb5e73b918edad64f386e9fb5
Bug 1444593: Make links in CONTRIBUTING.md safe for export. r=me

testing/geckodriver/CONTRIBUTING.md is exported to GitHub which
means it cannot use relative links to files that live in-tree.

MozReview-Commit-ID: 2YPaAowgE7


diff --git a/testing/geckodriver/CONTRIBUTING.md b/testing/geckodriver/CONTRIBUTING.md
--- a/testing/geckodriver/CONTRIBUTING.md
+++ b/testing/geckodriver/CONTRIBUTING.md
@@ -17,19 +17,19 @@ parts it can be useful to know about:
   * [_webdriver_] is a Rust crate providing interfaces, traits
     and types, errors, type- and bounds checks, and JSON marshaling
     for correctly parsing and emitting the [WebDriver protocol].
 
 By participating in this project, you agree to abide by the Mozilla
 [Community Participation Guidelines].  Here are some guidelines
 for contributing high-quality and actionable bugs and code.
 
-[_geckodriver_]: ./README.md
-[_Marionette_]: ../marionette/README.md
-[_webdriver_]: ../webdriver/README.md
+[_geckodriver_]: https://firefox-source-docs.mozilla.org/testing/geckodriver/geckodriver/
+[_Marionette_]: https://firefox-source-docs.mozilla.org/testing/marionette/marionette/
+[_webdriver_]: https://crates.io/crates/webdriver
 [WebDriver protocol]: https://w3c.github.io/webdriver/webdriver-spec.html#protocol
 [XPCOM]: https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/Guide
 [Community Participation Guidelines]: https://www.mozilla.org/en-US/about/governance/policies/participation/
 
 
 Reporting bugs
 ==============
 
@@ -55,32 +55,30 @@ relating to a specific client should be 
 We welcome you to file issues in the [GitHub issue tracker] once you are
 confident it has not already been reported.  The [ISSUE_TEMPLATE.md]
 contains a helpful checklist for things we will want to know about
 the affected system, reproduction steps, and logs.
 
 geckodriver development follows a rolling release model as we don’t
 release patches for older versions.  It is therefore useful to use
 the tip-of-tree geckodriver binary, or failing this, the latest
-release when verifying the problem.  Similarly, as noted in the
-[README], geckodriver is only compatible with the current release
-channel versions of Firefox, and it consequently does not help
-to report bugs that affect outdated and unsupported Firefoxen.
-Please always try to verify the issue in the latest Firefox Nightly
-before you file your bug.
+release when verifying the problem.  geckodriver is only compatible
+with the current release channel versions of Firefox, and it
+consequently does not help to report bugs that affect outdated and
+unsupported Firefoxen.  Please always try to verify the issue in
+the latest Firefox Nightly before you file your bug.
 
 Once we are satisfied the issue raised is of sufficiently actionable
 character, we will continue with triaging it and file a bug where it
 is appropriate.  Bugs specific to geckodriver will be filed in the
 [`Testing :: geckodriver`] component in Bugzilla.
 
 [mailing list]: #communication
-[trace-level log]: doc/TraceLogs.md
+[trace-level log]: https://firefox-source-docs.mozilla.org/testing/geckodriver/geckodriver/TraceLogs.html
 [GitHub issue tracker]: https://github.com/mozilla/geckodriver/issues
-[README]: ./README.md
 [`Testing :: geckodriver`]: https://bugzilla.mozilla.org/buglist.cgi?component=geckodriver
 
 
 Writing code
 ============
 
 Because there are many moving parts involved remote controlling
 a web browser, it can be challenging to a new contributor to know
@@ -128,22 +126,22 @@ When you have, you are ready to start of
 
 To run the executable from the objdir:
 
 	% ./mach geckodriver -- --version
 	 0:00.27 /home/ato/src/gecko/obj-x86_64-pc-linux-gnu/dist/bin/geckodriver --version --binary /home/ato/src/gecko/obj-x86_64-pc-linux-gnu/dist/bin/firefox
 	geckodriver 0.19.0 (f3e939a81ee1169f9501ad96eb43bbf4bf4a1bde 2017-10-11)
 
 [Rust]: https://www.rust-lang.org/
-[webdriver crate]: ../webdriver/README.md
+[webdriver crate]: https://crates.io/crates/webdriver
 [commands]: https://docs.rs/webdriver/newest/webdriver/command/index.html
 [responses]: https://docs.rs/webdriver/newest/webdriver/response/index.html
 [errors]: https://docs.rs/webdriver/newest/webdriver/error/enum.ErrorStatus.html
 [Marionette protocol]: https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette/Protocol
-[Marionette]: ../marionette/README.md
+[Marionette]: https://firefox-source-docs.mozilla.org/testing/marionette/marionette/index.html
 [Firefox CI]: https://treeherder.mozilla.org/
 [mozconfig]: https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Build_Instructions/Configuring_Build_Options
 
 
 Running the tests
 -----------------
 
 We verify and test geckodriver in a couple of different ways.

