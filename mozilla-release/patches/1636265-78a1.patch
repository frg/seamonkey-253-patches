# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1588955195 0
# Node ID 14d0cb2955bf11ec54b3774da433a04587746a10
# Parent  033b91c32ab0bb61558f0907f87bec964e96880e
Bug 1636265. Fix the Eclipse CDT build backend (broken by the switch to Python 3). r=botond

Python 3 doesn't allow strings to be written to files opened in binary mode
(it requires a byte array in that case).  As it happens, we should really be
opening these Eclipse config files in text mode since it seems on Windows the
files use Windows line ending characters.  So rather than change the strings
to byte arrays, this patch simply changes the code to open the files in text
mode.

Differential Revision: https://phabricator.services.mozilla.com/D74318

diff --git a/python/mozbuild/mozbuild/backend/cpp_eclipse.py b/python/mozbuild/mozbuild/backend/cpp_eclipse.py
--- a/python/mozbuild/mozbuild/backend/cpp_eclipse.py
+++ b/python/mozbuild/mozbuild/backend/cpp_eclipse.py
@@ -109,77 +109,77 @@ class CppEclipseBackend(CommonBackend):
         for dir_name in [self._project_dir, settings_dir, launch_dir, workspace_settings_dir, self._workspace_lang_dir]:
             try:
                 os.makedirs(dir_name)
             except OSError as e:
                 if e.errno != errno.EEXIST:
                     raise
 
         project_path = os.path.join(self._project_dir, '.project')
-        with open(project_path, 'wb') as fh:
+        with open(project_path, 'w') as fh:
             self._write_project(fh)
 
         cproject_path = os.path.join(self._project_dir, '.cproject')
-        with open(cproject_path, 'wb') as fh:
+        with open(cproject_path, 'w') as fh:
             self._write_cproject(fh)
 
         language_path = os.path.join(settings_dir, 'language.settings.xml')
-        with open(language_path, 'wb') as fh:
+        with open(language_path, 'w') as fh:
             self._write_language_settings(fh)
 
         workspace_language_path = os.path.join(self._workspace_lang_dir, 'language.settings.xml')
-        with open(workspace_language_path, 'wb') as fh:
+        with open(workspace_language_path, 'w') as fh:
             workspace_lang_settings = WORKSPACE_LANGUAGE_SETTINGS_TEMPLATE
             workspace_lang_settings = workspace_lang_settings.replace(
                 "@COMPILER_FLAGS@", self._cxx + " " + self._cppflags)
             fh.write(workspace_lang_settings)
 
         self._write_launch_files(launch_dir)
 
         core_resources_prefs_path = os.path.join(
             workspace_settings_dir, 'org.eclipse.core.resources.prefs')
-        with open(core_resources_prefs_path, 'wb') as fh:
+        with open(core_resources_prefs_path, 'w') as fh:
             fh.write(STATIC_CORE_RESOURCES_PREFS)
 
         core_runtime_prefs_path = os.path.join(
             workspace_settings_dir, 'org.eclipse.core.runtime.prefs')
-        with open(core_runtime_prefs_path, 'wb') as fh:
+        with open(core_runtime_prefs_path, 'w') as fh:
             fh.write(STATIC_CORE_RUNTIME_PREFS)
 
         ui_prefs_path = os.path.join(workspace_settings_dir, 'org.eclipse.ui.prefs')
-        with open(ui_prefs_path, 'wb') as fh:
+        with open(ui_prefs_path, 'w') as fh:
             fh.write(STATIC_UI_PREFS)
 
         cdt_ui_prefs_path = os.path.join(workspace_settings_dir, 'org.eclipse.cdt.ui.prefs')
         cdt_ui_prefs = STATIC_CDT_UI_PREFS
         # Here we generate the code formatter that will show up in the UI with
         # the name "Mozilla".  The formatter is stored as a single line of XML
         # in the org.eclipse.cdt.ui.formatterprofiles pref.
         cdt_ui_prefs += """org.eclipse.cdt.ui.formatterprofiles=<?xml version\="1.0" encoding\="UTF-8" standalone\="no"?>\\n<profiles version\="1">\\n<profile kind\="CodeFormatterProfile" name\="Mozilla" version\="1">\\n"""
         XML_PREF_TEMPLATE = """<setting id\="@PREF_NAME@" value\="@PREF_VAL@"/>\\n"""
         for line in FORMATTER_SETTINGS.splitlines():
             [pref, val] = line.split("=")
             cdt_ui_prefs += XML_PREF_TEMPLATE.replace("@PREF_NAME@",
                                                       pref).replace("@PREF_VAL@", val)
         cdt_ui_prefs += "</profile>\\n</profiles>\\n"
-        with open(cdt_ui_prefs_path, 'wb') as fh:
+        with open(cdt_ui_prefs_path, 'w') as fh:
             fh.write(cdt_ui_prefs)
 
         cdt_core_prefs_path = os.path.join(workspace_settings_dir, 'org.eclipse.cdt.core.prefs')
-        with open(cdt_core_prefs_path, 'wb') as fh:
+        with open(cdt_core_prefs_path, 'w') as fh:
             cdt_core_prefs = STATIC_CDT_CORE_PREFS
             # When we generated the code formatter called "Mozilla" above, we
             # also set it to be the active formatter.  When a formatter is set
             # as the active formatter all its prefs are set in this prefs file,
             # so we need add those now:
             cdt_core_prefs += FORMATTER_SETTINGS
             fh.write(cdt_core_prefs)
 
         editor_prefs_path = os.path.join(workspace_settings_dir, "org.eclipse.ui.editors.prefs")
-        with open(editor_prefs_path, 'wb') as fh:
+        with open(editor_prefs_path, 'w') as fh:
             fh.write(EDITOR_SETTINGS)
 
         # Now import the project into the workspace
         self._import_project()
 
     def _import_project(self):
         # If the workspace already exists then don't import the project again because
         # eclipse doesn't handle this properly
@@ -205,17 +205,17 @@ class CppEclipseBackend(CommonBackend):
                                 "Ensure 'eclipse' is in your PATH and try again")
             else:
                 raise
         finally:
             self._remove_noindex()
 
     def _write_noindex(self):
         noindex_path = os.path.join(self._project_dir, '.settings/org.eclipse.cdt.core.prefs')
-        with open(noindex_path, 'wb') as fh:
+        with open(noindex_path, 'w') as fh:
             fh.write(NOINDEX_TEMPLATE)
 
     def _remove_noindex(self):
         # Below we remove the config file that temporarily disabled the indexer
         # while we were importing the project. Unfortunately, CDT doesn't
         # notice indexer settings changes in config files when it restarts. To
         # work around that we remove the index database here to force it to:
         for f in glob.glob(os.path.join(self._workspace_lang_dir, "Gecko.*.pdom")):
@@ -328,17 +328,17 @@ class CppEclipseBackend(CommonBackend):
         if self._macbundle:
             exe_path = os.path.join(bin_dir, self._macbundle, 'Contents/MacOS')
         else:
             exe_path = os.path.join(bin_dir, 'bin')
 
         exe_path = os.path.join(exe_path, self._appname + self._bin_suffix)
 
         main_gecko_launch = os.path.join(launch_dir, 'gecko.launch')
-        with open(main_gecko_launch, 'wb') as fh:
+        with open(main_gecko_launch, 'w') as fh:
             launch = GECKO_LAUNCH_CONFIG_TEMPLATE
             launch = launch.replace('@LAUNCH_PROGRAM@', exe_path)
             launch = launch.replace('@LAUNCH_ARGS@', '-P -no-remote')
             fh.write(launch)
 
         # TODO Add more launch configs (and delegate calls to mach)
 
     def _write_project(self, fh):

