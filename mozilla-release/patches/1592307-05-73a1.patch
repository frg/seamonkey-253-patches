# HG changeset patch
# User Iain Ireland <iireland@mozilla.com>
# Date 1576095528 0
#      Wed Dec 11 20:18:48 2019 +0000
# Node ID 2e11fc71e4cfefd07cb7619dca18407bb299ec80
# Parent  7d5e2cda6fbb7a888df9e4ee4deb5d10741a86be
Bug 1592307: Part 5: Add V8::Object shim r=mgaudet

This patch adds a shim for V8's Object class, which maps fairly well to SM's Value.

Remaining rough spots:

1. V8 provides an interface for converting between an Object and an Address (typedef for uintptr_t). SM does its best to avoid exposing the internal Value representation to the outside world. We can either make this work by adding v8::internal::Object as a friend class of JSValue, or upstream an irregexp patch to remove the relatively few places where this is necessary. (A later patch goes with the former for now.)
2. V8's HeapObject (halfway between SM's Object and GCThing) has a Size method, but it should be easy to upstream a patch to eliminate the need for it in irregexp. (This patch is written and ready to go.)
3. V8's ByteArray is a fixed-length array of bytes. In the previous port of irregexp, we just used uint8_t[] (or a unique pointer to uint8_t[]). However, since our goal here is to avoid modifying the implementation of irregexp, we would prefer something that is compatible with the existing code, which means it needs to be a HeapObject. There are a variety of options (add a new class, use Uint8Array, rewrite HeapObject to enable us to store non-GCThings), but none of them is so obviously correct that I was willing to commit to it here. (In a later patch, I create a new class.)

Depends on D56487

Differential Revision: https://phabricator.services.mozilla.com/D56488

diff --git a/js/src/new-regexp/regexp-shim.h b/js/src/new-regexp/regexp-shim.h
--- a/js/src/new-regexp/regexp-shim.h
+++ b/js/src/new-regexp/regexp-shim.h
@@ -15,16 +15,17 @@
 #include "mozilla/Attributes.h"
 #include "mozilla/MathAlgorithms.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/Types.h"
 
 #include <algorithm>
 
 #include "js/Value.h"
+#include "vm/NativeObject.h"
 
 // Forward declaration of classes
 namespace v8 {
 namespace internal {
 
 class Isolate;
 
 }  // namespace internal
@@ -294,16 +295,90 @@ inline int CompareChars(const lchar* lhs
 inline int HexValue(uc32 c) {
   c -= '0';
   if (static_cast<unsigned>(c) <= 9) return c;
   c = (c | 0x20) - ('a' - '0');  // detect 0x11..0x16 and 0x31..0x36.
   if (static_cast<unsigned>(c) <= 5) return c + 10;
   return -1;
 }
 
+// V8::Object ~= JS::Value
+class Object {
+ public:
+  // The default object constructor in V8 stores a nullptr,
+  // which has its low bit clear and is interpreted as Smi(0).
+  constexpr Object() : value_(JS::Int32Value(0)) {}
+
+  // Conversions to/from SpiderMonkey types
+  constexpr Object(JS::Value value) : value_(value) {}
+  operator JS::Value() const { return value_; }
+
+  // Used in regexp-macro-assembler.cc and regexp-interpreter.cc to
+  // check the return value of isolate->stack_guard()->HandleInterrupts()
+  // In V8, this will be either an exception object or undefined.
+  // In SM, we store the exception in the context, so we can use our normal
+  // idiom: return false iff we are throwing an exception.
+  inline bool IsException(Isolate*) const { return !value_.toBoolean(); }
+
+  // SpiderMonkey tries to avoid leaking the internal representation of its
+  // objects. V8 is not so strict. These functions are used when calling /
+  // being called by native code: objects are converted to Addresses for the
+  // call, then cast back to objects on the other side.
+  // We might be able to upstream a patch that eliminates the need for these.
+  Object(Address bits);
+  Address ptr() const;
+
+ protected:
+  JS::Value value_;
+};
+
+class Smi : public Object {
+ public:
+  static Smi FromInt(int32_t value) {
+    Smi smi;
+    smi.value_ = JS::Int32Value(value);
+    return smi;
+  }
+  static inline int32_t ToInt(const Object object) {
+    return JS::Value(object).toInt32();
+  }
+};
+
+// V8::HeapObject ~= JSObject
+class HeapObject : public Object {
+public:
+  // Only used for bookkeeping of total code generated in regexp-compiler.
+  // We may be able to refactor this away.
+  int Size() const;
+};
+
+// A fixed-size array with Objects (aka Values) as element types
+// Implemented as a wrapper around a regular native object with dense elements.
+class FixedArray : public HeapObject {
+ public:
+  inline void set(uint32_t index, Object value) {
+    JS::Value(*this).toObject().as<js::NativeObject>().setDenseElement(index,
+                                                                       value);
+  }
+};
+
+// A fixed-size array of bytes.
+// TODO: figure out the best implementation for this. Uint8Array might work,
+// but it's not currently visible outside of TypedArrayObject.cpp.
+class ByteArray : public HeapObject {
+ public:
+  uint8_t get(uint32_t index);
+  void set(uint32_t index, uint8_t val);
+  uint32_t length();
+  byte* GetDataStartAddress();
+  byte* GetDataEndAddress();
+
+  static ByteArray cast(Object object);
+};
+
 // RAII Guard classes
 
 class DisallowHeapAllocation {
  public:
   DisallowHeapAllocation() {}
   operator const JS::AutoAssertNoGC&() const { return no_gc_; }
 
  private:
