# HG changeset patch
# User J. Ryan Stinnett <jryans@gmail.com>
# Date 1581083949 0
# Node ID ad03b7c30423f5c514b3f6ed423aecb8a7f867d9
# Parent  e902bcded49d41d7092881e2229ab9a1d32b5df4
Bug 1486331 - Remove child-src deprecation warning. r=ckerschb

As child-src is no longer deprecated in the CSP level 3 spec, this removes the
console warning, since it's valid to use it as a fallback for frame-src and
worker-src.

Differential Revision: https://phabricator.services.mozilla.com/D62034

diff --git a/dom/locales/en-US/chrome/security/csp.properties b/dom/locales/en-US/chrome/security/csp.properties
--- a/dom/locales/en-US/chrome/security/csp.properties
+++ b/dom/locales/en-US/chrome/security/csp.properties
@@ -107,15 +107,11 @@ couldntParseInvalidHost = Couldn’t parse invalid host %1$S
 # %1$S is the string source
 couldntParseScheme = Couldn’t parse scheme in %1$S
 # LOCALIZATION NOTE (couldntParsePort):
 # %1$S is the string source
 couldntParsePort = Couldn’t parse port in %1$S
 # LOCALIZATION NOTE (duplicateDirective):
 # %1$S is the name of the duplicate directive
 duplicateDirective = Duplicate %1$S directives detected.  All but the first instance will be ignored.
-# LOCALIZATION NOTE (deprecatedChildSrcDirective):
-# %1$S is the value of the deprecated directive.
-# Do not localize: worker-src, frame-src
-deprecatedChildSrcDirective = Directive ‘%1$S’ has been deprecated. Please use directive ‘worker-src’ to control workers, or directive ‘frame-src’ to control frames respectively.
 # LOCALIZATION NOTE (couldntParseInvalidSandboxFlag):
 # %1$S is the option that could not be understood
 couldntParseInvalidSandboxFlag = Couldn’t parse invalid sandbox flag ‘%1$S’
diff --git a/dom/security/nsCSPParser.cpp b/dom/security/nsCSPParser.cpp
--- a/dom/security/nsCSPParser.cpp
+++ b/dom/security/nsCSPParser.cpp
@@ -1106,24 +1106,20 @@ nsCSPParser::directiveName()
     return new nsBlockAllMixedContentDirective(CSP_StringToCSPDirective(mCurToken));
   }
 
   // special case handling for upgrade-insecure-requests
   if (CSP_IsDirective(mCurToken, nsIContentSecurityPolicy::UPGRADE_IF_INSECURE_DIRECTIVE)) {
     return new nsUpgradeInsecureDirective(CSP_StringToCSPDirective(mCurToken));
   }
 
-  // child-src by itself is deprecatd but will be enforced
-  //   * for workers (if worker-src is not explicitly specified)
-  //   * for frames  (if frame-src is not explicitly specified)
+  // if we have a child-src, cache it as a fallback for
+  //   * workers (if worker-src is not explicitly specified)
+  //   * frames  (if frame-src is not explicitly specified)
   if (CSP_IsDirective(mCurToken, nsIContentSecurityPolicy::CHILD_SRC_DIRECTIVE)) {
-    const char16_t* params[] = { mCurToken.get() };
-    logWarningErrorToConsole(nsIScriptError::warningFlag,
-                             "deprecatedChildSrcDirective",
-                             params, ArrayLength(params));
     mChildSrc = new nsCSPChildSrcDirective(CSP_StringToCSPDirective(mCurToken));
     return mChildSrc;
   }
 
   // if we have a frame-src, cache it so we can discard child-src for frames
   if (CSP_IsDirective(mCurToken, nsIContentSecurityPolicy::FRAME_SRC_DIRECTIVE)) {
     mFrameSrc = new nsCSPDirective(CSP_StringToCSPDirective(mCurToken));
     return mFrameSrc;
