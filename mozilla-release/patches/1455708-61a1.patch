# HG changeset patch
# User Gregory Szorc <gps@mozilla.com>
# Date 1524253445 25200
#      Fri Apr 20 12:44:05 2018 -0700
# Node ID 1e78f2ccb865f96d439c62e865f1467732f41b21
# Parent  d41633871a3253f5492db5d1557a2348d0c78820
Bug 1455708 - Find Python 3 in MozillaBuild, require Python 3 everywhere; r=mshal

We previously did not require Python 3.5+ everywhere because we failed
to detect Python 3.5 on Windows in CI. That's because CI isn't using
start-shell.bat and it hasn't yet updated PATH to include
%MOZILLABUILD%\python3.

We shouldn't need to teach CI to have PATH contain everything in
MozillaBuild. This commit teaches moz.configure to automatically use
MozillaBuild's Python 3 if we're running in MozillaBuild.

Since we can now detect Python 3 everywhere in CI, we make Python 3.5+
required on all build configurations.

MozReview-Commit-ID: BwgWGeYMyPM

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -374,58 +374,60 @@ shell = help_shell | shell
 
 
 # Python 3
 # ========
 
 option(env='PYTHON3', nargs=1, help='Python 3 interpreter (3.5 or later)')
 
 
-@depends('PYTHON3', 'MOZ_AUTOMATION')
+@depends('PYTHON3', 'MOZILLABUILD')
 @checking('for Python 3',
           callback=lambda x: '%s (%s)' % (x.path, x.str_version) if x else 'no')
 @imports(_from='__builtin__', _import='Exception')
-@imports('platform')
 @imports(_from='mozbuild.pythonutil', _import='find_python3_executable')
 @imports(_from='mozbuild.pythonutil', _import='python_executable_version')
-def python3(env_python, automation):
+def python3(env_python, mozillabuild):
     python = env_python[0] if env_python else None
 
     # If Python given by environment variable, it must work.
     if python:
         try:
             version = python_executable_version(python).version
         except Exception as e:
             raise FatalCheckError('could not determine version of PYTHON '
                                   '(%s): %s' % (python, e))
+    elif mozillabuild:
+        # MozillaBuild provides a Python 3.
+        python = normsep('%s/python3/python3.exe' % mozillabuild)
 
-        if version < (3, 5, 0):
-            raise FatalCheckError('PYTHON3 must point to Python 3.5 or newer; '
-                                  '%d.%d found' % (version[0], version[1]))
+        try:
+            version = python_executable_version(python).version
+        except Exception as e:
+            raise FatalCheckError('could not determine version of '
+                                  'MozillaBuild python: %s' % e)
     else:
         # Fall back to the search routine.
         python, version = find_python3_executable(min_version='3.5.0')
 
         # The API returns a bytes whereas everything in configure is unicode.
         if python:
             python = python.decode('utf-8')
 
-    # Outside of automation, require Python 3.5.
-    # In automation, only require where it is known to be installed.
-    require = not automation or platform.system() == 'Linux'
-
     if not python:
-        if not require:
-            return None
-
         raise FatalCheckError('Python 3.5 or newer is required to build. '
                               'Ensure a `python3.x` executable is in your '
                               'PATH or define PYTHON3 to point to a Python '
                               '3.5 executable.')
 
+    if version < (3, 5, 0):
+        raise FatalCheckError('Python 3.5 or newer is required to build; '
+                              '%s is Python %d.%d' % (python, version[0],
+                                                      version[1]))
+
     return namespace(
         path=python,
         version=version,
         str_version='.'.join(str(v) for v in version),
     )
 
 
 set_config('PYTHON3', depends_if(python3)(lambda p: p.path))
