# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1601917505 0
# Node ID fb38825fa2ffca6e55a4eda0234548c314d2b291
# Parent  59a556ead629204c5665e879ba5f5e17d17a57b1
Bug 1668718 - Don't `import mach_bootstrap` for `virtualenv`s that don't have `populate_local_paths` set, or in Python 3 r=mhentges,firefox-build-system-reviewers

Before, this would be written to `sitecustomize.py` irrespective of the value of `populate_local_paths`. This doesn't make sense -- since the local paths aren't included in the `virtualenv`'s `PYTHONPATH` when Python starts up, it doesn't know how to `import mach_bootstrap`. Since on `mach` startup the import hook will be loaded anyway, and the `virtualenv`s in `~/.mozbuild` (i.e. the only `virtualenv`s for which we don't `populate_local_paths`) are just used to run `mach`, this is fine and won't regress anything.

Also, since the `import` hook is only necessary for Python 2, add a couple conditional checks to get rid of the added overhead when we're running with Python 3.

This was never noticed because importing `sitecustomize` is allowed to throw an `ImportError`, which failure is ignored silently. This may be fixed in the latest version of `virtualenv`.

Differential Revision: https://phabricator.services.mozilla.com/D92290

diff --git a/build/mach_bootstrap.py b/build/mach_bootstrap.py
--- a/build/mach_bootstrap.py
+++ b/build/mach_bootstrap.py
@@ -403,10 +403,11 @@ class ImportHook(object):
             if os.path.exists(module.__file__):
                 os.remove(module.__file__)
             del sys.modules[module.__name__]
             module = self(name, globals, locals, fromlist, level)
 
         return module
 
 
-# Install our hook
-builtins.__import__ = ImportHook(builtins.__import__)
+# Install our hook. This can be deleted when the Python 3 migration is complete.
+if sys.version_info[0] < 3:
+    builtins.__import__ = ImportHook(builtins.__import__)
diff --git a/python/mozbuild/mozbuild/virtualenv.py b/python/mozbuild/mozbuild/virtualenv.py
--- a/python/mozbuild/mozbuild/virtualenv.py
+++ b/python/mozbuild/mozbuild/virtualenv.py
@@ -437,20 +437,23 @@ class VirtualenvManager(VirtualenvHelper
                 old_env_variables[k] = os.environ[k]
                 del os.environ[k]
 
             for package in packages:
                 handle_package(package)
 
         finally:
             if do_close:
-                sitecustomize.write(
-                    '# Importing mach_bootstrap has the side effect of\n'
-                    '# installing an import hook\n'
-                    'import mach_bootstrap\n')
+                # This hack isn't necessary for Python 3, or for the
+                # out-of-objdir virtualenvs.
+                if self.populate_local_paths and PY2:
+                    sitecustomize.write(
+                        '# Importing mach_bootstrap has the side effect of\n'
+                        '# installing an import hook\n'
+                        'import mach_bootstrap\n')
                 sitecustomize.close()
 
             os.environ.pop('MACOSX_DEPLOYMENT_TARGET', None)
 
             if old_target is not None:
                 os.environ['MACOSX_DEPLOYMENT_TARGET'] = old_target
 
             os.environ.update(old_env_variables)
