# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1524087070 14400
# Node ID a71038f16918df5b43dd9813f238b23fd703b6ba
# Parent  b23c22c84b249ede7f8f8ee62b5f160577bd67be
Bug 1454640 - [docs] Lazy load the package and version properties r=mshal on a CLOSED TREE

We no longer store the docs under a project name (since all the docs are now
built using the root conf.py). This mean the name and version are only used for
packaging and uploading, which typically is only used in CI.

This allows us to lazy load the package name and version, so we only read the
conf.py when we need to.

MozReview-Commit-ID: DV5Jxrbskoh

diff --git a/tools/docs/mach_commands.py b/tools/docs/mach_commands.py
--- a/tools/docs/mach_commands.py
+++ b/tools/docs/mach_commands.py
@@ -23,16 +23,18 @@ here = os.path.abspath(os.path.dirname(_
 @CommandProvider
 class Documentation(MachCommandBase):
     """Helps manage in-tree documentation."""
 
     def __init__(self, context):
         super(Documentation, self).__init__(context)
 
         self._manager = None
+        self._project = None
+        self._version = None
 
     @Command('doc', category='devenv',
              description='Generate and serve documentation from the tree.')
     @CommandArgument('path', default=None, metavar='DIRECTORY', nargs='?',
                      help='Path to documentation to build and display.')
     @CommandArgument('--format', default='html', dest='fmt',
                      help='Documentation format to write.')
     @CommandArgument('--outdir', default=None, metavar='DESTINATION',
@@ -68,27 +70,25 @@ class Documentation(MachCommandBase):
         path = path or os.path.join(self.topsrcdir, 'tools')
         path = os.path.normpath(os.path.abspath(path))
 
         docdir = self._find_doc_dir(path)
         if not docdir:
             return die('failed to generate documentation:\n'
                        '%s: could not find docs at this location' % path)
 
-        props = self._project_properties(docdir)
         result = self._run_sphinx(docdir, savedir, fmt=fmt)
         if result != 0:
             return die('failed to generate documentation:\n'
                        '%s: sphinx return code %d' % (path, result))
         else:
             print('\nGenerated documentation:\n%s' % savedir)
 
         if archive:
-            archive_path = os.path.join(outdir,
-                                        '%s.tar.gz' % props['project'])
+            archive_path = os.path.join(outdir, '%s.tar.gz' % self.project)
             create_tarball(archive_path, savedir)
             print('Archived to %s' % archive_path)
 
         if not serve:
             index_path = os.path.join(savedir, 'index.html')
             if auto_open and os.path.isfile(index_path):
                 webbrowser.open(index_path)
             return
@@ -124,33 +124,43 @@ class Documentation(MachCommandBase):
 
     @property
     def manager(self):
         if not self._manager:
             from moztreedocs import manager
             self._manager = manager
         return self._manager
 
-    def _project_properties(self, path):
+    def _read_project_properties(self):
         import imp
         path = os.path.normpath(self.manager.conf_py_path)
         with open(path, 'r') as fh:
             conf = imp.load_module('doc_conf', fh, path,
                                    ('.py', 'r', imp.PY_SOURCE))
 
         # Prefer the Mozilla project name, falling back to Sphinx's
         # default variable if it isn't defined.
         project = getattr(conf, 'moz_project_name', None)
         if not project:
             project = conf.project.replace(' ', '_')
 
-        return {
-            'project': project,
-            'version': getattr(conf, 'version', None)
-        }
+        self._project = project,
+        self._version = getattr(conf, 'version', None)
+
+    @property
+    def project(self):
+        if not self._project:
+            self._read_project_properties()
+        return self._project
+
+    @property
+    def version(self):
+        if not self._version:
+            self._read_project_properties()
+        return self._version
 
     def _find_doc_dir(self, path):
         if os.path.isfile(path):
             return
 
         valid_doc_dirs = ('doc', 'docs')
         if os.path.basename(path) in valid_doc_dirs:
             return path
diff --git a/tools/docs/mach_commands.py.1454640-6.later b/tools/docs/mach_commands.py.1454640-6.later
new file mode 100644
--- /dev/null
+++ b/tools/docs/mach_commands.py.1454640-6.later
@@ -0,0 +1,21 @@
+--- mach_commands.py
++++ mach_commands.py
+@@ -70,17 +70,17 @@ class Documentation(MachCommandBase):
+             print('\nGenerated documentation:\n%s' % savedir)
+ 
+         if archive:
+             archive_path = os.path.join(outdir, '%s.tar.gz' % self.project)
+             create_tarball(archive_path, savedir)
+             print('Archived to %s' % archive_path)
+ 
+         if upload:
+-            self._s3_upload(savedir, props['project'], props['version'])
++            self._s3_upload(savedir, self.project, self.version)
+ 
+         if not serve:
+             index_path = os.path.join(savedir, 'index.html')
+             if auto_open and os.path.isfile(index_path):
+                 webbrowser.open(index_path)
+             return
+ 
+         # Create livereload server. Any files modified in the specified docdir
