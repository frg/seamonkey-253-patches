# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1613121771 0
#      Fri Feb 12 09:22:51 2021 +0000
# Node ID 68a46b7ab6e99e25199bdfaa6b4f0b14e9c39c72
# Parent  3ac901ba324e1f8aa339e2220eae0d76e57acb9a
Bug 1692355 - [angle] Don't assert for non-sampleable non-ms sources in blitRenderbufferRect if we can CopySubResource. r=gw

Differential Revision: https://phabricator.services.mozilla.com/D104940

diff --git a/gfx/angle/checkout/out/gen/angle/angle_commit.h b/gfx/angle/checkout/out/gen/angle/angle_commit.h
--- a/gfx/angle/checkout/out/gen/angle/angle_commit.h
+++ b/gfx/angle/checkout/out/gen/angle/angle_commit.h
@@ -1,4 +1,4 @@
-#define ANGLE_COMMIT_HASH "9ee445bf2841"
+#define ANGLE_COMMIT_HASH "3778168311ca"
 #define ANGLE_COMMIT_HASH_SIZE 12
-#define ANGLE_COMMIT_DATE "2021-02-04 11:04:55 -0800"
-#define ANGLE_COMMIT_POSITION 14224
+#define ANGLE_COMMIT_DATE "2021-02-11 17:43:41 -0800"
+#define ANGLE_COMMIT_POSITION 14225
diff --git a/gfx/angle/checkout/src/libANGLE/renderer/d3d/d3d11/Renderer11.cpp b/gfx/angle/checkout/src/libANGLE/renderer/d3d/d3d11/Renderer11.cpp
--- a/gfx/angle/checkout/src/libANGLE/renderer/d3d/d3d11/Renderer11.cpp
+++ b/gfx/angle/checkout/src/libANGLE/renderer/d3d/d3d11/Renderer11.cpp
@@ -3461,56 +3461,16 @@ angle::Result Renderer11::blitRenderbuff
     ASSERT(drawRenderTarget11);
 
     const TextureHelper11 &drawTexture = drawRenderTarget11->getTexture();
     unsigned int drawSubresource       = drawRenderTarget11->getSubresourceIndex();
 
     RenderTarget11 *readRenderTarget11 = GetAs<RenderTarget11>(readRenderTarget);
     ASSERT(readRenderTarget11);
 
-    TextureHelper11 readTexture;
-    unsigned int readSubresource = 0;
-    d3d11::SharedSRV readSRV;
-
-    if (readRenderTarget->isMultisampled())
-    {
-        ANGLE_TRY(resolveMultisampledTexture(context, readRenderTarget11, depthBlit, stencilBlit,
-                                             &readTexture));
-
-        if (!stencilBlit)
-        {
-            const auto &readFormatSet = readTexture.getFormatSet();
-
-            D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
-            viewDesc.Format                    = readFormatSet.srvFormat;
-            viewDesc.ViewDimension             = D3D11_SRV_DIMENSION_TEXTURE2D;
-            viewDesc.Texture2D.MipLevels       = 1;
-            viewDesc.Texture2D.MostDetailedMip = 0;
-
-            ANGLE_TRY(allocateResource(GetImplAs<Context11>(context), viewDesc, readTexture.get(),
-                                       &readSRV));
-        }
-    }
-    else
-    {
-        ASSERT(readRenderTarget11);
-        readTexture     = readRenderTarget11->getTexture();
-        readSubresource = readRenderTarget11->getSubresourceIndex();
-        readSRV         = readRenderTarget11->getBlitShaderResourceView(context).makeCopy();
-        if (!readSRV.valid())
-        {
-            ASSERT(depthBlit || stencilBlit);
-            readSRV = readRenderTarget11->getShaderResourceView(context).makeCopy();
-        }
-        ASSERT(readSRV.valid());
-    }
-
-    // Stencil blits don't use shaders.
-    ASSERT(readSRV.valid() || stencilBlit);
-
     const gl::Extents readSize(readRenderTarget->getWidth(), readRenderTarget->getHeight(), 1);
     const gl::Extents drawSize(drawRenderTarget->getWidth(), drawRenderTarget->getHeight(), 1);
 
     // From the spec:
     // "The actual region taken from the read framebuffer is limited to the intersection of the
     // source buffers being transferred, which may include the color buffer selected by the read
     // buffer, the depth buffer, and / or the stencil buffer depending on mask."
     // This means negative x and y are out of bounds, and not to be read from. We handle this here
@@ -3616,21 +3576,65 @@ angle::Result Renderer11::blitRenderbuff
     bool outOfBounds = readRect.x < 0 || readRect.x + readRect.width > readSize.width ||
                        readRect.y < 0 || readRect.y + readRect.height > readSize.height ||
                        drawRect.x < 0 || drawRect.x + drawRect.width > drawSize.width ||
                        drawRect.y < 0 || drawRect.y + drawRect.height > drawSize.height;
 
     bool partialDSBlit =
         (nativeFormat.depthBits > 0 && depthBlit) != (nativeFormat.stencilBits > 0 && stencilBlit);
 
-    if (drawRenderTarget->getSamples() == readRenderTarget->getSamples() &&
+    const bool canCopySubresource =
+        drawRenderTarget->getSamples() == readRenderTarget->getSamples() &&
         readRenderTarget11->getFormatSet().formatID ==
             drawRenderTarget11->getFormatSet().formatID &&
         !stretchRequired && !outOfBounds && !reversalRequired && !partialDSBlit &&
-        !colorMaskingNeeded && (!(depthBlit || stencilBlit) || wholeBufferCopy))
+        !colorMaskingNeeded && (!(depthBlit || stencilBlit) || wholeBufferCopy);
+
+    TextureHelper11 readTexture;
+    unsigned int readSubresource = 0;
+    d3d11::SharedSRV readSRV;
+
+    if (readRenderTarget->isMultisampled())
+    {
+        ANGLE_TRY(resolveMultisampledTexture(context, readRenderTarget11, depthBlit, stencilBlit,
+                                             &readTexture));
+
+        if (!stencilBlit && !canCopySubresource)
+        {
+            const auto &readFormatSet = readTexture.getFormatSet();
+
+            D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
+            viewDesc.Format                    = readFormatSet.srvFormat;
+            viewDesc.ViewDimension             = D3D11_SRV_DIMENSION_TEXTURE2D;
+            viewDesc.Texture2D.MipLevels       = 1;
+            viewDesc.Texture2D.MostDetailedMip = 0;
+
+            ANGLE_TRY(allocateResource(GetImplAs<Context11>(context), viewDesc, readTexture.get(),
+                                       &readSRV));
+        }
+    }
+    else
+    {
+        ASSERT(readRenderTarget11);
+        readTexture     = readRenderTarget11->getTexture();
+        readSubresource = readRenderTarget11->getSubresourceIndex();
+
+        if (!canCopySubresource)
+        {
+            readSRV = readRenderTarget11->getBlitShaderResourceView(context).makeCopy();
+            if (!readSRV.valid())
+            {
+                ASSERT(depthBlit || stencilBlit);
+                readSRV = readRenderTarget11->getShaderResourceView(context).makeCopy();
+            }
+            ASSERT(readSRV.valid());
+        }
+    }
+
+    if (canCopySubresource)
     {
         UINT dstX = drawRect.x;
         UINT dstY = drawRect.y;
 
         D3D11_BOX readBox;
         readBox.left   = readRect.x;
         readBox.right  = readRect.x + readRect.width;
         readBox.top    = readRect.y;
diff --git a/gfx/angle/cherry_picks.txt b/gfx/angle/cherry_picks.txt
--- a/gfx/angle/cherry_picks.txt
+++ b/gfx/angle/cherry_picks.txt
@@ -1,31 +1,42 @@
-commit 9ee445bf28419a05d19cfc9caccc0e1c795c6149
+commit 3778168311ca10e8d57b3bce16bfcbc0f5b0dd01
+Author: Jeff Gilbert <jdashg@gmail.com>
+Date:   Thu Feb 11 17:34:00 2021 -0800
+
+    Don't assert for non-sampleable non-ms sources in blitRenderbufferRect if we can CopySubResource.
+    
+    In Firefox, we can have a source from DirectComposition without
+    D3D11_BIND_SHADER_RESOURCE. This is fine so long as our formats etc
+    match enough to hit the CopySubResource path.
+    Firefox bug: https://bugzilla.mozilla.org/show_bug.cgi?id=1692355
+
+commit f96c68edb00194eebccd998cfd4ceb7207810092
 Author: Jeff Gilbert <jdashg@gmail.com>
 Date:   Wed Feb 3 18:50:38 2021 -0800
 
     Add ANGLE_TRANSLATOR_ESSL_ONLY instead of using build system logic to choose.
     
     Build system logic is really hard to handle when vendoring into Gecko for Firefox.
 
-commit 00fc0892abedd5d5f0a7167ddf4431f7bb727486
+commit 2a40d0f143e00c46db28c664678529a36daf4e41
 Author: Jeff Gilbert <jdashg@gmail.com>
 Date:   Mon Feb 1 17:35:56 2021 -0800
 
     Build fixes needed by Gecko.
 
-commit 43dc75e84ad5d0b8255c3dcfec2efa2a3d017827
+commit 226aa28ce47cf20769f8f27f23255ca1eca83942
 Author: Jeff Muizelaar <jrmuizel@gmail.com>
 Date:   Mon Nov 9 17:09:12 2020 -0500
 
     Bug 1620075. Add a feature flag to allow ES3 on 10.0 devices. (#26)
     
     This lets us run WebRender on devices that only support D3D 10.0
 
-commit fffb2aa74832466f2433acb3f05a0b498f0a6350
+commit c0645aac0a4b4ad6bc9691ce381e5043115737b5
 Author: Jeff Muizelaar <jrmuizel@gmail.com>
 Date:   Wed May 6 14:04:42 2020 -0400
 
     Don't use ClearView if we previously used dual source blending on Intel gen6. (#22)
     
     Doing a ClearView after a dual source blend seems to cause a TDR on
     Intel SandyBridge. Presumeably this is because the ClearView is
     implemented as a regular draw and the driver doesn't properly set up the
