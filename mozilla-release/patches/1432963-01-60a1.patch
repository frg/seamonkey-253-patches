# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1517383150 -3600
# Node ID d7e5e7c4cf8857e267c60689e355aafeddfd2bdd
# Parent  4d7ded83036fe9e5366f5352631fcabc9c4ba63d
Bug 1432963 - Fixing workers headers - part 1 - no workers namespace for SharedWorker, r=smaug

diff --git a/dom/bindings/Bindings.conf b/dom/bindings/Bindings.conf
--- a/dom/bindings/Bindings.conf
+++ b/dom/bindings/Bindings.conf
@@ -767,21 +767,16 @@ DOMInterfaces = {
 'ServiceWorkerGlobalScope': {
     'headerFile': 'mozilla/dom/WorkerScope.h',
 },
 
 'ServiceWorkerRegistration': {
     'implicitJSContext': [ 'pushManager' ],
 },
 
-'SharedWorker': {
-    'nativeType': 'mozilla::dom::workers::SharedWorker',
-    'headerFile': 'mozilla/dom/workers/bindings/SharedWorker.h',
-},
-
 'SharedWorkerGlobalScope': {
     'headerFile': 'mozilla/dom/WorkerScope.h',
     'implicitJSContext': [ 'close' ],
 },
 
 'StreamFilter': {
     'nativeType': 'mozilla::extensions::StreamFilter',
 },
diff --git a/dom/workers/RuntimeService.h b/dom/workers/RuntimeService.h
--- a/dom/workers/RuntimeService.h
+++ b/dom/workers/RuntimeService.h
@@ -14,19 +14,24 @@
 #include "mozilla/dom/BindingDeclarations.h"
 #include "nsClassHashtable.h"
 #include "nsHashKeys.h"
 #include "nsTArray.h"
 
 class nsITimer;
 class nsPIDOMWindowInner;
 
+namespace mozilla {
+namespace dom {
+class SharedWorker;
+}
+}
+
 BEGIN_WORKERS_NAMESPACE
 
-class SharedWorker;
 struct WorkerLoadInfo;
 class WorkerThread;
 
 class RuntimeService final : public nsIObserver
 {
   struct SharedWorkerInfo
   {
     WorkerPrivate* mWorkerPrivate;
diff --git a/dom/workers/SharedWorker.cpp b/dom/workers/SharedWorker.cpp
--- a/dom/workers/SharedWorker.cpp
+++ b/dom/workers/SharedWorker.cpp
@@ -24,18 +24,18 @@
 #ifdef XP_WIN
 #undef PostMessage
 #endif
 
 using mozilla::dom::Optional;
 using mozilla::dom::Sequence;
 using mozilla::dom::MessagePort;
 using namespace mozilla;
-
-USING_WORKERS_NAMESPACE
+using namespace mozilla::dom;
+using namespace mozilla::dom::workers;
 
 SharedWorker::SharedWorker(nsPIDOMWindowInner* aWindow,
                            WorkerPrivate* aWorkerPrivate,
                            MessagePort* aMessagePort)
   : DOMEventTargetHelper(aWindow)
   , mWorkerPrivate(aWorkerPrivate)
   , mMessagePort(aMessagePort)
   , mFrozen(false)
diff --git a/dom/workers/SharedWorker.h b/dom/workers/SharedWorker.h
--- a/dom/workers/SharedWorker.h
+++ b/dom/workers/SharedWorker.h
@@ -20,32 +20,30 @@ class nsIDOMEvent;
 class nsPIDOMWindowInner;
 
 namespace mozilla {
 class EventChainPreVisitor;
 
 namespace dom {
 class MessagePort;
 class StringOrWorkerOptions;
-}
-} // namespace mozilla
 
-BEGIN_WORKERS_NAMESPACE
-
+namespace workers {
 class RuntimeService;
 class WorkerPrivate;
+}
 
 class SharedWorker final : public DOMEventTargetHelper
 {
-  friend class RuntimeService;
+  friend class workers::RuntimeService;
 
   typedef mozilla::ErrorResult ErrorResult;
   typedef mozilla::dom::GlobalObject GlobalObject;
 
-  RefPtr<WorkerPrivate> mWorkerPrivate;
+  RefPtr<workers::WorkerPrivate> mWorkerPrivate;
   RefPtr<MessagePort> mMessagePort;
   nsTArray<nsCOMPtr<nsIDOMEvent>> mFrozenEvents;
   bool mFrozen;
 
 public:
   static already_AddRefed<SharedWorker>
   Constructor(const GlobalObject& aGlobal, const nsAString& aScriptURL,
               const StringOrWorkerOptions& aOptions, ErrorResult& aRv);
@@ -77,32 +75,33 @@ public:
   IMPL_EVENT_HANDLER(error)
 
   virtual JSObject*
   WrapObject(JSContext* aCx, JS::Handle<JSObject*> aGivenProto) override;
 
   virtual nsresult
   GetEventTargetParent(EventChainPreVisitor& aVisitor) override;
 
-  WorkerPrivate*
+  workers::WorkerPrivate*
   GetWorkerPrivate() const
   {
     return mWorkerPrivate;
   }
 
 private:
   // This class can only be created from the RuntimeService.
   SharedWorker(nsPIDOMWindowInner* aWindow,
-               WorkerPrivate* aWorkerPrivate,
+               workers::WorkerPrivate* aWorkerPrivate,
                MessagePort* aMessagePort);
 
   // This class is reference-counted and will be destroyed from Release().
   ~SharedWorker();
 
   // Only called by MessagePort.
   void
   PostMessage(JSContext* aCx, JS::Handle<JS::Value> aMessage,
               const Sequence<JSObject*>& aTransferable, ErrorResult& aRv);
 };
 
-END_WORKERS_NAMESPACE
+} // dom namespace
+} // mozilla namespace
 
 #endif // mozilla_dom_workers_sharedworker_h__
diff --git a/dom/workers/WorkerPrivate.h b/dom/workers/WorkerPrivate.h
--- a/dom/workers/WorkerPrivate.h
+++ b/dom/workers/WorkerPrivate.h
@@ -31,27 +31,27 @@ namespace mozilla {
 namespace dom {
 
 class ClientInfo;
 class ClientSource;
 class Function;
 class MessagePort;
 class MessagePortIdentifier;
 class PerformanceStorage;
+class SharedWorker;
 class WorkerDebuggerGlobalScope;
 class WorkerErrorReport;
 class WorkerGlobalScope;
 struct WorkerOptions;
 
 } // dom namespace
 } // mozilla namespace
 
 BEGIN_WORKERS_NAMESPACE
 
-class SharedWorker;
 class WorkerControlRunnable;
 class WorkerDebugger;
 class WorkerEventTarget;
 class WorkerRunnable;
 class WorkerThread;
 
 // SharedMutex is a small wrapper around an (internal) reference-counted Mutex
 // object. It exists to avoid changing a lot of code to use Mutex* instead of
diff --git a/dom/workers/moz.build b/dom/workers/moz.build
--- a/dom/workers/moz.build
+++ b/dom/workers/moz.build
@@ -4,16 +4,17 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 with Files("**"):
     BUG_COMPONENT = ("Core", "DOM: Workers")
 
 # Public stuff.
 EXPORTS.mozilla.dom += [
+    'SharedWorker.h',
     'WorkerLocation.h',
     'WorkerNavigator.h',
     'WorkerPrivate.h',
     'WorkerRunnable.h',
     'WorkerScope.h',
 ]
 
 EXPORTS.mozilla.dom.workers += [
@@ -21,17 +22,16 @@ EXPORTS.mozilla.dom.workers += [
     'WorkerCommon.h',
     'WorkerDebugger.h',
     'WorkerDebuggerManager.h',
     'WorkerLoadInfo.h',
 ]
 
 # Stuff needed for the bindings, not really public though.
 EXPORTS.mozilla.dom.workers.bindings += [
-    'SharedWorker.h',
     'WorkerHolder.h',
     'WorkerHolderToken.h',
 ]
 
 XPIDL_MODULE = 'dom_workers'
 
 XPIDL_SOURCES += [
     'nsIWorkerDebugger.idl',
