# HG changeset patch
# User Chris Peterson <cpeterson@mozilla.com>
# Date 1534784865 25200
# Node ID 0119ff5d4de90ac8c66397721aacb91beb8f6756
# Parent  11b5498663f59f7f927071a379914207e79857c0
Bug 1485179 - Part 2: Resize BCCornerInfo's bit fields to fit all BCBorderOwner enum values. r=dholbert

Unfortunately, accepting all BCBorderOwner enum values causes the layout/reftests/table-bordercollapse/frame_above_rules_all.html mochitest to fail. To fix the -Wbitfield-enum-conversion warnings without breaking the test, this patch explicitly masks the BCBorderOwner enum values to preserve the previous implicit truncation. I filed follow-up bug 1508921 to remove this workaround and fix the test.

This enum truncation was reported by clang's -Wbitfield-enum-conversion warnings:

layout/tables/nsTableFrame.cpp:5318:14 [-Wbitfield-enum-conversion] bit-field 'ownerElem' is not wide enough to store all enumerators of 'BCBorderOwner'
layout/tables/nsTableFrame.cpp:5358:16 [-Wbitfield-enum-conversion] bit-field 'ownerElem' is not wide enough to store all enumerators of 'BCBorderOwner'
layout/tables/nsTableFrame.cpp:5374:18 [-Wbitfield-enum-conversion] bit-field 'subElem' is not wide enough to store all enumerators of 'BCBorderOwner'
layout/tables/nsTableFrame.cpp:5385:18 [-Wbitfield-enum-conversion] bit-field 'subElem' is not wide enough to store all enumerators of 'BCBorderOwner'

Differential Revision: https://phabricator.services.mozilla.com/D12382

diff --git a/layout/tables/nsTableFrame.cpp b/layout/tables/nsTableFrame.cpp
--- a/layout/tables/nsTableFrame.cpp
+++ b/layout/tables/nsTableFrame.cpp
@@ -5281,17 +5281,17 @@ CompareBorders(const nsIFrame*  aTableFr
 static bool
 Perpendicular(mozilla::LogicalSide aSide1,
               mozilla::LogicalSide aSide2)
 {
   return IsInline(aSide1) != IsInline(aSide2);
 }
 
 // Initial value indicating that BCCornerInfo's ownerStyle hasn't been set yet.
-#define BORDER_STYLE_UNSET 0xFF
+#define BORDER_STYLE_UNSET 0xF
 
 // XXX allocate this as number-of-cols+1 instead of number-of-cols+1 * number-of-rows+1
 struct BCCornerInfo
 {
   BCCornerInfo() { ownerColor = 0; ownerWidth = subWidth = ownerElem = subSide =
                    subElem = hasDashDot = numSegs = bevel = 0; ownerSide = eLogicalSideBStart;
                    ownerStyle = BORDER_STYLE_UNSET;
                    subStyle = NS_STYLE_BORDER_STYLE_SOLID; }
@@ -5303,32 +5303,35 @@ struct BCCornerInfo
               BCCellBorder  border);
 
   nscolor   ownerColor;     // color of borderOwner
   uint16_t  ownerWidth;     // pixel width of borderOwner
   uint16_t  subWidth;       // pixel width of the largest border intersecting the border perpendicular
                             // to ownerSide
   uint32_t  ownerSide:2;    // LogicalSide (e.g eLogicalSideBStart, etc) of the border
                             // owning the corner relative to the corner
-  uint32_t  ownerElem:3;    // elem type (e.g. eTable, eGroup, etc) owning the corner
-  uint32_t  ownerStyle:8;   // border style of ownerElem
+  uint32_t  ownerElem:4;    // elem type (e.g. eTable, eGroup, etc) owning the corner
+  uint32_t  ownerStyle:4;   // border style of ownerElem
   uint32_t  subSide:2;      // side of border with subWidth relative to the corner
-  uint32_t  subElem:3;      // elem type (e.g. eTable, eGroup, etc) of sub owner
-  uint32_t  subStyle:8;     // border style of subElem
+  uint32_t  subElem:4;      // elem type (e.g. eTable, eGroup, etc) of sub owner
+  uint32_t  subStyle:4;     // border style of subElem
   uint32_t  hasDashDot:1;   // does a dashed, dotted segment enter the corner, they cannot be beveled
   uint32_t  numSegs:3;      // number of segments entering corner
   uint32_t  bevel:1;        // is the corner beveled (uses the above two fields together with subWidth)
-  uint32_t  unused:1;
+  // 7 bits are unused
 };
 
 void
 BCCornerInfo::Set(mozilla::LogicalSide aSide,
                   BCCellBorder  aBorder)
 {
-  ownerElem  = aBorder.owner;
+  // FIXME bug 1508921: We mask 4-bit BCBorderOwner enum to 3 bits to preserve
+  // buggy behavior found by the frame_above_rules_all.html mochitest.
+  ownerElem  = aBorder.owner & 0x7;
+
   ownerStyle = aBorder.style;
   ownerWidth = aBorder.width;
   ownerColor = aBorder.color;
   ownerSide  = aSide;
   hasDashDot = 0;
   numSegs    = 0;
   if (aBorder.width > 0) {
     numSegs++;
