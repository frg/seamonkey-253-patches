# HG changeset patch
# User Nika Layzell <nika@thelayzells.com>
# Date 1516317508 18000
# Node ID 175470d22bd5258738db4f6b22c18ce57a87f4a4
# Parent  aa971ef6a78e0a28ca57343a5f142cae05e06131
Bug 1431867 - Part 1: Remove some unnecessary complexity from generated IPDL structs, r=mccr8

MozReview-Commit-ID: HvX4ZF8t9Eg

diff --git a/ipc/glue/IPCMessageUtils.h b/ipc/glue/IPCMessageUtils.h
--- a/ipc/glue/IPCMessageUtils.h
+++ b/ipc/glue/IPCMessageUtils.h
@@ -64,16 +64,21 @@ struct null_t {
 
 struct SerializedStructuredCloneBuffer final
 {
   SerializedStructuredCloneBuffer()
    : data(JS::StructuredCloneScope::Unassigned)
   {
   }
 
+  SerializedStructuredCloneBuffer(const SerializedStructuredCloneBuffer& aOther)
+   : SerializedStructuredCloneBuffer()
+  {
+    *this = aOther;
+  }
 
   SerializedStructuredCloneBuffer&
   operator=(const SerializedStructuredCloneBuffer& aOther)
   {
     data.Clear();
     data.initScope(aOther.data.scope());
     data.Append(aOther.data);
     return *this;
diff --git a/ipc/ipdl/ipdl/lower.py b/ipc/ipdl/ipdl/lower.py
--- a/ipc/ipdl/ipdl/lower.py
+++ b/ipc/ipdl/ipdl/lower.py
@@ -4,17 +4,17 @@
 
 import os, re, sys
 from copy import deepcopy
 from collections import OrderedDict
 
 import ipdl.ast
 import ipdl.builtin
 from ipdl.cxx.ast import *
-from ipdl.type import ActorType, TypeVisitor, builtinHeaderIncludes
+from ipdl.type import ActorType, UnionType, TypeVisitor, builtinHeaderIncludes
 
 ##-----------------------------------------------------------------------------
 ## "Public" interface to lowering
 ##
 class LowerToCxx:
     def lower(self, tu, segmentcapacitydict):
         '''returns |[ header: File ], [ cpp : File ]| representing the
 lowered form of |tu|'''
@@ -703,23 +703,16 @@ class HasFQName:
     def fqClassName(self):
         return self.decl.type.fullname()
 
 class _CompoundTypeComponent(_HybridDecl):
     def __init__(self, ipdltype, name, side, ct):
         _HybridDecl.__init__(self, ipdltype, name)
         self.side = side
         self.special = _hasVisibleActor(ipdltype)
-        self.recursive = ct.decl.type.mutuallyRecursiveWith(ipdltype)
-
-    def internalType(self):
-        if self.recursive:
-            return self.ptrToType()
-        else:
-            return self.bareType()
 
     # @override the following methods to pass |self.side| instead of
     # forcing the caller to remember which side we're declared to
     # represent.
     def bareType(self, side=None):
         return _HybridDecl.bareType(self, self.side)
     def refType(self, side=None):
         return _HybridDecl.refType(self, self.side)
@@ -760,18 +753,16 @@ class _StructField(_CompoundTypeComponen
         if self.ipdltype.isIPDL() and self.ipdltype.isActor():
             expr = ExprCast(expr, self.bareType(), const=1)
         return expr
 
     def refExpr(self, thisexpr=None):
         ref = self.memberVar()
         if thisexpr is not None:
             ref = ExprSelect(thisexpr, '.', ref.name)
-        if self.recursive:
-            ref = ExprDeref(ref)
         return ref
 
     def constRefExpr(self, thisexpr=None):
         # sigh, gross hack
         refexpr = self.refExpr(thisexpr)
         if 'Shmem' == self.ipdltype.name():
             refexpr = ExprCast(refexpr, Type('Shmem', ref=1), const=1)
         if 'ByteBuf' == self.ipdltype.name():
@@ -781,32 +772,16 @@ class _StructField(_CompoundTypeComponen
         return refexpr
 
     def argVar(self):
         return ExprVar('_'+ self.name)
 
     def memberVar(self):
         return ExprVar(self.name + '_')
 
-    def initStmts(self):
-        if self.recursive:
-            return [ StmtExpr(ExprAssn(self.memberVar(),
-                                       ExprNew(self.bareType()))) ]
-        elif self.ipdltype.isIPDL() and self.ipdltype.isActor():
-            return [ StmtExpr(ExprAssn(self.memberVar(),
-                                       ExprLiteral.NULL)) ]
-        else:
-            return []
-
-    def destructStmts(self):
-        if self.recursive:
-            return [ StmtExpr(ExprDelete(self.memberVar())) ]
-        else:
-            return []
-
 
 class UnionDecl(ipdl.ast.UnionDecl, HasFQName):
     def callType(self, var=None):
         func = ExprVar('type')
         if var is not None:
             func = ExprSelect(var, '.', func.name)
         return ExprCall(func)
 
@@ -828,25 +803,36 @@ IPDL union type."""
         _CompoundTypeComponent.__init__(self, ipdltype, 'V'+ flatname, side, ud)
         self.flattypename = flatname
         if special:
             if other is not None:
                 self.other = other
             else:
                 self.other = _UnionMember(ipdltype, ud, _otherSide(side), self)
 
+        # To create a finite object with a mutually recursive type, a union must
+        # be present somewhere in the recursive loop. Because of that we only
+        # need to care about introducing indirections inside unions.
+        self.recursive = ud.decl.type.mutuallyRecursiveWith(ipdltype)
+
     def enum(self):
         return 'T' + self.flattypename
 
     def pqEnum(self):
         return self.ud.name +'::'+ self.enum()
 
     def enumvar(self):
         return ExprVar(self.enum())
 
+    def internalType(self):
+        if self.recursive:
+            return self.ptrToType()
+        else:
+            return self.bareType()
+
     def unionType(self):
         """Type used for storage in generated C union decl."""
         if self.recursive:
             return self.ptrToType()
         else:
             return Type('mozilla::AlignedStorage2', T=self.internalType())
 
     def unionValue(self):
@@ -1483,17 +1469,30 @@ class _GenerateProtocolCode(ipdl.ast.Vis
 
         if len(aggregateTypeIncludes) != 0:
             hf.addthing(Whitespace.NL)
             hf.addthings([ Whitespace("// Headers for typedefs"), Whitespace.NL ])
 
             for headername in sorted(iter(aggregateTypeIncludes)):
                 hf.addthing(CppDirective('include', '"' + headername + '"'))
 
-        ipdl.ast.Visitor.visitTranslationUnit(self, tu)
+        # Manually run Visitor.visitTranslationUnit. For dependency resolution
+        # we need to handle structs and unions separately.
+        for cxxInc in tu.cxxIncludes:
+            cxxInc.accept(self)
+        for inc in tu.includes:
+            inc.accept(self)
+        self.generateStructsAndUnions(tu)
+        for using in tu.builtinUsing:
+            using.accept(self)
+        for using in tu.using:
+            using.accept(self)
+        if tu.protocol:
+            tu.protocol.accept(self)
+
         if tu.filetype == 'header':
             self.cppIncludeHeaders.append(_ipdlhHeaderName(tu))
 
         hf.addthing(Whitespace.NL)
         hf.addthings(_includeGuardEnd(hf))
 
         cf = self.cppfile
         cf.addthings((
@@ -1517,46 +1516,66 @@ class _GenerateProtocolCode(ipdl.ast.Vis
     def visitBuiltinCxxInclude(self, inc):
         self.hdrfile.addthing(CppDirective('include', '"'+ inc.file +'"'))
 
     def visitInclude(self, inc):
         if inc.tu.filetype == 'header':
             self.hdrfile.addthing(CppDirective(
                     'include', '"'+ _ipdlhHeaderName(inc.tu) +'.h"'))
 
-    def processStructOrUnionClass(self, su, which, forwarddecls, cls):
-        clsdecl, methoddefns = _splitClassDeclDefn(cls)
-
-        self.hdrfile.addthings(
-            [  Whitespace.NL ]
-            + forwarddecls
-            + [ Whitespace("""
+    def generateStructsAndUnions(self, tu):
+        '''Generate the definitions for all structs and unions. This will
+        re-order the declarations if needed in the C++ code such that
+        dependencies have already been defined.'''
+        decls = OrderedDict()
+        for su in tu.structsAndUnions:
+            if isinstance(su, StructDecl):
+                which = 'struct'
+                forwarddecls, fulldecltypes, cls = _generateCxxStruct(su)
+            else:
+                assert isinstance(su, UnionDecl)
+                which = 'union'
+                forwarddecls, fulldecltypes, cls = _generateCxxUnion(su)
+
+            clsdecl, methoddefns = _splitClassDeclDefn(cls)
+
+            # Store the declarations in the decls map so we can emit in
+            # dependency order.
+            decls[su.decl.type] = (
+                fulldecltypes,
+                [  Whitespace.NL ]
+                + forwarddecls
+                + [ Whitespace("""
 //-----------------------------------------------------------------------------
 // Declaration of the IPDL type |%s %s|
 //
 """% (which, su.name)),
-                _putInNamespaces(clsdecl, su.namespaces),
-            ])
-
-        self.structUnionDefns.extend([
-            Whitespace("""
+                    _putInNamespaces(clsdecl, su.namespaces),
+                ])
+
+            self.structUnionDefns.extend([
+                Whitespace("""
 //-----------------------------------------------------------------------------
 // Method definitions for the IPDL type |%s %s|
 //
 """% (which, su.name)),
-            _putInNamespaces(methoddefns, su.namespaces),
-        ])
-
-    def visitStructDecl(self, sd):
-        return self.processStructOrUnionClass(sd, 'struct',
-                                              *_generateCxxStruct(sd))
-
-    def visitUnionDecl(self, ud):
-        return self.processStructOrUnionClass(ud, 'union',
-                                              *_generateCxxUnion(ud))
+                _putInNamespaces(methoddefns, su.namespaces),
+            ])
+
+        # Generate the declarations structs in dependency order.
+        def gen_struct(deps, defn):
+            for dep in deps:
+                if dep in decls:
+                    d, t = decls[dep]
+                    del decls[dep]
+                    gen_struct(d, t)
+            self.hdrfile.addthings(defn)
+        while len(decls) > 0:
+            _, (d, t) = decls.popitem(False)
+            gen_struct(d, t)
 
     def visitProtocol(self, p):
         self.cppIncludeHeaders.append(_protocolHeaderName(self.protocol, ''))
 
         # Forward declare our own actors.
         self.hdrfile.addthings([
             Whitespace.NL,
             _makeForwardDeclForActor(p.decl.type, 'Parent'),
@@ -1801,24 +1820,26 @@ def _generateMessageConstructor(md, segm
                                       flags ])))
 
     return func
 
 ##--------------------------------------------------
 
 class _ComputeTypeDeps(TypeVisitor):
     '''Pass that gathers the C++ types that a particular IPDL type
-(recursively) depends on.  There are two kinds of dependencies: (i)
+(recursively) depends on.  There are three kinds of dependencies: (i)
 types that need forward declaration; (ii) types that need a |using|
-stmt.  Some types generate both kinds.'''
+stmt; (iii) IPDL structs or unions which must be fully declared
+before this struct.  Some types generate multiple kinds.'''
 
     def __init__(self, fortype, unqualifiedTypedefs=False):
         ipdl.type.TypeVisitor.__init__(self)
         self.usingTypedefs = [ ]
         self.forwardDeclStmts = [ ]
+        self.fullDeclTypes = [ ]
         self.fortype = fortype
         self.unqualifiedTypedefs = unqualifiedTypedefs
 
     def maybeTypedef(self, fqname, name):
         if fqname != name or self.unqualifiedTypedefs:
             self.usingTypedefs.append(Typedef(Type(fqname), name))
 
     def visitImportedCxxType(self, t):
@@ -1842,18 +1863,22 @@ stmt.  Some types generate both kinds.''
             _makeForwardDeclForActor(t.protocol, 'child'), Whitespace.NL
         ])
 
     def visitStructOrUnionType(self, su, defaultVisit):
         if su in self.visited or su == self.fortype: return
         self.visited.add(su)
         self.maybeTypedef(su.fullname(), su.name())
 
-        if su.mutuallyRecursiveWith(self.fortype):
+        # Mutually recursive fields in unions are behind indirection, so we only
+        # need a forward decl, and don't need a full type declaration.
+        if isinstance(self.fortype, UnionType) and self.fortype.mutuallyRecursiveWith(su):
             self.forwardDeclStmts.append(_makeForwardDecl(su))
+        else:
+            self.fullDeclTypes.append(su)
 
         return defaultVisit(self, su)
 
     def visitStructType(self, t):
         return self.visitStructOrUnionType(t, TypeVisitor.visitStructType)
 
     def visitUnionType(self, t):
         return self.visitStructOrUnionType(t, TypeVisitor.visitUnionType)
@@ -1886,87 +1911,61 @@ def _generateCxxStruct(sd):
     ''' '''
     # compute all the typedefs and forward decls we need to make
     gettypedeps = _ComputeTypeDeps(sd.decl.type)
     for f in sd.fields:
         f.ipdltype.accept(gettypedeps)
 
     usingTypedefs = gettypedeps.usingTypedefs
     forwarddeclstmts = gettypedeps.forwardDeclStmts
+    fulldecltypes = gettypedeps.fullDeclTypes
 
     struct = Class(sd.name, final=1)
     struct.addstmts([ Label.PRIVATE ]
                     + usingTypedefs
                     + [ Whitespace.NL, Label.PUBLIC ])
 
     constreftype = Type(sd.name, const=1, ref=1)
-    initvar = ExprVar('Init')
-    callinit = ExprCall(initvar)
-    assignvar = ExprVar('Assign')
 
     def fieldsAsParamList():
         return [ Decl(f.inType(), f.argVar().name) for f in sd.fields ]
 
     def assignFromOther(oexpr):
         return ExprCall(assignvar,
                         args=[ f.initExpr(oexpr) for f in sd.fields ])
 
     # If this is an empty struct (no fields), then the default ctor
     # and "create-with-fields" ctors are equivalent.  So don't bother
     # with the default ctor.
     if len(sd.fields):
         # Struct()
-        defctor = ConstructorDefn(ConstructorDecl(sd.name))
-        defctor.addstmt(StmtExpr(callinit))
-        defctor.memberinits = []
-        for f in sd.fields:
-          # Only generate default values for primitives.
-          if not (f.ipdltype.isCxx() and f.ipdltype.isAtom()):
-            continue
-          defctor.memberinits.append(ExprMemberInit(f.memberVar()))
+        defctor = ConstructorDefn(ConstructorDecl(sd.name, force_inline=1))
+
+        # We want to explicitly default-construct every member of the struct.
+        # This will initialize all primitives which wouldn't be initialized
+        # normally to their default values, and will initialize any actor member
+        # pointers to the correct default value of `nullptr`. Other C++ types
+        # with custom constructors must also provide a default constructor.
+        defctor.memberinits = [ ExprMemberInit(f.memberVar()) for f in sd.fields ]
         struct.addstmts([ defctor, Whitespace.NL ])
 
     # Struct(const field1& _f1, ...)
     valctor = ConstructorDefn(ConstructorDecl(sd.name,
                                               params=fieldsAsParamList(),
                                               force_inline=1))
-    valctor.addstmts([
-        StmtExpr(callinit),
-        StmtExpr(ExprCall(assignvar,
-                          args=[ f.argVar() for f in sd.fields ]))
-    ])
+    valctor.memberinits = [ ExprMemberInit(f.memberVar(),
+                                           args=[ f.argVar() ])
+                            for f in sd.fields ]
     struct.addstmts([ valctor, Whitespace.NL ])
 
-    # Struct(const Struct& _o)
-    ovar = ExprVar('_o')
-    copyctor = ConstructorDefn(ConstructorDecl(
-        sd.name,
-        params=[ Decl(constreftype, ovar.name) ],
-        force_inline=1))
-    copyctor.addstmts([
-        StmtExpr(callinit),
-        StmtExpr(assignFromOther(ovar))
-    ])
-    struct.addstmts([ copyctor, Whitespace.NL ])
-
-    # ~Struct()
-    dtor = DestructorDefn(DestructorDecl(sd.name))
-    for f in sd.fields:
-        dtor.addstmts(f.destructStmts())
-    struct.addstmts([ dtor, Whitespace.NL ])
-
-    # Struct& operator=(const Struct& _o)
-    opeq = MethodDefn(MethodDecl(
-        'operator=',
-        params=[ Decl(constreftype, ovar.name) ],
-        force_inline=1))
-    opeq.addstmt(StmtExpr(assignFromOther(ovar)))
-    struct.addstmts([ opeq, Whitespace.NL ])
+    # The default copy, move, and assignment constructors, and the default
+    # destructor, will do the right thing.
 
     # bool operator==(const Struct& _o)
+    ovar = ExprVar('_o')
     opeqeq = MethodDefn(MethodDecl(
         'operator==',
         params=[ Decl(constreftype, ovar.name) ],
         ret=Type.BOOL,
         const=1))
     for f in sd.fields:
         ifneq = StmtIf(ExprNot(
             ExprBinary(ExprCall(f.getMethod()), '==',
@@ -2001,34 +2000,21 @@ def _generateCxxStruct(sd):
         getconst = MethodDefn(getconstdecl)
         getconst.addstmt(StmtReturn(f.constRefExpr()))
 
         struct.addstmts([ get, getconst, Whitespace.NL ])
 
     # private:
     struct.addstmt(Label.PRIVATE)
 
-    # Init()
-    init = MethodDefn(MethodDecl(initvar.name))
-    for f in sd.fields:
-        init.addstmts(f.initStmts())
-    struct.addstmts([ init, Whitespace.NL ])
-
-    # Assign(const field1& _f1, ...)
-    assign = MethodDefn(MethodDecl(assignvar.name,
-                                   params=fieldsAsParamList()))
-    assign.addstmts([ StmtExpr(ExprAssn(f.refExpr(), f.argVar()))
+    # members
+    struct.addstmts([ StmtDecl(Decl(f.bareType(), f.memberVar().name))
                       for f in sd.fields ])
-    struct.addstmts([ assign, Whitespace.NL ])
-
-    # members
-    struct.addstmts([ StmtDecl(Decl(f.internalType(), f.memberVar().name))
-                      for f in sd.fields ])
-
-    return forwarddeclstmts, struct
+
+    return forwarddeclstmts, fulldecltypes, struct
 
 ##--------------------------------------------------
 
 def _generateCxxUnion(ud):
     # This Union class basically consists of a type (enum) and a
     # union for storage.  The union can contain POD and non-POD
     # types.  Each type needs a copy ctor, assignment operator,
     # and dtor.
@@ -2095,16 +2081,17 @@ def _generateCxxUnion(ud):
 
     # compute all the typedefs and forward decls we need to make
     gettypedeps = _ComputeTypeDeps(ud.decl.type)
     for c in ud.components:
         c.ipdltype.accept(gettypedeps)
 
     usingTypedefs = gettypedeps.usingTypedefs
     forwarddeclstmts = gettypedeps.forwardDeclStmts
+    fulldecltypes = gettypedeps.fullDeclTypes
 
     # the |Type| enum, used to switch on the discunion's real type
     cls.addstmt(Label.PUBLIC)
     typeenum = TypeEnum(typetype.name)
     typeenum.addId(tnonevar.name, 0)
     firstid = ud.components[0].enum()
     typeenum.addId(firstid, 1)
     for c in ud.components[1:]:
@@ -2392,17 +2379,17 @@ def _generateCxxUnion(ud):
 
     # private vars
     cls.addstmts([
         Label.PRIVATE,
         StmtDecl(Decl(valuetype, mvaluevar.name)),
         StmtDecl(Decl(typetype, mtypevar.name))
     ])
 
-    return forwarddeclstmts, cls
+    return forwarddeclstmts, fulldecltypes, cls
 
 ##-----------------------------------------------------------------------------
 
 class _FindFriends(ipdl.ast.Visitor):
     def __init__(self):
         self.mytype = None              # ProtocolType
         self.vtype = None               # ProtocolType
         self.friends = set()            # set<ProtocolType>
