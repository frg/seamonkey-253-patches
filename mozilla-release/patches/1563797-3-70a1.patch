# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1562853819 0
# Node ID 7f92f148ea546a606b4e434049ea3f4d29450b1c
# Parent  93a99f189b0b2ede007ea4377ea0283ab09de551
Bug 1563797 - Use 'backports.shutil_which' instead of 'which' across the tree r=Callek

Differential Revision: https://phabricator.services.mozilla.com/D37097

diff --git a/js/src/builtin/embedjs.py b/js/src/builtin/embedjs.py
--- a/js/src/builtin/embedjs.py
+++ b/js/src/builtin/embedjs.py
@@ -32,21 +32,24 @@
 # OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 # This utility converts JS files containing self-hosted builtins into a C
 # header file that can be embedded into SpiderMonkey.
 #
 # It uses the C preprocessor to process its inputs.
 
 from __future__ import with_statement
+
+import errno
 import re, sys, os, subprocess
-import mozpack.path as mozpath
 import shlex
-import which
+
 import buildconfig
+import mozpack.path as mozpath
+from mozfile import which
 
 def ToCAsciiArray(lines):
   result = []
   for chr in lines:
     value = ord(chr)
     assert value < 128
     result.append(str(value))
   return ", ".join(result)
@@ -100,17 +103,21 @@ def embed(cxx, preprocessorOption, cppfl
     'sources_name': 'compressedSources',
     'compressed_total_length': len(compressed),
     'raw_total_length': len(processed),
     'namespace': namespace
   })
 
 def preprocess(cxx, preprocessorOption, source, args = []):
   if (not os.path.exists(cxx[0])):
-    cxx[0] = which.which(cxx[0])
+      binary = cxx[0]
+      cxx[0] = which(binary)
+      if not cxx[0]:
+          raise OSError(errno.ENOENT, "%s not found on PATH" % binary)
+
   # Clang seems to complain and not output anything if the extension of the
   # input is not something it recognizes, so just fake a .cpp here.
   tmpIn = 'self-hosting-cpp-input.cpp';
   tmpOut = 'self-hosting-preprocessed.pp';
   outputArg = shlex.split(preprocessorOption + tmpOut)
 
   with open(tmpIn, 'wb') as input:
     input.write(source)
diff --git a/python/mozboot/mozboot/mach_commands.py b/python/mozboot/mozboot/mach_commands.py
--- a/python/mozboot/mozboot/mach_commands.py
+++ b/python/mozboot/mozboot/mach_commands.py
@@ -1,14 +1,15 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this,
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
+import errno
 import sys
 
 from mach.decorators import (
     CommandArgument,
     CommandProvider,
     Command,
 )
 
@@ -62,33 +63,34 @@ class VersionControlCommands(object):
 
         User choice is respected: no changes are made without explicit
         confirmation from you.
 
         If "--update-only" is used, the interactive wizard is disabled
         and this command only ensures that remote repositories providing
         VCS extensions are up to date.
         """
-        import which
         import mozboot.bootstrap as bootstrap
         import mozversioncontrol
+        from mozfile import which
 
         repo = mozversioncontrol.get_repository_object(self._context.topdir)
-        vcs = 'hg'
+        tool = 'hg'
         if repo.name == 'git':
-            vcs = 'git'
+            tool = 'git'
 
-        # "hg" is an executable script with a shebang, which will be found
-        # by which.which. We need to pass a win32 executable to the function
-        # because we spawn a process
-        # from it.
+        # "hg" is an executable script with a shebang, which will be found by
+        # which. We need to pass a win32 executable to the function because we
+        # spawn a process from it.
         if sys.platform in ('win32', 'msys'):
-            vcs = which.which(vcs + '.exe')
-        else:
-            vcs = which.which(vcs)
+            tool += '.exe'
+
+        vcs = which(tool)
+        if not vcs:
+            raise OSError(errno.ENOENT, "Could not find {} on $PATH".format(tool))
 
         if update_only:
             if repo.name == 'git':
                 bootstrap.update_git_tools(vcs, self._context.state_dir, self._context.topdir)
             else:
                 bootstrap.update_vct(vcs, self._context.state_dir)
         else:
             if repo.name == 'git':
diff --git a/testing/mach_commands.py b/testing/mach_commands.py
--- a/testing/mach_commands.py
+++ b/testing/mach_commands.py
@@ -1,15 +1,16 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import argparse
+import errno
 import json
 import logging
 import os
 import sys
 import tempfile
 import subprocess
 import shutil
 
@@ -562,19 +563,18 @@ class TestInfoCommand(MachCommandBase):
                      help='Retrieve and display ActiveData test result summary.')
     @CommandArgument('--show-durations', action='store_true',
                      help='Retrieve and display ActiveData test duration summary.')
     @CommandArgument('--show-bugs', action='store_true',
                      help='Retrieve and display related Bugzilla bugs.')
     @CommandArgument('--verbose', action='store_true',
                      help='Enable debug logging.')
     def test_info(self, **params):
-
-        import which
         from mozbuild.base import MozbuildObject
+        from mozfile import which
 
         self.branches = params['branches']
         self.start = params['start']
         self.end = params['end']
         self.show_info = params['show_info']
         self.show_results = params['show_results']
         self.show_durations = params['show_durations']
         self.show_bugs = params['show_bugs']
@@ -590,27 +590,25 @@ class TestInfoCommand(MachCommandBase):
             self.show_durations = True
             self.show_bugs = True
 
         here = os.path.abspath(os.path.dirname(__file__))
         build_obj = MozbuildObject.from_environment(cwd=here)
 
         self._hg = None
         if conditions.is_hg(build_obj):
-            if self._is_windows():
-                self._hg = which.which('hg.exe')
-            else:
-                self._hg = which.which('hg')
+            self._hg = which('hg')
+            if not self._hg:
+                raise OSError(errno.ENOENT, "Could not find 'hg' on PATH.")
 
         self._git = None
         if conditions.is_git(build_obj):
-            if self._is_windows():
-                self._git = which.which('git.exe')
-            else:
-                self._git = which.which('git')
+            self._git = which('git')
+            if not self._git:
+                raise OSError(errno.ENOENT, "Could not find 'git' on PATH.")
 
         for test_name in params['test_names']:
             print("===== %s =====" % test_name)
             self.test_name = test_name
             if len(self.test_name) < 6:
                 print("'%s' is too short for a test name!" % self.test_name)
                 continue
             if self.show_info:
diff --git a/tools/docs/mach_commands.py b/tools/docs/mach_commands.py
--- a/tools/docs/mach_commands.py
+++ b/tools/docs/mach_commands.py
@@ -3,25 +3,23 @@
 # file, # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import os
 import sys
 from functools import partial
 
+from mozbuild.base import MachCommandBase
 from mach.decorators import (
     Command,
     CommandArgument,
     CommandProvider,
 )
 
-import which
-from mozbuild.base import MachCommandBase
-
 here = os.path.abspath(os.path.dirname(__file__))
 
 
 @CommandProvider
 class Documentation(MachCommandBase):
     """Helps manage in-tree documentation."""
 
     def __init__(self, context):
@@ -46,19 +44,20 @@ class Documentation(MachCommandBase):
                      help="Don't automatically open HTML docs in a browser.")
     @CommandArgument('--no-serve', dest='serve', default=True, action='store_false',
                      help="Don't serve the generated docs after building.")
     @CommandArgument('--http', default='localhost:5500', metavar='ADDRESS',
                      help='Serve documentation on the specified host and port, '
                           'default "localhost:5500".')
     def build_docs(self, path=None, fmt='html', outdir=None, auto_open=True,
                    serve=True, http=None, archive=False):
-        try:
-            which.which('jsdoc')
-        except which.WhichError:
+
+        from mozfile import which
+
+        if not which('jsdoc'):
             return die('jsdoc not found - please install from npm.')
 
         self._activate_virtualenv()
         self.virtualenv_manager.install_pip_requirements(
             os.path.join(here, 'requirements.txt'), quiet=True)
 
         import webbrowser
         from livereload import Server
diff --git a/tools/lint/docs/create.rst b/tools/lint/docs/create.rst
--- a/tools/lint/docs/create.rst
+++ b/tools/lint/docs/create.rst
@@ -90,30 +90,32 @@ let's call the file ``flake8_lint.py``:
 
     import json
     import os
     import subprocess
     from collections import defaultdict
 
     from mozlint import result
 
+    try:
+        from shutil import which
+    except ImportError:
+        from shutil_which import which
+
 
     FLAKE8_NOT_FOUND = """
     Could not find flake8! Install flake8 and try again.
     """.strip()
 
 
     def lint(files, config, **lintargs):
-        import which
-
         binary = os.environ.get('FLAKE8')
         if not binary:
-            try:
-                binary = which.which('flake8')
-            except which.WhichError:
+            binary = which('flake8')
+            if not binary:
                 print(FLAKE8_NOT_FOUND)
                 return 1
 
         # Flake8 allows passing in a custom format string. We use
         # this to help mold the default flake8 format into what
         # mozlint's Issue object expects.
         cmdargs = [
             binary,
diff --git a/tools/lint/rust/__init__.py b/tools/lint/rust/__init__.py
--- a/tools/lint/rust/__init__.py
+++ b/tools/lint/rust/__init__.py
@@ -2,20 +2,20 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function
 from collections import namedtuple
 
 import os
 import signal
-import which
 import re
 import subprocess
 
+from mozfile import which
 from mozlint import result
 from mozlint.pathutils import expand_exclusions
 from mozprocess import ProcessHandler
 
 RUSTFMT_NOT_FOUND = """
 Could not find rustfmt! Install rustfmt and try again.
 
     $ rustup component add rustfmt
@@ -100,20 +100,17 @@ def get_rustfmt_binary():
     """
     Returns the path of the first rustfmt binary available
     if not found returns None
     """
     binary = os.environ.get("RUSTFMT")
     if binary:
         return binary
 
-    try:
-        return which.which("rustfmt")
-    except which.WhichError:
-        return None
+    return which("rustfmt")
 
 
 def is_old_rustfmt(binary):
     """
     Check if we are running the deprecated rustfmt
     """
     try:
         output = subprocess.check_output(
diff --git a/tools/lint/shell/__init__.py b/tools/lint/shell/__init__.py
--- a/tools/lint/shell/__init__.py
+++ b/tools/lint/shell/__init__.py
@@ -2,27 +2,27 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function
 
 import os
 import json
 import signal
-import which
 
-# Py3/Py2 compatibility.
+# py2-compat
 try:
     from json.decoder import JSONDecodeError
 except ImportError:
     JSONDecodeError = ValueError
 
 import mozpack.path as mozpath
+from mozfile import which
+from mozlint import result
 from mozpack.files import FileFinder
-from mozlint import result
 from mozprocess import ProcessHandlerMixin
 
 
 SHELLCHECK_NOT_FOUND = """
 Unable to locate shellcheck, please ensure it is installed and in
 your PATH or set the SHELLCHECK environment variable.
 
 https://shellcheck.net or your system's package manager.
@@ -130,20 +130,17 @@ def get_shellcheck_binary():
     """
     Returns the path of the first shellcheck binary available
     if not found returns None
     """
     binary = os.environ.get('SHELLCHECK')
     if binary:
         return binary
 
-    try:
-        return which.which('shellcheck')
-    except which.WhichError:
-        return None
+    return which('shellcheck')
 
 
 def lint(paths, config, **lintargs):
 
     binary = get_shellcheck_binary()
 
     if not binary:
         print(SHELLCHECK_NOT_FOUND)
diff --git a/tools/lint/spell/__init__.py b/tools/lint/spell/__init__.py
--- a/tools/lint/spell/__init__.py
+++ b/tools/lint/spell/__init__.py
@@ -1,25 +1,25 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function
 
 import os
 import signal
-import which
 import re
 
-# Py3/Py2 compatibility.
+# py2-compat
 try:
     from json.decoder import JSONDecodeError
 except ImportError:
     JSONDecodeError = ValueError
 
+from mozfile import which
 from mozlint import result
 from mozlint.util import pip
 from mozprocess import ProcessHandlerMixin
 
 here = os.path.abspath(os.path.dirname(__file__))
 CODESPELL_REQUIREMENTS_PATH = os.path.join(here, 'codespell_requirements.txt')
 
 CODESPELL_NOT_FOUND = """
@@ -85,20 +85,17 @@ def get_codespell_binary():
     """
     Returns the path of the first codespell binary available
     if not found returns None
     """
     binary = os.environ.get('CODESPELL')
     if binary:
         return binary
 
-    try:
-        return which.which('codespell')
-    except which.WhichError:
-        return None
+    return which('codespell')
 
 
 def lint(paths, config, fix=None, **lintargs):
 
     if not pip.reinstall_program(CODESPELL_REQUIREMENTS_PATH):
         print(CODESPELL_INSTALL_ERROR)
         return 1
 
diff --git a/tools/lint/yamllint_/__init__.py b/tools/lint/yamllint_/__init__.py
--- a/tools/lint/yamllint_/__init__.py
+++ b/tools/lint/yamllint_/__init__.py
@@ -3,21 +3,20 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 import re
 import os
 import signal
 import subprocess
 from collections import defaultdict
 
-import which
-from mozprocess import ProcessHandlerMixin
-
+from mozfile import which
 from mozlint import result
 from mozlint.pathutils import get_ancestors_by_name
+from mozprocess import ProcessHandlerMixin
 
 
 here = os.path.abspath(os.path.dirname(__file__))
 YAMLLINT_REQUIREMENTS_PATH = os.path.join(here, 'yamllint_requirements.txt')
 
 
 YAMLLINT_INSTALL_ERROR = """
 Unable to install correct version of yamllint
@@ -66,20 +65,17 @@ def get_yamllint_binary():
     """
     Returns the path of the first yamllint binary available
     if not found returns None
     """
     binary = os.environ.get('YAMLLINT')
     if binary:
         return binary
 
-    try:
-        return which.which('yamllint')
-    except which.WhichError:
-        return None
+    return which('yamllint')
 
 
 def _run_pip(*args):
     """
     Helper function that runs pip with subprocess
     """
     try:
         subprocess.check_output(['pip'] + list(args),
diff --git a/xpcom/reflect/xptcall/md/win32/preprocess.py b/xpcom/reflect/xptcall/md/win32/preprocess.py
--- a/xpcom/reflect/xptcall/md/win32/preprocess.py
+++ b/xpcom/reflect/xptcall/md/win32/preprocess.py
@@ -1,27 +1,33 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
+import errno
 import sys
 import os
 import shlex
 import subprocess
 import tempfile
-import which
+
 import buildconfig
+from mozfile import which
 
 
 def preprocess(out, asm_file):
     cxx = shlex.split(buildconfig.substs['CXX'])
     if not os.path.exists(cxx[0]):
-        cxx[0] = which.which(cxx[0])
+        tool = cxx[0]
+        cxx[0] = which(tool)
+        if not cxx[0]:
+            raise OSError(errno.ENOENT, "Could not find {} on PATH.".format(tool))
+
     cppflags = buildconfig.substs['OS_CPPFLAGS']
 
     # subprocess.Popen(stdout=) only accepts actual file objects, which `out`,
     # above, is not.  So fake a temporary file to write to.
     (outhandle, tmpout) = tempfile.mkstemp(suffix='.cpp')
 
     # #line directives will confuse armasm64, and /EP is the only way to
     # preprocess without emitting #line directives.
