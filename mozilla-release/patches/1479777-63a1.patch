# HG changeset patch
# User Masatoshi Kimura <VYV03354@nifty.ne.jp>
# Date 1533042223 -32400
# Node ID 93516631d63527d285b518f7d5e3b8aa800b627a
# Parent  91c968d08a2ae09e47537a89f298eda5e3ba256d
Bug 1479777 - Reduce clang-cl warnings from mozglue/misc/interceptor/. r=aklotz

diff --git a/mozglue/misc/interceptor/PatcherBase.h b/mozglue/misc/interceptor/PatcherBase.h
--- a/mozglue/misc/interceptor/PatcherBase.h
+++ b/mozglue/misc/interceptor/PatcherBase.h
@@ -20,17 +20,17 @@ protected:
 
   template <typename... Args>
   explicit WindowsDllPatcherBase(Args... aArgs)
     : mVMPolicy(mozilla::Forward<Args>(aArgs)...)
   {
   }
 
   ReadOnlyTargetFunction<MMPolicyT>
-  ResolveRedirectedAddress(const void* aOriginalFunction)
+  ResolveRedirectedAddress(FARPROC aOriginalFunction)
   {
     ReadOnlyTargetFunction<MMPolicyT> origFn(mVMPolicy, aOriginalFunction);
     // If function entry is jmp rel8 stub to the internal implementation, we
     // resolve redirected address from the jump target.
     if (origFn[0] == 0xeb) {
       int8_t offset = (int8_t)(origFn[1]);
       if (offset <= 0) {
         // Bail out for negative offset: probably already patched by some
diff --git a/mozglue/misc/interceptor/PatcherDetour.h b/mozglue/misc/interceptor/PatcherDetour.h
--- a/mozglue/misc/interceptor/PatcherDetour.h
+++ b/mozglue/misc/interceptor/PatcherDetour.h
@@ -396,17 +396,17 @@ protected:
     return aModBits | (aReg << kRegFieldShift) | aRm;
   }
 
   void CreateTrampoline(ReadOnlyTargetFunction<MMPolicyT>& origBytes,
                         intptr_t aDest, void** aOutTramp)
   {
     *aOutTramp = nullptr;
 
-    Trampoline<MMPolicyT> tramp(mVMPolicy.GetNextTrampoline());
+    Trampoline<MMPolicyT> tramp(this->mVMPolicy.GetNextTrampoline());
     if (!tramp) {
       return;
     }
 
     // The beginning of the trampoline contains two pointer-width slots:
     // [0]: |this|, so that we know whether the trampoline belongs to us;
     // [1]: Pointer to original function, so that we can reset the hook upon
     //      destruction.
diff --git a/mozglue/misc/interceptor/TargetFunction.h b/mozglue/misc/interceptor/TargetFunction.h
--- a/mozglue/misc/interceptor/TargetFunction.h
+++ b/mozglue/misc/interceptor/TargetFunction.h
@@ -7,16 +7,17 @@
 #ifndef mozilla_interceptor_TargetFunction_h
 #define mozilla_interceptor_TargetFunction_h
 
 #include "mozilla/Assertions.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/Maybe.h"
 #include "mozilla/Tuple.h"
 #include "mozilla/Types.h"
+#include "mozilla/Unused.h"
 #include "mozilla/Vector.h"
 
 #include <memory>
 
 namespace mozilla {
 namespace interceptor {
 
 template <typename MMPolicy>
@@ -470,30 +471,30 @@ public:
     , mBase(aOther.mBase)
   {
   }
 
   ReadOnlyTargetBytes(const ReadOnlyTargetBytes& aOther)
     : mMMPolicy(aOther.mMMPolicy)
     , mBase(aOther.mBase)
   {
-    mLocalBytes.appendAll(aOther.mLocalBytes);
+    Unused << mLocalBytes.appendAll(aOther.mLocalBytes);
   }
 
   ReadOnlyTargetBytes(const ReadOnlyTargetBytes& aOther,
                       const uint32_t aOffsetFromOther)
     : mMMPolicy(aOther.mMMPolicy)
     , mBase(aOther.mBase + aOffsetFromOther)
   {
     if (aOffsetFromOther >= aOther.mLocalBytes.length()) {
       return;
     }
 
-    mLocalBytes.append(aOther.mLocalBytes.begin() + aOffsetFromOther,
-                       aOther.mLocalBytes.end());
+    Unused << mLocalBytes.append(aOther.mLocalBytes.begin() + aOffsetFromOther,
+                                 aOther.mLocalBytes.end());
   }
 
   void EnsureLimit(uint32_t aDesiredLimit)
   {
     size_t prevSize = mLocalBytes.length();
     if (aDesiredLimit < prevSize) {
       return;
     }
@@ -672,16 +673,23 @@ class MOZ_STACK_CLASS ReadOnlyTargetFunc
 
 public:
   ReadOnlyTargetFunction(const MMPolicy& aMMPolicy, const void* aFunc)
     : mTargetBytes(TargetBytesPtr<MMPolicy>::Make(aMMPolicy, aFunc))
     , mOffset(0)
   {
   }
 
+  ReadOnlyTargetFunction(const MMPolicy& aMMPolicy, FARPROC aFunc)
+    : mTargetBytes(TargetBytesPtr<MMPolicy>::Make(aMMPolicy,
+        reinterpret_cast<const void*>(aFunc)))
+    , mOffset(0)
+  {
+  }
+
   ReadOnlyTargetFunction(const MMPolicy& aMMPolicy, uintptr_t aFunc)
     : mTargetBytes(TargetBytesPtr<MMPolicy>::Make(aMMPolicy,
         reinterpret_cast<const void*>(aFunc)))
     , mOffset(0)
   {
   }
 
   ReadOnlyTargetFunction(ReadOnlyTargetFunction&& aOther)
@@ -809,27 +817,27 @@ private:
 
   template <typename T>
   struct ChasePointerHelper<T*>
   {
     template <typename MMPolicy>
     static auto Result(const MMPolicy& aPolicy, T* aValue)
     {
       ReadOnlyTargetFunction<MMPolicy> ptr(aPolicy, aValue);
-      return ptr.ChasePointer<T>();
+      return ptr.template ChasePointer<T>();
     }
   };
 
 public:
   // Keep chasing pointers until T is not a pointer type anymore
   template <typename T>
   auto ChasePointer()
   {
     mTargetBytes->EnsureLimit(mOffset + sizeof(T));
-    const typename RemoveCV<T>::Type result = *reinterpret_cast<const RemoveCV<T>::Type*>(mTargetBytes->GetLocalBytes() + mOffset);
+    const typename RemoveCV<T>::Type result = *reinterpret_cast<const typename RemoveCV<T>::Type*>(mTargetBytes->GetLocalBytes() + mOffset);
     return ChasePointerHelper<typename RemoveCV<T>::Type>::Result(mTargetBytes->GetMMPolicy(), result);
   }
 
   uintptr_t ChasePointerFromDisp()
   {
     uintptr_t ptrFromDisp = ReadDisp32AsAbsolute();
     ReadOnlyTargetFunction<MMPolicy> ptr(mTargetBytes->GetMMPolicy(),
                                          reinterpret_cast<const void*>(ptrFromDisp));
diff --git a/mozglue/misc/interceptor/VMSharingPolicies.h b/mozglue/misc/interceptor/VMSharingPolicies.h
--- a/mozglue/misc/interceptor/VMSharingPolicies.h
+++ b/mozglue/misc/interceptor/VMSharingPolicies.h
@@ -101,16 +101,17 @@ public:
     static const bool isAlloc = []() -> bool {
       DWORD flags = 0;
 #if defined(RELEASE_OR_BETA)
       flags |= CRITICAL_SECTION_NO_DEBUG_INFO;
 #endif // defined(RELEASE_OR_BETA)
       ::InitializeCriticalSectionEx(&sCS, 4000, flags);
       return true;
     }();
+    Unused << isAlloc;
   }
 
   explicit operator bool() const
   {
     AutoCriticalSection lock(&sCS);
     return !!sUniqueVM;
   }
 
diff --git a/mozglue/misc/nsWindowsDllInterceptor.h b/mozglue/misc/nsWindowsDllInterceptor.h
--- a/mozglue/misc/nsWindowsDllInterceptor.h
+++ b/mozglue/misc/nsWindowsDllInterceptor.h
@@ -163,21 +163,21 @@ public:
   FuncHook(FuncHook&&) = delete;
   FuncHook& operator=(const FuncHook&) = delete;
   FuncHook& operator=(FuncHook&& aOther) = delete;
 
 private:
   struct MOZ_RAII InitOnceContext final
   {
     InitOnceContext(ThisType* aHook, InterceptorT* aInterceptor,
-                    const char* aName, void* aHookDest, bool aForceDetour)
+                    const char* aName, FuncPtrT aHookDest, bool aForceDetour)
       : mHook(aHook)
       , mInterceptor(aInterceptor)
       , mName(aName)
-      , mHookDest(aHookDest)
+      , mHookDest(reinterpret_cast<void*>(aHookDest))
       , mForceDetour(aForceDetour)
     {
     }
 
     ThisType*     mHook;
     InterceptorT* mInterceptor;
     const char*   mName;
     void*         mHookDest;
