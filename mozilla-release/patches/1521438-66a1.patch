# HG changeset patch
# User James Teh <jteh@mozilla.com>
# Date 1548055261 0
# Node ID 311d09aeb1ab0e1ee583123634a4b28975172969
# Parent  792c5419ed8c367631812c722ad91f25dac71db2
Bug 1521438: Correct IAccessible::get_accSelection implementation for no selection and single selection. r=MarcoZ

Previously, we were always returning VT_UNKNOWN and an IEnumVARIANT.
According to the IAccessible documentation, this should only be done for multiple selection.
Change this to correctly return VT_EMPTY for no selection and VT_DISPATCH (with an accessible) for single selection.

Differential Revision: https://phabricator.services.mozilla.com/D17091

diff --git a/accessible/windows/msaa/AccessibleWrap.cpp b/accessible/windows/msaa/AccessibleWrap.cpp
--- a/accessible/windows/msaa/AccessibleWrap.cpp
+++ b/accessible/windows/msaa/AccessibleWrap.cpp
@@ -798,25 +798,35 @@ AccessibleWrap::get_accSelection(VARIANT
     return E_INVALIDARG;
 
   VariantInit(pvarChildren);
   pvarChildren->vt = VT_EMPTY;
 
   if (IsDefunct())
     return CO_E_OBJNOTCONNECTED;
 
-  if (IsSelect()) {
-    AutoTArray<Accessible*, 10> selectedItems;
-    SelectedItems(&selectedItems);
+  if (!IsSelect()) {
+    return S_OK;
+  }
 
-    // 1) Create and initialize the enumeration
-    RefPtr<AccessibleEnumerator> pEnum = new AccessibleEnumerator(selectedItems);
-    pvarChildren->vt = VT_UNKNOWN;    // this must be VT_UNKNOWN for an IEnumVARIANT
+  AutoTArray<Accessible*, 10> selectedItems;
+  SelectedItems(&selectedItems);
+  uint32_t count = selectedItems.Length();
+  if (count == 1) {
+    pvarChildren->vt = VT_DISPATCH;
+    pvarChildren->pdispVal = NativeAccessible(selectedItems[0]);
+  } else if (count > 1) {
+    RefPtr<AccessibleEnumerator> pEnum =
+        new AccessibleEnumerator(selectedItems);
+    pvarChildren->vt =
+        VT_UNKNOWN;  // this must be VT_UNKNOWN for an IEnumVARIANT
     NS_ADDREF(pvarChildren->punkVal = pEnum);
   }
+  // If count == 0, vt is already VT_EMPTY, so there's nothing else to do.
+
   return S_OK;
 }
 
 STDMETHODIMP
 AccessibleWrap::get_accDefaultAction(
       /* [optional][in] */ VARIANT varChild,
       /* [retval][out] */ BSTR __RPC_FAR *pszDefaultAction)
 {
