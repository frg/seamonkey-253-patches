# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1518453170 18000
# Node ID cacc7bcb3220254672b6261d6ff08d8cb48d42f4
# Parent  e308ef6051c80a6d302415cbad2d86f64156d2fd
Bug 1436058 - Update for log 0.4 bump in WR PR 2385. r=jrmuizel

MozReview-Commit-ID: Cxkecg2cTDW

diff --git a/gfx/webrender_bindings/WebRenderAPI.cpp b/gfx/webrender_bindings/WebRenderAPI.cpp
--- a/gfx/webrender_bindings/WebRenderAPI.cpp
+++ b/gfx/webrender_bindings/WebRenderAPI.cpp
@@ -239,17 +239,17 @@ TransactionBuilder::UpdateScrollPosition
 }
 
 
 /*static*/ void
 WebRenderAPI::InitExternalLogHandler()
 {
   // Redirect the webrender's log to gecko's log system.
   // The current log level is "error".
-  mozilla::wr::wr_init_external_log_handler(wr::LogLevelFilter::Error);
+  mozilla::wr::wr_init_external_log_handler(wr::WrLogLevelFilter::Error);
 }
 
 /*static*/ void
 WebRenderAPI::ShutdownExternalLogHandler()
 {
   mozilla::wr::wr_shutdown_external_log_handler();
 }
 
diff --git a/gfx/webrender_bindings/src/bindings.rs b/gfx/webrender_bindings/src/bindings.rs
--- a/gfx/webrender_bindings/src/bindings.rs
+++ b/gfx/webrender_bindings/src/bindings.rs
@@ -13,17 +13,17 @@ use webrender::{ExternalImage, ExternalI
 use webrender::DebugFlags;
 use webrender::{ApiRecordingReceiver, BinaryRecorder};
 use webrender::{ProgramCache, UploadMethod, VertexUsageHint};
 use thread_profiler::register_thread_with_profiler;
 use moz2d_renderer::Moz2dImageRenderer;
 use app_units::Au;
 use rayon;
 use euclid::SideOffsets2D;
-use log::{set_logger, shutdown_logger, LogLevelFilter, Log, LogLevel, LogMetadata, LogRecord};
+use log;
 
 #[cfg(target_os = "windows")]
 use dwrote::{FontDescriptor, FontWeight, FontStretch, FontStyle};
 
 #[cfg(target_os = "macos")]
 use core_foundation::string::CFString;
 #[cfg(target_os = "macos")]
 use core_graphics::font::CGFont;
@@ -71,17 +71,17 @@ type WrPipelineId = PipelineId;
 type WrImageKey = ImageKey;
 /// cbindgen:field-names=[mNamespace, mHandle]
 pub type WrFontKey = FontKey;
 /// cbindgen:field-names=[mNamespace, mHandle]
 type WrFontInstanceKey = FontInstanceKey;
 /// cbindgen:field-names=[mNamespace, mHandle]
 type WrYuvColorSpace = YuvColorSpace;
 /// cbindgen:field-names=[mNamespace, mHandle]
-type WrLogLevelFilter = LogLevelFilter;
+type WrLogLevelFilter = log::LevelFilter;
 
 fn make_slice<'a, T>(ptr: *const T, len: usize) -> &'a [T] {
     if ptr.is_null() {
         &[]
     } else {
         unsafe { slice::from_raw_parts(ptr, len) }
     }
 }
@@ -2156,60 +2156,62 @@ extern "C" {
 type ExternalMessageHandler = unsafe extern "C" fn(msg: *const c_char);
 
 struct WrExternalLogHandler {
     error_msg: ExternalMessageHandler,
     warn_msg: ExternalMessageHandler,
     info_msg: ExternalMessageHandler,
     debug_msg: ExternalMessageHandler,
     trace_msg: ExternalMessageHandler,
-    log_level: LogLevel,
+    log_level: log::Level,
 }
 
 impl WrExternalLogHandler {
-    fn new(log_level: LogLevel) -> WrExternalLogHandler {
+    fn new(log_level: log::Level) -> WrExternalLogHandler {
         WrExternalLogHandler {
             error_msg: gfx_critical_note,
             warn_msg: gfx_critical_note,
             info_msg: gecko_printf_stderr_output,
             debug_msg: gecko_printf_stderr_output,
             trace_msg: gecko_printf_stderr_output,
             log_level: log_level,
         }
     }
 }
 
-impl Log for WrExternalLogHandler {
-    fn enabled(&self, metadata : &LogMetadata) -> bool {
+impl log::Log for WrExternalLogHandler {
+    fn enabled(&self, metadata : &log::Metadata) -> bool {
         metadata.level() <= self.log_level
     }
 
-    fn log(&self, record: &LogRecord) {
+    fn log(&self, record: &log::Record) {
         if self.enabled(record.metadata()) {
             // For file path and line, please check the record.location().
             let msg = CString::new(format!("WR: {}",
                                            record.args())).unwrap();
             unsafe {
                 match record.level() {
-                    LogLevel::Error => (self.error_msg)(msg.as_ptr()),
-                    LogLevel::Warn => (self.warn_msg)(msg.as_ptr()),
-                    LogLevel::Info => (self.info_msg)(msg.as_ptr()),
-                    LogLevel::Debug => (self.debug_msg)(msg.as_ptr()),
-                    LogLevel::Trace => (self.trace_msg)(msg.as_ptr()),
+                    log::Level::Error => (self.error_msg)(msg.as_ptr()),
+                    log::Level::Warn => (self.warn_msg)(msg.as_ptr()),
+                    log::Level::Info => (self.info_msg)(msg.as_ptr()),
+                    log::Level::Debug => (self.debug_msg)(msg.as_ptr()),
+                    log::Level::Trace => (self.trace_msg)(msg.as_ptr()),
                 }
             }
         }
     }
+
+    fn flush(&self) {
+    }
 }
 
 #[no_mangle]
 pub extern "C" fn wr_init_external_log_handler(log_filter: WrLogLevelFilter) {
-    let _ = set_logger(|max_log_level| {
-        max_log_level.set(log_filter);
-        Box::new(WrExternalLogHandler::new(log_filter.to_log_level()
-                                                     .unwrap_or(LogLevel::Error)))
-    });
+    log::set_max_level(log_filter);
+    let logger = Box::new(WrExternalLogHandler::new(log_filter
+                                                    .to_level()
+                                                    .unwrap_or(log::Level::Error)));
+    let _ = log::set_logger(unsafe { &*Box::into_raw(logger) });
 }
 
 #[no_mangle]
 pub extern "C" fn wr_shutdown_external_log_handler() {
-    let _ = shutdown_logger();
 }
diff --git a/gfx/webrender_bindings/webrender_ffi_generated.h b/gfx/webrender_bindings/webrender_ffi_generated.h
--- a/gfx/webrender_bindings/webrender_ffi_generated.h
+++ b/gfx/webrender_bindings/webrender_ffi_generated.h
@@ -96,57 +96,57 @@ enum class ImageFormat : uint32_t {
 enum class ImageRendering : uint32_t {
   Auto = 0,
   CrispEdges = 1,
   Pixelated = 2,
 
   Sentinel /* this must be last for serialization purposes. */
 };
 
+// An enum representing the available verbosity level filters of the logger.
+//
+// A `LevelFilter` may be compared directly to a [`Level`]. Use this type
+// to get and set the maximum log level with [`max_level()`] and [`set_max_level`].
+//
+// [`Level`]: enum.Level.html
+// [`max_level()`]: fn.max_level.html
+// [`set_max_level`]: fn.set_max_level.html
+enum class LevelFilter : uintptr_t {
+  // A level lower than all log levels.
+  Off = 0,
+  // Corresponds to the `Error` log level.
+  Error = 1,
+  // Corresponds to the `Warn` log level.
+  Warn = 2,
+  // Corresponds to the `Info` log level.
+  Info = 3,
+  // Corresponds to the `Debug` log level.
+  Debug = 4,
+  // Corresponds to the `Trace` log level.
+  Trace = 5,
+
+  Sentinel /* this must be last for serialization purposes. */
+};
+
 enum class LineOrientation : uint8_t {
   Vertical = 0,
   Horizontal = 1,
 
   Sentinel /* this must be last for serialization purposes. */
 };
 
 enum class LineStyle : uint8_t {
   Solid = 0,
   Dotted = 1,
   Dashed = 2,
   Wavy = 3,
 
   Sentinel /* this must be last for serialization purposes. */
 };
 
-// An enum representing the available verbosity level filters of the logging
-// framework.
-//
-// A `LogLevelFilter` may be compared directly to a [`LogLevel`](enum.LogLevel.html).
-// Use this type to [`get()`](struct.MaxLogLevelFilter.html#method.get) and
-// [`set()`](struct.MaxLogLevelFilter.html#method.set) the
-// [`MaxLogLevelFilter`](struct.MaxLogLevelFilter.html), or to match with the getter
-// [`max_log_level()`](fn.max_log_level.html).
-enum class LogLevelFilter : uintptr_t {
-  // A level lower than all log levels.
-  Off = 0,
-  // Corresponds to the `Error` log level.
-  Error = 1,
-  // Corresponds to the `Warn` log level.
-  Warn = 2,
-  // Corresponds to the `Info` log level.
-  Info = 3,
-  // Corresponds to the `Debug` log level.
-  Debug = 4,
-  // Corresponds to the `Trace` log level.
-  Trace = 5,
-
-  Sentinel /* this must be last for serialization purposes. */
-};
-
 enum class MixBlendMode : uint32_t {
   Normal = 0,
   Multiply = 1,
   Screen = 2,
   Overlay = 3,
   Darken = 4,
   Lighten = 5,
   ColorDodge = 6,
@@ -716,17 +716,17 @@ struct GlyphOptions {
   bool operator==(const GlyphOptions& aOther) const {
     return render_mode == aOther.render_mode &&
            flags == aOther.flags;
   }
 };
 
 using WrYuvColorSpace = YuvColorSpace;
 
-using WrLogLevelFilter = LogLevelFilter;
+using WrLogLevelFilter = LevelFilter;
 
 struct ByteSlice {
   const uint8_t *buffer;
   size_t len;
 
   bool operator==(const ByteSlice& aOther) const {
     return buffer == aOther.buffer &&
            len == aOther.len;
