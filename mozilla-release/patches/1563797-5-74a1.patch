# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1578690250 0
# Node ID c9e597d3ee12f292aff9e65ffd6a41bef7282025
# Parent  9f90b32431d1e23bc80325b1f2d641d3f0579c11
Bug 1563797 - Use 'backports.shutil_which' instead of 'which' in configure r=glandium

This gets rid of the last use of the 'which' module left in the tree. So not
only will this help 'configure' become a little more Python 3 compatible, but
we can now remove 'third_party/python/which'.

Differential Revision: https://phabricator.services.mozilla.com/D37427

diff --git a/build/moz.configure/util.configure b/build/moz.configure/util.configure
--- a/build/moz.configure/util.configure
+++ b/build/moz.configure/util.configure
@@ -157,53 +157,48 @@ def normalize_path():
 
 normalize_path = normalize_path()
 
 
 # Locates the given program using which, or returns the given path if it
 # exists.
 # The `paths` parameter may be passed to search the given paths instead of
 # $PATH.
-@imports(_from='which', _import='which')
-@imports(_from='which', _import='WhichError')
-@imports('itertools')
 @imports('sys')
 @imports(_from='os', _import='pathsep')
 @imports(_from='os', _import='environ')
+@imports(_from='mozfile', _import='which')
 def find_program(file, paths=None):
     # The following snippet comes from `which` itself, with a slight
     # modification to use lowercase extensions, because it's confusing rustup
     # (on top of making results not really appealing to the eye).
 
     # Windows has the concept of a list of extensions (PATHEXT env var).
     if sys.platform.startswith("win"):
         exts = [e.lower()
                 for e in environ.get("PATHEXT", "").split(pathsep)]
         # If '.exe' is not in exts then obviously this is Win9x and
         # or a bogus PATHEXT, then use a reasonable default.
         if '.exe' not in exts:
             exts = ['.com', '.exe', '.bat']
     else:
         exts = None
 
-    try:
-        if is_absolute_or_relative(file):
-            return normalize_path(which(os.path.basename(file),
-                                        [os.path.dirname(file)], exts=exts))
-        if paths:
-            if not isinstance(paths, (list, tuple)):
-                die("Paths provided to find_program must be a list of strings, "
-                    "not %r", paths)
-            paths = list(itertools.chain(
-                *(p.split(pathsep) for p in paths if p)))
-        else:
-            paths = environ['PATH'].split(pathsep)
-        return normalize_path(which(file, path=paths, exts=exts))
-    except WhichError:
-        return None
+    if is_absolute_or_relative(file):
+        path = which(os.path.basename(file), path=os.path.dirname(file), exts=exts)
+        return normalize_path(path) if path else None
+
+    if paths:
+        if not isinstance(paths, (list, tuple)):
+            die("Paths provided to find_program must be a list of strings, "
+                "not %r", paths)
+        paths = pathsep.join(paths)
+
+    path = which(file, path=paths, exts=exts)
+    return normalize_path(path) if path else None
 
 
 @imports('os')
 @imports(_from='mozbuild.configure.util', _import='LineIO')
 @imports(_from='tempfile', _import='mkstemp')
 def try_invoke_compiler(compiler, language, source, flags=None, onerror=None):
     flags = flags or []
 
diff --git a/python/mozbuild/mozbuild/test/configure/common.py b/python/mozbuild/mozbuild/test/configure/common.py
--- a/python/mozbuild/mozbuild/test/configure/common.py
+++ b/python/mozbuild/mozbuild/test/configure/common.py
@@ -6,23 +6,22 @@ from __future__ import absolute_import, 
 
 import copy
 import errno
 import os
 import subprocess
 import sys
 import tempfile
 import unittest
+from StringIO import StringIO
 
 from mozbuild.configure import ConfigureSandbox
 from mozbuild.util import ReadOnlyNamespace
 from mozpack import path as mozpath
-
-from StringIO import StringIO
-from which import WhichError
+from six import string_types
 
 from buildconfig import (
     topobjdir,
     topsrcdir,
 )
 
 
 def fake_short_path(path):
@@ -106,23 +105,22 @@ class ConfigureTestSandbox(ConfigureSand
 
         super(ConfigureTestSandbox, self).__init__(config, environ, *args,
                                                    **kwargs)
 
     def _get_one_import(self, what):
         if what in self.modules:
             return self.modules[what]
 
-        if what == 'which.which':
+        if what == 'mozfile.which':
             return self.which
 
-        if what == 'which':
+        if what == 'mozfile':
             return ReadOnlyNamespace(
                 which=self.which,
-                WhichError=WhichError,
             )
 
         if what == 'subprocess.Popen':
             return self.Popen
 
         if what == 'subprocess':
             return ReadOnlyNamespace(
                 CalledProcessError=subprocess.CalledProcessError,
@@ -173,28 +171,30 @@ class ConfigureTestSandbox(ConfigureSand
                 self.value = ''
 
         return Buffer()
 
     def GetShortPathNameW(self, path_in, path_out, length):
         path_out.value = fake_short_path(path_in)
         return length
 
-    def which(self, command, path=None, exts=None):
+    def which(self, command, mode=None, path=None, exts=None):
+        if isinstance(path, string_types):
+            path = path.split(os.pathsep)
+
         for parent in (path or self._search_path):
             c = mozpath.abspath(mozpath.join(parent, command))
             for candidate in (c, ensure_exe_extension(c)):
                 if self.imported_os.path.exists(candidate):
                     return candidate
-        raise WhichError()
+        return None
 
     def Popen(self, args, stdin=None, stdout=None, stderr=None, **kargs):
-        try:
-            program = self.which(args[0])
-        except WhichError:
+        program = self.which(args[0])
+        if not program:
             raise OSError(errno.ENOENT, 'File not found')
 
         func = self._subprocess_paths.get(program)
         retcode, stdout, stderr = func(stdin, args[1:])
 
         class Process(object):
             def communicate(self, stdin=None):
                 return stdout, stderr
