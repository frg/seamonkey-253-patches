# HG changeset patch
# User Mike Hommey <mh@glandium.org>
# Date 1603265438 0
#      Wed Oct 21 07:30:38 2020 +0000
# Node ID f165b048285c4d043f0d8dffd33e2eca1614a029
# Parent  a5a35a205a44328aaeaf42bb493e7173fbfe0ae2
Bug 1495733 - Fix elfhack for files > 2GiB and < 4GiB. r=gsvelto

This only solves the easy half of the problem outlined in the bug,
leaving the other half for later.

iostream::tellg() actually returns streampos, which is able to support
files larger than 4GiB with libstdc++, but converting to an int
obviously truncated that, as well as transformed values between 2GiB and
4GiB into invalid negative numbers.

iostream::seekg() also takes a streampos, so storing the streampos as-is
is enough to address the problem with tellg()/seekg() sequences.

The other half of the problem involves elfhack converting 64-bits ELF
headers to 32-bits headers internally, which requires deeper changes.

This change however, is enough to support files up to 4GiB, which is
already a good first step.

Differential Revision: https://phabricator.services.mozilla.com/D94252

diff --git a/build/unix/elfhack/elf.cpp b/build/unix/elfhack/elf.cpp
--- a/build/unix/elfhack/elf.cpp
+++ b/build/unix/elfhack/elf.cpp
@@ -504,17 +504,17 @@ ElfSection::ElfSection(Elf_Shdr& s, std:
   if ((file == nullptr) || (shdr.sh_type == SHT_NULL) ||
       (shdr.sh_type == SHT_NOBITS))
     data = nullptr;
   else {
     data = static_cast<char*>(malloc(shdr.sh_size));
     if (!data) {
       throw std::runtime_error("Could not malloc ElfSection data");
     }
-    int pos = file->tellg();
+    auto pos = file->tellg();
     file->seekg(shdr.sh_offset);
     file->read(data, shdr.sh_size);
     file->seekg(pos);
   }
   if (shdr.sh_name == 0)
     name = nullptr;
   else {
     ElfStrtab_Section* strtab = (ElfStrtab_Section*)parent->getSection(-1);
@@ -717,17 +717,17 @@ bool ElfDynamic_Section::setValueForType
   dyns[i].tag = tag;
   dyns[i].value = val;
   return true;
 }
 
 ElfDynamic_Section::ElfDynamic_Section(Elf_Shdr& s, std::ifstream* file,
                                        Elf* parent)
     : ElfSection(s, file, parent) {
-  int pos = file->tellg();
+  auto pos = file->tellg();
   dyns.resize(s.sh_size / s.sh_entsize);
   file->seekg(shdr.sh_offset);
   // Here we assume tags refer to only one section (e.g. DT_RELSZ accounts
   // for .rel.dyn size)
   for (unsigned int i = 0; i < s.sh_size / s.sh_entsize; i++) {
     Elf_Dyn dyn(*file, parent->getClass(), parent->getData());
     dyns[i].tag = dyn.d_tag;
     switch (dyn.d_tag) {
@@ -818,17 +818,17 @@ void ElfDynamic_Section::serialize(std::
     dyn.d_un.d_val = (dyns[i].value != nullptr) ? dyns[i].value->getValue() : 0;
     dyn.serialize(file, ei_class, ei_data);
   }
 }
 
 ElfSymtab_Section::ElfSymtab_Section(Elf_Shdr& s, std::ifstream* file,
                                      Elf* parent)
     : ElfSection(s, file, parent) {
-  int pos = file->tellg();
+  auto pos = file->tellg();
   syms.resize(s.sh_size / s.sh_entsize);
   ElfStrtab_Section* strtab = (ElfStrtab_Section*)getLink();
   file->seekg(shdr.sh_offset);
   for (unsigned int i = 0; i < shdr.sh_size / shdr.sh_entsize; i++) {
     Elf_Sym sym(*file, parent->getClass(), parent->getData());
     syms[i].name = strtab->getStr(sym.st_name);
     syms[i].info = sym.st_info;
     syms[i].other = sym.st_other;
diff --git a/build/unix/elfhack/elfxx.h b/build/unix/elfhack/elfxx.h
--- a/build/unix/elfhack/elfxx.h
+++ b/build/unix/elfhack/elfxx.h
@@ -578,17 +578,17 @@ class Elf_Rela : public serializable<Elf
   static const unsigned int d_tag_count = DT_RELACOUNT;
 };
 
 template <class Rel>
 class ElfRel_Section : public ElfSection {
  public:
   ElfRel_Section(Elf_Shdr& s, std::ifstream* file, Elf* parent)
       : ElfSection(s, file, parent) {
-    int pos = file->tellg();
+    auto pos = file->tellg();
     file->seekg(shdr.sh_offset);
     for (unsigned int i = 0; i < s.sh_size / s.sh_entsize; i++) {
       Rel r(*file, parent->getClass(), parent->getData());
       rels.push_back(r);
     }
     file->seekg(pos);
   }
 
