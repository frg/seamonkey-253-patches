# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1519842892 18000
# Node ID 32b33a66cf6b224b155f93115b555eff8bc22988
# Parent  d59b859c27c9ddafe245ea5c50491555bfcdac20
Bug 523950 - Part 4. Expose new surface provider APIs that will help drive animation decoding. r=tnikkel

Note that AnimationSurfaceProvider will override these methods to give a
proper implementation in a later patch in this series. For now, they are
mostly stubbed, using the default implementation from ISurfaceProvider.

They focus on the main operations we perform on an animation:

1) Progressing through the animation, e.g. advancing a frame. If we
don't decode the whole animation up front, we need to know at the
decoder level where we are in the display of the animation.

2) Restarting an animation from the beginning. This is a specialized
case of the above, where we want to skip explicitly advancing through
the remaining frames and instead restart at the beginning. The decoder
may have already discarded the earliest frames and must start redecoding
them.

3) Knowing whether or not the decoder is still active, e.g. can we be
missing frames.

diff --git a/image/FrameAnimator.cpp b/image/FrameAnimator.cpp
--- a/image/FrameAnimator.cpp
+++ b/image/FrameAnimator.cpp
@@ -66,22 +66,17 @@ AnimationState::UpdateStateInternal(Look
     // If mHasBeenDecoded is true then we know the true total frame count and
     // we can use it to determine if we have all the frames now so we know if
     // we are currently fully decoded.
     // If mHasBeenDecoded is false then we'll get another UpdateState call
     // when the decode finishes.
     if (mHasBeenDecoded) {
       Maybe<uint32_t> frameCount = FrameCount();
       MOZ_ASSERT(frameCount.isSome());
-      if (NS_SUCCEEDED(aResult.Surface().Seek(*frameCount - 1)) &&
-          aResult.Surface()->IsFinished()) {
-        mIsCurrentlyDecoded = true;
-      } else {
-        mIsCurrentlyDecoded = false;
-      }
+      mIsCurrentlyDecoded = aResult.Surface().IsFullyDecoded();
     }
   }
 
   gfx::IntRect ret;
 
   if (aAllowInvalidation) {
     // Update the value of mCompositedFrameInvalid.
     if (mIsCurrentlyDecoded || aAnimationFinished) {
@@ -298,18 +293,22 @@ FrameAnimator::AdvanceFrame(AnimationSta
   MOZ_ASSERT(nextFrameIndex < aState.KnownFrameCount());
   RawAccessFrameRef nextFrame = GetRawFrame(aFrames, nextFrameIndex);
 
   // We should always check to see if we have the next frame even if we have
   // previously finished decoding. If we needed to redecode (e.g. due to a draw
   // failure) we would have discarded all the old frames and may not yet have
   // the new ones.
   if (!nextFrame || !nextFrame->IsFinished()) {
-    // Uh oh, the frame we want to show is currently being decoded (partial)
-    // Wait until the next refresh driver tick and try again
+    // Uh oh, the frame we want to show is currently being decoded (partial).
+    // Similar to the above case, we could be blocked by network or decoding,
+    // and so we should advance our current time rather than risk jumping
+    // through the animation. We will wait until the next refresh driver tick
+    // and try again.
+    aState.mCurrentAnimationFrameTime = aTime;
     return ret;
   }
 
   Maybe<FrameTimeout> nextFrameTimeout = GetTimeoutForFrame(aState, aFrames, nextFrameIndex);
   // GetTimeoutForFrame can only return none if frame doesn't exist,
   // but we just got it above.
   MOZ_ASSERT(nextFrameTimeout.isSome());
   if (*nextFrameTimeout == FrameTimeout::Forever()) {
@@ -325,16 +324,17 @@ FrameAnimator::AdvanceFrame(AnimationSta
     if (!DoBlend(aFrames, &ret.mDirtyRect, currentFrameIndex, nextFrameIndex)) {
       // something went wrong, move on to next
       NS_WARNING("FrameAnimator::AdvanceFrame(): Compositing of frame failed");
       nextFrame->SetCompositingFailed(true);
       Maybe<TimeStamp> currentFrameEndTime = GetCurrentImgFrameEndTime(aState, aFrames);
       MOZ_ASSERT(currentFrameEndTime.isSome());
       aState.mCurrentAnimationFrameTime = *currentFrameEndTime;
       aState.mCurrentAnimationFrameIndex = nextFrameIndex;
+      aFrames.Advance(nextFrameIndex);
 
       return ret;
     }
 
     nextFrame->SetCompositingFailed(false);
   }
 
   Maybe<TimeStamp> currentFrameEndTime = GetCurrentImgFrameEndTime(aState, aFrames);
@@ -370,23 +370,43 @@ FrameAnimator::AdvanceFrame(AnimationSta
         MOZ_ASSERT(loops <= CheckedUint64(aState.mLoopRemainingCount).value());
         aState.mLoopRemainingCount -= CheckedInt32(loops).value();
       }
     }
   }
 
   // Set currentAnimationFrameIndex at the last possible moment
   aState.mCurrentAnimationFrameIndex = nextFrameIndex;
+  aFrames.Advance(nextFrameIndex);
 
   // If we're here, we successfully advanced the frame.
   ret.mFrameAdvanced = true;
 
   return ret;
 }
 
+void
+FrameAnimator::ResetAnimation(AnimationState& aState)
+{
+  aState.ResetAnimation();
+
+  // Our surface provider is synchronized to our state, so we need to reset its
+  // state as well, if we still have one.
+  LookupResult result =
+    SurfaceCache::Lookup(ImageKey(mImage),
+                         RasterSurfaceKey(mSize,
+                                          DefaultSurfaceFlags(),
+                                          PlaybackType::eAnimated));
+  if (!result) {
+    return;
+  }
+
+  result.Surface().Reset();
+}
+
 RefreshResult
 FrameAnimator::RequestRefresh(AnimationState& aState,
                               const TimeStamp& aTime,
                               bool aAnimationFinished)
 {
   // By default, an empty RefreshResult.
   RefreshResult ret;
 
diff --git a/image/FrameAnimator.h b/image/FrameAnimator.h
--- a/image/FrameAnimator.h
+++ b/image/FrameAnimator.h
@@ -285,16 +285,22 @@ public:
   }
 
   ~FrameAnimator()
   {
     MOZ_COUNT_DTOR(FrameAnimator);
   }
 
   /**
+   * Call when you need to re-start animating. Ensures we start from the first
+   * frame.
+   */
+  void ResetAnimation(AnimationState& aState);
+
+  /**
    * Re-evaluate what frame we're supposed to be on, and do whatever blending
    * is necessary to get us to that frame.
    *
    * Returns the result of that blending, including whether the current frame
    * changed and what the resulting dirty rectangle is.
    */
   RefreshResult RequestRefresh(AnimationState& aState,
                                const TimeStamp& aTime,
diff --git a/image/ISurfaceProvider.h b/image/ISurfaceProvider.h
--- a/image/ISurfaceProvider.h
+++ b/image/ISurfaceProvider.h
@@ -48,16 +48,22 @@ public:
   const SurfaceKey& GetSurfaceKey() const { return mSurfaceKey; }
 
   /// @return a (potentially lazily computed) drawable reference to a surface.
   virtual DrawableSurface Surface();
 
   /// @return true if DrawableRef() will return a completely decoded surface.
   virtual bool IsFinished() const = 0;
 
+  /// @return true if the underlying decoder is currently fully decoded. For
+  /// animated images, this means that at least every frame has been decoded
+  /// at least once. It does not guarantee that all of the frames are present,
+  /// as the surface provider has the option to discard as it deems necessary.
+  virtual bool IsFullyDecoded() const { return IsFinished(); }
+
   /// @return the number of bytes of memory this ISurfaceProvider is expected to
   /// require. Optimizations may result in lower real memory usage. Trivial
   /// overhead is ignored. Because this value is used in bookkeeping, it's
   /// important that it be constant over the lifetime of this object.
   virtual size_t LogicalSizeInBytes() const = 0;
 
   /// @return the actual number of bytes of memory this ISurfaceProvider is
   /// using. May vary over the lifetime of the ISurfaceProvider. The default
@@ -71,16 +77,19 @@ public:
     if (!ref) {
       return;
     }
 
     ref->AddSizeOfExcludingThis(aMallocSizeOf, aHeapSizeOut,
                                 aNonHeapSizeOut, aExtHandlesOut);
   }
 
+  virtual void Reset() { }
+  virtual void Advance(size_t aFrame) { }
+
   /// @return the availability state of this ISurfaceProvider, which indicates
   /// whether DrawableRef() could successfully return a surface. Should only be
   /// called from SurfaceCache code as it relies on SurfaceCache for
   /// synchronization.
   AvailabilityState& Availability() { return mAvailability; }
   const AvailabilityState& Availability() const { return mAvailability; }
 
 protected:
@@ -185,16 +194,46 @@ public:
       return NS_ERROR_FAILURE;
     }
 
     mDrawableRef = mProvider->DrawableRef(aFrame);
 
     return mDrawableRef ? NS_OK : NS_ERROR_FAILURE;
   }
 
+  void Reset()
+  {
+    if (!mProvider) {
+      MOZ_ASSERT_UNREACHABLE("Trying to reset a static DrawableSurface?");
+      return;
+    }
+
+    mProvider->Reset();
+  }
+
+  void Advance(size_t aFrame)
+  {
+    if (!mProvider) {
+      MOZ_ASSERT_UNREACHABLE("Trying to advance a static DrawableSurface?");
+      return;
+    }
+
+    mProvider->Advance(aFrame);
+  }
+
+  bool IsFullyDecoded() const
+  {
+    if (!mProvider) {
+      MOZ_ASSERT_UNREACHABLE("Trying to check decoding state of a static DrawableSurface?");
+      return false;
+    }
+
+    return mProvider->IsFullyDecoded();
+  }
+
   explicit operator bool() const { return mHaveSurface; }
   imgFrame* operator->() { return DrawableRef().get(); }
 
 private:
   DrawableSurface(const DrawableSurface& aOther) = delete;
   DrawableSurface& operator=(const DrawableSurface& aOther) = delete;
 
   DrawableFrameRef& DrawableRef()
diff --git a/image/RasterImage.cpp b/image/RasterImage.cpp
--- a/image/RasterImage.cpp
+++ b/image/RasterImage.cpp
@@ -864,17 +864,18 @@ RasterImage::ResetAnimation()
 
   mAnimationFinished = false;
 
   if (mAnimating) {
     StopAnimation();
   }
 
   MOZ_ASSERT(mAnimationState, "Should have AnimationState");
-  mAnimationState->ResetAnimation();
+  MOZ_ASSERT(mFrameAnimator, "Should have FrameAnimator");
+  mFrameAnimator->ResetAnimation(*mAnimationState);
 
   NotifyProgress(NoProgress, mAnimationState->FirstFrameRefreshArea());
 
   // Start the animation again. It may not have been running before, if
   // mAnimationFinished was true before entering this function.
   EvaluateAnimation();
 
   return NS_OK;
