# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1516981639 0
# Node ID 625b879e0a93343591140c2ba4cf3a2afc614c1c
# Parent  75b5af7910068a6fe04c61b267d985c80362e49d
Bug 1323743 - patch 1 - Add an API to gfxFontEntry to allow querying the available variation axes in a font face. (No actual implementation yet.) r=dholbert

diff --git a/gfx/thebes/gfxFontEntry.h b/gfx/thebes/gfxFontEntry.h
--- a/gfx/thebes/gfxFontEntry.h
+++ b/gfx/thebes/gfxFontEntry.h
@@ -6,16 +6,17 @@
 #ifndef GFX_FONTENTRY_H
 #define GFX_FONTENTRY_H
 
 #include "gfxTypes.h"
 #include "nsString.h"
 #include "gfxFontConstants.h"
 #include "gfxFontFeatures.h"
 #include "gfxFontUtils.h"
+#include "gfxFontVariations.h"
 #include "nsTArray.h"
 #include "nsTHashtable.h"
 #include "mozilla/HashFunctions.h"
 #include "mozilla/MemoryReporting.h"
 #include "MainThreadUtils.h"
 #include "nsUnicodeScriptCodes.h"
 #include "nsDataHashtable.h"
 #include "harfbuzz/hb.h"
@@ -339,16 +340,26 @@ public:
         uint32_t         rangeStart;
         uint32_t         rangeEnd;
         hb_tag_t         tags[3]; // one or two OpenType script tags to check,
                                   // plus a NULL terminator
     };
 
     bool SupportsScriptInGSUB(const hb_tag_t* aScriptTags);
 
+    // For variation font support, which is not yet implemented on all
+    // platforms; default implementations assume it is not present.
+    virtual bool HasVariations()
+    {
+        return false;
+    }
+    virtual void GetVariationAxes(nsTArray<gfxFontVariationAxis>& aVariationAxes)
+    {
+    }
+
     nsString         mName;
     nsString         mFamilyName;
 
     uint8_t          mStyle       : 2; // italic/oblique
     bool             mFixedPitch  : 1;
     bool             mIsBadUnderlineFont : 1;
     bool             mIsUserFontContainer : 1; // userfont entry
     bool             mIsDataUserFont : 1;      // platform font entry (data)
diff --git a/gfx/thebes/gfxFontVariations.h b/gfx/thebes/gfxFontVariations.h
--- a/gfx/thebes/gfxFontVariations.h
+++ b/gfx/thebes/gfxFontVariations.h
@@ -6,9 +6,17 @@
 
 #ifndef GFX_FONT_VARIATIONS_H
 #define GFX_FONT_VARIATIONS_H
 
 #include "mozilla/gfx/FontVariation.h"
 
 typedef mozilla::gfx::FontVariation gfxFontVariation;
 
+struct gfxFontVariationAxis {
+    uint32_t mTag;
+    nsString mName;
+    float    mMinValue;
+    float    mMaxValue;
+    float    mDefaultValue;
+};
+
 #endif
diff --git a/gfx/thebes/gfxMacPlatformFontList.h b/gfx/thebes/gfxMacPlatformFontList.h
--- a/gfx/thebes/gfxMacPlatformFontList.h
+++ b/gfx/thebes/gfxMacPlatformFontList.h
@@ -56,17 +56,17 @@ public:
 
     void AddSizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf,
                                 FontListSizes* aSizes) const override;
 
     nsresult ReadCMAP(FontInfoData *aFontInfoData = nullptr) override;
 
     bool RequiresAATLayout() const { return mRequiresAAT; }
 
-    bool HasVariations();
+    bool HasVariations() override;
     bool IsCFF();
 
     // Return true if the font has a 'trak' table (and we can successfully
     // interpret it), otherwise false. This will load and cache the table
     // the first time it is called.
     bool HasTrackingTable();
 
     bool SupportsOpenTypeFeature(Script aScript, uint32_t aFeatureTag) override;

