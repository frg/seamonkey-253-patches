# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1527498568 -3600
# Node ID 7a238a8591587be826418452275f7b2e7a6364f5
# Parent  214e3bd35a63eedf11605248467359e3fd57de86
Bug 1463884 - patch 3 - Make gfxFcPlatformFontList::TryLangForGroup safe for off-main-thread use when called from GetSystemFontList. r=emilio

diff --git a/gfx/thebes/gfxFcPlatformFontList.cpp b/gfx/thebes/gfxFcPlatformFontList.cpp
--- a/gfx/thebes/gfxFcPlatformFontList.cpp
+++ b/gfx/thebes/gfxFcPlatformFontList.cpp
@@ -1563,17 +1563,18 @@ GetSystemFontList(nsTArray<nsString>& aL
     nsAutoRef<FcObjectSet> os(FcObjectSetBuild(FC_FAMILY, nullptr));
     if (!os) {
         return;
     }
 
     // add the lang to the pattern
     nsAutoCString fcLang;
     gfxFcPlatformFontList* pfl = gfxFcPlatformFontList::PlatformFontList();
-    pfl->GetSampleLangForGroup(aLangGroup, fcLang);
+    pfl->GetSampleLangForGroup(aLangGroup, fcLang,
+                               /*aForFontEnumerationThread*/ true);
     if (!fcLang.IsEmpty()) {
         FcPatternAddString(pat, FC_LANG, ToFcChar8Ptr(fcLang.get()));
     }
 
     nsAutoRef<FcFontSet> fs(FcFontList(nullptr, pat, os));
     if (!fs) {
         return;
     }
@@ -2196,17 +2197,18 @@ const MozLangGroupData MozLangGroups[] =
     { nsGkAtoms::x_telu,         "te" },
     { nsGkAtoms::x_tibt,         "bo" },
     { nsGkAtoms::Unicode,        0    }
 };
 
 bool
 gfxFcPlatformFontList::TryLangForGroup(const nsACString& aOSLang,
                                        nsIAtom* aLangGroup,
-                                       nsACString& aFcLang)
+                                       nsACString& aFcLang,
+                                       bool aForFontEnumerationThread)
 {
     // Truncate at '.' or '@' from aOSLang, and convert '_' to '-'.
     // aOSLang is in the form "language[_territory][.codeset][@modifier]".
     // fontconfig takes languages in the form "language-territory".
     // nsLanguageAtomService takes languages in the form language-subtag,
     // where subtag may be a territory.  fontconfig and nsLanguageAtomService
     // handle case-conversion for us.
     const char *pos, *end;
@@ -2223,23 +2225,36 @@ gfxFcPlatformFontList::TryLangForGroup(c
                 aFcLang.Append('-');
                 break;
             default:
                 aFcLang.Append(*pos);
         }
         ++pos;
     }
 
-    nsIAtom *atom = mLangService->LookupLanguage(aFcLang);
-    return atom == aLangGroup;
+    if (!aForFontEnumerationThread) {
+        nsIAtom *atom = mLangService->LookupLanguage(aFcLang);
+        return atom == aLangGroup;
+    }
+
+    // If we were called by the font enumeration thread, we can't use
+    // mLangService->LookupLanguage because it is not thread-safe.
+    // Use GetUncachedLanguageGroup to avoid unsafe access to the lang-group
+    // mapping cache hashtable.
+    nsAutoCString lowered(aFcLang);
+    ToLowerCase(lowered);
+    nsCOMPtr<nsIAtom> lang = NS_Atomize(lowered);
+    nsCOMPtr<nsIAtom> group = mLangService->GetUncachedLanguageGroup(lang);
+    return group.get() == aLangGroup;
 }
 
 void
 gfxFcPlatformFontList::GetSampleLangForGroup(nsIAtom* aLanguage,
-                                             nsACString& aLangStr)
+                                             nsACString& aLangStr,
+                                             bool aForFontEnumerationThread)
 {
     aLangStr.Truncate();
     if (!aLanguage) {
         return;
     }
 
     // set up lang string
     const MozLangGroupData *mozLangGroup = nullptr;
@@ -2266,31 +2281,33 @@ gfxFcPlatformFontList::GetSampleLangForG
     const char *languages = getenv("LANGUAGE");
     if (languages) {
         const char separator = ':';
 
         for (const char *pos = languages; true; ++pos) {
             if (*pos == '\0' || *pos == separator) {
                 if (languages < pos &&
                     TryLangForGroup(Substring(languages, pos),
-                                    aLanguage, aLangStr)) {
+                                    aLanguage, aLangStr,
+                                    aForFontEnumerationThread)) {
                     return;
                 }
 
                 if (*pos == '\0') {
                     break;
                 }
 
                 languages = pos + 1;
             }
         }
     }
     const char *ctype = setlocale(LC_CTYPE, nullptr);
     if (ctype &&
-        TryLangForGroup(nsDependentCString(ctype), aLanguage, aLangStr)) {
+        TryLangForGroup(nsDependentCString(ctype), aLanguage, aLangStr,
+                        aForFontEnumerationThread)) {
         return;
     }
 
     if (mozLangGroup->defaultLang) {
         aLangStr.Assign(mozLangGroup->defaultLang);
     } else {
         aLangStr.Truncate();
     }
diff --git a/gfx/thebes/gfxFcPlatformFontList.h b/gfx/thebes/gfxFcPlatformFontList.h
--- a/gfx/thebes/gfxFcPlatformFontList.h
+++ b/gfx/thebes/gfxFcPlatformFontList.h
@@ -300,17 +300,21 @@ public:
     void ClearLangGroupPrefFonts() override;
 
     // clear out cached generic-lang ==> family-list mappings
     void ClearGenericMappings() {
         mGenericMappings.Clear();
     }
 
     // map lang group ==> lang string
-    void GetSampleLangForGroup(nsIAtom* aLanguage, nsACString& aLangStr);
+    // When aForFontEnumerationThread is true, this method will avoid using
+    // LanguageService::LookupLanguage, because it is not safe for off-main-
+    // thread use (except by stylo traversal, which does the necessary locking)
+    void GetSampleLangForGroup(nsIAtom* aLanguage, nsACString& aLangStr,
+                               bool aForFontEnumerationThread = false);
 
     static FT_Library GetFTLibrary();
 
 protected:
     virtual ~gfxFcPlatformFontList();
 
 #ifdef MOZ_CONTENT_SANDBOX
     typedef mozilla::SandboxBroker::Policy SandboxPolicy;
@@ -342,17 +346,17 @@ protected:
 
     virtual gfxFontFamily*
     GetDefaultFontForPlatform(const gfxFontStyle* aStyle) override;
 
     gfxFontFamily* CreateFontFamily(const nsAString& aName) const override;
 
     // helper method for finding an appropriate lang string
     bool TryLangForGroup(const nsACString& aOSLang, nsIAtom* aLangGroup,
-                         nsACString& aLang);
+                         nsACString& aLang, bool aForFontEnumerationThread);
 
 #ifdef MOZ_BUNDLED_FONTS
     void ActivateBundledFonts();
     nsCString mBundledFontsPath;
     bool mBundledFontsInitialized;
 #endif
 
     // to avoid enumerating all fonts, maintain a mapping of local font
