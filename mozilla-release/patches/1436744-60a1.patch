# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1518465742 -3600
# Node ID d33f2acef1ed12be6bce66e8a615fa17b1d9be9b
# Parent  8b27eed036213deabe9b3b27215fe16018d122d3
Bug 1436744 - Get rid of WorkerCheckAPIExposureOnMainThreadRunnable, r=catalinb

diff --git a/dom/base/DOMPrefsInternal.h b/dom/base/DOMPrefsInternal.h
--- a/dom/base/DOMPrefsInternal.h
+++ b/dom/base/DOMPrefsInternal.h
@@ -34,23 +34,25 @@ DOM_PREF(PushEnabled, "dom.push.enabled"
 DOM_PREF(StreamsEnabled, "dom.streams.enabled")
 DOM_PREF(RequestContextEnabled, "dom.requestcontext.enabled")
 DOM_PREF(OffscreenCanvasEnabled, "gfx.offscreencanvas.enabled")
 DOM_PREF(WebkitBlinkDirectoryPickerEnabled, "dom.webkitBlink.dirPicker.enabled")
 DOM_PREF(NetworkInformationEnabled, "dom.netinfo.enabled")
 DOM_PREF(FetchObserverEnabled, "dom.fetchObserver.enabled")
 DOM_PREF(ResistFingerprintingEnabled, "privacy.resistFingerprinting")
 DOM_PREF(DevToolsEnabled, "devtools.enabled")
+DOM_PREF(PerformanceObserverEnabled, "dom.enable_performance_observer")
 
 DOM_WEBIDL_PREF(ImageBitmapExtensionsEnabled)
 DOM_WEBIDL_PREF(DOMCachesEnabled)
 DOM_WEBIDL_PREF(NotificationEnabledInServiceWorkers)
 DOM_WEBIDL_PREF(NotificationRIEnabled)
 DOM_WEBIDL_PREF(ServiceWorkersEnabled)
 DOM_WEBIDL_PREF(StorageManagerEnabled)
 DOM_WEBIDL_PREF(PromiseRejectionEventsEnabled)
 DOM_WEBIDL_PREF(PushEnabled)
 DOM_WEBIDL_PREF(StreamsEnabled)
 DOM_WEBIDL_PREF(RequestContextEnabled)
 DOM_WEBIDL_PREF(OffscreenCanvasEnabled)
 DOM_WEBIDL_PREF(WebkitBlinkDirectoryPickerEnabled)
 DOM_WEBIDL_PREF(NetworkInformationEnabled)
 DOM_WEBIDL_PREF(FetchObserverEnabled)
+DOM_WEBIDL_PREF(PerformanceObserverEnabled)
diff --git a/dom/performance/Performance.cpp b/dom/performance/Performance.cpp
--- a/dom/performance/Performance.cpp
+++ b/dom/performance/Performance.cpp
@@ -31,48 +31,16 @@
 #include "ProfilerMarkerPayload.h"
 #endif
 
 #define PERFLOG(msg, ...) printf_stderr(msg, ##__VA_ARGS__)
 
 namespace mozilla {
 namespace dom {
 
-namespace {
-
-class PrefEnabledRunnable final
-  : public WorkerCheckAPIExposureOnMainThreadRunnable
-{
-public:
-  PrefEnabledRunnable(WorkerPrivate* aWorkerPrivate,
-                      const nsCString& aPrefName)
-    : WorkerCheckAPIExposureOnMainThreadRunnable(aWorkerPrivate)
-    , mEnabled(false)
-    , mPrefName(aPrefName)
-  { }
-
-  bool MainThreadRun() override
-  {
-    MOZ_ASSERT(NS_IsMainThread());
-    mEnabled = Preferences::GetBool(mPrefName.get(), false);
-    return true;
-  }
-
-  bool IsEnabled() const
-  {
-    return mEnabled;
-  }
-
-private:
-  bool mEnabled;
-  nsCString mPrefName;
-};
-
-} // anonymous namespace
-
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(Performance)
 NS_INTERFACE_MAP_END_INHERITING(DOMEventTargetHelper)
 
 NS_IMPL_CYCLE_COLLECTION_INHERITED(Performance,
                                    DOMEventTargetHelper,
                                    mUserEntries,
                                    mResourceEntries);
 
@@ -534,34 +502,16 @@ Performance::QueueEntry(PerformanceEntry
                                            PerformanceObserver,
                                            QueueEntry, (aEntry));
 
   if (!mPendingNotificationObserversTask) {
     RunNotificationObserversTask();
   }
 }
 
-/* static */ bool
-Performance::IsObserverEnabled(JSContext* aCx, JSObject* aGlobal)
-{
-  if (NS_IsMainThread()) {
-    return Preferences::GetBool("dom.enable_performance_observer", false);
-  }
-
-  WorkerPrivate* workerPrivate = GetCurrentThreadWorkerPrivate();
-  MOZ_ASSERT(workerPrivate);
-  workerPrivate->AssertIsOnWorkerThread();
-
-  RefPtr<PrefEnabledRunnable> runnable =
-    new PrefEnabledRunnable(workerPrivate,
-                            NS_LITERAL_CSTRING("dom.enable_performance_observer"));
-
-  return runnable->Dispatch() && runnable->IsEnabled();
-}
-
 void
 Performance::MemoryPressure()
 {
   mUserEntries.Clear();
 }
 
 size_t
 Performance::SizeOfUserEntries(mozilla::MallocSizeOf aMallocSizeOf) const
diff --git a/dom/performance/Performance.h b/dom/performance/Performance.h
--- a/dom/performance/Performance.h
+++ b/dom/performance/Performance.h
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_Performance_h
 #define mozilla_dom_Performance_h
 
 #include "mozilla/Attributes.h"
 #include "mozilla/DOMEventTargetHelper.h"
+#include "mozilla/dom/DOMPrefs.h"
 #include "nsCOMPtr.h"
 #include "nsDOMNavigationTiming.h"
 
 class nsITimedChannel;
 
 namespace mozilla {
 
 class ErrorResult;
diff --git a/dom/performance/PerformanceObserverEntryList.h b/dom/performance/PerformanceObserverEntryList.h
--- a/dom/performance/PerformanceObserverEntryList.h
+++ b/dom/performance/PerformanceObserverEntryList.h
@@ -6,16 +6,17 @@
 
 #ifndef mozilla_dom_PerformanceObserverEntryList_h__
 #define mozilla_dom_PerformanceObserverEntryList_h__
 
 #include "nsCOMPtr.h"
 #include "nsISupports.h"
 #include "nsTArray.h"
 #include "nsWrapperCache.h"
+#include "mozilla/dom/DOMPrefs.h"
 #include "mozilla/dom/PerformanceEntryBinding.h"
 
 namespace mozilla {
 namespace dom {
 
 struct PerformanceEntryFilterOptions;
 class PerformanceEntry;
 template<typename T> class Optional;
diff --git a/dom/webidl/PerformanceObserver.webidl b/dom/webidl/PerformanceObserver.webidl
--- a/dom/webidl/PerformanceObserver.webidl
+++ b/dom/webidl/PerformanceObserver.webidl
@@ -10,17 +10,17 @@
 dictionary PerformanceObserverInit {
   required sequence<DOMString> entryTypes;
   boolean buffered = false;
 };
 
 callback PerformanceObserverCallback = void (PerformanceObserverEntryList entries,
                                              PerformanceObserver observer);
 
-[Func="Performance::IsObserverEnabled",
+[Func="mozilla::dom::DOMPrefs::PerformanceObserverEnabled",
  Constructor(PerformanceObserverCallback callback),
  Exposed=(Window,Worker)]
 interface PerformanceObserver {
   [Throws]
     void                 observe(PerformanceObserverInit options);
     void                 disconnect();
     PerformanceEntryList takeRecords();
 };
diff --git a/dom/webidl/PerformanceObserverEntryList.webidl b/dom/webidl/PerformanceObserverEntryList.webidl
--- a/dom/webidl/PerformanceObserverEntryList.webidl
+++ b/dom/webidl/PerformanceObserverEntryList.webidl
@@ -9,16 +9,17 @@
 
 // XXX should be moved into Performance.webidl.
 dictionary PerformanceEntryFilterOptions {
   DOMString name;
   DOMString entryType;
   DOMString initiatorType;
 };
 
-[Func="Performance::IsObserverEnabled", Exposed=(Window,Worker)]
+[Func="mozilla::dom::DOMPrefs::PerformanceObserverEnabled",
+ Exposed=(Window,Worker)]
 interface PerformanceObserverEntryList {
   PerformanceEntryList getEntries(optional PerformanceEntryFilterOptions filter);
   PerformanceEntryList getEntriesByType(DOMString entryType);
   PerformanceEntryList getEntriesByName(DOMString name,
                                         optional DOMString entryType);
 };
 
diff --git a/dom/workers/WorkerRunnable.cpp b/dom/workers/WorkerRunnable.cpp
--- a/dom/workers/WorkerRunnable.cpp
+++ b/dom/workers/WorkerRunnable.cpp
@@ -616,34 +616,16 @@ WorkerMainThreadRunnable::Run()
                                        mSyncLoopTarget.forget(),
                                        runResult);
 
   MOZ_ALWAYS_TRUE(response->Dispatch());
 
   return NS_OK;
 }
 
-WorkerCheckAPIExposureOnMainThreadRunnable::WorkerCheckAPIExposureOnMainThreadRunnable(WorkerPrivate* aWorkerPrivate):
-  WorkerMainThreadRunnable(aWorkerPrivate,
-                           NS_LITERAL_CSTRING("WorkerCheckAPIExposureOnMainThread"))
-{}
-
-WorkerCheckAPIExposureOnMainThreadRunnable::~WorkerCheckAPIExposureOnMainThreadRunnable()
-{}
-
-bool
-WorkerCheckAPIExposureOnMainThreadRunnable::Dispatch()
-{
-  ErrorResult rv;
-  WorkerMainThreadRunnable::Dispatch(Terminating, rv);
-  bool ok = !rv.Failed();
-  rv.SuppressException();
-  return ok;
-}
-
 bool
 WorkerSameThreadRunnable::PreDispatch(WorkerPrivate* aWorkerPrivate)
 {
   // We don't call WorkerRunnable::PreDispatch, because we're using
   // WorkerThreadModifyBusyCount for mBehavior, and WorkerRunnable will assert
   // that PreDispatch is on the parent thread in that case.
   aWorkerPrivate->AssertIsOnWorkerThread();
   return true;
diff --git a/dom/workers/WorkerRunnable.h b/dom/workers/WorkerRunnable.h
--- a/dom/workers/WorkerRunnable.h
+++ b/dom/workers/WorkerRunnable.h
@@ -452,37 +452,16 @@ private:
   bool HoldWorker();
   void ReleaseWorker();
 
 protected:
   WorkerPrivate* mWorkerPrivate;
   UniquePtr<WorkerHolder> mWorkerHolder;
 };
 
-// Class for checking API exposure.  This totally violates the "MUST" in the
-// comments on WorkerMainThreadRunnable::Dispatch, because API exposure checks
-// can't throw.  Maybe we should change it so they _could_ throw.  But for now
-// we are bad people and should be ashamed of ourselves.  Let's hope none of
-// them happen while a worker is shutting down.
-//
-// Do NOT copy what this class is doing elsewhere.  Just don't.
-class WorkerCheckAPIExposureOnMainThreadRunnable
-  : public WorkerMainThreadRunnable
-{
-public:
-  explicit
-  WorkerCheckAPIExposureOnMainThreadRunnable(WorkerPrivate* aWorkerPrivate);
-  virtual
-  ~WorkerCheckAPIExposureOnMainThreadRunnable();
-
-  // Returns whether the dispatch succeeded.  If this returns false, the API
-  // should not be exposed.
-  bool Dispatch();
-};
-
 // This runnable is used to stop a sync loop and it's meant to be used on the
 // main-thread only. As sync loops keep the busy count incremented as long as
 // they run this runnable does not modify the busy count
 // in any way.
 class MainThreadStopSyncLoopRunnable : public WorkerSyncRunnable
 {
   bool mResult;
 
