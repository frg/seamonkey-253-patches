# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1559336017 25200
# Node ID 8735151820ebeaf7ee06e9f2ed16868c164d85ab
# Parent  675e5efe2086772992eed663fc03cf1489dbbe6f
Bug 1556073: Also ignore Rust panic frames in crash signature. r=gbrown

Differential Revision: https://phabricator.services.mozilla.com/D33359

diff --git a/testing/mozbase/mozcrash/mozcrash/mozcrash.py b/testing/mozbase/mozcrash/mozcrash/mozcrash.py
--- a/testing/mozbase/mozcrash/mozcrash/mozcrash.py
+++ b/testing/mozbase/mozcrash/mozcrash/mozcrash.py
@@ -134,21 +134,39 @@ def log_crashes(logger,
         logger.crash(process=process, test=test, **kwargs)
     return crash_count
 
 
 # Function signatures of abort functions which should be ignored when
 # determining the appropriate frame for the crash signature.
 ABORT_SIGNATURES = (
     "Abort(char const*)",
+    "GeckoCrash",
     "NS_DebugBreak",
+    # This signature is part of Rust panic stacks on some platforms. On
+    # others, it includes a template parameter containing "core::panic::" and
+    # is automatically filtered out by that pattern.
+    "core::ops::function::Fn::call",
+    "gkrust_shared::panic_hook",
+    "intentional_panic",
     "mozalloc_abort",
     "static void Abort(const char *)",
 )
 
+# Similar to above, but matches if the substring appears anywhere in the
+# frame's signature.
+ABORT_SUBSTRINGS = (
+    # On some platforms, Rust panic frames unfortunately appear without the
+    # std::panicking or core::panic namespaces.
+    "_panic_",
+    "core::panic::",
+    "core::result::unwrap_failed",
+    "std::panicking::",
+)
+
 
 class CrashInfo(object):
     """Get information about a crash based on dump files.
 
     Typical usage is to iterate over the CrashInfo object. This returns StackInfo
     objects, one for each crash dump file that is found in the dump_directory.
 
     :param dump_directory: Path to search for minidump files
@@ -291,17 +309,19 @@ class CrashInfo(object):
                             if not line.startswith(" "):
                                 break
 
                             match = re.search(r"^ \d  (?:.*!)?(?:void )?([^\[]+)", line)
                             if match:
                                 func = match.group(1).strip()
                                 signature = "@ %s" % func
 
-                                if func not in ABORT_SIGNATURES:
+                                if not (func in ABORT_SIGNATURES or
+                                        any(pat in func
+                                            for pat in ABORT_SUBSTRINGS)):
                                     break
                         break
             else:
                 include_stderr = True
 
         else:
             if not self.symbols_path:
                 errors.append("No symbols path given, can't process dump.")
