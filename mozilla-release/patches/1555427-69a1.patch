# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1559238985 0
# Node ID 65ccd8aad15af04763db602d3e88d75c0a34254b
# Parent  60bb70332a2af5751f10ef169ef9cfd621cec27b
Bug 1555427: Skip abort functions when determining crash signature. r=gbrown

mozalloc_abort and related abort functions are the top frame for many
different, unrelated crashes because they happen to be the standard way to
abort execution. That makes it difficult to properly classify and deal with
intermittent failures.

This patch changes our crash handling behavior so that we try to skip any
frames at the top of the stack that are in generic abort functions, and use
the topmost frame which is actually relevant to the crash reason instead.

Differential Revision: https://phabricator.services.mozilla.com/D33051

diff --git a/testing/mozbase/mozcrash/mozcrash/mozcrash.py b/testing/mozbase/mozcrash/mozcrash/mozcrash.py
--- a/testing/mozbase/mozcrash/mozcrash/mozcrash.py
+++ b/testing/mozbase/mozcrash/mozcrash/mozcrash.py
@@ -130,16 +130,26 @@ def log_crashes(logger,
                           stackwalk_binary=stackwalk_binary):
         crash_count += 1
         kwargs = info._asdict()
         kwargs.pop("extra")
         logger.crash(process=process, test=test, **kwargs)
     return crash_count
 
 
+# Function signatures of abort functions which should be ignored when
+# determining the appropriate frame for the crash signature.
+ABORT_SIGNATURES = (
+    "Abort(char const*)",
+    "NS_DebugBreak",
+    "mozalloc_abort",
+    "static void Abort(const char *)",
+)
+
+
 class CrashInfo(object):
     """Get information about a crash based on dump files.
 
     Typical usage is to iterate over the CrashInfo object. This returns StackInfo
     objects, one for each crash dump file that is found in the dump_directory.
 
     :param dump_directory: Path to search for minidump files
     :param symbols_path: Path to a path to a directory containing symbols to use for
@@ -270,19 +280,29 @@ class CrashInfo(object):
                 #  0  libc.so + 0xa888
                 #  0  libnss3.so!nssCertificate_Destroy [certificate.c : 102 + 0x0]
                 #  0  mozjs.dll!js::GlobalObject::getDebuggers() [GlobalObject.cpp:89df18f9b6da : 580 + 0x0] # noqa
                 # 0  libxul.so!void js::gc::MarkInternal<JSObject>(JSTracer*, JSObject**)
                 # [Marking.cpp : 92 + 0x28]
                 lines = out.splitlines()
                 for i, line in enumerate(lines):
                     if "(crashed)" in line:
-                        match = re.search(r"^ 0  (?:.*!)?(?:void )?([^\[]+)", lines[i + 1])
-                        if match:
-                            signature = "@ %s" % match.group(1).strip()
+                        # Try to find the first frame that isn't an abort
+                        # function to use as the signature.
+                        for line in lines[i + 1:]:
+                            if not line.startswith(" "):
+                                break
+
+                            match = re.search(r"^ \d  (?:.*!)?(?:void )?([^\[]+)", line)
+                            if match:
+                                func = match.group(1).strip()
+                                signature = "@ %s" % func
+
+                                if func not in ABORT_SIGNATURES:
+                                    break
                         break
             else:
                 include_stderr = True
 
         else:
             if not self.symbols_path:
                 errors.append("No symbols path given, can't process dump.")
             if not self.stackwalk_binary:
