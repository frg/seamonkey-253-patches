# HG changeset patch
# User Mats Palmgren <mats@mozilla.com>
# Date 1521150084 -3600
#      Thu Mar 15 22:41:24 2018 +0100
# Node ID 020c8add83120b1bb2e93f7a2125104c713e565b
# Parent  47858fdcb6d0f4105e903efd1d483ee7f9e662aa
Bug 1425599 part 9 - [css-grid] Merge DistributeToTrackLimits/Bases (idempotent change).  r=dholbert

diff --git a/layout/generic/nsGridContainerFrame.cpp b/layout/generic/nsGridContainerFrame.cpp
--- a/layout/generic/nsGridContainerFrame.cpp
+++ b/layout/generic/nsGridContainerFrame.cpp
@@ -1476,17 +1476,17 @@ struct nsGridContainerFrame::Tracks
                          "unless we clamped some track's size");
   }
 
   /**
    * Distribute aAvailableSpace to the planned base size for aGrowableTracks
    * up to their limits, then distribute the remaining space beyond the limits.
    */
   template<TrackSizingPhase phase>
-  void DistributeToTrackBases(nscoord              aAvailableSpace,
+  void DistributeToTrackSizes(nscoord              aAvailableSpace,
                               nsTArray<TrackSize>& aPlan,
                               nsTArray<TrackSize>& aItemPlan,
                               nsTArray<uint32_t>&  aGrowableTracks,
                               TrackSize::StateBits aSelector,
                               const FitContentClamper& aFitContentClamper)
   {
     InitializeItemPlan<phase>(aItemPlan, aGrowableTracks);
     nscoord space = GrowTracksToLimit(aAvailableSpace, aItemPlan, aGrowableTracks,
@@ -1500,42 +1500,16 @@ struct nsGridContainerFrame::Tracks
       nscoord itemIncurredSize = aItemPlan[track].mBase;
       if (plannedSize < itemIncurredSize) {
         plannedSize = itemIncurredSize;
       }
     }
   }
 
   /**
-   * Distribute aAvailableSpace to the planned limits for aGrowableTracks.
-   */
-  template<TrackSizingPhase phase>
-  void DistributeToTrackLimits(nscoord              aAvailableSpace,
-                               nsTArray<TrackSize>& aPlan,
-                               nsTArray<TrackSize>& aItemPlan,
-                               nsTArray<uint32_t>&  aGrowableTracks,
-                               const FitContentClamper& aFitContentClamper)
-  {
-    InitializeItemPlan<phase>(aItemPlan, aGrowableTracks);
-    nscoord space = GrowTracksToLimit(aAvailableSpace, aItemPlan, aGrowableTracks,
-                                      aFitContentClamper);
-    if (space > 0) {
-      GrowSelectedTracksUnlimited(space, aItemPlan, aGrowableTracks,
-                                  TrackSize::StateBits(0), aFitContentClamper);
-    }
-    for (uint32_t track : aGrowableTracks) {
-      nscoord& plannedSize = aPlan[track].mBase;
-      nscoord itemIncurredSize = aItemPlan[track].mBase;
-      if (plannedSize < itemIncurredSize) {
-        plannedSize = itemIncurredSize;
-      }
-    }
-  }
-
-  /**
    * Distribute aAvailableSize to the tracks.  This implements 12.6 at:
    * http://dev.w3.org/csswg/css-grid/#algo-grow-tracks
    */
   void DistributeFreeSpace(nscoord aAvailableSize)
   {
     const uint32_t numTracks = mSizes.Length();
     if (MOZ_UNLIKELY(numTracks == 0 || aAvailableSize <= 0)) {
       return;
@@ -4262,17 +4236,17 @@ nsGridContainerFrame::Tracks::GrowBaseFo
     nscoord space = item.SizeContributionForPhase(phase);
     if (space <= 0) {
       continue;
     }
     aTracks.ClearAndRetainStorage();
     space = CollectGrowable<phase>(space, item.mLineRange, aSelector,
                                    aTracks);
     if (space > 0) {
-      DistributeToTrackBases<phase>(space, aPlan, aItemPlan, aTracks, aSelector,
+      DistributeToTrackSizes<phase>(space, aPlan, aItemPlan, aTracks, aSelector,
                                     aFitContentClamper);
       updatedBase = true;
     }
   }
   if (updatedBase) {
     CopyPlanToBase(aPlan);
   }
   return updatedBase;
@@ -4300,18 +4274,19 @@ nsGridContainerFrame::Tracks::GrowLimitF
       aPlan[j].mState |= TrackSize::eModified;
     }
     nscoord space = item.SizeContributionForPhase(phase);
     if (space > 0) {
       aTracks.ClearAndRetainStorage();
       space = CollectGrowable<phase>(space, item.mLineRange, aSelector,
                                      aTracks);
       if (space > 0) {
-        DistributeToTrackLimits<phase>(space, aPlan, aItemPlan, aTracks,
-                                       aFitContentClamper);
+        DistributeToTrackSizes<phase>(space, aPlan, aItemPlan, aTracks,
+                                      TrackSize::StateBits(0),
+                                      aFitContentClamper);
       }
     }
   }
   return true;
 }
 
 void
 nsGridContainerFrame::Tracks::ResolveIntrinsicSize(
