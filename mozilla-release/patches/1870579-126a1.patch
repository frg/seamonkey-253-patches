# HG changeset patch
# User Kershaw Chang <kershaw@mozilla.com>
# Date 1710798674 0
# Node ID 4c58e1b47bd6cd1610c6d4554314df14db3722e2
# Parent  687d9288706814f80d0f9fff6ad7eade549f92e9
Bug 1870579 - Use PK11_GenerateRandom to generate random number, r=necko-reviewers,valentin

Differential Revision: https://phabricator.services.mozilla.com/D204755

diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -2011,16 +2011,19 @@ pref("network.http.tcp_keepalive.long_li
 pref("network.http.tcp_keepalive.long_lived_idle_time", 600);
 
 pref("network.http.enforce-framing.http1", false); // should be named "strict"
 pref("network.http.enforce-framing.soft", true);
 
 // Max size, in bytes, for received HTTP response header.
 pref("network.http.max_response_header_size", 393216);
 
+// The length of the cnonce string used in HTTP digest auth.
+pref("network.http.digest_auth_cnonce_length", 64);
+
 // If we should attempt to race the cache and network
 pref("network.http.rcwn.enabled", false);
 pref("network.http.rcwn.cache_queue_normal_threshold", 8);
 pref("network.http.rcwn.cache_queue_priority_threshold", 2);
 // We might attempt to race the cache with the network only if a resource
 // is smaller than this size.
 pref("network.http.rcwn.small_resource_size_kb", 256);
 
diff --git a/netwerk/protocol/http/nsHttpDigestAuth.cpp b/netwerk/protocol/http/nsHttpDigestAuth.cpp
--- a/netwerk/protocol/http/nsHttpDigestAuth.cpp
+++ b/netwerk/protocol/http/nsHttpDigestAuth.cpp
@@ -2,30 +2,32 @@
  *
  * This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 // HttpLog.h should generally be included first
 #include "HttpLog.h"
 
+#include "mozilla/Preferences.h"
 #include "mozilla/Sprintf.h"
 #include "mozilla/Unused.h"
 
 #include "nsHttp.h"
 #include "nsHttpDigestAuth.h"
 #include "nsIHttpAuthenticableChannel.h"
 #include "nsISupportsPrimitives.h"
 #include "nsIURI.h"
 #include "nsString.h"
 #include "nsEscape.h"
 #include "nsNetCID.h"
 #include "nsCRT.h"
 #include "nsICryptoHash.h"
 #include "nsComponentManagerUtils.h"
+#include "pk11pub.h"
 
 namespace mozilla {
 namespace net {
 
 //-----------------------------------------------------------------------------
 // nsHttpDigestAuth <public>
 //-----------------------------------------------------------------------------
 
@@ -299,19 +301,28 @@ nsHttpDigestAuth::GenerateCredentials(ns
   }
   LOG(("   nonce_count=%s\n", nonce_count));
 
   //
   // this lets the client verify the server response (via a server
   // returned Authentication-Info header). also used for session info.
   //
   nsAutoCString cnonce;
-  static const char hexChar[] = "0123456789abcdef";
-  for (int i=0; i<16; ++i) {
-    cnonce.Append(hexChar[(int)(15.0 * rand()/(RAND_MAX + 1.0))]);
+  nsTArray<uint8_t> cnonceBuf;
+  int cnonceLength =
+     Preferences::GetInt("network.http.digest_auth_cnonce_length", 64);
+  if (cnonceLength < 4 || cnonceLength > 256) {
+    cnonceLength = 64;
+  }
+
+  cnonceBuf.SetLength(cnonceLength / 2);
+  PK11_GenerateRandom(reinterpret_cast<unsigned char*>(cnonceBuf.Elements()),
+                      cnonceBuf.Length());
+  for (auto byte : cnonceBuf) {
+    cnonce.AppendPrintf("%02x", byte);
   }
   LOG(("   cnonce=%s\n", cnonce.get()));
 
   //
   // calculate credentials
   //
 
   NS_ConvertUTF16toUTF8 cUser(username), cPass(password);
