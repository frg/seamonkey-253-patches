# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1566338724 0
# Node ID a07ecf63a8975f077d11a847db9426d828b6071e
# Parent  d4a6fecc32d0a103efab14c5aba12c46d9e0fa76
Bug 844509 - Don't create a separate unicode version of the build config. r=nalexander

Now that the configuration comes in without bytes strings, there is no
need to convert it anymore.

Differential Revision: https://phabricator.services.mozilla.com/D42631

diff --git a/python/mozbuild/mozbuild/backend/configenvironment.py b/python/mozbuild/mozbuild/backend/configenvironment.py
--- a/python/mozbuild/mozbuild/backend/configenvironment.py
+++ b/python/mozbuild/mozbuild/backend/configenvironment.py
@@ -179,41 +179,16 @@ class ConfigEnvironment(object):
         self.external_source_dir = None
         external = self.substs.get('EXTERNAL_SOURCE_DIR', '')
         if external:
             external = mozpath.normpath(external)
             if not os.path.isabs(external):
                 external = mozpath.join(self.topsrcdir, external)
             self.external_source_dir = mozpath.normpath(external)
 
-        # Populate a Unicode version of substs. This is an optimization to make
-        # moz.build reading faster, since each sandbox needs a Unicode version
-        # of these variables and doing it over a thousand times is a hotspot
-        # during sandbox execution!
-        # Bug 844509 tracks moving everything to Unicode.
-        self.substs_unicode = {}
-
-        def decode(v):
-            if not isinstance(v, six.text_type):
-                try:
-                    return v.decode('utf-8')
-                except UnicodeDecodeError:
-                    return v.decode('utf-8', 'replace')
-
-        for k, v in self.substs.items():
-            if not isinstance(v, six.string_types):
-                if isinstance(v, Iterable):
-                    type(v)(decode(i) for i in v)
-            elif not isinstance(v, six.text_type):
-                v = decode(v)
-
-            self.substs_unicode[k] = v
-
-        self.substs_unicode = ReadOnlyDict(self.substs_unicode)
-
     @property
     def is_artifact_build(self):
         return self.substs.get('MOZ_ARTIFACT_BUILDS', False)
 
     @memoized_property
     def acdefines(self):
         acdefines = dict((name, self.defines[name])
                          for name in self.defines
diff --git a/python/mozbuild/mozbuild/frontend/context.py b/python/mozbuild/mozbuild/frontend/context.py
--- a/python/mozbuild/mozbuild/frontend/context.py
+++ b/python/mozbuild/mozbuild/frontend/context.py
@@ -2485,17 +2485,17 @@ SPECIAL_VARIABLES = {
 
     'OBJDIR': (lambda context: context.objdir, str,
                """The path to the object directory for this file.
 
         Is is the same as ``TOPOBJDIR + RELATIVEDIR``.
         """),
 
     'CONFIG': (lambda context: ReadOnlyKeyedDefaultDict(
-            lambda key: context.config.substs_unicode.get(key)), dict,
+            lambda key: context.config.substs.get(key)), dict,
         """Dictionary containing the current configuration variables.
 
         All the variables defined by the configuration system are available
         through this object. e.g. ``ENABLE_TESTS``, ``CFLAGS``, etc.
 
         Values in this container are read-only. Attempts at changing values
         will result in a run-time error.
 
diff --git a/python/mozbuild/mozbuild/frontend/reader.py b/python/mozbuild/mozbuild/frontend/reader.py
--- a/python/mozbuild/mozbuild/frontend/reader.py
+++ b/python/mozbuild/mozbuild/frontend/reader.py
@@ -105,34 +105,27 @@ class EmptyConfig(object):
         """
 
         def get(self, key, default=None):
             return self[key]
 
     default_substs = {
         # These 2 variables are used semi-frequently and it isn't worth
         # changing all the instances.
-        b'MOZ_APP_NAME': b'empty',
-        b'MOZ_CHILD_PROCESS_NAME': b'empty',
+        'MOZ_APP_NAME': 'empty',
+        'MOZ_CHILD_PROCESS_NAME': 'empty',
         # Needed to prevent js/src's config.status from loading.
-        b'JS_STANDALONE': b'1',
+        'JS_STANDALONE': '1',
     }
 
     def __init__(self, topsrcdir, substs=None):
         self.topsrcdir = topsrcdir
         self.topobjdir = ''
 
         self.substs = self.PopulateOnGetDict(EmptyValue, substs or self.default_substs)
-        udict = {}
-        for k, v in self.substs.items():
-            if isinstance(v, str):
-                udict[k.decode('utf-8')] = v.decode('utf-8')
-            else:
-                udict[k] = v
-        self.substs_unicode = self.PopulateOnGetDict(EmptyValue, udict)
         self.defines = self.substs
         self.external_source_dir = None
         self.error_is_fatal = False
 
 
 def is_read_allowed(path, config):
     """Whether we are allowed to load a mozbuild file at the specified path.
 
diff --git a/python/mozbuild/mozbuild/test/backend/test_configenvironment.py b/python/mozbuild/mozbuild/test/backend/test_configenvironment.py
--- a/python/mozbuild/mozbuild/test/backend/test_configenvironment.py
+++ b/python/mozbuild/mozbuild/test/backend/test_configenvironment.py
@@ -24,20 +24,16 @@ class ConfigEnvironment(ConfigStatus.Con
                 top_srcdir = self.topsrcdir.replace(os.sep, '/')
             else:
                 top_srcdir = mozpath.relpath(self.topsrcdir, self.topobjdir).replace(os.sep, '/')
 
             d = dict(self.substs)
             d['top_srcdir'] = top_srcdir
             self.substs = ReadOnlyDict(d)
 
-            d = dict(self.substs_unicode)
-            d[u'top_srcdir'] = top_srcdir.decode('utf-8')
-            self.substs_unicode = ReadOnlyDict(d)
-
 
 class TestEnvironment(unittest.TestCase):
     def test_auto_substs(self):
         '''Test the automatically set values of ACDEFINES, ALLSUBSTS
         and ALLEMPTYSUBSTS.
         '''
         env = ConfigEnvironment('.', '.',
                                 defines={'foo': 'bar', 'baz': 'qux 42',
diff --git a/python/mozbuild/mozbuild/test/common.py b/python/mozbuild/mozbuild/test/common.py
--- a/python/mozbuild/mozbuild/test/common.py
+++ b/python/mozbuild/mozbuild/test/common.py
@@ -54,24 +54,16 @@ class MockConfig(object):
             'MOZ_FOO': 'foo',
             'MOZ_BAR': 'bar',
             'MOZ_TRUE': '1',
             'MOZ_FALSE': '',
             'DLL_PREFIX': 'lib',
             'DLL_SUFFIX': '.so'
         }, **extra_substs)
 
-        def decode_value(value):
-            if isinstance(value, list):
-                return [v.decode('utf-8', 'replace') for v in value]
-            return value.decode('utf-8', 'replace')
-
-        self.substs_unicode = ReadOnlyDict({k.decode('utf-8'): decode_value(v)
-                                            for k, v in self.substs.items()})
-
         self.defines = self.substs
 
         self.external_source_dir = None
         self.lib_prefix = 'lib'
         self.rust_lib_prefix = 'lib'
         self.lib_suffix = '.a'
         self.rust_lib_suffix = '.a'
         self.import_prefix = 'lib'
