# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1518692202 0
#      Thu Feb 15 10:56:42 2018 +0000
# Node ID 12b89457c62313feacba9ed2f0cd55af8dd58a36
# Parent  2b0d04896eae3f2417d4f81da20c580db9a96e9a
Bug 1436997 - When variation-font support is enabled, accept new CSS Fonts 4 format-hint strings for variation fonts. r=jwatt

diff --git a/gfx/thebes/gfxUserFontSet.h b/gfx/thebes/gfxUserFontSet.h
--- a/gfx/thebes/gfxUserFontSet.h
+++ b/gfx/thebes/gfxUserFontSet.h
@@ -192,24 +192,33 @@ public:
         FLAG_FORMAT_OPENTYPE       = 1 << 1,
         FLAG_FORMAT_TRUETYPE       = 1 << 2,
         FLAG_FORMAT_TRUETYPE_AAT   = 1 << 3,
         FLAG_FORMAT_EOT            = 1 << 4,
         FLAG_FORMAT_SVG            = 1 << 5,
         FLAG_FORMAT_WOFF           = 1 << 6,
         FLAG_FORMAT_WOFF2          = 1 << 7,
 
+        FLAG_FORMAT_OPENTYPE_VARIATIONS = 1 << 8,
+        FLAG_FORMAT_TRUETYPE_VARIATIONS = 1 << 9,
+        FLAG_FORMAT_WOFF_VARIATIONS     = 1 << 10,
+        FLAG_FORMAT_WOFF2_VARIATIONS    = 1 << 11,
+
         // the common formats that we support everywhere
         FLAG_FORMATS_COMMON        = FLAG_FORMAT_OPENTYPE |
                                      FLAG_FORMAT_TRUETYPE |
                                      FLAG_FORMAT_WOFF     |
-                                     FLAG_FORMAT_WOFF2,
+                                     FLAG_FORMAT_WOFF2    |
+                                     FLAG_FORMAT_OPENTYPE_VARIATIONS |
+                                     FLAG_FORMAT_TRUETYPE_VARIATIONS |
+                                     FLAG_FORMAT_WOFF_VARIATIONS     |
+                                     FLAG_FORMAT_WOFF2_VARIATIONS,
 
         // mask of all unused bits, update when adding new formats
-        FLAG_FORMAT_NOT_USED       = ~((1 << 8)-1)
+        FLAG_FORMAT_NOT_USED       = ~((1 << 12)-1)
     };
 
 
     // creates a font face without adding it to a particular family
     // weight - [100, 900] (multiples of 100)
     // stretch = [NS_FONT_STRETCH_ULTRA_CONDENSED, NS_FONT_STRETCH_ULTRA_EXPANDED]
     // italic style = constants in gfxFontConstants.h, e.g. NS_FONT_STYLE_NORMAL
     // language override = result of calling nsRuleNode::ParseFontLanguageOverride
diff --git a/layout/inspector/nsFontFace.cpp b/layout/inspector/nsFontFace.cpp
--- a/layout/inspector/nsFontFace.cpp
+++ b/layout/inspector/nsFontFace.cpp
@@ -177,16 +177,28 @@ nsFontFace::GetFormat(nsAString & aForma
       AppendToFormat(aFormat, "svg");
     }
     if (formatFlags & gfxUserFontSet::FLAG_FORMAT_WOFF) {
       AppendToFormat(aFormat, "woff");
     }
     if (formatFlags & gfxUserFontSet::FLAG_FORMAT_WOFF2) {
       AppendToFormat(aFormat, "woff2");
     }
+    if (formatFlags & gfxUserFontSet::FLAG_FORMAT_OPENTYPE_VARIATIONS) {
+      AppendToFormat(aFormat, "opentype-variations");
+    }
+    if (formatFlags & gfxUserFontSet::FLAG_FORMAT_TRUETYPE_VARIATIONS) {
+      AppendToFormat(aFormat, "truetype-variations");
+    }
+    if (formatFlags & gfxUserFontSet::FLAG_FORMAT_WOFF_VARIATIONS) {
+      AppendToFormat(aFormat, "woff-variations");
+    }
+    if (formatFlags & gfxUserFontSet::FLAG_FORMAT_WOFF2_VARIATIONS) {
+      AppendToFormat(aFormat, "woff2-variations");
+    }
   }
   return NS_OK;
 }
 
 NS_IMETHODIMP
 nsFontFace::GetMetadata(nsAString & aMetadata)
 {
   aMetadata.Truncate();
diff --git a/layout/style/FontFaceSet.cpp b/layout/style/FontFaceSet.cpp
--- a/layout/style/FontFaceSet.cpp
+++ b/layout/style/FontFaceSet.cpp
@@ -49,16 +49,17 @@
 #include "nsLayoutUtils.h"
 #include "nsPresContext.h"
 #include "nsPrintfCString.h"
 #ifdef MOZ_OLD_STYLE
 #include "nsStyleSet.h"
 #endif
 #include "nsUTF8Utils.h"
 #include "nsDOMNavigationTiming.h"
+#include "StylePrefs.h"
 
 using namespace mozilla;
 using namespace mozilla::css;
 using namespace mozilla::dom;
 
 #define LOG(args) MOZ_LOG(gfxUserFontSet::GetUserFontsLog(), mozilla::LogLevel::Debug, args)
 #define LOG_ENABLED() MOZ_LOG_TEST(gfxUserFontSet::GetUserFontsLog(), \
                                   LogLevel::Debug)
@@ -1192,16 +1193,29 @@ FontFaceSet::FindOrCreateUserFontEntryFr
             } else if (valueString.LowerCaseEqualsASCII("truetype")) {
               face->mFormatFlags |= gfxUserFontSet::FLAG_FORMAT_TRUETYPE;
             } else if (valueString.LowerCaseEqualsASCII("truetype-aat")) {
               face->mFormatFlags |= gfxUserFontSet::FLAG_FORMAT_TRUETYPE_AAT;
             } else if (valueString.LowerCaseEqualsASCII("embedded-opentype")) {
               face->mFormatFlags |= gfxUserFontSet::FLAG_FORMAT_EOT;
             } else if (valueString.LowerCaseEqualsASCII("svg")) {
               face->mFormatFlags |= gfxUserFontSet::FLAG_FORMAT_SVG;
+            } else if (StylePrefs::sFontVariationsEnabled &&
+                       valueString.LowerCaseEqualsASCII("woff-variations")) {
+              face->mFormatFlags |= gfxUserFontSet::FLAG_FORMAT_WOFF_VARIATIONS;
+            } else if (StylePrefs::sFontVariationsEnabled &&
+                       Preferences::GetBool(GFX_PREF_WOFF2_ENABLED) &&
+                       valueString.LowerCaseEqualsASCII("woff2-variations")) {
+              face->mFormatFlags |= gfxUserFontSet::FLAG_FORMAT_WOFF2_VARIATIONS;
+            } else if (StylePrefs::sFontVariationsEnabled &&
+                       valueString.LowerCaseEqualsASCII("opentype-variations")) {
+              face->mFormatFlags |= gfxUserFontSet::FLAG_FORMAT_OPENTYPE_VARIATIONS;
+            } else if (StylePrefs::sFontVariationsEnabled &&
+                       valueString.LowerCaseEqualsASCII("truetype-variations")) {
+              face->mFormatFlags |= gfxUserFontSet::FLAG_FORMAT_TRUETYPE_VARIATIONS;
             } else {
               // unknown format specified, mark to distinguish from the
               // case where no format hints are specified
               face->mFormatFlags |= gfxUserFontSet::FLAG_FORMAT_UNKNOWN;
             }
             i++;
           }
           if (!face->mURI) {
