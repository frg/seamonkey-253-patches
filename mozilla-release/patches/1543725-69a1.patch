# HG changeset patch
# User Samuel Thibault <samuel.thibault@ens-lyon.org>
# Date 1561644545 0
# Node ID 12fef65e97c98045b79af9b7f13352f0c2d0a9f2
# Parent  f7bd46abce399d9ae31c1c2ef88a1d20f4bc6ee8
Bug 1543725 Add marionette support to thunderbird r=ato,whimboo

Differential Revision: https://phabricator.services.mozilla.com/D27102

diff --git a/python/mozbuild/mozbuild/action/test_archive.py b/python/mozbuild/mozbuild/action/test_archive.py
--- a/python/mozbuild/mozbuild/action/test_archive.py
+++ b/python/mozbuild/mozbuild/action/test_archive.py
@@ -515,16 +515,30 @@ if buildconfig.substs.get('commtopsrcdir
     commtopsrcdir = buildconfig.substs.get('commtopsrcdir')
     mozharness_comm = {
         'source': commtopsrcdir,
         'base': 'mozharness',
         'pattern': '**',
         'dest': 'mozharness/configs'
     }
     ARCHIVE_FILES['mozharness'].append(mozharness_comm)
+    marionette_comm = {
+        'source': commtopsrcdir,
+        'base': '',
+        'manifest': 'testing/marionette/unit-tests.ini',
+        'dest': 'marionette/tests/comm',
+    }
+    ARCHIVE_FILES['common'].append(marionette_comm)
+    thunderbirdinstance = {
+        'source': commtopsrcdir,
+        'base': 'testing/marionette',
+        'pattern': 'thunderbirdinstance.py',
+        'dest': 'marionette/client/marionette_driver',
+    }
+    ARCHIVE_FILES['common'].append(thunderbirdinstance)
 
 
 # "common" is our catch all archive and it ignores things from other archives.
 # Verify nothing sneaks into ARCHIVE_FILES without a corresponding exclusion
 # rule in the "common" archive.
 for k, v in ARCHIVE_FILES.items():
     # Skip mozharness because it isn't staged.
     if k in ('common', 'mozharness'):
@@ -591,17 +605,17 @@ def find_files(archive):
         if pattern:
             patterns.append(pattern)
 
         manifest = entry.get('manifest')
         manifests = entry.get('manifests', [])
         if manifest:
             manifests.append(manifest)
         if manifests:
-            dirs = find_manifest_dirs(buildconfig.topsrcdir, manifests)
+            dirs = find_manifest_dirs(os.path.join(source, base), manifests)
             patterns.extend({'{}/**'.format(d) for d in dirs})
 
         ignore = list(entry.get('ignore', []))
         ignore.extend([
             '**/.flake8',
             '**/.mkdir.done',
             '**/*.pyc',
         ])
diff --git a/testing/marionette/client/marionette_driver/geckoinstance.py b/testing/marionette/client/marionette_driver/geckoinstance.py
--- a/testing/marionette/client/marionette_driver/geckoinstance.py
+++ b/testing/marionette/client/marionette_driver/geckoinstance.py
@@ -559,22 +559,39 @@ class DesktopInstance(GeckoInstance):
         "startup.homepage_welcome_url.additional": "",
     }
 
     def __init__(self, *args, **kwargs):
         super(DesktopInstance, self).__init__(*args, **kwargs)
         self.required_prefs.update(DesktopInstance.desktop_prefs)
 
 
+class ThunderbirdInstance(GeckoInstance):
+    def __init__(self, *args, **kwargs):
+        super(ThunderbirdInstance, self).__init__(*args, **kwargs)
+        try:
+            # Copied alongside in the test archive
+            from .thunderbirdinstance import thunderbird_prefs
+        except ImportError:
+            try:
+                # Coming from source tree through virtualenv
+                from thunderbirdinstance import thunderbird_prefs
+            except ImportError:
+                thunderbird_prefs = {}
+        self.required_prefs.update(thunderbird_prefs)
+
+
 class NullOutput(object):
     def __call__(self, line):
         pass
 
 
 apps = {
     'fennec': FennecInstance,
     'fxdesktop': DesktopInstance,
+    'thunderbird': ThunderbirdInstance,
 }
 
 app_ids = {
     '{aa3c5121-dab2-40e2-81ca-7ea25febc110}': 'fennec',
     '{ec8030f7-c20a-464f-9b0e-13a3a9e97384}': 'fxdesktop',
+    '{3550f703-e582-4d05-9a08-453d09bdfdc6}': 'thunderbird',
 }
diff --git a/testing/marionette/mach_commands.py b/testing/marionette/mach_commands.py
--- a/testing/marionette/mach_commands.py
+++ b/testing/marionette/mach_commands.py
@@ -13,16 +13,17 @@ from mach.decorators import (
     Command,
 )
 
 from mozbuild.base import (
     MachCommandBase,
     MachCommandConditions as conditions,
 )
 
+SUPPORTED_APPS = ['firefox', 'android', 'thunderbird']
 
 def create_parser_tests():
     from marionette_harness.runtests import MarionetteArguments
     from mozlog.structured import commandline
     parser = MarionetteArguments()
     commandline.add_logging_group(parser)
     return parser
 
@@ -32,20 +33,16 @@ def run_marionette(tests, binary=None, t
 
     from marionette_harness.runtests import (
         MarionetteTestRunner,
         MarionetteHarness
     )
 
     parser = create_parser_tests()
 
-    if not tests:
-        tests = [os.path.join(topsrcdir,
-                 "testing/marionette/harness/marionette_harness/tests/unit-tests.ini")]
-
     args = argparse.Namespace(tests=tests)
 
     args.binary = binary
     args.logger = kwargs.pop('log', None)
 
     for k, v in kwargs.iteritems():
         setattr(args, k, v)
 
@@ -57,31 +54,53 @@ def run_marionette(tests, binary=None, t
                                                 {"mach": sys.stdout})
     failed = MarionetteHarness(MarionetteTestRunner, args=vars(args)).run()
     if failed > 0:
         return 1
     else:
         return 0
 
 
+def is_buildapp_in(*apps):
+    def is_buildapp_supported(cls):
+        for a in apps:
+            c = getattr(conditions, 'is_{}'.format(a), None)
+            if c and c(cls):
+                return True
+        return False
+
+    is_buildapp_supported.__doc__ = 'Must have a {} build.'.format(
+        ' or '.join(apps))
+    return is_buildapp_supported
+
+
 @CommandProvider
 class MarionetteTest(MachCommandBase):
     @Command("marionette-test",
              category="testing",
              description="Remote control protocol to Gecko, used for browser automation.",
-             conditions=[conditions.is_firefox_or_android],
+             conditions=[is_buildapp_in(*SUPPORTED_APPS)],
              parser=create_parser_tests,
              )
     def marionette_test(self, tests, **kwargs):
         if "test_objects" in kwargs:
             tests = []
             for obj in kwargs["test_objects"]:
                 tests.append(obj["file_relpath"])
             del kwargs["test_objects"]
 
+        if not tests:
+            if conditions.is_thunderbird(self):
+                tests = [os.path.join(self.topsrcdir,
+                         "comm/testing/marionette/unit-tests.ini")]
+            else:
+                tests = [os.path.join(self.topsrcdir,
+                         "testing/marionette/harness/marionette_harness/tests/unit-tests.ini")]
+
         # Force disable e10s because it is not supported in Fennec
         if kwargs.get("app") == "fennec":
             kwargs["e10s"] = False
 
-        if not kwargs.get("binary") and conditions.is_firefox(self):
+        if not kwargs.get("binary") and \
+                (conditions.is_firefox(self) or conditions.is_thunderbird(self)):
             kwargs["binary"] = self.get_binary_path("app")
 
         return run_marionette(tests, topsrcdir=self.topsrcdir, **kwargs)
\ No newline at end of file
