# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1517413993 -3600
# Node ID dc3f24fe33a3b22c85c2f7e16e00bcefbf6af48c
# Parent  fcfb3de2c4a8f80f1041b051605b675ed0e486d9
Bug 1434267 - Add scratch register to MacroAssembler::loadStringChar. r=nbp

diff --git a/js/src/jit/CacheIRCompiler.cpp b/js/src/jit/CacheIRCompiler.cpp
--- a/js/src/jit/CacheIRCompiler.cpp
+++ b/js/src/jit/CacheIRCompiler.cpp
@@ -1837,17 +1837,17 @@ CacheIRCompiler::emitLoadStringCharResul
 
     FailurePath* failure;
     if (!addFailurePath(&failure))
         return false;
 
     // Bounds check, load string char.
     masm.boundsCheck32ForLoad(index, Address(str, JSString::offsetOfLength()), scratch1,
                               failure->label());
-    masm.loadStringChar(str, index, scratch1, failure->label());
+    masm.loadStringChar(str, index, scratch1, scratch2, failure->label());
 
     // Load StaticString for this char.
     masm.boundsCheck32PowerOfTwo(scratch1, StaticStrings::UNIT_STATIC_LIMIT, failure->label());
     masm.movePtr(ImmPtr(&cx_->staticStrings().unitStaticTable), scratch2);
     masm.loadPtr(BaseIndex(scratch2, scratch1, ScalePointer), scratch2);
 
     EmitStoreResult(masm, scratch2, JSVAL_TYPE_STRING, output);
     return true;
@@ -2679,9 +2679,9 @@ CacheIRCompiler::emitLoadInstanceOfObjec
     EmitStoreBoolean(masm, false, output);
     masm.jump(&done);
 
     masm.bind(&returnTrue);
     EmitStoreBoolean(masm, true, output);
     //fallthrough
     masm.bind(&done);
     return true;
-}
\ No newline at end of file
+}
diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -8164,19 +8164,20 @@ static const VMFunction CharCodeAtInfo =
     FunctionInfo<CharCodeAtFn>(jit::CharCodeAt, "CharCodeAt");
 
 void
 CodeGenerator::visitCharCodeAt(LCharCodeAt* lir)
 {
     Register str = ToRegister(lir->str());
     Register index = ToRegister(lir->index());
     Register output = ToRegister(lir->output());
+    Register temp = ToRegister(lir->temp());
 
     OutOfLineCode* ool = oolCallVM(CharCodeAtInfo, lir, ArgList(str, index), StoreRegisterTo(output));
-    masm.loadStringChar(str, index, output, ool->entry());
+    masm.loadStringChar(str, index, output, temp, ool->entry());
     masm.bind(ool->rejoin());
 }
 
 typedef JSFlatString* (*StringFromCharCodeFn)(JSContext*, int32_t);
 static const VMFunction StringFromCharCodeInfo =
     FunctionInfo<StringFromCharCodeFn>(jit::StringFromCharCode, "StringFromCharCode");
 
 void
diff --git a/js/src/jit/Lowering.cpp b/js/src/jit/Lowering.cpp
--- a/js/src/jit/Lowering.cpp
+++ b/js/src/jit/Lowering.cpp
@@ -1967,17 +1967,17 @@ void
 LIRGenerator::visitCharCodeAt(MCharCodeAt* ins)
 {
     MDefinition* str = ins->getOperand(0);
     MDefinition* idx = ins->getOperand(1);
 
     MOZ_ASSERT(str->type() == MIRType::String);
     MOZ_ASSERT(idx->type() == MIRType::Int32);
 
-    LCharCodeAt* lir = new(alloc()) LCharCodeAt(useRegister(str), useRegister(idx));
+    LCharCodeAt* lir = new(alloc()) LCharCodeAt(useRegister(str), useRegister(idx), temp());
     define(lir, ins);
     assignSafepoint(lir, ins);
 }
 
 void
 LIRGenerator::visitFromCharCode(MFromCharCode* ins)
 {
     MDefinition* code = ins->getOperand(0);
diff --git a/js/src/jit/MacroAssembler.cpp b/js/src/jit/MacroAssembler.cpp
--- a/js/src/jit/MacroAssembler.cpp
+++ b/js/src/jit/MacroAssembler.cpp
@@ -1498,44 +1498,36 @@ MacroAssembler::loadStringChars(Register
 
     bind(&isInline);
     computeEffectiveAddress(Address(str, JSInlineString::offsetOfInlineStorage()), dest);
 
     bind(&done);
 }
 
 void
-MacroAssembler::loadStringChar(Register str, Register index, Register output, Label* fail)
+MacroAssembler::loadStringChar(Register str, Register index, Register output, Register scratch,
+                               Label* fail)
 {
     MOZ_ASSERT(str != output);
     MOZ_ASSERT(str != index);
     MOZ_ASSERT(index != output);
+    MOZ_ASSERT(output != scratch);
 
     movePtr(str, output);
 
     // This follows JSString::getChar.
     Label notRope;
     branchIfNotRope(str, &notRope);
 
     // Load leftChild.
     loadPtr(Address(str, JSRope::offsetOfLeft()), output);
 
     // Check if the index is contained in the leftChild.
     // Todo: Handle index in the rightChild.
-    Label failPopStr, inLeft;
-    push(str);
-    boundsCheck32ForLoad(index, Address(output, JSString::offsetOfLength()), str, &failPopStr);
-    pop(str);
-    jump(&inLeft);
-
-    bind(&failPopStr);
-    pop(str);
-    jump(fail);
-
-    bind(&inLeft);
+    boundsCheck32ForLoad(index, Address(output, JSString::offsetOfLength()), scratch, fail);
 
     // If the left side is another rope, give up.
     branchIfRope(output, fail);
 
     bind(&notRope);
 
     Label isLatin1, done;
     // We have to check the left/right side for ropes,
diff --git a/js/src/jit/MacroAssembler.h b/js/src/jit/MacroAssembler.h
--- a/js/src/jit/MacroAssembler.h
+++ b/js/src/jit/MacroAssembler.h
@@ -1807,17 +1807,18 @@ class MacroAssembler : public MacroAssem
         loadPtr(Address(dest, ObjectGroup::offsetOfProto()), dest);
     }
 
     void loadStringLength(Register str, Register dest) {
         load32(Address(str, JSString::offsetOfLength()), dest);
     }
 
     void loadStringChars(Register str, Register dest);
-    void loadStringChar(Register str, Register index, Register output, Label* fail);
+    void loadStringChar(Register str, Register index, Register output, Register scratch,
+                        Label* fail);
 
     void loadStringIndexValue(Register str, Register dest, Label* fail);
 
     void loadJSContext(Register dest);
     void loadJitActivation(Register dest) {
         loadJSContext(dest);
         loadPtr(Address(dest, offsetof(JSContext, activation_)), dest);
     }
diff --git a/js/src/jit/shared/LIR-shared.h b/js/src/jit/shared/LIR-shared.h
--- a/js/src/jit/shared/LIR-shared.h
+++ b/js/src/jit/shared/LIR-shared.h
@@ -4079,32 +4079,36 @@ class LConcat : public LInstructionHelpe
         return this->getTemp(3);
     }
     const LDefinition* temp5() {
         return this->getTemp(4);
     }
 };
 
 // Get uint16 character code from a string.
-class LCharCodeAt : public LInstructionHelper<1, 2, 0>
+class LCharCodeAt : public LInstructionHelper<1, 2, 1>
 {
   public:
     LIR_HEADER(CharCodeAt)
 
-    LCharCodeAt(const LAllocation& str, const LAllocation& index) {
+    LCharCodeAt(const LAllocation& str, const LAllocation& index, const LDefinition& temp) {
         setOperand(0, str);
         setOperand(1, index);
+        setTemp(0, temp);
     }
 
     const LAllocation* str() {
         return this->getOperand(0);
     }
     const LAllocation* index() {
         return this->getOperand(1);
     }
+    const LDefinition* temp() {
+        return getTemp(0);
+    }
 };
 
 // Convert uint16 character code to a string.
 class LFromCharCode : public LInstructionHelper<1, 1, 0>
 {
   public:
     LIR_HEADER(FromCharCode)
 
