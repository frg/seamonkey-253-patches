# HG changeset patch
# User Lars T Hansen <lhansen@mozilla.com>
# Date 1517211343 -3600
#      Mon Jan 29 08:35:43 2018 +0100
# Node ID db980507149b41f59b4c9c242b61ba1caee7eb8c
# Parent  542ca8e8601686d85ab715f81bd3d6bf665449ce
Bug 1428453 : [MIPS] Implement hardware wasm traps support; patch=dragan.mladjenovic, r=luke, push=lth

diff --git a/js/src/jit/MacroAssembler.cpp b/js/src/jit/MacroAssembler.cpp
--- a/js/src/jit/MacroAssembler.cpp
+++ b/js/src/jit/MacroAssembler.cpp
@@ -3042,17 +3042,17 @@ MacroAssembler::maybeBranchTestType(MIRT
             MOZ_CRASH("Unsupported type");
         }
     }
 }
 
 void
 MacroAssembler::wasmTrap(wasm::Trap trap, wasm::BytecodeOffset bytecodeOffset)
 {
-    append(trap, wasm::TrapSite(illegalInstruction().offset(), bytecodeOffset));
+    append(trap, wasm::TrapSite(wasmTrapInstruction().offset(), bytecodeOffset));
 }
 
 void
 MacroAssembler::wasmCallImport(const wasm::CallSiteDesc& desc, const wasm::CalleeDesc& callee)
 {
     // Load the callee, before the caller's registers are clobbered.
     uint32_t globalDataOffset = callee.importGlobalDataOffset();
     loadWasmGlobalPtr(globalDataOffset + offsetof(wasm::FuncImportTls, code), ABINonArgReg0);
diff --git a/js/src/jit/MacroAssembler.h b/js/src/jit/MacroAssembler.h
--- a/js/src/jit/MacroAssembler.h
+++ b/js/src/jit/MacroAssembler.h
@@ -1408,17 +1408,18 @@ class MacroAssembler : public MacroAssem
 
     void convertInt64ToDouble(Register64 src, FloatRegister dest)
         DEFINED_ON(arm64, mips64, x64, x86);
 
   public:
     // ========================================================================
     // wasm support
 
-    CodeOffset illegalInstruction() PER_SHARED_ARCH;
+    CodeOffset wasmTrapInstruction() PER_SHARED_ARCH;
+
     void wasmTrap(wasm::Trap trap, wasm::BytecodeOffset bytecodeOffset);
 
     // Emit a bounds check against the wasm heap limit, jumping to 'label' if 'cond' holds.
     // Required when WASM_HUGE_MEMORY is not defined.
     template <class L>
     inline void wasmBoundsCheck(Condition cond, Register index, Register boundsCheckLimit, L label)
         DEFINED_ON(arm, arm64, mips32, mips64, x86);
 
diff --git a/js/src/jit/arm/MacroAssembler-arm.cpp b/js/src/jit/arm/MacroAssembler-arm.cpp
--- a/js/src/jit/arm/MacroAssembler-arm.cpp
+++ b/js/src/jit/arm/MacroAssembler-arm.cpp
@@ -4886,17 +4886,17 @@ MacroAssembler::storeUnboxedValue(const 
 template void
 MacroAssembler::storeUnboxedValue(const ConstantOrRegister& value, MIRType valueType,
                                   const Address& dest, MIRType slotType);
 template void
 MacroAssembler::storeUnboxedValue(const ConstantOrRegister& value, MIRType valueType,
                                   const BaseIndex& dest, MIRType slotType);
 
 CodeOffset
-MacroAssembler::illegalInstruction()
+MacroAssembler::wasmTrapInstruction()
 {
     return CodeOffset(as_illegal_trap().getOffset());
 }
 
 void
 MacroAssembler::wasmTruncateDoubleToUInt32(FloatRegister input, Register output, Label* oolEntry)
 {
     wasmTruncateToInt32(input, output, MIRType::Double, /* isUnsigned= */ true, oolEntry);
diff --git a/js/src/jit/arm64/MacroAssembler-arm64.cpp b/js/src/jit/arm64/MacroAssembler-arm64.cpp
--- a/js/src/jit/arm64/MacroAssembler-arm64.cpp
+++ b/js/src/jit/arm64/MacroAssembler-arm64.cpp
@@ -848,17 +848,17 @@ MacroAssembler::comment(const char* msg)
 {
     Assembler::comment(msg);
 }
 
 // ========================================================================
 // wasm support
 
 CodeOffset
-MacroAssembler::illegalInstruction()
+MacroAssembler::wasmTrapInstruction()
 {
     MOZ_CRASH("NYI");
 }
 
 void
 MacroAssembler::wasmTruncateDoubleToUInt32(FloatRegister input, Register output, Label* oolEntry)
 {
     MOZ_CRASH("NYI");
diff --git a/js/src/jit/mips-shared/Assembler-mips-shared.cpp b/js/src/jit/mips-shared/Assembler-mips-shared.cpp
--- a/js/src/jit/mips-shared/Assembler-mips-shared.cpp
+++ b/js/src/jit/mips-shared/Assembler-mips-shared.cpp
@@ -1774,16 +1774,64 @@ AssemblerMIPSShared::as_movn(FloatFormat
         spew("movn.d %3s,%3s,%3s", fd.name(), fs.name(), rt.name());
         return writeInst(InstReg(op_cop1, rs_d, rt, fs, fd, ff_movn_fmt).encode());
     } else {
         spew("movn.s %3s,%3s,%3s", fd.name(), fs.name(), rt.name());
         return writeInst(InstReg(op_cop1, rs_s, rt, fs, fd, ff_movn_fmt).encode());
     }
 }
 
+BufferOffset
+AssemblerMIPSShared::as_tge(Register rs, Register rt, uint32_t code)
+{
+    MOZ_ASSERT(code <= MAX_BREAK_CODE);
+    spew("tge %3s,%3s,%d", rs.name(), rt.name(), code);
+    return writeInst(InstReg(op_special, rs, rt, zero, code, ff_tge).encode());
+}
+
+BufferOffset
+AssemblerMIPSShared::as_tgeu(Register rs, Register rt, uint32_t code)
+{
+    MOZ_ASSERT(code <= MAX_BREAK_CODE);
+    spew("tgeu %3s,%3s,%d", rs.name(), rt.name(), code);
+    return writeInst(InstReg(op_special, rs, rt, zero, code, ff_tgeu).encode());
+}
+
+BufferOffset
+AssemblerMIPSShared::as_tlt(Register rs, Register rt, uint32_t code)
+{
+    MOZ_ASSERT(code <= MAX_BREAK_CODE);
+    spew("tlt %3s,%3s,%d", rs.name(), rt.name(), code);
+    return writeInst(InstReg(op_special, rs, rt, zero, code, ff_tlt).encode());
+}
+
+BufferOffset
+AssemblerMIPSShared::as_tltu(Register rs, Register rt, uint32_t code)
+{
+    MOZ_ASSERT(code <= MAX_BREAK_CODE);
+    spew("tltu %3s,%3s,%d", rs.name(), rt.name(), code);
+    return writeInst(InstReg(op_special, rs, rt, zero, code, ff_tltu).encode());
+}
+
+BufferOffset
+AssemblerMIPSShared::as_teq(Register rs, Register rt, uint32_t code)
+{
+    MOZ_ASSERT(code <= MAX_BREAK_CODE);
+    spew("teq %3s,%3s,%d", rs.name(), rt.name(), code);
+    return writeInst(InstReg(op_special, rs, rt, zero, code, ff_teq).encode());
+}
+
+BufferOffset
+AssemblerMIPSShared::as_tne(Register rs, Register rt, uint32_t code)
+{
+    MOZ_ASSERT(code <= MAX_BREAK_CODE);
+    spew("tne %3s,%3s,%d", rs.name(), rt.name(), code);
+    return writeInst(InstReg(op_special, rs, rt, zero, code, ff_tne).encode());
+}
+
 void
 AssemblerMIPSShared::bind(Label* label, BufferOffset boff)
 {
     spew(".set Llabel %p", label);
     // If our caller didn't give us an explicit target to bind to
     // then we want to bind to the location of the next instruction
     BufferOffset dest = boff.assigned() ? boff : nextOffset();
     if (label->used()) {
diff --git a/js/src/jit/mips-shared/Assembler-mips-shared.h b/js/src/jit/mips-shared/Assembler-mips-shared.h
--- a/js/src/jit/mips-shared/Assembler-mips-shared.h
+++ b/js/src/jit/mips-shared/Assembler-mips-shared.h
@@ -206,16 +206,17 @@ static const uint32_t RSMask = ((1 << RS
 static const uint32_t RTMask = ((1 << RTBits) - 1) << RTShift;
 static const uint32_t RDMask = ((1 << RDBits) - 1) << RDShift;
 static const uint32_t SAMask = ((1 << SABits) - 1) << SAShift;
 static const uint32_t FunctionMask = ((1 << FunctionBits) - 1) << FunctionShift;
 static const uint32_t RegMask = Registers::Total - 1;
 
 static const uint32_t BREAK_STACK_UNALIGNED = 1;
 static const uint32_t MAX_BREAK_CODE = 1024 - 1;
+static const uint32_t WASM_TRAP = 6; // BRK_OVERFLOW
 
 class Instruction;
 class InstReg;
 class InstImm;
 class InstJump;
 
 uint32_t RS(Register r);
 uint32_t RT(Register r);
@@ -1225,16 +1226,24 @@ class AssemblerMIPSShared : public Assem
     // FP conditional move.
     BufferOffset as_movt(FloatFormat fmt, FloatRegister fd, FloatRegister fs,
                          FPConditionBit fcc = FCC0);
     BufferOffset as_movf(FloatFormat fmt, FloatRegister fd, FloatRegister fs,
                          FPConditionBit fcc = FCC0);
     BufferOffset as_movz(FloatFormat fmt, FloatRegister fd, FloatRegister fs, Register rt);
     BufferOffset as_movn(FloatFormat fmt, FloatRegister fd, FloatRegister fs, Register rt);
 
+    // Conditional trap operations
+    BufferOffset as_tge(Register rs, Register rt, uint32_t code = 0);
+    BufferOffset as_tgeu(Register rs, Register rt, uint32_t code = 0);
+    BufferOffset as_tlt(Register rs, Register rt, uint32_t code = 0);
+    BufferOffset as_tltu(Register rs, Register rt, uint32_t code = 0);
+    BufferOffset as_teq(Register rs, Register rt, uint32_t code = 0);
+    BufferOffset as_tne(Register rs, Register rt, uint32_t code = 0);
+
     // label operations
     void bind(Label* label, BufferOffset boff = BufferOffset());
     void bindLater(Label* label, wasm::OldTrapDesc target);
     virtual void bind(InstImm* inst, uintptr_t branch, uintptr_t target) = 0;
     void bind(CodeOffset* label) {
         label->bind(currentOffset());
     }
     void use(CodeOffset* label) {
diff --git a/js/src/jit/mips-shared/MacroAssembler-mips-shared.cpp b/js/src/jit/mips-shared/MacroAssembler-mips-shared.cpp
--- a/js/src/jit/mips-shared/MacroAssembler-mips-shared.cpp
+++ b/js/src/jit/mips-shared/MacroAssembler-mips-shared.cpp
@@ -1864,19 +1864,21 @@ MacroAssembler::comment(const char* msg)
 {
     Assembler::comment(msg);
 }
 
 // ===============================================================
 // WebAssembly
 
 CodeOffset
-MacroAssembler::illegalInstruction()
+MacroAssembler::wasmTrapInstruction()
 {
-    MOZ_CRASH("NYI");
+    CodeOffset offset(currentOffset());
+    as_teq(zero, zero, WASM_TRAP);
+    return offset;
 }
 
 void
 MacroAssembler::wasmTruncateDoubleToInt32(FloatRegister input, Register output, Label* oolEntry)
 {
     as_truncwd(ScratchFloat32Reg, input);
     as_cfc1(ScratchRegister, Assembler::FCSR);
     moveFromFloat32(ScratchFloat32Reg, output);
diff --git a/js/src/jit/mips32/Simulator-mips32.cpp b/js/src/jit/mips32/Simulator-mips32.cpp
--- a/js/src/jit/mips32/Simulator-mips32.cpp
+++ b/js/src/jit/mips32/Simulator-mips32.cpp
@@ -237,17 +237,17 @@ class SimInstruction
         MOZ_ASSERT(instructionType() == kJumpType);
         return bits(Imm26Shift + Imm26Bits - 1, Imm26Shift);
     }
 
     // Say if the instruction should not be used in a branch delay slot.
     bool isForbiddenInBranchDelay() const;
     // Say if the instruction 'links'. e.g. jal, bal.
     bool isLinkingInstruction() const;
-    // Say if the instruction is a break or a trap.
+    // Say if the instruction is a debugger break/trap.
     bool isTrap() const;
 
   private:
 
     SimInstruction() = delete;
     SimInstruction(const SimInstruction& other) = delete;
     void operator=(const SimInstruction& other) = delete;
 };
@@ -323,29 +323,31 @@ SimInstruction::isLinkingInstruction() c
 bool
 SimInstruction::isTrap() const
 {
     if (opcodeFieldRaw() != op_special) {
         return false;
     } else {
         switch (functionFieldRaw()) {
           case ff_break:
+            return instructionBits() != kCallRedirInstr;
           case ff_tge:
           case ff_tgeu:
           case ff_tlt:
           case ff_tltu:
           case ff_teq:
           case ff_tne:
-            return true;
+            return bits(15, 6) != kWasmTrapCode;
           default:
             return false;
         };
     }
 }
 
+
 SimInstruction::Type
 SimInstruction::instructionType() const
 {
     switch (opcodeFieldRaw()) {
       case op_special:
         switch (functionFieldRaw()) {
           case ff_jr:
           case ff_jalr:
@@ -853,18 +855,17 @@ MipsDebugger::debug()
             // moment no command expects more than two parameters.
             int argc = sscanf(line,
                               "%" XSTR(COMMAND_SIZE) "s "
                               "%" XSTR(ARG_SIZE) "s "
                               "%" XSTR(ARG_SIZE) "s",
                               cmd, arg1, arg2);
             if ((strcmp(cmd, "si") == 0) || (strcmp(cmd, "stepi") == 0)) {
                 SimInstruction* instr = reinterpret_cast<SimInstruction*>(sim_->get_pc());
-                if (!(instr->isTrap()) ||
-                        instr->instructionBits() == kCallRedirInstr) {
+                if (!instr->isTrap()) {
                     sim_->instructionDecode(
                         reinterpret_cast<SimInstruction*>(sim_->get_pc()));
                 } else {
                     // Allow si to jump over generated breakpoints.
                     printf("/!\\ Jumping over generated breakpoint.\n");
                     sim_->set_pc(sim_->get_pc() + SimInstruction::kInstrSize);
                 }
             } else if ((strcmp(cmd, "c") == 0) || (strcmp(cmd, "cont") == 0)) {
@@ -1696,16 +1697,44 @@ Simulator::handleWasmFault(int32_t addr,
         return true;
     }
 
     MOZ_ASSERT(memoryAccess->hasTrapOutOfLineCode());
     set_pc(int32_t(memoryAccess->trapOutOfLineCode(segment->base())));
     return true;
 }
 
+bool
+Simulator::handleWasmTrapFault()
+{
+    if (!wasm::CodeExists)
+        return false;
+
+    JSContext* cx = TlsContext.get();
+    if (!cx->activation() || !cx->activation()->isJit())
+        return false;
+    JitActivation* act = cx->activation()->asJit();
+
+    void* pc = reinterpret_cast<void*>(get_pc());
+    uint8_t* fp = reinterpret_cast<uint8_t*>(getRegister(Register::fp));
+
+    const wasm::CodeSegment* segment = wasm::LookupCodeSegment(pc);
+    if (!segment)
+        return false;
+
+    wasm::Trap trap;
+    wasm::BytecodeOffset bytecode;
+    if (!segment->code().lookupTrap(pc, &trap, &bytecode))
+        return false;
+
+    act->startWasmTrap(trap, bytecode.offset, pc, fp);
+    set_pc(int32_t(segment->trapCode()));
+    return true;
+}
+
 // MIPS memory instructions (except lwl/r and swl/r) trap on unaligned memory
 // access enabling the OS to handle them via trap-and-emulate.
 // Note that simulator runs have the runtime system running directly on the host
 // system and only generated code is executed in the simulator.
 // Since the host is typically IA32 it will not trap on unaligned memory access.
 // We assume that that executing correct generated code will not produce unaligned
 // memory access, so we explicitly check for address alignment and trap.
 // Note that trapping does not occur when executing wasm code, which requires that
@@ -2260,16 +2289,26 @@ Simulator::softwareInterrupt(SimInstruct
     } else if (func == ff_break && code <= kMaxStopCode) {
         if (isWatchpoint(code)) {
             printWatchpoint(code);
         } else {
             increaseStopCounter(code);
             handleStop(code, instr);
         }
     } else {
+          switch (func) {
+            case ff_tge:
+            case ff_tgeu:
+            case ff_tlt:
+            case ff_tltu:
+            case ff_teq:
+            case ff_tne:
+            if (instr->bits(15, 6) == kWasmTrapCode && handleWasmTrapFault())
+                return;
+        };
         // All remaining break_ codes, and all traps are handled here.
         MipsDebugger dbg(this);
         dbg.debug();
     }
 }
 
 // Stop helper functions.
 bool
diff --git a/js/src/jit/mips32/Simulator-mips32.h b/js/src/jit/mips32/Simulator-mips32.h
--- a/js/src/jit/mips32/Simulator-mips32.h
+++ b/js/src/jit/mips32/Simulator-mips32.h
@@ -96,16 +96,17 @@ const uint32_t kFCSRExceptionFlagMask = 
 // - Breaks between 0 and kMaxWatchpointCode are treated as simple watchpoints,
 //   the simulator will run through them and print the registers.
 // - Breaks between kMaxWatchpointCode and kMaxStopCode are treated as stop()
 //   instructions (see Assembler::stop()).
 // - Breaks larger than kMaxStopCode are simple breaks, dropping you into the
 //   debugger.
 const uint32_t kMaxWatchpointCode = 31;
 const uint32_t kMaxStopCode = 127;
+const uint32_t kWasmTrapCode = 6;
 
 // -----------------------------------------------------------------------------
 // Utility functions
 
 typedef uint32_t Instr;
 class SimInstruction;
 
 // Per thread simulator state.
@@ -295,16 +296,17 @@ class Simulator {
     void printStopInfo(uint32_t code);
 
     // Handle a wasm interrupt triggered by an async signal handler.
     void handleWasmInterrupt();
     void startInterrupt(JitActivation* act);
 
     // Handle any wasm faults, returning true if the fault was handled.
     bool handleWasmFault(int32_t addr, unsigned numBytes);
+    bool handleWasmTrapFault();
 
     // Executes one instruction.
     void instructionDecode(SimInstruction* instr);
     // Execute one instruction placed in a branch delay slot.
     void branchDelayInstructionDecode(SimInstruction* instr);
 
   public:
     static int StopSimAt;
diff --git a/js/src/jit/mips64/Simulator-mips64.cpp b/js/src/jit/mips64/Simulator-mips64.cpp
--- a/js/src/jit/mips64/Simulator-mips64.cpp
+++ b/js/src/jit/mips64/Simulator-mips64.cpp
@@ -232,17 +232,17 @@ class SimInstruction
         MOZ_ASSERT(instructionType() == kJumpType);
         return bits(Imm26Shift + Imm26Bits - 1, Imm26Shift);
     }
 
     // Say if the instruction should not be used in a branch delay slot.
     bool isForbiddenInBranchDelay() const;
     // Say if the instruction 'links'. e.g. jal, bal.
     bool isLinkingInstruction() const;
-    // Say if the instruction is a break or a trap.
+    // Say if the instruction is a debugger break/trap.
     bool isTrap() const;
 
   private:
 
     SimInstruction() = delete;
     SimInstruction(const SimInstruction& other) = delete;
     void operator=(const SimInstruction& other) = delete;
 };
@@ -318,23 +318,24 @@ SimInstruction::isLinkingInstruction() c
 bool
 SimInstruction::isTrap() const
 {
     if (opcodeFieldRaw() != op_special) {
         return false;
     } else {
         switch (functionFieldRaw()) {
           case ff_break:
+            return instructionBits() != kCallRedirInstr;
           case ff_tge:
           case ff_tgeu:
           case ff_tlt:
           case ff_tltu:
           case ff_teq:
           case ff_tne:
-            return true;
+            return bits(15, 6) != kWasmTrapCode;
           default:
             return false;
         };
     }
 }
 
 SimInstruction::Type
 SimInstruction::instructionType() const
@@ -864,18 +865,17 @@ MipsDebugger::debug()
             // moment no command expects more than two parameters.
             int argc = sscanf(line,
                               "%" XSTR(COMMAND_SIZE) "s "
                               "%" XSTR(ARG_SIZE) "s "
                               "%" XSTR(ARG_SIZE) "s",
                               cmd, arg1, arg2);
             if ((strcmp(cmd, "si") == 0) || (strcmp(cmd, "stepi") == 0)) {
                 SimInstruction* instr = reinterpret_cast<SimInstruction*>(sim_->get_pc());
-                if (!(instr->isTrap()) ||
-                        instr->instructionBits() == kCallRedirInstr) {
+                if (!instr->isTrap()) {
                     sim_->instructionDecode(
                         reinterpret_cast<SimInstruction*>(sim_->get_pc()));
                 } else {
                     // Allow si to jump over generated breakpoints.
                     printf("/!\\ Jumping over generated breakpoint.\n");
                     sim_->set_pc(sim_->get_pc() + SimInstruction::kInstrSize);
                 }
             } else if ((strcmp(cmd, "c") == 0) || (strcmp(cmd, "cont") == 0)) {
@@ -1619,16 +1619,44 @@ Simulator::startInterrupt(JitActivation*
 }
 
 void
 Simulator::handleWasmInterrupt()
 {
     MOZ_CRASH("NIY");
 }
 
+bool
+Simulator::handleWasmTrapFault()
+{
+    if (!wasm::CodeExists)
+        return false;
+
+    JSContext* cx = TlsContext.get();
+    if (!cx->activation() || !cx->activation()->isJit())
+        return false;
+    JitActivation* act = cx->activation()->asJit();
+
+    void* pc = reinterpret_cast<void*>(get_pc());
+    uint8_t* fp = reinterpret_cast<uint8_t*>(getRegister(Register::fp));
+
+    const wasm::CodeSegment* segment = wasm::LookupCodeSegment(pc);
+    if (!segment)
+        return false;
+
+    wasm::Trap trap;
+    wasm::BytecodeOffset bytecode;
+    if (!segment->code().lookupTrap(pc, &trap, &bytecode))
+        return false;
+
+    act->startWasmTrap(trap, bytecode.offset, pc, fp);
+    set_pc(int64_t(segment->trapCode()));
+    return true;
+}
+
 // The MIPS cannot do unaligned reads and writes.  On some MIPS platforms an
 // interrupt is caused.  On others it does a funky rotation thing.  For now we
 // simply disallow unaligned reads, but at some point we may want to move to
 // emulating the rotate behaviour.  Note that simulator runs have the runtime
 // system running directly on the host system and only generated code is
 // executed in the simulator.  Since the host is typically IA32 we will not
 // get the correct MIPS-like behaviour on unaligned accesses.
 
@@ -2127,16 +2155,26 @@ Simulator::softwareInterrupt(SimInstruct
     } else if (func == ff_break && code <= kMaxStopCode) {
         if (isWatchpoint(code)) {
             printWatchpoint(code);
         } else {
             increaseStopCounter(code);
             handleStop(code, instr);
         }
     } else {
+        switch (func) {
+            case ff_tge:
+            case ff_tgeu:
+            case ff_tlt:
+            case ff_tltu:
+            case ff_teq:
+            case ff_tne:
+            if (instr->bits(15, 6) == kWasmTrapCode && handleWasmTrapFault())
+                return;
+        };
         // All remaining break_ codes, and all traps are handled here.
         MipsDebugger dbg(this);
         dbg.debug();
     }
 }
 
 // Stop helper functions.
 bool
diff --git a/js/src/jit/mips64/Simulator-mips64.h b/js/src/jit/mips64/Simulator-mips64.h
--- a/js/src/jit/mips64/Simulator-mips64.h
+++ b/js/src/jit/mips64/Simulator-mips64.h
@@ -101,16 +101,17 @@ const uint32_t kFCSRExceptionFlagMask = 
 // - Breaks between 0 and kMaxWatchpointCode are treated as simple watchpoints,
 //   the simulator will run through them and print the registers.
 // - Breaks between kMaxWatchpointCode and kMaxStopCode are treated as stop()
 //   instructions (see Assembler::stop()).
 // - Breaks larger than kMaxStopCode are simple breaks, dropping you into the
 //   debugger.
 const uint32_t kMaxWatchpointCode = 31;
 const uint32_t kMaxStopCode = 127;
+const uint32_t kWasmTrapCode = 6;
 
 // -----------------------------------------------------------------------------
 // Utility functions
 
 typedef uint32_t Instr;
 class SimInstruction;
 
 // Per thread simulator state.
@@ -302,16 +303,18 @@ class Simulator {
     void disableStop(uint32_t code);
     void increaseStopCounter(uint32_t code);
     void printStopInfo(uint32_t code);
 
     // Handle a wasm interrupt triggered by an async signal handler.
     void handleWasmInterrupt();
     void startInterrupt(JitActivation* act);
 
+    bool handleWasmTrapFault();
+
     // Executes one instruction.
     void instructionDecode(SimInstruction* instr);
     // Execute one instruction placed in a branch delay slot.
     void branchDelayInstructionDecode(SimInstruction* instr);
 
   public:
     static int64_t StopSimAt;
 
diff --git a/js/src/jit/x86-shared/MacroAssembler-x86-shared.cpp b/js/src/jit/x86-shared/MacroAssembler-x86-shared.cpp
--- a/js/src/jit/x86-shared/MacroAssembler-x86-shared.cpp
+++ b/js/src/jit/x86-shared/MacroAssembler-x86-shared.cpp
@@ -667,17 +667,17 @@ MacroAssembler::pushFakeReturnAddress(Re
     addCodeLabel(cl);
     return retAddr;
 }
 
 // ===============================================================
 // WebAssembly
 
 CodeOffset
-MacroAssembler::illegalInstruction()
+MacroAssembler::wasmTrapInstruction()
 {
     return ud2();
 }
 
 // RAII class that generates the jumps to traps when it's destructed, to
 // prevent some code duplication in the outOfLineWasmTruncateXtoY methods.
 struct MOZ_RAII AutoHandleWasmTruncateToIntErrors
 {
diff --git a/js/src/wasm/WasmSignalHandlers.cpp b/js/src/wasm/WasmSignalHandlers.cpp
--- a/js/src/wasm/WasmSignalHandlers.cpp
+++ b/js/src/wasm/WasmSignalHandlers.cpp
@@ -1334,44 +1334,55 @@ MachExceptionHandler::install(JSContext*
 
     installed_ = true;
     onFailure.release();
     return true;
 }
 
 #else  // If not Windows or Mac, assume Unix
 
+#ifdef __mips__
+    static const uint32_t kWasmTrapSignal = SIGFPE;
+#else
+    static const uint32_t kWasmTrapSignal = SIGILL;
+#endif
+
 // Be very cautious and default to not handling; we don't want to accidentally
 // silence real crashes from real bugs.
 static bool
 HandleFault(int signum, siginfo_t* info, void* ctx)
 {
     // Before anything else, prevent handler recursion.
     if (sAlreadyInSignalHandler.get())
         return false;
     AutoSignalHandler ash;
 
-    MOZ_RELEASE_ASSERT(signum == SIGSEGV || signum == SIGBUS || signum == SIGILL);
+    MOZ_RELEASE_ASSERT(signum == SIGSEGV || signum == SIGBUS || signum == kWasmTrapSignal);
 
     CONTEXT* context = (CONTEXT*)ctx;
     uint8_t** ppc = ContextToPC(context);
     uint8_t* pc = *ppc;
 
     const CodeSegment* segment = LookupCodeSegment(pc);
     if (!segment)
         return false;
 
     const Instance* instance = LookupFaultingInstance(*segment, pc, ContextToFP(context));
     if (!instance)
         return false;
 
     JitActivation* activation = TlsContext.get()->activation()->asJit();
     MOZ_ASSERT(activation->compartment() == instance->compartment());
 
-    if (signum == SIGILL) {
+    if (signum == kWasmTrapSignal) {
+        // Wasm traps for MIPS raise only integer overflow fp exception.
+#ifdef __mips__
+        if (info->si_code != FPE_INTOVF)
+            return false;
+#endif
         Trap trap;
         BytecodeOffset bytecode;
         if (!segment->code().lookupTrap(pc, &trap, &bytecode))
             return false;
 
         activation->startWasmTrap(trap, bytecode.offset, pc, ContextToFP(context));
         *ppc = segment->trapCode();
         return true;
@@ -1411,29 +1422,29 @@ HandleFault(int signum, siginfo_t* info,
 #endif
 
     HandleMemoryAccess(context, pc, faultingAddress, segment, *instance, activation, ppc);
     return true;
 }
 
 static struct sigaction sPrevSEGVHandler;
 static struct sigaction sPrevSIGBUSHandler;
-static struct sigaction sPrevSIGILLHandler;
+static struct sigaction sPrevWasmTrapHandler;
 
 static void
 WasmFaultHandler(int signum, siginfo_t* info, void* context)
 {
     if (HandleFault(signum, info, context))
         return;
 
     struct sigaction* previousSignal = nullptr;
     switch (signum) {
       case SIGSEGV: previousSignal = &sPrevSEGVHandler; break;
       case SIGBUS: previousSignal = &sPrevSIGBUSHandler; break;
-      case SIGILL: previousSignal = &sPrevSIGILLHandler; break;
+      case kWasmTrapSignal: previousSignal = &sPrevWasmTrapHandler; break;
     }
     MOZ_ASSERT(previousSignal);
 
     // This signal is not for any asm.js code we expect, so we need to forward
     // the signal to the next handler. If there is no next handler (SIG_IGN or
     // SIG_DFL), then it's time to crash. To do this, we set the signal back to
     // its original disposition and return. This will cause the faulting op to
     // be re-executed which will crash in the normal way. The advantage of
@@ -1666,24 +1677,24 @@ ProcessHasSignalHandlers()
     struct sigaction busHandler;
     busHandler.sa_flags = SA_SIGINFO | SA_NODEFER | SA_ONSTACK;
     busHandler.sa_sigaction = WasmFaultHandler;
     sigemptyset(&busHandler.sa_mask);
     if (sigaction(SIGBUS, &busHandler, &sPrevSIGBUSHandler))
         MOZ_CRASH("unable to install sigbus handler");
 #  endif
 
-    // Install a SIGILL handler to handle the ud2 instructions that are emitted
-    // to implement wasm traps.
-    struct sigaction illegalHandler;
-    illegalHandler.sa_flags = SA_SIGINFO | SA_NODEFER | SA_ONSTACK;
-    illegalHandler.sa_sigaction = WasmFaultHandler;
-    sigemptyset(&illegalHandler.sa_mask);
-    if (sigaction(SIGILL, &illegalHandler, &sPrevSIGILLHandler))
-        MOZ_CRASH("unable to install segv handler");
+    // Install a handler to handle the instructions that are emitted to implement
+    // wasm traps.
+    struct sigaction wasmTrapHandler;
+    wasmTrapHandler.sa_flags = SA_SIGINFO | SA_NODEFER | SA_ONSTACK;
+    wasmTrapHandler.sa_sigaction = WasmFaultHandler;
+    sigemptyset(&wasmTrapHandler.sa_mask);
+    if (sigaction(kWasmTrapSignal, &wasmTrapHandler, &sPrevWasmTrapHandler))
+        MOZ_CRASH("unable to install wasm trap handler");
 # endif
 
     sHaveSignalHandlers = true;
     return true;
 }
 
 bool
 wasm::EnsureSignalHandlers(JSContext* cx)
