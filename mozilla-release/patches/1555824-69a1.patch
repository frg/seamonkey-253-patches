# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1559252876 0
# Node ID 87017ac1c788a9226a8f2d291a65ea974482dc43
# Parent  03ad8f4f38764e8c9859b6fadc49ee0a922aa369
Bug 1555824 - Don't call MozbuildObject.substs from a MachCommandCondition r=nalexander

Invoking MozbuildObject.substs causes an exception to be raised if there is no
BuildEnvironment. This means that we hit an error on |mach help| if there is no
build, which is not ideal for people trying to learn how to build :).

Differential Revision: https://phabricator.services.mozilla.com/D33217

diff --git a/python/mozbuild/mozbuild/base.py b/python/mozbuild/mozbuild/base.py
--- a/python/mozbuild/mozbuild/base.py
+++ b/python/mozbuild/mozbuild/base.py
@@ -18,16 +18,18 @@ try:
 except ImportError:
     # shutil.which is not available in Python 2.7
     import which
 
 from mach.mixin.process import ProcessExecutionMixin
 from mozversioncontrol import (
     get_repository_from_build_config,
     get_repository_object,
+    GitRepository,
+    HgRepository,
     InvalidRepoPath,
 )
 
 from .backend.configenvironment import ConfigEnvironment
 from .configure import ConfigureSandbox
 from .controller.clobber import Clobberer
 from .mozconfig import (
     MozconfigFindException,
@@ -971,27 +973,42 @@ class MachCommandConditions(object):
     @staticmethod
     def is_firefox_or_android(cls):
         """Must have a Firefox or Android build."""
         return MachCommandConditions.is_firefox(cls) or MachCommandConditions.is_android(cls)
 
     @staticmethod
     def is_hg(cls):
         """Must have a mercurial source checkout."""
-        return getattr(cls, 'substs', {}).get('VCS_CHECKOUT_TYPE') == 'hg'
+        try:
+            return isinstance(cls.repository, HgRepository)
+        except InvalidRepoPath:
+            return False
 
     @staticmethod
     def is_git(cls):
         """Must have a git source checkout."""
-        return getattr(cls, 'substs', {}).get('VCS_CHECKOUT_TYPE') == 'git'
+        try:
+            return isinstance(cls.repository, GitRepository)
+        except InvalidRepoPath:
+            return False
 
     @staticmethod
     def is_artifact_build(cls):
         """Must be an artifact build."""
-        return getattr(cls, 'substs', {}).get('MOZ_ARTIFACT_BUILDS')
+        if hasattr(cls, 'substs'):
+            return getattr(cls, 'substs', {}).get('MOZ_ARTIFACT_BUILDS')
+        return False
+
+    @staticmethod
+    def is_non_artifact_build(cls):
+        """Must not be an artifact build."""
+        if hasattr(cls, 'substs'):
+            return not MachCommandConditions.is_artifact_build(cls)
+        return False
 
 
 class PathArgument(object):
     """Parse a filesystem path argument and transform it in various ways."""
 
     def __init__(self, arg, topsrcdir, topobjdir, cwd=None):
         self.arg = arg
         self.topsrcdir = topsrcdir
