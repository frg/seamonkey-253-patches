# HG changeset patch
# User Bryce Van Dyk <bvandyk@mozilla.com>
# Date 1521831281 14400
# Node ID bf78f178cc7f798c7d1552240c5e609a5be2e7b6
# Parent  d79358f153b9f80a191d30df1b967229f126d6e7
Bug 1447821 - Update EMEDecoderModule to handle profile values < 1. r=jya

Update EMEDecoderModule to use 2 as profile number when the given profile is
less than 1 or greater than 4. The CDM doesn't appear to care what values are
given, but 2 was chosen as a safe fallback per discussion on the bug. This
addresses the use case where 0 values are stored in mProfile due to the use of
extended profiles (which are then stored in the mExtendedProfile field).

MozReview-Commit-ID: 5XgabNDsgdf

diff --git a/dom/media/platforms/agnostic/eme/EMEDecoderModule.cpp b/dom/media/platforms/agnostic/eme/EMEDecoderModule.cpp
--- a/dom/media/platforms/agnostic/eme/EMEDecoderModule.cpp
+++ b/dom/media/platforms/agnostic/eme/EMEDecoderModule.cpp
@@ -16,34 +16,37 @@
 #include "mozilla/EMEUtils.h"
 #include "mozilla/UniquePtr.h"
 #include "mozilla/Unused.h"
 #include "nsAutoPtr.h"
 #include "nsClassHashtable.h"
 #include "nsServiceManagerUtils.h"
 #include "DecryptThroughputLimit.h"
 #include "ChromiumCDMVideoDecoder.h"
-#include <algorithm>
 
 namespace mozilla {
 
 typedef MozPromiseRequestHolder<DecryptPromise> DecryptPromiseRequestHolder;
 extern already_AddRefed<PlatformDecoderModule> CreateBlankDecoderModule();
 
 DDLoggedTypeDeclNameAndBase(EMEDecryptor, MediaDataDecoder);
 
 class ADTSSampleConverter
 {
 public:
   explicit ADTSSampleConverter(const AudioInfo& aInfo)
     : mNumChannels(aInfo.mChannels)
-    // Note: we clamp profile to 4 so that HE-AACv2 (profile 5) can pass
-    // through the conversion to ADTS and back again.
-    , mProfile(
-        std::min<uint8_t>(static_cast<uint8_t>(0xff & aInfo.mProfile), 4))
+    // Note: we set profile to 2 if we encounter an extended profile (which set
+    // mProfile to 0 and then set mExtendedProfile) such as HE-AACv2
+    // (profile 5). These can then pass through conversion to ADTS and back.
+    // This is done as ADTS only has 2 bits for profile, and the transform
+    // subtracts one from the value. We check if the profile supplied is > 4 for
+    // safety. 2 is used as a fallback value, though it seems the CDM doesn't
+    // care what is set.
+    , mProfile(aInfo.mProfile < 1 || aInfo.mProfile > 4 ? 2 : aInfo.mProfile)
     , mFrequencyIndex(Adts::GetFrequencyIndex(aInfo.mRate))
   {
   }
   bool Convert(MediaRawData* aSample) const
   {
     return Adts::ConvertSample(
       mNumChannels, mFrequencyIndex, mProfile, aSample);
   }
