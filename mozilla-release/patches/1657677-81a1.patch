# HG changeset patch
# User Mitchell Hentges <mhentges@mozilla.com>
# Date 1596744656 0
# Node ID 467580a245539dd05401e0d1061dac8b6a5dd8bd
# Parent  6e698dd497ea1e3044a414b91aae915d0863c89b
Bug 1657677: Resolves FileNotFoundError when running python-test from PyCharm r=firefox-build-system-reviewers,rstewart

When subprocessing to get the venv from pipenv with PyCharm, the returned path has a command sequence to reset the terminal color.
This command sequence is unexpected, and causes the returned path to be incorrect.

The root issue is that colorama mistakenly believes that, if it's ever running underneath PyCharm, it's probably attached to a tty.
However, this assumption isn't true if PyCharm is running a script which subprocesses out a colorama-enabled script.
This issue has already been reported here: https://github.com/tartley/colorama/issues/263

To workaround this issue, we clear the "PYCHARM_HOSTED" environment variable when we invoke pipenv, which works around the colorama logic.

Differential Revision: https://phabricator.services.mozilla.com/D86242

diff --git a/python/mozbuild/mozbuild/virtualenv.py b/python/mozbuild/mozbuild/virtualenv.py
--- a/python/mozbuild/mozbuild/virtualenv.py
+++ b/python/mozbuild/mozbuild/virtualenv.py
@@ -634,20 +634,22 @@ class VirtualenvManager(object):
                     [pipenv, '--python', python],
                     stderr=subprocess.STDOUT,
                     env=env)
             return get_venv()
 
         def get_venv():
             """Return path to virtual environment or None"""
             try:
+                sub_env = env.copy()
+                sub_env.pop('PYCHARM_HOSTED', None)
                 return subprocess.check_output(
                         [pipenv, '--venv'],
                         stderr=subprocess.STDOUT,
-                        env=env, universal_newlines=True).rstrip()
+                        env=sub_env, universal_newlines=True).rstrip()
 
             except subprocess.CalledProcessError:
                 # virtual environment does not exist
                 return None
 
         if pipfile is not None:
             # Install from Pipfile
             env_ = env.copy()
