# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1531255309 0
# Node ID 0facbb3ab290162afef2002a779ca83ebec8e2ef
# Parent  84c3caf12dad3a4768d8ae569d783d1d6c1fd0d5
Bug 1413922 - [mozversioncontrol] Always use hglib.client if available and fall back to subprocesses if not, r=gps

Most HG commands use subprocesses, even if a context manager (and therefore an
hglib client) has been created. There are only two commands that make use of
the client, but they *only* work inside a context manager. I don't think
there are any technical reason these two commands *need* to use the context
manager.

This patch merges the HgRepository._run_in_client function with
HgRepository._run(). If a client exists, that will be used, otherwise a
subprocess will be used.

Differential Revision: https://phabricator.services.mozilla.com/D1809

diff --git a/python/mozbuild/mozbuild/action/langpack_manifest.py b/python/mozbuild/mozbuild/action/langpack_manifest.py
--- a/python/mozbuild/mozbuild/action/langpack_manifest.py
+++ b/python/mozbuild/mozbuild/action/langpack_manifest.py
@@ -48,23 +48,23 @@ pushlog_api_url = "{0}/json-rev/{1}"
 #    (datetime) - a datetime object
 #
 # Example:
 #    dt = get_dt_from_hg("/var/vcs/l10n-central/pl")
 #    dt == datetime(2017, 10, 11, 23, 31, 54, 0)
 ###
 def get_dt_from_hg(path):
     with mozversioncontrol.get_repository_object(path=path) as repo:
-        phase = repo._run_in_client(["log", "-r", ".", "-T" "{phase}"])
+        phase = repo._run("log", "-r", ".", "-T" "{phase}")
         if phase.strip() != "public":
             return datetime.datetime.utcnow()
-        repo_url = repo._run_in_client(["paths", "default"])
+        repo_url = repo._run("paths", "default")
         repo_url = repo_url.strip().replace("ssh://", "https://")
         repo_url = repo_url.replace("hg://", "https://")
-        cs = repo._run_in_client(["log", "-r", ".", "-T" "{node}"])
+        cs = repo._run("log", "-r", ".", "-T" "{node}")
 
     url = pushlog_api_url.format(repo_url, cs)
     session = requests.Session()
     try:
         response = session.get(url)
     except Exception as e:
         msg = "Failed to retrieve push timestamp using {}\nError: {}".format(url, e)
         raise Exception(msg)
diff --git a/python/mozversioncontrol/mozversioncontrol/__init__.py b/python/mozversioncontrol/mozversioncontrol/__init__.py
--- a/python/mozversioncontrol/mozversioncontrol/__init__.py
+++ b/python/mozversioncontrol/mozversioncontrol/__init__.py
@@ -231,20 +231,19 @@ class HgRepository(Repository):
             finally:
                 os.chdir(old_cwd)
 
         return self
 
     def __exit__(self, exc_type, exc_val, exc_tb):
         self._client.close()
 
-    def _run_in_client(self, args):
+    def _run(self, *args, **runargs):
         if not self._client.server:
-            raise Exception('active HgRepository context manager required')
-
+            return super(HgRepository, self)._run(*args, **runargs)
         return self._client.rawcommand(args)
 
     def sparse_checkout_present(self):
         # We assume a sparse checkout is enabled if the .hg/sparse file
         # has data. Strictly speaking, we should look for a requirement in
         # .hg/requires. But since the requirement is still experimental
         # as of Mercurial 4.3, it's probably more trouble than its worth
         # to verify it.
@@ -299,30 +298,29 @@ class HgRepository(Repository):
         self._run(*args)
 
     def forget_add_remove_files(self, path):
         self._run('forget', path)
 
     def get_files_in_working_directory(self):
         # Can return backslashes on Windows. Normalize to forward slashes.
         return list(p.replace('\\', '/') for p in
-                    self._run_in_client([b'files', b'-0']).split(b'\0')
-                    if p)
+                    self._run(b'files', b'-0').split(b'\0') if p)
 
     def working_directory_clean(self, untracked=False, ignored=False):
         args = [b'status', b'\0', b'--modified', b'--added', b'--removed',
                 b'--deleted']
         if untracked:
             args.append(b'--unknown')
         if ignored:
             args.append(b'--ignored')
 
         # If output is empty, there are no entries of requested status, which
         # means we are clean.
-        return not len(self._run_in_client(args).strip())
+        return not len(self._run(*args).strip())
 
 
 class GitRepository(Repository):
     '''An implementation of `Repository` for Git repositories.'''
     def __init__(self, path, git='git'):
         super(GitRepository, self).__init__(path, tool=git)
 
     @property
diff --git a/python/mozversioncontrol/test/conftest.py b/python/mozversioncontrol/test/conftest.py
new file mode 100644
--- /dev/null
+++ b/python/mozversioncontrol/test/conftest.py
@@ -0,0 +1,74 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+from __future__ import absolute_import
+
+import os
+import subprocess
+
+import pytest
+
+
+SETUP = {
+    'hg': [
+        """
+        echo "foo" > foo
+        echo "bar" > bar
+        hg init
+        hg add *
+        hg commit -m "Initial commit"
+        """,
+        """
+        echo "[paths]\ndefault = ../remoterepo" > .hg/hgrc
+        """,
+    ],
+    'git': [
+        """
+        echo "foo" > foo
+        echo "bar" > bar
+        git init
+        git add *
+        git commit -am "Initial commit"
+        """,
+        """
+        git remote add upstream ../remoterepo
+        git fetch upstream
+        git branch -u upstream/master
+        """,
+    ]
+}
+
+
+def shell(cmd):
+    subprocess.check_call(cmd, shell=True)
+
+
+@pytest.yield_fixture(params=['git', 'hg'])
+def repo(tmpdir, request):
+    vcs = request.param
+    steps = SETUP[vcs]
+
+    if hasattr(request.module, 'STEPS'):
+        steps.extend(request.module.STEPS[vcs])
+
+    # tmpdir and repo are py.path objects
+    # http://py.readthedocs.io/en/latest/path.html
+    repo = tmpdir.mkdir('repo')
+    repo.vcs = vcs
+
+    # This creates a step iterator. Each time next() is called
+    # on it, the next set of instructions will be executed.
+    repo.step = (shell(cmd) for cmd in steps)
+
+    oldcwd = os.getcwd()
+    os.chdir(repo.strpath)
+
+    next(repo.step)
+
+    repo.copy(tmpdir.join('remoterepo'))
+
+    next(repo.step)
+
+    yield repo
+    os.chdir(oldcwd)
diff --git a/python/mozversioncontrol/test/python.ini b/python/mozversioncontrol/test/python.ini
--- a/python/mozversioncontrol/test/python.ini
+++ b/python/mozversioncontrol/test/python.ini
@@ -1,5 +1,6 @@
 [DEFAULT]
 subsuite=mozversioncontrol
 skip-if = python == 3
 
+[test_context_manager.py]
 [test_workdir_outgoing.py]
diff --git a/python/mozversioncontrol/test/test_context_manager.py b/python/mozversioncontrol/test/test_context_manager.py
new file mode 100644
--- /dev/null
+++ b/python/mozversioncontrol/test/test_context_manager.py
@@ -0,0 +1,30 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+from __future__ import absolute_import
+
+import mozunit
+
+from mozversioncontrol import get_repository_object
+
+
+def test_context_manager(repo):
+    is_git = repo.vcs == 'git'
+    cmd = ['show', '--no-patch'] if is_git else ['tip']
+
+    vcs = get_repository_object(repo.strpath)
+    output_subprocess = vcs._run(*cmd)
+    assert is_git or vcs._client.server is None
+    assert "Initial commit" in output_subprocess
+
+    with vcs:
+        assert is_git or vcs._client.server is not None
+        output_client = vcs._run(*cmd)
+
+    assert is_git or vcs._client.server is None
+    assert output_subprocess == output_client
+
+
+if __name__ == '__main__':
+    mozunit.main()
diff --git a/python/mozversioncontrol/test/test_workdir_outgoing.py b/python/mozversioncontrol/test/test_workdir_outgoing.py
--- a/python/mozversioncontrol/test/test_workdir_outgoing.py
+++ b/python/mozversioncontrol/test/test_workdir_outgoing.py
@@ -1,122 +1,68 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import
 
 import os
-import subprocess
 
 import mozunit
-import pytest
 
 from mozversioncontrol import get_repository_object
 
 
-setup = {
+STEPS = {
     'hg': [
         """
-        echo "foo" > foo
-        echo "bar" > bar
-        hg init
-        hg add *
-        hg commit -m "Initial commit"
-        """,
-        """
-        echo "[paths]\ndefault = ../remoterepo" > .hg/hgrc
-        """,
-        """
         echo "bar" >> bar
         echo "baz" > baz
         hg add baz
         hg rm foo
         """,
         """
         hg commit -m "Remove foo; modify bar; add baz"
         """,
     ],
     'git': [
         """
-        echo "foo" > foo
-        echo "bar" > bar
-        git init
-        git add *
-        git commit -am "Initial commit"
-        """,
-        """
-        git remote add upstream ../remoterepo
-        git fetch upstream
-        git branch -u upstream/master
-        """,
-        """
         echo "bar" >> bar
         echo "baz" > baz
         git add baz
         git rm foo
         """,
         """
         git commit -am "Remove foo; modify bar; add baz"
         """
     ]
 }
 
 
-def shell(cmd):
-    subprocess.check_call(cmd, shell=True)
-
-
-@pytest.yield_fixture(params=['git', 'hg'])
-def repo(tmpdir, request):
-    vcs = request.param
-
-    # tmpdir and repo are py.path objects
-    # http://py.readthedocs.io/en/latest/path.html
-    repo = tmpdir.mkdir('repo')
-    repo.vcs = vcs
-
-    # This creates a setup iterator. Each time next() is called
-    # on it, the next set of instructions will be executed.
-    repo.setup = (shell(cmd) for cmd in setup[vcs])
-
-    oldcwd = os.getcwd()
-    os.chdir(repo.strpath)
-
-    next(repo.setup)
-
-    repo.copy(tmpdir.join('remoterepo'))
-
-    next(repo.setup)
-
-    yield repo
-    os.chdir(oldcwd)
-
-
 def assert_files(actual, expected):
     assert set(map(os.path.basename, actual)) == set(expected)
 
 
 def test_workdir_outgoing(repo):
     vcs = get_repository_object(repo.strpath)
     assert vcs.path == repo.strpath
 
     remotepath = '../remoterepo' if repo.vcs == 'hg' else 'upstream/master'
 
-    next(repo.setup)
+    next(repo.step)
 
     assert_files(vcs.get_changed_files('AM', 'all'), ['bar', 'baz'])
     if repo.vcs == 'git':
         assert_files(vcs.get_changed_files('AM', mode='staged'), ['baz'])
     elif repo.vcs == 'hg':
         assert_files(vcs.get_changed_files('AM', 'staged'), ['bar', 'baz'])
     assert_files(vcs.get_outgoing_files('AM'), [])
     assert_files(vcs.get_outgoing_files('AM', remotepath), [])
 
-    next(repo.setup)
+    next(repo.step)
 
     assert_files(vcs.get_changed_files('AM', 'all'), [])
     assert_files(vcs.get_changed_files('AM', 'staged'), [])
     assert_files(vcs.get_outgoing_files('AM'), ['bar', 'baz'])
     assert_files(vcs.get_outgoing_files('AM', remotepath), ['bar', 'baz'])
 
 
 if __name__ == '__main__':
