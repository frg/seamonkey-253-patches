# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1517383225 -3600
# Node ID 89714cb6e883b06aba04d3572238c97933002228
# Parent  5765953ba9b4925aab4add6449d4e363c613cd3d
Bug 1432963 - Fixing workers headers - part 5 - WorkerEventTarget in a separate file, r=smaug

diff --git a/dom/workers/WorkerEventTarget.cpp b/dom/workers/WorkerEventTarget.cpp
new file mode 100644
--- /dev/null
+++ b/dom/workers/WorkerEventTarget.cpp
@@ -0,0 +1,160 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#include "WorkerEventTarget.h"
+
+namespace mozilla {
+namespace dom {
+
+using namespace workers;
+
+namespace {
+
+class WrappedControlRunnable final : public WorkerControlRunnable
+{
+  nsCOMPtr<nsIRunnable> mInner;
+
+  ~WrappedControlRunnable()
+  {
+  }
+
+public:
+  WrappedControlRunnable(WorkerPrivate* aWorkerPrivate,
+                         already_AddRefed<nsIRunnable>&& aInner)
+    : WorkerControlRunnable(aWorkerPrivate, WorkerThreadUnchangedBusyCount)
+    , mInner(aInner)
+  {
+  }
+
+  virtual bool
+  PreDispatch(WorkerPrivate* aWorkerPrivate) override
+  {
+    // Silence bad assertions, this can be dispatched from any thread.
+    return true;
+  }
+
+  virtual void
+  PostDispatch(WorkerPrivate* aWorkerPrivate, bool aDispatchResult) override
+  {
+    // Silence bad assertions, this can be dispatched from any thread.
+  }
+
+  bool
+  WorkerRun(JSContext* aCx, WorkerPrivate* aWorkerPrivate) override
+  {
+    mInner->Run();
+    return true;
+  }
+
+  nsresult
+  Cancel() override
+  {
+    nsCOMPtr<nsICancelableRunnable> cr = do_QueryInterface(mInner);
+
+    // If the inner runnable is not cancellable, then just do the normal
+    // WorkerControlRunnable thing.  This will end up calling Run().
+    if (!cr) {
+      WorkerControlRunnable::Cancel();
+      return NS_OK;
+    }
+
+    // Otherwise call the inner runnable's Cancel() and treat this like
+    // a WorkerRunnable cancel.  We can't call WorkerControlRunnable::Cancel()
+    // in this case since that would result in both Run() and the inner
+    // Cancel() being called.
+    Unused << cr->Cancel();
+    return WorkerRunnable::Cancel();
+  }
+};
+
+} // anonymous namespace
+
+NS_IMPL_ISUPPORTS(WorkerEventTarget, nsIEventTarget, nsISerialEventTarget)
+
+WorkerEventTarget::WorkerEventTarget(WorkerPrivate* aWorkerPrivate,
+                                    Behavior aBehavior)
+  : mMutex("WorkerEventTarget")
+  , mWorkerPrivate(aWorkerPrivate)
+  , mBehavior(aBehavior)
+{
+  MOZ_DIAGNOSTIC_ASSERT(mWorkerPrivate);
+}
+
+void
+WorkerEventTarget::ForgetWorkerPrivate(WorkerPrivate* aWorkerPrivate)
+{
+  MutexAutoLock lock(mMutex);
+  MOZ_DIAGNOSTIC_ASSERT(!mWorkerPrivate || mWorkerPrivate == aWorkerPrivate);
+  mWorkerPrivate = nullptr;
+}
+
+NS_IMETHODIMP
+WorkerEventTarget::DispatchFromScript(nsIRunnable* aRunnable,
+                                      uint32_t aFlags)
+{
+  nsCOMPtr<nsIRunnable> runnable(aRunnable);
+  return Dispatch(runnable.forget(), aFlags);
+}
+
+NS_IMETHODIMP
+WorkerEventTarget::Dispatch(already_AddRefed<nsIRunnable> aRunnable,
+                            uint32_t aFlags)
+{
+  MutexAutoLock lock(mMutex);
+
+  if (!mWorkerPrivate) {
+    return NS_ERROR_FAILURE;
+  }
+
+  nsCOMPtr<nsIRunnable> runnable(aRunnable);
+
+  if (mBehavior == Behavior::Hybrid) {
+    RefPtr<WorkerRunnable> r =
+      mWorkerPrivate->MaybeWrapAsWorkerRunnable(runnable.forget());
+    if (r->Dispatch()) {
+      return NS_OK;
+    }
+
+    runnable = r.forget();
+  }
+
+  RefPtr<WorkerControlRunnable> r =
+    new WrappedControlRunnable(mWorkerPrivate, runnable.forget());
+  if (!r->Dispatch()) {
+    return NS_ERROR_FAILURE;
+  }
+
+  return NS_OK;
+}
+
+NS_IMETHODIMP
+WorkerEventTarget::DelayedDispatch(already_AddRefed<nsIRunnable>, uint32_t)
+{
+  return NS_ERROR_NOT_IMPLEMENTED;
+}
+
+NS_IMETHODIMP_(bool)
+WorkerEventTarget::IsOnCurrentThreadInfallible()
+{
+  MutexAutoLock lock(mMutex);
+
+  if (!mWorkerPrivate) {
+    return false;
+  }
+
+  return mWorkerPrivate->IsOnCurrentThread();
+}
+
+NS_IMETHODIMP
+WorkerEventTarget::IsOnCurrentThread(bool* aIsOnCurrentThread)
+{
+  MOZ_ASSERT(aIsOnCurrentThread);
+  *aIsOnCurrentThread = IsOnCurrentThreadInfallible();
+  return NS_OK;
+}
+
+} // dom namespace
+} // mozilla namespace
diff --git a/dom/workers/WorkerEventTarget.h b/dom/workers/WorkerEventTarget.h
new file mode 100644
--- /dev/null
+++ b/dom/workers/WorkerEventTarget.h
@@ -0,0 +1,59 @@
+/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
+/* vim: set ts=8 sts=2 et sw=2 tw=80: */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+#ifndef mozilla_dom_WorkerEventTarget_h
+#define mozilla_dom_WorkerEventTarget_h
+
+#include "nsISerialEventTarget.h"
+#include "mozilla/Mutex.h"
+
+namespace mozilla {
+namespace dom {
+
+namespace workers {
+class WorkerPrivate;
+}
+
+class WorkerEventTarget final : public nsISerialEventTarget
+{
+public:
+  // The WorkerEventTarget supports different dispatch behaviors:
+  //
+  // * Hybrid targets will attempt to dispatch as a normal runnable,
+  //   but fallback to a control runnable if that fails.  This is
+  //   often necessary for code that wants normal dispatch order, but
+  //   also needs to execute while the worker is shutting down (possibly
+  //   with a holder in place.)
+  //
+  // * ControlOnly targets will simply dispatch a control runnable.
+  enum class Behavior : uint8_t {
+    Hybrid,
+    ControlOnly
+  };
+
+private:
+  mozilla::Mutex mMutex;
+  workers::WorkerPrivate* mWorkerPrivate;
+  const Behavior mBehavior;
+
+  ~WorkerEventTarget() = default;
+
+public:
+  WorkerEventTarget(workers::WorkerPrivate* aWorkerPrivate,
+                    Behavior aBehavior);
+
+  void
+  ForgetWorkerPrivate(workers::WorkerPrivate* aWorkerPrivate);
+
+  NS_DECL_THREADSAFE_ISUPPORTS
+  NS_DECL_NSIEVENTTARGET
+  NS_DECL_NSISERIALEVENTTARGET
+};
+
+} // dom namespace
+} // mozilla namespace
+
+#endif // mozilla_dom_WorkerEventTarget_h
diff --git a/dom/workers/WorkerPrivate.cpp b/dom/workers/WorkerPrivate.cpp
--- a/dom/workers/WorkerPrivate.cpp
+++ b/dom/workers/WorkerPrivate.cpp
@@ -22,16 +22,17 @@
 #include "mozilla/dom/MessageEvent.h"
 #include "mozilla/dom/MessageEventBinding.h"
 #include "mozilla/dom/MessagePort.h"
 #include "mozilla/dom/MessagePortBinding.h"
 #include "mozilla/dom/nsCSPUtils.h"
 #include "mozilla/dom/Performance.h"
 #include "mozilla/dom/PerformanceStorageWorker.h"
 #include "mozilla/dom/PromiseDebugging.h"
+#include "mozilla/dom/WorkerBinding.h"
 #include "mozilla/ThreadEventQueue.h"
 #include "mozilla/ThrottledEventQueue.h"
 #include "mozilla/TimelineConsumers.h"
 #include "mozilla/WorkerTimelineMarker.h"
 #include "nsCycleCollector.h"
 #include "nsNetUtil.h"
 #include "nsIMemoryReporter.h"
 #include "nsIPermissionManager.h"
@@ -47,16 +48,17 @@
 #include "RuntimeService.h"
 #include "ScriptLoader.h"
 #include "mozilla/dom/ServiceWorkerEvents.h"
 #include "mozilla/dom/ServiceWorkerManager.h"
 #include "SharedWorker.h"
 #include "WorkerDebugger.h"
 #include "WorkerDebuggerManager.h"
 #include "WorkerError.h"
+#include "WorkerEventTarget.h"
 #include "WorkerNavigator.h"
 #include "WorkerRunnable.h"
 #include "WorkerScope.h"
 #include "WorkerThread.h"
 
 #ifdef DEBUG
 #include "nsThreadManager.h"
 #endif
@@ -968,190 +970,16 @@ public:
 
   virtual bool Notify(Status aStatus) override { return true; }
 };
 
 } /* anonymous namespace */
 
 NS_IMPL_ISUPPORTS_INHERITED0(TopLevelWorkerFinishedRunnable, Runnable)
 
-namespace {
-
-class WrappedControlRunnable final : public WorkerControlRunnable
-{
-  nsCOMPtr<nsIRunnable> mInner;
-
-  ~WrappedControlRunnable()
-  {
-  }
-
-public:
-  WrappedControlRunnable(WorkerPrivate* aWorkerPrivate,
-                         already_AddRefed<nsIRunnable>&& aInner)
-    : WorkerControlRunnable(aWorkerPrivate, WorkerThreadUnchangedBusyCount)
-    , mInner(aInner)
-  {
-  }
-
-  virtual bool
-  PreDispatch(WorkerPrivate* aWorkerPrivate) override
-  {
-    // Silence bad assertions, this can be dispatched from any thread.
-    return true;
-  }
-
-  virtual void
-  PostDispatch(WorkerPrivate* aWorkerPrivate, bool aDispatchResult) override
-  {
-    // Silence bad assertions, this can be dispatched from any thread.
-  }
-
-  bool
-  WorkerRun(JSContext* aCx, WorkerPrivate* aWorkerPrivate) override
-  {
-    mInner->Run();
-    return true;
-  }
-
-  nsresult
-  Cancel() override
-  {
-    nsCOMPtr<nsICancelableRunnable> cr = do_QueryInterface(mInner);
-
-    // If the inner runnable is not cancellable, then just do the normal
-    // WorkerControlRunnable thing.  This will end up calling Run().
-    if (!cr) {
-      WorkerControlRunnable::Cancel();
-      return NS_OK;
-    }
-
-    // Otherwise call the inner runnable's Cancel() and treat this like
-    // a WorkerRunnable cancel.  We can't call WorkerControlRunnable::Cancel()
-    // in this case since that would result in both Run() and the inner
-    // Cancel() being called.
-    Unused << cr->Cancel();
-    return WorkerRunnable::Cancel();
-  }
-};
-
-} // anonymous namespace
-
-BEGIN_WORKERS_NAMESPACE
-
-class WorkerEventTarget final : public nsISerialEventTarget
-{
-public:
-  // The WorkerEventTarget supports different dispatch behaviors:
-  //
-  // * Hybrid targets will attempt to dispatch as a normal runnable,
-  //   but fallback to a control runnable if that fails.  This is
-  //   often necessary for code that wants normal dispatch order, but
-  //   also needs to execute while the worker is shutting down (possibly
-  //   with a holder in place.)
-  //
-  // * ControlOnly targets will simply dispatch a control runnable.
-  enum class Behavior : uint8_t {
-    Hybrid,
-    ControlOnly
-  };
-
-private:
-  mozilla::Mutex mMutex;
-  WorkerPrivate* mWorkerPrivate;
-  const Behavior mBehavior;
-
-  ~WorkerEventTarget() = default;
-
-public:
-  WorkerEventTarget(WorkerPrivate* aWorkerPrivate,
-                           Behavior aBehavior)
-    : mMutex("WorkerEventTarget")
-    , mWorkerPrivate(aWorkerPrivate)
-    , mBehavior(aBehavior)
-  {
-    MOZ_DIAGNOSTIC_ASSERT(mWorkerPrivate);
-  }
-
-  void
-  ForgetWorkerPrivate(WorkerPrivate* aWorkerPrivate)
-  {
-    MutexAutoLock lock(mMutex);
-    MOZ_DIAGNOSTIC_ASSERT(!mWorkerPrivate || mWorkerPrivate == aWorkerPrivate);
-    mWorkerPrivate = nullptr;
-  }
-
-  NS_IMETHOD
-  DispatchFromScript(nsIRunnable* aRunnable, uint32_t aFlags) override
-  {
-    nsCOMPtr<nsIRunnable> runnable(aRunnable);
-    return Dispatch(runnable.forget(), aFlags);
-  }
-
-  NS_IMETHOD
-  Dispatch(already_AddRefed<nsIRunnable> aRunnable, uint32_t aFlags = NS_DISPATCH_NORMAL) override
-  {
-    MutexAutoLock lock(mMutex);
-
-    if (!mWorkerPrivate) {
-      return NS_ERROR_FAILURE;
-    }
-
-    nsCOMPtr<nsIRunnable> runnable(aRunnable);
-
-    if (mBehavior == Behavior::Hybrid) {
-      RefPtr<WorkerRunnable> r =
-        mWorkerPrivate->MaybeWrapAsWorkerRunnable(runnable.forget());
-      if (r->Dispatch()) {
-        return NS_OK;
-      }
-
-      runnable = r.forget();
-    }
-
-    RefPtr<WorkerControlRunnable> r = new WrappedControlRunnable(mWorkerPrivate,
-                                                                 runnable.forget());
-    if (!r->Dispatch()) {
-      return NS_ERROR_FAILURE;
-    }
-
-    return NS_OK;
-  }
-
-  NS_IMETHOD
-  DelayedDispatch(already_AddRefed<nsIRunnable>, uint32_t) override
-  {
-    return NS_ERROR_NOT_IMPLEMENTED;
-  }
-
-  NS_IMETHOD_(bool) IsOnCurrentThreadInfallible() override
-  {
-    MutexAutoLock lock(mMutex);
-
-    if (!mWorkerPrivate) {
-      return false;
-    }
-
-    return mWorkerPrivate->IsOnCurrentThread();
-  }
-
-  NS_IMETHOD
-  IsOnCurrentThread(bool* aIsOnCurrentThread) override
-  {
-    MOZ_ASSERT(aIsOnCurrentThread);
-    *aIsOnCurrentThread = IsOnCurrentThreadInfallible();
-    return NS_OK;
-  }
-
-  NS_DECL_THREADSAFE_ISUPPORTS
-};
-
-NS_IMPL_ISUPPORTS(WorkerEventTarget, nsIEventTarget,
-                                            nsISerialEventTarget)
-
-END_WORKERS_NAMESPACE
 
 template <class Derived>
 class WorkerPrivateParent<Derived>::EventTarget final
   : public nsISerialEventTarget
 {
   // This mutex protects mWorkerPrivate and must be acquired *before* the
   // WorkerPrivate's mutex whenever they must both be held.
   mozilla::Mutex mMutex;
diff --git a/dom/workers/WorkerPrivate.h b/dom/workers/WorkerPrivate.h
--- a/dom/workers/WorkerPrivate.h
+++ b/dom/workers/WorkerPrivate.h
@@ -34,28 +34,28 @@ class ClientInfo;
 class ClientSource;
 class Function;
 class MessagePort;
 class MessagePortIdentifier;
 class PerformanceStorage;
 class SharedWorker;
 class WorkerDebuggerGlobalScope;
 class WorkerErrorReport;
+class WorkerEventTarget;
 class WorkerGlobalScope;
 struct WorkerOptions;
 class WorkerThread;
 
 } // dom namespace
 } // mozilla namespace
 
 BEGIN_WORKERS_NAMESPACE
 
 class WorkerControlRunnable;
 class WorkerDebugger;
-class WorkerEventTarget;
 class WorkerRunnable;
 
 // SharedMutex is a small wrapper around an (internal) reference-counted Mutex
 // object. It exists to avoid changing a lot of code to use Mutex* instead of
 // Mutex&.
 class SharedMutex
 {
   typedef mozilla::Mutex Mutex;
diff --git a/dom/workers/moz.build b/dom/workers/moz.build
--- a/dom/workers/moz.build
+++ b/dom/workers/moz.build
@@ -5,29 +5,29 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 with Files("**"):
     BUG_COMPONENT = ("Core", "DOM: Workers")
 
 # Public stuff.
 EXPORTS.mozilla.dom += [
     'SharedWorker.h',
+    'WorkerLoadInfo.h',
     'WorkerLocation.h',
     'WorkerNavigator.h',
     'WorkerPrivate.h',
     'WorkerRunnable.h',
     'WorkerScope.h',
 ]
 
 EXPORTS.mozilla.dom.workers += [
     'RuntimeService.h',
     'WorkerCommon.h',
     'WorkerDebugger.h',
     'WorkerDebuggerManager.h',
-    'WorkerLoadInfo.h',
 ]
 
 # Stuff needed for the bindings, not really public though.
 EXPORTS.mozilla.dom.workers.bindings += [
     'WorkerHolder.h',
     'WorkerHolderToken.h',
 ]
 
@@ -44,16 +44,17 @@ UNIFIED_SOURCES += [
     'Principal.cpp',
     'RegisterBindings.cpp',
     'RuntimeService.cpp',
     'ScriptLoader.cpp',
     'SharedWorker.cpp',
     'WorkerDebugger.cpp',
     'WorkerDebuggerManager.cpp',
     'WorkerError.cpp',
+    'WorkerEventTarget.cpp',
     'WorkerHolder.cpp',
     'WorkerHolderToken.cpp',
     'WorkerLoadInfo.cpp',
     'WorkerLocation.cpp',
     'WorkerNavigator.cpp',
     'WorkerPrivate.cpp',
     'WorkerRunnable.cpp',
     'WorkerScope.cpp',
