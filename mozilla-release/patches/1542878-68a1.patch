# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1555511479 0
# Node ID 30271e4881e7a27c75e5490f99908d2e764e8fa4
# Parent  d2ec3222ba6d9e1e902dd4fb9fa6e99b2f16ac65
Bug 1542878 - Check all the cbindgen executables before failing configure. r=froydnj

Before this patch, we first find an executable, then check the version. So if
the first executable we find is outdated, we won't look for others.

Instead, check each of them for different versions manually. This will also
unblock bug 1540533, since at that point we know that we'll be able to find
a cbindgen with the right version.

Differential Revision: https://phabricator.services.mozilla.com/D27890

diff --git a/build/moz.configure/bindgen.configure b/build/moz.configure/bindgen.configure
--- a/build/moz.configure/bindgen.configure
+++ b/build/moz.configure/bindgen.configure
@@ -1,40 +1,70 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
-# cbindgen is needed by the style system build.
-cbindgen = check_prog('CBINDGEN', ['cbindgen'], paths=toolchain_search_path,
-                      when=depends(build_project)
-                      (lambda build_project: build_project != 'js'))
+# cbindgen is needed by the style system build and webrender.
+cbindgen_is_needed = depends(build_project)(lambda build_project: build_project != 'js')
+
+option(env='CBINDGEN', nargs=1, when=cbindgen_is_needed,
+       help='Path to cbindgen')
 
 
-@depends_if(cbindgen)
-@checking('cbindgen version')
 @imports(_from='textwrap', _import='dedent')
-def cbindgen_version(cbindgen):
+def check_cbindgen_version(cbindgen, fatal=False):
+    log.debug("trying cbindgen: %s" % cbindgen)
+
     cbindgen_min_version = Version('0.6.2')
 
     # cbindgen x.y.z
     version = Version(check_cmd_output(cbindgen, '--version').strip().split(" ")[1])
+    log.debug("%s has version %s" % (cbindgen, version))
+    if version >= cbindgen_min_version:
+        return True
+    if not fatal:
+        return False
 
-    if version < cbindgen_min_version:
-        die(dedent('''\
-        cbindgen version {} is too old. At least version {} is required.
+    die(dedent('''\
+    cbindgen version {} is too old. At least version {} is required.
+
+    Please update using 'cargo install cbindgen --force' or running
+    './mach bootstrap', after removing the existing executable located at
+    {}.
+    '''.format(version, cbindgen_min_version, cbindgen)))
+
 
-        Please update using 'cargo install cbindgen --force' or running
-        './mach bootstrap', after removing the existing executable located at
-        {}.
-        '''.format(version, cbindgen_min_version, cbindgen)))
+@depends_if('CBINDGEN', toolchain_search_path, when=cbindgen_is_needed)
+@checking('for cbindgen')
+@imports(_from='textwrap', _import='dedent')
+def cbindgen(cbindgen_override, toolchain_search_path):
+    if cbindgen_override:
+        check_cbindgen_version(cbindgen_override[0], fatal=True)
+        return cbindgen_override[0]
 
-    return version
+    candidates = []
+    for path in toolchain_search_path:
+        candidate = find_program('cbindgen', [path])
+        if not candidate:
+            continue
+        if check_cbindgen_version(candidate):
+            return candidate
+        candidates.append(candidate)
 
+    if not candidates:
+        raise FatalCheckError(dedent('''\
+        Cannot find cbindgen. Please run `mach bootstrap`,
+        `cargo install cbindgen`, ensure that `cbindgen` is on your PATH,
+        or point at an executable with `CBINDGEN`.
+        '''))
+    check_cbindgen_version(candidates[0], fatal=True)
+
+set_config('CBINDGEN', cbindgen)
 
 # Bindgen can use rustfmt to format Rust file, but it's not required.
 js_option(env='RUSTFMT', nargs=1, help='Path to the rustfmt program')
 
 rustfmt = check_prog('RUSTFMT', ['rustfmt'], paths=toolchain_search_path,
                      input='RUSTFMT', allow_missing=True)
 
 
