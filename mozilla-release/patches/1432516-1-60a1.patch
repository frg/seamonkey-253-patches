# HG changeset patch
# User Ryan Hunt <rhunt@eqrion.net>
# Date 1516744333 21600
# Node ID b12026769193229191c1abc4e73c540c8cbe28f2
# Parent  e9ad7c0aaa60860006c72405a18704ff398f4498
Add tiling and paint worker count information to about:support. (bug 1432516, r=milan)

diff --git a/gfx/layers/PaintThread.cpp b/gfx/layers/PaintThread.cpp
--- a/gfx/layers/PaintThread.cpp
+++ b/gfx/layers/PaintThread.cpp
@@ -121,16 +121,20 @@ PaintThread::Release()
 void
 PaintThread::AddRef()
 {
 }
 
 /* static */ int32_t
 PaintThread::CalculatePaintWorkerCount()
 {
+  if (!gfxPlatform::GetPlatform()->UsesTiling()) {
+    return 0;
+  }
+
   int32_t cpuCores = 1;
   nsCOMPtr<nsIPropertyBag2> systemInfo = do_GetService(NS_SYSTEMINFO_CONTRACTID);
   if (systemInfo) {
     nsresult rv = systemInfo->GetPropertyAsInt32(NS_LITERAL_STRING("cpucores"), &cpuCores);
     if (NS_FAILED(rv)) {
       cpuCores = 1;
     }
   }
diff --git a/gfx/layers/PaintThread.h b/gfx/layers/PaintThread.h
--- a/gfx/layers/PaintThread.h
+++ b/gfx/layers/PaintThread.h
@@ -295,21 +295,21 @@ public:
 
   // Sync Runnables need threads to be ref counted,
   // But this thread lives through the whole process.
   // We're only temporarily using sync runnables so
   // Override release/addref but don't do anything.
   void Release();
   void AddRef();
 
+  static int32_t CalculatePaintWorkerCount();
+
 private:
   PaintThread();
 
-  static int32_t CalculatePaintWorkerCount();
-
   bool Init();
   void ShutdownOnPaintThread();
   void InitOnPaintThread();
 
   void AsyncPrepareBuffer(CompositorBridgeChild* aBridge,
                           CapturedBufferState* aState);
   void AsyncPaintContents(CompositorBridgeChild* aBridge,
                           CapturedPaintState* aState,
diff --git a/toolkit/content/aboutSupport.js b/toolkit/content/aboutSupport.js
--- a/toolkit/content/aboutSupport.js
+++ b/toolkit/content/aboutSupport.js
@@ -455,17 +455,19 @@ var snapshotFormatters = {
     addRowFromKey("features", "webgl1Extensions");
     addRowFromKey("features", "webgl2WSIInfo");
     addRowFromKey("features", "webgl2Renderer");
     addRowFromKey("features", "webgl2Version");
     addRowFromKey("features", "webgl2DriverExtensions");
     addRowFromKey("features", "webgl2Extensions");
     addRowFromKey("features", "supportsHardwareH264", "hardwareH264");
     addRowFromKey("features", "direct2DEnabled", "#Direct2D");
+    addRowFromKey("features", "usesTiling");
     addRowFromKey("features", "offMainThreadPaintEnabled");
+    addRowFromKey("features", "offMainThreadPaintWorkerCount");
 
     if ("directWriteEnabled" in data) {
       let message = data.directWriteEnabled;
       if ("directWriteVersion" in data)
         message += " (" + data.directWriteVersion + ")";
       addRow("features", "#DirectWrite", message);
       delete data.directWriteEnabled;
       delete data.directWriteVersion;
diff --git a/toolkit/locales/en-US/chrome/global/aboutSupport.properties b/toolkit/locales/en-US/chrome/global/aboutSupport.properties
--- a/toolkit/locales/en-US/chrome/global/aboutSupport.properties
+++ b/toolkit/locales/en-US/chrome/global/aboutSupport.properties
@@ -102,17 +102,19 @@ bugLink = bug %1$S
 unknownFailure = Blocklisted; failure code %1$S
 d3d11layersCrashGuard = D3D11 Compositor
 d3d11videoCrashGuard = D3D11 Video Decoder
 d3d9videoCrashGuard = D3D9 Video Decoder
 glcontextCrashGuard = OpenGL
 resetOnNextRestart = Reset on Next Restart
 gpuProcessKillButton = Terminate GPU Process
 gpuDeviceResetButton = Trigger Device Reset
+usesTiling = Uses Tiling
 offMainThreadPaintEnabled = Off Main Thread Painting Enabled
+offMainThreadPaintWorkerCount = Off Main Thread Painting Worker Count
 
 audioBackend = Audio Backend
 maxAudioChannels = Max Channels
 channelLayout = Preferred Channel Layout
 sampleRate = Preferred Sample Rate
 
 minLibVersions = Expected minimum version
 loadedLibVersions = Version in use
diff --git a/toolkit/modules/Troubleshoot.jsm b/toolkit/modules/Troubleshoot.jsm
--- a/toolkit/modules/Troubleshoot.jsm
+++ b/toolkit/modules/Troubleshoot.jsm
@@ -457,17 +457,19 @@ var dataProviders = {
       adapterDriverVersion2: "driverVersion2",
       adapterDriverDate2: "driverDate2",
       isGPU2Active: null,
 
       D2DEnabled: "direct2DEnabled",
       DWriteEnabled: "directWriteEnabled",
       DWriteVersion: "directWriteVersion",
       cleartypeParameters: "clearTypeParameters",
+      UsesTiling: "usesTiling",
       OffMainThreadPaintEnabled: "offMainThreadPaintEnabled",
+      OffMainThreadPaintWorkerCount: "offMainThreadPaintWorkerCount",
     };
 
     for (let prop in gfxInfoProps) {
       try {
         data[gfxInfoProps[prop] || prop] = gfxInfo[prop];
       } catch (e) {}
     }
 
diff --git a/widget/GfxInfoBase.cpp b/widget/GfxInfoBase.cpp
--- a/widget/GfxInfoBase.cpp
+++ b/widget/GfxInfoBase.cpp
@@ -27,16 +27,17 @@
 #include "nsXULAppAPI.h"
 #include "nsIXULAppInfo.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/dom/ContentChild.h"
 #include "mozilla/gfx/2D.h"
 #include "mozilla/gfx/GPUProcessManager.h"
 #include "mozilla/gfx/Logging.h"
 #include "mozilla/gfx/gfxVars.h"
+#include "mozilla/layers/PaintThread.h"
 #include "MediaPrefs.h"
 #include "gfxPrefs.h"
 #include "gfxPlatform.h"
 #include "gfxConfig.h"
 #include "DriverCrashGuard.h"
 
 using namespace mozilla::widget;
 using namespace mozilla;
@@ -1487,23 +1488,41 @@ GfxInfoBase::GetActiveCrashGuards(JSCont
 NS_IMETHODIMP
 GfxInfoBase::GetWebRenderEnabled(bool* aWebRenderEnabled)
 {
   *aWebRenderEnabled = gfxVars::UseWebRender();
   return NS_OK;
 }
 
 NS_IMETHODIMP
+GfxInfoBase::GetUsesTiling(bool* aUsesTiling)
+{
+  *aUsesTiling = gfxPlatform::GetPlatform()->UsesTiling();
+  return NS_OK;
+}
+
+NS_IMETHODIMP
 GfxInfoBase::GetOffMainThreadPaintEnabled(bool* aOffMainThreadPaintEnabled)
 {
   *aOffMainThreadPaintEnabled = gfxConfig::IsEnabled(Feature::OMTP);
   return NS_OK;
 }
 
 NS_IMETHODIMP
+GfxInfoBase::GetOffMainThreadPaintWorkerCount(int32_t* aOffMainThreadPaintWorkerCount)
+{
+  if (gfxConfig::IsEnabled(Feature::OMTP)) {
+    *aOffMainThreadPaintWorkerCount = layers::PaintThread::CalculatePaintWorkerCount();
+  } else {
+    *aOffMainThreadPaintWorkerCount = 0;
+  }
+  return NS_OK;
+}
+
+NS_IMETHODIMP
 GfxInfoBase::GetIsHeadless(bool* aIsHeadless)
 {
   *aIsHeadless = gfxPlatform::IsHeadless();
   return NS_OK;
 }
 
 NS_IMETHODIMP
 GfxInfoBase::GetContentBackend(nsAString & aContentBackend)
diff --git a/widget/GfxInfoBase.h b/widget/GfxInfoBase.h
--- a/widget/GfxInfoBase.h
+++ b/widget/GfxInfoBase.h
@@ -56,17 +56,19 @@ public:
   NS_IMETHOD GetInfo(JSContext*, JS::MutableHandle<JS::Value>) override;
   NS_IMETHOD GetFeatures(JSContext*, JS::MutableHandle<JS::Value>) override;
   NS_IMETHOD GetFeatureLog(JSContext*, JS::MutableHandle<JS::Value>) override;
   NS_IMETHOD GetActiveCrashGuards(JSContext*, JS::MutableHandle<JS::Value>) override;
   NS_IMETHOD GetContentBackend(nsAString & aContentBackend) override;
   NS_IMETHOD GetUsingGPUProcess(bool *aOutValue) override;
   NS_IMETHOD GetWebRenderEnabled(bool* aWebRenderEnabled) override;
   NS_IMETHOD GetIsHeadless(bool* aIsHeadless) override;
+  NS_IMETHOD GetUsesTiling(bool* aUsesTiling) override;
   NS_IMETHOD GetOffMainThreadPaintEnabled(bool* aOffMainThreadPaintEnabled) override;
+  NS_IMETHOD GetOffMainThreadPaintWorkerCount(int32_t* aOffMainThreadPaintWorkerCount) override;
 
   // Initialization function. If you override this, you must call this class's
   // version of Init first.
   // We need Init to be called separately from the constructor so we can
   // register as an observer after all derived classes have been constructed
   // and we know we have a non-zero refcount.
   // Ideally, Init() would be void-return, but the rules of
   // NS_GENERIC_FACTORY_CONSTRUCTOR_INIT require it be nsresult return.
diff --git a/widget/nsIGfxInfo.idl b/widget/nsIGfxInfo.idl
--- a/widget/nsIGfxInfo.idl
+++ b/widget/nsIGfxInfo.idl
@@ -20,17 +20,19 @@ interface nsIGfxInfo : nsISupports
   readonly attribute DOMString cleartypeParameters;
 
   /*
    * These are valid across all platforms.
    */
   readonly attribute DOMString ContentBackend;
   readonly attribute boolean WebRenderEnabled;
   readonly attribute boolean isHeadless;
+  readonly attribute boolean UsesTiling;
   readonly attribute boolean OffMainThreadPaintEnabled;
+  readonly attribute long OffMainThreadPaintWorkerCount;
 
   // XXX: Switch to a list of devices, rather than explicitly numbering them.
 
   /**
    * The name of the display adapter.
    */
   readonly attribute DOMString adapterDescription;
   readonly attribute DOMString adapterDescription2;
