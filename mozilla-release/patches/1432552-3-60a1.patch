# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1517232251 0
# Node ID fc99d0e42468b22f4bcd2dd6d1394f8cb4f32862
# Parent  9b6d1dec8ab8ea0f8fab141397e917f4b084d976
Bug 1432552 - patch 3 - DirectWrite and macOS font back-end implementation of getVariationInstances. r=dholbert

diff --git a/gfx/thebes/gfxDWriteFontList.cpp b/gfx/thebes/gfxDWriteFontList.cpp
--- a/gfx/thebes/gfxDWriteFontList.cpp
+++ b/gfx/thebes/gfxDWriteFontList.cpp
@@ -644,16 +644,23 @@ gfxDWriteFontEntry::GetVariationAxes(nsT
         }
         axis.mMinValue = ranges[i].minValue;
         axis.mMaxValue = ranges[i].maxValue;
         axis.mDefaultValue = defaultValues[i].value;
         aAxes.AppendElement(axis);
     }
 }
 
+void
+gfxDWriteFontEntry::GetVariationInstances(
+    nsTArray<gfxFontVariationInstance>& aInstances)
+{
+    gfxFontUtils::GetVariationInstances(this, aInstances);
+}
+
 gfxFont *
 gfxDWriteFontEntry::CreateFontInstance(const gfxFontStyle* aFontStyle,
                                        bool aNeedsBold)
 {
     DWRITE_FONT_SIMULATIONS sims =
         aNeedsBold ? DWRITE_FONT_SIMULATIONS_BOLD : DWRITE_FONT_SIMULATIONS_NONE;
     if (HasVariations() && !aFontStyle->variationSettings.IsEmpty()) {
         // If we need to apply variations, we can't use the cached mUnscaledFont
diff --git a/gfx/thebes/gfxDWriteFontList.h b/gfx/thebes/gfxDWriteFontList.h
--- a/gfx/thebes/gfxDWriteFontList.h
+++ b/gfx/thebes/gfxDWriteFontList.h
@@ -186,16 +186,17 @@ public:
     virtual hb_blob_t* GetFontTable(uint32_t aTableTag) override;
 
     nsresult ReadCMAP(FontInfoData *aFontInfoData = nullptr);
 
     bool IsCJKFont();
 
     bool HasVariations() override;
     void GetVariationAxes(nsTArray<gfxFontVariationAxis>& aAxes) override;
+    void GetVariationInstances(nsTArray<gfxFontVariationInstance>& aInstances) override;
 
     void SetForceGDIClassic(bool aForce) { mForceGDIClassic = aForce; }
     bool GetForceGDIClassic() { return mForceGDIClassic; }
 
     virtual void AddSizeOfExcludingThis(mozilla::MallocSizeOf aMallocSizeOf,
                                         FontListSizes* aSizes) const;
     virtual void AddSizeOfIncludingThis(mozilla::MallocSizeOf aMallocSizeOf,
                                         FontListSizes* aSizes) const;
diff --git a/gfx/thebes/gfxFontUtils.cpp b/gfx/thebes/gfxFontUtils.cpp
--- a/gfx/thebes/gfxFontUtils.cpp
+++ b/gfx/thebes/gfxFontUtils.cpp
@@ -2,16 +2,18 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/BinarySearch.h"
 
 #include "gfxFontUtils.h"
+#include "gfxFontEntry.h"
+#include "gfxFontVariations.h"
 
 #include "nsServiceManagerUtils.h"
 
 #include "mozilla/Preferences.h"
 #include "mozilla/Services.h"
 #include "mozilla/BinarySearch.h"
 #include "mozilla/Sprintf.h"
 
@@ -1798,16 +1800,155 @@ gfxFontUtils::GetColorGlyphLayers(hb_blo
                                                       color->blue / 255.0,
                                                       color->alpha / 255.0));
         }
         layer++;
     }
     return true;
 }
 
+void
+gfxFontUtils::GetVariationInstances(gfxFontEntry* aFontEntry,
+                                    nsTArray<gfxFontVariationInstance>& aInstances)
+{
+    MOZ_ASSERT(aInstances.IsEmpty());
+
+    if (!aFontEntry->HasVariations()) {
+        return;
+    }
+
+    // Some platforms don't offer a simple API to return the list of instances,
+    // so we have to interpret the 'fvar' table ourselves.
+
+    // https://www.microsoft.com/typography/otspec/fvar.htm#fvarHeader
+    struct FvarHeader {
+        AutoSwap_PRUint16 majorVersion;
+        AutoSwap_PRUint16 minorVersion;
+        AutoSwap_PRUint16 axesArrayOffset;
+        AutoSwap_PRUint16 reserved;
+        AutoSwap_PRUint16 axisCount;
+        AutoSwap_PRUint16 axisSize;
+        AutoSwap_PRUint16 instanceCount;
+        AutoSwap_PRUint16 instanceSize;
+    };
+
+    // https://www.microsoft.com/typography/otspec/fvar.htm#variationAxisRecord
+    struct AxisRecord {
+        AutoSwap_PRUint32 axisTag;
+        AutoSwap_PRInt32  minValue;
+        AutoSwap_PRInt32  defaultValue;
+        AutoSwap_PRInt32  maxValue;
+        AutoSwap_PRUint16 flags;
+        AutoSwap_PRUint16 axisNameID;
+    };
+
+    // https://www.microsoft.com/typography/otspec/fvar.htm#instanceRecord
+    struct InstanceRecord {
+        AutoSwap_PRUint16 subfamilyNameID;
+        AutoSwap_PRUint16 flags;
+        AutoSwap_PRInt32  coordinates[1]; // variable-size array [axisCount]
+        // The variable-length 'coordinates' array may be followed by an
+        // optional extra field 'postScriptNameID'. We can't directly
+        // represent this in the struct, because its offset varies depending
+        // on the number of axes present.
+        // (Not currently used by our code here anyhow.)
+    //  AutoSwap_PRUint16 postScriptNameID;
+    };
+
+    // Helper to ensure we free a font table when we return.
+    class AutoHBBlob {
+    public:
+        explicit AutoHBBlob(hb_blob_t* aBlob) : mBlob(aBlob)
+        { }
+
+        ~AutoHBBlob() {
+            if (mBlob) {
+                hb_blob_destroy(mBlob);
+            }
+        }
+
+        operator hb_blob_t* () { return mBlob; }
+
+    private:
+        hb_blob_t* const mBlob;
+    };
+
+    // Load the two font tables we need as harfbuzz blobs; if either is absent,
+    // just bail out.
+    AutoHBBlob fvarTable(aFontEntry->GetFontTable(TRUETYPE_TAG('f','v','a','r')));
+    AutoHBBlob nameTable(aFontEntry->GetFontTable(TRUETYPE_TAG('n','a','m','e')));
+    if (!fvarTable || !nameTable) {
+        return;
+    }
+    unsigned int len;
+    const char* data = hb_blob_get_data(fvarTable, &len);
+    if (len < sizeof(FvarHeader)) {
+        return;
+    }
+    // Read the fields of the table header; bail out if it looks broken.
+    auto fvar = reinterpret_cast<const FvarHeader*>(data);
+    if (uint16_t(fvar->majorVersion) != 1 ||
+        uint16_t(fvar->minorVersion) != 0 ||
+        uint16_t(fvar->reserved) != 2) {
+        return;
+    }
+    uint16_t axisCount = fvar->axisCount;
+    uint16_t axisSize = fvar->axisSize;
+    uint16_t instanceCount = fvar->instanceCount;
+    uint16_t instanceSize = fvar->instanceSize;
+    if (axisCount == 0 || // no axes?
+        // https://www.microsoft.com/typography/otspec/fvar.htm#axisSize
+        axisSize != 20 || // required value for current table version
+        instanceCount == 0 || // no instances?
+        // https://www.microsoft.com/typography/otspec/fvar.htm#instanceSize
+        (instanceSize != axisCount * sizeof(int32_t) + 4 &&
+         instanceSize != axisCount * sizeof(int32_t) + 6)) {
+        return;
+    }
+    // Check that axis array will not exceed table size
+    uint16_t axesOffset = fvar->axesArrayOffset;
+    if (axesOffset + uint32_t(axisCount) * axisSize > len) {
+        return;
+    }
+    // Get pointer to the array of axis records
+    auto axes = reinterpret_cast<const AxisRecord*>(data + axesOffset);
+    // Get address of instance array, and check it doesn't overflow table size.
+    // https://www.microsoft.com/typography/otspec/fvar.htm#axisAndInstanceArrays
+    auto instData = data + axesOffset + axisCount * axisSize;
+    if (instData + uint32_t(instanceCount) * instanceSize > data + len) {
+        return;
+    }
+    aInstances.SetCapacity(instanceCount);
+    for (unsigned i = 0; i < instanceCount; ++i, instData += instanceSize) {
+        // Typed pointer to the current instance record, to read its fields.
+        auto inst = reinterpret_cast<const InstanceRecord*>(instData);
+        // Pointer to the coordinates array within the instance record.
+        // This array has axisCount elements, and is included in instanceSize
+        // (which depends on axisCount, and was validated above) so we know
+        // access to coords[j] below will not be outside the table bounds.
+        auto coords = &inst->coordinates[0];
+        gfxFontVariationInstance instance;
+        uint16_t nameID = inst->subfamilyNameID;
+        nsresult rv =
+            ReadCanonicalName(nameTable, nameID, instance.mName);
+        if (NS_FAILED(rv)) {
+            // If no name was available for the instance, ignore it.
+            continue;
+        }
+        instance.mValues.SetCapacity(axisCount);
+        for (unsigned j = 0; j < axisCount; ++j) {
+            gfxFontVariationValue value;
+            value.mAxis = axes[j].axisTag;
+            value.mValue = int32_t(coords[j]) / 65536.0;
+            instance.mValues.AppendElement(value);
+        }
+        aInstances.AppendElement(instance);
+    }
+}
+
 #ifdef XP_WIN
 
 /* static */
 bool
 gfxFontUtils::IsCffFont(const uint8_t* aFontData)
 {
     // this is only called after aFontData has passed basic validation,
     // so we know there is enough data present to allow us to read the version!
diff --git a/gfx/thebes/gfxFontUtils.h b/gfx/thebes/gfxFontUtils.h
--- a/gfx/thebes/gfxFontUtils.h
+++ b/gfx/thebes/gfxFontUtils.h
@@ -21,16 +21,18 @@
 /* Bug 341128 - w32api defines min/max which causes problems with <bitset> */
 #ifdef __MINGW32__
 #undef min
 #undef max
 #endif
 
 typedef struct hb_blob_t hb_blob_t;
 
+struct gfxFontVariationInstance;
+
 class gfxSparseBitSet {
 private:
     enum { BLOCK_SIZE = 32 };   // ==> 256 codepoints per block
     enum { BLOCK_SIZE_BITS = BLOCK_SIZE * 8 };
     enum { BLOCK_INDEX_SHIFT = 8 };
 
     struct Block {
         Block(const Block& aBlock) { memcpy(mBits, aBlock.mBits, sizeof(mBits)); }
@@ -995,16 +997,25 @@ public:
     static bool ValidateColorGlyphs(hb_blob_t* aCOLR, hb_blob_t* aCPAL);
     static bool GetColorGlyphLayers(hb_blob_t* aCOLR,
                                     hb_blob_t* aCPAL,
                                     uint32_t aGlyphId,
                                     const mozilla::gfx::Color& aDefaultColor,
                                     nsTArray<uint16_t> &aGlyphs,
                                     nsTArray<mozilla::gfx::Color> &aColors);
 
+    // Helper used to implement gfxFontEntry::GetVariationInstances for
+    // platforms where the native font APIs don't provide the info we want
+    // in a convenient form.
+    // (Not used on platforms -- currently, freetype -- where the font APIs
+    // expose variation instance details directly.)
+    static void
+    GetVariationInstances(gfxFontEntry* aFontEntry,
+                          nsTArray<gfxFontVariationInstance>& aInstances);
+
 protected:
     friend struct MacCharsetMappingComparator;
 
     static nsresult
     ReadNames(const char *aNameData, uint32_t aDataLen, uint32_t aNameID,
               int32_t aLangID, int32_t aPlatformID, nsTArray<nsString>& aNames);
 
     // convert opentype name-table platform/encoding/language values to an
diff --git a/gfx/thebes/gfxMacPlatformFontList.h b/gfx/thebes/gfxMacPlatformFontList.h
--- a/gfx/thebes/gfxMacPlatformFontList.h
+++ b/gfx/thebes/gfxMacPlatformFontList.h
@@ -58,16 +58,17 @@ public:
                                 FontListSizes* aSizes) const override;
 
     nsresult ReadCMAP(FontInfoData *aFontInfoData = nullptr) override;
 
     bool RequiresAATLayout() const { return mRequiresAAT; }
 
     bool HasVariations() override;
     void GetVariationAxes(nsTArray<gfxFontVariationAxis>& aVariationAxes) override;
+    void GetVariationInstances(nsTArray<gfxFontVariationInstance>& aInstances) override;
 
     bool IsCFF();
 
     // Return true if the font has a 'trak' table (and we can successfully
     // interpret it), otherwise false. This will load and cache the table
     // the first time it is called.
     bool HasTrackingTable();
 
diff --git a/gfx/thebes/gfxMacPlatformFontList.mm b/gfx/thebes/gfxMacPlatformFontList.mm
--- a/gfx/thebes/gfxMacPlatformFontList.mm
+++ b/gfx/thebes/gfxMacPlatformFontList.mm
@@ -337,16 +337,22 @@ MacOSFontEntry::GetVariationAxes(nsTArra
             axis.mMaxValue = maxValue;
             axis.mDefaultValue = defaultValue;
             aVariationAxes.AppendElement(axis);
         }
         CFRelease(axes);
     }
 }
 
+void
+MacOSFontEntry::GetVariationInstances(nsTArray<gfxFontVariationInstance>& aInstances)
+{
+    gfxFontUtils::GetVariationInstances(this, aInstances);
+}
+
 bool
 MacOSFontEntry::IsCFF()
 {
     if (!mIsCFFInitialized) {
         mIsCFFInitialized = true;
         mIsCFF = HasFontTable(TRUETYPE_TAG('C','F','F',' '));
     }
 
