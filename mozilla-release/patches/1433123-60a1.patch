# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1516890445 -3600
# Node ID 586c8597bb3476f032191c7c743d245dabc5041f
# Parent  3d609272ee6d558f13641e920dfda7ba805df9bd
Bug 1433123 - Fix console launchpad; r=Honza.

Updating to latest devtools-launchpad add adding the
disablePostCSS option fixes the postCSS errors.
Remove the netmonitor from babelExclude and add the
rewrite-lazy-getter loader fixes the issues about the use
of the object spread operator.

MozReview-Commit-ID: 4wRHuR4q7fz

diff --git a/devtools/client/webconsole/.babelrc b/devtools/client/webconsole/.babelrc
--- a/devtools/client/webconsole/.babelrc
+++ b/devtools/client/webconsole/.babelrc
@@ -1,10 +1,12 @@
 {
   "env": {
     "test": {
       "presets": ["es2015", "es2017"]
     }
   },
   "plugins": [
-    "transform-object-rest-spread"
+    "transform-react-jsx",
+    "transform-object-rest-spread",
+    "transform-flow-strip-types"
   ]
 }
diff --git a/devtools/client/webconsole/configs/development.json b/devtools/client/webconsole/configs/development.json
--- a/devtools/client/webconsole/configs/development.json
+++ b/devtools/client/webconsole/configs/development.json
@@ -7,29 +7,19 @@
   "dir": "ltr",
   "features": {
   },
   "logging": {
     "client": false,
     "firefoxProxy": false,
     "actions": false
   },
-  "chrome": {
-    "debug": false,
-    "host": "localhost",
-    "port": 9222
-  },
-  "node": {
-    "debug": false,
-    "host": "localhost",
-    "port": 9229
-  },
   "firefox": {
     "webSocketConnection": false,
-    "proxyHost": "localhost:9000",
-    "webSocketHost": "localhost:6080",
-    "mcPath": "./firefox"
+    "host": "localhost",
+    "webSocketPort": 8116,
+    "tcpPort": 6080
   },
   "development": {
     "serverPort": 8000,
     "examplesPort": 7999
   }
 }
diff --git a/devtools/client/webconsole/package.json b/devtools/client/webconsole/package.json
--- a/devtools/client/webconsole/package.json
+++ b/devtools/client/webconsole/package.json
@@ -7,22 +7,25 @@
   "scripts": {
     "preinstall": "cd ../netmonitor && npm install && cd ../webconsole",
     "start": "cross-env NODE_ENV=production node bin/dev-server",
     "dev": "node bin/dev-server",
     "test": "cross-env NODE_ENV=test NODE_PATH=../../../ mocha new-console-output/test/**/*.test.js --compilers js:babel-register -r jsdom-global/register -r ./new-console-output/test/require-helper.js"
   },
   "dependencies": {
     "amd-loader": "0.0.5",
+    "babel-plugin-transform-flow-strip-types": "^6.22.0",
+    "babel-plugin-transform-react-jsx": "^6.24.1",
+    "babel-plugin-transform-object-rest-spread": "^6.26.0",
     "babel-preset-es2015": "^6.6.0",
     "babel-preset-es2017": "^6.24.1",
     "babel-register": "^6.24.0",
     "cross-env": "^3.1.3",
     "devtools-config": "0.0.12",
-    "devtools-launchpad": "0.0.96",
+    "devtools-launchpad": "0.0.115",
     "devtools-modules": "0.0.31",
     "devtools-source-editor": "=0.0.3",
     "enzyme": "^2.4.1",
     "expect": "^1.16.0",
     "file-loader": "^0.10.1",
     "immutable": "^3.8.1",
     "jsdom": "^9.4.1",
     "jsdom-global": "^2.0.0",
@@ -32,13 +35,10 @@
     "react": "=15.3.2",
     "react-addons-perf": "=15.3.2",
     "react-dom": "=15.3.2",
     "react-redux": "=5.0.3",
     "redux": "^3.6.0",
     "require-hacker": "^2.1.4",
     "reselect": "^3.0.1",
     "sinon": "^1.17.5"
-  },
-  "devDependencies": {
-    "babel-plugin-transform-object-rest-spread": "^6.26.0"
   }
 }
diff --git a/devtools/client/webconsole/webpack.config.js b/devtools/client/webconsole/webpack.config.js
--- a/devtools/client/webconsole/webpack.config.js
+++ b/devtools/client/webconsole/webpack.config.js
@@ -36,16 +36,18 @@ let webpackConfig = {
           * so the raw-loader declared in devtools-launchpad config can load
           * those files.
           */
           "rewrite-raw",
           // Replace all references to this.browserRequire() by require()
           "rewrite-browser-require",
           // Replace all references to loader.lazyRequire() by require()
           "rewrite-lazy-require",
+          // Replace all references to loader.lazyGetter() by require()
+          "rewrite-lazy-getter",
         ],
       }
     ]
   },
 
   resolveLoader: {
     modules: [
       path.resolve("./node_modules"),
@@ -137,21 +139,21 @@ const mappings = [
     }
   ],
 ];
 
 webpackConfig.plugins = mappings.map(([regex, res]) =>
   new NormalModuleReplacementPlugin(regex, res));
 
 const basePath = path.join(__dirname, "../../").replace(/\\/g, "\\\\");
-const baseName = path.basename(__dirname);
 
 let config = toolboxConfig(webpackConfig, getConfig(), {
-  // Exclude to transpile all scripts in devtools/ but not for this folder
-  babelExcludes: new RegExp(`^${basePath}(.(?!${baseName}))*$`)
+  // Exclude to transpile all scripts in devtools/ but not for this folder nor netmonitor.
+  babelExcludes: new RegExp(`^${basePath}(.(?!(webconsole|netmonitor)))*$`),
+  disablePostCSS: true,
 });
 
 // Remove loaders from devtools-launchpad's webpack.config.js
 // * For svg-inline loader:
 //   Webconsole uses file loader to bundle image assets instead of svg-inline-loader
 config.module.rules = config.module.rules
   .filter((rule) => !["svg-inline-loader"].includes(rule.loader));
 
