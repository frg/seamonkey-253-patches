# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1517418626 28800
#      Wed Jan 31 09:10:26 2018 -0800
# Node ID 6b5ed759f753d1f204be3826ab56bb33bb2d2611
# Parent  edfc00999a3afac47595e1389f35d8b7903739b6
Bug 1434342 P6 Make ServiceWorker call nsIGlobalObject::AddServiceWorker and RemoveServiceWorker. r=asuth

diff --git a/dom/serviceworkers/ServiceWorker.cpp b/dom/serviceworkers/ServiceWorker.cpp
--- a/dom/serviceworkers/ServiceWorker.cpp
+++ b/dom/serviceworkers/ServiceWorker.cpp
@@ -68,26 +68,33 @@ ServiceWorker::Create(nsIGlobalObject* a
 ServiceWorker::ServiceWorker(nsIGlobalObject* aGlobal,
                              const ServiceWorkerDescriptor& aDescriptor,
                              ServiceWorker::Inner* aInner)
   : DOMEventTargetHelper(aGlobal)
   , mDescriptor(aDescriptor)
   , mInner(aInner)
 {
   MOZ_ASSERT(NS_IsMainThread());
+  MOZ_DIAGNOSTIC_ASSERT(aGlobal);
   MOZ_DIAGNOSTIC_ASSERT(mInner);
 
+  aGlobal->AddServiceWorker(this);
+
   // This will update our state too.
   mInner->AddServiceWorker(this);
 }
 
 ServiceWorker::~ServiceWorker()
 {
   MOZ_ASSERT(NS_IsMainThread());
   mInner->RemoveServiceWorker(this);
+  nsIGlobalObject* global = GetParentObject();
+  if (global) {
+    global->RemoveServiceWorker(this);
+  }
 }
 
 NS_IMPL_ADDREF_INHERITED(ServiceWorker, DOMEventTargetHelper)
 NS_IMPL_RELEASE_INHERITED(ServiceWorker, DOMEventTargetHelper)
 
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(ServiceWorker)
 NS_INTERFACE_MAP_END_INHERITING(DOMEventTargetHelper)
 
@@ -140,10 +147,20 @@ ServiceWorker::MatchesDescriptor(const S
   // Compare everything in the descriptor except the state.  That is mutable
   // and may not exactly match.
   return mDescriptor.PrincipalInfo() == aDescriptor.PrincipalInfo() &&
          mDescriptor.Scope() == aDescriptor.Scope() &&
          mDescriptor.ScriptURL() == aDescriptor.ScriptURL() &&
          mDescriptor.Id() == aDescriptor.Id();
 }
 
+void
+ServiceWorker::DisconnectFromOwner()
+{
+  nsIGlobalObject* global = GetParentObject();
+  if (global) {
+    global->RemoveServiceWorker(this);
+  }
+  DOMEventTargetHelper::DisconnectFromOwner();
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/serviceworkers/ServiceWorker.h b/dom/serviceworkers/ServiceWorker.h
--- a/dom/serviceworkers/ServiceWorker.h
+++ b/dom/serviceworkers/ServiceWorker.h
@@ -81,16 +81,19 @@ public:
 
   void
   PostMessage(JSContext* aCx, JS::Handle<JS::Value> aMessage,
               const Sequence<JSObject*>& aTransferable, ErrorResult& aRv);
 
   bool
   MatchesDescriptor(const ServiceWorkerDescriptor& aDescriptor) const;
 
+  void
+  DisconnectFromOwner() override;
+
 private:
   ServiceWorker(nsIGlobalObject* aWindow,
                 const ServiceWorkerDescriptor& aDescriptor,
                 Inner* aInner);
 
   // This class is reference-counted and will be destroyed from Release().
   ~ServiceWorker();
 
