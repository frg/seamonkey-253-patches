# HG changeset patch
# User Anthony Ramine <n.oxyde@gmail.com>
# Date 1520104517 18000
# Node ID f4d9447f8424a0847560e636319e9d2d0b3a43a0
# Parent  1bdc255998e7115cb23875878e240ef3c39b9a65
servo: Merge #20183 - Unconditionally derive ToComputedValue as Clone for non-generic types (from servo:computed-as-clone); r=emilio

We assume that types such as `<Self as ToComputedValue>::ToComputedValue == Self`
just construct a new value that is just a clone of the original one without any
additional code.

Source-Repo: https://github.com/servo/servo
Source-Revision: 95f81d0c394e99a43552b1f964a9bf4df90ec759

diff --git a/servo/components/style/values/computed/mod.rs b/servo/components/style/values/computed/mod.rs
--- a/servo/components/style/values/computed/mod.rs
+++ b/servo/components/style/values/computed/mod.rs
@@ -284,17 +284,20 @@ impl<'a, 'cx, 'cx_a: 'cx, S: ToComputedV
     }
 }
 
 /// A trait to represent the conversion between computed and specified values.
 ///
 /// This trait is derivable with `#[derive(ToComputedValue)]`. The derived
 /// implementation just calls `ToComputedValue::to_computed_value` on each field
 /// of the passed value, or `Clone::clone` if the field is annotated with
-/// `#[compute(clone)]`.
+/// `#[compute(clone)]`. The deriving code assumes that if the type isn't
+/// generic, then the trait can be implemented as simple `Clone::clone` calls,
+/// this means that a manual implementation with `ComputedValue = Self` is bogus
+/// if it returns anything else than a clone.
 pub trait ToComputedValue {
     /// The computed value type we're going to be converted to.
     type ComputedValue;
 
     /// Convert a specified value to a computed value, using itself and the data
     /// inside the `Context`.
     #[inline]
     fn to_computed_value(&self, context: &Context) -> Self::ComputedValue;
diff --git a/servo/components/style_derive/to_computed_value.rs b/servo/components/style_derive/to_computed_value.rs
--- a/servo/components/style_derive/to_computed_value.rs
+++ b/servo/components/style_derive/to_computed_value.rs
@@ -8,16 +8,39 @@ use syn::{Ident, DeriveInput};
 use synstructure::BindStyle;
 
 pub fn derive(input: DeriveInput) -> Tokens {
     let name = &input.ident;
     let trait_path = parse_quote!(values::computed::ToComputedValue);
     let (impl_generics, ty_generics, mut where_clause, computed_value_type) =
         cg::fmap_trait_parts(&input, &trait_path, Ident::from("ComputedValue"));
 
+    if input.generics.params.is_empty() {
+        return quote! {
+            impl #impl_generics ::values::computed::ToComputedValue for #name #ty_generics
+            #where_clause
+            {
+                type ComputedValue = #computed_value_type;
+
+                #[inline]
+                fn to_computed_value(
+                    &self,
+                    _context: &::values::computed::Context,
+                ) -> Self::ComputedValue {
+                    ::std::clone::Clone::clone(self)
+                }
+
+                #[inline]
+                fn from_computed_value(computed: &Self::ComputedValue) -> Self {
+                    ::std::clone::Clone::clone(computed)
+                }
+            }
+        }
+    }
+
     let to_body = cg::fmap_match(&input, BindStyle::Ref, |binding| {
         let attrs = cg::parse_field_attrs::<ComputedValueAttrs>(&binding.ast());
         if attrs.clone {
             if cg::is_parameterized(&binding.ast().ty, &where_clause.params, None) {
                 where_clause.add_predicate(cg::where_predicate(
                     binding.ast().ty.clone(),
                     &parse_quote!(std::clone::Clone),
                     None,
