# HG changeset patch
# User Dmitry Butskoy <buc@buc.me>
# Date 1690630056 -7200
# Parent  9a3f6b41f179fa3776f368e831393b4b153e2d89
No Bug - Import new regexp V8 engine. r=frg a=frg

diff --git a/js/moz.configure b/js/moz.configure
--- a/js/moz.configure
+++ b/js/moz.configure
@@ -397,19 +397,19 @@ def enable_pipeline_operator(value):
 
 set_config('ENABLE_PIPELINE_OPERATOR', enable_pipeline_operator)
 set_define('ENABLE_PIPELINE_OPERATOR', enable_pipeline_operator)
 
 
 # Initial support for new regexp engine
 # ==================================================
 
-option('--enable-new-regexp', default=False, help='Enable new regexp engine')
+option('--disable-new-regexp', help='{Enable|Disable} new regexp engine')
 
-@depends('--enable-new-regexp')
+@depends('--disable-new-regexp')
 def enable_new_regexp(value):
     if value:
         return True
 
 set_config('ENABLE_NEW_REGEXP', enable_new_regexp)
 set_define('ENABLE_NEW_REGEXP', enable_new_regexp)
 
 
diff --git a/js/src/new-regexp/RegExpAPI.cpp b/js/src/new-regexp/RegExpAPI.cpp
--- a/js/src/new-regexp/RegExpAPI.cpp
+++ b/js/src/new-regexp/RegExpAPI.cpp
@@ -127,16 +127,20 @@ static uint32_t ErrorNumber(RegExpError 
 Isolate* CreateIsolate(JSContext* cx) {
   auto isolate = MakeUnique<Isolate>(cx);
   if (!isolate || !isolate->init()) {
     return nullptr;
   }
   return isolate.release();
 }
 
+void DestroyIsolate(Isolate* isolate) {
+  js_delete(isolate);
+}
+
 static size_t ComputeColumn(const Latin1Char* begin, const Latin1Char* end) {
   return mozilla::PointerRangeSize(begin, end);
 }
 
 static size_t ComputeColumn(const char16_t* begin, const char16_t* end) {
   return unicode::CountCodePoints(begin, end);
 }
 
@@ -207,18 +211,18 @@ static void ReportSyntaxError(TokenStrea
   err.lineLength = windowLength;
   err.tokenOffset = offset - (windowStart - start);
 
   va_list args;
   va_start(args, length);
   ReportCompileError(ts.context(),
                      std::move(err),
                      nullptr, /* notes/report */
+                     JSREPORT_ERROR, /*flags*/
                      errorNumber,
-                     0 /*flags*/,
                      args);
   va_end(args);
 }
 
 static void ReportSyntaxError(TokenStream& ts,
                               RegExpCompileData& result, HandleAtom pattern) {
   JS::AutoCheckCannotGC nogc_;
   if (pattern->hasLatin1Chars()) {
@@ -244,27 +248,43 @@ bool CheckPatternSyntax(JSContext* cx, T
                         const mozilla::Range<const char16_t> chars,
                         JS::RegExpFlags flags) {
   FlatStringReader reader(chars.begin().get(), chars.length());
   RegExpCompileData result;
   if (!CheckPatternSyntaxImpl(cx, &reader, flags, &result)) {
     ReportSyntaxError(ts, result, chars.begin().get(), chars.length());
     return false;
   }
+  if (!result.capture_name_map.is_null()) {
+    // We can parse named captures, but we don't support them yet.
+    // Until we do, this will report a syntax error:
+    //  "invalid capture group name in regular expression"
+    result.error = RegExpError::kInvalidCaptureGroupName;
+    ReportSyntaxError(ts, result, chars.begin().get(), chars.length());
+    return false;
+  }
   return true;
 }
 
 bool CheckPatternSyntax(JSContext* cx, TokenStream& ts,
                         HandleAtom pattern, JS::RegExpFlags flags) {
   FlatStringReader reader(pattern);
   RegExpCompileData result;
   if (!CheckPatternSyntaxImpl(cx, &reader, flags, &result)) {
     ReportSyntaxError(ts, result, pattern);
     return false;
   }
+  if (!result.capture_name_map.is_null()) {
+    // We can parse named captures, but we don't support them yet.
+    // Until we do, this will report a syntax error:
+    //  "invalid capture group name in regular expression"
+    result.error = RegExpError::kInvalidCaptureGroupName;
+    ReportSyntaxError(ts, result, pattern);
+    return false;
+  }
   return true;
 }
 
 // A regexp is a good candidate for Boyer-Moore if it has at least 3
 // times as many characters as it has unique characters. Note that
 // table lookups in irregexp are done modulo tableSize (128).
 template <typename CharT>
 static bool HasFewDifferentCharacters(const CharT* chars, size_t length) {
@@ -314,71 +334,123 @@ static void SampleCharacters(HandleLinea
 
   int half_way = (length - kSampleSize) / 2;
   for (int i = std::max(0, half_way); i < length && chars_sampled < kSampleSize;
        i++, chars_sampled++) {
       compiler.frequency_collator()->CountCharacter(sample_subject.Get(i));
   }
 }
 
-static RegExpNode* WrapBody(MutableHandleRegExpShared re,
-                            RegExpCompiler& compiler, RegExpCompileData& data,
-                            Zone* zone, bool isLatin1) {
-  using v8::internal::ChoiceNode;
-  using v8::internal::EndNode;
-  using v8::internal::GuardedAlternative;
-  using v8::internal::RegExpCapture;
-  using v8::internal::RegExpCharacterClass;
-  using v8::internal::RegExpQuantifier;
-  using v8::internal::RegExpTree;
-  using v8::internal::TextNode;
+enum class AssembleResult {
+  Success,
+  TooLarge,
+  OutOfMemory,
+};
 
-  RegExpNode* captured_body =
-      RegExpCapture::ToNode(data.tree, 0, &compiler, compiler.accept());
-  RegExpNode* node = captured_body;
+static MOZ_MUST_USE AssembleResult Assemble(JSContext* cx,
+                                            RegExpCompiler* compiler,
+                                            RegExpCompileData* data,
+                                            MutableHandleRegExpShared re,
+                                            HandleAtom pattern, Zone* zone,
+                                            bool useNativeCode, bool isLatin1) {
+  // Because we create a StackMacroAssembler, this function is not allowed
+  // to GC. If needed, we allocate and throw errors in the caller.
+  Maybe<jit::JitContext> jctx;
+  Maybe<js::jit::StackMacroAssembler> stack_masm;
+  UniquePtr<RegExpMacroAssembler> masm;
+  if (useNativeCode) {
+    NativeRegExpMacroAssembler::Mode mode =
+        isLatin1 ? NativeRegExpMacroAssembler::LATIN1
+                 : NativeRegExpMacroAssembler::UC16;
+    // If we are compiling native code, we need a macroassembler,
+    // which needs a jit context.
+    jctx.emplace(cx, nullptr);
+    stack_masm.emplace();
+    uint32_t num_capture_registers = re->pairCount() * 2;
+    masm = MakeUnique<SMRegExpMacroAssembler>(cx, stack_masm.ref(), zone, mode,
+                                              num_capture_registers);
+  } else {
+    masm = MakeUnique<RegExpBytecodeGenerator>(cx->isolate, zone);
+  }
+  if (!masm) {
+    return AssembleResult::OutOfMemory;
+  }
 
-  if (!data.tree->IsAnchoredAtStart() && !re->sticky()) {
-    // Add a .*? at the beginning, outside the body capture, unless
-    // this expression is anchored at the beginning or sticky.
-    JS::RegExpFlags default_flags;
-    RegExpNode* loop_node = RegExpQuantifier::ToNode(
-        0, RegExpTree::kInfinity, false,
-        new (zone) RegExpCharacterClass('*', default_flags), &compiler,
-        captured_body, data.contains_anchor);
+  bool isLargePattern =
+      pattern->length() > v8::internal::RegExp::kRegExpTooLargeToOptimize;
+  masm->set_slow_safe(isLargePattern);
+  if (compiler->optimize()) {
+    compiler->set_optimize(!isLargePattern);
+  }
 
-    if (data.contains_anchor) {
-      // Unroll loop once, to take care of the case that might start
-      // at the start of input.
-      ChoiceNode* first_step_node = new (zone) ChoiceNode(2, zone);
-      first_step_node->AddAlternative(GuardedAlternative(captured_body));
-      first_step_node->AddAlternative(GuardedAlternative(new (zone) TextNode(
-          new (zone) RegExpCharacterClass('*', default_flags), false,
-          loop_node)));
-      node = first_step_node;
-    } else {
-      node = loop_node;
+  // When matching a regexp with known maximum length that is anchored
+  // at the end, we may be able to skip the beginning of long input
+  // strings. This decision is made here because it depends on
+  // information in the AST that isn't replicated in the Node
+  // structure used inside the compiler.
+  bool is_start_anchored = data->tree->IsAnchoredAtStart();
+  bool is_end_anchored = data->tree->IsAnchoredAtEnd();
+  int max_length = data->tree->max_match();
+  static const int kMaxBacksearchLimit = 1024;
+  if (is_end_anchored && !is_start_anchored && !re->sticky() &&
+      max_length < kMaxBacksearchLimit) {
+    masm->SetCurrentPositionFromEnd(max_length);
+  }
+
+  if (re->global()) {
+    RegExpMacroAssembler::GlobalMode mode = RegExpMacroAssembler::GLOBAL;
+    if (data->tree->min_match() > 0) {
+      mode = RegExpMacroAssembler::GLOBAL_NO_ZERO_LENGTH_CHECK;
+    } else if (re->unicode()) {
+      mode = RegExpMacroAssembler::GLOBAL_UNICODE;
     }
+    masm->set_global_mode(mode);
   }
-  if (isLatin1) {
-    node = node->FilterOneByte(RegExpCompiler::kMaxRecursion);
-    // Do it again to propagate the new nodes to places where they were not
-    // put because they had not been calculated yet.
-    if (node != nullptr) {
-      node = node->FilterOneByte(RegExpCompiler::kMaxRecursion);
+
+  // Compile the regexp.
+  V8HandleString wrappedPattern(v8::internal::String(pattern), cx->isolate);
+  RegExpCompiler::CompilationResult result = compiler->Assemble(
+      cx->isolate, masm.get(), data->node, data->capture_count, wrappedPattern);
+  if (!result.Succeeded()) {
+    MOZ_ASSERT(result.error == RegExpError::kTooLarge);
+    return AssembleResult::TooLarge;
+  }
+  if (result.code->value().isUndefined()) {
+    // SMRegExpMacroAssembler::GetCode returns undefined on OOM.
+    MOZ_ASSERT(useNativeCode);
+    return AssembleResult::OutOfMemory;
+  }
+
+  re->updateMaxRegisters(result.num_registers);
+  if (useNativeCode) {
+    // Transfer ownership of the tables from the macroassembler to the
+    // RegExpShared.
+    SMRegExpMacroAssembler::TableVector& tables =
+        static_cast<SMRegExpMacroAssembler*>(masm.get())->tables();
+    for (uint32_t i = 0; i < tables.length(); i++) {
+      if (!re->addTable(std::move(tables[i]))) {
+        return AssembleResult::OutOfMemory;
+      }
     }
-  } else if (re->unicode() && (re->global() || re->sticky())) {
-    node = RegExpCompiler::OptionallyStepBackToLeadSurrogate(&compiler, node,
-                                                             re->getFlags());
+    re->setJitCode(v8::internal::Code::cast(*result.code).inner(), isLatin1);
+  } else {
+    // Transfer ownership of the bytecode from the HandleScope to the
+    // RegExpShared.
+    ByteArray bytecode =
+        v8::internal::ByteArray::cast(*result.code).takeOwnership(cx->isolate);
+    uint32_t length = bytecode->length;
+    re->setByteCode(bytecode.release(), isLatin1);
+    // js::AddCellMemory(re, length, MemoryUse::RegExpSharedBytecode);
   }
-  if (node == nullptr) node = new (zone) EndNode(EndNode::BACKTRACK, zone);
-  return node;
+
+  return AssembleResult::Success;
 }
 
 bool CompilePattern(JSContext* cx, MutableHandleRegExpShared re,
-                    HandleLinearString input) {
+                    HandleLinearString input, RegExpShared::CodeKind codeKind) {
   RootedAtom pattern(cx, re->getSource());
   JS::RegExpFlags flags = re->getFlags();
   LifoAllocScope allocScope(&cx->tempLifoAlloc());
   Zone zone(allocScope.alloc());
 
   RegExpCompileData data;
   {
     FlatStringReader patternBytes(pattern);
@@ -424,120 +496,37 @@ bool CompilePattern(JSContext* cx, Mutab
 
   HandleScope handleScope(cx->isolate);
   RegExpCompiler compiler(cx->isolate, &zone, data.capture_count,
                           input->hasLatin1Chars());
 
   bool isLatin1 = input->hasLatin1Chars();
 
   SampleCharacters(input, compiler);
-  data.node = WrapBody(re, compiler, data, &zone, isLatin1);
+  data.node = compiler.PreprocessRegExp(&data, flags, isLatin1);
   data.error = AnalyzeRegExp(cx->isolate, isLatin1, data.node);
   if (data.error != RegExpError::kNone) {
     MOZ_ASSERT(data.error == RegExpError::kAnalysisStackOverflow);
     ReportOverRecursed(cx);
     return false;
   }
 
-  bool useNativeCode = re->markedForTierUp(cx);
-
+  bool useNativeCode = codeKind == RegExpShared::CodeKind::Jitcode;
   MOZ_ASSERT_IF(useNativeCode, IsNativeRegExpEnabled(cx));
 
-  Maybe<jit::JitContext> jctx;
-  Maybe<js::jit::StackMacroAssembler> stack_masm;
-  UniquePtr<RegExpMacroAssembler> masm;
-  if (useNativeCode) {
-    NativeRegExpMacroAssembler::Mode mode =
-        isLatin1 ? NativeRegExpMacroAssembler::LATIN1
-                 : NativeRegExpMacroAssembler::UC16;
-    // If we are compiling native code, we need a macroassembler,
-    // which needs a jit context.
-    jctx.emplace(cx, nullptr);
-    stack_masm.emplace();
-    uint32_t num_capture_registers = re->pairCount() * 2;
-    masm = MakeUnique<SMRegExpMacroAssembler>(cx, stack_masm.ref(), &zone, mode,
-                                              num_capture_registers);
-  } else {
-    masm = MakeUnique<RegExpBytecodeGenerator>(cx->isolate, &zone);
-  }
-  if (!masm) {
-    ReportOutOfMemory(cx);
-    return false;
-  }
-
-  bool largePattern =
-      pattern->length() > v8::internal::RegExp::kRegExpTooLargeToOptimize;
-  masm->set_slow_safe(largePattern);
-  if (compiler.optimize()) {
-    compiler.set_optimize(!largePattern);
-  }
-
-  // When matching a regexp with known maximum length that is anchored
-  // at the end, we may be able to skip the beginning of long input
-  // strings. This decision is made here because it depends on
-  // information in the AST that isn't replicated in the Node
-  // structure used inside the compiler.
-  bool is_start_anchored = data.tree->IsAnchoredAtStart();
-  bool is_end_anchored = data.tree->IsAnchoredAtEnd();
-  int max_length = data.tree->max_match();
-  static const int kMaxBacksearchLimit = 1024;
-  if (is_end_anchored && !is_start_anchored && !re->sticky() &&
-      max_length < kMaxBacksearchLimit) {
-    masm->SetCurrentPositionFromEnd(max_length);
+  switch (Assemble(cx, &compiler, &data, re, pattern, &zone, useNativeCode, isLatin1)) {
+      case AssembleResult::TooLarge:
+          JS_ReportErrorASCII(cx, "regexp too big");
+          return false;
+      case AssembleResult::OutOfMemory:
+          ReportOutOfMemory(cx);
+          return false;
+      case AssembleResult::Success:
+          break;
   }
-
-  if (re->global()) {
-    RegExpMacroAssembler::GlobalMode mode = RegExpMacroAssembler::GLOBAL;
-    if (data.tree->min_match() > 0) {
-      mode = RegExpMacroAssembler::GLOBAL_NO_ZERO_LENGTH_CHECK;
-    } else if (re->unicode()) {
-      mode = RegExpMacroAssembler::GLOBAL_UNICODE;
-    }
-    masm->set_global_mode(mode);
-  }
-
-  // Compile the regexp
-  V8HandleString wrappedPattern(v8::internal::String(pattern), cx->isolate);
-  RegExpCompiler::CompilationResult result = compiler.Assemble(
-      cx->isolate, masm.get(), data.node, data.capture_count, wrappedPattern);
-  if (result.code.value().isUndefined()) {
-    // SMRegExpMacroAssembler::GetCode returns undefined on OOM.
-    MOZ_ASSERT(useNativeCode);
-    ReportOutOfMemory(cx);
-    return false;
-  }
-  if (!result.Succeeded()) {
-    MOZ_ASSERT(result.error == RegExpError::kTooLarge);
-    JS_ReportErrorASCII(cx, "regexp too big");
-    return false;
-  }
-
-  re->updateMaxRegisters(result.num_registers);
-  if (useNativeCode) {
-    // Transfer ownership of the tables from the macroassembler to the
-    // RegExpShared.
-    SMRegExpMacroAssembler::TableVector& tables =
-        static_cast<SMRegExpMacroAssembler*>(masm.get())->tables();
-    for (uint32_t i = 0; i < tables.length(); i++) {
-      if (!re->addTable(std::move(tables[i]))) {
-        ReportOutOfMemory(cx);
-        return false;
-      }
-    }
-    re->setJitCode(v8::internal::Code::cast(result.code).inner(), isLatin1);
-  } else {
-    // Transfer ownership of the bytecode from the HandleScope to the
-    // RegExpShared.
-    ByteArray bytecode =
-        v8::internal::ByteArray::cast(result.code).takeOwnership(cx->isolate);
-    uint32_t length = bytecode->length;
-    re->setByteCode(bytecode.release(), isLatin1);
-    // js::AddCellMemory(re, length, MemoryUse::RegExpSharedBytecode);
-  }
-
   return true;
 }
 
 template <typename CharT>
 RegExpRunStatus ExecuteRaw(jit::JitCode* code, const CharT* chars,
                            size_t length, size_t startIndex,
                            MatchPairs* matches) {
   InputOutputData data(chars, chars + length, startIndex, matches);
@@ -559,51 +548,32 @@ RegExpRunStatus ExecuteRaw(jit::JitCode*
 
 RegExpRunStatus Interpret(JSContext* cx, MutableHandleRegExpShared re,
                           HandleLinearString input, size_t startIndex,
                           MatchPairs* matches) {
   HandleScope handleScope(cx->isolate);
   V8HandleRegExp wrappedRegExp(v8::internal::JSRegExp(re), cx->isolate);
   V8HandleString wrappedInput(v8::internal::String(input), cx->isolate);
 
-  uint32_t numRegisters = re->getMaxRegisters();
-
-  // Allocate memory for registers. They will be initialized by the
-  // interpreter. (See IrregexpInterpreter::MatchInternal.)
-  Vector<int32_t, 8, SystemAllocPolicy> registers;
-  if (!registers.growByUninitialized(numRegisters)) {
-    ReportOutOfMemory(cx);
-    return RegExpRunStatus_Error;
-  }
-
   static_assert(RegExpRunStatus_Error ==
                 v8::internal::RegExp::kInternalRegExpException);
   static_assert(RegExpRunStatus_Success ==
                 v8::internal::RegExp::kInternalRegExpSuccess);
   static_assert(RegExpRunStatus_Success_NotFound ==
                 v8::internal::RegExp::kInternalRegExpFailure);
 
   RegExpRunStatus status =
       (RegExpRunStatus)IrregexpInterpreter::MatchForCallFromRuntime(
-          cx->isolate, wrappedRegExp, wrappedInput, registers.begin(),
-          numRegisters, startIndex);
+           cx->isolate, wrappedRegExp, wrappedInput, matches->pairsRaw(),
+           matches->pairCount() * 2, startIndex);
 
   MOZ_ASSERT(status == RegExpRunStatus_Error ||
              status == RegExpRunStatus_Success ||
              status == RegExpRunStatus_Success_NotFound);
 
-  // Copy results out of registers
-  if (status == RegExpRunStatus_Success) {
-    uint32_t length = re->pairCount() * 2;
-    MOZ_ASSERT(length <= registers.length());
-    for (uint32_t i = 0; i < length; i++) {
-      matches->pairsRaw()[i] = registers[i];
-    }
-  }
-
   return status;
 }
 
 RegExpRunStatus Execute(JSContext* cx, MutableHandleRegExpShared re,
                         HandleLinearString input, size_t startIndex,
                         MatchPairs* matches) {
   bool latin1 = input->hasLatin1Chars();
   jit::JitCode* jitCode = re->getJitCode(latin1);
diff --git a/js/src/new-regexp/RegExpAPI.h b/js/src/new-regexp/RegExpAPI.h
--- a/js/src/new-regexp/RegExpAPI.h
+++ b/js/src/new-regexp/RegExpAPI.h
@@ -15,24 +15,25 @@
 #include "frontend/TokenStream.h"
 #include "jscntxt.h"
 #include "vm/RegExpObject.h"
 
 namespace js {
 namespace irregexp {
 
 Isolate* CreateIsolate(JSContext* cx);
+void DestroyIsolate(Isolate* isolate);
 
 bool CheckPatternSyntax(JSContext* cx, frontend::TokenStream& ts,
                         const mozilla::Range<const char16_t> chars,
                         JS::RegExpFlags flags);
 bool CheckPatternSyntax(JSContext* cx, frontend::TokenStream& ts,
                         HandleAtom pattern, JS::RegExpFlags flags);
 bool CompilePattern(JSContext* cx, MutableHandleRegExpShared re,
-                    HandleLinearString input);
+                    HandleLinearString input, RegExpShared::CodeKind codeKind);
 
 RegExpRunStatus Execute(JSContext* cx, MutableHandleRegExpShared re,
                         HandleLinearString input, size_t start,
                         MatchPairs* matches);
 
 }  // namespace irregexp
 }  // namespace js
 
diff --git a/js/src/new-regexp/regexp-compiler.cc b/js/src/new-regexp/regexp-compiler.cc
--- a/js/src/new-regexp/regexp-compiler.cc
+++ b/js/src/new-regexp/regexp-compiler.cc
@@ -213,17 +213,17 @@ class RecursionCheck {
  private:
   RegExpCompiler* compiler_;
 };
 
 // Attempts to compile the regexp using an Irregexp code generator.  Returns
 // a fixed array or a null handle depending on whether it succeeded.
 RegExpCompiler::RegExpCompiler(Isolate* isolate, Zone* zone, int capture_count,
                                bool one_byte)
-    : next_register_(2 * (capture_count + 1)),
+    : next_register_(JSRegExp::RegistersForCaptureCount(capture_count)),
       unicode_lookaround_stack_register_(kNoRegister),
       unicode_lookaround_position_register_(kNoRegister),
       work_list_(nullptr),
       recursion_depth_(0),
       one_byte_(one_byte),
       reg_exp_too_big_(false),
       limiting_recursion_(false),
       optimize_(FLAG_regexp_optimization),
@@ -3794,38 +3794,80 @@ void TextNode::FillInBMInfo(Isolate* iso
     if (initial_offset == 0) set_bm_info(not_at_start, bm);
     return;
   }
   on_success()->FillInBMInfo(isolate, offset, budget - 1, bm,
                              true);  // Not at start after a text node.
   if (initial_offset == 0) set_bm_info(not_at_start, bm);
 }
 
-// static
 RegExpNode* RegExpCompiler::OptionallyStepBackToLeadSurrogate(
-    RegExpCompiler* compiler, RegExpNode* on_success, JSRegExp::Flags flags) {
-  DCHECK(!compiler->read_backward());
-  Zone* zone = compiler->zone();
+    RegExpNode* on_success, JSRegExp::Flags flags) {
+  DCHECK(!read_backward());
   ZoneList<CharacterRange>* lead_surrogates = CharacterRange::List(
-      zone, CharacterRange::Range(kLeadSurrogateStart, kLeadSurrogateEnd));
+      zone(), CharacterRange::Range(kLeadSurrogateStart, kLeadSurrogateEnd));
   ZoneList<CharacterRange>* trail_surrogates = CharacterRange::List(
-      zone, CharacterRange::Range(kTrailSurrogateStart, kTrailSurrogateEnd));
-
-  ChoiceNode* optional_step_back = new (zone) ChoiceNode(2, zone);
-
-  int stack_register = compiler->UnicodeLookaroundStackRegister();
-  int position_register = compiler->UnicodeLookaroundPositionRegister();
+      zone(), CharacterRange::Range(kTrailSurrogateStart, kTrailSurrogateEnd));
+
+  ChoiceNode* optional_step_back = new (zone()) ChoiceNode(2, zone());
+
+  int stack_register = UnicodeLookaroundStackRegister();
+  int position_register = UnicodeLookaroundPositionRegister();
   RegExpNode* step_back = TextNode::CreateForCharacterRanges(
-      zone, lead_surrogates, true, on_success, flags);
+      zone(), lead_surrogates, true, on_success, flags);
   RegExpLookaround::Builder builder(true, step_back, stack_register,
                                     position_register);
   RegExpNode* match_trail = TextNode::CreateForCharacterRanges(
-      zone, trail_surrogates, false, builder.on_match_success(), flags);
+      zone(), trail_surrogates, false, builder.on_match_success(), flags);
 
   optional_step_back->AddAlternative(
       GuardedAlternative(builder.ForMatch(match_trail)));
   optional_step_back->AddAlternative(GuardedAlternative(on_success));
 
   return optional_step_back;
 }
 
+RegExpNode* RegExpCompiler::PreprocessRegExp(RegExpCompileData* data,
+                                             JSRegExp::Flags flags,
+                                             bool is_one_byte) {
+  // Wrap the body of the regexp in capture #0.
+  RegExpNode* captured_body =
+      RegExpCapture::ToNode(data->tree, 0, this, accept());
+  RegExpNode* node = captured_body;
+  if (!data->tree->IsAnchoredAtStart() && !IsSticky(flags)) {
+    // Add a .*? at the beginning, outside the body capture, unless
+    // this expression is anchored at the beginning or sticky.
+    JSRegExp::Flags default_flags = JSRegExp::Flags();
+    RegExpNode* loop_node = RegExpQuantifier::ToNode(
+        0, RegExpTree::kInfinity, false,
+        new (zone()) RegExpCharacterClass('*', default_flags), this,
+        captured_body, data->contains_anchor);
+
+    if (data->contains_anchor) {
+      // Unroll loop once, to take care of the case that might start
+      // at the start of input.
+      ChoiceNode* first_step_node = new (zone()) ChoiceNode(2, zone());
+      first_step_node->AddAlternative(GuardedAlternative(captured_body));
+      first_step_node->AddAlternative(GuardedAlternative(new (zone()) TextNode(
+          new (zone()) RegExpCharacterClass('*', default_flags), false,
+          loop_node)));
+      node = first_step_node;
+    } else {
+      node = loop_node;
+    }
+  }
+  if (is_one_byte) {
+    node = node->FilterOneByte(RegExpCompiler::kMaxRecursion);
+    // Do it again to propagate the new nodes to places where they were not
+    // put because they had not been calculated yet.
+    if (node != nullptr) {
+      node = node->FilterOneByte(RegExpCompiler::kMaxRecursion);
+    }
+  } else if (IsUnicode(flags) && (IsGlobal(flags) || IsSticky(flags))) {
+    node = OptionallyStepBackToLeadSurrogate(node, flags);
+  }
+
+  if (node == nullptr) node = new (zone()) EndNode(EndNode::BACKTRACK, zone());
+  return node;
+}
+
 }  // namespace internal
 }  // namespace v8
diff --git a/js/src/new-regexp/regexp-compiler.h b/js/src/new-regexp/regexp-compiler.h
--- a/js/src/new-regexp/regexp-compiler.h
+++ b/js/src/new-regexp/regexp-compiler.h
@@ -513,21 +513,29 @@ class RegExpCompiler {
     Object code;
     int num_registers = 0;
   };
 
   CompilationResult Assemble(Isolate* isolate, RegExpMacroAssembler* assembler,
                              RegExpNode* start, int capture_count,
                              Handle<String> pattern);
 
+  // Preprocessing is the final step of node creation before analysis
+  // and assembly. It includes:
+  // - Wrapping the body of the regexp in capture 0.
+  // - Inserting the implicit .* before/after the regexp if necessary.
+  // - If the input is a one-byte string, filtering out nodes that can't match.
+  // - Fixing up regexp matches that start within a surrogate pair.
+  RegExpNode* PreprocessRegExp(RegExpCompileData* data, JSRegExp::Flags flags,
+                               bool is_one_byte);
+
   // If the regexp matching starts within a surrogate pair, step back to the
   // lead surrogate and start matching from there.
-  static RegExpNode* OptionallyStepBackToLeadSurrogate(RegExpCompiler* compiler,
-                                                       RegExpNode* on_success,
-                                                       JSRegExp::Flags flags);
+  RegExpNode* OptionallyStepBackToLeadSurrogate(RegExpNode* on_success,
+                                                JSRegExp::Flags flags);
 
   inline void AddWork(RegExpNode* node) {
     if (!node->on_work_list() && !node->label()->is_bound()) {
       node->set_on_work_list(true);
       work_list_->push_back(node);
     }
   }
 
diff --git a/js/src/new-regexp/regexp-interpreter.h b/js/src/new-regexp/regexp-interpreter.h
--- a/js/src/new-regexp/regexp-interpreter.h
+++ b/js/src/new-regexp/regexp-interpreter.h
@@ -18,44 +18,50 @@ class V8_EXPORT_PRIVATE IrregexpInterpre
     FAILURE = RegExp::kInternalRegExpFailure,
     SUCCESS = RegExp::kInternalRegExpSuccess,
     EXCEPTION = RegExp::kInternalRegExpException,
     RETRY = RegExp::kInternalRegExpRetry,
   };
 
   // In case a StackOverflow occurs, a StackOverflowException is created and
   // EXCEPTION is returned.
-  static Result MatchForCallFromRuntime(Isolate* isolate,
-                                        Handle<JSRegExp> regexp,
-                                        Handle<String> subject_string,
-                                        int* registers, int registers_length,
-                                        int start_position);
+  static Result MatchForCallFromRuntime(
+      Isolate* isolate, Handle<JSRegExp> regexp, Handle<String> subject_string,
+      int* output_registers, int output_register_count, int start_position);
 
   // In case a StackOverflow occurs, EXCEPTION is returned. The caller is
   // responsible for creating the exception.
+  //
   // RETRY is returned if a retry through the runtime is needed (e.g. when
   // interrupts have been scheduled or the regexp is marked for tier-up).
+  //
   // Arguments input_start, input_end and backtrack_stack are
   // unused. They are only passed to match the signature of the native irregex
   // code.
+  //
+  // Arguments output_registers and output_register_count describe the results
+  // array, which will contain register values of all captures if SUCCESS is
+  // returned. For all other return codes, the results array remains unmodified.
   static Result MatchForCallFromJs(Address subject, int32_t start_position,
                                    Address input_start, Address input_end,
-                                   int* registers, int32_t registers_length,
+                                   int* output_registers,
+                                   int32_t output_register_count,
                                    Address backtrack_stack,
                                    RegExp::CallOrigin call_origin,
                                    Isolate* isolate, Address regexp);
 
   static Result MatchInternal(Isolate* isolate, ByteArray code_array,
-                              String subject_string, int* registers,
-                              int registers_length, int start_position,
+                              String subject_string, int* output_registers,
+                              int output_register_count,
+                              int total_register_count, int start_position,
                               RegExp::CallOrigin call_origin,
                               uint32_t backtrack_limit);
 
  private:
   static Result Match(Isolate* isolate, JSRegExp regexp, String subject_string,
-                      int* registers, int registers_length, int start_position,
-                      RegExp::CallOrigin call_origin);
+                      int* output_registers, int output_register_count,
+                      int start_position, RegExp::CallOrigin call_origin);
 };
 
 }  // namespace internal
 }  // namespace v8
 
 #endif  // V8_REGEXP_REGEXP_INTERPRETER_H_
diff --git a/js/src/new-regexp/regexp-macro-assembler.cc b/js/src/new-regexp/regexp-macro-assembler.cc
--- a/js/src/new-regexp/regexp-macro-assembler.cc
+++ b/js/src/new-regexp/regexp-macro-assembler.cc
@@ -101,16 +101,34 @@ bool RegExpMacroAssembler::CheckSpecialC
 }
 
 NativeRegExpMacroAssembler::NativeRegExpMacroAssembler(Isolate* isolate,
                                                        Zone* zone)
     : RegExpMacroAssembler(isolate, zone) {}
 
 NativeRegExpMacroAssembler::~NativeRegExpMacroAssembler() = default;
 
+void NativeRegExpMacroAssembler::LoadCurrentCharacterImpl(
+    int cp_offset, Label* on_end_of_input, bool check_bounds, int characters,
+    int eats_at_least) {
+  // It's possible to preload a small number of characters when each success
+  // path requires a large number of characters, but not the reverse.
+  DCHECK_GE(eats_at_least, characters);
+
+  DCHECK(base::IsInRange(cp_offset, kMinCPOffset, kMaxCPOffset));
+  if (check_bounds) {
+    if (cp_offset >= 0) {
+      CheckPosition(cp_offset + eats_at_least - 1, on_end_of_input);
+    } else {
+      CheckPosition(cp_offset, on_end_of_input);
+    }
+  }
+  LoadCurrentCharacterUnchecked(cp_offset, characters);
+}
+
 bool NativeRegExpMacroAssembler::CanReadUnaligned() {
   return FLAG_enable_regexp_unaligned_accesses && !slow_safe();
 }
 
 #ifndef COMPILING_IRREGEXP_FOR_EXTERNAL_EMBEDDER
 
 // This method may only be called after an interrupt.
 int NativeRegExpMacroAssembler::CheckStackGuardState(
diff --git a/js/src/new-regexp/regexp-macro-assembler.h b/js/src/new-regexp/regexp-macro-assembler.h
--- a/js/src/new-regexp/regexp-macro-assembler.h
+++ b/js/src/new-regexp/regexp-macro-assembler.h
@@ -23,23 +23,24 @@ struct DisjunctDecisionRow {
   RegExpCharacterClass cc;
   Label* on_match;
 };
 
 
 class RegExpMacroAssembler {
  public:
   // The implementation must be able to handle at least:
-  static const int kMaxRegister = (1 << 16) - 1;
-  static const int kMaxCPOffset = (1 << 15) - 1;
-  static const int kMinCPOffset = -(1 << 15);
+  static constexpr int kMaxRegisterCount = (1 << 16);
+  static constexpr int kMaxRegister = kMaxRegisterCount - 1;
+  static constexpr int kMaxCPOffset = (1 << 15) - 1;
+  static constexpr int kMinCPOffset = -(1 << 15);
 
-  static const int kTableSizeBits = 7;
-  static const int kTableSize = 1 << kTableSizeBits;
-  static const int kTableMask = kTableSize - 1;
+  static constexpr int kTableSizeBits = 7;
+  static constexpr int kTableSize = 1 << kTableSizeBits;
+  static constexpr int kTableMask = kTableSize - 1;
 
   static constexpr int kUseCharactersValue = -1;
 
   enum IrregexpImplementation {
     kIA32Implementation,
     kARMImplementation,
     kARM64Implementation,
     kMIPSImplementation,
@@ -267,14 +268,21 @@ class NativeRegExpMacroAssembler: public
   }
 
   // Returns a {Result} sentinel, or the number of successful matches.
   V8_EXPORT_PRIVATE static int Execute(String input, int start_offset,
                                        const byte* input_start,
                                        const byte* input_end, int* output,
                                        int output_size, Isolate* isolate,
                                        JSRegExp regexp);
+  void LoadCurrentCharacterImpl(int cp_offset, Label* on_end_of_input,
+                                bool check_bounds, int characters,
+                                int eats_at_least) override;
+  // Load a number of characters at the given offset from the
+  // current position, into the current-character register.
+  virtual void LoadCurrentCharacterUnchecked(int cp_offset,
+                                             int character_count) = 0;
 };
 
 }  // namespace internal
 }  // namespace v8
 
 #endif  // V8_REGEXP_REGEXP_MACRO_ASSEMBLER_H_
diff --git a/js/src/new-regexp/regexp-parser.cc b/js/src/new-regexp/regexp-parser.cc
--- a/js/src/new-regexp/regexp-parser.cc
+++ b/js/src/new-regexp/regexp-parser.cc
@@ -1007,19 +1007,19 @@ Handle<FixedArray> RegExpParser::CreateC
   Handle<FixedArray> array = factory->NewFixedArray(len);
 
   int i = 0;
   for (const auto& capture : sorted_named_captures) {
     Vector<const uc16> capture_name(capture->name()->data(),
                                     capture->name()->size());
     // CSA code in ConstructNewResultFromMatchInfo requires these strings to be
     // internalized so they can be used as property names in the 'exec' results.
-    Handle<String> name = factory->InternalizeString(capture_name);
-    array->set(i * 2, *name);
-    array->set(i * 2 + 1, Smi::FromInt(capture->index()));
+    // Handle<String> name = factory->InternalizeString(capture_name);
+    // array->set(i * 2, *name);
+    // array->set(i * 2 + 1, Smi::FromInt(capture->index()));
 
     i++;
   }
   DCHECK_EQ(i * 2, len);
 
   return array;
 }
 
diff --git a/js/src/new-regexp/regexp.h b/js/src/new-regexp/regexp.h
--- a/js/src/new-regexp/regexp.h
+++ b/js/src/new-regexp/regexp.h
@@ -22,17 +22,17 @@ struct RegExpCompileData {
   // The parsed AST as produced by the RegExpParser.
   RegExpTree* tree = nullptr;
 
   // The compiled Node graph as produced by RegExpTree::ToNode methods.
   RegExpNode* node = nullptr;
 
   // Either the generated code as produced by the compiler or a trampoline
   // to the interpreter.
-  Object code;
+  Handle<Object> code;
 
   // True, iff the pattern is a 'simple' atom with zero captures. In other
   // words, the pattern consists of a string with no metacharacters and special
   // regexp features, and can be implemented as a standard string search.
   bool simple = true;
 
   // True, iff the pattern is anchored at the start of the string with '^'.
   bool contains_anchor = false;
diff --git a/js/src/vm/JSContext.cpp b/js/src/vm/JSContext.cpp
--- a/js/src/vm/JSContext.cpp
+++ b/js/src/vm/JSContext.cpp
@@ -1359,16 +1359,20 @@ JSContext::~JSContext()
     js::jit::Simulator::Destroy(simulator_);
 #endif
 
 #ifdef JS_TRACE_LOGGING
     if (traceLogger)
         DestroyTraceLogger(traceLogger);
 #endif
 
+#ifdef JS_NEW_REGEXP
+    irregexp::DestroyIsolate(isolate.ref());
+#endif
+
     MOZ_ASSERT(TlsContext.get() == this);
     TlsContext.set(nullptr);
 }
 
 void
 JSContext::setRuntime(JSRuntime* rt)
 {
     MOZ_ASSERT(!resolvingList);
diff --git a/js/src/vm/RegExpObject.cpp b/js/src/vm/RegExpObject.cpp
--- a/js/src/vm/RegExpObject.cpp
+++ b/js/src/vm/RegExpObject.cpp
@@ -1069,32 +1069,39 @@ RegExpShared::compile(JSContext* cx,
 }
 /* static */
 bool
 RegExpShared::compileIfNecessary(JSContext* cx,
                                  MutableHandleRegExpShared re,
                                  HandleLinearString input,
                                  RegExpShared::CodeKind codeKind)
 {
+  if (codeKind == RegExpShared::CodeKind::Any) {
+    // We start by interpreting regexps, then compile them once they are
+    // sufficiently hot. For very long input strings, we tier up eagerly.
+    codeKind = RegExpShared::CodeKind::Bytecode;
+    if (IsNativeRegExpEnabled(cx) &&
+        (re->markedForTierUp(cx) || input->length() > 1000)) {
+      codeKind = RegExpShared::CodeKind::Jitcode;
+    }
+  }
+
   bool needsCompile = false;
   if (re->kind() == RegExpShared::Kind::Unparsed) {
     needsCompile = true;
   }
 
   if (re->kind() == RegExpShared::Kind::RegExp) {
-    if (codeKind == RegExpShared::CodeKind::Any && re->markedForTierUp(cx)) {
-      codeKind = RegExpShared::CodeKind::Jitcode;
-    }
     if (!re->isCompiled(input->hasLatin1Chars(), codeKind)) {
       needsCompile = true;
     }
   }
 
   if (needsCompile) {
-    return irregexp::CompilePattern(cx, re, input);
+    return irregexp::CompilePattern(cx, re, input, codeKind);
   }
   return true;
 }
 
 /* static */
 RegExpRunStatus
 RegExpShared::execute(JSContext* cx,
                       MutableHandleRegExpShared re,
@@ -1190,21 +1197,21 @@ void RegExpShared::useRegExpMatch(size_t
 
 void RegExpShared::tierUpTick() {
   MOZ_ASSERT(kind() == RegExpShared::Kind::RegExp);
   if (ticks_ > 0) {
     ticks_--;
   }
 }
 
-bool RegExpShared::markedForTierUp(JSContext* cx) {
+bool RegExpShared::markedForTierUp(JSContext* cx) const {
   if (!IsNativeRegExpEnabled(cx)) {
     return false;
   }
-  if (kind() == RegExpShared::Kind::Atom) {
+  if (kind() != RegExpShared::Kind::RegExp) {
     return false;
   }
   return ticks_ == 0;
 }
 
 #else   // !JS_NEW_REGEXP
 /* static */ bool
 RegExpShared::compile(JSContext* cx,
diff --git a/js/src/vm/RegExpShared.h b/js/src/vm/RegExpShared.h
--- a/js/src/vm/RegExpShared.h
+++ b/js/src/vm/RegExpShared.h
@@ -213,17 +213,17 @@ class RegExpShared : public gc::TenuredC
 
   // Use simple string matching for this regexp.
   void useAtomMatch(HandleAtom pattern);
 
   // Use the regular expression engine for this regexp.
   void useRegExpMatch(size_t parenCount);
 
   void tierUpTick();
-  bool markedForTierUp(JSContext* cx);
+  bool markedForTierUp(JSContext* cx) const;
 
   void setByteCode(ByteCode* code, bool latin1) {
     compilation(latin1).byteCode = code;
   }
   ByteCode* getByteCode(bool latin1) const {
     return compilation(latin1).byteCode;
   }
   void setJitCode(jit::JitCode* code, bool latin1) {
