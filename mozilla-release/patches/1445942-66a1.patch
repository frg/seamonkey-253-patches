# HG changeset patch
# User Neil Deakin <neil@mozilla.com>
# Date 1547040613 18000
# Node ID 72da18aff712e3bb7db72d638f26fa9177dcce00
# Parent  82f38f2eade988fffe0809fdae2bb5bd41ed31cb
Bug 1445942, make the backspace key a non-reserved key so that it isn't blocked by shortcut permissions, r=felipe

diff --git a/browser/base/content/browser-sets.inc b/browser/base/content/browser-sets.inc
--- a/browser/base/content/browser-sets.inc
+++ b/browser/base/content/browser-sets.inc
@@ -252,18 +252,18 @@
          key="&copyCmd.key;"
          modifiers="accel"/>
     <key id="key_paste"
          key="&pasteCmd.key;"
          modifiers="accel"/>
     <key id="key_delete" keycode="VK_DELETE" command="cmd_delete"/>
     <key id="key_selectAll" key="&selectAllCmd.key;" modifiers="accel"/>
 
-    <key keycode="VK_BACK" command="cmd_handleBackspace"/>
-    <key keycode="VK_BACK" command="cmd_handleShiftBackspace" modifiers="shift"/>
+    <key keycode="VK_BACK" command="cmd_handleBackspace" reserved="false"/>
+    <key keycode="VK_BACK" command="cmd_handleShiftBackspace" modifiers="shift" reserved="false"/>
 #ifndef XP_MACOSX
     <key id="goBackKb"  keycode="VK_LEFT" command="Browser:Back" modifiers="alt"/>
     <key id="goForwardKb"  keycode="VK_RIGHT" command="Browser:Forward" modifiers="alt"/>
 #else
     <key id="goBackKb" keycode="VK_LEFT" command="Browser:Back" modifiers="accel" />
     <key id="goForwardKb" keycode="VK_RIGHT" command="Browser:Forward" modifiers="accel" />
 #endif
 #ifdef XP_UNIX
diff --git a/browser/base/content/test/permissions/browser_reservedkey.js b/browser/base/content/test/permissions/browser_reservedkey.js
--- a/browser/base/content/test/permissions/browser_reservedkey.js
+++ b/browser/base/content/test/permissions/browser_reservedkey.js
@@ -81,8 +81,47 @@ if (!navigator.platform.includes("Mac"))
 
     popupHidden = BrowserTestUtils.waitForEvent(filePopup, "popuphidden");
     filePopup.hidePopup();
     await popupHidden;
 
     BrowserTestUtils.removeTab(tab1);
   });
 }
+
+// There is a <key> element for Backspace with reserved="false", so make sure that it is not
+// treated as a blocked shortcut key.
+add_task(async function test_backspace() {
+  await new Promise(resolve => {
+    SpecialPowers.pushPrefEnv({"set": [["permissions.default.shortcuts", 2]]}, resolve);
+  });
+
+  // The input field is autofocused. If this test fails, backspace can go back
+  // in history so cancel the beforeunload event and adjust the field to make the test fail.
+  const uri = "data:text/html,<body onbeforeunload='document.getElementById(\"field\").value = \"failed\";'>" +
+                 "<input id='field' value='something'></body>";
+  let tab = await BrowserTestUtils.openNewForegroundTab(gBrowser, uri);
+
+  await ContentTask.spawn(tab.linkedBrowser, { }, async function() {
+    content.document.getElementById("field").focus();
+
+    // Add a promise that resolves when the backspace key gets received
+    // so we can ensure the key gets received before checking the result.
+    content.keysPromise = new Promise(resolve => {
+      content.addEventListener("keyup", event => {
+        if (event.code == "Backspace") {
+          resolve(content.document.getElementById("field").value);
+        }
+      });
+    });
+  });
+
+  // Move the caret so backspace will delete the first character.
+  EventUtils.synthesizeKey("KEY_ArrowRight", {});
+  EventUtils.synthesizeKey("KEY_Backspace", {});
+
+  let fieldValue = await ContentTask.spawn(tab.linkedBrowser, { }, async function() {
+    return content.keysPromise;
+  });
+  is(fieldValue, "omething", "backspace not prevented");
+
+  BrowserTestUtils.removeTab(tab);
+});
