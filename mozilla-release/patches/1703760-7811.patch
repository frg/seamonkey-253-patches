# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1618989261 0
# Node ID b203605dcd4bec5d95b1d5b5f70a0b1b7c91ab7f
# Parent  6d533e7d0162348e8156040a400a69ed4c4e1c4c
Bug 1703760 - Fix invalid early return in BaselineFrame::trace. r=tcampbell, a=RyanVM

We were not tracing debugger environments for Baseline frames without any local/expression slots.

Differential Revision: https://phabricator.services.mozilla.com/D112741

diff --git a/js/src/jit-test/tests/debug/bug1703760.js b/js/src/jit-test/tests/debug/bug1703760.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/debug/bug1703760.js
@@ -0,0 +1,18 @@
+let g = newGlobal({newCompartment: true});
+
+Debugger(g).onEnterFrame = function(fr) {
+    fr.eval("")
+};
+
+gczeal(11);
+gczeal(9, 9);
+
+g.eval(`
+  var count = 0;
+  function inner() {
+    if (++count < 20) {
+      inner();
+    }
+  }
+  inner();
+`);
diff --git a/js/src/jit/BaselineFrame.cpp b/js/src/jit/BaselineFrame.cpp
--- a/js/src/jit/BaselineFrame.cpp
+++ b/js/src/jit/BaselineFrame.cpp
@@ -22,71 +22,74 @@ TraceLocals(BaselineFrame* frame, JSTrac
 {
     if (start < end) {
         // Stack grows down.
         Value* last = frame->valueSlot(end - 1);
         TraceRootRange(trc, end - start, last, "baseline-stack");
     }
 }
 
-void
-BaselineFrame::trace(JSTracer* trc, const JSJitFrameIter& frameIterator)
-{
-    replaceCalleeToken(TraceCalleeToken(trc, calleeToken()));
+void BaselineFrame::trace(JSTracer* trc, const JSJitFrameIter& frameIterator) {
+  replaceCalleeToken(TraceCalleeToken(trc, calleeToken()));
+
+  // Trace |this|, actual and formal args.
+  if (isFunctionFrame()) {
+    TraceRoot(trc, &thisArgument(), "baseline-this");
 
-    // Trace |this|, actual and formal args.
-    if (isFunctionFrame()) {
-        TraceRoot(trc, &thisArgument(), "baseline-this");
+    unsigned numArgs = std::max(numActualArgs(), numFormalArgs());
+    TraceRootRange(trc, numArgs + isConstructing(), argv(), "baseline-args");
+  }
 
-        unsigned numArgs = js::Max(numActualArgs(), numFormalArgs());
-        TraceRootRange(trc, numArgs + isConstructing(), argv(), "baseline-args");
-    }
-
-    // Trace environment chain, if it exists.
-    if (envChain_)
-        TraceRoot(trc, &envChain_, "baseline-envchain");
+  // Trace environment chain, if it exists.
+  if (envChain_) {
+    TraceRoot(trc, &envChain_, "baseline-envchain");
+  }
 
-    // Trace return value.
-    if (hasReturnValue())
-        TraceRoot(trc, returnValue().address(), "baseline-rval");
+  // Trace return value.
+  if (hasReturnValue()) {
+    TraceRoot(trc, returnValue().address(), "baseline-rval");
+  }
 
-    if (isEvalFrame() && script()->isDirectEvalInFunction())
-        TraceRoot(trc, evalNewTargetAddress(), "baseline-evalNewTarget");
-
-    if (hasArgsObj())
-        TraceRoot(trc, &argsObj_, "baseline-args-obj");
+  if (isEvalFrame() && script()->isDirectEvalInFunction()) {
+    TraceRoot(trc, evalNewTargetAddress(), "baseline-evalNewTarget");
+  }
 
-    // Trace locals and stack values.
-    JSScript* script = this->script();
-    size_t nfixed = script->nfixed();
-    jsbytecode* pc;
-    frameIterator.baselineScriptAndPc(nullptr, &pc);
-    size_t nlivefixed = script->calculateLiveFixed(pc);
+  if (hasArgsObj()) {
+    TraceRoot(trc, &argsObj_, "baseline-args-obj");
+  }
 
-    // NB: It is possible that numValueSlots() could be zero, even if nfixed is
-    // nonzero.  This is the case if the function has an early stack check.
-    if (numValueSlots() == 0)
-        return;
+  // Trace locals and stack values.
+  JSScript* script = this->script();
+  size_t nfixed = script->nfixed();
+  jsbytecode* pc;
+  frameIterator.baselineScriptAndPc(nullptr, &pc);
+  size_t nlivefixed = script->calculateLiveFixed(pc);
 
+  // NB: It is possible that numValueSlots could be zero, even if nfixed is
+  // nonzero.  This is the case when we're initializing the environment chain or
+  // failed the prologue stack check.
+  if (numValueSlots() > 0) {
     MOZ_ASSERT(nfixed <= numValueSlots());
 
     if (nfixed == nlivefixed) {
-        // All locals are live.
-        TraceLocals(this, trc, 0, numValueSlots());
+      // All locals are live.
+      TraceLocals(this, trc, 0, numValueSlots());
     } else {
-        // Trace operand stack.
-        TraceLocals(this, trc, nfixed, numValueSlots());
+      // Trace operand stack.
+      TraceLocals(this, trc, nfixed, numValueSlots());
 
-        // Clear dead block-scoped locals.
-        while (nfixed > nlivefixed)
-            unaliasedLocal(--nfixed).setUndefined();
+      // Clear dead block-scoped locals.
+      while (nfixed > nlivefixed) {
+        unaliasedLocal(--nfixed).setUndefined();
+      }
 
-        // Trace live locals.
-        TraceLocals(this, trc, 0, nlivefixed);
+      // Trace live locals.
+      TraceLocals(this, trc, 0, nlivefixed);
     }
+  }
 
     if (script->compartment()->debugEnvs)
         script->compartment()->debugEnvs->traceLiveFrame(trc, this);
 }
 
 bool
 BaselineFrame::isNonGlobalEvalFrame() const
 {
