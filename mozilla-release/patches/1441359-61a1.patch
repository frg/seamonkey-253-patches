# HG changeset patch
# User Justin Wood <Callek@gmail.com>
# Date 1522347047 14400
# Node ID eead32dddd0222c20c4527cbda8639a9cbcc7e63
# Parent  305aefdbaf13dbd515248c4f4762a576f03ae91d
Bug 1441359 - Make langpack version include buildid. r=mshal, a=RyanVM

For Automated submission to AMO we need unique language pack versions for every submitted build.

This will produce versions that are not valid for AMO on nightly (61.0a1buildid2018...) but that's ok, we're not ready to
submit these automatically on the nightly channel yet, they do still install fine into Firefox manually.

On beta/release/esr the version will however be ok for AMO "60.0buildid2018..." and will be reasonably unique per push.

This patch has the intent that, outside of automation, users who don't set a buildid get a langpack version without that
specified (rather than a buildid being automatically generated for them).

The releng scriptworker (addonscript) that will do the publishing to AMO will additionally sanity check that the string
'buildid' is present in the version part, so we don't accidentally break this functionality in production.

This patch is also intended to be uplifted to Gecko 60 to support ESR60 needs.

MozReview-Commit-ID: KuvwMyD6bwd

diff --git a/python/mozbuild/mozbuild/action/langpack_manifest.py b/python/mozbuild/mozbuild/action/langpack_manifest.py
--- a/python/mozbuild/mozbuild/action/langpack_manifest.py
+++ b/python/mozbuild/mozbuild/action/langpack_manifest.py
@@ -284,16 +284,40 @@ def parse_chrome_manifest(path, base_pat
                 'locale': entry.id,
                 'platforms': convert_entry_flags_to_platform_codes(entry.flags),
                 'path': mozpath.normsep(entry_path)
             })
         else:
             raise Exception('Unknown type {0}'.format(entry.name))
 
 
+##
+# Gets the version to use in the langpack.
+#
+# This uses the env variable MOZ_BUILD_DATE if it exists to expand the version to be unique
+# in automation.
+#
+# Args:
+#    min_version - Application version
+#
+# Returns:
+#    str - Version to use, may include buildid
+#
+###
+def get_version_maybe_buildid(min_version):
+    version = str(min_version)
+    buildid = os.environ.get('MOZ_BUILD_DATE')
+    if buildid and len(buildid) != 14:
+        print >>sys.stderr, 'Ignoring invalid MOZ_BUILD_DATE: %s' % buildid
+        buildid = None
+    if buildid:
+        version = version + "buildid" + buildid
+    return version
+
+
 ###
 # Generates a new web manifest dict with values specific for a language pack.
 #
 # Args:
 #    locstr         (str)  - A string with a comma separated list of locales
 #                            for which resources are embedded in the
 #                            language pack
 #    min_app_ver    (str)  - A minimum version of the application the language
@@ -370,17 +394,17 @@ def create_webmanifest(locstr, min_app_v
             'gecko': {
                 'id': 'langpack-{0}@firefox.mozilla.org'.format(main_locale),
                 'strict_min_version': min_app_ver,
                 'strict_max_version': max_app_ver,
             }
         },
         'name': '{0} Language Pack'.format(defines['MOZ_LANG_TITLE']),
         'description': 'Language pack for {0} for {1}'.format(app_name, main_locale),
-        'version': min_app_ver,
+        'version': get_version_maybe_buildid(min_app_ver),
         'languages': {},
         'sources': {
             'browser': {
                 'base_path': 'browser/'
             }
         },
         'author': author
     }

