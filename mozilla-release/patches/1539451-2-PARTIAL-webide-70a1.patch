# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1563294226 0
# Node ID 124a38ea2ab287d0a8e7d52ada945ee5a91cff9b
# Parent  d5b98ce460d5b6fe6c6d732548d2090e1f1481b8
Bug 1539451 - Disable WebIDE and ConnectPage by default r=remote-debugging-reviewers,daisuke

Only disable WebIDE.

Differential Revision: https://phabricator.services.mozilla.com/D37860

diff --git a/devtools/client/preferences/devtools.js b/devtools/client/preferences/devtools.js
--- a/devtools/client/preferences/devtools.js
+++ b/devtools/client/preferences/devtools.js
@@ -1,17 +1,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 // Developer toolbar preferences
 pref("devtools.toolbar.enabled", true);
 
-// Enable DevTools WebIDE by default
-pref("devtools.webide.enabled", true);
+// Disable WebIDE by default (Bug 1539451)
+pref("devtools.webide.enabled", false);
 
 // Toolbox preferences
 pref("devtools.toolbox.footer.height", 250);
 pref("devtools.toolbox.sidebar.width", 500);
 pref("devtools.toolbox.host", "bottom");
 pref("devtools.toolbox.previousHost", "side");
 pref("devtools.toolbox.selectedTool", "webconsole");
 pref("devtools.toolbox.sideEnabled", true);
diff --git a/devtools/startup/devtools-startup.js b/devtools/startup/devtools-startup.js
--- a/devtools/startup/devtools-startup.js
+++ b/devtools/startup/devtools-startup.js
@@ -25,16 +25,18 @@ const kDebuggerPrefs = [
   "devtools.debugger.remote-enabled",
   "devtools.chrome.enabled"
 ];
 
 // If devtools.toolbar.visible is set to true, the developer toolbar should appear on
 // startup.
 const TOOLBAR_VISIBLE_PREF = "devtools.toolbar.visible";
 
+const WEBIDE_ENABLED_PREF = "devtools.webide.enabled";
+
 const DEVTOOLS_ENABLED_PREF = "devtools.enabled";
 
 const DEVTOOLS_POLICY_DISABLED_PREF = "devtools.policy.disabled";
 
 const { XPCOMUtils } = ChromeUtils.import("resource://gre/modules/XPCOMUtils.jsm", {});
 ChromeUtils.defineModuleGetter(this, "Services",
                                "resource://gre/modules/Services.jsm");
 ChromeUtils.defineModuleGetter(this, "AppConstants",
@@ -57,17 +59,17 @@ XPCOMUtils.defineLazyGetter(this, "KeySh
 XPCOMUtils.defineLazyGetter(this, "KeyShortcuts", function() {
   const isMac = AppConstants.platform == "macosx";
 
   // Common modifier shared by most key shortcuts
   const modifiers = isMac ? "accel,alt" : "accel,shift";
 
   // List of all key shortcuts triggering installation UI
   // `id` should match tool's id from client/definitions.js
-  return [
+  let shortcuts = [
     // The following keys are also registered in /client/menus.js
     // And should be synced.
 
     // Both are toggling the toolbox on the last selected panel
     // or the default one.
     {
       id: "toggleToolbox",
       shortcut: KeyShortcutsBundle.GetStringFromName("toggleToolbox.commandkey"),
@@ -80,22 +82,16 @@ XPCOMUtils.defineLazyGetter(this, "KeySh
       modifiers: "" // F12 is the only one without modifiers
     },
     // Toggle the visibility of the Developer Toolbar (=gcli)
     {
       id: "toggleToolbar",
       shortcut: KeyShortcutsBundle.GetStringFromName("toggleToolbar.commandkey"),
       modifiers: "shift"
     },
-    // Open WebIDE window
-    {
-      id: "webide",
-      shortcut: KeyShortcutsBundle.GetStringFromName("webide.commandkey"),
-      modifiers: "shift"
-    },
     // Open the Browser Toolbox
     {
       id: "browserToolbox",
       shortcut: KeyShortcutsBundle.GetStringFromName("browserToolbox.commandkey"),
       modifiers: "accel,alt,shift"
     },
     // Open the Browser Console
     {
@@ -163,16 +159,32 @@ XPCOMUtils.defineLazyGetter(this, "KeySh
     },
     // Key for opening the DOM Panel
     {
       toolId: "dom",
       shortcut: KeyShortcutsBundle.GetStringFromName("dom.commandkey"),
       modifiers
     },
   ];
+
+  // Only add the WebIDE shortcut if WebIDE is enabled.
+  const isWebIDEEnabled = Services.prefs.getBoolPref(
+    WEBIDE_ENABLED_PREF,
+    false
+  );
+  if (isWebIDEEnabled) {
+    // Open WebIDE window
+    shortcuts.push({
+      id: "webide",
+      shortcut: KeyShortcutsBundle.GetStringFromName("webide.commandkey"),
+      modifiers: "shift",
+    });
+  }
+
+  return shortcuts;
 });
 
 function DevToolsStartup() {
   this.onEnabledPrefChanged = this.onEnabledPrefChanged.bind(this);
   this.onWindowReady = this.onWindowReady.bind(this);
 }
 
 DevToolsStartup.prototype = {
