# HG changeset patch
# User Valentin Gosu <valentin.gosu@gmail.com>
# Date 1519171254 -3600
# Node ID 5caa81103c0070a52d1b2a30b854352e462dc5b3
# Parent  68ed1d4590e2cea7bdc2505120b862cf1eb9e99c
Bug 1435671 - Reduce binary size by making NS_MutateURI.Apply not be a templated method r=mayhemer

We instead add a templated method NS_MutatorMethod that returns a std::function<nsresult(nsIURIMutator*)> which Apply then calls with mMutator as an argument.
The function returned by NS_MutatorMethod performs a QueryInterface, then calls the passed method with arguments on the result.

MozReview-Commit-ID: Jjqp7gGLG1D

diff --git a/chrome/nsChromeProtocolHandler.cpp b/chrome/nsChromeProtocolHandler.cpp
--- a/chrome/nsChromeProtocolHandler.cpp
+++ b/chrome/nsChromeProtocolHandler.cpp
@@ -73,20 +73,22 @@ nsChromeProtocolHandler::NewURI(const ns
                                 nsIURI *aBaseURI,
                                 nsIURI **result)
 {
 
     // Chrome: URLs (currently) have no additional structure beyond that provided
     // by standard URLs, so there is no "outer" given to CreateInstance
     nsresult rv;
     nsCOMPtr<nsIURL> surl;
+    nsCOMPtr<nsIURI> base(aBaseURI);
     rv = NS_MutateURI(new mozilla::net::nsStandardURL::Mutator())
-           .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                         nsIStandardURL::URLTYPE_STANDARD, -1,
-                                         nsCString(aSpec), aCharset, aBaseURI, nullptr)
+           .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                                   nsIStandardURL::URLTYPE_STANDARD,
+                                   -1, nsCString(aSpec), aCharset,
+                                   base, nullptr))
            .Finalize(surl);
     if (NS_FAILED(rv)) {
         return rv;
     }
 
     // Canonify the "chrome:" URL; e.g., so that we collapse
     // "chrome://navigator/content/" and "chrome://navigator/content"
     // and "chrome://navigator/content/navigator.xul".
diff --git a/dom/base/nsContentAreaDragDrop.cpp b/dom/base/nsContentAreaDragDrop.cpp
--- a/dom/base/nsContentAreaDragDrop.cpp
+++ b/dom/base/nsContentAreaDragDrop.cpp
@@ -595,19 +595,18 @@ DragDataProducer::Produce(DataTransfer* 
                   NS_FAILED(mimeInfo->ExtensionExists(extension,
                                                       &validExtension)) ||
                   !validExtension) {
                 // Fix the file extension in the URL
                 nsAutoCString primaryExtension;
                 mimeInfo->GetPrimaryExtension(primaryExtension);
 
                 rv = NS_MutateURI(imgUrl)
-                       .Apply<nsIURLMutator>(&nsIURLMutator::SetFileExtension,
-                                             primaryExtension,
-                                             nullptr)
+                       .Apply(NS_MutatorMethod(&nsIURLMutator::SetFileExtension,
+                                               primaryExtension, nullptr))
                        .Finalize(imgUrl);
                 NS_ENSURE_SUCCESS(rv, rv);
               }
 
               nsAutoCString fileName;
               imgUrl->GetFileName(fileName);
 
               NS_UnescapeURL(fileName);
diff --git a/dom/base/nsCopySupport.cpp b/dom/base/nsCopySupport.cpp
--- a/dom/base/nsCopySupport.cpp
+++ b/dom/base/nsCopySupport.cpp
@@ -643,19 +643,18 @@ static nsresult AppendImagePromise(nsITr
       NS_FAILED(mimeInfo->ExtensionExists(extension,
                                           &validExtension)) ||
       !validExtension) {
     // Fix the file extension in the URL
     nsAutoCString primaryExtension;
     mimeInfo->GetPrimaryExtension(primaryExtension);
 
     rv = NS_MutateURI(imgUri)
-           .Apply<nsIURLMutator>(&nsIURLMutator::SetFileExtension,
-                                 primaryExtension,
-                                 nullptr)
+           .Apply(NS_MutatorMethod(&nsIURLMutator::SetFileExtension,
+                                   primaryExtension, nullptr))
            .Finalize(imgUrl);
     NS_ENSURE_SUCCESS(rv, rv);
   }
 
   nsAutoCString fileName;
   imgUrl->GetFileName(fileName);
 
   NS_UnescapeURL(fileName);
diff --git a/dom/file/nsHostObjectProtocolHandler.cpp b/dom/file/nsHostObjectProtocolHandler.cpp
--- a/dom/file/nsHostObjectProtocolHandler.cpp
+++ b/dom/file/nsHostObjectProtocolHandler.cpp
@@ -894,18 +894,18 @@ nsHostObjectProtocolHandler::NewURI(cons
     MOZ_ASSERT(info->mBlobImpl);
     principal = info->mPrincipal;
     blob = info->mBlobImpl;
   }
 
   nsCOMPtr<nsIURI> uri;
   rv = NS_MutateURI(new nsHostObjectURI::Mutator())
          .SetSpec(aSpec)
-         .Apply<nsIBlobURIMutator>(&nsIBlobURIMutator::SetBlobImpl, blob)
-         .Apply<nsIPrincipalURIMutator>(&nsIPrincipalURIMutator::SetPrincipal, principal)
+         .Apply(NS_MutatorMethod(&nsIBlobURIMutator::SetBlobImpl, blob))
+         .Apply(NS_MutatorMethod(&nsIPrincipalURIMutator::SetPrincipal, principal))
          .Finalize(uri);
   NS_ENSURE_SUCCESS(rv, rv);
 
   NS_TryToSetImmutable(uri);
   uri.forget(aResult);
 
   if (info && info->mObjectType == DataInfo::eBlobImpl) {
     info->mURIs.AppendElement(do_GetWeakReference(*aResult));
diff --git a/dom/url/URLWorker.cpp b/dom/url/URLWorker.cpp
--- a/dom/url/URLWorker.cpp
+++ b/dom/url/URLWorker.cpp
@@ -644,21 +644,21 @@ URLWorker::Init(const nsAString& aURL, c
       }
       if (NS_WARN_IF(NS_FAILED(rv)) || baseScheme.IsEmpty()) {
         aRv.ThrowTypeError<MSG_INVALID_URL>(aBase.Value());
         return;
       }
     }
     nsCOMPtr<nsIURI> uri;
     rv = NS_MutateURI(new nsStandardURL::Mutator())
-            .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                          nsIStandardURL::URLTYPE_STANDARD, -1,
-                                          NS_ConvertUTF16toUTF8(aURL),
-                                          nullptr, baseURL, nullptr)
-            .Finalize(uri);
+           .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                                   nsIStandardURL::URLTYPE_STANDARD,
+                                   -1, NS_ConvertUTF16toUTF8(aURL),
+                                   nullptr, baseURL, nullptr))
+           .Finalize(uri);
     aRv = rv;
     if (NS_SUCCEEDED(rv)) {
       mStdURL = static_cast<nsStandardURL*>(uri.get());
     }
     return;
   }
 
   // create url proxy
diff --git a/dom/webbrowserpersist/nsWebBrowserPersist.cpp b/dom/webbrowserpersist/nsWebBrowserPersist.cpp
--- a/dom/webbrowserpersist/nsWebBrowserPersist.cpp
+++ b/dom/webbrowserpersist/nsWebBrowserPersist.cpp
@@ -2114,21 +2114,20 @@ nsWebBrowserPersist::CalculateUniqueFile
             // Resync the URI with the file after the extension has been appended
             nsresult rv;
             nsCOMPtr<nsIFileURL> fileURL = do_QueryInterface(aURI, &rv);
             NS_ENSURE_SUCCESS(rv, NS_ERROR_FAILURE);
             fileURL->SetFile(localFile);  // this should recalculate uri
         }
         else
         {
-            return NS_MutateURI(url)
-                     .Apply<nsIURLMutator>(&nsIURLMutator::SetFileName,
-                                           filename,
-                                           nullptr)
-                     .Finalize(aOutURI);
+          return NS_MutateURI(url)
+                   .Apply(NS_MutatorMethod(&nsIURLMutator::SetFileName,
+                                           filename, nullptr))
+                   .Finalize(aOutURI);
         }
     }
 
     // TODO (:valentin) This method should always clone aURI
     aOutURI = aURI;
     return NS_OK;
 }
 
@@ -2292,21 +2291,20 @@ nsWebBrowserPersist::CalculateAndAppendF
 
                     // Resync the URI with the file after the extension has been appended
                     nsCOMPtr<nsIFileURL> fileURL = do_QueryInterface(aURI, &rv);
                     NS_ENSURE_SUCCESS(rv, NS_ERROR_FAILURE);
                     fileURL->SetFile(localFile);  // this should recalculate uri
                 }
                 else
                 {
-                    return NS_MutateURI(url)
-                             .Apply<nsIURLMutator>(&nsIURLMutator::SetFileName,
-                                                   newFileName,
-                                                   nullptr)
-                             .Finalize(aOutURI);
+                  return NS_MutateURI(url)
+                           .Apply(NS_MutatorMethod(&nsIURLMutator::SetFileName,
+                                                   newFileName, nullptr))
+                           .Finalize(aOutURI);
                 }
             }
 
         }
     }
 
     // TODO (:valentin) This method should always clone aURI
     aOutURI = aURI;
diff --git a/modules/libjar/nsJARURI.cpp b/modules/libjar/nsJARURI.cpp
--- a/modules/libjar/nsJARURI.cpp
+++ b/modules/libjar/nsJARURI.cpp
@@ -92,21 +92,20 @@ nsresult
 nsJARURI::CreateEntryURL(const nsACString& entryFilename,
                          const char* charset,
                          nsIURL** url)
 {
     *url = nullptr;
     // Flatten the concatenation, just in case.  See bug 128288
     nsAutoCString spec(NS_BOGUS_ENTRY_SCHEME + entryFilename);
     return NS_MutateURI(NS_STANDARDURLMUTATOR_CONTRACTID)
-             .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                           nsIStandardURL::URLTYPE_NO_AUTHORITY, -1,
-                                           spec, charset, nullptr,
-                                           nullptr)
-             .Finalize(url);
+        .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                                nsIStandardURL::URLTYPE_NO_AUTHORITY, -1,
+                                spec, charset, nullptr, nullptr))
+        .Finalize(url);
 }
 
 ////////////////////////////////////////////////////////////////////////////////
 // nsISerializable methods:
 
 NS_IMETHODIMP
 nsJARURI::Read(nsIObjectInputStream* aInputStream)
 {
@@ -327,21 +326,20 @@ nsJARURI::SetSpecWithBase(const nsACStri
         aBaseURL->QueryInterface(NS_GET_IID(nsJARURI), getter_AddRefs(otherJAR));
         NS_ENSURE_TRUE(otherJAR, NS_NOINTERFACE);
 
         mJARFile = otherJAR->mJARFile;
 
         nsCOMPtr<nsIURI> entry;
 
         rv = NS_MutateURI(NS_STANDARDURLMUTATOR_CONTRACTID)
-               .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                             nsIStandardURL::URLTYPE_NO_AUTHORITY, -1,
-                                             nsCString(aSpec), mCharsetHint.get(),
-                                             otherJAR->mJAREntry,
-                                             nullptr)
+               .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                                       nsIStandardURL::URLTYPE_NO_AUTHORITY,
+                                       -1, nsCString(aSpec), mCharsetHint.get(),
+                                       otherJAR->mJAREntry, nullptr))
                .Finalize(entry);
         if (NS_FAILED(rv)) {
             return rv;
         }
 
         mJAREntry = do_QueryInterface(entry);
         if (!mJAREntry)
             return NS_NOINTERFACE;
@@ -716,52 +714,49 @@ nsJARURI::GetFileName(nsACString& fileNa
 {
     return mJAREntry->GetFileName(fileName);
 }
 
 NS_IMETHODIMP
 nsJARURI::SetFileName(const nsACString& fileName)
 {
     return NS_MutateURI(mJAREntry)
-             .Apply<nsIURLMutator>(&nsIURLMutator::SetFileName,
-                                   nsCString(fileName),
-                                   nullptr)
-             .Finalize(mJAREntry);
+        .Apply(NS_MutatorMethod(&nsIURLMutator::SetFileName,
+                                nsCString(fileName), nullptr))
+        .Finalize(mJAREntry);
 }
 
 NS_IMETHODIMP
 nsJARURI::GetFileBaseName(nsACString& fileBaseName)
 {
     return mJAREntry->GetFileBaseName(fileBaseName);
 }
 
 NS_IMETHODIMP
 nsJARURI::SetFileBaseName(const nsACString& fileBaseName)
 {
     return NS_MutateURI(mJAREntry)
-             .Apply<nsIURLMutator>(&nsIURLMutator::SetFileBaseName,
-                                   nsCString(fileBaseName),
-                                   nullptr)
-             .Finalize(mJAREntry);
+        .Apply(NS_MutatorMethod(&nsIURLMutator::SetFileBaseName,
+                                nsCString(fileBaseName), nullptr))
+        .Finalize(mJAREntry);
 }
 
 NS_IMETHODIMP
 nsJARURI::GetFileExtension(nsACString& fileExtension)
 {
     return mJAREntry->GetFileExtension(fileExtension);
 }
 
 NS_IMETHODIMP
 nsJARURI::SetFileExtension(const nsACString& fileExtension)
 {
     return NS_MutateURI(mJAREntry)
-             .Apply<nsIURLMutator>(&nsIURLMutator::SetFileExtension,
-                                   nsCString(fileExtension),
-                                   nullptr)
-             .Finalize(mJAREntry);
+        .Apply(NS_MutatorMethod(&nsIURLMutator::SetFileExtension,
+                                nsCString(fileExtension), nullptr))
+        .Finalize(mJAREntry);
 }
 
 NS_IMETHODIMP
 nsJARURI::GetCommonBaseSpec(nsIURI* uriToCompare, nsACString& commonSpec)
 {
     commonSpec.Truncate();
 
     NS_ENSURE_ARG_POINTER(uriToCompare);
diff --git a/netwerk/base/nsIURIMutator.idl b/netwerk/base/nsIURIMutator.idl
--- a/netwerk/base/nsIURIMutator.idl
+++ b/netwerk/base/nsIURIMutator.idl
@@ -6,16 +6,17 @@
 #include "nsISupports.idl"
 interface nsIURI;
 interface nsIObjectInputStream;
 interface nsIURIMutator;
 
 %{C++
 #include "nsString.h"
 #include "nsCOMPtr.h"
+#include <functional>
 
 #undef SetPort  // XXX Windows!
 
 namespace mozilla {
 class Encoding;
 }
 
 namespace mozilla {
@@ -266,16 +267,55 @@ interface nsIURIMutator : nsIURISetters
    * Finishes changing or constructing the URI and returns an immutable URI.
    */
   [must_use]
   nsIURI finalize();
 };
 
 %{C++
 
+// This templated struct is used to extract the class type of the method
+// passed to NS_MutatorMethod.
+template <typename Method>
+struct nsMethodTypeTraits;
+
+template <class C, typename R, typename... As>
+struct nsMethodTypeTraits<R(C::*)(As...)>
+{
+  typedef C class_type;
+};
+
+#ifdef NS_HAVE_STDCALL
+template <class C, typename R, typename... As>
+struct nsMethodTypeTraits<R(__stdcall C::*)(As...)>
+{
+  typedef C class_type;
+};
+#endif
+
+// This helper returns a std::function that will be applied on the
+// nsIURIMutator. The type of `Interface` will be deduced from the method type.
+// aMethod will be called on the target object if it successfully QIs to
+// `Interface`, and the arguments will be passed to the call.
+template <typename Method, typename... Args>
+const std::function<nsresult(nsIURIMutator*)>
+NS_MutatorMethod(Method aMethod, Args ...aArgs)
+{
+  // Capture arguments by value, otherwise we crash.
+  return [=](nsIURIMutator* aMutator) {
+    typedef typename nsMethodTypeTraits<Method>::class_type Interface;
+    nsresult rv;
+    nsCOMPtr<Interface> target = do_QueryInterface(aMutator, &rv);
+    NS_ENSURE_SUCCESS(rv, rv);
+    rv = (target->*aMethod)(aArgs...);
+    NS_ENSURE_SUCCESS(rv, rv);
+    return NS_OK;
+  };
+}
+
 // This class provides a useful helper that allows chaining of setter operations
 class MOZ_STACK_CLASS NS_MutateURI
 {
 public:
   explicit NS_MutateURI(nsIURI* aURI);
   explicit NS_MutateURI(const char * aContractID);
 
   explicit NS_MutateURI(nsIURIMutator* m)
@@ -372,31 +412,29 @@ public:
   /**
    * This method allows consumers to call the methods declared in other
    * interfaces implemented by the mutator object.
    *
    * Example:
    * nsCOMPtr<nsIURI> uri;
    * nsresult rv = NS_MutateURI(new URIClass::Mutator())
    *                 .SetSpec(aSpec)
-   *                 .Apply<SomeInterface>(&SomeInterface::Method, arg1, arg2)
+   *                 .Apply(NS_MutatorMethod(&SomeInterface::Method, arg1, arg2))
    *                 .Finalize(uri);
    *
    * If mMutator does not implement SomeInterface, do_QueryInterface will fail
    * and the method will not be called.
    * If aMethod does not exist, or if there is a mismatch between argument
    * types, or the number of arguments, then there will be a compile error.
    */
-  template <typename Interface, typename Method, typename... Args>
-  NS_MutateURI& Apply(Method aMethod, Args ...aArgs)
+  NS_MutateURI& Apply(const std::function<nsresult(nsIURIMutator*)>& aFunction)
   {
     NS_ENSURE_SUCCESS(mStatus, *this);
-    nsCOMPtr<Interface> target = do_QueryInterface(mMutator, &mStatus);
+    mStatus = aFunction(mMutator);
     NS_ENSURE_SUCCESS(mStatus, *this);
-    mStatus = (target->*aMethod)(aArgs...);
     return *this;
   }
 
   template <class C>
   MOZ_MUST_USE nsresult Finalize(nsCOMPtr<C>& aURI)
   {
     NS_ENSURE_SUCCESS(mStatus, mStatus);
 
diff --git a/netwerk/base/nsNetUtil.cpp b/netwerk/base/nsNetUtil.cpp
--- a/netwerk/base/nsNetUtil.cpp
+++ b/netwerk/base/nsNetUtil.cpp
@@ -2824,17 +2824,17 @@ nsresult
 NS_GetSecureUpgradedURI(nsIURI* aURI, nsIURI** aUpgradedURI)
 {
   NS_MutateURI mutator(aURI);
   mutator.SetScheme(NS_LITERAL_CSTRING("https")); // Change the scheme to HTTPS:
 
   // Change the default port to 443:
   nsCOMPtr<nsIStandardURL> stdURL = do_QueryInterface(aURI);
   if (stdURL) {
-    mutator.Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::SetDefaultPort, 443, nullptr);
+    mutator.Apply(NS_MutatorMethod(&nsIStandardURLMutator::SetDefaultPort, 443, nullptr));
   } else {
     // If we don't have a nsStandardURL, fall back to using GetPort/SetPort.
     // XXXdholbert Is this function even called with a non-nsStandardURL arg,
     // in practice?
     NS_WARNING("Calling NS_GetSecureUpgradedURI for non nsStandardURL");
     int32_t oldPort = -1;
     nsresult rv = aURI->GetPort(&oldPort);
     if (NS_FAILED(rv)) return rv;
diff --git a/netwerk/dns/TRRService.cpp.1435671.later b/netwerk/dns/TRRService.cpp.1435671.later
new file mode 100644
--- /dev/null
+++ b/netwerk/dns/TRRService.cpp.1435671.later
@@ -0,0 +1,26 @@
+--- TRRService.cpp
++++ TRRService.cpp
+@@ -308,20 +308,19 @@ TRRService::MaybeBootstrap(const nsACStr
+ {
+   MutexAutoLock lock(mLock);
+   if ((mMode == MODE_NATIVEONLY) || mBootstrapAddr.IsEmpty()) {
+     return false;
+   }
+ 
+   nsCOMPtr<nsIURI> url;
+   nsresult rv = NS_MutateURI(NS_STANDARDURLMUTATOR_CONTRACTID)
+-    .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
+-                                  nsIStandardURL::URLTYPE_STANDARD, 443,
+-                                  mPrivateURI, nullptr, nullptr,
+-                                  nullptr)
++    .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
++                            nsIStandardURL::URLTYPE_STANDARD, 443,
++                            mPrivateURI, nullptr, nullptr, nullptr))
+     .Finalize(url);
+   if (NS_FAILED(rv)) {
+     LOG(("TRRService::MaybeBootstrap failed to create URI!\n"));
+     return false;
+   }
+ 
+   nsAutoCString host;
+   url->GetHost(host);
diff --git a/netwerk/protocol/file/nsFileProtocolHandler.cpp b/netwerk/protocol/file/nsFileProtocolHandler.cpp
--- a/netwerk/protocol/file/nsFileProtocolHandler.cpp
+++ b/netwerk/protocol/file/nsFileProtocolHandler.cpp
@@ -159,35 +159,35 @@ nsFileProtocolHandler::GetProtocolFlags(
 {
     *result = URI_NOAUTH | URI_IS_LOCAL_FILE | URI_IS_LOCAL_RESOURCE;
     return NS_OK;
 }
 
 NS_IMETHODIMP
 nsFileProtocolHandler::NewURI(const nsACString &spec,
                               const char *charset,
-                              nsIURI *baseURI,
+                              nsIURI *aBaseURI,
                               nsIURI **result)
 {
     nsCOMPtr<nsIURI> url = new nsStandardURL(true);
 
     nsAutoCString buf(spec);
 #if defined(XP_WIN)
     buf.Truncate();
     if (!net_NormalizeFileURL(spec, buf)) {
         buf = spec;
     }
 #endif
 
+    nsCOMPtr<nsIURI> base(aBaseURI);
     return NS_MutateURI(url)
-             .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                           nsIStandardURL::URLTYPE_NO_AUTHORITY, -1,
-                                           buf, charset, baseURI,
-                                           nullptr)
-             .Finalize(result);
+      .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                              nsIStandardURL::URLTYPE_NO_AUTHORITY,
+                              -1, buf, charset, base, nullptr))
+      .Finalize(result);
 }
 
 NS_IMETHODIMP
 nsFileProtocolHandler::NewChannel2(nsIURI* uri,
                                    nsILoadInfo* aLoadInfo,
                                    nsIChannel** result)
 {
     nsresult rv;
diff --git a/netwerk/protocol/ftp/nsFtpProtocolHandler.cpp b/netwerk/protocol/ftp/nsFtpProtocolHandler.cpp
--- a/netwerk/protocol/ftp/nsFtpProtocolHandler.cpp
+++ b/netwerk/protocol/ftp/nsFtpProtocolHandler.cpp
@@ -166,23 +166,22 @@ nsFtpProtocolHandler::NewURI(const nsACS
     // NS_UnescapeURL() modified spec's buffer, truncate to ensure
     // spec knows its new length.
     spec.Truncate(len);
 
     // return an error if we find a NUL, CR, or LF in the path
     if (spec.FindCharInSet(CRLF) >= 0 || spec.FindChar('\0') >= 0)
         return NS_ERROR_MALFORMED_URI;
 
-    nsCOMPtr<nsIURI> url;
+    nsCOMPtr<nsIURI> base(aBaseURI);
     return NS_MutateURI(NS_STANDARDURLMUTATOR_CONTRACTID)
-             .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                           nsIStandardURL::URLTYPE_AUTHORITY, 21,
-                                           nsCString(aSpec), aCharset, aBaseURI,
-                                           nullptr)
-             .Finalize(result);
+      .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                              nsIStandardURL::URLTYPE_AUTHORITY,
+                              21, nsCString(aSpec), aCharset, base, nullptr))
+      .Finalize(result);
 }
 
 NS_IMETHODIMP
 nsFtpProtocolHandler::NewChannel2(nsIURI* url,
                                   nsILoadInfo* aLoadInfo,
                                   nsIChannel** result)
 {
     return NewProxiedChannel2(url, nullptr, 0, nullptr, aLoadInfo, result);
diff --git a/netwerk/protocol/gio/nsGIOProtocolHandler.cpp b/netwerk/protocol/gio/nsGIOProtocolHandler.cpp
--- a/netwerk/protocol/gio/nsGIOProtocolHandler.cpp
+++ b/netwerk/protocol/gio/nsGIOProtocolHandler.cpp
@@ -1022,22 +1022,22 @@ nsGIOProtocolHandler::NewURI(const nsACS
       uri_schemes++;
     }
 
     if (!uri_scheme_supported) {
       return NS_ERROR_UNKNOWN_PROTOCOL;
     }
   }
 
+  nsCOMPtr<nsIURI> base(aBaseURI);
   return NS_MutateURI(NS_STANDARDURLMUTATOR_CONTRACTID)
-           .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                         nsIStandardURL::URLTYPE_STANDARD, -1,
-                                         flatSpec, aOriginCharset, aBaseURI,
-                                         nullptr)
-           .Finalize(aResult);
+    .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                            nsIStandardURL::URLTYPE_STANDARD,
+                            -1, flatSpec, aOriginCharset, base, nullptr))
+    .Finalize(aResult);
 }
 
 NS_IMETHODIMP
 nsGIOProtocolHandler::NewChannel2(nsIURI* aURI,
                                   nsILoadInfo* aLoadInfo,
                                   nsIChannel** aResult)
 {
   NS_ENSURE_ARG_POINTER(aURI);
diff --git a/netwerk/protocol/http/Http2Stream.cpp b/netwerk/protocol/http/Http2Stream.cpp
--- a/netwerk/protocol/http/Http2Stream.cpp
+++ b/netwerk/protocol/http/Http2Stream.cpp
@@ -350,24 +350,22 @@ Http2Stream::MakeOriginURL(const nsACStr
   return MakeOriginURL(scheme, origin, url);
 }
 
 nsresult
 Http2Stream::MakeOriginURL(const nsACString &scheme, const nsACString &origin,
                            nsCOMPtr<nsIURI> &url)
 {
   return NS_MutateURI(new nsStandardURL::Mutator())
-           .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                         nsIStandardURL::URLTYPE_AUTHORITY,
-                                         scheme.EqualsLiteral("http") ?
-                                             NS_HTTP_DEFAULT_PORT :
-                                             NS_HTTPS_DEFAULT_PORT,
-                                         nsCString(origin), nullptr, nullptr,
-                                         nullptr)
-           .Finalize(url);
+    .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                            nsIStandardURL::URLTYPE_AUTHORITY,
+                            scheme.EqualsLiteral("http") ? NS_HTTP_DEFAULT_PORT
+                                                         : NS_HTTPS_DEFAULT_PORT,
+                            nsCString(origin), nullptr, nullptr, nullptr))
+    .Finalize(url);
 }
 
 void
 Http2Stream::CreatePushHashKey(const nsCString &scheme,
                                const nsCString &hostHeader,
                                const mozilla::OriginAttributes &originAttributes,
                                uint64_t serial,
                                const nsACString& pathInfo,
diff --git a/netwerk/protocol/http/nsHttpHandler.cpp b/netwerk/protocol/http/nsHttpHandler.cpp
--- a/netwerk/protocol/http/nsHttpHandler.cpp
+++ b/netwerk/protocol/http/nsHttpHandler.cpp
@@ -128,22 +128,23 @@ LazyLogModule gHttpLog("nsHttp");
 
 static nsresult
 NewURI(const nsACString &aSpec,
        const char *aCharset,
        nsIURI *aBaseURI,
        int32_t aDefaultPort,
        nsIURI **aURI)
 {
+    nsCOMPtr<nsIURI> base(aBaseURI);
     return NS_MutateURI(new nsStandardURL::Mutator())
-             .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                           nsIStandardURL::URLTYPE_AUTHORITY,
-                                           aDefaultPort, nsCString(aSpec), aCharset, aBaseURI,
-                                           nullptr)
-             .Finalize(aURI);
+        .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                                nsIStandardURL::URLTYPE_AUTHORITY,
+                                aDefaultPort, nsCString(aSpec), aCharset,
+                                base, nullptr))
+        .Finalize(aURI);
 }
 
 #ifdef ANDROID
 static nsCString
 GetDeviceModelId() {
     // Assumed to be running on the main thread
     // We need the device property in either case
     nsAutoCString deviceModelId;
diff --git a/netwerk/protocol/res/SubstitutingProtocolHandler.cpp b/netwerk/protocol/res/SubstitutingProtocolHandler.cpp
--- a/netwerk/protocol/res/SubstitutingProtocolHandler.cpp
+++ b/netwerk/protocol/res/SubstitutingProtocolHandler.cpp
@@ -224,22 +224,22 @@ SubstitutingProtocolHandler::NewURI(cons
         src += 2;
         last = src+1; // src will be incremented by the loop
       }
     }
   }
   if (last < src)
     spec.Append(last, src-last);
 
+  nsCOMPtr<nsIURI> base(aBaseURI);
   return NS_MutateURI(new SubstitutingURL::Mutator())
-           .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                         nsIStandardURL::URLTYPE_STANDARD, -1,
-                                         spec, aCharset, aBaseURI,
-                                         nullptr)
-           .Finalize(result);
+    .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                            nsIStandardURL::URLTYPE_STANDARD,
+                            -1, spec, aCharset, base, nullptr))
+    .Finalize(result);
 }
 
 nsresult
 SubstitutingProtocolHandler::NewChannel2(nsIURI* uri,
                                          nsILoadInfo* aLoadInfo,
                                          nsIChannel** result)
 {
   NS_ENSURE_ARG_POINTER(uri);
diff --git a/netwerk/protocol/websocket/BaseWebSocketChannel.cpp b/netwerk/protocol/websocket/BaseWebSocketChannel.cpp
--- a/netwerk/protocol/websocket/BaseWebSocketChannel.cpp
+++ b/netwerk/protocol/websocket/BaseWebSocketChannel.cpp
@@ -301,22 +301,23 @@ BaseWebSocketChannel::NewURI(const nsACS
 {
   LOG(("BaseWebSocketChannel::NewURI() %p\n", this));
 
   int32_t port;
   nsresult rv = GetDefaultPort(&port);
   if (NS_FAILED(rv))
     return rv;
 
+  nsCOMPtr<nsIURI> base(aBaseURI);
   return NS_MutateURI(new nsStandardURL::Mutator())
-           .Apply<nsIStandardURLMutator>(&nsIStandardURLMutator::Init,
-                                         nsIStandardURL::URLTYPE_AUTHORITY, port,
-                                         nsCString(aSpec), aOriginCharset, aBaseURI,
-                                         nullptr)
-           .Finalize(_retval);
+    .Apply(NS_MutatorMethod(&nsIStandardURLMutator::Init,
+                            nsIStandardURL::URLTYPE_AUTHORITY,
+                            port, nsCString(aSpec), aOriginCharset,
+                            base, nullptr))
+    .Finalize(_retval);
 }
 
 NS_IMETHODIMP
 BaseWebSocketChannel::NewChannel2(nsIURI* aURI,
                                   nsILoadInfo* aLoadInfo,
                                   nsIChannel** outChannel)
 {
   LOG(("BaseWebSocketChannel::NewChannel2() %p\n", this));
