# HG changeset patch
# User Valentin Gosu <valentin.gosu@gmail.com>
# Date 1520300691 -3600
# Node ID 964191c031db7c9b50b6a4f5dad9da1eb63acb49
# Parent  10bc967b5e6604585624333e781953cb8c64285b
Bug 1426353 - Do not allow the parent process to consume alt-data generated in the content process. r=michal

MozReview-Commit-ID: htQ28muaBI

diff --git a/netwerk/protocol/http/HttpBaseChannel.cpp b/netwerk/protocol/http/HttpBaseChannel.cpp
--- a/netwerk/protocol/http/HttpBaseChannel.cpp
+++ b/netwerk/protocol/http/HttpBaseChannel.cpp
@@ -212,16 +212,17 @@ HttpBaseChannel::HttpBaseChannel()
   , mDecodedBodySize(0)
   , mEncodedBodySize(0)
   , mRequestContextID(0)
   , mContentWindowId(0)
   , mTopLevelOuterContentWindowId(0)
   , mRequireCORSPreflight(false)
   , mReportCollector(new ConsoleReportCollector())
   , mAltDataLength(0)
+  , mAltDataForChild(false)
   , mForceMainDocumentChannel(false)
   , mIsTrackingResource(false)
   , mLastRedirectFlags(0)
   , mReqContentLength(0U)
   , mReqContentLengthDetermined(false)
 {
   LOG(("Creating HttpBaseChannel @%p\n", this));
 
@@ -3684,16 +3685,18 @@ HttpBaseChannel::SetupReplacementChannel
 
     // Preserve Redirect mode flag.
     rv = httpInternal->SetRedirectMode(mRedirectMode);
     MOZ_ASSERT(NS_SUCCEEDED(rv));
 
     // Preserve Integrity metadata.
     rv = httpInternal->SetIntegrityMetadata(mIntegrityMetadata);
     MOZ_ASSERT(NS_SUCCEEDED(rv));
+
+    httpInternal->SetAltDataForChild(mAltDataForChild);
   }
 
   // transfer application cache information
   nsCOMPtr<nsIApplicationCacheChannel> appCacheChannel =
     do_QueryInterface(newChannel);
   if (appCacheChannel) {
     appCacheChannel->SetApplicationCache(mApplicationCache);
     appCacheChannel->SetInheritApplicationCache(mInheritApplicationCache);
@@ -4433,16 +4436,22 @@ void
 HttpBaseChannel::SetCorsPreflightParameters(const nsTArray<nsCString>& aUnsafeHeaders)
 {
   MOZ_RELEASE_ASSERT(!mRequestObserversCalled);
 
   mRequireCORSPreflight = true;
   mUnsafeHeaders = aUnsafeHeaders;
 }
 
+void
+HttpBaseChannel::SetAltDataForChild(bool aIsForChild)
+{
+  mAltDataForChild = aIsForChild;
+}
+
 NS_IMETHODIMP
 HttpBaseChannel::GetBlockAuthPrompt(bool* aValue)
 {
   if (!aValue) {
     return NS_ERROR_FAILURE;
   }
 
   *aValue = mBlockAuthPrompt;
diff --git a/netwerk/protocol/http/HttpBaseChannel.h b/netwerk/protocol/http/HttpBaseChannel.h
--- a/netwerk/protocol/http/HttpBaseChannel.h
+++ b/netwerk/protocol/http/HttpBaseChannel.h
@@ -258,16 +258,17 @@ public:
   NS_IMETHOD GetRedirectMode(uint32_t* aRedirectMode) override;
   NS_IMETHOD SetRedirectMode(uint32_t aRedirectMode) override;
   NS_IMETHOD GetFetchCacheMode(uint32_t* aFetchCacheMode) override;
   NS_IMETHOD SetFetchCacheMode(uint32_t aFetchCacheMode) override;
   NS_IMETHOD GetTopWindowURI(nsIURI **aTopWindowURI) override;
   NS_IMETHOD SetTopWindowURIIfUnknown(nsIURI *aTopWindowURI) override;
   NS_IMETHOD GetProxyURI(nsIURI **proxyURI) override;
   virtual void SetCorsPreflightParameters(const nsTArray<nsCString>& unsafeHeaders) override;
+  virtual void SetAltDataForChild(bool aIsForChild) override;
   NS_IMETHOD GetConnectionInfoHashKey(nsACString& aConnectionInfoHashKey) override;
   NS_IMETHOD GetIntegrityMetadata(nsAString& aIntegrityMetadata) override;
   NS_IMETHOD SetIntegrityMetadata(const nsAString& aIntegrityMetadata) override;
   NS_IMETHOD GetLastRedirectFlags(uint32_t *aValue) override;
   NS_IMETHOD SetLastRedirectFlags(uint32_t aValue) override;
 
   inline void CleanRedirectCacheChainIfNecessary()
   {
@@ -699,16 +700,19 @@ protected:
 
   nsCOMPtr<nsIConsoleReportCollector> mReportCollector;
 
   // Holds the name of the preferred alt-data type.
   nsCString mPreferredCachedAltDataType;
   // Holds the name of the alternative data type the channel returned.
   nsCString mAvailableCachedAltDataType;
   int64_t   mAltDataLength;
+  // This flag will be true if the consumer is requesting alt-data AND the
+  // consumer is in the child process.
+  bool mAltDataForChild;
 
   bool mForceMainDocumentChannel;
   Atomic<bool, ReleaseAcquire> mIsTrackingResource;
 
   uint64_t mChannelId;
 
   // If this channel was created as the result of a redirect, then this value
   // will reflect the redirect flags passed to the SetupReplacementChannel()
diff --git a/netwerk/protocol/http/HttpChannelParent.cpp b/netwerk/protocol/http/HttpChannelParent.cpp
--- a/netwerk/protocol/http/HttpChannelParent.cpp
+++ b/netwerk/protocol/http/HttpChannelParent.cpp
@@ -694,18 +694,24 @@ HttpChannelParent::DoAsyncOpen(  const U
 
   nsCOMPtr<nsICacheInfoChannel> cacheChannel =
     do_QueryInterface(static_cast<nsIChannel*>(httpChannel.get()));
   if (cacheChannel) {
     cacheChannel->SetCacheKey(cacheKey);
     cacheChannel->PreferAlternativeDataType(aPreferredAlternativeType);
 
     cacheChannel->SetAllowStaleCacheContent(aAllowStaleCacheContent);
+
+    // This is to mark that the results are going to the content process.
+    if (httpChannelImpl) {
+      httpChannelImpl->SetAltDataForChild(true);
+    }
   }
 
+
   httpChannel->SetContentType(aContentTypeHint);
 
   if (priority != nsISupportsPriority::PRIORITY_NORMAL) {
     httpChannel->SetPriority(priority);
   }
   if (classOfService) {
     httpChannel->SetClassFlags(classOfService);
   }
@@ -2271,17 +2277,21 @@ HttpChannelParent::NotifyDiversionFailed
 nsresult
 HttpChannelParent::OpenAlternativeOutputStream(const nsACString & type, nsIOutputStream * *_retval)
 {
   // We need to make sure the child does not call SendDocumentChannelCleanup()
   // before opening the altOutputStream, because that clears mCacheEntry.
   if (!mCacheEntry) {
     return NS_ERROR_NOT_AVAILABLE;
   }
-  return mCacheEntry->OpenAlternativeOutputStream(type, _retval);
+  nsresult rv = mCacheEntry->OpenAlternativeOutputStream(type, _retval);
+  if (NS_SUCCEEDED(rv)) {
+    mCacheEntry->SetMetaDataElement("alt-data-from-child", "1");
+  }
+  return rv;
 }
 
 NS_IMETHODIMP
 HttpChannelParent::GetAuthPrompt(uint32_t aPromptReason, const nsIID& iid,
                                  void** aResult)
 {
   nsCOMPtr<nsIAuthPrompt2> prompt =
     new NeckoParent::NestedFrameAuthPrompt(Manager(), mNestedFrameId);
diff --git a/netwerk/protocol/http/nsHttpChannel.cpp b/netwerk/protocol/http/nsHttpChannel.cpp
--- a/netwerk/protocol/http/nsHttpChannel.cpp
+++ b/netwerk/protocol/http/nsHttpChannel.cpp
@@ -4783,17 +4783,26 @@ nsHttpChannel::OpenCacheInputStream(nsIC
     }
 
     // Open an input stream for the entity, so that the call to OpenInputStream
     // happens off the main thread.
     nsCOMPtr<nsIInputStream> stream;
 
     // If an alternate representation was requested, try to open the alt
     // input stream.
-    if (!mPreferredCachedAltDataType.IsEmpty()) {
+    // If the entry has a "is-from-child" metadata, then only open the altdata stream if the consumer is also from child.
+    bool altDataFromChild = false;
+    {
+        nsCString value;
+        rv = cacheEntry->GetMetaDataElement("alt-data-from-child",
+                                            getter_Copies(value));
+        altDataFromChild = !value.IsEmpty();
+    }
+
+    if (!mPreferredCachedAltDataType.IsEmpty() && (altDataFromChild == mAltDataForChild)) {
         rv = cacheEntry->OpenAlternativeInputStream(mPreferredCachedAltDataType,
                                                     getter_AddRefs(stream));
         if (NS_SUCCEEDED(rv)) {
             // We have succeeded.
             mAvailableCachedAltDataType = mPreferredCachedAltDataType;
             // Set the correct data size on the channel.
             int64_t altDataSize;
             if (NS_SUCCEEDED(cacheEntry->GetAltDataSize(&altDataSize))) {
@@ -7869,17 +7878,23 @@ NS_IMETHODIMP
 nsHttpChannel::OpenAlternativeOutputStream(const nsACString & type, nsIOutputStream * *_retval)
 {
     // OnStopRequest will clear mCacheEntry, but we may use mAltDataCacheEntry
     // if the consumer called PreferAlternativeDataType()
     nsCOMPtr<nsICacheEntry> cacheEntry = mCacheEntry ? mCacheEntry : mAltDataCacheEntry;
     if (!cacheEntry) {
         return NS_ERROR_NOT_AVAILABLE;
     }
-    return cacheEntry->OpenAlternativeOutputStream(type, _retval);
+    nsresult rv = cacheEntry->OpenAlternativeOutputStream(type, _retval);
+    if (NS_SUCCEEDED(rv)) {
+        // Clear this metadata flag in case it exists.
+        // The caller of this method may set it again.
+        cacheEntry->SetMetaDataElement("alt-data-from-child", nullptr);
+    }
+    return rv;
 }
 
 //-----------------------------------------------------------------------------
 // nsHttpChannel::nsICachingChannel
 //-----------------------------------------------------------------------------
 
 NS_IMETHODIMP
 nsHttpChannel::GetCacheToken(nsISupports **token)
diff --git a/netwerk/protocol/http/nsIHttpChannelInternal.idl b/netwerk/protocol/http/nsIHttpChannelInternal.idl
--- a/netwerk/protocol/http/nsIHttpChannelInternal.idl
+++ b/netwerk/protocol/http/nsIHttpChannelInternal.idl
@@ -297,16 +297,19 @@ interface nsIHttpChannelInternal : nsISu
 
     /**
      * Make cross-origin CORS loads happen with a CORS preflight, and specify
      * the CORS preflight parameters.
      */
     [noscript, notxpcom, nostdcall]
     void setCorsPreflightParameters(in StringArrayRef unsafeHeaders);
 
+    [noscript, notxpcom, nostdcall]
+    void setAltDataForChild(in boolean aIsForChild);
+
     /**
      * When set to true, the channel will not pop any authentication prompts up
      * to the user.  When provided or cached credentials lead to an
      * authentication failure, that failure will be propagated to the channel
      * listener.  Must be called before opening the channel, otherwise throws.
      */
     [infallible]
     attribute boolean blockAuthPrompt;
diff --git a/netwerk/protocol/viewsource/nsViewSourceChannel.cpp b/netwerk/protocol/viewsource/nsViewSourceChannel.cpp
--- a/netwerk/protocol/viewsource/nsViewSourceChannel.cpp
+++ b/netwerk/protocol/viewsource/nsViewSourceChannel.cpp
@@ -1114,16 +1114,22 @@ nsViewSourceChannel::SetIsMainDocumentCh
 
 // Have to manually forward SetCorsPreflightParameters since it's [notxpcom]
 void
 nsViewSourceChannel::SetCorsPreflightParameters(const nsTArray<nsCString>& aUnsafeHeaders)
 {
   mHttpChannelInternal->SetCorsPreflightParameters(aUnsafeHeaders);
 }
 
+void
+nsViewSourceChannel::SetAltDataForChild(bool aIsForChild)
+{
+    mHttpChannelInternal->SetAltDataForChild(aIsForChild);
+}
+
 NS_IMETHODIMP
 nsViewSourceChannel::LogBlockedCORSRequest(const nsAString& aMessage)
 {
   if (!mHttpChannel) {
     NS_WARNING("nsViewSourceChannel::LogBlockedCORSRequest mHttpChannel is null");
     return NS_ERROR_UNEXPECTED;
   }
   return mHttpChannel->LogBlockedCORSRequest(aMessage);
diff --git a/netwerk/test/unit/test_alt-data_simple.js b/netwerk/test/unit/test_alt-data_cross_process.js
copy from netwerk/test/unit/test_alt-data_simple.js
copy to netwerk/test/unit/test_alt-data_cross_process.js
--- a/netwerk/test/unit/test_alt-data_simple.js
+++ b/netwerk/test/unit/test_alt-data_cross_process.js
@@ -67,28 +67,22 @@ function check_has_alt_data_in_index(aHa
   }
   var hasAltData = {};
   cache_storage.getCacheIndexEntryAttrs(createURI(URL), "", hasAltData, {});
   Assert.equal(hasAltData.value, aHasAltData);
 }
 
 function run_test()
 {
-  do_get_profile();
   httpServer = new HttpServer();
   httpServer.registerPathHandler("/content", contentHandler);
   httpServer.start(-1);
   do_test_pending();
 
-  if (!inChildProcess()) {
-    cache_storage = getCacheStorage("disk") ;
-    wait_for_cache_index(asyncOpen);
-  } else {
-    asyncOpen();
-  }
+  asyncOpen();
 }
 
 function asyncOpen()
 {
   var chan = make_channel(URL);
 
   var cc = chan.QueryInterface(Ci.nsICacheInfoChannel);
   cc.preferAlternativeDataType(altContentType);
@@ -108,34 +102,24 @@ function readServerContent(request, buff
     var os = cc.openAlternativeOutputStream(altContentType);
     os.write(altContent, altContent.length);
     os.close();
 
     executeSoon(flushAndOpenAltChannel);
   });
 }
 
-// needs to be rooted
-var cacheFlushObserver = cacheFlushObserver = { observe: function() {
-  cacheFlushObserver = null;
-  openAltChannel();
-}};
-
 function flushAndOpenAltChannel()
 {
   // We need to do a GC pass to ensure the cache entry has been freed.
   gc();
-  if (!inChildProcess()) {
-    Services.cache2.QueryInterface(Ci.nsICacheTesting).flush(cacheFlushObserver);
-  } else {
-    do_send_remote_message('flush');
-    do_await_remote_message('flushed').then(() => {
-      openAltChannel();
-    });
-  }
+  do_send_remote_message('flush');
+  do_await_remote_message('flushed').then(() => {
+    openAltChannel();
+  });
 }
 
 function openAltChannel() {
   var chan = make_channel(URL);
   var cc = chan.QueryInterface(Ci.nsICacheInfoChannel);
   cc.preferAlternativeDataType(altContentType);
 
   chan.asyncOpen2(new ChannelListener(readAltContent, null));
@@ -143,33 +127,15 @@ function openAltChannel() {
 
 function readAltContent(request, buffer)
 {
   var cc = request.QueryInterface(Ci.nsICacheInfoChannel);
 
   Assert.equal(servedNotModified, true);
   Assert.equal(cc.alternativeDataType, altContentType);
   Assert.equal(buffer, altContent);
-  check_has_alt_data_in_index(true);
 
-  requestAgain();
-}
-
-function requestAgain()
-{
-  shouldPassRevalidation = false;
-  var chan = make_channel(URL);
-  var cc = chan.QueryInterface(Ci.nsICacheInfoChannel);
-  cc.preferAlternativeDataType(altContentType);
-  chan.asyncOpen2(new ChannelListener(readEmptyAltContent, null));
+  // FINISH
+  do_send_remote_message('done');
+  do_await_remote_message('finish').then(() => {
+    httpServer.stop(do_test_finished);
+  });
 }
-
-function readEmptyAltContent(request, buffer)
-{
-  var cc = request.QueryInterface(Ci.nsICacheInfoChannel);
-
-  // the cache is overwrite and the alt-data is reset
-  Assert.equal(cc.alternativeDataType, "");
-  Assert.equal(buffer, responseContent2);
-  check_has_alt_data_in_index(false);
-
-  httpServer.stop(do_test_finished);
-}
diff --git a/netwerk/test/unit/xpcshell.ini b/netwerk/test/unit/xpcshell.ini
--- a/netwerk/test/unit/xpcshell.ini
+++ b/netwerk/test/unit/xpcshell.ini
@@ -15,16 +15,17 @@ support-files =
   data/test_readline6.txt
   data/test_readline7.txt
   data/test_readline8.txt
   data/signed_win.exe
   socks_client_subprocess.js
   test_link.desktop
   test_link.url
   ../../dns/effective_tld_names.dat
+  test_alt-data_cross_process.js
 
 [test_network_activity.js]
 [test_nsIBufferedOutputStream_writeFrom_block.js]
 [test_cache2-00-service-get.js]
 [test_cache2-01-basic.js]
 [test_cache2-01a-basic-readonly.js]
 [test_cache2-01b-basic-datasize.js]
 [test_cache2-01c-basic-hasmeta-only.js]
diff --git a/netwerk/test/unit_ipc/test_alt-data_simple_wrap.js b/netwerk/test/unit_ipc/test_alt-data_cross_process_wrap.js
copy from netwerk/test/unit_ipc/test_alt-data_simple_wrap.js
copy to netwerk/test/unit_ipc/test_alt-data_cross_process_wrap.js
--- a/netwerk/test/unit_ipc/test_alt-data_simple_wrap.js
+++ b/netwerk/test/unit_ipc/test_alt-data_cross_process_wrap.js
@@ -1,15 +1,81 @@
 ChromeUtils.import("resource://gre/modules/Services.jsm");
+ChromeUtils.import("resource://gre/modules/NetUtil.jsm");
 
 // needs to be rooted
 var cacheFlushObserver = { observe: function() {
   cacheFlushObserver = null;
   do_send_remote_message('flushed');
 }};
 
+// We get this from the child a bit later
+var URL = null;
+
+// needs to be rooted
+var cacheFlushObserver2 = { observe: function() {
+  cacheFlushObserver2 = null;
+  openAltChannel();
+}};
+
 function run_test() {
   do_get_profile();
   do_await_remote_message('flush').then(() => {
     Services.cache2.QueryInterface(Ci.nsICacheTesting).flush(cacheFlushObserver);
   });
-  run_test_in_child("../unit/test_alt-data_simple.js");
+
+  do_await_remote_message('done').then(() => { sendCommand("URL;", load_channel); });
+
+  run_test_in_child("../unit/test_alt-data_cross_process.js");
+}
+
+function load_channel(url) {
+  ok(url);
+  URL = url; // save this to open the alt data channel later
+  var chan = make_channel(url);
+  var cc = chan.QueryInterface(Ci.nsICacheInfoChannel);
+  cc.preferAlternativeDataType("text/binary");
+  chan.asyncOpen2(new ChannelListener(readTextData, null));
+}
+
+function make_channel(url, callback, ctx) {
+  return NetUtil.newChannel({uri: url, loadUsingSystemPrincipal: true});
 }
+
+function readTextData(request, buffer)
+{
+  var cc = request.QueryInterface(Ci.nsICacheInfoChannel);
+  // Since we are in a different process from what that generated the alt-data,
+  // we should receive the original data, not processed content.
+  Assert.equal(cc.alternativeDataType, "");
+  Assert.equal(buffer, "response body");
+
+  // Now let's generate some alt-data in the parent, and make sure we can get it
+  var altContent = "altContentParentGenerated";
+  executeSoon(() => {
+    var os = cc.openAlternativeOutputStream("text/parent-binary");
+    os.write(altContent, altContent.length);
+    os.close();
+
+    executeSoon(() => {
+      Services.cache2.QueryInterface(Ci.nsICacheTesting).flush(cacheFlushObserver2);
+    });
+  });
+}
+
+function openAltChannel() {
+  var chan = make_channel(URL);
+  var cc = chan.QueryInterface(Ci.nsICacheInfoChannel);
+  cc.preferAlternativeDataType("text/parent-binary");
+  chan.asyncOpen2(new ChannelListener(readAltData, null));
+}
+
+function readAltData(request, buffer)
+{
+  var cc = request.QueryInterface(Ci.nsICacheInfoChannel);
+
+  // This was generated in the parent, so it's OK to get it.
+  Assert.equal(buffer, "altContentParentGenerated");
+  Assert.equal(cc.alternativeDataType, "text/parent-binary");
+
+  // FINISH
+  do_send_remote_message('finish');
+}
diff --git a/netwerk/test/unit_ipc/xpcshell.ini b/netwerk/test/unit_ipc/xpcshell.ini
--- a/netwerk/test/unit_ipc/xpcshell.ini
+++ b/netwerk/test/unit_ipc/xpcshell.ini
@@ -54,16 +54,17 @@ support-files =
   !/netwerk/test/unit/data/test_readline7.txt
   !/netwerk/test/unit/data/test_readline8.txt
   !/netwerk/test/unit/data/signed_win.exe
   !/netwerk/test/unit/test_alt-data_simple.js
   !/netwerk/test/unit/test_alt-data_stream.js
   !/netwerk/test/unit/test_channel_priority.js
   !/netwerk/test/unit/test_multipart_streamconv.js
   !/netwerk/test/unit/test_original_sent_received_head.js
+  !/netwerk/test/unit/test_alt-data_cross_process.js
 
 [test_bug528292_wrap.js]
 [test_bug248970_cookie_wrap.js]
 [test_cacheflags_wrap.js]
 [test_cache-entry-id_wrap.js]
 [test_cache_jar_wrap.js]
 [test_channel_close_wrap.js]
 [test_cookie_header_wrap.js]
@@ -101,8 +102,9 @@ skip-if = true
 [test_alt-data_simple_wrap.js]
 [test_alt-data_stream_wrap.js]
 [test_original_sent_received_head_wrap.js]
 [test_channel_id.js]
 [test_trackingProtection_annotateChannels_wrap1.js]
 [test_trackingProtection_annotateChannels_wrap2.js]
 [test_channel_priority_wrap.js]
 [test_multipart_streamconv_wrap.js]
+[test_alt-data_cross_process_wrap.js]

