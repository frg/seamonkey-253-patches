# HG changeset patch
# User Andreas Pehrson <pehrsons@mozilla.com>
# Date 1517824701 -3600
#      Mon Feb 05 10:58:21 2018 +0100
# Node ID f1b6dc89e5d95a286b81a9d5b87d29e09c515cba
# Parent  a77670d180770a2dc5af301ac83da83c4165b395
Bug 1435673 - Strengthen some MediaEngineWebRTCMicrophoneSource asserts. r=padenot

MozReview-Commit-ID: HtjndNLWZI8

diff --git a/dom/media/webrtc/MediaEngineWebRTCAudio.cpp b/dom/media/webrtc/MediaEngineWebRTCAudio.cpp
--- a/dom/media/webrtc/MediaEngineWebRTCAudio.cpp
+++ b/dom/media/webrtc/MediaEngineWebRTCAudio.cpp
@@ -592,19 +592,20 @@ MediaEngineWebRTCMicrophoneSource::Alloc
 }
 
 nsresult
 MediaEngineWebRTCMicrophoneSource::Deallocate(const RefPtr<const AllocationHandle>& aHandle)
 {
   AssertIsOnOwningThread();
 
   size_t i = mAllocations.IndexOf(aHandle, 0, AllocationHandleComparator());
-  MOZ_ASSERT(i != mAllocations.NoIndex);
-  MOZ_ASSERT(!mAllocations[i].mEnabled,
-             "Source should be stopped for the track before removing");
+  MOZ_DIAGNOSTIC_ASSERT(i != mAllocations.NoIndex);
+  MOZ_DIAGNOSTIC_ASSERT(!mAllocations[i].mEnabled,
+                        "Source should be stopped for the track before removing");
+
   if (mAllocations[i].mStream && IsTrackIDExplicit(mAllocations[i].mTrackID)) {
     mAllocations[i].mStream->EndTrack(mAllocations[i].mTrackID);
   }
 
   {
     MutexAutoLock lock(mMutex);
     mAllocations.RemoveElementAt(i);
   }
@@ -640,17 +641,20 @@ MediaEngineWebRTCMicrophoneSource::SetTr
   // will share a Graph(), and we can allow it.
   if (!mAllocations.IsEmpty() &&
       mAllocations[0].mStream &&
       mAllocations[0].mStream->Graph() != aStream->Graph()) {
     return NS_ERROR_NOT_AVAILABLE;
   }
 
   size_t i = mAllocations.IndexOf(aHandle, 0, AllocationHandleComparator());
-  MOZ_ASSERT(i != mAllocations.NoIndex);
+  MOZ_DIAGNOSTIC_ASSERT(i != mAllocations.NoIndex);
+  MOZ_ASSERT(!mAllocations[i].mStream);
+  MOZ_ASSERT(mAllocations[i].mTrackID == TRACK_NONE);
+  MOZ_ASSERT(mAllocations[i].mPrincipal == PRINCIPAL_HANDLE_NONE);
   {
     MutexAutoLock lock(mMutex);
     mAllocations[i].mStream = aStream;
     mAllocations[i].mTrackID = aTrackID;
     mAllocations[i].mPrincipal = aPrincipal;
   }
 
   AudioSegment* segment = new AudioSegment();
@@ -673,17 +677,18 @@ MediaEngineWebRTCMicrophoneSource::Start
 {
   AssertIsOnOwningThread();
 
   if (sChannelsOpen == 0) {
     return NS_ERROR_FAILURE;
   }
 
   size_t i = mAllocations.IndexOf(aHandle, 0, AllocationHandleComparator());
-  MOZ_ASSERT(i != mAllocations.NoIndex, "Can't start track that hasn't been added");
+  MOZ_DIAGNOSTIC_ASSERT(i != mAllocations.NoIndex,
+                        "Can't start track that hasn't been added");
   Allocation& allocation = mAllocations[i];
 
   MOZ_ASSERT(!allocation.mEnabled, "Source already started");
   {
     // This spans setting both the enabled state and mState.
     MutexAutoLock lock(mMutex);
     allocation.mEnabled = true;
 
@@ -705,17 +710,18 @@ MediaEngineWebRTCMicrophoneSource::Start
 }
 
 nsresult
 MediaEngineWebRTCMicrophoneSource::Stop(const RefPtr<const AllocationHandle>& aHandle)
 {
   AssertIsOnOwningThread();
 
   size_t i = mAllocations.IndexOf(aHandle, 0, AllocationHandleComparator());
-  MOZ_ASSERT(i != mAllocations.NoIndex, "Cannot stop track that we don't know about");
+  MOZ_DIAGNOSTIC_ASSERT(i != mAllocations.NoIndex,
+                        "Cannot stop track that we don't know about");
   Allocation& allocation = mAllocations[i];
 
   if (!allocation.mEnabled) {
     // Already stopped - this is allowed
     return NS_OK;
   }
 
   {
