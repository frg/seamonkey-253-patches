# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1519798481 -3600
# Node ID 33cf111a726576238a35fa42cfb688915e210024
# Parent  119c8ae6aee9963d5b5a34de1add6e9fe84b7283
Bug 1431646 - Adding a completed boolean to know if the operation is completed in NS_ReadInputStreamToBuffer(), r=smaug

diff --git a/netwerk/base/nsNetUtil.cpp b/netwerk/base/nsNetUtil.cpp
--- a/netwerk/base/nsNetUtil.cpp
+++ b/netwerk/base/nsNetUtil.cpp
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 // HttpLog.h should generally be included first
 #include "HttpLog.h"
 
 #include "nsNetUtil.h"
 
+#include "mozilla/Atomics.h"
 #include "mozilla/Encoding.h"
 #include "mozilla/LoadContext.h"
 #include "mozilla/LoadInfo.h"
 #include "mozilla/BasePrincipal.h"
 #include "mozilla/Monitor.h"
 #include "mozilla/TaskQueue.h"
 #include "nsCategoryCache.h"
 #include "nsContentUtils.h"
@@ -1430,16 +1431,17 @@ public:
         , mMonitor("BufferWriter.mMonitor")
         , mInputStream(aInputStream)
         , mBuffer(aBuffer)
         , mCount(aCount)
         , mWrittenData(0)
         , mBufferType(aBuffer ? eExternal : eInternal)
         , mAsyncResult(NS_OK)
         , mBufferSize(0)
+        , mCompleted(false)
     {
         MOZ_ASSERT(aInputStream);
         MOZ_ASSERT(aCount == -1 || aCount > 0);
         MOZ_ASSERT_IF(mBuffer, aCount > 0);
     }
 
     nsresult
     Write()
@@ -1554,30 +1556,35 @@ private:
 
         nsCOMPtr<nsIRunnable> runnable = this;
         nsresult rv = mTaskQueue->Dispatch(runnable.forget());
         NS_ENSURE_SUCCESS(rv, rv);
 
         rv = lock.Wait();
         NS_ENSURE_SUCCESS(rv, rv);
 
+        mCompleted = true;
         return mAsyncResult;
     }
 
     // This method runs on the I/O Thread when the owning thread is blocked by
     // the mMonitor. It is called multiple times until mCount is greater than 0
     // or until there is something to read in the stream.
     NS_IMETHOD
     Run() override
     {
         MOZ_ASSERT(mAsyncInputStream);
         MOZ_ASSERT(!mInputStream);
 
         MonitorAutoLock lock(mMonitor);
 
+        if (mCompleted) {
+            return NS_OK;
+        }
+
         if (mCount == 0) {
             OperationCompleted(lock, NS_OK);
             return NS_OK;
         }
 
         if (mCount == -1 && !MaybeExpandBufferSize()) {
             OperationCompleted(lock, NS_ERROR_OUT_OF_MEMORY);
             return NS_OK;
@@ -1606,17 +1613,17 @@ private:
                 mCount -= writtenData;
             }
 
             nsCOMPtr<nsIRunnable> runnable = this;
             rv = mTaskQueue->Dispatch(runnable.forget());
             if (NS_WARN_IF(NS_FAILED(rv))) {
                 OperationCompleted(lock, rv);
             }
-        
+
             return NS_OK;
         }
 
         // Async wait...
         if (rv == NS_BASE_STREAM_WOULD_BLOCK) {
             rv = mAsyncInputStream->AsyncWait(this, 0, length, mTaskQueue);
             if (NS_WARN_IF(NS_FAILED(rv))) {
                 OperationCompleted(lock, rv);
@@ -1698,16 +1705,17 @@ private:
 
         // The buffer is not owned by this object and it cannot be reallocated.
         eExternal,
     } mBufferType;
 
     // The following set if needed for the async read.
     nsresult mAsyncResult;
     uint64_t mBufferSize;
+    Atomic<bool> mCompleted;
 };
 
 NS_IMPL_ISUPPORTS_INHERITED(BufferWriter, Runnable, nsIInputStreamCallback)
 
 } // anonymous namespace
 
 nsresult
 NS_ReadInputStreamToBuffer(nsIInputStream* aInputStream,
