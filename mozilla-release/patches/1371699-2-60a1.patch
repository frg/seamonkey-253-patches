# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1517413520 -3600
#      Wed Jan 31 16:45:20 2018 +0100
# Node ID a1a8765481eb2699021093181aeb174d75fa3505
# Parent  7a8cb42003027cc384ea21d699b9659c31e62784
Bug 1371699 - Use of NonBlockingAsyncInputStream in our code base, r=froydnj

diff --git a/dom/fetch/FetchStream.cpp b/dom/fetch/FetchStream.cpp
--- a/dom/fetch/FetchStream.cpp
+++ b/dom/fetch/FetchStream.cpp
@@ -3,19 +3,18 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "FetchStream.h"
 #include "mozilla/dom/DOMException.h"
 #include "mozilla/dom/WorkerCommon.h"
 #include "mozilla/dom/WorkerPrivate.h"
-#include "nsITransport.h"
-#include "nsIStreamTransportService.h"
 #include "nsProxyRelease.h"
+#include "nsStreamUtils.h"
 
 #define FETCH_STREAM_FLAG 0
 
 static NS_DEFINE_CID(kStreamTransportServiceCID,
                      NS_STREAMTRANSPORTSERVICE_CID);
 
 namespace mozilla {
 namespace dom {
@@ -199,55 +198,25 @@ FetchStream::RequestDataCallback(JSConte
 
   stream->mState = eReading;
 
   if (!stream->mInputStream) {
     // This is the first use of the stream. Let's convert the
     // mOriginalInputStream into an nsIAsyncInputStream.
     MOZ_ASSERT(stream->mOriginalInputStream);
 
-    bool nonBlocking = false;
-    nsresult rv = stream->mOriginalInputStream->IsNonBlocking(&nonBlocking);
+    nsCOMPtr<nsIAsyncInputStream> asyncStream;
+    nsresult rv =
+      NS_MakeAsyncNonBlockingInputStream(stream->mOriginalInputStream,
+                                         getter_AddRefs(asyncStream));
     if (NS_WARN_IF(NS_FAILED(rv))) {
       stream->ErrorPropagation(aCx, aStream, rv);
       return;
     }
 
-    nsCOMPtr<nsIAsyncInputStream> asyncStream =
-      do_QueryInterface(stream->mOriginalInputStream);
-    if (!nonBlocking || !asyncStream) {
-      nsCOMPtr<nsIStreamTransportService> sts =
-        do_GetService(kStreamTransportServiceCID, &rv);
-      if (NS_WARN_IF(NS_FAILED(rv))) {
-        stream->ErrorPropagation(aCx, aStream, rv);
-        return;
-      }
-
-      nsCOMPtr<nsITransport> transport;
-      rv = sts->CreateInputTransport(stream->mOriginalInputStream,
-                                     /* aCloseWhenDone */ true,
-                                     getter_AddRefs(transport));
-      if (NS_WARN_IF(NS_FAILED(rv))) {
-        stream->ErrorPropagation(aCx, aStream, rv);
-        return;
-      }
-
-      nsCOMPtr<nsIInputStream> wrapper;
-      rv = transport->OpenInputStream(/* aFlags */ 0,
-                                       /* aSegmentSize */ 0,
-                                       /* aSegmentCount */ 0,
-                                       getter_AddRefs(wrapper));
-      if (NS_WARN_IF(NS_FAILED(rv))) {
-        stream->ErrorPropagation(aCx, aStream, rv);
-        return;
-      }
-
-      asyncStream = do_QueryInterface(wrapper);
-    }
-
     stream->mInputStream = asyncStream;
     stream->mOriginalInputStream = nullptr;
   }
 
   MOZ_DIAGNOSTIC_ASSERT(stream->mInputStream);
   MOZ_DIAGNOSTIC_ASSERT(!stream->mOriginalInputStream);
 
   nsresult rv =
diff --git a/dom/file/FileReader.cpp b/dom/file/FileReader.cpp
--- a/dom/file/FileReader.cpp
+++ b/dom/file/FileReader.cpp
@@ -4,50 +4,45 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "FileReader.h"
 
 #include "nsIEventTarget.h"
 #include "nsIGlobalObject.h"
 #include "nsITimer.h"
-#include "nsITransport.h"
-#include "nsIStreamTransportService.h"
 
 #include "mozilla/Base64.h"
 #include "mozilla/CheckedInt.h"
 #include "mozilla/dom/DOMException.h"
 #include "mozilla/dom/File.h"
 #include "mozilla/dom/FileReaderBinding.h"
 #include "mozilla/dom/ProgressEvent.h"
 #include "mozilla/dom/WorkerPrivate.h"
 #include "mozilla/dom/WorkerScope.h"
 #include "mozilla/Encoding.h"
 #include "nsAlgorithm.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsDOMJSUtils.h"
 #include "nsError.h"
-#include "nsNetCID.h"
 #include "nsNetUtil.h"
 #include "xpcpublic.h"
 
 namespace mozilla {
 namespace dom {
 
 #define ABORT_STR "abort"
 #define LOAD_STR "load"
 #define LOADSTART_STR "loadstart"
 #define LOADEND_STR "loadend"
 #define ERROR_STR "error"
 #define PROGRESS_STR "progress"
 
 const uint64_t kUnknownSize = uint64_t(-1);
 
-static NS_DEFINE_CID(kStreamTransportServiceCID, NS_STREAMTRANSPORTSERVICE_CID);
-
 NS_IMPL_CYCLE_COLLECTION_CLASS(FileReader)
 
 NS_IMPL_CYCLE_COLLECTION_TRAVERSE_BEGIN_INHERITED(FileReader,
                                                   DOMEventTargetHelper)
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mBlob)
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mProgressNotifier)
   NS_IMPL_CYCLE_COLLECTION_TRAVERSE(mError)
 NS_IMPL_CYCLE_COLLECTION_TRAVERSE_END
@@ -411,54 +406,22 @@ FileReader::ReadFileContent(Blob& aBlob,
   CopyUTF16toUTF8(aCharset, mCharset);
 
   nsCOMPtr<nsIInputStream> stream;
   mBlob->CreateInputStream(getter_AddRefs(stream), aRv);
   if (NS_WARN_IF(aRv.Failed())) {
     return;
   }
 
-  bool nonBlocking = false;
-  aRv = stream->IsNonBlocking(&nonBlocking);
+  aRv = NS_MakeAsyncNonBlockingInputStream(stream,
+                                           getter_AddRefs(mAsyncStream));
   if (NS_WARN_IF(aRv.Failed())) {
     return;
   }
 
-  mAsyncStream = do_QueryInterface(stream);
-
-  // We want to have a non-blocking nsIAsyncInputStream.
-  if (!mAsyncStream || !nonBlocking) {
-    nsresult rv;
-    nsCOMPtr<nsIStreamTransportService> sts =
-      do_GetService(kStreamTransportServiceCID, &rv);
-    if (NS_WARN_IF(NS_FAILED(rv))) {
-      aRv.Throw(rv);
-      return;
-    }
-
-    nsCOMPtr<nsITransport> transport;
-    aRv = sts->CreateInputTransport(stream,
-                                    /* aCloseWhenDone */ true,
-                                    getter_AddRefs(transport));
-    if (NS_WARN_IF(aRv.Failed())) {
-      return;
-    }
-
-    nsCOMPtr<nsIInputStream> wrapper;
-    aRv = transport->OpenInputStream(/* aFlags */ 0,
-                                     /* aSegmentSize */ 0,
-                                     /* aSegmentCount */ 0,
-                                     getter_AddRefs(wrapper));
-    if (NS_WARN_IF(aRv.Failed())) {
-      return;
-    }
-
-    mAsyncStream = do_QueryInterface(wrapper);
-  }
-
   MOZ_ASSERT(mAsyncStream);
 
   mTotal = mBlob->GetSize(aRv);
   if (NS_WARN_IF(aRv.Failed())) {
     return;
   }
 
   // Binary Format doesn't need a post-processing of the data. Everything is
diff --git a/dom/file/ipc/IPCBlobInputStream.cpp b/dom/file/ipc/IPCBlobInputStream.cpp
--- a/dom/file/ipc/IPCBlobInputStream.cpp
+++ b/dom/file/ipc/IPCBlobInputStream.cpp
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "IPCBlobInputStream.h"
 #include "IPCBlobInputStreamChild.h"
 #include "IPCBlobInputStreamStorage.h"
 #include "mozilla/ipc/InputStreamParams.h"
 #include "mozilla/SlicedInputStream.h"
+#include "mozilla/NonBlockingAsyncInputStream.h"
 #include "IPCBlobInputStreamThread.h"
 #include "nsIAsyncInputStream.h"
 #include "nsIAsyncOutputStream.h"
 #include "nsIPipe.h"
 #include "nsStreamUtils.h"
 #include "nsStringStream.h"
 
 namespace mozilla {
@@ -640,17 +641,27 @@ IPCBlobInputStream::EnsureAsyncRemoteStr
   // If the stream is blocking, we want to make it unblocking using a pipe.
   bool nonBlocking = false;
   nsresult rv = mRemoteStream->IsNonBlocking(&nonBlocking);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   nsCOMPtr<nsIAsyncInputStream> asyncStream = do_QueryInterface(mRemoteStream);
-  if (!asyncStream || !nonBlocking) {
+
+  // If non-blocking and non-async, let's use NonBlockingAsyncInputStream.
+  if (nonBlocking && !asyncStream) {
+    rv = NonBlockingAsyncInputStream::Create(mRemoteStream,
+                                             getter_AddRefs(asyncStream));
+    if (NS_WARN_IF(NS_FAILED(rv))) {
+      return rv;
+    }
+  }
+
+  if (!asyncStream) {
     // Let's make the stream async using the DOMFile thread.
     nsCOMPtr<nsIAsyncInputStream> pipeIn;
     nsCOMPtr<nsIAsyncOutputStream> pipeOut;
     rv = NS_NewPipe2(getter_AddRefs(pipeIn),
                      getter_AddRefs(pipeOut),
                      true, true);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
diff --git a/netwerk/base/nsIStreamTransportService.idl b/netwerk/base/nsIStreamTransportService.idl
--- a/netwerk/base/nsIStreamTransportService.idl
+++ b/netwerk/base/nsIStreamTransportService.idl
@@ -7,16 +7,19 @@
 interface nsITransport;
 interface nsIInputStream;
 interface nsIOutputStream;
 interface nsIInputAvailableCallback;
 
 /**
  * This service read/writes a stream on a background thread.
  *
+ * Note: instead of using this interface, probably you want to use
+ * NS_MakeAsyncNonBlockingInputStream.
+ *
  * Use this service to transform any blocking stream (e.g., file stream)
  * into a fully asynchronous stream that can be read/written without 
  * blocking the main thread.
  */
 [builtinclass, scriptable, uuid(5e0adf7d-9785-45c3-a193-04f25a75da8f)]
 interface nsIStreamTransportService : nsISupports
 {
     /**
diff --git a/netwerk/base/nsInputStreamPump.cpp b/netwerk/base/nsInputStreamPump.cpp
--- a/netwerk/base/nsInputStreamPump.cpp
+++ b/netwerk/base/nsInputStreamPump.cpp
@@ -8,16 +8,18 @@
 #include "nsInputStreamPump.h"
 #include "nsIStreamTransportService.h"
 #include "nsISeekableStream.h"
 #include "nsITransport.h"
 #include "nsIThreadRetargetableStreamListener.h"
 #include "nsThreadUtils.h"
 #include "nsCOMPtr.h"
 #include "mozilla/Logging.h"
+#include "mozilla/NonBlockingAsyncInputStream.h"
+#include "mozilla/SlicedInputStream.h"
 #include "GeckoProfiler.h"
 #include "nsIStreamListener.h"
 #include "nsILoadGroup.h"
 #include "nsNetCID.h"
 #include "nsStreamUtils.h"
 #include <algorithm>
 
 static NS_DEFINE_CID(kStreamTransportServiceCID, NS_STREAMTRANSPORTSERVICE_CID);
@@ -327,16 +329,21 @@ nsInputStreamPump::AsyncRead(nsIStreamLi
     //
 
     bool nonBlocking;
     nsresult rv = mStream->IsNonBlocking(&nonBlocking);
     if (NS_FAILED(rv)) return rv;
 
     if (nonBlocking) {
         mAsyncStream = do_QueryInterface(mStream);
+        if (!mAsyncStream) {
+            rv = NonBlockingAsyncInputStream::Create(mStream,
+                                                     getter_AddRefs(mAsyncStream));
+            if (NS_WARN_IF(NS_FAILED(rv))) return rv;
+        }
     }
 
     if (!mAsyncStream) {
         // ok, let's use the stream transport service to read this stream.
         nsCOMPtr<nsIStreamTransportService> sts =
             do_GetService(kStreamTransportServiceCID, &rv);
         if (NS_FAILED(rv)) return rv;
 
diff --git a/xpcom/io/nsStreamUtils.cpp b/xpcom/io/nsStreamUtils.cpp
--- a/xpcom/io/nsStreamUtils.cpp
+++ b/xpcom/io/nsStreamUtils.cpp
@@ -16,19 +16,24 @@
 #include "nsISafeOutputStream.h"
 #include "nsString.h"
 #include "nsIAsyncInputStream.h"
 #include "nsIAsyncOutputStream.h"
 #include "nsIBufferedStreams.h"
 #include "nsNetCID.h"
 #include "nsServiceManagerUtils.h"
 #include "nsThreadUtils.h"
+#include "nsITransport.h"
+#include "nsIStreamTransportService.h"
+#include "NonBlockingAsyncInputStream.h"
 
 using namespace mozilla;
 
+static NS_DEFINE_CID(kStreamTransportServiceCID, NS_STREAMTRANSPORTSERVICE_CID);
+
 //-----------------------------------------------------------------------------
 
 // This is a nsICancelableRunnable because we can dispatch it to Workers and
 // those can be shut down at any time, and in these cases, Cancel() is called
 // instead of Run().
 class nsInputStreamReadyEvent final
   : public CancelableRunnable
   , public nsIInputStreamCallback
@@ -963,8 +968,65 @@ NS_CloneInputStream(nsIInputStream* aSou
   rv = NS_AsyncCopy(aSource, writer, target, NS_ASYNCCOPY_VIA_WRITESEGMENTS);
   if (NS_WARN_IF(NS_FAILED(rv))) { return rv; }
 
   readerClone.forget(aCloneOut);
   reader.forget(aReplacementOut);
 
   return NS_OK;
 }
+
+nsresult
+NS_MakeAsyncNonBlockingInputStream(nsIInputStream* aSource,
+                                   nsIAsyncInputStream** aAsyncInputStream)
+{
+  if (NS_WARN_IF(!aSource || !aAsyncInputStream)) {
+    return NS_ERROR_FAILURE;
+  }
+
+  bool nonBlocking = false;
+  nsresult rv = aSource->IsNonBlocking(&nonBlocking);
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
+  }
+
+  nsCOMPtr<nsIAsyncInputStream> asyncStream = do_QueryInterface(aSource);
+
+  if (nonBlocking && asyncStream) {
+    // This stream is perfect!
+    asyncStream.forget(aAsyncInputStream);
+    return NS_OK;
+  }
+
+  if (nonBlocking) {
+    // If the stream is non-blocking but not async, we wrap it.
+    return NonBlockingAsyncInputStream::Create(aSource, aAsyncInputStream);
+  }
+
+  nsCOMPtr<nsIStreamTransportService> sts =
+    do_GetService(kStreamTransportServiceCID, &rv);
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
+  }
+
+  nsCOMPtr<nsITransport> transport;
+  rv = sts->CreateInputTransport(aSource,
+                                 /* aCloseWhenDone */ true,
+                                 getter_AddRefs(transport));
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
+  }
+
+  nsCOMPtr<nsIInputStream> wrapper;
+  rv = transport->OpenInputStream(/* aFlags */ 0,
+                                  /* aSegmentSize */ 0,
+                                  /* aSegmentCount */ 0,
+                                  getter_AddRefs(wrapper));
+  if (NS_WARN_IF(NS_FAILED(rv))) {
+    return rv;
+  }
+
+  asyncStream = do_QueryInterface(wrapper);
+  MOZ_ASSERT(asyncStream);
+
+  asyncStream.forget(aAsyncInputStream);
+  return NS_OK;
+}
diff --git a/xpcom/io/nsStreamUtils.h b/xpcom/io/nsStreamUtils.h
--- a/xpcom/io/nsStreamUtils.h
+++ b/xpcom/io/nsStreamUtils.h
@@ -7,16 +7,17 @@
 #ifndef nsStreamUtils_h__
 #define nsStreamUtils_h__
 
 #include "nsCOMPtr.h"
 #include "nsStringFwd.h"
 #include "nsIInputStream.h"
 #include "nsTArray.h"
 
+class nsIAsyncInputStream;
 class nsIOutputStream;
 class nsIInputStreamCallback;
 class nsIOutputStreamCallback;
 class nsIEventTarget;
 
 /**
  * A "one-shot" proxy of the OnInputStreamReady callback.  The resulting
  * proxy object's OnInputStreamReady function may only be called once!  The
@@ -288,9 +289,28 @@ NS_InputStreamIsCloneable(nsIInputStream
  *                        supported and a non-cloneable source will result
  *                        in failure.  Replacement streams are non-blocking.
  * @return NS_OK on successful clone.  Error otherwise.
  */
 extern nsresult
 NS_CloneInputStream(nsIInputStream* aSource, nsIInputStream** aCloneOut,
                     nsIInputStream** aReplacementOut = nullptr);
 
+/*
+ * This function returns a non-blocking nsIAsyncInputStream. Internally,
+ * different approaches are used based on what |aSource| is and what it
+ * implements.
+ *
+ * If the |aSource| is already a non-blocking and async stream,
+ * |aAsyncInputStream| will be equal to |aSource|.
+ *
+ * Otherwise, if |aSource| is just non-blocking, NonBlockingAsyncInputStream
+ * class is used in order to make it async.
+ *
+ * The last step is to use nsIStreamTransportService and create a pipe in order
+ * to expose a non-blocking async inputStream and read |aSource| data from
+ * a separate thread.
+ */
+extern nsresult
+NS_MakeAsyncNonBlockingInputStream(nsIInputStream* aSource,
+                                   nsIAsyncInputStream** aAsyncInputStream);
+
 #endif // !nsStreamUtils_h__
