# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1582893109 0
# Node ID 863de9919ba03163f29312b6a4867dd4c6ba9872
# Parent  ac1a194c3d6da60e24f792c205e26766b567e20b
Bug 1618775 - Uniformize preprocessor-inserted paths between platforms. r=froydnj

The preprocessor adds line markers in preprocessed files with line
numbers and file they came from. Bug 1528892 changed those markers
to be independent of the topobjdir and topsrcdir, by replacing them
with $OBJDIR and $SRCDIR, respectively.

This goes further, making these paths always use forward-slash, and
never backwards-slash, making the preprocessed files identical whether
the build occurred on Windows or Unix. (well, except when building
for different targets for target-specific sections)

Differential Revision: https://phabricator.services.mozilla.com/D64714

diff --git a/python/mozbuild/mozbuild/preprocessor.py b/python/mozbuild/mozbuild/preprocessor.py
--- a/python/mozbuild/mozbuild/preprocessor.py
+++ b/python/mozbuild/mozbuild/preprocessor.py
@@ -28,16 +28,17 @@ import errno
 import io
 from optparse import OptionParser
 import os
 import re
 import six
 import sys
 
 from mozbuild.makeutil import Makefile
+from mozpack.path import normsep
 
 # hack around win32 mangling our line endings
 # http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/65443
 if sys.platform == "win32":
     import msvcrt
     msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)
     os.linesep = '\n'
 
@@ -823,19 +824,19 @@ class Preprocessor:
             self.context['FILE'] = '-'
             self.context['DIRECTORY'] = ''
             self.curdir = '.'
         else:
             abspath = os.path.abspath(args.name)
             self.curdir = os.path.dirname(abspath)
             self.includes.add(six.ensure_text(abspath))
             if self.topobjdir and path_starts_with(abspath, self.topobjdir):
-                abspath = '$OBJDIR' + abspath[len(self.topobjdir):]
+                abspath = '$OBJDIR' + normsep(abspath[len(self.topobjdir):])
             elif self.topsrcdir and path_starts_with(abspath, self.topsrcdir):
-                abspath = '$SRCDIR' + abspath[len(self.topsrcdir):]
+                abspath = '$SRCDIR' + normsep(abspath[len(self.topsrcdir):])
             self.context['FILE'] = abspath
             self.context['DIRECTORY'] = os.path.dirname(abspath)
         self.context['LINE'] = 0
 
         for l in args:
             self.context['LINE'] += 1
             self.handleLine(l)
         if isName:
diff --git a/python/mozbuild/mozbuild/test/backend/test_build.py b/python/mozbuild/mozbuild/test/backend/test_build.py
--- a/python/mozbuild/mozbuild/test/backend/test_build.py
+++ b/python/mozbuild/mozbuild/test/backend/test_build.py
@@ -143,18 +143,18 @@ class TestBuild(unittest.TestCase):
                                                        'faster'),
                                 target=overrides, silent=False,
                                 line_handler=handle_make_line)
 
             self.validate(config)
 
     def validate(self, config):
         self.maxDiff = None
-        test_path = os.sep.join(('$SRCDIR', 'python', 'mozbuild', 'mozbuild',
-                                 'test', 'backend', 'data', 'build')) + os.sep
+        test_path = mozpath.join('$SRCDIR', 'python', 'mozbuild', 'mozbuild',
+                                 'test', 'backend', 'data', 'build')
 
         result = {
             p: f.open(mode='r').read()
             for p, f in FileFinder(mozpath.join(config.topobjdir, 'dist'))
         }
         self.assertTrue(len(result))
         self.assertEqual(result, {
             'bin/baz.ini': 'baz.ini: FOO is foo\n',
@@ -166,34 +166,34 @@ class TestBuild(unittest.TestCase):
                 'manifest components/components.manifest\n',
             'bin/chrome/foo.manifest':
                 'content bar foo/child/\n'
                 'content foo foo/\n'
                 'override chrome://foo/bar.svg#hello '
                 'chrome://bar/bar.svg#hello\n',
             'bin/chrome/foo/bar.js': 'bar.js\n',
             'bin/chrome/foo/child/baz.jsm':
-                '//@line 2 "%sbaz.jsm"\nbaz.jsm: FOO is foo\n' % (test_path),
+                '//@line 2 "%s/baz.jsm"\nbaz.jsm: FOO is foo\n' % (test_path),
             'bin/chrome/foo/child/hoge.js':
-                '//@line 2 "%sbar.js"\nbar.js: FOO is foo\n' % (test_path),
+                '//@line 2 "%s/bar.js"\nbar.js: FOO is foo\n' % (test_path),
             'bin/chrome/foo/foo.css': 'foo.css: FOO is foo\n',
             'bin/chrome/foo/foo.js': 'foo.js\n',
             'bin/chrome/foo/qux.js': 'bar.js\n',
             'bin/components/bar.js':
-                '//@line 2 "%sbar.js"\nbar.js: FOO is foo\n' % (test_path),
+                '//@line 2 "%s/bar.js"\nbar.js: FOO is foo\n' % (test_path),
             'bin/components/components.manifest':
                 'component {foo} foo.js\ncomponent {bar} bar.js\n',
             'bin/components/foo.js': 'foo.js\n',
             'bin/defaults/pref/prefs.js': 'prefs.js\n',
             'bin/foo.ini': 'foo.ini\n',
             'bin/modules/baz.jsm':
-                '//@line 2 "%sbaz.jsm"\nbaz.jsm: FOO is foo\n' % (test_path),
+                '//@line 2 "%s/baz.jsm"\nbaz.jsm: FOO is foo\n' % (test_path),
             'bin/modules/child/bar.jsm': 'bar.jsm\n',
             'bin/modules/child2/qux.jsm':
-                '//@line 4 "%squx.jsm"\nqux.jsm: BAR is not defined\n'
+                '//@line 4 "%s/qux.jsm"\nqux.jsm: BAR is not defined\n'
                 % (test_path),
             'bin/modules/foo.jsm': 'foo.jsm\n',
             'bin/res/resource': 'resource\n',
             'bin/res/child/resource2': 'resource2\n',
 
             'bin/app/baz.ini': 'baz.ini: FOO is bar\n',
             'bin/app/child/bar.ini': 'bar.ini\n',
             'bin/app/child2/qux.ini': 'qux.ini: BAR is defined\n',
@@ -202,34 +202,34 @@ class TestBuild(unittest.TestCase):
                 'manifest components/components.manifest\n',
             'bin/app/chrome/foo.manifest':
                 'content bar foo/child/\n'
                 'content foo foo/\n'
                 'override chrome://foo/bar.svg#hello '
                 'chrome://bar/bar.svg#hello\n',
             'bin/app/chrome/foo/bar.js': 'bar.js\n',
             'bin/app/chrome/foo/child/baz.jsm':
-                '//@line 2 "%sbaz.jsm"\nbaz.jsm: FOO is bar\n' % (test_path),
+                '//@line 2 "%s/baz.jsm"\nbaz.jsm: FOO is bar\n' % (test_path),
             'bin/app/chrome/foo/child/hoge.js':
-                '//@line 2 "%sbar.js"\nbar.js: FOO is bar\n' % (test_path),
+                '//@line 2 "%s/bar.js"\nbar.js: FOO is bar\n' % (test_path),
             'bin/app/chrome/foo/foo.css': 'foo.css: FOO is bar\n',
             'bin/app/chrome/foo/foo.js': 'foo.js\n',
             'bin/app/chrome/foo/qux.js': 'bar.js\n',
             'bin/app/components/bar.js':
-                '//@line 2 "%sbar.js"\nbar.js: FOO is bar\n' % (test_path),
+                '//@line 2 "%s/bar.js"\nbar.js: FOO is bar\n' % (test_path),
             'bin/app/components/components.manifest':
                 'component {foo} foo.js\ncomponent {bar} bar.js\n',
             'bin/app/components/foo.js': 'foo.js\n',
             'bin/app/defaults/preferences/prefs.js': 'prefs.js\n',
             'bin/app/foo.css': 'foo.css: FOO is bar\n',
             'bin/app/foo.ini': 'foo.ini\n',
             'bin/app/modules/baz.jsm':
-                '//@line 2 "%sbaz.jsm"\nbaz.jsm: FOO is bar\n' % (test_path),
+                '//@line 2 "%s/baz.jsm"\nbaz.jsm: FOO is bar\n' % (test_path),
             'bin/app/modules/child/bar.jsm': 'bar.jsm\n',
             'bin/app/modules/child2/qux.jsm':
-                '//@line 2 "%squx.jsm"\nqux.jsm: BAR is defined\n'
+                '//@line 2 "%s/qux.jsm"\nqux.jsm: BAR is defined\n'
                 % (test_path),
             'bin/app/modules/foo.jsm': 'foo.jsm\n',
         })
 
 
 if __name__ == '__main__':
     main()
diff --git a/python/mozbuild/mozbuild/test/test_preprocessor.py b/python/mozbuild/mozbuild/test/test_preprocessor.py
--- a/python/mozbuild/mozbuild/test/test_preprocessor.py
+++ b/python/mozbuild/mozbuild/test/test_preprocessor.py
@@ -624,17 +624,17 @@ class TestPreprocessor(unittest.TestCase
                         'bazbarfoo\n'
                         '//@line 2 "$SRCDIR/bar.js"\n'
                         'foobarbaz\n'
                         '//@line 3 "$SRCDIR/test.js"\n'
                         'barfoobaz\n'
                         '//@line 1 "$OBJDIR/baz.js"\n'
                         'baz\n'
                         '//@line 6 "$SRCDIR/f.js"\n'
-                        'fin\n').replace('DIR/', 'DIR' + os.sep)
+                        'fin\n')
 
         # Try with separate srcdir/objdir
         with MockedOpen(files):
             self.pp.topsrcdir = os.path.abspath('srcdir')
             self.pp.topobjdir = os.path.abspath('objdir')
             self.pp.do_include('srcdir/f.js')
             self.assertEqual(self.pp.out.getvalue(), preprocessed)
 
diff --git a/python/mozbuild/mozpack/test/test_packager.py b/python/mozbuild/mozpack/test/test_packager.py
--- a/python/mozbuild/mozpack/test/test_packager.py
+++ b/python/mozbuild/mozpack/test/test_packager.py
@@ -41,17 +41,17 @@ foo/zot
 #ifdef baz
 [baz]
 baz@SUFFIX@
 #endif
 '''
 
 
 class TestPreprocessManifest(unittest.TestCase):
-    MANIFEST_PATH = os.path.join('$OBJDIR', 'manifest')
+    MANIFEST_PATH = mozpath.join('$OBJDIR', 'manifest')
 
     EXPECTED_LOG = [
         ((MANIFEST_PATH, 2), 'add', '', 'bar/*'),
         ((MANIFEST_PATH, 4), 'add', 'foo', 'foo/*'),
         ((MANIFEST_PATH, 5), 'remove', 'foo', 'foo/bar'),
         ((MANIFEST_PATH, 6), 'add', 'foo', 'chrome.manifest'),
         ((MANIFEST_PATH, 8), 'add', 'zot destdir="destdir"', 'foo/zot'),
     ]
