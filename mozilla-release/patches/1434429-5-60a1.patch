# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1516304067 28800
#      Thu Jan 18 11:34:27 2018 -0800
# Node ID 229b3ea6530f11018de78fd3bd7bf1bd4e1f29d6
# Parent  53a6e670175b94a32b2ec0e15be8be74c0b4a379
Bug 1434429 - Implement TokenStreamChars::matchMultiUnitCodePoint as a better nailing-down of behavior when processing a multi-code unit code point.  r=arai

diff --git a/js/src/frontend/TokenStream.cpp b/js/src/frontend/TokenStream.cpp
--- a/js/src/frontend/TokenStream.cpp
+++ b/js/src/frontend/TokenStream.cpp
@@ -1283,41 +1283,44 @@ IsTokenSane(Token* tp)
 
     if (tp->pos.end < tp->pos.begin)
         return false;
 
     return true;
 }
 #endif
 
-template<class AnyCharsAccess>
-bool
-TokenStreamChars<char16_t, AnyCharsAccess>::matchTrailForLeadSurrogate(char16_t lead,
-                                                                       uint32_t* codePoint)
-{
-    int32_t maybeTrail = getCharIgnoreEOL();
-    if (!unicode::IsTrailSurrogate(maybeTrail)) {
-        ungetCharIgnoreEOL(maybeTrail);
-        return false;
-    }
-
-    *codePoint = unicode::UTF16Decode(lead, maybeTrail);
-    return true;
-}
-
 template<>
 MOZ_MUST_USE bool
 TokenStreamCharsBase<char16_t>::appendMultiUnitCodepointToTokenbuf(uint32_t codepoint)
 {
     char16_t lead, trail;
     unicode::UTF16Encode(codepoint, &lead, &trail);
 
     return tokenbuf.append(lead) && tokenbuf.append(trail);
 }
 
+template<class AnyCharsAccess>
+void
+TokenStreamChars<char16_t, AnyCharsAccess>::matchMultiUnitCodePointSlow(char16_t lead,
+                                                                        uint32_t* codePoint)
+{
+    MOZ_ASSERT(unicode::IsLeadSurrogate(lead),
+               "matchMultiUnitCodepoint should have ensured |lead| is a lead "
+               "surrogate");
+
+    int32_t maybeTrail = getCharIgnoreEOL();
+    if (MOZ_LIKELY(unicode::IsTrailSurrogate(maybeTrail))) {
+        *codePoint = unicode::UTF16Decode(lead, maybeTrail);
+    } else {
+        ungetCharIgnoreEOL(maybeTrail);
+        *codePoint = 0;
+    }
+}
+
 template<typename CharT, class AnyCharsAccess>
 bool
 TokenStreamSpecific<CharT, AnyCharsAccess>::putIdentInTokenbuf(const CharT* identStart)
 {
     const CharT* const originalAddress = userbuf.addressOfNextRawChar();
     userbuf.setAddressOfNextRawChar(identStart);
 
     auto restoreNextRawCharAddress =
@@ -1325,17 +1328,19 @@ TokenStreamSpecific<CharT, AnyCharsAcces
             this->userbuf.setAddressOfNextRawChar(originalAddress);
         });
 
     tokenbuf.clear();
     for (;;) {
         int32_t c = getCharIgnoreEOL();
 
         uint32_t codePoint;
-        if (isMultiUnitCodepoint(c, &codePoint)) {
+        if (!matchMultiUnitCodePoint(c, &codePoint))
+            return false;
+        if (codePoint) {
             if (!unicode::IsIdentifierPart(codePoint))
                 break;
 
             if (!appendMultiUnitCodepointToTokenbuf(codePoint))
                 return false;
 
             continue;
         }
@@ -1510,17 +1515,19 @@ TokenStreamSpecific<CharT, AnyCharsAcces
                       "IdentifierStart contains '_', but as !IsUnicodeIDStart('_'), "
                       "ensure that '_' is never handled here");
         if (unicode::IsUnicodeIDStart(char16_t(c))) {
             hadUnicodeEscape = false;
             goto identifier;
         }
 
         uint32_t codePoint = c;
-        if (isMultiUnitCodepoint(c, &codePoint) && unicode::IsUnicodeIDStart(codePoint)) {
+        if (!matchMultiUnitCodePoint(c, &codePoint))
+            goto error;
+        if (codePoint && unicode::IsUnicodeIDStart(codePoint)) {
             hadUnicodeEscape = false;
             goto identifier;
         }
 
         ungetCodePointIgnoreEOL(codePoint);
         error(JSMSG_ILLEGAL_CHARACTER);
         goto error;
     }
@@ -1569,17 +1576,19 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 
       identifier:
         for (;;) {
             c = getCharIgnoreEOL();
             if (c == EOF)
                 break;
 
             uint32_t codePoint;
-            if (isMultiUnitCodepoint(c, &codePoint)) {
+            if (!matchMultiUnitCodePoint(c, &codePoint))
+                goto error;
+            if (codePoint) {
                 if (!unicode::IsIdentifierPart(codePoint))
                     break;
 
                 continue;
             }
 
             if (!unicode::IsIdentifierPart(char16_t(c))) {
                 if (c != '\\' || !matchUnicodeEscapeIdent(&qc))
@@ -1658,19 +1667,19 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 
         if (c != EOF) {
             if (unicode::IsIdentifierStart(char16_t(c))) {
                 reportError(JSMSG_IDSTART_AFTER_NUMBER);
                 goto error;
             }
 
             uint32_t codePoint;
-            if (isMultiUnitCodepoint(c, &codePoint) &&
-                unicode::IsIdentifierStart(codePoint))
-            {
+            if (!matchMultiUnitCodePoint(c, &codePoint))
+                goto error;
+            if (codePoint && unicode::IsIdentifierStart(codePoint)) {
                 reportError(JSMSG_IDSTART_AFTER_NUMBER);
                 goto error;
             }
         }
 
         // Unlike identifiers and strings, numbers cannot contain escaped
         // chars, so we don't need to use tokenbuf.  Instead we can just
         // convert the char16_t characters in userbuf to the numeric value.
@@ -1783,19 +1792,19 @@ TokenStreamSpecific<CharT, AnyCharsAcces
 
         if (c != EOF) {
             if (unicode::IsIdentifierStart(char16_t(c))) {
                 reportError(JSMSG_IDSTART_AFTER_NUMBER);
                 goto error;
             }
 
             uint32_t codePoint;
-            if (isMultiUnitCodepoint(c, &codePoint) &&
-                unicode::IsIdentifierStart(codePoint))
-            {
+            if (!matchMultiUnitCodePoint(c, &codePoint))
+                goto error;
+            if (codePoint && unicode::IsIdentifierStart(codePoint)) {
                 reportError(JSMSG_IDSTART_AFTER_NUMBER);
                 goto error;
             }
         }
 
         double dval;
         const char16_t* dummy;
         if (!GetPrefixInteger(anyCharsAccess().cx, numStart, userbuf.addressOfNextRawChar(),
diff --git a/js/src/frontend/TokenStream.h b/js/src/frontend/TokenStream.h
--- a/js/src/frontend/TokenStream.h
+++ b/js/src/frontend/TokenStream.h
@@ -1041,31 +1041,49 @@ class TokenStreamChars<char16_t, AnyChar
     using Self = TokenStreamChars<char16_t, AnyCharsAccess>;
     using GeneralCharsBase = GeneralTokenStreamChars<char16_t, AnyCharsAccess>;
     using CharsSharedBase = TokenStreamCharsBase<char16_t>;
 
     using GeneralCharsBase::asSpecific;
 
     using typename GeneralCharsBase::TokenStreamSpecific;
 
-    bool matchTrailForLeadSurrogate(char16_t lead, uint32_t* codePoint);
+    void matchMultiUnitCodePointSlow(char16_t lead, uint32_t* codePoint);
 
   protected:
     using GeneralCharsBase::anyCharsAccess;
     using GeneralCharsBase::getCharIgnoreEOL;
     using CharsSharedBase::ungetCharIgnoreEOL;
     using GeneralCharsBase::userbuf;
 
     using GeneralCharsBase::GeneralCharsBase;
 
-    MOZ_ALWAYS_INLINE bool isMultiUnitCodepoint(char16_t c, uint32_t* codepoint) {
+    // |c| must be the code unit just gotten.  If it and the subsequent code
+    // unit form a valid surrogate pair, get the second code unit, set
+    // |*codePoint| to the code point encoded by the surrogate pair, and return
+    // true.  Otherwise do not get a second code unit, set |*codePoint = 0|,
+    // and return true.
+    //
+    // ECMAScript specifically requires that unpaired UTF-16 surrogates be
+    // treated as the corresponding code point and not as an error.  See
+    // <https://tc39.github.io/ecma262/#sec-ecmascript-language-types-string-type>.
+    // Therefore this function always returns true.  The |bool| return type
+    // exists so that a future UTF-8 |TokenStreamChars| can treat malformed
+    // multi-code unit UTF-8 sequences as errors.  (Because ECMAScript only
+    // interprets UTF-16 inputs, the process of translating the UTF-8 to UTF-16
+    // would fail, so no script should execute.  Technically, we shouldn't even
+    // be tokenizing -- but it probably isn't realistic to assume every user
+    // correctly passes only valid UTF-8, at least not without better types in
+    // our codebase for strings that by construction only contain valid UTF-8.)
+    MOZ_ALWAYS_INLINE bool matchMultiUnitCodePoint(char16_t c, uint32_t* codePoint) {
         if (MOZ_LIKELY(!unicode::IsLeadSurrogate(c)))
-            return false;
-
-        return matchTrailForLeadSurrogate(c, codepoint);
+            *codePoint = 0;
+        else
+            matchMultiUnitCodePointSlow(c, codePoint);
+        return true;
     }
 
     void ungetCodePointIgnoreEOL(uint32_t codePoint);
 };
 
 // TokenStream is the lexical scanner for JavaScript source text.
 //
 // It takes a buffer of CharT characters (currently only char16_t encoding
@@ -1139,17 +1157,17 @@ class MOZ_STACK_CLASS TokenStreamSpecifi
     using typename CharsSharedBase::CharBuffer;
     using typename CharsSharedBase::TokenBuf;
 
   private:
     using CharsSharedBase::appendMultiUnitCodepointToTokenbuf;
     using CharsSharedBase::atomizeChars;
     using CharsSharedBase::copyTokenbufTo;
     using GeneralCharsBase::getCharIgnoreEOL;
-    using CharsBase::isMultiUnitCodepoint;
+    using CharsBase::matchMultiUnitCodePoint;
     using CharsSharedBase::tokenbuf;
     using GeneralCharsBase::ungetChar;
     using CharsSharedBase::ungetCharIgnoreEOL;
     using CharsBase::ungetCodePointIgnoreEOL;
     using CharsSharedBase::userbuf;
 
   public:
     TokenStreamSpecific(JSContext* cx, const ReadOnlyCompileOptions& options,
