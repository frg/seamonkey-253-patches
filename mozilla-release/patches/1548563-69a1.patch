# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1558531506 0
# Node ID 3e785d8704585cc8de56d94abbd47653ceb09ad4
# Parent  3c8b14061cda65c5f8c3304acec0776c80468cc9
Bug 1548563 - move symbol file logic into the moz.build frontend; r=firefox-build-system-reviewers,chmanchester

This way compilation backends don't all have to figure out the right way
to handle symbol files.

Differential Revision: https://phabricator.services.mozilla.com/D29673

diff --git a/config/rules.mk b/config/rules.mk
--- a/config/rules.mk
+++ b/config/rules.mk
@@ -289,34 +289,16 @@ endif
 
 ifeq ($(OS_ARCH),Darwin)
 ifdef SHARED_LIBRARY
 _LOADER_PATH := @executable_path
 EXTRA_DSO_LDOPTS	+= -dynamiclib -install_name $(_LOADER_PATH)/$(SHARED_LIBRARY) -compatibility_version 1 -current_version 1 -single_module
 endif
 endif
 
-ifdef SYMBOLS_FILE
-ifeq ($(OS_TARGET),WINNT)
-ifndef GNU_CC
-EXTRA_DSO_LDOPTS += -DEF:$(call normalizepath,$(SYMBOLS_FILE))
-else
-EXTRA_DSO_LDOPTS += $(call normalizepath,$(SYMBOLS_FILE))
-endif
-else
-ifdef GCC_USE_GNU_LD
-EXTRA_DSO_LDOPTS += -Wl,--version-script,$(SYMBOLS_FILE)
-else
-ifeq ($(OS_TARGET),Darwin)
-EXTRA_DSO_LDOPTS += -Wl,-exported_symbols_list,$(SYMBOLS_FILE)
-endif
-endif
-endif
-EXTRA_DEPS += $(SYMBOLS_FILE)
-endif
 #
 # GNU doesn't have path length limitation
 #
 
 ifeq ($(OS_ARCH),GNU)
 OS_CPPFLAGS += -DPATH_MAX=1024 -DMAXPATHLEN=1024
 endif
 
diff --git a/python/mozbuild/mozbuild/backend/recursivemake.py b/python/mozbuild/mozbuild/backend/recursivemake.py
--- a/python/mozbuild/mozbuild/backend/recursivemake.py
+++ b/python/mozbuild/mozbuild/backend/recursivemake.py
@@ -1332,17 +1332,19 @@ class RecursiveMakeBackend(CommonBackend
     def _process_shared_library(self, libdef, backend_file):
         backend_file.write_once('LIBRARY_NAME := %s\n' % libdef.basename)
         backend_file.write('FORCE_SHARED_LIB := 1\n')
         backend_file.write('IMPORT_LIBRARY := %s\n' % libdef.import_name)
         backend_file.write('SHARED_LIBRARY := %s\n' % libdef.lib_name)
         if libdef.soname:
             backend_file.write('DSO_SONAME := %s\n' % libdef.soname)
         if libdef.symbols_file:
-            backend_file.write('SYMBOLS_FILE := %s\n' % libdef.symbols_file)
+            if libdef.symbols_link_arg:
+                backend_file.write('EXTRA_DSO_LDOPTS += %s\n' % libdef.symbols_link_arg)
+                backend_file.write('EXTRA_DEPS += %s\n' % libdef.symbols_file)
         if not libdef.cxx_link:
             backend_file.write('LIB_IS_C_ONLY := 1\n')
         if libdef.output_category:
             self._process_non_default_target(libdef, libdef.lib_name,
                                              backend_file)
             # Override the install rule target for this library. This is hacky,
             # but can go away as soon as we start building libraries in their
             # final location (bug 1459764).
diff --git a/python/mozbuild/mozbuild/frontend/data.py b/python/mozbuild/mozbuild/frontend/data.py
--- a/python/mozbuild/mozbuild/frontend/data.py
+++ b/python/mozbuild/mozbuild/frontend/data.py
@@ -709,16 +709,17 @@ class RustLibrary(StaticLibrary):
 
 class SharedLibrary(Library):
     """Context derived container object for a shared library"""
     __slots__ = (
         'soname',
         'variant',
         'symbols_file',
         'output_category',
+        'symbols_link_arg',
     )
 
     DICT_ATTRS = {
         'basename',
         'import_name',
         'install_target',
         'lib_name',
         'relobjdir',
@@ -767,16 +768,28 @@ class SharedLibrary(Library):
             if context.config.substs['OS_TARGET'] == 'WINNT':
                 self.symbols_file = '%s.def' % self.lib_name
             else:
                 self.symbols_file = '%s.symbols' % self.lib_name
         else:
             # Explicitly provided name.
             self.symbols_file = symbols_file
 
+        if self.symbols_file:
+            os_target = context.config.substs['OS_TARGET']
+            if os_target == 'Darwin':
+                self.symbols_link_arg = '-Wl,-exported_symbols_list,' + self.symbols_file
+            elif os_target == 'WINNT':
+                if context.config.substs.get('GNU_CC'):
+                    self.symbols_link_arg = self.symbols_file
+                else:
+                    self.symbols_link_arg = '-DEF:' + self.symbols_file
+            elif context.config.substs.get('GCC_USE_GNU_LD'):
+                self.symbols_link_arg = '-Wl,--version-script,' + self.symbols_file
+
 
 class HostSharedLibrary(HostMixin, Library):
     """Context derived container object for a host shared library.
 
     This class supports less things than SharedLibrary does for target shared
     libraries. Currently has enough build system support to build the clang
     plugin."""
     KIND = 'host'
