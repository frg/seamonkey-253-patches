# HG changeset patch
# User Johann Hofmann <jhofmann@mozilla.com>
# Date 1519134857 -3600
# Node ID 963a227648afe5d8d4544b8d3878f7a3bf062acf
# Parent  cf46f500960643afb541426a1c51a54e8efedc5e
Bug 1431029 - Expose timestamp for quota managed storage. r=asuth

MozReview-Commit-ID: 7MNd2m2Jp46

diff --git a/dom/quota/ActorsChild.cpp b/dom/quota/ActorsChild.cpp
--- a/dom/quota/ActorsChild.cpp
+++ b/dom/quota/ActorsChild.cpp
@@ -158,17 +158,18 @@ QuotaUsageRequestChild::HandleResponse(c
 
     usageResults.SetCapacity(count);
 
     for (uint32_t index = 0; index < count; index++) {
       auto& originUsage = aResponse[index];
 
       RefPtr<UsageResult> usageResult = new UsageResult(originUsage.origin(),
                                                         originUsage.persisted(),
-                                                        originUsage.usage());
+                                                        originUsage.usage(),
+                                                        originUsage.lastAccessed());
 
       usageResults.AppendElement(usageResult.forget());
     }
 
     variant->SetAsArray(nsIDataType::VTYPE_INTERFACE_IS,
                         &NS_GET_IID(nsIQuotaUsageResult),
                         usageResults.Length(),
                         static_cast<void*>(usageResults.Elements()));
diff --git a/dom/quota/ActorsParent.cpp b/dom/quota/ActorsParent.cpp
--- a/dom/quota/ActorsParent.cpp
+++ b/dom/quota/ActorsParent.cpp
@@ -6973,16 +6973,18 @@ GetUsageOp::TraverseRepository(QuotaMana
 
       mOriginUsagesIndex.Put(origin, index);
     }
 
     if (aPersistenceType == PERSISTENCE_TYPE_DEFAULT) {
       originUsage->persisted() = persisted;
     }
 
+    originUsage->lastAccessed() = timestamp;
+
     UsageInfo usageInfo;
     rv = GetUsageForOrigin(aQuotaManager,
                            aPersistenceType,
                            group,
                            origin,
                            &usageInfo);
     if (NS_WARN_IF(NS_FAILED(rv))) {
       return rv;
diff --git a/dom/quota/PQuotaUsageRequest.ipdl b/dom/quota/PQuotaUsageRequest.ipdl
--- a/dom/quota/PQuotaUsageRequest.ipdl
+++ b/dom/quota/PQuotaUsageRequest.ipdl
@@ -8,16 +8,17 @@ namespace mozilla {
 namespace dom {
 namespace quota {
 
 struct OriginUsage
 {
   nsCString origin;
   bool persisted;
   uint64_t usage;
+  uint64_t lastAccessed;
 };
 
 struct AllUsageResponse
 {
   OriginUsage[] originUsages;
 };
 
 struct OriginUsageResponse
diff --git a/dom/quota/QuotaResults.cpp b/dom/quota/QuotaResults.cpp
--- a/dom/quota/QuotaResults.cpp
+++ b/dom/quota/QuotaResults.cpp
@@ -7,20 +7,22 @@
 #include "QuotaResults.h"
 
 namespace mozilla {
 namespace dom {
 namespace quota {
 
 UsageResult::UsageResult(const nsACString& aOrigin,
                          bool aPersisted,
-                         uint64_t aUsage)
+                         uint64_t aUsage,
+                         uint64_t aLastAccessed)
   : mOrigin(aOrigin)
   , mUsage(aUsage)
   , mPersisted(aPersisted)
+  , mLastAccessed(aLastAccessed)
 {
 }
 
 NS_IMPL_ISUPPORTS(UsageResult,
                   nsIQuotaUsageResult)
 
 NS_IMETHODIMP
 UsageResult::GetOrigin(nsACString& aOrigin)
@@ -42,16 +44,25 @@ NS_IMETHODIMP
 UsageResult::GetUsage(uint64_t* aUsage)
 {
   MOZ_ASSERT(aUsage);
 
   *aUsage = mUsage;
   return NS_OK;
 }
 
+NS_IMETHODIMP
+UsageResult::GetLastAccessed(uint64_t* aLastAccessed)
+{
+  MOZ_ASSERT(aLastAccessed);
+
+  *aLastAccessed = mLastAccessed;
+  return NS_OK;
+}
+
 OriginUsageResult::OriginUsageResult(uint64_t aUsage,
                                      uint64_t aFileUsage,
                                      uint64_t aLimit)
   : mUsage(aUsage)
   , mFileUsage(aFileUsage)
   , mLimit(aLimit)
 {
 }
diff --git a/dom/quota/QuotaResults.h b/dom/quota/QuotaResults.h
--- a/dom/quota/QuotaResults.h
+++ b/dom/quota/QuotaResults.h
@@ -14,21 +14,23 @@ namespace dom {
 namespace quota {
 
 class UsageResult
   : public nsIQuotaUsageResult
 {
   nsCString mOrigin;
   uint64_t mUsage;
   bool mPersisted;
+  uint64_t mLastAccessed;
 
 public:
   UsageResult(const nsACString& aOrigin,
               bool aPersisted,
-              uint64_t aUsage);
+              uint64_t aUsage,
+              uint64_t aLastAccessed);
 
 private:
   virtual ~UsageResult()
   { }
 
   NS_DECL_ISUPPORTS
   NS_DECL_NSIQUOTAUSAGERESULT
 };
diff --git a/dom/quota/nsIQuotaResults.idl b/dom/quota/nsIQuotaResults.idl
--- a/dom/quota/nsIQuotaResults.idl
+++ b/dom/quota/nsIQuotaResults.idl
@@ -9,16 +9,18 @@
 [scriptable, function, uuid(d8c9328b-9aa8-4f5d-90e6-482de4a6d5b8)]
 interface nsIQuotaUsageResult : nsISupports
 {
   readonly attribute ACString origin;
 
   readonly attribute boolean persisted;
 
   readonly attribute unsigned long long usage;
+
+  readonly attribute unsigned long long lastAccessed;
 };
 
 [scriptable, function, uuid(96df03d2-116a-493f-bb0b-118c212a6b32)]
 interface nsIQuotaOriginUsageResult : nsISupports
 {
   readonly attribute unsigned long long usage;
 
   readonly attribute unsigned long long fileUsage;
