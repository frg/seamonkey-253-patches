# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1570733433 0
# Node ID 2921dbcceadc9adb0f12188f62a8f86bd18b0f93
# Parent  a8593c5355463cc463b05de64f34e635172f99f6
Bug 1587206 - [lint.flake8] Enable F632 across the tree, r=sylvestre

This ensures we use ==/!= to strings and ints (instead of is/is not).

Differential Revision: https://phabricator.services.mozilla.com/D48609

diff --git a/.flake8 b/.flake8
--- a/.flake8
+++ b/.flake8
@@ -76,17 +76,17 @@ exclude =
     tools/crashreporter/*.configure,
     .ycm_extra_conf.py,
 
 # See:
 #   - http://flake8.pycqa.org/en/latest/user/error-codes.html
 #   - http://pep8.readthedocs.io/en/latest/intro.html#configuration
 ignore =
     # These should be triaged and either fixed or moved to the list below.
-    F632, F633, F811, E117, W504, W605, W606,
+    F633, F811, E117, W504, W605, W606,
     # These are intentionally disabled (not necessarily for good reason).
     #   F723: syntax error in type comment
     #       text contains quotes which breaks our custom JSON formatter
     F723, E121, E123, E126, E129, E133, E226, E241, E242, E402, E704, E741, W503,
 
 per-file-ignores =
     ipc/ipdl/*: F403, F405
     # cpp_eclipse has a lot of multi-line embedded XML which exceeds line length
diff --git a/js/src/gdb/mozilla/unwind.py b/js/src/gdb/mozilla/unwind.py
--- a/js/src/gdb/mozilla/unwind.py
+++ b/js/src/gdb/mozilla/unwind.py
@@ -502,17 +502,17 @@ class x64UnwinderState(UnwinderState):
     def unwind_entry_frame_registers(self, sp, unwind_info):
         sp = sp.cast(self.typecache.void_starstar)
         # Skip the "result" push.
         sp = sp + 1
         for reg in self.PUSHED_REGS:
             data = sp.dereference()
             sp = sp + 1
             unwind_info.add_saved_register(reg, data)
-            if reg is "rbp":
+            if reg == "rbp":
                 unwind_info.add_saved_register(self.SP_REGISTER, sp)
 
 # The unwinder object.  This provides the "user interface" to the JIT
 # unwinder, and also handles constructing or destroying UnwinderState
 # objects as needed.
 class SpiderMonkeyUnwinder(Unwinder):
     # A list of all the possible unwinders.  See |self.make_unwinder|.
     UNWINDERS = [x64UnwinderState]
diff --git a/js/src/tests/jstests.py b/js/src/tests/jstests.py
--- a/js/src/tests/jstests.py
+++ b/js/src/tests/jstests.py
@@ -263,17 +263,17 @@ def load_tests(options, requested_paths,
 
     if options.js_shell is None:
         xul_tester = manifest.NullXULInfoTester()
     else:
         if options.xul_info_src is None:
             xul_info = manifest.XULInfo.create(options.js_shell)
         else:
             xul_abi, xul_os, xul_debug = options.xul_info_src.split(r':')
-            xul_debug = xul_debug.lower() is 'true'
+            xul_debug = xul_debug.lower() == 'true'
             xul_info = manifest.XULInfo(xul_abi, xul_os, xul_debug)
         xul_tester = manifest.XULInfoTester(xul_info, options.js_shell)
 
     test_dir = dirname(abspath(__file__))
     test_count = manifest.count_tests(test_dir, requested_paths, excluded_paths)
     test_gen = manifest.load_reftests(test_dir, requested_paths, excluded_paths,
                                       xul_tester)
 
diff --git a/python/mozrelease/mozrelease/versions.py b/python/mozrelease/mozrelease/versions.py
--- a/python/mozrelease/mozrelease/versions.py
+++ b/python/mozrelease/mozrelease/versions.py
@@ -26,17 +26,17 @@ class MozillaVersionCompareMixin():
                     LooseModernMozillaVersion(str(other)))
         else:
             # No versions are loose, therefore we can use StrictVersion
             val = StrictVersion.__cmp__(self, other)
         if has_esr.isdisjoint(set(['other', 'self'])) or \
                 has_esr.issuperset(set(['other', 'self'])):
             #  If both had esr string or neither, then cmp() was accurate
             return val
-        elif val is not 0:
+        elif val != 0:
             # cmp is accurate here even if esr is present in only 1 compare, since
             # versions are not equal
             return val
         elif 'other' in has_esr:
             return -1  # esr is not greater than non esr
         return 1  # non esr is greater than esr
 
 
diff --git a/testing/marionette/client/marionette_driver/gestures.py b/testing/marionette/client/marionette_driver/gestures.py
--- a/testing/marionette/client/marionette_driver/gestures.py
+++ b/testing/marionette/client/marionette_driver/gestures.py
@@ -20,17 +20,17 @@ def smooth_scroll(marionette_session, st
     if axis not in ["x", "y"]:
         raise Exception("Axis must be either 'x' or 'y'")
     if direction not in [-1, 0]:
         raise Exception("Direction must either be -1 negative or 0 positive")
     increments = increments or 100
     wait_period = wait_period or 0.05
     scroll_back = scroll_back or False
     current = 0
-    if axis is "x":
+    if axis == "x":
         if direction is -1:
             offset = [-increments, 0]
         else:
             offset = [increments, 0]
     else:
         if direction is -1:
             offset = [0, -increments]
         else:
diff --git a/testing/mozbase/mozinstall/mozinstall/mozinstall.py b/testing/mozbase/mozinstall/mozinstall/mozinstall.py
--- a/testing/mozbase/mozinstall/mozinstall/mozinstall.py
+++ b/testing/mozbase/mozinstall/mozinstall/mozinstall.py
@@ -220,17 +220,17 @@ def uninstall(install_folder):
         uninstall_folder = '%s\\uninstall' % install_folder
         log_file = '%s\\uninstall.log' % uninstall_folder
 
         if os.path.isfile(log_file):
             trbk = None
             try:
                 cmdArgs = ['%s\\uninstall\helper.exe' % install_folder, '/S']
                 result = subprocess.call(cmdArgs)
-                if result is not 0:
+                if result != 0:
                     raise Exception('Execution of uninstaller failed.')
 
                 # The uninstaller spawns another process so the subprocess call
                 # returns immediately. We have to wait until the uninstall
                 # folder has been removed or until we run into a timeout.
                 end_time = time.time() + TIMEOUT_UNINSTALL
                 while os.path.exists(uninstall_folder):
                     time.sleep(1)
diff --git a/testing/talos/talos/output.py b/testing/talos/talos/output.py
--- a/testing/talos/talos/output.py
+++ b/testing/talos/talos/output.py
@@ -128,17 +128,17 @@ class Output(object):
 
                     # mainthread IO is a list of filenames and accesses, we do
                     # not report this as a counter
                     if 'mainthreadio' in name:
                         continue
 
                     # responsiveness has it's own metric, not the mean
                     # TODO: consider doing this for all counters
-                    if 'responsiveness' is name:
+                    if 'responsiveness' == name:
                         subtest = {
                             'name': name,
                             'value': filter.responsiveness_Metric(vals)
                         }
                         counter_subtests.append(subtest)
                         continue
 
                     subtest = {
diff --git a/xpcom/idl-parser/xpidl/xpidl.py b/xpcom/idl-parser/xpidl/xpidl.py
--- a/xpcom/idl-parser/xpidl/xpidl.py
+++ b/xpcom/idl-parser/xpidl/xpidl.py
@@ -511,17 +511,17 @@ class Interface(object):
                 if hasattr(member, 'doccomments'):
                     member.doccomments[0:0] = self.doccomments
                     break
             self.doccomments = parent.getName(self.name, None).doccomments
 
         if self.attributes.function:
             has_method = False
             for member in self.members:
-                if member.kind is 'method':
+                if member.kind == 'method':
                     if has_method:
                         raise IDLError(
                             "interface '%s' has multiple methods, but marked 'function'" %
                             self.name, self.location)
                     else:
                         has_method = True
 
         parent.setName(self)
