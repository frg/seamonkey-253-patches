# HG changeset patch
# User Valentin Gosu <valentin.gosu@gmail.com>
# Date 1516632757 -3600
# Node ID d89605a285130c256278fa48420e49cb9a2e9527
# Parent  f0387428bf02d4c4c12a4a9426b346f7e6861e1c
Bug 1431760 - Add NS_MutateURI.Apply that allows calling methods declared in other interfaces implemented by NS_MutateURI::mMutator r=mayhemer

* The method can be chained just as the other methods on NS_MutateURI.
* In case the mutator object does not implement the interface, mStatus will be set to an error code.
* This is useful when you are constructing a new URI and the type of the mutator is known. I expect a future patch will add a MaybeApply method, that does not set mStatus to an error code if the mutator does not implement the interface.
* This patch changes nsHostObjectProtocolHandler::NewURI to use the new method and avoid a static_cast<nsHostObjectURI*>(uri)


MozReview-Commit-ID: 9kvXJX54gUP

diff --git a/dom/file/nsHostObjectProtocolHandler.cpp b/dom/file/nsHostObjectProtocolHandler.cpp
--- a/dom/file/nsHostObjectProtocolHandler.cpp
+++ b/dom/file/nsHostObjectProtocolHandler.cpp
@@ -883,29 +883,32 @@ nsHostObjectProtocolHandler::NewURI(cons
                                     nsIURI *aBaseURI,
                                     nsIURI **aResult)
 {
   *aResult = nullptr;
   nsresult rv;
 
   DataInfo* info = GetDataInfo(aSpec);
 
+  nsCOMPtr<nsIPrincipal> principal;
+  RefPtr<mozilla::dom::BlobImpl> blob;
+  if (info && info->mObjectType == DataInfo::eBlobImpl) {
+    MOZ_ASSERT(info->mBlobImpl);
+    principal = info->mPrincipal;
+    blob = info->mBlobImpl;
+  }
+
   nsCOMPtr<nsIURI> uri;
   rv = NS_MutateURI(new nsHostObjectURI::Mutator())
          .SetSpec(aSpec)
+         .Apply<nsIBlobURIMutator>(&nsIBlobURIMutator::SetBlobImpl, blob)
+         .Apply<nsIPrincipalURIMutator>(&nsIPrincipalURIMutator::SetPrincipal, principal)
          .Finalize(uri);
   NS_ENSURE_SUCCESS(rv, rv);
 
-  RefPtr<nsHostObjectURI> hostURI = static_cast<nsHostObjectURI*>(uri.get());
-  if (info && info->mObjectType == DataInfo::eBlobImpl) {
-    MOZ_ASSERT(info->mBlobImpl);
-    hostURI->mPrincipal = info->mPrincipal;
-    hostURI->mBlobImpl = info->mBlobImpl;
-  }
-
   NS_TryToSetImmutable(uri);
   uri.forget(aResult);
 
   if (info && info->mObjectType == DataInfo::eBlobImpl) {
     info->mURIs.AppendElement(do_GetWeakReference(*aResult));
   }
 
   return NS_OK;
diff --git a/dom/file/nsHostObjectURI.cpp b/dom/file/nsHostObjectURI.cpp
--- a/dom/file/nsHostObjectURI.cpp
+++ b/dom/file/nsHostObjectURI.cpp
@@ -225,17 +225,17 @@ nsHostObjectURI::EqualsInternal(nsIURI* 
     // Both of us have mPrincipals. Compare them.
     return mPrincipal->Equals(otherUri->mPrincipal, aResult);
   }
   // else, at least one of us lacks a principal; only equal if *both* lack it.
   *aResult = (!mPrincipal && !otherUri->mPrincipal);
   return NS_OK;
 }
 
-NS_IMPL_ISUPPORTS(nsHostObjectURI::Mutator, nsIURISetters, nsIURIMutator)
+NS_IMPL_ISUPPORTS(nsHostObjectURI::Mutator, nsIURISetters, nsIURIMutator, nsIBlobURIMutator, nsIPrincipalURIMutator)
 
 NS_IMETHODIMP
 nsHostObjectURI::Mutate(nsIURIMutator** aMutator)
 {
     RefPtr<nsHostObjectURI::Mutator> mutator = new nsHostObjectURI::Mutator();
     nsresult rv = mutator->InitFromURI(this);
     if (NS_FAILED(rv)) {
         return rv;
diff --git a/dom/file/nsHostObjectURI.h b/dom/file/nsHostObjectURI.h
--- a/dom/file/nsHostObjectURI.h
+++ b/dom/file/nsHostObjectURI.h
@@ -76,21 +76,43 @@ public:
 
 protected:
   virtual ~nsHostObjectURI() {}
 
 public:
   class Mutator
     : public nsIURIMutator
     , public BaseURIMutator<nsHostObjectURI>
+    , public nsIBlobURIMutator
+    , public nsIPrincipalURIMutator
   {
     NS_DECL_ISUPPORTS
     NS_FORWARD_SAFE_NSIURISETTERS_RET(mURI)
     NS_DEFINE_NSIMUTATOR_COMMON
 
+    MOZ_MUST_USE NS_IMETHOD
+    SetBlobImpl(mozilla::dom::BlobImpl *aBlobImpl) override
+    {
+        if (!mURI) {
+            return NS_ERROR_NULL_POINTER;
+        }
+        mURI->mBlobImpl = aBlobImpl;
+        return NS_OK;
+    }
+
+    MOZ_MUST_USE NS_IMETHOD
+    SetPrincipal(nsIPrincipal *aPrincipal) override
+    {
+        if (!mURI) {
+            return NS_ERROR_NULL_POINTER;
+        }
+        mURI->mPrincipal = aPrincipal;
+        return NS_OK;
+    }
+
     explicit Mutator() { }
   private:
     virtual ~Mutator() { }
 
     friend class nsHostObjectURI;
   };
 };
 
diff --git a/netwerk/base/nsIURIMutator.idl b/netwerk/base/nsIURIMutator.idl
--- a/netwerk/base/nsIURIMutator.idl
+++ b/netwerk/base/nsIURIMutator.idl
@@ -364,16 +364,42 @@ public:
   }
   NS_MutateURI& SetQueryWithEncoding(const nsACString& query, const mozilla::Encoding *encoding)
   {
     NS_ENSURE_SUCCESS(mStatus, *this);
     mStatus = mMutator->SetQueryWithEncoding(query, encoding, nullptr);
     return *this;
   }
 
+  /**
+   * This method allows consumers to call the methods declared in other
+   * interfaces implemented by the mutator object.
+   *
+   * Example:
+   * nsCOMPtr<nsIURI> uri;
+   * nsresult rv = NS_MutateURI(new URIClass::Mutator())
+   *                 .SetSpec(aSpec)
+   *                 .Apply<SomeInterface>(&SomeInterface::Method, arg1, arg2)
+   *                 .Finalize(uri);
+   *
+   * If mMutator does not implement SomeInterface, do_QueryInterface will fail
+   * and the method will not be called.
+   * If aMethod does not exist, or if there is a mismatch between argument
+   * types, or the number of arguments, then there will be a compile error.
+   */
+  template <typename Interface, typename Method, typename... Args>
+  NS_MutateURI& Apply(Method aMethod, Args ...aArgs)
+  {
+    NS_ENSURE_SUCCESS(mStatus, *this);
+    nsCOMPtr<Interface> target = do_QueryInterface(mMutator, &mStatus);
+    NS_ENSURE_SUCCESS(mStatus, *this);
+    mStatus = (target->*aMethod)(aArgs...);
+    return *this;
+  }
+
   template <class C>
   MOZ_MUST_USE nsresult Finalize(nsCOMPtr<C>& aURI)
   {
     NS_ENSURE_SUCCESS(mStatus, mStatus);
 
     nsCOMPtr<nsIURI> uri;
     mStatus = mMutator->Finalize(getter_AddRefs(uri));
     NS_ENSURE_SUCCESS(mStatus, mStatus);
diff --git a/netwerk/base/nsIURIWithBlobImpl.idl b/netwerk/base/nsIURIWithBlobImpl.idl
--- a/netwerk/base/nsIURIWithBlobImpl.idl
+++ b/netwerk/base/nsIURIWithBlobImpl.idl
@@ -1,20 +1,40 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsISupports.idl"
 
 interface nsIURI;
 
+%{C++
+namespace mozilla {
+namespace dom {
+  class BlobImpl;
+}}
+%}
+
+[ptr] native BlobImplPtr(mozilla::dom::BlobImpl);
+
 /**
  * nsIURIWithBlobImpl is implemented by URIs which are associated with a
  * specific BlobImpl.
  */
 [builtinclass, uuid(331b41d3-3506-4ab5-bef9-aab41e3202a3)]
 interface nsIURIWithBlobImpl : nsISupports
 {
     /**
      * The BlobImpl associated with the resource returned when loading this uri.
      */
     readonly attribute nsISupports blobImpl;
 };
+
+[builtinclass, uuid(d3e8c9fa-ff07-47cc-90dc-0cc5445ddb59)]
+interface nsIBlobURIMutator : nsISupports
+{
+    /**
+     * Associates a blobImpl to the mutated URI.
+     * Would normally return nsIURIMutator, but since it only gets called
+     * from C++, there is no need for that.
+     */
+    [must_use, noscript] void setBlobImpl(in BlobImplPtr blobImpl);
+};
diff --git a/netwerk/base/nsIURIWithPrincipal.idl b/netwerk/base/nsIURIWithPrincipal.idl
--- a/netwerk/base/nsIURIWithPrincipal.idl
+++ b/netwerk/base/nsIURIWithPrincipal.idl
@@ -1,16 +1,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsISupports.idl"
 
 interface nsIPrincipal;
 interface nsIURI;
+interface nsIURIMutator;
 
 /**
  * nsIURIWithPrincipal is implemented by URIs which are associated with a
  * specific principal.
  */
 [scriptable, uuid(626a5c0c-bfd8-4531-8b47-a8451b0daa33)]
 interface nsIURIWithPrincipal : nsISupports
 {
@@ -20,8 +21,19 @@ interface nsIURIWithPrincipal : nsISuppo
      */
     readonly attribute nsIPrincipal principal;
 
     /**
      * The uri for the principal.
      */
     readonly attribute nsIURI principalUri;
 };
+
+[builtinclass, uuid(fa138a89-c76e-4b7f-95ec-c7b56ded5ef5)]
+interface nsIPrincipalURIMutator : nsISupports
+{
+    /**
+     * Associates a principal to the mutated URI.
+     * Would normally return nsIURIMutator, but since it only gets called
+     * from C++, there is no need for that.
+     */
+    [must_use, noscript] void setPrincipal(in nsIPrincipal aPrincipal);
+};
