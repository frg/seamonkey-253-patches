# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1586984338 0
# Node ID 32a048d1d8035a8801431614e2781af98aa9c6a9
# Parent  d03cb9556fed8a98ca8705edf95b585a694191df
Bug 1630317 - Add a warning if there is an attempt to mach bootstrap from an "old commit" r=nalexander

Differential Revision: https://phabricator.services.mozilla.com/D71076

diff --git a/python/mozboot/mozboot/bootstrap.py b/python/mozboot/mozboot/bootstrap.py
--- a/python/mozboot/mozboot/bootstrap.py
+++ b/python/mozboot/mozboot/bootstrap.py
@@ -1,20 +1,21 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this file,
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 from collections import OrderedDict
 
+import os
 import platform
 import sys
-import os
 import subprocess
+import time
 
 # NOTE: This script is intended to be run with a vanilla Python install.  We
 # have to rely on the standard library instead of Python 2+3 helpers like
 # the six module.
 if sys.version_info < (3,):
     from ConfigParser import (
         Error as ConfigParserError,
         RawConfigParser,
@@ -180,16 +181,25 @@ may be called ~/.bashrc or ~/.bash_profi
 lines:
 
     export PATH="{}:$PATH"
 
 Then restart your shell.
 '''
 
 
+OLD_REVISION_WARNING = '''
+WARNING! You appear to be running `mach bootstrap` from an old revision.
+bootstrap is meant primarily for getting developer environments up-to-date to
+build the latest version of tree. Running bootstrap on old revisions may fail
+and is not guaranteed to bring your machine to any working state in particular.
+Proceed at your own peril.
+'''
+
+
 class Bootstrapper(object):
     """Main class that performs system bootstrap."""
 
     def __init__(self, finished=FINISHED, choice=None, no_interactive=False,
                  hg_configure=False, no_system_changes=False, mach_context=None,
                  vcs=None):
         self.instance = None
         self.finished = finished
@@ -600,28 +610,30 @@ def current_firefox_checkout(check_outpu
         if hg and os.path.exists(hg_dir):
             # Verify the hg repo is a Firefox repo by looking at rev 0.
             try:
                 node = check_output([hg, 'log', '-r', '0', '--template', '{node}'],
                                     cwd=path,
                                     env=env,
                                     universal_newlines=True)
                 if node in HG_ROOT_REVISIONS:
+                    _warn_if_risky_revision(path)
                     return ('hg', path)
                 # Else the root revision is different. There could be nested
                 # repos. So keep traversing the parents.
             except subprocess.CalledProcessError:
                 pass
 
         # Just check for known-good files in the checkout, to prevent attempted
         # foot-shootings.  Determining a canonical git checkout of mozilla-unified
         # is...complicated
         elif os.path.exists(git_dir):
             moz_configure = os.path.join(path, 'moz.configure')
             if os.path.exists(moz_configure):
+                _warn_if_risky_revision(path)
                 return ('git', path)
 
         path, child = os.path.split(path)
         if child == '':
             break
 
     return (None, None)
 
@@ -683,16 +695,28 @@ def update_git_repo(git, url, dest):
 
 def configure_git(git, root_state_dir, top_src_dir):
     """Run the Git configuration steps."""
     cinnabar_dir = update_git_tools(git, root_state_dir, top_src_dir)
 
     print(ADD_GIT_TOOLS_PATH.format(cinnabar_dir))
 
 
+def _warn_if_risky_revision(path):
+    # Warn the user if they're trying to bootstrap from an obviously old
+    # version of tree as reported by the version control system (a month in
+    # this case). This is an approximate calculation but is probably good
+    # enough for our purposes.
+    NUM_SECONDS_IN_MONTH = 60 * 60 * 24 * 30
+    from mozversioncontrol import get_repository_object
+    repo = get_repository_object(path)
+    if (time.time() - repo.get_commit_time()) >= NUM_SECONDS_IN_MONTH:
+        print(OLD_REVISION_WARNING)
+
+
 def git_clone_firefox(git, dest, watchman=None):
     """Clone the Firefox repository to a specified destination."""
     print('Cloning Firefox repository to %s' % dest)
 
     try:
         # Configure git per the git-cinnabar requirements.
         subprocess.check_call([git, 'clone', '-b', 'bookmarks/central',
                                'hg::https://hg.mozilla.org/mozilla-unified', dest])
diff --git a/python/mozversioncontrol/mozversioncontrol/__init__.py b/python/mozversioncontrol/mozversioncontrol/__init__.py
--- a/python/mozversioncontrol/mozversioncontrol/__init__.py
+++ b/python/mozversioncontrol/mozversioncontrol/__init__.py
@@ -144,16 +144,21 @@ class Repository(object):
     def head_ref(self):
         """Hash of HEAD revision."""
 
     @abc.abstractproperty
     def base_ref(self):
         """Hash of revision the current topic branch is based on."""
 
     @abc.abstractmethod
+    def get_commit_time(self):
+        """Return the Unix time of the HEAD revision.
+        """
+
+    @abc.abstractmethod
     def sparse_checkout_present(self):
         """Whether the working directory is using a sparse checkout.
 
         A sparse checkout is defined as a working directory that only
         materializes a subset of files in a given revision.
 
         Returns a bool.
         """
@@ -289,16 +294,20 @@ class HgRepository(Repository):
     def _run(self, *args, **runargs):
         if not self._client.server:
             return super(HgRepository, self)._run(*args, **runargs)
 
         # hglib requires bytes on python 3
         args = [a.encode('utf-8') if not isinstance(a, bytes) else a for a in args]
         return self._client.rawcommand(args).decode('utf-8')
 
+    def get_commit_time(self):
+        return int(self._run(
+            'parent', '--template', '{word(0, date|hgdate)}').strip())
+
     def sparse_checkout_present(self):
         # We assume a sparse checkout is enabled if the .hg/sparse file
         # has data. Strictly speaking, we should look for a requirement in
         # .hg/requires. But since the requirement is still experimental
         # as of Mercurial 4.3, it's probably more trouble than its worth
         # to verify it.
         sparse = os.path.join(self.path, '.hg', 'sparse')
 
@@ -424,16 +433,19 @@ class GitRepository(Repository):
     @property
     def has_git_cinnabar(self):
         try:
             self._run('cinnabar', '--version')
         except subprocess.CalledProcessError:
             return False
         return True
 
+    def get_commit_time(self):
+        return int(self._run('log', '-1', '--format=%ct').strip())
+
     def sparse_checkout_present(self):
         # Not yet implemented.
         return False
 
     def get_upstream(self):
         ref = self._run('symbolic-ref', '-q', 'HEAD').strip()
         upstream = self._run('for-each-ref', '--format=%(upstream:short)', ref).strip()
 
