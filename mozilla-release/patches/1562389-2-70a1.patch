# HG changeset patch
# User Makoto Kato <m_kato@ga2.so-net.ne.jp>
# Date 1562582140 0
# Node ID c3c853745453f187d60d195a04c5d877f04de896
# Parent  cbb88524ab230cfdaa8a36c8136568e45d521ac4
Bug 1562389 - Better detect non-clang-cl native windows clang. r=glandium

When building Gecko/Android/aarch64 on Windows, `--target` parameter may not be incorrect value. Although `check_compiler`'s `info` is target compiler, clang on Windows is always detected as `clang-cl`, not `clang`.

```
c:/Users/mkato/.mozbuild/clang/bin/clang.exe -E  -dM - < /dev/null
...
#define _MSC_VER 1916
```

So even if using clang on Windows, not clang-cl, we should detect as 'clang' correctly

Differential Revision: https://phabricator.services.mozilla.com/D36422

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -746,16 +746,18 @@ def split_triplet(triplet, allow_unknown
         return CPU_bitness[cpu]
 
     # Toolchains, most notably for cross compilation may use cpu-os
     # prefixes. We need to be more specific about the LLVM target on Mac
     # so cross-language LTO will work correctly.
 
     if os.startswith('darwin'):
         toolchain = '%s-apple-%s' % (cpu, os)
+    elif canonical_cpu == 'aarch64' and canonical_os == 'WINNT':
+        toolchain = 'aarch64-windows-msvc'
     else:
         toolchain = '%s-%s' % (cpu, os)
 
     return namespace(
         alias=triplet,
         cpu=sanitize(CPU, canonical_cpu),
         bitness=sanitize(bitness, canonical_cpu),
         kernel=sanitize(Kernel, canonical_kernel),
diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -415,16 +415,18 @@ def get_compiler_info(compiler, language
     # Note: We'd normally do a version check for clang, but versions of clang
     # in Xcode have a completely different versioning scheme despite exposing
     # the version with the same defines.
     # So instead, we make things such that the version is missing when the
     # clang used is below the minimum supported version (currently clang 4.0).
     # We then only include the version information when the compiler matches
     # the feature check, so that an unsupported version of clang would have
     # no version number.
+    # Bug 1562389: We don't check _MT here because it is either clang-cl or
+    # msvc. Building Android is not supported or used anyway.
     check = dedent('''\
         #if defined(_MSC_VER)
         #if defined(__clang__)
         %COMPILER "clang-cl"
         %VERSION _MSC_FULL_VER
         #else
         %COMPILER "msvc"
         %VERSION _MSC_FULL_VER
@@ -581,18 +583,17 @@ def check_compiler(compiler, language, t
 
     if not has_target and (not info.cpu or info.cpu != target.cpu and info.type != 'msvc'):
         same_arch = same_arch_different_bits()
         if (target.cpu, info.cpu) in same_arch:
             flags.append('-m32')
         elif (info.cpu, target.cpu) in same_arch:
             flags.append('-m64')
         elif info.type == 'clang-cl' and target.cpu == 'aarch64':
-            # clang-cl uses a different name for this target
-            flags.append('--target=aarch64-windows-msvc')
+            flags.append('--target=%s' % target.toolchain)
         elif info.type == 'clang':
             flags.append('--target=%s' % target.toolchain)
 
     return namespace(
         type=info.type,
         version=info.version,
         target_cpu=info.cpu,
         target_kernel=info.kernel,
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
@@ -225,16 +225,17 @@ CLANG_PLATFORM_X86_64_WIN = CLANG_PLATFO
 @memoize
 def VS(version):
     version = Version(version)
     return FakeCompiler({
         None: {
             '_MSC_VER': '%02d%02d' % (version.major, version.minor),
             '_MSC_FULL_VER': '%02d%02d%05d' % (version.major, version.minor,
                                                version.patch),
+            '_MT': '1',
         },
         '*.cpp': DEFAULT_CXX_97,
     })
 
 
 VS_2013u2 = VS('18.00.30501')
 VS_2013u3 = VS('18.00.30723')
 VS_2015 = VS('19.00.23026')
