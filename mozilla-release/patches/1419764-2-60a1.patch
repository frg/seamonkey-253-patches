# HG changeset patch
# User Robert Longson <longsonr@gmail.com>
# Date 1517089148 0
# Node ID 003668afcef5a0919792fc1331d44e6e8c2fbb40
# Parent  cec2f73a3bad64e0bcf253f87a634a730e4ab684
Bug 1419764 - change mHadTransformBeforeLastBaseValChange to mRequiresFrameReconstruction in order to better reflect its purpose r=dholbert

diff --git a/dom/svg/SVGTransformableElement.cpp b/dom/svg/SVGTransformableElement.cpp
--- a/dom/svg/SVGTransformableElement.cpp
+++ b/dom/svg/SVGTransformableElement.cpp
@@ -67,19 +67,21 @@ SVGTransformableElement::GetAttributeCha
     bool isAdditionOrRemoval = false;
     if (aModType == nsIDOMMutationEvent::ADDITION ||
         aModType == nsIDOMMutationEvent::REMOVAL) {
       isAdditionOrRemoval = true;
     } else {
       MOZ_ASSERT(aModType == nsIDOMMutationEvent::MODIFICATION,
                  "Unknown modification type.");
       if (!mTransforms ||
-          !mTransforms->HasTransform() ||
-          !mTransforms->HadTransformBeforeLastBaseValChange()) {
-        // New or old value is empty; this is effectively addition or removal.
+          !mTransforms->HasTransform()) {
+        // New value is empty, treat as removal.
+        isAdditionOrRemoval = true;
+      } else if (mTransforms->RequiresFrameReconstruction()) {
+        // Old value was empty, treat as addition.
         isAdditionOrRemoval = true;
       }
     }
 
     if (isAdditionOrRemoval) {
       // Reconstruct the frame tree to handle stacking context changes:
       retval |= nsChangeHint_ReconstructFrame;
     } else {
diff --git a/dom/svg/nsSVGAnimatedTransformList.cpp b/dom/svg/nsSVGAnimatedTransformList.cpp
--- a/dom/svg/nsSVGAnimatedTransformList.cpp
+++ b/dom/svg/nsSVGAnimatedTransformList.cpp
@@ -57,30 +57,28 @@ nsSVGAnimatedTransformList::SetBaseValue
 
   nsresult rv = mBaseVal.CopyFrom(aValue);
   if (NS_FAILED(rv) && domWrapper) {
     // Attempting to increase mBaseVal's length failed - reduce domWrapper
     // back to the same length:
     domWrapper->InternalBaseValListWillChangeLengthTo(mBaseVal.Length());
   } else {
     mIsAttrSet = true;
-    // If we set this flag to false, we're indicating that aSVGElement's frames
-    // will need reconstructing to account for stacking context changes.
-    // If aSVGElement doesn't have any frames, then that's clearly unnecessary,
-    // so in that case we set the flag to true.
-    mHadTransformBeforeLastBaseValChange =
-      !aSVGElement->GetPrimaryFrame() || hadTransform;
+    // We only need to reconstruct the frame for aSVGElement if it already
+    // exists and the stacking context changes because a transform is created.
+    mRequiresFrameReconstruction =
+      aSVGElement->GetPrimaryFrame() && !hadTransform;
   }
   return rv;
 }
 
 void
 nsSVGAnimatedTransformList::ClearBaseValue()
 {
-  mHadTransformBeforeLastBaseValChange = HasTransform();
+  mRequiresFrameReconstruction = !HasTransform();
 
   SVGAnimatedTransformList *domWrapper =
     SVGAnimatedTransformList::GetDOMWrapperIfExists(this);
   if (domWrapper) {
     // We must send this notification *before* changing mBaseVal! (See above.)
     domWrapper->InternalBaseValListWillChangeLengthTo(0);
   }
   mBaseVal.Clear();
diff --git a/dom/svg/nsSVGAnimatedTransformList.h b/dom/svg/nsSVGAnimatedTransformList.h
--- a/dom/svg/nsSVGAnimatedTransformList.h
+++ b/dom/svg/nsSVGAnimatedTransformList.h
@@ -42,17 +42,17 @@ class nsSVGAnimatedTransformList
 {
   // friends so that they can get write access to mBaseVal
   friend class dom::SVGTransform;
   friend class DOMSVGTransformList;
 
 public:
   nsSVGAnimatedTransformList()
     : mIsAttrSet(false),
-      mHadTransformBeforeLastBaseValChange(false) { }
+      mRequiresFrameReconstruction(true) { }
 
   /**
    * Because it's so important that mBaseVal and its DOMSVGTransformList wrapper
    * (if any) be kept in sync (see the comment in
    * SVGAnimatedTransformList::InternalBaseValListWillChangeTo), this method
    * returns a const reference. Only our friend classes may get mutable
    * references to mBaseVal.
    */
@@ -93,44 +93,44 @@ public:
   bool HasTransform() const
     { return (mAnimVal && !mAnimVal->IsEmpty()) || !mBaseVal.IsEmpty(); }
 
   bool IsAnimating() const {
     return !!mAnimVal;
   }
 
   /**
-   * Returns true iff "HasTransform" returned true just before our most recent
-   * SetBaseValue/SetBaseValueString/ClearBaseValue change.
+   * Returns true if we need to reconstruct the frame of the element associated
+   * with this transform list because the stacking context has changed.
    *
    * (This is used as part of an optimization in
    * SVGTransformableElement::GetAttributeChangeHint. That function reports an
    * inexpensive nsChangeHint when a transform has just modified -- but this
    * accessor lets it detect cases where the "modification" is actually adding
    * a transform where we previously had none. These cases require a more
    * thorough nsChangeHint.)
    */
-  bool HadTransformBeforeLastBaseValChange() const {
-    return mHadTransformBeforeLastBaseValChange;
+  bool RequiresFrameReconstruction() const {
+    return mRequiresFrameReconstruction;
   }
 
   mozilla::UniquePtr<nsISMILAttr> ToSMILAttr(nsSVGElement* aSVGElement);
 
 private:
 
   // mAnimVal is a pointer to allow us to determine if we're being animated or
   // not. Making it a non-pointer member and using mAnimVal.IsEmpty() to check
   // if we're animating is not an option, since that would break animation *to*
   // the empty string (<set to="">).
 
   SVGTransformList mBaseVal;
   nsAutoPtr<SVGTransformList> mAnimVal;
   bool mIsAttrSet;
-   // (See documentation for accessor, HadTransformBeforeLastBaseValChange.)
-  bool mHadTransformBeforeLastBaseValChange;
+   // (See documentation for accessor, RequiresFrameReconstruction.)
+  bool mRequiresFrameReconstruction;
 
   struct SMILAnimatedTransformList : public nsISMILAttr
   {
   public:
     SMILAnimatedTransformList(nsSVGAnimatedTransformList* aVal,
                               nsSVGElement* aSVGElement)
       : mVal(aVal)
       , mElement(aSVGElement)
