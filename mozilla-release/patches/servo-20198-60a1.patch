# HG changeset patch
# User Anthony Ramine <n.oxyde@gmail.com>
# Date 1520256275 18000
# Node ID 0c69a5236f6c9fa6d6fdc7e4b8ebc52207992b12
# Parent  71ff0c373966ee7779632e2b7f262d295ecf3d7a
servo: Merge #20198 - Use darling::util::Override in #[derive(ToCss)] (from servo:derive-all-the-things); r=emilio

Source-Repo: https://github.com/servo/servo
Source-Revision: 7931df716d3f2145758b5bfc278fa345d3b3b327

diff --git a/servo/components/style_derive/to_css.rs b/servo/components/style_derive/to_css.rs
--- a/servo/components/style_derive/to_css.rs
+++ b/servo/components/style_derive/to_css.rs
@@ -1,14 +1,14 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 use cg;
-use darling::{Error, FromMetaItem};
+use darling::util::Override;
 use quote::Tokens;
 use syn::{self, Ident};
 use synstructure;
 
 pub fn derive(input: syn::DeriveInput) -> Tokens {
     let name = &input.ident;
     let trait_path = parse_quote!(style_traits::ToCss);
     let (impl_generics, ty_generics, mut where_clause) =
@@ -75,17 +75,17 @@ pub fn derive(input: syn::DeriveInput) -
         };
 
         if variant_attrs.dimension {
             expr = quote! {
                 #expr?;
                 ::std::fmt::Write::write_str(dest, #identifier)
             }
         } else if let Some(function) = variant_attrs.function {
-            let mut identifier = function.name.map_or(identifier, |name| name.to_string());
+            let mut identifier = function.explicit().map_or(identifier, |name| name.to_string());
             identifier.push_str("(");
             expr = quote! {
                 ::std::fmt::Write::write_str(dest, #identifier)?;
                 #expr?;
                 ::std::fmt::Write::write_str(dest, ")")
             }
         }
         Some(expr)
@@ -124,45 +124,32 @@ pub fn derive(input: syn::DeriveInput) -
 
     impls
 }
 
 #[darling(attributes(css), default)]
 #[derive(Default, FromDeriveInput)]
 struct CssInputAttrs {
     derive_debug: bool,
-    function: Option<Function>,
+    // Here because structs variants are also their whole type definition.
+    function: Option<Override<Ident>>,
+    // Here because structs variants are also their whole type definition.
     comma: bool,
     // Here because structs variants are also their whole type definition.
     iterable: bool,
 }
 
 #[darling(attributes(css), default)]
 #[derive(Default, FromVariant)]
 pub struct CssVariantAttrs {
-    pub function: Option<Function>,
+    pub function: Option<Override<Ident>>,
     pub iterable: bool,
     pub comma: bool,
     pub dimension: bool,
     pub keyword: Option<String>,
     pub aliases: Option<String>,
 }
 
 #[darling(attributes(css), default)]
 #[derive(Default, FromField)]
 struct CssFieldAttrs {
     ignore_bound: bool,
 }
-
-pub struct Function {
-    name: Option<Ident>,
-}
-
-impl FromMetaItem for Function {
-    fn from_word() -> Result<Self, Error> {
-        Ok(Self { name: None })
-    }
-
-    fn from_string(name: &str) -> Result<Self, Error> {
-        let name = Ident::from(name);
-        Ok(Self { name: Some(name) })
-    }
-}
