# HG changeset patch
# User Alexandre Poirot <poirot.alex@gmail.com>
# Date 1520762700 -7200
# Node ID 8e5e21375673e0be523fb43205ba07dfba937617
# Parent  0656b5bddd3fd71eddf226b1b681dccc27193a36
Bug 1440321 - Convert Task.jsm to async/await in devtools/client - part 1e. r=jryans

MozReview-Commit-ID: HaGOC5cn3JD

diff --git a/devtools/client/dom/dom-panel.js b/devtools/client/dom/dom-panel.js
--- a/devtools/client/dom/dom-panel.js
+++ b/devtools/client/dom/dom-panel.js
@@ -5,17 +5,16 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 "use strict";
 
 const { Cu } = require("chrome");
 const ObjectClient = require("devtools/shared/client/object-client");
 
 const defer = require("devtools/shared/defer");
 const EventEmitter = require("devtools/shared/old-event-emitter");
-const { Task } = require("devtools/shared/task");
 
 /**
  * This object represents DOM panel. It's responsibility is to
  * render Document Object Model of the current debugger target.
  */
 function DomPanel(iframeWindow, toolbox) {
   this.panelWin = iframeWindow;
   this._toolbox = toolbox;
@@ -31,37 +30,37 @@ function DomPanel(iframeWindow, toolbox)
 
 DomPanel.prototype = {
   /**
    * Open is effectively an asynchronous constructor.
    *
    * @return object
    *         A promise that is resolved when the DOM panel completes opening.
    */
-  open: Task.async(function* () {
+  async open() {
     if (this._opening) {
       return this._opening;
     }
 
     let deferred = defer();
     this._opening = deferred.promise;
 
     // Local monitoring needs to make the target remote.
     if (!this.target.isRemote) {
-      yield this.target.makeRemote();
+      await this.target.makeRemote();
     }
 
     this.initialize();
 
     this.isReady = true;
     this.emit("ready");
     deferred.resolve(this);
 
     return this._opening;
-  }),
+  },
 
   // Initialization
 
   initialize: function() {
     this.panelWin.addEventListener("devtools/content/message",
       this.onContentMessage, true);
 
     this.target.on("navigate", this.onTabNavigated);
@@ -73,32 +72,32 @@ DomPanel.prototype = {
       openLink: this.openLink.bind(this),
     };
 
     exportIntoContentScope(this.panelWin, provider, "DomProvider");
 
     this.shouldRefresh = true;
   },
 
-  destroy: Task.async(function* () {
+  async destroy() {
     if (this._destroying) {
       return this._destroying;
     }
 
     let deferred = defer();
     this._destroying = deferred.promise;
 
     this.target.off("navigate", this.onTabNavigated);
     this._toolbox.off("select", this.onPanelVisibilityChange);
 
     this.emit("destroyed");
 
     deferred.resolve();
     return this._destroying;
-  }),
+  },
 
   // Events
 
   refresh: function() {
     // Do not refresh if the panel isn't visible.
     if (!this.isPanelVisible()) {
       return;
     }
diff --git a/devtools/client/dom/test/browser_dom_array.js b/devtools/client/dom/test/browser_dom_array.js
--- a/devtools/client/dom/test/browser_dom_array.js
+++ b/devtools/client/dom/test/browser_dom_array.js
@@ -9,23 +9,23 @@ const TEST_PAGE_URL = URL_ROOT + "page_a
 const TEST_ARRAY = [
   "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
   "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
 ];
 
 /**
  * Basic test that checks content of the DOM panel.
  */
-add_task(function* () {
+add_task(async function() {
   info("Test DOM Panel Array Expansion started");
 
-  let { panel } = yield addTestTab(TEST_PAGE_URL);
+  let { panel } = await addTestTab(TEST_PAGE_URL);
 
   // Expand specified row and wait till children are displayed.
-  yield expandRow(panel, "_a");
+  await expandRow(panel, "_a");
 
   // Verify that children is displayed now.
   let childRows = getAllRowsForLabel(panel, "_a");
 
   let item = childRows.pop();
   is(item.name, "length", "length property is correct");
   is(item.value, 26, "length property value is 26");
 
diff --git a/devtools/client/dom/test/browser_dom_basic.js b/devtools/client/dom/test/browser_dom_basic.js
--- a/devtools/client/dom/test/browser_dom_basic.js
+++ b/devtools/client/dom/test/browser_dom_basic.js
@@ -5,20 +5,20 @@
 
 "use strict";
 
 const TEST_PAGE_URL = URL_ROOT + "page_basic.html";
 
 /**
  * Basic test that checks content of the DOM panel.
  */
-add_task(function* () {
+add_task(async function() {
   info("Test DOM panel basic started");
 
-  let { panel } = yield addTestTab(TEST_PAGE_URL);
+  let { panel } = await addTestTab(TEST_PAGE_URL);
 
   // Expand specified row and wait till children are displayed.
-  yield expandRow(panel, "_a");
+  await expandRow(panel, "_a");
 
   // Verify that child is displayed now.
   let childRow = getRowByLabel(panel, "_data");
   ok(childRow, "Child row must exist");
 });
diff --git a/devtools/client/dom/test/browser_dom_refresh.js b/devtools/client/dom/test/browser_dom_refresh.js
--- a/devtools/client/dom/test/browser_dom_refresh.js
+++ b/devtools/client/dom/test/browser_dom_refresh.js
@@ -5,21 +5,21 @@
 
 "use strict";
 
 const TEST_PAGE_URL = URL_ROOT + "page_basic.html";
 
 /**
  * Basic test that checks the Refresh action in DOM panel.
  */
-add_task(function* () {
+add_task(async function() {
   info("Test DOM panel basic started");
 
-  let { panel } = yield addTestTab(TEST_PAGE_URL);
+  let { panel } = await addTestTab(TEST_PAGE_URL);
 
   // Create a new variable in the page scope and refresh the panel.
-  yield evaluateJSAsync(panel, "var _b = 10");
-  yield refreshPanel(panel);
+  await evaluateJSAsync(panel, "var _b = 10");
+  await refreshPanel(panel);
 
   // Verify that the variable is displayed now.
   let row = getRowByLabel(panel, "_b");
   ok(row, "New variable must be displayed");
 });
diff --git a/devtools/client/dom/test/head.js b/devtools/client/dom/test/head.js
--- a/devtools/client/dom/test/head.js
+++ b/devtools/client/dom/test/head.js
@@ -220,17 +220,17 @@ function _afterDispatchDone(store, type)
   });
 }
 
 function waitForDispatch(panel, type, eventRepeat = 1) {
   const store = panel.panelWin.view.mainFrame.store;
   const actionType = constants[type];
   let count = 0;
 
-  return Task.spawn(function* () {
+  return (async function() {
     info("Waiting for " + type + " to dispatch " + eventRepeat + " time(s)");
     while (count < eventRepeat) {
-      yield _afterDispatchDone(store, actionType);
+      await _afterDispatchDone(store, actionType);
       count++;
       info(type + " dispatched " + count + " time(s)");
     }
-  });
+  })();
 }
