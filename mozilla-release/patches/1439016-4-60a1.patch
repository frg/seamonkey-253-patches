# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1519060538 -3600
# Node ID 84dbe2d215f78534b9cd22e1e39e4b4104586fdf
# Parent  9be766849b47bd7631750307cd0db1abff8a881c
Bug 1439016: Remove dead code from Shadow DOM v0. r=bholley

Could do that on a different bug but...

MozReview-Commit-ID: L1ri2s8z75f

diff --git a/dom/base/ShadowRoot.cpp b/dom/base/ShadowRoot.cpp
--- a/dom/base/ShadowRoot.cpp
+++ b/dom/base/ShadowRoot.cpp
@@ -53,17 +53,16 @@ NS_IMPL_ADDREF_INHERITED(ShadowRoot, Doc
 NS_IMPL_RELEASE_INHERITED(ShadowRoot, DocumentFragment)
 
 ShadowRoot::ShadowRoot(Element* aElement, bool aClosed,
                        already_AddRefed<mozilla::dom::NodeInfo>&& aNodeInfo,
                        nsXBLPrototypeBinding* aProtoBinding)
   : DocumentFragment(aNodeInfo)
   , DocumentOrShadowRoot(*this)
   , mProtoBinding(aProtoBinding)
-  , mInsertionPointChanged(false)
   , mIsComposedDocParticipant(false)
 {
   SetHost(aElement);
   mMode = aClosed ? ShadowRootMode::Closed : ShadowRootMode::Open;
 
   // Nodes in a shadow tree should never store a value
   // in the subtree root pointer, nodes in the shadow tree
   // track the subtree root using GetContainingShadow().
@@ -423,42 +422,16 @@ ShadowRoot::MaybeReassignElement(Element
       }
       return true;
     }
   }
 
   return false;
 }
 
-void
-ShadowRoot::DistributionChanged()
-{
-  // FIXME(emilio): We could be more granular in a bunch of cases.
-  auto* host = GetHost();
-  if (!host || !host->IsInComposedDoc()) {
-    return;
-  }
-
-  auto* shell = OwnerDoc()->GetShell();
-  if (!shell) {
-    return;
-  }
-
-  shell->DestroyFramesForAndRestyle(host);
-}
-
-void
-ShadowRoot::DistributeAllNodes()
-{
-
-  //XXX Handle <slot>.
-
-  DistributionChanged();
-}
-
 Element*
 ShadowRoot::GetActiveElement()
 {
   return GetRetargetedFocusedElement();
 }
 
 void
 ShadowRoot::GetInnerHTML(nsAString& aInnerHTML)
@@ -522,17 +495,17 @@ ShadowRoot::AttributeChanged(nsIDocument
     return;
   }
 
   auto* shell = OwnerDoc()->GetShell();
   if (!shell) {
     return;
   }
 
-  //XXX optimize this!
+  // FIXME(emilio): We could be more granular in a bunch of cases.
   shell->DestroyFramesForAndRestyle(aElement);
 }
 
 void
 ShadowRoot::ContentAppended(nsIDocument* aDocument,
                             nsIContent* aContainer,
                             nsIContent* aFirstNewContent)
 {
diff --git a/dom/base/ShadowRoot.h b/dom/base/ShadowRoot.h
--- a/dom/base/ShadowRoot.h
+++ b/dom/base/ShadowRoot.h
@@ -80,22 +80,16 @@ public:
   void RemoveSheet(StyleSheet* aSheet);
   bool ApplyAuthorStyles();
   void SetApplyAuthorStyles(bool aApplyAuthorStyles);
   StyleSheetList* StyleSheets()
   {
     return &DocumentOrShadowRoot::EnsureDOMStyleSheets();
   }
 
-  /**
-   * Distributes all the explicit children of the pool host to the content
-   * insertion points in this ShadowRoot.
-   */
-  void DistributeAllNodes();
-
 private:
 
   /**
    * Try to reassign an element to a slot and returns whether the assignment
    * changed.
    */
   bool MaybeReassignElement(Element* aElement, const nsAttrValue* aOldValue);
 
@@ -110,29 +104,20 @@ private:
    * assigned slot if found.
    *
    * Note: slot attribute of aContent may have changed already, so pass slot
    *       name explicity here.
    */
   const HTMLSlotElement* UnassignSlotFor(nsIContent* aContent,
                                          const nsAString& aSlotName);
 
-  /**
-   * Called when we redistribute content after insertion points have changed.
-   */
-  void DistributionChanged();
-
-  bool IsPooledNode(nsIContent* aChild) const;
-
 public:
   void AddSlot(HTMLSlotElement* aSlot);
   void RemoveSlot(HTMLSlotElement* aSlot);
 
-  void SetInsertionPointChanged() { mInsertionPointChanged = true; }
-
   void SetAssociatedBinding(nsXBLBinding* aBinding) { mAssociatedBinding = aBinding; }
 
   JSObject* WrapObject(JSContext* aCx, JS::Handle<JSObject*> aGivenProto) override;
 
   void AddToIdTable(Element* aElement, nsIAtom* aId);
   void RemoveFromIdTable(Element* aElement, nsIAtom* aId);
 
   // WebIDL methods.
@@ -162,22 +147,16 @@ protected:
   nsClassHashtable<nsStringHashKey, nsTArray<mozilla::dom::HTMLSlotElement*>> mSlotMap;
   nsXBLPrototypeBinding* mProtoBinding;
 
   // It is necessary to hold a reference to the associated nsXBLBinding
   // because the binding holds a reference on the nsXBLDocumentInfo that
   // owns |mProtoBinding|.
   RefPtr<nsXBLBinding> mAssociatedBinding;
 
-  // A boolean that indicates that an insertion point was added or removed
-  // from this ShadowRoot and that the nodes need to be redistributed into
-  // the insertion points. After this flag is set, nodes will be distributed
-  // on the next mutation event.
-  bool mInsertionPointChanged;
-
   // Flag to indicate whether the descendants of this shadow root are part of the
   // composed document. Ideally, we would use a node flag on nodes to
   // mark whether it is in the composed document, but we have run out of flags
   // so instead we track it here.
   bool mIsComposedDocParticipant;
 
   nsresult Clone(mozilla::dom::NodeInfo *aNodeInfo, nsINode **aResult,
                  bool aPreallocateChildren) const override;
