# HG changeset patch
# User Robert Longson <longsonr@gmail.com>
# Date 1520970723 0
# Node ID e8276eda78b833e8f0e7dbea73c1033bbe32ce6b
# Parent  643a44d32a20be162e5c1171c6c232adb73dd7e5
Bug 1325320 part 3 - move GetPathLengthScale to SVGGeometryElement and convert callers of SVGPathElement methods to work on the base SVGGeometryElement class and work for all shapes, not just paths r=dholbert

diff --git a/dom/base/nsINode.h b/dom/base/nsINode.h
--- a/dom/base/nsINode.h
+++ b/dom/base/nsINode.h
@@ -393,17 +393,19 @@ public:
     /** data nodes (comments, PIs, text). Nodes of this type always
      returns a non-null value for nsIContent::GetText() */
     eDATA_NODE           = 1 << 8,
     /** HTMLMediaElement */
     eMEDIA               = 1 << 9,
     /** animation elements */
     eANIMATION           = 1 << 10,
     /** filter elements that implement SVGFilterPrimitiveStandardAttributes */
-    eFILTER              = 1 << 11
+    eFILTER              = 1 << 11,
+    /** SVGGeometryElement */
+    eSHAPE               = 1 << 12
   };
 
   /**
    * API for doing a quick check if a content is of a given
    * type, such as Text, Document, Comment ...  Use this when you can instead of
    * checking the tag.
    *
    * @param aFlags what types you want to test for (see above)
diff --git a/dom/svg/SVGContentUtils.cpp b/dom/svg/SVGContentUtils.cpp
--- a/dom/svg/SVGContentUtils.cpp
+++ b/dom/svg/SVGContentUtils.cpp
@@ -98,19 +98,19 @@ GetStrokeDashData(SVGContentUtils::AutoS
       (i % 2 ? totalLengthOfGaps : totalLengthOfDashes) += dashSrc[i];
     }
   } else {
     const nsTArray<nsStyleCoord>& dasharray = aStyleSVG->mStrokeDasharray;
     dashArrayLength = aStyleSVG->mStrokeDasharray.Length();
     if (dashArrayLength <= 0) {
       return eContinuousStroke;
     }
-    if (aElement->IsSVGElement(nsGkAtoms::path)) {
-      pathScale = static_cast<SVGPathElement*>(aElement)->
-        GetPathLengthScale(SVGPathElement::eForStroking);
+    if (aElement->IsNodeOfType(nsINode::eSHAPE)) {
+      pathScale = static_cast<SVGGeometryElement*>(aElement)->
+        GetPathLengthScale(SVGGeometryElement::eForStroking);
       if (pathScale <= 0) {
         return eContinuousStroke;
       }
     }
     Float* dashPattern = aStrokeOptions->InitDashPattern(dashArrayLength);
     if (!dashPattern) {
       return eContinuousStroke;
     }
diff --git a/dom/svg/SVGGeometryElement.cpp b/dom/svg/SVGGeometryElement.cpp
--- a/dom/svg/SVGGeometryElement.cpp
+++ b/dom/svg/SVGGeometryElement.cpp
@@ -48,16 +48,22 @@ SVGGeometryElement::AfterSetAttr(int32_t
   }
   return SVGGeometryElementBase::AfterSetAttr(aNamespaceID, aName,
                                               aValue, aOldValue,
                                               aSubjectPrincipal,
                                               aNotify);
 }
 
 bool
+SVGGeometryElement::IsNodeOfType(uint32_t aFlags) const
+{
+  return !(aFlags & ~eSHAPE);
+}
+
+bool
 SVGGeometryElement::AttributeDefinesGeometry(const nsIAtom *aName)
 {
   if (aName == nsGkAtoms::pathLength) {
     return true;
   }
 
   // Check for nsSVGLength2 attribute
   LengthAttributesInfo info = GetLengthInfo();
@@ -168,13 +174,44 @@ SVGGeometryElement::GetPointAtLength(flo
   }
 
   nsCOMPtr<nsISVGPoint> point =
     new DOMSVGPoint(path->ComputePointAtLength(
       clamped(distance, 0.f, path->ComputeLength())));
   return point.forget();
 }
 
+float
+SVGGeometryElement::GetPathLengthScale(PathLengthScaleForType aFor)
+{
+  MOZ_ASSERT(aFor == eForTextPath || aFor == eForStroking,
+             "Unknown enum");
+  if (mPathLength.IsExplicitlySet()) {
+    float authorsPathLengthEstimate = mPathLength.GetAnimValue();
+    if (authorsPathLengthEstimate > 0) {
+      RefPtr<Path> path = GetOrBuildPathForMeasuring();
+      if (!path) {
+        // The path is empty or invalid so its length must be zero and
+        // we know that 0 / authorsPathLengthEstimate = 0.
+        return 0.0;
+      }
+      if (aFor == eForTextPath) {
+        // For textPath, a transform on the referenced path affects the
+        // textPath layout, so when calculating the actual path length
+        // we need to take that into account.
+        gfxMatrix matrix = PrependLocalTransformsTo(gfxMatrix());
+        if (!matrix.IsIdentity()) {
+          RefPtr<PathBuilder> builder =
+            path->TransformedCopyToBuilder(ToMatrix(matrix));
+          path = builder->Finish();
+        }
+      }
+      return path->ComputeLength() / authorsPathLengthEstimate;
+    }
+  }
+  return 1.0;
+}
+
 already_AddRefed<SVGAnimatedNumber>
 SVGGeometryElement::PathLength()
 {
   return mPathLength.ToDOMAnimatedNumber(this);
 }
diff --git a/dom/svg/SVGGeometryElement.h b/dom/svg/SVGGeometryElement.h
--- a/dom/svg/SVGGeometryElement.h
+++ b/dom/svg/SVGGeometryElement.h
@@ -51,16 +51,17 @@ protected:
 public:
   explicit SVGGeometryElement(already_AddRefed<mozilla::dom::NodeInfo>& aNodeInfo);
 
   virtual nsresult AfterSetAttr(int32_t aNamespaceID, nsIAtom* aName,
                                 const nsAttrValue* aValue,
                                 const nsAttrValue* aOldValue,
                                 nsIPrincipal* aSubjectPrincipal,
                                 bool aNotify) override;
+  bool IsNodeOfType(uint32_t aFlags) const override;
 
   /**
    * Causes this element to discard any Path object that GetOrBuildPath may
    * have cached.
    */
   virtual void ClearAnyCachedPath() override final {
     mCachedPath = nullptr;
   }
@@ -202,16 +203,28 @@ public:
   virtual already_AddRefed<Path> GetOrBuildPathForMeasuring();
 
   /**
    * Returns the current computed value of the CSS property 'fill-rule' for
    * this element.
    */
   FillRule GetFillRule();
 
+  enum PathLengthScaleForType {
+    eForTextPath,
+    eForStroking
+  };
+
+  /**
+   * Gets the ratio of the actual element's length to the content author's
+   * estimated length (as provided by the element's 'pathLength' attribute).
+   * This is used to scale stroke dashing, and to scale offsets along a textPath.
+   */
+  float GetPathLengthScale(PathLengthScaleForType aFor);
+
   // WebIDL
   already_AddRefed<SVGAnimatedNumber> PathLength();
   float GetTotalLength();
   already_AddRefed<nsISVGPoint>
     GetPointAtLength(float distance, ErrorResult& rv);
 
 protected:
   // nsSVGElement method
diff --git a/dom/svg/SVGPathElement.cpp b/dom/svg/SVGPathElement.cpp
--- a/dom/svg/SVGPathElement.cpp
+++ b/dom/svg/SVGPathElement.cpp
@@ -280,47 +280,16 @@ SVGPathElement::IsMarkable()
 }
 
 void
 SVGPathElement::GetMarkPoints(nsTArray<nsSVGMark> *aMarks)
 {
   mD.GetAnimValue().GetMarkerPositioningData(aMarks);
 }
 
-float
-SVGPathElement::GetPathLengthScale(PathLengthScaleForType aFor)
-{
-  MOZ_ASSERT(aFor == eForTextPath || aFor == eForStroking,
-             "Unknown enum");
-  if (mPathLength.IsExplicitlySet()) {
-    float authorsPathLengthEstimate = mPathLength.GetAnimValue();
-    if (authorsPathLengthEstimate > 0) {
-      RefPtr<Path> path = GetOrBuildPathForMeasuring();
-      if (!path) {
-        // The path is empty or invalid so its length must be zero and
-        // we know that 0 / authorsPathLengthEstimate = 0.
-        return 0.0;
-      }
-      if (aFor == eForTextPath) {
-        // For textPath, a transform on the referenced path affects the
-        // textPath layout, so when calculating the actual path length
-        // we need to take that into account.
-        gfxMatrix matrix = PrependLocalTransformsTo(gfxMatrix());
-        if (!matrix.IsIdentity()) {
-          RefPtr<PathBuilder> builder =
-            path->TransformedCopyToBuilder(ToMatrix(matrix));
-          path = builder->Finish();
-        }
-      }
-      return path->ComputeLength() / authorsPathLengthEstimate;
-    }
-  }
-  return 1.0;
-}
-
 already_AddRefed<Path>
 SVGPathElement::BuildPath(PathBuilder* aBuilder)
 {
   // The Moz2D PathBuilder that our SVGPathData will be using only cares about
   // the fill rule. However, in order to fulfill the requirements of the SVG
   // spec regarding zero length sub-paths when square line caps are in use,
   // SVGPathData needs to know our stroke-linecap style and, if "square", then
   // also our stroke width. See the comment for
diff --git a/dom/svg/SVGPathElement.h b/dom/svg/SVGPathElement.h
--- a/dom/svg/SVGPathElement.h
+++ b/dom/svg/SVGPathElement.h
@@ -63,28 +63,16 @@ public:
   virtual SVGAnimatedPathSegList* GetAnimPathSegList() override {
     return &mD;
   }
 
   virtual nsIAtom* GetPathDataAttrName() const override {
     return nsGkAtoms::d;
   }
 
-  enum PathLengthScaleForType {
-    eForTextPath,
-    eForStroking
-  };
-
-  /**
-   * Gets the ratio of the actual path length to the content author's estimated
-   * length (as provided by the <path> element's 'pathLength' attribute). This
-   * is used to scale stroke dashing, and to scale offsets along a textPath.
-   */
-  float GetPathLengthScale(PathLengthScaleForType aFor);
-
   // WebIDL
   uint32_t GetPathSegAtLength(float distance);
   already_AddRefed<DOMSVGPathSegClosePath> CreateSVGPathSegClosePath();
   already_AddRefed<DOMSVGPathSegMovetoAbs> CreateSVGPathSegMovetoAbs(float x, float y);
   already_AddRefed<DOMSVGPathSegMovetoRel> CreateSVGPathSegMovetoRel(float x, float y);
   already_AddRefed<DOMSVGPathSegLinetoAbs> CreateSVGPathSegLinetoAbs(float x, float y);
   already_AddRefed<DOMSVGPathSegLinetoRel> CreateSVGPathSegLinetoRel(float x, float y);
   already_AddRefed<DOMSVGPathSegCurvetoCubicAbs>
diff --git a/layout/svg/SVGTextFrame.cpp b/layout/svg/SVGTextFrame.cpp
--- a/layout/svg/SVGTextFrame.cpp
+++ b/layout/svg/SVGTextFrame.cpp
@@ -37,17 +37,17 @@
 #include "nsTArray.h"
 #include "nsTextFrame.h"
 #include "nsTextNode.h"
 #include "SVGAnimatedNumberList.h"
 #include "SVGContentUtils.h"
 #include "SVGContextPaint.h"
 #include "SVGLengthList.h"
 #include "SVGNumberList.h"
-#include "SVGPathElement.h"
+#include "SVGGeometryElement.h"
 #include "SVGTextPathElement.h"
 #include "nsLayoutUtils.h"
 #include "nsFrameSelection.h"
 #include "nsStyleStructInlines.h"
 #include <algorithm>
 #include <cmath>
 #include <limits>
 
@@ -4992,18 +4992,18 @@ SVGTextFrame::AdjustPositionsForClusters
         mPositions[charIndex + 1].mStartOfChunk = true;
       }
     }
 
     it.Next();
   }
 }
 
-SVGPathElement*
-SVGTextFrame::GetTextPathPathElement(nsIFrame* aTextPathFrame)
+SVGGeometryElement*
+SVGTextFrame::GetTextPathGeometryElement(nsIFrame* aTextPathFrame)
 {
   nsSVGTextPathProperty *property =
     aTextPathFrame->GetProperty(SVGObserverUtils::HrefAsTextPathProperty());
 
   if (!property) {
     nsIContent* content = aTextPathFrame->GetContent();
     dom::SVGTextPathElement* tp = static_cast<dom::SVGTextPathElement*>(content);
     nsAutoString href;
@@ -5028,24 +5028,24 @@ SVGTextFrame::GetTextPathPathElement(nsI
       targetURI,
       aTextPathFrame,
       SVGObserverUtils::HrefAsTextPathProperty());
     if (!property)
       return nullptr;
   }
 
   Element* element = property->GetReferencedElement();
-  return (element && element->IsSVGElement(nsGkAtoms::path)) ?
-    static_cast<SVGPathElement*>(element) : nullptr;
+  return (element && element->IsNodeOfType(nsINode::eSHAPE)) ?
+    static_cast<SVGGeometryElement*>(element) : nullptr;
 }
 
 already_AddRefed<Path>
 SVGTextFrame::GetTextPath(nsIFrame* aTextPathFrame)
 {
-  SVGPathElement* element = GetTextPathPathElement(aTextPathFrame);
+  SVGGeometryElement* element = GetTextPathGeometryElement(aTextPathFrame);
   if (!element) {
     return nullptr;
   }
 
   RefPtr<Path> path = element->GetOrBuildPathForMeasuring();
   if (!path) {
     return nullptr;
   }
@@ -5058,21 +5058,21 @@ SVGTextFrame::GetTextPath(nsIFrame* aTex
   }
 
   return path.forget();
 }
 
 gfxFloat
 SVGTextFrame::GetOffsetScale(nsIFrame* aTextPathFrame)
 {
-  SVGPathElement* pathElement = GetTextPathPathElement(aTextPathFrame);
-  if (!pathElement)
+  SVGGeometryElement* element = GetTextPathGeometryElement(aTextPathFrame);
+  if (!element)
     return 1.0;
 
-  return pathElement->GetPathLengthScale(dom::SVGPathElement::eForTextPath);
+  return element->GetPathLengthScale(dom::SVGGeometryElement::eForTextPath);
 }
 
 gfxFloat
 SVGTextFrame::GetStartOffset(nsIFrame* aTextPathFrame)
 {
   dom::SVGTextPathElement *tp =
     static_cast<dom::SVGTextPathElement*>(aTextPathFrame->GetContent());
   nsSVGLength2 *length =
diff --git a/layout/svg/SVGTextFrame.h b/layout/svg/SVGTextFrame.h
--- a/layout/svg/SVGTextFrame.h
+++ b/layout/svg/SVGTextFrame.h
@@ -29,17 +29,17 @@ class CharIterator;
 class nsISVGPoint;
 class TextFrameIterator;
 class TextNodeCorrespondenceRecorder;
 struct TextRenderedRun;
 class TextRenderedRunIterator;
 
 namespace dom {
 class SVGIRect;
-class SVGPathElement;
+class SVGGeometryElement;
 } // namespace dom
 
 /**
  * Information about the positioning for a single character in an SVG <text>
  * element.
  *
  * During SVG text layout, we use infinity values to represent positions and
  * rotations that are not explicitly specified with x/y/rotate attributes.
@@ -533,18 +533,18 @@ private:
    * the text frames.
    *
    * @param aShouldPaintSVGGlyphs (out) Whether SVG glyphs in the text
    *   should be painted.
    */
   bool ShouldRenderAsPath(nsTextFrame* aFrame, bool& aShouldPaintSVGGlyphs);
 
   // Methods to get information for a <textPath> frame.
-  mozilla::dom::SVGPathElement*
-  GetTextPathPathElement(nsIFrame* aTextPathFrame);
+  mozilla::dom::SVGGeometryElement*
+  GetTextPathGeometryElement(nsIFrame* aTextPathFrame);
   already_AddRefed<Path> GetTextPath(nsIFrame* aTextPathFrame);
   gfxFloat GetOffsetScale(nsIFrame* aTextPathFrame);
   gfxFloat GetStartOffset(nsIFrame* aTextPathFrame);
 
   /**
    * The MutationObserver we have registered for the <text> element subtree.
    */
   RefPtr<MutationObserver> mMutationObserver;
