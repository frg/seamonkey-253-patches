# HG changeset patch
# User Aaron Klotz <aklotz@mozilla.com>
# Date 1530735258 21600
# Node ID 2ac106b8f990e17d4a300caa855b0e970d3e57e9
# Parent  e86e4fcf3588b78c5a9dfa55946aab6014ccb024
Bug 1473371: Create a separate function hook type for use with cross-process DLL interceptors; r=handyman

diff --git a/browser/app/winlauncher/DllBlocklistWin.cpp.1473371.later b/browser/app/winlauncher/DllBlocklistWin.cpp.1473371.later
new file mode 100644
--- /dev/null
+++ b/browser/app/winlauncher/DllBlocklistWin.cpp.1473371.later
@@ -0,0 +1,55 @@
+--- DllBlocklistWin.cpp
++++ DllBlocklistWin.cpp
+@@ -352,31 +352,23 @@ private:
+   DWORD   mPrevProt;
+ };
+ 
+ bool
+ InitializeDllBlocklistOOP(HANDLE aChildProcess)
+ {
+   mozilla::CrossProcessDllInterceptor intcpt(aChildProcess);
+   intcpt.Init(L"ntdll.dll");
+-  bool ok = stub_NtMapViewOfSection.SetDetour(intcpt, "NtMapViewOfSection",
++  bool ok = stub_NtMapViewOfSection.SetDetour(aChildProcess, intcpt,
++                                              "NtMapViewOfSection",
+                                               &patched_NtMapViewOfSection);
+   if (!ok) {
+     return false;
+   }
+ 
+-  // Set the child process's copy of stub_NtMapViewOfSection
+-  SIZE_T bytesWritten;
+-  ok = !!::WriteProcessMemory(aChildProcess, &stub_NtMapViewOfSection,
+-                              &stub_NtMapViewOfSection,
+-                              sizeof(stub_NtMapViewOfSection), &bytesWritten);
+-  if (!ok) {
+-    return false;
+-  }
+-
+   // Because aChildProcess has just been created in a suspended state, its
+   // dynamic linker has not yet been initialized, thus its executable has
+   // not yet been linked with ntdll.dll. If the blocklist hook intercepts a
+   // library load prior to the link, the hook will be unable to invoke any
+   // ntdll.dll functions.
+   //
+   // We know that the executable for our *current* process's binary is already
+   // linked into ntdll, so we obtain the IAT from our own executable and graft
+@@ -402,16 +394,18 @@ InitializeDllBlocklistOOP(HANDLE aChildP
+   // Find the length by iterating through the table until we find a null entry
+   PIMAGE_THUNK_DATA curIatThunk = firstIatThunk;
+   while (mozilla::nt::PEHeaders::IsValid(curIatThunk)) {
+     ++curIatThunk;
+   }
+ 
+   ptrdiff_t iatLength = (curIatThunk - firstIatThunk) * sizeof(IMAGE_THUNK_DATA);
+ 
++  SIZE_T bytesWritten;
++
+   { // Scope for prot
+     AutoVirtualProtect prot(firstIatThunk, iatLength, PAGE_READWRITE,
+                             aChildProcess);
+     if (!prot) {
+       return false;
+     }
+ 
+     ok = !!::WriteProcessMemory(aChildProcess, firstIatThunk, firstIatThunk,
diff --git a/mozglue/misc/nsWindowsDllInterceptor.h b/mozglue/misc/nsWindowsDllInterceptor.h
--- a/mozglue/misc/nsWindowsDllInterceptor.h
+++ b/mozglue/misc/nsWindowsDllInterceptor.h
@@ -79,42 +79,42 @@
  *
  * Note that this is not thread-safe.  Sad day.
  *
  */
 
 namespace mozilla {
 namespace interceptor {
 
+template <typename T>
+struct OriginalFunctionPtrTraits;
+
+template <typename R, typename... Args>
+struct OriginalFunctionPtrTraits<R (*)(Args...)>
+{
+  using ReturnType = R;
+};
+
+#if defined(_M_IX86)
+template <typename R, typename... Args>
+struct OriginalFunctionPtrTraits<R (__stdcall*)(Args...)>
+{
+  using ReturnType = R;
+};
+
+template <typename R, typename... Args>
+struct OriginalFunctionPtrTraits<R (__fastcall*)(Args...)>
+{
+  using ReturnType = R;
+};
+#endif // defined(_M_IX86)
+
 template <typename InterceptorT, typename FuncPtrT>
 class FuncHook final
 {
-  template <typename T>
-  struct OriginalFunctionPtrTraits;
-
-  template <typename R, typename... Args>
-  struct OriginalFunctionPtrTraits<R (*)(Args...)>
-  {
-    using ReturnType = R;
-  };
-
-#if defined(_M_IX86)
-  template <typename R, typename... Args>
-  struct OriginalFunctionPtrTraits<R (__stdcall*)(Args...)>
-  {
-    using ReturnType = R;
-  };
-
-  template <typename R, typename... Args>
-  struct OriginalFunctionPtrTraits<R (__fastcall*)(Args...)>
-  {
-    using ReturnType = R;
-  };
-#endif // defined(_M_IX86)
-
 public:
   using ThisType = FuncHook<InterceptorT, FuncPtrT>;
   using ReturnType = typename OriginalFunctionPtrTraits<FuncPtrT>::ReturnType;
 
   constexpr FuncHook()
     : mOrigFunc(nullptr)
     , mInitOnce(INIT_ONCE_STATIC_INIT)
   {
@@ -216,25 +216,109 @@ private:
     return TRUE;
   }
 
 private:
   FuncPtrT  mOrigFunc;
   INIT_ONCE mInitOnce;
 };
 
+template <typename InterceptorT, typename FuncPtrT>
+class MOZ_ONLY_USED_TO_AVOID_STATIC_CONSTRUCTORS FuncHookCrossProcess final
+{
+public:
+  using ThisType = FuncHookCrossProcess<InterceptorT, FuncPtrT>;
+  using ReturnType = typename OriginalFunctionPtrTraits<FuncPtrT>::ReturnType;
+
+#if defined(DEBUG)
+  FuncHookCrossProcess() {}
+#endif // defined(DEBUG)
+
+  bool Set(HANDLE aProcess, InterceptorT& aInterceptor, const char* aName,
+           FuncPtrT aHookDest)
+  {
+    if (!aInterceptor.AddHook(aName, reinterpret_cast<intptr_t>(aHookDest),
+                              reinterpret_cast<void**>(&mOrigFunc))) {
+      return false;
+    }
+
+    return CopyStubToChildProcess(aProcess);
+  }
+
+  bool SetDetour(HANDLE aProcess, InterceptorT& aInterceptor, const char* aName,
+                 FuncPtrT aHookDest)
+  {
+    if (!aInterceptor.AddDetour(aName, reinterpret_cast<intptr_t>(aHookDest),
+                                reinterpret_cast<void**>(&mOrigFunc))) {
+      return false;
+    }
+
+    return CopyStubToChildProcess(aProcess);
+  }
+
+  explicit operator bool() const
+  {
+    return !!mOrigFunc;
+  }
+
+  /**
+   * NB: This operator is only meaningful when invoked in the target process!
+   */
+  template <typename... ArgsType>
+  ReturnType operator()(ArgsType... aArgs) const
+  {
+    return mOrigFunc(std::forward<ArgsType>(aArgs)...);
+  }
+
+#if defined(DEBUG)
+  FuncHookCrossProcess(const FuncHookCrossProcess&) = delete;
+  FuncHookCrossProcess(FuncHookCrossProcess&&) = delete;
+  FuncHookCrossProcess& operator=(const FuncHookCrossProcess&) = delete;
+  FuncHookCrossProcess& operator=(FuncHookCrossProcess&& aOther) = delete;
+#endif // defined(DEBUG)
+
+private:
+  bool CopyStubToChildProcess(HANDLE aProcess)
+  {
+    SIZE_T bytesWritten;
+    return !!::WriteProcessMemory(aProcess, &mOrigFunc, &mOrigFunc,
+                                  sizeof(mOrigFunc), &bytesWritten);
+  }
+
+private:
+  FuncPtrT  mOrigFunc;
+};
+
 enum
 {
   kDefaultTrampolineSize = 128
 };
 
+template <typename MMPolicyT, typename InterceptorT>
+struct TypeResolver;
+
+template <typename InterceptorT>
+struct TypeResolver<mozilla::interceptor::MMPolicyInProcess, InterceptorT>
+{
+  template <typename FuncPtrT>
+  using FuncHookType = FuncHook<InterceptorT, FuncPtrT>;
+};
+
+template <typename InterceptorT>
+struct TypeResolver<mozilla::interceptor::MMPolicyOutOfProcess, InterceptorT>
+{
+  template <typename FuncPtrT>
+  using FuncHookType = FuncHookCrossProcess<InterceptorT, FuncPtrT>;
+};
+
 template <typename VMPolicy =
             mozilla::interceptor::VMSharingPolicyShared<
               mozilla::interceptor::MMPolicyInProcess, kDefaultTrampolineSize>>
-class WindowsDllInterceptor final
+class WindowsDllInterceptor final : public TypeResolver<typename VMPolicy::MMPolicyT,
+                                                        WindowsDllInterceptor<VMPolicy>>
 {
   typedef WindowsDllInterceptor<VMPolicy> ThisType;
 
   interceptor::WindowsDllDetourPatcher<VMPolicy> mDetourPatcher;
 #if defined(_M_IX86)
   interceptor::WindowsDllNopSpacePatcher<typename VMPolicy::MMPolicyT> mNopSpacePatcher;
 #endif // defined(_M_IX86)
 
@@ -367,23 +451,22 @@ private:
 
     if (!mDetourPatcher.Initialized()) {
       mDetourPatcher.Init(mNHooks);
     }
 
     return mDetourPatcher.AddHook(aProc, aHookDest, aOrigFunc);
   }
 
-public:
-  template <typename FuncPtrT>
-  using FuncHookType = FuncHook<ThisType, FuncPtrT>;
-
 private:
   template <typename InterceptorT, typename FuncPtrT>
   friend class FuncHook;
+
+  template <typename InterceptorT, typename FuncPtrT>
+  friend class FuncHookCrossProcess;
 };
 
 } // namespace interceptor
 
 using WindowsDllInterceptor = interceptor::WindowsDllInterceptor<>;
 
 using CrossProcessDllInterceptor = interceptor::WindowsDllInterceptor<
   mozilla::interceptor::VMSharingPolicyUnique<
diff --git a/mozglue/tests/interceptor/TestDllInterceptorCrossProcess.cpp b/mozglue/tests/interceptor/TestDllInterceptorCrossProcess.cpp
--- a/mozglue/tests/interceptor/TestDllInterceptorCrossProcess.cpp
+++ b/mozglue/tests/interceptor/TestDllInterceptorCrossProcess.cpp
@@ -68,32 +68,24 @@ int ParentMain()
     printf("TEST-UNEXPECTED-FAIL | DllInterceptorCrossProcess | Failed to assign child process to job\n");
     ::TerminateProcess(childProcess.get(), 1);
     return 1;
   }
 
   mozilla::CrossProcessDllInterceptor intcpt(childProcess.get());
   intcpt.Init("TestDllInterceptorCrossProcess.exe");
 
-  if (!gOrigReturnResult.Set(intcpt, "ReturnResult", &ReturnResultHook)) {
+  if (!gOrigReturnResult.Set(childProcess.get(), intcpt, "ReturnResult",
+                             &ReturnResultHook)) {
     printf("TEST-UNEXPECTED-FAIL | DllInterceptorCrossProcess | Failed to add hook\n");
     return 1;
   }
 
   printf("TEST-PASS | DllInterceptorCrossProcess | Hook added\n");
 
-  // Let's save the original hook
-  SIZE_T bytesWritten;
-  if (!::WriteProcessMemory(childProcess.get(), &gOrigReturnResult,
-                            &gOrigReturnResult, sizeof(gOrigReturnResult),
-                            &bytesWritten)) {
-    printf("TEST-UNEXPECTED-FAIL | DllInterceptorCrossProcess | Failed to write original function pointer\n");
-    return 1;
-  }
-
   if (::ResumeThread(childMainThread.get()) == static_cast<DWORD>(-1)) {
     printf("TEST-UNEXPECTED-FAIL | DllInterceptorCrossProcess | Failed to resume child thread\n");
     return 1;
   }
 
   BOOL remoteDebugging;
   bool debugging = ::IsDebuggerPresent() ||
                    (::CheckRemoteDebuggerPresent(childProcess.get(),
