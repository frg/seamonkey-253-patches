# HG changeset patch
# User Gabriele Svelto <gsvelto@mozilla.com>
# Date 1587629377 0
# Node ID 42192ff2aa4c4dac0144b31ea39ea9aa8a3c728d
# Parent  596680c66da388d4d01248a4b5e97483f90ac1f2
Bug 1631382 - Update the Gentoo |mach bootstrap| implementation r=glandium

This fixes multiple issues:
* It switches mobile builds from the Oracle JDK to OpenJDK and removes all the
  logic needed to download the former
* It only installs the build dependencies required for building Firefox and
  stores them in the world file

Differential Revision: https://phabricator.services.mozilla.com/D71539

diff --git a/python/mozboot/mozboot/gentoo.py b/python/mozboot/mozboot/gentoo.py
--- a/python/mozboot/mozboot/gentoo.py
+++ b/python/mozboot/mozboot/gentoo.py
@@ -10,121 +10,63 @@ from mozboot.linux_common import (
     LucetcInstall,
     NasmInstall,
     NodeInstall,
     SccacheInstall,
     StyloInstall,
     WasiSysrootInstall,
 )
 
-try:
-    from urllib2 import urlopen
-except ImportError:
-    from urllib.request import urlopen
-
-import re
-import subprocess
-
 
 class GentooBootstrapper(
         NasmInstall, NodeInstall, StyloInstall, ClangStaticAnalysisInstall,
         SccacheInstall, LucetcInstall, WasiSysrootInstall, BaseBootstrapper):
 
     def __init__(self, version, dist_id, **kwargs):
         BaseBootstrapper.__init__(self, **kwargs)
 
         self.version = version
         self.dist_id = dist_id
 
     def install_system_packages(self):
-        self.run_as_root(['emerge', '--noreplace', '--quiet', 'nodejs'])
+        self.ensure_system_packages()
 
     def install_browser_packages(self):
         self.ensure_browser_packages()
 
     def install_browser_artifact_mode_packages(self):
         self.ensure_browser_packages(artifact_mode=True)
 
     def install_mobile_android_packages(self):
         self.ensure_mobile_android_packages(artifact_mode=False)
 
     def install_mobile_android_artifact_mode_packages(self):
         self.ensure_mobile_android_packages(artifact_mode=True)
 
+    def ensure_system_packages(self):
+        self.run_as_root(['emerge', '--noreplace', '--quiet',
+                          'app-arch/zip',
+                          'sys-devel/autoconf:2.1'
+                          ])
+
     def ensure_browser_packages(self, artifact_mode=False):
         # TODO: Figure out what not to install for artifact mode
-        self.run_as_root(['emerge', '--onlydeps', '--quiet', 'firefox'])
-        self.run_as_root(['emerge', '--noreplace', '--quiet', 'gtk+'])
-
-    @staticmethod
-    def _get_distdir():
-        # Obtain the path held in the DISTDIR portage variable
-        output = subprocess.check_output(
-            ['emerge', '--info'], universal_newlines=True)
-        match = re.search('^DISTDIR="(?P<distdir>.*)"$', output, re.MULTILINE)
-        return match.group('distdir')
-
-    @staticmethod
-    def _get_jdk_filename(emerge_output):
-        match = re.search(r'^ \* *(?P<tarball>jdk-.*-linux-.*.tar.gz)$',
-                          emerge_output, re.MULTILINE)
-
-        return match.group('tarball')
-
-    @staticmethod
-    def _get_jdk_page_urls(emerge_output):
-        urls = re.findall(r'^ \* *(https?://.*\.html)$', emerge_output,
-                          re.MULTILINE)
-        return [re.sub('^http://', 'https://', url) for url in urls]
-
-    @staticmethod
-    def _get_jdk_url(filename, urls):
-        for url in urls:
-            contents = urlopen(url).read()
-            match = re.search('.*(?P<url>https?://.*' + filename + ')',
-                              contents, re.MULTILINE)
-            if match:
-                url = match.group('url')
-                return re.sub('^http://', 'https://', url)
-
-        raise Exception("Could not find the JDK tarball URL")
-
-    def _fetch_jdk_tarball(self, filename, url):
-        distdir = self._get_distdir()
-        cookie = 'Cookie: oraclelicense=accept-securebackup-cookie'
-        self.run_as_root(['wget', '--no-verbose', '--show-progress', '-c', '-O',
-                          distdir + '/' + filename, '--header', cookie, url])
+        self.run_as_root(['emerge',
+                          '--oneshot', '--noreplace', '--quiet', '--newuse',
+                          'dev-lang/yasm',
+                          'dev-libs/dbus-glib',
+                          'media-sound/pulseaudio',
+                          'x11-libs/gtk+:2',
+                          'x11-libs/gtk+:3',
+                          'x11-libs/libXt'
+                          ])
 
     def ensure_mobile_android_packages(self, artifact_mode=False):
-        # For downloading the Oracle JDK, Android SDK and NDK.
-        self.run_as_root(['emerge', '--noreplace', '--quiet', 'wget'])
-
-        # Find the JDK file name and URL(s)
-        try:
-            output = self.check_output(['emerge', '--pretend', '--fetchonly',
-                                        'oracle-jdk-bin'],
-                                       env=None,
-                                       stderr=subprocess.STDOUT,
-                                       universal_newlines=True)
-        except subprocess.CalledProcessError as e:
-            output = e.output
-
-        jdk_filename = self._get_jdk_filename(output)
-        jdk_page_urls = self._get_jdk_page_urls(output)
-        jdk_url = self._get_jdk_url(jdk_filename, jdk_page_urls)
-
-        # Fetch the Oracle JDK since portage can't fetch it on its own
-        self._fetch_jdk_tarball(jdk_filename, jdk_url)
-
-        # Install the Oracle JDK. We explicitly prompt the user to accept the
-        # changes because this command might need to modify the portage
-        # configuration files and doing so without user supervision is dangerous
         self.run_as_root(['emerge', '--noreplace', '--quiet',
-                          '--autounmask-continue', '--ask',
-                          'dev-java/oracle-jdk-bin'])
+                          'dev-java/openjdk-bin'])
 
         self.ensure_java()
         from mozboot import android
         android.ensure_android('linux', artifact_mode=artifact_mode,
                                no_interactive=self.no_interactive)
 
     def suggest_mobile_android_mozconfig(self, artifact_mode=False):
         from mozboot import android
