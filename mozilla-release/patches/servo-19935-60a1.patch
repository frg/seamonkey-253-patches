# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1517678663 18000
# Node ID ccd78a8068090f51db6b2c48ad69cdb114ddfcde
# Parent  1f9dd8eb95d1b7e39863accde43a7d05cd0ad2ac
servo: Merge #19935 - style: Cleanup StyleBuilder (from emilio:cleanup-style-builder); r=nox

style: Cleanup StyleBuilder.

This is in preparation of a cascade optimization for custom properties.

This fixes various fishiness around our StyleBuilder stuff. In particular,
StyleBuilder::for_derived_style (renamed to for_animation) is only used to
compute specified values, and thus doesn't need to know about rules, visited
style, or other things like that.

The flag propagation that was done in StyleAdjuster is now done in StyleBuilder,
since we know beforehand which ones are always inherited, and it simplified the
callers and the StyleAdjuster code. It also fixed some fishiness wrt which flags
were propagated to anon boxes and text.

Source-Repo: https://github.com/servo/servo
Source-Revision: a0d9d3633b99c01868c98d2a5e64bf311f532d58

diff --git a/servo/components/style/matching.rs b/servo/components/style/matching.rs
--- a/servo/components/style/matching.rs
+++ b/servo/components/style/matching.rs
@@ -331,17 +331,17 @@ trait PrivateMatchMethods: TElement {
             self.compute_style_difference(old_values, new_values, pseudo);
 
         *damage |= difference.damage;
 
         debug!(" > style difference: {:?}", difference);
 
         // We need to cascade the children in order to ensure the correct
         // propagation of inherited computed value flags.
-        if old_values.flags.inherited() != new_values.flags.inherited() {
+        if old_values.flags.maybe_inherited() != new_values.flags.maybe_inherited() {
             debug!(" > flags changed: {:?} != {:?}", old_values.flags, new_values.flags);
             return ChildCascadeRequirement::MustCascadeChildren;
         }
 
         match difference.change {
             StyleChange::Unchanged => {
                 return ChildCascadeRequirement::CanSkipCascade
             },
diff --git a/servo/components/style/properties/computed_value_flags.rs b/servo/components/style/properties/computed_value_flags.rs
--- a/servo/components/style/properties/computed_value_flags.rs
+++ b/servo/components/style/properties/computed_value_flags.rs
@@ -66,16 +66,40 @@ bitflags! {
         /// Whether the style or any of the ancestors has a multicol style.
         ///
         /// Only used in Servo.
         const CAN_BE_FRAGMENTED = 1 << 10;
     }
 }
 
 impl ComputedValueFlags {
-    /// Returns the flags that are inherited.
+    /// Flags that are unconditionally propagated to descendants.
+    #[inline]
+    fn inherited_flags() -> Self {
+        ComputedValueFlags::IS_STYLE_IF_VISITED |
+        ComputedValueFlags::IS_RELEVANT_LINK_VISITED |
+        ComputedValueFlags::CAN_BE_FRAGMENTED |
+        ComputedValueFlags::IS_IN_DISPLAY_NONE_SUBTREE |
+        ComputedValueFlags::IS_IN_PSEUDO_ELEMENT_SUBTREE |
+        ComputedValueFlags::HAS_TEXT_DECORATION_LINES
+    }
+
+    /// Flags that may be propagated to descendants.
+    #[inline]
+    fn maybe_inherited_flags() -> Self {
+        Self::inherited_flags() | ComputedValueFlags::SHOULD_SUPPRESS_LINEBREAK
+    }
+
+    /// Returns the flags that are always propagated to descendants.
+    ///
+    /// See StyleAdjuster::set_bits and StyleBuilder.
     #[inline]
     pub fn inherited(self) -> Self {
-        self & !(ComputedValueFlags::INHERITS_DISPLAY |
-                 ComputedValueFlags::INHERITS_CONTENT |
-                 ComputedValueFlags::INHERITS_RESET_STYLE)
+        self & Self::inherited_flags()
+    }
+
+    /// Flags that are conditionally propagated to descendants, just to handle
+    /// properly style invalidation.
+    #[inline]
+    pub fn maybe_inherited(self) -> Self {
+        self & Self::maybe_inherited_flags()
     }
 }
diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -247,27 +247,16 @@ impl ops::Deref for ComputedValues {
 
 impl ops::DerefMut for ComputedValues {
     fn deref_mut(&mut self) -> &mut ComputedValuesInner {
         &mut self.0.mSource
     }
 }
 
 impl ComputedValuesInner {
-    /// Clone the visited style.  Used for inheriting parent styles in
-    /// StyleBuilder::for_derived_style.
-    pub fn clone_visited_style(&self) -> Option<Arc<ComputedValues>> {
-        self.visited_style.as_ref().map(|x| x.clone_arc())
-    }
-
-    #[inline]
-    pub fn is_display_contents(&self) -> bool {
-        self.get_box().clone_display() == longhands::display::computed_value::T::Contents
-    }
-
     /// Returns true if the value of the `content` property would make a
     /// pseudo-element not rendered.
     #[inline]
     pub fn ineffective_content_property(&self) -> bool {
         self.get_counters().ineffective_content_property()
     }
 
     % for style_struct in data.style_structs:
diff --git a/servo/components/style/properties/properties.mako.rs b/servo/components/style/properties/properties.mako.rs
--- a/servo/components/style/properties/properties.mako.rs
+++ b/servo/components/style/properties/properties.mako.rs
@@ -2215,16 +2215,21 @@ pub struct ComputedValues {
     /// In Gecko the outer ComputedValues is actually a style context,
     /// whereas ComputedValuesInner is the core set of computed values.
     ///
     /// We maintain this distinction in servo to reduce the amount of special casing.
     inner: ComputedValuesInner,
 }
 
 impl ComputedValues {
+    /// Returns whether this style's display value is equal to contents.
+    pub fn is_display_contents(&self) -> bool {
+        self.get_box().clone_display().is_contents()
+    }
+
     /// Whether we're a visited style.
     pub fn is_style_if_visited(&self) -> bool {
         self.flags.contains(ComputedValueFlags::IS_STYLE_IF_VISITED)
     }
 
     /// Gets a reference to the rule node. Panic if no rule node exists.
     pub fn rules(&self) -> &StrongRuleNode {
         self.rules.as_ref().unwrap()
@@ -2334,27 +2339,16 @@ impl ComputedValuesInner {
     pub fn rules(&self) -> &StrongRuleNode {
         self.rules.as_ref().unwrap()
     }
 
     /// Whether this style has a -moz-binding value. This is always false for
     /// Servo for obvious reasons.
     pub fn has_moz_binding(&self) -> bool { false }
 
-    /// Clone the visited style.  Used for inheriting parent styles in
-    /// StyleBuilder::for_derived_style.
-    pub fn clone_visited_style(&self) -> Option<Arc<ComputedValues>> {
-        self.visited_style.clone()
-    }
-
-    /// Returns whether this style's display value is equal to contents.
-    ///
-    /// Since this isn't supported in Servo, this is always false for Servo.
-    pub fn is_display_contents(&self) -> bool { false }
-
     #[inline]
     /// Returns whether the "content" property for the given style is completely
     /// ineffective, and would yield an empty `::before` or `::after`
     /// pseudo-element.
     pub fn ineffective_content_property(&self) -> bool {
         use properties::longhands::content::computed_value::T;
         match self.get_counters().content {
             T::Normal | T::None => true,
@@ -2727,18 +2721,16 @@ impl<'a> StyleBuilder<'a> {
     fn new(
         device: &'a Device,
         parent_style: Option<<&'a ComputedValues>,
         parent_style_ignoring_first_line: Option<<&'a ComputedValues>,
         pseudo: Option<<&'a PseudoElement>,
         cascade_flags: CascadeFlags,
         rules: Option<StrongRuleNode>,
         custom_properties: Option<Arc<::custom_properties::CustomPropertiesMap>>,
-        writing_mode: WritingMode,
-        mut flags: ComputedValueFlags,
         visited_style: Option<Arc<ComputedValues>>,
     ) -> Self {
         debug_assert_eq!(parent_style.is_some(), parent_style_ignoring_first_line.is_some());
         #[cfg(feature = "gecko")]
         debug_assert!(parent_style.is_none() ||
                       ::std::ptr::eq(parent_style.unwrap(),
                                      parent_style_ignoring_first_line.unwrap()) ||
                       parent_style.unwrap().pseudo() == Some(PseudoElement::FirstLine));
@@ -2750,31 +2742,32 @@ impl<'a> StyleBuilder<'a> {
         // backgrounds, for example.  This code doesn't attempt to make it play
         // nice with inherited_style_ignoring_first_line.
         let reset_style = if cascade_flags.contains(CascadeFlags::INHERIT_ALL) {
             inherited_style
         } else {
             reset_style
         };
 
+        let mut flags = inherited_style.flags.inherited();
         if cascade_flags.contains(CascadeFlags::VISITED_DEPENDENT_ONLY) {
             flags.insert(ComputedValueFlags::IS_STYLE_IF_VISITED);
         }
 
         StyleBuilder {
             device,
             parent_style,
             inherited_style,
             inherited_style_ignoring_first_line,
             reset_style,
             pseudo,
             rules,
             modified_reset: false,
             custom_properties,
-            writing_mode,
+            writing_mode: inherited_style.writing_mode,
             flags,
             visited_style,
             % for style_struct in data.active_style_structs():
             % if style_struct.inherited:
             ${style_struct.ident}: StyleStructRef::Borrowed(inherited_style.${style_struct.name_lower}_arc()),
             % else:
             ${style_struct.ident}: StyleStructRef::Borrowed(reset_style.${style_struct.name_lower}_arc()),
             % endif
@@ -2782,43 +2775,46 @@ impl<'a> StyleBuilder<'a> {
         }
     }
 
     /// Whether we're a visited style.
     pub fn is_style_if_visited(&self) -> bool {
         self.flags.contains(ComputedValueFlags::IS_STYLE_IF_VISITED)
     }
 
-    /// Creates a StyleBuilder holding only references to the structs of `s`, in
-    /// order to create a derived style.
-    pub fn for_derived_style(
+    /// NOTE(emilio): This is done so we can compute relative units with respect
+    /// to the parent style, but all the early properties / writing-mode / etc
+    /// are already set to the right ones on the kid.
+    ///
+    /// Do _not_ actually call this to construct a style, this should mostly be
+    /// used for animations.
+    pub fn for_animation(
         device: &'a Device,
         style_to_derive_from: &'a ComputedValues,
         parent_style: Option<<&'a ComputedValues>,
-        pseudo: Option<<&'a PseudoElement>,
     ) -> Self {
         let reset_style = device.default_computed_values();
         let inherited_style = parent_style.unwrap_or(reset_style);
         #[cfg(feature = "gecko")]
         debug_assert!(parent_style.is_none() ||
                       parent_style.unwrap().pseudo() != Some(PseudoElement::FirstLine));
         StyleBuilder {
             device,
             parent_style,
             inherited_style,
             // None of our callers pass in ::first-line parent styles.
             inherited_style_ignoring_first_line: inherited_style,
             reset_style,
-            pseudo,
+            pseudo: None,
             modified_reset: false,
-            rules: None, // FIXME(emilio): Dubious...
+            rules: None,
             custom_properties: style_to_derive_from.custom_properties().cloned(),
             writing_mode: style_to_derive_from.writing_mode,
             flags: style_to_derive_from.flags,
-            visited_style: style_to_derive_from.clone_visited_style(),
+            visited_style: None,
             % for style_struct in data.active_style_structs():
             ${style_struct.ident}: StyleStructRef::Borrowed(
                 style_to_derive_from.${style_struct.name_lower}_arc()
             ),
             % endfor
         }
     }
 
@@ -2911,44 +2907,41 @@ impl<'a> StyleBuilder<'a> {
     % endif
     % endif
     % endfor
 
     /// Inherits style from the parent element, accounting for the default
     /// computed values that need to be provided as well.
     pub fn for_inheritance(
         device: &'a Device,
-        parent: &'a ComputedValues,
+        parent: Option<<&'a ComputedValues>,
         pseudo: Option<<&'a PseudoElement>,
     ) -> Self {
         // Rebuild the visited style from the parent, ensuring that it will also
         // not have rules.  This matches the unvisited style that will be
         // produced by this builder.  This assumes that the caller doesn't need
         // to adjust or process visited style, so we can just build visited
         // style here for simplicity.
-        let visited_style = parent.visited_style().map(|style| {
-            Self::for_inheritance(
-                device,
-                style,
-                pseudo,
-            ).build()
+        let visited_style = parent.and_then(|parent| {
+            parent.visited_style().map(|style| {
+                Self::for_inheritance(
+                    device,
+                    Some(style),
+                    pseudo,
+                ).build()
+            })
         });
-        // FIXME(emilio): This Some(parent) here is inconsistent with what we
-        // usually do if `parent` is the default computed values, but that's
-        // fine, and we want to eventually get rid of it.
         Self::new(
             device,
-            Some(parent),
-            Some(parent),
+            parent,
+            parent,
             pseudo,
             CascadeFlags::empty(),
             /* rules = */ None,
-            parent.custom_properties().cloned(),
-            parent.writing_mode,
-            parent.flags.inherited(),
+            parent.and_then(|p| p.custom_properties().cloned()),
             visited_style,
         )
     }
 
     /// Returns whether we have a visited style.
     pub fn has_visited_style(&self) -> bool {
         self.visited_style.is_some()
     }
@@ -3071,19 +3064,20 @@ impl<'a> StyleBuilder<'a> {
     /// ::first-line case some of the inherited information needs to come from
     /// one ComputedValues instance and some from a different one.
 
     /// Inherited writing-mode.
     pub fn inherited_writing_mode(&self) -> &WritingMode {
         &self.inherited_style.writing_mode
     }
 
-    /// Inherited style flags.
-    pub fn inherited_flags(&self) -> &ComputedValueFlags {
-        &self.inherited_style.flags
+    /// The computed value flags of our parent.
+    #[inline]
+    pub fn get_parent_flags(&self) -> ComputedValueFlags {
+        self.inherited_style.flags
     }
 
     /// And access to inherited style structs.
     % for style_struct in data.active_style_structs():
         /// Gets our inherited `${style_struct.name}`.  We don't name these
         /// accessors `inherited_${style_struct.name_lower}` because we already
         /// have things like "box" vs "inherited_box" as struct names.  Do the
         /// next-best thing and call them `parent_${style_struct.name_lower}`
@@ -3325,18 +3319,16 @@ where
         builder: StyleBuilder::new(
             device,
             parent_style,
             parent_style_ignoring_first_line,
             pseudo,
             flags,
             Some(rules.clone()),
             custom_properties,
-            WritingMode::empty(),
-            ComputedValueFlags::empty(),
             visited_style,
         ),
         cached_system_font: None,
         in_media_query: false,
         for_smil_animation: false,
         for_non_inherited_property: None,
         font_metrics_provider,
         quirks_mode,
diff --git a/servo/components/style/style_adjuster.rs b/servo/components/style/style_adjuster.rs
--- a/servo/components/style/style_adjuster.rs
+++ b/servo/components/style/style_adjuster.rs
@@ -3,16 +3,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! A struct to encapsulate all the style fixups and flags propagations
 //! a computed style needs in order for it to adhere to the CSS spec.
 
 use app_units::Au;
 use dom::TElement;
 use properties::{self, CascadeFlags, ComputedValues, StyleBuilder};
+use properties::computed_value_flags::ComputedValueFlags;
 use properties::longhands::display::computed_value::T as Display;
 use properties::longhands::float::computed_value::T as Float;
 use properties::longhands::overflow_x::computed_value::T as Overflow;
 use properties::longhands::position::computed_value::T as Position;
 
 /// A struct that implements all the adjustment methods.
 ///
 /// NOTE(emilio): If new adjustments are introduced that depend on reset
@@ -106,33 +107,33 @@ impl<'a, 'b: 'a> StyleAdjuster<'a, 'b> {
                 blockified_display,
                 is_item_or_root,
             );
         }
     }
 
     /// Compute a few common flags for both text and element's style.
     pub fn set_bits(&mut self) {
-        use properties::computed_value_flags::ComputedValueFlags;
+        let display = self.style.get_box().clone_display();
 
-        if self.style.inherited_flags().contains(ComputedValueFlags::IS_IN_DISPLAY_NONE_SUBTREE) ||
-            self.style.get_box().clone_display() == Display::None {
+        if !display.is_contents() && !self.style.get_text().clone_text_decoration_line().is_empty() {
+            self.style.flags.insert(ComputedValueFlags::HAS_TEXT_DECORATION_LINES);
+        }
+
+        if display == Display::None {
             self.style.flags.insert(ComputedValueFlags::IS_IN_DISPLAY_NONE_SUBTREE);
         }
 
-        if self.style.inherited_flags().contains(ComputedValueFlags::IS_IN_PSEUDO_ELEMENT_SUBTREE) ||
-            self.style.is_pseudo_element() {
+        if self.style.is_pseudo_element() {
             self.style.flags.insert(ComputedValueFlags::IS_IN_PSEUDO_ELEMENT_SUBTREE);
         }
 
         #[cfg(feature = "servo")]
         {
-            if self.style.inherited_flags().contains(ComputedValueFlags::CAN_BE_FRAGMENTED) ||
-                self.style.get_parent_column().is_multicol()
-            {
+            if self.style.get_parent_column().is_multicol() {
                 self.style.flags.insert(ComputedValueFlags::CAN_BE_FRAGMENTED);
             }
         }
     }
 
     /// Adjust the style for text style.
     ///
     /// The adjustments here are a subset of the adjustments generally, because
@@ -155,39 +156,41 @@ impl<'a, 'b: 'a> StyleAdjuster<'a, 'b> {
     /// of display: contents.
     ///
     /// FIXME(emilio): How does this play with logical properties? Doesn't
     /// mutating writing-mode change the potential physical sides chosen?
     #[cfg(feature = "gecko")]
     fn adjust_for_text_combine_upright(&mut self) {
         use computed_values::text_combine_upright::T as TextCombineUpright;
         use computed_values::writing_mode::T as WritingMode;
-        use properties::computed_value_flags::ComputedValueFlags;
 
         let writing_mode =
             self.style.get_inheritedbox().clone_writing_mode();
         let text_combine_upright =
             self.style.get_inheritedtext().clone_text_combine_upright();
 
         if writing_mode != WritingMode::HorizontalTb &&
            text_combine_upright == TextCombineUpright::All {
             self.style.flags.insert(ComputedValueFlags::IS_TEXT_COMBINED);
             self.style.mutate_inheritedbox().set_writing_mode(WritingMode::HorizontalTb);
         }
     }
 
-    /// Applies the line break suppression flag to text if it is in any ruby
-    /// box. This is necessary because its parent may not itself have the flag
-    /// set (e.g. ruby or ruby containers), thus we may not inherit the flag
-    /// from them.
+    /// Unconditionally propagates the line break suppression flag to text, and
+    /// additionally it applies it if it is in any ruby box.
+    ///
+    /// This is necessary because its parent may not itself have the flag set
+    /// (e.g. ruby or ruby containers), thus we may not inherit the flag from
+    /// them.
     #[cfg(feature = "gecko")]
     fn adjust_for_text_in_ruby(&mut self) {
-        use properties::computed_value_flags::ComputedValueFlags;
         let parent_display = self.style.get_parent_box().clone_display();
-        if parent_display.is_ruby_type() {
+        if parent_display.is_ruby_type() ||
+           self.style.get_parent_flags().contains(ComputedValueFlags::SHOULD_SUPPRESS_LINEBREAK)
+        {
             self.style.flags.insert(ComputedValueFlags::SHOULD_SUPPRESS_LINEBREAK);
         }
     }
 
     /// <https://drafts.csswg.org/css-writing-modes-3/#block-flow:>
     ///
     ///    If a box has a different writing-mode value than its containing
     ///    block:
@@ -421,28 +424,16 @@ impl<'a, 'b: 'a> StyleAdjuster<'a, 'b> {
             TextAlign::MozCenter |
             TextAlign::MozRight => {},
             _ => return,
         }
 
         self.style.mutate_inheritedtext().set_text_align(TextAlign::Start)
     }
 
-    /// Set the HAS_TEXT_DECORATION_LINES flag based on parent style.
-    fn adjust_for_text_decoration_lines(
-        &mut self,
-        layout_parent_style: &ComputedValues,
-    ) {
-        use properties::computed_value_flags::ComputedValueFlags;
-        if layout_parent_style.flags.contains(ComputedValueFlags::HAS_TEXT_DECORATION_LINES) ||
-           !self.style.get_text().clone_text_decoration_line().is_empty() {
-            self.style.flags.insert(ComputedValueFlags::HAS_TEXT_DECORATION_LINES);
-        }
-    }
-
     /// Computes the used text decoration for Servo.
     ///
     /// FIXME(emilio): This is a layout tree concept, should move away from
     /// style, since otherwise we're going to have the same subtle bugs WebKit
     /// and Blink have with this very same thing.
     #[cfg(feature = "servo")]
     fn adjust_for_text_decorations_in_effect(&mut self) {
         use values::computed::text::TextDecorationsInEffect;
@@ -453,17 +444,16 @@ impl<'a, 'b: 'a> StyleAdjuster<'a, 'b> {
         }
     }
 
     #[cfg(feature = "gecko")]
     fn should_suppress_linebreak(
         &self,
         layout_parent_style: &ComputedValues,
     ) -> bool {
-        use properties::computed_value_flags::ComputedValueFlags;
         // Line break suppression should only be propagated to in-flow children.
         if self.style.floated() || self.style.out_of_flow_positioned() {
             return false;
         }
         let parent_display = layout_parent_style.get_box().clone_display();
         if layout_parent_style.flags.contains(ComputedValueFlags::SHOULD_SUPPRESS_LINEBREAK) {
             // Line break suppression is propagated to any children of
             // line participants.
@@ -498,17 +488,16 @@ impl<'a, 'b: 'a> StyleAdjuster<'a, 'b> {
     fn adjust_for_ruby<E>(
         &mut self,
         layout_parent_style: &ComputedValues,
         element: Option<E>,
     )
     where
         E: TElement,
     {
-        use properties::computed_value_flags::ComputedValueFlags;
         use properties::longhands::unicode_bidi::computed_value::T as UnicodeBidi;
 
         let self_display = self.style.get_box().clone_display();
         // Check whether line break should be suppressed for this element.
         if self.should_suppress_linebreak(layout_parent_style) {
             self.style.flags.insert(ComputedValueFlags::SHOULD_SUPPRESS_LINEBREAK);
             // Inlinify the display type if allowed.
             if !self.skip_item_display_fixup(element) {
@@ -550,33 +539,25 @@ impl<'a, 'b: 'a> StyleAdjuster<'a, 'b> {
     /// though.
     ///
     /// FIXME(emilio): This isn't technically a style adjustment thingie, could
     /// it move somewhere else?
     fn adjust_for_visited<E>(&mut self, element: Option<E>)
     where
         E: TElement,
     {
-        use properties::computed_value_flags::ComputedValueFlags;
-
         if !self.style.has_visited_style() {
             return;
         }
 
         let is_link_element =
             self.style.pseudo.is_none() &&
             element.map_or(false, |e| e.is_link());
 
-        let relevant_link_visited = if is_link_element {
-            element.unwrap().is_visited_link()
-        } else {
-            self.style.inherited_flags().contains(ComputedValueFlags::IS_RELEVANT_LINK_VISITED)
-        };
-
-        if relevant_link_visited {
+        if is_link_element && element.unwrap().is_visited_link() {
             self.style.flags.insert(ComputedValueFlags::IS_RELEVANT_LINK_VISITED);
         }
     }
 
     /// Resolves "justify-items: auto" based on the inherited style if needed to
     /// comply with:
     ///
     /// <https://drafts.csswg.org/css-align/#valdef-justify-items-legacy>
@@ -669,17 +650,16 @@ impl<'a, 'b: 'a> StyleAdjuster<'a, 'b> {
         }
         #[cfg(feature = "servo")]
         {
             self.adjust_for_alignment(layout_parent_style);
         }
         self.adjust_for_border_width();
         self.adjust_for_outline();
         self.adjust_for_writing_mode(layout_parent_style);
-        self.adjust_for_text_decoration_lines(layout_parent_style);
         #[cfg(feature = "gecko")]
         {
             self.adjust_for_ruby(layout_parent_style, element);
         }
         #[cfg(feature = "servo")]
         {
             self.adjust_for_text_decorations_in_effect();
         }
diff --git a/servo/components/style/stylesheets/viewport_rule.rs b/servo/components/style/stylesheets/viewport_rule.rs
--- a/servo/components/style/stylesheets/viewport_rule.rs
+++ b/servo/components/style/stylesheets/viewport_rule.rs
@@ -703,29 +703,26 @@ impl MaybeNew for ViewportConstraints {
         }
 
         // DEVICE-ADAPT § 6.2.2 Constrain zoom value to the [min-zoom, max-zoom] range
         if initial_zoom.is_some() {
             initial_zoom = max!(min_zoom, min!(max_zoom, initial_zoom));
         }
 
         // DEVICE-ADAPT § 6.2.3 Resolve non-auto lengths to pixel lengths
-        //
-        // Note: DEVICE-ADAPT § 5. states that relative length values are
-        // resolved against initial values
         let initial_viewport = device.au_viewport_size();
 
         let provider = get_metrics_provider_for_product();
 
-        let default_values = device.default_computed_values();
-
         let mut conditions = RuleCacheConditions::default();
         let context = Context {
             is_root_element: false,
-            builder: StyleBuilder::for_derived_style(device, default_values, None, None),
+            // Note: DEVICE-ADAPT § 5. states that relative length values are
+            // resolved against initial values
+            builder: StyleBuilder::for_inheritance(device, None, None),
             font_metrics_provider: &provider,
             cached_system_font: None,
             in_media_query: false,
             quirks_mode: quirks_mode,
             for_smil_animation: false,
             for_non_inherited_property: None,
             rule_cache_conditions: RefCell::new(&mut conditions),
         };
diff --git a/servo/components/style/values/computed/mod.rs b/servo/components/style/values/computed/mod.rs
--- a/servo/components/style/values/computed/mod.rs
+++ b/servo/components/style/values/computed/mod.rs
@@ -169,22 +169,21 @@ impl<'a> Context<'a> {
         device: &Device,
         quirks_mode: QuirksMode,
         f: F,
     ) -> R
     where
         F: FnOnce(&Context) -> R
     {
         let mut conditions = RuleCacheConditions::default();
-        let default_values = device.default_computed_values();
         let provider = get_metrics_provider_for_product();
 
         let context = Context {
             is_root_element: false,
-            builder: StyleBuilder::for_derived_style(device, default_values, None, None),
+            builder: StyleBuilder::for_inheritance(device, None, None),
             font_metrics_provider: &provider,
             cached_system_font: None,
             in_media_query: true,
             quirks_mode,
             for_smil_animation: false,
             for_non_inherited_property: None,
             rule_cache_conditions: RefCell::new(&mut conditions),
         };
diff --git a/servo/components/style/values/specified/box.rs b/servo/components/style/values/specified/box.rs
--- a/servo/components/style/values/specified/box.rs
+++ b/servo/components/style/values/specified/box.rs
@@ -212,16 +212,26 @@ impl Display {
             Display::Grid => Display::InlineGrid,
             Display::MozBox => Display::MozInlineBox,
             Display::MozStack => Display::MozInlineStack,
             Display::WebkitBox => Display::WebkitInlineBox,
             other => other,
         }
     }
 
+    /// Returns true if the value is `Contents`
+    #[inline]
+    pub fn is_contents(&self) -> bool {
+        match *self {
+            #[cfg(feature = "gecko")]
+            Display::Contents => true,
+            _ => false,
+        }
+    }
+
     /// Returns true if the value is `None`
     #[inline]
     pub fn is_none(&self) -> bool {
         *self == Display::None
     }
 }
 
 /// A specified value for the `vertical-align` property.
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -1981,20 +1981,21 @@ pub extern "C" fn Servo_FontFeatureValue
 pub extern "C" fn Servo_FontFeatureValuesRule_GetValueText(rule: RawServoFontFeatureValuesRuleBorrowed,
                                                            result: *mut nsAString) {
     read_locked_arc(rule, |rule: &FontFeatureValuesRule| {
         rule.value_to_css(&mut CssWriter::new(unsafe { result.as_mut().unwrap() })).unwrap();
     })
 }
 
 #[no_mangle]
-pub extern "C" fn Servo_ComputedValues_GetForAnonymousBox(parent_style_or_null: ServoStyleContextBorrowedOrNull,
-                                                          pseudo_tag: *mut nsIAtom,
-                                                          raw_data: RawServoStyleSetBorrowed)
-     -> ServoStyleContextStrong {
+pub extern "C" fn Servo_ComputedValues_GetForAnonymousBox(
+    parent_style_or_null: ServoStyleContextBorrowedOrNull,
+    pseudo_tag: *mut nsIAtom,
+    raw_data: RawServoStyleSetBorrowed,
+) -> ServoStyleContextStrong {
     let global_style_data = &*GLOBAL_STYLE_DATA;
     let guard = global_style_data.shared_lock.read();
     let guards = StylesheetGuards::same(&guard);
     let data = PerDocumentStyleData::from_ffi(raw_data).borrow_mut();
     let atom = Atom::from(pseudo_tag);
     let pseudo = PseudoElement::from_anon_box_atom(&atom)
         .expect("Not an anon box pseudo?");
 
@@ -2283,17 +2284,17 @@ fn get_pseudo_style(
 
     if is_probe {
         return style;
     }
 
     Some(style.unwrap_or_else(|| {
         StyleBuilder::for_inheritance(
             doc_data.stylist.device(),
-            styles.primary(),
+            Some(styles.primary()),
             Some(pseudo),
         ).build()
     }))
 }
 
 #[no_mangle]
 pub extern "C" fn Servo_ComputedValues_Inherit(
     raw_data: RawServoStyleSetBorrowed,
@@ -2302,40 +2303,28 @@ pub extern "C" fn Servo_ComputedValues_I
     target: structs::InheritTarget
 ) -> ServoStyleContextStrong {
     let data = PerDocumentStyleData::from_ffi(raw_data).borrow();
 
     let for_text = target == structs::InheritTarget::Text;
     let atom = Atom::from(pseudo_tag);
     let pseudo = PseudoElement::from_anon_box_atom(&atom)
         .expect("Not an anon-box? Gah!");
-    let style = if let Some(reference) = parent_style_context {
-        let mut style = StyleBuilder::for_inheritance(
-            data.stylist.device(),
-            reference,
-            Some(&pseudo)
-        );
-
-        if for_text {
-            StyleAdjuster::new(&mut style)
-                .adjust_for_text();
-        }
-
-        style.build()
-    } else {
-        debug_assert!(!for_text);
-        StyleBuilder::for_derived_style(
-            data.stylist.device(),
-            data.default_computed_values(),
-            /* parent_style = */ None,
-            Some(&pseudo),
-        ).build()
-    };
-
-    style.into()
+
+    let mut style = StyleBuilder::for_inheritance(
+        data.stylist.device(),
+        parent_style_context,
+        Some(&pseudo)
+    );
+
+    if for_text {
+        StyleAdjuster::new(&mut style).adjust_for_text();
+    }
+
+    style.build().into()
 }
 
 #[no_mangle]
 pub extern "C" fn Servo_ComputedValues_GetStyleBits(values: ServoStyleContextBorrowed) -> u64 {
     use style::properties::computed_value_flags::ComputedValueFlags;
     // FIXME(emilio): We could do this more efficiently I'm quite sure.
     let flags = values.flags;
     let mut result = 0;
@@ -3642,32 +3631,30 @@ fn simulate_compute_values_failure(prope
     id.as_shorthand().is_ok() && property.mSimulateComputeValuesFailure
 }
 
 #[cfg(not(feature = "gecko_debug"))]
 fn simulate_compute_values_failure(_: &PropertyValuePair) -> bool {
     false
 }
 
-fn create_context<'a>(
+fn create_context_for_animation<'a>(
     per_doc_data: &'a PerDocumentStyleDataImpl,
     font_metrics_provider: &'a FontMetricsProvider,
     style: &'a ComputedValues,
     parent_style: Option<&'a ComputedValues>,
-    pseudo: Option<&'a PseudoElement>,
     for_smil_animation: bool,
     rule_cache_conditions: &'a mut RuleCacheConditions,
 ) -> Context<'a> {
     Context {
         is_root_element: false,
-        builder: StyleBuilder::for_derived_style(
+        builder: StyleBuilder::for_animation(
             per_doc_data.stylist.device(),
             style,
             parent_style,
-            pseudo,
         ),
         font_metrics_provider: font_metrics_provider,
         cached_system_font: None,
         in_media_query: false,
         quirks_mode: per_doc_data.stylist.quirks_mode(),
         for_smil_animation,
         for_non_inherited_property: None,
         rule_cache_conditions: RefCell::new(rule_cache_conditions),
@@ -3735,24 +3722,22 @@ pub extern "C" fn Servo_GetComputedKeyfr
     let data = PerDocumentStyleData::from_ffi(raw_data).borrow();
     let metrics = get_metrics_provider_for_product();
 
     let element = GeckoElement(element);
     let parent_element = element.inheritance_parent();
     let parent_data = parent_element.as_ref().and_then(|e| e.borrow_data());
     let parent_style = parent_data.as_ref().map(|d| d.styles.primary()).map(|x| &**x);
 
-    let pseudo = style.pseudo();
     let mut conditions = Default::default();
-    let mut context = create_context(
+    let mut context = create_context_for_animation(
         &data,
         &metrics,
         &style,
         parent_style,
-        pseudo.as_ref(),
         /* for_smil_animation = */ false,
         &mut conditions,
     );
 
     let global_style_data = &*GLOBAL_STYLE_DATA;
     let guard = global_style_data.shared_lock.read();
     let default_values = data.default_computed_values();
 
@@ -3844,24 +3829,22 @@ pub extern "C" fn Servo_GetAnimationValu
     let data = PerDocumentStyleData::from_ffi(raw_data).borrow();
     let metrics = get_metrics_provider_for_product();
 
     let element = GeckoElement(element);
     let parent_element = element.inheritance_parent();
     let parent_data = parent_element.as_ref().and_then(|e| e.borrow_data());
     let parent_style = parent_data.as_ref().map(|d| d.styles.primary()).map(|x| &**x);
 
-    let pseudo = style.pseudo();
     let mut conditions = Default::default();
-    let mut context = create_context(
+    let mut context = create_context_for_animation(
         &data,
         &metrics,
         &style,
         parent_style,
-        pseudo.as_ref(),
         /* for_smil_animation = */ true,
         &mut conditions,
     );
 
     let default_values = data.default_computed_values();
     let global_style_data = &*GLOBAL_STYLE_DATA;
     let guard = global_style_data.shared_lock.read();
 
@@ -3888,24 +3871,22 @@ pub extern "C" fn Servo_AnimationValue_C
     let data = PerDocumentStyleData::from_ffi(raw_data).borrow();
     let metrics = get_metrics_provider_for_product();
 
     let element = GeckoElement(element);
     let parent_element = element.inheritance_parent();
     let parent_data = parent_element.as_ref().and_then(|e| e.borrow_data());
     let parent_style = parent_data.as_ref().map(|d| d.styles.primary()).map(|x| &**x);
 
-    let pseudo = style.pseudo();
     let mut conditions = Default::default();
-    let mut context = create_context(
+    let mut context = create_context_for_animation(
         &data,
         &metrics,
         style,
         parent_style,
-        pseudo.as_ref(),
         /* for_smil_animation = */ false,
         &mut conditions,
     );
 
     let default_values = data.default_computed_values();
     let global_style_data = &*GLOBAL_STYLE_DATA;
     let guard = global_style_data.shared_lock.read();
     let declarations = Locked::<PropertyDeclarationBlock>::as_arc(&declarations);
@@ -4593,28 +4574,21 @@ pub extern "C" fn Servo_ComputeColor(
         ErrorReporter::new(ptr::null_mut(), loader, ptr::null_mut())
     });
 
     match parse_color(&value, reporter.as_ref()) {
         Ok(specified_color) => {
             let computed_color = match raw_data {
                 Some(raw_data) => {
                     let data = PerDocumentStyleData::from_ffi(raw_data).borrow();
-                    let metrics = get_metrics_provider_for_product();
-                    let mut conditions = Default::default();
-                    let context = create_context(
-                        &data,
-                        &metrics,
-                        data.stylist.device().default_computed_values(),
-                        /* parent_style = */ None,
-                        /* pseudo = */ None,
-                        /* for_smil_animation = */ false,
-                        &mut conditions,
-                    );
-                    specified_color.to_computed_color(Some(&context))
+                    let device = data.stylist.device();
+                    let quirks_mode = data.stylist.quirks_mode();
+                    Context::for_media_query_evaluation(device, quirks_mode, |context| {
+                        specified_color.to_computed_color(Some(&context))
+                    })
                 }
                 None => {
                     specified_color.to_computed_color(None)
                 }
             };
 
             match computed_color {
                 Some(computed_color) => {
