# HG changeset patch
# User Mike Pennisi <mike@mikepennisi.com>
# Date 1519626540 -7200
# Node ID 99444f1ed75a990c746ac40ffe6371258f3eb01d
# Parent  a5e59d9a81f94ebc4cd3ab14b74a5bbb005af8e5
Bug 1439411 - [mozlog] Document built-in CLI logging options r=jgraham

The output formatters provided by mozlog are well-documented in the
online help guide, but this information is not available to users in the
CLI. The `add_logging_group` method extends the consuming project's
command-line interface without referencing mozlog itself. This means
consumers may not have a means to discover the additional information,
and even in cases where they can infer this connection, there is no
indication of the stability of the behavior.

Extend the description of the built-in output formatters to explain
their origin and reference the relevant documentation.

diff --git a/testing/mozbase/mozlog/mozlog/commandline.py b/testing/mozbase/mozlog/mozlog/commandline.py
--- a/testing/mozbase/mozlog/mozlog/commandline.py
+++ b/testing/mozbase/mozlog/mozlog/commandline.py
@@ -10,29 +10,38 @@ import os
 import sys
 from collections import defaultdict
 
 from . import handlers
 from . import formatters
 from .structuredlog import StructuredLogger, set_default_logger
 
 log_formatters = {
-    'raw': (formatters.JSONFormatter, "Raw structured log messages"),
-    'unittest': (formatters.UnittestFormatter, "Unittest style output"),
-    'xunit': (formatters.XUnitFormatter, "xUnit compatible XML"),
-    'html': (formatters.HTMLFormatter, "HTML report"),
-    'mach': (formatters.MachFormatter, "Human-readable output"),
-    'tbpl': (formatters.TbplFormatter, "TBPL style log format"),
+    'raw': (formatters.JSONFormatter, "Raw structured log messages "
+                                      "(provided by mozlog)"),
+    'unittest': (formatters.UnittestFormatter, "Unittest style output "
+                                               "(provided by mozlog)"),
+    'xunit': (formatters.XUnitFormatter, "xUnit compatible XML "
+                                         "(povided by mozlog)"),
+    'html': (formatters.HTMLFormatter, "HTML report "
+                                       "(provided by mozlog)"),
+    'mach': (formatters.MachFormatter, "Human-readable output "
+                                       "(provided by mozlog)"),
+    'tbpl': (formatters.TbplFormatter, "TBPL style log format "
+                                       "(provided by mozlog)"),
     'errorsummary': (formatters.ErrorSummaryFormatter, argparse.SUPPRESS),
 }
 
 TEXT_FORMATTERS = ('raw', 'mach')
 """a subset of formatters for non test harnesses related applications"""
 
 
+DOCS_URL = "https://firefox-source-docs.mozilla.org/mozbase/mozlog.html"
+
+
 def level_filter_wrapper(formatter, level):
     return handlers.LogLevelFilter(formatter, level)
 
 
 def verbose_wrapper(formatter, verbose):
     formatter.verbose = verbose
     return formatter
 
@@ -116,17 +125,19 @@ def add_logging_group(parser, include_fo
                                of this option is to specify
                                :data:`TEXT_FORMATTERS` to include only the
                                most useful formatters for a command line tool
                                that is not related to test harnesses.
     """
     group_name = "Output Logging"
     group_description = ("Each option represents a possible logging format "
                          "and takes a filename to write that format to, "
-                         "or '-' to write to stdout.")
+                         "or '-' to write to stdout. Some options are "
+                         "provided by the mozlog utility; see %s "
+                         "for extended documentation." % DOCS_URL)
 
     if include_formatters is None:
         include_formatters = list(log_formatters.keys())
 
     if isinstance(parser, optparse.OptionParser):
         group = optparse.OptionGroup(parser,
                                      group_name,
                                      group_description)

