# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1560369055 14400
#      Wed Jun 12 15:50:55 2019 -0400
# Node ID b91b32c55b199626595ef97e3ae6c6319f826644
# Parent  05432633ef429b8d953b888eb53d7d77ca41dcf8
Bug 1558883 - Add QCMS transform correctness and performance tests. r=miko

Differential Revision: https://phabricator.services.mozilla.com/D34765

diff --git a/gfx/qcms/qcmsint.h b/gfx/qcms/qcmsint.h
--- a/gfx/qcms/qcmsint.h
+++ b/gfx/qcms/qcmsint.h
@@ -26,16 +26,20 @@ struct precache_output
 };
 
 #ifdef _MSC_VER
 #define ALIGN __declspec(align(16))
 #else
 #define ALIGN __attribute__(( aligned (16) ))
 #endif
 
+struct _qcms_transform;
+
+typedef void (*transform_fn_t)(const struct _qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length);
+
 struct _qcms_transform {
 	float ALIGN matrix[3][4];
 	float *input_gamma_table_r;
 	float *input_gamma_table_g;
 	float *input_gamma_table_b;
 
 	float *input_clut_table_r;
 	float *input_clut_table_g;
@@ -69,17 +73,17 @@ struct _qcms_transform {
 	size_t output_gamma_lut_b_length;
 
 	size_t output_gamma_lut_gray_length;
 
 	struct precache_output *output_table_r;
 	struct precache_output *output_table_g;
 	struct precache_output *output_table_b;
 
-	void (*transform_fn)(const struct _qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length);
+	transform_fn_t transform_fn;
 };
 
 struct matrix {
 	float m[3][3];
 	bool invalid;
 };
 
 struct qcms_modular_transform;
@@ -259,16 +263,42 @@ static inline float uInt16Number_to_floa
 	return ((int32_t)a)/65535.f;
 }
 
 
 void precache_release(struct precache_output *p);
 bool set_rgb_colorants(qcms_profile *profile, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries);
 bool get_rgb_colorants(struct matrix *colorants, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries);
 
+void qcms_transform_data_rgb_out_lut(const qcms_transform *transform,
+                                     const unsigned char *src,
+                                     unsigned char *dest,
+                                     size_t length);
+void qcms_transform_data_rgba_out_lut(const qcms_transform *transform,
+                                      const unsigned char *src,
+                                      unsigned char *dest,
+                                      size_t length);
+void qcms_transform_data_bgra_out_lut(const qcms_transform *transform,
+                                      const unsigned char *src,
+                                      unsigned char *dest,
+                                      size_t length);
+
+void qcms_transform_data_rgb_out_lut_precache(const qcms_transform *transform,
+                                              const unsigned char *src,
+                                              unsigned char *dest,
+                                              size_t length);
+void qcms_transform_data_rgba_out_lut_precache(const qcms_transform *transform,
+                                               const unsigned char *src,
+                                               unsigned char *dest,
+                                               size_t length);
+void qcms_transform_data_bgra_out_lut_precache(const qcms_transform *transform,
+                                               const unsigned char *src,
+                                               unsigned char *dest,
+                                               size_t length);
+
 void qcms_transform_data_rgb_out_lut_sse2(const qcms_transform *transform,
                                           const unsigned char *src,
                                           unsigned char *dest,
                                           size_t length);
 void qcms_transform_data_rgba_out_lut_sse2(const qcms_transform *transform,
                                           const unsigned char *src,
                                           unsigned char *dest,
                                           size_t length);
diff --git a/gfx/qcms/transform.cpp b/gfx/qcms/transform.cpp
--- a/gfx/qcms/transform.cpp
+++ b/gfx/qcms/transform.cpp
@@ -508,27 +508,27 @@ static void qcms_transform_data_template
 		dest[kBIndex] = transform->output_table_b->data[b];
 		if (kAIndex != NO_A_INDEX) {
 			dest[kAIndex] = alpha;
 		}
 		dest += components;
 	}
 }
 
-static void qcms_transform_data_rgb_out_lut_precache(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
+void qcms_transform_data_rgb_out_lut_precache(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
 {
 	qcms_transform_data_template_lut_precache<RGBA_R_INDEX, RGBA_G_INDEX, RGBA_B_INDEX>(transform, src, dest, length);
 }
 
-static void qcms_transform_data_rgba_out_lut_precache(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
+void qcms_transform_data_rgba_out_lut_precache(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
 {
 	qcms_transform_data_template_lut_precache<RGBA_R_INDEX, RGBA_G_INDEX, RGBA_B_INDEX, RGBA_A_INDEX>(transform, src, dest, length);
 }
 
-static void qcms_transform_data_bgra_out_lut_precache(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
+void qcms_transform_data_bgra_out_lut_precache(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
 {
 	qcms_transform_data_template_lut_precache<BGRA_R_INDEX, BGRA_G_INDEX, BGRA_B_INDEX, BGRA_A_INDEX>(transform, src, dest, length);
 }
 
 // Not used
 /* 
 static void qcms_transform_data_clut(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length) {
 	unsigned int i;
@@ -767,27 +767,27 @@ static void qcms_transform_data_template
 		dest[kBIndex] = clamp_u8(out_device_b*255);
 		if (kAIndex != NO_A_INDEX) {
 			dest[kAIndex] = alpha;
 		}
 		dest += components;
 	}
 }
 
-static void qcms_transform_data_rgb_out_lut(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
+void qcms_transform_data_rgb_out_lut(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
 {
 	qcms_transform_data_template_lut<RGBA_R_INDEX, RGBA_G_INDEX, RGBA_B_INDEX>(transform, src, dest, length);
 }
 
-static void qcms_transform_data_rgba_out_lut(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
+void qcms_transform_data_rgba_out_lut(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
 {
 	qcms_transform_data_template_lut<RGBA_R_INDEX, RGBA_G_INDEX, RGBA_B_INDEX, RGBA_A_INDEX>(transform, src, dest, length);
 }
 
-static void qcms_transform_data_bgra_out_lut(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
+void qcms_transform_data_bgra_out_lut(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
 {
 	qcms_transform_data_template_lut<BGRA_R_INDEX, BGRA_G_INDEX, BGRA_B_INDEX, BGRA_A_INDEX>(transform, src, dest, length);
 }
 
 #if 0
 static void qcms_transform_data_rgb_out_linear(const qcms_transform *transform, const unsigned char *src, unsigned char *dest, size_t length)
 {
 	int i;
diff --git a/gfx/tests/gtest/TestQcms.cpp b/gfx/tests/gtest/TestQcms.cpp
--- a/gfx/tests/gtest/TestQcms.cpp
+++ b/gfx/tests/gtest/TestQcms.cpp
@@ -1,70 +1,34 @@
 /* vim:set ts=2 sw=2 sts=2 et: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/
  */
 
 #include "gtest/gtest.h"
+#include "gtest/MozGTestBench.h"
 #include "gmock/gmock.h"
 
 #include "mozilla/ArrayUtils.h"
+#include "mozilla/UniquePtr.h"
+#include "mozilla/SSE.h"
+#include "mozilla/arm.h"
 #include "qcms.h"
 #include "qcmsint.h"
 #include "transform_util.h"
 
-const size_t allGBSize = 1 * 256 * 256 * 4;
-static unsigned char* createAllGB() {
-  unsigned char* buff = (unsigned char*)malloc(allGBSize);
-  int pos = 0;
-  for (int r = 0; r < 1; r++) { // Skip all R values for speed
-    for (int g = 0; g < 256; g++) {
-      for (int b = 0; b < 256; b++) {
-        buff[pos * 4 + 0] = r;
-        buff[pos * 4 + 1] = g;
-        buff[pos * 4 + 2] = b;
-        buff[pos * 4 + 3] = 0x80;
-        pos++;
-      }
-    }
-  }
-
-  return buff;
-}
-
-TEST(GfxQcms, Identity) {
-  // XXX: This means that we can't have qcms v2 unit test
-  //      without changing the qcms API.
-  qcms_enable_iccv4();
+#include <cmath>
 
-  qcms_profile* input_profile = qcms_profile_sRGB();
-  qcms_profile* output_profile = qcms_profile_sRGB();
-
-  EXPECT_FALSE(qcms_profile_is_bogus(input_profile));
-  EXPECT_FALSE(qcms_profile_is_bogus(output_profile));
-
-  const qcms_intent intent = QCMS_INTENT_DEFAULT;
-  qcms_data_type input_type = QCMS_DATA_RGBA_8;
-  qcms_data_type output_type = QCMS_DATA_RGBA_8;
+/* SSEv1 is only included in non-Windows or non-x86-64-bit builds. */
+#if defined(MOZILLA_MAY_SUPPORT_SSE) && \
+    (!(defined(_MSC_VER) && defined(_M_AMD64)))
+#  define QCMS_MAY_SUPPORT_SSE
+#endif
 
-  qcms_transform* transform = qcms_transform_create(input_profile, input_type,
-                                                    output_profile, output_type,
-                                                    intent);
-
-  unsigned char *data_in = createAllGB();;
-  unsigned char *data_out = (unsigned char*)malloc(allGBSize);
-  qcms_transform_data(transform, data_in, data_out, allGBSize / 4);
-
-  qcms_profile_release(input_profile);
-  qcms_profile_release(output_profile);
-  qcms_transform_release(transform);
-
-  free(data_in);
-  free(data_out);
-}
+using namespace mozilla;
 
 TEST(GfxQcms, LutInverseCrash) {
   uint16_t lutTable1[] = {
     0x0000, 0x0000, 0x0000, 0x8000, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
     0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
     0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
     0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
     0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
@@ -162,8 +126,489 @@ TEST(GfxQcms, LutInverseNonMonotonic) {
   }
 
   for (uint16_t i = 0; i < 65535; i++) {
     lut_inverse_interp16(i, lutTable, (int)mozilla::ArrayLength(lutTable));
   }
 
   // Make sure we don't crash, hang or let sanitizers do their magic
 }
+
+static bool CmpRgbChannel(const uint8_t* aRef, const uint8_t* aTest,
+                          size_t aIndex) {
+  return std::abs(static_cast<int8_t>(aRef[aIndex] - aTest[aIndex])) <= 1;
+}
+
+template <bool kSwapRB, bool kHasAlpha>
+static bool CmpRgbBufferImpl(const uint8_t* aRefBuffer,
+                             const uint8_t* aTestBuffer, size_t aPixels) {
+  const size_t pixelSize = kHasAlpha ? 4 : 3;
+  if (memcmp(aRefBuffer, aTestBuffer, aPixels * pixelSize) == 0) {
+    return true;
+  }
+
+  const size_t kRIndex = kSwapRB ? 2 : 0;
+  const size_t kGIndex = 1;
+  const size_t kBIndex = kSwapRB ? 0 : 2;
+  const size_t kAIndex = 3;
+
+  size_t remaining = aPixels;
+  const uint8_t* ref = aRefBuffer;
+  const uint8_t* test = aTestBuffer;
+  while (remaining > 0) {
+    if (!CmpRgbChannel(ref, test, kRIndex) ||
+        !CmpRgbChannel(ref, test, kGIndex) ||
+        !CmpRgbChannel(ref, test, kBIndex) ||
+        (kHasAlpha && ref[kAIndex] != test[kAIndex])) {
+      EXPECT_EQ(test[kRIndex], ref[kRIndex]);
+      EXPECT_EQ(test[kGIndex], ref[kGIndex]);
+      EXPECT_EQ(test[kBIndex], ref[kBIndex]);
+      if (kHasAlpha) {
+        EXPECT_EQ(test[kAIndex], ref[kAIndex]);
+      }
+      return false;
+    }
+
+    --remaining;
+    ref += pixelSize;
+    test += pixelSize;
+  }
+
+  return true;
+}
+
+template <bool kSwapRB, bool kHasAlpha>
+static size_t GetRgbInputBufferImpl(UniquePtr<uint8_t[]>& aOutBuffer) {
+  const uint8_t colorSamples[] = {0, 5, 16, 43, 101, 127, 182, 255};
+  const size_t colorSampleMax = sizeof(colorSamples) / sizeof(uint8_t);
+  const size_t pixelSize = kHasAlpha ? 4 : 3;
+  const size_t pixelCount = colorSampleMax * colorSampleMax * 256 * 3;
+
+  aOutBuffer = MakeUnique<uint8_t[]>(pixelCount * pixelSize);
+  if (!aOutBuffer) {
+    return 0;
+  }
+
+  const size_t kRIndex = kSwapRB ? 2 : 0;
+  const size_t kGIndex = 1;
+  const size_t kBIndex = kSwapRB ? 0 : 2;
+  const size_t kAIndex = 3;
+
+  // Sample every red pixel value with a subset of green and blue.
+  uint8_t* color = aOutBuffer.get();
+  for (uint16_t r = 0; r < 256; ++r) {
+    for (uint8_t g : colorSamples) {
+      for (uint8_t b : colorSamples) {
+        color[kRIndex] = r;
+        color[kGIndex] = g;
+        color[kBIndex] = b;
+        if (kHasAlpha) {
+          color[kAIndex] = 0x80;
+        }
+        color += pixelSize;
+      }
+    }
+  }
+
+  // Sample every green pixel value with a subset of red and blue.
+  for (uint8_t r : colorSamples) {
+    for (uint16_t g = 0; g < 256; ++g) {
+      for (uint8_t b : colorSamples) {
+        color[kRIndex] = r;
+        color[kGIndex] = g;
+        color[kBIndex] = b;
+        if (kHasAlpha) {
+          color[kAIndex] = 0x80;
+        }
+        color += pixelSize;
+      }
+    }
+  }
+
+  // Sample every blue pixel value with a subset of red and green.
+  for (uint8_t r : colorSamples) {
+    for (uint8_t g : colorSamples) {
+      for (uint16_t b = 0; b < 256; ++b) {
+        color[kRIndex] = r;
+        color[kGIndex] = g;
+        color[kBIndex] = b;
+        if (kHasAlpha) {
+          color[kAIndex] = 0x80;
+        }
+        color += pixelSize;
+      }
+    }
+  }
+
+  return pixelCount;
+}
+
+static size_t GetRgbInputBuffer(UniquePtr<uint8_t[]>& aOutBuffer) {
+  return GetRgbInputBufferImpl<false, false>(aOutBuffer);
+}
+
+static size_t GetRgbaInputBuffer(UniquePtr<uint8_t[]>& aOutBuffer) {
+  return GetRgbInputBufferImpl<false, true>(aOutBuffer);
+}
+
+static size_t GetBgraInputBuffer(UniquePtr<uint8_t[]>& aOutBuffer) {
+  return GetRgbInputBufferImpl<true, true>(aOutBuffer);
+}
+
+static bool CmpRgbBuffer(const uint8_t* aRefBuffer, const uint8_t* aTestBuffer,
+                         size_t aPixels) {
+  return CmpRgbBufferImpl<false, false>(aRefBuffer, aTestBuffer, aPixels);
+}
+
+static bool CmpRgbaBuffer(const uint8_t* aRefBuffer, const uint8_t* aTestBuffer,
+                          size_t aPixels) {
+  return CmpRgbBufferImpl<false, true>(aRefBuffer, aTestBuffer, aPixels);
+}
+
+static bool CmpBgraBuffer(const uint8_t* aRefBuffer, const uint8_t* aTestBuffer,
+                          size_t aPixels) {
+  return CmpRgbBufferImpl<true, true>(aRefBuffer, aTestBuffer, aPixels);
+}
+
+static void ClearRgbBuffer(uint8_t* aBuffer, size_t aPixels) {
+  if (aBuffer) {
+    memset(aBuffer, 0, aPixels * 3);
+  }
+}
+
+static void ClearRgbaBuffer(uint8_t* aBuffer, size_t aPixels) {
+  if (aBuffer) {
+    memset(aBuffer, 0, aPixels * 4);
+  }
+}
+
+static UniquePtr<uint8_t[]> GetRgbOutputBuffer(size_t aPixels) {
+  UniquePtr<uint8_t[]> buffer = MakeUnique<uint8_t[]>(aPixels * 3);
+  ClearRgbBuffer(buffer.get(), aPixels);
+  return buffer;
+}
+
+static UniquePtr<uint8_t[]> GetRgbaOutputBuffer(size_t aPixels) {
+  UniquePtr<uint8_t[]> buffer = MakeUnique<uint8_t[]>(aPixels * 4);
+  ClearRgbaBuffer(buffer.get(), aPixels);
+  return buffer;
+}
+
+class GfxQcms_ProfilePairBase : public ::testing::Test {
+ protected:
+  GfxQcms_ProfilePairBase()
+      : mInProfile(nullptr),
+        mOutProfile(nullptr),
+        mTransform(nullptr),
+        mPixels(0),
+        mStorageType(QCMS_DATA_RGB_8),
+        mPrecache(false) {}
+
+  void TransformPrecache();
+  void TransformPrecachePlatformExt();
+
+  void SetUp() override {
+    // XXX: This means that we can't have qcms v2 unit test
+    //      without changing the qcms API.
+    qcms_enable_iccv4();
+  }
+
+  void TearDown() override {
+    if (mInProfile) {
+      qcms_profile_release(mInProfile);
+    }
+    if (mOutProfile) {
+      qcms_profile_release(mOutProfile);
+    }
+    if (mTransform) {
+      qcms_transform_release(mTransform);
+    }
+  }
+
+  bool SetTransform(qcms_transform* aTransform) {
+    if (mTransform) {
+      qcms_transform_release(mTransform);
+    }
+    mTransform = aTransform;
+    return !!mTransform;
+  }
+
+  bool SetTransform(qcms_data_type aType) {
+    return SetTransform(qcms_transform_create(mInProfile, aType, mOutProfile,
+                                              aType, QCMS_INTENT_DEFAULT));
+  }
+
+  bool SetBuffers(qcms_data_type aType) {
+    switch (aType) {
+      case QCMS_DATA_RGB_8:
+        mPixels = GetRgbInputBuffer(mInput);
+        mRef = GetRgbOutputBuffer(mPixels);
+        mOutput = GetRgbOutputBuffer(mPixels);
+        break;
+      case QCMS_DATA_RGBA_8:
+        mPixels = GetRgbaInputBuffer(mInput);
+        mRef = GetRgbaOutputBuffer(mPixels);
+        mOutput = GetRgbaOutputBuffer(mPixels);
+        break;
+      case QCMS_DATA_BGRA_8:
+        mPixels = GetBgraInputBuffer(mInput);
+        mRef = GetRgbaOutputBuffer(mPixels);
+        mOutput = GetRgbaOutputBuffer(mPixels);
+        break;
+      default:
+        MOZ_ASSERT_UNREACHABLE("Unknown type!");
+        break;
+    }
+
+    mStorageType = aType;
+    return mInput && mOutput && mRef && mPixels > 0u;
+  }
+
+  void ClearOutputBuffer() {
+    switch (mStorageType) {
+      case QCMS_DATA_RGB_8:
+        ClearRgbBuffer(mOutput.get(), mPixels);
+        break;
+      case QCMS_DATA_RGBA_8:
+      case QCMS_DATA_BGRA_8:
+        ClearRgbaBuffer(mOutput.get(), mPixels);
+        break;
+      default:
+        MOZ_ASSERT_UNREACHABLE("Unknown type!");
+        break;
+    }
+  }
+
+  void ProduceRef(transform_fn_t aFn) {
+    aFn(mTransform, mInput.get(), mRef.get(), mPixels);
+  }
+
+  void CopyInputToRef() {
+    size_t pixelSize = 0;
+    switch (mStorageType) {
+      case QCMS_DATA_RGB_8:
+        pixelSize = 3;
+        break;
+      case QCMS_DATA_RGBA_8:
+      case QCMS_DATA_BGRA_8:
+        pixelSize = 4;
+        break;
+      default:
+        MOZ_ASSERT_UNREACHABLE("Unknown type!");
+        break;
+    }
+
+    memcpy(mRef.get(), mInput.get(), mPixels * pixelSize);
+  }
+
+  void ProduceOutput(transform_fn_t aFn) {
+    ClearOutputBuffer();
+    aFn(mTransform, mInput.get(), mOutput.get(), mPixels);
+  }
+
+  bool VerifyOutput(const UniquePtr<uint8_t[]>& aBuf) {
+    switch (mStorageType) {
+      case QCMS_DATA_RGB_8:
+        return CmpRgbBuffer(aBuf.get(), mOutput.get(), mPixels);
+      case QCMS_DATA_RGBA_8:
+        return CmpRgbaBuffer(aBuf.get(), mOutput.get(), mPixels);
+      case QCMS_DATA_BGRA_8:
+        return CmpBgraBuffer(aBuf.get(), mOutput.get(), mPixels);
+      default:
+        MOZ_ASSERT_UNREACHABLE("Unknown type!");
+        break;
+    }
+
+    return false;
+  }
+
+  bool ProduceVerifyOutput(transform_fn_t aFn) {
+    ProduceOutput(aFn);
+    return VerifyOutput(mRef);
+  }
+
+  void PrecacheOutput() {
+    qcms_profile_precache_output_transform(mOutProfile);
+    mPrecache = true;
+  }
+
+  qcms_profile* mInProfile;
+  qcms_profile* mOutProfile;
+  qcms_transform* mTransform;
+
+  UniquePtr<uint8_t[]> mInput;
+  UniquePtr<uint8_t[]> mOutput;
+  UniquePtr<uint8_t[]> mRef;
+  size_t mPixels;
+  qcms_data_type mStorageType;
+  bool mPrecache;
+};
+
+void GfxQcms_ProfilePairBase::TransformPrecache() {
+  // Produce reference using interpolation and the lookup tables.
+  ASSERT_FALSE(mPrecache);
+  ASSERT_TRUE(SetBuffers(QCMS_DATA_RGB_8));
+  ASSERT_TRUE(SetTransform(QCMS_DATA_RGB_8));
+  ProduceRef(qcms_transform_data_rgb_out_lut);
+
+  // Produce output using lut and precaching.
+  PrecacheOutput();
+  ASSERT_TRUE(SetTransform(QCMS_DATA_RGB_8));
+  EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_rgb_out_lut_precache));
+}
+
+void GfxQcms_ProfilePairBase::TransformPrecachePlatformExt() {
+  PrecacheOutput();
+
+  // Verify RGB transforms.
+  ASSERT_TRUE(SetBuffers(QCMS_DATA_RGB_8));
+  ASSERT_TRUE(SetTransform(QCMS_DATA_RGB_8));
+  ProduceRef(qcms_transform_data_rgb_out_lut_precache);
+#ifdef QCMS_MAY_SUPPORT_SSE
+  if (mozilla::supports_sse()) {
+    EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_rgb_out_lut_sse1));
+  }
+#endif
+#ifdef MOZILLA_MAY_SUPPORT_SSE2
+  if (mozilla::supports_sse2()) {
+    EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_rgb_out_lut_sse2));
+  }
+#endif
+#ifdef MOZILLA_MAY_SUPPORT_NEON
+  if (mozilla::supports_neon()) {
+    EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_rgb_out_lut_neon));
+  }
+#endif
+
+  // Verify RGBA transforms.
+  ASSERT_TRUE(SetBuffers(QCMS_DATA_RGBA_8));
+  ASSERT_TRUE(SetTransform(QCMS_DATA_RGBA_8));
+  ProduceRef(qcms_transform_data_rgba_out_lut_precache);
+#ifdef QCMS_MAY_SUPPORT_SSE
+  if (mozilla::supports_sse()) {
+    EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_rgba_out_lut_sse1));
+  }
+#endif
+#ifdef MOZILLA_MAY_SUPPORT_SSE2
+  if (mozilla::supports_sse2()) {
+    EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_rgba_out_lut_sse2));
+  }
+#endif
+#ifdef MOZILLA_MAY_SUPPORT_NEON
+  if (mozilla::supports_neon()) {
+    EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_rgba_out_lut_neon));
+  }
+#endif
+
+  // Verify BGRA transforms.
+  ASSERT_TRUE(SetBuffers(QCMS_DATA_BGRA_8));
+  ASSERT_TRUE(SetTransform(QCMS_DATA_BGRA_8));
+  ProduceRef(qcms_transform_data_bgra_out_lut_precache);
+#ifdef QCMS_MAY_SUPPORT_SSE
+  if (mozilla::supports_sse()) {
+    EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_bgra_out_lut_sse1));
+  }
+#endif
+#ifdef MOZILLA_MAY_SUPPORT_SSE2
+  if (mozilla::supports_sse2()) {
+    EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_bgra_out_lut_sse2));
+  }
+#endif
+#ifdef MOZILLA_MAY_SUPPORT_NEON
+  if (mozilla::supports_neon()) {
+    EXPECT_TRUE(ProduceVerifyOutput(qcms_transform_data_bgra_out_lut_neon));
+  }
+#endif
+}
+
+class GfxQcms_sRGB_To_sRGB : public GfxQcms_ProfilePairBase {
+ protected:
+  void SetUp() override {
+    GfxQcms_ProfilePairBase::SetUp();
+    mInProfile = qcms_profile_sRGB();
+    mOutProfile = qcms_profile_sRGB();
+  }
+};
+
+class GfxQcms_sRGB_To_SamsungSyncmaster : public GfxQcms_ProfilePairBase {
+ protected:
+  void SetUp() override {
+    GfxQcms_ProfilePairBase::SetUp();
+    mInProfile = qcms_profile_sRGB();
+    mOutProfile = qcms_profile_from_path("lcms_samsung_syncmaster.icc");
+  }
+};
+
+class GfxQcms_sRGB_To_ThinkpadW540 : public GfxQcms_ProfilePairBase {
+ protected:
+  void SetUp() override {
+    GfxQcms_ProfilePairBase::SetUp();
+    mInProfile = qcms_profile_sRGB();
+    mOutProfile = qcms_profile_from_path("lcms_thinkpad_w540.icc");
+  }
+};
+
+#define TEST_QCMS_PROFILE_F(test_fixture)                    \
+  TEST_F(test_fixture, TransformPrecachePlatformExt) {       \
+    GfxQcms_ProfilePairBase::TransformPrecachePlatformExt(); \
+  }
+
+TEST_F(GfxQcms_sRGB_To_sRGB, TransformPrecache) {
+  // TODO(aosmond): This doesn't pass for the non-identity transform. Should
+  // they produce the same results?
+  GfxQcms_ProfilePairBase::TransformPrecache();
+}
+
+TEST_QCMS_PROFILE_F(GfxQcms_sRGB_To_sRGB)
+
+TEST_F(GfxQcms_sRGB_To_sRGB, TransformIdentity) {
+  PrecacheOutput();
+  SetBuffers(QCMS_DATA_RGB_8);
+  SetTransform(QCMS_DATA_RGB_8);
+  qcms_transform_data(mTransform, mInput.get(), mOutput.get(), mPixels);
+  EXPECT_TRUE(VerifyOutput(mInput));
+}
+
+TEST_QCMS_PROFILE_F(GfxQcms_sRGB_To_SamsungSyncmaster)
+TEST_QCMS_PROFILE_F(GfxQcms_sRGB_To_ThinkpadW540)
+
+class GfxQcmsPerf_Base : public GfxQcms_sRGB_To_ThinkpadW540 {
+ protected:
+  explicit GfxQcmsPerf_Base(qcms_data_type aType) { mStorageType = aType; }
+
+  void TransformPerf() { ProduceRef(qcms_transform_data_rgb_out_lut_precache); }
+
+  void TransformPlatformPerf() {
+    qcms_transform_data(mTransform, mInput.get(), mRef.get(), mPixels);
+  }
+
+  void SetUp() override {
+    GfxQcms_sRGB_To_ThinkpadW540::SetUp();
+    PrecacheOutput();
+    SetBuffers(mStorageType);
+    SetTransform(mStorageType);
+  }
+};
+
+class GfxQcmsPerf_Rgb : public GfxQcmsPerf_Base {
+ protected:
+  GfxQcmsPerf_Rgb() : GfxQcmsPerf_Base(QCMS_DATA_RGB_8) {}
+};
+
+class GfxQcmsPerf_Rgba : public GfxQcmsPerf_Base {
+ protected:
+  GfxQcmsPerf_Rgba() : GfxQcmsPerf_Base(QCMS_DATA_RGBA_8) {}
+};
+
+class GfxQcmsPerf_Bgra : public GfxQcmsPerf_Base {
+ protected:
+  GfxQcmsPerf_Bgra() : GfxQcmsPerf_Base(QCMS_DATA_BGRA_8) {}
+};
+
+MOZ_GTEST_BENCH_F(GfxQcmsPerf_Rgb, TransformC, [this] { TransformPerf(); });
+MOZ_GTEST_BENCH_F(GfxQcmsPerf_Rgb, TransformPlatform,
+                  [this] { TransformPlatformPerf(); });
+MOZ_GTEST_BENCH_F(GfxQcmsPerf_Rgba, TransformC, [this] { TransformPerf(); });
+MOZ_GTEST_BENCH_F(GfxQcmsPerf_Rgba, TransformPlatform,
+                  [this] { TransformPlatformPerf(); });
+MOZ_GTEST_BENCH_F(GfxQcmsPerf_Bgra, TransformC, [this] { TransformPerf(); });
+MOZ_GTEST_BENCH_F(GfxQcmsPerf_Bgra, TransformPlatform,
+                  [this] { TransformPlatformPerf(); });
diff --git a/gfx/tests/gtest/icc_profiles/lcms_samsung_syncmaster.icc b/gfx/tests/gtest/icc_profiles/lcms_samsung_syncmaster.icc
new file mode 100644
index 0000000000000000000000000000000000000000..3dcde88d062d84e9cbf2217b48c1cdebdb44af2e
GIT binary patch
literal 1944
zc$}S7O=uHA6#kk7+lu%{5fz0+deG8HewtW8)LQ@1QnZQ+Qn$O?G@4DqW@8&r@#sbH
z;GqZcAc6`W1wDuqK|JU|5WI;!dg?);2rBV=Gh-7pJ+!;bzV~Ln_syGcUIMgF7aYG5
z+X48+FgSJOfOYQt1*`otR$@K6(1XpkQz;J&51+uI&-^E}iTxNCuHPNu>+6Y!kLTXq
znB9A4=ECB6>RIi26$fbRrJr)jK}i1+{ace^S^Tf`yPOHzrT>e5Z@>b9$aa|@)BYau
zbJ`ykKNvYRNS`@an``pNntW#>@ac08bi2OGxyZEC;QB@J-_sxRz0d}tTY%U#zfg5d
z&2rjRUh&Ln+6^3Aa2hof;ou}}R1m^LAiza(3Myv=6UY;7p-glM<BST-w9M={XFV0m
znuROep@d0(E$&oeR*i@}a^wj)CurGQ<{a8QF-6Yo1msTty{pQ2I;AUH69d}M)$8AG
z1D$VypL6y4&yjll_Y<D|1u)au$Pd2&`-p3M)0pf24s3k{JiphNyGYIl)Ns@LvK`n9
zw;f5RweEbP@_DcykQO;P1Kj24z6oqzOt<A|oiaH#e4?w&%u%aDN@-_7EenHG%crI`
z5j7!k^G;~^)=9sR*3vb!Wodg%pLWrPCcbahpV6+QeMP&Cc9!-s4}V;*M<lO<c0%(;
zrf8`{<b~!_y(f0o;OBK5B`1HCsvog}cAMsp9;8jsh6Z;_^G2sl{M5uRP5i;c!oQLB
zhmK<%I*wT;PU!t(hfRFe#03=(@&*oL)O4>)ZAH(V>QbNT#;Lc*8R<k(y}}un`g&ZI
z&itD{rq*q;)RvKbRo#EZbbFfJ-p`TY>HvoM9>Wlh5a$wiRo%76D9?C`d2-t>ZG6cc
z5^vduaU8{N?s$S#Bw0<C{g=W))-u8|N?eIM1gZ*;J4+phxPFXdl<Y1m&+;8-Zju~X
zjy@{n@|#hN9{D}&;_GpykL&%k9y0`!By%%gNu9gUvepK!qT)Q)3N7nzW-M~2kbDE=
zYMvKWs$8>`W>xiX6f9ZQ24hZT3U3O%9EpXufeDm`qrAxkF<I7|rY`+F!T*?=!lado
z8FjAaz4SkAae*t%uW%*#w#I3`jM6DVn`y$0@sw57XPPa^n@JHNwY5Uk3@Y)rwm&8r
BH<bVY

diff --git a/gfx/tests/gtest/icc_profiles/lcms_thinkpad_w540.icc b/gfx/tests/gtest/icc_profiles/lcms_thinkpad_w540.icc
new file mode 100644
index 0000000000000000000000000000000000000000..c154e7e589f305da007add83d0404edcb90bd419
GIT binary patch
literal 1428
zc$}?N-%Aux6#iz_jFOO&1Tok`BEmIu)eH(+%_OK;Y$J<c8)s&Jlrv3tX4QR@9(s-l
zD)3FPw;p`xA$q95{(&IswbyzGq6osa@7!y;MN&iqXU=!d{mz+l&$)MiraM!PA2s#@
zeh{nivEk&zg^S7Nw`fEHt=NTD+lj(VHhT&iKA&H~68$nIxt5lD_O)MLP@mFwvHN-Z
z(T(Huv(5D)2e7({2c1yG#4hp8nK%^up192^+4LoZ->q06vGCg_4L&BgXmC!j%8ic@
zmxx=7b$GH4J0;~4&k}d}UThQ3Q$p7d_^qZE`m?U@3ce(@`BN2VwPx9Eix-?bOWi@~
zL=GjC5#TDau;C(!^B6!sq_4k^yHXbyksm_g8Va<g;L$eT0==HLWi8440nrTKNjO9y
zs$`T=WS*ElVMj#bbEwPY1oZR(GS^?{6&2Ebrm_~reSWFcK6V1lkAW3ct9_lX)m9dO
zhBv^|>+A9P9l(Jb3|e0Ib-e?&d;^{=uKN_@mghK*XQ8d^wTUOvY14Z?4f1+G$#I6#
zmiy%IrEHyZ%TBx+v03ItxkcGYS(H9fmSonH7nJ>!4`$ukWNfR~#94AaC>HZ#JN0d9
za;$r+{D<Zn)+6dY)UT<BS!jo`6Gy1~F@n=Lfjp+!W5u4D-lo~xJ`C~QyT+K*XTc_`
z%E`#Ye8!|0<8dajUM<o}@ub-F`8v~&Xmb)=RP+S@K7JI(agvg!?eg@cxiZ9B4zs>N
z9MsG{?qh(S0pbEJ(XPg_K8QnHlb8bE_4xvNV%9gRJsF<c4E+(mTVb`1KBF${a>>c?
z4rIwYgE4X>`?Nld9xY+aGQws?mpY~Ii}Whedk|idv4P&dImuOeMsyEC&8_}anRqI!
a&eq+k=o_H^CF=(Fv|r!T|CjHN|BLSwZ}OJ_

diff --git a/gfx/tests/gtest/moz.build b/gfx/tests/gtest/moz.build
--- a/gfx/tests/gtest/moz.build
+++ b/gfx/tests/gtest/moz.build
@@ -44,16 +44,24 @@ UNIFIED_SOURCES += [ '/gfx/2d/unittest/%
 # tests (e g. "error: 'ScriptListTableTest_TestSuccess_Test' has a field
 # 'ScriptListTableTest_TestSuccess_Test::<anonymous>' whose type uses
 # the anonymous namespace").
 SOURCES += [ '/gfx/ots/tests/%s' % p for p in [
     'cff_type2_charstring_test.cc',
     'layout_common_table_test.cc',
 ]]
 
+# ICC profiles used for verifying QCMS transformations. The copyright
+# notice embedded in the profiles should be reviewed to ensure there are
+# no known restrictions on distribution.
+TEST_HARNESS_FILES.gtest += [
+    'icc_profiles/lcms_samsung_syncmaster.icc',
+    'icc_profiles/lcms_thinkpad_w540.icc',
+]
+
 include('/ipc/chromium/chromium-config.mozbuild')
 
 LOCAL_INCLUDES += [
     '/gfx/2d',
     '/gfx/2d/unittest',
     '/gfx/config',
     '/gfx/layers',
     '/gfx/ots/src',
