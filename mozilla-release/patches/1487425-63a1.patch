# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1535731512 0
#      Fri Aug 31 16:05:12 2018 +0000
# Node ID 56d916d0bfb187bfffc634ea8b10d4f984cc7ed4
# Parent  8d89bfeb0b2f480a638e83a7d0178039033438f1
Bug 1487425 - [mozlint] Fix regression where 'roll' returns dict instead of ResultSummary when no files linted, r=Gijs

This is a regression from bug 1460856.

Differential Revision: https://phabricator.services.mozilla.com/D4759

diff --git a/python/mozlint/mozlint/roller.py b/python/mozlint/mozlint/roller.py
--- a/python/mozlint/mozlint/roller.py
+++ b/python/mozlint/mozlint/roller.py
@@ -173,18 +173,17 @@ class LintRoller(object):
 
     def roll(self, paths=None, outgoing=None, workdir=None, num_procs=None):
         """Run all of the registered linters against the specified file paths.
 
         :param paths: An iterable of files and/or directories to lint.
         :param outgoing: Lint files touched by commits that are not on the remote repository.
         :param workdir: Lint all files touched in the working directory.
         :param num_procs: The number of processes to use. Default: cpu count
-        :return: A dictionary with file names as the key, and a list of
-                 :class:`~result.Issue`s as the value.
+        :return: A :class:`~result.ResultSummary` instance.
         """
         if not self.linters:
             raise LintersNotConfigured
 
         self.result.reset()
 
         # Need to use a set in case vcs operations specify the same file
         # more than once.
@@ -210,17 +209,17 @@ class LintRoller(object):
                     print("warning: could not find default push, specify a remote for --outgoing")
         except CalledProcessError as e:
             print("error running: {}".format(' '.join(e.cmd)))
             if e.output:
                 print(e.output)
 
         if not (paths or vcs_paths) and (workdir or outgoing):
             print("warning: no files linted")
-            return {}
+            return self.result
 
         # Make sure all paths are absolute. Join `paths` to cwd and `vcs_paths` to root.
         paths = set(map(os.path.abspath, paths))
         vcs_paths = set([os.path.join(self.root, p) if not os.path.isabs(p) else p
                          for p in vcs_paths])
 
         num_procs = num_procs or cpu_count()
         jobs = list(self._generate_jobs(paths, vcs_paths, num_procs))
diff --git a/python/mozlint/test/test_roller.py b/python/mozlint/test/test_roller.py
--- a/python/mozlint/test/test_roller.py
+++ b/python/mozlint/test/test_roller.py
@@ -95,16 +95,28 @@ def test_roll_with_excluded_path(lint, l
 
     lint.read(linters)
     result = lint.roll(files)
 
     assert len(result.issues) == 0
     assert result.failed == set([])
 
 
+def test_roll_with_no_files_to_lint(lint, linters, capfd):
+    lint.read(linters)
+    lint.mock_vcs([])
+    result = lint.roll([], workdir=True)
+    assert isinstance(result, ResultSummary)
+    assert len(result.issues) == 0
+    assert len(result.failed) == 0
+
+    out, err = capfd.readouterr()
+    assert 'warning: no files linted' in out
+
+
 def test_roll_with_invalid_extension(lint, lintdir, filedir):
     lint.read(os.path.join(lintdir, 'external.yml'))
     result = lint.roll(os.path.join(filedir, 'foobar.py'))
     assert len(result.issues) == 0
     assert result.failed == set([])
 
 
 def test_roll_with_failure_code(lint, lintdir, files):
