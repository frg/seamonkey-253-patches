# HG changeset patch
# User Henrik Skupin <mail@hskupin.info>
# Date 1521466651 -3600
# Node ID fef8c2970f6a7c5b488fe7761be903fda45f2f93
# Parent  5082e91767c230418f71d3888346414b7f99d665
Bug 1441287 - [mozcrash] Convert unit tests to pytest. r=ahal

Switch to the pytest framework to benefit from its rich
feature set for creating Python test.

MozReview-Commit-ID: AoptjhT1Hln

diff --git a/testing/mozbase/mozcrash/tests/conftest.py b/testing/mozbase/mozcrash/tests/conftest.py
new file mode 100644
--- /dev/null
+++ b/testing/mozbase/mozcrash/tests/conftest.py
@@ -0,0 +1,88 @@
+from __future__ import absolute_import
+
+import uuid
+
+import pytest
+from py._path.common import fspath
+
+import mozcrash
+
+
+@pytest.fixture(scope='session')
+def stackwalk(tmpdir_factory):
+    stackwalk = tmpdir_factory.mktemp('stackwalk_binary').join('stackwalk')
+    stackwalk.write('fake binary')
+    stackwalk.chmod(0o744)
+    return stackwalk
+
+
+@pytest.fixture
+def check_for_crashes(tmpdir, stackwalk):
+
+    def wrapper(dump_directory=fspath(tmpdir),
+                symbols_path='symbols_path',
+                stackwalk_binary=fspath(stackwalk),
+                dump_save_path=None,
+                test_name=None,
+                quiet=True):
+        return mozcrash.check_for_crashes(dump_directory,
+                                          symbols_path,
+                                          stackwalk_binary,
+                                          dump_save_path,
+                                          test_name,
+                                          quiet)
+
+    return wrapper
+
+
+@pytest.fixture
+def check_for_java_exception():
+
+    def wrapper(logcat=None,
+                test_name=None,
+                quiet=True):
+        return mozcrash.check_for_java_exception(logcat,
+                                                 test_name,
+                                                 quiet)
+
+    return wrapper
+
+
+@pytest.fixture
+def minidump_files(request, tmpdir):
+    files = []
+
+    for i in range(getattr(request, 'param', 1)):
+        name = uuid.uuid4()
+
+        dmp = tmpdir.join('{}.dmp'.format(name))
+        dmp.write('foo')
+
+        extra = tmpdir.join('{}.extra'.format(name))
+        extra.write('bar')
+
+        files.append({'dmp': dmp, 'extra': extra})
+
+    return files
+
+
+@pytest.fixture(autouse=True)
+def mock_popen(monkeypatch):
+    """Generate a class that can mock subprocess.Popen.
+
+    :param stdouts: Iterable that should return an iterable for the
+                    stdout of each process in turn.
+    """
+    class MockPopen(object):
+        def __init__(self, args, *args_rest, **kwargs):
+            # all_popens.append(self)
+            self.args = args
+            self.returncode = 0
+
+        def communicate(self):
+            return (u'Stackwalk command: {}'.format(" ".join(self.args)), "")
+
+        def wait(self):
+            return self.returncode
+
+    monkeypatch.setattr(mozcrash.mozcrash.subprocess, 'Popen', MockPopen)
diff --git a/testing/mozbase/mozcrash/tests/test_basic.py b/testing/mozbase/mozcrash/tests/test_basic.py
--- a/testing/mozbase/mozcrash/tests/test_basic.py
+++ b/testing/mozbase/mozcrash/tests/test_basic.py
@@ -1,35 +1,21 @@
 #!/usr/bin/env python
 
 from __future__ import absolute_import
 
 import mozunit
-
-import mozcrash
-from testcase import CrashTestCase
+import pytest
 
 
-class TestBasic(CrashTestCase):
+def test_no_dump_files(check_for_crashes):
+    """Test that check_for_crashes returns 0 if no dumps are present."""
+    assert 0 == check_for_crashes()
 
-    def test_no_dump_files(self):
-        """Test that check_for_crashes returns 0 if no dumps are present."""
-        self.stdouts.append(["this is some output"])
-
-        self.assertEqual(0, mozcrash.check_for_crashes(self.tempdir,
-                                                       symbols_path='symbols_path',
-                                                       stackwalk_binary=self.stackwalk,
-                                                       quiet=True))
 
-    def test_dump_count(self):
-        """Test that check_for_crashes returns True if a dump is present."""
-        self.create_minidump("test1")
-        self.create_minidump("test2")
-        self.create_minidump("test3")
-
-        self.assertEqual(3, mozcrash.check_for_crashes(self.tempdir,
-                                                       symbols_path='symbols_path',
-                                                       stackwalk_binary=self.stackwalk,
-                                                       quiet=True))
+@pytest.mark.parametrize('minidump_files', [3], indirect=True)
+def test_dump_count(check_for_crashes, minidump_files):
+    """Test that check_for_crashes returns the number of crash dumps."""
+    assert 3 == check_for_crashes()
 
 
 if __name__ == '__main__':
     mozunit.main()
diff --git a/testing/mozbase/mozcrash/tests/test_java_exception.py b/testing/mozbase/mozcrash/tests/test_java_exception.py
--- a/testing/mozbase/mozcrash/tests/test_java_exception.py
+++ b/testing/mozbase/mozcrash/tests/test_java_exception.py
@@ -1,63 +1,45 @@
 #!/usr/bin/env python
-#
-# This Source Code Form is subject to the terms of the Mozilla Public
-# License, v. 2.0. If a copy of the MPL was not distributed with this file,
-# You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import
 
-import os
-import unittest
-
-import mozlog.unstructured as mozlog
 import mozunit
-
-import mozcrash
+import pytest
 
 
-# Make logs go away
-try:
-    log = mozlog.getLogger("mozcrash", handler=mozlog.FileHandler(os.devnull))
-except ValueError:
-    pass
+@pytest.fixture
+def test_log():
+    return [
+        "01-30 20:15:41.937 E/GeckoAppShell( 1703): >>> "
+        "REPORTING UNCAUGHT EXCEPTION FROM THREAD 9 (\"GeckoBackgroundThread\")",
+        "01-30 20:15:41.937 E/GeckoAppShell( 1703): java.lang.NullPointerException",
+        "01-30 20:15:41.937 E/GeckoAppShell( 1703):"
+        "    at org.mozilla.gecko.GeckoApp$21.run(GeckoApp.java:1833)",
+        "01-30 20:15:41.937 E/GeckoAppShell( 1703):"
+        "    at android.os.Handler.handleCallback(Handler.java:587)"
+    ]
 
 
-class TestJavaException(unittest.TestCase):
+def test_uncaught_exception(check_for_java_exception, test_log):
+    """Test for an exception which should be caught."""
+    assert 1 == check_for_java_exception(test_log)
+
 
-    def setUp(self):
-        self.test_log = [
-            "01-30 20:15:41.937 E/GeckoAppShell( 1703): >>> "
-            "REPORTING UNCAUGHT EXCEPTION FROM THREAD 9 (\"GeckoBackgroundThread\")",
-            "01-30 20:15:41.937 E/GeckoAppShell( 1703): java.lang.NullPointerException",
-            "01-30 20:15:41.937 E/GeckoAppShell( 1703):"
-            "    at org.mozilla.gecko.GeckoApp$21.run(GeckoApp.java:1833)",
-            "01-30 20:15:41.937 E/GeckoAppShell( 1703):"
-            "    at android.os.Handler.handleCallback(Handler.java:587)"]
-
-    def test_uncaught_exception(self):
-        """
-        Test for an exception which should be caught
-        """
-        self.assertEqual(1, mozcrash.check_for_java_exception(self.test_log, quiet=True))
+def test_truncated_exception(check_for_java_exception, test_log):
+    """Test for an exception which should be caught which was truncated."""
+    truncated_log = list(test_log)
+    truncated_log[0], truncated_log[1] = truncated_log[1], truncated_log[0]
 
-    def test_truncated_exception(self):
-        """
-        Test for an exception which should be caught which
-        was truncated
-        """
-        truncated_log = list(self.test_log)
-        truncated_log[0], truncated_log[1] = truncated_log[1], truncated_log[0]
-        self.assertEqual(1, mozcrash.check_for_java_exception(truncated_log, quiet=True))
+    assert 1 == check_for_java_exception(truncated_log)
+
 
-    def test_unchecked_exception(self):
-        """
-        Test for an exception which should not be caught
-        """
-        passable_log = list(self.test_log)
-        passable_log[0] = "01-30 20:15:41.937 E/GeckoAppShell( 1703):" \
-                          " >>> NOT-SO-BAD EXCEPTION FROM THREAD 9 (\"GeckoBackgroundThread\")"
-        self.assertEqual(0, mozcrash.check_for_java_exception(passable_log, quiet=True))
+def test_unchecked_exception(check_for_java_exception, test_log):
+    """Test for an exception which should not be caught."""
+    passable_log = list(test_log)
+    passable_log[0] = "01-30 20:15:41.937 E/GeckoAppShell( 1703):" \
+        " >>> NOT-SO-BAD EXCEPTION FROM THREAD 9 (\"GeckoBackgroundThread\")"
+
+    assert 0 == check_for_java_exception(passable_log)
 
 
 if __name__ == '__main__':
     mozunit.main()
diff --git a/testing/mozbase/mozcrash/tests/test_save_path.py b/testing/mozbase/mozcrash/tests/test_save_path.py
--- a/testing/mozbase/mozcrash/tests/test_save_path.py
+++ b/testing/mozbase/mozcrash/tests/test_save_path.py
@@ -1,65 +1,71 @@
 #!/usr/bin/env python
 
 from __future__ import absolute_import
 
 import os
 
 import mozunit
+import pytest
 
-import mozcrash
-from testcase import CrashTestCase
+from conftest import fspath
+
+
+def test_save_path_not_present(check_for_crashes, minidump_files, tmpdir):
+    """Test that dump_save_path works when the directory doesn't exist."""
+    save_path = tmpdir.join("saved")
+
+    assert 1 == check_for_crashes(dump_save_path=fspath(save_path))
+
+    assert save_path.join(minidump_files[0]["dmp"].basename).check()
+    assert save_path.join(minidump_files[0]["extra"].basename).check()
+
+
+def test_save_path(check_for_crashes, minidump_files, tmpdir):
+    """Test that dump_save_path works."""
+    save_path = tmpdir.mkdir("saved")
+
+    assert 1 == check_for_crashes(dump_save_path=fspath(save_path))
+
+    assert save_path.join(minidump_files[0]["dmp"].basename).check()
+    assert save_path.join(minidump_files[0]["extra"].basename).check()
 
 
-class TestSavePath(CrashTestCase):
+def test_save_path_isfile(check_for_crashes, minidump_files, tmpdir):
+    """Test that dump_save_path works when the path is a file and not a directory."""
+    save_path = tmpdir.join("saved")
+    save_path.write("junk")
 
-    def setUp(self):
-        super(TestSavePath, self).setUp()
-
-        self.create_minidump("test")
+    assert 1 == check_for_crashes(dump_save_path=fspath(save_path))
 
-    def check_for_saved_minidump_files(self, save_path=None):
-        self.assertEqual(1, mozcrash.check_for_crashes(self.tempdir,
-                                                       symbols_path='symbols_path',
-                                                       stackwalk_binary=self.stackwalk,
-                                                       dump_save_path=save_path,
-                                                       quiet=True))
-        if save_path is None:
-            save_path = os.environ.get('MINIDUMP_SAVE_PATH', None)
+    assert save_path.join(minidump_files[0]["dmp"].basename).check()
+    assert save_path.join(minidump_files[0]["extra"].basename).check()
+
 
-        self.assert_(os.path.isfile(os.path.join(save_path, "test.dmp")))
-        self.assert_(os.path.isfile(os.path.join(save_path, "test.extra")))
-
-    def test_save_path_not_present(self):
-        """Test that dump_save_path works when the directory doesn't exist."""
-        save_path = os.path.join(self.tempdir, "saved")
-
-        self.check_for_saved_minidump_files(save_path)
+def test_save_path_envvar(check_for_crashes, minidump_files, tmpdir):
+    """Test that the MINDUMP_SAVE_PATH environment variable works."""
+    save_path = tmpdir.mkdir("saved")
 
-    def test_save_path(self):
-        """Test that dump_save_path works."""
-        save_path = os.path.join(self.tempdir, "saved")
-        os.mkdir(save_path)
+    os.environ['MINIDUMP_SAVE_PATH'] = fspath(save_path)
+    try:
+        assert 1 == check_for_crashes(dump_save_path=None)
+    finally:
+        del os.environ['MINIDUMP_SAVE_PATH']
 
-        self.check_for_saved_minidump_files(save_path)
+    assert save_path.join(minidump_files[0]["dmp"].basename).check()
+    assert save_path.join(minidump_files[0]["extra"].basename).check()
 
-    def test_save_path_isfile(self):
-        """Test that dump_save_path works when the path is a file and not a directory."""
-        save_path = os.path.join(self.tempdir, "saved")
-        open(save_path, "w").write("junk")
 
-        self.check_for_saved_minidump_files(save_path)
+@pytest.mark.parametrize('minidump_files', [3], indirect=True)
+def test_save_multiple(check_for_crashes, minidump_files, tmpdir):
+    """Test that all minidumps are saved."""
+    save_path = tmpdir.mkdir("saved")
 
-    def test_save_path_envvar(self):
-        """Test that the MINDUMP_SAVE_PATH environment variable works."""
-        save_path = os.path.join(self.tempdir, "saved")
-        os.mkdir(save_path)
+    assert 3 == check_for_crashes(dump_save_path=fspath(save_path))
 
-        os.environ['MINIDUMP_SAVE_PATH'] = save_path
-        try:
-            self.check_for_saved_minidump_files()
-        finally:
-            del os.environ['MINIDUMP_SAVE_PATH']
+    for i in range(3):
+        assert save_path.join(minidump_files[i]["dmp"].basename).check()
+        assert save_path.join(minidump_files[i]["extra"].basename).check()
 
 
 if __name__ == '__main__':
     mozunit.main()
diff --git a/testing/mozbase/mozcrash/tests/test_stackwalk.py b/testing/mozbase/mozcrash/tests/test_stackwalk.py
--- a/testing/mozbase/mozcrash/tests/test_stackwalk.py
+++ b/testing/mozbase/mozcrash/tests/test_stackwalk.py
@@ -1,27 +1,22 @@
 #!/usr/bin/env python
 
 from __future__ import absolute_import
 
 import os
 
 import mozunit
 
-import mozcrash
-from testcase import CrashTestCase
+from conftest import fspath
 
 
-class TestStackwalk(CrashTestCase):
-
-    def test_stackwalk_envvar(self):
-        """Test that check_for_crashes uses the MINIDUMP_STACKWALK environment var."""
-        self.create_minidump("test.")
-
-        os.environ['MINIDUMP_STACKWALK'] = self.stackwalk
-        self.assertEqual(1, mozcrash.check_for_crashes(self.tempdir,
-                                                       symbols_path='symbols_path',
-                                                       quiet=True))
+def test_stackwalk_envvar(check_for_crashes, minidump_files, stackwalk):
+    """Test that check_for_crashes uses the MINIDUMP_STACKWALK environment var."""
+    os.environ['MINIDUMP_STACKWALK'] = fspath(stackwalk)
+    try:
+        assert 1 == check_for_crashes(stackwalk_binary=None)
+    finally:
         del os.environ['MINIDUMP_STACKWALK']
 
 
 if __name__ == '__main__':
     mozunit.main()
diff --git a/testing/mozbase/mozcrash/tests/test_symbols_path.py b/testing/mozbase/mozcrash/tests/test_symbols_path.py
--- a/testing/mozbase/mozcrash/tests/test_symbols_path.py
+++ b/testing/mozbase/mozcrash/tests/test_symbols_path.py
@@ -1,63 +1,48 @@
 #!/usr/bin/env python
 
 from __future__ import absolute_import
 
 import urlparse
 import zipfile
 import StringIO
 
+import mozhttpd
 import mozunit
 
-import mozcrash
-import mozhttpd
 
-from testcase import CrashTestCase
+def test_symbols_path_not_present(check_for_crashes, minidump_files):
+    """Test that no symbols path let mozcrash try to find the symbols."""
+    assert 1 == check_for_crashes(symbols_path=None)
 
 
-class TestCrash(CrashTestCase):
-
-    def test_symbol_path_not_present(self):
-        """Test that no symbols path doesn't process the minidump."""
-        self.create_minidump("test")
+def test_symbols_path_url(check_for_crashes, minidump_files):
+    """Test that passing a URL as symbols_path correctly fetches the URL."""
+    data = {"retrieved": False}
 
-        self.assertEqual(1, mozcrash.check_for_crashes(self.tempdir,
-                                                       symbols_path=None,
-                                                       stackwalk_binary=self.stackwalk,
-                                                       quiet=True))
+    def make_zipfile():
+        data = StringIO.StringIO()
+        z = zipfile.ZipFile(data, 'w')
+        z.writestr("symbols.txt", "abc/xyz")
+        z.close()
+        return data.getvalue()
 
-    def test_symbol_path_url(self):
-        """Test that passing a URL as symbols_path correctly fetches the URL."""
-        self.create_minidump("test")
-
-        data = {"retrieved": False}
+    def get_symbols(req):
+        data["retrieved"] = True
 
-        def make_zipfile():
-            data = StringIO.StringIO()
-            z = zipfile.ZipFile(data, 'w')
-            z.writestr("symbols.txt", "abc/xyz")
-            z.close()
-            return data.getvalue()
-
-        def get_symbols(req):
-            data["retrieved"] = True
-
-            headers = {}
-            return (200, headers, make_zipfile())
+        headers = {}
+        return (200, headers, make_zipfile())
 
-        httpd = mozhttpd.MozHttpd(port=0,
-                                  urlhandlers=[{'method': 'GET',
-                                                'path': '/symbols',
-                                                'function': get_symbols}])
-        httpd.start()
-        symbol_url = urlparse.urlunsplit(('http', '%s:%d' % httpd.httpd.server_address,
-                                          '/symbols', '', ''))
+    httpd = mozhttpd.MozHttpd(port=0,
+                              urlhandlers=[{'method': 'GET',
+                                            'path': '/symbols',
+                                            'function': get_symbols}])
+    httpd.start()
+    symbol_url = urlparse.urlunsplit(('http', '%s:%d' % httpd.httpd.server_address,
+                                      '/symbols', '', ''))
 
-        self.assertEqual(1, mozcrash.check_for_crashes(self.tempdir,
-                                                       symbols_path=symbol_url,
-                                                       stackwalk_binary=self.stackwalk,
-                                                       quiet=True))
-        self.assertTrue(data["retrieved"])
+    assert 1 == check_for_crashes(symbols_path=symbol_url)
+    assert data["retrieved"]
 
 
 if __name__ == '__main__':
     mozunit.main()
diff --git a/testing/mozbase/mozcrash/tests/testcase.py b/testing/mozbase/mozcrash/tests/testcase.py
deleted file mode 100644
--- a/testing/mozbase/mozcrash/tests/testcase.py
+++ /dev/null
@@ -1,69 +0,0 @@
-from __future__ import absolute_import
-
-import os
-import unittest
-import subprocess
-import tempfile
-import shutil
-
-import mozlog.unstructured as mozlog
-
-
-# Make logs go away
-try:
-    log = mozlog.getLogger("mozcrash", handler=mozlog.FileHandler(os.devnull))
-except ValueError:
-    pass
-
-
-def popen_factory(stdouts):
-    """Generate a class that can mock subprocess.Popen.
-
-    :param stdouts: Iterable that should return an iterable for the
-                    stdout of each process in turn.
-    """
-    class mock_popen(object):
-
-        def __init__(self, args, *args_rest, **kwargs):
-            self.stdout = stdouts.next()
-            self.returncode = 0
-
-        def wait(self):
-            return 0
-
-        def communicate(self):
-            return (self.stdout.next(), "")
-
-    return mock_popen
-
-
-class CrashTestCase(unittest.TestCase):
-
-    def setUp(self):
-        self.tempdir = tempfile.mkdtemp()
-
-        # a fake file to use as a stackwalk binary
-        self.stackwalk = os.path.join(self.tempdir, "stackwalk")
-        open(self.stackwalk, "w").write("fake binary")
-
-        # set mock for subprocess.Popen
-        self._subprocess_popen = subprocess.Popen
-        subprocess.Popen = popen_factory(self.next_mock_stdout())
-        self.stdouts = []
-
-    def tearDown(self):
-        subprocess.Popen = self._subprocess_popen
-        shutil.rmtree(self.tempdir)
-
-    def create_minidump(self, name):
-        open(os.path.join(self.tempdir, "{}.dmp".format(name)), "w").write("foo")
-        open(os.path.join(self.tempdir, "{}.extra".format(name)), "w").write("bar")
-
-        self.stdouts.append(["This is some output for {}".format(name)])
-
-    def next_mock_stdout(self):
-        if not self.stdouts:
-            yield iter([])
-
-        for s in self.stdouts:
-            yield iter(s)

