# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1559795693 0
# Node ID c529582f125023e0b6b78f3144b11870c344c69b
# Parent  1f2d92f1a12e94bdf40f00c3bb76bdc05132cf00
Bug 1551639 - Use .inc suffix for generated source files that are only included. r=nalexander

There is a big difference between generated source files that are built
directly, and those that are only included.

In the latter case, the build system won't know the files that does the
including depends on the generated source. So those sources do need to
be built during the export tier.

But in the former case, the build system has all the dependency
information it needs, and, while these generated sources are currently
built as part of the export tier, they don't actually need to be. We're
going to change that, and in preparation, we rename included files so as
to be more clearly identified.

Differential Revision: https://phabricator.services.mozilla.com/D33770

diff --git a/TODO.txt b/TODO.txt
--- a/TODO.txt
+++ b/TODO.txt
@@ -1,4 +1,5 @@
-1408481-58a1.patch       Fix gfx/2d/ScaledFontMac.cpp. Uncomment font_smoothing. Needs later webrender from 2017/10/27.
+1408481-58a1.patch       /gfx/2d/ScaledFontMac.cpp.                                      Uncomment font_smoothing. Needs later webrender from 2017/10/27.
 1444169-60a1.patch       /accessible/ipc/win/handler/HandlerDataCleanup.h                Dependent on Bug 1418448.
 1472894-1-63a1.patch     /dom/canvas/test/webgl-conf\mochitest-errata.ini
+1551639-1-69a1.patch     /layout/style/moz.build                                         Dependent on Bug 1471114.
 1678413-786.patch        /netwerk/protocol/viewsource/nsViewSourceChannel.cpp add mCallback  in ReleaseListeners(); if backporting Bug 1608224
diff --git a/config/check_spidermonkey_style.py b/config/check_spidermonkey_style.py
--- a/config/check_spidermonkey_style.py
+++ b/config/check_spidermonkey_style.py
@@ -56,17 +56,17 @@ ignored_js_src_dirs = [
 # We ignore #includes of these files, because they don't follow the usual rules.
 included_inclnames_to_ignore = set([
     'ffi.h',                    # generated in ctypes/libffi/
     'devtools/Instruments.h',   # we ignore devtools/ in general
     'double-conversion/double-conversion.h', # strange MFBT case
     'javascript-trace.h',       # generated in $OBJDIR if HAVE_DTRACE is defined
     'frontend/ReservedWordsGenerated.h',  # generated in $OBJDIR
     'gc/StatsPhasesGenerated.h',         # generated in $OBJDIR
-    'gc/StatsPhasesGenerated.cpp',       # generated in $OBJDIR
+    'gc/StatsPhasesGenerated.inc',       # generated in $OBJDIR
     'jscustomallocator.h',      # provided by embedders;  allowed to be missing
     'js-config.h',              # generated in $OBJDIR
     'fdlibm.h',                 # fdlibm
     'FuzzerDefs.h',             # included without a path
     'FuzzingInterface.h',       # included without a path
     'mozmemory.h',              # included without a path
     'pratom.h',                 # NSPR
     'prcvar.h',                 # NSPR
@@ -107,17 +107,17 @@ included_inclnames_to_ignore = set([
 ])
 
 # These files have additional constraints on where they are #included, so we
 # ignore #includes of them when checking #include ordering.
 oddly_ordered_inclnames = set([
     'ctypes/typedefs.h',        # Included multiple times in the body of ctypes/CTypes.h
     'frontend/ReservedWordsGenerated.h',
     'gc/StatsPhasesGenerated.h',         # Included in the body of gc/Statistics.h
-    'gc/StatsPhasesGenerated.cpp',       # Included in the body of gc/Statistics.cpp
+    'gc/StatsPhasesGenerated.inc',       # Included in the body of gc/Statistics.cpp
     'jswin.h',                  # Must be #included before <psapi.h>
     'machine/endian.h',         # Must be included after <sys/types.h> on BSD
     'winbase.h',                # Must precede other system headers(?)
     'windef.h'                  # Must precede other system headers(?)
 ])
 
 # The files in tests/style/ contain code that fails this checking in various
 # ways.  Here is the output we expect.  If the actual output differs from
diff --git a/js/src/gc/Statistics.cpp b/js/src/gc/Statistics.cpp
--- a/js/src/gc/Statistics.cpp
+++ b/js/src/gc/Statistics.cpp
@@ -121,17 +121,17 @@ struct PhaseInfo
 };
 
 // A table of PhaseInfo indexed by Phase.
 using PhaseTable = EnumeratedArray<Phase, Phase::LIMIT, PhaseInfo>;
 
 // A table of PhaseKindInfo indexed by PhaseKind.
 using PhaseKindTable = EnumeratedArray<PhaseKind, PhaseKind::LIMIT, PhaseKindInfo>;
 
-#include "gc/StatsPhasesGenerated.cpp"
+#include "gc/StatsPhasesGenerated.inc"
 
 static double
 t(TimeDuration duration)
 {
     return duration.ToMilliseconds();
 }
 
 inline Phase
diff --git a/js/src/gc/moz.build b/js/src/gc/moz.build
--- a/js/src/gc/moz.build
+++ b/js/src/gc/moz.build
@@ -14,20 +14,20 @@ LOCAL_INCLUDES += [
     '..'
 ]
 
 include('../js-config.mozbuild')
 include('../js-cxxflags.mozbuild')
 
 
 # Generate GC statistics phase data.
-GENERATED_FILES += ['StatsPhasesGenerated.h', 'StatsPhasesGenerated.cpp']
+GENERATED_FILES += ['StatsPhasesGenerated.h', 'StatsPhasesGenerated.inc']
 StatsPhasesGeneratedHeader = GENERATED_FILES['StatsPhasesGenerated.h']
 StatsPhasesGeneratedHeader.script = 'GenerateStatsPhases.py:generateHeader'
-StatsPhasesGeneratedCpp = GENERATED_FILES['StatsPhasesGenerated.cpp']
+StatsPhasesGeneratedCpp = GENERATED_FILES['StatsPhasesGenerated.inc']
 StatsPhasesGeneratedCpp.script = 'GenerateStatsPhases.py:generateCpp'
 
 UNIFIED_SOURCES += [
     'Allocator.cpp',
     'AtomMarking.cpp',
     'Barrier.cpp',
     'GCTrace.cpp',
     'Iteration.cpp',
diff --git a/layout/style/moz.build.1551639.later b/layout/style/moz.build.1551639.later
new file mode 100644
--- /dev/null
+++ b/layout/style/moz.build.1551639.later
@@ -0,0 +1,38 @@
+--- moz.build
++++ moz.build
+@@ -282,33 +282,33 @@ servo_props = GENERATED_FILES['ServoCSSP
+ servo_props.script = 'GenerateServoCSSPropList.py:generate_data'
+ servo_props.inputs = [
+     'ServoCSSPropList.mako.py',
+ ]
+ 
+ if CONFIG['COMPILE_ENVIRONMENT']:
+     GENERATED_FILES += [
+         'CompositorAnimatableProperties.h',
+-        'nsComputedDOMStyleGenerated.cpp',
++        'nsComputedDOMStyleGenerated.inc',
+         'nsCSSPropsGenerated.inc',
+         'ServoStyleConsts.h',
+     ]
+ 
+     EXPORTS.mozilla += [
+         '!CompositorAnimatableProperties.h',
+         '!ServoStyleConsts.h',
+     ]
+ 
+     compositor = GENERATED_FILES['CompositorAnimatableProperties.h']
+     compositor.script = 'GenerateCompositorAnimatableProperties.py:generate'
+     compositor.inputs = [
+         '!ServoCSSPropList.py',
+     ]
+ 
+-    computed = GENERATED_FILES['nsComputedDOMStyleGenerated.cpp']
++    computed = GENERATED_FILES['nsComputedDOMStyleGenerated.inc']
+     computed.script = 'GenerateComputedDOMStyleGenerated.py:generate'
+     computed.inputs = [
+         '!ServoCSSPropList.py',
+     ]
+ 
+     css_props = GENERATED_FILES['nsCSSPropsGenerated.inc']
+     css_props.script = 'GenerateCSSPropsGenerated.py:generate'
+     css_props.inputs = [
diff --git a/layout/style/nsComputedDOMStyle.cpp b/layout/style/nsComputedDOMStyle.cpp
--- a/layout/style/nsComputedDOMStyle.cpp
+++ b/layout/style/nsComputedDOMStyle.cpp
@@ -239,16 +239,21 @@ struct nsComputedStyleMap
   enum {
 #define COMPUTED_STYLE_PROP(prop_, method_) \
     eComputedStyleProperty_##prop_,
 #include "nsComputedDOMStylePropertyList.h"
 #undef COMPUTED_STYLE_PROP
     eComputedStyleProperty_COUNT
   };
 
+   // This generated file includes definition of kEntries which is typed
+   // Entry[] and used below, so this #include has to be put here.
+// -#include "nsComputedDOMStyleGenerated.cpp"
+// +#include "nsComputedDOMStyleGenerated.inc"
+
   /**
    * Returns the number of properties that should be exposed on an
    * nsComputedDOMStyle, ecxluding any disabled properties.
    */
   uint32_t Length()
   {
     Update();
     return mExposedPropertyCount;
