# HG changeset patch
# User Bobby Holley <bobbyholley@gmail.com>
# Date 1519805340 -7200
# Node ID 8d4be00621c3b155b1d4978ef7cbf631f957468c
# Parent  0403aaa462efbeb1b991a1785ecd6b942cade66a
Bug 1441896 - Pass an explicit parent SheetLoadData for child stylesheet loads. r=bz CLOSED TREE

MozReview-Commit-ID: 7XNu42NtITm

diff --git a/dom/base/nsTreeSanitizer.cpp b/dom/base/nsTreeSanitizer.cpp
--- a/dom/base/nsTreeSanitizer.cpp
+++ b/dom/base/nsTreeSanitizer.cpp
@@ -1114,23 +1114,24 @@ nsTreeSanitizer::SanitizeStyleSheet(cons
 #endif
   }
   sheet->SetURIs(aDocument->GetDocumentURI(), nullptr, aBaseURI);
   sheet->SetPrincipal(aDocument->NodePrincipal());
   if (aDocument->IsStyledByServo()) {
     rv = sheet->AsServo()->ParseSheet(
         aDocument->CSSLoader(), NS_ConvertUTF16toUTF8(aOriginal),
         aDocument->GetDocumentURI(), aBaseURI, aDocument->NodePrincipal(),
-        0, aDocument->GetCompatibilityMode());
+        /* aLoadData = */ nullptr, 0, aDocument->GetCompatibilityMode());
   } else {
 #ifdef MOZ_OLD_STYLE
     // Create the CSS parser, and parse the CSS text.
     nsCSSParser parser(nullptr, sheet->AsGecko());
-    rv = parser.ParseSheet(aOriginal, aDocument->GetDocumentURI(), aBaseURI,
-                           aDocument->NodePrincipal(), 0);
+    rv = parser.ParseSheet(aOriginal, aDocument->GetDocumentURI(),
+                           aBaseURI, aDocument->NodePrincipal(),
+                           /* aLoadData = */ nullptr, 0);
 #else
     MOZ_CRASH("old style system disabled");
 #endif
   }
   NS_ENSURE_SUCCESS(rv, true);
   // Mark the sheet as complete.
   MOZ_ASSERT(!sheet->IsModified(),
              "should not get marked modified during parsing");
diff --git a/layout/style/CSSStyleSheet.cpp b/layout/style/CSSStyleSheet.cpp
--- a/layout/style/CSSStyleSheet.cpp
+++ b/layout/style/CSSStyleSheet.cpp
@@ -931,17 +931,18 @@ CSSStyleSheet::ReparseSheet(const nsAStr
     nsCOMPtr<nsIStyleSheetLinkingElement> link = do_QueryInterface(mOwningNode);
     if (link) {
       lineNumber = link->GetLineNumber();
     }
   }
 
   nsCSSParser parser(loader, this);
   nsresult rv = parser.ParseSheet(aInput, mInner->mSheetURI, mInner->mBaseURI,
-                                  mInner->mPrincipal, lineNumber, &reusableSheets);
+                                  mInner->mPrincipal, /* aLoadData = */ nullptr,
+                                  lineNumber, &reusableSheets);
   DidDirty(); // we are always 'dirty' here since we always remove rules first
   NS_ENSURE_SUCCESS(rv, rv);
 
   // notify document of all new rules
   for (int32_t index = 0; index < Inner()->mOrderedRules.Count(); ++index) {
     RefPtr<css::Rule> rule = Inner()->mOrderedRules.ObjectAt(index);
     if (rule->GetType() == css::Rule::IMPORT_RULE &&
         RuleHasPendingChildSheet(rule)) {
diff --git a/layout/style/Loader.cpp b/layout/style/Loader.cpp
--- a/layout/style/Loader.cpp
+++ b/layout/style/Loader.cpp
@@ -1688,27 +1688,29 @@ Loader::ParseSheet(const nsAString& aUTF
 
   if (aLoadData->mSheet->IsGecko()) {
 #ifdef MOZ_OLD_STYLE
     nsCSSParser parser(this, aLoadData->mSheet->AsGecko());
     rv = parser.ParseSheet(aUTF16,
                            sheetURI,
                            baseURI,
                            aLoadData->mSheet->Principal(),
+                           aLoadData,
                            aLoadData->mLineNumber);
 #else
     MOZ_CRASH("old style system disabled");
 #endif
   } else {
     rv = aLoadData->mSheet->AsServo()->ParseSheet(
       this,
       aUTF8.IsEmpty() ? NS_ConvertUTF16toUTF8(aUTF16) : aUTF8,
       sheetURI,
       baseURI,
       aLoadData->mSheet->Principal(),
+      aLoadData,
       aLoadData->mLineNumber,
       GetCompatibilityMode());
   }
 
   mParsingDatas.RemoveElementAt(mParsingDatas.Length() - 1);
 
   if (NS_FAILED(rv)) {
     LOG_ERROR(("  Low-level error in parser!"));
@@ -2162,16 +2164,17 @@ HaveAncestorDataWithURI(SheetLoadData *a
     aData = aData->mNext;
   }
 
   return false;
 }
 
 nsresult
 Loader::LoadChildSheet(StyleSheet* aParentSheet,
+                       SheetLoadData* aParentData,
                        nsIURI* aURL,
                        dom::MediaList* aMedia,
                        ImportRule* aGeckoParentRule,
                        LoaderReusableStyleSheets* aReusableSheets)
 {
   LOG(("css::Loader::LoadChildSheet"));
   NS_PRECONDITION(aURL, "Must have a URI to load");
   NS_PRECONDITION(aParentSheet, "Must have a parent sheet");
@@ -2205,32 +2208,29 @@ Loader::LoadChildSheet(StyleSheet* aPare
     context = mDocument;
     loadingPrincipal = mDocument->NodePrincipal();
   }
 
   nsIPrincipal* principal = aParentSheet->Principal();
   nsresult rv = CheckContentPolicy(loadingPrincipal, principal, aURL, context, false);
   NS_ENSURE_SUCCESS(rv, rv);
 
-  SheetLoadData* parentData = nullptr;
   nsCOMPtr<nsICSSLoaderObserver> observer;
 
-  int32_t count = mParsingDatas.Length();
-  if (count > 0) {
+  if (aParentData) {
     LOG(("  Have a parent load"));
-    parentData = mParsingDatas.ElementAt(count - 1);
     // Check for cycles
-    if (HaveAncestorDataWithURI(parentData, aURL)) {
+    if (HaveAncestorDataWithURI(aParentData, aURL)) {
       // Houston, we have a loop, blow off this child and pretend this never
       // happened
       LOG_ERROR(("  @import cycle detected, dropping load"));
       return NS_OK;
     }
 
-    NS_ASSERTION(parentData->mSheet == aParentSheet,
+    NS_ASSERTION(aParentData->mSheet == aParentSheet,
                  "Unexpected call to LoadChildSheet");
   } else {
     LOG(("  No parent load; must be CSSOM"));
     // No parent load data, so the sheet will need to be notified when
     // we finish, if it can be, if we do the load asynchronously.
     observer = aParentSheet;
   }
 
@@ -2250,17 +2250,17 @@ Loader::LoadChildSheet(StyleSheet* aPare
   } else {
     IsAlternate isAlternate;
     const nsAString& empty = EmptyString();
     // For now, use CORS_NONE for child sheets
     rv = CreateSheet(aURL, nullptr, principal,
                      aParentSheet->ParsingMode(),
                      CORS_NONE, aParentSheet->GetReferrerPolicy(),
                      EmptyString(), // integrity is only checked on main sheet
-                     parentData ? parentData->mSyncLoad : false,
+                     aParentData ? aParentData->mSyncLoad : false,
                      false, empty, state, &isAlternate, &sheet);
     NS_ENSURE_SUCCESS(rv, rv);
 
     PrepareSheet(sheet, empty, empty, aMedia, nullptr, isAlternate);
   }
 
   rv = InsertChildSheet(sheet, aParentSheet, aGeckoParentRule);
   NS_ENSURE_SUCCESS(rv, rv);
@@ -2269,17 +2269,17 @@ Loader::LoadChildSheet(StyleSheet* aPare
     LOG(("  Sheet already complete"));
     // We're completely done.  No need to notify, even, since the
     // @import rule addition/modification will trigger the right style
     // changes automatically.
     return NS_OK;
   }
 
   nsCOMPtr<nsINode> requestingNode = do_QueryInterface(context);
-  SheetLoadData* data = new SheetLoadData(this, aURL, sheet, parentData,
+  SheetLoadData* data = new SheetLoadData(this, aURL, sheet, aParentData,
                                           observer, principal, requestingNode);
 
   NS_ADDREF(data);
   bool syncLoad = data->mSyncLoad;
 
   // Load completion will release the data
   rv = LoadSheet(data, state, false);
   NS_ENSURE_SUCCESS(rv, rv);
diff --git a/layout/style/Loader.h b/layout/style/Loader.h
--- a/layout/style/Loader.h
+++ b/layout/style/Loader.h
@@ -292,25 +292,28 @@ public:
    * there is no sheet currently being parsed and the child sheet is not
    * complete when this method returns, then when the child sheet becomes
    * complete aParentSheet will be QIed to nsICSSLoaderObserver and
    * asynchronously notified, just like for LoadStyleLink.  Note that if the
    * child sheet is already complete when this method returns, no
    * nsICSSLoaderObserver notification will be sent.
    *
    * @param aParentSheet the parent of this child sheet
+   * @param aParentData the SheetLoadData corresponding to the load of the
+   *                    parent sheet.
    * @param aURL the URL of the child sheet
    * @param aMedia the already-parsed media list for the child sheet
    * @param aGeckoParentRule the @import rule importing this child, when using
    *                         Gecko's style system. This is used to properly
    *                         order the child sheet list of aParentSheet.
    * @param aSavedSheets any saved style sheets which could be reused
    *              for this load
    */
   nsresult LoadChildSheet(StyleSheet* aParentSheet,
+                          SheetLoadData* aParentData,
                           nsIURI* aURL,
                           dom::MediaList* aMedia,
                           ImportRule* aGeckoParentRule,
                           LoaderReusableStyleSheets* aSavedSheets);
 
   /**
    * Synchronously load and return the stylesheet at aURL.  Any child sheets
    * will also be loaded synchronously.  Note that synchronous loads over some
diff --git a/layout/style/ServoBindingList.h b/layout/style/ServoBindingList.h
--- a/layout/style/ServoBindingList.h
+++ b/layout/style/ServoBindingList.h
@@ -46,16 +46,17 @@ SERVO_BINDING_FUNC(Servo_InvalidateStyle
                    const nsTArray<RawServoStyleSetBorrowed>* sets,
                    uint64_t aStatesChanged)
 
 // Styleset and Stylesheet management
 SERVO_BINDING_FUNC(Servo_StyleSheet_FromUTF8Bytes,
                    RawServoStyleSheetContentsStrong,
                    mozilla::css::Loader* loader,
                    mozilla::ServoStyleSheet* gecko_stylesheet,
+                   mozilla::css::SheetLoadData* load_data,
                    const uint8_t* data,
                    size_t data_len,
                    mozilla::css::SheetParsingMode parsing_mode,
                    RawGeckoURLExtraData* extra_data,
                    uint32_t line_number_offset,
                    nsCompatibility quirks_mode,
                    mozilla::css::LoaderReusableStyleSheets* reusable_sheets)
 SERVO_BINDING_FUNC(Servo_StyleSheet_Empty, RawServoStyleSheetContentsStrong,
diff --git a/layout/style/ServoBindings.cpp b/layout/style/ServoBindings.cpp
--- a/layout/style/ServoBindings.cpp
+++ b/layout/style/ServoBindings.cpp
@@ -2531,16 +2531,17 @@ Gecko_GetAppUnitsPerPhysicalInch(RawGeck
 {
   nsPresContext* presContext = const_cast<nsPresContext*>(aPresContext);
   return presContext->DeviceContext()->AppUnitsPerPhysicalInch();
 }
 
 ServoStyleSheet*
 Gecko_LoadStyleSheet(css::Loader* aLoader,
                      ServoStyleSheet* aParent,
+                     SheetLoadData* aParentLoadData,
                      css::LoaderReusableStyleSheets* aReusableSheets,
                      RawGeckoURLExtraData* aURLExtraData,
                      const uint8_t* aURLString,
                      uint32_t aURLStringLength,
                      RawServoMediaListStrong aMediaList)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(aLoader, "Should've catched this before");
@@ -2552,17 +2553,17 @@ Gecko_LoadStyleSheet(css::Loader* aLoade
   nsDependentCSubstring urlSpec(reinterpret_cast<const char*>(aURLString),
                                 aURLStringLength);
   nsCOMPtr<nsIURI> uri;
   nsresult rv = NS_NewURI(getter_AddRefs(uri), urlSpec, nullptr,
                           aURLExtraData->BaseURI());
 
   StyleSheet* previousFirstChild = aParent->GetFirstChild();
   if (NS_SUCCEEDED(rv)) {
-    rv = aLoader->LoadChildSheet(aParent, uri, media, nullptr, aReusableSheets);
+    rv = aLoader->LoadChildSheet(aParent, aParentLoadData, uri, media, nullptr, aReusableSheets);
   }
 
   if (NS_FAILED(rv) ||
       !aParent->GetFirstChild() ||
       aParent->GetFirstChild() == previousFirstChild) {
     // Servo and Gecko have different ideas of what a valid URL is, so we might
     // get in here with a URL string that NS_NewURI can't handle.  We may also
     // reach here via an import cycle.  For the import cycle case, we need some
diff --git a/layout/style/ServoBindings.h b/layout/style/ServoBindings.h
--- a/layout/style/ServoBindings.h
+++ b/layout/style/ServoBindings.h
@@ -8,16 +8,17 @@
 #define mozilla_ServoBindings_h
 
 #include <stdint.h>
 
 #include "mozilla/AtomArray.h"
 #include "mozilla/ServoTypes.h"
 #include "mozilla/ServoBindingTypes.h"
 #include "mozilla/ServoElementSnapshot.h"
+#include "mozilla/css/SheetLoadData.h"
 #include "mozilla/css/SheetParsingMode.h"
 #include "mozilla/css/URLMatchingFunction.h"
 #include "mozilla/EffectCompositor.h"
 #include "mozilla/ComputedTimingFunction.h"
 #include "nsChangeHint.h"
 #include "nsCSSPseudoClasses.h"
 #include "nsIDocument.h"
 #include "nsStyleStruct.h"
@@ -172,16 +173,17 @@ void Gecko_ServoStyleContext_Destroy(moz
 void Gecko_ConstructStyleChildrenIterator(RawGeckoElementBorrowed aElement,
                                           RawGeckoStyleChildrenIteratorBorrowedMut aIterator);
 void Gecko_DestroyStyleChildrenIterator(RawGeckoStyleChildrenIteratorBorrowedMut aIterator);
 RawGeckoNodeBorrowedOrNull Gecko_GetNextStyleChild(RawGeckoStyleChildrenIteratorBorrowedMut it);
 
 mozilla::ServoStyleSheet*
 Gecko_LoadStyleSheet(mozilla::css::Loader* loader,
                      mozilla::ServoStyleSheet* parent,
+                     mozilla::css::SheetLoadData* parent_load_data,
                      mozilla::css::LoaderReusableStyleSheets* reusable_sheets,
                      RawGeckoURLExtraData* base_url_data,
                      const uint8_t* url_bytes,
                      uint32_t url_length,
                      RawServoMediaListStrong media_list);
 
 // Selector Matching.
 uint64_t Gecko_ElementState(RawGeckoElementBorrowed element);
diff --git a/layout/style/ServoBindings.toml b/layout/style/ServoBindings.toml
--- a/layout/style/ServoBindings.toml
+++ b/layout/style/ServoBindings.toml
@@ -42,16 +42,17 @@ args = [
 headers = [
     "nsStyleStruct.h",
     "mozilla/ServoPropPrefList.h",
     "mozilla/StyleAnimationValue.h",
     "gfxFontConstants.h",
     "gfxFontFeatures.h",
     "nsThemeConstants.h",
     "mozilla/css/Loader.h",
+    "mozilla/css/SheetLoadData.h",
     "mozilla/dom/AnimationEffectReadOnlyBinding.h",
     "mozilla/dom/HTMLSlotElement.h",
     "mozilla/dom/KeyframeEffectBinding.h",
     "mozilla/AnimationPropertySegment.h",
     "mozilla/ComputedTiming.h",
     "mozilla/ComputedTimingFunction.h",
     "mozilla/Keyframe.h",
     "mozilla/ServoElementSnapshot.h",
@@ -210,16 +211,17 @@ whitelist-types = [
     "mozilla::SeenPtrs",
     "mozilla::ServoElementSnapshot.*",
     "mozilla::ServoStyleContext",
     "mozilla::ServoStyleSheetInner",
     "mozilla::ServoStyleSetSizes",
     "mozilla::CSSPseudoClassType",
     "mozilla::css::ErrorReporter",
     "mozilla::css::LoaderReusableStyleSheets",
+    "mozilla::css::SheetLoadData",
     "mozilla::css::SheetParsingMode",
     "mozilla::css::URLMatchingFunction",
     "mozilla::dom::IterationCompositeOperation",
     "mozilla::dom::StyleChildrenIterator",
     "mozilla::HalfCorner",
     "mozilla::MallocSizeOf",
     "mozilla::OriginFlags",
     "mozilla::PropertyStyleAnimationValuePair",
@@ -577,16 +579,17 @@ structs-types = [
     "nsStyleVisibility",
     "nsStyleXUL",
     "nsTimingFunction",
     "nscolor",
     "nscoord",
     "nsresult",
     "Loader",
     "LoaderReusableStyleSheets",
+    "SheetLoadData",
     "ServoStyleSheet",
     "ServoComputedData",
     "ServoStyleContext",
     "ServoStyleContextStrong",
     "EffectCompositor_CascadeLevel",
     "UpdateAnimationsTasks",
     "ParsingMode",
     "InheritTarget",
diff --git a/layout/style/ServoStyleSheet.cpp b/layout/style/ServoStyleSheet.cpp
--- a/layout/style/ServoStyleSheet.cpp
+++ b/layout/style/ServoStyleSheet.cpp
@@ -198,26 +198,28 @@ ServoStyleSheet::HasRules() const
 }
 
 nsresult
 ServoStyleSheet::ParseSheet(css::Loader* aLoader,
                             Span<const uint8_t> aInput,
                             nsIURI* aSheetURI,
                             nsIURI* aBaseURI,
                             nsIPrincipal* aSheetPrincipal,
+                            css::SheetLoadData* aLoadData,
                             uint32_t aLineNumber,
                             nsCompatibility aCompatMode,
                             css::LoaderReusableStyleSheets* aReusableSheets)
 {
   MOZ_ASSERT(!mMedia || mMedia->IsServo());
   RefPtr<URLExtraData> extraData =
     new URLExtraData(aBaseURI, aSheetURI, aSheetPrincipal);
 
   Inner()->mContents = Servo_StyleSheet_FromUTF8Bytes(aLoader,
                                                       this,
+                                                      aLoadData,
                                                       aInput.Elements(),
                                                       aInput.Length(),
                                                       mParsingMode,
                                                       extraData,
                                                       aLineNumber,
                                                       aCompatMode,
                                                       aReusableSheets)
                          .Consume();
@@ -300,16 +302,17 @@ ServoStyleSheet::ReparseSheet(const nsAS
 
   DropRuleList();
 
   nsresult rv = ParseSheet(loader,
                            NS_ConvertUTF16toUTF8(aInput),
                            mInner->mSheetURI,
                            mInner->mBaseURI,
                            mInner->mPrincipal,
+                           /* aLoadData = */ nullptr,
                            lineNumber,
                            eCompatibility_FullStandards,
                            &reusableSheets);
   DidDirty();
   NS_ENSURE_SUCCESS(rv, rv);
 
   // Notify the stylesets about the new rules.
   {
diff --git a/layout/style/ServoStyleSheet.h b/layout/style/ServoStyleSheet.h
--- a/layout/style/ServoStyleSheet.h
+++ b/layout/style/ServoStyleSheet.h
@@ -18,16 +18,17 @@
 
 namespace mozilla {
 
 class ServoCSSRuleList;
 
 namespace css {
 class Loader;
 class LoaderReusableStyleSheets;
+class SheetLoadData;
 }
 
 // -------------------------------
 // Servo Style Sheet Inner Data Container
 //
 
 struct ServoStyleSheetInner final : public StyleSheetInfo
 {
@@ -79,22 +80,25 @@ public:
 
   NS_DECL_ISUPPORTS_INHERITED
   NS_DECL_CYCLE_COLLECTION_CLASS_INHERITED(ServoStyleSheet, StyleSheet)
 
   NS_DECLARE_STATIC_IID_ACCESSOR(NS_SERVO_STYLE_SHEET_IMPL_CID)
 
   bool HasRules() const;
 
+  // Parses a stylesheet. The aLoadData argument corresponds to the
+  // SheetLoadData for this stylesheet. It may be null in some cases.
   MOZ_MUST_USE nsresult
   ParseSheet(css::Loader* aLoader,
              Span<const uint8_t> aInput,
              nsIURI* aSheetURI,
              nsIURI* aBaseURI,
              nsIPrincipal* aSheetPrincipal,
+             css::SheetLoadData* aLoadData,
              uint32_t aLineNumber,
              nsCompatibility aCompatMode,
              css::LoaderReusableStyleSheets* aReusableSheets = nullptr);
 
   nsresult ReparseSheet(const nsAString& aInput);
 
   const RawServoStyleSheetContents* RawContents() const {
     return Inner()->mContents;
diff --git a/layout/style/nsCSSParser.cpp b/layout/style/nsCSSParser.cpp
--- a/layout/style/nsCSSParser.cpp
+++ b/layout/style/nsCSSParser.cpp
@@ -138,16 +138,17 @@ public:
 
   // Clears everything set by the above Set*() functions.
   void Reset();
 
   nsresult ParseSheet(const nsAString& aInput,
                       nsIURI*          aSheetURI,
                       nsIURI*          aBaseURI,
                       nsIPrincipal*    aSheetPrincipal,
+                      SheetLoadData*   aLoadData,
                       uint32_t         aLineNumber,
                       css::LoaderReusableStyleSheets* aReusableSheets);
 
   already_AddRefed<css::Declaration>
            ParseStyleAttribute(const nsAString&  aAttributeValue,
                                nsIURI*           aDocURL,
                                nsIURI*           aBaseURL,
                                nsIPrincipal*     aNodePrincipal);
@@ -1293,16 +1294,19 @@ protected:
   nsCOMPtr<nsIURI> mSheetURI;
 
   // The principal of the sheet involved
   nsCOMPtr<nsIPrincipal> mSheetPrincipal;
 
   // The sheet we're parsing into
   RefPtr<CSSStyleSheet> mSheet;
 
+  // The data describing this load, if applicable.
+  RefPtr<SheetLoadData> mLoadData;
+
   // Used for @import rules
   css::Loader* mChildLoader; // not ref counted, it owns us
 
   // Any sheets we may reuse when parsing an @import.
   css::LoaderReusableStyleSheets* mReusableSheets;
 
   // Sheet section we're in.  This is used to enforce correct ordering of the
   // various rule types (eg the fact that a @charset rule must come before
@@ -1563,16 +1567,17 @@ CSSParserImpl::ReleaseScanner()
   mSheetPrincipal = nullptr;
 }
 
 nsresult
 CSSParserImpl::ParseSheet(const nsAString& aInput,
                           nsIURI*          aSheetURI,
                           nsIURI*          aBaseURI,
                           nsIPrincipal*    aSheetPrincipal,
+                          SheetLoadData*   aLoadData,
                           uint32_t         aLineNumber,
                           css::LoaderReusableStyleSheets* aReusableSheets)
 {
   NS_PRECONDITION(aSheetPrincipal, "Must have principal here!");
   NS_PRECONDITION(aBaseURI, "need base URI");
   NS_PRECONDITION(aSheetURI, "need sheet URI");
   NS_PRECONDITION(mSheet, "Must have sheet to parse into");
   NS_ENSURE_STATE(mSheet);
@@ -1586,16 +1591,17 @@ CSSParserImpl::ParseSheet(const nsAStrin
                                                         &equal)) &&
                equal,
                "Sheet principal does not match passed principal");
 #endif
 
   nsCSSScanner scanner(aInput, aLineNumber);
   css::ErrorReporter reporter(scanner, mSheet, mChildLoader, aSheetURI);
   InitScanner(scanner, reporter, aSheetURI, aBaseURI, aSheetPrincipal);
+  mLoadData = aLoadData;
 
   int32_t ruleCount = mSheet->StyleRuleCount();
   if (0 < ruleCount) {
     const css::Rule* lastRule = mSheet->GetStyleRuleAt(ruleCount - 1);
     if (lastRule) {
       switch (lastRule->GetType()) {
         case css::Rule::CHARSET_RULE:
         case css::Rule::IMPORT_RULE:
@@ -1635,16 +1641,17 @@ CSSParserImpl::ParseSheet(const nsAStrin
     UngetToken();
     if (ParseRuleSet(AppendRuleToSheet, this)) {
       mSection = eCSSSection_General;
     }
   }
 
   mSheet->SetSourceMapURLFromComment(scanner.GetSourceMapURL());
   mSheet->SetSourceURL(scanner.GetSourceURL());
+  mLoadData = nullptr;
   ReleaseScanner();
 
   mParsingMode = css::eAuthorSheetFeatures;
   mIsChrome = false;
   mReusableSheets = nullptr;
 
   return NS_OK;
 }
@@ -3686,17 +3693,17 @@ CSSParserImpl::ProcessImport(const nsStr
       // import url is bad
       REPORT_UNEXPECTED_P(PEImportBadURI, aURLSpec);
       OUTPUT_ERROR();
     }
     return;
   }
 
   if (mChildLoader) {
-    mChildLoader->LoadChildSheet(mSheet, url, aMedia, rule, mReusableSheets);
+    mChildLoader->LoadChildSheet(mSheet, mLoadData, url, aMedia, rule, mReusableSheets);
   }
 }
 
 // Parse the {} part of an @media or @-moz-document rule.
 bool
 CSSParserImpl::ParseGroupRule(css::GroupRule* aRule,
                               RuleAppendFunc aAppendFunc,
                               void* aData)
@@ -17957,22 +17964,23 @@ nsCSSParser::Shutdown()
 
 // Wrapper methods
 
 nsresult
 nsCSSParser::ParseSheet(const nsAString& aInput,
                         nsIURI*          aSheetURI,
                         nsIURI*          aBaseURI,
                         nsIPrincipal*    aSheetPrincipal,
+                        SheetLoadData*   aLoadData,
                         uint32_t         aLineNumber,
                         css::LoaderReusableStyleSheets* aReusableSheets)
 {
   return static_cast<CSSParserImpl*>(mImpl)->
-    ParseSheet(aInput, aSheetURI, aBaseURI, aSheetPrincipal, aLineNumber,
-               aReusableSheets);
+    ParseSheet(aInput, aSheetURI, aBaseURI, aSheetPrincipal, aLoadData,
+               aLineNumber, aReusableSheets);
 }
 
 already_AddRefed<css::Declaration>
 nsCSSParser::ParseStyleAttribute(const nsAString&  aAttributeValue,
                                  nsIURI*           aDocURI,
                                  nsIURI*           aBaseURI,
                                  nsIPrincipal*     aNodePrincipal)
 {
diff --git a/layout/style/nsCSSParser.h b/layout/style/nsCSSParser.h
--- a/layout/style/nsCSSParser.h
+++ b/layout/style/nsCSSParser.h
@@ -8,16 +8,17 @@
 
 #ifndef nsCSSParser_h___
 #define nsCSSParser_h___
 
 #ifdef MOZ_OLD_STYLE
 
 #include "mozilla/Attributes.h"
 #include "mozilla/css/Loader.h"
+#include "mozilla/css/SheetLoadData.h"
 
 #include "nsCSSPropertyID.h"
 #include "nsCSSScanner.h"
 #include "nsCOMPtr.h"
 #include "nsAutoPtr.h"
 #include "nsStringFwd.h"
 #include "nsTArrayForwardDeclare.h"
 
@@ -69,24 +70,26 @@ public:
    * @param aInput the data to parse
    * @param aSheetURL the URI to use as the sheet URI (for error reporting).
    *                  This must match the URI of the sheet passed to
    *                  the constructor.
    * @param aBaseURI the URI to use for relative URI resolution
    * @param aSheetPrincipal the principal of the stylesheet.  This must match
    *                        the principal of the sheet passed to the
    *                        constructor.
+   * @param aLoadData the SheetLoadData for this sheet, if applicable.
    * @param aLineNumber the line number of the first line of the sheet.
    * @param aReusableSheets style sheets that can be reused by an @import.
    *                        This can be nullptr.
    */
   nsresult ParseSheet(const nsAString& aInput,
                       nsIURI*          aSheetURL,
                       nsIURI*          aBaseURI,
                       nsIPrincipal*    aSheetPrincipal,
+                      mozilla::css::SheetLoadData* aLoadData,
                       uint32_t         aLineNumber,
                       mozilla::css::LoaderReusableStyleSheets* aReusableSheets =
                         nullptr);
 
   // Parse HTML style attribute or its equivalent in other markup
   // languages.  aBaseURL is the base url to use for relative links in
   // the declaration.
   already_AddRefed<mozilla::css::Declaration>
diff --git a/layout/style/nsLayoutStylesheetCache.cpp b/layout/style/nsLayoutStylesheetCache.cpp
--- a/layout/style/nsLayoutStylesheetCache.cpp
+++ b/layout/style/nsLayoutStylesheetCache.cpp
@@ -988,17 +988,17 @@ nsLayoutStylesheetCache::BuildPreference
     sheet->AsGecko()->ReparseSheet(NS_ConvertUTF8toUTF16(sheetText));
 #else
     MOZ_CRASH("old style system disabled");
 #endif
   } else {
     ServoStyleSheet* servoSheet = sheet->AsServo();
     // NB: The pref sheet never has @import rules.
     nsresult rv = servoSheet->ParseSheet(
-      nullptr, sheetText, uri, uri, nullptr, 0, eCompatibility_FullStandards);
+      nullptr, sheetText, uri, uri, nullptr, /* aLoadData = */ nullptr, 0, eCompatibility_FullStandards);
     // Parsing the about:PreferenceStyleSheet URI can only fail on OOM. If we
     // are OOM before we parsed any documents we might as well abort.
     MOZ_RELEASE_ASSERT(NS_SUCCEEDED(rv));
   }
 
 #undef NS_GET_R_G_B
 }
 
diff --git a/layout/style/test/gtest/StyloParsingBench.cpp b/layout/style/test/gtest/StyloParsingBench.cpp
--- a/layout/style/test/gtest/StyloParsingBench.cpp
+++ b/layout/style/test/gtest/StyloParsingBench.cpp
@@ -29,16 +29,17 @@ static void ServoParsingBench() {
   ASSERT_EQ(Encoding::UTF8ValidUpTo(css), css.Length());
 
   RefPtr<URLExtraData> data = new URLExtraData(
     NullPrincipalURI::Create(), nullptr, NullPrincipal::Create());
   for (int i = 0; i < PARSING_REPETITIONS; i++) {
     RefPtr<RawServoStyleSheetContents> stylesheet =
       Servo_StyleSheet_FromUTF8Bytes(nullptr,
                                      nullptr,
+                                     nullptr,
                                      css.Elements(),
                                      css.Length(),
                                      eAuthorSheetFeatures,
                                      data,
                                      0,
                                      eCompatibility_FullStandards,
                                      nullptr)
         .Consume();
