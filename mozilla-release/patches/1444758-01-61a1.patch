# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1520797616 25200
# Node ID 6390ec7df035a339d043cdd6079b416dacec19d7
# Parent  960fba7e986687724fa3407a9113cc2f9f4462c5
Bug 1444758: Part 1: Add console message matching helper to AddonTestUtils. r=aswan

Several of our existing plain mochitests use the message matching features of
SimpleTest.monitorConsole. Having a similar utility for xpcshell tests would
make it much easier to migrate these tests to xpcshell.

MozReview-Commit-ID: 38pPanhN5Iu

diff --git a/toolkit/mozapps/extensions/internal/AddonTestUtils.jsm b/toolkit/mozapps/extensions/internal/AddonTestUtils.jsm
--- a/toolkit/mozapps/extensions/internal/AddonTestUtils.jsm
+++ b/toolkit/mozapps/extensions/internal/AddonTestUtils.jsm
@@ -68,16 +68,19 @@ const RDFDataSource = Components.Constru
 const ZipReader = Components.Constructor(
   "@mozilla.org/libjar/zip-reader;1",
   "nsIZipReader", "open");
 
 const ZipWriter = Components.Constructor(
   "@mozilla.org/zipwriter;1",
   "nsIZipWriter", "open");
 
+function isRegExp(val) {
+  return val && typeof val === "object" && typeof val.test === "function";
+}
 
 // We need some internal bits of AddonManager
 var AMscope = ChromeUtils.import("resource://gre/modules/AddonManager.jsm", {});
 var {AddonManager, AddonManagerPrivate} = AMscope;
 
 
 // Mock out AddonManager's reference to the AsyncShutdown module so we can shut
 // down AddonManager from the test
@@ -360,16 +363,22 @@ var AddonTestUtils = {
       try {
         this.tempDir.remove(true);
       } catch (e) {
         Cu.reportError(e);
       }
     });
   },
 
+  info(msg) {
+    // info() for mochitests, do_print for xpcshell.
+    let print = this.testScope.info || this.testScope.do_print;
+    print(msg);
+  },
+
   cleanupTempXPIs() {
     for (let file of this.tempXPIs.splice(0)) {
       if (file.exists()) {
         try {
           file.remove(false);
         } catch (e) {
           Cu.reportError(e);
         }
@@ -1245,16 +1254,82 @@ var AddonTestUtils = {
 
       return {messages, result};
     } finally {
       Services.console.unregisterListener(listener);
     }
   },
 
   /**
+   * An object describing an expected or forbidden console message. Each
+   * property in the object corresponds to a property with the same name
+   * in a console message. If the value in the pattern object is a
+   * regular expression, it must match the value of the corresponding
+   * console message property. If it is any other value, it must be
+   * strictly equal to the correspondng console message property.
+   *
+   * @typedef {object} ConsoleMessagePattern
+   */
+
+  /**
+   * Checks the list of messages returned from `promiseConsoleOutput`
+   * against the given set of expected messages.
+   *
+   * This is roughly equivalent to the expected and forbidden message
+   * matching functionality of SimpleTest.monitorConsole.
+   *
+   * @param {Array<object>} messages
+   *        The array of console messages to match.
+   * @param {object} options
+   *        Options describing how to perform the match.
+   * @param {Array<ConsoleMessagePattern>} [options.expected = []]
+   *        An array of messages which must appear in `messages`. The
+   *        matching messages in the `messages` array must appear in the
+   *        same order as the patterns in the `expected` array.
+   * @param {Array<ConsoleMessagePattern>} [options.forbidden = []]
+   *        An array of messages which must not appear in the `messages`
+   *        array.
+   * @param {bool} [options.forbidUnexpected = false]
+   *        If true, the `messages` array must not contain any messages
+   *        which are not matched by the given `expected` patterns.
+   */
+  checkMessages(messages, {expected = [], forbidden = [], forbidUnexpected = false}) {
+    function msgMatches(msg, expectedMsg) {
+      for (let [prop, pattern] of Object.entries(expectedMsg)) {
+        if (isRegExp(pattern) && typeof msg[prop] === "string") {
+          if (!pattern.test(msg[prop])) {
+            return false;
+          }
+        } else if (msg[prop] !== pattern) {
+          return false;
+        }
+      }
+      return true;
+    }
+
+    let i = 0;
+    for (let msg of messages) {
+      if (forbidden.some(pat => msgMatches(msg, pat))) {
+        this.testScope.ok(false, `Got forbidden console message: ${msg}`);
+        continue;
+      }
+
+      if (i < expected.length && msgMatches(msg, expected[i])) {
+        this.info(`Matched expected console message: ${msg}`);
+        i++;
+      } else if (forbidUnexpected) {
+        this.testScope.ok(false, `Got unexpected console message: ${msg}`);
+      }
+    }
+    for (let pat of expected.slice(i)) {
+      this.testScope.ok(false, `Did not get expected console message: ${uneval(pat)}`);
+    }
+  },
+
+  /**
    * Helper to wait for a webextension to completely start
    *
    * @param {string} [id]
    *        An optional extension id to look for.
    *
    * @returns {Promise<Extension>}
    *           A promise that resolves with the extension, once it is started.
    */
