# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1522355969 14400
# Node ID 9132dc1bb4ee4faae34bddecd1342f3a13122d2e
# Parent  88d9cc34c66f32fac40e5a98686344e2379d2e9e
Bug 1373368 - [lint] Ignore SIGINT in lint hooks, r=standard8

This defers SIGINT handling to the underlying mozlint subprocess. Mozlint *should*
return quickly and gracefully. This way the hook process itself will not abort, just
the linting. After hitting Ctrl-C, the vcs operation should continue as if the lint
were successful.

If mozlint had already found some errors before hitting Ctrl-C, those should get
printed and could potentially abort the hook.

MozReview-Commit-ID: BKXq1dXuMlB

diff --git a/tools/lint/hooks.py b/tools/lint/hooks.py
--- a/tools/lint/hooks.py
+++ b/tools/lint/hooks.py
@@ -1,33 +1,44 @@
 #!/usr/bin/env python
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 import os
+import signal
 import subprocess
 import sys
 from distutils.spawn import find_executable
 
 here = os.path.dirname(os.path.realpath(__file__))
 topsrcdir = os.path.join(here, os.pardir, os.pardir)
 
 
+def run_process(cmd):
+    proc = subprocess.Popen(cmd)
+
+    # ignore SIGINT, the mozlint subprocess should exit quickly and gracefully
+    orig_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
+    proc.wait()
+    signal.signal(signal.SIGINT, orig_handler)
+    return proc.returncode
+
+
 def run_mozlint(hooktype, args):
     # --quiet prevents warnings on eslint, it will be ignored by other linters
     python = find_executable('python2.7') or find_executable('python')
     cmd = [python, os.path.join(topsrcdir, 'mach'), 'lint', '--quiet']
 
     if 'commit' in hooktype:
         # don't prevent commits, just display the lint results
-        subprocess.call(cmd + ['--workdir=staged'])
+        run_process(cmd + ['--workdir=staged'])
         return False
     elif 'push' in hooktype:
-        return subprocess.call(cmd + ['--outgoing'] + args)
+        return run_process(cmd + ['--outgoing'] + args)
 
     print("warning: '{}' is not a valid mozlint hooktype".format(hooktype))
     return False
 
 
 def hg(ui, repo, **kwargs):
     hooktype = kwargs['hooktype']
     return run_mozlint(hooktype, kwargs.get('pats', []))
