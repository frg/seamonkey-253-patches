# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1582820541 0
# Node ID fdfedcfa512d569456e52c409339dbc21e265ee0
# Parent  85cf2250b11d92a3f68e238d01b62c66221239b8
Bug 1617303 - mozbuild/backend/recursivemake.py and /fastermake.py support Python 3 r=firefox-build-system-reviewers,mshal

Differential Revision: https://phabricator.services.mozilla.com/D63880

diff --git a/python/mozbuild/mozbuild/backend/base.py b/python/mozbuild/mozbuild/backend/base.py
--- a/python/mozbuild/mozbuild/backend/base.py
+++ b/python/mozbuild/mozbuild/backend/base.py
@@ -8,16 +8,17 @@ from abc import (
     ABCMeta,
     abstractmethod,
 )
 
 import errno
 import io
 import itertools
 import os
+import six
 import time
 
 from contextlib import contextmanager
 
 from mach.mixin.logging import LoggingMixin
 
 import mozpack.path as mozpath
 from ..preprocessor import Preprocessor
@@ -307,17 +308,17 @@ class BuildBackend(LoggingMixin):
     def _get_preprocessor(self, obj):
         '''Returns a preprocessor with a few predefined values depending on
         the given BaseConfigSubstitution(-like) object, and all the substs
         in the current environment.'''
         pp = Preprocessor()
         srcdir = mozpath.dirname(obj.input_path)
         pp.context.update({
             k: ' '.join(v) if isinstance(v, list) else v
-            for k, v in obj.config.substs.iteritems()
+            for k, v in six.iteritems(obj.config.substs)
         })
         pp.context.update(
             top_srcdir=obj.topsrcdir,
             topobjdir=obj.topobjdir,
             srcdir=srcdir,
             srcdir_rel=mozpath.relpath(srcdir, mozpath.dirname(obj.output_path)),
             relativesrcdir=mozpath.relpath(srcdir, obj.topsrcdir) or '.',
             DEPTH=mozpath.relpath(obj.topobjdir, mozpath.dirname(obj.output_path)) or '.',
diff --git a/python/mozbuild/mozbuild/backend/common.py b/python/mozbuild/mozbuild/backend/common.py
--- a/python/mozbuild/mozbuild/backend/common.py
+++ b/python/mozbuild/mozbuild/backend/common.py
@@ -2,16 +2,17 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import itertools
 import json
 import os
+import six
 
 import mozpack.path as mozpath
 
 from mozbuild.backend.base import BuildBackend
 
 from mozbuild.frontend.context import (
     Context,
     ObjDirPath,
diff --git a/python/mozbuild/mozbuild/backend/fastermake.py b/python/mozbuild/mozbuild/backend/fastermake.py
--- a/python/mozbuild/mozbuild/backend/fastermake.py
+++ b/python/mozbuild/mozbuild/backend/fastermake.py
@@ -1,14 +1,16 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, unicode_literals, print_function
 
+import six
+
 from mozbuild.backend.base import PartialBackend
 from mozbuild.backend.common import CommonBackend
 from mozbuild.frontend.context import (
     ObjDirPath,
 )
 from mozbuild.frontend.data import (
     ChromeManifestEntry,
     FinalTargetPreprocessedFiles,
@@ -175,40 +177,40 @@ class FasterMakeBackend(CommonBackend, P
             if value is not None:
                 mk.add_statement('%s = %s' % (var, value))
 
         install_manifests_bases = self._install_manifests.keys()
 
         # Add information for chrome manifest generation
         manifest_targets = []
 
-        for target, entries in self._manifest_entries.iteritems():
+        for target, entries in six.iteritems(self._manifest_entries):
             manifest_targets.append(target)
             install_target = mozpath.basedir(target, install_manifests_bases)
             self._install_manifests[install_target].add_content(
                 ''.join('%s\n' % e for e in sorted(entries)),
                 mozpath.relpath(target, install_target))
 
         # Add information for install manifests.
         mk.add_statement('INSTALL_MANIFESTS = %s'
                          % ' '.join(self._install_manifests.keys()))
 
         # Add dependencies we inferred:
-        for target, deps in self._dependencies.iteritems():
+        for target, deps in six.iteritems(self._dependencies):
             mk.create_rule([target]).add_dependencies(
                 '$(TOPOBJDIR)/%s' % d for d in deps)
 
         # This is not great, but it's better to have some dependencies on these Python files.
         python_deps = [
             '$(TOPSRCDIR)/python/mozbuild/mozbuild/action/l10n_merge.py',
             '$(TOPSRCDIR)/third_party/python/compare-locales/compare_locales/compare.py',
             '$(TOPSRCDIR)/third_party/python/compare-locales/compare_locales/paths.py',
         ]
         # Add l10n dependencies we inferred:
-        for target, deps in self._l10n_dependencies.iteritems():
+        for target, deps in six.iteritems(self._l10n_dependencies):
             mk.create_rule([target]).add_dependencies(
                 '%s' % d[0] for d in deps)
             for (merge, ref_file, l10n_file) in deps:
                 rule = mk.create_rule([merge]).add_dependencies(
                     [ref_file, l10n_file] + python_deps)
                 rule.add_commands(
                     [
                         '$(PYTHON) -m mozbuild.action.l10n_merge '
@@ -217,27 +219,27 @@ class FasterMakeBackend(CommonBackend, P
                         )
                     ]
                 )
                 # Add a dummy rule for the l10n file since it might not exist.
                 mk.create_rule([l10n_file])
 
         mk.add_statement('include $(TOPSRCDIR)/config/faster/rules.mk')
 
-        for base, install_manifest in self._install_manifests.iteritems():
+        for base, install_manifest in six.iteritems(self._install_manifests):
             with self._write_file(
                     mozpath.join(self.environment.topobjdir, 'faster',
                                  'install_%s' % base.replace('/', '_'))) as fh:
                 install_manifest.write(fileobj=fh)
 
         # For artifact builds only, write a single unified manifest
         # for consumption by |mach watch|.
         if self.environment.is_artifact_build:
             unified_manifest = InstallManifest()
-            for base, install_manifest in self._install_manifests.iteritems():
+            for base, install_manifest in six.iteritems(self._install_manifests):
                 # Expect 'dist/bin/**', which includes 'dist/bin' with no trailing slash.
                 assert base.startswith('dist/bin')
                 base = base[len('dist/bin'):]
                 if base and base[0] == '/':
                     base = base[1:]
                 unified_manifest.add_entries_from(install_manifest, base=base)
 
             with self._write_file(
diff --git a/python/mozbuild/mozbuild/backend/recursivemake.py b/python/mozbuild/mozbuild/backend/recursivemake.py
--- a/python/mozbuild/mozbuild/backend/recursivemake.py
+++ b/python/mozbuild/mozbuild/backend/recursivemake.py
@@ -2,22 +2,23 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import logging
 import os
 import re
+import six
 
 from collections import (
     defaultdict,
     namedtuple,
 )
-from StringIO import StringIO
+from six import StringIO
 from itertools import chain
 
 from mozpack.manifests import (
     InstallManifest,
 )
 import mozpack.path as mozpath
 
 from mozbuild.frontend.context import (
@@ -76,85 +77,85 @@ from ..util import (
     ensureParentDir,
     FileAvoidWrite,
     OrderedDefaultDict,
 )
 from ..makeutil import Makefile
 from mozbuild.shellutil import quote as shell_quote
 
 MOZBUILD_VARIABLES = [
-    b'ASFLAGS',
-    b'CMSRCS',
-    b'CMMSRCS',
-    b'CPP_UNIT_TESTS',
-    b'DIRS',
-    b'DIST_INSTALL',
-    b'EXTRA_DSO_LDOPTS',
-    b'EXTRA_JS_MODULES',
-    b'EXTRA_PP_COMPONENTS',
-    b'EXTRA_PP_JS_MODULES',
-    b'FORCE_SHARED_LIB',
-    b'FORCE_STATIC_LIB',
-    b'FINAL_LIBRARY',
-    b'HOST_CFLAGS',
-    b'HOST_CSRCS',
-    b'HOST_CMMSRCS',
-    b'HOST_CXXFLAGS',
-    b'HOST_EXTRA_LIBS',
-    b'HOST_LIBRARY_NAME',
-    b'HOST_PROGRAM',
-    b'HOST_SIMPLE_PROGRAMS',
-    b'JAR_MANIFEST',
-    b'JAVA_JAR_TARGETS',
-    b'LIBRARY_NAME',
-    b'LIBS',
-    b'MAKE_FRAMEWORK',
-    b'MODULE',
-    b'NO_DIST_INSTALL',
-    b'NO_EXPAND_LIBS',
-    b'NO_INTERFACES_MANIFEST',
-    b'NO_JS_MANIFEST',
-    b'OS_LIBS',
-    b'PARALLEL_DIRS',
-    b'PREF_JS_EXPORTS',
-    b'PROGRAM',
-    b'RESOURCE_FILES',
-    b'SHARED_LIBRARY_LIBS',
-    b'SHARED_LIBRARY_NAME',
-    b'SIMPLE_PROGRAMS',
-    b'SONAME',
-    b'STATIC_LIBRARY_NAME',
-    b'TEST_DIRS',
-    b'TOOL_DIRS',
+    'ASFLAGS',
+    'CMSRCS',
+    'CMMSRCS',
+    'CPP_UNIT_TESTS',
+    'DIRS',
+    'DIST_INSTALL',
+    'EXTRA_DSO_LDOPTS',
+    'EXTRA_JS_MODULES',
+    'EXTRA_PP_COMPONENTS',
+    'EXTRA_PP_JS_MODULES',
+    'FORCE_SHARED_LIB',
+    'FORCE_STATIC_LIB',
+    'FINAL_LIBRARY',
+    'HOST_CFLAGS',
+    'HOST_CSRCS',
+    'HOST_CMMSRCS',
+    'HOST_CXXFLAGS',
+    'HOST_EXTRA_LIBS',
+    'HOST_LIBRARY_NAME',
+    'HOST_PROGRAM',
+    'HOST_SIMPLE_PROGRAMS',
+    'JAR_MANIFEST',
+    'JAVA_JAR_TARGETS',
+    'LIBRARY_NAME',
+    'LIBS',
+    'MAKE_FRAMEWORK',
+    'MODULE',
+    'NO_DIST_INSTALL',
+    'NO_EXPAND_LIBS',
+    'NO_INTERFACES_MANIFEST',
+    'NO_JS_MANIFEST',
+    'OS_LIBS',
+    'PARALLEL_DIRS',
+    'PREF_JS_EXPORTS',
+    'PROGRAM',
+    'RESOURCE_FILES',
+    'SHARED_LIBRARY_LIBS',
+    'SHARED_LIBRARY_NAME',
+    'SIMPLE_PROGRAMS',
+    'SONAME',
+    'STATIC_LIBRARY_NAME',
+    'TEST_DIRS',
+    'TOOL_DIRS',
     # XXX config/Makefile.in specifies this in a make invocation
     # 'USE_EXTENSION_MANIFEST',
-    b'XPCSHELL_TESTS',
-    b'XPIDL_MODULE',
+    'XPCSHELL_TESTS',
+    'XPIDL_MODULE',
 ]
 
 DEPRECATED_VARIABLES = [
-    b'ALLOW_COMPILER_WARNINGS',
-    b'EXPORT_LIBRARY',
-    b'EXTRA_LIBS',
-    b'FAIL_ON_WARNINGS',
-    b'HOST_LIBS',
-    b'LIBXUL_LIBRARY',
-    b'MOCHITEST_A11Y_FILES',
-    b'MOCHITEST_BROWSER_FILES',
-    b'MOCHITEST_BROWSER_FILES_PARTS',
-    b'MOCHITEST_CHROME_FILES',
-    b'MOCHITEST_FILES',
-    b'MOCHITEST_FILES_PARTS',
-    b'MOCHITEST_METRO_FILES',
-    b'MOCHITEST_ROBOCOP_FILES',
-    b'MODULE_OPTIMIZE_FLAGS',
-    b'MOZ_CHROME_FILE_FORMAT',
-    b'SHORT_LIBNAME',
-    b'TESTING_JS_MODULES',
-    b'TESTING_JS_MODULE_DIR',
+    'ALLOW_COMPILER_WARNINGS',
+    'EXPORT_LIBRARY',
+    'EXTRA_LIBS',
+    'FAIL_ON_WARNINGS',
+    'HOST_LIBS',
+    'LIBXUL_LIBRARY',
+    'MOCHITEST_A11Y_FILES',
+    'MOCHITEST_BROWSER_FILES',
+    'MOCHITEST_BROWSER_FILES_PARTS',
+    'MOCHITEST_CHROME_FILES',
+    'MOCHITEST_FILES',
+    'MOCHITEST_FILES_PARTS',
+    'MOCHITEST_METRO_FILES',
+    'MOCHITEST_ROBOCOP_FILES',
+    'MODULE_OPTIMIZE_FLAGS',
+    'MOZ_CHROME_FILE_FORMAT',
+    'SHORT_LIBNAME',
+    'TESTING_JS_MODULES',
+    'TESTING_JS_MODULE_DIR',
 ]
 
 MOZBUILD_VARIABLES_MESSAGE = 'It should only be defined in moz.build files.'
 
 DEPRECATED_VARIABLES_MESSAGE = (
     'This variable has been deprecated. It does nothing. It must be removed '
     'in order to build.'
 )
@@ -203,19 +204,18 @@ class BackendMakeFile(object):
         self.fh = FileAvoidWrite(self.name, capture_diff=True, dry_run=dry_run)
         self.fh.write('# THIS FILE WAS AUTOMATICALLY GENERATED. DO NOT EDIT.\n')
         self.fh.write('\n')
 
     def write(self, buf):
         self.fh.write(buf)
 
     def write_once(self, buf):
-        if isinstance(buf, unicode):
-            buf = buf.encode('utf-8')
-        if b'\n' + buf not in self.fh.getvalue():
+        buf = six.ensure_text(buf)
+        if '\n' + buf not in six.ensure_text(self.fh.getvalue()):
             self.write(buf)
 
     # For compatibility with makeutil.Makefile
     def add_statement(self, stmt):
         self.write('%s\n' % stmt)
 
     def close(self):
         if self.xpt_name:
@@ -808,31 +808,32 @@ class RecursiveMakeBackend(CommonBackend
                 if deps:
                     rule.add_dependencies('%s/%s' % (d, tier) for d in deps if d)
                 if dir in self._idl_dirs and tier == 'export':
                     rule.add_dependencies(['xpcom/xpidl/%s' % tier])
             rule = root_deps_mk.create_rule(['recurse_%s' % tier])
             if main:
                 rule.add_dependencies('%s/%s' % (d, tier) for d in main)
 
-        all_compile_deps = reduce(lambda x, y: x | y,
-                                  self._compile_graph.values()) if self._compile_graph else set()
+        all_compile_deps = six.moves.reduce(
+            lambda x, y: x | y,
+            self._compile_graph.values()) if self._compile_graph else set()
         # Include the following as dependencies of the top recursion target for
         # compilation:
         # - nodes that are not dependended upon by anything. Typically, this
         #   would include programs, that need to be recursed, but that nothing
         #   depends on.
         # - nodes that have no dependencies of their own. Technically, this is
         #   not necessary, because other things have dependencies on them, and
         #   they all end up rooting to nodes from the above category. But the
         #   way make works[1] is such that there can be benefits listing them
         #   as direct dependencies of the top recursion target, to somehow
         #   prioritize them.
         #   1. See bug 1262241 comment 5.
-        compile_roots = [t for t, deps in self._compile_graph.iteritems()
+        compile_roots = [t for t, deps in six.iteritems(self._compile_graph)
                          if not deps or t not in all_compile_deps]
 
         def add_category_rules(category, roots, graph):
             rule = root_deps_mk.create_rule(['recurse_%s' % category])
             rule.add_dependencies(roots)
             for target, deps in sorted(graph.items()):
                 if deps:
                     rule = root_deps_mk.create_rule([target])
@@ -856,17 +857,17 @@ class RecursiveMakeBackend(CommonBackend
             dirname = mozpath.dirname(root)
             # If a directory only contains non-default compile targets, we don't
             # attempt to dump symbols there.
             if (dirname in self._no_skip['syms'] and
                 '%s/target' % dirname not in self._compile_graph):
                 self._no_skip['syms'].remove(dirname)
 
         add_category_rules('compile', compile_roots, self._compile_graph)
-        for category, graph in non_default_graphs.iteritems():
+        for category, graph in six.iteritems(non_default_graphs):
             add_category_rules(category, non_default_roots[category], graph)
 
         root_mk = Makefile()
 
         # Fill root.mk with the convenience variables.
         for tier, filter in filters:
             all_dirs = self._traversal.traverse('', filter)
             root_mk.add_statement('%s_dirs := %s' % (tier, ' '.join(all_dirs)))
@@ -876,17 +877,17 @@ class RecursiveMakeBackend(CommonBackend
         root_mk.add_statement('compile_targets := %s' % ' '.join(sorted(
             set(self._compile_graph.keys()) | all_compile_deps)))
         root_mk.add_statement('syms_targets := %s' % ' '.join(sorted(
             set('%s/syms' % d for d in self._no_skip['syms']))))
 
         root_mk.add_statement('non_default_tiers := %s' % ' '.join(sorted(
             non_default_roots.keys())))
 
-        for category, graphs in non_default_graphs.iteritems():
+        for category, graphs in six.iteritems(non_default_graphs):
             category_dirs = [mozpath.dirname(target)
                              for target in graphs.keys()]
             root_mk.add_statement('%s_dirs := %s' % (category,
                                                      ' '.join(category_dirs)))
 
         root_mk.add_statement('include root-deps.mk')
 
         with self._write_file(
@@ -921,24 +922,24 @@ class RecursiveMakeBackend(CommonBackend
                 '# Help it out by explicitly specifiying dependencies.')
             makefile.add_statement('all_absolute_unified_files := \\\n'
                                    '  $(addprefix $(CURDIR)/,$(%s))'
                                    % unified_files_makefile_variable)
             rule = makefile.create_rule(['$(all_absolute_unified_files)'])
             rule.add_dependencies(['$(CURDIR)/%: %'])
 
     def _check_blacklisted_variables(self, makefile_in, makefile_content):
-        if b'EXTERNALLY_MANAGED_MAKE_FILE' in makefile_content:
+        if 'EXTERNALLY_MANAGED_MAKE_FILE' in makefile_content:
             # Bypass the variable restrictions for externally managed makefiles.
             return
 
         for l in makefile_content.splitlines():
             l = l.strip()
             # Don't check comments
-            if l.startswith(b'#'):
+            if l.startswith('#'):
                 continue
             for x in chain(MOZBUILD_VARIABLES, DEPRECATED_VARIABLES):
                 if x not in l:
                     continue
 
                 # Finding the variable name in the Makefile is not enough: it
                 # may just appear as part of something else, like DIRS appears
                 # in GENERATED_DIRS.
@@ -985,21 +986,20 @@ class RecursiveMakeBackend(CommonBackend
                 obj.topobjdir = bf.environment.topobjdir
                 obj.config = bf.environment
                 self._create_makefile(obj, stub=stub)
                 with open(obj.output_path) as fh:
                     content = fh.read()
                     # Directories with a Makefile containing a tools target, or
                     # XPI_PKGNAME or INSTALL_EXTENSION_ID can't be skipped and
                     # must run during the 'tools' tier.
-                    for t in (b'XPI_PKGNAME', b'INSTALL_EXTENSION_ID',
-                              b'tools'):
+                    for t in ('XPI_PKGNAME', 'INSTALL_EXTENSION_ID', 'tools'):
                         if t not in content:
                             continue
-                        if t == b'tools' and not re.search('(?:^|\s)tools.*::', content, re.M):
+                        if t == 'tools' and not re.search('(?:^|\s)tools.*::', content, re.M):
                             continue
                         if objdir == self.environment.topobjdir:
                             continue
                         self._no_skip['tools'].add(mozpath.relpath(objdir,
                                                                    self.environment.topobjdir))
 
                     # Directories with a Makefile containing a check target
                     # can't be skipped and must run during the 'check' tier.
@@ -1718,30 +1718,30 @@ class RecursiveMakeBackend(CommonBackend
         When the stub argument is True, no source file is used, and a stub
         makefile with the default header and footer only is created.
         '''
         with self._get_preprocessor(obj) as pp:
             if extra:
                 pp.context.update(extra)
             if not pp.context.get('autoconfmk', ''):
                 pp.context['autoconfmk'] = 'autoconf.mk'
-            pp.handleLine(b'# THIS FILE WAS AUTOMATICALLY GENERATED. DO NOT MODIFY BY HAND.\n')
-            pp.handleLine(b'DEPTH := @DEPTH@\n')
-            pp.handleLine(b'topobjdir := @topobjdir@\n')
-            pp.handleLine(b'topsrcdir := @top_srcdir@\n')
-            pp.handleLine(b'srcdir := @srcdir@\n')
-            pp.handleLine(b'srcdir_rel := @srcdir_rel@\n')
-            pp.handleLine(b'relativesrcdir := @relativesrcdir@\n')
-            pp.handleLine(b'include $(DEPTH)/config/@autoconfmk@\n')
+            pp.handleLine('# THIS FILE WAS AUTOMATICALLY GENERATED. DO NOT MODIFY BY HAND.\n')
+            pp.handleLine('DEPTH := @DEPTH@\n')
+            pp.handleLine('topobjdir := @topobjdir@\n')
+            pp.handleLine('topsrcdir := @top_srcdir@\n')
+            pp.handleLine('srcdir := @srcdir@\n')
+            pp.handleLine('srcdir_rel := @srcdir_rel@\n')
+            pp.handleLine('relativesrcdir := @relativesrcdir@\n')
+            pp.handleLine('include $(DEPTH)/config/@autoconfmk@\n')
             if not stub:
                 pp.do_include(obj.input_path)
             # Empty line to avoid failures when last line in Makefile.in ends
             # with a backslash.
-            pp.handleLine(b'\n')
-            pp.handleLine(b'include $(topsrcdir)/config/recurse.mk\n')
+            pp.handleLine('\n')
+            pp.handleLine('include $(topsrcdir)/config/recurse.mk\n')
         if not stub:
             # Adding the Makefile.in here has the desired side-effect
             # that if the Makefile.in disappears, this will force
             # moz.build traversal. This means that when we remove empty
             # Makefile.in files, the old file will get replaced with
             # the autogenerated one automatically.
             self.backend_input_files.add(obj.input_path)
 
diff --git a/python/mozbuild/mozbuild/backend/test_manifest.py b/python/mozbuild/mozbuild/backend/test_manifest.py
--- a/python/mozbuild/mozbuild/backend/test_manifest.py
+++ b/python/mozbuild/mozbuild/backend/test_manifest.py
@@ -1,16 +1,17 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
-import cPickle as pickle
+import six.moves.cPickle as pickle
 from collections import defaultdict
+import six
 
 import mozpack.path as mozpath
 
 from mozbuild.backend.base import PartialBackend
 from mozbuild.frontend.data import TestManifest
 
 
 class TestManifestBackend(PartialBackend):
@@ -83,16 +84,16 @@ class TestManifestBackend(PartialBackend
 
     def add_defaults(self, manifest):
         if not hasattr(manifest, 'manifest_defaults'):
             return
         for sub_manifest, defaults in manifest.manifest_defaults.items():
             self.manifest_defaults[sub_manifest] = defaults
 
     def add_installs(self, obj, topsrcdir):
-        for src, (dest, _) in obj.installs.iteritems():
+        for src, (dest, _) in six.iteritems(obj.installs):
             key = src[len(topsrcdir)+1:]
             self.installs_by_path[key].append((src, dest))
         for src, pat, dest in obj.pattern_installs:
             key = mozpath.join(src[len(topsrcdir)+1:], pat)
             self.installs_by_path[key].append((src, pat, dest))
         for path in obj.deferred_installs:
             self.deferred_installs.add(path[2:])
diff --git a/python/mozbuild/mozbuild/frontend/emitter.py b/python/mozbuild/mozbuild/frontend/emitter.py
--- a/python/mozbuild/mozbuild/frontend/emitter.py
+++ b/python/mozbuild/mozbuild/frontend/emitter.py
@@ -397,17 +397,17 @@ class TreeMetadataEmitter(LoggingMixin):
                 if force_static:
                     if isinstance(l, StaticLibrary):
                         libs[key] = l
                 else:
                     if key in libs and isinstance(l, SharedLibrary):
                         libs[key] = l
                     if key not in libs:
                         libs[key] = l
-            candidates = libs.values()
+            candidates = list(libs.values())
             if force_static and not candidates:
                 if dir:
                     raise SandboxValidationError(
                         '%s contains "static:%s", but there is no static '
                         '"%s" %s in %s.' % (variable, path, name,
                                             self.LIBRARY_NAME_VAR[obj.KIND], dir), context)
                 raise SandboxValidationError(
                     '%s contains "static:%s", but there is no static "%s" '
diff --git a/python/mozbuild/mozbuild/test/backend/test_recursivemake.py b/python/mozbuild/mozbuild/test/backend/test_recursivemake.py
--- a/python/mozbuild/mozbuild/test/backend/test_recursivemake.py
+++ b/python/mozbuild/mozbuild/test/backend/test_recursivemake.py
@@ -1,16 +1,18 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
-import cPickle as pickle
+import io
 import os
+import six.moves.cPickle as pickle
+import six
 import unittest
 
 from mozpack.manifests import (
     InstallManifest,
 )
 from mozunit import main
 
 from mozbuild.backend.recursivemake import (
@@ -653,17 +655,17 @@ class TestRecursiveMakeBackend(BackendTe
                                            '_test_files')
         m = InstallManifest(path=test_files_manifest)
 
         # Then, synthesize one from the test-installs.pkl file. This should
         # allow us to re-create a subset of the above.
         env = self._consume('test-manifest-shared-support', TestManifestBackend)
         test_installs_path = mozpath.join(env.topobjdir, 'test-installs.pkl')
 
-        with open(test_installs_path, 'r') as fh:
+        with open(test_installs_path, 'rb') as fh:
             test_installs = pickle.load(fh)
 
         self.assertEqual(set(test_installs.keys()),
                          set(['child/test_sub.js',
                               'child/data/**',
                               'child/another-file.sjs']))
         for key in test_installs.keys():
             self.assertIn(key, test_installs)
@@ -943,17 +945,17 @@ class TestRecursiveMakeBackend(BackendTe
         ]
         expected[mozpath.join(env.topobjdir, 'xpi-name')] = [
             'XPI_NAME = mycrazyxpi',
             final_target_rule
         ]
         expected[mozpath.join(env.topobjdir, 'final-target')] = [
             'FINAL_TARGET = $(DEPTH)/random-final-target'
         ]
-        for key, expected_rules in expected.iteritems():
+        for key, expected_rules in six.iteritems(expected):
             backend_path = mozpath.join(key, 'backend.mk')
             lines = [l.strip() for l in open(backend_path, 'rt').readlines()[2:]]
             found = [str for str in lines if
                      str.startswith('FINAL_TARGET') or str.startswith('XPI_NAME') or
                      str.startswith('DIST_SUBDIR')]
             self.assertEqual(found, expected_rules)
 
     def test_final_target_pp_files(self):
@@ -1023,53 +1025,53 @@ class TestRecursiveMakeBackend(BackendTe
                 '@bar@\n',
             ])
 
     def test_prog_lib_c_only(self):
         """Test that C-only binary artifacts are marked as such."""
         env = self._consume('prog-lib-c-only', RecursiveMakeBackend)
 
         # PROGRAM C-onlyness.
-        with open(os.path.join(env.topobjdir, 'c-program', 'backend.mk'), 'rb') as fh:
+        with open(os.path.join(env.topobjdir, 'c-program', 'backend.mk'), 'r') as fh:
             lines = fh.readlines()
             lines = [line.rstrip() for line in lines]
 
             self.assertIn('PROG_IS_C_ONLY_c_test_program := 1', lines)
 
-        with open(os.path.join(env.topobjdir, 'cxx-program', 'backend.mk'), 'rb') as fh:
+        with open(os.path.join(env.topobjdir, 'cxx-program', 'backend.mk'), 'r') as fh:
             lines = fh.readlines()
             lines = [line.rstrip() for line in lines]
 
             # Test for only the absence of the variable, not the precise
             # form of the variable assignment.
             for line in lines:
                 self.assertNotIn('PROG_IS_C_ONLY_cxx_test_program', line)
 
         # SIMPLE_PROGRAMS C-onlyness.
-        with open(os.path.join(env.topobjdir, 'c-simple-programs', 'backend.mk'), 'rb') as fh:
+        with open(os.path.join(env.topobjdir, 'c-simple-programs', 'backend.mk'), 'r') as fh:
             lines = fh.readlines()
             lines = [line.rstrip() for line in lines]
 
             self.assertIn('PROG_IS_C_ONLY_c_simple_program := 1', lines)
 
-        with open(os.path.join(env.topobjdir, 'cxx-simple-programs', 'backend.mk'), 'rb') as fh:
+        with open(os.path.join(env.topobjdir, 'cxx-simple-programs', 'backend.mk'), 'r') as fh:
             lines = fh.readlines()
             lines = [line.rstrip() for line in lines]
 
             for line in lines:
                 self.assertNotIn('PROG_IS_C_ONLY_cxx_simple_program', line)
 
         # Libraries C-onlyness.
-        with open(os.path.join(env.topobjdir, 'c-library', 'backend.mk'), 'rb') as fh:
+        with open(os.path.join(env.topobjdir, 'c-library', 'backend.mk'), 'r') as fh:
             lines = fh.readlines()
             lines = [line.rstrip() for line in lines]
 
             self.assertIn('LIB_IS_C_ONLY := 1', lines)
 
-        with open(os.path.join(env.topobjdir, 'cxx-library', 'backend.mk'), 'rb') as fh:
+        with open(os.path.join(env.topobjdir, 'cxx-library', 'backend.mk'), 'r') as fh:
             lines = fh.readlines()
             lines = [line.rstrip() for line in lines]
 
             for line in lines:
                 self.assertNotIn('LIB_IS_C_ONLY', line)
 
     def test_linkage(self):
         env = self._consume('linkage', RecursiveMakeBackend)
@@ -1093,17 +1095,17 @@ class TestRecursiveMakeBackend(BackendTe
             'real': {
                 'STATIC_LIBS': [],
                 'SHARED_LIBS': ['../prog/qux/qux.so'],
                 'OS_LIBS': ['-lbaz'],
             }
         }
         actual_linkage = {}
         for name in expected_linkage.keys():
-            with open(os.path.join(env.topobjdir, name, 'backend.mk'), 'rb') as fh:
+            with open(os.path.join(env.topobjdir, name, 'backend.mk'), 'r') as fh:
                 actual_linkage[name] = [line.rstrip() for line in fh.readlines()]
         for name in expected_linkage:
             for var in expected_linkage[name]:
                 for val in expected_linkage[name][var]:
                     val = os.path.normpath(val)
                     line = '%s += %s' % (var, val)
                     self.assertIn(line,
                                   actual_linkage[name])
@@ -1120,34 +1122,34 @@ class TestRecursiveMakeBackend(BackendTe
                 '../static/bar/bar_helper/bar_helper1.o',
             ],
             'shared/baz_so.list': [
                 'baz/baz1.o',
             ],
         }
         actual_list_files = {}
         for name in expected_list_files.keys():
-            with open(os.path.join(env.topobjdir, name), 'rb') as fh:
+            with open(os.path.join(env.topobjdir, name), 'r') as fh:
                 actual_list_files[name] = [line.rstrip()
                                            for line in fh.readlines()]
         for name in expected_list_files:
             self.assertEqual(actual_list_files[name],
                              [os.path.normpath(f) for f in expected_list_files[name]])
 
         # We don't produce a list file for a shared library composed only of
         # object files in its directory, but instead list them in a variable.
-        with open(os.path.join(env.topobjdir, 'prog', 'qux', 'backend.mk'), 'rb') as fh:
+        with open(os.path.join(env.topobjdir, 'prog', 'qux', 'backend.mk'), 'r') as fh:
             lines = [line.rstrip() for line in fh.readlines()]
 
         self.assertIn('qux.so_OBJS := qux1.o', lines)
 
     def test_jar_manifests(self):
         env = self._consume('jar-manifests', RecursiveMakeBackend)
 
-        with open(os.path.join(env.topobjdir, 'backend.mk'), 'rb') as fh:
+        with open(os.path.join(env.topobjdir, 'backend.mk'), 'r') as fh:
             lines = fh.readlines()
 
         lines = [line.rstrip() for line in lines]
 
         self.assertIn('JAR_MANIFEST := %s/jar.mn' % env.topsrcdir, lines)
 
     def test_test_manifests_duplicate_support_files(self):
         """Ensure duplicate support-files in test manifests work."""
@@ -1186,17 +1188,17 @@ class TestRecursiveMakeBackend(BackendTe
         expected = [
             ('dist-bin', '$(DEPTH)/dist/bin/dist-bin.prog'),
             ('dist-subdir', '$(DEPTH)/dist/bin/foo/dist-subdir.prog'),
             ('final-target', '$(DEPTH)/final/target/final-target.prog'),
             ('not-installed', 'not-installed.prog'),
         ]
         prefix = 'PROGRAM = '
         for (subdir, expected_program) in expected:
-            with open(os.path.join(env.topobjdir, subdir, 'backend.mk'), 'rb') as fh:
+            with io.open(os.path.join(env.topobjdir, subdir, 'backend.mk'), 'r') as fh:
                 lines = fh.readlines()
                 program = [line.rstrip().split(prefix, 1)[1] for line in lines
                            if line.startswith(prefix)][0]
                 self.assertEqual(program, expected_program)
 
 
 if __name__ == '__main__':
     main()
diff --git a/python/mozbuild/mozbuild/test/python.ini b/python/mozbuild/mozbuild/test/python.ini
--- a/python/mozbuild/mozbuild/test/python.ini
+++ b/python/mozbuild/mozbuild/test/python.ini
@@ -1,11 +1,13 @@
 [DEFAULT]
 subsuite = mozbuild
 
+[backend/test_fastermake.py]
+[backend/test_recursivemake.py]
 [configure/test_checks_configure.py]
 [configure/test_compile_checks.py]
 [configure/test_configure.py]
 [configure/test_moz_configure.py]
 [configure/test_options.py]
 [configure/test_toolchain_configure.py]
 [configure/test_toolchain_helpers.py]
 [configure/test_toolkit_moz_configure.py]
diff --git a/python/mozbuild/mozbuild/test/python2.ini b/python/mozbuild/mozbuild/test/python2.ini
--- a/python/mozbuild/mozbuild/test/python2.ini
+++ b/python/mozbuild/mozbuild/test/python2.ini
@@ -1,18 +1,16 @@
 [DEFAULT]
 subsuite = mozbuild
 
 [action/test_buildlist.py]
 [action/test_langpack_manifest.py]
 [action/test_process_install_manifest.py]
 [backend/test_build.py]
 [backend/test_configenvironment.py]
-[backend/test_fastermake.py]
 [backend/test_gn_processor.py]
 [backend/test_partialconfigenvironment.py]
-[backend/test_recursivemake.py]
 [backend/test_test_manifest.py]
 [backend/test_visualstudio.py]
 [codecoverage/test_lcov_rewrite.py]
 [compilation/test_warnings.py]
 [configure/lint.py]
 [configure/test_lint.py]
diff --git a/python/mozbuild/mozbuild/util.py b/python/mozbuild/mozbuild/util.py
--- a/python/mozbuild/mozbuild/util.py
+++ b/python/mozbuild/mozbuild/util.py
@@ -222,19 +222,17 @@ class FileAvoidWrite(BytesIO):
         assert 'r' in readmode
         self._capture_diff = capture_diff
         self._write_to_file = not dry_run
         self.diff = None
         self.mode = readmode
         self._binary_mode = 'b' in readmode
 
     def write(self, buf):
-        if isinstance(buf, six.text_type):
-            buf = buf.encode('utf-8')
-        BytesIO.write(self, buf)
+        BytesIO.write(self, six.ensure_binary(buf))
 
     def avoid_writing_to_file(self):
         self._write_to_file = False
 
     def close(self):
         """Stop accepting writes, compare file contents, and rewrite if needed.
 
         Returns a tuple of bools indicating what action was performed:
