# HG changeset patch
# User Matt A. Tobin <email@mattatobin.com>
# Date 1725062400 0
# Node ID 48e3c94fcf0683ededcafb7cdcd392129c15c8b2
# Parent  37256db90390003aedfbee9ce08dc1d3273692ee
Bug 1862395 - Use a compatibility milestone version for resisting fingerpriting. r=frg a=frg

 Previously the application version was used when spoofing the UA. This was not
 compatible with SeaMonkeys version scheme.

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -1085,16 +1085,17 @@ def js_option(*args, **kwargs):
 # The logic works like this:
 # - if we have "a1" in GRE_MILESTONE, we're building Nightly (define NIGHTLY_BUILD)
 # - otherwise, if we have "a" in GRE_MILESTONE, we're building Nightly or Aurora
 # - otherwise, we're building Release/Beta (define RELEASE_OR_BETA)
 @depends(check_build_environment, '--help')
 @imports(_from='__builtin__', _import='open')
 @imports('re')
 def milestone(build_env, _):
+    # Handle the Mozilla Milestone
     milestone_path = os.path.join(build_env.topsrcdir,
                                   'config',
                                   'milestone.txt')
     with open(milestone_path, 'r') as fh:
         milestone = fh.read().splitlines()[-1]
 
     is_nightly = is_release_or_beta = None
 
@@ -1102,23 +1103,37 @@ def milestone(build_env, _):
         is_nightly = True
     elif 'a' not in milestone:
         is_release_or_beta = True
 
     major_version = milestone.split('.')[0]
     m = re.search(r"([ab]\d+)", milestone)
     ab_patch = m.group(1) if m else ''
 
+    # Handle the Mozilla Compatibility Milestone
+    # This is set in build/milestone-compat.txt and should be a valid
+    # late-model Firefox ESR Version.
+    milestone_compat_path = os.path.join(build_env.topsrcdir,
+                                         'config',
+                                         'milestone-compat.txt')
+    with open(milestone_compat_path, 'r') as fh:
+        milestone_compat = fh.read().splitlines()[-1]
+
+    # In case this file becomes more useful than just XX.0 just grab the
+    # major version part.
+    milestone_compat = milestone_compat.split('.')[0]
+
     # Only expose the major version milestone in the UA string and hide the
     # patch leve (bugs 572659 and 870868).
     #
     # Only expose major milestone and alpha version in the symbolversion
     # string; as the name suggests, we use it for symbol versioning on Linux.
     return namespace(version=milestone,
                      uaversion='%s.0' % major_version,
+                     compatversion='%s.0' % milestone_compat,
                      symbolversion='%s%s' % (major_version, ab_patch),
                      is_nightly=is_nightly,
                      is_release_or_beta=is_release_or_beta)
 
 
 set_config('GRE_MILESTONE', milestone.version)
 set_config('NIGHTLY_BUILD', milestone.is_nightly)
 set_define('NIGHTLY_BUILD', milestone.is_nightly)
@@ -1126,16 +1141,20 @@ set_config('RELEASE_OR_BETA', milestone.
 set_define('RELEASE_OR_BETA', milestone.is_release_or_beta)
 add_old_configure_assignment('RELEASE_OR_BETA',
                              milestone.is_release_or_beta)
 set_define('MOZILLA_VERSION', depends(milestone)(lambda m: '"%s"' % m.version))
 set_config('MOZILLA_VERSION', milestone.version)
 set_define('MOZILLA_VERSION_U', milestone.version)
 set_define('MOZILLA_UAVERSION', depends(milestone)(lambda m: '"%s"' % m.uaversion))
 set_config('MOZILLA_SYMBOLVERSION', milestone.symbolversion)
+
+set_define('MOZILLA_COMPATVERSION_U', milestone.compatversion)
+set_define('MOZILLA_COMPATVERSION', depends(milestone)(lambda m: '"%s"' % m.compatversion))
+
 # JS configure still wants to look at these.
 add_old_configure_assignment('MOZILLA_VERSION', milestone.version)
 add_old_configure_assignment('MOZILLA_SYMBOLVERSION', milestone.symbolversion)
 
 # The app update channel is 'default' when not supplied. The value is used in
 # the application's confvars.sh (and is made available to a project specific
 # moz.configure).
 option('--enable-update-channel',
diff --git a/config/milestone-compat.txt b/config/milestone-compat.txt
new file mode 100644
--- /dev/null
+++ b/config/milestone-compat.txt
@@ -0,0 +1,9 @@
+# Holds the Compatibility milestone.
+# Should be in the format of
+#
+# XX.0
+#
+# Referenced by build/moz.configure/init.configure.
+#--------------------------------------------------------
+
+128.0
diff --git a/toolkit/components/resistfingerprinting/nsRFPService.cpp b/toolkit/components/resistfingerprinting/nsRFPService.cpp
--- a/toolkit/components/resistfingerprinting/nsRFPService.cpp
+++ b/toolkit/components/resistfingerprinting/nsRFPService.cpp
@@ -274,56 +274,24 @@ nsRFPService::GetSpoofedUserAgent(nsACSt
 {
   // This function generates the spoofed value of User Agent.
   // We spoof the values of the platform and Firefox version, which could be
   // used as fingerprinting sources to identify individuals.
   // Reference of the format of User Agent:
   // https://developer.mozilla.org/en-US/docs/Web/API/NavigatorID/userAgent
   // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent
 
-  nsresult rv;
-  nsCOMPtr<nsIXULAppInfo> appInfo =
-    do_GetService("@mozilla.org/xre/app-info;1", &rv);
-  NS_ENSURE_SUCCESS(rv, rv);
-
-  nsAutoCString appVersion;
-  rv = appInfo->GetVersion(appVersion);
-  NS_ENSURE_SUCCESS(rv, rv);
-
-  // The browser version will be spoofed as the last ESR version.
-  // By doing so, the anonymity group will cover more versions instead of one
-  // version.
-  uint32_t firefoxVersion = appVersion.ToInteger(&rv);
-  NS_ENSURE_SUCCESS(rv, rv);
+  // XXXTobin: The function has been simplified to send the a late-model
+  // common-ish compatibility esr version which is far more useful to
+  // SeaMonkey users than fuzzy math.
+  userAgent.Assign(nsPrintfCString(
+    "Mozilla/5.0 (%s; rv:%s) Gecko/%s Firefox/%s",
+    SPOOFED_UA_OS, MOZILLA_COMPATVERSION, LEGACY_BUILD_ID, MOZILLA_COMPATVERSION));
 
-  // Starting from Firefox 10, Firefox ESR was released once every seven
-  // Firefox releases, e.g. Firefox 10, 17, 24, 31, and so on.
-  // We infer the last and closest ESR version based on this rule.
-  nsCOMPtr<nsIXULRuntime> runtime =
-    do_GetService("@mozilla.org/xre/runtime;1", &rv);
-  NS_ENSURE_SUCCESS(rv, rv);
-
-  nsAutoCString updateChannel;
-  rv = runtime->GetDefaultUpdateChannel(updateChannel);
-  NS_ENSURE_SUCCESS(rv, rv);
-
-  // If we are running in Firefox ESR, determine whether the formula of ESR
-  // version has changed.  Once changed, we must update the formula in this
-  // function.
-  if (updateChannel.EqualsLiteral("esr")) {
-    MOZ_ASSERT(((firefoxVersion % 7) == 3),
-      "Please udpate ESR version formula in nsRFPService.cpp");
-  }
-
-  uint32_t spoofedVersion = firefoxVersion - ((firefoxVersion - 3) % 7);
-  userAgent.Assign(nsPrintfCString(
-    "Mozilla/5.0 (%s; rv:%d.0) Gecko/%s Firefox/%d.0",
-    SPOOFED_UA_OS, spoofedVersion, LEGACY_BUILD_ID, spoofedVersion));
-
-  return rv;
+  return NS_OK;
 }
 
 nsresult
 nsRFPService::Init()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   nsresult rv;
