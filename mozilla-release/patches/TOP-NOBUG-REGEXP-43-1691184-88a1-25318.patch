# HG changeset patch
# User Dmitry Butskoy <buc@buc.me>
# Date 1690631435 -7200
# Parent  0b0964a359f288544e37468ad59432de3bc51c68
No Bug - Import new regexp V8 engine. r=frg a=frg

Iain Ireland <iireland@mozilla.com>
Bug 1691184: Recompile if necessary before retrying interrupted regexp r=mgaudet

If an interrupt occurs during regexp execution, we return up the stack to RegExpShared::execute to handle it, then try again. Normally it's safe (if slow) to GC and discard jitcode at this point, because we can fall back to interpreted bytecode (which is not discarded). However, if the input string is long enough, then we [jump straight to compilation without producing bytecode](https://searchfox.org/mozilla-central/rev/7067bbd8194f4346ec59d77c33cd88f06763e090/js/src/vm/RegExpObject.cpp#590-596). In that case, when we resume, we will have neither bytecode nor jitcode, and end up dereferencing a null pointer.

The fix is to recompile after handling the interrupt. In addition to fixing the crash, forcing compilation here should improve our chance of eventual success (compared to resuming in the regexp interpreter).

diff --git a/js/src/irregexp/RegExpAPI.cpp b/js/src/irregexp/RegExpAPI.cpp
--- a/js/src/irregexp/RegExpAPI.cpp
+++ b/js/src/irregexp/RegExpAPI.cpp
@@ -544,16 +544,18 @@ RegExpRunStatus ExecuteRaw(jit::JitCode*
     JS::AutoSuppressGCAnalysis nogc;
     return (RegExpRunStatus)CALL_GENERATED_1(function, &data);
   }
 }
 
 RegExpRunStatus Interpret(JSContext* cx, MutableHandleRegExpShared re,
                           HandleLinearString input, size_t startIndex,
                           MatchPairs* matches) {
+  MOZ_ASSERT(re->getByteCode(input->hasLatin1Chars()));
+
   HandleScope handleScope(cx->isolate);
   V8HandleRegExp wrappedRegExp(v8::internal::JSRegExp(re), cx->isolate);
   V8HandleString wrappedInput(v8::internal::String(input), cx->isolate);
 
   static_assert(RegExpRunStatus_Error ==
                 v8::internal::RegExp::kInternalRegExpException);
   static_assert(RegExpRunStatus_Success ==
                 v8::internal::RegExp::kInternalRegExpSuccess);
diff --git a/js/src/vm/RegExpObject.cpp b/js/src/vm/RegExpObject.cpp
--- a/js/src/vm/RegExpObject.cpp
+++ b/js/src/vm/RegExpObject.cpp
@@ -643,22 +643,26 @@ RegExpShared::compileIfNecessary(JSConte
                                  MutableHandleRegExpShared re,
                                  HandleLinearString input,
                                  RegExpShared::CodeKind codeKind)
 {
   if (codeKind == RegExpShared::CodeKind::Any) {
     // We start by interpreting regexps, then compile them once they are
     // sufficiently hot. For very long input strings, we tier up eagerly.
     codeKind = RegExpShared::CodeKind::Bytecode;
-    if (IsNativeRegExpEnabled(cx) &&
-        (re->markedForTierUp(cx) || input->length() > 1000)) {
+    if (re->markedForTierUp(cx) || input->length() > 1000) {
       codeKind = RegExpShared::CodeKind::Jitcode;
     }
   }
 
+  // Fall back to bytecode if native codegen is not available.
+  if (!IsNativeRegExpEnabled(cx) && codeKind == RegExpShared::CodeKind::Jitcode) {
+    codeKind = RegExpShared::CodeKind::Bytecode;
+  }
+
   bool needsCompile = false;
   if (re->kind() == RegExpShared::Kind::Unparsed) {
     needsCompile = true;
   }
 
   if (re->kind() == RegExpShared::Kind::RegExp) {
     if (!re->isCompiled(input->hasLatin1Chars(), codeKind)) {
       needsCompile = true;
@@ -711,37 +715,46 @@ RegExpShared::execute(JSContext* cx,
     }
 
     uint32_t interruptRetries = 0;
     const uint32_t maxInterruptRetries = 4;
     do {
         RegExpRunStatus result = irregexp::Execute(cx, re, input, start, matches);
 
         if (result == RegExpRunStatus_Error) {
-            /* Execute can return RegExpRunStatus_Error:
-             *
-             *  1. If the native stack overflowed
-             *  2. If the backtrack stack overflowed
-             *  3. If an interrupt was requested during execution.
-             *
-             * In the first two cases, we want to throw an error. In the
-             * third case, we want to handle the interrupt and try again.
-             * We cap the number of times we will retry.
-             */
-            if (cx->hasPendingInterrupt()) {
-                if (!CheckForInterrupt(cx)) {
-                    return RegExpRunStatus_Error;
-                }
-                if (interruptRetries++ < maxInterruptRetries) {
-                    continue;
-                }
+          /* Execute can return RegExpRunStatus_Error:
+           *
+           *  1. If the native stack overflowed
+           *  2. If the backtrack stack overflowed
+           *  3. If an interrupt was requested during execution.
+           *
+           * In the first two cases, we want to throw an error. In the
+           * third case, we want to handle the interrupt and try again.
+           * We cap the number of times we will retry.
+           */
+          if (cx->hasPendingInterrupt()) {
+            if (!CheckForInterrupt(cx)) {
+              return RegExpRunStatus_Error;
             }
-            // If we have run out of retries, this regexp takes too long to execute.
-            ReportOverRecursed(cx);
-            return RegExpRunStatus_Error;
+            if (interruptRetries++ < maxInterruptRetries) {
+              // The initial execution may have been interpreted, or the
+              // interrupt may have triggered a GC that discarded jitcode.
+              // To maximize the chance of succeeding before being
+              // interrupted again, we want to ensure we are compiled.
+              if (!compileIfNecessary(cx, re, input,
+                                      RegExpShared::CodeKind::Jitcode)) {
+                return RegExpRunStatus_Error;
+              }
+              continue;
+            }
+          }
+          // If we have run out of retries, this regexp takes too long to
+          // execute.
+          ReportOverRecursed(cx);
+          return RegExpRunStatus_Error;
         }
 
         MOZ_ASSERT(result == RegExpRunStatus_Success ||
                    result == RegExpRunStatus_Success_NotFound);
 
         return result;
     } while (true);
 
