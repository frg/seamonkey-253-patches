# HG changeset patch
# User Randell Jesup <rjesup@jesup.org>
# Date 1548523085 18000
# Node ID 0f5c896960f5147c3600c847209fb786dc6c1481
# Parent  970ed8f727fd7ada49f2259d27c9c726921f14e8
Bug 1522150: Rename EventPriority to EventQueuePriority to avoid name conflict with MacOS r=froyd

diff --git a/xpcom/threads/AbstractEventQueue.h b/xpcom/threads/AbstractEventQueue.h
--- a/xpcom/threads/AbstractEventQueue.h
+++ b/xpcom/threads/AbstractEventQueue.h
@@ -9,17 +9,17 @@
 
 #include "mozilla/AlreadyAddRefed.h"
 #include "mozilla/Mutex.h"
 
 class nsIRunnable;
 
 namespace mozilla {
 
-enum class EventPriority
+enum class EventQueuePriority
 {
   High,
   Input,
   Normal,
   Idle,
 
   Count
 };
@@ -37,24 +37,24 @@ enum class EventPriority
 // ThreadEventQueue).
 class AbstractEventQueue
 {
 public:
   // Add an event to the end of the queue. Implementors are free to use
   // aPriority however they wish. They may ignore it if the runnable has its own
   // intrinsic priority (via nsIRunnablePriority).
   virtual void PutEvent(already_AddRefed<nsIRunnable>&& aEvent,
-                        EventPriority aPriority,
+                        EventQueuePriority aPriority,
                         const MutexAutoLock& aProofOfLock) = 0;
 
   // Get an event from the front of the queue. aPriority is an out param. If the
   // implementation supports priorities, then this should be the same priority
   // that the event was pushed with. aPriority may be null. This should return
   // null if the queue is non-empty but the event in front is not ready to run.
-  virtual already_AddRefed<nsIRunnable> GetEvent(EventPriority* aPriority,
+  virtual already_AddRefed<nsIRunnable> GetEvent(EventQueuePriority* aPriority,
                                                  const MutexAutoLock& aProofOfLock) = 0;
 
   // Returns true if the queue is empty. Implies !HasReadyEvent().
   virtual bool IsEmpty(const MutexAutoLock& aProofOfLock) = 0;
 
   // Returns true if the queue is non-empty and if the event in front is ready
   // to run. Implies !IsEmpty(). This should return true iff GetEvent returns a
   // non-null value.
diff --git a/xpcom/threads/EventQueue.cpp b/xpcom/threads/EventQueue.cpp
--- a/xpcom/threads/EventQueue.cpp
+++ b/xpcom/threads/EventQueue.cpp
@@ -4,39 +4,39 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/EventQueue.h"
 #include "nsIRunnable.h"
 
 using namespace mozilla;
 
-EventQueue::EventQueue(EventPriority aPriority)
+EventQueue::EventQueue(EventQueuePriority aPriority)
 {
 }
 
 void
 EventQueue::PutEvent(already_AddRefed<nsIRunnable>&& aEvent,
-                     EventPriority aPriority,
+                     EventQueuePriority aPriority,
                      const MutexAutoLock& aProofOfLock)
 {
   nsCOMPtr<nsIRunnable> event(aEvent);
   mQueue.Push(std::move(event));
 }
 
 already_AddRefed<nsIRunnable>
-EventQueue::GetEvent(EventPriority* aPriority,
+EventQueue::GetEvent(EventQueuePriority* aPriority,
                      const MutexAutoLock& aProofOfLock)
 {
   if (mQueue.IsEmpty()) {
     return nullptr;
   }
 
   if (aPriority) {
-    *aPriority = EventPriority::Normal;
+    *aPriority = EventQueuePriority::Normal;
   }
 
   nsCOMPtr<nsIRunnable> result = mQueue.Pop();
   return result.forget();
 }
 
 bool
 EventQueue::IsEmpty(const MutexAutoLock& aProofOfLock)
diff --git a/xpcom/threads/EventQueue.h b/xpcom/threads/EventQueue.h
--- a/xpcom/threads/EventQueue.h
+++ b/xpcom/threads/EventQueue.h
@@ -14,22 +14,22 @@
 class nsIRunnable;
 
 namespace mozilla {
 
 class EventQueue final : public AbstractEventQueue
 {
 public:
   EventQueue() {}
-  explicit EventQueue(EventPriority aPriority);
+  explicit EventQueue(EventQueuePriority aPriority);
 
   void PutEvent(already_AddRefed<nsIRunnable>&& aEvent,
-                EventPriority aPriority,
+                EventQueuePriority aPriority,
                 const MutexAutoLock& aProofOfLock) final override;
-  already_AddRefed<nsIRunnable> GetEvent(EventPriority* aPriority,
+  already_AddRefed<nsIRunnable> GetEvent(EventQueuePriority* aPriority,
                                          const MutexAutoLock& aProofOfLock) final override;
 
   bool IsEmpty(const MutexAutoLock& aProofOfLock) final override;
   bool HasReadyEvent(const MutexAutoLock& aProofOfLock) final override;
 
   size_t Count(const MutexAutoLock& aProofOfLock) const final override;
   already_AddRefed<nsIRunnable> PeekEvent(const MutexAutoLock& aProofOfLock);
 
diff --git a/xpcom/threads/MainThreadQueue.h b/xpcom/threads/MainThreadQueue.h
--- a/xpcom/threads/MainThreadQueue.h
+++ b/xpcom/threads/MainThreadQueue.h
@@ -18,20 +18,20 @@ namespace mozilla {
 
 template<typename SynchronizedQueueT, typename InnerQueueT>
 inline already_AddRefed<nsThread>
 CreateMainThread(nsIIdlePeriod* aIdlePeriod, SynchronizedQueueT** aSynchronizedQueue = nullptr)
 {
   using MainThreadQueueT = PrioritizedEventQueue<InnerQueueT>;
 
   auto queue = MakeUnique<MainThreadQueueT>(
-    MakeUnique<InnerQueueT>(EventPriority::High),
-    MakeUnique<InnerQueueT>(EventPriority::Input),
-    MakeUnique<InnerQueueT>(EventPriority::Normal),
-    MakeUnique<InnerQueueT>(EventPriority::Idle),
+    MakeUnique<InnerQueueT>(EventQueuePriority::High),
+    MakeUnique<InnerQueueT>(EventQueuePriority::Input),
+    MakeUnique<InnerQueueT>(EventQueuePriority::Normal),
+    MakeUnique<InnerQueueT>(EventQueuePriority::Idle),
     do_AddRef(aIdlePeriod));
 
   MainThreadQueueT* prioritized = queue.get();
 
   RefPtr<SynchronizedQueueT> synchronizedQueue = new SynchronizedQueueT(std::move(queue));
 
   prioritized->SetMutexRef(synchronizedQueue->MutexRef());
 
diff --git a/xpcom/threads/PrioritizedEventQueue.cpp b/xpcom/threads/PrioritizedEventQueue.cpp
--- a/xpcom/threads/PrioritizedEventQueue.cpp
+++ b/xpcom/threads/PrioritizedEventQueue.cpp
@@ -27,51 +27,51 @@ PrioritizedEventQueue<InnerQueueT>::Prio
 {
   static_assert(IsBaseOf<AbstractEventQueue, InnerQueueT>::value,
                 "InnerQueueT must be an AbstractEventQueue subclass");
 }
 
 template<class InnerQueueT>
 void
 PrioritizedEventQueue<InnerQueueT>::PutEvent(already_AddRefed<nsIRunnable>&& aEvent,
-                                             EventPriority aPriority,
+                                             EventQueuePriority aPriority,
                                              const MutexAutoLock& aProofOfLock)
 {
   // Double check the priority with a QI.
   RefPtr<nsIRunnable> event(aEvent);
-  EventPriority priority = aPriority;
+  EventQueuePriority priority = aPriority;
   if (nsCOMPtr<nsIRunnablePriority> runnablePrio = do_QueryInterface(event)) {
     uint32_t prio = nsIRunnablePriority::PRIORITY_NORMAL;
     runnablePrio->GetPriority(&prio);
     if (prio == nsIRunnablePriority::PRIORITY_HIGH) {
-      priority = EventPriority::High;
+      priority = EventQueuePriority::High;
     } else if (prio == nsIRunnablePriority::PRIORITY_INPUT) {
-      priority = EventPriority::Input;
+      priority = EventQueuePriority::Input;
     }
   }
 
-  if (priority == EventPriority::Input && mInputQueueState == STATE_DISABLED) {
-    priority = EventPriority::Normal;
+  if (priority == EventQueuePriority::Input && mInputQueueState == STATE_DISABLED) {
+    priority = EventQueuePriority::Normal;
   }
 
   switch (priority) {
-  case EventPriority::High:
+  case EventQueuePriority::High:
     mHighQueue->PutEvent(event.forget(), priority, aProofOfLock);
     break;
-  case EventPriority::Input:
+  case EventQueuePriority::Input:
     mInputQueue->PutEvent(event.forget(), priority, aProofOfLock);
     break;
-  case EventPriority::Normal:
+  case EventQueuePriority::Normal:
     mNormalQueue->PutEvent(event.forget(), priority, aProofOfLock);
     break;
-  case EventPriority::Idle:
+  case EventQueuePriority::Idle:
     mIdleQueue->PutEvent(event.forget(), priority, aProofOfLock);
     break;
-  case EventPriority::Count:
-    MOZ_CRASH("EventPriority::Count isn't a valid priority");
+  case EventQueuePriority::Count:
+    MOZ_CRASH("EventQueuePriority::Count isn't a valid priority");
     break;
   }
 }
 
 template<class InnerQueueT>
 TimeStamp
 PrioritizedEventQueue<InnerQueueT>::GetIdleDeadline()
 {
@@ -114,17 +114,17 @@ PrioritizedEventQueue<InnerQueueT>::GetI
     // longer in the idle period, we must return a valid timestamp to pretend that
     // we are still in the idle period.
     return TimeStamp::Now();
   }
   return idleDeadline;
 }
 
 template<class InnerQueueT>
-EventPriority
+EventQueuePriority
 PrioritizedEventQueue<InnerQueueT>::SelectQueue(bool aUpdateState,
                                                 const MutexAutoLock& aProofOfLock)
 {
   bool highPending = !mHighQueue->IsEmpty(aProofOfLock);
   bool normalPending = !mNormalQueue->IsEmpty(aProofOfLock);
   size_t inputCount = mInputQueue->Count(aProofOfLock);
 
   if (aUpdateState &&
@@ -150,92 +150,92 @@ PrioritizedEventQueue<InnerQueueT>::Sele
   // HIGH
   // INPUT
   // IDLE: if GetIdleDeadline()
   //
   // If we don't get an event in this pass, then we return null since no events
   // are ready.
 
   // This variable determines which queue we will take an event from.
-  EventPriority queue;
+  EventQueuePriority queue;
 
   if (mProcessHighPriorityQueue) {
-    queue = EventPriority::High;
+    queue = EventQueuePriority::High;
   } else if (inputCount > 0 && (mInputQueueState == STATE_FLUSHING ||
                                 (mInputQueueState == STATE_ENABLED &&
                                  !mInputHandlingStartTime.IsNull() &&
                                  TimeStamp::Now() > mInputHandlingStartTime))) {
-    queue = EventPriority::Input;
+    queue = EventQueuePriority::Input;
   } else if (normalPending) {
     MOZ_ASSERT(mInputQueueState != STATE_FLUSHING,
                "Shouldn't consume normal event when flusing input events");
-    queue = EventPriority::Normal;
+    queue = EventQueuePriority::Normal;
   } else if (highPending) {
-    queue = EventPriority::High;
+    queue = EventQueuePriority::High;
   } else if (inputCount > 0 && mInputQueueState != STATE_SUSPEND) {
     MOZ_ASSERT(mInputQueueState != STATE_DISABLED,
                "Shouldn't consume input events when the input queue is disabled");
-    queue = EventPriority::Input;
+    queue = EventQueuePriority::Input;
   } else {
     // We may not actually return an idle event in this case.
-    queue = EventPriority::Idle;
+    queue = EventQueuePriority::Idle;
   }
 
-  MOZ_ASSERT_IF(queue == EventPriority::Input,
+  MOZ_ASSERT_IF(queue == EventQueuePriority::Input,
                 mInputQueueState != STATE_DISABLED && mInputQueueState != STATE_SUSPEND);
 
   if (aUpdateState) {
     mProcessHighPriorityQueue = highPending;
   }
 
   return queue;
 }
 
 template<class InnerQueueT>
 already_AddRefed<nsIRunnable>
-PrioritizedEventQueue<InnerQueueT>::GetEvent(EventPriority* aPriority,
+PrioritizedEventQueue<InnerQueueT>::GetEvent(EventQueuePriority* aPriority,
                                              const MutexAutoLock& aProofOfLock)
 {
   MakeScopeExit([&] {
     mHasPendingEventsPromisedIdleEvent = false;
   });
 
 #ifndef RELEASE_OR_BETA
   // Clear mNextIdleDeadline so that it is possible to determine that
   // we're running an idle runnable in ProcessNextEvent.
   *mNextIdleDeadline = TimeStamp();
 #endif
 
-  EventPriority queue = SelectQueue(true, aProofOfLock);
+  EventQueuePriority queue = SelectQueue(true, aProofOfLock);
 
   if (aPriority) {
     *aPriority = queue;
   }
 
-  if (queue == EventPriority::High) {
+  if (queue == EventQueuePriority::High) {
     nsCOMPtr<nsIRunnable> event = mHighQueue->GetEvent(aPriority, aProofOfLock);
     MOZ_ASSERT(event);
     mInputHandlingStartTime = TimeStamp();
     mProcessHighPriorityQueue = false;
     return event.forget();
   }
 
-  if (queue == EventPriority::Input) {
+  if (queue == EventQueuePriority::Input) {
     nsCOMPtr<nsIRunnable> event = mInputQueue->GetEvent(aPriority, aProofOfLock);
     MOZ_ASSERT(event);
     return event.forget();
   }
 
-  if (queue == EventPriority::Normal) {
+  if (queue == EventQueuePriority::Normal) {
     nsCOMPtr<nsIRunnable> event = mNormalQueue->GetEvent(aPriority, aProofOfLock);
     return event.forget();
   }
 
   // If we get here, then all queues except idle are empty.
-  MOZ_ASSERT(queue == EventPriority::Idle);
+  MOZ_ASSERT(queue == EventQueuePriority::Idle);
 
   if (mIdleQueue->IsEmpty(aProofOfLock)) {
     MOZ_ASSERT(!mHasPendingEventsPromisedIdleEvent);
     return nullptr;
   }
 
   TimeStamp idleDeadline = GetIdleDeadline();
   if (!idleDeadline) {
@@ -272,27 +272,27 @@ PrioritizedEventQueue<InnerQueueT>::IsEm
 }
 
 template<class InnerQueueT>
 bool
 PrioritizedEventQueue<InnerQueueT>::HasReadyEvent(const MutexAutoLock& aProofOfLock)
 {
   mHasPendingEventsPromisedIdleEvent = false;
 
-  EventPriority queue = SelectQueue(false, aProofOfLock);
+  EventQueuePriority queue = SelectQueue(false, aProofOfLock);
 
-  if (queue == EventPriority::High) {
+  if (queue == EventQueuePriority::High) {
     return mHighQueue->HasReadyEvent(aProofOfLock);
-  } else if (queue == EventPriority::Input) {
+  } else if (queue == EventQueuePriority::Input) {
     return mInputQueue->HasReadyEvent(aProofOfLock);
-  } else if (queue == EventPriority::Normal) {
+  } else if (queue == EventQueuePriority::Normal) {
     return mNormalQueue->HasReadyEvent(aProofOfLock);
   }
 
-  MOZ_ASSERT(queue == EventPriority::Idle);
+  MOZ_ASSERT(queue == EventQueuePriority::Idle);
 
   // If we get here, then both the high and normal queues are empty.
 
   if (mIdleQueue->IsEmpty(aProofOfLock)) {
     return false;
   }
 
   TimeStamp idleDeadline = GetIdleDeadline();
diff --git a/xpcom/threads/PrioritizedEventQueue.h b/xpcom/threads/PrioritizedEventQueue.h
--- a/xpcom/threads/PrioritizedEventQueue.h
+++ b/xpcom/threads/PrioritizedEventQueue.h
@@ -13,19 +13,19 @@
 #include "mozilla/UniquePtr.h"
 #include "nsCOMPtr.h"
 #include "nsIIdlePeriod.h"
 
 class nsIRunnable;
 
 namespace mozilla {
 
-// This AbstractEventQueue implementation has one queue for each EventPriority.
-// The type of queue used for each priority is determined by the template
-// parameter.
+// This AbstractEventQueue implementation has one queue for each
+// EventQueuePriority. The type of queue used for each priority is determined by
+// the template parameter.
 //
 // When an event is pushed, its priority is determined by QIing the runnable to
 // nsIRunnablePriority, or by falling back to the aPriority parameter if the QI
 // fails.
 //
 // When an event is popped, a queue is selected based on heuristics that
 // optimize for performance. Roughly, events are selected from the highest
 // priority queue that is non-empty. However, there are a few exceptions:
@@ -41,20 +41,21 @@ class PrioritizedEventQueue final : publ
 public:
   PrioritizedEventQueue(UniquePtr<InnerQueueT> aHighQueue,
                         UniquePtr<InnerQueueT> aInputQueue,
                         UniquePtr<InnerQueueT> aNormalQueue,
                         UniquePtr<InnerQueueT> aIdleQueue,
                         already_AddRefed<nsIIdlePeriod> aIdlePeriod);
 
   void PutEvent(already_AddRefed<nsIRunnable>&& aEvent,
-                EventPriority aPriority,
+                EventQueuePriority aPriority,
                 const MutexAutoLock& aProofOfLock) final override;
-  already_AddRefed<nsIRunnable> GetEvent(EventPriority* aPriority,
-                                         const MutexAutoLock& aProofOfLock) final override;
+  already_AddRefed<nsIRunnable> GetEvent(
+      EventQueuePriority* aPriority,
+      const MutexAutoLock& aProofOfLock) final override;
 
   bool IsEmpty(const MutexAutoLock& aProofOfLock) final override;
   size_t Count(const MutexAutoLock& aProofOfLock) const final override;
   bool HasReadyEvent(const MutexAutoLock& aProofOfLock) final override;
 
   // When checking the idle deadline, we need to drop whatever mutex protects
   // this queue. This method allows that mutex to be stored so that we can drop
   // it and reacquire it when checking the idle deadline. The mutex must live at
@@ -69,17 +70,18 @@ public:
 #endif
 
   void EnableInputEventPrioritization(const MutexAutoLock& aProofOfLock) final override;
   void FlushInputEventPrioritization(const MutexAutoLock& aProofOfLock) final override;
   void SuspendInputEventPrioritization(const MutexAutoLock& aProofOfLock) final override;
   void ResumeInputEventPrioritization(const MutexAutoLock& aProofOfLock) final override;
 
 private:
-  EventPriority SelectQueue(bool aUpdateState, const MutexAutoLock& aProofOfLock);
+  EventQueuePriority SelectQueue(bool aUpdateState,
+                                 const MutexAutoLock& aProofOfLock);
 
   // Returns a null TimeStamp if we're not in the idle period.
   mozilla::TimeStamp GetIdleDeadline();
 
   UniquePtr<InnerQueueT> mHighQueue;
   UniquePtr<InnerQueueT> mInputQueue;
   UniquePtr<InnerQueueT> mNormalQueue;
   UniquePtr<InnerQueueT> mIdleQueue;
diff --git a/xpcom/threads/SchedulerGroup.h b/xpcom/threads/SchedulerGroup.h
--- a/xpcom/threads/SchedulerGroup.h
+++ b/xpcom/threads/SchedulerGroup.h
@@ -183,18 +183,17 @@ public:
       : mRunnable(aRunnable)
       , mEpochNumber(aEpoch)
     {
     }
   };
 
   using RunnableEpochQueue = Queue<EpochQueueEntry, 32>;
 
-  RunnableEpochQueue& GetQueue(mozilla::EventPriority aPriority)
-  {
+  RunnableEpochQueue& GetQueue(mozilla::EventQueuePriority aPriority) {
     return mEventQueues[size_t(aPriority)];
   }
 
 protected:
   nsresult DispatchWithDocGroup(TaskCategory aCategory,
                                 already_AddRefed<nsIRunnable>&& aRunnable,
                                 dom::DocGroup* aDocGroup);
 
@@ -228,16 +227,16 @@ protected:
   bool mIsRunning;
 
   // Number of events that are currently enqueued for this SchedulerGroup
   // (across all queues).
   size_t mEventCount = 0;
 
   nsCOMPtr<nsISerialEventTarget> mEventTargets[size_t(TaskCategory::Count)];
   RefPtr<AbstractThread> mAbstractThreads[size_t(TaskCategory::Count)];
-  RunnableEpochQueue mEventQueues[size_t(mozilla::EventPriority::Count)];
+  RunnableEpochQueue mEventQueues[size_t(mozilla::EventQueuePriority::Count)];
 };
 
 NS_DEFINE_STATIC_IID_ACCESSOR(SchedulerGroup::Runnable, NS_SCHEDULERGROUPRUNNABLE_IID);
 
 } // namespace mozilla
 
 #endif // mozilla_SchedulerGroup_h
diff --git a/xpcom/threads/SynchronizedEventQueue.h b/xpcom/threads/SynchronizedEventQueue.h
--- a/xpcom/threads/SynchronizedEventQueue.h
+++ b/xpcom/threads/SynchronizedEventQueue.h
@@ -31,30 +31,30 @@ namespace mozilla {
 // will handle the cooperative threading model.
 
 class ThreadTargetSink
 {
 public:
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(ThreadTargetSink)
 
   virtual bool PutEvent(already_AddRefed<nsIRunnable>&& aEvent,
-                        EventPriority aPriority) = 0;
+                        EventQueuePriority aPriority) = 0;
 
   // After this method is called, no more events can be posted.
   virtual void Disconnect(const MutexAutoLock& aProofOfLock) = 0;
 
 protected:
   virtual ~ThreadTargetSink() {}
 };
 
 class SynchronizedEventQueue : public ThreadTargetSink
 {
 public:
-  virtual already_AddRefed<nsIRunnable> GetEvent(bool aMayWait,
-                                                 EventPriority* aPriority) = 0;
+  virtual already_AddRefed<nsIRunnable> GetEvent(
+      bool aMayWait, EventQueuePriority* aPriority) = 0;
   virtual bool HasPendingEvent() = 0;
 
   // This method atomically checks if there are pending events and, if there are
   // none, forbids future events from being posted. It returns true if there
   // were no pending events.
   virtual bool ShutdownIfNoPendingEvents() = 0;
 
   // These methods provide access to an nsIThreadObserver, whose methods are
diff --git a/xpcom/threads/ThreadEventQueue.cpp b/xpcom/threads/ThreadEventQueue.cpp
--- a/xpcom/threads/ThreadEventQueue.cpp
+++ b/xpcom/threads/ThreadEventQueue.cpp
@@ -22,17 +22,17 @@ class ThreadEventQueue<InnerQueueT>::Nes
 public:
   NestedSink(EventQueue* aQueue, ThreadEventQueue* aOwner)
     : mQueue(aQueue)
     , mOwner(aOwner)
   {
   }
 
   bool PutEvent(already_AddRefed<nsIRunnable>&& aEvent,
-                EventPriority aPriority) final override
+                EventQueuePriority aPriority) final override
   {
     return mOwner->PutEventInternal(std::move(aEvent), aPriority, this);
   }
 
   void Disconnect(const MutexAutoLock& aProofOfLock) final override
   {
     mQueue = nullptr;
   }
@@ -60,25 +60,25 @@ template<class InnerQueueT>
 ThreadEventQueue<InnerQueueT>::~ThreadEventQueue()
 {
   MOZ_ASSERT(mNestedQueues.IsEmpty());
 }
 
 template<class InnerQueueT>
 bool
 ThreadEventQueue<InnerQueueT>::PutEvent(already_AddRefed<nsIRunnable>&& aEvent,
-                                        EventPriority aPriority)
+                                        EventQueuePriority aPriority)
 {
   return PutEventInternal(std::move(aEvent), aPriority, nullptr);
 }
 
 template<class InnerQueueT>
 bool
 ThreadEventQueue<InnerQueueT>::PutEventInternal(already_AddRefed<nsIRunnable>&& aEvent,
-                                                EventPriority aPriority,
+                                                EventQueuePriority aPriority,
                                                 NestedSink* aSink)
 {
   // We want to leak the reference when we fail to dispatch it, so that
   // we won't release the event in a wrong thread.
   LeakRefPtr<nsIRunnable> event(std::move(aEvent));
   nsCOMPtr<nsIThreadObserver> obs;
 
   {
@@ -112,17 +112,17 @@ ThreadEventQueue<InnerQueueT>::PutEventI
   }
 
   return true;
 }
 
 template<class InnerQueueT>
 already_AddRefed<nsIRunnable>
 ThreadEventQueue<InnerQueueT>::GetEvent(bool aMayWait,
-                                        EventPriority* aPriority)
+                                        EventQueuePriority* aPriority)
 {
   MutexAutoLock lock(mLock);
 
   nsCOMPtr<nsIRunnable> event;
   for (;;) {
     if (mNestedQueues.IsEmpty()) {
       event = mBaseQueue->GetEvent(aPriority, lock);
     } else {
@@ -230,17 +230,17 @@ ThreadEventQueue<InnerQueueT>::PopEventQ
 
   AbstractEventQueue* prevQueue =
     mNestedQueues.Length() == 1
     ? static_cast<AbstractEventQueue*>(mBaseQueue.get())
     : static_cast<AbstractEventQueue*>(mNestedQueues[mNestedQueues.Length() - 2].mQueue.get());
 
   // Move events from the old queue to the new one.
   nsCOMPtr<nsIRunnable> event;
-  EventPriority prio;
+  EventQueuePriority prio;
   while ((event = item.mQueue->GetEvent(&prio, lock))) {
     prevQueue->PutEvent(event.forget(), prio, lock);
   }
 
   mNestedQueues.RemoveLastElement();
 }
 
 template<class InnerQueueT>
diff --git a/xpcom/threads/ThreadEventQueue.h b/xpcom/threads/ThreadEventQueue.h
--- a/xpcom/threads/ThreadEventQueue.h
+++ b/xpcom/threads/ThreadEventQueue.h
@@ -33,20 +33,20 @@ class ThreadEventTarget;
 // is a template parameter to avoid virtual dispatch overhead.
 template<class InnerQueueT>
 class ThreadEventQueue final : public SynchronizedEventQueue
 {
 public:
   explicit ThreadEventQueue(UniquePtr<InnerQueueT> aQueue);
 
   bool PutEvent(already_AddRefed<nsIRunnable>&& aEvent,
-                EventPriority aPriority) final override;
+                EventQueuePriority aPriority) final override;
 
   already_AddRefed<nsIRunnable> GetEvent(bool aMayWait,
-                                         EventPriority* aPriority) final override;
+                                         EventQueuePriority* aPriority) final override;
   bool HasPendingEvent() final override;
 
   bool ShutdownIfNoPendingEvents() final override;
 
   void Disconnect(const MutexAutoLock& aProofOfLock) final override {}
 
   void EnableInputEventPrioritization() final override;
   void FlushInputEventPrioritization() final override;
@@ -82,17 +82,17 @@ public:
   Mutex& MutexRef() { return mLock; }
 
 private:
   class NestedSink;
 
   virtual ~ThreadEventQueue();
 
   bool PutEventInternal(already_AddRefed<nsIRunnable>&& aEvent,
-                        EventPriority aPriority,
+                        EventQueuePriority aPriority,
                         NestedSink* aQueue);
 
   UniquePtr<InnerQueueT> mBaseQueue;
 
   struct NestedQueueItem
   {
     UniquePtr<EventQueue> mQueue;
     RefPtr<ThreadEventTarget> mEventTarget;
diff --git a/xpcom/threads/ThreadEventTarget.cpp b/xpcom/threads/ThreadEventTarget.cpp
--- a/xpcom/threads/ThreadEventTarget.cpp
+++ b/xpcom/threads/ThreadEventTarget.cpp
@@ -147,17 +147,17 @@ ThreadEventTarget::Dispatch(already_AddR
     }
 
     // XXX we should be able to do something better here... we should
     //     be able to monitor the slot occupied by this event and use
     //     that to tell us when the event has been processed.
 
     RefPtr<nsThreadSyncDispatch> wrapper =
       new nsThreadSyncDispatch(current.forget(), event.take());
-    bool success = mSink->PutEvent(do_AddRef(wrapper), EventPriority::Normal); // hold a ref
+    bool success = mSink->PutEvent(do_AddRef(wrapper), EventQueuePriority::Normal); // hold a ref
     if (!success) {
       // PutEvent leaked the wrapper runnable object on failure, so we
       // explicitly release this object once for that. Note that this
       // object will be released again soon because it exits the scope.
       wrapper.get()->Release();
       return NS_ERROR_UNEXPECTED;
     }
 
@@ -166,17 +166,17 @@ ThreadEventTarget::Dispatch(already_AddR
         return !wrapper->IsPending();
       });
 
     return NS_OK;
   }
 
   NS_ASSERTION(aFlags == NS_DISPATCH_NORMAL ||
                aFlags == NS_DISPATCH_AT_END, "unexpected dispatch flags");
-  if (!mSink->PutEvent(event.take(), EventPriority::Normal)) {
+  if (!mSink->PutEvent(event.take(), EventQueuePriority::Normal)) {
     return NS_ERROR_UNEXPECTED;
   }
   return NS_OK;
 }
 
 NS_IMETHODIMP
 ThreadEventTarget::DelayedDispatch(already_AddRefed<nsIRunnable> aEvent, uint32_t aDelayMs)
 {
diff --git a/xpcom/threads/ThrottledEventQueue.cpp b/xpcom/threads/ThrottledEventQueue.cpp
--- a/xpcom/threads/ThrottledEventQueue.cpp
+++ b/xpcom/threads/ThrottledEventQueue.cpp
@@ -349,17 +349,17 @@ public:
       if (NS_WARN_IF(NS_FAILED(rv))) {
         mExecutor = nullptr;
         return rv;
       }
     }
 
     // Only add the event to the underlying queue if are able to
     // dispatch to our base target.
-    mEventQueue.PutEvent(std::move(aEvent), EventPriority::Normal, lock);
+    mEventQueue.PutEvent(std::move(aEvent), EventQueuePriority::Normal, lock);
     return NS_OK;
   }
 
   nsresult
   DelayedDispatch(already_AddRefed<nsIRunnable> aEvent, uint32_t aDelay)
   {
     // The base target may implement this, but we don't.  Always fail
     // to provide consistent behavior.
diff --git a/xpcom/threads/nsThread.cpp b/xpcom/threads/nsThread.cpp
--- a/xpcom/threads/nsThread.cpp
+++ b/xpcom/threads/nsThread.cpp
@@ -528,17 +528,17 @@ nsThread::Init(const nsACString& aName)
     NS_RELEASE_THIS();
     return NS_ERROR_OUT_OF_MEMORY;
   }
 
   // ThreadFunc will wait for this event to be run before it tries to access
   // mThread.  By delaying insertion of this event into the queue, we ensure
   // that mThread is set properly.
   {
-    mEvents->PutEvent(do_AddRef(startup), EventPriority::Normal); // retain a reference
+    mEvents->PutEvent(do_AddRef(startup), EventQueuePriority::Normal); // retain a reference
   }
 
   // Wait for thread to call ThreadManager::SetupCurrentThread, which completes
   // initialization of ThreadFunc.
   startup->Wait();
   return NS_OK;
 }
 
@@ -650,17 +650,17 @@ nsThread::ShutdownInternal(bool aSync)
     *currentThread->mRequestedShutdownContexts.AppendElement();
   context = new nsThreadShutdownContext(WrapNotNull(this), currentThread, aSync);
 
   // Set mShutdownContext and wake up the thread in case it is waiting for
   // events to process.
   nsCOMPtr<nsIRunnable> event =
     new nsThreadShutdownEvent(WrapNotNull(this), WrapNotNull(context.get()));
   // XXXroc What if posting the event fails due to OOM?
-  mEvents->PutEvent(event.forget(), EventPriority::Normal);
+  mEvents->PutEvent(event.forget(), EventQueuePriority::Normal);
 
   // We could still end up with other events being added after the shutdown
   // task, but that's okay because we process pending events in ThreadFunc
   // after setting mShutdownContext just before exiting.
   return context;
 }
 
 void
@@ -748,17 +748,17 @@ NS_IMETHODIMP
 nsThread::IdleDispatch(already_AddRefed<nsIRunnable> aEvent)
 {
   nsCOMPtr<nsIRunnable> event = aEvent;
 
   if (NS_WARN_IF(!event)) {
     return NS_ERROR_INVALID_ARG;
   }
 
-  if (!mEvents->PutEvent(event.forget(), EventPriority::Idle)) {
+  if (!mEvents->PutEvent(event.forget(), EventQueuePriority::Idle)) {
     NS_WARNING("An idle event was posted to a thread that will never run it (rejected)");
     return NS_ERROR_UNEXPECTED;
   }
 
   return NS_OK;
 }
 
 #ifdef MOZ_CANARY
@@ -881,17 +881,17 @@ nsThread::ProcessNextEvent(bool aMayWait
   Canary canary;
 #endif
   nsresult rv = NS_OK;
 
   {
     // Scope for |event| to make sure that its destructor fires while
     // mNestedEventLoopDepth has been incremented, since that destructor can
     // also do work.
-    EventPriority priority;
+    EventQueuePriority priority;
     nsCOMPtr<nsIRunnable> event = mEvents->GetEvent(reallyWait, &priority);
 
     *aResult = (event.get() != nullptr);
 
     if (event) {
       LOG(("THRD(%p) running [%p]\n", this, event.get()));
 
       if (MAIN_THREAD == mIsMainThread) {
@@ -906,17 +906,17 @@ nsThread::ProcessNextEvent(bool aMayWait
       if ((MAIN_THREAD == mIsMainThread) || mNextIdleDeadline) {
         bool labeled = GetLabeledRunnableName(event, name);
 
         if (MAIN_THREAD == mIsMainThread) {
           timer.emplace(name);
 
           // High-priority runnables are ignored here since they'll run right away
           // even with the cooperative scheduler.
-          if (!labeled && priority == EventPriority::Normal) {
+          if (!labeled && priority == EventQueuePriority::Normal) {
             TimeStamp now = TimeStamp::Now();
             double diff = (now - mLastUnlabeledRunnable).ToMilliseconds();
             Telemetry::Accumulate(Telemetry::TIME_BETWEEN_UNLABELED_RUNNABLES_MS, diff);
             mLastUnlabeledRunnable = now;
           }
         }
 
         if (mNextIdleDeadline) {
@@ -947,17 +947,17 @@ nsThread::ProcessNextEvent(bool aMayWait
         // terminating null.
         uint32_t length = std::min((uint32_t) kRunnableNameBufSize - 1,
                                    (uint32_t) name.Length());
         memcpy(sMainThreadRunnableName.begin(), name.BeginReading(), length);
         sMainThreadRunnableName[length] = '\0';
       }
 #endif
       Maybe<AutoTimeDurationHelper> timeDurationHelper;
-      if (priority == EventPriority::Input) {
+      if (priority == EventQueuePriority::Input) {
         timeDurationHelper.emplace();
       }
       event->Run();
     } else if (aMayWait) {
       MOZ_ASSERT(ShuttingDown(),
                  "This should only happen when shutting down");
       rv = NS_ERROR_UNEXPECTED;
     }
diff --git a/xpcom/threads/nsThreadPool.cpp b/xpcom/threads/nsThreadPool.cpp
--- a/xpcom/threads/nsThreadPool.cpp
+++ b/xpcom/threads/nsThreadPool.cpp
@@ -90,17 +90,17 @@ nsThreadPool::PutEvent(already_AddRefed<
     if (mThreads.Count() < (int32_t)mThreadLimit &&
         !(aFlags & NS_DISPATCH_AT_END) &&
         // Spawn a new thread if we don't have enough idle threads to serve
         // pending events immediately.
         mEvents.Count(lock) >= mIdleCount) {
       spawnThread = true;
     }
 
-    mEvents.PutEvent(std::move(aEvent), EventPriority::Normal, lock);
+    mEvents.PutEvent(std::move(aEvent), EventQueuePriority::Normal, lock);
     mEventsAvailable.Notify();
     stackSize = mStackSize;
   }
 
   LOG(("THRD-P(%p) put [spawn=%d]\n", this, spawnThread));
   if (!spawnThread) {
     return NS_OK;
   }
