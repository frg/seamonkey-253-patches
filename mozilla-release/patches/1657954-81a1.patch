# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1597161532 0
# Node ID 3a2dea9d4ff28c6a76a379c69a497fab49b48fa3
# Parent  db03e01465544a13a668ed0793895d90893332d7
Bug 1657954 - Move various branding options from `old-configure` r=geckoview-reviewers,mhentges,nalexander,snorp

Differential Revision: https://phabricator.services.mozilla.com/D86391

diff --git a/browser/confvars.sh b/browser/confvars.sh
--- a/browser/confvars.sh
+++ b/browser/confvars.sh
@@ -1,14 +1,13 @@
 #! /bin/sh
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
-MOZ_APP_BASENAME=Firefox
 MOZ_APP_VENDOR=Mozilla
 MOZ_UPDATER=1
 
 if test "$OS_ARCH" = "WINNT"; then
   if ! test "$HAVE_64BIT_BUILD"; then
     if test "$MOZ_UPDATE_CHANNEL" = "nightly" -o \
             "$MOZ_UPDATE_CHANNEL" = "aurora" -o \
             "$MOZ_UPDATE_CHANNEL" = "beta" -o \
diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -1181,12 +1181,30 @@ def project_flag(env=None, set_for_old_c
     set_config(env, option_implementation)
     if set_as_define:
         set_define(env, option_implementation)
     if set_for_old_configure:
         add_old_configure_assignment(env, option_implementation)
 
 # milestone.is_nightly corresponds to cases NIGHTLY_BUILD is set.
 
-
 @depends(milestone)
 def enabled_in_nightly(milestone):
     return milestone.is_nightly
+
+# Branding
+# ==============================================================
+option('--with-app-basename', env='MOZ_APP_BASENAME', nargs=1,
+       help='Typically stays consistent for multiple branded versions of a '
+       'given application (e.g. Aurora and Firefox both use "Firefox"), but '
+       'may vary for full rebrandings (e.g. Iceweasel). Used for '
+       'application.ini\'s "Name" field, which controls profile location in '
+       'the absence of a "Profile" field (see below), and various system '
+       'integration hooks (Unix remoting, Windows MessageWindow name, etc.')
+
+@depends('--with-app-basename')
+def moz_app_basename(value):
+    if value:
+        return value[0]
+    return 'Firefox'
+
+set_config('MOZ_APP_BASENAME', moz_app_basename,
+           when=depends(build_project)(lambda p: p != 'js'))
diff --git a/build/moz.configure/old.configure b/build/moz.configure/old.configure
--- a/build/moz.configure/old.configure
+++ b/build/moz.configure/old.configure
@@ -237,18 +237,16 @@ def old_configure_options(*options):
     '--enable-xul',
     '--enable-zipwriter',
     '--includedir',
     '--libdir',
     '--prefix',
     '--with-android-distribution-directory',
     '--with-android-max-sdk',
     '--with-android-min-sdk',
-    '--with-app-basename',
-    '--with-app-name',
     '--with-branding',
     '--with-debug-label',
     '--with-distribution-id',
     '--with-jitreport-granularity',
     '--with-macbundlename-prefix',
     '--with-nss-exec-prefix',
     '--with-nss-prefix',
     '--with-system-libevent',
diff --git a/js/moz.configure b/js/moz.configure
--- a/js/moz.configure
+++ b/js/moz.configure
@@ -13,16 +13,32 @@ def building_js(build_project):
 
 # Exception to the rule above: JS_STANDALONE is a special option that doesn't
 # want the js_option treatment. When we're done merging js/src/configure and
 # top-level configure, it can go away, although the JS_STANDALONE config
 # will still need to be set depending on building_js above.
 option(env='JS_STANDALONE', default=building_js,
        help='Reserved for internal use')
 
+# Branding
+# ==============================================================
+js_option('--with-app-name', env='MOZ_APP_NAME', nargs=1,
+          help='Used for e.g. the binary program file name. If not set, '
+          'defaults to a lowercase form of MOZ_APP_BASENAME.')
+
+@depends('--with-app-name', 'JS_STANDALONE', moz_app_basename)
+def moz_app_name(value, js_standalone, moz_app_basename):
+    if value:
+        return value[0]
+    if js_standalone:
+        return 'js'
+    return moz_app_basename.lower()
+
+set_config('MOZ_APP_NAME', moz_app_name)
+
 include('../build/moz.configure/nspr.configure',
         when='--enable-compile-environment')
 include('../build/moz.configure/rust.configure',
         when='--enable-compile-environment')
 include('../build/moz.configure/bindgen.configure',
         when='--enable-compile-environment')
 
 @depends('JS_STANDALONE')
diff --git a/js/src/old-configure.in b/js/src/old-configure.in
--- a/js/src/old-configure.in
+++ b/js/src/old-configure.in
@@ -1450,17 +1450,16 @@ AC_SUBST_LIST(WARNINGS_CFLAGS)
 AC_SUBST(LIBICONV)
 
 AC_SUBST(INCREMENTAL_LINKER)
 
 AC_SUBST_LIST(MOZ_FIX_LINK_PATHS)
 
 AC_SUBST(MOZ_POST_PROGRAM_COMMAND)
 
-AC_SUBST(MOZ_APP_NAME)
 AC_SUBST(MOZ_APP_DISPLAYNAME)
 AC_SUBST(MOZ_APP_VERSION)
 
 AC_SUBST(MOZ_PKG_SPECIAL)
 
 dnl Echo the CFLAGS to remove extra whitespace.
 CFLAGS=`echo \
 	$_COMPILATION_CFLAGS \
@@ -1556,17 +1555,16 @@ dnl ====================================
 dnl JavaScript shell
 dnl ========================================================
 
 MOZ_CHECK_ALLOCATOR
 
 AC_SUBST(ac_configure_args)
 
 if test -n "$JS_STANDALONE"; then
-MOZ_APP_NAME="mozjs"
 MOZ_APP_VERSION="$MOZILLA_SYMBOLVERSION"
 JS_LIBRARY_NAME="mozjs-$MOZILLA_SYMBOLVERSION"
 else
 JS_LIBRARY_NAME="mozjs"
 fi
 JS_CONFIG_LIBS="$NSPR_LIBS $LIBS"
 if test -n "$GNU_CC"; then
 JS_CONFIG_MOZ_JS_LIBS='-L${libdir} -l${JS_LIBRARY_NAME}'
diff --git a/js/sub.configure b/js/sub.configure
--- a/js/sub.configure
+++ b/js/sub.configure
@@ -70,20 +70,19 @@ def js_subconfigure(host, target, build_
 
     # Variables that were explicitly exported from old-configure, and those
     # explicitly set in the environment when invoking old-configure, were
     # automatically inherited from subconfigure. We assume the relevant ones
     # have a corresponding AC_SUBST in old-configure, making them available
     # in `substs`.
     for var in (
         'MOZ_SYSTEM_ZLIB', 'MOZ_ZLIB_CFLAGS', 'MOZ_ZLIB_LIBS',
-        'MOZ_APP_NAME', 'MOZ_APP_REMOTINGNAME', 'MOZ_DEV_EDITION',
-        'STLPORT_LIBS', 'DIST', 'MOZ_LINKER', 'ZLIB_IN_MOZGLUE', 'RANLIB',
-        'AR', 'CPP', 'CC', 'CXX', 'CPPFLAGS', 'CFLAGS', 'CXXFLAGS',
-        'LDFLAGS', 'HOST_CC', 'HOST_CXX', 'HOST_CPPFLAGS',
+        'MOZ_DEV_EDITION', 'STLPORT_LIBS', 'DIST', 'MOZ_LINKER',
+        'ZLIB_IN_MOZGLUE', 'RANLIB', 'AR', 'CPP', 'CC', 'CXX', 'CPPFLAGS',
+        'CFLAGS', 'CXXFLAGS', 'LDFLAGS', 'HOST_CC', 'HOST_CXX', 'HOST_CPPFLAGS',
         'HOST_CXXFLAGS', 'HOST_LDFLAGS'
     ):
         if var not in from_assignment and var in substs:
             value = substs[var]
         elif var in assignments:
             value = assignments[var]
         elif mozconfig and var in mozconfig and \
                 not mozconfig[var][1].startswith('removed'):
diff --git a/old-configure.in b/old-configure.in
--- a/old-configure.in
+++ b/old-configure.in
@@ -82,17 +82,16 @@ case "$target" in
     AC_DEFINE(ANDROID)
     ;;
 *-linux*)
     AC_PATH_PROG(OBJCOPY,objcopy)
     ;;
 esac
 
 AC_SUBST(ANDROID_SOURCE)
-AC_SUBST(ANDROID_PACKAGE_NAME)
 AC_SUBST(OBJCOPY)
 
 dnl ========================================================
 dnl Checks for compilers.
 dnl ========================================================
 
 if test "$COMPILE_ENVIRONMENT"; then
 
@@ -1695,35 +1694,16 @@ fi
 if test "$BUILDING_RELEASE"; then
   # Override value in defines.sh, if any
   EARLY_BETA_OR_EARLIER=
 elif test "$EARLY_BETA_OR_EARLIER"; then
   AC_DEFINE(EARLY_BETA_OR_EARLIER)
 fi
 AC_SUBST(EARLY_BETA_OR_EARLIER)
 
-# Allow someone to change MOZ_APP_NAME and MOZ_APP_BASENAME in mozconfig
-MOZ_ARG_WITH_STRING(app-name,
-[--with-app-name=APPNAME sets MOZ_APP_NAME to APPNAME],
-WITH_APP_NAME=$withval,
-)
-
-if test -n "$WITH_APP_NAME" ; then
-    MOZ_APP_NAME="$WITH_APP_NAME"
-fi
-
-MOZ_ARG_WITH_STRING(app-basename,
-[--with-app-basename=BASENAME sets MOZ_APP_BASENAME to BASENAME],
-WITH_APP_BASENAME=$withval,
-)
-
-if test -n "$WITH_APP_BASENAME" ; then
-    MOZ_APP_BASENAME="$WITH_APP_BASENAME"
-fi
-
 dnl ========================================================
 dnl Ensure Android SDK and build-tools versions depending on
 dnl mobile target.
 dnl ========================================================
 
 case "$MOZ_BUILD_APP" in
 mobile/android)
     MOZ_ANDROID_SDK
@@ -2929,67 +2909,32 @@ MOZ_CHILD_PROCESS_BUNDLENAME="${MOZ_APP_
 AC_SUBST(MOZ_CHILD_PROCESS_NAME)
 AC_SUBST(MOZ_CHILD_PROCESS_BUNDLE)
 AC_SUBST(MOZ_CHILD_PROCESS_BUNDLENAME)
 
 # The following variables are available to branding and application
 # configuration ($BRANDING/configure.sh and $APPLICATION/confvars.sh):
 # - MOZ_APP_VENDOR: Used for application.ini's "Vendor" field, which also
 # impacts profile location and user-visible fields.
-# - MOZ_APP_BASENAME: Typically stays consistent for multiple branded
-# versions of a given application (e.g. Aurora and Firefox both use
-# "Firefox"), but may vary for full rebrandings (e.g. Iceweasel). Used
-# for application.ini's "Name" field, which controls profile location in
-# the absence of a "Profile" field (see below), and various system
-# integration hooks (Unix remoting, Windows MessageWindow name, etc.)
 # - MOZ_APP_DISPLAYNAME: Used in user-visible fields (DLL properties,
 # Mac Bundle name, Updater, Installer), it is typically used for nightly
 # builds (e.g. Aurora for Firefox).
 # - MOZ_APP_VERSION: Defines the application version number.
 # - MOZ_APP_VERSION_DISPLAY: Defines the application version number. Used
 # in the "About" window. If not set, defaults to MOZ_APP_VERSION.
-# - MOZ_APP_NAME: Used for e.g. the binary program file name. If not set,
-# defaults to a lowercase form of MOZ_APP_BASENAME.
-# - MOZ_APP_REMOTINGNAME: Used for the internal program name, which affects
-# profile name and remoting. If not set, defaults to MOZ_APP_NAME.
 # - MOZ_APP_PROFILE: When set, used for application.ini's
 # "Profile" field, which controls profile location.
 # - MOZ_APP_ID: When set, used for application.ini's "ID" field, and
 # crash reporter server url.
 # - MOZ_PROFILE_MIGRATOR: When set, enables profile migrator.
 
-if test -z "$MOZ_APP_NAME"; then
-   MOZ_APP_NAME=`echo $MOZ_APP_BASENAME | tr A-Z a-z`
-fi
-
-if test -z "$MOZ_APP_REMOTINGNAME"; then
-   MOZ_APP_REMOTINGNAME=$MOZ_APP_NAME
-fi
-
 if test -z "$MOZ_APP_VERSION_DISPLAY"; then
    MOZ_APP_VERSION_DISPLAY=$MOZ_APP_VERSION
 fi
 
-if test -z "$ANDROID_PACKAGE_NAME" ; then
-   # When we got rid of the Aurora channel we decided to replace the old
-   # Nightly ANDROID_PACKAGE_NAME with Aurora. To make sure this is inherited
-   # by all mozilla-central based branches we make this the new "default"
-   # for Fennec. Non mozilla-central based branches set ANDROID_PACKAGE_NAME
-   # in their mozconfig, so they will never get this. If there are ever
-   # non-Fennec builds for Android, they will fall into the else block
-   # and use their own default name.
-   # https://bugzilla.mozilla.org/show_bug.cgi?id=1357808 has additional
-   # background on this.
-   if test "$MOZ_APP_NAME" = "fennec"; then
-      ANDROID_PACKAGE_NAME="org.mozilla.fennec_aurora"
-   else
-      ANDROID_PACKAGE_NAME="org.mozilla.$MOZ_APP_NAME"
-   fi
-fi
-
 # For extensions and langpacks, we require a max version that is compatible
 # across security releases. MOZ_APP_MAXVERSION is our method for doing that.
 # 24.0a1 and 24.0a2 aren't affected
 # 24.0 becomes 24.*
 # 24.1.1 becomes 24.*
 IS_ALPHA=`echo $MOZ_APP_VERSION | grep a`
 if test -z "$IS_ALPHA"; then
   changequote(,)
@@ -2998,20 +2943,17 @@ if test -z "$IS_ALPHA"; then
   else
     MOZ_APP_MAXVERSION=`echo $MOZ_APP_VERSION | sed "s|\(^[0-9]*\).*|\1|"`.*
   fi
   changequote([,])
 else
   MOZ_APP_MAXVERSION=$MOZ_APP_VERSION
 fi
 
-AC_SUBST(MOZ_APP_NAME)
-AC_SUBST(MOZ_APP_REMOTINGNAME)
 AC_SUBST(MOZ_APP_DISPLAYNAME)
-AC_SUBST(MOZ_APP_BASENAME)
 AC_SUBST(MOZ_APP_VENDOR)
 AC_SUBST(MOZ_APP_PROFILE)
 AC_SUBST(MOZ_APP_ID)
 AC_SUBST(MAR_CHANNEL_ID)
 AC_SUBST(ACCEPTED_MAR_CHANNEL_IDS)
 AC_SUBST(MOZ_PROFILE_MIGRATOR)
 AC_DEFINE_UNQUOTED(MOZ_APP_UA_NAME, "$MOZ_APP_UA_NAME")
 AC_SUBST(MOZ_APP_UA_NAME)
diff --git a/toolkit/moz.configure b/toolkit/moz.configure
--- a/toolkit/moz.configure
+++ b/toolkit/moz.configure
@@ -1639,8 +1639,36 @@ with only_when(compile_environment & tar
 
 # Alternative Crashreporter setting
 option("--with-crashreporter-url", env="MOZ_CRASHREPORTER_URL",
        default="https://crash-reports.mozilla.com/", nargs=1,
        help="Set an alternative crashreporter url")
 
 set_config("MOZ_CRASHREPORTER_URL",
            depends("--with-crashreporter-url")(lambda x: x[0].rstrip("/")))
+
+
+# Branding
+# ==============================================================
+option(env='MOZ_APP_REMOTINGNAME', nargs=1,
+       help='Used for the internal program name, which affects profile name '
+       'and remoting. If not set, defaults to MOZ_APP_NAME.')
+
+@depends('MOZ_APP_REMOTINGNAME', moz_app_name)
+def moz_app_remotingname(value, moz_app_name):
+    if value:
+        return value[0]
+    return moz_app_name
+
+set_config('MOZ_APP_REMOTINGNAME', moz_app_remotingname)
+
+option(env='ANDROID_PACKAGE_NAME', nargs=1,
+       help='Name of the Android package (default org.mozilla.$MOZ_APP_NAME)')
+
+@depends('ANDROID_PACKAGE_NAME', moz_app_name)
+def android_package_name(value, moz_app_name):
+    if value:
+        return value[0]
+    if moz_app_name == 'fennec':
+        return 'org.mozilla.fennec_aurora'
+    return 'org.mozilla.%s' % moz_app_name
+
+set_config('ANDROID_PACKAGE_NAME', android_package_name)
\ No newline at end of file
