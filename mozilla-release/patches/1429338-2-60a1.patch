# HG changeset patch
# User Henrik Skupin <mail@hskupin.info>
# Date 1516725066 -3600
# Node ID 75f63c4d1ebc949647184fd60972fc7b9fd4affb
# Parent  0a0c8aa2a94f76c42917944f04498183ddb4df3a
Bug 1429338 - Marionette has to honor "moz:useNonSpecCompliantPointerOrigin" capability. r=maja_zf

This flag is used to turn off the WebDriver spec conforming pointer origin
calculation. It has to be kept until all Selenium bindings can successfully
handle the WebDriver spec conforming Pointer Origin calculation.

MozReview-Commit-ID: 3YknXlWoyi1

diff --git a/testing/marionette/action.js b/testing/marionette/action.js
--- a/testing/marionette/action.js
+++ b/testing/marionette/action.js
@@ -333,22 +333,25 @@ const KEY_CODE_LOOKUP = {
 };
 
 /** Represents possible values for a pointer-move origin. */
 action.PointerOrigin = {
   Viewport: "viewport",
   Pointer: "pointer",
 };
 
+/** Flag for WebDriver spec conforming pointer origin calculation. */
+action.specCompatPointerOrigin = true;
+
 /**
  * Look up a PointerOrigin.
  *
  * @param {(string|Element)=} obj
  *     Origin for a <code>pointerMove</code> action.  Must be one of
- *     "viewport" (default), "pointer", or a DOM element or a DOM element.
+ *     "viewport" (default), "pointer", or a DOM element.
  *
  * @return {action.PointerOrigin}
  *     Pointer origin.
  *
  * @throws {InvalidArgumentError}
  *     If <var>obj</var> is not a valid origin.
  */
 action.PointerOrigin.get = function(obj) {
@@ -964,21 +967,28 @@ action.Mouse = class {
  * tick's actions are not dispatched until the Promise for the current
  * tick is resolved.
  *
  * @param {action.Chain} chain
  *     Actions grouped by tick; each element in |chain| is a sequence of
  *     actions for one tick.
  * @param {WindowProxy} window
  *     Current window global.
+ * @param {boolean=} [specCompatPointerOrigin=true] specCompatPointerOrigin
+ *     Flag to turn off the WebDriver spec conforming pointer origin
+ *     calculation. It has to be kept until all Selenium bindings can
+ *     successfully handle the WebDriver spec conforming Pointer Origin
+ *     calculation. See https://bugzilla.mozilla.org/show_bug.cgi?id=1429338.
  *
  * @return {Promise}
  *     Promise for dispatching all actions in |chain|.
  */
-action.dispatch = function(chain, window) {
+action.dispatch = function(chain, window, specCompatPointerOrigin = true) {
+  action.specCompatPointerOrigin = specCompatPointerOrigin;
+
   let chainEvents = (async () => {
     for (let tickActions of chain) {
       await action.dispatchTickActions(
           tickActions,
           action.computeTickDuration(tickActions),
           window);
     }
   })();
@@ -1001,18 +1011,17 @@ action.dispatch = function(chain, window
  * @param {number} tickDuration
  *     Duration in milliseconds of this tick.
  * @param {WindowProxy} window
  *     Current window global.
  *
  * @return {Promise}
  *     Promise for dispatching all tick-actions and pending DOM events.
  */
-action.dispatchTickActions = function(
-    tickActions, tickDuration, window) {
+action.dispatchTickActions = function(tickActions, tickDuration, window) {
   let pendingEvents = tickActions.map(toEvents(tickDuration, window));
   return Promise.all(pendingEvents);
 };
 
 /**
  * Compute tick duration in milliseconds for a collection of actions.
  *
  * @param {Array.<action.Action>} tickActions
@@ -1422,12 +1431,15 @@ function inViewPort(x, y, win) {
   assert.number(x, `Expected x to be finite number`);
   assert.number(y, `Expected y to be finite number`);
   // Viewport includes scrollbars if rendered.
   return !(x < 0 || y < 0 || x > win.innerWidth || y > win.innerHeight);
 }
 
 function getElementCenter(el, window) {
   if (element.isDOMElement(el)) {
-    return element.getInViewCentrePoint(el.getClientRects()[0], window);
+    if (action.specCompatPointerOrigin) {
+      return element.getInViewCentrePoint(el.getClientRects()[0], window);
+    }
+    return element.coordinates(el);
   }
   return {};
 }
diff --git a/testing/marionette/driver.js b/testing/marionette/driver.js
--- a/testing/marionette/driver.js
+++ b/testing/marionette/driver.js
@@ -594,16 +594,20 @@ GeckoDriver.prototype.listeningPromise =
  *  <dd>Describes the timeouts imposed on certian session operations.
  *
  *  <dt><code>proxy</code> (Proxy object)
  *  <dd>Defines the proxy configuration.
  *
  *  <dt><code>moz:accessibilityChecks</code> (boolean)
  *  <dd>Run a11y checks when clicking elements.
  *
+ *  <dt><code>moz:useNonSpecCompliantPointerOrigin</code> (boolean)
+ *  <dd>Use the not WebDriver conforming calculation of the pointer origin
+ *   when the origin is an element, and the element center point is used.
+ *
  *  <dt><code>moz:webdriverClick</code> (boolean)
  *  <dd>Use a WebDriver conforming <i>WebDriver::ElementClick</i>.
  * </dl>
  *
  * <h4>Timeouts object</h4>
  *
  * <dl>
  *  <dt><code>script</code> (number)
diff --git a/testing/marionette/harness/marionette_harness/tests/unit/test_capabilities.py b/testing/marionette/harness/marionette_harness/tests/unit/test_capabilities.py
--- a/testing/marionette/harness/marionette_harness/tests/unit/test_capabilities.py
+++ b/testing/marionette/harness/marionette_harness/tests/unit/test_capabilities.py
@@ -63,24 +63,34 @@ class TestCapabilities(MarionetteTestCas
                 current_profile = self.marionette.instance.runner.device.app_ctx.remote_profile
             else:
                 current_profile = convert_path(self.marionette.instance.runner.profile.profile)
             self.assertEqual(convert_path(str(self.caps["moz:profile"])), current_profile)
             self.assertEqual(convert_path(str(self.marionette.profile)), current_profile)
 
         self.assertIn("moz:accessibilityChecks", self.caps)
         self.assertFalse(self.caps["moz:accessibilityChecks"])
+
+        self.assertIn("moz:useNonSpecCompliantPointerOrigin", self.caps)
+        self.assertFalse(self.caps["moz:useNonSpecCompliantPointerOrigin"])
+
         self.assertIn("moz:webdriverClick", self.caps)
-        self.assertEqual(self.caps["moz:webdriverClick"], True)
+        self.assertTrue(self.caps["moz:webdriverClick"])
 
     def test_disable_webdriver_click(self):
         self.marionette.delete_session()
         self.marionette.start_session({"moz:webdriverClick": False})
         caps = self.marionette.session_capabilities
-        self.assertEqual(False, caps["moz:webdriverClick"])
+        self.assertFalse(caps["moz:webdriverClick"])
+
+    def test_use_non_spec_compliant_pointer_origin(self):
+        self.marionette.delete_session()
+        self.marionette.start_session({"moz:useNonSpecCompliantPointerOrigin": True})
+        caps = self.marionette.session_capabilities
+        self.assertTrue(caps["moz:useNonSpecCompliantPointerOrigin"])
 
     def test_we_get_valid_uuid4_when_creating_a_session(self):
         self.assertNotIn("{", self.marionette.session_id,
                          "Session ID has {{}} in it: {}".format(
                              self.marionette.session_id))
 
 
 class TestCapabilityMatching(MarionetteTestCase):
diff --git a/testing/marionette/harness/marionette_harness/tests/unit/test_mouse_action.py b/testing/marionette/harness/marionette_harness/tests/unit/test_mouse_action.py
new file mode 100644
--- /dev/null
+++ b/testing/marionette/harness/marionette_harness/tests/unit/test_mouse_action.py
@@ -0,0 +1,145 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+from __future__ import absolute_import
+
+import urllib
+
+from marionette_driver import By, errors, Wait
+from marionette_driver.keys import Keys
+from marionette_driver.marionette import W3C_WEBELEMENT_KEY
+
+from marionette_harness import MarionetteTestCase
+
+
+def inline(doc):
+    return "data:text/html;charset=utf-8,{}".format(urllib.quote(doc))
+
+
+class Actions(object):
+    """Temporary class until Marionette client supports the WebDriver actions."""
+
+    def __init__(self, marionette):
+        self.marionette = marionette
+
+        self.action_chain = []
+
+    def perform(self):
+        params = {"actions": [{
+            "actions": self.action_chain,
+            "id": "mouse",
+            "parameters": {
+                "pointerType": "mouse"
+            },
+            "type": "pointer"
+        }]}
+
+        return self.marionette._send_message("performActions", params=params)
+
+    def move(self, element, x=0, y=0, duration=250):
+        self.action_chain.append({
+            "duration": duration,
+            "origin": {
+                W3C_WEBELEMENT_KEY: element.id
+            },
+            "type": "pointerMove",
+            "x": x,
+            "y": y,
+        })
+
+        return self
+
+    def click(self):
+        self.action_chain.extend([{
+            "button": 0,
+            "type": "pointerDown"
+        }, {
+            "button": 0,
+            "type": "pointerUp"
+        }])
+
+        return self
+
+
+class BaseMouseAction(MarionetteTestCase):
+
+    def setUp(self):
+        super(BaseMouseAction, self).setUp()
+
+        if self.marionette.session_capabilities["platformName"] == "darwin":
+            self.mod_key = Keys.META
+        else:
+            self.mod_key = Keys.CONTROL
+
+        self.action = Actions(self.marionette)
+
+    def tearDown(self):
+        super(BaseMouseAction, self).tearDown()
+
+    @property
+    def click_position(self):
+        return self.marionette.execute_script("""
+          if (window.click_x && window.click_y) {
+            return {x: window.click_x, y: window.click_y};
+          }
+        """, sandbox=None)
+
+    def get_element_center_point(self, elem):
+        return {
+            "x": elem.location["x"] + elem.size["width"] / 2,
+            "y": elem.location["y"] + elem.size["height"] / 2
+        }
+
+
+class TestNonSpecCompliantPointerOrigin(BaseMouseAction):
+
+    def setUp(self):
+        super(TestNonSpecCompliantPointerOrigin, self).setUp()
+
+        self.marionette.delete_session()
+        self.marionette.start_session({"moz:useNonSpecCompliantPointerOrigin": True})
+
+    def tearDown(self):
+        self.marionette.delete_session()
+        self.marionette.start_session()
+
+        super(TestNonSpecCompliantPointerOrigin, self).tearDown()
+
+    def test_click_element_smaller_than_viewport(self):
+        self.marionette.navigate(inline("""
+          <div id="div" style="width: 10vw; height: 10vh; background: green;"
+               onclick="window.click_x = event.clientX; window.click_y = event.clientY" />
+        """))
+        elem = self.marionette.find_element(By.ID, "div")
+        elem_center_point = self.get_element_center_point(elem)
+
+        self.action.move(elem).click().perform()
+        click_position = Wait(self.marionette).until(lambda _: self.click_position,
+                                                     message="No click event has been detected")
+        self.assertAlmostEqual(click_position["x"], elem_center_point["x"], delta=1)
+        self.assertAlmostEqual(click_position["y"], elem_center_point["y"], delta=1)
+
+    def test_click_element_larger_than_viewport_with_center_point_inside(self):
+        self.marionette.navigate(inline("""
+          <div id="div" style="width: 150vw; height: 150vh; background: green;"
+               onclick="window.click_x = event.clientX; window.click_y = event.clientY" />
+        """))
+        elem = self.marionette.find_element(By.ID, "div")
+        elem_center_point = self.get_element_center_point(elem)
+
+        self.action.move(elem).click().perform()
+        click_position = Wait(self.marionette).until(lambda _: self.click_position,
+                                                     message="No click event has been detected")
+        self.assertAlmostEqual(click_position["x"], elem_center_point["x"], delta=1)
+        self.assertAlmostEqual(click_position["y"], elem_center_point["y"], delta=1)
+
+    def test_click_element_larger_than_viewport_with_center_point_outside(self):
+        self.marionette.navigate(inline("""
+          <div id="div" style="width: 300vw; height: 300vh; background: green;"
+               onclick="window.click_x = event.clientX; window.click_y = event.clientY" />
+        """))
+        elem = self.marionette.find_element(By.ID, "div")
+
+        with self.assertRaises(errors.MoveTargetOutOfBoundsException):
+            self.action.move(elem).click().perform()
diff --git a/testing/marionette/harness/marionette_harness/tests/unit/unit-tests.ini b/testing/marionette/harness/marionette_harness/tests/unit/unit-tests.ini
--- a/testing/marionette/harness/marionette_harness/tests/unit/unit-tests.ini
+++ b/testing/marionette/harness/marionette_harness/tests/unit/unit-tests.ini
@@ -98,16 +98,18 @@ skip-if = manage_instance == false || ap
 skip-if = manage_instance == false || appname == 'fennec' # Bug 1298921
 [test_with_using_context.py]
 
 [test_modal_dialogs.py]
 skip-if = appname == 'fennec' # Bug 1325738
 [test_key_actions.py]
 [test_legacy_mouse_action.py]
 skip-if = appname == 'fennec'
+[test_mouse_action.py]
+skip-if = appname == 'fennec'
 [test_teardown_context_preserved.py]
 [test_file_upload.py]
 skip-if = appname == 'fennec' || os == "win" # http://bugs.python.org/issue14574
 
 [test_execute_sandboxes.py]
 [test_prefs.py]
 [test_prefs_enforce.py]
 skip-if = manage_instance == false || appname == 'fennec' # Bug 1298921
diff --git a/testing/marionette/listener.js b/testing/marionette/listener.js
--- a/testing/marionette/listener.js
+++ b/testing/marionette/listener.js
@@ -781,17 +781,19 @@ function createATouch(el, corx, cory, to
  * Perform a series of grouped actions at the specified points in time.
  *
  * @param {obj} msg
  *      Object with an |actions| attribute that is an Array of objects
  *      each of which represents an action sequence.
  */
 async function performActions(msg) {
   let chain = action.Chain.fromJSON(msg.actions);
-  await action.dispatch(chain, curContainer.frame);
+  await action.dispatch(chain, curContainer.frame,
+      !capabilities.get("moz:useNonSpecCompliantPointerOrigin"),
+  );
 }
 
 /**
  * The release actions command is used to release all the keys and pointer
  * buttons that are currently depressed. This causes events to be fired
  * as if the state was released by an explicit series of actions. It also
  * clears all the internal state of the virtual devices.
  */
diff --git a/testing/marionette/session.js b/testing/marionette/session.js
--- a/testing/marionette/session.js
+++ b/testing/marionette/session.js
@@ -359,16 +359,17 @@ session.Capabilities = class extends Map
       // features
       ["rotatable", appinfo.name == "B2G"],
 
       // proprietary
       ["moz:accessibilityChecks", false],
       ["moz:headless", Cc["@mozilla.org/gfx/info;1"].getService(Ci.nsIGfxInfo).isHeadless],
       ["moz:processID", Services.appinfo.processID],
       ["moz:profile", maybeProfile()],
+      ["moz:useNonSpecCompliantPointerOrigin", false],
       ["moz:webdriverClick", true],
     ]);
   }
 
   /**
    * @param {string} key
    *     Capability key.
    * @param {(string|number|boolean)} value
@@ -446,25 +447,30 @@ session.Capabilities = class extends Map
           matched.set("proxy", proxy);
           break;
 
         case "timeouts":
           let timeouts = session.Timeouts.fromJSON(v);
           matched.set("timeouts", timeouts);
           break;
 
+        case "moz:accessibilityChecks":
+          assert.boolean(v);
+          matched.set("moz:accessibilityChecks", v);
+          break;
+
+        case "moz:useNonSpecCompliantPointerOrigin":
+          assert.boolean(v);
+          matched.set("moz:useNonSpecCompliantPointerOrigin", v);
+          break;
+
         case "moz:webdriverClick":
           assert.boolean(v);
           matched.set("moz:webdriverClick", v);
           break;
-
-        case "moz:accessibilityChecks":
-          assert.boolean(v);
-          matched.set("moz:accessibilityChecks", v);
-          break;
       }
     }
 
     return matched;
   }
 };
 
 // Specialisation of |JSON.stringify| that produces JSON-safe object
diff --git a/testing/marionette/test_session.js b/testing/marionette/test_session.js
--- a/testing/marionette/test_session.js
+++ b/testing/marionette/test_session.js
@@ -372,16 +372,17 @@ add_test(function test_Capabilities_ctor
   ok(caps.get("timeouts") instanceof session.Timeouts);
   ok(caps.get("proxy") instanceof session.Proxy);
 
   ok(caps.has("rotatable"));
 
   equal(false, caps.get("moz:accessibilityChecks"));
   ok(caps.has("moz:processID"));
   ok(caps.has("moz:profile"));
+  equal(false, caps.get("moz:useNonSpecCompliantPointerOrigin"));
   equal(true, caps.get("moz:webdriverClick"));
 
   run_next_test();
 });
 
 add_test(function test_Capabilities_toString() {
   equal("[object session.Capabilities]", new session.Capabilities().toString());
 
@@ -401,16 +402,18 @@ add_test(function test_Capabilities_toJS
   deepEqual(caps.get("timeouts").toJSON(), json.timeouts);
   equal(undefined, json.proxy);
 
   equal(caps.get("rotatable"), json.rotatable);
 
   equal(caps.get("moz:accessibilityChecks"), json["moz:accessibilityChecks"]);
   equal(caps.get("moz:processID"), json["moz:processID"]);
   equal(caps.get("moz:profile"), json["moz:profile"]);
+  equal(caps.get("moz:useNonSpecCompliantPointerOrigin"),
+        json["moz:useNonSpecCompliantPointerOrigin"]);
   equal(caps.get("moz:webdriverClick"), json["moz:webdriverClick"]);
 
   run_next_test();
 });
 
 add_test(function test_Capabilities_fromJSON() {
   const {fromJSON} = session.Capabilities;
 
@@ -440,27 +443,36 @@ add_test(function test_Capabilities_from
   let proxyConfig = {proxyType: "manual"};
   caps = fromJSON({proxy: proxyConfig});
   equal("manual", caps.get("proxy").proxyType);
 
   let timeoutsConfig = {implicit: 123};
   caps = fromJSON({timeouts: timeoutsConfig});
   equal(123, caps.get("timeouts").implicit);
 
-  equal(true, caps.get("moz:webdriverClick"));
-  caps = fromJSON({"moz:webdriverClick": true});
-  equal(true, caps.get("moz:webdriverClick"));
-  Assert.throws(() => fromJSON({"moz:webdriverClick": "foo"}));
-  Assert.throws(() => fromJSON({"moz:webdriverClick": 1}));
-
   caps = fromJSON({"moz:accessibilityChecks": true});
   equal(true, caps.get("moz:accessibilityChecks"));
   caps = fromJSON({"moz:accessibilityChecks": false});
   equal(false, caps.get("moz:accessibilityChecks"));
   Assert.throws(() => fromJSON({"moz:accessibilityChecks": "foo"}));
+  Assert.throws(() => fromJSON({"moz:accessibilityChecks": 1}));
+
+  caps = fromJSON({"moz:useNonSpecCompliantPointerOrigin": false});
+  equal(false, caps.get("moz:useNonSpecCompliantPointerOrigin"));
+  caps = fromJSON({"moz:useNonSpecCompliantPointerOrigin": true});
+  equal(true, caps.get("moz:useNonSpecCompliantPointerOrigin"));
+  Assert.throws(() => fromJSON({"moz:useNonSpecCompliantPointerOrigin": "foo"}));
+  Assert.throws(() => fromJSON({"moz:useNonSpecCompliantPointerOrigin": 1}));
+
+  caps = fromJSON({"moz:webdriverClick": true});
+  equal(true, caps.get("moz:webdriverClick"));
+  caps = fromJSON({"moz:webdriverClick": false});
+  equal(false, caps.get("moz:webdriverClick"));
+  Assert.throws(() => fromJSON({"moz:webdriverClick": "foo"}));
+  Assert.throws(() => fromJSON({"moz:webdriverClick": 1}));
 
   run_next_test();
 });
 
 // use session.Proxy.toJSON to test marshal
 add_test(function test_marshal() {
   let proxy = new session.Proxy();
 
