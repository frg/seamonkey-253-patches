# HG changeset patch
# User Alexandre Poirot <poirot.alex@gmail.com>
# Date 1519309325 28800
# Node ID 214a9d4099a6907aeeab59417162ef008ad0de0f
# Parent  8ef7eceaa5248dbad24c762f7cada72efad597fb
Bug 1440322 - Use async/await instead of Task.jsm on devtools/shared/fronts/css-properties.js. r=ochameau
[Fixed Mochi tests] r=?

MozReview-Commit-ID: 3NM90kIzqgd

diff --git a/devtools/client/inspector/inspector.js b/devtools/client/inspector/inspector.js
--- a/devtools/client/inspector/inspector.js
+++ b/devtools/client/inspector/inspector.js
@@ -145,18 +145,17 @@ function Inspector(toolbox) {
 Inspector.prototype = {
   /**
    * open is effectively an asynchronous constructor
    */
   init: Task.async(function* () {
     // Localize all the nodes containing a data-localization attribute.
     localizeMarkup(this.panelDoc);
 
-    this._cssPropertiesLoaded = initCssProperties(this.toolbox);
-    yield this._cssPropertiesLoaded;
+    this._cssProperties = yield initCssProperties(this.toolbox);
     yield this.target.makeRemote();
     yield this._getPageStyle();
 
     // This may throw if the document is still loading and we are
     // refering to a dead about:blank document
     let defaultSelection = yield this._getDefaultNodeForSelection()
       .catch(this._handleRejectionIfNotDestroyed);
 
@@ -1264,21 +1263,17 @@ Inspector.prototype = {
     if (this.fontinspector) {
       this.fontinspector.destroy();
     }
 
     if (this.animationinspector) {
       this.animationinspector.destroy();
     }
 
-    let cssPropertiesDestroyer = this._cssPropertiesLoaded.then(({front}) => {
-      if (front) {
-        front.destroy();
-      }
-    });
+    let cssPropertiesDestroyer = this._cssProperties.front.destroy();
 
     this.sidebar.off("select", this.onSidebarSelect);
     let sidebarDestroyer = this.sidebar.destroy();
 
     let ruleViewSideBarDestroyer = this.ruleViewSideBar ?
       this.ruleViewSideBar.destroy() : null;
 
     this.teardownSplitter();
diff --git a/devtools/shared/fronts/css-properties.js b/devtools/shared/fronts/css-properties.js
--- a/devtools/shared/fronts/css-properties.js
+++ b/devtools/shared/fronts/css-properties.js
@@ -6,17 +6,16 @@
 loader.lazyRequireGetter(this, "CSS_PROPERTIES_DB",
   "devtools/shared/css/properties-db", true);
 
 loader.lazyRequireGetter(this, "cssColors",
   "devtools/shared/css/color-db", true);
 
 const { FrontClassWithSpec, Front } = require("devtools/shared/protocol");
 const { cssPropertiesSpec } = require("devtools/shared/specs/css-properties");
-const { Task } = require("devtools/shared/task");
 
 /**
  * Build up a regular expression that matches a CSS variable token. This is an
  * ident token that starts with two dashes "--".
  *
  * https://www.w3.org/TR/css-syntax-3/#ident-token-diagram
  */
 var NON_ASCII = "[^\\x00-\\x7F]";
@@ -223,38 +222,38 @@ CssProperties.prototype = {
  * is potentially async and should be handled up-front when the tool is created.
  *
  * The front is returned only with this function so that it can be destroyed
  * once the toolbox is destroyed.
  *
  * @param {Toolbox} The current toolbox.
  * @returns {Promise} Resolves to {cssProperties, cssPropertiesFront}.
  */
-const initCssProperties = Task.async(function* (toolbox) {
+const initCssProperties = async function (toolbox) {
   const client = toolbox.target.client;
   if (cachedCssProperties.has(client)) {
     return cachedCssProperties.get(client);
   }
 
   let db, front;
 
   // Get the list dynamically if the cssProperties actor exists.
   if (toolbox.target.hasActor("cssProperties")) {
     front = CssPropertiesFront(client, toolbox.target.form);
-    db = yield front.getCSSDatabase();
+    db = await front.getCSSDatabase();
   } else {
     // The target does not support this actor, so require a static list of supported
     // properties.
     db = CSS_PROPERTIES_DB;
   }
 
   const cssProperties = new CssProperties(normalizeCssData(db));
   cachedCssProperties.set(client, {cssProperties, front});
   return {cssProperties, front};
-});
+};
 
 /**
  * Synchronously get a cached and initialized CssProperties.
  *
  * @param {Toolbox} The current toolbox.
  * @returns {CssProperties}
  */
 function getCssProperties(toolbox) {
