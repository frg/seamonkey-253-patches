# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1517577693 18000
#      Fri Feb 02 08:21:33 2018 -0500
# Node ID 41a8ffeb9c9f5a86cb51d8c9bf16a2c21be9b08d
# Parent  fd5cde4290ec98d5ab7bf02f0761e870d9597dc8
Bug 1435138 part 4.  Remove nsIDOMSVGLength.  r=qdot

MozReview-Commit-ID: HwKT9Bdby6F

diff --git a/dom/interfaces/svg/moz.build b/dom/interfaces/svg/moz.build
--- a/dom/interfaces/svg/moz.build
+++ b/dom/interfaces/svg/moz.build
@@ -4,13 +4,12 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 with Files("**"):
     BUG_COMPONENT = ("Core", "SVG")
 
 XPIDL_SOURCES += [
     'nsIDOMSVGElement.idl',
-    'nsIDOMSVGLength.idl',
 ]
 
 XPIDL_MODULE = 'dom_svg'
 
diff --git a/dom/interfaces/svg/nsIDOMSVGLength.idl b/dom/interfaces/svg/nsIDOMSVGLength.idl
deleted file mode 100644
--- a/dom/interfaces/svg/nsIDOMSVGLength.idl
+++ /dev/null
@@ -1,15 +0,0 @@
-/* -*- Mode: IDL; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
-/* This Source Code Form is subject to the terms of the Mozilla Public
- * License, v. 2.0. If a copy of the MPL was not distributed with this
- * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
-
-#include "domstubs.idl"
-
-[uuid(2596325c-aed0-487e-96a1-0a6d589b9c6b)]
-interface nsIDOMSVGLength : nsISupports
-{ 
-};
-
-
-
-
diff --git a/dom/svg/DOMSVGLength.cpp b/dom/svg/DOMSVGLength.cpp
--- a/dom/svg/DOMSVGLength.cpp
+++ b/dom/svg/DOMSVGLength.cpp
@@ -6,17 +6,16 @@
 
 #include "DOMSVGLength.h"
 #include "DOMSVGLengthList.h"
 #include "DOMSVGAnimatedLengthList.h"
 #include "SVGLength.h"
 #include "SVGAnimatedLengthList.h"
 #include "nsSVGElement.h"
 #include "nsSVGLength2.h"
-#include "nsIDOMSVGLength.h"
 #include "nsError.h"
 #include "nsMathUtils.h"
 #include "mozilla/dom/SVGLengthBinding.h"
 #include "mozilla/FloatingPoint.h"
 #include "nsSVGAttrTearoffTable.h"
 
 // See the architecture comment in DOMSVGAnimatedLengthList.h.
 
@@ -52,17 +51,16 @@ NS_IMPL_CYCLE_COLLECTION_TRACE_PRESERVED
 NS_IMPL_CYCLE_COLLECTION_TRACE_END
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(DOMSVGLength)
 NS_IMPL_CYCLE_COLLECTING_RELEASE(DOMSVGLength)
 
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(DOMSVGLength)
   NS_WRAPPERCACHE_INTERFACE_MAP_ENTRY
   NS_INTERFACE_MAP_ENTRY(mozilla::DOMSVGLength) // pseudo-interface
-  NS_INTERFACE_MAP_ENTRY(nsIDOMSVGLength)
   NS_INTERFACE_MAP_ENTRY(nsISupports)
 NS_INTERFACE_MAP_END
 
 //----------------------------------------------------------------------
 // Helper class: AutoChangeLengthNotifier
 // Stack-based helper class to pair calls to WillChangeLengthList and
 // DidChangeLengthList.
 class MOZ_RAII AutoChangeLengthNotifier
diff --git a/dom/svg/DOMSVGLength.h b/dom/svg/DOMSVGLength.h
--- a/dom/svg/DOMSVGLength.h
+++ b/dom/svg/DOMSVGLength.h
@@ -5,17 +5,16 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef MOZILLA_DOMSVGLENGTH_H__
 #define MOZILLA_DOMSVGLENGTH_H__
 
 #include "DOMSVGLengthList.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsDebug.h"
-#include "nsIDOMSVGLength.h"
 #include "nsTArray.h"
 #include "SVGLength.h"
 #include "mozilla/Attributes.h"
 #include "nsWrapperCache.h"
 
 class nsSVGElement;
 
 // We make DOMSVGLength a pseudo-interface to allow us to QI to it in order to
@@ -69,33 +68,32 @@ class ErrorResult;
  * type can find their corresponding internal SVGLength.
  *
  * To use these classes for <length> attributes as well as <list-of-length>
  * attributes, we would need to take a bit from mListIndex and use that to
  * indicate whether the object belongs to a list or non-list attribute, then
  * if-else as appropriate. The bug for doing that work is:
  * https://bugzilla.mozilla.org/show_bug.cgi?id=571734
  */
-class DOMSVGLength final : public nsIDOMSVGLength,
+class DOMSVGLength final : public nsISupports,
                            public nsWrapperCache
 {
   friend class AutoChangeLengthNotifier;
 
   /**
    * Ctor for creating the object returned by nsSVGLength2::ToDOMBaseVal/ToDOMAnimVal
    */
   DOMSVGLength(nsSVGLength2* aVal, nsSVGElement* aSVGElement, bool aAnimVal);
 
   ~DOMSVGLength();
 
 public:
   NS_DECLARE_STATIC_IID_ACCESSOR(MOZILLA_DOMSVGLENGTH_IID)
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
   NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS(DOMSVGLength)
-  NS_DECL_NSIDOMSVGLENGTH
 
   /**
    * Generic ctor for DOMSVGLength objects that are created for an attribute.
    */
   DOMSVGLength(DOMSVGLengthList *aList,
                uint8_t aAttrEnum,
                uint32_t aListIndex,
                bool aIsAnimValItem);
diff --git a/dom/svg/SVGLength.h b/dom/svg/SVGLength.h
--- a/dom/svg/SVGLength.h
+++ b/dom/svg/SVGLength.h
@@ -3,17 +3,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef MOZILLA_SVGLENGTH_H__
 #define MOZILLA_SVGLENGTH_H__
 
 #include "nsDebug.h"
-#include "nsIDOMSVGLength.h"
 #include "nsMathUtils.h"
 #include "mozilla/FloatingPoint.h"
 #include "mozilla/dom/SVGLengthBinding.h"
 
 class nsSVGElement;
 
 namespace mozilla {
 
diff --git a/dom/svg/nsSVGAttrTearoffTable.h b/dom/svg/nsSVGAttrTearoffTable.h
--- a/dom/svg/nsSVGAttrTearoffTable.h
+++ b/dom/svg/nsSVGAttrTearoffTable.h
@@ -8,17 +8,17 @@
 #define NS_SVGATTRTEAROFFTABLE_H_
 
 #include "nsDataHashtable.h"
 #include "nsDebug.h"
 #include "nsHashKeys.h"
 
 /**
  * Global hashmap to associate internal SVG data types (e.g. nsSVGLength2) with
- * DOM tear-off objects (e.g. nsIDOMSVGLength). This allows us to always return
+ * DOM tear-off objects (e.g. DOMSVGLength). This allows us to always return
  * the same object for subsequent requests for DOM objects.
  *
  * We don't keep an owning reference to the tear-off objects so they are
  * responsible for removing themselves from this table when they die.
  */
 template<class SimpleType, class TearoffType>
 class nsSVGAttrTearoffTable
 {
diff --git a/dom/svg/nsSVGLength2.h b/dom/svg/nsSVGLength2.h
--- a/dom/svg/nsSVGLength2.h
+++ b/dom/svg/nsSVGLength2.h
@@ -8,17 +8,16 @@
 #define __NS_SVGLENGTH2_H__
 
 #include "mozilla/Attributes.h"
 #include "mozilla/UniquePtr.h"
 #include "mozilla/dom/SVGLengthBinding.h"
 #include "nsCoord.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsError.h"
-#include "nsIDOMSVGLength.h"
 #include "nsISMILAttr.h"
 #include "nsMathUtils.h"
 #include "nsSVGElement.h"
 #include "SVGContentUtils.h"
 #include "mozilla/gfx/Rect.h"
 
 class nsIFrame;
 class nsSMILValue;
diff --git a/image/SVGDocumentWrapper.cpp b/image/SVGDocumentWrapper.cpp
--- a/image/SVGDocumentWrapper.cpp
+++ b/image/SVGDocumentWrapper.cpp
@@ -7,17 +7,16 @@
 
 #include "mozilla/dom/DocumentTimeline.h"
 #include "mozilla/dom/Element.h"
 #include "nsICategoryManager.h"
 #include "nsIChannel.h"
 #include "nsIContentViewer.h"
 #include "nsIDocument.h"
 #include "nsIDocumentLoaderFactory.h"
-#include "nsIDOMSVGLength.h"
 #include "nsIHttpChannel.h"
 #include "nsIObserverService.h"
 #include "nsIParser.h"
 #include "nsIPresShell.h"
 #include "nsIRequest.h"
 #include "nsIStreamListener.h"
 #include "nsIXMLContentSink.h"
 #include "nsNetCID.h"
diff --git a/js/xpconnect/tests/mochitest/test_bug790732.html b/js/xpconnect/tests/mochitest/test_bug790732.html
--- a/js/xpconnect/tests/mochitest/test_bug790732.html
+++ b/js/xpconnect/tests/mochitest/test_bug790732.html
@@ -33,17 +33,16 @@ https://bugzilla.mozilla.org/show_bug.cg
   is(Ci.nsIDOMMouseScrollEvent, MouseScrollEvent);
   is(Ci.nsIDOMMutationEvent, MutationEvent);
   // XXX We can't test this here because it's only exposed to chrome
   //is(Ci.nsIDOMSimpleGestureEvent, SimpleGestureEvent);
   is(Ci.nsIDOMUIEvent, UIEvent);
   is(Ci.nsIDOMHTMLMediaElement, HTMLMediaElement);
   is(Ci.nsIDOMOfflineResourceList, OfflineResourceList);
   is(Ci.nsIDOMRange, Range);
-  is(Ci.nsIDOMSVGLength, SVGLength);
   is(Ci.nsIDOMNodeFilter, NodeFilter);
   is(Ci.nsIDOMXPathResult, XPathResult);
 
   // Test for Bug 895231
   for (var k of Object.keys(Components.interfaces)) {
     ok(SpecialPowers.Ci.hasOwnProperty(k),
        k + " should be removed from the Components shim");
   }
diff --git a/layout/svg/SVGTextFrame.cpp b/layout/svg/SVGTextFrame.cpp
--- a/layout/svg/SVGTextFrame.cpp
+++ b/layout/svg/SVGTextFrame.cpp
@@ -20,17 +20,16 @@
 #include "mozilla/gfx/PatternHelpers.h"
 #include "mozilla/Likely.h"
 #include "nsAlgorithm.h"
 #include "nsBidiPresUtils.h"
 #include "nsBlockFrame.h"
 #include "nsCaret.h"
 #include "nsContentUtils.h"
 #include "nsGkAtoms.h"
-#include "nsIDOMSVGLength.h"
 #include "nsISelection.h"
 #include "nsQuickSort.h"
 #include "SVGObserverUtils.h"
 #include "nsSVGOuterSVGFrame.h"
 #include "nsSVGPaintServerFrame.h"
 #include "mozilla/dom/SVGRect.h"
 #include "nsSVGIntegrationUtils.h"
 #include "nsSVGUtils.h"
diff --git a/xpcom/reflect/xptinfo/ShimInterfaceInfo.cpp b/xpcom/reflect/xptinfo/ShimInterfaceInfo.cpp
--- a/xpcom/reflect/xptinfo/ShimInterfaceInfo.cpp
+++ b/xpcom/reflect/xptinfo/ShimInterfaceInfo.cpp
@@ -106,17 +106,16 @@
 #include "nsIDOMRect.h"
 #include "nsIDOMScreen.h"
 #include "nsIDOMScrollAreaEvent.h"
 #include "nsIDOMSerializer.h"
 #include "nsIDOMSimpleGestureEvent.h"
 #include "nsIDOMStyleSheet.h"
 #include "nsIDOMStyleSheetList.h"
 #include "nsIDOMSVGElement.h"
-#include "nsIDOMSVGLength.h"
 #include "nsIDOMText.h"
 #include "nsIDOMTimeEvent.h"
 #include "nsIDOMTimeRanges.h"
 #include "nsIDOMTransitionEvent.h"
 #include "nsIDOMTreeWalker.h"
 #include "nsIDOMUIEvent.h"
 #include "nsIDOMValidityState.h"
 #include "nsIDOMWheelEvent.h"
@@ -240,17 +239,16 @@
 #include "mozilla/dom/ScrollBoxObjectBinding.h"
 #include "mozilla/dom/SelectionBinding.h"
 #include "mozilla/dom/ScrollAreaEventBinding.h"
 #include "mozilla/dom/SimpleGestureEventBinding.h"
 #include "mozilla/dom/StorageEventBinding.h"
 #include "mozilla/dom/StyleSheetBinding.h"
 #include "mozilla/dom/StyleSheetListBinding.h"
 #include "mozilla/dom/SVGElementBinding.h"
-#include "mozilla/dom/SVGLengthBinding.h"
 #include "mozilla/dom/TextBinding.h"
 #include "mozilla/dom/TimeEventBinding.h"
 #include "mozilla/dom/TimeRangesBinding.h"
 #include "mozilla/dom/TransitionEventBinding.h"
 #include "mozilla/dom/TreeBoxObjectBinding.h"
 #include "mozilla/dom/TreeWalkerBinding.h"
 #include "mozilla/dom/UIEventBinding.h"
 #include "mozilla/dom/ValidityStateBinding.h"
@@ -423,17 +421,16 @@ const ComponentsInterfaceShimEntry kComp
   DEFINE_SHIM(Screen),
   DEFINE_SHIM(ScrollAreaEvent),
   DEFINE_SHIM_WITH_CUSTOM_INTERFACE(nsIScrollBoxObject, ScrollBoxObject),
   DEFINE_SHIM_WITH_CUSTOM_INTERFACE(nsIDOMSerializer, XMLSerializer),
   DEFINE_SHIM(SimpleGestureEvent),
   DEFINE_SHIM(StyleSheet),
   DEFINE_SHIM(StyleSheetList),
   DEFINE_SHIM(SVGElement),
-  DEFINE_SHIM(SVGLength),
   DEFINE_SHIM(Text),
   DEFINE_SHIM(TimeEvent),
   DEFINE_SHIM(TimeRanges),
   DEFINE_SHIM(TransitionEvent),
   DEFINE_SHIM_WITH_CUSTOM_INTERFACE(nsITreeBoxObject, TreeBoxObject),
   DEFINE_SHIM(TreeWalker),
   DEFINE_SHIM(UIEvent),
   DEFINE_SHIM(ValidityState),
