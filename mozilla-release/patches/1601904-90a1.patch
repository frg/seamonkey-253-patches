# HG changeset patch
# User Ava Katushka <ava8katushka@gmail.com>
# Date 1620677557 0
# Node ID 0cb172fb490e9882d7af6935f2cef3b75dccb830
# Parent  be7203873bea52bb826c29328aff3f7ed3bd895d
Bug 1601904 - Keep the downloading file while clearing the downloads. r=mtigley

Differential Revision: https://phabricator.services.mozilla.com/D113969

diff --git a/toolkit/components/downloads/DownloadCore.jsm b/toolkit/components/downloads/DownloadCore.jsm
--- a/toolkit/components/downloads/DownloadCore.jsm
+++ b/toolkit/components/downloads/DownloadCore.jsm
@@ -938,16 +938,21 @@ this.Download.prototype = {
 
   /**
    * True if the "finalize" method has been called.  This prevents the download
    * from starting again after having been stopped.
    */
   _finalized: false,
 
   /**
+   * True if the "finalize" has been called and fully finished it's execution.
+   */
+  _finalizeExecuted: false,
+
+  /**
    * Ensures that the download is stopped, and optionally removes any partial
    * data kept as part of a canceled or failed download.  After this method has
    * been called, the download cannot be started again.
    *
    * This method should be used in place of "cancel" and removePartialData while
    * shutting down or disposing of the download object, to prevent other callers
    * from interfering with the operation.  This is required because cancellation
    * and other operations are asynchronous.
@@ -959,26 +964,34 @@ this.Download.prototype = {
    * @return {Promise}
    * @resolves When the operation has finished successfully.
    * @rejects JavaScript exception if an error occurred while removing the
    *          partially downloaded data.
    */
   finalize(aRemovePartialData) {
     // Prevents the download from starting again after having been stopped.
     this._finalized = true;
+    let promise;
 
     if (aRemovePartialData) {
       // Cancel the download, in case it is currently in progress, then remove
       // any partially downloaded data.  The removal operation waits for
       // cancellation to be completed before resolving the promise it returns.
       this.cancel();
-      return this.removePartialData();
+      promise = this.removePartialData();
+    } else {
+      // Just cancel the download, in case it is currently in progress.
+      promise = this.cancel();
     }
-    // Just cancel the download, in case it is currently in progress.
-    return this.cancel();
+    promise.then(() => {
+      // At this point, either removing data / just cancelling the download should be done.
+      this._finalizeExecuted = true;
+    });
+
+    return promise;
   },
 
   /**
    * Indicates the time of the last progress notification, expressed as the
    * number of milliseconds since January 1, 1970, 00:00:00 UTC.  This is zero
    * until some bytes have actually been transferred.
    */
   _lastProgressTimeMs: 0,
diff --git a/toolkit/components/downloads/DownloadList.jsm b/toolkit/components/downloads/DownloadList.jsm
--- a/toolkit/components/downloads/DownloadList.jsm
+++ b/toolkit/components/downloads/DownloadList.jsm
@@ -215,21 +215,33 @@ this.DownloadList.prototype = {
         // Remove downloads that have been canceled, even if the cancellation
         // operation hasn't completed yet so we don't check "stopped" here.
         // Failed downloads with partial data are also removed.
         if (download.stopped && (!download.hasPartialData || download.error) &&
             (!aFilterFn || aFilterFn(download))) {
           // Remove the download first, so that the views don't get the change
           // notifications that may occur during finalization.
           await this.remove(download);
+          // Find if a file with the same path is also downloading.
+          let sameFileIsDownloading = false;
+          for (let otherDownload of await this.getAll()) {
+            if (
+              download !== otherDownload &&
+              download.target.path == otherDownload.target.path &&
+              !otherDownload.error
+            ) {
+              sameFileIsDownloading = true;
+            }
+          }
           // Ensure that the download is stopped and no partial data is kept.
           // This works even if the download state has changed meanwhile.  We
           // don't need to wait for the procedure to be complete before
           // processing the other downloads in the list.
-          download.finalize(true).catch(Cu.reportError);
+          let removePartialData = !sameFileIsDownloading;
+          download.finalize(removePartialData).catch(Cu.reportError);
         }
       }
     })().catch(Cu.reportError);
   },
 };
 
 /**
  * Provides a unified, unordered list combining public and private downloads.
diff --git a/toolkit/components/downloads/test/unit/test_DownloadList.js b/toolkit/components/downloads/test/unit/test_DownloadList.js
--- a/toolkit/components/downloads/test/unit/test_DownloadList.js
+++ b/toolkit/components/downloads/test/unit/test_DownloadList.js
@@ -430,16 +430,69 @@ add_task(async function test_removeFinis
   list.removeFinished();
   await deferred.promise;
 
   let downloads = await list.getAll();
   Assert.equal(downloads.length, 1);
 });
 
 /**
+ * Tests that removeFinished method keeps the file that is currently downloading,
+ * even if it needs to remove failed download of the same file.
+ */
+add_task(async function test_removeFinished_keepsDownloadingFile() {
+  let targetFile = getTempFile(TEST_TARGET_FILE_NAME);
+
+  let oneDownload = await Downloads.createDownload({
+    source: httpUrl("empty.txt"),
+    target: targetFile.path,
+  });
+
+  let otherDownload = await Downloads.createDownload({
+    source: httpUrl("empty.txt"),
+    target: targetFile.path,
+  });
+
+  let list = await promiseNewList();
+  await list.add(oneDownload);
+  await list.add(otherDownload);
+
+  let deferred = PromiseUtils.defer();
+  let downloadView = {
+    async onDownloadRemoved(aDownload) {
+      Assert.equal(aDownload, oneDownload);
+      await BrowserTestUtils.waitForCondition(() => oneDownload._finalizeExecuted);
+      deferred.resolve();
+    },
+  };
+  await list.addView(downloadView);
+
+  await oneDownload.start();
+  await otherDownload.start();
+
+  oneDownload.hasPartialData = otherDownload.hasPartialData = true;
+  oneDownload.error = "Download failed";
+
+  list.removeFinished();
+  await deferred.promise;
+
+  let downloads = await list.getAll();
+  Assert.equal(
+    downloads.length,
+    1,
+    "Failed download should be removed, active download should be kept"
+  );
+
+  Assert.ok(
+    await OS.File.exists(otherDownload.target.path),
+    "The file should not have been deleted."
+  );
+});
+
+/**
  * Tests the global DownloadSummary objects for the public, private, and
  * combined download lists.
  */
 add_task(async function test_DownloadSummary() {
   mustInterruptResponses();
 
   let publicList = await promiseNewList();
   let privateList = await Downloads.getList(Downloads.PRIVATE);
