# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1516804629 -3600
# Node ID 37cd2d413696175be0a6ea9359feb1aa3c425dd0
# Parent  1025e76559180b1a7f6752f8a292f297b21fc9ed
Bug 1432771 - Clear queues in dispatchMessagesClear; r=bgrins.

In the console, to batch messages addition, we use queues which are
then consumed once every 50ms. But if a message is pushed to a queue,
and dispatchMessagesClear is called before the 50ms interval, the message
would still be added to the output.
This is happening for example when calling the `clear` helper from jsterm.
Clearing the queues when calling dispatchMessagesClear fixes the issue.
A mochitest is added to make sure we don't regress this.

MozReview-Commit-ID: 2CI0yIGSrT

diff --git a/devtools/client/webconsole/new-console-output/new-console-output-wrapper.js b/devtools/client/webconsole/new-console-output/new-console-output-wrapper.js
--- a/devtools/client/webconsole/new-console-output/new-console-output-wrapper.js
+++ b/devtools/client/webconsole/new-console-output/new-console-output-wrapper.js
@@ -253,16 +253,19 @@ NewConsoleOutputWrapper.prototype = {
     return promise;
   },
 
   dispatchMessagesAdd: function (messages) {
     store.dispatch(actions.messagesAdd(messages));
   },
 
   dispatchMessagesClear: function () {
+    this.queuedMessageAdds = [];
+    this.queuedMessageUpdates = [];
+    this.queuedRequestUpdates = [];
     store.dispatch(actions.messagesClear());
   },
 
   dispatchTimestampsToggle: function (enabled) {
     store.dispatch(actions.timestampsToggle(enabled));
   },
 
   dispatchMessageUpdate: function (message, res) {
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -212,16 +212,17 @@ skip-if = true #       Bug 1403188
 [browser_jsterm_autocomplete_return_key_no_selection.js]
 [browser_jsterm_autocomplete_return_key.js]
 [browser_jsterm_autocomplete-properties-with-non-alphanumeric-names.js]
 [browser_jsterm_completion.js]
 [browser_jsterm_copy_command.js]
 [browser_jsterm_ctrl_key_nav.js]
 skip-if = os != 'mac' # The tested ctrl+key shortcuts are OSX only
 [browser_jsterm_dollar.js]
+[browser_jsterm_helper_clear.js]
 [browser_jsterm_history.js]
 [browser_jsterm_history_persist.js]
 [browser_jsterm_history_nav.js]
 [browser_jsterm_input_expansion.js]
 [browser_jsterm_inspect.js]
 [browser_jsterm_multiline.js]
 [browser_jsterm_no_autocompletion_on_defined_variables.js]
 [browser_jsterm_no_input_and_tab_key_pressed.js]
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_jsterm_helper_clear.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_jsterm_helper_clear.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_jsterm_helper_clear.js
@@ -0,0 +1,22 @@
+/* Any copyright is dedicated to the Public Domain.
+ * http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+const TEST_URI = "data:text/html,Test <code>clear()</code> jsterm helper";
+
+add_task(async function () {
+  let hud = await openNewTabAndConsole(TEST_URI);
+
+  let onMessage = waitForMessage(hud, "message");
+  ContentTask.spawn(gBrowser.selectedBrowser, {}, function () {
+    content.wrappedJSObject.console.log("message");
+  });
+  await onMessage;
+
+  const onCleared = waitFor(() =>
+    hud.jsterm.outputNode.querySelector(".message") === null);
+  hud.jsterm.execute("clear()");
+  await onCleared;
+  ok(true, "Console was cleared");
+});
