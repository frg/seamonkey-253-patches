# HG changeset patch
# User Tim Taubert <ttaubert@mozilla.com>
# Date 1519291662 -3600
# Node ID b1420da362d944178077224087400c16dece2e8a
# Parent  2b09e9751108e0a2485deafdd677c2da735de8ae
Bug 1433015 - Turn AllocReplacement gtests into death tests r=froydnj

diff --git a/xpcom/tests/gtest/TestAllocReplacement.cpp b/xpcom/tests/gtest/TestAllocReplacement.cpp
--- a/xpcom/tests/gtest/TestAllocReplacement.cpp
+++ b/xpcom/tests/gtest/TestAllocReplacement.cpp
@@ -101,39 +101,49 @@ ValidateHookedAllocation(void* (*aAlloca
   rv = manager->GetHeapAllocated(&after);
   if (NS_FAILED(rv)) {
     return false;
   }
 
   return before == after;
 }
 
-TEST(AllocReplacement, malloc_check)
+// We use the "*DeathTest" suffix for all tests in this file to ensure they
+// run before other GTests. As noted at the top, this is important because
+// other tests might spawn threads that interfere with heap memory
+// measurements.
+//
+// See <https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md#death-tests>
+// for more information about death tests in the GTest framework.
+TEST(AllocReplacementDeathTest, malloc_check)
 {
   ASSERT_ALLOCATION_HAPPENED([] {
     return malloc(kAllocAmount);
   });
 }
 
-TEST(AllocReplacement, calloc_check)
+// See above for an explanation of the "*DeathTest" suffix used here.
+TEST(AllocReplacementDeathTest, calloc_check)
 {
   ASSERT_ALLOCATION_HAPPENED([] {
     return calloc(1, kAllocAmount);
   });
 }
 
-TEST(AllocReplacement, realloc_check)
+// See above for an explanation of the "*DeathTest" suffix used here.
+TEST(AllocReplacementDeathTest, realloc_check)
 {
   ASSERT_ALLOCATION_HAPPENED([] {
     return realloc(nullptr, kAllocAmount);
   });
 }
 
 #if defined(HAVE_POSIX_MEMALIGN)
-TEST(AllocReplacement, posix_memalign_check)
+// See above for an explanation of the "*DeathTest" suffix used here.
+TEST(AllocReplacementDeathTest, posix_memalign_check)
 {
   ASSERT_ALLOCATION_HAPPENED([] {
     void* p = nullptr;
     int result = posix_memalign(&p, sizeof(void*), kAllocAmount);
     if (result != 0) {
       return static_cast<void*>(nullptr);
     }
     return p;
@@ -145,25 +155,27 @@ TEST(AllocReplacement, posix_memalign_ch
 #include <windows.h>
 
 #undef ASSERT_ALLOCATION_HAPPENED
 #define ASSERT_ALLOCATION_HAPPENED(lambda)      \
   ALLOCATION_ASSERT(ValidateHookedAllocation(lambda, [](void* p) { \
     HeapFree(GetProcessHeap(), 0, p); \
   }));
 
-TEST(AllocReplacement, HeapAlloc_check)
+// See above for an explanation of the "*DeathTest" suffix used here.
+TEST(AllocReplacementDeathTest, HeapAlloc_check)
 {
   ASSERT_ALLOCATION_HAPPENED([] {
     HANDLE h = GetProcessHeap();
     return HeapAlloc(h, 0, kAllocAmount);
   });
 }
 
-TEST(AllocReplacement, HeapReAlloc_check)
+// See above for an explanation of the "*DeathTest" suffix used here.
+TEST(AllocReplacementDeathTest, HeapReAlloc_check)
 {
   ASSERT_ALLOCATION_HAPPENED([] {
     HANDLE h = GetProcessHeap();
     void *p = HeapAlloc(h, 0, kAllocAmount / 2);
 
     if (!p) {
       return static_cast<void*>(nullptr);
     }

