# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1516806093 18000
# Node ID b7004af91c5b833f80a11e8a6b1cd2ef9ad37116
# Parent  bd0fe42d0b1a66b2a70f599485ea29888d04506b
Bug 1432472 - Do not assume CompositorBridgeChild is available for FlushAsyncPaints. r=rhunt

CompositorBridgeChild may be lost at any time due to a GPU process
crash. Additionally it may be already destroyed due to shutdown being
initiated. For FlushAsyncPaints, we can safely ignore the missing child
because the subsequent operations will generally fail and we will
recover when the GPU process is respawned (or switched to the UI
process).

diff --git a/gfx/layers/client/ClientLayerManager.cpp b/gfx/layers/client/ClientLayerManager.cpp
--- a/gfx/layers/client/ClientLayerManager.cpp
+++ b/gfx/layers/client/ClientLayerManager.cpp
@@ -220,17 +220,17 @@ ClientLayerManager::CreateReadbackLayer(
   RefPtr<ReadbackLayer> layer = new ClientReadbackLayer(this);
   return layer.forget();
 }
 
 bool
 ClientLayerManager::BeginTransactionWithTarget(gfxContext* aTarget)
 {
   // Wait for any previous async paints to complete before starting to paint again.
-  GetCompositorBridgeChild()->FlushAsyncPaints();
+  FlushAsyncPaints();
 
   MOZ_ASSERT(mForwarder, "ClientLayerManager::BeginTransaction without forwarder");
   if (!mForwarder->IPCOpen()) {
     gfxCriticalNote << "ClientLayerManager::BeginTransaction with IPC channel down. GPU process may have died.";
     return false;
   }
 
   mInTransaction = true;
@@ -400,17 +400,17 @@ ClientLayerManager::EndTransaction(DrawP
   if (!mForwarder->IPCOpen()) {
     mInTransaction = false;
     return;
   }
 
   if (mTransactionIncomplete) {
     // If the previous transaction was incomplete then we may have buffer operations
     // running on the paint thread that haven't finished yet
-    GetCompositorBridgeChild()->FlushAsyncPaints();
+    FlushAsyncPaints();
   }
 
   if (mWidget) {
     mWidget->PrepareWindowEffects();
   }
   EndTransactionInternal(aCallback, aCallbackData, aFlags);
   ForwardTransaction(!(aFlags & END_NO_REMOTE_COMPOSITE));
 
@@ -436,17 +436,17 @@ ClientLayerManager::EndEmptyTransaction(
 
   if (!mRoot || !mForwarder->IPCOpen()) {
     return false;
   }
 
   if (mTransactionIncomplete) {
     // If the previous transaction was incomplete then we may have buffer operations
     // running on the paint thread that haven't finished yet
-    GetCompositorBridgeChild()->FlushAsyncPaints();
+    FlushAsyncPaints();
   }
 
   if (!EndTransactionInternal(nullptr, nullptr, aFlags)) {
     // Return without calling ForwardTransaction. This leaves the
     // ShadowLayerForwarder transaction open; the following
     // EndTransaction will complete it.
     if (PaintThread::Get() && mQueuedAsyncPaints) {
       PaintThread::Get()->EndLayerTransaction(nullptr);
@@ -476,16 +476,25 @@ ClientLayerManager::GetCompositorBridgeC
 {
   if (!XRE_IsParentProcess()) {
     return CompositorBridgeChild::Get();
   }
   return GetRemoteRenderer();
 }
 
 void
+ClientLayerManager::FlushAsyncPaints()
+{
+  CompositorBridgeChild* cbc = GetCompositorBridgeChild();
+  if (cbc) {
+    cbc->FlushAsyncPaints();
+  }
+}
+
+void
 ClientLayerManager::ScheduleComposite()
 {
   mForwarder->Composite();
 }
 
 void
 ClientLayerManager::DidComposite(uint64_t aTransactionId,
                                  const TimeStamp& aCompositeStart,
diff --git a/gfx/layers/client/ClientLayerManager.h b/gfx/layers/client/ClientLayerManager.h
--- a/gfx/layers/client/ClientLayerManager.h
+++ b/gfx/layers/client/ClientLayerManager.h
@@ -299,16 +299,18 @@ private:
   void ClearLayer(Layer* aLayer);
 
   void HandleMemoryPressureLayer(Layer* aLayer);
 
   bool EndTransactionInternal(DrawPaintedLayerCallback aCallback,
                               void* aCallbackData,
                               EndTransactionFlags);
 
+  void FlushAsyncPaints();
+
   LayerRefArray mKeepAlive;
 
   nsIWidget* mWidget;
 
   /* PaintedLayer callbacks; valid at the end of a transaciton,
    * while rendering */
   DrawPaintedLayerCallback mPaintedLayerCallback;
   void *mPaintedLayerCallbackData;
diff --git a/gfx/layers/ipc/CompositorManagerChild.cpp b/gfx/layers/ipc/CompositorManagerChild.cpp
--- a/gfx/layers/ipc/CompositorManagerChild.cpp
+++ b/gfx/layers/ipc/CompositorManagerChild.cpp
@@ -104,17 +104,17 @@ CompositorManagerChild::CreateContentCom
   if (NS_WARN_IF(!sInstance || !sInstance->CanSend())) {
     return false;
   }
 
   CompositorBridgeOptions options = ContentCompositorOptions();
   PCompositorBridgeChild* pbridge =
     sInstance->SendPCompositorBridgeConstructor(options);
   if (NS_WARN_IF(!pbridge)) {
-    return true;
+    return false;
   }
 
   auto bridge = static_cast<CompositorBridgeChild*>(pbridge);
   bridge->InitForContent(aNamespace);
   return true;
 }
 
 /* static */ already_AddRefed<CompositorBridgeChild>
diff --git a/gfx/thebes/DeviceManagerDx.cpp b/gfx/thebes/DeviceManagerDx.cpp
--- a/gfx/thebes/DeviceManagerDx.cpp
+++ b/gfx/thebes/DeviceManagerDx.cpp
@@ -778,17 +778,19 @@ DeviceManagerDx::CreateMLGDevice()
 void
 DeviceManagerDx::ResetDevices()
 {
   // Flush the paint thread before revoking all these singletons. This
   // should ensure that the paint thread doesn't start mixing and matching
   // old and new objects together.
   if (PaintThread::Get()) {
     CompositorBridgeChild* cbc = CompositorBridgeChild::Get();
-    cbc->FlushAsyncPaints();
+    if (cbc) {
+      cbc->FlushAsyncPaints();
+    }
   }
 
   MutexAutoLock lock(mDeviceLock);
 
   mAdapter = nullptr;
   mCompositorAttachments = nullptr;
   mMLGDevice = nullptr;
   mCompositorDevice = nullptr;
