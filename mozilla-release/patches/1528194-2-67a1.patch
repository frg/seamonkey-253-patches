# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1550232747 -32400
# Node ID 20edea24a191284d3547fed803e9faae40192be1
# Parent  32c8a6349996d1bc88fa85cdd73f1e111eb1b76f
Bug 1528194 - Run config.sub when split_target fails when not running it. r=mshal

In bug 1522354, we changed host and target detection to not invoke
config.sub, assuming the output from config.guess would satisfy our
needs in split_target.

It turns out that on some plaforms, that doesn't work out, so, while we
still skip config.sub, we now catch errors from split_target when doing
so, and try again after running config.sub when split_target fails.

Differential Revision: https://phabricator.services.mozilla.com/D19937

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -635,17 +635,17 @@ def split_triplet(triplet, allow_unknown
     # is not specified and emit
     #   CPU_TYPE-OPERATING_SYSTEM
     parts = triplet.split('-', 2)
     if len(parts) == 3:
         cpu, _, os = parts
     elif len(parts) == 2:
         cpu, os = parts
     else:
-        die("Unexpected triplet string: %s" % triplet)
+        raise ValueError("Unexpected triplet string: %s" % triplet)
 
     # Autoconf uses config.sub to validate and canonicalize those triplets,
     # but the granularity of its results has never been satisfying to our
     # use, so we've had our own, different, canonicalization. We've also
     # historically not been very consistent with how we use the canonicalized
     # values. Hopefully, this will help us make things better.
     # The tests are inherited from our decades-old autoconf-based configure,
     # which can probably be improved/cleaned up because they are based on a
@@ -676,17 +676,17 @@ def split_triplet(triplet, allow_unknown
         canonical_os = canonical_kernel = 'NetBSD'
     elif os.startswith('openbsd'):
         canonical_os = canonical_kernel = 'OpenBSD'
     elif os.startswith('solaris'):
         canonical_os = canonical_kernel = 'SunOS'
     elif allow_unknown:
         canonical_os = canonical_kernel = os
     else:
-        die('Unknown OS: %s' % os)
+        raise ValueError('Unknown OS: %s' % os)
 
     # The CPU granularity is probably not enough. Moving more things from
     # old-configure will tell us if we need more
     if cpu.endswith('86') or (cpu.startswith('i') and '86' in cpu):
         canonical_cpu = 'x86'
         endianness = 'little'
     elif cpu in ('x86_64', 'ia64'):
         canonical_cpu = cpu
@@ -726,17 +726,17 @@ def split_triplet(triplet, allow_unknown
         endianness = 'little'
     elif cpu == 'sh4':
         canonical_cpu = 'sh4'
         endianness = 'little'
     elif allow_unknown:
         canonical_cpu = cpu
         endianness = 'unknown'
     else:
-        die('Unknown CPU type: %s' % cpu)
+        raise ValueError('Unknown CPU type: %s' % cpu)
 
     def sanitize(cls, value):
         try:
             return cls(value)
         except (KeyError, ValueError):
             if allow_unknown:
                 return value
             raise
@@ -785,51 +785,69 @@ def config_sub(shell, triplet):
     return subprocess.check_output([shell, config_sub, triplet]).strip()
 
 
 @depends('--host', shell)
 @checking('for host system type', lambda h: h.alias)
 @imports('os')
 @imports('subprocess')
 @imports('sys')
+@imports(_from='__builtin__', _import='ValueError')
 def real_host(value, shell):
     if not value and sys.platform == 'win32':
         arch = (os.environ.get('PROCESSOR_ARCHITEW6432') or
                 os.environ.get('PROCESSOR_ARCHITECTURE'))
         if arch == 'AMD64':
             return split_triplet('x86_64-pc-mingw32')
         elif arch == 'x86':
             return split_triplet('i686-pc-mingw32')
 
     if not value:
         config_guess = os.path.join(os.path.dirname(__file__), '..',
                                     'autoconf', 'config.guess')
         host = subprocess.check_output([shell, config_guess]).strip()
+        try:
+            return split_triplet(host)
+        except ValueError:
+            pass
     else:
-        host = config_sub(shell, value[0])
+        host = value[0]
+
+    host = config_sub(shell, host)
 
-    return split_triplet(host)
+    try:
+        return split_triplet(host)
+    except ValueError as e:
+        die(e.message)
 
 
 host = help_host_target | real_host
 
 
 @depends('--target', real_host, shell)
 @checking('for target system type', lambda t: t.alias)
+@imports(_from='__builtin__', _import='ValueError')
 def real_target(value, host, shell):
     if not value:
         return host
     # If --target was only given a cpu arch, expand it with the
     # non-cpu part of the host.
     target = value[0]
     if '-' not in target:
         cpu, rest = host.alias.split('-', 1)
-        return split_triplet('-'.join((target, rest)))
+        target = '-'.join((target, rest))
+        try:
+            return split_triplet(target)
+        except ValueError:
+            pass
 
-    return split_triplet(config_sub(shell, target))
+    try:
+        return split_triplet(config_sub(shell, target))
+    except ValueError as e:
+        die(e.message)
 
 
 target = help_host_target | real_target
 
 
 @depends(host, target)
 @checking('whether cross compiling')
 def cross_compiling(host, target):
diff --git a/python/mozbuild/mozbuild/test/configure/test_moz_configure.py b/python/mozbuild/mozbuild/test/configure/test_moz_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_moz_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_moz_configure.py
@@ -15,16 +15,18 @@ from common import BaseConfigureTest
 
 
 class TargetTest(BaseConfigureTest):
     def get_target(self, args, env={}):
         if 'linux' in self.HOST:
             platform = 'linux2'
         elif 'mingw' in self.HOST:
             platform = 'win32'
+        elif 'openbsd6' in self.HOST:
+            platform = 'openbsd6'
         else:
             raise Exception('Missing platform for HOST {}'.format(self.HOST))
         wrapped_sys = {}
         exec_('from sys import *', wrapped_sys)
         wrapped_sys['platform'] = platform
         modules = {
             'sys': ReadOnlyNamespace(**wrapped_sys),
         }
@@ -96,16 +98,30 @@ class TestTargetWindows(TargetTest):
         self.assertEqual(
             self.get_target(['--target=x86_64-pc-mingw32']),
             'x86_64-pc-mingw32')
         self.assertEqual(
             self.get_target(['--target=x86_64']),
             'x86_64-pc-mingw32')
 
 
+class TestTargetOpenBSD(TargetTest):
+    # config.guess returns amd64 on OpenBSD, which we need to pass through to
+    # config.sub so that it canonicalizes to x86_64.
+    HOST = 'amd64-unknown-openbsd6.4'
+
+    def test_target(self):
+        self.assertEqual(self.get_target([]), 'x86_64-unknown-openbsd6.4')
+
+    def config_sub(self, stdin, args):
+        if args[0] == 'amd64-unknown-openbsd6.4':
+            return 0, 'x86_64-unknown-openbsd6.4', ''
+        return super(TestTargetOpenBSD, self).config_sub(stdin, args)
+
+
 class TestMozConfigure(BaseConfigureTest):
     def test_nsis_version(self):
         this = self
 
         class FakeNSIS(object):
             def __init__(self, version):
                 self.version = version
 
