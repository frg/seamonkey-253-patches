# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1518123875 -3600
# Node ID 766c0dd5ed3da5e7c2bc964aca34d667b15534af
# Parent  d97d4af127ba2a7b2b916c1b40664c3abffcfee4
Bug 1434230 part 3 - Fix Spectre issues related to string character encoding. r=luke

diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -1326,17 +1326,17 @@ PrepareAndExecuteRegExp(JSContext* cx, M
 
         // Check if |lastIndex > 0 && lastIndex < input->length()|.
         // lastIndex should already have no sign here.
         masm.branchTest32(Assembler::Zero, lastIndex, lastIndex, &done);
         masm.loadStringLength(input, temp2);
         masm.branch32(Assembler::AboveOrEqual, lastIndex, temp2, &done);
 
         // Check if input[lastIndex] is trail surrogate.
-        masm.loadStringChars(input, temp2);
+        masm.loadStringChars(input, temp2, CharEncoding::TwoByte);
         masm.computeEffectiveAddress(BaseIndex(temp2, lastIndex, TimesTwo), temp3);
         masm.load16ZeroExtend(Address(temp3, 0), temp3);
 
         masm.branch32(Assembler::Below, temp3, Imm32(unicode::TrailSurrogateMin), &done);
         masm.branch32(Assembler::Above, temp3, Imm32(unicode::TrailSurrogateMax), &done);
 
         // Check if input[lastIndex-1] is lead surrogate.
         masm.move32(lastIndex, temp3);
@@ -1362,30 +1362,32 @@ PrepareAndExecuteRegExp(JSContext* cx, M
         masm.add32(Imm32(1), temp2);
         masm.store32(temp2, pairCountAddress);
     }
 
     // Load the code pointer for the type of input string we have, and compute
     // the input start/end pointers in the InputOutputData.
     Register codePointer = temp1;
     {
-        masm.loadStringChars(input, temp2);
-        masm.storePtr(temp2, inputStartAddress);
         masm.loadStringLength(input, temp3);
 
         Label isLatin1, done;
         masm.branchLatin1String(input, &isLatin1);
         {
+            masm.loadStringChars(input, temp2, CharEncoding::TwoByte);
+            masm.storePtr(temp2, inputStartAddress);
             masm.lshiftPtr(Imm32(1), temp3);
             masm.loadPtr(Address(temp1, RegExpShared::offsetOfTwoByteJitCode(mode)),
                          codePointer);
-        }
-        masm.jump(&done);
+            masm.jump(&done);
+        }
+        masm.bind(&isLatin1);
         {
-            masm.bind(&isLatin1);
+            masm.loadStringChars(input, temp2, CharEncoding::Latin1);
+            masm.storePtr(temp2, inputStartAddress);
             masm.loadPtr(Address(temp1, RegExpShared::offsetOfLatin1JitCode(mode)),
                          codePointer);
         }
         masm.bind(&done);
 
         masm.addPtr(temp3, temp2);
         masm.storePtr(temp2, inputEndAddress);
     }
@@ -1571,17 +1573,18 @@ CreateDependentString::generate(MacroAss
         MOZ_ASSERT(startIndexAddress.base == masm.getStackPointer());
         BaseIndex newStartIndexAddress = startIndexAddress;
         newStartIndexAddress.offset += 2 * sizeof(void*);
 
         // Load chars pointer for the new string.
         masm.loadInlineStringCharsForStore(string, string);
 
         // Load the source characters pointer.
-        masm.loadStringChars(base, temp2);
+        masm.loadStringChars(base, temp2,
+                             latin1 ? CharEncoding::Latin1 : CharEncoding::TwoByte);
         masm.load32(newStartIndexAddress, base);
         if (latin1)
             masm.addPtr(temp2, base);
         else
             masm.computeEffectiveAddress(BaseIndex(temp2, base, TimesTwo), base);
 
         CopyStringChars(masm, string, base, temp1, temp2, latin1 ? 1 : 2, latin1 ? 1 : 2);
 
@@ -1602,17 +1605,18 @@ CreateDependentString::generate(MacroAss
         // Make a dependent string.
         int32_t flags = (latin1 ? JSString::LATIN1_CHARS_BIT : 0) | JSString::DEPENDENT_FLAGS;
 
         masm.newGCString(string, temp2, &fallbacks_[FallbackKind::NotInlineString]);
         masm.bind(&joins_[FallbackKind::NotInlineString]);
         masm.store32(Imm32(flags), Address(string, JSString::offsetOfFlags()));
         masm.store32(temp1, Address(string, JSString::offsetOfLength()));
 
-        masm.loadNonInlineStringChars(base, temp1);
+        masm.loadNonInlineStringChars(base, temp1,
+                                      latin1 ? CharEncoding::Latin1 : CharEncoding::TwoByte);
         masm.load32(startIndexAddress, temp2);
         if (latin1)
             masm.addPtr(temp2, temp1);
         else
             masm.computeEffectiveAddress(BaseIndex(temp1, temp2, TimesTwo), temp1);
         masm.storeNonInlineStringChars(temp1, string);
         masm.storeDependentStringBase(base, string);
 
@@ -2477,26 +2481,26 @@ CodeGenerator::visitGetFirstDollarIndex(
     Register len = ToRegister(ins->temp2());
 
     OutOfLineCode* ool = oolCallVM(GetFirstDollarIndexRawInfo, ins, ArgList(str),
                                    StoreRegisterTo(output));
 
     masm.branchIfRope(str, ool->entry());
     masm.loadStringLength(str, len);
 
-    masm.loadStringChars(str, temp0);
-
     Label isLatin1, done;
     masm.branchLatin1String(str, &isLatin1);
     {
+        masm.loadStringChars(str, temp0, CharEncoding::TwoByte);
         FindFirstDollarIndex(masm, str, len, temp0, temp1, output, /* isLatin1 = */ false);
-    }
-    masm.jump(&done);
+        masm.jump(&done);
+    }
+    masm.bind(&isLatin1);
     {
-        masm.bind(&isLatin1);
+        masm.loadStringChars(str, temp0, CharEncoding::Latin1);
         FindFirstDollarIndex(masm, str, len, temp0, temp1, output, /* isLatin1 = */ true);
     }
     masm.bind(&done);
     masm.bind(ool->rejoin());
 }
 
 typedef JSString* (*StringReplaceFn)(JSContext*, HandleString, HandleString, HandleString);
 static const VMFunction StringFlatReplaceInfo =
@@ -7768,25 +7772,26 @@ static void
 CopyStringCharsMaybeInflate(MacroAssembler& masm, Register input, Register destChars,
                             Register temp1, Register temp2)
 {
     // destChars is TwoByte and input is a Latin1 or TwoByte string, so we may
     // have to inflate.
 
     Label isLatin1, done;
     masm.loadStringLength(input, temp1);
-    masm.loadStringChars(input, temp2);
     masm.branchLatin1String(input, &isLatin1);
     {
+        masm.loadStringChars(input, temp2, CharEncoding::TwoByte);
         masm.movePtr(temp2, input);
         CopyStringChars(masm, destChars, input, temp1, temp2, sizeof(char16_t), sizeof(char16_t));
         masm.jump(&done);
     }
     masm.bind(&isLatin1);
     {
+        masm.loadStringChars(input, temp2, CharEncoding::Latin1);
         masm.movePtr(temp2, input);
         CopyStringChars(masm, destChars, input, temp1, temp2, sizeof(char), sizeof(char16_t));
     }
     masm.bind(&done);
 }
 
 static void
 ConcatInlineString(MacroAssembler& masm, Register lhs, Register rhs, Register output,
@@ -7834,27 +7839,27 @@ ConcatInlineString(MacroAssembler& masm,
 
     {
         // Copy lhs chars. Note that this advances temp2 to point to the next
         // char. This also clobbers the lhs register.
         if (isTwoByte) {
             CopyStringCharsMaybeInflate(masm, lhs, temp2, temp1, temp3);
         } else {
             masm.loadStringLength(lhs, temp3);
-            masm.loadStringChars(lhs, temp1);
+            masm.loadStringChars(lhs, temp1, CharEncoding::Latin1);
             masm.movePtr(temp1, lhs);
             CopyStringChars(masm, temp2, lhs, temp3, temp1, sizeof(char), sizeof(char));
         }
 
         // Copy rhs chars. Clobbers the rhs register.
         if (isTwoByte) {
             CopyStringCharsMaybeInflate(masm, rhs, temp2, temp1, temp3);
         } else {
             masm.loadStringLength(rhs, temp3);
-            masm.loadStringChars(rhs, temp1);
+            masm.loadStringChars(rhs, temp1, CharEncoding::Latin1);
             masm.movePtr(temp1, rhs);
             CopyStringChars(masm, temp2, rhs, temp3, temp1, sizeof(char), sizeof(char));
         }
 
         // Null-terminate.
         if (isTwoByte)
             masm.store16(Imm32(0), Address(temp2, 0));
         else
@@ -7911,36 +7916,40 @@ CodeGenerator::visitSubstr(LSubstr* lir)
     masm.branchTest32(Assembler::Zero, stringFlags, Imm32(JSString::INLINE_CHARS_BIT), &notInline);
     masm.newGCFatInlineString(output, temp, slowPath);
     masm.store32(length, Address(output, JSString::offsetOfLength()));
 
     masm.branchLatin1String(string, &isInlinedLatin1);
     {
         masm.store32(Imm32(JSString::INIT_FAT_INLINE_FLAGS),
                      Address(output, JSString::offsetOfFlags()));
-        masm.loadInlineStringChars(string, temp);
+        masm.loadInlineStringChars(string, temp, CharEncoding::TwoByte);
         if (temp2 == string)
             masm.push(string);
         BaseIndex chars(temp, begin, ScaleFromElemWidth(sizeof(char16_t)));
         masm.computeEffectiveAddress(chars, temp2);
         masm.loadInlineStringCharsForStore(output, temp);
         CopyStringChars(masm, temp, temp2, length, temp3, sizeof(char16_t), sizeof(char16_t));
         masm.load32(Address(output, JSString::offsetOfLength()), length);
         masm.store16(Imm32(0), Address(temp, 0));
         if (temp2 == string)
             masm.pop(string);
         masm.jump(done);
     }
     masm.bind(&isInlinedLatin1);
     {
         masm.store32(Imm32(JSString::INIT_FAT_INLINE_FLAGS | JSString::LATIN1_CHARS_BIT),
                      Address(output, JSString::offsetOfFlags()));
-        if (temp2 == string)
+        if (temp2 == string) {
             masm.push(string);
-        masm.loadInlineStringChars(string, temp2);
+            masm.loadInlineStringChars(string, temp, CharEncoding::Latin1);
+            masm.movePtr(temp, temp2);
+        } else {
+            masm.loadInlineStringChars(string, temp2, CharEncoding::Latin1);
+        }
         static_assert(sizeof(char) == 1, "begin index shouldn't need scaling");
         masm.addPtr(begin, temp2);
         masm.loadInlineStringCharsForStore(output, temp);
         CopyStringChars(masm, temp, temp2, length, temp3, sizeof(char), sizeof(char));
         masm.load32(Address(output, JSString::offsetOfLength()), length);
         masm.store8(Imm32(0), Address(temp, 0));
         if (temp2 == string)
             masm.pop(string);
@@ -7951,27 +7960,27 @@ CodeGenerator::visitSubstr(LSubstr* lir)
     masm.bind(&notInline);
     masm.newGCString(output, temp, slowPath);
     masm.store32(length, Address(output, JSString::offsetOfLength()));
     masm.storeDependentStringBase(string, output);
 
     masm.branchLatin1String(string, &isLatin1);
     {
         masm.store32(Imm32(JSString::DEPENDENT_FLAGS), Address(output, JSString::offsetOfFlags()));
-        masm.loadNonInlineStringChars(string, temp);
+        masm.loadNonInlineStringChars(string, temp, CharEncoding::TwoByte);
         BaseIndex chars(temp, begin, ScaleFromElemWidth(sizeof(char16_t)));
         masm.computeEffectiveAddress(chars, temp);
         masm.storeNonInlineStringChars(temp, output);
         masm.jump(done);
     }
     masm.bind(&isLatin1);
     {
         masm.store32(Imm32(JSString::DEPENDENT_FLAGS | JSString::LATIN1_CHARS_BIT),
                      Address(output, JSString::offsetOfFlags()));
-        masm.loadNonInlineStringChars(string, temp);
+        masm.loadNonInlineStringChars(string, temp, CharEncoding::Latin1);
         static_assert(sizeof(char) == 1, "begin index shouldn't need scaling");
         masm.addPtr(begin, temp);
         masm.storeNonInlineStringChars(temp, output);
         masm.jump(done);
     }
 
     masm.bind(done);
 }
diff --git a/js/src/jit/MacroAssembler.cpp b/js/src/jit/MacroAssembler.cpp
--- a/js/src/jit/MacroAssembler.cpp
+++ b/js/src/jit/MacroAssembler.cpp
@@ -1482,88 +1482,115 @@ MacroAssembler::compareStrings(JSOp op, 
     loadStringLength(left, result);
     branch32(Assembler::Equal, Address(right, JSString::offsetOfLength()), result, fail);
     move32(Imm32(op == JSOP_NE || op == JSOP_STRICTNE), result);
 
     bind(&done);
 }
 
 void
-MacroAssembler::loadStringChars(Register str, Register dest)
+MacroAssembler::loadStringChars(Register str, Register dest, CharEncoding encoding)
 {
     MOZ_ASSERT(str != dest);
 
-    // First, if the string is a rope, zero the |str| register. The code below
-    // depends on str->flags so this should block speculative execution.
     if (JitOptions.spectreStringMitigations) {
-        movePtr(ImmWord(0), dest);
-        test32MovePtr(Assembler::Zero,
-                      Address(str, JSString::offsetOfFlags()), Imm32(JSString::LINEAR_BIT),
-                      dest, str);
+        if (encoding == CharEncoding::Latin1) {
+            // If the string is a rope, zero the |str| register. The code below
+            // depends on str->flags so this should block speculative execution.
+            movePtr(ImmWord(0), dest);
+            test32MovePtr(Assembler::Zero,
+                          Address(str, JSString::offsetOfFlags()), Imm32(JSString::LINEAR_BIT),
+                          dest, str);
+        } else {
+            // If we're loading TwoByte chars, there's an additional risk:
+            // if the string has Latin1 chars, we could read out-of-bounds. To
+            // prevent this, we check both the Linear and Latin1 bits. We don't
+            // have a scratch register, so we use these flags also to block
+            // speculative execution, similar to the use of 0 above.
+            MOZ_ASSERT(encoding == CharEncoding::TwoByte);
+            static constexpr uint32_t Mask = JSString::LINEAR_BIT | JSString::LATIN1_CHARS_BIT;
+            static_assert(Mask < 1024,
+                          "Mask should be a small, near-null value to ensure we "
+                          "block speculative execution when it's used as string "
+                          "pointer");
+            move32(Imm32(Mask), dest);
+            and32(Address(str, JSString::offsetOfFlags()), dest);
+            cmp32MovePtr(Assembler::NotEqual, dest, Imm32(JSString::LINEAR_BIT),
+                         dest, str);
+        }
     }
 
     // Load the inline chars.
     computeEffectiveAddress(Address(str, JSInlineString::offsetOfInlineStorage()), dest);
 
     // If it's not an inline string, load the non-inline chars. Use a
     // conditional move to prevent speculative execution.
     test32LoadPtr(Assembler::Zero,
                   Address(str, JSString::offsetOfFlags()), Imm32(JSString::INLINE_CHARS_BIT),
                   Address(str, JSString::offsetOfNonInlineChars()), dest);
 }
 
 void
-MacroAssembler::loadNonInlineStringChars(Register str, Register dest)
+MacroAssembler::loadNonInlineStringChars(Register str, Register dest, CharEncoding encoding)
 {
     MOZ_ASSERT(str != dest);
 
     if (JitOptions.spectreStringMitigations) {
-        movePtr(ImmWord(0), dest);
-
-        // First, if the string is a rope, zero the |str| register. The code
-        // below depends on str->flags so this should block speculative
-        // execution.
-        test32MovePtr(Assembler::Zero,
-                      Address(str, JSString::offsetOfFlags()), Imm32(JSString::LINEAR_BIT),
-                      dest, str);
-
-        // Load non-inline chars if the inline-chars bit is not set.
-        test32LoadPtr(Assembler::Zero,
-                      Address(str, JSString::offsetOfFlags()), Imm32(JSString::INLINE_CHARS_BIT),
-                      Address(str, JSString::offsetOfNonInlineChars()), dest);
-    } else {
-        loadPtr(Address(str, JSString::offsetOfNonInlineChars()), dest);
+        // If the string is a rope, has inline chars, or has a different
+        // character encoding, set str to a near-null value to prevent
+        // speculative execution below (when reading str->nonInlineChars).
+
+        static constexpr uint32_t Mask =
+            JSString::LINEAR_BIT |
+            JSString::INLINE_CHARS_BIT |
+            JSString::LATIN1_CHARS_BIT;
+        static_assert(Mask < 1024,
+                      "Mask should be a small, near-null value to ensure we "
+                      "block speculative execution when it's used as string "
+                      "pointer");
+
+        uint32_t expectedBits = JSString::LINEAR_BIT;
+        if (encoding == CharEncoding::Latin1)
+            expectedBits |= JSString::LATIN1_CHARS_BIT;
+
+        move32(Imm32(Mask), dest);
+        and32(Address(str, JSString::offsetOfFlags()), dest);
+
+        cmp32MovePtr(Assembler::NotEqual, dest, Imm32(expectedBits),
+                     dest, str);
     }
+
+    loadPtr(Address(str, JSString::offsetOfNonInlineChars()), dest);
 }
 
 void
 MacroAssembler::storeNonInlineStringChars(Register chars, Register str)
 {
     MOZ_ASSERT(chars != str);
     storePtr(chars, Address(str, JSString::offsetOfNonInlineChars()));
 }
 
 void
 MacroAssembler::loadInlineStringCharsForStore(Register str, Register dest)
 {
     computeEffectiveAddress(Address(str, JSInlineString::offsetOfInlineStorage()), dest);
 }
 
 void
-MacroAssembler::loadInlineStringChars(Register str, Register dest)
+MacroAssembler::loadInlineStringChars(Register str, Register dest, CharEncoding encoding)
 {
     MOZ_ASSERT(str != dest);
 
     if (JitOptions.spectreStringMitigations) {
         // Making this Spectre-safe is a bit complicated: using
         // computeEffectiveAddress and then zeroing the output register if
         // non-inline is not sufficient: when the index is very large, it would
         // allow reading |nullptr + index|. Just fall back to loadStringChars
         // for now.
-        loadStringChars(str, dest);
+        loadStringChars(str, dest, encoding);
     } else {
         computeEffectiveAddress(Address(str, JSInlineString::offsetOfInlineStorage()), dest);
     }
 }
 
 void
 MacroAssembler::loadRopeLeftChild(Register str, Register dest)
 {
@@ -1632,26 +1659,26 @@ MacroAssembler::loadStringChar(Register 
     // Todo: Handle index in the rightChild.
     boundsCheck32ForLoad(index, Address(output, JSString::offsetOfLength()), scratch, fail);
 
     // If the left side is another rope, give up.
     branchIfRope(output, fail);
 
     bind(&notRope);
 
-    loadStringChars(output, scratch);
-
     Label isLatin1, done;
     // We have to check the left/right side for ropes,
     // because a TwoByte rope might have a Latin1 child.
     branchLatin1String(output, &isLatin1);
+    loadStringChars(output, scratch, CharEncoding::TwoByte);
     load16ZeroExtend(BaseIndex(scratch, index, TimesTwo), output);
     jump(&done);
 
     bind(&isLatin1);
+    loadStringChars(output, scratch, CharEncoding::Latin1);
     load8ZeroExtend(BaseIndex(scratch, index, TimesOne), output);
 
     bind(&done);
 }
 
 void
 MacroAssembler::loadStringIndexValue(Register str, Register dest, Label* fail)
 {
diff --git a/js/src/jit/MacroAssembler.h b/js/src/jit/MacroAssembler.h
--- a/js/src/jit/MacroAssembler.h
+++ b/js/src/jit/MacroAssembler.h
@@ -224,16 +224,18 @@ enum class CheckUnsafeCallWithABI {
     DontCheckHasExitFrame,
 
     // Don't check this callWithABI uses AutoUnsafeCallWithABI, for instance
     // because we're calling a simple helper function (like malloc or js_free)
     // that we can't change and/or that we know won't GC.
     DontCheckOther,
 };
 
+enum class CharEncoding { Latin1, TwoByte };
+
 // The public entrypoint for emitting assembly. Note that a MacroAssembler can
 // use cx->lifoAlloc, so take care not to interleave masm use with other
 // lifoAlloc use if one will be destroyed before the other.
 class MacroAssembler : public MacroAssemblerSpecific
 {
     MacroAssembler* thisFromCtor() {
         return this;
     }
@@ -1338,16 +1340,20 @@ class MacroAssembler : public MacroAssem
     inline void cmp32Move32(Condition cond, Register lhs, Register rhs, Register src,
                             Register dest)
         DEFINED_ON(arm, arm64, x86_shared);
 
     inline void cmp32Move32(Condition cond, Register lhs, const Address& rhs, Register src,
                             Register dest)
         DEFINED_ON(arm, arm64, x86_shared);
 
+    inline void cmp32MovePtr(Condition cond, Register lhs, Imm32 rhs, Register src,
+                             Register dest)
+        DEFINED_ON(arm, arm64, x86, x64);
+
     inline void test32LoadPtr(Condition cond, const Address& addr, Imm32 mask, const Address& src,
                               Register dest)
         DEFINED_ON(arm, arm64, x86, x64);
 
     inline void test32MovePtr(Condition cond, const Address& addr, Imm32 mask, Register src,
                               Register dest)
         DEFINED_ON(arm, arm64, x86, x64);
 
@@ -1935,23 +1941,23 @@ class MacroAssembler : public MacroAssem
         loadPtr(Address(obj, JSObject::offsetOfGroup()), dest);
         loadPtr(Address(dest, ObjectGroup::offsetOfProto()), dest);
     }
 
     void loadStringLength(Register str, Register dest) {
         load32(Address(str, JSString::offsetOfLength()), dest);
     }
 
-    void loadStringChars(Register str, Register dest);
-
-    void loadNonInlineStringChars(Register str, Register dest);
+    void loadStringChars(Register str, Register dest, CharEncoding encoding);
+
+    void loadNonInlineStringChars(Register str, Register dest, CharEncoding encoding);
     void loadNonInlineStringCharsForStore(Register str, Register dest);
     void storeNonInlineStringChars(Register chars, Register str);
 
-    void loadInlineStringChars(Register str, Register dest);
+    void loadInlineStringChars(Register str, Register dest, CharEncoding encoding);
     void loadInlineStringCharsForStore(Register str, Register dest);
 
     void loadStringChar(Register str, Register index, Register output, Register scratch,
                         Label* fail);
 
     void loadRopeLeftChild(Register str, Register dest);
     void storeRopeChildren(Register left, Register right, Register str);
 
diff --git a/js/src/jit/arm/MacroAssembler-arm-inl.h b/js/src/jit/arm/MacroAssembler-arm-inl.h
--- a/js/src/jit/arm/MacroAssembler-arm-inl.h
+++ b/js/src/jit/arm/MacroAssembler-arm-inl.h
@@ -2140,16 +2140,24 @@ void
 MacroAssembler::cmp32Move32(Condition cond, Register lhs, Register rhs, Register src,
                             Register dest)
 {
     cmp32(lhs, rhs);
     ma_mov(src, dest, LeaveCC, cond);
 }
 
 void
+MacroAssembler::cmp32MovePtr(Condition cond, Register lhs, Imm32 rhs, Register src,
+                             Register dest)
+{
+    cmp32(lhs, rhs);
+    ma_mov(src, dest, LeaveCC, cond);
+}
+
+void
 MacroAssembler::cmp32Move32(Condition cond, Register lhs, const Address& rhs, Register src,
                             Register dest)
 {
     ScratchRegisterScope scratch(*this);
     SecondScratchRegisterScope scratch2(*this);
     ma_ldr(rhs, scratch, scratch2);
     cmp32Move32(cond, lhs, scratch, src, dest);
 }
diff --git a/js/src/jit/arm64/MacroAssembler-arm64-inl.h b/js/src/jit/arm64/MacroAssembler-arm64-inl.h
--- a/js/src/jit/arm64/MacroAssembler-arm64-inl.h
+++ b/js/src/jit/arm64/MacroAssembler-arm64-inl.h
@@ -1722,16 +1722,24 @@ void
 MacroAssembler::cmp32Move32(Condition cond, Register lhs, const Address& rhs, Register src,
                             Register dest)
 {
     cmp32(lhs, rhs);
     Csel(ARMRegister(dest, 32), ARMRegister(src, 32), ARMRegister(dest, 32), cond);
 }
 
 void
+MacroAssembler::cmp32MovePtr(Condition cond, Register lhs, Imm32 rhs, Register src,
+                             Register dest)
+{
+    cmp32(lhs, rhs);
+    Csel(ARMRegister(dest, 64), ARMRegister(src, 64), ARMRegister(dest, 64), cond);
+}
+
+void
 MacroAssembler::test32LoadPtr(Condition cond, const Address& addr, Imm32 mask, const Address& src,
                               Register dest)
 {
     MOZ_ASSERT(cond == Assembler::Zero || cond == Assembler::NonZero);
 
     // ARM64 does not support conditional loads, so we use a branch with a CSel
     // (to prevent Spectre attacks).
     vixl::UseScratchRegisterScope temps(this);
diff --git a/js/src/jit/x64/MacroAssembler-x64-inl.h b/js/src/jit/x64/MacroAssembler-x64-inl.h
--- a/js/src/jit/x64/MacroAssembler-x64-inl.h
+++ b/js/src/jit/x64/MacroAssembler-x64-inl.h
@@ -816,16 +816,24 @@ MacroAssembler::branchTestMagic(Conditio
 
 void
 MacroAssembler::branchToComputedAddress(const BaseIndex& address)
 {
     jmp(Operand(address));
 }
 
 void
+MacroAssembler::cmp32MovePtr(Condition cond, Register lhs, Imm32 rhs, Register src,
+                             Register dest)
+{
+    cmp32(lhs, rhs);
+    cmovCCq(cond, Operand(src), dest);
+}
+
+void
 MacroAssembler::test32LoadPtr(Condition cond, const Address& addr, Imm32 mask, const Address& src,
                               Register dest)
 {
     MOZ_ASSERT(cond == Assembler::Zero || cond == Assembler::NonZero);
     test32(addr, mask);
     cmovCCq(cond, Operand(src), dest);
 }
 
diff --git a/js/src/jit/x86/MacroAssembler-x86-inl.h b/js/src/jit/x86/MacroAssembler-x86-inl.h
--- a/js/src/jit/x86/MacroAssembler-x86-inl.h
+++ b/js/src/jit/x86/MacroAssembler-x86-inl.h
@@ -1005,16 +1005,24 @@ MacroAssembler::branchTestMagic(Conditio
 
 void
 MacroAssembler::branchToComputedAddress(const BaseIndex& addr)
 {
     jmp(Operand(addr));
 }
 
 void
+MacroAssembler::cmp32MovePtr(Condition cond, Register lhs, Imm32 rhs, Register src,
+                             Register dest)
+{
+    cmp32(lhs, rhs);
+    cmovCCl(cond, Operand(src), dest);
+}
+
+void
 MacroAssembler::test32LoadPtr(Condition cond, const Address& addr, Imm32 mask, const Address& src,
                               Register dest)
 {
     MOZ_ASSERT(cond == Assembler::Zero || cond == Assembler::NonZero);
     test32(addr, mask);
     cmovCCl(cond, Operand(src), dest);
 }
 
