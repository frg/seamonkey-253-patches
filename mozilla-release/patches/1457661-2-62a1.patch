# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1527541598 -7200
# Node ID 27562071aaaa1175d70a03bc2f9214fee7e22829
# Parent  b5cfcaea920dee6a100b52fdee336b191dbf1cff
Bug 1457661 - P2. Ensure we call NotifyDataEnded for local resource once size is known. r=bryce

A call to NotifyDataEnded is required even if the size was known when the resource was created. This ensures that the readyState is properly updated and that playback can immediately as no more data can be added once first loaded.

MozReview-Commit-ID: FaJMBxJ9NkM

diff --git a/dom/media/FileMediaResource.cpp b/dom/media/FileMediaResource.cpp
--- a/dom/media/FileMediaResource.cpp
+++ b/dom/media/FileMediaResource.cpp
@@ -14,31 +14,37 @@
 
 namespace mozilla {
 
 void
 FileMediaResource::EnsureSizeInitialized()
 {
   mLock.AssertCurrentThreadOwns();
   NS_ASSERTION(mInput, "Must have file input stream");
-  if (mSizeInitialized) {
+  if (mSizeInitialized && mNotifyDataEndedProcessed) {
     return;
   }
+
+  if (!mSizeInitialized) {
+    // Get the file size and inform the decoder.
+    uint64_t size;
+    nsresult res = mInput->Available(&size);
+    if (NS_SUCCEEDED(res) && size <= INT64_MAX) {
+      mSize = (int64_t)size;
+    }
+  }
   mSizeInitialized = true;
-  // Get the file size and inform the decoder.
-  uint64_t size;
-  nsresult res = mInput->Available(&size);
-  if (NS_SUCCEEDED(res) && size <= INT64_MAX) {
-    mSize = (int64_t)size;
+  if (!mNotifyDataEndedProcessed && mSize >= 0) {
     mCallback->AbstractMainThread()->Dispatch(
       NewRunnableMethod<nsresult>("MediaResourceCallback::NotifyDataEnded",
                                   mCallback.get(),
                                   &MediaResourceCallback::NotifyDataEnded,
                                   NS_OK));
   }
+  mNotifyDataEndedProcessed = true;
 }
 
 nsresult
 FileMediaResource::GetCachedRanges(MediaByteRangeSet& aRanges)
 {
   MutexAutoLock lock(mLock);
 
   EnsureSizeInitialized();
diff --git a/dom/media/FileMediaResource.h b/dom/media/FileMediaResource.h
--- a/dom/media/FileMediaResource.h
+++ b/dom/media/FileMediaResource.h
@@ -128,13 +128,16 @@ private:
   // Input stream for the media data. This can be used from any
   // thread.
   nsCOMPtr<nsIInputStream>  mInput;
 
   // Whether we've attempted to initialize mSize. Note that mSize can be -1
   // when mSizeInitialized is true if we tried and failed to get the size
   // of the file.
   bool mSizeInitialized;
+  // Set to true if NotifyDataEnded callback has been processed (which only
+  // occurs if resource size is known)
+  bool mNotifyDataEndedProcessed = false;
 };
 
 } // namespace mozilla
 
 #endif // mozilla_dom_media_FileMediaResource_h
