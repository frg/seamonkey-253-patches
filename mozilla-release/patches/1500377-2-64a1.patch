# HG changeset patch
# User Alex Chronopoulos <achronop@gmail.com>
# Date 1539986438 0
# Node ID abbf8a8eed9da1931dbbcdf71810d21ccd3ef333
# Parent  37df3d44e6882b69f9b8eda9ca67b5d322272fa7
Bug 1500377 - Update cubeb-pulse-rs to upstream commit 100b858. r=kinetik

Differential Revision: https://phabricator.services.mozilla.com/D9242

diff --git a/media/libcubeb/cubeb-pulse-rs/README_MOZILLA b/media/libcubeb/cubeb-pulse-rs/README_MOZILLA
--- a/media/libcubeb/cubeb-pulse-rs/README_MOZILLA
+++ b/media/libcubeb/cubeb-pulse-rs/README_MOZILLA
@@ -1,8 +1,8 @@
 The source from this directory was copied from the cubeb-pulse-rs
 git repository using the update.sh script.  The only changes
 made were those applied by update.sh and the addition of
 Makefile.in build files for the Mozilla build system.
 
 The cubeb-pulse-rs git repository is: https://github.com/djg/cubeb-pulse-rs.git
 
-The git commit ID used was 798864774d7b9e65e8327ba87a753254afa3da6e (2018-07-24 14:13:11 +1000)
+The git commit ID used was 100b85828388d4bf8b9713fe5c5dd9dc00d8f24f (2018-10-19 10:32:47 +1300)
diff --git a/media/libcubeb/cubeb-pulse-rs/src/backend/context.rs b/media/libcubeb/cubeb-pulse-rs/src/backend/context.rs
--- a/media/libcubeb/cubeb-pulse-rs/src/backend/context.rs
+++ b/media/libcubeb/cubeb-pulse-rs/src/backend/context.rs
@@ -28,18 +28,20 @@ pub const PULSE_OPS: Ops = capi_new!(Pul
 #[repr(C)]
 #[derive(Debug)]
 pub struct PulseContext {
     _ops: *const Ops,
     pub mainloop: pulse::ThreadedMainloop,
     pub context: Option<pulse::Context>,
     pub default_sink_info: Option<DefaultInfo>,
     pub context_name: Option<CString>,
-    pub collection_changed_callback: ffi::cubeb_device_collection_changed_callback,
-    pub collection_changed_user_ptr: *mut c_void,
+    pub input_collection_changed_callback: ffi::cubeb_device_collection_changed_callback,
+    pub input_collection_changed_user_ptr: *mut c_void,
+    pub output_collection_changed_callback: ffi::cubeb_device_collection_changed_callback,
+    pub output_collection_changed_user_ptr: *mut c_void,
     pub error: bool,
     pub version_2_0_0: bool,
     pub version_0_9_8: bool,
     #[cfg(feature = "pulse-dlopen")]
     pub libpulse: LibLoader,
     devids: RefCell<Intern>,
 }
 
@@ -53,18 +55,20 @@ impl PulseContext {
 
         let ctx = Box::new(PulseContext {
             _ops: &PULSE_OPS,
             libpulse: libpulse.unwrap(),
             mainloop: pulse::ThreadedMainloop::new(),
             context: None,
             default_sink_info: None,
             context_name: name,
-            collection_changed_callback: None,
-            collection_changed_user_ptr: ptr::null_mut(),
+            input_collection_changed_callback: None,
+            input_collection_changed_user_ptr: ptr::null_mut(),
+            output_collection_changed_callback: None,
+            output_collection_changed_user_ptr: ptr::null_mut(),
             error: true,
             version_0_9_8: false,
             version_2_0_0: false,
             devids: RefCell::new(Intern::new()),
         });
 
         Ok(ctx)
     }
@@ -72,18 +76,20 @@ impl PulseContext {
     #[cfg(not(feature = "pulse-dlopen"))]
     fn _new(name: Option<CString>) -> Result<Box<Self>> {
         Ok(Box::new(PulseContext {
             _ops: &PULSE_OPS,
             mainloop: pulse::ThreadedMainloop::new(),
             context: None,
             default_sink_info: None,
             context_name: name,
-            collection_changed_callback: None,
-            collection_changed_user_ptr: ptr::null_mut(),
+            input_collection_changed_callback: None,
+            input_collection_changed_user_ptr: ptr::null_mut(),
+            output_collection_changed_callback: None,
+            output_collection_changed_user_ptr: ptr::null_mut(),
             error: true,
             version_0_9_8: false,
             version_2_0_0: false,
             devids: RefCell::new(Intern::new()),
         }))
     }
 
     fn new(name: Option<&CStr>) -> Result<Box<Self>> {
@@ -469,53 +475,73 @@ impl ContextOps for PulseContext {
                         };
                         let dev = if f == pulse::SubscriptionEventFacility::Sink {
                             "sink"
                         } else {
                             "source "
                         };
                         cubeb_log!("{} {} index {}", op, dev, index);
                     }
-                    unsafe {
-                        ctx.collection_changed_callback.unwrap()(
-                            ctx as *mut _ as *mut _,
-                            ctx.collection_changed_user_ptr,
-                        );
+
+                    if f == pulse::SubscriptionEventFacility::Source {
+                        unsafe {
+                            ctx.input_collection_changed_callback.unwrap()(
+                                ctx as *mut _ as *mut _,
+                                ctx.input_collection_changed_user_ptr,
+                            );
+                        }
+                    }
+                    if f == pulse::SubscriptionEventFacility::Sink {
+                        unsafe {
+                            ctx.output_collection_changed_callback.unwrap()(
+                                ctx as *mut _ as *mut _,
+                                ctx.output_collection_changed_user_ptr,
+                            );
+                        }
                     }
                 }
             }
         }
 
         fn success(_: &pulse::Context, success: i32, user_data: *mut c_void) {
             let ctx = unsafe { &*(user_data as *mut PulseContext) };
             if success != 1 {
                 cubeb_log!("subscribe_success ignored failure: {}", success);
             }
             ctx.mainloop.signal();
         }
 
-        self.collection_changed_callback = cb;
-        self.collection_changed_user_ptr = user_ptr;
+        if devtype.contains(DeviceType::INPUT) {
+            self.input_collection_changed_callback = cb;
+            self.input_collection_changed_user_ptr = user_ptr;
+        }
+        if devtype.contains(DeviceType::OUTPUT) {
+            self.output_collection_changed_callback = cb;
+            self.output_collection_changed_user_ptr = user_ptr;
+        }
+
+        let mut mask = pulse::SubscriptionMask::empty();
+        if self.input_collection_changed_callback.is_some() {
+            mask |= pulse::SubscriptionMask::SOURCE;
+        }
+        if self.output_collection_changed_callback.is_some() {
+            mask |= pulse::SubscriptionMask::SINK;
+        }
 
         let user_data: *mut c_void = self as *const _ as *mut _;
         if let Some(ref context) = self.context {
             self.mainloop.lock();
 
-            let mut mask = pulse::SubscriptionMask::empty();
-            if self.collection_changed_callback.is_none() {
-                // Unregister subscription
-                context.clear_subscribe_callback();
+            if cb.is_none() {
+                if mask.is_empty() {
+                    // Unregister subscription
+                    context.clear_subscribe_callback();
+                }
             } else {
                 context.set_subscribe_callback(update_collection, user_data);
-                if devtype.contains(DeviceType::INPUT) {
-                    mask |= pulse::SubscriptionMask::SOURCE
-                };
-                if devtype.contains(DeviceType::OUTPUT) {
-                    mask = pulse::SubscriptionMask::SINK
-                };
             }
 
             if let Ok(o) = context.subscribe(mask, success, self as *const _ as *mut _) {
                 self.operation_wait(None, &o);
             } else {
                 self.mainloop.unlock();
                 cubeb_log!("Context subscribe failed");
                 return Err(Error::error());

