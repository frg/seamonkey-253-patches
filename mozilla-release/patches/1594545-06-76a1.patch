# HG changeset patch
# User Iain Ireland <iireland@mozilla.com>
# Date 1585577748 0
#      Mon Mar 30 14:15:48 2020 +0000
# Node ID ff4dd0cd887720cd5745ad584ba050b89f635ed0
# Parent  049b6c8d127673cfd4d4317145bf2ef3d07596d4
Bug 1594545: Part 6: Registers r=jandem

The irregexp API has a bunch of methods that work with "registers". They are stored on the stack, and in practice they work like local variables.

Depends on D68627

Differential Revision: https://phabricator.services.mozilla.com/D68628

diff --git a/js/src/new-regexp/regexp-macro-assembler-arch.h b/js/src/new-regexp/regexp-macro-assembler-arch.h
--- a/js/src/new-regexp/regexp-macro-assembler-arch.h
+++ b/js/src/new-regexp/regexp-macro-assembler-arch.h
@@ -75,16 +75,29 @@ class SMRegExpMacroAssembler final : pub
   virtual void CheckAtStart(int cp_offset, Label* on_at_start);
   virtual void CheckNotAtStart(int cp_offset, Label* on_not_at_start);
   virtual void CheckPosition(int cp_offset, Label* on_outside_input);
 
   virtual void LoadCurrentCharacterImpl(int cp_offset, Label* on_end_of_input,
                                         bool check_bounds, int characters,
                                         int eats_at_least);
 
+  virtual void AdvanceRegister(int reg, int by);
+  virtual void IfRegisterGE(int reg, int comparand, Label* if_ge);
+  virtual void IfRegisterLT(int reg, int comparand, Label* if_lt);
+  virtual void IfRegisterEqPos(int reg, Label* if_eq);
+  virtual void PopRegister(int register_index);
+  virtual void PushRegister(int register_index,
+                            StackCheckFlag check_stack_limit);
+  virtual void ReadCurrentPositionFromRegister(int reg);
+  virtual void WriteCurrentPositionToRegister(int reg, int cp_offset);
+  virtual void ReadStackPointerFromRegister(int reg);
+  virtual void WriteStackPointerToRegister(int reg);
+  virtual void SetRegister(int register_index, int to);
+  virtual void ClearRegisters(int reg_from, int reg_to);
 
  private:
   // Push a register on the backtrack stack.
   void Push(js::jit::Register value);
 
   // Pop a value from the backtrack stack.
   void Pop(js::jit::Register target);
 
@@ -128,16 +141,31 @@ class SMRegExpMacroAssembler final : pub
     return js::jit::Address(masm_.getStackPointer(),
                             offsetof(FrameData, matches));
   }
   js::jit::Address numMatches() {
     return js::jit::Address(masm_.getStackPointer(),
                             offsetof(FrameData, numMatches));
   }
 
+  // The stack-pointer-relative location of a regexp register.
+  js::jit::Address register_location(int register_index) {
+    return js::jit::Address(masm_.getStackPointer(),
+                            register_offset(register_index));
+  }
+
+  int32_t register_offset(int register_index) {
+    MOZ_ASSERT(register_index >= 0 && register_index <= kMaxRegister);
+    if (num_registers_ <= register_index) {
+      num_registers_ = register_index + 1;
+    }
+    static_assert(alignof(uintptr_t) <= alignof(FrameData));
+    return sizeof(FrameData) + register_index * sizeof(uintptr_t*);
+  }
+
   JSContext* cx_;
   js::jit::StackMacroAssembler& masm_;
 
   /*
    * This assembler uses the following registers:
    *
    * - current_character_:
    *     Contains the character (or characters) currently being examined.
diff --git a/js/src/new-regexp/regexp-native-macro-assembler.cc b/js/src/new-regexp/regexp-native-macro-assembler.cc
--- a/js/src/new-regexp/regexp-native-macro-assembler.cc
+++ b/js/src/new-regexp/regexp-native-macro-assembler.cc
@@ -61,16 +61,23 @@ int SMRegExpMacroAssembler::stack_limit_
 }
 
 void SMRegExpMacroAssembler::AdvanceCurrentPosition(int by) {
   if (by != 0) {
     masm_.addPtr(Imm32(by * char_size()), current_position_);
   }
 }
 
+void SMRegExpMacroAssembler::AdvanceRegister(int reg, int by) {
+  MOZ_ASSERT(reg >= 0 && reg < num_registers_);
+  if (by != 0) {
+    masm_.addPtr(Imm32(by), register_location(reg));
+  }
+}
+
 void SMRegExpMacroAssembler::Backtrack() {
   // Check for an interrupt. We have to restart from the beginning if we
   // are interrupted, so we only check for urgent interrupts.
   js::jit::Label noInterrupt;
   masm_.branchTest32(
       Assembler::Zero, AbsoluteAddress(cx_->addressOfInterruptBits()),
       Imm32(uint32_t(js::InterruptReason::CallbackUrgent)), &noInterrupt);
   masm_.movePtr(ImmWord(js::RegExpRunStatus_Error), temp0_);
@@ -229,16 +236,33 @@ void SMRegExpMacroAssembler::Fail() {
   masm_.movePtr(ImmWord(js::RegExpRunStatus_Success_NotFound), temp0_);
   masm_.jump(&exit_label_);
 }
 
 void SMRegExpMacroAssembler::GoTo(Label* to) {
   masm_.jump(LabelOrBacktrack(to));
 }
 
+void SMRegExpMacroAssembler::IfRegisterGE(int reg, int comparand,
+                                          Label* if_ge) {
+  masm_.branchPtr(Assembler::GreaterThanOrEqual, register_location(reg),
+                  ImmWord(comparand), LabelOrBacktrack(if_ge));
+}
+
+void SMRegExpMacroAssembler::IfRegisterLT(int reg, int comparand,
+                                          Label* if_lt) {
+  masm_.branchPtr(Assembler::LessThan, register_location(reg),
+                  ImmWord(comparand), LabelOrBacktrack(if_lt));
+}
+
+void SMRegExpMacroAssembler::IfRegisterEqPos(int reg, Label* if_eq) {
+  masm_.branchPtr(Assembler::Equal, register_location(reg), current_position_,
+                  LabelOrBacktrack(if_eq));
+}
+
 // This is a word-for-word identical copy of the V8 code, which is
 // duplicated in at least nine different places in V8 (one per
 // supported architecture) with no differences outside of comments and
 // formatting. It should be hoisted into the superclass. Once that is
 // done upstream, this version can be deleted.
 void SMRegExpMacroAssembler::LoadCurrentCharacterImpl(int cp_offset,
                                                       Label* on_end_of_input,
                                                       bool check_bounds,
@@ -282,29 +306,71 @@ void SMRegExpMacroAssembler::LoadCurrent
       MOZ_ASSERT(characters == 1);
       masm_.load16ZeroExtend(address, current_character_);
     }
   }
 }
 
 void SMRegExpMacroAssembler::PopCurrentPosition() { Pop(current_position_); }
 
+void SMRegExpMacroAssembler::PopRegister(int register_index) {
+  Pop(temp0_);
+  masm_.storePtr(temp0_, register_location(register_index));
+}
+
 void SMRegExpMacroAssembler::PushBacktrack(Label* label) {
   MOZ_ASSERT(!label->is_bound());
   MOZ_ASSERT(!label->patchOffset_.bound());
   label->patchOffset_ = masm_.movWithPatch(ImmPtr(nullptr), temp0_);
   MOZ_ASSERT(label->patchOffset_.bound());
 
   Push(temp0_);
 
   CheckBacktrackStackLimit();
 }
 
 void SMRegExpMacroAssembler::PushCurrentPosition() { Push(current_position_); }
 
+void SMRegExpMacroAssembler::PushRegister(int register_index,
+                                          StackCheckFlag check_stack_limit) {
+  masm_.loadPtr(register_location(register_index), temp0_);
+  Push(temp0_);
+  if (check_stack_limit) {
+    CheckBacktrackStackLimit();
+  }
+}
+
+void SMRegExpMacroAssembler::ReadCurrentPositionFromRegister(int reg) {
+  masm_.loadPtr(register_location(reg), current_position_);
+}
+
+void SMRegExpMacroAssembler::WriteCurrentPositionToRegister(int reg,
+                                                            int cp_offset) {
+  if (cp_offset == 0) {
+    masm_.storePtr(current_position_, register_location(reg));
+  } else {
+    Address addr(current_position_, cp_offset * char_size());
+    masm_.computeEffectiveAddress(addr, temp0_);
+    masm_.storePtr(temp0_, register_location(reg));
+  }
+}
+
+// Note: The backtrack stack pointer is stored in a register as an
+// offset from the stack top, not as a bare pointer, so that it is not
+// corrupted if the backtrack stack grows (and therefore moves).
+void SMRegExpMacroAssembler::ReadStackPointerFromRegister(int reg) {
+  masm_.loadPtr(register_location(reg), backtrack_stack_pointer_);
+  masm_.addPtr(backtrackStackBase(), backtrack_stack_pointer_);
+}
+void SMRegExpMacroAssembler::WriteStackPointerToRegister(int reg) {
+  masm_.movePtr(backtrack_stack_pointer_, temp0_);
+  masm_.subPtr(backtrackStackBase(), temp0_);
+  masm_.storePtr(temp0_, register_location(reg));
+}
+
 // When matching a regexp that is anchored at the end, this operation
 // is used to try skipping the beginning of long strings. If the
 // maximum length of a match is less than the length of the string, we
 // can skip the initial len - max_len bytes.
 void SMRegExpMacroAssembler::SetCurrentPositionFromEnd(int by) {
   js::jit::Label after_position;
   masm_.branchPtr(Assembler::GreaterThanOrEqual, current_position_,
                   ImmWord(-by * char_size()), &after_position);
@@ -312,23 +378,38 @@ void SMRegExpMacroAssembler::SetCurrentP
 
   // On RegExp code entry (where this operation is used), the character before
   // the current position is expected to be already loaded.
   // We have advanced the position, so it's safe to read backwards.
   LoadCurrentCharacterUnchecked(-1, 1);
   masm_.bind(&after_position);
 }
 
+void SMRegExpMacroAssembler::SetRegister(int register_index, int to) {
+  MOZ_ASSERT(register_index >= num_capture_registers_);
+  masm_.storePtr(ImmWord(to), register_location(register_index));
+}
+
 // Returns true if a regexp match can be restarted (aka the regexp is global).
 // The return value is not used anywhere, but we implement it to be safe.
 bool SMRegExpMacroAssembler::Succeed() {
   masm_.jump(&success_label_);
   return global();
 }
 
+// Capture registers are initialized to input[-1]
+void SMRegExpMacroAssembler::ClearRegisters(int reg_from, int reg_to) {
+  MOZ_ASSERT(reg_from <= reg_to);
+  masm_.loadPtr(inputStart(), temp0_);
+  masm_.subPtr(Imm32(char_size()), temp0_);
+  for (int reg = reg_from; reg <= reg_to; reg++) {
+    masm_.storePtr(temp0_, register_location(reg));
+  }
+}
+
 void SMRegExpMacroAssembler::Push(Register source) {
   MOZ_ASSERT(source != backtrack_stack_pointer_);
 
   masm_.subPtr(Imm32(sizeof(void*)), backtrack_stack_pointer_);
   masm_.storePtr(source, Address(backtrack_stack_pointer_, 0));
 }
 
 void SMRegExpMacroAssembler::Pop(Register target) {
