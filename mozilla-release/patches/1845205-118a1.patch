# HG changeset patch
# User Randell Jesup <rjesup@mozilla.com>
# Date 1690829985 0
# Node ID 2d698aca1dece26df297dd085dbde9216e3b2e66
# Parent  356da7283e5b0da457b6446f934e9242a630ce1d
Bug 1845205: Add locking around access to WebSocketChannel::mPMCECompressor r=kershaw,necko-reviewers

Differential Revision: https://phabricator.services.mozilla.com/D184868

diff --git a/netwerk/protocol/websocket/WebSocketChannel.cpp b/netwerk/protocol/websocket/WebSocketChannel.cpp
--- a/netwerk/protocol/websocket/WebSocketChannel.cpp
+++ b/netwerk/protocol/websocket/WebSocketChannel.cpp
@@ -1177,16 +1177,17 @@ WebSocketChannel::WebSocketChannel() :
   mServerCloseCode(CLOSE_ABNORMAL),
   mScriptCloseCode(0),
   mFragmentOpcode(nsIWebSocketFrame::OPCODE_CONTINUATION),
   mFragmentAccumulator(0),
   mBuffered(0),
   mBufferSize(kIncomingBufferInitialSize),
   mCurrentOut(nullptr),
   mCurrentOutSent(0),
+  mCompressorMutex("WebSocketChannel::mCompressorMutex"),
   mDynamicOutputSize(0),
   mDynamicOutput(nullptr),
   mPrivateBrowsing(false),
   mConnectionLogService(nullptr),
   mMutex("WebSocketChannel::mMutex")
 {
   MOZ_ASSERT(NS_IsMainThread(), "not main thread");
 
@@ -1627,16 +1628,17 @@ WebSocketChannel::ProcessInput(uint8_t *
     if (!finBit && (opcode & kControlFrameMask)) {
       LOG(("WebSocketChannel:: fragmented control frame code %d\n", opcode));
       return NS_ERROR_ILLEGAL_VALUE;
     }
 
     if (rsvBits) {
       // PMCE sets RSV1 bit in the first fragment when the non-control frame
       // is deflated
+      MutexAutoLock lock(mCompressorMutex);
       if (mPMCECompressor && rsvBits == kRsv1Bit && mFragmentAccumulator == 0 &&
           !(opcode & kControlFrameMask)) {
         mPMCECompressor->SetMessageDeflated();
         LOG(("WebSocketChannel::ProcessInput: received deflated frame\n"));
       } else {
         LOG(("WebSocketChannel::ProcessInput: unexpected reserved bits %x\n",
              rsvBits));
         return NS_ERROR_ILLEGAL_VALUE;
@@ -1700,35 +1702,39 @@ WebSocketChannel::ProcessInput(uint8_t *
     if (mServerClosed) {
       LOG(("WebSocketChannel:: ignoring read frame code %d after close\n",
                  opcode));
       // nop
     } else if (mStopped) {
       LOG(("WebSocketChannel:: ignoring read frame code %d after completion\n",
            opcode));
     } else if (opcode == nsIWebSocketFrame::OPCODE_TEXT) {
-      bool isDeflated = mPMCECompressor && mPMCECompressor->IsMessageDeflated();
-      LOG(("WebSocketChannel:: %stext frame received\n",
-           isDeflated ? "deflated " : ""));
-
       if (mListenerMT) {
         nsCString utf8Data;
-
-        if (isDeflated) {
-          rv = mPMCECompressor->Inflate(payload, payloadLength, utf8Data);
-          if (NS_FAILED(rv)) {
-            return rv;
-          }
-          LOG(("WebSocketChannel:: message successfully inflated "
-               "[origLength=%d, newLength=%d]\n", payloadLength,
-               utf8Data.Length()));
-        } else {
-          if (!utf8Data.Assign((const char *)payload, payloadLength,
-                               mozilla::fallible)) {
-            return NS_ERROR_OUT_OF_MEMORY;
+        {
+          MutexAutoLock lock(mCompressorMutex);
+          bool isDeflated =
+              mPMCECompressor && mPMCECompressor->IsMessageDeflated();
+          LOG(("WebSocketChannel:: %stext frame received\n",
+               isDeflated ? "deflated " : ""));
+
+          if (isDeflated) {
+            rv = mPMCECompressor->Inflate(payload, payloadLength, utf8Data);
+            if (NS_FAILED(rv)) {
+              return rv;
+            }
+            LOG(
+                ("WebSocketChannel:: message successfully inflated "
+                 "[origLength=%d, newLength=%zd]\n",
+                 payloadLength, utf8Data.Length()));
+          } else {
+            if (!utf8Data.Assign((const char*)payload, payloadLength,
+                                 mozilla::fallible)) {
+              return NS_ERROR_OUT_OF_MEMORY;
+            }
           }
         }
 
         // Section 8.1 says to fail connection if invalid utf-8 in text message
         if (!IsUTF8(utf8Data)) {
           LOG(("WebSocketChannel:: text frame invalid utf-8\n"));
           return NS_ERROR_CANNOT_CONVERT_DATA;
         }
@@ -1835,35 +1841,39 @@ WebSocketChannel::ProcessInput(uint8_t *
           mBuffered -= framingLength + payloadLength;
         payloadLength = 0;
       }
 
       if (frame) {
         mService->FrameReceived(mSerial, mInnerWindowID, frame.forget());
       }
     } else if (opcode == nsIWebSocketFrame::OPCODE_BINARY) {
-      bool isDeflated = mPMCECompressor && mPMCECompressor->IsMessageDeflated();
-      LOG(("WebSocketChannel:: %sbinary frame received\n",
-           isDeflated ? "deflated " : ""));
-
       if (mListenerMT) {
         nsCString binaryData;
-
-        if (isDeflated) {
-          rv = mPMCECompressor->Inflate(payload, payloadLength, binaryData);
-          if (NS_FAILED(rv)) {
-            return rv;
-          }
-          LOG(("WebSocketChannel:: message successfully inflated "
-               "[origLength=%d, newLength=%d]\n", payloadLength,
-               binaryData.Length()));
-        } else {
-          if (!binaryData.Assign((const char *)payload, payloadLength,
-                                 mozilla::fallible)) {
-            return NS_ERROR_OUT_OF_MEMORY;
+        {
+          MutexAutoLock lock(mCompressorMutex);
+          bool isDeflated =
+              mPMCECompressor && mPMCECompressor->IsMessageDeflated();
+          LOG(("WebSocketChannel:: %sbinary frame received\n",
+               isDeflated ? "deflated " : ""));
+
+          if (isDeflated) {
+            rv = mPMCECompressor->Inflate(payload, payloadLength, binaryData);
+            if (NS_FAILED(rv)) {
+              return rv;
+            }
+            LOG(
+                ("WebSocketChannel:: message successfully inflated "
+                 "[origLength=%d, newLength=%zd]\n",
+                 payloadLength, binaryData.Length()));
+          } else {
+            if (!binaryData.Assign((const char*)payload, payloadLength,
+                                   mozilla::fallible)) {
+              return NS_ERROR_OUT_OF_MEMORY;
+            }
           }
         }
 
         RefPtr<WebSocketFrame> frame =
           mService->CreateFrameIfNeeded(finBit, rsvBit1, rsvBit2, rsvBit3,
                                         opcode, maskBit, mask, binaryData);
         if (frame) {
           mService->FrameReceived(mSerial, mInnerWindowID, frame.forget());
@@ -2160,16 +2170,17 @@ WebSocketChannel::PrimeNewOutgoingMessag
       mOutHeader[0] = kFinalFragBit | nsIWebSocketFrame::OPCODE_BINARY;
       break;
     case kMsgTypeFin:
       MOZ_ASSERT(false, "unreachable");  // avoid compiler warning
       break;
     }
 
     // deflate the payload if PMCE is negotiated
+    MutexAutoLock lock(mCompressorMutex);
     if (mPMCECompressor &&
         (msgType == kMsgTypeString || msgType == kMsgTypeBinaryString)) {
       if (mCurrentOut->DeflatePayload(mPMCECompressor)) {
         // The payload was deflated successfully, set RSV1 bit
         mOutHeader[0] |= kRsv1Bit;
 
         LOG(("WebSocketChannel::PrimeNewOutgoingMessage %p current msg %p was "
              "deflated [origLength=%d, newLength=%d].\n", this, mCurrentOut,
@@ -2465,18 +2476,20 @@ WebSocketChannel::DoStopSession(nsresult
     CleanupConnection();
   }
 
   if (mCancelable) {
     mCancelable->Cancel(NS_ERROR_UNEXPECTED);
     mCancelable = nullptr;
   }
 
-  mPMCECompressor = nullptr;
-
+  {
+    MutexAutoLock lock(mCompressorMutex);
+    mPMCECompressor = nullptr;
+  }
   if (!mCalledOnStop) {
     mCalledOnStop = 1;
 
     nsWSAdmissionManager::OnStopSession(this, reason);
 
     RefPtr<CallOnStop> runnable = new CallOnStop(this, reason);
     mTargetThread->Dispatch(runnable, NS_DISPATCH_NORMAL);
   }
@@ -2726,16 +2739,17 @@ WebSocketChannel::HandleExtensions()
 
   if (clientMaxWindowBits == -1) {
     clientMaxWindowBits = 15;
   }
   if (serverMaxWindowBits == -1) {
     serverMaxWindowBits = 15;
   }
 
+  MutexAutoLock lock(mCompressorMutex);
   mPMCECompressor = new PMCECompression(clientNoContextTakeover,
                                         clientMaxWindowBits,
                                         serverMaxWindowBits);
   if (mPMCECompressor->Active()) {
     LOG(("WebSocketChannel::HandleExtensions: PMCE negotiated, %susing "
          "context takeover, clientMaxWindowBits=%d, "
          "serverMaxWindowBits=%d\n",
          clientNoContextTakeover ? "NOT " : "", clientMaxWindowBits,
@@ -3748,16 +3762,17 @@ WebSocketChannel::OnTransportAvailable(n
 
       if (clientMaxWindowBits == -1) {
         clientMaxWindowBits = 15;
       }
       if (serverMaxWindowBits == -1) {
         serverMaxWindowBits = 15;
       }
 
+      MutexAutoLock lock(mCompressorMutex);
       mPMCECompressor = new PMCECompression(serverNoContextTakeover,
                                             serverMaxWindowBits,
                                             clientMaxWindowBits);
       if (mPMCECompressor->Active()) {
         LOG(("WebSocketChannel::OnTransportAvailable: PMCE negotiated, %susing "
              "context takeover, serverMaxWindowBits=%d, "
              "clientMaxWindowBits=%d\n",
              serverNoContextTakeover ? "NOT " : "", serverMaxWindowBits,
diff --git a/netwerk/protocol/websocket/WebSocketChannel.h b/netwerk/protocol/websocket/WebSocketChannel.h
--- a/netwerk/protocol/websocket/WebSocketChannel.h
+++ b/netwerk/protocol/websocket/WebSocketChannel.h
@@ -15,16 +15,17 @@
 #include "nsITimer.h"
 #include "nsIDNSListener.h"
 #include "nsINamed.h"
 #include "nsIObserver.h"
 #include "nsIProtocolProxyCallback.h"
 #include "nsIChannelEventSink.h"
 #include "nsIHttpChannelInternal.h"
 #include "nsIStringStream.h"
+#include "mozilla/Mutex.h"
 #include "BaseWebSocketChannel.h"
 
 #include "nsCOMPtr.h"
 #include "nsString.h"
 #include "nsDeque.h"
 #include "mozilla/Atomics.h"
 
 class nsIAsyncVerifyRedirectCallback;
@@ -289,17 +290,19 @@ private:
   OutboundMessage                *mCurrentOut;
   uint32_t                        mCurrentOutSent;
   nsDeque                         mOutgoingMessages;
   nsDeque                         mOutgoingPingMessages;
   nsDeque                         mOutgoingPongMessages;
   uint32_t                        mHdrOutToSend;
   uint8_t                        *mHdrOut;
   uint8_t                         mOutHeader[kCopyBreak + 16];
+  Mutex                           mCompressorMutex;
   nsAutoPtr<PMCECompression>      mPMCECompressor;
+
   uint32_t                        mDynamicOutputSize;
   uint8_t                        *mDynamicOutput;
   bool                            mPrivateBrowsing;
 
   nsCOMPtr<nsIDashboardEventNotifier> mConnectionLogService;
 
   mozilla::Mutex mMutex;
 };
diff --git a/netwerk/protocol/websocket/WebSocketChannel.h.1845205.later b/netwerk/protocol/websocket/WebSocketChannel.h.1845205.later
new file mode 100644
--- /dev/null
+++ b/netwerk/protocol/websocket/WebSocketChannel.h.1845205.later
@@ -0,0 +1,29 @@
+--- WebSocketChannel.h
++++ WebSocketChannel.h
+@@ -338,20 +339,22 @@ class WebSocketChannel : public BaseWebS
+   uint32_t mCurrentOutSent;
+   nsDeque<OutboundMessage> mOutgoingMessages;
+   nsDeque<OutboundMessage> mOutgoingPingMessages;
+   nsDeque<OutboundMessage> mOutgoingPongMessages;
+   uint32_t mHdrOutToSend;
+   uint8_t* mHdrOut;
+   uint8_t mOutHeader[kCopyBreak + 16]{0};
+ 
+-  // Set on MainThread in OnStartRequest (before mDataStarted), used on IO
+-  // Thread (after mDataStarted), cleared in DoStopSession on IOThread or
+-  // on MainThread (if mDataStarted == false)
+-  UniquePtr<PMCECompression> mPMCECompressor;
++  // Set on MainThread in OnStartRequest (before mDataStarted), or in
++  // HandleExtensions() or OnTransportAvailableInternal(),used on IO Thread
++  // (after mDataStarted), cleared in DoStopSession on IOThread or on
++  // MainThread (if mDataStarted == false).
++  Mutex mCompressorMutex;
++  UniquePtr<PMCECompression> mPMCECompressor MOZ_GUARDED_BY(mCompressorMutex);
+ 
+   // Used by EnsureHdrOut, which isn't called anywhere
+   uint32_t mDynamicOutputSize;
+   uint8_t* mDynamicOutput;
+   // Set on creation and AsyncOpen, read on both threads
+   Atomic<bool> mPrivateBrowsing;
+ 
+   nsCOMPtr<nsIDashboardEventNotifier>
