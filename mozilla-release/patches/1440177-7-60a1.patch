# HG changeset patch
# User Matt Woodrow <mwoodrow@mozilla.com>
# Date 1519942757 -46800
# Node ID 114d99e1c41c8bf49633d331cf3edbe236c2b4a5
# Parent  fa03c3d7c00c776267f1233ee7e3884dd126d88e
Bug 1440177 - Part 7: Don't allocate new clips when flattening nsDisplayOpacity. r=mstange

Combing the two clips as-is should always be correct, and since they're frequently identical, we can usually make IntersectClip a no-op.

MozReview-Commit-ID: 3xxMyZjwPvJ

diff --git a/layout/painting/nsDisplayList.cpp b/layout/painting/nsDisplayList.cpp
--- a/layout/painting/nsDisplayList.cpp
+++ b/layout/painting/nsDisplayList.cpp
@@ -3212,17 +3212,17 @@ FindCommonAncestorClipForIntersection(co
   return nullptr;
 }
 
 void
 nsDisplayItem::IntersectClip(nsDisplayListBuilder* aBuilder,
                              const DisplayItemClipChain* aOther,
                              bool aStore)
 {
-  if (!aOther) {
+  if (!aOther || mClipChain == aOther) {
     return;
   }
 
   // aOther might be a reference to a clip on the stack. We need to make sure
   // that CreateClipChainIntersection will allocate the actual intersected
   // clip in the builder's arena, so for the mClipChain == nullptr case,
   // we supply nullptr as the common ancestor so that CreateClipChainIntersection
   // clones the whole chain.
@@ -6630,31 +6630,18 @@ nsDisplayOpacity::ShouldFlattenAway(nsDi
   for (size_t i = 0; i < childCount; i++) {
     for (size_t j = i+1; j < childCount; j++) {
       if (children[i].bounds.Intersects(children[j].bounds)) {
         return false;
       }
     }
   }
 
-  // When intersecting the children's clip, only intersect with the clip for
-  // our ASR and not with the whole clip chain, because the rest of the clip
-  // chain is usually already set on the children. In fact, opacity items
-  // usually never have their own clip because during display item creation
-  // time we propagated the clip to our contents, so maybe we should just
-  // remove the clip parameter from ApplyOpacity completely.
-  const DisplayItemClipChain* clip = nullptr;
-
-  if (mClip) {
-    clip = aBuilder->AllocateDisplayItemClipChain(*mClip, mActiveScrolledRoot,
-                                                  nullptr);
-  }
-
   for (uint32_t i = 0; i < childCount; i++) {
-    children[i].item->ApplyOpacity(aBuilder, mOpacity, clip);
+    children[i].item->ApplyOpacity(aBuilder, mOpacity, mClipChain);
   }
 
   return true;
 }
 
 nsDisplayItem::LayerState
 nsDisplayOpacity::GetLayerState(nsDisplayListBuilder* aBuilder,
                                 LayerManager* aManager,
