# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1517570159 -3600
# Node ID 4fef754d27619d28340ddcf86dcbcda7694af4fb
# Parent  6172d07fc6b001b5421c29745133c5274c8a9edf
Bug 1435214: Optimize @keyframes rule insertions. r=xidorn,hiro

Also add some missing test for the tag name invalidations (bug 1407522) and an
empty stylesheet just for sanity.

MozReview-Commit-ID: AHwhZynLBv

diff --git a/layout/base/nsPresContext.h b/layout/base/nsPresContext.h
--- a/layout/base/nsPresContext.h
+++ b/layout/base/nsPresContext.h
@@ -239,16 +239,17 @@ public:
   mozilla::AnimationEventDispatcher* AnimationEventDispatcher()
   {
     return mAnimationEventDispatcher;
   }
 
   mozilla::EffectCompositor* EffectCompositor() { return mEffectCompositor; }
   nsTransitionManager* TransitionManager() { return mTransitionManager; }
   nsAnimationManager* AnimationManager() { return mAnimationManager; }
+  const nsAnimationManager* AnimationManager() const { return mAnimationManager; }
 
   nsRefreshDriver* RefreshDriver() { return mRefreshDriver; }
 
   mozilla::RestyleManager* RestyleManager() {
     MOZ_ASSERT(mRestyleManager);
     return mRestyleManager;
   }
 
diff --git a/layout/style/ServoBindings.cpp b/layout/style/ServoBindings.cpp
--- a/layout/style/ServoBindings.cpp
+++ b/layout/style/ServoBindings.cpp
@@ -350,16 +350,24 @@ Gecko_NoteDirtySubtreeForInvalidation(Ra
 
 void
 Gecko_NoteAnimationOnlyDirtyElement(RawGeckoElementBorrowed aElement)
 {
   MOZ_ASSERT(NS_IsMainThread());
   const_cast<Element*>(aElement)->NoteAnimationOnlyDirtyForServo();
 }
 
+bool Gecko_AnimationNameMayBeReferencedFromStyle(
+  RawGeckoPresContextBorrowed aPresContext,
+  nsIAtom* aName)
+{
+  MOZ_ASSERT(aPresContext);
+  return aPresContext->AnimationManager()->AnimationMayBeReferenced(aName);
+}
+
 CSSPseudoElementType
 Gecko_GetImplementedPseudo(RawGeckoElementBorrowed aElement)
 {
   return aElement->GetPseudoElementType();
 }
 
 uint32_t
 Gecko_CalcStyleDifference(ServoStyleContextBorrowed aOldStyle,
diff --git a/layout/style/ServoBindings.h b/layout/style/ServoBindings.h
--- a/layout/style/ServoBindings.h
+++ b/layout/style/ServoBindings.h
@@ -388,16 +388,20 @@ nsStyleContentData::CounterFunction* Gec
 
 // Dirtiness tracking.
 void Gecko_SetNodeFlags(RawGeckoNodeBorrowed node, uint32_t flags);
 void Gecko_UnsetNodeFlags(RawGeckoNodeBorrowed node, uint32_t flags);
 void Gecko_NoteDirtyElement(RawGeckoElementBorrowed element);
 void Gecko_NoteDirtySubtreeForInvalidation(RawGeckoElementBorrowed element);
 void Gecko_NoteAnimationOnlyDirtyElement(RawGeckoElementBorrowed element);
 
+bool Gecko_AnimationNameMayBeReferencedFromStyle(
+  RawGeckoPresContextBorrowed pres_context,
+  nsIAtom* name);
+
 // Incremental restyle.
 mozilla::CSSPseudoElementType Gecko_GetImplementedPseudo(RawGeckoElementBorrowed element);
 // We'd like to return `nsChangeHint` here, but bindgen bitfield enums don't
 // work as return values with the Linux 32-bit ABI at the moment because
 // they wrap the value in a struct.
 uint32_t Gecko_CalcStyleDifference(ServoStyleContextBorrowed old_style,
                                    ServoStyleContextBorrowed new_style,
                                    bool* any_style_changed,
diff --git a/layout/style/nsAnimationManager.cpp b/layout/style/nsAnimationManager.cpp
--- a/layout/style/nsAnimationManager.cpp
+++ b/layout/style/nsAnimationManager.cpp
@@ -1003,30 +1003,33 @@ GeckoCSSAnimationBuilder::FillInMissingK
 #endif
 
 template<class BuilderType>
 static nsAnimationManager::OwningCSSAnimationPtrArray
 BuildAnimations(nsPresContext* aPresContext,
                 const NonOwningAnimationTarget& aTarget,
                 const nsStyleDisplay& aStyleDisplay,
                 BuilderType& aBuilder,
-                nsAnimationManager::CSSAnimationCollection* aCollection)
+                nsAnimationManager::CSSAnimationCollection* aCollection,
+                nsTHashtable<nsRefPtrHashKey<nsIAtom>>& aReferencedAnimations)
 {
   nsAnimationManager::OwningCSSAnimationPtrArray result;
 
   for (size_t animIdx = aStyleDisplay.mAnimationNameCount; animIdx-- != 0;) {
+    nsIAtom* name = aStyleDisplay.GetAnimationName(animIdx);
     // CSS Animations whose animation-name does not match a @keyframes rule do
     // not generate animation events. This includes when the animation-name is
     // "none" which is represented by an empty name in the StyleAnimation.
     // Since such animations neither affect style nor dispatch events, we do
     // not generate a corresponding CSSAnimation for them.
-    if (aStyleDisplay.GetAnimationName(animIdx) == nsGkAtoms::_empty) {
+    if (name == nsGkAtoms::_empty) {
       continue;
     }
 
+    aReferencedAnimations.PutEntry(name);
     RefPtr<CSSAnimation> dest = BuildAnimation(aPresContext,
                                                aTarget,
                                                aStyleDisplay,
                                                animIdx,
                                                aBuilder,
                                                aCollection);
     if (!dest) {
       continue;
@@ -1120,17 +1123,18 @@ nsAnimationManager::DoUpdateAnimations(
 
   // Build the updated animations list, extracting matching animations from
   // the existing collection as we go.
   OwningCSSAnimationPtrArray newAnimations;
   newAnimations = BuildAnimations(mPresContext,
                                   aTarget,
                                   aStyleDisplay,
                                   aBuilder,
-                                  collection);
+                                  collection,
+                                  mMaybeReferencedAnimations);
 
   if (newAnimations.IsEmpty()) {
     if (collection) {
       collection->Destroy();
     }
     return;
   }
 
diff --git a/layout/style/nsAnimationManager.h b/layout/style/nsAnimationManager.h
--- a/layout/style/nsAnimationManager.h
+++ b/layout/style/nsAnimationManager.h
@@ -332,20 +332,33 @@ public:
       if (keyframe.mTimingFunction == aTimingFunctionToMatch) {
         return true;
       }
       ++aIndex;
     }
     return false;
   }
 
+  bool AnimationMayBeReferenced(nsIAtom* aName) const
+  {
+    return mMaybeReferencedAnimations.Contains(aName);
+  }
+
 protected:
   ~nsAnimationManager() override = default;
 
 private:
+  // This includes all animation names referenced regardless of whether a
+  // corresponding `@keyframes` rule is available.
+  //
+  // It may contain names which are no longer referenced, but it should always
+  // contain names which are currently referenced, so that it is usable for
+  // style invalidation.
+  nsTHashtable<nsRefPtrHashKey<nsIAtom>> mMaybeReferencedAnimations;
+
   template<class BuilderType>
   void DoUpdateAnimations(
     const mozilla::NonOwningAnimationTarget& aTarget,
     const nsStyleDisplay& aStyleDisplay,
     BuilderType& aBuilder);
 };
 
 #endif /* !defined(nsAnimationManager_h_) */
diff --git a/layout/style/test/test_stylesheet_additions.html b/layout/style/test/test_stylesheet_additions.html
--- a/layout/style/test/test_stylesheet_additions.html
+++ b/layout/style/test/test_stylesheet_additions.html
@@ -25,37 +25,44 @@ const TESTS = [
   { selector: ".nonexistentClassScope", restyle: false },
   { selector: ".nonexistentClassScope + div", restyle: true },
   { selector: ".nonexistentClassScope div + div", restyle: false },
   { selector: ".classScope", restyle: true },
   { selector: ".classScope div", restyle: true },
   { selector: "#idScope", restyle: true },
   { selector: "#nonexistentIdScope", restyle: false },
   { selector: "#nonexistentIdScope div + bar", restyle: false },
+  { selector: "baz", restyle: false },
+  { cssText: " ", restyle: false },
+  { cssText: "@keyframes foo { from { color: green } to { color: red } } #whatever { animation-name: foo; }", restyle: false },
 ];
 
 for (const test of TESTS) {
-  target.innerHTML = test.selector + " { color: green; }";
+  let cssText = test.cssText ? test.cssText : (test.selector + " { color: green; }");
+  target.innerHTML = cssText;
 
   document.body.offsetWidth;
   const prevGeneration = utils.restyleGeneration;
 
   target.disabled = true;
 
   document.body.offsetWidth;
   (test.restyle ? isnot : is)(utils.restyleGeneration, prevGeneration,
-     `Stylesheet removal with ${test.selector} should ${test.restyle ? "have" : "not have"} caused a restyle`);
+     `Stylesheet removal with ${cssText} should ${test.restyle ? "have" : "not have"} caused a restyle`);
 
   target.disabled = false; // Make the stylesheet effective.
 
-  let element = document.querySelector(test.selector);
-  if (element) {
-    is(test.restyle, true, "How could we not expect a restyle?");
-    is(getComputedStyle(element).color, "rgb(0, 128, 0)",
-       "Element style should've changed appropriately");
+  if (test.selector) {
+    let element = document.querySelector(test.selector);
+    if (element) {
+      is(test.restyle, true, "How could we not expect a restyle?");
+      is(getComputedStyle(element).color, "rgb(0, 128, 0)",
+         "Element style should've changed appropriately");
+    }
   }
+
   document.body.offsetWidth;
   (test.restyle ? isnot : is)(utils.restyleGeneration, prevGeneration,
-     `Stylesheet addition with ${test.selector} should ${test.restyle ? "have" : "not have"} caused a restyle`);
+     `Stylesheet addition with ${cssText} should ${test.restyle ? "have" : "not have"} caused a restyle`);
 }
 
 SimpleTest.finish();
 </script>
