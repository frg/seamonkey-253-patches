# HG changeset patch
# User Andi-Bogdan Postelnicu <bpostelnicu@mozilla.com>
# Date 1588265186 0
# Node ID 8e4a846c1c5bdb6f5c1d871b4d0e826a2a09b79e
# Parent  1e7451bc8d3e7da32dc32f5b5b2cea707792cd0b
Bug 1634050 - [clang-format] When analyzing files outside of the tree take along the appropriate `.clang-format` file as well. r=sylvestre

Differential Revision: https://phabricator.services.mozilla.com/D73053

diff --git a/python/mozbuild/mozbuild/code-analysis/mach_commands.py b/python/mozbuild/mozbuild/code-analysis/mach_commands.py
--- a/python/mozbuild/mozbuild/code-analysis/mach_commands.py
+++ b/python/mozbuild/mozbuild/code-analysis/mach_commands.py
@@ -2181,16 +2181,50 @@ class StaticAnalysis(MachCommandBase):
 
         process = subprocess.Popen(args, stdin=subprocess.PIPE)
         with open(paths[0], 'r') as fin:
             process.stdin.write(fin.read())
             process.stdin.close()
             process.wait()
             return process.returncode
 
+    def _get_clang_format_cfg(self, current_dir):
+        clang_format_cfg_path = mozpath.join(current_dir, '.clang-format')
+
+        if os.path.exists(clang_format_cfg_path):
+            # Return found path for .clang-format
+            return clang_format_cfg_path
+
+        if current_dir != self.topsrcdir:
+            # Go to parent directory
+            return self._get_clang_format_cfg(os.path.split(current_dir)[0])
+        # We have reached self.topsrcdir so return None
+        return None
+
+    def _copy_clang_format_for_show_diff(self, current_dir, cached_clang_format_cfg, tmpdir):
+        # Lookup for .clang-format first in cache
+        clang_format_cfg = cached_clang_format_cfg.get(current_dir, None)
+
+        if clang_format_cfg is None:
+            # Go through top directories
+            clang_format_cfg = self._get_clang_format_cfg(current_dir)
+
+            # This is unlikely to happen since we must have .clang-format from
+            # self.topsrcdir but in any case we should handle a potential error
+            if clang_format_cfg is None:
+                print("Cannot find corresponding .clang-format.")
+                return 1
+
+            # Cache clang_format_cfg for potential later usage
+            cached_clang_format_cfg[current_dir] = clang_format_cfg
+
+        # Copy .clang-format to the tmp dir where the formatted file is copied
+        shutil.copy(clang_format_cfg, tmpdir)
+        return 0
+
     def _run_clang_format_path(self, clang_format, paths, output_file, output_format):
 
         # Run clang-format on files or directories directly
         from subprocess import check_output, CalledProcessError
 
         if output_format == 'json':
             # Get replacements in xml, then process to json
             args = [clang_format, '-output-replacements-xml']
@@ -2207,31 +2241,39 @@ class StaticAnalysis(MachCommandBase):
 
         if path_list == []:
             return
 
         print("Processing %d file(s)..." % len(path_list))
 
         if output_file:
             patches = {}
+            cached_clang_format_cfg = {}
             for i in range(0, len(path_list)):
                 l = path_list[i: (i + 1)]
 
                 # Copy the files into a temp directory
                 # and run clang-format on the temp directory
                 # and show the diff
                 original_path = l[0]
                 local_path = ntpath.basename(original_path)
+                current_dir = ntpath.dirname(original_path)
                 target_file = os.path.join(tmpdir, local_path)
                 faketmpdir = os.path.dirname(target_file)
                 if not os.path.isdir(faketmpdir):
                     os.makedirs(faketmpdir)
                 shutil.copy(l[0], faketmpdir)
                 l[0] = target_file
 
+                ret = self._copy_clang_format_for_show_diff(current_dir,
+                                                            cached_clang_format_cfg,
+                                                            faketmpdir)
+                if ret != 0:
+                    return ret
+
                 # Run clang-format on the list
                 try:
                     output = check_output(args + l)
                     if output and output_format == 'json':
                         # Output a relative path in json patch list
                         relative_path = os.path.relpath(original_path, self.topsrcdir)
                         patches[relative_path] = self._parse_xml_output(original_path, output)
                 except CalledProcessError as e:
