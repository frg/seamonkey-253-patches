# HG changeset patch
# User Andreas Pehrson <pehrsons@mozilla.com>
# Date 1519987437 -3600
#      Fri Mar 02 11:43:57 2018 +0100
# Node ID df7b724956eb943a25b30f3f75204f8d8255c1d2
# Parent  486b8df0c3aad6646c29a7be9f4932ab7c5894d3
Bug 1440169 - Take TrackTicks samples in SineWaveGenerator::generate. r=achronop,padenot

MediaEngineDefaultAudio uses the SineWaveGenerator and passes a
TrackTicks (int64_t) arg to generate(). It need to take the same type
or bad things can happen.

MozReview-Commit-ID: EoybtTFkWhT

diff --git a/dom/media/webrtc/SineWaveGenerator.h b/dom/media/webrtc/SineWaveGenerator.h
--- a/dom/media/webrtc/SineWaveGenerator.h
+++ b/dom/media/webrtc/SineWaveGenerator.h
@@ -1,15 +1,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef SINEWAVEGENERATOR_H_
 #define SINEWAVEGENERATOR_H_
 
+#include "MediaSegment.h"
+
 namespace mozilla {
 
 // generate 1k sine wave per second
 class SineWaveGenerator
 {
 public:
   static const int bytesPerSample = 2;
   static const int millisecondsPerSecond = PR_MSEC_PER_SEC;
@@ -23,21 +25,21 @@ public:
     mAudioBuffer = MakeUnique<int16_t[]>(mTotalLength);
     for (int i = 0; i < mTotalLength; i++) {
       // Set volume to -20db. It's from 32768.0 * 10^(-20/20) = 3276.8
       mAudioBuffer[i] = (3276.8f * sin(2 * M_PI * i / mTotalLength));
     }
   }
 
   // NOTE: only safely called from a single thread (MSG callback)
-  void generate(int16_t* aBuffer, int16_t aLengthInSamples) {
-    int16_t remaining = aLengthInSamples;
+  void generate(int16_t* aBuffer, TrackTicks aLengthInSamples) {
+    TrackTicks remaining = aLengthInSamples;
 
     while (remaining) {
-      int16_t processSamples = 0;
+      TrackTicks processSamples = 0;
 
       if (mTotalLength - mReadLength >= remaining) {
         processSamples = remaining;
       } else {
         processSamples = mTotalLength - mReadLength;
       }
       memcpy(aBuffer, &mAudioBuffer[mReadLength], processSamples * bytesPerSample);
       aBuffer += processSamples;
@@ -46,15 +48,15 @@ public:
       if (mReadLength == mTotalLength) {
         mReadLength = 0;
       }
     }
   }
 
 private:
   UniquePtr<int16_t[]> mAudioBuffer;
-  int16_t mTotalLength;
-  int16_t mReadLength;
+  TrackTicks mTotalLength;
+  TrackTicks mReadLength;
 };
 
 } // namespace mozilla
 
 #endif /* SINEWAVEGENERATOR_H_ */
