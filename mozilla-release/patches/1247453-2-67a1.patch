# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1549297175 18000
# Node ID 9fb802aba6b9167ce4ecc0bd3a30704aeb3fb7ae
# Parent  71ff941121c3276d9b2aa13f7ab03775ca05131b
Bug 1247453 - part 2 - be more stringent in checking for live pids; r=gbrown

We're seeing a lot of cases where our "check for zombie child processes"
check is finding live processes, but the minidumps that we get from such
processes are nonsense, and don't even feature Firefox symbols.

The working theory at this point, courtesy of bobowen, is that child
processes that we launch are getting closed during the test runs,
completely normally, and then we are finding other (non-Firefox) live
processes with the PIDs that were used for Firefox child processes at
the end of the test run.  This scenario is plausible due to Windows's
aggressive reuse of PIDs.  We don't see the same behavior on our Unix
test machines because Linux (and OS X, apparently) are not nearly as
aggressive in reusing PIDs.

Since we should be ensuring that any live processes are actually Firefox
processes anyway, let's add the appropriate check.  If the check works
to reduce the incidence of zombiecheck failures, that's great!  If not,
we've at least made our test runner more robust and can investigate
other possibilities for these intermittent failures.

diff --git a/testing/mochitest/runtests.py b/testing/mochitest/runtests.py
--- a/testing/mochitest/runtests.py
+++ b/testing/mochitest/runtests.py
@@ -356,23 +356,41 @@ if mozinfo.isWin:
         STILL_ACTIVE = 259
         PROCESS_QUERY_LIMITED_INFORMATION = 0x1000
         pHandle = ctypes.windll.kernel32.OpenProcess(
             PROCESS_QUERY_LIMITED_INFORMATION,
             0,
             pid)
         if not pHandle:
             return False
-        pExitCode = ctypes.wintypes.DWORD()
-        ctypes.windll.kernel32.GetExitCodeProcess(
-            pHandle,
-            ctypes.byref(pExitCode))
-        ctypes.windll.kernel32.CloseHandle(pHandle)
-        return pExitCode.value == STILL_ACTIVE
-
+
+        try:
+            pExitCode = ctypes.wintypes.DWORD()
+            ctypes.windll.kernel32.GetExitCodeProcess(
+                pHandle,
+                ctypes.byref(pExitCode))
+
+            if pExitCode.value != STILL_ACTIVE:
+                return False
+
+            # We have a live process handle.  But Windows aggressively
+            # re-uses pids, so let's attempt to verify that this is
+            # actually Firefox.
+            namesize = 1024
+            pName = ctypes.create_string_buffer(namesize)
+            namelen = ctypes.windll.kernel32.GetProcessImageFileNameA(pHandle,
+                                                                      pName,
+                                                                      namesize)
+            if namelen == 0:
+                # Still an active process, so conservatively assume it's Firefox.
+                return True
+
+            return pName.value.endswith(('firefox.exe', 'plugin-container.exe'))
+        finally:
+            ctypes.windll.kernel32.CloseHandle(pHandle)
 else:
     import errno
 
     def isPidAlive(pid):
         try:
             # kill(pid, 0) checks for a valid PID without actually sending a signal
             # The method throws OSError if the PID is invalid, which we catch
             # below.
