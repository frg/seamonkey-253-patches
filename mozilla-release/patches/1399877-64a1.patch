# HG changeset patch
# User Ted Mielczarek <ted@mielczarek.org>
# Date 1536687080 14400
# Node ID e186d8665b004ef0c016b861a56c4de1103bd1cf
# Parent  35619a822e095700acf97a3522e499c6e35cd8d5
Bug 1399877 - globally define MOZ_DLL_PREFIX/MOZ_DLL_SUFFIX; r=gps

Several source files use DLL_PREFIX/DLL_SUFFIX defines, and they all set
them in moz.build using `DEFINES`.  This is problematic for the WSL
build because the quoting gets lost somewhere between bash and cl.exe.
We cannot simply set them globally in moz.configure because their
stringified definitions would conflict with the `set_config` of
DLL_PREFIX/DLL_SUFFIX.  Therefore, we globally define
MOZ_DLL_PREFIX/MOZ_DLL_SUFFIX and change all define-related uses of
DLL_PREFIX/DLL_SUFFIX to use their MOZ-equivalents instead.

diff --git a/dom/system/OSFileConstants.cpp b/dom/system/OSFileConstants.cpp
--- a/dom/system/OSFileConstants.cpp
+++ b/dom/system/OSFileConstants.cpp
@@ -942,19 +942,19 @@ OSFileConstantsService::DefineOSFileCons
   // and we need to provide the full path.
   nsAutoString libxul;
   libxul.Append(mPaths->libDir);
   libxul.AppendLiteral("/XUL");
 #else
   // On other platforms, libxul is a library "xul" with regular
   // library prefix/suffix.
   nsAutoString libxul;
-  libxul.AppendLiteral(DLL_PREFIX);
+  libxul.AppendLiteral(MOZ_DLL_PREFIX);
   libxul.AppendLiteral("xul");
-  libxul.AppendLiteral(DLL_SUFFIX);
+  libxul.AppendLiteral(MOZ_DLL_SUFFIX);
 #endif // defined(XP_MACOSX)
 
   if (!SetStringProperty(aCx, objPath, "libxul", libxul)) {
     return false;
   }
 
   if (!SetStringProperty(aCx, objPath, "libDir", mPaths->libDir)) {
     return false;
@@ -1011,24 +1011,24 @@ OSFileConstantsService::DefineOSFileCons
     return false;
   }
 #endif // defined(XP_MACOSX)
 
   // sqlite3 is linked from different places depending on the platform
   nsAutoString libsqlite3;
 #if defined(ANDROID)
   // On Android, we use the system's libsqlite3
-  libsqlite3.AppendLiteral(DLL_PREFIX);
+  libsqlite3.AppendLiteral(MOZ_DLL_PREFIX);
   libsqlite3.AppendLiteral("sqlite3");
-  libsqlite3.AppendLiteral(DLL_SUFFIX);
+  libsqlite3.AppendLiteral(MOZ_DLL_SUFFIX);
 #elif defined(XP_WIN)
   // On Windows, for some reason, this is part of nss3.dll
-  libsqlite3.AppendLiteral(DLL_PREFIX);
+  libsqlite3.AppendLiteral(MOZ_DLL_PREFIX);
   libsqlite3.AppendLiteral("nss3");
-  libsqlite3.AppendLiteral(DLL_SUFFIX);
+  libsqlite3.AppendLiteral(MOZ_DLL_SUFFIX);
 #else
     // On other platforms, we link sqlite3 into libxul
   libsqlite3 = libxul;
 #endif // defined(ANDROID) || defined(XP_WIN)
 
   if (!SetStringProperty(aCx, objPath, "libsqlite3", libsqlite3)) {
     return false;
   }
diff --git a/dom/system/moz.build b/dom/system/moz.build
--- a/dom/system/moz.build
+++ b/dom/system/moz.build
@@ -74,13 +74,10 @@ FINAL_LIBRARY = 'xul'
 # We fire the nsDOMDeviceAcceleration
 LOCAL_INCLUDES += [
     '/dom/base',
     '/dom/bindings',
     '/js/xpconnect/loader',
     '/xpcom/base',
 ]
 
-DEFINES['DLL_PREFIX'] = '"%s"' % CONFIG['DLL_PREFIX']
-DEFINES['DLL_SUFFIX'] = '"%s"' % CONFIG['DLL_SUFFIX']
-
 MOCHITEST_CHROME_MANIFESTS += ['tests/chrome.ini']
 MOCHITEST_MANIFESTS += ['tests/mochitest.ini']
diff --git a/ipc/glue/moz.build b/ipc/glue/moz.build
--- a/ipc/glue/moz.build
+++ b/ipc/glue/moz.build
@@ -215,18 +215,17 @@ LOCAL_INCLUDES += [
     '/toolkit/xre',
     '/xpcom/threads',
 ]
 
 include('/ipc/chromium/chromium-config.mozbuild')
 
 FINAL_LIBRARY = 'xul'
 
-for var in ('MOZ_CHILD_PROCESS_NAME', 'MOZ_CHILD_PROCESS_BUNDLE',
-            'DLL_PREFIX', 'DLL_SUFFIX'):
+for var in ('MOZ_CHILD_PROCESS_NAME', 'MOZ_CHILD_PROCESS_BUNDLE'):
     DEFINES[var] = '"%s"' % CONFIG[var]
 
 if CONFIG['MOZ_SANDBOX'] and CONFIG['OS_ARCH'] == 'WINNT':
     LOCAL_INCLUDES += [
         '/security/sandbox/chromium',
         '/security/sandbox/chromium-shim',
         '/security/sandbox/win/src/sandboxbroker',
     ]
diff --git a/js/src/ctypes/Library.cpp b/js/src/ctypes/Library.cpp
--- a/js/src/ctypes/Library.cpp
+++ b/js/src/ctypes/Library.cpp
@@ -67,19 +67,19 @@ Library::Name(JSContext* cx, unsigned ar
   if (arg.isString()) {
     str = arg.toString();
   } else {
     JS_ReportErrorASCII(cx, "name argument must be a string");
     return false;
   }
 
   AutoString resultString;
-  AppendString(resultString, DLL_PREFIX);
+  AppendString(resultString, MOZ_DLL_PREFIX);
   AppendString(resultString, str);
-  AppendString(resultString, DLL_SUFFIX);
+  AppendString(resultString, MOZ_DLL_SUFFIX);
 
   JSString* result = JS_NewUCStringCopyN(cx, resultString.begin(),
                                          resultString.length());
   if (!result)
     return false;
 
   args.rval().setString(result);
   return true;
diff --git a/js/src/moz.build b/js/src/moz.build
--- a/js/src/moz.build
+++ b/js/src/moz.build
@@ -690,18 +690,16 @@ if CONFIG['MOZ_DEBUG'] or CONFIG['NIGHTL
 
 # Also set in shell/moz.build
 DEFINES['ENABLE_SHARED_ARRAY_BUFFER'] = True
 
 DEFINES['EXPORT_JS_API'] = True
 
 if CONFIG['JS_HAS_CTYPES']:
     DEFINES['JS_HAS_CTYPES'] = True
-    for var in ('DLL_PREFIX', 'DLL_SUFFIX'):
-        DEFINES[var] = '"%s"' % CONFIG[var]
 
 if CONFIG['MOZ_LINKER']:
     DEFINES['MOZ_LINKER'] = True
 
 if CONFIG['CC_TYPE'] in ('msvc', 'clang-cl'):
     # Prevent floating point errors caused by VC++ optimizations
     # XXX We should add this to CXXFLAGS, too?
     CFLAGS += ['-fp:precise']
diff --git a/moz.configure b/moz.configure
--- a/moz.configure
+++ b/moz.configure
@@ -231,16 +231,17 @@ set_config('HOST_DLL_PREFIX', host_libra
 set_config('HOST_DLL_SUFFIX', host_library_name_info.dll.suffix)
 set_config('LIB_PREFIX', library_name_info.lib.prefix)
 set_config('LIB_SUFFIX', library_name_info.lib.suffix)
 set_config('RUST_LIB_PREFIX', library_name_info.rust_lib.prefix)
 set_config('RUST_LIB_SUFFIX', library_name_info.rust_lib.suffix)
 set_config('OBJ_SUFFIX', library_name_info.obj.suffix)
 # Lots of compilation tests depend on this variable being present.
 add_old_configure_assignment('OBJ_SUFFIX', library_name_info.obj.suffix)
+set_define('MOZ_DLL_PREFIX', depends(library_name_info.dll.prefix)(lambda s: '"%s"' % s))
 set_config('IMPORT_LIB_SUFFIX', library_name_info.import_lib.suffix)
 set_define('MOZ_DLL_SUFFIX', depends(library_name_info.dll.suffix)(lambda s: '"%s"' % s))
 
 # Depends on host_library_name_info, so needs to go here.
 include('build/moz.configure/bindgen.configure',
         when='--enable-compile-environment')
 include(include_project_configure)
 
diff --git a/security/certverifier/NSSCertDBTrustDomain.cpp b/security/certverifier/NSSCertDBTrustDomain.cpp
--- a/security/certverifier/NSSCertDBTrustDomain.cpp
+++ b/security/certverifier/NSSCertDBTrustDomain.cpp
@@ -1149,28 +1149,28 @@ bool
 LoadLoadableRoots(const nsCString& dir, const nsCString& modNameUTF8)
 {
   // If a module exists with the same name, make a best effort attempt to delete
   // it. Note that it isn't possible to delete the internal module, so checking
   // the return value would be detrimental in that case.
   int unusedModType;
   Unused << SECMOD_DeleteModule(modNameUTF8.get(), &unusedModType);
   // Some NSS command-line utilities will load a roots module under the name
-  // "Root Certs" if there happens to be a `DLL_PREFIX "nssckbi" DLL_SUFFIX`
+  // "Root Certs" if there happens to be a `MOZ_DLL_PREFIX "nssckbi" MOZ_DLL_SUFFIX`
   // file in the directory being operated on. In some cases this can cause us to
   // fail to load our roots module. In these cases, deleting the "Root Certs"
   // module allows us to load the correct one. See bug 1406396.
   Unused << SECMOD_DeleteModule("Root Certs", &unusedModType);
 
   nsAutoCString fullLibraryPath;
   if (!dir.IsEmpty()) {
     fullLibraryPath.Assign(dir);
     fullLibraryPath.AppendLiteral(FILE_PATH_SEPARATOR);
   }
-  fullLibraryPath.Append(DLL_PREFIX "nssckbi" DLL_SUFFIX);
+  fullLibraryPath.Append(MOZ_DLL_PREFIX "nssckbi" MOZ_DLL_SUFFIX);
   // Escape the \ and " characters.
   fullLibraryPath.ReplaceSubstring("\\", "\\\\");
   fullLibraryPath.ReplaceSubstring("\"", "\\\"");
 
   nsAutoCString pkcs11ModuleSpec("name=\"");
   pkcs11ModuleSpec.Append(modNameUTF8);
   pkcs11ModuleSpec.AppendLiteral("\" library=\"");
   pkcs11ModuleSpec.Append(fullLibraryPath);
diff --git a/security/certverifier/moz.build b/security/certverifier/moz.build
--- a/security/certverifier/moz.build
+++ b/security/certverifier/moz.build
@@ -40,19 +40,16 @@ UNIFIED_SOURCES += [
     'SignedCertificateTimestamp.cpp',
 ]
 
 if not CONFIG['NSS_NO_EV_CERTS']:
     UNIFIED_SOURCES += [
         'ExtendedValidation.cpp',
     ]
 
-for var in ('DLL_PREFIX', 'DLL_SUFFIX'):
-    DEFINES[var] = '"%s"' % CONFIG[var]
-
 LOCAL_INCLUDES += [
     '/security/manager/ssl',
 ]
 
 TEST_DIRS += [
     'tests/gtest',
 ]
 
diff --git a/security/manager/ssl/PKCS11ModuleDB.cpp b/security/manager/ssl/PKCS11ModuleDB.cpp
--- a/security/manager/ssl/PKCS11ModuleDB.cpp
+++ b/security/manager/ssl/PKCS11ModuleDB.cpp
@@ -74,21 +74,21 @@ PKCS11ModuleDB::AddModule(const nsAStrin
                           int32_t aCipherFlags)
 {
   if (aModuleName.IsEmpty()) {
     return NS_ERROR_INVALID_ARG;
   }
 
   // "Root Certs" is the name some NSS command-line utilities will give the
   // roots module if they decide to load it when there happens to be a
-  // `DLL_PREFIX "nssckbi" DLL_SUFFIX` file in the directory being operated on.
-  // This causes failures, so as a workaround, the PSM initialization code will
-  // unconditionally remove any module named "Root Certs". We should prevent the
-  // user from adding an unrelated module named "Root Certs" in the first place
-  // so PSM doesn't delete it. See bug 1406396.
+  // `MOZ_DLL_PREFIX "nssckbi" MOZ_DLL_SUFFIX` file in the directory being
+  // operated on.  This causes failures, so as a workaround, the PSM
+  // initialization code will unconditionally remove any module named "Root
+  // Certs". We should prevent the user from adding an unrelated module named
+  // "Root Certs" in the first place so PSM doesn't delete it. See bug 1406396.
   if (aModuleName.EqualsLiteral("Root Certs")) {
     return NS_ERROR_ILLEGAL_VALUE;
   }
 
   // There appears to be a deadlock if we try to load modules concurrently, so
   // just wait until the loadable roots module has been loaded.
   nsresult rv = BlockUntilLoadableRootsLoaded();
   if (NS_FAILED(rv)) {
diff --git a/security/manager/ssl/moz.build b/security/manager/ssl/moz.build
--- a/security/manager/ssl/moz.build
+++ b/security/manager/ssl/moz.build
@@ -170,18 +170,16 @@ dafsa_data = GENERATED_FILES['nsSTSPrelo
 dafsa_data.script = '../../../xpcom/ds/make_dafsa.py'
 dafsa_data.inputs = ['nsSTSPreloadList.inc']
 
 if CONFIG['NSS_DISABLE_DBM']:
     DEFINES['NSS_DISABLE_DBM'] = '1'
 
 DEFINES['SSL_DISABLE_DEPRECATED_CIPHER_SUITE_NAMES'] = 'True'
 DEFINES['NSS_ENABLE_ECC'] = 'True'
-for var in ('DLL_PREFIX', 'DLL_SUFFIX'):
-    DEFINES[var] = '"%s"' % CONFIG[var]
 
 if not CONFIG['MOZ_SYSTEM_NSS']:
     USE_LIBS += [
         'crmf',
     ]
 
 # mozpkix is linked statically from the in-tree sources independent of whether
 # system NSS is used or not.
diff --git a/security/manager/ssl/nsNSSComponent.cpp b/security/manager/ssl/nsNSSComponent.cpp
--- a/security/manager/ssl/nsNSSComponent.cpp
+++ b/security/manager/ssl/nsNSSComponent.cpp
@@ -1220,22 +1220,22 @@ nsNSSComponent::CheckForSmartCardChanges
     }
   }
 #endif
 
   return NS_OK;
 }
 
 // Returns by reference the path to the directory containing the file that has
-// been loaded as DLL_PREFIX nss3 DLL_SUFFIX.
+// been loaded as MOZ_DLL_PREFIX nss3 MOZ_DLL_SUFFIX.
 static nsresult
 GetNSS3Directory(nsCString& result)
 {
   UniquePRString nss3Path(
-    PR_GetLibraryFilePathname(DLL_PREFIX "nss3" DLL_SUFFIX,
+    PR_GetLibraryFilePathname(MOZ_DLL_PREFIX "nss3" MOZ_DLL_SUFFIX,
                               reinterpret_cast<PRFuncPtr>(NSS_Initialize)));
   if (!nss3Path) {
     MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("nss not loaded?"));
     return NS_ERROR_FAILURE;
   }
   nsCOMPtr<nsIFile> nss3File(do_CreateInstance(NS_LOCAL_FILE_CONTRACTID));
   if (!nss3File) {
     MOZ_LOG(gPIPNSSLog, LogLevel::Debug, ("couldn't create a file?"));
@@ -1299,17 +1299,17 @@ LoadLoadableRootsTask::LoadLoadableRoots
     // the string and only use the localized version when displaying it to the
     // user, so this is a step in that direction anyway.
     modName.AssignLiteral("Builtin Roots Module");
   }
   NS_ConvertUTF16toUTF8 modNameUTF8(modName);
 
   Vector<nsCString> possibleCKBILocations;
   // First try in the directory where we've already loaded
-  // DLL_PREFIX nss3 DLL_SUFFIX, since that's likely to be correct.
+  // MOZ_DLL_PREFIX nss3 MOZ_DLL_SUFFIX, since that's likely to be correct.
   nsAutoCString nss3Dir;
   rv = GetNSS3Directory(nss3Dir);
   if (NS_SUCCEEDED(rv)) {
     if (!possibleCKBILocations.append(std::move(nss3Dir))) {
       return NS_ERROR_OUT_OF_MEMORY;
     }
   } else {
     // For some reason this fails on android. In any case, we should try with
