# HG changeset patch
# User Chris Manchester <cmanchester@mozilla.com>
# Date 1571264173 0
# Node ID fbcb7dcd82acfc9196c0dfd60e28248c25a4583b
# Parent  fd9846b184e1aa2659b18e5cc6189dcd03f2d61b
Bug 1587913 - Fix rust target detection for rustc 1.38. r=firefox-build-system-reviewers,glandium

Differential Revision: https://phabricator.services.mozilla.com/D48996

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -611,26 +611,27 @@ option('--target', nargs=1,
 @imports(_from='mozbuild.configure.constants', _import='CPU')
 @imports(_from='mozbuild.configure.constants', _import='CPU_bitness')
 @imports(_from='mozbuild.configure.constants', _import='Endianness')
 @imports(_from='mozbuild.configure.constants', _import='Kernel')
 @imports(_from='mozbuild.configure.constants', _import='OS')
 @imports(_from='__builtin__', _import='ValueError')
 def split_triplet(triplet):
     # The standard triplet is defined as
-    #   CPU_TYPE-MANUFACTURER-OPERATING_SYSTEM
+    #   CPU_TYPE-VENDOR-OPERATING_SYSTEM
     # There is also a quartet form:
-    #   CPU_TYPE-MANUFACTURER-KERNEL-OPERATING_SYSTEM
+    #   CPU_TYPE-VENDOR-KERNEL-OPERATING_SYSTEM
     # But we can consider the "KERNEL-OPERATING_SYSTEM" as one.
-    # Additionally, some may omit "unknown" when the manufacturer
+    # Additionally, some may omit "unknown" when the vendor
     # is not specified and emit
     #   CPU_TYPE-OPERATING_SYSTEM
+    vendor = 'unknown'
     parts = triplet.split('-', 2)
     if len(parts) == 3:
-        cpu, _, os = parts
+        cpu, vendor, os = parts
     elif len(parts) == 2:
         cpu, os = parts
     else:
         raise ValueError("Unexpected triplet string: %s" % triplet)
 
     # Autoconf uses config.sub to validate and canonicalize those triplets,
     # but the granularity of its results has never been satisfying to our
     # use, so we've had our own, different, canonicalization. We've also
@@ -731,16 +732,17 @@ def split_triplet(triplet):
         cpu=CPU(canonical_cpu),
         bitness=CPU_bitness[canonical_cpu],
         kernel=Kernel(canonical_kernel),
         os=OS(canonical_os),
         endianness=Endianness(endianness),
         raw_cpu=cpu,
         raw_os=os,
         toolchain=toolchain,
+        vendor=vendor,
     )
 
 
 # This defines a fake target/host namespace for when running with --help
 # If either --host or --target is passed on the command line, then fall
 # back to the real deal.
 @depends('--help', '--host', '--target')
 def help_host_target(help, host, target):
diff --git a/build/moz.configure/rust.configure b/build/moz.configure/rust.configure
--- a/build/moz.configure/rust.configure
+++ b/build/moz.configure/rust.configure
@@ -183,16 +183,22 @@ def rust_triple_alias(host_or_target):
                 else:
                     suffix = 'windows-msvc'
                 narrowed = [c for c in candidates if c.rust_target.endswith('-{}'.format(suffix))]
                 if len(narrowed) == 1:
                     return narrowed[0].rust_target
                 elif narrowed:
                     candidates = narrowed
 
+                vendor_aliases = {'pc': 'w64'}
+                narrowed = [c for c in candidates
+                            if vendor_aliases.get(c.target.vendor) == host_or_target.vendor]
+                if len(narrowed) == 1:
+                    return narrowed[0].rust_target
+
             # - For arm targets, correlate with arm_target
             #   we could be more thorough with the supported rust targets, but they
             #   don't support OSes that are supported to build Gecko anyways.
             #   Also, sadly, the only interface to check the rust target cpu features
             #   is --print target-spec-json, and it's unstable, so we have to rely on
             #   our own knowledge of what each arm target means.
             if host_or_target.cpu == 'arm' and host_or_target.endianness == 'little':
                 prefixes = []
@@ -247,16 +253,24 @@ def rust_triple_alias(host_or_target):
             narrowed = [
                 c for c in candidates
                 if c.target.raw_os == host_or_target.raw_os and
                    c.target.raw_cpu == host_or_target.raw_cpu
             ]
             if len(narrowed) == 1:
                 return narrowed[0].rust_target
 
+            # Finally, see if the vendor can be used to disambiguate.
+            narrowed = [
+                c for c in candidates
+                if c.target.vendor == host_or_target.vendor
+            ]
+            if len(narrowed) == 1:
+                return narrowed[0].rust_target
+
             return None
 
         rustc_target = find_candidate(candidates)
 
         if rustc_target is None:
             die("Don't know how to translate {} for rustc".format(
                 host_or_target.alias))
 
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolchain_configure.py
@@ -1703,28 +1703,52 @@ def gen_invoke_rustc(version):
                 ]
             # Additional targets from 1.36
             if Version(version) >= '1.36.0':
                 rust_targets += [
                     'wasm32-wasi',
                 ]
                 rust_targets.remove('wasm32-unknown-wasi')
                 rust_targets.remove('x86_64-unknown-bitrig')
+            # Additional targets from 1.37
+            if Version(version) >= '1.37.0':
+                rust_targets += [
+                    'x86_64-pc-solaris',
+                ]
+            # Additional targets from 1.38
+            if Version(version) >= '1.38.0':
+                rust_targets += [
+                    'aarch64-unknown-redox',
+                    'aarch64-wrs-vxworks',
+                    'armv7-unknown-linux-gnueabi',
+                    'armv7-unknown-linux-musleabi',
+                    'armv7-wrs-vxworks',
+                    'hexagon-unknown-linux-musl',
+                    'i586-wrs-vxworks',
+                    'i686-uwp-windows-gnu',
+                    'i686-wrs-vxworks',
+                    'powerpc-wrs-vxworks',
+                    'powerpc-wrs-vxworks-spe',
+                    'powerpc64-wrs-vxworks',
+                    'riscv32i-unknown-none-elf',
+                    'x86_64-uwp-windows-gnu',
+                    'x86_64-wrs-vxworks',
+                ]
             return 0, '\n'.join(sorted(rust_targets)), ''
         if (len(args) == 6 and args[:2] == ('--crate-type', 'staticlib') and
             args[2].startswith('--target=') and args[3] == '-o'):
             with open(args[4], 'w') as fh:
                 fh.write('foo')
             return 0, '', ''
         raise NotImplementedError('unsupported arguments')
     return invoke_rustc
 
 
 class RustTest(BaseConfigureTest):
-    def get_rust_target(self, target, compiler_type='gcc', version='1.36.0',
+    def get_rust_target(self, target, compiler_type='gcc', version='1.38.0',
                         arm_target=None):
         environ = {
             'PATH': os.pathsep.join(
                 mozpath.abspath(p) for p in ('/bin', '/usr/bin')),
         }
 
         paths = {
             mozpath.abspath('/usr/bin/cargo'): gen_invoke_cargo(version),
@@ -1753,17 +1777,16 @@ class RustTest(BaseConfigureTest):
             'i686-unknown-freebsd',
             'x86_64-unknown-freebsd',
             'sparc64-unknown-netbsd',
             'i686-unknown-netbsd',
             'x86_64-unknown-netbsd',
             'i686-unknown-openbsd',
             'x86_64-unknown-openbsd',
             'aarch64-unknown-linux-gnu',
-            'armv7-unknown-linux-gnueabihf',
             'sparc64-unknown-linux-gnu',
             'i686-unknown-linux-gnu',
             'i686-apple-darwin',
             'x86_64-apple-darwin',
             'mips-unknown-linux-gnu',
             'mipsel-unknown-linux-gnu',
             'mips64-unknown-linux-gnuabi64',
             'mips64el-unknown-linux-gnuabi64',
@@ -1774,31 +1797,34 @@ class RustTest(BaseConfigureTest):
 
         # Cases where the output of config.sub is different
         for autoconf, rust in (
             ('aarch64-unknown-linux-android', 'aarch64-linux-android'),
             ('arm-unknown-linux-androideabi', 'armv7-linux-androideabi'),
             ('armv7-unknown-linux-androideabi', 'armv7-linux-androideabi'),
             ('i386-unknown-linux-android', 'i686-linux-android'),
             ('i686-unknown-linux-android', 'i686-linux-android'),
+            ('i686-pc-linux-gnu', 'i686-unknown-linux-gnu'),
             ('x86_64-unknown-linux-android', 'x86_64-linux-android'),
             ('x86_64-pc-linux-gnu', 'x86_64-unknown-linux-gnu'),
             ('sparcv9-sun-solaris2', 'sparcv9-sun-solaris'),
             ('x86_64-sun-solaris2', 'x86_64-sun-solaris'),
         ):
             self.assertEqual(self.get_rust_target(autoconf), rust)
 
         # Windows
         for autoconf, building_with_gcc, rust in (
             ('i686-pc-mingw32', 'cl', 'i686-pc-windows-msvc'),
             ('x86_64-pc-mingw32', 'cl', 'x86_64-pc-windows-msvc'),
             ('i686-pc-mingw32', 'gcc', 'i686-pc-windows-gnu'),
             ('x86_64-pc-mingw32', 'gcc', 'x86_64-pc-windows-gnu'),
             ('i686-pc-mingw32', 'clang', 'i686-pc-windows-gnu'),
             ('x86_64-pc-mingw32', 'clang', 'x86_64-pc-windows-gnu'),
+            ('i686-w64-mingw32', 'clang', 'i686-pc-windows-gnu'),
+            ('x86_64-w64-mingw32', 'clang', 'x86_64-pc-windows-gnu'),
         ):
             self.assertEqual(self.get_rust_target(autoconf, building_with_gcc), rust)
 
         # Arm special cases
         self.assertEqual(
             self.get_rust_target('arm-unknown-linux-androideabi',
                                  arm_target=ReadOnlyNamespace(
                                      arm_arch=7, fpu='neon', thumb2=True, float_abi='softfp')),
