# HG changeset patch
# User Chris Peterson <cpeterson@mozilla.com>
# Date 1542689808 28800
# Node ID 85587782f51f248b59d27d72e4debce1f6221fbe
# Parent  ca350e6fc7238cfba0517fda44f6ea5fffc1bca2
Bug 1485179 - Part 1: Define a BORDER_STYLE_UNSET macro to abstract ownerStyle's sentinel value. r=dholbert

The value of BORDER_STYLE_UNSET will change in patch part 2 when we must shrink the sentinel value from 0xFF to 0xF to fit in the new 4-bit style bit fields.

Differential Revision: https://phabricator.services.mozilla.com/D12381

diff --git a/layout/tables/nsTableFrame.cpp b/layout/tables/nsTableFrame.cpp
--- a/layout/tables/nsTableFrame.cpp
+++ b/layout/tables/nsTableFrame.cpp
@@ -5280,22 +5280,27 @@ CompareBorders(const nsIFrame*  aTableFr
 
 static bool
 Perpendicular(mozilla::LogicalSide aSide1,
               mozilla::LogicalSide aSide2)
 {
   return IsInline(aSide1) != IsInline(aSide2);
 }
 
+// Initial value indicating that BCCornerInfo's ownerStyle hasn't been set yet.
+#define BORDER_STYLE_UNSET 0xFF
+
 // XXX allocate this as number-of-cols+1 instead of number-of-cols+1 * number-of-rows+1
 struct BCCornerInfo
 {
   BCCornerInfo() { ownerColor = 0; ownerWidth = subWidth = ownerElem = subSide =
                    subElem = hasDashDot = numSegs = bevel = 0; ownerSide = eLogicalSideBStart;
-                   ownerStyle = 0xFF; subStyle = NS_STYLE_BORDER_STYLE_SOLID;  }
+                   ownerStyle = BORDER_STYLE_UNSET;
+                   subStyle = NS_STYLE_BORDER_STYLE_SOLID; }
+
   void Set(mozilla::LogicalSide aSide,
            BCCellBorder  border);
 
   void Update(mozilla::LogicalSide aSide,
               BCCellBorder  border);
 
   nscolor   ownerColor;     // color of borderOwner
   uint16_t  ownerWidth;     // pixel width of borderOwner
@@ -5337,30 +5342,30 @@ BCCornerInfo::Set(mozilla::LogicalSide a
   subElem    = eTableOwner;
   subStyle   = NS_STYLE_BORDER_STYLE_SOLID;
 }
 
 void
 BCCornerInfo::Update(mozilla::LogicalSide aSide,
                      BCCellBorder  aBorder)
 {
-  bool existingWins = false;
-  if (0xFF == ownerStyle) { // initial value indiating that it hasn't been set yet
+  if (ownerStyle == BORDER_STYLE_UNSET) {
     Set(aSide, aBorder);
   }
   else {
     bool isInline = IsInline(aSide); // relative to the corner
     BCCellBorder oldBorder, tempBorder;
     oldBorder.owner  = (BCBorderOwner) ownerElem;
     oldBorder.style =  ownerStyle;
     oldBorder.width =  ownerWidth;
     oldBorder.color =  ownerColor;
 
     LogicalSide oldSide  = LogicalSide(ownerSide);
 
+    bool existingWins = false;
     tempBorder = CompareBorders(CELL_CORNER, oldBorder, aBorder, isInline, &existingWins);
 
     ownerElem  = tempBorder.owner;
     ownerStyle = tempBorder.style;
     ownerWidth = tempBorder.width;
     ownerColor = tempBorder.color;
     if (existingWins) { // existing corner is dominant
       if (::Perpendicular(LogicalSide(ownerSide), aSide)) {
