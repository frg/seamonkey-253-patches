# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1584550954 0
# Node ID ad00ba004bc0727c277aab655c57d765c84f1ed1
# Parent  36b877235085246a9ac0f179969b23cde2853845
Bug 1623339 - [lint.flake8] Fix path filtering bug when specifying subdirectory of excluded path, r=linter-reviewers,sylvestre

Differential Revision: https://phabricator.services.mozilla.com/D67335

diff --git a/docs/code-quality/lint/linters/flake8.rst.1623339.later b/docs/code-quality/lint/linters/flake8.rst.1623339.later
new file mode 100644
--- /dev/null
+++ b/docs/code-quality/lint/linters/flake8.rst.1623339.later
@@ -0,0 +1,54 @@
+--- flake8.rst
++++ flake8.rst
+@@ -1,12 +1,12 @@
+ Flake8
+ ======
+ 
+-`Flake8 <https://flake8.readthedocs.io/en/latest/>`__ is a popular lint wrapper for python. Under the hood, it runs three other tools and
++`Flake8 <https://flake8.pycqa.org/en/latest/index.html>`__ is a popular lint wrapper for python. Under the hood, it runs three other tools and
+ combines their results:
+ 
+ * `pep8 <http://pep8.readthedocs.io/en/latest/>`__ for checking style
+ * `pyflakes <https://github.com/pyflakes/pyflakes>`__ for checking syntax
+ * `mccabe <https://github.com/pycqa/mccabe>`__ for checking complexity
+ 
+ 
+ Run Locally
+@@ -20,32 +20,22 @@ The mozlint integration of flake8 can be
+ 
+ Alternatively, omit the ``--linter flake8`` and run all configured linters, which will include
+ flake8.
+ 
+ 
+ Configuration
+ -------------
+ 
+-Only directories explicitly whitelisted will have flake8 run against them. To enable flake8 linting
+-in a source directory, it must be added to the ``include`` directive in ```tools/lint/flake8.lint``.
+-If you wish to exclude a subdirectory of an included one, you can add it to the ``exclude``
+-directive.
++Path configuration is defined in the root `.flake8`_ file. Please update this file rather than
++``tools/lint/flake8.yml`` if you need to exclude a new path. For an overview of the supported
++configuration, see `flake8's documentation`_.
+ 
+-The default configuration file lives in ``topsrcdir/.flake8``. The default configuration can be
+-overridden for a given subdirectory by creating a new ``.flake8`` file in the subdirectory. Be warned
+-that ``.flake8`` files cannot inherit from one another, so all configuration you wish to keep must
+-be re-defined.
+-
+-.. warning::
+-
+-    Only ``.flake8`` files that live in a directory that is explicitly included in the ``include``
+-    directive will be considered. See `bug 1277851 <https://bugzilla.mozilla.org/show_bug.cgi?id=1277851>`__ for more details.
+-
+-For an overview of the supported configuration, see `flake8's documentation <https://flake8.readthedocs.io/en/latest/config.html>`__.
++.. _.flake8: https://searchfox.org/mozilla-central/source/.flake8
++.. _flake8's documentation: https://flake8.pycqa.org/en/latest/user/configuration.html
+ 
+ Autofix
+ -------
+ 
+ The flake8 linter provides a ``--fix`` option. It is based on `autopep8 <https://github.com/hhatto/autopep8>`__.
+ Please note that autopep8 does NOT fix all issues reported by flake8.
+ 
+ 
diff --git a/tools/lint/python/flake8.py b/tools/lint/python/flake8.py
--- a/tools/lint/python/flake8.py
+++ b/tools/lint/python/flake8.py
@@ -113,21 +113,29 @@ def lint(paths, config, **lintargs):
         rules, using `expand_exclusions` to turn directories into a list of
         relevant python files is an order of magnitude faster.
 
         Hooking into flake8 here also gives us a convenient place to merge the
         `exclude` rules specified in the root .flake8 with the ones added by
         tools/lint/mach_commands.py.
         """
         # Ignore exclude rules if `--no-filter` was passed in.
+        config.setdefault('exclude', [])
         if lintargs.get('use_filters', True):
-            config.setdefault('exclude', []).extend(self.options.exclude)
+            config['exclude'].extend(self.options.exclude)
+
+        # Since we use the root .flake8 file to store exclusions, we haven't
+        # properly filtered the paths through mozlint's `filterpaths` function
+        # yet. This mimics that though there could be other edge cases that are
+        # different. Maybe we should call `filterpaths` directly, though for
+        # now that doesn't appear to be necessary.
+        filtered = [p for p in paths if not any(p.startswith(e) for e in config['exclude'])]
 
         self.options.exclude = None
-        self.args = self.args + list(expand_exclusions(paths, config, root))
+        self.args = self.args + list(expand_exclusions(filtered, config, root))
 
         if not self.args:
             raise NothingToLint
         return orig_make_file_checker_manager()
 
     app.make_file_checker_manager = wrap_make_file_checker_manager.__get__(app, Application)
 
     # Make sure to run from repository root so exclusions are joined to the
diff --git a/tools/lint/test/files/flake8/subdir/exclude/bad.py b/tools/lint/test/files/flake8/subdir/exclude/exclude_subdir/bad.py
copy from tools/lint/test/files/flake8/subdir/exclude/bad.py
copy to tools/lint/test/files/flake8/subdir/exclude/exclude_subdir/bad.py
diff --git a/tools/lint/test/test_flake8.py b/tools/lint/test/test_flake8.py
--- a/tools/lint/test/test_flake8.py
+++ b/tools/lint/test/test_flake8.py
@@ -60,17 +60,17 @@ foo = ['A list of strings', 'that go ove
     # Make sure autopep8 reads the global config under lintargs['root']. If it
     # didn't, then the line-length over 80 would get fixed.
     with open(path, 'r') as fh:
         assert fh.read() == contents
 
 
 def test_lint_excluded_file(lint, paths, config):
     # First file is globally excluded, second one is from .flake8 config.
-    files = paths('bad.py', 'subdir/exclude/bad.py')
+    files = paths('bad.py', 'subdir/exclude/bad.py', 'subdir/exclude/exclude_subdir')
     config['exclude'] = paths('bad.py')
     results = lint(files, config)
     print(results)
     assert len(results) == 0
 
     # Make sure excludes also apply when running from a different cwd.
     cwd = paths('subdir')[0]
     os.chdir(cwd)
@@ -78,17 +78,17 @@ def test_lint_excluded_file(lint, paths,
     results = lint(paths('subdir/exclude'))
     print(results)
     assert len(results) == 0
 
 
 def test_lint_excluded_file_with_no_filter(lint, paths, config):
     results = lint(paths('subdir/exclude'), use_filters=False)
     print(results)
-    assert len(results) == 2
+    assert len(results) == 4
 
 
 def test_lint_uses_custom_extensions(lint, paths):
     assert len(lint(paths('ext'))) == 1
     assert len(lint(paths('ext/bad.configure'))) == 1
 
 
 if __name__ == '__main__':
