# HG changeset patch
# User Jeff Muizelaar <jmuizelaar@mozilla.com>
# Date 1518278094 18000
# Node ID ccb72c7fa363a2153832845ff5dce5dd81302d55
# Parent  e255b444fa436ee6152942010eca48d2c4262215
Bug 1437184. Inline FillAzure() r=bas

There's only one caller so let's just eliminate inline FillAzure into
Fill.

MozReview-Commit-ID: JYpAQMkhEcS

diff --git a/gfx/thebes/gfxContext.cpp b/gfx/thebes/gfxContext.cpp
--- a/gfx/thebes/gfxContext.cpp
+++ b/gfx/thebes/gfxContext.cpp
@@ -224,17 +224,34 @@ gfxContext::Fill()
 {
   Fill(PatternFromState(this));
 }
 
 void
 gfxContext::Fill(const Pattern& aPattern)
 {
   AUTO_PROFILER_LABEL("gfxContext::Fill", GRAPHICS);
-  FillAzure(aPattern, 1.0f);
+  AzureState &state = CurrentState();
+
+  CompositionOp op = GetOp();
+
+  if (mPathIsRect) {
+    MOZ_ASSERT(!mTransformChanged);
+
+    if (op == CompositionOp::OP_SOURCE) {
+      // Emulate cairo operator source which is bound by mask!
+      mDT->ClearRect(mRect);
+      mDT->FillRect(mRect, aPattern, DrawOptions(1.0f));
+    } else {
+      mDT->FillRect(mRect, aPattern, DrawOptions(1.0f, op, state.aaMode));
+    }
+  } else {
+    EnsurePath();
+    mDT->Fill(mPath, aPattern, DrawOptions(1.0f, op, state.aaMode));
+  }
 }
 
 void
 gfxContext::MoveTo(const gfxPoint& pt)
 {
   EnsurePathBuilder();
   mPathBuilder->MoveTo(ToPoint(pt));
 }
@@ -884,39 +901,16 @@ gfxContext::EnsurePathBuilder()
       gfxCriticalError() << "gfxContext::EnsurePathBuilder failed in PathBuilder::Finish";
     }
     mPathBuilder = path->TransformedCopyToBuilder(toNewUS);
   }
 
   mPathIsRect = false;
 }
 
-void
-gfxContext::FillAzure(const Pattern& aPattern, Float aOpacity)
-{
-  AzureState &state = CurrentState();
-
-  CompositionOp op = GetOp();
-
-  if (mPathIsRect) {
-    MOZ_ASSERT(!mTransformChanged);
-
-    if (op == CompositionOp::OP_SOURCE) {
-      // Emulate cairo operator source which is bound by mask!
-      mDT->ClearRect(mRect);
-      mDT->FillRect(mRect, aPattern, DrawOptions(aOpacity));
-    } else {
-      mDT->FillRect(mRect, aPattern, DrawOptions(aOpacity, op, state.aaMode));
-    }
-  } else {
-    EnsurePath();
-    mDT->Fill(mPath, aPattern, DrawOptions(aOpacity, op, state.aaMode));
-  }
-}
-
 CompositionOp
 gfxContext::GetOp()
 {
   if (CurrentState().op != CompositionOp::OP_SOURCE) {
     return CurrentState().op;
   }
 
   AzureState &state = CurrentState();
diff --git a/gfx/thebes/gfxContext.h b/gfx/thebes/gfxContext.h
--- a/gfx/thebes/gfxContext.h
+++ b/gfx/thebes/gfxContext.h
@@ -506,17 +506,16 @@ private:
     bool mContentChanged;
 #endif
   };
 
   // This ensures mPath contains a valid path (in user space!)
   void EnsurePath();
   // This ensures mPathBuilder contains a valid PathBuilder (in user space!)
   void EnsurePathBuilder();
-  void FillAzure(const Pattern& aPattern, mozilla::gfx::Float aOpacity);
   CompositionOp GetOp();
   void ChangeTransform(const mozilla::gfx::Matrix &aNewMatrix, bool aUpdatePatternTransform = true);
   Rect GetAzureDeviceSpaceClipBounds() const;
   Matrix GetDeviceTransform() const;
   Matrix GetDTTransform() const;
 
   bool mPathIsRect;
   bool mTransformChanged;
