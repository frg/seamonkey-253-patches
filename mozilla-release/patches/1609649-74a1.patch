# HG changeset patch
# User Gabriele Svelto <gsvelto@mozilla.com>
# Date 1579814531 0
# Node ID e13ec28f3d3f2cf8e71a11efb2024cb30c117efc
# Parent  2c5e0642886b5819ca4418690f9ee4796e2c1537
Bug 1609649 - Strip telemetry annotations from the crashes we submit via Firefox r=chutten

Differential Revision: https://phabricator.services.mozilla.com/D60145

diff --git a/browser/modules/test/browser/browser_UnsubmittedCrashHandler.js b/browser/modules/test/browser/browser_UnsubmittedCrashHandler.js
--- a/browser/modules/test/browser/browser_UnsubmittedCrashHandler.js
+++ b/browser/modules/test/browser/browser_UnsubmittedCrashHandler.js
@@ -98,19 +98,23 @@ function createPendingCrashReports(howMa
         tmpPath: file.path + ".tmp",
       }));
     }
     return Promise.all(promises);
   };
 
   let uuidGenerator = Cc["@mozilla.org/uuid-generator;1"]
                       .getService(Ci.nsIUUIDGenerator);
-  // CrashSubmit expects there to be a ServerURL key-value
-  // pair in the .extra file, so we'll satisfy it.
-  let extraFileContents = JSON.stringify({ ServerURL: SERVER_URL });
+  // Some annotations are always present in the .extra file and CrashSubmit.jsm
+  // expects there to be a ServerURL entry, so we'll add them here.
+  let extraFileContents = JSON.stringify({
+    ServerURL: SERVER_URL,
+    TelemetryServerURL: "http://telemetry.mozilla.org/",
+    TelemetryClientId: "c69e7487-df10-4c98-ab1a-c85660feecf3",
+  });
 
   return (async function() {
     let uuids = [];
     for (let i = 0; i < howMany; ++i) {
       let uuid = uuidGenerator.generateUUID().toString();
       // Strip the {}...
       uuid = uuid.substring(1, uuid.length - 1);
       await createFile(uuid, "dmp", accessDate);
@@ -135,16 +139,31 @@ function waitForSubmittedReports(reportI
   for (let reportID of reportIDs) {
     let promise = TestUtils.topicObserved("crash-report-status", (subject, data) => {
       if (data == "success") {
         let propBag = subject.QueryInterface(Ci.nsIPropertyBag2);
         let dumpID = propBag.getPropertyAsAString("minidumpID");
         if (dumpID == reportID) {
           return true;
         }
+          let extra = propBag.getPropertyAsInterface(
+            "extra",
+           Ci.nsIPropertyBag2
+          );
+          const blockedAnnotations = [
+            "ServerURL",
+            "TelemetryClientId",
+            "TelemetryServerURL",
+          ];
+          for (const key of blockedAnnotations) {
+            Assert.ok(
+              !extra.hasKey(key),
+              "The " + key + " annotation should have been stripped away"
+            );
+          }
       }
       return false;
     });
     promises.push(promise);
   }
   return Promise.all(promises);
 }
 
diff --git a/toolkit/crashreporter/CrashSubmit.jsm b/toolkit/crashreporter/CrashSubmit.jsm
--- a/toolkit/crashreporter/CrashSubmit.jsm
+++ b/toolkit/crashreporter/CrashSubmit.jsm
@@ -223,35 +223,32 @@ Submitter.prototype = {
     return parsedResponse;
   },
 
   submitForm: function Submitter_submitForm() {
     if (!("ServerURL" in this.extraKeyVals)) {
       return false;
     }
     let serverURL = this.extraKeyVals.ServerURL;
+    delete this.extraKeyVals.ServerURL;
 
     // Override the submission URL from the environment
-
     let envOverride = Cc["@mozilla.org/process/environment;1"]
       .getService(Ci.nsIEnvironment)
       .get("MOZ_CRASHREPORTER_URL");
     if (envOverride != "") {
       serverURL = envOverride;
     }
 
     let xhr = new XMLHttpRequest();
     xhr.open("POST", serverURL, true);
 
     let formData = Cc["@mozilla.org/files/formdata;1"]
                    .createInstance(Ci.nsIDOMFormData);
     // add the data
-    delete this.extraKeyVals.ServerURL;
-    delete this.extraKeyVals.StackTraces;
-
     let payload = Object.assign({}, this.extraKeyVals);
     if (this.noThrottle) {
       // tell the server not to throttle this, since it was manually submitted
       payload.Throttleable = "0";
     }
     let json = new Blob([JSON.stringify(payload)], {
       type: "application/json",
     });
@@ -360,16 +357,32 @@ Submitter.prototype = {
       case FAILED:
         this.rejectSubmitStatusPromise(FAILED);
         break;
       default:
       // no callbacks invoked.
     }
   },
 
+  readAnnotations: async function Submitter_readAnnotations(extra) {
+    // These annotations are used only by the crash reporter client and should
+    // not be submitted to Socorro.
+    const strippedAnnotations = [
+      "StackTraces",
+      "TelemetryClientId",
+      "TelemetryServerURL",
+    ];
+    let decoder = new TextDecoder();
+    let extraData = await OS.File.read(extra);
+    let extraKeyVals = JSON.parse(decoder.decode(extraData));
+
+    this.extraKeyVals = { ...extraKeyVals, ...this.extraKeyVals };
+    strippedAnnotations.forEach(key => delete this.extraKeyVals[key]);
+  },
+
   submit: async function Submitter_submit() {
     if (this.recordSubmission) {
       await Services.crashmanager.ensureCrashIsPresent(this.id);
     }
 
     let [dump, extra, memory] = getPendingMinidump(this.id);
     let [dumpExists, extraExists, memoryExists] = await Promise.all([
       OS.File.exists(dump),
@@ -385,26 +398,17 @@ Submitter.prototype = {
 
     if (!extraExists) {
       await synthesizeExtraFile(extra);
     }
 
     this.dump = dump;
     this.extra = extra;
     this.memory = memoryExists ? memory : null;
-
-    let decoder = new TextDecoder();
-    let extraData = await OS.File.read(extra);
-    let extraKeyVals = JSON.parse(decoder.decode(extraData));
-
-    for (let key in extraKeyVals) {
-      if (!(key in this.extraKeyVals)) {
-        this.extraKeyVals[key] = extraKeyVals[key];
-      }
-    }
+    await this.readAnnotations(extra);
 
     let additionalDumps = [];
 
     if ("additional_minidumps" in this.extraKeyVals) {
       let dumpsExistsPromises = [];
       let names = this.extraKeyVals.additional_minidumps.split(",");
 
       for (let name of names) {
