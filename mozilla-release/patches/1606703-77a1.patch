# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1586222381 0
# Node ID b7d4ff29805ddabfebebb34b1e5beee07fcad45b
# Parent  0bab19c796a42531d9b6ff8a1713bd7134d69c54
Bug 1606703 - Switch python configure lint to python 3. r=rstewart

Support for python 2 is completely dropped. It wouldn't be too much code
to support both python 2 and 3 but since configure is going to switch to
python 3 shortly after, it's not worth the effort.

Differential Revision: https://phabricator.services.mozilla.com/D69525

diff --git a/python/mozbuild/mozbuild/configure/lint.py b/python/mozbuild/mozbuild/configure/lint.py
--- a/python/mozbuild/mozbuild/configure/lint.py
+++ b/python/mozbuild/mozbuild/configure/lint.py
@@ -1,26 +1,26 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import inspect
+from dis import Bytecode
 from functools import wraps
-from StringIO import StringIO
+from io import StringIO
 from . import (
     CombinedDependsFunction,
     ConfigureError,
     ConfigureSandbox,
     DependsFunction,
     SandboxedGlobal,
     TrivialDependsFunction,
 )
-from .lint_util import disassemble_as_iter
 from mozbuild.util import memoize
 
 
 class LintSandbox(ConfigureSandbox):
     def __init__(self, environ=None, argv=None, stdout=None, stderr=None):
         out = StringIO()
         stdout = stdout or out
         stderr = stderr or out
@@ -30,17 +30,17 @@ class LintSandbox(ConfigureSandbox):
         self._has_imports = set()
         super(LintSandbox, self).__init__({}, environ=environ, argv=argv,
                                           stdout=stdout, stderr=stderr)
 
     def run(self, path=None):
         if path:
             self.include_file(path)
 
-        for dep in self._depends.itervalues():
+        for dep in self._depends.values():
             self._check_dependencies(dep)
 
     def _raise_from(self, exception, obj, line=0):
         '''
         Raises the given exception as if it were emitted from the given
         location.
 
         The location is determined from the values of obj and line.
@@ -52,18 +52,18 @@ class LintSandbox(ConfigureSandbox):
         def thrower(e):
             raise e
 
         if isinstance(obj, DependsFunction):
             obj, _ = self.unwrap(obj._func)
 
         if inspect.isfunction(obj):
             funcname = obj.__name__
-            filename = obj.func_code.co_filename
-            firstline = obj.func_code.co_firstlineno
+            filename = obj.__code__.co_filename
+            firstline = obj.__code__.co_firstlineno
             line += firstline
         elif inspect.isframe(obj):
             funcname = obj.f_code.co_name
             filename = obj.f_code.co_filename
             firstline = obj.f_code.co_firstlineno
             line = obj.f_lineno
         else:
             # Don't know how to handle the given location, still raise the
@@ -74,20 +74,21 @@ class LintSandbox(ConfigureSandbox):
         # the `def` line is on the first line of the function given as
         # argument, and the `raise` line is on the line given as argument.
 
         offset = line - firstline
         # co_lnotab is a string where each pair of consecutive character is
         # (chr(byte_increment), chr(line_increment)), mapping bytes in co_code
         # to line numbers relative to co_firstlineno.
         # If the offset we need to encode is larger than 255, we need to split it.
-        co_lnotab = (chr(0) + chr(255)) * (offset / 255) + chr(0) + chr(offset % 255)
+        co_lnotab = bytes([0, 255] * (offset // 255) + [0, offset % 255])
         code = thrower.func_code
         code = types.CodeType(
             code.co_argcount,
+            code.co_kwonlyargcount,
             code.co_nlocals,
             code.co_stacksize,
             code.co_flags,
             code.co_code,
             code.co_consts,
             code.co_names,
             code.co_varnames,
             filename,
@@ -115,20 +116,20 @@ class LintSandbox(ConfigureSandbox):
                     'Keyword arguments are not allowed in @depends functions')
             self._raise_from(e, func)
 
         all_args = list(func_args.args)
         if func_args.varargs:
             all_args.append(func_args.varargs)
         used_args = set()
 
-        for op, arg, _ in disassemble_as_iter(func):
-            if op in ('LOAD_FAST', 'LOAD_CLOSURE'):
-                if arg in all_args:
-                    used_args.add(arg)
+        for instr in Bytecode(func):
+            if instr.opname in ('LOAD_FAST', 'LOAD_CLOSURE'):
+                if instr.argval in all_args:
+                    used_args.add(instr.argval)
 
         for num, arg in enumerate(all_args):
             if arg not in used_args:
                 dep = obj.dependencies[num]
                 if dep != self._help_option or not self._need_help_dependency(obj):
                     if isinstance(dep, DependsFunction):
                         dep = dep.name
                     else:
@@ -144,24 +145,24 @@ class LintSandbox(ConfigureSandbox):
                 return False
             func, glob = self.unwrap(obj._func)
             # We allow missing --help dependencies for functions that:
             # - don't use @imports
             # - don't have a closure
             # - don't use global variables
             if func in self._has_imports or func.__closure__:
                 return True
-            for op, arg, _ in disassemble_as_iter(func):
-                if op in ('LOAD_GLOBAL', 'STORE_GLOBAL'):
+            for instr in Bytecode(func):
+                if instr.opname in ('LOAD_GLOBAL', 'STORE_GLOBAL'):
                     # There is a fake os module when one is not imported,
                     # and it's allowed for functions without a --help
                     # dependency.
-                    if arg == 'os' and glob.get('os') is self.OS:
+                    if instr.argval == 'os' and glob.get('os') is self.OS:
                         continue
-                    if arg in self.BUILTINS:
+                    if instr.argval in self.BUILTINS:
                         continue
                     return True
         return False
 
     def _missing_help_dependency(self, obj):
         if (isinstance(obj, DependsFunction) and
                 self._help_option in obj.dependencies):
             return False
@@ -209,27 +210,27 @@ class LintSandbox(ConfigureSandbox):
         _, glob = self.unwrap(wrapped)
         imports = set()
         for _from, _import, _as in self._imports.get(func, ()):
             if _as:
                 imports.add(_as)
             else:
                 what = _import.split('.')[0]
                 imports.add(what)
-        for op, arg, line in disassemble_as_iter(func):
+        for instr in Bytecode(func):
             code = func.func_code
-            if op == 'LOAD_GLOBAL' and \
-                    arg not in glob and \
-                    arg not in imports and \
-                    arg not in glob['__builtins__'] and \
-                    arg not in code.co_varnames[:code.co_argcount]:
+            if instr.opname == 'LOAD_GLOBAL' and \
+                    instr.argval not in glob and \
+                    instr.argval not in imports and \
+                    instr.argval not in glob['__builtins__'] and \
+                    instr.argval not in code.co_varnames[:code.co_argcount]:
                 # Raise the same kind of error as what would happen during
                 # execution.
-                e = NameError("global name '{}' is not defined".format(arg))
-                self._raise_from(e, func, line)
+                e = NameError("global name '{}' is not defined".format(instr.argval))
+                self._raise_from(e, func, instr.starts_line - func.__code__.co_firstlineno)
 
         return wrapped
     def imports_impl(self, _import, _from=None, _as=None):
         wrapper = super(LintSandbox, self).imports_impl(_import, _from=_from, _as=_as)
         def decorator(func):
             self._has_imports.add(func)
             return wrapper(func)
         return decorator
diff --git a/python/mozbuild/mozbuild/configure/lint.py.1606703.later b/python/mozbuild/mozbuild/configure/lint.py.1606703.later
new file mode 100644
--- /dev/null
+++ b/python/mozbuild/mozbuild/configure/lint.py.1606703.later
@@ -0,0 +1,40 @@
+--- lint.py
++++ lint.py
+@@ -220,17 +221,17 @@ class LintSandbox(ConfigureSandbox):
+                 'enable': 'disable',
+                 'with': 'without',
+             },
+             False: {
+                 'disable': 'enable',
+                 'without': 'with',
+             }
+         }
+-        for prefix, replacement in table[default].iteritems():
++        for prefix, replacement in table[default].items():
+             if name.startswith('--{}-'.format(prefix)):
+                 frame = inspect.currentframe()
+                 while frame and frame.f_code.co_name != self.option_impl.__name__:
+                     frame = frame.f_back
+                 e = ConfigureError('{} should be used instead of '
+                                    '{} with default={}'.format(
+                                        name.replace('--{}-'.format(prefix),
+                                                     '--{}-'.format(replacement)),
+@@ -242,17 +243,17 @@ class LintSandbox(ConfigureSandbox):
+ 
+         if not isinstance(default, SandboxDependsFunction):
+             return
+ 
+         if not option.prefix:
+             return
+ 
+         default = self._resolve(default)
+-        if type(default) in (str, unicode):
++        if type(default) is str:
+             return
+ 
+         help = kwargs['help']
+         match = re.search(HelpFormatter.RE_FORMAT, help)
+         if match:
+             return
+ 
+         if option.prefix in ('enable', 'disable'):
diff --git a/python/mozbuild/mozbuild/configure/lint_util.py b/python/mozbuild/mozbuild/configure/lint_util.py
deleted file mode 100644
--- a/python/mozbuild/mozbuild/configure/lint_util.py
+++ /dev/null
@@ -1,74 +0,0 @@
-# This Source Code Form is subject to the terms of the Mozilla Public
-# License, v. 2.0. If a copy of the MPL was not distributed with this
-# file, You can obtain one at http://mozilla.org/MPL/2.0/.
-
-from __future__ import absolute_import, print_function, unicode_literals
-
-import dis
-import inspect
-import itertools
-
-
-# Like python 3.2's itertools.accumulate
-def accumulate(iterable):
-    t = 0
-    for i in iterable:
-        t += i
-        yield t
-
-
-# dis.dis only outputs to stdout. This is a modified version that
-# returns an iterator, and includes line numbers.
-def disassemble_as_iter(co):
-    if inspect.ismethod(co):
-        co = co.im_func
-    if inspect.isfunction(co):
-        co = co.func_code
-    code = co.co_code
-    n = len(code)
-    i = 0
-    extended_arg = 0
-    free = None
-    line = 0
-    # co_lnotab is a string where each pair of consecutive character is
-    # (chr(byte_increment), chr(line_increment)), mapping bytes in co_code
-    # to line numbers relative to co_firstlineno.
-    # We want to iterate over pairs of (accumulated_byte, accumulated_line).
-    lnotab = itertools.chain(
-        itertools.izip(accumulate(ord(c) for c in co.co_lnotab[0::2]),
-                       accumulate(ord(c) for c in co.co_lnotab[1::2])),
-        (None,))
-    next_byte_line = lnotab.next()
-    while i < n:
-        while next_byte_line and i >= next_byte_line[0]:
-            line = next_byte_line[1]
-            next_byte_line = lnotab.next()
-        c = code[i]
-        op = ord(c)
-        opname = dis.opname[op]
-        i += 1
-        if op >= dis.HAVE_ARGUMENT:
-            arg = ord(code[i]) + ord(code[i + 1]) * 256 + extended_arg
-            extended_arg = 0
-            i += 2
-            if op == dis.EXTENDED_ARG:
-                extended_arg = arg * 65536
-                continue
-            if op in dis.hasconst:
-                yield opname, co.co_consts[arg], line
-            elif op in dis.hasname:
-                yield opname, co.co_names[arg], line
-            elif op in dis.hasjrel:
-                yield opname, i + arg, line
-            elif op in dis.haslocal:
-                yield opname, co.co_varnames[arg], line
-            elif op in dis.hascompare:
-                yield opname, dis.cmp_op[arg], line
-            elif op in dis.hasfree:
-                if free is None:
-                    free = co.co_cellvars + co.co_freevars
-                yield opname, free[arg], line
-            else:
-                yield opname, None, line
-        else:
-            yield opname, None, line
diff --git a/python/mozbuild/mozbuild/test/configure/lint.py b/python/mozbuild/mozbuild/test/configure/lint.py
--- a/python/mozbuild/mozbuild/test/configure/lint.py
+++ b/python/mozbuild/mozbuild/test/configure/lint.py
@@ -8,16 +8,17 @@ import os
 import unittest
 from mozunit import main
 from buildconfig import (
     topobjdir,
     topsrcdir,
 )
 
 from mozbuild.configure.lint import LintSandbox
+import six
 
 
 test_path = os.path.abspath(__file__)
 
 
 class LintMeta(type):
     def __new__(mcs, name, bases, attrs):
         def create_test(project, func):
@@ -33,19 +34,19 @@ class LintMeta(type):
             'mobile/android',
         ):
             attrs['test_%s' % project.replace('/', '_')] = create_test(
                 project, attrs['lint'])
 
         return type.__new__(mcs, name, bases, attrs)
 
 
+# We don't actually need python2 compat, but this makes flake8 happy.
+@six.add_metaclass(LintMeta)
 class Lint(unittest.TestCase):
-    __metaclass__ = LintMeta
-
     def setUp(self):
         self._curdir = os.getcwd()
         os.chdir(topobjdir)
 
     def tearDown(self):
         os.chdir(self._curdir)
 
     def lint(self, project):
diff --git a/python/mozbuild/mozbuild/test/configure/test_lint.py b/python/mozbuild/mozbuild/test/configure/test_lint.py
--- a/python/mozbuild/mozbuild/test/configure/test_lint.py
+++ b/python/mozbuild/mozbuild/test/configure/test_lint.py
@@ -73,17 +73,17 @@ class TestLint(unittest.TestCase):
                     return value
 
                 @depends('--help', foo)
                 def bar(help, foo):
                     return foo
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The dependency on `--help` is unused")
 
         with self.assertRaisesFromLine(ConfigureError, 3) as e:
             with self.moz_configure('''
                 option('--foo', help='foo')
                 @depends('--foo')
                 @imports('os')
                 def foo(value):
@@ -92,17 +92,17 @@ class TestLint(unittest.TestCase):
                 @depends('--help', foo)
                 @imports('os')
                 def bar(help, foo):
                     return foo
             '''):
                 self.lint_test()
 
         self.assertEquals(
-            e.exception.message,
+            str(e.exception),
             "Missing '--help' dependency because `bar` depends on '--help' and `foo`")
 
         with self.assertRaisesFromLine(ConfigureError, 7) as e:
             with self.moz_configure('''
                 @template
                 def tmpl():
                     qux = 42
 
@@ -116,17 +116,17 @@ class TestLint(unittest.TestCase):
                     @imports('os')
                     def bar(help, foo):
                         return foo
                 tmpl()
             '''):
                 self.lint_test()
 
         self.assertEquals(
-            e.exception.message,
+            str(e.exception),
             "Missing '--help' dependency because `bar` depends on '--help' and `foo`")
 
         with self.moz_configure('''
             option('--foo', help='foo')
             @depends('--foo')
             def foo(value):
                 return value
 
@@ -141,17 +141,17 @@ class TestLint(unittest.TestCase):
                 @imports('os')
                 def foo(value):
                     return value
 
                 include(foo)
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "Missing '--help' dependency")
 
         with self.assertRaisesFromLine(ConfigureError, 3) as e:
             with self.moz_configure('''
                 option('--foo', help='foo')
                 @depends('--foo')
                 @imports('os')
                 def foo(value):
@@ -160,32 +160,32 @@ class TestLint(unittest.TestCase):
                 @depends(foo)
                 def bar(value):
                     return value
 
                 include(bar)
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "Missing '--help' dependency")
 
         with self.assertRaisesFromLine(ConfigureError, 3) as e:
             with self.moz_configure('''
                 option('--foo', help='foo')
                 @depends('--foo')
                 @imports('os')
                 def foo(value):
                     return value
 
                 option('--bar', help='bar', when=foo)
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "Missing '--help' dependency")
 
         # This would have failed with "Missing '--help' dependency"
         # in the past, because of the reference to the builtin False.
         with self.moz_configure('''
             option('--foo', help='foo')
             @depends('--foo')
             def foo(value):
@@ -196,29 +196,29 @@ class TestLint(unittest.TestCase):
             self.lint_test()
 
         # However, when something that is normally a builtin is overridden,
         # we should still want the dependency on --help.
         with self.assertRaisesFromLine(ConfigureError, 7) as e:
             with self.moz_configure('''
                 @template
                 def tmpl():
-                    False = 42
+                    sorted = 42
 
                     option('--foo', help='foo')
                     @depends('--foo')
                     def foo(value):
-                        return False
+                        return sorted
 
                     option('--bar', help='bar', when=foo)
                 tmpl()
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "Missing '--help' dependency")
 
         # There is a default restricted `os` module when there is no explicit
         # @imports, and it's fine to use it without a dependency on --help.
         with self.moz_configure('''
             option('--foo', help='foo')
             @depends('--foo')
             def foo(value):
@@ -235,46 +235,46 @@ class TestLint(unittest.TestCase):
                 @depends('--foo')
                 def foo(value):
                     return
 
                 include(foo)
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The dependency on `--foo` is unused")
 
         with self.assertRaisesFromLine(ConfigureError, 5) as e:
             with self.moz_configure('''
                 @depends(when=True)
                 def bar():
                     return
                 @depends(bar)
                 def foo(value):
                     return
 
                 include(foo)
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The dependency on `bar` is unused")
 
         with self.assertRaisesFromLine(ConfigureError, 2) as e:
             with self.moz_configure('''
                 @depends(depends(when=True)(lambda: None))
                 def foo(value):
                     return
 
                 include(foo)
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The dependency on `<lambda>` is unused")
 
         with self.assertRaisesFromLine(ConfigureError, 9) as e:
             with self.moz_configure('''
                 @template
                 def tmpl():
                     @depends(when=True)
                     def bar():
@@ -284,29 +284,29 @@ class TestLint(unittest.TestCase):
                 @depends(qux)
                 def foo(value):
                     return
 
                 include(foo)
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The dependency on `qux` is unused")
 
     def test_undefined_global(self):
         with self.assertRaisesFromLine(NameError, 6) as e:
             with self.moz_configure('''
                 option(env='FOO', help='foo')
                 @depends('FOO')
                 def foo(value):
                     if value:
                         return unknown
                     return value
             '''):
                 self.lint_test()
 
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "global name 'unknown' is not defined")
 
 
 if __name__ == '__main__':
     main()
diff --git a/python/mozbuild/mozbuild/test/python.ini b/python/mozbuild/mozbuild/test/python.ini
--- a/python/mozbuild/mozbuild/test/python.ini
+++ b/python/mozbuild/mozbuild/test/python.ini
@@ -7,19 +7,23 @@ subsuite = mozbuild
 [backend/test_fastermake.py]
 [backend/test_recursivemake.py]
 [backend/test_build.py]
 [backend/test_configenvironment.py]
 [backend/test_gn_processor.py]
 [backend/test_partialconfigenvironment.py]
 [backend/test_test_manifest.py]
 [backend/test_visualstudio.py]
+[configure/lint.py]
+skip-if = python == 2
 [configure/test_checks_configure.py]
 [configure/test_compile_checks.py]
 [configure/test_configure.py]
+[configure/test_lint.py]
+skip-if = python == 2
 [configure/test_moz_configure.py]
 [configure/test_options.py]
 [configure/test_toolchain_configure.py]
 [configure/test_toolchain_helpers.py]
 [configure/test_toolkit_moz_configure.py]
 [configure/test_util.py]
 [controller/test_ccachestats.py]
 [controller/test_clobber.py]
diff --git a/python/mozbuild/mozbuild/test/python2.ini b/python/mozbuild/mozbuild/test/python2.ini
--- a/python/mozbuild/mozbuild/test/python2.ini
+++ b/python/mozbuild/mozbuild/test/python2.ini
@@ -1,7 +1,5 @@
 [DEFAULT]
 subsuite = mozbuild
 
 [codecoverage/test_lcov_rewrite.py]
 [compilation/test_warnings.py]
-[configure/lint.py]
-[configure/test_lint.py]
