# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1517383348 -3600
# Node ID 69e7ac848a5d98e3eb94ba0c5fcd069ac432a5cc
# Parent  89a644ee8f6eacc5e790ae5e2e203bd0ff8a1ff7
Bug 1432963 - Fixing workers headers - part 11 - MessageEventRunnable without workers namespace, r=smaug

diff --git a/dom/workers/ChromeWorkerScope.cpp b/dom/workers/ChromeWorkerScope.cpp
--- a/dom/workers/ChromeWorkerScope.cpp
+++ b/dom/workers/ChromeWorkerScope.cpp
@@ -9,18 +9,18 @@
 #include "jsapi.h"
 
 #include "nsXPCOM.h"
 #include "nsNativeCharsetUtils.h"
 #include "nsString.h"
 
 #include "WorkerPrivate.h"
 
-using namespace mozilla::dom;
-USING_WORKERS_NAMESPACE
+namespace mozilla {
+namespace dom {
 
 namespace {
 
 #ifdef BUILD_CTYPES
 
 char*
 UnicodeToNative(JSContext* aCx, const char16_t* aSource, size_t aSourceLen)
 {
@@ -41,18 +41,16 @@ UnicodeToNative(JSContext* aCx, const ch
   result[native.Length()] = 0;
   return result;
 }
 
 #endif // BUILD_CTYPES
 
 } // namespace
 
-BEGIN_WORKERS_NAMESPACE
-
 bool
 DefineChromeWorkerFunctions(JSContext* aCx, JS::Handle<JSObject*> aGlobal)
 {
   // Currently ctypes is the only special property given to ChromeWorkers.
 #ifdef BUILD_CTYPES
   {
     JS::Rooted<JS::Value> ctypes(aCx);
     if (!JS_InitCTypesClass(aCx, aGlobal) ||
@@ -66,9 +64,10 @@ DefineChromeWorkerFunctions(JSContext* a
 
     JS_SetCTypesCallbacks(ctypes.toObjectOrNull(), &callbacks);
   }
 #endif // BUILD_CTYPES
 
   return true;
 }
 
-END_WORKERS_NAMESPACE
+} // dom namespace
+} // mozilla namespace
diff --git a/dom/workers/ChromeWorkerScope.h b/dom/workers/ChromeWorkerScope.h
--- a/dom/workers/ChromeWorkerScope.h
+++ b/dom/workers/ChromeWorkerScope.h
@@ -4,16 +4,18 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_workers_chromeworkerscope_h__
 #define mozilla_dom_workers_chromeworkerscope_h__
 
 #include "WorkerCommon.h"
 
-BEGIN_WORKERS_NAMESPACE
+namespace mozilla {
+namespace dom {
 
 bool
 DefineChromeWorkerFunctions(JSContext* aCx, JS::Handle<JSObject*> aGlobal);
 
-END_WORKERS_NAMESPACE
+} // dom namespace
+} // mozilla namespace
 
 #endif // mozilla_dom_workers_chromeworkerscope_h__
diff --git a/dom/workers/MessageEventRunnable.cpp b/dom/workers/MessageEventRunnable.cpp
--- a/dom/workers/MessageEventRunnable.cpp
+++ b/dom/workers/MessageEventRunnable.cpp
@@ -9,17 +9,20 @@
 #include "mozilla/dom/MessageEvent.h"
 #include "mozilla/dom/MessageEventBinding.h"
 #include "mozilla/TimelineConsumers.h"
 #include "mozilla/WorkerTimelineMarker.h"
 #include "nsQueryObject.h"
 #include "WorkerPrivate.h"
 #include "WorkerScope.h"
 
-BEGIN_WORKERS_NAMESPACE
+namespace mozilla {
+namespace dom {
+
+using namespace workers;
 
 MessageEventRunnable::MessageEventRunnable(WorkerPrivate* aWorkerPrivate,
                                            TargetAndBusyBehavior aBehavior)
   : WorkerRunnable(aWorkerPrivate, aBehavior)
   , StructuredCloneHolder(CloningSupported, TransferringSupported,
                           StructuredCloneScope::SameProcessDifferentThread)
 {
 }
@@ -146,9 +149,10 @@ MessageEventRunnable::DispatchError(JSCo
   RefPtr<Event> event =
     MessageEvent::Constructor(aTarget, NS_LITERAL_STRING("messageerror"), init);
   event->SetTrusted(true);
 
   bool dummy;
   aTarget->DispatchEvent(event, &dummy);
 }
 
-END_WORKERS_NAMESPACE
+} // dom namespace
+} // mozilla namespace
diff --git a/dom/workers/MessageEventRunnable.h b/dom/workers/MessageEventRunnable.h
--- a/dom/workers/MessageEventRunnable.h
+++ b/dom/workers/MessageEventRunnable.h
@@ -11,34 +11,32 @@
 #include "WorkerRunnable.h"
 #include "mozilla/dom/StructuredCloneHolder.h"
 
 namespace mozilla {
 
 class DOMEventTargetHelper;
 
 namespace dom {
-namespace workers {
 
-class MessageEventRunnable final : public WorkerRunnable
+class MessageEventRunnable final : public workers::WorkerRunnable
                                  , public StructuredCloneHolder
 {
 public:
-  MessageEventRunnable(WorkerPrivate* aWorkerPrivate,
+  MessageEventRunnable(workers::WorkerPrivate* aWorkerPrivate,
                        TargetAndBusyBehavior aBehavior);
 
   bool
-  DispatchDOMEvent(JSContext* aCx, WorkerPrivate* aWorkerPrivate,
+  DispatchDOMEvent(JSContext* aCx, workers::WorkerPrivate* aWorkerPrivate,
                    DOMEventTargetHelper* aTarget, bool aIsMainThread);
 
 private:
   bool
-  WorkerRun(JSContext* aCx, WorkerPrivate* aWorkerPrivate) override;
+  WorkerRun(JSContext* aCx, workers::WorkerPrivate* aWorkerPrivate) override;
 
   void
   DispatchError(JSContext* aCx, DOMEventTargetHelper* aTarget);
 };
 
-} // workers namespace
 } // dom namespace
 } // mozilla namespace
 
 #endif // mozilla_dom_workers_MessageEventRunnable_h
