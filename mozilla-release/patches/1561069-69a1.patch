# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1561411592 0
# Node ID 1d5517788ccb535a9d6573e4db1c81417386af2a
# Parent  65f73e2c491b96b6bd2b4725b034487f622c0b7e
Bug 1561069 - fix mistranslation in preprocess_libffi_asm.py; r=glandium

When we converted our libffi assembly preprocessing to use
`GENERATED_FILES`, we translated `sed 's%/F[dpa][^ ]*%%g'` to use the Python
regular expression `'F[dpa][^ ]*'`.  Note that we missed the leading `/` in
the sed expression; omitting that leading slash causes no end of trouble if
you have particular expressions in your assembly file, such as
"FastFailCode".  (I'm a little surprised this hasn't bitten us yet.)

The straightfoward fix is to add the leading slash.

But wait, the rabbit hole goes deeper.  The actual bit from libffi's
`msvcc.sh` that this was trying to translate was:

```
    echo "$cl -nologo -EP $includes $defines $src > $ppsrc"
    "$cl" -nologo -EP $includes $defines $src > $ppsrc || exit $?
    output="$(echo $output | sed 's%/F[dpa][^ ]*%%g')"
    args="-nologo $safeseh $single $output $ppsrc"

    echo "$ml $args"
    eval "\"$ml\" $args"
    result=$?
```

Note that the sed expression is operating on `$output`, which is a
completely *separate* thing from the output from the result of
preprocessing.  In `msvcc.sh`, `$output` is actually some arguments that are
supposed to be passed to the assembler, per the above and the only place in
the script that sets `$output` to a non-trivial value:

```
    -o)
      outdir="$(dirname $2)"
      base="$(basename $2|sed 's/\.[^.]*//g')"
      if [ -n "$single" ]; then
        output="-Fo$2"
      else
        output="-Fe$2"
      fi
      if [ -n "$assembly" ]; then
        args="$args $output"
      else
        args="$args $output -Fd$outdir/$base -Fp$outdir/$base -Fa$outdir/$base"
      fi
```

Presumably the sed expression is attempting to remove `-Fd` and friends from
`$args` instead of `$output`, but failing badly at doing so.

In any event, the regex substitution the script is doing is unnecessary and,
with the current code, actively harmful.  Let's remove the regular
expression substitution entirely.

Differential Revision: https://phabricator.services.mozilla.com/D35705

diff --git a/config/external/ffi/preprocess_libffi_asm.py b/config/external/ffi/preprocess_libffi_asm.py
--- a/config/external/ffi/preprocess_libffi_asm.py
+++ b/config/external/ffi/preprocess_libffi_asm.py
@@ -2,26 +2,22 @@
 # vim: set filetype=python:
 # This Souce Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distibuted with this
 # file, You can obtain one at http://mozilla.og/MPL/2.0/.
 
 import buildconfig
 import mozpack.path as mozpath
 import os
-import re
 import shlex
 import subprocess
 
 
 def main(output, input_asm, ffi_h, ffi_config_h, defines, includes):
     defines = shlex.split(defines)
     includes = shlex.split(includes)
     # CPP uses -E which generates #line directives. -EP suppresses them.
     cpp = buildconfig.substs['CPP'] + ['-EP']
     input_asm = mozpath.relpath(input_asm, os.getcwd())
     args = cpp + defines + includes + [input_asm]
     print(' '.join(args))
     preprocessed = subprocess.check_output(args)
-    r = re.compile('F[dpa][^ ]*')
-    for line in preprocessed.splitlines():
-        output.write(r.sub('', line))
-        output.write('\n')
+    output.write(preprocessed)

