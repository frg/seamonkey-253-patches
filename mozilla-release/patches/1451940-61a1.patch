# HG changeset patch
# User Kris Maglione <maglione.k@gmail.com>
# Date 1523383586 25200
# Node ID 95438fe6d5a87270b1ad713da14f123b17faaea2
# Parent  a7f8801089e8362914f7ad727b241f9fb6132ced
Bug 1451940: Don't reparse stylesheets when assigning empty string to empty element. r=bz

Per spec, assinging an empty string to textContent or innerHTML should not
trigger any mutation observers, and therefore should not trigger a stylesheet
reparse, when the element is already empty.

Since we need to apply some special handling to innerHTML and textContent
assignments to <style> nodes, and therefore re-parse directly in the setter
rather than in response to mutation observers, we need to special-case this
scenario.

MozReview-Commit-ID: KdOYFs8ayT7

diff --git a/dom/html/HTMLStyleElement.cpp b/dom/html/HTMLStyleElement.cpp
--- a/dom/html/HTMLStyleElement.cpp
+++ b/dom/html/HTMLStyleElement.cpp
@@ -181,16 +181,26 @@ HTMLStyleElement::SetInnerHTML(const nsA
   SetTextContentInternal(aInnerHTML, aScriptedPrincipal, aError);
 }
 
 void
 HTMLStyleElement::SetTextContentInternal(const nsAString& aTextContent,
                                          nsIPrincipal* aScriptedPrincipal,
                                          ErrorResult& aError)
 {
+  // Per spec, if we're setting text content to an empty string and don't
+  // already have any children, we should not trigger any mutation observers, or
+  // re-parse the stylesheet.
+  if (aTextContent.IsEmpty() && !nsINode::GetFirstChild()) {
+    nsIPrincipal* principal = mTriggeringPrincipal ? mTriggeringPrincipal.get() : NodePrincipal();
+    if (principal == aScriptedPrincipal) {
+      return;
+    }
+  }
+
   SetEnableUpdates(false);
 
   aError = nsContentUtils::SetNodeTextContent(this, aTextContent, true);
 
   SetEnableUpdates(true);
 
   mTriggeringPrincipal = aScriptedPrincipal;
 
