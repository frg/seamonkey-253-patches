# HG changeset patch
# User Dmitry Butskoy <buc@buc.me>
# Date 1690629932 -7200
# Parent  5bae547ae6fc664a51bdbebf8f7731e86544c0bd
No Bug - Import new regexp V8 engine. r=frg a=frg

Iain Ireland <iireland@mozilla.com>
Bug 1629670: Change ForceByteCode to CodeKind

The current ForceByteCodeEnum is a glorified boolean. This patch replaces it with a three-value bytecode/jitcode/either enum, which will make our tiering-up logic slightly nicer in the next patch.

diff --git a/js/src/new-regexp/RegExpAPI.cpp b/js/src/new-regexp/RegExpAPI.cpp
--- a/js/src/new-regexp/RegExpAPI.cpp
+++ b/js/src/new-regexp/RegExpAPI.cpp
@@ -7,16 +7,17 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
 #include "new-regexp/RegExpAPI.h"
 
 #include "mozilla/ArrayUtils.h"
 #include "mozilla/Casting.h"
 
+#include "jit/JitCommon.h"
 #include "new-regexp/regexp-bytecode-generator.h"
 #include "new-regexp/regexp-compiler.h"
 #include "new-regexp/regexp-interpreter.h"
 #include "new-regexp/regexp-macro-assembler-arch.h"
 #include "new-regexp/regexp-parser.h"
 #include "new-regexp/regexp-shim.h"
 #include "new-regexp/regexp.h"
 
@@ -30,25 +31,27 @@ namespace js {
 namespace irregexp {
 
 using namespace mozilla;
 
 using frontend::TokenStream;
 
 using v8::internal::FlatStringReader;
 using v8::internal::HandleScope;
+using v8::internal::InputOutputData;
 using v8::internal::IrregexpInterpreter;
 using v8::internal::NativeRegExpMacroAssembler;
 using v8::internal::RegExpBytecodeGenerator;
 using v8::internal::RegExpCompileData;
 using v8::internal::RegExpCompiler;
 using v8::internal::RegExpError;
 using v8::internal::RegExpMacroAssembler;
 using v8::internal::RegExpNode;
 using v8::internal::RegExpParser;
+using v8::internal::SMRegExpMacroAssembler;
 using v8::internal::Zone;
 
 using V8HandleString = v8::internal::Handle<v8::internal::String>;
 using V8HandleRegExp = v8::internal::Handle<v8::internal::JSRegExp>;
 
 using namespace v8::internal::regexp_compiler_constants;
 
 static uint32_t ErrorNumber(RegExpError err) {
@@ -137,18 +140,17 @@ static size_t ComputeColumn(const char16
   return unicode::CountCodePoints(begin, end);
 }
 
 // This function is varargs purely so it can call ReportCompileErrorLatin1.
 // We never call it with additional arguments.
 template <typename CharT>
 static void ReportSyntaxError(TokenStream& ts,
                               RegExpCompileData& result, CharT* start,
-                              size_t length,
-                              ...) {
+                              size_t length, ...) {
   gc::AutoSuppressGC suppressGC(ts.context());
   uint32_t errorNumber = ErrorNumber(result.error);
 
   uint32_t offset = std::max(result.error_pos, 0);
   MOZ_ASSERT(offset <= length);
 
   ErrorMetadata err;
 
@@ -301,20 +303,19 @@ static void SampleCharacters(HandleLinea
                              RegExpCompiler& compiler) {
   static const int kSampleSize = 128;
   int chars_sampled = 0;
 
   FlatStringReader sample_subject(input);
   int length = sample_subject.length();
 
   int half_way = (length - kSampleSize) / 2;
-  for (int i = std::max(0, half_way);
-       i < length && chars_sampled < kSampleSize;
+  for (int i = std::max(0, half_way); i < length && chars_sampled < kSampleSize;
        i++, chars_sampled++) {
-    compiler.frequency_collator()->CountCharacter(sample_subject.Get(i));
+      compiler.frequency_collator()->CountCharacter(sample_subject.Get(i));
   }
 }
 
 static RegExpNode* WrapBody(MutableHandleRegExpShared re,
                             RegExpCompiler& compiler, RegExpCompileData& data,
                             Zone* zone, bool isLatin1) {
   using v8::internal::ChoiceNode;
   using v8::internal::EndNode;
@@ -426,21 +427,37 @@ bool CompilePattern(JSContext* cx, Mutab
   data.node = WrapBody(re, compiler, data, &zone, isLatin1);
   data.error = AnalyzeRegExp(cx->isolate, isLatin1, data.node);
   if (data.error != RegExpError::kNone) {
     MOZ_ASSERT(data.error == RegExpError::kAnalysisStackOverflow);
     JS_ReportErrorASCII(cx, "Stack overflow");
     return false;
   }
 
-  // Note: This code looks weird because in a future patch we will add
-  // support for native compilation, which will initialize `masm` with
-  // a different subclass of RegExpMacroAssembler.
+  bool useNativeCode = re->markedForTierUp(cx);
+
+  MOZ_ASSERT_IF(useNativeCode, IsNativeRegExpEnabled(cx));
+
+  Maybe<jit::JitContext> jctx;
+  Maybe<js::jit::StackMacroAssembler> stack_masm;
   UniquePtr<RegExpMacroAssembler> masm;
-  masm = MakeUnique<RegExpBytecodeGenerator>(cx->isolate, &zone);
+  if (useNativeCode) {
+    NativeRegExpMacroAssembler::Mode mode =
+        isLatin1 ? NativeRegExpMacroAssembler::LATIN1
+                 : NativeRegExpMacroAssembler::UC16;
+    // If we are compiling native code, we need a macroassembler,
+    // which needs a jit context.
+    jctx.emplace(cx, nullptr);
+    stack_masm.emplace();
+    uint32_t num_capture_registers = re->pairCount() * 2;
+    masm = MakeUnique<SMRegExpMacroAssembler>(cx, stack_masm.ref(), &zone, mode,
+                                              num_capture_registers);
+  } else {
+    masm = MakeUnique<RegExpBytecodeGenerator>(cx->isolate, &zone);
+  }
   if (!masm) {
     ReportOutOfMemory(cx);
     return false;
   }
 
   bool largePattern =
       pattern->length() > v8::internal::RegExp::kRegExpTooLargeToOptimize;
   masm->set_slow_safe(largePattern);
@@ -471,32 +488,72 @@ bool CompilePattern(JSContext* cx, Mutab
     }
     masm->set_global_mode(mode);
   }
 
   // Compile the regexp
   V8HandleString wrappedPattern(v8::internal::String(pattern), cx->isolate);
   RegExpCompiler::CompilationResult result = compiler.Assemble(
       cx->isolate, masm.get(), data.node, data.capture_count, wrappedPattern);
+  if (JS::Value(result.code).isUndefined()) {
+    // SMRegExpMacroAssembler::GetCode returns undefined on OOM.
+    MOZ_ASSERT(useNativeCode);
+    ReportOutOfMemory(cx);
+    return false;
+  }
   if (!result.Succeeded()) {
     MOZ_ASSERT(result.error == RegExpError::kTooLarge);
     JS_ReportErrorASCII(cx, "regexp too big");
     return false;
   }
 
   re->updateMaxRegisters(result.num_registers);
-  ByteArray bytecode =
-    v8::internal::ByteArray::cast(result.code).takeOwnership(cx->isolate);
-  uint32_t length = bytecode->length;
-  re->setByteCode(bytecode.release(), isLatin1);
-//   js::AddCellMemory(re, length, MemoryUse::RegExpSharedBytecode);
+  if (useNativeCode) {
+    // Transfer ownership of the tables from the macroassembler to the
+    // RegExpShared.
+    SMRegExpMacroAssembler::TableVector& tables =
+        static_cast<SMRegExpMacroAssembler*>(masm.get())->tables();
+    for (uint32_t i = 0; i < tables.length(); i++) {
+      if (!re->addTable(std::move(tables[i]))) {
+        ReportOutOfMemory(cx);
+        return false;
+      }
+    }
+    re->setJitCode(v8::internal::Code::cast(result.code).inner(), isLatin1);
+  } else {
+    // Transfer ownership of the bytecode from the HandleScope to the
+    // RegExpShared.
+    ByteArray bytecode =
+        v8::internal::ByteArray::cast(result.code).takeOwnership(cx->isolate);
+    uint32_t length = bytecode->length;
+    re->setByteCode(bytecode.release(), isLatin1);
+    // js::AddCellMemory(re, length, MemoryUse::RegExpSharedBytecode);
+  }
 
   return true;
 }
 
+template <typename CharT>
+RegExpRunStatus ExecuteRaw(jit::JitCode* code, const CharT* chars,
+                           size_t length, size_t startIndex,
+                           MatchPairs* matches) {
+  InputOutputData data(chars, chars + length, startIndex, matches);
+
+  static_assert(RegExpRunStatus_Error ==
+                v8::internal::RegExp::kInternalRegExpException);
+  static_assert(RegExpRunStatus_Success ==
+                v8::internal::RegExp::kInternalRegExpSuccess);
+  static_assert(RegExpRunStatus_Success_NotFound ==
+                v8::internal::RegExp::kInternalRegExpFailure);
+
+  typedef int (*RegExpCodeSignature)(InputOutputData*);
+  auto function = reinterpret_cast<RegExpCodeSignature>(code->raw());
+  return (RegExpRunStatus) CALL_GENERATED_1(function, &data);
+}
+
 RegExpRunStatus Interpret(JSContext* cx, MutableHandleRegExpShared re,
                           HandleLinearString input, size_t startIndex,
                           MatchPairs* matches) {
   HandleScope handleScope(cx->isolate);
   V8HandleRegExp wrappedRegExp(v8::internal::JSRegExp(re), cx->isolate);
   V8HandleString wrappedInput(v8::internal::String(input), cx->isolate);
 
   uint32_t numRegisters = re->getMaxRegisters();
@@ -512,32 +569,50 @@ RegExpRunStatus Interpret(JSContext* cx,
   static_assert(RegExpRunStatus_Error ==
                 v8::internal::RegExp::kInternalRegExpException);
   static_assert(RegExpRunStatus_Success ==
                 v8::internal::RegExp::kInternalRegExpSuccess);
   static_assert(RegExpRunStatus_Success_NotFound ==
                 v8::internal::RegExp::kInternalRegExpFailure);
 
   RegExpRunStatus status =
-      (RegExpRunStatus) IrregexpInterpreter::MatchForCallFromRuntime(
+      (RegExpRunStatus)IrregexpInterpreter::MatchForCallFromRuntime(
           cx->isolate, wrappedRegExp, wrappedInput, registers.begin(),
           numRegisters, startIndex);
 
+  MOZ_ASSERT(status == RegExpRunStatus_Error ||
+             status == RegExpRunStatus_Success ||
+             status == RegExpRunStatus_Success_NotFound);
+
   // Copy results out of registers
   if (status == RegExpRunStatus_Success) {
     uint32_t length = re->pairCount() * 2;
     MOZ_ASSERT(length <= registers.length());
     for (uint32_t i = 0; i < length; i++) {
       matches->pairsRaw()[i] = registers[i];
     }
   }
 
   return status;
 }
 
 RegExpRunStatus Execute(JSContext* cx, MutableHandleRegExpShared re,
                         HandleLinearString input, size_t startIndex,
                         MatchPairs* matches) {
+  bool latin1 = input->hasLatin1Chars();
+  jit::JitCode* jitCode = re->getJitCode(latin1);
+  bool isCompiled = !!jitCode;
+
+  if (isCompiled) {
+    JS::AutoCheckCannotGC nogc;
+    if (latin1) {
+      return ExecuteRaw(jitCode, input->latin1Chars(nogc), input->length(),
+                        startIndex, matches);
+    }
+    return ExecuteRaw(jitCode, input->twoByteChars(nogc), input->length(),
+                      startIndex, matches);
+  }
+
   return Interpret(cx, re, input, startIndex, matches);
 }
 
 }  // namespace irregexp
 }  // namespace js
diff --git a/js/src/vm/RegExpObject.cpp b/js/src/vm/RegExpObject.cpp
--- a/js/src/vm/RegExpObject.cpp
+++ b/js/src/vm/RegExpObject.cpp
@@ -562,17 +562,17 @@ RegExpObject::toString(JSContext* cx) co
 
     return sb.finishString();
 }
 
 #if defined(DEBUG) && !defined(JS_NEW_REGEXP)
 /* static */ bool
 RegExpShared::dumpBytecode(JSContext* cx, MutableHandleRegExpShared re, HandleLinearString input)
 {
-    if (!RegExpShared::compileIfNecessary(cx, re, input, ForceByteCode))
+    if (!RegExpShared::compileIfNecessary(cx, re, input, CodeKind::Bytecode))
         return false;
 
     const uint8_t* byteCode = re->compilation(input->hasLatin1Chars()).byteCode;
     const uint8_t* pc = byteCode;
 
     auto Load32Aligned = [](const uint8_t* pc) -> int32_t {
         MOZ_ASSERT((reinterpret_cast<uintptr_t>(pc) & 3) == 0);
         return *reinterpret_cast<const int32_t*>(pc);
@@ -1043,48 +1043,55 @@ RegExpShared::finalize(FreeOp* fop)
 {
     for (auto& comp : compilationArray)
         js_free(comp.byteCode);
     tables.~JitCodeTables();
 }
 
 /* static */ bool
 RegExpShared::compile(JSContext* cx, MutableHandleRegExpShared re, HandleLinearString input,
-                      ForceByteCodeEnum force)
+                      RegExpShared::CodeKind codeKind)
 {
     TraceLoggerThread* logger = TraceLoggerForCurrentThread(cx);
     AutoTraceLog logCompile(logger, TraceLogger_IrregexpCompile);
 
     RootedAtom pattern(cx, re->source);
-    return compile(cx, re, pattern, input, force);
+    return compile(cx, re, pattern, input, codeKind);
 }
 
 #ifdef JS_NEW_REGEXP
 bool
 RegExpShared::compile(JSContext* cx,
                       MutableHandleRegExpShared re,
                       HandleAtom pattern,
                       HandleLinearString input,
-                      ForceByteCodeEnum force)
+                      RegExpShared::CodeKind code)
 {
     MOZ_CRASH("TODO");
 }
 /* static */
 bool
 RegExpShared::compileIfNecessary(JSContext* cx,
                                  MutableHandleRegExpShared re,
                                  HandleLinearString input,
-                                 ForceByteCodeEnum force)
+                                 RegExpShared::CodeKind codeKind)
 {
   bool needsCompile = false;
   if (re->kind() == RegExpShared::Kind::Unparsed) {
     needsCompile = true;
   }
 
-  // TODO: tier-up from interpreter to generated code
+  if (re->kind() == RegExpShared::Kind::RegExp) {
+    if (codeKind == RegExpShared::CodeKind::Any && re->markedForTierUp(cx)) {
+      codeKind = RegExpShared::CodeKind::Jitcode;
+    }
+    if (!re->isCompiled(input->hasLatin1Chars(), codeKind)) {
+      needsCompile = true;
+    }
+  }
 
   if (needsCompile) {
     return irregexp::CompilePattern(cx, re, input);
   }
   return true;
 }
 
 /* static */
@@ -1095,17 +1102,17 @@ RegExpShared::execute(JSContext* cx,
                       size_t start,
                       MatchPairs* matches)
 {
     MOZ_ASSERT(matches);
 
     // TODO: Add tracelogger support
 
     /* Compile the code at point-of-use. */
-    if (!compileIfNecessary(cx, re, input, DontForceByteCode)) {
+    if (!compileIfNecessary(cx, re, input, RegExpShared::CodeKind::Any)) {
         return RegExpRunStatus_Error;
     }
 
     /*
      * Ensure sufficient memory for output vector.
      * No need to initialize it. The RegExp engine fills them in on a match.
      */
     if (!matches->allocOrExpandArray(re->pairCount())) {
@@ -1173,25 +1180,43 @@ void RegExpShared::useAtomMatch(HandleAt
   patternAtom_ = pattern;
   pairCount_ = 1;
 }
 
 void RegExpShared::useRegExpMatch(size_t pairCount) {
   MOZ_ASSERT(kind() == RegExpShared::Kind::Unparsed);
   kind_ = RegExpShared::Kind::RegExp;
   pairCount_ = pairCount;
+  ticks_ = 10;  // TODO: add a jit option to control this threshold
+}
+
+void RegExpShared::tierUpTick() {
+  MOZ_ASSERT(kind() == RegExpShared::Kind::RegExp);
+  if (ticks_ > 0) {
+    ticks_--;
+  }
+}
+
+bool RegExpShared::markedForTierUp(JSContext* cx) {
+  if (!IsNativeRegExpEnabled(cx)) {
+    return false;
+  }
+  if (kind() == RegExpShared::Kind::Atom) {
+    return false;
+  }
+  return ticks_ == 0;
 }
 
 #else   // !JS_NEW_REGEXP
 /* static */ bool
 RegExpShared::compile(JSContext* cx,
                       MutableHandleRegExpShared re,
                       HandleAtom pattern,
                       HandleLinearString input,
-                      ForceByteCodeEnum force)
+                      RegExpShared::CodeKind codeKind)
 {
     if (!re->ignoreCase() && !StringHasRegExpMetaChars(pattern))
         re->canStringMatch = true;
 
     CompileOptions options(cx);
     frontend::TokenStream dummyTokenStream(cx, options, nullptr, 0, nullptr);
 
     LifoAllocScope scope(&cx->tempLifoAlloc());
@@ -1205,23 +1230,24 @@ RegExpShared::compile(JSContext* cx,
                                 re->getFlags(),
                                 &data)) {
         return false;
     }
 
     // Add one to account for the whole-match capture.
     re->pairCount_ = data.capture_count + 1;
 
+    bool forceBytecode = codeKind == RegExpShared::CodeKind::Bytecode;
     JitCodeTables tables;
     irregexp::RegExpCode code = irregexp::CompilePattern(cx, re, &data, input,
                                                          false /* global() */,
                                                          re->ignoreCase(),
                                                          input->hasLatin1Chars(),
                                                          /*match_only = */ false,
-                                                         force == ForceByteCode,
+                                                         forceBytecode,
                                                          re->sticky(),
                                                          re->unicode(),
                                                          tables);
     if (code.empty())
         return false;
 
     MOZ_ASSERT(!code.jitCode || !code.byteCode);
 
@@ -1245,35 +1271,35 @@ RegExpShared::compile(JSContext* cx,
 
     return true;
 }
 
 /* static */ bool
 RegExpShared::compileIfNecessary(JSContext* cx,
                                  MutableHandleRegExpShared re,
                                  HandleLinearString input,
-                                 ForceByteCodeEnum force)
+                                 RegExpShared::CodeKind codeKind)
 {
-    if (re->isCompiled(input->hasLatin1Chars(), force))
+    if (re->isCompiled(input->hasLatin1Chars(), codeKind))
         return true;
-    return compile(cx, re, input, force);
+    return compile(cx, re, input, codeKind);
 }
 
 /* static */ RegExpRunStatus
 RegExpShared::execute(JSContext* cx,
                       MutableHandleRegExpShared re,
                       HandleLinearString input,
                       size_t start,
                       MatchPairs* matches)
 {
     MOZ_ASSERT(matches);
     TraceLoggerThread* logger = TraceLoggerForCurrentThread(cx);
 
     /* Compile the code at point-of-use. */
-    if (!compileIfNecessary(cx, re, input, DontForceByteCode))
+    if (!compileIfNecessary(cx, re, input, RegExpShared::CodeKind::Any))
         return RegExpRunStatus_Error;
 
     /*
      * Ensure sufficient memory for output vector.
      * No need to initialize it. The RegExp engine fills them in on a match.
      */
     if (!matches->allocOrExpandArray(re->pairCount())) {
         ReportOutOfMemory(cx);
@@ -1323,17 +1349,17 @@ RegExpShared::execute(JSContext* cx,
 
         MOZ_ASSERT(result == RegExpRunStatus_Success);
 
         matches->checkAgainst(length);
         return RegExpRunStatus_Success;
     } while (false);
 
     // Compile bytecode for the RegExp if necessary.
-    if (!compileIfNecessary(cx, re, input, ForceByteCode))
+    if (!compileIfNecessary(cx, re, input, RegExpShared::CodeKind::Bytecode))
         return RegExpRunStatus_Error;
 
     uint8_t* byteCode = re->compilation(input->hasLatin1Chars()).byteCode;
     AutoTraceLog logInterpreter(logger, TraceLogger_IrregexpExecute);
 
     AutoStableStringChars inputChars(cx);
     if (!inputChars.init(cx, input))
         return RegExpRunStatus_Error;
diff --git a/js/src/vm/RegExpShared.h b/js/src/vm/RegExpShared.h
--- a/js/src/vm/RegExpShared.h
+++ b/js/src/vm/RegExpShared.h
@@ -16,16 +16,17 @@
 #include "mozilla/MemoryReporting.h"
 
 #include "jsalloc.h"
 
 #include "builtin/SelfHostingDefines.h"
 #include "gc/Barrier.h"
 #include "gc/Heap.h"
 #include "gc/Marking.h"
+#include "jit/JitOptions.h"
 #include "js/RegExpFlags.h"
 #include "js/UbiNode.h"
 #include "js/Vector.h"
 #ifdef JS_NEW_REGEXP
 #  include "new-regexp/RegExpTypes.h"
 #endif
 #include "vm/ArrayObject.h"
 #include "vm/JSAtom.h"
@@ -44,16 +45,27 @@ using MutableHandleRegExpShared = JS::Mu
 
 enum RegExpRunStatus : int32_t
 {
     RegExpRunStatus_Error = -1,
     RegExpRunStatus_Success = 1,
     RegExpRunStatus_Success_NotFound = 0,
 };
 
+#ifdef JS_NEW_REGEXP
+
+inline bool IsNativeRegExpEnabled(JSContext* cx) {
+#  ifdef JS_CODEGEN_NONE
+  return false;
+#  else
+  return cx->options().nativeRegExp();
+#  endif
+}
+#endif  // JS_NEW_REGEXP
+
 /*
  * A RegExpShared is the compiled representation of a regexp. A RegExpShared is
  * potentially pointed to by multiple RegExpObjects. Additionally, C++ code may
  * have pointers to RegExpShareds on the stack. The RegExpShareds are kept in a
  * table so that they can be reused when compiling the same regex string.
  *
  * To save memory, a RegExpShared is not created for a RegExpObject until it is
  * needed for execution. When a RegExpShared needs to be created, it is looked
@@ -64,28 +76,30 @@ enum RegExpRunStatus : int32_t
  * than explicitly tracing them, so that the RegExpShared and any jitcode can
  * be reclaimed quicker. However, the RegExpShareds are traced through by
  * objects when we are preserving jitcode in their zone, to avoid the same
  * recompilation inefficiencies as normal Ion and baseline compilation.
  */
 class RegExpShared : public gc::TenuredCell
 {
   public:
-    enum ForceByteCodeEnum {
-        DontForceByteCode,
-        ForceByteCode
-    };
-
     enum class Kind
     {
         Unparsed,
         Atom,
         RegExp
     };
 
+    enum class CodeKind
+    {
+        Bytecode,
+        Jitcode,
+        Any
+    };
+
 #ifdef JS_NEW_REGEXP
     using ByteCode = js::irregexp::ByteArrayData;
     using JitCodeTable = js::irregexp::ByteArray;
 #else
     using ByteCode = uint8_t;
     using JitCodeTable = UniquePtr<uint8_t[], JS::FreePolicy>;
 #endif
     using JitCodeTables = Vector<JitCodeTable, 0, SystemAllocPolicy>;
@@ -96,30 +110,40 @@ class RegExpShared : public gc::TenuredC
 
     struct RegExpCompilation
     {
         ReadBarriered<jit::JitCode*> jitCode;
         ByteCode* byteCode;
 
         RegExpCompilation() : byteCode(nullptr) {}
 
-        bool compiled(ForceByteCodeEnum force = DontForceByteCode) const {
-            return byteCode || (force == DontForceByteCode && jitCode);
+        bool compiled(CodeKind kind = CodeKind::Any) const
+        {
+            switch (kind) {
+                case CodeKind::Bytecode:
+                    return !!byteCode;
+                case CodeKind::Jitcode:
+                    return !!jitCode;
+                case CodeKind::Any:
+                    return !!byteCode || !!jitCode;
+            }
+            MOZ_CRASH("Unreachable");
         }
     };
 
     /* Source to the RegExp, for lazy compilation. */
     GCPtr<JSAtom*>     source;
 
     JS::RegExpFlags    flags;
 
 #ifdef JS_NEW_REGEXP
     RegExpShared::Kind kind_ = Kind::Unparsed;
     GCPtrAtom patternAtom_;
     uint32_t maxRegisters_ = 0;
+    uint32_t ticks_ = 0;
 #else
     bool canStringMatch = false;
 #endif
 
     size_t             pairCount_;
 
     RegExpCompilation  compilationArray[2];
 
@@ -129,27 +153,27 @@ class RegExpShared : public gc::TenuredC
     JitCodeTables tables;
 
     /* Internal functions. */
     RegExpShared(JSAtom* source, JS::RegExpFlags flags);
 
     static bool compile(JSContext* cx,
                         MutableHandleRegExpShared res,
                         HandleLinearString input,
-                        ForceByteCodeEnum force);
+                        CodeKind code);
     static bool compile(JSContext* cx,
                         MutableHandleRegExpShared res,
                         HandleAtom pattern,
                         HandleLinearString input,
-                        ForceByteCodeEnum force);
+                        CodeKind code);
 
     static bool compileIfNecessary(JSContext* cx,
                                    MutableHandleRegExpShared res,
                                    HandleLinearString input,
-                                   ForceByteCodeEnum force);
+                                   CodeKind code);
 
     const RegExpCompilation& compilation(bool latin1) const {
         return compilationArray[CompilationIndex(latin1)];
     }
 
     RegExpCompilation& compilation(bool latin1) {
         return compilationArray[CompilationIndex(latin1)];
     }
@@ -188,22 +212,31 @@ class RegExpShared : public gc::TenuredC
   RegExpShared::Kind kind() const { return kind_; }
 
   // Use simple string matching for this regexp.
   void useAtomMatch(HandleAtom pattern);
 
   // Use the regular expression engine for this regexp.
   void useRegExpMatch(size_t parenCount);
 
+  void tierUpTick();
+  bool markedForTierUp(JSContext* cx);
+
   void setByteCode(ByteCode* code, bool latin1) {
     compilation(latin1).byteCode = code;
   }
   ByteCode* getByteCode(bool latin1) const {
     return compilation(latin1).byteCode;
   }
+  void setJitCode(jit::JitCode* code, bool latin1) {
+    compilation(latin1).jitCode = code;
+  }
+  jit::JitCode* getJitCode(bool latin1) const {
+    return compilation(latin1).jitCode;
+  }
   uint32_t getMaxRegisters() const { return maxRegisters_; }
   void updateMaxRegisters(uint32_t numRegisters) {
     maxRegisters_ = std::max(maxRegisters_, numRegisters);
   }
 
 #endif
 
     JSAtom* getSource() const           { return source; }
@@ -218,19 +251,19 @@ class RegExpShared : public gc::TenuredC
 
     bool global() const                 { return flags.global(); }
     bool ignoreCase() const             { return flags.ignoreCase(); }
     bool multiline() const              { return flags.multiline(); }
     bool dotAll() const                 { return flags.dotAll(); }
     bool unicode() const                { return flags.unicode(); }
     bool sticky() const                 { return flags.sticky(); }
 
-    bool isCompiled(bool latin1,
-                    ForceByteCodeEnum force = DontForceByteCode) const {
-        return compilation(latin1).compiled(force);
+    bool isCompiled(bool latin1, CodeKind codeKind = CodeKind::Any) const
+    {
+        return compilation(latin1).compiled(codeKind);
     }
     bool isCompiled() const { return isCompiled(true) || isCompiled(false); }
 
     void traceChildren(JSTracer* trc);
     void discardJitCode();
     void finalize(FreeOp* fop);
 
     static size_t offsetOfSource() {
