# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1540575562 25200
# Node ID f1c0fa980fd1ce976b3628fe8ea445e6f337a168
# Parent  3d4b9b77da164a7fe0433f2fea36aac9a666c538
Bug 1499933: Sort properties returned from resolvedOptions methods. r=jorendorff

diff --git a/js/src/builtin/intl/DateTimeFormat.js b/js/src/builtin/intl/DateTimeFormat.js
--- a/js/src/builtin/intl/DateTimeFormat.js
+++ b/js/src/builtin/intl/DateTimeFormat.js
@@ -873,59 +878,42 @@ function Intl_DateTimeFormat_resolvedOpt
     }
 
     resolveICUPattern(internals.pattern, result);
 
     // Step 6.
     return result;
 }
 
-// Table mapping ICU pattern characters back to the corresponding date-time
-// components of DateTimeFormat. See
-// http://unicode.org/reports/tr35/tr35-dates.html#Date_Field_Symbol_Table
-var icuPatternCharToComponent = {
-    E: "weekday",
-    G: "era",
-    y: "year",
-    M: "month",
-    L: "month",
-    d: "day",
-    h: "hour",
-    H: "hour",
-    k: "hour",
-    K: "hour",
-    m: "minute",
-    s: "second",
-    z: "timeZoneName",
-    v: "timeZoneName",
-    V: "timeZoneName"
-};
-
+/* eslint-disable complexity */
 /**
  * Maps an ICU pattern string to a corresponding set of date-time components
  * and their values, and adds properties for these components to the result
  * object, which will be returned by the resolvedOptions method. For the
  * interpretation of ICU pattern characters, see
  * http://unicode.org/reports/tr35/tr35-dates.html#Date_Field_Symbol_Table
  */
 function resolveICUPattern(pattern, result) {
     assert(IsObject(result), "resolveICUPattern");
+
+    var hourCycle, weekday, era, year, month, day, hour, minute, second, timeZoneName;
     var i = 0;
     while (i < pattern.length) {
         var c = pattern[i++];
         if (c === "'") {
             while (i < pattern.length && pattern[i] !== "'")
                 i++;
             i++;
         } else {
             var count = 1;
             while (i < pattern.length && pattern[i] === c) {
                 i++;
                 count++;
             }
+
             var value;
             switch (c) {
             // "text" cases
             case "G":
             case "E":
             case "z":
             case "v":
             case "V":
@@ -962,31 +950,93 @@ function resolveICUPattern(pattern, resu
                 else if (count === 4)
                     value = "long";
                 else
                     value = "narrow";
                 break;
             default:
                 // skip other pattern characters and literal text
             }
-            if (hasOwn(c, icuPatternCharToComponent))
-                _DefineDataProperty(result, icuPatternCharToComponent[c], value);
+
+            // Map ICU pattern characters back to the corresponding date-time
+            // components of DateTimeFormat. See
+            // http://unicode.org/reports/tr35/tr35-dates.html#Date_Field_Symbol_Table
             switch (c) {
+            case "E":
+                weekday = value;
+                break;
+            case "G":
+                era = value;
+                break;
+            case "y":
+                year = value;
+                break;
+            case "M":
+            case "L":
+                month = value;
+                break;
+            case "d":
+                day = value;
+                break;
             case "h":
-                _DefineDataProperty(result, "hourCycle", "h12");
-                _DefineDataProperty(result, "hour12", true);
+                hourCycle = "h12";
+                hour = value;
+                break;
+            case "H":
+                hourCycle = "h23";
+                hour = value;
+                break;
+            case "k":
+                hourCycle = "h24";
+                hour = value;
                 break;
             case "K":
-                _DefineDataProperty(result, "hourCycle", "h11");
-                _DefineDataProperty(result, "hour12", true);
+                hourCycle = "h11";
+                hour = value;
+                break;
+            case "m":
+                minute = value;
                 break;
-            case "H":
-                _DefineDataProperty(result, "hourCycle", "h23");
-                _DefineDataProperty(result, "hour12", false);
+            case "s":
+                second = value;
                 break;
-            case "k":
-                _DefineDataProperty(result, "hourCycle", "h24");
-                _DefineDataProperty(result, "hour12", false);
+            case "z":
+            case "v":
+            case "V":
+                timeZoneName = value;
                 break;
             }
         }
     }
+
+    if (hourCycle) {
+        _DefineDataProperty(result, "hourCycle", hourCycle);
+        _DefineDataProperty(result, "hour12", hourCycle === "h11" || hourCycle === "h12");
+    }
+    if (weekday) {
+        _DefineDataProperty(result, "weekday", weekday);
+    }
+    if (era) {
+        _DefineDataProperty(result, "era", era);
+    }
+    if (year) {
+        _DefineDataProperty(result, "year", year);
+    }
+    if (month) {
+        _DefineDataProperty(result, "month", month);
+    }
+    if (day) {
+        _DefineDataProperty(result, "day", day);
+    }
+    if (hour) {
+        _DefineDataProperty(result, "hour", hour);
+    }
+    if (minute) {
+        _DefineDataProperty(result, "minute", minute);
+    }
+    if (second) {
+        _DefineDataProperty(result, "second", second);
+    }
+    if (timeZoneName) {
+        _DefineDataProperty(result, "timeZoneName", timeZoneName);
+    }
 }
+/* eslint-enable complexity */
diff --git a/js/src/builtin/intl/NumberFormat.js b/js/src/builtin/intl/NumberFormat.js
--- a/js/src/builtin/intl/NumberFormat.js
+++ b/js/src/builtin/intl/NumberFormat.js
@@ -475,40 +475,42 @@ function Intl_NumberFormat_resolvedOptio
 
     var internals = getNumberFormatInternals(nf);
 
     // Steps 4-5.
     var result = {
         locale: internals.locale,
         numberingSystem: internals.numberingSystem,
         style: internals.style,
-        minimumIntegerDigits: internals.minimumIntegerDigits,
-        minimumFractionDigits: internals.minimumFractionDigits,
-        maximumFractionDigits: internals.maximumFractionDigits,
-        useGrouping: internals.useGrouping
     };
 
     // currency and currencyDisplay are only present for currency formatters.
     assert(hasOwn("currency", internals) === (internals.style === "currency"),
            "currency is present iff style is 'currency'");
     assert(hasOwn("currencyDisplay", internals) === (internals.style === "currency"),
            "currencyDisplay is present iff style is 'currency'");
 
     if (hasOwn("currency", internals)) {
         _DefineDataProperty(result, "currency", internals.currency);
         _DefineDataProperty(result, "currencyDisplay", internals.currencyDisplay);
     }
 
+    _DefineDataProperty(result, "minimumIntegerDigits", internals.minimumIntegerDigits);
+    _DefineDataProperty(result, "minimumFractionDigits", internals.minimumFractionDigits);
+    _DefineDataProperty(result, "maximumFractionDigits", internals.maximumFractionDigits);
+
     // Min/Max significant digits are either both present or not at all.
     assert(hasOwn("minimumSignificantDigits", internals) ===
            hasOwn("maximumSignificantDigits", internals),
            "minimumSignificantDigits is present iff maximumSignificantDigits is present");
 
     if (hasOwn("minimumSignificantDigits", internals)) {
         _DefineDataProperty(result, "minimumSignificantDigits",
                             internals.minimumSignificantDigits);
         _DefineDataProperty(result, "maximumSignificantDigits",
                             internals.maximumSignificantDigits);
     }
 
+    _DefineDataProperty(result, "useGrouping", internals.useGrouping);
+
     // Step 6.
     return result;
 }
diff --git a/js/src/builtin/intl/PluralRules.js b/js/src/builtin/intl/PluralRules.js
--- a/js/src/builtin/intl/PluralRules.js
+++ b/js/src/builtin/intl/PluralRules.js
@@ -221,34 +221,20 @@ function Intl_PluralRules_resolvedOption
     // Steps 2-3.
     if (!IsObject(pluralRules) || (pluralRules = GuardToPluralRules(pluralRules)) === null) {
         ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "PluralRules", "resolvedOptions",
                        "PluralRules");
     }
 
     var internals = getPluralRulesInternals(pluralRules);
 
-    var internalsPluralCategories = internals.pluralCategories;
-    if (internalsPluralCategories === null) {
-        internalsPluralCategories = intl_GetPluralCategories(pluralRules);
-        internals.pluralCategories = internalsPluralCategories;
-    }
-
-    // TODO: The current spec actually requires to return the internal array
-    // object and not a copy of it.
-    // <https://github.com/tc39/proposal-intl-plural-rules/issues/28#issuecomment-341557030>
-    var pluralCategories = [];
-    for (var i = 0; i < internalsPluralCategories.length; i++)
-        _DefineDataProperty(pluralCategories, i, internalsPluralCategories[i]);
-
     // Steps 4-5.
     var result = {
         locale: internals.locale,
         type: internals.type,
-        pluralCategories,
         minimumIntegerDigits: internals.minimumIntegerDigits,
         minimumFractionDigits: internals.minimumFractionDigits,
         maximumFractionDigits: internals.maximumFractionDigits,
     };
 
     // Min/Max significant digits are either both present or not at all.
     assert(hasOwn("minimumSignificantDigits", internals) ===
            hasOwn("maximumSignificantDigits", internals),
@@ -257,10 +243,24 @@ function Intl_PluralRules_resolvedOption
     if (hasOwn("minimumSignificantDigits", internals)) {
         _DefineDataProperty(result, "minimumSignificantDigits",
                             internals.minimumSignificantDigits);
         _DefineDataProperty(result, "maximumSignificantDigits",
                             internals.maximumSignificantDigits);
     }
 
     // Step 6.
+    var internalsPluralCategories = internals.pluralCategories;
+    if (internalsPluralCategories === null) {
+        internalsPluralCategories = intl_GetPluralCategories(pluralRules);
+        internals.pluralCategories = internalsPluralCategories;
+    }
+
+    var pluralCategories = [];
+    for (var i = 0; i < internalsPluralCategories.length; i++)
+        _DefineDataProperty(pluralCategories, i, internalsPluralCategories[i]);
+
+    // Step 7.
+    _DefineDataProperty(result, "pluralCategories", pluralCategories);
+
+    // Step 8.
     return result;
 }
diff --git a/js/src/builtin/intl/RelativeTimeFormat.js b/js/src/builtin/intl/RelativeTimeFormat.js
--- a/js/src/builtin/intl/RelativeTimeFormat.js
+++ b/js/src/builtin/intl/RelativeTimeFormat.js
@@ -214,17 +214,17 @@ function Intl_RelativeTimeFormat_format(
         ThrowRangeError(JSMSG_INVALID_OPTION_VALUE, "unit", u);
     }
 
     // Step 5.
     return intl_FormatRelativeTime(relativeTimeFormat, t, u, internals.numeric);
 }
 
 /**
- * Returns the resolved options for a PluralRules object.
+ * Returns the resolved options for a RelativeTimeFormat object.
  *
  * Spec: ECMAScript 402 API, RelativeTimeFormat, 1.4.4.
  */
 function Intl_RelativeTimeFormat_resolvedOptions() {
     var relativeTimeFormat;
     // Check "this RelativeTimeFormat object" per introduction of section 1.4.
     if (!IsObject(this) || (relativeTimeFormat = GuardToRelativeTimeFormat(this)) === null) {
         ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "RelativeTimeFormat", "resolvedOptions",
diff --git a/js/src/tests/jstests.list.1499933.later b/js/src/tests/jstests.list.1499933.later
new file mode 100644
--- /dev/null
+++ b/js/src/tests/jstests.list.1499933.later
@@ -0,0 +1,59 @@
+--- jstests.list
++++ jstests.list
+@@ -5,16 +5,26 @@
+ skip include test/jstests.list
+ 
+ skip script non262/String/normalize-generateddata-input.js # input data for other test
+ 
+ # Timeouts on arm and cgc builds.
+ slow script test262/built-ins/decodeURI/S15.1.3.1_A2.5_T1.js
+ slow script test262/built-ins/decodeURIComponent/S15.1.3.2_A2.5_T1.js
+ 
++# Fields are not fully implemented yet
++# https://bugzilla.mozilla.org/show_bug.cgi?id=1499448
++skip script non262/fields/access.js
++skip script non262/fields/basic.js
++skip script non262/fields/error.js
++skip script non262/fields/field_types.js
++skip script non262/fields/literal.js
++skip script non262/fields/mixed_methods.js
++skip script non262/fields/quirks.js
++
+ 
+ #################################################################
+ # Tests disabled due to intentional alternative implementations #
+ #################################################################
+ 
+ # Legacy "caller" and "arguments" implemented as accessor properties on Function.prototype.
+ skip script test262/built-ins/Function/prototype/restricted-property-arguments.js
+ skip script test262/built-ins/Function/prototype/restricted-property-caller.js
+@@ -438,29 +448,16 @@ skip script test262/intl402/RelativeTime
+ skip script test262/intl402/RelativeTimeFormat/prototype/format/en-us-style-short.js
+ skip script test262/intl402/RelativeTimeFormat/prototype/format/pl-pl-style-long.js
+ skip script test262/intl402/RelativeTimeFormat/prototype/format/pl-pl-style-narrow.js
+ skip script test262/intl402/RelativeTimeFormat/prototype/format/pl-pl-style-short.js
+ 
+ # https://bugzilla.mozilla.org/show_bug.cgi?id=1473588
+ skip script test262/intl402/RelativeTimeFormat/prototype/format/unit-plural.js
+ 
+-# https://bugzilla.mozilla.org/show_bug.cgi?id=1499933
+-skip script test262/intl402/DateTimeFormat/prototype/resolvedOptions/order.js
+-skip script test262/intl402/PluralRules/prototype/resolvedOptions/order.js
+-skip script test262/intl402/NumberFormat/prototype/resolvedOptions/order.js
+-
+-# Fields are not fully implemented yet
+-skip script non262/fields/access.js
+-skip script non262/fields/basic.js
+-skip script non262/fields/error.js
+-skip script non262/fields/field_types.js
+-skip script non262/fields/literal.js
+-skip script non262/fields/mixed_methods.js
+-skip script non262/fields/quirks.js
+ 
+ ###########################################################
+ # Tests disabled due to issues in test262 importer script #
+ ###########################################################
+ 
+ # test262 importer merges all includes in a per directory shell.js file, breaking this harness test case.
+ skip script test262/harness/detachArrayBuffer.js
+ 
