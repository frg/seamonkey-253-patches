# HG changeset patch
# User Anthony Ramine <n.oxyde@gmail.com>
# Date 1520438769 18000
# Node ID fef8761290c4e235f3d95b33f10bafa072fe4a45
# Parent  933d16106d13e8bafaf3c98229e9a5cda31a3960
servo: Merge #20230 - Introduce #[css(if_empty = "…", iterable)] (from servo:derive-all-the-things); r=emilio

Source-Repo: https://github.com/servo/servo
Source-Revision: 2f4c13d27d4acf5a5a356c9168feb7203ecf2d14

diff --git a/servo/components/style/properties/helpers.mako.rs b/servo/components/style/properties/helpers.mako.rs
--- a/servo/components/style/properties/helpers.mako.rs
+++ b/servo/components/style/properties/helpers.mako.rs
@@ -78,20 +78,16 @@
     `initial_value` need not be defined for these.
 </%doc>
 <%def name="vector_longhand(name, animation_value_type=None, allow_empty=False, separator='Comma',
                             need_animatable=False, **kwargs)">
     <%call expr="longhand(name, animation_value_type=animation_value_type, vector=True,
                           need_animatable=need_animatable, **kwargs)">
         #[allow(unused_imports)]
         use smallvec::SmallVec;
-        % if allow_empty:
-        use std::fmt::{self, Write};
-        use style_traits::{CssWriter, Separator, ToCss};
-        % endif
 
         pub mod single_value {
             #[allow(unused_imports)]
             use cssparser::{Parser, BasicParseError};
             #[allow(unused_imports)]
             use parser::{Parse, ParserContext};
             #[allow(unused_imports)]
             use properties::ShorthandId;
@@ -115,33 +111,32 @@
             % if allow_empty and allow_empty != "NotInitial":
             use std::vec::IntoIter;
             % else:
             use smallvec::{IntoIter, SmallVec};
             % endif
             use values::computed::ComputedVecIter;
 
             /// The computed value, effectively a list of single values.
-            #[derive(Clone, Debug, MallocSizeOf, PartialEq)]
-            % if need_animatable or animation_value_type == "ComputedValue":
-            #[derive(Animate, ComputeSquaredDistance)]
-            % endif
-            % if not allow_empty:
             % if separator == "Comma":
             #[css(comma)]
             % endif
-            #[derive(ToCss)]
+            #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
+            % if need_animatable or animation_value_type == "ComputedValue":
+            #[derive(Animate, ComputeSquaredDistance)]
             % endif
             pub struct T(
+                % if not allow_empty:
+                #[css(iterable)]
+                % else:
+                #[css(if_empty = "none", iterable)]
+                % endif
                 % if allow_empty and allow_empty != "NotInitial":
                 pub Vec<single_value::T>,
                 % else:
-                % if not allow_empty:
-                #[css(iterable)]
-                % endif
                 pub SmallVec<[single_value::T; 1]>,
                 % endif
             );
 
             % if need_animatable or animation_value_type == "ComputedValue":
                 use values::animated::{ToAnimatedZero};
 
                 impl ToAnimatedZero for T {
@@ -160,73 +155,30 @@
                 type IntoIter = IntoIter<[single_value::T; 1]>;
                 % endif
                 fn into_iter(self) -> Self::IntoIter {
                     self.0.into_iter()
                 }
             }
         }
 
-        % if allow_empty:
-        impl ToCss for computed_value::T {
-            fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-            where
-                W: Write,
-            {
-                let mut iter = self.0.iter();
-                if let Some(val) = iter.next() {
-                    val.to_css(dest)?;
-                } else {
-                    return dest.write_str("none");
-                }
-                for i in iter {
-                    dest.write_str(::style_traits::${separator}::separator())?;
-                    i.to_css(dest)?;
-                }
-                Ok(())
-            }
-        }
-        % endif
-
         /// The specified value of ${name}.
-        #[derive(Clone, Debug, MallocSizeOf, PartialEq)]
-        % if not allow_empty:
         % if separator == "Comma":
         #[css(comma)]
         % endif
-        #[derive(ToCss)]
-        % endif
+        #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
         pub struct SpecifiedValue(
             % if not allow_empty:
             #[css(iterable)]
+            % else:
+            #[css(if_empty = "none", iterable)]
             % endif
             pub Vec<single_value::SpecifiedValue>,
         );
 
-        % if allow_empty:
-        impl ToCss for SpecifiedValue {
-            fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-            where
-                W: Write,
-            {
-                let mut iter = self.0.iter();
-                if let Some(val) = iter.next() {
-                    val.to_css(dest)?;
-                } else {
-                    return dest.write_str("none");
-                }
-                for i in iter {
-                    dest.write_str(::style_traits::${separator}::separator())?;
-                    i.to_css(dest)?;
-                }
-                Ok(())
-            }
-        }
-        % endif
-
         pub fn get_initial_value() -> computed_value::T {
             % if allow_empty and allow_empty != "NotInitial":
                 computed_value::T(vec![])
             % else:
                 let mut v = SmallVec::new();
                 v.push(single_value::get_initial_value());
                 computed_value::T(v)
             % endif
diff --git a/servo/components/style/values/specified/font.rs b/servo/components/style/values/specified/font.rs
--- a/servo/components/style/values/specified/font.rs
+++ b/servo/components/style/values/specified/font.rs
@@ -733,19 +733,22 @@ pub enum VariantAlternates {
     Ornaments(CustomIdent),
     /// Enables display of alternate annotation forms
     #[css(function)]
     Annotation(CustomIdent),
     /// Enables display of historical forms
     HistoricalForms,
 }
 
-#[derive(Clone, Debug, MallocSizeOf, PartialEq)]
+#[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
 /// List of Variant Alternates
-pub struct VariantAlternatesList(pub Box<[VariantAlternates]>);
+pub struct VariantAlternatesList(
+    #[css(if_empty = "normal", iterable)]
+    pub Box<[VariantAlternates]>,
+);
 
 impl VariantAlternatesList {
     /// Returns the length of all variant alternates.
     pub fn len(&self) -> usize {
         self.0.iter().fold(0, |acc, alternate| {
             match *alternate {
                 VariantAlternates::Swash(_) | VariantAlternates::Stylistic(_) |
                 VariantAlternates::Ornaments(_) | VariantAlternates::Annotation(_) => {
@@ -756,35 +759,16 @@ impl VariantAlternatesList {
                     acc + slice.len()
                 },
                 _ => acc,
             }
         })
     }
 }
 
-impl ToCss for VariantAlternatesList {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        if self.0.is_empty() {
-            return dest.write_str("normal");
-        }
-
-        let mut iter = self.0.iter();
-        iter.next().unwrap().to_css(dest)?;
-        for alternate in iter {
-            dest.write_str(" ")?;
-            alternate.to_css(dest)?;
-        }
-        Ok(())
-    }
-}
-
 #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
 /// Control over the selection of these alternate glyphs
 pub enum FontVariantAlternates {
     /// Use alternative glyph from value
     Value(VariantAlternatesList),
     /// Use system font glyph
     System(SystemFont)
 }
diff --git a/servo/components/style/values/specified/position.rs b/servo/components/style/values/specified/position.rs
--- a/servo/components/style/values/specified/position.rs
+++ b/servo/components/style/values/specified/position.rs
@@ -504,24 +504,27 @@ impl From<GridAutoFlow> for u8 {
         if v.dense {
             result |= structs::NS_STYLE_GRID_AUTO_FLOW_DENSE as u8;
         }
         result
     }
 }
 
 #[cfg_attr(feature = "gecko", derive(MallocSizeOf))]
-#[derive(Clone, Debug, PartialEq)]
+#[derive(Clone, Debug, PartialEq, ToCss)]
 /// https://drafts.csswg.org/css-grid/#named-grid-area
 pub struct TemplateAreas {
     /// `named area` containing for each template area
+    #[css(skip)]
     pub areas: Box<[NamedArea]>,
     /// The original CSS string value of each template area
+    #[css(iterable)]
     pub strings: Box<[Box<str>]>,
     /// The number of columns of the grid.
+    #[css(skip)]
     pub width: u32,
 }
 
 impl TemplateAreas {
     /// Transform `vector` of str into `template area`
     pub fn from_vec(strings: Vec<Box<str>>) -> Result<TemplateAreas, ()> {
         if strings.is_empty() {
             return Err(());
@@ -591,31 +594,16 @@ impl TemplateAreas {
         Ok(TemplateAreas {
             areas: areas.into_boxed_slice(),
             strings: strings.into_boxed_slice(),
             width: width,
         })
     }
 }
 
-impl ToCss for TemplateAreas {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        for (i, string) in self.strings.iter().enumerate() {
-            if i != 0 {
-                dest.write_str(" ")?;
-            }
-            string.to_css(dest)?;
-        }
-        Ok(())
-    }
-}
-
 impl Parse for TemplateAreas {
     fn parse<'i, 't>(
         _context: &ParserContext,
         input: &mut Parser<'i, 't>,
     ) -> Result<Self, ParseError<'i>> {
         let mut strings = vec![];
         while let Ok(string) = input.try(|i| i.expect_string().map(|s| s.as_ref().into())) {
             strings.push(string);
diff --git a/servo/components/style_derive/to_css.rs b/servo/components/style_derive/to_css.rs
--- a/servo/components/style_derive/to_css.rs
+++ b/servo/components/style_derive/to_css.rs
@@ -145,16 +145,30 @@ fn derive_variant_fields_expr(
 }
 
 fn derive_single_field_expr(
     field: &BindingInfo,
     attrs: CssFieldAttrs,
     where_clause: &mut WhereClause,
 ) -> Tokens {
     if attrs.iterable {
+        if let Some(if_empty) = attrs.if_empty {
+            return quote! {
+                {
+                    let mut iter = #field.iter().peekable();
+                    if iter.peek().is_none() {
+                        writer.item(&::style_traits::values::Verbatim(#if_empty))?;
+                    } else {
+                        for item in iter {
+                            writer.item(&item)?;
+                        }
+                    }
+                }
+            };
+        }
         quote! {
             for item in #field.iter() {
                 writer.item(&item)?;
             }
         }
     } else {
         if !attrs.ignore_bound {
             where_clause.add_trait_bound(&field.ast().ty);
@@ -181,12 +195,13 @@ pub struct CssVariantAttrs {
     pub dimension: bool,
     pub keyword: Option<String>,
     pub aliases: Option<String>,
 }
 
 #[darling(attributes(css), default)]
 #[derive(Default, FromField)]
 struct CssFieldAttrs {
+    if_empty: Option<String>,
     ignore_bound: bool,
     iterable: bool,
     skip: bool,
 }
diff --git a/servo/components/style_traits/values.rs b/servo/components/style_traits/values.rs
--- a/servo/components/style_traits/values.rs
+++ b/servo/components/style_traits/values.rs
@@ -22,16 +22,18 @@ use std::fmt::{self, Write};
 ///   with a "-";
 /// * if `#[css(comma)]` is found on a variant, its fields are separated by
 ///   commas, otherwise, by spaces;
 /// * if `#[css(function)]` is found on a variant, the variant name gets
 ///   serialised like unit variants and its fields are surrounded by parentheses;
 /// * if `#[css(iterable)]` is found on a function variant, that variant needs
 ///   to have a single member, and that member needs to be iterable. The
 ///   iterable will be serialized as the arguments for the function;
+/// * an iterable field can also be annotated with `#[css(if_empty = "foo")]`
+///   to print `"foo"` if the iterator is empty;
 /// * if `#[css(dimension)]` is found on a variant, that variant needs
 ///   to have a single member. The variant would be serialized as a CSS
 ///   dimension token, like: <member><identifier>;
 /// * if `#[css(skip)]` is found on a field, the `ToCss` call for that field
 ///   is skipped;
 /// * finally, one can put `#[css(derive_debug)]` on the whole type, to
 ///   implement `Debug` by a single call to `ToCss::to_css`.
 pub trait ToCss {
@@ -205,16 +207,31 @@ where
                 // either.
                 debug_assert_eq!(old, new);
             }
         }
         Ok(())
     }
 }
 
+/// A wrapper type that implements `ToCss` by printing its inner field.
+pub struct Verbatim<'a, T>(pub &'a T)
+where
+    T: ?Sized + 'a;
+
+impl<'a, T> ToCss for Verbatim<'a, T>
+where
+    T: AsRef<str> + ?Sized + 'a,
+{
+    #[inline]
+    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result where W: Write {
+        dest.write_str(self.0.as_ref())
+    }
+}
+
 /// Type used as the associated type in the `OneOrMoreSeparated` trait on a
 /// type to indicate that a serialized list of elements of this type is
 /// separated by commas.
 pub struct Comma;
 
 /// Type used as the associated type in the `OneOrMoreSeparated` trait on a
 /// type to indicate that a serialized list of elements of this type is
 /// separated by spaces.
