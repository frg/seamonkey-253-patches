# HG changeset patch
# User Hiroyuki Ikezoe <hikezoe@mozilla.com>
# Date 1520546984 -32400
# Node ID f01d2ea74eb5438ec54035fd99e4c8239a540345
# Parent  59cec4f97aab3a140a2661557b2aaebec3128a09
Bug 1444221 - #ifdef out some animation related stuff for MOZ_OLD_STYLE. r=xidorn

MozReview-Commit-ID: 9VC4qUbO6uK

diff --git a/dom/animation/EffectCompositor.cpp b/dom/animation/EffectCompositor.cpp
--- a/dom/animation/EffectCompositor.cpp
+++ b/dom/animation/EffectCompositor.cpp
@@ -557,31 +557,31 @@ EffectCompositor::HasPendingStyleUpdates
     if (elementSet.Count()) {
       return true;
     }
   }
 
   return false;
 }
 
+#ifdef MOZ_OLD_STYLE
 bool
 EffectCompositor::HasThrottledStyleUpdates() const
 {
   for (auto& elementSet : mElementsToRestyle) {
     for (auto iter = elementSet.ConstIter(); !iter.Done(); iter.Next()) {
       if (!iter.Data()) {
         return true;
       }
     }
   }
 
   return false;
 }
 
-#ifdef MOZ_OLD_STYLE
 void
 EffectCompositor::AddStyleUpdatesTo(RestyleTracker& aTracker)
 {
   if (!mPresContext) {
     return;
   }
 
   for (size_t i = 0; i < kCascadeLevelCount; i++) {
diff --git a/dom/animation/EffectCompositor.h b/dom/animation/EffectCompositor.h
--- a/dom/animation/EffectCompositor.h
+++ b/dom/animation/EffectCompositor.h
@@ -168,19 +168,20 @@ public:
   // some thread-safe issues.
   bool GetServoAnimationRule(
     const dom::Element* aElement,
     CSSPseudoElementType aPseudoType,
     CascadeLevel aCascadeLevel,
     RawServoAnimationValueMapBorrowedMut aAnimationValues);
 
   bool HasPendingStyleUpdates() const;
+
+#ifdef MOZ_OLD_STYLE
   bool HasThrottledStyleUpdates() const;
 
-#ifdef MOZ_OLD_STYLE
   // Tell the restyle tracker about all the animated styles that have
   // pending updates so that it can update the animation rule for these
   // elements.
   void AddStyleUpdatesTo(RestyleTracker& aTracker);
 
   nsIStyleRuleProcessor* RuleProcessor(CascadeLevel aCascadeLevel) const
   {
     return mRuleProcessors[aCascadeLevel];
diff --git a/layout/style/AnimationCollection.cpp b/layout/style/AnimationCollection.cpp
--- a/layout/style/AnimationCollection.cpp
+++ b/layout/style/AnimationCollection.cpp
@@ -111,23 +111,25 @@ AnimationCollection<AnimationType>::GetO
 
     *aCreatedCollection = true;
     aElement->SetMayHaveAnimations();
   }
 
   return collection;
 }
 
+#ifdef MOZ_OLD_STYLE
 template <class AnimationType>
 void
 AnimationCollection<AnimationType>::UpdateCheckGeneration(
   nsPresContext* aPresContext)
 {
   mCheckGeneration = aPresContext->RestyleManager()->GetAnimationGeneration();
 }
+#endif
 
 template<class AnimationType>
 /*static*/ nsIAtom*
 AnimationCollection<AnimationType>::GetPropertyAtomForPseudoType(
   CSSPseudoElementType aPseudoType)
 {
   nsIAtom* propName = nullptr;
 
diff --git a/layout/style/AnimationCollection.h b/layout/style/AnimationCollection.h
--- a/layout/style/AnimationCollection.h
+++ b/layout/style/AnimationCollection.h
@@ -33,17 +33,19 @@ class AnimationCollection
   : public LinkedListElement<AnimationCollection<AnimationType>>
 {
   typedef AnimationCollection<AnimationType> SelfType;
   typedef AnimationTypeTraits<AnimationType> TraitsType;
 
   AnimationCollection(dom::Element* aElement, nsIAtom* aElementProperty)
     : mElement(aElement)
     , mElementProperty(aElementProperty)
+#ifdef MOZ_OLD_STYLE
     , mCheckGeneration(0)
+#endif
 #ifdef DEBUG
     , mCalledPropertyDtor(false)
 #endif
   {
     MOZ_COUNT_CTOR(AnimationCollection);
   }
 
 public:
@@ -91,25 +93,27 @@ public:
   dom::Element *mElement;
 
   // the atom we use in mElement's prop table (must be a static atom,
   // i.e., in an atom list)
   nsIAtom *mElementProperty;
 
   InfallibleTArray<RefPtr<AnimationType>> mAnimations;
 
+#ifdef MOZ_OLD_STYLE
   // For CSS transitions only, we record the most recent generation
   // for which we've done the transition update, so that we avoid doing
   // it more than once per style change.
   // (Note that we also store an animation generation on each EffectSet in
   // order to track when we need to update animations on layers.)
   uint64_t mCheckGeneration;
 
   // Update mCheckGeneration to RestyleManager's count
   void UpdateCheckGeneration(nsPresContext* aPresContext);
+#endif
 
 private:
   static nsIAtom* GetPropertyAtomForPseudoType(
     CSSPseudoElementType aPseudoType);
 
 #ifdef DEBUG
   bool mCalledPropertyDtor;
 #endif
diff --git a/layout/style/nsTransitionManager.h b/layout/style/nsTransitionManager.h
--- a/layout/style/nsTransitionManager.h
+++ b/layout/style/nsTransitionManager.h
@@ -302,17 +302,19 @@ struct AnimationTypeTraits<dom::CSSTrans
 } // namespace mozilla
 
 class nsTransitionManager final
   : public mozilla::CommonAnimationManager<mozilla::dom::CSSTransition>
 {
 public:
   explicit nsTransitionManager(nsPresContext *aPresContext)
     : mozilla::CommonAnimationManager<mozilla::dom::CSSTransition>(aPresContext)
+#ifdef MOZ_OLD_STYLE
     , mInAnimationOnlyStyleUpdate(false)
+#endif
   {
   }
 
   NS_INLINE_DECL_REFCOUNTING(nsTransitionManager)
 
   typedef mozilla::AnimationCollection<mozilla::dom::CSSTransition>
     CSSTransitionCollection;
 
@@ -352,25 +354,25 @@ public:
    * say, it was display:none for a while, but previously displayed.
    *
    * This method removes any finished transitions that don't match the
    * new style.
    */
   void PruneCompletedTransitions(mozilla::dom::Element* aElement,
                                  mozilla::CSSPseudoElementType aPseudoType,
                                  mozilla::GeckoStyleContext* aNewStyleContext);
-#endif
 
   void SetInAnimationOnlyStyleUpdate(bool aInAnimationOnlyUpdate) {
     mInAnimationOnlyStyleUpdate = aInAnimationOnlyUpdate;
   }
 
   bool InAnimationOnlyStyleUpdate() const {
     return mInAnimationOnlyStyleUpdate;
   }
+#endif
 
 protected:
   virtual ~nsTransitionManager() {}
 
   typedef nsTArray<RefPtr<mozilla::dom::CSSTransition>>
     OwningCSSTransitionPtrArray;
 
   // Update transitions. This will start new transitions,
@@ -393,12 +395,14 @@ protected:
                                mozilla::dom::Element* aElement,
                                mozilla::CSSPseudoElementType aPseudoType,
                                CSSTransitionCollection*& aElementTransitions,
                                StyleType aOldStyle,
                                StyleType aNewStyle,
                                bool* aStartedAny,
                                nsCSSPropertyIDSet* aWhichStarted);
 
+#ifdef MOZ_OLD_STYLE
   bool mInAnimationOnlyStyleUpdate;
+#endif
 };
 
 #endif /* !defined(nsTransitionManager_h_) */
