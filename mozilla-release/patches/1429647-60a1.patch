# HG changeset patch
# User Tom Ritter <tom@mozilla.com>
# Date 1516838983 21600
# Node ID 43679b41bece563cc07b032b325b274bcbe2b229
# Parent  c41d517fa928b52c3827c47aaa559165324e65ed
Bug 1429647 Switch privacy.reduceTimerPrecision and privacy.resistFingerprinting to use Relaxed Semantics r=froydnj,mystor

MozReview-Commit-ID: HdW4tmQAcTT

diff --git a/js/src/jsdate.cpp b/js/src/jsdate.cpp
--- a/js/src/jsdate.cpp
+++ b/js/src/jsdate.cpp
@@ -49,26 +49,26 @@
 
 using namespace js;
 
 using mozilla::Atomic;
 using mozilla::ArrayLength;
 using mozilla::IsFinite;
 using mozilla::IsNaN;
 using mozilla::NumbersAreIdentical;
-using mozilla::ReleaseAcquire;
+using mozilla::Relaxed;
 
 using JS::AutoCheckCannotGC;
 using JS::ClippedTime;
 using JS::GenericNaN;
 using JS::TimeClip;
 using JS::ToInteger;
 
 // When this value is non-zero, we'll round the time by this resolution.
-static Atomic<uint32_t, ReleaseAcquire> sResolutionUsec;
+static Atomic<uint32_t, Relaxed> sResolutionUsec;
 
 /*
  * The JS 'Date' object is patterned after the Java 'Date' object.
  * Here is a script:
  *
  *    today = new Date();
  *
  *    print(today.toLocaleString());
diff --git a/toolkit/components/resistfingerprinting/nsRFPService.cpp b/toolkit/components/resistfingerprinting/nsRFPService.cpp
--- a/toolkit/components/resistfingerprinting/nsRFPService.cpp
+++ b/toolkit/components/resistfingerprinting/nsRFPService.cpp
@@ -52,22 +52,32 @@ static mozilla::LazyLogModule gResistFin
 #define RFP_TARGET_VIDEO_RES_DEFAULT 480
 #define PROFILE_INITIALIZED_TOPIC "profile-initial-state"
 
 #define RFP_DEFAULT_SPOOFING_KEYBOARD_LANG KeyboardLang::EN
 #define RFP_DEFAULT_SPOOFING_KEYBOARD_REGION KeyboardRegion::US
 
 NS_IMPL_ISUPPORTS(nsRFPService, nsIObserver)
 
+/*
+ * The below variables are marked with 'Relaxed' memory ordering. We don't
+ * particurally care that threads have a percently consistent view of the values
+ * of these prefs. They are not expected to change often, and having an outdated
+ * view is not particurally harmful. They will eventually become consistent.
+ *
+ * The variables will, however, be read often (specifically sResolutionUSec on
+ * each timer rounding) so performance is important.
+ */
+
 static StaticRefPtr<nsRFPService> sRFPService;
 static bool sInitialized = false;
-Atomic<bool, ReleaseAcquire> nsRFPService::sPrivacyResistFingerprinting;
-Atomic<bool, ReleaseAcquire> nsRFPService::sPrivacyTimerPrecisionReduction;
+Atomic<bool, Relaxed> nsRFPService::sPrivacyResistFingerprinting;
+Atomic<bool, Relaxed> nsRFPService::sPrivacyTimerPrecisionReduction;
 // Note: anytime you want to use this variable, you should probably use TimerResolution() instead
-Atomic<uint32_t, ReleaseAcquire> sResolutionUSec;
+Atomic<uint32_t, Relaxed> sResolutionUSec;
 static uint32_t sVideoFramesPerSec;
 static uint32_t sVideoDroppedRatio;
 static uint32_t sTargetVideoRes;
 nsDataHashtable<KeyboardHashKey, const SpoofingKeyboardCode*>*
   nsRFPService::sSpoofingKeyboardCodes = nullptr;
 
 /* static */
 nsRFPService*
diff --git a/toolkit/components/resistfingerprinting/nsRFPService.h b/toolkit/components/resistfingerprinting/nsRFPService.h
--- a/toolkit/components/resistfingerprinting/nsRFPService.h
+++ b/toolkit/components/resistfingerprinting/nsRFPService.h
@@ -242,18 +242,18 @@ private:
 
   static void GetKeyboardLangAndRegion(const nsAString& aLanguage,
                                        KeyboardLangs& aLang,
                                        KeyboardRegions& aRegion);
   static bool GetSpoofedKeyCodeInfo(const nsIDocument* aDoc,
                                     const WidgetKeyboardEvent* aKeyboardEvent,
                                     SpoofingKeyboardCode& aOut);
 
-  static Atomic<bool, ReleaseAcquire> sPrivacyResistFingerprinting;
-  static Atomic<bool, ReleaseAcquire> sPrivacyTimerPrecisionReduction;
+  static Atomic<bool, Relaxed> sPrivacyResistFingerprinting;
+  static Atomic<bool, Relaxed> sPrivacyTimerPrecisionReduction;
 
   static nsDataHashtable<KeyboardHashKey, const SpoofingKeyboardCode*>* sSpoofingKeyboardCodes;
 
   nsCString mInitialTZValue;
 };
 
 } // mozilla namespace
 
