# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1544826244 0
# Node ID ed2f754410ed822c5a1e2496a59eb1679346dda5
# Parent  932e84dada781f0ecf8b2f3f5de4885651f9f3fb
Bug 1514121 - Remove unused rust OOM handling variant. r=froydnj

This removes the code added in bug 1458161, because the old versions of
rust that required it can't be used to build Gecko anymore. The variant
for newer versions of rust stays.

Differential Revision: https://phabricator.services.mozilla.com/D14528

diff --git a/toolkit/library/rust/shared/lib.rs b/toolkit/library/rust/shared/lib.rs
--- a/toolkit/library/rust/shared/lib.rs
+++ b/toolkit/library/rust/shared/lib.rs
@@ -1,14 +1,12 @@
 // This Source Code Form is subject to the terms of the Mozilla Public
 // License, v. 2.0. If a copy of the MPL was not distributed with this
 // file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
-#![cfg_attr(feature = "oom_with_global_alloc",
-            feature(global_allocator, alloc, alloc_system, allocator_api))]
 #![cfg_attr(feature = "oom_with_hook", feature(oom_hook))]
 
 #[cfg(feature="servo")]
 extern crate geckoservo;
 
 extern crate mp4parse_capi;
 extern crate nsstring;
 extern crate nserror;
@@ -127,72 +125,16 @@ fn panic_hook(info: &panic::PanicInfo) {
 }
 
 /// Configure a panic hook to redirect rust panics to Gecko's MOZ_CrashOOL.
 #[no_mangle]
 pub extern "C" fn install_rust_panic_hook() {
     panic::set_hook(Box::new(panic_hook));
 }
 
-// Wrap the rust system allocator to override the OOM handler, redirecting
-// to Gecko's, which interacts with the crash reporter.
-// This relies on unstable APIs that have not changed between 1.24 and 1.27.
-// In 1.27, the API changed, so we'll need to adapt to those changes before
-// we can ship with 1.27. As of writing, there might still be further changes
-// to those APIs before 1.27 is released, so we wait for those.
-#[cfg(feature = "oom_with_global_alloc")]
-mod global_alloc {
-    extern crate alloc;
-    extern crate alloc_system;
-
-    use self::alloc::allocator::{Alloc, AllocErr, Layout};
-    use self::alloc_system::System;
-
-    pub struct GeckoHeap;
-
-    extern "C" {
-        fn GeckoHandleOOM(size: usize) -> !;
-    }
-
-    unsafe impl<'a> Alloc for &'a GeckoHeap {
-        unsafe fn alloc(&mut self, layout: Layout) -> Result<*mut u8, AllocErr> {
-            System.alloc(layout)
-        }
-
-        unsafe fn dealloc(&mut self, ptr: *mut u8, layout: Layout) {
-            System.dealloc(ptr, layout)
-        }
-
-        fn oom(&mut self, e: AllocErr) -> ! {
-            match e {
-                AllocErr::Exhausted { request } => unsafe { GeckoHandleOOM(request.size()) },
-                _ => System.oom(e),
-            }
-        }
-
-        unsafe fn realloc(
-            &mut self,
-            ptr: *mut u8,
-            layout: Layout,
-            new_layout: Layout,
-        ) -> Result<*mut u8, AllocErr> {
-            System.realloc(ptr, layout, new_layout)
-        }
-
-        unsafe fn alloc_zeroed(&mut self, layout: Layout) -> Result<*mut u8, AllocErr> {
-            System.alloc_zeroed(layout)
-        }
-    }
-
-}
-
-#[cfg(feature = "oom_with_global_alloc")]
-#[global_allocator]
-static HEAP: global_alloc::GeckoHeap = global_alloc::GeckoHeap;
-
 #[cfg(feature = "oom_with_hook")]
 mod oom_hook {
     use std::alloc::{Layout, set_oom_hook};
 
     extern "C" {
         fn GeckoHandleOOM(size: usize) -> !;
     }
 
