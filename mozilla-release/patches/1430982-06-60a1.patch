# HG changeset patch
# User Masayuki Nakano <masayuki@d-toybox.com>
# Date 1516276873 -32400
# Node ID 2398b267741bd5e487cc0bde7b4a8952c5a7d4e4
# Parent  95545a5aa90e178607c3ca488f0c9c8b16abc514
Bug 1430982 - part 6: Rename nsEditorSpellCheck to mozilla::EditorSpellCheck and expose its header r=m_kato

For making mozInlineSpellChecker stores nsEditorSpellCheck directly, we need
to expose its header file.  For doing that this patch renames the class to
mozilla::EditorSpellCheck and expose it as "mozilla/EditorSpellCheck.h".

MozReview-Commit-ID: 5H66Y2vVshu

diff --git a/editor/composer/nsEditorSpellCheck.cpp b/editor/composer/EditorSpellCheck.cpp
rename from editor/composer/nsEditorSpellCheck.cpp
rename to editor/composer/EditorSpellCheck.cpp
--- a/editor/composer/nsEditorSpellCheck.cpp
+++ b/editor/composer/EditorSpellCheck.cpp
@@ -1,29 +1,29 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=2 sts=2 sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-#include <stdlib.h>                     // for getenv
+#include "EditorSpellCheck.h"
 
 #include "mozilla/Attributes.h"         // for final
+#include "mozilla/EditorBase.h"         // for EditorBase
 #include "mozilla/dom/Element.h"        // for Element
 #include "mozilla/dom/Selection.h"
 #include "mozilla/intl/LocaleService.h" // for retrieving app locale
 #include "mozilla/mozalloc.h"           // for operator delete, etc
 #include "mozilla/mozSpellChecker.h"    // for mozSpellChecker
 #include "mozilla/Preferences.h"        // for Preferences
 #include "mozilla/TextServicesDocument.h" // for TextServicesDocument
 #include "nsAString.h"                  // for nsAString::IsEmpty, etc
 #include "nsComponentManagerUtils.h"    // for do_CreateInstance
 #include "nsDebug.h"                    // for NS_ENSURE_TRUE, etc
 #include "nsDependentSubstring.h"       // for Substring
-#include "nsEditorSpellCheck.h"
 #include "nsError.h"                    // for NS_ERROR_NOT_INITIALIZED, etc
 #include "nsIContent.h"                 // for nsIContent
 #include "nsIContentPrefService2.h"     // for nsIContentPrefService2, etc
 #include "nsIDOMDocument.h"             // for nsIDOMDocument
 #include "nsIDOMElement.h"              // for nsIDOMElement
 #include "nsIDocument.h"                // for nsIDocument
 #include "nsIEditor.h"                  // for nsIEditor
 #include "nsIHTMLEditor.h"              // for nsIHTMLEditor
@@ -41,34 +41,40 @@
 #include "nsReadableUtils.h"            // for ToNewUnicode, EmptyString, etc
 #include "nsServiceManagerUtils.h"      // for do_GetService
 #include "nsString.h"                   // for nsAutoString, nsString, etc
 #include "nsStringFwd.h"                // for nsAFlatString
 #include "nsStyleUtil.h"                // for nsStyleUtil
 #include "nsXULAppAPI.h"                // for XRE_GetProcessType
 #include "nsIPlaintextEditor.h"         // for editor flags
 
-using namespace mozilla;
-using namespace mozilla::dom;
-using mozilla::intl::LocaleService;
+namespace mozilla {
+
+using namespace dom;
+using intl::LocaleService;
+
+class UpdateDictionaryHolder
+{
+private:
+  EditorSpellCheck* mSpellCheck;
 
-class UpdateDictionaryHolder {
-  private:
-    nsEditorSpellCheck* mSpellCheck;
-  public:
-    explicit UpdateDictionaryHolder(nsEditorSpellCheck* esc): mSpellCheck(esc) {
-      if (mSpellCheck) {
-        mSpellCheck->BeginUpdateDictionary();
-      }
+public:
+  explicit UpdateDictionaryHolder(EditorSpellCheck* esc)
+    : mSpellCheck(esc)
+  {
+    if (mSpellCheck) {
+      mSpellCheck->BeginUpdateDictionary();
     }
-    ~UpdateDictionaryHolder() {
-      if (mSpellCheck) {
-        mSpellCheck->EndUpdateDictionary();
-      }
+  }
+
+  ~UpdateDictionaryHolder() {
+    if (mSpellCheck) {
+      mSpellCheck->EndUpdateDictionary();
     }
+  }
 };
 
 #define CPS_PREF_NAME NS_LITERAL_STRING("spellcheck.lang")
 
 /**
  * Gets the URI of aEditor's document.
  */
 static nsresult
@@ -110,20 +116,24 @@ GetLoadContext(nsIEditor* aEditor)
  * Fetches the dictionary stored in content prefs and maintains state during the
  * fetch, which is asynchronous.
  */
 class DictionaryFetcher final : public nsIContentPrefCallback2
 {
 public:
   NS_DECL_ISUPPORTS
 
-  DictionaryFetcher(nsEditorSpellCheck* aSpellCheck,
+  DictionaryFetcher(EditorSpellCheck* aSpellCheck,
                     nsIEditorSpellCheckCallback* aCallback,
                     uint32_t aGroup)
-    : mCallback(aCallback), mGroup(aGroup), mSpellCheck(aSpellCheck) {}
+    : mCallback(aCallback)
+    , mGroup(aGroup)
+    , mSpellCheck(aSpellCheck)
+  {
+  }
 
   NS_IMETHOD Fetch(nsIEditor* aEditor);
 
   NS_IMETHOD HandleResult(nsIContentPref* aPref) override
   {
     nsCOMPtr<nsIVariant> value;
     nsresult rv = aPref->GetValue(getter_AddRefs(value));
     NS_ENSURE_SUCCESS(rv, rv);
@@ -146,18 +156,19 @@ public:
   uint32_t mGroup;
   nsString mRootContentLang;
   nsString mRootDocContentLang;
   nsString mDictionary;
 
 private:
   ~DictionaryFetcher() {}
 
-  RefPtr<nsEditorSpellCheck> mSpellCheck;
+  RefPtr<EditorSpellCheck> mSpellCheck;
 };
+
 NS_IMPL_ISUPPORTS(DictionaryFetcher, nsIContentPrefCallback2)
 
 class ContentPrefInitializerRunnable final : public Runnable
 {
 public:
   ContentPrefInitializerRunnable(nsIEditor* aEditor,
                                  nsIContentPrefCallback2* aCallback)
     : Runnable("ContentPrefInitializerRunnable")
@@ -276,67 +287,66 @@ ClearCurrentDictionary(nsIEditor* aEdito
     do_GetService(NS_CONTENT_PREF_SERVICE_CONTRACTID);
   NS_ENSURE_TRUE(contentPrefService, NS_ERROR_NOT_INITIALIZED);
 
   nsCOMPtr<nsILoadContext> loadContext = GetLoadContext(aEditor);
   return contentPrefService->RemoveByDomainAndName(
     NS_ConvertUTF8toUTF16(docUriSpec), CPS_PREF_NAME, loadContext, nullptr);
 }
 
-NS_IMPL_CYCLE_COLLECTING_ADDREF(nsEditorSpellCheck)
-NS_IMPL_CYCLE_COLLECTING_RELEASE(nsEditorSpellCheck)
+NS_IMPL_CYCLE_COLLECTING_ADDREF(EditorSpellCheck)
+NS_IMPL_CYCLE_COLLECTING_RELEASE(EditorSpellCheck)
 
-NS_INTERFACE_MAP_BEGIN(nsEditorSpellCheck)
+NS_INTERFACE_MAP_BEGIN(EditorSpellCheck)
   NS_INTERFACE_MAP_ENTRY(nsIEditorSpellCheck)
   NS_INTERFACE_MAP_ENTRY_AMBIGUOUS(nsISupports, nsIEditorSpellCheck)
-  NS_INTERFACE_MAP_ENTRIES_CYCLE_COLLECTION(nsEditorSpellCheck)
+  NS_INTERFACE_MAP_ENTRIES_CYCLE_COLLECTION(EditorSpellCheck)
 NS_INTERFACE_MAP_END
 
-NS_IMPL_CYCLE_COLLECTION(nsEditorSpellCheck,
+NS_IMPL_CYCLE_COLLECTION(EditorSpellCheck,
                          mEditor,
                          mSpellChecker,
                          mTxtSrvFilter)
 
-nsEditorSpellCheck::nsEditorSpellCheck()
+EditorSpellCheck::EditorSpellCheck()
   : mSuggestedWordIndex(0)
   , mDictionaryIndex(0)
-  , mEditor(nullptr)
   , mDictionaryFetcherGroup(0)
   , mUpdateDictionaryRunning(false)
 {
 }
 
-nsEditorSpellCheck::~nsEditorSpellCheck()
+EditorSpellCheck::~EditorSpellCheck()
 {
   // Make sure we blow the spellchecker away, just in
   // case it hasn't been destroyed already.
   mSpellChecker = nullptr;
 }
 
 // The problem is that if the spell checker does not exist, we can not tell
 // which dictionaries are installed. This function works around the problem,
 // allowing callers to ask if we can spell check without actually doing so (and
 // enabling or disabling UI as necessary). This just creates a spellcheck
 // object if needed and asks it for the dictionary list.
 NS_IMETHODIMP
-nsEditorSpellCheck::CanSpellCheck(bool* _retval)
+EditorSpellCheck::CanSpellCheck(bool* aCanSpellCheck)
 {
   RefPtr<mozSpellChecker> spellChecker = mSpellChecker;
   if (!spellChecker) {
     spellChecker = new mozSpellChecker();
     DebugOnly<nsresult> rv = spellChecker->Init();
     MOZ_ASSERT(NS_SUCCEEDED(rv));
   }
   nsTArray<nsString> dictList;
   nsresult rv = spellChecker->GetDictionaryList(&dictList);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
-  *_retval = (dictList.Length() > 0);
+  *aCanSpellCheck = !dictList.IsEmpty();
   return NS_OK;
 }
 
 // Instances of this class can be used as either runnables or RAII helpers.
 class CallbackCaller final : public Runnable
 {
 public:
   explicit CallbackCaller(nsIEditorSpellCheckCallback* aCallback)
@@ -359,17 +369,19 @@ public:
     return NS_OK;
   }
 
 private:
   nsCOMPtr<nsIEditorSpellCheckCallback> mCallback;
 };
 
 NS_IMETHODIMP
-nsEditorSpellCheck::InitSpellChecker(nsIEditor* aEditor, bool aEnableSelectionChecking, nsIEditorSpellCheckCallback* aCallback)
+EditorSpellCheck::InitSpellChecker(nsIEditor* aEditor,
+                                   bool aEnableSelectionChecking,
+                                   nsIEditorSpellCheckCallback* aCallback)
 {
   NS_ENSURE_TRUE(aEditor, NS_ERROR_NULL_POINTER);
   mEditor = aEditor;
 
   nsCOMPtr<nsIDOMDocument> domDoc;
   mEditor->GetDocument(getter_AddRefs(domDoc));
   nsCOMPtr<nsIDocument> doc = do_QueryInterface(domDoc);
   NS_ENSURE_STATE(doc);
@@ -437,125 +449,126 @@ nsEditorSpellCheck::InitSpellChecker(nsI
     rv = doc->Dispatch(TaskCategory::Other, caller.forget());
     NS_ENSURE_SUCCESS(rv, rv);
   }
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::GetNextMisspelledWord(nsAString& aNextMisspelledWord)
+EditorSpellCheck::GetNextMisspelledWord(nsAString& aNextMisspelledWord)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   DeleteSuggestedWordList();
   // Beware! This may flush notifications via synchronous
   // ScrollSelectionIntoView.
   return mSpellChecker->NextMisspelledWord(aNextMisspelledWord,
                                            &mSuggestedWordList);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::GetSuggestedWord(nsAString& aSuggestedWord)
+EditorSpellCheck::GetSuggestedWord(nsAString& aSuggestedWord)
 {
   // XXX This is buggy if mSuggestedWordList.Length() is over INT32_MAX.
   if (mSuggestedWordIndex < static_cast<int32_t>(mSuggestedWordList.Length())) {
     aSuggestedWord = mSuggestedWordList[mSuggestedWordIndex];
     mSuggestedWordIndex++;
   } else {
     // A blank string signals that there are no more strings
     aSuggestedWord.Truncate();
   }
   return NS_OK;
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::CheckCurrentWord(const nsAString& aSuggestedWord,
-                                     bool *aIsMisspelled)
+EditorSpellCheck::CheckCurrentWord(const nsAString& aSuggestedWord,
+                                   bool* aIsMisspelled)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   DeleteSuggestedWordList();
   return mSpellChecker->CheckWord(aSuggestedWord,
                                   aIsMisspelled, &mSuggestedWordList);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::CheckCurrentWordNoSuggest(const nsAString& aSuggestedWord,
-                                              bool *aIsMisspelled)
+EditorSpellCheck::CheckCurrentWordNoSuggest(const nsAString& aSuggestedWord,
+                                            bool* aIsMisspelled)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   return mSpellChecker->CheckWord(aSuggestedWord,
                                   aIsMisspelled, nullptr);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::ReplaceWord(const nsAString& aMisspelledWord,
-                                const nsAString& aReplaceWord,
-                                bool             allOccurrences)
+EditorSpellCheck::ReplaceWord(const nsAString& aMisspelledWord,
+                              const nsAString& aReplaceWord,
+                              bool aAllOccurrences)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   return mSpellChecker->Replace(aMisspelledWord,
-                                aReplaceWord, allOccurrences);
+                                aReplaceWord, aAllOccurrences);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::IgnoreWordAllOccurrences(const nsAString& aWord)
+EditorSpellCheck::IgnoreWordAllOccurrences(const nsAString& aWord)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   return mSpellChecker->IgnoreAll(aWord);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::GetPersonalDictionary()
+EditorSpellCheck::GetPersonalDictionary()
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
    // We can spell check with any editor type
   mDictionaryList.Clear();
   mDictionaryIndex = 0;
   return mSpellChecker->GetPersonalDictionary(&mDictionaryList);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::GetPersonalDictionaryWord(nsAString& aDictionaryWord)
+EditorSpellCheck::GetPersonalDictionaryWord(nsAString& aDictionaryWord)
 {
   // XXX This is buggy if mDictionaryList.Length() is over INT32_MAX.
   if (mDictionaryIndex < static_cast<int32_t>(mDictionaryList.Length())) {
     aDictionaryWord = mDictionaryList[mDictionaryIndex];
     mDictionaryIndex++;
   } else {
     // A blank string signals that there are no more strings
     aDictionaryWord.Truncate();
   }
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::AddWordToDictionary(const nsAString& aWord)
+EditorSpellCheck::AddWordToDictionary(const nsAString& aWord)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   return mSpellChecker->AddWordToPersonalDictionary(aWord);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::RemoveWordFromDictionary(const nsAString& aWord)
+EditorSpellCheck::RemoveWordFromDictionary(const nsAString& aWord)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   return mSpellChecker->RemoveWordFromPersonalDictionary(aWord);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::GetDictionaryList(char16_t ***aDictionaryList, uint32_t *aCount)
+EditorSpellCheck::GetDictionaryList(char16_t*** aDictionaryList,
+                                    uint32_t* aCount)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   NS_ENSURE_TRUE(aDictionaryList && aCount, NS_ERROR_NULL_POINTER);
 
   *aDictionaryList = 0;
   *aCount          = 0;
 
@@ -588,29 +601,29 @@ nsEditorSpellCheck::GetDictionaryList(ch
   for (uint32_t i = 0; i < *aCount; i++) {
     tmpPtr[i] = ToNewUnicode(dictList[i]);
   }
 
   return rv;
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::GetCurrentDictionary(nsAString& aDictionary)
+EditorSpellCheck::GetCurrentDictionary(nsAString& aDictionary)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   return mSpellChecker->GetCurrentDictionary(aDictionary);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::SetCurrentDictionary(const nsAString& aDictionary)
+EditorSpellCheck::SetCurrentDictionary(const nsAString& aDictionary)
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
-  RefPtr<nsEditorSpellCheck> kungFuDeathGrip = this;
+  RefPtr<EditorSpellCheck> kungFuDeathGrip = this;
 
   // The purpose of mUpdateDictionaryRunning is to avoid doing all of this if
   // UpdateCurrentDictionary's helper method DictionaryFetched, which calls us,
   // is on the stack. In other words: Only do this, if the user manually selected a
   // dictionary to use.
   if (!mUpdateDictionaryRunning) {
 
     // Ignore pending dictionary fetchers by increasing this number.
@@ -657,55 +670,56 @@ nsEditorSpellCheck::SetCurrentDictionary
 #endif
       }
     }
   }
   return mSpellChecker->SetCurrentDictionary(aDictionary);
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::UninitSpellChecker()
+EditorSpellCheck::UninitSpellChecker()
 {
   NS_ENSURE_TRUE(mSpellChecker, NS_ERROR_NOT_INITIALIZED);
 
   // Cleanup - kill the spell checker
   DeleteSuggestedWordList();
   mDictionaryList.Clear();
   mDictionaryIndex = 0;
   mDictionaryFetcherGroup++;
   mSpellChecker = nullptr;
   return NS_OK;
 }
 
 
 NS_IMETHODIMP
-nsEditorSpellCheck::SetFilter(nsITextServicesFilter *filter)
+EditorSpellCheck::SetFilter(nsITextServicesFilter *aFilter)
 {
-  mTxtSrvFilter = filter;
+  mTxtSrvFilter = aFilter;
   return NS_OK;
 }
 
 nsresult
-nsEditorSpellCheck::DeleteSuggestedWordList()
+EditorSpellCheck::DeleteSuggestedWordList()
 {
   mSuggestedWordList.Clear();
   mSuggestedWordIndex = 0;
   return NS_OK;
 }
 
 NS_IMETHODIMP
-nsEditorSpellCheck::UpdateCurrentDictionary(nsIEditorSpellCheckCallback* aCallback)
+EditorSpellCheck::UpdateCurrentDictionary(
+                    nsIEditorSpellCheckCallback* aCallback)
 {
   if (NS_WARN_IF(!mSpellChecker)) {
     return NS_ERROR_NOT_INITIALIZED;
   }
 
   nsresult rv;
 
-  RefPtr<nsEditorSpellCheck> kungFuDeathGrip = this;
+  RefPtr<EditorSpellCheck> kungFuDeathGrip = this;
 
   // Get language with html5 algorithm
   nsCOMPtr<nsIContent> rootContent;
   nsCOMPtr<nsIHTMLEditor> htmlEditor = do_QueryInterface(mEditor);
   if (htmlEditor) {
     rootContent = htmlEditor->GetActiveEditingHost();
   } else {
     nsCOMPtr<nsIDOMElement> rootElement;
@@ -741,20 +755,20 @@ nsEditorSpellCheck::UpdateCurrentDiction
   NS_ENSURE_SUCCESS(rv, rv);
 
   return NS_OK;
 }
 
 // Helper function that iterates over the list of dictionaries and sets the one
 // that matches based on a given comparison type.
 void
-nsEditorSpellCheck::BuildDictionaryList(const nsAString& aDictName,
-                                        const nsTArray<nsString>& aDictList,
-                                        enum dictCompare aCompareType,
-                                        nsTArray<nsString>& aOutList)
+EditorSpellCheck::BuildDictionaryList(const nsAString& aDictName,
+                                      const nsTArray<nsString>& aDictList,
+                                      enum dictCompare aCompareType,
+                                      nsTArray<nsString>& aOutList)
 {
   for (uint32_t i = 0; i < aDictList.Length(); i++) {
     nsAutoString dictStr(aDictList.ElementAt(i));
     bool equals = false;
     switch (aCompareType) {
       case DICT_NORMAL_COMPARE:
         equals = aDictName.Equals(dictStr);
         break;
@@ -776,20 +790,20 @@ nsEditorSpellCheck::BuildDictionaryList(
       // dictionary from the list. This must work, if it doesn't, there is
       // no point trying another one.
       return;
     }
   }
 }
 
 nsresult
-nsEditorSpellCheck::DictionaryFetched(DictionaryFetcher* aFetcher)
+EditorSpellCheck::DictionaryFetched(DictionaryFetcher* aFetcher)
 {
   MOZ_ASSERT(aFetcher);
-  RefPtr<nsEditorSpellCheck> kungFuDeathGrip = this;
+  RefPtr<EditorSpellCheck> kungFuDeathGrip = this;
 
   BeginUpdateDictionary();
 
   if (aFetcher->mGroup < mDictionaryFetcherGroup) {
     // SetCurrentDictionary was called after the fetch started.  Don't overwrite
     // that dictionary with the fetched one.
     EndUpdateDictionary();
     if (aFetcher->mCallback) {
@@ -857,17 +871,17 @@ nsEditorSpellCheck::DictionaryFetched(Di
   uint32_t flags;
   mEditor->GetFlags(&flags);
   if (!(flags & nsIPlaintextEditor::eEditorMailMask)) {
     dictName.Assign(aFetcher->mDictionary);
     if (!dictName.IsEmpty()) {
       AutoTArray<nsString, 1> tryDictList;
       BuildDictionaryList(dictName, dictList, DICT_NORMAL_COMPARE, tryDictList);
 
-      RefPtr<nsEditorSpellCheck> self = this;
+      RefPtr<EditorSpellCheck> self = this;
       RefPtr<DictionaryFetcher> fetcher = aFetcher;
       mSpellChecker->SetCurrentDictionaryFromList(tryDictList)->Then(
         GetMainThreadSerialEventTarget(),
         __func__,
         [self, fetcher]() {
 #ifdef DEBUG_DICT
           printf("***** Assigned from content preferences |%s|\n",
                  NS_ConvertUTF16toUTF8(dictName).get());
@@ -892,17 +906,17 @@ nsEditorSpellCheck::DictionaryFetched(Di
       return NS_OK;
     }
   }
   SetFallbackDictionary(aFetcher);
   return NS_OK;
 }
 
 void
-nsEditorSpellCheck::SetFallbackDictionary(DictionaryFetcher* aFetcher)
+EditorSpellCheck::SetFallbackDictionary(DictionaryFetcher* aFetcher)
 {
   MOZ_ASSERT(mUpdateDictionaryRunning);
 
   AutoTArray<nsString, 6> tryDictList;
 
   // We obtain a list of available dictionaries.
   AutoTArray<nsString, 8> dictList;
   nsresult rv = mSpellChecker->GetDictionaryList(&dictList);
@@ -1040,25 +1054,27 @@ nsEditorSpellCheck::SetFallbackDictionar
     BuildDictionaryList(dictList[0], dictList, DICT_NORMAL_COMPARE,
                         tryDictList);
 #ifdef DEBUG_DICT
     printf("***** Trying first of list |%s|\n",
            NS_ConvertUTF16toUTF8(dictList[0]).get());
 #endif
   }
 
-  RefPtr<nsEditorSpellCheck> self = this;
+  RefPtr<EditorSpellCheck> self = this;
   RefPtr<DictionaryFetcher> fetcher = aFetcher;
   mSpellChecker->SetCurrentDictionaryFromList(tryDictList)->Then(
     GetMainThreadSerialEventTarget(),
     __func__,
     [self, fetcher]() {
       // If an error was thrown while setting the dictionary, just
       // fail silently so that the spellchecker dialog is allowed to come
       // up. The user can manually reset the language to their choice on
       // the dialog if it is wrong.
       self->DeleteSuggestedWordList();
       self->EndUpdateDictionary();
       if (fetcher->mCallback) {
         fetcher->mCallback->EditorSpellCheckDone();
       }
     });
 }
+
+} // namespace mozilla
diff --git a/editor/composer/nsEditorSpellCheck.h b/editor/composer/EditorSpellCheck.h
rename from editor/composer/nsEditorSpellCheck.h
rename to editor/composer/EditorSpellCheck.h
--- a/editor/composer/nsEditorSpellCheck.h
+++ b/editor/composer/EditorSpellCheck.h
@@ -1,15 +1,15 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-#ifndef nsEditorSpellCheck_h___
-#define nsEditorSpellCheck_h___
+#ifndef mozilla_EditorSpellCheck_h
+#define mozilla_EditorSpellCheck_h
 
 
 #include "nsCOMPtr.h"                   // for nsCOMPtr
 #include "nsCycleCollectionParticipant.h"
 #include "nsIEditorSpellCheck.h"        // for NS_DECL_NSIEDITORSPELLCHECK, etc
 #include "nsISupportsImpl.h"
 #include "nsString.h"                   // for nsString
 #include "nsTArray.h"                   // for nsTArray
@@ -21,71 +21,73 @@ class nsISpellChecker;
 class nsITextServicesFilter;
 
 #define NS_EDITORSPELLCHECK_CID                     \
 { /* {75656ad9-bd13-4c5d-939a-ec6351eea0cc} */        \
     0x75656ad9, 0xbd13, 0x4c5d,                       \
     { 0x93, 0x9a, 0xec, 0x63, 0x51, 0xee, 0xa0, 0xcc }\
 }
 
+namespace mozilla {
+
 class DictionaryFetcher;
 
-enum dictCompare {
+enum dictCompare
+{
   DICT_NORMAL_COMPARE,
   DICT_COMPARE_CASE_INSENSITIVE,
   DICT_COMPARE_DASHMATCH
 };
 
-class nsEditorSpellCheck final : public nsIEditorSpellCheck
+class EditorSpellCheck final : public nsIEditorSpellCheck
 {
   friend class DictionaryFetcher;
 
 public:
-  nsEditorSpellCheck();
+  EditorSpellCheck();
 
   NS_DECL_CYCLE_COLLECTING_ISUPPORTS
-  NS_DECL_CYCLE_COLLECTION_CLASS(nsEditorSpellCheck)
+  NS_DECL_CYCLE_COLLECTION_CLASS(EditorSpellCheck)
 
   /* Declare all methods in the nsIEditorSpellCheck interface */
   NS_DECL_NSIEDITORSPELLCHECK
 
 protected:
-  virtual ~nsEditorSpellCheck();
+  virtual ~EditorSpellCheck();
 
   RefPtr<mozSpellChecker> mSpellChecker;
+  nsCOMPtr<nsITextServicesFilter> mTxtSrvFilter;
+  nsCOMPtr<nsIEditor> mEditor;
 
-  nsTArray<nsString>  mSuggestedWordList;
-  int32_t        mSuggestedWordIndex;
+  nsTArray<nsString> mSuggestedWordList;
 
   // these are the words in the current personal dictionary,
   // GetPersonalDictionary must be called to load them.
   nsTArray<nsString>  mDictionaryList;
-  int32_t        mDictionaryIndex;
-
-  nsresult       DeleteSuggestedWordList();
-
-  nsCOMPtr<nsITextServicesFilter> mTxtSrvFilter;
-  nsCOMPtr<nsIEditor> mEditor;
 
   nsString mPreferredLang;
 
+  int32_t mSuggestedWordIndex;
+  int32_t mDictionaryIndex;
   uint32_t mDictionaryFetcherGroup;
 
   bool mUpdateDictionaryRunning;
 
+  nsresult DeleteSuggestedWordList();
+
   void BuildDictionaryList(const nsAString& aDictName,
                            const nsTArray<nsString>& aDictList,
                            enum dictCompare aCompareType,
                            nsTArray<nsString>& aTryList);
 
   nsresult DictionaryFetched(DictionaryFetcher* aFetchState);
 
   void SetFallbackDictionary(DictionaryFetcher* aFetcher);
 
 
 public:
   void BeginUpdateDictionary() { mUpdateDictionaryRunning = true ;}
   void EndUpdateDictionary() { mUpdateDictionaryRunning = false ;}
 };
 
-#endif // nsEditorSpellCheck_h___
+} // namespace mozilla
 
-
+#endif // mozilla_EditorSpellCheck_h
diff --git a/editor/composer/moz.build b/editor/composer/moz.build
--- a/editor/composer/moz.build
+++ b/editor/composer/moz.build
@@ -10,24 +10,28 @@ MOCHITEST_CHROME_MANIFESTS += ['test/chr
 
 XPIDL_SOURCES += [
     'nsIEditingSession.idl',
 ]
 
 XPIDL_MODULE = 'composer'
 
 UNIFIED_SOURCES += [
+    'EditorSpellCheck.cpp',
     'nsComposerCommands.cpp',
     'nsComposerCommandsUpdater.cpp',
     'nsComposerController.cpp',
     'nsComposerDocumentCommands.cpp',
     'nsComposerRegistration.cpp',
     'nsComposeTxtSrvFilter.cpp',
     'nsEditingSession.cpp',
-    'nsEditorSpellCheck.cpp',
+]
+
+EXPORTS.mozilla += [
+    'EditorSpellCheck.h',
 ]
 
 # Needed because we include HTMLEditor.h which indirectly includes nsDocument.h
 LOCAL_INCLUDES += [
     '/dom/base',
 ]
 
 FINAL_LIBRARY = 'xul'
diff --git a/editor/composer/nsComposerRegistration.cpp b/editor/composer/nsComposerRegistration.cpp
--- a/editor/composer/nsComposerRegistration.cpp
+++ b/editor/composer/nsComposerRegistration.cpp
@@ -1,35 +1,35 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-#include <stddef.h>                     // for nullptr
-
+#include "mozilla/EditorSpellCheck.h"   // for NS_EDITORSPELLCHECK_CID, etc
 #include "mozilla/Module.h"             // for Module, Module::CIDEntry, etc
 #include "mozilla/ModuleUtils.h"
 #include "mozilla/mozalloc.h"           // for operator new
 #include "nsCOMPtr.h"                   // for nsCOMPtr, getter_AddRefs, etc
 #include "nsComponentManagerUtils.h"    // for do_CreateInstance
 #include "nsComposeTxtSrvFilter.h"      // for nsComposeTxtSrvFilter, etc
 #include "nsComposerController.h"       // for nsComposerController, etc
 #include "nsDebug.h"                    // for NS_ENSURE_SUCCESS
 #include "nsEditingSession.h"           // for NS_EDITINGSESSION_CID, etc
-#include "nsEditorSpellCheck.h"         // for NS_EDITORSPELLCHECK_CID, etc
 #include "nsError.h"                    // for NS_ERROR_NO_AGGREGATION, etc
 #include "nsIController.h"              // for nsIController
 #include "nsIControllerCommandTable.h"  // for nsIControllerCommandTable, etc
 #include "nsIControllerContext.h"       // for nsIControllerContext
 #include "nsID.h"                       // for NS_DEFINE_NAMED_CID, etc
 #include "nsISupportsImpl.h"
 #include "nsISupportsUtils.h"           // for NS_ADDREF, NS_RELEASE
 #include "nsServiceManagerUtils.h"      // for do_GetService
 #include "nscore.h"                     // for nsresult
 
+using mozilla::EditorSpellCheck;
+
 class nsISupports;
 
 #define NS_HTMLEDITOR_COMMANDTABLE_CID \
 { 0x13e50d8d, 0x9cee, 0x4ad1, { 0xa3, 0xa2, 0x4a, 0x44, 0x2f, 0xdf, 0x7d, 0xfa } }
 
 #define NS_HTMLEDITOR_DOCSTATE_COMMANDTABLE_CID \
 { 0xa33982d3, 0x1adf, 0x4162, { 0x99, 0x41, 0xf7, 0x34, 0xbc, 0x45, 0xe4, 0xed } }
 
@@ -40,17 +40,17 @@ static NS_DEFINE_CID(kHTMLEditorDocState
 
 ////////////////////////////////////////////////////////////////////////
 // Define the contructor function for the objects
 //
 // NOTE: This creates an instance of objects by using the default constructor
 //
 
 NS_GENERIC_FACTORY_CONSTRUCTOR(nsEditingSession)
-NS_GENERIC_FACTORY_CONSTRUCTOR(nsEditorSpellCheck)
+NS_GENERIC_FACTORY_CONSTRUCTOR(EditorSpellCheck)
 
 // There are no macros that enable us to have 2 constructors
 // for the same object
 //
 // Here we are creating the same object with two different contract IDs
 // and then initializing it different.
 // Basically, we need to tell the filter whether it is doing mail or not
 static nsresult
@@ -196,17 +196,17 @@ NS_DEFINE_NAMED_CID(NS_COMPOSERTXTSRVFIL
 
 
 static const mozilla::Module::CIDEntry kComposerCIDs[] = {
   { &kNS_HTMLEDITORCONTROLLER_CID, false, nullptr, nsHTMLEditorControllerConstructor },
   { &kNS_EDITORDOCSTATECONTROLLER_CID, false, nullptr, nsHTMLEditorDocStateControllerConstructor },
   { &kNS_HTMLEDITOR_COMMANDTABLE_CID, false, nullptr, nsHTMLEditorCommandTableConstructor },
   { &kNS_HTMLEDITOR_DOCSTATE_COMMANDTABLE_CID, false, nullptr, nsHTMLEditorDocStateCommandTableConstructor },
   { &kNS_EDITINGSESSION_CID, false, nullptr, nsEditingSessionConstructor },
-  { &kNS_EDITORSPELLCHECK_CID, false, nullptr, nsEditorSpellCheckConstructor },
+  { &kNS_EDITORSPELLCHECK_CID, false, nullptr, EditorSpellCheckConstructor },
   { &kNS_COMPOSERTXTSRVFILTER_CID, false, nullptr, nsComposeTxtSrvFilterConstructorForComposer },
   { &kNS_COMPOSERTXTSRVFILTERMAIL_CID, false, nullptr, nsComposeTxtSrvFilterConstructorForMail },
   { nullptr }
 };
 
 static const mozilla::Module::ContractIDEntry kComposerContracts[] = {
   { "@mozilla.org/editor/htmleditorcontroller;1", &kNS_HTMLEDITORCONTROLLER_CID },
   { "@mozilla.org/editor/editordocstatecontroller;1", &kNS_EDITORDOCSTATECONTROLLER_CID },
diff --git a/layout/build/nsLayoutStatics.cpp b/layout/build/nsLayoutStatics.cpp
--- a/layout/build/nsLayoutStatics.cpp
+++ b/layout/build/nsLayoutStatics.cpp
@@ -93,17 +93,16 @@
 
 #include "nsError.h"
 
 #include "nsJSEnvironment.h"
 #include "nsContentSink.h"
 #include "nsFrameMessageManager.h"
 #include "nsDOMMutationObserver.h"
 #include "nsHyphenationManager.h"
-#include "nsEditorSpellCheck.h"
 #include "nsWindowMemoryReporter.h"
 #include "mozilla/dom/ContentParent.h"
 #include "mozilla/ProcessPriorityManager.h"
 #include "nsPermissionManager.h"
 #include "nsCookieService.h"
 #include "nsApplicationCacheService.h"
 #include "mozilla/dom/CustomElementRegistry.h"
 #include "mozilla/EventDispatcher.h"
