# HG changeset patch
# User Jed Davis <jld@mozilla.com>
# Date 1519792208 25200
# Node ID 936b73ae6e3ce7a98e596b0422b2776db349e85d
# Parent  599ddc1814f56b4a7fc0cdbdffaaaee6ffcdd0a9
Bug 1438401 - Quietly fail shmget() in sandboxed content processes. r=gcp

The X11 symbol interposition isn't enough, possibly because Cairo can
also use XCB.  Interposing XCB is more difficult because the API exposes
more protocol details.  Instead, just allow shmget to be called and
fail; this will tell Cairo that it can't use SysV IPC with the X server,
which is what we want.

MozReview-Commit-ID: 5y9tE7UXMTE

diff --git a/security/sandbox/linux/SandboxFilter.cpp b/security/sandbox/linux/SandboxFilter.cpp
--- a/security/sandbox/linux/SandboxFilter.cpp
+++ b/security/sandbox/linux/SandboxFilter.cpp
@@ -707,21 +707,25 @@ public:
     default:
       return SandboxPolicyCommon::EvaluateSocketCall(aCall, aHasArgs);
     }
   }
 
 #ifdef DESKTOP
   Maybe<ResultExpr> EvaluateIpcCall(int aCall) const override {
     switch(aCall) {
-      // These are a problem: SysV shared memory follows the Unix
-      // "same uid policy" and can't be restricted/brokered like file
-      // access.  But the graphics layer might not be using them
-      // anymore; this needs to be studied.
+      // These are a problem: SysV IPC follows the Unix "same uid
+      // policy" and can't be restricted/brokered like file access.
+      // We're not using it directly, but there are some library
+      // dependencies that do; see ContentNeedsSysVIPC() in
+      // SandboxLaunch.cpp.  Also, Cairo as used by GTK will sometimes
+      // try to use MIT-SHM, so shmget() is a non-fatal error.  See
+      // also bug 1376910 and bug 1438401.
     case SHMGET:
+      return Some(mAllowSysV ? Allow() : Error(EPERM));
     case SHMCTL:
     case SHMAT:
     case SHMDT:
     case SEMGET:
     case SEMCTL:
     case SEMOP:
       if (mAllowSysV) {
         return Some(Allow());
diff --git a/security/sandbox/linux/SandboxHooks.cpp b/security/sandbox/linux/SandboxHooks.cpp
--- a/security/sandbox/linux/SandboxHooks.cpp
+++ b/security/sandbox/linux/SandboxHooks.cpp
@@ -119,22 +119,8 @@ inotify_init(void)
 }
 
 extern "C" MOZ_EXPORT int
 inotify_init1(int flags)
 {
   errno = ENOSYS;
   return -1;
 }
-
-#ifdef MOZ_X11
-// We're already preventing the use of X11 MIT-SHM like this in
-// widget/gtk/mozgtk/mozgtk.c because of bug 1271100, but that's not
-// quite enough: sometimes libXext can be preloaded, so we have to
-// defeat that with our own preload library.  (With just the mozgtk
-// interposition, we saw crashes when we blocked the SysV IPC
-// syscalls; see bug 1376910 comment #14.)
-extern "C" MOZ_EXPORT Bool
-XShmQueryExtension(Display* aDisplay)
-{
-  return False;
-}
-#endif
