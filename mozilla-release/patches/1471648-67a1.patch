# HG changeset patch
# User Pavel Slepushkin <slepushkin@yandex.ru>
# Date 1549026489 0
# Node ID 5cf0c9b4e97bb208a2eaa3fdb3ad641b3b224b7a
# Parent  6301099b31e98881fa8f5d1ed516f2a3061f156f
Bug 1471648 - [mozlog] Add support for Python 3; r=raphael

Differential Revision: https://phabricator.services.mozilla.com/D18069

diff --git a/testing/mozbase/mozlog/mozlog/formatters/html/html.py b/testing/mozbase/mozlog/mozlog/formatters/html/html.py
--- a/testing/mozbase/mozlog/mozlog/formatters/html/html.py
+++ b/testing/mozbase/mozlog/mozlog/formatters/html/html.py
@@ -156,18 +156,21 @@ class HTMLFormatter(base.BaseFormatter):
                     if not content.startswith('data:image/png;base64,'):
                         href = 'data:image/png;base64,%s' % content
                     else:
                         href = content
                 else:
                     # Encode base64 to avoid that some browsers (such as Firefox, Opera)
                     # treats '#' as the start of another link if it is contained in the data URL.
                     # Use 'charset=utf-8' to show special characters like Chinese.
-                    utf_encoded = six.text_type(content).encode('utf-8', 'xmlcharrefreplace')
-                    href = 'data:text/html;charset=utf-8;base64,%s' % base64.b64encode(utf_encoded)
+                    utf8_encoded_bytes = six.text_type(content).encode('utf-8',
+                                                                       'xmlcharrefreplace')
+                    b64_encoded_bytes = base64.b64encode(utf8_encoded_bytes)
+                    b64_encoded_str = b64_encoded_bytes.decode()
+                    href = "data:text/html;charset=utf-8;base64,{0}".format(b64_encoded_str)
 
                 links_html.append(html.a(
                     name.title(),
                     class_=name,
                     href=href,
                     target='_blank'))
                 links_html.append(' ')
 
diff --git a/testing/mozbase/mozlog/mozlog/formatters/machformatter.py b/testing/mozbase/mozlog/mozlog/formatters/machformatter.py
--- a/testing/mozbase/mozlog/mozlog/formatters/machformatter.py
+++ b/testing/mozbase/mozlog/mozlog/formatters/machformatter.py
@@ -115,17 +115,17 @@ class MachFormatter(base.BaseFormatter):
         count = summary['counts']
         logs = summary['unexpected_logs']
 
         rv = ["", self.term.yellow(suite), self.term.yellow("~" * len(suite))]
 
         # Format check counts
         checks = self.summary.aggregate('count', count)
         rv.append("Ran {} checks ({})".format(sum(checks.values()),
-                  ', '.join(['{} {}s'.format(v, k) for k, v in checks.items() if v])))
+                  ', '.join(['{} {}s'.format(v, k) for k, v in sorted(checks.items()) if v])))
 
         # Format expected counts
         checks = self.summary.aggregate('expected', count, include_skip=False)
         rv.append("Expected results: {}".format(sum(checks.values())))
 
         # Format skip counts
         skip_tests = count["test"]["expected"]["skip"]
         skip_subtests = count["subtest"]["expected"]["skip"]
@@ -139,17 +139,17 @@ class MachFormatter(base.BaseFormatter):
         checks = self.summary.aggregate('unexpected', count)
         unexpected_count = sum(checks.values())
         if unexpected_count:
             rv.append("Unexpected results: {}".format(unexpected_count))
             for key in ('test', 'subtest', 'assert'):
                 if not count[key]['unexpected']:
                     continue
                 status_str = ", ".join(["{} {}".format(n, s)
-                                        for s, n in count[key]['unexpected'].items()])
+                                        for s, n in sorted(count[key]['unexpected'].items())])
                 rv.append("  {}: {} ({})".format(
                           key, sum(count[key]['unexpected'].values()), status_str))
 
         # Format status
         if not any(count[key]["unexpected"] for key in ('test', 'subtest', 'assert')):
             rv.append(self.term.green("OK"))
         else:
             heading = "Unexpected Results"
diff --git a/testing/mozbase/mozlog/mozlog/handlers/base.py b/testing/mozbase/mozlog/mozlog/handlers/base.py
--- a/testing/mozbase/mozlog/mozlog/handlers/base.py
+++ b/testing/mozbase/mozlog/mozlog/handlers/base.py
@@ -73,36 +73,49 @@ class StreamHandler(BaseHandler):
     :param formatter: formatter to convert messages to string format
     """
 
     _lock = Lock()
 
     def __init__(self, stream, formatter):
         BaseHandler.__init__(self, formatter)
         assert stream is not None
-        # This is a hack to deal with the case where we are passed a
-        # StreamWriter (e.g. by mach for stdout). A StreamWriter requires
-        # the code to handle unicode in exactly the opposite way compared
-        # to a normal stream i.e. you always have to pass in a Unicode
-        # object rather than a string object. Cope with that by extracting
-        # the underlying raw stream.
-        if isinstance(stream, codecs.StreamWriter):
-            stream = stream.stream
+        if six.PY2:
+            # This is a hack to deal with the case where we are passed a
+            # StreamWriter (e.g. by mach for stdout). A StreamWriter requires
+            # the code to handle unicode in exactly the opposite way compared
+            # to a normal stream i.e. you always have to pass in a Unicode
+            # object rather than a string object. Cope with that by extracting
+            # the underlying raw stream.
+            if isinstance(stream, codecs.StreamWriter):
+                stream = stream.stream
 
         self.formatter = formatter
         self.stream = stream
 
     def __call__(self, data):
         """Write a log message.
 
         :param data: Structured log message dictionary."""
         formatted = self.formatter(data)
         if not formatted:
             return
         with self._lock:
-            if isinstance(formatted, six.text_type):
-                self.stream.write(formatted.encode("utf-8", "replace"))
-            elif isinstance(formatted, str):
+            if six.PY3:
+                import io
+                import mozfile
+                if isinstance(self.stream, io.StringIO) and isinstance(formatted, bytes):
+                    formatted = formatted.decode()
+                elif (
+                     isinstance(self.stream, io.BytesIO)
+                     or isinstance(self.stream, mozfile.NamedTemporaryFile)
+                     ) and isinstance(formatted, str):
+                    formatted = formatted.encode()
                 self.stream.write(formatted)
             else:
-                assert False, "Got output from the formatter of an unexpected type"
+                if isinstance(formatted, six.text_type):
+                    self.stream.write(formatted.encode("utf-8", "replace"))
+                elif isinstance(formatted, str):
+                    self.stream.write(formatted)
+                else:
+                    assert False, "Got output from the formatter of an unexpected type"
 
             self.stream.flush()
diff --git a/testing/mozbase/mozlog/mozlog/unstructured/logger.py b/testing/mozbase/mozlog/mozlog/unstructured/logger.py
--- a/testing/mozbase/mozlog/mozlog/unstructured/logger.py
+++ b/testing/mozbase/mozlog/mozlog/unstructured/logger.py
@@ -145,17 +145,17 @@ class MozFormatter(Formatter):
             pad = self.level_length - len(record.levelname) + 1
         sep = '|'.rjust(pad)
         fmt = '%(name)s %(levelname)s ' + sep + ' %(message)s'
         if self.include_timestamp:
             fmt = '%(asctime)s ' + fmt
         # this protected member is used to define the format
         # used by the base Formatter's method
         self._fmt = fmt
-        return Formatter.format(self, record)
+        return Formatter(fmt=fmt).format(record)
 
 
 def getLogger(name, handler=None):
     """
     Returns the logger with the specified name.
     If the logger doesn't exist, it is created.
     If handler is specified, adds it to the logger. Otherwise a default handler
     that logs to standard output will be used.
diff --git a/testing/mozbase/mozlog/mozlog/unstructured/loglistener.py b/testing/mozbase/mozlog/mozlog/unstructured/loglistener.py
--- a/testing/mozbase/mozlog/mozlog/unstructured/loglistener.py
+++ b/testing/mozbase/mozlog/mozlog/unstructured/loglistener.py
@@ -27,17 +27,17 @@ class LogMessageHandler(socketserver.Bas
         self._partial_message = ''
         self.request.settimeout(self.server.timeout)
 
         while True:
             try:
                 data = self.request.recv(1024)
                 if not data:
                     return
-                self.process_message(data)
+                self.process_message(data.decode())
             except socket.timeout:
                 return
 
     def process_message(self, data):
         """Processes data from a connected log source. Messages are assumed
         to be newline delimited, and generally well-formed JSON."""
         for part in data.split('\n'):
             msg_string = self._partial_message + part
diff --git a/testing/mozbase/mozlog/setup.cfg b/testing/mozbase/mozlog/setup.cfg
new file mode 100644
--- /dev/null
+++ b/testing/mozbase/mozlog/setup.cfg
@@ -0,0 +1,2 @@
+[bdist_wheel]
+universal=1
diff --git a/testing/mozbase/mozlog/setup.py b/testing/mozbase/mozlog/setup.py
--- a/testing/mozbase/mozlog/setup.py
+++ b/testing/mozbase/mozlog/setup.py
@@ -2,17 +2,17 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this file,
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import
 
 from setuptools import setup, find_packages
 
 PACKAGE_NAME = 'mozlog'
-PACKAGE_VERSION = '3.10'
+PACKAGE_VERSION = '4.0'
 DEPS = [
     'blessings>=1.3',
     'mozterm',
     'six >= 1.10.0',
 ]
 
 
 setup(name=PACKAGE_NAME,
@@ -29,17 +29,17 @@ setup(name=PACKAGE_NAME,
       tests_require=['mozfile'],
       platforms=['Any'],
       classifiers=['Development Status :: 4 - Beta',
                    'Environment :: Console',
                    'Intended Audience :: Developers',
                    'License :: OSI Approved :: Mozilla Public License 1.1 (MPL 1.1)',
                    'Operating System :: OS Independent',
                    'Programming Language :: Python :: 2.7',
-                   'Programming Language :: Python :: 3',
+                   'Programming Language :: Python :: 3.5',
                    'Topic :: Software Development :: Libraries :: Python Modules'],
       package_data={"mozlog": ["formatters/html/main.js",
                                "formatters/html/style.css"]},
       entry_points={
           "console_scripts": [
               "structlog = mozlog.scripts:main"
           ],
           'pytest11': [
diff --git a/testing/mozbase/mozlog/tests/manifest.ini b/testing/mozbase/mozlog/tests/manifest.ini
--- a/testing/mozbase/mozlog/tests/manifest.ini
+++ b/testing/mozbase/mozlog/tests/manifest.ini
@@ -1,9 +1,6 @@
 [DEFAULT]
 subsuite = mozbase
 [test_logger.py]
-skip-if = python == 3
 [test_logtypes.py]
 [test_formatters.py]
-skip-if = python == 3
 [test_structured.py]
-skip-if = python == 3
diff --git a/testing/mozbase/mozlog/tests/test_formatters.py b/testing/mozbase/mozlog/tests/test_formatters.py
--- a/testing/mozbase/mozlog/tests/test_formatters.py
+++ b/testing/mozbase/mozlog/tests/test_formatters.py
@@ -1,135 +1,135 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import mozunit
 import pytest
-from io import BytesIO
+from six import BytesIO
 
 from mozlog.structuredlog import StructuredLogger
 from mozlog.formatters import (
     MachFormatter,
 )
 from mozlog.handlers import StreamHandler
 
 formatters = {
     'mach': MachFormatter,
 }
 
 FORMATS = {
     # A list of tuples consisting of (name, options, expected string).
     'PASS': [
-        ('mach', {}, """
+        ('mach', {}, b"""
  0:00.00 SUITE_START: running 3 tests
  0:00.00 TEST_START: test_foo
  0:00.00 TEST_END: OK
  0:00.00 TEST_START: test_bar
  0:00.00 TEST_END: Test OK. Subtests passed 1/1. Unexpected 0
  0:00.00 TEST_START: test_baz
  0:00.00 TEST_END: FAIL
  0:00.00 SUITE_END
 
 suite 1
 ~~~~~~~
-Ran 4 checks (3 tests, 1 subtests)
+Ran 4 checks (1 subtests, 3 tests)
 Expected results: 4
 OK
-""".lstrip('\n')),
-        ('mach', {'verbose': True}, """
+""".lstrip(b'\n')),
+        ('mach', {'verbose': True}, b"""
  0:00.00 SUITE_START: running 3 tests
  0:00.00 TEST_START: test_foo
  0:00.00 TEST_END: OK
  0:00.00 TEST_START: test_bar
  0:00.00 PASS a subtest
  0:00.00 TEST_END: Test OK. Subtests passed 1/1. Unexpected 0
  0:00.00 TEST_START: test_baz
  0:00.00 TEST_END: FAIL
  0:00.00 SUITE_END
 
 suite 1
 ~~~~~~~
-Ran 4 checks (3 tests, 1 subtests)
+Ran 4 checks (1 subtests, 3 tests)
 Expected results: 4
 OK
-""".lstrip('\n')),
+""".lstrip(b'\n')),
     ],
 
     'FAIL': [
-        ('mach', {}, """
+        ('mach', {}, b"""
  0:00.00 SUITE_START: running 3 tests
  0:00.00 TEST_START: test_foo
  0:00.00 TEST_END: FAIL, expected PASS - expected 0 got 1
  0:00.00 TEST_START: test_bar
  0:00.00 TEST_END: Test OK. Subtests passed 0/2. Unexpected 2
 FAIL a subtest - expected 0 got 1
     SimpleTest.is@SimpleTest/SimpleTest.js:312:5
     @caps/tests/mochitest/test_bug246699.html:53:1
 TIMEOUT another subtest
  0:00.00 TEST_START: test_baz
  0:00.00 TEST_END: PASS, expected FAIL
  0:00.00 SUITE_END
 
 suite 1
 ~~~~~~~
-Ran 5 checks (3 tests, 2 subtests)
+Ran 5 checks (2 subtests, 3 tests)
 Expected results: 1
 Unexpected results: 4
   test: 2 (1 fail, 1 pass)
   subtest: 2 (1 fail, 1 timeout)
 
 Unexpected Results
 ------------------
 test_foo
   FAIL test_foo - expected 0 got 1
 test_bar
   FAIL a subtest - expected 0 got 1
     SimpleTest.is@SimpleTest/SimpleTest.js:312:5
     @caps/tests/mochitest/test_bug246699.html:53:1
   TIMEOUT another subtest
 test_baz
   UNEXPECTED-PASS test_baz
-""".lstrip('\n')),
-        ('mach', {'verbose': True}, """
+""".lstrip(b'\n')),
+        ('mach', {'verbose': True}, b"""
  0:00.00 SUITE_START: running 3 tests
  0:00.00 TEST_START: test_foo
  0:00.00 TEST_END: FAIL, expected PASS - expected 0 got 1
  0:00.00 TEST_START: test_bar
  0:00.00 FAIL a subtest - expected 0 got 1
     SimpleTest.is@SimpleTest/SimpleTest.js:312:5
     @caps/tests/mochitest/test_bug246699.html:53:1
  0:00.00 TIMEOUT another subtest
  0:00.00 TEST_END: Test OK. Subtests passed 0/2. Unexpected 2
  0:00.00 TEST_START: test_baz
  0:00.00 TEST_END: PASS, expected FAIL
  0:00.00 SUITE_END
 
 suite 1
 ~~~~~~~
-Ran 5 checks (3 tests, 2 subtests)
+Ran 5 checks (2 subtests, 3 tests)
 Expected results: 1
 Unexpected results: 4
   test: 2 (1 fail, 1 pass)
   subtest: 2 (1 fail, 1 timeout)
 
 Unexpected Results
 ------------------
 test_foo
   FAIL test_foo - expected 0 got 1
 test_bar
   FAIL a subtest - expected 0 got 1
     SimpleTest.is@SimpleTest/SimpleTest.js:312:5
     @caps/tests/mochitest/test_bug246699.html:53:1
   TIMEOUT another subtest
 test_baz
   UNEXPECTED-PASS test_baz
-""".lstrip('\n')),
+""".lstrip(b'\n')),
     ],
 }
 
 
 def ids(test):
     ids = []
     for value in FORMATS[test]:
         args = ", ".join(["{}={}".format(k, v) for k, v in value[1].items()])
diff --git a/testing/mozbase/mozlog/tests/test_logger.py b/testing/mozbase/mozlog/tests/test_logger.py
--- a/testing/mozbase/mozlog/tests/test_logger.py
+++ b/testing/mozbase/mozlog/tests/test_logger.py
@@ -205,25 +205,25 @@ class TestStructuredLogging(unittest.Tes
 
         host, port = self.log_server.server_address
 
         sock = socket.socket()
         sock.connect((host, port))
 
         # Sleeps prevent listener from receiving entire message in a single call
         # to recv in order to test reconstruction of partial messages.
-        sock.sendall(message_string[:8])
+        sock.sendall(message_string[:8].encode())
         time.sleep(.01)
-        sock.sendall(message_string[8:32])
+        sock.sendall(message_string[8:32].encode())
         time.sleep(.01)
-        sock.sendall(message_string[32:64])
+        sock.sendall(message_string[32:64].encode())
         time.sleep(.01)
-        sock.sendall(message_string[64:128])
+        sock.sendall(message_string[64:128].encode())
         time.sleep(.01)
-        sock.sendall(message_string[128:])
+        sock.sendall(message_string[128:].encode())
 
         server_thread.join()
 
 
 class Loggable(mozlog.LoggingMixin):
     """Trivial class inheriting from LoggingMixin"""
     pass
 
diff --git a/testing/mozbase/mozlog/tests/test_structured.py b/testing/mozbase/mozlog/tests/test_structured.py
--- a/testing/mozbase/mozlog/tests/test_structured.py
+++ b/testing/mozbase/mozlog/tests/test_structured.py
@@ -483,45 +483,60 @@ class TestTypeConversions(BaseStructured
                              "time": "1234"})
         self.assert_log_equals({"action": "suite_start",
                                 "tests": {"default": ["1"]},
                                 "time": 1234})
         self.logger.suite_end()
 
     def test_tuple(self):
         self.logger.suite_start([])
-        self.logger.test_start(("\xf0\x90\x8d\x84\xf0\x90\x8c\xb4\xf0\x90\x8d\x83\xf0\x90\x8d\x84",
-                                42, u"\u16a4"))
+        if six.PY3:
+            self.logger.test_start((b"\xf0\x90\x8d\x84\xf0\x90\x8c\xb4\xf0\x90"
+                                    b"\x8d\x83\xf0\x90\x8d\x84".decode(),
+                                    42, u"\u16a4"))
+        else:
+            self.logger.test_start(("\xf0\x90\x8d\x84\xf0\x90\x8c\xb4\xf0\x90"
+                                    "\x8d\x83\xf0\x90\x8d\x84",
+                                    42, u"\u16a4"))
         self.assert_log_equals({"action": "test_start",
                                 "test": (u'\U00010344\U00010334\U00010343\U00010344',
                                          u"42", u"\u16a4")})
         self.logger.suite_end()
 
     def test_non_string_messages(self):
         self.logger.suite_start([])
         self.logger.info(1)
         self.assert_log_equals({"action": "log",
                                 "message": "1",
                                 "level": "INFO"})
         self.logger.info([1, (2, '3'), "s", "s" + chr(255)])
-        self.assert_log_equals({"action": "log",
-                                "message": "[1, (2, '3'), 's', 's\\xff']",
-                                "level": "INFO"})
+        if six.PY3:
+            self.assert_log_equals({"action": "log",
+                                    "message": "[1, (2, '3'), 's', 's\xff']",
+                                    "level": "INFO"})
+        else:
+            self.assert_log_equals({"action": "log",
+                                    "message": "[1, (2, '3'), 's', 's\\xff']",
+                                    "level": "INFO"})
+
         self.logger.suite_end()
 
     def test_utf8str_write(self):
         with mozfile.NamedTemporaryFile() as logfile:
             _fmt = formatters.TbplFormatter()
             _handler = handlers.StreamHandler(logfile, _fmt)
             self.logger.add_handler(_handler)
             self.logger.suite_start([])
             self.logger.info("☺")
             logfile.seek(0)
             data = logfile.readlines()[-1].strip()
-            self.assertEquals(data, "☺")
+            if six.PY3:
+                self.assertEquals(data.decode(), "☺")
+            else:
+                self.assertEquals(data, "☺")
             self.logger.suite_end()
 
     def test_arguments(self):
         self.logger.info(message="test")
         self.assert_log_equals({"action": "log",
                                 "message": "test",
                                 "level": "INFO"})
 
@@ -793,17 +808,17 @@ Unexpected results: 2
         self.logger.test_start("test2")
         self.logger.test_status("test2", "subtest1",
                                 status="TIMEOUT", expected="PASS")
         self.logger.test_end("test2", status="TIMEOUT", expected="OK")
 
         self.set_position()
         self.logger.suite_end()
 
-        self.assertIn("Ran 5 checks (2 tests, 3 subtests)", self.loglines)
+        self.assertIn("Ran 5 checks (3 subtests, 2 tests)", self.loglines)
         self.assertIn("Expected results: 2", self.loglines)
         self.assertIn("""
 Unexpected results: 3
   test: 1 (1 timeout)
   subtest: 2 (1 fail, 1 timeout)
 """.strip(), "\n".join(self.loglines))
 
     def test_summary_ok(self):
@@ -956,47 +971,47 @@ class TestCommandline(unittest.TestCase)
         commandline.add_logging_group(parser)
 
         args = parser.parse_args(["--log-tbpl=%s" % self.logfile.name])
         logger = commandline.setup_logging("test_fmtopts", args, {})
         logger.info("INFO message")
         logger.debug("DEBUG message")
         logger.error("ERROR message")
         # The debug level is not logged by default.
-        self.assertEqual(["INFO message",
-                          "ERROR message"],
+        self.assertEqual([b"INFO message",
+                          b"ERROR message"],
                          self.loglines)
 
     def test_logging_errorlevel(self):
         parser = argparse.ArgumentParser()
         commandline.add_logging_group(parser)
         args = parser.parse_args(
             ["--log-tbpl=%s" % self.logfile.name, "--log-tbpl-level=error"])
         logger = commandline.setup_logging("test_fmtopts", args, {})
         logger.info("INFO message")
         logger.debug("DEBUG message")
         logger.error("ERROR message")
 
         # Only the error level and above were requested.
-        self.assertEqual(["ERROR message"],
+        self.assertEqual([b"ERROR message"],
                          self.loglines)
 
     def test_logging_debuglevel(self):
         parser = argparse.ArgumentParser()
         commandline.add_logging_group(parser)
         args = parser.parse_args(
             ["--log-tbpl=%s" % self.logfile.name, "--log-tbpl-level=debug"])
         logger = commandline.setup_logging("test_fmtopts", args, {})
         logger.info("INFO message")
         logger.debug("DEBUG message")
         logger.error("ERROR message")
         # Requesting a lower log level than default works as expected.
-        self.assertEqual(["INFO message",
-                          "DEBUG message",
-                          "ERROR message"],
+        self.assertEqual([b"INFO message",
+                          b"DEBUG message",
+                          b"ERROR message"],
                          self.loglines)
 
     def test_unused_options(self):
         parser = argparse.ArgumentParser()
         commandline.add_logging_group(parser)
         args = parser.parse_args(["--log-tbpl-level=error"])
         self.assertRaises(ValueError, commandline.setup_logging,
                           "test_fmtopts", args, {})
diff --git a/testing/mozbase/mozprofile/setup.py b/testing/mozbase/mozprofile/setup.py
--- a/testing/mozbase/mozprofile/setup.py
+++ b/testing/mozbase/mozprofile/setup.py
@@ -6,17 +6,17 @@ from __future__ import absolute_import
 
 from setuptools import setup
 
 PACKAGE_NAME = 'mozprofile'
 PACKAGE_VERSION = '2.1.0'
 
 deps = [
     'mozfile>=1.2',
-    'mozlog==3.*',
+    'mozlog~=4.0',
     'six>=1.10.0,<2',
 ]
 
 setup(name=PACKAGE_NAME,
       version=PACKAGE_VERSION,
       description="Library to create and modify Mozilla application profiles",
       long_description="see https://firefox-source-docs.mozilla.org/mozbase/index.html",
       classifiers=['Environment :: Console',
diff --git a/testing/mozbase/mozrunner/setup.py b/testing/mozbase/mozrunner/setup.py
--- a/testing/mozbase/mozrunner/setup.py
+++ b/testing/mozbase/mozrunner/setup.py
@@ -10,17 +10,17 @@ PACKAGE_NAME = 'mozrunner'
 PACKAGE_VERSION = '7.2.0'
 
 desc = """Reliable start/stop/configuration of Mozilla Applications (Firefox, Thunderbird, etc.)"""
 
 deps = [
     'mozdevice>=1.1.6',
     'mozfile>=1.2',
     'mozinfo>=0.7,<2',
-    'mozlog==3.*',
+    'mozlog~=4.0',
     'mozprocess>=0.23,<1',
     'mozprofile~=2.1',
     'six>=1.10.0,<2',
 ]
 
 EXTRAS_REQUIRE = {'crash': ['mozcrash >= 1.0']}
 
 
diff --git a/testing/mozbase/mozversion/setup.py b/testing/mozbase/mozversion/setup.py
--- a/testing/mozbase/mozversion/setup.py
+++ b/testing/mozbase/mozversion/setup.py
@@ -18,15 +18,15 @@ setup(name='mozversion',
       keywords='mozilla',
       author='Mozilla Automation and Testing Team',
       author_email='tools@lists.mozilla.org',
       url='https://wiki.mozilla.org/Auto-tools/Projects/Mozbase',
       license='MPL',
       packages=['mozversion'],
       include_package_data=True,
       zip_safe=False,
-      install_requires=['mozlog ~= 3.0',
+      install_requires=['mozlog ~= 4.0',
                         'six >= 1.10.0'],
       entry_points="""
       # -*- Entry points: -*-
       [console_scripts]
       mozversion = mozversion:cli
       """)
