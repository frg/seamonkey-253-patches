# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1517594349 28800
# Node ID af6cc52c9b8b737e4734710ad6786dacdb59d923
# Parent  3830cba07c1a3e6ad784581a2a426d13acfebf4e
Bug 1435325: Cache URelativeDateTimeFormatter in Intl.RelativeTimeFormat. r=gandalf

diff --git a/js/src/builtin/intl/RelativeTimeFormat.cpp b/js/src/builtin/intl/RelativeTimeFormat.cpp
--- a/js/src/builtin/intl/RelativeTimeFormat.cpp
+++ b/js/src/builtin/intl/RelativeTimeFormat.cpp
@@ -103,17 +103,18 @@ RelativeTimeFormat(JSContext* cx, unsign
     }
 
     Rooted<RelativeTimeFormatObject*> relativeTimeFormat(cx);
     relativeTimeFormat = NewObjectWithGivenProto<RelativeTimeFormatObject>(cx, proto);
     if (!relativeTimeFormat)
         return false;
 
     relativeTimeFormat->setReservedSlot(RelativeTimeFormatObject::INTERNALS_SLOT, NullValue());
-    relativeTimeFormat->setReservedSlot(RelativeTimeFormatObject::URELATIVE_TIME_FORMAT_SLOT, PrivateValue(nullptr));
+    relativeTimeFormat->setReservedSlot(RelativeTimeFormatObject::URELATIVE_TIME_FORMAT_SLOT,
+                                        PrivateValue(nullptr));
 
     HandleValue locales = args.get(0);
     HandleValue options = args.get(1);
 
     // Step 3.
     if (!intl::InitializeObject(cx, relativeTimeFormat, cx->names().InitializeRelativeTimeFormat,
                                 locales, options))
     {
@@ -124,18 +125,18 @@ RelativeTimeFormat(JSContext* cx, unsign
     return true;
 }
 
 void
 js::RelativeTimeFormatObject::finalize(FreeOp* fop, JSObject* obj)
 {
     MOZ_ASSERT(fop->onActiveCooperatingThread());
 
-    const Value& slot =
-        obj->as<RelativeTimeFormatObject>().getReservedSlot(RelativeTimeFormatObject::URELATIVE_TIME_FORMAT_SLOT);
+    constexpr auto RT_FORMAT_SLOT = RelativeTimeFormatObject::URELATIVE_TIME_FORMAT_SLOT;
+    const Value& slot = obj->as<RelativeTimeFormatObject>().getReservedSlot(RT_FORMAT_SLOT);
     if (URelativeDateTimeFormatter* rtf = static_cast<URelativeDateTimeFormatter*>(slot.toPrivate()))
         ureldatefmt_close(rtf);
 }
 
 JSObject*
 js::CreateRelativeTimeFormatPrototype(JSContext* cx, HandleObject Intl,
                                       Handle<GlobalObject*> global)
 {
@@ -205,16 +206,65 @@ js::intl_RelativeTimeFormat_availableLoc
     // We're going to use ULocale availableLocales as per ICU recommendation:
     // https://ssl.icu-project.org/trac/ticket/12756
     if (!GetAvailableLocales(cx, uloc_countAvailable, uloc_getAvailable, &result))
         return false;
     args.rval().set(result);
     return true;
 }
 
+/**
+ * Returns a new URelativeDateTimeFormatter with the locale and options of the
+ * given RelativeTimeFormatObject.
+ */
+static URelativeDateTimeFormatter*
+NewURelativeDateTimeFormatter(JSContext* cx, Handle<RelativeTimeFormatObject*> relativeTimeFormat)
+{
+    RootedObject internals(cx, intl::GetInternalsObject(cx, relativeTimeFormat));
+    if (!internals)
+        return nullptr;
+
+    RootedValue value(cx);
+
+    if (!GetProperty(cx, internals, internals, cx->names().locale, &value))
+        return nullptr;
+    JSAutoByteString locale(cx, value.toString());
+    if (!locale)
+        return nullptr;
+
+    if (!GetProperty(cx, internals, internals, cx->names().style, &value))
+        return nullptr;
+
+    UDateRelativeDateTimeFormatterStyle relDateTimeStyle;
+    {
+        JSLinearString* style = value.toString()->ensureLinear(cx);
+        if (!style)
+            return nullptr;
+
+        if (StringEqualsAscii(style, "short")) {
+            relDateTimeStyle = UDAT_STYLE_SHORT;
+        } else if (StringEqualsAscii(style, "narrow")) {
+            relDateTimeStyle = UDAT_STYLE_NARROW;
+        } else {
+            MOZ_ASSERT(StringEqualsAscii(style, "long"));
+            relDateTimeStyle = UDAT_STYLE_LONG;
+        }
+    }
+
+    UErrorCode status = U_ZERO_ERROR;
+    URelativeDateTimeFormatter* rtf =
+        ureldatefmt_open(IcuLocale(locale.ptr()), nullptr, relDateTimeStyle,
+                         UDISPCTX_CAPITALIZATION_FOR_STANDALONE, &status);
+    if (U_FAILURE(status)) {
+        intl::ReportInternalError(cx);
+        return nullptr;
+    }
+    return rtf;
+}
+
 enum class RelativeTimeType
 {
     /**
      * Only strings with numeric components like `1 day ago`.
      */
     Numeric,
     /**
      * Natural-language strings like `yesterday` when possible,
@@ -222,68 +272,37 @@ enum class RelativeTimeType
      */
     Text,
 };
 
 bool
 js::intl_FormatRelativeTime(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
-    MOZ_ASSERT(args.length() == 3);
+    MOZ_ASSERT(args.length() == 4);
 
-    RootedObject relativeTimeFormat(cx, &args[0].toObject());
+    Rooted<RelativeTimeFormatObject*> relativeTimeFormat(cx);
+    relativeTimeFormat = &args[0].toObject().as<RelativeTimeFormatObject>();
 
     double t = args[1].toNumber();
 
-    RootedObject internals(cx, intl::GetInternalsObject(cx, relativeTimeFormat));
-    if (!internals)
-        return false;
-
-    RootedValue value(cx);
-
-    if (!GetProperty(cx, internals, internals, cx->names().locale, &value))
-        return false;
-    JSAutoByteString locale(cx, value.toString());
-    if (!locale)
-        return false;
-
-    if (!GetProperty(cx, internals, internals, cx->names().style, &value))
-        return false;
-
-    UDateRelativeDateTimeFormatterStyle relDateTimeStyle;
-    {
-        JSLinearString* style = value.toString()->ensureLinear(cx);
-        if (!style)
-            return false;
+    // ICU doesn't handle -0 well: work around this by converting it to +0.
+    // See: http://bugs.icu-project.org/trac/ticket/12936
+    if (IsNegativeZero(t))
+        t = +0.0;
 
-        if (StringEqualsAscii(style, "short")) {
-            relDateTimeStyle = UDAT_STYLE_SHORT;
-        } else if (StringEqualsAscii(style, "narrow")) {
-            relDateTimeStyle = UDAT_STYLE_NARROW;
-        } else {
-            MOZ_ASSERT(StringEqualsAscii(style, "long"));
-            relDateTimeStyle = UDAT_STYLE_LONG;
-        }
-    }
-
-    if (!GetProperty(cx, internals, internals, cx->names().type, &value))
-        return false;
-
-    RelativeTimeType relDateTimeType;
-    {
-        JSLinearString* type = value.toString()->ensureLinear(cx);
-        if (!type)
+    // Obtain a cached URelativeDateTimeFormatter object.
+    constexpr auto RT_FORMAT_SLOT = RelativeTimeFormatObject::URELATIVE_TIME_FORMAT_SLOT;
+    void* priv = relativeTimeFormat->getReservedSlot(RT_FORMAT_SLOT).toPrivate();
+    URelativeDateTimeFormatter* rtf = static_cast<URelativeDateTimeFormatter*>(priv);
+    if (!rtf) {
+        rtf = NewURelativeDateTimeFormatter(cx, relativeTimeFormat);
+        if (!rtf)
             return false;
-
-        if (StringEqualsAscii(type, "text")) {
-            relDateTimeType = RelativeTimeType::Text;
-        } else {
-            MOZ_ASSERT(StringEqualsAscii(type, "numeric"));
-            relDateTimeType = RelativeTimeType::Numeric;
-        }
+        relativeTimeFormat->setReservedSlot(RT_FORMAT_SLOT, PrivateValue(rtf));
     }
 
     URelativeDateTimeUnit relDateTimeUnit;
     {
         JSLinearString* unit = args[2].toString()->ensureLinear(cx);
         if (!unit)
             return false;
 
@@ -302,32 +321,30 @@ js::intl_FormatRelativeTime(JSContext* c
         } else if (StringEqualsAscii(unit, "quarter")) {
             relDateTimeUnit = UDAT_REL_UNIT_QUARTER;
         } else {
             MOZ_ASSERT(StringEqualsAscii(unit, "year"));
             relDateTimeUnit = UDAT_REL_UNIT_YEAR;
         }
     }
 
-    // ICU doesn't handle -0 well: work around this by converting it to +0.
-    // See: http://bugs.icu-project.org/trac/ticket/12936
-    if (IsNegativeZero(t))
-        t = +0.0;
+    RelativeTimeType relDateTimeType;
+    {
+        JSLinearString* type = args[3].toString()->ensureLinear(cx);
+        if (!type)
+            return false;
 
-    UErrorCode status = U_ZERO_ERROR;
-    URelativeDateTimeFormatter* rtf =
-        ureldatefmt_open(IcuLocale(locale.ptr()), nullptr, relDateTimeStyle,
-                         UDISPCTX_CAPITALIZATION_FOR_STANDALONE, &status);
-    if (U_FAILURE(status)) {
-        intl::ReportInternalError(cx);
-        return false;
+        if (StringEqualsAscii(type, "text")) {
+            relDateTimeType = RelativeTimeType::Text;
+        } else {
+            MOZ_ASSERT(StringEqualsAscii(type, "numeric"));
+            relDateTimeType = RelativeTimeType::Numeric;
+        }
     }
 
-    ScopedICUObject<URelativeDateTimeFormatter, ureldatefmt_close> closeRelativeTimeFormat(rtf);
-
     JSString* str =
         CallICU(cx, [rtf, t, relDateTimeUnit, relDateTimeType](UChar* chars, int32_t size,
                                                                UErrorCode* status)
         {
             auto fmt = relDateTimeType == RelativeTimeType::Text
                        ? ureldatefmt_format
                        : ureldatefmt_formatNumeric;
             return fmt(rtf, t, relDateTimeUnit, chars, size, status);
diff --git a/js/src/builtin/intl/RelativeTimeFormat.h b/js/src/builtin/intl/RelativeTimeFormat.h
--- a/js/src/builtin/intl/RelativeTimeFormat.h
+++ b/js/src/builtin/intl/RelativeTimeFormat.h
@@ -53,17 +53,18 @@ extern MOZ_MUST_USE bool
 intl_RelativeTimeFormat_availableLocales(JSContext* cx, unsigned argc, JS::Value* vp);
 
 /**
  * Returns a relative time as a string formatted according to the effective
  * locale and the formatting options of the given RelativeTimeFormat.
  *
  * t should be a number representing a number to be formatted.
  * unit should be "second", "minute", "hour", "day", "week", "month", "quarter", or "year".
+ * type should be "text" or "numeric".
  *
- * Usage: formatted = intl_FormatRelativeTime(relativeTimeFormat, t, unit)
+ * Usage: formatted = intl_FormatRelativeTime(relativeTimeFormat, t, unit, type)
  */
 extern MOZ_MUST_USE bool
 intl_FormatRelativeTime(JSContext* cx, unsigned argc, JS::Value* vp);
 
 } // namespace js
 
 #endif /* builtin_intl_RelativeTimeFormat_h */
diff --git a/js/src/builtin/intl/RelativeTimeFormat.js b/js/src/builtin/intl/RelativeTimeFormat.js
--- a/js/src/builtin/intl/RelativeTimeFormat.js
+++ b/js/src/builtin/intl/RelativeTimeFormat.js
@@ -170,17 +170,17 @@ function Intl_RelativeTimeFormat_format(
     // Step 1.
     let relativeTimeFormat = this;
 
     // Step 2.
     if (!IsObject(relativeTimeFormat) || !IsRelativeTimeFormat(relativeTimeFormat))
         ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "RelativeTimeFormat", "format", "RelativeTimeFormat");
 
     // Ensure the RelativeTimeFormat internals are resolved.
-    getRelativeTimeFormatInternals(relativeTimeFormat);
+    var internals = getRelativeTimeFormatInternals(relativeTimeFormat);
 
     // Step 3.
     let t = ToNumber(value);
 
     // Step 4.
     let u = ToString(unit);
 
     switch (u) {
@@ -193,17 +193,17 @@ function Intl_RelativeTimeFormat_format(
       case "quarter":
       case "year":
         break;
       default:
         ThrowRangeError(JSMSG_INVALID_OPTION_VALUE, "unit", u);
     }
 
     // Step 5.
-    return intl_FormatRelativeTime(relativeTimeFormat, t, u);
+    return intl_FormatRelativeTime(relativeTimeFormat, t, u, internals.type);
 }
 
 /**
  * Returns the resolved options for a PluralRules object.
  *
  * Spec: ECMAScript 402 API, RelativeTimeFormat, 1.4.4.
  */
 function Intl_RelativeTimeFormat_resolvedOptions() {
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -2495,17 +2495,17 @@ static const JSFunctionSpec intrinsic_fu
     JS_FN("intl_NumberFormat_availableLocales", intl_NumberFormat_availableLocales, 0,0),
     JS_FN("intl_numberingSystem", intl_numberingSystem, 1,0),
     JS_FN("intl_patternForSkeleton", intl_patternForSkeleton, 2,0),
     JS_FN("intl_patternForStyle", intl_patternForStyle, 3,0),
     JS_FN("intl_PluralRules_availableLocales", intl_PluralRules_availableLocales, 0,0),
     JS_FN("intl_GetPluralCategories", intl_GetPluralCategories, 1, 0),
     JS_FN("intl_SelectPluralRule", intl_SelectPluralRule, 2,0),
     JS_FN("intl_RelativeTimeFormat_availableLocales", intl_RelativeTimeFormat_availableLocales, 0,0),
-    JS_FN("intl_FormatRelativeTime", intl_FormatRelativeTime, 3,0),
+    JS_FN("intl_FormatRelativeTime", intl_FormatRelativeTime, 4,0),
     JS_FN("intl_toLocaleLowerCase", intl_toLocaleLowerCase, 2,0),
     JS_FN("intl_toLocaleUpperCase", intl_toLocaleUpperCase, 2,0),
 
     JS_INLINABLE_FN("IsCollator",
                     intrinsic_IsInstanceOfBuiltin<CollatorObject>, 1,0,
                     IntlIsCollator),
     JS_INLINABLE_FN("IsDateTimeFormat",
                     intrinsic_IsInstanceOfBuiltin<DateTimeFormatObject>, 1,0,
