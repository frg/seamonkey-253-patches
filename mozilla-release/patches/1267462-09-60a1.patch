# HG changeset patch
# User Daniel Holbert <dholbert@cs.stanford.edu>
# Date 1518131823 28800
#      Thu Feb 08 15:17:03 2018 -0800
# Node ID d28772595a77be6de5632e0a2176325ec229f154
# Parent  b18dea201a45de5059bb2e04c146824ea54b2f76
Bug 1267462 part 9: Fix IsCrossSizeDefinite() helper-method to use logical axes & coords. r=mats

MozReview-Commit-ID: Eda6ltmcDgQ

diff --git a/layout/generic/nsFlexContainerFrame.cpp b/layout/generic/nsFlexContainerFrame.cpp
--- a/layout/generic/nsFlexContainerFrame.cpp
+++ b/layout/generic/nsFlexContainerFrame.cpp
@@ -1291,31 +1291,36 @@ nsFlexContainerFrame::GenerateFlexItemFo
   // require us to reflow the item to measure content height)
   ResolveAutoFlexBasisAndMinSize(aPresContext, *item,
                                  childRI, aAxisTracker);
   return item;
 }
 
 // Static helper-functions for ResolveAutoFlexBasisAndMinSize():
 // -------------------------------------------------------------
-// Indicates whether the cross-size property is set to something definite.
-// The logic here should be similar to the logic for isAutoWidth/isAutoHeight
+// Indicates whether the cross-size property is set to something definite,
+// for the purpose of intrinsic ratio calculations.
+// The logic here should be similar to the logic for isAutoISize/isAutoBSize
 // in nsFrame::ComputeSizeWithIntrinsicDimensions().
 static bool
 IsCrossSizeDefinite(const ReflowInput& aItemReflowInput,
                     const FlexboxAxisTracker& aAxisTracker)
 {
   const nsStylePosition* pos = aItemReflowInput.mStylePosition;
-  if (aAxisTracker.IsCrossAxisHorizontal()) {
-    return pos->mWidth.GetUnit() != eStyleUnit_Auto;
+  const WritingMode containerWM = aAxisTracker.GetWritingMode();
+
+  if (aAxisTracker.IsColumnOriented()) {
+    // Column-oriented means cross axis is container's inline axis.
+    return pos->ISize(containerWM).GetUnit() != eStyleUnit_Auto;
   }
-  // else, vertical. (We need to use IsAutoHeight() to catch e.g. %-height
-  // applied to indefinite-height containing block, which counts as auto.)
-  nscoord cbHeight = aItemReflowInput.mCBReflowInput->ComputedHeight();
-  return !nsLayoutUtils::IsAutoHeight(pos->mHeight, cbHeight);
+  // Else, we're row-oriented, which means cross axis is container's block
+  // axis. We need to use IsAutoBSize() to catch e.g. %-BSize applied to
+  // indefinite container BSize, which counts as auto.
+  nscoord cbBSize = aItemReflowInput.mCBReflowInput->ComputedBSize();
+  return !nsLayoutUtils::IsAutoBSize(pos->BSize(containerWM), cbBSize);
 }
 
 // If aFlexItem has a definite cross size, this function returns it, for usage
 // (in combination with an intrinsic ratio) for resolving the item's main size
 // or main min-size.
 //
 // The parameter "aMinSizeFallback" indicates whether we should fall back to
 // returning the cross min-size, when the cross size is indefinite. (This param
