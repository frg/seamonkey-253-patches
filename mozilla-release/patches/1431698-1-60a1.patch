# HG changeset patch
# User Aanchal <aanchal138@gmail.com>
# Date 1516974177 -19800
# Node ID c3e6151076e305a94e01e359135952a17ba87f08
# Parent  e2dc4d2e3b81e8e50fea6be76e783a91e9ba5263
Bug 1431698 - Move the IntConversion enumerations from MacroAssembler.h to IonTypes.h. r=bbouvier

diff --git a/js/src/jit/IonTypes.h b/js/src/jit/IonTypes.h
--- a/js/src/jit/IonTypes.h
+++ b/js/src/jit/IonTypes.h
@@ -386,16 +386,32 @@ class SimdConstant {
         uint32_t hash = mozilla::HashBytes(&val.u, sizeof(val.u));
         return mozilla::AddToHash(hash, val.type_);
     }
     static bool match(const SimdConstant& lhs, const SimdConstant& rhs) {
         return lhs == rhs;
     }
 };
 
+enum IntConversionBehavior {
+    // These two try to convert the input to an int32 using ToNumber and
+    // will fail if the resulting int32 isn't strictly equal to the input.
+    IntConversion_Normal,
+    IntConversion_NegativeZeroCheck,
+    // These two will convert the input to an int32 with loss of precision.
+    IntConversion_Truncate,
+    IntConversion_ClampToUint8,
+};
+
+enum IntConversionInputKind {
+    IntConversion_NumbersOnly,
+    IntConversion_NumbersOrBoolsOnly,
+    IntConversion_Any
+};
+
 // The ordering of this enumeration is important: Anything < Value is a
 // specialized type. Furthermore, anything < String has trivial conversion to
 // a number.
 enum class MIRType
 {
     Undefined,
     Null,
     Boolean,
diff --git a/js/src/jit/Lowering.cpp b/js/src/jit/Lowering.cpp
--- a/js/src/jit/Lowering.cpp
+++ b/js/src/jit/Lowering.cpp
@@ -2194,23 +2194,23 @@ LIRGenerator::visitToNumberInt32(MToNumb
             new(alloc()) LValueToInt32(useBox(opd), tempDouble(), temp(), LValueToInt32::NORMAL);
         assignSnapshot(lir, Bailout_NonPrimitiveInput);
         define(lir, convert);
         assignSafepoint(lir, convert);
         break;
       }
 
       case MIRType::Null:
-        MOZ_ASSERT(convert->conversion() == MacroAssembler::IntConversion_Any);
+        MOZ_ASSERT(convert->conversion() == IntConversion_Any);
         define(new(alloc()) LInteger(0), convert);
         break;
 
       case MIRType::Boolean:
-        MOZ_ASSERT(convert->conversion() == MacroAssembler::IntConversion_Any ||
-                   convert->conversion() == MacroAssembler::IntConversion_NumbersOrBoolsOnly);
+        MOZ_ASSERT(convert->conversion() == IntConversion_Any ||
+                   convert->conversion() == IntConversion_NumbersOrBoolsOnly);
         redefine(convert, opd);
         break;
 
       case MIRType::Int32:
         redefine(convert, opd);
         break;
 
       case MIRType::Float32:
diff --git a/js/src/jit/MIR.cpp b/js/src/jit/MIR.cpp
--- a/js/src/jit/MIR.cpp
+++ b/js/src/jit/MIR.cpp
@@ -4315,24 +4315,24 @@ MResumePoint::isRecoverableOperand(MUse*
 
 MDefinition*
 MToNumberInt32::foldsTo(TempAllocator& alloc)
 {
     MDefinition* input = getOperand(0);
 
     // Fold this operation if the input operand is constant.
     if (input->isConstant()) {
-        DebugOnly<MacroAssembler::IntConversionInputKind> convert = conversion();
+        DebugOnly<IntConversionInputKind> convert = conversion();
         switch (input->type()) {
           case MIRType::Null:
-            MOZ_ASSERT(convert == MacroAssembler::IntConversion_Any);
+            MOZ_ASSERT(convert == IntConversion_Any);
             return MConstant::New(alloc, Int32Value(0));
           case MIRType::Boolean:
-            MOZ_ASSERT(convert == MacroAssembler::IntConversion_Any ||
-                       convert == MacroAssembler::IntConversion_NumbersOrBoolsOnly);
+            MOZ_ASSERT(convert == IntConversion_Any ||
+                       convert == IntConversion_NumbersOrBoolsOnly);
             return MConstant::New(alloc, Int32Value(input->toConstant()->toBoolean()));
           case MIRType::Int32:
             return MConstant::New(alloc, Int32Value(input->toConstant()->toInt32()));
           case MIRType::Float32:
           case MIRType::Double:
             int32_t ival;
             // Only the value within the range of Int32 can be substituted as constant.
             if (mozilla::NumberIsInt32(input->toConstant()->numberToDouble(), &ival))
diff --git a/js/src/jit/MIR.h b/js/src/jit/MIR.h
--- a/js/src/jit/MIR.h
+++ b/js/src/jit/MIR.h
@@ -5679,20 +5679,20 @@ class MInt64ToFloatingPoint
 // If the input is not primitive at runtime, a bailout occurs. If the input
 // cannot be converted to an int32 without loss (i.e. 5.5 or undefined) then a
 // bailout occurs.
 class MToNumberInt32
   : public MUnaryInstruction,
     public ToInt32Policy::Data
 {
     bool canBeNegativeZero_;
-    MacroAssembler::IntConversionInputKind conversion_;
-
-    explicit MToNumberInt32(MDefinition* def, MacroAssembler::IntConversionInputKind conversion
-                                              = MacroAssembler::IntConversion_Any)
+    IntConversionInputKind conversion_;
+
+    explicit MToNumberInt32(MDefinition* def, IntConversionInputKind conversion
+                                              = IntConversion_Any)
       : MUnaryInstruction(classOpcode, def),
         canBeNegativeZero_(true),
         conversion_(conversion)
     {
         setResultType(MIRType::Int32);
         setMovable();
 
         // An object might have "valueOf", which means it is effectful.
@@ -5712,17 +5712,17 @@ class MToNumberInt32
 
     bool canBeNegativeZero() const {
         return canBeNegativeZero_;
     }
     void setCanBeNegativeZero(bool negativeZero) {
         canBeNegativeZero_ = negativeZero;
     }
 
-    MacroAssembler::IntConversionInputKind conversion() const {
+    IntConversionInputKind conversion() const {
         return conversion_;
     }
 
     bool congruentTo(const MDefinition* ins) const override {
         if (!ins->isToNumberInt32() || ins->toToNumberInt32()->conversion() != conversion())
             return false;
         return congruentIfOperandsEqual(ins);
     }
diff --git a/js/src/jit/MacroAssembler.h b/js/src/jit/MacroAssembler.h
--- a/js/src/jit/MacroAssembler.h
+++ b/js/src/jit/MacroAssembler.h
@@ -27,16 +27,17 @@
 # include "jit/mips64/MacroAssembler-mips64.h"
 #elif defined(JS_CODEGEN_NONE)
 # include "jit/none/MacroAssembler-none.h"
 #else
 # error "Unknown architecture!"
 #endif
 #include "jit/AtomicOp.h"
 #include "jit/IonInstrumentation.h"
+#include "jit/IonTypes.h"
 #include "jit/JitCompartment.h"
 #include "jit/VMFunctions.h"
 #include "vm/ProxyObject.h"
 #include "vm/Shape.h"
 #include "vm/TypedArrayObject.h"
 #include "vm/UnboxedObject.h"
 
 using mozilla::FloatingPoint;
@@ -2418,33 +2419,16 @@ class MacroAssembler : public MacroAssem
                                                        const ConstantOrRegister& src,
                                                        FloatRegister output, Label* fail)
     {
         return convertConstantOrRegisterToFloatingPoint(cx, src, output, fail, MIRType::Float32);
     }
     void convertTypedOrValueToFloat(TypedOrValueRegister src, FloatRegister output, Label* fail) {
         convertTypedOrValueToFloatingPoint(src, output, fail, MIRType::Float32);
     }
-
-    enum IntConversionBehavior {
-        // These two try to convert the input to an int32 using ToNumber and
-        // will fail if the resulting int32 isn't strictly equal to the input.
-        IntConversion_Normal,
-        IntConversion_NegativeZeroCheck,
-        // These two will convert the input to an int32 with loss of precision.
-        IntConversion_Truncate,
-        IntConversion_ClampToUint8,
-    };
-
-    enum IntConversionInputKind {
-        IntConversion_NumbersOnly,
-        IntConversion_NumbersOrBoolsOnly,
-        IntConversion_Any
-    };
-
     //
     // Functions for converting values to int.
     //
     void convertDoubleToInt(FloatRegister src, Register output, FloatRegister temp,
                             Label* truncateFail, Label* fail, IntConversionBehavior behavior);
 
     // Strings may be handled by providing labels to jump to when the behavior
     // is truncation or clamping. The subroutine, usually an OOL call, is
diff --git a/js/src/jit/TypePolicy.cpp b/js/src/jit/TypePolicy.cpp
--- a/js/src/jit/TypePolicy.cpp
+++ b/js/src/jit/TypePolicy.cpp
@@ -230,22 +230,22 @@ ComparePolicy::adjustInputs(TempAllocato
             if (compare->compareType() == MCompare::Compare_DoubleMaybeCoerceLHS && i == 0)
                 convert = MToFPInstruction::NonNullNonStringPrimitives;
             else if (compare->compareType() == MCompare::Compare_DoubleMaybeCoerceRHS && i == 1)
                 convert = MToFPInstruction::NonNullNonStringPrimitives;
             replace = MToFloat32::New(alloc, in, convert);
             break;
           }
           case MIRType::Int32: {
-            MacroAssembler::IntConversionInputKind convert = MacroAssembler::IntConversion_NumbersOnly;
+            IntConversionInputKind convert = IntConversion_NumbersOnly;
             if (compare->compareType() == MCompare::Compare_Int32MaybeCoerceBoth ||
                 (compare->compareType() == MCompare::Compare_Int32MaybeCoerceLHS && i == 0) ||
                 (compare->compareType() == MCompare::Compare_Int32MaybeCoerceRHS && i == 1))
             {
-                convert = MacroAssembler::IntConversion_NumbersOrBoolsOnly;
+                convert = IntConversion_NumbersOrBoolsOnly;
             }
             replace = MToNumberInt32::New(alloc, in, convert);
             break;
           }
           case MIRType::Object:
             replace = MUnbox::New(alloc, in, MIRType::Object, MUnbox::Infallible);
             break;
           case MIRType::String:
@@ -723,17 +723,17 @@ ToDoublePolicy::staticAdjustInputs(TempA
     return true;
 }
 
 bool
 ToInt32Policy::staticAdjustInputs(TempAllocator& alloc, MInstruction* ins)
 {
     MOZ_ASSERT(ins->isToNumberInt32() || ins->isTruncateToInt32());
 
-    MacroAssembler::IntConversionInputKind conversion = MacroAssembler::IntConversion_Any;
+    IntConversionInputKind conversion = IntConversion_Any;
     if (ins->isToNumberInt32())
         conversion = ins->toToNumberInt32()->conversion();
 
     MDefinition* in = ins->getOperand(0);
     switch (in->type()) {
       case MIRType::Int32:
       case MIRType::Float32:
       case MIRType::Double:
@@ -742,24 +742,24 @@ ToInt32Policy::staticAdjustInputs(TempAl
         return true;
       case MIRType::Undefined:
         // No need for boxing when truncating.
         if (ins->isTruncateToInt32())
             return true;
         break;
       case MIRType::Null:
         // No need for boxing, when we will convert.
-        if (conversion == MacroAssembler::IntConversion_Any)
+        if (conversion == IntConversion_Any)
             return true;
         break;
       case MIRType::Boolean:
         // No need for boxing, when we will convert.
-        if (conversion == MacroAssembler::IntConversion_Any)
+        if (conversion == IntConversion_Any)
             return true;
-        if (conversion == MacroAssembler::IntConversion_NumbersOrBoolsOnly)
+        if (conversion == IntConversion_NumbersOrBoolsOnly)
             return true;
         break;
       case MIRType::Object:
       case MIRType::String:
       case MIRType::Symbol:
         // Objects might be effectful. Symbols give TypeError.
         break;
       default:
@@ -852,17 +852,17 @@ SimdShufflePolicy::adjustInputs(TempAllo
         MOZ_ASSERT(ins->getOperand(i)->type() == ins->typePolicySpecialization());
 
     // Next inputs are the lanes, which need to be int32
     for (unsigned i = 0; i < s->numLanes(); i++) {
         MDefinition* in = ins->getOperand(s->numVectors() + i);
         if (in->type() == MIRType::Int32)
             continue;
 
-        auto* replace = MToNumberInt32::New(alloc, in, MacroAssembler::IntConversion_NumbersOnly);
+        auto* replace = MToNumberInt32::New(alloc, in, IntConversion_NumbersOnly);
         ins->block()->insertBefore(ins, replace);
         ins->replaceOperand(s->numVectors() + i, replace);
         if (!replace->typePolicy()->adjustInputs(alloc, replace))
             return false;
     }
 
     return true;
 }
