# HG changeset patch
# User Anthony Ramine <n.oxyde@gmail.com>
# Date 1519751863 18000
# Node ID dcf2598e65e1fa7a882c2887cf16d7d5c1418f4b
# Parent  63b0fd7bc93136bc1300f58cdf888d1d4a4b061d
servo: Merge #20131 - Replace NonNegativeLengthOrNumber by a specific type for -moz-tab-size (from servo:moz-tab-size); r=emilio

This is the only use of this type.

Source-Repo: https://github.com/servo/servo
Source-Revision: 030509e66b9d3432c112bb5639f446535749963e

diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -59,16 +59,17 @@ use std::mem::{forget, uninitialized, tr
 use std::{cmp, ops, ptr};
 use values::{self, CustomIdent, Either, KeyframesName, None_};
 use values::computed::{NonNegativeLength, ToComputedValue, Percentage};
 use values::computed::font::{FontSize, SingleFontFamily};
 use values::computed::effects::{BoxShadow, Filter, SimpleShadow};
 use values::computed::outline::OutlineStyle;
 use values::generics::column::ColumnCount;
 use values::generics::position::ZIndex;
+use values::generics::text::MozTabSize;
 use values::generics::transform::TransformStyle;
 use computed_values::border_style;
 
 pub mod style_structs {
     % for style_struct in data.style_structs:
     pub use super::${style_struct.gecko_struct_name} as ${style_struct.name};
     % endfor
 }
@@ -4860,35 +4861,31 @@ fn static_assert() {
         })
     }
 
     ${impl_non_negative_length('_webkit_text_stroke_width',
                                'mWebkitTextStrokeWidth')}
 
     #[allow(non_snake_case)]
     pub fn set__moz_tab_size(&mut self, v: longhands::_moz_tab_size::computed_value::T) {
-        use values::Either;
-
         match v {
-            Either::First(non_negative_number) => {
+            MozTabSize::Number(non_negative_number) => {
                 self.gecko.mTabSize.set_value(CoordDataValue::Factor(non_negative_number.0));
             }
-            Either::Second(non_negative_length) => {
+            MozTabSize::Length(non_negative_length) => {
                 self.gecko.mTabSize.set(non_negative_length);
             }
         }
     }
 
     #[allow(non_snake_case)]
     pub fn clone__moz_tab_size(&self) -> longhands::_moz_tab_size::computed_value::T {
-        use values::Either;
-
         match self.gecko.mTabSize.as_value() {
-            CoordDataValue::Coord(coord) => Either::Second(Au(coord).into()),
-            CoordDataValue::Factor(number) => Either::First(From::from(number)),
+            CoordDataValue::Coord(coord) => MozTabSize::Length(Au(coord).into()),
+            CoordDataValue::Factor(number) => MozTabSize::Number(From::from(number)),
             _ => unreachable!(),
         }
     }
 
     <%call expr="impl_coord_copy('_moz_tab_size', 'mTabSize')"></%call>
 </%self:impl_trait>
 
 <%self:impl_trait style_struct_name="Text"
diff --git a/servo/components/style/properties/longhand/inherited_text.mako.rs b/servo/components/style/properties/longhand/inherited_text.mako.rs
--- a/servo/components/style/properties/longhand/inherited_text.mako.rs
+++ b/servo/components/style/properties/longhand/inherited_text.mako.rs
@@ -482,21 +482,23 @@
     initial_specified_value="specified::Color::currentcolor()",
     products="gecko",
     animation_value_type="AnimatedColor",
     ignored_when_colors_disabled=True,
     spec="https://drafts.csswg.org/css-text-decor/#propdef-text-emphasis-color",
 )}
 
 ${helpers.predefined_type(
-    "-moz-tab-size", "length::NonNegativeLengthOrNumber",
-    "::values::Either::First(From::from(8.0))",
-    products="gecko", animation_value_type="::values::computed::length::NonNegativeLengthOrNumber",
-    spec="https://drafts.csswg.org/css-text-3/#tab-size-property")}
-
+    "-moz-tab-size",
+    "MozTabSize",
+    "generics::text::MozTabSize::Number(From::from(8.0))",
+    products="gecko",
+    animation_value_type="AnimatedMozTabSize",
+    spec="https://drafts.csswg.org/css-text-3/#tab-size-property",
+)}
 
 // CSS Compatibility
 // https://compat.spec.whatwg.org
 ${helpers.predefined_type(
     "-webkit-text-fill-color",
     "Color",
     "computed_value::T::currentcolor()",
     products="gecko",
diff --git a/servo/components/style/values/computed/length.rs b/servo/components/style/values/computed/length.rs
--- a/servo/components/style/values/computed/length.rs
+++ b/servo/components/style/values/computed/length.rs
@@ -10,17 +10,16 @@ use ordered_float::NotNaN;
 use properties::LonghandId;
 use std::fmt::{self, Write};
 use std::ops::{Add, Neg};
 use style_traits::{CssWriter, ToCss};
 use style_traits::values::specified::AllowedNumericType;
 use super::{Number, ToComputedValue, Context, Percentage};
 use values::{Auto, CSSFloat, Either, Normal, specified};
 use values::animated::{Animate, Procedure, ToAnimatedValue, ToAnimatedZero};
-use values::computed::NonNegativeNumber;
 use values::distance::{ComputeSquaredDistance, SquaredDistance};
 use values::generics::NonNegative;
 use values::specified::length::{AbsoluteLength, FontBaseSize, FontRelativeLength};
 use values::specified::length::ViewportPercentageLength;
 
 pub use super::image::Image;
 pub use values::specified::{Angle, BorderStyle, Time, UrlOrNone};
 
@@ -893,19 +892,16 @@ impl From<NonNegativeLength> for Au {
 }
 
 /// Either a computed NonNegativeLength or the `auto` keyword.
 pub type NonNegativeLengthOrAuto = Either<NonNegativeLength, Auto>;
 
 /// Either a computed NonNegativeLength or the `normal` keyword.
 pub type NonNegativeLengthOrNormal = Either<NonNegativeLength, Normal>;
 
-/// Either a computed NonNegativeLength or a NonNegativeNumber value.
-pub type NonNegativeLengthOrNumber = Either<NonNegativeNumber, NonNegativeLength>;
-
 /// A type for possible values for min- and max- flavors of width, height,
 /// block-size, and inline-size.
 #[allow(missing_docs)]
 #[cfg_attr(feature = "servo", derive(Deserialize, Serialize))]
 #[derive(Clone, Copy, Debug, Eq, MallocSizeOf, Parse, PartialEq, ToCss)]
 pub enum ExtremumLength {
     MozMaxContent,
     MozMinContent,
diff --git a/servo/components/style/values/computed/mod.rs b/servo/components/style/values/computed/mod.rs
--- a/servo/components/style/values/computed/mod.rs
+++ b/servo/components/style/values/computed/mod.rs
@@ -73,17 +73,18 @@ pub use self::percentage::Percentage;
 pub use self::position::{GridAutoFlow, GridTemplateAreas, Position, ZIndex};
 pub use self::pointing::Cursor;
 #[cfg(feature = "gecko")]
 pub use self::pointing::CursorImage;
 pub use self::svg::{SVGLength, SVGOpacity, SVGPaint, SVGPaintKind};
 pub use self::svg::{SVGPaintOrder, SVGStrokeDashArray, SVGWidth};
 pub use self::svg::MozContextProperties;
 pub use self::table::XSpan;
-pub use self::text::{InitialLetter, LetterSpacing, LineHeight, TextAlign, TextOverflow, WordSpacing};
+pub use self::text::{InitialLetter, LetterSpacing, LineHeight, MozTabSize};
+pub use self::text::{TextAlign, TextOverflow, WordSpacing};
 pub use self::time::Time;
 pub use self::transform::{Rotate, Scale, TimingFunction, Transform, TransformOperation};
 pub use self::transform::{TransformOrigin, TransformStyle, Translate};
 pub use self::ui::MozForceBrokenImageIcon;
 
 #[cfg(feature = "gecko")]
 pub mod align;
 pub mod angle;
diff --git a/servo/components/style/values/computed/text.rs b/servo/components/style/values/computed/text.rs
--- a/servo/components/style/values/computed/text.rs
+++ b/servo/components/style/values/computed/text.rs
@@ -8,16 +8,17 @@
 use properties::StyleBuilder;
 use std::fmt::{self, Write};
 use style_traits::{CssWriter, ToCss};
 use values::{CSSInteger, CSSFloat};
 use values::computed::{NonNegativeLength, NonNegativeNumber};
 use values::computed::length::{Length, LengthOrPercentage};
 use values::generics::text::InitialLetter as GenericInitialLetter;
 use values::generics::text::LineHeight as GenericLineHeight;
+use values::generics::text::MozTabSize as GenericMozTabSize;
 use values::generics::text::Spacing;
 use values::specified::text::{TextOverflowSide, TextDecorationLine};
 
 pub use values::specified::TextAlignKeyword as TextAlign;
 
 /// A computed value for the `initial-letter` property.
 pub type InitialLetter = GenericInitialLetter<CSSFloat, CSSInteger>;
 
@@ -142,8 +143,11 @@ impl TextDecorationsInEffect {
 
         result.underline |= text_style.has_underline();
         result.overline |= text_style.has_overline();
         result.line_through |= text_style.has_line_through();
 
         result
     }
 }
+
+/// A specified value for the `-moz-tab-size` property.
+pub type MozTabSize = GenericMozTabSize<NonNegativeNumber, NonNegativeLength>;
diff --git a/servo/components/style/values/generics/text.rs b/servo/components/style/values/generics/text.rs
--- a/servo/components/style/values/generics/text.rs
+++ b/servo/components/style/values/generics/text.rs
@@ -128,8 +128,18 @@ impl<N, L> ToAnimatedZero for LineHeight
 
 impl<N, L> LineHeight<N, L> {
     /// Returns `normal`.
     #[inline]
     pub fn normal() -> Self {
         LineHeight::Normal
     }
 }
+
+/// A generic value for the `-moz-tab-size` property.
+#[derive(Animate, Clone, ComputeSquaredDistance, Copy, Debug, MallocSizeOf)]
+#[derive(PartialEq, ToAnimatedValue, ToAnimatedZero, ToComputedValue, ToCss)]
+pub enum MozTabSize<Number, Length> {
+    /// A number.
+    Number(Number),
+    /// A length.
+    Length(Length),
+}
diff --git a/servo/components/style/values/specified/length.rs b/servo/components/style/values/specified/length.rs
--- a/servo/components/style/values/specified/length.rs
+++ b/servo/components/style/values/specified/length.rs
@@ -15,17 +15,16 @@ use std::cmp;
 #[allow(unused_imports)] use std::ascii::AsciiExt;
 use std::ops::{Add, Mul};
 use style_traits::{ParseError, StyleParseErrorKind};
 use style_traits::values::specified::AllowedNumericType;
 use super::{AllowQuirks, Number, ToComputedValue, Percentage};
 use values::{Auto, CSSFloat, Either, Normal};
 use values::computed::{self, CSSPixelLength, Context, ExtremumLength};
 use values::generics::NonNegative;
-use values::specified::NonNegativeNumber;
 use values::specified::calc::CalcNode;
 
 pub use values::specified::calc::CalcLengthOrPercentage;
 pub use super::image::{ColorStop, EndingShape as GradientEndingShape, Gradient};
 pub use super::image::{GradientKind, Image};
 
 /// Number of app units per pixel
 pub const AU_PER_PX: CSSFloat = 60.;
@@ -682,22 +681,16 @@ impl NonNegativeLength {
 }
 
 /// Either a NonNegativeLength or the `normal` keyword.
 pub type NonNegativeLengthOrNormal = Either<NonNegativeLength, Normal>;
 
 /// Either a NonNegativeLength or the `auto` keyword.
 pub type NonNegativeLengthOrAuto = Either<NonNegativeLength, Auto>;
 
-/// Either a NonNegativeLength or a NonNegativeNumber value.
-///
-/// The order is important, because `0` must be parsed as the number `0` and not
-/// the length `0px`.
-pub type NonNegativeLengthOrNumber = Either<NonNegativeNumber, NonNegativeLength>;
-
 /// A length or a percentage value.
 #[allow(missing_docs)]
 #[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
 pub enum LengthOrPercentage {
     Length(NoCalcLength),
     Percentage(computed::Percentage),
     Calc(Box<CalcLengthOrPercentage>),
 }
diff --git a/servo/components/style/values/specified/mod.rs b/servo/components/style/values/specified/mod.rs
--- a/servo/components/style/values/specified/mod.rs
+++ b/servo/components/style/values/specified/mod.rs
@@ -67,18 +67,18 @@ pub use self::position::{GridAutoFlow, G
 pub use self::position::{PositionComponent, ZIndex};
 pub use self::pointing::Cursor;
 #[cfg(feature = "gecko")]
 pub use self::pointing::CursorImage;
 pub use self::svg::{SVGLength, SVGOpacity, SVGPaint, SVGPaintKind};
 pub use self::svg::{SVGPaintOrder, SVGStrokeDashArray, SVGWidth};
 pub use self::svg::MozContextProperties;
 pub use self::table::XSpan;
-pub use self::text::{InitialLetter, LetterSpacing, LineHeight, TextDecorationLine};
-pub use self::text::{TextAlign, TextAlignKeyword, TextOverflow, WordSpacing};
+pub use self::text::{InitialLetter, LetterSpacing, LineHeight, MozTabSize, TextAlign};
+pub use self::text::{TextAlignKeyword, TextDecorationLine, TextOverflow, WordSpacing};
 pub use self::time::Time;
 pub use self::transform::{Rotate, Scale, TimingFunction, Transform};
 pub use self::transform::{TransformOrigin, TransformStyle, Translate};
 pub use self::ui::MozForceBrokenImageIcon;
 pub use super::generics::grid::GridTemplateComponent as GenericGridTemplateComponent;
 
 #[cfg(feature = "gecko")]
 pub mod align;
diff --git a/servo/components/style/values/specified/text.rs b/servo/components/style/values/specified/text.rs
--- a/servo/components/style/values/specified/text.rs
+++ b/servo/components/style/values/specified/text.rs
@@ -10,20 +10,21 @@ use selectors::parser::SelectorParseErro
 #[allow(unused_imports)] use std::ascii::AsciiExt;
 use std::fmt::{self, Write};
 use style_traits::{CssWriter, ParseError, StyleParseErrorKind, ToCss};
 use values::computed::{Context, ToComputedValue};
 use values::computed::text::LineHeight as ComputedLineHeight;
 use values::computed::text::TextOverflow as ComputedTextOverflow;
 use values::generics::text::InitialLetter as GenericInitialLetter;
 use values::generics::text::LineHeight as GenericLineHeight;
+use values::generics::text::MozTabSize as GenericMozTabSize;
 use values::generics::text::Spacing;
 use values::specified::{AllowQuirks, Integer, NonNegativeNumber, Number};
 use values::specified::length::{FontRelativeLength, Length, LengthOrPercentage, NoCalcLength};
-use values::specified::length::NonNegativeLengthOrPercentage;
+use values::specified::length::{NonNegativeLength, NonNegativeLengthOrPercentage};
 
 /// A specified type for the `initial-letter` property.
 pub type InitialLetter = GenericInitialLetter<Number, Integer>;
 
 /// A specified value for the `letter-spacing` property.
 pub type LetterSpacing = Spacing<Length>;
 
 /// A specified value for the `word-spacing` property.
@@ -509,8 +510,25 @@ impl ToComputedValue for TextAlign {
         }
     }
 
     #[inline]
     fn from_computed_value(computed: &Self::ComputedValue) -> Self {
         TextAlign::Keyword(*computed)
     }
 }
+
+/// A specified value for the `-moz-tab-size` property.
+pub type MozTabSize = GenericMozTabSize<NonNegativeNumber, NonNegativeLength>;
+
+impl Parse for MozTabSize {
+    fn parse<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<Self, ParseError<'i>> {
+        if let Ok(number) = input.try(|i| NonNegativeNumber::parse(context, i)) {
+            // Numbers need to be parsed first because `0` must be recognised
+            // as the number `0` and not the length `0px`.
+            return Ok(GenericMozTabSize::Number(number));
+        }
+        Ok(GenericMozTabSize::Length(NonNegativeLength::parse(context, input)?))
+    }
+}
