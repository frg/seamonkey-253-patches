# HG changeset patch
# User Markus Stange <mstange@themasta.com>
# Date 1516836027 18000
# Node ID 296ef6baf20b73d754ea7053ace0a4f1749cb4e3
# Parent  c1ced18856e8fa12804b72c10cc78aa6fb9d3606
Bug 1431184 - Register DOM Worker threads with the profiler for their entire lifetime, not just for the ranges during which they're running a worker script. r=froydnj

Our Web Worker code uses a thread pool where a single OS thread can be reused
for different worker scripts during its lifetime. Before this patch, we only
registered these threads with the profiler for the duration that they're
running a worker script. So the same OS thread could be registered with the
profiler during multiple disjoint time ranges, and we would expect the profiler
to treat those different registrations as different conceptual threads.

This had multiple advantages:
 - The "thread name" of the conceptual thread can include the script URL:
   "DOM Worker <scriptURL>". This allowed you to create thread filter which
   match a part of the URL, so you had the option of profiling just the worker
   threads you were interested in.
 - We wouldn't waste time sampling a worker thread while it's idle and has no
   script.

But it also had disadvantages:
 - The profiler platform doesn't actually know how to deal with different
   "conceptual threads" that share the same OS thread. This lead to surprising
   breakage in different places. For example, the contents in the profiler
   buffer are marked with ThreadId entries which use the OS thread id.
 - What we show in the profiler UI didn't not match reality, and might be
   confusing to some people.

I don't think the advantages are large enough to warrant teaching the rest of
the profiler platform to deal with conceptual threads. So this change makes us
stop doing the special thing and just register the OS threads for their entire
duration.

MozReview-Commit-ID: 82RtlRlwy3Y

diff --git a/dom/workers/RuntimeService.cpp b/dom/workers/RuntimeService.cpp
--- a/dom/workers/RuntimeService.cpp
+++ b/dom/workers/RuntimeService.cpp
@@ -2831,27 +2831,21 @@ LogViolationDetailsRunnable::MainThreadR
   return true;
 }
 
 NS_IMPL_ISUPPORTS_INHERITED0(WorkerThreadPrimaryRunnable, Runnable)
 
 NS_IMETHODIMP
 WorkerThreadPrimaryRunnable::Run()
 {
+  AUTO_PROFILER_LABEL_DYNAMIC_LOSSY_NSSTRING(
+    "WorkerThreadPrimaryRunnable::Run", OTHER, mWorkerPrivate->ScriptURL());
+
   using mozilla::ipc::BackgroundChild;
 
-  NS_SetCurrentThreadName("DOM Worker");
-
-  nsAutoCString threadName;
-  threadName.AssignLiteral("DOM Worker '");
-  threadName.Append(NS_LossyConvertUTF16toASCII(mWorkerPrivate->ScriptURL()));
-  threadName.Append('\'');
-
-  AUTO_PROFILER_REGISTER_THREAD(threadName.get());
-
   // Note: GetOrCreateForCurrentThread() must be called prior to
   //       mWorkerPrivate->SetThread() in order to avoid accidentally consuming
   //       worker messages here.
   if (NS_WARN_IF(!BackgroundChild::GetOrCreateForCurrentThread())) {
     // XXX need to fire an error at parent.
     // Failed in creating BackgroundChild: probably in shutdown. Continue to run
     // without BackgroundChild created.
   }
diff --git a/dom/workers/WorkerThread.cpp b/dom/workers/WorkerThread.cpp
--- a/dom/workers/WorkerThread.cpp
+++ b/dom/workers/WorkerThread.cpp
@@ -88,17 +88,17 @@ WorkerThread::~WorkerThread()
   MOZ_ASSERT(mAcceptingNonWorkerRunnables);
 }
 
 // static
 already_AddRefed<WorkerThread>
 WorkerThread::Create(const WorkerThreadFriendKey& /* aKey */)
 {
   RefPtr<WorkerThread> thread = new WorkerThread();
-  if (NS_FAILED(thread->Init())) {
+  if (NS_FAILED(thread->Init(NS_LITERAL_CSTRING("DOM Worker")))) {
     NS_WARNING("Failed to create new thread!");
     return nullptr;
   }
 
   return thread.forget();
 }
 
 void

