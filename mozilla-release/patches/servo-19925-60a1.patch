# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1518608978 18000
#      Wed Feb 14 06:49:38 2018 -0500
# Node ID c54159f5458a2784f273715c29cf10021ff29b62
# Parent  a0b74422be174c66c5535e093c9229bcb6e44846
servo: Merge #19925 - Use cstr macro for ffi literal strings (from upsuper:cstr); r=emilio

Use `cstr!()` macro with `CStr` to ensure that literal strings used with FFI is properly nul-terminated to avoid cases like #19915.

Source-Repo: https://github.com/servo/servo
Source-Revision: 5af219e8188f2e657c5711eb5def4c8e8065e29d

diff --git a/servo/ports/geckolib/Cargo.toml b/servo/ports/geckolib/Cargo.toml
--- a/servo/ports/geckolib/Cargo.toml
+++ b/servo/ports/geckolib/Cargo.toml
@@ -11,16 +11,17 @@ crate-type = ["staticlib", "rlib"]
 
 [features]
 bindgen = ["style/use_bindgen"]
 gecko_debug = ["style/gecko_debug"]
 
 [dependencies]
 atomic_refcell = "0.1"
 cssparser = "0.23.0"
+cstr = "0.1.2"
 env_logger = {version = "0.4", default-features = false} # disable `regex` to reduce code size
 libc = "0.2"
 log = {version = "0.3.5", features = ["release_max_level_info"]}
 malloc_size_of = {path = "../../components/malloc_size_of"}
 nsstring = {path = "../../support/gecko/nsstring"}
 parking_lot = "0.4"
 selectors = {path = "../../components/selectors"}
 servo_arc = {path = "../../components/servo_arc"}
diff --git a/servo/ports/geckolib/error_reporter.rs b/servo/ports/geckolib/error_reporter.rs
--- a/servo/ports/geckolib/error_reporter.rs
+++ b/servo/ports/geckolib/error_reporter.rs
@@ -4,16 +4,17 @@
 
 //! Wrapper around Gecko's CSS error reporting mechanism.
 
 #![allow(unsafe_code)]
 
 use cssparser::{CowRcStr, serialize_identifier, ToCss};
 use cssparser::{SourceLocation, ParseError, ParseErrorKind, Token, BasicParseErrorKind};
 use selectors::parser::SelectorParseErrorKind;
+use std::ffi::CStr;
 use std::ptr;
 use style::error_reporting::{ParseErrorReporter, ContextualParseError};
 use style::gecko_bindings::bindings::{Gecko_CreateCSSErrorReporter, Gecko_DestroyCSSErrorReporter};
 use style::gecko_bindings::bindings::Gecko_ReportUnexpectedCSSError;
 use style::gecko_bindings::structs::{Loader, ServoStyleSheet, nsIURI};
 use style::gecko_bindings::structs::ErrorReporter as GeckoErrorReporter;
 use style::gecko_bindings::structs::URLExtraData as RawUrlExtraData;
 use style::stylesheets::UrlExtraData;
@@ -71,17 +72,17 @@ enum Action {
     Nothing,
     Skip,
     Drop,
 }
 
 trait ErrorHelpers<'a> {
     fn error_data(self) -> (CowRcStr<'a>, ErrorKind<'a>);
     fn error_params(self) -> ErrorParams<'a>;
-    fn to_gecko_message(&self) -> (Option<&'static [u8]>, &'static [u8], Action);
+    fn to_gecko_message(&self) -> (Option<&'static CStr>, &'static CStr, Action);
 }
 
 fn extract_error_param<'a>(err: ErrorKind<'a>) -> Option<ErrorString<'a>> {
     Some(match err {
         ParseErrorKind::Basic(BasicParseErrorKind::UnexpectedToken(t)) => {
             ErrorString::UnexpectedToken(t)
         }
 
@@ -221,184 +222,184 @@ impl<'a> ErrorHelpers<'a> for Contextual
     fn error_params(self) -> ErrorParams<'a> {
         let (s, error) = self.error_data();
         extract_error_params(error).unwrap_or_else(|| ErrorParams {
             main_param: Some(ErrorString::Snippet(s)),
             prefix_param: None
         })
     }
 
-    fn to_gecko_message(&self) -> (Option<&'static [u8]>, &'static [u8], Action) {
-        let (msg, action): (&[u8], Action) = match *self {
+    fn to_gecko_message(&self) -> (Option<&'static CStr>, &'static CStr, Action) {
+        let (msg, action): (&CStr, Action) = match *self {
             ContextualParseError::UnsupportedPropertyDeclaration(
                 _, ParseError { kind: ParseErrorKind::Basic(BasicParseErrorKind::UnexpectedToken(_)), .. }
             ) |
             ContextualParseError::UnsupportedPropertyDeclaration(
                 _, ParseError { kind: ParseErrorKind::Basic(BasicParseErrorKind::AtRuleInvalid(_)), .. }
             ) => {
-                (b"PEParseDeclarationDeclExpected\0", Action::Skip)
+                (cstr!("PEParseDeclarationDeclExpected"), Action::Skip)
             }
             ContextualParseError::UnsupportedPropertyDeclaration(
                 _, ParseError { kind: ParseErrorKind::Custom(ref err), .. }
             ) => {
                 match *err {
                     StyleParseErrorKind::InvalidColor(_, _) => {
-                        return (Some(b"PEColorNotColor\0"),
-                                b"PEValueParsingError\0", Action::Drop)
+                        return (Some(cstr!("PEColorNotColor")),
+                                cstr!("PEValueParsingError"), Action::Drop)
                     }
                     StyleParseErrorKind::InvalidFilter(_, _) => {
-                        return (Some(b"PEExpectedNoneOrURLOrFilterFunction\0"),
-                                b"PEValueParsingError\0", Action::Drop)
+                        return (Some(cstr!("PEExpectedNoneOrURLOrFilterFunction")),
+                                cstr!("PEValueParsingError"), Action::Drop)
                     }
                     StyleParseErrorKind::OtherInvalidValue(_) => {
-                        (b"PEValueParsingError\0", Action::Drop)
+                        (cstr!("PEValueParsingError"), Action::Drop)
                     }
-                    _ => (b"PEUnknownProperty\0", Action::Drop)
+                    _ => (cstr!("PEUnknownProperty"), Action::Drop)
                 }
             }
             ContextualParseError::UnsupportedPropertyDeclaration(..) =>
-                (b"PEUnknownProperty\0", Action::Drop),
+                (cstr!("PEUnknownProperty"), Action::Drop),
             ContextualParseError::UnsupportedFontFaceDescriptor(..) =>
-                (b"PEUnknownFontDesc\0", Action::Skip),
+                (cstr!("PEUnknownFontDesc"), Action::Skip),
             ContextualParseError::InvalidKeyframeRule(..) =>
-                (b"PEKeyframeBadName\0", Action::Nothing),
+                (cstr!("PEKeyframeBadName"), Action::Nothing),
             ContextualParseError::UnsupportedKeyframePropertyDeclaration(..) =>
-                (b"PEBadSelectorKeyframeRuleIgnored\0", Action::Nothing),
+                (cstr!("PEBadSelectorKeyframeRuleIgnored"), Action::Nothing),
             ContextualParseError::InvalidRule(
                 _, ParseError { kind: ParseErrorKind::Custom(
                     StyleParseErrorKind::UnexpectedTokenWithinNamespace(_)
                 ), .. }
             ) => {
-                (b"PEAtNSUnexpected\0", Action::Nothing)
+                (cstr!("PEAtNSUnexpected"), Action::Nothing)
             }
             ContextualParseError::InvalidRule(
                 _, ParseError { kind: ParseErrorKind::Basic(BasicParseErrorKind::AtRuleInvalid(_)), .. }
             ) |
             ContextualParseError::InvalidRule(
                 _, ParseError { kind: ParseErrorKind::Custom(
                     StyleParseErrorKind::UnsupportedAtRule(_)
                 ), .. }
             ) => {
-                (b"PEUnknownAtRule\0", Action::Nothing)
+                (cstr!("PEUnknownAtRule"), Action::Nothing)
             }
             ContextualParseError::InvalidRule(_, ref err) => {
                 let prefix = match err.kind {
                     ParseErrorKind::Custom(StyleParseErrorKind::SelectorError(ref err)) => match *err {
                         SelectorParseErrorKind::UnexpectedTokenInAttributeSelector(_) => {
-                            Some(&b"PEAttSelUnexpected\0"[..])
+                            Some(cstr!("PEAttSelUnexpected"))
                         }
                         SelectorParseErrorKind::ExpectedBarInAttr(_) => {
-                            Some(&b"PEAttSelNoBar\0"[..])
+                            Some(cstr!("PEAttSelNoBar"))
                         }
                         SelectorParseErrorKind::BadValueInAttr(_) => {
-                            Some(&b"PEAttSelBadValue\0"[..])
+                            Some(cstr!("PEAttSelBadValue"))
                         }
                         SelectorParseErrorKind::NoQualifiedNameInAttributeSelector(_) => {
-                            Some(&b"PEAttributeNameOrNamespaceExpected\0"[..])
+                            Some(cstr!("PEAttributeNameOrNamespaceExpected"))
                         }
                         SelectorParseErrorKind::InvalidQualNameInAttr(_) => {
-                            Some(&b"PEAttributeNameExpected\0"[..])
+                            Some(cstr!("PEAttributeNameExpected"))
                         }
                         SelectorParseErrorKind::ExplicitNamespaceUnexpectedToken(_) => {
-                            Some(&b"PETypeSelNotType\0"[..])
+                            Some(cstr!("PETypeSelNotType"))
                         }
                         SelectorParseErrorKind::ExpectedNamespace(_) => {
-                           Some(&b"PEUnknownNamespacePrefix\0"[..])
+                           Some(cstr!("PEUnknownNamespacePrefix"))
                         }
                         SelectorParseErrorKind::EmptySelector => {
-                            Some(&b"PESelectorGroupNoSelector\0"[..])
+                            Some(cstr!("PESelectorGroupNoSelector"))
                         }
                         SelectorParseErrorKind::DanglingCombinator => {
-                            Some(&b"PESelectorGroupExtraCombinator\0"[..])
+                            Some(cstr!("PESelectorGroupExtraCombinator"))
                         }
                         SelectorParseErrorKind::UnsupportedPseudoClassOrElement(_) => {
-                            Some(&b"PEPseudoSelUnknown\0"[..])
+                            Some(cstr!("PEPseudoSelUnknown"))
                         }
                         SelectorParseErrorKind::PseudoElementExpectedColon(_) => {
-                            Some(&b"PEPseudoSelEndOrUserActionPC\0"[..])
+                            Some(cstr!("PEPseudoSelEndOrUserActionPC"))
                         }
                         SelectorParseErrorKind::NoIdentForPseudo(_) => {
-                            Some(&b"PEPseudoClassArgNotIdent\0"[..])
+                            Some(cstr!("PEPseudoClassArgNotIdent"))
                         }
                         SelectorParseErrorKind::PseudoElementExpectedIdent(_) => {
-                            Some(&b"PEPseudoSelBadName\0"[..])
+                            Some(cstr!("PEPseudoSelBadName"))
                         }
                         SelectorParseErrorKind::ClassNeedsIdent(_) => {
-                            Some(&b"PEClassSelNotIdent\0"[..])
+                            Some(cstr!("PEClassSelNotIdent"))
                         }
                         SelectorParseErrorKind::EmptyNegation => {
-                            Some(&b"PENegationBadArg\0"[..])
+                            Some(cstr!("PENegationBadArg"))
                         }
                         _ => None,
                     },
                     _ => None,
                 };
-                return (prefix, b"PEBadSelectorRSIgnored\0", Action::Nothing);
+                return (prefix, cstr!("PEBadSelectorRSIgnored"), Action::Nothing);
             }
             ContextualParseError::InvalidMediaRule(_, ref err) => {
-                let err: &[u8] = match err.kind {
+                let err: &CStr = match err.kind {
                     ParseErrorKind::Custom(StyleParseErrorKind::ExpectedIdentifier(..)) => {
-                        b"PEGatherMediaNotIdent\0"
+                        cstr!("PEGatherMediaNotIdent")
                     },
                     ParseErrorKind::Custom(StyleParseErrorKind::MediaQueryExpectedFeatureName(..)) => {
-                        b"PEMQExpectedFeatureName\0"
+                        cstr!("PEMQExpectedFeatureName")
                     },
                     ParseErrorKind::Custom(StyleParseErrorKind::MediaQueryExpectedFeatureValue) => {
-                        b"PEMQExpectedFeatureValue\0"
+                        cstr!("PEMQExpectedFeatureValue")
                     },
                     ParseErrorKind::Custom(StyleParseErrorKind::RangedExpressionWithNoValue) => {
-                        b"PEMQNoMinMaxWithoutValue\0"
+                        cstr!("PEMQNoMinMaxWithoutValue")
                     },
                     _ => {
-                        b"PEDeclDropped\0"
+                        cstr!("PEDeclDropped")
                     },
                 };
                 (err, Action::Nothing)
             }
             ContextualParseError::UnsupportedRule(..) =>
-                (b"PEDeclDropped\0", Action::Nothing),
+                (cstr!("PEDeclDropped"), Action::Nothing),
             ContextualParseError::UnsupportedViewportDescriptorDeclaration(..) |
             ContextualParseError::UnsupportedCounterStyleDescriptorDeclaration(..) |
             ContextualParseError::InvalidCounterStyleWithoutSymbols(..) |
             ContextualParseError::InvalidCounterStyleNotEnoughSymbols(..) |
             ContextualParseError::InvalidCounterStyleWithoutAdditiveSymbols |
             ContextualParseError::InvalidCounterStyleExtendsWithSymbols |
             ContextualParseError::InvalidCounterStyleExtendsWithAdditiveSymbols |
             ContextualParseError::UnsupportedFontFeatureValuesDescriptor(..) |
             ContextualParseError::InvalidFontFeatureValuesRule(..) =>
-                (b"PEUnknownAtRule\0", Action::Skip),
+                (cstr!("PEUnknownAtRule"), Action::Skip),
             ContextualParseError::UnsupportedValue(_, ParseError { ref kind, .. }) => {
                 match *kind {
                     ParseErrorKind::Custom(
                         StyleParseErrorKind::ValueError(
                             ValueParseErrorKind::InvalidColor(..)
                         )
-                    ) => (b"PEColorNotColor\0", Action::Nothing),
+                    ) => (cstr!("PEColorNotColor"), Action::Nothing),
                     _ => {
                         // Not the best error message, since we weren't parsing
                         // a declaration, just a value. But we don't produce
                         // UnsupportedValue errors other than InvalidColors
                         // currently.
                         debug_assert!(false, "should use a more specific error message");
-                        (b"PEDeclDropped\0", Action::Nothing)
+                        (cstr!("PEDeclDropped"), Action::Nothing)
                     }
                 }
             }
         };
         (None, msg, action)
     }
 }
 
 impl ErrorReporter {
     pub fn report(&self, location: SourceLocation, error: ContextualParseError) {
         let (pre, name, action) = error.to_gecko_message();
         let suffix = match action {
             Action::Nothing => ptr::null(),
-            Action::Skip => b"PEDeclSkipped\0".as_ptr(),
-            Action::Drop => b"PEDeclDropped\0".as_ptr(),
+            Action::Skip => cstr!("PEDeclSkipped").as_ptr(),
+            Action::Drop => cstr!("PEDeclDropped").as_ptr(),
         };
         let params = error.error_params();
         let param = params.main_param;
         let pre_param = params.prefix_param;
         let param = param.map(|p| p.into_str());
         let pre_param = pre_param.map(|p| p.into_str());
         let param_ptr = param.as_ref().map_or(ptr::null(), |p| p.as_ptr());
         let pre_param_ptr = pre_param.as_ref().map_or(ptr::null(), |p| p.as_ptr());
diff --git a/servo/ports/geckolib/lib.rs b/servo/ports/geckolib/lib.rs
--- a/servo/ports/geckolib/lib.rs
+++ b/servo/ports/geckolib/lib.rs
@@ -1,14 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 
 extern crate cssparser;
+#[macro_use] extern crate cstr;
 extern crate env_logger;
 extern crate libc;
 #[macro_use] extern crate log;
 extern crate malloc_size_of;
 extern crate selectors;
 extern crate servo_arc;
 extern crate smallvec;
 #[macro_use] extern crate style;
diff --git a/servo/ports/geckolib/tests/Cargo.toml b/servo/ports/geckolib/tests/Cargo.toml
--- a/servo/ports/geckolib/tests/Cargo.toml
+++ b/servo/ports/geckolib/tests/Cargo.toml
@@ -9,16 +9,17 @@ build = "build.rs"
 [lib]
 name = "stylo_tests"
 path = "lib.rs"
 doctest = false
 
 [dependencies]
 atomic_refcell = "0.1"
 cssparser = "0.23.0"
+cstr = "0.1.2"
 env_logger = "0.4"
 euclid = "0.17"
 geckoservo = {path = "../../../ports/geckolib"}
 libc = "0.2"
 log = {version = "0.3.5", features = ["release_max_level_info"]}
 malloc_size_of = {path = "../../../components/malloc_size_of"}
 selectors = {path = "../../../components/selectors", features = ["gecko_like_types"]}
 size_of_test = {path = "../../../components/size_of_test"}
diff --git a/servo/ports/geckolib/tests/lib.rs b/servo/ports/geckolib/tests/lib.rs
--- a/servo/ports/geckolib/tests/lib.rs
+++ b/servo/ports/geckolib/tests/lib.rs
@@ -8,16 +8,17 @@
 //
 // On Linux and OS X linking succeeds anyway.
 // Presumably these symbol declarations don’t need to be resolved
 // as they’re not used in any code called from this crate.
 #![cfg(any(linking_with_gecko, not(windows)))]
 
 extern crate atomic_refcell;
 extern crate cssparser;
+#[macro_use] extern crate cstr;
 extern crate env_logger;
 extern crate geckoservo;
 #[macro_use] extern crate log;
 extern crate malloc_size_of;
 extern crate selectors;
 extern crate smallvec;
 #[macro_use] extern crate size_of_test;
 #[macro_use] extern crate style;
