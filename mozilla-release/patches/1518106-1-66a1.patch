# HG changeset patch
# User Alex Chronopoulos <achronop@gmail.com>
# Date 1548272180 0
# Node ID 3692b1c3df6076cb3c07b102726e17cd0acce818
# Parent  08b686c04a013ca91738d80f6f34b92a86c130eb
Bug 1518106 - Update cubeb from upstream to feec7e2. r=kinetik

Differential Revision: https://phabricator.services.mozilla.com/D17399

diff --git a/media/libcubeb/moz.yaml b/media/libcubeb/moz.yaml
--- a/media/libcubeb/moz.yaml
+++ b/media/libcubeb/moz.yaml
@@ -14,10 +14,10 @@ bugzilla:
 origin:
   name: "cubeb"
   description: "Cross platform audio library"
 
   url: "https://github.com/kinetiknz/cubeb"
   license: "ISC"
 
   # update.sh will update this value
-  release: "67d37c16be84fcc469531d4b6f70eae8a2867a66 (2019-01-21 14:41:14 +0200)"
+  release: "feec7e2e893c6dc4188fcb95e15dcf8c19890f46 (2019-01-23 17:15:35 +0200)"
 
diff --git a/media/libcubeb/src/cubeb_pulse.c b/media/libcubeb/src/cubeb_pulse.c
--- a/media/libcubeb/src/cubeb_pulse.c
+++ b/media/libcubeb/src/cubeb_pulse.c
@@ -80,16 +80,17 @@
   X(pa_stream_peek)                             \
   X(pa_stream_drop)                             \
   X(pa_stream_get_buffer_attr)                  \
   X(pa_stream_get_device_name)                  \
   X(pa_context_set_subscribe_callback)          \
   X(pa_context_subscribe)                       \
   X(pa_mainloop_api_once)                       \
   X(pa_get_library_version)                     \
+  X(pa_channel_map_init_auto)                   \
 
 #define MAKE_TYPEDEF(x) static typeof(x) * cubeb_##x;
 LIBPULSE_API_VISIT(MAKE_TYPEDEF);
 #undef MAKE_TYPEDEF
 #endif
 
 #if PA_CHECK_VERSION(2, 0, 0)
 static int has_pulse_v2 = 0;
@@ -781,16 +782,35 @@ to_pulse_format(cubeb_sample_format form
     return PA_SAMPLE_FLOAT32LE;
   case CUBEB_SAMPLE_FLOAT32BE:
     return PA_SAMPLE_FLOAT32BE;
   default:
     return PA_SAMPLE_INVALID;
   }
 }
 
+static cubeb_channel_layout
+pulse_default_layout_for_channels(uint32_t ch)
+{
+  assert (ch > 0 && ch <= 8);
+  switch (ch) {
+    case 1: return CUBEB_LAYOUT_MONO;
+    case 2: return CUBEB_LAYOUT_STEREO;
+    case 3: return CUBEB_LAYOUT_3F;
+    case 4: return CUBEB_LAYOUT_QUAD;
+    case 5: return CUBEB_LAYOUT_3F2;
+    case 6: return CUBEB_LAYOUT_3F_LFE |
+                   CHANNEL_SIDE_LEFT | CHANNEL_SIDE_RIGHT;
+    case 7: return CUBEB_LAYOUT_3F3R_LFE;
+    case 8: return CUBEB_LAYOUT_3F4_LFE;
+  }
+  // Never get here!
+  return CUBEB_LAYOUT_UNDEFINED;
+}
+
 static int
 create_pa_stream(cubeb_stream * stm,
                  pa_stream ** pa_stm,
                  cubeb_stream_params * stream_params,
                  char const * stream_name)
 {
   assert(stm && stream_params);
   assert(&stm->input_stream == pa_stm || (&stm->output_stream == pa_stm &&
@@ -804,17 +824,26 @@ create_pa_stream(cubeb_stream * stm,
   pa_sample_spec ss;
   ss.format = to_pulse_format(stream_params->format);
   if (ss.format == PA_SAMPLE_INVALID)
     return CUBEB_ERROR_INVALID_FORMAT;
   ss.rate = stream_params->rate;
   ss.channels = stream_params->channels;
 
   if (stream_params->layout == CUBEB_LAYOUT_UNDEFINED) {
-    *pa_stm = WRAP(pa_stream_new)(stm->context->context, stream_name, &ss, NULL);
+    pa_channel_map cm;
+    if (stream_params->channels <= 8 &&
+       !WRAP(pa_channel_map_init_auto)(&cm, stream_params->channels, PA_CHANNEL_MAP_DEFAULT)) {
+      LOG("Layout undefined and PulseAudio's default layout has not been configured, guess one.");
+      layout_to_channel_map(pulse_default_layout_for_channels(stream_params->channels), &cm);
+      *pa_stm = WRAP(pa_stream_new)(stm->context->context, stream_name, &ss, &cm);
+    } else {
+      LOG("Layout undefined, PulseAudio will use its default.");
+      *pa_stm = WRAP(pa_stream_new)(stm->context->context, stream_name, &ss, NULL);
+    }
   } else {
     pa_channel_map cm;
     layout_to_channel_map(stream_params->layout, &cm);
     *pa_stm = WRAP(pa_stream_new)(stm->context->context, stream_name, &ss, &cm);
   }
   return (*pa_stm == NULL) ? CUBEB_ERROR : CUBEB_OK;
 }
 

