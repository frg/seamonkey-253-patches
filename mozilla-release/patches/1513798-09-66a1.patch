# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1544685155 -32400
#      Thu Dec 13 16:12:35 2018 +0900
# Node ID 93d4d156771f860e7d85625edd00bfea5bf70ee3
# Parent  6499936019eccf13e28f40a3860853ddf92d396c
Bug 1513798 - Move --with-macos-{sdk,private-frameworks} to python configure. r=nalexander

Because we now set the sysroot include flags early in python configure,
we don't need to set CPP/CXXCPP. We also skip the explicit compiler test
because more complete tests follow anyways.

Differential Revision: https://phabricator.services.mozilla.com/D14380

diff --git a/build/moz.configure/old.configure b/build/moz.configure/old.configure
--- a/build/moz.configure/old.configure
+++ b/build/moz.configure/old.configure
@@ -232,18 +232,16 @@ def old_configure_options(*options):
     '--with-doc-include-dirs',
     '--with-doc-input-dirs',
     '--with-doc-output-dir',
     '--with-float-abi',
     '--with-fpu',
     '--with-intl-api',
     '--with-jitreport-granularity',
     '--with-macbundlename-prefix',
-    '--with-macos-private-frameworks',
-    '--with-macos-sdk',
     '--with-nspr-cflags',
     '--with-nspr-exec-prefix',
     '--with-nspr-libs',
     '--with-nspr-prefix',
     '--with-nss-exec-prefix',
     '--with-nss-prefix',
     '--with-qemu-exe',
     '--with-sixgill',
diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -190,36 +190,75 @@ add_old_configure_assignment('YASM', hav
 
 @depends('--disable-compile-environment', build_project)
 def compiling_android(compile_env, build_project):
     return compile_env and build_project in ('mobile/android', 'js')
 
 
 include('android-ndk.configure', when=compiling_android)
 
-# MacOS deployment target version
-# ==============================================================
-# This needs to happen before any compilation test is done.
+with only_when(target_is_osx):
+    # MacOS deployment target version
+    # ==============================================================
+    # This needs to happen before any compilation test is done.
+
+    option('--enable-macos-target', env='MACOSX_DEPLOYMENT_TARGET', nargs=1,
+           default='10.9', help='Set the minimum MacOS version needed at runtime')
 
-option('--enable-macos-target', env='MACOSX_DEPLOYMENT_TARGET', nargs=1,
-       default='10.9', help='Set the minimum MacOS version needed at runtime',
-       when=target_is_osx)
+
+    @depends('--enable-macos-target')
+    @imports(_from='os', _import='environ')
+    def macos_target(value):
+        if value:
+            # Ensure every compiler process we spawn uses this value.
+            environ['MACOSX_DEPLOYMENT_TARGET'] = value[0]
+            return value[0]
 
 
-@depends('--enable-macos-target', when=target_is_osx)
-@imports(_from='os', _import='environ')
-def macos_target(value):
-    if value:
-        # Ensure every compiler process we spawn uses this value.
-        environ['MACOSX_DEPLOYMENT_TARGET'] = value[0]
+    set_config('MACOSX_DEPLOYMENT_TARGET', macos_target)
+    add_old_configure_assignment('MACOSX_DEPLOYMENT_TARGET', macos_target)
+
+    # MacOS SDK
+    # =========
+
+    option('--with-macos-sdk', nargs=1, help='Location of platform SDK to use')
+
+    @depends_if('--with-macos-sdk')
+    @imports(_from='os.path', _import='isdir')
+    def macos_sdk(value):
+        if not isdir(value[0]):
+            die('SDK not found in %s. When using --with-macos-sdk, you must specify a '
+                'valid SDK. SDKs are installed when the optional cross-development '
+                'tools are selected during the Xcode/Developer Tools installation.'
+                % value[0])
         return value[0]
 
+    set_config('MACOS_SDK_DIR', macos_sdk)
 
-set_config('MACOSX_DEPLOYMENT_TARGET', macos_target)
-add_old_configure_assignment('MACOSX_DEPLOYMENT_TARGET', macos_target)
+    with only_when(cross_compiling):
+        option('--with-macos-private-frameworks', nargs=1,
+               help='Location of private frameworks to use')
+
+        @depends_if('--with-macos-private-frameworks')
+        @imports(_from='os.path', _import='isdir')
+        def macos_private_frameworks(value):
+            if value and not isdir(value[0]):
+                die('PrivateFrameworks not found not found in %s. When using '
+                    '--with-macos-private-frameworks, you must specify a valid '
+                    'directory', value[0])
+            return value[0]
+
+    @depends(macos_private_frameworks)
+    def macos_private_frameworks(value):
+        if value:
+            return value
+        return '/System/Library/PrivateFrameworks'
+
+    set_config('MACOS_PRIVATE_FRAMEWORKS_DIR', macos_private_frameworks)
+
 
 # Xcode state
 # ===========
 
 js_option('--disable-xcode-checks',
           help='Do not check that Xcode is installed and properly configured')
 
 
@@ -900,34 +939,37 @@ def compiler(language, host_or_target, c
     # Normally, we'd use `var` instead of `_var`, but the interaction with
     # old-configure complicates things, and for now, we a) can't take the plain
     # result from check_prog as CC/CXX/HOST_CC/HOST_CXX and b) have to let
     # old-configure AC_SUBST it (because it's autoconf doing it, not us)
     compiler = check_prog('_%s' % var, what=what, progs=default_compilers,
                           input=provided_compiler.program,
                           paths=toolchain_search_path)
 
-    @depends(compiler, provided_compiler, compiler_wrapper, host_or_target)
+    @depends(compiler, provided_compiler, compiler_wrapper, host_or_target, macos_sdk)
     @checking('whether %s can be used' % what, lambda x: bool(x))
     @imports(_from='mozbuild.shellutil', _import='quote')
     def valid_compiler(compiler, provided_compiler, compiler_wrapper,
-                       host_or_target):
+                       host_or_target, macos_sdk):
         wrapper = list(compiler_wrapper or ())
         if provided_compiler:
             provided_wrapper = list(provided_compiler.wrapper)
             # When doing a subconfigure, the compiler is set by old-configure
             # and it contains the wrappers from --with-compiler-wrapper and
             # --with-ccache.
             if provided_wrapper[:len(wrapper)] == wrapper:
                 provided_wrapper = provided_wrapper[len(wrapper):]
             wrapper.extend(provided_wrapper)
             flags = provided_compiler.flags
         else:
             flags = []
 
+        if not flags and macos_sdk and host_or_target.os == 'OSX':
+            flags = ['-isysroot', macos_sdk]
+
         # Ideally, we'd always use the absolute path, but unfortunately, on
         # Windows, the compiler is very often in a directory containing spaces.
         # Unfortunately, due to the way autoconf does its compiler tests with
         # eval, that doesn't work out. So in that case, check that the
         # compiler can still be found in $PATH, and use the file name instead
         # of the full path.
         if quote(compiler) != compiler:
             full_path = os.path.abspath(compiler)
diff --git a/js/src/old-configure.in b/js/src/old-configure.in
--- a/js/src/old-configure.in
+++ b/js/src/old-configure.in
@@ -299,60 +299,16 @@ AC_SUBST(_MSC_VER)
 AC_SUBST(GNU_CC)
 AC_SUBST(GNU_CXX)
 
 dnl ========================================================
 dnl Checks for programs.
 dnl ========================================================
 if test "$COMPILE_ENVIRONMENT"; then
 
-dnl ========================================================
-dnl = Mac OS X SDK support
-dnl ========================================================
-MACOS_SDK_DIR=
-MOZ_ARG_WITH_STRING(macos-sdk,
-[  --with-macos-sdk=dir    Location of platform SDK to use (Mac OS X only)],
-    MACOS_SDK_DIR=$withval)
-
-dnl MACOS_SDK_DIR will be set to the SDK location whenever one is in use.
-AC_SUBST(MACOS_SDK_DIR)
-
-if test "$MACOS_SDK_DIR"; then
-  dnl Sync this section with the ones in NSPR and NSS.
-  dnl Changes to the cross environment here need to be accounted for in
-  dnl the libIDL checks (below) and xpidl build.
-
-  if test ! -d "$MACOS_SDK_DIR"; then
-    AC_MSG_ERROR([SDK not found.  When using --with-macos-sdk, you must
-specify a valid SDK.  SDKs are installed when the optional cross-development
-tools are selected during the Xcode/Developer Tools installation.])
-  fi
-
-  CFLAGS="$CFLAGS -isysroot ${MACOS_SDK_DIR}"
-  CXXFLAGS="$CXXFLAGS -isysroot ${MACOS_SDK_DIR}"
-
-  dnl CPP/CXXCPP needs to be set for MOZ_CHECK_HEADER.
-  CPP="$CPP -isysroot ${MACOS_SDK_DIR}"
-  CXXCPP="$CXXCPP -isysroot ${MACOS_SDK_DIR}"
-
-  AC_LANG_SAVE
-  AC_MSG_CHECKING([for valid compiler/Mac OS X SDK combination])
-  AC_LANG_CPLUSPLUS
-  AC_TRY_COMPILE([#include <new>
-                 int main() { return 0; }],
-   result=yes,
-   result=no)
-  AC_LANG_RESTORE
-  AC_MSG_RESULT($result)
-
-  if test "$result" = "no" ; then
-    AC_MSG_ERROR([The selected compiler and Mac OS X SDK are incompatible.])
-  fi
-fi
-
 AC_PATH_XTRA
 
 XCFLAGS="$X_CFLAGS"
 
 fi # COMPILE_ENVIRONMENT
 
 # Separate version into components for use in shared object naming etc
 changequote(,)
diff --git a/old-configure.in b/old-configure.in
--- a/old-configure.in
+++ b/old-configure.in
@@ -333,76 +333,16 @@ AC_SUBST_LIST(STL_FLAGS)
 AC_SUBST(WRAP_STL_INCLUDES)
 AC_SUBST(MOZ_MSVC_STL_WRAP_RAISE)
 
 dnl ========================================================
 dnl Checks for programs.
 dnl ========================================================
 if test "$COMPILE_ENVIRONMENT"; then
 
-dnl ========================================================
-dnl = Mac OS X SDK support
-dnl ========================================================
-MACOS_SDK_DIR=
-MOZ_ARG_WITH_STRING(macos-sdk,
-[  --with-macos-sdk=dir    Location of platform SDK to use (Mac OS X only)],
-    MACOS_SDK_DIR=$withval)
-
-MACOS_PRIVATE_FRAMEWORKS_DIR_DEFAULTED=
-MOZ_ARG_WITH_STRING(macos-private-frameworks,
-[  --with-macos-private-frameworks=dir    Location of private frameworks to use (Mac OS X only)],
-    MACOS_PRIVATE_FRAMEWORKS_DIR=$withval,
-    MACOS_PRIVATE_FRAMEWORKS_DIR=/System/Library/PrivateFrameworks
-    MACOS_PRIVATE_FRAMEWORKS_DEFAULTED=1)
-
-if test -z "${MACOS_PRIVATE_FRAMEWORKS_DEFAULTED}"; then
-  if test -z "$CROSS_COMPILE"; then
-    AC_MSG_WARN([You should only be using --with-macos-private-frameworks when cross-compiling.])
-  fi
-  if test ! -d "$MACOS_PRIVATE_FRAMEWORKS_DIR"; then
-    AC_MSG_ERROR([PrivateFrameworks directory not found.])
-  fi
-fi
-
-dnl MACOS_SDK_DIR will be set to the SDK location whenever one is in use.
-AC_SUBST(MACOS_SDK_DIR)
-AC_SUBST(MACOS_PRIVATE_FRAMEWORKS_DIR)
-
-if test "$MACOS_SDK_DIR"; then
-  dnl Sync this section with the ones in NSPR and NSS.
-  dnl Changes to the cross environment here need to be accounted for in
-  dnl the libIDL checks (below) and xpidl build.
-
-  if test ! -d "$MACOS_SDK_DIR"; then
-    AC_MSG_ERROR([SDK not found.  When using --with-macos-sdk, you must
-specify a valid SDK.  SDKs are installed when the optional cross-development
-tools are selected during the Xcode/Developer Tools installation.])
-  fi
-
-  CFLAGS="$CFLAGS -isysroot ${MACOS_SDK_DIR}"
-  CXXFLAGS="$CXXFLAGS -isysroot ${MACOS_SDK_DIR}"
-
-  dnl CPP/CXXCPP needs to be set for MOZ_CHECK_HEADER.
-  CPP="$CPP -isysroot ${MACOS_SDK_DIR}"
-  CXXCPP="$CXXCPP -isysroot ${MACOS_SDK_DIR}"
-
-  AC_LANG_SAVE
-  AC_MSG_CHECKING([for valid compiler/Mac OS X SDK combination])
-  AC_LANG_CPLUSPLUS
-  AC_TRY_COMPILE([#include <new>],[],
-   result=yes,
-   result=no)
-  AC_LANG_RESTORE
-  AC_MSG_RESULT($result)
-
-  if test "$result" = "no" ; then
-    AC_MSG_ERROR([The selected compiler and Mac OS X SDK are incompatible.])
-  fi
-fi
-
 AC_PATH_XTRA
 
 XCFLAGS="$X_CFLAGS"
 
 fi # COMPILE_ENVIRONMENT
 
 dnl ========================================================
 dnl set the defaults first
