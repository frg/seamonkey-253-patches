# HG changeset patch
# User Aaron Klotz <aklotz@mozilla.com>
# Date 1521224626 21600
# Node ID 2e74592c8f953de873f5495d2fa9afa9661ac99c
# Parent  ab9bb29e3f418da75c0ee93dc96803d63e5aae90
Bug 1446280: Ensure a11y::SetInstantiator only runs once; r=eeejay

diff --git a/accessible/windows/msaa/Platform.cpp b/accessible/windows/msaa/Platform.cpp
--- a/accessible/windows/msaa/Platform.cpp
+++ b/accessible/windows/msaa/Platform.cpp
@@ -324,17 +324,17 @@ AppendVersionInfo(nsIFile* aClientExe, n
   aStrToAppend.AppendInt(minor);
   aStrToAppend.Append(dot);
   aStrToAppend.AppendInt(patch);
   aStrToAppend.Append(dot);
   aStrToAppend.AppendInt(build);
 }
 
 static void
-AccumulateInstantiatorStats(nsIFile* aClientExe, const nsAString& aValue)
+AccumulateInstantiatorStats(const nsAString& aValue)
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   if (!aValue.IsEmpty()) {
     CrashReporter::
       AnnotateCrashReport(NS_LITERAL_CSTRING("AccessibilityClient"),
                           NS_ConvertUTF16toUTF8(aValue));
   }
@@ -346,47 +346,58 @@ GatherInstantiatorStats(nsIFile* aClient
   MOZ_ASSERT(!NS_IsMainThread());
 
   nsString value;
   nsresult rv = aClientExe->GetLeafName(value);
   if (NS_SUCCEEDED(rv)) {
     AppendVersionInfo(aClientExe, value);
   }
 
-  nsCOMPtr<nsIFile> ref(aClientExe);
   nsCOMPtr<nsIRunnable> runnable(
     NS_NewRunnableFunction("a11y::AccumulateInstantiatorStats",
-                           [ref, value]() -> void {
-                             AccumulateInstantiatorStats(ref, value);
+                           [value]() -> void {
+                             AccumulateInstantiatorStats(value);
                            }));
 
   // Now that we've (possibly) obtained version info, send the resulting
   // string back to the main thread to accumulate in statistics.
   NS_DispatchToMainThread(runnable);
 }
 
 void
 a11y::SetInstantiator(const uint32_t aPid)
 {
   nsCOMPtr<nsIFile> clientExe;
   if (!GetInstantiatorExecutable(aPid, getter_AddRefs(clientExe))) {
-    AccumulateInstantiatorStats(nullptr, NS_LITERAL_STRING("(Failed to retrieve client image name)"));
+    AccumulateInstantiatorStats(NS_LITERAL_STRING("(Failed to retrieve client image name)"));
     return;
   }
 
+  // Only record the instantiator if it is the first instantiator, or if it does
+  // not match the previous one. Some blocked clients are repeatedly requesting
+  // a11y over and over so we don't want to be spawning countless statistics
+  // threads.
+  if (gInstantiator) {
+    bool equal;
+    nsresult rv = gInstantiator->Equals(clientExe, &equal);
+    if (NS_SUCCEEDED(rv) && equal) {
+      return;
+    }
+  }
+
   gInstantiator = clientExe;
 
   nsCOMPtr<nsIRunnable> runnable(
     NS_NewRunnableFunction("a11y::GatherInstantiatorStats",
                            [clientExe]() -> void {
                              GatherInstantiatorStats(clientExe);
                            }));
 
   nsCOMPtr<nsIThread> statsThread;
-  NS_NewThread(getter_AddRefs(statsThread), runnable);
+  NS_NewNamedThread("a11y statistics", getter_AddRefs(statsThread), runnable);
 }
 
 bool
 a11y::GetInstantiator(nsIFile** aOutInstantiator)
 {
   if (!gInstantiator) {
     return false;
   }
