# HG changeset patch
# User Aaron Klotz <aklotz@mozilla.com>
# Date 1517000922 25200
# Node ID ad05b79ebbe5cabb4d7ca0198b02987fe7de44d1
# Parent  9f3982c9444693edffddd69c93b07639f257bb8b
Bug 1433551: Use fallible allocation for handleInfoBuf inside UIA detection; r=davidb

diff --git a/accessible/windows/msaa/CompatibilityUIA.cpp b/accessible/windows/msaa/CompatibilityUIA.cpp
--- a/accessible/windows/msaa/CompatibilityUIA.cpp
+++ b/accessible/windows/msaa/CompatibilityUIA.cpp
@@ -1,16 +1,17 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "Compatibility.h"
 
+#include "mozilla/UniquePtrExtensions.h"
 #include "mozilla/WindowsVersion.h"
 
 #include "nsDataHashtable.h"
 #include "nsPrintfCString.h"
 #include "nsReadableUtils.h"
 #include "nsString.h"
 #include "nsTHashtable.h"
 #include "nsUnicharUtils.h"
@@ -42,17 +43,17 @@ struct ByteArrayDeleter
 };
 
 typedef UniquePtr<OBJECT_DIRECTORY_INFORMATION, ByteArrayDeleter> ObjDirInfoPtr;
 
 // ComparatorFnT returns true to continue searching, or else false to indicate
 // search completion.
 template <typename ComparatorFnT>
 static bool
-FindNamedObject(ComparatorFnT aComparator)
+FindNamedObject(const ComparatorFnT& aComparator)
 {
   // We want to enumerate every named kernel object in our session. We do this
   // by opening a directory object using a path constructed using the session
   // id under which our process resides.
   DWORD sessionId;
   if (!::ProcessIdToSessionId(::GetCurrentProcessId(), &sessionId)) {
     return false;
   }
@@ -179,17 +180,22 @@ Compatibility::OnUIAMessage(WPARAM aWPar
   UniquePtr<char[]> handleInfoBuf;
   ULONG handleInfoBufLen = sizeof(SYSTEM_HANDLE_INFORMATION_EX) +
                            1024 * sizeof(SYSTEM_HANDLE_TABLE_ENTRY_INFO_EX);
 
   // We must query for handle information in a loop, since we are effectively
   // asking the kernel to take a snapshot of all the handles on the system;
   // the size of the required buffer may fluctuate between successive calls.
   while (true) {
-    handleInfoBuf = MakeUnique<char[]>(handleInfoBufLen);
+    // These allocations can be hundreds of megabytes on some computers, so
+    // we should use fallible new here.
+    handleInfoBuf = MakeUniqueFallible<char[]>(handleInfoBufLen);
+    if (!handleInfoBuf) {
+      return Nothing();
+    }
 
     ntStatus = ::NtQuerySystemInformation(
                  (SYSTEM_INFORMATION_CLASS) SystemExtendedHandleInformation,
                  handleInfoBuf.get(), handleInfoBufLen, &handleInfoBufLen);
     if (ntStatus == STATUS_INFO_LENGTH_MISMATCH) {
       continue;
     }
 
@@ -287,17 +293,16 @@ Compatibility::OnUIAMessage(WPARAM aWPar
     // We found kernelObject *after* we saw the remote process's copy. Now we
     // must look it up in objMap.
     DWORD pid;
     if (objMap.Get(kernelObject.value(), &pid)) {
       remotePid = Some(pid);
     }
   }
 
-
   if (!remotePid) {
     return Nothing();
   }
 
   a11y::SetInstantiator(remotePid.value());
 
   /* This is where we could block UIA stuff
   nsCOMPtr<nsIFile> instantiator;
