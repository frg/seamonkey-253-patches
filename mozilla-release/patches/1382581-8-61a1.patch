# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1521223140 -3600
# Node ID 3214b1a3f2355410e940991a4435aef2cbc38025
# Parent  7b9a1c4f283b3fb75ca6bcf0c2a1d191af53454b
Bug 1382581 - Adapt shadereditor code to the EventEmitter change in devtools/client/framework; r=bgrins.

MozReview-Commit-ID: 9Z9uVMuTqKE

diff --git a/devtools/client/shadereditor/shadereditor.js b/devtools/client/shadereditor/shadereditor.js
--- a/devtools/client/shadereditor/shadereditor.js
+++ b/devtools/client/shadereditor/shadereditor.js
@@ -86,32 +86,33 @@ function shutdownShaderEditor() {
  */
 var EventsHandler = {
   /**
    * Listen for events emitted by the current tab target.
    */
   initialize: function () {
     this._onHostChanged = this._onHostChanged.bind(this);
     this._onTabNavigated = this._onTabNavigated.bind(this);
+    this._onTabWillNavigate = this._onTabWillNavigate.bind(this);
     this._onProgramLinked = this._onProgramLinked.bind(this);
     this._onProgramsAdded = this._onProgramsAdded.bind(this);
     gToolbox.on("host-changed", this._onHostChanged);
-    gTarget.on("will-navigate", this._onTabNavigated);
+    gTarget.on("will-navigate", this._onTabWillNavigate);
     gTarget.on("navigate", this._onTabNavigated);
     gFront.on("program-linked", this._onProgramLinked);
     this.reloadButton = $("#requests-menu-reload-notice-button");
     this.reloadButton.addEventListener("command", this._onReloadCommand);
   },
 
   /**
    * Remove events emitted by the current tab target.
    */
   destroy: function () {
     gToolbox.off("host-changed", this._onHostChanged);
-    gTarget.off("will-navigate", this._onTabNavigated);
+    gTarget.off("will-navigate", this._onTabWillNavigate);
     gTarget.off("navigate", this._onTabNavigated);
     gFront.off("program-linked", this._onProgramLinked);
     this.reloadButton.removeEventListener("command", this._onReloadCommand);
   },
 
   /**
    * Handles a command event on reload button
    */
@@ -123,53 +124,47 @@ var EventsHandler = {
    * Handles a host change event on the parent toolbox.
    */
   _onHostChanged: function () {
     if (gToolbox.hostType == "side") {
       $("#shaders-pane").removeAttribute("height");
     }
   },
 
+  _onTabWillNavigate: function({isFrameSwitching}) {
+    // Make sure the backend is prepared to handle WebGL contexts.
+    if (!isFrameSwitching) {
+      gFront.setup({ reload: false });
+    }
+
+    // Reset UI.
+    ShadersListView.empty();
+    // When switching to an iframe, ensure displaying the reload button.
+    // As the document has already been loaded without being hooked.
+    if (isFrameSwitching) {
+      $("#reload-notice").hidden = false;
+      $("#waiting-notice").hidden = true;
+    } else {
+      $("#reload-notice").hidden = true;
+      $("#waiting-notice").hidden = false;
+    }
+
+    $("#content").hidden = true;
+    window.emit(EVENTS.UI_RESET);
+  },
+
   /**
    * Called for each location change in the debugged tab.
    */
-  _onTabNavigated: function (event, {isFrameSwitching}) {
-    switch (event) {
-      case "will-navigate": {
-        // Make sure the backend is prepared to handle WebGL contexts.
-        if (!isFrameSwitching) {
-          gFront.setup({ reload: false });
-        }
-
-        // Reset UI.
-        ShadersListView.empty();
-        // When switching to an iframe, ensure displaying the reload button.
-        // As the document has already been loaded without being hooked.
-        if (isFrameSwitching) {
-          $("#reload-notice").hidden = false;
-          $("#waiting-notice").hidden = true;
-        } else {
-          $("#reload-notice").hidden = true;
-          $("#waiting-notice").hidden = false;
-        }
-
-        $("#content").hidden = true;
-        window.emit(EVENTS.UI_RESET);
-
-        break;
-      }
-      case "navigate": {
-        // Manually retrieve the list of program actors known to the server,
-        // because the backend won't emit "program-linked" notifications
-        // in the case of a bfcache navigation (since no new programs are
-        // actually linked).
-        gFront.getPrograms().then(this._onProgramsAdded);
-        break;
-      }
-    }
+  _onTabNavigated: function () {
+    // Manually retrieve the list of program actors known to the server,
+    // because the backend won't emit "program-linked" notifications
+    // in the case of a bfcache navigation (since no new programs are
+    // actually linked).
+    gFront.getPrograms().then(this._onProgramsAdded);
   },
 
   /**
    * Called every time a program was linked in the debugged tab.
    */
   _onProgramLinked: function (programActor) {
     this._addProgram(programActor);
     window.emit(EVENTS.NEW_PROGRAM);
