# HG changeset patch
# User Ping Chen <remotenonsense@gmail.com>
# Date 1625098814 0
# Node ID 7b43d8647aa46af5edf47cb1fb70685adf1d6331
# Parent  e840c3781f20931c3172b4484dd71536720a56f3
Bug 1717185 - Fix mBufferedAmount and return value of send in TCPSocket. r=valentin

Previously, mBufferedAmount is only updated when copy complete. If I keep calling `send`, `NotifyCopyComplete` is only called after the last `send`.

This patch changes to update mBufferedAmount in every `send` call, so that it returns `false` correctly if buffer is full.

Differential Revision: https://phabricator.services.mozilla.com/D118258

diff --git a/dom/network/TCPSocket.cpp b/dom/network/TCPSocket.cpp
--- a/dom/network/TCPSocket.cpp
+++ b/dom/network/TCPSocket.cpp
@@ -345,16 +345,29 @@ NS_IMETHODIMP
 CopierCallbacks::OnStopRequest(nsIRequest* aRequest, nsISupports* aContext, nsresult aStatus)
 {
   mOwner->NotifyCopyComplete(aStatus);
   mOwner = nullptr;
   return NS_OK;
 }
 } // unnamed namespace
 
+void TCPSocket::CalculateBufferedAmount() {
+  // Let's update the buffered amount of data.
+  uint64_t bufferedAmount = 0;
+  for (uint32_t i = 0, len = mPendingData.Length(); i < len; ++i) {
+    nsCOMPtr<nsIInputStream> stream = mPendingData[i];
+    uint64_t available = 0;
+    if (NS_SUCCEEDED(stream->Available(&available))) {
+      bufferedAmount += available;
+    }
+  }
+  mBufferedAmount = bufferedAmount;
+}
+
 nsresult
 TCPSocket::EnsureCopying()
 {
   if (mAsyncCopierActive) {
     return NS_OK;
   }
 
   mAsyncCopierActive = true;
@@ -397,39 +410,29 @@ TCPSocket::EnsureCopying()
 
   return NS_OK;
 }
 
 void
 TCPSocket::NotifyCopyComplete(nsresult aStatus)
 {
   mAsyncCopierActive = false;
-
-  // Let's update the buffered amount of data.
-  uint64_t bufferedAmount = 0;
-  for (uint32_t i = 0, len = mPendingData.Length(); i < len; ++i) {
-    nsCOMPtr<nsIInputStream> stream = mPendingData[i];
-    uint64_t available = 0;
-    if (NS_SUCCEEDED(stream->Available(&available))) {
-      bufferedAmount += available;
-    }
-  }
-  mBufferedAmount = bufferedAmount;
+  CalculateBufferedAmount();
 
   if (mSocketBridgeParent) {
     mozilla::Unused << mSocketBridgeParent->SendUpdateBufferedAmount(BufferedAmount(),
                                                                      mTrackingNumber);
   }
 
   if (NS_FAILED(aStatus)) {
     MaybeReportErrorAndCloseIfOpen(aStatus);
     return;
   }
 
-  if (bufferedAmount != 0) {
+  if (BufferedAmount() != 0) {
     EnsureCopying();
     return;
   }
 
   // Maybe we have some empty stream. We want to have an empty queue now.
   mPendingData.Clear();
 
   // If we are waiting for initiating starttls, we can begin to
@@ -898,16 +901,17 @@ TCPSocket::Send(nsIInputStream* aStream,
   if (mWaitingForStartTLS) {
     // When we are waiting for starttls, newStream is added to pendingData
     // and will be appended to multiplexStream after tls had been set up.
     mPendingDataAfterStartTLS.AppendElement(aStream);
   } else {
     mPendingData.AppendElement(aStream);
   }
 
+  CalculateBufferedAmount();
   EnsureCopying();
 
   return !bufferFull;
 }
 
 TCPReadyState
 TCPSocket::ReadyState()
 {
diff --git a/dom/network/TCPSocket.h b/dom/network/TCPSocket.h
--- a/dom/network/TCPSocket.h
+++ b/dom/network/TCPSocket.h
@@ -168,16 +168,18 @@ private:
   nsresult CreateStream();
   // Initialize the asynchronous read operation from this socket's input stream.
   nsresult CreateInputStreamPump();
   // Send the contents of the provided input stream, which is assumed to be the given length
   // for reporting and buffering purposes.
   bool Send(nsIInputStream* aStream, uint32_t aByteLength);
   // Begin an asynchronous copy operation if one is not already in progress.
   nsresult EnsureCopying();
+  // Re-calculate buffered amount.
+  void CalculateBufferedAmount();
   // Enable TLS on this socket.
   void ActivateTLS();
   // Dispatch an error event if necessary, then dispatch a "close" event.
   nsresult MaybeReportErrorAndCloseIfOpen(nsresult status);
 
   // Helper for FireDataStringEvent/FireDataArrayEvent.
   nsresult FireDataEvent(JSContext* aCx, const nsAString& aType,
                          JS::Handle<JS::Value> aData);
diff --git a/dom/network/tests/test_tcpsocket_client_and_server_basics.js b/dom/network/tests/test_tcpsocket_client_and_server_basics.js
--- a/dom/network/tests/test_tcpsocket_client_and_server_basics.js
+++ b/dom/network/tests/test_tcpsocket_client_and_server_basics.js
@@ -258,32 +258,47 @@ async function test_basics() {
 
 
   // -- Send "big" data in both directions
   // (Enough to cross the buffering/drain threshold; 64KiB)
   let bigUint8Array = new Uint8Array(65536 + 3);
   for (let i = 0; i < bigUint8Array.length; i++) {
     bigUint8Array[i] = i % 256;
   }
+  // This can be anything from 1 to 65536. The idea is spliting and sending
+  // bigUint8Array in two chunks should trigger ondrain the same as sending
+  // bigUint8Array in one chunk.
+  let lengthOfChunk1 = 65536;
+  is(clientSocket.send(bigUint8Array.buffer, 0, lengthOfChunk1), true,
+     "Client sending chunk1 should not result in the buffer being full.");
   // Do this twice so we have confidence that the 'drain' event machinery
-  // doesn't break after the first use.
+  // doesn't break after the first use. The first time we send bigUint8Array in
+  // two chunks, the second time we send bigUint8Array in one chunk.
   for (let iSend = 0; iSend < 2; iSend++) {
     // - Send "big" data from the client to the server
-    is(clientSocket.send(bigUint8Array.buffer, 0, bigUint8Array.length), false,
+    let offset = iSend == 0 ? lengthOfChunk1 : 0;
+    is(clientSocket.send(bigUint8Array.buffer, offset, bigUint8Array.length),
+       false,
        'Client sending more than 64k should result in the buffer being full.');
     is((await clientQueue.waitForEvent()).type, 'drain',
        'The drain event should fire after a large send that indicated full.');
 
     serverReceived = await serverQueue.waitForDataWithAtLeastLength(
       bigUint8Array.length);
     assertUint8ArraysEqual(serverReceived, bigUint8Array,
                            'server received/client sent');
 
+    if (iSend == 0) {
+      is(serverSocket.send(bigUint8Array.buffer, 0, lengthOfChunk1), true,
+         "Server sending chunk1 should not result in the buffer being full.");
+    }
+
     // - Send "big" data from the server to the client
-    is(serverSocket.send(bigUint8Array.buffer, 0, bigUint8Array.length), false,
+    is(serverSocket.send(bigUint8Array.buffer, offset, bigUint8Array.length),
+       false,
        'Server sending more than 64k should result in the buffer being full.');
     is((await serverQueue.waitForEvent()).type, 'drain',
        'The drain event should fire after a large send that indicated full.');
 
     clientReceived = await clientQueue.waitForDataWithAtLeastLength(
       bigUint8Array.length);
     assertUint8ArraysEqual(clientReceived, bigUint8Array,
                            'client received/server sent');
