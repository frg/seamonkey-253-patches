# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1551999293 0
# Node ID e19e5857c7a8dc26f96df4e9487c6d0af86863f8
# Parent  423ce3616d583d98cc459d304674caee81dcb5f2
Bug 1529894 - Change zip logging initialization. r=aklotz

Instead of checking the MOZ_JAR_LOG_FILE for each log entry, only check
it once, and only check whether to log once per archive rather than once
per item.

Differential Revision: https://phabricator.services.mozilla.com/D21655

diff --git a/modules/libjar/nsZipArchive.cpp b/modules/libjar/nsZipArchive.cpp
--- a/modules/libjar/nsZipArchive.cpp
+++ b/modules/libjar/nsZipArchive.cpp
@@ -76,22 +76,18 @@ static uint16_t xtoint(const uint8_t *ii
 static uint32_t xtolong(const uint8_t *ll);
 static uint32_t HashName(const char* aName, uint16_t nameLen);
 #ifdef XP_UNIX
 static nsresult ResolveSymlink(const char *path);
 #endif
 
 class ZipArchiveLogger {
 public:
-  void Write(const nsACString &zip, const char *entry) const {
+  void Init(const char *env) {
     if (!fd) {
-      char *env = PR_GetEnv("MOZ_JAR_LOG_FILE");
-      if (!env)
-        return;
-
       nsCOMPtr<nsIFile> logFile;
       nsresult rv = NS_NewLocalFile(NS_ConvertUTF8toUTF16(env), false, getter_AddRefs(logFile));
       if (NS_FAILED(rv))
         return;
 
       // Create the log file and its parent directory (in case it doesn't exist)
       rv = logFile->Create(nsIFile::NORMAL_FILE_TYPE, 0644);
       if (NS_FAILED(rv))
@@ -115,38 +111,43 @@ public:
         return;
 #else
       rv = logFile->OpenNSPRFileDesc(PR_WRONLY|PR_CREATE_FILE|PR_APPEND, 0644, &file);
       if (NS_FAILED(rv))
         return;
 #endif
       fd = file;
     }
-    nsCString buf(zip);
-    buf.Append(' ');
-    buf.Append(entry);
-    buf.Append('\n');
-    PR_Write(fd, buf.get(), buf.Length());
+  }
+
+  void Write(const nsACString &zip, const char *entry) const {
+    if (fd) {
+      nsCString buf(zip);
+      buf.Append(' ');
+      buf.Append(entry);
+      buf.Append('\n');
+      PR_Write(fd, buf.get(), buf.Length());
+    }
   }
 
   void AddRef() {
     MOZ_ASSERT(refCnt >= 0);
     ++refCnt;
   }
 
   void Release() {
     MOZ_ASSERT(refCnt > 0);
     if ((0 == --refCnt) && fd) {
       PR_Close(fd);
       fd = nullptr;
     }
   }
 private:
   int refCnt;
-  mutable PRFileDesc *fd;
+  PRFileDesc *fd;
 };
 
 static ZipArchiveLogger zipLog;
 
 //***********************************************************
 // For every inflation the following allocations are done:
 // malloc(1 * 9520)
 // malloc(32768 * 1)
@@ -354,18 +355,23 @@ nsZipHandle::~nsZipHandle()
 //---------------------------------------------
 nsresult nsZipArchive::OpenArchive(nsZipHandle *aZipHandle, PRFileDesc *aFd)
 {
   mFd = aZipHandle;
 
   //-- get table of contents for archive
   nsresult rv = BuildFileList(aFd);
   if (NS_SUCCEEDED(rv)) {
-    if (aZipHandle->mFile)
-      aZipHandle->mFile.GetURIString(mURI);
+    if (aZipHandle->mFile && XRE_IsParentProcess()) {
+      static char *env = PR_GetEnv("MOZ_JAR_LOG_FILE");
+      if (env) {
+        zipLog.Init(env);
+        aZipHandle->mFile.GetURIString(mURI);
+      }
+    }
   }
   return rv;
 }
 
 nsresult nsZipArchive::OpenArchive(nsIFile *aFile)
 {
   RefPtr<nsZipHandle> handle;
 #if defined(XP_WIN)
@@ -457,17 +463,19 @@ nsZipItem*  nsZipArchive::GetItem(const 
     }
 MOZ_WIN_MEM_TRY_BEGIN
     nsZipItem* item = mFiles[ HashName(aEntryName, len) ];
     while (item) {
       if ((len == item->nameLength) &&
           (!memcmp(aEntryName, item->Name(), len))) {
 
         // Successful GetItem() is a good indicator that the file is about to be read
-        zipLog.Write(mURI, aEntryName);
+        if (mURI.Length()) {
+          zipLog.Write(mURI, aEntryName);
+        }
         return item; //-- found it
       }
       item = item->next;
     }
 MOZ_WIN_MEM_TRY_CATCH(return nullptr)
   }
   return nullptr;
 }
