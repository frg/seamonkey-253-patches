# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1547670022 0
# Node ID 5035c0df097405358d3233832538679c058766e4
# Parent  7ed55fb80635d3b8c5b7625d077cbd2afd392c4d
Bug 1520410 - Make --host/--target alter the output from configure --help. r=nalexander

Before bug 1322025, running configure --help along --host and/or
--target would show the options that are specific to the
host/target/cross-compilation (e.g. those with when=target_is_windows)
which was convenient. Bug 1322025 unfortunately broke that, and this
restores the feature.

Differential Revision: https://phabricator.services.mozilla.com/D16673

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -358,16 +358,27 @@ def shell(value, mozillabuild):
     shell = 'sh'
     if mozillabuild:
         shell = mozillabuild[0] + '/msys/bin/sh'
     if sys.platform == 'win32':
         shell = shell + '.exe'
     return find_program(shell)
 
 
+# This defines a reasonable shell for when running with --help.
+# If one was passed in the environment, though, fall back to that.
+@depends('--help', 'CONFIG_SHELL')
+def help_shell(help, shell):
+    if help and not shell:
+        return 'sh'
+
+
+shell = help_shell | shell
+
+
 # Python 3
 # ========
 
 option(env='PYTHON3', nargs=1, help='Python 3 interpreter (3.5 or later)')
 
 
 @depends('PYTHON3', 'MOZILLABUILD')
 @checking('for Python 3',
@@ -716,19 +727,21 @@ def split_triplet(triplet, allow_unknown
         raw_os=os,
         # Toolchains, most notably for cross compilation may use cpu-os
         # prefixes.
         toolchain='%s-%s' % (cpu, os),
     )
 
 
 # This defines a fake target/host namespace for when running with --help
-@depends('--help')
-def help_host_target(help):
-    if help:
+# If either --host or --target is passed on the command line, then fall
+# back to the real deal.
+@depends('--help', '--host', '--target')
+def help_host_target(help, host, target):
+    if help and not host and not target:
         return namespace(
             alias='unknown-unknown-unknown',
             cpu='unknown',
             bitness='unknown',
             kernel='unknown',
             os='unknown',
             endianness='unknown',
             raw_cpu='unknown',
