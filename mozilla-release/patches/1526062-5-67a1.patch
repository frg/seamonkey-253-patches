# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1549658285 0
# Node ID af6a8b058f9a64e344be618ed578979cf0491722
# Parent  9eb300e393009bb545b1ce7f109f60a242048dde
Bug 1526062 - Add a configure lint for undefined variables. r=nalexander

There are cases that can be easily detected where an undefined variable
is used, and such mistakes seem to happen more often than they should,
as all the errors fixed in the previous patches (that this new lint
caught).

Depends on D19112

Differential Revision: https://phabricator.services.mozilla.com/D19113

diff --git a/python/mozbuild/mozbuild/configure/__init__.py b/python/mozbuild/mozbuild/configure/__init__.py
--- a/python/mozbuild/mozbuild/configure/__init__.py
+++ b/python/mozbuild/mozbuild/configure/__init__.py
@@ -476,17 +476,17 @@ class ConfigureSandbox(dict):
         return super(ConfigureSandbox, self).__getitem__(key)
 
     def __setitem__(self, key, value):
         if (key in self.BUILTINS or key == '__builtins__' or
                 hasattr(self, '%s_impl' % key)):
             raise KeyError('Cannot reassign builtins')
 
         if inspect.isfunction(value) and value not in self._templates:
-            value, _ = self._prepare_function(value)
+            value = self._prepare_function(value)
 
         elif (not isinstance(value, SandboxDependsFunction) and
                 value not in self._templates and
                 not (inspect.isclass(value) and issubclass(value, Exception))):
             raise KeyError('Cannot assign `%s` because it is neither a '
                            '@depends nor a @template' % key)
 
         if isinstance(value, SandboxDependsFunction):
@@ -707,17 +707,17 @@ class ConfigureSandbox(dict):
             if c != when:
                 raise ConfigureError('@depends function needs the same `when` '
                                      'as options it depends on')
 
         def decorator(func):
             if inspect.isgeneratorfunction(func):
                 raise ConfigureError(
                     'Cannot decorate generator functions with @depends')
-            func, glob = self._prepare_function(func)
+            func = self._prepare_function(func)
             depends = DependsFunction(self, func, dependencies, when=when)
             return depends.sandboxed
 
         return decorator
 
     def include_impl(self, what, when=None):
         '''Implementation of include().
         Allows to include external files for execution in the sandbox.
@@ -736,34 +736,35 @@ class ConfigureSandbox(dict):
         '''Implementation of @template.
         This function is a decorator. Template functions are called
         immediately. They are altered so that their global namespace exposes
         a limited set of functions from os.path, as well as `depends` and
         `option`.
         Templates allow to simplify repetitive constructs, or to implement
         helper decorators and somesuch.
         '''
-        template, glob = self._prepare_function(func)
-        glob.update(
-            (k[:-len('_impl')], getattr(self, k))
-            for k in dir(self) if k.endswith('_impl') and k != 'template_impl'
-        )
-        glob.update((k, v) for k, v in self.iteritems() if k not in glob)
+        def update_globals(glob):
+            glob.update(
+                (k[:-len('_impl')], getattr(self, k))
+                for k in dir(self) if k.endswith('_impl') and k != 'template_impl'
+            )
+            glob.update((k, v) for k, v in self.iteritems() if k not in glob)
+
+        template = self._prepare_function(func, update_globals)
 
         # Any function argument to the template must be prepared to be sandboxed.
         # If the template itself returns a function (in which case, it's very
         # likely a decorator), that function must be prepared to be sandboxed as
         # well.
         def wrap_template(template):
             isfunction = inspect.isfunction
 
             def maybe_prepare_function(obj):
                 if isfunction(obj):
-                    func, _ = self._prepare_function(obj)
-                    return func
+                    return self._prepare_function(obj)
                 return obj
 
             # The following function may end up being prepared to be sandboxed,
             # so it mustn't depend on anything from the global scope in this
             # file. It can however depend on variables from the closure, thus
             # maybe_prepare_function and isfunction are declared above to be
             # available there.
             @self.wraps(template)
@@ -1023,37 +1024,39 @@ class ConfigureSandbox(dict):
             prefix=prefix,
             name=name,
             value=value,
             caller=inspect.stack()[1],
             reason=reason,
             when=when,
         ))
 
-    def _prepare_function(self, func):
+    def _prepare_function(self, func, update_globals=None):
         '''Alter the given function global namespace with the common ground
         for @depends, and @template.
         '''
         if not inspect.isfunction(func):
             raise TypeError("Unexpected type: '%s'" % type(func).__name__)
         if func in self._prepared_functions:
-            return func, func.func_globals
+            return func
 
         glob = SandboxedGlobal(
             (k, v) for k, v in func.func_globals.iteritems()
             if (inspect.isfunction(v) and v not in self._templates) or (
                 inspect.isclass(v) and issubclass(v, Exception))
         )
         glob.update(
             __builtins__=self.BUILTINS,
             __file__=self._paths[-1] if self._paths else '',
             __name__=self._paths[-1] if self._paths else '',
             os=self.OS,
             log=self.log_impl,
         )
+        if update_globals:
+            update_globals(glob)
 
         # The execution model in the sandbox doesn't guarantee the execution
         # order will always be the same for a given function, and if it uses
         # variables from a closure that are changed after the function is
         # declared, depending when the function is executed, the value of the
         # variable can differ. For consistency, we force the function to use
         # the value from the earliest it can be run, which is at declaration.
         # Note this is not entirely bullet proof (if the value is e.g. a list,
@@ -1077,9 +1080,9 @@ class ConfigureSandbox(dict):
         ))
         @self.wraps(new_func)
         def wrapped(*args, **kwargs):
             if func in self._imports:
                 self._apply_imports(func, glob)
             return new_func(*args, **kwargs)
 
         self._prepared_functions.add(wrapped)
-        return wrapped, glob
+        return wrapped
diff --git a/python/mozbuild/mozbuild/configure/lint.py b/python/mozbuild/mozbuild/configure/lint.py
--- a/python/mozbuild/mozbuild/configure/lint.py
+++ b/python/mozbuild/mozbuild/configure/lint.py
@@ -115,17 +115,17 @@ class LintSandbox(ConfigureSandbox):
                     'Keyword arguments are not allowed in @depends functions')
             self._raise_from(e, func)
 
         all_args = list(func_args.args)
         if func_args.varargs:
             all_args.append(func_args.varargs)
         used_args = set()
 
-        for op, arg in disassemble_as_iter(func):
+        for op, arg, _ in disassemble_as_iter(func):
             if op in ('LOAD_FAST', 'LOAD_CLOSURE'):
                 if arg in all_args:
                     used_args.add(arg)
 
         for num, arg in enumerate(all_args):
             if arg not in used_args:
                 dep = obj.dependencies[num]
                 if dep != self._help_option or not self._need_help_dependency(obj):
@@ -144,17 +144,17 @@ class LintSandbox(ConfigureSandbox):
                 return False
             func, glob = self.unwrap(obj._func)
             # We allow missing --help dependencies for functions that:
             # - don't use @imports
             # - don't have a closure
             # - don't use global variables
             if func in self._has_imports or func.func_closure:
                 return True
-            for op, arg in disassemble_as_iter(func):
+            for op, arg, _ in disassemble_as_iter(func):
                 if op in ('LOAD_GLOBAL', 'STORE_GLOBAL'):
                     # There is a fake os module when one is not imported,
                     # and it's allowed for functions without a --help
                     # dependency.
                     if arg == 'os' and glob.get('os') is self.OS:
                         continue
                     if arg in self.BUILTINS:
                         continue
@@ -199,14 +199,37 @@ class LintSandbox(ConfigureSandbox):
         return func, glob
 
     def wraps(self, func):
         def do_wraps(wrapper):
             self._wrapped[wrapper] = func
             return wraps(func)(wrapper)
         return do_wraps
 
+    def _prepare_function(self, func, update_globals=None):
+        wrapped = super(LintSandbox, self)._prepare_function(func, update_globals)
+        _, glob = self.unwrap(wrapped)
+        imports = set()
+        for _from, _import, _as in self._imports.get(func, ()):
+            if _as:
+                imports.add(_as)
+            else:
+                what = _import.split('.')[0]
+                imports.add(what)
+        for op, arg, line in disassemble_as_iter(func):
+            code = func.func_code
+            if op == 'LOAD_GLOBAL' and \
+                    arg not in glob and \
+                    arg not in imports and \
+                    arg not in glob['__builtins__'] and \
+                    arg not in code.co_varnames[:code.co_argcount]:
+                # Raise the same kind of error as what would happen during
+                # execution.
+                e = NameError("global name '{}' is not defined".format(arg))
+                self._raise_from(e, func, line)
+
+        return wrapped
     def imports_impl(self, _import, _from=None, _as=None):
         wrapper = super(LintSandbox, self).imports_impl(_import, _from=_from, _as=_as)
         def decorator(func):
             self._has_imports.add(func)
             return wrapper(func)
         return decorator
diff --git a/python/mozbuild/mozbuild/configure/lint_util.py b/python/mozbuild/mozbuild/configure/lint_util.py
--- a/python/mozbuild/mozbuild/configure/lint_util.py
+++ b/python/mozbuild/mozbuild/configure/lint_util.py
@@ -1,52 +1,74 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import dis
 import inspect
+import itertools
+
+
+# Like python 3.2's itertools.accumulate
+def accumulate(iterable):
+    t = 0
+    for i in iterable:
+        t += i
+        yield t
 
 
 # dis.dis only outputs to stdout. This is a modified version that
-# returns an iterator.
+# returns an iterator, and includes line numbers.
 def disassemble_as_iter(co):
     if inspect.ismethod(co):
         co = co.im_func
     if inspect.isfunction(co):
         co = co.func_code
     code = co.co_code
     n = len(code)
     i = 0
     extended_arg = 0
     free = None
+    line = 0
+    # co_lnotab is a string where each pair of consecutive character is
+    # (chr(byte_increment), chr(line_increment)), mapping bytes in co_code
+    # to line numbers relative to co_firstlineno.
+    # We want to iterate over pairs of (accumulated_byte, accumulated_line).
+    lnotab = itertools.chain(
+        itertools.izip(accumulate(ord(c) for c in co.co_lnotab[0::2]),
+                       accumulate(ord(c) for c in co.co_lnotab[1::2])),
+        (None,))
+    next_byte_line = lnotab.next()
     while i < n:
+        while next_byte_line and i >= next_byte_line[0]:
+            line = next_byte_line[1]
+            next_byte_line = lnotab.next()
         c = code[i]
         op = ord(c)
         opname = dis.opname[op]
         i += 1;
         if op >= dis.HAVE_ARGUMENT:
             arg = ord(code[i]) + ord(code[i + 1]) * 256 + extended_arg
             extended_arg = 0
             i += 2
             if op == dis.EXTENDED_ARG:
                 extended_arg = arg * 65536
                 continue
             if op in dis.hasconst:
-                yield opname, co.co_consts[arg]
+                yield opname, co.co_consts[arg], line
             elif op in dis.hasname:
-                yield opname, co.co_names[arg]
+                yield opname, co.co_names[arg], line
             elif op in dis.hasjrel:
-                yield opname, i + arg
+                yield opname, i + arg, line
             elif op in dis.haslocal:
-                yield opname, co.co_varnames[arg]
+                yield opname, co.co_varnames[arg], line
             elif op in dis.hascompare:
-                yield opname, dis.cmp_op[arg]
+                yield opname, dis.cmp_op[arg], line
             elif op in dis.hasfree:
                 if free is None:
                     free = co.co_cellvars + co.co_freevars
-                yield opname, free[arg]
+                yield opname, free[arg], line
             else:
-                yield opname, None
+                yield opname, None, line
         else:
-            yield opname, None
+            yield opname, None, line
diff --git a/python/mozbuild/mozbuild/test/configure/test_lint.py b/python/mozbuild/mozbuild/test/configure/test_lint.py
--- a/python/mozbuild/mozbuild/test/configure/test_lint.py
+++ b/python/mozbuild/mozbuild/test/configure/test_lint.py
@@ -288,11 +288,26 @@ class TestLint(unittest.TestCase):
 
                 include(foo)
             '''):
                 self.lint_test()
 
         self.assertEquals(e.exception.message,
                           "The dependency on `qux` is unused")
 
+    def test_undefined_global(self):
+        with self.assertRaisesFromLine(NameError, 6) as e:
+            with self.moz_configure('''
+                option(env='FOO', help='foo')
+                @depends('FOO')
+                def foo(value):
+                    if value:
+                        return unknown
+                    return value
+            '''):
+                self.lint_test()
+
+        self.assertEquals(e.exception.message,
+                          "global name 'unknown' is not defined")
+
 
 if __name__ == '__main__':
     main()
