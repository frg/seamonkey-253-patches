# HG changeset patch
# User Dragan Mladjenovic <dragan.mladjenovic>
# Date 1517300604 -3600
# Node ID 4f302023b7f59f438c18a66a764fc1d50d6cffa9
# Parent  2ca7c24f9115202d48b6f6e122c7b45adbae3953
Bug 1435185 : [MIPS32] [MIPS32] Ensure that Baldr and Rabaldr locals are naturally aligned ; r=luke

diff --git a/js/src/wasm/WasmFrameIter.cpp b/js/src/wasm/WasmFrameIter.cpp
--- a/js/src/wasm/WasmFrameIter.cpp
+++ b/js/src/wasm/WasmFrameIter.cpp
@@ -272,48 +272,34 @@ static const unsigned PoppedTLSReg = 0;
 static const unsigned BeforePushRetAddr = 0;
 static const unsigned PushedRetAddr = 0;
 static const unsigned PushedTLS = 1;
 static const unsigned PushedFP = 1;
 static const unsigned SetFP = 0;
 static const unsigned PoppedFP = 0;
 static const unsigned PoppedTLSReg = 0;
 #elif defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
-static const unsigned BeforePushRetAddr = 0;
 static const unsigned PushedRetAddr = 8;
-static const unsigned PushedTLS = 16;
-static const unsigned PushedFP = 24;
-static const unsigned SetFP = 28;
-static const unsigned PoppedFP = 16;
-static const unsigned PoppedTLSReg = 8;
+static const unsigned PushedTLS = 12;
+static const unsigned PushedFP = 16;
+static const unsigned SetFP = 20;
+static const unsigned PoppedFP = 8;
+static const unsigned PoppedTLSReg = 4;
 #elif defined(JS_CODEGEN_NONE)
 // Synthetic values to satisfy asserts and avoid compiler warnings.
 static const unsigned PushedRetAddr = 0;
 static const unsigned PushedTLS = 1;
 static const unsigned PushedFP = 2;
 static const unsigned SetFP = 3;
 static const unsigned PoppedFP = 4;
 static const unsigned PoppedTLSReg = 5;
 #else
 # error "Unknown architecture!"
 #endif
 
-static void
-PushRetAddr(MacroAssembler& masm, unsigned entry)
-{
-#if defined(JS_CODEGEN_ARM)
-    MOZ_ASSERT(masm.currentOffset() - entry == BeforePushRetAddr);
-    masm.push(lr);
-#elif defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
-    MOZ_ASSERT(masm.currentOffset() - entry == BeforePushRetAddr);
-    masm.push(ra);
-#else
-    // The x86/x64 call instruction pushes the return address.
-#endif
-}
 
 static void
 LoadActivation(MacroAssembler& masm, const Register& dest)
 {
     // WasmCall pushes a JitActivation.
     masm.loadPtr(Address(WasmTlsReg, offsetof(wasm::TlsData, cx)), dest);
     masm.loadPtr(Address(dest, JSContext::offsetOfActivation()), dest);
 }
@@ -345,32 +331,51 @@ static void
 GenerateCallablePrologue(MacroAssembler& masm, unsigned framePushed, ExitReason reason,
                          uint32_t* entry, uint32_t* tierEntry, CompileMode mode, uint32_t funcIndex)
 {
     // ProfilingFrameIterator needs to know the offsets of several key
     // instructions from entry. To save space, we make these offsets static
     // constants and assert that they match the actual codegen below. On ARM,
     // this requires AutoForbidPools to prevent a constant pool from being
     // randomly inserted between two instructions.
-    {
-#if defined(JS_CODEGEN_ARM)
-        AutoForbidPools afp(&masm, /* number of instructions in scope = */ 7);
-#endif
+#if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
         *entry = masm.currentOffset();
 
-        PushRetAddr(masm, *entry);
+        masm.subFromStackPtr(Imm32(sizeof(Frame)));
+        masm.storePtr(ra, Address(StackPointer, offsetof(Frame, returnAddress)));
+        MOZ_ASSERT_IF(!masm.oom(), PushedRetAddr == masm.currentOffset() - *entry);
+        masm.storePtr(WasmTlsReg, Address(StackPointer, offsetof(Frame, tls)));
+        MOZ_ASSERT_IF(!masm.oom(), PushedTLS == masm.currentOffset() - *entry);
+        masm.storePtr(FramePointer, Address(StackPointer, offsetof(Frame, callerFP)));
+        MOZ_ASSERT_IF(!masm.oom(), PushedFP == masm.currentOffset() - *entry);
+        masm.moveStackPtrTo(FramePointer);
+        MOZ_ASSERT_IF(!masm.oom(), SetFP == masm.currentOffset() - *entry);
+#else
+    {
+# if defined(JS_CODEGEN_ARM)
+        AutoForbidPools afp(&masm, /* number of instructions in scope = */ 7);
+
+        *entry = masm.currentOffset();
+
+        MOZ_ASSERT(BeforePushRetAddr == 0);
+        masm.push(lr);
+# else
+        *entry = masm.currentOffset();
+        // The x86/x64 call instruction pushes the return address.
+# endif
+
         MOZ_ASSERT_IF(!masm.oom(), PushedRetAddr == masm.currentOffset() - *entry);
         masm.push(WasmTlsReg);
         MOZ_ASSERT_IF(!masm.oom(), PushedTLS == masm.currentOffset() - *entry);
         masm.push(FramePointer);
         MOZ_ASSERT_IF(!masm.oom(), PushedFP == masm.currentOffset() - *entry);
         masm.moveStackPtrTo(FramePointer);
         MOZ_ASSERT_IF(!masm.oom(), SetFP == masm.currentOffset() - *entry);
     }
-
+#endif
     // Tiering works as follows.  The Code owns a jumpTable, which has one
     // pointer-sized element for each function up to the largest funcIndex in
     // the module.  Each table element is an address into the Tier-1 or the
     // Tier-2 function at that index; the elements are updated when Tier-2 code
     // becomes available.  The Tier-1 function will unconditionally jump to this
     // address.  The table elements are written racily but without tearing when
     // Tier-2 compilation is finished.
     //
@@ -404,41 +409,53 @@ GenerateCallableEpilogue(MacroAssembler&
                          uint32_t* ret)
 {
     if (framePushed)
         masm.addToStackPtr(Imm32(framePushed));
 
     if (!reason.isNone())
         ClearExitFP(masm, ABINonArgReturnVolatileReg);
 
+    DebugOnly<uint32_t> poppedFP;
+    DebugOnly<uint32_t> poppedTlsReg;
+
+#if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
+
+    masm.loadPtr(Address(StackPointer, offsetof(Frame, callerFP)), FramePointer);
+    poppedFP = masm.currentOffset();
+    masm.loadPtr(Address(StackPointer, offsetof(Frame, tls)), WasmTlsReg);
+    poppedTlsReg = masm.currentOffset();
+    masm.loadPtr(Address(StackPointer, offsetof(Frame, returnAddress)), ra);
+
+    *ret = masm.currentOffset();
+    masm.as_jr(ra);
+    masm.addToStackPtr(Imm32(sizeof(Frame)));
+
+#else
     // Forbid pools for the same reason as described in GenerateCallablePrologue.
-#if defined(JS_CODEGEN_ARM)
+# if defined(JS_CODEGEN_ARM)
     AutoForbidPools afp(&masm, /* number of instructions in scope = */ 7);
-#endif
+# endif
 
     // There is an important ordering constraint here: fp must be repointed to
     // the caller's frame before any field of the frame currently pointed to by
     // fp is popped: asynchronous signal handlers (which use stack space
     // starting at sp) could otherwise clobber these fields while they are still
     // accessible via fp (fp fields are read during frame iteration which is
     // *also* done asynchronously).
 
     masm.pop(FramePointer);
-    DebugOnly<uint32_t> poppedFP = masm.currentOffset();
+    poppedFP = masm.currentOffset();
 
     masm.pop(WasmTlsReg);
-    DebugOnly<uint32_t> poppedTlsReg = masm.currentOffset();
+    poppedTlsReg = masm.currentOffset();
 
-#if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
-    masm.pop(ra);
-    *ret = masm.currentOffset();
-    masm.branch(ra);
-#else
     *ret = masm.currentOffset();
     masm.ret();
+
 #endif
 
     MOZ_ASSERT_IF(!masm.oom(), PoppedFP == *ret - poppedFP);
     MOZ_ASSERT_IF(!masm.oom(), PoppedTLSReg == *ret - poppedTlsReg);
 }
 
 void
 wasm::GenerateFunctionPrologue(MacroAssembler& masm, unsigned framePushed, const SigIdDesc& sigId,
@@ -720,23 +737,19 @@ js::wasm::StartUnwinding(const RegisterS
       case CodeRange::Function:
       case CodeRange::FarJumpIsland:
       case CodeRange::ImportJitExit:
       case CodeRange::ImportInterpExit:
       case CodeRange::BuiltinThunk:
       case CodeRange::OldTrapExit:
       case CodeRange::DebugTrap:
 #if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
-        if ((offsetFromEntry >= BeforePushRetAddr && offsetFromEntry < PushedFP) || codeRange->isThunk()) {
-            // See BUG 1407986.
-            // On MIPS push is emulated by two instructions: adjusting the sp
-            // and storing the value to sp.
-            // Execution might be interrupted in between the two operation so we
-            // have to relay on register state instead of state saved on stack
-            // until the wasm::Frame is completely built.
+        if (offsetFromEntry < PushedFP || codeRange->isThunk()) {
+            // On MIPS we relay on register state instead of state saved on
+            // stack until the wasm::Frame is completely built.
             // On entry the return address is in ra (registers.lr) and
             // fp holds the caller's fp.
             fixedPC = (uint8_t*) registers.lr;
             fixedFP = fp;
             AssertMatchesCallSite(fixedPC, fixedFP);
         } else
 #elif defined(JS_CODEGEN_ARM) || defined(JS_CODEGEN_ARM64)
         if (offsetFromEntry == BeforePushRetAddr || codeRange->isThunk()) {
@@ -759,39 +772,36 @@ js::wasm::StartUnwinding(const RegisterS
             fixedFP = fp;
             AssertMatchesCallSite(fixedPC, fixedFP);
         } else if (offsetFromEntry == PushedFP) {
             // The full Frame has been pushed; fp is still the caller's fp.
             MOZ_ASSERT(fp == reinterpret_cast<Frame*>(sp)->callerFP);
             fixedPC = reinterpret_cast<Frame*>(sp)->returnAddress;
             fixedFP = fp;
             AssertMatchesCallSite(fixedPC, fixedFP);
+#if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
+        } else if (offsetInCode >= codeRange->ret() - PoppedFP &&
+                   offsetInCode <= codeRange->ret())
+        {
+            (void)PoppedTLSReg;
+            // The fixedFP field of the Frame has been loaded into fp.
+            // The ra and TLS might also be loaded, but the Frame structure is
+            // still on stack, so we can acess the ra form there.
+            MOZ_ASSERT(fp == reinterpret_cast<Frame*>(sp)->callerFP);
+            fixedPC = reinterpret_cast<Frame*>(sp)->returnAddress;
+            fixedFP = fp;
+            AssertMatchesCallSite(fixedPC, fixedFP);
+#else
         } else if (offsetInCode >= codeRange->ret() - PoppedFP &&
                    offsetInCode < codeRange->ret() - PoppedTLSReg)
         {
             // The fixedFP field of the Frame has been popped into fp.
             fixedPC = sp[1];
             fixedFP = fp;
             AssertMatchesCallSite(fixedPC, fixedFP);
-#if defined(JS_CODEGEN_MIPS32) || defined(JS_CODEGEN_MIPS64)
-        } else if (offsetInCode >= codeRange->ret() - PoppedTLSReg &&
-                   offsetInCode < codeRange->ret())
-        {
-            // The fixedFP field of the Frame has been popped into fp, but the
-            // exit reason hasn't been popped yet.
-            fixedPC = sp[0];
-            fixedFP = fp;
-            AssertMatchesCallSite(fixedPC, fixedFP);
-        } else if (offsetInCode == codeRange->ret()) {
-            // Both the TLS, fixedFP and ra have been popped and fp now
-            // points to the caller's frame.
-            fixedPC = (uint8_t*) registers.lr;
-            fixedFP = fp;
-            AssertMatchesCallSite(fixedPC, fixedFP);
-#else
         } else if (offsetInCode == codeRange->ret()) {
             // Both the TLS and fixedFP fields have been popped and fp now
             // points to the caller's frame.
             fixedPC = sp[0];
             fixedFP = fp;
             AssertMatchesCallSite(fixedPC, fixedFP);
 #endif
         } else {
diff --git a/js/src/wasm/WasmTypes.h b/js/src/wasm/WasmTypes.h
--- a/js/src/wasm/WasmTypes.h
+++ b/js/src/wasm/WasmTypes.h
@@ -1853,16 +1853,21 @@ struct Frame
     // The caller's Frame*. See GenerateCallableEpilogue for why this must be
     // the first field of wasm::Frame (in a downward-growing stack).
     Frame* callerFP;
 
     // The saved value of WasmTlsReg on entry to the function. This is
     // effectively the callee's instance.
     TlsData* tls;
 
+#if defined(JS_CODEGEN_MIPS32)
+    // Double word aligned frame ensures correct alignment for wasm locals
+    // on architectures that require the stack alignment to be more than word size.
+    uintptr_t padding_;
+#endif
     // The return address pushed by the call (in the case of ARM/MIPS the return
     // address is pushed by the first instruction of the prologue).
     void* returnAddress;
 
     // Helper functions:
 
     Instance* instance() const { return tls->instance; }
 };
@@ -1904,18 +1909,20 @@ class DebugFrame
             bool hasCachedSavedFrame_ : 1;
             bool hasCachedReturnJSValue_ : 1;
         };
         void* flagsWord_;
     };
 
     // Avoid -Wunused-private-field warnings.
   protected:
-#if JS_BITS_PER_WORD == 32
-    uint32_t padding_;  // See alignmentStaticAsserts().
+#if JS_BITS_PER_WORD == 32 && !defined(JS_CODEGEN_MIPS32)
+    // See alignmentStaticAsserts().
+    // For MIPS32 padding is already incorporated in the frame.
+    uint32_t padding_;
 #endif
 
   private:
     // The Frame goes at the end since the stack grows down.
     Frame frame_;
 
   public:
     static DebugFrame* from(Frame* fp);
