# HG changeset patch
# User Christoph Kerschbaumer <ckerschb@christophkerschbaumer.com>
# Date 1565955357 0
# Node ID 61a0b22d978af8c1c29a4ac84c023727d0621d66
# Parent  cdcec4945879f67d2c27c6f0abd1947c7f52f200
Bug 1404367: Pass LoadInfo of channel to nsViewSourceChannel::Init. r=Gijs

Differential Revision: https://phabricator.services.mozilla.com/D42296

diff --git a/netwerk/protocol/viewsource/nsViewSourceChannel.cpp b/netwerk/protocol/viewsource/nsViewSourceChannel.cpp
--- a/netwerk/protocol/viewsource/nsViewSourceChannel.cpp
+++ b/netwerk/protocol/viewsource/nsViewSourceChannel.cpp
@@ -33,19 +33,17 @@ NS_INTERFACE_MAP_BEGIN(nsViewSourceChann
     NS_INTERFACE_MAP_ENTRY_CONDITIONAL(nsIApplicationCacheChannel, mApplicationCacheChannel)
     NS_INTERFACE_MAP_ENTRY_CONDITIONAL(nsIUploadChannel, mUploadChannel)
     NS_INTERFACE_MAP_ENTRY_CONDITIONAL(nsIFormPOSTActionChannel, mPostChannel)
     NS_INTERFACE_MAP_ENTRY_AMBIGUOUS(nsIRequest, nsIViewSourceChannel)
     NS_INTERFACE_MAP_ENTRY_AMBIGUOUS(nsIChannel, nsIViewSourceChannel)
     NS_INTERFACE_MAP_ENTRY_AMBIGUOUS(nsISupports, nsIViewSourceChannel)
 NS_INTERFACE_MAP_END
 
-nsresult
-nsViewSourceChannel::Init(nsIURI* uri)
-{
+nsresult nsViewSourceChannel::Init(nsIURI* uri, nsILoadInfo* aLoadInfo) {
     mOriginalURI = uri;
 
     nsAutoCString path;
     nsresult rv = uri->GetPath(path);
     if (NS_FAILED(rv))
       return rv;
 
     nsCOMPtr<nsIIOService> pService(do_GetIOService(&rv));
@@ -57,32 +55,22 @@ nsViewSourceChannel::Init(nsIURI* uri)
       return rv;
 
     // prevent viewing source of javascript URIs (see bug 204779)
     if (scheme.LowerCaseEqualsLiteral("javascript")) {
       NS_WARNING("blocking view-source:javascript:");
       return NS_ERROR_INVALID_ARG;
     }
 
-    // This function is called from within nsViewSourceHandler::NewChannel2
-    // and sets the right loadInfo right after returning from this function.
-    // Until then we follow the principal of least privilege and use
-    // nullPrincipal as the loadingPrincipal and the least permissive
-    // securityflag.
-    nsCOMPtr<nsIPrincipal> nullPrincipal = NullPrincipal::Create();
+    nsCOMPtr<nsIURI> newChannelURI;
+    rv = pService->NewURI(path, nullptr, nullptr, getter_AddRefs(newChannelURI));
+    NS_ENSURE_SUCCESS(rv, rv);
 
-    rv = pService->NewChannel2(path,
-                               nullptr, // aOriginCharset
-                               nullptr, // aCharSet
-                               nullptr, // aLoadingNode
-                               nullPrincipal,
-                               nullptr, // aTriggeringPrincipal
-                               nsILoadInfo::SEC_REQUIRE_SAME_ORIGIN_DATA_IS_BLOCKED,
-                               nsIContentPolicy::TYPE_OTHER,
-                               getter_AddRefs(mChannel));
+    rv = pService->NewChannelFromURIWithLoadInfo(newChannelURI, aLoadInfo,
+                                                 getter_AddRefs(mChannel));
     NS_ENSURE_SUCCESS(rv, rv);
 
     mIsSrcdocChannel = false;
 
     mChannel->SetOriginalURI(mOriginalURI);
     mHttpChannel = do_QueryInterface(mChannel);
     mHttpChannelInternal = do_QueryInterface(mChannel);
     mCachingChannel = do_QueryInterface(mChannel);
diff --git a/netwerk/protocol/viewsource/nsViewSourceChannel.h b/netwerk/protocol/viewsource/nsViewSourceChannel.h
--- a/netwerk/protocol/viewsource/nsViewSourceChannel.h
+++ b/netwerk/protocol/viewsource/nsViewSourceChannel.h
@@ -43,17 +43,17 @@ public:
     NS_FORWARD_SAFE_NSIFORMPOSTACTIONCHANNEL(mPostChannel)
     NS_FORWARD_SAFE_NSIHTTPCHANNELINTERNAL(mHttpChannelInternal)
 
     // nsViewSourceChannel methods:
     nsViewSourceChannel()
         : mIsDocument(false)
         , mOpened(false) {}
 
-    MOZ_MUST_USE nsresult Init(nsIURI* uri);
+    MOZ_MUST_USE nsresult Init(nsIURI* uri, nsILoadInfo* aLoadInfo);
 
     MOZ_MUST_USE nsresult InitSrcdoc(nsIURI* aURI,
                                      nsIURI* aBaseURI,
                                      const nsAString &aSrcdoc,
                                      nsILoadInfo* aLoadInfo);
 
     // Updates or sets the result principal URI of the underlying channel's
     // loadinfo to be prefixed with the "view-source:" schema as:
diff --git a/netwerk/protocol/viewsource/nsViewSourceHandler.cpp b/netwerk/protocol/viewsource/nsViewSourceHandler.cpp
--- a/netwerk/protocol/viewsource/nsViewSourceHandler.cpp
+++ b/netwerk/protocol/viewsource/nsViewSourceHandler.cpp
@@ -98,24 +98,17 @@ nsViewSourceHandler::NewChannel2(nsIURI*
                                  nsIChannel** result)
 {
     NS_ENSURE_ARG_POINTER(uri);
     nsViewSourceChannel *channel = new nsViewSourceChannel();
     if (!channel)
         return NS_ERROR_OUT_OF_MEMORY;
     NS_ADDREF(channel);
 
-    nsresult rv = channel->Init(uri);
-    if (NS_FAILED(rv)) {
-        NS_RELEASE(channel);
-        return rv;
-    }
-
-    // set the loadInfo on the new channel
-    rv = channel->SetLoadInfo(aLoadInfo);
+    nsresult rv = channel->Init(uri, aLoadInfo);
     if (NS_FAILED(rv)) {
         NS_RELEASE(channel);
         return rv;
     }
 
     *result = static_cast<nsIViewSourceChannel*>(channel);
     return NS_OK;
 }
