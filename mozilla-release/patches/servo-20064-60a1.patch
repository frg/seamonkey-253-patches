# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1518810230 18000
# Node ID 97908780b56a4498eea6857df76eaa5a9a398be4
# Parent  3c847060a57be0268d1c3aef36dce4042d606ff0
servo: Merge #20064 - style: Trivially cleanup length parsing (from emilio:cleanup-length-parsing); r=nox

Mostly formatting signatures properly, but also removing useless functions and
stuff.

Source-Repo: https://github.com/servo/servo
Source-Revision: d092c2e877fdbae241d6f4b53d61701d7d50e346

diff --git a/servo/components/style/parser.rs b/servo/components/style/parser.rs
--- a/servo/components/style/parser.rs
+++ b/servo/components/style/parser.rs
@@ -108,16 +108,22 @@ impl<'a> ParserContext<'a> {
             url_data: context.url_data,
             rule_type: Some(rule_type),
             parsing_mode: context.parsing_mode,
             quirks_mode: context.quirks_mode,
             namespaces: Some(namespaces),
         }
     }
 
+    /// Whether we're in a @page rule.
+    #[inline]
+    pub fn in_page_rule(&self) -> bool {
+        self.rule_type.map_or(false, |rule_type| rule_type == CssRuleType::Page)
+    }
+
     /// Get the rule type, which assumes that one is available.
     pub fn rule_type(&self) -> CssRuleType {
         self.rule_type.expect("Rule type expected, but none was found.")
     }
 
     /// Record a CSS parse error with this context’s error reporting.
     pub fn log_css_error<R>(
         &self,
diff --git a/servo/components/style/values/specified/length.rs b/servo/components/style/values/specified/length.rs
--- a/servo/components/style/values/specified/length.rs
+++ b/servo/components/style/values/specified/length.rs
@@ -11,17 +11,16 @@ use cssparser::{Parser, Token};
 use euclid::Size2D;
 use font_metrics::FontMetricsQueryResult;
 use parser::{Parse, ParserContext};
 use std::cmp;
 #[allow(unused_imports)] use std::ascii::AsciiExt;
 use std::ops::{Add, Mul};
 use style_traits::{ParseError, StyleParseErrorKind};
 use style_traits::values::specified::AllowedNumericType;
-use stylesheets::CssRuleType;
 use super::{AllowQuirks, Number, ToComputedValue, Percentage};
 use values::{Auto, CSSFloat, Either, None_, Normal};
 use values::computed::{self, CSSPixelLength, Context, ExtremumLength};
 use values::generics::NonNegative;
 use values::specified::NonNegativeNumber;
 use values::specified::calc::CalcNode;
 
 pub use values::specified::calc::CalcLengthOrPercentage;
@@ -417,53 +416,55 @@ impl Mul<CSSFloat> for NoCalcLength {
             NoCalcLength::ViewportPercentage(v) => NoCalcLength::ViewportPercentage(v * scalar),
             NoCalcLength::ServoCharacterWidth(_) => panic!("Can't multiply ServoCharacterWidth!"),
         }
     }
 }
 
 impl NoCalcLength {
     /// Parse a given absolute or relative dimension.
-    pub fn parse_dimension(context: &ParserContext, value: CSSFloat, unit: &str)
-                           -> Result<NoCalcLength, ()> {
-        let in_page_rule = context.rule_type.map_or(false, |rule_type| rule_type == CssRuleType::Page);
+    pub fn parse_dimension(
+        context: &ParserContext,
+        value: CSSFloat,
+        unit: &str,
+    ) -> Result<Self, ()> {
         match_ignore_ascii_case! { unit,
             "px" => Ok(NoCalcLength::Absolute(AbsoluteLength::Px(value))),
             "in" => Ok(NoCalcLength::Absolute(AbsoluteLength::In(value))),
             "cm" => Ok(NoCalcLength::Absolute(AbsoluteLength::Cm(value))),
             "mm" => Ok(NoCalcLength::Absolute(AbsoluteLength::Mm(value))),
             "q" => Ok(NoCalcLength::Absolute(AbsoluteLength::Q(value))),
             "pt" => Ok(NoCalcLength::Absolute(AbsoluteLength::Pt(value))),
             "pc" => Ok(NoCalcLength::Absolute(AbsoluteLength::Pc(value))),
             // font-relative
             "em" => Ok(NoCalcLength::FontRelative(FontRelativeLength::Em(value))),
             "ex" => Ok(NoCalcLength::FontRelative(FontRelativeLength::Ex(value))),
             "ch" => Ok(NoCalcLength::FontRelative(FontRelativeLength::Ch(value))),
             "rem" => Ok(NoCalcLength::FontRelative(FontRelativeLength::Rem(value))),
             // viewport percentages
             "vw" => {
-                if in_page_rule {
+                if context.in_page_rule() {
                     return Err(())
                 }
                 Ok(NoCalcLength::ViewportPercentage(ViewportPercentageLength::Vw(value)))
             },
             "vh" => {
-                if in_page_rule {
+                if context.in_page_rule() {
                     return Err(())
                 }
                 Ok(NoCalcLength::ViewportPercentage(ViewportPercentageLength::Vh(value)))
             },
             "vmin" => {
-                if in_page_rule {
+                if context.in_page_rule() {
                     return Err(())
                 }
                 Ok(NoCalcLength::ViewportPercentage(ViewportPercentageLength::Vmin(value)))
             },
             "vmax" => {
-                if in_page_rule {
+                if context.in_page_rule() {
                     return Err(())
                 }
                 Ok(NoCalcLength::ViewportPercentage(ViewportPercentageLength::Vmax(value)))
             },
             _ => Err(())
         }
     }
 
@@ -561,35 +562,31 @@ impl Mul<CSSFloat> for ViewportPercentag
 
 impl Length {
     #[inline]
     /// Returns a `zero` length.
     pub fn zero() -> Length {
         Length::NoCalc(NoCalcLength::zero())
     }
 
-    /// Parse a given absolute or relative dimension.
-    pub fn parse_dimension(context: &ParserContext, value: CSSFloat, unit: &str)
-                           -> Result<Length, ()> {
-        NoCalcLength::parse_dimension(context, value, unit).map(Length::NoCalc)
-    }
-
     #[inline]
-    fn parse_internal<'i, 't>(context: &ParserContext,
-                              input: &mut Parser<'i, 't>,
-                              num_context: AllowedNumericType,
-                              allow_quirks: AllowQuirks)
-                              -> Result<Length, ParseError<'i>> {
+    fn parse_internal<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+        num_context: AllowedNumericType,
+        allow_quirks: AllowQuirks,
+    ) -> Result<Self, ParseError<'i>> {
         // FIXME: remove early returns when lifetimes are non-lexical
         {
             let location = input.current_source_location();
             let token = input.next()?;
             match *token {
                 Token::Dimension { value, ref unit, .. } if num_context.is_ok(context.parsing_mode, value) => {
-                    return Length::parse_dimension(context, value, unit)
+                    return NoCalcLength::parse_dimension(context, value, unit)
+                        .map(Length::NoCalc)
                         .map_err(|()| location.new_unexpected_token_error(token.clone()))
                 }
                 Token::Number { value, .. } if num_context.is_ok(context.parsing_mode, value) => {
                     if value != 0. &&
                        !context.parsing_mode.allows_unitless_lengths() &&
                        !allow_quirks.allowed(context.quirks_mode) {
                         return Err(location.new_custom_error(StyleParseErrorKind::UnspecifiedError))
                     }
@@ -754,22 +751,22 @@ impl From<computed::Percentage> for Leng
 
 impl LengthOrPercentage {
     #[inline]
     /// Returns a `zero` length.
     pub fn zero() -> LengthOrPercentage {
         LengthOrPercentage::Length(NoCalcLength::zero())
     }
 
-    fn parse_internal<'i, 't>(context: &ParserContext,
-                              input: &mut Parser<'i, 't>,
-                              num_context: AllowedNumericType,
-                              allow_quirks: AllowQuirks)
-                              -> Result<LengthOrPercentage, ParseError<'i>>
-    {
+    fn parse_internal<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+        num_context: AllowedNumericType,
+        allow_quirks: AllowQuirks,
+    ) -> Result<Self, ParseError<'i>> {
         // FIXME: remove early returns when lifetimes are non-lexical
         {
             let location = input.current_source_location();
             let token = input.next()?;
             match *token {
                 Token::Dimension { value, ref unit, .. } if num_context.is_ok(context.parsing_mode, value) => {
                     return NoCalcLength::parse_dimension(context, value, unit)
                         .map(LengthOrPercentage::Length)
@@ -856,21 +853,22 @@ impl From<NoCalcLength> for LengthOrPerc
 impl From<computed::Percentage> for LengthOrPercentageOrAuto {
     #[inline]
     fn from(pc: computed::Percentage) -> Self {
         LengthOrPercentageOrAuto::Percentage(pc)
     }
 }
 
 impl LengthOrPercentageOrAuto {
-    fn parse_internal<'i, 't>(context: &ParserContext,
-                              input: &mut Parser<'i, 't>,
-                              num_context: AllowedNumericType,
-                              allow_quirks: AllowQuirks)
-                              -> Result<Self, ParseError<'i>> {
+    fn parse_internal<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+        num_context: AllowedNumericType,
+        allow_quirks: AllowQuirks,
+    ) -> Result<Self, ParseError<'i>> {
         // FIXME: remove early returns when lifetimes are non-lexical
         {
             let location = input.current_source_location();
             let token = input.next()?;
             match *token {
                 Token::Dimension { value, ref unit, .. } if num_context.is_ok(context.parsing_mode, value) => {
                     return NoCalcLength::parse_dimension(context, value, unit)
                         .map(LengthOrPercentageOrAuto::Length)
@@ -963,22 +961,22 @@ impl LengthOrPercentageOrAuto {
 pub enum LengthOrPercentageOrNone {
     Length(NoCalcLength),
     Percentage(computed::Percentage),
     Calc(Box<CalcLengthOrPercentage>),
     None,
 }
 
 impl LengthOrPercentageOrNone {
-    fn parse_internal<'i, 't>(context: &ParserContext,
-                              input: &mut Parser<'i, 't>,
-                              num_context: AllowedNumericType,
-                              allow_quirks: AllowQuirks)
-                              -> Result<LengthOrPercentageOrNone, ParseError<'i>>
-    {
+    fn parse_internal<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+        num_context: AllowedNumericType,
+        allow_quirks: AllowQuirks,
+    ) -> Result<Self, ParseError<'i>> {
         // FIXME: remove early returns when lifetimes are non-lexical
         {
             let location = input.current_source_location();
             let token = input.next()?;
             match *token {
                 Token::Dimension { value, ref unit, .. } if num_context.is_ok(context.parsing_mode, value) => {
                     return NoCalcLength::parse_dimension(context, value, unit)
                         .map(LengthOrPercentageOrNone::Length)
diff --git a/servo/components/style_traits/lib.rs b/servo/components/style_traits/lib.rs
--- a/servo/components/style_traits/lib.rs
+++ b/servo/components/style_traits/lib.rs
@@ -208,21 +208,23 @@ bitflags! {
         /// In SVG; out-of-range values are not treated as an error in parsing.
         /// <https://www.w3.org/TR/SVG/implnote.html#RangeClamping>
         const ALLOW_ALL_NUMERIC_VALUES = 0x02;
     }
 }
 
 impl ParsingMode {
     /// Whether the parsing mode allows unitless lengths for non-zero values to be intpreted as px.
+    #[inline]
     pub fn allows_unitless_lengths(&self) -> bool {
         self.intersects(ParsingMode::ALLOW_UNITLESS_LENGTH)
     }
 
     /// Whether the parsing mode allows all numeric values.
+    #[inline]
     pub fn allows_all_numeric_values(&self) -> bool {
         self.intersects(ParsingMode::ALLOW_ALL_NUMERIC_VALUES)
     }
 }
 
 #[cfg(feature = "servo")]
 /// Speculatively execute paint code in the worklet thread pool.
 pub trait SpeculativePainter: Send + Sync {
