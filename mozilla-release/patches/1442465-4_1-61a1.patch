# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1521425534 -32400
# Node ID 67af8170257afb47e77ccad1a449733b6257fe07
# Parent  70132e48b6db1970e97cbfa3b4ff3dec87d2b513
Bug 1442465 - Part 4.1: Stop unnecessarily awaiting on BrowserTestUtils.removeTab (non-simple part). r=dao

diff --git a/browser/base/content/test/general/browser_blockHPKP.js b/browser/base/content/test/general/browser_blockHPKP.js
--- a/browser/base/content/test/general/browser_blockHPKP.js
+++ b/browser/base/content/test/general/browser_blockHPKP.js
@@ -87,13 +87,12 @@ function pinningRemovalLoaded() {
   BrowserTestUtils.loadURI(gBrowser.selectedBrowser, "https://" + kBadPinningDomain).then(function() {
     return BrowserTestUtils.browserLoaded(gBrowser.selectedBrowser);
   }).then(badPinningPageLoaded);
 }
 
 // Finally, we should successfully load
 // https://bad.include-subdomains.pinning-dynamic.example.com.
 function badPinningPageLoaded() {
-  BrowserTestUtils.removeTab(gBrowser.selectedTab).then(function() {
-    ok(true, "load complete");
-    finish();
-  });
+  BrowserTestUtils.removeTab(gBrowser.selectedTab);
+  ok(true, "load complete");
+  finish();
 }
diff --git a/browser/base/content/test/general/browser_bug553455.js b/browser/base/content/test/general/browser_bug553455.js
--- a/browser/base/content/test/general/browser_bug553455.js
+++ b/browser/base/content/test/general/browser_bug553455.js
@@ -174,21 +174,20 @@ async function waitForInstallDialog() {
 
   // Override the countdown timer on the accept button
   let button = domwindow.document.documentElement.getButton("accept");
   button.disabled = false;
 
   return null;
 }
 
-function removeTab() {
-  return Promise.all([
-    waitForNotificationClose(),
-    BrowserTestUtils.removeTab(gBrowser.selectedTab)
-  ]);
+function removeTabAndWaitForNotificationClose() {
+  let closePromise = waitForNotificationClose();
+  BrowserTestUtils.removeTab(gBrowser.selectedTab);
+  return closePromise;
 }
 
 function acceptInstallDialog(installDialog) {
   if (Services.prefs.getBoolPref("xpinstall.customConfirmationUI", false)) {
     installDialog.button.click();
   } else {
     let win = Services.wm.getMostRecentWindow("Addons:Install");
     win.document.documentElement.acceptDialog();
@@ -296,17 +295,17 @@ async function test_blockedInstall() {
   is(notification.button.label, "Restart Now", "Should have seen the right button");
   is(notification.getAttribute("label"),
      "XPI Test will be installed after you restart " + gApp + ".",
      "Should have seen the right message");
 
   let installs = await getInstalls();
   is(installs.length, 1, "Should be one pending install");
   installs[0].cancel();
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_whitelistedInstall() {
   let originalTab = gBrowser.selectedTab;
   let tab;
   gBrowser.selectedTab = originalTab;
   let pm = Services.perms;
   pm.add(makeURI("http://example.com/"), "install", pm.ALLOW_ACTION);
@@ -335,17 +334,17 @@ async function test_whitelistedInstall()
      "XPI Test will be installed after you restart " + gApp + ".",
      "Should have seen the right message");
 
   let installs = await getInstalls();
   is(installs.length, 1, "Should be one pending install");
   installs[0].cancel();
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_failedDownload() {
   let pm = Services.perms;
   pm.add(makeURI("http://example.com/"), "install", pm.ALLOW_ACTION);
 
   let progressPromise = waitForProgressNotification();
   let failPromise = waitForNotification("addon-install-failed");
@@ -357,17 +356,17 @@ async function test_failedDownload() {
   let panel = await failPromise;
 
   let notification = panel.childNodes[0];
   is(notification.getAttribute("label"),
      "The add-on could not be downloaded because of a connection failure.",
      "Should have seen the right message");
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_corruptFile() {
   let pm = Services.perms;
   pm.add(makeURI("http://example.com/"), "install", pm.ALLOW_ACTION);
 
   let progressPromise = waitForProgressNotification();
   let failPromise = waitForNotification("addon-install-failed");
@@ -380,17 +379,17 @@ async function test_corruptFile() {
 
   let notification = panel.childNodes[0];
   is(notification.getAttribute("label"),
      "The add-on downloaded from this site could not be installed " +
      "because it appears to be corrupt.",
      "Should have seen the right message");
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_incompatible() {
   let pm = Services.perms;
   pm.add(makeURI("http://example.com/"), "install", pm.ALLOW_ACTION);
 
   let progressPromise = waitForProgressNotification();
   let failPromise = waitForNotification("addon-install-failed");
@@ -403,17 +402,17 @@ async function test_incompatible() {
 
   let notification = panel.childNodes[0];
   is(notification.getAttribute("label"),
      "XPI Test could not be installed because it is not compatible with " +
      gApp + " " + gVersion + ".",
      "Should have seen the right message");
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_restartless() {
   let pm = Services.perms;
   pm.add(makeURI("http://example.com/"), "install", pm.ALLOW_ACTION);
 
   let progressPromise = waitForProgressNotification();
   let dialogPromise = waitForInstallDialog();
@@ -435,20 +434,17 @@ async function test_restartless() {
   let addon = await new Promise(resolve => {
     AddonManager.getAddonByID("restartless-xpi@tests.mozilla.org", result => {
       resolve(result);
     });
   });
   addon.uninstall();
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
-
-  let closePromise = waitForNotificationClose();
-  gBrowser.removeTab(gBrowser.selectedTab);
-  await closePromise;
+  await removeTabAndWaitForNotificationClose(gBrowser.selectedTab);
 },
 
 async function test_sequential() {
   // This test is only relevant if using the new doorhanger UI
   // TODO: this subtest is disabled until multiple notification prompts are
   // reworked in bug 1188152
   if (true || !Services.prefs.getBoolPref("xpinstall.customConfirmationUI", false)) {
     return;
@@ -556,17 +552,17 @@ async function test_allUnverified() {
   let addon = await new Promise(resolve => {
     AddonManager.getAddonByID("restartless-xpi@tests.mozilla.org", function(result) {
       resolve(result);
     });
   });
   addon.uninstall();
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_url() {
   let progressPromise = waitForProgressNotification();
   let dialogPromise = waitForInstallDialog();
   gBrowser.selectedTab = BrowserTestUtils.addTab(gBrowser, "about:blank");
   await BrowserTestUtils.browserLoaded(gBrowser.selectedBrowser);
   gBrowser.loadURI(TESTROOT + "amosigned.xpi");
@@ -582,17 +578,17 @@ async function test_url() {
   is(notification.getAttribute("label"),
      "XPI Test will be installed after you restart " + gApp + ".",
      "Should have seen the right message");
 
   let installs = await getInstalls();
   is(installs.length, 1, "Should be one pending install");
   installs[0].cancel();
 
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_localFile() {
   let cr = Cc["@mozilla.org/chrome/chrome-registry;1"]
              .getService(Ci.nsIChromeRegistry);
   let path;
   try {
     path = cr.convertChromeURL(makeURI(CHROMEROOT + "corrupt.xpi")).spec;
@@ -615,17 +611,17 @@ async function test_localFile() {
   await waitForSingleNotification();
 
   let notification = PopupNotifications.panel.childNodes[0];
   is(notification.id, "addon-install-failed-notification", "Should have seen the install fail");
   is(notification.getAttribute("label"),
      "This add-on could not be installed because it appears to be corrupt.",
      "Should have seen the right message");
 
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_tabClose() {
   if (!Services.prefs.getBoolPref("xpinstall.customConfirmationUI", false)) {
     info("Test skipped due to xpinstall.customConfirmationUI being false.");
     return;
   }
 
@@ -635,19 +631,17 @@ async function test_tabClose() {
   await BrowserTestUtils.browserLoaded(gBrowser.selectedBrowser);
   gBrowser.loadURI(TESTROOT + "amosigned.xpi");
   await progressPromise;
   await dialogPromise;
 
   let installs = await getInstalls();
   is(installs.length, 1, "Should be one pending install");
 
-  let closePromise = waitForNotificationClose();
-  await BrowserTestUtils.removeTab(gBrowser.selectedTab);
-  await closePromise;
+  await removeTabAndWaitForNotificationClose(gBrowser.selectedTab);
 
   installs = await getInstalls();
   is(installs.length, 0, "Should be no pending install since the tab is closed");
 },
 
 // Add-ons should be cancelled and the install notification destroyed when
 // navigating to a new origin
 async function test_tabNavigate() {
@@ -671,17 +665,18 @@ async function test_tabNavigate() {
   gBrowser.loadURI("about:blank");
   await closePromise;
 
   let installs = await getInstalls();
   is(installs.length, 0, "Should be no pending install");
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
   await loadPromise;
-  await BrowserTestUtils.removeTab(gBrowser.selectedTab);
+
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_urlBar() {
   let progressPromise = waitForProgressNotification();
   let dialogPromise = waitForInstallDialog();
 
   gBrowser.selectedTab = BrowserTestUtils.addTab(gBrowser, "about:blank");
   await BrowserTestUtils.browserLoaded(gBrowser.selectedBrowser);
@@ -701,17 +696,17 @@ async function test_urlBar() {
   is(notification.getAttribute("label"),
      "XPI Test will be installed after you restart " + gApp + ".",
      "Should have seen the right message");
 
   let installs = await getInstalls();
   is(installs.length, 1, "Should be one pending install");
   installs[0].cancel();
 
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_wrongHost() {
   let requestedUrl = TESTROOT2 + "enabled.html";
   gBrowser.selectedTab = BrowserTestUtils.addTab(gBrowser);
 
   let loadedPromise = BrowserTestUtils.browserLoaded(gBrowser.selectedBrowser, false, requestedUrl);
   gBrowser.loadURI(TESTROOT2 + "enabled.html");
@@ -724,17 +719,17 @@ async function test_wrongHost() {
   let panel = await notificationPromise;
 
   let notification = panel.childNodes[0];
   is(notification.getAttribute("label"),
      "The add-on downloaded from this site could not be installed " +
      "because it appears to be corrupt.",
      "Should have seen the right message");
 
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_reload() {
   let pm = Services.perms;
   pm.add(makeURI("http://example.com/"), "install", pm.ALLOW_ACTION);
 
   let progressPromise = waitForProgressNotification();
   let dialogPromise = waitForInstallDialog();
@@ -765,17 +760,17 @@ async function test_reload() {
   await loadedPromise;
   PopupNotifications.panel.removeEventListener("popuphiding", testFail);
 
   let installs = await getInstalls();
   is(installs.length, 1, "Should be one pending install");
   installs[0].cancel();
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_theme() {
   let pm = Services.perms;
   pm.add(makeURI("http://example.com/"), "install", pm.ALLOW_ACTION);
 
   let progressPromise = waitForProgressNotification();
   let dialogPromise = waitForInstallDialog();
@@ -809,17 +804,17 @@ async function test_theme() {
     AddonManager.getAddonByID("theme-xpi@tests.mozilla.org", function(result) {
       resolve(result);
     });
   });
   isnot(addon, null, "Test theme will have been installed");
   addon.uninstall();
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_renotifyBlocked() {
   let notificationPromise = waitForNotification("addon-install-blocked");
   let triggers = encodeURIComponent(JSON.stringify({
     "XPI": "amosigned.xpi"
   }));
   BrowserTestUtils.openNewForegroundTab(gBrowser, TESTROOT + "installtrigger.html?" + triggers);
@@ -836,19 +831,17 @@ async function test_renotifyBlocked() {
 
   notificationPromise = waitForNotification("addon-install-blocked");
   gBrowser.loadURI(TESTROOT + "installtrigger.html?" + triggers);
   await notificationPromise;
 
   let installs = await getInstalls();
   is(installs.length, 2, "Should be two pending installs");
 
-  closePromise = waitForNotificationClose();
-  await BrowserTestUtils.removeTab(gBrowser.selectedTab);
-  await closePromise;
+  await removeTabAndWaitForNotificationClose(gBrowser.selectedTab);
 
   installs = await getInstalls();
   is(installs.length, 0, "Should have cancelled the installs");
 },
 
 async function test_renotifyInstalled() {
   let pm = Services.perms;
   pm.add(makeURI("http://example.com/"), "install", pm.ALLOW_ACTION);
@@ -888,17 +881,17 @@ async function test_renotifyInstalled() 
   acceptInstallDialog(installDialog);
   await notificationPromise;
 
   let installs = await getInstalls();
   is(installs.length, 1, "Should be one pending installs");
   installs[0].cancel();
 
   Services.perms.remove(makeURI("http://example.com/"), "install");
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 },
 
 async function test_cancel() {
   let pm = Services.perms;
   pm.add(makeURI("http://example.com/"), "install", pm.ALLOW_ACTION);
 
   let notificationPromise = waitForNotification(PROGRESS_NOTIFICATION);
   let triggers = encodeURIComponent(JSON.stringify({
@@ -977,17 +970,17 @@ async function test_failedSecurity() {
   // Allow the browser code to add the failure notification and then wait
   // for the progress notification to dismiss itself
   await waitForSingleNotification();
   is(PopupNotifications.panel.childNodes.length, 1, "Should be only one notification");
   notification = panel.childNodes[0];
   is(notification.id, "addon-install-failed-notification", "Should have seen the install fail");
 
   Services.prefs.setBoolPref(PREF_INSTALL_REQUIREBUILTINCERTS, true);
-  await removeTab();
+  await removeTabAndWaitForNotificationClose();
 }
 ];
 
 var gTestStart = null;
 
 var XPInstallObserver = {
   observe(aSubject, aTopic, aData) {
     var installInfo = aSubject.wrappedJSObject;
diff --git a/browser/base/content/test/general/browser_bug575561.js b/browser/base/content/test/general/browser_bug575561.js
--- a/browser/base/content/test/general/browser_bug575561.js
+++ b/browser/base/content/test/general/browser_bug575561.js
@@ -60,17 +60,18 @@ async function testLink(aLinkIndexOrFunc
 
   let browser = appTab.linkedBrowser;
   await BrowserTestUtils.browserLoaded(browser);
 
   let promise;
   if (expectNewTab) {
     promise = BrowserTestUtils.waitForNewTab(gBrowser).then(tab => {
       let loaded = tab.linkedBrowser.documentURI.spec;
-      return BrowserTestUtils.removeTab(tab).then(() => loaded);
+      BrowserTestUtils.removeTab(tab);
+      return loaded;
     });
   } else {
     promise = BrowserTestUtils.browserLoaded(browser, testSubFrame);
   }
 
   let href;
   if (typeof aLinkIndexOrFunction === "function") {
     ok(!browser.isRemoteBrowser, "don't pass a function for a remote browser");
diff --git a/browser/base/content/test/general/browser_printpreview.js b/browser/base/content/test/general/browser_printpreview.js
--- a/browser/base/content/test/general/browser_printpreview.js
+++ b/browser/base/content/test/general/browser_printpreview.js
@@ -12,17 +12,18 @@ function test() {
       openPrintPreview(testClosePrintPreviewWithEscKey);
     } else {
       openPrintPreview(testClosePrintPreviewWithAccessKey);
     }
   });
 }
 
 function tidyUp() {
-  BrowserTestUtils.removeTab(ourTab).then(finish);
+  BrowserTestUtils.removeTab(ourTab);
+  finish();
 }
 
 function testClosePrintPreviewWithAccessKey() {
   EventUtils.synthesizeKey("c", {altKey: true});
   checkPrintPreviewClosed(function(aSucceeded) {
     ok(aSucceeded,
        "print preview mode should be finished by access key");
     openPrintPreview(testClosePrintPreviewWithEscKey);
diff --git a/browser/base/content/test/general/browser_tab_close_dependent_window.js b/browser/base/content/test/general/browser_tab_close_dependent_window.js
--- a/browser/base/content/test/general/browser_tab_close_dependent_window.js
+++ b/browser/base/content/test/general/browser_tab_close_dependent_window.js
@@ -10,18 +10,15 @@ add_task(async function closing_tab_with
   await BrowserTestUtils.removeTab(win.gBrowser.tabs[0]);
   info("Clicking into the window");
   let depTabOpened = BrowserTestUtils.waitForEvent(win.gBrowser.tabContainer, "TabOpen");
   await BrowserTestUtils.synthesizeMouse("html", 0, 0, {}, tab.linkedBrowser);
 
   let openedTab = (await depTabOpened).target;
   info("Got opened tab");
 
-  let otherTabClosePromise = BrowserTestUtils.tabRemoved(openedTab);
   let windowClosedPromise = BrowserTestUtils.windowClosed(win);
-  await BrowserTestUtils.removeTab(tab);
-  info("Wait for other tab to close, this shouldn't time out");
-  await otherTabClosePromise;
+  BrowserTestUtils.removeTab(tab);
   is(Cu.isDeadWrapper(openedTab) || openedTab.linkedBrowser == null, true, "Opened tab should also have closed");
   info("If we timeout now, the window failed to close - that shouldn't happen!");
   await windowClosedPromise;
 });
 
diff --git a/browser/components/contextualidentity/test/browser/browser_middleClick.js b/browser/components/contextualidentity/test/browser/browser_middleClick.js
--- a/browser/components/contextualidentity/test/browser/browser_middleClick.js
+++ b/browser/components/contextualidentity/test/browser/browser_middleClick.js
@@ -30,11 +30,14 @@ add_task(async function() {
       resolve(openEvent.target);
     }, {once: true});
 
     BrowserTestUtils.synthesizeMouseAtCenter("#clickMe", { button: 1 }, browser);
   });
 
   is(newTab.getAttribute("usercontextid"), 1, "Correct UserContextId?");
 
-  await BrowserTestUtils.removeTab(tab);
-  await BrowserTestUtils.removeTab(newTab);
+  // newTab shouldn't be closed in the same event tick as TabOpen.
+  await TestUtils.waitForTick();
+
+  BrowserTestUtils.removeTab(tab);
+  BrowserTestUtils.removeTab(newTab);
 });
diff --git a/browser/components/preferences/in-content-new/tests/browser_change_app_handler.js b/browser/components/preferences/in-content-new/tests/browser_change_app_handler.js
--- a/browser/components/preferences/in-content-new/tests/browser_change_app_handler.js
+++ b/browser/components/preferences/in-content-new/tests/browser_change_app_handler.js
@@ -84,18 +84,16 @@ add_task(async function() {
   ok(!mimeInfo.preferredApplicationHandler, "App should no longer be set as preferred.");
 
   // Check that we display this result:
   list = await waitForCondition(() => win.document.getAnonymousElementByAttribute(ourItem, "class", "actionsMenu"));
   ok(list.selectedItem, "Should have a selected item");
   ok(!list.selectedItem.handlerApp,
      "No app should be visible as preferred item.");
 
-  let tabRemovedPromise = BrowserTestUtils.tabRemoved(gBrowser.selectedTab);
-  gBrowser.removeCurrentTab();
-  await tabRemovedPromise;
+  BrowserTestUtils.removeTab(gBrowser.selectedTab);
 });
 
 registerCleanupFunction(function() {
   let infoToModify = gMimeSvc.getFromTypeAndExtension("text/x-test-handler", null);
   gHandlerSvc.remove(infoToModify);
 });
 
diff --git a/browser/components/sessionstore/test/browser_1284886_suspend_tab.js.1442465-4_1.later b/browser/components/sessionstore/test/browser_1284886_suspend_tab.js.1442465-4_1.later
new file mode 100644
--- /dev/null
+++ b/browser/components/sessionstore/test/browser_1284886_suspend_tab.js.1442465-4_1.later
@@ -0,0 +1,41 @@
+--- browser_1284886_suspend_tab.js
++++ browser_1284886_suspend_tab.js
+@@ -38,35 +38,35 @@ add_task(async function test() {
+   // Test that tab with beforeunload handler which would show a prompt cannot be suspended.
+   gBrowser.discardBrowser(browser1);
+   ok(tab1.linkedPanel, "cannot suspend a tab with beforeunload handler which would show a prompt");
+ 
+   // Test that tab with beforeunload handler which would show a prompt will be suspended if forced.
+   gBrowser.discardBrowser(browser1, true);
+   ok(!tab1.linkedPanel, "force suspending a tab with beforeunload handler which would show a prompt");
+ 
+-  await promiseRemoveTab(tab1);
++  BrowserTestUtils.removeTab(tab1);
+ 
+   // Open tab containing a page which has a beforeunload handler which does not show a prompt.
+   url = "http://example.com/browser/browser/components/sessionstore/test/browser_1284886_suspend_tab_2.html";
+   tab1 = await BrowserTestUtils.openNewForegroundTab(gBrowser, url);
+   browser1 = tab1.linkedBrowser;
+   await BrowserTestUtils.switchTab(gBrowser, tab0);
+ 
+   // Test that tab with beforeunload handler which would not show a prompt can be suspended.
+   gBrowser.discardBrowser(browser1);
+   ok(!tab1.linkedPanel, "can suspend a tab with beforeunload handler which would not show a prompt");
+ 
+-  await promiseRemoveTab(tab1);
++  BrowserTestUtils.removeTab(tab1);
+ 
+   // Test that non-remote tab is not able to be suspended.
+   url = "about:robots";
+   tab1 = BrowserTestUtils.addTab(gBrowser, url, { forceNotRemote: true });
+   await promiseBrowserLoaded(tab1.linkedBrowser, true, url);
+   await BrowserTestUtils.switchTab(gBrowser, tab1);
+   await BrowserTestUtils.switchTab(gBrowser, tab0);
+ 
+   gBrowser.discardBrowser(tab1.linkedBrowser);
+   ok(tab1.linkedPanel, "cannot suspend a remote tab");
+ 
+-  promiseRemoveTab(tab1);
++  BrowserTestUtils.removeTab(tab1);
+ });
+ 
diff --git a/browser/components/sessionstore/test/browser_339445.js b/browser/components/sessionstore/test/browser_339445.js
--- a/browser/components/sessionstore/test/browser_339445.js
+++ b/browser/components/sessionstore/test/browser_339445.js
@@ -22,11 +22,11 @@ add_task(async function test() {
 
   await ContentTask.spawn(tab2.linkedBrowser, null, function() {
     let doc2 = content.document;
     is(doc2.getElementById("storageTestItem").textContent, "SUCCESS",
        "sessionStorage value has been duplicated");
   });
 
   // clean up
-  await Promise.all([ BrowserTestUtils.removeTab(tab2),
-                      BrowserTestUtils.removeTab(tab) ]);
+  BrowserTestUtils.removeTab(tab2);
+  BrowserTestUtils.removeTab(tab);
 });
diff --git a/devtools/client/performance/test/browser_perf-private-browsing.js b/devtools/client/performance/test/browser_perf-private-browsing.js
--- a/devtools/client/performance/test/browser_perf-private-browsing.js
+++ b/devtools/client/performance/test/browser_perf-private-browsing.js
@@ -13,17 +13,17 @@ const { startRecording, stopRecording } 
 const { once } = require("devtools/client/performance/test/helpers/event-utils");
 
 let gPanelWinTuples = [];
 add_task(async function() {
   await testNormalWindow();
   await testPrivateWindow();
   await testRecordingFailingInWindow(0);
   await testRecordingFailingInWindow(1);
-  await teardownPerfInWindow(1, { shouldCloseWindow: true, dontWaitForTabClose: true });
+  yield teardownPerfInWindow(1, { shouldCloseWindow: true });
   await testRecordingSucceedingInWindow(0);
   await teardownPerfInWindow(0, { shouldCloseWindow: false });
 
   gPanelWinTuples = null;
 });
 
 async function createPanelInNewWindow(options) {
   let win = await addWindow(options);
@@ -100,14 +100,14 @@ async function testRecordingSucceedingIn
   ok(true, "Recording has succeeded.");
 
   PerformanceController.off(EVENTS.BACKEND_FAILED_AFTER_RECORDING_START,
                            onRecordingFailed);
 }
 
 async function teardownPerfInWindow(index, options) {
   let { panel, win } = gPanelWinTuples[index];
-  await teardownToolboxAndRemoveTab(panel, options);
+  await teardownToolboxAndRemoveTab(panel);
 
   if (options.shouldCloseWindow) {
     win.close();
   }
 }
diff --git a/devtools/client/performance/test/helpers/panel-utils.js b/devtools/client/performance/test/helpers/panel-utils.js
--- a/devtools/client/performance/test/helpers/panel-utils.js
+++ b/devtools/client/performance/test/helpers/panel-utils.js
@@ -92,15 +92,15 @@ exports.initConsoleInTab = async functio
   };
 
   return { target, toolbox, panel, console: { profile, profileEnd } };
 };
 
 /**
  * Tears down a toolbox panel and removes an associated tab.
  */
-exports.teardownToolboxAndRemoveTab = async function(panel, options) {
+exports.teardownToolboxAndRemoveTab = async function(panel) {
   dump("Destroying panel.\n");
 
   let tab = panel.target.tab;
   await panel.toolbox.destroy();
-  await removeTab(tab, options);
+  await removeTab(tab);
 };
diff --git a/devtools/client/performance/test/helpers/tab-utils.js b/devtools/client/performance/test/helpers/tab-utils.js
--- a/devtools/client/performance/test/helpers/tab-utils.js
+++ b/devtools/client/performance/test/helpers/tab-utils.js
@@ -30,26 +30,20 @@ exports.addTab = function({ url, win }, 
   let { gBrowser } = win || window;
   return BrowserTestUtils.openNewForegroundTab(gBrowser, url,
                                                !options.dontWaitForTabReady);
 };
 
 /**
  * Removes a browser tab from the specified window and waits for it to close.
  */
-exports.removeTab = function (tab, options = {}) {
+exports.removeTab = function (tab) {
   dump(`Removing tab: ${tab.linkedBrowser.currentURI.spec}.\n`);
 
-  return new Promise(resolve => {
-    BrowserTestUtils.removeTab(tab).then(() => resolve(tab));
-
-    if (options.dontWaitForTabClose) {
-      resolve(tab);
-    }
-  });
+  BrowserTestUtils.removeTab(tab);
 };
 
 /**
  * Adds a browser window with the provided options.
  */
 exports.addWindow = async function(options) {
   let { OpenBrowserWindow } = Services.wm.getMostRecentWindow(gDevTools.chromeWindowType);
   let win = OpenBrowserWindow(options);
diff --git a/toolkit/crashreporter/test/browser/browser_aboutCrashesResubmit.js b/toolkit/crashreporter/test/browser/browser_aboutCrashesResubmit.js
--- a/toolkit/crashreporter/test/browser/browser_aboutCrashesResubmit.js
+++ b/toolkit/crashreporter/test/browser/browser_aboutCrashesResubmit.js
@@ -1,14 +1,15 @@
 function cleanup_and_finish() {
   try {
     cleanup_fake_appdir();
   } catch (ex) {}
   Services.prefs.clearUserPref("breakpad.reportURL");
-  BrowserTestUtils.removeTab(gBrowser.selectedTab).then(finish);
+  BrowserTestUtils.removeTab(gBrowser.selectedTab);
+  finish();
 }
 
 /*
  * check_crash_list
  *
  * Check that the list of crashes displayed by about:crashes matches
  * the list of crashes that we placed in the pending+submitted directories.
  *
