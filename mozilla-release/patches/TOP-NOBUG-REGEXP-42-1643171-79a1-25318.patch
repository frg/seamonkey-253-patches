# HG changeset patch
# User Dmitry Butskoy <buc@buc.me>
# Date 1690631408 -7200
# Parent  ce33dd60be4b1cf4784c37407398f8281a9fa4bf
No Bug - Import new regexp V8 engine. r=frg a=frg

diff --git a/js/src/irregexp/RegExpAPI.cpp b/js/src/irregexp/RegExpAPI.cpp
--- a/js/src/irregexp/RegExpAPI.cpp
+++ b/js/src/irregexp/RegExpAPI.cpp
@@ -129,17 +129,26 @@ static uint32_t ErrorNumber(RegExpError 
 Isolate* CreateIsolate(JSContext* cx) {
   auto isolate = MakeUnique<Isolate>(cx);
   if (!isolate || !isolate->init()) {
     return nullptr;
   }
   return isolate.release();
 }
 
-void DestroyIsolate(Isolate* isolate) { js_delete(isolate); }
+void DestroyIsolate(Isolate* isolate) {
+  MOZ_ASSERT(isolate->liveHandles() == 0);
+  MOZ_ASSERT(isolate->livePseudoHandles() == 0);
+  js_delete(isolate);
+}
+
+size_t IsolateSizeOfIncludingThis(Isolate* isolate,
+                                  mozilla::MallocSizeOf mallocSizeOf) {
+  return isolate->sizeOfIncludingThis(mallocSizeOf);
+}
 
 static size_t ComputeColumn(const Latin1Char* begin, const Latin1Char* end) {
   return mozilla::PointerRangeSize(begin, end);
 }
 
 static size_t ComputeColumn(const char16_t* begin, const char16_t* end) {
   return unicode::CountCodePoints(begin, end);
 }
@@ -425,16 +434,17 @@ static MOZ_MUST_USE AssembleResult Assem
   return AssembleResult::Success;
 }
 
 bool CompilePattern(JSContext* cx, MutableHandleRegExpShared re,
                     HandleLinearString input, RegExpShared::CodeKind codeKind) {
   RootedAtom pattern(cx, re->getSource());
   JS::RegExpFlags flags = re->getFlags();
   LifoAllocScope allocScope(&cx->tempLifoAlloc());
+  HandleScope handleScope(cx->isolate);
   Zone zone(allocScope.alloc());
 
   RegExpCompileData data;
   {
     FlatStringReader patternBytes(cx, pattern);
     if (!RegExpParser::ParseRegExp(cx->isolate, &zone, &patternBytes, flags,
                                    &data)) {
       MOZ_ASSERT(data.error == RegExpError::kStackOverflow);
@@ -478,17 +488,16 @@ bool CompilePattern(JSContext* cx, Mutab
     // All fallible initialization has succeeded, so we can change state.
     // Add one to capture_count to account for the whole-match capture.
     uint32_t pairCount = data.capture_count + 1;
     re->useRegExpMatch(pairCount);
   }
 
   MOZ_ASSERT(re->kind() == RegExpShared::Kind::RegExp);
 
-  HandleScope handleScope(cx->isolate);
   RegExpCompiler compiler(cx->isolate, &zone, data.capture_count,
                           input->hasLatin1Chars());
 
   bool isLatin1 = input->hasLatin1Chars();
 
   FlatStringReader sample_subject(cx, input);
   SampleCharacters(&sample_subject, compiler);
   data.node = compiler.PreprocessRegExp(&data, flags, isLatin1);
diff --git a/js/src/irregexp/RegExpAPI.h b/js/src/irregexp/RegExpAPI.h
--- a/js/src/irregexp/RegExpAPI.h
+++ b/js/src/irregexp/RegExpAPI.h
@@ -7,26 +7,31 @@
  * defined by the Mozilla Public License, v. 2.0.
  */
 
 /* This is the interface that the regexp engine exposes to SpiderMonkey. */
 
 #ifndef regexp_RegExpAPI_h
 #define regexp_RegExpAPI_h
 
+#include "mozilla/MemoryReporting.h"
+
 #include "frontend/TokenStream.h"
 #include "jscntxt.h"
 #include "vm/RegExpObject.h"
 
 namespace js {
 namespace irregexp {
 
 Isolate* CreateIsolate(JSContext* cx);
 void DestroyIsolate(Isolate* isolate);
 
+size_t IsolateSizeOfIncludingThis(Isolate* isolate,
+                                  mozilla::MallocSizeOf mallocSizeOf);
+
 bool CheckPatternSyntax(JSContext* cx, frontend::TokenStream& ts,
                         const mozilla::Range<const char16_t> chars,
                         JS::RegExpFlags flags);
 bool CheckPatternSyntax(JSContext* cx, frontend::TokenStream& ts,
                         HandleAtom pattern, JS::RegExpFlags flags);
 bool CompilePattern(JSContext* cx, MutableHandleRegExpShared re,
                     HandleLinearString input, RegExpShared::CodeKind codeKind);
 
diff --git a/js/src/irregexp/imported/regexp-macro-assembler.cc b/js/src/irregexp/imported/regexp-macro-assembler.cc
--- a/js/src/irregexp/imported/regexp-macro-assembler.cc
+++ b/js/src/irregexp/imported/regexp-macro-assembler.cc
@@ -1,15 +1,16 @@
 // Copyright 2012 the V8 project authors. All rights reserved.
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
 #include "irregexp/imported/regexp-macro-assembler.h"
 
 #include "irregexp/imported/regexp-stack.h"
+#include "irregexp/imported/special-case.h"
 
 #ifdef V8_INTL_SUPPORT
 #include "unicode/uchar.h"
 #include "unicode/unistr.h"
 #endif  // V8_INTL_SUPPORT
 
 namespace v8 {
 namespace internal {
@@ -17,23 +18,52 @@ namespace internal {
 RegExpMacroAssembler::RegExpMacroAssembler(Isolate* isolate, Zone* zone)
     : slow_safe_compiler_(false),
       global_mode_(NOT_GLOBAL),
       isolate_(isolate),
       zone_(zone) {}
 
 RegExpMacroAssembler::~RegExpMacroAssembler() = default;
 
-int RegExpMacroAssembler::CaseInsensitiveCompareUC16(Address byte_offset1,
-                                                     Address byte_offset2,
-                                                     size_t byte_length,
-                                                     Isolate* isolate) {
+int RegExpMacroAssembler::CaseInsensitiveCompareNonUnicode(Address byte_offset1,
+                                                           Address byte_offset2,
+                                                           size_t byte_length,
+                                                           Isolate* isolate) {
+#ifdef V8_INTL_SUPPORT
   // This function is not allowed to cause a garbage collection.
   // A GC might move the calling generated code and invalidate the
   // return address on the stack.
+  DisallowHeapAllocation no_gc;
+  DCHECK_EQ(0, byte_length % 2);
+  size_t length = byte_length / 2;
+  uc16* substring1 = reinterpret_cast<uc16*>(byte_offset1);
+  uc16* substring2 = reinterpret_cast<uc16*>(byte_offset2);
+
+  for (size_t i = 0; i < length; i++) {
+    UChar32 c1 = RegExpCaseFolding::Canonicalize(substring1[i]);
+    UChar32 c2 = RegExpCaseFolding::Canonicalize(substring2[i]);
+    if (c1 != c2) {
+      return 0;
+    }
+  }
+  return 1;
+#else
+  return CaseInsensitiveCompareUnicode(byte_offset1, byte_offset2, byte_length,
+                                       isolate);
+#endif
+}
+
+int RegExpMacroAssembler::CaseInsensitiveCompareUnicode(Address byte_offset1,
+                                                        Address byte_offset2,
+                                                        size_t byte_length,
+                                                        Isolate* isolate) {
+  // This function is not allowed to cause a garbage collection.
+  // A GC might move the calling generated code and invalidate the
+  // return address on the stack.
+  DisallowHeapAllocation no_gc;
   DCHECK_EQ(0, byte_length % 2);
 
 #ifdef V8_INTL_SUPPORT
   int32_t length = (int32_t)(byte_length >> 1);
   icu::UnicodeString uni_str_1(reinterpret_cast<const char16_t*>(byte_offset1),
                                length);
   return uni_str_1.caseCompare(reinterpret_cast<const char16_t*>(byte_offset2),
                                length, U_FOLD_CASE_DEFAULT) == 0;
@@ -58,17 +88,16 @@ int RegExpMacroAssembler::CaseInsensitiv
         }
       }
     }
   }
   return 1;
 #endif  // V8_INTL_SUPPORT
 }
 
-
 void RegExpMacroAssembler::CheckNotInSurrogatePair(int cp_offset,
                                                    Label* on_failure) {
   Label ok;
   // Check that current character is not a trail surrogate.
   LoadCurrentCharacter(cp_offset, &ok);
   CheckCharacterNotInRange(kTrailSurrogateStart, kTrailSurrogateEnd, &ok);
   // Check that previous character is not a lead surrogate.
   LoadCurrentCharacter(cp_offset - 1, &ok);
diff --git a/js/src/vm/JSContext.cpp b/js/src/vm/JSContext.cpp
--- a/js/src/vm/JSContext.cpp
+++ b/js/src/vm/JSContext.cpp
@@ -1350,17 +1350,19 @@ JSContext::~JSContext()
     js::jit::Simulator::Destroy(simulator_);
 #endif
 
 #ifdef JS_TRACE_LOGGING
     if (traceLogger)
         DestroyTraceLogger(traceLogger);
 #endif
 
-    irregexp::DestroyIsolate(isolate.ref());
+    if (isolate) {
+      irregexp::DestroyIsolate(isolate.ref());
+    }
 
     MOZ_ASSERT(TlsContext.get() == this);
     TlsContext.set(nullptr);
 }
 
 void
 JSContext::setRuntime(JSRuntime* rt)
 {
@@ -1483,25 +1485,25 @@ IsJITBrokenHere()
 }
 
 void
 JSContext::updateJITEnabled()
 {
     jitIsBroken = IsJITBrokenHere();
 }
 
-size_t
-JSContext::sizeOfExcludingThis(mozilla::MallocSizeOf mallocSizeOf) const
-{
-    /*
-     * There are other JSContext members that could be measured; the following
-     * ones have been found by DMD to be worth measuring.  More stuff may be
-     * added later.
-     */
-    return cycleDetectorVector().sizeOfExcludingThis(mallocSizeOf);
+size_t JSContext::sizeOfExcludingThis(
+    mozilla::MallocSizeOf mallocSizeOf) const {
+  /*
+   * There are other JSContext members that could be measured; the following
+   * ones have been found by DMD to be worth measuring.  More stuff may be
+   * added later.
+   */
+  return cycleDetectorVector().sizeOfExcludingThis(mallocSizeOf) +
+         irregexp::IsolateSizeOfIncludingThis(isolate, mallocSizeOf);
 }
 
 void
 JSContext::trace(JSTracer* trc)
 {
     cycleDetectorVector().trace(trc);
     geckoProfiler().trace(trc);
 
