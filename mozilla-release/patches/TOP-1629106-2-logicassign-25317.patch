# HG changeset patch
# User Dmitry Butskoy <buc@buc.me>
# Date 1684790018 -7200
# Parent  e12f241ebeba10c76f50ebd3d71e264e73923038
Bug 1629106 - Implement Logical Assignment Operators proposal JSOP_GOTO workaround. r=frg a=frg

Original logical assignment uses JSOP_GOTO, but our legacy code
still uses SourceNotes for BaselineJIT compiling. The SourceNotes require
each JSOP_GOTO to be accompanied by a call to newSrcNote() with a proper note type,
but there is no such a type for logical assignment yet.

To make the current BaselineJIT happy, we just adapt the algorithm
to use common code instead of jumping by goto.

The original one can be represented as:

  {
      goto AroundPop;
  jump:
      UnpickN()
      PopN()
  AroundPop:
  }

we change it to an equivalent:

  {
      DupN()
      PickN()
  jump:
      UnpickN()
      PopN()
    }

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -6970,35 +6970,39 @@ BytecodeEmitter::emitShortCircuitAssignm
       default:
         MOZ_CRASH();
     }
 
     MOZ_ASSERT(stackDepth == depth + 1);
 
     // Join with the short-circuit jump and pop anything left on the stack.
     if (numPushed > 0) {
-        JumpList jumpAroundPop;
-        if (!emitJump(JSOP_GOTO, &jumpAroundPop))
+        // Adapt stack to use common following code with short-circuit case.
+        // This way we avoid the use of JSOP_GOTO, which prevents BaselineJit
+        // compiling due to missing SourceNote type for such a goto case.
+
+        for (int i = 0; i < numPushed; i++) {
+            if (!emit1(JSOP_DUP))
+                return false;
+        }
+        if (!emitPickN(numPushed))
             return false;
 
         if (!emitJumpTargetAndPatch(jump))
             return false;
 
         // Reconstruct the stack depth after the jump.
         stackDepth = depth + 1 + numPushed;
 
         // Move the left-hand side value to the bottom and pop the rest.
         if (!emitUnpickN(numPushed))
             return false;
         if (!emitPopN(numPushed))
             return false;
 
-        if (!emitJumpTargetAndPatch(jumpAroundPop))
-            return false;
-
     } else {
         if (!emitJumpTargetAndPatch(jump))
             return false;
     }
 
     MOZ_ASSERT(stackDepth == depth + 1);
 
     return true;
