# HG changeset patch
# User Anthony Ramine <nox@nox.paris>
# Date 1584981127 0
# Node ID d9b2acdce930ee1279fd8e51a4b8f85c5350435d
# Parent  83f8bafcf59fcf26c5c4688805d16e312a430b78
Bug 1624190 - Use time.process_time() on Python 3; r=rstewart

time.clock() is deprecated since Python 3.3 and gone in Python 3.8.

Depends on D67773

Differential Revision: https://phabricator.services.mozilla.com/D67774

diff --git a/python/mozbuild/mozbuild/config_status.py b/python/mozbuild/mozbuild/config_status.py
--- a/python/mozbuild/mozbuild/config_status.py
+++ b/python/mozbuild/mozbuild/config_status.py
@@ -16,23 +16,26 @@ import time
 from argparse import ArgumentParser
 
 from mach.logging import LoggingManager
 from mozbuild.backend.configenvironment import ConfigEnvironment
 from mozbuild.base import MachCommandConditions
 from mozbuild.frontend.emitter import TreeMetadataEmitter
 from mozbuild.frontend.reader import BuildReader
 from mozbuild.mozinfo import write_mozinfo
-from mozbuild.util import FileAvoidWrite
 from itertools import chain
 
 from mozbuild.backend import (
     backends,
     get_backend_class,
 )
+from mozbuild.util import (
+    FileAvoidWrite,
+    process_time,
+)
 
 
 log_manager = LoggingManager()
 
 
 ANDROID_IDE_ADVERTISEMENT = '''
 =============
 ADVERTISEMENT
@@ -112,17 +115,17 @@ def config_status(topobjdir='.', topsrcd
 
     env = ConfigEnvironment(topsrcdir, topobjdir, defines=defines,
                             non_global_defines=non_global_defines, substs=substs,
                             source=source, mozconfig=mozconfig)
 
     with FileAvoidWrite(os.path.join(topobjdir, 'mozinfo.json')) as f:
         write_mozinfo(f, env, os.environ)
 
-    cpu_start = time.clock()
+    cpu_start = process_time()
     time_start = time.time()
 
     # Make appropriate backend instances, defaulting to RecursiveMakeBackend,
     # or what is in BUILD_BACKENDS.
     selected_backends = [get_backend_class(b)(env) for b in options.backend]
 
     if options.dry_run:
         for b in selected_backends:
@@ -148,17 +151,17 @@ def config_status(topobjdir='.', topsrcd
     for obj in chain((reader, emitter), selected_backends):
         summary = obj.summary()
         print(summary, file=sys.stderr)
         execution_time += summary.execution_time
         if hasattr(obj, 'gyp_summary'):
             summary = obj.gyp_summary()
             print(summary, file=sys.stderr)
 
-    cpu_time = time.clock() - cpu_start
+    cpu_time = process_time() - cpu_start
     wall_time = time.time() - time_start
     efficiency = cpu_time / wall_time if wall_time else 100
     untracked = wall_time - execution_time
 
     print(
         'Total wall time: {:.2f}s; CPU time: {:.2f}s; Efficiency: '
         '{:.0%}; Untracked: {:.2f}s'.format(
             wall_time, cpu_time, efficiency, untracked),
diff --git a/python/mozbuild/mozbuild/util.py b/python/mozbuild/mozbuild/util.py
--- a/python/mozbuild/mozbuild/util.py
+++ b/python/mozbuild/mozbuild/util.py
@@ -1480,8 +1480,15 @@ def ensure_subprocess_env(env, encoding=
 
     Args:
         env (dict): Environment to ensure.
         encoding (str): Encoding to use when converting to/from bytes/text
                         (default: utf-8).
     """
     ensure = ensure_bytes if sys.version_info[0] < 3 else ensure_unicode
     return {ensure(k, encoding): ensure(v, encoding) for k, v in six.iteritems(env)}
+
+
+def process_time():
+    if six.PY2:
+        return time.clock()
+    else:
+        return time.process_time()
