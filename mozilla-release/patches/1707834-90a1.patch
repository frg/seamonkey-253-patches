# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1619559862 0
# Node ID 39213a71d10f64e29eab0ad7fbd843379cf72e52
# Parent  e248a2991bde932260bd26688d3793a39d953f7a
Bug 1707834 - Avoid libmozgtk not being linked to libxul. r=firefox-build-system-reviewers,mhentges

When the linker defaults to --as-needed, libmozgtk ends up not linked
to libxul because libxul doesn't use anything from it.
We solve the problem by adding a symbol to mozgtk and have libxul use
it.

In bug 1377445, we moved XShmQueryExtension to mozglue. While libxul
currently doesn't use the symbol (it's a workaround for system
libraries), we can move the function back to mozgtk and add a dummy
call.

Differential Revision: https://phabricator.services.mozilla.com/D113487

diff --git a/mozglue/build/moz.build b/mozglue/build/moz.build
--- a/mozglue/build/moz.build
+++ b/mozglue/build/moz.build
@@ -21,21 +21,16 @@ if CONFIG['MOZ_ASAN']:
         'AsanOptions.cpp',
     ]
 
 if CONFIG['MOZ_UBSAN']:
     SOURCES += [
         'UbsanOptions.cpp',
     ]
 
-if CONFIG["MOZ_WIDGET_TOOLKIT"] == "gtk":
-    SOURCES += [
-        "gtk.c",
-    ]
-
 if CONFIG['OS_TARGET'] == 'WINNT':
     DEFFILE = '!mozglue.def'
     # We'll break the DLL blocklist if we immediately load user32.dll
     DELAYLOAD_DLLS += [
         'user32.dll',
     ]
 
     if CONFIG['CC_TYPE'] == "msvc":
diff --git a/widget/gtk/mozgtk/moz.build b/widget/gtk/mozgtk/moz.build
--- a/widget/gtk/mozgtk/moz.build
+++ b/widget/gtk/mozgtk/moz.build
@@ -1,16 +1,20 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 SharedLibrary("mozgtk")
 
+SOURCES += [
+    "mozgtk.c",
+]
+
 # If LDFLAGS contains -Wl,--as-needed or if it's the default for the toolchain,
 # we need to add -Wl,--no-as-needed before the gtk libraries, otherwise the
 # linker will drop those dependencies because no symbols are used from them.
 # But those dependencies need to be kept for things to work properly.
 # Ideally, we'd only add -Wl,--no-as-needed if necessary, but it's just simpler
 # to add it unconditionally. This library is also simple enough that forcing
 # -Wl,--as-needed after the gtk libraries is not going to make a significant
 # difference.
diff --git a/mozglue/build/gtk.c b/widget/gtk/mozgtk/mozgtk.c
rename from mozglue/build/gtk.c
rename to widget/gtk/mozgtk/mozgtk.c
--- a/mozglue/build/gtk.c
+++ b/widget/gtk/mozgtk/mozgtk.c
@@ -1,19 +1,21 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/Types.h"
 
-// Only define the following workaround when using GTK3, which we detect
-// by checking if GTK3 stubs are not provided.
 #include <X11/Xlib.h>
 // Bug 1271100
 // We need to trick system Cairo into not using the XShm extension due to
 // a race condition in it that results in frequent BadAccess errors. Cairo
 // relies upon XShmQueryExtension to initially detect if XShm is available.
 // So we define our own stub that always indicates XShm not being present.
 // mozgtk loads before libXext/libcairo and so this stub will take priority.
 // Our tree usage goes through xcb and remains unaffected by this.
-MFBT_API Bool XShmQueryExtension(Display* aDisplay) { return False; }
+//
+// This is also used to force libxul to depend on the mozgtk library. If we
+// ever can remove this workaround for system Cairo, we'll need something
+// to replace it for that purpose.
+MOZ_EXPORT Bool XShmQueryExtension(Display* aDisplay) { return False; }
diff --git a/widget/gtk/nsWindow.cpp b/widget/gtk/nsWindow.cpp
--- a/widget/gtk/nsWindow.cpp
+++ b/widget/gtk/nsWindow.cpp
@@ -4009,16 +4009,20 @@ nsWindow::Create(nsIWidget* aParent,
       mXDisplay = GDK_WINDOW_XDISPLAY(mGdkWindow);
       mXWindow = gdk_x11_window_get_xid(mGdkWindow);
 
       GdkVisual* gdkVisual = gdk_window_get_visual(mGdkWindow);
       mXVisual = gdk_x11_visual_get_xvisual(gdkVisual);
       mXDepth = gdk_visual_get_depth(gdkVisual);
 
       mSurfaceProvider.Initialize(mXDisplay, mXWindow, mXVisual, mXDepth);
+
+      // Dummy call to a function in mozgtk to prevent the linker from removing
+      // the dependency with --as-needed.
+      XShmQueryExtension(mXDisplay);
     }
 #ifdef MOZ_WAYLAND
     else if (!mIsX11Display) {
       mSurfaceProvider.Initialize(this);
     }
 #endif
 #endif
     return NS_OK;
