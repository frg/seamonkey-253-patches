# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1519211355 18000
# Node ID ba27cf3b85203a5ab0ba513586f57ffe1eff1510
# Parent  1b3d10b07879b03fc484aa018c4960ff776a21f9
servo: Merge #20082 - style: Cleanup always-false argument to Servo_ResolveStyleLazily (from emilio:ignore-existing-styles); r=bholley

I changed this setup in https://bugzilla.mozilla.org/show_bug.cgi?id=1414999,
because it was totally unsound.

Source-Repo: https://github.com/servo/servo
Source-Revision: 691f3be24a6fcc90ae7d0b9b0783abf8674e1b0f

diff --git a/servo/components/style/traversal.rs b/servo/components/style/traversal.rs
--- a/servo/components/style/traversal.rs
+++ b/servo/components/style/traversal.rs
@@ -295,38 +295,36 @@ pub trait DomTraversal<E: TElement> : Sy
 /// Manually resolve style by sequentially walking up the parent chain to the
 /// first styled Element, ignoring pending restyles. The resolved style is made
 /// available via a callback, and can be dropped by the time this function
 /// returns in the display:none subtree case.
 pub fn resolve_style<E>(
     context: &mut StyleContext<E>,
     element: E,
     rule_inclusion: RuleInclusion,
-    ignore_existing_style: bool,
     pseudo: Option<&PseudoElement>,
 ) -> ElementStyles
 where
     E: TElement,
 {
     use style_resolver::StyleResolverForElement;
 
     debug_assert!(rule_inclusion == RuleInclusion::DefaultOnly ||
-                  ignore_existing_style ||
                   pseudo.map_or(false, |p| p.is_before_or_after()) ||
                   element.borrow_data().map_or(true, |d| !d.has_styles()),
                   "Why are we here?");
     let mut ancestors_requiring_style_resolution = SmallVec::<[E; 16]>::new();
 
     // Clear the bloom filter, just in case the caller is reusing TLS.
     context.thread_local.bloom_filter.clear();
 
     let mut style = None;
     let mut ancestor = element.traversal_parent();
     while let Some(current) = ancestor {
-        if rule_inclusion == RuleInclusion::All && !ignore_existing_style {
+        if rule_inclusion == RuleInclusion::All {
             if let Some(data) = current.borrow_data() {
                 if let Some(ancestor_style) = data.styles.get_primary() {
                     style = Some(ancestor_style.clone());
                     break;
                 }
             }
         }
         ancestors_requiring_style_resolution.push(current);
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -3641,17 +3641,16 @@ pub extern "C" fn Servo_ResolveStyle(
 
 #[no_mangle]
 pub extern "C" fn Servo_ResolveStyleLazily(
     element: RawGeckoElementBorrowed,
     pseudo_type: CSSPseudoElementType,
     rule_inclusion: StyleRuleInclusion,
     snapshots: *const ServoElementSnapshotTable,
     raw_data: RawServoStyleSetBorrowed,
-    ignore_existing_styles: bool,
 ) -> ServoStyleContextStrong {
     debug_assert!(!snapshots.is_null());
     let global_style_data = &*GLOBAL_STYLE_DATA;
     let guard = global_style_data.shared_lock.read();
     let element = GeckoElement(element);
     let doc_data = PerDocumentStyleData::from_ffi(raw_data);
     let data = doc_data.borrow();
     let rule_inclusion = RuleInclusion::from(rule_inclusion);
@@ -3673,24 +3672,22 @@ pub extern "C" fn Servo_ResolveStyleLazi
             }
             None => Some(styles.primary().clone()),
         }
     };
 
     let is_before_or_after = pseudo.as_ref().map_or(false, |p| p.is_before_or_after());
 
     // In the common case we already have the style. Check that before setting
-    // up all the computation machinery. (Don't use it when we're getting
-    // default styles or in a bfcached document (as indicated by
-    // ignore_existing_styles), though.)
+    // up all the computation machinery.
     //
     // Also, only probe in the ::before or ::after case, since their styles may
     // not be in the `ElementData`, given they may exist but not be applicable
     // to generate an actual pseudo-element (like, having a `content: none`).
-    if rule_inclusion == RuleInclusion::All && !ignore_existing_styles {
+    if rule_inclusion == RuleInclusion::All {
         let styles = element.mutate_data().and_then(|d| {
             if d.has_styles() {
                 finish(&d.styles, is_before_or_after)
             } else {
                 None
             }
         });
         if let Some(result) = styles {
@@ -3709,17 +3706,16 @@ pub extern "C" fn Servo_ResolveStyleLazi
         shared: &shared,
         thread_local: &mut tlc,
     };
 
     let styles = resolve_style(
         &mut context,
         element,
         rule_inclusion,
-        ignore_existing_styles,
         pseudo.as_ref()
     );
 
     finish(&styles, /* is_probe = */ false)
         .expect("We're not probing, so we should always get a style back")
         .into()
 }
 
