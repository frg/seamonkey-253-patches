# HG changeset patch
# User Matt Woodrow <mwoodrow@mozilla.com>
# Date 1518415389 -46800
# Node ID 4aa9b74724b8326d5f13cad627ea1fddc9bcaf47
# Parent  0deb7b23b31c0fe3bafaadd0619f0284e82abd31
Bug 1435649 - Don't deference the display item when attempting the early return in ComputeGeometryChangeForItem to avoid a cache miss. r=jnicol

diff --git a/layout/painting/FrameLayerBuilder.cpp b/layout/painting/FrameLayerBuilder.cpp
--- a/layout/painting/FrameLayerBuilder.cpp
+++ b/layout/painting/FrameLayerBuilder.cpp
@@ -139,16 +139,17 @@ DisplayItemData::DisplayItemData(LayerMa
 
   : mRefCnt(0)
   , mParent(aParent)
   , mLayer(aLayer)
   , mDisplayItemKey(aKey)
   , mItem(nullptr)
   , mUsed(true)
   , mIsInvalid(false)
+  , mReusedItem(false)
 {
   MOZ_COUNT_CTOR(DisplayItemData);
 
   if (!sAliveDisplayItemDatas) {
     sAliveDisplayItemDatas = new nsTHashtable<nsPtrHashKey<DisplayItemData>>();
   }
   MOZ_RELEASE_ASSERT(!sAliveDisplayItemDatas->Contains(this));
   sAliveDisplayItemDatas->PutEntry(this);
@@ -180,19 +181,20 @@ DisplayItemData::RemoveFrame(nsIFrame* a
   SmallPointerArray<DisplayItemData>& array = aFrame->DisplayItemData();
   array.RemoveElement(this);
 }
 
 void
 DisplayItemData::EndUpdate()
 {
   MOZ_RELEASE_ASSERT(mLayer);
-  MOZ_ASSERT(!mItem);
+  mItem = nullptr;
   mIsInvalid = false;
   mUsed = false;
+  mReusedItem = false;
 }
 
 void
 DisplayItemData::EndUpdate(nsAutoPtr<nsDisplayItemGeometry> aGeometry)
 {
   MOZ_RELEASE_ASSERT(mLayer);
   MOZ_ASSERT(mItem);
   MOZ_ASSERT(mGeometry || aGeometry);
@@ -216,16 +218,17 @@ DisplayItemData::BeginUpdate(Layer* aLay
   mLayer = aLayer;
   mOptLayer = nullptr;
   mInactiveManager = nullptr;
   mLayerState = aState;
   mUsed = true;
 
   if (aLayer->AsPaintedLayer()) {
     mItem = aItem;
+    mReusedItem = aItem->IsReused();
   }
 
   if (!aItem) {
     return;
   }
 
   // We avoid adding or removing element unnecessarily
   // since we have to modify userdata each time
@@ -4626,18 +4629,18 @@ FrameLayerBuilder::ComputeGeometryChange
     return;
   }
 
   // If we're a reused display item, then we can't be invalid, so no need to
   // do an in-depth comparison. If we haven't previously stored geometry
   // for this item (if it was an active layer), then we can't skip this
   // yet.
   nsAutoPtr<nsDisplayItemGeometry> geometry;
-  if (item->IsReused() && aData->mGeometry) {
-    aData->EndUpdate(geometry);
+  if (aData->mReusedItem && aData->mGeometry) {
+    aData->EndUpdate();
     return;
   }
 
   PaintedDisplayItemLayerUserData* layerData =
     static_cast<PaintedDisplayItemLayerUserData*>(aData->mLayer->GetUserData(&gPaintedDisplayItemLayerUserData));
   nsPoint shift = layerData->mAnimatedGeometryRootOrigin - layerData->mLastAnimatedGeometryRootOrigin;
 
   const DisplayItemClip& clip = item->GetClip();
diff --git a/layout/painting/FrameLayerBuilder.h b/layout/painting/FrameLayerBuilder.h
--- a/layout/painting/FrameLayerBuilder.h
+++ b/layout/painting/FrameLayerBuilder.h
@@ -180,16 +180,17 @@ private:
   nsRegion mChangedFrameInvalidations;
 
   /**
     * Used to track if data currently stored in mFramesWithLayers (from an existing
     * paint) has been updated in the current paint.
     */
   bool            mUsed;
   bool            mIsInvalid;
+  bool            mReusedItem;
 };
 
 class RefCountedRegion {
 private:
   ~RefCountedRegion() {}
 public:
   NS_INLINE_DECL_REFCOUNTING(RefCountedRegion)
 
