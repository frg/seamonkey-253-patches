# HG changeset patch
# User Andrew Halberstadt <ahal@pm.me>
# Date 1602646324 0
# Node ID 7a7006c24e69828fdc543ed04229964329563274
# Parent  f9c4c56fdbcfbd577945c623cc48295594279632
Bug 1670385 - [mach] Move 'TestBase._run_mach' to a pytest fixture, r=firefox-build-system-reviewers,rstewart

I accidentally broke the 'mach' unittests on Python 2 due to some difference in the unittest
module. Rather than poking into 'unittest', this patch moves us closer to the pytest format
while also fixing the issue.

Differential Revision: https://phabricator.services.mozilla.com/D93420

diff --git a/python/mach/mach/test/common.py b/python/mach/mach/test/conftest.py
rename from python/mach/mach/test/common.py
rename to python/mach/mach/test/conftest.py
--- a/python/mach/mach/test/common.py
+++ b/python/mach/mach/test/conftest.py
@@ -1,62 +1,84 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
-from __future__ import absolute_import
-from __future__ import unicode_literals
+from __future__ import absolute_import, unicode_literals
 
 import os
 import sys
 import unittest
 
+import pytest
 import six
+from buildconfig import topsrcdir
 
 try:
     from StringIO import StringIO
 except ImportError:
     # TODO io.StringIO causes failures with Python 2 (needs to be sorted out)
     from io import StringIO
 
 from mach.main import Mach
 
 here = os.path.abspath(os.path.dirname(__file__))
+PROVIDER_DIR = os.path.join(here, 'providers')
 
 
-class TestBase(unittest.TestCase):
-    provider_dir = os.path.join(here, 'providers')
+@pytest.fixture(scope="class")
+def get_mach(request):
 
-    @classmethod
-    def get_mach(cls, provider_files=None, entry_point=None, context_handler=None):
+    def _populate_context(key):
+        if key == 'topdir':
+            return topsrcdir
+
+    def inner(provider_files=None, entry_point=None, context_handler=None):
         m = Mach(os.getcwd())
         m.define_category('testing', 'Mach unittest', 'Testing for mach core', 10)
         m.define_category('misc', 'Mach misc', 'Testing for mach core', 20)
-        m.populate_context_handler = context_handler
+        m.populate_context_handler = context_handler or _populate_context
 
         if provider_files:
             if isinstance(provider_files, six.string_types):
                 provider_files = [provider_files]
 
             for path in provider_files:
-                m.load_commands_from_file(os.path.join(cls.provider_dir, path))
+                m.load_commands_from_file(os.path.join(PROVIDER_DIR, path))
 
         if entry_point:
             m.load_commands_from_entry_point(entry_point)
 
         return m
 
-    def _run_mach(self, argv, *args, **kwargs):
-        m = self.get_mach(*args, **kwargs)
+    if request.cls and issubclass(request.cls, unittest.TestCase):
+        request.cls.get_mach = lambda cls, *args, **kwargs: inner(*args, **kwargs)
+    return inner
+
+
+@pytest.fixture(scope="class")
+def run_mach(request, get_mach):
+
+    def inner(argv, *args, **kwargs):
+        m = get_mach(*args, **kwargs)
 
         stdout = StringIO()
         stderr = StringIO()
 
         if sys.version_info < (3, 0):
             stdout.encoding = 'UTF-8'
             stderr.encoding = 'UTF-8'
 
         try:
             result = m.run(argv, stdout=stdout, stderr=stderr)
         except SystemExit:
             result = None
 
         return (result, stdout.getvalue(), stderr.getvalue())
+
+    if request.cls and issubclass(request.cls, unittest.TestCase):
+        request.cls._run_mach = lambda cls, *args, **kwargs: inner(*args, **kwargs)
+    return inner
+
+
+@pytest.mark.usefixtures("get_mach", "run_mach")
+class TestBase(unittest.TestCase):
+    pass
diff --git a/python/mach/mach/test/test_commands.py b/python/mach/mach/test/test_commands.py
--- a/python/mach/mach/test/test_commands.py
+++ b/python/mach/mach/test/test_commands.py
@@ -7,73 +7,71 @@ from __future__ import absolute_import, 
 import os
 import sys
 
 import pytest
 from mozunit import main
 
 from buildconfig import topsrcdir
 import mach
-from mach.test.common import TestBase
 
 ALL_COMMANDS = [
     'cmd_bar',
     'cmd_foo',
     'cmd_foobar',
     'mach-commands',
     'mach-completion',
     'mach-debug-commands',
 ]
 
 
 @pytest.fixture
-def run_mach():
+def run_completion(run_mach):
 
-    tester = TestBase()
-
-    def inner(args):
+    def inner(args=[]):
         mach_dir = os.path.dirname(mach.__file__)
         providers = [
             'commands.py',
             os.path.join(mach_dir, 'commands', 'commandinfo.py'),
         ]
 
         def context_handler(key):
             if key == 'topdir':
                 return topsrcdir
 
-        return tester._run_mach(args, providers, context_handler=context_handler)
+        args = ["mach-completion"] + args
+        return run_mach(args, providers, context_handler=context_handler)
 
     return inner
 
 
 def format(targets):
     return "\n".join(targets) + "\n"
 
 
-def test_mach_completion(run_mach):
-    result, stdout, stderr = run_mach(['mach-completion'])
+def test_mach_completion(run_completion):
+    result, stdout, stderr = run_completion()
     assert result == 0
     assert stdout == format(ALL_COMMANDS)
 
-    result, stdout, stderr = run_mach(['mach-completion', 'cmd_f'])
+    result, stdout, stderr = run_completion(['cmd_f'])
     assert result == 0
     # While it seems like this should return only commands that have
     # 'cmd_f' as a prefix, the completion script will handle this case
     # properly.
     assert stdout == format(ALL_COMMANDS)
 
-    result, stdout, stderr = run_mach(['mach-completion', 'cmd_foo'])
+    result, stdout, stderr = run_completion(['cmd_foo'])
     assert result == 0
     assert stdout == format(['help', '--arg'])
 
 
 @pytest.mark.parametrize("shell", ("bash", "fish", "zsh"))
-def test_generate_mach_completion_script(run_mach, shell):
-    rv, out, err = run_mach(["mach-completion", shell])
+def test_generate_mach_completion_script(run_completion, shell):
+    rv, out, err = run_completion([shell])
     print(out)
     print(err, file=sys.stderr)
     assert rv == 0
     assert err == ""
 
     assert "cmd_foo" in out
     assert "arg" in out
     assert "cmd_foobar" in out
diff --git a/python/mach/mach/test/test_conditions.py b/python/mach/mach/test/test_conditions.py
--- a/python/mach/mach/test/test_conditions.py
+++ b/python/mach/mach/test/test_conditions.py
@@ -6,17 +6,17 @@ from __future__ import absolute_import
 from __future__ import unicode_literals
 
 import os
 
 from buildconfig import topsrcdir
 from mach.base import MachError
 from mach.main import Mach
 from mach.registrar import Registrar
-from mach.test.common import TestBase
+from mach.test.conftest import TestBase, PROVIDER_DIR
 
 from mozunit import main
 
 
 def _make_populate_context(include_extra_attributes):
     def _populate_context(key=None):
         if key is None:
             return
@@ -38,60 +38,59 @@ def _make_populate_context(include_extra
 
 _populate_bare_context = _make_populate_context(False)
 _populate_context = _make_populate_context(True)
 
 
 class TestConditions(TestBase):
     """Tests for conditionally filtering commands."""
 
-    def _run_mach(self, args, context_handler=_populate_bare_context):
-        return TestBase._run_mach(self, args, 'conditions.py',
-                                  context_handler=context_handler)
+    def _run(self, args, context_handler=_populate_bare_context):
+        return self._run_mach(args, 'conditions.py', context_handler=context_handler)
 
     def test_conditions_pass(self):
         """Test that a command which passes its conditions is runnable."""
 
-        self.assertEquals((0, '', ''), self._run_mach(['cmd_foo']))
-        self.assertEquals((0, '', ''), self._run_mach(['cmd_foo_ctx'], _populate_context))
+        self.assertEquals((0, '', ''), self._run(['cmd_foo']))
+        self.assertEquals((0, '', ''), self._run(['cmd_foo_ctx'], _populate_context))
 
     def test_invalid_context_message(self):
         """Test that commands which do not pass all their conditions
         print the proper failure message."""
 
         def is_bar():
             """Bar must be true"""
         fail_conditions = [is_bar]
 
         for name in ('cmd_bar', 'cmd_foobar'):
-            result, stdout, stderr = self._run_mach([name])
+            result, stdout, stderr = self._run([name])
             self.assertEquals(1, result)
 
             fail_msg = Registrar._condition_failed_message(name, fail_conditions)
             self.assertEquals(fail_msg.rstrip(), stdout.rstrip())
 
         for name in ('cmd_bar_ctx', 'cmd_foobar_ctx'):
-            result, stdout, stderr = self._run_mach([name], _populate_context)
+            result, stdout, stderr = self._run([name], _populate_context)
             self.assertEquals(1, result)
 
             fail_msg = Registrar._condition_failed_message(name, fail_conditions)
             self.assertEquals(fail_msg.rstrip(), stdout.rstrip())
 
     def test_invalid_type(self):
         """Test that a condition which is not callable raises an exception."""
 
         m = Mach(os.getcwd())
         m.define_category('testing', 'Mach unittest', 'Testing for mach core', 10)
         self.assertRaises(MachError, m.load_commands_from_file,
-                          os.path.join(self.provider_dir, 'conditions_invalid.py'))
+                          os.path.join(PROVIDER_DIR, 'conditions_invalid.py'))
 
     def test_help_message(self):
         """Test that commands that are not runnable do not show up in help."""
 
-        result, stdout, stderr = self._run_mach(['help'], _populate_context)
+        result, stdout, stderr = self._run(['help'], _populate_context)
         self.assertIn('cmd_foo', stdout)
         self.assertNotIn('cmd_bar', stdout)
         self.assertNotIn('cmd_foobar', stdout)
         self.assertIn('cmd_foo_ctx', stdout)
         self.assertNotIn('cmd_bar_ctx', stdout)
         self.assertNotIn('cmd_foobar_ctx', stdout)
 
 
diff --git a/python/mach/mach/test/test_dispatcher.py b/python/mach/mach/test/test_dispatcher.py
--- a/python/mach/mach/test/test_dispatcher.py
+++ b/python/mach/mach/test/test_dispatcher.py
@@ -1,29 +1,31 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import
 from __future__ import unicode_literals
 
 import os
+import unittest
 from io import StringIO
 
+import pytest
 from mozunit import main
 from six import string_types
 
 from mach.base import CommandContext
 from mach.registrar import Registrar
-from mach.test.common import TestBase
 
 here = os.path.abspath(os.path.dirname(__file__))
 
 
-class TestDispatcher(TestBase):
+@pytest.mark.usefixtures("get_mach", "run_mach")
+class TestDispatcher(unittest.TestCase):
     """Tests dispatch related code"""
 
     def get_parser(self, config=None):
         mach = self.get_mach('basic.py')
 
         for provider in Registrar.settings_providers:
             mach.settings.register_provider(provider)
 
diff --git a/python/mach/mach/test/test_entry_point.py b/python/mach/mach/test/test_entry_point.py
--- a/python/mach/mach/test/test_entry_point.py
+++ b/python/mach/mach/test/test_entry_point.py
@@ -4,17 +4,17 @@
 from __future__ import absolute_import
 from __future__ import unicode_literals
 
 import imp
 import os
 import sys
 
 from mach.base import MachError
-from mach.test.common import TestBase
+from mach.test.conftest import TestBase
 from mock import patch
 
 from mozunit import main
 
 
 here = os.path.abspath(os.path.dirname(__file__))
 
 
@@ -28,35 +28,35 @@ class Entry():
             return self.providers
         return _providers
 
 
 class TestEntryPoints(TestBase):
     """Test integrating with setuptools entry points"""
     provider_dir = os.path.join(here, 'providers')
 
-    def _run_mach(self):
-        return TestBase._run_mach(self, ['help'], entry_point='mach.providers')
+    def _run_help(self):
+        return self._run_mach(['help'], entry_point='mach.providers')
 
     @patch('pkg_resources.iter_entry_points')
     def test_load_entry_point_from_directory(self, mock):
         # Ensure parent module is present otherwise we'll (likely) get
         # an error due to unknown parent.
         if 'mach.commands' not in sys.modules:
             mod = imp.new_module('mach.commands')
             sys.modules['mach.commands'] = mod
 
         mock.return_value = [Entry([self.provider_dir])]
         # Mach error raised due to conditions_invalid.py
         with self.assertRaises(MachError):
-            self._run_mach()
+            self._run_help()
 
     @patch('pkg_resources.iter_entry_points')
     def test_load_entry_point_from_file(self, mock):
         mock.return_value = [Entry([os.path.join(self.provider_dir, 'basic.py')])]
 
-        result, stdout, stderr = self._run_mach()
+        result, stdout, stderr = self._run_help()
         self.assertIsNone(result)
         self.assertIn('cmd_foo', stdout)
 
 
 if __name__ == '__main__':
     main()
diff --git a/python/mach/mach/test/test_error_output.py b/python/mach/mach/test/test_error_output.py
--- a/python/mach/mach/test/test_error_output.py
+++ b/python/mach/mach/test/test_error_output.py
@@ -1,46 +1,28 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
-from __future__ import absolute_import
-from __future__ import unicode_literals
+from __future__ import absolute_import, unicode_literals
 
-from buildconfig import topsrcdir
 from mach.main import (
     COMMAND_ERROR,
     MODULE_ERROR
 )
-from mach.test.common import TestBase
-
 from mozunit import main
 
 
-def _populate_context(key):
-    if key == 'topdir':
-        return topsrcdir
-
-class TestErrorOutput(TestBase):
-
-    def _run_mach(self, args):
-        return TestBase._run_mach(
-            self, args, 'throw.py', context_handler=_populate_context)
+def test_command_error(run_mach):
+    result, stdout, stderr = run_mach(['throw', '--message', 'Command Error'],
+                                      provider_files='throw.py')
+    assert result == 1
+    assert COMMAND_ERROR_TEMPLATE % 'throw' in stdout
 
-    def test_command_error(self):
-        result, stdout, stderr = self._run_mach(['throw', '--message',
-                                                'Command Error'])
-
-        self.assertEqual(result, 1)
-
-        self.assertIn(COMMAND_ERROR, stdout)
-
-    def test_invoked_error(self):
-        result, stdout, stderr = self._run_mach(['throw_deep', '--message',
-                                                'Deep stack'])
-
-        self.assertEqual(result, 1)
-
-        self.assertIn(MODULE_ERROR, stdout)
+def test_invoked_error(run_mach):
+    result, stdout, stderr = run_mach(['throw_deep', '--message', 'Deep stack'],
+                                      provider_files='throw.py')
+    assert result == 1
+    assert MODULE_ERROR_TEMPLATE % 'throw_deep' in stdout
 
 
 if __name__ == '__main__':
     main()
diff --git a/python/mach/mach/test/test_mach.py b/python/mach/mach/test/test_mach.py
--- a/python/mach/mach/test/test_mach.py
+++ b/python/mach/mach/test/test_mach.py
@@ -1,26 +1,25 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, unicode_literals
 
 import os
 
-from mach.test.common import TestBase
 from mozunit import main
 
 
-def test_set_isatty_environ(monkeypatch):
+def test_set_isatty_environ(monkeypatch, get_mach):
     # Make sure the 'MACH_STDOUT_ISATTY' variable gets set.
     monkeypatch.delenv('MACH_STDOUT_ISATTY', raising=False)
     monkeypatch.setattr(os, 'isatty', lambda fd: True)
 
-    m = TestBase.get_mach()
+    m = get_mach()
     orig_run = m._run
     env_is_set = []
 
     def wrap_run(*args, **kwargs):
         env_is_set.append('MACH_STDOUT_ISATTY' in os.environ)
         return orig_run(*args, **kwargs)
 
     monkeypatch.setattr(m, '_run', wrap_run)
