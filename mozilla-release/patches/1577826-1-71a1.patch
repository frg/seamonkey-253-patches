# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1567640973 0
# Node ID 7f09f747bec7226d2d67575662b775586145cb07
# Parent  cf4a6f305b85a178833a60c9232a94cbe33e23c7
Bug 1577826 - [mozbuild] Create an 'ensure_subprocess_env' utility function, r=glandium

Differential Revision: https://phabricator.services.mozilla.com/D44667

diff --git a/python/mozbuild/mozbuild/configure/__init__.py b/python/mozbuild/mozbuild/configure/__init__.py
--- a/python/mozbuild/mozbuild/configure/__init__.py
+++ b/python/mozbuild/mozbuild/configure/__init__.py
@@ -25,16 +25,17 @@ from mozbuild.configure.options import (
 )
 from mozbuild.configure.help import HelpFormatter
 from mozbuild.configure.util import (
     ConfigureOutputHandler,
     getpreferredencoding,
     LineIO,
 )
 from mozbuild.util import (
+    ensure_subprocess_env,
     exec_,
     memoize,
     memoized_property,
     ReadOnlyDict,
     ReadOnlyNamespace,
     system_encoding,
 )
 
@@ -879,30 +880,17 @@ class ConfigureSandbox(dict):
         def wrap(function):
             def wrapper(*args, **kwargs):
                 if 'env' not in kwargs:
                     kwargs['env'] = dict(self._environ)
                 # Subprocess on older Pythons can't handle unicode keys or
                 # values in environment dicts while subprocess on newer Pythons
                 # needs text in the env. Normalize automagically so callers
                 # don't have to deal with this.
-                env = {}
-                for k, v in six.iteritems(kwargs['env']):
-                    if sys.version_info[0] < 3:
-                        if isinstance(k, six.text_type):
-                            k = k.encode(system_encoding)
-                        if isinstance(v, six.text_type):
-                            v = v.encode(system_encoding)
-                    else:
-                        if isinstance(k, six.binary_type):
-                            k = k.decode(system_encoding)
-                        if isinstance(v, six.binary_type):
-                            v = v.decode(system_encoding)
-                    env[k] = v
-                kwargs['env'] = env
+                kwargs['env'] = ensure_subprocess_env(kwargs['env'], encoding=system_encoding)
                 return function(*args, **kwargs)
             return wrapper
 
         for f in ('call', 'check_call', 'check_output', 'Popen'):
             wrapped_subprocess[f] = wrap(wrapped_subprocess[f])
 
         return ReadOnlyNamespace(**wrapped_subprocess)
 
diff --git a/python/mozbuild/mozbuild/util.py b/python/mozbuild/mozbuild/util.py
--- a/python/mozbuild/mozbuild/util.py
+++ b/python/mozbuild/mozbuild/util.py
@@ -1382,18 +1382,34 @@ def patch_main():
                            ''.join(x[12:] for x in fork_code[1:]))
             cmdline = orig_command_line()
             cmdline[2] = fork_string
             return cmdline
         orig_command_line = forking.get_command_line
         forking.get_command_line = my_get_command_line
 
 
-def ensure_bytes(value):
+def ensure_bytes(value, encoding='utf-8'):
     if isinstance(value, six.text_type):
-        return value.encode('utf8')
+        return value.encode(encoding)
+    return value
+
+
+def ensure_unicode(value, encoding='utf-8'):
+    if isinstance(value, six.binary_type):
+        return value.decode(encoding)
     return value
 
 
-def ensure_unicode(value):
-    if isinstance(value, type(b'')):
-        return value.decode('utf8')
-    return value
+def ensure_subprocess_env(env, encoding='utf-8'):
+    """Ensure the environment is in the correct format for the `subprocess`
+    module.
+
+    This will convert all keys and values to bytes on Python 2, and text on
+    Python 3.
+
+    Args:
+        env (dict): Environment to ensure.
+        encoding (str): Encoding to use when converting to/from bytes/text
+                        (default: utf-8).
+    """
+    ensure = ensure_bytes if sys.version_info[0] < 3 else ensure_unicode
+    return {ensure(k, encoding): ensure(v, encoding) for k, v in six.iteritems(env)}
