# HG changeset patch
# User Boris Zbarsky <bzbarsky@mit.edu>
# Date 1523456821 14400
# Node ID 0008c21a14abceaa092c96777ed97c86b4298562
# Parent  6ec283afcf347457ca77e5a3b6b8a4b3b36e2e75
Bug 1453132.  Change nsIEventListenerService to use WebIDL event listeners, not nsIDOMEventListener.  r=smaug

The test change is needed because there is no notok() function.  But the old
nsIDOMEventListener setup would fail to report the exception anywhere, so the
test still passed (albeit without testing what it thought it was testing).  The
new setup reports the exception via an error event on the window, and the test
harness notices that.

MozReview-Commit-ID: 3ISmcyhMk0R

diff --git a/dom/events/EventListenerManager.cpp b/dom/events/EventListenerManager.cpp
--- a/dom/events/EventListenerManager.cpp
+++ b/dom/events/EventListenerManager.cpp
@@ -1429,34 +1429,33 @@ EventListenerManager::RemoveEventListene
     const auto& options = aOptions.GetAsEventListenerOptions();
     flags.mCapture = options.mCapture;
     flags.mInSystemGroup = options.mMozSystemGroup;
   }
   RemoveEventListenerByType(Move(aListenerHolder), aType, flags);
 }
 
 void
-EventListenerManager::AddListenerForAllEvents(nsIDOMEventListener* aDOMListener,
+EventListenerManager::AddListenerForAllEvents(EventListener* aDOMListener,
                                               bool aUseCapture,
                                               bool aWantsUntrusted,
                                               bool aSystemEventGroup)
 {
   EventListenerFlags flags;
   flags.mCapture = aUseCapture;
   flags.mAllowUntrustedEvents = aWantsUntrusted;
   flags.mInSystemGroup = aSystemEventGroup;
   AddEventListenerInternal(EventListenerHolder(aDOMListener), eAllEvents,
                            nullptr, EmptyString(), flags, false, true);
 }
 
 void
-EventListenerManager::RemoveListenerForAllEvents(
-                        nsIDOMEventListener* aDOMListener,
-                        bool aUseCapture,
-                        bool aSystemEventGroup)
+EventListenerManager::RemoveListenerForAllEvents(EventListener* aDOMListener,
+                                                 bool aUseCapture,
+                                                 bool aSystemEventGroup)
 {
   EventListenerFlags flags;
   flags.mCapture = aUseCapture;
   flags.mInSystemGroup = aSystemEventGroup;
   RemoveEventListenerInternal(EventListenerHolder(aDOMListener), eAllEvents,
                               nullptr, EmptyString(), flags, true);
 }
 
diff --git a/dom/events/EventListenerManager.h b/dom/events/EventListenerManager.h
--- a/dom/events/EventListenerManager.h
+++ b/dom/events/EventListenerManager.h
@@ -292,38 +292,50 @@ public:
   }
   void RemoveEventListener(const nsAString& aType,
                            dom::EventListener* aListener,
                            const dom::EventListenerOptionsOrBoolean& aOptions)
   {
     RemoveEventListener(aType, EventListenerHolder(aListener), aOptions);
   }
 
-  void AddListenerForAllEvents(nsIDOMEventListener* aListener,
+  void AddListenerForAllEvents(dom::EventListener* aListener,
                                bool aUseCapture,
                                bool aWantsUntrusted,
                                bool aSystemEventGroup);
-  void RemoveListenerForAllEvents(nsIDOMEventListener* aListener,
+  void RemoveListenerForAllEvents(dom::EventListener* aListener,
                                   bool aUseCapture,
                                   bool aSystemEventGroup);
 
   /**
   * Sets events listeners of all types.
   * @param an event listener
   */
-  void AddEventListenerByType(nsIDOMEventListener *aListener,
+  void AddEventListenerByType(nsIDOMEventListener* aListener,
+                              const nsAString& type,
+                              const EventListenerFlags& aFlags)
+  {
+    AddEventListenerByType(EventListenerHolder(aListener), type, aFlags);
+  }
+  void AddEventListenerByType(dom::EventListener* aListener,
                               const nsAString& type,
                               const EventListenerFlags& aFlags)
   {
     AddEventListenerByType(EventListenerHolder(aListener), type, aFlags);
   }
   void AddEventListenerByType(EventListenerHolder aListener,
                               const nsAString& type,
                               const EventListenerFlags& aFlags);
-  void RemoveEventListenerByType(nsIDOMEventListener *aListener,
+  void RemoveEventListenerByType(nsIDOMEventListener* aListener,
+                                 const nsAString& type,
+                                 const EventListenerFlags& aFlags)
+  {
+    RemoveEventListenerByType(EventListenerHolder(aListener), type, aFlags);
+  }
+  void RemoveEventListenerByType(dom::EventListener* aListener,
                                  const nsAString& type,
                                  const EventListenerFlags& aFlags)
   {
     RemoveEventListenerByType(EventListenerHolder(aListener), type, aFlags);
   }
   void RemoveEventListenerByType(EventListenerHolder aListener,
                                  const nsAString& type,
                                  const EventListenerFlags& aFlags);
diff --git a/dom/events/EventListenerService.cpp b/dom/events/EventListenerService.cpp
--- a/dom/events/EventListenerService.cpp
+++ b/dom/events/EventListenerService.cpp
@@ -5,16 +5,18 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "EventListenerService.h"
 #include "mozilla/BasicEvents.h"
 #include "mozilla/EventDispatcher.h"
 #include "mozilla/EventListenerManager.h"
 #include "mozilla/JSEventHandler.h"
 #include "mozilla/Maybe.h"
+#include "mozilla/dom/EventListenerBinding.h"
+#include "mozilla/dom/ScriptSettings.h"
 #include "nsArrayUtils.h"
 #include "nsCOMArray.h"
 #include "nsDOMClassInfoID.h"
 #include "nsIXPConnect.h"
 #include "nsJSUtils.h"
 #include "nsMemory.h"
 #include "nsServiceManagerUtils.h"
 #include "nsArray.h"
@@ -270,94 +272,129 @@ EventListenerService::HasListenersFor(ns
   nsCOMPtr<EventTarget> eventTarget = do_QueryInterface(aEventTarget);
   NS_ENSURE_TRUE(eventTarget, NS_ERROR_NO_INTERFACE);
 
   EventListenerManager* elm = eventTarget->GetExistingListenerManager();
   *aRetVal = elm && elm->HasListenersFor(aType);
   return NS_OK;
 }
 
+static already_AddRefed<EventListener>
+ToEventListener(JSContext* aCx, JS::Handle<JS::Value> aValue)
+{
+  if (NS_WARN_IF(!aValue.isObject())) {
+    return nullptr;
+  }
+
+  JS::Rooted<JSObject*> obj(aCx, &aValue.toObject());
+  RefPtr<EventListener> listener =
+    new EventListener(aCx, obj, GetIncumbentGlobal());
+  return listener.forget();
+}
+
 NS_IMETHODIMP
 EventListenerService::AddSystemEventListener(nsIDOMEventTarget *aTarget,
                                              const nsAString& aType,
-                                             nsIDOMEventListener* aListener,
-                                             bool aUseCapture)
+                                             JS::Handle<JS::Value> aListener,
+                                             bool aUseCapture,
+                                             JSContext* aCx)
 {
   NS_PRECONDITION(aTarget, "Missing target");
-  NS_PRECONDITION(aListener, "Missing listener");
 
   nsCOMPtr<EventTarget> eventTarget = do_QueryInterface(aTarget);
   NS_ENSURE_TRUE(eventTarget, NS_ERROR_NO_INTERFACE);
 
+  RefPtr<EventListener> listener = ToEventListener(aCx, aListener);
+  if (!listener) {
+    return NS_ERROR_UNEXPECTED;
+  }
+
   EventListenerManager* manager = eventTarget->GetOrCreateListenerManager();
   NS_ENSURE_STATE(manager);
 
   EventListenerFlags flags =
     aUseCapture ? TrustedEventsAtSystemGroupCapture() :
                   TrustedEventsAtSystemGroupBubble();
-  manager->AddEventListenerByType(aListener, aType, flags);
+  manager->AddEventListenerByType(listener, aType, flags);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 EventListenerService::RemoveSystemEventListener(nsIDOMEventTarget *aTarget,
                                                 const nsAString& aType,
-                                                nsIDOMEventListener* aListener,
-                                                bool aUseCapture)
+                                                JS::Handle<JS::Value> aListener,
+                                                bool aUseCapture,
+                                                JSContext* aCx)
 {
   NS_PRECONDITION(aTarget, "Missing target");
-  NS_PRECONDITION(aListener, "Missing listener");
 
   nsCOMPtr<EventTarget> eventTarget = do_QueryInterface(aTarget);
   NS_ENSURE_TRUE(eventTarget, NS_ERROR_NO_INTERFACE);
 
+  RefPtr<EventListener> listener = ToEventListener(aCx, aListener);
+  if (!listener) {
+    return NS_ERROR_UNEXPECTED;
+  }
+
   EventListenerManager* manager = eventTarget->GetExistingListenerManager();
   if (manager) {
     EventListenerFlags flags =
       aUseCapture ? TrustedEventsAtSystemGroupCapture() :
                     TrustedEventsAtSystemGroupBubble();
-    manager->RemoveEventListenerByType(aListener, aType, flags);
+    manager->RemoveEventListenerByType(listener, aType, flags);
   }
 
   return NS_OK;
 }
 
 NS_IMETHODIMP
 EventListenerService::AddListenerForAllEvents(nsIDOMEventTarget* aTarget,
-                                              nsIDOMEventListener* aListener,
+                                              JS::Handle<JS::Value> aListener,
                                               bool aUseCapture,
                                               bool aWantsUntrusted,
-                                              bool aSystemEventGroup)
+                                              bool aSystemEventGroup,
+                                              JSContext* aCx)
 {
-  NS_ENSURE_STATE(aTarget && aListener);
+  NS_ENSURE_STATE(aTarget);
 
   nsCOMPtr<EventTarget> eventTarget = do_QueryInterface(aTarget);
   NS_ENSURE_TRUE(eventTarget, NS_ERROR_NO_INTERFACE);
 
+  RefPtr<EventListener> listener = ToEventListener(aCx, aListener);
+  if (!listener) {
+    return NS_ERROR_UNEXPECTED;
+  }
+
   EventListenerManager* manager = eventTarget->GetOrCreateListenerManager();
   NS_ENSURE_STATE(manager);
-  manager->AddListenerForAllEvents(aListener, aUseCapture, aWantsUntrusted,
-                               aSystemEventGroup);
+  manager->AddListenerForAllEvents(listener, aUseCapture, aWantsUntrusted,
+                                   aSystemEventGroup);
   return NS_OK;
 }
 
 NS_IMETHODIMP
 EventListenerService::RemoveListenerForAllEvents(nsIDOMEventTarget* aTarget,
-                                                 nsIDOMEventListener* aListener,
+                                                 JS::Handle<JS::Value> aListener,
                                                  bool aUseCapture,
-                                                 bool aSystemEventGroup)
+                                                 bool aSystemEventGroup,
+                                                 JSContext* aCx)
 {
-  NS_ENSURE_STATE(aTarget && aListener);
+  NS_ENSURE_STATE(aTarget);
 
   nsCOMPtr<EventTarget> eventTarget = do_QueryInterface(aTarget);
   NS_ENSURE_TRUE(eventTarget, NS_ERROR_NO_INTERFACE);
 
+  RefPtr<EventListener> listener = ToEventListener(aCx, aListener);
+  if (!listener) {
+    return NS_ERROR_UNEXPECTED;
+  }
+
   EventListenerManager* manager = eventTarget->GetExistingListenerManager();
   if (manager) {
-    manager->RemoveListenerForAllEvents(aListener, aUseCapture, aSystemEventGroup);
+    manager->RemoveListenerForAllEvents(listener, aUseCapture, aSystemEventGroup);
   }
   return NS_OK;
 }
 
 NS_IMETHODIMP
 EventListenerService::AddListenerChangeListener(nsIListenerChangeListener* aListener)
 {
   if (!mChangeListeners.Contains(aListener)) {
diff --git a/dom/events/nsIEventListenerService.idl b/dom/events/nsIEventListenerService.idl
--- a/dom/events/nsIEventListenerService.idl
+++ b/dom/events/nsIEventListenerService.idl
@@ -1,16 +1,15 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsISupports.idl"
 
-interface nsIDOMEventListener;
 interface nsIDOMEventTarget;
 interface nsIArray;
 
 /**
  * Contains an event target along with a count of event listener changes
  * affecting accessibility.
  */
 [scriptable, uuid(07222b02-da12-4cf4-b2f7-761da007a8d8)]
@@ -89,36 +88,40 @@ interface nsIEventListenerService : nsIS
    * Returns true if a event target has any listener for the given type.
    */
   boolean hasListenersFor(in nsIDOMEventTarget aEventTarget,
                           in DOMString aType);
 
   /**
    * Add a system-group eventlistener to a event target.
    */
+  [implicit_jscontext]
   void addSystemEventListener(in nsIDOMEventTarget target,
                               in DOMString type,
-                              in nsIDOMEventListener listener,
+                              in jsval listener,
                               in boolean useCapture);
 
   /**
    * Remove a system-group eventlistener from a event target.
    */
+  [implicit_jscontext]
   void removeSystemEventListener(in nsIDOMEventTarget target,
                                  in DOMString type,
-                                 in nsIDOMEventListener listener,
+                                 in jsval listener,
                                  in boolean useCapture);
 
+  [implicit_jscontext]
   void addListenerForAllEvents(in nsIDOMEventTarget target,
-                               in nsIDOMEventListener listener,
+                               in jsval listener,
                                [optional] in boolean aUseCapture,
                                [optional] in boolean aWantsUntrusted,
                                [optional] in boolean aSystemEventGroup);
 
+  [implicit_jscontext]
   void removeListenerForAllEvents(in nsIDOMEventTarget target,
-                                  in nsIDOMEventListener listener,
+                                  in jsval listener,
                                   [optional] in boolean aUseCapture,
                                   [optional] in boolean aSystemEventGroup);
 
   void addListenerChangeListener(in nsIListenerChangeListener aListener);
   void removeListenerChangeListener(in nsIListenerChangeListener aListener);
 };
 
diff --git a/dom/events/test/test_bug448602.html b/dom/events/test/test_bug448602.html
--- a/dom/events/test/test_bug448602.html
+++ b/dom/events/test/test_bug448602.html
@@ -180,17 +180,17 @@ function testAllListener() {
   var clickListenerCalled = false;
   var allListenerCalled = false;
   function clickListener() {
     clickListenerCalled = true;
     ok(allListenerCalled, "Should have called '*' listener before normal listener!");
   }
   function allListener2() {
     allListenerCalled = true;
-    notok(clickListenerCalled, "Shouldn't have called click listener before '*' listener!");
+    ok(!clickListenerCalled, "Shouldn't have called click listener before '*' listener!");
   }
   root.onclick = null; // Remove the listener added in earlier tests.
   root.addEventListener("click", clickListener);
   els.addListenerForAllEvents(root, allListener2, false, true);
   l3.dispatchEvent(new MouseEvent("click", { bubbles: true }));
   root.removeEventListener("click", clickListener);
   els.removeListenerForAllEvents(root, allListener2, false);
   ok(allListenerCalled, "Should have called '*' listener");
