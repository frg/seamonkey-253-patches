# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1535173887 -7200
# Node ID 20a43066e367897769f5c155b1daba955cfaeea7
# Parent  4d21fa04a8c2a2d982eae570149816d7ddf32d6b
Bug 1486166 - Set the tooltip text on the parent of the urlbar input. r=dao

The reason bug 1440682 broke this was because I moved all the code to
nsXULElement. However, there was a way for non-XUL elements to get XUL tooltips
before that, which was via the RestyleManager mechanism to handle attribute
mutations.

So the behavior before that patch was that non-XUL elements that got the
attribute dynamically added or removed before that patch got their tooltips,
like the HTML input in the toolbar, but if you specified the attributes
statically in the markup, or while the element was somehow outside of the
document or what not, it would never work.

Given that, this looks completely unintentional, and the fact that this ever
worked was a bit lucky.

Chances are we eventually want tooltip support for HTML elements in chrome
documents, but it is pretty likely that we want to use the HTML tooltip
infrastructure instead of nsXULTooltipListener, which is kind of an odd thing.

Thus, for now patch the code so that it sets it on the container of the <input>,
which is a XUL box that takes the same space as the <input>, instead of moving
all the XUL tooltip support to work on non-XUL elements.

Also, while at it, remove references to inputtooltiptext, since I didn't find a
single reference in the code that would set this attribute ever.

Differential Revision: https://phabricator.services.mozilla.com/D4294

diff --git a/browser/base/content/urlbarBindings.xml b/browser/base/content/urlbarBindings.xml
--- a/browser/base/content/urlbarBindings.xml
+++ b/browser/base/content/urlbarBindings.xml
@@ -27,23 +27,23 @@ file, You can obtain one at http://mozil
       <xul:hbox anonid="textbox-container"
                 class="autocomplete-textbox-container urlbar-textbox-container"
                 flex="1" xbl:inherits="focused">
         <children includes="image|deck|stack|box">
           <xul:image class="autocomplete-icon" allowevents="true"/>
         </children>
         <xul:hbox anonid="textbox-input-box"
                   class="textbox-input-box urlbar-input-box"
-                  flex="1" xbl:inherits="tooltiptext=inputtooltiptext">
+                  flex="1">
           <children/>
           <html:input anonid="input"
                       class="autocomplete-textbox urlbar-input textbox-input"
                       allowevents="true"
                       inputmode="url"
-                      xbl:inherits="tooltiptext=inputtooltiptext,value,maxlength,disabled,size,readonly,placeholder,tabindex,accesskey,focused,textoverflow"/>
+                      xbl:inherits="value,maxlength,disabled,size,readonly,placeholder,tabindex,accesskey,focused,textoverflow"/>
 #ifdef MOZ_PHOTON_THEME
           <xul:image anonid="go-button"
                      class="urlbar-go-button"
                      onclick="gURLBar.handleCommand(event);"
                      tooltiptext="&goEndCap.tooltip;"
                      xbl:inherits="pageproxystate,parentfocused=focused"/>
 #endif
         </xul:hbox>
@@ -850,23 +850,27 @@ file, You can obtain one at http://mozil
           this.popup.overrideValue = "http://www." + url;
         ]]></body>
       </method>
 
       <method name="_initURLTooltip">
         <body><![CDATA[
           if (this.focused || !this.hasAttribute("textoverflow"))
             return;
-          this.inputField.setAttribute("tooltiptext", this.value);
+          // We set the tooltip text on the parent node instead of the input
+          // field because XUL tooltips only work on XUL elements.
+          //
+          // FIXME(bug 1486716): title should work on chrome documents instead.
+          this.inputField.parentNode.setAttribute("tooltiptext", this.value);
         ]]></body>
       </method>
 
       <method name="_hideURLTooltip">
         <body><![CDATA[
-          this.inputField.removeAttribute("tooltiptext");
+          this.inputField.parentNode.removeAttribute("tooltiptext");
         ]]></body>
       </method>
 
       <!-- Returns:
            null if there's a security issue and we should do nothing.
            a URL object if there is one that we're OK with loading,
            a text value otherwise.
            -->
