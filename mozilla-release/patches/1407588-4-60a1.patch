# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1518471903 28800
# Node ID f8478e479e23c1319805ab823c9a604e1236801b
# Parent  5616d18b4fa532e38a1c9a45aad2a271603e10c0
Bug 1407588 - Part 4: Update Async-from-Sync iterator to access "next" only once. r=jandem

diff --git a/js/src/builtin/Promise.cpp b/js/src/builtin/Promise.cpp
--- a/js/src/builtin/Promise.cpp
+++ b/js/src/builtin/Promise.cpp
@@ -2628,16 +2628,21 @@ js::AsyncFromSyncIteratorMethod(JSContex
 
     // Step 2.
     RootedObject resultPromise(cx, CreatePromiseObjectWithoutResolutionFunctions(cx));
     if (!resultPromise)
         return false;
 
     // Step 3.
     if (!thisVal.isObject() || !thisVal.toObject().is<AsyncFromSyncIteratorObject>()) {
+        // NB: See https://github.com/tc39/proposal-async-iteration/issues/105
+        // for why this check shouldn't be necessary as long as we can ensure
+        // the Async-from-Sync iterator can't be accessed directly by user
+        // code.
+
         // Step 3.a.
         RootedValue badGeneratorError(cx);
         if (!GetTypeError(cx, JSMSG_NOT_AN_ASYNC_ITERATOR, &badGeneratorError))
             return false;
 
         // Step 3.b.
         if (!RejectMaybeWrappedPromise(cx, resultPromise, badGeneratorError))
             return false;
@@ -2652,18 +2657,17 @@ js::AsyncFromSyncIteratorMethod(JSContex
 
     // Step 4.
     RootedObject iter(cx, asyncIter->iterator());
 
     RootedValue resultVal(cx);
     RootedValue func(cx);
     if (completionKind == CompletionKind::Normal) {
         // 11.1.3.2.1 steps 5-6 (partially).
-        if (!GetProperty(cx, iter, iter, cx->names().next, &func))
-            return AbruptRejectPromise(cx, args, resultPromise, nullptr);
+        func.set(asyncIter->nextMethod());
     } else if (completionKind == CompletionKind::Return) {
         // 11.1.3.2.2 steps 5-6.
         if (!GetProperty(cx, iter, iter, cx->names().return_, &func))
             return AbruptRejectPromise(cx, args, resultPromise, nullptr);
 
         // Step 7.
         if (func.isNullOrUndefined()) {
             // Step 7.a.
diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -7035,16 +7035,21 @@ BytecodeEmitter::emitAsyncIterator()
     if (!emit1(JSOP_SWAP))                                        // ITERFN OBJ
         return false;
     if (!emitCall(JSOP_CALLITER, 0))                              // ITER
         return false;
     checkTypeSet(JSOP_CALLITER);
     if (!emitCheckIsObj(CheckIsObjectKind::GetIterator))          // ITER
         return false;
 
+    if (!emit1(JSOP_DUP))                                         // ITER ITER
+        return false;
+    if (!emitAtomOp(cx->names().next, JSOP_GETPROP))              // ITER SYNCNEXT
+        return false;
+
     if (!emit1(JSOP_TOASYNCITER))                                 // ITER
         return false;
 
     if (!ifAsyncIterIsUndefined.emitElse())                       // OBJ ITERFN
         return false;
 
     if (!emit1(JSOP_SWAP))                                        // ITERFN OBJ
         return false;
diff --git a/js/src/jit/BaselineCompiler.cpp b/js/src/jit/BaselineCompiler.cpp
--- a/js/src/jit/BaselineCompiler.cpp
+++ b/js/src/jit/BaselineCompiler.cpp
@@ -4080,34 +4080,36 @@ BaselineCompiler::emit_JSOP_TOASYNCGEN()
         return false;
 
     masm.tagValue(JSVAL_TYPE_OBJECT, ReturnReg, R0);
     frame.pop();
     frame.push(R0);
     return true;
 }
 
-typedef JSObject* (*ToAsyncIterFn)(JSContext*, HandleObject);
+typedef JSObject* (*ToAsyncIterFn)(JSContext*, HandleObject, HandleValue);
 static const VMFunction ToAsyncIterInfo =
     FunctionInfo<ToAsyncIterFn>(js::CreateAsyncFromSyncIterator, "ToAsyncIter");
 
 bool
 BaselineCompiler::emit_JSOP_TOASYNCITER()
 {
     frame.syncStack(0);
-    masm.unboxObject(frame.addressOfStackValue(frame.peek(-1)), R0.scratchReg());
+    masm.unboxObject(frame.addressOfStackValue(frame.peek(-2)), R0.scratchReg());
+    masm.loadValue(frame.addressOfStackValue(frame.peek(-1)), R1);
 
     prepareVMCall();
+    pushArg(R1);
     pushArg(R0.scratchReg());
 
     if (!callVM(ToAsyncIterInfo))
         return false;
 
     masm.tagValue(JSVAL_TYPE_OBJECT, ReturnReg, R0);
-    frame.pop();
+    frame.popn(2);
     frame.push(R0);
     return true;
 }
 
 typedef bool (*ThrowObjectCoercibleFn)(JSContext*, HandleValue);
 static const VMFunction ThrowObjectCoercibleInfo =
     FunctionInfo<ThrowObjectCoercibleFn>(ThrowObjectCoercible, "ThrowObjectCoercible");
 
diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -10746,24 +10746,25 @@ static const VMFunction ToAsyncGenInfo =
 
 void
 CodeGenerator::visitToAsyncGen(LToAsyncGen* lir)
 {
     pushArg(ToRegister(lir->unwrapped()));
     callVM(ToAsyncGenInfo, lir);
 }
 
-typedef JSObject* (*ToAsyncIterFn)(JSContext*, HandleObject);
+typedef JSObject* (*ToAsyncIterFn)(JSContext*, HandleObject, HandleValue);
 static const VMFunction ToAsyncIterInfo =
     FunctionInfo<ToAsyncIterFn>(js::CreateAsyncFromSyncIterator, "ToAsyncIter");
 
 void
 CodeGenerator::visitToAsyncIter(LToAsyncIter* lir)
 {
-    pushArg(ToRegister(lir->unwrapped()));
+    pushArg(ToValue(lir, LToAsyncIter::NextMethodIndex));
+    pushArg(ToRegister(lir->iterator()));
     callVM(ToAsyncIterInfo, lir);
 }
 
 typedef bool (*ToIdFn)(JSContext*, HandleValue, MutableHandleValue);
 static const VMFunction ToIdInfo = FunctionInfo<ToIdFn>(ToIdOperation, "ToIdOperation");
 
 void
 CodeGenerator::visitToIdV(LToIdV* lir)
diff --git a/js/src/jit/IonBuilder.cpp b/js/src/jit/IonBuilder.cpp
--- a/js/src/jit/IonBuilder.cpp
+++ b/js/src/jit/IonBuilder.cpp
@@ -12601,20 +12601,21 @@ IonBuilder::jsop_toasyncgen()
     current->push(ins);
 
     return resumeAfter(ins);
 }
 
 AbortReasonOr<Ok>
 IonBuilder::jsop_toasynciter()
 {
-    MDefinition* unwrapped = current->pop();
-    MOZ_ASSERT(unwrapped->type() == MIRType::Object);
-
-    MToAsyncIter* ins = MToAsyncIter::New(alloc(), unwrapped);
+    MDefinition* nextMethod = current->pop();
+    MDefinition* iterator = current->pop();
+    MOZ_ASSERT(iterator->type() == MIRType::Object);
+
+    MToAsyncIter* ins = MToAsyncIter::New(alloc(), iterator, nextMethod);
 
     current->add(ins);
     current->push(ins);
 
     return resumeAfter(ins);
 }
 
 AbortReasonOr<Ok>
diff --git a/js/src/jit/Lowering.cpp b/js/src/jit/Lowering.cpp
--- a/js/src/jit/Lowering.cpp
+++ b/js/src/jit/Lowering.cpp
@@ -1217,17 +1217,18 @@ LIRGenerator::visitToAsyncGen(MToAsyncGe
     LToAsyncGen* lir = new(alloc()) LToAsyncGen(useRegisterAtStart(ins->input()));
     defineReturn(lir, ins);
     assignSafepoint(lir, ins);
 }
 
 void
 LIRGenerator::visitToAsyncIter(MToAsyncIter* ins)
 {
-    LToAsyncIter* lir = new(alloc()) LToAsyncIter(useRegisterAtStart(ins->input()));
+    LToAsyncIter* lir = new(alloc()) LToAsyncIter(useRegisterAtStart(ins->getIterator()),
+                                                  useBoxAtStart(ins->getNextMethod()));
     defineReturn(lir, ins);
     assignSafepoint(lir, ins);
 }
 
 void
 LIRGenerator::visitToId(MToId* ins)
 {
     LToIdV* lir = new(alloc()) LToIdV(useBox(ins->input()), tempDouble());
diff --git a/js/src/jit/MIR.h b/js/src/jit/MIR.h
--- a/js/src/jit/MIR.h
+++ b/js/src/jit/MIR.h
@@ -6006,28 +6006,29 @@ class MToAsyncGen
     }
 
   public:
     INSTRUCTION_HEADER(ToAsyncGen)
     TRIVIAL_NEW_WRAPPERS
 };
 
 class MToAsyncIter
-  : public MUnaryInstruction,
-    public SingleObjectPolicy::Data
-{
-    explicit MToAsyncIter(MDefinition* unwrapped)
-      : MUnaryInstruction(classOpcode, unwrapped)
+  : public MBinaryInstruction,
+    public MixPolicy<ObjectPolicy<0>, BoxPolicy<1>>::Data
+{
+    explicit MToAsyncIter(MDefinition* iterator, MDefinition* nextMethod)
+      : MBinaryInstruction(classOpcode, iterator, nextMethod)
     {
         setResultType(MIRType::Object);
     }
 
   public:
     INSTRUCTION_HEADER(ToAsyncIter)
     TRIVIAL_NEW_WRAPPERS
+    NAMED_OPERANDS((0, getIterator), (1, getNextMethod))
 };
 
 class MToId
   : public MUnaryInstruction,
     public BoxInputsPolicy::Data
 {
     explicit MToId(MDefinition* index)
       : MUnaryInstruction(classOpcode, index)
diff --git a/js/src/jit/shared/LIR-shared.h b/js/src/jit/shared/LIR-shared.h
--- a/js/src/jit/shared/LIR-shared.h
+++ b/js/src/jit/shared/LIR-shared.h
@@ -1619,25 +1619,28 @@ class LToAsyncGen : public LCallInstruct
         setOperand(0, input);
     }
 
     const LAllocation* unwrapped() {
         return getOperand(0);
     }
 };
 
-class LToAsyncIter : public LCallInstructionHelper<1, 1, 0>
+class LToAsyncIter : public LCallInstructionHelper<1, 1 + BOX_PIECES, 0>
 {
   public:
     LIR_HEADER(ToAsyncIter)
-    explicit LToAsyncIter(const LAllocation& input) {
-        setOperand(0, input);
-    }
-
-    const LAllocation* unwrapped() {
+    explicit LToAsyncIter(const LAllocation& iterator, const LBoxAllocation& nextMethod) {
+        setOperand(0, iterator);
+        setBoxOperand(NextMethodIndex, nextMethod);
+    }
+
+    static const size_t NextMethodIndex = 1;
+
+    const LAllocation* iterator() {
         return getOperand(0);
     }
 };
 
 class LToIdV : public LInstructionHelper<BOX_PIECES, BOX_PIECES, 1>
 {
   public:
     LIR_HEADER(ToIdV)
diff --git a/js/src/vm/AsyncIteration.cpp b/js/src/vm/AsyncIteration.cpp
--- a/js/src/vm/AsyncIteration.cpp
+++ b/js/src/vm/AsyncIteration.cpp
@@ -163,44 +163,48 @@ js::AsyncGeneratorYieldReturnAwaitedReje
 
 const Class AsyncFromSyncIteratorObject::class_ = {
     "AsyncFromSyncIteratorObject",
     JSCLASS_HAS_RESERVED_SLOTS(AsyncFromSyncIteratorObject::Slots)
 };
 
 // Async Iteration proposal 11.1.3.1.
 JSObject*
-js::CreateAsyncFromSyncIterator(JSContext* cx, HandleObject iter)
+js::CreateAsyncFromSyncIterator(JSContext* cx, HandleObject iter, HandleValue nextMethod)
 {
     // Step 1 (implicit).
     // Done in bytecode emitted by emitAsyncIterator.
 
     // Steps 2-4.
-    return AsyncFromSyncIteratorObject::create(cx, iter);
+    return AsyncFromSyncIteratorObject::create(cx, iter, nextMethod);
 }
 
 // Async Iteration proposal 11.1.3.1 steps 2-4.
 /* static */ JSObject*
-AsyncFromSyncIteratorObject::create(JSContext* cx, HandleObject iter)
+AsyncFromSyncIteratorObject::create(JSContext* cx, HandleObject iter, HandleValue nextMethod)
 {
     // Step 2.
     RootedObject proto(cx, GlobalObject::getOrCreateAsyncFromSyncIteratorPrototype(cx,
                                                                                    cx->global()));
     if (!proto)
         return nullptr;
 
     RootedObject obj(cx, NewNativeObjectWithGivenProto(cx, &class_, proto));
     if (!obj)
         return nullptr;
 
     Handle<AsyncFromSyncIteratorObject*> asyncIter = obj.as<AsyncFromSyncIteratorObject>();
 
     // Step 3.
     asyncIter->setIterator(iter);
 
+    // Spec update pending:
+    // https://github.com/tc39/proposal-async-iteration/issues/116
+    asyncIter->setNextMethod(nextMethod);
+
     // Step 4.
     return asyncIter;
 }
 
 // Async Iteration proposal 11.1.3.2.1 %AsyncFromSyncIteratorPrototype%.next.
 static bool
 AsyncFromSyncIteratorNext(JSContext* cx, unsigned argc, Value* vp)
 {
diff --git a/js/src/vm/AsyncIteration.h b/js/src/vm/AsyncIteration.h
--- a/js/src/vm/AsyncIteration.h
+++ b/js/src/vm/AsyncIteration.h
@@ -257,39 +257,48 @@ class AsyncGeneratorObject : public Nati
     }
 
     void clearCachedRequest() {
         setFixedSlot(Slot_CachedRequest, NullValue());
     }
 };
 
 JSObject*
-CreateAsyncFromSyncIterator(JSContext* cx, HandleObject iter);
+CreateAsyncFromSyncIterator(JSContext* cx, HandleObject iter, HandleValue nextMethod);
 
 class AsyncFromSyncIteratorObject : public NativeObject
 {
   private:
     enum AsyncFromSyncIteratorObjectSlots {
         Slot_Iterator = 0,
+        Slot_NextMethod = 1,
         Slots
     };
 
-    void setIterator(HandleObject iterator_) {
-        setFixedSlot(Slot_Iterator, ObjectValue(*iterator_));
+    void setIterator(HandleObject iterator) {
+        setFixedSlot(Slot_Iterator, ObjectValue(*iterator));
+    }
+
+    void setNextMethod(HandleValue nextMethod) {
+        setFixedSlot(Slot_NextMethod, nextMethod);
     }
 
   public:
     static const Class class_;
 
     static JSObject*
-    create(JSContext* cx, HandleObject iter);
+    create(JSContext* cx, HandleObject iter, HandleValue nextMethod);
 
     JSObject* iterator() const {
         return &getFixedSlot(Slot_Iterator).toObject();
     }
+
+    const Value& nextMethod() const {
+        return getFixedSlot(Slot_NextMethod);
+    }
 };
 
 MOZ_MUST_USE bool
 AsyncGeneratorResume(JSContext* cx, Handle<AsyncGeneratorObject*> asyncGenObj,
                      CompletionKind completionKind, HandleValue argument);
 
 } // namespace js
 
diff --git a/js/src/vm/Interpreter.cpp b/js/src/vm/Interpreter.cpp
--- a/js/src/vm/Interpreter.cpp
+++ b/js/src/vm/Interpreter.cpp
@@ -3688,21 +3688,23 @@ CASE(JSOP_TOASYNCGEN)
         goto error;
 
     REGS.sp[-1].setObject(*wrapped);
 }
 END_CASE(JSOP_TOASYNCGEN)
 
 CASE(JSOP_TOASYNCITER)
 {
-    ReservedRooted<JSObject*> iter(&rootObject1, &REGS.sp[-1].toObject());
-    JSObject* asyncIter = CreateAsyncFromSyncIterator(cx, iter);
+    ReservedRooted<Value> nextMethod(&rootValue0, REGS.sp[-1]);
+    ReservedRooted<JSObject*> iter(&rootObject1, &REGS.sp[-2].toObject());
+    JSObject* asyncIter = CreateAsyncFromSyncIterator(cx, iter, nextMethod);
     if (!asyncIter)
         goto error;
 
+    REGS.sp--;
     REGS.sp[-1].setObject(*asyncIter);
 }
 END_CASE(JSOP_TOASYNCITER)
 
 CASE(JSOP_SETFUNNAME)
 {
     MOZ_ASSERT(REGS.stackDepth() >= 2);
     FunctionPrefixKind prefixKind = FunctionPrefixKind(GET_UINT8(REGS.pc));
diff --git a/js/src/vm/Opcodes.h b/js/src/vm/Opcodes.h
--- a/js/src/vm/Opcodes.h
+++ b/js/src/vm/Opcodes.h
@@ -2142,24 +2142,25 @@ 1234567890123456789012345678901234567890
      * and returns 'promise'. Pushes resolved value onto the stack.
      *   Category: Statements
      *   Type: Generator
      *   Operands: uint24_t yieldAndAwaitIndex
      *   Stack: promise, gen => resolved
      */ \
     macro(JSOP_AWAIT,         209, "await",        NULL,  4,  2,  1,  JOF_UINT24) \
     /*
-     * Pops the iterator from the top of the stack, and create async iterator
-     * from it and push the async iterator back onto the stack.
+     * Pops the iterator and its next method from the top of the stack, and
+     * create async iterator from it and push the async iterator back onto the
+     * stack.
      *   Category: Statements
      *   Type: Generator
      *   Operands:
-     *   Stack: iter => asynciter
+     *   Stack: iter, next => asynciter
      */ \
-    macro(JSOP_TOASYNCITER,   210, "toasynciter",  NULL,  1,  1,  1,  JOF_BYTE) \
+    macro(JSOP_TOASYNCITER,   210, "toasynciter",  NULL,  1,  2,  1,  JOF_BYTE) \
     /*
      * Pops the top two values 'id' and 'obj' from the stack, then pushes
      * obj.hasOwnProperty(id)
      *
      * Note that 'obj' is the top value.
      *   Category: Other
      *   Type:
      *   Operands:
