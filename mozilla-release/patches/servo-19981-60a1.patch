# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1518025338 18000
# Node ID c93f28ff0d54c3ddbca9ba381cc039de1754b008
# Parent  9b3da1d3211a0f39370ab061fa0d718eeef97f15
servo: Merge #19981 - style: Derive ToCss for FontFamilyList (from emilio:nicer); r=nox

The extra reference in to_css is needed because the family list iterator returns
by value in Gecko.

Source-Repo: https://github.com/servo/servo
Source-Revision: 10552c23fc6c81b37bcfcba8f35430fc8d6a0ea8

diff --git a/servo/components/style/values/specified/font.rs b/servo/components/style/values/specified/font.rs
--- a/servo/components/style/values/specified/font.rs
+++ b/servo/components/style/values/specified/font.rs
@@ -160,19 +160,20 @@ impl ToCss for FontSize {
 
 impl From<LengthOrPercentage> for FontSize {
     fn from(other: LengthOrPercentage) -> Self {
         FontSize::Length(other)
     }
 }
 
 /// Specifies a prioritized list of font family names or generic family names.
-#[derive(Clone, Debug, Eq, Hash, PartialEq)]
+#[derive(Clone, Debug, Eq, Hash, PartialEq, ToCss)]
 pub enum FontFamily {
     /// List of `font-family`
+    #[css(iterable, comma)]
     Values(FontFamilyList),
     /// System font
     System(SystemFont),
 }
 
 impl FontFamily {
     /// Get `font-family` with system font
     pub fn system_font(f: SystemFont) -> Self {
@@ -240,36 +241,16 @@ impl MallocSizeOf for FontFamily {
                     )
                 }
             }
             FontFamily::System(_) => 0,
         }
     }
 }
 
-impl ToCss for FontFamily {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        match *self {
-            FontFamily::Values(ref v) => {
-                let mut iter = v.iter();
-                iter.next().unwrap().to_css(dest)?;
-                for family in iter {
-                    dest.write_str(", ")?;
-                    family.to_css(dest)?;
-                }
-                Ok(())
-            }
-            FontFamily::System(sys) => sys.to_css(dest),
-        }
-    }
-}
-
 impl Parse for FontFamily {
     /// <family-name>#
     /// <family-name> = <string> | [ <ident>+ ]
     /// TODO: <generic-family>
     fn parse<'i, 't>(
         _: &ParserContext,
         input: &mut Parser<'i, 't>
     ) -> Result<FontFamily, ParseError<'i>> {
diff --git a/servo/components/style_derive/to_css.rs b/servo/components/style_derive/to_css.rs
--- a/servo/components/style_derive/to_css.rs
+++ b/servo/components/style_derive/to_css.rs
@@ -39,17 +39,17 @@ pub fn derive(input: DeriveInput) -> Tok
             let mut expr = quote! {};
             if variant_attrs.iterable {
                 assert_eq!(bindings.len(), 1);
                 let binding = &bindings[0];
                 expr = quote! {
                     #expr
 
                     for item in #binding.iter() {
-                        writer.item(item)?;
+                        writer.item(&item)?;
                     }
                 };
             } else {
                 for binding in bindings {
                     let attrs = cg::parse_field_attrs::<CssFieldAttrs>(&binding.field);
                     if !attrs.ignore_bound {
                         where_clause.add_trait_bound(&binding.field.ty);
                     }
