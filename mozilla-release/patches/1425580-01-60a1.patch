# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1517907904 -3600
#      Tue Feb 06 10:05:04 2018 +0100
# Node ID ac0a43bf81cf7633f5ce68c1d9b931d372217312
# Parent  87368f8020c40b9e8d19ad3a15150863a6e102d9
Bug 1425580 part 1 - Devirtualize LNode::isCall. r=nbp

diff --git a/js/src/jit/LIR.h b/js/src/jit/LIR.h
--- a/js/src/jit/LIR.h
+++ b/js/src/jit/LIR.h
@@ -651,27 +651,33 @@ using LInt64Definition = LInt64Value<LDe
 class LSnapshot;
 class LSafepoint;
 class LInstruction;
 class LElementVisitor;
 
 // The common base class for LPhi and LInstruction.
 class LNode
 {
-    uint32_t id_;
-    LBlock* block_;
-
   protected:
     MDefinition* mir_;
 
+  private:
+    LBlock* block_;
+    uint32_t id_;
+
+  protected:
+    // Bitfields below are all uint32_t to make sure MSVC packs them correctly.
+    uint32_t isCall_ : 1;
+
   public:
     LNode()
-      : id_(0),
+      : mir_(nullptr),
         block_(nullptr),
-        mir_(nullptr)
+        id_(0),
+        isCall_(false)
     { }
 
     enum Opcode {
 #   define LIROP(name) LOp_##name,
         LIR_OPCODE_LIST(LIROP)
 #   undef LIROP
         LOp_Invalid
     };
@@ -720,18 +726,18 @@ class LNode
     virtual void setTemp(size_t index, const LDefinition& a) = 0;
 
     // Returns the number of successors of this instruction, if it is a control
     // transfer instruction, or zero otherwise.
     virtual size_t numSuccessors() const = 0;
     virtual MBasicBlock* getSuccessor(size_t i) const = 0;
     virtual void setSuccessor(size_t i, MBasicBlock* successor) = 0;
 
-    virtual bool isCall() const {
-        return false;
+    bool isCall() const {
+        return isCall_;
     }
 
     // Does this call preserve the given register?
     // By default, it is assumed that all registers are clobbered by a call.
     virtual bool isCallPreserved(AnyRegister reg) const {
         return false;
     }
 
@@ -813,16 +819,20 @@ class LInstruction
     LInstruction()
       : snapshot_(nullptr),
         safepoint_(nullptr),
         inputMoves_(nullptr),
         fixReuseMoves_(nullptr),
         movesAfter_(nullptr)
     { }
 
+    void setIsCall() {
+        isCall_ = true;
+    }
+
   public:
     LSnapshot* snapshot() const {
         return snapshot_;
     }
     LSafepoint* safepoint() const {
         return safepoint_;
     }
     LMoveGroup* inputMoves() const {
@@ -1186,18 +1196,18 @@ class LVariadicInstruction : public deta
 #endif
     }
 };
 
 template <size_t Defs, size_t Operands, size_t Temps>
 class LCallInstructionHelper : public LInstructionHelper<Defs, Operands, Temps>
 {
   public:
-    virtual bool isCall() const override {
-        return true;
+    LCallInstructionHelper() {
+        this->setIsCall();
     }
 };
 
 template <size_t Defs, size_t Temps>
 class LBinaryCallInstructionHelper : public LCallInstructionHelper<Defs, 2, Temps>
 {
   public:
     const LAllocation* lhs() {
diff --git a/js/src/jit/shared/LIR-shared.h b/js/src/jit/shared/LIR-shared.h
--- a/js/src/jit/shared/LIR-shared.h
+++ b/js/src/jit/shared/LIR-shared.h
@@ -4059,23 +4059,21 @@ class LModD : public LBinaryMath<1>
 {
   public:
     LIR_HEADER(ModD)
 
     LModD(const LAllocation& lhs, const LAllocation& rhs, const LDefinition& temp) {
         setOperand(0, lhs);
         setOperand(1, rhs);
         setTemp(0, temp);
+        setIsCall();
     }
     const LDefinition* temp() {
         return getTemp(0);
     }
-    bool isCall() const override {
-        return true;
-    }
     MMod* mir() const {
         return mir_->toMod();
     }
 };
 
 // Call a VM function to perform a binary operation.
 class LBinaryV : public LCallInstructionHelper<BOX_PIECES, 2 * BOX_PIECES, 0>
 {
@@ -9005,25 +9003,24 @@ class LWasmCallBase : public LInstructio
     uint32_t needsBoundsCheck_;
 
   public:
 
     LWasmCallBase(LAllocation* operands, uint32_t numOperands, bool needsBoundsCheck)
       : operands_(operands),
         numOperands_(numOperands),
         needsBoundsCheck_(needsBoundsCheck)
-    {}
+    {
+        setIsCall();
+    }
 
     MWasmCall* mir() const {
         return mir_->toWasmCall();
     }
 
-    bool isCall() const override {
-        return true;
-    }
     bool isCallPreserved(AnyRegister reg) const override {
         // All MWasmCalls preserve the TLS register:
         //  - internal/indirect calls do by the internal wasm ABI
         //  - import calls do by explicitly saving/restoring at the callsite
         //  - builtin calls do because the TLS reg is non-volatile
         // See also CodeGeneratorShared::emitWasmCallBase.
         return !reg.isFloat() && reg.gpr() == WasmTlsReg;
     }
