# HG changeset patch
# User Steve Fink <sfink@mozilla.com>
# Date 1630609618 0
# Node ID c250dd684d8e7e770d562bbb2c7bfac464dc6593
# Parent  f80398046f9071e6faa6c92869052413d83295e2
Bug 1727374 - Root Init dictionaries to fix rooting hazards. r=peterv, a=RyanVM

Differential Revision: https://phabricator.services.mozilla.com/D123633

diff --git a/dom/cache/Cache.cpp b/dom/cache/Cache.cpp
--- a/dom/cache/Cache.cpp
+++ b/dom/cache/Cache.cpp
@@ -348,18 +348,19 @@ Cache::Add(JSContext* aContext, const Re
   if (!IsValidPutRequestMethod(aRequest, aRv)) {
     return nullptr;
   }
 
   GlobalObject global(aContext, mGlobal->GetGlobalJSObject());
   MOZ_DIAGNOSTIC_ASSERT(!global.Failed());
 
   nsTArray<RefPtr<Request>> requestList(1);
-  RefPtr<Request> request = Request::Constructor(global, aRequest,
-                                                   RequestInit(), aRv);
+  RootedDictionary<RequestInit> requestInit(aContext);
+  RefPtr<Request> request =
+      Request::Constructor(global, aRequest, requestInit, aRv);
   if (NS_WARN_IF(aRv.Failed())) {
     return nullptr;
   }
 
   nsAutoString url;
   request->GetUrl(url);
   if (NS_WARN_IF(!IsValidPutRequestURL(url, aRv))) {
     return nullptr;
@@ -396,18 +397,19 @@ Cache::AddAll(JSContext* aContext,
         return nullptr;
       }
     } else {
       requestOrString.SetAsUSVString().Rebind(
         aRequestList[i].GetAsUSVString().Data(),
         aRequestList[i].GetAsUSVString().Length());
     }
 
-    RefPtr<Request> request = Request::Constructor(global, requestOrString,
-                                                     RequestInit(), aRv);
+    RootedDictionary<RequestInit> requestInit(aContext);
+    RefPtr<Request> request =
+        Request::Constructor(global, requestOrString, requestInit, aRv);
     if (NS_WARN_IF(aRv.Failed())) {
       return nullptr;
     }
 
     nsAutoString url;
     request->GetUrl(url);
     if (NS_WARN_IF(!IsValidPutRequestURL(url, aRv))) {
       return nullptr;
@@ -609,18 +611,19 @@ Cache::AddAll(const GlobalObject& aGloba
 
   // Begin fetching each request in parallel.  For now, if an error occurs just
   // abandon our previous fetch calls.  In theory we could cancel them in the
   // future once fetch supports it.
 
   for (uint32_t i = 0; i < aRequestList.Length(); ++i) {
     RequestOrUSVString requestOrString;
     requestOrString.SetAsRequest() = aRequestList[i];
-    RefPtr<Promise> fetch = FetchRequest(mGlobal, requestOrString,
-                                         RequestInit(), aCallerType, aRv);
+    RootedDictionary<RequestInit> requestInit(aGlobal.Context());
+    RefPtr<Promise> fetch =
+        FetchRequest(mGlobal, requestOrString, requestInit, aCallerType, aRv);
     if (NS_WARN_IF(aRv.Failed())) {
       return nullptr;
     }
 
     fetchList.AppendElement(std::move(fetch));
   }
 
   RefPtr<Promise> promise = Promise::Create(mGlobal, aRv);
diff --git a/dom/cache/TypeUtils.cpp b/dom/cache/TypeUtils.cpp
--- a/dom/cache/TypeUtils.cpp
+++ b/dom/cache/TypeUtils.cpp
@@ -472,18 +472,19 @@ TypeUtils::ToInternalRequest(const nsASt
   if (NS_WARN_IF(!jsapi.Init(GetGlobalObject()))) {
     aRv.Throw(NS_ERROR_UNEXPECTED);
     return nullptr;
   }
   JSContext* cx = jsapi.cx();
   GlobalObject global(cx, GetGlobalObject()->GetGlobalJSObject());
   MOZ_DIAGNOSTIC_ASSERT(!global.Failed());
 
-  RefPtr<Request> request = Request::Constructor(global, requestOrString,
-                                                   RequestInit(), aRv);
+  RootedDictionary<RequestInit> requestInit(cx);
+  RefPtr<Request> request =
+      Request::Constructor(global, requestOrString, requestInit, aRv);
   if (NS_WARN_IF(aRv.Failed())) { return nullptr; }
 
   return request->GetInternalRequest();
 }
 
 void
 TypeUtils::SerializeCacheStream(nsIInputStream* aStream,
                                 CacheReadStreamOrVoid* aStreamOut,
diff --git a/dom/push/PushManager.cpp b/dom/push/PushManager.cpp
--- a/dom/push/PushManager.cpp
+++ b/dom/push/PushManager.cpp
@@ -551,17 +551,17 @@ PushManager::PermissionState(const PushS
 
   return p.forget();
 }
 
 already_AddRefed<Promise>
 PushManager::PerformSubscriptionActionFromWorker(SubscriptionAction aAction,
                                                  ErrorResult& aRv)
 {
-  PushSubscriptionOptionsInit options;
+  RootedDictionary<PushSubscriptionOptionsInit> options(RootingCx());
   return PerformSubscriptionActionFromWorker(aAction, options, aRv);
 }
 
 already_AddRefed<Promise>
 PushManager::PerformSubscriptionActionFromWorker(SubscriptionAction aAction,
                                                  const PushSubscriptionOptionsInit& aOptions,
                                                  ErrorResult& aRv)
 {
diff --git a/dom/reporting/ReportDeliver.cpp.1727374.later b/dom/reporting/ReportDeliver.cpp.1727374.later
new file mode 100644
--- /dev/null
+++ b/dom/reporting/ReportDeliver.cpp.1727374.later
@@ -0,0 +1,24 @@
+--- ReportDeliver.cpp
++++ ReportDeliver.cpp
+@@ -216,18 +216,19 @@ void SendReports(nsTArray<ReportDeliver:
+   internalRequest->SetCredentialsMode(RequestCredentials::Include);
+ 
+   RefPtr<Request> request =
+       new Request(globalObject, std::move(internalRequest), nullptr);
+ 
+   RequestOrUSVString fetchInput;
+   fetchInput.SetAsRequest() = request;
+ 
+-  RefPtr<Promise> promise = FetchRequest(
+-      globalObject, fetchInput, RequestInit(), CallerType::NonSystem, error);
++  RootedDictionary<RequestInit> requestInit(RootingCx());
++  RefPtr<Promise> promise = FetchRequest(globalObject, fetchInput, requestInit,
++                                         CallerType::NonSystem, error);
+   if (error.Failed()) {
+     for (auto& report : aReports) {
+       ++report.mFailures;
+       if (gReportDeliver) {
+         gReportDeliver->AppendReportData(report);
+       }
+     }
+     return;
diff --git a/dom/serviceworkers/ServiceWorkerOp.cpp.1727374.later b/dom/serviceworkers/ServiceWorkerOp.cpp.1727374.later
new file mode 100644
--- /dev/null
+++ b/dom/serviceworkers/ServiceWorkerOp.cpp.1727374.later
@@ -0,0 +1,21 @@
+--- ServiceWorkerOp.cpp
++++ ServiceWorkerOp.cpp
+@@ -648,17 +648,17 @@ class PushEventOp final : public Extenda
+ 
+       RejectAll(result.StealNSResult());
+       ReportError(aWorkerPrivate);
+     });
+ 
+     const ServiceWorkerPushEventOpArgs& args =
+         mArgs.get_ServiceWorkerPushEventOpArgs();
+ 
+-    PushEventInit pushEventInit;
++    RootedDictionary<PushEventInit> pushEventInit(aCx);
+ 
+     if (args.data().type() != OptionalPushData::Tvoid_t) {
+       auto& bytes = args.data().get_ArrayOfuint8_t();
+       JSObject* data =
+           Uint8Array::Create(aCx, bytes.Length(), bytes.Elements());
+ 
+       if (!data) {
+         result = ErrorResult(NS_ERROR_FAILURE);
diff --git a/dom/serviceworkers/ServiceWorkerPrivate.cpp b/dom/serviceworkers/ServiceWorkerPrivate.cpp
--- a/dom/serviceworkers/ServiceWorkerPrivate.cpp
+++ b/dom/serviceworkers/ServiceWorkerPrivate.cpp
@@ -935,17 +935,17 @@ public:
   WorkerRun(JSContext* aCx, WorkerPrivate* aWorkerPrivate) override
   {
     MOZ_ASSERT(aWorkerPrivate);
     GlobalObject globalObj(aCx, aWorkerPrivate->GlobalScope()->GetWrapper());
 
     RefPtr<PushErrorReporter> errorReporter =
       new PushErrorReporter(aWorkerPrivate, mMessageId);
 
-    PushEventInit pei;
+    RootedDictionary<PushEventInit> pei(aCx);
     if (mData) {
       const nsTArray<uint8_t>& bytes = mData.ref();
       JSObject* data = Uint8Array::Create(aCx, bytes.Length(), bytes.Elements());
       if (!data) {
         errorReporter->Report();
         return false;
       }
       pei.mData.Construct().SetAsArrayBufferView().Init(data);
diff --git a/dom/worklet/Worklet.cpp.1727374.later b/dom/worklet/Worklet.cpp.1727374.later
new file mode 100644
--- /dev/null
+++ b/dom/worklet/Worklet.cpp.1727374.later
@@ -0,0 +1,21 @@
+--- Worklet.cpp
++++ Worklet.cpp
+@@ -121,17 +121,17 @@ class WorkletFetchHandler final : public
+         handler->AddPromise(promise);
+         return promise.forget();
+       }
+     }
+ 
+     RequestOrUSVString requestInput;
+     requestInput.SetAsUSVString().ShareOrDependUpon(aModuleURL);
+ 
+-    RequestInit requestInit;
++    RootedDictionary<RequestInit> requestInit(aCx);
+     requestInit.mCredentials.Construct(aOptions.mCredentials);
+ 
+     SafeRefPtr<Request> request =
+         Request::Constructor(global, aCx, requestInput, requestInit, aRv);
+     if (aRv.Failed()) {
+       return nullptr;
+     }
+ 
diff --git a/js/src/wasm/WasmJS.cpp b/js/src/wasm/WasmJS.cpp
--- a/js/src/wasm/WasmJS.cpp
+++ b/js/src/wasm/WasmJS.cpp
@@ -988,27 +988,31 @@ WasmInstanceObject::create(JSContext* cx
                            UniqueDebugState debug,
                            UniqueTlsData tlsData,
                            HandleWasmMemoryObject memory,
                            SharedTableVector&& tables,
                            Handle<FunctionVector> funcImports,
                            const ValVector& globalImports,
                            HandleObject proto)
 {
-    UniquePtr<ExportMap> exports = js::MakeUnique<ExportMap>();
+    Rooted<UniquePtr<ExportMap>> exports(cx,
+                                         js::MakeUnique<ExportMap>());
+
     if (!exports || !exports->init()) {
         ReportOutOfMemory(cx);
         return nullptr;
     }
 
     UniquePtr<ScopeMap> scopes = js::MakeUnique<ScopeMap>(cx->zone());
     if (!scopes || !scopes->init()) {
         ReportOutOfMemory(cx);
         return nullptr;
     }
+    // Note that `scopes` is a WeakCache, auto-linked into a sweep list on the
+    // Zone, and so does not require rooting.
 
     AutoSetNewObjectMetadata metadata(cx);
     RootedWasmInstanceObject obj(cx, NewObjectWithGivenProto<WasmInstanceObject>(cx, proto));
     if (!obj)
         return nullptr;
 
     obj->setReservedSlot(EXPORTS_SLOT, PrivateValue(exports.release()));
     obj->setReservedSlot(SCOPES_SLOT, PrivateValue(scopes.release()));
