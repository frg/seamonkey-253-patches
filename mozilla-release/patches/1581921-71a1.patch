# HG changeset patch
# User Dzmitry Malyshau <dmalyshau@mozilla.com>
# Date 1568819557 0
# Node ID 571c8fcde3e0bbf7c7bc1b86c21a76315024c9dd
# Parent  273eb2dbb0e3fc575bf3f4ee1fcff25e29ff39d5
Bug 1581921 - Support Solus Linux by mach bootstrap r=froydnj

Differential Revision: https://phabricator.services.mozilla.com/D46190

diff --git a/python/mozboot/mozboot/bootstrap.py b/python/mozboot/mozboot/bootstrap.py
--- a/python/mozboot/mozboot/bootstrap.py
+++ b/python/mozboot/mozboot/bootstrap.py
@@ -29,16 +29,17 @@ else:
 from mozboot.base import MODERN_RUST_VERSION
 from mozboot.centosfedora import CentOSFedoraBootstrapper
 from mozboot.debian import DebianBootstrapper
 from mozboot.freebsd import FreeBSDBootstrapper
 from mozboot.gentoo import GentooBootstrapper
 from mozboot.osx import OSXBootstrapper
 from mozboot.openbsd import OpenBSDBootstrapper
 from mozboot.archlinux import ArchlinuxBootstrapper
+from mozboot.solus import SolusBootstrapper
 from mozboot.windows import WindowsBootstrapper
 from mozboot.mozillabuild import MozillaBuildBootstrapper
 from mozboot.util import (
     get_state_dir,
 )
 
 APPLICATION_CHOICE = '''
 Note on Artifact Mode:
@@ -241,32 +242,45 @@ class Bootstrapper(object):
         self.no_system_changes = no_system_changes
         self.mach_context = mach_context
         self.vcs = vcs
         cls = None
         args = {'no_interactive': no_interactive,
                 'no_system_changes': no_system_changes}
 
         if sys.platform.startswith('linux'):
+            # TODO: don't call `linux_distribution` at all since it's deprecated
             distro, version, dist_id = platform.linux_distribution()
 
+            # Read the standard `os-release` configuration
+            if distro == '' and os.path.exists('/etc/os-release'):
+                d = {}
+                for line in open('/etc/os-release'):
+                    k, v = line.rstrip().split("=")
+                    d[k] = v.strip('"')
+                distro = d["NAME"]
+                version = d["VERSION_ID"]
+                dist_id = d["ID"]
+
             if distro in ('CentOS', 'CentOS Linux', 'Fedora'):
                 cls = CentOSFedoraBootstrapper
                 args['distro'] = distro
             elif distro in DEBIAN_DISTROS:
                 cls = DebianBootstrapper
                 args['distro'] = distro
             elif distro in ('Gentoo Base System', 'Funtoo Linux - baselayout '):
                 cls = GentooBootstrapper
+            elif distro in ('Solus'):
+                cls = SolusBootstrapper
             elif os.path.exists('/etc/arch-release'):
                 # Even on archlinux, platform.linux_distribution() returns ['','','']
                 cls = ArchlinuxBootstrapper
             else:
                 raise NotImplementedError('Bootstrap support for this Linux '
-                                          'distro not yet available.')
+                                          'distro not yet available: ' + distro)
 
             args['version'] = version
             args['dist_id'] = dist_id
 
         elif sys.platform.startswith('darwin'):
             # TODO Support Darwin platforms that aren't OS X.
             osx_version = platform.mac_ver()[0]
 
diff --git a/python/mozboot/mozboot/solus.py b/python/mozboot/mozboot/solus.py
new file mode 100644
--- /dev/null
+++ b/python/mozboot/mozboot/solus.py
@@ -0,0 +1,143 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+from __future__ import absolute_import, print_function, unicode_literals
+
+import sys
+import subprocess
+
+from mozboot.base import BaseBootstrapper
+from mozboot.linux_common import (
+    ClangStaticAnalysisInstall,
+    NodeInstall,
+    SccacheInstall,
+    StyloInstall,
+)
+
+# NOTE: This script is intended to be run with a vanilla Python install.  We
+# have to rely on the standard library instead of Python 2+3 helpers like
+# the six module.
+if sys.version_info < (3,):
+    input = raw_input
+
+
+class SolusBootstrapper(NodeInstall, StyloInstall, SccacheInstall,
+                        ClangStaticAnalysisInstall, BaseBootstrapper):
+    '''Solus experimental bootstrapper.'''
+
+    SYSTEM_PACKAGES = [
+        'autoconf213',
+        'nodejs',
+        'python',
+        'python3',
+        'unzip',
+        'zip',
+    ]
+    SYSTEM_COMPONENTS = [
+        'system.devel',
+    ]
+
+    BROWSER_PACKAGES = [
+        'alsa-lib',
+        'dbus',
+        'libgtk-2',
+        'libgtk-3',
+        'libevent',
+        'libvpx',
+        'libxt',
+        'nasm',
+        'libstartup-notification',
+        'gst-plugins-base',
+        'gst-plugins-good',
+        'pulseaudio',
+        'xorg-server-xvfb',
+        'yasm',
+    ]
+
+    MOBILE_ANDROID_COMMON_PACKAGES = [
+        'openjdk-8',
+        # For downloading the Android SDK and NDK.
+        'wget',
+        # See comment about 32 bit binaries and multilib below.
+        'ncurses-32bit',
+        'readline-32bit',
+        'zlib-32bit',
+    ]
+
+    def __init__(self, version, dist_id, **kwargs):
+        print('Using an experimental bootstrapper for Solus.')
+        BaseBootstrapper.__init__(self, **kwargs)
+
+    def install_system_packages(self):
+        self.package_install(*self.SYSTEM_PACKAGES)
+        self.component_install(*self.SYSTEM_COMPONENTS)
+
+    def install_browser_packages(self):
+        self.ensure_browser_packages()
+
+    def install_browser_artifact_mode_packages(self):
+        self.ensure_browser_packages(artifact_mode=True)
+
+    def install_mobile_android_packages(self):
+        self.ensure_mobile_android_packages()
+
+    def install_mobile_android_artifact_mode_packages(self):
+        self.ensure_mobile_android_packages(artifact_mode=True)
+
+    def ensure_browser_packages(self, artifact_mode=False):
+        self.package_install(*self.BROWSER_PACKAGES)
+
+    def ensure_nasm_packages(self, state_dir, checkout_root):
+        # installed via ensure_browser_packages
+        pass
+
+    def ensure_mobile_android_packages(self, artifact_mode=False):
+        try:
+            self.package_install(*self.MOBILE_ANDROID_COMMON_PACKAGES)
+        except Exception as e:
+            print('Failed to install all packages!')
+            raise e
+
+        # 2. Android pieces.
+        self.ensure_java()
+        from mozboot import android
+        android.ensure_android('linux', artifact_mode=artifact_mode,
+                               no_interactive=self.no_interactive)
+
+    def suggest_mobile_android_mozconfig(self, artifact_mode=False):
+        from mozboot import android
+        android.suggest_mozconfig('linux', artifact_mode=artifact_mode)
+
+    def suggest_mobile_android_artifact_mode_mozconfig(self):
+        self.suggest_mobile_android_mozconfig(artifact_mode=True)
+
+    def _update_package_manager(self):
+        pass
+
+    def upgrade_mercurial(self, current):
+        self.package_install('mercurial')
+
+    def upgrade_python(self, current):
+        self.package_install('python2')
+
+    def package_install(self, *packages):
+        command = ['eopkg', 'install']
+        if self.no_interactive:
+            command.append('--yes-all')
+
+        command.extend(packages)
+
+        self.run_as_root(command)
+
+    def component_install(self, *components):
+        command = ['eopkg', 'install', '-c']
+        if self.no_interactive:
+            command.append('--yes-all')
+
+        command.extend(components)
+
+        self.run_as_root(command)
+
+    def run(self, command, env=None):
+        subprocess.check_call(command, stdin=sys.stdin, env=env)

