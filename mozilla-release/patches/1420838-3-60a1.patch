# HG changeset patch
# User Dragan Mladjenovic <dragan.mladjenovic>
# Date 1513169805 -3600
#      Wed Dec 13 13:56:45 2017 +0100
# Node ID 0d5297d75f8ec5328bda101f01af50d7c31e4140
# Parent  c1c26c066dce7840f40bd08f540753a54030e365
Bug 1420838 : [MIPS64] Add atomic support into simulator; r=lth

diff --git a/js/src/jit/mips64/Simulator-mips64.cpp b/js/src/jit/mips64/Simulator-mips64.cpp
--- a/js/src/jit/mips64/Simulator-mips64.cpp
+++ b/js/src/jit/mips64/Simulator-mips64.cpp
@@ -32,16 +32,17 @@
 #include "mozilla/Casting.h"
 #include "mozilla/FloatingPoint.h"
 #include "mozilla/IntegerPrintfMacros.h"
 #include "mozilla/Likely.h"
 #include "mozilla/MathAlgorithms.h"
 
 #include <float.h>
 
+#include "jit/AtomicOperations.h"
 #include "jit/mips64/Assembler-mips64.h"
 #include "threading/LockGuard.h"
 #include "vm/Runtime.h"
 #include "wasm/WasmInstance.h"
 #include "wasm/WasmSignalHandlers.h"
 
 #define I8(v)   static_cast<int8_t>(v)
 #define I16(v)  static_cast<int16_t>(v)
@@ -460,25 +461,27 @@ SimInstruction::instructionType() const
       case op_lbu:
       case op_lh:
       case op_lhu:
       case op_lw:
       case op_lwu:
       case op_lwl:
       case op_lwr:
       case op_ll:
+      case op_lld:
       case op_ld:
       case op_ldl:
       case op_ldr:
       case op_sb:
       case op_sh:
       case op_sw:
       case op_swl:
       case op_swr:
       case op_sc:
+      case op_scd:
       case op_sd:
       case op_sdl:
       case op_sdr:
       case op_lwc1:
       case op_ldc1:
       case op_swc1:
       case op_sdc1:
         return kImmediateType;
@@ -1146,20 +1149,20 @@ Simulator::setLastDebuggerInput(char* in
 }
 
 static CachePage*
 GetCachePageLocked(SimulatorProcess::ICacheMap& i_cache, void* page)
 {
     SimulatorProcess::ICacheMap::AddPtr p = i_cache.lookupForAdd(page);
     if (p)
         return p->value();
-
+    AutoEnterOOMUnsafeRegion oomUnsafe;
     CachePage* new_page = js_new<CachePage>();
-    if (!i_cache.add(p, page, new_page))
-        return nullptr;
+    if (!new_page || !i_cache.add(p, page, new_page))
+         oomUnsafe.crash("Simulator CachePage");
     return new_page;
 }
 
 // Flush from start up to and not including start + size.
 static void
 FlushOnePageLocked(SimulatorProcess::ICacheMap& i_cache, intptr_t start, int size)
 {
     MOZ_ASSERT(size <= CachePage::kPageSize);
@@ -1278,16 +1281,19 @@ Simulator::Simulator()
 
     // Set up architecture state.
     // All registers are initialized to zero to start with.
     for (int i = 0; i < Register::kNumSimuRegisters; i++)
         registers_[i] = 0;
     for (int i = 0; i < Simulator::FPURegister::kNumFPURegisters; i++)
         FPUregisters_[i] = 0;
     FCSR_ = 0;
+    LLBit_ = false;
+    LLAddr_ = 0;
+    lastLLValue_ = 0;
 
     // The ra and pc are initialized to a known bad value that will cause an
     // access violation if the simulator ever tries to execute it.
     registers_[pc] = bad_ra;
     registers_[ra] = bad_ra;
 
     for (int i = 0; i < kNumExceptions; i++)
         exceptions[i] = 0;
@@ -1353,21 +1359,20 @@ class Redirection
         Redirection* current = SimulatorProcess::redirection();
         for (; current != nullptr; current = current->next_) {
             if (current->nativeFunction_ == nativeFunction) {
                 MOZ_ASSERT(current->type() == type);
                 return current;
             }
         }
 
+        AutoEnterOOMUnsafeRegion oomUnsafe;
         Redirection* redir = (Redirection*)js_malloc(sizeof(Redirection));
         if (!redir) {
-            MOZ_ReportAssertionFailure("[unhandlable oom] Simulator redirection",
-                                       __FILE__, __LINE__);
-            MOZ_CRASH();
+            oomUnsafe.crash("Simulator redirection");
         }
         new(redir) Redirection(nativeFunction, type);
         return redir;
     }
 
     static Redirection* FromSwiInstruction(SimInstruction* swiInstruction) {
         uint8_t* addrOfSwi = reinterpret_cast<uint8_t*>(swiInstruction);
         uint8_t* addrOfRedirection = addrOfSwi - offsetof(Redirection, swiInstruction_);
@@ -1610,23 +1615,93 @@ int64_t
 Simulator::get_pc() const
 {
     return registers_[pc];
 }
 
 void
 Simulator::startInterrupt(JitActivation* activation)
 {
-    MOZ_CRASH("NIY");
+    JS::ProfilingFrameIterator::RegisterState state;
+    state.pc = (void*) get_pc();
+    state.fp = (void*) getRegister(fp);
+    state.sp = (void*) getRegister(sp);
+    state.lr = (void*) getRegister(ra);
+    activation->startWasmInterrupt(state);
 }
 
+// The signal handler only redirects the PC to the interrupt stub when the PC is
+// in function code. However, this guard is racy for the simulator since the
+// signal handler samples PC in the middle of simulating an instruction and thus
+// the current PC may have advanced once since the signal handler's guard. So we
+// re-check here.
 void
 Simulator::handleWasmInterrupt()
 {
-    MOZ_CRASH("NIY");
+    if (!wasm::CodeExists)
+        return;
+
+    void* pc = (void*)get_pc();
+    void* fp = (void*)getRegister(Register::fp);
+
+    JitActivation* activation = TlsContext.get()->activation()->asJit();
+    const wasm::CodeSegment* segment = wasm::LookupCodeSegment(pc);
+    if (!segment || !segment->containsCodePC(pc))
+        return;
+
+    // fp can be null during the prologue/epilogue of the entry function.
+    if (!fp)
+        return;
+
+    startInterrupt(activation);
+    set_pc(int64_t(segment->interruptCode()));
+}
+
+// WebAssembly memories contain an extra region of guard pages (see
+// WasmArrayRawBuffer comment). The guard pages catch out-of-bounds accesses
+// using a signal handler that redirects PC to a stub that safely reports an
+// error. However, if the handler is hit by the simulator, the PC is in C++ code
+// and cannot be redirected. Therefore, we must avoid hitting the handler by
+// redirecting in the simulator before the real handler would have been hit.
+bool
+Simulator::handleWasmFault(uint64_t addr, unsigned numBytes)
+{
+    if (!wasm::CodeExists)
+        return false;
+
+    JSContext* cx = TlsContext.get();
+    if (!cx->activation() || !cx->activation()->isJit())
+        return false;
+    JitActivation* act = cx->activation()->asJit();
+
+    void* pc = reinterpret_cast<void*>(get_pc());
+    uint8_t* fp = reinterpret_cast<uint8_t*>(getRegister(Register::fp));
+
+    const wasm::CodeSegment* segment = wasm::LookupCodeSegment(pc);
+    if (!segment)
+        return false;
+
+    wasm::Instance* instance = wasm::LookupFaultingInstance(*segment, pc, fp);
+    if (!instance || !instance->memoryAccessInGuardRegion((uint8_t*)addr, numBytes))
+        return false;
+
+    LLBit_ = false;
+
+    const wasm::MemoryAccess* memoryAccess = instance->code().lookupMemoryAccess(pc);
+    if (!memoryAccess) {
+        startInterrupt(act);
+        if (!instance->code().containsCodePC(pc))
+            MOZ_CRASH("Cannot map PC to trap handler");
+        set_pc(int64_t(segment->outOfBoundsCode()));
+        return true;
+    }
+
+    MOZ_ASSERT(memoryAccess->hasTrapOutOfLineCode());
+    set_pc(int64_t(memoryAccess->trapOutOfLineCode(segment->base())));
+    return true;
 }
 
 bool
 Simulator::handleWasmTrapFault()
 {
     if (!wasm::CodeExists)
         return false;
 
@@ -1647,244 +1722,368 @@ Simulator::handleWasmTrapFault()
     if (!segment->code().lookupTrap(pc, &trap, &bytecode))
         return false;
 
     act->startWasmTrap(trap, bytecode.offset, pc, fp);
     set_pc(int64_t(segment->trapCode()));
     return true;
 }
 
-// The MIPS cannot do unaligned reads and writes.  On some MIPS platforms an
-// interrupt is caused.  On others it does a funky rotation thing.  For now we
-// simply disallow unaligned reads, but at some point we may want to move to
-// emulating the rotate behaviour.  Note that simulator runs have the runtime
-// system running directly on the host system and only generated code is
-// executed in the simulator.  Since the host is typically IA32 we will not
-// get the correct MIPS-like behaviour on unaligned accesses.
+// MIPS memory instructions (except lw(d)l/r , sw(d)l/r) trap on unaligned memory
+// access enabling the OS to handle them via trap-and-emulate.
+// Note that simulator runs have the runtime system running directly on the host
+// system and only generated code is executed in the simulator.
+// Since the host is typically IA32 it will not trap on unaligned memory access.
+// We assume that that executing correct generated code will not produce unaligned
+// memory access, so we explicitly check for address alignment and trap.
+// Note that trapping does not occur when executing wasm code, which requires that
+// unaligned memory access provides correct result.
 
 uint8_t
 Simulator::readBU(uint64_t addr, SimInstruction* instr)
 {
+    if (handleWasmFault(addr, 1))
+        return 0xff;
+
     uint8_t* ptr = reinterpret_cast<uint8_t*>(addr);
     return* ptr;
 }
 
 int8_t
 Simulator::readB(uint64_t addr, SimInstruction* instr)
 {
+    if (handleWasmFault(addr, 1))
+        return -1;
+
     int8_t* ptr = reinterpret_cast<int8_t*>(addr);
     return* ptr;
 }
 
 void
 Simulator::writeB(uint64_t addr, uint8_t value, SimInstruction* instr)
 {
+    if (handleWasmFault(addr, 1))
+        return;
+
     uint8_t* ptr = reinterpret_cast<uint8_t*>(addr);
     *ptr = value;
 }
 
 void
 Simulator::writeB(uint64_t addr, int8_t value, SimInstruction* instr)
 {
+    if (handleWasmFault(addr, 1))
+        return;
+
     int8_t* ptr = reinterpret_cast<int8_t*>(addr);
     *ptr = value;
 }
 
 uint16_t
 Simulator::readHU(uint64_t addr, SimInstruction* instr)
 {
-    if ((addr & 1) == 0) {
+    if (handleWasmFault(addr, 2))
+        return 0xffff;
+
+    if ((addr & 1) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         uint16_t* ptr = reinterpret_cast<uint16_t*>(addr);
         return *ptr;
     }
     printf("Unaligned unsigned halfword read at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
     return 0;
 }
 
 int16_t
 Simulator::readH(uint64_t addr, SimInstruction* instr)
 {
-    if ((addr & 1) == 0) {
+    if (handleWasmFault(addr, 2))
+        return -1;
+
+    if ((addr & 1) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         int16_t* ptr = reinterpret_cast<int16_t*>(addr);
         return *ptr;
     }
     printf("Unaligned signed halfword read at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
     return 0;
 }
 
 void
 Simulator::writeH(uint64_t addr, uint16_t value, SimInstruction* instr)
 {
-    if ((addr & 1) == 0) {
+    if (handleWasmFault(addr, 2))
+        return;
+
+    if ((addr & 1) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         uint16_t* ptr = reinterpret_cast<uint16_t*>(addr);
+        LLBit_ = false;
         *ptr = value;
         return;
     }
     printf("Unaligned unsigned halfword write at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
 }
 
 void
 Simulator::writeH(uint64_t addr, int16_t value, SimInstruction* instr)
 {
-    if ((addr & 1) == 0) {
+    if (handleWasmFault(addr, 2))
+        return;
+
+    if ((addr & 1) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         int16_t* ptr = reinterpret_cast<int16_t*>(addr);
+        LLBit_ = false;
         *ptr = value;
         return;
     }
     printf("Unaligned halfword write at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
 }
 
 uint32_t
 Simulator::readWU(uint64_t addr, SimInstruction* instr)
 {
-    if (addr < 0x400) {
-        // This has to be a NULL-dereference, drop into debugger.
-        printf("Memory read from bad address: 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
-               addr, reinterpret_cast<intptr_t>(instr));
-        MOZ_CRASH();
-    }
-    if ((addr & 3) == 0) {
+    if (handleWasmFault(addr, 4))
+        return -1;
+
+    if ((addr & 3) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         uint32_t* ptr = reinterpret_cast<uint32_t*>(addr);
         return *ptr;
     }
     printf("Unaligned read at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
     return 0;
 }
 
 int32_t
 Simulator::readW(uint64_t addr, SimInstruction* instr)
 {
-    if (addr < 0x400) {
-        // This has to be a NULL-dereference, drop into debugger.
-        printf("Memory read from bad address: 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
-               addr, reinterpret_cast<intptr_t>(instr));
-        MOZ_CRASH();
-    }
-    if ((addr & 3) == 0) {
+    if (handleWasmFault(addr, 4))
+        return -1;
+
+    if ((addr & 3) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         int32_t* ptr = reinterpret_cast<int32_t*>(addr);
         return *ptr;
     }
     printf("Unaligned read at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
     return 0;
 }
 
 void
 Simulator::writeW(uint64_t addr, uint32_t value, SimInstruction* instr)
 {
-    if (addr < 0x400) {
-        // This has to be a NULL-dereference, drop into debugger.
-        printf("Memory write to bad address: 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
-               addr, reinterpret_cast<intptr_t>(instr));
-        MOZ_CRASH();
-    }
-    if ((addr & 3) == 0) {
+    if (handleWasmFault(addr, 4))
+        return;
+
+    if ((addr & 3) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         uint32_t* ptr = reinterpret_cast<uint32_t*>(addr);
+        LLBit_ = false;
         *ptr = value;
         return;
     }
     printf("Unaligned write at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
 }
 
 void
 Simulator::writeW(uint64_t addr, int32_t value, SimInstruction* instr)
 {
-    if (addr < 0x400) {
-        // This has to be a NULL-dereference, drop into debugger.
-        printf("Memory write to bad address: 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
-               addr, reinterpret_cast<intptr_t>(instr));
-        MOZ_CRASH();
-    }
-    if ((addr & 3) == 0) {
+    if (handleWasmFault(addr, 4))
+        return;
+
+    if ((addr & 3) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         int32_t* ptr = reinterpret_cast<int32_t*>(addr);
+        LLBit_ = false;
         *ptr = value;
         return;
     }
     printf("Unaligned write at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
 }
 
 int64_t
 Simulator::readDW(uint64_t addr, SimInstruction* instr)
 {
-    if (addr < 0x400) {
-        // This has to be a NULL-dereference, drop into debugger.
-        printf("Memory read from bad address: 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
-               addr, reinterpret_cast<intptr_t>(instr));
-        MOZ_CRASH();
-    }
-    if ((addr & kPointerAlignmentMask) == 0) {
-        int64_t* ptr = reinterpret_cast<int64_t*>(addr);
-        return* ptr;
+    if (handleWasmFault(addr, 8))
+        return -1;
+
+    if ((addr & kPointerAlignmentMask) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
+        intptr_t* ptr = reinterpret_cast<intptr_t*>(addr);
+        return *ptr;
     }
     printf("Unaligned read at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
     return 0;
 }
 
 void
 Simulator::writeDW(uint64_t addr, int64_t value, SimInstruction* instr)
 {
-    if (addr < 0x400) {
-        // This has to be a NULL-dereference, drop into debugger.
-        printf("Memory write to bad address: 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
-               addr, reinterpret_cast<intptr_t>(instr));
-        MOZ_CRASH();
-    }
-    if ((addr & kPointerAlignmentMask) == 0) {
+    if (handleWasmFault(addr, 8))
+        return;
+
+    if ((addr & kPointerAlignmentMask) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         int64_t* ptr = reinterpret_cast<int64_t*>(addr);
+        LLBit_ = false;
         *ptr = value;
         return;
     }
     printf("Unaligned write at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
 }
 
 double
 Simulator::readD(uint64_t addr, SimInstruction* instr)
 {
-    if ((addr & kDoubleAlignmentMask) == 0) {
+    if (handleWasmFault(addr, 8))
+        return NAN;
+
+    if ((addr & kDoubleAlignmentMask) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         double* ptr = reinterpret_cast<double*>(addr);
         return *ptr;
     }
     printf("Unaligned (double) read at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
     return 0;
 }
 
 void
 Simulator::writeD(uint64_t addr, double value, SimInstruction* instr)
 {
-    if ((addr & kDoubleAlignmentMask) == 0) {
+    if (handleWasmFault(addr, 8))
+        return;
+
+    if ((addr & kDoubleAlignmentMask) == 0 || wasm::InCompiledCode(reinterpret_cast<void *>(get_pc()))) {
         double* ptr = reinterpret_cast<double*>(addr);
+        LLBit_ = false;
         *ptr = value;
         return;
     }
     printf("Unaligned (double) write at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
            addr, reinterpret_cast<intptr_t>(instr));
     MOZ_CRASH();
 }
 
+int
+Simulator::loadLinkedW(uint64_t addr, SimInstruction* instr)
+{
+    if ((addr & 3) == 0) {
+
+        if (handleWasmFault(addr, 4))
+            return -1;
+
+        volatile int32_t* ptr = reinterpret_cast<volatile int32_t*>(addr);
+        int32_t value = *ptr;
+        lastLLValue_ = value;
+        LLAddr_ = addr;
+        // Note that any memory write or "external" interrupt should reset this value to false.
+        LLBit_ = true;
+        return value;
+    }
+    printf("Unaligned write at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
+           addr, reinterpret_cast<intptr_t>(instr));
+    MOZ_CRASH();
+    return 0;
+}
+
+int
+Simulator::storeConditionalW(uint64_t addr, int value, SimInstruction* instr)
+{
+    // Correct behavior in this case, as defined by architecture, is to just return 0,
+    // but there is no point at allowing that. It is certainly an indicator of a bug.
+    if (addr != LLAddr_) {
+        printf("SC to bad address: 0x%016" PRIx64 ", pc=0x%016" PRIx64 ", expected: 0x%016" PRIx64 "\n",
+               addr, reinterpret_cast<intptr_t>(instr), LLAddr_);
+        MOZ_CRASH();
+    }
+
+    if ((addr & 3) == 0) {
+        SharedMem<int32_t*> ptr = SharedMem<int32_t*>::shared(reinterpret_cast<int32_t*>(addr));
+
+        if (!LLBit_) {
+            return 0;
+        }
+
+        LLBit_ = false;
+        LLAddr_ = 0;
+        int32_t expected = int32_t(lastLLValue_);
+        int32_t old = AtomicOperations::compareExchangeSeqCst(ptr, expected, int32_t(value));
+        return (old == expected) ? 1:0;
+    }
+    printf("Unaligned SC at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
+           addr, reinterpret_cast<intptr_t>(instr));
+    MOZ_CRASH();
+    return 0;
+}
+
+int64_t
+Simulator::loadLinkedD(uint64_t addr, SimInstruction* instr)
+{
+    if ((addr & kPointerAlignmentMask) == 0) {
+
+        if (handleWasmFault(addr, 8))
+            return -1;
+
+        volatile int64_t* ptr = reinterpret_cast<volatile int64_t*>(addr);
+        int64_t value = *ptr;
+        lastLLValue_ = value;
+        LLAddr_ = addr;
+        // Note that any memory write or "external" interrupt should reset this value to false.
+        LLBit_ = true;
+        return value;
+    }
+    printf("Unaligned write at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
+           addr, reinterpret_cast<intptr_t>(instr));
+    MOZ_CRASH();
+    return 0;
+}
+
+int
+Simulator::storeConditionalD(uint64_t addr, int64_t value, SimInstruction* instr)
+{
+    // Correct behavior in this case, as defined by architecture, is to just return 0,
+    // but there is no point at allowing that. It is certainly an indicator of a bug.
+    if (addr != LLAddr_) {
+        printf("SC to bad address: 0x%016" PRIx64 ", pc=0x%016" PRIx64 ", expected: 0x%016" PRIx64 "\n",
+               addr, reinterpret_cast<intptr_t>(instr), LLAddr_);
+        MOZ_CRASH();
+    }
+
+    if ((addr & kPointerAlignmentMask) == 0) {
+        SharedMem<int64_t*> ptr = SharedMem<int64_t*>::shared(reinterpret_cast<int64_t*>(addr));
+
+        if (!LLBit_) {
+            return 0;
+        }
+
+        LLBit_ = false;
+        LLAddr_ = 0;
+        int64_t expected = lastLLValue_;
+        int64_t old = AtomicOperations::compareExchangeSeqCst(ptr, expected, int64_t(value));
+        return (old == expected) ? 1:0;
+    }
+    printf("Unaligned SC at 0x%016" PRIx64 ", pc=0x%016" PRIxPTR "\n",
+           addr, reinterpret_cast<intptr_t>(instr));
+    MOZ_CRASH();
+    return 0;
+}
+
 uintptr_t
 Simulator::stackLimit() const
 {
     return stackLimit_;
 }
 
 uintptr_t*
 Simulator::addressOfStackLimit()
@@ -1927,25 +2126,30 @@ typedef int64_t (*Prototype_General4)(in
 typedef int64_t (*Prototype_General5)(int64_t arg0, int64_t arg1, int64_t arg2, int64_t arg3,
                                       int64_t arg4);
 typedef int64_t (*Prototype_General6)(int64_t arg0, int64_t arg1, int64_t arg2, int64_t arg3,
                                       int64_t arg4, int64_t arg5);
 typedef int64_t (*Prototype_General7)(int64_t arg0, int64_t arg1, int64_t arg2, int64_t arg3,
                                       int64_t arg4, int64_t arg5, int64_t arg6);
 typedef int64_t (*Prototype_General8)(int64_t arg0, int64_t arg1, int64_t arg2, int64_t arg3,
                                       int64_t arg4, int64_t arg5, int64_t arg6, int64_t arg7);
-
+typedef int64_t (*Prototype_GeneralGeneralGeneralInt64)(int64_t arg0, int64_t arg1, int64_t arg2,
+                                                        int64_t arg3);
+typedef int64_t (*Prototype_GeneralGeneralInt64Int64)(int64_t arg0, int64_t arg1, int64_t arg2,
+                                                      int64_t arg3);
 typedef double (*Prototype_Double_None)();
 typedef double (*Prototype_Double_Double)(double arg0);
 typedef double (*Prototype_Double_Int)(int64_t arg0);
 typedef int64_t (*Prototype_Int_Double)(double arg0);
 typedef int64_t (*Prototype_Int_DoubleIntInt)(double arg0, int64_t arg1, int64_t arg2);
 typedef int64_t (*Prototype_Int_IntDoubleIntInt)(int64_t arg0, double arg1, int64_t arg2,
                                                  int64_t arg3);
 typedef float (*Prototype_Float32_Float32)(float arg0);
+typedef float (*Prototype_Float32_Float32Float32)(float arg0, float arg1);
+typedef float (*Prototype_Float32_IntInt)(int arg0, int arg1);
 
 typedef double (*Prototype_DoubleInt)(double arg0, int64_t arg1);
 typedef double (*Prototype_Double_IntDouble)(int64_t arg0, double arg1);
 typedef double (*Prototype_Double_DoubleDouble)(double arg0, double arg1);
 typedef int64_t (*Prototype_Int_IntDouble)(int64_t arg0, double arg1);
 
 typedef double (*Prototype_Double_DoubleDoubleDouble)(double arg0, double arg1, double arg2);
 typedef double (*Prototype_Double_DoubleDoubleDoubleDouble)(double arg0, double arg1,
@@ -2003,16 +2207,19 @@ Simulator::softwareInterrupt(SimInstruct
             Prototype_General2 target = reinterpret_cast<Prototype_General2>(external);
             int64_t result = target(arg0, arg1);
             setCallResult(result);
             break;
           }
           case Args_General3: {
             Prototype_General3 target = reinterpret_cast<Prototype_General3>(external);
             int64_t result = target(arg0, arg1, arg2);
+            if(external == intptr_t(&js::wasm::Instance::wake)) {
+                result = int32_t(result);
+            }
             setCallResult(result);
             break;
           }
           case Args_General4: {
             Prototype_General4 target = reinterpret_cast<Prototype_General4>(external);
             int64_t result = target(arg0, arg1, arg2, arg3);
             setCallResult(result);
             break;
@@ -2052,16 +2259,36 @@ Simulator::softwareInterrupt(SimInstruct
           }
           case Args_Int_Double: {
             double dval0 = getFpuRegisterDouble(12);
             Prototype_Int_Double target = reinterpret_cast<Prototype_Int_Double>(external);
             int64_t res = target(dval0);
             setRegister(v0, res);
             break;
           }
+          case Args_Int_GeneralGeneralGeneralInt64: {
+            Prototype_GeneralGeneralGeneralInt64 target =
+                reinterpret_cast<Prototype_GeneralGeneralGeneralInt64>(external);
+            int64_t result = target(arg0, arg1, arg2, arg3);
+            if(external == intptr_t(&js::wasm::Instance::wait_i32)) {
+                result = int32_t(result);
+            }
+            setCallResult(result);
+            break;
+          }
+          case Args_Int_GeneralGeneralInt64Int64: {
+            Prototype_GeneralGeneralInt64Int64 target =
+                reinterpret_cast<Prototype_GeneralGeneralInt64Int64>(external);
+            int64_t result = target(arg0, arg1, arg2, arg3);
+            if(external == intptr_t(&js::wasm::Instance::wait_i64)) {
+                result = int32_t(result);
+            }
+            setCallResult(result);
+            break;
+          }
           case Args_Int_DoubleIntInt: {
             double dval = getFpuRegisterDouble(12);
             Prototype_Int_DoubleIntInt target = reinterpret_cast<Prototype_Int_DoubleIntInt>(external);
             int64_t res = target(dval, arg1, arg2);
             setRegister(v0, res);
             break;
           }
           case Args_Int_IntDoubleIntInt: {
@@ -2081,16 +2308,32 @@ Simulator::softwareInterrupt(SimInstruct
           case Args_Float32_Float32: {
             float fval0;
             fval0 = getFpuRegisterFloat(12);
             Prototype_Float32_Float32 target = reinterpret_cast<Prototype_Float32_Float32>(external);
             float fresult = target(fval0);
             setCallResultFloat(fresult);
             break;
           }
+          case Args_Float32_Float32Float32: {
+            float fval0;
+            float fval1;
+            fval0 = getFpuRegisterFloat(12);
+            fval1 = getFpuRegisterFloat(13);
+            Prototype_Float32_Float32Float32 target = reinterpret_cast<Prototype_Float32_Float32Float32>(external);
+            float fresult = target(fval0, fval1);
+            setCallResultFloat(fresult);
+            break;
+          }
+          case Args_Float32_IntInt: {
+            Prototype_Float32_IntInt target = reinterpret_cast<Prototype_Float32_IntInt>(external);
+            float fresult = target(arg0, arg1);
+            setCallResultFloat(fresult);
+            break;
+          }
           case Args_Double_Int: {
             Prototype_Double_Int target = reinterpret_cast<Prototype_Double_Int>(external);
             double dresult = target(arg0);
             setCallResultDouble(dresult);
             break;
           }
           case Args_Double_DoubleInt: {
             double dval0 = getFpuRegisterDouble(12);
@@ -2929,16 +3172,31 @@ Simulator::decodeTypeRegister(SimInstruc
               case ff_ceil_l_fmt:  // Mips64r2 instruction.
                 i64 = I64(std::ceil(fs_value));
                 setFpuRegister(fd_reg, i64);
                 break;
               case ff_cvt_ps_s:
               case ff_c_f_fmt:
                 MOZ_CRASH();
                 break;
+              case ff_movf_fmt:
+                if (testFCSRBit(fcsr_cc)) {
+                  setFpuRegisterFloat(fd_reg, getFpuRegisterFloat(fs_reg));
+                }
+                break;
+              case ff_movz_fmt:
+                if (rt == 0) {
+                  setFpuRegisterFloat(fd_reg, getFpuRegisterFloat(fs_reg));
+                }
+                break;
+              case ff_movn_fmt:
+                if (rt != 0) {
+                  setFpuRegisterFloat(fd_reg, getFpuRegisterFloat(fs_reg));
+                }
+                break;
               default:
                 MOZ_CRASH();
             }
             break;
           case rs_d:
             double dt_value, ds_value;
             ds_value = getFpuRegisterDouble(fs_reg);
             dt_value = getFpuRegisterDouble(ft_reg);
@@ -3106,17 +3364,18 @@ Simulator::decodeTypeRegister(SimInstruc
             break;
           case rs_l:
             switch (instr->functionFieldRaw()) {
               case ff_cvt_d_fmt:  // Mips64r2 instruction.
                 i64 = getFpuRegister(fs_reg);
                 setFpuRegisterDouble(fd_reg, static_cast<double>(i64));
                 break;
               case ff_cvt_s_fmt:
-                MOZ_CRASH();
+                i64 = getFpuRegister(fs_reg);
+                setFpuRegisterFloat(fd_reg, static_cast<float>(i64));
                 break;
               default:
                 MOZ_CRASH();
             }
             break;
           case rs_ps:
             break;
           default:
@@ -3473,17 +3732,21 @@ Simulator::decodeTypeImmediate(SimInstru
         addr = rs + se_imm16 - al_offset;
         alu_out = readW(addr, instr);
         alu_out = U32(alu_out) >> al_offset * 8;
         alu_out |= rt & mask;
         break;
       }
       case op_ll:
         addr = rs + se_imm16;
-        alu_out = readW(addr, instr);
+        alu_out = loadLinkedW(addr, instr);
+        break;
+      case op_lld:
+        addr = rs + se_imm16;
+        alu_out = loadLinkedD(addr, instr);
         break;
       case op_ld:
         addr = rs + se_imm16;
         alu_out = readDW(addr, instr);
         break;
       case op_ldl: {
         // al_offset is offset of the effective address within an aligned word.
         uint8_t al_offset = (rs + se_imm16) & 7;
@@ -3530,16 +3793,19 @@ Simulator::decodeTypeImmediate(SimInstru
         addr = rs + se_imm16 - al_offset;
         mem_value = readW(addr, instr);
         mem_value = (rt << al_offset * 8) | (mem_value & mask);
         break;
       }
       case op_sc:
         addr = rs + se_imm16;
         break;
+      case op_scd:
+        addr = rs + se_imm16;
+        break;
       case op_sd:
         addr = rs + se_imm16;
         break;
       case op_sdl: {
         uint8_t al_offset = (rs + se_imm16) & 7;
         uint8_t byte_shift = 7 - al_offset;
         uint64_t mask = byte_shift ? (~0ul << (al_offset + 1) * 8) : 0;
         addr = rs + se_imm16 - al_offset;
@@ -3611,16 +3877,17 @@ Simulator::decodeTypeImmediate(SimInstru
       case op_lb:
       case op_lhu:
       case op_lh:
       case op_lwu:
       case op_lw:
       case op_lwl:
       case op_lwr:
       case op_ll:
+      case op_lld:
       case op_ld:
       case op_ldl:
       case op_ldr:
         setRegister(rt_reg, alu_out);
         break;
       case op_sb:
         writeB(addr, I8(rt), instr);
         break;
@@ -3632,18 +3899,20 @@ Simulator::decodeTypeImmediate(SimInstru
         break;
       case op_swl:
         writeW(addr, I32(mem_value), instr);
         break;
       case op_swr:
         writeW(addr, I32(mem_value), instr);
         break;
       case op_sc:
-        writeW(addr, I32(rt), instr);
-        setRegister(rt_reg, 1);
+        setRegister(rt_reg, storeConditionalW(addr, I32(rt), instr));
+        break;
+      case op_scd:
+        setRegister(rt_reg, storeConditionalD(addr, rt, instr));
         break;
       case op_sd:
         writeDW(addr, rt, instr);
         break;
       case op_sdl:
         writeDW(addr, mem_value, instr);
         break;
       case op_sdr:
diff --git a/js/src/jit/mips64/Simulator-mips64.h b/js/src/jit/mips64/Simulator-mips64.h
--- a/js/src/jit/mips64/Simulator-mips64.h
+++ b/js/src/jit/mips64/Simulator-mips64.h
@@ -155,16 +155,18 @@ class Simulator {
     static Simulator* Create(JSContext* cx);
 
     static void Destroy(Simulator* simulator);
 
     // Constructor/destructor are for internal use only; use the static methods above.
     Simulator();
     ~Simulator();
 
+    static bool supportsAtomics() { return true; }
+
     // The currently executing Simulator instance. Potentially there can be one
     // for each native thread.
     static Simulator* Current();
 
     static inline uintptr_t StackLimit() {
         return Simulator::Current()->stackLimit();
     }
 
@@ -271,16 +273,22 @@ class Simulator {
     inline int64_t readDW(uint64_t addr, SimInstruction* instr);
     inline int64_t readDWL(uint64_t addr, SimInstruction* instr);
     inline int64_t readDWR(uint64_t addr, SimInstruction* instr);
     inline void writeDW(uint64_t addr, int64_t value, SimInstruction* instr);
 
     inline double readD(uint64_t addr, SimInstruction* instr);
     inline void writeD(uint64_t addr, double value, SimInstruction* instr);
 
+    inline int32_t loadLinkedW(uint64_t addr, SimInstruction* instr);
+    inline int storeConditionalW(uint64_t addr, int32_t value, SimInstruction* instr);
+
+    inline int64_t loadLinkedD(uint64_t addr, SimInstruction* instr);
+    inline int storeConditionalD(uint64_t addr, int64_t value, SimInstruction* instr);
+
     // Helper function for decodeTypeRegister.
     void configureTypeRegister(SimInstruction* instr,
                                int64_t& alu_out,
                                __int128& i128hilo,
                                unsigned __int128& u128hilo,
                                int64_t& next_pc,
                                int32_t& return_addr_reg,
                                bool& do_interrupt);
@@ -303,16 +311,18 @@ class Simulator {
     void disableStop(uint32_t code);
     void increaseStopCounter(uint32_t code);
     void printStopInfo(uint32_t code);
 
     // Handle a wasm interrupt triggered by an async signal handler.
     void handleWasmInterrupt();
     void startInterrupt(JitActivation* act);
 
+    // Handle any wasm faults, returning true if the fault was handled.
+    bool handleWasmFault(uint64_t addr, unsigned numBytes);
     bool handleWasmTrapFault();
 
     // Executes one instruction.
     void instructionDecode(SimInstruction* instr);
     // Execute one instruction placed in a branch delay slot.
     void branchDelayInstructionDecode(SimInstruction* instr);
 
   public:
@@ -345,16 +355,20 @@ class Simulator {
     // Architecture state.
     // Registers.
     int64_t registers_[kNumSimuRegisters];
     // Coprocessor Registers.
     int64_t FPUregisters_[kNumFPURegisters];
     // FPU control register.
     uint32_t FCSR_;
 
+    bool LLBit_;
+    uintptr_t LLAddr_;
+    int64_t lastLLValue_;
+
     // Simulator support.
     char* stack_;
     uintptr_t stackLimit_;
     bool pc_modified_;
     int64_t icount_;
     int64_t break_count_;
 
     // wasm async interrupt support
diff --git a/js/src/wasm/WasmBuiltins.cpp b/js/src/wasm/WasmBuiltins.cpp
--- a/js/src/wasm/WasmBuiltins.cpp
+++ b/js/src/wasm/WasmBuiltins.cpp
@@ -589,17 +589,17 @@ AddressOf(SymbolicAddress imm, ABIFuncti
         return FuncCast(Instance::currentMemory_i32, *abiType);
       case SymbolicAddress::WaitI32:
         *abiType = Args_Int_GeneralGeneralGeneralInt64;
         return FuncCast(Instance::wait_i32, *abiType);
       case SymbolicAddress::WaitI64:
         *abiType = Args_Int_GeneralGeneralInt64Int64;
         return FuncCast(Instance::wait_i64, *abiType);
       case SymbolicAddress::Wake:
-        *abiType = Args_General2;
+        *abiType = Args_General3;
         return FuncCast(Instance::wake, *abiType);
 #if defined(JS_CODEGEN_MIPS32)
       case SymbolicAddress::js_jit_gAtomic64Lock:
         return &js::jit::gAtomic64Lock;
 #endif
       case SymbolicAddress::Limit:
         break;
     }
