# HG changeset patch
# User Edwin Gao <egao@mozilla.com>
# Date 1543442636 0
# Node ID c4fe963f4eb4167dbb2149cfc7444a688de8bf66
# Parent  4939e4a1cc202d78ab4506c0ef37e06c587537cb
Bug 1305743 - Add optional parameter to make failure to find mozinfo.json a fatal error r=gbrown

Changes:
- added optional keyword argument to the find_and_update_from_json() function: raise_exception
- the optional keyword argument accepts a boolean value depending on if exception is desired.

Tests
- added a few tests to ensure the expected exceptions are raised only when raise_exception flag is True.

Other changes:
- improved docstring for the find_and_update_from_json() method.

Differential Revision: https://phabricator.services.mozilla.com/D13316

diff --git a/testing/mozbase/mozinfo/mozinfo/mozinfo.py b/testing/mozbase/mozinfo/mozinfo/mozinfo.py
--- a/testing/mozbase/mozinfo/mozinfo/mozinfo.py
+++ b/testing/mozbase/mozinfo/mozinfo/mozinfo.py
@@ -210,26 +210,39 @@ def update(new_info):
     # convenience data for os access
     for os_name in choices['os']:
         globals()['is' + os_name.title()] = info['os'] == os_name
     # unix is special
     if isLinux or isBsd:  # noqa
         globals()['isUnix'] = True
 
 
-def find_and_update_from_json(*dirs):
-    """
-    Find a mozinfo.json file, load it, and update the info with the
-    contents.
+def find_and_update_from_json(*dirs, **kwargs):
+    """Find a mozinfo.json file, load it, and update global symbol table.
+
+    This method will first check the relevant objdir directory for the
+    necessary mozinfo.json file, if the current script is being run from a
+    Mozilla objdir.
+
+    If the objdir directory did not supply the necessary data, this method
+    will then look for the required mozinfo.json file from the provided
+    tuple of directories.
 
-    :param dirs: Directories in which to look for the file. They will be
-                 searched after first looking in the root of the objdir
-                 if the current script is being run from a Mozilla objdir.
+    If file is found, the global symbols table is updated via a helper method.
+
+    If no valid files are found, this method no-ops unless the raise_exception
+    kwargs is provided with explicit boolean value of True.
 
-    Returns the full path to mozinfo.json if it was found, or None otherwise.
+    :param tuple dirs: Directories in which to look for the file.
+    :param dict kwargs: optional values:
+                        raise_exception: if True, exceptions are raised.
+                                         False by default.
+    :returns: None: default behavior if mozinfo.json cannot be found.
+              json_path: string representation of mozinfo.json path.
+    :raises: IOError: if raise_exception is True and file is not found.
     """
     # First, see if we're in an objdir
     try:
         from mozbuild.base import MozbuildObject, BuildEnvironmentNotFoundException
         from mozbuild.mozconfig import MozconfigFindException
         build = MozbuildObject.from_environment()
         json_path = _os.path.join(build.topobjdir, "mozinfo.json")
         if _os.path.isfile(json_path):
@@ -242,16 +255,20 @@ def find_and_update_from_json(*dirs):
 
     for d in dirs:
         d = _os.path.abspath(d)
         json_path = _os.path.join(d, "mozinfo.json")
         if _os.path.isfile(json_path):
             update(json_path)
             return json_path
 
+    # by default, exceptions are suppressed. Set this to True if otherwise
+    # desired.
+    if kwargs.get('raise_exception', False):
+        raise IOError('mozinfo.json could not be found.')
     return None
 
 
 def output_to_file(path):
     import json
     with open(path, 'w') as f:
         f.write(json.dumps(info))
 
diff --git a/testing/mozbase/mozinfo/tests/test.py b/testing/mozbase/mozinfo/tests/test.py
--- a/testing/mozbase/mozinfo/tests/test.py
+++ b/testing/mozbase/mozinfo/tests/test.py
@@ -70,24 +70,45 @@ class TestMozinfo(unittest.TestCase):
         """Test that mozinfo.find_and_update_from_json can
         find mozinfo.json in a directory passed to it."""
         j = os.path.join(self.tempdir, "mozinfo.json")
         with open(j, "w") as f:
             f.write(json.dumps({"foo": "abcdefg"}))
         self.assertEqual(mozinfo.find_and_update_from_json(self.tempdir), j)
         self.assertEqual(mozinfo.info["foo"], "abcdefg")
 
+    def test_find_and_update_file_no_argument(self):
+        """Test that mozinfo.find_and_update_from_json no-ops on not being
+        given any arguments.
+        """
+        self.assertEqual(mozinfo.find_and_update_from_json(), None)
+
     def test_find_and_update_file_invalid_json(self):
         """Test that mozinfo.find_and_update_from_json can
         handle invalid JSON"""
         j = os.path.join(self.tempdir, "mozinfo.json")
         with open(j, 'w') as f:
             f.write('invalid{"json":')
         self.assertRaises(ValueError, mozinfo.find_and_update_from_json, self.tempdir)
 
+    def test_find_and_update_file_raise_exception(self):
+        """Test that mozinfo.find_and_update_from_json raises
+        an IOError when exceptions are unsuppressed.
+        """
+        with self.assertRaises(IOError):
+            mozinfo.find_and_update_from_json(raise_exception=True)
+
+    def test_find_and_update_file_suppress_exception(self):
+        """Test that mozinfo.find_and_update_from_json suppresses
+        an IOError exception if a False boolean value is
+        provided as the only argument.
+        """
+        self.assertEqual(mozinfo.find_and_update_from_json(
+            raise_exception=False), None)
+
     def test_find_and_update_file_mozbuild(self):
         """Test that mozinfo.find_and_update_from_json can
         find mozinfo.json using the mozbuild module."""
         j = os.path.join(self.tempdir, "mozinfo.json")
         with open(j, "w") as f:
             f.write(json.dumps({"foo": "123456"}))
         m = mock.MagicMock()
         # Mock the value of MozbuildObject.from_environment().topobjdir.

