# HG changeset patch
# User Mats Palmgren <mats@mozilla.com>
# Date 1521150085 -3600
#      Thu Mar 15 22:41:25 2018 +0100
# Node ID bcc15578356141883ac267f12a224522302c3ce8
# Parent  b3bfae184ffe4d52ace8a43e35c1534c454b973a
Bug 1425599 part 13 - [css-grid] Merge Grow[Base|Limits]ForSpanningItems into a templated method instead (idempotent change).  r=dholbert

diff --git a/layout/generic/nsGridContainerFrame.cpp b/layout/generic/nsGridContainerFrame.cpp
--- a/layout/generic/nsGridContainerFrame.cpp
+++ b/layout/generic/nsGridContainerFrame.cpp
@@ -1178,35 +1178,27 @@ struct nsGridContainerFrame::Tracks
       }
       MOZ_MAKE_COMPILER_ASSUME_IS_UNREACHABLE("Unexpected phase");
     }
   };
 
   using FitContentClamper =
     std::function<bool(uint32_t aTrack, nscoord aMinSize, nscoord* aSize)>;
 
-  // Helper methods for ResolveIntrinsicSize.
+  // Helper method for ResolveIntrinsicSize.
   template<TrackSizingPhase phase>
-  bool GrowBaseForSpanningItems(const nsTArray<Step2ItemData>& aItemData,
+  bool GrowSizeForSpanningItems(const nsTArray<Step2ItemData>& aItemData,
                                 nsTArray<uint32_t>& aTracks,
                                 nsTArray<TrackSize>& aPlan,
                                 nsTArray<TrackSize>& aItemPlan,
                                 TrackSize::StateBits aSelector,
                                 uint32_t aStartIndex,
                                 uint32_t aEndIndex,
-                                const FitContentClamper& aClamper = nullptr);
-  template<TrackSizingPhase phase>
-  bool GrowLimitForSpanningItems(const nsTArray<Step2ItemData>& aItemData,
-                                 nsTArray<uint32_t>& aTracks,
-                                 nsTArray<TrackSize>& aPlan,
-                                 nsTArray<TrackSize>& aItemPlan,
-                                 TrackSize::StateBits aSelector,
-                                 uint32_t aStartIndex,
-                                 uint32_t aEndIndex,
-                                 const FitContentClamper& aClamper = nullptr);
+                                const FitContentClamper& aClamper = nullptr,
+                                bool aNeedInfinitelyGrowableFlag = false);
   /**
    * Resolve Intrinsic Track Sizes.
    * http://dev.w3.org/csswg/css-grid/#algo-content
    */
   void ResolveIntrinsicSize(GridReflowInput&            aState,
                             nsTArray<GridItemInfo>&     aGridItems,
                             const TrackSizingFunctions& aFunctions,
                             LineRange GridArea::*       aRange,
@@ -4250,86 +4242,62 @@ nsGridContainerFrame::Tracks::AlignBasel
       break;
     default:
       MOZ_ASSERT_UNREACHABLE("unexpected baseline subtree alignment");
   }
 }
 
 template<nsGridContainerFrame::Tracks::TrackSizingPhase phase>
 bool
-nsGridContainerFrame::Tracks::GrowBaseForSpanningItems(
+nsGridContainerFrame::Tracks::GrowSizeForSpanningItems(
   const nsTArray<Step2ItemData>& aItemData,
   nsTArray<uint32_t>& aTracks,
   nsTArray<TrackSize>& aPlan,
   nsTArray<TrackSize>& aItemPlan,
   TrackSize::StateBits aSelector,
   uint32_t aStartIndex,
   uint32_t aEndIndex,
-  const FitContentClamper& aFitContentClamper)
+  const FitContentClamper& aFitContentClamper,
+  bool aNeedInfinitelyGrowableFlag)
 {
-  bool updatedBase = false;
+  constexpr bool isMaxSizingPhase =
+    phase == TrackSizingPhase::eIntrinsicMaximums ||
+    phase == TrackSizingPhase::eMaxContentMaximums;
+  bool needToUpdateSizes = false;
   InitializePlan<phase>(aPlan);
   for (uint32_t i = aStartIndex; i < aEndIndex; ++i) {
     const Step2ItemData& item = aItemData[i];
     if (!(item.mState & aSelector)) {
       continue;
     }
+    if (isMaxSizingPhase) {
+      for (auto j = item.mLineRange.mStart, end = item.mLineRange.mEnd; j < end; ++j) {
+        aPlan[j].mState |= TrackSize::eModified;
+      }
+    }
     nscoord space = item.SizeContributionForPhase(phase);
     if (space <= 0) {
       continue;
     }
     aTracks.ClearAndRetainStorage();
     space = CollectGrowable<phase>(space, item.mLineRange, aSelector,
                                    aTracks);
     if (space > 0) {
       DistributeToTrackSizes<phase>(space, aPlan, aItemPlan, aTracks, aSelector,
                                     aFitContentClamper);
-      updatedBase = true;
-    }
-  }
-  if (updatedBase) {
-    CopyPlanToSize<phase>(aPlan);
-  }
-  return updatedBase;
-}
-
-template<nsGridContainerFrame::Tracks::TrackSizingPhase phase>
-bool
-nsGridContainerFrame::Tracks::GrowLimitForSpanningItems(
-  const nsTArray<Step2ItemData>& aItemData,
-  nsTArray<uint32_t>& aTracks,
-  nsTArray<TrackSize>& aPlan,
-  nsTArray<TrackSize>& aItemPlan,
-  TrackSize::StateBits aSelector,
-  uint32_t aStartIndex,
-  uint32_t aEndIndex,
-  const FitContentClamper& aFitContentClamper)
-{
-  InitializePlan<phase>(aPlan);
-  for (uint32_t i = aStartIndex; i < aEndIndex; ++i) {
-    const Step2ItemData& item = aItemData[i];
-    if (!(item.mState & aSelector)) {
-      continue;
-    }
-    for (auto j = item.mLineRange.mStart, end = item.mLineRange.mEnd; j < end; ++j) {
-      aPlan[j].mState |= TrackSize::eModified;
-    }
-    nscoord space = item.SizeContributionForPhase(phase);
-    if (space > 0) {
-      aTracks.ClearAndRetainStorage();
-      space = CollectGrowable<phase>(space, item.mLineRange, aSelector,
-                                     aTracks);
-      if (space > 0) {
-        DistributeToTrackSizes<phase>(space, aPlan, aItemPlan, aTracks,
-                                      TrackSize::StateBits(0),
-                                      aFitContentClamper);
-      }
-    }
-  }
-  return true;
+      needToUpdateSizes = true;
+    }
+  }
+  if (isMaxSizingPhase) {
+    needToUpdateSizes = true;
+  }
+  if (needToUpdateSizes) {
+    CopyPlanToSize<phase>(aPlan, aNeedInfinitelyGrowableFlag);
+  }
+  return needToUpdateSizes;
 }
 
 void
 nsGridContainerFrame::Tracks::ResolveIntrinsicSize(
   GridReflowInput&            aState,
   nsTArray<GridItemInfo>&     aGridItems,
   const TrackSizingFunctions& aFunctions,
   LineRange GridArea::*       aRange,
@@ -4479,37 +4447,37 @@ nsGridContainerFrame::Tracks::ResolveInt
         }
       }
 
       bool updatedBase = false; // Did we update any mBase in step 2.1 - 2.3?
       TrackSize::StateBits selector(TrackSize::eIntrinsicMinSizing);
       if (stateBitsPerSpan[span] & selector) {
         // Step 2.1 MinSize to intrinsic min-sizing.
         updatedBase =
-          GrowBaseForSpanningItems<TrackSizingPhase::eIntrinsicMinimums>(
+          GrowSizeForSpanningItems<TrackSizingPhase::eIntrinsicMinimums>(
             step2Items, tracks, plan, itemPlan, selector,
             spanGroupStartIndex, spanGroupEndIndex);
       }
 
       selector = contentBasedMinSelector;
       if (stateBitsPerSpan[span] & selector) {
         // Step 2.2 MinContentContribution to min-/max-content (and 'auto' when
         // sizing under a min-content constraint) min-sizing.
         updatedBase |=
-          GrowBaseForSpanningItems<TrackSizingPhase::eContentBasedMinimums>(
+          GrowSizeForSpanningItems<TrackSizingPhase::eContentBasedMinimums>(
             step2Items, tracks, plan, itemPlan, selector,
             spanGroupStartIndex, spanGroupEndIndex);
       }
 
       selector = maxContentMinSelector;
       if (stateBitsPerSpan[span] & selector) {
         // Step 2.3 MaxContentContribution to max-content (and 'auto' when
         // sizing under a max-content constraint) min-sizing.
         updatedBase |=
-          GrowBaseForSpanningItems<TrackSizingPhase::eMaxContentMinimums>(
+          GrowSizeForSpanningItems<TrackSizingPhase::eMaxContentMinimums>(
             step2Items, tracks, plan, itemPlan, selector,
             spanGroupStartIndex, spanGroupEndIndex);
       }
 
       if (updatedBase) {
         // Step 2.4
         for (TrackSize& sz : mSizes) {
           if (sz.mBase > sz.mLimit) {
@@ -4517,27 +4485,26 @@ nsGridContainerFrame::Tracks::ResolveInt
           }
         }
       }
 
       if (stateBitsPerSpan[span] & TrackSize::eIntrinsicMaxSizing) {
         const bool willRunStep2_6 =
           stateBitsPerSpan[span] & TrackSize::eAutoOrMaxContentMaxSizing;
         // Step 2.5 MinSize to intrinsic max-sizing.
-        GrowLimitForSpanningItems<TrackSizingPhase::eIntrinsicMaximums>(
+        GrowSizeForSpanningItems<TrackSizingPhase::eIntrinsicMaximums>(
           step2Items, tracks, plan, itemPlan, TrackSize::eIntrinsicMaxSizing,
-          spanGroupStartIndex, spanGroupEndIndex, fitContentClamper);
-        CopyPlanToSize<TrackSizingPhase::eIntrinsicMaximums>(plan, willRunStep2_6);
+          spanGroupStartIndex, spanGroupEndIndex, fitContentClamper,
+          willRunStep2_6);
 
         if (willRunStep2_6) {
           // Step 2.6 MaxContentContribution to max-content max-sizing.
-          GrowLimitForSpanningItems<TrackSizingPhase::eMaxContentMaximums>(
+          GrowSizeForSpanningItems<TrackSizingPhase::eMaxContentMaximums>(
             step2Items, tracks, plan, itemPlan, TrackSize::eAutoOrMaxContentMaxSizing,
             spanGroupStartIndex, spanGroupEndIndex, fitContentClamper);
-          CopyPlanToSize<TrackSizingPhase::eMaxContentMaximums>(plan);
         }
       }
 
       i = spanGroupEndIndex;
     }
   }
 
   // Step 3.
