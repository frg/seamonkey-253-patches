# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1540992420 0
# Node ID ab9d9c4cd361557ea0f07e71e6b1a10fb0585938
# Parent  58bb44b8cbba05ecdc9bb23cd826fd90f3a88969
Bug 1408675 p2. Fix Eclipse CDT project missing defines. r=botond

consume_object() is only called with Defines objects for directories containing
a moz.build file that sets a DEFINES value, and those Defines objects only
contain the defines set in that moz.build (not defines that affect the
directory's files that are set elsewhere).  That's why we fail to set some
defines.

This commit changes consume_object to obtain defines from the ComputedFlags
objects that it is passed instead.  We get one of these objects for every
directory with a moz.build, and we can obtain all defines passed to the
compiler for the files in a given directory from these objects.

Differential Revision: https://phabricator.services.mozilla.com/D12009

diff --git a/python/mozbuild/mozbuild/backend/cpp_eclipse.py b/python/mozbuild/mozbuild/backend/cpp_eclipse.py
--- a/python/mozbuild/mozbuild/backend/cpp_eclipse.py
+++ b/python/mozbuild/mozbuild/backend/cpp_eclipse.py
@@ -10,16 +10,17 @@ import os
 import shutil
 import subprocess
 import types
 from xml.sax.saxutils import quoteattr
 import xml.etree.ElementTree as ET
 from .common import CommonBackend
 
 from ..frontend.data import (
+    ComputedFlags,
     Defines,
 )
 from mozbuild.base import ExecutionSummary
 
 # TODO Have ./mach eclipse generate the workspace and index it:
 # /Users/bgirard/mozilla/eclipse/eclipse/eclipse/eclipse -application org.eclipse.cdt.managedbuilder.core.headlessbuild -data $PWD/workspace -importAll $PWD/eclipse
 # Open eclipse:
 # /Users/bgirard/mozilla/eclipse/eclipse/eclipse/eclipse -data $PWD/workspace
@@ -72,18 +73,22 @@ class CppEclipseBackend(CommonBackend):
         workspace_dirname = "eclipse_" + os.path.basename(topobjdir)
         return os.path.join(srcdir_parent, workspace_dirname)
 
     def consume_object(self, obj):
         reldir = getattr(obj, 'relsrcdir', None)
 
         # Note that unlike VS, Eclipse' indexer seem to crawl the headers and
         # isn't picky about the local includes.
-        if isinstance(obj, Defines):
-            self._paths_to_defines.setdefault(reldir, {}).update(obj.defines)
+        if isinstance(obj, ComputedFlags):
+            defs = self._paths_to_defines.setdefault(reldir, [])
+            if "DEFINES" in obj.flags and obj.flags["DEFINES"]:
+                defs += obj.flags["DEFINES"]
+            if "LIBRARY_DEFINES" in obj.flags and obj.flags["LIBRARY_DEFINES"]:
+                defs += obj.flags["LIBRARY_DEFINES"]
 
         return True
 
     def consume_finished(self):
         settings_dir = os.path.join(self._project_dir, '.settings')
         launch_dir = os.path.join(self._project_dir, 'RunConfigurations')
         workspace_settings_dir = os.path.join(self._workspace_dir, '.metadata/.plugins/org.eclipse.core.runtime/.settings')
         workspace_language_dir = os.path.join(self._workspace_dir, '.metadata/.plugins/org.eclipse.cdt.core')
@@ -258,20 +263,30 @@ class CppEclipseBackend(CommonBackend):
         # important headers has the defines we want.
         #
         dirsettings_template += add_objdir_include_path('ipc/ipdl/_ipdlheaders')
         dirsettings_template += add_define('MOZILLA_INTERNAL_API', '1')
 
         for path, defines in self._paths_to_defines.items():
             dirsettings = dirsettings_template
             dirsettings = dirsettings.replace('@RELATIVE_PATH@', path)
-            for k, v in defines.items():
-                if v == True:
-                    v = ""
-                dirsettings += add_define(k, str(v))
+            for d in defines:
+                assert(d[:2] == u"-D" or d[:2] == u"-U")
+                if d[:2] == u"-U":
+                    # gfx/harfbuzz/src uses -UDEBUG, at least on Mac
+                    # netwerk/sctp/src uses -U__APPLE__ on Mac
+                    # XXX We should make this code smart enough to remove existing defines.
+                    continue
+                d = d[2:] # get rid of leading "-D"
+                name_value = d.split("=", 1)
+                name = name_value[0]
+                value = ""
+                if len(name_value) == 2:
+                    value = name_value[1]
+                dirsettings += add_define(name, str(value))
             dirsettings += LANGUAGE_SETTINGS_TEMPLATE_DIR_FOOTER
             fh.write(dirsettings)
 
         fh.write(LANGUAGE_SETTINGS_TEMPLATE_FOOTER.replace("@COMPILER_FLAGS@", self._cxx + " " + self._cppflags))
 
     def _write_launch_files(self, launch_dir):
         bin_dir = os.path.join(self.environment.topobjdir, 'dist')
 

