# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1614043605 0
#      Tue Feb 23 01:26:45 2021 +0000
# Node ID da0ee340f69903904c61da6b2d1cfac2d3aca4f2
# Parent  d17a4e2acf84047fcb1a47598747760378dbc8a4
Bug 1692940 - Switch ffvpx build to nasm instead of yasm. r=firefox-build-system-reviewers,dmajor

nasm doesn't like compiling simple_idct10.asm on x86
(https://bugzilla.nasm.us/show_bug.cgi?id=3392738), which is empty once
preprocessed for x86, so exclude it there.

Differential Revision: https://phabricator.services.mozilla.com/D105429

diff --git a/media/ffvpx/ffvpxcommon.mozbuild b/media/ffvpx/ffvpxcommon.mozbuild
--- a/media/ffvpx/ffvpxcommon.mozbuild
+++ b/media/ffvpx/ffvpxcommon.mozbuild
@@ -7,18 +7,18 @@
 # Add assembler flags and includes
 if CONFIG['CPU_ARCH'] != 'aarch64':
     ASFLAGS += CONFIG['FFVPX_ASFLAGS']
     ASFLAGS += ['-I%s/media/ffvpx/' % TOPSRCDIR]
     ASFLAGS += ['-I%s/media/ffvpx/libavcodec/x86/' % TOPSRCDIR]
     ASFLAGS += ['-I%s/media/ffvpx/libavutil/x86/' % TOPSRCDIR]
 
 if CONFIG['FFVPX_ASFLAGS']:
-    if CONFIG['FFVPX_USE_YASM']:
-        USE_YASM = True
+    if CONFIG['FFVPX_USE_NASM']:
+        USE_NASM = True
 
     if CONFIG['OS_ARCH'] == 'WINNT':
        # Fix inline symbols and math defines for windows.
         DEFINES['_USE_MATH_DEFINES'] = True
         DEFINES['inline'] = "__inline"
 
 LOCAL_INCLUDES += ['/media/ffvpx']
 
diff --git a/media/ffvpx/libavcodec/x86/moz.build b/media/ffvpx/libavcodec/x86/moz.build
--- a/media/ffvpx/libavcodec/x86/moz.build
+++ b/media/ffvpx/libavcodec/x86/moz.build
@@ -6,16 +6,18 @@
 
 SOURCES += [
     'constants.c',
     'flacdsp.asm',
     'flacdsp_init.c',
     'h264_intrapred.asm',
     'h264_intrapred_10bit.asm',
     'h264_intrapred_init.c',
+# Bug 1582271
+#    -    'simple_idct10.asm',
     'videodsp.asm',
     'videodsp_init.c',
     'vp8dsp.asm',
     'vp8dsp_init.c',
     'vp8dsp_loopfilter.asm',
     'vp9dsp_init.c',
     'vp9dsp_init_10bpp.c',
     'vp9dsp_init_12bpp.c',
@@ -25,16 +27,22 @@ SOURCES += [
     'vp9itxfm.asm',
     'vp9itxfm_16bpp.asm',
     'vp9lpf.asm',
     'vp9lpf_16bpp.asm',
     'vp9mc.asm',
     'vp9mc_16bpp.asm',
 ]
 
+# Bug 1582271
+# if CONFIG['CPU_ARCH'] == "x86_64":
+#     SOURCES += [
+#         'simple_idct10.asm',
+#     ]
+
 if CONFIG['MOZ_LIBAV_FFT']:
     SOURCES += [
         'fft.asm',
         'fft_init.c',
     ]
 
 FINAL_LIBRARY = 'mozavcodec'
 
diff --git a/toolkit/moz.configure b/toolkit/moz.configure
--- a/toolkit/moz.configure
+++ b/toolkit/moz.configure
@@ -1392,19 +1392,19 @@ with only_when(compile_environment):
         return target.kernel == "WINNT" or target.cpu == "x86_64"
 
     set_config('MOZ_LIBAV_FFT', depends(when=libav_fft)(lambda: True))
     set_define('MOZ_LIBAV_FFT', depends(when=libav_fft)(lambda: True))
 
 
 with only_when(compile_environment):
 
-    @depends(vpx_as_flags, target)
-    def ffvpx(vpx_as_flags, target):
-        enable = use_yasm = True
+    @depends(target)
+    def ffvpx(target):
+        enable = use_nasm = True
         flac_only = False
         flags = []
 
         if target.kernel == "WINNT":
             if target.cpu == "x86":
                 # 32-bit windows need to prefix symbols with an underscore.
                 flags = ["-DPIC", "-DWIN32", "-DPREFIX", "-Pconfig_win32.asm"]
             elif target.cpu == "x86_64":
@@ -1412,17 +1412,17 @@ with only_when(compile_environment):
                     "-D__x86_64__",
                     "-DPIC",
                     "-DWIN64",
                     "-DMSVC",
                     "-Pconfig_win64.asm",
                 ]
             elif target.cpu == "aarch64":
                 flags = ["-DPIC", "-DWIN64"]
-                use_yasm = False
+                use_nasm = False
         elif target.kernel == "Darwin":
             if target.cpu == "x86_64":
                 # 32/64-bit macosx asemblers need to prefix symbols with an
                 # underscore.
                 flags = [
                     "-D__x86_64__",
                     "-DPIC",
                     "-DMACHO",
@@ -1430,53 +1430,59 @@ with only_when(compile_environment):
                     "-Pconfig_darwin64.asm",
                 ]
             else:
                 flac_only = True
         elif target.cpu == "x86_64":
             flags = ["-D__x86_64__", "-DPIC", "-DELF", "-Pconfig_unix64.asm"]
         elif target.cpu == "x86":
             flac_only = True
-        elif target.cpu in ("arm", "aarch64"):
-            flac_only = True
-            flags.extend(vpx_as_flags)
         else:
             enable = False
 
         if flac_only or not enable:
-            use_yasm = False
+            use_nasm = False
 
-        if use_yasm:
+        if use_nasm:
             # default disabled components
             flags.append('-Pdefaults_disabled.asm')
 
         return namespace(
             enable=enable,
-            need_yasm="1.2" if use_yasm else False,
+            use_nasm=use_nasm,
             flac_only=flac_only,
             flags=flags,
         )
 
+    @depends(when=ffvpx.use_nasm)
+    def ffvpx_nasm():
+        # nasm 2.10 for AVX-2 support.
+        return namespace(version="2.10", what="FFVPX")
+
+    # ffvpx_nasm can't indirectly depend on vpx_as_flags, because it depends
+    # on a compiler test, so we have to do a little bit of dance here.
+    @depends(ffvpx, vpx_as_flags, target)
+    def ffvpx(ffvpx, vpx_as_flags, target):
+        if ffvpx and target.cpu in ("arm", "aarch64"):
+            ffvpx.flags.extend(vpx_as_flags)
+        return ffvpx
 
     set_config('MOZ_FFVPX', True, when=ffvpx.enable)
     set_define('MOZ_FFVPX', True, when=ffvpx.enable)
     set_config('MOZ_FFVPX_FLACONLY', True, when=ffvpx.flac_only)
     set_define('MOZ_FFVPX_FLACONLY', True, when=ffvpx.flac_only)
     set_config('FFVPX_ASFLAGS', ffvpx.flags)
-    set_config("FFVPX_USE_YASM", True, when=ffvpx.need_yasm)
+    set_config("FFVPX_USE_NASM", True, when=ffvpx.use_nasm)
 
 
 @depends(yasm_version,
-         ffvpx.use_yasm,
 )
 @imports(_from='__builtin__', _import='sorted')
-def valid_yasm_version(yasm_version, for_ffvpx=False):
-    # Note: the default for for_ffvpx above only matters for unit tests.
+def valid_yasm_version(yasm_version):
     requires = {
-        'ffvpx': for_ffvpx,
     }
     requires = {k: v for (k, v) in requires.items() if v}
     if requires and not yasm_version:
         items = sorted(requires.keys())
         if len(items) > 1:
             what = ' and '.join((', '.join(items[:-1]), items[-1]))
         else:
             what = items[0]
@@ -1489,17 +1495,17 @@ def valid_yasm_version(yasm_version, for
         what, version = by_version[-1]
         if yasm_version < version:
             die('Yasm version %s or greater is required to build with %s.'
                 % (version, what))
 
 
 # nasm detection
 # ==============================================================
-@depends(dav1d_nasm, vpx_nasm, jpeg_nasm)
+@depends(dav1d_nasm, vpx_nasm, jpeg_nasm, ffvpx_nasm)
 def need_nasm(*requirements):
     requires = {
         x.what: x.version if hasattr(x, "version") else True for x in requirements if x
     }
     if requires:
         items = sorted(requires.keys())
         if len(items) > 1:
             what = " and ".join((", ".join(items[:-1]), items[-1]))
