# HG changeset patch
# User Daisuke Akatsuka <dakatsuka@mozilla.com>
# Date 1518617891 -32400
# Node ID 1a0183c9ee732d99e1f056a33e121ba891455e18
# Parent  fde55d1476760a60b75289453221c1ff8b71a15f
Bug 1416106 - Part 2: Implement compositor sign to property name. r=gl

MozReview-Commit-ID: v7c6rt6vpl

diff --git a/devtools/client/inspector/animation/components/AnimatedPropertyItem.js b/devtools/client/inspector/animation/components/AnimatedPropertyItem.js
--- a/devtools/client/inspector/animation/components/AnimatedPropertyItem.js
+++ b/devtools/client/inspector/animation/components/AnimatedPropertyItem.js
@@ -9,29 +9,34 @@ const dom = require("devtools/client/sha
 const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
 
 const AnimatedPropertyName = createFactory(require("./AnimatedPropertyName"));
 
 class AnimatedPropertyItem extends PureComponent {
   static get propTypes() {
     return {
       property: PropTypes.string.isRequired,
+      state: PropTypes.object.isRequired,
       values: PropTypes.array.isRequired,
     };
   }
 
   render() {
-    const { property } = this.props;
+    const {
+      property,
+      state,
+    } = this.props;
 
     return dom.li(
       {
         className: "animated-property-item"
       },
       AnimatedPropertyName(
         {
           property,
+          state,
         }
       )
     );
   }
 }
 
 module.exports = AnimatedPropertyItem;
diff --git a/devtools/client/inspector/animation/components/AnimatedPropertyList.js b/devtools/client/inspector/animation/components/AnimatedPropertyList.js
--- a/devtools/client/inspector/animation/components/AnimatedPropertyList.js
+++ b/devtools/client/inspector/animation/components/AnimatedPropertyList.js
@@ -30,16 +30,28 @@ class AnimatedPropertyList extends PureC
   componentDidMount() {
     this.updateKeyframesList(this.props.animation);
   }
 
   componentWillReceiveProps(nextProps) {
     this.updateKeyframesList(nextProps.animation);
   }
 
+  getPropertyState(property) {
+    const { animation } = this.props;
+
+    for (const propState of animation.state.propertyState) {
+      if (propState.property === property) {
+        return propState;
+      }
+    }
+
+    return null;
+  }
+
   async updateKeyframesList(animation) {
     const {
       getAnimatedPropertyMap,
       emitEventForTest,
     } = this.props;
     const animatedPropertyMap = await getAnimatedPropertyMap(animation);
 
     this.setState({ animatedPropertyMap });
@@ -54,19 +66,21 @@ class AnimatedPropertyList extends PureC
       return null;
     }
 
     return dom.ul(
       {
         className: "animated-property-list"
       },
       [...animatedPropertyMap.entries()].map(([property, values]) => {
+        const state = this.getPropertyState(property);
         return AnimatedPropertyItem(
           {
             property,
+            state,
             values,
           }
         );
       })
     );
   }
 }
 
diff --git a/devtools/client/inspector/animation/components/AnimatedPropertyListContainer.js b/devtools/client/inspector/animation/components/AnimatedPropertyListContainer.js
--- a/devtools/client/inspector/animation/components/AnimatedPropertyListContainer.js
+++ b/devtools/client/inspector/animation/components/AnimatedPropertyListContainer.js
@@ -24,17 +24,17 @@ class AnimatedPropertyListContainer exte
     const {
       animation,
       emitEventForTest,
       getAnimatedPropertyMap,
     } = this.props;
 
     return dom.div(
       {
-        className: "animated-property-list-container"
+        className: `animated-property-list-container ${ animation.state.type }`
       },
       AnimatedPropertyListHeader(),
       AnimatedPropertyList(
         {
           animation,
           emitEventForTest,
           getAnimatedPropertyMap,
         }
diff --git a/devtools/client/inspector/animation/components/AnimatedPropertyName.js b/devtools/client/inspector/animation/components/AnimatedPropertyName.js
--- a/devtools/client/inspector/animation/components/AnimatedPropertyName.js
+++ b/devtools/client/inspector/animation/components/AnimatedPropertyName.js
@@ -7,25 +7,30 @@
 const { PureComponent } = require("devtools/client/shared/vendor/react");
 const dom = require("devtools/client/shared/vendor/react-dom-factories");
 const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
 
 class AnimatedPropertyName extends PureComponent {
   static get propTypes() {
     return {
       property: PropTypes.string.isRequired,
+      state: PropTypes.oneOfType([null, PropTypes.object]).isRequired,
     };
   }
 
   render() {
-    const { property } = this.props;
+    const {
+      property,
+      state,
+    } = this.props;
 
     return dom.div(
       {
-        className: "animated-property-name",
+        className: "animated-property-name" +
+                   (state && state.runningOnCompositor ? " compositor" : ""),
       },
       dom.span(
         {},
         property
       )
     );
   }
 }
diff --git a/devtools/client/themes/animation.css b/devtools/client/themes/animation.css
--- a/devtools/client/themes/animation.css
+++ b/devtools/client/themes/animation.css
@@ -2,18 +2,25 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* Animation-inspector specific theme variables */
 
 :root {
   --animation-even-background-color: rgba(0, 0, 0, 0.05);
   --command-pick-image: url(chrome://devtools/skin/images/command-pick.svg);
+  --fast-track-image: url("images/animation-fast-track.svg");
+  --fill-color-cssanimation: var(--theme-contrast-background);
+  --fill-color-csstransition: var(--theme-highlight-blue);
+  --fill-color-scriptanimation: var(--theme-graphs-green);
   --graph-right-offset: 10px;
   --sidebar-width: 200px;
+  --stroke-color-cssanimation: var(--theme-highlight-lightorange);
+  --stroke-color-csstransition: var(--theme-highlight-bluegrey);
+  --stroke-color-scriptanimation: var(--theme-highlight-green);
   --tick-line-style: 0.5px solid rgba(128, 136, 144, 0.5);
 }
 
 :root.theme-dark {
   --animation-even-background-color: rgba(255, 255, 255, 0.05);
 }
 
 :root.theme-firebug {
@@ -77,28 +84,28 @@
   height: 30px;
 }
 
 .animation-item:nth-child(2n+1) {
   background-color: var(--animation-even-background-color);
 }
 
 .animation-item.cssanimation {
-  --computed-timing-graph-color: var(--theme-contrast-background);
-  --effect-timing-graph-color: var(--theme-highlight-lightorange);
+  --computed-timing-graph-color: var(--fill-color-cssanimation);
+  --effect-timing-graph-color: var(--stroke-color-cssanimation);
 }
 
 .animation-item.csstransition {
-  --computed-timing-graph-color: var(--theme-highlight-blue);
-  --effect-timing-graph-color: var(--theme-highlight-bluegrey);
+  --computed-timing-graph-color: var(--fill-color-csstransition);
+  --effect-timing-graph-color: var(--stroke-color-csstransition);
 }
 
 .animation-item.scriptanimation {
-  --computed-timing-graph-color: var(--theme-graphs-green);
-  --effect-timing-graph-color: var(--theme-highlight-green);
+  --computed-timing-graph-color: var(--fill-color-scriptanimation);
+  --effect-timing-graph-color: var(--stroke-color-scriptanimation);
 }
 
 .animation-item.selected {
   background-color: var(--theme-selection-background-hover);
 }
 
 /* Animation Target */
 .animation-target {
@@ -117,17 +124,17 @@
 .animation-summary-graph {
   height: 100%;
   padding-top: 5px;
   position: relative;
   width: calc(100% - var(--sidebar-width) - var(--graph-right-offset));
 }
 
 .animation-summary-graph.compositor::after {
-  background-image: url("images/animation-fast-track.svg");
+  background-image: var(--fast-track-image);
   background-repeat: no-repeat;
   content: "";
   display: block;
   fill: var(--theme-content-color3);
   height: 100%;
   position: absolute;
   right: 0;
   top: 5px;
@@ -310,16 +317,46 @@
   align-items: center;
   display: flex;
   height: 100%;
   justify-content: flex-end;
   padding-right: 10px;
   width: var(--sidebar-width);
 }
 
+.animated-property-name.compositor span {
+  padding-left: 15px;
+  position: relative;
+}
+
+.animated-property-list-container.cssanimation .animated-property-name.compositor {
+  --fast-track-color: var(--stroke-color-cssanimation);
+}
+
+.animated-property-list-container.csstransition .animated-property-name.compositor {
+  --fast-track-color: var(--stroke-color-csstransition);
+}
+
+.animated-property-list-container.scriptanimation .animated-property-name.compositor {
+  --fast-track-color: var(--stroke-color-scriptanimation);
+}
+
+.animated-property-name.compositor span::before {
+  background-image: var(--fast-track-image);
+  background-repeat: no-repeat;
+  background-size: contain;
+  content: "";
+  fill: var(--fast-track-color);
+  height: 100%;
+  position: absolute;
+  left: 0;
+  width: 15px;
+  -moz-context-properties: fill;
+}
+
 /* No Animation Panel */
 .animation-error-message {
   overflow: auto;
 }
 
 .animation-error-message > p {
   white-space: pre;
 }
