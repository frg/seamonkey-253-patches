# HG changeset patch
# User Valentin Gosu <valentin.gosu@gmail.com>
# Date 1595187605 0
# Node ID 09c4323949a5b30dd482faf03dd121f5ef2d527d
# Parent  40f73c39b9fcebb8d23bfa496c1d2fd31298703f
Bug 1653201 - Remove internal nsStandardURL punycode pref r=Gijs

Differential Revision: https://phabricator.services.mozilla.com/D84105

diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -2289,20 +2289,16 @@ pref("network.dns.localDomains", "");
 pref("network.dns.forceResolve", "");
 
 // Contols whether or not "localhost" should resolve when offline
 pref("network.dns.offline-localhost", true);
 
 // The maximum allowed length for a URL - 1MB default
 pref("network.standard-url.max-length", 1048576);
 
-// Whether nsIURI.host/.hostname/.spec should return a punycode string
-// If set to false we will revert to previous behaviour and return a unicode string.
-pref("network.standard-url.punycode-host", true);
-
 // Idle timeout for ftp control connections - 5 minute default
 pref("network.ftp.idleConnectionTimeout", 300);
 
 // enables the prefetch service (i.e., prefetching of <link rel="next"> and
 // <link rel="prefetch"> URLs).
 pref("network.prefetch-next", true);
 // enables the preloading (i.e., preloading of <link rel="preload"> URLs).
 pref("network.preload", false);
diff --git a/netwerk/base/nsStandardURL.cpp b/netwerk/base/nsStandardURL.cpp
--- a/netwerk/base/nsStandardURL.cpp
+++ b/netwerk/base/nsStandardURL.cpp
@@ -54,17 +54,16 @@ static NS_DEFINE_CID(kStandardURLCID, NS
 nsIIDNService *nsStandardURL::gIDN = nullptr;
 
 // This value will only be updated on the main thread once. Worker threads
 // may race when reading this values, but that's OK because in the worst
 // case we will just dispatch a noop runnable to the main thread.
 bool nsStandardURL::gInitialized = false;
 
 const char nsStandardURL::gHostLimitDigits[] = { '/', '\\', '?', '#', 0 };
-bool nsStandardURL::gPunycodeHost = true;
 
 // Invalid host characters
 // We still allow % because it is in the ID of addons.
 // Any percent encoded ASCII characters that are not allowed in the
 // hostname are not percent decoded, and will be parsed just fine.
 //
 // Note that the array below will be initialized at compile time,
 // so we do not need to "optimize" TestForInvalidHostCharacters.
@@ -274,17 +273,16 @@ nsStandardURL::InitGlobalObjects()
     gInitialized = true;
 
     nsCOMPtr<nsIPrefBranch> prefBranch( do_GetService(NS_PREFSERVICE_CONTRACTID) );
     if (prefBranch) {
         nsCOMPtr<nsIObserver> obs( new nsPrefObserver() );
         PrefsChanged(prefBranch, nullptr);
     }
 
-    Preferences::AddBoolVarCache(&gPunycodeHost, "network.standard-url.punycode-host", true);
     nsCOMPtr<nsIIDNService> serv(do_GetService(NS_IDNSERVICE_CONTRACTID));
     if (serv) {
         NS_ADDREF(gIDN = serv.get());
         MOZ_ASSERT(gIDN);
     }
 }
 
 void
@@ -1247,23 +1245,18 @@ NS_INTERFACE_MAP_END
 //----------------------------------------------------------------------------
 
 // result may contain unescaped UTF-8 characters
 NS_IMETHODIMP
 nsStandardURL::GetSpec(nsACString &result)
 {
     MOZ_ASSERT(mSpec.Length() <= (uint32_t) net_GetURLMaxLength(),
                "The spec should never be this long, we missed a check.");
-    nsresult rv = NS_OK;
-    if (gPunycodeHost) {
-        result = mSpec;
-    } else { // XXX: This code path may be slow
-        rv = GetDisplaySpec(result);
-    }
-    return rv;
+    result = mSpec;
+    return NS_OK;
 }
 
 // result may contain unescaped UTF-8 characters
 NS_IMETHODIMP
 nsStandardURL::GetSensitiveInfoHiddenSpec(nsACString &result)
 {
     nsresult rv = GetSpec(result);
     if (NS_FAILED(rv)) {
@@ -1281,23 +1274,18 @@ nsStandardURL::GetSpecIgnoringRef(nsACSt
 {
     // URI without ref is 0 to one char before ref
     if (mRef.mLen < 0) {
         return GetSpec(result);
     }
 
     URLSegment noRef(0, mRef.mPos - 1);
     result = Segment(noRef);
-
     CheckIfHostIsAscii();
     MOZ_ASSERT(mCheckedIfHostA);
-    if (!gPunycodeHost && !mDisplayHost.IsEmpty()) {
-        result.Replace(mHost.mPos, mHost.mLen, mDisplayHost);
-    }
-
     return NS_OK;
 }
 
 nsresult
 nsStandardURL::CheckIfHostIsAscii()
 {
     nsresult rv;
     if (mCheckedIfHostA) {
@@ -1380,19 +1368,16 @@ nsStandardURL::GetDisplayHost(nsACString
 
 // result may contain unescaped UTF-8 characters
 NS_IMETHODIMP
 nsStandardURL::GetPrePath(nsACString &result)
 {
     result = Prepath();
     CheckIfHostIsAscii();
     MOZ_ASSERT(mCheckedIfHostA);
-    if (!gPunycodeHost && !mDisplayHost.IsEmpty()) {
-        result.Replace(mHost.mPos, mHost.mLen, mDisplayHost);
-    }
     return NS_OK;
 }
 
 // result may contain unescaped UTF-8 characters
 NS_IMETHODIMP
 nsStandardURL::GetDisplayPrePath(nsACString &result)
 {
     result = Prepath();
@@ -1434,36 +1419,21 @@ nsStandardURL::GetPassword(nsACString &r
 {
     result = Password();
     return NS_OK;
 }
 
 NS_IMETHODIMP
 nsStandardURL::GetHostPort(nsACString &result)
 {
-    nsresult rv;
-    if (gPunycodeHost) {
-        rv = GetAsciiHostPort(result);
-    } else {
-        rv = GetDisplayHostPort(result);
-    }
-    return rv;
+    return GetAsciiHostPort(result);
 }
 
 NS_IMETHODIMP
-nsStandardURL::GetHost(nsACString &result)
-{
-    nsresult rv;
-    if (gPunycodeHost) {
-        rv = GetAsciiHost(result);
-    } else {
-        rv = GetDisplayHost(result);
-    }
-    return rv;
-}
+nsStandardURL::GetHost(nsACString &result) { return GetAsciiHost(result); }
 
 NS_IMETHODIMP
 nsStandardURL::GetPort(int32_t *result)
 {
     // should never be more than 16 bit
     MOZ_ASSERT(mPort <= std::numeric_limits<uint16_t>::max());
     *result = mPort;
     return NS_OK;
diff --git a/netwerk/base/nsStandardURL.h b/netwerk/base/nsStandardURL.h
--- a/netwerk/base/nsStandardURL.h
+++ b/netwerk/base/nsStandardURL.h
@@ -300,17 +300,16 @@ private:
                                    // mDisplayHost has a been initialized, or
                                    // that the hostname is not punycode
 
     // global objects.  don't use COMPtr as its destructor will cause a
     // coredump if we leak it.
     static nsIIDNService               *gIDN;
     static const char                   gHostLimitDigits[];
     static bool                         gInitialized;
-    static bool                         gPunycodeHost;
 
 public:
 #ifdef DEBUG_DUMP_URLS_AT_SHUTDOWN
     void PrintSpec() const { printf("  %s\n", mSpec.get()); }
 #endif
 
 public:
 
diff --git a/netwerk/test/unit/test_standardurl.js b/netwerk/test/unit/test_standardurl.js
--- a/netwerk/test/unit/test_standardurl.js
+++ b/netwerk/test/unit/test_standardurl.js
@@ -519,52 +519,17 @@ add_test(function test_emptyPassword() {
 });
 
 registerCleanupFunction(function () {
   gPrefs.clearUserPref("network.standard-url.punycode-host");
 });
 
 add_test(function test_idna_host() {
   // See bug 945240 - this test makes sure that URLs return a punycode hostname
-  // when the pref is set, or unicode otherwise.
-
-  // First we test that the old behaviour still works properly for all methods
-  // that return strings containing the hostname
-
-  gPrefs.setBoolPref("network.standard-url.punycode-host", false);
   let url = stringToURL("http://user:password@ält.example.org:8080/path?query#etc");
-
-  equal(url.host, "ält.example.org");
-  equal(url.hostPort, "ält.example.org:8080");
-  equal(url.prePath, "http://user:password@ält.example.org:8080");
-  equal(url.spec, "http://user:password@ält.example.org:8080/path?query#etc");
-  equal(url.specIgnoringRef, "http://user:password@ält.example.org:8080/path?query");
-  equal(url.QueryInterface(Ci.nsISensitiveInfoHiddenURI).getSensitiveInfoHiddenSpec(), "http://user:****@ält.example.org:8080/path?query#etc");
-
-  equal(url.displayHost, "ält.example.org");
-  equal(url.displayHostPort, "ält.example.org:8080");
-  equal(url.displaySpec, "http://user:password@ält.example.org:8080/path?query#etc");
-
-  equal(url.asciiHost, "xn--lt-uia.example.org");
-  equal(url.asciiHostPort, "xn--lt-uia.example.org:8080");
-  equal(url.asciiSpec, "http://user:password@xn--lt-uia.example.org:8080/path?query#etc");
-
-  url.ref = ""; // SetRef calls InvalidateCache()
-  equal(url.spec, "http://user:password@ält.example.org:8080/path?query");
-  equal(url.displaySpec, "http://user:password@ält.example.org:8080/path?query");
-  equal(url.asciiSpec, "http://user:password@xn--lt-uia.example.org:8080/path?query");
-
-  url = stringToURL("http://user:password@www.ält.com:8080/path?query#etc");
-  url.ref = "";
-  equal(url.spec, "http://user:password@www.ält.com:8080/path?query");
-
-  // We also check that the default behaviour changes once we filp the pref
-  gPrefs.setBoolPref("network.standard-url.punycode-host", true);
-
-  url = stringToURL("http://user:password@ält.example.org:8080/path?query#etc");
   equal(url.host, "xn--lt-uia.example.org");
   equal(url.hostPort, "xn--lt-uia.example.org:8080");
   equal(url.prePath, "http://user:password@xn--lt-uia.example.org:8080");
   equal(url.spec, "http://user:password@xn--lt-uia.example.org:8080/path?query#etc");
   equal(url.specIgnoringRef, "http://user:password@xn--lt-uia.example.org:8080/path?query");
   equal(url.QueryInterface(Ci.nsISensitiveInfoHiddenURI).getSensitiveInfoHiddenSpec(), "http://user:****@xn--lt-uia.example.org:8080/path?query#etc");
 
   equal(url.displayHost, "ält.example.org");
