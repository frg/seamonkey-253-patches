# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1521141910 14400
# Node ID e6393fca03cb84e30d001119c9f134d4c3add50b
# Parent  e99ba1f1b40ff15d10392179c3c45d5ec11a7b67
Bug 1445662 - Assert that IAPZCTreeManager's helper methods are always on the controller thread. r=rhunt

These two functions (UpdateWheelTransaction and ProcessUnhandledEvent)
are only ever called on the concrete APZCTreeManager when the APZ code
is living in the GPU process. This is because the calls are made by the
IAPZCTreeManager implementation which lives in the UI process, and
remoted over PAPZCTreeManager. So the assertion is safe, and will help
us guard against inadvertent breakage when we try making a different
thread the controller thread in the GPU process.

In addition, the WillHandleInput function can be called in the GPU
process on the compositor thread, but we will allow it to be called on
the main thread as well. In that case we need to ensure we don't try
running EventStateManager pref-reading code in the GPU process, and
instead preserve the current behaviour of just returning true.

diff --git a/gfx/layers/apz/src/APZCTreeManager.cpp b/gfx/layers/apz/src/APZCTreeManager.cpp
--- a/gfx/layers/apz/src/APZCTreeManager.cpp
+++ b/gfx/layers/apz/src/APZCTreeManager.cpp
@@ -1059,17 +1059,17 @@ APZCTreeManager::PrepareNodeForLayer(con
   node->SetFixedPosData(aLayer.GetFixedPositionScrollContainerId());
   return node;
 }
 
 template<typename PanGestureOrScrollWheelInput>
 static bool
 WillHandleInput(const PanGestureOrScrollWheelInput& aPanInput)
 {
-  if (!NS_IsMainThread()) {
+  if (!XRE_IsParentProcess() || !NS_IsMainThread()) {
     return true;
   }
 
   WidgetWheelEvent wheelEvent = aPanInput.ToWidgetWheelEvent(nullptr);
   return IAPZCTreeManager::WillHandleWheelEvent(&wheelEvent);
 }
 
 void
@@ -1790,16 +1790,18 @@ APZCTreeManager::SetupScrollbarDrag(Mous
                          *thumbData.mDirection));
   }
 }
 
 void
 APZCTreeManager::UpdateWheelTransaction(LayoutDeviceIntPoint aRefPoint,
                                         EventMessage aEventMessage)
 {
+  APZThreadUtils::AssertOnControllerThread();
+
   WheelBlockState* txn = mInputQueue->GetActiveWheelTransaction();
   if (!txn) {
     return;
   }
 
   // If the transaction has simply timed out, we don't need to do anything
   // else.
   if (txn->MaybeTimeout(TimeStamp::Now())) {
@@ -1835,16 +1837,18 @@ APZCTreeManager::UpdateWheelTransaction(
   }
 }
 
 void
 APZCTreeManager::ProcessUnhandledEvent(LayoutDeviceIntPoint* aRefPoint,
                                         ScrollableLayerGuid*  aOutTargetGuid,
                                         uint64_t*             aOutFocusSequenceNumber)
 {
+  APZThreadUtils::AssertOnControllerThread();
+
   // Transform the aRefPoint.
   // If the event hits an overscrolled APZC, instruct the caller to ignore it.
   CompositorHitTestInfo hitResult = CompositorHitTestInfo::eInvisibleToHitTest;
   PixelCastJustification LDIsScreen = PixelCastJustification::LayoutDeviceIsScreenForUntransformedEvent;
   ScreenIntPoint refPointAsScreen =
     ViewAs<ScreenPixel>(*aRefPoint, LDIsScreen);
   RefPtr<AsyncPanZoomController> apzc = GetTargetAPZC(refPointAsScreen, &hitResult);
   if (apzc) {
