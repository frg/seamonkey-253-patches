# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1520027118 18000
# Node ID 02bf3c12111caa986bfaec1f2c6a50381492698e
# Parent  bc078c7a560daeb85bd3ece7fb6ef8147f844d9a
Bug 1442608 - Fix rendering failure with WebRender on Windows. r=nical

This works around a rendering failure on Windows where the browser window opens
and remains white. Details of why this happens can be found in bug 1442748; this
workaround just forces a scene build if we get a GenerateFrame request after
having the root pipeline id set on the pending scene, but if it hasn't yet
been propagated to the current scene. This ensures the render isn't skipped
and prevents the C++-side frame throttler from preventing future composites.

diff --git a/gfx/webrender/src/render_backend.rs b/gfx/webrender/src/render_backend.rs
--- a/gfx/webrender/src/render_backend.rs
+++ b/gfx/webrender/src/render_backend.rs
@@ -934,17 +934,29 @@ impl RenderBackend {
             return;
         }
 
         self.resource_cache.update_resources(
             transaction_msg.resource_updates,
             &mut profile_counters.resources,
         );
 
-        if op.build {
+        // If we get a generate_frame message after getting a root pipeline set,
+        // but that pipeline id hasn't been propagated to the current scene, then
+        // we should force that to happen. Otherwise we will skip a render that
+        // the caller is expecting to happen, and in Gecko's case, that will
+        // leave it wedged permanently.
+        let force_build = if transaction_msg.generate_frame {
+            let doc = self.documents.get_mut(&document_id).unwrap();
+            doc.pending.scene.root_pipeline_id.is_some() && !doc.current.scene.root_pipeline_id.is_some()
+        } else {
+            false
+        };
+
+        if op.build || force_build {
             let doc = self.documents.get_mut(&document_id).unwrap();
             let _timer = profile_counters.total_time.timer();
             profile_scope!("build scene");
 
             doc.build_scene(&mut self.resource_cache);
             doc.render_on_hittest = true;
         }
 
