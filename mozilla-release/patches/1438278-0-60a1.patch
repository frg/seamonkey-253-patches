# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1518645401 21600
# Node ID 02d24f7856d50c678016cd33292882217ce29f36
# Parent  79c9d52886f396beab696cf721207a6c5dec01f1
Bug 1438278 - Part 0: Fix places where bug 1429206 left old filenames in comments. r=jandem.

diff --git a/js/src/jit/BaselineFrame.h b/js/src/jit/BaselineFrame.h
--- a/js/src/jit/BaselineFrame.h
+++ b/js/src/jit/BaselineFrame.h
@@ -39,18 +39,18 @@ class BaselineFrame
         // Frame has an arguments object, argsObj_.
         HAS_ARGS_OBJ     = 1 << 4,
 
         // See InterpreterFrame::PREV_UP_TO_DATE.
         PREV_UP_TO_DATE  = 1 << 5,
 
         // Frame has execution observed by a Debugger.
         //
-        // See comment above 'isDebuggee' in jscompartment.h for explanation of
-        // invariants of debuggee compartments, scripts, and frames.
+        // See comment above 'isDebuggee' in vm/JSCompartment.h for explanation
+        // of invariants of debuggee compartments, scripts, and frames.
         DEBUGGEE         = 1 << 6,
 
         // (1 << 7 and 1 << 8 are unused)
 
         // Frame has over-recursed on an early check.
         OVER_RECURSED    = 1 << 9,
 
         // Frame has a BaselineRecompileInfo stashed in the scratch value
diff --git a/js/src/jsfriendapi.h b/js/src/jsfriendapi.h
--- a/js/src/jsfriendapi.h
+++ b/js/src/jsfriendapi.h
@@ -635,17 +635,17 @@ struct String
         char16_t inlineStorageTwoByte[1];
     };
     const JSStringFinalizer* externalFinalizer;
 };
 
 } /* namespace shadow */
 
 // This is equal to |&JSObject::class_|.  Use it in places where you don't want
-// to #include jsobj.h.
+// to #include vm/JSObject.h.
 extern JS_FRIEND_DATA(const js::Class* const) ObjectClassPtr;
 
 inline const js::Class*
 GetObjectClass(const JSObject* obj)
 {
     return reinterpret_cast<const shadow::Object*>(obj)->group->clasp;
 }
 
@@ -1383,32 +1383,28 @@ GetXrayJitInfo();
 extern JS_FRIEND_API(bool)
 DateIsValid(JSContext* cx, JS::HandleObject obj, bool* isValid);
 
 extern JS_FRIEND_API(bool)
 DateGetMsecSinceEpoch(JSContext* cx, JS::HandleObject obj, double* msecSinceEpoch);
 
 } /* namespace js */
 
-/* Implemented in jscntxt.cpp. */
-
-/**
- * Report an exception, which is currently realized as a printf-style format
- * string and its arguments.
- */
 typedef enum JSErrNum {
 #define MSG_DEF(name, count, exception, format) \
     name,
 #include "js.msg"
 #undef MSG_DEF
     JSErr_Limit
 } JSErrNum;
 
 namespace js {
 
+/* Implemented in vm/JSContext.cpp. */
+
 extern JS_FRIEND_API(const JSErrorFormatString*)
 GetErrorMessage(void* userRef, const unsigned errorNumber);
 
 // AutoStableStringChars is here so we can use it in ErrorReport.  It
 // should get moved out of here if we can manage it.  See bug 1040316.
 
 /**
  * This class provides safe access to a string's chars across a GC. Once
@@ -2605,17 +2601,17 @@ namespace js {
 
 static MOZ_ALWAYS_INLINE shadow::Function*
 FunctionObjectToShadowFunction(JSObject* fun)
 {
     MOZ_ASSERT(GetObjectClass(fun) == FunctionClassPtr);
     return reinterpret_cast<shadow::Function*>(fun);
 }
 
-/* Statically asserted in jsfun.h. */
+/* Statically asserted in JSFunction.h. */
 static const unsigned JS_FUNCTION_INTERPRETED_BITS = 0x0201;
 
 // Return whether the given function object is native.
 static MOZ_ALWAYS_INLINE bool
 FunctionObjectIsNative(JSObject* fun)
 {
     return !(FunctionObjectToShadowFunction(fun)->flags & JS_FUNCTION_INTERPRETED_BITS);
 }
diff --git a/js/src/vm/GlobalObject.h b/js/src/vm/GlobalObject.h
--- a/js/src/vm/GlobalObject.h
+++ b/js/src/vm/GlobalObject.h
@@ -900,17 +900,17 @@ DefineToStringTag(JSContext *cx, HandleO
  * for ClassSpecs.
  */
 
 template<JSNative ctor, unsigned length, gc::AllocKind kind, const JSJitInfo* jitInfo = nullptr>
 JSObject*
 GenericCreateConstructor(JSContext* cx, JSProtoKey key)
 {
     // Note - We duplicate the trick from ClassName() so that we don't need to
-    // include jsatominlines.h here.
+    // include vm/JSAtom-inl.h here.
     PropertyName* name = (&cx->names().Null)[key];
     return GlobalObject::createConstructor(cx, ctor, name, length, kind, jitInfo);
 }
 
 inline JSObject*
 GenericCreatePrototype(JSContext* cx, JSProtoKey key)
 {
     MOZ_ASSERT(key != JSProto_Object);
diff --git a/js/src/vm/JSContext.h b/js/src/vm/JSContext.h
--- a/js/src/vm/JSContext.h
+++ b/js/src/vm/JSContext.h
@@ -1012,16 +1012,20 @@ enum ErrorArgumentsType {
  * Loads and returns a self-hosted function by name. For performance, define
  * the property name in vm/CommonPropertyNames.h.
  *
  * Defined in SelfHosting.cpp.
  */
 JSFunction*
 SelfHostedFunction(JSContext* cx, HandlePropertyName propName);
 
+/**
+ * Report an exception, using printf-style APIs to generate the error
+ * message.
+ */
 #ifdef va_start
 extern bool
 ReportErrorVA(JSContext* cx, unsigned flags, const char* format,
               ErrorArgumentsType argumentsType, va_list ap) MOZ_FORMAT_PRINTF(3, 0);
 
 extern bool
 ReportErrorNumberVA(JSContext* cx, unsigned flags, JSErrorCallback callback,
                     void* userRef, const unsigned errorNumber,
diff --git a/js/src/vm/JSScript.h b/js/src/vm/JSScript.h
--- a/js/src/vm/JSScript.h
+++ b/js/src/vm/JSScript.h
@@ -1975,17 +1975,17 @@ class JSScript : public js::gc::TenuredC
     js::DebugScript* debugScript();
     js::DebugScript* releaseDebugScript();
     void destroyDebugScript(js::FreeOp* fop);
 
   public:
     bool hasBreakpointsAt(jsbytecode* pc);
     bool hasAnyBreakpointsOrStepMode() { return hasDebugScript_; }
 
-    // See comment above 'debugMode' in jscompartment.h for explanation of
+    // See comment above 'debugMode' in JSCompartment.h for explanation of
     // invariants of debuggee compartments, scripts, and frames.
     inline bool isDebuggee() const;
 
     js::BreakpointSite* getBreakpointSite(jsbytecode* pc)
     {
         return hasDebugScript_ ? debugScript()->breakpoints[pcToOffset(pc)] : nullptr;
     }
 
diff --git a/js/src/vm/NativeObject.h b/js/src/vm/NativeObject.h
--- a/js/src/vm/NativeObject.h
+++ b/js/src/vm/NativeObject.h
@@ -1567,17 +1567,17 @@ MaybeNativeObject(JSObject* obj)
 bool IsPackedArray(JSObject* obj);
 
 extern void
 AddPropertyTypesAfterProtoChange(JSContext* cx, NativeObject* obj, ObjectGroup* oldGroup);
 
 } // namespace js
 
 
-/*** Inline functions declared in jsobj.h that use the native declarations above *****************/
+/*** Inline functions declared in JSObject.h that use the native declarations above **************/
 
 inline bool
 js::HasProperty(JSContext* cx, HandleObject obj, HandleId id, bool* foundp)
 {
     if (HasPropertyOp op = obj->getOpsHasProperty())
         return op(cx, obj, id, foundp);
     return NativeHasProperty(cx, obj.as<NativeObject>(), id, foundp);
 }
diff --git a/js/src/vm/Stack.h b/js/src/vm/Stack.h
--- a/js/src/vm/Stack.h
+++ b/js/src/vm/Stack.h
@@ -318,17 +318,17 @@ class InterpreterFrame
 
         /* Lazy frame initialization */
         HAS_RVAL               =       0x10,  /* frame has rval_ set */
 
         /* Debugger state */
         PREV_UP_TO_DATE        =       0x20,  /* see DebugScopes::updateLiveScopes */
 
         /*
-         * See comment above 'isDebuggee' in jscompartment.h for explanation of
+         * See comment above 'isDebuggee' in JSCompartment.h for explanation of
          * invariants of debuggee compartments, scripts, and frames.
          */
         DEBUGGEE               =       0x40,  /* Execution is being observed by Debugger */
 
         /* Used in tracking calls and profiling (see vm/GeckoProfiler.cpp) */
         HAS_PUSHED_PROF_FRAME  =       0x80,  /* Gecko Profiler was notified of entry */
 
         /*
