# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1598894027 0
# Node ID 2c928095568736985b097cc0af9a14575d3aa1b0
# Parent  18f954db1706f01be0e3e6cad0a73e8bfe3143b0
Bug 1661790 - Allow configuring the name of the `virtualenv` used for builds r=nalexander

We don't anticipate end users will actually care to do this, but it's useful especially for unit tests. For example, after bug 1659539, Python `configure` tests will run in a new, non-`init_py3` `virtualenv`, and we'll want to target that `virtualenv` for `configure` rather than having it create a new `virtualenv` for no reason.

Differential Revision: https://phabricator.services.mozilla.com/D88661

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -231,30 +231,34 @@ def help_shell(help, shell):
 shell = help_shell | shell
 
 
 # Python 3
 # ========
 
 option(env='PYTHON3', nargs=1, help='Python 3 interpreter (3.6 or later)')
 
+option(env='VIRTUALENV_NAME', nargs=1, default='init_py3',
+       help='Name of the in-objdir virtualenv')
 
-@depends('PYTHON3', check_build_environment, mozconfig, '--help')
+
+@depends('PYTHON3', 'VIRTUALENV_NAME', check_build_environment, mozconfig,
+         '--help')
 @imports(_from='__builtin__', _import='Exception')
 @imports('os')
 @imports('sys')
 @imports('subprocess')
 @imports('distutils.sysconfig')
 @imports(_from='mozbuild.configure.util', _import='LineIO')
 @imports(_from='mozbuild.virtualenv', _import='VirtualenvManager')
 @imports(_from='mozbuild.virtualenv', _import='verify_python_version')
 @imports(_from='mozbuild.pythonutil', _import='find_python3_executable')
 @imports(_from='mozbuild.pythonutil', _import='python_executable_version')
 @imports(_from='six', _import='ensure_text')
-def virtualenv_python3(env_python, build_env, mozconfig, help):
+def virtualenv_python3(env_python, virtualenv_name, build_env, mozconfig, help):
     # Avoid re-executing python when running configure --help.
     if help:
         return
 
     # NOTE: We cannot assume the Python we are calling this code with is the
     # Python we want to set up a virtualenv for.
     #
     # We also cannot assume that the Python the caller is configuring meets our
@@ -262,16 +266,17 @@ def virtualenv_python3(env_python, build
     #
     # Because of this the code is written to re-execute itself with the correct
     # interpreter if required.
 
     log.debug("python3: running with pid %r" % os.getpid())
     log.debug("python3: sys.executable: %r" % sys.executable)
 
     python = env_python[0] if env_python else None
+    virtualenv_name = virtualenv_name[0]
 
     # Did our python come from mozconfig? Overrides environment setting.
     # Ideally we'd rely on the mozconfig injection from mozconfig_options,
     # but we'd rather avoid the verbosity when we need to reexecute with
     # a different python.
     if mozconfig['path']:
         if 'PYTHON3' in mozconfig['env']['added']:
             python = mozconfig['env']['added']['PYTHON3']
@@ -297,17 +302,17 @@ def virtualenv_python3(env_python, build
     topsrcdir, topobjdir = build_env.topsrcdir, build_env.topobjdir
     if topobjdir.endswith('/js/src'):
         topobjdir = topobjdir[:-7]
 
     virtualenvs_root = os.path.join(topobjdir, '_virtualenvs')
     with LineIO(lambda l: log.info(l), 'replace') as out:
         manager = VirtualenvManager(
             topsrcdir,
-            os.path.join(virtualenvs_root, 'init_py3'), out,
+            os.path.join(virtualenvs_root, virtualenv_name), out,
             os.path.join(topsrcdir, 'build', 'build_virtualenv_packages.txt'))
 
     # If we're not in the virtualenv, we need to update the path to include some
     # necessary modules for find_program.
     try:
         import mozfile
     except ImportError:
         sys.path.insert(
