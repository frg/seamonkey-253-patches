# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1517495369 -3600
# Node ID a806fd1e54279d82f19462665955c830c14a3b08
# Parent  9dbaa674b27a4c3944637ec5ecea9502aa742fa1
Bug 1434263 - Refactor JSString to have an IsLinear flag instead of IsFlat. r=luke

diff --git a/js/src/jit/CodeGenerator.cpp b/js/src/jit/CodeGenerator.cpp
--- a/js/src/jit/CodeGenerator.cpp
+++ b/js/src/jit/CodeGenerator.cpp
@@ -1614,20 +1614,19 @@ CreateDependentString::generate(MacroAss
             masm.computeEffectiveAddress(BaseIndex(temp1, temp2, TimesTwo), temp1);
         masm.storePtr(temp1, Address(string, JSString::offsetOfNonInlineChars()));
         masm.storePtr(base, Address(string, JSDependentString::offsetOfBase()));
 
         // Follow any base pointer if the input is itself a dependent string.
         // Watch for undepended strings, which have a base pointer but don't
         // actually share their characters with it.
         Label noBase;
-        masm.branchTest32(Assembler::Zero, Address(base, JSString::offsetOfFlags()),
-                          Imm32(JSString::HAS_BASE_BIT), &noBase);
-        masm.branchTest32(Assembler::NonZero, Address(base, JSString::offsetOfFlags()),
-                          Imm32(JSString::FLAT_BIT), &noBase);
+        masm.load32(Address(base, JSString::offsetOfFlags()), temp1);
+        masm.and32(Imm32(JSString::TYPE_FLAGS_MASK), temp1);
+        masm.branch32(Assembler::NotEqual, temp1, Imm32(JSString::DEPENDENT_FLAGS), &noBase);
         masm.loadPtr(Address(base, JSDependentString::offsetOfBase()), temp1);
         masm.storePtr(temp1, Address(string, JSDependentString::offsetOfBase()));
         masm.bind(&noBase);
     }
 
     masm.bind(&done);
 }
 
@@ -8028,17 +8027,17 @@ JitCompartment::generateStringConcatStub
     masm.branch32(Assembler::Above, temp2, Imm32(JSString::MAX_LENGTH), &failure);
 
     // Allocate a new rope.
     masm.newGCString(output, temp3, &failure);
 
     // Store rope length and flags. temp1 still holds the result of AND'ing the
     // lhs and rhs flags, so we just have to clear the other flags to get our
     // rope flags (Latin1 if both lhs and rhs are Latin1).
-    static_assert(JSString::ROPE_FLAGS == 0, "Rope flags must be 0");
+    static_assert(JSString::INIT_ROPE_FLAGS == 0, "Rope type flags must be 0");
     masm.and32(Imm32(JSString::LATIN1_CHARS_BIT), temp1);
     masm.store32(temp1, Address(output, JSString::offsetOfFlags()));
     masm.store32(temp2, Address(output, JSString::offsetOfLength()));
 
     // Store left and right nodes.
     masm.storePtr(lhs, Address(output, JSRope::offsetOfLeft()));
     masm.storePtr(rhs, Address(output, JSRope::offsetOfRight()));
     masm.ret();
diff --git a/js/src/jit/MacroAssembler-inl.h b/js/src/jit/MacroAssembler-inl.h
--- a/js/src/jit/MacroAssembler-inl.h
+++ b/js/src/jit/MacroAssembler-inl.h
@@ -392,39 +392,36 @@ MacroAssembler::branchIfTrueBool(Registe
     // Note that C++ bool is only 1 byte, so ignore the higher-order bits.
     branchTest32(Assembler::NonZero, reg, Imm32(0xFF), label);
 }
 
 void
 MacroAssembler::branchIfRope(Register str, Label* label)
 {
     Address flags(str, JSString::offsetOfFlags());
-    static_assert(JSString::ROPE_FLAGS == 0, "Rope type flags must be 0");
-    branchTest32(Assembler::Zero, flags, Imm32(JSString::TYPE_FLAGS_MASK), label);
+    branchTest32(Assembler::Zero, flags, Imm32(JSString::LINEAR_BIT), label);
 }
 
 void
 MacroAssembler::branchIfRopeOrExternal(Register str, Register temp, Label* label)
 {
     Address flags(str, JSString::offsetOfFlags());
     move32(Imm32(JSString::TYPE_FLAGS_MASK), temp);
     and32(flags, temp);
 
-    static_assert(JSString::ROPE_FLAGS == 0, "Rope type flags must be 0");
-    branchTest32(Assembler::Zero, temp, temp, label);
+    branchTest32(Assembler::Zero, temp, Imm32(JSString::LINEAR_BIT), label);
 
     branch32(Assembler::Equal, temp, Imm32(JSString::EXTERNAL_FLAGS), label);
 }
 
 void
 MacroAssembler::branchIfNotRope(Register str, Label* label)
 {
     Address flags(str, JSString::offsetOfFlags());
-    static_assert(JSString::ROPE_FLAGS == 0, "Rope type flags must be 0");
-    branchTest32(Assembler::NonZero, flags, Imm32(JSString::TYPE_FLAGS_MASK), label);
+    branchTest32(Assembler::NonZero, flags, Imm32(JSString::LINEAR_BIT), label);
 }
 
 void
 MacroAssembler::branchLatin1String(Register string, Label* label)
 {
     branchTest32(Assembler::NonZero, Address(string, JSString::offsetOfFlags()),
                  Imm32(JSString::LATIN1_CHARS_BIT), label);
 }
diff --git a/js/src/jsfriendapi.h b/js/src/jsfriendapi.h
--- a/js/src/jsfriendapi.h
+++ b/js/src/jsfriendapi.h
@@ -617,20 +617,20 @@ struct Function {
     /* Used only for natives */
     JSNative native;
     const JSJitInfo* jitinfo;
     void* _1;
 };
 
 struct String
 {
+    static const uint32_t LINEAR_BIT       = JS_BIT(0);
     static const uint32_t INLINE_CHARS_BIT = JS_BIT(2);
     static const uint32_t LATIN1_CHARS_BIT = JS_BIT(6);
-    static const uint32_t ROPE_FLAGS       = 0;
-    static const uint32_t EXTERNAL_FLAGS   = JS_BIT(5);
+    static const uint32_t EXTERNAL_FLAGS   = JS_BIT(0) | JS_BIT(5);
     static const uint32_t TYPE_FLAGS_MASK  = JS_BIT(6) - 1;
     uint32_t flags;
     uint32_t length;
     union {
         const JS::Latin1Char* nonInlineCharsLatin1;
         const char16_t* nonInlineCharsTwoByte;
         JS::Latin1Char inlineStorageLatin1[1];
         char16_t inlineStorageTwoByte[1];
@@ -909,17 +909,17 @@ IsExternalString(JSString* str, const JS
 JS_FRIEND_API(JSLinearString*)
 StringToLinearStringSlow(JSContext* cx, JSString* str);
 
 MOZ_ALWAYS_INLINE JSLinearString*
 StringToLinearString(JSContext* cx, JSString* str)
 {
     using shadow::String;
     String* s = reinterpret_cast<String*>(str);
-    if (MOZ_UNLIKELY((s->flags & String::TYPE_FLAGS_MASK) == String::ROPE_FLAGS))
+    if (MOZ_UNLIKELY(!(s->flags & String::LINEAR_BIT)))
         return StringToLinearStringSlow(cx, str);
     return reinterpret_cast<JSLinearString*>(str);
 }
 
 template<typename CharType>
 MOZ_ALWAYS_INLINE void
 CopyLinearStringChars(CharType* dest, JSLinearString* s, size_t len, size_t start = 0);
 
diff --git a/js/src/vm/String-inl.h b/js/src/vm/String-inl.h
--- a/js/src/vm/String-inl.h
+++ b/js/src/vm/String-inl.h
@@ -104,17 +104,17 @@ JSString::validateLength(JSContext* mayb
 
     return true;
 }
 
 MOZ_ALWAYS_INLINE void
 JSRope::init(JSContext* cx, JSString* left, JSString* right, size_t length)
 {
     d.u1.length = length;
-    d.u1.flags = ROPE_FLAGS;
+    d.u1.flags = INIT_ROPE_FLAGS;
     if (left->hasLatin1Chars() && right->hasLatin1Chars())
         d.u1.flags |= LATIN1_CHARS_BIT;
     d.s.u2.left = left;
     d.s.u3.right = right;
     js::StringWriteBarrierPost(cx, &d.s.u2.left);
     js::StringWriteBarrierPost(cx, &d.s.u3.right);
 }
 
@@ -201,25 +201,25 @@ JSDependentString::new_(JSContext* cx, J
     str->init(cx, base, start, length);
     return str;
 }
 
 MOZ_ALWAYS_INLINE void
 JSFlatString::init(const char16_t* chars, size_t length)
 {
     d.u1.length = length;
-    d.u1.flags = FLAT_BIT;
+    d.u1.flags = LINEAR_BIT;
     d.s.u2.nonInlineCharsTwoByte = chars;
 }
 
 MOZ_ALWAYS_INLINE void
 JSFlatString::init(const JS::Latin1Char* chars, size_t length)
 {
     d.u1.length = length;
-    d.u1.flags = FLAT_BIT | LATIN1_CHARS_BIT;
+    d.u1.flags = LINEAR_BIT | LATIN1_CHARS_BIT;
     d.s.u2.nonInlineCharsLatin1 = chars;
 }
 
 template <js::AllowGC allowGC, typename CharT>
 MOZ_ALWAYS_INLINE JSFlatString*
 JSFlatString::new_(JSContext* cx, const CharT* chars, size_t length)
 {
     MOZ_ASSERT(chars[length] == CharT(0));
diff --git a/js/src/vm/String.cpp b/js/src/vm/String.cpp
--- a/js/src/vm/String.cpp
+++ b/js/src/vm/String.cpp
@@ -200,17 +200,17 @@ JSString::dumpRepresentation(js::Generic
 
 void
 JSString::dumpRepresentationHeader(js::GenericPrinter& out, int indent, const char* subclass) const
 {
     uint32_t flags = d.u1.flags;
     // Print the string's address as an actual C++ expression, to facilitate
     // copy-and-paste into a debugger.
     out.printf("((%s*) %p) length: %zu  flags: 0x%x", subclass, this, length(), flags);
-    if (flags & FLAT_BIT)               out.put(" FLAT");
+    if (flags & LINEAR_BIT)             out.put(" LINEAR");
     if (flags & HAS_BASE_BIT)           out.put(" HAS_BASE");
     if (flags & INLINE_CHARS_BIT)       out.put(" INLINE_CHARS");
     if (flags & ATOM_BIT)               out.put(" ATOM");
     if (isPermanentAtom())              out.put(" PERMANENT");
     if (flags & LATIN1_CHARS_BIT)       out.put(" LATIN1");
     if (flags & INDEX_VALUE_BIT)        out.put(" INDEX_VALUE(%u)", getIndexValue());
     out.putChar('\n');
 }
@@ -492,18 +492,20 @@ JSRope::flattenInternal(JSContext* maybe
             if (b == WithIncrementalBarrier) {
                 JSString::writeBarrierPre(str->d.s.u2.left);
                 JSString::writeBarrierPre(str->d.s.u3.right);
             }
             str->setNonInlineChars(left.nonInlineChars<CharT>(nogc));
             wholeCapacity = capacity;
             wholeChars = const_cast<CharT*>(left.nonInlineChars<CharT>(nogc));
             pos = wholeChars + left.d.u1.length;
-            JS_STATIC_ASSERT(!(EXTENSIBLE_FLAGS & DEPENDENT_FLAGS));
-            left.d.u1.flags ^= (EXTENSIBLE_FLAGS | DEPENDENT_FLAGS);
+            if (IsSame<CharT, char16_t>::value)
+                left.d.u1.flags = DEPENDENT_FLAGS;
+            else
+                left.d.u1.flags = DEPENDENT_FLAGS | LATIN1_CHARS_BIT;
             left.d.s.u3.base = (JSLinearString*)this;  /* will be true on exit */
             StringWriteBarrierPostRemove(maybecx, &left.d.s.u2.left);
             StringWriteBarrierPost(maybecx, (JSString**)&left.d.s.u3.base);
             goto visit_right_child;
         }
     }
 
     if (!AllocChars(this, wholeLength, &wholeChars, &wholeCapacity)) {
@@ -1097,17 +1099,17 @@ JSExternalString::ensureFlat(JSContext* 
         s[n] = '\0';
     }
 
     // Release the external chars.
     finalize(cx->runtime()->defaultFreeOp());
 
     // Transform the string into a non-external, flat string.
     setNonInlineChars<char16_t>(s);
-    d.u1.flags = FLAT_BIT;
+    d.u1.flags = LINEAR_BIT;
 
     return &this->asFlat();
 }
 
 #ifdef DEBUG
 void
 JSAtom::dump(js::GenericPrinter& out)
 {
diff --git a/js/src/vm/String.h b/js/src/vm/String.h
--- a/js/src/vm/String.h
+++ b/js/src/vm/String.h
@@ -215,66 +215,66 @@ class JSString : public js::gc::TenuredC
      * string instance of that type. Abstract types have no instances and thus
      * have no such entry. The "subtype predicate" entry for a type specifies
      * the predicate used to query whether a JSString instance is subtype
      * (reflexively) of that type.
      *
      *   String        Instance     Subtype
      *   type          encoding     predicate
      *   ------------------------------------
-     *   Rope          000000       000000
-     *   Linear        -           !000000
+     *   Rope          000000       xxxxx0
+     *   Linear        -            xxxxx1
      *   HasBase       -            xxxx1x
-     *   Dependent     000010       000010
-     *   External      100000       100000
-     *   Flat          -            xxxxx1
-     *   Undepended    000011       000011
+     *   Dependent     000011       000011
+     *   External      100001       100001
+     *   Flat          -            Linear && !Dependent && !External
+     *   Undepended    010011       010011
      *   Extensible    010001       010001
      *   Inline        000101       xxx1xx
      *   FatInline     010101       x1x1xx
      *   Atom          001001       xx1xxx
      *   PermanentAtom 101001       1x1xxx
      *   InlineAtom    -            xx11xx
      *   FatInlineAtom -            x111xx
      *
      * Note that the first 4 flag bits (from right to left in the previous table)
      * have the following meaning and can be used for some hot queries:
      *
-     *   Bit 0: IsFlat
+     *   Bit 0: IsLinear
      *   Bit 1: HasBase (Dependent, Undepended)
      *   Bit 2: IsInline (Inline, FatInline)
      *   Bit 3: IsAtom (Atom, PermanentAtom)
      *
      *  "HasBase" here refers to the two string types that have a 'base' field:
      *  JSDependentString and JSUndependedString.
      *  A JSUndependedString is a JSDependentString which has been 'fixed' (by ensureFixed)
      *  to be null-terminated.  In such cases, the string must keep marking its base since
      *  there may be any number of *other* JSDependentStrings transitively depending on it.
      *
      * If the INDEX_VALUE_BIT is set the upper 16 bits of the flag word hold the integer
      * index.
      */
 
-    static const uint32_t FLAT_BIT               = JS_BIT(0);
+    static const uint32_t LINEAR_BIT             = JS_BIT(0);
     static const uint32_t HAS_BASE_BIT           = JS_BIT(1);
     static const uint32_t INLINE_CHARS_BIT       = JS_BIT(2);
     static const uint32_t ATOM_BIT               = JS_BIT(3);
 
-    static const uint32_t ROPE_FLAGS             = 0;
-    static const uint32_t DEPENDENT_FLAGS        = HAS_BASE_BIT;
-    static const uint32_t UNDEPENDED_FLAGS       = FLAT_BIT | HAS_BASE_BIT;
-    static const uint32_t EXTENSIBLE_FLAGS       = FLAT_BIT | JS_BIT(4);
-    static const uint32_t EXTERNAL_FLAGS         = JS_BIT(5);
+    static const uint32_t DEPENDENT_FLAGS        = LINEAR_BIT | HAS_BASE_BIT;
+    static const uint32_t UNDEPENDED_FLAGS       = LINEAR_BIT | HAS_BASE_BIT | JS_BIT(4);
+    static const uint32_t EXTENSIBLE_FLAGS       = LINEAR_BIT | JS_BIT(4);
+    static const uint32_t EXTERNAL_FLAGS         = LINEAR_BIT | JS_BIT(5);
 
     static const uint32_t FAT_INLINE_MASK        = INLINE_CHARS_BIT | JS_BIT(4);
     static const uint32_t PERMANENT_ATOM_MASK    = ATOM_BIT | JS_BIT(5);
 
     /* Initial flags for thin inline and fat inline strings. */
-    static const uint32_t INIT_THIN_INLINE_FLAGS = FLAT_BIT | INLINE_CHARS_BIT;
-    static const uint32_t INIT_FAT_INLINE_FLAGS  = FLAT_BIT | FAT_INLINE_MASK;
+    static const uint32_t INIT_THIN_INLINE_FLAGS = LINEAR_BIT | INLINE_CHARS_BIT;
+    static const uint32_t INIT_FAT_INLINE_FLAGS  = LINEAR_BIT | FAT_INLINE_MASK;
+    static const uint32_t INIT_ROPE_FLAGS        = 0;
 
     static const uint32_t TYPE_FLAGS_MASK        = JS_BIT(6) - 1;
 
     static const uint32_t LATIN1_CHARS_BIT       = JS_BIT(6);
 
     static const uint32_t INDEX_VALUE_BIT        = JS_BIT(7);
     static const uint32_t INDEX_VALUE_SHIFT      = 16;
 
@@ -311,24 +311,24 @@ class JSString : public js::gc::TenuredC
         static_assert(offsetof(JSString, d.s.u2.nonInlineCharsTwoByte) == offsetof(String, nonInlineCharsTwoByte),
                       "shadow::String nonInlineChars offset must match JSString");
         static_assert(offsetof(JSString, d.s.u3.externalFinalizer) == offsetof(String, externalFinalizer),
                       "shadow::String externalFinalizer offset must match JSString");
         static_assert(offsetof(JSString, d.inlineStorageLatin1) == offsetof(String, inlineStorageLatin1),
                       "shadow::String inlineStorage offset must match JSString");
         static_assert(offsetof(JSString, d.inlineStorageTwoByte) == offsetof(String, inlineStorageTwoByte),
                       "shadow::String inlineStorage offset must match JSString");
+        static_assert(LINEAR_BIT == String::LINEAR_BIT,
+                      "shadow::String::LINEAR_BIT must match JSString::LINEAR_BIT");
         static_assert(INLINE_CHARS_BIT == String::INLINE_CHARS_BIT,
                       "shadow::String::INLINE_CHARS_BIT must match JSString::INLINE_CHARS_BIT");
         static_assert(LATIN1_CHARS_BIT == String::LATIN1_CHARS_BIT,
                       "shadow::String::LATIN1_CHARS_BIT must match JSString::LATIN1_CHARS_BIT");
         static_assert(TYPE_FLAGS_MASK == String::TYPE_FLAGS_MASK,
                       "shadow::String::TYPE_FLAGS_MASK must match JSString::TYPE_FLAGS_MASK");
-        static_assert(ROPE_FLAGS == String::ROPE_FLAGS,
-                      "shadow::String::ROPE_FLAGS must match JSString::ROPE_FLAGS");
         static_assert(EXTERNAL_FLAGS == String::EXTERNAL_FLAGS,
                       "shadow::String::EXTERNAL_FLAGS must match JSString::EXTERNAL_FLAGS");
     }
 
     /* Avoid lame compile errors in JSRope::flatten */
     friend class JSRope;
 
     friend class js::gc::RelocationOverlay;
@@ -379,28 +379,28 @@ class JSString : public js::gc::TenuredC
     static bool ensureLinear(JSContext* cx, JSString* str) {
         return str->ensureLinear(cx) != nullptr;
     }
 
     /* Type query and debug-checked casts */
 
     MOZ_ALWAYS_INLINE
     bool isRope() const {
-        return (d.u1.flags & TYPE_FLAGS_MASK) == ROPE_FLAGS;
+        return !(d.u1.flags & LINEAR_BIT);
     }
 
     MOZ_ALWAYS_INLINE
     JSRope& asRope() const {
         MOZ_ASSERT(isRope());
         return *(JSRope*)this;
     }
 
     MOZ_ALWAYS_INLINE
     bool isLinear() const {
-        return !isRope();
+        return d.u1.flags & LINEAR_BIT;
     }
 
     MOZ_ALWAYS_INLINE
     JSLinearString& asLinear() const {
         MOZ_ASSERT(JSString::isLinear());
         return *(JSLinearString*)this;
     }
 
@@ -412,17 +412,17 @@ class JSString : public js::gc::TenuredC
     MOZ_ALWAYS_INLINE
     JSDependentString& asDependent() const {
         MOZ_ASSERT(isDependent());
         return *(JSDependentString*)this;
     }
 
     MOZ_ALWAYS_INLINE
     bool isFlat() const {
-        return d.u1.flags & FLAT_BIT;
+        return isLinear() && !isDependent() && !isExternal();
     }
 
     MOZ_ALWAYS_INLINE
     JSFlatString& asFlat() const {
         MOZ_ASSERT(isFlat());
         return *(JSFlatString*)this;
     }
 
