# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1517418625 28800
#      Wed Jan 31 09:10:25 2018 -0800
# Node ID a246a99f932b26f44c1c691ba741025a5d38c1d3
# Parent  21fea000a9fadc75f1170337d34065992b7a74fd
Bug 1434342 P1 Add ServiceWorker::Create() factory method. r=asuth

diff --git a/dom/serviceworkers/ServiceWorker.cpp b/dom/serviceworkers/ServiceWorker.cpp
--- a/dom/serviceworkers/ServiceWorker.cpp
+++ b/dom/serviceworkers/ServiceWorker.cpp
@@ -33,19 +33,46 @@ ServiceWorkerVisible(JSContext* aCx, JSO
 {
   if (NS_IsMainThread()) {
     return DOMPrefs::ServiceWorkersEnabled();
   }
 
   return IS_INSTANCE_OF(ServiceWorkerGlobalScope, aObj);
 }
 
-ServiceWorker::ServiceWorker(nsPIDOMWindowInner* aWindow,
+// static
+already_AddRefed<ServiceWorker>
+ServiceWorker::Create(nsIGlobalObject* aOwner,
+                      const ServiceWorkerDescriptor& aDescriptor)
+{
+  RefPtr<ServiceWorker> ref;
+
+  RefPtr<ServiceWorkerManager> swm = ServiceWorkerManager::GetInstance();
+  if (!swm) {
+    return ref.forget();
+  }
+
+  RefPtr<ServiceWorkerRegistrationInfo> reg =
+    swm->GetRegistration(aDescriptor.PrincipalInfo(), aDescriptor.Scope());
+  if (!reg) {
+    return ref.forget();
+  }
+
+  RefPtr<ServiceWorkerInfo> info = reg->GetByID(aDescriptor.Id());
+  if (!info) {
+    return ref.forget();
+  }
+
+  ref = new ServiceWorker(aOwner, info);
+  return ref.forget();
+}
+
+ServiceWorker::ServiceWorker(nsIGlobalObject* aGlobal,
                              ServiceWorkerInfo* aInfo)
-  : DOMEventTargetHelper(aWindow),
+  : DOMEventTargetHelper(aGlobal),
     mInfo(aInfo)
 {
   MOZ_ASSERT(NS_IsMainThread());
   MOZ_ASSERT(aInfo);
 
   // This will update our state too.
   mInfo->AppendWorker(this);
 }
diff --git a/dom/serviceworkers/ServiceWorker.h b/dom/serviceworkers/ServiceWorker.h
--- a/dom/serviceworkers/ServiceWorker.h
+++ b/dom/serviceworkers/ServiceWorker.h
@@ -6,37 +6,39 @@
 
 #ifndef mozilla_dom_serviceworker_h__
 #define mozilla_dom_serviceworker_h__
 
 #include "mozilla/DOMEventTargetHelper.h"
 #include "mozilla/dom/BindingDeclarations.h"
 #include "mozilla/dom/ServiceWorkerBinding.h" // For ServiceWorkerState.
 
-class nsPIDOMWindowInner;
+class nsIGlobalObject;
 
 namespace mozilla {
 namespace dom {
 
 class ServiceWorkerInfo;
 class ServiceWorkerManager;
 class SharedWorker;
 
 bool
 ServiceWorkerVisible(JSContext* aCx, JSObject* aObj);
 
 class ServiceWorker final : public DOMEventTargetHelper
 {
-  friend class ServiceWorkerInfo;
 public:
   NS_DECL_ISUPPORTS_INHERITED
 
   IMPL_EVENT_HANDLER(statechange)
   IMPL_EVENT_HANDLER(error)
 
+  static already_AddRefed<ServiceWorker>
+  Create(nsIGlobalObject* aOwner, const ServiceWorkerDescriptor& aDescriptor);
+
   virtual JSObject*
   WrapObject(JSContext* aCx, JS::Handle<JSObject*> aGivenProto) override;
 
   ServiceWorkerState
   State() const
   {
     return mState;
   }
@@ -60,18 +62,17 @@ public:
 #undef PostMessage
 #endif
 
   void
   PostMessage(JSContext* aCx, JS::Handle<JS::Value> aMessage,
               const Sequence<JSObject*>& aTransferable, ErrorResult& aRv);
 
 private:
-  // This class can only be created from ServiceWorkerInfo::GetOrCreateInstance().
-  ServiceWorker(nsPIDOMWindowInner* aWindow, ServiceWorkerInfo* aInfo);
+  ServiceWorker(nsIGlobalObject* aWindow, ServiceWorkerInfo* aInfo);
 
   // This class is reference-counted and will be destroyed from Release().
   ~ServiceWorker();
 
   ServiceWorkerState mState;
   const RefPtr<ServiceWorkerInfo> mInfo;
 };
 
diff --git a/dom/serviceworkers/ServiceWorkerInfo.cpp b/dom/serviceworkers/ServiceWorkerInfo.cpp
--- a/dom/serviceworkers/ServiceWorkerInfo.cpp
+++ b/dom/serviceworkers/ServiceWorkerInfo.cpp
@@ -272,17 +272,18 @@ ServiceWorkerInfo::GetOrCreateInstance(n
     MOZ_ASSERT(mInstances[i]);
     if (mInstances[i]->GetOwner() == aWindow) {
       ref = mInstances[i];
       break;
     }
   }
 
   if (!ref) {
-    ref = new ServiceWorker(aWindow, this);
+    nsCOMPtr<nsIGlobalObject> global(do_QueryInterface(aWindow));
+    ref = ServiceWorker::Create(global, mDescriptor);
   }
 
   return ref.forget();
 }
 
 void
 ServiceWorkerInfo::UpdateInstalledTime()
 {
