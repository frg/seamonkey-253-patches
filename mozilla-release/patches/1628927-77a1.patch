# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1586649834 0
# Node ID f09ef9c6f56ab0addf33e1de2edf028d97bd2e4a
# Parent  26c2a341e3c58ac6fe686d4ccc5937736f4704d3
Bug 1628927 - Convert symbolstore.py to python 3. r=rstewart

Differential Revision: https://phabricator.services.mozilla.com/D70467

diff --git a/python/mozbuild/mozbuild/action/dumpsymbols.py b/python/mozbuild/mozbuild/action/dumpsymbols.py
--- a/python/mozbuild/mozbuild/action/dumpsymbols.py
+++ b/python/mozbuild/mozbuild/action/dumpsymbols.py
@@ -54,17 +54,17 @@ def dump_symbols(target, tracking_file):
                                                                      'dist_include'),
                                                         os.path.join(buildconfig.topobjdir,
                                                                      'dist',
                                                                      'include')))
     objcopy = buildconfig.substs.get('OBJCOPY')
     if objcopy:
         os.environ['OBJCOPY'] = objcopy
 
-    args = ([buildconfig.substs['PYTHON'],
+    args = ([sys.executable,
              os.path.join(buildconfig.topsrcdir, 'toolkit',
                           'crashreporter', 'tools', 'symbolstore.py')] +
             sym_store_args +
             ['-s', buildconfig.topsrcdir, dump_syms_bin, os.path.join(buildconfig.topobjdir,
                                                                       'dist',
                                                                       'crashreporter-symbols'),
              os.path.abspath(target)])
     print('Running: %s' % ' '.join(args))
diff --git a/toolkit/crashreporter/tools/python.ini b/toolkit/crashreporter/tools/python.ini
--- a/toolkit/crashreporter/tools/python.ini
+++ b/toolkit/crashreporter/tools/python.ini
@@ -1,4 +1,4 @@
 [DEFAULT]
-skip-if = python == 3
+skip-if = python == 2
 
 [unit-symbolstore.py]
diff --git a/toolkit/crashreporter/tools/symbolstore.py b/toolkit/crashreporter/tools/symbolstore.py
--- a/toolkit/crashreporter/tools/symbolstore.py
+++ b/toolkit/crashreporter/tools/symbolstore.py
@@ -16,31 +16,28 @@
 #     -c           : Copy debug info files to the same directory structure
 #                    as sym files. On Windows, this will also copy
 #                    binaries into the symbol store.
 #     -a "<archs>" : Run dump_syms -a <arch> for each space separated
 #                    cpu architecture in <archs> (only on OS X)
 #     -s <srcdir>  : Use <srcdir> as the top source directory to
 #                    generate relative filenames.
 
-from __future__ import print_function
-
 import buildconfig
 import errno
 import sys
 import platform
 import os
 import re
 import shutil
 import textwrap
 import fnmatch
 import subprocess
 import time
 import ctypes
-import urlparse
 import concurrent.futures
 import multiprocessing
 
 from optparse import OptionParser
 
 from mozbuild.util import memoize
 from mozbuild.generated_sources import (
     get_filename_with_digest,
@@ -132,17 +129,18 @@ class VCSFileInfo:
 #
 #   http://foo.com/bar
 #   svn+ssh://user@foo.com/bar
 #   svn+ssh://user:pass@foo.com/bar
 #
 rootRegex = re.compile(r'^\S+?:/+(?:[^\s/]*@)?(\S+)$')
 
 def read_output(*args):
-    (stdout, _) = subprocess.Popen(args=args, stdout=subprocess.PIPE).communicate()
+    (stdout, _) = subprocess.Popen(
+        args=args, universal_newlines=True, stdout=subprocess.PIPE).communicate()
     return stdout.rstrip()
 
 class HGRepoInfo:
     def __init__(self, path):
         self.path = path
 
         rev = os.environ.get('MOZ_SOURCE_CHANGESET')
         if not rev:
@@ -261,18 +259,16 @@ if platform.system() == 'Windows':
 
         This function also resolves any symlinks in the path.
         '''
         # Return the original path if something fails, which can happen for paths that
         # don't exist on this system (like paths from the CRT).
         result = path
 
         ctypes.windll.kernel32.SetErrorMode(ctypes.c_uint(1))
-        if not isinstance(path, unicode):
-            path = unicode(path, sys.getfilesystemencoding())
         handle = ctypes.windll.kernel32.CreateFileW(path,
                                                     # GENERIC_READ
                                                     0x80000000,
                                                     # FILE_SHARE_READ
                                                     1,
                                                     None,
                                                     # OPEN_EXISTING
                                                     3,
@@ -288,17 +284,17 @@ if platform.system() == 'Windows':
                                                                     0)
             buf = ctypes.create_unicode_buffer(size)
             if ctypes.windll.kernel32.GetFinalPathNameByHandleW(handle,
                                                                 buf,
                                                                 size,
                                                                 0) > 0:
                 # The return value of GetFinalPathNameByHandleW uses the
                 # '\\?\' prefix.
-                result = buf.value.encode(sys.getfilesystemencoding())[4:]
+                result = buf.value[4:]
             ctypes.windll.kernel32.CloseHandle(handle)
         return result
 else:
     # Just use the os.path version otherwise.
     normpath = os.path.normpath
 
 def IsInDir(file, dir):
     # the lower() is to handle win32+vc8, where
@@ -353,17 +349,17 @@ def GetVCSFilename(file, srcdirs):
 
 def validate_install_manifests(install_manifest_args):
     args = []
     for arg in install_manifest_args:
         bits = arg.split(',')
         if len(bits) != 2:
             raise ValueError('Invalid format for --install-manifest: '
                              'specify manifest,target_dir')
-        manifest_file, destination = map(os.path.abspath, bits)
+        manifest_file, destination = [os.path.abspath(b) for b in bits]
         if not os.path.isfile(manifest_file):
             raise IOError(errno.ENOENT, 'Manifest file not found',
                           manifest_file)
         if not os.path.isdir(destination):
             raise IOError(errno.ENOENT, 'Install directory not found',
                           destination)
         try:
             manifest = InstallManifest(manifest_file)
@@ -522,19 +518,19 @@ class Dumper:
         t_start = time.time()
         print("Processing file: %s" % file, file=sys.stderr)
 
         sourceFileStream = ''
         code_id, code_file = None, None
         try:
             cmd = self.dump_syms_cmdline(file, arch, dsymbundle=dsymbundle)
             print(' '.join(cmd), file=sys.stderr)
-            proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,
+            proc = subprocess.Popen(cmd, universal_newlines=True, stdout=subprocess.PIPE,
                                     stderr=open(os.devnull, 'wb'))
-            module_line = proc.stdout.next()
+            module_line = next(proc.stdout)
             if module_line.startswith("MODULE"):
                 # MODULE os cpu guid debug_file
                 (guid, debug_file) = (module_line.split())[3:5]
                 # strip off .pdb extensions, and append .sym
                 sym_file = re.sub("\.pdb$", "", debug_file) + ".sym"
                 # we do want forward slashes here
                 rel_path = os.path.join(debug_file,
                                         guid,
@@ -800,18 +796,18 @@ class Dumper_Mac(Dumper):
             shutil.rmtree(dsymbundle)
         dsymutil = buildconfig.substs['DSYMUTIL']
         # dsymutil takes --arch=foo instead of -a foo like everything else
         cmd = ([dsymutil] +
                [a.replace('-a ', '--arch=') for a in self.archs if a] +
                [file])
         print(' '.join(cmd), file=sys.stderr)
 
-        dsymutil_proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,
-                                         stderr=subprocess.PIPE)
+        dsymutil_proc = subprocess.Popen(cmd, universal_newlines=True,
+                                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
         dsymout, dsymerr = dsymutil_proc.communicate()
         if dsymutil_proc.returncode != 0:
             raise RuntimeError('Error running dsymutil: %s' % dsymerr)
 
         # Regular dsymutil won't produce a .dSYM for files without symbols.
         if not os.path.exists(dsymbundle):
             print("No symbols found in file: %s" % (file,), file=sys.stderr)
             return False
@@ -837,17 +833,18 @@ class Dumper_Mac(Dumper):
         files = os.listdir(contents_dir)
         if len(files) != 1:
             print("Unexpected files in .dSYM bundle %s" % (files,),
                   file=sys.stderr)
             return False
 
         otool_out = subprocess.check_output([buildconfig.substs['OTOOL'],
                                              '-l',
-                                             os.path.join(contents_dir, files[0])])
+                                             os.path.join(contents_dir, files[0])],
+                                            universal_newlines=True)
         if 'sectname __debug_info' not in otool_out:
             print("No symbols in .dSYM bundle %s" % (dsymbundle,),
                   file=sys.stderr)
             return False
 
         elapsed = time.time() - t_start
         print('Finished processing %s in %.2fs' % (file, elapsed),
               file=sys.stderr)
diff --git a/toolkit/crashreporter/tools/unit-symbolstore.py b/toolkit/crashreporter/tools/unit-symbolstore.py
--- a/toolkit/crashreporter/tools/unit-symbolstore.py
+++ b/toolkit/crashreporter/tools/unit-symbolstore.py
@@ -108,17 +108,17 @@ class TestCopyDebug(HelperMixin, unittes
         self.symbol_dir = tempfile.mkdtemp()
         self.mock_call = patch("subprocess.call").start()
         self.stdouts = []
         self.mock_popen = patch("subprocess.Popen").start()
         stdout_iter = self.next_mock_stdout()
         def next_popen(*args, **kwargs):
             m = mock.MagicMock()
             # Get the iterators over whatever output was provided.
-            stdout_ = stdout_iter.next()
+            stdout_ = next(stdout_iter)
             # Eager evaluation for communicate(), below.
             stdout_ = list(stdout_)
             # stdout is really an iterator, so back to iterators we go.
             m.stdout = iter(stdout_)
             m.wait.return_value = 0
             # communicate returns the full text of stdout and stderr.
             m.communicate.return_value = ('\n'.join(stdout_), '')
             return m
@@ -254,30 +254,30 @@ class TestGeneratedFilePath(HelperMixin,
         self.assertEqual(expected, symbolstore.get_generated_file_s3_path(g, rel_path, 'bucket'))
 
 
 if target_platform() == 'WINNT':
     class TestNormpath(HelperMixin, unittest.TestCase):
         def test_normpath(self):
             # self.test_dir is going to be 8.3 paths...
             junk = os.path.join(self.test_dir, 'x')
-            with open(junk, 'wb') as o:
+            with open(junk, 'w') as o:
                 o.write('x')
             fixed_dir = os.path.dirname(normpath(junk))
             files = [
                 'one\\two.c',
                 'three\\Four.d',
                 'Five\\Six.e',
                 'seven\\Eight\\nine.F',
             ]
             for rel_path in files:
                 full_path = os.path.normpath(os.path.join(self.test_dir,
                                                           rel_path))
                 self.make_dirs(full_path)
-                with open(full_path, 'wb') as o:
+                with open(full_path, 'w') as o:
                     o.write('x')
                 fixed_path = normpath(full_path.lower())
                 fixed_path = os.path.relpath(fixed_path, fixed_dir)
                 self.assertEqual(rel_path, fixed_path)
 
     class TestSourceServer(HelperMixin, unittest.TestCase):
         @patch("subprocess.call")
         @patch("subprocess.Popen")
@@ -353,17 +353,17 @@ class TestInstallManifest(HelperMixin, u
         arg = '%s,%s' % (self.manifest_file, self.objdir)
         ret = symbolstore.validate_install_manifests([arg])
         self.assertEqual(len(ret), 1)
         manifest, dest = ret[0]
         self.assertTrue(isinstance(manifest, InstallManifest))
         self.assertEqual(dest, self.objdir)
 
         file_mapping = symbolstore.make_file_mapping(ret)
-        for obj, src in self.canonical_mapping.iteritems():
+        for obj, src in self.canonical_mapping.items():
             self.assertTrue(obj in file_mapping)
             self.assertEqual(file_mapping[obj], src)
 
     def testMissingFiles(self):
         '''
         Test that missing manifest files or install directories give errors.
         '''
         missing_manifest = os.path.join(self.test_dir, 'missing-manifest')
@@ -378,17 +378,17 @@ class TestInstallManifest(HelperMixin, u
             symbolstore.validate_install_manifests([arg])
             self.assertEqual(e.filename, missing_install_dir)
 
     def testBadManifest(self):
         '''
         Test that a bad manifest file give errors.
         '''
         bad_manifest = os.path.join(self.test_dir, 'bad-manifest')
-        with open(bad_manifest, 'wb') as f:
+        with open(bad_manifest, 'w') as f:
             f.write('junk\n')
         arg = '%s,%s' % (bad_manifest, self.objdir)
         with self.assertRaises(IOError) as e:
             symbolstore.validate_install_manifests([arg])
             self.assertEqual(e.filename, bad_manifest)
 
     def testBadArgument(self):
         '''
@@ -440,17 +440,17 @@ class TestFileMapping(HelperMixin, unitt
                     'PUBLIC xyz 123\n'
                 ]
             )
         mock_Popen.return_value.stdout = mk_output(dumped_files)
 
         d = symbolstore.Dumper('dump_syms', self.symboldir,
                                file_mapping=file_mapping)
         f = os.path.join(self.objdir, 'somefile')
-        open(f, 'wb').write('blah')
+        open(f, 'w').write('blah')
         d.Process(f)
         expected_output = ''.join(mk_output(expected_files))
         symbol_file = os.path.join(self.symboldir,
                                    file_id[1], file_id[0], file_id[1] + '.sym')
         self.assertEqual(open(symbol_file, 'r').read(), expected_output)
 
 class TestFunctional(HelperMixin, unittest.TestCase):
     '''Functional tests of symbolstore.py, calling it with a real
@@ -506,19 +506,20 @@ class TestFunctional(HelperMixin, unitte
                                           self.script_path,
                                           '--vcs-info',
                                           '-s', self.topsrcdir,
                                           '--install-manifest=%s,%s' % (dist_include_manifest,
                                                                         dist_include),
                                           self.dump_syms,
                                           self.test_dir,
                                           self.target_bin],
+                                         universal_newlines=True,
                                          stderr=open(os.devnull, 'w'),
                                          cwd=browser_app)
-        lines = filter(lambda x: x.strip(), output.splitlines())
+        lines = [l for l in output.splitlines() if l.strip()]
         self.assertEqual(1, len(lines),
                          'should have one filename in the output')
         symbol_file = os.path.join(self.test_dir, lines[0])
         self.assertTrue(os.path.isfile(symbol_file))
         symlines = open(symbol_file, 'r').readlines()
         file_lines = [l for l in symlines if l.startswith('FILE')]
         def check_hg_path(lines, match):
             match_lines = [l for l in file_lines if match in l]
