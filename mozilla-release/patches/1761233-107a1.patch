# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1665050726 0
#      Thu Oct 06 10:05:26 2022 +0000
# Node ID ddbd657d52dd29e05d9c2d2d1cef56679e257935
# Parent  119144758463ba3a87fa23a220a573e5d4bad7f9
Bug 1761233 - Apply VDMX sanitization fix from https://github.com/khaledhosny/ots/pull/250 to avoid generating invalid "sanitized" data. r=gfx-reviewers,lsalzman

With this fix, the site from comment 21 loads successfully.

Differential Revision: https://phabricator.services.mozilla.com/D158712

diff --git a/gfx/ots/src/vdmx.cc b/gfx/ots/src/vdmx.cc
--- a/gfx/ots/src/vdmx.cc
+++ b/gfx/ots/src/vdmx.cc
@@ -1,41 +1,46 @@
 // Copyright (c) 2009-2017 The OTS Authors. All rights reserved.
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
 #include "vdmx.h"
 
+#include <set>
+
 // VDMX - Vertical Device Metrics
 // http://www.microsoft.com/typography/otspec/vdmx.htm
 
 namespace ots {
 
+#define TABLE_NAME "VDMX"
+
 bool OpenTypeVDMX::Parse(const uint8_t *data, size_t length) {
   Buffer table(data, length);
+  ots::Font* font = this->GetFont();
 
   if (!table.ReadU16(&this->version) ||
       !table.ReadU16(&this->num_recs) ||
       !table.ReadU16(&this->num_ratios)) {
-    return Error("Failed to read table header");
+    return Drop("Failed to read table header");
   }
 
   if (this->version > 1) {
     return Drop("Unsupported table version: %u", this->version);
   }
 
   this->rat_ranges.reserve(this->num_ratios);
   for (unsigned i = 0; i < this->num_ratios; ++i) {
     OpenTypeVDMXRatioRecord rec;
 
     if (!table.ReadU8(&rec.charset) ||
         !table.ReadU8(&rec.x_ratio) ||
         !table.ReadU8(&rec.y_start_ratio) ||
         !table.ReadU8(&rec.y_end_ratio)) {
-      return Error("Failed to read RatioRange record %d", i);
+      return Drop("Failed to read RatioRange record %d", i);
     }
 
     if (rec.charset > 1) {
       return Drop("Unsupported character set: %u", rec.charset);
     }
 
     if (rec.y_start_ratio > rec.y_end_ratio) {
       return Drop("Bad y ratio");
@@ -51,44 +56,55 @@ bool OpenTypeVDMX::Parse(const uint8_t *
       return Drop("Superfluous terminator found");
     }
 
     this->rat_ranges.push_back(rec);
   }
 
   this->offsets.reserve(this->num_ratios);
   const size_t current_offset = table.offset();
+  std::set<uint16_t> unique_offsets;
   // current_offset is less than (2 bytes * 3) + (4 bytes * USHRT_MAX) = 256k.
   for (unsigned i = 0; i < this->num_ratios; ++i) {
     uint16_t offset;
     if (!table.ReadU16(&offset)) {
-      return Error("Failed to read ratio offset %d", i);
+      return Drop("Failed to read ratio offset %d", i);
     }
     if (current_offset + offset >= length) {  // thus doesn't overflow.
-      return Error("Bad ratio offset %d for ration %d", offset, i);
+      return Drop("Bad ratio offset %d for ration %d", offset, i);
     }
 
     this->offsets.push_back(offset);
+    unique_offsets.insert(offset);
+  }
+
+  // Check that num_recs is sufficient to provide as many VDMXGroup records
+  // as there are unique offsets; if not, update it (we'll return an error
+  // below if they're not actually present).
+  if (unique_offsets.size() > this->num_recs) {
+    OTS_WARNING("increasing num_recs (%u is too small for %u unique offsets)",
+                this->num_recs, unique_offsets.size());
+    this->num_recs = unique_offsets.size();
   }
 
   this->groups.reserve(this->num_recs);
   for (unsigned i = 0; i < this->num_recs; ++i) {
     OpenTypeVDMXGroup group;
     if (!table.ReadU16(&group.recs) ||
         !table.ReadU8(&group.startsz) ||
         !table.ReadU8(&group.endsz)) {
-      return Error("Failed to read record header %d", i);
+      return Drop("Failed to read record header %d", i);
     }
     group.entries.reserve(group.recs);
     for (unsigned j = 0; j < group.recs; ++j) {
       OpenTypeVDMXVTable vt;
       if (!table.ReadU16(&vt.y_pel_height) ||
           !table.ReadS16(&vt.y_max) ||
           !table.ReadS16(&vt.y_min)) {
-        return Error("Failed to read reacord %d group %d", i, j);
+        return Drop("Failed to read record %d group %d", i, j);
       }
       if (vt.y_max < vt.y_min) {
         return Drop("bad y min/max");
       }
 
       // This table must appear in sorted order (sorted by yPelHeight),
       // but need not be continuous.
       if ((j != 0) && (group.entries[j - 1].y_pel_height >= vt.y_pel_height)) {
@@ -147,9 +163,11 @@ bool OpenTypeVDMX::Serialize(OTSStream *
         return Error("Failed to write group %d entry %d", i, j);
       }
     }
   }
 
   return true;
 }
 
+#undef TABLE_NAME
+
 }  // namespace ots
