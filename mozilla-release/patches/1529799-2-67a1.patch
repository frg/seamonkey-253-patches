# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1551218724 0
# Node ID 69f758a41070f572edf341089336bc1d4c3168a8
# Parent  8897873580ec4d75d97fedee30edd1bbb28a5232
Bug 1529799 - Prevent recursive resolution of options during imply_option. r=chmanchester

In cases like those in the added unit test, explicit options on the
command line could end up being silently ignored. So instead of that happening,
error out. Unfortunately, the error message is not entirely accurate,
but it's better than nothing. It's rare anyways (I only stumbled upon it
because I was trying to do something fishy), and correlation between the
error message and the corresponding changes should make it clear what's
going on.

Depends on D20822

Differential Revision: https://phabricator.services.mozilla.com/D20823

diff --git a/python/mozbuild/mozbuild/configure/__init__.py b/python/mozbuild/mozbuild/configure/__init__.py
--- a/python/mozbuild/mozbuild/configure/__init__.py
+++ b/python/mozbuild/mozbuild/configure/__init__.py
@@ -560,16 +560,24 @@ class ConfigureSandbox(dict):
             reason = implied[e.arg].reason
             if isinstance(reason, Option):
                 reason = self._raw_options.get(reason) or reason.option
                 reason = reason.split('=', 1)[0]
             raise InvalidOptionError(
                 "'%s' implied by '%s' conflicts with '%s' from the %s"
                 % (e.arg, reason, e.old_arg, e.old_origin))
 
+        if value.origin == 'implied':
+            recursed_value = getattr(self, '__value_for_option').get((option,))
+            if recursed_value is not None:
+                _, filename, line, _, _, _ = implied[value.format(option.option)].caller
+                raise ConfigureError(
+                    "'%s' appears somewhere in the direct or indirect dependencies when "
+                    "resolving imply_option at %s:%d" % (option.option, filename, line))
+
         if option_string:
             self._raw_options[option] = option_string
 
         when = self._conditions.get(option)
         # If `when` resolves to a false-ish value, we always return None.
         # This makes option(..., when='--foo') equivalent to
         # option(..., when=depends('--foo')(lambda x: x)).
         if when and not self._value_for(when) and value is not None:
diff --git a/python/mozbuild/mozbuild/test/configure/test_configure.py b/python/mozbuild/mozbuild/test/configure/test_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_configure.py
@@ -782,16 +782,53 @@ class TestConfigure(unittest.TestCase):
                               "with '--without-foo' from the command-line")
 
             config = self.get_config(['--without-qux'])
             self.assertEquals(config, {
                 'FOO': NegativeOptionValue(),
                 'QUX': NegativeOptionValue(),
             })
 
+    def test_imply_option_recursion(self):
+        config_path = mozpath.abspath(
+            mozpath.join(test_data_path, 'moz.configure'))
+
+        message = ("'--without-foo' appears somewhere in the direct or indirect dependencies "
+                   "when resolving imply_option at %s:8" % config_path)
+
+        with self.moz_configure('''
+            option('--without-foo', help='foo')
+
+            imply_option('--with-qux', depends('--with-foo')(lambda x: x or None))
+
+            option('--with-qux', help='qux')
+
+            imply_option('--with-foo', depends('--with-qux')(lambda x: x or None))
+
+            set_config('FOO', depends('--with-foo')(lambda x: x))
+            set_config('QUX', depends('--with-qux')(lambda x: x))
+        '''):
+            # Note: no error is detected when the depends function in the
+            # imply_options resolve to None, which disables the imply_option.
+
+            with self.assertRaises(ConfigureError) as e:
+                config = self.get_config()
+
+            self.assertEquals(e.exception.message, message)
+
+            with self.assertRaises(ConfigureError) as e:
+                config = self.get_config(['--with-qux'])
+
+            self.assertEquals(e.exception.message, message)
+
+            with self.assertRaises(ConfigureError) as e:
+                config = self.get_config(['--without-foo', '--with-qux'])
+
+            self.assertEquals(e.exception.message, message)
+
     def test_option_failures(self):
         with self.assertRaises(ConfigureError) as e:
             with self.moz_configure('option("--with-foo", help="foo")'):
                 self.get_config()
 
         self.assertEquals(
             e.exception.message,
             'Option `--with-foo` is not handled ; reference it with a @depends'
