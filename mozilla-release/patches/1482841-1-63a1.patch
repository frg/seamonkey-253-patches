# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1534193735 -7200
# Node ID 7b1366d8fa15fcce4a300cff135119985f04b167
# Parent  b0fe1a2781b29bdfe3096749718dcd34c9ead1a6
Bug 1482841 - P1. Update mp4parse-rust to 643f48e137592e6318f6c780448374324908da31. r=padenot

Differential Revision: https://phabricator.services.mozilla.com/D3261

diff --git a/media/mp4parse-rust/mp4parse.h b/media/mp4parse-rust/mp4parse.h
--- a/media/mp4parse-rust/mp4parse.h
+++ b/media/mp4parse-rust/mp4parse.h
@@ -39,16 +39,17 @@ typedef enum {
   MP4PARSE_STATUS_EOF = 4,
   MP4PARSE_STATUS_IO = 5,
   MP4PARSE_STATUS_OOM = 6,
 } Mp4parseStatus;
 
 typedef enum {
   MP4PARSE_TRACK_TYPE_VIDEO = 0,
   MP4PARSE_TRACK_TYPE_AUDIO = 1,
+  MP4PARSE_TRACK_TYPE_METADATA = 2,
 } Mp4parseTrackType;
 
 typedef struct Mp4parseParser Mp4parseParser;
 
 typedef struct {
   uint64_t fragment_duration;
 } Mp4parseFragmentInfo;
 
diff --git a/media/mp4parse-rust/mp4parse/src/lib.rs b/media/mp4parse-rust/mp4parse/src/lib.rs
--- a/media/mp4parse-rust/mp4parse/src/lib.rs
+++ b/media/mp4parse-rust/mp4parse/src/lib.rs
@@ -20,16 +20,19 @@ use std::cmp;
 use num_traits::Num;
 
 #[cfg(feature = "mp4parse_fallible")]
 extern crate mp4parse_fallible;
 
 #[cfg(feature = "mp4parse_fallible")]
 use mp4parse_fallible::FallibleVec;
 
+#[macro_use]
+mod macros;
+
 mod boxes;
 use boxes::{BoxType, FourCC};
 
 // Unit tests.
 #[cfg(test)]
 mod tests;
 
 // Arbitrary buffer size limit used for raw read_bufs on a box.
@@ -447,16 +450,17 @@ impl MediaContext {
         Default::default()
     }
 }
 
 #[derive(Debug, PartialEq)]
 pub enum TrackType {
     Audio,
     Video,
+    Metadata,
     Unknown,
 }
 
 impl Default for TrackType {
     fn default() -> Self { TrackType::Unknown }
 }
 
 #[derive(Debug, Clone, Copy, PartialEq)]
@@ -671,25 +675,16 @@ fn skip_box_remain<T: Read>(src: &mut BM
         let header = src.get_header();
         let len = src.bytes_left();
         debug!("remain {} (skipped) in {:?}", len, header);
         len
     };
     skip(src, remain)
 }
 
-macro_rules! check_parser_state {
-    ( $src:expr ) => {
-        if $src.limit() > 0 {
-            debug!("bad parser state: {} content bytes left", $src.limit());
-            return Err(Error::InvalidData("unread box content or bad parser sync"));
-        }
-    }
-}
-
 /// Read the contents of a box, including sub boxes.
 ///
 /// Metadata is accumulated in the passed-through `MediaContext` struct,
 /// which can be examined later.
 pub fn read_mp4<T: Read>(f: &mut T, context: &mut MediaContext) -> Result<()> {
     let mut found_ftyp = false;
     let mut found_moov = false;
     // TODO(kinetik): Top-level parsing should handle zero-sized boxes
@@ -930,16 +925,17 @@ fn read_mdia<T: Read>(f: &mut BMFFBox<T>
                 debug!("{:?}", mdhd);
             }
             BoxType::HandlerBox => {
                 let hdlr = read_hdlr(&mut b)?;
 
                 match hdlr.handler_type.value.as_ref() {
                     "vide" => track.track_type = TrackType::Video,
                     "soun" => track.track_type = TrackType::Audio,
+                    "meta" => track.track_type = TrackType::Metadata,
                     _ => (),
                 }
                 debug!("{:?}", hdlr);
             }
             BoxType::MediaInformationBox => read_minf(&mut b, track)?,
             _ => skip_box_content(&mut b)?,
         };
         check_parser_state!(b.content);
@@ -2074,16 +2070,17 @@ fn read_stsd<T: Read>(src: &mut BMFFBox<
 
     {
         // TODO(kinetik): check if/when more than one desc per track? do we need to support?
         let mut iter = src.box_iter();
         while let Some(mut b) = iter.next_box()? {
             let description = match track.track_type {
                 TrackType::Video => read_video_sample_entry(&mut b),
                 TrackType::Audio => read_audio_sample_entry(&mut b),
+                TrackType::Metadata => Err(Error::Unsupported("metadata track")),
                 TrackType::Unknown => Err(Error::Unsupported("unknown track type")),
             };
             let description = match description {
                 Ok((codec_type, desc)) => {
                     track.codec_type = codec_type;
                     desc
                 }
                 Err(Error::Unsupported(_)) => {
diff --git a/media/mp4parse-rust/mp4parse/src/macros.rs b/media/mp4parse-rust/mp4parse/src/macros.rs
new file mode 100644
--- /dev/null
+++ b/media/mp4parse-rust/mp4parse/src/macros.rs
@@ -0,0 +1,12 @@
+// This Source Code Form is subject to the terms of the Mozilla Public
+// License, v. 2.0. If a copy of the MPL was not distributed with this
+// file, You can obtain one at https://mozilla.org/MPL/2.0/.
+
+macro_rules! check_parser_state {
+    ( $src:expr ) => {
+        if $src.limit() > 0 {
+            debug!("bad parser state: {} content bytes left", $src.limit());
+            return Err(Error::InvalidData("unread box content or bad parser sync"));
+        }
+    }
+}
diff --git a/media/mp4parse-rust/mp4parse_capi/src/lib.rs b/media/mp4parse-rust/mp4parse_capi/src/lib.rs
--- a/media/mp4parse-rust/mp4parse_capi/src/lib.rs
+++ b/media/mp4parse-rust/mp4parse_capi/src/lib.rs
@@ -72,16 +72,17 @@ pub enum Mp4parseStatus {
     Oom = 6,
 }
 
 #[repr(C)]
 #[derive(PartialEq, Debug)]
 pub enum Mp4parseTrackType {
     Video = 0,
     Audio = 1,
+    Metadata = 2,
 }
 
 impl Default for Mp4parseTrackType {
     fn default() -> Self { Mp4parseTrackType::Video }
 }
 
 #[allow(non_camel_case_types)]
 #[repr(C)]
@@ -402,16 +403,17 @@ pub unsafe extern fn mp4parse_get_track_
 
     if track_index >= context.tracks.len() {
         return Mp4parseStatus::BadArg;
     }
 
     info.track_type = match context.tracks[track_index].track_type {
         TrackType::Video => Mp4parseTrackType::Video,
         TrackType::Audio => Mp4parseTrackType::Audio,
+        TrackType::Metadata => Mp4parseTrackType::Metadata,
         TrackType::Unknown => return Mp4parseStatus::Unsupported,
     };
 
     // Return UNKNOWN for unsupported format.
     info.codec = match context.tracks[track_index].data {
         Some(SampleEntry::Audio(ref audio)) => match audio.codec_specific {
             AudioCodecSpecific::OpusSpecificBox(_) =>
                 Mp4parseCodec::Opus,
@@ -538,41 +540,41 @@ pub unsafe extern fn mp4parse_get_track_
             }
         }
         AudioCodecSpecific::FLACSpecificBox(ref flac) => {
             // Return the STREAMINFO metadata block in the codec_specific.
             let streaminfo = &flac.blocks[0];
             if streaminfo.block_type != 0 || streaminfo.data.len() != 34 {
                 return Mp4parseStatus::Invalid;
             }
-            (*info).extra_data.length = streaminfo.data.len() as u32;
-            (*info).extra_data.data = streaminfo.data.as_ptr();
+            (*info).codec_specific_config.length = streaminfo.data.len() as u32;
+            (*info).codec_specific_config.data = streaminfo.data.as_ptr();
         }
         AudioCodecSpecific::OpusSpecificBox(ref opus) => {
             let mut v = Vec::new();
             match serialize_opus_header(opus, &mut v) {
                 Err(_) => {
                     return Mp4parseStatus::Invalid;
                 }
                 Ok(_) => {
                     let header = (*parser).opus_header_mut();
                     header.insert(track_index, v);
                     if let Some(v) = header.get(&track_index) {
                         if v.len() > std::u32::MAX as usize {
                             return Mp4parseStatus::Invalid;
                         }
-                        (*info).extra_data.length = v.len() as u32;
-                        (*info).extra_data.data = v.as_ptr();
+                        (*info).codec_specific_config.length = v.len() as u32;
+                        (*info).codec_specific_config.data = v.as_ptr();
                     }
                 }
             }
         }
         AudioCodecSpecific::ALACSpecificBox(ref alac) => {
-            (*info).extra_data.length = alac.data.len() as u32;
-            (*info).extra_data.data = alac.data.as_ptr();
+            (*info).codec_specific_config.length = alac.data.len() as u32;
+            (*info).codec_specific_config.data = alac.data.as_ptr();
         }
         AudioCodecSpecific::MP3 | AudioCodecSpecific::LPCM => (),
     }
 
     if let Some(p) = audio.protection_info.iter().find(|sinf| sinf.tenc.is_some()) {
         if let Some(ref tenc) = p.tenc {
             (*info).protected_data.is_encrypted = tenc.is_encrypted;
             (*info).protected_data.iv_size = tenc.iv_size;
