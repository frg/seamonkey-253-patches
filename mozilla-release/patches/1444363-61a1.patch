# HG changeset patch
# User Byron Campen [:bwc] <docfaraday@gmail.com>
# Date 1523893949 18000
# Node ID e4c4c815992454d67e947beb0c18aa5202c26579
# Parent  bb77e5c3f39b20ab2245db32515181f3494df4e5
Bug 1444363: Don't dispatch Encode/Decode to main in the fake h264 impl, because the callsite is on main in the first place. r=bryce

MozReview-Commit-ID: Iznr1jNr75Z

diff --git a/dom/media/gmp-plugin-openh264/gmp-fake-openh264.cpp b/dom/media/gmp-plugin-openh264/gmp-fake-openh264.cpp
--- a/dom/media/gmp-plugin-openh264/gmp-fake-openh264.cpp
+++ b/dom/media/gmp-plugin-openh264/gmp-fake-openh264.cpp
@@ -80,18 +80,16 @@ static int g_log_level = GL_CRIT;
         if ((l >= 0) && (l <= 3)) {                        \
         log_string = kLogStrings[l];                       \
         }                                                  \
         std::cerr << log_string << ": " << x << std::endl; \
         }                                                  \
     } while(0)
 
 
-GMPPlatformAPI* g_platform_api = nullptr;
-
 class FakeVideoEncoder;
 class FakeVideoDecoder;
 
 struct EncodedFrame {
   uint32_t length_;
   uint8_t h264_compat_;
   uint32_t magic_;
   uint32_t width_;
@@ -121,31 +119,16 @@ template <typename T> class SelfDestruct
     return t;
   }
 #endif
 
  private:
   T* t_;
 };
 
-class FakeEncoderTask : public GMPTask {
- public:
-  FakeEncoderTask(FakeVideoEncoder* encoder,
-                  GMPVideoi420Frame* frame,
-                  GMPVideoFrameType type)
-      : encoder_(encoder), frame_(frame), type_(type) {}
-
-  void Run() override;
-  void Destroy() override { delete this; }
-
-  FakeVideoEncoder* encoder_;
-  GMPVideoi420Frame* frame_;
-  GMPVideoFrameType type_;
-};
-
 class FakeVideoEncoder : public GMPVideoEncoder {
  public:
   explicit FakeVideoEncoder (GMPVideoHost* hostAPI) :
     host_ (hostAPI),
     callback_ (nullptr),
     frame_size_(BIG_FRAME),
     frames_encoded_(0) {}
 
@@ -251,24 +234,18 @@ class FakeVideoEncoder : public GMPVideo
                const GMPVideoFrameType* aFrameTypes,
                uint32_t aFrameTypesLength) override {
     GMPLOG (GL_DEBUG,
             __FUNCTION__
             << " size="
             << inputImage->Width() << "x" << inputImage->Height());
 
     assert (aFrameTypesLength != 0);
+    GMPVideoFrameType frame_type = aFrameTypes[0];
 
-    g_platform_api->runonmainthread(new FakeEncoderTask(this,
-                                                        inputImage,
-                                                        aFrameTypes[0]));
-  }
-
-  void Encode_m (GMPVideoi420Frame* inputImage,
-                 GMPVideoFrameType frame_type) {
     SelfDestruct<GMPVideoi420Frame> ifd (inputImage);
 
     if (frame_type  == kGMPKeyFrame) {
       if (!inputImage)
         return;
     }
     if (!inputImage) {
       GMPLOG (GL_ERROR, "no input image");
@@ -310,36 +287,16 @@ class FakeVideoEncoder : public GMPVideo
   }
 
   GMPVideoHost* host_;
   GMPVideoEncoderCallback* callback_;
   uint32_t frame_size_;
   uint32_t frames_encoded_;
 };
 
-void FakeEncoderTask::Run() {
-  encoder_->Encode_m(frame_, type_);
-  frame_ = nullptr; // Encode_m() destroys the frame
-}
-
-class FakeDecoderTask : public GMPTask {
- public:
-  FakeDecoderTask(FakeVideoDecoder* decoder,
-                  GMPVideoEncodedFrame* frame,
-                  int64_t time)
-      : decoder_(decoder), frame_(frame), time_(time) {}
-
-  void Run() override;
-  void Destroy() override { delete this; }
-
-  FakeVideoDecoder* decoder_;
-  GMPVideoEncodedFrame* frame_;
-  int64_t time_;
-};
-
 class FakeVideoDecoder : public GMPVideoDecoder {
  public:
   explicit FakeVideoDecoder (GMPVideoHost* hostAPI) :
     host_ (hostAPI),
     callback_ (nullptr) {}
 
   ~FakeVideoDecoder() override = default;
 
@@ -360,32 +317,17 @@ class FakeVideoDecoder : public GMPVideo
   void Decode (GMPVideoEncodedFrame* inputFrame,
                bool missingFrames,
                const uint8_t* aCodecSpecificInfo,
                uint32_t aCodecSpecificInfoLength,
                int64_t renderTimeMs = -1) override {
     GMPLOG (GL_DEBUG, __FUNCTION__
             << "Decoding frame size=" << inputFrame->Size()
             << " timestamp=" << inputFrame->TimeStamp());
-    g_platform_api->runonmainthread(new FakeDecoderTask(this, inputFrame, renderTimeMs));
-  }
 
-  void Reset() override {
-  }
-
-  void Drain() override {
-  }
-
-  void DecodingComplete() override {
-    delete this;
-  }
-
-  // Return the decoded data back to the parent.
-  void Decode_m (GMPVideoEncodedFrame* inputFrame,
-                 int64_t renderTimeMs) {
     // Attach a self-destructor so that the input frame is destroyed on return.
     SelfDestruct<GMPVideoEncodedFrame> ifd (inputFrame);
 
     EncodedFrame *eframe;
     eframe = reinterpret_cast<EncodedFrame*>(inputFrame->Buffer());
     GMPLOG(GL_DEBUG,"magic="  << eframe->magic_ << " h264_compat="  << (int) eframe->h264_compat_
            << " width=" << eframe->width_ << " height=" << eframe->height_
            << " timestamp=" << inputFrame->TimeStamp()
@@ -447,30 +389,34 @@ class FakeVideoDecoder : public GMPVideo
 
     GMPLOG (GL_DEBUG, "Allocated size = "
             << frame->AllocatedSize (kGMPYPlane));
     frame->SetTimestamp (inputFrame->TimeStamp());
     frame->SetDuration (inputFrame->Duration());
     callback_->Decoded (frame);
   }
 
+  void Reset() override {
+  }
+
+  void Drain() override {
+  }
+
+  void DecodingComplete() override {
+    delete this;
+  }
+
   GMPVideoHost* host_;
   GMPVideoDecoderCallback* callback_;
 };
 
-void FakeDecoderTask::Run() {
-  decoder_->Decode_m(frame_, time_);
-  frame_ = nullptr; // Decode_m() destroys the frame
-}
-
 extern "C" {
 
   PUBLIC_FUNC GMPErr
   GMPInit (GMPPlatformAPI* aPlatformAPI) {
-    g_platform_api = aPlatformAPI;
     return GMPNoErr;
   }
 
   PUBLIC_FUNC GMPErr
   GMPGetAPI (const char* aApiName, void* aHostAPI, void** aPluginApi) {
     if (!strcmp (aApiName, GMP_API_VIDEO_DECODER)) {
       *aPluginApi = new FakeVideoDecoder (static_cast<GMPVideoHost*> (aHostAPI));
       return GMPNoErr;
@@ -479,12 +425,11 @@ extern "C" {
       *aPluginApi = new FakeVideoEncoder (static_cast<GMPVideoHost*> (aHostAPI));
       return GMPNoErr;
     }
     return GMPGenericErr;
   }
 
   PUBLIC_FUNC void
   GMPShutdown (void) {
-    g_platform_api = nullptr;
   }
 
 } // extern "C"
