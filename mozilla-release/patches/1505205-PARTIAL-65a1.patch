# HG changeset patch
# User Ted Mielczarek <ted@mielczarek.org>
# Date 1541876670 0
# Node ID 008ce258d380085fdc87241c9dec57e47a4b069a
# Parent  93cf06bcf8d8a095330006bceb6ee38acf669356
bug 1505205 - don't write telemetry for recursive mach command invocations. r=firefox-build-system-reviewers,chmanchester

This change tries to ensure that we don't write telemetry data for mach
commands invoked recursively as part of other mach commands. The intent of
build system telemetry is to only collect data about commands that users are
invoking directly.

There are two ways that we found mach commands can be recursively invoked:
* By running a python subprocess to recursively invoke mach (used in
  `mach bootstrap` to call `mach artifact toolchain`)
* By using `Registrar.dispatch` to delegate to a sub-command (used by many
  build system commands to invoke `mach build`).

The subprocess case is handled here by having mach set a `MACH_MAIN_PID`
environment variable whose value is the current process' pid on startup if it
does not already exist in the environment. Telemetry code then checks that the
value of that variable matches the current pid and skips writing telemetry data
if not.

The dispatch case is handled by making `MachRegistrar` store the current depth
of the command stack and pass it to the `post_dispatch_handler` which will skip
writing telemetry data if depth != 1.

Additionally the `should_skip_dispatch` function in mach_bootstrap is renamed
to `should_skip_telemetry_submission`, which was its original intent. The
combination of checks added in this change should be sufficient for deciding
when to write telemetry data, and we were not collecting telemetry for the set
of mach commands in that function (which included `mach bootstrap`).

In order to facilitate writing a test for the dispatch case this change adds a
`mach python --exec-file` option to execute Python code directly in the context
of the `mach python` command.

Differential Revision: https://phabricator.services.mozilla.com/D11207

diff --git a/build/mach_bootstrap.py b/build/mach_bootstrap.py
--- a/build/mach_bootstrap.py
+++ b/build/mach_bootstrap.py
@@ -199,17 +199,17 @@ def bootstrap(topsrcdir, mozilla_dir=Non
 
         # The environment is likely a machine invocation.
         if sys.stdin.closed or not sys.stdin.isatty():
             return True
 
         return False
 
     def post_dispatch_handler(context, handler, instance, result,
-                              start_time, end_time, args):
+                              start_time, end_time, depth, args):
         """Perform global operations after command dispatch.
 
         """
         # Don't do anything when...
         if should_skip_dispatch(context, handler):
             return
 
         # We call mach environment in client.mk which would cause the
@@ -250,16 +250,22 @@ def bootstrap(topsrcdir, mozilla_dir=Non
         if key == 'post_dispatch_handler':
             return post_dispatch_handler
 
         if key == 'repository':
             return resolve_repository()
 
         raise AttributeError(key)
 
+    # Note which process is top-level so that recursive mach invocations can avoid writing
+    # telemetry data. SM: Put this in os.environ as well, in case other things start
+    # checking for this.
+    if 'MACH_MAIN_PID' not in os.environ:
+        os.environ['MACH_MAIN_PID'] = str(os.getpid())
+
     driver = mach.main.Mach(os.getcwd())
     driver.populate_context_handler = populate_context
 
     if not driver.settings_paths:
         # default global machrc location
         driver.settings_paths.append(get_state_dir()[0])
     # always load local repository configuration
     driver.settings_paths.append(mozilla_dir)
diff --git a/python/mach/mach/registrar.py b/python/mach/mach/registrar.py
--- a/python/mach/mach/registrar.py
+++ b/python/mach/mach/registrar.py
@@ -19,16 +19,17 @@ class MachRegistrar(object):
     """Container for mach command and config providers."""
 
     def __init__(self):
         self.command_handlers = {}
         self.commands_by_category = {}
         self.settings_providers = set()
         self.categories = {}
         self.require_conditions = False
+        self.command_depth = 0
 
     def register_command_handler(self, handler):
         name = handler.name
 
         if not handler.category:
             raise MachError('Cannot register a mach command without a '
                             'category: %s' % name)
 
@@ -77,16 +78,17 @@ class MachRegistrar(object):
             for c in handler.conditions:
                 if not c(instance):
                     fail_conditions.append(c)
 
             if fail_conditions:
                 print(self._condition_failed_message(handler.name, fail_conditions))
                 return 1
 
+        self.command_depth += 1
         fn = getattr(instance, handler.method)
 
         start_time = time.time()
 
         if debug_command:
             import pdb
             result = pdb.runcall(fn, **kwargs)
         else:
@@ -96,17 +98,18 @@ class MachRegistrar(object):
 
         result = result or 0
         assert isinstance(result, (int, long))
 
         if context and not debug_command:
             postrun = getattr(context, 'post_dispatch_handler', None)
             if postrun:
                 postrun(context, handler, instance, result,
-                        start_time, end_time, args=kwargs)
+                        start_time, end_time, self.command_depth, args=kwargs)
+        self.command_depth -= 1
 
         return result
 
     def dispatch(self, name, context=None, argv=None, subcommand=None, **kwargs):
         """Dispatch/run a command.
 
         Commands can use this to call other commands.
         """
diff --git a/python/mach/mach/test/invoke_mach_command.py b/python/mach/mach/test/invoke_mach_command.py
new file mode 100644
--- /dev/null
+++ b/python/mach/mach/test/invoke_mach_command.py
@@ -0,0 +1,4 @@
+import subprocess
+import sys
+
+subprocess.check_call([sys.executable] + sys.argv[1:])
diff --git a/python/mach/mach/test/registrar_dispatch.py b/python/mach/mach/test/registrar_dispatch.py
new file mode 100644
--- /dev/null
+++ b/python/mach/mach/test/registrar_dispatch.py
@@ -0,0 +1,3 @@
+# This code is loaded via `mach python --exec-file`, so it runs in the scope of
+# the `mach python` command.
+self._mach_context.commands.dispatch('uuid', self._mach_context)  # noqa: F821
diff --git a/python/mach_commands.py b/python/mach_commands.py
--- a/python/mach_commands.py
+++ b/python/mach_commands.py
@@ -35,33 +35,40 @@ here = os.path.abspath(os.path.dirname(_
 
 
 @CommandProvider
 class MachCommands(MachCommandBase):
     @Command('python', category='devenv',
              description='Run Python.')
     @CommandArgument('--no-virtualenv', action='store_true',
                      help='Do not set up a virtualenv')
+    @CommandArgument('--exec-file',
+                     default=None,
+                     help='Execute this Python file using `execfile`')
     @CommandArgument('args', nargs=argparse.REMAINDER)
-    def python(self, no_virtualenv, args):
+    def python(self, no_virtualenv, exec_file, args):
         # Avoid logging the command
         self.log_manager.terminal_handler.setLevel(logging.CRITICAL)
 
         # Note: subprocess requires native strings in os.environ on Windows.
         append_env = {
             b'PYTHONDONTWRITEBYTECODE': str('1'),
         }
 
         if no_virtualenv:
             python_path = sys.executable
             append_env[b'PYTHONPATH'] = os.pathsep.join(sys.path)
         else:
             self._activate_virtualenv()
             python_path = self.virtualenv_manager.python_path
 
+        if exec_file:
+            execfile(exec_file)
+            return 0
+
         return self.run_process([python_path] + args,
                                 pass_thru=True,  # Allow user to run Python interactively.
                                 ensure_exit_code=False,  # Don't throw on non-zero exit code.
                                 append_env=append_env)
 
     @Command('python-test', category='testing',
              description='Run Python unit tests with an appropriate test runner.')
     @CommandArgument('-v', '--verbose',
