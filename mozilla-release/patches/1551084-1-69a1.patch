# HG changeset patch
# User Andrew Osmond <aosmond@mozilla.com>
# Date 1557426547 14400
#      Thu May 09 14:29:07 2019 -0400
# Node ID e198d4820748d4708dff1967a9d87658011291e3
# Parent  06921cb39681394998d6b8c19a23089a5e436cee
Bug 1551084 - Part 1. Minor reworking of QCMS to allow C files to compile as C++. r=miko

Differential Revision: https://phabricator.services.mozilla.com/D30818

diff --git a/gfx/qcms/chain.h b/gfx/qcms/chain.h
--- a/gfx/qcms/chain.h
+++ b/gfx/qcms/chain.h
@@ -19,12 +19,20 @@
 // NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
 // LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 // OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 // WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 #ifndef _QCMS_CHAIN_H
 #define _QCMS_CHAIN_H
 
+#ifdef __cplusplus
+extern "C" {
+#endif
+
 // Generates and returns a 3D LUT with lutSize^3 samples using the provided src/dest.
 float* qcms_chain_transform(qcms_profile *in, qcms_profile *out, float *src, float *dest, size_t lutSize);
 
+#ifdef __cplusplus
+}
 #endif
+
+#endif
diff --git a/gfx/qcms/iccread.c b/gfx/qcms/iccread.c
--- a/gfx/qcms/iccread.c
+++ b/gfx/qcms/iccread.c
@@ -72,17 +72,17 @@ static uint16_t be16_to_cpu(be16 v)
 }
 
 /* a wrapper around the memory that we are going to parse
  * into a qcms_profile */
 struct mem_source
 {
 	const unsigned char *buf;
 	size_t size;
-	qcms_bool valid;
+	bool valid;
 	const char *invalid_reason;
 };
 
 static void invalid_source(struct mem_source *mem, const char *reason)
 {
 	mem->valid = false;
 	mem->invalid_reason = reason;
 }
@@ -190,18 +190,18 @@ static void check_profile_version(struct
 #define OUTPUT_DEVICE_PROFILE  0x70727472 // 'prtr'
 #define DEVICE_LINK_PROFILE    0x6c696e6b // 'link'
 #define COLOR_SPACE_PROFILE    0x73706163 // 'spac'
 #define ABSTRACT_PROFILE       0x61627374 // 'abst'
 #define NAMED_COLOR_PROFILE    0x6e6d636c // 'nmcl'
 
 static void read_class_signature(qcms_profile *profile, struct mem_source *mem)
 {
-	profile->class = read_u32(mem, 12);
-	switch (profile->class) {
+	profile->class_type = read_u32(mem, 12);
+	switch (profile->class_type) {
 		case DISPLAY_DEVICE_PROFILE:
 		case INPUT_DEVICE_PROFILE:
 		case OUTPUT_DEVICE_PROFILE:
 		case COLOR_SPACE_PROFILE:
 			break;
 		default:
 			invalid_source(mem, "Invalid  Profile/Device Class signature");
 	}
@@ -264,17 +264,17 @@ static struct tag_index read_tag_table(q
 	}
 
 	return index;
 }
 
 // Checks a profile for obvious inconsistencies and returns
 // true if the profile looks bogus and should probably be
 // ignored.
-qcms_bool qcms_profile_is_bogus(qcms_profile *profile)
+bool qcms_profile_is_bogus(qcms_profile *profile)
 {
        float sum[3], target[3], tolerance[3];
        float rX, rY, rZ, gX, gY, gZ, bX, bY, bZ;
        bool negative;
        unsigned i;
 
        // We currently only check the bogosity of RGB profiles
        if (profile->color_space != RGB_SIGNATURE)
@@ -921,17 +921,17 @@ qcms_profile* qcms_profile_create_rgb_wi
 	profile->redTRC = curve_from_gamma(gamma);
 	profile->blueTRC = curve_from_gamma(gamma);
 	profile->greenTRC = curve_from_gamma(gamma);
 
 	if (!profile->redTRC || !profile->blueTRC || !profile->greenTRC) {
 		qcms_profile_release(profile);
 		return NO_MEM_PROFILE;
 	}
-	profile->class = DISPLAY_DEVICE_PROFILE;
+	profile->class_type = DISPLAY_DEVICE_PROFILE;
 	profile->rendering_intent = QCMS_INTENT_PERCEPTUAL;
 	profile->color_space = RGB_SIGNATURE;
         profile->pcs = XYZ_SIGNATURE;
 	return profile;
 }
 
 qcms_profile* qcms_profile_create_rgb_with_table(
 		qcms_CIE_xyY white_point,
@@ -951,17 +951,17 @@ qcms_profile* qcms_profile_create_rgb_wi
 	profile->redTRC = curve_from_table(table, num_entries);
 	profile->blueTRC = curve_from_table(table, num_entries);
 	profile->greenTRC = curve_from_table(table, num_entries);
 
 	if (!profile->redTRC || !profile->blueTRC || !profile->greenTRC) {
 		qcms_profile_release(profile);
 		return NO_MEM_PROFILE;
 	}
-	profile->class = DISPLAY_DEVICE_PROFILE;
+	profile->class_type = DISPLAY_DEVICE_PROFILE;
 	profile->rendering_intent = QCMS_INTENT_PERCEPTUAL;
 	profile->color_space = RGB_SIGNATURE;
         profile->pcs = XYZ_SIGNATURE;
 	return profile;
 }
 
 /* from lcms: cmsWhitePointFromTemp */
 /* tempK must be >= 4000. and <= 25000.
@@ -1089,18 +1089,18 @@ qcms_profile* qcms_profile_from_memory(c
 		goto invalid_tag_table;
 
 	if (find_tag(index, TAG_CHAD)) {
 		profile->chromaticAdaption = read_tag_s15Fixed16ArrayType(src, index, TAG_CHAD);
 	} else {
 		profile->chromaticAdaption.invalid = true; //Signal the data is not present
 	}
 
-	if (profile->class == DISPLAY_DEVICE_PROFILE || profile->class == INPUT_DEVICE_PROFILE ||
-            profile->class == OUTPUT_DEVICE_PROFILE  || profile->class == COLOR_SPACE_PROFILE) {
+	if (profile->class_type == DISPLAY_DEVICE_PROFILE || profile->class_type == INPUT_DEVICE_PROFILE ||
+            profile->class_type == OUTPUT_DEVICE_PROFILE  || profile->class_type == COLOR_SPACE_PROFILE) {
 		if (profile->color_space == RGB_SIGNATURE) {
 			if (find_tag(index, TAG_A2B0)) {
 				if (read_u32(src, find_tag(index, TAG_A2B0)->offset) == LUT8_TYPE ||
 				    read_u32(src, find_tag(index, TAG_A2B0)->offset) == LUT16_TYPE) {
 					profile->A2B0 = read_tag_lutType(src, index, TAG_A2B0);
 				} else if (read_u32(src, find_tag(index, TAG_A2B0)->offset) == LUT_MAB_TYPE) {
 					profile->mAB = read_tag_lutmABType(src, index, TAG_A2B0);
 				}
@@ -1400,17 +1400,17 @@ void qcms_data_create_rgb_with_gamma(qcm
 
 	/* Part3 : write profile header
 	 *
 	 * Important header fields are left empty. This generates a profile for internal use only.
 	 * We should be generating: Profile version (04300000h), Profile signature (acsp), 
 	 * PCS illumiant field. Likewise mandatory profile tags are omitted.
 	 */
 	write_u32(data, 0, length); // the total length of this memory
-	write_u32(data, 12, DISPLAY_DEVICE_PROFILE); // profile->class
+	write_u32(data, 12, DISPLAY_DEVICE_PROFILE); // profile->class_type
 	write_u32(data, 16, RGB_SIGNATURE); // profile->color_space
 	write_u32(data, 20, XYZ_SIGNATURE); // profile->pcs
 	write_u32(data, 64, QCMS_INTENT_PERCEPTUAL); // profile->rendering_intent
 
 	write_u32(data, ICC_PROFILE_HEADER_LENGTH, 6); // total tag count
 
 	// prepare the result
 	*mem = data;
diff --git a/gfx/qcms/matrix.h b/gfx/qcms/matrix.h
--- a/gfx/qcms/matrix.h
+++ b/gfx/qcms/matrix.h
@@ -19,21 +19,29 @@
 // NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
 // LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
 // OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION 
 // WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 #ifndef _QCMS_MATRIX_H
 #define _QCMS_MATRIX_H
 
+#ifdef __cplusplus
+extern "C" {
+#endif
+
 struct vector {
         float v[3];
 };
 
 struct vector matrix_eval(struct matrix mat, struct vector v);
 float matrix_det(struct matrix mat);
 struct matrix matrix_identity(void);
 struct matrix matrix_multiply(struct matrix a, struct matrix b);
 struct matrix matrix_invert(struct matrix mat);
 
 struct matrix matrix_invalid(void);
 
+#ifdef __cplusplus
+}
 #endif
+
+#endif
diff --git a/gfx/qcms/moz.build b/gfx/qcms/moz.build
--- a/gfx/qcms/moz.build
+++ b/gfx/qcms/moz.build
@@ -35,14 +35,14 @@ if CONFIG['INTEL_ARCHITECTURE']:
 elif CONFIG['HAVE_ALTIVEC']:
     use_altivec = True
 
 if use_sse1:
     SOURCES += ['transform-sse1.c']
     SOURCES['transform-sse1.c'].flags += CONFIG['SSE_FLAGS']
 
 if use_sse2:
-    SOURCES += ['transform-sse2.c']
-    SOURCES['transform-sse2.c'].flags += CONFIG['SSE2_FLAGS']
+    SOURCES += ['transform-sse2.cpp']
+    SOURCES['transform-sse2.cpp'].flags += CONFIG['SSE2_FLAGS']
 
 if use_altivec:
     SOURCES += ['transform-altivec.c']
     SOURCES['transform-altivec.c'].flags += ['-maltivec']
diff --git a/gfx/qcms/qcms.h b/gfx/qcms/qcms.h
--- a/gfx/qcms/qcms.h
+++ b/gfx/qcms/qcms.h
@@ -77,18 +77,17 @@ typedef enum {
     icSig13colorData                    = 0x44434C52L,  /* 'DCLR' */
     icSig14colorData                    = 0x45434C52L,  /* 'ECLR' */
     icSig15colorData                    = 0x46434C52L,  /* 'FCLR' */
     icMaxEnumData                       = 0xFFFFFFFFL   
 } icColorSpaceSignature;
 #endif
 
 #include <stdio.h>
-
-typedef int qcms_bool;
+#include <stdbool.h>
 
 struct _qcms_transform;
 typedef struct _qcms_transform qcms_transform;
 
 struct _qcms_profile;
 typedef struct _qcms_profile qcms_profile;
 
 /* these values match the Rendering Intent values from the ICC spec */
@@ -150,17 +149,17 @@ void qcms_data_from_path(const char *pat
 
 #ifdef _WIN32
 qcms_profile* qcms_profile_from_unicode_path(const wchar_t *path);
 void qcms_data_from_unicode_path(const wchar_t *path, void **mem, size_t *size);
 #endif
 qcms_profile* qcms_profile_sRGB(void);
 void qcms_profile_release(qcms_profile *profile);
 
-qcms_bool qcms_profile_is_bogus(qcms_profile *profile);
+bool qcms_profile_is_bogus(qcms_profile *profile);
 qcms_intent qcms_profile_get_rendering_intent(qcms_profile *profile);
 icColorSpaceSignature qcms_profile_get_color_space(qcms_profile *profile);
 
 void qcms_profile_precache_output_transform(qcms_profile *profile);
 
 qcms_transform* qcms_transform_create(
 		qcms_profile *in, qcms_data_type in_type,
 		qcms_profile* out, qcms_data_type out_type,
diff --git a/gfx/qcms/qcmsint.h b/gfx/qcms/qcmsint.h
--- a/gfx/qcms/qcmsint.h
+++ b/gfx/qcms/qcmsint.h
@@ -1,12 +1,19 @@
 /* vim: set ts=8 sw=8 noexpandtab: */
+#ifndef QCMS_INT_H
+#define QCMS_INT_H
+
 #include "qcms.h"
 #include "qcmstypes.h"
 
+#ifdef __cplusplus
+extern "C" {
+#endif
+
 /* used as a lookup table for the output transformation.
  * we refcount them so we only need to have one around per output
  * profile, instead of duplicating them per transform */
 struct precache_output
 {
 	int ref_count;
 	/* We previously used a count of 65536 here but that seems like more
 	 * precision than we actually need.  By reducing the size we can
@@ -199,17 +206,17 @@ struct tag_value {
 #endif
 
 #define RGB_SIGNATURE  0x52474220
 #define GRAY_SIGNATURE 0x47524159
 #define XYZ_SIGNATURE  0x58595A20
 #define LAB_SIGNATURE  0x4C616220
 
 struct _qcms_profile {
-	uint32_t class;
+	uint32_t class_type;
 	uint32_t color_space;
 	uint32_t pcs;
 	qcms_intent rendering_intent;
 	struct XYZNumber redColorant;
 	struct XYZNumber blueColorant;
 	struct XYZNumber greenColorant;
 	struct curveType *redTRC;
 	struct curveType *blueTRC;
@@ -249,18 +256,18 @@ static inline float uInt8Number_to_float
 
 static inline float uInt16Number_to_float(uInt16Number a)
 {
 	return ((int32_t)a)/65535.f;
 }
 
 
 void precache_release(struct precache_output *p);
-qcms_bool set_rgb_colorants(qcms_profile *profile, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries);
-qcms_bool get_rgb_colorants(struct matrix *colorants, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries);
+bool set_rgb_colorants(qcms_profile *profile, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries);
+bool get_rgb_colorants(struct matrix *colorants, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries);
 
 void qcms_transform_data_rgb_out_lut_sse2(qcms_transform *transform,
                                           unsigned char *src,
                                           unsigned char *dest,
                                           size_t length);
 void qcms_transform_data_rgba_out_lut_sse2(qcms_transform *transform,
                                           unsigned char *src,
                                           unsigned char *dest,
@@ -278,17 +285,17 @@ void qcms_transform_data_rgb_out_lut_alt
                                              unsigned char *src,
                                              unsigned char *dest,
                                              size_t length);
 void qcms_transform_data_rgba_out_lut_altivec(qcms_transform *transform,
                                               unsigned char *src,
                                               unsigned char *dest,
                                               size_t length);
 
-extern qcms_bool qcms_supports_iccv4;
+extern bool qcms_supports_iccv4;
 
 #ifdef _MSC_VER
 
 long __cdecl _InterlockedIncrement(long volatile *);
 long __cdecl _InterlockedDecrement(long volatile *);
 #pragma intrinsic(_InterlockedIncrement)
 #pragma intrinsic(_InterlockedDecrement)
 
@@ -320,8 +327,14 @@ long __cdecl _InterlockedDecrement(long 
 #else
 # define RGB_OUTPUT_COMPONENTS 3
 # define RGBA_OUTPUT_COMPONENTS 4
 # define OUTPUT_R_INDEX 0
 # define OUTPUT_G_INDEX 1
 # define OUTPUT_B_INDEX 2
 # define OUTPUT_A_INDEX 3
 #endif
+
+#ifdef __cplusplus
+}
+#endif
+
+#endif
diff --git a/gfx/qcms/qcmstypes.h b/gfx/qcms/qcmstypes.h
--- a/gfx/qcms/qcmstypes.h
+++ b/gfx/qcms/qcmstypes.h
@@ -1,26 +1,32 @@
 #ifndef QCMS_TYPES_H
 #define QCMS_TYPES_H
 
+#ifdef __cplusplus
+extern "C" {
+#endif
+
+#if !defined(IS_LITTLE_ENDIAN) && !defined(IS_BIG_ENDIAN)
 #if BYTE_ORDER == LITTLE_ENDIAN
 #define IS_LITTLE_ENDIAN
 #elif BYTE_ORDER == BIG_ENDIAN
 #define IS_BIG_ENDIAN
 #endif
 
 /* all of the platforms that we use _MSC_VER on are little endian
  * so this is sufficient for now */
 #ifdef _MSC_VER
 #define IS_LITTLE_ENDIAN
 #endif
 
 #ifdef __OS2__
 #define IS_LITTLE_ENDIAN
 #endif
+#endif
 
 #if !defined(IS_LITTLE_ENDIAN) && !defined(IS_BIG_ENDIAN)
 #error Unknown endianess
 #endif
 
 #if defined (_SVR4) || defined (SVR4) || defined (__OpenBSD__) || defined (_sgi) || defined (__sun) || defined (sun) || defined (__digital__)
 #  include <inttypes.h>
 #elif defined (_MSC_VER) && _MSC_VER < 1600
@@ -39,13 +45,15 @@ typedef unsigned long uintptr_t;
 #endif
 
 #elif defined (_AIX)
 #  include <sys/inttypes.h>
 #else
 #  include <stdint.h>
 #endif
 
-typedef qcms_bool bool;
-#define true 1
-#define false 0
+#include <stdbool.h>
+
+#ifdef __cplusplus
+}
+#endif
 
 #endif
diff --git a/gfx/qcms/transform.c b/gfx/qcms/transform.c
--- a/gfx/qcms/transform.c
+++ b/gfx/qcms/transform.c
@@ -46,17 +46,17 @@
 #include <unistd.h>
 #include <fcntl.h>
 #include <stdio.h>
 #include <elf.h>
 #include <linux/auxvec.h>
 #include <asm/cputable.h>
 #include <link.h>
 
-static inline qcms_bool have_altivec() {
+static inline bool have_altivec() {
 	static int available = -1;
 	int new_avail = 0;
         ElfW(auxv_t) auxv;
 	ssize_t count;
 	int fd, i;
 
 	if (available != -1)
 		return (available != 0 ? true : false);
@@ -83,17 +83,17 @@ out:
 }
 #elif defined(__APPLE__) && defined(__MACH__)
 #include <sys/sysctl.h>
 
 /**
  * rip-off from ffmpeg AltiVec detection code.
  * this code also appears on Apple's AltiVec pages.
  */
-static inline qcms_bool have_altivec() {
+static inline bool have_altivec() {
 	int sels[2] = {CTL_HW, HW_VECTORUNIT};
 	static int available = -1;
 	size_t len = sizeof(available);
 	int err;
 
 	if (available != -1)
 		return (available != 0 ? true : false);
 
@@ -286,17 +286,17 @@ static struct matrix adapt_matrix_to_D50
 
 	Bradford = adaption_matrix(Dn, D50_XYZ);
 	if (Bradford.invalid) {
 		return matrix_invalid();
 	}
 	return matrix_multiply(Bradford, r);
 }
 
-qcms_bool set_rgb_colorants(qcms_profile *profile, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries)
+bool set_rgb_colorants(qcms_profile *profile, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries)
 {
 	struct matrix colorants;
 	colorants = build_RGB_to_XYZ_transfer_matrix(white_point, primaries);
 	colorants = adapt_matrix_to_D50(colorants, white_point);
 
 	if (colorants.invalid)
 		return false;
 
@@ -311,17 +311,17 @@ qcms_bool set_rgb_colorants(qcms_profile
 
 	profile->blueColorant.X = double_to_s15Fixed16Number(colorants.m[0][2]);
 	profile->blueColorant.Y = double_to_s15Fixed16Number(colorants.m[1][2]);
 	profile->blueColorant.Z = double_to_s15Fixed16Number(colorants.m[2][2]);
 
 	return true;
 }
 
-qcms_bool get_rgb_colorants(struct matrix *colorants, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries)
+bool get_rgb_colorants(struct matrix *colorants, qcms_CIE_xyY white_point, qcms_CIE_xyYTRIPLE primaries)
 {
 	*colorants = build_RGB_to_XYZ_transfer_matrix(white_point, primaries);
 	*colorants = adapt_matrix_to_D50(*colorants, white_point);
 
 	return (colorants->invalid ? true : false);
 }
 
 #if 0
@@ -915,17 +915,17 @@ static void qcms_transform_data_rgb_out_
 static struct precache_output *precache_reference(struct precache_output *p)
 {
 	qcms_atomic_increment(p->ref_count);
 	return p;
 }
 
 static struct precache_output *precache_create()
 {
-	struct precache_output *p = malloc(sizeof(struct precache_output));
+	struct precache_output *p = (struct precache_output*)malloc(sizeof(struct precache_output));
 	if (p)
 		p->ref_count = 1;
 	return p;
 }
 
 void precache_release(struct precache_output *p)
 {
 	if (qcms_atomic_decrement(p->ref_count) == 0) {
@@ -937,31 +937,31 @@ void precache_release(struct precache_ou
 static qcms_transform *transform_alloc(void)
 {
 	qcms_transform *t;
 
 	void *allocated_memory;
 	if (!posix_memalign(&allocated_memory, 16, sizeof(qcms_transform))) {
 		/* Doing a memset to initialise all bits to 'zero'*/
 		memset(allocated_memory, 0, sizeof(qcms_transform));
-		t = allocated_memory;
+		t = (qcms_transform*)allocated_memory;
 		return t;
 	} else {
 		return NULL;
 	}
 }
 static void transform_free(qcms_transform *t)
 {
 	free(t);
 }
 #else
 static qcms_transform *transform_alloc(void)
 {
 	/* transform needs to be aligned on a 16byte boundrary */
-	char *original_block = calloc(sizeof(qcms_transform) + sizeof(void*) + 16, 1);
+	char *original_block = (char *)calloc(sizeof(qcms_transform) + sizeof(void*) + 16, 1);
 	/* make room for a pointer to the block returned by calloc */
 	void *transform_start = original_block + sizeof(void*);
 	/* align transform_start */
 	qcms_transform *transform_aligned = (qcms_transform*)(((uintptr_t)transform_start + 15) & ~0xf);
 
 	/* store a pointer to the block returned by calloc so that we can free it later */
 	void **(original_block_ptr) = (void**)transform_aligned;
 	if (!original_block)
@@ -1168,18 +1168,18 @@ qcms_transform* qcms_transform_precacheL
 	/* The range between which 2 consecutive sample points can be used to interpolate */
 	uint16_t x,y,z;
 	uint32_t l;
 	uint32_t lutSize = 3 * samples * samples * samples;
 	float* src = NULL;
 	float* dest = NULL;
 	float* lut = NULL;
 
-	src = malloc(lutSize*sizeof(float));
-	dest = malloc(lutSize*sizeof(float));
+	src = (float*)malloc(lutSize*sizeof(float));
+	dest = (float*)malloc(lutSize*sizeof(float));
 
 	if (src && dest) {
 		/* Prepare a list of points we want to sample */
 		l = 0;
 		for (x = 0; x < samples; x++) {
 			for (y = 0; y < samples; y++) {
 				for (z = 0; z < samples; z++) {
 					src[l++] = x / (float)(samples-1);
@@ -1410,16 +1410,16 @@ qcms_transform* qcms_transform_create(
 }
 
 #if defined(__GNUC__) && defined(__i386__)
 /* we need this to avoid crashes when gcc assumes the stack is 128bit aligned */
 __attribute__((__force_align_arg_pointer__))
 #endif
 void qcms_transform_data(qcms_transform *transform, void *src, void *dest, size_t length)
 {
-	transform->transform_fn(transform, src, dest, length);
+	transform->transform_fn(transform, (unsigned char*)src, (unsigned char*)dest, length);
 }
 
-qcms_bool qcms_supports_iccv4;
+bool qcms_supports_iccv4;
 void qcms_enable_iccv4()
 {
 	qcms_supports_iccv4 = true;
 }
diff --git a/gfx/qcms/transform_util.c b/gfx/qcms/transform_util.c
--- a/gfx/qcms/transform_util.c
+++ b/gfx/qcms/transform_util.c
@@ -384,17 +384,17 @@ void compute_precache_linear(uint8_t *ou
 {
 	uint32_t v = 0;
 	for (v = 0; v < PRECACHE_OUTPUT_SIZE; v++) {
 		//XXX: round?
 		output[v] = v / (PRECACHE_OUTPUT_SIZE/256);
 	}
 }
 
-qcms_bool compute_precache(struct curveType *trc, uint8_t *output)
+bool compute_precache(struct curveType *trc, uint8_t *output)
 {
         
         if (trc->type == PARAMETRIC_CURVE_TYPE) {
                         float gamma_table[256];
                         uint16_t gamma_table_uint[256];
                         uint16_t i;
                         uint16_t *inverted;
                         int inverted_size = 256;
diff --git a/gfx/qcms/transform_util.h b/gfx/qcms/transform_util.h
--- a/gfx/qcms/transform_util.h
+++ b/gfx/qcms/transform_util.h
@@ -22,16 +22,20 @@
 // WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 #ifndef _QCMS_TRANSFORM_UTIL_H
 #define _QCMS_TRANSFORM_UTIL_H
 
 #include <stdlib.h>
 #include <math.h>
 
+#ifdef __cplusplus
+extern "C" {
+#endif
+
 #define CLU(table,x,y,z) table[(x*len + y*x_len + z*xy_len)*3]
 
 //XXX: could use a bettername
 typedef uint16_t uint16_fract_t;
 
 float lut_interp_linear(double value, uint16_t *table, int length);
 float lut_interp_linear_float(float value, float *table, int length);
 uint16_t lut_interp_linear16(uint16_t input_value, uint16_t *table, int length);
@@ -83,22 +87,17 @@ static inline float u8Fixed8Number_to_fl
 }
 
 float *build_input_gamma_table(struct curveType *TRC);
 struct matrix build_colorant_matrix(qcms_profile *p);
 void build_output_lut(struct curveType *trc,
                       uint16_t **output_gamma_lut, size_t *output_gamma_lut_length);
 
 struct matrix matrix_invert(struct matrix mat);
-qcms_bool compute_precache(struct curveType *trc, uint8_t *output);
-
-// Tested by GTest
-#ifdef  __cplusplus
-extern "C" {
-#endif
+bool compute_precache(struct curveType *trc, uint8_t *output);
 
 uint16_fract_t lut_inverse_interp16(uint16_t Value, uint16_t LutTable[], int length);
 
 #ifdef  __cplusplus
 }
 #endif
 
 #endif
diff --git a/gfx/tests/gtest/TestQcms.cpp b/gfx/tests/gtest/TestQcms.cpp
--- a/gfx/tests/gtest/TestQcms.cpp
+++ b/gfx/tests/gtest/TestQcms.cpp
@@ -3,16 +3,17 @@
  * http://creativecommons.org/publicdomain/zero/1.0/
  */
 
 #include "gtest/gtest.h"
 #include "gmock/gmock.h"
 
 #include "mozilla/ArrayUtils.h"
 #include "qcms.h"
+#include "qcmsint.h"
 #include "transform_util.h"
 
 const size_t allGBSize = 1 * 256 * 256 * 4;
 static unsigned char* createAllGB() {
   unsigned char* buff = (unsigned char*)malloc(allGBSize);
   int pos = 0;
   for (int r = 0; r < 1; r++) { // Skip all R values for speed
     for (int g = 0; g < 256; g++) {
