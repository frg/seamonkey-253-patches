# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1627515254 0
# Node ID e1e0316f3b91ff0732512d384054865da10980a1
# Parent  278f815cc8a2c56da106de1565717e2424f9f134
Bug 1722540 - Fix `gyp.common.RelativePath`'s handling of case. r=firefox-build-system-reviewers,nalexander

For some reason, it's not using os.path.relpath, but also doesn't handle
case sensitivity correctly, which os.path.relpath does. However, it
has some differences with os.path.relpath that need to be kept:
- os.path.relpath throws an exception when both paths have a different
drive letter.
- os.path.relpath returns os.path.curdir when both paths are identical.
- the follow_path_symlink flag is not supported by os.path.relpath.

Differential Revision: https://phabricator.services.mozilla.com/D121145

diff --git a/third_party/python/gyp/pylib/gyp/common.py b/third_party/python/gyp/pylib/gyp/common.py
--- a/third_party/python/gyp/pylib/gyp/common.py
+++ b/third_party/python/gyp/pylib/gyp/common.py
@@ -150,34 +150,22 @@ def RelativePath(path, relative_to, foll
 
   # On Windows, we can't create a relative path to a different drive, so just
   # use the absolute path.
   if sys.platform == 'win32':
     if (os.path.splitdrive(path)[0].lower() !=
         os.path.splitdrive(relative_to)[0].lower()):
       return path
 
-  # Split the paths into components.
-  path_split = path.split(os.path.sep)
-  relative_to_split = relative_to.split(os.path.sep)
-
-  # Determine how much of the prefix the two paths share.
-  prefix_len = len(os.path.commonprefix([path_split, relative_to_split]))
-
-  # Put enough ".." components to back up out of relative_to to the common
-  # prefix, and then append the part of path_split after the common prefix.
-  relative_split = [os.path.pardir] * (len(relative_to_split) - prefix_len) + \
-                   path_split[prefix_len:]
-
-  if len(relative_split) == 0:
+  relative = os.path.relpath(path, relative_to)
+  if relative == os.path.curdir:
     # The paths were the same.
     return ''
 
-  # Turn it back into a string and we're done.
-  return os.path.join(*relative_split)
+  return relative
 
 
 @memoize
 def InvertRelativePath(path, toplevel_dir=None):
   """Given a path like foo/bar that is relative to toplevel_dir, return
   the inverse relative path back to the toplevel_dir.
 
   E.g. os.path.normpath(os.path.join(path, InvertRelativePath(path)))

