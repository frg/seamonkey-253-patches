# HG changeset patch
# User Andreas Tolfsen <ato@sny.no>
# Date 1520525135 0
# Node ID 7a1e77dd53f6ea39c364acda678c3bb52405fee4
# Parent  fdd321dd569223ea64ed9b7224bfeba1e921f39d
Bug 1401129 - Note changes for geckodriver 0.20.0 release. r=whimboo

MozReview-Commit-ID: 1vb4nzpUlYD

diff --git a/testing/geckodriver/CHANGES.md b/testing/geckodriver/CHANGES.md
--- a/testing/geckodriver/CHANGES.md
+++ b/testing/geckodriver/CHANGES.md
@@ -1,39 +1,55 @@
 Change log
 ==========
 
 All notable changes to this program is documented in this file.
 
-Unreleased
-----------
+0.20.0 (2018-03-08)
+-------------------
 
 ### Added
 
-- New `--jsdebugger` flag to open the Browser Toolbox when Firefox
+- New `--jsdebugger` flag to open the [Browser Toolbox] when Firefox
   launches.  This is useful for debugging Marionette internals
 
 - Introduced the temporary, boolean capability
   `moz:useNonSpecCompliantPointerOrigin` to disable the WebDriver
   conforming behavior of calculating the Pointer Origin
 
 ### Changed
 
 - HTTP status code for the [`StaleElementReference`] error changed
-  from 400 (Bad Request) to 404 (Not Found)
+  from 400 (Bad Request) to 404 (Not Found).
 
 - Backtraces from geckodriver no longer substitute for missing
-  Marionette stacktraces
-
-- `Delete Session` now allows Firefox to safely shutdown within 70s before
-  force-killing the process
+  Marionette stacktraces.
 
 ### Fixed
 
-- Improved error messages for malformed capabilities
+- The Firefox process is now given ample time to shut down, allowing
+  enough time for the Firefox shutdown hang monitor to kick in.
+
+  Firefox has an integrated background monitor that observes
+  long-running threads during shutdown.  These threads will be
+  killed after 63 seconds in the event of a hang.  To allow Firefox
+  to shut down these threads on its own, geckodriver has to wait
+  that time and some additional seconds.
+
+- Grapheme clusters are now accepted as input for keyboard input
+  to actions.
+
+  Input to the `value` field of the `keyDown` and `keyUp` action
+  primitives used to only accept single characters, which means
+  geckodriver would error when a valid grapheme cluster was sent in,
+  for example with the tamil nadu character U+0BA8 U+0BBF.
+
+  Thanks to Greg Fraley for fixing this bug.
+
+- Improved error messages for malformed capability values.
 
 
 0.19.1 (2017-10-30)
 -------------------
 
 ### Changed
 
 - Search suggestions in the location bar turned off as not to
@@ -786,19 +802,21 @@ 0.1.0 (2015-04-09)
 
 - Fixed build by updating Cargo.lock with new dependencies for building
 
 - Squash compile warnings
 
 
 
 [README]: https://github.com/mozilla/geckodriver/blob/master/README.md
+[Browser Toolbox]: https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox
 
 [`CloseWindowResponse`]: https://docs.rs/webdriver/newest/webdriver/response/struct.CloseWindowResponse.html
 [`CookieResponse`]: https://docs.rs/webdriver/newest/webdriver/response/struct.CookieResponse.html
+[`DeleteSession`]: https://docs.rs/webdriver/newest/webdriver/command/enum.WebDriverCommand.html#variant.DeleteSession
 [`ElementClickIntercepted`]: https://docs.rs/webdriver/newest/webdriver/error/enum.ErrorStatus.html#variant.ElementClickIntercepted
 [`ElementNotInteractable`]: https://docs.rs/webdriver/newest/webdriver/error/enum.ErrorStatus.html#variant.ElementNotInteractable
 [`FullscreenWindow`]: https://docs.rs/webdriver/newest/webdriver/command/enum.WebDriverCommand.html#variant.FullscreenWindow
 [`GetNamedCookie`]: https://docs.rs/webdriver/newest/webdriver/command/enum.WebDriverCommand.html#variant.GetNamedCookie
 [`GetWindowRect`]: https://docs.rs/webdriver/newest/webdriver/command/enum.WebDriverCommand.html#variant.GetWindowRect
 [`InvalidCoordinates`]: https://docs.rs/webdriver/newest/webdriver/error/enum.ErrorStatus.html#variant.InvalidCoordinates
 [`MaximizeWindow`]: https://docs.rs/webdriver/newest/webdriver/command/enum.WebDriverCommand.html#variant.MaximizeWindow
 [`MinimizeWindow`]: https://docs.rs/webdriver/newest/webdriver/command/enum.WebDriverCommand.html#variant.MinimizeWindow
@@ -814,25 +832,26 @@ 0.1.0 (2015-04-09)
 [`UnknownCommand`]: https://docs.rs/webdriver/newest/webdriver/error/enum.ErrorStatus.html#variant.UnknownCommand
 [`UnknownError`]: https://docs.rs/webdriver/newest/webdriver/error/enum.ErrorStatus.html#variant.UnknownError
 [`WindowRectParameters`]: https://docs.rs/webdriver/newest/webdriver/command/struct.WindowRectParameters.html
 
 [mozrunner crate]: https://crates.io/crates/mozrunner
 [webdriver crate]: https://crates.io/crates/webdriver
 
 [Actions]: https://w3c.github.io/webdriver/webdriver-spec.html#actions
+[Delete Session]: https://w3c.github.io/webdriver/webdriver-spec.html#delete-session
 [Element Click]: https://w3c.github.io/webdriver/webdriver-spec.html#element-click
 [Get Timeouts]: https://w3c.github.io/webdriver/webdriver-spec.html#get-timeouts
-[Set Timeouts]: https://w3c.github.io/webdriver/webdriver-spec.html#set-timeouts
 [Get Window Rect]: https://w3c.github.io/webdriver/webdriver-spec.html#get-window-rect
-[Set Window Rect]: https://w3c.github.io/webdriver/webdriver-spec.html#set-window-rect
 [insecure certificate]: https://w3c.github.io/webdriver/webdriver-spec.html#dfn-insecure-certificate
 [Minimize Window]: https://w3c.github.io/webdriver/webdriver-spec.html#minimize-window
 [New Session]: https://w3c.github.io/webdriver/webdriver-spec.html#new-session
 [Send Alert Text]: https://w3c.github.io/webdriver/webdriver-spec.html#send-alert-text
+[Set Timeouts]: https://w3c.github.io/webdriver/webdriver-spec.html#set-timeouts
+[Set Window Rect]: https://w3c.github.io/webdriver/webdriver-spec.html#set-window-rect
 [Status]: https://w3c.github.io/webdriver/webdriver-spec.html#status
 [Take Element Screenshot]: https://w3c.github.io/webdriver/webdriver-spec.html#take-element-screenshot
 [WebDriver errors]: https://w3c.github.io/webdriver/webdriver-spec.html#handling-errors
 
 [Jason Juang]: https://github.com/juangj
 [Joshua Bruning]: https://github.com/joshbruning
 [Kalpesh Krishna]: https://github.com/martiansideofthemoon
 [Mike Pennisi]: https://github.com/jugglinmike
