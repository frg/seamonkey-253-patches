# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1521412907 -3600
# Node ID 05270b574f0085299f8d0f3477a8ad7ba939196c
# Parent  1fe192e4d736c7de3e3c5df56b7c57e1a58419ef
Bug 1446342: Don't include forms in the scope chain for XBL datetime bindings. r=smaug

Reviewers: smaug

Bug #: 1446342

Differential Revision: https://phabricator.services.mozilla.com/D769

MozReview-Commit-ID: HK7nChYf0X6

diff --git a/dom/base/nsGkAtomList.h b/dom/base/nsGkAtomList.h
--- a/dom/base/nsGkAtomList.h
+++ b/dom/base/nsGkAtomList.h
@@ -1662,16 +1662,17 @@ GK_ATOM(requiredFeatures, "requiredFeatu
 GK_ATOM(rotate, "rotate")
 GK_ATOM(rx, "rx")
 GK_ATOM(ry, "ry")
 GK_ATOM(saturate, "saturate")
 GK_ATOM(saturation, "saturation")
 //GK_ATOM(set, "set")  # "set" is present below
 GK_ATOM(seed, "seed")
 GK_ATOM(shape_rendering, "shape-rendering")
+GK_ATOM(simpleScopeChain, "simpleScopeChain")
 GK_ATOM(skewX, "skewX")
 GK_ATOM(skewY, "skewY")
 GK_ATOM(slope, "slope")
 GK_ATOM(slot, "slot")
 GK_ATOM(softLight, "soft-light")
 GK_ATOM(spacing, "spacing")
 GK_ATOM(spacingAndGlyphs, "spacingAndGlyphs")
 GK_ATOM(specularConstant, "specularConstant")
diff --git a/dom/base/nsJSUtils.cpp b/dom/base/nsJSUtils.cpp
--- a/dom/base/nsJSUtils.cpp
+++ b/dom/base/nsJSUtils.cpp
@@ -21,16 +21,17 @@
 #include "nsCOMPtr.h"
 #include "nsIScriptSecurityManager.h"
 #include "nsPIDOMWindow.h"
 #include "GeckoProfiler.h"
 #include "nsJSPrincipals.h"
 #include "xpcpublic.h"
 #include "nsContentUtils.h"
 #include "nsGlobalWindow.h"
+#include "nsXBLPrototypeBinding.h"
 #include "mozilla/CycleCollectedJSContext.h"
 #include "mozilla/dom/BindingUtils.h"
 #include "mozilla/dom/Date.h"
 #include "mozilla/dom/Element.h"
 #include "mozilla/dom/ScriptSettings.h"
 
 using namespace mozilla;
 using namespace mozilla::dom;
@@ -458,37 +459,74 @@ nsJSUtils::ModuleEvaluate(JSContext* aCx
 
   if (!JS::ModuleEvaluate(aCx, aModule)) {
     return NS_ERROR_FAILURE;
   }
 
   return NS_OK;
 }
 
+static bool
+AddScopeChainItem(JSContext* aCx,
+                  nsINode* aNode,
+                  JS::AutoObjectVector& aScopeChain)
+{
+  JS::RootedValue val(aCx);
+  if (!GetOrCreateDOMReflector(aCx, aNode, &val)) {
+    return false;
+  }
+
+  if (!aScopeChain.append(&val.toObject())) {
+    return false;
+  }
+
+  return true;
+}
+
 /* static */
 bool
 nsJSUtils::GetScopeChainForElement(JSContext* aCx,
-                                   mozilla::dom::Element* aElement,
+                                   Element* aElement,
                                    JS::AutoObjectVector& aScopeChain)
 {
   for (nsINode* cur = aElement; cur; cur = cur->GetScopeChainParent()) {
-    JS::RootedValue val(aCx);
-    if (!GetOrCreateDOMReflector(aCx, cur, &val)) {
-      return false;
-    }
-
-    if (!aScopeChain.append(&val.toObject())) {
+    if (!AddScopeChainItem(aCx, cur, aScopeChain)) {
       return false;
     }
   }
 
   return true;
 }
 
 /* static */
+bool
+nsJSUtils::GetScopeChainForXBL(JSContext* aCx,
+                               Element* aElement,
+                               const nsXBLPrototypeBinding& aProtoBinding,
+                               JS::AutoObjectVector& aScopeChain)
+{
+  if (!aElement) {
+    return true;
+  }
+
+  if (!aProtoBinding.SimpleScopeChain()) {
+    return GetScopeChainForElement(aCx, aElement, aScopeChain);
+  }
+
+  if (!AddScopeChainItem(aCx, aElement, aScopeChain)) {
+    return false;
+  }
+
+  if (!AddScopeChainItem(aCx, aElement->OwnerDoc(), aScopeChain)) {
+    return false;
+  }
+  return true;
+}
+
+/* static */
 void
 nsJSUtils::ResetTimeZone()
 {
   JS::ResetTimeZone();
 }
 
 //
 // nsDOMJSUtils.h
diff --git a/dom/base/nsJSUtils.h b/dom/base/nsJSUtils.h
--- a/dom/base/nsJSUtils.h
+++ b/dom/base/nsJSUtils.h
@@ -20,16 +20,17 @@
 #include "jsapi.h"
 #include "jsfriendapi.h"
 #include "js/Conversions.h"
 #include "nsString.h"
 
 class nsIScriptContext;
 class nsIScriptElement;
 class nsIScriptGlobalObject;
+class nsXBLPrototypeBinding;
 
 namespace mozilla {
 namespace dom {
 class AutoJSAPI;
 class Element;
 } // namespace dom
 } // namespace mozilla
 
@@ -197,16 +198,27 @@ public:
                                  JS::Handle<JSObject*> aModule);
 
   // Returns false if an exception got thrown on aCx.  Passing a null
   // aElement is allowed; that wil produce an empty aScopeChain.
   static bool GetScopeChainForElement(JSContext* aCx,
                                       mozilla::dom::Element* aElement,
                                       JS::AutoObjectVector& aScopeChain);
 
+  // Returns a scope chain suitable for XBL execution.
+  //
+  // This is by default GetScopeChainForElemenet, but will be different if the
+  // <binding> element had the simpleScopeChain attribute.
+  //
+  // This is to prevent footguns like bug 1446342.
+  static bool GetScopeChainForXBL(JSContext* aCx,
+                                  mozilla::dom::Element* aBoundElement,
+                                  const nsXBLPrototypeBinding& aProtoBinding,
+                                  JS::AutoObjectVector& aScopeChain);
+
   static void ResetTimeZone();
 };
 
 template<typename T>
 inline bool
 AssignJSString(JSContext *cx, T &dest, JSString *s)
 {
   size_t len = js::GetStringLength(s);
diff --git a/dom/xbl/nsXBLProtoImplField.cpp b/dom/xbl/nsXBLProtoImplField.cpp
--- a/dom/xbl/nsXBLProtoImplField.cpp
+++ b/dom/xbl/nsXBLProtoImplField.cpp
@@ -203,17 +203,17 @@ InstallXBLField(JSContext* cx,
     JS::Value slotVal = ::JS_GetReservedSlot(xblProto, 0);
     protoBinding = static_cast<nsXBLPrototypeBinding*>(slotVal.toPrivate());
     MOZ_ASSERT(protoBinding);
   }
 
   nsXBLProtoImplField* field = protoBinding->FindField(fieldName);
   MOZ_ASSERT(field);
 
-  nsresult rv = field->InstallField(thisObj, protoBinding->DocURI(), installed);
+  nsresult rv = field->InstallField(thisObj, *protoBinding, installed);
   if (NS_SUCCEEDED(rv)) {
     return true;
   }
 
   if (!::JS_IsExceptionPending(cx)) {
     xpc::Throw(cx, rv);
   }
   return false;
@@ -368,34 +368,34 @@ nsXBLProtoImplField::InstallAccessors(JS
     return NS_ERROR_OUT_OF_MEMORY;
   }
 
   return NS_OK;
 }
 
 nsresult
 nsXBLProtoImplField::InstallField(JS::Handle<JSObject*> aBoundNode,
-                                  nsIURI* aBindingDocURI,
+                                  const nsXBLPrototypeBinding& aProtoBinding,
                                   bool* aDidInstall) const
 {
   NS_PRECONDITION(aBoundNode,
                   "uh-oh, bound node should NOT be null or bad things will "
                   "happen");
 
   *aDidInstall = false;
 
   // Empty fields are treated as not actually present.
   if (IsEmpty()) {
     return NS_OK;
   }
 
   nsAutoMicroTask mt;
 
   nsAutoCString uriSpec;
-  nsresult rv = aBindingDocURI->GetSpec(uriSpec);
+  nsresult rv = aProtoBinding.DocURI()->GetSpec(uriSpec);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
 
   nsIGlobalObject* globalObject = xpc::WindowGlobalOrNull(aBoundNode);
   if (!globalObject) {
     return NS_OK;
   }
@@ -407,17 +407,17 @@ nsXBLProtoImplField::InstallField(JS::Ha
   // to be our entry global.
   AutoJSAPI jsapi;
   if (!jsapi.Init(globalObject)) {
     return NS_ERROR_UNEXPECTED;
   }
   MOZ_ASSERT(!::JS_IsExceptionPending(jsapi.cx()),
              "Shouldn't get here when an exception is pending!");
 
-  JSAddonId* addonId = MapURIToAddonID(aBindingDocURI);
+  JSAddonId* addonId = MapURIToAddonID(aProtoBinding.DocURI());
 
   // Note: the UNWRAP_OBJECT may mutate boundNode; don't use it after that call.
   JS::Rooted<JSObject*> boundNode(jsapi.cx(), aBoundNode);
   Element* boundElement = nullptr;
   rv = UNWRAP_OBJECT(Element, &boundNode, boundElement);
   if (NS_WARN_IF(NS_FAILED(rv))) {
     return rv;
   }
@@ -430,17 +430,17 @@ nsXBLProtoImplField::InstallField(JS::Ha
 
   AutoEntryScript aes(scopeObject, "XBL <field> initialization", true);
   JSContext* cx = aes.cx();
 
   JS::Rooted<JS::Value> result(cx);
   JS::CompileOptions options(cx);
   options.setFileAndLine(uriSpec.get(), mLineNumber);
   JS::AutoObjectVector scopeChain(cx);
-  if (!nsJSUtils::GetScopeChainForElement(cx, boundElement, scopeChain)) {
+  if (!nsJSUtils::GetScopeChainForXBL(cx, boundElement, aProtoBinding, scopeChain)) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
   rv = NS_OK;
   {
     nsJSUtils::ExecutionContext exec(cx, scopeObject);
     exec.SetScopeChain(scopeChain);
     exec.CompileAndExec(options, nsDependentString(mFieldText,
                                                    mFieldTextLength));
diff --git a/dom/xbl/nsXBLProtoImplField.h b/dom/xbl/nsXBLProtoImplField.h
--- a/dom/xbl/nsXBLProtoImplField.h
+++ b/dom/xbl/nsXBLProtoImplField.h
@@ -28,17 +28,17 @@ public:
   void SetLineNumber(uint32_t aLineNumber) {
     mLineNumber = aLineNumber;
   }
 
   nsXBLProtoImplField* GetNext() const { return mNext; }
   void SetNext(nsXBLProtoImplField* aNext) { mNext = aNext; }
 
   nsresult InstallField(JS::Handle<JSObject*> aBoundNode,
-                        nsIURI* aBindingDocURI,
+                        const nsXBLPrototypeBinding& aProtoBinding,
                         bool* aDidInstall) const;
 
   nsresult InstallAccessors(JSContext* aCx,
                             JS::Handle<JSObject*> aTargetClassObject);
 
   nsresult Read(nsIObjectInputStream* aStream);
   nsresult Write(nsIObjectOutputStream* aStream);
 
diff --git a/dom/xbl/nsXBLProtoImplMethod.cpp b/dom/xbl/nsXBLProtoImplMethod.cpp
--- a/dom/xbl/nsXBLProtoImplMethod.cpp
+++ b/dom/xbl/nsXBLProtoImplMethod.cpp
@@ -255,17 +255,17 @@ nsXBLProtoImplMethod::Write(nsIObjectOut
     JS::Rooted<JSObject*> method(RootingCx(), GetCompiledMethod());
     return XBL_SerializeFunction(aStream, method);
   }
 
   return NS_OK;
 }
 
 nsresult
-nsXBLProtoImplAnonymousMethod::Execute(nsIContent* aBoundElement, JSAddonId* aAddonId)
+nsXBLProtoImplAnonymousMethod::Execute(nsIContent* aBoundElement, JSAddonId* aAddonId, const nsXBLPrototypeBinding& aProtoBinding)
 {
   MOZ_ASSERT(aBoundElement->IsElement());
   NS_PRECONDITION(IsCompiled(), "Can't execute uncompiled method");
 
   if (!GetCompiledMethod()) {
     // Nothing to do here
     return NS_OK;
   }
@@ -298,18 +298,19 @@ nsXBLProtoImplAnonymousMethod::Execute(n
     xpc::GetScopeForXBLExecution(jsapi.cx(), globalObject, aAddonId));
   NS_ENSURE_TRUE(scopeObject, NS_ERROR_OUT_OF_MEMORY);
 
   dom::AutoEntryScript aes(scopeObject,
                            "XBL <constructor>/<destructor> invocation",
                            true);
   JSContext* cx = aes.cx();
   JS::AutoObjectVector scopeChain(cx);
-  if (!nsJSUtils::GetScopeChainForElement(cx, aBoundElement->AsElement(),
-                                          scopeChain)) {
+  if (!nsJSUtils::GetScopeChainForXBL(cx, aBoundElement->AsElement(),
+                                      aProtoBinding,
+                                      scopeChain)) {
     return NS_ERROR_OUT_OF_MEMORY;
   }
   MOZ_ASSERT(scopeChain.length() != 0);
 
   // Clone the function object, using our scope chain (for backwards
   // compat to the days when this was an event handler).
   JS::Rooted<JSObject*> jsMethodObject(cx, GetCompiledMethod());
   JS::Rooted<JSObject*> method(cx, JS::CloneFunctionObject(cx, jsMethodObject,
diff --git a/dom/xbl/nsXBLProtoImplMethod.h b/dom/xbl/nsXBLProtoImplMethod.h
--- a/dom/xbl/nsXBLProtoImplMethod.h
+++ b/dom/xbl/nsXBLProtoImplMethod.h
@@ -133,17 +133,17 @@ protected:
 };
 
 class nsXBLProtoImplAnonymousMethod : public nsXBLProtoImplMethod {
 public:
   explicit nsXBLProtoImplAnonymousMethod(const char16_t* aName) :
     nsXBLProtoImplMethod(aName)
   {}
 
-  nsresult Execute(nsIContent* aBoundElement, JSAddonId* aAddonId);
+  nsresult Execute(nsIContent* aBoundElement, JSAddonId* aAddonId, const nsXBLPrototypeBinding&);
 
   // Override InstallMember; these methods never get installed as members on
   // binding instantiations (though they may hang out in mMembers on the
   // prototype implementation).
   virtual nsresult InstallMember(JSContext* aCx,
                                  JS::Handle<JSObject*> aTargetClassObject) override {
     return NS_OK;
   }
diff --git a/dom/xbl/nsXBLPrototypeBinding.cpp b/dom/xbl/nsXBLPrototypeBinding.cpp
--- a/dom/xbl/nsXBLPrototypeBinding.cpp
+++ b/dom/xbl/nsXBLPrototypeBinding.cpp
@@ -101,16 +101,17 @@ protected:
 nsXBLPrototypeBinding::nsXBLPrototypeBinding()
 : mImplementation(nullptr),
   mBaseBinding(nullptr),
   mInheritStyle(true),
   mCheckedBaseProto(false),
   mKeyHandlersRegistered(false),
   mChromeOnlyContent(false),
   mBindToUntrustedContent(false),
+  mSimpleScopeChain(false),
   mResources(nullptr),
   mBaseNameSpaceID(kNameSpaceID_None)
 {
   MOZ_COUNT_CTOR(nsXBLPrototypeBinding);
 }
 
 nsresult
 nsXBLPrototypeBinding::Init(const nsACString& aID,
@@ -217,16 +218,21 @@ nsXBLPrototypeBinding::SetBindingElement
 
   mChromeOnlyContent = mBinding->AttrValueIs(kNameSpaceID_None,
                                              nsGkAtoms::chromeOnlyContent,
                                              nsGkAtoms::_true, eCaseMatters);
 
   mBindToUntrustedContent = mBinding->AttrValueIs(kNameSpaceID_None,
                                                   nsGkAtoms::bindToUntrustedContent,
                                                   nsGkAtoms::_true, eCaseMatters);
+
+  // TODO(emilio): Should we imply mBindToUntrustedContent -> mSimpleScopeChain?
+  mSimpleScopeChain = mBinding->AttrValueIs(kNameSpaceID_None,
+                                            nsGkAtoms::simpleScopeChain,
+                                            nsGkAtoms::_true, eCaseMatters);
 }
 
 bool
 nsXBLPrototypeBinding::GetAllowScripts() const
 {
   return mXBLDocInfoWeak->GetScriptAccess();
 }
 
@@ -257,26 +263,26 @@ nsXBLPrototypeBinding::FlushSkinSheets()
   return NS_OK;
 }
 
 nsresult
 nsXBLPrototypeBinding::BindingAttached(nsIContent* aBoundElement)
 {
   if (mImplementation && mImplementation->CompiledMembers() &&
       mImplementation->mConstructor)
-    return mImplementation->mConstructor->Execute(aBoundElement, MapURIToAddonID(mBindingURI));
+    return mImplementation->mConstructor->Execute(aBoundElement, MapURIToAddonID(mBindingURI), *this);
   return NS_OK;
 }
 
 nsresult
 nsXBLPrototypeBinding::BindingDetached(nsIContent* aBoundElement)
 {
   if (mImplementation && mImplementation->CompiledMembers() &&
       mImplementation->mDestructor)
-    return mImplementation->mDestructor->Execute(aBoundElement, MapURIToAddonID(mBindingURI));
+    return mImplementation->mDestructor->Execute(aBoundElement, MapURIToAddonID(mBindingURI), *this);
   return NS_OK;
 }
 
 nsXBLProtoImplAnonymousMethod*
 nsXBLPrototypeBinding::GetConstructor()
 {
   if (mImplementation)
     return mImplementation->mConstructor;
@@ -842,16 +848,18 @@ nsXBLPrototypeBinding::Read(nsIObjectInp
                             nsIDocument* aDocument,
                             uint8_t aFlags)
 {
   mInheritStyle = (aFlags & XBLBinding_Serialize_InheritStyle) ? true : false;
   mChromeOnlyContent =
     (aFlags & XBLBinding_Serialize_ChromeOnlyContent) ? true : false;
   mBindToUntrustedContent =
     (aFlags & XBLBinding_Serialize_BindToUntrustedContent) ? true : false;
+  mSimpleScopeChain =
+    (aFlags & XBLBinding_Serialize_SimpleScopeChain) ? true : false;
 
   // nsXBLContentSink::ConstructBinding doesn't create a binding with an empty
   // id, so we don't here either.
   nsAutoCString id;
   nsresult rv = aStream->ReadCString(id);
 
   NS_ENSURE_SUCCESS(rv, rv);
   NS_ENSURE_TRUE(!id.IsEmpty(), NS_ERROR_FAILURE);
@@ -1065,16 +1073,20 @@ nsXBLPrototypeBinding::Write(nsIObjectOu
   if (mChromeOnlyContent) {
     flags |= XBLBinding_Serialize_ChromeOnlyContent;
   }
 
   if (mBindToUntrustedContent) {
     flags |= XBLBinding_Serialize_BindToUntrustedContent;
   }
 
+  if (mSimpleScopeChain) {
+    flags |= XBLBinding_Serialize_SimpleScopeChain;
+  }
+
   nsresult rv = aStream->Write8(flags);
   NS_ENSURE_SUCCESS(rv, rv);
 
   nsAutoCString id;
   mBindingURI->GetRef(id);
   rv = aStream->WriteStringZ(id.get());
   NS_ENSURE_SUCCESS(rv, rv);
 
diff --git a/dom/xbl/nsXBLPrototypeBinding.h b/dom/xbl/nsXBLPrototypeBinding.h
--- a/dom/xbl/nsXBLPrototypeBinding.h
+++ b/dom/xbl/nsXBLPrototypeBinding.h
@@ -264,18 +264,19 @@ public:
    * has the localname given by aTag and is in the XBL namespace.
    */
   mozilla::dom::Element* GetImmediateChild(nsIAtom* aTag);
   mozilla::dom::Element* LocateInstance(mozilla::dom::Element* aBoundElt,
                                         nsIContent* aTemplRoot,
                                         nsIContent* aCopyRoot,
                                         mozilla::dom::Element* aTemplChild);
 
-  bool ChromeOnlyContent() { return mChromeOnlyContent; }
-  bool BindToUntrustedContent() { return mBindToUntrustedContent; }
+  bool ChromeOnlyContent() const { return mChromeOnlyContent; }
+  bool SimpleScopeChain() const { return mSimpleScopeChain; }
+  bool BindToUntrustedContent() const { return mBindToUntrustedContent; }
 
   typedef nsClassHashtable<nsISupportsHashKey, nsXBLAttributeEntry> InnerAttributeTable;
 
 protected:
   // Ensure that mAttributeTable has been created.
   void EnsureAttributeTable();
   // Ad an entry to the attribute table
   void AddToAttributeTable(int32_t aSourceNamespaceID, nsIAtom* aSourceTag,
@@ -302,16 +303,19 @@ protected:
 
   // Weak.  The docinfo will own our base binding.
   mozilla::WeakPtr<nsXBLPrototypeBinding> mBaseBinding;
   bool mInheritStyle;
   bool mCheckedBaseProto;
   bool mKeyHandlersRegistered;
   bool mChromeOnlyContent;
   bool mBindToUntrustedContent;
+  // True if constructors, handlers, etc for this binding would skip the scope
+  // chain for parent elements and go directly to the document.
+  bool mSimpleScopeChain;
 
   nsAutoPtr<nsXBLPrototypeResources> mResources; // If we have any resources, this will be non-null.
 
   nsXBLDocumentInfo* mXBLDocInfoWeak; // A pointer back to our doc info.  Weak, since it owns us.
 
   // A table for attribute containers. Namespace IDs are used as
   // keys in the table. Containers are InnerAttributeTables.
   // This table is used to efficiently handle attribute changes.
diff --git a/dom/xbl/nsXBLPrototypeHandler.cpp b/dom/xbl/nsXBLPrototypeHandler.cpp
--- a/dom/xbl/nsXBLPrototypeHandler.cpp
+++ b/dom/xbl/nsXBLPrototypeHandler.cpp
@@ -363,17 +363,17 @@ nsXBLPrototypeHandler::ExecuteHandler(Ev
   JS::Rooted<JSObject*> genericHandler(cx, handler.get());
   bool ok = JS_WrapObject(cx, &genericHandler);
   NS_ENSURE_TRUE(ok, NS_ERROR_OUT_OF_MEMORY);
   MOZ_ASSERT(!js::IsCrossCompartmentWrapper(genericHandler));
 
   // Build a scope chain in the XBL scope.
   RefPtr<Element> targetElement = do_QueryObject(scriptTarget);
   JS::AutoObjectVector scopeChain(cx);
-  ok = nsJSUtils::GetScopeChainForElement(cx, targetElement, scopeChain);
+  ok = nsJSUtils::GetScopeChainForXBL(cx, targetElement, *mPrototypeBinding, scopeChain);
   NS_ENSURE_TRUE(ok, NS_ERROR_OUT_OF_MEMORY);
 
   // Next, clone the generic handler with our desired scope chain.
   JS::Rooted<JSObject*> bound(cx, JS::CloneFunctionObject(cx, genericHandler,
                                                           scopeChain));
   NS_ENSURE_TRUE(bound, NS_ERROR_FAILURE);
 
   RefPtr<EventHandlerNonNull> handlerCallback =
diff --git a/dom/xbl/nsXBLSerialize.h b/dom/xbl/nsXBLSerialize.h
--- a/dom/xbl/nsXBLSerialize.h
+++ b/dom/xbl/nsXBLSerialize.h
@@ -11,29 +11,32 @@
 #include "nsIObjectOutputStream.h"
 #include "mozilla/dom/NameSpaceConstants.h"
 #include "js/TypeDecls.h"
 
 typedef uint8_t XBLBindingSerializeDetails;
 
 // A version number to ensure we don't load cached data in a different
 // file format.
-#define XBLBinding_Serialize_Version 0x00000004
+#define XBLBinding_Serialize_Version 0x00000005
 
 // Set for the first binding in a document
-#define XBLBinding_Serialize_IsFirstBinding 1
+#define XBLBinding_Serialize_IsFirstBinding (1 << 0)
 
 // Set to indicate that nsXBLPrototypeBinding::mInheritStyle should be true
-#define XBLBinding_Serialize_InheritStyle 2
+#define XBLBinding_Serialize_InheritStyle (1 << 1)
 
 // Set to indicate that nsXBLPrototypeBinding::mChromeOnlyContent should be true
-#define XBLBinding_Serialize_ChromeOnlyContent 4
+#define XBLBinding_Serialize_ChromeOnlyContent (1 << 2)
 
 // Set to indicate that nsXBLPrototypeBinding::mBindToUntrustedContent should be true
-#define XBLBinding_Serialize_BindToUntrustedContent 8
+#define XBLBinding_Serialize_BindToUntrustedContent (1 << 3)
+
+// Set to indicate that nsXBLPrototypeBinding::mSimpleScopeChain should be true
+#define XBLBinding_Serialize_SimpleScopeChain (1 << 4)
 
 // Appears at the end of the serialized data to indicate that no more bindings
 // are present for this document.
 #define XBLBinding_Serialize_NoMoreBindings 0x80
 
 // Implementation member types. The serialized value for each member contains one
 // of these values, combined with the read-only flag XBLBinding_Serialize_ReadOnly.
 // Use XBLBinding_Serialize_Mask to filter out the read-only flag and check for
diff --git a/dom/xbl/test/file_fieldScopeChain.xml b/dom/xbl/test/file_fieldScopeChain.xml
--- a/dom/xbl/test/file_fieldScopeChain.xml
+++ b/dom/xbl/test/file_fieldScopeChain.xml
@@ -1,8 +1,13 @@
 <bindings xmlns="http://www.mozilla.org/xbl"
           xmlns:html="http://www.w3.org/1999/xhtml">
-  <binding id="foo">
+  <binding id="foo" simpleScopeChain="true">
     <implementation>
       <field name="bar">baz</field>
+      <constructor>
+      <![CDATA[
+        is(document, this.ownerDocument, "Shouldn't have forms in the scope chain");
+      ]]>
+      </constructor>
     </implementation>
   </binding>
 </bindings>
diff --git a/dom/xbl/test/test_fieldScopeChain.html b/dom/xbl/test/test_fieldScopeChain.html
--- a/dom/xbl/test/test_fieldScopeChain.html
+++ b/dom/xbl/test/test_fieldScopeChain.html
@@ -24,11 +24,15 @@ https://bugzilla.mozilla.org/show_bug.cg
 </head>
 <body>
 <a target="_blank" href="https://bugzilla.mozilla.org/show_bug.cgi?id=1095660">Mozilla Bug </a>
 <p id="display"></p>
 <div id="content" style="display: none">
 
 </div>
 <pre id="test" style="-moz-binding: url(file_fieldScopeChain.xml#foo)">
+<form>
+  <input name="document">
+  <input type="text" id="input-test" style="-moz-binding: url(file_fieldScopeChain.xml#foo)">
+</form>
 </pre>
 </body>
 </html>
diff --git a/toolkit/content/widgets/datetimebox.xml b/toolkit/content/widgets/datetimebox.xml
--- a/toolkit/content/widgets/datetimebox.xml
+++ b/toolkit/content/widgets/datetimebox.xml
@@ -11,16 +11,17 @@
 
 <bindings id="datetimeboxBindings"
    xmlns="http://www.mozilla.org/xbl"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns:xul="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul"
    xmlns:xbl="http://www.mozilla.org/xbl">
 
   <binding id="date-input"
+           simpleScopeChain="true"
            extends="chrome://global/content/bindings/datetimebox.xml#datetime-input-base">
     <resources>
       <stylesheet src="chrome://global/content/textbox.css"/>
       <stylesheet src="chrome://global/skin/textbox.css"/>
       <stylesheet src="chrome://global/content/bindings/datetimebox.css"/>
     </resources>
 
     <implementation>
@@ -423,16 +424,17 @@
         ]]>
         </body>
       </method>
 
     </implementation>
   </binding>
 
   <binding id="time-input"
+           simpleScopeChain="true"
            extends="chrome://global/content/bindings/datetimebox.xml#datetime-input-base">
     <resources>
       <stylesheet src="chrome://global/content/textbox.css"/>
       <stylesheet src="chrome://global/skin/textbox.css"/>
       <stylesheet src="chrome://global/content/bindings/datetimebox.css"/>
     </resources>
 
     <implementation>
@@ -1187,17 +1189,18 @@
           this.log("getCurrentValue: " + JSON.stringify(time));
           return time;
         ]]>
         </body>
       </method>
     </implementation>
   </binding>
 
-  <binding id="datetime-input-base">
+  <binding id="datetime-input-base"
+           simpleScopeChain="true">
     <resources>
       <stylesheet src="chrome://global/content/textbox.css"/>
       <stylesheet src="chrome://global/skin/textbox.css"/>
       <stylesheet src="chrome://global/content/bindings/datetimebox.css"/>
     </resources>
 
     <content>
       <html:div class="datetime-input-box-wrapper" anonid="input-box-wrapper"
