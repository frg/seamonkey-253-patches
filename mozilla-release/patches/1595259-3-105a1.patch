# HG changeset patch
# User Tooru Fujisawa <arai_a@mac.com>
# Date 1660114831 0
# Node ID 1995acac9f11fb8c9bfad8b71d6c32f26ed57e18
# Parent  93b5b7fc9b5fba266e583178eca67d2788e862fb
Bug 1595259 - Part 3: Update fdlibm to 369ea0520a3061c07400d7cd32172efb6af39815. r=mgaudet

Differential Revision: https://phabricator.services.mozilla.com/D154079

diff --git a/modules/fdlibm/README.mozilla b/modules/fdlibm/README.mozilla
--- a/modules/fdlibm/README.mozilla
+++ b/modules/fdlibm/README.mozilla
@@ -8,12 +8,12 @@ Each file is downloaded separately, as c
 resources.
 
 The in-tree copy is updated by running
   sh update.sh
 or
   sh update.sh <sha-commit>
 from within the modules/fdlibm directory.
 
-Current version: [commit cf4707bb2f78ecf56ba350bdc24e3135b4339622 (2019-09-25T18:50:57Z)].
+Current version: [commit 369ea0520a3061c07400d7cd32172efb6af39815 (2022-08-04T17:33:34Z)].
 
 patches 01-18 fixes files to be usable within mozilla-central tree.
 See https://bugzilla.mozilla.org/show_bug.cgi?id=933257
diff --git a/modules/fdlibm/src/e_hypot.cpp b/modules/fdlibm/src/e_hypot.cpp
--- a/modules/fdlibm/src/e_hypot.cpp
+++ b/modules/fdlibm/src/e_hypot.cpp
@@ -113,15 +113,13 @@ double
 	    SET_HIGH_WORD(y1,hb);
 	    y2 = b - y1;
 	    t1 = 0;
 	    SET_HIGH_WORD(t1,ha+0x00100000);
 	    t2 = a - t1;
 	    w  = std::sqrt(t1*y1-(w*(-w)-(t1*y2+t2*b)));
 	}
 	if(k!=0) {
-	    u_int32_t high;
-	    t1 = 1.0;
-	    GET_HIGH_WORD(high,t1);
-	    SET_HIGH_WORD(t1,high+(k<<20));
+	    t1 = 0.0;
+	    SET_HIGH_WORD(t1,(1023+k)<<20);
 	    return t1*w;
 	} else return w;
 }
diff --git a/modules/fdlibm/src/e_pow.cpp b/modules/fdlibm/src/e_pow.cpp
--- a/modules/fdlibm/src/e_pow.cpp
+++ b/modules/fdlibm/src/e_pow.cpp
@@ -53,17 +53,16 @@
  * Constants :
  * The hexadecimal values are the intended ones for the following
  * constants. The decimal values may be used, provided that the
  * compiler will convert from decimal to binary accurately enough
  * to produce the hexadecimal values shown.
  */
 
 #include <cmath>
-
 #include <float.h>
 #include "math_private.h"
 
 static const double
 bp[] = {1.0, 1.5,},
 dp_h[] = { 0.0, 5.84962487220764160156e-01,}, /* 0x3FE2B803, 0x40000000 */
 dp_l[] = { 0.0, 1.35003920212974897128e-08,}, /* 0x3E4CFDEB, 0x43CFD006 */
 zero    =  0.0,
diff --git a/modules/fdlibm/src/fdlibm.h b/modules/fdlibm/src/fdlibm.h
--- a/modules/fdlibm/src/fdlibm.h
+++ b/modules/fdlibm/src/fdlibm.h
@@ -18,51 +18,49 @@
 #define mozilla_imported_fdlibm_h
 
 namespace fdlibm {
 
 double	acos(double);
 double	asin(double);
 double	atan(double);
 double	atan2(double, double);
-
 double	cos(double);
 double	sin(double);
 double	tan(double);
 
 double	cosh(double);
 double	sinh(double);
 double	tanh(double);
 
 double	exp(double);
 double	log(double);
 double	log10(double);
 
 double	pow(double, double);
+
+double	ceil(double);
 double	fabs(double);
-
 double	floor(double);
-double	trunc(double);
-double	ceil(double);
 
 double	acosh(double);
 double	asinh(double);
 double	atanh(double);
 double	cbrt(double);
 double	expm1(double);
 double	hypot(double, double);
 double	log1p(double);
 double	log2(double);
 double	rint(double);
 double	copysign(double, double);
 double	nearbyint(double);
 double	scalbn(double, int);
+double	trunc(double);
 
 float	ceilf(float);
 float	floorf(float);
-
 float	nearbyintf(float);
 float	rintf(float);
 float	truncf(float);
 
 } /* namespace fdlibm */
 
 #endif /* mozilla_imported_fdlibm_h */
diff --git a/modules/fdlibm/src/math_private.h b/modules/fdlibm/src/math_private.h
--- a/modules/fdlibm/src/math_private.h
+++ b/modules/fdlibm/src/math_private.h
@@ -16,27 +16,29 @@
 
 #ifndef _MATH_PRIVATE_H_
 #define	_MATH_PRIVATE_H_
 
 #include <cfloat>
 #include <stdint.h>
 #include <sys/types.h>
 
-#include "fdlibm.h"
+#include "mozilla/EndianUtils.h"
 
-#include "mozilla/EndianUtils.h"
+#include "fdlibm.h"
 
 /*
  * Emulate FreeBSD internal double types.
  * Adapted from https://github.com/freebsd/freebsd-src/search?q=__double_t
  */
 
 typedef double      __double_t;
 typedef __double_t  double_t;
+typedef float       __float_t;
+typedef __float_t   float_t;
 
 /*
  * The original fdlibm code used statements like:
  *	n0 = ((*(int*)&one)>>29)^1;		* index of high word *
  *	ix0 = *(n0+(int*)&x);			* high word of x *
  *	ix1 = *((1-n0)+(int*)&x);		* low word of x *
  * to dig two 32 bit words out of the 64 bit IEEE floating point
  * value.  That is non-ANSI, and, moreover, the gcc instruction
@@ -89,20 +91,16 @@ typedef union
   struct {
     u_int64_t lsw;
     u_int64_t msw;
   } parts64;
 } ieee_quad_shape_type;
 
 #endif
 
-/*
- * A union which permits us to convert between a double and two 32 bit
- * ints.
- */
 
 #if MOZ_BIG_ENDIAN
 
 typedef union
 {
   double value;
   struct
   {
@@ -476,17 +473,17 @@ do {								\
  * That is usually enough, and adding c (which by normalization is about
  * 2**53 times smaller than a) cannot change b significantly.  However,
  * cancellation of 'a' with c in normalization of (a, c) may reduce 'a'
  * significantly relative to b.  The caller must ensure that significant
  * cancellation doesn't occur, either by having c of the same sign as 'a',
  * or by having |c| a few percent smaller than |a|.  Pre-normalization of
  * (a, b) may help.
  *
- * This is is a variant of an algorithm of Kahan (see Knuth (1981) 4.2.2
+ * This is a variant of an algorithm of Kahan (see Knuth (1981) 4.2.2
  * exercise 19).  We gain considerable efficiency by requiring the terms to
  * be sufficiently normalized and sufficiently increasing.
  */
 #define	_3sumF(a, b, c) do {	\
 	__typeof(a) __tmp;	\
 				\
 	__tmp = (c);		\
 	_2sumF(__tmp, (a));	\
@@ -630,27 +627,60 @@ rnint(__double_t x)
 	return ((double)(x + 0x1.8p52) - 0x1.8p52);
 }
 
 /*
  * irint() and i64rint() give the same result as casting to their integer
  * return type provided their arg is a floating point integer.  They can
  * sometimes be more efficient because no rounding is required.
  */
-#if (defined(amd64) || defined(__i386__)) && defined(__GNUCLIKE_ASM)
+#if defined(amd64) || defined(__i386__)
 #define	irint(x)						\
     (sizeof(x) == sizeof(float) &&				\
     sizeof(__float_t) == sizeof(long double) ? irintf(x) :	\
     sizeof(x) == sizeof(double) &&				\
     sizeof(__double_t) == sizeof(long double) ? irintd(x) :	\
     sizeof(x) == sizeof(long double) ? irintl(x) : (int)(x))
 #else
 #define	irint(x)	((int)(x))
 #endif
 
+#define	i64rint(x)	((int64_t)(x))	/* only needed for ld128 so not opt. */
+
+#if defined(__i386__)
+static __inline int
+irintf(float x)
+{
+	int n;
+
+	__asm("fistl %0" : "=m" (n) : "t" (x));
+	return (n);
+}
+
+static __inline int
+irintd(double x)
+{
+	int n;
+
+	__asm("fistl %0" : "=m" (n) : "t" (x));
+	return (n);
+}
+#endif
+
+#if defined(__amd64__) || defined(__i386__)
+static __inline int
+irintl(long double x)
+{
+	int n;
+
+	__asm("fistl %0" : "=m" (n) : "t" (x));
+	return (n);
+}
+#endif
+
 #ifdef DEBUG
 #if defined(__amd64__) || defined(__i386__)
 #define	breakpoint()	asm("int $3")
 #else
 #include <signal.h>
 
 #define	breakpoint()	raise(SIGTRAP)
 #endif
diff --git a/modules/fdlibm/src/s_cbrt.cpp b/modules/fdlibm/src/s_cbrt.cpp
--- a/modules/fdlibm/src/s_cbrt.cpp
+++ b/modules/fdlibm/src/s_cbrt.cpp
@@ -102,12 +102,12 @@ cbrt(double x)
 	u.bits=(u.bits+0x80000000)&0xffffffffc0000000ULL;
 	t=u.value;
 
     /* one step Newton iteration to 53 bits with error < 0.667 ulps */
 	s=t*t;				/* t*t is exact */
 	r=x/s;				/* error <= 0.5 ulps; |r| < |t| */
 	w=t+t;				/* t+t is exact */
 	r=(r-t)/(w+r);			/* r-t is exact; w+r ~= 3*t */
-	t=t+t*r;			/* error <= 0.5 + 0.5/3 + epsilon */
+	t=t+t*r;			/* error <= (0.5 + 0.5/3) * ulp */
 
 	return(t);
 }
diff --git a/modules/fdlibm/src/s_nearbyint.cpp b/modules/fdlibm/src/s_nearbyint.cpp
--- a/modules/fdlibm/src/s_nearbyint.cpp
+++ b/modules/fdlibm/src/s_nearbyint.cpp
@@ -25,16 +25,17 @@
  * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
  * SUCH DAMAGE.
  */
 
 //#include <sys/cdefs.h>
 //__FBSDID("$FreeBSD$");
 
 #include <fenv.h>
+
 #include "math_private.h"
 
 /*
  * We save and restore the floating-point environment to avoid raising
  * an inexact exception.  We can get away with using fesetenv()
  * instead of feclearexcept()/feupdateenv() to restore the environment
  * because the only exception defined for rint() is overflow, and
  * rounding can't overflow as long as emax >= p.
diff --git a/modules/fdlibm/src/s_scalbn.cpp b/modules/fdlibm/src/s_scalbn.cpp
--- a/modules/fdlibm/src/s_scalbn.cpp
+++ b/modules/fdlibm/src/s_scalbn.cpp
@@ -1,60 +1,43 @@
-/* @(#)s_scalbn.c 5.1 93/09/24 */
 /*
- * ====================================================
- * Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
+ * Copyright (c) 2005-2020 Rich Felker, et al.
  *
- * Developed at SunPro, a Sun Microsystems, Inc. business.
- * Permission to use, copy, modify, and distribute this
- * software is freely granted, provided that this notice
- * is preserved.
- * ====================================================
+ * SPDX-License-Identifier: MIT
+ *
+ * Please see https://git.musl-libc.org/cgit/musl/tree/COPYRIGHT
+ * for all contributors to musl.
  */
-
-//#include <sys/cdefs.h>
-//__FBSDID("$FreeBSD$");
-
-/*
- * scalbn (double x, int n)
- * scalbn(x,n) returns x* 2**n  computed by  exponent
- * manipulation rather than by actually performing an
- * exponentiation or a multiplication.
- */
-
 #include <float.h>
+#include <stdint.h>
 
 #include "math_private.h"
 
-static const double
-two54   =  1.80143985094819840000e+16, /* 0x43500000, 0x00000000 */
-twom54  =  5.55111512312578270212e-17, /* 0x3C900000, 0x00000000 */
-huge   = 1.0e+300,
-tiny   = 1.0e-300;
-
-double
-scalbn (double x, int n)
+double scalbn(double x, int n)
 {
-	int32_t k,hx,lx;
-	EXTRACT_WORDS(hx,lx,x);
-        k = (hx&0x7ff00000)>>20;		/* extract exponent */
-        if (k==0) {				/* 0 or subnormal x */
-            if ((lx|(hx&0x7fffffff))==0) return x; /* +-0 */
-	    x *= two54;
-	    GET_HIGH_WORD(hx,x);
-	    k = ((hx&0x7ff00000)>>20) - 54;
-            if (n< -50000) return tiny*x; 	/*underflow*/
-	    }
-        if (k==0x7ff) return x+x;		/* NaN or Inf */
-        k = k+n;
-        if (k >  0x7fe) return huge*copysign(huge,x); /* overflow  */
-        if (k > 0) 				/* normal result */
-	    {SET_HIGH_WORD(x,(hx&0x800fffff)|(k<<20)); return x;}
-        if (k <= -54) {
-            if (n > 50000) 	/* in case integer overflow in n+k */
-		return huge*copysign(huge,x);	/*overflow*/
-	    else
-		return tiny*copysign(tiny,x); 	/*underflow*/
+	union {double f; uint64_t i;} u;
+	double_t y = x;
+
+	if (n > 1023) {
+		y *= 0x1p1023;
+		n -= 1023;
+		if (n > 1023) {
+			y *= 0x1p1023;
+			n -= 1023;
+			if (n > 1023)
+				n = 1023;
+		}
+	} else if (n < -1022) {
+		/* make sure final n < -53 to avoid double
+		   rounding in the subnormal range */
+		y *= 0x1p-1022 * 0x1p53;
+		n += 1022 - 53;
+		if (n < -1022) {
+			y *= 0x1p-1022 * 0x1p53;
+			n += 1022 - 53;
+			if (n < -1022)
+				n = -1022;
+		}
 	}
-        k += 54;				/* subnormal result */
-	SET_HIGH_WORD(x,(hx&0x800fffff)|(k<<20));
-        return x*twom54;
+	u.i = (uint64_t)(0x3ff+n)<<52;
+	x = y * u.f;
+	return x;
 }

