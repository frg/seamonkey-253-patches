# HG changeset patch
# User Andrew Swan <aswan@mozilla.com>
# Date 1515622700 28800
# Node ID 5dd1b3a4ea85dc32387d744dcc2f19fb86f63fb0
# Parent  3420a3d686547fd52d0b535fea004004f67257a9
Bug 1402064 Switch to modern AMO metadata API r=kmag

Switch from the old XML-based AMO metadata API to the modern JSON based
API.  This turned into something between a modest update and complete
rewrite.  Most notably, external APIs became (mostly) promise-based.  The
exception is getCachedAddonById() which XPIInstall.jsm requires a
synchronous callback from.

Also, hopefully we will be able to get rid of a bunch of this metadata
handling soon.  If this code had a long life ahead of it, the unit tests
could use some more attention, but I mostly did the minimum here just to
keep them running for now with the expectation that we'll be able to get
rid of them within some small number of months.

MozReview-Commit-ID: 3DRaBdWGaiJ

diff --git a/toolkit/mozapps/extensions/test/xpcshell/test_AddonRepository.js b/toolkit/mozapps/extensions/test/xpcshell/test_AddonRepository.js
--- a/toolkit/mozapps/extensions/test/xpcshell/test_AddonRepository.js
+++ b/toolkit/mozapps/extensions/test/xpcshell/test_AddonRepository.js
@@ -288,80 +288,30 @@ var SEARCH_TEST = {
                     "%API_VERSION%/%MAX_RESULTS%/%TERMS%",
   failedURL:        "/XPCShell/1/1.5/1.5/" + (2 * FAILED_MAX_RESULTS) +
                     "/odd%3Dsearch%3Awith%26weird%22characters",
   successfulURL:    "/XPCShell/1/1.5/1.5/" + (2 * MAX_RESULTS) +
                     "/odd%3Dsearch%3Awith%26weird%22characters"
 };
 
 // Test that actual results and expected results are equal
-function check_results(aActualAddons, aExpectedAddons, aAddonCount, aInstallNull) {
-  Assert.ok(!AddonRepository.isSearching);
-
-  Assert.equal(aActualAddons.length, aAddonCount);
+function check_results(aActualAddons, aExpectedAddons) {
   do_check_addons(aActualAddons, aExpectedAddons, ADDON_PROPERTIES);
 
   // Additional tests
   aActualAddons.forEach(function check_each_addon(aActualAddon) {
     // Separately check name so better messages are output when test fails
     if (aActualAddon.name == "FAIL")
       do_throw(aActualAddon.id + " - " + aActualAddon.description);
     if (aActualAddon.name != "PASS")
       do_throw(aActualAddon.id + " - invalid add-on name " + aActualAddon.name);
 
-    Assert.equal(aActualAddon.install == null, !!aInstallNull || !aActualAddon.sourceURI);
-
-    // Check that sourceURI property consistent within actual addon
-    if (aActualAddon.install)
-      Assert.equal(aActualAddon.install.sourceURI.spec, aActualAddon.sourceURI.spec);
   });
 }
 
-// Complete a search, also testing cancelSearch() and isSearching
-function complete_search(aSearch, aSearchCallback) {
-  var failCallback = {
-    searchSucceeded(addons, length, total) {
-      do_throw("failCallback.searchSucceeded should not be called");
-      end_test();
-    },
-
-    searchFailed() {
-      do_throw("failCallback.searchFailed should not be called");
-      end_test();
-    }
-  };
-
-  var callbackCalled = false;
-  var testCallback = {
-    searchSucceeded(addons, length, total) {
-      do_throw("testCallback.searchSucceeded should not be called");
-      end_test();
-    },
-
-    searchFailed() {
-      callbackCalled = true;
-    }
-  };
-
-  // Should fail because cancelled it immediately
-  aSearch(failCallback);
-  Assert.ok(AddonRepository.isSearching);
-  AddonRepository.cancelSearch();
-  Assert.ok(!AddonRepository.isSearching);
-
-  aSearch(aSearchCallback);
-  Assert.ok(AddonRepository.isSearching);
-
-  // searchFailed should be called immediately because already searching
-  aSearch(testCallback);
-  Assert.ok(callbackCalled);
-  Assert.ok(AddonRepository.isSearching);
-}
-
-
 function run_test() {
   // Setup for test
   do_test_pending();
   createAppInfo("xpcshell@tests.mozilla.org", "XPCShell", "1", "1.9");
 
   startupManager();
 
   // Install an add-on so can check that it isn't returned in the results
@@ -495,128 +445,74 @@ function run_test_1() {
   });
 
   run_test_getAddonsByID_fails();
 }
 
 // Tests failure of AddonRepository.getAddonsByIDs()
 function run_test_getAddonsByID_fails() {
   Services.prefs.setCharPref(GET_TEST.preference, GET_TEST.preferenceValue);
-  var callback = {
-    searchSucceeded(aAddonsList, aAddonCount, aTotalResults) {
-      do_throw("searchAddons should not have succeeded");
-      end_test();
-    },
-
-    searchFailed() {
-      Assert.ok(!AddonRepository.isSearching);
-      run_test_getAddonsByID_succeeds();
-    }
-  };
-
-  complete_search(function complete_search_fail_callback(aCallback) {
-    AddonRepository.getAddonsByIDs(GET_TEST.failedIDs, aCallback);
-  }, callback);
+  AddonRepository.getAddonsByIDs(GET_TEST.failedIDs).then(result => {
+    do_throw("getAddonsByIDs should not have succeeded");
+    end_test();
+  }).catch(err => {
+    run_test_getAddonsByID_succeeds();
+  });
 }
 
 // Tests success of AddonRepository.getAddonsByIDs()
 function run_test_getAddonsByID_succeeds() {
-  var callback = {
-    searchSucceeded(aAddonsList, aAddonCount, aTotalResults) {
-      Assert.equal(aTotalResults, -1);
-      check_results(aAddonsList, GET_RESULTS, aAddonCount, true);
-      run_test_retrieveRecommended_fails();
-    },
-
-    searchFailed() {
-      do_throw("searchAddons should not have failed");
-      end_test();
-    }
-  };
-
-  complete_search(function complete_search_succeed_callback(aCallback) {
-    AddonRepository.getAddonsByIDs(GET_TEST.successfulIDs, aCallback);
-  }, callback);
+  AddonRepository.getAddonsByIDs(GET_TEST.successfulIDs).then(result => {
+    check_results(result, GET_RESULTS);
+    run_test_retrieveRecommended_fails();
+  }).catch(err => {
+    do_throw(err);
+    end_test();
+  });
 }
 
 // Tests failure of AddonRepository.retrieveRecommendedAddons()
 function run_test_retrieveRecommended_fails() {
   Services.prefs.setCharPref(RECOMMENDED_TEST.preference,
                              RECOMMENDED_TEST.preferenceValue);
-  var callback = {
-    searchSucceeded(aAddonsList, aAddonCount, aTotalResults) {
-      do_throw("retrieveRecommendedAddons should not have succeeded");
-      end_test();
-    },
-
-    searchFailed() {
-      Assert.ok(!AddonRepository.isSearching);
-      run_test_retrieveRecommended_succeed();
-    }
-  };
-
-  complete_search(function retrieveRecommended_failing_callback(aCallback) {
-    AddonRepository.retrieveRecommendedAddons(FAILED_MAX_RESULTS, aCallback);
-  }, callback);
+  AddonRepository.retrieveRecommendedAddons(FAILED_MAX_RESULTS).then(result => {
+    do_throw("retrieveRecommendedAddons should not have succeeded");
+    end_test();
+  }).catch(err => {
+    run_test_retrieveRecommended_succeed();
+  });
 }
 
 // Tests success of AddonRepository.retrieveRecommendedAddons()
 function run_test_retrieveRecommended_succeed() {
-  var callback = {
-    searchSucceeded(aAddonsList, aAddonCount, aTotalResults) {
-      Assert.equal(aTotalResults, -1);
-      check_results(aAddonsList, SEARCH_RESULTS, aAddonCount);
-      run_test_searchAddons_fails();
-    },
-
-    searchFailed() {
-      do_throw("retrieveRecommendedAddons should not have failed");
-      end_test();
-    }
-  };
-
-  complete_search(function retrieveRecommended_succeed_callback(aCallback) {
-    AddonRepository.retrieveRecommendedAddons(MAX_RESULTS, aCallback);
-  }, callback);
+  AddonRepository.retrieveRecommendedAddons(MAX_RESULTS).then(result => {
+    check_results(result, SEARCH_RESULTS);
+    run_test_searchAddons_fails();
+  }).catch(err => {
+    do_throw(err);
+    end_test();
+  });
 }
 
 // Tests failure of AddonRepository.searchAddons()
 function run_test_searchAddons_fails() {
-  Services.prefs.setCharPref(SEARCH_TEST.preference, SEARCH_TEST.preferenceValue);
-  var callback = {
-    searchSucceeded(aAddonsList, aAddonCount, aTotalResults) {
-      do_throw("searchAddons should not have succeeded");
-      end_test();
-    },
-
-    searchFailed() {
-      Assert.ok(!AddonRepository.isSearching);
-      run_test_searchAddons_succeeds();
-    }
-  };
-
-  complete_search(function(aCallback) {
-    var searchTerms = SEARCH_TEST.searchTerms;
-    AddonRepository.searchAddons(searchTerms, FAILED_MAX_RESULTS, aCallback);
-  }, callback);
+  Services.prefs.setCharPref(SEARCH_TEST.preference,
+                             SEARCH_TEST.preferenceValue);
+  var searchTerms = SEARCH_TEST.searchTerms;
+  AddonRepository.searchAddons(searchTerms, FAILED_MAX_RESULTS).then(result => {
+    do_throw("searchAddons should not have succeeded");
+    end_test();
+  }).catch(err => {
+    run_test_searchAddons_succeeds();
+  });
 }
 
 // Tests success of AddonRepository.searchAddons()
 function run_test_searchAddons_succeeds() {
-  var callback = {
-    searchSucceeded(aAddonsList, aAddonCount, aTotalResults) {
-      Assert.equal(aTotalResults, TOTAL_RESULTS);
-      check_results(aAddonsList, SEARCH_RESULTS, aAddonCount);
-      end_test();
-    },
-
-    searchFailed() {
-      do_throw("searchAddons should not have failed");
-      end_test();
-    }
-  };
-
-  complete_search(function(aCallback) {
-    var searchTerms = SEARCH_TEST.searchTerms;
-    AddonRepository.searchAddons(searchTerms, MAX_RESULTS, aCallback);
-  }, callback);
+  var searchTerms = SEARCH_TEST.searchTerms;
+  AddonRepository.searchAddons(searchTerms, MAX_RESULTS).then(result => {
+    check_results(result, SEARCH_RESULTS);
+    end_test();
+  }).catch(err => {
+    do_throw(err);
+    end_test();
+  });
 }
-
