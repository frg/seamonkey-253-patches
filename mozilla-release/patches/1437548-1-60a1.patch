# HG changeset patch
# User Patrick Brosset <pbrosset@mozilla.com>
# Date 1518518345 -3600
# Node ID 270d44332b5d10ba2fb45a78fcc813d353d19ce4
# Parent  94c03aa5d4dc85e151a7bcf21752211a917052f3
Bug 1437548 - 1 - Simplified font inspector layout; r=gl

A lot of simplifications of the current fonts UI, and alignment
to Victoria's mockup here:
https://mozilla.invisionapp.com/share/Z3F7OGCTK#/screens/278367863

MozReview-Commit-ID: EgcZDWyKafV

diff --git a/devtools/client/inspector/fonts/actions/font-options.js b/devtools/client/inspector/fonts/actions/font-options.js
--- a/devtools/client/inspector/fonts/actions/font-options.js
+++ b/devtools/client/inspector/fonts/actions/font-options.js
@@ -1,34 +1,23 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const {
   UPDATE_PREVIEW_TEXT,
-  UPDATE_SHOW_ALL_FONTS,
 } = require("./index");
 
 module.exports = {
 
   /**
    * Update the preview text in the font inspector
    */
   updatePreviewText(previewText) {
     return {
       type: UPDATE_PREVIEW_TEXT,
       previewText,
     };
   },
 
-  /**
-   * Update whether to show all fonts in the font inspector
-   */
-  updateShowAllFonts(showAllFonts) {
-    return {
-      type: UPDATE_SHOW_ALL_FONTS,
-      showAllFonts,
-    };
-  },
-
 };
diff --git a/devtools/client/inspector/fonts/actions/index.js b/devtools/client/inspector/fonts/actions/index.js
--- a/devtools/client/inspector/fonts/actions/index.js
+++ b/devtools/client/inspector/fonts/actions/index.js
@@ -9,12 +9,9 @@ const { createEnum } = require("devtools
 createEnum([
 
   // Update the list of fonts.
   "UPDATE_FONTS",
 
   // Update the preview text.
   "UPDATE_PREVIEW_TEXT",
 
-  // Update whether to show all fonts.
-  "UPDATE_SHOW_ALL_FONTS",
-
 ], module.exports);
diff --git a/devtools/client/inspector/fonts/components/Font.js b/devtools/client/inspector/fonts/components/Font.js
--- a/devtools/client/inspector/fonts/components/Font.js
+++ b/devtools/client/inspector/fonts/components/Font.js
@@ -1,137 +1,162 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
-const { PureComponent } = require("devtools/client/shared/vendor/react");
+const { createFactory, PureComponent } = require("devtools/client/shared/vendor/react");
 const dom = require("devtools/client/shared/vendor/react-dom-factories");
 const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
 
+const FontPreview = createFactory(require("./FontPreview"));
+
 const { getStr } = require("../utils/l10n");
 const Types = require("../types");
 
 class Font extends PureComponent {
   static get propTypes() {
-    return PropTypes.shape(Types.font).isRequired;
+    return {
+      font: PropTypes.shape(Types.font).isRequired,
+      fontOptions: PropTypes.shape(Types.fontOptions).isRequired,
+      onPreviewFonts: PropTypes.func.isRequired,
+    };
   }
 
   constructor(props) {
     super(props);
-    this.renderFontCSS = this.renderFontCSS.bind(this);
-    this.renderFontCSSCode = this.renderFontCSSCode.bind(this);
-    this.renderFontFormatURL = this.renderFontFormatURL.bind(this);
-    this.renderFontName = this.renderFontName.bind(this);
-    this.renderFontPreview = this.renderFontPreview.bind(this);
+
+    this.state = {
+      isFontExpanded: false
+    };
+
+    this.onFontToggle = this.onFontToggle.bind(this);
+  }
+
+  componentWillReceiveProps(newProps) {
+    if (this.props.font.name === newProps.font.name) {
+      return;
+    }
+
+    this.setState({
+      isFontExpanded: false
+    });
+  }
+
+  onFontToggle() {
+    this.setState({
+      isFontExpanded: !this.state.isFontExpanded
+    });
   }
 
   renderFontCSS(cssFamilyName) {
     return dom.p(
       {
-        className: "font-css"
+        className: "font-css-name"
       },
-      dom.span(
-        {},
-        getStr("fontinspector.usedAs")
-      ),
-      " \"",
-      dom.span(
-        {
-          className: "font-css-name"
-        },
-        cssFamilyName
-      ),
-      "\""
+      `${getStr("fontinspector.usedAs")} "${cssFamilyName}"`
     );
   }
 
-  renderFontCSSCode(ruleText) {
+  renderFontCSSCode(rule, ruleText) {
+    if (!rule) {
+      return null;
+    }
+
     return dom.pre(
       {
-        className: "font-css-code"
+        className: "font-css-code",
       },
       ruleText
     );
   }
 
-  renderFontFormatURL(url, format) {
+  renderFontTypeAndURL(url, format) {
+    if (!url) {
+      return dom.p(
+        {
+          className: "font-format-url"
+        },
+        getStr("fontinspector.system")
+      );
+    }
+
     return dom.p(
       {
         className: "font-format-url"
       },
-      dom.input(
+      getStr("fontinspector.remote"),
+      dom.a(
         {
           className: "font-url",
-          readOnly: "readonly",
-          value: url
-        }
-      ),
-      " ",
-      format ?
-        dom.span(
-          {
-            className: "font-format"
-          },
-          format
-        )
-        :
-        dom.span(
-          {
-            className: "font-format",
-            hidden: "true"
-          },
-          format
-        )
+          href: url
+        },
+        format
+      )
     );
   }
 
   renderFontName(name) {
     return dom.h1(
       {
         className: "font-name",
+        onClick: this.onFontToggle,
       },
       name
     );
   }
 
-  renderFontPreview(previewUrl) {
-    return dom.div(
-      {
-        className: "font-preview-container",
-      },
-      dom.img(
-        {
-          className: "font-preview",
-          src: previewUrl
-        }
-      )
-    );
+  renderTwisty() {
+    let { isFontExpanded } = this.state;
+
+    let attributes = {
+      className: "theme-twisty",
+      onClick: this.onFontToggle,
+    };
+    if (isFontExpanded) {
+      attributes.open = "true";
+    }
+
+    return dom.span(attributes);
   }
 
   render() {
-    let { font } = this.props;
+    let {
+      font,
+      fontOptions,
+      onPreviewFonts,
+    } = this.props;
+
+    let { previewText } = fontOptions;
+
     let {
       CSSFamilyName,
       format,
       name,
       previewUrl,
       rule,
       ruleText,
       URI,
     } = font;
 
+    let { isFontExpanded } = this.state;
+
     return dom.li(
       {
-        className: "font",
+        className: "font" + (isFontExpanded ? " expanded" : ""),
       },
-      this.renderFontPreview(previewUrl),
+      this.renderTwisty(),
       this.renderFontName(name),
-      " " + (URI ? getStr("fontinspector.remote") : getStr("fontinspector.system")),
-      URI ? this.renderFontFormatURL(URI, format) : null,
-      this.renderFontCSS(CSSFamilyName),
-      rule ? this.renderFontCSSCode(ruleText) : null
+      FontPreview({ previewText, previewUrl, onPreviewFonts }),
+      dom.div(
+        {
+          className: "font-details"
+        },
+        this.renderFontTypeAndURL(URI, format),
+        this.renderFontCSSCode(rule, ruleText),
+        this.renderFontCSS(CSSFamilyName)
+      )
     );
   }
 }
 
 module.exports = Font;
diff --git a/devtools/client/inspector/fonts/components/FontList.js b/devtools/client/inspector/fonts/components/FontList.js
--- a/devtools/client/inspector/fonts/components/FontList.js
+++ b/devtools/client/inspector/fonts/components/FontList.js
@@ -10,33 +10,41 @@ const PropTypes = require("devtools/clie
 
 const Font = createFactory(require("./Font"));
 
 const Types = require("../types");
 
 class FontList extends PureComponent {
   static get propTypes() {
     return {
-      fonts: PropTypes.arrayOf(PropTypes.shape(Types.font)).isRequired
+      fontOptions: PropTypes.shape(Types.fontOptions).isRequired,
+      fonts: PropTypes.arrayOf(PropTypes.shape(Types.font)).isRequired,
+      onPreviewFonts: PropTypes.func.isRequired,
     };
   }
 
   render() {
-    let { fonts } = this.props;
+    let {
+      fonts,
+      fontOptions,
+      onPreviewFonts
+    } = this.props;
 
     return dom.div(
       {
         id: "font-container"
       },
       dom.ul(
         {
           id: "all-fonts"
         },
         fonts.map((font, i) => Font({
           key: i,
-          font
+          font,
+          fontOptions,
+          onPreviewFonts,
         }))
       )
     );
   }
 }
 
 module.exports = FontList;
diff --git a/devtools/client/inspector/fonts/components/FontPreview.js b/devtools/client/inspector/fonts/components/FontPreview.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/fonts/components/FontPreview.js
@@ -0,0 +1,93 @@
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+"use strict";
+
+const { PureComponent } = require("devtools/client/shared/vendor/react");
+const dom = require("devtools/client/shared/vendor/react-dom-factories");
+const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
+
+const Types = require("../types");
+
+class FontPreview extends PureComponent {
+  static get propTypes() {
+    return {
+      previewText: Types.fontOptions.previewText.isRequired,
+      previewUrl: Types.font.previewUrl.isRequired,
+      onPreviewFonts: PropTypes.func.isRequired,
+    };
+  }
+
+  constructor(props) {
+    super(props);
+
+    this.state = {
+      // Is the text preview input field currently focused?
+      isFocused: false,
+    };
+
+    this.onBlur = this.onBlur.bind(this);
+    this.onClick = this.onClick.bind(this);
+    this.onChange = this.onChange.bind(this);
+  }
+
+  componentDidUpdate() {
+    if (this.state.isFocused) {
+      let input = this.fontPreviewInput;
+      input.focus();
+      input.selectionStart = input.selectionEnd = input.value.length;
+    }
+  }
+
+  onBlur() {
+    this.setState({ isFocused: false });
+  }
+
+  onClick() {
+    this.setState({ isFocused: true });
+  }
+
+  onChange(event) {
+    this.props.onPreviewFonts(event.target.value);
+  }
+
+  render() {
+    let {
+      previewText,
+      previewUrl,
+    } = this.props;
+
+    let { isFocused } = this.state;
+
+    return dom.div(
+      {
+        className: "font-preview-container",
+      },
+      isFocused ?
+        dom.input(
+          {
+            type: "search",
+            className: "font-preview-input devtools-searchinput",
+            value: previewText,
+            onBlur: this.onBlur,
+            onChange: this.onChange,
+            ref: input => {
+              this.fontPreviewInput = input;
+            }
+          }
+        )
+        :
+        null,
+      dom.img(
+        {
+          className: "font-preview",
+          src: previewUrl,
+          onClick: this.onClick,
+        }
+      )
+    );
+  }
+}
+
+module.exports = FontPreview;
diff --git a/devtools/client/inspector/fonts/components/FontsApp.js b/devtools/client/inspector/fonts/components/FontsApp.js
--- a/devtools/client/inspector/fonts/components/FontsApp.js
+++ b/devtools/client/inspector/fonts/components/FontsApp.js
@@ -4,67 +4,48 @@
 
 "use strict";
 
 const { createFactory, PureComponent } = require("devtools/client/shared/vendor/react");
 const dom = require("devtools/client/shared/vendor/react-dom-factories");
 const PropTypes = require("devtools/client/shared/vendor/react-prop-types");
 const { connect } = require("devtools/client/shared/vendor/react-redux");
 
-const SearchBox = createFactory(require("devtools/client/shared/components/SearchBox"));
 const FontList = createFactory(require("./FontList"));
 
 const { getStr } = require("../utils/l10n");
 const Types = require("../types");
 
-const PREVIEW_UPDATE_DELAY = 150;
-
 class FontsApp extends PureComponent {
   static get propTypes() {
     return {
       fonts: PropTypes.arrayOf(PropTypes.shape(Types.font)).isRequired,
+      fontOptions: PropTypes.shape(Types.fontOptions).isRequired,
       onPreviewFonts: PropTypes.func.isRequired,
-      onShowAllFont: PropTypes.func.isRequired,
     };
   }
 
   render() {
     let {
       fonts,
+      fontOptions,
       onPreviewFonts,
-      onShowAllFont,
     } = this.props;
 
     return dom.div(
       {
         className: "theme-sidebar inspector-tabpanel",
         id: "sidebar-panel-fontinspector"
       },
-      dom.div(
-        {
-          className: "devtools-toolbar"
-        },
-        SearchBox({
-          delay: PREVIEW_UPDATE_DELAY,
-          placeholder: getStr("fontinspector.previewText"),
-          type: "text",
-          onChange: onPreviewFonts,
-        }),
-        dom.label(
-          {
-            id: "font-showall",
-            className: "theme-link",
-            title: getStr("fontinspector.seeAll.tooltip"),
-            onClick: onShowAllFont,
-          },
-          getStr("fontinspector.seeAll")
-        )
-      ),
       fonts.length ?
-        FontList({ fonts })
+        FontList({
+          fonts,
+          fontOptions,
+          onPreviewFonts
+        })
         :
         dom.div(
           {
             className: "devtools-sidepanel-no-result"
           },
           getStr("fontinspector.noFontsOnSelectedElement")
         )
     );
diff --git a/devtools/client/inspector/fonts/components/moz.build b/devtools/client/inspector/fonts/components/moz.build
--- a/devtools/client/inspector/fonts/components/moz.build
+++ b/devtools/client/inspector/fonts/components/moz.build
@@ -2,10 +2,11 @@
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 DevToolsModules(
     'Font.js',
     'FontList.js',
+    'FontPreview.js',
     'FontsApp.js',
 )
diff --git a/devtools/client/inspector/fonts/fonts.js b/devtools/client/inspector/fonts/fonts.js
--- a/devtools/client/inspector/fonts/fonts.js
+++ b/devtools/client/inspector/fonts/fonts.js
@@ -15,43 +15,41 @@ const { gDevTools } = require("devtools/
 
 const FontsApp = createFactory(require("./components/FontsApp"));
 
 const { LocalizationHelper } = require("devtools/shared/l10n");
 const INSPECTOR_L10N =
   new LocalizationHelper("devtools/client/locales/inspector.properties");
 
 const { updateFonts } = require("./actions/fonts");
-const { updatePreviewText, updateShowAllFonts } = require("./actions/font-options");
+const { updatePreviewText } = require("./actions/font-options");
 
 class FontInspector {
   constructor(inspector, window) {
     this.document = window.document;
     this.inspector = inspector;
     this.pageStyle = this.inspector.pageStyle;
     this.store = this.inspector.store;
 
     this.update = this.update.bind(this);
 
     this.onNewNode = this.onNewNode.bind(this);
     this.onPreviewFonts = this.onPreviewFonts.bind(this);
-    this.onShowAllFont = this.onShowAllFont.bind(this);
     this.onThemeChanged = this.onThemeChanged.bind(this);
 
     this.init();
   }
 
   init() {
     if (!this.inspector) {
       return;
     }
 
     let fontsApp = FontsApp({
       onPreviewFonts: this.onPreviewFonts,
-      onShowAllFont: this.onShowAllFont,
     });
 
     let provider = createElement(Provider, {
       id: "fontinspector",
       key: "fontinspector",
       store: this.store,
       title: INSPECTOR_L10N.getStr("inspector.sidebar.fontInspectorTitle"),
     }, fontsApp);
@@ -61,17 +59,16 @@ class FontInspector {
 
     this.inspector.selection.on("new-node-front", this.onNewNode);
     this.inspector.sidebar.on("fontinspector-selected", this.onNewNode);
 
     // Listen for theme changes as the color of the previews depend on the theme
     gDevTools.on("theme-switched", this.onThemeChanged);
 
     this.store.dispatch(updatePreviewText(""));
-    this.store.dispatch(updateShowAllFonts(false));
     this.update(false, "");
   }
 
   /**
    * Destruction function called when the inspector is destroyed. Removes event listeners
    * and cleans up references.
    */
   destroy() {
@@ -93,38 +90,29 @@ class FontInspector {
            this.inspector.sidebar.getCurrentTabID() === "fontinspector";
   }
 
   /**
    * Selection 'new-node' event handler.
    */
   onNewNode() {
     if (this.isPanelVisible()) {
-      this.store.dispatch(updateShowAllFonts(false));
       this.update();
     }
   }
 
   /**
    * Handler for change in preview input.
    */
   onPreviewFonts(value) {
     this.store.dispatch(updatePreviewText(value));
     this.update();
   }
 
   /**
-   * Handler for click on show all fonts button.
-   */
-  onShowAllFont() {
-    this.store.dispatch(updateShowAllFonts(true));
-    this.update();
-  }
-
-  /**
    * Handler for the "theme-switched" event.
    */
   onThemeChanged(event, frame) {
     if (frame === this.document.defaultView) {
       this.update();
     }
   }
 
@@ -132,44 +120,37 @@ class FontInspector {
     // Stop refreshing if the inspector or store is already destroyed.
     if (!this.inspector || !this.store) {
       return;
     }
 
     let node = this.inspector.selection.nodeFront;
     let fonts = [];
     let { fontOptions } = this.store.getState();
-    let { showAllFonts, previewText } = fontOptions;
+    let { previewText } = fontOptions;
 
     // Clear the list of fonts if the currently selected node is not connected or a text
     // or element node unless all fonts are supposed to be shown.
     let isElementOrTextNode = this.inspector.selection.isElementNode() ||
                               this.inspector.selection.isTextNode();
-    if (!showAllFonts &&
-        (!node ||
-         !this.isPanelVisible() ||
-         !this.inspector.selection.isConnected() ||
-         !isElementOrTextNode)) {
+    if (!node ||
+        !this.isPanelVisible() ||
+        !this.inspector.selection.isConnected() ||
+        !isElementOrTextNode) {
       this.store.dispatch(updateFonts(fonts));
       return;
     }
 
     let options = {
       includePreviews: true,
       previewText,
       previewFillStyle: getColor("body-color")
     };
 
-    if (showAllFonts) {
-      fonts = await this.pageStyle.getAllUsedFontFaces(options)
-                      .catch(console.error);
-    } else {
-      fonts = await this.pageStyle.getUsedFontFaces(node, options)
-                      .catch(console.error);
-    }
+    fonts = await this.pageStyle.getUsedFontFaces(node, options).catch(console.error);
 
     if (!fonts || !fonts.length) {
       // No fonts to display. Clear the previously shown fonts.
       if (this.store) {
         this.store.dispatch(updateFonts(fonts));
       }
       return;
     }
diff --git a/devtools/client/inspector/fonts/reducers/font-options.js b/devtools/client/inspector/fonts/reducers/font-options.js
--- a/devtools/client/inspector/fonts/reducers/font-options.js
+++ b/devtools/client/inspector/fonts/reducers/font-options.js
@@ -1,34 +1,28 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const {
   UPDATE_PREVIEW_TEXT,
-  UPDATE_SHOW_ALL_FONTS
 } = require("../actions/index");
 
 const INITIAL_FONT_OPTIONS = {
   previewText: "Abc",
-  showAllFonts: false,
 };
 
 let reducers = {
 
   [UPDATE_PREVIEW_TEXT](fontOptions, { previewText }) {
     return Object.assign({}, fontOptions, { previewText });
   },
 
-  [UPDATE_SHOW_ALL_FONTS](fontOptions, { showAllFonts }) {
-    return Object.assign({}, fontOptions, { showAllFonts });
-  },
-
 };
 
 module.exports = function (fontOptions = INITIAL_FONT_OPTIONS, action) {
   let reducer = reducers[action.type];
   if (!reducer) {
     return fontOptions;
   }
   return reducer(fontOptions, action);
diff --git a/devtools/client/inspector/fonts/test/browser.ini b/devtools/client/inspector/fonts/test/browser.ini
--- a/devtools/client/inspector/fonts/test/browser.ini
+++ b/devtools/client/inspector/fonts/test/browser.ini
@@ -11,11 +11,11 @@ support-files =
   !/devtools/client/framework/test/shared-head.js
   !/devtools/client/inspector/test/head.js
   !/devtools/client/inspector/test/shared-head.js
   !/devtools/client/shared/test/test-actor.js
   !/devtools/client/shared/test/test-actor-registry.js
 
 [browser_fontinspector.js]
 [browser_fontinspector_edit-previews.js]
-[browser_fontinspector_edit-previews-show-all.js]
+[browser_fontinspector_expand-details.js]
 [browser_fontinspector_text-node.js]
 [browser_fontinspector_theme-change.js]
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector.js b/devtools/client/inspector/fonts/test/browser_fontinspector.js
--- a/devtools/client/inspector/fonts/test/browser_fontinspector.js
+++ b/devtools/client/inspector/fonts/test/browser_fontinspector.js
@@ -35,53 +35,64 @@ const FONTS = [{
 add_task(function* () {
   let { inspector, view } = yield openFontInspectorForURL(TEST_URI);
   ok(!!view, "Font inspector document is alive.");
 
   let viewDoc = view.document;
 
   yield testBodyFonts(inspector, viewDoc);
   yield testDivFonts(inspector, viewDoc);
-  yield testShowAllFonts(inspector, viewDoc);
 });
 
-function isRemote(fontLiEl) {
-  return fontLiEl.querySelectorAll(".font-format-url").length === 1;
+function isRemote(fontLi) {
+  return fontLi.querySelectorAll(".font-format-url a").length === 1;
+}
+
+function getName(fontLi) {
+  return fontLi.querySelector(".font-name").textContent;
+}
+
+function getFormat(fontLi) {
+  let link = fontLi.querySelector(".font-format-url a");
+  if (!link) {
+    return null;
+  }
+
+  return link.textContent;
+}
+
+function getCSSName(fontLi) {
+  let text = fontLi.querySelector(".font-css-name").textContent;
+
+  return text.substring(text.indexOf('"') + 1, text.lastIndexOf('"'));
 }
 
 function* testBodyFonts(inspector, viewDoc) {
   let lis = viewDoc.querySelectorAll("#all-fonts > li");
   is(lis.length, 5, "Found 5 fonts");
 
   for (let i = 0; i < FONTS.length; i++) {
     let li = lis[i];
     let font = FONTS[i];
-    is(li.querySelector(".font-name").textContent, font.name,
-       "font " + i + " right font name");
 
-    is(isRemote(li), font.remote,
-       "font " + i + " remote value correct");
-    is(li.querySelector(".font-url").value, font.url,
-       "font " + i + " url correct");
-    is(li.querySelector(".font-format").hidden, !font.format,
-       "font " + i + " format hidden value correct");
-    is(li.querySelector(".font-format").textContent,
-       font.format, "font " + i + " format correct");
-    is(li.querySelector(".font-css-name").textContent,
-       font.cssName, "font " + i + " css name correct");
+    is(getName(li), font.name, "font " + i + " right font name");
+    is(isRemote(li), font.remote, "font " + i + " remote value correct");
+    is(li.querySelector(".font-url").href, font.url, "font " + i + " url correct");
+    is(getFormat(li), font.format, "font " + i + " format correct");
+    is(getCSSName(li), font.cssName, "font " + i + " css name correct");
   }
 
   // test that the bold and regular fonts have different previews
   let regSrc = lis[0].querySelector(".font-preview").src;
   let boldSrc = lis[1].querySelector(".font-preview").src;
   isnot(regSrc, boldSrc, "preview for bold font is different from regular");
 
   // test system font
-  let localFontName = lis[4].querySelector(".font-name").textContent;
-  let localFontCSSName = lis[4].querySelector(".font-css-name").textContent;
+  let localFontName = getName(lis[4]);
+  let localFontCSSName = getCSSName(lis[4]);
 
   // On Linux test machines, the Arial font doesn't exist.
   // The fallback is "Liberation Sans"
   ok((localFontName == "Arial") || (localFontName == "Liberation Sans"),
      "local font right font name");
   ok(!isRemote(lis[4]), "local font is local");
   ok((localFontCSSName == "Arial") || (localFontCSSName == "Liberation Sans"),
      "Arial", "local font has right css name");
@@ -89,25 +100,10 @@ function* testBodyFonts(inspector, viewD
 
 function* testDivFonts(inspector, viewDoc) {
   let updated = inspector.once("fontinspector-updated");
   yield selectNode("div", inspector);
   yield updated;
 
   let lis = viewDoc.querySelectorAll("#all-fonts > li");
   is(lis.length, 1, "Found 1 font on DIV");
-  is(lis[0].querySelector(".font-name").textContent,
-     "Ostrich Sans Medium",
-     "The DIV font has the right name");
+  is(getName(lis[0]), "Ostrich Sans Medium", "The DIV font has the right name");
 }
-
-function* testShowAllFonts(inspector, viewDoc) {
-  info("testing showing all fonts");
-
-  let updated = inspector.once("fontinspector-updated");
-  viewDoc.querySelector("#font-showall").click();
-  yield updated;
-
-  // shouldn't change the node selection
-  is(inspector.selection.nodeFront.nodeName, "DIV", "Show all fonts selected");
-  let lis = viewDoc.querySelectorAll("#all-fonts > li");
-  is(lis.length, 6, "Font inspector shows 6 fonts (1 from iframe)");
-}
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector_edit-previews-show-all.js b/devtools/client/inspector/fonts/test/browser_fontinspector_edit-previews-show-all.js
deleted file mode 100644
--- a/devtools/client/inspector/fonts/test/browser_fontinspector_edit-previews-show-all.js
+++ /dev/null
@@ -1,44 +0,0 @@
-/* vim: set ts=2 et sw=2 tw=80: */
-/* Any copyright is dedicated to the Public Domain.
-   http://creativecommons.org/publicdomain/zero/1.0/ */
-"use strict";
-
-// Test that correct previews are shown if the text is edited after 'Show all'
-// button is pressed.
-
-const TEST_URI = URL_ROOT + "browser_fontinspector.html";
-
-add_task(function* () {
-  let { inspector, view } = yield openFontInspectorForURL(TEST_URI);
-  let viewDoc = view.document;
-
-  info("Selecting a node that doesn't contain all document fonts.");
-  yield selectNode(".normal-text", inspector);
-
-  let normalTextNumPreviews =
-    viewDoc.querySelectorAll("#all-fonts .font-preview").length;
-
-  let onUpdated = inspector.once("fontinspector-updated");
-
-  info("Clicking 'Select all' button.");
-  viewDoc.getElementById("font-showall").click();
-
-  info("Waiting for font-inspector to update.");
-  yield onUpdated;
-
-  let allFontsNumPreviews =
-    viewDoc.querySelectorAll("#all-fonts .font-preview").length;
-
-  // Sanity check. If this fails all fonts apply also to the .normal-text node
-  // meaning we won't detect if preview editing causes the panel not to show all
-  // fonts.
-  isnot(allFontsNumPreviews, normalTextNumPreviews,
-    "The .normal-text didn't show all fonts.");
-
-  info("Editing the preview text.");
-  yield updatePreviewText(view, "The quick brown");
-
-  let numPreviews = viewDoc.querySelectorAll("#all-fonts .font-preview").length;
-  is(numPreviews, allFontsNumPreviews,
-    "All fonts are still shown after the preview text was edited.");
-});
diff --git a/devtools/client/inspector/fonts/test/browser_fontinspector_expand-details.js b/devtools/client/inspector/fonts/test/browser_fontinspector_expand-details.js
new file mode 100644
--- /dev/null
+++ b/devtools/client/inspector/fonts/test/browser_fontinspector_expand-details.js
@@ -0,0 +1,47 @@
+/* vim: set ts=2 et sw=2 tw=80: */
+/* Any copyright is dedicated to the Public Domain.
+   http://creativecommons.org/publicdomain/zero/1.0/ */
+
+"use strict";
+
+// Test that fonts are collapsed by default, and can be expanded.
+
+const TEST_URI = URL_ROOT + "browser_fontinspector.html";
+
+add_task(function* () {
+  let { inspector, view } = yield openFontInspectorForURL(TEST_URI);
+  let viewDoc = view.document;
+
+  info("Checking all font are collapsed by default");
+  let fonts = viewDoc.querySelectorAll("#all-fonts > li");
+  checkAllFontsCollapsed(fonts);
+
+  info("Clicking on the first one to expand the font details");
+  let onExpanded = BrowserTestUtils.waitForCondition(() => isFontDetailsVisible(fonts[0]),
+                                                     "Waiting for font details");
+  let twisty = fonts[0].querySelector(".theme-twisty");
+  twisty.click();
+  yield onExpanded;
+
+  ok(twisty.hasAttribute("open"), `Twisty is open`);
+  ok(isFontDetailsVisible(fonts[0]), `Font details is shown`);
+
+  info("Selecting a node with different fonts and checking that all fonts are collapsed");
+  yield selectNode(".black-text", inspector);
+  fonts = viewDoc.querySelectorAll("#all-fonts > li");
+  checkAllFontsCollapsed(fonts);
+});
+
+function isFontDetailsVisible(li) {
+  let details = li.querySelectorAll(".font-css-name, .font-css-code, .font-format-url");
+  return [...details].every(el => el.getBoxQuads().length);
+}
+
+function checkAllFontsCollapsed(fonts) {
+  fonts.forEach((el, i) => {
+    let twisty = el.querySelector(".theme-twisty");
+    ok(twisty, `Twisty ${i} exists`);
+    ok(!twisty.hasAttribute("open"), `Twisty ${i} is closed`);
+    ok(!isFontDetailsVisible(el), `Font details ${i} is hidden`);
+  });
+}
diff --git a/devtools/client/inspector/fonts/test/head.js b/devtools/client/inspector/fonts/test/head.js
--- a/devtools/client/inspector/fonts/test/head.js
+++ b/devtools/client/inspector/fonts/test/head.js
@@ -46,40 +46,44 @@ var openFontInspectorForURL = Task.async
   return {
     toolbox,
     inspector,
     view: inspector.fontinspector
   };
 });
 
 /**
- * Clears the preview input field, types new text into it and waits for the
+ * Focus one of the preview inputs, clear it, type new text into it and wait for the
  * preview images to be updated.
  *
  * @param {FontInspector} view - The FontInspector instance.
  * @param {String} text - The text to preview.
  */
 function* updatePreviewText(view, text) {
   info(`Changing the preview text to '${text}'`);
 
   let doc = view.document;
-  let input = doc.querySelector("#sidebar-panel-fontinspector .devtools-textinput");
-  let update = view.inspector.once("fontinspector-updated");
+  let previewImg = doc.querySelector("#sidebar-panel-fontinspector .font-preview");
 
-  info("Focusing the input field.");
-  input.focus();
+  info("Clicking the font preview element to turn it to edit mode");
+  let onClick = once(doc, "click");
+  previewImg.click();
+  yield onClick;
 
+  let input = previewImg.parentNode.querySelector("input");
   is(doc.activeElement, input, "The input was focused.");
 
   info("Blanking the input field.");
-  for (let i = input.value.length; i >= 0; i--) {
+  while (input.value.length) {
+    let update = view.inspector.once("fontinspector-updated");
     EventUtils.sendKey("BACK_SPACE", doc.defaultView);
+    yield update;
   }
 
-  is(input.value, "", "The input is now blank.");
+  if (text) {
+    info("Typing the specified text to the input field.");
+    let update = waitForNEvents(view.inspector, "fontinspector-updated", text.length);
+    EventUtils.sendString(text, doc.defaultView);
+    yield update;
+  }
 
-  info("Typing the specified text to the input field.");
-  EventUtils.sendString(text, doc.defaultView);
   is(input.value, text, "The input now contains the correct text.");
-
-  info("Waiting for the font-inspector to update.");
-  yield update;
 }
diff --git a/devtools/client/inspector/fonts/types.js b/devtools/client/inspector/fonts/types.js
--- a/devtools/client/inspector/fonts/types.js
+++ b/devtools/client/inspector/fonts/types.js
@@ -26,8 +26,13 @@ exports.font = {
   rule: PropTypes.object,
 
   // The text of the CSS rule
   ruleText: PropTypes.string,
 
   // The URI of the font file
   URI: PropTypes.string,
 };
+
+exports.fontOptions = {
+  // The current preview text
+  previewText: PropTypes.string,
+};
diff --git a/devtools/client/locales/en-US/font-inspector.properties b/devtools/client/locales/en-US/font-inspector.properties
--- a/devtools/client/locales/en-US/font-inspector.properties
+++ b/devtools/client/locales/en-US/font-inspector.properties
@@ -1,33 +1,22 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 # LOCALIZATION NOTE This file contains the Font Inspector strings.
 # The Font Inspector is a panel accessible in the Inspector sidebar.
 
-# LOCALIZATION NOTE (fontinspector.seeAll) This is the label of a link that will show all
-# the fonts used in the page, instead of the ones related to the inspected element.
-fontinspector.seeAll=Show all fonts used
-
-# LOCALIZATION NOTE (fontinspector.seeAll.tooltip) see fontinspector.seeAll.
-fontinspector.seeAll.tooltip=See all the fonts used in the page
-
 # LOCALIZATION NOTE (fontinspector.usedAs) This label introduces the name used to refer to
 # the font in a stylesheet.
 fontinspector.usedAs=Used as:
 
 # LOCALIZATION NOTE (fontinspector.system) This label indicates that the font is a local
 # system font.
 fontinspector.system=system
 
 # LOCALIZATION NOTE (fontinspector.remote) This label indicates that the font is a remote
 # font.
 fontinspector.remote=remote
 
-# LOCALIZATION NOTE (fontinspector.previewText):
-# This is the label shown as the placeholder in font inspector preview text box.
-fontinspector.previewText=Preview Text
-
 # LOCALIZATION NOTE (fontinspector.noFontsOnSelectedElement): This label is shown when
 # no fonts found on the selected element.
 fontinspector.noFontsOnSelectedElement=No fonts were found for the current element.
diff --git a/devtools/client/themes/fonts.css b/devtools/client/themes/fonts.css
--- a/devtools/client/themes/fonts.css
+++ b/devtools/client/themes/fonts.css
@@ -5,106 +5,133 @@
 #sidebar-panel-fontinspector {
   margin: 0;
   display: flex;
   flex-direction: column;
   width: 100%;
   height: 100%;
 }
 
-#sidebar-panel-fontinspector > .devtools-toolbar {
-  display: flex;
-}
-
 #font-container {
   overflow: auto;
   flex: auto;
 }
 
 #all-fonts {
   padding: 0;
   margin: 0;
-}
-
-#font-showall {
-  cursor: pointer;
-  flex-shrink: 0;
-}
-
-#font-showall:hover {
-  text-decoration: underline;
-}
-
-.font-format::before {
-  content: "(";
-}
-
-.font-format::after {
-  content: ")";
-}
-
-.preview-input-toolbar {
-  display: flex;
-  width: 100%;
-}
-
-.font-preview-container {
-  overflow-x: auto;
-}
-
-#font-preview-text-input {
-  margin-top: 1px;
-  margin-bottom: 1px;
-  padding-top: 0;
-  padding-bottom: 0;
-  flex: 1;
+  list-style: none;
 }
 
 .font {
-  padding: 10px 10px;
-}
-
-.theme-dark .font {
-  border-bottom: 1px solid #444;
+  border: 1px solid var(--theme-splitter-color);
+  border-width: 0 1px 1px 0;
+  display: grid;
+  grid-template-columns: 14px auto 1fr;
+  grid-template-rows: 50px;
+  grid-column-gap: 10px;
+  padding: 0 10px;
 }
 
-.theme-light .font {
-  border-bottom: 1px solid #DDD;
+#font-container .theme-twisty {
+  display: inline-block;
+  cursor: pointer;
+  place-self: center;
 }
 
-.font:last-of-type {
-  border-bottom: 0;
-}
-
-.theme-light .font:nth-child(even) {
-  background: #F4F4F4;
+.font-preview-container {
+  grid-column: 3 / -1;
+  grid-row: 1;
+  overflow: hidden;
+  display: grid;
+  place-items: center end;
+  position: relative;
 }
 
 .font-preview {
-  margin-left: -4px;
-  height: 60px;
+  height: 50px;
   display: block;
 }
 
+.font-preview:hover {
+  cursor: text;
+  background-image: linear-gradient(to right,
+    var(--grey-40) 3px, transparent 3px, transparent);
+  background-size: 6px 1px;
+  background-repeat: repeat-x;
+  background-position-y: 45px;
+}
+
+.font-preview-input {
+  position: absolute;
+  top: 0;
+  left: 0;
+  width: calc(100% - 5px);
+  height: calc(100% - 2px);
+  background: transparent;
+  color: transparent;
+}
+
+.font-preview-input::-moz-selection {
+  background: transparent;
+  color: transparent;
+}
+
 .font-name {
-  display: inline;
+  margin: 0;
+  font-size: 1em;
+  white-space: nowrap;
+  grid-column: 2;
+  place-self: center start;
+}
+
+.font-details {
+  grid-column: 2 / 4;
+  padding-inline-end: 14px;
+  width: 100%;
 }
 
 .font-css-code {
-  max-width: 100%;
+  direction: ltr;
+  padding: 5px;
+  margin: 0;
+  border: 1px solid var(--theme-splitter-color);
+  border-radius: 3px;
   overflow: hidden;
   text-overflow: ellipsis;
-  padding: 5px;
-  direction: ltr;
+  color: var(--theme-toolbar-color);
+}
+
+.font-format-url {
+  text-transform: capitalize;
+  margin-block-start: 0;
 }
 
-.theme-light .font-css-code,
-.theme-light .font-url {
-  border: 1px solid #CCC;
-  background: white;
+.font-url {
+  margin-inline-start: 1em;
+  text-transform: uppercase;
+  text-decoration: underline;
+  color: var(--theme-highlight-blue);
+  background: transparent;
+  border: none;
+  cursor: pointer;
 }
 
-.theme-dark .font-css-code,
-.theme-dark .font-url {
-  border: 1px solid #333;
-  background: black;
-  color: white;
+.font-url::after {
+  content: "";
+  display: inline-block;
+  height: 13px;
+  width: 13px;
+  margin: -.3rem .15rem 0 0.25rem;
+  vertical-align: middle;
+  background-image: url(chrome://devtools-shim/content/aboutdevtools/images/external-link.svg);
+  background-repeat: no-repeat;
+  background-size: 13px 13px;
+  -moz-context-properties: fill;
+  fill: var(--blue-60);
 }
+
+
+.font:not(.expanded) .font-css-name,
+.font:not(.expanded) .font-css-code,
+.font:not(.expanded) .font-format-url {
+  display: none;
+}
