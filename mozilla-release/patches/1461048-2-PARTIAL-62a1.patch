# HG changeset patch
# User Zibi Braniecki <zbraniecki@mozilla.com>
# Date 1526326661 25200
# Node ID 1ba2e60eb70b130aa71674c550012afd4ff18f6f
# Parent  c7fefd2e422295f7aed98357b19368df76cae15f
Bug 1461048 - Convert Fluent API calls to FluentDOM 0.3.0. r=mossop

MozReview-Commit-ID: K3eA8blcP2x

diff --git a/intl/l10n/DOMLocalization.jsm b/intl/l10n/DOMLocalization.jsm
--- a/intl/l10n/DOMLocalization.jsm
+++ b/intl/l10n/DOMLocalization.jsm
@@ -639,17 +639,18 @@ class DOMLocalization extends Localizati
       // operations during startup.
       // For details see bug 1441037, bug 1442262, and bug 1363862.
 
       // A sparse array which will store translations separated out from
       // all translations that is needed for DOM Overlay.
       const overlayTranslations = [];
 
       const getTranslationsForItems = async l10nItems => {
-        const keys = l10nItems.map(l10nItem => [l10nItem.l10nId, l10nItem.l10nArgs]);
+        const keys = l10nItems.map(
+          l10nItem => ({id: l10nItem.l10nId, args: l10nItem.l10nArgs}));
         const translations = await this.formatMessages(keys);
 
         // Here we want to separate out elements that require DOM Overlays.
         // Those elements will have to be translated using our JS
         // implementation, while everything else is going to use the fast-path.
         for (const [i, translation] of translations.entries()) {
           if (translation === undefined) {
             continue;
diff --git a/intl/l10n/docs/fluent_tutorial.rst b/intl/l10n/docs/fluent_tutorial.rst
--- a/intl/l10n/docs/fluent_tutorial.rst
+++ b/intl/l10n/docs/fluent_tutorial.rst
@@ -323,17 +323,17 @@ Non-Markup Localization
 -----------------------
 
 In rare cases, when the runtime code needs to retrieve the translation and not
 apply it onto the DOM, Fluent provides an API to retrieve it:
 
 .. code-block:: javascript
 
   let [ msg ] = await document.l10n.formatValues([
-    ["remove-containers-description"]
+    {id: "remove-containers-description"}
   ]);
 
   alert(msg);
 
 This model is heavily discouraged and should be used only in cases where the
 DOM annotation is not possible.
 
 .. note::
@@ -545,17 +545,17 @@ contexts manually using `Localization` c
   
   const myL10n = new Localization([
     "branding/brand.ftl",
     "browser/preferences/preferences.ftl"
   ]);
   
   
   let [isDefaultMsg, isNotDefaultMsg] =
-    myL10n.formatValues(["is-default", "is-not-default"]);
+    myL10n.formatValues({id: "is-default"}, {id: "is-not-default"});
 
 
 .. admonition:: Example
 
   An example of a use case is the Preferences UI in Firefox which uses the
   main context to localize the UI but also to build a search index.
 
   It is common to build such search index both in a current language and additionally
diff --git a/intl/l10n/test/test_localization.js b/intl/l10n/test/test_localization.js
--- a/intl/l10n/test/test_localization.js
+++ b/intl/l10n/test/test_localization.js
@@ -32,17 +32,17 @@ add_task(async function test_methods_cal
   async function* generateMessages(resIds) {
     yield * await L10nRegistry.generateContexts(["de", "en-US"], resIds);
   }
 
   const l10n = new Localization([
     "/browser/menu.ftl"
   ], generateMessages);
 
-  let values = await l10n.formatValues([["key"], ["key2"]]);
+  let values = await l10n.formatValues([{id: "key"}, {id: "key2"}]);
 
   equal(values[0], "[de] Value2");
   equal(values[1], "[en] Value3");
 
   L10nRegistry.sources.clear();
   L10nRegistry.ctxCache.clear();
   L10nRegistry.load = originalLoad;
   Services.locale.setRequestedLocales(originalRequested);
@@ -79,17 +79,17 @@ key = { PLATFORM() ->
   async function* generateMessages(resIds) {
     yield * await L10nRegistry.generateContexts(["en-US"], resIds);
   }
 
   const l10n = new Localization([
     "/test.ftl"
   ], generateMessages);
 
-  let values = await l10n.formatValues([["key"]]);
+  let values = await l10n.formatValues([{id: "key"}]);
 
   ok(values[0].includes(
     `${ known_platforms[AppConstants.platform].toUpperCase() } Value`));
 
   L10nRegistry.sources.clear();
   L10nRegistry.ctxCache.clear();
   L10nRegistry.load = originalLoad;
 });
