# HG changeset patch
# User Chris Peterson <cpeterson@mozilla.com>
# Date 1534313032 25200
#      Tue Aug 14 23:03:52 2018 -0700
# Node ID 36f6bab3d669cef41b9b2089e9081af388ce1c2b
# Parent  f9a890fba236bdcd13e72cbede68c9d6755f3b78
Bug 1483761 - Enable clang's -Wfloat-(overflow|zero)-conversion warnings. r=glandium

-Wfloat-overflow-conversion detects when a constant floating point value is converted to an integer type and will overflow the target type.

https://clang.llvm.org/docs/DiagnosticsReference.html#wfloat-overflow-conversion

-Wfloat-zero-conversion detects when a non-zero floating point value is converted to a zero integer value.

https://clang.llvm.org/docs/DiagnosticsReference.html#wfloat-zero-conversion

There are currently no -Wfloat-overlap-conversion warnings in mozilla-central. There is one -Wfloat-zero-conversion warning in a webrtc test. It doesn't block enabling this check because the webrtc tests are not compiled with warnings-as-errors.

media/webrtc/trunk/webrtc/modules/audio_coding/audio_network_adaptor/frame_length_controller_unittest.cc:255:54 [-Wfloat-zero-conversion] implicit conversion from 'const float' to 'int' changes non-zero value from 0.045000002 to 0

We can't enable all -Wfloat-conversion warnings (for any implicit conversion of a floating-point number into an integer) because there are currently over 1400 warnings. I spot checked a few of these -Wfloat-conversion warnings. I didn't find any obvious bugs, but there is some suspicious code, such as implicit conversions of floats to bools.

Differential Revision: https://phabricator.services.mozilla.com/D3476

diff --git a/build/moz.configure/warnings.configure b/build/moz.configure/warnings.configure
--- a/build/moz.configure/warnings.configure
+++ b/build/moz.configure/warnings.configure
@@ -45,16 +45,20 @@ check_and_add_gcc_warning('-Wunreachable
 add_gcc_warning('-Wwrite-strings', cxx_compiler)
 
 # turned on by -Wall, but we use offsetof on non-POD types frequently
 add_gcc_warning('-Wno-invalid-offsetof', cxx_compiler)
 
 # catches objects passed by value to variadic functions.
 check_and_add_gcc_warning('-Wclass-varargs')
 
+# catches some implicit conversion of floats to ints
+check_and_add_gcc_warning('-Wfloat-overflow-conversion')
+check_and_add_gcc_warning('-Wfloat-zero-conversion')
+
 # catches issues around loops
 check_and_add_gcc_warning('-Wloop-analysis')
 
 # catches C++ version forward-compat issues
 check_and_add_gcc_warning('-Wc++1z-compat', cxx_compiler)
 check_and_add_gcc_warning('-Wc++2a-compat', cxx_compiler)
 
 # catches possible misuse of the comma operator
