# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1614043605 0
#      Tue Feb 23 01:26:45 2021 +0000
# Node ID 2d78e4bc3367320976d35629044085e8ee26a1fe
# Parent  e42b1e9178d2725962db0e7bb52a3880f3242f23
Bug 1692940 - Switch jpeg build to nasm instead of yasm. r=firefox-build-system-reviewers,dmajor

Differential Revision: https://phabricator.services.mozilla.com/D105428

diff --git a/media/libjpeg/moz.build b/media/libjpeg/moz.build
--- a/media/libjpeg/moz.build
+++ b/media/libjpeg/moz.build
@@ -70,18 +70,18 @@ SOURCES += [
     'jcsample.c',
     'jctrans.c',
 ]
 
 SOURCES += [
     'jpeg_nbits_table.c',
 ]
 
-if CONFIG['LIBJPEG_TURBO_USE_YASM']:
-    USE_YASM = True
+if CONFIG['LIBJPEG_TURBO_USE_NASM']:
+    USE_NASM = True
 
 if CONFIG['LIBJPEG_TURBO_ASFLAGS']:
     if CONFIG['CPU_ARCH'] == 'arm':
         SOURCES += [
             'simd/arm/jsimd.c',
             'simd/arm/jsimd_neon.S',
         ]
     elif CONFIG['CPU_ARCH'] == 'aarch64':
diff --git a/toolkit/moz.configure b/toolkit/moz.configure
--- a/toolkit/moz.configure
+++ b/toolkit/moz.configure
@@ -1348,51 +1348,45 @@ with only_when(compile_environment):
         set_config('MOZ_JPEG_CFLAGS', jpeg_flags.cflags)
         set_config('MOZ_JPEG_LIBS', jpeg_flags.ldflags)
 
     @depends('--with-system-jpeg', target)
     def in_tree_jpeg(system_jpeg, target):
         if system_jpeg:
             return
 
-        flags = ()
-        use_yasm = None
-        need_yasm = False
         if target.kernel == 'Darwin':
             if target.cpu == 'x86':
-                flags = ('-DPIC', '-DMACHO')
+                return ("-DPIC", "-DMACHO")
             elif target.cpu == 'x86_64':
-                flags = ('-D__x86_64__', '-DPIC', '-DMACHO')
+                return ("-D__x86_64__", "-DPIC", "-DMACHO")
         elif target.kernel == 'WINNT':
             if target.cpu == 'x86':
-                flags = ('-DPIC', '-DWIN32')
+                return ("-DPIC", "-DWIN32")
             elif target.cpu == 'x86_64':
-                flags = ('-D__x86_64__', '-DPIC', '-DWIN64', '-DMSVC')
+                return ("-D__x86_64__", "-DPIC", "-DWIN64", "-DMSVC")
         elif target.cpu == 'arm':
-            flags = ('-march=armv7-a', '-mfpu=neon')
+            return ("-march=armv7-a", "-mfpu=neon")
         elif target.cpu == 'aarch64':
-            flags = ('-march=armv8-a',)
+            return ("-march=armv8-a",)
         elif target.cpu == 'mips32':
-            flags = ('-mdspr2',)
+            return ("-mdspr2",)
         elif target.cpu == 'x86':
-            flags = ('-DPIC', '-DELF')
+            return ("-DPIC", "-DELF")
         elif target.cpu == 'x86_64':
-            flags = ('-D__x86_64__', '-DPIC', '-DELF')
+            return ("-D__x86_64__", "-DPIC", "-DELF")
 
+    @depends(target, when=in_tree_jpeg)
+    def jpeg_nasm(target):
         if target.cpu in ('x86', 'x86_64'):
-            use_yasm = True
-            if target.kernel == 'Linux' and target.os == 'GNU':
-                need_yasm = Version('1.0.1')
-            else:
-                need_yasm = Version('1.1')
+            # libjpeg-turbo 2.0.6 requires nasm 2.10.
+            return namespace(version="2.10", what="JPEG")
 
-        return namespace(flags=flags, use_yasm=use_yasm, need_yasm=need_yasm)
-
-    set_config('LIBJPEG_TURBO_USE_YASM', in_tree_jpeg.use_yasm)
-    set_config('LIBJPEG_TURBO_ASFLAGS', in_tree_jpeg.flags)
+    set_config("LIBJPEG_TURBO_USE_NASM", True, when=jpeg_nasm)
+    set_config("LIBJPEG_TURBO_ASFLAGS", in_tree_jpeg)
 
 
 # FFmpeg's ffvpx configuration
 # ==============================================================
 with only_when(compile_environment):
     @depends(target)
     def libav_fft(target):
         return target.kernel == "WINNT" or target.cpu == "x86_64"
@@ -1465,23 +1459,23 @@ with only_when(compile_environment):
     set_config('MOZ_FFVPX', True, when=ffvpx.enable)
     set_define('MOZ_FFVPX', True, when=ffvpx.enable)
     set_config('MOZ_FFVPX_FLACONLY', True, when=ffvpx.flac_only)
     set_define('MOZ_FFVPX_FLACONLY', True, when=ffvpx.flac_only)
     set_config('FFVPX_ASFLAGS', ffvpx.flags)
     set_config("FFVPX_USE_YASM", True, when=ffvpx.need_yasm)
 
 
-@depends(yasm_version, in_tree_jpeg.use_yasm,
-         ffvpx.need_yasm)
+@depends(yasm_version,
+         ffvpx.use_yasm,
+)
 @imports(_from='__builtin__', _import='sorted')
-def valid_yasm_version(yasm_version, for_jpeg, for_ffvpx=False):
+def valid_yasm_version(yasm_version, for_ffvpx=False):
     # Note: the default for for_ffvpx above only matters for unit tests.
     requires = {
-        'jpeg': for_jpeg,
         'ffvpx': for_ffvpx,
     }
     requires = {k: v for (k, v) in requires.items() if v}
     if requires and not yasm_version:
         items = sorted(requires.keys())
         if len(items) > 1:
             what = ' and '.join((', '.join(items[:-1]), items[-1]))
         else:
@@ -1495,17 +1489,17 @@ def valid_yasm_version(yasm_version, for
         what, version = by_version[-1]
         if yasm_version < version:
             die('Yasm version %s or greater is required to build with %s.'
                 % (version, what))
 
 
 # nasm detection
 # ==============================================================
-@depends(dav1d_nasm, vpx_nasm)
+@depends(dav1d_nasm, vpx_nasm, jpeg_nasm)
 def need_nasm(*requirements):
     requires = {
         x.what: x.version if hasattr(x, "version") else True for x in requirements if x
     }
     if requires:
         items = sorted(requires.keys())
         if len(items) > 1:
             what = " and ".join((", ".join(items[:-1]), items[-1]))
