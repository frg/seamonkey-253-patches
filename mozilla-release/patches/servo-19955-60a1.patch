# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1517842235 18000
# Node ID f22417a9e3d7ae81e0044169fe824578eef0e2cf
# Parent  261b83dd642301d58fd70b29cc09e92506b7dd90
servo: Merge #19955 - style: Make input[type=number] pseudo-elements accessible to chrome (from emilio:number-pseudos-chrome); r=jwatt

Bug: 1433389
Reviewed-by: jwatt
MozReview-Commit-ID: 2ycajPYd3CV
Source-Repo: https://github.com/servo/servo
Source-Revision: 3a3a7cdc22ecdc63ff39d802f669d0a0052aa251

diff --git a/servo/components/style/gecko/pseudo_element.rs b/servo/components/style/gecko/pseudo_element.rs
--- a/servo/components/style/gecko/pseudo_element.rs
+++ b/servo/components/style/gecko/pseudo_element.rs
@@ -119,26 +119,36 @@ impl PseudoElement {
     }
 
     /// Whether this pseudo-element is lazily-cascaded.
     #[inline]
     pub fn is_lazy(&self) -> bool {
         !self.is_eager() && !self.is_precomputed()
     }
 
-    /// Whether this pseudo-element is web-exposed.
-    pub fn exposed_in_non_ua_sheets(&self) -> bool {
-        (self.flags() & structs::CSS_PSEUDO_ELEMENT_UA_SHEET_ONLY) == 0
-    }
-
     /// Whether this pseudo-element supports user action selectors.
     pub fn supports_user_action_state(&self) -> bool {
         (self.flags() & structs::CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE) != 0
     }
 
+    /// Whether this pseudo-element is enabled for all content.
+    pub fn enabled_in_content(&self) -> bool {
+        (self.flags() & structs::CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS_AND_CHROME) == 0
+    }
+
+    /// Whether this pseudo is enabled explicitly in UA sheets.
+    pub fn enabled_in_ua_sheets(&self) -> bool {
+        (self.flags() & structs::CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS) != 0
+    }
+
+    /// Whether this pseudo is enabled explicitly in chrome sheets.
+    pub fn enabled_in_chrome(&self) -> bool {
+        (self.flags() & structs::CSS_PSEUDO_ELEMENT_ENABLED_IN_CHROME) != 0
+    }
+
     /// Whether this pseudo-element skips flex/grid container display-based
     /// fixup.
     #[inline]
     pub fn skip_item_display_fixup(&self) -> bool {
         (self.flags() & structs::CSS_PSEUDO_ELEMENT_IS_FLEX_OR_GRID_ITEM) == 0
     }
 
     /// Whether this pseudo-element is precomputed.
diff --git a/servo/components/style/gecko/pseudo_element_definition.mako.rs b/servo/components/style/gecko/pseudo_element_definition.mako.rs
--- a/servo/components/style/gecko/pseudo_element_definition.mako.rs
+++ b/servo/components/style/gecko/pseudo_element_definition.mako.rs
@@ -109,17 +109,17 @@ impl PseudoElement {
     /// anonymous box.
     pub fn flags(&self) -> u32 {
         match *self {
             % for pseudo in PSEUDOS:
                 ${pseudo_element_variant(pseudo)} =>
                 % if pseudo.is_tree_pseudo_element():
                     0,
                 % elif pseudo.is_anon_box():
-                    structs::CSS_PSEUDO_ELEMENT_UA_SHEET_ONLY,
+                    structs::CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS,
                 % else:
                     structs::SERVO_CSS_PSEUDO_ELEMENT_FLAGS_${pseudo.original_ident},
                 % endif
             % endfor
         }
     }
 
     /// Construct a pseudo-element from a `CSSPseudoElementType`.
@@ -219,29 +219,34 @@ impl PseudoElement {
     /// Constructs an atom from a string of text, and whether we're in a
     /// user-agent stylesheet.
     ///
     /// If we're not in a user-agent stylesheet, we will never parse anonymous
     /// box pseudo-elements.
     ///
     /// Returns `None` if the pseudo-element is not recognised.
     #[inline]
-    pub fn from_slice(s: &str, in_ua_stylesheet: bool) -> Option<Self> {
-        #[allow(unused_imports)] use std::ascii::AsciiExt;
-
+    pub fn from_slice(name: &str) -> Option<Self> {
         // We don't need to support tree pseudos because functional
         // pseudo-elements needs arguments, and thus should be created
         // via other methods.
-        % for pseudo in SIMPLE_PSEUDOS:
-            if in_ua_stylesheet || ${pseudo_element_variant(pseudo)}.exposed_in_non_ua_sheets() {
-                if s.eq_ignore_ascii_case("${pseudo.value[1:]}") {
-                    return Some(${pseudo_element_variant(pseudo)});
+        match_ignore_ascii_case! { name,
+            % for pseudo in SIMPLE_PSEUDOS:
+            "${pseudo.value[1:]}" => {
+                return Some(${pseudo_element_variant(pseudo)})
+            }
+            % endfor
+            _ => {
+                // FIXME: -moz-tree check should probably be
+                // ascii-case-insensitive.
+                if name.starts_with("-moz-tree-") {
+                    return PseudoElement::tree_pseudo_element(name, Box::new([]))
                 }
             }
-        % endfor
+        }
 
         None
     }
 
     /// Constructs a tree pseudo-element from the given name and arguments.
     /// "name" must start with "-moz-tree-".
     ///
     /// Returns `None` if the pseudo-element is not recognized.
diff --git a/servo/components/style/gecko/selector_parser.rs b/servo/components/style/gecko/selector_parser.rs
--- a/servo/components/style/gecko/selector_parser.rs
+++ b/servo/components/style/gecko/selector_parser.rs
@@ -154,20 +154,20 @@ impl NonTSPseudoClass {
             }
         }
         apply_non_ts_list!(pseudo_class_check_is_enabled_in)
     }
 
     /// Returns whether the pseudo-class is enabled in content sheets.
     fn is_enabled_in_content(&self) -> bool {
         use gecko_bindings::structs::mozilla;
-        match self {
+        match *self {
             // For pseudo-classes with pref, the availability in content
             // depends on the pref.
-            &NonTSPseudoClass::Fullscreen =>
+            NonTSPseudoClass::Fullscreen =>
                 unsafe { mozilla::StylePrefs_sUnprefixedFullscreenApiEnabled },
             // Otherwise, a pseudo-class is enabled in content when it
             // doesn't have any enabled flag.
             _ => !self.has_any_flag(NonTSPseudoClassFlag::PSEUDO_CLASS_ENABLED_IN_UA_SHEETS_AND_CHROME),
         }
     }
 
     /// <https://drafts.csswg.org/selectors-4/#useraction-pseudos>
@@ -317,16 +317,35 @@ impl<'a> SelectorParser<'a> {
         if self.chrome_rules_enabled() &&
            pseudo_class.has_any_flag(NonTSPseudoClassFlag::PSEUDO_CLASS_ENABLED_IN_CHROME)
         {
             return true;
         }
 
         return false;
     }
+
+    fn is_pseudo_element_enabled(
+        &self,
+        pseudo_element: &PseudoElement,
+    ) -> bool {
+        if pseudo_element.enabled_in_content() {
+            return true;
+        }
+
+        if self.in_user_agent_stylesheet() && pseudo_element.enabled_in_ua_sheets() {
+            return true;
+        }
+
+        if self.chrome_rules_enabled() && pseudo_element.enabled_in_chrome() {
+            return true;
+        }
+
+        return false;
+    }
 }
 
 impl<'a, 'i> ::selectors::Parser<'i> for SelectorParser<'a> {
     type Impl = SelectorImpl;
     type Error = StyleParseErrorKind<'i>;
 
     fn parse_slotted(&self) -> bool {
         // NOTE(emilio): Slot assignment and such works per-document, but
@@ -413,27 +432,25 @@ impl<'a, 'i> ::selectors::Parser<'i> for
         }
     }
 
     fn parse_pseudo_element(
         &self,
         location: SourceLocation,
         name: CowRcStr<'i>,
     ) -> Result<PseudoElement, ParseError<'i>> {
-        PseudoElement::from_slice(&name, self.in_user_agent_stylesheet())
-            .or_else(|| {
-                // FIXME: -moz-tree check should probably be
-                // ascii-case-insensitive.
-                if name.starts_with("-moz-tree-") {
-                    PseudoElement::tree_pseudo_element(&name, Box::new([]))
-                } else {
-                    None
-                }
-            })
-            .ok_or(location.new_custom_error(SelectorParseErrorKind::UnsupportedPseudoClassOrElement(name.clone())))
+        if let Some(pseudo) = PseudoElement::from_slice(&name) {
+            if self.is_pseudo_element_enabled(&pseudo) {
+                return Ok(pseudo);
+            }
+        }
+
+        Err(location.new_custom_error(
+            SelectorParseErrorKind::UnsupportedPseudoClassOrElement(name)
+        ))
     }
 
     fn parse_functional_pseudo_element<'t>(
         &self,
         name: CowRcStr<'i>,
         parser: &mut Parser<'i, 't>,
     ) -> Result<PseudoElement, ParseError<'i>> {
         // FIXME: -moz-tree check should probably be ascii-case-insensitive.
@@ -451,17 +468,19 @@ impl<'a, 'i> ::selectors::Parser<'i> for
                     _ => unreachable!("Parser::next() shouldn't return any other error"),
                 }
             }
             let args = args.into_boxed_slice();
             if let Some(pseudo) = PseudoElement::tree_pseudo_element(&name, args) {
                 return Ok(pseudo);
             }
         }
-        Err(parser.new_custom_error(SelectorParseErrorKind::UnsupportedPseudoClassOrElement(name.clone())))
+        Err(parser.new_custom_error(
+            SelectorParseErrorKind::UnsupportedPseudoClassOrElement(name)
+        ))
     }
 
     fn default_namespace(&self) -> Option<Namespace> {
         self.namespaces.default.clone().as_ref().map(|&(ref ns, _)| ns.clone())
     }
 
     fn namespace_for_prefix(&self, prefix: &Atom) -> Option<Namespace> {
         self.namespaces.prefixes.get(prefix).map(|&(ref ns, _)| ns.clone())
