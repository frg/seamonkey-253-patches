# HG changeset patch
# User Alexander Surkov <surkov.alexander@gmail.com>
# Date 1522765617 14400
# Node ID a11cb2c5d1f9f9159a62cb3b311f369a32019869
# Parent  b9a19d50f51cec21ad141b1acb5f46c89fd85590
Bug 1449530 - clean up ATK states mapping, r=eeejay

diff --git a/accessible/atk/AccessibleWrap.cpp b/accessible/atk/AccessibleWrap.cpp
--- a/accessible/atk/AccessibleWrap.cpp
+++ b/accessible/atk/AccessibleWrap.cpp
@@ -925,30 +925,28 @@ static void
 TranslateStates(uint64_t aState, AtkStateSet* aStateSet)
 {
   // atk doesn't have a read only state so read only things shouldn't be
   // editable.
   if (aState & states::READONLY)
     aState &= ~states::EDITABLE;
 
   // Convert every state to an entry in AtkStateMap
-  uint32_t stateIndex = 0;
   uint64_t bitMask = 1;
-  while (gAtkStateMap[stateIndex].stateMapEntryType != kNoSuchState) {
+  for (auto stateIndex = 0U; stateIndex < gAtkStateMapLen; stateIndex++) {
     if (gAtkStateMap[stateIndex].atkState) { // There's potentially an ATK state for this
       bool isStateOn = (aState & bitMask) != 0;
       if (gAtkStateMap[stateIndex].stateMapEntryType == kMapOpposite) {
         isStateOn = !isStateOn;
       }
       if (isStateOn) {
         atk_state_set_add_state(aStateSet, gAtkStateMap[stateIndex].atkState);
       }
     }
     bitMask <<= 1;
-    ++ stateIndex;
   }
 }
 
 AtkStateSet *
 refStateSetCB(AtkObject *aAtkObj)
 {
   AtkStateSet *state_set = nullptr;
   state_set = ATK_OBJECT_CLASS(parent_class)->ref_state_set(aAtkObj);
@@ -1534,34 +1532,42 @@ a11y::ProxyCaretMoveEvent(ProxyAccessibl
 {
   AtkObject* wrapper = GetWrapperFor(aTarget);
   g_signal_emit_by_name(wrapper, "text_caret_moved", aOffset);
 }
 
 void
 MaiAtkObject::FireStateChangeEvent(uint64_t aState, bool aEnabled)
 {
-    int32_t stateIndex = AtkStateMap::GetStateIndexFor(aState);
-    if (stateIndex >= 0) {
-        NS_ASSERTION(gAtkStateMap[stateIndex].stateMapEntryType != kNoSuchState,
-                     "No such state");
+  auto state = aState;
+  int32_t stateIndex = -1;
+  while (state > 0) {
+    ++stateIndex;
+    state >>= 1;
+  }
 
-        if (gAtkStateMap[stateIndex].atkState != kNone) {
-            NS_ASSERTION(gAtkStateMap[stateIndex].stateMapEntryType != kNoStateChange,
-                         "State changes should not fired for this state");
+  MOZ_ASSERT(stateIndex >= 0 && stateIndex < gAtkStateMapLen,
+             "No ATK state for internal state was found");
+  if (stateIndex < 0 || stateIndex >= static_cast<int32_t>(gAtkStateMapLen)) {
+    return;
+  }
 
-            if (gAtkStateMap[stateIndex].stateMapEntryType == kMapOpposite)
-                aEnabled = !aEnabled;
+  if (gAtkStateMap[stateIndex].atkState != kNone) {
+    MOZ_ASSERT(gAtkStateMap[stateIndex].stateMapEntryType != kNoStateChange,
+                 "State changes should not fired for this state");
 
-            // Fire state change for first state if there is one to map
-            atk_object_notify_state_change(&parent,
-                                           gAtkStateMap[stateIndex].atkState,
-                                           aEnabled);
-        }
+    if (gAtkStateMap[stateIndex].stateMapEntryType == kMapOpposite) {
+      aEnabled = !aEnabled;
     }
+
+    // Fire state change for first state if there is one to map
+    atk_object_notify_state_change(&parent,
+                                   gAtkStateMap[stateIndex].atkState,
+                                   aEnabled);
+  }
 }
 
 void
 a11y::ProxyTextChangeEvent(ProxyAccessible* aTarget, const nsString& aStr,
                            int32_t aStart, uint32_t aLen, bool aIsInsert,
                            bool aFromUser)
 {
   MaiAtkObject* atkObj = MAI_ATK_OBJECT(GetWrapperFor(aTarget));
diff --git a/accessible/atk/nsStateMap.h b/accessible/atk/nsStateMap.h
--- a/accessible/atk/nsStateMap.h
+++ b/accessible/atk/nsStateMap.h
@@ -2,16 +2,18 @@
 /* vim: set ts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include <atk/atk.h>
 #include "AccessibleWrap.h"
 
+#include <type_traits>
+
 /******************************************************************************
 The following accessible states aren't translated, just ignored:
   STATE_READONLY:        Supported indirectly via EXT_STATE_EDITABLE
   STATE_HOTTRACKED:      No ATK equivalent.  No known use case.
                          The nsIAccessible state is not currently supported.
   STATE_FLOATING:        No ATK equivalent.  No known use case.
                          The nsIAccessible state is not currently supported.
   STATE_MOVEABLE:        No ATK equivalent.  No known use case.
@@ -34,34 +36,23 @@ The following ATK states are not support
   ATK_STATE_TRUNCATED:   No clear use case. Indicates that an object's onscreen content is truncated,
                          e.g. a text value in a spreadsheet cell. No IA2 state.
 ******************************************************************************/
 
 enum EStateMapEntryType {
   kMapDirectly,
   kMapOpposite,   // For example, UNAVAILABLE is the opposite of ENABLED
   kNoStateChange, // Don't fire state change event
-  kNoSuchState
 };
 
 const AtkStateType kNone = ATK_STATE_INVALID;
 
 struct AtkStateMap {
   AtkStateType atkState;
   EStateMapEntryType stateMapEntryType;
-
-  static int32_t GetStateIndexFor(uint64_t aState)
-  {
-    int32_t stateIndex = -1;
-    while (aState > 0) {
-      ++ stateIndex;
-      aState >>= 1;
-    }
-    return stateIndex;  // Returns -1 if not mapped
-  }
 };
 
 
 // Map array from cross platform states to ATK states
 static const AtkStateMap gAtkStateMap[] = {                     // Cross Platform States
   { kNone,                                    kMapOpposite },   // states::UNAVAILABLE             = 1 << 0
   { ATK_STATE_SELECTED,                       kMapDirectly },   // states::SELECTED                = 1 << 1
   { ATK_STATE_FOCUSED,                        kMapDirectly },   // states::FOCUSED                 = 1 << 2
@@ -105,11 +96,15 @@ static const AtkStateMap gAtkStateMap[] 
   { ATK_STATE_SINGLE_LINE,                    kMapDirectly },   // states::SINGLE_LINE             = 1 << 40
   { ATK_STATE_TRANSIENT,                      kMapDirectly },   // states::TRANSIENT               = 1 << 41
   { ATK_STATE_VERTICAL,                       kMapDirectly },   // states::VERTICAL                = 1 << 42
   { ATK_STATE_STALE,                          kMapDirectly },   // states::STALE                   = 1 << 43
   { ATK_STATE_ENABLED,                        kMapDirectly },   // states::ENABLED                 = 1 << 44
   { ATK_STATE_SENSITIVE,                      kMapDirectly },   // states::SENSITIVE               = 1 << 45
   { ATK_STATE_EXPANDABLE,                     kMapDirectly },   // states::EXPANDABLE              = 1 << 46
   { kNone,                                    kMapDirectly },   // states::PINNED                  = 1 << 47
-  { ATK_STATE_ACTIVE,                         kMapDirectly },   // states::CURRENT                 = 1 << 48
-  { kNone,                                    kNoSuchState },   //                                 = 1 << 49
+  { ATK_STATE_ACTIVE,                         kMapDirectly }    // states::CURRENT                 = 1 << 48
 };
+
+static const auto gAtkStateMapLen = std::extent<decltype(gAtkStateMap)>::value;
+
+static_assert(((uint64_t) 0x1) << (gAtkStateMapLen - 1) == mozilla::a11y::states::LAST_ENTRY,
+              "ATK states map is out of sync with internal states");
diff --git a/accessible/base/States.h b/accessible/base/States.h
--- a/accessible/base/States.h
+++ b/accessible/base/States.h
@@ -278,14 +278,19 @@ namespace states {
    */
   const uint64_t PINNED = ((uint64_t) 0x1) << 47;
 
   /**
    * The object is the current item within a container or set of related elements.
    */
   const uint64_t CURRENT = ((uint64_t) 0x1) << 48;
 
+  /**
+   * Not a real state, used for static assertions.
+   */
+  const uint64_t LAST_ENTRY = CURRENT;
+
 } // namespace states
 } // namespace a11y
 } // namespace mozilla
 
 #endif
 

