# HG changeset patch
# User Jed Davis <jld@mozilla.com>
# Date 1523391867 21600
# Node ID 062265dbddb227da8313bbbdecc9a05926be5815
# Parent  8dd86546cc66f05745a460dc2766fcea2b756238
Bug 1278361 - Step 1: Update eintr_wrapper.h to bring in IGNORE_EINTR. r=froydnj

This is based on the current security/sandbox/chromium version of eintr_wrapper.h,
taken from upstream commit 937db09514e061d7983e90e0c448cfa61680f605.

I've edited it to remove some things that aren't relevant to us: the
debug-mode loop limit in HANDLE_EINTR, because we don't seem to be
having the problem it's meant to fix and it risks regressions, and
references to Fuchsia, which we don't (yet) support.  I also kept the
original include guards (the file path has changed upstream).

What this patch *does* do is add IGNORE_EINTR and modernize the C++
slightly (using decltype instead of nonstandard typeof).

MozReview-Commit-ID: BO4uQL9jUtf

diff --git a/ipc/chromium/src/base/eintr_wrapper.h b/ipc/chromium/src/base/eintr_wrapper.h
--- a/ipc/chromium/src/base/eintr_wrapper.h
+++ b/ipc/chromium/src/base/eintr_wrapper.h
@@ -1,35 +1,51 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
-// Copyright (c) 2009 The Chromium Authors. All rights reserved.
+// Copyright (c) 2012 The Chromium Authors. All rights reserved.
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
 // This provides a wrapper around system calls which may be interrupted by a
 // signal and return EINTR. See man 7 signal.
 //
-// On Windows, this wrapper macro does nothing.
+// On Windows, this wrapper macro does nothing because there are no
+// signals.
+//
+// Don't wrap close calls in HANDLE_EINTR. Use IGNORE_EINTR if the return
+// value of close is significant. See http://crbug.com/269623.
 
 #ifndef BASE_EINTR_WRAPPER_H_
 #define BASE_EINTR_WRAPPER_H_
 
 #include "build/build_config.h"
 
 #if defined(OS_POSIX)
 
 #include <errno.h>
 
 #define HANDLE_EINTR(x) ({ \
-  typeof(x) __eintr_result__; \
+  decltype(x) eintr_wrapper_result; \
   do { \
-    __eintr_result__ = x; \
-  } while (__eintr_result__ == -1 && errno == EINTR); \
-  __eintr_result__;\
+    eintr_wrapper_result = (x); \
+  } while (eintr_wrapper_result == -1 && errno == EINTR); \
+  eintr_wrapper_result; \
+})
+
+#define IGNORE_EINTR(x) ({ \
+  decltype(x) eintr_wrapper_result; \
+  do { \
+    eintr_wrapper_result = (x); \
+    if (eintr_wrapper_result == -1 && errno == EINTR) { \
+      eintr_wrapper_result = 0; \
+    } \
+  } while (0); \
+  eintr_wrapper_result; \
 })
 
 #else
 
-#define HANDLE_EINTR(x) x
+#define HANDLE_EINTR(x) (x)
+#define IGNORE_EINTR(x) (x)
 
 #endif  // OS_POSIX
 
 #endif  // !BASE_EINTR_WRAPPER_H_

