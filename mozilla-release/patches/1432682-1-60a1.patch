# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1519674629 21600
# Node ID 9114315e792d29e3fc41a0748e683ded96f689d5
# Parent  1d44d1b81b27c42909ac9b1285a5d616003acbc9
Bug 1432682 - Part 1: Remove the hack that causes the bad behavior. r=jimb.

The hack caused bytecode for block declaration instantiation to be assigned the
location of the first statement inside the block. Unfortunately it made the
source view of the debugger client seem out of sync with the Scopes panel: when
paused after hitting a breakpoint on that line or stepping there, the source
panel showed our location as being inside the block, but the Scopes panel did
not show a block scope.

Two server tests required fixes (also r=jimb, in a separate patch in the same
bug).

test_stepping-08.js assumes that stepping into a function stops at the first
statement in the function. This is usually true. However, now we are removing a
hack, such that our actual behavior for this *particular* function is to stop
at the opening curly brace. This causes the test to fail, without anything
really being broken.

The test is intended to test the interaction of stepping and breakpoints, so
the fix that stays truest to the purpose of the test is to change the debuggee
here to a function with no prologue instructions, so that we don't stop at the
opening brace.

test_blackboxing-01.js is a similar story.

diff --git a/devtools/server/tests/unit/test_blackboxing-01.js b/devtools/server/tests/unit/test_blackboxing-01.js
--- a/devtools/server/tests/unit/test_blackboxing-01.js
+++ b/devtools/server/tests/unit/test_blackboxing-01.js
@@ -91,17 +91,17 @@ const testBlackBox = async function () {
 
   finishClient(gClient);
 };
 
 function evalCode() {
   /* eslint-disable */
   Cu.evalInSandbox(
     "" + function doStuff(k) { // line 1
-      let arg = 15;            // line 2 - Step in here
+      var arg = 15;            // line 2 - Step in here
       k(arg);                  // line 3
     },                         // line 4
     gDebuggee,
     "1.8",
     BLACK_BOXED_URL,
     1
   );
 
diff --git a/devtools/server/tests/unit/test_stepping-08.js b/devtools/server/tests/unit/test_stepping-08.js
--- a/devtools/server/tests/unit/test_stepping-08.js
+++ b/devtools/server/tests/unit/test_stepping-08.js
@@ -55,18 +55,18 @@ function evaluateTestCode() {
   /* eslint-disable */
   Cu.evalInSandbox(
     `                                   //  1
     function outerFunction() {          //  2
       debugger; innerFunction();        //  3
     }                                   //  4
                                         //  5
     function innerFunction() {          //  6
-      let x = 0;                        //  7
-      let y = 72;                       //  8
+      var x = 0;                        //  7
+      var y = 72;                       //  8
       return x+y;                       //  9
     }                                   // 10
     outerFunction();                    // 11
     `,                                  // 12
     gDebuggee,
     "1.8",
     "test_stepping-08-test-code.js",
     1
diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -891,17 +891,17 @@ BytecodeEmitter::EmitterScope::enterLexi
                                             Handle<LexicalScope::Data*> bindings)
 {
     MOZ_ASSERT(kind != ScopeKind::NamedLambda && kind != ScopeKind::StrictNamedLambda);
     MOZ_ASSERT(this == bce->innermostEmitterScopeNoCheck());
 
     if (!ensureCache(bce))
         return false;
 
-    // Marks all names as closed over if the the context requires it. This
+    // Marks all names as closed over if the context requires it. This
     // cannot be done in the Parser as we may not know if the context requires
     // all bindings to be closed over until after parsing is finished. For
     // example, legacy generators require all bindings to be closed over but
     // it is unknown if a function is a legacy generator until the first
     // 'yield' expression is parsed.
     //
     // This is not a problem with other scopes, as all other scopes with
     // bindings are body-level. At the time of their creation, whether or not
@@ -7273,34 +7273,22 @@ BytecodeEmitter::emitLexicalScope(ParseN
     MOZ_ASSERT(pn->isKind(ParseNodeKind::LexicalScope));
 
     TDZCheckCache tdzCache(this);
 
     ParseNode* body = pn->scopeBody();
     if (pn->isEmptyScope())
         return emitLexicalScopeBody(body);
 
-    // Update line number notes before emitting TDZ poison in
-    // EmitterScope::enterLexical to avoid spurious pausing on seemingly
-    // non-effectful lines in Debugger.
-    //
-    // For example, consider the following code.
-    //
-    // L1: {
-    // L2:   let x = 42;
-    // L3: }
-    //
-    // If line number notes were not updated before the TDZ poison, the TDZ
-    // poison bytecode sequence of 'uninitialized; initlexical' will have line
-    // number L1, and the Debugger will pause there.
+    // We are about to emit some bytecode for what the spec calls "declaration
+    // instantiation". Assign these instructions to the opening `{` of the
+    // block. (Using the location of each declaration we're instantiating is
+    // too weird when stepping in the debugger.)
     if (!ParseNodeRequiresSpecialLineNumberNotes(body)) {
-        ParseNode* pnForPos = body;
-        if (body->isKind(ParseNodeKind::StatementList) && body->pn_head)
-            pnForPos = body->pn_head;
-        if (!updateLineNumberNotes(pnForPos->pn_pos.begin))
+        if (!updateSourceCoordNotes(pn->pn_pos.begin))
             return false;
     }
 
     EmitterScope emitterScope(this);
     ScopeKind kind;
     if (body->isKind(ParseNodeKind::Catch))
         kind = (!body->pn_left || body->pn_left->isKind(ParseNodeKind::Name))
                ? ScopeKind::SimpleCatch
diff --git a/js/src/jit-test/tests/debug/Frame-onStep-19.js b/js/src/jit-test/tests/debug/Frame-onStep-19.js
--- a/js/src/jit-test/tests/debug/Frame-onStep-19.js
+++ b/js/src/jit-test/tests/debug/Frame-onStep-19.js
@@ -13,17 +13,17 @@ testStepping(
       (function() {              // line 1
         let x = 1;               // line 2
         funcb("funcb");          // line 3
         function funcb(msg) {    // line 4
           console.log(msg)
         }
       })                         // line 7
     `,
-    [2, 3, 7]);
+    [1, 2, 3, 7]);
 
 // Stopping at the ClassDeclaration on line 8 is fine. For that matter,
 // stopping on line 5 wouldn't be so bad if we did it after line 3 and before
 // line 8; alas, the actual order of execution is 5, 2, 3, 8... which is too
 // confusing.
 testStepping(
     `\
       function f() {    //  1
@@ -33,9 +33,9 @@ testStepping(
         function a() {  //  5
           x += 1;       //  6
         }               //  7
         class Car {}    //  8
         return x;       //  9
       }                 // 10
       f
     `,
-    [2, 3, 8, 9, 10]);
+    [1, 2, 3, 8, 9, 10]);
