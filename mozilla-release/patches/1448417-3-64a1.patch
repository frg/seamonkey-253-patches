# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1537900223 0
# Node ID a80cb43e8d5ea8e5227ea335b65ab7b7ebae3ba8
# Parent  1acd29479611efc9cdc0b5016109ec8ead57eca6
Bug 1448417 - [mozlint] Remove ability to specify globs in an 'include' directive, r=egao

There is only a single linter (test-disable.yml) that uses a glob in any
include path, and that usage is easily replaced by using the 'extensions' key
instead.

Since globs in include directives aren't very useful, let's disallow them. This
will allow us to simplify the 'filterpaths' logic quite substantially and make
future refactorings in this area easier.

Differential Revision: https://phabricator.services.mozilla.com/D6798

diff --git a/python/mozlint/mozlint/parser.py b/python/mozlint/mozlint/parser.py
--- a/python/mozlint/mozlint/parser.py
+++ b/python/mozlint/mozlint/parser.py
@@ -48,16 +48,19 @@ class Parser(object):
 
             if not isinstance(linter[attr], list) or \
                     not all(isinstance(a, basestring) for a in linter[attr]):
                 raise LinterParseError(relpath, "The {} directive must be a "
                                                 "list of strings!".format(attr))
             invalid_paths = set()
             for path in linter[attr]:
                 if '*' in path:
+                    if attr == 'include':
+                        raise LinterParseError(relpath, "Paths in the include directive cannot "
+                                                        "contain globs:\n  {}".format(path))
                     continue
 
                 abspath = path
                 if not os.path.isabs(abspath):
                     abspath = os.path.join(self.root, path)
 
                 if not os.path.exists(abspath):
                     invalid_paths.add('  ' + path)
@@ -96,10 +99,11 @@ class Parser(object):
             raise LinterParseError(path, "No lint definitions found!")
 
         linters = []
         for name, linter in config.iteritems():
             linter['name'] = name
             linter['path'] = path
             self._validate(linter)
             linter.setdefault('support-files', []).append(path)
+            linter.setdefault('include', ['.'])
             linters.append(linter)
         return linters
diff --git a/python/mozlint/mozlint/pathutils.py b/python/mozlint/mozlint/pathutils.py
--- a/python/mozlint/mozlint/pathutils.py
+++ b/python/mozlint/mozlint/pathutils.py
@@ -10,25 +10,22 @@ from mozpack import path as mozpath
 from mozpack.files import FileFinder
 
 
 class FilterPath(object):
     """Helper class to make comparing and matching file paths easier."""
     def __init__(self, path, exclude=None):
         self.path = os.path.normpath(path)
         self._finder = None
-        self.exclude = exclude
 
     @property
     def finder(self):
         if self._finder:
             return self._finder
-        self._finder = FileFinder(
-            mozpath.normsep(self.path),
-            ignore=[mozpath.normsep(e) for e in self.exclude])
+        self._finder = FileFinder(mozpath.normsep(self.path))
         return self._finder
 
     @property
     def ext(self):
         return os.path.splitext(self.path)[1].strip('.')
 
     @property
     def exists(self):
@@ -89,41 +86,39 @@ def filterpaths(paths, linter, **lintarg
     if not lintargs.get('use_filters', True) or (not include and not exclude):
         return paths
 
     def normalize(path):
         if '*' not in path and not os.path.isabs(path):
             path = os.path.join(root, path)
         return FilterPath(path)
 
+    # Includes are always paths and should always exist.
     include = map(normalize, include)
-    exclude = map(normalize, exclude)
 
-    # Paths with and without globs will be handled separately,
+    # Exclude paths with and without globs will be handled separately,
     # pull them apart now.
-    includepaths = [p for p in include if p.exists]
+    exclude = map(normalize, exclude)
     excludepaths = [p for p in exclude if p.exists]
-
-    includeglobs = [p for p in include if not p.exists]
-    excludeglobs = [p for p in exclude if not p.exists]
+    excludeglobs = [p.path for p in exclude if not p.exists]
 
     extensions = linter.get('extensions')
     keep = set()
     discard = set()
     for path in map(FilterPath, paths):
         # Exclude bad file extensions
         if extensions and path.isfile and path.ext not in extensions:
             continue
 
         if path.match(excludeglobs):
             continue
 
         # First handle include/exclude directives
         # that exist (i.e don't have globs)
-        for inc in includepaths:
+        for inc in include:
             # Only excludes that are subdirectories of the include
             # path matter.
             excs = [e for e in excludepaths if inc.contains(e)]
 
             if path.contains(inc):
                 # If specified path is an ancestor of include path,
                 # then lint the include path.
                 keep.add(inc)
@@ -136,35 +131,21 @@ def filterpaths(paths, linter, **lintarg
 
             elif inc.contains(path):
                 # If the include path is an ancestor of the specified
                 # path, then add the specified path only if there are
                 # no exclude paths in-between them.
                 if not any(e.contains(path) for e in excs):
                     keep.add(path)
 
-        # Next handle include/exclude directives that
-        # contain globs.
-        if path.isfile:
-            # If the specified path is a file it must be both
-            # matched by an include directive and not matched
-            # by an exclude directive.
-            if not path.match(includeglobs) or any(e.contains(path) for e in excludepaths):
-                continue
-
-            keep.add(path)
-        elif path.isdir:
-            # If the specified path is a directory, use a
-            # FileFinder to resolve all relevant globs.
-            path.exclude = [os.path.relpath(e.path, root) for e in exclude]
-            for pattern in includeglobs:
-                for p, f in path.finder.find(pattern.path):
-                    if extensions and os.path.splitext(p)[1][1:] not in extensions:
-                        continue
-                    keep.add(path.join(p))
+        # Next expand excludes with globs in them so we can add them to
+        # the set of files to discard.
+        for pattern in excludeglobs:
+            for p, f in path.finder.find(pattern):
+                discard.add(path.join(p))
 
     # Only pass paths we couldn't exclude here to the underlying linter
     lintargs['exclude'] = [f.path for f in discard]
     return [f.path for f in keep]
 
 
 def findobject(path):
     """
diff --git a/python/mozlint/test/linters/invalid_include_with_glob.yml b/python/mozlint/test/linters/invalid_include_with_glob.yml
new file mode 100644
--- /dev/null
+++ b/python/mozlint/test/linters/invalid_include_with_glob.yml
@@ -0,0 +1,6 @@
+---
+BadIncludeLinterWithGlob:
+    description: Has an invalid include directive.
+    include: ['**/*.js']
+    type: string
+    payload: foobar
diff --git a/python/mozlint/test/linters/raises.yml b/python/mozlint/test/linters/raises.yml
--- a/python/mozlint/test/linters/raises.yml
+++ b/python/mozlint/test/linters/raises.yml
@@ -1,5 +1,6 @@
 ---
 RaisesLinter:
     description: Raises an exception
+    include: ['.']
     type: external
     payload: external:raises
diff --git a/python/mozlint/test/linters/regex.yml b/python/mozlint/test/linters/regex.yml
--- a/python/mozlint/test/linters/regex.yml
+++ b/python/mozlint/test/linters/regex.yml
@@ -1,11 +1,10 @@
 ---
 RegexLinter:
     description: >-
         Make sure the string foobar never appears in a js variable
         file because it is bad.
     rule: no-foobar
-    include:
-        - '**/*.js'
-        - '**/*.jsm'
+    include: ['.']
+    extensions: ['js', '.jsm']
     type: regex
     payload: foobar
diff --git a/python/mozlint/test/linters/string.yml b/python/mozlint/test/linters/string.yml
--- a/python/mozlint/test/linters/string.yml
+++ b/python/mozlint/test/linters/string.yml
@@ -1,12 +1,9 @@
 ---
 StringLinter:
     description: >-
         Make sure the string foobar never appears in browser js
         files because it is bad
     rule: no-foobar
-    include:
-        - '**/*.js'
-        - '**/*.jsm'
+    extensions: ['.js', 'jsm']
     type: string
-    extensions: ['.js', 'jsm']
     payload: foobar
diff --git a/python/mozlint/test/linters/warning.yml b/python/mozlint/test/linters/warning.yml
--- a/python/mozlint/test/linters/warning.yml
+++ b/python/mozlint/test/linters/warning.yml
@@ -1,13 +1,11 @@
 ---
 WarningLinter:
     description: >-
         Make sure the string foobar never appears in browser js
         files because it is bad, but not too bad (just a warning)
     rule: no-foobar
     level: warning
-    include:
-        - '**/*.js'
-        - '**/*.jsm'
+    include: ['.']
     type: string
     extensions: ['.js', 'jsm']
     payload: foobar
diff --git a/python/mozlint/test/test_parser.py b/python/mozlint/test/test_parser.py
--- a/python/mozlint/test/test_parser.py
+++ b/python/mozlint/test/test_parser.py
@@ -35,23 +35,26 @@ def test_parse_valid_linter(parse):
 
     lintobj = lintobj[0]
     assert isinstance(lintobj, dict)
     assert 'name' in lintobj
     assert 'description' in lintobj
     assert 'type' in lintobj
     assert 'payload' in lintobj
     assert 'extensions' in lintobj
+    assert 'include' in lintobj
+    assert lintobj['include'] == ['.']
     assert set(lintobj['extensions']) == set(['js', 'jsm'])
 
 
 @pytest.mark.parametrize('linter', [
     'invalid_type.yml',
     'invalid_extension.ym',
     'invalid_include.yml',
+    'invalid_include_with_glob.yml',
     'invalid_exclude.yml',
     'invalid_support_files.yml',
     'missing_attrs.yml',
     'missing_definition.yml',
     'non_existing_include.yml',
     'non_existing_exclude.yml',
     'non_existing_support_files.yml',
 ])
diff --git a/python/mozlint/test/test_pathutils.py b/python/mozlint/test/test_pathutils.py
--- a/python/mozlint/test/test_pathutils.py
+++ b/python/mozlint/test/test_pathutils.py
@@ -52,52 +52,49 @@ def test_no_filter(filterpaths):
         'exclude': ['**/*.py'],
         'use_filters': False,
     }
 
     paths = filterpaths(**args)
     assert_paths(paths, args['paths'])
 
 
-def test_extensions(filterpaths):
-    args = {
-        'paths': ['a.py', 'a.js', 'subdir2'],
-        'include': ['**'],
-        'exclude': [],
-        'extensions': ['py'],
-    }
-
-    paths = filterpaths(**args)
-    assert_paths(paths, ['a.py', 'subdir2/c.py'])
-
-
 TEST_CASES = (
     {
         'paths': ['a.js', 'subdir1/subdir3/d.js'],
         'include': ['.'],
         'exclude': ['subdir1'],
         'expected': ['a.js'],
     },
     {
         'paths': ['a.js', 'subdir1/subdir3/d.js'],
         'include': ['subdir1/subdir3'],
         'exclude': ['subdir1'],
         'expected': ['subdir1/subdir3/d.js'],
     },
     {
         'paths': ['.'],
-        'include': ['**/*.py'],
+        'include': ['.'],
         'exclude': ['**/c.py', 'subdir1/subdir3'],
+        'extensions': ['py'],
+        'expected': ['.'],
+    },
+    {
+        'paths': ['a.py', 'a.js', 'subdir1/b.py', 'subdir2/c.py', 'subdir1/subdir3/d.py'],
+        'include': ['.'],
+        'exclude': ['**/c.py', 'subdir1/subdir3'],
+        'extensions': ['py'],
         'expected': ['a.py', 'subdir1/b.py'],
     },
     {
-        'paths': ['a.py', 'a.js', 'subdir1/b.py', 'subdir2/c.py', 'subdir1/subdir3/d.py'],
-        'include': ['**/*.py'],
-        'exclude': ['**/c.py', 'subdir1/subdir3'],
-        'expected': ['a.py', 'subdir1/b.py'],
+        'paths': ['a.py', 'a.js', 'subdir2'],
+        'include': ['.'],
+        'exclude': [],
+        'extensions': ['py'],
+        'expected': ['a.py', 'subdir2'],
     },
 )
 
 
 @pytest.mark.parametrize('test', TEST_CASES)
 def test_filterpaths(filterpaths, test):
     expected = test.pop('expected')
 
diff --git a/python/mozlint/test/test_roller.py b/python/mozlint/test/test_roller.py
--- a/python/mozlint/test/test_roller.py
+++ b/python/mozlint/test/test_roller.py
@@ -56,20 +56,17 @@ def test_roll_from_subdir(lint, linters)
         result = lint.roll('no_foobar.js')
         assert len(result.issues) == 0
         assert len(result.failed) == 0
         assert result.returncode == 0
 
         # Path relative to root doesn't work
         result = lint.roll(os.path.join('files', 'no_foobar.js'))
         assert len(result.issues) == 0
-        # TODO Only the external linter is failing, the other two
-        # should be failing as well. Not using xfail so we don't
-        # lose coverage from the rest of the test.
-        assert len(result.failed) == 1
+        assert len(result.failed) == 3
         assert result.returncode == 1
 
         # Paths from vcs are always joined to root instead of cwd
         lint.mock_vcs([os.path.join('files', 'no_foobar.js')])
         result = lint.roll(outgoing=True)
         assert len(result.issues) == 0
         assert len(result.failed) == 0
         assert result.returncode == 0
diff --git a/tools/lint/docs/create.rst b/tools/lint/docs/create.rst
--- a/tools/lint/docs/create.rst
+++ b/tools/lint/docs/create.rst
@@ -7,18 +7,17 @@ be python code alongside the definition,
 Here's a trivial example:
 
 no-eval.yml
 
 .. code-block::
 
     EvalLinter:
         description: Ensures the string eval doesn't show up.
-        include:
-            - "**/*.js"
+        extensions: ['js']
         type: string
         payload: eval
 
 Now ``no-eval.yml`` gets passed into :func:`LintRoller.read`.
 
 
 Linter Types
 ------------
@@ -56,18 +55,18 @@ this case the signature for lint functio
 Linter Definition
 -----------------
 
 Each ``.yml`` file must have at least one linter defined in it. Here are the supported keys:
 
 * description - A brief description of the linter's purpose (required)
 * type - One of 'string', 'regex' or 'external' (required)
 * payload - The actual linting logic, depends on the type (required)
-* include - A list of glob patterns that must be matched (optional)
-* exclude - A list of glob patterns that must not be matched (optional)
+* include - A list of file paths that will be considered (optional)
+* exclude - A list of file paths or glob patterns that must not be matched (optional)
 * extensions - A list of file extensions to be considered (optional)
 * setup - A function that sets up external dependencies (optional)
 * support-files - A list of glob patterns matching configuration files (optional)
 
 In addition to the above, some ``.yml`` files correspond to a single lint rule. For these, the
 following additional keys may be specified:
 
 * message - A string to print on infraction (optional)
@@ -148,18 +147,18 @@ let's call the file ``flake8_lint.py``:
         return results
 
 Now here is the linter definition that would call it:
 
 .. code-block:: yml
 
     flake8:
         description: Python linter
-        include:
-            - '**/*.py'
+        include: ['.']
+        extensions: ['py']
         type: external
         payload: py.flake8:lint
         support-files:
             - '**/.flake8'
 
 Notice the payload has two parts, delimited by ':'. The first is the module
 path, which ``mozlint`` will attempt to import. The second is the object path
 within that module (e.g, the name of a function to call). It is up to consumers
@@ -183,18 +182,18 @@ Either way, to reduce the burden on user
 automated bootstrapping of all their dependencies. To help with this,
 ``mozlint`` allows linters to define a ``setup`` config, which has the same
 path object format as an external payload. For example:
 
 .. code-block:: yml
 
     flake8:
         description: Python linter
-        include:
-            - '**/*.py'
+        include: ['.']
+        extensions: ['py']
         type: external
         payload: py.flake8:lint
         setup: py.flake8:setup
 
 The setup function takes a single argument, the root of the repository being
 linted. In the case of ``flake8``, it might look like:
 
 .. code-block:: python
diff --git a/tools/lint/test-disable.yml b/tools/lint/test-disable.yml
--- a/tools/lint/test-disable.yml
+++ b/tools/lint/test-disable.yml
@@ -1,13 +1,12 @@
 ---
 no-comment-disable:
     description: "Use 'disable=<reason>' to disable a test instead of a comment"
-    include:
-        - "**/*.ini"
+    include: ['.']
     exclude:
         - "**/application.ini"
         - "**/l10n.ini"
         - testing/mozbase/manifestparser/tests
         - testing/web-platform
         - third_party
         - xpcom/tests/unit/data
     extensions: ['ini']
