# HG changeset patch
# User Alex Chronopoulos <achronop@gmail.com>
# Date 1527846016 -10800
# Node ID 4b57c07f8c2cd55c94a766b769808ccba71868bf
# Parent  b54db66223586b4e04f5cb926fccdacf8a176b91
Bug 1466066 - Update cubeb from upstream to abf6ae2. r=kinetik

diff --git a/media/libcubeb/README_MOZILLA b/media/libcubeb/README_MOZILLA
--- a/media/libcubeb/README_MOZILLA
+++ b/media/libcubeb/README_MOZILLA
@@ -1,8 +1,8 @@
 The source from this directory was copied from the cubeb
 git repository using the update.sh script.  The only changes
 made were those applied by update.sh and the addition of
 Makefile.in build files for the Mozilla build system.
 
 The cubeb git repository is: git://github.com/kinetiknz/cubeb.git
 
-The git commit ID used was 44341a1e0658a3939d29c0b5a230ca095ae63dd3 (2018-05-01 22:09:43 +1200)
+The git commit ID used was abf6ae235b0f15a2656f2d8692ac13708188165e (2018-06-01 13:02:45 +1200)
diff --git a/media/libcubeb/src/cubeb_audiounit.cpp b/media/libcubeb/src/cubeb_audiounit.cpp
--- a/media/libcubeb/src/cubeb_audiounit.cpp
+++ b/media/libcubeb/src/cubeb_audiounit.cpp
@@ -72,17 +72,17 @@ static vector<AudioObjectID>
 audiounit_get_devices_of_type(cubeb_device_type devtype);
 static UInt32 audiounit_get_device_presentation_latency(AudioObjectID devid, AudioObjectPropertyScope scope);
 
 extern cubeb_ops const audiounit_ops;
 
 struct cubeb {
   cubeb_ops const * ops = &audiounit_ops;
   owned_critical_section mutex;
-  atomic<int> active_streams{ 0 };
+  int active_streams = 0;
   uint32_t global_latency_frames = 0;
   cubeb_device_collection_changed_callback collection_changed_callback = nullptr;
   void * collection_changed_user_ptr = nullptr;
   /* Differentiate input from output devices. */
   cubeb_device_type collection_changed_devtype = CUBEB_DEVICE_TYPE_UNKNOWN;
   vector<AudioObjectID> devtype_device_array;
   // The queue is asynchronously deallocated once all references to it are released
   dispatch_queue_t serial_queue = dispatch_queue_create(DISPATCH_QUEUE_LABEL, DISPATCH_QUEUE_SERIAL);
@@ -316,21 +316,42 @@ AudioConvertHostTimeToNanos(uint64_t hos
     answer *= timebase_info.numer;
     answer /= timebase_info.denom;
   }
   return (uint64_t)answer;
 }
 #endif
 
 static void
-audiounit_set_global_latency(cubeb_stream * stm, uint32_t latency_frames)
+audiounit_increment_active_streams(cubeb * ctx)
+{
+  ctx->mutex.assert_current_thread_owns();
+  ctx->active_streams += 1;
+}
+
+static void
+audiounit_decrement_active_streams(cubeb * ctx)
 {
-  stm->mutex.assert_current_thread_owns();
-  assert(stm->context->active_streams == 1);
-  stm->context->global_latency_frames = latency_frames;
+  ctx->mutex.assert_current_thread_owns();
+  ctx->active_streams -= 1;
+}
+
+static int
+audiounit_active_streams(cubeb * ctx)
+{
+  ctx->mutex.assert_current_thread_owns();
+  return ctx->active_streams;
+}
+
+static void
+audiounit_set_global_latency(cubeb * ctx, uint32_t latency_frames)
+{
+  ctx->mutex.assert_current_thread_owns();
+  assert(audiounit_active_streams(ctx) == 1);
+  ctx->global_latency_frames = latency_frames;
 }
 
 static void
 audiounit_make_silent(AudioBuffer * ioData)
 {
   assert(ioData);
   assert(ioData->mData);
   memset(ioData->mData, 0, ioData->mDataByteSize);
@@ -1252,24 +1273,25 @@ audiounit_get_current_channel_layout(Aud
 
 static int audiounit_create_unit(AudioUnit * unit, device_info * device);
 
 static OSStatus audiounit_remove_device_listener(cubeb * context);
 
 static void
 audiounit_destroy(cubeb * ctx)
 {
-  // Disabling this assert for bug 1083664 -- we seem to leak a stream
-  // assert(ctx->active_streams == 0);
-  if (ctx->active_streams > 0) {
-    LOG("(%p) API misuse, %d streams active when context destroyed!", ctx, ctx->active_streams.load());
-  }
-
   {
     auto_lock lock(ctx->mutex);
+
+    // Disabling this assert for bug 1083664 -- we seem to leak a stream
+    // assert(ctx->active_streams == 0);
+    if (audiounit_active_streams(ctx) > 0) {
+      LOG("(%p) API misuse, %d streams active when context destroyed!", ctx, audiounit_active_streams(ctx));
+    }
+
     /* Unregister the callback if necessary. */
     if (ctx->collection_changed_callback) {
       audiounit_remove_device_listener(ctx);
     }
   }
 
   delete ctx;
 }
@@ -1945,18 +1967,18 @@ audiounit_init_input_linear_buffer(cubeb
 
   return CUBEB_OK;
 }
 
 static uint32_t
 audiounit_clamp_latency(cubeb_stream * stm, uint32_t latency_frames)
 {
   // For the 1st stream set anything within safe min-max
-  assert(stm->context->active_streams > 0);
-  if (stm->context->active_streams == 1) {
+  assert(audiounit_active_streams(stm->context) > 0);
+  if (audiounit_active_streams(stm->context) == 1) {
     return max(min<uint32_t>(latency_frames, SAFE_MAX_LATENCY_FRAMES),
                     SAFE_MIN_LATENCY_FRAMES);
   }
   assert(stm->output_unit);
 
   // If more than one stream operates in parallel
   // allow only lower values of latency
   int r;
@@ -2405,26 +2427,26 @@ audiounit_setup_stream(cubeb_stream * st
     if (r != CUBEB_OK) {
       LOG("(%p) AudioUnit creation for output failed.", stm);
       return r;
     }
   }
 
   /* Latency cannot change if another stream is operating in parallel. In this case
   * latecy is set to the other stream value. */
-  if (stm->context->active_streams > 1) {
+  if (audiounit_active_streams(stm->context) > 1) {
     LOG("(%p) More than one active stream, use global latency.", stm);
     stm->latency_frames = stm->context->global_latency_frames;
   } else {
     /* Silently clamp the latency down to the platform default, because we
     * synthetize the clock from the callbacks, and we want the clock to update
     * often. */
     stm->latency_frames = audiounit_clamp_latency(stm, stm->latency_frames);
     assert(stm->latency_frames); // Ungly error check
-    audiounit_set_global_latency(stm, stm->latency_frames);
+    audiounit_set_global_latency(stm->context, stm->latency_frames);
   }
 
   /* Configure I/O stream */
   if (has_input(stm)) {
     r = audiounit_configure_input(stm);
     if (r != CUBEB_OK) {
       LOG("(%p) Configure audiounit input failed.", stm);
       return r;
@@ -2571,22 +2593,22 @@ audiounit_stream_init(cubeb * context,
                       cubeb_stream_params * input_stream_params,
                       cubeb_devid output_device,
                       cubeb_stream_params * output_stream_params,
                       unsigned int latency_frames,
                       cubeb_data_callback data_callback,
                       cubeb_state_callback state_callback,
                       void * user_ptr)
 {
+  assert(context);
+  auto_lock context_lock(context->mutex);
+  audiounit_increment_active_streams(context);
   unique_ptr<cubeb_stream, decltype(&audiounit_stream_destroy)> stm(new cubeb_stream(context),
                                                                     audiounit_stream_destroy);
-  context->active_streams += 1;
   int r;
-
-  assert(context);
   *stream = NULL;
   assert(latency_frames > 0);
   if ((input_device && !input_stream_params) ||
       (output_device && !output_stream_params)) {
     return CUBEB_ERROR_INVALID_PARAMETER;
   }
 
   /* These could be different in the future if we have both
@@ -2607,17 +2629,16 @@ audiounit_stream_init(cubeb * context,
     stm->output_stream_params = *output_stream_params;
     r = audiounit_set_device_info(stm.get(), reinterpret_cast<uintptr_t>(output_device), OUTPUT);
     if (r != CUBEB_OK) {
       LOG("(%p) Fail to set device info for output.", stm.get());
       return r;
     }
   }
 
-  auto_lock context_lock(context->mutex);
   {
     // It's not critical to lock here, because no other thread has been started
     // yet, but it allows to assert that the lock has been taken in
     // `audiounit_setup_stream`.
     auto_lock lock(stm->mutex);
     r = audiounit_setup_stream(stm.get());
   }
 
@@ -2685,18 +2706,19 @@ audiounit_stream_destroy(cubeb_stream * 
     audiounit_stream_stop_internal(stm);
   }
 
   // Execute close in serial queue to avoid collision
   // with reinit when un/plug devices
   dispatch_sync(stm->context->serial_queue, ^() {
     auto_lock lock(stm->mutex);
     audiounit_close_stream(stm);
-    assert(stm->context->active_streams >= 1);
-    stm->context->active_streams -= 1;
+    auto_lock context_lock(stm->context->mutex);
+    assert(audiounit_active_streams(stm->context) >= 1);
+    audiounit_decrement_active_streams(stm->context);
   });
 
   LOG("Cubeb stream (%p) destroyed successful.", stm);
   delete stm;
 }
 
 void
 audiounit_stream_start_internal(cubeb_stream * stm)

