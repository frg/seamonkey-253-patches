# HG changeset patch
# User Thomas P. <pdknsk+mozilla@gmail.com>
# Date 1537478498 0
# Node ID 5ab8b903147a0cc97b21d278299840b9e38aa1f6
# Parent  c304fc3634f88a96e2dd3d19ccd414d9d3d9a947
Bug 1483780: enable sanitizer-less libfuzzer builds r=froydnj

diff --git a/build/autoconf/sanitize.m4 b/build/autoconf/sanitize.m4
--- a/build/autoconf/sanitize.m4
+++ b/build/autoconf/sanitize.m4
@@ -111,16 +111,23 @@ if test -n "$MOZ_SIGNED_OVERFLOW_SANITIZ
     CXXFLAGS="$SANITIZER_BLACKLISTS $CXXFLAGS"
     AC_DEFINE(MOZ_UBSAN)
     MOZ_PATH_PROG(LLVM_SYMBOLIZER, llvm-symbolizer)
 fi
 AC_SUBST(MOZ_SIGNED_OVERFLOW_SANITIZE)
 AC_SUBST(MOZ_UNSIGNED_OVERFLOW_SANITIZE)
 AC_SUBST(MOZ_UBSAN)
 
+dnl =======================================================
+dnl = Required for stand-alone (sanitizer-less) libFuzzer.
+dnl =======================================================
+if test -n "$LIBFUZZER"; then
+   LDFLAGS="$LIBFUZZER_FLAGS -rdynamic $LDFLAGS"
+fi
+
 # The LLVM symbolizer is used by all sanitizers
 AC_SUBST(LLVM_SYMBOLIZER)
 
 dnl ========================================================
 dnl = Test for whether the compiler is compatible with the
 dnl = given sanitize options.
 dnl ========================================================
 AC_TRY_LINK(,,,AC_MSG_ERROR([compiler is incompatible with sanitize options]))
diff --git a/build/moz.configure/toolchain.configure b/build/moz.configure/toolchain.configure
--- a/build/moz.configure/toolchain.configure
+++ b/build/moz.configure/toolchain.configure
@@ -2267,22 +2267,33 @@ add_gcc_flag(
 add_gcc_flag(
     '-D_GLIBCXX_USE_CXX11_ABI=0', host_cxx_compiler,
     when=libstdcxx_version(
         'MOZ_LIBSTDCXX_HOST_VERSION', host_cxx_compiler, host))
 
 
 @depends(c_compiler.try_compile(flags=['-fsanitize=fuzzer-no-link'],
          check_msg='whether the C compiler supports -fsanitize=fuzzer-no-link'))
-def have_libfuzzer_flag_fuzzer_no_link(value):
+def libfuzzer_flags(value):
     if value:
-        return True
-
-
-set_config('HAVE_LIBFUZZER_FLAG_FUZZER_NO_LINK', have_libfuzzer_flag_fuzzer_no_link)
+        no_link_flag_supported = True
+        # recommended for (and only supported by) clang >= 6
+        use_flags = ['-fsanitize=fuzzer-no-link']
+    else:
+        no_link_flag_supported = False
+        use_flags = ['-fsanitize-coverage=trace-pc-guard,trace-cmp']
+
+    return namespace(
+        no_link_flag_supported=no_link_flag_supported,
+        use_flags=use_flags,
+    )
+
+set_config('HAVE_LIBFUZZER_FLAG_FUZZER_NO_LINK', libfuzzer_flags.no_link_flag_supported)
+set_config('LIBFUZZER_FLAGS', libfuzzer_flags.use_flags)
+add_old_configure_assignment('LIBFUZZER_FLAGS', libfuzzer_flags.use_flags)
 
 # Shared library building
 # ==============================================================
 
 # XXX: The use of makefile constructs in these variables is awful.
 @depends(target, c_compiler)
 def make_shared_library(target, compiler):
     if target.os == 'WINNT':
diff --git a/config/rules.mk.1483780-1.later b/config/rules.mk.1483780-1.later
new file mode 100644
--- /dev/null
+++ b/config/rules.mk.1483780-1.later
@@ -0,0 +1,65 @@
+--- rules.mk
++++ rules.mk
+@@ -907,16 +907,17 @@ endif
+ ifdef MOZ_USING_SCCACHE
+ sccache_wrap := RUSTC_WRAPPER='$(CCACHE)'
+ endif
+ 
+ ifndef MOZ_ASAN
+ ifndef MOZ_TSAN
+ ifndef MOZ_UBSAN
+ ifndef MOZ_CODE_COVERAGE
++ifndef FUZZING_INTERFACES
+ # Pass the compilers and flags in use to cargo for use in build scripts.
+ # * Don't do this for ASAN/TSAN builds because we don't pass our custom linker (see below)
+ #   which will muck things up.
+ # * Don't do this for code coverage builds because the way rustc invokes the linker doesn't
+ #   work with GCC 6: https://bugzilla.mozilla.org/show_bug.cgi?id=1477305
+ #
+ # We don't pass HOST_{CC,CXX} down in any form because our host value might not match
+ # what cargo chooses and there's no way to control cargo's selection, so we just have to
+@@ -938,16 +939,17 @@ else
+ cargo_c_compiler_envs := \
+  CC_$(rust_cc_env_name)="$(CC)" \
+  CXX_$(rust_cc_env_name)="$(CXX)" \
+  CFLAGS_$(rust_cc_env_name)="$(COMPUTED_CFLAGS)" \
+  CXXFLAGS_$(rust_cc_env_name)="$(COMPUTED_CXXFLAGS)" \
+  AR_$(rust_cc_env_name)="$(AR)" \
+  $(NULL)
+ endif # WINNT
++endif # FUZZING_INTERFACES
+ endif # MOZ_CODE_COVERAGE
+ endif # MOZ_UBSAN
+ endif # MOZ_TSAN
+ endif # MOZ_ASAN
+ 
+ # We use the + prefix to pass down the jobserver fds to cargo, but we
+ # don't use the prefix when make -n is used, so that cargo doesn't run
+ # in that case)
+@@ -1003,24 +1005,26 @@ cargo_linker_env_var := CARGO_TARGET_$(R
+ # have to pass in any special linker options on Windows.
+ ifneq (WINNT,$(OS_ARCH))
+ 
+ # Defining all of this for ASan/TSan builds results in crashes while running
+ # some crates's build scripts (!), so disable it for now.
+ ifndef MOZ_ASAN
+ ifndef MOZ_TSAN
+ ifndef MOZ_UBSAN
++ifndef FUZZING_INTERFACES
+ # Cargo needs the same linker flags as the C/C++ compiler,
+ # but not the final libraries. Filter those out because they
+ # cause problems on macOS 10.7; see bug 1365993 for details.
+ # Also, we don't want to pass PGO flags until cargo supports them.
+ target_cargo_env_vars := \
+ 	MOZ_CARGO_WRAP_LDFLAGS="$(filter-out -fsanitize=cfi% -framework Cocoa -lobjc AudioToolbox ExceptionHandling -fprofile-%,$(LDFLAGS))" \
+ 	MOZ_CARGO_WRAP_LD="$(CC)" \
+ 	$(cargo_linker_env_var)=$(topsrcdir)/build/cargo-linker
++endif # FUZZING_INTERFACES
+ endif # MOZ_UBSAN
+ endif # MOZ_TSAN
+ endif # MOZ_ASAN
+ 
+ endif # ifneq WINNT
+ 
+ ifdef RUST_LIBRARY_FILE
+ 
diff --git a/js/moz.configure b/js/moz.configure
--- a/js/moz.configure
+++ b/js/moz.configure
@@ -413,19 +413,21 @@ with only_when('--enable-compile-environ
         if fuzzing and (afl or libfuzzer):
             return True
 
     set_config('FUZZING', enable_fuzzing)
     set_define('FUZZING', enable_fuzzing)
 
     set_config('LIBFUZZER', enable_libfuzzer)
     set_define('LIBFUZZER', enable_libfuzzer)
+    add_old_configure_assignment('LIBFUZZER', enable_libfuzzer)
 
     set_config('FUZZING_INTERFACES', enable_fuzzing_interfaces)
     set_define('FUZZING_INTERFACES', enable_fuzzing_interfaces)
+    add_old_configure_assignment('FUZZING_INTERFACES', enable_fuzzing_interfaces)
 
 # Enable pipeline operator
 # ===================================================
 js_option('--enable-pipeline-operator', default=False, help='Enable pipeline operator')
 
 @depends('--enable-pipeline-operator')
 def enable_pipeline_operator(value):
     if value:
diff --git a/js/src/old-configure.in b/js/src/old-configure.in
--- a/js/src/old-configure.in
+++ b/js/src/old-configure.in
@@ -339,17 +339,17 @@ dnl ====================================
 dnl GNU specific defaults
 dnl ========================================================
 if test "$GNU_CC"; then
     DSO_LDOPTS='-shared'
     if test "$GCC_USE_GNU_LD"; then
         # Some tools like ASan use a runtime library that is only
         # linked against executables, so we must allow undefined
         # symbols for shared objects in some cases.
-        if test -z "$MOZ_ASAN$MOZ_MSAN$MOZ_UBSAN$MOZ_TSAN"; then
+        if test -z "$MOZ_ASAN$MOZ_MSAN$MOZ_UBSAN$MOZ_TSAN$FUZZING_INTERFACES"; then
             # Don't allow undefined symbols in libraries
             DSO_LDOPTS="$DSO_LDOPTS -Wl,-z,defs"
         fi
     fi
     WARNINGS_AS_ERRORS='-Werror'
     DSO_CFLAGS=''
 
     if test "$OS_ARCH" != "WINNT"; then
diff --git a/old-configure.in b/old-configure.in
--- a/old-configure.in
+++ b/old-configure.in
@@ -454,17 +454,17 @@ if test "$GNU_CC"; then
       ;;
     esac
 
     DSO_LDOPTS='-shared'
     if test "$GCC_USE_GNU_LD"; then
         # Some tools like ASan use a runtime library that is only
         # linked against executables, so we must allow undefined
         # symbols for shared objects in some cases.
-        if test -z "$MOZ_ASAN$MOZ_MSAN$MOZ_UBSAN$MOZ_TSAN"; then
+        if test -z "$MOZ_ASAN$MOZ_MSAN$MOZ_UBSAN$MOZ_TSAN$FUZZING_INTERFACES"; then
             # Don't allow undefined symbols in libraries
             DSO_LDOPTS="$DSO_LDOPTS -Wl,-z,defs"
 
             # BSDs need `environ' exposed for posix_spawn (bug 753046)
             case "$OS_TARGET" in
             DragonFly|FreeBSD|NetBSD|OpenBSD)
                 if test -n "$HAVE_LINKER_SUPPORT_IGNORE_UNRESOLVED"; then
                     DSO_LDOPTS="$DSO_LDOPTS -Wl,--ignore-unresolved-symbol,environ"
diff --git a/tools/fuzzing/libfuzzer-flags.mozbuild b/tools/fuzzing/libfuzzer-flags.mozbuild
--- a/tools/fuzzing/libfuzzer-flags.mozbuild
+++ b/tools/fuzzing/libfuzzer-flags.mozbuild
@@ -1,13 +1,8 @@
 # -*- Mode: python; c-basic-offset: 4; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 libfuzzer_flags = []
-
-if CONFIG['HAVE_LIBFUZZER_FLAG_FUZZER_NO_LINK']:
-    libfuzzer_flags += ['-fsanitize=fuzzer-no-link']
-else:
-    # Older Clang versions (<= 5) only support an older set of libFuzzer flags
-    libfuzzer_flags += ['-fsanitize-coverage=trace-pc-guard,trace-cmp']
+libfuzzer_flags += CONFIG['LIBFUZZER_FLAGS']
