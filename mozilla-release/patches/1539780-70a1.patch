# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1565379799 0
# Node ID 5967a3947ad0eeeecd97099e50655765259db28a
# Parent  bc814091c9f1d2e322a01efe9a590e9774334faa
Bug 1539780: Remove "--with-intl-api=build" build config option. r=jwalden

There are about the same number of occurrences of "ENABLE_INTL_API" and "EXPOSE_INTL_API"
in the tree, so preferring one over the other doesn't lead to fewer changes. Therefore
I went with "ENABLE_INTL_API", because "ENABLE_" (resp. "MOZ_ENABLE") is already used as
the prefix for other preprocessor ifdef's.

Differential Revision: https://phabricator.services.mozilla.com/D41188

diff --git a/build/autoconf/icu.m4 b/build/autoconf/icu.m4
--- a/build/autoconf/icu.m4
+++ b/build/autoconf/icu.m4
@@ -18,48 +18,38 @@ if test -n "$MOZ_SYSTEM_ICU"; then
     CFLAGS="$CFLAGS $MOZ_ICU_CFLAGS"
     CXXFLAGS="$CXXFLAGS $MOZ_ICU_CFLAGS"
     AC_DEFINE(MOZ_SYSTEM_ICU)
 fi
 
 AC_SUBST(MOZ_SYSTEM_ICU)
 
 MOZ_ARG_WITH_STRING(intl-api,
-[  --with-intl-api, --with-intl-api=build, --without-intl-api
+[  --with-intl-api, --without-intl-api
     Determine the status of the ECMAScript Internationalization API.  The first
-    (or lack of any of these) builds and exposes the API.  The second builds it
-    but doesn't use ICU or expose the API to script.  The third doesn't build
-    ICU at all.],
+    (or lack of any of these) builds and exposes the API.  The second doesn't
+    build ICU at all.],
     _INTL_API=$withval)
 
 ENABLE_INTL_API=
-EXPOSE_INTL_API=
 case "$_INTL_API" in
 no)
     ;;
-build)
-    ENABLE_INTL_API=1
-    ;;
 yes)
     ENABLE_INTL_API=1
-    EXPOSE_INTL_API=1
     ;;
 *)
     AC_MSG_ERROR([Invalid value passed to --with-intl-api: $_INTL_API])
     ;;
 esac
 
 if test -n "$ENABLE_INTL_API"; then
     USE_ICU=1
 fi
 
-if test -n "$EXPOSE_INTL_API"; then
-    AC_DEFINE(EXPOSE_INTL_API)
-fi
-
 if test -n "$ENABLE_INTL_API"; then
     AC_DEFINE(ENABLE_INTL_API)
 fi
 
 dnl Settings for the implementation of the ECMAScript Internationalization API
 if test -n "$USE_ICU"; then
     icudir="$_topsrcdir/intl/icu/source"
     if test ! -d "$icudir"; then
diff --git a/js/src/builtin/Array.js b/js/src/builtin/Array.js
--- a/js/src/builtin/Array.js
+++ b/js/src/builtin/Array.js
@@ -921,17 +921,17 @@ function ArrayToLocaleString(locales, op
     // Step 5.
     var firstElement = array[0];
 
     // Steps 6-7.
     var R;
     if (firstElement === undefined || firstElement === null) {
         R = "";
     } else {
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
         R = ToString(callContentFunction(firstElement.toLocaleString, firstElement, locales, options));
 #else
         R = ToString(callContentFunction(firstElement.toLocaleString, firstElement));
 #endif
     }
 
     // Step 3 (reordered).
     // We don't (yet?) implement locale-dependent separators.
@@ -940,17 +940,17 @@ function ArrayToLocaleString(locales, op
     // Steps 8-9.
     for (var k = 1; k < len; k++) {
         // Step 9.b.
         var nextElement = array[k];
 
         // Steps 9.a, 9.c-e.
         R += separator;
         if (!(nextElement === undefined || nextElement === null)) {
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
             R += ToString(callContentFunction(nextElement.toLocaleString, nextElement, locales, options));
 #else
             R += ToString(callContentFunction(nextElement.toLocaleString, nextElement));
 #endif
         }
     }
 
     // Step 10.
diff --git a/js/src/builtin/String.js b/js/src/builtin/String.js
--- a/js/src/builtin/String.js
+++ b/js/src/builtin/String.js
@@ -853,17 +853,17 @@ function String_static_raw(callSite/*, .
  */
 function String_static_localeCompare(str1, str2) {
     WarnDeprecatedStringMethod(STRING_GENERICS_LOCALE_COMPARE, "localeCompare");
     if (arguments.length < 1)
         ThrowTypeError(JSMSG_MISSING_FUN_ARG, 0, "String.localeCompare");
     var locales = arguments.length > 2 ? arguments[2] : undefined;
     var options = arguments.length > 3 ? arguments[3] : undefined;
 /* eslint-disable no-unreachable */
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     return callFunction(String_localeCompare, str1, str2, locales, options);
 #else
     return callFunction(std_String_localeCompare, str1, str2, locales, options);
 #endif
 /* eslint-enable no-unreachable */
 }
 
 // ES6 draft 2014-04-27 B.2.3.3
@@ -1055,40 +1055,40 @@ function String_static_trimRight(string)
     return callFunction(std_String_trimEnd, string);
 }
 
 function String_static_toLocaleLowerCase(string) {
     WarnDeprecatedStringMethod(STRING_GENERICS_TO_LOCALE_LOWER_CASE, "toLocaleLowerCase");
     if (arguments.length < 1)
         ThrowTypeError(JSMSG_MISSING_FUN_ARG, 0, "String.toLocaleLowerCase");
 /* eslint-disable no-unreachable */
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     var locales = arguments.length > 1 ? arguments[1] : undefined;
     return callFunction(String_toLocaleLowerCase, string, locales);
 #else
     return callFunction(std_String_toLocaleLowerCase, string);
 #endif
 /* eslint-enable no-unreachable */
 }
 
 function String_static_toLocaleUpperCase(string) {
     WarnDeprecatedStringMethod(STRING_GENERICS_TO_LOCALE_UPPER_CASE, "toLocaleUpperCase");
     if (arguments.length < 1)
         ThrowTypeError(JSMSG_MISSING_FUN_ARG, 0, "String.toLocaleUpperCase");
 /* eslint-disable no-unreachable */
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     var locales = arguments.length > 1 ? arguments[1] : undefined;
     return callFunction(String_toLocaleUpperCase, string, locales);
 #else
     return callFunction(std_String_toLocaleUpperCase, string);
 #endif
 /* eslint-enable no-unreachable */
 }
 
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
 function String_static_normalize(string) {
     WarnDeprecatedStringMethod(STRING_GENERICS_NORMALIZE, "normalize");
     if (arguments.length < 1)
         ThrowTypeError(JSMSG_MISSING_FUN_ARG, 0, "String.normalize");
     var form = arguments.length > 1 ? arguments[1] : undefined;
     return callFunction(std_String_normalize, string, form);
 }
 #endif
diff --git a/js/src/builtin/TestingFunctions.cpp b/js/src/builtin/TestingFunctions.cpp
--- a/js/src/builtin/TestingFunctions.cpp
+++ b/js/src/builtin/TestingFunctions.cpp
@@ -247,17 +247,17 @@ GetBuildConfiguration(JSContext* cx, uns
 #ifdef ENABLE_BINARYDATA
     value = BooleanValue(true);
 #else
     value = BooleanValue(false);
 #endif
     if (!JS_SetProperty(cx, info, "binary-data", value))
         return false;
 
-#ifdef EXPOSE_INTL_API
+#ifdef ENABLE_INTL_API
     value = BooleanValue(true);
 #else
     value = BooleanValue(false);
 #endif
     if (!JS_SetProperty(cx, info, "intl-api", value))
         return false;
 
 #if defined(SOLARIS)
diff --git a/js/src/builtin/TypedArray.js b/js/src/builtin/TypedArray.js
--- a/js/src/builtin/TypedArray.js
+++ b/js/src/builtin/TypedArray.js
@@ -1264,17 +1264,17 @@ function TypedArrayToLocaleString(locale
         return "";
 
     // Step 5.
     var firstElement = array[0];
 
     // Steps 6-7.
     // Omit the 'if' clause in step 6, since typed arrays can't have undefined
     // or null elements.
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     var R = ToString(callContentFunction(firstElement.toLocaleString, firstElement, locales, options));
 #else
     var R = ToString(callContentFunction(firstElement.toLocaleString, firstElement));
 #endif
 
     // Step 3 (reordered).
     // We don't (yet?) implement locale-dependent separators.
     var separator = ",";
@@ -1290,17 +1290,17 @@ function TypedArrayToLocaleString(locale
         // Step 9.c *should* be unreachable: typed array elements are numbers.
         // But bug 1079853 means |nextElement| *could* be |undefined|, if the
         // previous iteration's step 9.d or step 7 detached |array|'s buffer.
         // Conveniently, if this happens, evaluating |nextElement.toLocaleString|
         // throws the required TypeError, and the only observable difference is
         // the error message. So despite bug 1079853, we can skip step 9.c.
 
         // Step 9.d.
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
         R = ToString(callContentFunction(nextElement.toLocaleString, nextElement, locales, options));
 #else
         R = ToString(callContentFunction(nextElement.toLocaleString, nextElement));
 #endif
 
         // Step 9.e.
         R = S + R;
     }
diff --git a/js/src/jsapi.h b/js/src/jsapi.h
--- a/js/src/jsapi.h
+++ b/js/src/jsapi.h
@@ -5179,19 +5179,19 @@ JS_GetDefaultLocale(JSContext* cx);
  */
 extern JS_PUBLIC_API(void)
 JS_ResetDefaultLocale(JSRuntime* rt);
 
 /**
  * Locale specific string conversion and error message callbacks.
  */
 struct JSLocaleCallbacks {
-    JSLocaleToUpperCase     localeToUpperCase; // not used #if EXPOSE_INTL_API
-    JSLocaleToLowerCase     localeToLowerCase; // not used #if EXPOSE_INTL_API
-    JSLocaleCompare         localeCompare; // not used #if EXPOSE_INTL_API
+    JSLocaleToUpperCase     localeToUpperCase; // not used #if ENABLE_INTL_API
+    JSLocaleToLowerCase     localeToLowerCase; // not used #if ENABLE_INTL_API
+    JSLocaleCompare         localeCompare; // not used #if ENABLE_INTL_API
     JSLocaleToUnicode       localeToUnicode;
 };
 
 /**
  * Establish locale callbacks. The pointer must persist as long as the
  * JSContext.  Passing nullptr restores the default behaviour.
  */
 extern JS_PUBLIC_API(void)
diff --git a/js/src/jsdate.cpp b/js/src/jsdate.cpp
--- a/js/src/jsdate.cpp
+++ b/js/src/jsdate.cpp
@@ -2880,17 +2880,17 @@ FormatDate(JSContext* cx, double utcTime
         if (!str)
             return false;
     }
 
     rval.setString(str);
     return true;
 }
 
-#if !EXPOSE_INTL_API
+#if !ENABLE_INTL_API
 static bool
 ToLocaleFormatHelper(JSContext* cx, HandleObject obj, const char* format, MutableHandleValue rval)
 {
     double utcTime = obj->as<DateObject>().UTCTime().toNumber();
 
     char buf[100];
     if (!IsFinite(utcTime)) {
         strcpy(buf, js_InvalidDate_str);
@@ -2994,17 +2994,17 @@ date_toLocaleTimeString_impl(JSContext* 
 }
 
 static bool
 date_toLocaleTimeString(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     return CallNonGenericMethod<IsDate, date_toLocaleTimeString_impl>(cx, args);
 }
-#endif /* !EXPOSE_INTL_API */
+#endif /* !ENABLE_INTL_API */
 
 /* ES5 15.9.5.4. */
 MOZ_ALWAYS_INLINE bool
 date_toTimeString_impl(JSContext* cx, const CallArgs& args)
 {
     return FormatDate(cx, args.thisv().toObject().as<DateObject>().UTCTime().toNumber(),
                       FormatSpec::Time, args.rval());
 }
@@ -3150,17 +3150,17 @@ static const JSFunctionSpec date_methods
     JS_FN("setUTCHours",         date_setUTCHours,        4,0),
     JS_FN("setMinutes",          date_setMinutes,         3,0),
     JS_FN("setUTCMinutes",       date_setUTCMinutes,      3,0),
     JS_FN("setSeconds",          date_setSeconds,         2,0),
     JS_FN("setUTCSeconds",       date_setUTCSeconds,      2,0),
     JS_FN("setMilliseconds",     date_setMilliseconds,    1,0),
     JS_FN("setUTCMilliseconds",  date_setUTCMilliseconds, 1,0),
     JS_FN("toUTCString",         date_toGMTString,        0,0),
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     JS_SELF_HOSTED_FN(js_toLocaleString_str, "Date_toLocaleString", 0,0),
     JS_SELF_HOSTED_FN("toLocaleDateString", "Date_toLocaleDateString", 0,0),
     JS_SELF_HOSTED_FN("toLocaleTimeString", "Date_toLocaleTimeString", 0,0),
 #else
     JS_FN(js_toLocaleString_str, date_toLocaleString,     0,0),
     JS_FN("toLocaleDateString",  date_toLocaleDateString, 0,0),
     JS_FN("toLocaleTimeString",  date_toLocaleTimeString, 0,0),
 #endif
diff --git a/js/src/jsnum.cpp b/js/src/jsnum.cpp
--- a/js/src/jsnum.cpp
+++ b/js/src/jsnum.cpp
@@ -732,17 +732,17 @@ num_toString_impl(JSContext* cx, const C
 
 bool
 js::num_toString(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     return CallNonGenericMethod<IsNumber, num_toString_impl>(cx, args);
 }
 
-#if !EXPOSE_INTL_API
+#if !ENABLE_INTL_API
 MOZ_ALWAYS_INLINE bool
 num_toLocaleString_impl(JSContext* cx, const CallArgs& args)
 {
     MOZ_ASSERT(IsNumber(args.thisv()));
 
     double d = Extract(args.thisv());
 
     RootedString str(cx, NumberToStringWithBase<CanGC>(cx, d, 10));
@@ -865,17 +865,17 @@ num_toLocaleString_impl(JSContext* cx, c
 }
 
 static bool
 num_toLocaleString(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     return CallNonGenericMethod<IsNumber, num_toLocaleString_impl>(cx, args);
 }
-#endif /* !EXPOSE_INTL_API */
+#endif /* !ENABLE_INTL_API */
 
 MOZ_ALWAYS_INLINE bool
 num_valueOf_impl(JSContext* cx, const CallArgs& args)
 {
     MOZ_ASSERT(IsNumber(args.thisv()));
     args.rval().setNumber(Extract(args.thisv()));
     return true;
 }
@@ -1084,17 +1084,17 @@ num_toPrecision(JSContext* cx, unsigned 
 {
     CallArgs args = CallArgsFromVp(argc, vp);
     return CallNonGenericMethod<IsNumber, num_toPrecision_impl>(cx, args);
 }
 
 static const JSFunctionSpec number_methods[] = {
     JS_FN(js_toSource_str,       num_toSource,          0, 0),
     JS_FN(js_toString_str,       num_toString,          1, 0),
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     JS_SELF_HOSTED_FN(js_toLocaleString_str, "Number_toLocaleString", 0,0),
 #else
     JS_FN(js_toLocaleString_str, num_toLocaleString,     0,0),
 #endif
     JS_FN(js_valueOf_str,        num_valueOf,           0, 0),
     JS_FN("toFixed",             num_toFixed,           1, 0),
     JS_FN("toExponential",       num_toExponential,     1, 0),
     JS_FN("toPrecision",         num_toPrecision,       1, 0),
@@ -1115,20 +1115,20 @@ static const JSFunctionSpec number_stati
     JS_SELF_HOSTED_FN("isSafeInteger", "Number_isSafeInteger", 1,0),
     JS_FS_END
 };
 
 
 bool
 js::InitRuntimeNumberState(JSRuntime* rt)
 {
-    // XXX If EXPOSE_INTL_API becomes true all the time at some point,
+    // XXX If ENABLE_INTL_API becomes true all the time at some point,
     //     js::InitRuntimeNumberState is no longer fallible, and we should
     //     change its return type.
-#if !EXPOSE_INTL_API
+#if !ENABLE_INTL_API
     /* Copy locale-specific separators into the runtime strings. */
     const char* thousandsSeparator;
     const char* decimalPoint;
     const char* grouping;
 #ifdef HAVE_LOCALECONV
     struct lconv* locale = localeconv();
     thousandsSeparator = locale->thousands_sep;
     decimalPoint = locale->decimal_point;
@@ -1164,21 +1164,21 @@ js::InitRuntimeNumberState(JSRuntime* rt
     storage += thousandsSeparatorSize;
 
     js_memcpy(storage, decimalPoint, decimalPointSize);
     rt->decimalSeparator = storage;
     storage += decimalPointSize;
 
     js_memcpy(storage, grouping, groupingSize);
     rt->numGrouping = grouping;
-#endif /* !EXPOSE_INTL_API */
+#endif /* !ENABLE_INTL_API */
     return true;
 }
 
-#if !EXPOSE_INTL_API
+#if !ENABLE_INTL_API
 void
 js::FinishRuntimeNumberState(JSRuntime* rt)
 {
     /*
      * The free also releases the memory for decimalSeparator and numGrouping
      * strings.
      */
     char* storage = const_cast<char*>(rt->thousandsSeparator.ref());
diff --git a/js/src/jsnum.h b/js/src/jsnum.h
--- a/js/src/jsnum.h
+++ b/js/src/jsnum.h
@@ -34,17 +34,17 @@
 
 namespace js {
 
 class StringBuffer;
 
 extern MOZ_MUST_USE bool
 InitRuntimeNumberState(JSRuntime* rt);
 
-#if !EXPOSE_INTL_API
+#if !ENABLE_INTL_API
 extern void
 FinishRuntimeNumberState(JSRuntime* rt);
 #endif
 
 /* Initialize the Number class, returning its prototype object. */
 extern JSObject*
 InitNumberClass(JSContext* cx, HandleObject obj);
 
diff --git a/js/src/jsprototypes.h b/js/src/jsprototypes.h
--- a/js/src/jsprototypes.h
+++ b/js/src/jsprototypes.h
@@ -31,17 +31,17 @@
  * JS_FOR_EACH_PROTOTYPE does.
  */
 
 #define CLASP(name)                 (&name##Class)
 #define OCLASP(name)                (&name##Object::class_)
 #define TYPED_ARRAY_CLASP(type)     (&TypedArrayObject::classes[Scalar::type])
 #define ERROR_CLASP(type)           (&ErrorObject::classes[type])
 
-#ifdef EXPOSE_INTL_API
+#ifdef ENABLE_INTL_API
 #define IF_INTL(real,imaginary) real
 #else
 #define IF_INTL(real,imaginary) imaginary
 #endif
 
 #ifdef ENABLE_BINARYDATA
 #define IF_BDATA(real,imaginary) real
 #else
diff --git a/js/src/jsstr.cpp b/js/src/jsstr.cpp
--- a/js/src/jsstr.cpp
+++ b/js/src/jsstr.cpp
@@ -1073,17 +1073,17 @@ js::intl_toLocaleLowerCase(JSContext* cx
         });
     if (!str)
         return false;
 
     args.rval().setString(str);
     return true;
 }
 
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
 
 // String.prototype.toLocaleLowerCase is self-hosted when Intl is exposed,
 // with core functionality performed by the intrinsic above.
 
 #else
 
 bool
 js::str_toLocaleLowerCase(JSContext* cx, unsigned argc, Value* vp)
@@ -1114,17 +1114,17 @@ js::str_toLocaleLowerCase(JSContext* cx,
     JSString* result = StringToLowerCase(cx, linear);
     if (!result)
         return false;
 
     args.rval().setString(result);
     return true;
 }
 
-#endif // EXPOSE_INTL_API
+#endif // ENABLE_INTL_API
 
 static inline bool
 ToUpperCaseHasSpecialCasing(Latin1Char charCode)
 {
     // U+00DF LATIN SMALL LETTER SHARP S is the only Latin-1 code point with
     // special casing rules, so detect it inline.
     bool hasUpperCaseSpecialCasing = charCode == unicode::LATIN_SMALL_LETTER_SHARP_S;
     MOZ_ASSERT(hasUpperCaseSpecialCasing == unicode::ChangesWhenUpperCasedSpecialCasing(charCode));
@@ -1461,17 +1461,17 @@ js::intl_toLocaleUpperCase(JSContext* cx
         });
     if (!str)
         return false;
 
     args.rval().setString(str);
     return true;
 }
 
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
 
 // String.prototype.toLocaleLowerCase is self-hosted when Intl is exposed,
 // with core functionality performed by the intrinsic above.
 
 #else
 
 bool
 js::str_toLocaleUpperCase(JSContext* cx, unsigned argc, Value* vp)
@@ -1502,19 +1502,19 @@ js::str_toLocaleUpperCase(JSContext* cx,
     JSString* result = StringToUpperCase(cx, linear);
     if (!result)
         return false;
 
     args.rval().setString(result);
     return true;
 }
 
-#endif // EXPOSE_INTL_API
-
-#if EXPOSE_INTL_API
+#endif // ENABLE_INTL_API
+
+#if ENABLE_INTL_API
 
 // String.prototype.localeCompare is self-hosted when Intl is exposed.
 
 #else
 
 bool
 js::str_localeCompare(JSContext* cx, unsigned argc, Value* vp)
 {
@@ -1539,19 +1539,19 @@ js::str_localeCompare(JSContext* cx, uns
     int32_t result;
     if (!CompareStrings(cx, str, thatStr, &result))
         return false;
 
     args.rval().setInt32(result);
     return true;
 }
 
-#endif // EXPOSE_INTL_API
-
-#if EXPOSE_INTL_API
+#endif // ENABLE_INTL_API
+
+#if ENABLE_INTL_API
 
 // ES2017 draft rev 45e890512fd77add72cc0ee742785f9f6f6482de
 // 21.1.3.12 String.prototype.normalize ( [ form ] )
 bool
 js::str_normalize(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
@@ -1677,17 +1677,17 @@ js::str_normalize(JSContext* cx, unsigne
     if (!ns)
         return false;
 
     // Step 7.
     args.rval().setString(ns);
     return true;
 }
 
-#endif // EXPOSE_INTL_API
+#endif // ENABLE_INTL_API
 
 bool
 js::str_charAt(JSContext* cx, unsigned argc, Value* vp)
 {
     CallArgs args = CallArgsFromVp(argc, vp);
 
     RootedString str(cx);
     size_t i;
@@ -3564,27 +3564,27 @@ static const JSFunctionSpec string_metho
     JS_FN("includes",          str_includes,          1,0),
     JS_FN("indexOf",           str_indexOf,           1,0),
     JS_FN("lastIndexOf",       str_lastIndexOf,       1,0),
     JS_FN("startsWith",        str_startsWith,        1,0),
     JS_FN("endsWith",          str_endsWith,          1,0),
     JS_FN("trim",              str_trim,              0,0),
     JS_FN("trimStart",         str_trimStart,         0,0),
     JS_FN("trimEnd",           str_trimEnd,           0,0),
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     JS_SELF_HOSTED_FN("toLocaleLowerCase", "String_toLocaleLowerCase", 0,0),
     JS_SELF_HOSTED_FN("toLocaleUpperCase", "String_toLocaleUpperCase", 0,0),
     JS_SELF_HOSTED_FN("localeCompare", "String_localeCompare", 1,0),
 #else
     JS_FN("toLocaleLowerCase", str_toLocaleLowerCase, 0,0),
     JS_FN("toLocaleUpperCase", str_toLocaleUpperCase, 0,0),
     JS_FN("localeCompare",     str_localeCompare,     1,0),
 #endif
     JS_SELF_HOSTED_FN("repeat", "String_repeat",      1,0),
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     JS_FN("normalize",         str_normalize,         0,0),
 #endif
 
     /* Perl-ish methods (search is actually Python-esque). */
     JS_SELF_HOSTED_FN("match", "String_match",        1,0),
     JS_SELF_HOSTED_FN("search", "String_search",      1,0),
     JS_SELF_HOSTED_FN("replace", "String_replace",    2,0),
     JS_SELF_HOSTED_FN("replaceAll", "String_replaceAll", 2, 0),
@@ -3874,17 +3874,17 @@ static const JSFunctionSpec string_stati
     JS_SELF_HOSTED_FN("lastIndexOf",     "String_static_lastIndexOf",   2,0),
     JS_SELF_HOSTED_FN("startsWith",      "String_static_startsWith",    2,0),
     JS_SELF_HOSTED_FN("endsWith",        "String_static_endsWith",      2,0),
     JS_SELF_HOSTED_FN("trim",            "String_static_trim",          1,0),
     JS_SELF_HOSTED_FN("trimLeft",        "String_static_trimLeft",      1,0),
     JS_SELF_HOSTED_FN("trimRight",       "String_static_trimRight",     1,0),
     JS_SELF_HOSTED_FN("toLocaleLowerCase","String_static_toLocaleLowerCase",1,0),
     JS_SELF_HOSTED_FN("toLocaleUpperCase","String_static_toLocaleUpperCase",1,0),
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     JS_SELF_HOSTED_FN("normalize",       "String_static_normalize",     1,0),
 #endif
     JS_SELF_HOSTED_FN("concat",          "String_static_concat",        2,0),
 
     JS_SELF_HOSTED_FN("localeCompare",   "String_static_localeCompare", 2,0),
     JS_FS_END
 };
 
diff --git a/js/src/jsstr.h b/js/src/jsstr.h
--- a/js/src/jsstr.h
+++ b/js/src/jsstr.h
@@ -345,78 +345,78 @@ str_trimStart(JSContext* cx, unsigned ar
 
 extern bool
 str_trimEnd(JSContext* cx, unsigned argc, Value* vp);
 
 /**
  * Returns the input string converted to lower case based on the language
  * specific case mappings for the input locale.
  *
- * This function only works #if EXPOSE_INTL_API; if not, it will *crash*.
+ * This function only works #if ENABLE_INTL_API; if not, it will *crash*.
  * Govern yourself accordingly.
  *
  * Usage: lowerCase = intl_toLocaleLowerCase(string, locale)
  */
 extern MOZ_MUST_USE bool
 intl_toLocaleLowerCase(JSContext* cx, unsigned argc, Value* vp);
 
 /**
  * Returns the input string converted to upper case based on the language
  * specific case mappings for the input locale.
  *
- * This function only works #if EXPOSE_INTL_API; if not, it will *crash*.
+ * This function only works #if ENABLE_INTL_API; if not, it will *crash*.
  * Govern yourself accordingly.
  *
  * Usage: upperCase = intl_toLocaleUpperCase(string, locale)
  */
 extern MOZ_MUST_USE bool
 intl_toLocaleUpperCase(JSContext* cx, unsigned argc, Value* vp);
 
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
 
 // When the Intl API is exposed, String.prototype.to{Lower,Upper}Case is
 // self-hosted.  The core functionality is provided by the intrinsics above.
 
 #else
 
 // When the Intl API is not exposed, String.prototype.to{Lower,Upper}Case are
 // implemented in C++.
 
 extern bool
 str_toLocaleLowerCase(JSContext* cx, unsigned argc, Value* vp);
 
 extern bool
 str_toLocaleUpperCase(JSContext* cx, unsigned argc, Value* vp);
 
-#endif // EXPOSE_INTL_API
+#endif // ENABLE_INTL_API
 
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
 
 // String.prototype.normalize is only implementable if ICU's normalization
 // functionality is available.
 extern bool
 str_normalize(JSContext* cx, unsigned argc, Value* vp);
 
-#endif // EXPOSE_INTL_API
+#endif // ENABLE_INTL_API
 
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
 
 // String.prototype.localeCompare is self-hosted when Intl functionality is
 // exposed, and the only intrinsics it requires are provided in the
 // implementation of Intl.Collator.
 
 #else
 
 // String.prototype.localeCompare is implemented in C++ (delegating to
 // JSLocaleCallbacks) when Intl functionality is not exposed.
 
 extern bool
 str_localeCompare(JSContext* cx, unsigned argc, Value* vp);
 
-#endif // EXPOSE_INTL_API
+#endif // ENABLE_INTL_API
 
 extern bool
 str_concat(JSContext* cx, unsigned argc, Value* vp);
 
 /*
  * Convert one UCS-4 char and write it into a UTF-8 buffer, which must be at
  * least 4 bytes long.  Return the number of UTF-8 bytes of data written.
  */
diff --git a/js/src/vm/Initialization.cpp b/js/src/vm/Initialization.cpp
--- a/js/src/vm/Initialization.cpp
+++ b/js/src/vm/Initialization.cpp
@@ -117,22 +117,22 @@ JS::detail::InitWithFailureDiagnostic(bo
     RETURN_IF_FAIL(js::jit::InitializeIon());
 
     RETURN_IF_FAIL(js::InitDateTimeState());
 
 #ifdef MOZ_VTUNE
     RETURN_IF_FAIL(js::vtune::Initialize());
 #endif
 
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     UErrorCode err = U_ZERO_ERROR;
     u_init(&err);
     if (U_FAILURE(err))
         return "u_init() failed";
-#endif // EXPOSE_INTL_API
+#endif // ENABLE_INTL_API
 
     RETURN_IF_FAIL(js::CreateHelperThreadsState());
     RETURN_IF_FAIL(FutexThread::initialize());
     RETURN_IF_FAIL(js::gcstats::Statistics::initialize());
 
 #ifdef JS_SIMULATOR
     RETURN_IF_FAIL(js::jit::SimulatorProcess::initialize());
 #endif
@@ -184,19 +184,19 @@ JS_ShutDown(void)
     // PRMJ_Now subsystem.  (For reinitialization to be permitted, we'd need to
     // "reset" the called-once status -- doable, but more trouble than it's
     // worth now.)  Initializing that subsystem from JS_Init eliminates the
     // problem, but initialization can take a comparatively long time (15ms or
     // so), so we really don't want to do it in JS_Init, and we really do want
     // to do it only when PRMJ_Now is eventually called.
     PRMJ_NowShutdown();
 
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     u_cleanup();
-#endif // EXPOSE_INTL_API
+#endif // ENABLE_INTL_API
 
 #ifdef MOZ_VTUNE
     js::vtune::Shutdown();
 #endif // MOZ_VTUNE
 
     js::FinishDateTimeState();
 
     if (!JSRuntime::hasLiveRuntimes()) {
@@ -212,16 +212,16 @@ JS_ShutDown(void)
 
 JS_PUBLIC_API(bool)
 JS_SetICUMemoryFunctions(JS_ICUAllocFn allocFn, JS_ICUReallocFn reallocFn, JS_ICUFreeFn freeFn)
 {
     MOZ_ASSERT(libraryInitState == InitState::Uninitialized,
                "must call JS_SetICUMemoryFunctions before any other JSAPI "
                "operation (including JS_Init)");
 
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     UErrorCode status = U_ZERO_ERROR;
     u_setMemoryFunctions(/* context = */ nullptr, allocFn, reallocFn, freeFn, &status);
     return U_SUCCESS(status);
 #else
     return true;
 #endif
 }
diff --git a/js/src/vm/Runtime.cpp b/js/src/vm/Runtime.cpp
--- a/js/src/vm/Runtime.cpp
+++ b/js/src/vm/Runtime.cpp
@@ -34,17 +34,17 @@
 #include "jit/arm/Simulator-arm.h"
 #include "jit/arm64/vixl/Simulator-vixl.h"
 #include "jit/JitCompartment.h"
 #include "jit/mips32/Simulator-mips32.h"
 #include "jit/mips64/Simulator-mips64.h"
 #include "js/Date.h"
 #include "js/MemoryMetrics.h"
 #include "js/SliceBudget.h"
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
 #  include "unicode/uloc.h"
 #endif
 #include "vm/DateTime.h"
 #include "vm/Debugger.h"
 #include "vm/JSAtom.h"
 #include "vm/JSObject.h"
 #include "vm/JSScript.h"
 #include "vm/TraceLogging.h"
@@ -151,17 +151,17 @@ JSRuntime::JSRuntime(JSRuntime* parentRu
     selfHostingGlobal_(nullptr),
     gc(thisFromCtor()),
     gcInitialized(false),
     NaNValue(DoubleNaNValue()),
     negativeInfinityValue(DoubleValue(NegativeInfinity<double>())),
     positiveInfinityValue(DoubleValue(PositiveInfinity<double>())),
     emptyString(nullptr),
     defaultFreeOp_(nullptr),
-#if !EXPOSE_INTL_API
+#if !ENABLE_INTL_API
     thousandsSeparator(nullptr),
     decimalSeparator(nullptr),
     numGrouping(nullptr),
 #endif
     beingDestroyed_(false),
     allowContentJS_(true),
     atoms_(nullptr),
     atomsAddedWhileSweeping_(nullptr),
@@ -326,17 +326,17 @@ JSRuntime::destroyRuntime()
     MOZ_ASSERT(!hasHelperThreadZones());
 
     /*
      * Even though all objects in the compartment are dead, we may have keep
      * some filenames around because of gcKeepAtoms.
      */
     FreeScriptData(this);
 
-#if !EXPOSE_INTL_API
+#if !ENABLE_INTL_API
     FinishRuntimeNumberState(this);
 #endif
 
     gc.finish();
     atomsCompartment_ = nullptr;
 
     js_delete(defaultFreeOp_.ref());
 
@@ -555,17 +555,17 @@ JSRuntime::resetDefaultLocale()
 const char*
 JSRuntime::getDefaultLocale()
 {
     if (defaultLocale)
         return defaultLocale;
 
     // Use ICU if available to retrieve the default locale, this ensures ICU's
     // default locale matches our default locale.
-#if EXPOSE_INTL_API
+#if ENABLE_INTL_API
     const char* locale = uloc_getDefault();
 #else
     const char* locale = setlocale(LC_ALL, nullptr);
 #endif
 
     // convert to a well-formed BCP 47 language tag
     if (!locale || !strcmp(locale, "C"))
         locale = "und";
diff --git a/js/src/vm/Runtime.h b/js/src/vm/Runtime.h
--- a/js/src/vm/Runtime.h
+++ b/js/src/vm/Runtime.h
@@ -663,17 +663,17 @@ struct JSRuntime : public js::MallocProv
     js::WriteOnceData<js::FreeOp*> defaultFreeOp_;
 
   public:
     js::FreeOp* defaultFreeOp() {
         MOZ_ASSERT(defaultFreeOp_);
         return defaultFreeOp_;
     }
 
-#if !EXPOSE_INTL_API
+#if !ENABLE_INTL_API
     /* Number localization, used by jsnum.cpp. */
     js::WriteOnceData<const char*> thousandsSeparator;
     js::WriteOnceData<const char*> decimalSeparator;
     js::WriteOnceData<const char*> numGrouping;
 #endif
 
   private:
     mozilla::Maybe<js::SharedImmutableStringsCache> sharedImmutableStrings_;
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -2246,17 +2246,17 @@ static const JSFunctionSpec intrinsic_fu
     JS_INLINABLE_FN("std_String_toLowerCase",    str_toLowerCase,              0,0, StringToLowerCase),
     JS_INLINABLE_FN("std_String_toUpperCase",    str_toUpperCase,              0,0, StringToUpperCase),
 
     JS_INLINABLE_FN("std_String_charAt",         str_charAt,                   1,0, StringCharAt),
     JS_FN("std_String_endsWith",                 str_endsWith,                 1,0),
     JS_FN("std_String_trim",                     str_trim,                     0,0),
     JS_FN("std_String_trimStart",                str_trimStart,                0,0),
     JS_FN("std_String_trimEnd",                  str_trimEnd,                  0,0),
-#if !EXPOSE_INTL_API
+#if !ENABLE_INTL_API
     JS_FN("std_String_toLocaleLowerCase",        str_toLocaleLowerCase,        0,0),
     JS_FN("std_String_toLocaleUpperCase",        str_toLocaleUpperCase,        0,0),
     JS_FN("std_String_localeCompare",            str_localeCompare,            1,0),
 #else
     JS_FN("std_String_normalize",                str_normalize,                0,0),
 #endif
     JS_FN("std_String_concat",                   str_concat,                   1,0),
 
diff --git a/js/xpconnect/src/XPCLocale.cpp b/js/xpconnect/src/XPCLocale.cpp
--- a/js/xpconnect/src/XPCLocale.cpp
+++ b/js/xpconnect/src/XPCLocale.cpp
@@ -68,17 +68,17 @@ XPCLocaleObserver::Observe(nsISupports* 
 struct XPCLocaleCallbacks : public JSLocaleCallbacks
 {
   XPCLocaleCallbacks()
   {
     MOZ_COUNT_CTOR(XPCLocaleCallbacks);
 
     // Disable the toLocaleUpper/Lower case hooks to use the standard,
     // locale-insensitive definition from String.prototype. (These hooks are
-    // only consulted when EXPOSE_INTL_API is not set.) Since EXPOSE_INTL_API
+    // only consulted when ENABLE_INTL_API is not set.) Since ENABLE_INTL_API
     // is always set, these hooks should be disabled.
     localeToUpperCase = nullptr;
     localeToLowerCase = nullptr;
     localeCompare = nullptr;
     localeToUnicode = nullptr;
 
     // It's going to be retained by the ObserverService.
     RefPtr<XPCLocaleObserver> locObs = new XPCLocaleObserver();
