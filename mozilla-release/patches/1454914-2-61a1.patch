# HG changeset patch
# User Christoph Kerschbaumer <ckerschb@christophkerschbaumer.com>
# Date 1524160852 -7200
# Node ID 5524e587eff281b0f397d17ccd38c8bb26fb221e
# Parent  d8febb60e58a3d4a543af5aae1cce999c14fcbba
Bug 1454914: Test web extensions are exempt from samesite cookie policy.r=jkt

diff --git a/toolkit/components/extensions/test/mochitest/file_same_site_cookies_webextension.sjs b/toolkit/components/extensions/test/mochitest/file_same_site_cookies_webextension.sjs
new file mode 100644
--- /dev/null
+++ b/toolkit/components/extensions/test/mochitest/file_same_site_cookies_webextension.sjs
@@ -0,0 +1,40 @@
+// Custom *.sjs file specifically for the needs of Bug 1454914
+
+const WIN = `<html><body>dummy page setting a same-site cookie</body></html>`;
+const FRAME = `<html><body>dummy frame getting a same-site cookie</body></html>`;
+
+// small red image
+const IMG_BYTES = atob(
+  "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12" +
+  "P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==");
+
+function handleRequest(request, response)
+{
+  // avoid confusing cache behaviors
+  response.setHeader("Cache-Control", "no-cache", false);
+
+  if (request.queryString === "loadWin") {
+    response.write(WIN);
+    return;
+  }
+
+  // using startsWith and discard the math random
+  if (request.queryString.startsWith("loadImage")) {
+    response.setHeader("Set-Cookie", "myKey=mySameSiteExtensionCookie; samesite=strict", true);
+    response.setHeader("Content-Type", "image/png");
+    response.write(IMG_BYTES);
+    return;
+  }
+
+  if (request.queryString === "loadXHR") {
+    let cookie = "noCookie";
+    if (request.hasHeader("Cookie")) {
+      cookie = request.getHeader("Cookie");
+    }
+    response.write(cookie);
+    return;
+  }
+
+  // we should never get here, but just in case return something unexpected
+  response.write("D'oh");
+}
diff --git a/toolkit/components/extensions/test/mochitest/mochitest-common.ini b/toolkit/components/extensions/test/mochitest/mochitest-common.ini
--- a/toolkit/components/extensions/test/mochitest/mochitest-common.ini
+++ b/toolkit/components/extensions/test/mochitest/mochitest-common.ini
@@ -49,16 +49,17 @@ support-files =
   oauth.html
   redirect_auto.sjs
   redirection.sjs
   return_headers.sjs
   slow_response.sjs
   webrequest_worker.js
   !/dom/tests/mochitest/geolocation/network_geolocation.sjs
   !/toolkit/components/passwordmgr/test/authenticate.sjs
+  file_same_site_cookies_webextension.sjs
 
 [test_ext_background_api_injection.html]
 [test_ext_background_generated_url.html]
 [test_ext_background_canvas.html]
 [test_ext_background_page.html]
 skip-if = (toolkit == 'android') # android doesn't have devtools
 [test_ext_background_teardown.html]
 [test_ext_clipboard.html]
@@ -146,9 +147,10 @@ skip-if = os == 'android'
 [test_ext_webrequest_filter.html]
 [test_ext_webrequest_frameId.html]
 [test_ext_webrequest_hsts.html]
 skip-if = os == 'android' || os == 'linux' # linux, bug 1398120
 [test_ext_webrequest_upgrade.html]
 [test_ext_webrequest_upload.html]
 skip-if = os == 'android' # Currently fails in emulator tests
 [test_ext_window_postMessage.html]
+[test_same_site_cookies_webextension.html]
 [test_ext_xhr_capabilities.html]
diff --git a/toolkit/components/extensions/test/mochitest/test_same_site_cookies_webextension.html b/toolkit/components/extensions/test/mochitest/test_same_site_cookies_webextension.html
new file mode 100644
--- /dev/null
+++ b/toolkit/components/extensions/test/mochitest/test_same_site_cookies_webextension.html
@@ -0,0 +1,88 @@
+<!DOCTYPE HTML>
+<html>
+<head>
+  <title>Bug 1454914: Exempt web-extensions from same-site cookie policy</title>
+  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <script type="text/javascript" src="/tests/SimpleTest/SpawnTask.js"></script>
+  <script type="text/javascript" src="/tests/SimpleTest/ExtensionTestUtils.js"></script>
+  <script type="text/javascript" src="head.js"></script>
+  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css"/>
+</head>
+<body>
+
+<script type="text/javascript">
+"use strict";
+
+/* Description of the test:
+ * (1) We load an image from mochi.test which sets a same site cookie
+ * (2) We have the web extension perform an XHR request to mochi.test
+ * (3) We verify the web-extension can access the same-site cookie
+ */
+
+add_task(async function test_webRequest_same_site_cookie_access() {
+  let extension = ExtensionTestUtils.loadExtension({
+    manifest: {
+      permissions: [
+        "http://example.com/*",
+      ],
+      content_scripts: [{
+        matches: ["http://example.com/*"],
+        run_at: "document_start",
+        js: ["content_script.js"],
+      }],
+    },
+
+    background() {
+      browser.test.onMessage.addListener(msg => {
+        if (msg === "verify-same-site-cookie-moz-extension") {
+          let xhr = new XMLHttpRequest();
+          try {
+            xhr.open("GET", "http://example.com/tests/toolkit/components/extensions/test/mochitest/file_same_site_cookies_webextension.sjs?loadXHR", true);
+            xhr.onload = function() {
+              browser.test.assertEq("myKey=mySameSiteExtensionCookie", xhr.responseText,
+                                    "cookie should be accessible from moz-extension context");
+              browser.test.sendMessage("same-site-cookie-test-done");
+            };
+            xhr.onerror = function() {
+              browser.test.fail("xhr onerror");
+              browser.test.sendMessage("same-site-cookie-test-done");
+            };
+          } catch (e) {
+            browser.test.fail("xhr failure: " + e);
+          }
+          xhr.send();
+        }
+      });
+    },
+
+    files: {
+      "content_script.js": function() {
+        let myImage = document.createElement("img");
+        // Set the src via wrappedJSObject so the load is triggered with the
+        // content page's principal rather than ours.
+        myImage.wrappedJSObject.setAttribute("src", "http://example.com/tests/toolkit/components/extensions/test/mochitest/file_same_site_cookies_webextension.sjs?loadImage" + Math.random());
+        myImage.onload = function() {
+          browser.test.log("image onload");
+          browser.test.sendMessage("image-loaded-and-same-site-cookie-set");
+        };
+        myImage.onerror = function() {
+          browser.test.log("image onerror");
+        };
+        document.body.appendChild(myImage);
+      },
+    },
+  });
+
+  await extension.startup();
+  let win = window.open("http://example.com/tests/toolkit/components/extensions/test/mochitest/file_same_site_cookies_webextension.sjs?loadWin");
+  await extension.awaitMessage("image-loaded-and-same-site-cookie-set");
+  extension.sendMessage("verify-same-site-cookie-moz-extension");
+  await extension.awaitMessage("same-site-cookie-test-done");
+  win.close();
+  await extension.unload();
+});
+
+</script>
+
+</body>
+</html>
