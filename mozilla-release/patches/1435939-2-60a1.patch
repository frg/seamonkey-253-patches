# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1517925637 -3600
# Node ID 66535b923cac1661d8e40a4570bb18127db8987b
# Parent  e92b3e13839c1fffbfd53f5d4ec32c603a9968f2
Bug 1435939: Propagate the media feature change reason around. r=xidorn

Just some argument shuffling and such.

MozReview-Commit-ID: vPFoU2DUXb

diff --git a/dom/xbl/nsBindingManager.cpp b/dom/xbl/nsBindingManager.cpp
--- a/dom/xbl/nsBindingManager.cpp
+++ b/dom/xbl/nsBindingManager.cpp
@@ -752,17 +752,18 @@ nsBindingManager::WalkAllRules(nsIStyleR
       (*(aFunc))(ruleProcessor, aData);
     }
     return true;
   });
 }
 #endif
 
 bool
-nsBindingManager::MediumFeaturesChanged(nsPresContext* aPresContext)
+nsBindingManager::MediumFeaturesChanged(nsPresContext* aPresContext,
+                                        mozilla::MediaFeatureChangeReason aReason)
 {
   bool rulesChanged = false;
   RefPtr<nsPresContext> presContext = aPresContext;
   bool isStyledByServo = mDocument->IsStyledByServo();
 
   EnumerateBoundContentBindings([=, &rulesChanged](nsXBLBinding* aBinding) {
     if (isStyledByServo) {
       ServoStyleSet* styleSet = aBinding->PrototypeBinding()->GetServoStyleSet();
@@ -772,17 +773,17 @@ nsBindingManager::MediumFeaturesChanged(
         if (styleSet->IsPresContextChanged(presContext)) {
           styleSetChanged = styleSet->SetPresContext(presContext);
         } else {
           // PresContext is not changed. This means aPresContext is still
           // alive since the last time it initialized this XBL styleset.
           // It's safe to check whether medium features changed.
           bool viewportUnitsUsed = false;
           styleSetChanged =
-            styleSet->MediumFeaturesChangedRules(&viewportUnitsUsed);
+            styleSet->MediumFeaturesChangedRules(&viewportUnitsUsed, aReason);
           MOZ_ASSERT(!viewportUnitsUsed,
                      "Non-master stylesets shouldn't get flagged as using "
                      "viewport units!");
         }
         rulesChanged = rulesChanged || styleSetChanged;
       }
     } else {
 #ifdef MOZ_OLD_STYLE
diff --git a/dom/xbl/nsBindingManager.h b/dom/xbl/nsBindingManager.h
--- a/dom/xbl/nsBindingManager.h
+++ b/dom/xbl/nsBindingManager.h
@@ -13,16 +13,17 @@
 #include "nsHashKeys.h"
 #include "nsInterfaceHashtable.h"
 #include "nsRefPtrHashtable.h"
 #include "nsURIHashKey.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsXBLBinding.h"
 #include "nsTArray.h"
 #include "nsThreadUtils.h"
+#include "mozilla/MediaFeatureChange.h"
 #include "mozilla/StyleSheet.h"
 #include "mozilla/EventStates.h"
 
 struct ElementDependentRuleProcessorData;
 class nsIXPConnectWrappedJS;
 class nsIAtom;
 class nsIDOMNodeList;
 class nsIDocument;
@@ -133,17 +134,18 @@ public:
   void WalkAllRules(nsIStyleRuleProcessor::EnumFunc aFunc,
                     ElementDependentRuleProcessorData* aData);
 #endif
 
   // Do any processing that needs to happen as a result of a change in the
   // characteristics of the medium, and return whether this rule processor's
   // rules or the servo style set have changed (e.g., because of media
   // queries).
-  bool MediumFeaturesChanged(nsPresContext* aPresContext);
+  bool MediumFeaturesChanged(nsPresContext* aPresContext,
+                             mozilla::MediaFeatureChangeReason);
 
   // Update the content bindings in mBoundContentSet due to medium features
   // changed.
   void UpdateBoundContentBindingsForServo(nsPresContext* aPresContext);
 
   void AppendAllSheets(nsTArray<mozilla::StyleSheet*>& aArray);
 
   void Traverse(nsIContent *aContent,
diff --git a/layout/base/nsPresContext.cpp b/layout/base/nsPresContext.cpp
--- a/layout/base/nsPresContext.cpp
+++ b/layout/base/nsPresContext.cpp
@@ -2046,24 +2046,20 @@ nsPresContext::FlushPendingMediaFeatureV
 {
   if (!mPendingMediaFeatureValuesChange) {
     return;
   }
 
   MediaFeatureChange change = *mPendingMediaFeatureValuesChange;
   mPendingMediaFeatureValuesChange.reset();
 
-  const bool viewportChanged =
-    bool(change.mReason & MediaFeatureChangeReason::ViewportChange);
-
-
   // MediumFeaturesChanged updates the applied rules, so it always gets called.
   if (mShell) {
     change.mRestyleHint |=
-      mShell->StyleSet()->MediumFeaturesChanged(viewportChanged);
+      mShell->StyleSet()->MediumFeaturesChanged(change.mReason);
   }
 
   if (change.mRestyleHint || change.mChangeHint) {
     RebuildAllStyleData(change.mChangeHint, change.mRestyleHint);
   }
 
   if (!mShell || !mShell->DidInitialize()) {
     return;
diff --git a/layout/style/ServoStyleSet.cpp b/layout/style/ServoStyleSet.cpp
--- a/layout/style/ServoStyleSet.cpp
+++ b/layout/style/ServoStyleSet.cpp
@@ -259,42 +259,46 @@ ServoStyleSet::InvalidateStyleForDocumen
       return true;
     });
 
   Servo_InvalidateStyleForDocStateChanges(
     root, &styleSets, aStatesChanged.ServoValue());
 }
 
 nsRestyleHint
-ServoStyleSet::MediumFeaturesChanged(bool aViewportChanged)
+ServoStyleSet::MediumFeaturesChanged(MediaFeatureChangeReason aReason)
 {
   bool viewportUnitsUsed = false;
-  bool rulesChanged = MediumFeaturesChangedRules(&viewportUnitsUsed);
+  bool rulesChanged = MediumFeaturesChangedRules(&viewportUnitsUsed, aReason);
 
   if (nsPresContext* pc = GetPresContext()) {
-    if (mDocument->BindingManager()->MediumFeaturesChanged(pc)) {
+    if (mDocument->BindingManager()->MediumFeaturesChanged(pc, aReason)) {
       // TODO(emilio): We could technically just restyle the bound elements.
       SetStylistXBLStyleSheetsDirty();
       rulesChanged = true;
     }
   }
 
   if (rulesChanged) {
     return eRestyle_Subtree;
   }
 
-  if (viewportUnitsUsed && aViewportChanged) {
+  const bool viewportChanged =
+    bool(aReason & MediaFeatureChangeReason::ViewportChange);
+  if (viewportUnitsUsed && viewportChanged) {
     return eRestyle_ForceDescendants;
   }
 
   return nsRestyleHint(0);
 }
 
 bool
-ServoStyleSet::MediumFeaturesChangedRules(bool* aViewportUnitsUsed)
+ServoStyleSet::MediumFeaturesChangedRules(
+  bool* aViewportUnitsUsed,
+  MediaFeatureChangeReason aReason)
 {
   MOZ_ASSERT(aViewportUnitsUsed);
 
   const OriginFlags rulesChanged = static_cast<OriginFlags>(
     Servo_StyleSet_MediumFeaturesChanged(mRawSet.get(), aViewportUnitsUsed));
 
   if (rulesChanged != OriginFlags(0)) {
     MarkOriginsDirty(rulesChanged);
diff --git a/layout/style/ServoStyleSet.h b/layout/style/ServoStyleSet.h
--- a/layout/style/ServoStyleSet.h
+++ b/layout/style/ServoStyleSet.h
@@ -6,16 +6,17 @@
 
 #ifndef mozilla_ServoStyleSet_h
 #define mozilla_ServoStyleSet_h
 
 #include "mozilla/AtomArray.h"
 #include "mozilla/EffectCompositor.h"
 #include "mozilla/EnumeratedArray.h"
 #include "mozilla/EventStates.h"
+#include "mozilla/MediaFeatureChange.h"
 #include "mozilla/PostTraversalTask.h"
 #include "mozilla/ServoBindingTypes.h"
 #include "mozilla/ServoElementSnapshot.h"
 #include "mozilla/ServoBindings.h"
 #include "mozilla/ServoUtils.h"
 #include "mozilla/StyleSheetInlines.h"
 #include "mozilla/SheetType.h"
 #include "mozilla/UniquePtr.h"
@@ -155,29 +156,29 @@ public:
     MarkOriginsDirty(OriginFlags::All);
   }
 
   bool StyleSheetsHaveChanged() const
   {
     return StylistNeedsUpdate();
   }
 
-  nsRestyleHint MediumFeaturesChanged(bool aViewportChanged);
+  nsRestyleHint MediumFeaturesChanged(MediaFeatureChangeReason);
 
   // Evaluates a given SourceSizeList, returning the optimal viewport width in
   // app units.
   //
   // The SourceSizeList parameter can be null, in which case it will return
   // 100vw.
   nscoord EvaluateSourceSizeList(const RawServoSourceSizeList* aSourceSizeList) const {
     return Servo_SourceSizeList_Evaluate(mRawSet.get(), aSourceSizeList);
   }
 
   // aViewportChanged outputs whether any viewport units is used.
-  bool MediumFeaturesChangedRules(bool* aViewportUnitsUsed);
+  bool MediumFeaturesChangedRules(bool* aViewportUnitsUsed, MediaFeatureChangeReason);
 
   void InvalidateStyleForCSSRuleChanges();
 
   void AddSizeOfIncludingThis(nsWindowSizes& aSizes) const;
   const RawServoStyleSet* RawSet() const {
     return mRawSet.get();
   }
 
diff --git a/layout/style/StyleSetHandle.h b/layout/style/StyleSetHandle.h
--- a/layout/style/StyleSetHandle.h
+++ b/layout/style/StyleSetHandle.h
@@ -4,16 +4,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_StyleSetHandle_h
 #define mozilla_StyleSetHandle_h
 
 #include "mozilla/AtomArray.h"
 #include "mozilla/EventStates.h"
+#include "mozilla/MediaFeatureChange.h"
 #include "mozilla/RefPtr.h"
 #include "mozilla/ServoTypes.h"
 #include "mozilla/SheetType.h"
 #include "mozilla/StyleBackendType.h"
 #include "mozilla/StyleSheet.h"
 #include "nsChangeHint.h"
 #include "nsCSSPseudoElements.h"
 #include "nsTArray.h"
@@ -189,17 +190,17 @@ public:
     inline void RuleAdded(StyleSheet&, css::Rule&);
     inline void RuleChanged(StyleSheet&, css::Rule*);
 
     // TODO(emilio): Remove in favor of Rule* methods.
     inline void RecordStyleSheetChange(StyleSheet* aSheet, StyleSheet::ChangeType);
     inline void RecordShadowStyleChange(mozilla::dom::ShadowRoot* aShadowRoot);
     inline bool StyleSheetsHaveChanged() const;
     inline void InvalidateStyleForCSSRuleChanges();
-    inline nsRestyleHint MediumFeaturesChanged(bool aViewportChanged);
+    inline nsRestyleHint MediumFeaturesChanged(mozilla::MediaFeatureChangeReason);
     inline already_AddRefed<nsStyleContext>
     ProbePseudoElementStyle(dom::Element* aParentElement,
                             mozilla::CSSPseudoElementType aType,
                             nsStyleContext* aParentContext);
     inline already_AddRefed<nsStyleContext>
     ProbePseudoElementStyle(dom::Element* aParentElement,
                             mozilla::CSSPseudoElementType aType,
                             nsStyleContext* aParentContext,
diff --git a/layout/style/StyleSetHandleInlines.h b/layout/style/StyleSetHandleInlines.h
--- a/layout/style/StyleSetHandleInlines.h
+++ b/layout/style/StyleSetHandleInlines.h
@@ -312,19 +312,19 @@ StyleSetHandle::Ptr::RecordShadowStyleCh
 }
 
 bool
 StyleSetHandle::Ptr::StyleSheetsHaveChanged() const
 {
   FORWARD(StyleSheetsHaveChanged, ());
 }
 nsRestyleHint
-StyleSetHandle::Ptr::MediumFeaturesChanged(bool aViewportChanged)
+StyleSetHandle::Ptr::MediumFeaturesChanged(mozilla::MediaFeatureChangeReason aReason)
 {
-  FORWARD(MediumFeaturesChanged, (aViewportChanged));
+  FORWARD(MediumFeaturesChanged, (aReason));
 }
 void
 StyleSetHandle::Ptr::InvalidateStyleForCSSRuleChanges()
 {
   FORWARD(InvalidateStyleForCSSRuleChanges, ());
 }
 
 // check whether there is ::before/::after style for an element
diff --git a/layout/style/nsStyleSet.cpp b/layout/style/nsStyleSet.cpp
--- a/layout/style/nsStyleSet.cpp
+++ b/layout/style/nsStyleSet.cpp
@@ -2661,17 +2661,17 @@ nsStyleSet::HasAttributeDependentStyle(E
     // No point keeping the list of selectors around if we are going to
     // restyle the whole subtree unconditionally.
     aRestyleHintDataResult = Move(data.mHintData);
   }
   return data.mHint;
 }
 
 nsRestyleHint
-nsStyleSet::MediumFeaturesChanged(bool aViewportChanged)
+nsStyleSet::MediumFeaturesChanged(mozilla::MediaFeatureChangeReason aReason)
 {
   NS_ASSERTION(mBatching == 0, "rule processors out of date");
 
   // We can't use WalkRuleProcessors without a content node.
   nsPresContext* presContext = PresContext();
   bool stylesChanged = false;
   for (nsIStyleRuleProcessor* processor : mRuleProcessors) {
     if (!processor) {
@@ -2682,24 +2682,27 @@ nsStyleSet::MediumFeaturesChanged(bool a
   }
   for (nsIStyleRuleProcessor* processor : mScopedDocSheetRuleProcessors) {
     bool thisChanged = processor->MediumFeaturesChanged(presContext);
     stylesChanged = stylesChanged || thisChanged;
   }
 
   if (mBindingManager) {
     bool thisChanged =
-      mBindingManager->MediumFeaturesChanged(presContext);
+      mBindingManager->MediumFeaturesChanged(presContext, aReason);
     stylesChanged = stylesChanged || thisChanged;
   }
 
   if (stylesChanged) {
     return eRestyle_Subtree;
   }
-  if (aViewportChanged && mUsesViewportUnits) {
+  const bool viewportChanged =
+    bool(aReason & MediaFeatureChangeReason::ViewportChange);
+
+  if (viewportChanged && mUsesViewportUnits) {
     // Rebuild all style data without rerunning selector matching.
     return eRestyle_ForceDescendants;
   }
   return nsRestyleHint(0);
 }
 
 bool
 nsStyleSet::EnsureUniqueInnerOnCSSSheets()
diff --git a/layout/style/nsStyleSet.h b/layout/style/nsStyleSet.h
--- a/layout/style/nsStyleSet.h
+++ b/layout/style/nsStyleSet.h
@@ -13,16 +13,17 @@
 #ifndef nsStyleSet_h_
 #define nsStyleSet_h_
 
 #include "mozilla/AtomArray.h"
 #include "mozilla/Attributes.h"
 #include "mozilla/StyleSheetInlines.h"
 #include "mozilla/EnumeratedArray.h"
 #include "mozilla/LinkedList.h"
+#include "mozilla/MediaFeatureChange.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/ServoTypes.h"
 #include "mozilla/SheetType.h"
 
 #include "nsIStyleRuleProcessor.h"
 #include "nsBindingManager.h"
 #include "nsRuleNode.h"
 #include "nsTArray.h"
@@ -408,17 +409,17 @@ class nsStyleSet final
                                            mozilla::RestyleHintData&
                                              aRestyleHintDataResult);
 
   /*
    * Do any processing that needs to happen as a result of a change in
    * the characteristics of the medium, and return restyle hint needed
    * for the change.
    */
-  nsRestyleHint MediumFeaturesChanged(bool aViewportChanged);
+  nsRestyleHint MediumFeaturesChanged(mozilla::MediaFeatureChangeReason);
 
   // APIs to manipulate the style sheet lists.  The sheets in each
   // list are stored with the most significant sheet last.
   nsresult AppendStyleSheet(mozilla::SheetType aType,
                             mozilla::CSSStyleSheet* aSheet);
   nsresult PrependStyleSheet(mozilla::SheetType aType,
                              mozilla::CSSStyleSheet* aSheet);
   nsresult RemoveStyleSheet(mozilla::SheetType aType,
