# HG changeset patch
# User Chris AtLee <catlee@mozilla.com>
# Date 1590067713 0
# Node ID 4aae9dc67310a0d7cc8eae3c415205b9930c153d
# Parent  2926b949f2d56662c2e45bb21a82c541454caabd
Bug 1637381: Add support for extracting zst files to mozbuild tooltool r=glandium

Differential Revision: https://phabricator.services.mozilla.com/D75203

diff --git a/python/mozbuild/mozbuild/action/tooltool.py b/python/mozbuild/mozbuild/action/tooltool.py
--- a/python/mozbuild/mozbuild/action/tooltool.py
+++ b/python/mozbuild/mozbuild/action/tooltool.py
@@ -539,17 +539,17 @@ def _cache_checksum_matches(base_file, c
 
 def _compute_cache_checksum(filename):
     with open(filename, "rb") as f:
         return digest_file(f, "sha256")
 
 
 def unpack_file(filename, setup=None):
     """Untar `filename`, assuming it is uncompressed or compressed with bzip2,
-    xz, gzip, or unzip a zip file. The file is assumed to contain a single
+    xz, gzip, zst, or unzip a zip file. The file is assumed to contain a single
     directory with a name matching the base of the given filename.
     Xz support is handled by shelling out to 'tar'."""
 
     checksum = _compute_cache_checksum(filename)
 
     if tarfile.is_tarfile(filename):
         tar_file, zip_ext = os.path.splitext(filename)
         base_file, tar_ext = os.path.splitext(tar_file)
@@ -563,16 +563,28 @@ def unpack_file(filename, setup=None):
     elif filename.endswith('.tar.xz'):
         base_file = filename.replace('.tar.xz', '')
         if _cache_checksum_matches(base_file, checksum):
             return True
         clean_path(base_file)
         log.info('untarring "%s"' % filename)
         if not execute('tar -Jxf %s 2>&1' % filename):
             return False
+    elif os.path.isfile(filename) and filename.endswith('.tar.zst'):
+        import zstandard
+        base_file = filename.replace('.tar.zst', '')
+        if _cache_checksum_matches(base_file, checksum):
+            return True
+        clean_path(base_file)
+        log.info('untarring "%s"' % filename)
+        dctx = zstandard.ZstdDecompressor()
+        with dctx.stream_reader(open(filename, "rb")) as fileobj:
+            tar = tarfile.open(fileobj=fileobj, mode='r|')
+            tar.extractall()
+            tar.close()
     elif zipfile.is_zipfile(filename):
         base_file = filename.replace('.zip', '')
         if _cache_checksum_matches(base_file, checksum):
             return True
         clean_path(base_file)
         log.info('unzipping "%s"' % filename)
         z = zipfile.ZipFile(filename)
         z.extractall()
diff --git a/python/mozbuild/mozbuild/artifact_commands.py b/python/mozbuild/mozbuild/artifact_commands.py
--- a/python/mozbuild/mozbuild/artifact_commands.py
+++ b/python/mozbuild/mozbuild/artifact_commands.py
@@ -316,28 +316,37 @@ class PackageFrontend(MachCommandBase):
                     b = 'toolchain-{}'.format(b)
 
                 task = toolchains.get(aliases.get(b, b))
                 if not task:
                     self.log(logging.ERROR, 'artifact', {'build': user_value},
                              'Could not find a toolchain build named `{build}`')
                     return 1
 
+                artifact_name = task.attributes.get('toolchain-artifact')
+                self.log(logging.DEBUG, 'artifact',
+                         {'name': artifact_name,
+                          'index': task.optimization.get('index-search')},
+                         'Searching for {name} in {index}')
                 task_id = IndexSearch().should_replace_task(
                     task, {}, task.optimization.get('index-search', []))
-                artifact_name = task.attributes.get('toolchain-artifact')
                 if task_id in (True, False) or not artifact_name:
                     self.log(logging.ERROR, 'artifact', {'build': user_value},
                              'Could not find artifacts for a toolchain build '
                              'named `{build}`. Local commits and other changes '
                              'in your checkout may cause this error. Try '
                              'updating to a fresh checkout of mozilla-central '
                              'to use artifact builds.')
                     return 1
 
+                self.log(logging.DEBUG, 'artifact',
+                         {'name': artifact_name,
+                          'task_id': task_id},
+                         'Found {name} in {task_id}')
+
                 record = ArtifactRecord(task_id, artifact_name)
                 records[record.filename] = record
 
         # Handle the list of files of the form path@task-id on the command
         # line. Each of those give a path to an artifact to download.
         for f in files:
             if '@' not in f:
                 self.log(logging.ERROR, 'artifact', {},
@@ -423,17 +432,26 @@ class PackageFrontend(MachCommandBase):
                         data = fh.read(1024 * 1024)
                         if not data:
                             break
                         h.update(data)
                 artifacts[record.url] = {
                     'sha256': h.hexdigest(),
                 }
             if record.unpack and not no_unpack:
-                unpack_file(local)
+                # Try to unpack the file. If we get an exception importing
+                # zstandard when calling unpack_file, we can try installing
+                # zstandard locally and trying again
+                try:
+                    unpack_file(local)
+                except ImportError as e:
+                    if e.name != "zstandard":
+                        raise
+                    self._ensure_zstd()
+                    unpack_file(local)
                 os.unlink(local)
 
         if not downloaded:
             self.log(logging.ERROR, 'artifact', {}, 'Nothing to download')
             if files:
                 return 1
 
         if artifacts:
diff --git a/python/mozbuild/mozbuild/base.py b/python/mozbuild/mozbuild/base.py
--- a/python/mozbuild/mozbuild/base.py
+++ b/python/mozbuild/mozbuild/base.py
@@ -842,16 +842,23 @@ class MozbuildObject(ProcessExecutionMix
         return pipenv
 
     def activate_pipenv(self, pipfile=None, populate=False, python=None):
         if pipfile is not None and not os.path.exists(pipfile):
             raise Exception('Pipfile not found: %s.' % pipfile)
         self.ensure_pipenv()
         self.virtualenv_manager.activate_pipenv(pipfile, populate, python)
 
+    def _ensure_zstd(self):
+        try:
+            import zstandard  # noqa: F401
+        except (ImportError, AttributeError):
+            self._activate_virtualenv()
+            self.virtualenv_manager.install_pip_package('zstandard>=0.9.0,<=0.13.0')
+
 
 class MachCommandBase(MozbuildObject):
     """Base class for mach command providers that wish to be MozbuildObjects.
 
     This provides a level of indirection so MozbuildObject can be refactored
     without having to change everything that inherits from it.
     """
 
