# HG changeset patch
# User Frank-Rainer Grahl <frgrahl@gmx.net>
# Date 1684967687 -7200
# Parent  362aa4fb5e1f3640e5a5cba91af07ab36b636b3c
No Bug - Kill telemetry code in docshell. r=me a=me

diff --git a/docshell/base/nsDocShell.cpp b/docshell/base/nsDocShell.cpp
--- a/docshell/base/nsDocShell.cpp
+++ b/docshell/base/nsDocShell.cpp
@@ -25,17 +25,16 @@
 #include "mozilla/HTMLEditor.h"
 #include "mozilla/LoadInfo.h"
 #include "mozilla/Logging.h"
 #include "mozilla/MediaFeatureChange.h"
 #include "mozilla/Preferences.h"
 #include "mozilla/ResultExtensions.h"
 #include "mozilla/Services.h"
 #include "mozilla/StartupTimeline.h"
-#include "mozilla/Telemetry.h"
 #include "mozilla/Unused.h"
 
 #include "mozilla/dom/ClientChannelHelper.h"
 #include "mozilla/dom/ClientHandle.h"
 #include "mozilla/dom/ClientInfo.h"
 #include "mozilla/dom/ClientManager.h"
 #include "mozilla/dom/ClientSource.h"
 #include "mozilla/dom/ContentChild.h"
@@ -111,17 +110,16 @@
 #include "nsIReflowObserver.h"
 #include "nsIScriptChannel.h"
 #include "nsIScriptError.h"
 #include "nsIScriptObjectPrincipal.h"
 #include "nsIScriptSecurityManager.h"
 #include "nsIScrollableFrame.h"
 #include "nsIScrollObserver.h"
 #include "nsISecureBrowserUI.h"
-#include "nsISecurityUITelemetry.h"
 #include "nsISeekableStream.h"
 #include "nsISelectionDisplay.h"
 #include "nsIServiceWorkerManager.h"
 #include "nsISHContainer.h"
 #include "nsISHEntry.h"
 #include "nsISHistory.h"
 #include "nsISHistoryInternal.h"
 #include "nsISiteSecurityService.h"
@@ -1470,87 +1468,16 @@ nsDocShell::GetCharset(nsACString& aChar
   NS_ENSURE_TRUE(presShell, NS_ERROR_FAILURE);
   nsIDocument* doc = presShell->GetDocument();
   NS_ENSURE_TRUE(doc, NS_ERROR_FAILURE);
   doc->GetDocumentCharacterSet()->Name(aCharset);
   return NS_OK;
 }
 
 NS_IMETHODIMP
-nsDocShell::GatherCharsetMenuTelemetry()
-{
-  nsCOMPtr<nsIContentViewer> viewer;
-  GetContentViewer(getter_AddRefs(viewer));
-  if (!viewer) {
-    return NS_OK;
-  }
-
-  nsIDocument* doc = viewer->GetDocument();
-  if (!doc || doc->WillIgnoreCharsetOverride()) {
-    return NS_OK;
-  }
-
-  Telemetry::Accumulate(Telemetry::CHARSET_OVERRIDE_USED, true);
-
-  bool isFileURL = false;
-  nsIURI* url = doc->GetOriginalURI();
-  if (url) {
-    url->SchemeIs("file", &isFileURL);
-  }
-
-  int32_t charsetSource = doc->GetDocumentCharacterSetSource();
-  switch (charsetSource) {
-    case kCharsetFromTopLevelDomain:
-      // Unlabeled doc on a domain that we map to a fallback encoding
-      Telemetry::Accumulate(Telemetry::CHARSET_OVERRIDE_SITUATION, 7);
-      break;
-    case kCharsetFromFallback:
-    case kCharsetFromDocTypeDefault:
-    case kCharsetFromCache:
-    case kCharsetFromParentFrame:
-    case kCharsetFromHintPrevDoc:
-      // Changing charset on an unlabeled doc.
-      if (isFileURL) {
-        Telemetry::Accumulate(Telemetry::CHARSET_OVERRIDE_SITUATION, 0);
-      } else {
-        Telemetry::Accumulate(Telemetry::CHARSET_OVERRIDE_SITUATION, 1);
-      }
-      break;
-    case kCharsetFromAutoDetection:
-      // Changing charset on unlabeled doc where chardet fired
-      if (isFileURL) {
-        Telemetry::Accumulate(Telemetry::CHARSET_OVERRIDE_SITUATION, 2);
-      } else {
-        Telemetry::Accumulate(Telemetry::CHARSET_OVERRIDE_SITUATION, 3);
-      }
-      break;
-    case kCharsetFromMetaPrescan:
-    case kCharsetFromMetaTag:
-    case kCharsetFromChannel:
-      // Changing charset on a doc that had a charset label.
-      Telemetry::Accumulate(Telemetry::CHARSET_OVERRIDE_SITUATION, 4);
-      break;
-    case kCharsetFromParentForced:
-    case kCharsetFromUserForced:
-      // Changing charset on a document that already had an override.
-      Telemetry::Accumulate(Telemetry::CHARSET_OVERRIDE_SITUATION, 5);
-      break;
-    case kCharsetFromIrreversibleAutoDetection:
-    case kCharsetFromOtherComponent:
-    case kCharsetFromByteOrderMark:
-    case kCharsetUninitialized:
-    default:
-      // Bug. This isn't supposed to happen.
-      Telemetry::Accumulate(Telemetry::CHARSET_OVERRIDE_SITUATION, 6);
-      break;
-  }
-  return NS_OK;
-}
-
-NS_IMETHODIMP
 nsDocShell::SetCharset(const nsACString& aCharset)
 {
   // set the charset override
   return SetForcedCharset(aCharset);
 }
 
 NS_IMETHODIMP
 nsDocShell::SetForcedCharset(const nsACString& aCharset)
@@ -4627,38 +4554,25 @@ nsDocShell::DisplayLoadError(nsresult aE
         // HSTS/pinning takes precedence over the expert bad cert pref. We
         // never want to show the "Add Exception" button for these sites.
         // In the future we should differentiate between an HSTS host and a
         // pinned host and display a more informative message to the user.
         if (isStsHost || isPinnedHost) {
           cssClass.AssignLiteral("badStsCert");
         }
 
-        uint32_t bucketId;
-        if (isStsHost) {
-          // measuring STS separately allows us to measure click through
-          // rates easily
-          bucketId = nsISecurityUITelemetry::WARNING_BAD_CERT_TOP_STS;
-        } else {
-          bucketId = nsISecurityUITelemetry::WARNING_BAD_CERT_TOP;
-        }
-
         // See if an alternate cert error page is registered
         nsAutoCString alternateErrorPage;
         nsresult rv =
           Preferences::GetCString("security.alternate_certificate_error_page",
                                   alternateErrorPage);
         if (NS_SUCCEEDED(rv)) {
           errorPage.Assign(alternateErrorPage);
         }
 
-        if (!IsFrame() && errorPage.EqualsIgnoreCase("certerror")) {
-          Telemetry::Accumulate(mozilla::Telemetry::SECURITY_UI, bucketId);
-        }
-
       } else {
         error = "nssFailure2";
       }
     }
   } else if (NS_ERROR_PHISHING_URI == aError ||
              NS_ERROR_MALWARE_URI == aError ||
              NS_ERROR_UNWANTED_URI == aError ||
              NS_ERROR_HARMFUL_URI == aError) {
@@ -7195,18 +7109,16 @@ nsDocShell::EndPageLoad(nsIWebProgress* 
     return rv;
   }
 
   nsCOMPtr<nsITimedChannel> timingChannel = do_QueryInterface(aChannel);
   if (timingChannel) {
     TimeStamp channelCreationTime;
     rv = timingChannel->GetChannelCreation(&channelCreationTime);
     if (NS_SUCCEEDED(rv) && !channelCreationTime.IsNull()) {
-      Telemetry::AccumulateTimeDelta(Telemetry::TOTAL_CONTENT_PAGE_LOAD_TIME,
-                                     channelCreationTime);
       nsCOMPtr<nsPILoadGroupInternal> internalLoadGroup =
         do_QueryInterface(mLoadGroup);
       if (internalLoadGroup) {
         internalLoadGroup->OnEndPageLoad(aChannel);
       }
     }
   }
 
diff --git a/docshell/base/nsIDocShell.idl b/docshell/base/nsIDocShell.idl
--- a/docshell/base/nsIDocShell.idl
+++ b/docshell/base/nsIDocShell.idl
@@ -709,23 +709,16 @@ interface nsIDocShell : nsIDocShellTreeI
    * Upon getting, returns the canonical encoding label of the document
    * currently loaded into this docshell.
    *
    * Upon setting, sets forcedCharset for compatibility with legacy callers.
    */
   attribute ACString charset;
 
   /**
-   * Called when the user chose an encoding override from the character
-   * encoding menu. Separate from the setter for the charset property to avoid
-   * extensions adding noise to the data.
-   */
-  void gatherCharsetMenuTelemetry();
-
-  /**
    * The charset forced by the user.
    */
   attribute ACString forcedCharset;
 
   /**
    * In a child docshell, this is the charset of the parent docshell
    */
   [noscript, notxpcom, nostdcall] void setParentCharset(
diff --git a/toolkit/content/browser-child.js b/toolkit/content/browser-child.js
--- a/toolkit/content/browser-child.js
+++ b/toolkit/content/browser-child.js
@@ -522,17 +522,16 @@ addEventListener("TextZoomChange", funct
 }, false);
 
 addEventListener("ZoomChangeUsingMouseWheel", function() {
   sendAsyncMessage("ZoomChangeUsingMouseWheel", {});
 }, false);
 
 addMessageListener("UpdateCharacterSet", function(aMessage) {
   docShell.charset = aMessage.data.value;
-  docShell.gatherCharsetMenuTelemetry();
 });
 
 /**
  * Remote thumbnail request handler for PageThumbs thumbnails.
  */
 addMessageListener("Browser:Thumbnail:Request", function(aMessage) {
   let snapshot;
   let args = aMessage.data.additionalArgs;
diff --git a/toolkit/content/widgets/browser.xml b/toolkit/content/widgets/browser.xml
--- a/toolkit/content/widgets/browser.xml
+++ b/toolkit/content/widgets/browser.xml
@@ -516,17 +516,16 @@
       <property name="contentTitle"
                 onget="return this.contentDocument.title;"
                 readonly="true"/>
 
       <property name="characterSet"
                 onget="return this.docShell.charset;">
         <setter><![CDATA[
           this.docShell.charset = val;
-          this.docShell.gatherCharsetMenuTelemetry();
         ]]></setter>
       </property>
 
       <property name="mayEnableCharacterEncodingMenu"
                 onget="return this.docShell.mayEnableCharacterEncodingMenu;"
                 readonly="true"/>
 
       <property name="contentPrincipal"
