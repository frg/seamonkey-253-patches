# HG changeset patch
# User Chris Manchester <cmanchester@mozilla.com>
# Date 1528330339 25200
#      Wed Jun 06 17:12:19 2018 -0700
# Node ID f7fa20cd1fdc53e314385340dea1223bd5665914
# Parent  d41073f73725a3a8326d1fe057656094350cf071
Bug 1466254 - Don't assume flags/includes only appear once when factoring gn-generated moz.build conditions. r=mshal

Some recently introduced gn-configs contain duplicate flags/includes, which
aren't handled correctly and introduce non-determinism into the gn-moz.build
generation. This patch makes the moz.build generator faithfully reproduce
duplicated flags, usually to no effect, which is unfortunate in these cases,
but a reasonable approach for the moz.build generator in general.

MozReview-Commit-ID: 6PvobD9JRwN

diff --git a/python/mozbuild/mozbuild/gn_processor.py b/python/mozbuild/mozbuild/gn_processor.py
--- a/python/mozbuild/mozbuild/gn_processor.py
+++ b/python/mozbuild/mozbuild/gn_processor.py
@@ -354,18 +354,18 @@ def find_common_attrs(config_attributes)
         # contains parts it had in common with in `input_attrs`.
 
         for k, input_value in input_attrs.items():
             # Anything in `input_attrs` must match what's already in
             # `reference`.
             common_value = reference.get(k)
             if common_value:
                 if isinstance(input_value, list):
-                    input_value = set(input_value)
-                    reference[k] = [i for i in common_value if i in input_value]
+                    reference[k] = [i for i in common_value
+                                    if input_value.count(i) == common_value.count(i)]
                 elif isinstance(input_value, dict):
                     reference[k] = {key: value for key, value in common_value.items()
                                     if key in input_value and value == input_value[key]}
                 elif input_value != common_value:
                     del reference[k]
             elif k in reference:
                 del reference[k]
 
@@ -376,18 +376,18 @@ def find_common_attrs(config_attributes)
 
     def make_difference(reference, input_attrs):
         # Modifies `input_attrs` so that after calling this function it contains
         # no parts it has in common with in `reference`.
         for k, input_value in input_attrs.items():
             common_value = reference.get(k)
             if common_value:
                 if isinstance(input_value, list):
-                    common_value = set(common_value)
-                    input_attrs[k] = [i for i in input_value if i not in common_value]
+                    input_attrs[k] = [i for i in input_value
+                                      if common_value.count(i) != input_value.count(i)]
                 elif isinstance(input_value, dict):
                     input_attrs[k] = {key: value for key, value in input_value.items()
                                       if key not in common_value}
                 else:
                     del input_attrs[k]
 
     for config_attr_set in config_attributes[1:]:
         make_intersection(common_attrs, config_attr_set)
