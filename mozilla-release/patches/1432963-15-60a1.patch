# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1517383470 -3600
# Node ID 7a858acf697e98a60921123fb4866a41834f0f87
# Parent  af31f41a5011a76a5a387dadff483a06878bbdda
Bug 1432963 - Fixing workers headers - part 15 - static function in a workers namespace, r=smaug

diff --git a/dom/base/ImageEncoder.cpp b/dom/base/ImageEncoder.cpp
--- a/dom/base/ImageEncoder.cpp
+++ b/dom/base/ImageEncoder.cpp
@@ -84,17 +84,17 @@ public:
   explicit EncodingCompleteEvent(EncodeCompleteCallback* aEncodeCompleteCallback)
     : CancelableRunnable("EncodingCompleteEvent")
     , mImgSize(0)
     , mType()
     , mImgData(nullptr)
     , mEncodeCompleteCallback(aEncodeCompleteCallback)
     , mFailed(false)
   {
-    if (!NS_IsMainThread() && GetCurrentThreadWorkerPrivate()) {
+    if (!NS_IsMainThread() && workers::GetCurrentThreadWorkerPrivate()) {
       mCreationEventTarget = GetCurrentThreadEventTarget();
     } else {
       mCreationEventTarget = GetMainThreadEventTarget();
     }
   }
 
   NS_IMETHOD Run() override
   {
diff --git a/dom/base/nsContentUtils.cpp b/dom/base/nsContentUtils.cpp
--- a/dom/base/nsContentUtils.cpp
+++ b/dom/base/nsContentUtils.cpp
@@ -2512,17 +2512,17 @@ nsContentUtils::CalcRoundedWindowSizeFor
   *aOutputHeight = resultHeight;
 }
 
 bool
 nsContentUtils::ThreadsafeIsCallerChrome()
 {
   return NS_IsMainThread() ?
     IsCallerChrome() :
-    mozilla::dom::IsCurrentThreadRunningChromeWorker();
+    workers::IsCurrentThreadRunningChromeWorker();
 }
 
 bool
 nsContentUtils::IsCallerContentXBL()
 {
   JSContext *cx = GetCurrentJSContext();
   if (!cx)
     return false;
@@ -2551,17 +2551,17 @@ nsContentUtils::IsSystemCaller(JSContext
 
 bool
 nsContentUtils::ThreadsafeIsSystemCaller(JSContext* aCx)
 {
   if (NS_IsMainThread()) {
     return IsSystemCaller(aCx);
   }
 
-  return GetWorkerPrivateFromContext(aCx)->UsesSystemPrincipal();
+  return workers::GetWorkerPrivateFromContext(aCx)->UsesSystemPrincipal();
 }
 
 // static
 bool
 nsContentUtils::LookupBindingMember(JSContext* aCx, nsIContent *aContent,
                                     JS::Handle<jsid> aId,
                                     JS::MutableHandle<JS::PropertyDescriptor> aDesc)
 {
@@ -6208,17 +6208,17 @@ nsContentUtils::GetCurrentJSContext()
 /* static */
 JSContext *
 nsContentUtils::GetCurrentJSContextForThread()
 {
   MOZ_ASSERT(IsInitialized());
   if (MOZ_LIKELY(NS_IsMainThread())) {
     return GetCurrentJSContext();
   }
-  return GetCurrentThreadJSContext();
+  return workers::GetCurrentThreadJSContext();
 }
 
 template<typename StringType, typename CharType>
 void
 _ASCIIToLowerInSitu(StringType& aStr)
 {
   CharType* iter = aStr.BeginWriting();
   CharType* end = aStr.EndWriting();
diff --git a/dom/clients/api/Client.cpp b/dom/clients/api/Client.cpp
--- a/dom/clients/api/Client.cpp
+++ b/dom/clients/api/Client.cpp
@@ -14,16 +14,17 @@
 #include "mozilla/dom/Promise.h"
 #include "mozilla/dom/WorkerPrivate.h"
 #include "mozilla/dom/WorkerScope.h"
 #include "nsIGlobalObject.h"
 
 namespace mozilla {
 namespace dom {
 
+using namespace workers;
 using mozilla::dom::ipc::StructuredCloneData;
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(mozilla::dom::Client);
 NS_IMPL_CYCLE_COLLECTING_RELEASE(mozilla::dom::Client);
 NS_IMPL_CYCLE_COLLECTION_WRAPPERCACHE(mozilla::dom::Client, mGlobal);
 
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(mozilla::dom::Client)
   NS_WRAPPERCACHE_INTERFACE_MAP_ENTRY
diff --git a/dom/clients/api/ClientDOMUtil.h b/dom/clients/api/ClientDOMUtil.h
--- a/dom/clients/api/ClientDOMUtil.h
+++ b/dom/clients/api/ClientDOMUtil.h
@@ -23,17 +23,18 @@ template<typename Func, typename Arg, ty
 void
 StartClientManagerOp(Func aFunc, const Arg& aArg, nsISerialEventTarget* aTarget,
                      Resolve aResolve, Reject aReject)
 {
   using mozilla::dom::WorkerStatus::Closing;
 
   RefPtr<WorkerHolderToken> token;
   if (!NS_IsMainThread()) {
-    token = WorkerHolderToken::Create(GetCurrentThreadWorkerPrivate(), Closing);
+    token = WorkerHolderToken::Create(workers::GetCurrentThreadWorkerPrivate(),
+                                      Closing);
   }
 
   RefPtr<ClientOpPromise> promise = aFunc(aArg, aTarget);
   promise->Then(aTarget, __func__,
     [aResolve, token](const ClientOpResult& aResult) {
       if (token && token->IsShuttingDown()) {
         return;
       }
diff --git a/dom/clients/api/Clients.cpp b/dom/clients/api/Clients.cpp
--- a/dom/clients/api/Clients.cpp
+++ b/dom/clients/api/Clients.cpp
@@ -16,16 +16,17 @@
 #include "mozilla/dom/WorkerPrivate.h"
 #include "mozilla/SystemGroup.h"
 #include "nsIGlobalObject.h"
 #include "nsString.h"
 
 namespace mozilla {
 namespace dom {
 
+using namespace workers;
 using mozilla::ipc::PrincipalInfo;
 
 NS_IMPL_CYCLE_COLLECTING_ADDREF(Clients);
 NS_IMPL_CYCLE_COLLECTING_RELEASE(Clients);
 NS_IMPL_CYCLE_COLLECTION_WRAPPERCACHE(Clients, mGlobal);
 
 NS_INTERFACE_MAP_BEGIN_CYCLE_COLLECTION(Clients)
   NS_WRAPPERCACHE_INTERFACE_MAP_ENTRY
diff --git a/dom/clients/manager/ClientManager.cpp b/dom/clients/manager/ClientManager.cpp
--- a/dom/clients/manager/ClientManager.cpp
+++ b/dom/clients/manager/ClientManager.cpp
@@ -37,17 +37,17 @@ ClientManager::ClientManager()
   PBackgroundChild* parentActor = BackgroundChild::GetOrCreateForCurrentThread();
   if (NS_WARN_IF(!parentActor)) {
     Shutdown();
     return;
   }
 
   RefPtr<WorkerHolderToken> workerHolderToken;
   if (!NS_IsMainThread()) {
-    WorkerPrivate* workerPrivate = GetCurrentThreadWorkerPrivate();
+    WorkerPrivate* workerPrivate = workers::GetCurrentThreadWorkerPrivate();
     MOZ_DIAGNOSTIC_ASSERT(workerPrivate);
 
     workerHolderToken =
       WorkerHolderToken::Create(workerPrivate, Closing,
                                 WorkerHolderToken::AllowIdleShutdownStart);
     if (NS_WARN_IF(!workerHolderToken)) {
       Shutdown();
       return;
diff --git a/dom/crypto/WebCryptoTask.cpp b/dom/crypto/WebCryptoTask.cpp
--- a/dom/crypto/WebCryptoTask.cpp
+++ b/dom/crypto/WebCryptoTask.cpp
@@ -148,17 +148,17 @@ class WebCryptoTask::InternalWorkerHolde
     // worker automatically.
   }
 
 public:
   static already_AddRefed<InternalWorkerHolder>
   Create()
   {
     MOZ_ASSERT(!NS_IsMainThread());
-    WorkerPrivate* workerPrivate = GetCurrentThreadWorkerPrivate();
+    WorkerPrivate* workerPrivate = workers::GetCurrentThreadWorkerPrivate();
     MOZ_ASSERT(workerPrivate);
     RefPtr<InternalWorkerHolder> ref = new InternalWorkerHolder();
     if (NS_WARN_IF(!ref->HoldWorker(workerPrivate, Canceling))) {
       return nullptr;
     }
     return ref.forget();
   }
 
diff --git a/dom/events/Event.cpp b/dom/events/Event.cpp
--- a/dom/events/Event.cpp
+++ b/dom/events/Event.cpp
@@ -329,17 +329,17 @@ Event::SetTrusted(bool aTrusted)
 {
   mEvent->mFlags.mIsTrusted = aTrusted;
 }
 
 bool
 Event::Init(mozilla::dom::EventTarget* aGlobal)
 {
   if (!mIsMainThreadEvent) {
-    return IsCurrentThreadRunningChromeWorker();
+    return workers::IsCurrentThreadRunningChromeWorker();
   }
   bool trusted = false;
   nsCOMPtr<nsPIDOMWindowInner> w = do_QueryInterface(aGlobal);
   if (w) {
     nsCOMPtr<nsIDocument> d = w->GetExtantDoc();
     if (d) {
       trusted = nsContentUtils::IsChromeDoc(d);
       nsIPresShell* s = d->GetShell();
@@ -1113,17 +1113,17 @@ Event::TimeStampImpl() const
     Performance* perf = win->GetPerformance();
     if (NS_WARN_IF(!perf)) {
       return 0.0;
     }
 
     return perf->GetDOMTiming()->TimeStampToDOMHighRes(mEvent->mTimeStamp);
   }
 
-  WorkerPrivate* workerPrivate = GetCurrentThreadWorkerPrivate();
+  WorkerPrivate* workerPrivate = workers::GetCurrentThreadWorkerPrivate();
   MOZ_ASSERT(workerPrivate);
 
   return workerPrivate->TimeStampToDOMHighRes(mEvent->mTimeStamp);
 }
 
 double
 Event::TimeStamp() const
 {
diff --git a/dom/events/EventDispatcher.cpp b/dom/events/EventDispatcher.cpp
--- a/dom/events/EventDispatcher.cpp
+++ b/dom/events/EventDispatcher.cpp
@@ -920,17 +920,17 @@ EventDispatcher::DispatchDOMEvent(nsISup
       innerEvent->mOriginalTarget = nullptr;
     } else {
       aDOMEvent->GetIsTrusted(&dontResetTrusted);
     }
 
     if (!dontResetTrusted) {
       //Check security state to determine if dispatcher is trusted
       bool trusted = NS_IsMainThread() ? nsContentUtils::LegacyIsCallerChromeOrNativeCode()
-                                       : mozilla::dom::IsCurrentThreadRunningChromeWorker();
+                                       : mozilla::dom::workers::IsCurrentThreadRunningChromeWorker();
       aDOMEvent->SetTrusted(trusted);
     }
 
     return EventDispatcher::Dispatch(aTarget, aPresContext, innerEvent,
                                      aDOMEvent, aEventStatus);
   } else if (aEvent) {
     return EventDispatcher::Dispatch(aTarget, aPresContext, aEvent,
                                      aDOMEvent, aEventStatus);
diff --git a/dom/events/JSEventHandler.cpp b/dom/events/JSEventHandler.cpp
--- a/dom/events/JSEventHandler.cpp
+++ b/dom/events/JSEventHandler.cpp
@@ -121,17 +121,17 @@ JSEventHandler::HandleEvent(nsIDOMEvent*
 
   Event* event = aEvent->InternalDOMEvent();
   bool isMainThread = event->IsMainThreadEvent();
   bool isChromeHandler =
     isMainThread ?
       nsContentUtils::ObjectPrincipal(
         GetTypedEventHandler().Ptr()->CallbackPreserveColor()) ==
         nsContentUtils::GetSystemPrincipal() :
-      mozilla::dom::IsCurrentThreadRunningChromeWorker();
+      mozilla::dom::workers::IsCurrentThreadRunningChromeWorker();
 
   if (mTypedHandler.Type() == TypedEventHandler::eOnError) {
     MOZ_ASSERT_IF(mEventName, mEventName == nsGkAtoms::onerror);
 
     nsString errorMsg, file;
     EventOrString msgOrEvent;
     Optional<nsAString> fileName;
     Optional<uint32_t> lineNumber;
diff --git a/dom/file/FileBlobImpl.cpp b/dom/file/FileBlobImpl.cpp
--- a/dom/file/FileBlobImpl.cpp
+++ b/dom/file/FileBlobImpl.cpp
@@ -164,17 +164,17 @@ FileBlobImpl::GetType(nsAString& aType)
 {
   aType.Truncate();
 
   if (mContentType.IsVoid()) {
     MOZ_ASSERT(mWholeFile,
                "Should only use lazy ContentType when using the whole file");
 
     if (!NS_IsMainThread()) {
-      WorkerPrivate* workerPrivate = GetCurrentThreadWorkerPrivate();
+      WorkerPrivate* workerPrivate = workers::GetCurrentThreadWorkerPrivate();
       if (!workerPrivate) {
         // I have no idea in which thread this method is called. We cannot
         // return any valid value.
         return;
       }
 
       RefPtr<GetTypeRunnable> runnable =
         new GetTypeRunnable(workerPrivate, this);
diff --git a/dom/file/FileReader.cpp b/dom/file/FileReader.cpp
--- a/dom/file/FileReader.cpp
+++ b/dom/file/FileReader.cpp
@@ -135,17 +135,17 @@ FileReader::~FileReader()
 /* static */ already_AddRefed<FileReader>
 FileReader::Constructor(const GlobalObject& aGlobal, ErrorResult& aRv)
 {
   nsCOMPtr<nsIGlobalObject> global = do_QueryInterface(aGlobal.GetAsSupports());
   WorkerPrivate* workerPrivate = nullptr;
 
   if (!NS_IsMainThread()) {
     JSContext* cx = aGlobal.Context();
-    workerPrivate = GetWorkerPrivateFromContext(cx);
+    workerPrivate = workers::GetWorkerPrivateFromContext(cx);
     MOZ_ASSERT(workerPrivate);
   }
 
   RefPtr<FileReader> fileReader = new FileReader(global, workerPrivate);
 
   return fileReader.forget();
 }
 
diff --git a/dom/file/FileReaderSync.cpp b/dom/file/FileReaderSync.cpp
--- a/dom/file/FileReaderSync.cpp
+++ b/dom/file/FileReaderSync.cpp
@@ -438,17 +438,17 @@ FileReaderSync::SyncRead(nsIInputStream*
   }
 
   // We need to proceed async.
   nsCOMPtr<nsIAsyncInputStream> asyncStream = do_QueryInterface(aStream);
   if (!asyncStream) {
     return rv;
   }
 
-  WorkerPrivate* workerPrivate = GetCurrentThreadWorkerPrivate();
+  WorkerPrivate* workerPrivate = workers::GetCurrentThreadWorkerPrivate();
   MOZ_ASSERT(workerPrivate);
 
   AutoSyncLoopHolder syncLoop(workerPrivate, Closing);
 
   nsCOMPtr<nsIEventTarget> syncLoopTarget = syncLoop.GetEventTarget();
   if (!syncLoopTarget) {
     // SyncLoop creation can fail if the worker is shutting down.
     return NS_ERROR_DOM_INVALID_STATE_ERR;
diff --git a/dom/notification/Notification.cpp b/dom/notification/Notification.cpp
--- a/dom/notification/Notification.cpp
+++ b/dom/notification/Notification.cpp
@@ -54,17 +54,17 @@
 #include "nsXULAppAPI.h"
 #include "WorkerPrivate.h"
 #include "WorkerRunnable.h"
 #include "WorkerScope.h"
 
 namespace mozilla {
 namespace dom {
 
-using workers::AssertIsOnMainThread;
+using namespace workers;
 
 struct NotificationStrings
 {
   const nsString mID;
   const nsString mTitle;
   const nsString mDir;
   const nsString mLang;
   const nsString mBody;
diff --git a/dom/performance/Performance.cpp b/dom/performance/Performance.cpp
--- a/dom/performance/Performance.cpp
+++ b/dom/performance/Performance.cpp
@@ -536,17 +536,17 @@ Performance::QueueEntry(PerformanceEntry
 
 /* static */ bool
 Performance::IsObserverEnabled(JSContext* aCx, JSObject* aGlobal)
 {
   if (NS_IsMainThread()) {
     return Preferences::GetBool("dom.enable_performance_observer", false);
   }
 
-  WorkerPrivate* workerPrivate = GetCurrentThreadWorkerPrivate();
+  WorkerPrivate* workerPrivate = workers::GetCurrentThreadWorkerPrivate();
   MOZ_ASSERT(workerPrivate);
   workerPrivate->AssertIsOnWorkerThread();
 
   RefPtr<PrefEnabledRunnable> runnable =
     new PrefEnabledRunnable(workerPrivate,
                             NS_LITERAL_CSTRING("dom.enable_performance_observer"));
 
   return runnable->Dispatch() && runnable->IsEnabled();
diff --git a/dom/performance/PerformanceObserver.cpp b/dom/performance/PerformanceObserver.cpp
--- a/dom/performance/PerformanceObserver.cpp
+++ b/dom/performance/PerformanceObserver.cpp
@@ -84,17 +84,17 @@ PerformanceObserver::Constructor(const G
     MOZ_ASSERT(ownerWindow->IsInnerWindow());
 
     RefPtr<PerformanceObserver> observer =
       new PerformanceObserver(ownerWindow, aCb);
     return observer.forget();
   }
 
   JSContext* cx = aGlobal.Context();
-  WorkerPrivate* workerPrivate = GetWorkerPrivateFromContext(cx);
+  WorkerPrivate* workerPrivate = workers::GetWorkerPrivateFromContext(cx);
   MOZ_ASSERT(workerPrivate);
 
   RefPtr<PerformanceObserver> observer =
     new PerformanceObserver(workerPrivate, aCb);
   return observer.forget();
 }
 
 JSObject*
diff --git a/dom/promise/Promise.cpp b/dom/promise/Promise.cpp
--- a/dom/promise/Promise.cpp
+++ b/dom/promise/Promise.cpp
@@ -492,17 +492,17 @@ Promise::ReportRejectedPromise(JSContext
   if (!report.init(aCx, result, js::ErrorReport::NoSideEffects)) {
     JS_ClearPendingException(aCx);
     return;
   }
 
   RefPtr<xpc::ErrorReport> xpcReport = new xpc::ErrorReport();
   bool isMainThread = MOZ_LIKELY(NS_IsMainThread());
   bool isChrome = isMainThread ? nsContentUtils::IsSystemPrincipal(nsContentUtils::ObjectPrincipal(aPromise))
-                               : GetCurrentThreadWorkerPrivate()->IsChromeWorker();
+                               : workers::GetCurrentThreadWorkerPrivate()->IsChromeWorker();
   nsGlobalWindow* win = isMainThread ? xpc::WindowGlobalOrNull(aPromise) : nullptr;
   xpcReport->Init(report.report(), report.toStringResult().c_str(), isChrome,
                   win ? win->AsInner()->WindowID() : 0);
 
   // Now post an event to do the real reporting async
   NS_DispatchToMainThread(new AsyncErrorReporter(xpcReport));
 }
 
@@ -758,17 +758,17 @@ PromiseWorkerProxy::~PromiseWorkerProxy(
   MOZ_ASSERT(!mWorkerPromise);
   MOZ_ASSERT(!mWorkerPrivate);
 }
 
 void
 PromiseWorkerProxy::CleanProperties()
 {
 #ifdef DEBUG
-  WorkerPrivate* worker = GetCurrentThreadWorkerPrivate();
+  WorkerPrivate* worker = workers::GetCurrentThreadWorkerPrivate();
   MOZ_ASSERT(worker);
   worker->AssertIsOnWorkerThread();
 #endif
   // Ok to do this unprotected from Create().
   // CleanUp() holds the lock before calling this.
   mCleanedUp = true;
   mWorkerPromise = nullptr;
   mWorkerPrivate = nullptr;
@@ -811,17 +811,17 @@ PromiseWorkerProxy::GetWorkerPrivate() c
 
   return mWorkerPrivate;
 }
 
 Promise*
 PromiseWorkerProxy::WorkerPromise() const
 {
 #ifdef DEBUG
-  WorkerPrivate* worker = GetCurrentThreadWorkerPrivate();
+  WorkerPrivate* worker = workers::GetCurrentThreadWorkerPrivate();
   MOZ_ASSERT(worker);
   worker->AssertIsOnWorkerThread();
 #endif
   MOZ_ASSERT(mWorkerPromise);
   return mWorkerPromise;
 }
 
 void
diff --git a/dom/push/PushManager.cpp b/dom/push/PushManager.cpp
--- a/dom/push/PushManager.cpp
+++ b/dom/push/PushManager.cpp
@@ -28,16 +28,17 @@
 
 #include "WorkerRunnable.h"
 #include "WorkerPrivate.h"
 #include "WorkerScope.h"
 
 namespace mozilla {
 namespace dom {
 
+using namespace workers;
 using workers::AssertIsOnMainThread;
 
 namespace {
 
 nsresult
 GetPermissionState(nsIPrincipal* aPrincipal,
                    PushPermissionState& aState)
 {
diff --git a/dom/push/PushSubscription.cpp b/dom/push/PushSubscription.cpp
--- a/dom/push/PushSubscription.cpp
+++ b/dom/push/PushSubscription.cpp
@@ -18,16 +18,18 @@
 #include "mozilla/dom/PushUtil.h"
 #include "mozilla/dom/WorkerPrivate.h"
 #include "mozilla/dom/WorkerScope.h"
 #include "mozilla/dom/workers/WorkerCommon.h"
 
 namespace mozilla {
 namespace dom {
 
+using namespace workers;
+
 namespace {
 
 class UnsubscribeResultCallback final : public nsIUnsubscribeResultCallback
 {
 public:
   NS_DECL_ISUPPORTS
 
   explicit UnsubscribeResultCallback(Promise* aPromise)
diff --git a/dom/url/URLWorker.cpp b/dom/url/URLWorker.cpp
--- a/dom/url/URLWorker.cpp
+++ b/dom/url/URLWorker.cpp
@@ -15,16 +15,17 @@
 #include "nsURLHelper.h"
 
 namespace mozilla {
 
 using net::nsStandardURL;
 
 namespace dom {
 
+using namespace workers;
 using workers::AssertIsOnMainThread;
 
 // Proxy class to forward all the requests to a URLMainThread object.
 class URLWorker::URLProxy final
 {
 public:
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(URLProxy)
 
diff --git a/dom/workers/MessageEventRunnable.cpp b/dom/workers/MessageEventRunnable.cpp
--- a/dom/workers/MessageEventRunnable.cpp
+++ b/dom/workers/MessageEventRunnable.cpp
@@ -125,17 +125,17 @@ MessageEventRunnable::WorkerRun(JSContex
     }
 
     aWorkerPrivate->AssertInnerWindowIsCorrect();
 
     return DispatchDOMEvent(aCx, aWorkerPrivate, aWorkerPrivate,
                             !aWorkerPrivate->GetParent());
   }
 
-  MOZ_ASSERT(aWorkerPrivate == GetWorkerPrivateFromContext(aCx));
+  MOZ_ASSERT(aWorkerPrivate == workers::GetWorkerPrivateFromContext(aCx));
 
   return DispatchDOMEvent(aCx, aWorkerPrivate, aWorkerPrivate->GlobalScope(),
                           false);
 }
 
 void
 MessageEventRunnable::DispatchError(JSContext* aCx,
                                     DOMEventTargetHelper* aTarget)
diff --git a/dom/workers/Queue.h b/dom/workers/Queue.h
--- a/dom/workers/Queue.h
+++ b/dom/workers/Queue.h
@@ -7,17 +7,19 @@
 #ifndef mozilla_dom_workers_queue_h__
 #define mozilla_dom_workers_queue_h__
 
 #include "mozilla/dom/workers/WorkerCommon.h"
 
 #include "mozilla/Mutex.h"
 #include "nsTArray.h"
 
-BEGIN_WORKERS_NAMESPACE
+namespace mozilla {
+namespace dom {
+namespace workersinternals {
 
 template <typename T, int TCount>
 struct StorageWithTArray
 {
   typedef AutoTArray<T, TCount> StorageType;
 
   static void Reverse(StorageType& aStorage)
   {
@@ -193,11 +195,13 @@ public:
   }
 
 private:
   // Queue is not copyable.
   Queue(const Queue&);
   Queue & operator=(const Queue&);
 };
 
-END_WORKERS_NAMESPACE
+} // workersinternals namespace
+} // dom namespace
+} // mozilla namespace
 
 #endif /* mozilla_dom_workers_queue_h__ */
diff --git a/dom/workers/RegisterBindings.cpp b/dom/workers/RegisterBindings.cpp
--- a/dom/workers/RegisterBindings.cpp
+++ b/dom/workers/RegisterBindings.cpp
@@ -8,17 +8,16 @@
 #include "ChromeWorkerScope.h"
 #include "RuntimeService.h"
 
 #include "jsapi.h"
 #include "mozilla/dom/RegisterWorkerBindings.h"
 #include "mozilla/dom/RegisterWorkerDebuggerBindings.h"
 #include "mozilla/OSFileConstants.h"
 
-USING_WORKERS_NAMESPACE
 using namespace mozilla::dom;
 
 bool
 WorkerPrivate::RegisterBindings(JSContext* aCx, JS::Handle<JSObject*> aGlobal)
 {
   // Init Web IDL bindings
   if (!RegisterWorkerBindings(aCx, aGlobal)) {
     return false;
diff --git a/dom/workers/RuntimeService.cpp b/dom/workers/RuntimeService.cpp
--- a/dom/workers/RuntimeService.cpp
+++ b/dom/workers/RuntimeService.cpp
@@ -73,25 +73,22 @@
 #include "WorkerPrivate.h"
 #include "WorkerRunnable.h"
 #include "WorkerScope.h"
 #include "WorkerThread.h"
 #include "prsystem.h"
 
 #define WORKERS_SHUTDOWN_TOPIC "web-workers-shutdown"
 
-using namespace mozilla;
-using namespace mozilla::dom;
-using namespace mozilla::ipc;
-
-USING_WORKERS_NAMESPACE
-
-using mozilla::MutexAutoLock;
-using mozilla::MutexAutoUnlock;
-using mozilla::Preferences;
+namespace mozilla {
+
+using namespace ipc;
+
+namespace dom {
+namespace workers {
 
 // The size of the worker runtime heaps in bytes. May be changed via pref.
 #define WORKER_DEFAULT_RUNTIME_HEAPSIZE 32 * 1024 * 1024
 
 // The size of the generational GC nursery for workers, in bytes.
 #define WORKER_DEFAULT_NURSERY_SIZE 1 * 1024 * 1024
 
 // The size of the worker JS allocation threshold in MB. May be changed via pref.
@@ -1256,18 +1253,16 @@ PlatformOverrideChanged(const char* /* a
   RuntimeService* runtime = RuntimeService::GetService();
   if (runtime) {
     runtime->UpdatePlatformOverridePreference(override);
   }
 }
 
 } /* anonymous namespace */
 
-BEGIN_WORKERS_NAMESPACE
-
 void
 CancelWorkersForWindow(nsPIDOMWindowInner* aWindow)
 {
   AssertIsOnMainThread();
   RuntimeService* runtime = RuntimeService::GetService();
   if (runtime) {
     runtime->CancelWorkersForWindow(aWindow);
   }
@@ -1308,21 +1303,16 @@ ResumeWorkersForWindow(nsPIDOMWindowInne
 {
   AssertIsOnMainThread();
   RuntimeService* runtime = RuntimeService::GetService();
   if (runtime) {
     runtime->ResumeWorkersForWindow(aWindow);
   }
 }
 
-END_WORKERS_NAMESPACE
-
-namespace mozilla {
-namespace dom {
-
 WorkerPrivate*
 GetWorkerPrivateFromContext(JSContext* aCx)
 {
   MOZ_ASSERT(!NS_IsMainThread());
   MOZ_ASSERT(aCx);
 
   void* cxPrivate = JS_GetContextPrivate(aCx);
   if (!cxPrivate) {
@@ -1378,18 +1368,25 @@ GetCurrentThreadWorkerGlobal()
   }
   WorkerGlobalScope* scope = wp->GlobalScope();
   if (!scope) {
     return nullptr;
   }
   return scope->GetGlobalJSObject();
 }
 
-} // dom namespace
-} // mozilla namespace
+#ifdef DEBUG
+
+void
+AssertIsOnMainThread()
+{
+  MOZ_ASSERT(NS_IsMainThread(), "Wrong thread!");
+}
+
+#endif
 
 struct RuntimeService::IdleThreadInfo
 {
   RefPtr<WorkerThread> mThread;
   mozilla::TimeStamp mExpirationTime;
 };
 
 // This is only touched on the main thread. Initialized in Init() below.
@@ -2916,8 +2913,12 @@ WorkerThreadPrimaryRunnable::FinishedRun
     rts->NoteIdleThread(thread);
   }
   else if (thread->ShutdownRequired()) {
     MOZ_ALWAYS_SUCCEEDS(thread->Shutdown());
   }
 
   return NS_OK;
 }
+
+} // workers namespace
+} // dom namespace
+} // mozilla namespace
diff --git a/dom/workers/RuntimeService.h b/dom/workers/RuntimeService.h
--- a/dom/workers/RuntimeService.h
+++ b/dom/workers/RuntimeService.h
@@ -19,20 +19,18 @@
 class nsITimer;
 class nsPIDOMWindowInner;
 
 namespace mozilla {
 namespace dom {
 class SharedWorker;
 struct WorkerLoadInfo;
 class WorkerThread;
-}
-}
 
-BEGIN_WORKERS_NAMESPACE
+namespace workers {
 
 class RuntimeService final : public nsIObserver
 {
   struct SharedWorkerInfo
   {
     WorkerPrivate* mWorkerPrivate;
     nsCString mScriptSpec;
     nsString mName;
@@ -267,11 +265,13 @@ private:
   nsresult
   CreateSharedWorkerFromLoadInfo(JSContext* aCx,
                                  WorkerLoadInfo* aLoadInfo,
                                  const nsAString& aScriptURL,
                                  const nsAString& aName,
                                  SharedWorker** aSharedWorker);
 };
 
-END_WORKERS_NAMESPACE
+} // workers namespace
+} // dom namespace
+} // mozilla namespace
 
 #endif /* mozilla_dom_workers_runtimeservice_h__ */
diff --git a/dom/workers/ScriptLoader.cpp b/dom/workers/ScriptLoader.cpp
--- a/dom/workers/ScriptLoader.cpp
+++ b/dom/workers/ScriptLoader.cpp
@@ -68,25 +68,25 @@
 #include "Principal.h"
 #include "WorkerHolder.h"
 #include "WorkerPrivate.h"
 #include "WorkerRunnable.h"
 #include "WorkerScope.h"
 
 #define MAX_CONCURRENT_SCRIPTS 1000
 
-USING_WORKERS_NAMESPACE
-
 using mozilla::dom::cache::Cache;
 using mozilla::dom::cache::CacheStorage;
 using mozilla::ipc::PrincipalInfo;
 
 namespace mozilla {
 namespace dom {
 
+using namespace workers;
+
 namespace {
 
 nsIURI*
 GetBaseURI(bool aIsMainScript, WorkerPrivate* aWorkerPrivate)
 {
   MOZ_ASSERT(aWorkerPrivate);
   nsIURI* baseURI;
   WorkerPrivate* parentWorker = aWorkerPrivate->GetParent();
diff --git a/dom/workers/SharedWorker.cpp b/dom/workers/SharedWorker.cpp
--- a/dom/workers/SharedWorker.cpp
+++ b/dom/workers/SharedWorker.cpp
@@ -53,17 +53,17 @@ SharedWorker::~SharedWorker()
 already_AddRefed<SharedWorker>
 SharedWorker::Constructor(const GlobalObject& aGlobal,
                           const nsAString& aScriptURL,
                           const StringOrWorkerOptions& aOptions,
                           ErrorResult& aRv)
 {
   AssertIsOnMainThread();
 
-  RuntimeService* rts = RuntimeService::GetOrCreateService();
+  workers::RuntimeService* rts = workers::RuntimeService::GetOrCreateService();
   if (!rts) {
     aRv = NS_ERROR_NOT_AVAILABLE;
     return nullptr;
   }
 
   nsAutoString name;
   if (aOptions.IsString()) {
     name = aOptions.GetAsString();
diff --git a/dom/workers/WorkerCommon.h b/dom/workers/WorkerCommon.h
--- a/dom/workers/WorkerCommon.h
+++ b/dom/workers/WorkerCommon.h
@@ -13,23 +13,16 @@
 #include "mozilla/Mutex.h"
 #include "nsAutoPtr.h"
 #include "nsCOMPtr.h"
 #include "nsString.h"
 #include "nsTArray.h"
 
 #include "mozilla/dom/ServiceWorkerDescriptor.h"
 
-#define BEGIN_WORKERS_NAMESPACE \
-  namespace mozilla { namespace dom { namespace workers {
-#define END_WORKERS_NAMESPACE \
-  } /* namespace workers */ } /* namespace dom */ } /* namespace mozilla */
-#define USING_WORKERS_NAMESPACE \
-  using namespace mozilla::dom::workers;
-
 class nsIGlobalObject;
 class nsPIDOMWindowInner;
 
 namespace mozilla {
 namespace dom {
 
 // If you change this, the corresponding list in nsIWorkerDebugger.idl needs to
 // be updated too.
@@ -37,33 +30,21 @@ enum WorkerType
 {
   WorkerTypeDedicated,
   WorkerTypeShared,
   WorkerTypeService
 };
 
 class WorkerPrivate;
 
-} // namespace dom
-} // namespace mozilla
-
-BEGIN_WORKERS_NAMESPACE
+namespace workers {
 
 struct PrivatizableBase
 { };
 
-#ifdef DEBUG
-void
-AssertIsOnMainThread();
-#else
-inline void
-AssertIsOnMainThread()
-{ }
-#endif
-
 struct JSSettings
 {
   enum {
     // All the GC parameters that we support.
     JSSettings_JSGC_MAX_BYTES = 0,
     JSSettings_JSGC_MAX_MALLOC_BYTES,
     JSSettings_JSGC_HIGH_FREQUENCY_TIME_LIMIT,
     JSSettings_JSGC_LOW_FREQUENCY_HEAP_GROWTH,
@@ -163,38 +144,66 @@ struct JSSettings
     }
 
     return false;
   }
 };
 
 // All of these are implemented in RuntimeService.cpp
 
+#ifdef DEBUG
+void
+AssertIsOnMainThread();
+#else
+inline void
+AssertIsOnMainThread()
+{ }
+#endif
+
+WorkerPrivate*
+GetWorkerPrivateFromContext(JSContext* aCx);
+
+WorkerPrivate*
+GetCurrentThreadWorkerPrivate();
+
+bool
+IsCurrentThreadRunningChromeWorker();
+
+JSContext*
+GetCurrentThreadJSContext();
+
+JSObject*
+GetCurrentThreadWorkerGlobal();
+
 void
 CancelWorkersForWindow(nsPIDOMWindowInner* aWindow);
 
 void
 FreezeWorkersForWindow(nsPIDOMWindowInner* aWindow);
 
 void
 ThawWorkersForWindow(nsPIDOMWindowInner* aWindow);
 
 void
 SuspendWorkersForWindow(nsPIDOMWindowInner* aWindow);
 
 void
 ResumeWorkersForWindow(nsPIDOMWindowInner* aWindow);
 
-// Random unique constant to facilitate JSPrincipal debugging
-const uint32_t kJSPrincipalsDebugToken = 0x7e2df9d2;
+// All of these are implemented in WorkerScope.cpp
 
 bool
 IsWorkerGlobal(JSObject* global);
 
 bool
 IsDebuggerGlobal(JSObject* global);
 
 bool
 IsDebuggerSandbox(JSObject* object);
 
-END_WORKERS_NAMESPACE
+// Random unique constant to facilitate JSPrincipal debugging
+const uint32_t kJSPrincipalsDebugToken = 0x7e2df9d2;
+
+} // workers namespace
+} // dom namespace
+} // mozilla namespace
 
 #endif // mozilla_dom_workers_WorkerCommon_h
diff --git a/dom/workers/WorkerPrivate.cpp b/dom/workers/WorkerPrivate.cpp
--- a/dom/workers/WorkerPrivate.cpp
+++ b/dom/workers/WorkerPrivate.cpp
@@ -89,34 +89,21 @@ TimeoutsLog()
 {
   return sWorkerTimeoutsLog;
 }
 
 #define LOG(log, _args) MOZ_LOG(log, LogLevel::Debug, _args);
 
 using namespace mozilla;
 using namespace mozilla::dom;
+using namespace mozilla::dom::workers;
 using namespace mozilla::ipc;
 
 MOZ_DEFINE_MALLOC_SIZE_OF(JsWorkerMallocSizeOf)
 
-#ifdef DEBUG
-
-BEGIN_WORKERS_NAMESPACE
-
-void
-AssertIsOnMainThread()
-{
-  MOZ_ASSERT(NS_IsMainThread(), "Wrong thread!");
-}
-
-END_WORKERS_NAMESPACE
-
-#endif
-
 namespace {
 
 #ifdef DEBUG
 
 const nsIID kDEBUGWorkerEventTargetIID = {
   0xccaba3fa, 0x5be2, 0x4de2, { 0xba, 0x87, 0x3b, 0x3b, 0x5b, 0x1d, 0x5, 0xfb }
 };
 
diff --git a/dom/workers/WorkerPrivate.h b/dom/workers/WorkerPrivate.h
--- a/dom/workers/WorkerPrivate.h
+++ b/dom/workers/WorkerPrivate.h
@@ -884,18 +884,18 @@ class WorkerPrivate : public WorkerPriva
     PeriodicTimer = 0,
     IdleTimer,
     NoTimer
   };
 
   bool mDebuggerRegistered;
   WorkerDebugger* mDebugger;
 
-  workers::Queue<WorkerControlRunnable*, 4> mControlQueue;
-  workers::Queue<WorkerRunnable*, 4> mDebuggerQueue;
+  workersinternals::Queue<WorkerControlRunnable*, 4> mControlQueue;
+  workersinternals::Queue<WorkerRunnable*, 4> mDebuggerQueue;
 
   // Touched on multiple threads, protected with mMutex.
   JSContext* mJSContext;
   RefPtr<WorkerThread> mThread;
   PRThread* mPRThread;
 
   // Things touched on worker thread only.
   RefPtr<WorkerGlobalScope> mScope;
@@ -1500,31 +1500,16 @@ public:
   WorkerAvailable(JSContext* aCx, JSObject* /* unused */);
 
 private:
   ChromeWorkerPrivate() = delete;
   ChromeWorkerPrivate(const ChromeWorkerPrivate& aRHS) = delete;
   ChromeWorkerPrivate& operator =(const ChromeWorkerPrivate& aRHS) = delete;
 };
 
-WorkerPrivate*
-GetWorkerPrivateFromContext(JSContext* aCx);
-
-WorkerPrivate*
-GetCurrentThreadWorkerPrivate();
-
-bool
-IsCurrentThreadRunningChromeWorker();
-
-JSContext*
-GetCurrentThreadJSContext();
-
-JSObject*
-GetCurrentThreadWorkerGlobal();
-
 class AutoSyncLoopHolder
 {
   WorkerPrivate* mWorkerPrivate;
   nsCOMPtr<nsIEventTarget> mTarget;
   uint32_t mIndex;
 
 public:
   // See CreateNewSyncLoop() for more information about the correct value to use
diff --git a/dom/workers/WorkerScope.cpp b/dom/workers/WorkerScope.cpp
--- a/dom/workers/WorkerScope.cpp
+++ b/dom/workers/WorkerScope.cpp
@@ -62,19 +62,18 @@ NS_CreateJSTimeoutHandler(JSContext* aCx
                           const mozilla::dom::Sequence<JS::Value>& aArguments,
                           mozilla::ErrorResult& aError);
 
 extern already_AddRefed<nsIScriptTimeoutHandler>
 NS_CreateJSTimeoutHandler(JSContext* aCx,
                           mozilla::dom::WorkerPrivate* aWorkerPrivate,
                           const nsAString& aExpression);
 
-using namespace mozilla;
-using namespace mozilla::dom;
-USING_WORKERS_NAMESPACE
+namespace mozilla {
+namespace dom {
 
 using mozilla::dom::cache::CacheStorage;
 using mozilla::ipc::PrincipalInfo;
 
 WorkerGlobalScope::WorkerGlobalScope(WorkerPrivate* aWorkerPrivate)
 : mSerialEventTarget(aWorkerPrivate->HybridEventTarget())
 , mWindowInteractionsAllowed(0)
 , mWorkerPrivate(aWorkerPrivate)
@@ -1113,17 +1112,17 @@ WorkerDebuggerGlobalScope::EventTargetFo
 }
 
 AbstractThread*
 WorkerDebuggerGlobalScope::AbstractMainThreadFor(TaskCategory aCategory)
 {
   MOZ_CRASH("AbstractMainThreadFor not supported for workers.");
 }
 
-BEGIN_WORKERS_NAMESPACE
+namespace workers {
 
 bool
 IsWorkerGlobal(JSObject* object)
 {
   return IS_INSTANCE_OF(WorkerGlobalScope, object);
 }
 
 bool
@@ -1134,9 +1133,11 @@ IsDebuggerGlobal(JSObject* object)
 
 bool
 IsDebuggerSandbox(JSObject* object)
 {
   return SimpleGlobalObject::SimpleGlobalType(object) ==
     SimpleGlobalObject::GlobalType::WorkerDebuggerSandbox;
 }
 
-END_WORKERS_NAMESPACE
+} // workers namespace
+} // dom namespace
+} // mozilla namespace
diff --git a/ipc/glue/IPCStreamSource.cpp b/ipc/glue/IPCStreamSource.cpp
--- a/ipc/glue/IPCStreamSource.cpp
+++ b/ipc/glue/IPCStreamSource.cpp
@@ -8,17 +8,16 @@
 #include "mozilla/webrender/WebRenderTypes.h"
 #include "nsIAsyncInputStream.h"
 #include "nsICancelableRunnable.h"
 #include "nsIRunnable.h"
 #include "nsISerialEventTarget.h"
 #include "nsStreamUtils.h"
 #include "nsThreadUtils.h"
 
-using mozilla::dom::GetCurrentThreadWorkerPrivate;
 using mozilla::dom::WorkerPrivate;
 using mozilla::dom::WorkerStatus;
 using mozilla::wr::ByteBuffer;
 
 namespace mozilla {
 namespace ipc {
 
 class IPCStreamSource::Callback final : public nsIInputStreamCallback
@@ -133,17 +132,17 @@ IPCStreamSource::Initialize()
   // A source can be used on any thread, but we only support IPCStream on
   // main thread, Workers and PBackground thread right now.  This is due
   // to the requirement  that the thread be guaranteed to live long enough to
   // receive messages. We can enforce this guarantee with a WorkerHolder on
   // worker threads, but not other threads. Main-thread and PBackground thread
   // do not need anything special in order to be kept alive.
   WorkerPrivate* workerPrivate = nullptr;
   if (!NS_IsMainThread()) {
-    workerPrivate = GetCurrentThreadWorkerPrivate();
+    workerPrivate = mozilla::dom::workers::GetCurrentThreadWorkerPrivate();
     if (workerPrivate) {
       bool result = HoldWorker(workerPrivate, WorkerStatus::Canceling);
       if (!result) {
         return false;
       }
 
       mWorkerPrivate = workerPrivate;
     } else {
diff --git a/layout/generic/WritingModes.h b/layout/generic/WritingModes.h
--- a/layout/generic/WritingModes.h
+++ b/layout/generic/WritingModes.h
@@ -601,25 +601,25 @@ public:
    * @param aLogicalAxis The axis to compare from this WritingMode.
    * @param aOther The other WritingMode (from which we'll choose the axis
    *               that's parallel to this WritingMode's aLogicalAxis, for
    *               comparison).
    */
   bool ParallelAxisStartsOnSameSide(LogicalAxis aLogicalAxis,
                                     const WritingMode& aOther) const
   {
-    Side myStartSide =
+    mozilla::Side myStartSide =
       this->PhysicalSide(MakeLogicalSide(aLogicalAxis,
                                          eLogicalEdgeStart));
 
     // Figure out which of aOther's axes is parallel to |this| WritingMode's
     // aLogicalAxis, and get its physical start side as well.
     LogicalAxis otherWMAxis = aOther.IsOrthogonalTo(*this) ?
       GetOrthogonalAxis(aLogicalAxis) : aLogicalAxis;
-    Side otherWMStartSide =
+    mozilla::Side otherWMStartSide =
       aOther.PhysicalSide(MakeLogicalSide(otherWMAxis,
                                           eLogicalEdgeStart));
 
     NS_ASSERTION(myStartSide % 2 == otherWMStartSide % 2,
                  "Should end up with sides in the same physical axis");
     return myStartSide == otherWMStartSide;
   }
 
