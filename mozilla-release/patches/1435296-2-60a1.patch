# HG changeset patch
# User Tom Ritter <tom@mozilla.com>
# Date 1518043857 21600
# Node ID 3171a2575b4efae71c994a41d3c06391d3c868f5
# Parent  9ba4e87b6b014fef875024f51cf8a7b20ff4840b
Bug 1435296 Do not apply timer clamping to CSS animations. r=birtles

This patch creates the capability to have callsites specify if timestamps
should be clamped only in Resist Fingerprinting Mode, or in the more expansive
Timer PRecision Reduction Mode.

Then it changes the CSS Animation callsite to only apply in RFP Mode.

This avoids regressing RFP.

MozReview-Commit-ID: B1pSri0kRk6

diff --git a/dom/animation/AnimationUtils.h b/dom/animation/AnimationUtils.h
--- a/dom/animation/AnimationUtils.h
+++ b/dom/animation/AnimationUtils.h
@@ -28,17 +28,17 @@ class AnimationUtils
 public:
   static dom::Nullable<double>
   TimeDurationToDouble(const dom::Nullable<TimeDuration>& aTime)
   {
     dom::Nullable<double> result;
 
     if (!aTime.IsNull()) {
       result.SetValue(
-        nsRFPService::ReduceTimePrecisionAsMSecs(aTime.Value().ToMilliseconds())
+        nsRFPService::ReduceTimePrecisionAsMSecs(aTime.Value().ToMilliseconds(), TimerPrecisionType::RFPOnly)
       );
     }
 
     return result;
   }
 
   static dom::Nullable<TimeDuration>
   DoubleToTimeDuration(const dom::Nullable<double>& aTime)
diff --git a/toolkit/components/resistfingerprinting/nsRFPService.cpp b/toolkit/components/resistfingerprinting/nsRFPService.cpp
--- a/toolkit/components/resistfingerprinting/nsRFPService.cpp
+++ b/toolkit/components/resistfingerprinting/nsRFPService.cpp
@@ -102,44 +102,48 @@ TimerResolution()
 bool
 nsRFPService::IsResistFingerprintingEnabled()
 {
   return sPrivacyResistFingerprinting;
 }
 
 /* static */
 bool
-nsRFPService::IsTimerPrecisionReductionEnabled()
+nsRFPService::IsTimerPrecisionReductionEnabled(TimerPrecisionType aType)
 {
+  if (aType == TimerPrecisionType::RFPOnly) {
+    return IsResistFingerprintingEnabled();
+  }
+
   return (sPrivacyTimerPrecisionReduction || IsResistFingerprintingEnabled()) &&
          TimerResolution() != 0;
 }
 
 /* static */
 double
-nsRFPService::ReduceTimePrecisionAsMSecs(double aTime)
+nsRFPService::ReduceTimePrecisionAsMSecs(double aTime, TimerPrecisionType aType /* = TimerPrecisionType::All */)
 {
-  if (!IsTimerPrecisionReductionEnabled()) {
+  if (!IsTimerPrecisionReductionEnabled(aType)) {
     return aTime;
   }
   const double resolutionMSec = TimerResolution() / 1000.0;
   double ret = floor(aTime / resolutionMSec) * resolutionMSec;
 #if defined(DEBUG)
   MOZ_LOG(gResistFingerprintingLog, LogLevel::Verbose,
     ("Given: %.*f, Rounding with %.*f, Intermediate: %.*f, Got: %.*f",
       DBL_DIG-1, aTime, DBL_DIG-1, resolutionMSec, DBL_DIG-1, floor(aTime / resolutionMSec), DBL_DIG-1, ret));
 #endif
   return ret;
 }
 
 /* static */
 double
-nsRFPService::ReduceTimePrecisionAsUSecs(double aTime)
+nsRFPService::ReduceTimePrecisionAsUSecs(double aTime, TimerPrecisionType aType /* = TimerPrecisionType::All */)
 {
-  if (!IsTimerPrecisionReductionEnabled()) {
+  if (!IsTimerPrecisionReductionEnabled(aType)) {
     return aTime;
   }
   double resolutionUSec = TimerResolution();
   double ret = floor(aTime / resolutionUSec) * resolutionUSec;
 #if defined(DEBUG)
   double tmp_sResolutionUSec = resolutionUSec;
   MOZ_LOG(gResistFingerprintingLog, LogLevel::Verbose,
     ("Given: %.*f, Rounding with %.*f, Intermediate: %.*f, Got: %.*f",
@@ -152,19 +156,19 @@ nsRFPService::ReduceTimePrecisionAsUSecs
 uint32_t
 nsRFPService::CalculateTargetVideoResolution(uint32_t aVideoQuality)
 {
   return aVideoQuality * NSToIntCeil(aVideoQuality * 16 / 9.0);
 }
 
 /* static */
 double
-nsRFPService::ReduceTimePrecisionAsSecs(double aTime)
+nsRFPService::ReduceTimePrecisionAsSecs(double aTime, TimerPrecisionType aType /* = TimerPrecisionType::All */)
 {
-  if (!IsTimerPrecisionReductionEnabled()) {
+  if (!IsTimerPrecisionReductionEnabled(aType)) {
     return aTime;
   }
   double resolutionUSec = TimerResolution();
   if (TimerResolution() < 1000000) {
     // The resolution is smaller than one sec.  Use the reciprocal to avoid
     // floating point error.
     const double resolutionSecReciprocal = 1000000.0 / resolutionUSec;
     double ret = floor(aTime * resolutionSecReciprocal) / resolutionSecReciprocal;
diff --git a/toolkit/components/resistfingerprinting/nsRFPService.h b/toolkit/components/resistfingerprinting/nsRFPService.h
--- a/toolkit/components/resistfingerprinting/nsRFPService.h
+++ b/toolkit/components/resistfingerprinting/nsRFPService.h
@@ -143,30 +143,41 @@ public:
   enum { ALLOW_MEMMOVE = true };
 
   KeyboardLangs mLang;
   KeyboardRegions mRegion;
   KeyNameIndexType mKeyIdx;
   nsString mKey;
 };
 
+enum TimerPrecisionType {
+  All = 1,
+  RFPOnly = 2
+};
+
 class nsRFPService final : public nsIObserver
 {
 public:
   NS_DECL_ISUPPORTS
   NS_DECL_NSIOBSERVER
 
   static nsRFPService* GetOrCreate();
   static bool IsResistFingerprintingEnabled();
-  static bool IsTimerPrecisionReductionEnabled();
+  static bool IsTimerPrecisionReductionEnabled(TimerPrecisionType aType);
 
   // The following Reduce methods can be called off main thread.
-  static double ReduceTimePrecisionAsMSecs(double aTime);
-  static double ReduceTimePrecisionAsUSecs(double aTime);
-  static double ReduceTimePrecisionAsSecs(double aTime);
+  static double ReduceTimePrecisionAsMSecs(
+    double aTime,
+    TimerPrecisionType aType = TimerPrecisionType::All);
+  static double ReduceTimePrecisionAsUSecs(
+    double aTime,
+    TimerPrecisionType aType = TimerPrecisionType::All);
+  static double ReduceTimePrecisionAsSecs(
+    double aTime,
+    TimerPrecisionType aType = TimerPrecisionType::All);
 
   // This method calculates the video resolution (i.e. height x width) based
   // on the video quality (480p, 720p, etc).
   static uint32_t CalculateTargetVideoResolution(uint32_t aVideoQuality);
 
   // Methods for getting spoofed media statistics and the return value will
   // depend on the video resolution.
   static uint32_t GetSpoofedTotalFrames(double aTime);
