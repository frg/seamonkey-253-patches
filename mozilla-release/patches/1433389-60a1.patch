# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1516972394 -3600
# Node ID 7c524fa25f58fd81a2506ebc374ce0d2b9209754
# Parent  3470abcd3c4b86c80764f98cb8d523836c778ff7
Bug 1433389: Make input[type=number] pseudo-elements accessible to chrome. r=jwatt

MozReview-Commit-ID: 2ycajPYd3CV

diff --git a/layout/style/nsCSSPseudoElementList.h b/layout/style/nsCSSPseudoElementList.h
--- a/layout/style/nsCSSPseudoElementList.h
+++ b/layout/style/nsCSSPseudoElementList.h
@@ -60,29 +60,29 @@ CSS_PSEUDO_ELEMENT(mozFocusOuter, ":-moz
 CSS_PSEUDO_ELEMENT(mozListBullet, ":-moz-list-bullet", 0)
 CSS_PSEUDO_ELEMENT(mozListNumber, ":-moz-list-number", 0)
 
 CSS_PSEUDO_ELEMENT(mozMathAnonymous, ":-moz-math-anonymous", 0)
 
 // HTML5 Forms pseudo elements
 CSS_PSEUDO_ELEMENT(mozNumberWrapper, ":-moz-number-wrapper",
                    CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE |
-                   CSS_PSEUDO_ELEMENT_UA_SHEET_ONLY)
+                   CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS_AND_CHROME)
 CSS_PSEUDO_ELEMENT(mozNumberText, ":-moz-number-text",
                    CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE |
-                   CSS_PSEUDO_ELEMENT_UA_SHEET_ONLY)
+                   CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS_AND_CHROME)
 CSS_PSEUDO_ELEMENT(mozNumberSpinBox, ":-moz-number-spin-box",
                    CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE |
-                   CSS_PSEUDO_ELEMENT_UA_SHEET_ONLY)
+                   CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS_AND_CHROME)
 CSS_PSEUDO_ELEMENT(mozNumberSpinUp, ":-moz-number-spin-up",
                    CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE |
-                   CSS_PSEUDO_ELEMENT_UA_SHEET_ONLY)
+                   CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS_AND_CHROME)
 CSS_PSEUDO_ELEMENT(mozNumberSpinDown, ":-moz-number-spin-down",
                    CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE |
-                   CSS_PSEUDO_ELEMENT_UA_SHEET_ONLY)
+                   CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS_AND_CHROME)
 CSS_PSEUDO_ELEMENT(mozProgressBar, ":-moz-progress-bar",
                    CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE)
 CSS_PSEUDO_ELEMENT(mozRangeTrack, ":-moz-range-track",
                    CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE)
 CSS_PSEUDO_ELEMENT(mozRangeProgress, ":-moz-range-progress",
                    CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE)
 CSS_PSEUDO_ELEMENT(mozRangeThumb, ":-moz-range-thumb",
                    CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE)
diff --git a/layout/style/nsCSSPseudoElements.h b/layout/style/nsCSSPseudoElements.h
--- a/layout/style/nsCSSPseudoElements.h
+++ b/layout/style/nsCSSPseudoElements.h
@@ -32,25 +32,33 @@
 // pseudo element (by default it's ignored).
 #define CSS_PSEUDO_ELEMENT_SUPPORTS_STYLE_ATTRIBUTE    (1<<2)
 // Flag that indicate the pseudo-element supports a user action pseudo-class
 // following it, such as :active or :hover.  This would normally correspond
 // to whether the pseudo-element is tree-like, but we don't support these
 // pseudo-classes on ::before and ::after generated content yet.  See
 // http://dev.w3.org/csswg/selectors4/#pseudo-elements.
 #define CSS_PSEUDO_ELEMENT_SUPPORTS_USER_ACTION_STATE  (1<<3)
-// Is content prevented from parsing selectors containing this pseudo-element?
-#define CSS_PSEUDO_ELEMENT_UA_SHEET_ONLY               (1<<4)
+// Should this pseudo-element be enabled only for UA sheets?
+#define CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS (1<<4)
+// Should this pseudo-element be enabled only for UA sheets and chrome
+// stylesheets?
+#define CSS_PSEUDO_ELEMENT_ENABLED_IN_CHROME (1<<5)
+
+#define CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS_AND_CHROME \
+  (CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS |               \
+   CSS_PSEUDO_ELEMENT_ENABLED_IN_CHROME)
+
 // Can we use the ChromeOnly document.createElement(..., { pseudo: "::foo" })
 // API for creating pseudo-implementing native anonymous content in JS with this
 // pseudo-element?
-#define CSS_PSEUDO_ELEMENT_IS_JS_CREATED_NAC           (1<<5)
+#define CSS_PSEUDO_ELEMENT_IS_JS_CREATED_NAC           (1<<6)
 // Does this pseudo-element act like an item for containers (such as flex and
 // grid containers) and thus needs parent display-based style fixup?
-#define CSS_PSEUDO_ELEMENT_IS_FLEX_OR_GRID_ITEM        (1<<6)
+#define CSS_PSEUDO_ELEMENT_IS_FLEX_OR_GRID_ITEM        (1<<7)
 
 namespace mozilla {
 
 // The total count of CSSPseudoElement is less than 256,
 // so use uint8_t as its underlying type.
 typedef uint8_t CSSPseudoElementTypeBase;
 enum class CSSPseudoElementType : CSSPseudoElementTypeBase {
   // If the actual pseudo-elements stop being first here, change
@@ -132,18 +140,32 @@ public:
   static bool PseudoElementIsFlexOrGridItem(const Type aType)
   {
     return PseudoElementHasFlags(aType,
                                  CSS_PSEUDO_ELEMENT_IS_FLEX_OR_GRID_ITEM);
   }
 
   static bool IsEnabled(Type aType, EnabledState aEnabledState)
   {
-    return !PseudoElementHasFlags(aType, CSS_PSEUDO_ELEMENT_UA_SHEET_ONLY) ||
-           (aEnabledState & EnabledState::eInUASheets);
+    if (!PseudoElementHasFlags(
+      aType, CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS_AND_CHROME)) {
+      return true;
+    }
+
+    if ((aEnabledState & EnabledState::eInUASheets) &&
+        PseudoElementHasFlags(aType, CSS_PSEUDO_ELEMENT_ENABLED_IN_UA_SHEETS)) {
+      return true;
+    }
+
+    if ((aEnabledState & EnabledState::eInChrome) &&
+        PseudoElementHasFlags(aType, CSS_PSEUDO_ELEMENT_ENABLED_IN_CHROME)) {
+      return true;
+    }
+
+    return false;
   }
 
   static nsString PseudoTypeAsString(Type aPseudoType);
 
 private:
   // Does the given pseudo-element have all of the flags given?
 
   // Work around https://gcc.gnu.org/bugzilla/show_bug.cgi?id=64037 ,
diff --git a/layout/style/test/mochitest.ini b/layout/style/test/mochitest.ini
--- a/layout/style/test/mochitest.ini
+++ b/layout/style/test/mochitest.ini
@@ -247,16 +247,17 @@ skip-if = !stylo
 [test_media_queries.html]
 skip-if = android_version == '18' #debug-only failure; timed out #Android 4.3 aws only; bug 1030419
 [test_media_queries_dynamic.html]
 [test_media_queries_dynamic_xbl.html]
 [test_media_query_list.html]
 [test_media_query_serialization.html]
 [test_moz_device_pixel_ratio.html]
 [test_namespace_rule.html]
+[test_non_content_accessible_pseudos.html]
 [test_of_type_selectors.xhtml]
 [test_overscroll_behavior_pref.html]
 [test_page_parser.html]
 [test_parse_eof.html]
 [test_parse_ident.html]
 [test_parse_rule.html]
 [test_parse_url.html]
 [test_parser_diagnostics_unprintables.html]
diff --git a/layout/style/test/test_non_content_accessible_pseudos.html b/layout/style/test/test_non_content_accessible_pseudos.html
new file mode 100644
--- /dev/null
+++ b/layout/style/test/test_non_content_accessible_pseudos.html
@@ -0,0 +1,57 @@
+<!doctype html>
+<script src="/resources/testharness.js"></script>
+<script src="/resources/testharnessreport.js"></script>
+<style id="sheet"></style>
+<script>
+const NON_CONTENT_ACCESIBLE_PSEUDOS = [
+  "::-moz-number-wrapper",
+  "::-moz-number-spin-up",
+  "::-moz-number-spin-down",
+  "::-moz-number-spin-box",
+  "::-moz-number-text",
+  // FIXME: probably ::-moz-math-anonymous should be on this list...
+
+  ":-moz-native-anonymous",
+  ":-moz-use-shadow-tree-root",
+  ":-moz-table-border-nonzero",
+  ":-moz-browser-frame",
+  ":-moz-devtools-highlighted",
+  ":-moz-styleeditor-transitioning",
+  ":-moz-user-disabled",
+  ":-moz-handler-clicktoplay",
+  ":-moz-handler-vulnerable-updatable",
+  ":-moz-handler-vulnerable-no-update",
+  ":-moz-handler-disabled",
+  ":-moz-handler-blocked",
+  ":-moz-handler-chrased",
+  ":-moz-has-dir-attr",
+  ":-moz-dir-attr-ltr",
+  ":-moz-dir-attr-rtl",
+  ":-moz-dir-attr-like-auto",
+  ":-moz-autofill",
+  ":-moz-autofill-preview",
+];
+
+test(function() {
+  sheet.textContent = `div { color: initial }`;
+  assert_equals(sheet.sheet.cssRules.length, 1);
+}, "sanity");
+
+for (const pseudo of NON_CONTENT_ACCESIBLE_PSEUDOS) {
+  test(function() {
+    sheet.textContent = `${pseudo} { color: blue; }`;
+    assert_equals(sheet.sheet.cssRules.length, 0,
+      pseudo + " shouldn't be accessible to content");
+    // FIXME(emilio, bug 1433439): This test should pass!
+    //
+    // if (pseudo.indexOf("::") === 0) {
+    //   let pseudoStyle = getComputedStyle(document.documentElement, pseudo);
+    //   let elementStyle = getComputedStyle(document.documentElement);
+    //   for (prop of pseudoStyle) {
+    //     assert_equals(pseudoStyle[prop], elementStyle[prop],
+    //                   pseudo + " styles shouldn't be visible from getComputedStyle");
+    //   }
+    // }
+  }, pseudo);
+}
+</script>
