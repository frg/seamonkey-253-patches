# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1578690256 0
# Node ID 065106e36c922bdb2d073d3bcc0ef3be2a978e83
# Parent  46102e1993b55d87d5f40d90563cf9e035fae512
Bug 1607345 - mozbuild/configure/options.py supports Python 3 r=ahal

Differential Revision: https://phabricator.services.mozilla.com/D58867

diff --git a/python/mozbuild/mozbuild/configure/options.py b/python/mozbuild/mozbuild/configure/options.py
--- a/python/mozbuild/mozbuild/configure/options.py
+++ b/python/mozbuild/mozbuild/configure/options.py
@@ -258,17 +258,17 @@ class Option(object):
                 raise InvalidOptionError(
                     "The given `default` doesn't satisfy `nargs`")
             if has_choices and not all(d in choices for d in self.default):
                 raise InvalidOptionError(
                     'The `default` value must be one of %s' %
                     ', '.join("'%s'" % c for c in choices))
         elif has_choices:
             maxargs = self.maxargs
-            if len(choices) < maxargs and maxargs != sys.maxint:
+            if len(choices) < maxargs and maxargs != sys.maxsize:
                 raise InvalidOptionError('Not enough `choices` for `nargs`')
         self.choices = choices
         self.help = help
 
     @staticmethod
     def split_option(option):
         '''Split a flag or variable into a prefix, a name and values
 
@@ -325,17 +325,17 @@ class Option(object):
         if isinstance(self.nargs, int):
             return self.nargs
         return 1 if self.nargs == '+' else 0
 
     @property
     def maxargs(self):
         if isinstance(self.nargs, int):
             return self.nargs
-        return 1 if self.nargs == '?' else sys.maxint
+        return 1 if self.nargs == '?' else sys.maxsize
 
     def _validate_nargs(self, num):
         minargs, maxargs = self.minargs, self.maxargs
         return num >= minargs and num <= maxargs
 
     def get_value(self, option=None, origin='unknown'):
         '''Given a full command line option (e.g. --enable-foo=bar) or a
         variable assignment (FOO=bar), returns the corresponding OptionValue.
diff --git a/python/mozbuild/mozbuild/test/configure/test_options.py b/python/mozbuild/mozbuild/test/configure/test_options.py
--- a/python/mozbuild/mozbuild/test/configure/test_options.py
+++ b/python/mozbuild/mozbuild/test/configure/test_options.py
@@ -63,77 +63,77 @@ class TestOption(unittest.TestCase):
         option = Option(env='MOZ_OPTION')
         self.assertEquals(option.prefix, '')
         self.assertEquals(option.name, None)
         self.assertEquals(option.env, 'MOZ_OPTION')
         self.assertFalse(option.default)
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs=0, default=('a',))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs=1, default=())
         self.assertEquals(
-            e.exception.message,
+            str(e.exception),
             'default must be a bool, a string or a tuple of strings')
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs=1, default=True)
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs=1, default=('a', 'b'))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs=2, default=())
         self.assertEquals(
-            e.exception.message,
+            str(e.exception),
             'default must be a bool, a string or a tuple of strings')
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs=2, default=True)
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs=2, default=('a',))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs='?', default=('a', 'b'))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs='+', default=())
         self.assertEquals(
-            e.exception.message,
+            str(e.exception),
             'default must be a bool, a string or a tuple of strings')
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs='+', default=True)
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
         # --disable options with a nargs value that requires at least one
         # argument need to be given a default.
         with self.assertRaises(InvalidOptionError) as e:
             Option('--disable-option', nargs=1)
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--disable-option', nargs='+')
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
         # Test nargs inference from default value
         option = Option('--with-foo', default=True)
         self.assertEquals(option.nargs, 0)
 
         option = Option('--with-foo', default=False)
         self.assertEquals(option.nargs, 0)
@@ -183,58 +183,58 @@ class TestOption(unittest.TestCase):
                               option.replace('-enable-', '-disable-')
                                     .replace('-with-', '-without-'))
 
         self.assertEquals(Option(env='FOO').option, 'FOO')
 
     def test_option_choices(self):
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs=3, choices=('a', 'b'))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           'Not enough `choices` for `nargs`')
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--without-option', nargs=1, choices=('a', 'b'))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           'A `default` must be given along with `choices`')
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--without-option', nargs='+', choices=('a', 'b'))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           'A `default` must be given along with `choices`')
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--without-option', default='c', choices=('a', 'b'))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The `default` value must be one of 'a', 'b'")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--without-option', default=('a', 'c',), choices=('a', 'b'))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The `default` value must be one of 'a', 'b'")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--without-option', default=('c',), choices=('a', 'b'))
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The `default` value must be one of 'a', 'b'")
 
         option = Option('--with-option', nargs='+', choices=('a', 'b'))
         with self.assertRaises(InvalidOptionError) as e:
             option.get_value('--with-option=c')
-        self.assertEquals(e.exception.message, "'c' is not one of 'a', 'b'")
+        self.assertEquals(str(e.exception), "'c' is not one of 'a', 'b'")
 
         value = option.get_value('--with-option=b,a')
         self.assertTrue(value)
         self.assertEquals(PositiveOptionValue(('b', 'a')), value)
 
         option = Option('--without-option', nargs='*', default='a',
                         choices=('a', 'b'))
         with self.assertRaises(InvalidOptionError) as e:
             option.get_value('--with-option=c')
-        self.assertEquals(e.exception.message, "'c' is not one of 'a', 'b'")
+        self.assertEquals(str(e.exception), "'c' is not one of 'a', 'b'")
 
         value = option.get_value('--with-option=b,a')
         self.assertTrue(value)
         self.assertEquals(PositiveOptionValue(('b', 'a')), value)
 
         # Test nargs inference from choices
         option = Option('--with-option', choices=('a', 'b'))
         self.assertEqual(option.nargs, 1)
@@ -258,28 +258,28 @@ class TestOption(unittest.TestCase):
 
         # Removing something that is not in the default is fine, as long as it
         # is one of the choices
         value = option.get_value('--with-option=-a')
         self.assertEquals(PositiveOptionValue(('b', 'c')), value)
 
         with self.assertRaises(InvalidOptionError) as e:
             option.get_value('--with-option=-e')
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "'e' is not one of 'a', 'b', 'c', 'd'")
 
         # Other "not a choice" errors.
         with self.assertRaises(InvalidOptionError) as e:
             option.get_value('--with-option=+e')
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "'e' is not one of 'a', 'b', 'c', 'd'")
 
         with self.assertRaises(InvalidOptionError) as e:
             option.get_value('--with-option=e')
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "'e' is not one of 'a', 'b', 'c', 'd'")
 
     def test_option_value_compare(self):
         # OptionValue are tuple and equivalence should compare as tuples.
         val = PositiveOptionValue(('foo',))
 
         self.assertEqual(val[0], 'foo')
         self.assertEqual(val, PositiveOptionValue(('foo',)))
@@ -360,23 +360,23 @@ class TestOption(unittest.TestCase):
         if nargs in (0, '?', '*') or disabled:
             value = option.get_value('--%s' % name, 'option')
             self.assertEquals(value, posOptionValue())
             self.assertEquals(value.origin, 'option')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('--%s' % name)
             if nargs == 1:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes 1 value' % name)
             elif nargs == '+':
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes 1 or more values' % name)
             else:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes 2 values' % name)
 
         value = option.get_value('')
         self.assertEquals(value, defaultValue)
         self.assertEquals(value.origin, 'default')
 
         value = option.get_value(None)
         self.assertEquals(value, defaultValue)
@@ -394,71 +394,71 @@ class TestOption(unittest.TestCase):
         if nargs in (1, '?', '*', '+') and not disabled:
             value = option.get_value('--%s=' % name, 'option')
             self.assertEquals(value, PositiveOptionValue(('',)))
             self.assertEquals(value.origin, 'option')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('--%s=' % name)
             if disabled:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   'Cannot pass a value to --%s' % name)
             else:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes %d values' % (name, nargs))
 
         if nargs in (1, '?', '*', '+') and not disabled:
             value = option.get_value('--%s=foo' % name, 'option')
             self.assertEquals(value, PositiveOptionValue(('foo',)))
             self.assertEquals(value.origin, 'option')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('--%s=foo' % name)
             if disabled:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   'Cannot pass a value to --%s' % name)
             else:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes %d values' % (name, nargs))
 
         if nargs in (2, '*', '+') and not disabled:
             value = option.get_value('--%s=foo,bar' % name, 'option')
             self.assertEquals(value, PositiveOptionValue(('foo', 'bar')))
             self.assertEquals(value.origin, 'option')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('--%s=foo,bar' % name, 'option')
             if disabled:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   'Cannot pass a value to --%s' % name)
             elif nargs == '?':
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes 0 or 1 values' % name)
             else:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes %d value%s'
                                   % (name, nargs, 's' if nargs != 1 else ''))
 
         option = Option('--%s' % name, env='MOZ_OPTION', nargs=nargs,
                         default=default)
         if nargs in (0, '?', '*') or disabled:
             value = option.get_value('--%s' % name, 'option')
             self.assertEquals(value, posOptionValue())
             self.assertEquals(value.origin, 'option')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('--%s' % name)
             if disabled:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   'Cannot pass a value to --%s' % name)
             elif nargs == '+':
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes 1 or more values' % name)
             else:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes %d value%s'
                                   % (name, nargs, 's' if nargs != 1 else ''))
 
         value = option.get_value('')
         self.assertEquals(value, defaultValue)
         self.assertEquals(value.origin, 'default')
 
         value = option.get_value(None)
@@ -475,57 +475,57 @@ class TestOption(unittest.TestCase):
             self.assertEquals(value.origin, 'environment')
         elif nargs in (1, '+'):
             value = option.get_value('MOZ_OPTION=1', 'environment')
             self.assertEquals(value, PositiveOptionValue(('1',)))
             self.assertEquals(value.origin, 'environment')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('MOZ_OPTION=1', 'environment')
-            self.assertEquals(e.exception.message, 'MOZ_OPTION takes 2 values')
+            self.assertEquals(str(e.exception), 'MOZ_OPTION takes 2 values')
 
         if nargs in (1, '?', '*', '+') and not disabled:
             value = option.get_value('--%s=' % name, 'option')
             self.assertEquals(value, PositiveOptionValue(('',)))
             self.assertEquals(value.origin, 'option')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('--%s=' % name, 'option')
             if disabled:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   'Cannot pass a value to --%s' % name)
             else:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s takes %d values' % (name, nargs))
 
         with self.assertRaises(AssertionError):
             value = option.get_value('--foo', 'option')
 
         if nargs in (1, '?', '*', '+'):
             value = option.get_value('MOZ_OPTION=foo', 'environment')
             self.assertEquals(value, PositiveOptionValue(('foo',)))
             self.assertEquals(value.origin, 'environment')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('MOZ_OPTION=foo', 'environment')
-            self.assertEquals(e.exception.message,
+            self.assertEquals(str(e.exception),
                               'MOZ_OPTION takes %d values' % nargs)
 
         if nargs in (2, '*', '+'):
             value = option.get_value('MOZ_OPTION=foo,bar', 'environment')
             self.assertEquals(value, PositiveOptionValue(('foo', 'bar')))
             self.assertEquals(value.origin, 'environment')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('MOZ_OPTION=foo,bar', 'environment')
             if nargs == '?':
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   'MOZ_OPTION takes 0 or 1 values')
             else:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   'MOZ_OPTION takes %d value%s'
                                   % (nargs, 's' if nargs != 1 else ''))
 
         if disabled:
             return option
 
         env_option = Option(env='MOZ_OPTION', nargs=nargs, default=default)
         with self.assertRaises(AssertionError):
@@ -546,46 +546,46 @@ class TestOption(unittest.TestCase):
             self.assertEquals(value.origin, 'environment')
         elif nargs in (1, '+'):
             value = env_option.get_value('MOZ_OPTION=1', 'environment')
             self.assertEquals(value, PositiveOptionValue(('1',)))
             self.assertEquals(value.origin, 'environment')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 env_option.get_value('MOZ_OPTION=1', 'environment')
-            self.assertEquals(e.exception.message, 'MOZ_OPTION takes 2 values')
+            self.assertEquals(str(e.exception), 'MOZ_OPTION takes 2 values')
 
         with self.assertRaises(AssertionError) as e:
             env_option.get_value('--%s' % name)
 
         with self.assertRaises(AssertionError) as e:
             env_option.get_value('--foo')
 
         if nargs in (1, '?', '*', '+'):
             value = env_option.get_value('MOZ_OPTION=foo', 'environment')
             self.assertEquals(value, PositiveOptionValue(('foo',)))
             self.assertEquals(value.origin, 'environment')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 env_option.get_value('MOZ_OPTION=foo', 'environment')
-            self.assertEquals(e.exception.message,
+            self.assertEquals(str(e.exception),
                               'MOZ_OPTION takes %d values' % nargs)
 
         if nargs in (2, '*', '+'):
             value = env_option.get_value('MOZ_OPTION=foo,bar', 'environment')
             self.assertEquals(value, PositiveOptionValue(('foo', 'bar')))
             self.assertEquals(value.origin, 'environment')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 env_option.get_value('MOZ_OPTION=foo,bar', 'environment')
             if nargs == '?':
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   'MOZ_OPTION takes 0 or 1 values')
             else:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   'MOZ_OPTION takes %d value%s'
                                   % (nargs, 's' if nargs != 1 else ''))
 
         return option
 
     def test_option_value_enable(self, enable='enable', disable='disable',
                                  nargs=0, default=None):
         option = self.test_option_value('%s-option' % enable, nargs=nargs,
@@ -601,60 +601,60 @@ class TestOption(unittest.TestCase):
         if nargs in (0, '?', '*'):
             value = option.get_value('--%s-option' % enable, 'option')
             self.assertEquals(value, PositiveOptionValue())
             self.assertEquals(value.origin, 'option')
         else:
             with self.assertRaises(InvalidOptionError) as e:
                 option.get_value('--%s-option' % enable, 'option')
             if nargs == 1:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s-option takes 1 value' % enable)
             elif nargs == '+':
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s-option takes 1 or more values'
                                   % enable)
             else:
-                self.assertEquals(e.exception.message,
+                self.assertEquals(str(e.exception),
                                   '--%s-option takes 2 values' % enable)
 
     def test_option_value_with(self):
         self.test_option_value_enable('with', 'without')
 
     def test_option_value_invalid_nargs(self):
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs='foo')
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "nargs must be a positive integer, '?', '*' or '+'")
 
         with self.assertRaises(InvalidOptionError) as e:
             Option('--option', nargs=-2)
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "nargs must be a positive integer, '?', '*' or '+'")
 
     def test_option_value_nargs_1(self):
         self.test_option_value(nargs=1)
         self.test_option_value(nargs=1, default=('a',))
         self.test_option_value_enable(nargs=1, default=('a',))
 
         # A default is required
         with self.assertRaises(InvalidOptionError) as e:
             Option('--disable-option', nargs=1)
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
     def test_option_value_nargs_2(self):
         self.test_option_value(nargs=2)
         self.test_option_value(nargs=2, default=('a', 'b'))
         self.test_option_value_enable(nargs=2, default=('a', 'b'))
 
         # A default is required
         with self.assertRaises(InvalidOptionError) as e:
             Option('--disable-option', nargs=2)
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
     def test_option_value_nargs_0_or_1(self):
         self.test_option_value(nargs='?')
         self.test_option_value(nargs='?', default=('a',))
         self.test_option_value_enable(nargs='?')
         self.test_option_value_enable(nargs='?', default=('a',))
 
@@ -671,17 +671,17 @@ class TestOption(unittest.TestCase):
         self.test_option_value(nargs='+', default=('a',))
         self.test_option_value(nargs='+', default=('a', 'b'))
         self.test_option_value_enable(nargs='+', default=('a',))
         self.test_option_value_enable(nargs='+', default=('a', 'b'))
 
         # A default is required
         with self.assertRaises(InvalidOptionError) as e:
             Option('--disable-option', nargs='+')
-        self.assertEquals(e.exception.message,
+        self.assertEquals(str(e.exception),
                           "The given `default` doesn't satisfy `nargs`")
 
 
 class TestCommandLineHelper(unittest.TestCase):
     def test_basic(self):
         helper = CommandLineHelper({}, ['cmd', '--foo', '--bar'])
 
         self.assertEquals(['--foo', '--bar'], list(helper))
diff --git a/python/mozbuild/mozbuild/test/python.ini b/python/mozbuild/mozbuild/test/python.ini
--- a/python/mozbuild/mozbuild/test/python.ini
+++ b/python/mozbuild/mozbuild/test/python.ini
@@ -1,11 +1,12 @@
 [DEFAULT]
 subsuite = mozbuild
 
+[configure/test_options.py]
 [configure/test_util.py]
 [controller/test_ccachestats.py]
 [controller/test_clobber.py]
 [test_artifacts.py]
 [test_base.py]
 [test_containers.py]
 [test_dotproperties.py]
 [test_expression.py]
diff --git a/python/mozbuild/mozbuild/test/python2.ini b/python/mozbuild/mozbuild/test/python2.ini
--- a/python/mozbuild/mozbuild/test/python2.ini
+++ b/python/mozbuild/mozbuild/test/python2.ini
@@ -15,17 +15,16 @@ subsuite = mozbuild
 [codecoverage/test_lcov_rewrite.py]
 [compilation/test_warnings.py]
 [configure/lint.py]
 [configure/test_checks_configure.py]
 [configure/test_compile_checks.py]
 [configure/test_configure.py]
 [configure/test_lint.py]
 [configure/test_moz_configure.py]
-[configure/test_options.py]
 [configure/test_toolchain_configure.py]
 [configure/test_toolchain_helpers.py]
 [configure/test_toolkit_moz_configure.py]
 [frontend/test_context.py]
 [frontend/test_emitter.py]
 [frontend/test_namespaces.py]
 [frontend/test_reader.py]
 [frontend/test_sandbox.py]
