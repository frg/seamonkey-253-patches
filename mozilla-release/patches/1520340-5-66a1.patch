# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1547680340 0
# Node ID 5e51e7031e8f239efd765328071a21b877e1d814
# Parent  907a24a30c73be0770183db159d9b5f3bbaa075b
Bug 1520340 - Move subconfigure invocation from old-configure to python configure. r=froydnj

This happens to remove the last use of perl from configure.

Depends on D16621

Differential Revision: https://phabricator.services.mozilla.com/D16622

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -2,22 +2,24 @@
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 include('util.configure')
 include('checks.configure')
 
+# Make `toolkit` available when toolkit/moz.configure is not included.
+toolkit = dependable(None)
+
 option(env='DIST', nargs=1, help='DIST directory')
 
+
 # Do not allow objdir == srcdir builds.
 # ==============================================================
-
-
 @depends('--help', 'DIST')
 @imports(_from='__builtin__', _import='open')
 @imports(_from='os.path', _import='exists')
 def check_build_environment(help, dist):
     topobjdir = os.path.realpath(os.path.abspath('.'))
     topsrcdir = os.path.realpath(os.path.abspath(
         os.path.join(os.path.dirname(__file__), '..', '..')))
 
diff --git a/build/subconfigure.py b/build/subconfigure.py
--- a/build/subconfigure.py
+++ b/build/subconfigure.py
@@ -98,46 +98,26 @@ def prepare(srcdir, objdir, args):
 
     data_file = os.path.join(objdir, CONFIGURE_DATA)
     previous_args = None
     if os.path.exists(data_file):
         with open(data_file, 'rb') as f:
             data = pickle.load(f)
             previous_args = data['args']
 
-    # Msys likes to break environment variables and command line arguments,
-    # so read those from stdin, as they are passed from the configure script
-    # when necessary (on windows).
-    input = sys.stdin.read()
-    if input:
-        data = {a: b for [a, b] in eval(input)}
-        environ = {a: b for a, b in data['env']}
-        # These environment variables as passed from old-configure may contain
-        # posix-style paths, which will not be meaningful to the js
-        # subconfigure, which runs as a native python process, so use their
-        # values from the environment. In the case of autoconf implemented
-        # subconfigures, Msys will re-convert them properly.
-        for var in ('HOME', 'TERM', 'PATH', 'TMPDIR', 'TMP',
-                    'TEMP', 'INCLUDE'):
-            if var in environ and var in os.environ:
-                environ[var] = os.environ[var]
-        args = data['args']
-    else:
-        environ = os.environ
-
     args, others = parser.parse_known_args(args)
 
     data = {
         'target': args.target,
         'host': args.host,
         'build': args.build,
         'args': others,
         'srcdir': srcdir,
         'objdir': objdir,
-        'env': environ,
+        'env': os.environ,
     }
 
     if args.cache_file:
         data['cache-file'] = mozpath.normpath(mozpath.join(os.getcwd(),
                                                            args.cache_file))
     else:
         data['cache-file'] = mozpath.join(objdir, 'config.cache')
 
diff --git a/build/win32/dumpenv4python.pl b/build/win32/dumpenv4python.pl
deleted file mode 100644
--- a/build/win32/dumpenv4python.pl
+++ /dev/null
@@ -1,19 +0,0 @@
-# This Source Code Form is subject to the terms of the Mozilla Public
-# License, v. 2.0. If a copy of the MPL was not distributed with this
-# file, You can obtain one at http://mozilla.org/MPL/2.0/.
-
-# See build/autoconf/hooks.m4
-
-use Data::Dumper;
-
-$Data::Dumper::Terse = 1;
-$Data::Dumper::Indent = 0;
-
-# We can't use perl hashes because Mozilla-Build's perl is 5.6, and perl
-# 5.6's Data::Dumper doesn't have Pair to change ' => ' into ' : '.
-@data = (
-  ['env', [map { [$_, $ENV{$_}] } keys %ENV]],
-  ['args', \@ARGV],
-);
-
-print Dumper \@data;
diff --git a/js/src/make-source-package.sh.1520340.later b/js/src/make-source-package.sh.1520340.later
new file mode 100644
--- /dev/null
+++ b/js/src/make-source-package.sh.1520340.later
@@ -0,0 +1,22 @@
+--- make-source-package.sh
++++ make-source-package.sh
+@@ -178,18 +178,17 @@ case $cmd in
+         ${tgtpath}/toolkit/mozapps/installer/
+ 
+ 
+     # SpiderMonkey itself
+ 
+     ${MKDIR} -p ${tgtpath}/js/src
+     cp -pPR \
+         ${TOPSRCDIR}/js/app.mozbuild \
+-        ${TOPSRCDIR}/js/ffi.configure \
+-        ${TOPSRCDIR}/js/moz.configure \
++        ${TOPSRCDIR}/js/*.configure \
+         ${tgtpath}/js/
+     cp -pPR \
+         ${TOPSRCDIR}/js/examples \
+         ${TOPSRCDIR}/js/public \
+         ${TOPSRCDIR}/js/rust \
+         ${tgtpath}/js/
+ 
+     find ${TOPSRCDIR}/js/src -mindepth 1 -maxdepth 1 -not -path ${STAGING} -a -not -name ${pkg} \
diff --git a/js/sub.configure b/js/sub.configure
new file mode 100644
--- /dev/null
+++ b/js/sub.configure
@@ -0,0 +1,92 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+@depends(check_build_environment, prepare_configure_options, prepare_mozconfig,
+         old_configure, old_configure_assignments, '--cache-file')
+@imports('itertools')
+@imports('os')
+@imports('subprocess')
+@imports('sys')
+@imports(_from='mozbuild.util', _import='encode')
+def js_subconfigure(build_env, prepare_configure_options, mozconfig,
+                    old_configure, old_configure_assignments, cache_file):
+    substs = dict(old_configure['substs'])
+    assignments = dict(old_configure_assignments)
+    environ = dict(os.environ)
+    if prepare_configure_options.extra_env:
+        environ.update(prepare_configure_options.extra_env)
+
+    options = [
+        o for o in prepare_configure_options.options
+        # --with-system-nspr will have been converted into the relevant $NSPR_CFLAGS
+        # and $NSPR_LIBS.
+        if not o.startswith('--with-system-nspr')
+    ]
+
+    if not substs.get('ENABLE_INTL_API'):
+        options.append('--without-intl-api')
+
+    if substs.get('NSPR_CFLAGS') or substs.get('NSPR_LIBS'):
+        options.append(
+            '--with-nspr-cflags=%s' % ' '.join(substs.get('NSPR_CFLAGS', [])))
+        options.append(
+            '--with-nspr-libs=%s' % ' '.join(substs.get('NSPR_LIBS', [])))
+
+    options.append('--prefix=%s/dist' % build_env.topobjdir)
+
+    if substs.get('ZLIB_IN_MOZGLUE'):
+        substs['MOZ_ZLIB_LIBS'] = ''
+
+    environ['MOZILLA_CENTRAL_PATH'] = build_env.topsrcdir
+    if 'MOZ_BUILD_APP' in environ:
+        del environ['MOZ_BUILD_APP']
+
+    # Here, we mimic what we used to do from old-configure, which makes this
+    # all awkward.
+
+    # The following variables were saved at the beginning of old-configure,
+    # and restored before invoking the subconfigure. Which means their value
+    # should be taken from the old_configure_assignments or mozconfig.
+    from_assignment = set(
+        ('CC', 'CXX', 'CPPFLAGS', 'CFLAGS', 'CXXFLAGS', 'LDFLAGS', 'HOST_CC',
+         'HOST_CXXFLAGS', 'HOST_LDFLAGS'))
+
+    # Variables that were explicitly exported from old-configure, and those
+    # explicitly set in the environment when invoking old-configure, were
+    # automatically inherited from subconfigure. We assume the relevant ones
+    # have a corresponding AC_SUBST in old-configure, making them available
+    # in `substs`.
+    for var in itertools.chain((
+        'MOZ_SYSTEM_ZLIB', 'MOZ_ZLIB_CFLAGS', 'MOZ_ZLIB_LIBS',
+        'MOZ_APP_NAME', 'MOZ_APP_REMOTINGNAME', 'MOZ_DEV_EDITION',
+        'STLPORT_LIBS', 'DIST', 'MOZ_LINKER', 'ZLIB_IN_MOZGLUE', 'RANLIB',
+        'AR', 'CPP', 'CC', 'CXX', 'CPPFLAGS', 'CFLAGS', 'CXXFLAGS',
+        'LDFLAGS', 'HOST_CC', 'HOST_CXX', 'HOST_CPPFLAGS',
+        'HOST_CXXFLAGS', 'HOST_LDFLAGS'
+    ), prepare_configure_options.extra_env):
+        if var not in from_assignment and var in substs:
+            value = substs[var]
+        elif var in assignments:
+            value = assignments[var]
+        elif mozconfig and var in mozconfig and \
+                not mozconfig[var][1].startswith('removed'):
+            value = mozconfig[var][0]
+        else:
+            continue
+        if isinstance(value, list):
+            value = ' '.join(value)
+        environ[var] = value
+
+    ret = subprocess.call([
+        sys.executable,
+        os.path.join(build_env.topsrcdir, 'build', 'subconfigure.py'),
+        build_env.topsrcdir
+    ] + options + [
+        'JS_STANDALONE=',
+        '--cache-file=%s' % (cache_file or './config.cache'),
+    ], env=encode(environ))
+
+    if ret:
+        log.error('subconfigure failed')
+        sys.exit(ret)
diff --git a/moz.configure b/moz.configure
--- a/moz.configure
+++ b/moz.configure
@@ -584,19 +584,25 @@ def nsis_flags(host):
     if host.kernel != 'WINNT':
         return '-nocd'
     return ''
 
 set_config('MAKENSISU_FLAGS', nsis_flags)
 
 check_prog('7Z', ('7z', '7za'), allow_missing=True, when=target_is_windows)
 
+# Please do not add configure checks from here on.
+
 # Fallthrough to autoconf-based configure
 include('build/moz.configure/old.configure')
 
+# JS Subconfigure.
+include('js/sub.configure', when=compile_environment & toolkit)
+
+
 @depends(check_build_environment, build_project)
 @imports('__sandbox__')
 @imports('glob')
 def config_status_deps(build_env, build_project):
 
     topsrcdir = build_env.topsrcdir
     topobjdir = build_env.topobjdir
 
diff --git a/old-configure.in b/old-configure.in
--- a/old-configure.in
+++ b/old-configure.in
@@ -3565,106 +3565,9 @@ HAVE_SYS_VFS_H
 HAVE_SYS_MOUNT_H
 "
 
 # Avoid using obsolete NSPR features
 AC_DEFINE(NO_NSPR_10_SUPPORT)
 
 MOZ_CREATE_CONFIG_STATUS()
 
-unset MAKEFILES
-unset CONFIG_FILES
-
-if test "$COMPILE_ENVIRONMENT" -a "$MOZ_WIDGET_TOOLKIT"; then
-
-
-dnl ========================================================
-dnl = Setup a nice relatively clean build environment for
-dnl = sub-configures.
-dnl ========================================================
-CC="$_SUBDIR_CC"
-CXX="$_SUBDIR_CXX"
-CFLAGS="$_SUBDIR_CFLAGS"
-CPPFLAGS="$_SUBDIR_CPPFLAGS"
-CXXFLAGS="$_SUBDIR_CXXFLAGS"
-LDFLAGS="$_SUBDIR_LDFLAGS"
-HOST_CC="$_SUBDIR_HOST_CC"
-HOST_CFLAGS="$_SUBDIR_HOST_CFLAGS"
-HOST_CXXFLAGS="$_SUBDIR_HOST_CXXFLAGS"
-HOST_LDFLAGS="$_SUBDIR_HOST_LDFLAGS"
-
-# Run the SpiderMonkey 'configure' script.
-dist=$MOZ_BUILD_ROOT/dist
-ac_configure_args="$_SUBDIR_CONFIG_ARGS"
-
-# --with-system-nspr will have been converted into the relevant $NSPR_CFLAGS
-# and $NSPR_LIBS.
-ac_configure_args="`echo $ac_configure_args | sed -e 's/--with-system-nspr[[^[:space:]]]* *//'`"
-
-if test "$_INTL_API" = no; then
-    ac_configure_args="$ac_configure_args --without-intl-api"
-fi
-
-if test -n "$NSPR_CFLAGS" -o -n "$NSPR_LIBS"; then
-    ac_configure_args="$ac_configure_args --with-nspr-cflags='$NSPR_CFLAGS'"
-    ac_configure_args="$ac_configure_args --with-nspr-libs='$NSPR_LIBS'"
-fi
-ac_configure_args="$ac_configure_args --prefix=$dist"
-if test -n "$ZLIB_IN_MOZGLUE"; then
-   MOZ_ZLIB_LIBS=
-fi
-export MOZ_SYSTEM_ZLIB
-export MOZ_ZLIB_CFLAGS
-export MOZ_ZLIB_LIBS
-export MOZ_APP_NAME
-export MOZ_APP_REMOTINGNAME
-export MOZ_DEV_EDITION
-export MOZILLA_CENTRAL_PATH=$_topsrcdir
-export STLPORT_LIBS
-unset MOZ_BUILD_APP
-export DIST
-export MOZ_LINKER
-export ZLIB_IN_MOZGLUE
-export AR
-export RANLIB
-export CPP
-export CC
-export CXX
-export ARFLAGS
-export CPPFLAGS
-export CFLAGS
-export CXXFLAGS
-export LDFLAGS
-export HOST_CC
-export HOST_CXX
-export HOST_CFLAGS
-export HOST_CPPFLAGS
-export HOST_CXXFLAGS
-export HOST_LDFLAGS
-
-if ! test -e js; then
-    mkdir js
-fi
-
-ac_configure_args="$ac_configure_args JS_STANDALONE="
-
-dumpenv="true | "
-case "$host" in
-*-mingw*)
-  dnl Yes, this is horrible. But since msys doesn't preserve environment
-  dnl variables and command line arguments as they are when transitioning
-  dnl from msys (this script) to python (below), we have to resort to hacks,
-  dnl storing the environment and command line arguments from a msys process
-  dnl (perl), and reading it from python.
-  dumpenv="$PERL $_topsrcdir/build/win32/dumpenv4python.pl $ac_configure_args | "
-  ;;
-esac
-
-trap '' EXIT
-eval $dumpenv $PYTHON $_topsrcdir/build/subconfigure.py "$srcdir" $ac_configure_args --cache-file="$cache_file"
-if test $? -ne 0; then
-  exit 1
-fi
-ac_configure_args="$_SUBDIR_CONFIG_ARGS"
-
-fi # COMPILE_ENVIRONMENT
-
 rm -fr confdefs* $ac_clean_files
