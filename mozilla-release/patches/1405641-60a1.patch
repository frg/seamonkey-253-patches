# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1518196168 -3600
# Node ID 84ef4e4f8804ebf928e22eb84a54b7454b8387f1
# Parent  61294be866e753c76194c8b49917fbb18bda5c8d
Bug 1405641 - Enable browser_webconsole_output_copy_newlines.js in new console frontend; r=jdescottes.

MozReview-Commit-ID: 8KtpDUXpuj7

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -319,18 +319,16 @@ skip-if = true #       Bug 1403448
 [browser_webconsole_object_inspector.js]
 [browser_webconsole_object_inspector_entries.js]
 [browser_webconsole_observer_notifications.js]
 [browser_webconsole_optimized_out_vars.js]
 [browser_webconsole_output_copy.js]
 subsuite = clipboard
 [browser_webconsole_output_copy_newlines.js]
 subsuite = clipboard
-skip-if = true #       Bug 1405641
-# old console skip-if = (os == 'linux' && bits == 32 && debug) # bug 1328915, disable linux32 debug devtools for timeouts
 [browser_webconsole_output_order.js]
 [browser_webconsole_persist.js]
 [browser_webconsole_prune_scroll.js]
 skip-if = true #       Bug 1404832
 [browser_webconsole_reopen_closed_tab.js]
 [browser_webconsole_repeat_different_objects.js]
 [browser_webconsole_repeated_messages_accuracy.js]
 skip-if = true #       Bug 1403450
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_output_copy_newlines.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_output_copy_newlines.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_output_copy_newlines.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_output_copy_newlines.js
@@ -1,72 +1,42 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
-// Test that multiple messages are copied into the clipboard and that they are
-// separated by new lines. See bug 916997.
-
 "use strict";
 
-add_task(function* () {
-  const TEST_URI = "data:text/html;charset=utf8,<p>hello world, bug 916997";
-  let clipboardValue = "";
+// Test that multiple messages are copied into the clipboard and that they are
+// separated by new lines. See bug 916997.
+const TEST_URI = "data:text/html,<meta charset=utf8>" +
+  "Test copy multiple messages to clipboard";
 
-  yield loadTab(TEST_URI);
-  let hud = yield openConsole();
-  hud.jsterm.clearOutput();
-
-  let controller = top.document.commandDispatcher
-                   .getControllerForCommand("cmd_copy");
-  is(controller.isCommandEnabled("cmd_copy"), false, "cmd_copy is disabled");
-
-  content.console.log("Hello world! bug916997a");
-  content.console.log("Hello world 2! bug916997b");
+add_task(async function () {
+  let hud = await openNewTabAndConsole(TEST_URI);
 
-  yield waitForMessages({
-    webconsole: hud,
-    messages: [{
-      text: "Hello world! bug916997a",
-      category: CATEGORY_WEBDEV,
-      severity: SEVERITY_LOG,
-    }, {
-      text: "Hello world 2! bug916997b",
-      category: CATEGORY_WEBDEV,
-      severity: SEVERITY_LOG,
-    }],
+  const messages = Array.from({length: 10}, (_, i) => `Message number ${i + 1}`);
+  const lastMessage = [...messages].pop();
+  let onMessage = waitForMessage(hud, lastMessage);
+  ContentTask.spawn(gBrowser.selectedBrowser, messages, msgs => {
+    msgs.forEach(msg => content.wrappedJSObject.console.log(msg));
   });
+  const {node} = await onMessage;
+  ok(node, "Messages were logged");
 
-  hud.ui.output.selectAllMessages();
-  hud.outputNode.focus();
-
-  goUpdateCommand("cmd_copy");
-  controller = top.document.commandDispatcher
-               .getControllerForCommand("cmd_copy");
-  is(controller.isCommandEnabled("cmd_copy"), true, "cmd_copy is enabled");
+  // Select the whole output.
+  const output = node.closest(".webconsole-output");
+  const selection = node.ownerDocument.getSelection();
+  const range = document.createRange();
+  range.selectNodeContents(output);
+  selection.removeAllRanges();
+  selection.addRange(range);
 
-  let selection = hud.iframeWindow.getSelection() + "";
-  info("selection '" + selection + "'");
-
-  waitForClipboard((str) => {
-    clipboardValue = str;
-    return str.indexOf("bug916997a") > -1 && str.indexOf("bug916997b") > -1;
-  },
+  info("Wait for the clipboard to contain the text corresponding to all the messages");
+  await waitForClipboardPromise(
     () => {
+      // The focus is on the JsTerm, so we need to blur it for the copy comand to work.
+      output.ownerDocument.activeElement.blur();
       goDoCommand("cmd_copy");
     },
-    () => {
-      info("clipboard value '" + clipboardValue + "'");
-      let lines = clipboardValue.trim().split("\n");
-      is(hud.outputNode.children.length, 2, "number of messages");
-      is(lines.length, hud.outputNode.children.length, "number of lines");
-      isnot(lines[0].indexOf("bug916997a"), -1,
-            "first message text includes 'bug916997a'");
-      isnot(lines[1].indexOf("bug916997b"), -1,
-            "second message text includes 'bug916997b'");
-      is(lines[0].indexOf("bug916997b"), -1,
-         "first message text does not include 'bug916997b'");
-    },
-    () => {
-      info("last clipboard value: '" + clipboardValue + "'");
-    });
+    data => data.trim() === messages.join("\n")
+  );
 });
