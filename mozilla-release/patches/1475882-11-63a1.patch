# HG changeset patch
# User Chris Peterson <cpeterson@mozilla.com>
# Date 1531637320 25200
#      Sat Jul 14 23:48:40 2018 -0700
# Node ID 3a3725565d124f04d30100b094d0550552ec17cd
# Parent  ffa7c88ba619ab4c0bd18e237a54f753b6dd980f
Bug 1475882 - clang-tidy: Enable misc-string-integer-assignment check. r=andi

The check finds assignments of an integer to std::string:

https://clang.llvm.org/extra/clang-tidy/checks/bugprone-string-integer-assignment.html

There are currently ten misc-string-integer-assignment warnings in mozilla-central, but they are all in third-party Breakpad code:

toolkit/crashreporter/google-breakpad/src/processor/minidump.cc:306:17: warning: an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility
toolkit/crashreporter/google-breakpad/src/processor/minidump.cc:307:17: warning: an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility
toolkit/crashreporter/google-breakpad/src/processor/minidump.cc:309:17: warning: an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility
toolkit/crashreporter/google-breakpad/src/processor/minidump.cc:310:17: warning: an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility
toolkit/crashreporter/google-breakpad/src/processor/minidump.cc:311:17: warning: an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility
toolkit/crashreporter/google-breakpad/src/processor/minidump.cc:313:17: warning: an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility
toolkit/crashreporter/google-breakpad/src/processor/minidump.cc:314:17: warning: an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility
toolkit/crashreporter/google-breakpad/src/processor/minidump.cc:315:17: warning: an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility
toolkit/crashreporter/google-breakpad/src/processor/minidump.cc:316:17: warning: an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility

MozReview-Commit-ID: AUOyOuCzy1R

diff --git a/tools/clang-tidy/config.yaml b/tools/clang-tidy/config.yaml
--- a/tools/clang-tidy/config.yaml
+++ b/tools/clang-tidy/config.yaml
@@ -51,16 +51,18 @@ clang_checkers:
   - name: misc-forward-declaration-namespace
     # Name with clang tidy 6.0. We are currently using 5.0
     # - name: bugprone-forward-declaration-namespace
     publish: !!bool yes
   - name: misc-macro-repeated-side-effects
     publish: !!bool yes
   - name: misc-string-constructor
     publish: !!bool yes
+  - name: misc-string-integer-assignment
+    publish: !!bool yes
   - name: misc-suspicious-missing-comma
     publish: !!bool yes
   - name: misc-suspicious-semicolon
     publish: !!bool yes
   - name: misc-swapped-arguments
     publish: !!bool yes
   - name: misc-unused-alias-decls
     publish: !!bool yes
diff --git a/tools/clang-tidy/test/misc-string-integer-assignment.cpp b/tools/clang-tidy/test/misc-string-integer-assignment.cpp
new file mode 100644
--- /dev/null
+++ b/tools/clang-tidy/test/misc-string-integer-assignment.cpp
@@ -0,0 +1,28 @@
+// https://clang.llvm.org/extra/clang-tidy/checks/bugprone-string-integer-assignment.html
+
+#include "structures.h"
+
+void test_int()
+{
+  // Numeric types can be implicitly casted to character types.
+  std::string s;
+  int x = 5965;
+  s = 6; // warning
+  s = x; // warning
+}
+
+void test_conversion()
+{
+  // Use the appropriate conversion functions or character literals.
+  std::string s;
+  int x = 5965;
+  s = '6'; // OK
+  s = std::to_string(x); // OK
+}
+
+void test_cast()
+{
+  // In order to suppress false positives, use an explicit cast.
+  std::string s;
+  s = static_cast<char>(6); // OK
+}
diff --git a/tools/clang-tidy/test/misc-string-integer-assignment.json b/tools/clang-tidy/test/misc-string-integer-assignment.json
new file mode 100644
--- /dev/null
+++ b/tools/clang-tidy/test/misc-string-integer-assignment.json
@@ -0,0 +1,1 @@
+"[[\"warning\", \"an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility\", \"misc-string-integer-assignment\"], [\"warning\", \"an integer is interpreted as a character code when assigning it to a string; if this is intended, cast the integer to the appropriate character type; if you want a string representation, use the appropriate conversion facility\", \"misc-string-integer-assignment\"]]"
\ No newline at end of file
diff --git a/tools/clang-tidy/test/structures.h b/tools/clang-tidy/test/structures.h
--- a/tools/clang-tidy/test/structures.h
+++ b/tools/clang-tidy/test/structures.h
@@ -41,22 +41,25 @@ public:
   basic_string(const T *p, size_t count);
   basic_string(size_t count, char ch);
   ~basic_string() {}
   size_t size() const;
   bool empty() const;
   size_t find (const char* s, size_t pos = 0) const;
   const T *c_str() const;
   _Type& assign(const T *s);
+  basic_string<T> &operator=(T ch);
   basic_string<T> *operator+=(const basic_string<T> &) {}
   friend basic_string<T> operator+(const basic_string<T> &, const basic_string<T> &) {}
 };
 typedef basic_string<char> string;
 typedef basic_string<wchar_t> wstring;
 
+string to_string(int value);
+
 template <typename T>
 struct default_delete {};
 
 template <typename T, typename D = default_delete<T>>
 class unique_ptr {
  public:
   unique_ptr();
   ~unique_ptr();
