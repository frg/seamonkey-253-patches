# HG changeset patch
# User Andreas Pehrson <pehrsons@mozilla.com>
# Date 1521448838 -3600
# Node ID 37039eaf524823ed3157fafbdfcfbc450b67b4ab
# Parent  575783f39653bea8ea1d8a93f67afe4e0fbe1bda
Bug 1435036 - Improve window checks. r=jib

diff --git a/dom/media/MediaManager.cpp b/dom/media/MediaManager.cpp
--- a/dom/media/MediaManager.cpp
+++ b/dom/media/MediaManager.cpp
@@ -1126,37 +1126,40 @@ public:
 
   ~GetUserMediaStreamRunnable() {}
 
   class TracksAvailableCallback : public OnTracksAvailableCallback
   {
   public:
     TracksAvailableCallback(MediaManager* aManager,
                             const nsMainThreadPtrHandle<nsIDOMGetUserMediaSuccessCallback>& aSuccess,
-                            uint64_t aWindowID,
+                            const RefPtr<GetUserMediaWindowListener>& aWindowListener,
                             DOMMediaStream* aStream)
-      : mWindowID(aWindowID), mOnSuccess(aSuccess), mManager(aManager),
-        mStream(aStream) {}
+      : mWindowListener(aWindowListener),
+        mOnSuccess(aSuccess),
+        mManager(aManager),
+        mStream(aStream)
+    {}
     void NotifyTracksAvailable(DOMMediaStream* aStream) override
     {
-      // We're in the main thread, so no worries here.
-      if (!(mManager->IsWindowStillActive(mWindowID))) {
+      // We're on the main thread, so no worries here.
+      if (!mManager->IsWindowListenerStillActive(mWindowListener)) {
         return;
       }
 
       // Start currentTime from the point where this stream was successfully
       // returned.
       aStream->SetLogicalStreamStartTime(aStream->GetPlaybackStream()->GetCurrentTime());
 
       // This is safe since we're on main-thread, and the windowlist can only
       // be invalidated from the main-thread (see OnNavigation)
       LOG(("Returning success for getUserMedia()"));
       mOnSuccess->OnSuccess(aStream);
     }
-    uint64_t mWindowID;
+    RefPtr<GetUserMediaWindowListener> mWindowListener;
     nsMainThreadPtrHandle<nsIDOMGetUserMediaSuccessCallback> mOnSuccess;
     RefPtr<MediaManager> mManager;
     // Keep the DOMMediaStream alive until the NotifyTracksAvailable callback
     // has fired, otherwise we might immediately destroy the DOMMediaStream and
     // shut down the underlying MediaStream prematurely.
     // This creates a cycle which is broken when NotifyTracksAvailable
     // is fired (which will happen unless the browser shuts down,
     // since we only add this callback when we've successfully appended
@@ -1169,20 +1172,18 @@ public:
   Run() override
   {
     MOZ_ASSERT(NS_IsMainThread());
     nsGlobalWindow* globalWindow = nsGlobalWindow::GetInnerWindowWithId(mWindowID);
     nsPIDOMWindowInner* window = globalWindow ? globalWindow->AsInner() : nullptr;
 
     // We're on main-thread, and the windowlist can only
     // be invalidated from the main-thread (see OnNavigation)
-    GetUserMediaWindowListener* listener =
-      mManager->GetWindowListener(mWindowID);
-    if (!listener || !window || !window->GetExtantDoc()) {
-      // This window is no longer live.  mListener has already been removed
+    if (!mManager->IsWindowListenerStillActive(mWindowListener)) {
+      // This window is no longer live. mListener has already been removed.
       return NS_OK;
     }
 
     MediaStreamGraph::GraphDriverType graphDriverType =
       mAudioDevice ? MediaStreamGraph::AUDIO_THREAD_DRIVER
                    : MediaStreamGraph::SYSTEM_THREAD_DRIVER;
     MediaStreamGraph* msg =
       MediaStreamGraph::GetInstance(graphDriverType, window);
@@ -1362,17 +1363,17 @@ public:
     // Note: includes JS callbacks; must be released on MainThread
     typedef Refcountable<UniquePtr<TracksAvailableCallback>> Callback;
     nsMainThreadPtrHandle<Callback> callback(
       new nsMainThreadPtrHolder<Callback>(
         "GetUserMediaStreamRunnable::TracksAvailableCallbackMainThreadHolder",
         MakeAndAddRef<Callback>(
           new TracksAvailableCallback(mManager,
                                       mOnSuccess,
-                                      mWindowID,
+                                      mWindowListener,
                                       domStream))));
 
     // Dispatch to the media thread to ask it to start the sources,
     // because that can take a while.
     // Pass ownership of domStream through the lambda to the nested chrome
     // notification lambda to ensure it's kept alive until that lambda runs or is discarded.
     RefPtr<GetUserMediaStreamRunnable> self = this;
     MediaManager::PostTask(NewTaskFrom([self, domStream, callback]() mutable {
@@ -3213,16 +3214,23 @@ MediaManager::RemoveWindowID(uint64_t aW
   nsString data = NS_ConvertUTF8toUTF16(windowBuffer);
 
   nsCOMPtr<nsIObserverService> obs = services::GetObserverService();
   obs->NotifyObservers(nullptr, "recording-window-ended", data.get());
   LOG(("Sent recording-window-ended for window %" PRIu64 " (outer %" PRIu64 ")",
        aWindowId, outerID));
 }
 
+bool
+MediaManager::IsWindowListenerStillActive(GetUserMediaWindowListener* aListener)
+{
+  MOZ_DIAGNOSTIC_ASSERT(aListener);
+  return aListener && aListener == GetWindowListener(aListener->WindowID());
+}
+
 void
 MediaManager::GetPref(nsIPrefBranch *aBranch, const char *aPref,
                       const char *aData, int32_t *aVal)
 {
   int32_t temp;
   if (aData == nullptr || strcmp(aPref,aData) == 0) {
     if (NS_SUCCEEDED(aBranch->GetIntPref(aPref, &temp))) {
       *aVal = temp;
diff --git a/dom/media/MediaManager.h b/dom/media/MediaManager.h
--- a/dom/media/MediaManager.h
+++ b/dom/media/MediaManager.h
@@ -175,16 +175,17 @@ public:
     return mActiveWindows.GetWeak(aWindowId);
   }
   void AddWindowID(uint64_t aWindowId, GetUserMediaWindowListener *aListener);
   void RemoveWindowID(uint64_t aWindowId);
   void SendPendingGUMRequest();
   bool IsWindowStillActive(uint64_t aWindowId) {
     return !!GetWindowListener(aWindowId);
   }
+  bool IsWindowListenerStillActive(GetUserMediaWindowListener* aListener);
   // Note: also calls aListener->Remove(), even if inactive
   void RemoveFromWindowList(uint64_t aWindowID,
     GetUserMediaWindowListener *aListener);
 
   nsresult GetUserMedia(
     nsPIDOMWindowInner* aWindow,
     const dom::MediaStreamConstraints& aConstraints,
     nsIDOMGetUserMediaSuccessCallback* onSuccess,
