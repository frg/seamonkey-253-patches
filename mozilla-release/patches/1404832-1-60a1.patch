# HG changeset patch
# User Julian Descottes <jdescottes@mozilla.com>
# Date 1518191493 -3600
# Node ID 1e36136d7dda14351dc8a2945410e6faa735b021
# Parent  b24e0fb4e8031488e28d7847f229688713be4d48
Bug 1404832 - rename and enable browser_webconsole_loglimit.js;r=nchevobbe

The scrolling part of the test already seems covered in
browser_webconsole_scroll.js

MozReview-Commit-ID: 3CX9HKkPzqW

diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser.ini
@@ -296,16 +296,17 @@ skip-if = true #       Bug 1405343
 [browser_webconsole_jsterm.js]
 skip-if = true #       Bug 1405352
 # old console skip-if = e10s # Bug 1042253 - webconsole e10s tests (Linux debug timeout)
 [browser_webconsole_keyboard_accessibility.js]
 [browser_webconsole_location_debugger_link.js]
 [browser_webconsole_location_scratchpad_link.js]
 [browser_webconsole_location_styleeditor_link.js]
 [browser_webconsole_logErrorInPage.js]
+[browser_webconsole_loglimit.js]
 [browser_webconsole_longstring_expand.js]
 skip-if = true #       Bug 1403448
 [browser_webconsole_longstring_hang.js]
 skip-if = true #       Bug 1403448
 [browser_webconsole_message_categories.js]
 [browser_webconsole_multiple_windows_and_tabs.js]
 [browser_webconsole_network_attach.js]
 [browser_webconsole_network_exceptions.js]
@@ -322,18 +323,16 @@ skip-if = true #       Bug 1403448
 [browser_webconsole_observer_notifications.js]
 [browser_webconsole_optimized_out_vars.js]
 [browser_webconsole_output_copy.js]
 subsuite = clipboard
 [browser_webconsole_output_copy_newlines.js]
 subsuite = clipboard
 [browser_webconsole_output_order.js]
 [browser_webconsole_persist.js]
-[browser_webconsole_prune_scroll.js]
-skip-if = true #       Bug 1404832
 [browser_webconsole_reopen_closed_tab.js]
 [browser_webconsole_repeat_different_objects.js]
 [browser_webconsole_repeated_messages_accuracy.js]
 skip-if = true #       Bug 1403450
 [browser_webconsole_sandbox_update_after_navigation.js]
 [browser_webconsole_script_errordoc_urls.js]
 skip-if = true #       Bug 1403454
 # old console skip-if = e10s # Bug 1042253 - webconsole tests disabled with e10s
diff --git a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_prune_scroll.js b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_loglimit.js
rename from devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_prune_scroll.js
rename to devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_loglimit.js
--- a/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_prune_scroll.js
+++ b/devtools/client/webconsole/new-console-output/test/mochitest/browser_webconsole_loglimit.js
@@ -1,84 +1,40 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* Any copyright is dedicated to the Public Domain.
  * http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
-// See Bug 613642.
+// Test that messages are properly updated when the log limit is reached.
 
 const TEST_URI = "data:text/html;charset=utf-8,Web Console test for " +
-                 "bug 613642: maintain scroll with pruning of old messages";
-
-var hud;
+                 "Old messages are removed after passing devtools.hud.loglimit";
 
-add_task(function* () {
-  yield loadTab(TEST_URI);
-
-  hud = yield openConsole();
-
+add_task(async function () {
+  await pushPref("devtools.hud.loglimit", 140);
+  let hud = await openNewTabAndConsole(TEST_URI);
   hud.jsterm.clearOutput();
 
-  let outputNode = hud.outputNode;
-
-  Services.prefs.setIntPref("devtools.hud.loglimit.console", 140);
-  let scrollBoxElement = hud.ui.outputWrapper;
-
+  let onMessage = waitForMessage(hud, "test message [149]");
   ContentTask.spawn(gBrowser.selectedBrowser, {}, function* () {
     for (let i = 0; i < 150; i++) {
-      content.console.log("test message " + i);
+      content.console.log(`test message [${i}]`);
     }
   });
-
-  yield waitForMessages({
-    webconsole: hud,
-    messages: [{
-      text: "test message 149",
-      category: CATEGORY_WEBDEV,
-      severity: SEVERITY_LOG,
-    }],
-  });
-
-  let oldScrollTop = scrollBoxElement.scrollTop;
-  isnot(oldScrollTop, 0, "scroll location is not at the top");
+  await onMessage;
 
-  let firstNode = outputNode.firstChild;
-  ok(firstNode, "found the first message");
-
-  let msgNode = outputNode.children[80];
-  ok(msgNode, "found the 80th message");
+  ok(!findMessage(hud, "test message [0]"), "Message 0 has been pruned");
+  ok(!findMessage(hud, "test message [9]"), "Message 9 has been pruned");
+  ok(findMessage(hud, "test message [10]"), "Message 10 is still displayed");
+  is(findMessages(hud, "").length, 140, "Number of displayed messages is correct");
 
-  // scroll to the middle message node
-  msgNode.scrollIntoView(false);
-
-  isnot(scrollBoxElement.scrollTop, oldScrollTop,
-        "scroll location updated (scrolled to message)");
-
-  oldScrollTop = scrollBoxElement.scrollTop;
-
-  // add a message
+  onMessage = waitForMessage(hud, "hello world");
   ContentTask.spawn(gBrowser.selectedBrowser, {}, function* () {
     content.console.log("hello world");
   });
-
-  yield waitForMessages({
-    webconsole: hud,
-    messages: [{
-      text: "hello world",
-      category: CATEGORY_WEBDEV,
-      severity: SEVERITY_LOG,
-    }],
-  });
+  await onMessage;
 
-  // Scroll location needs to change, because one message is also removed, and
-  // we need to scroll a bit towards the top, to keep the current view in sync.
-  isnot(scrollBoxElement.scrollTop, oldScrollTop,
-        "scroll location updated (added a message)");
-
-  isnot(outputNode.firstChild, firstNode,
-        "first message removed");
-
-  Services.prefs.clearUserPref("devtools.hud.loglimit.console");
-
-  hud = null;
+  ok(!findMessage(hud, "test message [10]"), "Message 10 has been pruned");
+  ok(findMessage(hud, "test message [11]"), "Message 11 is still displayed");
+  is(findMessages(hud, "").length, 140, "Number of displayed messages is still correct");
 });
