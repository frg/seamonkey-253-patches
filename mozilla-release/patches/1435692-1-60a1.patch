# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1520099430 -3600
# Node ID 65a8e5654ef56bb7a2cf81267a78d7151643765b
# Parent  b38cc9cbd3068979e58b0417c44d077e9375b2c8
Bug 1435692 - patch 1 - Add support in gfx for automatic application of the optical size axis in variation fonts that support it. r=jwatt


diff --git a/gfx/src/nsFont.cpp b/gfx/src/nsFont.cpp
--- a/gfx/src/nsFont.cpp
+++ b/gfx/src/nsFont.cpp
@@ -51,16 +51,17 @@ nsFont::CalcDifference(const nsFont& aOt
   if ((style != aOther.style) ||
       (systemFont != aOther.systemFont) ||
       (weight != aOther.weight) ||
       (stretch != aOther.stretch) ||
       (size != aOther.size) ||
       (sizeAdjust != aOther.sizeAdjust) ||
       (fontlist != aOther.fontlist) ||
       (kerning != aOther.kerning) ||
+      (opticalSizing != aOther.opticalSizing) ||
       (synthesis != aOther.synthesis) ||
       (fontFeatureSettings != aOther.fontFeatureSettings) ||
       (fontVariationSettings != aOther.fontVariationSettings) ||
       (languageOverride != aOther.languageOverride) ||
       (variantAlternates != aOther.variantAlternates) ||
       (variantCaps != aOther.variantCaps) ||
       (variantEastAsian != aOther.variantEastAsian) ||
       (variantLigatures != aOther.variantLigatures) ||
@@ -287,13 +288,32 @@ void nsFont::AddFontFeaturesToStyle(gfxF
   }
 
   aStyle->fontSmoothingBackgroundColor = fontSmoothingBackgroundColor;
 }
 
 void nsFont::AddFontVariationsToStyle(gfxFontStyle *aStyle) const
 {
   // TODO: add variation settings from specific CSS properties
-  // such as weight, width, optical-size
+  // such as weight, width, stretch
+
+  // If auto optical sizing is enabled, and if there's no 'opsz' axis in
+  // fontVariationSettings, then set the automatic value on the style.
+  class VariationTagComparator {
+  public:
+    bool Equals(const gfxFontVariation& aVariation, uint32_t aTag) const {
+      return aVariation.mTag == aTag;
+    }
+  };
+  const uint32_t kTagOpsz = TRUETYPE_TAG('o','p','s','z');
+  if (opticalSizing == NS_FONT_OPTICAL_SIZING_AUTO &&
+    !fontVariationSettings.Contains(kTagOpsz, VariationTagComparator())) {
+    gfxFontVariation opsz = {
+      kTagOpsz,
+      // size is in app units, but we want a floating-point px size
+      float(size) / float(AppUnitsPerCSSPixel())
+    };
+    aStyle->variationSettings.AppendElement(opsz);
+  }
 
   // Add in arbitrary values from font-variation-settings
   aStyle->variationSettings.AppendElements(fontVariationSettings);
 }
diff --git a/gfx/src/nsFont.h b/gfx/src/nsFont.h
--- a/gfx/src/nsFont.h
+++ b/gfx/src/nsFont.h
@@ -83,16 +83,20 @@ struct nsFont {
 
   // The stretch of the font (the sum of various NS_FONT_STRETCH_*
   // constants; see gfxFontConstants.h).
   int16_t stretch = NS_FONT_STRETCH_NORMAL;
 
   // Kerning
   uint8_t kerning = NS_FONT_KERNING_AUTO;
 
+  // Whether automatic optical sizing should be applied to variation fonts
+  // that include an 'opsz' axis
+  uint8_t opticalSizing = NS_FONT_OPTICAL_SIZING_AUTO;
+
   // Synthesis setting, controls use of fake bolding/italics
   uint8_t synthesis = NS_FONT_SYNTHESIS_WEIGHT | NS_FONT_SYNTHESIS_STYLE;
 
   // The logical size of the font, in nscoord units
   nscoord size = 0;
 
   // The aspect-value (ie., the ratio actualsize:actualxheight) that any
   // actual physical font created from this font structure must have when
diff --git a/gfx/thebes/gfxFontConstants.h b/gfx/thebes/gfxFontConstants.h
--- a/gfx/thebes/gfxFontConstants.h
+++ b/gfx/thebes/gfxFontConstants.h
@@ -42,16 +42,19 @@
 #define NS_FONT_SYNTHESIS_STYLE                     0x2
 
 #define NS_FONT_DISPLAY_AUTO            0
 #define NS_FONT_DISPLAY_BLOCK           1
 #define NS_FONT_DISPLAY_SWAP            2
 #define NS_FONT_DISPLAY_FALLBACK        3
 #define NS_FONT_DISPLAY_OPTIONAL        4
 
+#define NS_FONT_OPTICAL_SIZING_AUTO     0
+#define NS_FONT_OPTICAL_SIZING_NONE     1
+
 #define NS_FONT_VARIANT_ALTERNATES_NORMAL             0
 // alternates - simple enumerated values
 #define NS_FONT_VARIANT_ALTERNATES_HISTORICAL        (1 << 0)
 
 // alternates - values that use functional syntax
 #define NS_FONT_VARIANT_ALTERNATES_STYLISTIC         (1 << 1)
 #define NS_FONT_VARIANT_ALTERNATES_STYLESET          (1 << 2)
 #define NS_FONT_VARIANT_ALTERNATES_CHARACTER_VARIANT (1 << 3)

