# HG changeset patch
# User Valentin Gosu <valentin.gosu@gmail.com>
# Date 1638912723 0
#      Tue Dec 07 21:32:03 2021 +0000
# Node ID 0592ba15e779a54d0c40c2cf72c25f2875a77dcd
# Parent  dd341dd9159055b00ef49f123ecc2f396757434b
Bug 1742334 - Make sure to null out ChannelEventQueue::mOwner when object is released. r=dragana, a=tjr

Differential Revision: https://phabricator.services.mozilla.com/D131896

diff --git a/netwerk/ipc/ChannelEventQueue.cpp b/netwerk/ipc/ChannelEventQueue.cpp
--- a/netwerk/ipc/ChannelEventQueue.cpp
+++ b/netwerk/ipc/ChannelEventQueue.cpp
@@ -32,18 +32,22 @@ ChannelEventQueue::TakeEvent()
 }
 
 void
 ChannelEventQueue::FlushQueue()
 {
   // Events flushed could include destruction of channel (and our own
   // destructor) unless we make sure its refcount doesn't drop to 0 while this
   // method is running.
-  nsCOMPtr<nsISupports> kungFuDeathGrip(mOwner);
-  mozilla::Unused << kungFuDeathGrip; // Not used in this function
+  nsCOMPtr<nsISupports> kungFuDeathGrip;
+  {
+    MutexAutoLock lock(mMutex);
+    kungFuDeathGrip = mOwner;
+  }
+  mozilla::Unused << kungFuDeathGrip;  // Not used in this function
 
 #ifdef DEBUG
   {
     MutexAutoLock lock(mMutex);
     MOZ_ASSERT(mFlushing);
   }
 #endif // DEBUG
 
@@ -164,16 +168,20 @@ ChannelEventQueue::ResumeInternal()
 
     private:
       virtual ~CompleteResumeRunnable() {}
 
       RefPtr<ChannelEventQueue> mQueue;
       nsCOMPtr<nsISupports> mOwner;
     };
 
+    if (!mOwner) {
+      return;
+    }
+
     // Worker thread requires a CancelableRunnable.
     RefPtr<Runnable> event = new CompleteResumeRunnable(this, mOwner);
 
     nsCOMPtr<nsIEventTarget> target;
       target = mEventQueue[0]->GetEventTarget();
     MOZ_ASSERT(target);
 
     Unused << NS_WARN_IF(NS_FAILED(target->Dispatch(event.forget(),
diff --git a/netwerk/ipc/ChannelEventQueue.h b/netwerk/ipc/ChannelEventQueue.h
--- a/netwerk/ipc/ChannelEventQueue.h
+++ b/netwerk/ipc/ChannelEventQueue.h
@@ -126,16 +126,21 @@ class ChannelEventQueue final
   // Suspend/resume event queue.  RunOrEnqueue() will start enqueuing
   // events and they will be run/flushed when resume is called.  These should be
   // called when the channel owning the event queue is suspended/resumed.
   void Suspend();
   // Resume flushes the queue asynchronously, i.e. items in queue will be
   // dispatched in a new event on the current thread.
   void Resume();
 
+  void NotifyReleasingOwner() {
+    MutexAutoLock lock(mMutex);
+    mOwner = nullptr;
+  }
+
  private:
   // Private destructor, to discourage deletion outside of Release():
   ~ChannelEventQueue()
   {
   }
 
   void SuspendInternal();
   void ResumeInternal();
diff --git a/netwerk/protocol/http/HttpChannelChild.cpp b/netwerk/protocol/http/HttpChannelChild.cpp
--- a/netwerk/protocol/http/HttpChannelChild.cpp
+++ b/netwerk/protocol/http/HttpChannelChild.cpp
@@ -207,16 +207,18 @@ HttpChannelChild::HttpChannelChild()
   // processing HTTP responses.
   RefPtr<CookieServiceChild> cookieService = CookieServiceChild::GetSingleton();
 }
 
 HttpChannelChild::~HttpChannelChild()
 {
   LOG(("Destroying HttpChannelChild @%p\n", this));
 
+  mEventQ->NotifyReleasingOwner();
+
   ReleaseMainThreadOnlyReferences();
 }
 
 void
 HttpChannelChild::ReleaseMainThreadOnlyReferences()
 {
   if (NS_IsMainThread()) {
       // Already on main thread, let dtor to
diff --git a/netwerk/protocol/http/HttpChannelParent.cpp b/netwerk/protocol/http/HttpChannelParent.cpp
--- a/netwerk/protocol/http/HttpChannelParent.cpp
+++ b/netwerk/protocol/http/HttpChannelParent.cpp
@@ -95,16 +95,18 @@ HttpChannelParent::HttpChannelParent(con
 
   mEventQ = new ChannelEventQueue(static_cast<nsIParentRedirectingChannel*>(this));
 }
 
 HttpChannelParent::~HttpChannelParent()
 {
   LOG(("Destroying HttpChannelParent [this=%p]\n", this));
   CleanupBackgroundChannel();
+
+  mEventQ->NotifyReleasingOwner();
 }
 
 void
 HttpChannelParent::ActorDestroy(ActorDestroyReason why)
 {
   // We may still have refcount>0 if nsHttpChannel hasn't called OnStopRequest
   // yet, but child process has crashed.  We must not try to send any more msgs
   // to child, or IPDL will kill chrome process, too.
diff --git a/netwerk/protocol/http/HttpTransactionParent.cpp.1742334.later b/netwerk/protocol/http/HttpTransactionParent.cpp.1742334.later
new file mode 100644
--- /dev/null
+++ b/netwerk/protocol/http/HttpTransactionParent.cpp.1742334.later
@@ -0,0 +1,20 @@
+--- HttpTransactionParent.cpp
++++ HttpTransactionParent.cpp
+@@ -77,16 +77,17 @@ NS_IMETHODIMP_(MozExternalRefCountType) 
+ HttpTransactionParent::HttpTransactionParent(bool aIsDocumentLoad)
+     : mIsDocumentLoad(aIsDocumentLoad) {
+   LOG(("Creating HttpTransactionParent @%p\n", this));
+   mEventQ = new ChannelEventQueue(static_cast<nsIRequest*>(this));
+ }
+ 
+ HttpTransactionParent::~HttpTransactionParent() {
+   LOG(("Destroying HttpTransactionParent @%p\n", this));
++  mEventQ->NotifyReleasingOwner();
+ }
+ 
+ //-----------------------------------------------------------------------------
+ // HttpTransactionParent <nsAHttpTransactionShell>
+ //-----------------------------------------------------------------------------
+ 
+ // Let socket process init the *real* nsHttpTransaction.
+ nsresult HttpTransactionParent::Init(
diff --git a/netwerk/protocol/websocket/WebSocketChannelChild.cpp b/netwerk/protocol/websocket/WebSocketChannelChild.cpp
--- a/netwerk/protocol/websocket/WebSocketChannelChild.cpp
+++ b/netwerk/protocol/websocket/WebSocketChannelChild.cpp
@@ -61,16 +61,17 @@ WebSocketChannelChild::WebSocketChannelC
   LOG(("WebSocketChannelChild::WebSocketChannelChild() %p\n", this));
   mEncrypted = aEncrypted;
   mEventQ = new ChannelEventQueue(static_cast<nsIWebSocketChannel*>(this));
 }
 
 WebSocketChannelChild::~WebSocketChannelChild()
 {
   LOG(("WebSocketChannelChild::~WebSocketChannelChild() %p\n", this));
+  mEventQ->NotifyReleasingOwner();
 }
 
 void
 WebSocketChannelChild::AddIPDLReference()
 {
   MOZ_ASSERT(NS_IsMainThread());
 
   {
diff --git a/toolkit/components/extensions/webrequest/StreamFilterParent.cpp b/toolkit/components/extensions/webrequest/StreamFilterParent.cpp
--- a/toolkit/components/extensions/webrequest/StreamFilterParent.cpp
+++ b/toolkit/components/extensions/webrequest/StreamFilterParent.cpp
@@ -117,16 +117,17 @@ StreamFilterParent::StreamFilterParent()
 StreamFilterParent::~StreamFilterParent()
 {
   NS_ReleaseOnMainThreadSystemGroup("StreamFilterParent::mChannel",
                                     mChannel.forget());
   NS_ReleaseOnMainThreadSystemGroup("StreamFilterParent::mOrigListener",
                                     mOrigListener.forget());
   NS_ReleaseOnMainThreadSystemGroup("StreamFilterParent::mContext",
                                     mContext.forget());
+  mQueue->NotifyReleasingOwner();
 }
 
 bool
 StreamFilterParent::Create(dom::ContentParent* aContentParent, uint64_t aChannelId, const nsAString& aAddonId,
                            Endpoint<PStreamFilterChild>* aEndpoint)
 {
   AssertIsMainThread();
 
