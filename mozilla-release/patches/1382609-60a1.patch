# HG changeset patch
# User Nicolas Chevobbe <nchevobbe@mozilla.com>
# Date 1519718816 -3600
# Node ID 784928adcf87fadbbbcfc2a28345d94b6cffb31c
# Parent  b47ef99804aefe96f2ec8d8eee900378594ee681
Bug 1382609 - Remove old-event-emitter usage from devtools/server; r=pbro.

MozReview-Commit-ID: 3B3slS3EPYn

diff --git a/devtools/client/framework/ToolboxProcess.jsm b/devtools/client/framework/ToolboxProcess.jsm
--- a/devtools/client/framework/ToolboxProcess.jsm
+++ b/devtools/client/framework/ToolboxProcess.jsm
@@ -329,17 +329,17 @@ BrowserToolboxProcess.prototype = {
   /**
    * Called upon receiving the connectionchange event from a debuggerServer.
    *
    * @param {String} what
    *        Type of connection change (can be either 'opened' or 'closed').
    * @param {DebuggerServerConnection} connection
    *        The connection that was opened or closed.
    */
-  _onConnectionChange: function (evt, what, connection) {
+  _onConnectionChange: function (what, connection) {
     let wrappedJSObject = { what, connection };
     Services.obs.notifyObservers({ wrappedJSObject }, "toolbox-connection-change");
   },
 
   /**
    * Closes the remote debugging server and kills the toolbox process.
    */
   close: async function () {
diff --git a/devtools/server/actors/highlighters.js b/devtools/server/actors/highlighters.js
--- a/devtools/server/actors/highlighters.js
+++ b/devtools/server/actors/highlighters.js
@@ -2,17 +2,17 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const { Ci, Cu } = require("chrome");
 
 const { XPCOMUtils } = require("resource://gre/modules/XPCOMUtils.jsm");
-const EventEmitter = require("devtools/shared/old-event-emitter");
+const EventEmitter = require("devtools/shared/event-emitter");
 const protocol = require("devtools/shared/protocol");
 const Services = require("Services");
 const { highlighterSpec, customHighlighterSpec } = require("devtools/shared/specs/highlighters");
 
 loader.lazyRequireGetter(this, "isWindowIncluded", "devtools/shared/layout/utils", true);
 loader.lazyRequireGetter(this, "isXUL", "devtools/server/actors/highlighters/utils/markup", true);
 loader.lazyRequireGetter(this, "SimpleOutlineHighlighter", "devtools/server/actors/highlighters/simple-outline", true);
 loader.lazyRequireGetter(this, "BoxModelHighlighter", "devtools/server/actors/highlighters/box-model", true);
@@ -512,17 +512,17 @@ exports.CustomHighlighterActor = protoco
     if (this._highlighter) {
       this._highlighter.hide();
     }
   },
 
   /**
    * Upon receiving an event from the highlighter, forward it to the client.
    */
-  _onHighlighterEvent: function (type, data) {
+  _onHighlighterEvent: function (data) {
     this.emit("highlighter-event", data);
   },
 
   /**
    * Kill this actor. This method is called automatically just before the actor
    * is destroyed.
    */
   finalize: function () {
diff --git a/devtools/server/actors/highlighters/auto-refresh.js b/devtools/server/actors/highlighters/auto-refresh.js
--- a/devtools/server/actors/highlighters/auto-refresh.js
+++ b/devtools/server/actors/highlighters/auto-refresh.js
@@ -1,16 +1,16 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const { Cu } = require("chrome");
-const EventEmitter = require("devtools/shared/old-event-emitter");
+const EventEmitter = require("devtools/shared/event-emitter");
 const { isNodeValid } = require("./utils/markup");
 const { getAdjustedQuads, getWindowDimensions } = require("devtools/shared/layout/utils");
 
 // Note that the order of items in this array is important because it is used
 // for drawing the BoxModelHighlighter's path elements correctly.
 const BOX_MODEL_REGIONS = ["margin", "border", "padding", "content"];
 const QUADS_PROPS = ["p1", "p2", "p3", "p4", "bounds"];
 
diff --git a/devtools/server/actors/highlighters/eye-dropper.js b/devtools/server/actors/highlighters/eye-dropper.js
--- a/devtools/server/actors/highlighters/eye-dropper.js
+++ b/devtools/server/actors/highlighters/eye-dropper.js
@@ -6,17 +6,17 @@
 // Eye-dropper tool. This is implemented as a highlighter so it can be displayed in the
 // content page.
 // It basically displays a magnifier that tracks mouse moves and shows a magnified version
 // of the page. On click, it samples the color at the pixel being hovered.
 
 const {Ci, Cc} = require("chrome");
 const {CanvasFrameAnonymousContentHelper, createNode} = require("./utils/markup");
 const Services = require("Services");
-const EventEmitter = require("devtools/shared/old-event-emitter");
+const EventEmitter = require("devtools/shared/event-emitter");
 const {rgbToHsl, rgbToColorName} = require("devtools/shared/css/color").colorUtils;
 const {getCurrentZoom, getFrameOffsets} = require("devtools/shared/layout/utils");
 
 loader.lazyGetter(this, "clipboardHelper",
   () => Cc["@mozilla.org/widget/clipboardhelper;1"].getService(Ci.nsIClipboardHelper));
 loader.lazyGetter(this, "l10n",
   () => Services.strings.createBundle("chrome://devtools-shared/locale/eyedropper.properties"));
 
diff --git a/devtools/server/actors/highlighters/shapes.js b/devtools/server/actors/highlighters/shapes.js
--- a/devtools/server/actors/highlighters/shapes.js
+++ b/devtools/server/actors/highlighters/shapes.js
@@ -21,17 +21,17 @@ const {
   apply,
   translate,
   multiply,
   scale,
   rotate,
   changeMatrixBase,
   getBasis
 } = require("devtools/shared/layout/dom-matrix-2d");
-const EventEmitter = require("devtools/shared/old-event-emitter");
+const EventEmitter = require("devtools/shared/event-emitter");
 const { getCSSStyleRules } = require("devtools/shared/inspector/css-logic");
 
 const BASE_MARKER_SIZE = 5;
 // the width of the area around highlighter lines that can be clicked, in px
 const LINE_CLICK_WIDTH = 5;
 const ROTATE_LINE_LENGTH = 50;
 const DOM_EVENTS = ["mousedown", "mousemove", "mouseup", "dblclick"];
 const _dragging = Symbol("shapes/dragging");
diff --git a/devtools/server/actors/highlighters/utils/markup.js b/devtools/server/actors/highlighters/utils/markup.js
--- a/devtools/server/actors/highlighters/utils/markup.js
+++ b/devtools/server/actors/highlighters/utils/markup.js
@@ -298,17 +298,17 @@ CanvasFrameAnonymousContentHelper.protot
 
   /**
    * The "window-ready" event can be triggered when:
    *   - a new window is created
    *   - a window is unfrozen from bfcache
    *   - when first attaching to a page
    *   - when swapping frame loaders (moving tabs, toggling RDM)
    */
-  _onWindowReady(e, {isTopLevel}) {
+  _onWindowReady({isTopLevel}) {
     if (isTopLevel) {
       this._removeAllListeners();
       this.elements.clear();
       this._insert();
       this.anonymousContentDocument = this.highlighterEnv.document;
     }
   },
 
diff --git a/devtools/server/actors/inspector/inspector.js b/devtools/server/actors/inspector/inspector.js
--- a/devtools/server/actors/inspector/inspector.js
+++ b/devtools/server/actors/inspector/inspector.js
@@ -301,16 +301,16 @@ exports.InspectorActor = protocol.ActorC
     // SVG documents do not render the canvasFrame (see Bug 1157592).
     if (ns === SVG_NS) {
       return false;
     }
 
     return true;
   },
 
-  _onColorPicked: function (e, color) {
+  _onColorPicked: function (color) {
     this.emit("color-picked", color);
   },
 
   _onColorPickCanceled: function () {
     this.emit("color-pick-canceled");
   }
 });
diff --git a/devtools/server/actors/performance.js b/devtools/server/actors/performance.js
--- a/devtools/server/actors/performance.js
+++ b/devtools/server/actors/performance.js
@@ -38,23 +38,42 @@ var PerformanceActor = ActorClassWithSpe
       withGCEvents: true,
       withDocLoadingEvents: true,
       withAllocations: true,
     },
   },
 
   initialize: function (conn, tabActor) {
     Actor.prototype.initialize.call(this, conn);
-    this._onRecorderEvent = this._onRecorderEvent.bind(this);
+
+    this._onRecordingStarted = this._onRecordingStarted.bind(this);
+    this._onRecordingStopping = this._onRecordingStopping.bind(this);
+    this._onRecordingStopped = this._onRecordingStopped.bind(this);
+    this._onProfilerStatus = this._onProfilerStatus.bind(this);
+    this._onTimelineData = this._onTimelineData.bind(this);
+    this._onConsoleProfileStart = this._onConsoleProfileStart.bind(this);
+
     this.bridge = new PerformanceRecorder(conn, tabActor);
-    this.bridge.on("*", this._onRecorderEvent);
+
+    this.bridge.on("recording-started", this._onRecordingStarted);
+    this.bridge.on("recording-stopping", this._onRecordingStopping);
+    this.bridge.on("recording-stopped", this._onRecordingStopped);
+    this.bridge.on("profiler-status", this._onProfilerStatus);
+    this.bridge.on("timeline-data", this._onTimelineData);
+    this.bridge.on("console-profile-start", this._onConsoleProfileStart);
   },
 
   destroy: function () {
-    this.bridge.off("*", this._onRecorderEvent);
+    this.bridge.off("recording-started", this._onRecordingStarted);
+    this.bridge.off("recording-stopping", this._onRecordingStopping);
+    this.bridge.off("recording-stopped", this._onRecordingStopped);
+    this.bridge.off("profiler-status", this._onProfilerStatus);
+    this.bridge.off("timeline-data", this._onTimelineData);
+    this.bridge.off("console-profile-start", this._onConsoleProfileStart);
+
     this.bridge.destroy();
     Actor.prototype.destroy.call(this);
   },
 
   connect: function (config) {
     this.bridge.connect({ systemClient: config.systemClient });
     return { traits: this.traits };
   },
@@ -79,17 +98,41 @@ var PerformanceActor = ActorClassWithSpe
   isRecording: actorBridgeWithSpec("isRecording"),
   getRecordings: actorBridgeWithSpec("getRecordings"),
   getConfiguration: actorBridgeWithSpec("getConfiguration"),
   setProfilerStatusInterval: actorBridgeWithSpec("setProfilerStatusInterval"),
 
   /**
    * Filter which events get piped to the front.
    */
-  _onRecorderEvent: function (eventName, ...data) {
+  _onRecordingStarted: function (...data) {
+    this._onRecorderEvent("recording-started", data);
+  },
+
+  _onRecordingStopping: function (...data) {
+    this._onRecorderEvent("recording-stopping", data);
+  },
+
+  _onRecordingStopped: function (...data) {
+    this._onRecorderEvent("recording-stopped", data);
+  },
+
+  _onProfilerStatus: function (...data) {
+    this._onRecorderEvent("profiler-status", data);
+  },
+
+  _onTimelineData: function (...data) {
+    this._onRecorderEvent("timeline-data", data);
+  },
+
+  _onConsoleProfileStart: function (...data) {
+    this._onRecorderEvent("console-profile-start", data);
+  },
+
+  _onRecorderEvent: function (eventName, data) {
     // If this is a recording state change, call
     // a method on the related PerformanceRecordingActor so it can
     // update its internal state.
     if (RECORDING_STATE_CHANGE_EVENTS.has(eventName)) {
       let recording = data[0];
       let extraData = data[1];
       recording._setState(eventName, extraData);
     }
diff --git a/devtools/server/actors/reflow.js b/devtools/server/actors/reflow.js
--- a/devtools/server/actors/reflow.js
+++ b/devtools/server/actors/reflow.js
@@ -22,17 +22,17 @@
  * - Dedicated observers: There's only one of them for now: ReflowObserver which
  *   listens to reflow events via the docshell,
  *   These dedicated classes are used by the LayoutChangesObserver.
  */
 
 const {Ci} = require("chrome");
 const {XPCOMUtils} = require("resource://gre/modules/XPCOMUtils.jsm");
 const protocol = require("devtools/shared/protocol");
-const EventEmitter = require("devtools/shared/old-event-emitter");
+const EventEmitter = require("devtools/shared/event-emitter");
 const {reflowSpec} = require("devtools/shared/specs/reflow");
 
 /**
  * The reflow actor tracks reflows and emits events about them.
  */
 exports.ReflowActor = protocol.ActorClassWithSpec(reflowSpec, {
   initialize: function (conn, tabActor) {
     protocol.Actor.prototype.initialize.call(this, conn);
@@ -71,17 +71,17 @@ exports.ReflowActor = protocol.ActorClas
    */
   stop: function () {
     if (this._isStarted) {
       this.observer.off("reflows", this._onReflow);
       this._isStarted = false;
     }
   },
 
-  _onReflow: function (event, reflows) {
+  _onReflow: function (reflows) {
     if (this._isStarted) {
       this.emit("reflows", reflows);
     }
   }
 });
 
 /**
  * Base class for all sorts of observers that need to listen to events on the
diff --git a/devtools/server/actors/thread.js b/devtools/server/actors/thread.js
--- a/devtools/server/actors/thread.js
+++ b/devtools/server/actors/thread.js
@@ -81,17 +81,19 @@ const ThreadActor = ActorClassWithSpec(t
     // A map of actorID -> actor for breakpoints created and managed by the
     // server.
     this._hiddenBreakpoints = new Map();
 
     this.global = global;
 
     this._allEventsListener = this._allEventsListener.bind(this);
     this.onNewGlobal = this.onNewGlobal.bind(this);
-    this.onSourceEvent = this.onSourceEvent.bind(this);
+    this.onNewSourceEvent = this.onNewSourceEvent.bind(this);
+    this.onUpdatedSourceEvent = this.onUpdatedSourceEvent.bind(this);
+
     this.uncaughtExceptionHook = this.uncaughtExceptionHook.bind(this);
     this.onDebuggerStatement = this.onDebuggerStatement.bind(this);
     this.onNewScript = this.onNewScript.bind(this);
     this.objectGrip = this.objectGrip.bind(this);
     this.pauseObjectGrip = this.pauseObjectGrip.bind(this);
     this._onWindowReady = this._onWindowReady.bind(this);
     EventEmitter.on(this._parent, "window-ready", this._onWindowReady);
     // Set a wrappedJSObject property so |this| can be sent via the observer svc
@@ -216,18 +218,18 @@ const ThreadActor = ActorClassWithSpec(t
     }
 
     // Blow away our source actor ID store because those IDs are only
     // valid for this connection. This is ok because we never keep
     // things like breakpoints across connections.
     this._sourceActorStore = null;
 
     EventEmitter.off(this._parent, "window-ready", this._onWindowReady);
-    this.sources.off("newSource", this.onSourceEvent);
-    this.sources.off("updatedSource", this.onSourceEvent);
+    this.sources.off("newSource", this.onNewSourceEvent);
+    this.sources.off("updatedSource", this.onUpdatedSourceEvent);
     this.clearDebuggees();
     this.conn.removeActorPool(this._threadLifetimePool);
     this._threadLifetimePool = null;
 
     if (this._prettyPrintWorker) {
       this._prettyPrintWorker.destroy();
       this._prettyPrintWorker = null;
     }
@@ -258,18 +260,18 @@ const ThreadActor = ActorClassWithSpec(t
                message: "Current state is " + this.state };
     }
 
     this._state = "attached";
     this._debuggerSourcesSeen = new WeakSet();
 
     Object.assign(this._options, request.options || {});
     this.sources.setOptions(this._options);
-    this.sources.on("newSource", this.onSourceEvent);
-    this.sources.on("updatedSource", this.onSourceEvent);
+    this.sources.on("newSource", this.onNewSourceEvent);
+    this.sources.on("updatedSource", this.onUpdatedSourceEvent);
 
     // Initialize an event loop stack. This can't be done in the constructor,
     // because this.conn is not yet initialized by the actor pool at that time.
     this._nestedEventLoops = new EventLoopStack({
       hooks: this._parent,
       connection: this.conn,
       thread: this
     });
@@ -1543,38 +1545,50 @@ const ThreadActor = ActorClassWithSpec(t
    * @param global Debugger.Object
    *        A Debugger.Object instance whose referent is the global object.
    */
   onNewScript: function (script, global) {
     this._addSource(script.source);
   },
 
   /**
-   * A function called when there's a new or updated source from a thread actor's
-   * sources. Emits `newSource` and `updatedSource` on the tab actor.
+   * A function called when there's a new source from a thread actor's sources.
+   * Emits `newSource` on the tab actor.
    *
-   * @param {String} name
    * @param {SourceActor} source
    */
-  onSourceEvent: function (name, source) {
+  onNewSourceEvent: function (source) {
+    const type = "newSource";
     this.conn.send({
       from: this._parent.actorID,
-      type: name,
+      type,
       source: source.form()
     });
 
     // For compatibility and debugger still using `newSource` on the thread client,
     // still emit this event here. Clean up in bug 1247084
-    if (name === "newSource") {
-      this.conn.send({
-        from: this.actorID,
-        type: name,
-        source: source.form()
-      });
-    }
+    this.conn.send({
+      from: this.actorID,
+      type,
+      source: source.form()
+    });
+  },
+
+  /**
+   * A function called when there's an updated source from a thread actor' sources.
+   * Emits `updatedSource` on the tab actor.
+   *
+   * @param {SourceActor} source
+   */
+  onUpdatedSourceEvent: function (source) {
+    this.conn.send({
+      from: this._parent.actorID,
+      type: "updatedSource",
+      source: source.form()
+    });
   },
 
   /**
    * Add the provided source to the server cache.
    *
    * @param aSource Debugger.Source
    *        The source that will be stored.
    * @returns true, if the source was added; false otherwise.
diff --git a/devtools/server/actors/timeline.js b/devtools/server/actors/timeline.js
--- a/devtools/server/actors/timeline.js
+++ b/devtools/server/actors/timeline.js
@@ -29,37 +29,61 @@ exports.TimelineActor = protocol.ActorCl
   /**
    * Initializes this actor with the provided connection and tab actor.
    */
   initialize: function (conn, tabActor) {
     protocol.Actor.prototype.initialize.call(this, conn);
     this.tabActor = tabActor;
     this.bridge = new Timeline(tabActor);
 
-    this._onTimelineEvent = this._onTimelineEvent.bind(this);
-    this.bridge.on("*", this._onTimelineEvent);
+    this._onTimelineDocLoading = this._onTimelineDocLoading.bind(this);
+    this._onTimelineMarkers = this._onTimelineMarkers.bind(this);
+    this._onTimelineTicks = this._onTimelineTicks.bind(this);
+    this._onTimelineMemory = this._onTimelineMemory.bind(this);
+    this._onTimelineFrames = this._onTimelineFrames.bind(this);
+
+    this.bridge.on("doc-loading", this._onTimelineDocLoading);
+    this.bridge.on("markers", this._onTimelineMarkers);
+    this.bridge.on("ticks", this._onTimelineTicks);
+    this.bridge.on("memory", this._onTimelineMemory);
+    this.bridge.on("frames", this._onTimelineFrames);
   },
 
   /**
    * Destroys this actor, stopping recording first.
    */
   destroy: function () {
-    this.bridge.off("*", this._onTimelineEvent);
+    this.bridge.off("doc-loading", this._onTimelineDocLoading);
+    this.bridge.off("markers", this._onTimelineMarkers);
+    this.bridge.off("ticks", this._onTimelineTicks);
+    this.bridge.off("memory", this._onTimelineMemory);
+    this.bridge.off("frames", this._onTimelineFrames);
     this.bridge.destroy();
     this.bridge = null;
     this.tabActor = null;
     protocol.Actor.prototype.destroy.call(this);
   },
 
   /**
-   * Propagate events from the Timeline module over RDP if the event is defined
-   * here.
+   * Propagate events from the Timeline module over RDP if the event is defined here.
    */
-  _onTimelineEvent: function (eventName, ...args) {
-    this.emit(eventName, ...args);
+  _onTimelineDocLoading: function (...args) {
+    this.emit("doc-loading", ...args);
+  },
+  _onTimelineMarkers: function (...args) {
+    this.emit("markers", ...args);
+  },
+  _onTimelineTicks: function (...args) {
+    this.emit("ticks", ...args);
+  },
+  _onTimelineMemory: function (...args) {
+    this.emit("memory", ...args);
+  },
+  _onTimelineFrames: function (...args) {
+    this.emit("frames", ...args);
   },
 
   isRecording: actorBridgeWithSpec("isRecording", {
     request: {},
     response: {
       value: RetVal("boolean")
     }
   }),
diff --git a/devtools/server/actors/utils/TabSources.js b/devtools/server/actors/utils/TabSources.js
--- a/devtools/server/actors/utils/TabSources.js
+++ b/devtools/server/actors/utils/TabSources.js
@@ -1,17 +1,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const DevToolsUtils = require("devtools/shared/DevToolsUtils");
 const { assert, fetch } = DevToolsUtils;
-const EventEmitter = require("devtools/shared/old-event-emitter");
+const EventEmitter = require("devtools/shared/event-emitter");
 const { OriginalLocation, GeneratedLocation } = require("devtools/server/actors/common");
 const { joinURI } = require("devtools/shared/path");
 
 loader.lazyRequireGetter(this, "SourceActor", "devtools/server/actors/source", true);
 loader.lazyRequireGetter(this, "isEvalSource", "devtools/server/actors/source", true);
 loader.lazyRequireGetter(this, "SourceMapConsumer", "source-map", true);
 loader.lazyRequireGetter(this, "SourceMapGenerator", "source-map", true);
 loader.lazyRequireGetter(this, "WasmRemap", "devtools/shared/wasm-source-map", true);
diff --git a/devtools/server/actors/utils/make-debugger.js b/devtools/server/actors/utils/make-debugger.js
--- a/devtools/server/actors/utils/make-debugger.js
+++ b/devtools/server/actors/utils/make-debugger.js
@@ -1,17 +1,17 @@
 /* -*- indent-tabs-mode: nil; js-indent-level: 2; js-indent-level: 2 -*- */
 /* vim: set ft=javascript ts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
-const EventEmitter = require("devtools/shared/old-event-emitter");
+const EventEmitter = require("devtools/shared/event-emitter");
 const Debugger = require("Debugger");
 
 const { reportException } = require("devtools/shared/DevToolsUtils");
 
 /**
  * Multiple actors that use a |Debugger| instance come in a few versions, each
  * with a different set of debuggees. One version for content tabs (globals
  * within a tab), one version for chrome debugging (all globals), and sometimes
diff --git a/devtools/server/main.js b/devtools/server/main.js
--- a/devtools/server/main.js
+++ b/devtools/server/main.js
@@ -12,17 +12,16 @@ var { Ci, Cc } = require("chrome");
 var Services = require("Services");
 var { ActorPool, OriginalLocation, RegisteredActorFactory,
       ObservedActorFactory } = require("devtools/server/actors/common");
 var { LocalDebuggerTransport, ChildDebuggerTransport, WorkerDebuggerTransport } =
   require("devtools/shared/transport/transport");
 var DevToolsUtils = require("devtools/shared/DevToolsUtils");
 var { dumpn } = DevToolsUtils;
 var flags = require("devtools/shared/flags");
-var OldEventEmitter = require("devtools/shared/old-event-emitter");
 var SyncPromise = require("devtools/shared/deprecated-sync-thenables");
 
 DevToolsUtils.defineLazyGetter(this, "DebuggerSocket", () => {
   let { DebuggerSocket } = require("devtools/shared/security/socket");
   return DebuggerSocket;
 });
 DevToolsUtils.defineLazyGetter(this, "Authentication", () => {
   return require("devtools/shared/security/auth");
@@ -1379,17 +1378,17 @@ var DebuggerServer = {
 // Expose these to save callers the trouble of importing DebuggerSocket
 DevToolsUtils.defineLazyGetter(DebuggerServer, "Authenticators", () => {
   return Authentication.Authenticators;
 });
 DevToolsUtils.defineLazyGetter(DebuggerServer, "AuthenticationResult", () => {
   return Authentication.AuthenticationResult;
 });
 
-OldEventEmitter.decorate(DebuggerServer);
+EventEmitter.decorate(DebuggerServer);
 
 if (this.exports) {
   exports.DebuggerServer = DebuggerServer;
   exports.ActorPool = ActorPool;
   exports.OriginalLocation = OriginalLocation;
 }
 
 // Needed on B2G (See header note)
diff --git a/devtools/server/tests/unit/test_layout-reflows-observer.js b/devtools/server/tests/unit/test_layout-reflows-observer.js
--- a/devtools/server/tests/unit/test_layout-reflows-observer.js
+++ b/devtools/server/tests/unit/test_layout-reflows-observer.js
@@ -127,17 +127,17 @@ function eventsAreBatched() {
 
   // Note that in this test, we mock the TabActor and its window property, so we
   // also mock the setTimeout/clearTimeout mechanism and just call the callback
   // manually
   let tabActor = new MockTabActor();
   let observer = getLayoutChangesObserver(tabActor);
 
   let reflowsEvents = [];
-  let onReflows = (event, reflows) => reflowsEvents.push(reflows);
+  let onReflows = reflows => reflowsEvents.push(reflows);
   observer.on("reflows", onReflows);
 
   let resizeEvents = [];
   let onResize = () => resizeEvents.push("resize");
   observer.on("resize", onResize);
 
   info("Fake one reflow event");
   tabActor.window.docShell.observer.reflow();
@@ -170,17 +170,17 @@ function eventsAreBatched() {
 function noEventsAreSentWhenThereAreNoReflowsAndLoopTimeouts() {
   info("Checking that if no reflows were detected and the event batching " +
   "loop expires, then no reflows event is sent");
 
   let tabActor = new MockTabActor();
   let observer = getLayoutChangesObserver(tabActor);
 
   let reflowsEvents = [];
-  let onReflows = (event, reflows) => reflowsEvents.push(reflows);
+  let onReflows = (reflows) => reflowsEvents.push(reflows);
   observer.on("reflows", onReflows);
 
   info("Faking timeout expiration and checking for reflows");
   observer.eventLoopTimer();
   Assert.equal(reflowsEvents.length, 0);
 
   observer.off("reflows", onReflows);
   releaseLayoutChangesObserver(tabActor);
diff --git a/devtools/server/tests/unit/test_registerClient.js b/devtools/server/tests/unit/test_registerClient.js
--- a/devtools/server/tests/unit/test_registerClient.js
+++ b/devtools/server/tests/unit/test_registerClient.js
@@ -1,16 +1,16 @@
 /* Any copyright is dedicated to the Public Domain.
    http://creativecommons.org/publicdomain/zero/1.0/ */
 
 "use strict";
 
 // Test the DebuggerClient.registerClient API
 
-var EventEmitter = require("devtools/shared/old-event-emitter");
+var EventEmitter = require("devtools/shared/event-emitter");
 
 var gClient;
 var gTestClient;
 
 function TestActor(conn) {
   this.conn = conn;
 }
 TestActor.prototype = {
@@ -69,18 +69,17 @@ function init() {
     .then(response => {
       gTestClient = new TestClient(gClient, response);
       run_next_test();
     });
 }
 
 function test_client_events() {
   // Test DebuggerClient.registerClient and DebuggerServerConnection.sendActorEvent
-  gTestClient.on("foo", function (type, data) {
-    Assert.equal(type, "foo");
+  gTestClient.on("foo", function (data) {
     Assert.equal(data.hello, "world");
     run_next_test();
   });
   gTestClient.start();
 }
 
 function close_client() {
   gClient.close().then(() => {
diff --git a/devtools/shared/transport/stream-utils.js b/devtools/shared/transport/stream-utils.js
--- a/devtools/shared/transport/stream-utils.js
+++ b/devtools/shared/transport/stream-utils.js
@@ -3,17 +3,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
 const { Ci, Cc, Cr, CC } = require("chrome");
 const Services = require("Services");
 const DevToolsUtils = require("devtools/shared/DevToolsUtils");
 const { dumpv } = DevToolsUtils;
-const EventEmitter = require("devtools/shared/old-event-emitter");
+const EventEmitter = require("devtools/shared/event-emitter");
 const defer = require("devtools/shared/defer");
 
 DevToolsUtils.defineLazyGetter(this, "IOUtil", () => {
   return Cc["@mozilla.org/io-util;1"].getService(Ci.nsIIOUtil);
 });
 
 DevToolsUtils.defineLazyGetter(this, "ScriptableInputStream", () => {
   return CC("@mozilla.org/scriptableinputstream;1",
diff --git a/devtools/shared/transport/tests/unit/test_client_server_bulk.js b/devtools/shared/transport/tests/unit/test_client_server_bulk.js
--- a/devtools/shared/transport/tests/unit/test_client_server_bulk.js
+++ b/devtools/shared/transport/tests/unit/test_client_server_bulk.js
@@ -174,17 +174,17 @@ var test_bulk_request_cs = async functio
 
     // Set up reply handling for this type
     replyHandlers[replyType](request).then(() => {
       client.close();
       transport.close();
     });
   }).catch(do_throw);
 
-  DebuggerServer.on("connectionchange", (event, type) => {
+  DebuggerServer.on("connectionchange", type => {
     if (type === "closed") {
       serverDeferred.resolve();
     }
   });
 
   return promise.all([
     clientDeferred.promise,
     bulkCopyDeferred.promise,
@@ -216,17 +216,17 @@ var test_json_request_cs = async functio
 
     // Set up reply handling for this type
     replyHandlers[replyType](request).then(() => {
       client.close();
       transport.close();
     });
   }).catch(do_throw);
 
-  DebuggerServer.on("connectionchange", (event, type) => {
+  DebuggerServer.on("connectionchange", type => {
     if (type === "closed") {
       serverDeferred.resolve();
     }
   });
 
   return promise.all([
     clientDeferred.promise,
     serverDeferred.promise
diff --git a/devtools/shared/transport/tests/unit/test_queue.js b/devtools/shared/transport/tests/unit/test_queue.js
--- a/devtools/shared/transport/tests/unit/test_queue.js
+++ b/devtools/shared/transport/tests/unit/test_queue.js
@@ -107,17 +107,17 @@ var test_transport = async function (tra
 
       // Server
       Assert.equal(Object.keys(DebuggerServer._connections).length, 1);
       info(Object.keys(DebuggerServer._connections));
       for (let connId in DebuggerServer._connections) {
         DebuggerServer._connections[connId].onBulkPacket = on_bulk_packet;
       }
 
-      DebuggerServer.on("connectionchange", (event, type) => {
+      DebuggerServer.on("connectionchange", type => {
         if (type === "closed") {
           serverDeferred.resolve();
         }
       });
 
       send_packets();
     },
 
diff --git a/devtools/shared/transport/tests/unit/test_transport_bulk.js b/devtools/shared/transport/tests/unit/test_transport_bulk.js
--- a/devtools/shared/transport/tests/unit/test_transport_bulk.js
+++ b/devtools/shared/transport/tests/unit/test_transport_bulk.js
@@ -80,17 +80,17 @@ var test_bulk_transfer_transport = async
 
       // Server
       Assert.equal(Object.keys(DebuggerServer._connections).length, 1);
       info(Object.keys(DebuggerServer._connections));
       for (let connId in DebuggerServer._connections) {
         DebuggerServer._connections[connId].onBulkPacket = on_bulk_packet;
       }
 
-      DebuggerServer.on("connectionchange", (event, type) => {
+      DebuggerServer.on("connectionchange", type => {
         if (type === "closed") {
           serverDeferred.resolve();
         }
       });
 
       transport.startBulkSend({
         actor: "root",
         type: "file-stream",
diff --git a/devtools/shared/transport/tests/unit/test_transport_events.js b/devtools/shared/transport/tests/unit/test_transport_events.js
--- a/devtools/shared/transport/tests/unit/test_transport_events.js
+++ b/devtools/shared/transport/tests/unit/test_transport_events.js
@@ -23,51 +23,46 @@ async function test_transport_events(nam
   let transport = await transportFactory();
 
   // Transport expects the hooks to be not null
   transport.hooks = {
     onPacket: () => {},
     onClosed: () => {},
   };
 
-  let rootReceived = transport.once("packet", (event, packet) => {
-    info(`Packet event: ${event} ${JSON.stringify(packet)}`);
-    Assert.equal(event, "packet");
+  let rootReceived = transport.once("packet", packet => {
+    info(`Packet event: ${JSON.stringify(packet)}`);
     Assert.equal(packet.from, "root");
   });
 
   transport.ready();
   await rootReceived;
 
-  let echoSent = transport.once("send", (event, packet) => {
-    info(`Send event: ${event} ${JSON.stringify(packet)}`);
-    Assert.equal(event, "send");
+  let echoSent = transport.once("send", packet => {
+    info(`Send event: ${JSON.stringify(packet)}`);
     Assert.equal(packet.to, "root");
     Assert.equal(packet.type, "echo");
   });
 
-  let echoReceived = transport.once("packet", (event, packet) => {
-    info(`Packet event: ${event} ${JSON.stringify(packet)}`);
-    Assert.equal(event, "packet");
+  let echoReceived = transport.once("packet", packet => {
+    info(`Packet event: ${JSON.stringify(packet)}`);
     Assert.equal(packet.from, "root");
     Assert.equal(packet.type, "echo");
   });
 
   transport.send({ to: "root", type: "echo" });
   await echoSent;
   await echoReceived;
 
-  let clientClosed = transport.once("close", (event) => {
-    info(`Close event: ${event}`);
-    Assert.equal(event, "close");
+  let clientClosed = transport.once("close", () => {
+    info(`Close event`);
   });
 
-  let serverClosed = DebuggerServer.once("connectionchange", (event, type) => {
+  let serverClosed = DebuggerServer.once("connectionchange", type => {
     info(`Server closed`);
-    Assert.equal(event, "connectionchange");
     Assert.equal(type, "closed");
   });
 
   transport.close();
 
   await clientClosed;
   await serverClosed;
 
diff --git a/devtools/shared/transport/transport.js b/devtools/shared/transport/transport.js
--- a/devtools/shared/transport/transport.js
+++ b/devtools/shared/transport/transport.js
@@ -25,17 +25,17 @@
   const DevToolsUtils = require("devtools/shared/DevToolsUtils");
   const { dumpn, dumpv } = DevToolsUtils;
   const flags = require("devtools/shared/flags");
   const StreamUtils = require("devtools/shared/transport/stream-utils");
   const { Packet, JSONPacket, BulkPacket } =
   require("devtools/shared/transport/packets");
   const promise = require("promise");
   const defer = require("devtools/shared/defer");
-  const EventEmitter = require("devtools/shared/old-event-emitter");
+  const EventEmitter = require("devtools/shared/event-emitter");
 
   DevToolsUtils.defineLazyGetter(this, "Pipe", () => {
     return CC("@mozilla.org/pipe;1", "nsIPipe", "init");
   });
 
   DevToolsUtils.defineLazyGetter(this, "ScriptableInputStream", () => {
     return CC("@mozilla.org/scriptableinputstream;1",
             "nsIScriptableInputStream", "init");
diff --git a/devtools/shared/transport/websocket-transport.js b/devtools/shared/transport/websocket-transport.js
--- a/devtools/shared/transport/websocket-transport.js
+++ b/devtools/shared/transport/websocket-transport.js
@@ -1,15 +1,15 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 "use strict";
 
-const EventEmitter = require("devtools/shared/old-event-emitter");
+const EventEmitter = require("devtools/shared/event-emitter");
 
 function WebSocketDebuggerTransport(socket) {
   EventEmitter.decorate(this);
 
   this.active = false;
   this.hooks = null;
   this.socket = socket;
 }
