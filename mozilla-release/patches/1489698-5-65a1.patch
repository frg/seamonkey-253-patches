# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1536361555 14400
#      Fri Sep 07 19:05:55 2018 -0400
# Node ID a17dfbac6b10ec140dc69ed3053662987eba25ef
# Parent  d5803dbb51b2399a89c048849c799661382b0005
Bug 1489698 - Add moz.build for js/src/jit. r=jandem,froydnj

diff --git a/js/src/jit/moz.build b/js/src/jit/moz.build
new file mode 100644
--- /dev/null
+++ b/js/src/jit/moz.build
@@ -0,0 +1,246 @@
+# -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
+# vim: set filetype=python:
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+FINAL_LIBRARY = 'js'
+
+FILES_PER_UNIFIED_FILE = 6
+
+# Includes should be relative to parent path
+LOCAL_INCLUDES += [
+    '!..',
+    '..'
+]
+
+include('../js-config.mozbuild')
+include('../js-cxxflags.mozbuild')
+
+
+UNIFIED_SOURCES += [
+    'AliasAnalysis.cpp',
+    'AliasAnalysisShared.cpp',
+    'AlignmentMaskAnalysis.cpp',
+    'BacktrackingAllocator.cpp',
+    'Bailouts.cpp',
+    'BaselineBailouts.cpp',
+    'BaselineCacheIRCompiler.cpp',
+    'BaselineCompiler.cpp',
+    'BaselineDebugModeOSR.cpp',
+    'BaselineFrame.cpp',
+    'BaselineFrameInfo.cpp',
+    'BaselineIC.cpp',
+    'BaselineInspector.cpp',
+    'BaselineJIT.cpp',
+    'BitSet.cpp',
+    'BytecodeAnalysis.cpp',
+    'CacheIR.cpp',
+    'CacheIRCompiler.cpp',
+    'CacheIRSpewer.cpp',
+    'CodeGenerator.cpp',
+    'CompileWrappers.cpp',
+    'Disassembler.cpp',
+    'EagerSimdUnbox.cpp',
+    'EdgeCaseAnalysis.cpp',
+    'EffectiveAddressAnalysis.cpp',
+    'ExecutableAllocator.cpp',
+    'FlowAliasAnalysis.cpp',
+    'FoldLinearArithConstants.cpp',
+    'InstructionReordering.cpp',
+    'Ion.cpp',
+    'IonAnalysis.cpp',
+    'IonBuilder.cpp',
+    'IonCacheIRCompiler.cpp',
+    'IonControlFlow.cpp',
+    'IonIC.cpp',
+    'IonOptimizationLevels.cpp',
+    'Jit.cpp',
+    'JitcodeMap.cpp',
+    'JitFrames.cpp',
+    'JitOptions.cpp',
+    'JitSpewer.cpp',
+    'JSJitFrameIter.cpp',
+    'JSONSpewer.cpp',
+    'LICM.cpp',
+    'Linker.cpp',
+    'LIR.cpp',
+    'LoopUnroller.cpp',
+    'Lowering.cpp',
+    'MacroAssembler.cpp',
+    'MCallOptimize.cpp',
+    'MIR.cpp',
+    'MIRGraph.cpp',
+    'MoveResolver.cpp',
+    'OptimizationTracking.cpp',
+    'PerfSpewer.cpp',
+    'ProcessExecutableMemory.cpp',
+    'RangeAnalysis.cpp',
+    'Recover.cpp',
+    'RegisterAllocator.cpp',
+    'RematerializedFrame.cpp',
+    'Safepoints.cpp',
+    'ScalarReplacement.cpp',
+    'shared/Assembler-shared.cpp',
+    'shared/BaselineCompiler-shared.cpp',
+    'shared/CodeGenerator-shared.cpp',
+    'shared/Disassembler-shared.cpp',
+    'shared/Lowering-shared.cpp',
+    'SharedIC.cpp',
+    'Sink.cpp',
+    'Snapshots.cpp',
+    'StupidAllocator.cpp',
+    'TypedObjectPrediction.cpp',
+    'TypePolicy.cpp',
+    'ValueNumbering.cpp',
+    'VMFunctions.cpp',
+    'WasmBCE.cpp',
+]
+
+
+if not CONFIG['ENABLE_ION']:
+    UNIFIED_SOURCES += [
+        'none/Trampoline-none.cpp'
+    ]
+elif CONFIG['JS_CODEGEN_X86'] or CONFIG['JS_CODEGEN_X64']:
+    UNIFIED_SOURCES += [
+        'x86-shared/Architecture-x86-shared.cpp',
+        'x86-shared/Assembler-x86-shared.cpp',
+        'x86-shared/AssemblerBuffer-x86-shared.cpp',
+        'x86-shared/BaselineCompiler-x86-shared.cpp',
+        'x86-shared/BaselineIC-x86-shared.cpp',
+        'x86-shared/CodeGenerator-x86-shared.cpp',
+        'x86-shared/Lowering-x86-shared.cpp',
+        'x86-shared/MacroAssembler-x86-shared.cpp',
+        'x86-shared/MoveEmitter-x86-shared.cpp',
+    ]
+    SOURCES += [
+        'x86-shared/Disassembler-x86-shared.cpp',  # using namespace js::jit::X86Encoding;
+    ]
+    if CONFIG['JS_CODEGEN_X64']:
+        UNIFIED_SOURCES += [
+            'x64/Assembler-x64.cpp',
+            'x64/Bailouts-x64.cpp',
+            'x64/BaselineCompiler-x64.cpp',
+            'x64/BaselineIC-x64.cpp',
+            'x64/CodeGenerator-x64.cpp',
+            'x64/Lowering-x64.cpp',
+            'x64/MacroAssembler-x64.cpp',
+            'x64/SharedIC-x64.cpp',
+            'x64/Trampoline-x64.cpp',
+        ]
+    else:
+        UNIFIED_SOURCES += [
+            'x86/Assembler-x86.cpp',
+            'x86/Bailouts-x86.cpp',
+            'x86/BaselineCompiler-x86.cpp',
+            'x86/BaselineIC-x86.cpp',
+            'x86/CodeGenerator-x86.cpp',
+            'x86/Lowering-x86.cpp',
+            'x86/MacroAssembler-x86.cpp',
+            'x86/SharedIC-x86.cpp',
+            'x86/Trampoline-x86.cpp',
+        ]
+elif CONFIG['JS_CODEGEN_ARM']:
+    UNIFIED_SOURCES += [
+        'arm/Architecture-arm.cpp',
+        'arm/Assembler-arm.cpp',
+        'arm/Bailouts-arm.cpp',
+        'arm/BaselineCompiler-arm.cpp',
+        'arm/BaselineIC-arm.cpp',
+        'arm/CodeGenerator-arm.cpp',
+        'arm/disasm/Constants-arm.cpp',
+        'arm/disasm/Disasm-arm.cpp',
+        'arm/Lowering-arm.cpp',
+        'arm/MacroAssembler-arm.cpp',
+        'arm/MoveEmitter-arm.cpp',
+        'arm/SharedIC-arm.cpp',
+        'arm/Trampoline-arm.cpp',
+    ]
+    if CONFIG['JS_SIMULATOR_ARM']:
+        UNIFIED_SOURCES += [
+            'arm/Simulator-arm.cpp'
+        ]
+    elif CONFIG['OS_ARCH'] == 'Darwin':
+        SOURCES += [
+            'arm/llvm-compiler-rt/arm/aeabi_idivmod.S',
+            'arm/llvm-compiler-rt/arm/aeabi_uidivmod.S',
+        ]
+elif CONFIG['JS_CODEGEN_ARM64']:
+    UNIFIED_SOURCES += [
+        'arm64/Architecture-arm64.cpp',
+        'arm64/Assembler-arm64.cpp',
+        'arm64/Bailouts-arm64.cpp',
+        'arm64/BaselineIC-arm64.cpp',
+        'arm64/CodeGenerator-arm64.cpp',
+        'arm64/Lowering-arm64.cpp',
+        'arm64/MacroAssembler-arm64.cpp',
+        'arm64/MoveEmitter-arm64.cpp',
+        'arm64/SharedIC-arm64.cpp',
+        'arm64/Trampoline-arm64.cpp',
+        'arm64/vixl/Assembler-vixl.cpp',
+        'arm64/vixl/Cpu-vixl.cpp',
+        'arm64/vixl/Decoder-vixl.cpp',
+        'arm64/vixl/Disasm-vixl.cpp',
+        'arm64/vixl/Instructions-vixl.cpp',
+        'arm64/vixl/Instrument-vixl.cpp',
+        'arm64/vixl/MacroAssembler-vixl.cpp',
+        'arm64/vixl/MozAssembler-vixl.cpp',
+        'arm64/vixl/MozInstructions-vixl.cpp',
+        'arm64/vixl/Utils-vixl.cpp'
+    ]
+    if CONFIG['JS_SIMULATOR_ARM64']:
+        UNIFIED_SOURCES += [
+            'arm64/vixl/Debugger-vixl.cpp',
+            'arm64/vixl/Logic-vixl.cpp',
+            'arm64/vixl/MozSimulator-vixl.cpp',
+            'arm64/vixl/Simulator-vixl.cpp'
+        ]
+elif CONFIG['JS_CODEGEN_MIPS32'] or CONFIG['JS_CODEGEN_MIPS64']:
+    UNIFIED_SOURCES += [
+        'mips-shared/Architecture-mips-shared.cpp',
+        'mips-shared/Assembler-mips-shared.cpp',
+        'mips-shared/Bailouts-mips-shared.cpp',
+        'mips-shared/BaselineCompiler-mips-shared.cpp',
+        'mips-shared/BaselineIC-mips-shared.cpp',
+        'mips-shared/CodeGenerator-mips-shared.cpp',
+        'mips-shared/Lowering-mips-shared.cpp',
+        'mips-shared/MacroAssembler-mips-shared.cpp',
+        'mips-shared/MoveEmitter-mips-shared.cpp',
+    ]
+    if CONFIG['JS_CODEGEN_MIPS32']:
+        UNIFIED_SOURCES += [
+            'mips32/Architecture-mips32.cpp',
+            'mips32/Assembler-mips32.cpp',
+            'mips32/Bailouts-mips32.cpp',
+            'mips32/BaselineCompiler-mips32.cpp',
+            'mips32/BaselineIC-mips32.cpp',
+            'mips32/CodeGenerator-mips32.cpp',
+            'mips32/Lowering-mips32.cpp',
+            'mips32/MacroAssembler-mips32.cpp',
+            'mips32/MoveEmitter-mips32.cpp',
+            'mips32/SharedIC-mips32.cpp',
+            'mips32/Trampoline-mips32.cpp',
+        ]
+        if CONFIG['JS_SIMULATOR_MIPS32']:
+            UNIFIED_SOURCES += [
+                'mips32/Simulator-mips32.cpp'
+            ]
+    elif CONFIG['JS_CODEGEN_MIPS64']:
+        UNIFIED_SOURCES += [
+            'mips64/Architecture-mips64.cpp',
+            'mips64/Assembler-mips64.cpp',
+            'mips64/Bailouts-mips64.cpp',
+            'mips64/BaselineCompiler-mips64.cpp',
+            'mips64/BaselineIC-mips64.cpp',
+            'mips64/CodeGenerator-mips64.cpp',
+            'mips64/Lowering-mips64.cpp',
+            'mips64/MacroAssembler-mips64.cpp',
+            'mips64/MoveEmitter-mips64.cpp',
+            'mips64/SharedIC-mips64.cpp',
+            'mips64/Trampoline-mips64.cpp',
+        ]
+        if CONFIG['JS_SIMULATOR_MIPS64']:
+            UNIFIED_SOURCES += [
+                'mips64/Simulator-mips64.cpp'
+            ]
diff --git a/js/src/moz.build b/js/src/moz.build
--- a/js/src/moz.build
+++ b/js/src/moz.build
@@ -223,93 +223,16 @@ UNIFIED_SOURCES += [
     'irregexp/NativeRegExpMacroAssembler.cpp',
     'irregexp/RegExpAST.cpp',
     'irregexp/RegExpCharacters.cpp',
     'irregexp/RegExpEngine.cpp',
     'irregexp/RegExpInterpreter.cpp',
     'irregexp/RegExpMacroAssembler.cpp',
     'irregexp/RegExpParser.cpp',
     'irregexp/RegExpStack.cpp',
-    'jit/AliasAnalysis.cpp',
-    'jit/AliasAnalysisShared.cpp',
-    'jit/AlignmentMaskAnalysis.cpp',
-    'jit/BacktrackingAllocator.cpp',
-    'jit/Bailouts.cpp',
-    'jit/BaselineBailouts.cpp',
-    'jit/BaselineCacheIRCompiler.cpp',
-    'jit/BaselineCompiler.cpp',
-    'jit/BaselineDebugModeOSR.cpp',
-    'jit/BaselineFrame.cpp',
-    'jit/BaselineFrameInfo.cpp',
-    'jit/BaselineIC.cpp',
-    'jit/BaselineInspector.cpp',
-    'jit/BaselineJIT.cpp',
-    'jit/BitSet.cpp',
-    'jit/BytecodeAnalysis.cpp',
-    'jit/C1Spewer.cpp',
-    'jit/CacheIR.cpp',
-    'jit/CacheIRCompiler.cpp',
-    'jit/CacheIRSpewer.cpp',
-    'jit/CodeGenerator.cpp',
-    'jit/CompileWrappers.cpp',
-    'jit/Disassembler.cpp',
-    'jit/EagerSimdUnbox.cpp',
-    'jit/EdgeCaseAnalysis.cpp',
-    'jit/EffectiveAddressAnalysis.cpp',
-    'jit/ExecutableAllocator.cpp',
-    'jit/FlowAliasAnalysis.cpp',
-    'jit/FoldLinearArithConstants.cpp',
-    'jit/InstructionReordering.cpp',
-    'jit/Ion.cpp',
-    'jit/IonAnalysis.cpp',
-    'jit/IonBuilder.cpp',
-    'jit/IonCacheIRCompiler.cpp',
-    'jit/IonControlFlow.cpp',
-    'jit/IonIC.cpp',
-    'jit/IonOptimizationLevels.cpp',
-    'jit/Jit.cpp',
-    'jit/JitcodeMap.cpp',
-    'jit/JitFrames.cpp',
-    'jit/JitOptions.cpp',
-    'jit/JitSpewer.cpp',
-    'jit/JSJitFrameIter.cpp',
-    'jit/JSONSpewer.cpp',
-    'jit/LICM.cpp',
-    'jit/Linker.cpp',
-    'jit/LIR.cpp',
-    'jit/LoopUnroller.cpp',
-    'jit/Lowering.cpp',
-    'jit/MacroAssembler.cpp',
-    'jit/MCallOptimize.cpp',
-    'jit/MIR.cpp',
-    'jit/MIRGraph.cpp',
-    'jit/MoveResolver.cpp',
-    'jit/OptimizationTracking.cpp',
-    'jit/PerfSpewer.cpp',
-    'jit/ProcessExecutableMemory.cpp',
-    'jit/RangeAnalysis.cpp',
-    'jit/Recover.cpp',
-    'jit/RegisterAllocator.cpp',
-    'jit/RematerializedFrame.cpp',
-    'jit/Safepoints.cpp',
-    'jit/ScalarReplacement.cpp',
-    'jit/shared/Assembler-shared.cpp',
-    'jit/shared/BaselineCompiler-shared.cpp',
-    'jit/shared/CodeGenerator-shared.cpp',
-    'jit/shared/Disassembler-shared.cpp',
-    'jit/shared/Lowering-shared.cpp',
-    'jit/SharedIC.cpp',
-    'jit/Sink.cpp',
-    'jit/Snapshots.cpp',
-    'jit/StupidAllocator.cpp',
-    'jit/TypedObjectPrediction.cpp',
-    'jit/TypePolicy.cpp',
-    'jit/ValueNumbering.cpp',
-    'jit/VMFunctions.cpp',
-    'jit/WasmBCE.cpp',
     'jsalloc.cpp',
     'jsapi.cpp',
     'jsarray.cpp',
     'jsbool.cpp',
     'jsdate.cpp',
     'jsexn.cpp',
     'jsfriendapi.cpp',
     'jsgc.cpp',
@@ -461,163 +384,16 @@ if CONFIG['MOZ_INSTRUMENTS']:
 
 if CONFIG['ENABLE_TRACE_LOGGING']:
     SOURCES += [
         'vm/TraceLogging.cpp',
         'vm/TraceLoggingGraph.cpp',
         'vm/TraceLoggingTypes.cpp',
     ]
 
-if not CONFIG['ENABLE_ION']:
-    UNIFIED_SOURCES += [
-        'jit/none/Trampoline-none.cpp'
-    ]
-elif CONFIG['JS_CODEGEN_X86'] or CONFIG['JS_CODEGEN_X64']:
-    UNIFIED_SOURCES += [
-        'jit/x86-shared/Architecture-x86-shared.cpp',
-        'jit/x86-shared/Assembler-x86-shared.cpp',
-        'jit/x86-shared/AssemblerBuffer-x86-shared.cpp',
-        'jit/x86-shared/BaselineCompiler-x86-shared.cpp',
-        'jit/x86-shared/BaselineIC-x86-shared.cpp',
-        'jit/x86-shared/CodeGenerator-x86-shared.cpp',
-        'jit/x86-shared/Lowering-x86-shared.cpp',
-        'jit/x86-shared/MacroAssembler-x86-shared.cpp',
-        'jit/x86-shared/MoveEmitter-x86-shared.cpp',
-    ]
-    SOURCES += [
-        'jit/x86-shared/Disassembler-x86-shared.cpp',  # using namespace js::jit::X86Encoding;
-    ]
-    if CONFIG['JS_CODEGEN_X64']:
-        UNIFIED_SOURCES += [
-            'jit/x64/Assembler-x64.cpp',
-            'jit/x64/Bailouts-x64.cpp',
-            'jit/x64/BaselineCompiler-x64.cpp',
-            'jit/x64/BaselineIC-x64.cpp',
-            'jit/x64/CodeGenerator-x64.cpp',
-            'jit/x64/Lowering-x64.cpp',
-            'jit/x64/MacroAssembler-x64.cpp',
-            'jit/x64/SharedIC-x64.cpp',
-            'jit/x64/Trampoline-x64.cpp',
-        ]
-    else:
-        UNIFIED_SOURCES += [
-            'jit/x86/Assembler-x86.cpp',
-            'jit/x86/Bailouts-x86.cpp',
-            'jit/x86/BaselineCompiler-x86.cpp',
-            'jit/x86/BaselineIC-x86.cpp',
-            'jit/x86/CodeGenerator-x86.cpp',
-            'jit/x86/Lowering-x86.cpp',
-            'jit/x86/MacroAssembler-x86.cpp',
-            'jit/x86/SharedIC-x86.cpp',
-            'jit/x86/Trampoline-x86.cpp',
-        ]
-elif CONFIG['JS_CODEGEN_ARM']:
-    UNIFIED_SOURCES += [
-        'jit/arm/Architecture-arm.cpp',
-        'jit/arm/Assembler-arm.cpp',
-        'jit/arm/Bailouts-arm.cpp',
-        'jit/arm/BaselineCompiler-arm.cpp',
-        'jit/arm/BaselineIC-arm.cpp',
-        'jit/arm/CodeGenerator-arm.cpp',
-        'jit/arm/disasm/Constants-arm.cpp',
-        'jit/arm/disasm/Disasm-arm.cpp',
-        'jit/arm/Lowering-arm.cpp',
-        'jit/arm/MacroAssembler-arm.cpp',
-        'jit/arm/MoveEmitter-arm.cpp',
-        'jit/arm/SharedIC-arm.cpp',
-        'jit/arm/Trampoline-arm.cpp',
-    ]
-    if CONFIG['JS_SIMULATOR_ARM']:
-        UNIFIED_SOURCES += [
-            'jit/arm/Simulator-arm.cpp'
-        ]
-    elif CONFIG['OS_ARCH'] == 'Darwin':
-        SOURCES += [
-            'jit/arm/llvm-compiler-rt/arm/aeabi_idivmod.S',
-            'jit/arm/llvm-compiler-rt/arm/aeabi_uidivmod.S',
-        ]
-elif CONFIG['JS_CODEGEN_ARM64']:
-    UNIFIED_SOURCES += [
-        'jit/arm64/Architecture-arm64.cpp',
-        'jit/arm64/Assembler-arm64.cpp',
-        'jit/arm64/Bailouts-arm64.cpp',
-        'jit/arm64/BaselineIC-arm64.cpp',
-        'jit/arm64/CodeGenerator-arm64.cpp',
-        'jit/arm64/Lowering-arm64.cpp',
-        'jit/arm64/MacroAssembler-arm64.cpp',
-        'jit/arm64/MoveEmitter-arm64.cpp',
-        'jit/arm64/SharedIC-arm64.cpp',
-        'jit/arm64/Trampoline-arm64.cpp',
-        'jit/arm64/vixl/Assembler-vixl.cpp',
-        'jit/arm64/vixl/Cpu-vixl.cpp',
-        'jit/arm64/vixl/Decoder-vixl.cpp',
-        'jit/arm64/vixl/Disasm-vixl.cpp',
-        'jit/arm64/vixl/Instructions-vixl.cpp',
-        'jit/arm64/vixl/Instrument-vixl.cpp',
-        'jit/arm64/vixl/MacroAssembler-vixl.cpp',
-        'jit/arm64/vixl/MozAssembler-vixl.cpp',
-        'jit/arm64/vixl/MozInstructions-vixl.cpp',
-        'jit/arm64/vixl/Utils-vixl.cpp'
-    ]
-    if CONFIG['JS_SIMULATOR_ARM64']:
-        UNIFIED_SOURCES += [
-            'jit/arm64/vixl/Debugger-vixl.cpp',
-            'jit/arm64/vixl/Logic-vixl.cpp',
-            'jit/arm64/vixl/MozSimulator-vixl.cpp',
-            'jit/arm64/vixl/Simulator-vixl.cpp'
-        ]
-elif CONFIG['JS_CODEGEN_MIPS32'] or CONFIG['JS_CODEGEN_MIPS64']:
-    UNIFIED_SOURCES += [
-        'jit/mips-shared/Architecture-mips-shared.cpp',
-        'jit/mips-shared/Assembler-mips-shared.cpp',
-        'jit/mips-shared/Bailouts-mips-shared.cpp',
-        'jit/mips-shared/BaselineCompiler-mips-shared.cpp',
-        'jit/mips-shared/BaselineIC-mips-shared.cpp',
-        'jit/mips-shared/CodeGenerator-mips-shared.cpp',
-        'jit/mips-shared/Lowering-mips-shared.cpp',
-        'jit/mips-shared/MacroAssembler-mips-shared.cpp',
-        'jit/mips-shared/MoveEmitter-mips-shared.cpp',
-    ]
-    if CONFIG['JS_CODEGEN_MIPS32']:
-        UNIFIED_SOURCES += [
-            'jit/mips32/Architecture-mips32.cpp',
-            'jit/mips32/Assembler-mips32.cpp',
-            'jit/mips32/Bailouts-mips32.cpp',
-            'jit/mips32/BaselineCompiler-mips32.cpp',
-            'jit/mips32/BaselineIC-mips32.cpp',
-            'jit/mips32/CodeGenerator-mips32.cpp',
-            'jit/mips32/Lowering-mips32.cpp',
-            'jit/mips32/MacroAssembler-mips32.cpp',
-            'jit/mips32/MoveEmitter-mips32.cpp',
-            'jit/mips32/SharedIC-mips32.cpp',
-            'jit/mips32/Trampoline-mips32.cpp',
-        ]
-        if CONFIG['JS_SIMULATOR_MIPS32']:
-            UNIFIED_SOURCES += [
-                'jit/mips32/Simulator-mips32.cpp'
-            ]
-    elif CONFIG['JS_CODEGEN_MIPS64']:
-        UNIFIED_SOURCES += [
-            'jit/mips64/Architecture-mips64.cpp',
-            'jit/mips64/Assembler-mips64.cpp',
-            'jit/mips64/Bailouts-mips64.cpp',
-            'jit/mips64/BaselineCompiler-mips64.cpp',
-            'jit/mips64/BaselineIC-mips64.cpp',
-            'jit/mips64/CodeGenerator-mips64.cpp',
-            'jit/mips64/Lowering-mips64.cpp',
-            'jit/mips64/MacroAssembler-mips64.cpp',
-            'jit/mips64/MoveEmitter-mips64.cpp',
-            'jit/mips64/SharedIC-mips64.cpp',
-            'jit/mips64/Trampoline-mips64.cpp',
-        ]
-        if CONFIG['JS_SIMULATOR_MIPS64']:
-            UNIFIED_SOURCES += [
-                'jit/mips64/Simulator-mips64.cpp'
-            ]
-
 if CONFIG['OS_ARCH'] == 'WINNT':
     UNIFIED_SOURCES += [
         'threading/windows/CpuCount.cpp',
         'threading/windows/Thread.cpp',
     ]
 else:
     UNIFIED_SOURCES += [
         'threading/posix/CpuCount.cpp',
@@ -659,16 +435,17 @@ GENERATED_FILES += ['frontend/ReservedWo
 ReservedWordsGenerated = GENERATED_FILES['frontend/ReservedWordsGenerated.h']
 ReservedWordsGenerated.script = 'frontend/GenerateReservedWords.py'
 ReservedWordsGenerated.inputs += [
     'frontend/ReservedWords.h'
 ]
 
 DIRS += [
     'build',
+    'jit',
 ]
 
 FINAL_LIBRARY = 'js'
 
 # Prepare self-hosted JS code for embedding
 GENERATED_FILES += [('selfhosted.out.h', 'selfhosted.js')]
 selfhosted = GENERATED_FILES[('selfhosted.out.h', 'selfhosted.js')]
 selfhosted.script = 'builtin/embedjs.py:generate_selfhosted'
