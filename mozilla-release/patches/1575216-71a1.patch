# HG changeset patch
# User Gabriele Svelto <gsvelto@mozilla.com>
# Date 1568302453 0
# Node ID 2d4e0a16ffdf5c9b88afd55771618b67fdc74315
# Parent  8ddbbc17f5a83df70193fd44389dfecd713a3867
Bug 1575216 - Make low-memory detection use per-process stats to avoid janking the main thread r=dmajor

This reverts the available memory tracker back to its old implementation that
relied on GlobalMemoryStatusEx() instead of the potentially expensive
K32GetPerformanceInfo(). In spite of its name GlobalMemoryStatusEx() may return
per-process values instead of global ones which might lead to underestimating
the actual memory pressure. Because of this limitation this change is a
stop-gap while we replace this code with a non-polling implementation based on
QueryMemoryResourceNotification().

Differential Revision: https://phabricator.services.mozilla.com/D45338

diff --git a/xpcom/base/AvailableMemoryTracker.cpp b/xpcom/base/AvailableMemoryTracker.cpp
--- a/xpcom/base/AvailableMemoryTracker.cpp
+++ b/xpcom/base/AvailableMemoryTracker.cpp
@@ -6,17 +6,16 @@
 
 #include "mozilla/AvailableMemoryTracker.h"
 
 #if defined(XP_WIN)
 #include "nsExceptionHandler.h"
 #include "nsICrashReporter.h"
 #include "nsIMemoryReporter.h"
 #include "nsMemoryPressure.h"
-#include "psapi.h"
 #endif
 
 #include "nsIObserver.h"
 #include "nsIObserverService.h"
 #include "nsIRunnable.h"
 #include "nsISupports.h"
 #include "nsITimer.h"
 #include "nsThreadUtils.h"
@@ -67,18 +66,18 @@ private:
   static const uint32_t kLowMemoryNotificationIntervalMS = 10000;
 
   // Poll the amount of free memory at this rate.
   static const uint32_t kPollingIntervalMS = 1000;
 
   // Observer topics we subscribe to, see below.
   static const char* const kObserverTopics[];
 
-  static bool IsVirtualMemoryLow();
-  static bool IsCommitSpaceLow();
+  static bool IsVirtualMemoryLow(const MEMORYSTATUSEX& aStat);
+  static bool IsCommitSpaceLow(const MEMORYSTATUSEX& aStat);
 
   ~nsAvailableMemoryWatcher() {};
   bool OngoingMemoryPressure() { return mUnderMemoryPressure; }
   void AdjustPollingInterval(const bool aLowMemory);
   void SendMemoryPressureEvent();
   void MaybeSaveMemoryReport();
   void Shutdown();
 
@@ -133,50 +132,37 @@ nsAvailableMemoryWatcher::Shutdown()
 
   if (mTimer) {
     mTimer->Cancel();
     mTimer = nullptr;
   }
 }
 
 /* static */ bool
-nsAvailableMemoryWatcher::IsVirtualMemoryLow() 
+nsAvailableMemoryWatcher::IsVirtualMemoryLow(const MEMORYSTATUSEX& aStat)
 {
-  if (kLowVirtualMemoryThreshold != 0) {
-    MEMORYSTATUSEX stat;
-    stat.dwLength = sizeof(stat);
-    bool success = GlobalMemoryStatusEx(&stat);
-
-    if (success && (stat.ullAvailVirtual < kLowVirtualMemoryThreshold)) {
-      sNumLowVirtualMemEvents++;
-      return true;
-    }
+  if ((kLowVirtualMemoryThreshold != 0) &&
+      (aStat.ullAvailVirtual < kLowVirtualMemoryThreshold)) {
+    sNumLowVirtualMemEvents++;
+    return true;
   }
 
   return false;
 }
 
 /* static */ bool
-nsAvailableMemoryWatcher::IsCommitSpaceLow()
+nsAvailableMemoryWatcher::IsCommitSpaceLow(const MEMORYSTATUSEX& aStat)
 {
-  if (kLowCommitSpaceThreshold != 0) {
-    PERFORMANCE_INFORMATION info;
-    bool success = K32GetPerformanceInfo(&info, sizeof(info));
-
-    if (success) {
-      size_t commitFree = (info.CommitLimit - info.CommitTotal) * info.PageSize;
-
-      if (commitFree < kLowCommitSpaceThreshold) {
-        sNumLowCommitSpaceEvents++;
-        CrashReporter::AnnotateCrashReport(
-            CrashReporter::Annotation::LowCommitSpaceEvents,
-            uint32_t(sNumLowCommitSpaceEvents));
-        return true;
-      }
-    }
+  if ((kLowCommitSpaceThreshold != 0) &&
+      (aStat.ullAvailPageFile < kLowCommitSpaceThreshold)) {
+    sNumLowCommitSpaceEvents++;
+    CrashReporter::AnnotateCrashReport(
+        CrashReporter::Annotation::LowCommitSpaceEvents,
+        uint32_t(sNumLowCommitSpaceEvents));
+    return true;
   }
 
   return false;
 }
 
 void
 nsAvailableMemoryWatcher::SendMemoryPressureEvent()
 {
@@ -214,28 +200,34 @@ nsAvailableMemoryWatcher::AdjustPollingI
 }
 
 // Timer callback, polls memory stats to detect low-memory conditions. This
 // will send memory-pressure events if memory is running low and adjust the
 // polling interval accordingly.
 NS_IMETHODIMP
 nsAvailableMemoryWatcher::Notify(nsITimer* aTimer)
 {
-  bool lowMemory = IsVirtualMemoryLow() || IsCommitSpaceLow();
+  MEMORYSTATUSEX stat;
+  stat.dwLength = sizeof(stat);
+  bool success = GlobalMemoryStatusEx(&stat);
+
+  if (success) {
+    bool lowMemory = IsVirtualMemoryLow(stat) || IsCommitSpaceLow(stat);
 
-  if (lowMemory) {
-    SendMemoryPressureEvent();
-    MaybeSaveMemoryReport();
-  } else {
-    mSavedReport = false;  // Save a new report if memory gets low again
+    if (lowMemory) {
+      SendMemoryPressureEvent();
+      MaybeSaveMemoryReport();
+    } else {
+      mSavedReport = false;  // Save a new report if memory gets low again
+    }
+
+    AdjustPollingInterval(lowMemory);
+    mUnderMemoryPressure = lowMemory;
   }
 
-  AdjustPollingInterval(lowMemory);
-  mUnderMemoryPressure = lowMemory;
-
   return NS_OK;
 }
 
 // Observer service callback, used to stop the polling timer when the user
 // stops interacting with Firefox and resuming it when they interact again.
 // Also used to shut down the service if the application is quitting.
 NS_IMETHODIMP
 nsAvailableMemoryWatcher::Observe(nsISupports* aSubject, const char* aTopic,
