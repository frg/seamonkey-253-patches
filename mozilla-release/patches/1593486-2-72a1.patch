# HG changeset patch
# User Ted Campbell <tcampbell@mozilla.com>
# Date 1572971094 0
# Node ID 1e50698ac5d2bfc2748682727a913e7c2edc625c
# Parent  a2af53d3a556155ab5cde88b8bceeb49d1317162
Bug 1593486 - Cleanup ./mach jsapi-tests implementation r=sfink,froydnj

Standardize the jsapi-tests logic to match the other spidermonkey check
commands in testing/mach_commands.py.

Differential Revision: https://phabricator.services.mozilla.com/D51530

diff --git a/testing/mach_commands.py b/testing/mach_commands.py
--- a/testing/mach_commands.py
+++ b/testing/mach_commands.py
@@ -263,18 +263,36 @@ class MachCommands(MachCommandBase):
             raise
 
         return 0 if result else 1
 
 
 def executable_name(name):
     return name + '.exe' if sys.platform.startswith('win') else name
 
+
 @CommandProvider
-class CheckSpiderMonkeyCommand(MachCommandBase):
+class SpiderMonkeyTests(MachCommandBase):
+    def has_js_binary(binary):
+        def has_binary(cls):
+            try:
+                name = binary + cls.substs['BIN_SUFFIX']
+            except BuildEnvironmentNotFoundException:
+                return False
+
+            path = os.path.join(cls.topobjdir, 'dist', 'bin', name)
+
+            has_binary.__doc__ = """
+    `{}` not found in <objdir>/dist/bin. Make sure you aren't using an artifact build
+    and try rebuilding with `ac_add_options --enable-js-shell`.
+    """.format(name).lstrip()
+
+            return os.path.isfile(path)
+        return has_binary
+
     @Command('jit-test', category='testing',
              description='Run SpiderMonkey jit-tests in the JavaScript shell.')
     @CommandArgument('--shell', help='The shell to be used')
     @CommandArgument('params', nargs=argparse.REMAINDER,
                      help="Extra arguments to pass down to the test harness.")
     def run_jittests(self, shell, params):
         import subprocess
 
@@ -286,16 +304,33 @@ class CheckSpiderMonkeyCommand(MachComma
             python,
             os.path.join(self.topsrcdir, 'js', 'src',
                          'jit-test', 'jit_test.py'),
             js,
         ] + params
 
         return subprocess.call(jittest_cmd)
 
+    @Command('jsapi-tests', category='testing',
+             conditions=[has_js_binary('jsapi-tests')],
+             description='Run jsapi tests (JavaScript engine).')
+    @CommandArgument('test_name', nargs='?', metavar='N',
+                     help='Test to run. Can be a prefix or omitted. If '
+                     'omitted, the entire test suite is executed.')
+    def run_jsapitests(self, **params):
+        import subprocess
+
+        jsapi_tests_cmd = [
+            os.path.join(self.bindir, executable_name('jsapi-tests'))
+        ]
+        if params['test_name']:
+            jsapi_tests_cmd.append(params['test_name'])
+
+        return subprocess.call(jsapi_tests_cmd)
+
     @Command('check-spidermonkey', category='testing',
              description='Run SpiderMonkey tests (JavaScript engine).')
     @CommandArgument('--valgrind', action='store_true',
                      help='Run jit-test suite with valgrind flag')
     def run_checkspidermonkey(self, **params):
         import subprocess
 
         self.virtualenv_manager.ensure()
@@ -317,19 +352,17 @@ class CheckSpiderMonkeyCommand(MachComma
             python,
             os.path.join(self.topsrcdir, 'js', 'src', 'tests', 'jstests.py'),
             js,
             '--jitflags=all',
         ]
         jstest_result = subprocess.call(jstest_cmd)
 
         print('running jsapi-tests')
-        jsapi_tests_cmd = [os.path.join(
-            self.bindir, executable_name('jsapi-tests'))]
-        jsapi_tests_result = subprocess.call(jsapi_tests_cmd)
+        jsapi_tests_result = self.run_jsapitests(test_name=None)
 
         print('running check-style')
         check_style_cmd = [python, os.path.join(
             self.topsrcdir, 'config', 'check_spidermonkey_style.py')]
         check_style_result = subprocess.call(
             check_style_cmd, cwd=self.topsrcdir)
 
         print('running check-masm')
@@ -346,36 +379,16 @@ class CheckSpiderMonkeyCommand(MachComma
 
         all_passed = jittest_result and jstest_result and jsapi_tests_result and \
             check_style_result and check_masm_result and check_js_msg_result
 
         return all_passed
 
 
 @CommandProvider
-class JsapiTestsCommand(MachCommandBase):
-    @Command('jsapi-tests', category='testing', description='Run jsapi tests (JavaScript engine).')
-    @CommandArgument('test_name', nargs='?', metavar='N',
-                     help='Test to run. Can be a prefix or omitted. If omitted, the entire '
-                     'test suite is executed.')
-    def run_jsapitests(self, **params):
-        import subprocess
-
-        print('running jsapi-tests')
-        jsapi_tests_cmd = [os.path.join(
-            self.bindir, executable_name('jsapi-tests'))]
-        if params['test_name']:
-            jsapi_tests_cmd.append(params['test_name'])
-
-        jsapi_tests_result = subprocess.call(jsapi_tests_cmd)
-
-        return jsapi_tests_result
-
-
-@CommandProvider
 class CramTest(MachCommandBase):
     @Command('cramtest', category='testing',
              description="Mercurial style .t tests for command line applications.")
     @CommandArgument('test_paths', nargs='*', metavar='N',
                      help="Test paths to run. Each path can be a test file or directory. "
                           "If omitted, the entire suite will be run.")
     @CommandArgument('cram_args', nargs=argparse.REMAINDER,
                      help="Extra arguments to pass down to the cram binary. See "
