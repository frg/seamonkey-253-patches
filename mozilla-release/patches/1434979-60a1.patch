# HG changeset patch
# User Jason Orendorff <jorendorff@mozilla.com>
# Date 1517509759 21600
# Node ID 21d459aa8ca8fcf4bf646d586d15db42660e53a3
# Parent  663a36bf36a34363e9e934abe63c586c8c370725
Bug 1434979 - LCovSource::writeScripts mini-bug with odd source notes. r=nbp.

Before this patch, we would only ever add hits on instructions that have source
notes. That is usually right, because that's the only time the line number
changes.

But at the start of a script, this makes us skip instructions until we reach
one with source notes. This interacts badly with SRC_XDELTA notes, which can
appear on any instruction, or even between instructions, because all it means
is "bump the source note pc". So "skip instructions until we see source notes"
is nondeterministic because of SRC_XDELTA's meaninglessness.

The fix is to add hits on the first non-prologue instruction of a script, as
well as instructions that have source notes.

diff --git a/js/src/vm/CodeCoverage.cpp b/js/src/vm/CodeCoverage.cpp
--- a/js/src/vm/CodeCoverage.cpp
+++ b/js/src/vm/CodeCoverage.cpp
@@ -193,35 +193,35 @@ LCovSource::writeScript(JSScript* script
         if (sc) {
             const PCCounts* counts = sc->maybeGetPCCounts(script->pcToOffset(pc));
             if (counts)
                 hits = counts->numExec();
         }
 
         // If we have additional source notes, walk all the source notes of the
         // current pc.
-        if (snpc <= pc) {
+        if (snpc <= pc || !firstLineHasBeenWritten) {
             size_t oldLine = lineno;
-            // Without this check, we'll never write the script's first line
-            if (lineno == script->lineno() && !firstLineHasBeenWritten)
-                oldLine = 0;
             while (!SN_IS_TERMINATOR(sn) && snpc <= pc) {
                 SrcNoteType type = SN_TYPE(sn);
                 if (type == SRC_SETLINE)
                     lineno = size_t(GetSrcNoteOffset(sn, 0));
                 else if (type == SRC_NEWLINE)
                     lineno++;
                 else if (type == SRC_TABLESWITCH)
                     tableswitchExitOffset = GetSrcNoteOffset(sn, 0);
 
                 sn = SN_NEXT(sn);
                 snpc += SN_DELTA(sn);
             }
 
-            if (oldLine != lineno && fallsthrough) {
+            if ((oldLine != lineno || !firstLineHasBeenWritten) &&
+                pc >= script->main() &&
+                fallsthrough)
+            {
                 auto p = linesHit_.lookupForAdd(lineno);
                 if (!p) {
                     if (!linesHit_.add(p, lineno, hits))
                         return false;
                 } else {
                     p->value() += hits;
                 }
 

