# HG changeset patch
# User Markus Stange <mstange@themasta.com>
# Date 1565780707 0
#      Wed Aug 14 11:05:07 2019 +0000
# Node ID 9072a9f04ddd8999c1b6260fca8145776364fc45
# Parent  92608a5b4182a61e8dee5524683b6c8050484fdf
Bug 1565717 - Don't use dynamic symbol lookup for CGLTexImageIOSurface2D. r=mattwoodrow

Differential Revision: https://phabricator.services.mozilla.com/D41810

diff --git a/gfx/2d/MacIOSurface.cpp b/gfx/2d/MacIOSurface.cpp
--- a/gfx/2d/MacIOSurface.cpp
+++ b/gfx/2d/MacIOSurface.cpp
@@ -1,62 +1,57 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "MacIOSurface.h"
 #include <OpenGL/gl.h>
+#include <OpenGL/CGLIOSurface.h>
 #include <QuartzCore/QuartzCore.h>
 #include <dlfcn.h>
 #include "mozilla/RefPtr.h"
 #include "mozilla/Assertions.h"
 #include "GLConsts.h"
 #include "GLContextCGL.h"
 
 using namespace mozilla;
 // IOSurface signatures
 #define IOSURFACE_FRAMEWORK_PATH \
   "/System/Library/Frameworks/IOSurface.framework/IOSurface"
-#define OPENGL_FRAMEWORK_PATH \
-  "/System/Library/Frameworks/OpenGL.framework/OpenGL"
 #define COREVIDEO_FRAMEWORK_PATH \
   "/System/Library/Frameworks/ApplicationServices.framework/Frameworks/" \
   "CoreVideo.framework/CoreVideo"
 
 #define GET_CONST(const_name) \
   ((CFStringRef*) dlsym(sIOSurfaceFramework, const_name))
 #define GET_IOSYM(dest,sym_name) \
   (typeof(dest)) dlsym(sIOSurfaceFramework, sym_name)
-#define GET_CGLSYM(dest,sym_name) \
-  (typeof(dest)) dlsym(sOpenGLFramework, sym_name)
 #define GET_CVSYM(dest, sym_name) \
   (typeof(dest)) dlsym(sCoreVideoFramework, sym_name)
 
 MacIOSurfaceLib::LibraryUnloader MacIOSurfaceLib::sLibraryUnloader;
 bool                          MacIOSurfaceLib::isLoaded = false;
 void*                         MacIOSurfaceLib::sIOSurfaceFramework;
-void*                         MacIOSurfaceLib::sOpenGLFramework;
 void*                         MacIOSurfaceLib::sCoreVideoFramework;
 IOSurfaceCreateFunc           MacIOSurfaceLib::sCreate;
 IOSurfaceGetIDFunc            MacIOSurfaceLib::sGetID;
 IOSurfaceLookupFunc           MacIOSurfaceLib::sLookup;
 IOSurfaceGetBaseAddressFunc   MacIOSurfaceLib::sGetBaseAddress;
 IOSurfaceGetBaseAddressOfPlaneFunc  MacIOSurfaceLib::sGetBaseAddressOfPlane;
 IOSurfaceSizePlaneTFunc       MacIOSurfaceLib::sWidth;
 IOSurfaceSizePlaneTFunc       MacIOSurfaceLib::sHeight;
 IOSurfaceSizeTFunc            MacIOSurfaceLib::sPlaneCount;
 IOSurfaceSizePlaneTFunc       MacIOSurfaceLib::sBytesPerRow;
 IOSurfaceGetPropertyMaximumFunc   MacIOSurfaceLib::sGetPropertyMaximum;
 IOSurfaceVoidFunc             MacIOSurfaceLib::sIncrementUseCount;
 IOSurfaceVoidFunc             MacIOSurfaceLib::sDecrementUseCount;
 IOSurfaceLockFunc             MacIOSurfaceLib::sLock;
 IOSurfaceUnlockFunc           MacIOSurfaceLib::sUnlock;
-CGLTexImageIOSurface2DFunc    MacIOSurfaceLib::sTexImage;
 CVPixelBufferGetIOSurfaceFunc MacIOSurfaceLib::sCVPixelBufferGetIOSurface;
 IOSurfacePixelFormatFunc      MacIOSurfaceLib::sPixelFormat;
 
 CFStringRef                   MacIOSurfaceLib::kPropWidth;
 CFStringRef                   MacIOSurfaceLib::kPropHeight;
 CFStringRef                   MacIOSurfaceLib::kPropBytesPerElem;
 CFStringRef                   MacIOSurfaceLib::kPropBytesPerRow;
 CFStringRef                   MacIOSurfaceLib::kPropIsGlobal;
@@ -128,25 +123,16 @@ IOReturn MacIOSurfaceLib::IOSurfaceUnloc
 void MacIOSurfaceLib::IOSurfaceIncrementUseCount(IOSurfacePtr aIOSurfacePtr) {
   sIncrementUseCount(aIOSurfacePtr);
 }
 
 void MacIOSurfaceLib::IOSurfaceDecrementUseCount(IOSurfacePtr aIOSurfacePtr) {
   sDecrementUseCount(aIOSurfacePtr);
 }
 
-CGLError MacIOSurfaceLib::CGLTexImageIOSurface2D(CGLContextObj ctxt,
-                             GLenum target, GLenum internalFormat,
-                             GLsizei width, GLsizei height,
-                             GLenum format, GLenum type,
-                             IOSurfacePtr ioSurface, GLuint plane) {
-  return sTexImage(ctxt, target, internalFormat, width, height,
-                   format, type, ioSurface, plane);
-}
-
 IOSurfacePtr MacIOSurfaceLib::CVPixelBufferGetIOSurface(CVPixelBufferRef aPixelBuffer) {
   return sCVPixelBufferGetIOSurface(aPixelBuffer);
 }
 
 CFStringRef MacIOSurfaceLib::GetIOConst(const char* symbole) {
   CFStringRef *address = (CFStringRef*)dlsym(sIOSurfaceFramework, symbole);
   if (!address)
     return nullptr;
@@ -156,31 +142,25 @@ CFStringRef MacIOSurfaceLib::GetIOConst(
 
 void MacIOSurfaceLib::LoadLibrary() {
   if (isLoaded) {
     return;
   }
   isLoaded = true;
   sIOSurfaceFramework = dlopen(IOSURFACE_FRAMEWORK_PATH,
                             RTLD_LAZY | RTLD_LOCAL);
-  sOpenGLFramework = dlopen(OPENGL_FRAMEWORK_PATH,
-                            RTLD_LAZY | RTLD_LOCAL);
-
   sCoreVideoFramework = dlopen(COREVIDEO_FRAMEWORK_PATH,
                             RTLD_LAZY | RTLD_LOCAL);
 
-  if (!sIOSurfaceFramework || !sOpenGLFramework || !sCoreVideoFramework) {
+  if (!sIOSurfaceFramework || !sCoreVideoFramework) {
     if (sIOSurfaceFramework)
       dlclose(sIOSurfaceFramework);
-    if (sOpenGLFramework)
-      dlclose(sOpenGLFramework);
     if (sCoreVideoFramework)
       dlclose(sCoreVideoFramework);
     sIOSurfaceFramework = nullptr;
-    sOpenGLFramework = nullptr;
     sCoreVideoFramework = nullptr;
     return;
   }
 
   kPropWidth = GetIOConst("kIOSurfaceWidth");
   kPropHeight = GetIOConst("kIOSurfaceHeight");
   kPropBytesPerElem = GetIOConst("kIOSurfaceBytesPerElement");
   kPropBytesPerRow = GetIOConst("kIOSurfaceBytesPerRow");
@@ -199,43 +179,37 @@ void MacIOSurfaceLib::LoadLibrary() {
   sDecrementUseCount =
     GET_IOSYM(sDecrementUseCount, "IOSurfaceDecrementUseCount");
   sGetBaseAddress = GET_IOSYM(sGetBaseAddress, "IOSurfaceGetBaseAddress");
   sGetBaseAddressOfPlane =
     GET_IOSYM(sGetBaseAddressOfPlane, "IOSurfaceGetBaseAddressOfPlane");
   sPlaneCount = GET_IOSYM(sPlaneCount, "IOSurfaceGetPlaneCount");
   sPixelFormat = GET_IOSYM(sPixelFormat, "IOSurfaceGetPixelFormat");
 
-  sTexImage = GET_CGLSYM(sTexImage, "CGLTexImageIOSurface2D");
-
   sCVPixelBufferGetIOSurface =
     GET_CVSYM(sCVPixelBufferGetIOSurface, "CVPixelBufferGetIOSurface");
 
-  if (!sCreate || !sGetID || !sLookup || !sTexImage || !sGetBaseAddress ||
-      !sGetBaseAddressOfPlane || !sPlaneCount ||
-      !kPropWidth || !kPropHeight || !kPropBytesPerElem || !kPropIsGlobal ||
-      !sLock || !sUnlock || !sIncrementUseCount || !sDecrementUseCount ||
-      !sWidth || !sHeight || !kPropBytesPerRow ||
-      !sBytesPerRow || !sGetPropertyMaximum || !sCVPixelBufferGetIOSurface) {
+  if (!sCreate || !sGetID || !sLookup || !sGetBaseAddress ||
+      !sGetBaseAddressOfPlane || !sPlaneCount || !kPropWidth || !kPropHeight ||
+      !kPropBytesPerElem || !kPropIsGlobal || !sLock || !sUnlock ||
+      !sIncrementUseCount || !sDecrementUseCount || !sWidth || !sHeight ||
+      !kPropBytesPerRow || !sBytesPerRow || !sGetPropertyMaximum ||
+      !sCVPixelBufferGetIOSurface) {
     CloseLibrary();
   }
 }
 
 void MacIOSurfaceLib::CloseLibrary() {
   if (sIOSurfaceFramework) {
     dlclose(sIOSurfaceFramework);
   }
-  if (sOpenGLFramework) {
-    dlclose(sOpenGLFramework);
-  }
   if (sCoreVideoFramework) {
     dlclose(sCoreVideoFramework);
   }
   sIOSurfaceFramework = nullptr;
-  sOpenGLFramework = nullptr;
   sCoreVideoFramework = nullptr;
 }
 
 MacIOSurface::MacIOSurface(IOSurfacePtr aIOSurfacePtr,
                            double aContentsScaleFactor, bool aHasAlpha)
   : mIOSurfacePtr(aIOSurfacePtr)
   , mContentsScaleFactor(aContentsScaleFactor)
   , mHasAlpha(aHasAlpha)
@@ -466,21 +440,20 @@ MacIOSurface::GetReadFormat()
   }
 }
 
 CGLError
 MacIOSurface::CGLTexImageIOSurface2D(CGLContextObj ctx,
                                      GLenum target, GLenum internalFormat,
                                      GLsizei width, GLsizei height,
                                      GLenum format, GLenum type,
-                                     GLuint plane) const
-{
-  return MacIOSurfaceLib::CGLTexImageIOSurface2D(ctx, target, internalFormat, width,
-                                                 height, format, type, mIOSurfacePtr,
-                                                 plane);
+                                     GLuint plane) const {
+  return ::CGLTexImageIOSurface2D(ctx, target, internalFormat, width, height,
+                                  format, type, (IOSurfaceRef)mIOSurfacePtr,
+                                  plane);
 }
 
 CGLError
 MacIOSurface::CGLTexImageIOSurface2D(mozilla::gl::GLContext* aGL,
                                      CGLContextObj ctx,
                                      size_t plane,
                                      mozilla::gfx::SurfaceFormat* aOutReadFormat)
 {
diff --git a/gfx/2d/MacIOSurface.h b/gfx/2d/MacIOSurface.h
--- a/gfx/2d/MacIOSurface.h
+++ b/gfx/2d/MacIOSurface.h
@@ -37,22 +37,16 @@ typedef IOReturn (*IOSurfaceLockFunc)(IO
 typedef IOReturn (*IOSurfaceUnlockFunc)(IOSurfacePtr io_surface,
                                         uint32_t options, uint32_t *seed);
 typedef void* (*IOSurfaceGetBaseAddressFunc)(IOSurfacePtr io_surface);
 typedef void* (*IOSurfaceGetBaseAddressOfPlaneFunc)(IOSurfacePtr io_surface,
                                                     size_t planeIndex);
 typedef size_t (*IOSurfaceSizeTFunc)(IOSurfacePtr io_surface);
 typedef size_t (*IOSurfaceSizePlaneTFunc)(IOSurfacePtr io_surface, size_t plane);
 typedef size_t (*IOSurfaceGetPropertyMaximumFunc) (CFStringRef property);
-typedef CGLError (*CGLTexImageIOSurface2DFunc) (CGLContextObj ctxt,
-                             GLenum target, GLenum internalFormat,
-                             GLsizei width, GLsizei height,
-                             GLenum format, GLenum type,
-                             IOSurfacePtr ioSurface, GLuint plane);
-
 typedef IOSurfacePtr (*CVPixelBufferGetIOSurfaceFunc)(
   CVPixelBufferRef pixelBuffer);
 
 typedef OSType (*IOSurfacePixelFormatFunc)(IOSurfacePtr io_surface);
 
 #ifdef XP_MACOSX
 #import <OpenGL/OpenGL.h>
 #else
@@ -130,34 +124,32 @@ private:
   double mContentsScaleFactor;
   bool mHasAlpha;
 };
 
 class MacIOSurfaceLib {
 public:
   MacIOSurfaceLib() = delete;
   static void                        *sIOSurfaceFramework;
-  static void                        *sOpenGLFramework;
   static void                        *sCoreVideoFramework;
   static bool                         isLoaded;
   static IOSurfaceCreateFunc          sCreate;
   static IOSurfaceGetIDFunc           sGetID;
   static IOSurfaceLookupFunc          sLookup;
   static IOSurfaceGetBaseAddressFunc  sGetBaseAddress;
   static IOSurfaceGetBaseAddressOfPlaneFunc sGetBaseAddressOfPlane;
   static IOSurfaceSizeTFunc           sPlaneCount;
   static IOSurfaceLockFunc            sLock;
   static IOSurfaceUnlockFunc          sUnlock;
   static IOSurfaceVoidFunc            sIncrementUseCount;
   static IOSurfaceVoidFunc            sDecrementUseCount;
   static IOSurfaceSizePlaneTFunc      sWidth;
   static IOSurfaceSizePlaneTFunc      sHeight;
   static IOSurfaceSizePlaneTFunc      sBytesPerRow;
   static IOSurfaceGetPropertyMaximumFunc  sGetPropertyMaximum;
-  static CGLTexImageIOSurface2DFunc   sTexImage;
   static CVPixelBufferGetIOSurfaceFunc    sCVPixelBufferGetIOSurface;
   static IOSurfacePixelFormatFunc     sPixelFormat;
   static CFStringRef                  kPropWidth;
   static CFStringRef                  kPropHeight;
   static CFStringRef                  kPropBytesPerElem;
   static CFStringRef                  kPropBytesPerRow;
   static CFStringRef                  kPropIsGlobal;
 
@@ -175,21 +167,16 @@ public:
   static size_t       IOSurfaceGetBytesPerRow(IOSurfacePtr aIOSurfacePtr, size_t plane);
   static size_t       IOSurfaceGetPropertyMaximum(CFStringRef property);
   static IOReturn     IOSurfaceLock(IOSurfacePtr aIOSurfacePtr,
                                     uint32_t options, uint32_t *seed);
   static IOReturn     IOSurfaceUnlock(IOSurfacePtr aIOSurfacePtr,
                                       uint32_t options, uint32_t *seed);
   static void         IOSurfaceIncrementUseCount(IOSurfacePtr aIOSurfacePtr);
   static void         IOSurfaceDecrementUseCount(IOSurfacePtr aIOSurfacePtr);
-  static CGLError     CGLTexImageIOSurface2D(CGLContextObj ctxt,
-                             GLenum target, GLenum internalFormat,
-                             GLsizei width, GLsizei height,
-                             GLenum format, GLenum type,
-                             IOSurfacePtr ioSurface, GLuint plane);
   static IOSurfacePtr CVPixelBufferGetIOSurface(CVPixelBufferRef apixelBuffer);
   static OSType       IOSurfaceGetPixelFormat(IOSurfacePtr aIOSurfacePtr);
   static void LoadLibrary();
   static void CloseLibrary();
 
   // Static deconstructor
   static class LibraryUnloader {
   public:
diff --git a/gfx/2d/QuartzSupport.mm b/gfx/2d/QuartzSupport.mm
--- a/gfx/2d/QuartzSupport.mm
+++ b/gfx/2d/QuartzSupport.mm
@@ -6,16 +6,17 @@
 
 #include "QuartzSupport.h"
 #include "nsDebug.h"
 #include "MacIOSurface.h"
 #include "mozilla/Sprintf.h"
 
 #import <QuartzCore/QuartzCore.h>
 #import <AppKit/NSOpenGL.h>
+#import <OpenGL/CGLIOSurface.h>
 #include <dlfcn.h>
 #include "GLDefs.h"
 
 #define IOSURFACE_FRAMEWORK_PATH \
   "/System/Library/Frameworks/IOSurface.framework/IOSurface"
 #define OPENGL_FRAMEWORK_PATH \
   "/System/Library/Frameworks/OpenGL.framework/OpenGL"
 #define COREGRAPHICS_FRAMEWORK_PATH \
@@ -206,21 +207,20 @@ nsresult nsCARenderer::SetupRenderer(voi
   ::CGLSetCurrentContext(mOpenGLContext);
 
   if (mIOSurface) {
     // Create the IOSurface mapped texture.
     ::glGenTextures(1, &mIOTexture);
     ::glBindTexture(GL_TEXTURE_RECTANGLE_ARB, mIOTexture);
     ::glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
     ::glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
-    MacIOSurfaceLib::CGLTexImageIOSurface2D(mOpenGLContext, GL_TEXTURE_RECTANGLE_ARB,
-                                           GL_RGBA, aWidth * intScaleFactor,
-                                           aHeight * intScaleFactor,
-                                           GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV,
-                                           mIOSurface->mIOSurfacePtr, 0);
+    ::CGLTexImageIOSurface2D(mOpenGLContext, GL_TEXTURE_RECTANGLE_ARB, GL_RGBA,
+                             aWidth * intScaleFactor, aHeight * intScaleFactor, GL_BGRA,
+                             GL_UNSIGNED_INT_8_8_8_8_REV, (IOSurfaceRef)mIOSurface->mIOSurfacePtr,
+                             0);
     ::glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
   } else {
     ::glGenTextures(1, &mFBOTexture);
     ::glBindTexture(GL_TEXTURE_RECTANGLE_ARB, mFBOTexture);
     ::glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
     ::glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
     ::glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
   }
@@ -349,21 +349,20 @@ void nsCARenderer::AttachIOSurface(MacIO
     CARenderer* caRenderer = (CARenderer*)mCARenderer;
     size_t intScaleFactor = ceil(mContentsScaleFactor);
     int width = caRenderer.bounds.size.width / intScaleFactor;
     int height = caRenderer.bounds.size.height / intScaleFactor;
 
     CGLContextObj oldContext = ::CGLGetCurrentContext();
     ::CGLSetCurrentContext(mOpenGLContext);
     ::glBindTexture(GL_TEXTURE_RECTANGLE_ARB, mIOTexture);
-    MacIOSurfaceLib::CGLTexImageIOSurface2D(mOpenGLContext, GL_TEXTURE_RECTANGLE_ARB,
-                                           GL_RGBA, mIOSurface->GetDevicePixelWidth(),
-                                           mIOSurface->GetDevicePixelHeight(),
-                                           GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV,
-                                           mIOSurface->mIOSurfacePtr, 0);
+    ::CGLTexImageIOSurface2D(mOpenGLContext, GL_TEXTURE_RECTANGLE_ARB, GL_RGBA,
+                             mIOSurface->GetDevicePixelWidth(), mIOSurface->GetDevicePixelHeight(),
+                             GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV,
+                             (IOSurfaceRef)mIOSurface->mIOSurfacePtr, 0);
     ::glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
 
     // Rebind the FBO to make it live
     ::glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, mFBO);
 
     if (static_cast<int>(mIOSurface->GetWidth()) != width ||
         static_cast<int>(mIOSurface->GetHeight()) != height) {
       width = mIOSurface->GetWidth();
