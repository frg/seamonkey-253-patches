# HG changeset patch
# User Bryce Van Dyk <bvandyk@mozilla.com>
# Date 1536850185 0
# Node ID 8f814dfe27bd6b76b48056da82b036642594024c
# Parent  d86360092545773c6bee4df433deac2fabaceff7
Bug 1487811 - P3: Add CDM10 functions to IPDL. r=cpearce

The CDM10 interface makes 2 notable changes here:
1) Instead of the binary choice between unencrypted and encrypted (which assumed
cenc encryption), the interface now allows for specification of encryption used.
Practically this means we will eventually need to support choosing between not
encrypted, cenc, or cbcs.
2) The interface adds a bool for hardware secure codecs for use when
initializing the CDM.

This changeset adjusts the IPDL for the CDM to accommodate these changes. The
changes are also supported in surrounding code, but are not fully plumbed to
either the web, or the CDM.

Fully supporting new encryption schemes and hardware secure codecs will require
further work which is beyond the scope of this bug.

Some formatting drive bys so new formatting and old formatting both match
expected formatting by clang-format.

Depends on D5629

Differential Revision: https://phabricator.services.mozilla.com/D5630

diff --git a/dom/media/gmp/ChromiumCDMChild.cpp b/dom/media/gmp/ChromiumCDMChild.cpp
--- a/dom/media/gmp/ChromiumCDMChild.cpp
+++ b/dom/media/gmp/ChromiumCDMChild.cpp
@@ -459,19 +459,19 @@ ChromiumCDMChild::RecvPurgeShmems()
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
 ChromiumCDMChild::RecvInit(const bool& aAllowDistinctiveIdentifier,
                            const bool& aAllowPersistentState)
 {
   MOZ_ASSERT(IsOnMessageLoopThread());
-  GMP_LOG("ChromiumCDMChild::RecvInit(distinctiveId=%d, persistentState=%d)",
-          aAllowDistinctiveIdentifier,
-          aAllowPersistentState);
+  GMP_LOG("ChromiumCDMChild::RecvInit(distinctiveId=%s, persistentState=%s)",
+          aAllowDistinctiveIdentifier ? "true" : "false",
+          aAllowPersistentState ? "true" : "false");
   mPersistentStateAllowed = aAllowPersistentState;
   if (mCDM) {
     mCDM->Initialize(aAllowDistinctiveIdentifier, aAllowPersistentState);
   }
   return IPC_OK();
 }
 
 mozilla::ipc::IPCResult
@@ -645,17 +645,21 @@ ChromiumCDMChild::RecvGetStatusForPolicy
 static void
 InitInputBuffer(const CDMInputBuffer& aBuffer,
                 nsTArray<cdm::SubsampleEntry>& aSubSamples,
                 cdm::InputBuffer_1& aInputBuffer)
 {
   aInputBuffer.data = aBuffer.mData().get<uint8_t>();
   aInputBuffer.data_size = aBuffer.mData().Size<uint8_t>();
 
-  if (aBuffer.mIsEncrypted()) {
+  if (aBuffer.mEncryptionScheme() > GMPEncryptionScheme::kGMPEncryptionNone) {
+    // Cbcs is not yet supported, so we expect only cenc if the buffer us
+    // encrypted
+    MOZ_ASSERT(aBuffer.mEncryptionScheme() ==
+               GMPEncryptionScheme::kGMPEncryptionCenc);
     aInputBuffer.key_id = aBuffer.mKeyId().Elements();
     aInputBuffer.key_id_size = aBuffer.mKeyId().Length();
 
     aInputBuffer.iv = aBuffer.mIV().Elements();
     aInputBuffer.iv_size = aBuffer.mIV().Length();
 
     aSubSamples.SetCapacity(aBuffer.mClearBytes().Length());
     for (size_t i = 0; i < aBuffer.mCipherBytes().Length(); i++) {
diff --git a/dom/media/gmp/ChromiumCDMParent.cpp b/dom/media/gmp/ChromiumCDMParent.cpp
--- a/dom/media/gmp/ChromiumCDMParent.cpp
+++ b/dom/media/gmp/ChromiumCDMParent.cpp
@@ -61,17 +61,18 @@ ChromiumCDMParent::Init(ChromiumCDMCallb
                                         !aMainThread);
     GMP_LOG("ChromiumCDMParent::Init(this=%p) failure since aCDMCallback(%p) or"
             " aMainThread(%p) is nullptr", this, aCDMCallback, aMainThread);
     return false;
   }
   mCDMCallback = aCDMCallback;
   mMainThread = aMainThread;
 
-  if (SendInit(aAllowDistinctiveIdentifier, aAllowPersistentState)) {
+  if (SendInit(aAllowDistinctiveIdentifier,
+               aAllowPersistentState)) {
     return true;
   }
 
   RefPtr<gmp::GeckoMediaPluginService> service =
     gmp::GeckoMediaPluginService::GetGeckoMediaPluginService();
   bool xpcomWillShutdown = service && service->XPCOMWillShutdownReceived();
   aOutFailureReason = nsPrintfCString(
     "ChromiumCDMParent::Init() failed "
@@ -248,17 +249,24 @@ ChromiumCDMParent::InitCDMInputBuffer(gm
 
   aBuffer = gmp::CDMInputBuffer(shmem,
                                 crypto.mKeyId,
                                 crypto.mIV,
                                 aSample->mTime.ToMicroseconds(),
                                 aSample->mDuration.ToMicroseconds(),
                                 crypto.mPlainSizes,
                                 crypto.mEncryptedSizes,
-                                crypto.mValid);
+                                crypto.mValid
+                                  ? GMPEncryptionScheme::kGMPEncryptionCenc
+                                  : GMPEncryptionScheme::kGMPEncryptionNone);
+  MOZ_ASSERT(
+    aBuffer.mEncryptionScheme() == GMPEncryptionScheme::kGMPEncryptionNone ||
+      aBuffer.mEncryptionScheme() == GMPEncryptionScheme::kGMPEncryptionCenc,
+    "aBuffer should use either no encryption or cenc, other kinds are not yet "
+    "supported");
   return true;
 }
 
 bool
 ChromiumCDMParent::SendBufferToCDM(uint32_t aSizeInBytes)
 {
   GMP_LOG("ChromiumCDMParent::SendBufferToCDM() size=%" PRIu32, aSizeInBytes);
   Shmem shmem;
diff --git a/dom/media/gmp/GMPMessageUtils.h b/dom/media/gmp/GMPMessageUtils.h
--- a/dom/media/gmp/GMPMessageUtils.h
+++ b/dom/media/gmp/GMPMessageUtils.h
@@ -7,66 +7,79 @@
 #define GMPMessageUtils_h_
 
 #include "gmp-video-codec.h"
 #include "gmp-video-frame-encoded.h"
 #include "IPCMessageUtils.h"
 
 namespace IPC {
 
-template <>
+template<>
 struct ParamTraits<GMPErr>
-: public ContiguousEnumSerializer<GMPErr,
-                                  GMPNoErr,
-                                  GMPLastErr>
-{};
+  : public ContiguousEnumSerializer<GMPErr, GMPNoErr, GMPLastErr>
+{
+};
 
-template <>
+template<>
 struct ParamTraits<GMPVideoFrameType>
-: public ContiguousEnumSerializer<GMPVideoFrameType,
-                                  kGMPKeyFrame,
-                                  kGMPVideoFrameInvalid>
-{};
+  : public ContiguousEnumSerializer<GMPVideoFrameType,
+                                    kGMPKeyFrame,
+                                    kGMPVideoFrameInvalid>
+{
+};
 
-template <>
+template<>
 struct ParamTraits<GMPVideoCodecComplexity>
-: public ContiguousEnumSerializer<GMPVideoCodecComplexity,
-                                  kGMPComplexityNormal,
-                                  kGMPComplexityInvalid>
-{};
+  : public ContiguousEnumSerializer<GMPVideoCodecComplexity,
+                                    kGMPComplexityNormal,
+                                    kGMPComplexityInvalid>
+{
+};
 
-template <>
+template<>
 struct ParamTraits<GMPVP8ResilienceMode>
-: public ContiguousEnumSerializer<GMPVP8ResilienceMode,
-                                  kResilienceOff,
-                                  kResilienceInvalid>
-{};
+  : public ContiguousEnumSerializer<GMPVP8ResilienceMode,
+                                    kResilienceOff,
+                                    kResilienceInvalid>
+{
+};
 
-template <>
+template<>
 struct ParamTraits<GMPVideoCodecType>
-: public ContiguousEnumSerializer<GMPVideoCodecType,
-                                  kGMPVideoCodecVP8,
-                                  kGMPVideoCodecInvalid>
-{};
+  : public ContiguousEnumSerializer<GMPVideoCodecType,
+                                    kGMPVideoCodecVP8,
+                                    kGMPVideoCodecInvalid>
+{
+};
 
-template <>
+template<>
 struct ParamTraits<GMPVideoCodecMode>
-: public ContiguousEnumSerializer<GMPVideoCodecMode,
-                                  kGMPRealtimeVideo,
-                                  kGMPCodecModeInvalid>
-{};
+  : public ContiguousEnumSerializer<GMPVideoCodecMode,
+                                    kGMPRealtimeVideo,
+                                    kGMPCodecModeInvalid>
+{
+};
 
-template <>
+template<>
 struct ParamTraits<GMPBufferType>
-: public ContiguousEnumSerializer<GMPBufferType,
-                                  GMP_BufferSingle,
-                                  GMP_BufferInvalid>
-{};
+  : public ContiguousEnumSerializer<GMPBufferType,
+                                    GMP_BufferSingle,
+                                    GMP_BufferInvalid>
+{
+};
 
-template <>
+template<>
+struct ParamTraits<GMPEncryptionScheme>
+  : public ContiguousEnumSerializer<GMPEncryptionScheme,
+                                    GMPEncryptionScheme::kGMPEncryptionNone,
+                                    GMPEncryptionScheme::kGMPEncryptionInvalid>
+{
+};
+
+template<>
 struct ParamTraits<GMPSimulcastStream>
 {
   typedef GMPSimulcastStream paramType;
 
   static void Write(Message* aMsg, const paramType& aParam)
   {
     WriteParam(aMsg, aParam.mWidth);
     WriteParam(aMsg, aParam.mHeight);
diff --git a/dom/media/gmp/GMPTypes.ipdlh b/dom/media/gmp/GMPTypes.ipdlh
--- a/dom/media/gmp/GMPTypes.ipdlh
+++ b/dom/media/gmp/GMPTypes.ipdlh
@@ -1,16 +1,17 @@
 /* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 include "GMPMessageUtils.h";
 
 using GMPBufferType from "gmp-video-codec.h";
+using GMPEncryptionScheme from "gmp-video-codec.h";
 
 namespace mozilla {
 namespace gmp {
 
 struct NodeIdData {
   nsString mOrigin;
   nsString mTopLevelOrigin;
   nsString mGMPName;
@@ -50,26 +51,27 @@ struct GMPVideoi420FrameData
 struct CDMInputBuffer {
   Shmem mData;
   uint8_t[] mKeyId;
   uint8_t[] mIV;
   int64_t mTimestamp;
   int64_t mDuration;
   uint16_t[] mClearBytes;
   uint32_t[] mCipherBytes;
-  bool mIsEncrypted;
+  GMPEncryptionScheme mEncryptionScheme;
 };
 
 struct CDMVideoDecoderConfig {
   uint32_t mCodec;
   uint32_t mProfile;
   uint32_t mFormat;
   int32_t mImageWidth;
   int32_t mImageHeight;
   uint8_t[] mExtraData;
+  GMPEncryptionScheme mEncryptionScheme;
 };
 
 struct CDMKeyInformation {
   uint8_t[] mKeyId;
   uint32_t mStatus;
   uint32_t mSystemCode;
 };
 
diff --git a/dom/media/gmp/PChromiumCDM.ipdl b/dom/media/gmp/PChromiumCDM.ipdl
--- a/dom/media/gmp/PChromiumCDM.ipdl
+++ b/dom/media/gmp/PChromiumCDM.ipdl
@@ -9,17 +9,17 @@ include GMPTypes;
 namespace mozilla {
 namespace gmp {
 
 async protocol PChromiumCDM
 {
   manager PGMPContent;
 child:
 
-  // cdm::ContentDecryptionModule9
+  // cdm::ContentDecryptionModule9+10
   async Init(bool aAllowDistinctiveIdentifier,
              bool aAllowPersistentState);
 
   async GetStatusForPolicy(uint32_t aPromiseId,
                            nsCString aMinHdcpVersion);
 
   async SetServerCertificate(uint32_t aPromiseId,
                              uint8_t[] aServerCert);
@@ -60,17 +60,17 @@ child:
   async GiveBuffer(Shmem aShmem);
 
   async PurgeShmems();
   
 
 parent:
   async __delete__();
 
-  // cdm::Host9
+  // cdm::Host9+10
   async OnResolvePromiseWithKeyStatus(uint32_t aPromiseId, uint32_t aKeyStatus);
 
   async OnResolveNewSessionPromise(uint32_t aPromiseId, nsCString aSessionId);
 
   async OnResolvePromise(uint32_t aPromiseId);
 
   async OnRejectPromise(uint32_t aPromiseId,
                         uint32_t aException,
diff --git a/dom/media/gmp/gmp-api/gmp-video-codec.h b/dom/media/gmp/gmp-api/gmp-video-codec.h
--- a/dom/media/gmp/gmp-api/gmp-video-codec.h
+++ b/dom/media/gmp/gmp-api/gmp-video-codec.h
@@ -218,9 +218,19 @@ union GMPCodecSpecificInfoUnion
 // in the copy-constructor of VCMEncodedFrame.
 struct GMPCodecSpecificInfo
 {
   GMPVideoCodecType mCodecType;
   GMPBufferType mBufferType;
   GMPCodecSpecificInfoUnion mCodecSpecific;
 };
 
+// The encryption scheme used. Historically Widevine only supported none or
+// cenc, but starting at interface version 10 the CDM should also support cbcs.
+enum class GMPEncryptionScheme : uint8_t
+{
+  kGMPEncryptionNone = 0,
+  kGMPEncryptionCenc = 1,
+  kGMPEncryptionCbcs = 2,
+  kGMPEncryptionInvalid = 3,
+};
+
 #endif // GMP_VIDEO_CODEC_h_
