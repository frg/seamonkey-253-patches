# HG changeset patch
# User Andreas Pehrson <pehrsons@mozilla.com>
# Date 1516703482 -3600
#      Tue Jan 23 11:31:22 2018 +0100
# Node ID a75be3f4a9f2c488209a7c939c49545df9b19c46
# Parent  805aa621143efb915c4e3677f1c96c143fdbb18f
Bug 1299515 - Make LocalTrackSource hold a WeakPtr to SourceListener. r=jib

MozReview-Commit-ID: 93jKsK5JfqG

diff --git a/dom/media/MediaManager.cpp b/dom/media/MediaManager.cpp
--- a/dom/media/MediaManager.cpp
+++ b/dom/media/MediaManager.cpp
@@ -48,16 +48,17 @@
 #include "mozilla/dom/Promise.h"
 #include "mozilla/dom/MediaDevices.h"
 #include "mozilla/Base64.h"
 #include "mozilla/ipc/BackgroundChild.h"
 #include "mozilla/media/MediaChild.h"
 #include "mozilla/media/MediaTaskUtils.h"
 #include "MediaTrackConstraints.h"
 #include "VideoUtils.h"
+#include "ThreadSafeRefcountingWithMainThreadDestruction.h"
 #include "Latency.h"
 #include "nsProxyRelease.h"
 #include "NullPrincipal.h"
 #include "nsVariant.h"
 
 // For snprintf
 #include "mozilla/Sprintf.h"
 
@@ -221,18 +222,31 @@ static uint16_t
 FromCaptureState(CaptureState aState)
 {
   MOZ_ASSERT(aState == CaptureState::Off ||
              aState == CaptureState::Enabled ||
              aState == CaptureState::Disabled);
   return static_cast<uint16_t>(aState);
 }
 
-class SourceListener : public MediaStreamListener {
+/**
+ * SourceListener has threadsafe refcounting for use across the main, media and
+ * MSG threads. But it has a non-threadsafe SupportsWeakPtr for WeakPtr usage
+ * only from main thread, to ensure that garbage- and cycle-collected objects
+ * don't hold a reference to it during late shutdown.
+ *
+ * There's also a hard reference to the SourceListener through its
+ * SourceStreamListener and the MediaStreamGraph. MediaStreamGraph
+ * clears this on XPCOM_WILL_SHUTDOWN, before MediaManager enters shutdown.
+ */
+class SourceListener : public SupportsWeakPtr<SourceListener> {
 public:
+  MOZ_DECLARE_WEAKREFERENCE_TYPENAME(SourceListener)
+  NS_INLINE_DECL_THREADSAFE_REFCOUNTING_WITH_MAIN_THREAD_DESTRUCTION(SourceListener)
+
   SourceListener();
 
   /**
    * Registers this source listener as belonging to the given window listener.
    */
   void Register(GetUserMediaWindowListener* aListener);
 
   /**
@@ -308,21 +322,22 @@ public:
     return mAudioDeviceState ? mAudioDeviceState->mDevice.get() : nullptr;
   }
 
   MediaDevice* GetVideoDevice() const
   {
     return mVideoDeviceState ? mVideoDeviceState->mDevice.get() : nullptr;
   }
 
+  /**
+   * Called on MediaStreamGraph thread when MSG asks us for more data from
+   * input devices.
+   */
   void NotifyPull(MediaStreamGraph* aGraph,
-                  StreamTime aDesiredTime) override;
-
-  void NotifyEvent(MediaStreamGraph* aGraph,
-                   MediaStreamGraphEvent aEvent) override;
+                  StreamTime aDesiredTime);
 
   /**
    * Called on main thread after MediaStreamGraph notifies us that our
    * MediaStream was marked finish in the graph.
    */
   void NotifyFinished();
 
   /**
@@ -352,16 +367,79 @@ public:
                           TrackID aTrackID,
                           const dom::MediaTrackConstraints& aConstraints,
                           dom::CallerType aCallerType);
 
   PrincipalHandle GetPrincipalHandle() const;
 
 private:
   /**
+   * Wrapper class for the MediaStreamListener part of SourceListener.
+   *
+   * This is required since MediaStreamListener and SupportsWeakPtr
+   * both implement refcounting.
+   */
+  class SourceStreamListener : public MediaStreamListener {
+  public:
+    explicit SourceStreamListener(SourceListener* aSourceListener)
+      : mSourceListener(aSourceListener)
+    {
+    }
+
+    void NotifyPull(MediaStreamGraph* aGraph,
+                    StreamTime aDesiredTime) override
+    {
+      mSourceListener->NotifyPull(aGraph, aDesiredTime);
+    }
+
+    void NotifyEvent(MediaStreamGraph* aGraph,
+                     MediaStreamGraphEvent aEvent) override
+    {
+      nsCOMPtr<nsIEventTarget> target;
+
+      switch (aEvent) {
+        case MediaStreamGraphEvent::EVENT_FINISHED:
+          target = GetMainThreadEventTarget();
+          if (NS_WARN_IF(!target)) {
+            NS_ASSERTION(false, "Mainthread not available; running on current thread");
+            // Ensure this really *was* MainThread (NS_GetCurrentThread won't work)
+            MOZ_RELEASE_ASSERT(mSourceListener->mMainThreadCheck == GetCurrentVirtualThread());
+            mSourceListener->NotifyFinished();
+            return;
+          }
+          target->Dispatch(NewRunnableMethod("SourceListener::NotifyFinished",
+                                             mSourceListener,
+                                             &SourceListener::NotifyFinished),
+                           NS_DISPATCH_NORMAL);
+          break;
+        case MediaStreamGraphEvent::EVENT_REMOVED:
+          target = GetMainThreadEventTarget();
+          if (NS_WARN_IF(!target)) {
+            NS_ASSERTION(false, "Mainthread not available; running on current thread");
+            // Ensure this really *was* MainThread (NS_GetCurrentThread won't work)
+            MOZ_RELEASE_ASSERT(mSourceListener->mMainThreadCheck == GetCurrentVirtualThread());
+            mSourceListener->NotifyRemoved();
+            return;
+          }
+          target->Dispatch(NewRunnableMethod("SourceListener::NotifyRemoved",
+                                             mSourceListener,
+                                             &SourceListener::NotifyRemoved),
+                           NS_DISPATCH_NORMAL);
+          break;
+        default:
+          break;
+      }
+    }
+  private:
+    RefPtr<SourceListener> mSourceListener;
+  };
+
+  virtual ~SourceListener() = default;
+
+  /**
    * Returns a pointer to the device state for aTrackID.
    *
    * This is intended for internal use where we need to figure out which state
    * corresponds to aTrackID, not for availability checks. As such, we assert
    * that the device does indeed exist.
    *
    * Since this is a raw pointer and the state lifetime depends on the
    * SourceListener's lifetime, it's internal use only.
@@ -388,16 +466,17 @@ private:
   // Weak pointer to the window listener that owns us. MainThread only.
   GetUserMediaWindowListener* mWindowListener;
 
   // Accessed from MediaStreamGraph thread, MediaManager thread, and MainThread
   // No locking needed as they're set on Activate() and never assigned to again.
   UniquePtr<DeviceState> mAudioDeviceState;
   UniquePtr<DeviceState> mVideoDeviceState;
   RefPtr<SourceMediaStream> mStream; // threadsafe refcnt
+  RefPtr<SourceStreamListener> mStreamListener; // threadsafe refcnt
 };
 
 /**
  * This class represents a WindowID and handles all MediaStreamListeners
  * (here subclassed as SourceListeners) used to feed GetUserMedia source
  * streams. It proxies feedback from them into messages for browser chrome.
  * The SourceListeners are used to Start() and Stop() the underlying
  * MediaEngineSource when MediaStreams are assigned and deassigned in content.
@@ -1131,22 +1210,26 @@ public:
             mWindowID, domStream->GetInputStream()->AsProcessedStream());
       window->SetAudioCapture(true);
     } else {
       class LocalTrackSource : public MediaStreamTrackSource
       {
       public:
         LocalTrackSource(nsIPrincipal* aPrincipal,
                          const nsString& aLabel,
-                         SourceListener* aListener,
+                         const RefPtr<SourceListener>& aListener,
                          const MediaSourceEnum aSource,
                          const TrackID aTrackID,
                          const PeerIdentity* aPeerIdentity)
-          : MediaStreamTrackSource(aPrincipal, aLabel), mListener(aListener),
-            mSource(aSource), mTrackID(aTrackID), mPeerIdentity(aPeerIdentity) {}
+          : MediaStreamTrackSource(aPrincipal, aLabel),
+            mListener(aListener.get()),
+            mSource(aSource),
+            mTrackID(aTrackID),
+            mPeerIdentity(aPeerIdentity)
+        {}
 
         MediaSourceEnum GetMediaSource() const override
         {
           return mSource;
         }
 
         const PeerIdentity* GetPeerIdentity() const override
         {
@@ -1198,17 +1281,22 @@ public:
           if (mListener) {
             mListener->SetEnabledFor(mTrackID, true);
           }
         }
 
       protected:
         ~LocalTrackSource() {}
 
-        RefPtr<SourceListener> mListener;
+        // This is a weak pointer to avoid having the SourceListener (which may
+        // have references to threads and threadpools) kept alive by DOM-objects
+        // that may have ref-cycles and thus are released very late during
+        // shutdown, even after xpcom-shutdown-threads. See bug 1351655 for what
+        // can happen.
+        WeakPtr<SourceListener> mListener;
         const MediaSourceEnum mSource;
         const TrackID mTrackID;
         const RefPtr<const PeerIdentity> mPeerIdentity;
       };
 
       nsCOMPtr<nsIPrincipal> principal;
       if (mPeerIdentity) {
         principal = NullPrincipal::CreateWithInheritedAttributes(window->GetExtantDoc()->NodePrincipal());
@@ -3741,33 +3829,34 @@ SourceListener::Activate(SourceMediaStre
 
   LOG(("SourceListener %p activating audio=%p video=%p", this, aAudioDevice, aVideoDevice));
 
   MOZ_ASSERT(!mStopped, "Cannot activate stopped source listener");
   MOZ_ASSERT(!Activated(), "Already activated");
 
   mMainThreadCheck = GetCurrentVirtualThread();
   mStream = aStream;
+  mStreamListener = new SourceStreamListener(this);
   if (aAudioDevice) {
     mAudioDeviceState =
       MakeUnique<DeviceState>(
           aAudioDevice,
           aAudioDevice->GetMediaSource() == dom::MediaSourceEnum::Microphone &&
           Preferences::GetBool("media.getusermedia.microphone.off_while_disabled.enabled", false));
   }
 
   if (aVideoDevice) {
     mVideoDeviceState =
       MakeUnique<DeviceState>(
           aVideoDevice,
           aVideoDevice->GetMediaSource() == dom::MediaSourceEnum::Camera &&
           Preferences::GetBool("media.getusermedia.camera.off_while_disabled.enabled", true));
   }
 
-  mStream->AddListener(this);
+  mStream->AddListener(mStreamListener);
 }
 
 void
 SourceListener::Stop()
 {
   MOZ_ASSERT(NS_IsMainThread(), "Only call on main thread");
 
   if (mStopped) {
@@ -3819,18 +3908,19 @@ SourceListener::Remove()
   mWindowListener = nullptr;
 
   // If it's destroyed, don't call - listener will be removed and we'll be notified!
   if (!mStream->IsDestroyed()) {
     // We disable pulling before removing so we don't risk having live tracks
     // without a listener attached - that wouldn't produce data and would be
     // illegal to the graph.
     mStream->SetPullEnabled(false);
-    mStream->RemoveListener(this);
+    mStream->RemoveListener(mStreamListener);
   }
+  mStreamListener = nullptr;
 }
 
 void
 SourceListener::StopTrack(TrackID aTrackID)
 {
   MOZ_ASSERT(NS_IsMainThread(), "Only call on main thread");
   MOZ_ASSERT(Activated(), "No device to stop");
   MOZ_ASSERT(aTrackID == kAudioTrack || aTrackID == kVideoTrack,
@@ -4080,56 +4170,16 @@ SourceListener::NotifyPull(MediaStreamGr
   }
   if (mVideoDeviceState) {
     mVideoDeviceState->mDevice->Pull(mStream, kVideoTrack,
                                      aDesiredTime, mPrincipalHandle);
   }
 }
 
 void
-SourceListener::NotifyEvent(MediaStreamGraph* aGraph,
-                            MediaStreamGraphEvent aEvent)
-{
-  nsCOMPtr<nsIEventTarget> target;
-
-  switch (aEvent) {
-    case MediaStreamGraphEvent::EVENT_FINISHED:
-      target = GetMainThreadEventTarget();
-      if (NS_WARN_IF(!target)) {
-        NS_ASSERTION(false, "Mainthread not available; running on current thread");
-        // Ensure this really *was* MainThread (NS_GetCurrentThread won't work)
-        MOZ_RELEASE_ASSERT(mMainThreadCheck == GetCurrentVirtualThread());
-        NotifyFinished();
-        return;
-      }
-      target->Dispatch(NewRunnableMethod("SourceListener::NotifyFinished",
-                                         this,
-                                         &SourceListener::NotifyFinished),
-                       NS_DISPATCH_NORMAL);
-      break;
-    case MediaStreamGraphEvent::EVENT_REMOVED:
-      target = GetMainThreadEventTarget();
-      if (NS_WARN_IF(!target)) {
-        NS_ASSERTION(false, "Mainthread not available; running on current thread");
-        // Ensure this really *was* MainThread (NS_GetCurrentThread won't work)
-        MOZ_RELEASE_ASSERT(mMainThreadCheck == GetCurrentVirtualThread());
-        NotifyRemoved();
-        return;
-      }
-      target->Dispatch(NewRunnableMethod("SourceListener::NotifyRemoved",
-                                         this,
-                                         &SourceListener::NotifyRemoved),
-                       NS_DISPATCH_NORMAL);
-      break;
-    default:
-      break;
-  }
-}
-
-void
 SourceListener::NotifyFinished()
 {
   MOZ_ASSERT(NS_IsMainThread());
   mFinished = true;
   if (!mWindowListener) {
     // Removed explicitly before finished.
     return;
   }
@@ -4147,16 +4197,17 @@ SourceListener::NotifyRemoved()
   LOG(("SourceListener removed, mFinished = %d", (int) mFinished));
   mRemoved = true;
 
   if (Activated() && !mFinished) {
     NotifyFinished();
   }
 
   mWindowListener = nullptr;
+  mStreamListener = nullptr;
 }
 
 bool
 SourceListener::CapturingVideo() const
 {
   MOZ_ASSERT(NS_IsMainThread());
   return Activated() && mVideoDeviceState && !mVideoDeviceState->mStopped &&
          (!mVideoDeviceState->mDevice->mSource->IsFake() ||
