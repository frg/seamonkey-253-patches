# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1589819229 0
# Node ID 69e7bacf059bcef8befee258f8a0bb631a15c4f9
# Parent  0055ce6a063f0556034bb8f8a0e44e7823671ef1
Bug 1638012 - commonize taskcluster-related bootstrappers for Linux; r=nalexander

This change doesn't fix all of the boilerplate involved in declaring
that certain packages should be fetched from taskcluster, but it's a
start, at least.

Differential Revision: https://phabricator.services.mozilla.com/D75330

diff --git a/python/mozboot/mozboot/archlinux.py b/python/mozboot/mozboot/archlinux.py
--- a/python/mozboot/mozboot/archlinux.py
+++ b/python/mozboot/mozboot/archlinux.py
@@ -6,37 +6,28 @@ from __future__ import absolute_import, 
 
 import os
 import sys
 import tempfile
 import subprocess
 import glob
 
 from mozboot.base import BaseBootstrapper
-from mozboot.linux_common import (
-    ClangStaticAnalysisInstall,
-    LucetcInstall,
-    MinidumpStackwalkInstall,
-    NodeInstall,
-    SccacheInstall,
-    StyloInstall,
-    WasiSysrootInstall,
-)
+from mozboot.linux_common import LinuxBootstrapper
 
 # NOTE: This script is intended to be run with a vanilla Python install.  We
 # have to rely on the standard library instead of Python 2+3 helpers like
 # the six module.
 if sys.version_info < (3,):
     input = raw_input  # noqa
 
 
 class ArchlinuxBootstrapper(
-        NodeInstall, StyloInstall, SccacheInstall, ClangStaticAnalysisInstall,
-        MinidumpStackwalkInstall,
-        LucetcInstall, WasiSysrootInstall, BaseBootstrapper):
+        LinuxBootstrapper,
+        BaseBootstrapper):
     '''Archlinux experimental bootstrapper.'''
 
     SYSTEM_PACKAGES = [
         'autoconf2.13',
         'base-devel',
         'nodejs',
         'python2',
         'python2-setuptools',
diff --git a/python/mozboot/mozboot/centosfedora.py b/python/mozboot/mozboot/centosfedora.py
--- a/python/mozboot/mozboot/centosfedora.py
+++ b/python/mozboot/mozboot/centosfedora.py
@@ -2,32 +2,21 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this file,
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import platform
 
 from mozboot.base import BaseBootstrapper
-from mozboot.linux_common import (
-    ClangStaticAnalysisInstall,
-    LucetcInstall,
-    MinidumpStackwalkInstall,
-    NasmInstall,
-    NodeInstall,
-    SccacheInstall,
-    StyloInstall,
-    WasiSysrootInstall,
-)
+from mozboot.linux_common import LinuxBootstrapper
 
 
 class CentOSFedoraBootstrapper(
-        NasmInstall, NodeInstall, StyloInstall, SccacheInstall,
-        MinidumpStackwalkInstall,
-        ClangStaticAnalysisInstall, LucetcInstall, WasiSysrootInstall,
+        LinuxBootstrapper,
         BaseBootstrapper):
     def __init__(self, distro, version, dist_id, **kwargs):
         BaseBootstrapper.__init__(self, **kwargs)
 
         self.distro = distro
         self.version = int(version.split('.')[0])
         self.dist_id = dist_id
 
diff --git a/python/mozboot/mozboot/debian.py b/python/mozboot/mozboot/debian.py
--- a/python/mozboot/mozboot/debian.py
+++ b/python/mozboot/mozboot/debian.py
@@ -1,25 +1,16 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 from mozboot.base import BaseBootstrapper
-from mozboot.linux_common import (
-    ClangStaticAnalysisInstall,
-    LucetcInstall,
-    MinidumpStackwalkInstall,
-    NasmInstall,
-    NodeInstall,
-    SccacheInstall,
-    StyloInstall,
-    WasiSysrootInstall,
-)
+from mozboot.linux_common import LinuxBootstrapper
 
 
 MERCURIAL_INSTALL_PROMPT = '''
 Mercurial releases a new version every 3 months and your distro's package
 may become out of date. This may cause incompatibility with some
 Mercurial extensions that rely on new Mercurial features. As a result,
 you may not have an optimal version control experience.
 
@@ -30,19 +21,18 @@ in files being placed in /usr/local/bin 
 How would you like to continue?
   1. Install a modern Mercurial via pip (recommended)
   2. Install a legacy Mercurial via apt
   3. Do not install Mercurial
 Your choice: '''
 
 
 class DebianBootstrapper(
-        NasmInstall, NodeInstall, StyloInstall, ClangStaticAnalysisInstall,
-        MinidumpStackwalkInstall,
-        SccacheInstall, LucetcInstall, WasiSysrootInstall, BaseBootstrapper):
+        LinuxBootstrapper,
+        BaseBootstrapper):
     # These are common packages for all Debian-derived distros (such as
     # Ubuntu).
     COMMON_PACKAGES = [
         'autoconf2.13',
         'build-essential',
         'nodejs',
         'python-dev',
         'python-pip',
diff --git a/python/mozboot/mozboot/gentoo.py b/python/mozboot/mozboot/gentoo.py
--- a/python/mozboot/mozboot/gentoo.py
+++ b/python/mozboot/mozboot/gentoo.py
@@ -1,31 +1,21 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 from mozboot.base import BaseBootstrapper
-from mozboot.linux_common import (
-    ClangStaticAnalysisInstall,
-    LucetcInstall,
-    MinidumpStackwalkInstall,
-    NasmInstall,
-    NodeInstall,
-    SccacheInstall,
-    StyloInstall,
-    WasiSysrootInstall,
-)
+from mozboot.linux_common import LinuxBootstrapper
 
 
 class GentooBootstrapper(
-        NasmInstall, NodeInstall, StyloInstall, ClangStaticAnalysisInstall,
-        MinidumpStackwalkInstall,
-        SccacheInstall, LucetcInstall, WasiSysrootInstall, BaseBootstrapper):
+        LinuxBootstrapper,
+        BaseBootstrapper):
 
     def __init__(self, version, dist_id, **kwargs):
         BaseBootstrapper.__init__(self, **kwargs)
 
         self.version = version
         self.dist_id = dist_id
 
     def install_system_packages(self):
diff --git a/python/mozboot/mozboot/linux_common.py b/python/mozboot/mozboot/linux_common.py
--- a/python/mozboot/mozboot/linux_common.py
+++ b/python/mozboot/mozboot/linux_common.py
@@ -110,8 +110,22 @@ class MinidumpStackwalkInstall(object):
     def __init__(self, **kwargs):
         pass
 
     def ensure_minidump_stackwalk_packages(self, state_dir, checkout_root):
         from mozboot import minidump_stackwalk
 
         self.install_toolchain_artifact(state_dir, checkout_root,
                                         minidump_stackwalk.LINUX_MINIDUMP_STACKWALK)
+
+
+class LinuxBootstrapper(
+        ClangStaticAnalysisInstall,
+        LucetcInstall,
+        MinidumpStackwalkInstall,
+        NasmInstall,
+        NodeInstall,
+        SccacheInstall,
+        StyloInstall,
+        WasiSysrootInstall):
+
+    def __init__(self, **kwargs):
+        pass
diff --git a/python/mozboot/mozboot/opensuse.py b/python/mozboot/mozboot/opensuse.py
--- a/python/mozboot/mozboot/opensuse.py
+++ b/python/mozboot/mozboot/opensuse.py
@@ -1,29 +1,20 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 from mozboot.base import BaseBootstrapper
-from mozboot.linux_common import (
-    ClangStaticAnalysisInstall,
-    LucetcInstall,
-    NasmInstall,
-    NodeInstall,
-    SccacheInstall,
-    StyloInstall,
-    WasiSysrootInstall,
-)
+from mozboot.linux_common import LinuxBootstrapper
 
 
 class OpenSUSEBootstrapper(
-        NasmInstall, NodeInstall, StyloInstall, ClangStaticAnalysisInstall,
-        SccacheInstall, LucetcInstall, WasiSysrootInstall, BaseBootstrapper):
+        LinuxBootstrapper, BaseBootstrapper):
     '''openSUSE experimental bootstrapper.'''
 
     SYSTEM_PACKAGES = [
         'autoconf213',
         'nodejs',
         'npm',
         'which',
         'python3-devel',
diff --git a/python/mozboot/mozboot/solus.py b/python/mozboot/mozboot/solus.py
--- a/python/mozboot/mozboot/solus.py
+++ b/python/mozboot/mozboot/solus.py
@@ -3,37 +3,28 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import sys
 import subprocess
 
 from mozboot.base import BaseBootstrapper
-from mozboot.linux_common import (
-    ClangStaticAnalysisInstall,
-    LucetcInstall,
-    MinidumpStackwalkInstall,
-    NodeInstall,
-    SccacheInstall,
-    StyloInstall,
-    WasiSysrootInstall,
-)
+from mozboot.linux_common import LinuxBootstrapper
 
 # NOTE: This script is intended to be run with a vanilla Python install.  We
 # have to rely on the standard library instead of Python 2+3 helpers like
 # the six module.
 if sys.version_info < (3,):
     input = raw_input  # noqa
 
 
 class SolusBootstrapper(
-        NodeInstall, StyloInstall, SccacheInstall, ClangStaticAnalysisInstall,
-        MinidumpStackwalkInstall,
-        LucetcInstall, WasiSysrootInstall, BaseBootstrapper):
+        LinuxBootstrapper,
+        BaseBootstrapper):
     '''Solus experimental bootstrapper.'''
 
     SYSTEM_PACKAGES = [
         'autoconf213',
         'nodejs',
         'python',
         'python3',
         'unzip',
