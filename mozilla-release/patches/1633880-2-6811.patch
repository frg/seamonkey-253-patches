# HG changeset patch
# User ssengupta <ssengupta@mozilla.com>
# Date 1594937788 14400
# Node ID bf6f03da78802e3608cd85816f317c4cd8bea9b7
# Parent  d724fa8ddfb06e8a312dd7a9c6bdc61a5a1e733b
Bug 1633880 - P2 - IPCBlobInputStreamStorage::Get() now returns mozilla::Result<RefPtr<IPCBlobInputStreamStorage>, nsresult>. r=baku, a=RyanVM

diff --git a/dom/file/ipc/IPCBlobInputStream.cpp b/dom/file/ipc/IPCBlobInputStream.cpp
--- a/dom/file/ipc/IPCBlobInputStream.cpp
+++ b/dom/file/ipc/IPCBlobInputStream.cpp
@@ -134,22 +134,23 @@ IPCBlobInputStream::IPCBlobInputStream(I
   , mLength(0)
 {
   MOZ_ASSERT(aActor);
 
   mLength = aActor->Size();
 
   if (XRE_IsParentProcess()) {
     nsCOMPtr<nsIInputStream> stream;
-    IPCBlobInputStreamStorage::Get()->GetStream(mActor->ID(),
-                                                0, mLength,
-                                                getter_AddRefs(stream));
-    if (stream) {
-      mState = eRunning;
-      mRemoteStream = stream;
+    auto storage = IPCBlobInputStreamStorage::Get().unwrapOr(nullptr);
+    if (storage) {
+      storage->GetStream(mActor->ID(), 0, mLength, getter_AddRefs(stream));
+      if (stream) {
+        mState = eRunning;
+        mRemoteStream = stream;
+      }
     }
   }
 }
 
 IPCBlobInputStream::~IPCBlobInputStream()
 {
   Close();
 }
diff --git a/dom/file/ipc/IPCBlobInputStreamParent.cpp b/dom/file/ipc/IPCBlobInputStreamParent.cpp
--- a/dom/file/ipc/IPCBlobInputStreamParent.cpp
+++ b/dom/file/ipc/IPCBlobInputStreamParent.cpp
@@ -21,31 +21,44 @@ IPCBlobInputStreamParent::Create(nsIInpu
   MOZ_ASSERT(aRv);
 
   nsID id;
   *aRv = nsContentUtils::GenerateUUIDInPlace(id);
   if (NS_WARN_IF(NS_FAILED(*aRv))) {
     return nullptr;
   }
 
-  IPCBlobInputStreamStorage::Get()->AddStream(aInputStream, id, aSize, aChildID);
+  auto storageOrErr = IPCBlobInputStreamStorage::Get();
+
+  if (NS_WARN_IF(storageOrErr.isErr())) {
+    *aRv = storageOrErr.unwrapErr();
+    return nullptr;
+  }
+
+  auto storage = storageOrErr.unwrap();
+  storage->AddStream(aInputStream, id, aSize, aChildID);
 
   return new IPCBlobInputStreamParent(id, aSize, aManager);
 }
 
 /* static */ IPCBlobInputStreamParent*
 IPCBlobInputStreamParent::Create(const nsID& aID, uint64_t aSize,
                                  PBackgroundParent* aManager)
 {
   IPCBlobInputStreamParent* actor =
     new IPCBlobInputStreamParent(aID, aSize, aManager);
 
-  actor->mCallback = IPCBlobInputStreamStorage::Get()->TakeCallback(aID);
+  auto storage = IPCBlobInputStreamStorage::Get().unwrapOr(nullptr);
 
-  return actor;
+  if (storage) {
+    actor->mCallback = storage->TakeCallback(aID);
+    return actor;
+  }
+
+  return nullptr;
 }
 
 IPCBlobInputStreamParent::IPCBlobInputStreamParent(const nsID& aID,
                                                    uint64_t aSize,
                                                    nsIContentParent* aManager)
   : mID(aID)
   , mSize(aSize)
   , mContentManager(aManager)
@@ -69,17 +82,17 @@ IPCBlobInputStreamParent::ActorDestroy(I
   MOZ_ASSERT(mContentManager || mPBackgroundManager);
 
   mContentManager = nullptr;
   mPBackgroundManager = nullptr;
 
   RefPtr<IPCBlobInputStreamParentCallback> callback;
   mCallback.swap(callback);
 
-  RefPtr<IPCBlobInputStreamStorage> storage = IPCBlobInputStreamStorage::Get();
+  auto storage = IPCBlobInputStreamStorage::Get().unwrapOr(nullptr);
 
   if (mMigrating) {
     if (callback && storage) {
       // We need to assign this callback to the next parent.
       storage->StoreCallback(mID, callback);
     }
     return;
   }
@@ -104,25 +117,42 @@ IPCBlobInputStreamParent::SetCallback(
 }
 
 mozilla::ipc::IPCResult
 IPCBlobInputStreamParent::RecvStreamNeeded()
 {
   MOZ_ASSERT(mContentManager || mPBackgroundManager);
 
   nsCOMPtr<nsIInputStream> stream;
-  IPCBlobInputStreamStorage::Get()->GetStream(mID, 0, mSize, getter_AddRefs(stream));
+  auto storage = IPCBlobInputStreamStorage::Get().unwrapOr(nullptr);
+  if (storage) {
+    storage->GetStream(mID, 0, mSize, getter_AddRefs(stream));
+  }
+
   if (!stream) {
     if (!SendStreamReady(void_t())) {
       return IPC_FAIL(this, "SendStreamReady failed");
     }
 
     return IPC_OK();
   }
 
+// mozilla::ipc::IPCResult IPCBlobInputStreamParent::RecvLengthNeeded() {
+//   MOZ_ASSERT(mContentManager || mPBackgroundManager);
+//
+//    nsCOMPtr<nsIInputStream> stream;
+// -  IPCBlobInputStreamStorage::Get()->GetStream(mID, 0, mSize,
+// -                                              getter_AddRefs(stream));
+// +  auto storage = IPCBlobInputStreamStorage::Get().unwrapOr(nullptr);
+// +  if (storage) {
+// +    storage->GetStream(mID, 0, mSize, getter_AddRefs(stream));
+// +  }
+// +
+//    if (!stream) {
+
   mozilla::ipc::AutoIPCStream ipcStream;
   bool ok = false;
 
   if (mContentManager) {
     MOZ_ASSERT(NS_IsMainThread());
     ok = ipcStream.Serialize(stream, mContentManager);
   } else {
     MOZ_ASSERT(mPBackgroundManager);
@@ -152,18 +182,21 @@ IPCBlobInputStreamParent::RecvClose()
 mozilla::ipc::IPCResult
 IPCBlobInputStreamParent::Recv__delete__()
 {
   MOZ_ASSERT(mContentManager || mPBackgroundManager);
   mMigrating = true;
   return IPC_OK();
 }
 
-bool
-IPCBlobInputStreamParent::HasValidStream() const
-{
+bool IPCBlobInputStreamParent::HasValidStream() const {
+  auto storage = IPCBlobInputStreamStorage::Get().unwrapOr(nullptr);
+  if (!storage) {
+    return false;
+  }
+
   nsCOMPtr<nsIInputStream> stream;
-  IPCBlobInputStreamStorage::Get()->GetStream(mID, 0, mSize, getter_AddRefs(stream));
+  storage->GetStream(mID, 0, mSize, getter_AddRefs(stream));
   return !!stream;
 }
 
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/file/ipc/IPCBlobInputStreamStorage.cpp b/dom/file/ipc/IPCBlobInputStreamStorage.cpp
--- a/dom/file/ipc/IPCBlobInputStreamStorage.cpp
+++ b/dom/file/ipc/IPCBlobInputStreamStorage.cpp
@@ -30,19 +30,25 @@ NS_INTERFACE_MAP_END
 
 NS_IMPL_ADDREF(IPCBlobInputStreamStorage)
 NS_IMPL_RELEASE(IPCBlobInputStreamStorage)
 
 IPCBlobInputStreamStorage::IPCBlobInputStreamStorage() {}
 
 IPCBlobInputStreamStorage::~IPCBlobInputStreamStorage() {}
 
-IPCBlobInputStreamStorage* IPCBlobInputStreamStorage::Get() {
+Result<RefPtr<IPCBlobInputStreamStorage>, nsresult>
+IPCBlobInputStreamStorage::Get() {
   mozilla::StaticMutexAutoLock lock(gMutex);
-  return gStorage;
+  if (gStorage) {
+    RefPtr<IPCBlobInputStreamStorage> storage = gStorage;
+    return storage;
+  }
+
+  return Err(NS_ERROR_NOT_INITIALIZED);
 }
 
 /* static */ void
 IPCBlobInputStreamStorage::Initialize() {
   mozilla::StaticMutexAutoLock lock(gMutex);
   MOZ_ASSERT(!gStorage);
   gStorage = new IPCBlobInputStreamStorage();
 
diff --git a/dom/file/ipc/IPCBlobInputStreamStorage.h b/dom/file/ipc/IPCBlobInputStreamStorage.h
--- a/dom/file/ipc/IPCBlobInputStreamStorage.h
+++ b/dom/file/ipc/IPCBlobInputStreamStorage.h
@@ -21,28 +21,24 @@ class IPCBlobInputStreamParentCallback;
 
 class IPCBlobInputStreamStorage final : public nsIObserver
 {
 public:
   NS_DECL_THREADSAFE_ISUPPORTS
   NS_DECL_NSIOBSERVER
 
   // This initializes the singleton and it must be called on the main-thread.
-  static void
-  Initialize();
+  static void Initialize();
 
-  static IPCBlobInputStreamStorage*
-  Get();
+  static Result<RefPtr<IPCBlobInputStreamStorage>, nsresult> Get();
 
-  void
-  AddStream(nsIInputStream* aInputStream, const nsID& aID, uint64_t aSize,
-            uint64_t aChildID);
+  void AddStream(nsIInputStream* aInputStream, const nsID& aID, uint64_t aSize,
+                 uint64_t aChildID);
 
-  void
-  ForgetStream(const nsID& aID);
+  void ForgetStream(const nsID& aID);
 
   void
   GetStream(const nsID& aID, uint64_t aStart, uint64_t aLength,
             nsIInputStream** aInputStream);
 
   void
   StoreCallback(const nsID& aID, IPCBlobInputStreamParentCallback* aCallback);
 
diff --git a/ipc/glue/BackgroundParentImpl.cpp b/ipc/glue/BackgroundParentImpl.cpp
--- a/ipc/glue/BackgroundParentImpl.cpp
+++ b/ipc/glue/BackgroundParentImpl.cpp
@@ -342,16 +342,17 @@ BackgroundParentImpl::DeallocPTemporaryI
 
 PIPCBlobInputStreamParent*
 BackgroundParentImpl::AllocPIPCBlobInputStreamParent(const nsID& aID,
                                                      const uint64_t& aSize)
 {
   AssertIsInMainProcess();
   AssertIsOnBackgroundThread();
 
+  // Can return a null pointer since Bug 1633880
   return mozilla::dom::IPCBlobInputStreamParent::Create(aID, aSize, this);
 }
 
 mozilla::ipc::IPCResult
 BackgroundParentImpl::RecvPIPCBlobInputStreamConstructor(PIPCBlobInputStreamParent* aActor,
                                                          const nsID& aID,
                                                          const uint64_t& aSize)
 {
diff --git a/ipc/glue/InputStreamUtils.cpp b/ipc/glue/InputStreamUtils.cpp
--- a/ipc/glue/InputStreamUtils.cpp
+++ b/ipc/glue/InputStreamUtils.cpp
@@ -53,30 +53,31 @@ InputStreamHelper::SerializeInputStream(
 
   serializable->Serialize(aParams, aFileDescriptors);
 
   if (aParams.type() == InputStreamParams::T__None) {
     MOZ_CRASH("Serialize failed!");
   }
 }
 
-already_AddRefed<nsIInputStream>
-InputStreamHelper::DeserializeInputStream(const InputStreamParams& aParams,
-                                          const nsTArray<FileDescriptor>& aFileDescriptors)
-{
+already_AddRefed<nsIInputStream> InputStreamHelper::DeserializeInputStream(
+    const InputStreamParams& aParams,
+    const nsTArray<FileDescriptor>& aFileDescriptors) {
   nsCOMPtr<nsIInputStream> stream;
   nsCOMPtr<nsIIPCSerializableInputStream> serializable;
 
   // IPCBlobInputStreams are not deserializable on the parent side.
   if (aParams.type() == InputStreamParams::TIPCBlobInputStreamParams) {
     MOZ_ASSERT(XRE_IsParentProcess());
-    IPCBlobInputStreamStorage::Get()->GetStream(aParams.get_IPCBlobInputStreamParams().id(),
-                                                aParams.get_IPCBlobInputStreamParams().start(),
-                                                aParams.get_IPCBlobInputStreamParams().length(),
-                                                getter_AddRefs(stream));
+    auto storage = IPCBlobInputStreamStorage::Get().unwrapOr(nullptr);
+    MOZ_ASSERT(storage);
+    storage->GetStream(aParams.get_IPCBlobInputStreamParams().id(),
+                       aParams.get_IPCBlobInputStreamParams().start(),
+                       aParams.get_IPCBlobInputStreamParams().length(),
+                       getter_AddRefs(stream));
     return stream.forget();
   }
 
   switch (aParams.type()) {
     case InputStreamParams::TStringInputStreamParams:
       serializable = do_CreateInstance(kStringInputStreamCID);
       break;
 
