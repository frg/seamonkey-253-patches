# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1664316411 0
#      Tue Sep 27 22:06:51 2022 +0000
# Node ID 431ae85fa1a6035d08683fb2864544f53c9a2cf1
# Parent  e8a0cacdc4a5c41f55fc51d74d5ac79c3ffcb947
Bug 1792214 - Apply OTS patch from https://github.com/khaledhosny/ots/pull/249 to work around Core Text and FreeType failures with variable COLR fonts that lack a gvar table. r=gfx-reviewers,jrmuizel

Differential Revision: https://phabricator.services.mozilla.com/D158141

diff --git a/gfx/ots/src/gvar.cc b/gfx/ots/src/gvar.cc
--- a/gfx/ots/src/gvar.cc
+++ b/gfx/ots/src/gvar.cc
@@ -2,16 +2,17 @@
 // Use of this source code is governed by a BSD-style license that can be
 // found in the LICENSE file.
 
 #include "gvar.h"
 
 #include "fvar.h"
 #include "maxp.h"
 #include "variations.h"
+#include "ots-memory-stream.h"
 
 #define TABLE_NAME "gvar"
 
 namespace ots {
 
 // -----------------------------------------------------------------------------
 // OpenTypeGVAR
 // -----------------------------------------------------------------------------
@@ -140,16 +141,66 @@ bool OpenTypeGVAR::Parse(const uint8_t* 
   }
 
   this->m_data = data;
   this->m_length = length;
 
   return true;
 }
 
+#ifdef OTS_SYNTHESIZE_MISSING_GVAR
+bool OpenTypeGVAR::InitEmpty() {
+  // Generate an empty but well-formed 'gvar' table for the font.
+  const ots::Font* font = GetFont();
+
+  OpenTypeFVAR* fvar = static_cast<OpenTypeFVAR*>(font->GetTypedTable(OTS_TAG_FVAR));
+  if (!fvar) {
+    return DropVariations("Required fvar table missing");
+  }
+
+  OpenTypeMAXP* maxp = static_cast<OpenTypeMAXP*>(font->GetTypedTable(OTS_TAG_MAXP));
+  if (!maxp) {
+    return DropVariations("Required maxp table missing");
+  }
+
+  uint16_t majorVersion = 1;
+  uint16_t minorVersion = 0;
+  uint16_t axisCount = fvar->AxisCount();
+  uint16_t sharedTupleCount = 0;
+  uint32_t sharedTuplesOffset = 0;
+  uint16_t glyphCount = maxp->num_glyphs;
+  uint16_t flags = 0;
+  uint32_t glyphVariationDataArrayOffset = 0;
+
+  size_t length = 6 * sizeof(uint16_t) + 2 * sizeof(uint32_t)  // basic header fields
+      + (glyphCount + 1) * sizeof(uint16_t);  // glyphVariationDataOffsets[] array
+
+  uint8_t* data = new uint8_t[length];
+  MemoryStream stream(data, length);
+  if (!stream.WriteU16(majorVersion) ||
+      !stream.WriteU16(minorVersion) ||
+      !stream.WriteU16(axisCount) ||
+      !stream.WriteU16(sharedTupleCount) ||
+      !stream.WriteU32(sharedTuplesOffset) ||
+      !stream.WriteU16(glyphCount) ||
+      !stream.WriteU16(flags) ||
+      !stream.WriteU32(glyphVariationDataArrayOffset) ||
+      !stream.Pad((glyphCount + 1) * sizeof(uint16_t))) {
+    delete[] data;
+    return DropVariations("Failed to generate dummy gvar table");
+  }
+
+  this->m_data = data;
+  this->m_length = length;
+  this->m_ownsData = true;
+
+  return true;
+}
+#endif
+
 bool OpenTypeGVAR::Serialize(OTSStream* out) {
   if (!out->Write(this->m_data, this->m_length)) {
     return Error("Failed to write gvar table");
   }
 
   return true;
 }
 
diff --git a/gfx/ots/src/gvar.h b/gfx/ots/src/gvar.h
--- a/gfx/ots/src/gvar.h
+++ b/gfx/ots/src/gvar.h
@@ -11,21 +11,33 @@ namespace ots {
 
 // -----------------------------------------------------------------------------
 // OpenTypeGVAR Interface
 // -----------------------------------------------------------------------------
 
 class OpenTypeGVAR : public Table {
  public:
   explicit OpenTypeGVAR(Font* font, uint32_t tag)
-      : Table(font, tag, tag) { }
+      : Table(font, tag, tag), m_ownsData(false) { }
+
+  virtual ~OpenTypeGVAR() {
+    if (m_ownsData) {
+      delete[] m_data;
+    }
+  }
 
   bool Parse(const uint8_t* data, size_t length);
   bool Serialize(OTSStream* out);
 
+#ifdef OTS_SYNTHESIZE_MISSING_GVAR
+  bool InitEmpty();
+#endif
+
  private:
   const uint8_t *m_data;
   size_t m_length;
+
+  bool m_ownsData;
 };
 
 }  // namespace ots
 
 #endif  // OTS_GVAR_H_
diff --git a/gfx/ots/src/moz.build b/gfx/ots/src/moz.build
--- a/gfx/ots/src/moz.build
+++ b/gfx/ots/src/moz.build
@@ -61,16 +61,17 @@ UNIFIED_SOURCES += [
 AllowCompilerWarnings()
 
 FINAL_LIBRARY = 'gkmedias'
 
 DEFINES['PACKAGE_VERSION'] = '"moz"'
 DEFINES['PACKAGE_BUGREPORT'] = '"http://bugzilla.mozilla.org/"'
 DEFINES['OTS_GRAPHITE'] = 1
 DEFINES['OTS_VARIATIONS'] = 1
+DEFINES['OTS_SYNTHESIZE_MISSING_GVAR'] = 1
 
 USE_LIBS += [
     'brotli',
     'woff2',
 ]
 
 LOCAL_INCLUDES += [
     '/modules/woff2/src',
diff --git a/gfx/ots/src/ots.cc b/gfx/ots/src/ots.cc
--- a/gfx/ots/src/ots.cc
+++ b/gfx/ots/src/ots.cc
@@ -728,16 +728,28 @@ bool ProcessGeneric(ots::FontFile *heade
   for (const auto &table_entry : tables) {
     if (!font->GetTable(table_entry.tag)) {
       if (!font->ParseTable(table_entry, data, arena)) {
         return OTS_FAILURE_MSG_TAG("Failed to parse table", table_entry.tag);
       }
     }
   }
 
+#ifdef OTS_SYNTHESIZE_MISSING_GVAR
+  // If there was an fvar table but no gvar, synthesize an empty gvar to avoid
+  // issues with rasterizers (e.g. Core Text) that assume it must be present.
+  if (font->GetTable(OTS_TAG_FVAR) && !font->GetTable(OTS_TAG_GVAR)) {
+    ots::OpenTypeGVAR *gvar = new ots::OpenTypeGVAR(font, OTS_TAG_GVAR);
+    if (gvar->InitEmpty()) {
+      table_map[OTS_TAG_GVAR] = { OTS_TAG_GVAR, 0, 0, 0, 0 };
+      font->AddTable(gvar);
+    }
+  }
+#endif
+
   ots::Table *glyf = font->GetTable(OTS_TAG_GLYF);
   ots::Table *loca = font->GetTable(OTS_TAG_LOCA);
   ots::Table *cff  = font->GetTable(OTS_TAG_CFF);
   ots::Table *cff2 = font->GetTable(OTS_TAG_CFF2);
 
   if (glyf && loca) {
     if (font->version != 0x000010000) {
       OTS_WARNING_MSG_HDR("wrong sfntVersion for glyph data");
@@ -999,16 +1011,23 @@ Table* Font::GetTable(uint32_t tag) cons
 
 Table* Font::GetTypedTable(uint32_t tag) const {
   Table* t = GetTable(tag);
   if (t && t->Type() == tag)
     return t;
   return NULL;
 }
 
+void Font::AddTable(Table* table) {
+  // Attempting to add a duplicate table would be an error; this should only
+  // be used to add a table that does not already exist.
+  assert(m_tables.find(table->Tag()) == m_tables.end());
+  m_tables[table->Tag()] = table;
+}
+
 void Font::DropGraphite() {
   file->context->Message(0, "Dropping all Graphite tables");
   for (const std::pair<uint32_t, Table*> entry : m_tables) {
     if (entry.first == OTS_TAG_FEAT ||
         entry.first == OTS_TAG_GLAT ||
         entry.first == OTS_TAG_GLOC ||
         entry.first == OTS_TAG_SILE ||
         entry.first == OTS_TAG_SILF ||
diff --git a/gfx/ots/src/ots.h b/gfx/ots/src/ots.h
--- a/gfx/ots/src/ots.h
+++ b/gfx/ots/src/ots.h
@@ -258,16 +258,19 @@ class Table {
 
   // Return the tag (table type) this Table was parsed as, to support
   // "poor man's RTTI" so that we know if we can safely down-cast to
   // a specific Table subclass. The m_type field is initialized to the
   // appropriate tag when a subclass is constructed, or to zero for
   // TablePassthru (indicating unparsed data).
   uint32_t Type() { return m_type; }
 
+  // Return the tag assigned when this table was constructed.
+  uint32_t Tag() { return m_tag; }
+
   Font* GetFont() { return m_font; }
 
   bool Error(const char *format, ...);
   bool Warning(const char *format, ...);
   bool Drop(const char *format, ...);
   bool DropGraphite(const char *format, ...);
   bool DropVariations(const char *format, ...);
 
@@ -310,16 +313,19 @@ struct Font {
                   Arena &arena);
   Table* GetTable(uint32_t tag) const;
 
   // This checks that the returned Table is actually of the correct subclass
   // for |tag|, so it can safely be downcast to the corresponding OpenTypeXXXX;
   // if not (i.e. if the table was treated as Passthru), it will return NULL.
   Table* GetTypedTable(uint32_t tag) const;
 
+  // Insert a new table. Asserts if a table with the same tag already exists.
+  void AddTable(Table* table);
+
   // Drop all Graphite tables and don't parse new ones.
   void DropGraphite();
 
   // Drop all Variations tables and don't parse new ones.
   void DropVariations();
 
   FontFile *file;
 
