# HG changeset patch
# User Andreas Pehrson <pehrsons@mozilla.com>
# Date 1515058350 -3600
#      Thu Jan 04 10:32:30 2018 +0100
# Node ID c7bd57a9d7ef2010fe408ffab503077bc15f3e01
# Parent  0fdd4ce7e10eb7d31fd0497012ba6ce622a8ad42
Bug 1408294 - Don't assume there is always a listener feeding a SourceMediaStream. r=padenot

There are legit cases when a SourceMediaStream gets pulled without a listener
present.

A clear example (though a corner case and easily overlooked) that I've hit is
when the last track is ended and the only stream listener is removed at the same
time. This leads to a pull on the next iteration where the track-end has not
yet been picked up. And thus, a false positive error saying that a live track
doesn't have listeners.

The real error here will now instead be caught by the new assert for when a
pulled stream underruns (which is now illegal).

MozReview-Commit-ID: 3e8FcCZfhYJ

diff --git a/dom/media/MediaStreamGraph.cpp b/dom/media/MediaStreamGraph.cpp
--- a/dom/media/MediaStreamGraph.cpp
+++ b/dom/media/MediaStreamGraph.cpp
@@ -2753,27 +2753,16 @@ SourceMediaStream::PullNewData(
   LOG(LogLevel::Verbose,
       ("Calling NotifyPull aStream=%p t=%f current end=%f",
         this,
         GraphImpl()->MediaTimeToSeconds(t),
         GraphImpl()->MediaTimeToSeconds(current)));
   if (t <= current) {
     return false;
   }
-#ifdef DEBUG
-  if (mListeners.Length() == 0) {
-    LOG(
-      LogLevel::Error,
-      ("No listeners in NotifyPull aStream=%p desired=%f current end=%f",
-        this,
-        GraphImpl()->MediaTimeToSeconds(t),
-        GraphImpl()->MediaTimeToSeconds(current)));
-    DumpTrackInfo();
-  }
-#endif
   for (uint32_t j = 0; j < mListeners.Length(); ++j) {
     MediaStreamListener* l = mListeners[j];
     {
       MutexAutoUnlock unlock(mMutex);
       aPromises.AppendElement(l->AsyncNotifyPull(GraphImpl(), t));
     }
   }
   return true;
