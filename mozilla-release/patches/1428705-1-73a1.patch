# HG changeset patch
# User Edwin Takahashi <egao@mozilla.com>
# Date 1575671158 0
# Node ID f85d054c6a1b350a24441959bf3d39c19b4a01b7
# Parent  1981c2c6f2ab9f8e55f0bf23f4d0054435c2bdbf
Bug 1428705 - [manifestparser] Add support for Python 3. r=egao

Differential Revision: https://phabricator.services.mozilla.com/D54113

diff --git a/testing/mozbase/manifestparser/manifestparser/expression.py b/testing/mozbase/manifestparser/manifestparser/expression.py
--- a/testing/mozbase/manifestparser/manifestparser/expression.py
+++ b/testing/mozbase/manifestparser/manifestparser/expression.py
@@ -3,17 +3,17 @@
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function
 
 import re
 import sys
 import traceback
 
-from six import reraise
+import six
 
 __all__ = ['parse', 'ParseError', 'ExpressionParser']
 
 # expr.py
 # from:
 # http://k0s.org/mozilla/hg/expressionparser
 # http://hg.mozilla.org/users/tmielczarek_mozilla.com/expressionparser
 
@@ -275,47 +275,47 @@ class ExpressionParser(object):
 
     def advance(self, expected):
         """
         Assert that the next token is an instance of |expected|, and advance
         to the next token.
         """
         if not isinstance(self.token, expected):
             raise Exception("Unexpected token!")
-        self.token = self.iter.next()
+        self.token = six.next(self.iter)
 
     def expression(self, rbp=0):
         """
         Parse and return the value of an expression until a token with
         right binding power greater than rbp is encountered.
         """
         t = self.token
-        self.token = self.iter.next()
+        self.token = six.next(self.iter)
         left = t.nud(self)
         while rbp < self.token.lbp:
             t = self.token
-            self.token = self.iter.next()
+            self.token = six.next(self.iter)
             left = t.led(self, left)
         return left
 
     def parse(self):
         """
         Parse and return the value of the expression in the text
         passed to the constructor. Raises a ParseError if the expression
         could not be parsed.
         """
         try:
             self.iter = self._tokenize()
-            self.token = self.iter.next()
+            self.token = six.next(self.iter)
             return self.expression()
         except Exception:
             extype, ex, tb = sys.exc_info()
             formatted = ''.join(traceback.format_exception_only(extype, ex))
-            reraise(ParseError("could not parse: %s\nexception: %svariables: %s" %
-                    (self.text, formatted, self.valuemapping)), None, tb)
+            six.reraise(ParseError, ParseError("could not parse: %s\nexception: %svariables: %s" %
+                        (self.text, formatted, self.valuemapping)), tb)
 
     __call__ = parse
 
 
 def parse(text, **values):
     """
     Parse and evaluate a boolean expression.
     :param text: The expression to parse, as a string.
diff --git a/testing/mozbase/manifestparser/manifestparser/filters.py b/testing/mozbase/manifestparser/manifestparser/filters.py
--- a/testing/mozbase/manifestparser/manifestparser/filters.py
+++ b/testing/mozbase/manifestparser/manifestparser/filters.py
@@ -10,16 +10,17 @@ possible to define custom filters if the
 
 from __future__ import absolute_import
 
 import itertools
 import os
 from abc import ABCMeta, abstractmethod
 from collections import defaultdict, MutableSequence
 
+import six
 from six import string_types
 
 from .expression import (
     parse,
     ParseError,
 )
 
 
@@ -89,20 +90,22 @@ class InstanceFilter(object):
     Generally only one instance of a class filter should be applied at a time.
     Two instances of `InstanceFilter` are considered equal if they have the
     same class name. This ensures only a single instance is ever added to
     `filterlist`. This class also formats filters' __str__ method for easier
     debugging.
     """
     unique = True
 
+    __hash__ = super.__hash__
+
     def __init__(self, *args, **kwargs):
         self.fmt_args = ', '.join(itertools.chain(
             [str(a) for a in args],
-            ['{}={}'.format(k, v) for k, v in kwargs.iteritems()]))
+            ['{}={}'.format(k, v) for k, v in six.iteritems(kwargs)]))
 
     def __eq__(self, other):
         if self.unique:
             return self.__class__ == other.__class__
         return self.__hash__() == other.__hash__()
 
     def __str__(self):
         return "{}({})".format(self.__class__.__name__, self.fmt_args)
@@ -249,17 +252,17 @@ class chunk_by_dir(InstanceFilter):
         for i in range(start, end):
             for test in tests_by_dir.pop(ordered_dirs[i]):
                 yield test
 
         # find directories that only contain disabled tests. They still need to
         # be yielded for reporting purposes. Put them all in chunk 1 for
         # simplicity.
         if self.this_chunk == 1:
-            disabled_dirs = [v for k, v in tests_by_dir.iteritems()
+            disabled_dirs = [v for k, v in six.iteritems(tests_by_dir)
                              if k not in ordered_dirs]
             for disabled_test in itertools.chain(*disabled_dirs):
                 yield disabled_test
 
 
 class ManifestChunk(InstanceFilter):
     """
     Base class for chunking tests by manifest using a numerical key.
diff --git a/testing/mozbase/manifestparser/manifestparser/manifestparser.py b/testing/mozbase/manifestparser/manifestparser/manifestparser.py
--- a/testing/mozbase/manifestparser/manifestparser/manifestparser.py
+++ b/testing/mozbase/manifestparser/manifestparser/manifestparser.py
@@ -322,26 +322,26 @@ class ManifestParser(object):
             tags = set()
 
         # make some check functions
         if inverse:
             def has_tags(test):
                 return not tags.intersection(test.keys())
 
             def dict_query(test):
-                for key, value in kwargs.items():
+                for key, value in list(kwargs.items()):
                     if test.get(key) == value:
                         return False
                 return True
         else:
             def has_tags(test):
                 return tags.issubset(test.keys())
 
             def dict_query(test):
-                for key, value in kwargs.items():
+                for key, value in list(kwargs.items()):
                     if test.get(key) != value:
                         return False
                 return True
 
         # query the tests
         tests = self.query(has_tags, dict_query, tests=tests)
 
         # if a key is given, return only a list of that key
@@ -355,17 +355,17 @@ class ManifestParser(object):
     def manifests(self, tests=None):
         """
         return manifests in order in which they appear in the tests
         If |tests| is not set, the order of the manifests is unspecified.
         """
         if tests is None:
             manifests = []
             # Make sure to return all the manifests, even ones without tests.
-            for manifest in self.manifest_defaults.keys():
+            for manifest in list(self.manifest_defaults.keys()):
                 if isinstance(manifest, tuple):
                     parentmanifest, manifest = manifest
                 if manifest not in manifests:
                     manifests.append(manifest)
             return manifests
 
         manifests = []
         for test in tests:
@@ -472,17 +472,17 @@ class ManifestParser(object):
         # get matching tests
         tests = self.get(tags=tags, **kwargs)
 
         # print the .ini manifest
         if global_tags or global_kwargs:
             print('[DEFAULT]', file=fp)
             for tag in global_tags:
                 print('%s =' % tag, file=fp)
-            for key, value in global_kwargs.items():
+            for key, value in list(global_kwargs.items()):
                 print('%s = %s' % (key, value), file=fp)
             print(file=fp)
 
         for test in tests:
             test = test.copy()  # don't overwrite
 
             path = test['name']
             if not os.path.isabs(path):
diff --git a/testing/mozbase/manifestparser/tests/manifest.ini b/testing/mozbase/manifestparser/tests/manifest.ini
--- a/testing/mozbase/manifestparser/tests/manifest.ini
+++ b/testing/mozbase/manifestparser/tests/manifest.ini
@@ -1,13 +1,14 @@
 [DEFAULT]
 subsuite = mozbase
-skip-if = python == 3
 [test_expressionparser.py]
 [test_manifestparser.py]
 [test_testmanifest.py]
 [test_read_ini.py]
 [test_convert_directory.py]
+skip-if = python == 3
 [test_filters.py]
 [test_chunking.py]
+skip-if = python == 3
 
 [test_convert_symlinks.py]
 disabled = https://bugzilla.mozilla.org/show_bug.cgi?id=920938
diff --git a/testing/mozbase/manifestparser/tests/test_manifestparser.py b/testing/mozbase/manifestparser/tests/test_manifestparser.py
--- a/testing/mozbase/manifestparser/tests/test_manifestparser.py
+++ b/testing/mozbase/manifestparser/tests/test_manifestparser.py
@@ -5,17 +5,17 @@
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import
 
 import os
 import shutil
 import tempfile
 import unittest
-from StringIO import StringIO
+from six import StringIO
 
 import mozunit
 
 from manifestparser import ManifestParser
 
 here = os.path.dirname(os.path.abspath(__file__))
 
 
diff --git a/testing/mozbase/manifestparser/tests/test_read_ini.py b/testing/mozbase/manifestparser/tests/test_read_ini.py
--- a/testing/mozbase/manifestparser/tests/test_read_ini.py
+++ b/testing/mozbase/manifestparser/tests/test_read_ini.py
@@ -9,17 +9,17 @@ is the default:
 
 http://docs.python.org/2/library/configparser.html
 """
 
 from __future__ import absolute_import
 
 import unittest
 from manifestparser import read_ini
-from StringIO import StringIO
+from six import StringIO
 
 import mozunit
 
 
 class IniParserTest(unittest.TestCase):
 
     def parse_manifest(self, string):
         buf = StringIO()
