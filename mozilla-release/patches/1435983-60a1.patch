# HG changeset patch
# User Jonathan Kew <jkew@mozilla.com>
# Date 1520263522 -3600
# Node ID a6cc8bd3527949b1fab39a0a5bc71ae235a9895d
# Parent  0ec609d41161eb2221a9a697b44e1708088cf94f
Bug 1435983 - Make the 'font' shorthand reset the 'font-variation-settings' property, as required by css-fonts-4. r=emilio

diff --git a/devtools/shared/css/generated/properties-db.js b/devtools/shared/css/generated/properties-db.js
--- a/devtools/shared/css/generated/properties-db.js
+++ b/devtools/shared/css/generated/properties-db.js
@@ -6323,16 +6323,17 @@ exports.CSS_PROPERTIES = {
       "line-height",
       "font-size-adjust",
       "font-stretch",
       "-x-system-font",
       "font-feature-settings",
       "font-language-override",
       "font-kerning",
       "font-optical-sizing",
+      "font-variation-settings",
       "font-variant-alternates",
       "font-variant-caps",
       "font-variant-east-asian",
       "font-variant-ligatures",
       "font-variant-numeric",
       "font-variant-position"
     ],
     "supports": [
diff --git a/layout/style/Declaration.cpp b/layout/style/Declaration.cpp
--- a/layout/style/Declaration.cpp
+++ b/layout/style/Declaration.cpp
@@ -831,16 +831,18 @@ Declaration::GetPropertyValueInternal(
       const nsCSSValue *featureSettings =
         data->ValueFor(eCSSProperty_font_feature_settings);
       const nsCSSValue *languageOverride =
         data->ValueFor(eCSSProperty_font_language_override);
       const nsCSSValue *opticalSizing =
         data->ValueFor(eCSSProperty_font_optical_sizing); // may be missing!
       const nsCSSValue *fontKerning =
         data->ValueFor(eCSSProperty_font_kerning);
+      const nsCSSValue *variationSettings =
+        data->ValueFor(eCSSProperty_font_variation_settings); // may be missing!
       const nsCSSValue *fontVariantAlternates =
         data->ValueFor(eCSSProperty_font_variant_alternates);
       const nsCSSValue *fontVariantCaps =
         data->ValueFor(eCSSProperty_font_variant_caps);
       const nsCSSValue *fontVariantEastAsian =
         data->ValueFor(eCSSProperty_font_variant_east_asian);
       const nsCSSValue *fontVariantLigatures =
         data->ValueFor(eCSSProperty_font_variant_ligatures);
@@ -858,16 +860,17 @@ Declaration::GetPropertyValueInternal(
             lh->GetUnit() != eCSSUnit_System_Font ||
             family->GetUnit() != eCSSUnit_System_Font ||
             stretch->GetUnit() != eCSSUnit_System_Font ||
             sizeAdjust->GetUnit() != eCSSUnit_System_Font ||
             featureSettings->GetUnit() != eCSSUnit_System_Font ||
             languageOverride->GetUnit() != eCSSUnit_System_Font ||
             (opticalSizing && opticalSizing->GetUnit() != eCSSUnit_System_Font) ||
             fontKerning->GetUnit() != eCSSUnit_System_Font ||
+            (variationSettings && variationSettings->GetUnit() != eCSSUnit_System_Font) ||
             fontVariantAlternates->GetUnit() != eCSSUnit_System_Font ||
             fontVariantCaps->GetUnit() != eCSSUnit_System_Font ||
             fontVariantEastAsian->GetUnit() != eCSSUnit_System_Font ||
             fontVariantLigatures->GetUnit() != eCSSUnit_System_Font ||
             fontVariantNumeric->GetUnit() != eCSSUnit_System_Font ||
             fontVariantPosition->GetUnit() != eCSSUnit_System_Font) {
           // This can't be represented as a shorthand.
           return;
@@ -876,16 +879,17 @@ Declaration::GetPropertyValueInternal(
       } else {
         // properties reset by this shorthand property to their
         // initial values but not represented in its syntax
         if (sizeAdjust->GetUnit() != eCSSUnit_None ||
             featureSettings->GetUnit() != eCSSUnit_Normal ||
             languageOverride->GetUnit() != eCSSUnit_Normal ||
             (opticalSizing && opticalSizing->GetIntValue() != NS_FONT_OPTICAL_SIZING_AUTO) ||
             fontKerning->GetIntValue() != NS_FONT_KERNING_AUTO ||
+            (variationSettings && variationSettings->GetUnit() != eCSSUnit_Normal) ||
             fontVariantAlternates->GetUnit() != eCSSUnit_Normal ||
             fontVariantEastAsian->GetUnit() != eCSSUnit_Normal ||
             fontVariantLigatures->GetUnit() != eCSSUnit_Normal ||
             fontVariantNumeric->GetUnit() != eCSSUnit_Normal ||
             fontVariantPosition->GetUnit() != eCSSUnit_Normal) {
           return;
         }
 
diff --git a/layout/style/nsCSSParser.cpp b/layout/style/nsCSSParser.cpp
--- a/layout/style/nsCSSParser.cpp
+++ b/layout/style/nsCSSParser.cpp
@@ -14070,16 +14070,17 @@ CSSParserImpl::ParseFont()
       AppendValue(eCSSProperty_font_kerning, family);
       AppendValue(eCSSProperty_font_variant_alternates, family);
       AppendValue(eCSSProperty_font_variant_caps, family);
       AppendValue(eCSSProperty_font_variant_east_asian, family);
       AppendValue(eCSSProperty_font_variant_ligatures, family);
       AppendValue(eCSSProperty_font_variant_numeric, family);
       AppendValue(eCSSProperty_font_variant_position, family);
       if (StylePrefs::sFontVariationsEnabled) {
+        AppendValue(eCSSProperty_font_variation_settings, family);
         AppendValue(eCSSProperty_font_optical_sizing, family);
       }
     }
     else {
       AppendValue(eCSSProperty__x_system_font, family);
       nsCSSValue systemFont(eCSSUnit_System_Font);
       AppendValue(eCSSProperty_font_family, systemFont);
       AppendValue(eCSSProperty_font_style, systemFont);
@@ -14093,16 +14094,17 @@ CSSParserImpl::ParseFont()
       AppendValue(eCSSProperty_font_kerning, systemFont);
       AppendValue(eCSSProperty_font_variant_alternates, systemFont);
       AppendValue(eCSSProperty_font_variant_caps, systemFont);
       AppendValue(eCSSProperty_font_variant_east_asian, systemFont);
       AppendValue(eCSSProperty_font_variant_ligatures, systemFont);
       AppendValue(eCSSProperty_font_variant_numeric, systemFont);
       AppendValue(eCSSProperty_font_variant_position, systemFont);
       if (StylePrefs::sFontVariationsEnabled) {
+        AppendValue(eCSSProperty_font_variation_settings, systemFont);
         AppendValue(eCSSProperty_font_optical_sizing, systemFont);
       }
     }
     return true;
   }
 
   // Get optional font-style, font-variant, font-weight, font-stretch
   // (in any order)
@@ -14206,16 +14208,17 @@ CSSParserImpl::ParseFont()
                   nsCSSValue(eCSSUnit_Normal));
       AppendValue(eCSSProperty_font_variant_ligatures,
                   nsCSSValue(eCSSUnit_Normal));
       AppendValue(eCSSProperty_font_variant_numeric,
                   nsCSSValue(eCSSUnit_Normal));
       AppendValue(eCSSProperty_font_variant_position,
                   nsCSSValue(eCSSUnit_Normal));
       if (StylePrefs::sFontVariationsEnabled) {
+        AppendValue(eCSSProperty_font_variation_settings, nsCSSValue(eCSSUnit_Normal));
         AppendValue(eCSSProperty_font_optical_sizing,
                     nsCSSValue(NS_FONT_OPTICAL_SIZING_AUTO, eCSSUnit_Enumerated));
       }
       return true;
     }
   }
   return false;
 }
diff --git a/layout/style/nsCSSProps.cpp b/layout/style/nsCSSProps.cpp
--- a/layout/style/nsCSSProps.cpp
+++ b/layout/style/nsCSSProps.cpp
@@ -2804,16 +2804,17 @@ static const nsCSSPropertyID gFontSubpro
   eCSSProperty_line_height,
   eCSSProperty_font_size_adjust,
   eCSSProperty_font_stretch,
   eCSSProperty__x_system_font,
   eCSSProperty_font_feature_settings,
   eCSSProperty_font_language_override,
   eCSSProperty_font_kerning,
   eCSSProperty_font_optical_sizing,
+  eCSSProperty_font_variation_settings,
   eCSSProperty_font_variant_alternates,
   eCSSProperty_font_variant_caps,
   eCSSProperty_font_variant_east_asian,
   eCSSProperty_font_variant_ligatures,
   eCSSProperty_font_variant_numeric,
   eCSSProperty_font_variant_position,
   eCSSProperty_UNKNOWN
 };
diff --git a/layout/style/test/property_database.js b/layout/style/test/property_database.js
--- a/layout/style/test/property_database.js
+++ b/layout/style/test/property_database.js
@@ -6336,16 +6336,17 @@ if (IsCSSPropertyPrefEnabled("layout.css
       "'wdth' 1,", // trailing comma
       "'wdth' 1 , , 'wght' 2", // extra comma
       "'wdth', 1" // comma within pair
     ],
     unbalanced_values: [
       "'wdth\" 1", "\"wdth' 1" // mismatched quotes
     ]
   };
+  gCSSProperties["font"].subproperties.push("font-variation-settings");
   gCSSProperties["font-optical-sizing"] = {
     domProp: "fontOpticalSizing",
     inherited: true,
     type: CSS_TYPE_LONGHAND,
     applies_to_first_letter: true,
     applies_to_first_line: true,
     applies_to_placeholder: true,
     initial_values: [ "auto" ],
diff --git a/layout/style/test/test_bug377947.html b/layout/style/test/test_bug377947.html
--- a/layout/style/test/test_bug377947.html
+++ b/layout/style/test/test_bug377947.html
@@ -54,16 +54,17 @@ var all_but_one = {
   "font-family": "serif",
   "font-style": "normal",
   "font-variant": "normal",
   "font-weight": "bold",
   "font-size": "small",
   "font-stretch": "normal",
   "font-size-adjust": "none", // has to be default value
   "font-feature-settings": "normal", // has to be default value
+  "font-variation-settings": "normal", // has to be default value
   "font-language-override": "normal", // has to be default value
   "font-kerning": "auto", // has to be default value
   "font-optical-sizing": "auto", // has to be default value
   "font-synthesis": "weight style", // has to be default value
   "font-variant-alternates": "normal", // has to be default value
   "font-variant-caps": "normal", // has to be default value
   "font-variant-east-asian": "normal", // has to be default value
   "font-variant-ligatures": "normal", // has to be default value
diff --git a/layout/style/test/test_system_font_serialization.html b/layout/style/test/test_system_font_serialization.html
--- a/layout/style/test/test_system_font_serialization.html
+++ b/layout/style/test/test_system_font_serialization.html
@@ -75,16 +75,17 @@ EXPECTED_DECLS = [
   "font-stretch: inherit;",
   "font-style: inherit;",
   "font-variant: inherit;",
   "font-weight: inherit;",
   "line-height: inherit;",
 ];
 if (IsCSSPropertyPrefEnabled("layout.css.font-variations.enabled")) {
   EXPECTED_DECLS.push("font-optical-sizing: inherit;");
+  EXPECTED_DECLS.push("font-variation-settings: inherit;");
 }
 EXPECTED_DECLS = EXPECTED_DECLS.sort().join(" ");
 let sortedDecls = e.style.cssText.split(/ (?=font-|line-)/).sort().join(" ");
 is(sortedDecls, EXPECTED_DECLS, "don't serialize system font for font:inherit");
 is(e.style.font, "", "font getter returns nothing");
 
 </script>
 </pre>
