# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1535572197 14400
# Node ID 694735f79cb758fd5847c021a495273b9f56fb31
# Parent  ee972c60b4eb2e0b92cb5cce2d1053ab4158f6ec
Bug 1480549 - add aarch64 windows support to xptcall; r=dmajor

Since the ABI on this platform just uses the ARM procedure call
standard, which is the same standard Unix uses, we can just
copy-and-paste everything, with some adjustments to the assembly code to
compile properly.

diff --git a/xpcom/reflect/xptcall/md/win32/moz.build b/xpcom/reflect/xptcall/md/win32/moz.build
--- a/xpcom/reflect/xptcall/md/win32/moz.build
+++ b/xpcom/reflect/xptcall/md/win32/moz.build
@@ -17,28 +17,37 @@ if CONFIG['CPU_ARCH'] == 'x86_64':
         SOURCES += [
             'xptcinvoke_x86_64.cpp',
             'xptcstubs_x86_64.cpp'
         ]
         SOURCES += [
             'xptcinvoke_asm_x86_64.asm',
             'xptcstubs_asm_x86_64.asm'
         ]
-else:
+elif CONFIG['CPU_ARCH'] == 'x86':
     if CONFIG['CC_TYPE'] in ('clang', 'gcc'):
         SOURCES += [
             'xptcinvoke_x86_gnu.cpp',
             'xptcstubs.cpp',
         ]
     else:
         SOURCES += [
             'xptcinvoke.cpp',
             'xptcinvoke_asm_x86_msvc.asm',
             'xptcstubs.cpp',
         ]
         SOURCES['xptcinvoke_asm_x86_msvc.asm'].flags += ['-safeseh']
+elif CONFIG['CPU_ARCH'] == 'aarch64':
+    SOURCES += [
+        'xptcinvoke_aarch64.cpp',
+        'xptcstubs_aarch64.cpp',
+    ]
+    SOURCES += [
+        'xptcinvoke_asm_aarch64.asm',
+        'xptcstubs_asm_aarch64.asm',
+    ]
 
 FINAL_LIBRARY = 'xul'
 
 LOCAL_INCLUDES += [
     '../..',
     '/xpcom/reflect/xptinfo',
 ]
diff --git a/xpcom/reflect/xptcall/md/win32/xptcinvoke_aarch64.cpp b/xpcom/reflect/xptcall/md/win32/xptcinvoke_aarch64.cpp
new file mode 100644
--- /dev/null
+++ b/xpcom/reflect/xptcall/md/win32/xptcinvoke_aarch64.cpp
@@ -0,0 +1,142 @@
+/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+/* Platform specific code to invoke XPCOM methods on native objects */
+
+#include "xptcprivate.h"
+
+#if !defined(_MSC_VER) && !defined(_M_ARM64)
+#error "This code is for AArch64 Windows only"
+#endif
+
+/* "Procedure Call Standard for the ARM 64-bit Architecture" document, sections
+ * "5.4 Parameter Passing" and "6.1.2 Procedure Calling" contain all the
+ * needed information.
+ *
+ * http://infocenter.arm.com/help/topic/com.arm.doc.ihi0042d/IHI0042D_aapcs.pdf
+ *
+ * Windows follows this document, see:
+ *
+ * https://docs.microsoft.com/en-us/cpp/build/arm64-windows-abi-conventions
+ */
+
+/*
+ * Allocation of integer function arguments initially to registers r1-r7
+ * and then to stack. Handling of 'that' argument which goes to register r0
+ * is handled separately and does not belong here.
+ *
+ * 'ireg_args'  - pointer to the current position in the buffer,
+ *                corresponding to the register arguments
+ * 'stack_args' - pointer to the current position in the buffer,
+ *                corresponding to the arguments on stack
+ * 'end'        - pointer to the end of the registers argument
+ *                buffer.
+ */
+static inline void alloc_word(uint64_t* &ireg_args,
+                              uint64_t* &stack_args,
+                              uint64_t* end,
+                              uint64_t  data)
+{
+    if (ireg_args < end) {
+        *ireg_args = data;
+        ireg_args++;
+    } else {
+        *stack_args = data;
+        stack_args++;
+    }
+}
+
+static inline void alloc_double(double* &freg_args,
+                                uint64_t* &stack_args,
+                                double* end,
+                                double  data)
+{
+    if (freg_args < end) {
+        *freg_args = data;
+        freg_args++;
+    } else {
+        memcpy(stack_args, &data, sizeof(data));
+        stack_args++;
+    }
+}
+
+static inline void alloc_float(double* &freg_args,
+                               uint64_t* &stack_args,
+                               double* end,
+                               float  data)
+{
+    if (freg_args < end) {
+        memcpy(freg_args, &data, sizeof(data));
+        freg_args++;
+    } else {
+        memcpy(stack_args, &data, sizeof(data));
+        stack_args++;
+    }
+}
+
+
+extern "C" void
+invoke_copy_to_stack(uint64_t* stk, uint64_t *end,
+                     uint32_t paramCount, nsXPTCVariant* s)
+{
+    uint64_t *ireg_args = stk;
+    uint64_t *ireg_end  = ireg_args + 8;
+    double *freg_args = (double *)ireg_end;
+    double *freg_end  = freg_args + 8;
+    uint64_t *stack_args = (uint64_t *)freg_end;
+
+    // leave room for 'that' argument in x0
+    ++ireg_args;
+
+    for (uint32_t i = 0; i < paramCount; i++, s++) {
+        uint64_t word;
+
+        if (s->IsIndirect()) {
+            word = (uint64_t)&s->val;
+        } else {
+            // According to the ABI, integral types that are smaller than 8
+            // bytes are to be passed in 8-byte registers or 8-byte stack
+            // slots.
+            switch (s->type) {
+                case nsXPTType::T_FLOAT:
+                    alloc_float(freg_args, stack_args, freg_end, s->val.f);
+                    continue;
+                case nsXPTType::T_DOUBLE:
+                    alloc_double(freg_args, stack_args, freg_end, s->val.d);
+                    continue;
+                case nsXPTType::T_I8:    word = s->val.i8;  break;
+                case nsXPTType::T_I16:   word = s->val.i16; break;
+                case nsXPTType::T_I32:   word = s->val.i32; break;
+                case nsXPTType::T_I64:   word = s->val.i64; break;
+                case nsXPTType::T_U8:    word = s->val.u8;  break;
+                case nsXPTType::T_U16:   word = s->val.u16; break;
+                case nsXPTType::T_U32:   word = s->val.u32; break;
+                case nsXPTType::T_U64:   word = s->val.u64; break;
+                case nsXPTType::T_BOOL:  word = s->val.b;   break;
+                case nsXPTType::T_CHAR:  word = s->val.c;   break;
+                case nsXPTType::T_WCHAR: word = s->val.wc;  break;
+                default:
+                    // all the others are plain pointer types
+                    word = reinterpret_cast<uint64_t>(s->val.p);
+                    break;
+            }
+        }
+
+        alloc_word(ireg_args, stack_args, ireg_end, word);
+    }
+}
+
+extern "C" nsresult
+XPTC__InvokebyIndex(nsISupports* that, uint32_t methodIndex,
+                    uint32_t paramCount, nsXPTCVariant* params);
+
+extern "C"
+EXPORT_XPCOM_API(nsresult)
+NS_InvokeByIndex(nsISupports* that, uint32_t methodIndex,
+                   uint32_t paramCount, nsXPTCVariant* params)
+{
+    return XPTC__InvokebyIndex(that, methodIndex, paramCount, params);
+}
+
diff --git a/xpcom/reflect/xptcall/md/win32/xptcinvoke_asm_aarch64.asm b/xpcom/reflect/xptcall/md/win32/xptcinvoke_asm_aarch64.asm
new file mode 100644
--- /dev/null
+++ b/xpcom/reflect/xptcall/md/win32/xptcinvoke_asm_aarch64.asm
@@ -0,0 +1,64 @@
+; This Source Code Form is subject to the terms of the Mozilla Public
+; License, v. 2.0. If a copy of the MPL was not distributed with this
+; file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+	AREA |.text|, CODE, ARM64
+	IMPORT |invoke_copy_to_stack|
+	EXPORT |XPTC__InvokebyIndex|
+
+;
+;XPTC__InvokebyIndex(nsISupports* that, uint32_t methodIndex,
+;                   uint32_t paramCount, nsXPTCVariant* params)
+;
+|XPTC__InvokebyIndex| PROC
+	; set up frame
+	stp         x29, x30, [sp,#-32]!
+	mov         x29, sp
+	stp         x19, x20, [sp,#16]
+
+	; save methodIndex across function calls
+	mov         w20, w1
+
+	; end of stack area passed to invoke_copy_to_stack
+	mov         x1, sp
+
+	; assume 8 bytes of stack for each argument with 16-byte alignment
+	add         w19, w2, #1
+	and         w19, w19, #0xfffffffe
+	sub         sp, sp, w19, uxth #3
+
+	; temporary place to store args passed in r0-r7,v0-v7
+	sub         sp, sp, #128
+
+	; save 'that' on stack
+	str         x0, [sp]
+
+	; start of stack area passed to invoke_copy_to_stack
+	mov         x0, sp
+	bl          invoke_copy_to_stack
+
+	; load arguments passed in r0-r7
+	ldp         x6, x7, [sp, #48]
+	ldp         x4, x5, [sp, #32]
+	ldp         x2, x3, [sp, #16]
+	ldp         x0, x1, [sp],#64
+
+	; load arguments passed in v0-v7
+	ldp         d6, d7, [sp, #48]
+	ldp         d4, d5, [sp, #32]
+	ldp         d2, d3, [sp, #16]
+	ldp         d0, d1, [sp],#64
+
+	; call the method
+	ldr         x16, [x0]
+	add         x16, x16, w20, uxth #3
+	ldr         x16, [x16]
+	blr         x16
+
+	add         sp, sp, w19, uxth #3
+	ldp         x19, x20, [sp,#16]
+	ldp         x29, x30, [sp],#32
+	ret
+
+        ENDP
+	END
diff --git a/xpcom/reflect/xptcall/md/win32/xptcstubs_aarch64.cpp b/xpcom/reflect/xptcall/md/win32/xptcstubs_aarch64.cpp
new file mode 100644
--- /dev/null
+++ b/xpcom/reflect/xptcall/md/win32/xptcstubs_aarch64.cpp
@@ -0,0 +1,200 @@
+/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
+/* This Source Code Form is subject to the terms of the Mozilla Public
+ * License, v. 2.0. If a copy of the MPL was not distributed with this
+ * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
+
+/* Implement shared vtbl methods. */
+
+#include "xptcprivate.h"
+
+/*
+ * This is for AArch64 ABI
+ *
+ * When we're called, the "gp" registers are stored in gprData and
+ * the "fp" registers are stored in fprData. Each array has 8 regs
+ * but first reg in gprData is a placeholder for 'self'.
+ */
+extern "C" nsresult
+PrepareAndDispatch(nsXPTCStubBase* self, uint32_t methodIndex, uint64_t* args,
+                   uint64_t *gprData, double *fprData)
+{
+#define PARAM_BUFFER_COUNT        16
+#define PARAM_GPR_COUNT            8
+#define PARAM_FPR_COUNT            8
+
+    nsXPTCMiniVariant paramBuffer[PARAM_BUFFER_COUNT];
+    nsXPTCMiniVariant* dispatchParams = NULL;
+    const nsXPTMethodInfo* info;
+
+    NS_ASSERTION(self,"no self");
+
+    self->mEntry->GetMethodInfo(uint16_t(methodIndex), &info);
+    NS_ASSERTION(info,"no method info");
+
+    uint32_t paramCount = info->GetParamCount();
+
+    // setup variant array pointer
+    if (paramCount > PARAM_BUFFER_COUNT) {
+        dispatchParams = new nsXPTCMiniVariant[paramCount];
+    } else {
+        dispatchParams = paramBuffer;
+    }
+    NS_ASSERTION(dispatchParams,"no place for params");
+
+    const uint8_t indexOfJSContext = info->IndexOfJSContext();
+
+    uint64_t* ap = args;
+    uint32_t next_gpr = 1; // skip first arg which is 'self'
+    uint32_t next_fpr = 0;
+    for (uint32_t i = 0; i < paramCount; i++) {
+        const nsXPTParamInfo& param = info->GetParam(i);
+        const nsXPTType& type = param.GetType();
+        nsXPTCMiniVariant* dp = &dispatchParams[i];
+
+        if (i == indexOfJSContext) {
+            if (next_gpr < PARAM_GPR_COUNT)
+                next_gpr++;
+            else
+                ap++;
+        }
+
+        if (param.IsOut() || !type.IsArithmetic()) {
+            if (next_gpr < PARAM_GPR_COUNT) {
+                dp->val.p = (void*)gprData[next_gpr++];
+            } else {
+                dp->val.p = (void*)*ap++;
+            }
+            continue;
+        }
+
+        switch (type) {
+            case nsXPTType::T_I8:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.i8  = (int8_t)gprData[next_gpr++];
+                } else {
+                    dp->val.i8  = (int8_t)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_I16:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.i16  = (int16_t)gprData[next_gpr++];
+                } else {
+                    dp->val.i16  = (int16_t)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_I32:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.i32  = (int32_t)gprData[next_gpr++];
+                } else {
+                    dp->val.i32  = (int32_t)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_I64:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.i64  = (int64_t)gprData[next_gpr++];
+                } else {
+                    dp->val.i64  = (int64_t)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_U8:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.u8  = (uint8_t)gprData[next_gpr++];
+                } else {
+                    dp->val.u8  = (uint8_t)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_U16:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.u16  = (uint16_t)gprData[next_gpr++];
+                } else {
+                    dp->val.u16  = (uint16_t)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_U32:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.u32  = (uint32_t)gprData[next_gpr++];
+                } else {
+                    dp->val.u32  = (uint32_t)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_U64:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.u64  = (uint64_t)gprData[next_gpr++];
+                } else {
+                    dp->val.u64  = (uint64_t)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_FLOAT:
+                if (next_fpr < PARAM_FPR_COUNT) {
+                    memcpy(&dp->val.f, &fprData[next_fpr++], sizeof(dp->val.f));
+                } else {
+                    memcpy(&dp->val.f, ap++, sizeof(dp->val.f));
+                }
+                break;
+
+            case nsXPTType::T_DOUBLE:
+                if (next_fpr < PARAM_FPR_COUNT) {
+                    memcpy(&dp->val.d, &fprData[next_fpr++], sizeof(dp->val.d));
+                } else {
+                    memcpy(&dp->val.d, ap++, sizeof(dp->val.d));
+                }
+                break;
+
+            case nsXPTType::T_BOOL:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.b  = (bool)gprData[next_gpr++];
+                } else {
+                    dp->val.b  = (bool)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_CHAR:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.c  = (char)gprData[next_gpr++];
+                } else {
+                    dp->val.c  = (char)*ap++;
+                }
+                break;
+
+            case nsXPTType::T_WCHAR:
+                if (next_gpr < PARAM_GPR_COUNT) {
+                    dp->val.wc  = (wchar_t)gprData[next_gpr++];
+                } else {
+                    dp->val.wc  = (wchar_t)*ap++;
+                }
+                break;
+
+            default:
+                NS_ASSERTION(0, "bad type");
+                break;
+        }
+    }
+
+    nsresult result = self->mOuter->CallMethod((uint16_t)methodIndex, info,
+                                               dispatchParams);
+
+    if (dispatchParams != paramBuffer) {
+        delete [] dispatchParams;
+    }
+
+    return result;
+}
+
+#define STUB_ENTRY(n)  /* defined in the assembly file */
+
+#define SENTINEL_ENTRY(n) \
+nsresult nsXPTCStubBase::Sentinel##n() \
+{ \
+    NS_ERROR("nsXPTCStubBase::Sentinel called"); \
+    return NS_ERROR_NOT_IMPLEMENTED; \
+}
+
+#include "xptcstubsdef.inc"
diff --git a/xpcom/reflect/xptcall/md/win32/xptcstubs_asm_aarch64.asm b/xpcom/reflect/xptcall/md/win32/xptcstubs_asm_aarch64.asm
new file mode 100644
--- /dev/null
+++ b/xpcom/reflect/xptcall/md/win32/xptcstubs_asm_aarch64.asm
@@ -0,0 +1,296 @@
+; This Source Code Form is subject to the terms of the Mozilla Public
+; License, v. 2.0. If a copy of the MPL was not distributed with this
+; file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+NGPREGS EQU 8
+NFPREGS EQU 8
+
+	AREA |.text|, CODE, ARM64
+	IMPORT |PrepareAndDispatch|
+
+|SharedStub| PROC
+	stp         x29, x30, [sp,#-16]!
+	mov         x29, sp
+
+	sub         sp, sp, #128 ; 8*(NGPREGS+NFPREGS)
+	stp         x0, x1, [sp, #64+(0*8)]
+	stp         x2, x3, [sp, #64+(2*8)]
+	stp         x4, x5, [sp, #64+(4*8)]
+	stp         x6, x7, [sp, #64+(6*8)]
+	stp         d0, d1, [sp, #(0*8)]
+	stp         d2, d3, [sp, #(2*8)]
+	stp         d4, d5, [sp, #(4*8)]
+	stp         d6, d7, [sp, #(6*8)]
+
+	; methodIndex passed from stub
+	mov         w1, w17
+
+	add         x2, sp, #144 ; 16+(8*(NGPREGS+NFPREGS))
+	add         x3, sp, #64 ; 8*NFPREGS
+	add         x4, sp, #0
+
+	bl          PrepareAndDispatch
+
+	add         sp, sp, #128 ; 8*(NGPREGS+NFPREGS)
+	ldp         x29, x30, [sp],#16
+	ret
+	ENDP
+
+
+	MACRO
+	STUBENTRY $functionname, $paramcount
+|$functionname| PROC
+	mov	w17, $paramcount
+	b	SharedStub
+	ENDP
+	MEND
+
+    STUBENTRY ?Stub3@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 3
+    STUBENTRY ?Stub4@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 4
+    STUBENTRY ?Stub5@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 5
+    STUBENTRY ?Stub6@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 6
+    STUBENTRY ?Stub7@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 7
+    STUBENTRY ?Stub8@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 8
+    STUBENTRY ?Stub9@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 9
+    STUBENTRY ?Stub10@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 10
+    STUBENTRY ?Stub11@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 11
+    STUBENTRY ?Stub12@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 12
+    STUBENTRY ?Stub13@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 13
+    STUBENTRY ?Stub14@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 14
+    STUBENTRY ?Stub15@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 15
+    STUBENTRY ?Stub16@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 16
+    STUBENTRY ?Stub17@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 17
+    STUBENTRY ?Stub18@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 18
+    STUBENTRY ?Stub19@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 19
+    STUBENTRY ?Stub20@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 20
+    STUBENTRY ?Stub21@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 21
+    STUBENTRY ?Stub22@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 22
+    STUBENTRY ?Stub23@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 23
+    STUBENTRY ?Stub24@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 24
+    STUBENTRY ?Stub25@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 25
+    STUBENTRY ?Stub26@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 26
+    STUBENTRY ?Stub27@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 27
+    STUBENTRY ?Stub28@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 28
+    STUBENTRY ?Stub29@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 29
+    STUBENTRY ?Stub30@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 30
+    STUBENTRY ?Stub31@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 31
+    STUBENTRY ?Stub32@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 32
+    STUBENTRY ?Stub33@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 33
+    STUBENTRY ?Stub34@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 34
+    STUBENTRY ?Stub35@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 35
+    STUBENTRY ?Stub36@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 36
+    STUBENTRY ?Stub37@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 37
+    STUBENTRY ?Stub38@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 38
+    STUBENTRY ?Stub39@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 39
+    STUBENTRY ?Stub40@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 40
+    STUBENTRY ?Stub41@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 41
+    STUBENTRY ?Stub42@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 42
+    STUBENTRY ?Stub43@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 43
+    STUBENTRY ?Stub44@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 44
+    STUBENTRY ?Stub45@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 45
+    STUBENTRY ?Stub46@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 46
+    STUBENTRY ?Stub47@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 47
+    STUBENTRY ?Stub48@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 48
+    STUBENTRY ?Stub49@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 49
+    STUBENTRY ?Stub50@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 50
+    STUBENTRY ?Stub51@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 51
+    STUBENTRY ?Stub52@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 52
+    STUBENTRY ?Stub53@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 53
+    STUBENTRY ?Stub54@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 54
+    STUBENTRY ?Stub55@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 55
+    STUBENTRY ?Stub56@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 56
+    STUBENTRY ?Stub57@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 57
+    STUBENTRY ?Stub58@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 58
+    STUBENTRY ?Stub59@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 59
+    STUBENTRY ?Stub60@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 60
+    STUBENTRY ?Stub61@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 61
+    STUBENTRY ?Stub62@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 62
+    STUBENTRY ?Stub63@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 63
+    STUBENTRY ?Stub64@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 64
+    STUBENTRY ?Stub65@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 65
+    STUBENTRY ?Stub66@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 66
+    STUBENTRY ?Stub67@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 67
+    STUBENTRY ?Stub68@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 68
+    STUBENTRY ?Stub69@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 69
+    STUBENTRY ?Stub70@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 70
+    STUBENTRY ?Stub71@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 71
+    STUBENTRY ?Stub72@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 72
+    STUBENTRY ?Stub73@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 73
+    STUBENTRY ?Stub74@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 74
+    STUBENTRY ?Stub75@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 75
+    STUBENTRY ?Stub76@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 76
+    STUBENTRY ?Stub77@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 77
+    STUBENTRY ?Stub78@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 78
+    STUBENTRY ?Stub79@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 79
+    STUBENTRY ?Stub80@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 80
+    STUBENTRY ?Stub81@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 81
+    STUBENTRY ?Stub82@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 82
+    STUBENTRY ?Stub83@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 83
+    STUBENTRY ?Stub84@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 84
+    STUBENTRY ?Stub85@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 85
+    STUBENTRY ?Stub86@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 86
+    STUBENTRY ?Stub87@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 87
+    STUBENTRY ?Stub88@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 88
+    STUBENTRY ?Stub89@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 89
+    STUBENTRY ?Stub90@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 90
+    STUBENTRY ?Stub91@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 91
+    STUBENTRY ?Stub92@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 92
+    STUBENTRY ?Stub93@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 93
+    STUBENTRY ?Stub94@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 94
+    STUBENTRY ?Stub95@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 95
+    STUBENTRY ?Stub96@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 96
+    STUBENTRY ?Stub97@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 97
+    STUBENTRY ?Stub98@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 98
+    STUBENTRY ?Stub99@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 99
+    STUBENTRY ?Stub100@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 100
+    STUBENTRY ?Stub101@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 101
+    STUBENTRY ?Stub102@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 102
+    STUBENTRY ?Stub103@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 103
+    STUBENTRY ?Stub104@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 104
+    STUBENTRY ?Stub105@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 105
+    STUBENTRY ?Stub106@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 106
+    STUBENTRY ?Stub107@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 107
+    STUBENTRY ?Stub108@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 108
+    STUBENTRY ?Stub109@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 109
+    STUBENTRY ?Stub110@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 110
+    STUBENTRY ?Stub111@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 111
+    STUBENTRY ?Stub112@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 112
+    STUBENTRY ?Stub113@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 113
+    STUBENTRY ?Stub114@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 114
+    STUBENTRY ?Stub115@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 115
+    STUBENTRY ?Stub116@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 116
+    STUBENTRY ?Stub117@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 117
+    STUBENTRY ?Stub118@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 118
+    STUBENTRY ?Stub119@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 119
+    STUBENTRY ?Stub120@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 120
+    STUBENTRY ?Stub121@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 121
+    STUBENTRY ?Stub122@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 122
+    STUBENTRY ?Stub123@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 123
+    STUBENTRY ?Stub124@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 124
+    STUBENTRY ?Stub125@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 125
+    STUBENTRY ?Stub126@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 126
+    STUBENTRY ?Stub127@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 127
+    STUBENTRY ?Stub128@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 128
+    STUBENTRY ?Stub129@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 129
+    STUBENTRY ?Stub130@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 130
+    STUBENTRY ?Stub131@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 131
+    STUBENTRY ?Stub132@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 132
+    STUBENTRY ?Stub133@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 133
+    STUBENTRY ?Stub134@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 134
+    STUBENTRY ?Stub135@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 135
+    STUBENTRY ?Stub136@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 136
+    STUBENTRY ?Stub137@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 137
+    STUBENTRY ?Stub138@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 138
+    STUBENTRY ?Stub139@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 139
+    STUBENTRY ?Stub140@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 140
+    STUBENTRY ?Stub141@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 141
+    STUBENTRY ?Stub142@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 142
+    STUBENTRY ?Stub143@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 143
+    STUBENTRY ?Stub144@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 144
+    STUBENTRY ?Stub145@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 145
+    STUBENTRY ?Stub146@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 146
+    STUBENTRY ?Stub147@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 147
+    STUBENTRY ?Stub148@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 148
+    STUBENTRY ?Stub149@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 149
+    STUBENTRY ?Stub150@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 150
+    STUBENTRY ?Stub151@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 151
+    STUBENTRY ?Stub152@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 152
+    STUBENTRY ?Stub153@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 153
+    STUBENTRY ?Stub154@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 154
+    STUBENTRY ?Stub155@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 155
+    STUBENTRY ?Stub156@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 156
+    STUBENTRY ?Stub157@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 157
+    STUBENTRY ?Stub158@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 158
+    STUBENTRY ?Stub159@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 159
+    STUBENTRY ?Stub160@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 160
+    STUBENTRY ?Stub161@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 161
+    STUBENTRY ?Stub162@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 162
+    STUBENTRY ?Stub163@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 163
+    STUBENTRY ?Stub164@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 164
+    STUBENTRY ?Stub165@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 165
+    STUBENTRY ?Stub166@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 166
+    STUBENTRY ?Stub167@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 167
+    STUBENTRY ?Stub168@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 168
+    STUBENTRY ?Stub169@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 169
+    STUBENTRY ?Stub170@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 170
+    STUBENTRY ?Stub171@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 171
+    STUBENTRY ?Stub172@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 172
+    STUBENTRY ?Stub173@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 173
+    STUBENTRY ?Stub174@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 174
+    STUBENTRY ?Stub175@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 175
+    STUBENTRY ?Stub176@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 176
+    STUBENTRY ?Stub177@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 177
+    STUBENTRY ?Stub178@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 178
+    STUBENTRY ?Stub179@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 179
+    STUBENTRY ?Stub180@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 180
+    STUBENTRY ?Stub181@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 181
+    STUBENTRY ?Stub182@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 182
+    STUBENTRY ?Stub183@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 183
+    STUBENTRY ?Stub184@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 184
+    STUBENTRY ?Stub185@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 185
+    STUBENTRY ?Stub186@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 186
+    STUBENTRY ?Stub187@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 187
+    STUBENTRY ?Stub188@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 188
+    STUBENTRY ?Stub189@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 189
+    STUBENTRY ?Stub190@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 190
+    STUBENTRY ?Stub191@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 191
+    STUBENTRY ?Stub192@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 192
+    STUBENTRY ?Stub193@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 193
+    STUBENTRY ?Stub194@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 194
+    STUBENTRY ?Stub195@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 195
+    STUBENTRY ?Stub196@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 196
+    STUBENTRY ?Stub197@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 197
+    STUBENTRY ?Stub198@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 198
+    STUBENTRY ?Stub199@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 199
+    STUBENTRY ?Stub200@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 200
+    STUBENTRY ?Stub201@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 201
+    STUBENTRY ?Stub202@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 202
+    STUBENTRY ?Stub203@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 203
+    STUBENTRY ?Stub204@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 204
+    STUBENTRY ?Stub205@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 205
+    STUBENTRY ?Stub206@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 206
+    STUBENTRY ?Stub207@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 207
+    STUBENTRY ?Stub208@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 208
+    STUBENTRY ?Stub209@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 209
+    STUBENTRY ?Stub210@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 210
+    STUBENTRY ?Stub211@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 211
+    STUBENTRY ?Stub212@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 212
+    STUBENTRY ?Stub213@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 213
+    STUBENTRY ?Stub214@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 214
+    STUBENTRY ?Stub215@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 215
+    STUBENTRY ?Stub216@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 216
+    STUBENTRY ?Stub217@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 217
+    STUBENTRY ?Stub218@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 218
+    STUBENTRY ?Stub219@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 219
+    STUBENTRY ?Stub220@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 220
+    STUBENTRY ?Stub221@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 221
+    STUBENTRY ?Stub222@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 222
+    STUBENTRY ?Stub223@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 223
+    STUBENTRY ?Stub224@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 224
+    STUBENTRY ?Stub225@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 225
+    STUBENTRY ?Stub226@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 226
+    STUBENTRY ?Stub227@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 227
+    STUBENTRY ?Stub228@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 228
+    STUBENTRY ?Stub229@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 229
+    STUBENTRY ?Stub230@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 230
+    STUBENTRY ?Stub231@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 231
+    STUBENTRY ?Stub232@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 232
+    STUBENTRY ?Stub233@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 233
+    STUBENTRY ?Stub234@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 234
+    STUBENTRY ?Stub235@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 235
+    STUBENTRY ?Stub236@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 236
+    STUBENTRY ?Stub237@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 237
+    STUBENTRY ?Stub238@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 238
+    STUBENTRY ?Stub239@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 239
+    STUBENTRY ?Stub240@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 240
+    STUBENTRY ?Stub241@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 241
+    STUBENTRY ?Stub242@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 242
+    STUBENTRY ?Stub243@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 243
+    STUBENTRY ?Stub244@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 244
+    STUBENTRY ?Stub245@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 245
+    STUBENTRY ?Stub246@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 246
+    STUBENTRY ?Stub247@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 247
+    STUBENTRY ?Stub248@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 248
+    STUBENTRY ?Stub249@nsXPTCStubBase@@UEAA?AW4nsresult@@XZ, 249
+
+	END
