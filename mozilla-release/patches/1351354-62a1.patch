# HG changeset patch
# User Haik Aftandilian <haftandilian@mozilla.com>
# Date 1528500674 25200
# Node ID 578ef56130c95c80676f2db78798b37348287e42
# Parent  8aca2cae520b81f6b63b4b60e1c9d5d73648d864
Bug 1351354 - Add missing lock protection to crashReporterAPIData_Hash r=gsvelto

Add missing mutex acquisition calls to protect crashReporterAPIData_Hash and avoid
races with CrashReporter::AnnotateCrashReport() that cause assertion failures.

MozReview-Commit-ID: 6AzSlMMKV3h

diff --git a/toolkit/crashreporter/nsExceptionHandler.cpp b/toolkit/crashreporter/nsExceptionHandler.cpp
--- a/toolkit/crashreporter/nsExceptionHandler.cpp
+++ b/toolkit/crashreporter/nsExceptionHandler.cpp
@@ -2219,16 +2219,17 @@ nsresult AppendAppNotesToCrashReport(con
 
 // Returns true if found, false if not found.
 static bool
 GetAnnotation(const nsACString& key, nsACString& data)
 {
   if (!gExceptionHandler)
     return false;
 
+  MutexAutoLock lock(*crashReporterAPILock);
   nsAutoCString entry;
   if (!crashReporterAPIData_Hash->Get(key, &entry))
     return false;
 
   data = entry;
   return true;
 }
 
@@ -2919,16 +2920,20 @@ WriteAnnotation(PRFileDesc* fd, const ns
 
 template<int N>
 static void
 WriteLiteral(PRFileDesc* fd, const char (&str)[N])
 {
   PR_Write(fd, str, N - 1);
 }
 
+/*
+ * If accessing the AnnotationTable |data| argument requires locks, the
+ * caller should ensure the required locks are already held.
+ */
 static bool
 WriteExtraData(nsIFile* extraFile,
                const AnnotationTable& data,
                const Blacklist& blacklist,
                bool writeCrashTime=false,
                bool truncate=false)
 {
   PRFileDesc* fd;
@@ -3043,21 +3048,24 @@ WriteExtraForMinidump(nsIFile* minidump,
                       const Blacklist& blacklist,
                       nsIFile** extraFile)
 {
   nsCOMPtr<nsIFile> extra;
   if (!GetExtraFileForMinidump(minidump, getter_AddRefs(extra))) {
     return false;
   }
 
-  if (!WriteExtraData(extra, *crashReporterAPIData_Hash,
-                      blacklist,
-                      true /*write crash time*/,
-                      true /*truncate*/)) {
-    return false;
+  {
+    MutexAutoLock lock(*crashReporterAPILock);
+    if (!WriteExtraData(extra, *crashReporterAPIData_Hash,
+                        blacklist,
+                        true /*write crash time*/,
+                        true /*truncate*/)) {
+      return false;
+    }
   }
 
   if (pid && processToCrashFd.count(pid)) {
     PRFileDesc* prFd = processToCrashFd[pid];
     processToCrashFd.erase(pid);
     FILE* fd;
 #if defined(XP_WIN)
     int nativeFd = _open_osfhandle(PR_FileDesc2NativeHandle(prFd), 0);
