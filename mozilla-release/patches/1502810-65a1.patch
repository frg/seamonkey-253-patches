# HG changeset patch
# User Andre Bargull <andre.bargull@gmail.com>
# Date 1541078724 25200
# Node ID 0dd3fef17f153feec51df058785579dcdeb91d83
# Parent  fade2f013efe188b9af33335a57eeb999a32ec4a
Bug 1502810 - Update Intl functions to work with cross-compartment wrappers. r=jorendorff

diff --git a/js/src/builtin/intl/Collator.cpp b/js/src/builtin/intl/Collator.cpp
--- a/js/src/builtin/intl/Collator.cpp
+++ b/js/src/builtin/intl/Collator.cpp
@@ -447,17 +447,16 @@ js::intl_CompareStrings(JSContext* cx, u
     MOZ_ASSERT(args.length() == 3);
     MOZ_ASSERT(args[0].isObject());
     MOZ_ASSERT(args[1].isString());
     MOZ_ASSERT(args[2].isString());
 
     Rooted<CollatorObject*> collator(cx, &args[0].toObject().as<CollatorObject>());
 
     // Obtain a cached UCollator object.
-    // XXX Does this handle Collator instances from other globals correctly?
     void* priv = collator->getReservedSlot(CollatorObject::UCOLLATOR_SLOT).toPrivate();
     UCollator* coll = static_cast<UCollator*>(priv);
     if (!coll) {
         coll = NewUCollator(cx, collator);
         if (!coll)
             return false;
         collator->setReservedSlot(CollatorObject::UCOLLATOR_SLOT, PrivateValue(coll));
     }
diff --git a/js/src/builtin/intl/Collator.js b/js/src/builtin/intl/Collator.js
--- a/js/src/builtin/intl/Collator.js
+++ b/js/src/builtin/intl/Collator.js
@@ -349,17 +349,17 @@ function collatorCompareToBind(x, y) {
  * Spec: ECMAScript Internationalization API Specification, 10.3.3.
  */
 function Intl_Collator_compare_get() {
     // Step 1.
     var collator = this;
 
     // Steps 2-3.
     if (!IsObject(collator) || (collator = GuardToCollator(collator)) === null)
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "Collator", "compare", "Collator");
+        return callFunction(CallCollatorMethodIfWrapped, this, "Intl_Collator_compare_get");
 
     var internals = getCollatorInternals(collator);
 
     // Step 4.
     if (internals.boundCompare === undefined) {
         // Steps 4.a-b.
         var F = callFunction(FunctionBind, collatorCompareToBind, collator);
 
@@ -378,17 +378,17 @@ function Intl_Collator_compare_get() {
  * Spec: ECMAScript Internationalization API Specification, 10.3.4.
  */
 function Intl_Collator_resolvedOptions() {
     // Step 1.
     var collator = this;
 
     // Steps 2-3.
     if (!IsObject(collator) || (collator = GuardToCollator(collator)) === null)
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "Collator", "resolvedOptions", "Collator");
+        return callFunction(CallCollatorMethodIfWrapped, this, "Intl_Collator_resolvedOptions");
 
     var internals = getCollatorInternals(collator);
 
     // Steps 4-5.
     var result = {
         locale: internals.locale,
         usage: internals.usage,
         sensitivity: internals.sensitivity,
diff --git a/js/src/builtin/intl/DateTimeFormat.js b/js/src/builtin/intl/DateTimeFormat.js
--- a/js/src/builtin/intl/DateTimeFormat.js
+++ b/js/src/builtin/intl/DateTimeFormat.js
@@ -183,30 +183,25 @@ function getDateTimeFormatInternals(obj)
     internalProps = resolveDateTimeFormatInternals(internals.lazyData);
     setInternalProperties(internals, internalProps);
     return internalProps;
 }
 
 /**
  * 12.1.10 UnwrapDateTimeFormat( dtf )
  */
-function UnwrapDateTimeFormat(dtf, methodName) {
-    // Step 1 (not applicable in our implementation).
-
-    // Step 2.
-    if (IsObject(dtf) && (GuardToDateTimeFormat(dtf)) === null && dtf instanceof GetDateTimeFormatConstructor())
+function UnwrapDateTimeFormat(dtf) {
+    // Steps 2 and 4 (error handling moved to caller).
+    if (IsObject(dtf) &&
+        GuardToDateTimeFormat(dtf) === null &&
+        !IsWrappedDateTimeFormat(dtf) &&
+        dtf instanceof GetDateTimeFormatConstructor())
+    {
         dtf = dtf[intlFallbackSymbol()];
-
-    // Step 3.
-    if (!IsObject(dtf) || (dtf = GuardToDateTimeFormat(dtf)) === null) {
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "DateTimeFormat", methodName,
-                       "DateTimeFormat");
     }
-
-    // Step 4.
     return dtf;
 }
 
 /**
  * 6.4.2 CanonicalizeTimeZoneName ( timeZone )
  *
  * Canonicalizes the given IANA time zone name.
  *
@@ -796,17 +791,22 @@ function dateTimeFormatFormatToBind(date
  * Returns a function bound to this DateTimeFormat that returns a String value
  * representing the result of calling ToNumber(date) according to the
  * effective locale and the formatting options of this DateTimeFormat.
  *
  * Spec: ECMAScript Internationalization API Specification, 12.4.3.
  */
 function Intl_DateTimeFormat_format_get() {
     // Steps 1-3.
-    var dtf = UnwrapDateTimeFormat(this, "format");
+    var thisArg = UnwrapDateTimeFormat(this);
+    var dtf = thisArg;
+    if (!IsObject(dtf) || (dtf = GuardToDateTimeFormat(dtf)) === null) {
+        return callFunction(CallDateTimeFormatMethodIfWrapped, thisArg,
+                            "Intl_DateTimeFormat_format_get");
+    }
 
     var internals = getDateTimeFormatInternals(dtf);
 
     // Step 4.
     if (internals.boundFormat === undefined) {
         // Steps 4.a-b.
         var F = callFunction(FunctionBind, dateTimeFormatFormatToBind, dtf);
 
@@ -824,19 +824,19 @@ function Intl_DateTimeFormat_format_get(
  *
  * Spec: ECMAScript Internationalization API Specification, 12.4.4.
  */
 function Intl_DateTimeFormat_formatToParts(date) {
     // Step 1.
     var dtf = this;
 
     // Steps 2-3.
-    if (!IsObject(dtf) || (dtf = GuardToDateTimeFormat(dtf)) == null) {
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "DateTimeFormat", "formatToParts",
-                       "DateTimeFormat");
+    if (!IsObject(dtf) || (dtf = GuardToDateTimeFormat(dtf)) === null) {
+        return callFunction(CallDateTimeFormatMethodIfWrapped, this, date,
+                            "Intl_DateTimeFormat_formatToParts");
     }
 
     // Ensure the DateTimeFormat internals are resolved.
     getDateTimeFormatInternals(dtf);
 
     // Steps 4-5.
     var x = (date === undefined) ? std_Date_now() : ToNumber(date);
 
@@ -846,17 +846,22 @@ function Intl_DateTimeFormat_formatToPar
 
 /**
  * Returns the resolved options for a DateTimeFormat object.
  *
  * Spec: ECMAScript Internationalization API Specification, 12.4.5.
  */
 function Intl_DateTimeFormat_resolvedOptions() {
     // Steps 1-3.
-    var dtf = UnwrapDateTimeFormat(this, "resolvedOptions");
+    var thisArg = UnwrapDateTimeFormat(this);
+    var dtf = thisArg;
+    if (!IsObject(dtf) || (dtf = GuardToDateTimeFormat(dtf)) === null) {
+        return callFunction(CallDateTimeFormatMethodIfWrapped, thisArg,
+                            "Intl_DateTimeFormat_resolvedOptions");
+    }
 
     var internals = getDateTimeFormatInternals(dtf);
 
     // Steps 4-5.
     var result = {
         locale: internals.locale,
         calendar: internals.calendar,
         numberingSystem: internals.numberingSystem,
diff --git a/js/src/builtin/intl/NumberFormat.js b/js/src/builtin/intl/NumberFormat.js
--- a/js/src/builtin/intl/NumberFormat.js
+++ b/js/src/builtin/intl/NumberFormat.js
@@ -102,28 +102,25 @@ function getNumberFormatInternals(obj) {
     internalProps = resolveNumberFormatInternals(internals.lazyData);
     setInternalProperties(internals, internalProps);
     return internalProps;
 }
 
 /**
  * 11.1.11 UnwrapNumberFormat( nf )
  */
-function UnwrapNumberFormat(nf, methodName) {
-    // Step 1 (not applicable in our implementation).
-
-    // Step 2.
-    if (IsObject(nf) && (GuardToNumberFormat(nf)) === null && nf instanceof GetNumberFormatConstructor())
+function UnwrapNumberFormat(nf) {
+    // Steps 2 and 4 (error handling moved to caller).
+    if (IsObject(nf) &&
+        GuardToNumberFormat(nf) === null &&
+        !IsWrappedNumberFormat(nf) &&
+        nf instanceof GetNumberFormatConstructor())
+    {
         nf = nf[intlFallbackSymbol()];
-
-    // Step 3.
-    if (!IsObject(nf) || (nf = GuardToNumberFormat(nf)) === null)
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "NumberFormat", methodName, "NumberFormat");
-
-    // Step 4.
+    }
     return nf;
 }
 
 /**
  * Applies digit options used for number formatting onto the intl object.
  *
  * Spec: ECMAScript Internationalization API Specification, 11.1.1.
  */
@@ -418,17 +415,22 @@ function numberFormatFormatToBind(value)
  * Returns a function bound to this NumberFormat that returns a String value
  * representing the result of calling ToNumber(value) according to the
  * effective locale and the formatting options of this NumberFormat.
  *
  * Spec: ECMAScript Internationalization API Specification, 11.4.3.
  */
 function Intl_NumberFormat_format_get() {
     // Steps 1-3.
-    var nf = UnwrapNumberFormat(this, "format");
+    var thisArg = UnwrapNumberFormat(this);
+    var nf = thisArg;
+    if (!IsObject(nf) || (nf = GuardToNumberFormat(nf)) === null) {
+        return callFunction(CallNumberFormatMethodIfWrapped, thisArg,
+                            "Intl_NumberFormat_format_get");
+    }
 
     var internals = getNumberFormatInternals(nf);
 
     // Step 4.
     if (internals.boundFormat === undefined) {
         // Steps 4.a-b.
         var F = callFunction(FunctionBind, numberFormatFormatToBind, nf);
 
@@ -445,18 +447,18 @@ function Intl_NumberFormat_format_get() 
  * 11.4.4 Intl.NumberFormat.prototype.formatToParts ( value )
  */
 function Intl_NumberFormat_formatToParts(value) {
     // Step 1.
     var nf = this;
 
     // Steps 2-3.
     if (!IsObject(nf) || (nf = GuardToNumberFormat(nf)) === null) {
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "NumberFormat", "formatToParts",
-                       "NumberFormat");
+        return callFunction(CallNumberFormatMethodIfWrapped, this, value,
+                            "Intl_NumberFormat_formatToParts");
     }
 
     // Ensure the NumberFormat internals are resolved.
     getNumberFormatInternals(nf);
 
     // Step 4.
     var x = ToNumber(value);
 
@@ -466,17 +468,22 @@ function Intl_NumberFormat_formatToParts
 
 /**
  * Returns the resolved options for a NumberFormat object.
  *
  * Spec: ECMAScript Internationalization API Specification, 11.4.5.
  */
 function Intl_NumberFormat_resolvedOptions() {
     // Steps 1-3.
-    var nf = UnwrapNumberFormat(this, "resolvedOptions");
+    var thisArg = UnwrapNumberFormat(this);
+    var nf = thisArg;
+    if (!IsObject(nf) || (nf = GuardToNumberFormat(nf)) === null) {
+        return callFunction(CallNumberFormatMethodIfWrapped, thisArg,
+                            "Intl_NumberFormat_resolvedOptions");
+    }
 
     var internals = getNumberFormatInternals(nf);
 
     // Steps 4-5.
     var result = {
         locale: internals.locale,
         numberingSystem: internals.numberingSystem,
         style: internals.style,
diff --git a/js/src/builtin/intl/PluralRules.js b/js/src/builtin/intl/PluralRules.js
--- a/js/src/builtin/intl/PluralRules.js
+++ b/js/src/builtin/intl/PluralRules.js
@@ -191,18 +191,20 @@ function Intl_PluralRules_supportedLocal
  *
  * Spec: ECMAScript 402 API, PluralRules, 13.4.3.
  */
 function Intl_PluralRules_select(value) {
     // Step 1.
     let pluralRules = this;
 
     // Steps 2-3.
-    if (!IsObject(pluralRules) || (pluralRules = GuardToPluralRules(pluralRules)) === null)
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "PluralRules", "select", "PluralRules");
+    if (!IsObject(pluralRules) || (pluralRules = GuardToPluralRules(pluralRules)) === null) {
+        return callFunction(CallPluralRulesMethodIfWrapped, this, value,
+                            "Intl_PluralRules_select");
+    }
 
     // Ensure the PluralRules internals are resolved.
     getPluralRulesInternals(pluralRules);
 
     // Step 4.
     let n = ToNumber(value);
 
     // Step 5.
@@ -215,18 +217,18 @@ function Intl_PluralRules_select(value) 
  * Spec: ECMAScript 402 API, PluralRules, 13.4.4.
  */
 function Intl_PluralRules_resolvedOptions() {
     // Step 1.
     var pluralRules = this;
 
     // Steps 2-3.
     if (!IsObject(pluralRules) || (pluralRules = GuardToPluralRules(pluralRules)) === null) {
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "PluralRules", "resolvedOptions",
-                       "PluralRules");
+        return callFunction(CallPluralRulesMethodIfWrapped, this,
+                            "Intl_PluralRules_resolvedOptions");
     }
 
     var internals = getPluralRulesInternals(pluralRules);
 
     // Steps 4-5.
     var result = {
         locale: internals.locale,
         type: internals.type,
diff --git a/js/src/builtin/intl/RelativeTimeFormat.js b/js/src/builtin/intl/RelativeTimeFormat.js
--- a/js/src/builtin/intl/RelativeTimeFormat.js
+++ b/js/src/builtin/intl/RelativeTimeFormat.js
@@ -169,18 +169,22 @@ function Intl_RelativeTimeFormat_support
  *
  * Spec: ECMAScript 402 API, RelativeTImeFormat, 1.4.3.
  */
 function Intl_RelativeTimeFormat_format(value, unit) {
     // Step 1.
     let relativeTimeFormat = this;
 
     // Step 2.
-    if (!IsObject(relativeTimeFormat) || (relativeTimeFormat = GuardToRelativeTimeFormat(relativeTimeFormat)) === null)
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "RelativeTimeFormat", "format", "RelativeTimeFormat");
+    if (!IsObject(relativeTimeFormat) ||
+        (relativeTimeFormat = GuardToRelativeTimeFormat(relativeTimeFormat)) === null)
+    {
+        return callFunction(CallRelativeTimeFormatMethodIfWrapped, this, value, unit,
+                            "Intl_RelativeTimeFormat_format");
+    }
 
     // Ensure the RelativeTimeFormat internals are resolved.
     var internals = getRelativeTimeFormatInternals(relativeTimeFormat);
 
     // Step 3.
     let t = ToNumber(value);
 
     // Step 4.
@@ -222,18 +226,18 @@ function Intl_RelativeTimeFormat_format(
  * Returns the resolved options for a RelativeTimeFormat object.
  *
  * Spec: ECMAScript 402 API, RelativeTimeFormat, 1.4.4.
  */
 function Intl_RelativeTimeFormat_resolvedOptions() {
     var relativeTimeFormat;
     // Check "this RelativeTimeFormat object" per introduction of section 1.4.
     if (!IsObject(this) || (relativeTimeFormat = GuardToRelativeTimeFormat(this)) === null) {
-        ThrowTypeError(JSMSG_INTL_OBJECT_NOT_INITED, "RelativeTimeFormat", "resolvedOptions",
-                       "RelativeTimeFormat");
+        return callFunction(CallRelativeTimeFormatMethodIfWrapped, this,
+                            "Intl_RelativeTimeFormat_resolvedOptions");
     }
 
     var internals = getRelativeTimeFormatInternals(relativeTimeFormat, "resolvedOptions");
 
     var result = {
         locale: internals.locale,
         style: internals.style,
         numeric: internals.numeric,
diff --git a/js/src/js.msg b/js/src/js.msg
--- a/js/src/js.msg
+++ b/js/src/js.msg
@@ -485,17 +485,16 @@ MSG_DEF(JSMSG_DEBUG_NO_BINARY_SOURCE,  0
 MSG_DEF(JSMSG_TESTING_SCRIPTS_ONLY, 0, JSEXN_TYPEERR, "only works on scripts")
 
 // Tracelogger
 MSG_DEF(JSMSG_TRACELOGGER_ENABLE_FAIL, 1, JSEXN_ERR, "enabling tracelogger failed: {0}")
 
 // Intl
 MSG_DEF(JSMSG_DATE_NOT_FINITE,         1, JSEXN_RANGEERR, "date value is not finite in {0}.format()")
 MSG_DEF(JSMSG_INTERNAL_INTL_ERROR,     0, JSEXN_ERR, "internal error while computing Intl data")
-MSG_DEF(JSMSG_INTL_OBJECT_NOT_INITED,  3, JSEXN_TYPEERR, "Intl.{0}.prototype.{1} called on value that's not an object initialized as a {2}")
 MSG_DEF(JSMSG_INVALID_CURRENCY_CODE,   1, JSEXN_RANGEERR, "invalid currency code in NumberFormat(): {0}")
 MSG_DEF(JSMSG_INVALID_DIGITS_VALUE,    1, JSEXN_RANGEERR, "invalid digits value: {0}")
 MSG_DEF(JSMSG_INVALID_KEYS_TYPE,       0, JSEXN_TYPEERR, "calendar info keys must be an object or undefined")
 MSG_DEF(JSMSG_INVALID_KEY,             1, JSEXN_RANGEERR, "invalid key: {0}")
 MSG_DEF(JSMSG_INVALID_LANGUAGE_TAG,    1, JSEXN_RANGEERR, "invalid language tag: {0}")
 MSG_DEF(JSMSG_INVALID_LOCALES_ELEMENT, 0, JSEXN_TYPEERR, "invalid element in locales argument")
 MSG_DEF(JSMSG_INVALID_LOCALE_MATCHER,  1, JSEXN_RANGEERR, "invalid locale matcher in supportedLocalesOf(): {0}")
 MSG_DEF(JSMSG_INVALID_OPTION_VALUE,    2, JSEXN_RANGEERR, "invalid value {1} for option {0}")
diff --git a/js/src/tests/non262/Intl/Collator/cross-compartment.js b/js/src/tests/non262/Intl/Collator/cross-compartment.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/Intl/Collator/cross-compartment.js
@@ -0,0 +1,22 @@
+// |reftest| skip-if(!this.hasOwnProperty("Intl"))
+
+var otherGlobal = newGlobal();
+
+var collator = new Intl.Collator();
+var ccwCollator = new otherGlobal.Intl.Collator();
+
+// Test Intl.Collator.prototype.compare with a CCW object.
+var Intl_Collator_compare_get = Object.getOwnPropertyDescriptor(Intl.Collator.prototype, "compare").get;
+
+assertEq(Intl_Collator_compare_get.call(ccwCollator)("a", "A"),
+         Intl_Collator_compare_get.call(collator)("a", "A"));
+
+// Test Intl.Collator.prototype.resolvedOptions with a CCW object.
+var Intl_Collator_resolvedOptions = Intl.Collator.prototype.resolvedOptions;
+
+assertEq(deepEqual(Intl_Collator_resolvedOptions.call(ccwCollator),
+                   Intl_Collator_resolvedOptions.call(collator)),
+         true);
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
diff --git a/js/src/tests/non262/Intl/DateTimeFormat/cross-compartment.js b/js/src/tests/non262/Intl/DateTimeFormat/cross-compartment.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/Intl/DateTimeFormat/cross-compartment.js
@@ -0,0 +1,70 @@
+// |reftest| skip-if(!this.hasOwnProperty("Intl"))
+
+var otherGlobal = newGlobal();
+
+var dateTimeFormat = new Intl.DateTimeFormat();
+var ccwDateTimeFormat = new otherGlobal.Intl.DateTimeFormat();
+
+// Test Intl.DateTimeFormat.prototype.format with a CCW object.
+var Intl_DateTimeFormat_format_get = Object.getOwnPropertyDescriptor(Intl.DateTimeFormat.prototype, "format").get;
+
+assertEq(Intl_DateTimeFormat_format_get.call(ccwDateTimeFormat)(0),
+         Intl_DateTimeFormat_format_get.call(dateTimeFormat)(0));
+
+// Test Intl.DateTimeFormat.prototype.formatToParts with a CCW object.
+var Intl_DateTimeFormat_formatToParts = Intl.DateTimeFormat.prototype.formatToParts;
+
+assertEq(deepEqual(Intl_DateTimeFormat_formatToParts.call(ccwDateTimeFormat, 0),
+                   Intl_DateTimeFormat_formatToParts.call(dateTimeFormat, 0)),
+         true);
+
+// Test Intl.DateTimeFormat.prototype.resolvedOptions with a CCW object.
+var Intl_DateTimeFormat_resolvedOptions = Intl.DateTimeFormat.prototype.resolvedOptions;
+
+assertEq(deepEqual(Intl_DateTimeFormat_resolvedOptions.call(ccwDateTimeFormat),
+                   Intl_DateTimeFormat_resolvedOptions.call(dateTimeFormat)),
+         true);
+
+// Special case for Intl.DateTimeFormat: The Intl fallback symbol.
+
+function fallbackSymbol(global) {
+    var DTF = global.Intl.DateTimeFormat;
+    return Object.getOwnPropertySymbols(DTF.call(Object.create(DTF.prototype)))[0];
+}
+
+const intlFallbackSymbol = fallbackSymbol(this);
+const otherIntlFallbackSymbol = fallbackSymbol(otherGlobal);
+assertEq(intlFallbackSymbol === otherIntlFallbackSymbol, false);
+
+// Test when the fallback symbol points to a CCW DateTimeFormat object.
+var objWithFallbackCCWDateTimeFormat = {
+    __proto__: Intl.DateTimeFormat.prototype,
+    [intlFallbackSymbol]: ccwDateTimeFormat,
+};
+
+assertEq(Intl_DateTimeFormat_format_get.call(objWithFallbackCCWDateTimeFormat)(0),
+         Intl_DateTimeFormat_format_get.call(dateTimeFormat)(0));
+
+assertEq(deepEqual(Intl_DateTimeFormat_resolvedOptions.call(objWithFallbackCCWDateTimeFormat),
+                   Intl_DateTimeFormat_resolvedOptions.call(dateTimeFormat)),
+         true);
+
+// Ensure the fallback symbol(s) are not accessed for CCW DateTimeFormat objects.
+var ccwDateTimeFormatWithPoisonedFallback = new otherGlobal.Intl.DateTimeFormat();
+Object.setPrototypeOf(ccwDateTimeFormatWithPoisonedFallback, Intl.DateTimeFormat.prototype);
+Object.defineProperty(ccwDateTimeFormatWithPoisonedFallback, intlFallbackSymbol, {
+    get() { throw new Error(); }
+});
+Object.defineProperty(ccwDateTimeFormatWithPoisonedFallback, otherIntlFallbackSymbol, {
+    get() { throw new Error(); }
+});
+
+assertEq(Intl_DateTimeFormat_format_get.call(ccwDateTimeFormatWithPoisonedFallback)(0),
+         Intl_DateTimeFormat_format_get.call(dateTimeFormat)(0));
+
+assertEq(deepEqual(Intl_DateTimeFormat_resolvedOptions.call(ccwDateTimeFormatWithPoisonedFallback),
+                   Intl_DateTimeFormat_resolvedOptions.call(dateTimeFormat)),
+         true);
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
diff --git a/js/src/tests/non262/Intl/NumberFormat/cross-compartment.js b/js/src/tests/non262/Intl/NumberFormat/cross-compartment.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/Intl/NumberFormat/cross-compartment.js
@@ -0,0 +1,70 @@
+// |reftest| skip-if(!this.hasOwnProperty("Intl"))
+
+var otherGlobal = newGlobal();
+
+var numberFormat = new Intl.NumberFormat();
+var ccwNumberFormat = new otherGlobal.Intl.NumberFormat();
+
+// Test Intl.NumberFormat.prototype.format with a CCW object.
+var Intl_NumberFormat_format_get = Object.getOwnPropertyDescriptor(Intl.NumberFormat.prototype, "format").get;
+
+assertEq(Intl_NumberFormat_format_get.call(ccwNumberFormat)(0),
+         Intl_NumberFormat_format_get.call(numberFormat)(0));
+
+// Test Intl.NumberFormat.prototype.formatToParts with a CCW object.
+var Intl_NumberFormat_formatToParts = Intl.NumberFormat.prototype.formatToParts;
+
+assertEq(deepEqual(Intl_NumberFormat_formatToParts.call(ccwNumberFormat, 0),
+                   Intl_NumberFormat_formatToParts.call(numberFormat, 0)),
+         true);
+
+// Test Intl.NumberFormat.prototype.resolvedOptions with a CCW object.
+var Intl_NumberFormat_resolvedOptions = Intl.NumberFormat.prototype.resolvedOptions;
+
+assertEq(deepEqual(Intl_NumberFormat_resolvedOptions.call(ccwNumberFormat),
+                   Intl_NumberFormat_resolvedOptions.call(numberFormat)),
+         true);
+
+// Special case for Intl.NumberFormat: The Intl fallback symbol.
+
+function fallbackSymbol(global) {
+    var NF = global.Intl.NumberFormat;
+    return Object.getOwnPropertySymbols(NF.call(Object.create(NF.prototype)))[0];
+}
+
+const intlFallbackSymbol = fallbackSymbol(this);
+const otherIntlFallbackSymbol = fallbackSymbol(otherGlobal);
+assertEq(intlFallbackSymbol === otherIntlFallbackSymbol, false);
+
+// Test when the fallback symbol points to a CCW NumberFormat object.
+var objWithFallbackCCWNumberFormat = {
+    __proto__: Intl.NumberFormat.prototype,
+    [intlFallbackSymbol]: ccwNumberFormat,
+};
+
+assertEq(Intl_NumberFormat_format_get.call(objWithFallbackCCWNumberFormat)(0),
+         Intl_NumberFormat_format_get.call(numberFormat)(0));
+
+assertEq(deepEqual(Intl_NumberFormat_resolvedOptions.call(objWithFallbackCCWNumberFormat),
+                   Intl_NumberFormat_resolvedOptions.call(numberFormat)),
+         true);
+
+// Ensure the fallback symbol(s) are not accessed for CCW NumberFormat objects.
+var ccwNumberFormatWithPoisonedFallback = new otherGlobal.Intl.NumberFormat();
+Object.setPrototypeOf(ccwNumberFormatWithPoisonedFallback, Intl.NumberFormat.prototype);
+Object.defineProperty(ccwNumberFormatWithPoisonedFallback, intlFallbackSymbol, {
+    get() { throw new Error(); }
+});
+Object.defineProperty(ccwNumberFormatWithPoisonedFallback, otherIntlFallbackSymbol, {
+    get() { throw new Error(); }
+});
+
+assertEq(Intl_NumberFormat_format_get.call(ccwNumberFormatWithPoisonedFallback)(0),
+         Intl_NumberFormat_format_get.call(numberFormat)(0));
+
+assertEq(deepEqual(Intl_NumberFormat_resolvedOptions.call(ccwNumberFormatWithPoisonedFallback),
+                   Intl_NumberFormat_resolvedOptions.call(numberFormat)),
+         true);
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
diff --git a/js/src/tests/non262/Intl/PluralRules/cross-compartment.js b/js/src/tests/non262/Intl/PluralRules/cross-compartment.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/Intl/PluralRules/cross-compartment.js
@@ -0,0 +1,22 @@
+// |reftest| skip-if(!this.hasOwnProperty("Intl"))
+
+var otherGlobal = newGlobal();
+
+var pluralRules = new Intl.PluralRules();
+var ccwPluralRules = new otherGlobal.Intl.PluralRules();
+
+// Test Intl.PluralRules.prototype.select with a CCW object.
+var Intl_PluralRules_select = Intl.PluralRules.prototype.select;
+
+assertEq(Intl_PluralRules_select.call(ccwPluralRules, 0),
+         Intl_PluralRules_select.call(pluralRules, 0));
+
+// Test Intl.PluralRules.prototype.resolvedOptions with a CCW object.
+var Intl_PluralRules_resolvedOptions = Intl.PluralRules.prototype.resolvedOptions;
+
+assertEq(deepEqual(Intl_PluralRules_resolvedOptions.call(ccwPluralRules),
+                   Intl_PluralRules_resolvedOptions.call(pluralRules)),
+         true);
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
diff --git a/js/src/tests/non262/Intl/RelativeTimeFormat/cross-compartment.js b/js/src/tests/non262/Intl/RelativeTimeFormat/cross-compartment.js
new file mode 100644
--- /dev/null
+++ b/js/src/tests/non262/Intl/RelativeTimeFormat/cross-compartment.js
@@ -0,0 +1,25 @@
+// |reftest| skip-if(!this.hasOwnProperty("Intl")||!this.hasOwnProperty("addIntlExtras"))
+
+addIntlExtras(Intl);
+
+var otherGlobal = newGlobal();
+otherGlobal.addIntlExtras(otherGlobal.Intl);
+
+var relativeTimeFormat = new Intl.RelativeTimeFormat();
+var ccwRelativeTimeFormat = new otherGlobal.Intl.RelativeTimeFormat();
+
+// Test Intl.RelativeTimeFormat.prototype.format with a CCW object.
+var Intl_RelativeTimeFormat_format = Intl.RelativeTimeFormat.prototype.format;
+
+assertEq(Intl_RelativeTimeFormat_format.call(ccwRelativeTimeFormat, 0, "hour"),
+         Intl_RelativeTimeFormat_format.call(relativeTimeFormat, 0, "hour"));
+
+// Test Intl.RelativeTimeFormat.prototype.resolvedOptions with a CCW object.
+var Intl_RelativeTimeFormat_resolvedOptions = Intl.RelativeTimeFormat.prototype.resolvedOptions;
+
+assertEq(deepEqual(Intl_RelativeTimeFormat_resolvedOptions.call(ccwRelativeTimeFormat),
+                   Intl_RelativeTimeFormat_resolvedOptions.call(relativeTimeFormat)),
+         true);
+
+if (typeof reportCompare === "function")
+    reportCompare(true, true);
diff --git a/js/src/vm/SelfHosting.cpp b/js/src/vm/SelfHosting.cpp
--- a/js/src/vm/SelfHosting.cpp
+++ b/js/src/vm/SelfHosting.cpp
@@ -2541,16 +2541,33 @@ static const JSFunctionSpec intrinsic_fu
                     intrinsic_GuardToBuiltin<NumberFormatObject>, 1,0,
                     IntlGuardToNumberFormat),
     JS_INLINABLE_FN("GuardToPluralRules",
                     intrinsic_GuardToBuiltin<PluralRulesObject>, 1,0,
                     IntlGuardToPluralRules),
     JS_INLINABLE_FN("GuardToRelativeTimeFormat",
                     intrinsic_GuardToBuiltin<RelativeTimeFormatObject>, 1,0,
                     IntlGuardToRelativeTimeFormat),
+
+    JS_FN("IsWrappedDateTimeFormat",
+          intrinsic_IsWrappedInstanceOfBuiltin<DateTimeFormatObject>, 1,0),
+    JS_FN("IsWrappedNumberFormat",
+          intrinsic_IsWrappedInstanceOfBuiltin<NumberFormatObject>, 1,0),
+
+    JS_FN("CallCollatorMethodIfWrapped",
+          CallNonGenericSelfhostedMethod<Is<CollatorObject>>, 2,0),
+    JS_FN("CallDateTimeFormatMethodIfWrapped",
+          CallNonGenericSelfhostedMethod<Is<DateTimeFormatObject>>, 2,0),
+    JS_FN("CallNumberFormatMethodIfWrapped",
+          CallNonGenericSelfhostedMethod<Is<NumberFormatObject>>, 2,0),
+    JS_FN("CallPluralRulesMethodIfWrapped",
+          CallNonGenericSelfhostedMethod<Is<PluralRulesObject>>, 2,0),
+    JS_FN("CallRelativeTimeFormatMethodIfWrapped",
+          CallNonGenericSelfhostedMethod<Is<RelativeTimeFormatObject>>, 2,0),
+
     JS_FN("GetDateTimeFormatConstructor",
           intrinsic_GetBuiltinIntlConstructor<GlobalObject::getOrCreateDateTimeFormatConstructor>,
           0,0),
     JS_FN("GetNumberFormatConstructor",
           intrinsic_GetBuiltinIntlConstructor<GlobalObject::getOrCreateNumberFormatConstructor>,
           0,0),
 
     JS_FN("GetOwnPropertyDescriptorToArray", GetOwnPropertyDescriptorToArray, 2,0),
