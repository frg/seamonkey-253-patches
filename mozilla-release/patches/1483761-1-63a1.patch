# HG changeset patch
# User Chris Peterson <cpeterson@mozilla.com>
# Date 1534352527 25200
#      Wed Aug 15 10:02:07 2018 -0700
# Node ID f9a890fba236bdcd13e72cbede68c9d6755f3b78
# Parent  96c84ecff48708165d27b56260f11d60c04166d2
Bug 1483761 - Enable clang's -Wc++2a-compat warnings. r=glandium

Warn about C++ constructs whose meaning change in C++2a.

https://clang.llvm.org/docs/DiagnosticsReference.html#wc-2a-compat

So far the only -Wc++2a-compat check that I know of is for valid pre-C++2a code that inadvertently parses as C++2a's new <=> "spaceship" comparison operator. `f<&A::operator<=>();` is an example of a warning reported for a real project on GitHub. That code can be rewritten as `f< &operator<= >();`.

There are currently no -Wc++2a-compat warnings in mozilla-central.

Differential Revision: https://phabricator.services.mozilla.com/D3478

diff --git a/build/moz.configure/warnings.configure b/build/moz.configure/warnings.configure
--- a/build/moz.configure/warnings.configure
+++ b/build/moz.configure/warnings.configure
@@ -50,16 +50,17 @@ add_gcc_warning('-Wno-invalid-offsetof',
 # catches objects passed by value to variadic functions.
 check_and_add_gcc_warning('-Wclass-varargs')
 
 # catches issues around loops
 check_and_add_gcc_warning('-Wloop-analysis')
 
 # catches C++ version forward-compat issues
 check_and_add_gcc_warning('-Wc++1z-compat', cxx_compiler)
+check_and_add_gcc_warning('-Wc++2a-compat', cxx_compiler)
 
 # catches possible misuse of the comma operator
 check_and_add_gcc_warning('-Wcomma', cxx_compiler)
 
 # catches duplicated conditions in if-else-if chains
 check_and_add_gcc_warning('-Wduplicated-cond')
 
 # catches unintentional switch case fallthroughs
