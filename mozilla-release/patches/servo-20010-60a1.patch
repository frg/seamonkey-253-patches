# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1518266798 18000
# Node ID a7613be6bcd21f4ee8b7e49034c4c18b0b9d163d
# Parent  17adccb7d50087670af21ca61f7b46dd2292e217
servo: Merge #20010 - style: Cascade pres hints after normal user rules (from emilio:pres-hints-cascade); r=bholley

Per https://drafts.csswg.org/css-cascade/#preshint and
https://html.spec.whatwg.org/#presentational-hints.

This was causing failures in the link color reftests with the preferences sheet
as a User sheet.

Bug: 1436782
Reviewed-by: bholley
MozReview-Commit-ID: 9iwEqPBw4CF
Source-Repo: https://github.com/servo/servo
Source-Revision: 2cc75a783d1a7e2a82ef1e6502ca5a4463a3fb4b

diff --git a/servo/components/style/rule_tree/mod.rs b/servo/components/style/rule_tree/mod.rs
--- a/servo/components/style/rule_tree/mod.rs
+++ b/servo/components/style/rule_tree/mod.rs
@@ -467,32 +467,38 @@ impl RuleTree {
     }
 }
 
 /// The number of RuleNodes added to the free list before we will consider
 /// doing a GC when calling maybe_gc().  (The value is copied from Gecko,
 /// where it likely did not result from a rigorous performance analysis.)
 const RULE_TREE_GC_INTERVAL: usize = 300;
 
-/// The cascade level these rules are relevant at, as per[1].
+/// The cascade level these rules are relevant at, as per[1][2][3].
+///
+/// Presentational hints for SVG and HTML are in the "author-level
+/// zero-specificity" level, that is, right after user rules, and before author
+/// rules.
 ///
 /// The order of variants declared here is significant, and must be in
 /// _ascending_ order of precedence.
 ///
 /// [1]: https://drafts.csswg.org/css-cascade/#cascade-origin
+/// [2]: https://drafts.csswg.org/css-cascade/#preshint
+/// [3]: https://html.spec.whatwg.org/multipage/#presentational-hints
 #[repr(u8)]
 #[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd)]
 #[cfg_attr(feature = "servo", derive(MallocSizeOf))]
 pub enum CascadeLevel {
     /// Normal User-Agent rules.
     UANormal = 0,
+    /// User normal rules.
+    UserNormal,
     /// Presentational hints.
     PresHints,
-    /// User normal rules.
-    UserNormal,
     /// Author normal rules.
     AuthorNormal,
     /// Style attribute normal rules.
     StyleAttributeNormal,
     /// SVG SMIL animations.
     SMILOverride,
     /// CSS animations and script-generated animations.
     Animations,
diff --git a/servo/components/style/stylist.rs b/servo/components/style/stylist.rs
--- a/servo/components/style/stylist.rs
+++ b/servo/components/style/stylist.rs
@@ -1152,17 +1152,20 @@ impl Stylist {
         //
         // This should probably be an argument to `update`, and use the quirks
         // mode info in the `SharedLayoutContext`.
         self.quirks_mode = quirks_mode;
     }
 
     /// Returns the applicable CSS declarations for the given element.
     ///
-    /// This corresponds to `ElementRuleCollector` in WebKit.
+    /// This corresponds to `ElementRuleCollector` in WebKit, and should push to
+    /// elements in the list in the order defined by:
+    ///
+    /// https://drafts.csswg.org/css-cascade/#cascade-origin
     pub fn push_applicable_declarations<E, F>(
         &self,
         element: E,
         pseudo_element: Option<&PseudoElement>,
         style_attribute: Option<ArcBorrow<Locked<PropertyDeclarationBlock>>>,
         smil_override: Option<ArcBorrow<Locked<PropertyDeclarationBlock>>>,
         animation_rules: AnimationRules,
         rule_inclusion: RuleInclusion,
@@ -1189,67 +1192,70 @@ impl Stylist {
         let only_default_rules =
             rule_inclusion == RuleInclusion::DefaultOnly;
         let matches_user_rules =
             rule_hash_target.matches_user_and_author_rules();
         let matches_author_rules =
             matches_user_rules &&
             self.author_styles_enabled == AuthorStylesEnabled::Yes;
 
-        // Step 1: Normal user-agent rules.
+        // Normal user-agent rules.
         if let Some(map) = self.cascade_data.user_agent.cascade_data.normal_rules(pseudo_element) {
             map.get_all_matching_rules(
                 element,
                 rule_hash_target,
                 applicable_declarations,
                 context,
                 flags_setter,
                 CascadeLevel::UANormal
             );
         }
 
+        // NB: the following condition, although it may look somewhat
+        // inaccurate, would be equivalent to something like:
+        //
+        //     element.matches_user_and_author_rules() ||
+        //     (is_implemented_pseudo &&
+        //      rule_hash_target.matches_user_and_author_rules())
+        //
+        // Which may be more what you would probably expect.
+        if matches_user_rules {
+            // User normal rules.
+            if let Some(map) = self.cascade_data.user.normal_rules(pseudo_element) {
+                map.get_all_matching_rules(
+                    element,
+                    rule_hash_target,
+                    applicable_declarations,
+                    context,
+                    flags_setter,
+                    CascadeLevel::UserNormal,
+                );
+            }
+        }
+
         if pseudo_element.is_none() && !only_default_rules {
-            // Step 2: Presentational hints.
+            // Presentational hints.
+            //
+            // These go before author rules, but after user rules, see:
+            // https://drafts.csswg.org/css-cascade/#preshint
             let length_before_preshints = applicable_declarations.len();
             element.synthesize_presentational_hints_for_legacy_attributes(
                 context.visited_handling(),
                 applicable_declarations
             );
             if applicable_declarations.len() != length_before_preshints {
                 if cfg!(debug_assertions) {
                     for declaration in &applicable_declarations[length_before_preshints..] {
                         assert_eq!(declaration.level(), CascadeLevel::PresHints);
                     }
                 }
             }
         }
 
-        // NB: the following condition, although it may look somewhat
-        // inaccurate, would be equivalent to something like:
-        //
-        //     element.matches_user_and_author_rules() ||
-        //     (is_implemented_pseudo &&
-        //      rule_hash_target.matches_user_and_author_rules())
-        //
-        // Which may be more what you would probably expect.
-        if matches_user_rules {
-            // Step 3a: User normal rules.
-            if let Some(map) = self.cascade_data.user.normal_rules(pseudo_element) {
-                map.get_all_matching_rules(
-                    element,
-                    rule_hash_target,
-                    applicable_declarations,
-                    context,
-                    flags_setter,
-                    CascadeLevel::UserNormal,
-                );
-            }
-        }
-
-        // Step 3b: XBL / Shadow DOM rules.
+        // XBL / Shadow DOM rules, which are author rules too.
         //
         // TODO(emilio): Cascade order here is wrong for Shadow DOM. In
         // particular, normally document rules override ::slotted() rules, but
         // for !important it should be the other way around. So probably we need
         // to add some sort of AuthorScoped cascade level or something.
         if matches_author_rules && !only_default_rules {
             // Match slotted rules in reverse order, so that the outer slotted
             // rules come before the inner rules (and thus have less priority).
@@ -1303,72 +1309,70 @@ impl Stylist {
                     &mut matching_context,
                     flags_setter,
                     CascadeLevel::AuthorNormal,
                 );
             }
         });
 
         if matches_author_rules && !only_default_rules && !cut_off_inheritance {
-            // Step 3c: Author normal rules.
+            // Author normal rules.
             if let Some(map) = self.cascade_data.author.normal_rules(pseudo_element) {
                 map.get_all_matching_rules(
                     element,
                     rule_hash_target,
                     applicable_declarations,
                     context,
                     flags_setter,
                     CascadeLevel::AuthorNormal
                 );
             }
         }
 
         if !only_default_rules {
-            // Step 4: Normal style attributes.
+            // Style attribute ("Normal override declarations").
             if let Some(sa) = style_attribute {
                 applicable_declarations.push(
                     ApplicableDeclarationBlock::from_declarations(
                         sa.clone_arc(),
                         CascadeLevel::StyleAttributeNormal
                     )
                 );
             }
 
-            // Step 5: SMIL override.
             // Declarations from SVG SMIL animation elements.
             if let Some(so) = smil_override {
                 applicable_declarations.push(
                     ApplicableDeclarationBlock::from_declarations(
                         so.clone_arc(),
                         CascadeLevel::SMILOverride
                     )
                 );
             }
 
-            // Step 6: Animations.
-            // The animations sheet (CSS animations, script-generated animations,
-            // and CSS transitions that are no longer tied to CSS markup)
+            // The animations sheet (CSS animations, script-generated
+            // animations, and CSS transitions that are no longer tied to CSS
+            // markup).
             if let Some(anim) = animation_rules.0 {
                 applicable_declarations.push(
                     ApplicableDeclarationBlock::from_declarations(
                         anim.clone(),
                         CascadeLevel::Animations
                     )
                 );
             }
         }
 
         //
-        // Steps 7-10 correspond to !important rules, and are handled during
-        // rule tree insertion.
+        // !important rules are handled during rule tree insertion.
         //
 
         if !only_default_rules {
-            // Step 11: Transitions.
-            // The transitions sheet (CSS transitions that are tied to CSS markup)
+            // The transitions sheet (CSS transitions that are tied to CSS
+            // markup).
             if let Some(anim) = animation_rules.1 {
                 applicable_declarations.push(
                     ApplicableDeclarationBlock::from_declarations(
                         anim.clone(),
                         CascadeLevel::Transitions
                     )
                 );
             }
