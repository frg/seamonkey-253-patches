# HG changeset patch
# User Jean-Yves Avenard <jyavenard@mozilla.com>
# Date 1521215261 -3600
#      Fri Mar 16 16:47:41 2018 +0100
# Node ID b821de55844fc0d8bf5def89110bfa55268ed166
# Parent  2ea728f4e688b877820403a452a31c4b25428120
Bug 1444479 - P3. Add Channels(ChannelMap) method. r=padenot

MozReview-Commit-ID: 9sYeh30NHFF

diff --git a/dom/media/AudioConfig.h b/dom/media/AudioConfig.h
--- a/dom/media/AudioConfig.h
+++ b/dom/media/AudioConfig.h
@@ -1,18 +1,19 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 #if !defined(AudioLayout_h)
 #define AudioLayout_h
 
+#include <cstdint>
 #include <initializer_list>
-#include <cstdint>
+#include "mozilla/MathAlgorithms.h"
 #include "nsTArray.h"
 
 namespace mozilla {
 
   // Maximum channel number we can currently handle (7.1)
 #define MAX_AUDIO_CHANNELS 8
 
 class AudioConfig
@@ -98,16 +99,23 @@ public:
     // If aMap is empty, then MappingTable can be used to simply determine if
     // the current layout can be easily reordered to aOther.
     bool MappingTable(const ChannelLayout& aOther, nsTArray<uint8_t>* aMap = nullptr) const;
     bool IsValid() const { return mValid; }
     bool HasChannel(Channel aChannel) const
     {
       return mChannelMap & (1 << aChannel);
     }
+    // Return the number of channels found in this ChannelMap.
+    static uint32_t Channels(ChannelMap aMap)
+    {
+      static_assert(sizeof(ChannelMap) == sizeof(uint32_t),
+                    "Must adjust ChannelMap type");
+      return CountPopulation32(aMap);
+    }
 
     static ChannelLayout SMPTEDefault(
       const ChannelLayout& aChannelLayout);
     static ChannelLayout SMPTEDefault(ChannelMap aMap);
 
     static constexpr ChannelMap UNKNOWN_MAP = 0;
 
     // Common channel layout definitions.
