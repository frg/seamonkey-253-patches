# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1567471162 0
# Node ID 59610f7142a7b87a9c701edafe227491a5e1d531
# Parent  d03e5063962d02a60355c3f04c95cc589fcf4562
Bug 1577501 - [mach] Make sure ./mach help <command> uses the correct Python, r=glandium

Some commands use external argument parsers, so invoking |mach help <command>| will import
external modules (which may only be Python 2 compatible).

This makes sure that we detect the actual subcommand we're generating help for
and use the proper Python.

A much simpler solution would have been to run |mach help| with Python 2 all
the time. However, as we convert things to Python 3 this would have meant that
Python 3 only code would blow up. This would have forced us to continue
supporting Python 2, even for Python 3-only commands.

Differential Revision: https://phabricator.services.mozilla.com/D43989

diff --git a/mach b/mach
--- a/mach
+++ b/mach
@@ -131,18 +131,25 @@ run_py() {
         exec python "$0" "$@"
     else
         echo "This mach command requires $py_executable, which wasn't found on the system!"
         exit 1
     fi
 }
 
 first_arg=$1
+if [ "$first_arg" = "help" ]; then
+    # When running `./mach help <command>`, the correct Python for <command>
+    # needs to be used.
+    first_arg=$2
+fi
+
 if [ -z "$first_arg" ]; then
-    run_py python3
+    # User ran `./mach` or `./mach help`, use Python 3.
+    run_py python3 "$@"
 fi
 
 case "${first_arg}" in
     "-"*)
         # We have global arguments which are tricky to parse from this shell
         # script. So invoke `mach` with a special --print-command argument to
         # return the name of the command. This adds extra overhead when using
         # global arguments, but global arguments are an edge case and this hack
diff --git a/python/mach/mach/dispatcher.py b/python/mach/mach/dispatcher.py
--- a/python/mach/mach/dispatcher.py
+++ b/python/mach/mach/dispatcher.py
@@ -103,45 +103,51 @@ class CommandAction(argparse.Action):
             self._handle_main_help(parser, namespace.verbose)
             sys.exit(0)
         elif values:
             command = values[0].lower()
             args = values[1:]
             if command == 'help':
                 if args and args[0] not in ['-h', '--help']:
                     # Make sure args[0] is indeed a command.
-                    self._handle_command_help(parser, args[0], args)
+                    self._handle_command_help(parser, args[0], args, namespace.print_command)
                 else:
                     self._handle_main_help(parser, namespace.verbose)
                 sys.exit(0)
             elif '-h' in args or '--help' in args:
                 # -h or --help is in the command arguments.
                 if '--' in args:
                     # -- is in command arguments
                     if '-h' in args[:args.index('--')] or '--help' in args[:args.index('--')]:
                         # Honor -h or --help only if it appears before --
-                        self._handle_command_help(parser, command, args)
+                        self._handle_command_help(parser, command, args, namespace.print_command)
                         sys.exit(0)
                 else:
-                    self._handle_command_help(parser, command, args)
+                    self._handle_command_help(parser, command, args, namespace.print_command)
                     sys.exit(0)
         else:
             raise NoCommandError()
 
         # First see if the this is a user-defined alias
         if command in self._context.settings.alias:
             alias = self._context.settings.alias[command]
             defaults = shlex.split(alias)
             command = defaults.pop(0)
             args = defaults + args
 
         if command not in self._mach_registrar.command_handlers:
             # Try to find similar commands, may raise UnknownCommandError.
             command = self._suggest_command(command)
 
+        # This is used by the `mach` driver to find the command name amidst
+        # global arguments.
+        if namespace.print_command:
+            print(command)
+            sys.exit(0)
+
         handler = self._mach_registrar.command_handlers.get(command)
 
         usage = '%(prog)s [global arguments] ' + command + \
             ' [command arguments]'
 
         subcommand = None
 
         # If there are sub-commands, parse the intent out immediately.
@@ -299,22 +305,26 @@ class CommandAction(argparse.Action):
         for arg in handler.arguments:
             # Apply our group keyword.
             group_name = arg[1].get('group')
             if group_name:
                 del arg[1]['group']
                 group = extra_groups[group_name]
             group.add_argument(*arg[0], **arg[1])
 
-    def _handle_command_help(self, parser, command, args):
+    def _handle_command_help(self, parser, command, args, print_command):
         handler = self._mach_registrar.command_handlers.get(command)
 
         if not handler:
             raise UnknownCommandError(command, 'query')
 
+        if print_command:
+            print(command)
+            sys.exit(0)
+
         if handler.subcommand_handlers:
             self._handle_subcommand_help(parser, handler, args)
             return
 
         # This code is worth explaining. Because we are doing funky things with
         # argument registration to allow the same option in both global and
         # command arguments, we can't simply put all arguments on the same
         # parser instance because argparse would complain. We can't register an
diff --git a/python/mach/mach/main.py b/python/mach/mach/main.py
--- a/python/mach/mach/main.py
+++ b/python/mach/mach/main.py
@@ -427,22 +427,16 @@ To see more help for a specific command,
                                                  ' '.join(e.arguments)))
             return 1
 
         if not hasattr(args, 'mach_handler'):
             raise MachError('ArgumentParser result missing mach handler info.')
 
         handler = getattr(args, 'mach_handler')
 
-        # This is used by the `mach` driver to find the command name amidst
-        # global arguments.
-        if args.print_command:
-            print(handler.name)
-            sys.exit(0)
-
         # Add JSON logging to a file if requested.
         if args.logfile:
             self.log_manager.add_json_handler(args.logfile)
 
         # Up the logging level if requested.
         log_level = logging.INFO
         if args.verbose:
             log_level = logging.DEBUG
