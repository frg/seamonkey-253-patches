# HG changeset patch
# User Mark Smith <mcs@pearlcrescent.com>
# Date 1590758326 0
# Node ID cb93af886469eb4e622a0be1ab73434de0b958d8
# Parent  9c00e27173af706ea2de2a6cd564ddc49b23069d
Bug 1641329 - Add -q option to MAR generation scripts r=nthomas

Differential Revision: https://phabricator.services.mozilla.com/D77138

diff --git a/tools/update-packaging/common.sh b/tools/update-packaging/common.sh
--- a/tools/update-packaging/common.sh
+++ b/tools/update-packaging/common.sh
@@ -4,16 +4,18 @@
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 #
 # Code shared by update packaging scripts.
 # Author: Darin Fisher
 #
 
 # -----------------------------------------------------------------------------
+QUIET=0
+
 # By default just assume that these tools exist on our path
 MAR=${MAR:-mar}
 MBSDIFF=${MBSDIFF:-mbsdiff}
 if [[ -z "${MAR_OLD_FORMAT}" ]]; then
   XZ=${XZ:-xz}
   $XZ --version > /dev/null 2>&1
   if [ $? -ne 0 ]; then
     # If $XZ is not set and not found on the path then this is probably
@@ -39,16 +41,22 @@ fi
 
 # -----------------------------------------------------------------------------
 # Helper routines
 
 notice() {
   echo "$*" 1>&2
 }
 
+verbose_notice() {
+  if [ $QUIET -eq 0 ]; then
+    notice "$*"
+  fi
+}
+
 get_file_size() {
   info=($(ls -ln "$1"))
   echo ${info[4]}
 }
 
 copy_perm() {
   reference="$1"
   target="$2"
@@ -77,23 +85,23 @@ make_add_instruction() {
     forced=
   fi
 
   is_extension=$(echo "$f" | grep -c 'distribution/extensions/.*/')
   if [ $is_extension = "1" ]; then
     # Use the subdirectory of the extensions folder as the file to test
     # before performing this add instruction.
     testdir=$(echo "$f" | sed 's/\(.*distribution\/extensions\/[^\/]*\)\/.*/\1/')
-    notice "     add-if \"$testdir\" \"$f\""
+    verbose_notice "     add-if \"$testdir\" \"$f\""
     echo "add-if \"$testdir\" \"$f\"" >> "$filev2"
     if [ ! $filev3 = "" ]; then
       echo "add-if \"$testdir\" \"$f\"" >> "$filev3"
     fi
   else
-    notice "        add \"$f\"$forced"
+    verbose_notice "        add \"$f\"$forced"
     echo "add \"$f\"" >> "$filev2"
     if [ ! "$filev3" = "" ]; then
       echo "add \"$f\"" >> "$filev3"
     fi
   fi
 }
 
 check_for_add_if_not_update() {
@@ -118,35 +126,35 @@ check_for_add_to_manifestv2() {
   ## 'false'... because this is bash. Oh yay!
   return 1;
 }
 
 make_add_if_not_instruction() {
   f="$1"
   filev3="$2"
 
-  notice " add-if-not \"$f\" \"$f\""
+  verbose_notice " add-if-not \"$f\" \"$f\""
   echo "add-if-not \"$f\" \"$f\"" >> "$filev3"
 }
 
 make_patch_instruction() {
   f="$1"
   filev2="$2"
   filev3="$3"
 
   is_extension=$(echo "$f" | grep -c 'distribution/extensions/.*/')
   if [ $is_extension = "1" ]; then
     # Use the subdirectory of the extensions folder as the file to test
     # before performing this add instruction.
     testdir=$(echo "$f" | sed 's/\(.*distribution\/extensions\/[^\/]*\)\/.*/\1/')
-    notice "   patch-if \"$testdir\" \"$f.patch\" \"$f\""
+    verbose_notice "   patch-if \"$testdir\" \"$f.patch\" \"$f\""
     echo "patch-if \"$testdir\" \"$f.patch\" \"$f\"" >> "$filev2"
     echo "patch-if \"$testdir\" \"$f.patch\" \"$f\"" >> "$filev3"
   else
-    notice "      patch \"$f.patch\" \"$f\""
+    verbose_notice "      patch \"$f.patch\" \"$f\""
     echo "patch \"$f.patch\" \"$f\"" >> "$filev2"
     echo "patch \"$f.patch\" \"$f\"" >> "$filev3"
   fi
 }
 
 append_remove_instructions() {
   dir="$1"
   filev2="$2"
@@ -166,27 +174,27 @@ append_remove_instructions() {
       f=$(echo ${files[$i]} | tr "|" " " | tr -d '\r')
       # Trim whitespace
       f=$(echo $f)
       # Exclude blank lines.
       if [ -n "$f" ]; then
         # Exclude comments
         if [ ! $(echo "$f" | grep -c '^#') = 1 ]; then
           if [ $(echo "$f" | grep -c '\/$') = 1 ]; then
-            notice "      rmdir \"$f\""
+            verbose_notice "      rmdir \"$f\""
             echo "rmdir \"$f\"" >> "$filev2"
             echo "rmdir \"$f\"" >> "$filev3"
           elif [ $(echo "$f" | grep -c '\/\*$') = 1 ]; then
             # Remove the *
             f=$(echo "$f" | sed -e 's:\*$::')
-            notice "    rmrfdir \"$f\""
+            verbose_notice "    rmrfdir \"$f\""
             echo "rmrfdir \"$f\"" >> "$filev2"
             echo "rmrfdir \"$f\"" >> "$filev3"
           else
-            notice "     remove \"$f\""
+            verbose_notice "     remove \"$f\""
             echo "remove \"$f\"" >> "$filev2"
             echo "remove \"$f\"" >> "$filev3"
           fi
         fi
       fi
     done
   fi
 }
diff --git a/tools/update-packaging/make_full_update.sh b/tools/update-packaging/make_full_update.sh
--- a/tools/update-packaging/make_full_update.sh
+++ b/tools/update-packaging/make_full_update.sh
@@ -23,20 +23,26 @@ fi
 
 if [ $1 = -h ]; then
   print_usage
   notice ""
   notice "The contents of DIRECTORY will be stored in ARCHIVE."
   notice ""
   notice "Options:"
   notice "  -h  show this help text"
+  notice "  -q  be less verbose"
   notice ""
   exit 1
 fi
 
+if [ $1 = -q ]; then
+  QUIET=1
+  shift
+fi
+
 # -----------------------------------------------------------------------------
 
 archive="$1"
 targetdir="$2"
 # Prevent the workdir from being inside the targetdir so it isn't included in
 # the update mar.
 if [ $(echo "$targetdir" | grep -c '\/$') = 1 ]; then
   # Remove the /
diff --git a/tools/update-packaging/make_incremental_update.sh b/tools/update-packaging/make_incremental_update.sh
--- a/tools/update-packaging/make_incremental_update.sh
+++ b/tools/update-packaging/make_incremental_update.sh
@@ -16,16 +16,17 @@ print_usage() {
   notice "Usage: $(basename $0) [OPTIONS] ARCHIVE FROMDIR TODIR"
   notice ""
   notice "The differences between FROMDIR and TODIR will be stored in ARCHIVE."
   notice ""
   notice "Options:"
   notice "  -h  show this help text"
   notice "  -f  clobber this file in the installation"
   notice "      Must be a path to a file to clobber in the partial update."
+  notice "  -q  be less verbose"
   notice ""
 }
 
 check_for_forced_update() {
   force_list="$1"
   forced_file_chk="$2"
 
   local f
@@ -78,21 +79,23 @@ check_for_forced_update() {
 
 if [ $# = 0 ]; then
   print_usage
   exit 1
 fi
 
 requested_forced_updates='Contents/MacOS/firefox'
 
-while getopts "hf:" flag
+while getopts "hqf:" flag
 do
    case "$flag" in
       h) print_usage; exit 0
       ;;
+      q) QUIET=1
+      ;;
       f) requested_forced_updates="$requested_forced_updates $OPTARG"
       ;;
       ?) print_usage; exit 1
       ;;
    esac
 done
 
 # -----------------------------------------------------------------------------
@@ -194,17 +197,17 @@ for ((i=0; $i<$num_oldfiles; i=$i+1)); d
       continue 1
     fi
 
     if ! diff "$olddir/$f" "$newdir/$f" > /dev/null; then
       # Compute both the compressed binary diff and the compressed file, and
       # compare the sizes.  Then choose the smaller of the two to package.
       dir=$(dirname "$workdir/$f")
       mkdir -p "$dir"
-      notice "diffing \"$f\""
+      verbose_notice "diffing \"$f\""
       # MBSDIFF_HOOK represents the communication interface with funsize and,
       # if enabled, caches the intermediate patches for future use and
       # compute avoidance
       #
       # An example of MBSDIFF_HOOK env variable could look like this:
       # export MBSDIFF_HOOK="myscript.sh -A https://funsize/api -c /home/user"
       # where myscript.sh has the following usage:
       # myscript.sh -A SERVER-URL [-c LOCAL-CACHE-DIR-PATH] [-g] [-u] \
@@ -219,26 +222,26 @@ for ((i=0; $i<$num_oldfiles; i=$i+1)); d
           $BZIP2 -z9 "$workdir/$f.patch"
         else
           $XZ --compress $BCJ_OPTIONS --lzma2 --format=xz --check=crc64 --force "$workdir/$f.patch"
         fi
       else
         # if service enabled then check patch existence for retrieval
         if [[ -n $MAR_OLD_FORMAT ]]; then
           if $MBSDIFF_HOOK -g "$olddir/$f" "$newdir/$f" "$workdir/$f.patch.bz2"; then
-            notice "file \"$f\" found in funsize, diffing skipped"
+            verbose_notice "file \"$f\" found in funsize, diffing skipped"
           else
             # if not found already - compute it and cache it for future use
             $MBSDIFF "$olddir/$f" "$newdir/$f" "$workdir/$f.patch"
             $BZIP2 -z9 "$workdir/$f.patch"
             $MBSDIFF_HOOK -u "$olddir/$f" "$newdir/$f" "$workdir/$f.patch.bz2"
           fi
         else
           if $MBSDIFF_HOOK -g "$olddir/$f" "$newdir/$f" "$workdir/$f.patch.xz"; then
-            notice "file \"$f\" found in funsize, diffing skipped"
+            verbose_notice "file \"$f\" found in funsize, diffing skipped"
           else
             # if not found already - compute it and cache it for future use
             $MBSDIFF "$olddir/$f" "$newdir/$f" "$workdir/$f.patch"
             $XZ --compress $BCJ_OPTIONS --lzma2 --format=xz --check=crc64 --force "$workdir/$f.patch"
             $MBSDIFF_HOOK -u "$olddir/$f" "$newdir/$f" "$workdir/$f.patch.xz"
           fi
         fi
       fi
@@ -309,17 +312,17 @@ for ((i=0; $i<$num_newfiles; i=$i+1)); d
 
   archivefiles="$archivefiles \"$f\""
 done
 
 notice ""
 notice "Adding file remove instructions to update manifests"
 for ((i=0; $i<$num_removes; i=$i+1)); do
   f="${remove_array[$i]}"
-  notice "     remove \"$f\""
+  verbose_notice "     remove \"$f\""
   echo "remove \"$f\"" >> $updatemanifestv2
   echo "remove \"$f\"" >> $updatemanifestv3
 done
 
 # Add remove instructions for any dead files.
 notice ""
 notice "Adding file and directory remove instructions from file 'removed-files'"
 append_remove_instructions "$newdir" "$updatemanifestv2" "$updatemanifestv3"
@@ -327,17 +330,17 @@ append_remove_instructions "$newdir" "$u
 notice ""
 notice "Adding directory remove instructions for directories that no longer exist"
 num_olddirs=${#olddirs[*]}
 
 for ((i=0; $i<$num_olddirs; i=$i+1)); do
   f="${olddirs[$i]}"
   # If this dir doesn't exist in the new directory remove it.
   if [ ! -d "$newdir/$f" ]; then
-    notice "      rmdir $f/"
+    verbose_notice "      rmdir $f/"
     echo "rmdir \"$f/\"" >> $updatemanifestv2
     echo "rmdir \"$f/\"" >> $updatemanifestv3
   fi
 done
 
 if [[ -n $MAR_OLD_FORMAT ]]; then
   $BZIP2 -z9 "$updatemanifestv2" && mv -f "$updatemanifestv2.bz2" "$updatemanifestv2"
   $BZIP2 -z9 "$updatemanifestv3" && mv -f "$updatemanifestv3.bz2" "$updatemanifestv3"
