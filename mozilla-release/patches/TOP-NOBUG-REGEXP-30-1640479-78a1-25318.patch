# HG changeset patch
# User Dmitry Butskoy <buc@buc.me>
# Date 1690630900 -7200
# Parent  6229804f744ac44514d88c3c70fa0114354172d4
No Bug - Import new regexp V8 engine. r=frg a=frg

hawkeye116477 <hawkeye116477@gmail.com>
Bug 1640479: Don't set kind if initializeNamedCaptures fails

If we throw an OOM in initializeNamedCaptures for a RegExpShared, we will set kind to RegExp, but not initialize the named captures data. If we recover from the OOM and then execute the same regexp, the cached RegExpShared will not be reparsed, and we won't create named captures for it.

The fix is to reorder CompilePattern so that we only change the state of the RegExpShared after all of the initialization has succeeded. initializeNamedCaptures already avoids this problem by saving the updates until the end.

diff --git a/js/src/jit-test/tests/regexp/bug1640479.js b/js/src/jit-test/tests/regexp/bug1640479.js
new file mode 100644
--- /dev/null
+++ b/js/src/jit-test/tests/regexp/bug1640479.js
@@ -0,0 +1,15 @@
+// |jit-test| skip-if: !('oomTest' in this)
+
+var failures = 0;
+var i = 0;
+
+function foo() {
+    var e;
+    var r = RegExp("(?<_" + (i++) + "a>)");
+    try { e = r.exec("a"); } catch {}
+    e = r.exec("a");
+    if (e.groups === undefined) failures++;
+}
+
+oomTest(foo);
+assertEq(failures, 0);
diff --git a/js/src/new-regexp/RegExpAPI.cpp b/js/src/new-regexp/RegExpAPI.cpp
--- a/js/src/new-regexp/RegExpAPI.cpp
+++ b/js/src/new-regexp/RegExpAPI.cpp
@@ -466,26 +466,26 @@ bool CompilePattern(JSContext* cx, Mutab
         }
       }
       JS::AutoCheckCannotGC nogc(cx);
       if (searchAtom && !UseBoyerMoore(searchAtom, nogc)) {
         re->useAtomMatch(searchAtom);
         return true;
       }
     }
-    // Add one to account for the whole-match capture
-    uint32_t pairCount = data.capture_count + 1;
-    re->useRegExpMatch(pairCount);
-
     if (!data.capture_name_map.is_null()) {
       RootedNativeObject namedCaptures(cx, data.capture_name_map->inner());
       if (!RegExpShared::initializeNamedCaptures(cx, re, namedCaptures)) {
         return false;
       }
     }
+    // All fallible initialization has succeeded, so we can change state.
+    // Add one to capture_count to account for the whole-match capture.
+    uint32_t pairCount = data.capture_count + 1;
+    re->useRegExpMatch(pairCount);
   }
 
   MOZ_ASSERT(re->kind() == RegExpShared::Kind::RegExp);
 
   HandleScope handleScope(cx->isolate);
   RegExpCompiler compiler(cx->isolate, &zone, data.capture_count,
                           input->hasLatin1Chars());
 
diff --git a/js/src/vm/RegExpObject.cpp b/js/src/vm/RegExpObject.cpp
--- a/js/src/vm/RegExpObject.cpp
+++ b/js/src/vm/RegExpObject.cpp
@@ -1206,17 +1206,16 @@ void RegExpShared::useRegExpMatch(size_t
 }
 
 /* static */
 bool
 RegExpShared::initializeNamedCaptures(JSContext* cx,
                                       HandleRegExpShared re,
                                       HandleNativeObject namedCaptures)
 {
-    MOZ_ASSERT(re->kind() == RegExpShared::Kind::RegExp);
     MOZ_ASSERT(!re->groupsTemplate_);
     MOZ_ASSERT(!re->namedCaptureIndices_);
 
     // The irregexp parser returns named capture information in the form
     // of an ArrayObject, where even elements store the capture name and
     // odd elements store the corresponding capture index. We create a
     // template object with a property for each capture name, and store
     // the capture indices as a heap-allocated array.
