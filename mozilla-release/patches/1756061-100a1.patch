# HG changeset patch
# User Dana Keeler <dkeeler@mozilla.com>
# Date 1646954460 0
# Node ID f0bcbabee6dbf3d227307af0c1f26a92316cba4e
# Parent  2a867450a0ae68a4ad11f32c9be6f989bdcd2533
Bug 1756061 - PSM changes corresponding to mozilla::pkix signature verification changes in bug 1755092 r=jschanck

Bug 1755092 changed how mozilla::pkix verifies signatures. This patch makes the
corresponding changes in PSM.

Depends on D140597

Differential Revision: https://phabricator.services.mozilla.com/D139202

diff --git a/security/apps/AppTrustDomain.cpp b/security/apps/AppTrustDomain.cpp
--- a/security/apps/AppTrustDomain.cpp
+++ b/security/apps/AppTrustDomain.cpp
@@ -252,45 +252,45 @@ AppTrustDomain::CheckRSAPublicKeyModulus
   EndEntityOrCA /*endEntityOrCA*/, unsigned int modulusSizeInBits)
 {
   if (modulusSizeInBits < 2048u) {
     return Result::ERROR_INADEQUATE_KEY_SIZE;
   }
   return Success;
 }
 
-Result
-AppTrustDomain::VerifyRSAPKCS1SignedDigest(const SignedDigest& signedDigest,
-                                           Input subjectPublicKeyInfo)
-{
+Result AppTrustDomain::VerifyRSAPKCS1SignedData(Input data,
+                                                DigestAlgorithm digestAlgorithm,
+                                                Input signature,
+                                                Input subjectPublicKeyInfo) {
   // TODO: We should restrict signatures to SHA-256 or better.
-  return VerifyRSAPKCS1SignedDigestNSS(signedDigest, subjectPublicKeyInfo,
-                                       mPinArg);
+  return VerifyRSAPKCS1SignedDataNSS(data, digestAlgorithm, signature,
+                                     subjectPublicKeyInfo, mPinArg);
 }
 
 Result
 AppTrustDomain::CheckECDSACurveIsAcceptable(EndEntityOrCA /*endEntityOrCA*/,
                                             NamedCurve curve)
 {
   switch (curve) {
     case NamedCurve::secp256r1: // fall through
     case NamedCurve::secp384r1: // fall through
     case NamedCurve::secp521r1:
       return Success;
   }
 
   return Result::ERROR_UNSUPPORTED_ELLIPTIC_CURVE;
 }
 
-Result
-AppTrustDomain::VerifyECDSASignedDigest(const SignedDigest& signedDigest,
-                                        Input subjectPublicKeyInfo)
-{
-  return VerifyECDSASignedDigestNSS(signedDigest, subjectPublicKeyInfo,
-                                    mPinArg);
+Result AppTrustDomain::VerifyECDSASignedData(Input data,
+                                             DigestAlgorithm digestAlgorithm,
+                                             Input signature,
+                                             Input subjectPublicKeyInfo) {
+  return VerifyECDSASignedDataNSS(data, digestAlgorithm, signature,
+                                  subjectPublicKeyInfo, mPinArg);
 }
 
 Result
 AppTrustDomain::CheckValidityIsAcceptable(Time /*notBefore*/, Time /*notAfter*/,
                                           EndEntityOrCA /*endEntityOrCA*/,
                                           KeyPurposeId /*keyPurpose*/)
 {
   return Success;
diff --git a/security/apps/AppTrustDomain.h b/security/apps/AppTrustDomain.h
--- a/security/apps/AppTrustDomain.h
+++ b/security/apps/AppTrustDomain.h
@@ -44,25 +44,27 @@ public:
                               override;
   virtual Result CheckSignatureDigestAlgorithm(
                    mozilla::pkix::DigestAlgorithm digestAlg,
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    mozilla::pkix::Time notBefore) override;
   virtual Result CheckRSAPublicKeyModulusSizeInBits(
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    unsigned int modulusSizeInBits) override;
-  virtual Result VerifyRSAPKCS1SignedDigest(
-                   const mozilla::pkix::SignedDigest& signedDigest,
-                   mozilla::pkix::Input subjectPublicKeyInfo) override;
+  virtual Result VerifyRSAPKCS1SignedData(
+      mozilla::pkix::Input data, mozilla::pkix::DigestAlgorithm digestAlgorithm,
+      mozilla::pkix::Input signature,
+      mozilla::pkix::Input subjectPublicKeyInfo) override;
   virtual Result CheckECDSACurveIsAcceptable(
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    mozilla::pkix::NamedCurve curve) override;
-  virtual Result VerifyECDSASignedDigest(
-                   const mozilla::pkix::SignedDigest& signedDigest,
-                   mozilla::pkix::Input subjectPublicKeyInfo) override;
+  virtual Result VerifyECDSASignedData(
+      mozilla::pkix::Input data, mozilla::pkix::DigestAlgorithm digestAlgorithm,
+      mozilla::pkix::Input signature,
+      mozilla::pkix::Input subjectPublicKeyInfo) override;
   virtual Result CheckValidityIsAcceptable(
                    mozilla::pkix::Time notBefore, mozilla::pkix::Time notAfter,
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    mozilla::pkix::KeyPurposeId keyPurpose) override;
   virtual Result NetscapeStepUpMatchesServerAuth(
                    mozilla::pkix::Time notBefore,
                    /*out*/ bool& matches) override;
   virtual void NoteAuxiliaryExtension(
diff --git a/security/certverifier/NSSCertDBTrustDomain.cpp b/security/certverifier/NSSCertDBTrustDomain.cpp
--- a/security/certverifier/NSSCertDBTrustDomain.cpp
+++ b/security/certverifier/NSSCertDBTrustDomain.cpp
@@ -952,45 +952,42 @@ NSSCertDBTrustDomain::CheckRSAPublicKeyM
   EndEntityOrCA /*endEntityOrCA*/, unsigned int modulusSizeInBits)
 {
   if (modulusSizeInBits < mMinRSABits) {
     return Result::ERROR_INADEQUATE_KEY_SIZE;
   }
   return Success;
 }
 
-Result
-NSSCertDBTrustDomain::VerifyRSAPKCS1SignedDigest(
-  const SignedDigest& signedDigest,
-  Input subjectPublicKeyInfo)
-{
-  return VerifyRSAPKCS1SignedDigestNSS(signedDigest, subjectPublicKeyInfo,
-                                       mPinArg);
+Result NSSCertDBTrustDomain::VerifyRSAPKCS1SignedData(
+    Input data, DigestAlgorithm digestAlgorithm, Input signature,
+    Input subjectPublicKeyInfo) {
+  return VerifyRSAPKCS1SignedDataNSS(data, digestAlgorithm, signature,
+                                     subjectPublicKeyInfo, mPinArg);
 }
 
 Result
 NSSCertDBTrustDomain::CheckECDSACurveIsAcceptable(
   EndEntityOrCA /*endEntityOrCA*/, NamedCurve curve)
 {
   switch (curve) {
     case NamedCurve::secp256r1: // fall through
     case NamedCurve::secp384r1: // fall through
     case NamedCurve::secp521r1:
       return Success;
   }
 
   return Result::ERROR_UNSUPPORTED_ELLIPTIC_CURVE;
 }
 
-Result
-NSSCertDBTrustDomain::VerifyECDSASignedDigest(const SignedDigest& signedDigest,
-                                              Input subjectPublicKeyInfo)
-{
-  return VerifyECDSASignedDigestNSS(signedDigest, subjectPublicKeyInfo,
-                                    mPinArg);
+Result NSSCertDBTrustDomain::VerifyECDSASignedData(
+    Input data, DigestAlgorithm digestAlgorithm, Input signature,
+    Input subjectPublicKeyInfo) {
+  return VerifyECDSASignedDataNSS(data, digestAlgorithm, signature,
+                                  subjectPublicKeyInfo, mPinArg);
 }
 
 Result
 NSSCertDBTrustDomain::CheckValidityIsAcceptable(Time notBefore, Time notAfter,
                                                 EndEntityOrCA endEntityOrCA,
                                                 KeyPurposeId keyPurpose)
 {
   if (endEntityOrCA != EndEntityOrCA::MustBeEndEntity) {
diff --git a/security/certverifier/NSSCertDBTrustDomain.cpp.1756061.later b/security/certverifier/NSSCertDBTrustDomain.cpp.1756061.later
new file mode 100644
--- /dev/null
+++ b/security/certverifier/NSSCertDBTrustDomain.cpp.1756061.later
@@ -0,0 +1,30 @@
+--- NSSCertDBTrustDomain.cpp
++++ NSSCertDBTrustDomain.cpp
+@@ -156,25 +156,18 @@ static bool ShouldSkipSelfSignedNonTrust
+                                certDER, trust) != Success) {
+     return false;
+   }
+   // If the trust for this certificate is anything other than "inherit", we want
+   // to process it like normal.
+   if (trust != TrustLevel::InheritsTrust) {
+     return false;
+   }
+-  uint8_t digestBuf[MAX_DIGEST_SIZE_IN_BYTES];
+-  pkix::der::PublicKeyAlgorithm publicKeyAlg;
+-  SignedDigest signature;
+-  if (DigestSignedData(trustDomain, cert.GetSignedData(), digestBuf,
+-                       publicKeyAlg, signature) != Success) {
+-    return false;
+-  }
+-  if (VerifySignedDigest(trustDomain, publicKeyAlg, signature,
+-                         cert.GetSubjectPublicKeyInfo()) != Success) {
++  if (VerifySignedData(trustDomain, cert.GetSignedData(),
++                       cert.GetSubjectPublicKeyInfo()) != Success) {
+     return false;
+   }
+   // This is a self-signed, non-trust-anchor certificate, so we shouldn't use it
+   // for path building. See bug 1056341.
+   return true;
+ }
+ 
+ static Result CheckCandidates(TrustDomain& trustDomain,
diff --git a/security/certverifier/NSSCertDBTrustDomain.h b/security/certverifier/NSSCertDBTrustDomain.h
--- a/security/certverifier/NSSCertDBTrustDomain.h
+++ b/security/certverifier/NSSCertDBTrustDomain.h
@@ -107,27 +107,29 @@ public:
                    mozilla::pkix::DigestAlgorithm digestAlg,
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    mozilla::pkix::Time notBefore) override;
 
   virtual Result CheckRSAPublicKeyModulusSizeInBits(
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    unsigned int modulusSizeInBits) override;
 
-  virtual Result VerifyRSAPKCS1SignedDigest(
-                   const mozilla::pkix::SignedDigest& signedDigest,
-                   mozilla::pkix::Input subjectPublicKeyInfo) override;
+  virtual Result VerifyRSAPKCS1SignedData(
+      mozilla::pkix::Input data, mozilla::pkix::DigestAlgorithm digestAlgorithm,
+      mozilla::pkix::Input signature,
+      mozilla::pkix::Input subjectPublicKeyInfo) override;
 
   virtual Result CheckECDSACurveIsAcceptable(
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    mozilla::pkix::NamedCurve curve) override;
 
-  virtual Result VerifyECDSASignedDigest(
-                   const mozilla::pkix::SignedDigest& signedDigest,
-                   mozilla::pkix::Input subjectPublicKeyInfo) override;
+  virtual Result VerifyECDSASignedData(
+      mozilla::pkix::Input data, mozilla::pkix::DigestAlgorithm digestAlgorithm,
+      mozilla::pkix::Input signature,
+      mozilla::pkix::Input subjectPublicKeyInfo) override;
 
   virtual Result DigestBuf(mozilla::pkix::Input item,
                            mozilla::pkix::DigestAlgorithm digestAlg,
                            /*out*/ uint8_t* digestBuf,
                            size_t digestBufLen) override;
 
   virtual Result CheckValidityIsAcceptable(
                    mozilla::pkix::Time notBefore, mozilla::pkix::Time notAfter,
diff --git a/security/certverifier/OCSPVerificationTrustDomain.cpp b/security/certverifier/OCSPVerificationTrustDomain.cpp
--- a/security/certverifier/OCSPVerificationTrustDomain.cpp
+++ b/security/certverifier/OCSPVerificationTrustDomain.cpp
@@ -66,37 +66,35 @@ OCSPVerificationTrustDomain::CheckSignat
 Result
 OCSPVerificationTrustDomain::CheckRSAPublicKeyModulusSizeInBits(
   EndEntityOrCA aEEOrCA, unsigned int aModulusSizeInBits)
 {
   return mCertDBTrustDomain.
       CheckRSAPublicKeyModulusSizeInBits(aEEOrCA, aModulusSizeInBits);
 }
 
-Result
-OCSPVerificationTrustDomain::VerifyRSAPKCS1SignedDigest(
-  const SignedDigest& aSignedDigest, Input aSubjectPublicKeyInfo)
-{
-  return mCertDBTrustDomain.VerifyRSAPKCS1SignedDigest(aSignedDigest,
-                                                       aSubjectPublicKeyInfo);
+Result OCSPVerificationTrustDomain::VerifyRSAPKCS1SignedData(
+    Input data, DigestAlgorithm digestAlgorithm, Input signature,
+    Input subjectPublicKeyInfo) {
+  return mCertDBTrustDomain.VerifyRSAPKCS1SignedData(
+      data, digestAlgorithm, signature, subjectPublicKeyInfo);
 }
 
 Result
 OCSPVerificationTrustDomain::CheckECDSACurveIsAcceptable(
   EndEntityOrCA aEEOrCA, NamedCurve aCurve)
 {
   return mCertDBTrustDomain.CheckECDSACurveIsAcceptable(aEEOrCA, aCurve);
 }
 
-Result
-OCSPVerificationTrustDomain::VerifyECDSASignedDigest(
-  const SignedDigest& aSignedDigest, Input aSubjectPublicKeyInfo)
-{
-  return mCertDBTrustDomain.VerifyECDSASignedDigest(aSignedDigest,
-                                                    aSubjectPublicKeyInfo);
+Result OCSPVerificationTrustDomain::VerifyECDSASignedData(
+    Input data, DigestAlgorithm digestAlgorithm, Input signature,
+    Input subjectPublicKeyInfo) {
+  return mCertDBTrustDomain.VerifyECDSASignedData(
+      data, digestAlgorithm, signature, subjectPublicKeyInfo);
 }
 
 Result
 OCSPVerificationTrustDomain::CheckValidityIsAcceptable(
   Time notBefore, Time notAfter, EndEntityOrCA endEntityOrCA,
   KeyPurposeId keyPurpose)
 {
   return mCertDBTrustDomain.CheckValidityIsAcceptable(notBefore, notAfter,
diff --git a/security/certverifier/OCSPVerificationTrustDomain.h b/security/certverifier/OCSPVerificationTrustDomain.h
--- a/security/certverifier/OCSPVerificationTrustDomain.h
+++ b/security/certverifier/OCSPVerificationTrustDomain.h
@@ -33,27 +33,29 @@ public:
                    mozilla::pkix::DigestAlgorithm digestAlg,
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    mozilla::pkix::Time notBefore) override;
 
   virtual Result CheckRSAPublicKeyModulusSizeInBits(
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    unsigned int modulusSizeInBits) override;
 
-  virtual Result VerifyRSAPKCS1SignedDigest(
-                   const mozilla::pkix::SignedDigest& signedDigest,
-                   mozilla::pkix::Input subjectPublicKeyInfo) override;
+  virtual Result VerifyRSAPKCS1SignedData(
+      mozilla::pkix::Input data, mozilla::pkix::DigestAlgorithm digestAlgorithm,
+      mozilla::pkix::Input signature,
+      mozilla::pkix::Input subjectPublicKeyInfo) override;
 
   virtual Result CheckECDSACurveIsAcceptable(
                    mozilla::pkix::EndEntityOrCA endEntityOrCA,
                    mozilla::pkix::NamedCurve curve) override;
 
-  virtual Result VerifyECDSASignedDigest(
-                   const mozilla::pkix::SignedDigest& signedDigest,
-                   mozilla::pkix::Input subjectPublicKeyInfo) override;
+  virtual Result VerifyECDSASignedData(
+      mozilla::pkix::Input data, mozilla::pkix::DigestAlgorithm digestAlgorithm,
+      mozilla::pkix::Input signature,
+      mozilla::pkix::Input subjectPublicKeyInfo) override;
 
   virtual Result DigestBuf(mozilla::pkix::Input item,
                            mozilla::pkix::DigestAlgorithm digestAlg,
                    /*out*/ uint8_t* digestBuf,
                            size_t digestBufLen) override;
 
   virtual Result CheckValidityIsAcceptable(
                    mozilla::pkix::Time notBefore, mozilla::pkix::Time notAfter,
diff --git a/security/ct/BTVerifier.cpp.1756061.later b/security/ct/BTVerifier.cpp.1756061.later
new file mode 100644
--- /dev/null
+++ b/security/ct/BTVerifier.cpp.1756061.later
@@ -0,0 +1,70 @@
+--- BTVerifier.cpp
++++ BTVerifier.cpp
+@@ -95,64 +95,33 @@ Result DecodeAndVerifySignedTreeHead(
+   }
+ 
+   Input signedDataInput;
+   rv = reader.GetInput(signedDataMark, signedDataInput);
+   if (rv != Success) {
+     return rv;
+   }
+ 
+-  SECOidTag unusedDigestAlgorithmId;
+-  size_t digestAlgorithmLength;
+-  rv = GetDigestAlgorithmLengthAndIdentifier(
+-      digestAlgorithm, digestAlgorithmLength, unusedDigestAlgorithmId);
+-  if (rv != Success) {
+-    return rv;
+-  }
+-
+-  uint8_t digestBuf[MAX_DIGEST_SIZE_IN_BYTES];
+-  rv = DigestBufNSS(signedDataInput, digestAlgorithm, digestBuf,
+-                    digestAlgorithmLength);
+-  if (rv != Success) {
+-    return rv;
+-  }
+-
+-  Input digestInput;
+-  rv = digestInput.Init(digestBuf, digestAlgorithmLength);
+-  if (rv != Success) {
+-    return rv;
+-  }
+-
+   Input signatureInput;
+   rv = ReadVariableBytes<kSTHSignatureLengthBytes>(reader, signatureInput);
+   if (rv != Success) {
+     return rv;
+   }
+ 
+-  SignedDigest signedDigest = {digestInput, digestAlgorithm, signatureInput};
+   switch (publicKeyAlgorithm) {
+     case der::PublicKeyAlgorithm::ECDSA:
+-      rv = VerifyECDSASignedDigestNSS(signedDigest, signerSubjectPublicKeyInfo,
+-                                      nullptr);
++      rv = VerifyECDSASignedDataNSS(signedDataInput, digestAlgorithm,
++                                    signatureInput, signerSubjectPublicKeyInfo,
++                                    nullptr);
+       break;
+     case der::PublicKeyAlgorithm::RSA_PKCS1:
+-    case der::PublicKeyAlgorithm::Uninitialized:
+     default:
+       return Result::FATAL_ERROR_INVALID_ARGS;
+   }
+   if (rv != Success) {
+-    // VerifyECDSASignedDigestNSS eventually calls VFY_VerifyDigestDirect, which
+-    // can set the PR error code to SEC_ERROR_PKCS7_KEYALG_MISMATCH if the type
+-    // of key decoded from the SPKI does not match the given signature
+-    // algorithm. mozilla::pkix does not have a corresponding Result value and
+-    // turns this error code into Result::ERROR_UNKNOWN_ERROR. Since this is
+-    // uninformative, we'll turn that result into a bad signature error.
+-    if (rv == Result::ERROR_UNKNOWN_ERROR) {
+-      return Result::ERROR_BAD_SIGNATURE;
+-    }
+     return rv;
+   }
+ 
+   if (!reader.AtEnd()) {
+     return pkix::Result::ERROR_BAD_DER;
+   }
+ 
+   signedTreeHead = std::move(result);
diff --git a/security/ct/CTLogVerifier.cpp b/security/ct/CTLogVerifier.cpp
--- a/security/ct/CTLogVerifier.cpp
+++ b/security/ct/CTLogVerifier.cpp
@@ -71,18 +71,17 @@ public:
       DigitallySigned::SignatureAlgorithm::Anonymous);
     if (curve != NamedCurve::secp256r1) {
       return Result::ERROR_UNSUPPORTED_ELLIPTIC_CURVE;
     }
     mSignatureAlgorithm = DigitallySigned::SignatureAlgorithm::ECDSA;
     return Success;
   }
 
-  Result VerifyECDSASignedDigest(const SignedDigest&, Input) override
-  {
+  Result VerifyECDSASignedData(Input, DigestAlgorithm, Input, Input) override {
     return Result::FATAL_ERROR_LIBRARY_FAILURE;
   }
 
   Result CheckRSAPublicKeyModulusSizeInBits(EndEntityOrCA,
                                             unsigned int modulusSizeInBits)
                                             override
   {
     MOZ_ASSERT(mSignatureAlgorithm ==
@@ -90,18 +89,18 @@ public:
     // Require RSA keys of at least 2048 bits. See RFC 6962, Section 2.1.4.
     if (modulusSizeInBits < 2048) {
       return Result::ERROR_INADEQUATE_KEY_SIZE;
     }
     mSignatureAlgorithm = DigitallySigned::SignatureAlgorithm::RSA;
     return Success;
   }
 
-  Result VerifyRSAPKCS1SignedDigest(const SignedDigest&, Input) override
-  {
+  Result VerifyRSAPKCS1SignedData(Input, DigestAlgorithm, Input,
+                                  Input) override {
     return Result::FATAL_ERROR_LIBRARY_FAILURE;
   }
 
   Result CheckValidityIsAcceptable(Time, Time, EndEntityOrCA,
                                    KeyPurposeId) override
   {
     return Result::FATAL_ERROR_LIBRARY_FAILURE;
   }
@@ -256,81 +255,62 @@ CTLogVerifier::VerifySignedTreeHead(cons
 
 bool
 CTLogVerifier::SignatureParametersMatch(const DigitallySigned& signature)
 {
   return signature.SignatureParametersMatch(
     DigitallySigned::HashAlgorithm::SHA256, mSignatureAlgorithm);
 }
 
-static Result
-FasterVerifyECDSASignedDigestNSS(const SignedDigest& sd,
-                                 UniqueSECKEYPublicKey& pubkey)
-{
+static Result FasterVerifyECDSASignedDataNSS(Input data, Input signature,
+                                             UniqueSECKEYPublicKey& pubkey) {
   MOZ_ASSERT(pubkey);
   if (!pubkey) {
     return Result::FATAL_ERROR_LIBRARY_FAILURE;
   }
   // The signature is encoded as a DER SEQUENCE of two INTEGERs. PK11_Verify
   // expects the signature as only the two integers r and s (so no encoding -
   // just two series of bytes each half as long as SECKEY_SignatureLen(pubkey)).
   // DSAU_DecodeDerSigToLen converts from the former format to the latter.
-  SECItem derSignatureSECItem(UnsafeMapInputToSECItem(sd.signature));
+  SECItem derSignatureSECItem(UnsafeMapInputToSECItem(signature));
   size_t signatureLen = SECKEY_SignatureLen(pubkey.get());
   if (signatureLen == 0) {
     return MapPRErrorCodeToResult(PR_GetError());
   }
   UniqueSECItem signatureSECItem(DSAU_DecodeDerSigToLen(&derSignatureSECItem,
                                                         signatureLen));
   if (!signatureSECItem) {
     return MapPRErrorCodeToResult(PR_GetError());
   }
-  SECItem digestSECItem(UnsafeMapInputToSECItem(sd.digest));
-  SECStatus srv = PK11_Verify(pubkey.get(), signatureSECItem.get(),
-                              &digestSECItem, nullptr);
+  SECItem dataSECItem(UnsafeMapInputToSECItem(data));
+  SECStatus srv =
+      PK11_VerifyWithMechanism(pubkey.get(), CKM_ECDSA_SHA256, nullptr,
+                               signatureSECItem.get(), &dataSECItem, nullptr);
   if (srv != SECSuccess) {
     return MapPRErrorCodeToResult(PR_GetError());
   }
-
   return Success;
 }
 
 Result
 CTLogVerifier::VerifySignature(Input data, Input signature)
 {
-  uint8_t digest[SHA256_LENGTH];
-  Result rv = DigestBufNSS(data, DigestAlgorithm::sha256, digest,
-                           ArrayLength(digest));
-  if (rv != Success) {
-    return rv;
-  }
-
-  SignedDigest signedDigest;
-  signedDigest.digestAlgorithm = DigestAlgorithm::sha256;
-  rv = signedDigest.digest.Init(digest, ArrayLength(digest));
-  if (rv != Success) {
-    return rv;
-  }
-  rv = signedDigest.signature.Init(signature);
-  if (rv != Success) {
-    return rv;
-  }
-
   Input spki;
-  rv = BufferToInput(mSubjectPublicKeyInfo, spki);
+  Result rv = BufferToInput(mSubjectPublicKeyInfo, spki);
   if (rv != Success) {
     return rv;
   }
 
   switch (mSignatureAlgorithm) {
     case DigitallySigned::SignatureAlgorithm::RSA:
-      rv = VerifyRSAPKCS1SignedDigestNSS(signedDigest, spki, nullptr);
+      rv = VerifyRSAPKCS1SignedDataNSS(data, DigestAlgorithm::sha256, signature,
+                                       spki, nullptr);
       break;
     case DigitallySigned::SignatureAlgorithm::ECDSA:
-      rv = FasterVerifyECDSASignedDigestNSS(signedDigest, mPublicECKey);
+      rv = FasterVerifyECDSASignedDataNSS(data, signature, mPublicECKey);
       break;
     // We do not expect new values added to this enum any time soon,
     // so just listing all the available ones seems to be the easiest way
     // to suppress warning C4061 on MSVC (which expects all values of the
     // enum to be explicitly handled).
     case DigitallySigned::SignatureAlgorithm::Anonymous:
     case DigitallySigned::SignatureAlgorithm::DSA:
     default:
diff --git a/security/ct/tests/gtest/BTSignedTreeHeadTest.cpp.1756061.later b/security/ct/tests/gtest/BTSignedTreeHeadTest.cpp.1756061.later
new file mode 100644
--- /dev/null
+++ b/security/ct/tests/gtest/BTSignedTreeHeadTest.cpp.1756061.later
@@ -0,0 +1,54 @@
+--- BTSignedTreeHeadTest.cpp
++++ BTSignedTreeHeadTest.cpp
+@@ -120,26 +120,26 @@ static const BTSignedTreeHeadTestParams 
+     {ValidSecp521r1SHA512STH::kSPKIHex, pkix::DigestAlgorithm::sha512,
+      pkix::der::PublicKeyAlgorithm::ECDSA, ValidSecp521r1SHA512STH::kSTHHex,
+      Success, 1542136309473, 731393445, kValidRootHashSHA512Hex},
+     {ValidSecp521r1SHA512STH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, ValidSecp521r1SHA512STH::kSTHHex,
+      Result::ERROR_BAD_SIGNATURE, 0, 0, nullptr},
+     {ValidSTH::kSPKIHex, pkix::DigestAlgorithm::sha512,
+      pkix::der::PublicKeyAlgorithm::ECDSA, ValidSecp521r1SHA512STH::kSTHHex,
+-     Result::ERROR_BAD_DER, 0, 0, nullptr},
++     Result::ERROR_BAD_SIGNATURE, 0, 0, nullptr},
+     {SignatureCoversLogIDSTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, SignatureCoversLogIDSTH::kSTHHex,
+      Result::ERROR_BAD_SIGNATURE, 0, 0, nullptr},
+     {WrongSPKISTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, WrongSPKISTH::kSTHHex,
+      Result::ERROR_BAD_SIGNATURE, 0, 0, nullptr},
+     {WrongSigningKeySTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, WrongSigningKeySTH::kSTHHex,
+-     Result::ERROR_BAD_DER, 0, 0, nullptr},
++     Result::ERROR_BAD_SIGNATURE, 0, 0, nullptr},
+     {MissingLogIDSTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, MissingLogIDSTH::kSTHHex,
+      Result::ERROR_BAD_DER, 0, 0, nullptr},
+     {MissingTimestampSTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, MissingTimestampSTH::kSTHHex,
+      Result::ERROR_BAD_DER, 0, 0, nullptr},
+     {MissingTreeSizeSTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, MissingTreeSizeSTH::kSTHHex,
+@@ -165,20 +165,20 @@ static const BTSignedTreeHeadTestParams 
+     {TruncatedExtensionSTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, TruncatedExtensionSTH::kSTHHex,
+      Result::ERROR_BAD_DER, 0, 0, nullptr},
+     {RSASignerRSASPKISTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, RSASignerRSASPKISTH::kSTHHex,
+      Result::ERROR_BAD_SIGNATURE, 0, 0, nullptr},
+     {RSASignerECSPKISTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, RSASignerECSPKISTH::kSTHHex,
+-     Result::ERROR_BAD_DER, 0, 0, nullptr},
++     Result::ERROR_BAD_SIGNATURE, 0, 0, nullptr},
+     {ECSignerRSASPKISTH::kSPKIHex, pkix::DigestAlgorithm::sha256,
+      pkix::der::PublicKeyAlgorithm::ECDSA, ECSignerRSASPKISTH::kSTHHex,
+-     Result::ERROR_BAD_SIGNATURE, 0, 0, nullptr},
++     Result::ERROR_INVALID_KEY, 0, 0, nullptr},
+ };
+ 
+ TEST_P(BTSignedTreeHeadTest, BTSignedTreeHeadSimpleTest) {
+   const BTSignedTreeHeadTestParams& params(GetParam());
+ 
+   Buffer subjectPublicKeyInfoBuffer(
+       HexToBytes(params.mSubjectPublicKeyInfoHex));
+   Input subjectPublicKeyInfoInput = InputForBuffer(subjectPublicKeyInfoBuffer);
diff --git a/security/ct/tests/gtest/CTTestUtils.cpp b/security/ct/tests/gtest/CTTestUtils.cpp
--- a/security/ct/tests/gtest/CTTestUtils.cpp
+++ b/security/ct/tests/gtest/CTTestUtils.cpp
@@ -794,35 +794,37 @@ public:
   }
 
   pkix::Result CheckECDSACurveIsAcceptable(EndEntityOrCA, NamedCurve) override
   {
     ADD_FAILURE();
     return pkix::Result::FATAL_ERROR_LIBRARY_FAILURE;
   }
 
-  pkix::Result VerifyECDSASignedDigest(const SignedDigest& signedDigest,
-                                       Input subjectPublicKeyInfo) override
-  {
-    return VerifyECDSASignedDigestNSS(signedDigest, subjectPublicKeyInfo,
-                                      nullptr);
+  pkix::Result VerifyECDSASignedData(Input data,
+                                     DigestAlgorithm digestAlgorithm,
+                                     Input signature,
+                                     Input subjectPublicKeyInfo) override {
+    return VerifyECDSASignedDataNSS(data, digestAlgorithm, signature,
+                                    subjectPublicKeyInfo, nullptr);
   }
 
   pkix::Result CheckRSAPublicKeyModulusSizeInBits(EndEntityOrCA, unsigned int)
                                             override
   {
     ADD_FAILURE();
     return pkix::Result::FATAL_ERROR_LIBRARY_FAILURE;
   }
 
-  pkix::Result VerifyRSAPKCS1SignedDigest(const SignedDigest& signedDigest,
-                                          Input subjectPublicKeyInfo) override
-  {
-    return VerifyRSAPKCS1SignedDigestNSS(signedDigest, subjectPublicKeyInfo,
-                                         nullptr);
+  pkix::Result VerifyRSAPKCS1SignedData(Input data,
+                                        DigestAlgorithm digestAlgorithm,
+                                        Input signature,
+                                        Input subjectPublicKeyInfo) override {
+    return VerifyRSAPKCS1SignedDataNSS(data, digestAlgorithm, signature,
+                                       subjectPublicKeyInfo, nullptr);
   }
 
   pkix::Result CheckValidityIsAcceptable(Time, Time, EndEntityOrCA, KeyPurposeId)
                                    override
   {
     ADD_FAILURE();
     return pkix::Result::FATAL_ERROR_LIBRARY_FAILURE;
   }
diff --git a/security/manager/ssl/CSTrustDomain.cpp b/security/manager/ssl/CSTrustDomain.cpp
--- a/security/manager/ssl/CSTrustDomain.cpp
+++ b/security/manager/ssl/CSTrustDomain.cpp
@@ -165,44 +165,44 @@ CSTrustDomain::CheckRSAPublicKeyModulusS
   EndEntityOrCA endEntityOrCA, unsigned int modulusSizeInBits)
 {
   if (modulusSizeInBits < 2048) {
     return Result::ERROR_INADEQUATE_KEY_SIZE;
   }
   return Success;
 }
 
-Result
-CSTrustDomain::VerifyRSAPKCS1SignedDigest(const SignedDigest& signedDigest,
-                                          Input subjectPublicKeyInfo)
-{
-  return VerifyRSAPKCS1SignedDigestNSS(signedDigest, subjectPublicKeyInfo,
-                                       nullptr);
+Result CSTrustDomain::VerifyRSAPKCS1SignedData(Input data,
+                                               DigestAlgorithm digestAlgorithm,
+                                               Input signature,
+                                               Input subjectPublicKeyInfo) {
+  return VerifyRSAPKCS1SignedDataNSS(data, digestAlgorithm, signature,
+                                     subjectPublicKeyInfo, nullptr);
 }
 
 Result
 CSTrustDomain::CheckECDSACurveIsAcceptable(EndEntityOrCA endEntityOrCA,
                                            NamedCurve curve)
 {
   switch (curve) {
     case NamedCurve::secp256r1: // fall through
     case NamedCurve::secp384r1: // fall through
     case NamedCurve::secp521r1:
       return Success;
   }
 
   return Result::ERROR_UNSUPPORTED_ELLIPTIC_CURVE;
 }
 
-Result
-CSTrustDomain::VerifyECDSASignedDigest(const SignedDigest& signedDigest,
-                                       Input subjectPublicKeyInfo)
-{
-  return VerifyECDSASignedDigestNSS(signedDigest, subjectPublicKeyInfo,
-                                    nullptr);
+Result CSTrustDomain::VerifyECDSASignedData(Input data,
+                                            DigestAlgorithm digestAlgorithm,
+                                            Input signature,
+                                            Input subjectPublicKeyInfo) {
+  return VerifyECDSASignedDataNSS(data, digestAlgorithm, signature,
+                                  subjectPublicKeyInfo, nullptr);
 }
 
 Result
 CSTrustDomain::CheckValidityIsAcceptable(Time notBefore, Time notAfter,
                                          EndEntityOrCA endEntityOrCA,
                                          KeyPurposeId keyPurpose)
 {
   return Success;
diff --git a/security/manager/ssl/CSTrustDomain.h b/security/manager/ssl/CSTrustDomain.h
--- a/security/manager/ssl/CSTrustDomain.h
+++ b/security/manager/ssl/CSTrustDomain.h
@@ -45,25 +45,27 @@ public:
                               override;
   virtual Result CheckSignatureDigestAlgorithm(
     mozilla::pkix::DigestAlgorithm digestAlg,
     mozilla::pkix::EndEntityOrCA endEntityOrCA,
     mozilla::pkix::Time notBefore) override;
   virtual Result CheckRSAPublicKeyModulusSizeInBits(
     mozilla::pkix::EndEntityOrCA endEntityOrCA,
     unsigned int modulusSizeInBits) override;
-  virtual Result VerifyRSAPKCS1SignedDigest(
-    const mozilla::pkix::SignedDigest& signedDigest,
-    mozilla::pkix::Input subjectPublicKeyInfo) override;
+  virtual Result VerifyRSAPKCS1SignedData(
+      mozilla::pkix::Input data, mozilla::pkix::DigestAlgorithm digestAlgorithm,
+      mozilla::pkix::Input signature,
+      mozilla::pkix::Input subjectPublicKeyInfo) override;
   virtual Result CheckECDSACurveIsAcceptable(
     mozilla::pkix::EndEntityOrCA endEntityOrCA,
     mozilla::pkix::NamedCurve curve) override;
-  virtual Result VerifyECDSASignedDigest(
-    const mozilla::pkix::SignedDigest& signedDigest,
-    mozilla::pkix::Input subjectPublicKeyInfo) override;
+  virtual Result VerifyECDSASignedData(
+      mozilla::pkix::Input data, mozilla::pkix::DigestAlgorithm digestAlgorithm,
+      mozilla::pkix::Input signature,
+      mozilla::pkix::Input subjectPublicKeyInfo) override;
   virtual Result CheckValidityIsAcceptable(
     mozilla::pkix::Time notBefore, mozilla::pkix::Time notAfter,
     mozilla::pkix::EndEntityOrCA endEntityOrCA,
     mozilla::pkix::KeyPurposeId keyPurpose) override;
   virtual Result NetscapeStepUpMatchesServerAuth(
     mozilla::pkix::Time notBefore, /*out*/ bool& matches) override;
   virtual void NoteAuxiliaryExtension(
     mozilla::pkix::AuxiliaryExtension extension,
diff --git a/security/manager/ssl/nsNSSIOLayer.cpp.1756061.later b/security/manager/ssl/nsNSSIOLayer.cpp.1756061.later
new file mode 100644
--- /dev/null
+++ b/security/manager/ssl/nsNSSIOLayer.cpp.1756061.later
@@ -0,0 +1,35 @@
+--- nsNSSIOLayer.cpp
++++ nsNSSIOLayer.cpp
+@@ -2056,26 +2056,28 @@ class ClientAuthCertNonverifyingTrustDom
+       DigestAlgorithm digestAlg, EndEntityOrCA endEntityOrCA,
+       Time notBefore) override {
+     return Success;
+   }
+   virtual mozilla::pkix::Result CheckRSAPublicKeyModulusSizeInBits(
+       EndEntityOrCA endEntityOrCA, unsigned int modulusSizeInBits) override {
+     return Success;
+   }
+-  virtual mozilla::pkix::Result VerifyRSAPKCS1SignedDigest(
+-      const SignedDigest& signedDigest, Input subjectPublicKeyInfo) override {
++  virtual mozilla::pkix::Result VerifyRSAPKCS1SignedData(
++      Input data, DigestAlgorithm, Input signature,
++      Input subjectPublicKeyInfo) override {
+     return Success;
+   }
+   virtual mozilla::pkix::Result CheckECDSACurveIsAcceptable(
+       EndEntityOrCA endEntityOrCA, NamedCurve curve) override {
+     return Success;
+   }
+-  virtual mozilla::pkix::Result VerifyECDSASignedDigest(
+-      const SignedDigest& signedDigest, Input subjectPublicKeyInfo) override {
++  virtual mozilla::pkix::Result VerifyECDSASignedData(
++      Input data, DigestAlgorithm, Input signature,
++      Input subjectPublicKeyInfo) override {
+     return Success;
+   }
+   virtual mozilla::pkix::Result CheckValidityIsAcceptable(
+       Time notBefore, Time notAfter, EndEntityOrCA endEntityOrCA,
+       KeyPurposeId keyPurpose) override {
+     return Success;
+   }
+   virtual mozilla::pkix::Result NetscapeStepUpMatchesServerAuth(
