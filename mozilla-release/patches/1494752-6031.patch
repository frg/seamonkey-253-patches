# HG changeset patch
# User Jeff Walden <jwalden@mit.edu>
# Date 1541688097 18000
#      Thu Nov 08 09:41:37 2018 -0500
# Node ID c264774b89130cd7759c40977835e2a0d32fd684
# Parent  6e867e170aed9e292e23d77c72b1c238ad3a3be7
Bug 1494752. r=jonco, a=RyanVM

diff --git a/js/src/frontend/Parser.cpp b/js/src/frontend/Parser.cpp
--- a/js/src/frontend/Parser.cpp
+++ b/js/src/frontend/Parser.cpp
@@ -14,20 +14,25 @@
  * syntax trees, see Parser.h).  After tree construction, it rewrites trees to
  * fold constants and evaluate compile-time expressions.
  *
  * This parser attempts no error recovery.
  */
 
 #include "frontend/Parser.h"
 
+#include "mozilla/ArrayUtils.h"
+#include "mozilla/Casting.h"
+#include "mozilla/Move.h"
 #include "mozilla/Range.h"
 #include "mozilla/Sprintf.h"
 #include "mozilla/TypeTraits.h"
 
+#include <memory>
+
 #include "jsapi.h"
 #include "jsopcode.h"
 #include "jstypes.h"
 
 #include "builtin/ModuleObject.h"
 #include "builtin/SelfHostingDefines.h"
 #include "frontend/BytecodeCompiler.h"
 #include "frontend/FoldConstants.h"
@@ -44,20 +49,22 @@
 #include "frontend/ParseNode-inl.h"
 #include "vm/EnvironmentObject-inl.h"
 #include "vm/JSAtom-inl.h"
 #include "vm/JSScript-inl.h"
 
 using namespace js;
 using namespace js::gc;
 
+using mozilla::AssertedCast;
+using mozilla::Forward;
 using mozilla::Maybe;
 using mozilla::Nothing;
-using mozilla::PodCopy;
 using mozilla::PodZero;
+using mozilla::PointerRangeSize;
 using mozilla::Some;
 
 using JS::AutoGCRooter;
 
 namespace js {
 namespace frontend {
 
 using DeclaredNamePtr = ParseContext::Scope::DeclaredNamePtr;
@@ -1719,16 +1726,69 @@ NewEmptyBindingData(JSContext* cx, LifoA
     if (!bindings) {
         ReportOutOfMemory(cx);
         return nullptr;
     }
     PodZero(bindings);
     return bindings;
 }
 
+namespace detail {
+
+template<class Data>
+static MOZ_ALWAYS_INLINE BindingName*
+InitializeIndexedBindings(Data* data, BindingName* start, BindingName* cursor)
+{
+    return cursor;
+}
+
+template<class Data, typename UnsignedInteger, typename... Step>
+static MOZ_ALWAYS_INLINE BindingName*
+InitializeIndexedBindings(Data* data, BindingName* start, BindingName* cursor,
+                          UnsignedInteger Data::* field, const Vector<BindingName>& bindings,
+                          Step&&... step)
+{
+    data->*field = AssertedCast<UnsignedInteger>(PointerRangeSize(start, cursor));
+
+    BindingName* newCursor = std::uninitialized_copy(bindings.begin(), bindings.end(), cursor);
+
+    return InitializeIndexedBindings(data, start, newCursor, mozilla::Forward<Step>(step)...);
+}
+
+} // namespace detail
+
+// Initialize |data->trailingNames| bindings, then set |data->length| to the
+// count of bindings added (which must equal |count|).
+//
+// First, |firstBindings| are added to |data->trailingNames|.  Then any "steps"
+// present are performed first to last.  Each step is 1) a pointer to a member
+// of |data| to be set to the current number of bindings added, and 2) a vector
+// of |BindingName|s to then copy into |data->trailingNames|.  (Thus each
+// |data| member field indicates where the corresponding vector's names start.)
+template<class Data, typename... Step>
+static MOZ_ALWAYS_INLINE void
+InitializeBindingData(Data* data, uint32_t count,
+                      const Vector<BindingName>& firstBindings,
+                      Step&&... step)
+{
+    MOZ_ASSERT(data->length == 0, "data shouldn't be filled yet");
+
+    BindingName* start = &data->names[0];
+    BindingName* cursor =
+        std::uninitialized_copy(firstBindings.begin(), firstBindings.end(), start);
+
+#ifdef DEBUG
+    BindingName* end =
+#endif
+        detail::InitializeIndexedBindings(data, start, cursor, mozilla::Forward<Step>(step)...);
+
+    MOZ_ASSERT(PointerRangeSize(start, end) == count);
+    data->length = count;
+}
+
 Maybe<GlobalScope::Data*>
 NewGlobalScopeData(JSContext* context, ParseContext::Scope& scope, LifoAlloc& alloc, ParseContext* pc)
 {
 
     Vector<BindingName> funs(context);
     Vector<BindingName> vars(context);
     Vector<BindingName> lets(context);
     Vector<BindingName> consts(context);
@@ -1763,33 +1823,21 @@ NewGlobalScopeData(JSContext* context, P
     uint32_t numBindings = funs.length() + vars.length() + lets.length() + consts.length();
 
     if (numBindings > 0) {
         bindings = NewEmptyBindingData<GlobalScope>(context, alloc, numBindings);
         if (!bindings)
             return Nothing();
 
         // The ordering here is important. See comments in GlobalScope.
-        BindingName* start = bindings->names;
-        BindingName* cursor = start;
-
-        PodCopy(cursor, funs.begin(), funs.length());
-        cursor += funs.length();
-
-        bindings->varStart = cursor - start;
-        PodCopy(cursor, vars.begin(), vars.length());
-        cursor += vars.length();
-
-        bindings->letStart = cursor - start;
-        PodCopy(cursor, lets.begin(), lets.length());
-        cursor += lets.length();
-
-        bindings->constStart = cursor - start;
-        PodCopy(cursor, consts.begin(), consts.length());
-        bindings->length = numBindings;
+        InitializeBindingData(bindings, numBindings,
+                              funs,
+                              &GlobalScope::Data::varStart, vars,
+                              &GlobalScope::Data::letStart, lets,
+                              &GlobalScope::Data::constStart, consts);
     }
 
     return Some(bindings);
 }
 
 Maybe<GlobalScope::Data*>
 ParserBase::newGlobalScopeData(ParseContext::Scope& scope)
 {
@@ -1835,33 +1883,21 @@ NewModuleScopeData(JSContext* context, P
     uint32_t numBindings = imports.length() + vars.length() + lets.length() + consts.length();
 
     if (numBindings > 0) {
         bindings = NewEmptyBindingData<ModuleScope>(context, alloc, numBindings);
         if (!bindings)
             return Nothing();
 
         // The ordering here is important. See comments in ModuleScope.
-        BindingName* start = bindings->names;
-        BindingName* cursor = start;
-
-        PodCopy(cursor, imports.begin(), imports.length());
-        cursor += imports.length();
-
-        bindings->varStart = cursor - start;
-        PodCopy(cursor, vars.begin(), vars.length());
-        cursor += vars.length();
-
-        bindings->letStart = cursor - start;
-        PodCopy(cursor, lets.begin(), lets.length());
-        cursor += lets.length();
-
-        bindings->constStart = cursor - start;
-        PodCopy(cursor, consts.begin(), consts.length());
-        bindings->length = numBindings;
+        InitializeBindingData(bindings, numBindings,
+                              imports,
+                              &ModuleScope::Data::varStart, vars,
+                              &ModuleScope::Data::letStart, lets,
+                              &ModuleScope::Data::constStart, consts);
     }
 
     return Some(bindings);
 }
 
 Maybe<ModuleScope::Data*>
 ParserBase::newModuleScopeData(ParseContext::Scope& scope)
 {
@@ -1891,27 +1927,21 @@ NewEvalScopeData(JSContext* context, Par
     EvalScope::Data* bindings = nullptr;
     uint32_t numBindings = funs.length() + vars.length();
 
     if (numBindings > 0) {
         bindings = NewEmptyBindingData<EvalScope>(context, alloc, numBindings);
         if (!bindings)
             return Nothing();
 
-        BindingName* start = bindings->names;
-        BindingName* cursor = start;
-
         // Keep track of what vars are functions. This is only used in BCE to omit
         // superfluous DEFVARs.
-        PodCopy(cursor, funs.begin(), funs.length());
-        cursor += funs.length();
-
-        bindings->varStart = cursor - start;
-        PodCopy(cursor, vars.begin(), vars.length());
-        bindings->length = numBindings;
+        InitializeBindingData(bindings, numBindings,
+                              funs,
+                              &EvalScope::Data::varStart, vars);
     }
 
     return Some(bindings);
 }
 
 Maybe<EvalScope::Data*>
 ParserBase::newEvalScopeData(ParseContext::Scope& scope)
 {
@@ -1989,29 +2019,20 @@ NewFunctionScopeData(JSContext* context,
     uint32_t numBindings = positionalFormals.length() + formals.length() + vars.length();
 
     if (numBindings > 0) {
         bindings = NewEmptyBindingData<FunctionScope>(context, alloc, numBindings);
         if (!bindings)
             return Nothing();
 
         // The ordering here is important. See comments in FunctionScope.
-        BindingName* start = bindings->names;
-        BindingName* cursor = start;
-
-        PodCopy(cursor, positionalFormals.begin(), positionalFormals.length());
-        cursor += positionalFormals.length();
-
-        bindings->nonPositionalFormalStart = cursor - start;
-        PodCopy(cursor, formals.begin(), formals.length());
-        cursor += formals.length();
-
-        bindings->varStart = cursor - start;
-        PodCopy(cursor, vars.begin(), vars.length());
-        bindings->length = numBindings;
+        InitializeBindingData(bindings, numBindings,
+                              positionalFormals,
+                              &FunctionScope::Data::nonPositionalFormalStart, formals,
+                              &FunctionScope::Data::varStart, vars);
     }
 
     return Some(bindings);
 }
 
 Maybe<FunctionScope::Data*>
 ParserBase::newFunctionScopeData(ParseContext::Scope& scope, bool hasParameterExprs)
 {
@@ -2036,22 +2057,17 @@ NewVarScopeData(JSContext* context, Pars
     VarScope::Data* bindings = nullptr;
     uint32_t numBindings = vars.length();
 
     if (numBindings > 0) {
         bindings = NewEmptyBindingData<VarScope>(context, alloc, numBindings);
         if (!bindings)
             return Nothing();
 
-        // The ordering here is important. See comments in FunctionScope.
-        BindingName* start = bindings->names;
-        BindingName* cursor = start;
-
-        PodCopy(cursor, vars.begin(), vars.length());
-        bindings->length = numBindings;
+        InitializeBindingData(bindings, numBindings, vars);
     }
 
     return Some(bindings);
 }
 
 Maybe<VarScope::Data*>
 ParserBase::newVarScopeData(ParseContext::Scope& scope)
 {
@@ -2090,25 +2106,19 @@ NewLexicalScopeData(JSContext* context, 
     uint32_t numBindings = lets.length() + consts.length();
 
     if (numBindings > 0) {
         bindings = NewEmptyBindingData<LexicalScope>(context, alloc, numBindings);
         if (!bindings)
             return Nothing();
 
         // The ordering here is important. See comments in LexicalScope.
-        BindingName* cursor = bindings->names;
-        BindingName* start = cursor;
-
-        PodCopy(cursor, lets.begin(), lets.length());
-        cursor += lets.length();
-
-        bindings->constStart = cursor - start;
-        PodCopy(cursor, consts.begin(), consts.length());
-        bindings->length = numBindings;
+        InitializeBindingData(bindings, numBindings,
+                              lets,
+                              &LexicalScope::Data::constStart, consts);
     }
 
     return Some(bindings);
 }
 
 Maybe<LexicalScope::Data*>
 ParserBase::newLexicalScopeData(ParseContext::Scope& scope)
 {
diff --git a/js/src/vm/Scope.cpp b/js/src/vm/Scope.cpp
--- a/js/src/vm/Scope.cpp
+++ b/js/src/vm/Scope.cpp
@@ -213,17 +213,17 @@ NewEmptyScopeData(JSContext* cx, uint32_
         ReportOutOfMemory(cx);
     auto data = reinterpret_cast<typename ConcreteScope::Data*>(bytes);
     if (data)
         new (data) typename ConcreteScope::Data();
     return UniquePtr<typename ConcreteScope::Data>(data);
 }
 
 static bool
-XDRBindingName(XDRState<XDR_ENCODE>* xdr, BindingName* bindingName)
+XDRTrailingName(XDRState<XDR_ENCODE>* xdr, BindingName* bindingName, const uint32_t* length)
 {
     JSContext* cx = xdr->cx();
 
     RootedAtom atom(cx, bindingName->name());
     bool hasAtom = !!atom;
 
     uint8_t u8 = uint8_t(hasAtom << 1) | uint8_t(bindingName->closedOver());
     if (!xdr->codeUint8(&u8))
@@ -231,32 +231,33 @@ XDRBindingName(XDRState<XDR_ENCODE>* xdr
 
     if (atom && !XDRAtom(xdr, &atom))
         return false;
 
     return true;
 }
 
 static bool
-XDRBindingName(XDRState<XDR_DECODE>* xdr, BindingName* bindingName)
+XDRTrailingName(XDRState<XDR_DECODE>* xdr, void* bindingName, uint32_t* length)
 {
     JSContext* cx = xdr->cx();
 
     uint8_t u8;
     if (!xdr->codeUint8(&u8))
         return false;
 
     bool closedOver = u8 & 1;
     bool hasAtom = u8 >> 1;
 
     RootedAtom atom(cx);
     if (hasAtom && !XDRAtom(xdr, &atom))
         return false;
 
-    *bindingName = BindingName(atom, closedOver);
+    new (bindingName) BindingName(atom, closedOver);
+    ++*length;
 
     return true;
 }
 
 template <typename ConcreteScopeData>
 static void
 DeleteScopeData(ConcreteScopeData* data)
 {
@@ -281,29 +282,32 @@ Scope::XDRSizedBindingNames(XDRState<mod
         return false;
 
     if (mode == XDR_ENCODE) {
         data.set(&scope->data());
     } else {
         data.set(NewEmptyScopeData<ConcreteScope>(cx, length).release());
         if (!data)
             return false;
-        data->length = length;
     }
 
     for (uint32_t i = 0; i < length; i++) {
-        if (!XDRBindingName(xdr, &data->names[i])) {
+        if (mode == XDR_DECODE) {
+            MOZ_ASSERT(i == data->length, "must be decoding at the end");
+        }
+        if (!XDRTrailingName(xdr, &data->names[i], &data->length)) {
             if (mode == XDR_DECODE) {
                 DeleteScopeData(data.get());
                 data.set(nullptr);
             }
 
             return false;
         }
     }
+    MOZ_ASSERT(data->length == length);
 
     return true;
 }
 
 /* static */ Scope*
 Scope::create(JSContext* cx, ScopeKind kind, HandleScope enclosing, HandleShape envShape)
 {
     Scope* scope = Allocate<Scope>(cx);
@@ -1253,16 +1257,31 @@ GenerateWasmName(JSContext* cx, const ch
     if (!sb.append(prefix))
         return nullptr;
     if (!NumberValueToStringBuffer(cx, Int32Value(index), sb))
         return nullptr;
 
     return sb.finishAtom();
 }
 
+static void
+InitializeTrailingName(BindingName* trailingNames, size_t i, JSAtom* name)
+{
+    void* trailingName = &trailingNames[i];
+    new (trailingName) BindingName(name, false);
+}
+
+template<class Data>
+static void
+InitializeNextTrailingName(const Rooted<UniquePtr<Data>>& data, JSAtom* name)
+{
+    InitializeTrailingName(data->names, data->length, name);
+    data->length++;
+}
+
 /* static */ WasmInstanceScope*
 WasmInstanceScope::create(JSContext* cx, WasmInstanceObject* instance)
 {
     // WasmInstanceScope::Data has GCManagedDeletePolicy because it contains a
     // GCPtr. Destruction of |data| below may trigger calls into the GC.
     Rooted<WasmInstanceScope*> wasmInstanceScope(cx);
 
     {
@@ -1273,37 +1292,35 @@ WasmInstanceScope::create(JSContext* cx,
         size_t globalsStart = namesCount;
         size_t globalsCount = instance->instance().metadata().globals.length();
         namesCount += globalsCount;
 
         Rooted<UniquePtr<Data>> data(cx, NewEmptyScopeData<WasmInstanceScope>(cx, namesCount));
         if (!data)
             return nullptr;
 
-        size_t nameIndex = 0;
         if (instance->instance().memory()) {
-            RootedAtom name(cx, GenerateWasmName(cx, "memory", /* index = */ 0));
-            if (!name)
+            JSAtom* wasmName = GenerateWasmName(cx, "memory", /* index = */ 0);
+            if (!wasmName)
                 return nullptr;
-            data->names[nameIndex] = BindingName(name, false);
-            nameIndex++;
+
+            InitializeNextTrailingName(data, wasmName);
         }
         for (size_t i = 0; i < globalsCount; i++) {
-            RootedAtom name(cx, GenerateWasmName(cx, "global", i));
-            if (!name)
+            JSAtom* wasmName = GenerateWasmName(cx, "global", i);
+            if (!wasmName)
                 return nullptr;
-            data->names[nameIndex] = BindingName(name, false);
-            nameIndex++;
+
+            InitializeNextTrailingName(data, wasmName);
         }
-        MOZ_ASSERT(nameIndex == namesCount);
+        MOZ_ASSERT(data->length == namesCount);
 
         data->instance.init(instance);
         data->memoriesStart = 0;
         data->globalsStart = globalsStart;
-        data->length = namesCount;
 
         Rooted<Scope*> enclosingScope(cx, &cx->global()->emptyGlobalScope());
 
         Scope* scope = Scope::create(cx, ScopeKind::WasmInstance, enclosingScope, /* envShape = */ nullptr);
         if (!scope)
             return nullptr;
 
         wasmInstanceScope = &scope->as<WasmInstanceScope>();
@@ -1340,24 +1357,26 @@ WasmFunctionScope::create(JSContext* cx,
     if (!instance->instance().debug().debugGetLocalTypes(funcIndex, &locals, &argsLength))
         return nullptr;
     uint32_t namesCount = locals.length();
 
     Rooted<UniquePtr<Data>> data(cx, NewEmptyScopeData<WasmFunctionScope>(cx, namesCount));
     if (!data)
         return nullptr;
 
+    for (size_t i = 0; i < namesCount; i++) {
+        JSAtom* wasmName = GenerateWasmName(cx, "var", i);
+        if (!wasmName)
+            return nullptr;
+
+        InitializeNextTrailingName(data, wasmName);
+    }
+    MOZ_ASSERT(data->length == namesCount);
+
     data->funcIndex = funcIndex;
-    data->length = namesCount;
-    for (size_t i = 0; i < namesCount; i++) {
-        RootedAtom name(cx, GenerateWasmName(cx, "var", i));
-        if (!name)
-            return nullptr;
-        data->names[i] = BindingName(name, false);
-    }
 
     Scope* scope = Scope::create(cx, ScopeKind::WasmFunction, enclosing, /* envShape = */ nullptr);
     if (!scope)
         return nullptr;
 
     wasmFunctionScope = &scope->as<WasmFunctionScope>();
     wasmFunctionScope->initData(std::move(data.get()));
 
