# HG changeset patch
# User L. David Baron <dbaron@dbaron.org>
# Date 1521851497 25200
# Node ID 5b91410deb5b02599f99e67a615b613bf7b9971b
# Parent  7c20b3024c6e074ec1c9fb01e382b368d314ce9d
Bug 1448138 - Rename string DataFlags::SHARED to REFCOUNTED to make it clearer to those reading the code.  r=erahm

MozReview-Commit-ID: 1mJuwY5dQkj

diff --git a/dom/bindings/FakeString.h b/dom/bindings/FakeString.h
--- a/dom/bindings/FakeString.h
+++ b/dom/bindings/FakeString.h
@@ -20,17 +20,17 @@ namespace binding_detail {
 struct FakeString {
   FakeString() :
     mDataFlags(nsString::DataFlags::TERMINATED),
     mClassFlags(nsString::ClassFlags(0))
   {
   }
 
   ~FakeString() {
-    if (mDataFlags & nsString::DataFlags::SHARED) {
+    if (mDataFlags & nsString::DataFlags::REFCOUNTED) {
       nsStringBuffer::FromData(mData)->Release();
     }
   }
 
   void Rebind(const nsString::char_type* aData, nsString::size_type aLength) {
     MOZ_ASSERT(mDataFlags == nsString::DataFlags::TERMINATED);
     mData = const_cast<nsString::char_type*>(aData);
     mLength = aLength;
@@ -124,17 +124,17 @@ private:
   void operator=(const FakeString& other) = delete;
 
   void SetData(nsString::char_type* aData) {
     MOZ_ASSERT(mDataFlags == nsString::DataFlags::TERMINATED);
     mData = const_cast<nsString::char_type*>(aData);
   }
   void AssignFromStringBuffer(already_AddRefed<nsStringBuffer> aBuffer) {
     SetData(static_cast<nsString::char_type*>(aBuffer.take()->Data()));
-    mDataFlags = nsString::DataFlags::SHARED | nsString::DataFlags::TERMINATED;
+    mDataFlags = nsString::DataFlags::REFCOUNTED | nsString::DataFlags::TERMINATED;
   }
 
   friend class NonNull<nsAString>;
 
   // A class to use for our static asserts to ensure our object layout
   // matches that of nsString.
   class StringAsserter;
   friend class StringAsserter;
diff --git a/xpcom/rust/gtest/nsstring/Test.cpp b/xpcom/rust/gtest/nsstring/Test.cpp
--- a/xpcom/rust/gtest/nsstring/Test.cpp
+++ b/xpcom/rust/gtest/nsstring/Test.cpp
@@ -47,35 +47,35 @@ MEMBER_CHECK(nsString, mDataFlags)
 MEMBER_CHECK(nsString, mClassFlags)
 MEMBER_CHECK(nsCString, mData)
 MEMBER_CHECK(nsCString, mLength)
 MEMBER_CHECK(nsCString, mDataFlags)
 MEMBER_CHECK(nsCString, mClassFlags)
 
 extern "C" void Rust_Test_NsStringFlags(uint16_t* f_terminated,
                                         uint16_t* f_voided,
-                                        uint16_t* f_shared,
+                                        uint16_t* f_refcounted,
                                         uint16_t* f_owned,
                                         uint16_t* f_inline,
                                         uint16_t* f_literal,
                                         uint16_t* f_class_inline,
                                         uint16_t* f_class_null_terminated);
 TEST(RustNsString, NsStringFlags) {
-  uint16_t f_terminated, f_voided, f_shared, f_owned, f_inline, f_literal,
+  uint16_t f_terminated, f_voided, f_refcounted, f_owned, f_inline, f_literal,
            f_class_inline, f_class_null_terminated;
   Rust_Test_NsStringFlags(&f_terminated,
-                          &f_voided, &f_shared,
+                          &f_voided, &f_refcounted,
                           &f_owned, &f_inline,
                           &f_literal, &f_class_inline, &f_class_null_terminated);
   EXPECT_EQ(f_terminated, uint16_t(nsAString::DataFlags::TERMINATED));
   EXPECT_EQ(f_terminated, uint16_t(nsACString::DataFlags::TERMINATED));
   EXPECT_EQ(f_voided, uint16_t(nsAString::DataFlags::VOIDED));
   EXPECT_EQ(f_voided, uint16_t(nsACString::DataFlags::VOIDED));
-  EXPECT_EQ(f_shared, uint16_t(nsAString::DataFlags::SHARED));
-  EXPECT_EQ(f_shared, uint16_t(nsACString::DataFlags::SHARED));
+  EXPECT_EQ(f_refcounted, uint16_t(nsAString::DataFlags::REFCOUNTED));
+  EXPECT_EQ(f_refcounted, uint16_t(nsACString::DataFlags::REFCOUNTED));
   EXPECT_EQ(f_owned, uint16_t(nsAString::DataFlags::OWNED));
   EXPECT_EQ(f_owned, uint16_t(nsACString::DataFlags::OWNED));
   EXPECT_EQ(f_inline, uint16_t(nsAString::DataFlags::INLINE));
   EXPECT_EQ(f_inline, uint16_t(nsACString::DataFlags::INLINE));
   EXPECT_EQ(f_literal, uint16_t(nsAString::DataFlags::LITERAL));
   EXPECT_EQ(f_literal, uint16_t(nsACString::DataFlags::LITERAL));
   EXPECT_EQ(f_class_inline, uint16_t(nsAString::ClassFlags::INLINE));
   EXPECT_EQ(f_class_inline, uint16_t(nsACString::ClassFlags::INLINE));
diff --git a/xpcom/string/nsStringFlags.h b/xpcom/string/nsStringFlags.h
--- a/xpcom/string/nsStringFlags.h
+++ b/xpcom/string/nsStringFlags.h
@@ -22,44 +22,44 @@ enum class StringDataFlags : uint16_t
 {
   // Some terminology:
   //
   //   "dependent buffer"    A dependent buffer is one that the string class
   //                         does not own.  The string class relies on some
   //                         external code to ensure the lifetime of the
   //                         dependent buffer.
   //
-  //   "shared buffer"       A shared buffer is one that the string class
-  //                         allocates.  When it allocates a shared string
+  //   "refcounted buffer"   A refcounted buffer is one that the string class
+  //                         allocates.  When it allocates a refcounted string
   //                         buffer, it allocates some additional space at
   //                         the beginning of the buffer for additional
   //                         fields, including a reference count and a
   //                         buffer length.  See nsStringHeader.
   //
   //   "adopted buffer"      An adopted buffer is a raw string buffer
   //                         allocated on the heap (using moz_xmalloc)
   //                         of which the string class subsumes ownership.
   //
   // Some comments about the string data flags:
   //
-  //   SHARED, OWNED, and INLINE are all mutually exlusive.  They
+  //   REFCOUNTED, OWNED, and INLINE are all mutually exlusive.  They
   //   indicate the allocation type of mData.  If none of these flags
   //   are set, then the string buffer is dependent.
   //
-  //   SHARED, OWNED, or INLINE imply TERMINATED.  This is because
+  //   REFCOUNTED, OWNED, or INLINE imply TERMINATED.  This is because
   //   the string classes always allocate null-terminated buffers, and
   //   non-terminated substrings are always dependent.
   //
   //   VOIDED implies TERMINATED, and moreover it implies that mData
   //   points to char_traits::sEmptyBuffer.  Therefore, VOIDED is
-  //   mutually exclusive with SHARED, OWNED, and INLINE.
+  //   mutually exclusive with REFCOUNTED, OWNED, and INLINE.
 
   TERMINATED   = 1 << 0,  // IsTerminated returns true
   VOIDED       = 1 << 1,  // IsVoid returns true
-  SHARED       = 1 << 2,  // mData points to a heap-allocated, shared buffer
+  REFCOUNTED   = 1 << 2,  // mData points to a heap-allocated, shareable, refcounted buffer
   OWNED        = 1 << 3,  // mData points to a heap-allocated, raw buffer
   INLINE       = 1 << 4,  // mData points to a writable, inline buffer
   LITERAL      = 1 << 5   // mData points to a string literal; DataFlags::TERMINATED will also be set
 };
 
 // bits for mClassFlags
 enum class StringClassFlags : uint16_t
 {
diff --git a/xpcom/string/nsSubstring.cpp b/xpcom/string/nsSubstring.cpp
--- a/xpcom/string/nsSubstring.cpp
+++ b/xpcom/string/nsSubstring.cpp
@@ -104,17 +104,17 @@ static nsStringStats gStringStats;
 #define STRING_STAT_INCREMENT(_s)
 #endif
 
 // ---------------------------------------------------------------------------
 
 void
 ReleaseData(void* aData, nsAString::DataFlags aFlags)
 {
-  if (aFlags & nsAString::DataFlags::SHARED) {
+  if (aFlags & nsAString::DataFlags::REFCOUNTED) {
     nsStringBuffer::FromData(aData)->Release();
   } else if (aFlags & nsAString::DataFlags::OWNED) {
     free(aData);
     STRING_STAT_INCREMENT(AdoptFree);
     // Treat this as destruction of a "StringAdopt" object for leak
     // tracking purposes.
     MOZ_LOG_DTOR(aData, "StringAdopt", 1);
   }
@@ -274,30 +274,30 @@ nsStringBuffer::Realloc(nsStringBuffer* 
 }
 
 nsStringBuffer*
 nsStringBuffer::FromString(const nsAString& aStr)
 {
   const nsAStringAccessor* accessor =
     static_cast<const nsAStringAccessor*>(&aStr);
 
-  if (!(accessor->flags() & nsAString::DataFlags::SHARED)) {
+  if (!(accessor->flags() & nsAString::DataFlags::REFCOUNTED)) {
     return nullptr;
   }
 
   return FromData(accessor->data());
 }
 
 nsStringBuffer*
 nsStringBuffer::FromString(const nsACString& aStr)
 {
   const nsACStringAccessor* accessor =
     static_cast<const nsACStringAccessor*>(&aStr);
 
-  if (!(accessor->flags() & nsACString::DataFlags::SHARED)) {
+  if (!(accessor->flags() & nsACString::DataFlags::REFCOUNTED)) {
     return nullptr;
   }
 
   return FromData(accessor->data());
 }
 
 void
 nsStringBuffer::ToString(uint32_t aLen, nsAString& aStr,
@@ -305,17 +305,17 @@ nsStringBuffer::ToString(uint32_t aLen, 
 {
   char16_t* data = static_cast<char16_t*>(Data());
 
   nsAStringAccessor* accessor = static_cast<nsAStringAccessor*>(&aStr);
   MOZ_DIAGNOSTIC_ASSERT(data[aLen] == char16_t(0),
                         "data should be null terminated");
 
   nsAString::DataFlags flags =
-    nsAString::DataFlags::SHARED | nsAString::DataFlags::TERMINATED;
+    nsAString::DataFlags::REFCOUNTED | nsAString::DataFlags::TERMINATED;
 
   if (!aMoveOwnership) {
     AddRef();
   }
   accessor->set(data, aLen, flags);
 }
 
 void
@@ -324,17 +324,17 @@ nsStringBuffer::ToString(uint32_t aLen, 
 {
   char* data = static_cast<char*>(Data());
 
   nsACStringAccessor* accessor = static_cast<nsACStringAccessor*>(&aStr);
   MOZ_DIAGNOSTIC_ASSERT(data[aLen] == char(0),
                         "data should be null terminated");
 
   nsACString::DataFlags flags =
-    nsACString::DataFlags::SHARED | nsACString::DataFlags::TERMINATED;
+    nsACString::DataFlags::REFCOUNTED | nsACString::DataFlags::TERMINATED;
 
   if (!aMoveOwnership) {
     AddRef();
   }
   accessor->set(data, aLen, flags);
 }
 
 size_t
diff --git a/xpcom/string/nsTSubstring.cpp b/xpcom/string/nsTSubstring.cpp
--- a/xpcom/string/nsTSubstring.cpp
+++ b/xpcom/string/nsTSubstring.cpp
@@ -116,29 +116,30 @@ nsTSubstring<T>::MutatePrep(size_type aC
     MOZ_ASSERT(XPCOM_MIN(temp, kMaxCapacity) >= aCapacity,
                "should have hit the early return at the top");
     aCapacity = XPCOM_MIN(temp, kMaxCapacity);
   }
 
   //
   // several cases:
   //
-  //  (1) we have a shared buffer (this->mDataFlags & DataFlags::SHARED)
+  //  (1) we have a refcounted shareable buffer (this->mDataFlags &
+  //      DataFlags::REFCOUNTED)
   //  (2) we have an owned buffer (this->mDataFlags & DataFlags::OWNED)
   //  (3) we have an inline buffer (this->mDataFlags & DataFlags::INLINE)
   //  (4) we have a readonly buffer
   //
   // requiring that we in some cases preserve the data before creating
   // a new buffer complicates things just a bit ;-)
   //
 
   size_type storageSize = (aCapacity + 1) * sizeof(char_type);
 
   // case #1
-  if (this->mDataFlags & DataFlags::SHARED) {
+  if (this->mDataFlags & DataFlags::REFCOUNTED) {
     nsStringBuffer* hdr = nsStringBuffer::FromData(this->mData);
     if (!hdr->IsReadonly()) {
       nsStringBuffer* newHdr = nsStringBuffer::Realloc(hdr, storageSize);
       if (!newHdr) {
         return false;  // out-of-memory (original header left intact)
       }
 
       hdr = newHdr;
@@ -164,17 +165,17 @@ nsTSubstring<T>::MutatePrep(size_type aC
 
     nsStringBuffer* newHdr =
       nsStringBuffer::Alloc(storageSize).take();
     if (!newHdr) {
       return false;  // we are still in a consistent state
     }
 
     newData = (char_type*)newHdr->Data();
-    newDataFlags = DataFlags::TERMINATED | DataFlags::SHARED;
+    newDataFlags = DataFlags::TERMINATED | DataFlags::REFCOUNTED;
   }
 
   // save old data and flags
   *aOldData = this->mData;
   *aOldDataFlags = this->mDataFlags;
 
   // this->mLength does not change
   SetData(newData, this->mLength, newDataFlags);
@@ -271,17 +272,17 @@ nsTSubstring<T>::ReplacePrepInternal(ind
 
 template <typename T>
 typename nsTSubstring<T>::size_type
 nsTSubstring<T>::Capacity() const
 {
   // return 0 to indicate an immutable or 0-sized buffer
 
   size_type capacity;
-  if (this->mDataFlags & DataFlags::SHARED) {
+  if (this->mDataFlags & DataFlags::REFCOUNTED) {
     // if the string is readonly, then we pretend that it has no capacity.
     nsStringBuffer* hdr = nsStringBuffer::FromData(this->mData);
     if (hdr->IsReadonly()) {
       capacity = 0;
     } else {
       capacity = (hdr->StorageSize() / sizeof(char_type)) - 1;
     }
   } else if (this->mDataFlags & DataFlags::INLINE) {
@@ -302,17 +303,17 @@ nsTSubstring<T>::Capacity() const
 template <typename T>
 bool
 nsTSubstring<T>::EnsureMutable(size_type aNewLen)
 {
   if (aNewLen == size_type(-1) || aNewLen == this->mLength) {
     if (this->mDataFlags & (DataFlags::INLINE | DataFlags::OWNED)) {
       return true;
     }
-    if ((this->mDataFlags & DataFlags::SHARED) &&
+    if ((this->mDataFlags & DataFlags::REFCOUNTED) &&
         !nsStringBuffer::FromData(this->mData)->IsReadonly()) {
       return true;
     }
 
     aNewLen = this->mLength;
   }
   return SetLength(aNewLen, mozilla::fallible);
 }
@@ -455,26 +456,26 @@ nsTSubstring<T>::Assign(const self_type&
   }
 
   if (!aStr.mLength) {
     Truncate();
     this->mDataFlags |= aStr.mDataFlags & DataFlags::VOIDED;
     return true;
   }
 
-  if (aStr.mDataFlags & DataFlags::SHARED) {
+  if (aStr.mDataFlags & DataFlags::REFCOUNTED) {
     // nice! we can avoid a string copy :-)
 
     // |aStr| should be null-terminated
     NS_ASSERTION(aStr.mDataFlags & DataFlags::TERMINATED, "shared, but not terminated");
 
     ::ReleaseData(this->mData, this->mDataFlags);
 
     SetData(aStr.mData, aStr.mLength,
-            DataFlags::TERMINATED | DataFlags::SHARED);
+            DataFlags::TERMINATED | DataFlags::REFCOUNTED);
 
     // get an owning reference to the this->mData
     nsStringBuffer::FromData(this->mData)->AddRef();
     return true;
   } else if (aStr.mDataFlags & DataFlags::LITERAL) {
     MOZ_ASSERT(aStr.mDataFlags & DataFlags::TERMINATED, "Unterminated literal");
 
     AssignLiteral(aStr.mData, aStr.mLength);
@@ -502,18 +503,18 @@ nsTSubstring<T>::Assign(self_type&& aStr
   // and in the fallback perform a copy-assignment followed by a truncation of
   // the original string.
 
   if (&aStr == this) {
     NS_WARNING("Move assigning a string to itself?");
     return true;
   }
 
-  if (aStr.mDataFlags & (DataFlags::SHARED | DataFlags::OWNED)) {
-    // If they have a SHARED or OWNED buffer, we can avoid a copy - so steal
+  if (aStr.mDataFlags & (DataFlags::REFCOUNTED | DataFlags::OWNED)) {
+    // If they have a REFCOUNTED or OWNED buffer, we can avoid a copy - so steal
     // their buffer and reset them to the empty string.
 
     // |aStr| should be null-terminated
     NS_ASSERTION(aStr.mDataFlags & DataFlags::TERMINATED,
                  "shared or owned, but not terminated");
 
     ::ReleaseData(this->mData, this->mDataFlags);
 
@@ -1217,43 +1218,43 @@ nsTSubstring<T>::AppendFloat(double aFlo
   AppendASCII(buf, length);
 }
 
 template <typename T>
 size_t
 nsTSubstring<T>::SizeOfExcludingThisIfUnshared(
     mozilla::MallocSizeOf aMallocSizeOf) const
 {
-  if (this->mDataFlags & DataFlags::SHARED) {
+  if (this->mDataFlags & DataFlags::REFCOUNTED) {
     return nsStringBuffer::FromData(this->mData)->
       SizeOfIncludingThisIfUnshared(aMallocSizeOf);
   }
   if (this->mDataFlags & DataFlags::OWNED) {
     return aMallocSizeOf(this->mData);
   }
 
   // If we reach here, exactly one of the following must be true:
   // - DataFlags::VOIDED is set, and this->mData points to sEmptyBuffer;
   // - DataFlags::INLINE is set, and this->mData points to a buffer within a
   //   string object (e.g. nsAutoString);
-  // - None of DataFlags::SHARED, DataFlags::OWNED, DataFlags::INLINE is set,
+  // - None of DataFlags::REFCOUNTED, DataFlags::OWNED, DataFlags::INLINE is set,
   //   and this->mData points to a buffer owned by something else.
   //
   // In all three cases, we don't measure it.
   return 0;
 }
 
 template <typename T>
 size_t
 nsTSubstring<T>::SizeOfExcludingThisEvenIfShared(
     mozilla::MallocSizeOf aMallocSizeOf) const
 {
   // This is identical to SizeOfExcludingThisIfUnshared except for the
-  // DataFlags::SHARED case.
-  if (this->mDataFlags & DataFlags::SHARED) {
+  // DataFlags::REFCOUNTED case.
+  if (this->mDataFlags & DataFlags::REFCOUNTED) {
     return nsStringBuffer::FromData(this->mData)->
       SizeOfIncludingThisEvenIfShared(aMallocSizeOf);
   }
   if (this->mDataFlags & DataFlags::OWNED) {
     return aMallocSizeOf(this->mData);
   }
   return 0;
 }
diff --git a/xpcom/string/nsTSubstring.h b/xpcom/string/nsTSubstring.h
--- a/xpcom/string/nsTSubstring.h
+++ b/xpcom/string/nsTSubstring.h
@@ -753,17 +753,17 @@ public:
   void StripCRLF();
 
   /**
    * If the string uses a shared buffer, this method
    * clears the pointer without releasing the buffer.
    */
   void ForgetSharedBuffer()
   {
-    if (base_string_type::mDataFlags & DataFlags::SHARED) {
+    if (base_string_type::mDataFlags & DataFlags::REFCOUNTED) {
       SetToEmptyBuffer();
     }
   }
 
 protected:
   void AssertValid()
   {
     MOZ_ASSERT(!(this->mClassFlags & ClassFlags::NULL_TERMINATED) ||
diff --git a/xpcom/tests/gtest/TestMoveString.cpp b/xpcom/tests/gtest/TestMoveString.cpp
--- a/xpcom/tests/gtest/TestMoveString.cpp
+++ b/xpcom/tests/gtest/TestMoveString.cpp
@@ -47,24 +47,24 @@ void ExpectNew(const nsACString& aStr)
 
 TEST(MoveString, SharedIntoOwned) {
   nsCString out;
   SetAsOwned(out, OLD_VAL);
   EXPECT_EQ(out.GetDataFlags(), Df::OWNED | Df::TERMINATED);
 
   nsCString in;
   in.Assign(NEW_VAL);
-  EXPECT_EQ(in.GetDataFlags(), Df::SHARED | Df::TERMINATED);
+  EXPECT_EQ(in.GetDataFlags(), Df::REFCOUNTED | Df::TERMINATED);
   const char* data = in.get();
 
   out.Assign(mozilla::Move(in));
   ExpectTruncated(in);
   ExpectNew(out);
 
-  EXPECT_EQ(out.GetDataFlags(), Df::SHARED | Df::TERMINATED);
+  EXPECT_EQ(out.GetDataFlags(), Df::REFCOUNTED | Df::TERMINATED);
   EXPECT_EQ(out.get(), data);
 }
 
 TEST(MoveString, OwnedIntoOwned) {
   nsCString out;
   SetAsOwned(out, OLD_VAL);
   EXPECT_EQ(out.GetDataFlags(), Df::OWNED | Df::TERMINATED);
 
@@ -108,33 +108,33 @@ TEST(MoveString, AutoIntoOwned) {
   in.Assign(NEW_VAL);
   EXPECT_EQ(in.GetDataFlags(), Df::INLINE | Df::TERMINATED);
   const char* data = in.get();
 
   out.Assign(mozilla::Move(in));
   ExpectTruncated(in);
   ExpectNew(out);
 
-  EXPECT_EQ(out.GetDataFlags(), Df::SHARED | Df::TERMINATED);
+  EXPECT_EQ(out.GetDataFlags(), Df::REFCOUNTED | Df::TERMINATED);
   EXPECT_NE(out.get(), data);
 }
 
 TEST(MoveString, DepIntoOwned) {
   nsCString out;
   SetAsOwned(out, OLD_VAL);
   EXPECT_EQ(out.GetDataFlags(), Df::OWNED | Df::TERMINATED);
 
   nsDependentCSubstring in(NEW_VAL "garbage after", strlen(NEW_VAL));
   EXPECT_EQ(in.GetDataFlags(), Df(0));
 
   out.Assign(mozilla::Move(in));
   ExpectTruncated(in);
   ExpectNew(out);
 
-  EXPECT_EQ(out.GetDataFlags(), Df::SHARED | Df::TERMINATED);
+  EXPECT_EQ(out.GetDataFlags(), Df::REFCOUNTED | Df::TERMINATED);
 }
 
 TEST(MoveString, VoidIntoOwned) {
   nsCString out;
   SetAsOwned(out, OLD_VAL);
   EXPECT_EQ(out.GetDataFlags(), Df::OWNED | Df::TERMINATED);
 
   nsCString in = VoidCString();
@@ -150,24 +150,24 @@ TEST(MoveString, VoidIntoOwned) {
 
 TEST(MoveString, SharedIntoAuto) {
   nsAutoCString out;
   out.Assign(OLD_VAL);
   EXPECT_EQ(out.GetDataFlags(), Df::INLINE | Df::TERMINATED);
 
   nsCString in;
   in.Assign(NEW_VAL);
-  EXPECT_EQ(in.GetDataFlags(), Df::SHARED | Df::TERMINATED);
+  EXPECT_EQ(in.GetDataFlags(), Df::REFCOUNTED | Df::TERMINATED);
   const char* data = in.get();
 
   out.Assign(mozilla::Move(in));
   ExpectTruncated(in);
   ExpectNew(out);
 
-  EXPECT_EQ(out.GetDataFlags(), Df::SHARED | Df::TERMINATED);
+  EXPECT_EQ(out.GetDataFlags(), Df::REFCOUNTED | Df::TERMINATED);
   EXPECT_EQ(out.get(), data);
 }
 
 TEST(MoveString, OwnedIntoAuto) {
   nsAutoCString out;
   out.Assign(OLD_VAL);
   EXPECT_EQ(out.GetDataFlags(), Df::INLINE | Df::TERMINATED);
 
diff --git a/xpcom/tests/gtest/TestStrings.cpp b/xpcom/tests/gtest/TestStrings.cpp
--- a/xpcom/tests/gtest/TestStrings.cpp
+++ b/xpcom/tests/gtest/TestStrings.cpp
@@ -74,44 +74,44 @@ TEST(Strings, DependentStrings)
   using DataFlags = mozilla::detail::StringDataFlags;
 
   {
     // Test copy ctor.
     nsDependentCString tmp("foo");
     auto data = tmp.Data();
     nsDependentCString foo(tmp);
     // Neither string should be using a shared buffer.
-    EXPECT_FALSE(tmp.GetDataFlags() & DataFlags::SHARED);
-    EXPECT_FALSE(foo.GetDataFlags() & DataFlags::SHARED);
+    EXPECT_FALSE(tmp.GetDataFlags() & DataFlags::REFCOUNTED);
+    EXPECT_FALSE(foo.GetDataFlags() & DataFlags::REFCOUNTED);
     // Both strings should be pointing to the original buffer.
     EXPECT_EQ(data, tmp.Data());
     EXPECT_EQ(data, foo.Data());
   }
   {
     // Test move ctor.
     nsDependentCString tmp("foo");
     auto data = tmp.Data();
     nsDependentCString foo(mozilla::Move(tmp));
     // Neither string should be using a shared buffer.
-    EXPECT_FALSE(tmp.GetDataFlags() & DataFlags::SHARED);
-    EXPECT_FALSE(foo.GetDataFlags() & DataFlags::SHARED);
+    EXPECT_FALSE(tmp.GetDataFlags() & DataFlags::REFCOUNTED);
+    EXPECT_FALSE(foo.GetDataFlags() & DataFlags::REFCOUNTED);
     // First string should be reset, the second should be pointing to the
     // original buffer.
     EXPECT_NE(data, tmp.Data());
     EXPECT_EQ(data, foo.Data());
     EXPECT_TRUE(tmp.IsEmpty());
   }
   {
     // Test copying to a nsCString.
     nsDependentCString tmp("foo");
     auto data = tmp.Data();
     nsCString foo(tmp);
     // Original string should not be shared, copy should be shared.
-    EXPECT_FALSE(tmp.GetDataFlags() & DataFlags::SHARED);
-    EXPECT_TRUE(foo.GetDataFlags() & DataFlags::SHARED);
+    EXPECT_FALSE(tmp.GetDataFlags() & DataFlags::REFCOUNTED);
+    EXPECT_TRUE(foo.GetDataFlags() & DataFlags::REFCOUNTED);
     // First string should remain the same, the second should be pointing to
     // a new buffer.
     EXPECT_EQ(data, tmp.Data());
     EXPECT_NE(data, foo.Data());
   }
 }
 
 TEST(Strings, assign)
