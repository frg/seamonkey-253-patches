# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1517383191 -3600
# Node ID d3c3ec4cbe7820c2b5370504ed1377eb260d5e1e
# Parent  1080be0af8db81282c6c339cccc2be150f477687
Bug 1432963 - Fixing workers headers - part 3 - WorkerError and WorkerPrincipal without workers namespace, r=smaug

diff --git a/dom/workers/Principal.cpp b/dom/workers/Principal.cpp
--- a/dom/workers/Principal.cpp
+++ b/dom/workers/Principal.cpp
@@ -4,17 +4,18 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "Principal.h"
 
 #include "jsapi.h"
 #include "mozilla/Assertions.h"
 
-BEGIN_WORKERS_NAMESPACE
+namespace mozilla {
+namespace dom {
 
 struct WorkerPrincipal final : public JSPrincipals
 {
   bool write(JSContext* aCx, JSStructuredCloneWriter* aWriter) override {
     MOZ_CRASH("WorkerPrincipal::write not implemented");
     return false;
   }
 };
@@ -43,9 +44,10 @@ GetWorkerPrincipal()
 }
 
 void
 DestroyWorkerPrincipals(JSPrincipals* aPrincipals)
 {
   MOZ_ASSERT_UNREACHABLE("Worker principals refcount should never fall below one");
 }
 
-END_WORKERS_NAMESPACE
+} // dom namespace
+} // mozilla namespace
diff --git a/dom/workers/Principal.h b/dom/workers/Principal.h
--- a/dom/workers/Principal.h
+++ b/dom/workers/Principal.h
@@ -4,19 +4,21 @@
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #ifndef mozilla_dom_workers_principal_h__
 #define mozilla_dom_workers_principal_h__
 
 #include "WorkerCommon.h"
 
-BEGIN_WORKERS_NAMESPACE
+namespace mozilla {
+namespace dom {
 
 JSPrincipals*
 GetWorkerPrincipal();
 
 void
 DestroyWorkerPrincipals(JSPrincipals* aPrincipals);
 
-END_WORKERS_NAMESPACE
+} // dom namespace
+} // mozilla namespace
 
 #endif /* mozilla_dom_workers_principal_h__ */
diff --git a/dom/workers/WorkerDebugger.cpp b/dom/workers/WorkerDebugger.cpp
--- a/dom/workers/WorkerDebugger.cpp
+++ b/dom/workers/WorkerDebugger.cpp
@@ -464,15 +464,15 @@ WorkerDebugger::ReportErrorToDebuggerOnM
   nsTArray<nsCOMPtr<nsIWorkerDebuggerListener>> listeners(mListeners);
   for (size_t index = 0; index < listeners.Length(); ++index) {
     listeners[index]->OnError(aFilename, aLineno, aMessage);
   }
 
   WorkerErrorReport report;
   report.mMessage = aMessage;
   report.mFilename = aFilename;
-  LogErrorToConsole(report, 0);
+  WorkerErrorReport::LogErrorToConsole(report, 0);
 }
 
 
 } // worker namespace
 } // dom namespace
 } // mozilla namespace
diff --git a/dom/workers/WorkerError.cpp b/dom/workers/WorkerError.cpp
--- a/dom/workers/WorkerError.cpp
+++ b/dom/workers/WorkerError.cpp
@@ -152,17 +152,17 @@ public:
     if (aWorkerPrivate) {
       RefPtr<ReportErrorRunnable> runnable =
         new ReportErrorRunnable(aWorkerPrivate, aReport);
       runnable->Dispatch();
       return;
     }
 
     // Otherwise log an error to the error console.
-    LogErrorToConsole(aReport, aInnerWindowId);
+    WorkerErrorReport::LogErrorToConsole(aReport, aInnerWindowId);
   }
 
   ReportErrorRunnable(WorkerPrivate* aWorkerPrivate,
                       const WorkerErrorReport& aReport)
   : WorkerRunnable(aWorkerPrivate, ParentThreadUnchangedBusyCount),
     mReport(aReport)
   { }
 
@@ -282,26 +282,25 @@ WorkerErrorReport::AssignErrorReport(JSE
     size_t i = 0;
     for (auto&& note : *aReport->notes) {
       mNotes.ElementAt(i).AssignErrorNote(note.get());
       i++;
     }
   }
 }
 
-namespace workers {
-
 // aWorkerPrivate is the worker thread we're on (or the main thread, if null)
 // aTarget is the worker object that we are going to fire an error at
 // (if any).
-void
-ReportError(JSContext* aCx, WorkerPrivate* aWorkerPrivate,
-            bool aFireAtScope, DOMEventTargetHelper* aTarget,
-            const WorkerErrorReport& aReport, uint64_t aInnerWindowId,
-            JS::Handle<JS::Value> aException)
+/* static */ void
+WorkerErrorReport::ReportError(JSContext* aCx, WorkerPrivate* aWorkerPrivate,
+                               bool aFireAtScope, DOMEventTargetHelper* aTarget,
+                               const WorkerErrorReport& aReport,
+                               uint64_t aInnerWindowId,
+                               JS::Handle<JS::Value> aException)
 {
   if (aWorkerPrivate) {
     aWorkerPrivate->AssertIsOnWorkerThread();
   } else {
     AssertIsOnMainThread();
   }
 
   // We should not fire error events for warnings but instead make sure that
@@ -409,21 +408,22 @@ ReportError(JSContext* aCx, WorkerPrivat
   if (aWorkerPrivate) {
     RefPtr<ReportErrorRunnable> runnable =
       new ReportErrorRunnable(aWorkerPrivate, aReport);
     runnable->Dispatch();
     return;
   }
 
   // Otherwise log an error to the error console.
-  LogErrorToConsole(aReport, aInnerWindowId);
+  WorkerErrorReport::LogErrorToConsole(aReport, aInnerWindowId);
 }
 
-void
-LogErrorToConsole(const WorkerErrorReport& aReport, uint64_t aInnerWindowId)
+/* static */ void
+WorkerErrorReport::LogErrorToConsole(const WorkerErrorReport& aReport,
+                                     uint64_t aInnerWindowId)
 {
   AssertIsOnMainThread();
 
   RefPtr<nsScriptErrorBase> scriptError = new nsScriptError();
   NS_WARNING_ASSERTION(scriptError, "Failed to create script error!");
 
   if (scriptError) {
     nsAutoCString category("Web Worker");
@@ -475,11 +475,10 @@ LogErrorToConsole(const WorkerErrorRepor
   __android_log_print(ANDROID_LOG_INFO, "Gecko", kErrorString, msg.get(),
                       filename.get(), aReport.mLineNumber);
 #endif
 
   fprintf(stderr, kErrorString, msg.get(), filename.get(), aReport.mLineNumber);
   fflush(stderr);
 }
 
-} // workers namespace
 } // dom namespace
 } // mozilla namespace
diff --git a/dom/workers/WorkerError.h b/dom/workers/WorkerError.h
--- a/dom/workers/WorkerError.h
+++ b/dom/workers/WorkerError.h
@@ -35,48 +35,47 @@ public:
 };
 
 class WorkerErrorNote : public WorkerErrorBase
 {
 public:
   void AssignErrorNote(JSErrorNotes::Note* aNote);
 };
 
+namespace workers {
+class WorkerPrivate;
+}
+
 class WorkerErrorReport : public WorkerErrorBase
 {
 public:
   nsString mLine;
   uint32_t mFlags;
   JSExnType mExnType;
   bool mMutedError;
   nsTArray<WorkerErrorNote> mNotes;
 
   WorkerErrorReport()
   : mFlags(0),
     mExnType(JSEXN_ERR),
     mMutedError(false)
   { }
 
   void AssignErrorReport(JSErrorReport* aReport);
+
+  // aWorkerPrivate is the worker thread we're on (or the main thread, if null)
+  // aTarget is the worker object that we are going to fire an error at
+  // (if any).
+  static void
+  ReportError(JSContext* aCx, workers::WorkerPrivate* aWorkerPrivate,
+              bool aFireAtScope, DOMEventTargetHelper* aTarget,
+              const WorkerErrorReport& aReport, uint64_t aInnerWindowId,
+              JS::Handle<JS::Value> aException = JS::NullHandleValue);
+
+  static void
+  LogErrorToConsole(const WorkerErrorReport& aReport, uint64_t aInnerWindowId);
+
 };
 
 } // dom namespace
 } // mozilla namespace
 
-BEGIN_WORKERS_NAMESPACE
-
-class WorkerPrivate;
-
-// aWorkerPrivate is the worker thread we're on (or the main thread, if null)
-// aTarget is the worker object that we are going to fire an error at
-// (if any).
-void
-ReportError(JSContext* aCx, WorkerPrivate* aWorkerPrivate,
-            bool aFireAtScope, DOMEventTargetHelper* aTarget,
-            const WorkerErrorReport& aReport, uint64_t aInnerWindowId,
-            JS::Handle<JS::Value> aException = JS::NullHandleValue);
-
-void
-LogErrorToConsole(const WorkerErrorReport& aReport, uint64_t aInnerWindowId);
-
-END_WORKERS_NAMESPACE
-
 #endif // mozilla_dom_workers_WorkerError_h
diff --git a/dom/workers/WorkerPrivate.cpp b/dom/workers/WorkerPrivate.cpp
--- a/dom/workers/WorkerPrivate.cpp
+++ b/dom/workers/WorkerPrivate.cpp
@@ -2607,17 +2607,17 @@ WorkerPrivateParent<Derived>::BroadcastE
                                                     bool aIsErrorEvent)
 {
   AssertIsOnMainThread();
 
   if (aIsErrorEvent && JSREPORT_IS_WARNING(aReport->mFlags)) {
     // Don't fire any events anywhere.  Just log to console.
     // XXXbz should we log to all the consoles of all the relevant windows?
     MOZ_ASSERT(aReport);
-    LogErrorToConsole(*aReport, 0);
+    WorkerErrorReport::LogErrorToConsole(*aReport, 0);
     return;
   }
 
   AutoTArray<RefPtr<SharedWorker>, 10> sharedWorkers;
   GetAllSharedWorkers(sharedWorkers);
 
   if (sharedWorkers.IsEmpty()) {
     return;
@@ -2726,17 +2726,17 @@ WorkerPrivateParent<Derived>::BroadcastE
     if (status == nsEventStatus_eConsumeNoDefault) {
       shouldLogErrorToConsole = false;
     }
   }
 
   // Finally log a warning in the console if no window tried to prevent it.
   if (shouldLogErrorToConsole) {
     MOZ_ASSERT(aReport);
-    LogErrorToConsole(*aReport, 0);
+    WorkerErrorReport::LogErrorToConsole(*aReport, 0);
   }
 }
 
 template <class Derived>
 void
 WorkerPrivateParent<Derived>::GetAllSharedWorkers(
                                nsTArray<RefPtr<SharedWorker>>& aSharedWorkers)
 {
@@ -5044,17 +5044,18 @@ WorkerPrivate::ReportError(JSContext* aC
   mErrorHandlerRecursionCount++;
 
   // Don't want to run the scope's error handler if this is a recursive error or
   // if we ran out of memory.
   bool fireAtScope = mErrorHandlerRecursionCount == 1 &&
                      report.mErrorNumber != JSMSG_OUT_OF_MEMORY &&
                      JS::CurrentGlobalOrNull(aCx);
 
-  workers::ReportError(aCx, this, fireAtScope, nullptr, report, 0, exn);
+  WorkerErrorReport::ReportError(aCx, this, fireAtScope, nullptr, report, 0,
+                                 exn);
 
   mErrorHandlerRecursionCount--;
 }
 
 // static
 void
 WorkerPrivate::ReportErrorToConsole(const char* aMessage)
 {
