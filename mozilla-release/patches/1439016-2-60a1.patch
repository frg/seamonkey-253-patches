# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1519048837 -3600
# Node ID 6ec2d0d818ce6d41d63e08a7322d1059f9b7d03c
# Parent  f4e2d03a3ab27bce5a7600ae8f352311b09b1678
Bug 1439016: Assert that there's no stale servo data in shadow roots either. r=bholley

Just expanding the assertion to cover shadow trees.

MozReview-Commit-ID: FLE0noGzaIF

diff --git a/dom/base/nsDocument.cpp b/dom/base/nsDocument.cpp
--- a/dom/base/nsDocument.cpp
+++ b/dom/base/nsDocument.cpp
@@ -4050,16 +4050,33 @@ nsIDocument::ShouldThrottleFrameRequests
     // platforms and is unlikely to be human-perceivable on non-APZ platforms.
     return true;
   }
 
   // We got painted during the last paint, so run at full speed.
   return false;
 }
 
+static inline void
+AssertNoStaleServoDataIn(const nsINode& aSubtreeRoot)
+{
+#ifdef DEBUG
+  for (const nsINode* node = aSubtreeRoot.GetFirstChild();
+       node;
+       node = node->GetNextNode()) {
+    if (node->IsElement()) {
+      MOZ_ASSERT(!node->AsElement()->HasServoData());
+      if (auto* shadow = node->AsElement()->GetShadowRoot()) {
+        AssertNoStaleServoDataIn(*shadow);
+      }
+    }
+  }
+#endif
+}
+
 void
 nsDocument::DeleteShell()
 {
   mExternalResourceMap.HideViewers();
   if (nsPresContext* presContext = mPresShell->GetPresContext()) {
     presContext->RefreshDriver()->CancelPendingEvents(this);
   }
 
@@ -4080,25 +4097,17 @@ nsDocument::DeleteShell()
 
   nsIPresShell* oldShell = mPresShell;
   mPresShell = nullptr;
   UpdateFrameRequestCallbackSchedulingState(oldShell);
   mStyleSetFilled = false;
 
   if (IsStyledByServo()) {
     ClearStaleServoData();
-#ifdef DEBUG
-    for (nsINode* node = static_cast<nsINode*>(this)->GetFirstChild();
-         node;
-         node = node->GetNextNode(this)) {
-      if (node->IsElement()) {
-        MOZ_ASSERT(!node->AsElement()->HasServoData());
-      }
-    }
-#endif
+    AssertNoStaleServoDataIn(static_cast<nsINode&>(*this));
   }
 }
 
 static void
 SubDocClearEntry(PLDHashTable *table, PLDHashEntryHdr *entry)
 {
   SubDocMapEntry *e = static_cast<SubDocMapEntry *>(entry);
 
