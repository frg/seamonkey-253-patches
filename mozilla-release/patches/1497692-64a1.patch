# HG changeset patch
# User Gabriele Svelto <gsvelto@mozilla.com>
# Date 1539355816 0
# Node ID f3d26ec88c8d6e045d4eb8defbf09810a4c440a9
# Parent  4b9364ed653d6c9e25a66e6111f94e404eabe993
Bug 1497692 - Lower the minidump-analyzer priority r=ted

Differential Revision: https://phabricator.services.mozilla.com/D8242

diff --git a/toolkit/crashreporter/minidump-analyzer/minidump-analyzer.cpp b/toolkit/crashreporter/minidump-analyzer/minidump-analyzer.cpp
--- a/toolkit/crashreporter/minidump-analyzer/minidump-analyzer.cpp
+++ b/toolkit/crashreporter/minidump-analyzer/minidump-analyzer.cpp
@@ -17,16 +17,17 @@
 #include "google_breakpad/processor/code_modules.h"
 #include "google_breakpad/processor/minidump.h"
 #include "google_breakpad/processor/minidump_processor.h"
 #include "google_breakpad/processor/process_state.h"
 #include "google_breakpad/processor/stack_frame.h"
 #include "processor/pathname_stripper.h"
 
 #include "mozilla/FStream.h"
+#include "mozilla/Unused.h"
 
 #if defined(XP_WIN32)
 
 #include <windows.h>
 #include "mozilla/glue/WindowsDllServices.h"
 
 #elif defined(XP_UNIX) || defined(XP_MACOSX)
 
@@ -66,16 +67,17 @@ using google_breakpad::CodeModules;
 using google_breakpad::Minidump;
 using google_breakpad::MinidumpProcessor;
 using google_breakpad::PathnameStripper;
 using google_breakpad::ProcessResult;
 using google_breakpad::ProcessState;
 using google_breakpad::StackFrame;
 
 using mozilla::OFStream;
+using mozilla::Unused;
 
 MinidumpAnalyzerOptions gMinidumpAnalyzerOptions;
 
 // Path of the minidump to be analyzed.
 static string gMinidumpPath;
 
 struct ModuleCompare {
   bool operator() (const CodeModule* aLhs, const CodeModule* aRhs) const {
@@ -470,16 +472,27 @@ struct CharTraits<wchar_t>
   {
     left = WideToUTF8(right);
     return left;
   }
 };
 
 #endif // defined(XP_WIN)
 
+static void
+LowerPriority()
+{
+#if defined(XP_WIN)
+  Unused << SetPriorityClass(GetCurrentProcess(),
+                             PROCESS_MODE_BACKGROUND_BEGIN);
+#else // Linux, MacOS X, etc...
+  Unused << nice(20);
+#endif
+}
+
 template <typename CharT, typename Traits = CharTraits<CharT>>
 static void
 ParseArguments(int argc, CharT** argv) {
   if (argc <= 1) {
     exit(EXIT_FAILURE);
   }
 
   for (int i = 1; i < argc - 1; i++) {
@@ -502,16 +515,17 @@ ParseArguments(int argc, CharT** argv) {
 // Using wmain here so that the CRT doesn't need to perform a wasteful and
 // lossy UTF-16 to MBCS conversion; ParseArguments will convert to UTF8 directly.
 extern "C"
 int wmain(int argc, wchar_t** argv)
 #else
 int main(int argc, char** argv)
 #endif
 {
+  LowerPriority();
   ParseArguments(argc, argv);
 
   if (!GenerateStacks(gMinidumpPath, gMinidumpAnalyzerOptions.fullMinidump)) {
     exit(EXIT_FAILURE);
   }
 
   exit(EXIT_SUCCESS);
 }
diff --git a/toolkit/crashreporter/minidump-analyzer/moz.build b/toolkit/crashreporter/minidump-analyzer/moz.build
--- a/toolkit/crashreporter/minidump-analyzer/moz.build
+++ b/toolkit/crashreporter/minidump-analyzer/moz.build
@@ -1,20 +1,16 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 if CONFIG['OS_TARGET'] != 'Android':
-    if CONFIG['OS_TARGET'] == 'WINNT':
-        # We need mozglue on Windows for access to DLL services
-        GeckoProgram(name='minidump-analyzer', linkage=None)
-    else:
-        Program('minidump-analyzer')
+    GeckoProgram(name='minidump-analyzer', linkage=None)
 
     if CONFIG['OS_TARGET'] == 'Darwin':
         DIST_SUBDIR = 'crashreporter.app/Contents/MacOS'
 
     if CONFIG['OS_TARGET'] == 'WINNT':
         DEFINES['UNICODE'] = True
         DEFINES['_UNICODE'] = True
 
