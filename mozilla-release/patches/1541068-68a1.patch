# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1554836359 0
# Node ID 4bb96b9e1a3241c400b6fcdcd6ebf7e97fc5fcb9
# Parent  7c856a8e02f7570074ede48a67934cf73488a958
Bug 1541068 - add Rust compilation directories to GARBAGE_DIRS; r=nalexander

We add to `GARBAGE_DIRS` in the toplevel `Makefile.in` because all of
our Rust libraries share a single `CARGO_TARGET_DIR`, located in
topobjdir.

We add to `GARBAGE_DIRS` for Rust programs because Rust programs
currently do not share compilation artifacts with Rust libraries (as our
libraries are built with `panic=abort` and our programs are not, sharing
compilation artifacts between the two is a non-starter).

Differential Revision: https://phabricator.services.mozilla.com/D26762

diff --git a/Makefile.in b/Makefile.in
--- a/Makefile.in
+++ b/Makefile.in
@@ -19,16 +19,23 @@ ifndef TEST_MOZBUILD
 ifdef MOZ_BUILD_APP
 include $(wildcard $(topsrcdir)/$(MOZ_BUILD_APP)/build.mk)
 endif
 endif
 
 include $(topsrcdir)/config/config.mk
 
 GARBAGE_DIRS += _javagen _profile staticlib
+# To share compilation of dependencies, Rust libraries all set their
+# CARGO_TARGET_DIR as a subdirectory of topobjdir.  Normally, we would add
+# RUST*TARGET to GARBAGE_DIRS for those directories building Rust libraries.
+# But the directories building Rust libraries don't actually have
+# subdirectories to remove.  So we add to GARBAGE_DIRS once here, globally,
+# for it to have the desired effect.
+GARBAGE_DIRS += $(RUST_TARGET)
 DIST_GARBAGE = config.cache config.log config.status* config-defs.h \
    config/autoconf.mk \
    mozilla-config.h \
    netwerk/necko-config.h xpcom/xpcom-config.h xpcom/xpcom-private.h \
    .mozconfig.mk
 
 ifndef MOZ_PROFILE_USE
 # Automation builds should always have a new buildid, but for the sake of not
diff --git a/config/makefiles/rust.mk b/config/makefiles/rust.mk
--- a/config/makefiles/rust.mk
+++ b/config/makefiles/rust.mk
@@ -259,29 +259,35 @@ force-cargo-host-library-build:
 force-cargo-host-library-check:
 	$(call CARGO_CHECK) --lib $(cargo_host_flag) $(host_rust_features_flag)
 else
 force-cargo-host-library-check:
 	@true
 endif # HOST_RUST_LIBRARY_FILE
 
 ifdef RUST_PROGRAMS
+
+GARBAGE_DIRS += $(RUST_TARGET)
+
 force-cargo-program-build:
 	$(REPORT_BUILD)
 	$(call CARGO_BUILD) $(addprefix --bin ,$(RUST_CARGO_PROGRAMS)) $(cargo_target_flag)
 
 $(RUST_PROGRAMS): force-cargo-program-build
 
 force-cargo-program-check:
 	$(call CARGO_CHECK) $(addprefix --bin ,$(RUST_CARGO_PROGRAMS)) $(cargo_target_flag)
 else
 force-cargo-program-check:
 	@true
 endif # RUST_PROGRAMS
 ifdef HOST_RUST_PROGRAMS
+
+GARBAGE_DIRS += $(RUST_HOST_TARGET)
+
 force-cargo-host-program-build:
 	$(REPORT_BUILD)
 	$(call CARGO_BUILD) $(addprefix --bin ,$(HOST_RUST_CARGO_PROGRAMS)) $(cargo_host_flag)
 
 $(HOST_RUST_PROGRAMS): force-cargo-host-program-build
 
 force-cargo-host-program-check:
 	$(REPORT_BUILD)
