# HG changeset patch
# User Mitchell Hentges <mhentges@mozilla.com>
# Date 1622128715 0
# Node ID 2e37a3ce90c2e7e2ecc612d2499a9bf15edb0bf1
# Parent  9c8ce3db5587fc68652c43c474ee48bcbc15b6a0
Bug 1712133: Remove build VIRTUALENV_NAME customization r=glandium

This was originally set up so that tests wouldn't "create a new
`virtualenv` for no reason." However, virtual environments now will have
different packages installed, and therefore the separation is necessary.

So, for the virtual environment used for builds (regular or for tests):
* We want it to be able to reuse the build venv, if it already exists.
* We don't want to pollute a `pytest` virtualenv with build-specific
  packages.

Differential Revision: https://phabricator.services.mozilla.com/D115641

diff --git a/build/moz.configure/init.configure b/build/moz.configure/init.configure
--- a/build/moz.configure/init.configure
+++ b/build/moz.configure/init.configure
@@ -212,34 +212,30 @@ def help_shell(help, shell):
 shell = help_shell | shell
 
 
 # Python 3
 # ========
 
 option(env='PYTHON3', nargs=1, help='Python 3 interpreter (3.6 or later)')
 
-option(env='VIRTUALENV_NAME', nargs=1, default='common',
-       help='Name of the in-objdir virtualenv')
 
-
-@depends('PYTHON3', 'VIRTUALENV_NAME', check_build_environment, mozconfig,
-         '--help')
+@depends("PYTHON3", check_build_environment, mozconfig, "--help")
 @imports(_from='__builtin__', _import='Exception')
 @imports('os')
 @imports('sys')
 @imports('subprocess')
 @imports('distutils.sysconfig')
 @imports(_from='mozbuild.configure.util', _import='LineIO')
 @imports(_from='mozbuild.virtualenv', _import='VirtualenvManager')
 @imports(_from='mozbuild.virtualenv', _import='verify_python_version')
 @imports(_from='mozbuild.pythonutil', _import='find_python3_executable')
 @imports(_from='mozbuild.pythonutil', _import='python_executable_version')
 @imports(_from='six', _import='ensure_text')
-def virtualenv_python3(env_python, virtualenv_name, build_env, mozconfig, help):
+def virtualenv_python3(env_python, build_env, mozconfig, help):
     # Avoid re-executing python when running configure --help.
     if help:
         return
 
     # NOTE: We cannot assume the Python we are calling this code with is the
     # Python we want to set up a virtualenv for.
     #
     # We also cannot assume that the Python the caller is configuring meets our
@@ -247,17 +243,16 @@ def virtualenv_python3(env_python, virtu
     #
     # Because of this the code is written to re-execute itself with the correct
     # interpreter if required.
 
     log.debug("python3: running with pid %r" % os.getpid())
     log.debug("python3: sys.executable: %r" % sys.executable)
 
     python = env_python[0] if env_python else None
-    virtualenv_name = virtualenv_name[0]
 
     # Did our python come from mozconfig? Overrides environment setting.
     # Ideally we'd rely on the mozconfig injection from mozconfig_options,
     # but we'd rather avoid the verbosity when we need to reexecute with
     # a different python.
     if mozconfig['path']:
         if 'PYTHON3' in mozconfig['env']['added']:
             python = mozconfig['env']['added']['PYTHON3']
@@ -283,17 +278,18 @@ def virtualenv_python3(env_python, virtu
     topsrcdir, topobjdir = build_env.topsrcdir, build_env.topobjdir
     if topobjdir.endswith('/js/src'):
         topobjdir = topobjdir[:-7]
 
     virtualenvs_root = os.path.join(topobjdir, '_virtualenvs')
     with LineIO(lambda l: log.info(l), 'replace') as out:
         manager = VirtualenvManager(
             topsrcdir,
-            os.path.join(virtualenvs_root, virtualenv_name), out,
+            os.path.join(virtualenvs_root, "common"),
+            out,
             os.path.join(topsrcdir, 'build', 'build_virtualenv_packages.txt'))
 
     # Update the path to include some necessary modules for find_program.
     sys.path.insert(0, os.path.join(topsrcdir, "testing", "mozbase", "mozfile"))
     sys.path.insert(0, os.path.join(topsrcdir, "third_party", "python", "backports"))
 
     # If we know the Python executable the caller is asking for then verify its
     # version. If the caller did not ask for a specific executable then find
diff --git a/python/mozbuild/mozbuild/test/configure/common.py b/python/mozbuild/mozbuild/test/configure/common.py
--- a/python/mozbuild/mozbuild/test/configure/common.py
+++ b/python/mozbuild/mozbuild/test/configure/common.py
@@ -289,17 +289,17 @@ class BaseConfigureTest(unittest.TestCas
             mozconfig_path = os.path.join(os.path.dirname(__file__), 'data',
                                           'empty_mozconfig')
 
         try:
             environ = dict(
                 environ,
                 OLD_CONFIGURE=os.path.join(topsrcdir, 'old-configure'),
                 MOZCONFIG=mozconfig_path,
-                VIRTUALENV_NAME='python-test')
+            )
 
             paths = dict(paths)
             autoconf_dir = mozpath.join(topsrcdir, 'build', 'autoconf')
             paths[mozpath.join(autoconf_dir,
                                'config.guess')] = self.config_guess
             paths[mozpath.join(autoconf_dir, 'config.sub')] = self.config_sub
 
             sandbox = cls(paths, config, environ, ['configure'] + target + args,
diff --git a/python/mozbuild/mozbuild/test/configure/test_toolkit_moz_configure.py b/python/mozbuild/mozbuild/test/configure/test_toolkit_moz_configure.py
--- a/python/mozbuild/mozbuild/test/configure/test_toolkit_moz_configure.py
+++ b/python/mozbuild/mozbuild/test/configure/test_toolkit_moz_configure.py
@@ -24,19 +24,17 @@ class TestToolkitMozConfigure(BaseConfig
                                 help='Help missing for old configure options')
 
             # Remove all implied options, otherwise, getting
             # all_configure_options below triggers them, and that triggers
             # configure parts that aren't expected to run during this test.
             del sandbox._implied_options[:]
             result = sandbox._value_for(sandbox['all_configure_options'])
             shell = mozpath.abspath('/bin/sh')
-            return (result
-                    .replace('CONFIG_SHELL=%s ' % shell, '')
-                    .replace('VIRTUALENV_NAME=python-test ', ''))
+            return result.replace("CONFIG_SHELL=%s " % shell, "")
 
         self.assertEquals('--enable-application=browser',
                           get_value_for(['--enable-application=browser']))
 
         self.assertEquals('--enable-application=browser '
                           'MOZ_VTUNE=1',
                           get_value_for(['--enable-application=browser',
                                          'MOZ_VTUNE=1']))
