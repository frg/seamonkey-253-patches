# HG changeset patch
# User Christoph Kerschbaumer <ckerschb@christophkerschbaumer.com>
# Date 1523861493 -7200
# Node ID b5e2ab3b7a26a301212db459a42067feabba426a
# Parent  ba7efbbc3ffc84d03cab502ee4af877376b4e675
Bug 1454242: Test samesite cookie on top-level page from cross-origin context. r=mgoodwin

diff --git a/dom/security/test/general/file_same_site_cookies_toplevel_set_cookie.sjs b/dom/security/test/general/file_same_site_cookies_toplevel_set_cookie.sjs
new file mode 100644
--- /dev/null
+++ b/dom/security/test/general/file_same_site_cookies_toplevel_set_cookie.sjs
@@ -0,0 +1,64 @@
+// Custom *.sjs file specifically for the needs of Bug 1454242
+
+const WIN = `
+  <html>
+  <body>
+  <script type="application/javascript">
+    let newWin = window.open("http://mochi.test:8888/tests/dom/security/test/general/file_same_site_cookies_toplevel_set_cookie.sjs?loadWinAndSetCookie");
+    newWin.onload = function() {
+      newWin.close();
+    }
+  </script>
+  </body>
+  </html>`;
+
+const DUMMY_WIN = `
+  <html>
+  <body>
+  just a dummy window that sets a same-site=lax cookie
+  <script type="application/javascript">
+    window.opener.opener.postMessage({value: 'testSetupComplete'}, '*');
+  </script>
+  </body>
+  </html>`;
+
+const FRAME = `
+  <html>
+  <body>
+  <script type="application/javascript">
+    let cookie = document.cookie;
+    // now reset the cookie for the next test
+    document.cookie = "myKey=;" + "expires=Thu, 01 Jan 1970 00:00:00 GMT";
+    window.parent.postMessage({value: cookie}, 'http://mochi.test:8888');
+  </script>
+  </body>
+  </html>`;
+
+const SAME_ORIGIN = "http://mochi.test:8888/"
+const CROSS_ORIGIN = "http://example.com/";
+const PATH = "tests/dom/security/test/general/file_same_site_cookies_redirect.sjs";
+
+function handleRequest(request, response)
+{
+  // avoid confusing cache behaviors
+  response.setHeader("Cache-Control", "no-cache", false);
+
+  if (request.queryString === "loadWin") {
+    response.write(WIN);
+    return;
+  }
+
+  if (request.queryString === "loadWinAndSetCookie") {
+    response.setHeader("Set-Cookie", "myKey=laxSameSiteCookie; samesite=lax", true);
+    response.write(DUMMY_WIN);
+    return;
+  }
+
+  if (request.queryString === "checkCookie") {
+    response.write(FRAME);
+    return;
+  }
+
+  // we should never get here, but just in case return something unexpected
+  response.write("D'oh");
+}
diff --git a/dom/security/test/general/mochitest.ini b/dom/security/test/general/mochitest.ini
--- a/dom/security/test/general/mochitest.ini
+++ b/dom/security/test/general/mochitest.ini
@@ -7,16 +7,17 @@ support-files =
   file_block_toplevel_data_navigation2.html
   file_block_toplevel_data_navigation3.html
   file_block_toplevel_data_redirect.sjs
   file_same_site_cookies_subrequest.sjs
   file_same_site_cookies_toplevel_nav.sjs
   file_same_site_cookies_cross_origin_context.sjs
   file_same_site_cookies_from_script.sjs
   file_same_site_cookies_redirect.sjs
+  file_same_site_cookies_toplevel_set_cookie.sjs
 
 [test_contentpolicytype_targeted_link_iframe.html]
 [test_nosniff.html]
 [test_block_script_wrong_mime.html]
 [test_block_toplevel_data_navigation.html]
 skip-if = toolkit == 'android' # intermittent failure
 [test_block_toplevel_data_img_navigation.html]
 skip-if = toolkit == 'android' # intermittent failure
@@ -24,8 +25,9 @@ skip-if = toolkit == 'android' # intermi
 skip-if = toolkit == 'android'
 [test_allow_opening_data_json.html]
 skip-if = toolkit == 'android'
 [test_same_site_cookies_subrequest.html]
 [test_same_site_cookies_toplevel_nav.html]
 [test_same_site_cookies_cross_origin_context.html]
 [test_same_site_cookies_from_script.html]
 [test_same_site_cookies_redirect.html]
+[test_same_site_cookies_toplevel_set_cookie.html]
diff --git a/dom/security/test/general/test_same_site_cookies_toplevel_set_cookie.html b/dom/security/test/general/test_same_site_cookies_toplevel_set_cookie.html
new file mode 100644
--- /dev/null
+++ b/dom/security/test/general/test_same_site_cookies_toplevel_set_cookie.html
@@ -0,0 +1,57 @@
+<!DOCTYPE HTML>
+<html>
+<head>
+  <title>Bug 1454242: Setting samesite cookie should not rely on NS_IsSameSiteForeign</title>
+  <script type="text/javascript" src="/tests/SimpleTest/SimpleTest.js"></script>
+  <link rel="stylesheet" type="text/css" href="/tests/SimpleTest/test.css" />
+</head>
+<body>
+<img id="cookieImage">
+<iframe id="testframe"></iframe>
+
+<script class="testbody" type="text/javascript">
+
+/*
+ * Description of the test:
+ * 1) We load a window from example.com which loads a window from mochi.test
+ *    which then sets a same-site cookie for mochi.test.
+ * 2) We load an iframe from mochi.test.
+ * 3) We observe that the cookie within (1) was allowed to be set and
+ *    is available for mochi.test.
+ */
+
+SimpleTest.waitForExplicitFinish();
+
+const SAME_ORIGIN = "http://mochi.test:8888/"
+const CROSS_ORIGIN = "http://example.com/";
+const PATH = "tests/dom/security/test/general/file_same_site_cookies_toplevel_set_cookie.sjs";
+
+let testWin = null;
+
+window.addEventListener("message", receiveMessage);
+function receiveMessage(event) {
+  // once the second window (which sets the cookie) loaded, we get a notification
+  // that the test setup is correct and we can now try to query the same-site cookie
+  if (event.data.value === "testSetupComplete") {
+    ok(true, "cookie setup worked");
+    let testframe = document.getElementById("testframe");
+    testframe.src = SAME_ORIGIN + PATH + "?checkCookie";
+    return;
+  }
+
+  // thie second message is the cookie value from verifying the
+  // cookie has been set correctly.
+  is(event.data.value, "myKey=laxSameSiteCookie",
+     "setting same-site cookie on cross origin top-level page");
+
+  window.removeEventListener("message", receiveMessage);
+  testWin.close();
+  SimpleTest.finish();
+}
+
+// fire up the test
+testWin = window.open(CROSS_ORIGIN + PATH + "?loadWin");
+
+</script>
+</body>
+</html>
