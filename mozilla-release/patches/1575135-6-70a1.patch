# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1566362683 0
#      Wed Aug 21 04:44:43 2019 +0000
# Node ID 2139ee18939eae14ea337e29ec46639d2b25836d
# Parent  782cbdafb3b5a59d24d62a34ec68dcb41ab29135
Bug 1575135 - Make configure sandbox open() look more like python 3's. r=nalexander

As a consequence, we can replace the encoded_open function that did the
same in an opt-in manner.

Differential Revision: https://phabricator.services.mozilla.com/D42605

diff --git a/build/moz.configure/old.configure b/build/moz.configure/old.configure
--- a/build/moz.configure/old.configure
+++ b/build/moz.configure/old.configure
@@ -1,22 +1,15 @@
 # -*- Mode: python; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 
-@imports('codecs')
-@imports('sys')
-@imports(_from='mozbuild.util', _import='system_encoding')
-def encoded_open(path, mode):
-    return codecs.open(path, mode, system_encoding)
-
-
 option(env='AUTOCONF', nargs=1, help='Path to autoconf 2.13')
 
 
 @depends(mozconfig, 'AUTOCONF')
 @checking('for autoconf')
 @imports(_from='os.path', _import='exists')
 @imports('re')
 def autoconf(mozconfig, autoconf):
@@ -143,17 +136,17 @@ def prepare_configure(old_configure, moz
             try:
                 # Likely the file already existed (on Windows). Retry after removing it.
                 remove(old_configure)
                 rename(fh.name, old_configure)
             except OSError as e:
                 die('Failed re-creating old-configure: %s' % e.message)
 
     cmd = [shell, old_configure]
-    with encoded_open('old-configure.vars', 'w') as out:
+    with open('old-configure.vars', 'w') as out:
         log.debug('Injecting the following to old-configure:')
 
         def inject(command):
             print(command, file=out) # noqa Python 2vs3
             log.debug('| %s', command)
 
         if mozconfig:
             inject('# start of mozconfig values')
@@ -345,34 +338,34 @@ def old_configure(prepare_configure, pre
         if not line:
             break
         log.info(line.rstrip())
 
     ret = proc.wait()
     if ret:
         with log.queue_debug():
             if config_log:
-                with encoded_open(config_log.baseFilename, 'r') as fh:
+                with open(config_log.baseFilename, 'r') as fh:
                     fh.seek(log_size)
                     for line in fh:
                         log.debug(line.rstrip())
             log.error('old-configure failed')
         sys.exit(ret)
 
     if config_log:
         # Create a new handler in append mode
         handler = logging.FileHandler(config_log.baseFilename, mode='a', delay=True)
         handler.setFormatter(config_log.formatter)
         logger.addHandler(handler)
 
     raw_config = {
         'split': split,
         'unique_list': unique_list,
     }
-    with encoded_open('config.data', 'r') as fh:
+    with open('config.data', 'r') as fh:
         code = compile(fh.read(), 'config.data', 'exec')
         # Every variation of the exec() function I tried led to:
         # SyntaxError: unqualified exec is not allowed in function 'main' it
         # contains a nested function with free variables
         exec code in raw_config # noqa
 
     # Ensure all the flags known to old-configure appear in the
     # @old_configure_options above.
diff --git a/config/mozunit/mozunit/mozunit.py b/config/mozunit/mozunit/mozunit.py
--- a/config/mozunit/mozunit/mozunit.py
+++ b/config/mozunit/mozunit/mozunit.py
@@ -165,17 +165,18 @@ class MockedOpen(object):
     self.assertRaises(Exception,f.open('foo', 'r'))
     '''
 
     def __init__(self, files={}):
         self.files = {}
         for name, content in files.items():
             self.files[normcase(os.path.abspath(name))] = content
 
-    def __call__(self, name, mode='r'):
+    def __call__(self, name, mode='r', buffering=None):
+        # buffering is ignored.
         absname = normcase(os.path.abspath(name))
         if 'w' in mode:
             file = MockedFile(self, absname)
         elif absname in self.files:
             content = self.files[absname]
             if content is None:
                 raise IOError(2, 'No such file or directory')
             file = MockedFile(self, absname, content)
diff --git a/python/mozbuild/mozbuild/configure/__init__.py b/python/mozbuild/mozbuild/configure/__init__.py
--- a/python/mozbuild/mozbuild/configure/__init__.py
+++ b/python/mozbuild/mozbuild/configure/__init__.py
@@ -1,14 +1,15 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
+import codecs
 import inspect
 import logging
 import os
 import re
 import six
 from six.moves import builtins as __builtin__
 import sys
 import types
@@ -30,16 +31,17 @@ from mozbuild.configure.util import (
 )
 from mozbuild.util import (
     encode,
     exec_,
     memoize,
     memoized_property,
     ReadOnlyDict,
     ReadOnlyNamespace,
+    system_encoding,
 )
 
 import mozpack.path as mozpath
 
 
 # TRACE logging level, below (thus more verbose than) DEBUG
 TRACE = 5
 
@@ -889,19 +891,31 @@ class ConfigureSandbox(dict):
 
     def _get_one_import(self, what):
         # The special `__sandbox__` module gives access to the sandbox
         # instance.
         if what == '__sandbox__':
             return self
         # Special case for the open() builtin, because otherwise, using it
         # fails with "IOError: file() constructor not accessible in
-        # restricted mode"
+        # restricted mode". We also make open() look more like python 3's,
+        # decoding to unicode strings unless the mode says otherwise.
         if what == '__builtin__.open':
-            return lambda *args, **kwargs: open(*args, **kwargs)
+            def wrapped_open(name, mode=None, buffering=None):
+                args = (name,)
+                kwargs = {}
+                if buffering is not None:
+                    kwargs['buffering'] = buffering
+                if mode is not None:
+                    args += (mode,)
+                    if 'b' in mode:
+                        return open(*args, **kwargs)
+                kwargs['encoding'] = system_encoding
+                return codecs.open(*args, **kwargs)
+            return wrapped_open
         # Special case os and os.environ so that os.environ is our copy of
         # the environment.
         if what == 'os.environ':
             return self._environ
         if what == 'os':
             return self._wrapped_os
         # And subprocess, so that its functions use our os.environ
         if what == 'subprocess':
diff --git a/python/mozbuild/mozbuild/test/configure/data/subprocess.configure b/python/mozbuild/mozbuild/test/configure/data/subprocess.configure
--- a/python/mozbuild/mozbuild/test/configure/data/subprocess.configure
+++ b/python/mozbuild/mozbuild/test/configure/data/subprocess.configure
@@ -10,14 +10,14 @@
 @imports('os')
 @imports(_from='__builtin__', _import='open')
 def dies_when_logging(_):
     test_file = 'test.txt'
     quote_char = "'"
     if getpreferredencoding().lower() == 'utf-8':
         quote_char = '\u00B4'.encode('utf-8')
     try:
-        with open(test_file, 'w+') as fh:
+        with open(test_file, 'w+b') as fh:
             fh.write(quote_char)
         out = check_cmd_output('cat', 'test.txt')
         log.info(out)
     finally:
         os.remove(test_file)
