# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1557941373 0
# Node ID 5f88dc67d75ba1e08dad4c4e9b39a8df593b2be6
# Parent  fae58a4be4042a3625b8c77fb1f189dd9f56b566
Bug 1505471 - Map intersection observer rects to the right viewport. r=mstange

targetFrame is modified during the intersection computation loop, so it's not
the viewport you want if there are scrollframes around.

The test is the same as iframe-no-root.html but with a wrapping scroller which
triggers this bug.

This code is quite subtle, so will refactor and clean it up in a followup.

Differential Revision: https://phabricator.services.mozilla.com/D31147

diff --git a/dom/base/DOMIntersectionObserver.cpp b/dom/base/DOMIntersectionObserver.cpp
--- a/dom/base/DOMIntersectionObserver.cpp
+++ b/dom/base/DOMIntersectionObserver.cpp
@@ -331,16 +331,17 @@ DOMIntersectionObserver::Update(nsIDocum
       MOZ_ASSERT_UNREACHABLE("invalid length unit");
     }
     rootMargin.Side(side) = nsLayoutUtils::ComputeCBDependentValue(basis, coord);
   }
 
   for (size_t i = 0; i < mObservationTargets.Length(); ++i) {
     Element* target = mObservationTargets.ElementAt(i);
     nsIFrame* targetFrame = target->GetPrimaryFrame();
+    nsIFrame* originalTargetFrame = targetFrame;
     nsRect targetRect;
     Maybe<nsRect> intersectionRect;
     bool isSameDoc = root && root->GetComposedDoc() == target->GetComposedDoc();
 
     if (rootFrame && targetFrame) {
       // If mRoot is set we are testing intersection with a container element
       // instead of the implicit root.
       if (mRoot) {
@@ -416,17 +417,17 @@ DOMIntersectionObserver::Update(nsIDocum
           nsLayoutUtils::GetContainingBlockForClientRect(rootFrame)
       );
       intersectionRect = EdgeInclusiveIntersection(
         intersectionRectRelativeToRoot,
         rootIntersectionRect
       );
       if (intersectionRect.isSome() && !isSameDoc) {
         nsRect rect = intersectionRect.value();
-        nsPresContext* presContext = targetFrame->PresContext();
+        nsPresContext* presContext = originalTargetFrame->PresContext();
         nsIFrame* rootScrollFrame = presContext->PresShell()->GetRootScrollFrame();
         if (rootScrollFrame) {
           nsLayoutUtils::TransformRect(rootFrame, rootScrollFrame, rect);
         }
         intersectionRect = Some(rect);
       }
     }
 
