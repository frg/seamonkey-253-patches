# HG changeset patch
# User Brian Grinstead <bgrinstead@mozilla.com>
# Date 1525202020 25200
#      Tue May 01 12:13:40 2018 -0700
# Node ID eb78f49f99f095346715906321a5730bf647dfec
# Parent  1a3bd8917100b7e1bff091f9b13b5e66ef5f0a4b
Bug 1457963 - Add a --temp-profile argument to `mach run` that creates a new temp profile directory;r=ahal

MozReview-Commit-ID: LI6a9KBNXer

diff --git a/python/mozbuild/mozbuild/mach_commands.py b/python/mozbuild/mozbuild/mach_commands.py
--- a/python/mozbuild/mozbuild/mach_commands.py
+++ b/python/mozbuild/mozbuild/mach_commands.py
@@ -9,16 +9,17 @@ import hashlib
 import itertools
 import json
 import logging
 import operator
 import os
 import re
 import subprocess
 import sys
+import tempfile
 
 from collections import OrderedDict
 
 import mozpack.path as mozpath
 
 from mach.decorators import (
     CommandArgument,
     CommandArgumentGroup,
@@ -817,16 +818,18 @@ class RunProgram(MachCommandBase):
     @CommandArgument('--noprofile', '-n', action='store_true', group=prog_group,
         help='Do not pass the --profile argument by default.')
     @CommandArgument('--disable-e10s', action='store_true', group=prog_group,
         help='Run the program with electrolysis disabled.')
     @CommandArgument('--enable-crash-reporter', action='store_true', group=prog_group,
         help='Run the program with the crash reporter enabled.')
     @CommandArgument('--setpref', action='append', default=[], group=prog_group,
         help='Set the specified pref before starting the program. Can be set multiple times. Prefs can also be set in ~/.mozbuild/machrc in the [runprefs] section - see `./mach settings` for more information.')
+    @CommandArgument('--temp-profile', action='store_true', group=prog_group,
+        help='Run the program using a new temporary profile created inside the objdir.')
 
     @CommandArgumentGroup('debugging')
     @CommandArgument('--debug', action='store_true', group='debugging',
         help='Enable the debugger. Not specifying a --debugger option will result in the default debugger being used.')
     @CommandArgument('--debugger', default=None, type=str, group='debugging',
         help='Name of debugger to use.')
     @CommandArgument('--debugger-args', default=None, metavar='params', type=str,
         group='debugging',
@@ -840,17 +843,17 @@ class RunProgram(MachCommandBase):
         help='Enable DMD. The following arguments have no effect without this.')
     @CommandArgument('--mode', choices=['live', 'dark-matter', 'cumulative', 'scan'], group='DMD',
          help='Profiling mode. The default is \'dark-matter\'.')
     @CommandArgument('--stacks', choices=['partial', 'full'], group='DMD',
         help='Allocation stack trace coverage. The default is \'partial\'.')
     @CommandArgument('--show-dump-stats', action='store_true', group='DMD',
         help='Show stats when doing dumps.')
     def run(self, params, remote, background, noprofile, disable_e10s,
-        enable_crash_reporter, setpref, debug, debugger,
+        enable_crash_reporter, setpref, temp_profile, debug, debugger,
         debugger_args, dmd, mode, stacks, show_dump_stats):
 
         if conditions.is_android(self):
             # Running Firefox for Android is completely different
             if dmd:
                 print("DMD is not supported for Firefox for Android")
                 return 1
             from mozrunner.devices.android_device import verify_android_device, run_firefox_for_android
@@ -889,17 +892,21 @@ class RunProgram(MachCommandBase):
                    'browser.shell.checkDefaultBrowser': False,
                    'general.warnOnAboutConfig': False,
                 }
                 prefs.update(self._mach_context.settings.runprefs)
                 prefs.update([p.split('=', 1) for p in setpref])
                 for pref in prefs:
                     prefs[pref] = Preferences.cast(prefs[pref])
 
-                path = os.path.join(self.topobjdir, 'tmp', 'scratch_user')
+                if (temp_profile):
+                    path = tempfile.mkdtemp(dir=os.path.join(self.topobjdir, 'tmp'), prefix='profile-')
+                else:
+                    path = os.path.join(self.topobjdir, 'tmp', 'profile-default')
+
                 profile = Profile(path, preferences=prefs)
                 args.append('-profile')
                 args.append(profile.profile)
 
             if not no_profile_option_given and setpref:
                 print("setpref is only supported if a profile is not specified")
                 return 1
 
