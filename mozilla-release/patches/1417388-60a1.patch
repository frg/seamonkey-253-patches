# HG changeset patch
# User Chia-Hung Duan<cduan@mozilla.com>
# Date 1512639088 -28800
# Node ID c78c6e1797b1ca3b8004804ce8708799ef5a188a
# Parent  313beab64fa8ad27466b3b35663a59df2958024a
Bug 1417388 - Simplify incremental sweeping on the main thread r=jonco

diff --git a/js/src/gc/ArenaList.h b/js/src/gc/ArenaList.h
--- a/js/src/gc/ArenaList.h
+++ b/js/src/gc/ArenaList.h
@@ -262,23 +262,19 @@ class ArenaLists
 
     // Arena lists which have yet to be swept, but need additional foreground
     // processing before they are swept.
     ZoneGroupData<Arena*> gcShapeArenasToUpdate;
     ZoneGroupData<Arena*> gcAccessorShapeArenasToUpdate;
     ZoneGroupData<Arena*> gcScriptArenasToUpdate;
     ZoneGroupData<Arena*> gcObjectGroupArenasToUpdate;
 
-    // While sweeping type information, these lists save the arenas for the
-    // objects which have already been finalized in the foreground (which must
-    // happen at the beginning of the GC), so that type sweeping can determine
-    // which of the object pointers are marked.
-    ZoneGroupData<ObjectAllocKindArray<ArenaList>> savedObjectArenas_;
-    ArenaList& savedObjectArenas(AllocKind i) { return savedObjectArenas_.ref()[i]; }
-    ZoneGroupData<Arena*> savedEmptyObjectArenas;
+    // The list of empty arenas which are collected during sweep phase and released at the end of
+    // sweeping every sweep group.
+    ZoneGroupData<Arena*> savedEmptyArenas;
 
   public:
     explicit ArenaLists(JSRuntime* rt, ZoneGroup* group);
     ~ArenaLists();
 
     const void* addressOfFreeList(AllocKind thingKind) const {
         return reinterpret_cast<const void*>(&freeLists_.refNoCheck()[thingKind]);
     }
@@ -318,17 +314,17 @@ class ArenaLists
     bool checkEmptyArenaList(AllocKind kind);
 
     bool relocateArenas(JS::Zone* zone, Arena*& relocatedListOut, JS::gcreason::Reason reason,
                         js::SliceBudget& sliceBudget, gcstats::Statistics& stats);
 
     void queueForegroundObjectsForSweep(FreeOp* fop);
     void queueForegroundThingsForSweep(FreeOp* fop);
 
-    void mergeForegroundSweptObjectArenas();
+    void releaseForegroundSweptEmptyArenas();
 
     bool foregroundFinalize(FreeOp* fop, AllocKind thingKind, js::SliceBudget& sliceBudget,
                             SortedArenaList& sweepList);
     static void backgroundFinalize(FreeOp* fop, Arena* listHead, Arena** empty);
 
     // When finalizing arenas, whether to keep empty arenas on the list or
     // release them immediately.
     enum KeepArenasEnum {
@@ -336,17 +332,16 @@ class ArenaLists
         KEEP_ARENAS
     };
 
   private:
     inline void queueForForegroundSweep(FreeOp* fop, const FinalizePhase& phase);
     inline void queueForBackgroundSweep(FreeOp* fop, const FinalizePhase& phase);
     inline void queueForForegroundSweep(FreeOp* fop, AllocKind thingKind);
     inline void queueForBackgroundSweep(FreeOp* fop, AllocKind thingKind);
-    inline void mergeSweptArenas(AllocKind thingKind);
 
     TenuredCell* allocateFromArena(JS::Zone* zone, AllocKind thingKind,
                                    ShouldCheckThresholds checkThresholds);
     inline TenuredCell* allocateFromArenaInner(JS::Zone* zone, Arena* arena, AllocKind kind);
 
     friend class GCRuntime;
     friend class js::Nursery;
     friend class js::TenuringTracer;
diff --git a/js/src/gc/GCRuntime.h b/js/src/gc/GCRuntime.h
--- a/js/src/gc/GCRuntime.h
+++ b/js/src/gc/GCRuntime.h
@@ -1118,17 +1118,17 @@ class GCRuntime
     IncrementalProgress maybeYieldForSweepingZeal(FreeOp* fop, SliceBudget& budget);
 #endif
     bool shouldReleaseObservedTypes();
     void sweepDebuggerOnMainThread(FreeOp* fop);
     void sweepJitDataOnMainThread(FreeOp* fop);
     IncrementalProgress endSweepingSweepGroup(FreeOp* fop, SliceBudget& budget);
     IncrementalProgress performSweepActions(SliceBudget& sliceBudget, AutoLockForExclusiveAccess& lock);
     IncrementalProgress sweepTypeInformation(FreeOp* fop, SliceBudget& budget, Zone* zone);
-    IncrementalProgress mergeSweptObjectArenas(FreeOp* fop, SliceBudget& budget, Zone* zone);
+    IncrementalProgress releaseSweptEmptyArenas(FreeOp* fop, SliceBudget& budget, Zone* zone);
     void startSweepingAtomsTable();
     IncrementalProgress sweepAtomsTable(FreeOp* fop, SliceBudget& budget);
     IncrementalProgress sweepWeakCaches(FreeOp* fop, SliceBudget& budget);
     IncrementalProgress finalizeAllocKind(FreeOp* fop, SliceBudget& budget, Zone* zone,
                                           AllocKind kind);
     IncrementalProgress sweepShapeTree(FreeOp* fop, SliceBudget& budget, Zone* zone);
     void endSweepPhase(bool lastGC, AutoLockForExclusiveAccess& lock);
     bool allCCVisibleZonesWereCollected() const;
diff --git a/js/src/jsgc.cpp b/js/src/jsgc.cpp
--- a/js/src/jsgc.cpp
+++ b/js/src/jsgc.cpp
@@ -2994,18 +2994,17 @@ ArenaLists::ArenaLists(JSRuntime* rt, Zo
     backgroundFinalizeState_(),
     arenaListsToSweep_(),
     incrementalSweptArenaKind(group, AllocKind::LIMIT),
     incrementalSweptArenas(group),
     gcShapeArenasToUpdate(group, nullptr),
     gcAccessorShapeArenasToUpdate(group, nullptr),
     gcScriptArenasToUpdate(group, nullptr),
     gcObjectGroupArenasToUpdate(group, nullptr),
-    savedObjectArenas_(group),
-    savedEmptyObjectArenas(group, nullptr)
+    savedEmptyArenas(group, nullptr)
 {
     for (auto i : AllAllocKinds())
         freeLists(i) = &placeholder;
     for (auto i : AllAllocKinds())
         backgroundFinalizeState(i) = BFS_DONE;
     for (auto i : AllAllocKinds())
         arenaListsToSweep(i) = nullptr;
 }
@@ -3029,19 +3028,17 @@ ArenaLists::~ArenaLists()
          * We can only call this during the shutdown after the last GC when
          * the background finalization is disabled.
          */
         MOZ_ASSERT(backgroundFinalizeState(i) == BFS_DONE);
         ReleaseArenaList(runtime_, arenaLists(i).head(), lock);
     }
     ReleaseArenaList(runtime_, incrementalSweptArenas.ref().head(), lock);
 
-    for (auto i : ObjectAllocKinds())
-        ReleaseArenaList(runtime_, savedObjectArenas(i).head(), lock);
-    ReleaseArenaList(runtime_, savedEmptyObjectArenas, lock);
+    ReleaseArenaList(runtime_, savedEmptyArenas, lock);
 }
 
 void
 ArenaLists::queueForForegroundSweep(FreeOp* fop, const FinalizePhase& phase)
 {
     gcstats::AutoPhase ap(fop->runtime()->gc.stats(), phase.statsPhase);
     for (auto kind : phase.kinds)
         queueForForegroundSweep(fop, kind);
@@ -3126,38 +3123,21 @@ ArenaLists::backgroundFinalize(FreeOp* f
 
         lists->arenaListsToSweep(thingKind) = nullptr;
     }
 
     lists->backgroundFinalizeState(thingKind) = BFS_DONE;
 }
 
 void
-ArenaLists::mergeForegroundSweptObjectArenas()
+ArenaLists::releaseForegroundSweptEmptyArenas()
 {
     AutoLockGC lock(runtime_);
-    ReleaseArenaList(runtime_, savedEmptyObjectArenas, lock);
-    savedEmptyObjectArenas = nullptr;
-
-    mergeSweptArenas(AllocKind::OBJECT0);
-    mergeSweptArenas(AllocKind::OBJECT2);
-    mergeSweptArenas(AllocKind::OBJECT4);
-    mergeSweptArenas(AllocKind::OBJECT8);
-    mergeSweptArenas(AllocKind::OBJECT12);
-    mergeSweptArenas(AllocKind::OBJECT16);
-}
-
-inline void
-ArenaLists::mergeSweptArenas(AllocKind thingKind)
-{
-    ArenaList* al = &arenaLists(thingKind);
-    ArenaList* saved = &savedObjectArenas(thingKind);
-
-    *al = saved->insertListWithCursorAtEnd(*al);
-    saved->clear();
+    ReleaseArenaList(runtime_, savedEmptyArenas, lock);
+    savedEmptyArenas = nullptr;
 }
 
 void
 ArenaLists::queueForegroundThingsForSweep(FreeOp* fop)
 {
     gcShapeArenasToUpdate = arenaListsToSweep(AllocKind::SHAPE);
     gcAccessorShapeArenasToUpdate = arenaListsToSweep(AllocKind::ACCESSOR_SHAPE);
     gcObjectGroupArenasToUpdate = arenaListsToSweep(AllocKind::OBJECT_GROUP);
@@ -3887,37 +3867,43 @@ FOR_EACH_ALLOCKIND(MAKE_CASE)
         MOZ_CRASH("Unknown AllocKind in AllocKindToAscii");
     }
 }
 #endif // DEBUG
 
 bool
 ArenaLists::checkEmptyArenaList(AllocKind kind)
 {
-    size_t num_live = 0;
+    bool isEmpty = true;
 #ifdef DEBUG
+    size_t numLive = 0;
     if (!arenaLists(kind).isEmpty()) {
-        size_t max_cells = 20;
+        isEmpty = false;
+        size_t maxCells = 20;
         char *env = getenv("JS_GC_MAX_LIVE_CELLS");
         if (env && *env)
-            max_cells = atol(env);
+            maxCells = atol(env);
         for (Arena* current = arenaLists(kind).head(); current; current = current->next) {
             for (ArenaCellIterUnderGC i(current); !i.done(); i.next()) {
                 TenuredCell* t = i.getCell();
                 MOZ_ASSERT(t->isMarkedAny(), "unmarked cells should have been finalized");
-                if (++num_live <= max_cells) {
+                if (++numLive <= maxCells) {
                     fprintf(stderr, "ERROR: GC found live Cell %p of kind %s at shutdown\n",
                             t, AllocKindToAscii(kind));
                 }
             }
         }
-        fprintf(stderr, "ERROR: GC found %zu live Cells at shutdown\n", num_live);
+        if (numLive > 0) {
+          fprintf(stderr, "ERROR: GC found %zu live Cells at shutdown\n", numLive);
+        } else {
+          fprintf(stderr, "ERROR: GC found empty Arenas at shutdown\n");
+        }
     }
 #endif // DEBUG
-    return num_live == 0;
+    return isEmpty;
 }
 
 class MOZ_RAII js::gc::AutoRunParallelTask : public GCParallelTask
 {
     gcstats::PhaseKind phase_;
     AutoLockHelperThreadState& lock_;
 
   public:
@@ -5782,43 +5768,39 @@ GCRuntime::beginSweepPhase(JS::gcreason:
     // group.
     safeToYield = false;
 }
 
 bool
 ArenaLists::foregroundFinalize(FreeOp* fop, AllocKind thingKind, SliceBudget& sliceBudget,
                                SortedArenaList& sweepList)
 {
-    MOZ_ASSERT_IF(IsObjectAllocKind(thingKind), savedObjectArenas(thingKind).isEmpty());
-
     if (!arenaListsToSweep(thingKind) && incrementalSweptArenas.ref().isEmpty())
         return true;
 
+    // Empty object arenas are not released until all foreground GC things have
+    // been swept.
     KeepArenasEnum keepArenas = IsObjectAllocKind(thingKind) ? KEEP_ARENAS : RELEASE_ARENAS;
+
     if (!FinalizeArenas(fop, &arenaListsToSweep(thingKind), sweepList,
                         thingKind, sliceBudget, keepArenas))
     {
         incrementalSweptArenaKind = thingKind;
         incrementalSweptArenas = sweepList.toArenaList();
         return false;
     }
 
     // Clear any previous incremental sweep state we may have saved.
     incrementalSweptArenas.ref().clear();
 
-    if (IsObjectAllocKind(thingKind)) {
-        // Delay releasing of object arenas until types have been swept.
-        sweepList.extractEmpty(&savedEmptyObjectArenas.ref());
-        savedObjectArenas(thingKind) = sweepList.toArenaList();
-    } else {
-        // Join |arenaLists[thingKind]| and |sweepList| into a single list.
-        ArenaList finalized = sweepList.toArenaList();
-        arenaLists(thingKind) =
-            finalized.insertListWithCursorAtEnd(arenaLists(thingKind));
-    }
+    if (IsObjectAllocKind(thingKind))
+      sweepList.extractEmpty(&savedEmptyArenas.ref());
+
+    ArenaList finalized = sweepList.toArenaList();
+    arenaLists(thingKind) = finalized.insertListWithCursorAtEnd(arenaLists(thingKind));
 
     return true;
 }
 
 IncrementalProgress
 GCRuntime::drainMarkStack(SliceBudget& sliceBudget, gcstats::PhaseKind phase)
 {
     /* Run a marking slice and return whether the stack is now empty. */
@@ -5892,24 +5874,23 @@ GCRuntime::sweepTypeInformation(FreeOp* 
         gcstats::AutoPhase ap(stats(), gcstats::PhaseKind::SWEEP_TYPES_END);
         zone->types.endSweep(rt);
     }
 
     return Finished;
 }
 
 IncrementalProgress
-GCRuntime::mergeSweptObjectArenas(FreeOp* fop, SliceBudget& budget,
-                                  Zone* zone)
+GCRuntime::releaseSweptEmptyArenas(FreeOp* fop, SliceBudget& budget, Zone* zone)
 {
     // Foreground finalized objects have already been finalized, and now their
     // arenas can be reclaimed by freeing empty ones and making non-empty ones
     // available for allocation.
 
-    zone->arenas.mergeForegroundSweptObjectArenas();
+    zone->arenas.releaseForegroundSweptEmptyArenas();
     return Finished;
 }
 
 void
 GCRuntime::startSweepingAtomsTable()
 {
     auto& maybeAtoms = maybeAtomsToSweep.ref();
     MOZ_ASSERT(maybeAtoms.isNothing());
@@ -6434,27 +6415,24 @@ GCRuntime::initSweepActions()
                 Call(&GCRuntime::endMarkingSweepGroup),
                 Call(&GCRuntime::beginSweepingSweepGroup),
 #ifdef JS_GC_ZEAL
                 Call(&GCRuntime::maybeYieldForSweepingZeal),
 #endif
                 Call(&GCRuntime::sweepAtomsTable),
                 Call(&GCRuntime::sweepWeakCaches),
                 ForEachZoneInSweepGroup(rt,
-                    ForEachAllocKind(ForegroundObjectFinalizePhase.kinds,
-                        Call(&GCRuntime::finalizeAllocKind))),
-                ForEachZoneInSweepGroup(rt,
                     Sequence(
                         Call(&GCRuntime::sweepTypeInformation),
-                        Call(&GCRuntime::mergeSweptObjectArenas))),
-                ForEachZoneInSweepGroup(rt,
-                    ForEachAllocKind(ForegroundNonObjectFinalizePhase.kinds,
-                        Call(&GCRuntime::finalizeAllocKind))),
-                ForEachZoneInSweepGroup(rt,
-                    Call(&GCRuntime::sweepShapeTree)),
+                        ForEachAllocKind(ForegroundObjectFinalizePhase.kinds,
+                                         Call(&GCRuntime::finalizeAllocKind)),
+                        ForEachAllocKind(ForegroundNonObjectFinalizePhase.kinds,
+                                         Call(&GCRuntime::finalizeAllocKind)),
+                        Call(&GCRuntime::sweepShapeTree),
+                        Call(&GCRuntime::releaseSweptEmptyArenas))),
                 Call(&GCRuntime::endSweepingSweepGroup)));
 
     return sweepActions != nullptr;
 }
 
 IncrementalProgress
 GCRuntime::performSweepActions(SliceBudget& budget, AutoLockForExclusiveAccess& lock)
 {
