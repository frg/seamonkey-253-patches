# HG changeset patch
# User Liang-Heng Chen <xeonchen@gmail.com>
# Date 1566309631 0
# Node ID c4beb93536583415da4c05c3d27154d38d8e4970
# Parent  b07fb8d4a1bf412ae8f57a11a4ee54c506613517
Bug 1560574 - use FormatPRExplodedTime to display GMT; r=kershaw

Differential Revision: https://phabricator.services.mozilla.com/D36502

diff --git a/netwerk/streamconv/converters/nsFTPDirListingConv.cpp b/netwerk/streamconv/converters/nsFTPDirListingConv.cpp
--- a/netwerk/streamconv/converters/nsFTPDirListingConv.cpp
+++ b/netwerk/streamconv/converters/nsFTPDirListingConv.cpp
@@ -295,17 +295,17 @@ nsFTPDirListingConv::DigestBufferLines(c
         result.fe_time.tm_params.tp_dst_offset = 0;
         PR_NormalizeTime(&result.fe_time, PR_GMTParameters);
 
         // Note: The below is the RFC822/1123 format, as required by
         // the application/http-index-format specs
         // viewers of such a format can then reformat this into the
         // current locale (or anything else they choose)
         PR_FormatTimeUSEnglish(buffer, sizeof(buffer),
-                               "%a, %d %b %Y %H:%M:%S", &result.fe_time );
+                               "%a, %d %b %Y %H:%M:%S GMT", &result.fe_time);
 
         nsAutoCString escaped;
         Unused << NS_WARN_IF(!NS_Escape(nsDependentCString(buffer), escaped, url_Path));
         aString.Append(escaped);
         aString.Append(' ');
 
         // ENTRY TYPE
         if (type == 'd')
diff --git a/netwerk/streamconv/converters/nsIndexedToHTML.cpp b/netwerk/streamconv/converters/nsIndexedToHTML.cpp
--- a/netwerk/streamconv/converters/nsIndexedToHTML.cpp
+++ b/netwerk/streamconv/converters/nsIndexedToHTML.cpp
@@ -18,16 +18,17 @@
 #include "nsURLHelper.h"
 #include "nsIPrefService.h"
 #include "nsIPrefBranch.h"
 #include "nsIPrefLocalizedString.h"
 #include "nsIStringBundle.h"
 #include "nsITextToSubURI.h"
 #include "nsNativeCharsetUtils.h"
 #include "nsString.h"
+#include "nsContentUtils.h"
 #include <algorithm>
 #include "nsIChannel.h"
 
 using mozilla::intl::LocaleService;
 
 NS_IMPL_ISUPPORTS(nsIndexedToHTML,
                   nsIDirIndexListener,
                   nsIStreamConverter,
@@ -659,16 +660,34 @@ NS_IMETHODIMP
 nsIndexedToHTML::OnDataAvailable(nsIRequest *aRequest,
                                  nsISupports *aCtxt,
                                  nsIInputStream* aInput,
                                  uint64_t aOffset,
                                  uint32_t aCount) {
     return mParser->OnDataAvailable(aRequest, aCtxt, aInput, aOffset, aCount);
 }
 
+static nsresult FormatTime(const nsDateFormatSelector aDateFormatSelector,
+                           const nsTimeFormatSelector aTimeFormatSelector,
+                           const PRTime aPrTime, nsAString& aStringOut) {
+  // FormatPRExplodedTime will use GMT based formatted string (e.g. GMT+1)
+  // instead of local time zone name (e.g. CEST).
+  // To avoid this case when ResistFingerprinting is disabled, use
+  // |FormatPRTime| to show exact time zone name.
+  if (!nsContentUtils::ShouldResistFingerprinting()) {
+    return mozilla::DateTimeFormat::FormatPRTime(
+        aDateFormatSelector, aTimeFormatSelector, aPrTime, aStringOut);
+  }
+
+  PRExplodedTime prExplodedTime;
+  PR_ExplodeTime(aPrTime, PR_GMTParameters, &prExplodedTime);
+  return mozilla::DateTimeFormat::FormatPRExplodedTime(
+      aDateFormatSelector, aTimeFormatSelector, &prExplodedTime, aStringOut);
+}
+
 NS_IMETHODIMP
 nsIndexedToHTML::OnIndexAvailable(nsIRequest *aRequest,
                                   nsISupports *aCtxt,
                                   nsIDirIndex *aIndex) {
     nsresult rv;
     if (!aIndex)
         return NS_ERROR_NULL_POINTER;
 
@@ -806,26 +825,20 @@ nsIndexedToHTML::OnIndexAvailable(nsIReq
 
     if (t == -1LL) {
         pushBuffer.AppendLiteral("></td>\n <td>");
     } else {
         pushBuffer.AppendLiteral(" sortable-data=\"");
         pushBuffer.AppendInt(static_cast<int64_t>(t));
         pushBuffer.AppendLiteral("\">");
         nsAutoString formatted;
-        mozilla::DateTimeFormat::FormatPRTime(kDateFormatShort,
-                                              kTimeFormatNone,
-                                              t,
-                                              formatted);
+        FormatTime(kDateFormatShort, kTimeFormatNone, t, formatted);
         AppendNonAsciiToNCR(formatted, pushBuffer);
         pushBuffer.AppendLiteral("</td>\n <td>");
-        mozilla::DateTimeFormat::FormatPRTime(kDateFormatNone,
-                                              kTimeFormatSeconds,
-                                              t,
-                                              formatted);
+        FormatTime(kDateFormatNone, kTimeFormatSeconds, t, formatted);
         // use NCR to show date in any doc charset
         AppendNonAsciiToNCR(formatted, pushBuffer);
     }
 
     pushBuffer.AppendLiteral("</td>\n</tr>");
 
     return SendToListener(aRequest, aCtxt, pushBuffer);
 }
diff --git a/netwerk/test/unit/test_bug365133.js b/netwerk/test/unit/test_bug365133.js
--- a/netwerk/test/unit/test_bug365133.js
+++ b/netwerk/test/unit/test_bug365133.js
@@ -1,66 +1,66 @@
 const URL = "ftp://localhost/bug365133/";
 
 const tests = [
   [ /* Unix style listing, space at the end of filename */
     "drwxrwxr-x    2 500      500          4096 Jan 01  2000 a \r\n"
   ,
     "300: " + URL + "\n" +
     "200: filename content-length last-modified file-type\n" +
-    "201: \"a%20\" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 DIRECTORY \n"
+    '201: "a%20" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT DIRECTORY \n',
   ],
   [ /* Unix style listing, space at the end of link name */
     "lrwxrwxrwx    1 500      500             2 Jan 01  2000 l  -> a \r\n"
   ,
     "300: " + URL + "\n" +
     "200: filename content-length last-modified file-type\n" +
-    "201: \"l%20\" 2 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 SYMBOLIC-LINK \n"
+    '201: "l%20" 2 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT SYMBOLIC-LINK \n',
   ],
   [ /* Unix style listing, regular file with " -> " in name */
     "-rw-rw-r--    1 500      500             0 Jan 01  2000 arrow -> in name \r\n"
   ,
     "300: " + URL + "\n" +
     "200: filename content-length last-modified file-type\n" +
-    "201: \"arrow%20-%3E%20in%20name%20\" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 FILE \n"
+    '201: "arrow%20-%3E%20in%20name%20" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT FILE \n',
   ],
   [ /* Unix style listing, tab at the end of filename */
     "drwxrwxrwx    2 500      500          4096 Jan 01  2000 t	\r\n"
   ,
     "300: " + URL + "\n" +
     "200: filename content-length last-modified file-type\n" +
-    "201: \"t%09\" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 DIRECTORY \n"
+    '201: "t%09" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT DIRECTORY \n',
   ],
   [ /* Unix style listing, multiple " -> " in filename */
     "lrwxrwxrwx    1 500      500            26 Jan 01  2000 symlink with arrow -> in name -> file with arrow -> in name\r\n"
   ,
     "300: " + URL + "\n" +
     "200: filename content-length last-modified file-type\n" +
-    "201: \"symlink%20with%20arrow%20-%3E%20in%20name\" 26 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 SYMBOLIC-LINK \n"
+    '201: "symlink%20with%20arrow%20-%3E%20in%20name" 26 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT SYMBOLIC-LINK \n',
   ],
   [ /* Unix style listing, multiple " -> " in filename, incorrect filesize */
     "lrwxrwxrwx    1 500      500             0 Jan 01  2000 symlink with arrow -> in name -> file with arrow -> in name\r\n"
   ,
     "300: " + URL + "\n" +
     "200: filename content-length last-modified file-type\n" +
-    "201: \"symlink%20with%20arrow%20-%3E%20in%20name%20-%3E%20file%20with%20arrow\" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 SYMBOLIC-LINK \n"
+    '201: "symlink%20with%20arrow%20-%3E%20in%20name%20-%3E%20file%20with%20arrow" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT SYMBOLIC-LINK \n',
   ],
   [ /* DOS style listing, space at the end of filename, year 1999 */
     "01-01-99  01:00AM                 1024 file \r\n"
   ,
     "300: " + URL + "\n" +
     "200: filename content-length last-modified file-type\n" +
-    "201: \"file%20\" 1024 Fri%2C%2001%20Jan%201999%2001%3A00%3A00 FILE \n"
+    '201: "file%20" 1024 Fri%2C%2001%20Jan%201999%2001%3A00%3A00%20GMT FILE \n',
   ],
   [ /* DOS style listing, tab at the end of filename, year 2000 */
     "01-01-00  01:00AM                 1024 file	\r\n"
   ,
     "300: " + URL + "\n" +
     "200: filename content-length last-modified file-type\n" +
-    "201: \"file%09\" 1024 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 FILE \n"
+    '201: "file%09" 1024 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT FILE \n',
   ]
 ]
 
 function checkData(request, data, ctx) {
   Assert.equal(tests[0][1], data);
   tests.shift();
   executeSoon(next_test);
 }
diff --git a/netwerk/test/unit/test_bug484684.js b/netwerk/test/unit/test_bug484684.js
--- a/netwerk/test/unit/test_bug484684.js
+++ b/netwerk/test/unit/test_bug484684.js
@@ -2,71 +2,76 @@ const URL = "ftp://localhost/bug464884/"
 
 const tests = [
   // standard ls unix format
   ["-rw-rw-r--    1 500      500             0 Jan 01  2000 file1\r\n" +
    "-rw-rw-r--    1 500      500             0 Jan 01  2000  file2\r\n",
 
    "300: " + URL + "\n" +
    "200: filename content-length last-modified file-type\n" +
-   "201: \"file1\" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 FILE \n" +
-   "201: \"%20file2\" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 FILE \n"],
+   '201: "file1" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT FILE \n' +
+   '201: "%20file2" 0 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT FILE \n',
+  ],
   // old Hellsoft unix format
   ["-[RWCEMFA] supervisor         214059       Jan 01  2000    file1\r\n" +
    "-[RWCEMFA] supervisor         214059       Jan 01  2000     file2\r\n",
 
    "300: " + URL + "\n" +
    "200: filename content-length last-modified file-type\n" +
-   "201: \"file1\" 214059 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 FILE \n" +
-   "201: \"file2\" 214059 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 FILE \n"],
+   '201: "file1" 214059 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT FILE \n' +
+   '201: "file2" 214059 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT FILE \n',
+  ],
   // new Hellsoft unix format
   ["- [RWCEAFMS] jrd                    192 Jan 01  2000 file1\r\n"+
    "- [RWCEAFMS] jrd                    192 Jan 01  2000  file2\r\n",
 
    "300: " + URL + "\n" +
    "200: filename content-length last-modified file-type\n" +
-   "201: \"file1\" 192 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 FILE \n" +
-   "201: \"%20file2\" 192 Sat%2C%2001%20Jan%202000%2000%3A00%3A00 FILE \n"],
+   '201: "file1" 192 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT FILE \n' +
+   '201: "%20file2" 192 Sat%2C%2001%20Jan%202000%2000%3A00%3A00%20GMT FILE \n',
+  ],
   // DOS format with correct offsets
   ["01-01-00  01:00AM       <DIR>          dir1\r\n" +
    "01-01-00  01:00AM       <JUNCTION>     junction1 -> foo1\r\n" +
    "01-01-00  01:00AM                95077 file1\r\n" +
    "01-01-00  01:00AM       <DIR>           dir2\r\n" +
    "01-01-00  01:00AM       <JUNCTION>      junction2 ->  foo2\r\n" +
    "01-01-00  01:00AM                95077  file2\r\n",
 
    "300: " + URL + "\n" +
    "200: filename content-length last-modified file-type\n" +
-   "201: \"dir1\" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 DIRECTORY \n" +
-   "201: \"junction1\"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00 SYMBOLIC-LINK \n" +
-   "201: \"file1\" 95077 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 FILE \n" +
-   "201: \"%20dir2\" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 DIRECTORY \n" +
-   "201: \"%20junction2\"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00 SYMBOLIC-LINK \n" +
-   "201: \"%20file2\" 95077 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 FILE \n"],
+   '201: "dir1" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT DIRECTORY \n' +
+   '201: "junction1"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT SYMBOLIC-LINK \n' +
+   '201: "file1" 95077 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT FILE \n' +
+   '201: "%20dir2" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT DIRECTORY \n' +
+   '201: "%20junction2"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT SYMBOLIC-LINK \n' +
+   '201: "%20file2" 95077 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT FILE \n',
+  ],
   // DOS format with wrong offsets
   ["01-01-00  01:00AM       <DIR>       dir1\r\n" +
    "01-01-00  01:00AM     <DIR>             dir2\r\n" +
    "01-01-00  01:00AM   <DIR>                  dir3\r\n" +
    "01-01-00  01:00AM       <JUNCTION>  junction1 -> foo1\r\n" +
    "01-01-00  01:00AM     <JUNCTION>        junction2 ->  foo2\r\n" +
    "01-01-00  01:00AM   <JUNCTION>             junction3 ->  foo3\r\n" +
    "01-01-00  01:00AM               95077  file1\r\n" +
    "01-01-00  01:00AM        95077 file2\r\n",
 
    "300: " + URL + "\n" +
    "200: filename content-length last-modified file-type\n" +
-   "201: \"dir1\" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 DIRECTORY \n" +
-   "201: \"dir2\" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 DIRECTORY \n" +
-   "201: \"dir3\" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 DIRECTORY \n" +
-   "201: \"junction1\"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00 SYMBOLIC-LINK \n" +
-   "201: \"junction2\"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00 SYMBOLIC-LINK \n" +
-   "201: \"junction3\"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00 SYMBOLIC-LINK \n" +
-   "201: \"file1\" 95077 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 FILE \n" +
-   "201: \"file2\" 95077 Sat%2C%2001%20Jan%202000%2001%3A00%3A00 FILE \n"]
-]
+   '201: "dir1" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT DIRECTORY \n' +
+   '201: "dir2" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT DIRECTORY \n' +
+   '201: "dir3" 0 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT DIRECTORY \n' +
+   '201: "junction1"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT SYMBOLIC-LINK \n' +
+   '201: "junction2"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT SYMBOLIC-LINK \n' +
+   '201: "junction3"  Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT SYMBOLIC-LINK \n' +
+   '201: "file1" 95077 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT FILE \n' +
+   '201: "file2" 95077 Sat%2C%2001%20Jan%202000%2001%3A00%3A00%20GMT FILE \n',
+  ],
+];
 
 function checkData(request, data, ctx) {
   Assert.equal(tests[0][1], data);
   tests.shift();
   executeSoon(next_test);
 }
 
 function storeData(status, entry) {
diff --git a/netwerk/test/unit/test_bug515583.js b/netwerk/test/unit/test_bug515583.js
--- a/netwerk/test/unit/test_bug515583.js
+++ b/netwerk/test/unit/test_bug515583.js
@@ -3,27 +3,26 @@ const URL = "ftp://localhost/bug515583/"
 const tests = [
   ["[RWCEM1 4 1-MAR-1993 18:09:01.12\r\n" +
    "[RWCEM1] 4 2-MAR-1993 18:09:01.12\r\n" +
    "[RWCEM1]A 4 3-MAR-1993 18:09:01.12\r\n" +
    "[RWCEM1]B; 4 4-MAR-1993 18:09:01.12\r\n" +
    "[RWCEM1];1 4 5-MAR-1993 18:09:01.12\r\n" +
    "[RWCEM1]; 4 6-MAR-1993 18:09:01.12\r\n" +
    "[RWCEM1]C;1D 4 7-MAR-1993 18:09:01.12\r\n" +
-   "[RWCEM1]E;1 4 8-MAR-1993 18:09:01.12\r\n"
-  ,
+   "[RWCEM1]E;1 4 8-MAR-1993 18:09:01.12\r\n",
    "300: " + URL + "\n" +
    "200: filename content-length last-modified file-type\n" +
-   "201: \"A\" 2048 Wed%2C%2003%20Mar%201993%2018%3A09%3A01 FILE \n" +
-   "201: \"E\" 2048 Mon%2C%2008%20Mar%201993%2018%3A09%3A01 FILE \n"]
-   ,
-   ["\r\r\r\n"
-   ,
+   '201: "A" 2048 Wed%2C%2003%20Mar%201993%2018%3A09%3A01%20GMT FILE \n' +
+   '201: "E" 2048 Mon%2C%2008%20Mar%201993%2018%3A09%3A01%20GMT FILE \n',
+  ],
+  ["\r\r\r\n",
    "300: " + URL + "\n" +
-   "200: filename content-length last-modified file-type\n"]
+   "200: filename content-length last-modified file-type\n"
+  ],
 ]
 
 function checkData(request, data, ctx) {
   Assert.equal(tests[0][1], data);
   tests.shift();
   executeSoon(next_test);
 }
 
diff --git a/netwerk/test/unit/test_bug543805.js b/netwerk/test/unit/test_bug543805.js
--- a/netwerk/test/unit/test_bug543805.js
+++ b/netwerk/test/unit/test_bug543805.js
@@ -10,40 +10,42 @@ const tests = [
    "-rw-r--r--   1 0                22 Jan  1 20:19  test.blankfile\r\n" +
    "-rw-r--r--   1 0                33 Apr  1 2008   test2.blankfile\r\n" +
    "-rw-r--r--   1 0                44 Jan  1 20:19 nodup.file\r\n" +
    "-rw-r--r--   1 0                55 Jan  1 20:19 test.file\r\n" +
    "-rw-r--r--   1 0                66 Apr  1 2008  test2.file\r\n",
 
    "300: " + URL + "\n" +
    "200: filename content-length last-modified file-type\n" +
-   "201: \"%20nodup.file\" 11 " + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00 FILE \n" +
-   "201: \"%20test.blankfile\" 22 " + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00 FILE \n" +
-   "201: \"%20test2.blankfile\" 33 Tue%2C%2001%20Apr%202008%2000%3A00%3A00 FILE \n" +
-   "201: \"nodup.file\" 44 " + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00 FILE \n" +
-   "201: \"test.file\" 55 " + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00 FILE \n" +
-   "201: \"test2.file\" 66 Tue%2C%2001%20Apr%202008%2000%3A00%3A00 FILE \n"],
+   '201: "%20nodup.file" 11 ' + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00%20GMT FILE \n" +
+   '201: "%20test.blankfile" 22 ' + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00%20GMT FILE \n" +
+   '201: "%20test2.blankfile" 33 Tue%2C%2001%20Apr%202008%2000%3A00%3A00%20GMT FILE \n' +
+   '201: "nodup.file" 44 ' + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00%20GMT FILE \n" +
+   '201: "test.file" 55 ' + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00%20GMT FILE \n" +
+   '201: "test2.file" 66 Tue%2C%2001%20Apr%202008%2000%3A00%3A00%20GMT FILE \n'
+  ],
 
   // standard ls format
   [
    "-rw-r--r--    1 500      500            11 Jan  1 20:19  nodup.file\r\n" +
    "-rw-r--r--    1 500      500            22 Jan  1 20:19  test.blankfile\r\n" +
    "-rw-r--r--    1 500      500            33 Apr  1  2008  test2.blankfile\r\n" +
    "-rw-r--r--    1 500      500            44 Jan  1 20:19 nodup.file\r\n" +
    "-rw-r--r--    1 500      500            55 Jan  1 20:19 test.file\r\n" +
    "-rw-r--r--    1 500      500            66 Apr  1  2008 test2.file\r\n",
 
    "300: " + URL + "\n" +
    "200: filename content-length last-modified file-type\n" +
-   "201: \"%20nodup.file\" 11 " + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00 FILE \n" +
-   "201: \"%20test.blankfile\" 22 " + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00 FILE \n" +
-   "201: \"%20test2.blankfile\" 33 Tue%2C%2001%20Apr%202008%2000%3A00%3A00 FILE \n" +
-   "201: \"nodup.file\" 44 " + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00 FILE \n" +
-   "201: \"test.file\" 55 " + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00 FILE \n" +
-   "201: \"test2.file\" 66 Tue%2C%2001%20Apr%202008%2000%3A00%3A00 FILE \n"]
+   '201: "%20nodup.file" 11 ' + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00%20GMT FILE \n" +
+   '201: "%20test.blankfile" 22 ' + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00%20GMT FILE \n" +
+   '201: "%20test2.blankfile" 33 Tue%2C%2001%20Apr%202008%2000%3A00%3A00%20GMT FILE \n' +
+   '201: "nodup.file" 44 ' + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00%20GMT FILE \n" +
+   '201: "test.file" 55 ' + day + "%2C%2001%20Jan%20" + year + "%2020%3A19%3A00%20GMT FILE \n" +
+   '201: "test2.file" 66 Tue%2C%2001%20Apr%202008%2000%3A00%3A00%20GMT FILE \n',
+  ],
 ]
 
 function checkData(request, data, ctx) {
   Assert.equal(tests[0][1], data);
   tests.shift();
   executeSoon(next_test);
 }
 
