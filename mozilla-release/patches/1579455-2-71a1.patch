# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1568652775 0
# Node ID f8e7b946ed2640524449ebaadaea3833dce84963
# Parent  be1f63b8035b0e7a1b2c92b41eaac09fe397d38e
Bug 1579455 - [mozbuild] Fix output issues with Python 3 in virtualenv.py r=nalexander

Differential Revision: https://phabricator.services.mozilla.com/D45418

diff --git a/python/mozbuild/mozbuild/virtualenv.py b/python/mozbuild/mozbuild/virtualenv.py
--- a/python/mozbuild/mozbuild/virtualenv.py
+++ b/python/mozbuild/mozbuild/virtualenv.py
@@ -21,16 +21,18 @@ IS_MSYS2 = (sys.platform == 'win32' and 
 IS_CYGWIN = (sys.platform == 'cygwin')
 
 # Minimum versions of Python required to build.
 MINIMUM_PYTHON_VERSIONS = {
     2: LooseVersion('2.7.3'),
     3: LooseVersion('3.5.0')
 }
 
+PY2 = sys.version_info[0] == 2
+PY3 = sys.version_info[0] == 3
 
 UPGRADE_WINDOWS = '''
 Please upgrade to the latest MozillaBuild development environment. See
 https://developer.mozilla.org/en-US/docs/Developer_Guide/Build_Instructions/Windows_Prerequisites
 '''.lstrip()
 
 UPGRADE_OTHER = '''
 Run |mach bootstrap| to ensure your system is up to date.
@@ -213,17 +215,18 @@ class VirtualenvManager(object):
             raise Exception(
                 'Failed to create virtualenv: %s' % self.virtualenv_root)
 
         self.write_exe_info(python)
 
         return self.virtualenv_root
 
     def packages(self):
-        with open(self.manifest_path, 'rU') as fh:
+        mode = 'rU' if PY2 else 'r'
+        with open(self.manifest_path, mode) as fh:
             packages = [line.rstrip().split(':')
                         for line in fh]
         return packages
 
     def populate(self):
         """Populate the virtualenv.
 
         The manifest file consists of colon-delimited fields. The first field
@@ -341,18 +344,17 @@ class VirtualenvManager(object):
                 for_win = not package[0].startswith('!')
                 is_win = sys.platform == 'win32'
                 if is_win == for_win:
                     handle_package(package[1:])
                 return True
 
             if package[0] in ('python2', 'python3'):
                 for_python3 = package[0].endswith('3')
-                is_python3 = sys.version_info[0] > 2
-                if is_python3 == for_python3:
+                if PY3 == for_python3:
                     handle_package(package[1:])
                 return True
 
             if package[0] == 'objdir':
                 assert len(package) == 2
                 path = os.path.join(self.topobjdir, package[1])
 
                 with open(os.path.join(python_lib, 'objdir.pth'), 'a') as f:
@@ -493,17 +495,17 @@ class VirtualenvManager(object):
         """Activate the virtualenv in this Python context.
 
         If you run a random Python script and wish to "activate" the
         virtualenv, you can simply instantiate an instance of this class
         and call .ensure() and .activate() to make the virtualenv active.
         """
 
         exec(open(self.activate_path).read(), dict(__file__=self.activate_path))
-        if sys.version_info[0] < 3 and isinstance(os.environ['PATH'], unicode):
+        if PY2 and isinstance(os.environ['PATH'], unicode):
             os.environ['PATH'] = os.environ['PATH'].encode('utf-8')
 
     def install_pip_package(self, package, vendored=False):
         """Install a package via pip.
 
         The supplied package is specified using a pip requirement specifier.
         e.g. 'foo' or 'foo==1.0'.
 
@@ -571,17 +573,18 @@ class VirtualenvManager(object):
         # It's tempting to call pip natively via pip.main(). However,
         # the current Python interpreter may not be the virtualenv python.
         # This will confuse pip and cause the package to attempt to install
         # against the executing interpreter. By creating a new process, we
         # force the virtualenv's interpreter to be used and all is well.
         # It /might/ be possible to cheat and set sys.executable to
         # self.python_path. However, this seems more risk than it's worth.
         pip = os.path.join(self.bin_path, 'pip')
-        subprocess.check_call([pip] + args, stderr=subprocess.STDOUT, cwd=self.topsrcdir)
+        subprocess.check_call([pip] + args, stderr=subprocess.STDOUT, cwd=self.topsrcdir,
+                              universal_newlines=PY3)
 
     def activate_pipenv(self, pipfile=None, populate=False, python=None):
         """Activate a virtual environment managed by pipenv
 
         If ``pipfile`` is not ``None`` then the Pipfile located at the path
         provided will be used to create the virtual environment. If
         ``populate`` is ``True`` then the virtual environment will be
         populated from the manifest file. The optional ``python`` argument
