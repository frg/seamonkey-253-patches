# HG changeset patch
# User Geoff Brown <whole.grains@protonmail.com>
# Date 1601310893 0
# Node ID c6ad9a8b1152800d31e17bb690bfa30ad6c7bd78
# Parent  ba49f27fcde46c427349f2bab85b7ebb68517573
Bug 1638974 - Convert mochitest runtests.py to run with Python 3; r=bc

Initial changes to allow mochitests to run in python 3. This works well on Linux,
and almost works on Android. A follow-up will complete the work on Android and
switch mach mochitest over to python 3.

Differential Revision: https://phabricator.services.mozilla.com/D91461

diff --git a/testing/mochitest/mach_commands.py b/testing/mochitest/mach_commands.py
--- a/testing/mochitest/mach_commands.py
+++ b/testing/mochitest/mach_commands.py
@@ -4,16 +4,17 @@
 
 from __future__ import absolute_import, unicode_literals
 
 from argparse import Namespace
 from collections import defaultdict
 import functools
 import logging
 import os
+import six
 import sys
 import warnings
 
 from mozbuild.base import (
     MachCommandBase,
     MachCommandConditions as conditions,
     MozbuildObject,
 )
@@ -267,24 +268,24 @@ class MachCommands(MachCommandBase):
         buildapp = None
         for app in SUPPORTED_APPS:
             if conditions.is_buildapp_in(self, apps=[app]):
                 buildapp = app
                 break
 
         flavors = None
         if flavor:
-            for fname, fobj in ALL_FLAVORS.iteritems():
+            for fname, fobj in six.iteritems(ALL_FLAVORS):
                 if flavor in fobj['aliases']:
                     if buildapp not in fobj['enabled_apps']:
                         continue
                     flavors = [fname]
                     break
         else:
-            flavors = [f for f, v in ALL_FLAVORS.iteritems() if buildapp in v['enabled_apps']]
+            flavors = [f for f, v in six.iteritems(ALL_FLAVORS) if buildapp in v['enabled_apps']]
 
         from mozbuild.controller.building import BuildDriver
         self._ensure_state_subdir_exists('.')
 
         test_paths = kwargs['test_paths']
         kwargs['test_paths'] = []
 
         mochitest = self._spawn(MochitestRunner)
diff --git a/testing/mochitest/runtests.py b/testing/mochitest/runtests.py
--- a/testing/mochitest/runtests.py
+++ b/testing/mochitest/runtests.py
@@ -191,18 +191,18 @@ class MessageLogger(object):
                 if test.startswith(prefix):
                     message['test'] = test[len(prefix):]
                     break
 
     def _fix_message_format(self, message):
         if 'message' in message:
             if isinstance(message['message'], bytes):
                 message['message'] = message['message'].decode('utf-8', 'replace')
-            elif not isinstance(message['message'], unicode):
-                message['message'] = unicode(message['message'])
+            elif not isinstance(message['message'], six.text_type):
+                message['message'] = six.text_type(message['message'])
 
     def parse_line(self, line):
         """Takes a given line of input (structured or not) and
         returns a list of structured messages"""
         line = line.rstrip().decode("UTF-8", "replace")
 
         messages = []
         for fragment in line.split(MessageLogger.DELIMITER):
@@ -781,17 +781,20 @@ def findTestMediaDevices(log):
 
     pactl = spawn.find_executable("pactl")
     pacmd = spawn.find_executable("pacmd")
 
     # Use pactl to see if the PulseAudio module-null-sink module is loaded.
     def null_sink_loaded():
         o = subprocess.check_output(
             [pactl, 'list', 'short', 'modules'])
-        return filter(lambda x: 'module-null-sink' in x, o.splitlines())
+
+        null_sink = [x for x in o.splitlines() if 'module-null-sink' in x]
+
+        return null_sink
 
     if not null_sink_loaded():
         # Load module-null-sink
         subprocess.check_call([pactl, 'load-module',
                                'module-null-sink'])
 
     if not null_sink_loaded():
         log.error('Couldn\'t load module-null-sink')
@@ -1573,36 +1576,31 @@ toolbar#nav-bar {
                 if patterns:
                     testob['expected'] = patterns
             paths.append(testob)
 
         # The 'prefs' key needs to be set in the DEFAULT section, unfortunately
         # we can't tell what comes from DEFAULT or not. So to validate this, we
         # stash all prefs from tests in the same manifest into a set. If the
         # length of the set > 1, then we know 'prefs' didn't come from DEFAULT.
-        pref_not_default = [m for m, p in self.prefs_by_manifest.iteritems() if len(p) > 1]
+        pref_not_default = [m for m, p in six.iteritems(self.prefs_by_manifest) if len(p) > 1]
         if pref_not_default:
             self.log.error("The 'prefs' key must be set in the DEFAULT section of a "
                            "manifest. Fix the following manifests: {}".format(
                             '\n'.join(pref_not_default)))
             sys.exit(1)
         # The 'environment' key needs to be set in the DEFAULT section too.
-        env_not_default = [m for m, p in self.env_vars_by_manifest.iteritems() if len(p) > 1]
+        env_not_default = [m for m, p in six.iteritems(self.env_vars_by_manifest) if len(p) > 1]
         if env_not_default:
             self.log.error("The 'environment' key must be set in the DEFAULT section of a "
                            "manifest. Fix the following manifests: {}".format(
                             '\n'.join(env_not_default)))
             sys.exit(1)
 
-        def path_sort(ob1, ob2):
-            path1 = ob1['path'].split('/')
-            path2 = ob2['path'].split('/')
-            return cmp(path1, path2)
-
-        paths.sort(path_sort)
+        paths.sort(key=lambda p: p['path'].split('/'))
         if options.dump_tests:
             options.dump_tests = os.path.expanduser(options.dump_tests)
             assert os.path.exists(os.path.dirname(options.dump_tests))
             with open(options.dump_tests, 'w') as dumpFile:
                 dumpFile.write(json.dumps({'active_tests': paths}))
 
             self.log.info("Dumping active_tests to %s file." % options.dump_tests)
             sys.exit()
@@ -1650,17 +1648,17 @@ toolbar#nav-bar {
         options.logFile = options.logFile.replace("\\", "\\\\")
 
         if "MOZ_HIDE_RESULTS_TABLE" in os.environ and os.environ[
                 "MOZ_HIDE_RESULTS_TABLE"] == "1":
             options.hideResultsTable = True
 
         # strip certain unnecessary items to avoid serialization errors in json.dumps()
         d = dict((k, v) for k, v in options.__dict__.items() if (v is None) or
-                 isinstance(v, (basestring, numbers.Number)))
+                 isinstance(v, (six.string_types, numbers.Number)))
         d['testRoot'] = self.testRoot
         if options.jscov_dir_prefix:
             d['jscovDirPrefix'] = options.jscov_dir_prefix
         if not options.keep_open:
             d['closeWhenDone'] = '1'
         content = json.dumps(d)
 
         with open(os.path.join(options.profilePath, "testConfig.js"), "w") as config:
@@ -1773,34 +1771,36 @@ toolbar#nav-bar {
                     # may not be able to access process info for all processes
                     continue
         else:
             def _psInfo(line):
                 if pname in line:
                     self.log.info(line)
 
             process = mozprocess.ProcessHandler(['ps', '-f'],
-                                                processOutputLine=_psInfo)
+                                                processOutputLine=_psInfo,
+                                                universal_newlines=True)
             process.run()
             process.wait()
 
             def _psKill(line):
                 parts = line.split()
                 if len(parts) == 3 and parts[0].isdigit():
                     pid = int(parts[0])
                     ppid = int(parts[1])
                     if parts[2] == pname:
                         if ppid == 1 or not orphans:
                             self.log.info("killing %s (pid %d)" % (pname, pid))
                             killPid(pid, self.log)
                         else:
                             self.log.info("NOT killing %s (pid %d) (not an orphan?)" %
                                           (pname, pid))
             process = mozprocess.ProcessHandler(['ps', '-o', 'pid,ppid,comm'],
-                                                processOutputLine=_psKill)
+                                                processOutputLine=_psKill,
+                                                universal_newlines=True)
             process.run()
             process.wait()
 
     def execute_start_script(self):
         if not self.start_script or not self.marionette:
             return
 
         if os.path.isfile(self.start_script):
@@ -1920,18 +1920,17 @@ toolbar#nav-bar {
         extensions = self.getExtensionsToInstall(options)
 
         # Whitelist the _tests directory (../..) so that TESTING_JS_MODULES work
         tests_dir = os.path.dirname(os.path.dirname(SCRIPT_DIR))
         sandbox_whitelist_paths = [tests_dir] + options.sandboxReadWhitelist
         if (platform.system() == "Linux" or
             platform.system() in ("Windows", "Microsoft")):
             # Trailing slashes are needed to indicate directories on Linux and Windows
-            sandbox_whitelist_paths = map(lambda p: os.path.join(p, ""),
-                                          sandbox_whitelist_paths)
+            sandbox_whitelist_paths = [os.path.join(p, "") for p in sandbox_whitelist_paths]
 
         # Create the profile
         self.profile = Profile(profile=options.profilePath,
                                addons=extensions,
                                locations=self.locations,
                                proxy=self.proxy(options),
                                whitelistpaths=sandbox_whitelist_paths,
                                )
@@ -2512,17 +2511,17 @@ toolbar#nav-bar {
             result = result or (-2 if self.countfail > 0 else 0)
             self.message_logger.finish()
             return result
 
         def step2():
             stepOptions = copy.deepcopy(options)
             stepOptions.repeat = 0
             stepOptions.keep_open = False
-            for i in xrange(VERIFY_REPEAT_SINGLE_BROWSER):
+            for i in range(VERIFY_REPEAT_SINGLE_BROWSER):
                 stepOptions.profilePath = None
                 self.urlOpts = []
                 result = self.runTests(stepOptions)
                 result = result or (-2 if self.countfail > 0 else 0)
                 self.message_logger.finish()
                 if result != 0:
                     break
             return result
@@ -2539,17 +2538,17 @@ toolbar#nav-bar {
             self.message_logger.finish()
             return result
 
         def step4():
             stepOptions = copy.deepcopy(options)
             stepOptions.repeat = 0
             stepOptions.keep_open = False
             stepOptions.environment.append("MOZ_CHAOSMODE=3")
-            for i in xrange(VERIFY_REPEAT_SINGLE_BROWSER):
+            for i in range(VERIFY_REPEAT_SINGLE_BROWSER):
                 stepOptions.profilePath = None
                 self.urlOpts = []
                 result = self.runTests(stepOptions)
                 result = result or (-2 if self.countfail > 0 else 0)
                 self.message_logger.finish()
                 if result != 0:
                     break
             return result
@@ -3120,17 +3119,17 @@ toolbar#nav-bar {
                 self.shutdownLeaks.log(message)
             return message
 
 
 def run_test_harness(parser, options):
     parser.validate(options)
 
     logger_options = {
-        key: value for key, value in vars(options).iteritems()
+        key: value for key, value in six.iteritems(vars(options))
         if key.startswith('log') or key == 'valgrind'}
 
     runner = MochitestDesktop(options.flavor, logger_options, quiet=options.quiet)
 
     if hasattr(options, 'log'):
         delattr(options, 'log')
 
     options.runByManifest = False
