# HG changeset patch
# User Botond Ballo <botond@mozilla.com>
# Date 1520285735 18000
# Node ID 6409e2ef806792557a01b47a883f11d218ec8b3a
# Parent  09864119e5f36535a2a2196fc47d14500c96b190
Bug 1442767 - Rename Box to RectAbsolute (Moz2D changes). r=bas

MozReview-Commit-ID: 7VOZs8pQgJ4

diff --git a/gfx/2d/Box.h b/gfx/2d/RectAbsolute.h
rename from gfx/2d/Box.h
rename to gfx/2d/RectAbsolute.h
--- a/gfx/2d/Box.h
+++ b/gfx/2d/RectAbsolute.h
@@ -1,50 +1,50 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
-#ifndef MOZILLA_GFX_BOX_H_
-#define MOZILLA_GFX_BOX_H_
+#ifndef MOZILLA_GFX_RECT_ABSOLUTE_H_
+#define MOZILLA_GFX_RECT_ABSOLUTE_H_
 
 #include <algorithm>
 #include <cstdint>
 
 #include "mozilla/Attributes.h"
 #include "Rect.h"
 #include "Types.h"
 
 namespace mozilla {
 
 template <typename> struct IsPixel;
 
 namespace gfx {
 
 /**
- * A Box is similar to a Rect (see BaseRect.h), but represented as
+ * A RectAbsolute is similar to a Rect (see BaseRect.h), but represented as
  * (x1, y1, x2, y2) instead of (x, y, width, height).
  *
  * Unless otherwise indicated, methods on this class correspond
  * to methods on BaseRect.
  *
  * The API is currently very bare-bones; it may be extended as needed.
  *
  * Do not use this class directly. Subclass it, pass that subclass as the
  * Sub parameter, and only use that subclass.
  */
 template <class T, class Sub, class Rect>
-struct BaseBox {
+struct BaseRectAbsolute {
 protected:
   T x1, y1, x2, y2;
 
 public:
-  BaseBox() : x1(0), y1(0), x2(0), y2(0) {}
-  BaseBox(T aX1, T aY1, T aX2, T aY2) :
+  BaseRectAbsolute() : x1(0), y1(0), x2(0), y2(0) {}
+  BaseRectAbsolute(T aX1, T aY1, T aX2, T aY2) :
     x1(aX1), y1(aY1), x2(aX2), y2(aY2) {}
 
   MOZ_ALWAYS_INLINE T X() const { return x1; }
   MOZ_ALWAYS_INLINE T Y() const { return y1; }
   MOZ_ALWAYS_INLINE T Width() const { return x2 - x1; }
   MOZ_ALWAYS_INLINE T Height() const { return y2 - y1; }
   MOZ_ALWAYS_INLINE T XMost() const { return x2; }
   MOZ_ALWAYS_INLINE T YMost() const { return y2; }
@@ -70,56 +70,56 @@ public:
     y2 = aY2;
   }
 
   static Sub FromRect(const Rect& aRect)
   {
     return Sub(aRect.x, aRect.y, aRect.XMost(), aRect.YMost());
   }
 
-  MOZ_MUST_USE Sub Intersect(const Sub& aBox) const
+  MOZ_MUST_USE Sub Intersect(const Sub& aOther) const
   {
     Sub result;
-    result.x1 = std::max<T>(x1, aBox.x1);
-    result.y1 = std::max<T>(y1, aBox.y1);
-    result.x2 = std::min<T>(x2, aBox.x2);
-    result.y2 = std::min<T>(y2, aBox.y2);
+    result.x1 = std::max<T>(x1, aOther.x1);
+    result.y1 = std::max<T>(y1, aOther.y1);
+    result.x2 = std::min<T>(x2, aOther.x2);
+    result.y2 = std::min<T>(y2, aOther.y2);
     return result;
   }
 
-  bool IsEqualEdges(const Sub& aBox) const
+  bool IsEqualEdges(const Sub& aOther) const
   {
-    return x1 == aBox.x1 && y1 == aBox.y1 &&
-           x2 == aBox.x2 && y2 == aBox.y2;
+    return x1 == aOther.x1 && y1 == aOther.y1 &&
+           x2 == aOther.x2 && y2 == aOther.y2;
   }
 };
 
 template <class Units>
-struct IntBoxTyped :
-    public BaseBox<int32_t, IntBoxTyped<Units>, IntRectTyped<Units>>,
+struct IntRectAbsoluteTyped :
+    public BaseRectAbsolute<int32_t, IntRectAbsoluteTyped<Units>, IntRectTyped<Units>>,
     public Units {
   static_assert(IsPixel<Units>::value,
                 "'units' must be a coordinate system tag");
-  typedef BaseBox<int32_t, IntBoxTyped<Units>, IntRectTyped<Units>> Super;
+  typedef BaseRectAbsolute<int32_t, IntRectAbsoluteTyped<Units>, IntRectTyped<Units>> Super;
   typedef IntParam<int32_t> ToInt;
 
-  IntBoxTyped() : Super() {}
-  IntBoxTyped(ToInt aX1, ToInt aY1, ToInt aX2, ToInt aY2) :
+  IntRectAbsoluteTyped() : Super() {}
+  IntRectAbsoluteTyped(ToInt aX1, ToInt aY1, ToInt aX2, ToInt aY2) :
       Super(aX1.value, aY1.value, aX2.value, aY2.value) {}
 };
 
 template <class Units>
-struct BoxTyped :
-    public BaseBox<Float, BoxTyped<Units>, RectTyped<Units>>,
+struct RectAbsoluteTyped :
+    public BaseRectAbsolute<Float, RectAbsoluteTyped<Units>, RectTyped<Units>>,
     public Units {
   static_assert(IsPixel<Units>::value,
                 "'units' must be a coordinate system tag");
-  typedef BaseBox<Float, BoxTyped<Units>, RectTyped<Units>> Super;
+  typedef BaseRectAbsolute<Float, RectAbsoluteTyped<Units>, RectTyped<Units>> Super;
 
-  BoxTyped() : Super() {}
-  BoxTyped(Float aX1, Float aY1, Float aX2, Float aY2) :
+  RectAbsoluteTyped() : Super() {}
+  RectAbsoluteTyped(Float aX1, Float aY1, Float aX2, Float aY2) :
       Super(aX1, aY1, aX2, aY2) {}
 };
 
 }
 }
 
-#endif /* MOZILLA_GFX_BOX_H_ */
+#endif /* MOZILLA_GFX_RECT_ABSOLUTE_H_ */
diff --git a/gfx/2d/moz.build b/gfx/2d/moz.build
--- a/gfx/2d/moz.build
+++ b/gfx/2d/moz.build
@@ -15,17 +15,16 @@ EXPORTS.mozilla.gfx += [
     'BasePoint.h',
     'BasePoint3D.h',
     'BasePoint4D.h',
     'BaseRect.h',
     'BaseSize.h',
     'BezierUtils.h',
     'Blur.h',
     'BorrowedContext.h',
-    'Box.h',
     'Coord.h',
     'CriticalSection.h',
     'DataSurfaceHelpers.h',
     'DrawEventRecorder.h',
     'DrawTargetRecording.h',
     'DrawTargetTiled.h',
     'DrawTargetWrapAndRecord.h',
     'Filters.h',
@@ -45,16 +44,17 @@ EXPORTS.mozilla.gfx += [
     'PathHelpers.h',
     'PatternHelpers.h',
     'Point.h',
     'Polygon.h',
     'Quaternion.h',
     'RecordedEvent.h',
     'RecordingTypes.h',
     'Rect.h',
+    'RectAbsolute.h',
     'Scale.h',
     'ScaleFactor.h',
     'ScaleFactors2D.h',
     'SourceSurfaceCairo.h',
     'SourceSurfaceCapture.h',
     'SourceSurfaceRawData.h',
     'StackArray.h',
     'Swizzle.h',
