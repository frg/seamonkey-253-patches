# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1586273909 0
# Node ID 46cc9d8517dd9ef1b612ce0841e359cd038b606f
# Parent  26f0d53f8eb8ff47ce35eda74fa85f89a6e7c109
Bug 1627163 - Avoid scanning ConfigureSandbox._implied_options when handling implied options. r=firefox-build-system-reviewers,rstewart

ConfigureSandbox._implied_options is a list of ReadOnlyNamespaces.
In python 3.5, ReadOnlyNamespaces end up with no guarantee in the order
of their __dict__. So when comparing

   ReadOnlyNamespace(a=1, b=2)

and

   ReadOnlyNamespace(a=3, b=4)

It's not guaranteed that the a's are compared before the b's.

In ConfigureSandbox._implied_options, some of those ReadOnlyNamespace
fields are SandboxDependsFunctions, which actually raise an error when
they're being compared, because we don't want that to happen in the
sandbox.

So when using python 3, configure would randomly fail when trying to
remove items from the ConfigureSandbox._implied_options list because
removing an item from a list scans the list to find the first element
that matches.

And ConfigureSandbox._implied_options needs to be ordered, which is why
it's currently a list.

So instead of removing by value, we create a new list with the remaining
values. But because the loop recurses, and needs the updated list, we
filter first.

Differential Revision: https://phabricator.services.mozilla.com/D69535

diff --git a/python/mozbuild/mozbuild/configure/__init__.py b/python/mozbuild/mozbuild/configure/__init__.py
--- a/python/mozbuild/mozbuild/configure/__init__.py
+++ b/python/mozbuild/mozbuild/configure/__init__.py
@@ -547,21 +547,26 @@ class ConfigureSandbox(dict):
     def _value_for_depends(self, obj):
         value = obj.result()
         self._logger.log(TRACE, '%r = %r', obj, value)
         return value
 
     @memoize
     def _value_for_option(self, option):
         implied = {}
-        for implied_option in self._implied_options[:]:
-            if implied_option.name not in (option.name, option.env):
-                continue
-            self._implied_options.remove(implied_option)
+        matching_implied_options = [
+            o for o in self._implied_options if o.name in (option.name, option.env)
+        ]
+        # Update self._implied_options before going into the loop with the non-matching
+        # options.
+        self._implied_options = [
+            o for o in self._implied_options if o.name not in (option.name, option.env)
+        ]
 
+        for implied_option in matching_implied_options:
             if (implied_option.when and
                 not self._value_for(implied_option.when)):
                 continue
 
             value = self._resolve(implied_option.value)
 
             if value is not None:
                 value = OptionValue.from_(value)
