# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1527710090 14400
# Node ID 437b901404dbea22d9fc7fdf8c3d363adbdf20f3
# Parent  7e71f13390fe0d4c76082af44fcbaa8598dad4a8
Bug 1457590 - Add the HitTestingTreeNodeAutoLock class. r=botond

This adds the HitTestingTreeNodeAutoLock RAII class that allows using
a HitTestingTreeNode safely outside a tree lock, and ensures that the
node won't get destroyed or recycled concurrently.

MozReview-Commit-ID: 8Tb3vdIeUgr

diff --git a/gfx/layers/apz/src/HitTestingTreeNode.cpp b/gfx/layers/apz/src/HitTestingTreeNode.cpp
--- a/gfx/layers/apz/src/HitTestingTreeNode.cpp
+++ b/gfx/layers/apz/src/HitTestingTreeNode.cpp
@@ -21,16 +21,17 @@ namespace layers {
 
 using gfx::CompositorHitTestInfo;
 
 HitTestingTreeNode::HitTestingTreeNode(AsyncPanZoomController* aApzc,
                                        bool aIsPrimaryHolder,
                                        LayersId aLayersId)
   : mApzc(aApzc)
   , mIsPrimaryApzcHolder(aIsPrimaryHolder)
+  , mLocked(false)
   , mLayersId(aLayersId)
   , mScrollbarAnimationId(0)
   , mFixedPosTarget(FrameMetrics::NULL_SCROLL_ID)
   , mIsBackfaceHidden(false)
   , mOverride(EventRegionsOverride::NoOverride)
 {
 if (mIsPrimaryApzcHolder) {
     MOZ_ASSERT(mApzc);
@@ -70,17 +71,17 @@ HitTestingTreeNode::Destroy()
     }
     mApzc = nullptr;
   }
 }
 
 bool
 HitTestingTreeNode::IsRecyclable(const RecursiveMutexAutoLock& aProofOfTreeLock)
 {
-  return !IsPrimaryHolder();
+  return !(IsPrimaryHolder() || mLocked);
 }
 
 void
 HitTestingTreeNode::SetLastChild(HitTestingTreeNode* aChild)
 {
   mLastChild = aChild;
   if (aChild) {
     aChild->mParent = this;
@@ -403,10 +404,63 @@ HitTestingTreeNode::SetApzcParent(AsyncP
   MOZ_ASSERT(GetApzc() != nullptr);
   if (IsPrimaryHolder()) {
     GetApzc()->SetParent(aParent);
   } else {
     MOZ_ASSERT(GetApzc()->GetParent() == aParent);
   }
 }
 
+void
+HitTestingTreeNode::Lock(const RecursiveMutexAutoLock& aProofOfTreeLock)
+{
+  MOZ_ASSERT(!mLocked);
+  mLocked = true;
+}
+
+void
+HitTestingTreeNode::Unlock(const RecursiveMutexAutoLock& aProofOfTreeLock)
+{
+  MOZ_ASSERT(mLocked);
+  mLocked = false;
+}
+
+HitTestingTreeNodeAutoLock::HitTestingTreeNodeAutoLock()
+  : mTreeMutex(nullptr)
+{
+}
+
+HitTestingTreeNodeAutoLock::~HitTestingTreeNodeAutoLock()
+{
+  Clear();
+}
+
+void
+HitTestingTreeNodeAutoLock::Initialize(const RecursiveMutexAutoLock& aProofOfTreeLock,
+                                       already_AddRefed<HitTestingTreeNode> aNode,
+                                       RecursiveMutex& aTreeMutex)
+{
+  MOZ_ASSERT(!mNode);
+
+  mNode = aNode;
+  mTreeMutex = &aTreeMutex;
+
+  mNode->Lock(aProofOfTreeLock);
+}
+
+void
+HitTestingTreeNodeAutoLock::Clear()
+{
+  if (!mNode) {
+    return;
+  }
+  MOZ_ASSERT(mTreeMutex);
+
+  { // scope lock
+    RecursiveMutexAutoLock lock(*mTreeMutex);
+    mNode->Unlock(lock);
+  }
+  mNode = nullptr;
+  mTreeMutex = nullptr;
+}
+
 } // namespace layers
 } // namespace mozilla
diff --git a/gfx/layers/apz/src/HitTestingTreeNode.h b/gfx/layers/apz/src/HitTestingTreeNode.h
--- a/gfx/layers/apz/src/HitTestingTreeNode.h
+++ b/gfx/layers/apz/src/HitTestingTreeNode.h
@@ -43,16 +43,23 @@ class AsyncPanZoomController;
  * the tree.
  *
  * The reason this tree exists at all is so that we can do hit-testing on the
  * thread that we receive input on (referred to the as the controller thread in
  * APZ terminology), which may be different from the compositor thread.
  * Accessing the compositor layer tree can only be done on the compositor
  * thread, and so it is simpler to make a copy of the hit-testing related
  * properties into a separate tree.
+ *
+ * The tree pointers on the node (mLastChild, etc.) can only be manipulated
+ * while holding the APZ tree lock. Any code that wishes to use a
+ * HitTestingTreeNode outside of holding the tree lock should do so by using
+ * the HitTestingTreeNodeAutoLock wrapper, which prevents the node from
+ * being recycled (and also holds a RefPtr to the node to prevent it from
+ * getting freed).
  */
 class HitTestingTreeNode {
   NS_INLINE_DECL_THREADSAFE_REFCOUNTING(HitTestingTreeNode);
 
 private:
   ~HitTestingTreeNode();
 public:
   HitTestingTreeNode(AsyncPanZoomController* aApzc, bool aIsPrimaryHolder,
@@ -132,24 +139,31 @@ public:
   EventRegionsOverride GetEventRegionsOverride() const;
   const CSSTransformMatrix& GetTransform() const;
   const LayerIntRegion& GetVisibleRegion() const;
 
   /* Debug helpers */
   void Dump(const char* aPrefix = "") const;
 
 private:
+  friend class HitTestingTreeNodeAutoLock;
+  // Functions that are private but called from HitTestingTreeNodeAutoLock
+  void Lock(const RecursiveMutexAutoLock& aProofOfTreeLock);
+  void Unlock(const RecursiveMutexAutoLock& aProofOfTreeLock);
+
+
   void SetApzcParent(AsyncPanZoomController* aApzc);
 
   RefPtr<HitTestingTreeNode> mLastChild;
   RefPtr<HitTestingTreeNode> mPrevSibling;
   RefPtr<HitTestingTreeNode> mParent;
 
   RefPtr<AsyncPanZoomController> mApzc;
   bool mIsPrimaryApzcHolder;
+  bool mLocked;
 
   LayersId mLayersId;
 
   // This is only set to non-zero if WebRender is enabled, and only for HTTNs
   // where IsScrollThumbNode() returns true. It holds the animation id that we
   // use to move the thumb node to reflect async scrolling.
   uint64_t mScrollbarAnimationId;
 
@@ -188,12 +202,39 @@ private:
    * present. This value is in L's ParentLayerPixels. */
   Maybe<ParentLayerIntRegion> mClipRegion;
 
   /* Indicates whether or not the event regions on this node need to be
    * overridden in a certain way. */
   EventRegionsOverride mOverride;
 };
 
+/**
+ * A class that allows safe usage of a HitTestingTreeNode outside of the APZ
+ * tree lock. In general, this class should be Initialize()'d inside the tree
+ * lock (enforced by the proof-of-lock to Initialize), and then can be returned
+ * to a scope outside the tree lock and used safely. Upon destruction or
+ * Clear() being called, it unlocks the underlying node at which point it can
+ * be recycled or freed.
+ */
+class MOZ_RAII HitTestingTreeNodeAutoLock
+{
+public:
+  HitTestingTreeNodeAutoLock();
+  HitTestingTreeNodeAutoLock(const HitTestingTreeNodeAutoLock&) = delete;
+  HitTestingTreeNodeAutoLock& operator=(const HitTestingTreeNodeAutoLock&) = delete;
+  HitTestingTreeNodeAutoLock(HitTestingTreeNodeAutoLock&&) = delete;
+  ~HitTestingTreeNodeAutoLock();
+
+  void Initialize(const RecursiveMutexAutoLock& aProofOfTreeLock,
+                  already_AddRefed<HitTestingTreeNode> aNode,
+                  RecursiveMutex& aTreeMutex);
+  void Clear();
+
+private:
+  RefPtr<HitTestingTreeNode> mNode;
+  RecursiveMutex* mTreeMutex;
+};
+
 } // namespace layers
 } // namespace mozilla
 
 #endif // mozilla_layers_HitTestingTreeNode_h
