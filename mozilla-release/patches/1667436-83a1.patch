# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1601916701 0
# Node ID dc6b76f01a2d7f12c6ddb71bc0dcb5d7892102e2
# Parent  4ab3141fc0f6551f3099afae3671c764ff34c8db
Bug 1667436 - Merge content of `docs/setup/mach.rst` into existing documentation r=firefox-build-system-reviewers,mhentges DONTBUILD

This document was imported from MDN and contained very outdated/incorrect information, and much of the information here is duplicated from the existing `mach` documentation. For the little content that isn't already expressed in the existing documentation in a better way, merge it into `python/mach/docs`.

The unique content is mainly in the FAQ, so I added a new page for that.

Differential Revision: https://phabricator.services.mozilla.com/D91455

diff --git a/python/mach/docs/faq.rst b/python/mach/docs/faq.rst
new file mode 100644
--- /dev/null
+++ b/python/mach/docs/faq.rst
@@ -0,0 +1,85 @@
+.. _mach_faq:
+
+==========================
+Frequently Asked Questions
+==========================
+
+How do I report bugs?
+---------------------
+
+Bugs against the ``mach`` core can be filed in Bugzilla in the `Firefox
+Build System::Mach
+Core <https://bugzilla.mozilla.org/enter_bug.cgi?product=Firefox%20Build%20System&component=Mach%20Core>`__ component.
+
+.. note::
+
+   Most ``mach`` bugs are bugs in individual commands, not bugs in the core
+   ``mach`` code. Bugs for individual commands should be filed against the
+   component that command is related to. For example, bugs in the
+   *build* command should be filed against *Firefox Build System ::
+   General*. Bugs against testing commands should be filed somewhere in
+   the *Testing* product.
+
+
+Is ``mach`` a build system?
+---------------------------
+
+No. ``mach`` is just a generic command dispatching tool that happens to have
+a few commands that interact with the real build system. Historically,
+``mach`` *was* born to become a better interface to the build system.
+However, its potential beyond just build system interaction was quickly
+realized and ``mach`` grew to fit those needs.
+
+How do I add features to ``mach``?
+----------------------------------
+If you would like to add a new feature to ``mach`` that cannot be implemented as
+a ``mach`` command, the first step is to file a bug in the
+``Firefox Build System :: Mach Core`` component.
+
+Should I implement X as a ``mach`` command?
+-------------------------------------------
+
+There are no hard or fast rules. Generally speaking, if you have some
+piece of functionality or action that is useful to multiple people
+(especially if it results in productivity wins), then you should
+consider implementing a ``mach`` command for it.
+
+Some other cases where you should consider implementing something as a
+mach command:
+
+-  When your tool is a random script in the tree. Random scripts are
+   hard to find and may not conform to coding conventions or best
+   practices. Mach provides a framework in which your tool can live that
+   will put it in a better position to succeed than if it were on its
+   own.
+-  When the alternative is a ``make`` target. The build team generally does
+   not like one-off ``make`` targets that aren't part of building (read:
+   compiling) the tree. This includes things related to testing and
+   packaging. These weigh down ``Makefiles`` and add to the burden of
+   maintaining the build system. Instead, you are encouraged to
+   implement ancillary functionality in Python. If you do implement something
+   in Python, hooking it up to ``mach`` is often trivial.
+
+
+How does ``mach`` fit into the modules system?
+----------------------------------------------
+
+Mozilla operates with a `modules governance
+system <https://www.mozilla.org/about/governance/policies/module-ownership/>`__ where
+there are different components with different owners. There is not
+currently a ``mach`` module. There may or may never be one; currently ``mach``
+is owned by the build team.
+
+Even if a ``mach`` module were established, ``mach`` command modules would
+likely never belong to it. Instead, ``mach`` command modules are owne by the
+team/module that owns the system they interact with. In other words, ``mach``
+is not a power play to consolidate authority for tooling. Instead, it aims to
+expose that tooling through a common, shared interface.
+
+
+Who do I contact for help or to report issues?
+----------------------------------------------
+
+You can ask questions in
+`#build <https://chat.mozilla.org/#/room/#build:mozilla.org>`__.
+
diff --git a/python/mach/docs/index.rst b/python/mach/docs/index.rst
--- a/python/mach/docs/index.rst
+++ b/python/mach/docs/index.rst
@@ -79,8 +79,9 @@ best fit for you.
    :maxdepth: 1
    :hidden:
 
    usage
    commands
    driver
    logging
    settings
+   faq
diff --git a/python/mach/docs/usage.rst b/python/mach/docs/usage.rst
--- a/python/mach/docs/usage.rst
+++ b/python/mach/docs/usage.rst
@@ -105,10 +105,17 @@ The settings file follows the ``ini`` fo
     eslint = lint -l eslint
 
     [build]
     telemetry = true
 
     [try]
     default = fuzzy
 
+Adding ``mach`` to your ``PATH``
+--------------------------------
+
+If you don't like having to type ``./mach``, you can add your source directory
+to your ``PATH``. DO NOT copy the script to a directory already in your
+``PATH``.
+
 
 .. _bash completion: https://searchfox.org/mozilla-central/source/python/mach/bash-completion.sh
