# HG changeset patch
# User Mike Shal <mshal@mozilla.com>
# Date 1558397987 0
# Node ID 4a3fcb8ad12cd7db72b7c805e4b07534760c95b4
# Parent  750c3b626d3683e5a2f6752666518a57ac1f3198
Bug 1552672 - Use is_tarfile() rather than is_zipfile() in mar repackaging; r=Callek

The zipfile.is_zipfile() function is overly lenient, in that non-zip
files that contain the four bytes "PK\005\006" near the end of the file
are reported as zip files even if the zip structure is not valid.
Occasionally, our target.tar.bz2 files randomly contain those 4 bytes at
the expected location, which causes the mar repackaging code to try to
process the package as a zip file instead of a tar file.

The tarfile.is_tarfile() logic looks a little more robust in that it
actually tries to open the file, so if we try tar first before falling
back to zip we should be a little more resilient.

Differential Revision: https://phabricator.services.mozilla.com/D31908

diff --git a/python/mozbuild/mozbuild/repackaging/mar.py b/python/mozbuild/mozbuild/repackaging/mar.py
--- a/python/mozbuild/mozbuild/repackaging/mar.py
+++ b/python/mozbuild/mozbuild/repackaging/mar.py
@@ -26,26 +26,26 @@ def repackage_mar(topsrcdir, package, ma
         raise Exception("Package file %s is not a valid .zip or .tar file." % package)
     if arch and arch not in _BCJ_OPTIONS:
         raise Exception("Unknown architecture {}, available architectures: {}".format(
             arch, _BCJ_OPTIONS.keys()))
 
     ensureParentDir(output)
     tmpdir = tempfile.mkdtemp()
     try:
-        if zipfile.is_zipfile(package):
+        if tarfile.is_tarfile(package):
+            z = tarfile.open(package)
+            z.extractall(tmpdir)
+            filelist = z.getnames()
+            z.close()
+        else:
             z = zipfile.ZipFile(package)
             z.extractall(tmpdir)
             filelist = z.namelist()
             z.close()
-        else:
-            z = tarfile.open(package)
-            z.extractall(tmpdir)
-            filelist = z.getnames()
-            z.close()
 
         toplevel_dirs = set([mozpath.split(f)[0] for f in filelist])
         excluded_stuff = set([' ', '.background', '.DS_Store', '.VolumeIcon.icns'])
         toplevel_dirs = toplevel_dirs - excluded_stuff
         # Make sure the .zip file just contains a directory like 'firefox/' at
         # the top, and find out what it is called.
         if len(toplevel_dirs) != 1:
             raise Exception("Package file is expected to have a single top-level directory"
