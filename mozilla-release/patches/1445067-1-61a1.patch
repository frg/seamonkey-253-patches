# HG changeset patch
# User Dan Glastonbury <dan.glastonbury@gmail.com>
# Date 1524022963 -36000
# Node ID 884c9dd8ec73a1d9a3fd33ad965f2ad5953a2a8e
# Parent  62296404c5b0152ed20d2fc55edfd948c9917cfc
Bug 1445067 - P1: Update audioipc to commit 219a811. r=kinetik

This pulls in the fixes to shutdown RPC channels correctly when all
client proxies are dropped. This stops leaking fd and shmem.

MozReview-Commit-ID: 8Kb0iFPn8Pc

diff --git a/media/audioipc/README_MOZILLA b/media/audioipc/README_MOZILLA
--- a/media/audioipc/README_MOZILLA
+++ b/media/audioipc/README_MOZILLA
@@ -1,8 +1,8 @@
 The source from this directory was copied from the audioipc-2
 git repository using the update.sh script.  The only changes
 made were those applied by update.sh and the addition of
 Makefile.in build files for the Mozilla build system.
 
 The audioipc-2 git repository is: https://github.com/djg/audioipc-2.git
 
-The git commit ID used was b93386611d7d9689c4f0177a4704f0adc16bc2d1 (2018-03-09 14:45:24 +1000)
+The git commit ID used was 219a811b62b83bd6e4cb54ae6aebc56bbb43203c (2018-04-18 13:57:59 +1200)
diff --git a/media/audioipc/audioipc/Cargo.toml b/media/audioipc/audioipc/Cargo.toml
--- a/media/audioipc/audioipc/Cargo.toml
+++ b/media/audioipc/audioipc/Cargo.toml
@@ -1,11 +1,11 @@
 [package]
 name = "audioipc"
-version = "0.2.3"
+version = "0.2.4"
 authors = [
         "Matthew Gregan <kinetik@flim.org>",
         "Dan Glastonbury <dan.glastonbury@gmail.com>"
         ]
 description = "Remote Cubeb IPC"
 
 [dependencies]
 cubeb = "0.5.2"
diff --git a/media/audioipc/audioipc/src/core.rs b/media/audioipc/audioipc/src/core.rs
--- a/media/audioipc/audioipc/src/core.rs
+++ b/media/audioipc/audioipc/src/core.rs
@@ -42,17 +42,17 @@ pub struct CoreThread {
 impl CoreThread {
     pub fn remote(&self) -> Remote {
         self.remote.clone()
     }
 }
 
 impl Drop for CoreThread {
     fn drop(&mut self) {
-        trace!("Shutting down {:?}", self);
+        debug!("Shutting down {:?}", self);
         if let Some(inner) = self.inner.take() {
             let _ = inner.shutdown.send(());
             drop(inner.join.join());
         }
     }
 }
 
 impl fmt::Debug for CoreThread {
diff --git a/media/audioipc/audioipc/src/fd_passing.rs b/media/audioipc/audioipc/src/fd_passing.rs
--- a/media/audioipc/audioipc/src/fd_passing.rs
+++ b/media/audioipc/audioipc/src/fd_passing.rs
@@ -80,17 +80,17 @@ pub struct FramedWithFds<A, C> {
 }
 
 impl<A, C> FramedWithFds<A, C>
 where
     A: AsyncSendMsg,
 {
     // If there is a buffered frame, try to write it to `A`
     fn do_write(&mut self) -> Poll<(), io::Error> {
-        debug!("do_write...");
+        trace!("do_write...");
         // Create a frame from any pending message in `write_buf`.
         if !self.write_buf.is_empty() {
             self.set_frame(None);
         }
 
         trace!("pending frames: {:?}", self.frames);
 
         let mut processed = 0;
@@ -136,18 +136,17 @@ where
                         drop(frame.msgs.split_to(n));
                         self.frames.push_front(frame);
                         break;
                     }
                 }
                 _ => panic!(),
             }
         }
-        debug!("process {} frames", processed);
-
+        trace!("process {} frames", processed);
         trace!("pending frames: {:?}", self.frames);
 
         Ok(().into())
     }
 
     fn set_frame(&mut self, fds: Option<Bytes>) {
         if self.write_buf.is_empty() {
             assert!(fds.is_none());
diff --git a/media/audioipc/audioipc/src/messages.rs b/media/audioipc/audioipc/src/messages.rs
--- a/media/audioipc/audioipc/src/messages.rs
+++ b/media/audioipc/audioipc/src/messages.rs
@@ -113,16 +113,17 @@ impl From<DeviceInfo> for ffi::cubeb_dev
             min_rate: info.min_rate,
 
             latency_lo: info.latency_lo,
             latency_hi: info.latency_hi,
         }
     }
 }
 
+#[repr(C)]
 #[derive(Clone, Copy, Debug, Deserialize, Serialize)]
 pub struct StreamParams {
     pub format: ffi::cubeb_sample_format,
     pub rate: c_uint,
     pub channels: c_uint,
     pub layout: ffi::cubeb_channel_layout,
     pub prefs: ffi::cubeb_stream_prefs,
 }
diff --git a/media/audioipc/audioipc/src/rpc/driver.rs b/media/audioipc/audioipc/src/rpc/driver.rs
--- a/media/audioipc/audioipc/src/rpc/driver.rs
+++ b/media/audioipc/audioipc/src/rpc/driver.rs
@@ -85,16 +85,17 @@ where
             match try!(self.handler.produce()) {
                 Async::Ready(Some(message)) => {
                     trace!("  --> got message");
                     try!(self.process_outgoing(message));
                 }
                 Async::Ready(None) => {
                     trace!("  --> got None");
                     // The service is done with the connection.
+                    self.run = false;
                     break;
                 }
                 // Nothing to dispatch
                 Async::NotReady => break,
             }
         }
 
         Ok(())
@@ -134,16 +135,17 @@ where
 
         // Handle completed responses
         try!(self.send_outgoing());
 
         // Try flushing buffered writes
         try!(self.flush());
 
         if self.is_done() {
+            trace!("  --> is done.");
             return Ok(().into());
         }
 
         // Tick again later
         Ok(Async::NotReady)
     }
 }
 
diff --git a/media/audioipc/client/src/context.rs b/media/audioipc/client/src/context.rs
--- a/media/audioipc/client/src/context.rs
+++ b/media/audioipc/client/src/context.rs
@@ -37,16 +37,19 @@ macro_rules! t(
         match $e {
             Ok(e) => e,
             Err(_) => return Err(Error::default())
         }
     ));
 
 pub const CLIENT_OPS: Ops = capi_new!(ClientContext, ClientStream);
 
+// ClientContext's layout *must* match cubeb.c's `struct cubeb` for the
+// common fields.
+#[repr(C)]
 pub struct ClientContext {
     _ops: *const Ops,
     rpc: rpc::ClientProxy<ServerMessage, ClientMessage>,
     core: core::CoreThread,
     cpu_pool: CpuPool,
 }
 
 impl ClientContext {
@@ -222,17 +225,17 @@ impl ContextOps for ClientContext {
         fn opt_stream_params(p: Option<&StreamParamsRef>) -> Option<messages::StreamParams> {
             match p {
                 Some(p) => Some(messages::StreamParams::from(p)),
                 None => None,
             }
         }
 
         let stream_name = match stream_name {
-            Some(s) => Some(s.to_bytes().to_vec()),
+            Some(s) => Some(s.to_bytes_with_nul().to_vec()),
             None => None,
         };
 
         let input_stream_params = opt_stream_params(input_stream_params);
         let output_stream_params = opt_stream_params(output_stream_params);
 
         let init_params = messages::StreamInitParams {
             stream_name: stream_name,
@@ -253,17 +256,17 @@ impl ContextOps for ClientContext {
     ) -> Result<()> {
         assert_not_in_callback();
         Ok(())
     }
 }
 
 impl Drop for ClientContext {
     fn drop(&mut self) {
-        debug!("ClientContext drop...");
+        debug!("ClientContext dropped...");
         let _ = send_recv!(self.rpc(), ClientDisconnect => ClientDisconnected);
         unsafe {
             if G_SERVER_FD.is_some() {
                 libc::close(super::G_SERVER_FD.take().unwrap());
             }
         }
     }
 }
diff --git a/media/audioipc/client/src/stream.rs b/media/audioipc/client/src/stream.rs
--- a/media/audioipc/client/src/stream.rs
+++ b/media/audioipc/client/src/stream.rs
@@ -35,21 +35,25 @@ impl Drop for Device {
             }
             if !self.0.output_name.is_null() {
                 let _ = CString::from_raw(self.0.output_name as *mut _);
             }
         }
     }
 }
 
+// ClientStream's layout *must* match cubeb.c's `struct cubeb_stream` for the
+// common fields.
+#[repr(C)]
 #[derive(Debug)]
 pub struct ClientStream<'ctx> {
     // This must be a reference to Context for cubeb, cubeb accesses
     // stream methods via stream->context->ops
     context: &'ctx ClientContext,
+    user_ptr: *mut c_void,
     token: usize,
 }
 
 struct CallbackServer {
     input_shm: SharedMemSlice,
     output_shm: SharedMemMutSlice,
     data_cb: ffi::cubeb_data_callback,
     state_cb: ffi::cubeb_state_callback,
@@ -61,19 +65,20 @@ impl rpc::Server for CallbackServer {
     type Request = CallbackReq;
     type Response = CallbackResp;
     type Future = CpuFuture<Self::Response, ()>;
     type Transport = Framed<UnixStream, LengthDelimitedCodec<Self::Response, Self::Request>>;
 
     fn process(&mut self, req: Self::Request) -> Self::Future {
         match req {
             CallbackReq::Data(nframes, frame_size) => {
-                debug!(
+                trace!(
                     "stream_thread: Data Callback: nframes={} frame_size={}",
-                    nframes, frame_size
+                    nframes,
+                    frame_size
                 );
 
                 // Clone values that need to be moved into the cpu pool thread.
                 let input_shm = unsafe { self.input_shm.clone_view() };
                 let mut output_shm = unsafe { self.output_shm.clone_view() };
                 let user_ptr = self.user_ptr;
                 let cb = self.data_cb.unwrap();
 
@@ -99,17 +104,17 @@ impl rpc::Server for CallbackServer {
                         )
                     };
                     set_in_callback(false);
 
                     Ok(CallbackResp::Data(nframes as isize))
                 })
             }
             CallbackReq::State(state) => {
-                debug!("stream_thread: State Callback: {:?}", state);
+                trace!("stream_thread: State Callback: {:?}", state);
                 let user_ptr = self.user_ptr;
                 let cb = self.state_cb.unwrap();
                 self.cpu_pool.spawn_fn(move || {
                     set_in_callback(true);
                     unsafe {
                         cb(ptr::null_mut(), user_ptr as *mut _, state);
                     }
                     set_in_callback(false);
@@ -129,17 +134,17 @@ impl<'ctx> ClientStream<'ctx> {
         state_callback: ffi::cubeb_state_callback,
         user_ptr: *mut c_void,
     ) -> Result<Stream> {
         assert_not_in_callback();
 
         let rpc = ctx.rpc();
         let data = try!(send_recv!(rpc, StreamInit(init_params) => StreamCreated()));
 
-        trace!("token = {}, fds = {:?}", data.token, data.fds);
+        debug!("token = {}, fds = {:?}", data.token, data.fds);
 
         let stm = data.fds[0];
         let stream = unsafe { net::UnixStream::from_raw_fd(stm) };
 
         let input = data.fds[1];
         let input_file = unsafe { File::from_raw_fd(input) };
         let input_shm = SharedMemSlice::from(&input_file, SHM_AREA_SIZE).unwrap();
 
@@ -167,25 +172,26 @@ impl<'ctx> ClientStream<'ctx> {
             rpc::bind_server(transport, server, handle);
             wait_tx.send(()).unwrap();
             Ok(())
         });
         wait_rx.recv().unwrap();
 
         let stream = Box::into_raw(Box::new(ClientStream {
             context: ctx,
+            user_ptr: user_ptr,
             token: data.token,
         }));
         Ok(unsafe { Stream::from_ptr(stream as *mut _) })
     }
 }
 
 impl<'ctx> Drop for ClientStream<'ctx> {
     fn drop(&mut self) {
-        trace!("ClientStream drop...");
+        debug!("ClientStream dropped...");
         let rpc = self.context.rpc();
         let _ = send_recv!(rpc, StreamDestroy(self.token) => StreamDestroyed);
     }
 }
 
 impl<'ctx> StreamOps for ClientStream<'ctx> {
     fn start(&mut self) -> Result<()> {
         assert_not_in_callback();
@@ -263,10 +269,18 @@ impl<'ctx> StreamOps for ClientStream<'c
 
 pub fn init(
     ctx: &ClientContext,
     init_params: messages::StreamInitParams,
     data_callback: ffi::cubeb_data_callback,
     state_callback: ffi::cubeb_state_callback,
     user_ptr: *mut c_void,
 ) -> Result<Stream> {
-    ClientStream::init(ctx, init_params, data_callback, state_callback, user_ptr)
+    let stm = try!(ClientStream::init(
+        ctx,
+        init_params,
+        data_callback,
+        state_callback,
+        user_ptr
+    ));
+    debug_assert_eq!(stm.user_ptr(), user_ptr);
+    Ok(stm)
 }
diff --git a/media/audioipc/server/Cargo.toml b/media/audioipc/server/Cargo.toml
--- a/media/audioipc/server/Cargo.toml
+++ b/media/audioipc/server/Cargo.toml
@@ -1,20 +1,20 @@
 [package]
 name = "audioipc-server"
-version = "0.2.2"
+version = "0.2.3"
 authors = [
         "Matthew Gregan <kinetik@flim.org>",
         "Dan Glastonbury <dan.glastonbury@gmail.com>"
         ]
 description = "Remote cubeb server"
 
 [dependencies]
 audioipc = { path = "../audioipc" }
-cubeb = "0.5.2"
+cubeb-core = "0.5.1"
 bytes = "0.4"
 lazycell = "^0.4"
 libc = "0.2"
 log = "^0.3.6"
 slab = "0.3.0"
 futures = "0.1.18"
 tokio-core = "0.1"
 tokio-uds = "0.1.7"
diff --git a/media/audioipc/server/src/lib.rs b/media/audioipc/server/src/lib.rs
--- a/media/audioipc/server/src/lib.rs
+++ b/media/audioipc/server/src/lib.rs
@@ -1,42 +1,45 @@
 #[macro_use]
 extern crate error_chain;
 
 #[macro_use]
 extern crate log;
 
 extern crate audioipc;
 extern crate bytes;
-extern crate cubeb;
+extern crate cubeb_core as cubeb;
 extern crate futures;
 extern crate lazycell;
 extern crate libc;
 extern crate slab;
 extern crate tokio_core;
 extern crate tokio_uds;
 
 use audioipc::codec::LengthDelimitedCodec;
 use audioipc::core;
 use audioipc::fd_passing::{framed_with_fds, FramedWithFds};
 use audioipc::frame::{framed, Framed};
 use audioipc::messages::{CallbackReq, CallbackResp, ClientMessage, Device, DeviceInfo,
                          ServerMessage, StreamCreate, StreamInitParams, StreamParams};
 use audioipc::rpc;
 use audioipc::shm::{SharedMemReader, SharedMemWriter};
-use futures::Future;
+use cubeb::ffi;
 use futures::future::{self, FutureResult};
 use futures::sync::oneshot;
-use std::{ptr, slice};
+use futures::Future;
 use std::cell::RefCell;
 use std::convert::From;
 use std::error::Error;
-use std::os::raw::c_void;
+use std::ffi::{CStr, CString};
+use std::mem::{size_of, ManuallyDrop};
+use std::os::raw::{c_long, c_void};
 use std::os::unix::net;
 use std::os::unix::prelude::*;
+use std::{panic, ptr, slice};
 use tokio_core::reactor::Remote;
 use tokio_uds::UnixStream;
 
 pub mod errors {
     error_chain! {
         links {
             AudioIPC(::audioipc::errors::Error, ::audioipc::errors::ErrorKind);
         }
@@ -55,17 +58,18 @@ thread_local!(static CONTEXT_KEY:Context
 
 fn with_local_context<T, F>(f: F) -> T
 where
     F: FnOnce(&cubeb::Result<cubeb::Context>) -> T,
 {
     CONTEXT_KEY.with(|k| {
         let mut context = k.borrow_mut();
         if context.is_none() {
-            *context = Some(cubeb::init("AudioIPC Server"));
+            let name = CString::new("AudioIPC Server").unwrap();
+            *context = Some(cubeb::Context::init(Some(name.as_c_str()), None));
         }
         f(context.as_ref().unwrap())
     })
 }
 
 // TODO: Remove and let caller allocate based on cubeb backend requirements.
 const SHM_AREA_SIZE: usize = 2 * 1024 * 1024;
 
@@ -75,17 +79,94 @@ const STREAM_CONN_CHUNK_SIZE: usize = 64
 struct CallbackClient;
 
 impl rpc::Client for CallbackClient {
     type Request = CallbackReq;
     type Response = CallbackResp;
     type Transport = Framed<UnixStream, LengthDelimitedCodec<Self::Request, Self::Response>>;
 }
 
-type StreamSlab = slab::Slab<cubeb::Stream<u8>, usize>;
+struct ServerStreamCallbacks {
+    /// Size of input frame in bytes
+    input_frame_size: u16,
+    /// Size of output frame in bytes
+    output_frame_size: u16,
+    /// Shared memory buffer for sending input data to client
+    input_shm: SharedMemWriter,
+    /// Shared memory buffer for receiving output data from client
+    output_shm: SharedMemReader,
+    /// RPC interface to callback server running in client
+    rpc: rpc::ClientProxy<CallbackReq, CallbackResp>,
+}
+
+impl ServerStreamCallbacks {
+    fn data_callback(&mut self, input: &[u8], output: &mut [u8]) -> isize {
+        trace!("Stream data callback: {} {}", input.len(), output.len());
+
+        // len is of input and output is frame len. Turn these into the real lengths.
+        let real_input = unsafe {
+            let nbytes = input.len() * self.input_frame_size as usize;
+            slice::from_raw_parts(input.as_ptr(), nbytes)
+        };
+
+        self.input_shm.write(real_input).unwrap();
+
+        let r = self.rpc
+            .call(CallbackReq::Data(
+                output.len() as isize,
+                self.output_frame_size as usize,
+            ))
+            .wait();
+
+        match r {
+            Ok(CallbackResp::Data(frames)) => {
+                if frames >= 0 {
+                    let nbytes = frames as usize * self.output_frame_size as usize;
+                    let real_output = unsafe {
+                        trace!("Resize output to {}", nbytes);
+                        slice::from_raw_parts_mut(output.as_mut_ptr(), nbytes)
+                    };
+                    self.output_shm.read(&mut real_output[..nbytes]).unwrap();
+                }
+                frames
+            }
+            _ => {
+                debug!("Unexpected message {:?} during data_callback", r);
+                -1
+            }
+        }
+    }
+
+    fn state_callback(&mut self, state: cubeb::State) {
+        trace!("Stream state callback: {:?}", state);
+        let r = self.rpc.call(CallbackReq::State(state.into())).wait();
+        match r {
+            Ok(CallbackResp::State) => {}
+            _ => {
+                debug!("Unexpected message {:?} during callback", r);
+            }
+        }
+    }
+}
+
+struct ServerStream {
+    stream: ManuallyDrop<cubeb::Stream>,
+    cbs: ManuallyDrop<Box<ServerStreamCallbacks>>,
+}
+
+impl Drop for ServerStream {
+    fn drop(&mut self) {
+        unsafe {
+            ManuallyDrop::drop(&mut self.stream);
+            ManuallyDrop::drop(&mut self.cbs);
+        }
+    }
+}
+
+type StreamSlab = slab::Slab<ServerStream, usize>;
 
 pub struct CubebServer {
     cb_remote: Remote,
     streams: StreamSlab,
 }
 
 impl rpc::Server for CubebServer {
     type Request = ServerMessage;
@@ -162,57 +243,65 @@ impl CubebServer {
                 .unwrap_or_else(|_| error(cubeb::Error::error())),
 
             ServerMessage::StreamDestroy(stm_tok) => {
                 self.streams.remove(stm_tok);
                 ClientMessage::StreamDestroyed
             }
 
             ServerMessage::StreamStart(stm_tok) => self.streams[stm_tok]
+                .stream
                 .start()
                 .map(|_| ClientMessage::StreamStarted)
                 .unwrap_or_else(error),
 
             ServerMessage::StreamStop(stm_tok) => self.streams[stm_tok]
+                .stream
                 .stop()
                 .map(|_| ClientMessage::StreamStopped)
                 .unwrap_or_else(error),
 
             ServerMessage::StreamResetDefaultDevice(stm_tok) => self.streams[stm_tok]
+                .stream
                 .reset_default_device()
                 .map(|_| ClientMessage::StreamDefaultDeviceReset)
                 .unwrap_or_else(error),
 
             ServerMessage::StreamGetPosition(stm_tok) => self.streams[stm_tok]
+                .stream
                 .position()
                 .map(ClientMessage::StreamPosition)
                 .unwrap_or_else(error),
 
             ServerMessage::StreamGetLatency(stm_tok) => self.streams[stm_tok]
+                .stream
                 .latency()
                 .map(ClientMessage::StreamLatency)
                 .unwrap_or_else(error),
 
             ServerMessage::StreamSetVolume(stm_tok, volume) => self.streams[stm_tok]
+                .stream
                 .set_volume(volume)
                 .map(|_| ClientMessage::StreamVolumeSet)
                 .unwrap_or_else(error),
 
             ServerMessage::StreamSetPanning(stm_tok, panning) => self.streams[stm_tok]
+                .stream
                 .set_panning(panning)
                 .map(|_| ClientMessage::StreamPanningSet)
                 .unwrap_or_else(error),
 
             ServerMessage::StreamGetCurrentDevice(stm_tok) => self.streams[stm_tok]
+                .stream
                 .current_device()
                 .map(|device| ClientMessage::StreamCurrentDevice(Device::from(device)))
                 .unwrap_or_else(error),
         };
 
-        debug!("process_msg: req={:?}, resp={:?}", msg, resp);
+        trace!("process_msg: req={:?}, resp={:?}", msg, resp);
 
         resp
     }
 
     // Stream init is special, so it's been separated from process_msg.
     fn process_stream_init(
         &mut self,
         context: &cubeb::Context,
@@ -231,27 +320,23 @@ impl CubebServer {
                         | cubeb::SampleFormat::Float32NE => 4,
                     };
                     let channel_count = p.channels as u16;
                     sample_size * channel_count
                 })
                 .unwrap_or(0u16)
         }
 
-        // TODO: Yuck!
-        let input_device = params.input_device as *const _;
-        let output_device = params.output_device as *const _;
-        let latency = params.latency_frames;
-
+        // Create the callback handling struct which is attached the cubeb stream.
         let input_frame_size = frame_size_in_bytes(params.input_stream_params.as_ref());
         let output_frame_size = frame_size_in_bytes(params.output_stream_params.as_ref());
 
         let (stm1, stm2) = net::UnixStream::pair()?;
         debug!("Created callback pair: {:?}-{:?}", stm1, stm2);
-        let (mut input_shm, input_file) =
+        let (input_shm, input_file) =
             SharedMemWriter::new(&audioipc::get_shm_path("input"), SHM_AREA_SIZE)?;
         let (output_shm, output_file) =
             SharedMemReader::new(&audioipc::get_shm_path("output"), SHM_AREA_SIZE)?;
 
         // This code is currently running on the Client/Server RPC
         // handling thread.  We need to move the registration of the
         // bind_client to the callback RPC handling thread.  This is
         // done by spawning a future on cb_remote.
@@ -265,122 +350,99 @@ impl CubebServer {
             assert_ne!(id, handle.id());
             let stream = UnixStream::from_stream(stm2, handle).unwrap();
             let transport = framed(stream, Default::default());
             let rpc = rpc::bind_client::<CallbackClient>(transport, handle);
             drop(tx.send(rpc));
             Ok(())
         });
 
-        let rpc_data: rpc::ClientProxy<CallbackReq, CallbackResp> = match rx.wait() {
+        let rpc: rpc::ClientProxy<CallbackReq, CallbackResp> = match rx.wait() {
             Ok(rpc) => rpc,
             Err(_) => bail!("Failed to create callback rpc."),
         };
-        let rpc_state = rpc_data.clone();
 
-        let mut builder = cubeb::StreamBuilder::new();
-
-        if let Some(ref stream_name) = params.stream_name {
-            builder.name(stream_name.clone());
-        }
+        let cbs = Box::new(ServerStreamCallbacks {
+            input_frame_size,
+            output_frame_size,
+            input_shm,
+            output_shm,
+            rpc,
+        });
 
-        if let Some(ref isp) = params.input_stream_params {
-            let input_stream_params =
-                unsafe { cubeb::StreamParamsRef::from_ptr(isp as *const StreamParams as *mut _) };
-            builder.input(input_device, input_stream_params);
-        }
+        // Create cubeb stream from params
+        let stream_name = params
+            .stream_name
+            .as_ref()
+            .and_then(|name| CStr::from_bytes_with_nul(name).ok());
 
-        if let Some(ref osp) = params.output_stream_params {
-            let output_stream_params =
-                unsafe { cubeb::StreamParamsRef::from_ptr(osp as *const StreamParams as *mut _) };
-            builder.output(output_device, output_stream_params);
-        }
+        let input_device = params.input_device as *const _;
+        let input_stream_params = params.input_stream_params.as_ref().map(|isp| unsafe {
+            cubeb::StreamParamsRef::from_ptr(isp as *const StreamParams as *mut _)
+        });
 
-        builder
-            .latency(latency)
-            .data_callback(move |input, output| {
-                trace!("Stream data callback: {} {}", input.len(), output.len());
+        let output_device = params.output_device as *const _;
+        let output_stream_params = params.output_stream_params.as_ref().map(|osp| unsafe {
+            cubeb::StreamParamsRef::from_ptr(osp as *const StreamParams as *mut _)
+        });
+
+        let latency = params.latency_frames;
+        assert!(size_of::<Box<ServerStreamCallbacks>>() == size_of::<usize>());
+        let user_ptr = cbs.as_ref() as *const ServerStreamCallbacks as *mut c_void;
 
-                // len is of input and output is frame len. Turn these into the real lengths.
-                let real_input = unsafe {
-                    let nbytes = input.len() * input_frame_size as usize;
-                    slice::from_raw_parts(input.as_ptr(), nbytes)
-                };
-
-                input_shm.write(real_input).unwrap();
+        unsafe {
+            context
+                .stream_init(
+                    stream_name,
+                    input_device,
+                    input_stream_params,
+                    output_device,
+                    output_stream_params,
+                    latency,
+                    Some(data_cb_c),
+                    Some(state_cb_c),
+                    user_ptr,
+                )
+                .and_then(|stream| {
+                    if !self.streams.has_available() {
+                        trace!(
+                            "server connection ran out of stream slots. reserving {} more.",
+                            STREAM_CONN_CHUNK_SIZE
+                        );
+                        self.streams.reserve_exact(STREAM_CONN_CHUNK_SIZE);
+                    }
 
-                let r = rpc_data
-                    .call(CallbackReq::Data(
-                        output.len() as isize,
-                        output_frame_size as usize,
-                    ))
-                    .wait();
+                    let stm_tok = match self.streams.vacant_entry() {
+                        Some(entry) => {
+                            debug!("Registering stream {:?}", entry.index(),);
 
-                match r {
-                    Ok(CallbackResp::Data(frames)) => {
-                        if frames >= 0 {
-                            let nbytes = frames as usize * output_frame_size as usize;
-                            let real_output = unsafe {
-                                trace!("Resize output to {}", nbytes);
-                                slice::from_raw_parts_mut(output.as_mut_ptr(), nbytes)
-                            };
-                            output_shm.read(&mut real_output[..nbytes]).unwrap();
+                            entry
+                                .insert(ServerStream {
+                                    stream: ManuallyDrop::new(stream),
+                                    cbs: ManuallyDrop::new(cbs),
+                                })
+                                .index()
                         }
-                        frames
-                    }
-                    _ => {
-                        debug!("Unexpected message {:?} during data_callback", r);
-                        -1
-                    }
-                }
-            })
-            .state_callback(move |state| {
-                trace!("Stream state callback: {:?}", state);
-                let r = rpc_state.call(CallbackReq::State(state.into())).wait();
-                match r {
-                    Ok(CallbackResp::State) => {}
-                    _ => {
-                        debug!("Unexpected message {:?} during callback", r);
-                    }
-                }
-            });
+                        None => {
+                            // TODO: Turn into error
+                            panic!("Failed to insert stream into slab. No entries")
+                        }
+                    };
 
-        builder
-            .init(context)
-            .and_then(|stream| {
-                if !self.streams.has_available() {
-                    trace!(
-                        "server connection ran out of stream slots. reserving {} more.",
-                        STREAM_CONN_CHUNK_SIZE
-                    );
-                    self.streams.reserve_exact(STREAM_CONN_CHUNK_SIZE);
-                }
-
-                let stm_tok = match self.streams.vacant_entry() {
-                    Some(entry) => {
-                        debug!("Registering stream {:?}", entry.index(),);
-
-                        entry.insert(stream).index()
-                    }
-                    None => {
-                        // TODO: Turn into error
-                        panic!("Failed to insert stream into slab. No entries")
-                    }
-                };
-
-                Ok(ClientMessage::StreamCreated(StreamCreate {
-                    token: stm_tok,
-                    fds: [
-                        stm1.into_raw_fd(),
-                        input_file.into_raw_fd(),
-                        output_file.into_raw_fd(),
-                    ],
-                }))
-            })
-            .map_err(|e| e.into())
+                    Ok(ClientMessage::StreamCreated(StreamCreate {
+                        token: stm_tok,
+                        fds: [
+                            stm1.into_raw_fd(),
+                            input_file.into_raw_fd(),
+                            output_file.into_raw_fd(),
+                        ],
+                    }))
+                })
+                .map_err(|e| e.into())
+        }
     }
 }
 
 struct ServerWrapper {
     core_thread: core::CoreThread,
     callback_thread: core::CoreThread,
 }
 
@@ -462,8 +524,46 @@ pub extern "C" fn audioipc_server_new_cl
 pub extern "C" fn audioipc_server_stop(p: *mut c_void) {
     let wrapper = unsafe { Box::<ServerWrapper>::from_raw(p as *mut _) };
     drop(wrapper);
 }
 
 fn error(error: cubeb::Error) -> ClientMessage {
     ClientMessage::Error(error.raw_code())
 }
+
+// C callable callbacks
+unsafe extern "C" fn data_cb_c(
+    _: *mut ffi::cubeb_stream,
+    user_ptr: *mut c_void,
+    input_buffer: *const c_void,
+    output_buffer: *mut c_void,
+    nframes: c_long,
+) -> c_long {
+    let ok = panic::catch_unwind(|| {
+        let cbs = &mut *(user_ptr as *mut ServerStreamCallbacks);
+        let input = if input_buffer.is_null() {
+            &[]
+        } else {
+            slice::from_raw_parts(input_buffer as *const u8, nframes as usize)
+        };
+        let output: &mut [u8] = if output_buffer.is_null() {
+            &mut []
+        } else {
+            slice::from_raw_parts_mut(output_buffer as *mut u8, nframes as usize)
+        };
+        cbs.data_callback(input, output) as c_long
+    });
+    ok.unwrap_or(0)
+}
+
+unsafe extern "C" fn state_cb_c(
+    _: *mut ffi::cubeb_stream,
+    user_ptr: *mut c_void,
+    state: ffi::cubeb_state,
+) {
+    let ok = panic::catch_unwind(|| {
+        let state = cubeb::State::from(state);
+        let cbs = &mut *(user_ptr as *mut ServerStreamCallbacks);
+        cbs.state_callback(state);
+    });
+    ok.expect("State callback panicked");
+}
