# HG changeset patch
# User Doug Thayer <dothayer@mozilla.com>
# Date 1512423970 28800
# Node ID 15a13cdc34d5dbbf361ddfc6d246436935222f10
# Parent  3325c31e498c6207c672988f0070477b6fd010c7
Bug 1421701 - Chunk visits when notifying main thread r=mak

Since processing several hundred visits synchronously on the
main thread can be somewhat expensive, and since the main thread
might be idle while the storage worker is running, it makes sense
to chunk the messages into groups of 100.

We did have a concern that this changes the move of the underlying
array to a copy, which might bring some cost with it. For small
arrays, we simply move the underlying array to avoid the copy to
mitigate it. However, for large arrays I was unable to observe any
significant performance cost anyway, so I think we're in the clear.

MozReview-Commit-ID: 1hWSEyKw6pi

diff --git a/toolkit/components/places/History.cpp b/toolkit/components/places/History.cpp
--- a/toolkit/components/places/History.cpp
+++ b/toolkit/components/places/History.cpp
@@ -1026,16 +1026,21 @@ public:
       return NS_OK;
     }
 
     mozStorageTransaction transaction(mDBConn, false,
                                       mozIStorageConnection::TRANSACTION_IMMEDIATE);
 
     const VisitData* lastFetchedPlace = nullptr;
     uint32_t lastFetchedVisitCount = 0;
+    bool shouldChunkNotifications = mPlaces.Length() > NOTIFY_VISITS_CHUNK_SIZE;
+    InfallibleTArray<VisitData> notificationChunk;
+    if (shouldChunkNotifications) {
+      notificationChunk.SetCapacity(NOTIFY_VISITS_CHUNK_SIZE);
+    }
     for (nsTArray<VisitData>::size_type i = 0; i < mPlaces.Length(); i++) {
       VisitData& place = mPlaces.ElementAt(i);
 
       // Fetching from the database can overwrite this information, so save it
       // apart.
       bool typed = place.typed;
       bool hidden = place.hidden;
 
@@ -1095,16 +1100,31 @@ public:
           nsCOMPtr<nsIRunnable> event =
             new NotifyPlaceInfoCallback(mCallback, place, true, rv);
           nsresult rv2 = NS_DispatchToMainThread(event);
           NS_ENSURE_SUCCESS(rv2, rv2);
         }
       }
       NS_ENSURE_SUCCESS(rv, rv);
 
+      if (shouldChunkNotifications) {
+        int32_t numRemaining = mPlaces.Length() - (i + 1);
+        notificationChunk.AppendElement(place);
+        if (notificationChunk.Length() == NOTIFY_VISITS_CHUNK_SIZE ||
+            numRemaining == 0) {
+          // This will SwapElements on notificationChunk with an empty nsTArray
+          nsCOMPtr<nsIRunnable> event = new NotifyManyVisitsObservers(notificationChunk);
+          rv = NS_DispatchToMainThread(event);
+          NS_ENSURE_SUCCESS(rv, rv);
+
+          int32_t nextCapacity = std::min(NOTIFY_VISITS_CHUNK_SIZE, numRemaining);
+          notificationChunk.SetCapacity(nextCapacity);
+        }
+      }
+
       // If we get here, we must have been successful adding/updating this
       // visit/place, so update the count:
       mSuccessfulUpdatedCount++;
     }
 
     {
       // Trigger an update for all the hosts of the places we inserted
       nsAutoCString query("DELETE FROM moz_updatehostsinsert_temp");
@@ -1113,19 +1133,23 @@ public:
       mozStorageStatementScoper scoper(stmt);
       nsresult rv = stmt->Execute();
       NS_ENSURE_SUCCESS(rv, rv);
     }
 
     nsresult rv = transaction.Commit();
     NS_ENSURE_SUCCESS(rv, rv);
 
-    nsCOMPtr<nsIRunnable> event = new NotifyManyVisitsObservers(mPlaces);
-    rv = NS_DispatchToMainThread(event);
-    NS_ENSURE_SUCCESS(rv, rv);
+    // If we don't need to chunk the notifications, just notify using the
+    // original mPlaces array.
+    if (!shouldChunkNotifications) {
+      nsCOMPtr<nsIRunnable> event = new NotifyManyVisitsObservers(mPlaces);
+      rv = NS_DispatchToMainThread(event);
+      NS_ENSURE_SUCCESS(rv, rv);
+    }
 
     return NS_OK;
   }
 private:
   InsertVisitedURIs(
     mozIStorageConnection* aConnection,
     nsTArray<VisitData>& aPlaces,
     const nsMainThreadPtrHandle<mozIVisitInfoCallback>& aCallback,
diff --git a/toolkit/components/places/History.h b/toolkit/components/places/History.h
--- a/toolkit/components/places/History.h
+++ b/toolkit/components/places/History.h
@@ -37,16 +37,20 @@ class ConcurrentStatementsHolder;
 // Initial size of mRecentlyVisitedURIs.
 #define RECENTLY_VISITED_URIS_SIZE 64
 // Microseconds after which a visit can be expired from mRecentlyVisitedURIs.
 // When an URI is reloaded we only take into account the first visit to it, and
 // ignore any subsequent visits, if they happen before this time has elapsed.
 // A commonly found case is to reload a page every 5 minutes, so we pick a time
 // larger than that.
 #define RECENTLY_VISITED_URIS_MAX_AGE 6 * 60 * PR_USEC_PER_SEC
+// When notifying the main thread after inserting visits, we chunk the visits
+// into medium-sized groups so that we can amortize the cost of the runnable
+// without janking the main thread by expecting it to process hundreds at once.
+#define NOTIFY_VISITS_CHUNK_SIZE 100
 
 class History final : public IHistory
                     , public nsIDownloadHistory
                     , public mozIAsyncHistory
                     , public nsIObserver
                     , public nsIMemoryReporter
 {
 public:

