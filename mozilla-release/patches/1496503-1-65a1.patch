# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1542234923 0
# Node ID c61a46d741f9649de8cdca1a431c2a51cf05f564
# Parent  24f860f4736fa50db631db5e7a3f0e516063845f
Bug 1496503 - Move MOZ_CrashOOL to Assertions.h. r=froydnj

Ideally, we'd want the function to stay in Assertions.cpp, but that's
only part of MFBT proper, and that doesn't have access to WalkTheStack
like MOZ_CRASH has from being in Assertion.h, when included from Gecko
code. Moving WalkTheStack to mozglue, putting it close together with
MozStackWalk would be prefered, but that causes problems linking MFBT
tests (which don't have access to mozglue), and other things.

Overall, this was too deep a rabbit hole, and moving MOZ_CrashOOL to
Assertions.h is much simpler. Since it's essentially the same as
MOZ_CRASH, except it allows non-literal strings, we can make it inlined,
and leave it to the compiler to drop the filename argument when it's not
used.

Differential Revision: https://phabricator.services.mozilla.com/D11718

diff --git a/mfbt/Assertions.cpp b/mfbt/Assertions.cpp
--- a/mfbt/Assertions.cpp
+++ b/mfbt/Assertions.cpp
@@ -13,31 +13,16 @@ MOZ_BEGIN_EXTERN_C
 /*
  * The crash reason is defined as a global variable here rather than in the
  * crash reporter itself to make it available to all code, even libraries like
  * JS that don't link with the crash reporter directly. This value will only
  * be consumed if the crash reporter is used by the target application.
  */
 MFBT_DATA const char* gMozCrashReason = nullptr;
 
-#ifndef DEBUG
-MFBT_API MOZ_COLD MOZ_NORETURN MOZ_NEVER_INLINE void
-MOZ_CrashOOL(int aLine, const char* aReason)
-#else
-MFBT_API MOZ_COLD MOZ_NORETURN MOZ_NEVER_INLINE void
-MOZ_CrashOOL(const char* aFilename, int aLine, const char* aReason)
-#endif
-{
-#ifdef DEBUG
-  MOZ_ReportCrash(aReason, aFilename, aLine);
-#endif
-  gMozCrashReason = aReason;
-  MOZ_REALLY_CRASH(aLine);
-}
-
 static char sPrintfCrashReason[sPrintfCrashReasonSize] = {};
 static mozilla::Atomic<bool> sCrashing(false);
 
 #ifndef DEBUG
 MFBT_API MOZ_COLD MOZ_NORETURN MOZ_NEVER_INLINE MOZ_FORMAT_PRINTF(2, 3) void
 MOZ_CrashPrintf(int aLine, const char* aFormat, ...)
 #else
 MFBT_API MOZ_COLD MOZ_NORETURN MOZ_NEVER_INLINE MOZ_FORMAT_PRINTF(3, 4) void
diff --git a/mfbt/Assertions.h b/mfbt/Assertions.h
--- a/mfbt/Assertions.h
+++ b/mfbt/Assertions.h
@@ -279,25 +279,26 @@ MOZ_NoReturn(int aLine)
  * arbitrary strings from a potentially compromised process is not without risk.
  * If the string being passed is the result of a printf-style function,
  * consider using MOZ_CRASH_UNSAFE_PRINTF instead.
  *
  * @note This macro causes data collection because crash strings are annotated
  * to crash-stats and are publicly visible. Firefox data stewards must do data
  * review on usages of this macro.
  */
-#ifndef DEBUG
-MFBT_API MOZ_COLD MOZ_NORETURN MOZ_NEVER_INLINE void
-MOZ_CrashOOL(int aLine, const char* aReason);
-#  define MOZ_CRASH_UNSAFE_OOL(reason) MOZ_CrashOOL(__LINE__, reason)
-#else
-MFBT_API MOZ_COLD MOZ_NORETURN MOZ_NEVER_INLINE void
-MOZ_CrashOOL(const char* aFilename, int aLine, const char* aReason);
-#  define MOZ_CRASH_UNSAFE_OOL(reason) MOZ_CrashOOL(__FILE__, __LINE__, reason)
+static inline MOZ_COLD MOZ_NORETURN void
+MOZ_CrashOOL(const char* aFilename, int aLine, const char* aReason)
+{
+#ifdef DEBUG
+  MOZ_ReportCrash(aReason, aFilename, aLine);
 #endif
+  MOZ_CRASH_ANNOTATE(aReason);
+  MOZ_REALLY_CRASH(aLine);
+}
+#define MOZ_CRASH_UNSAFE_OOL(reason) MOZ_CrashOOL(__FILE__, __LINE__, reason)
 
 static const size_t sPrintfMaxArgs = 4;
 static const size_t sPrintfCrashReasonSize = 1024;
 
 #ifndef DEBUG
 MFBT_API MOZ_COLD MOZ_NORETURN MOZ_NEVER_INLINE MOZ_FORMAT_PRINTF(2, 3) void
 MOZ_CrashPrintf(int aLine, const char* aFormat, ...);
 #  define MOZ_CALL_CRASH_PRINTF(format, ...) \
