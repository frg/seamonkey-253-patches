# HG changeset patch
# User Ashly Hale <ahale@mozilla.com>
# Date 1664803632 0
# Node ID 1f5b5639147de6beec8a6c5f5ed0612d54e97761
# Parent  3fab504fca03f45b32ea7228a06cb6c3b6746bd5
Bug 1789729 - Implement webgl.max-size-per-texture-mib r=jgilbert a=RyanVM

Differential Revision: https://phabricator.services.mozilla.com/D156903

diff --git a/dom/canvas/WebGLTextureUpload.cpp b/dom/canvas/WebGLTextureUpload.cpp
--- a/dom/canvas/WebGLTextureUpload.cpp
+++ b/dom/canvas/WebGLTextureUpload.cpp
@@ -1011,20 +1011,40 @@ ValidateCompressedTexImageRestrictions(c
     // Default: There are no restrictions on CompressedTexImage.
     default: // ATC, ETC1, ES3
         break;
     }
 
     return true;
 }
 
-static bool
-ValidateTargetForFormat(const char* funcName, WebGLContext* webgl, TexImageTarget target,
-                        const webgl::FormatInfo* format)
-{
+static bool ValidateFormatAndSize(const char* funcName,
+                                  WebGLContext* webgl,
+                                  TexImageTarget target,
+                                  const webgl::FormatInfo* format,
+                                  const uint32_t width,
+                                  const uint32_t height,
+                                  const uint32_t depth) {
+  // Check if texture size will likely be rejected by the driver and give a more
+  // meaningful error message.
+  auto baseImageSize = CheckedInt<uint64_t>(format->estimatedBytesPerPixel) *
+                       width * height * depth;
+  if (target == LOCAL_GL_TEXTURE_CUBE_MAP) {
+    baseImageSize *= 6;
+  }
+  if (!baseImageSize.isValid() ||
+      baseImageSize.value() >
+          (uint64_t)gfxPrefs::WebGLMaxSizePerTextureMiB *
+              (1024 * 1024)) {
+    webgl->ErrorOutOfMemory(
+        "Texture size too large; base image mebibytes > "
+        "webgl.max-size-per-texture-mib");
+    return false;
+  }
+
     // GLES 3.0.4 p127:
     // "Textures with a base internal format of DEPTH_COMPONENT or DEPTH_STENCIL are
     //  supported by texture image specification commands only if `target` is TEXTURE_2D,
     //  TEXTURE_2D_ARRAY, or TEXTURE_CUBE_MAP. Using these formats in conjunction with any
     //  other `target` will result in an INVALID_OPERATION error."
 
     switch (format->effectiveFormat) {
     // TEXTURE_2D_ARRAY but not TEXTURE_3D:
@@ -1117,17 +1137,18 @@ WebGLTexture::TexStorage(const char* fun
     auto dstUsage = mContext->mFormatUsage->GetSizedTexUsage(sizedFormat);
     if (!dstUsage) {
         mContext->ErrorInvalidEnum("%s: Invalid internalformat: 0x%04x", funcName,
                                    sizedFormat);
         return;
     }
     auto dstFormat = dstUsage->format;
 
-    if (!ValidateTargetForFormat(funcName, mContext, testTarget, dstFormat))
+    if (!ValidateFormatAndSize(funcName, mContext, testTarget, dstFormat,
+                               width, height, depth))
         return;
 
     if (dstFormat->compression) {
         if (!ValidateCompressedTexImageRestrictions(funcName, mContext, testTarget,
                                                     testLevel, dstFormat, width, height,
                                                     depth))
         {
             return;
@@ -1244,17 +1265,18 @@ WebGLTexture::TexImage(const char* funcN
                                         funcName, internalFormat, pi.format, pi.type);
         return;
     }
 
     ////////////////////////////////////
     // Check that source and dest info are compatible
     auto dstFormat = dstUsage->format;
 
-    if (!ValidateTargetForFormat(funcName, mContext, target, dstFormat))
+    if (!ValidateFormatAndSize(funcName, mContext, target, dstFormat,
+                               blob->mWidth, blob->mHeight, blob->mDepth))
         return;
 
     if (!mContext->IsWebGL2() && dstFormat->d) {
         if (target != LOCAL_GL_TEXTURE_2D ||
             blob->HasData() ||
             level != 0)
         {
             mContext->ErrorInvalidOperation("%s: With format %s, this function may only"
@@ -1451,17 +1473,18 @@ WebGLTexture::CompressedTexImage(const c
     if (!usage || !usage->format->compression) {
         mContext->ErrorInvalidEnumArg(funcName, "internalFormat",
                                       internalFormat);
         return;
     }
 
     auto format = usage->format;
 
-    if (!ValidateTargetForFormat(funcName, mContext, target, format))
+    if (!ValidateFormatAndSize(funcName, mContext, target, format,
+                               blob->mWidth, blob->mHeight, blob->mDepth))
         return;
 
     ////////////////////////////////////
     // Get source info
 
     if (!ValidateCompressedTexUnpack(mContext, funcName, blob->mWidth, blob->mHeight,
                                      blob->mDepth, format, blob->mAvailBytes))
     {
@@ -2131,17 +2154,18 @@ WebGLTexture::CopyTexImage2D(TexImageTar
 
     const auto& srcFormat = srcUsage->format;
     const auto dstUsage = ValidateCopyDestUsage(funcName, mContext, srcFormat,
                                                 internalFormat);
     if (!dstUsage)
         return;
 
     const auto& dstFormat = dstUsage->format;
-    if (!ValidateTargetForFormat(funcName, mContext, target, dstFormat))
+    if (!ValidateFormatAndSize(funcName, mContext, target, dstFormat,
+                               width, height, depth))
         return;
 
     if (!mContext->IsWebGL2() && dstFormat->d) {
         mContext->ErrorInvalidOperation("%s: Function may not be called with format %s.",
                                         funcName, dstFormat->name);
         return;
     }
 
diff --git a/gfx/thebes/gfxPrefs.h b/gfx/thebes/gfxPrefs.h
--- a/gfx/thebes/gfxPrefs.h
+++ b/gfx/thebes/gfxPrefs.h
@@ -747,16 +747,17 @@ private:
   DECL_GFX_PREF(Live, "webgl.enable-webgl2",                   WebGL2Enabled, bool, true);
   DECL_GFX_PREF(Live, "webgl.force-enabled",                   WebGLForceEnabled, bool, false);
   DECL_GFX_PREF(Once, "webgl.force-layers-readback",           WebGLForceLayersReadback, bool, false);
   DECL_GFX_PREF(Live, "webgl.force-index-validation",          WebGLForceIndexValidation, int32_t, 0);
   DECL_GFX_PREF(Live, "webgl.lose-context-on-memory-pressure", WebGLLoseContextOnMemoryPressure, bool, false);
   DECL_GFX_PREF(Live, "webgl.max-contexts",                    WebGLMaxContexts, uint32_t, 32);
   DECL_GFX_PREF(Live, "webgl.max-contexts-per-principal",      WebGLMaxContextsPerPrincipal, uint32_t, 16);
   DECL_GFX_PREF(Live, "webgl.max-warnings-per-context",        WebGLMaxWarningsPerContext, uint32_t, 32);
+  DECL_GFX_PREF(Live, "webgl.max-size-per-texture-mb",         WebGLMaxSizePerTextureMB, uint32_t, 1024); 
   DECL_GFX_PREF(Live, "webgl.min_capability_mode",             WebGLMinCapabilityMode, bool, false);
   DECL_GFX_PREF(Live, "webgl.msaa-force",                      WebGLForceMSAA, bool, false);
   DECL_GFX_PREF(Live, "webgl.msaa-samples",                    WebGLMsaaSamples, uint32_t, 4);
   DECL_GFX_PREF(Live, "webgl.prefer-16bpp",                    WebGLPrefer16bpp, bool, false);
   DECL_GFX_PREF(Live, "webgl.restore-context-when-visible",    WebGLRestoreWhenVisible, bool, true);
   DECL_GFX_PREF(Live, "webgl.allow-immediate-queries",         WebGLImmediateQueries, bool, false);
   DECL_GFX_PREF(Live, "webgl.allow-fb-invalidation",           WebGLFBInvalidation, bool, false);
 
diff --git a/modules/libpref/init/all.js b/modules/libpref/init/all.js
--- a/modules/libpref/init/all.js
+++ b/modules/libpref/init/all.js
@@ -4963,16 +4963,17 @@ pref("webgl.restore-context-when-visible
 #ifdef ANDROID
 pref("webgl.max-contexts", 16);
 pref("webgl.max-contexts-per-principal", 8);
 #else
 pref("webgl.max-contexts", 32);
 pref("webgl.max-contexts-per-principal", 16);
 #endif
 pref("webgl.max-warnings-per-context", 32);
+pref("webgl.max-size-per-texture-mib", 1024);
 pref("webgl.enable-draft-extensions", false);
 pref("webgl.enable-privileged-extensions", false);
 pref("webgl.bypass-shader-validation", false);
 pref("webgl.disable-fail-if-major-performance-caveat", false);
 pref("webgl.disable-DOM-blit-uploads", false);
 pref("webgl.allow-fb-invalidation", false);
 pref("webgl.webgl2-compat-mode", false);
 
