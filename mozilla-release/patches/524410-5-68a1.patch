# HG changeset patch
# User Nathan Froyd <froydnj@mozilla.com>
# Date 1554482110 0
# Node ID 17040bb20e256476df548a6be770e7d8f78387ef
# Parent  2cd3fd6f419de621f2df3a26a32bee544a42b681
Bug 524410 - part 5 - merge adjacent line records where possible; r=gsvelto

After replacing precise line information from .debug_line with coarse
line information from DW_AT_call_{file,line}, it's very likely that
adjacent line records actually refer to identical file and line
numbers.  Such adjacent records are not really useful and take up more
space than they should in the symbol file.  We might as well merge them
and save ourselves some space.

Differential Revision: https://phabricator.services.mozilla.com/D25473

diff --git a/toolkit/crashreporter/google-breakpad/src/common/dwarf_cu_to_module.cc b/toolkit/crashreporter/google-breakpad/src/common/dwarf_cu_to_module.cc
--- a/toolkit/crashreporter/google-breakpad/src/common/dwarf_cu_to_module.cc
+++ b/toolkit/crashreporter/google-breakpad/src/common/dwarf_cu_to_module.cc
@@ -1232,16 +1232,57 @@ vector<Module::Line> MergeLines(const ve
     if (orig_lines->address > inline_lines->address) {
       ++inline_lines;
       continue;
     }
   }
 
   return merged_lines;
 }
+
+// After merging the line information, we may have adjacent lines that belong
+// to the same file and line number.  (The compiler shouldn't be producing
+// such line records on its own.)  Let's merge adjacent lines where possible
+// to make symbol files smaller.
+void CollapseAdjacentLines(vector<Module::Line>& lines) {
+  if (lines.empty()) {
+    return;
+  }
+
+  auto merging_into = lines.begin();
+  auto next = merging_into + 1;
+  const auto end = lines.end();
+
+  while (next != end) {
+    // The next record might be able to be merged.
+    if ((merging_into->address + merging_into->size) == next->address &&
+        merging_into->file == next->file &&
+        merging_into->number == next->number) {
+      merging_into->size = next->address + next->size - merging_into->address;
+      ++next;
+      continue;
+    }
+
+    // We've merged all we can into this record.  Move on.
+    ++merging_into;
+
+    // next now points at the most recent record that wasn't able to be
+    // merged with a previous record.  We may still have more records to
+    // consider, and if merging_into and next have become discontiguous,
+    // we need to copy things around.
+    if (next != end) {
+      if (next != merging_into) {
+        *merging_into = std::move(*next);
+      }
+      ++next;
+    }
+  }
+
+  lines.erase(merging_into + 1, end);
+}
 }
 
 void DwarfCUToModule::AssignLinesToFunctions(const LineToModuleHandler::FileMap &files) {
   vector<Module::Function *> *functions = &cu_context_->functions;
   WarningReporter *reporter = cu_context_->reporter;
 
   // This would be simpler if we assumed that source line entries
   // don't cross function boundaries.  However, there's no real reason
@@ -1275,16 +1316,19 @@ void DwarfCUToModule::AssignLinesToFunct
     line.number = range.call_line_;
     line.file = f->second;
     inlines.push_back(line);
   }
   std::sort(inlines.begin(), inlines.end(), Module::Line::CompareByAddress);
 
   if (!inlines.empty()) {
     vector<Module::Line> merged_lines = MergeLines(inlines, lines_);
+
+    CollapseAdjacentLines(merged_lines);
+
     lines_ = std::move(merged_lines);
   }
 
   // The last line that we used any piece of.  We use this only for
   // generating warnings.
   const Module::Line *last_line_used = NULL;
 
   // The last function and line we warned about --- so we can avoid
