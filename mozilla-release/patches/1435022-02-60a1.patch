# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1517520530 18000
# Node ID 20a81a8196362e8446a71929158442f0508c78fc
# Parent  3e5d8247eca13f12c51c076910fda1bb496f4f6d
Bug 1435022 - Make ScheduleTask private and drop a useless argument. r=sotaro

MozReview-Commit-ID: 4bRjv1L7DLF

diff --git a/gfx/layers/ipc/CompositorVsyncScheduler.cpp b/gfx/layers/ipc/CompositorVsyncScheduler.cpp
--- a/gfx/layers/ipc/CompositorVsyncScheduler.cpp
+++ b/gfx/layers/ipc/CompositorVsyncScheduler.cpp
@@ -126,17 +126,17 @@ CompositorVsyncScheduler::PostCompositeT
   MonitorAutoLock lock(mCurrentCompositeTaskMonitor);
   if (mCurrentCompositeTask == nullptr && CompositorThreadHolder::Loop()) {
     RefPtr<CancelableRunnable> task = NewCancelableRunnableMethod<TimeStamp>(
       "layers::CompositorVsyncScheduler::Composite",
       this,
       &CompositorVsyncScheduler::Composite,
       aCompositeTimestamp);
     mCurrentCompositeTask = task;
-    ScheduleTask(task.forget(), 0);
+    ScheduleTask(task.forget());
   }
 }
 
 void
 CompositorVsyncScheduler::PostVRTask(TimeStamp aTimestamp)
 {
   MonitorAutoLock lockVR(mCurrentVRListenerTaskMonitor);
   if (mCurrentVRListenerTask == nullptr && VRListenerThreadHolder::Loop()) {
@@ -201,17 +201,17 @@ CompositorVsyncScheduler::SetNeedsCompos
 {
   if (!CompositorThreadHolder::IsInCompositorThread()) {
     MonitorAutoLock lock(mSetNeedsCompositeMonitor);
     RefPtr<CancelableRunnable> task = NewCancelableRunnableMethod(
       "layers::CompositorVsyncScheduler::SetNeedsComposite",
       this,
       &CompositorVsyncScheduler::SetNeedsComposite);
     mSetNeedsCompositeTask = task;
-    ScheduleTask(task.forget(), 0);
+    ScheduleTask(task.forget());
     return;
   } else {
     MonitorAutoLock lock(mSetNeedsCompositeMonitor);
     mSetNeedsCompositeTask = nullptr;
   }
 
   mNeedsComposite++;
   if (!mIsObservingVsync && mNeedsComposite) {
@@ -356,22 +356,20 @@ CompositorVsyncScheduler::DispatchVREven
     return;
   }
 
   VRManager* vm = VRManager::Get();
   vm->NotifyVsync(aVsyncTimestamp);
 }
 
 void
-CompositorVsyncScheduler::ScheduleTask(already_AddRefed<CancelableRunnable> aTask,
-                                       int aTime)
+CompositorVsyncScheduler::ScheduleTask(already_AddRefed<CancelableRunnable> aTask)
 {
   MOZ_ASSERT(CompositorThreadHolder::Loop());
-  MOZ_ASSERT(aTime >= 0);
-  CompositorThreadHolder::Loop()->PostDelayedTask(Move(aTask), aTime);
+  CompositorThreadHolder::Loop()->PostDelayedTask(Move(aTask), 0);
 }
 
 void
 CompositorVsyncScheduler::ResumeComposition()
 {
   MOZ_ASSERT(CompositorThreadHolder::IsInCompositorThread());
   mLastCompose = TimeStamp::Now();
   ComposeToTarget(nullptr);
diff --git a/gfx/layers/ipc/CompositorVsyncScheduler.h b/gfx/layers/ipc/CompositorVsyncScheduler.h
--- a/gfx/layers/ipc/CompositorVsyncScheduler.h
+++ b/gfx/layers/ipc/CompositorVsyncScheduler.h
@@ -46,17 +46,16 @@ class CompositorVsyncScheduler
 
 public:
   explicit CompositorVsyncScheduler(CompositorVsyncSchedulerOwner* aVsyncSchedulerOwner,
                                     widget::CompositorWidget* aWidget);
 
   bool NotifyVsync(TimeStamp aVsyncTimestamp);
   void SetNeedsComposite();
 
-  void ScheduleTask(already_AddRefed<CancelableRunnable>, int);
   void ResumeComposition();
   void ComposeToTarget(gfx::DrawTarget* aTarget, const gfx::IntRect* aRect = nullptr);
   void PostCompositeTask(TimeStamp aCompositeTimestamp);
   void PostVRTask(TimeStamp aTimestamp);
   void Destroy();
   void ScheduleComposition();
   void CancelCurrentCompositeTask();
   bool NeedsComposite();
@@ -73,16 +72,18 @@ public:
   {
     return mExpectedComposeStartTime;
   }
 #endif
 
 private:
   virtual ~CompositorVsyncScheduler();
 
+  // Schedule a task to run on the compositor thread.
+  void ScheduleTask(already_AddRefed<CancelableRunnable>);
   void NotifyCompositeTaskExecuted();
   void ObserveVsync();
   void UnobserveVsync();
   void DispatchTouchEvents(TimeStamp aVsyncTimestamp);
   void DispatchVREvents(TimeStamp aVsyncTimestamp);
   void CancelCurrentSetNeedsCompositeTask();
 
   class Observer final : public VsyncObserver
