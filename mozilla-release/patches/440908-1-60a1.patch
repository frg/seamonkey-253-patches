# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1520223514 -39600
# Node ID edb57fbc6beac233b12486fa7d7381220339de0f
# Parent  d6af7379beaab3408f3274ec10aaac3494704e1c
Bug 440908 - Remove gIsAnyPrefLocked. r=glandium

It optimizes Preferences::IsLocked(), but that function is called fewer than
200 times while starting the browser and opening a range of tabs.

MozReview-Commit-ID: 5q0zS8TSwdu

diff --git a/modules/libpref/Preferences.cpp b/modules/libpref/Preferences.cpp
--- a/modules/libpref/Preferences.cpp
+++ b/modules/libpref/Preferences.cpp
@@ -816,18 +816,16 @@ private:
 
 static PLDHashTable* gHashTable;
 
 // The callback list contains all the priority callbacks followed by the
 // non-priority callbacks. gLastPriorityNode records where the first part ends.
 static CallbackNode* gFirstCallback = nullptr;
 static CallbackNode* gLastPriorityNode = nullptr;
 
-static bool gIsAnyPrefLocked = false;
-
 // These are only used during the call to NotifyCallbacks().
 static bool gCallbacksInProgress = false;
 static bool gShouldCleanupDeadNodes = false;
 
 static PLDHashTableOps pref_HashTableOps = {
   PLDHashTable::HashStringKey, PrefEntry::MatchEntry,
   PLDHashTable::MoveEntryStub, PrefEntry::ClearEntry,
   PrefEntry::InitEntry,
@@ -4196,17 +4194,16 @@ Preferences::LockInAnyProcess(const char
 
   Pref* pref = pref_HashTableLookup(aPrefName);
   if (!pref) {
     return NS_ERROR_UNEXPECTED;
   }
 
   if (!pref->IsLocked()) {
     pref->SetIsLocked(true);
-    gIsAnyPrefLocked = true;
     NotifyCallbacks(aPrefName);
   }
 
   return NS_OK;
 }
 
 /* static */ nsresult
 Preferences::Lock(const char* aPrefName)
@@ -4234,24 +4231,18 @@ Preferences::Unlock(const char* aPrefNam
   return NS_OK;
 }
 
 /* static */ bool
 Preferences::IsLocked(const char* aPrefName)
 {
   NS_ENSURE_TRUE(InitStaticMembers(), false);
 
-  if (gIsAnyPrefLocked) {
-    Pref* pref = pref_HashTableLookup(aPrefName);
-    if (pref && pref->IsLocked()) {
-      return true;
-    }
-  }
-
-  return false;
+  Pref* pref = pref_HashTableLookup(aPrefName);
+  return pref && pref->IsLocked();
 }
 
 /* static */ nsresult
 Preferences::ClearUserInAnyProcess(const char* aPrefName)
 {
   NS_ENSURE_TRUE(InitStaticMembers(), NS_ERROR_NOT_AVAILABLE);
 
   PrefEntry* entry = pref_HashTableLookupInner(aPrefName);
