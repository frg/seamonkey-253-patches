# HG changeset patch
# User CJ Ku <cku@mozilla.com>
# Date 1517436985 21600
# Node ID 8b075bb31f5f203dc6d49ef9391a10344a5633cc
# Parent  b46fae25b139a74bbbb64296e83a501f7f10bc25
servo: Merge #19859 - Implement individual CSS transform properties: translate, rotate, scale (from birtles:individual-transforms); r=emilio

These are the servo-side changes for [Gecko bug 1207734](https://bugzilla.mozilla.org/show_bug.cgi?id=1207734).

- [X] `./mach build -d` does not report any errors
- [X] `./mach test-tidy` does not report any errors
- [X] These changes fix [Gecko bug 1207734](https://bugzilla.mozilla.org/show_bug.cgi?id=1207734)
- [X] There are tests for these changes on the Gecko side including web-platform-tests

Source-Repo: https://github.com/servo/servo
Source-Revision: 3e27459ad3c2d4c9d3582a16ae848f8c94578d6f

diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -3082,30 +3082,63 @@ fn static_assert() {
             % endfor
             _ => panic!("Found unexpected value for animation-${ident}"),
         }
     }
     ${impl_animation_count(ident, gecko_ffi_name)}
     ${impl_copy_animation_value(ident, gecko_ffi_name)}
 </%def>
 
+<%def name="impl_individual_transform(ident, type, gecko_ffi_name)">
+    pub fn set_${ident}(&mut self, other: values::computed::${type}) {
+        unsafe { self.gecko.${gecko_ffi_name}.clear() };
+
+        if let Some(operation) = other.to_transform_operation() {
+            convert_transform(&[operation], &mut self.gecko.${gecko_ffi_name})
+        }
+    }
+
+    pub fn copy_${ident}_from(&mut self, other: &Self) {
+        unsafe { self.gecko.${gecko_ffi_name}.set(&other.gecko.${gecko_ffi_name}); }
+    }
+
+    pub fn reset_${ident}(&mut self, other: &Self) {
+        self.copy_${ident}_from(other)
+    }
+
+    pub fn clone_${ident}(&self) -> values::computed::${type} {
+        use values::generics::transform::${type};
+
+        if self.gecko.${gecko_ffi_name}.mRawPtr.is_null() {
+            return ${type}::None;
+        }
+
+        let list = unsafe { (*self.gecko.${gecko_ffi_name}.to_safe().get()).mHead.as_ref() };
+
+        let mut transform = clone_transform_from_list(list);
+        debug_assert_eq!(transform.0.len(), 1);
+        ${type}::from_transform_operation(&transform.0.pop().unwrap())
+    }
+</%def>
+
 <% skip_box_longhands= """display overflow-y vertical-align
                           animation-name animation-delay animation-duration
                           animation-direction animation-fill-mode animation-play-state
                           animation-iteration-count animation-timing-function
                           transition-duration transition-delay
                           transition-timing-function transition-property
-                          page-break-before page-break-after
+                          page-break-before page-break-after rotate
                           scroll-snap-points-x scroll-snap-points-y
                           scroll-snap-type-x scroll-snap-type-y scroll-snap-coordinate
                           perspective-origin -moz-binding will-change
                           overscroll-behavior-x overscroll-behavior-y
                           overflow-clip-box-inline overflow-clip-box-block
                           perspective-origin -moz-binding will-change
-                          shape-outside contain touch-action""" %>
+                          shape-outside contain touch-action translate
+                          scale""" %>
 <%self:impl_trait style_struct_name="Box" skip_longhands="${skip_box_longhands}">
 
     // We manually-implement the |display| property until we get general
     // infrastructure for preffing certain values.
     <% display_keyword = Keyword("display", "inline block inline-block table inline-table table-row-group " +
                                             "table-header-group table-footer-group table-row table-column-group " +
                                             "table-column table-cell table-caption list-item flex none " +
                                             "inline-flex grid inline-grid ruby ruby-base ruby-base-container " +
@@ -3522,16 +3555,20 @@ fn static_assert() {
         T {
             horizontal: LengthOrPercentage::from_gecko_style_coord(&self.gecko.mPerspectiveOrigin[0])
                 .expect("Expected length or percentage for horizontal value of perspective-origin"),
             vertical: LengthOrPercentage::from_gecko_style_coord(&self.gecko.mPerspectiveOrigin[1])
                 .expect("Expected length or percentage for vertical value of perspective-origin"),
         }
     }
 
+    ${impl_individual_transform('rotate', 'Rotate', 'mSpecifiedRotate')}
+    ${impl_individual_transform('translate', 'Translate', 'mSpecifiedTranslate')}
+    ${impl_individual_transform('scale', 'Scale', 'mSpecifiedScale')}
+
     pub fn set_will_change(&mut self, v: longhands::will_change::computed_value::T) {
         use gecko_bindings::bindings::{Gecko_AppendWillChange, Gecko_ClearWillChange};
         use gecko_bindings::structs::NS_STYLE_WILL_CHANGE_OPACITY;
         use gecko_bindings::structs::NS_STYLE_WILL_CHANGE_SCROLL;
         use gecko_bindings::structs::NS_STYLE_WILL_CHANGE_TRANSFORM;
         use properties::PropertyId;
         use properties::longhands::will_change::computed_value::T;
 
diff --git a/servo/components/style/properties/helpers/animated_properties.mako.rs b/servo/components/style/properties/helpers/animated_properties.mako.rs
--- a/servo/components/style/properties/helpers/animated_properties.mako.rs
+++ b/servo/components/style/properties/helpers/animated_properties.mako.rs
@@ -41,17 +41,20 @@ use values::computed::{ClipRect, Context
 use values::computed::{Length, LengthOrPercentage, LengthOrPercentageOrAuto};
 use values::computed::{LengthOrPercentageOrNone, MaxLength};
 use values::computed::{NonNegativeNumber, Number, NumberOrPercentage, Percentage};
 use values::computed::length::NonNegativeLengthOrPercentage;
 use values::computed::ToComputedValue;
 use values::computed::transform::{DirectionVector, Matrix, Matrix3D};
 use values::computed::transform::TransformOperation as ComputedTransformOperation;
 use values::computed::transform::Transform as ComputedTransform;
-use values::generics::transform::{self, Transform, TransformOperation};
+use values::computed::transform::Rotate as ComputedRotate;
+use values::computed::transform::Translate as ComputedTranslate;
+use values::computed::transform::Scale as ComputedScale;
+use values::generics::transform::{self, Rotate, Translate, Scale, Transform, TransformOperation};
 use values::distance::{ComputeSquaredDistance, SquaredDistance};
 #[cfg(feature = "gecko")] use values::generics::FontSettings as GenericFontSettings;
 #[cfg(feature = "gecko")] use values::generics::FontSettingTag as GenericFontSettingTag;
 #[cfg(feature = "gecko")] use values::generics::FontSettingTagFloat;
 use values::generics::NonNegative;
 use values::generics::effects::Filter;
 use values::generics::position as generic_position;
 use values::generics::svg::{SVGLength,  SvgLengthOrPercentageOrNumber, SVGPaint};
@@ -1691,17 +1694,17 @@ pub struct Skew(f32, f32, f32);
 pub struct Perspective(f32, f32, f32, f32);
 
 /// A quaternion used to represent a rotation.
 #[derive(Clone, Copy, Debug)]
 #[cfg_attr(feature = "servo", derive(MallocSizeOf))]
 pub struct Quaternion(f64, f64, f64, f64);
 
 /// A decomposed 3d matrix.
-#[derive(Clone, ComputeSquaredDistance, Copy, Debug)]
+#[derive(Animate, Clone, ComputeSquaredDistance, Copy, Debug)]
 #[cfg_attr(feature = "servo", derive(MallocSizeOf))]
 pub struct MatrixDecomposed3D {
     /// A translation function.
     pub translate: Translate3D,
     /// A scale function.
     pub scale: Scale3D,
     /// The skew component of the transformation.
     pub skew: Skew,
@@ -1734,16 +1737,86 @@ impl Quaternion {
 
     /// Calculate the dot product.
     #[inline]
     fn dot(&self, other: &Self) -> f64 {
         self.0 * other.0 + self.1 * other.1 + self.2 * other.2 + self.3 * other.3
     }
 }
 
+impl Animate for Quaternion {
+    fn animate(&self, other: &Self, procedure: Procedure) -> Result<Self, ()> {
+        use std::f64;
+
+        let (this_weight, other_weight) = procedure.weights();
+        debug_assert!((this_weight + other_weight - 1.0f64).abs() <= f64::EPSILON ||
+                      other_weight == 1.0f64 || other_weight == 0.0f64,
+                      "animate should only be used for interpolating or accumulating transforms");
+
+        // We take a specialized code path for accumulation (where other_weight is 1)
+        if other_weight == 1.0 {
+            if this_weight == 0.0 {
+                return Ok(*other);
+            }
+
+            let clamped_w = self.3.min(1.0).max(-1.0);
+
+            // Determine the scale factor.
+            let mut theta = clamped_w.acos();
+            let mut scale = if theta == 0.0 { 0.0 } else { 1.0 / theta.sin() };
+            theta *= this_weight;
+            scale *= theta.sin();
+
+            // Scale the self matrix by this_weight.
+            let mut scaled_self = *self;
+            % for i in range(3):
+                scaled_self.${i} *= scale;
+            % endfor
+            scaled_self.3 = theta.cos();
+
+            // Multiply scaled-self by other.
+            let a = &scaled_self;
+            let b = other;
+            return Ok(Quaternion(
+                a.3 * b.0 + a.0 * b.3 + a.1 * b.2 - a.2 * b.1,
+                a.3 * b.1 - a.0 * b.2 + a.1 * b.3 + a.2 * b.0,
+                a.3 * b.2 + a.0 * b.1 - a.1 * b.0 + a.2 * b.3,
+                a.3 * b.3 - a.0 * b.0 - a.1 * b.1 - a.2 * b.2,
+            ));
+        }
+
+        let mut product = self.0 * other.0 +
+                          self.1 * other.1 +
+                          self.2 * other.2 +
+                          self.3 * other.3;
+
+        // Clamp product to -1.0 <= product <= 1.0
+        product = product.min(1.0);
+        product = product.max(-1.0);
+
+        if product == 1.0 {
+            return Ok(*self);
+        }
+
+        let theta = product.acos();
+        let w = (other_weight * theta).sin() * 1.0 / (1.0 - product * product).sqrt();
+
+        let mut a = *self;
+        let mut b = *other;
+        let mut result = Quaternion(0., 0., 0., 0.,);
+        % for i in range(4):
+            a.${i} *= (other_weight * theta).cos() - product * w;
+            b.${i} *= w;
+            result.${i} = a.${i} + b.${i};
+        % endfor
+
+        Ok(result)
+    }
+}
+
 impl ComputeSquaredDistance for Quaternion {
     #[inline]
     fn compute_squared_distance(&self, other: &Self) -> Result<SquaredDistance, ()> {
         // Use quaternion vectors to get the angle difference. Both q1 and q2 are unit vectors,
         // so we can get their angle difference by:
         // cos(theta/2) = (q1 dot q2) / (|q1| * |q2|) = q1 dot q2.
         let distance = self.dot(other).max(-1.0).min(1.0).acos() * 2.0;
         Ok(SquaredDistance::Value(distance * distance))
@@ -1997,97 +2070,16 @@ impl Animate for Perspective {
             self.0.animate(&other.0, procedure)?,
             self.1.animate(&other.1, procedure)?,
             self.2.animate(&other.2, procedure)?,
             animate_multiplicative_factor(self.3, other.3, procedure)?,
         ))
     }
 }
 
-impl Animate for MatrixDecomposed3D {
-    /// <https://drafts.csswg.org/css-transforms/#interpolation-of-decomposed-3d-matrix-values>
-    fn animate(&self, other: &Self, procedure: Procedure) -> Result<Self, ()> {
-        use std::f64;
-
-        let (this_weight, other_weight) = procedure.weights();
-
-        debug_assert!((this_weight + other_weight - 1.0f64).abs() <= f64::EPSILON ||
-                      other_weight == 1.0f64 || other_weight == 0.0f64,
-                      "animate should only be used for interpolating or accumulating transforms");
-
-        let mut sum = *self;
-
-        // Add translate, scale, skew and perspective components.
-        sum.translate = self.translate.animate(&other.translate, procedure)?;
-        sum.scale = self.scale.animate(&other.scale, procedure)?;
-        sum.skew = self.skew.animate(&other.skew, procedure)?;
-        sum.perspective = self.perspective.animate(&other.perspective, procedure)?;
-
-        // Add quaternions using spherical linear interpolation (Slerp).
-        //
-        // We take a specialized code path for accumulation (where other_weight is 1)
-        if other_weight == 1.0 {
-            if this_weight == 0.0 {
-                return Ok(*other)
-            }
-
-            let clamped_w = self.quaternion.3.min(1.0).max(-1.0);
-
-            // Determine the scale factor.
-            let mut theta = clamped_w.acos();
-            let mut scale = if theta == 0.0 { 0.0 } else { 1.0 / theta.sin() };
-            theta *= this_weight;
-            scale *= theta.sin();
-
-            // Scale the self matrix by this_weight.
-            let mut scaled_self = *self;
-            % for i in range(3):
-                scaled_self.quaternion.${i} *= scale;
-            % endfor
-            scaled_self.quaternion.3 = theta.cos();
-
-            // Multiply scaled-self by other.
-            let a = &scaled_self.quaternion;
-            let b = &other.quaternion;
-            sum.quaternion = Quaternion(
-                a.3 * b.0 + a.0 * b.3 + a.1 * b.2 - a.2 * b.1,
-                a.3 * b.1 - a.0 * b.2 + a.1 * b.3 + a.2 * b.0,
-                a.3 * b.2 + a.0 * b.1 - a.1 * b.0 + a.2 * b.3,
-                a.3 * b.3 - a.0 * b.0 - a.1 * b.1 - a.2 * b.2,
-            );
-        } else {
-            let mut product = self.quaternion.0 * other.quaternion.0 +
-                              self.quaternion.1 * other.quaternion.1 +
-                              self.quaternion.2 * other.quaternion.2 +
-                              self.quaternion.3 * other.quaternion.3;
-
-            // Clamp product to -1.0 <= product <= 1.0
-            product = product.min(1.0);
-            product = product.max(-1.0);
-
-            if product == 1.0 {
-                return Ok(sum);
-            }
-
-            let theta = product.acos();
-            let w = (other_weight * theta).sin() * 1.0 / (1.0 - product * product).sqrt();
-
-            let mut a = *self;
-            let mut b = *other;
-            % for i in range(4):
-                a.quaternion.${i} *= (other_weight * theta).cos() - product * w;
-                b.quaternion.${i} *= w;
-                sum.quaternion.${i} = a.quaternion.${i} + b.quaternion.${i};
-            % endfor
-        }
-
-        Ok(sum)
-    }
-}
-
 impl From<MatrixDecomposed3D> for Matrix3D {
     /// Recompose a 3D matrix.
     /// <https://drafts.csswg.org/css-transforms/#recomposing-to-a-3d-matrix>
     fn from(decomposed: MatrixDecomposed3D) -> Matrix3D {
         let mut matrix = Matrix3D::identity();
 
         // Apply perspective
         % for i in range(1, 5):
@@ -2275,16 +2267,139 @@ impl Matrix3D {
              self.m13*self.m21*self.m32 - self.m11*self.m23*self.m32 -
              self.m12*self.m21*self.m33 + self.m11*self.m22*self.m33),
         };
 
         Some(x)
     }
 }
 
+/// <https://drafts.csswg.org/css-transforms-2/#propdef-rotate>
+impl ComputedRotate {
+    fn fill_unspecified(rotate: &ComputedRotate) -> Result<(Number, Number, Number, Angle), ()> {
+        // According to the spec:
+        // https://drafts.csswg.org/css-transforms-2/#individual-transforms
+        //
+        // If the axis is unspecified, it defaults to "0 0 1"
+        match *rotate {
+            Rotate::None =>
+                Ok((0., 0., 1., Angle::zero())),
+            Rotate::Rotate3D(rx, ry, rz, angle) => Ok((rx, ry, rz, angle)),
+            Rotate::Rotate(angle) => Ok((0., 0., 1., angle)),
+        }
+    }
+}
+
+impl Animate for ComputedRotate {
+    #[inline]
+    fn animate(
+        &self,
+        other: &Self,
+        procedure: Procedure,
+    ) -> Result<Self, ()> {
+        let from = ComputedRotate::fill_unspecified(self)?;
+        let to = ComputedRotate::fill_unspecified(other)?;
+
+        let (fx, fy, fz, fa) = transform::get_normalized_vector_and_angle(from.0, from.1, from.2, from.3);
+        let (tx, ty, tz, ta) = transform::get_normalized_vector_and_angle(to.0, to.1, to.2, to.3);
+        if (fx, fy, fz) == (tx, ty, tz) {
+            return Ok(Rotate::Rotate3D(fx, fy, fz, fa.animate(&ta, procedure)?));
+        }
+
+        let fv = DirectionVector::new(fx, fy, fz);
+        let tv = DirectionVector::new(tx, ty, tz);
+        let fq = Quaternion::from_direction_and_angle(&fv, fa.radians64());
+        let tq = Quaternion::from_direction_and_angle(&tv, ta.radians64());
+
+        let rq = Quaternion::animate(&fq, &tq, procedure)?;
+        let (x, y, z, angle) =
+            transform::get_normalized_vector_and_angle(rq.0 as f32,
+                                                       rq.1 as f32,
+                                                       rq.2 as f32,
+                                                       rq.3.acos() as f32 *2.0);
+
+        Ok(Rotate::Rotate3D(x, y, z, Angle::from_radians(angle)))
+    }
+}
+
+/// <https://drafts.csswg.org/css-transforms-2/#propdef-translate>
+impl ComputedTranslate {
+    fn fill_unspecified(translate: &ComputedTranslate)
+        -> Result<(LengthOrPercentage, LengthOrPercentage, Length), ()> {
+        // According to the spec:
+        // https://drafts.csswg.org/css-transforms-2/#individual-transforms
+        //
+        // Unspecified translations default to 0px
+        match *translate {
+            Translate::None => {
+                Ok((LengthOrPercentage::Length(Length::zero()),
+                    LengthOrPercentage::Length(Length::zero()),
+                    Length::zero()))
+            },
+            Translate::Translate3D(tx, ty, tz) => Ok((tx, ty, tz)),
+            Translate::Translate(tx, ty) => Ok((tx, ty, Length::zero())),
+            Translate::TranslateX(tx) => Ok((tx, LengthOrPercentage::Length(Length::zero()), Length::zero())),
+        }
+    }
+}
+
+impl Animate for ComputedTranslate {
+    #[inline]
+    fn animate(
+        &self,
+        other: &Self,
+        procedure: Procedure,
+    ) -> Result<Self, ()> {
+        let from = ComputedTranslate::fill_unspecified(self)?;
+        let to = ComputedTranslate::fill_unspecified(other)?;
+
+        Ok(Translate::Translate3D(from.0.animate(&to.0, procedure)?,
+                                  from.1.animate(&to.1, procedure)?,
+                                  from.2.animate(&to.2, procedure)?))
+    }
+}
+
+/// <https://drafts.csswg.org/css-transforms-2/#propdef-scale>
+impl ComputedScale {
+    fn fill_unspecified(scale: &ComputedScale)
+        -> Result<(Number, Number, Number), ()> {
+        // According to the spec:
+        // https://drafts.csswg.org/css-transforms-2/#individual-transforms
+        //
+        // Unspecified scales default to 1
+        match *scale {
+            Scale::None => Ok((1.0, 1.0, 1.0)),
+            Scale::Scale3D(sx, sy, sz) => Ok((sx, sy, sz)),
+            Scale::Scale(sx, sy) => Ok((sx, sy, 1.)),
+            Scale::ScaleX(sx) => Ok((sx, 1., 1.)),
+        }
+    }
+}
+
+impl Animate for ComputedScale {
+    #[inline]
+    fn animate(
+        &self,
+        other: &Self,
+        procedure: Procedure,
+    ) -> Result<Self, ()> {
+        let from = ComputedScale::fill_unspecified(self)?;
+        let to = ComputedScale::fill_unspecified(other)?;
+
+        if procedure == Procedure::Add {
+            // scale(x1,y1,z1)*scale(x2,y2,z2) = scale(x1*x2, y1*y2, z1*z2)
+            return Ok(Scale::Scale3D(from.0 * to.0, from.1 * to.1, from.2 * to.2));
+        }
+
+        Ok(Scale::Scale3D(animate_multiplicative_factor(from.0, to.0, procedure)?,
+                          animate_multiplicative_factor(from.1, to.1, procedure)?,
+                          animate_multiplicative_factor(from.2, to.2, procedure)?))
+    }
+}
+
 /// <https://drafts.csswg.org/css-transforms/#interpolation-of-transforms>
 impl Animate for ComputedTransform {
     #[inline]
     fn animate(
         &self,
         other_: &Self,
         procedure: Procedure,
     ) -> Result<Self, ()> {
diff --git a/servo/components/style/properties/longhand/box.mako.rs b/servo/components/style/properties/longhand/box.mako.rs
--- a/servo/components/style/properties/longhand/box.mako.rs
+++ b/servo/components/style/properties/longhand/box.mako.rs
@@ -385,16 +385,40 @@
 ${helpers.predefined_type("transform", "Transform",
                           "generics::transform::Transform::none()",
                           extra_prefixes="webkit",
                           animation_value_type="ComputedValue",
                           gecko_ffi_name="mSpecifiedTransform",
                           flags="CREATES_STACKING_CONTEXT FIXPOS_CB",
                           spec="https://drafts.csswg.org/css-transforms/#propdef-transform")}
 
+${helpers.predefined_type("rotate", "Rotate",
+                          "generics::transform::Rotate::None",
+                          animation_value_type="ComputedValue",
+                          boxed=True,
+                          flags="CREATES_STACKING_CONTEXT FIXPOS_CB",
+                          gecko_pref="layout.css.individual-transform.enabled",
+                          spec="https://drafts.csswg.org/css-transforms-2/#individual-transforms")}
+
+${helpers.predefined_type("scale", "Scale",
+                          "generics::transform::Scale::None",
+                          animation_value_type="ComputedValue",
+                          boxed=True,
+                          flags="CREATES_STACKING_CONTEXT FIXPOS_CB",
+                          gecko_pref="layout.css.individual-transform.enabled",
+                          spec="https://drafts.csswg.org/css-transforms-2/#individual-transforms")}
+
+${helpers.predefined_type("translate", "Translate",
+                          "generics::transform::Translate::None",
+                          animation_value_type="ComputedValue",
+                          boxed=True,
+                          flags="CREATES_STACKING_CONTEXT FIXPOS_CB",
+                          gecko_pref="layout.css.individual-transform.enabled",
+                          spec="https://drafts.csswg.org/css-transforms-2/#individual-transforms")}
+
 // CSSOM View Module
 // https://www.w3.org/TR/cssom-view-1/
 ${helpers.single_keyword("scroll-behavior",
                          "auto smooth",
                          gecko_pref="layout.css.scroll-behavior.property-enabled",
                          products="gecko",
                          spec="https://drafts.csswg.org/cssom-view/#propdef-scroll-behavior",
                          animation_value_type="discrete")}
diff --git a/servo/components/style/values/computed/mod.rs b/servo/components/style/values/computed/mod.rs
--- a/servo/components/style/values/computed/mod.rs
+++ b/servo/components/style/values/computed/mod.rs
@@ -68,17 +68,17 @@ pub use self::pointing::Cursor;
 #[cfg(feature = "gecko")]
 pub use self::pointing::CursorImage;
 pub use self::svg::{SVGLength, SVGOpacity, SVGPaint, SVGPaintKind};
 pub use self::svg::{SVGPaintOrder, SVGStrokeDashArray, SVGWidth};
 pub use self::svg::MozContextProperties;
 pub use self::table::XSpan;
 pub use self::text::{InitialLetter, LetterSpacing, LineHeight, TextAlign, TextOverflow, WordSpacing};
 pub use self::time::Time;
-pub use self::transform::{TimingFunction, Transform, TransformOperation, TransformOrigin};
+pub use self::transform::{TimingFunction, Transform, TransformOperation, TransformOrigin, Rotate, Translate, Scale};
 pub use self::ui::MozForceBrokenImageIcon;
 
 #[cfg(feature = "gecko")]
 pub mod align;
 pub mod angle;
 pub mod background;
 pub mod basic_shape;
 pub mod border;
diff --git a/servo/components/style/values/computed/transform.rs b/servo/components/style/values/computed/transform.rs
--- a/servo/components/style/values/computed/transform.rs
+++ b/servo/components/style/values/computed/transform.rs
@@ -7,18 +7,21 @@
 use euclid::{Transform3D, Vector3D};
 use num_traits::Zero;
 use super::{CSSFloat, Either};
 use values::animated::ToAnimatedZero;
 use values::computed::{Angle, Integer, Length, LengthOrPercentage, Number, Percentage};
 use values::computed::{LengthOrNumber, LengthOrPercentageOrNumber};
 use values::generics::transform::{self, Matrix as GenericMatrix, Matrix3D as GenericMatrix3D};
 use values::generics::transform::{Transform as GenericTransform, TransformOperation as GenericTransformOperation};
+use values::generics::transform::Rotate as GenericRotate;
+use values::generics::transform::Scale as GenericScale;
 use values::generics::transform::TimingFunction as GenericTimingFunction;
 use values::generics::transform::TransformOrigin as GenericTransformOrigin;
+use values::generics::transform::Translate as GenericTranslate;
 
 /// A single operation in a computed CSS `transform`
 pub type TransformOperation = GenericTransformOperation<
     Angle,
     Number,
     Length,
     Integer,
     LengthOrNumber,
@@ -288,8 +291,82 @@ impl ToAnimatedZero for Transform {
     #[inline]
     fn to_animated_zero(&self) -> Result<Self, ()> {
         Ok(GenericTransform(self.0
             .iter()
             .map(|op| op.to_animated_zero())
             .collect::<Result<Vec<_>, _>>()?))
     }
 }
+
+/// A computed CSS `rotate`
+pub type Rotate = GenericRotate<Number, Angle>;
+
+impl Rotate {
+    /// Convert TransformOperation to Rotate.
+    pub fn to_transform_operation(&self) -> Option<TransformOperation> {
+        match *self {
+            GenericRotate::None => None,
+            GenericRotate::Rotate(angle) => Some(GenericTransformOperation::Rotate(angle)),
+            GenericRotate::Rotate3D(rx, ry, rz, angle) => Some(GenericTransformOperation::Rotate3D(rx, ry, rz, angle)),
+        }
+    }
+
+    /// Convert Rotate to TransformOperation.
+    pub fn from_transform_operation(operation: &TransformOperation) -> Rotate {
+        match *operation {
+            GenericTransformOperation::Rotate(angle) => GenericRotate::Rotate(angle),
+            GenericTransformOperation::Rotate3D(rx, ry, rz, angle) =>
+                GenericRotate::Rotate3D(rx, ry, rz, angle),
+            _ => unreachable!("Found unexpected value for rotate property"),
+        }
+    }
+}
+
+/// A computed CSS `translate`
+pub type Translate = GenericTranslate<LengthOrPercentage, Length>;
+
+impl Translate {
+    /// Convert TransformOperation to Translate.
+    pub fn to_transform_operation(&self) -> Option<TransformOperation> {
+        match *self {
+            GenericTranslate::None => None,
+            GenericTranslate::TranslateX(tx) => Some(GenericTransformOperation::TranslateX(tx)),
+            GenericTranslate::Translate(tx, ty) => Some(GenericTransformOperation::Translate(tx, Some(ty))),
+            GenericTranslate::Translate3D(tx, ty, tz) => Some(GenericTransformOperation::Translate3D(tx, ty, tz)),
+        }
+    }
+
+    /// Convert Translate to TransformOperation.
+    pub fn from_transform_operation(operation: &TransformOperation) -> Translate {
+        match *operation {
+            GenericTransformOperation::TranslateX(tx) => GenericTranslate::TranslateX(tx),
+            GenericTransformOperation::Translate(tx, Some(ty)) => GenericTranslate::Translate(tx, ty),
+            GenericTransformOperation::Translate3D(tx, ty, tz) => GenericTranslate::Translate3D(tx, ty, tz),
+            _ => unreachable!("Found unexpected value for translate"),
+        }
+    }
+}
+
+/// A computed CSS `scale`
+pub type Scale = GenericScale<Number>;
+
+impl Scale {
+    /// Convert TransformOperation to Scale.
+    pub fn to_transform_operation(&self) -> Option<TransformOperation> {
+        match *self {
+            GenericScale::None => None,
+            GenericScale::ScaleX(sx) => Some(GenericTransformOperation::ScaleX(sx)),
+            GenericScale::Scale(sx, sy) => Some(GenericTransformOperation::Scale(sx, Some(sy))),
+            GenericScale::Scale3D(sx, sy, sz) => Some(GenericTransformOperation::Scale3D(sx, sy, sz)),
+        }
+    }
+
+    /// Convert Scale to TransformOperation.
+    pub fn from_transform_operation(operation: &TransformOperation) -> Scale {
+        match *operation {
+            GenericTransformOperation::ScaleX(sx) => GenericScale::ScaleX(sx),
+            GenericTransformOperation::Scale(sx, Some(sy)) => GenericScale::Scale(sx, sy),
+            GenericTransformOperation::Scale3D(sx, sy, sz) => GenericScale::Scale3D(sx, sy, sz),
+            _ => unreachable!("Found unexpected value for scale"),
+        }
+    }
+}
diff --git a/servo/components/style/values/generics/transform.rs b/servo/components/style/values/generics/transform.rs
--- a/servo/components/style/values/generics/transform.rs
+++ b/servo/components/style/values/generics/transform.rs
@@ -663,8 +663,54 @@ pub fn get_normalized_vector_and_angle<T
         // A direction vector that cannot be normalized, such as [0, 0, 0], will cause the
         // rotation to not be applied, so we use identity matrix (i.e. rotate3d(0, 0, 1, 0)).
         (0., 0., 1., T::zero())
     } else {
         let vector = vector.normalize();
         (vector.x, vector.y, vector.z, angle)
     }
 }
+
+#[derive(ComputeSquaredDistance, ToAnimatedZero, ToComputedValue)]
+#[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
+/// A value of the `Rotate` property
+///
+/// <https://drafts.csswg.org/css-transforms-2/#individual-transforms>
+pub enum Rotate<Number, Angle> {
+    /// 'none'
+    None,
+    /// '<angle>'
+    Rotate(Angle),
+    /// '<number>{3} <angle>'
+    Rotate3D(Number, Number, Number, Angle),
+}
+
+#[derive(ComputeSquaredDistance, ToAnimatedZero, ToComputedValue)]
+#[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
+/// A value of the `Scale` property
+///
+/// <https://drafts.csswg.org/css-transforms-2/#individual-transforms>
+pub enum Scale<Number> {
+    /// 'none'
+    None,
+    /// '<number>'
+    ScaleX(Number),
+    /// '<number>{2}'
+    Scale(Number, Number),
+    /// '<number>{3}'
+    Scale3D(Number, Number, Number),
+}
+
+#[derive(ComputeSquaredDistance, ToAnimatedZero, ToComputedValue)]
+#[derive(Clone, Debug, MallocSizeOf, PartialEq, ToCss)]
+/// A value of the `Translate` property
+///
+/// <https://drafts.csswg.org/css-transforms-2/#individual-transforms>
+pub enum Translate<LengthOrPercentage, Length> {
+    /// 'none'
+    None,
+    /// '<length-percentage>'
+    TranslateX(LengthOrPercentage),
+    /// '<length-percentage> <length-percentage>'
+    Translate(LengthOrPercentage, LengthOrPercentage),
+    /// '<length-percentage> <length-percentage> <length>'
+    Translate3D(LengthOrPercentage, LengthOrPercentage, Length),
+}
diff --git a/servo/components/style/values/specified/mod.rs b/servo/components/style/values/specified/mod.rs
--- a/servo/components/style/values/specified/mod.rs
+++ b/servo/components/style/values/specified/mod.rs
@@ -64,17 +64,17 @@ pub use self::pointing::Cursor;
 pub use self::pointing::CursorImage;
 pub use self::svg::{SVGLength, SVGOpacity, SVGPaint, SVGPaintKind};
 pub use self::svg::{SVGPaintOrder, SVGStrokeDashArray, SVGWidth};
 pub use self::svg::MozContextProperties;
 pub use self::table::XSpan;
 pub use self::text::{InitialLetter, LetterSpacing, LineHeight, TextDecorationLine};
 pub use self::text::{TextAlign, TextAlignKeyword, TextOverflow, WordSpacing};
 pub use self::time::Time;
-pub use self::transform::{TimingFunction, Transform, TransformOrigin};
+pub use self::transform::{TimingFunction, Transform, TransformOrigin, Rotate, Translate, Scale};
 pub use self::ui::MozForceBrokenImageIcon;
 pub use super::generics::grid::GridTemplateComponent as GenericGridTemplateComponent;
 
 #[cfg(feature = "gecko")]
 pub mod align;
 pub mod angle;
 pub mod background;
 pub mod basic_shape;
diff --git a/servo/components/style/values/specified/transform.rs b/servo/components/style/values/specified/transform.rs
--- a/servo/components/style/values/specified/transform.rs
+++ b/servo/components/style/values/specified/transform.rs
@@ -9,17 +9,20 @@ use parser::{Parse, ParserContext};
 use selectors::parser::SelectorParseErrorKind;
 use style_traits::{ParseError, StyleParseErrorKind};
 use values::computed::{Context, LengthOrPercentage as ComputedLengthOrPercentage};
 use values::computed::{Percentage as ComputedPercentage, ToComputedValue};
 use values::computed::transform::TimingFunction as ComputedTimingFunction;
 use values::generics::transform::{Matrix3D, Transform as GenericTransform};
 use values::generics::transform::{StepPosition, TimingFunction as GenericTimingFunction, Matrix};
 use values::generics::transform::{TimingKeyword, TransformOrigin as GenericTransformOrigin};
+use values::generics::transform::Rotate as GenericRotate;
+use values::generics::transform::Scale as GenericScale;
 use values::generics::transform::TransformOperation as GenericTransformOperation;
+use values::generics::transform::Translate as GenericTranslate;
 use values::specified::{self, Angle, Number, Length, Integer};
 use values::specified::{LengthOrNumber, LengthOrPercentage, LengthOrPercentageOrNumber};
 use values::specified::position::{Side, X, Y};
 
 /// A single operation in a specified CSS `transform`
 pub type TransformOperation = GenericTransformOperation<
     Angle,
     Number,
@@ -503,8 +506,90 @@ impl ToComputedValue for TimingFunction 
                 GenericTimingFunction::Steps(Integer::from_computed_value(&(steps as i32)), position)
             },
             GenericTimingFunction::Frames(frames) => {
                 GenericTimingFunction::Frames(Integer::from_computed_value(&(frames as i32)))
             },
         }
     }
 }
+
+/// A specified CSS `rotate`
+pub type Rotate = GenericRotate<Number, Angle>;
+
+impl Parse for Rotate {
+    fn parse<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>
+    ) -> Result<Self, ParseError<'i>> {
+        if input.try(|i| i.expect_ident_matching("none")).is_ok() {
+            return Ok(GenericRotate::None);
+        }
+
+        if let Ok(rx) = input.try(|i| Number::parse(context, i)) {
+            // 'rotate: <number>{3} <angle>'
+            let ry = Number::parse(context, input)?;
+            let rz = Number::parse(context, input)?;
+            let angle = specified::Angle::parse(context, input)?;
+            return Ok(GenericRotate::Rotate3D(rx, ry, rz, angle));
+        }
+
+        // 'rotate: <angle>'
+        let angle = specified::Angle::parse(context, input)?;
+        Ok(GenericRotate::Rotate(angle))
+    }
+}
+
+/// A specified CSS `translate`
+pub type Translate = GenericTranslate<LengthOrPercentage, Length>;
+
+impl Parse for Translate {
+    fn parse<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>
+    ) -> Result<Self, ParseError<'i>> {
+        if input.try(|i| i.expect_ident_matching("none")).is_ok() {
+            return Ok(GenericTranslate::None);
+        }
+
+        let tx = specified::LengthOrPercentage::parse(context, input)?;
+        if let Ok(ty) = input.try(|i| specified::LengthOrPercentage::parse(context, i)) {
+            if let Ok(tz) = input.try(|i| specified::Length::parse(context, i)) {
+                // 'translate: <length-percentage> <length-percentage> <length>'
+                return Ok(GenericTranslate::Translate3D(tx, ty, tz));
+            }
+
+            // translate: <length-percentage> <length-percentage>'
+            return Ok(GenericTranslate::Translate(tx, ty));
+        }
+
+        // 'translate: <length-percentage> '
+        Ok(GenericTranslate::TranslateX(tx))
+    }
+}
+
+/// A specified CSS `scale`
+pub type Scale = GenericScale<Number>;
+
+impl Parse for Scale {
+    fn parse<'i, 't>(
+        context: &ParserContext,
+        input: &mut Parser<'i, 't>
+    ) -> Result<Self, ParseError<'i>> {
+        if input.try(|i| i.expect_ident_matching("none")).is_ok() {
+            return Ok(GenericScale::None);
+        }
+
+        let sx = Number::parse(context, input)?;
+        if let Ok(sy) = input.try(|i| Number::parse(context, i)) {
+            if let Ok(sz) = input.try(|i| Number::parse(context, i)) {
+                // 'scale: <number> <number> <number>'
+                return Ok(GenericScale::Scale3D(sx, sy, sz));
+            }
+
+            // 'scale: <number> <number>'
+            return Ok(GenericScale::Scale(sx, sy));
+        }
+
+        // 'scale: <number>'
+        Ok(GenericScale::ScaleX(sx))
+    }
+}
