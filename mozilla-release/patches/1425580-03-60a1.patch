# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1517924247 -3600
#      Tue Feb 06 14:37:27 2018 +0100
# Node ID 842b589abd9ca0563f14fd2e786f05a34c44e6bb
# Parent  38d736b0c4f64f31dcd620ec65fdc6dd9ef9133d
Bug 1425580 part 3 - Devirtualize LNode::numDefs. r=bbouvier

diff --git a/js/src/jit/LIR.h b/js/src/jit/LIR.h
--- a/js/src/jit/LIR.h
+++ b/js/src/jit/LIR.h
@@ -661,26 +661,29 @@ class LNode
 
   private:
     LBlock* block_;
     uint32_t id_;
 
   protected:
     // Bitfields below are all uint32_t to make sure MSVC packs them correctly.
     uint32_t isCall_ : 1;
+    uint32_t numDefs_ : 4;
     uint32_t numTemps_ : 4;
 
   public:
-    explicit LNode(uint32_t numTemps)
+    LNode(uint32_t numDefs, uint32_t numTemps)
       : mir_(nullptr),
         block_(nullptr),
         id_(0),
         isCall_(false),
+        numDefs_(numDefs),
         numTemps_(numTemps)
     {
+        MOZ_ASSERT(numDefs_ == numDefs, "numDefs must fit in bitfield");
         MOZ_ASSERT(numTemps_ == numTemps, "numTemps must fit in bitfield");
     }
 
     enum Opcode {
 #   define LIROP(name) LOp_##name,
         LIR_OPCODE_LIST(LIROP)
 #   undef LIROP
         LOp_Invalid
@@ -708,17 +711,19 @@ class LNode
     bool isInstruction() const {
         return op() != LOp_Phi;
     }
     inline LInstruction* toInstruction();
     inline const LInstruction* toInstruction() const;
 
     // Returns the number of outputs of this instruction. If an output is
     // unallocated, it is an LDefinition, defining a virtual register.
-    virtual size_t numDefs() const = 0;
+    size_t numDefs() const {
+        return numDefs_;
+    }
     virtual LDefinition* getDef(size_t index) = 0;
     virtual void setDef(size_t index, const LDefinition& def) = 0;
 
     // Returns information about operands.
     virtual size_t numOperands() const = 0;
     virtual LAllocation* getOperand(size_t index) = 0;
     virtual void setOperand(size_t index, const LAllocation& a) = 0;
 
@@ -817,18 +822,18 @@ class LInstruction
     // to hold either gcthings or Values.
     LSafepoint* safepoint_;
 
     LMoveGroup* inputMoves_;
     LMoveGroup* fixReuseMoves_;
     LMoveGroup* movesAfter_;
 
   protected:
-    explicit LInstruction(uint32_t numTemps)
-      : LNode(numTemps),
+    LInstruction(uint32_t numDefs, uint32_t numTemps)
+      : LNode(numDefs, numTemps),
         snapshot_(nullptr),
         safepoint_(nullptr),
         inputMoves_(nullptr),
         fixReuseMoves_(nullptr),
         movesAfter_(nullptr)
     { }
 
     void setIsCall() {
@@ -927,25 +932,22 @@ class LPhi final : public LNode
 {
     LAllocation* const inputs_;
     LDefinition def_;
 
   public:
     LIR_HEADER(Phi)
 
     LPhi(MPhi* ins, LAllocation* inputs)
-      : LNode(/* numTemps = */ 0),
+      : LNode(/* numDefs = */ 1, /* numTemps = */ 0),
         inputs_(inputs)
     {
         setMir(ins);
     }
 
-    size_t numDefs() const override {
-        return 1;
-    }
     LDefinition* getDef(size_t index) override {
         MOZ_ASSERT(index == 0);
         return &def_;
     }
     void setDef(size_t index, const LDefinition& def) override {
         MOZ_ASSERT(index == 0);
         def_ = def;
     }
@@ -1078,23 +1080,20 @@ namespace details {
     template <size_t Defs, size_t Temps>
     class LInstructionFixedDefsTempsHelper : public LInstruction
     {
         mozilla::Array<LDefinition, Defs> defs_;
         mozilla::Array<LDefinition, Temps> temps_;
 
       protected:
         LInstructionFixedDefsTempsHelper()
-          : LInstruction(Temps)
+          : LInstruction(Defs, Temps)
         {}
 
       public:
-        size_t numDefs() const final override {
-            return Defs;
-        }
         LDefinition* getDef(size_t index) final override {
             return &defs_[index];
         }
         LDefinition* getTemp(size_t index) final override {
             return &temps_[index];
         }
 
         void setDef(size_t index, const LDefinition& def) final override {
diff --git a/js/src/jit/Lowering.cpp b/js/src/jit/Lowering.cpp
--- a/js/src/jit/Lowering.cpp
+++ b/js/src/jit/Lowering.cpp
@@ -4664,20 +4664,22 @@ LIRGenerator::visitWasmCall(MWasmCall* i
             if (uint32_t(index->toConstant()->toInt32()) < ins->callee().wasmTableMinLength())
                 needsBoundsCheck = false;
         }
 
         args[ins->numArgs()] = useFixedAtStart(index, WasmTableCallIndexReg);
     }
 
     LInstruction* lir;
-    if (ins->type() == MIRType::Int64)
+    if (ins->type() == MIRType::Int64) {
         lir = new(alloc()) LWasmCallI64(args, ins->numOperands(), needsBoundsCheck);
-    else
-        lir = new(alloc()) LWasmCall(args, ins->numOperands(), needsBoundsCheck);
+    } else {
+        uint32_t numDefs = (ins->type() != MIRType::None) ? 1 : 0;
+        lir = new(alloc()) LWasmCall(args, ins->numOperands(), numDefs, needsBoundsCheck);
+    }
 
     if (ins->type() == MIRType::None)
         add(lir, ins);
     else
         defineReturn(lir, ins);
 }
 
 void
diff --git a/js/src/jit/shared/LIR-shared.h b/js/src/jit/shared/LIR-shared.h
--- a/js/src/jit/shared/LIR-shared.h
+++ b/js/src/jit/shared/LIR-shared.h
@@ -8999,18 +8999,19 @@ class LWasmStackArgI64 : public LInstruc
 class LWasmCallBase : public LInstruction
 {
     LAllocation* operands_;
     uint32_t numOperands_;
     uint32_t needsBoundsCheck_;
 
   public:
 
-    LWasmCallBase(LAllocation* operands, uint32_t numOperands, bool needsBoundsCheck)
-      : LInstruction(/* numTemps = */ 0),
+    LWasmCallBase(LAllocation* operands, uint32_t numOperands, uint32_t numDefs,
+                  bool needsBoundsCheck)
+      : LInstruction(numDefs, /* numTemps = */ 0),
         operands_(operands),
         numOperands_(numOperands),
         needsBoundsCheck_(needsBoundsCheck)
     {
         setIsCall();
     }
 
     MWasmCall* mir() const {
@@ -9060,25 +9061,22 @@ class LWasmCallBase : public LInstructio
 
 class LWasmCall : public LWasmCallBase
 {
      LDefinition def_;
 
   public:
     LIR_HEADER(WasmCall);
 
-    LWasmCall(LAllocation* operands, uint32_t numOperands, bool needsBoundsCheck)
-      : LWasmCallBase(operands, numOperands, needsBoundsCheck),
+    LWasmCall(LAllocation* operands, uint32_t numOperands, uint32_t numDefs, bool needsBoundsCheck)
+      : LWasmCallBase(operands, numOperands, numDefs, needsBoundsCheck),
         def_(LDefinition::BogusTemp())
     {}
 
     // LInstruction interface
-    size_t numDefs() const override {
-        return def_.isBogusTemp() ? 0 : 1;
-    }
     LDefinition* getDef(size_t index) override {
         MOZ_ASSERT(numDefs() == 1);
         MOZ_ASSERT(index == 0);
         return &def_;
     }
     void setDef(size_t index, const LDefinition& def) override {
         MOZ_ASSERT(index == 0);
         def_ = def;
@@ -9088,26 +9086,23 @@ class LWasmCall : public LWasmCallBase
 class LWasmCallI64 : public LWasmCallBase
 {
     LDefinition defs_[INT64_PIECES];
 
   public:
     LIR_HEADER(WasmCallI64);
 
     LWasmCallI64(LAllocation* operands, uint32_t numOperands, bool needsBoundsCheck)
-      : LWasmCallBase(operands, numOperands, needsBoundsCheck)
+      : LWasmCallBase(operands, numOperands, INT64_PIECES, needsBoundsCheck)
     {
         for (size_t i = 0; i < numDefs(); i++)
             defs_[i] = LDefinition::BogusTemp();
     }
 
     // LInstruction interface
-    size_t numDefs() const override {
-        return INT64_PIECES;
-    }
     LDefinition* getDef(size_t index) override {
         MOZ_ASSERT(index < numDefs());
         return &defs_[index];
     }
     void setDef(size_t index, const LDefinition& def) override {
         MOZ_ASSERT(index < numDefs());
         defs_[index] = def;
     }
