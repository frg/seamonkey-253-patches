# HG changeset patch
# User Ben Kelly <ben@wanderview.com>
# Date 1517418626 28800
#      Wed Jan 31 09:10:26 2018 -0800
# Node ID edfc00999a3afac47595e1389f35d8b7903739b6
# Parent  af4ebf511432483fad19cbfcad601e49454666d8
Bug 1434342 P5 Support caching the ServiceWorker DOM instance on the global. r=asuth

diff --git a/dom/base/nsGlobalWindow.cpp b/dom/base/nsGlobalWindow.cpp
--- a/dom/base/nsGlobalWindow.cpp
+++ b/dom/base/nsGlobalWindow.cpp
@@ -244,16 +244,17 @@
 #include "mozilla/dom/PrimitiveConversions.h"
 #include "mozilla/dom/WindowBinding.h"
 #include "nsITabChild.h"
 #include "mozilla/dom/MediaQueryList.h"
 #include "mozilla/dom/ScriptSettings.h"
 #include "mozilla/dom/NavigatorBinding.h"
 #include "mozilla/dom/ImageBitmap.h"
 #include "mozilla/dom/ImageBitmapBinding.h"
+#include "mozilla/dom/ServiceWorker.h"
 #include "mozilla/dom/ServiceWorkerRegistration.h"
 #include "mozilla/dom/U2F.h"
 #include "mozilla/dom/WebIDLGlobalNameHash.h"
 #include "mozilla/dom/Worklet.h"
 #include "AccessCheck.h"
 
 #ifdef HAVE_SIDEBAR
 #include "mozilla/dom/ExternalBinding.h"
@@ -4503,16 +4504,22 @@ nsPIDOMWindowInner::GetClientState() con
 }
 
 Maybe<ServiceWorkerDescriptor>
 nsPIDOMWindowInner::GetController() const
 {
   return Move(nsGlobalWindow::Cast(this)->GetController());
 }
 
+RefPtr<mozilla::dom::ServiceWorker>
+nsPIDOMWindowInner::GetOrCreateServiceWorker(const mozilla::dom::ServiceWorkerDescriptor& aDescriptor)
+{
+  return Move(nsGlobalWindow::Cast(this)->GetOrCreateServiceWorker(aDescriptor));
+}
+
 void
 nsPIDOMWindowInner::NoteCalledRegisterForServiceWorkerScope(const nsACString& aScope)
 {
   nsGlobalWindow::Cast(this)->NoteCalledRegisterForServiceWorkerScope(aScope);
 }
 
 bool
 nsGlobalWindow::ShouldReportForServiceWorkerScope(const nsAString& aScope)
@@ -12840,16 +12847,49 @@ nsGlobalWindow::GetController() const
   MOZ_ASSERT(NS_IsMainThread());
   Maybe<ServiceWorkerDescriptor> controller;
   if (mClientSource) {
     controller = mClientSource->GetController();
   }
   return Move(controller);
 }
 
+RefPtr<ServiceWorker>
+nsGlobalWindow::GetOrCreateServiceWorker(const ServiceWorkerDescriptor& aDescriptor)
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  RefPtr<ServiceWorker> ref;
+  for (auto sw : mServiceWorkerList) {
+    if (sw->MatchesDescriptor(aDescriptor)) {
+      ref = sw;
+      return ref.forget();
+    }
+  }
+  ref = ServiceWorker::Create(this, aDescriptor);
+  return ref.forget();
+}
+
+void
+nsGlobalWindow::AddServiceWorker(ServiceWorker* aServiceWorker)
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  MOZ_DIAGNOSTIC_ASSERT(aServiceWorker);
+  MOZ_ASSERT(!mServiceWorkerList.Contains(aServiceWorker));
+  mServiceWorkerList.AppendElement(aServiceWorker);
+}
+
+void
+nsGlobalWindow::RemoveServiceWorker(ServiceWorker* aServiceWorker)
+{
+  MOZ_ASSERT(NS_IsMainThread());
+  MOZ_DIAGNOSTIC_ASSERT(aServiceWorker);
+  MOZ_ASSERT(mServiceWorkerList.Contains(aServiceWorker));
+  mServiceWorkerList.RemoveElement(aServiceWorker);
+}
+
 nsresult
 nsGlobalWindow::FireDelayedDOMEvents()
 {
   FORWARD_TO_INNER(FireDelayedDOMEvents, (), NS_ERROR_UNEXPECTED);
 
   if (mApplicationCache) {
     static_cast<nsDOMOfflineResourceList*>(mApplicationCache.get())->FirePendingEvents();
   }
diff --git a/dom/base/nsGlobalWindow.h b/dom/base/nsGlobalWindow.h
--- a/dom/base/nsGlobalWindow.h
+++ b/dom/base/nsGlobalWindow.h
@@ -403,16 +403,25 @@ public:
   void Thaw();
   virtual bool IsFrozen() const override;
   void SyncStateFromParentWindow();
 
   mozilla::Maybe<mozilla::dom::ClientInfo> GetClientInfo() const override;
   mozilla::Maybe<mozilla::dom::ClientState> GetClientState() const;
   mozilla::Maybe<mozilla::dom::ServiceWorkerDescriptor> GetController() const override;
 
+  virtual RefPtr<mozilla::dom::ServiceWorker>
+  GetOrCreateServiceWorker(const mozilla::dom::ServiceWorkerDescriptor& aDescriptor) override;
+
+  virtual void
+  AddServiceWorker(mozilla::dom::ServiceWorker* aServiceWorker) override;
+
+  virtual void
+  RemoveServiceWorker(mozilla::dom::ServiceWorker* aServiceWorker) override;
+
   void NoteCalledRegisterForServiceWorkerScope(const nsACString& aScope);
 
   virtual nsresult FireDelayedDOMEvents() override;
 
   // Outer windows only.
   virtual bool WouldReuseInnerWindow(nsIDocument* aNewDocument) override;
 
   virtual void SetDocShell(nsIDocShell* aDocShell) override;
@@ -2085,16 +2094,20 @@ protected:
   // begin presentation on.
   uint32_t mAutoActivateVRDisplayID; // Outer windows only
   int64_t mBeforeUnloadListenerCount; // Inner windows only
 
   RefPtr<mozilla::dom::IntlUtils> mIntlUtils;
 
   mozilla::UniquePtr<mozilla::dom::ClientSource> mClientSource;
 
+  // Weak references added by AddServiceWorker() and cleared by
+  // RemoveServiceWorker() when the ServiceWorker is destroyed.
+  nsTArray<mozilla::dom::ServiceWorker*> mServiceWorkerList;
+
   nsTArray<RefPtr<mozilla::dom::Promise>> mPendingPromises; // Inner windows only
 
   friend class nsDOMScriptableHelper;
   friend class nsDOMWindowUtils;
   friend class mozilla::dom::PostMessageEvent;
   friend class DesktopNotification;
   friend class mozilla::dom::TimeoutManager;
   friend class IdleRequestExecutor;
diff --git a/dom/base/nsIGlobalObject.cpp b/dom/base/nsIGlobalObject.cpp
--- a/dom/base/nsIGlobalObject.cpp
+++ b/dom/base/nsIGlobalObject.cpp
@@ -1,21 +1,24 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "nsIGlobalObject.h"
+
+#include "mozilla/dom/ServiceWorker.h"
 #include "nsContentUtils.h"
 #include "nsThreadUtils.h"
 #include "nsHostObjectProtocolHandler.h"
 
 using mozilla::Maybe;
 using mozilla::dom::ClientInfo;
+using mozilla::dom::ServiceWorker;
 using mozilla::dom::ServiceWorkerDescriptor;
 
 nsIGlobalObject::~nsIGlobalObject()
 {
   UnlinkHostObjectURIs();
 }
 
 nsIPrincipal*
@@ -127,8 +130,27 @@ nsIGlobalObject::GetClientInfo() const
 
 Maybe<ServiceWorkerDescriptor>
 nsIGlobalObject::GetController() const
 {
   // By default globals do not have a service worker controller.  Only real
   // window and worker globals can currently be controlled as a client.
   return Maybe<ServiceWorkerDescriptor>();
 }
+
+RefPtr<ServiceWorker>
+nsIGlobalObject::GetOrCreateServiceWorker(const ServiceWorkerDescriptor& aDescriptor)
+{
+  MOZ_DIAGNOSTIC_ASSERT(false, "this global should not have any service workers");
+  return nullptr;
+}
+
+void
+nsIGlobalObject::AddServiceWorker(ServiceWorker* aServiceWorker)
+{
+  MOZ_DIAGNOSTIC_ASSERT(false, "this global should not have any service workers");
+}
+
+void
+nsIGlobalObject::RemoveServiceWorker(ServiceWorker* aServiceWorker)
+{
+  MOZ_DIAGNOSTIC_ASSERT(false, "this global should not have any service workers");
+}
diff --git a/dom/base/nsIGlobalObject.h b/dom/base/nsIGlobalObject.h
--- a/dom/base/nsIGlobalObject.h
+++ b/dom/base/nsIGlobalObject.h
@@ -18,16 +18,22 @@
 
 #define NS_IGLOBALOBJECT_IID \
 { 0x11afa8be, 0xd997, 0x4e07, \
 { 0xa6, 0xa3, 0x6f, 0x87, 0x2e, 0xc3, 0xee, 0x7f } }
 
 class nsCycleCollectionTraversalCallback;
 class nsIPrincipal;
 
+namespace mozilla {
+namespace dom {
+class ServiceWorker;
+} // namespace dom
+} // namespace mozilla
+
 class nsIGlobalObject : public nsISupports,
                         public mozilla::dom::DispatcherTrait
 {
   nsTArray<nsCString> mHostObjectURIs;
   bool mIsDying;
 
 protected:
   nsIGlobalObject()
@@ -80,16 +86,31 @@ public:
   virtual bool IsInSyncOperation() { return false; }
 
   virtual mozilla::Maybe<mozilla::dom::ClientInfo>
   GetClientInfo() const;
 
   virtual mozilla::Maybe<mozilla::dom::ServiceWorkerDescriptor>
   GetController() const;
 
+  // Get the DOM object for the given descriptor or attempt to create one.
+  // Creation can still fail and return nullptr during shutdown, etc.
+  virtual RefPtr<mozilla::dom::ServiceWorker>
+  GetOrCreateServiceWorker(const mozilla::dom::ServiceWorkerDescriptor& aDescriptor);
+
+  // These methods allow the ServiceWorker instances to note their existence
+  // so that the global can use weak references to them.  The global should
+  // not hold a strong reference to the ServiceWorker.
+  virtual void
+  AddServiceWorker(mozilla::dom::ServiceWorker* aServiceWorker);
+
+  // This method must be called by the ServiceWorker before it is destroyed.
+  virtual void
+  RemoveServiceWorker(mozilla::dom::ServiceWorker* aServiceWorker);
+
 protected:
   virtual ~nsIGlobalObject();
 
   void
   StartDying()
   {
     mIsDying = true;
   }
diff --git a/dom/base/nsPIDOMWindow.h b/dom/base/nsPIDOMWindow.h
--- a/dom/base/nsPIDOMWindow.h
+++ b/dom/base/nsPIDOMWindow.h
@@ -46,16 +46,17 @@ class ThrottledEventQueue;
 namespace dom {
 class AudioContext;
 class ClientInfo;
 class ClientState;
 class DocGroup;
 class TabGroup;
 class Element;
 class Performance;
+class ServiceWorker;
 class ServiceWorkerDescriptor;
 class ServiceWorkerRegistration;
 class Timeout;
 class TimeoutManager;
 class CustomElementRegistry;
 enum class CallerType : uint32_t;
 } // namespace dom
 } // namespace mozilla
@@ -937,16 +938,19 @@ public:
   // Return true if there are any open WebSockets that could block
   // timeout-throttling.
   bool HasOpenWebSockets() const;
 
   mozilla::Maybe<mozilla::dom::ClientInfo> GetClientInfo() const;
   mozilla::Maybe<mozilla::dom::ClientState> GetClientState() const;
   mozilla::Maybe<mozilla::dom::ServiceWorkerDescriptor> GetController() const;
 
+  RefPtr<mozilla::dom::ServiceWorker>
+  GetOrCreateServiceWorker(const mozilla::dom::ServiceWorkerDescriptor& aDescriptor);
+
   void NoteCalledRegisterForServiceWorkerScope(const nsACString& aScope);
 
 protected:
   void CreatePerformanceObjectIfNeeded();
 };
 
 NS_DEFINE_STATIC_IID_ACCESSOR(nsPIDOMWindowInner, NS_PIDOMWINDOWINNER_IID)
 
diff --git a/dom/serviceworkers/ServiceWorker.cpp b/dom/serviceworkers/ServiceWorker.cpp
--- a/dom/serviceworkers/ServiceWorker.cpp
+++ b/dom/serviceworkers/ServiceWorker.cpp
@@ -129,10 +129,21 @@ ServiceWorker::PostMessage(JSContext* aC
   if (State() == ServiceWorkerState::Redundant) {
     aRv.Throw(NS_ERROR_DOM_INVALID_STATE_ERR);
     return;
   }
 
   mInner->PostMessage(GetParentObject(), aCx, aMessage, aTransferable, aRv);
 }
 
+bool
+ServiceWorker::MatchesDescriptor(const ServiceWorkerDescriptor& aDescriptor) const
+{
+  // Compare everything in the descriptor except the state.  That is mutable
+  // and may not exactly match.
+  return mDescriptor.PrincipalInfo() == aDescriptor.PrincipalInfo() &&
+         mDescriptor.Scope() == aDescriptor.Scope() &&
+         mDescriptor.ScriptURL() == aDescriptor.ScriptURL() &&
+         mDescriptor.Id() == aDescriptor.Id();
+}
+
 } // namespace dom
 } // namespace mozilla
diff --git a/dom/serviceworkers/ServiceWorker.h b/dom/serviceworkers/ServiceWorker.h
--- a/dom/serviceworkers/ServiceWorker.h
+++ b/dom/serviceworkers/ServiceWorker.h
@@ -78,16 +78,19 @@ public:
 
   void
   GetScriptURL(nsString& aURL) const;
 
   void
   PostMessage(JSContext* aCx, JS::Handle<JS::Value> aMessage,
               const Sequence<JSObject*>& aTransferable, ErrorResult& aRv);
 
+  bool
+  MatchesDescriptor(const ServiceWorkerDescriptor& aDescriptor) const;
+
 private:
   ServiceWorker(nsIGlobalObject* aWindow,
                 const ServiceWorkerDescriptor& aDescriptor,
                 Inner* aInner);
 
   // This class is reference-counted and will be destroyed from Release().
   ~ServiceWorker();
 
