# HG changeset patch
# User Jeff Gilbert <jgilbert@mozilla.com>
# Date 1561616471 0
#      Thu Jun 27 06:21:11 2019 +0000
# Node ID 69b25d1fba9afa3c11441c1431a4f283e1ca1aab
# Parent  46f669479d6684a7cd9ac5a8088a7c23d9ec2a92
Bug 1507696 - Cherry-pick CopyTexImage3D fixes. r=lsalzman

Differential Revision: https://phabricator.services.mozilla.com/D36117

diff --git a/gfx/angle/checkout/out/gen/angle/id/commit.h b/gfx/angle/checkout/out/gen/angle/id/commit.h
--- a/gfx/angle/checkout/out/gen/angle/id/commit.h
+++ b/gfx/angle/checkout/out/gen/angle/id/commit.h
@@ -1,3 +1,3 @@
-#define ANGLE_COMMIT_HASH "ee49c7b031ea"
+#define ANGLE_COMMIT_HASH "b0a9c45400d2"
 #define ANGLE_COMMIT_HASH_SIZE 12
-#define ANGLE_COMMIT_DATE "2019-06-06 14:34:34 -0700"
+#define ANGLE_COMMIT_DATE "2019-06-26 14:30:37 -0700"
diff --git a/gfx/angle/checkout/src/libANGLE/Context.cpp b/gfx/angle/checkout/src/libANGLE/Context.cpp
--- a/gfx/angle/checkout/src/libANGLE/Context.cpp
+++ b/gfx/angle/checkout/src/libANGLE/Context.cpp
@@ -3699,20 +3699,21 @@ void Context::copyTexSubImage2D(TextureT
     }
 
     // Only sync the read FBO
     ANGLE_CONTEXT_TRY(mState.syncDirtyObject(this, GL_READ_FRAMEBUFFER));
 
     Offset destOffset(xoffset, yoffset, 0);
     Rectangle sourceArea(x, y, width, height);
 
+    ImageIndex index = ImageIndex::MakeFromTarget(target, level);
+
     Framebuffer *framebuffer = mState.getReadFramebuffer();
     Texture *texture         = getTargetTexture(TextureTargetToType(target));
-    ANGLE_CONTEXT_TRY(
-        texture->copySubImage(this, target, level, destOffset, sourceArea, framebuffer));
+    ANGLE_CONTEXT_TRY(texture->copySubImage(this, index, destOffset, sourceArea, framebuffer));
 }
 
 void Context::copyTexSubImage3D(TextureType target,
                                 GLint level,
                                 GLint xoffset,
                                 GLint yoffset,
                                 GLint zoffset,
                                 GLint x,
@@ -3726,20 +3727,21 @@ void Context::copyTexSubImage3D(TextureT
     }
 
     // Only sync the read FBO
     ANGLE_CONTEXT_TRY(mState.syncDirtyObject(this, GL_READ_FRAMEBUFFER));
 
     Offset destOffset(xoffset, yoffset, zoffset);
     Rectangle sourceArea(x, y, width, height);
 
+    ImageIndex index = ImageIndex::MakeFromType(target, level, zoffset);
+
     Framebuffer *framebuffer = mState.getReadFramebuffer();
     Texture *texture         = getTargetTexture(target);
-    ANGLE_CONTEXT_TRY(texture->copySubImage(this, NonCubeTextureTypeToTarget(target), level,
-                                            destOffset, sourceArea, framebuffer));
+    ANGLE_CONTEXT_TRY(texture->copySubImage(this, index, destOffset, sourceArea, framebuffer));
 }
 
 void Context::framebufferTexture2D(GLenum target,
                                    GLenum attachment,
                                    TextureTarget textarget,
                                    GLuint texture,
                                    GLint level)
 {
diff --git a/gfx/angle/checkout/src/libANGLE/Texture.cpp b/gfx/angle/checkout/src/libANGLE/Texture.cpp
--- a/gfx/angle/checkout/src/libANGLE/Texture.cpp
+++ b/gfx/angle/checkout/src/libANGLE/Texture.cpp
@@ -1141,34 +1141,32 @@ angle::Result Texture::copyImage(Context
 
     // We need to initialize this texture only if the source attachment is not initialized.
     signalDirtyStorage(context, InitState::Initialized);
 
     return angle::Result::Continue;
 }
 
 angle::Result Texture::copySubImage(Context *context,
-                                    TextureTarget target,
-                                    GLint level,
+                                    const ImageIndex &index,
                                     const Offset &destOffset,
                                     const Rectangle &sourceArea,
                                     Framebuffer *source)
 {
-    ASSERT(TextureTargetToType(target) == mState.mType);
+    ASSERT(TextureTargetToType(index.getTarget()) == mState.mType);
 
     // Ensure source FBO is initialized.
     ANGLE_TRY(source->ensureReadAttachmentInitialized(context, GL_COLOR_BUFFER_BIT));
 
-    Box destBox(destOffset.x, destOffset.y, destOffset.y, sourceArea.width, sourceArea.height, 1);
-    ANGLE_TRY(ensureSubImageInitialized(context, target, level, destBox));
-
-    ImageIndex index = ImageIndex::MakeFromTarget(target, level);
+    Box destBox(destOffset.x, destOffset.y, destOffset.z, sourceArea.width, sourceArea.height, 1);
+    ANGLE_TRY(
+        ensureSubImageInitialized(context, index.getTarget(), index.getLevelIndex(), destBox));
 
     ANGLE_TRY(mTexture->copySubImage(context, index, destOffset, sourceArea, source));
-    ANGLE_TRY(handleMipmapGenerationHint(context, level));
+    ANGLE_TRY(handleMipmapGenerationHint(context, index.getLevelIndex()));
 
     return angle::Result::Continue;
 }
 
 angle::Result Texture::copyTexture(Context *context,
                                    TextureTarget target,
                                    GLint level,
                                    GLenum internalFormat,
diff --git a/gfx/angle/checkout/src/libANGLE/Texture.h b/gfx/angle/checkout/src/libANGLE/Texture.h
--- a/gfx/angle/checkout/src/libANGLE/Texture.h
+++ b/gfx/angle/checkout/src/libANGLE/Texture.h
@@ -340,18 +340,17 @@ class Texture final : public RefCountObj
 
     angle::Result copyImage(Context *context,
                             TextureTarget target,
                             GLint level,
                             const Rectangle &sourceArea,
                             GLenum internalFormat,
                             Framebuffer *source);
     angle::Result copySubImage(Context *context,
-                               TextureTarget target,
-                               GLint level,
+                               const ImageIndex &index,
                                const Offset &destOffset,
                                const Rectangle &sourceArea,
                                Framebuffer *source);
 
     angle::Result copyTexture(Context *context,
                               TextureTarget target,
                               GLint level,
                               GLenum internalFormat,
diff --git a/gfx/angle/cherry_picks.txt b/gfx/angle/cherry_picks.txt
--- a/gfx/angle/cherry_picks.txt
+++ b/gfx/angle/cherry_picks.txt
@@ -1,8 +1,24 @@
+commit b0a9c45400d23281c67059980f7a39cc4285e5f9
+Author: Jamie Madill <jmadill@chromium.org>
+Date:   Mon Apr 8 16:26:50 2019 -0400
+
+    Fix glCopyTexSubImage3D.
+    
+    Two bugs were present in our implementation. We were using the y offset
+    for z in ensureSubImageInitialized. And for our D3D back-end we were
+    potentially reading from the wrong image index.
+    
+    Bug: chromium:947342
+    Change-Id: If39671a911e08fcc641b9ba6f5910e3a2c16eb5d
+    Reviewed-on: https://chromium-review.googlesource.com/c/angle/angle/+/1558671
+    Commit-Queue: Jamie Madill <jmadill@chromium.org>
+    Reviewed-by: Jonah Ryan-Davis <jonahr@google.com>
+
 commit ee49c7b031eaab9d9df23fa7fbc2fb5875397622
 Author: Jeff Gilbert <jgilbert@mozilla.com>
 Date:   Wed Jun 5 17:32:32 2019 -0700
 
     Use execv instead of execve w/ environ.
     
     Some platforms don't want us messing with environ, and these seem to be
     equivalent.
