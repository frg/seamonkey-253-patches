# HG changeset patch
# User Nicholas Nethercote <nnethercote@mozilla.com>
# Date 1559716999 0
# Node ID 30fa4e06dc108f6790d15b98a12d91bcbe65b15c
# Parent  0240c9ed1b1a773b3955d8bd44f06f0cd3d70694
Bug 1556891 - Remove `base` argument from `XP_TTOA` and `XP_STOA`. r=gsvelto

It's ignored on non-Windows platforms, and all callsites use 10.

Differential Revision: https://phabricator.services.mozilla.com/D33737

diff --git a/toolkit/crashreporter/nsExceptionHandler.cpp b/toolkit/crashreporter/nsExceptionHandler.cpp
--- a/toolkit/crashreporter/nsExceptionHandler.cpp
+++ b/toolkit/crashreporter/nsExceptionHandler.cpp
@@ -131,42 +131,40 @@ typedef std::wstring xpstring;
 #  define MINIDUMP_ANALYZER_FILENAME "minidump-analyzer.exe"
 #  define PATH_SEPARATOR "\\"
 #  define XP_PATH_SEPARATOR L"\\"
 #  define XP_PATH_SEPARATOR_CHAR L'\\'
 #  define XP_PATH_MAX (MAX_PATH + 1)
 // "<reporter path>" "<minidump path>"
 #  define CMDLINE_SIZE ((XP_PATH_MAX * 2) + 6)
 #  ifdef _USE_32BIT_TIME_T
-#    define XP_TTOA(time, buffer, base) ltoa(time, buffer, base)
+#    define XP_TTOA(time, buffer) ltoa(time, buffer, 10)
 #  else
-#    define XP_TTOA(time, buffer, base) _i64toa(time, buffer, base)
+#    define XP_TTOA(time, buffer) _i64toa(time, buffer, 10)
 #  endif
-#  define XP_STOA(size, buffer, base) _ui64toa(size, buffer, base)
+#  define XP_STOA(size, buffer) _ui64toa(size, buffer, 10)
 #else
 typedef char XP_CHAR;
 typedef std::string xpstring;
 #  define XP_TEXT(x) x
 #  define CONVERT_XP_CHAR_TO_UTF16(x) NS_ConvertUTF8toUTF16(x)
 #  define CRASH_REPORTER_FILENAME "crashreporter"
 #  define MINIDUMP_ANALYZER_FILENAME "minidump-analyzer"
 #  define PATH_SEPARATOR "/"
 #  define XP_PATH_SEPARATOR "/"
 #  define XP_PATH_SEPARATOR_CHAR '/'
 #  define XP_PATH_MAX PATH_MAX
 #  ifdef XP_LINUX
 #    define XP_STRLEN(x) my_strlen(x)
-#    define XP_TTOA(time, buffer, base) \
-      my_inttostring(time, buffer, sizeof(buffer))
-#    define XP_STOA(size, buffer, base) \
-      my_inttostring(size, buffer, sizeof(buffer))
+#    define XP_TTOA(time, buffer) my_inttostring(time, buffer, sizeof(buffer))
+#    define XP_STOA(size, buffer) my_inttostring(size, buffer, sizeof(buffer))
 #  else
 #    define XP_STRLEN(x) strlen(x)
-#    define XP_TTOA(time, buffer, base) sprintf(buffer, "%ld", time)
-#    define XP_STOA(size, buffer, base) sprintf(buffer, "%zu", (size_t)size)
+#    define XP_TTOA(time, buffer) sprintf(buffer, "%ld", time)
+#    define XP_STOA(size, buffer) sprintf(buffer, "%zu", (size_t)size)
 #    define my_strlen strlen
 #    define sys_close close
 #    define sys_fork fork
 #    define sys_open open
 #    define sys_read read
 #    define sys_write write
 #  endif
 #endif  // XP_WIN
@@ -838,44 +836,44 @@ static void WriteAnnotationsForMainProce
     }
   }
 
   if (currentSessionId) {
     WriteAnnotation(pw, Annotation::TelemetrySessionId, currentSessionId);
   }
 
   char crashTimeString[32];
-  XP_TTOA(crashTime, crashTimeString, 10);
+  XP_TTOA(crashTime, crashTimeString);
   WriteAnnotation(pw, Annotation::CrashTime, crashTimeString);
 
   double uptimeTS = (TimeStamp::NowLoRes() - TimeStamp::ProcessCreation())
                         .ToSecondsSigDigits();
   char uptimeTSString[64];
   SimpleNoCLibDtoA(uptimeTS, uptimeTSString, sizeof(uptimeTSString));
   WriteAnnotation(pw, Annotation::UptimeTS, uptimeTSString);
 
   // calculate time since last crash (if possible).
   if (lastCrashTime != 0) {
     time_t timeSinceLastCrash = crashTime - lastCrashTime;
 
     if (timeSinceLastCrash != 0) {
       char timeSinceLastCrashString[32];
-      XP_TTOA(timeSinceLastCrash, timeSinceLastCrashString, 10);
+      XP_TTOA(timeSinceLastCrash, timeSinceLastCrashString);
       WriteAnnotation(pw, Annotation::SecondsSinceLastCrash,
                       timeSinceLastCrashString);
     }
   }
 
   if (isGarbageCollecting) {
     WriteAnnotation(pw, Annotation::IsGarbageCollecting, "1");
   }
 
   char buffer[128];
   if (eventloopNestingLevel > 0) {
-    XP_STOA(eventloopNestingLevel, buffer, 10);
+    XP_STOA(eventloopNestingLevel, buffer);
     WriteAnnotation(pw, Annotation::EventLoopNestingLevel, buffer);
   }
 
 #ifdef XP_WIN
   if (gBreakpadReservedVM) {
     _ui64toa(uintptr_t(gBreakpadReservedVM), buffer, 10);
     WriteAnnotation(pw, Annotation::BreakpadReserveAddress, buffer);
     _ui64toa(kReserveSize, buffer, 10);
@@ -887,23 +885,23 @@ static void WriteAnnotationsForMainProce
 #  endif
   WriteGlobalMemoryStatus(pw);
 #endif  // XP_WIN
 
   WriteEscapedMozCrashReason(pw);
 
   char oomAllocationSizeBuffer[32] = "";
   if (gOOMAllocationSize) {
-    XP_STOA(gOOMAllocationSize, oomAllocationSizeBuffer, 10);
+    XP_STOA(gOOMAllocationSize, oomAllocationSizeBuffer);
     WriteAnnotation(pw, Annotation::OOMAllocationSize, oomAllocationSizeBuffer);
   }
 
   char texturesSizeBuffer[32] = "";
   if (gTexturesSize) {
-    XP_STOA(gTexturesSize, texturesSizeBuffer, 10);
+    XP_STOA(gTexturesSize, texturesSizeBuffer);
     WriteAnnotation(pw, Annotation::TextureUsage, texturesSizeBuffer);
   }
 
   if (memoryReportPath) {
     WriteAnnotation(pw, Annotation::ContainsMemoryReport, "1");
   }
 
   std::function<void(const char*)> getThreadAnnotationCB =
@@ -1012,17 +1010,17 @@ static bool MinidumpCallback(
 #ifdef XP_LINUX
   struct kernel_timeval tv;
   sys_gettimeofday(&tv, nullptr);
   crashTime = tv.tv_sec;
 #else
   crashTime = time(nullptr);
 #endif
   char crashTimeString[32];
-  XP_TTOA(crashTime, crashTimeString, 10);
+  XP_TTOA(crashTime, crashTimeString);
 
   // write crash time to file
   if (lastCrashTimeFilename[0] != 0) {
     PlatformWriter lastCrashFile(lastCrashTimeFilename);
     WriteString(lastCrashFile, crashTimeString);
   }
 
   WriteCrashEventFile(crashTime, crashTimeString,
@@ -1174,17 +1172,17 @@ static void PrepareChildExceptionTimeAnn
   // ...and write out any annotations. These must be escaped if necessary
   // (but don't call EscapeAnnotation here, because it touches the heap).
 #ifdef XP_WIN
   WriteGlobalMemoryStatus(apiData);
 #endif
 
   char oomAllocationSizeBuffer[32] = "";
   if (gOOMAllocationSize) {
-    XP_STOA(gOOMAllocationSize, oomAllocationSizeBuffer, 10);
+    XP_STOA(gOOMAllocationSize, oomAllocationSizeBuffer);
     WriteAnnotation(apiData, Annotation::OOMAllocationSize,
                     oomAllocationSizeBuffer);
   }
 
   WriteEscapedMozCrashReason(apiData);
 
   std::function<void(const char*)> getThreadAnnotationCB =
       [&](const char* aValue) -> void {
@@ -1512,17 +1510,17 @@ nsresult SetExceptionHandler(nsIFile* aX
         "SetUnhandledExceptionFilter hook failed; crash reporter is "
         "vulnerable.\n");
 #  endif
 #endif
 
   // store application start time
   char timeString[32];
   time_t startupTime = time(nullptr);
-  XP_TTOA(startupTime, timeString, 10);
+  XP_TTOA(startupTime, timeString);
   AnnotateCrashReport(Annotation::StartupTime, nsDependentCString(timeString));
 
 #if defined(XP_MACOSX)
   // On OS X, many testers like to see the OS crash reporting dialog
   // since it offers immediate stack traces.  We allow them to set
   // a default to pass exceptions to the OS handler.
   Boolean keyExistsAndHasValidFormat = false;
   Boolean prefValue = ::CFPreferencesGetAppBooleanValue(
