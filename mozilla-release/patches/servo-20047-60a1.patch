# HG changeset patch
# User Anthony Ramine <n.oxyde@gmail.com>
# Date 1518693962 18000
# Node ID 30f5526389d4009041da3d19bba5c56953a52556
# Parent  eafcc2185ead5716abd6fbe9ac986ac5de780212
servo: Merge #20047 - Move some font code around (from servo:derive-all-the-things); r=emilio

The only notable change is that KeywordInfo is now generic.

Source-Repo: https://github.com/servo/servo
Source-Revision: 0baab064df950cff65ce76dc5f134e2cd4c7413b

diff --git a/servo/components/style/gecko/rules.rs b/servo/components/style/gecko/rules.rs
--- a/servo/components/style/gecko/rules.rs
+++ b/servo/components/style/gecko/rules.rs
@@ -16,17 +16,18 @@ use gecko_bindings::sugar::ns_css_value:
 use gecko_bindings::sugar::refptr::{RefPtr, UniqueRefPtr};
 use nsstring::nsString;
 use properties::longhands::font_language_override;
 use shared_lock::{ToCssWithGuard, SharedRwLockReadGuard};
 use std::fmt::{self, Write};
 use std::str;
 use str::CssStringWriter;
 use values::computed::font::FamilyName;
-use values::specified::font::{FontTag, FontVariationSettings, SpecifiedFontFeatureSettings};
+use values::generics::font::FontTag;
+use values::specified::font::{FontVariationSettings, SpecifiedFontFeatureSettings};
 
 /// A @font-face rule
 pub type FontFaceRule = RefPtr<nsCSSFontFaceRule>;
 
 impl ToNsCssValue for FamilyName {
     fn convert(self, nscssvalue: &mut nsCSSValue) {
         nscssvalue.set_string_from_atom(&self.name)
     }
diff --git a/servo/components/style/properties/gecko.mako.rs b/servo/components/style/properties/gecko.mako.rs
--- a/servo/components/style/properties/gecko.mako.rs
+++ b/servo/components/style/properties/gecko.mako.rs
@@ -1447,18 +1447,17 @@ impl Clone for ${style_struct.gecko_stru
         }
     }
 
     pub fn reset_${ident}(&mut self, other: &Self) {
         self.copy_${ident}_from(other)
     }
 
     pub fn clone_${ident}(&self) -> longhands::${ident}::computed_value::T {
-        use values::generics::font::{FontSettings, ${tag_type}};
-        use values::specified::font::FontTag;
+        use values::generics::font::{FontSettings, FontTag, ${tag_type}};
 
         FontSettings(
             self.gecko.mFont.${gecko_ffi_name}.iter().map(|gecko_font_setting| {
                 ${tag_type} {
                     tag: FontTag(gecko_font_setting.mTag),
                     value: gecko_font_setting.mValue as ${value_type},
                 }
             }).collect::<Vec<_>>().into_boxed_slice()
@@ -2456,17 +2455,17 @@ fn static_assert() {
 
     pub fn unzoom_fonts(&mut self, device: &Device) {
         self.gecko.mSize = device.unzoom_text(Au(self.gecko.mSize)).0;
         self.gecko.mScriptUnconstrainedSize = device.unzoom_text(Au(self.gecko.mScriptUnconstrainedSize)).0;
         self.gecko.mFont.size = device.unzoom_text(Au(self.gecko.mFont.size)).0;
     }
 
     pub fn set_font_size(&mut self, v: FontSize) {
-        use values::specified::font::KeywordSize;
+        use values::generics::font::KeywordSize;
         self.gecko.mSize = v.size().0;
         self.gecko.mScriptUnconstrainedSize = v.size().0;
         if let Some(info) = v.keyword_info {
             self.gecko.mFontSizeKeyword = match info.kw {
                 KeywordSize::XXSmall => structs::NS_STYLE_FONT_SIZE_XXSMALL,
                 KeywordSize::XSmall => structs::NS_STYLE_FONT_SIZE_XSMALL,
                 KeywordSize::Small => structs::NS_STYLE_FONT_SIZE_SMALL,
                 KeywordSize::Medium => structs::NS_STYLE_FONT_SIZE_MEDIUM,
@@ -2668,18 +2667,17 @@ fn static_assert() {
             self.gecko.mFontSizeFactor = 1.;
             self.gecko.mFontSizeOffset = 0;
             self.gecko.mScriptUnconstrainedSize = parent.gecko.mScriptUnconstrainedSize;
         }
         self.fixup_font_min_size(device);
     }
 
     pub fn clone_font_size(&self) -> FontSize {
-        use values::computed::font::KeywordInfo;
-        use values::specified::font::KeywordSize;
+        use values::generics::font::{KeywordInfo, KeywordSize};
         let size = Au(self.gecko.mSize).into();
         let kw = match self.gecko.mFontSizeKeyword as u32 {
             structs::NS_STYLE_FONT_SIZE_XXSMALL => KeywordSize::XXSmall,
             structs::NS_STYLE_FONT_SIZE_XSMALL => KeywordSize::XSmall,
             structs::NS_STYLE_FONT_SIZE_SMALL => KeywordSize::Small,
             structs::NS_STYLE_FONT_SIZE_MEDIUM => KeywordSize::Medium,
             structs::NS_STYLE_FONT_SIZE_LARGE => KeywordSize::Large,
             structs::NS_STYLE_FONT_SIZE_XLARGE => KeywordSize::XLarge,
diff --git a/servo/components/style/properties/helpers/animated_properties.mako.rs b/servo/components/style/properties/helpers/animated_properties.mako.rs
--- a/servo/components/style/properties/helpers/animated_properties.mako.rs
+++ b/servo/components/style/properties/helpers/animated_properties.mako.rs
@@ -47,24 +47,22 @@ use values::computed::ToComputedValue;
 use values::computed::transform::{DirectionVector, Matrix, Matrix3D};
 use values::computed::transform::TransformOperation as ComputedTransformOperation;
 use values::computed::transform::Transform as ComputedTransform;
 use values::computed::transform::Rotate as ComputedRotate;
 use values::computed::transform::Translate as ComputedTranslate;
 use values::computed::transform::Scale as ComputedScale;
 use values::generics::transform::{self, Rotate, Translate, Scale, Transform, TransformOperation};
 use values::distance::{ComputeSquaredDistance, SquaredDistance};
-use values::generics::font::FontSettings as GenericFontSettings;
+use values::generics::font::{FontSettings as GenericFontSettings, FontTag, VariationValue};
 use values::computed::font::FontVariationSettings;
-use values::generics::font::VariationValue;
 use values::generics::effects::Filter;
 use values::generics::position as generic_position;
 use values::generics::svg::{SVGLength,  SvgLengthOrPercentageOrNumber, SVGPaint};
 use values::generics::svg::{SVGPaintKind, SVGStrokeDashArray, SVGOpacity};
-use values::specified::font::FontTag;
 use void::{self, Void};
 
 /// <https://drafts.csswg.org/css-transitions/#animtype-repeatable-list>
 pub trait RepeatableListAnimatable: Animate {}
 
 /// Returns true if this nsCSSPropertyID is one of the animatable properties.
 #[cfg(feature = "gecko")]
 pub fn nscsspropertyid_is_animatable(property: nsCSSPropertyID) -> bool {
diff --git a/servo/components/style/values/animated/effects.rs b/servo/components/style/values/animated/effects.rs
--- a/servo/components/style/values/animated/effects.rs
+++ b/servo/components/style/values/animated/effects.rs
@@ -8,17 +8,17 @@ use properties::longhands::box_shadow::c
 use properties::longhands::filter::computed_value::T as ComputedFilterList;
 use properties::longhands::text_shadow::computed_value::T as ComputedTextShadowList;
 use std::cmp;
 #[cfg(not(feature = "gecko"))]
 use values::Impossible;
 use values::animated::{Animate, Procedure, ToAnimatedValue, ToAnimatedZero};
 use values::animated::color::RGBA;
 use values::computed::{Angle, Number};
-use values::computed::length::{Length, NonNegativeLength};
+use values::computed::length::Length;
 use values::distance::{ComputeSquaredDistance, SquaredDistance};
 use values::generics::effects::BoxShadow as GenericBoxShadow;
 use values::generics::effects::Filter as GenericFilter;
 use values::generics::effects::SimpleShadow as GenericSimpleShadow;
 
 /// An animated value for the `box-shadow` property.
 pub type BoxShadowList = ShadowList<BoxShadow>;
 
@@ -28,33 +28,33 @@ pub type TextShadowList = ShadowList<Sim
 /// An animated value for shadow lists.
 ///
 /// <https://drafts.csswg.org/css-transitions/#animtype-shadow-list>
 #[cfg_attr(feature = "servo", derive(MallocSizeOf))]
 #[derive(Clone, Debug, PartialEq)]
 pub struct ShadowList<Shadow>(Vec<Shadow>);
 
 /// An animated value for a single `box-shadow`.
-pub type BoxShadow = GenericBoxShadow<Option<RGBA>, Length, NonNegativeLength, Length>;
+pub type BoxShadow = GenericBoxShadow<Option<RGBA>, Length, Length, Length>;
 
 /// An animated value for the `filter` property.
 #[cfg_attr(feature = "servo", derive(MallocSizeOf))]
 #[derive(Clone, Debug, PartialEq)]
 pub struct FilterList(pub Vec<Filter>);
 
 /// An animated value for a single `filter`.
 #[cfg(feature = "gecko")]
-pub type Filter = GenericFilter<Angle, Number, NonNegativeLength, SimpleShadow>;
+pub type Filter = GenericFilter<Angle, Number, Length, SimpleShadow>;
 
 /// An animated value for a single `filter`.
 #[cfg(not(feature = "gecko"))]
-pub type Filter = GenericFilter<Angle, Number, NonNegativeLength, Impossible>;
+pub type Filter = GenericFilter<Angle, Number, Length, Impossible>;
 
 /// An animated value for the `drop-shadow()` filter.
-pub type SimpleShadow = GenericSimpleShadow<Option<RGBA>, Length, NonNegativeLength>;
+pub type SimpleShadow = GenericSimpleShadow<Option<RGBA>, Length, Length>;
 
 impl ToAnimatedValue for ComputedBoxShadowList {
     type AnimatedValue = BoxShadowList;
 
     #[inline]
     fn to_animated_value(self) -> Self::AnimatedValue {
         ShadowList(self.0.to_animated_value())
     }
diff --git a/servo/components/style/values/animated/mod.rs b/servo/components/style/values/animated/mod.rs
--- a/servo/components/style/values/animated/mod.rs
+++ b/servo/components/style/values/animated/mod.rs
@@ -12,17 +12,16 @@ use app_units::Au;
 use euclid::{Point2D, Size2D};
 use smallvec::SmallVec;
 use values::computed::Angle as ComputedAngle;
 use values::computed::BorderCornerRadius as ComputedBorderCornerRadius;
 #[cfg(feature = "servo")]
 use values::computed::ComputedUrl;
 use values::computed::MaxLength as ComputedMaxLength;
 use values::computed::MozLength as ComputedMozLength;
-use values::computed::NonNegativeLength as ComputedNonNegativeLength;
 use values::specified::url::SpecifiedUrl;
 
 pub mod color;
 pub mod effects;
 
 /// Animate from one value to another.
 ///
 /// This trait is derivable with `#[derive(Animate)]`. The derived
@@ -256,30 +255,16 @@ macro_rules! trivial_to_animated_value {
 trivial_to_animated_value!(Au);
 trivial_to_animated_value!(ComputedAngle);
 trivial_to_animated_value!(SpecifiedUrl);
 #[cfg(feature = "servo")]
 trivial_to_animated_value!(ComputedUrl);
 trivial_to_animated_value!(bool);
 trivial_to_animated_value!(f32);
 
-impl ToAnimatedValue for ComputedNonNegativeLength {
-    type AnimatedValue = Self;
-
-    #[inline]
-    fn to_animated_value(self) -> Self {
-        self
-    }
-
-    #[inline]
-    fn from_animated_value(animated: Self::AnimatedValue) -> Self {
-        ComputedNonNegativeLength::new(animated.px().max(0.))
-    }
-}
-
 impl ToAnimatedValue for ComputedBorderCornerRadius {
     type AnimatedValue = Self;
 
     #[inline]
     fn to_animated_value(self) -> Self {
         self
     }
 
@@ -375,8 +360,21 @@ where
     #[inline]
     fn to_animated_zero(&self) -> Result<Self, ()> {
         match *self {
             Some(ref value) => Ok(Some(value.to_animated_zero()?)),
             None => Ok(None),
         }
     }
 }
+
+impl<T> ToAnimatedZero for Size2D<T>
+where
+    T: ToAnimatedZero,
+{
+    #[inline]
+    fn to_animated_zero(&self) -> Result<Self, ()> {
+        Ok(Size2D::new(
+            self.width.to_animated_zero()?,
+            self.height.to_animated_zero()?,
+        ))
+    }
+}
diff --git a/servo/components/style/values/computed/border.rs b/servo/components/style/values/computed/border.rs
--- a/servo/components/style/values/computed/border.rs
+++ b/servo/components/style/values/computed/border.rs
@@ -64,24 +64,16 @@ impl BorderSpacing {
 
 impl BorderCornerRadius {
     /// Returns `0 0`.
     pub fn zero() -> Self {
         GenericBorderCornerRadius(Size::new(LengthOrPercentage::zero(), LengthOrPercentage::zero()))
     }
 }
 
-impl ToAnimatedZero for BorderSpacing {
-    #[inline]
-    fn to_animated_zero(&self) -> Result<Self, ()> {
-        // FIXME(emilio): Why?
-        Err(())
-    }
-}
-
 impl ToAnimatedZero for BorderCornerRadius {
     #[inline]
     fn to_animated_zero(&self) -> Result<Self, ()> {
         // FIXME(nox): Why?
         Err(())
     }
 }
 
diff --git a/servo/components/style/values/computed/font.rs b/servo/components/style/values/computed/font.rs
--- a/servo/components/style/values/computed/font.rs
+++ b/servo/components/style/values/computed/font.rs
@@ -18,17 +18,18 @@ use std::fmt::{self, Write};
 #[cfg(feature = "gecko")]
 use std::hash::{Hash, Hasher};
 #[cfg(feature = "servo")]
 use std::slice;
 use style_traits::{CssWriter, ParseError, ToCss};
 use values::CSSFloat;
 use values::animated::{ToAnimatedValue, ToAnimatedZero};
 use values::computed::{Context, NonNegativeLength, ToComputedValue, Integer, Number};
-use values::generics::font::{FontSettings, FeatureTagValue, VariationValue};
+use values::generics::font::{FontSettings, FeatureTagValue};
+use values::generics::font::{KeywordInfo as GenericKeywordInfo, VariationValue};
 use values::specified::font as specified;
 use values::specified::length::{FontBaseSize, NoCalcLength};
 
 pub use values::computed::Length as MozScriptMinSize;
 pub use values::specified::font::{XTextZoom, XLang, MozScriptSizeMultiplier, FontSynthesis};
 
 /// As of CSS Fonts Module Level 3, only the following values are
 /// valid: 100 | 200 | 300 | 400 | 500 | 600 | 700 | 800 | 900
@@ -44,60 +45,18 @@ pub struct FontWeight(pub u16);
 /// The computed value of font-size
 pub struct FontSize {
     /// The size.
     pub size: NonNegativeLength,
     /// If derived from a keyword, the keyword and additional transformations applied to it
     pub keyword_info: Option<KeywordInfo>,
 }
 
-#[derive(Animate, ComputeSquaredDistance, MallocSizeOf, ToAnimatedValue, ToAnimatedZero)]
-#[derive(Clone, Copy, Debug, PartialEq)]
-/// Additional information for keyword-derived font sizes.
-pub struct KeywordInfo {
-    /// The keyword used
-    pub kw: specified::KeywordSize,
-    /// A factor to be multiplied by the computed size of the keyword
-    pub factor: f32,
-    /// An additional Au offset to add to the kw*factor in the case of calcs
-    pub offset: NonNegativeLength,
-}
-
-impl KeywordInfo {
-    /// Computes the final size for this font-size keyword, accounting for
-    /// text-zoom.
-    pub fn to_computed_value(&self, context: &Context) -> NonNegativeLength {
-        let base = context.maybe_zoom_text(self.kw.to_computed_value(context));
-        base.scale_by(self.factor) + context.maybe_zoom_text(self.offset)
-    }
-
-    /// Given a parent keyword info (self), apply an additional factor/offset to it
-    pub fn compose(self, factor: f32, offset: NonNegativeLength) -> Self {
-        KeywordInfo {
-            kw: self.kw,
-            factor: self.factor * factor,
-            offset: self.offset.scale_by(factor) + offset,
-        }
-    }
-
-    /// KeywordInfo value for font-size: medium
-    pub fn medium() -> Self {
-        specified::KeywordSize::Medium.into()
-    }
-}
-
-impl From<specified::KeywordSize> for KeywordInfo {
-    fn from(x: specified::KeywordSize) -> Self {
-        KeywordInfo {
-            kw: x,
-            factor: 1.,
-            offset: Au(0).into(),
-        }
-    }
-}
+/// Additional information for computed keyword-derived font sizes.
+pub type KeywordInfo = GenericKeywordInfo<NonNegativeLength>;
 
 impl FontWeight {
     /// Value for normal
     pub fn normal() -> Self {
         FontWeight(400)
     }
 
     /// Value for bold
diff --git a/servo/components/style/values/computed/length.rs b/servo/components/style/values/computed/length.rs
--- a/servo/components/style/values/computed/length.rs
+++ b/servo/components/style/values/computed/length.rs
@@ -810,16 +810,30 @@ impl LengthOrNumber {
 }
 
 /// Either a computed `<length>` or the `normal` keyword.
 pub type LengthOrNormal = Either<Length, Normal>;
 
 /// A wrapper of Length, whose value must be >= 0.
 pub type NonNegativeLength = NonNegative<Length>;
 
+impl ToAnimatedValue for NonNegativeLength {
+    type AnimatedValue = Length;
+
+    #[inline]
+    fn to_animated_value(self) -> Self::AnimatedValue {
+        self.0
+    }
+
+    #[inline]
+    fn from_animated_value(animated: Self::AnimatedValue) -> Self {
+        NonNegativeLength::new(animated.px().max(0.))
+    }
+}
+
 impl NonNegativeLength {
     /// Create a NonNegativeLength.
     #[inline]
     pub fn new(px: CSSFloat) -> Self {
         NonNegative(Length::new(px.max(0.)))
     }
 
     /// Return a zero value.
diff --git a/servo/components/style/values/generics/border.rs b/servo/components/style/values/generics/border.rs
--- a/servo/components/style/values/generics/border.rs
+++ b/servo/components/style/values/generics/border.rs
@@ -38,17 +38,17 @@ impl<L> BorderCornerRadius<L> {
     /// Trivially create a `BorderCornerRadius`.
     pub fn new(w: L, h: L) -> Self {
         BorderCornerRadius(Size::new(w, h))
     }
 }
 
 /// A generic value for the `border-spacing` property.
 #[derive(Animate, Clone, ComputeSquaredDistance, Copy, Debug, MallocSizeOf)]
-#[derive(PartialEq, ToAnimatedValue, ToComputedValue, ToCss)]
+#[derive(PartialEq, ToAnimatedValue, ToAnimatedZero, ToComputedValue, ToCss)]
 pub struct BorderSpacing<L>(pub Size<L>);
 
 impl<L> BorderSpacing<L> {
     /// Trivially create a `BorderCornerRadius`.
     pub fn new(w: L, h: L) -> Self {
         BorderSpacing(Size::new(w, h))
     }
 }
diff --git a/servo/components/style/values/generics/font.rs b/servo/components/style/values/generics/font.rs
--- a/servo/components/style/values/generics/font.rs
+++ b/servo/components/style/values/generics/font.rs
@@ -1,21 +1,23 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 //! Generic types for font stuff.
 
+use app_units::Au;
+use byteorder::{ReadBytesExt, BigEndian};
 use cssparser::Parser;
 use num_traits::One;
 use parser::{Parse, ParserContext};
 use std::fmt::{self, Write};
-use style_traits::{CssWriter, ParseError, ToCss};
+use std::io::Cursor;
+use style_traits::{CssWriter, ParseError, StyleParseErrorKind, ToCss};
 use values::distance::{ComputeSquaredDistance, SquaredDistance};
-use values::specified::font::FontTag;
 
 /// https://drafts.csswg.org/css-fonts-4/#feature-tag-value
 #[derive(Clone, Debug, Eq, MallocSizeOf, PartialEq, ToComputedValue)]
 pub struct FeatureTagValue<Integer> {
     /// A four-character tag, packed into a u32 (one byte per character).
     pub tag: FontTag,
     /// The actual value.
     pub value: Integer,
@@ -112,8 +114,149 @@ impl<T: ToCss> ToCss for FontSettings<T>
             }
             first = false;
             item.to_css(dest)?;
         }
 
         Ok(())
     }
 }
+
+/// A font four-character tag, represented as a u32 for convenience.
+///
+/// See:
+///   https://drafts.csswg.org/css-fonts-4/#font-variation-settings-def
+///   https://drafts.csswg.org/css-fonts-4/#descdef-font-face-font-feature-settings
+///
+#[derive(Clone, Copy, Debug, Eq, MallocSizeOf, PartialEq, ToComputedValue)]
+pub struct FontTag(pub u32);
+
+impl ToCss for FontTag {
+    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
+    where
+        W: Write,
+    {
+        use byteorder::{BigEndian, ByteOrder};
+        use std::str;
+
+        let mut raw = [0u8; 4];
+        BigEndian::write_u32(&mut raw, self.0);
+        str::from_utf8(&raw).unwrap_or_default().to_css(dest)
+    }
+}
+
+impl Parse for FontTag {
+    fn parse<'i, 't>(
+        _context: &ParserContext,
+        input: &mut Parser<'i, 't>,
+    ) -> Result<Self, ParseError<'i>> {
+        let location = input.current_source_location();
+        let tag = input.expect_string()?;
+
+        // allowed strings of length 4 containing chars: <U+20, U+7E>
+        if tag.len() != 4 || tag.as_bytes().iter().any(|c| *c < b' ' || *c > b'~') {
+            return Err(location.new_custom_error(StyleParseErrorKind::UnspecifiedError))
+        }
+
+        let mut raw = Cursor::new(tag.as_bytes());
+        Ok(FontTag(raw.read_u32::<BigEndian>().unwrap()))
+    }
+}
+
+#[derive(Animate, Clone, ComputeSquaredDistance, Copy, Debug, MallocSizeOf)]
+#[derive(PartialEq, ToAnimatedValue, ToAnimatedZero)]
+/// Additional information for keyword-derived font sizes.
+pub struct KeywordInfo<Length> {
+    /// The keyword used
+    pub kw: KeywordSize,
+    /// A factor to be multiplied by the computed size of the keyword
+    pub factor: f32,
+    /// An additional Au offset to add to the kw*factor in the case of calcs
+    pub offset: Length,
+}
+
+impl<L> KeywordInfo<L>
+where
+    Au: Into<L>,
+{
+    /// KeywordInfo value for font-size: medium
+    pub fn medium() -> Self {
+        KeywordSize::Medium.into()
+    }
+}
+
+impl<L> From<KeywordSize> for KeywordInfo<L>
+where
+    Au: Into<L>,
+{
+    fn from(x: KeywordSize) -> Self {
+        KeywordInfo {
+            kw: x,
+            factor: 1.,
+            offset: Au(0).into(),
+        }
+    }
+}
+
+/// CSS font keywords
+#[derive(Animate, ComputeSquaredDistance, MallocSizeOf, ToAnimatedValue, ToAnimatedZero)]
+#[derive(Clone, Copy, Debug, PartialEq)]
+#[allow(missing_docs)]
+pub enum KeywordSize {
+    XXSmall = 1, // This is to enable the NonZero optimization
+                 // which simplifies the representation of Option<KeywordSize>
+                 // in bindgen
+    XSmall,
+    Small,
+    Medium,
+    Large,
+    XLarge,
+    XXLarge,
+    // This is not a real font keyword and will not parse
+    // HTML font-size 7 corresponds to this value
+    XXXLarge,
+}
+
+impl KeywordSize {
+    /// Convert to an HTML <font size> value
+    pub fn html_size(&self) -> u8 {
+        match *self {
+            KeywordSize::XXSmall => 0,
+            KeywordSize::XSmall => 1,
+            KeywordSize::Small => 2,
+            KeywordSize::Medium => 3,
+            KeywordSize::Large => 4,
+            KeywordSize::XLarge => 5,
+            KeywordSize::XXLarge => 6,
+            KeywordSize::XXXLarge => 7,
+        }
+    }
+}
+
+impl Default for KeywordSize {
+    fn default() -> Self {
+        KeywordSize::Medium
+    }
+}
+
+impl ToCss for KeywordSize {
+    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
+    where
+        W: Write,
+    {
+        dest.write_str(match *self {
+            KeywordSize::XXSmall => "xx-small",
+            KeywordSize::XSmall => "x-small",
+            KeywordSize::Small => "small",
+            KeywordSize::Medium => "medium",
+            KeywordSize::Large => "large",
+            KeywordSize::XLarge => "x-large",
+            KeywordSize::XXLarge => "xx-large",
+            KeywordSize::XXXLarge => {
+                debug_assert!(
+                    false,
+                    "We should never serialize specified values set via HTML presentation attributes"
+                );
+                "-servo-xxx-large"
+            },
+        })
+    }
+}
diff --git a/servo/components/style/values/generics/size.rs b/servo/components/style/values/generics/size.rs
--- a/servo/components/style/values/generics/size.rs
+++ b/servo/components/style/values/generics/size.rs
@@ -9,17 +9,17 @@ use euclid::Size2D;
 use parser::ParserContext;
 use std::fmt::{self, Write};
 use style_traits::{CssWriter, ParseError, ToCss};
 use values::animated::ToAnimatedValue;
 
 /// A generic size, for `border-*-radius` longhand properties, or
 /// `border-spacing`.
 #[derive(Animate, Clone, ComputeSquaredDistance, Copy, Debug)]
-#[derive(MallocSizeOf, PartialEq, ToComputedValue)]
+#[derive(MallocSizeOf, PartialEq, ToAnimatedZero, ToComputedValue)]
 pub struct Size<L>(pub Size2D<L>);
 
 impl<L> Size<L> {
     #[inline]
     /// Create a new `Size` for an area of given width and height.
     pub fn new(width: L, height: L) -> Size<L> {
         Size(Size2D::new(width, height))
     }
diff --git a/servo/components/style/values/specified/font.rs b/servo/components/style/values/specified/font.rs
--- a/servo/components/style/values/specified/font.rs
+++ b/servo/components/style/values/specified/font.rs
@@ -16,17 +16,18 @@ use parser::{Parse, ParserContext};
 use properties::longhands::system_font::SystemFont;
 #[allow(unused_imports)]
 use std::ascii::AsciiExt;
 use std::fmt::{self, Write};
 use style_traits::{CssWriter, ParseError, StyleParseErrorKind, ToCss};
 use values::CustomIdent;
 use values::computed::{font as computed, Context, Length, NonNegativeLength, ToComputedValue};
 use values::computed::font::{SingleFontFamily, FontFamilyList, FamilyName};
-use values::generics::font::{FontSettings, FeatureTagValue, VariationValue};
+use values::generics::font::{FontSettings, FontTag, FeatureTagValue};
+use values::generics::font::{KeywordInfo as GenericKeywordInfo, KeywordSize, VariationValue};
 use values::specified::{AllowQuirks, Integer, LengthOrPercentage, NoCalcLength, Number};
 use values::specified::length::{AU_PER_PT, AU_PER_PX, FontBaseSize};
 
 const DEFAULT_SCRIPT_MIN_SIZE_PT: u32 = 8;
 const DEFAULT_SCRIPT_SIZE_MULTIPLIER: f64 = 0.71;
 
 #[derive(Clone, Copy, Debug, Eq, MallocSizeOf, PartialEq, ToCss)]
 /// A specified font-weight value
@@ -129,17 +130,17 @@ pub enum FontSize {
     /// will be 1 (with offset 0), but we cascade keywordness even
     /// after font-relative (percent and em) values
     /// have been applied, which is where the ratio
     /// comes in. The offset comes in if we cascaded a calc value,
     /// where the font-relative portion (em and percentage) will
     /// go into the ratio, and the remaining units all computed together
     /// will go into the offset.
     /// See bug 1355707.
-    Keyword(computed::KeywordInfo),
+    Keyword(KeywordInfo),
     /// font-size: smaller
     Smaller,
     /// font-size: larger
     Larger,
     /// Derived from a specified system font.
     System(SystemFont)
 }
 
@@ -335,88 +336,50 @@ impl Parse for FontSizeAdjust {
         if input.try(|input| input.expect_ident_matching("none")).is_ok() {
             return Ok(FontSizeAdjust::None);
         }
 
         Ok(FontSizeAdjust::Number(Number::parse_non_negative(context, input)?))
     }
 }
 
-/// CSS font keywords
-#[derive(Animate, ComputeSquaredDistance, MallocSizeOf, ToAnimatedValue, ToAnimatedZero)]
-#[derive(Clone, Copy, Debug, PartialEq)]
-#[allow(missing_docs)]
-pub enum KeywordSize {
-    XXSmall = 1, // This is to enable the NonZero optimization
-                 // which simplifies the representation of Option<KeywordSize>
-                 // in bindgen
-    XSmall,
-    Small,
-    Medium,
-    Large,
-    XLarge,
-    XXLarge,
-    // This is not a real font keyword and will not parse
-    // HTML font-size 7 corresponds to this value
-    XXXLarge,
+/// Additional information for specified keyword-derived font sizes.
+pub type KeywordInfo = GenericKeywordInfo<NonNegativeLength>;
+
+impl KeywordInfo {
+    /// Computes the final size for this font-size keyword, accounting for
+    /// text-zoom.
+    pub fn to_computed_value(&self, context: &Context) -> NonNegativeLength {
+        let base = context.maybe_zoom_text(self.kw.to_computed_value(context));
+        base.scale_by(self.factor) + context.maybe_zoom_text(self.offset)
+    }
+
+    /// Given a parent keyword info (self), apply an additional factor/offset to it
+    pub fn compose(self, factor: f32, offset: NonNegativeLength) -> Self {
+        KeywordInfo {
+            kw: self.kw,
+            factor: self.factor * factor,
+            offset: self.offset.scale_by(factor) + offset,
+        }
+    }
 }
 
 impl KeywordSize {
-    /// Parse a keyword size
+    /// Parses a keyword size.
     pub fn parse<'i, 't>(input: &mut Parser<'i, 't>) -> Result<Self, ParseError<'i>> {
         try_match_ident_ignore_ascii_case! { input,
             "xx-small" => Ok(KeywordSize::XXSmall),
             "x-small" => Ok(KeywordSize::XSmall),
             "small" => Ok(KeywordSize::Small),
             "medium" => Ok(KeywordSize::Medium),
             "large" => Ok(KeywordSize::Large),
             "x-large" => Ok(KeywordSize::XLarge),
             "xx-large" => Ok(KeywordSize::XXLarge),
         }
     }
-
-    /// Convert to an HTML <font size> value
-    pub fn html_size(&self) -> u8 {
-        match *self {
-            KeywordSize::XXSmall => 0,
-            KeywordSize::XSmall => 1,
-            KeywordSize::Small => 2,
-            KeywordSize::Medium => 3,
-            KeywordSize::Large => 4,
-            KeywordSize::XLarge => 5,
-            KeywordSize::XXLarge => 6,
-            KeywordSize::XXXLarge => 7,
-        }
-    }
-}
-
-impl Default for KeywordSize {
-    fn default() -> Self {
-        KeywordSize::Medium
-    }
-}
-
-impl ToCss for KeywordSize {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        dest.write_str(match *self {
-            KeywordSize::XXSmall => "xx-small",
-            KeywordSize::XSmall => "x-small",
-            KeywordSize::Small => "small",
-            KeywordSize::Medium => "medium",
-            KeywordSize::Large => "large",
-            KeywordSize::XLarge => "x-large",
-            KeywordSize::XXLarge => "xx-large",
-            KeywordSize::XXXLarge => unreachable!("We should never serialize \
-                                      specified values set via
-                                      HTML presentation attributes"),
-        })
-    }
 }
 
 /// This is the ratio applied for font-size: larger
 /// and smaller by both Firefox and Chrome
 const LARGER_FONT_SIZE_RATIO: f32 = 1.2;
 
 /// The default font size.
 pub const FONT_MEDIUM_PX: i32 = 16;
@@ -668,17 +631,17 @@ impl FontSize {
         } else {
             None
         }
     }
 
     #[inline]
     /// Get initial value for specified font size.
     pub fn medium() -> Self {
-        FontSize::Keyword(computed::KeywordInfo::medium())
+        FontSize::Keyword(KeywordInfo::medium())
     }
 
     /// Parses a font-size, with quirks.
     pub fn parse_quirky<'i, 't>(
         context: &ParserContext,
         input: &mut Parser<'i, 't>,
         allow_quirks: AllowQuirks
     ) -> Result<FontSize, ParseError<'i>> {
@@ -1942,60 +1905,16 @@ impl Parse for FontLanguageOverride {
             return Ok(FontLanguageOverride::Normal)
         }
 
         let string = input.expect_string()?;
         Ok(FontLanguageOverride::Override(string.as_ref().to_owned().into_boxed_str()))
     }
 }
 
-/// A font four-character tag, represented as a u32 for convenience.
-///
-/// See:
-///   https://drafts.csswg.org/css-fonts-4/#font-variation-settings-def
-///   https://drafts.csswg.org/css-fonts-4/#descdef-font-face-font-feature-settings
-///
-#[derive(Clone, Copy, Debug, Eq, MallocSizeOf, PartialEq, ToComputedValue)]
-pub struct FontTag(pub u32);
-
-impl Parse for FontTag {
-    fn parse<'i, 't>(
-        _context: &ParserContext,
-        input: &mut Parser<'i, 't>,
-    ) -> Result<Self, ParseError<'i>> {
-        use byteorder::{ReadBytesExt, BigEndian};
-        use std::io::Cursor;
-
-        let location = input.current_source_location();
-        let tag = input.expect_string()?;
-
-        // allowed strings of length 4 containing chars: <U+20, U+7E>
-        if tag.len() != 4 || tag.as_bytes().iter().any(|c| *c < b' ' || *c > b'~') {
-            return Err(location.new_custom_error(StyleParseErrorKind::UnspecifiedError))
-        }
-
-        let mut raw = Cursor::new(tag.as_bytes());
-        Ok(FontTag(raw.read_u32::<BigEndian>().unwrap()))
-    }
-}
-
-impl ToCss for FontTag {
-    fn to_css<W>(&self, dest: &mut CssWriter<W>) -> fmt::Result
-    where
-        W: Write,
-    {
-        use byteorder::{BigEndian, ByteOrder};
-        use std::str;
-
-        let mut raw = [0u8; 4];
-        BigEndian::write_u32(&mut raw, self.0);
-        str::from_utf8(&raw).unwrap_or_default().to_css(dest)
-    }
-}
-
 /// This property provides low-level control over OpenType or TrueType font
 /// variations.
 pub type FontVariationSettings = FontSettings<VariationValue<Number>>;
 
 fn parse_one_feature_value<'i, 't>(
     context: &ParserContext,
     input: &mut Parser<'i, 't>,
 ) -> Result<Integer, ParseError<'i>> {
