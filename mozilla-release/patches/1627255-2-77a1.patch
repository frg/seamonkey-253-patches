# HG changeset patch
# User Jan de Mooij <jdemooij@mozilla.com>
# Date 1586271153 0
# Node ID ddd50e956bc0807783580f04cc3d225a061ecef8
# Parent  f64cba8d1c73c0e18f4c684fbd4ccffb5cfa0e2f
Bug 1627255 part 2 - Rename the --enable-ion configure option to --enable-jit. r=glandium

For many years now this has covered all our JIT codegen, not just Ion, so this is
a lot less confusing. Using --enable-ion/--disable-ion now results in an error that
suggests the new name.

Differential Revision: https://phabricator.services.mozilla.com/D69579

diff --git a/js/moz.configure b/js/moz.configure
--- a/js/moz.configure
+++ b/js/moz.configure
@@ -82,33 +82,41 @@ def static_js(value):
         return True
 
 set_define('MOZ_STATIC_JS', static_js)
 
 
 # JIT support
 # =======================================================
 @depends(target)
-def ion_default(target):
+def jit_default(target):
     if target.cpu in ('x86', 'x86_64', 'arm', 'aarch64', 'mips32', 'mips64'):
         return True
+    return False
 
-js_option('--enable-ion',
-          default=ion_default,
-          help='{Enable|Disable} use of the IonMonkey JIT')
+js_option('--enable-jit',
+          default=jit_default,
+          help='{Enable|Disable} use of the JITs')
+
+@deprecated_option('--enable-ion')
+def report_deprecated(value):
+    if value:
+        die('--enable-ion is deprecated, use --enable-jit instead')
+    else:
+        die('--disable-ion is deprecated, use --disable-jit instead')
 
 # JIT code simulator for cross compiles
 # =======================================================
 js_option('--enable-simulator', choices=('arm', 'arm64', 'mips32', 'mips64'),
           nargs=1,
           help='Enable a JIT code simulator for the specified architecture')
 
-@depends('--enable-ion', '--enable-simulator', target)
-def simulator(ion_enabled, simulator_enabled, target):
-    if not ion_enabled or not simulator_enabled:
+@depends('--enable-jit', '--enable-simulator', target)
+def simulator(jit_enabled, simulator_enabled, target):
+    if not jit_enabled or not simulator_enabled:
         return
 
     sim_cpu = simulator_enabled[0]
 
     if sim_cpu in ('arm', 'mips32'):
         if target.cpu != 'x86':
             die('The %s simulator only works on x86.' % sim_cpu)
 
@@ -124,19 +132,19 @@ set_config('JS_SIMULATOR_ARM64', simulat
 set_config('JS_SIMULATOR_MIPS32', simulator.mips32)
 set_config('JS_SIMULATOR_MIPS64', simulator.mips64)
 set_define('JS_SIMULATOR', depends_if(simulator)(lambda x: True))
 set_define('JS_SIMULATOR_ARM', simulator.arm)
 set_define('JS_SIMULATOR_ARM64', simulator.arm64)
 set_define('JS_SIMULATOR_MIPS32', simulator.mips32)
 set_define('JS_SIMULATOR_MIPS64', simulator.mips64)
 
-@depends('--enable-ion', simulator, target)
-def jit_codegen(ion_enabled, simulator, target):
-    if not ion_enabled:
+@depends('--enable-jit', simulator, target)
+def jit_codegen(jit_enabled, simulator, target):
+    if not jit_enabled:
         return namespace(none=True)
 
     if simulator:
         return simulator
 
     if target.cpu == 'aarch64':
         return namespace(arm64=True)
     elif target.cpu == 'x86_64':
@@ -154,34 +162,34 @@ set_config('JS_CODEGEN_X64', jit_codegen
 set_define('JS_CODEGEN_NONE', jit_codegen.none)
 set_define('JS_CODEGEN_ARM', jit_codegen.arm)
 set_define('JS_CODEGEN_ARM64', jit_codegen.arm64)
 set_define('JS_CODEGEN_MIPS32', jit_codegen.mips32)
 set_define('JS_CODEGEN_MIPS64', jit_codegen.mips64)
 set_define('JS_CODEGEN_X86', jit_codegen.x86)
 set_define('JS_CODEGEN_X64', jit_codegen.x64)
 
-@depends('--enable-ion', simulator, target, moz_debug)
-def jit_disasm_arm(ion_enabled, simulator, target, debug):
-    if not ion_enabled:
+@depends('--enable-jit', simulator, target, moz_debug)
+def jit_disasm_arm(jit_enabled, simulator, target, debug):
+    if not jit_enabled:
         return
 
     if simulator and debug:
         if getattr(simulator, 'arm', None):
             return True
 
     if target.cpu == 'arm' and debug:
         return True
 
 set_config('JS_DISASM_ARM', jit_disasm_arm)
 set_define('JS_DISASM_ARM', jit_disasm_arm)
 
-@depends('--enable-ion', simulator, target, moz_debug)
-def jit_disasm_arm64(ion_enabled, simulator, target, debug):
-    if not ion_enabled:
+@depends('--enable-jit', simulator, target, moz_debug)
+def jit_disasm_arm64(jit_enabled, simulator, target, debug):
+    if not jit_enabled:
         return
 
     if simulator and debug:
         if getattr(simulator, 'arm64', None):
             return True
 
     if target.cpu == 'aarch64' and debug:
         return True
diff --git a/js/src/jsapi-tests/testPreserveJitCode.cpp b/js/src/jsapi-tests/testPreserveJitCode.cpp
--- a/js/src/jsapi-tests/testPreserveJitCode.cpp
+++ b/js/src/jsapi-tests/testPreserveJitCode.cpp
@@ -38,20 +38,21 @@ testPreserveJitCode(bool preserveJitCode
     cx->options().setBaseline(true);
     cx->options().setIon(true);
     cx->runtime()->setOffthreadIonCompilationEnabled(false);
 
     RootedObject global(cx, createTestGlobal(preserveJitCode));
     CHECK(global);
     JSAutoCompartment ac(cx, global);
 
-    // The Ion JIT may be unavailable due to --disable-ion or lack of support
+    // The Ion JIT may be unavailable due to --disable-jit or lack of support
     // for this platform.
-    if (!js::jit::IsIonEnabled(cx))
+    if (!js::jit::IsIonEnabled(cx)) {
         knownFail = true;
+    }
 
     CHECK_EQUAL(countIonScripts(global), 0u);
 
     const char* source =
         "var i = 0;\n"
         "var sum = 0;\n"
         "while (i < 10) {\n"
         "    sum += i;\n"
