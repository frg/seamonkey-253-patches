# HG changeset patch
# User Brad Werth <bwerth@mozilla.com>
# Date 1516918453 28800
# Node ID cac787a13132ce9498ff6a66793a49d6a2ce3eea
# Parent  d45873f6dcc403cea7fdf64b86c4feebe6652049
Bug 1418930 Part 2: Extend ImageLoader with a parameter to add anonymous CORS headers. r=bz,emilio

MozReview-Commit-ID: LobEEXmuNZ2

diff --git a/layout/style/ImageLoader.cpp b/layout/style/ImageLoader.cpp
--- a/layout/style/ImageLoader.cpp
+++ b/layout/style/ImageLoader.cpp
@@ -239,31 +239,35 @@ ImageLoader::ClearFrames(nsPresContext* 
   }
 
   mRequestToFrameMap.Clear();
   mFrameToRequestMap.Clear();
 }
 
 void
 ImageLoader::LoadImage(nsIURI* aURI, nsIPrincipal* aOriginPrincipal,
-                       nsIURI* aReferrer, ImageLoader::Image* aImage)
+                       nsIURI* aReferrer, ImageLoader::Image* aImage,
+                       CORSMode aCorsMode)
 {
   NS_ASSERTION(aImage->mRequests.Count() == 0, "Huh?");
 
   aImage->mRequests.Put(nullptr, nullptr);
 
   if (!aURI) {
     return;
   }
 
+  int32_t loadFlags = nsIRequest::LOAD_NORMAL |
+                      nsContentUtils::CORSModeToLoadImageFlags(aCorsMode);
+
   RefPtr<imgRequestProxy> request;
   nsresult rv = nsContentUtils::LoadImage(aURI, mDocument, mDocument,
                                           aOriginPrincipal, 0, aReferrer,
                                           mDocument->GetReferrerPolicy(),
-                                          nullptr, nsIRequest::LOAD_NORMAL,
+                                          nullptr, loadFlags,
                                           NS_LITERAL_STRING("css"),
                                           getter_AddRefs(request));
 
   if (NS_FAILED(rv) || !request) {
     return;
   }
 
   RefPtr<imgRequestProxy> clonedRequest;
diff --git a/layout/style/ImageLoader.h b/layout/style/ImageLoader.h
--- a/layout/style/ImageLoader.h
+++ b/layout/style/ImageLoader.h
@@ -5,16 +5,17 @@
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 // A class that handles style system image loads (other image loads are handled
 // by the nodes in the content tree).
 
 #ifndef mozilla_css_ImageLoader_h___
 #define mozilla_css_ImageLoader_h___
 
+#include "CORSMode.h"
 #include "nsClassHashtable.h"
 #include "nsHashKeys.h"
 #include "nsTArray.h"
 #include "imgIRequest.h"
 #include "imgINotificationObserver.h"
 #include "mozilla/Attributes.h"
 
 class imgIContainer;
@@ -60,17 +61,17 @@ public:
   void SetAnimationMode(uint16_t aMode);
 
   // The prescontext for this ImageLoader's document. We need it to be passed
   // in because this can be called during presentation destruction after the
   // presshell pointer on the document has been cleared.
   void ClearFrames(nsPresContext* aPresContext);
 
   void LoadImage(nsIURI* aURI, nsIPrincipal* aPrincipal, nsIURI* aReferrer,
-                 Image* aCSSValue);
+                 Image* aCSSValue, CORSMode aCorsMode);
 
   void DestroyRequest(imgIRequest* aRequest);
 
   void FlushUseCounters();
 
 private:
   ~ImageLoader() {}
 
diff --git a/layout/style/nsCSSValue.cpp b/layout/style/nsCSSValue.cpp
--- a/layout/style/nsCSSValue.cpp
+++ b/layout/style/nsCSSValue.cpp
@@ -3,16 +3,17 @@
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 /* representation of simple property values within CSS declarations */
 
 #include "nsCSSValue.h"
 
+#include "mozilla/CORSMode.h"
 #include "mozilla/ServoBindings.h"
 #include "mozilla/ServoStyleSet.h"
 #include "mozilla/ServoTypes.h"
 #include "mozilla/StyleSheetInlines.h"
 #include "mozilla/Likely.h"
 #include "mozilla/MemoryReporting.h"
 #include "mozilla/css/ImageLoader.h"
 #include "CSSCalc.h"
@@ -3161,17 +3162,19 @@ css::ImageValue::Initialize(nsIDocument*
   nsIDocument* loadingDoc = aDocument->GetOriginalDocument();
   if (!loadingDoc) {
     loadingDoc = aDocument;
   }
 
   if (!mLoadedImage) {
     loadingDoc->StyleImageLoader()->LoadImage(GetURI(),
                                               mExtraData->GetPrincipal(),
-                                              mExtraData->GetReferrer(), this);
+                                              mExtraData->GetReferrer(),
+                                              this,
+                                              CORSMode::CORS_NONE);
 
      mLoadedImage = true;
   }
 
   aDocument->StyleImageLoader()->MaybeRegisterCSSImage(this);
 }
 
 css::ImageValue::~ImageValue()

