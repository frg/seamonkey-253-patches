# HG changeset patch
# User Ehsan Akhgari <ehsan@mozilla.com>
# Date 1534878026 14400
# Node ID 9b2e26f618a443989ab88e724ecc52d160bd093d
# Parent  770ffe58f5b0911eb632a8d9a8ad6d1c934c1242
Bug 1485114 - Avoid creating nsPermission objects in the permission manager needlessly; r=nika

diff --git a/extensions/cookie/nsPermissionManager.cpp b/extensions/cookie/nsPermissionManager.cpp
--- a/extensions/cookie/nsPermissionManager.cpp
+++ b/extensions/cookie/nsPermissionManager.cpp
@@ -5,16 +5,17 @@
  * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/Attributes.h"
 #include "mozilla/DebugOnly.h"
 
 #include "mozilla/dom/ContentParent.h"
 #include "mozilla/dom/ContentChild.h"
 #include "mozilla/BasePrincipal.h"
+#include "mozilla/Pair.h"
 #include "mozilla/Services.h"
 #include "mozilla/SystemGroup.h"
 #include "mozilla/Unused.h"
 #include "nsPermissionManager.h"
 #include "nsPermission.h"
 #include "nsCRT.h"
 #include "nsNetUtil.h"
 #include "nsCOMArray.h"
@@ -2066,64 +2067,41 @@ nsPermissionManager::RemoveAllSince(int6
   ENSURE_NOT_CHILD_PROCESS;
   return RemoveAllModifiedSince(aSince);
 }
 
 template<class T>
 nsresult
 nsPermissionManager::RemovePermissionEntries(T aCondition)
 {
-  nsCOMArray<nsIPermission> array;
+  AutoTArray<Pair<nsCOMPtr<nsIPrincipal>, nsCString>, 10> array;
   for (auto iter = mPermissionTable.Iter(); !iter.Done(); iter.Next()) {
     PermissionHashKey* entry = iter.Get();
     for (const auto& permEntry : entry->GetPermissions()) {
       if (!aCondition(permEntry)) {
         continue;
       }
 
       nsCOMPtr<nsIPrincipal> principal;
       nsresult rv = GetPrincipalFromOrigin(entry->GetKey()->mOrigin,
                                            getter_AddRefs(principal));
       if (NS_FAILED(rv)) {
         continue;
       }
 
-      nsCOMPtr<nsIPermission> permission =
-        nsPermission::Create(principal,
-                             mTypeArray.ElementAt(permEntry.mType),
-                             permEntry.mPermission,
-                             permEntry.mExpireType,
-                             permEntry.mExpireTime);
-      if (NS_WARN_IF(!permission)) {
-        continue;
-      }
-      array.AppendObject(permission);
+      array.AppendElement(MakePair(principal,
+                                   mTypeArray.ElementAt(permEntry.mType)));
     }
   }
 
-  for (int32_t i = 0; i<array.Count(); ++i) {
-    nsCOMPtr<nsIPrincipal> principal;
-    nsAutoCString type;
-
-    nsresult rv = array[i]->GetPrincipal(getter_AddRefs(principal));
-    if (NS_FAILED(rv)) {
-      NS_ERROR("GetPrincipal() failed!");
-      continue;
-    }
-
-    rv = array[i]->GetType(type);
-    if (NS_FAILED(rv)) {
-      NS_ERROR("GetType() failed!");
-      continue;
-    }
-
+  for (size_t i = 0; i < array.Length(); ++i) {
     // AddInternal handles removal, so let it do the work...
     AddInternal(
-      principal,
-      type,
+      array[i].first(),
+      array[i].second(),
       nsIPermissionManager::UNKNOWN_ACTION,
       0,
       nsIPermissionManager::EXPIRE_NEVER, 0, 0,
       nsPermissionManager::eNotify,
       nsPermissionManager::eWriteToDB);
   }
   // now re-import any defaults as they may now be required if we just deleted
   // an override.
@@ -2659,54 +2637,40 @@ nsPermissionManager::RemovePermissionsWi
   }
 
   return RemovePermissionsWithAttributes(pattern);
 }
 
 nsresult
 nsPermissionManager::RemovePermissionsWithAttributes(mozilla::OriginAttributesPattern& aPattern)
 {
-  nsCOMArray<nsIPermission> permissions;
+  AutoTArray<Pair<nsCOMPtr<nsIPrincipal>, nsCString>, 10> permissions;
   for (auto iter = mPermissionTable.Iter(); !iter.Done(); iter.Next()) {
     PermissionHashKey* entry = iter.Get();
 
     nsCOMPtr<nsIPrincipal> principal;
     nsresult rv = GetPrincipalFromOrigin(entry->GetKey()->mOrigin,
                                          getter_AddRefs(principal));
     if (NS_FAILED(rv)) {
       continue;
     }
 
     if (!aPattern.Matches(principal->OriginAttributesRef())) {
       continue;
     }
 
     for (const auto& permEntry : entry->GetPermissions()) {
-      nsCOMPtr<nsIPermission> permission =
-        nsPermission::Create(principal,
-                             mTypeArray.ElementAt(permEntry.mType),
-                             permEntry.mPermission,
-                             permEntry.mExpireType,
-                             permEntry.mExpireTime);
-      if (NS_WARN_IF(!permission)) {
-        continue;
-      }
-      permissions.AppendObject(permission);
+      permissions.AppendElement(MakePair(principal,
+                                         mTypeArray.ElementAt(permEntry.mType)));
     }
   }
 
-  for (int32_t i = 0; i < permissions.Count(); ++i) {
-    nsCOMPtr<nsIPrincipal> principal;
-    nsAutoCString type;
-
-    permissions[i]->GetPrincipal(getter_AddRefs(principal));
-    permissions[i]->GetType(type);
-
-    AddInternal(principal,
-                type,
+  for (size_t i = 0; i < permissions.Length(); ++i) {
+    AddInternal(permissions[i].first(),
+                permissions[i].second(),
                 nsIPermissionManager::UNKNOWN_ACTION,
                 0,
                 nsIPermissionManager::EXPIRE_NEVER,
                 0,
                 0,
                 nsPermissionManager::eNotify,
                 nsPermissionManager::eWriteToDB);
   }
