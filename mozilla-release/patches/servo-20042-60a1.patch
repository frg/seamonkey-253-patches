# HG changeset patch
# User Xidorn Quan <me@upsuper.org>
# Date 1518567394 18000
# Node ID f88ab27ed42a201ad796123904a3eda33aa6981e
# Parent  b347653d529813bba7f41f52e9fccab2f486e041
servo: Merge #20042 - Integrate font-variation-settings descriptor with stylo (from upsuper:font-variation-desc); r=emilio

This is the Servo side change for [bug 1435984](https://bugzilla.mozilla.org/show_bug.cgi?id=1435984).

Source-Repo: https://github.com/servo/servo
Source-Revision: 27cd4f7b5387f188570a355c58bf87b9a029d76e

diff --git a/servo/components/style/font_face.rs b/servo/components/style/font_face.rs
--- a/servo/components/style/font_face.rs
+++ b/servo/components/style/font_face.rs
@@ -21,17 +21,17 @@ use properties::longhands::font_language
 use selectors::parser::SelectorParseErrorKind;
 use shared_lock::{SharedRwLockReadGuard, ToCssWithGuard};
 use std::fmt::{self, Write};
 use str::CssStringWriter;
 use style_traits::{Comma, CssWriter, OneOrMoreSeparated, ParseError};
 use style_traits::{StyleParseErrorKind, ToCss};
 use values::computed::font::FamilyName;
 #[cfg(feature = "gecko")]
-use values::specified::font::SpecifiedFontFeatureSettings;
+use values::specified::font::{SpecifiedFontFeatureSettings, FontVariationSettings};
 use values::specified::url::SpecifiedUrl;
 
 /// A source for a font-face rule.
 #[cfg_attr(feature = "servo", derive(Deserialize, Serialize))]
 #[derive(Clone, Debug, Eq, PartialEq, ToCss)]
 pub enum Source {
     /// A `url()` source.
     Url(UrlSource),
@@ -230,27 +230,33 @@ impl Parse for Source {
 
 macro_rules! is_descriptor_enabled {
     ("font-display") => {
         unsafe {
             use gecko_bindings::structs::mozilla;
             mozilla::StylePrefs_sFontDisplayEnabled
         }
     };
+    ("font-variation-settings") => {
+        unsafe {
+            use gecko_bindings::structs::mozilla;
+            mozilla::StylePrefs_sFontVariationsEnabled
+        }
+    };
     ($name: tt) => { true }
 }
 
 macro_rules! font_face_descriptors_common {
     (
         $( #[$doc: meta] $name: tt $ident: ident / $gecko_ident: ident: $ty: ty, )*
     ) => {
         /// Data inside a `@font-face` rule.
         ///
         /// <https://drafts.csswg.org/css-fonts/#font-face-rule>
-        #[derive(Clone, Debug, Eq, PartialEq)]
+        #[derive(Clone, Debug, PartialEq)]
         pub struct FontFaceRuleData {
             $(
                 #[$doc]
                 pub $ident: Option<$ty>,
             )*
             /// Line and column of the @font-face rule source code.
             pub source_location: SourceLocation,
         }
@@ -400,16 +406,21 @@ font_face_descriptors! {
             UnicodeRange { start: 0, end: 0x10FFFF }
         ],
 
         /// The feature settings of this font face.
         "font-feature-settings" feature_settings / mFontFeatureSettings: SpecifiedFontFeatureSettings = {
             font_feature_settings::SpecifiedValue::normal()
         },
 
+        /// The variation settings of this font face.
+        "font-variation-settings" variation_settings / mFontVariationSettings: FontVariationSettings = {
+            font_variation_settings::SpecifiedValue::normal()
+        },
+
         /// The language override of this font face.
         "font-language-override" language_override / mFontLanguageOverride: font_language_override::SpecifiedValue = {
             font_language_override::SpecifiedValue::Normal
         },
     ]
 }
 
 #[cfg(feature = "servo")]
diff --git a/servo/components/style/gecko/rules.rs b/servo/components/style/gecko/rules.rs
--- a/servo/components/style/gecko/rules.rs
+++ b/servo/components/style/gecko/rules.rs
@@ -16,17 +16,17 @@ use gecko_bindings::sugar::ns_css_value:
 use gecko_bindings::sugar::refptr::{RefPtr, UniqueRefPtr};
 use nsstring::nsString;
 use properties::longhands::font_language_override;
 use shared_lock::{ToCssWithGuard, SharedRwLockReadGuard};
 use std::fmt::{self, Write};
 use std::str;
 use str::CssStringWriter;
 use values::computed::font::FamilyName;
-use values::specified::font::SpecifiedFontFeatureSettings;
+use values::specified::font::{FontTag, FontVariationSettings, SpecifiedFontFeatureSettings};
 
 /// A @font-face rule
 pub type FontFaceRule = RefPtr<nsCSSFontFaceRule>;
 
 impl ToNsCssValue for FamilyName {
     fn convert(self, nscssvalue: &mut nsCSSValue) {
         nscssvalue.set_string_from_atom(&self.name)
     }
@@ -45,33 +45,50 @@ impl ToNsCssValue for FontWeight {
                 nscssvalue.set_enum(structs::NS_STYLE_FONT_WEIGHT_NORMAL as i32),
             FontWeight::Bold =>
                 nscssvalue.set_enum(structs::NS_STYLE_FONT_WEIGHT_BOLD as i32),
             FontWeight::Weight(weight) => nscssvalue.set_integer(weight.0 as i32),
         }
     }
 }
 
+impl ToNsCssValue for FontTag {
+    fn convert(self, nscssvalue: &mut nsCSSValue) {
+        let mut raw = [0u8; 4];
+        (&mut raw[..]).write_u32::<BigEndian>(self.0).unwrap();
+        nscssvalue.set_string(str::from_utf8(&raw).unwrap());
+    }
+}
+
 impl ToNsCssValue for SpecifiedFontFeatureSettings {
     fn convert(self, nscssvalue: &mut nsCSSValue) {
         if self.0.is_empty() {
             nscssvalue.set_normal();
             return;
         }
 
         nscssvalue.set_pair_list(self.0.into_iter().map(|entry| {
-            let mut feature = nsCSSValue::null();
-            let mut raw = [0u8; 4];
-            (&mut raw[..]).write_u32::<BigEndian>(entry.tag.0).unwrap();
-            feature.set_string(str::from_utf8(&raw).unwrap());
-
             let mut index = nsCSSValue::null();
             index.set_integer(entry.value.value());
+            (entry.tag.into(), index)
+        }))
+    }
+}
 
-            (feature, index)
+impl ToNsCssValue for FontVariationSettings {
+    fn convert(self, nscssvalue: &mut nsCSSValue) {
+        if self.0.is_empty() {
+            nscssvalue.set_normal();
+            return;
+        }
+
+        nscssvalue.set_pair_list(self.0.into_iter().map(|entry| {
+            let mut value = nsCSSValue::null();
+            value.set_number(entry.value.into());
+            (entry.tag.into(), value)
         }))
     }
 }
 
 impl ToNsCssValue for font_language_override::SpecifiedValue {
     fn convert(self, nscssvalue: &mut nsCSSValue) {
         match self {
             font_language_override::SpecifiedValue::Normal => nscssvalue.set_normal(),
diff --git a/servo/components/style/gecko_bindings/sugar/ns_css_value.rs b/servo/components/style/gecko_bindings/sugar/ns_css_value.rs
--- a/servo/components/style/gecko_bindings/sugar/ns_css_value.rs
+++ b/servo/components/style/gecko_bindings/sugar/ns_css_value.rs
@@ -200,16 +200,21 @@ impl nsCSSValue {
         self.set_int_internal(value, nsCSSUnit::eCSSUnit_Integer)
     }
 
     /// Set to an enumerated value
     pub fn set_enum<T: Into<i32>>(&mut self, value: T) {
         self.set_int_internal(value.into(), nsCSSUnit::eCSSUnit_Enumerated);
     }
 
+    /// Set to a number value
+    pub fn set_number(&mut self, number: f32) {
+        unsafe { bindings::Gecko_CSSValue_SetFloat(self, number, nsCSSUnit::eCSSUnit_Number) }
+    }
+
     /// Set to a url value
     pub fn set_url(&mut self, url: &SpecifiedUrl) {
         unsafe { bindings::Gecko_CSSValue_SetURL(self, url.for_ffi()) }
     }
 
     /// Set to an array of given length
     pub fn set_array(&mut self, len: i32) -> &mut nsCSSValue_Array {
         unsafe { bindings::Gecko_CSSValue_SetArray(self, len) }
@@ -403,8 +408,16 @@ impl IndexMut<usize> for nsCSSValue_Arra
     }
 }
 
 /// Generic conversion to nsCSSValue
 pub trait ToNsCssValue {
     /// Convert
     fn convert(self, nscssvalue: &mut nsCSSValue);
 }
+
+impl<T: ToNsCssValue> From<T> for nsCSSValue {
+    fn from(value: T) -> nsCSSValue {
+        let mut result = nsCSSValue::null();
+        value.convert(&mut result);
+        result
+    }
+}
diff --git a/servo/ports/geckolib/glue.rs b/servo/ports/geckolib/glue.rs
--- a/servo/ports/geckolib/glue.rs
+++ b/servo/ports/geckolib/glue.rs
@@ -4741,17 +4741,17 @@ pub extern "C" fn Servo_ParseFontDescrip
     result: nsCSSValueBorrowedMut,
 ) -> bool {
     use cssparser::UnicodeRange;
     use self::nsCSSFontDesc::*;
     use style::computed_values::{font_stretch, font_style};
     use style::font_face::{FontDisplay, FontWeight, Source};
     use style::properties::longhands::font_language_override;
     use style::values::computed::font::FamilyName;
-    use style::values::specified::font::SpecifiedFontFeatureSettings;
+    use style::values::specified::font::{SpecifiedFontFeatureSettings, FontVariationSettings};
 
     let string = unsafe { (*value).to_string() };
     let mut input = ParserInput::new(&string);
     let mut parser = Parser::new(&mut input);
     let url_data = unsafe { RefPtr::from_ptr_ref(&data) };
     let context = ParserContext::new(
         Origin::Author,
         url_data,
@@ -4791,16 +4791,17 @@ pub extern "C" fn Servo_ParseFontDescrip
         valid = [
             eCSSFontDesc_Family / FamilyName,
             eCSSFontDesc_Style / font_style::T,
             eCSSFontDesc_Weight / FontWeight,
             eCSSFontDesc_Stretch / font_stretch::T,
             eCSSFontDesc_Src / Vec<Source>,
             eCSSFontDesc_UnicodeRange / Vec<UnicodeRange>,
             eCSSFontDesc_FontFeatureSettings / SpecifiedFontFeatureSettings,
+            eCSSFontDesc_FontVariationSettings / FontVariationSettings,
             eCSSFontDesc_FontLanguageOverride / font_language_override::SpecifiedValue,
             eCSSFontDesc_Display / FontDisplay,
         ]
         invalid = [
             eCSSFontDesc_UNKNOWN,
             eCSSFontDesc_COUNT,
         ]
     );
