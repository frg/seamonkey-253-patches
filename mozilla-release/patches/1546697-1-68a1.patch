# HG changeset patch
# User Emilio Cobos Alvarez <emilio@crisal.io>
# Date 1556210447 0
# Node ID b1a3b366de8b9f469bb19d239f47d89d7306b04f
# Parent  95d21c96610220ae69dee410db42f05cd2149b4f
Bug 1546697 - EnumeratedArray should have a copy assignment operator. r=froydnj

I'll use this in a following patch.

Differential Revision: https://phabricator.services.mozilla.com/D28679

diff --git a/mfbt/EnumeratedArray.h b/mfbt/EnumeratedArray.h
--- a/mfbt/EnumeratedArray.h
+++ b/mfbt/EnumeratedArray.h
@@ -86,16 +86,23 @@ public:
   EnumeratedArray& operator =(EnumeratedArray&& aOther)
   {
     for (size_t i = 0; i < kSize; i++) {
       mArray[i] = std::move(aOther.mArray[i]);
     }
     return *this;
   }
 
+  EnumeratedArray& operator=(const EnumeratedArray& aOther) {
+    for (size_t i = 0; i < kSize; i++) {
+      mArray[i] = aOther.mArray[i];
+    }
+    return *this;
+  }
+
   typedef typename ArrayType::iterator               iterator;
   typedef typename ArrayType::const_iterator         const_iterator;
   typedef typename ArrayType::reverse_iterator       reverse_iterator;
   typedef typename ArrayType::const_reverse_iterator const_reverse_iterator;
 
   // Methods for range-based for loops.
   iterator begin() { return mArray.begin(); }
   const_iterator begin() const { return mArray.begin(); }
diff --git a/mfbt/tests/TestEnumeratedArray.cpp b/mfbt/tests/TestEnumeratedArray.cpp
--- a/mfbt/tests/TestEnumeratedArray.cpp
+++ b/mfbt/tests/TestEnumeratedArray.cpp
@@ -1,42 +1,55 @@
 /* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
 /* vim: set ts=8 sts=2 et sw=2 tw=80: */
 /* This Source Code Form is subject to the terms of the Mozilla Public
  * License, v. 2.0. If a copy of the MPL was not distributed with this file,
  * You can obtain one at http://mozilla.org/MPL/2.0/. */
 
 #include "mozilla/EnumeratedArray.h"
 
+using mozilla::EnumeratedArray;
+
 enum class AnimalSpecies
 {
   Cow,
   Sheep,
   Pig,
   Count
 };
 
+using TestArray = EnumeratedArray<AnimalSpecies, AnimalSpecies::Count, int>;
+
 void TestInitialValueByConstructor()
 {
-  using namespace mozilla;
   // Style 1
-  EnumeratedArray<AnimalSpecies, AnimalSpecies::Count, int> headCount(1, 2, 3);
+  TestArray headCount(1, 2, 3);
   MOZ_RELEASE_ASSERT(headCount[AnimalSpecies::Cow] == 1);
   MOZ_RELEASE_ASSERT(headCount[AnimalSpecies::Sheep] == 2);
   MOZ_RELEASE_ASSERT(headCount[AnimalSpecies::Pig] == 3);
   // Style 2
-  EnumeratedArray<AnimalSpecies, AnimalSpecies::Count, int> headCount2{5, 6, 7};
+  TestArray headCount2{5, 6, 7};
   MOZ_RELEASE_ASSERT(headCount2[AnimalSpecies::Cow] == 5);
   MOZ_RELEASE_ASSERT(headCount2[AnimalSpecies::Sheep] == 6);
   MOZ_RELEASE_ASSERT(headCount2[AnimalSpecies::Pig] == 7);
   // Style 3
-  EnumeratedArray<AnimalSpecies, AnimalSpecies::Count, int> headCount3({8, 9, 10});
+  TestArray headCount3({8, 9, 10});
   MOZ_RELEASE_ASSERT(headCount3[AnimalSpecies::Cow] == 8);
   MOZ_RELEASE_ASSERT(headCount3[AnimalSpecies::Sheep] == 9);
   MOZ_RELEASE_ASSERT(headCount3[AnimalSpecies::Pig] == 10);
 }
 
+void TestAssignment() {
+  TestArray headCount{8, 9, 10};
+  TestArray headCount2;
+  headCount2 = headCount;
+  MOZ_RELEASE_ASSERT(headCount2[AnimalSpecies::Cow] == 8);
+  MOZ_RELEASE_ASSERT(headCount2[AnimalSpecies::Sheep] == 9);
+  MOZ_RELEASE_ASSERT(headCount2[AnimalSpecies::Pig] == 10);
+}
+
 int
 main()
 {
   TestInitialValueByConstructor();
+  TestAssignment();
   return 0;
-}
\ No newline at end of file
+}
