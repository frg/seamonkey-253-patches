# HG changeset patch
# User Andrea Marchesini <amarchesini@mozilla.com>
# Date 1533294510 -7200
# Node ID ea6577b8c72f4800808199a7ce00aea4513040a1
# Parent  e74d58cc2937e847aa76ead415333a0dd8a79be4
Bug 1462883 - Performance object must be reset when the inner window changes document, r=bz

diff --git a/dom/base/nsGlobalWindow.cpp b/dom/base/nsGlobalWindow.cpp
--- a/dom/base/nsGlobalWindow.cpp
+++ b/dom/base/nsGlobalWindow.cpp
@@ -3249,17 +3249,22 @@ nsGlobalWindow::SetNewDocument(nsIDocume
 
       if (newInnerWindow->mDoc != aDocument) {
         newInnerWindow->mDoc = aDocument;
 
         // The storage objects contain the URL of the window. We have to
         // recreate them when the innerWindow is reused.
         newInnerWindow->mLocalStorage = nullptr;
         newInnerWindow->mSessionStorage = nullptr;
-
+        newInnerWindow->mPerformance = nullptr;
+
+        // This must be called after nullifying the internal objects because
+        // here we could recreate them, calling the getter methods, and store
+        // them into the JS slots. If we nullify them after, the slot values and
+        // the objects will be out of sync.
         newInnerWindow->ClearDocumentDependentSlots(cx);
 
         // When replacing an initial about:blank document we call
         // ExecutionReady again to update the client creation URL.
         rv = newInnerWindow->ExecutionReady();
         NS_ENSURE_SUCCESS(rv, rv);
       }
     } else {
@@ -3424,20 +3429,26 @@ nsGlobalWindow::InnerSetNewDocument(JSCo
   if (MOZ_LOG_TEST(gDOMLeakPRLog, LogLevel::Debug)) {
     nsIURI *uri = aDocument->GetDocumentURI();
     MOZ_LOG(gDOMLeakPRLog, LogLevel::Debug,
             ("DOMWINDOW %p SetNewDocument %s",
              this, uri ? uri->GetSpecOrDefault().get() : ""));
   }
 
   mDoc = aDocument;
-  ClearDocumentDependentSlots(aCx);
   mFocusedNode = nullptr;
   mLocalStorage = nullptr;
   mSessionStorage = nullptr;
+  mPerformance = nullptr;
+
+  // This must be called after nullifying the internal objects because here we
+  // could recreate them, calling the getter methods, and store them into the JS
+  // slots. If we nullify them after, the slot values and the objects will be
+  // out of sync.
+  ClearDocumentDependentSlots(aCx);
 
 #ifdef DEBUG
   mLastOpenedURI = aDocument->GetDocumentURI();
 #endif
 
   Telemetry::Accumulate(Telemetry::INNERWINDOWS_WITH_MUTATION_LISTENERS,
                         mMutationBits ? 1 : 0);
 
