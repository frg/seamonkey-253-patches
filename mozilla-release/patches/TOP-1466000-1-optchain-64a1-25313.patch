# HG changeset patch
# User Dmitry Butskoy <buc@buc.me>
# Date 1649886745 -7200
# Parent  6bdc07dfce0929e681e03a3a712e891881e517e7
Bug 1466000 - Part 1: Optional chaining prerequsite.

diff --git a/js/src/frontend/BytecodeEmitter.cpp b/js/src/frontend/BytecodeEmitter.cpp
--- a/js/src/frontend/BytecodeEmitter.cpp
+++ b/js/src/frontend/BytecodeEmitter.cpp
@@ -9132,43 +9132,44 @@ BytecodeEmitter::isRestParameter(ParseNo
             return paramName && name == paramName;
         }
     }
 
     return false;
 }
 
 bool
-BytecodeEmitter::emitCallee(ParseNode* callee, ParseNode* call, bool spread, bool* callop)
-{
+BytecodeEmitter::emitCalleeAndThis(ParseNode* callee, ParseNode* call, bool isCall, bool isNew)
+{
+    bool needsThis = !isCall;
     switch (callee->getKind()) {
       case ParseNodeKind::Name:
-        if (!emitGetName(callee, *callop))
+        if (!emitGetName(callee, isCall))
             return false;
         break;
       case ParseNodeKind::Dot:
         MOZ_ASSERT(emitterMode != BytecodeEmitter::SelfHosting);
         if (callee->as<PropertyAccess>().isSuper()) {
-            if (!emitSuperPropOp(callee, JSOP_GETPROP_SUPER, /* isCall = */ *callop))
+            if (!emitSuperPropOp(callee, JSOP_GETPROP_SUPER, isCall))
                 return false;
         } else {
-            if (!emitPropOp(callee, *callop ? JSOP_CALLPROP : JSOP_GETPROP))
+            if (!emitPropOp(callee, isCall ? JSOP_CALLPROP : JSOP_GETPROP))
                 return false;
         }
 
         break;
       case ParseNodeKind::Elem:
         MOZ_ASSERT(emitterMode != BytecodeEmitter::SelfHosting);
         if (callee->as<PropertyByValue>().isSuper()) {
-            if (!emitSuperElemOp(callee, JSOP_GETELEM_SUPER, /* isCall = */ *callop))
+            if (!emitSuperElemOp(callee, JSOP_GETELEM_SUPER, isCall))
                 return false;
         } else {
-            if (!emitElemOp(callee, *callop ? JSOP_CALLELEM : JSOP_GETELEM))
-                return false;
-            if (*callop) {
+            if (!emitElemOp(callee, isCall ? JSOP_CALLELEM : JSOP_GETELEM))
+                return false;
+            if (isCall) {
                 if (!emit1(JSOP_SWAP))
                     return false;
             }
         }
 
         break;
       case ParseNodeKind::Function:
         /*
@@ -9186,29 +9187,41 @@ BytecodeEmitter::emitCallee(ParseNode* c
             emittingRunOnceLambda = true;
             if (!emitTree(callee))
                 return false;
             emittingRunOnceLambda = false;
         } else {
             if (!emitTree(callee))
                 return false;
         }
-        *callop = false;
+        needsThis = true;
         break;
       case ParseNodeKind::SuperBase:
         MOZ_ASSERT(call->isKind(ParseNodeKind::SuperCall));
         MOZ_ASSERT(parser.isSuperBase(callee));
         if (!emit1(JSOP_SUPERFUN))
             return false;
         break;
       default:
         if (!emitTree(callee))
             return false;
-        *callop = false;             /* trigger JSOP_UNDEFINED after */
-        break;
+        needsThis = true;
+        break;
+    }
+
+    if (needsThis) {
+        if (isNew) {
+            if (!emit1(JSOP_IS_CONSTRUCTING)) {
+                return false;
+            }
+        } else {
+            if (!emit1(JSOP_UNDEFINED)) {
+                return false;
+            }
+        }
     }
 
     return true;
 }
 
 bool
 BytecodeEmitter::emitPipeline(ParseNode* pn)
 {
@@ -9216,24 +9229,18 @@ BytecodeEmitter::emitPipeline(ParseNode*
     MOZ_ASSERT(pn->pn_count >= 2);
 
     if (!emitTree(pn->pn_head))
         return false;
 
     ParseNode* callee = pn->pn_head->pn_next;
 
     do {
-        bool callop = true;
-        if (!emitCallee(callee, pn, false, &callop))
-            return false;
-
-        // Emit room for |this|
-        if (!callop) {
-            if (!emit1(JSOP_UNDEFINED))
-                return false;
+        if (!emitCalleeAndThis(callee, pn, true, false)) {
+            return false;
         }
 
         if (!emit2(JSOP_PICK, 2))
             return false;
 
         if (!emitCall(JSOP_CALL, 1, pn))
             return false;
 
@@ -9350,31 +9357,21 @@ BytecodeEmitter::emitCallOrNew(ParseNode
             return emitSelfHostedDefineDataProperty(pn);
         if (pn_callee->name() == cx->names().hasOwn)
             return emitSelfHostedHasOwn(pn);
         if (pn_callee->name() == cx->names().getPropertySuper)
             return emitSelfHostedGetPropertySuper(pn);
         // Fall through
     }
 
-    if (!emitCallee(pn_callee, pn, false, &callop))
-        return false;
-
     bool isNewOp = pn->getOp() == JSOP_NEW || pn->getOp() == JSOP_SPREADNEW ||
                    pn->getOp() == JSOP_SUPERCALL || pn->getOp() == JSOP_SPREADSUPERCALL;
 
-    // Emit room for |this|.
-    if (!callop) {
-        if (isNewOp) {
-            if (!emit1(JSOP_IS_CONSTRUCTING))
-                return false;
-        } else {
-            if (!emit1(JSOP_UNDEFINED))
-                return false;
-        }
+    if (!emitCalleeAndThis(pn_callee, pn, callop, isNewOp)) {
+        return false;
     }
 
     if (!emitArguments(pn_args, callop, spread))
         return false;
 
     uint32_t argc = pn_args->pn_count;
 
     /*
diff --git a/js/src/frontend/BytecodeEmitter.h b/js/src/frontend/BytecodeEmitter.h
--- a/js/src/frontend/BytecodeEmitter.h
+++ b/js/src/frontend/BytecodeEmitter.h
@@ -856,17 +856,18 @@ struct MOZ_STACK_CLASS BytecodeEmitter
 
     MOZ_MUST_USE bool emitClass(ParseNode* pn);
     MOZ_MUST_USE bool emitSuperPropLHS(ParseNode* superBase, bool isCall = false);
     MOZ_MUST_USE bool emitSuperPropOp(ParseNode* pn, JSOp op, bool isCall = false);
     MOZ_MUST_USE bool emitSuperElemOperands(ParseNode* pn,
                                             EmitElemOption opts = EmitElemOption::Get);
     MOZ_MUST_USE bool emitSuperElemOp(ParseNode* pn, JSOp op, bool isCall = false);
 
-    MOZ_MUST_USE bool emitCallee(ParseNode* callee, ParseNode* call, bool spread, bool* callop);
+    MOZ_MUST_USE bool emitCalleeAndThis(ParseNode* callee, ParseNode* call,
+                                        bool isCall, bool isNew);
 
     MOZ_MUST_USE bool emitPipeline(ParseNode* pn);
 
     MOZ_MUST_USE bool emitExportDefault(ParseNode* pn);
 };
 
 class MOZ_RAII AutoCheckUnstableEmitterScope {
 #ifdef DEBUG
