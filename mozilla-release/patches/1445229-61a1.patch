# HG changeset patch
# User Mats Palmgren <mats@mozilla.com>
# Date 1521198697 -3600
# Node ID 023c4f7e7d7682b3c245490ba4df3de7a7b48c0a
# Parent  e97c28fb32cac82b2539f56b67e8084b758cf325
Bug 1445229 - [css-grid] Store the number of grid items per span-length to avoid iterating them again (idempotent change).  r=dholbert

diff --git a/layout/generic/nsGridContainerFrame.cpp b/layout/generic/nsGridContainerFrame.cpp
--- a/layout/generic/nsGridContainerFrame.cpp
+++ b/layout/generic/nsGridContainerFrame.cpp
@@ -4304,17 +4304,23 @@ nsGridContainerFrame::Tracks::ResolveInt
   LineRange GridArea::*       aRange,
   nscoord                     aPercentageBasis,
   SizingConstraint            aConstraint)
 {
   // Resolve Intrinsic Track Sizes
   // http://dev.w3.org/csswg/css-grid/#algo-content
   // We're also setting eIsFlexing on the item state here to speed up
   // FindUsedFlexFraction later.
-  AutoTArray<TrackSize::StateBits, 16> stateBitsPerSpan;
+  struct PerSpanData {
+    PerSpanData() : mItemCountWithSameSpan(0)
+                  , mStateBits(TrackSize::StateBits(0)) {}
+    uint32_t mItemCountWithSameSpan;
+    TrackSize::StateBits mStateBits;
+  };
+  AutoTArray<PerSpanData, 16> perSpanData;
   nsTArray<Step2ItemData> step2Items;
   CSSOrderAwareFrameIterator& iter = aState.mIter;
   gfxContext* rc = &aState.mRenderingContext;
   WritingMode wm = aState.mWM;
   uint32_t maxSpan = 0; // max span of the step2Items items
   // Setup track selector for step 2.2:
   const auto contentBasedMinSelector =
     aConstraint == SizingConstraint::eMinContent ?
@@ -4349,24 +4355,21 @@ nsGridContainerFrame::Tracks::ResolveInt
         gridItem.mState[mAxis] |= ItemState::eApplyAutoMinSize;
       }
 
       if ((state & (TrackSize::eIntrinsicMinSizing |
                     TrackSize::eIntrinsicMaxSizing)) &&
           !(state & TrackSize::eFlexMaxSizing)) {
         // Collect data for Step 2.
         maxSpan = std::max(maxSpan, span);
-        if (span >= stateBitsPerSpan.Length()) {
-          uint32_t len = 2 * span;
-          stateBitsPerSpan.SetCapacity(len);
-          for (uint32_t i = stateBitsPerSpan.Length(); i < len; ++i) {
-            stateBitsPerSpan.AppendElement(TrackSize::StateBits(0));
-          }
+        if (span >= perSpanData.Length()) {
+          perSpanData.SetLength(2 * span);
         }
-        stateBitsPerSpan[span] |= state;
+        perSpanData[span].mItemCountWithSameSpan++;
+        perSpanData[span].mStateBits |= state;
         CachedIntrinsicSizes cache;
         // Calculate data for "Automatic Minimum Size" clamping, if needed.
         bool needed = ((state & TrackSize::eIntrinsicMinSizing) ||
                        aConstraint == SizingConstraint::eNoConstraint) &&
                       (gridItem.mState[mAxis] & ItemState::eApplyAutoMinSize);
         if (needed && TrackSize::IsDefiniteMaxSizing(state)) {
           nscoord minSizeClamp = 0;
           for (auto i = lineRange.mStart, end = lineRange.mEnd; i < end; ++i) {
@@ -4436,42 +4439,39 @@ nsGridContainerFrame::Tracks::ResolveInt
     plan.SetLength(mSizes.Length());
     nsTArray<TrackSize> itemPlan(mSizes.Length());
     itemPlan.SetLength(mSizes.Length());
     // Start / end iterator for items of the same span length:
     auto spanGroupStart = step2Items.begin();
     auto spanGroupEnd = spanGroupStart;
     const auto end = step2Items.end();
     for (; spanGroupStart != end; spanGroupStart = spanGroupEnd) {
-      while (spanGroupEnd != end &&
-             !Step2ItemData::IsSpanLessThan(*spanGroupStart, *spanGroupEnd)) {
-        ++spanGroupEnd;
-      }
-
       const uint32_t span = spanGroupStart->mSpan;
+      spanGroupEnd = spanGroupStart + perSpanData[span].mItemCountWithSameSpan;
+      TrackSize::StateBits stateBitsForSpan = perSpanData[span].mStateBits;
       bool updatedBase = false; // Did we update any mBase in step 2.1 - 2.3?
       TrackSize::StateBits selector(TrackSize::eIntrinsicMinSizing);
-      if (stateBitsPerSpan[span] & selector) {
+      if (stateBitsForSpan & selector) {
         // Step 2.1 MinSize to intrinsic min-sizing.
         updatedBase =
           GrowSizeForSpanningItems<TrackSizingPhase::eIntrinsicMinimums>(
             spanGroupStart, spanGroupEnd, tracks, plan, itemPlan, selector);
       }
 
       selector = contentBasedMinSelector;
-      if (stateBitsPerSpan[span] & selector) {
+      if (stateBitsForSpan & selector) {
         // Step 2.2 MinContentContribution to min-/max-content (and 'auto' when
         // sizing under a min-content constraint) min-sizing.
         updatedBase |=
           GrowSizeForSpanningItems<TrackSizingPhase::eContentBasedMinimums>(
             spanGroupStart, spanGroupEnd, tracks, plan, itemPlan, selector);
       }
 
       selector = maxContentMinSelector;
-      if (stateBitsPerSpan[span] & selector) {
+      if (stateBitsForSpan & selector) {
         // Step 2.3 MaxContentContribution to max-content (and 'auto' when
         // sizing under a max-content constraint) min-sizing.
         updatedBase |=
           GrowSizeForSpanningItems<TrackSizingPhase::eMaxContentMinimums>(
             spanGroupStart, spanGroupEnd, tracks, plan, itemPlan, selector);
       }
 
       if (updatedBase) {
@@ -4479,19 +4479,19 @@ nsGridContainerFrame::Tracks::ResolveInt
         for (TrackSize& sz : mSizes) {
           if (sz.mBase > sz.mLimit) {
             sz.mLimit = sz.mBase;
           }
         }
       }
 
       selector = TrackSize::eIntrinsicMaxSizing;
-      if (stateBitsPerSpan[span] & selector) {
+      if (stateBitsForSpan & selector) {
         const bool willRunStep2_6 =
-          stateBitsPerSpan[span] & TrackSize::eAutoOrMaxContentMaxSizing;
+          stateBitsForSpan & TrackSize::eAutoOrMaxContentMaxSizing;
         // Step 2.5 MinSize to intrinsic max-sizing.
         GrowSizeForSpanningItems<TrackSizingPhase::eIntrinsicMaximums>(
           spanGroupStart, spanGroupEnd, tracks, plan, itemPlan, selector,
           fitContentClamper, willRunStep2_6);
 
         if (willRunStep2_6) {
           // Step 2.6 MaxContentContribution to max-content max-sizing.
           selector = TrackSize::eAutoOrMaxContentMaxSizing;
