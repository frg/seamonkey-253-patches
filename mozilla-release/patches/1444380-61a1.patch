# HG changeset patch
# User Kartikaya Gupta <kgupta@mozilla.com>
# Date 1520873725 14400
# Node ID d45236740b636d635690fb69a8ec46c249a26b36
# Parent  28be170e6022d8f93660361ce75d69cd653dacfb
Bug 1444380 - Update webrender to commit 4ccaede43b3944199f89a42f49093d93409c7f61. r=jrmuizel

MozReview-Commit-ID: EBYITOhYdGz

diff --git a/gfx/webrender/res/brush.glsl b/gfx/webrender/res/brush.glsl
--- a/gfx/webrender/res/brush.glsl
+++ b/gfx/webrender/res/brush.glsl
@@ -86,18 +86,19 @@ void main(void) {
     VertexInfo vi;
 
     // Fetch the dynamic picture that we are drawing on.
     PictureTask pic_task = fetch_picture_task(brush.picture_address);
     ClipArea clip_area = fetch_clip_area(brush.clip_address);
 
     if (pic_task.pic_kind_and_raster_mode > 0.0) {
         vec2 local_pos = local_segment_rect.p0 + aPosition.xy * local_segment_rect.size;
+        vec2 clamped_local_pos = clamp_rect(local_pos, brush_prim.local_clip_rect);
 
-        vec2 device_pos = uDevicePixelRatio * local_pos;
+        vec2 device_pos = uDevicePixelRatio * clamped_local_pos;
 
         vec2 final_pos = device_pos +
                          pic_task.common_data.task_rect.p0 -
                          uDevicePixelRatio * pic_task.content_origin;
 
 #ifdef WR_FEATURE_ALPHA_PASS
         write_clip(
             vec2(0.0),
diff --git a/gfx/webrender/res/cs_text_run.glsl b/gfx/webrender/res/cs_text_run.glsl
--- a/gfx/webrender/res/cs_text_run.glsl
+++ b/gfx/webrender/res/cs_text_run.glsl
@@ -22,23 +22,32 @@ void main(void) {
     int subpx_dir = prim.user_data2;
 
     Glyph glyph = fetch_glyph(prim.specific_prim_address,
                               glyph_index,
                               subpx_dir);
 
     GlyphResource res = fetch_glyph_resource(resource_address);
 
-    // Glyph size is already in device-pixels.
-    // The render task origin is in device-pixels. Offset that by
-    // the glyph offset, relative to its primitive bounding rect.
-    vec2 glyph_size = res.uv_rect.zw - res.uv_rect.xy;
-    vec2 glyph_pos = res.offset + glyph_size * aPosition.xy;
-    vec2 local_pos = prim.task.common_data.task_rect.p0 + glyph_pos * res.scale +
-                     uDevicePixelRatio * (glyph.offset - prim.task.content_origin);
+    // Scale from glyph space to local space.
+    float scale = res.scale / uDevicePixelRatio;
+
+    // Compute the glyph rect in local space.
+    RectWithSize glyph_rect = RectWithSize(scale * res.offset + text.offset + glyph.offset,
+                                           scale * (res.uv_rect.zw - res.uv_rect.xy));
+
+    // Select the corner of the glyph rect that we are processing.
+    vec2 local_pos = (glyph_rect.p0 + glyph_rect.size * aPosition.xy);
+
+    // Clamp the local position to the text run's local clipping rectangle.
+    local_pos = clamp_rect(local_pos, prim.local_clip_rect);
+
+    // Move the point into device pixel space.
+    local_pos = (local_pos - prim.task.content_origin) * uDevicePixelRatio;
+    local_pos += prim.task.common_data.task_rect.p0;
     gl_Position = uTransform * vec4(local_pos, 0.0, 1.0);
 
     vec2 texture_size = vec2(textureSize(sColor0, 0));
     vec2 st0 = res.uv_rect.xy / texture_size;
     vec2 st1 = res.uv_rect.zw / texture_size;
 
     vUv = vec3(mix(st0, st1, aPosition.xy), res.layer);
     vColor = prim.task.color;
diff --git a/gfx/webrender/src/device.rs b/gfx/webrender/src/device.rs
--- a/gfx/webrender/src/device.rs
+++ b/gfx/webrender/src/device.rs
@@ -924,16 +924,32 @@ impl Device {
         debug_assert!(self.inside_frame);
 
         if self.bound_program != program.id {
             self.gl.use_program(program.id);
             self.bound_program = program.id;
         }
     }
 
+    //TODO: remove once the Angle workaround is no longer needed
+    pub fn reset_angle_sampler_metadata(&mut self, texture: &Texture) {
+        self.bind_texture(DEFAULT_TEXTURE, texture);
+        self.gl.tex_parameter_f(
+            texture.target,
+            gl::TEXTURE_BASE_LEVEL,
+            1.0 as _,
+        );
+        self.gl.draw_arrays(gl::TRIANGLES, 0, 1); // dummy draw
+        self.gl.tex_parameter_f(
+            texture.target,
+            gl::TEXTURE_BASE_LEVEL,
+            0.0 as _, // assumes 0.0 is the normal value for this texture
+        );
+    }
+
     pub fn create_texture(
         &mut self,
         target: TextureTarget,
         format: ImageFormat,
     ) -> Texture {
         Texture {
             id: self.gl.gen_textures(1)[0],
             target: get_gl_target(target),
diff --git a/gfx/webrender/src/renderer.rs b/gfx/webrender/src/renderer.rs
--- a/gfx/webrender/src/renderer.rs
+++ b/gfx/webrender/src/renderer.rs
@@ -3088,16 +3088,24 @@ impl Renderer {
 
     fn draw_instanced_batch<T>(
         &mut self,
         data: &[T],
         vertex_array_kind: VertexArrayKind,
         textures: &BatchTextures,
         stats: &mut RendererStats,
     ) {
+        // Work around Angle bug that forgets to update sampler metadata,
+        // by making the use of those samplers uniform across programs.
+        // https://github.com/servo/webrender/wiki/Driver-issues#texturesize-in-vertex-shaders
+        let work_around_angle_bug = cfg!(windows);
+        if work_around_angle_bug {
+            self.device.reset_angle_sampler_metadata(&self.texture_resolver.dummy_cache_texture);
+        }
+
         for i in 0 .. textures.colors.len() {
             self.texture_resolver.bind(
                 &textures.colors[i],
                 TextureSampler::color(i),
                 &mut self.device,
             );
         }
 
diff --git a/gfx/webrender_bindings/revision.txt b/gfx/webrender_bindings/revision.txt
--- a/gfx/webrender_bindings/revision.txt
+++ b/gfx/webrender_bindings/revision.txt
@@ -1,1 +1,1 @@
-5cb71f0f23719795e7c89417d91a7abad8ac20e9
+4ccaede43b3944199f89a42f49093d93409c7f61
