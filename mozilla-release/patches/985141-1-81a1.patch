# HG changeset patch
# User Andrew Halberstadt <ahalberstadt@mozilla.com>
# Date 1595952370 0
# Node ID 162f07f42162f5f36b9ed05aea26b1fe8ae72417
# Parent  c8485f31fa118c768a8bce004f77838fc1794a74
Bug 985141 - [mozbuild] Remove leading underscore from MozbuildObject._activate_virtualenv, r=firefox-build-system-reviewers,perftest-reviewers,andi,AlexandruIonescu,rstewart

This function is used all across the tree and should be considered a public API.

Differential Revision: https://phabricator.services.mozilla.com/D85045

diff --git a/build/upload_generated_sources.py b/build/upload_generated_sources.py
--- a/build/upload_generated_sources.py
+++ b/build/upload_generated_sources.py
@@ -143,17 +143,17 @@ def main(argv):
     parser = argparse.ArgumentParser(
         description='Upload generated source files in ARTIFACT to BUCKET in S3.')
     parser.add_argument('artifact',
                         help='generated-sources artifact from build task')
     args = parser.parse_args(argv)
     region, bucket = get_s3_region_and_bucket()
 
     config = MozbuildObject.from_environment()
-    config._activate_virtualenv()
+    config.activate_virtualenv()
     config.virtualenv_manager.install_pip_package('boto3==1.4.4')
 
     with timed() as elapsed:
         do_work(region=region, bucket=bucket, artifact=args.artifact)
         log.info('Finished in {:.03f}s'.format(elapsed()))
     return 0
 
 
diff --git a/python/mach_commands.py b/python/mach_commands.py
--- a/python/mach_commands.py
+++ b/python/mach_commands.py
@@ -58,17 +58,17 @@ class MachCommands(MachCommandBase):
         append_env = {
             'PYTHONDONTWRITEBYTECODE': str('1'),
         }
 
         if no_virtualenv:
             python_path = sys.executable
             append_env['PYTHONPATH'] = os.pathsep.join(sys.path)
         else:
-            self._activate_virtualenv()
+            self.activate_virtualenv()
             python_path = self.virtualenv_manager.python_path
 
         if exec_file:
             exec(open(exec_file).read())
             return 0
 
         if ipython:
             bindir = os.path.dirname(python_path)
diff --git a/python/mozbuild/mozbuild/base.py b/python/mozbuild/mozbuild/base.py
--- a/python/mozbuild/mozbuild/base.py
+++ b/python/mozbuild/mozbuild/base.py
@@ -818,25 +818,25 @@ class MozbuildObject(ProcessExecutionMix
         This is used as a convenience method to create other
         MozbuildObject-derived class instances. It can only be used on
         classes that have the same constructor arguments as us.
         """
 
         return cls(self.topsrcdir, self.settings, self.log_manager,
                    topobjdir=self.topobjdir)
 
-    def _activate_virtualenv(self):
+    def activate_virtualenv(self):
         self.virtualenv_manager.ensure()
         self.virtualenv_manager.activate()
 
     def _set_log_level(self, verbose):
         self.log_manager.terminal_handler.setLevel(logging.INFO if not verbose else logging.DEBUG)
 
     def ensure_pipenv(self):
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         pipenv = os.path.join(self.virtualenv_manager.bin_path, 'pipenv')
         if not os.path.exists(pipenv):
             for package in ['certifi', 'pipenv', 'six', 'virtualenv', 'virtualenv-clone']:
                 path = os.path.normpath(os.path.join(
                     self.topsrcdir, 'third_party/python', package))
                 self.virtualenv_manager.install_pip_package(path, vendored=True)
         return pipenv
 
@@ -845,17 +845,17 @@ class MozbuildObject(ProcessExecutionMix
             raise Exception('Pipfile not found: %s.' % pipfile)
         self.ensure_pipenv()
         self.virtualenv_manager.activate_pipenv(pipfile, populate, python)
 
     def _ensure_zstd(self):
         try:
             import zstandard  # noqa: F401
         except (ImportError, AttributeError):
-            self._activate_virtualenv()
+            self.activate_virtualenv()
             self.virtualenv_manager.install_pip_package('zstandard>=0.9.0,<=0.13.0')
 
 
 class MachCommandBase(MozbuildObject):
     """Base class for mach command providers that wish to be MozbuildObjects.
 
     This provides a level of indirection so MozbuildObject can be refactored
     without having to change everything that inherits from it.
diff --git a/python/mozbuild/mozbuild/code-analysis/mach_commands.py b/python/mozbuild/mozbuild/code-analysis/mach_commands.py
--- a/python/mozbuild/mozbuild/code-analysis/mach_commands.py
+++ b/python/mozbuild/mozbuild/code-analysis/mach_commands.py
@@ -239,17 +239,17 @@ class StaticAnalysis(MachCommandBase):
     def check(self, source=None, jobs=2, strip=1, verbose=False, checks='-*',
               fix=False, header_filter='', output=None, format='text', outgoing=False):
         from mozbuild.controller.building import (
             StaticAnalysisFooter,
             StaticAnalysisOutputManager,
         )
 
         self._set_log_level(verbose)
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         self.log_manager.enable_unstructured()
 
         rc = self._get_clang_tools(verbose=verbose)
         if rc != 0:
             return rc
 
         if self._is_version_eligible() is False:
             self.log(logging.ERROR, 'static-analysis', {},
@@ -349,17 +349,17 @@ class StaticAnalysis(MachCommandBase):
                      'directory, ~./mozbuild/coverity is used.')
     @CommandArgument('--outgoing', default=False, action='store_true',
                      help='Run coverity on outgoing files from mercurial or git repository')
     @CommandArgument('--full-build', default=False, action='store_true',
                      help='Run a full build for coverity analisys.')
     def check_coverity(self, source=[], output=None, coverity_output_path=None,
                        outgoing=False, full_build=False, verbose=False):
         self._set_log_level(verbose)
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         self.log_manager.enable_unstructured()
 
         if 'MOZ_AUTOMATION' not in os.environ:
             self.log(logging.INFO, 'static-analysis', {},
                      'Coverity based static-analysis cannot be ran outside automation.')
             return
 
         if full_build and outgoing:
@@ -812,17 +812,17 @@ class StaticAnalysis(MachCommandBase):
     @CommandArgument('--outgoing', default=False, action='store_true',
                      help='Run infer checks on outgoing files from repository')
     @CommandArgument('--output', default=None,
                      help='Write infer json output in a file')
     def check_java(self, source=['mobile'], jobs=2, strip=1, verbose=False, checks=[],
                    task='compileWithGeckoBinariesDebugSources',
                    skip_export=False, outgoing=False, output=None):
         self._set_log_level(verbose)
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         self.log_manager.enable_unstructured()
 
         if self.substs['MOZ_BUILD_APP'] != 'mobile/android':
             self.log(logging.WARNING, 'static-analysis', {},
                      'Cannot check java source code unless you are building for android!')
             return 1
         rc = self._check_for_java()
         if rc != 0:
@@ -1077,17 +1077,17 @@ class StaticAnalysis(MachCommandBase):
                      ' This option is only valid on automation environments.')
     @CommandArgument('checker_names', nargs='*', default=[],
                      help='Checkers that are going to be auto-tested.')
     def autotest(self, verbose=False, dump_results=False, intree_tool=False, checker_names=[]):
         # If 'dump_results' is True than we just want to generate the issues files for each
         # checker in particulat and thus 'force_download' becomes 'False' since we want to
         # do this on a local trusted clang-tidy package.
         self._set_log_level(verbose)
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         self._dump_results = dump_results
 
         force_download = not self._dump_results
 
         # Function return codes
         self.TOOLS_SUCCESS = 0
         self.TOOLS_FAILED_DOWNLOAD = 1
         self.TOOLS_UNSUPORTED_PLATFORM = 2
diff --git a/python/mozbuild/mozbuild/frontend/mach_commands.py b/python/mozbuild/mozbuild/frontend/mach_commands.py
--- a/python/mozbuild/mozbuild/frontend/mach_commands.py
+++ b/python/mozbuild/mozbuild/frontend/mach_commands.py
@@ -32,17 +32,17 @@ class MozbuildFileCommands(MachCommandBa
              description='View reference documentation on mozbuild files.')
     @CommandArgument('symbol', default=None, nargs='*',
                      help='Symbol to view help on. If not specified, all will be shown.')
     @CommandArgument('--name-only', '-n', default=False, action='store_true',
                      help='Print symbol names only.')
     def reference(self, symbol, name_only=False):
         # mozbuild.sphinx imports some Sphinx modules, so we need to be sure
         # the optional Sphinx package is installed.
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         self.virtualenv_manager.install_pip_package('Sphinx==1.1.3')
 
         from mozbuild.sphinx import (
             format_module,
             function_reference,
             special_reference,
             variable_reference,
         )
diff --git a/python/mozbuild/mozbuild/mach_commands.py b/python/mozbuild/mozbuild/mach_commands.py
--- a/python/mozbuild/mozbuild/mach_commands.py
+++ b/python/mozbuild/mozbuild/mach_commands.py
@@ -87,17 +87,17 @@ class Watch(MachCommandBase):
                   'https://developer.mozilla.org/docs/Mozilla/Developer_guide/Build_Instructions/Simple_Firefox_build')  # noqa
             return 1
 
         if not self.substs.get('WATCHMAN', None):
             print('mach watch requires watchman to be installed. See '
                   'https://developer.mozilla.org/docs/Mozilla/Developer_guide/Build_Instructions/Incremental_builds_with_filesystem_watching')  # noqa
             return 1
 
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         try:
             self.virtualenv_manager.install_pip_package('pywatchman==1.4.1')
         except Exception:
             print('Could not install pywatchman from pip. See '
                   'https://developer.mozilla.org/docs/Mozilla/Developer_guide/Build_Instructions/Incremental_builds_with_filesystem_watching')  # noqa
             return 1
 
         from mozbuild.faster_daemon import Daemon
@@ -167,17 +167,17 @@ class CargoProvider(MachCommandBase):
 @CommandProvider
 class Doctor(MachCommandBase):
     """Provide commands for diagnosing common build environment problems"""
     @Command('doctor', category='devenv',
              description='')
     @CommandArgument('--fix', default=None, action='store_true',
                      help='Attempt to fix found problems.')
     def doctor(self, fix=None):
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         from mozbuild.doctor import Doctor
         doctor = Doctor(self.topsrcdir, self.topobjdir, fix)
         return doctor.check_all()
 
 
 @CommandProvider
 class Clobber(MachCommandBase):
     NO_AUTO_LOG = True
diff --git a/python/mozbuild/mozbuild/vendor/vendor_python.py b/python/mozbuild/mozbuild/vendor/vendor_python.py
--- a/python/mozbuild/mozbuild/vendor/vendor_python.py
+++ b/python/mozbuild/mozbuild/vendor/vendor_python.py
@@ -23,17 +23,17 @@ class VendorPython(MozbuildObject):
         vendor_dir = mozpath.join(self.topsrcdir, os.path.join("third_party", "python"))
 
         packages = packages or []
         if with_windows_wheel and len(packages) != 1:
             raise Exception(
                 "--with-windows-wheel is only supported for a single package!"
             )
 
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         pip_compile = os.path.join(self.virtualenv_manager.bin_path, "pip-compile")
         if not os.path.exists(pip_compile):
             path = os.path.normpath(
                 os.path.join(self.topsrcdir, "third_party", "python", "pip-tools")
             )
             self.virtualenv_manager.install_pip_package(path, vendored=True)
         spec = os.path.join(vendor_dir, "requirements.in")
         requirements = os.path.join(vendor_dir, "requirements.txt")
diff --git a/python/mozperftest/mozperftest/browser/noderunner.py b/python/mozperftest/mozperftest/browser/noderunner.py
--- a/python/mozperftest/mozperftest/browser/noderunner.py
+++ b/python/mozperftest/mozperftest/browser/noderunner.py
@@ -19,17 +19,17 @@ class NodeRunner(MachEnvironment):
 
         from mozbuild.nodeutil import find_node_executable
 
         self.node_path = os.path.abspath(find_node_executable()[0])
 
     def setup(self):
         """Install the Node.js package.
         """
-        self.mach_cmd._activate_virtualenv()
+        self.mach_cmd.activate_virtualenv()
         self.verify_node_install()
 
     def node(self, args):
         """Invoke node (interactively) with the given arguments."""
         return self.run_process(
             [self.node_path] + args,
             append_env=self.append_env(),
             pass_thru=True,  # Allow user to run Node interactively.
diff --git a/python/mozperftest/mozperftest/mach_commands.py.985141-1.later b/python/mozperftest/mozperftest/mach_commands.py.985141-1.later
new file mode 100644
--- /dev/null
+++ b/python/mozperftest/mozperftest/mach_commands.py.985141-1.later
@@ -0,0 +1,40 @@
+--- mach_commands.py
++++ mach_commands.py
+@@ -61,17 +61,17 @@ class Perftest(MachCommandBase):
+                 "try_mode": "try_task_config",
+             }
+ 
+             task_config = {"parameters": parameters, "version": 2}
+             push_to_try("perftest", "perftest", try_task_config=task_config)
+             return
+ 
+         # run locally
+-        MachCommandBase._activate_virtualenv(self)
++        MachCommandBase.activate_virtualenv(self)
+ 
+         from mozperftest.runner import run_tests
+ 
+         run_tests(mach_cmd=self, **kwargs)
+ 
+ 
+ @CommandProvider
+ class PerftestTests(MachCommandBase):
+@@ -117,17 +117,17 @@ class PerftestTests(MachCommandBase):
+         action="store_true",
+         default=False,
+         help="Skip flake8 and black",
+     )
+     @CommandArgument(
+         "-v", "--verbose", action="store_true", default=False, help="Verbose mode",
+     )
+     def run_tests(self, **kwargs):
+-        MachCommandBase._activate_virtualenv(self)
++        MachCommandBase.activate_virtualenv(self)
+ 
+         from pathlib import Path
+         from mozperftest.runner import _setup_path
+         from mozperftest.utils import install_package, temporary_env
+ 
+         skip_linters = kwargs.get("skip_linters", False)
+         verbose = kwargs.get("verbose", False)
+ 
diff --git a/python/mozperftest/mozperftest/test/xpcshell.py.985141-1.later b/python/mozperftest/mozperftest/test/xpcshell.py.985141-1.later
new file mode 100644
--- /dev/null
+++ b/python/mozperftest/mozperftest/test/xpcshell.py.985141-1.later
@@ -0,0 +1,21 @@
+--- xpcshell.py
++++ xpcshell.py
+@@ -48,17 +48,17 @@ class XPCShell(Layer):
+         self.python_path = mach_cmd.virtualenv_manager.python_path
+         self.topobjdir = mach_cmd.topobjdir
+         self.distdir = mach_cmd.distdir
+         self.bindir = mach_cmd.bindir
+         self.statedir = mach_cmd.statedir
+         self.metrics = []
+ 
+     def setup(self):
+-        self.mach_cmd._activate_virtualenv()
++        self.mach_cmd.activate_virtualenv()
+ 
+     def run(self, metadata):
+         tests = self.get_arg("tests", [])
+         if len(tests) != 1:
+             # for now we support one single test
+             raise NotImplementedError(str(tests))
+ 
+         test = Path(tests[0])
diff --git a/testing/mach_commands.py b/testing/mach_commands.py
--- a/testing/mach_commands.py
+++ b/testing/mach_commands.py
@@ -359,17 +359,17 @@ class CramTest(MachCommandBase):
              description="Mercurial style .t tests for command line applications.")
     @CommandArgument('test_paths', nargs='*', metavar='N',
                      help="Test paths to run. Each path can be a test file or directory. "
                           "If omitted, the entire suite will be run.")
     @CommandArgument('cram_args', nargs=argparse.REMAINDER,
                      help="Extra arguments to pass down to the cram binary. See "
                           "'./mach python -m cram -- -h' for a list of available options.")
     def cramtest(self, cram_args=None, test_paths=None, test_objects=None):
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         import mozinfo
         from manifestparser import TestManifest
 
         if test_objects is None:
             from moztest.resolve import TestResolver
             resolver = self._spawn(TestResolver)
             if test_paths:
                 # If we were given test paths, try to find tests matching them.
@@ -495,17 +495,17 @@ class ChunkFinder(MachCommandBase):
             'chunkByDir': kwargs['chunk_by_dir'],
             'chunkByRuntime': kwargs['chunk_by_runtime'],
             'e10s': kwargs['e10s'],
             'subsuite': subsuite,
         }
 
         temp_dir = None
         if kwargs['platform'] or kwargs['debug']:
-            self._activate_virtualenv()
+            self.activate_virtualenv()
             self.virtualenv_manager.install_pip_package('mozdownload==1.17')
             temp_dir, temp_path = download_mozinfo(
                 kwargs['platform'], kwargs['debug'])
             args['extra_mozinfo_json'] = temp_path
 
         found = False
         for this_chunk in range(1, total_chunks + 1):
             args['thisChunk'] = this_chunk
@@ -903,17 +903,17 @@ class TestInfoCommand(MachCommandBase):
 class TestFluentMigration(MachCommandBase):
     @Command('fluent-migration-test', category='testing',
              description="Test Fluent migration recipes.")
     @CommandArgument('test_paths', nargs='*', metavar='N',
                      help="Recipe paths to test.")
     def run_migration_tests(self, test_paths=None, **kwargs):
         if not test_paths:
             test_paths = []
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         from test_fluent_migrations import fmt
         rv = 0
         with_context = []
         for to_test in test_paths:
             try:
                 context = fmt.inspect_migration(to_test)
                 for issue in context['issues']:
                     self.log(logging.ERROR, 'fluent-migration-test', {
diff --git a/testing/mochitest/mach_commands.py.985141-1.later b/testing/mochitest/mach_commands.py.985141-1.later
new file mode 100644
--- /dev/null
+++ b/testing/mochitest/mach_commands.py.985141-1.later
@@ -0,0 +1,21 @@
+--- mach_commands.py
++++ mach_commands.py
+@@ -285,17 +285,17 @@ class MachCommands(MachCommandBase):
+     def run_mochitest_general(self, flavor=None, test_objects=None, resolve_tests=True, **kwargs):
+         from mochitest_options import ALL_FLAVORS
+         from mozlog.commandline import setup_logging
+         from mozlog.handlers import StreamHandler
+         from moztest.resolve import get_suite_definition
+ 
+         # TODO: This is only strictly necessary while mochitest is using Python
+         # 2 and can be removed once the command is migrated to Python 3.
+-        self._activate_virtualenv()
++        self.activate_virtualenv()
+ 
+         buildapp = None
+         for app in SUPPORTED_APPS:
+             if conditions.is_buildapp_in(self, apps=[app]):
+                 buildapp = app
+                 break
+ 
+         flavors = None
diff --git a/testing/web-platform/mach_commands.py b/testing/web-platform/mach_commands.py
--- a/testing/web-platform/mach_commands.py
+++ b/testing/web-platform/mach_commands.py
@@ -398,17 +398,17 @@ def create_parser_testpaths():
         "--json", action="store_true", default=False,
         help="Output as JSON")
     return parser
 
 
 @CommandProvider
 class MachCommands(MachCommandBase):
     def setup(self):
-        self._activate_virtualenv()
+        self.activate_virtualenv()
 
     @Command("web-platform-tests",
              category="testing",
              conditions=[conditions.is_firefox],
              parser=create_parser_wpt)
     def run_web_platform_tests(self, **params):
         self.setup()
 
diff --git a/toolkit/crashreporter/tools/upload_symbols.py b/toolkit/crashreporter/tools/upload_symbols.py
--- a/toolkit/crashreporter/tools/upload_symbols.py
+++ b/toolkit/crashreporter/tools/upload_symbols.py
@@ -53,17 +53,17 @@ def get_taskcluster_secret(secret_name):
     secret = res.json()
     auth_token = secret['secret']['token']
 
     return auth_token
 
 
 def main():
     config = MozbuildObject.from_environment()
-    config._activate_virtualenv()
+    config.activate_virtualenv()
 
     import redo
     import requests
 
     logging.basicConfig()
     parser = argparse.ArgumentParser(
         description='Upload symbols in ZIP using token from Taskcluster secrets service.')
     parser.add_argument('zip',
diff --git a/tools/lint/mach_commands.py b/tools/lint/mach_commands.py
--- a/tools/lint/mach_commands.py
+++ b/tools/lint/mach_commands.py
@@ -60,17 +60,17 @@ def get_global_excludes(topsrcdir):
 class MachCommands(MachCommandBase):
 
     @Command(
         'lint', category='devenv',
         description='Run linters.',
         parser=setup_argument_parser)
     def lint(self, *runargs, **lintargs):
         """Run linters."""
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         from mozlint import cli, parser
 
         try:
             buildargs = {}
             buildargs['substs'] = copy.deepcopy(dict(self.substs))
             buildargs['defines'] = copy.deepcopy(dict(self.defines))
             buildargs['topobjdir'] = self.topobjdir
             lintargs.update(buildargs)
diff --git a/tools/mach_commands.py b/tools/mach_commands.py
--- a/tools/mach_commands.py
+++ b/tools/mach_commands.py
@@ -171,17 +171,17 @@ def mozregression_import():
     return mozregression.mach_interface
 
 
 def mozregression_create_parser():
     # Create the mozregression command line parser.
     # if mozregression is not installed, or not up to date, it will
     # first be installed.
     cmd = MozbuildObject.from_environment()
-    cmd._activate_virtualenv()
+    cmd.activate_virtualenv()
     mozregression = mozregression_import()
     if not mozregression:
         # mozregression is not here at all, install it
         cmd.virtualenv_manager.install_pip_package('mozregression')
         print("mozregression was installed. please re-run your"
               " command. If you keep getting this message please "
               " manually run: 'pip install -U mozregression'.")
     else:
@@ -209,11 +209,11 @@ def mozregression_create_parser():
 @CommandProvider
 class MozregressionCommand(MachCommandBase):
     @Command('mozregression',
              category='misc',
              description=("Regression range finder for nightly"
                           " and inbound builds."),
              parser=mozregression_create_parser)
     def run(self, **options):
-        self._activate_virtualenv()
+        self.activate_virtualenv()
         mozregression = mozregression_import()
         mozregression.run(options)
