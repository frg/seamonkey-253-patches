# HG changeset patch
# User Jonathan Watt <jwatt@jwatt.org>
# Date 1576861187 0
# Node ID 09a85a7bdbcbb6f9cd02bd1b64ca0de3e3c9be14
# Parent  1bf41c0160f73968ee14a94a357bcb971a5c5da5
Bug 1605144. Address most of the lint errors in python/lldbutils. r=ahal

Differential Revision: https://phabricator.services.mozilla.com/D57817

diff --git a/third_party/python/lldbutils/lldbutils/__init__.py b/third_party/python/lldbutils/lldbutils/__init__.py
--- a/third_party/python/lldbutils/lldbutils/__init__.py
+++ b/third_party/python/lldbutils/lldbutils/__init__.py
@@ -1,12 +1,18 @@
+# Any copyright is dedicated to the Public Domain.
+# http://creativecommons.org/publicdomain/zero/1.0/
+
+from __future__ import absolute_import
 import lldb
 
+
 __all__ = ['content', 'general', 'gfx', 'layout', 'utils']
 
+
 def init():
     for name in __all__:
         init = None
         try:
             init = __import__('lldbutils.' + name, globals(), locals(), ['init']).init
         except AttributeError:
             pass
         if init:
diff --git a/third_party/python/lldbutils/lldbutils/content.py b/third_party/python/lldbutils/lldbutils/content.py
--- a/third_party/python/lldbutils/lldbutils/content.py
+++ b/third_party/python/lldbutils/lldbutils/content.py
@@ -1,21 +1,27 @@
-import lldb
+# Any copyright is dedicated to the Public Domain.
+# http://creativecommons.org/publicdomain/zero/1.0/
+
+from __future__ import absolute_import
 from lldbutils import utils
 
+
 def summarize_text_fragment(valobj, internal_dict):
     content_union = valobj.GetChildAtIndex(0)
     state_union = valobj.GetChildAtIndex(1).GetChildMemberWithName("mState")
     length = state_union.GetChildMemberWithName("mLength").GetValueAsUnsigned(0)
     if state_union.GetChildMemberWithName("mIs2b").GetValueAsUnsigned(0):
         field = "m2b"
     else:
         field = "m1b"
     ptr = content_union.GetChildMemberWithName(field)
     return utils.format_string(ptr, length)
 
+
 def ptag(debugger, command, result, dict):
     """Displays the tag name of a content node."""
     debugger.HandleCommand("expr (" + command + ")->mNodeInfo.mRawPtr->mInner.mName")
 
+
 def init(debugger):
     debugger.HandleCommand("type summary add nsTextFragment -F lldbutils.content.summarize_text_fragment")
     debugger.HandleCommand("command script add -f lldbutils.content.ptag ptag")
diff --git a/third_party/python/lldbutils/lldbutils/general.py b/third_party/python/lldbutils/lldbutils/general.py
--- a/third_party/python/lldbutils/lldbutils/general.py
+++ b/third_party/python/lldbutils/lldbutils/general.py
@@ -1,23 +1,29 @@
+# Any copyright is dedicated to the Public Domain.
+# http://creativecommons.org/publicdomain/zero/1.0/
+
+from __future__ import absolute_import, print_function
 import lldb
 from lldbutils import utils
-from __future__ import print_function
+
 
 def summarize_string(valobj, internal_dict):
     data = valobj.GetChildMemberWithName("mData")
     length = valobj.GetChildMemberWithName("mLength").GetValueAsUnsigned(0)
     return utils.format_string(data, length)
 
+
 def summarize_atom(valobj, internal_dict):
     target = lldb.debugger.GetSelectedTarget()
     length = valobj.GetChildMemberWithName("mLength").GetValueAsUnsigned()
     string = target.EvaluateExpression("(char16_t*)%s.GetUTF16String()" % valobj.GetName())
     return utils.format_string(string, length)
 
+
 class TArraySyntheticChildrenProvider:
     def __init__(self, valobj, internal_dict):
         self.valobj = valobj
         self.header = self.valobj.GetChildMemberWithName("mHdr")
         self.element_type = self.valobj.GetType().GetTemplateArgumentType(0)
         self.element_size = self.element_type.GetByteSize()
         header_size = self.header.GetType().GetPointeeType().GetByteSize()
         self.element_base_addr = self.header.GetValueAsUnsigned(0) + header_size
@@ -25,26 +31,29 @@ class TArraySyntheticChildrenProvider:
     def num_children(self):
         return self.header.Dereference().GetChildMemberWithName("mLength").GetValueAsUnsigned(0)
 
     def get_child_index(self, name):
         try:
             index = int(name)
             if index >= self.num_children():
                 return None
-        except:
+        # Ideally we'd use the exception type, but it's unclear what that is
+        # without knowing how to trigger the original exception.
+        except:  # NOQA: E501
             pass
         return None
 
     def get_child_at_index(self, index):
         if index >= self.num_children():
             return None
         addr = self.element_base_addr + index * self.element_size
         return self.valobj.CreateValueFromAddress("[%d]" % index, addr, self.element_type)
 
+
 def prefcnt(debugger, command, result, dict):
     """Displays the refcount of an object."""
     # We handled regular nsISupports-like refcounted objects and cycle collected
     # objects.
     target = debugger.GetSelectedTarget()
     process = target.GetProcess()
     thread = process.GetSelectedThread()
     frame = thread.GetSelectedFrame()
@@ -68,16 +77,17 @@ def prefcnt(debugger, command, result, d
         print(field.GetChildMemberWithName("mValue").GetChildMemberWithName("mValue").GetValueAsUnsigned(0))
     elif refcnt_type == "int":  # non-atomic mozilla::RefCounted object
         print(field.GetValueAsUnsigned(0))
     elif refcnt_type == "mozilla::Atomic<int>":  # atomic mozilla::RefCounted object
         print(field.GetChildMemberWithName("mValue").GetValueAsUnsigned(0))
     else:
         print("unknown mRefCnt type " + refcnt_type)
 
+
 # Used to work around http://llvm.org/bugs/show_bug.cgi?id=22211
 def callfunc(debugger, command, result, dict):
     """Calls a function for which debugger information is unavailable by getting its address from the symbol table.
        The function is assumed to return void."""
 
     if '(' not in command:
         print('Usage: callfunc your_function(args)')
         return
@@ -93,16 +103,17 @@ def callfunc(debugger, command, result, 
         return
 
     sym = symbols[0]
     arg_types = '()'
     if sym.name and sym.name.startswith(funcname + '('):
         arg_types = sym.name[len(funcname):]
     debugger.HandleCommand('print ((void(*)%s)0x%0x)(%s' % (arg_types, sym.addr.GetLoadAddress(target), args))
 
+
 def init(debugger):
     debugger.HandleCommand("type summary add nsAString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsACString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsFixedString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsFixedCString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsAutoString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsAutoCString -F lldbutils.general.summarize_string")
     debugger.HandleCommand("type summary add nsIAtom -F lldbutils.general.summarize_atom")
diff --git a/third_party/python/lldbutils/lldbutils/gfx.py b/third_party/python/lldbutils/lldbutils/gfx.py
--- a/third_party/python/lldbutils/lldbutils/gfx.py
+++ b/third_party/python/lldbutils/lldbutils/gfx.py
@@ -1,9 +1,13 @@
-import lldb
+# Any copyright is dedicated to the Public Domain.
+# http://creativecommons.org/publicdomain/zero/1.0/
+
+from __future__ import absolute_import
+
 
 def summarize_nscolor(valobj, internal_dict):
     colors = {
         "#800000": "maroon",
         "#ff0000": "red",
         "#ffa500": "orange",
         "#ffff00": "yellow",
         "#808000": "olive",
@@ -25,28 +29,30 @@ def summarize_nscolor(valobj, internal_d
         return "transparent"
     if value & 0xff000000 != 0xff000000:
         return "rgba(%d, %d, %d, %f)" % (value & 0xff, (value >> 8) & 0xff, (value >> 16) & 0xff, ((value >> 24) & 0xff) / 255.0)
     color = "#%02x%02x%02x" % (value & 0xff, (value >> 8) & 0xff, (value >> 16) & 0xff)
     if color in colors:
         return colors[color]
     return color
 
+
 class RegionSyntheticChildrenProvider:
 
-    def __init__(self, valobj, internal_dict, rect_type = "nsRect"):
+    def __init__(self, valobj, internal_dict, rect_type="nsRect"):
         self.rect_type = rect_type
         self.valobj = valobj
         self.pixman_region = self.valobj.GetChildMemberWithName("mImpl")
         self.pixman_data = self.pixman_region.GetChildMemberWithName("data")
         self.pixman_extents = self.pixman_region.GetChildMemberWithName("extents")
         self.num_rects = self.pixman_region_num_rects()
         self.box_type = self.pixman_extents.GetType()
         self.box_type_size = self.box_type.GetByteSize()
-        self.box_list_base_ptr = self.pixman_data.GetValueAsUnsigned(0) + self.pixman_data.GetType().GetPointeeType().GetByteSize()
+        self.box_list_base_ptr = self.pixman_data.GetValueAsUnsigned(0) \
+            + self.pixman_data.GetType().GetPointeeType().GetByteSize()
 
     def pixman_region_num_rects(self):
         if self.pixman_data.GetValueAsUnsigned(0):
             return self.pixman_data.Dereference().GetChildMemberWithName("numRects").GetValueAsUnsigned(0)
         return 1
 
     def num_children(self):
         return 2 + self.num_rects
@@ -76,54 +82,59 @@ class RegionSyntheticChildrenProvider:
         if rect_index >= self.num_rects:
             return None
         if self.num_rects == 1:
             return self.convert_pixman_box_to_rect(self.pixman_extents, 'bounds')
         box_address = self.box_list_base_ptr + rect_index * self.box_type_size
         box = self.pixman_data.CreateValueFromAddress('', box_address, self.box_type)
         return self.convert_pixman_box_to_rect(box, "[%d]" % rect_index)
 
+
 class IntRegionSyntheticChildrenProvider:
     def __init__(self, valobj, internal_dict):
         wrapped_region = valobj.GetChildMemberWithName("mImpl")
         self.wrapped_provider = RegionSyntheticChildrenProvider(wrapped_region, internal_dict, "nsIntRect")
 
     def num_children(self):
         return self.wrapped_provider.num_children()
 
     def get_child_index(self, name):
         return self.wrapped_provider.get_child_index(name)
 
     def get_child_at_index(self, index):
         return self.wrapped_provider.get_child_at_index(index)
 
+
 def summarize_rect(valobj, internal_dict):
     x = valobj.GetChildMemberWithName("x").GetValue()
     y = valobj.GetChildMemberWithName("y").GetValue()
     width = valobj.GetChildMemberWithName("width").GetValue()
     height = valobj.GetChildMemberWithName("height").GetValue()
     return "%s, %s, %s, %s" % (x, y, width, height)
 
+
 def rect_is_empty(valobj):
     width = valobj.GetChildMemberWithName("width").GetValueAsSigned()
     height = valobj.GetChildMemberWithName("height").GetValueAsSigned()
     return width <= 0 or height <= 0
 
+
 def summarize_region(valobj, internal_dict):
     # This function makes use of the synthetic children generated for ns(Int)Regions.
     bounds = valobj.GetChildMemberWithName("bounds")
     bounds_summary = summarize_rect(bounds, internal_dict)
     num_rects = valobj.GetChildMemberWithName("numRects").GetValueAsUnsigned(0)
     if num_rects <= 1:
         if rect_is_empty(bounds):
             return "empty"
         if num_rects == 1:
             return "one rect: " + bounds_summary
     return str(num_rects) + " rects, bounds: " + bounds_summary
 
+
 def init(debugger):
     debugger.HandleCommand("type summary add nscolor -v -F lldbutils.gfx.summarize_nscolor")
     debugger.HandleCommand("type summary add nsRect -v -F lldbutils.gfx.summarize_rect")
     debugger.HandleCommand("type summary add nsIntRect -v -F lldbutils.gfx.summarize_rect")
     debugger.HandleCommand("type summary add gfxRect -v -F lldbutils.gfx.summarize_rect")
     debugger.HandleCommand("type synthetic add nsRegion -l lldbutils.gfx.RegionSyntheticChildrenProvider")
     debugger.HandleCommand("type synthetic add nsIntRegion -l lldbutils.gfx.IntRegionSyntheticChildrenProvider")
     debugger.HandleCommand("type summary add nsRegion -v -F lldbutils.gfx.summarize_region")
diff --git a/third_party/python/lldbutils/lldbutils/layout.py b/third_party/python/lldbutils/lldbutils/layout.py
--- a/third_party/python/lldbutils/lldbutils/layout.py
+++ b/third_party/python/lldbutils/lldbutils/layout.py
@@ -1,20 +1,27 @@
-import lldb
+# Any copyright is dedicated to the Public Domain.
+# http://creativecommons.org/publicdomain/zero/1.0/
+
+from __future__ import absolute_import
+
 
 def frametree(debugger, command, result, dict):
     """Dumps the frame tree containing the given nsIFrame*."""
     debugger.HandleCommand('expr (' + command + ')->DumpFrameTree()')
 
+
 def frametreelimited(debugger, command, result, dict):
     """Dumps the subtree of a frame tree rooted at the given nsIFrame*."""
     debugger.HandleCommand('expr (' + command + ')->DumpFrameTreeLimited()')
 
+
 def pstate(debugger, command, result, dict):
     """Displays a frame's state bits symbolically."""
     debugger.HandleCommand('expr mozilla::PrintFrameState(' + command + ')')
 
+
 def init(debugger):
     debugger.HandleCommand('command script add -f lldbutils.layout.frametree frametree')
     debugger.HandleCommand('command script add -f lldbutils.layout.frametreelimited frametreelimited')
     debugger.HandleCommand('command alias ft frametree')
     debugger.HandleCommand('command alias ftl frametreelimited')
-    debugger.HandleCommand('command script add -f lldbutils.layout.pstate pstate');
+    debugger.HandleCommand('command script add -f lldbutils.layout.pstate pstate')
diff --git a/third_party/python/lldbutils/lldbutils/utils.py b/third_party/python/lldbutils/lldbutils/utils.py
--- a/third_party/python/lldbutils/lldbutils/utils.py
+++ b/third_party/python/lldbutils/lldbutils/utils.py
@@ -1,8 +1,12 @@
+# Any copyright is dedicated to the Public Domain.
+# http://creativecommons.org/publicdomain/zero/1.0/
+
+
 def format_char(c):
     if c == 0:
         return "\\0"
     elif c == 0x07:
         return "\\a"
     elif c == 0x08:
         return "\\b"
     elif c == 0x0c:
@@ -23,16 +27,17 @@ def format_char(c):
         return "\\'"
     elif c < 0x20 or c >= 0x80 and c <= 0xff:
         return "\\x%02x" % c
     elif c >= 0x0100:
         return "\\u%04x" % c
     else:
         return chr(c)
 
+
 # Take an SBValue that is either a char* or char16_t* and formats it like lldb
 # would when printing it.
 def format_string(lldb_value, length=100):
     ptr = lldb_value.GetValueAsUnsigned(0)
     char_type = lldb_value.GetType().GetPointeeType()
     if char_type.GetByteSize() == 1:
         s = "\""
         size = 1
@@ -52,19 +57,21 @@ def format_string(lldb_value, length=100
             break
         s += format_char(c)
         i = i + 1
     s += "\""
     if not terminated and i != length:
         s += "..."
     return s
 
+
 # Dereferences a raw pointer, nsCOMPtr, RefPtr, nsAutoPtr, already_AddRefed or
 # mozilla::RefPtr; otherwise returns the value unchanged.
 def dereference(lldb_value):
     if lldb_value.TypeIsPointerType():
         return lldb_value.Dereference()
     name = lldb_value.GetType().GetUnqualifiedType().GetName()
-    if name.startswith("nsCOMPtr<") or name.startswith("RefPtr<") or name.startswith("nsAutoPtr<") or name.startswith("already_AddRefed<"):
+    if name.startswith("nsCOMPtr<") or name.startswith("RefPtr<") or \
+       name.startswith("nsAutoPtr<") or name.startswith("already_AddRefed<"):
         return lldb_value.GetChildMemberWithName("mRawPtr")
     if name.startswith("mozilla::RefPtr<"):
         return lldb_value.GetChildMemberWithName("ptr")
     return lldb_value
