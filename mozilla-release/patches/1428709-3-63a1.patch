# HG changeset patch
# User Raphael Pierzina <rpierzina@mozilla.com>
# Date 1531413474 -7200
# Node ID e4cf0bcb5b69c87b6596f581eb32c2f4fca55e47
# Parent  b70032519c16f1df64b553bbf722820c043b77c2
Bug 1428709 - Enable several mozhttpd tests for Python 3; r=davehunt

MozReview-Commit-ID: K7m58KGR29N

diff --git a/testing/mozbase/mozhttpd/mozhttpd/mozhttpd.py b/testing/mozbase/mozhttpd/mozhttpd/mozhttpd.py
--- a/testing/mozbase/mozhttpd/mozhttpd/mozhttpd.py
+++ b/testing/mozbase/mozhttpd/mozhttpd/mozhttpd.py
@@ -103,19 +103,19 @@ class RequestHandler(SimpleHTTPRequestHa
                 return True
 
         return False
 
     def _find_path(self):
         """Find the on-disk path to serve this request from,
         using self.path_mappings and self.docroot.
         Return (url_path, disk_path)."""
-        path_components = filter(None, self.request.path.split('/'))
+        path_components = list(filter(None, self.request.path.split('/')))
         for prefix, disk_path in iteritems(self.path_mappings):
-            prefix_components = filter(None, prefix.split('/'))
+            prefix_components = list(filter(None, prefix.split('/')))
             if len(path_components) < len(prefix_components):
                 continue
             if path_components[:len(prefix_components)] == prefix_components:
                 return ('/'.join(path_components[len(prefix_components):]),
                         disk_path)
         if self.docroot:
             return self.request.path, self.docroot
         return None
@@ -161,17 +161,17 @@ class RequestHandler(SimpleHTTPRequestHa
 
     def translate_path(self, path):
         # this is taken from SimpleHTTPRequestHandler.translate_path(),
         # except we serve from self.docroot instead of os.getcwd(), and
         # parse_request()/do_GET() have already stripped the query string and
         # fragment and mangled the path for proxying, if required.
         path = posixpath.normpath(unquote(self.path))
         words = path.split('/')
-        words = filter(None, words)
+        words = list(filter(None, words))
         path = self.disk_root
         for word in words:
             drive, word = os.path.splitdrive(word)
             head, word = os.path.split(word)
             if word in (os.curdir, os.pardir):
                 continue
             path = os.path.join(path, word)
         return path
diff --git a/testing/mozbase/mozhttpd/tests/manifest.ini b/testing/mozbase/mozhttpd/tests/manifest.ini
--- a/testing/mozbase/mozhttpd/tests/manifest.ini
+++ b/testing/mozbase/mozhttpd/tests/manifest.ini
@@ -1,14 +1,10 @@
 [DEFAULT]
 subsuite = mozbase, os == "linux"
 [api.py]
 skip-if = python == 3
 [baseurl.py]
-skip-if = python == 3
 [basic.py]
-skip-if = python == 3
 [filelisting.py]
 skip-if = python == 3
 [paths.py]
-skip-if = python == 3
 [requestlog.py]
-skip-if = python == 3
diff --git a/testing/mozbase/mozhttpd/tests/paths.py b/testing/mozbase/mozhttpd/tests/paths.py
--- a/testing/mozbase/mozhttpd/tests/paths.py
+++ b/testing/mozbase/mozhttpd/tests/paths.py
@@ -4,71 +4,73 @@
 # http://creativecommons.org/publicdomain/zero/1.0/
 
 from __future__ import absolute_import
 
 from mozfile import TemporaryDirectory
 import mozhttpd
 import os
 import unittest
-import urllib2
+
+from six.moves.urllib.request import urlopen
+from six.moves.urllib.error import HTTPError
 
 import mozunit
 
 
 class PathTest(unittest.TestCase):
 
     def try_get(self, url, expected_contents):
-        f = urllib2.urlopen(url)
+        f = urlopen(url)
         self.assertEqual(f.getcode(), 200)
         self.assertEqual(f.read(), expected_contents)
 
     def try_get_expect_404(self, url):
-        with self.assertRaises(urllib2.HTTPError) as cm:
-            urllib2.urlopen(url)
+        with self.assertRaises(HTTPError) as cm:
+            urlopen(url)
         self.assertEqual(404, cm.exception.code)
 
     def test_basic(self):
         """Test that requests to docroot and a path mapping work as expected."""
         with TemporaryDirectory() as d1, TemporaryDirectory() as d2:
             open(os.path.join(d1, "test1.txt"), "w").write("test 1 contents")
             open(os.path.join(d2, "test2.txt"), "w").write("test 2 contents")
             httpd = mozhttpd.MozHttpd(port=0,
                                       docroot=d1,
                                       path_mappings={'/files': d2}
                                       )
             httpd.start(block=False)
-            self.try_get(httpd.get_url("/test1.txt"), "test 1 contents")
-            self.try_get(httpd.get_url("/files/test2.txt"), "test 2 contents")
+            self.try_get(httpd.get_url("/test1.txt"), b"test 1 contents")
+            self.try_get(httpd.get_url("/files/test2.txt"), b"test 2 contents")
             self.try_get_expect_404(httpd.get_url("/files/test2_nope.txt"))
             httpd.stop()
 
     def test_substring_mappings(self):
         """Test that a path mapping that's a substring of another works."""
         with TemporaryDirectory() as d1, TemporaryDirectory() as d2:
             open(os.path.join(d1, "test1.txt"), "w").write("test 1 contents")
             open(os.path.join(d2, "test2.txt"), "w").write("test 2 contents")
             httpd = mozhttpd.MozHttpd(port=0,
                                       path_mappings={'/abcxyz': d1,
                                                      '/abc': d2, }
                                       )
             httpd.start(block=False)
-            self.try_get(httpd.get_url("/abcxyz/test1.txt"), "test 1 contents")
-            self.try_get(httpd.get_url("/abc/test2.txt"), "test 2 contents")
+            self.try_get(httpd.get_url("/abcxyz/test1.txt"), b"test 1 contents")
+            self.try_get(httpd.get_url("/abc/test2.txt"), b"test 2 contents")
             httpd.stop()
 
     def test_multipart_path_mapping(self):
         """Test that a path mapping with multiple directories works."""
         with TemporaryDirectory() as d1:
             open(os.path.join(d1, "test1.txt"), "w").write("test 1 contents")
             httpd = mozhttpd.MozHttpd(port=0,
                                       path_mappings={'/abc/def/ghi': d1}
                                       )
             httpd.start(block=False)
-            self.try_get(httpd.get_url("/abc/def/ghi/test1.txt"), "test 1 contents")
+            self.try_get(httpd.get_url("/abc/def/ghi/test1.txt"), b"test 1 contents")
             self.try_get_expect_404(httpd.get_url("/abc/test1.txt"))
             self.try_get_expect_404(httpd.get_url("/abc/def/test1.txt"))
             httpd.stop()
 
     def test_no_docroot(self):
         """Test that path mappings with no docroot work."""
         with TemporaryDirectory() as d1:
             httpd = mozhttpd.MozHttpd(port=0,
diff --git a/testing/mozbase/mozhttpd/tests/requestlog.py b/testing/mozbase/mozhttpd/tests/requestlog.py
--- a/testing/mozbase/mozhttpd/tests/requestlog.py
+++ b/testing/mozbase/mozhttpd/tests/requestlog.py
@@ -1,32 +1,33 @@
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this file,
 # You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import
 
 import mozhttpd
-import urllib2
 import os
 import unittest
 
+from six.moves.urllib.request import urlopen
+
 import mozunit
 
 here = os.path.dirname(os.path.abspath(__file__))
 
 
 class RequestLogTest(unittest.TestCase):
 
     def check_logging(self, log_requests=False):
 
         httpd = mozhttpd.MozHttpd(port=0, docroot=here, log_requests=log_requests)
         httpd.start(block=False)
         url = "http://%s:%s/" % ('127.0.0.1', httpd.httpd.server_port)
-        f = urllib2.urlopen(url)
+        f = urlopen(url)
         f.read()
 
         return httpd.request_log
 
     def test_logging_enabled(self):
         request_log = self.check_logging(log_requests=True)
 
         self.assertEqual(len(request_log), 1)

