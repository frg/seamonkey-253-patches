# HG changeset patch
# User M?ris Fogels <mars@mozilla.com>
# Date 1566335958 0
#      Tue Aug 20 21:19:18 2019 +0000
# Node ID d00d447a0c29aa932ab110abd0449f4463ef829a
# Parent  c25790c7cb79405918127d805c3c98d5d1594015
Bug 1210157 - Fix Python 3 issues in mozboot.gentoo and mozboot.archlinux r=firefox-build-system-reviewers,mshal

Fix some library calls and syntax that are required to support Python 3.

Depends on D39364

Differential Revision: https://phabricator.services.mozilla.com/D39365

diff --git a/python/mozboot/mozboot/archlinux.py b/python/mozboot/mozboot/archlinux.py
--- a/python/mozboot/mozboot/archlinux.py
+++ b/python/mozboot/mozboot/archlinux.py
@@ -13,16 +13,22 @@ import glob
 from mozboot.base import BaseBootstrapper
 from mozboot.linux_common import (
     ClangStaticAnalysisInstall,
     NodeInstall,
     SccacheInstall,
     StyloInstall,
 )
 
+# NOTE: This script is intended to be run with a vanilla Python install.  We
+# have to rely on the standard library instead of Python 2+3 helpers like
+# the six module.
+if sys.version_info < (3,):
+    input = raw_input
+
 
 class ArchlinuxBootstrapper(NodeInstall, StyloInstall, SccacheInstall,
                             ClangStaticAnalysisInstall, BaseBootstrapper):
     '''Archlinux experimental bootstrapper.'''
 
     SYSTEM_PACKAGES = [
         'autoconf2.13',
         'base-devel',
@@ -186,17 +192,17 @@ class ArchlinuxBootstrapper(NodeInstall,
 
     def aur_install(self, *packages):
         path = tempfile.mkdtemp()
         if not self.no_interactive:
             print('WARNING! This script requires to install packages from the AUR '
                   'This is potentially unsecure so I recommend that you carefully '
                   'read each package description and check the sources.'
                   'These packages will be built in ' + path + '.')
-            choice = raw_input('Do you want to continue? (yes/no) [no]')
+            choice = input('Do you want to continue? (yes/no) [no]')
             if choice != 'yes':
                 sys.exit(1)
 
         base_dir = os.getcwd()
         os.chdir(path)
         for package in packages:
             name, _, ext = package.split('/')[-1].split('.')
             directory = os.path.join(path, name)
diff --git a/python/mozboot/mozboot/gentoo.py b/python/mozboot/mozboot/gentoo.py
--- a/python/mozboot/mozboot/gentoo.py
+++ b/python/mozboot/mozboot/gentoo.py
@@ -49,17 +49,18 @@ class GentooBootstrapper(NasmInstall, No
     def ensure_browser_packages(self, artifact_mode=False):
         # TODO: Figure out what not to install for artifact mode
         self.run_as_root(['emerge', '--onlydeps', '--quiet', 'firefox'])
         self.run_as_root(['emerge', '--noreplace', '--quiet', 'gtk+'])
 
     @staticmethod
     def _get_distdir():
         # Obtain the path held in the DISTDIR portage variable
-        output = subprocess.check_output(['emerge', '--info'])
+        output = subprocess.check_output(
+            ['emerge', '--info'], universal_newlines=True)
         match = re.search('^DISTDIR="(?P<distdir>.*)"$', output, re.MULTILINE)
         return match.group('distdir')
 
     @staticmethod
     def _get_jdk_filename(emerge_output):
         match = re.search(r'^ \* *(?P<tarball>jdk-.*-linux-.*.tar.gz)$',
                           emerge_output, re.MULTILINE)
 
@@ -93,17 +94,18 @@ class GentooBootstrapper(NasmInstall, No
         # For downloading the Oracle JDK, Android SDK and NDK.
         self.run_as_root(['emerge', '--noreplace', '--quiet', 'wget'])
 
         # Find the JDK file name and URL(s)
         try:
             output = self.check_output(['emerge', '--pretend', '--fetchonly',
                                         'oracle-jdk-bin'],
                                        env=None,
-                                       stderr=subprocess.STDOUT)
+                                       stderr=subprocess.STDOUT,
+                                       universal_newlines=True)
         except subprocess.CalledProcessError as e:
             output = e.output
 
         jdk_filename = self._get_jdk_filename(output)
         jdk_page_urls = self._get_jdk_page_urls(output)
         jdk_url = self._get_jdk_url(jdk_filename, jdk_page_urls)
 
         # Fetch the Oracle JDK since portage can't fetch it on its own
