# HG changeset patch
# User Ricky Stewart <rstewart@mozilla.com>
# Date 1574944042 0
# Node ID 1ea6d897b8681285278db919dee4ab476c7b6247
# Parent  e174d87574ef5a0f75000b788b8bf20135c5e284
Bug 1599235 - Download lucetc during bootstrap for Linux r=firefox-build-system-reviewers,mshal

Differential Revision: https://phabricator.services.mozilla.com/D54835

diff --git a/python/mozboot/mozboot/archlinux.py b/python/mozboot/mozboot/archlinux.py
--- a/python/mozboot/mozboot/archlinux.py
+++ b/python/mozboot/mozboot/archlinux.py
@@ -8,30 +8,32 @@ import os
 import sys
 import tempfile
 import subprocess
 import glob
 
 from mozboot.base import BaseBootstrapper
 from mozboot.linux_common import (
     ClangStaticAnalysisInstall,
+    LucetcInstall,
     NodeInstall,
     SccacheInstall,
     StyloInstall,
 )
 
 # NOTE: This script is intended to be run with a vanilla Python install.  We
 # have to rely on the standard library instead of Python 2+3 helpers like
 # the six module.
 if sys.version_info < (3,):
     input = raw_input  # noqa
 
 
-class ArchlinuxBootstrapper(NodeInstall, StyloInstall, SccacheInstall,
-                            ClangStaticAnalysisInstall, BaseBootstrapper):
+class ArchlinuxBootstrapper(
+        NodeInstall, StyloInstall, SccacheInstall, ClangStaticAnalysisInstall,
+        LucetcInstall, BaseBootstrapper):
     '''Archlinux experimental bootstrapper.'''
 
     SYSTEM_PACKAGES = [
         'autoconf2.13',
         'base-devel',
         'nodejs',
         'python2',
         'python2-setuptools',
diff --git a/python/mozboot/mozboot/base.py b/python/mozboot/mozboot/base.py
--- a/python/mozboot/mozboot/base.py
+++ b/python/mozboot/mozboot/base.py
@@ -290,16 +290,22 @@ class BaseBootstrapper(object):
             % __name__)
 
     def ensure_sccache_packages(self, state_dir, checkout_root):
         '''
         Install sccache.
         '''
         pass
 
+    def ensure_lucetc_packages(self, state_dir, checkout_root):
+        '''
+        Install lucetc.
+        '''
+        pass
+
     def ensure_node_packages(self, state_dir, checkout_root):
         '''
         Install any necessary packages needed to supply NodeJS'''
         raise NotImplementedError(
             '%s does not yet implement ensure_node_packages()'
             % __name__)
 
     def install_toolchain_static_analysis(self, state_dir, checkout_root, toolchain_job):
diff --git a/python/mozboot/mozboot/bootstrap.py b/python/mozboot/mozboot/bootstrap.py
--- a/python/mozboot/mozboot/bootstrap.py
+++ b/python/mozboot/mozboot/bootstrap.py
@@ -352,16 +352,17 @@ class Bootstrapper(object):
             sys.exit(1)
 
         self.instance.state_dir = state_dir
         self.instance.ensure_node_packages(state_dir, checkout_root)
         self.instance.ensure_stylo_packages(state_dir, checkout_root)
         self.instance.ensure_clang_static_analysis_package(state_dir, checkout_root)
         self.instance.ensure_nasm_packages(state_dir, checkout_root)
         self.instance.ensure_sccache_packages(state_dir, checkout_root)
+        self.instance.ensure_lucetc_packages(state_dir, checkout_root)
 
     def bootstrap(self):
         if self.choice is None:
             # Like ['1. Firefox for Desktop', '2. Firefox for Android Artifact Mode', ...].
             labels = ['%s. %s' % (i + 1, name) for (i, (name, _)) in enumerate(APPLICATIONS_LIST)]
             prompt = APPLICATION_CHOICE % '\n'.join('  {}'.format(label) for label in labels)
             prompt_choice = self.instance.prompt_int(prompt=prompt, low=1, high=len(APPLICATIONS))
             name, application = APPLICATIONS_LIST[prompt_choice-1]
diff --git a/python/mozboot/mozboot/centosfedora.py b/python/mozboot/mozboot/centosfedora.py
--- a/python/mozboot/mozboot/centosfedora.py
+++ b/python/mozboot/mozboot/centosfedora.py
@@ -4,26 +4,27 @@
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 import platform
 
 from mozboot.base import BaseBootstrapper
 from mozboot.linux_common import (
     ClangStaticAnalysisInstall,
+    LucetcInstall,
     NasmInstall,
     NodeInstall,
     SccacheInstall,
     StyloInstall,
 )
 
 
 class CentOSFedoraBootstrapper(NasmInstall, NodeInstall, StyloInstall,
                                SccacheInstall, ClangStaticAnalysisInstall,
-                               BaseBootstrapper):
+                               LucetcInstall, BaseBootstrapper):
     def __init__(self, distro, version, dist_id, **kwargs):
         BaseBootstrapper.__init__(self, **kwargs)
 
         self.distro = distro
         self.version = int(version.split('.')[0])
         self.dist_id = dist_id
 
         self.group_packages = []
diff --git a/python/mozboot/mozboot/debian.py b/python/mozboot/mozboot/debian.py
--- a/python/mozboot/mozboot/debian.py
+++ b/python/mozboot/mozboot/debian.py
@@ -2,16 +2,17 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 from mozboot.base import BaseBootstrapper
 from mozboot.linux_common import (
     ClangStaticAnalysisInstall,
+    LucetcInstall,
     NasmInstall,
     NodeInstall,
     SccacheInstall,
     StyloInstall,
 )
 
 
 MERCURIAL_INSTALL_PROMPT = '''
@@ -27,17 +28,17 @@ in files being placed in /usr/local/bin 
 How would you like to continue?
   1. Install a modern Mercurial via pip (recommended)
   2. Install a legacy Mercurial via apt
   3. Do not install Mercurial
 Your choice: '''
 
 
 class DebianBootstrapper(NasmInstall, NodeInstall, StyloInstall, ClangStaticAnalysisInstall,
-                         SccacheInstall, BaseBootstrapper):
+                         SccacheInstall, LucetcInstall, BaseBootstrapper):
     # These are common packages for all Debian-derived distros (such as
     # Ubuntu).
     COMMON_PACKAGES = [
         'autoconf2.13',
         'build-essential',
         'nodejs',
         'python-dev',
         'python-pip',
diff --git a/python/mozboot/mozboot/gentoo.py b/python/mozboot/mozboot/gentoo.py
--- a/python/mozboot/mozboot/gentoo.py
+++ b/python/mozboot/mozboot/gentoo.py
@@ -2,33 +2,35 @@
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
 from __future__ import absolute_import, print_function, unicode_literals
 
 from mozboot.base import BaseBootstrapper
 from mozboot.linux_common import (
     ClangStaticAnalysisInstall,
+    LucetcInstall,
     NasmInstall,
     NodeInstall,
     SccacheInstall,
     StyloInstall,
 )
 
 try:
     from urllib2 import urlopen
 except ImportError:
     from urllib.request import urlopen
 
 import re
 import subprocess
 
 
-class GentooBootstrapper(NasmInstall, NodeInstall, StyloInstall, ClangStaticAnalysisInstall,
-                         SccacheInstall, BaseBootstrapper):
+class GentooBootstrapper(
+        NasmInstall, NodeInstall, StyloInstall, ClangStaticAnalysisInstall,
+        SccacheInstall, LucetcInstall, BaseBootstrapper):
 
     def __init__(self, version, dist_id, **kwargs):
         BaseBootstrapper.__init__(self, **kwargs)
 
         self.version = version
         self.dist_id = dist_id
 
     def install_system_packages(self):
diff --git a/python/mozboot/mozboot/linux_common.py b/python/mozboot/mozboot/linux_common.py
--- a/python/mozboot/mozboot/linux_common.py
+++ b/python/mozboot/mozboot/linux_common.py
@@ -20,16 +20,27 @@ class SccacheInstall(object):
         pass
 
     def ensure_sccache_packages(self, state_dir, checkout_root):
         from mozboot import sccache
 
         self.install_toolchain_artifact(state_dir, checkout_root, sccache.LINUX_SCCACHE)
 
 
+class LucetcInstall(object):
+    def __init__(self, **kwargs):
+        pass
+
+    def ensure_lucetc_packages(self, state_dir, checkout_root):
+        from mozboot import lucetc
+
+        self.install_toolchain_artifact(state_dir, checkout_root,
+                                        lucetc.LINUX_LUCETC)
+
+
 class StyloInstall(object):
     def __init__(self, **kwargs):
         pass
 
     def ensure_stylo_packages(self, state_dir, checkout_root):
         from mozboot import stylo
 
         if is_non_x86_64():
diff --git a/python/mozboot/mozboot/lucetc.py b/python/mozboot/mozboot/lucetc.py
new file mode 100644
--- /dev/null
+++ b/python/mozboot/mozboot/lucetc.py
@@ -0,0 +1,7 @@
+# This Source Code Form is subject to the terms of the Mozilla Public
+# License, v. 2.0. If a copy of the MPL was not distributed with this
+# file, You can obtain one at http://mozilla.org/MPL/2.0/.
+
+from __future__ import absolute_import, print_function, unicode_literals
+
+LINUX_LUCETC = 'lucetc'
diff --git a/python/mozboot/mozboot/solus.py b/python/mozboot/mozboot/solus.py
--- a/python/mozboot/mozboot/solus.py
+++ b/python/mozboot/mozboot/solus.py
@@ -5,30 +5,32 @@
 from __future__ import absolute_import, print_function, unicode_literals
 
 import sys
 import subprocess
 
 from mozboot.base import BaseBootstrapper
 from mozboot.linux_common import (
     ClangStaticAnalysisInstall,
+    LucetcInstall,
     NodeInstall,
     SccacheInstall,
     StyloInstall,
 )
 
 # NOTE: This script is intended to be run with a vanilla Python install.  We
 # have to rely on the standard library instead of Python 2+3 helpers like
 # the six module.
 if sys.version_info < (3,):
     input = raw_input  # noqa
 
 
-class SolusBootstrapper(NodeInstall, StyloInstall, SccacheInstall,
-                        ClangStaticAnalysisInstall, BaseBootstrapper):
+class SolusBootstrapper(
+        NodeInstall, StyloInstall, SccacheInstall, ClangStaticAnalysisInstall,
+        LucetcInstall, BaseBootstrapper):
     '''Solus experimental bootstrapper.'''
 
     SYSTEM_PACKAGES = [
         'autoconf213',
         'nodejs',
         'python',
         'python3',
         'unzip',
