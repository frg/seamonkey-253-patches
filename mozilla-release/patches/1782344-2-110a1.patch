# HG changeset patch
# User Mike Hommey <mh+mozilla@glandium.org>
# Date 1673570876 0
# Node ID 416d4ed96953b8d7220a0064e6b3893edd21e565
# Parent  3fe3b8251daaf32b3414c1d940cf5161caac67ea
Bug 1782344 - Remove cache directory and configs from CCacheStats. r=firefox-build-system-reviewers,ahochheiden

The info is unused and not part of the ccache --print-stats output that
we're going to use shortly.

Differential Revision: https://phabricator.services.mozilla.com/D166631

diff --git a/python/mozbuild/mozbuild/controller/building.py b/python/mozbuild/mozbuild/controller/building.py
--- a/python/mozbuild/mozbuild/controller/building.py
+++ b/python/mozbuild/mozbuild/controller/building.py
@@ -821,19 +821,21 @@ class CCacheStats(object):
         ('no_input', 'no input file'),
         ('bad_extra_file', 'error hashing extra file'),
         ('num_cleanups', 'cleanups performed'),
         ('cache_files', 'files in cache'),
         ('cache_size', 'cache size'),
         ('cache_max_size', 'max cache size'),
     ]
 
-    DIRECTORY_DESCRIPTION = "cache directory"
-    PRIMARY_CONFIG_DESCRIPTION = "primary config"
-    SECONDARY_CONFIG_DESCRIPTION = "secondary config"
+    SKIP_LINES = (
+        "cache directory",
+        "primary config",
+        "secondary config",
+    )
 
     STATS_KEYS_4_4 = [
         ("stats_updated", "Summary/Stats updated"),
         (
             "cache_hit_rate",
             "Summary/Hits",
             lambda x: next(iter(re.findall(r"\((.*) %\)", x)), "0.00 %"),
         ),
@@ -882,41 +884,37 @@ class CCacheStats(object):
             lambda x: str(
                 float(next(iter(re.findall(r".*\s+/\s+(.*)\s+\(", x)), "0"))
                 * CCacheStats.GiB
             ),
         ),
     ]
 
     SKIP_KEYS_4_4 = [
+        "Summary/Cache directory",
+        "Summary/Primary config",
+        "Summary/Secondary config",
         "Summary/Uncacheable",
         "Summary/Misses/Direct",
         "Summary/Misses/Preprocessed",
         "Primary storage/Hits",
         "Primary storage/Misses",
         "Errors/Could not find compiler",
     ]
 
-    DIRECTORY_DESCRIPTION_4_4 = "Summary/Cache directory"
-    PRIMARY_CONFIG_DESCRIPTION_4_4 = "Summary/Primary config"
-    SECONDARY_CONFIG_DESCRIPTION_4_4 = "Summary/Secondary config"
-
     ABSOLUTE_KEYS = {'cache_files', 'cache_size', 'cache_max_size'}
     FORMAT_KEYS = {'cache_size', 'cache_max_size'}
 
     GiB = 1024 ** 3
     MiB = 1024 ** 2
     KiB = 1024
 
     def __init__(self, output=None, is_version_4_4_or_newer=False):
         """Construct an instance from the output of ccache -s."""
         self._values = {}
-        self.cache_dir = ""
-        self.primary_config = ""
-        self.secondary_config = ""
 
         if not output:
             return
 
         if is_version_4_4_or_newer:
             self._parse_human_format_4_4_plus(output)
         else:
             self._parse_human_format(output)
@@ -940,68 +938,50 @@ class CCacheStats(object):
                         key += "/{}".format(subhead)
                     key += "/{}".format(name)
                     self._parse_line_4_4_plus(key, raw_value)
             else:
                 head = line.strip(":")
                 subhead = ""
 
     def _parse_line_4_4_plus(self, key, value):
-        if key.startswith(self.DIRECTORY_DESCRIPTION_4_4):
-            self.cache_dir = value
-        elif key.startswith(self.PRIMARY_CONFIG_DESCRIPTION_4_4):
-            self.primary_config = value
-        elif key.startswith(self.SECONDARY_CONFIG_DESCRIPTION_4_4):
-            self.secondary_config = value
+        for seq in self.STATS_KEYS_4_4:
+            stat_key = seq[0]
+            stat_description = seq[1]
+            raw_value = value
+            if len(seq) > 2:
+                raw_value = seq[2](value)
+            if stat_key not in self._values and key == stat_description:
+                self._values[stat_key] = self._parse_value(raw_value)
+
+                # We dont want to break when we need to extract two infos
+                # from the same line
+                if len(seq) < 4:
+                    break
         else:
-            for seq in self.STATS_KEYS_4_4:
-                stat_key = seq[0]
-                stat_description = seq[1]
-                raw_value = value
-                if len(seq) > 2:
-                    raw_value = seq[2](value)
-                if stat_key not in self._values and key == stat_description:
-                    self._values[stat_key] = self._parse_value(raw_value)
-
-                    # We dont want to break when we need to extract two infos
-                    # from the same line
-                    if len(seq) < 4:
-                        break
-            else:
-                if key not in self.SKIP_KEYS_4_4:
-                    raise ValueError(
-                        "Failed to parse ccache stats output: '{}' '{}'".format(
-                            key, value
-                        )
-                    )
+            if key not in self.SKIP_KEYS_4_4:
+                raise ValueError(
+                    "Failed to parse ccache stats output: '{}' '{}'".format(key, value)
+                )
 
     def _parse_human_format(self, output):
         for line in output.splitlines():
             line = line.strip()
             if line:
                 self._parse_line(line)
 
     def _parse_line(self, line):
         line = six.ensure_text(line)
-        if line.startswith(self.DIRECTORY_DESCRIPTION):
-            self.cache_dir = self._strip_prefix(line, self.DIRECTORY_DESCRIPTION)
-        elif line.startswith(self.PRIMARY_CONFIG_DESCRIPTION):
-            self.primary_config = self._strip_prefix(
-                line, self.PRIMARY_CONFIG_DESCRIPTION)
-        elif line.startswith(self.SECONDARY_CONFIG_DESCRIPTION):
-            self.secondary_config = self._strip_prefix(
-                self._strip_prefix(line, self.SECONDARY_CONFIG_DESCRIPTION),
-                "(readonly)")
+        for stat_key, stat_description in self.STATS_KEYS:
+            if line.startswith(stat_description):
+                raw_value = self._strip_prefix(line, stat_description)
+                self._values[stat_key] = self._parse_value(raw_value)
+                break
         else:
-            for stat_key, stat_description in self.STATS_KEYS:
-                if line.startswith(stat_description):
-                    raw_value = self._strip_prefix(line, stat_description)
-                    self._values[stat_key] = self._parse_value(raw_value)
-                    break
-            else:
+            if not line.startswith(self.SKIP_LINES):
                 raise ValueError('Failed to parse ccache stats output: %s' % line)
 
     @staticmethod
     def _strip_prefix(line, prefix):
         return line[len(prefix):].strip() if line.startswith(prefix) else line
 
     @staticmethod
     def _parse_value(raw_value):
@@ -1055,17 +1035,16 @@ class CCacheStats(object):
             direct /= total
             preprocessed /= total
             miss /= total
 
         return (direct, preprocessed, miss)
 
     def __sub__(self, other):
         result = CCacheStats()
-        result.cache_dir = self.cache_dir
 
         for k, prefix in self.STATS_KEYS:
             if k not in self._values and k not in other._values:
                 continue
 
             our_value = self._values.get(k, 0)
             other_value = other._values.get(k, 0)
 
@@ -1075,20 +1054,16 @@ class CCacheStats(object):
                 result._values[k] = our_value - other_value
 
         return result
 
     def __str__(self):
         LEFT_ALIGN = 34
         lines = []
 
-        if self.cache_dir:
-            lines.append('%s%s' % (self.DIRECTORY_DESCRIPTION.ljust(LEFT_ALIGN),
-                                   self.cache_dir))
-
         for stat_key, stat_description in self.STATS_KEYS:
             if stat_key not in self._values:
                 continue
 
             value = self._values[stat_key]
 
             if stat_key in self.FORMAT_KEYS:
                 value = '%15s' % self._format_value(value)
diff --git a/python/mozbuild/mozbuild/test/controller/test_ccachestats.py b/python/mozbuild/mozbuild/test/controller/test_ccachestats.py
--- a/python/mozbuild/mozbuild/test/controller/test_ccachestats.py
+++ b/python/mozbuild/mozbuild/test/controller/test_ccachestats.py
@@ -402,17 +402,16 @@ Uncacheable:
 
     maxDiff = None
 
     def test_parse_garbage_stats_message(self):
         self.assertRaises(ValueError, CCacheStats, self.STAT_GARBAGE)
 
     def test_parse_zero_stats_message(self):
         stats = CCacheStats(self.STAT0)
-        self.assertEqual(stats.cache_dir, "/home/tlin/.ccache")
         self.assertEqual(stats.hit_rates(), (0, 0, 0))
 
     def test_hit_rate_of_diff_stats(self):
         stats1 = CCacheStats(self.STAT1)
         stats2 = CCacheStats(self.STAT2)
         stats_diff = stats2 - stats1
         self.assertEqual(stats_diff.hit_rates(), (0.9, 0.05, 0.05))
 
@@ -432,17 +431,16 @@ Uncacheable:
         self.assertFalse(stats_diff_negative2)
 
     def test_stats_version32(self):
         stat2 = CCacheStats(self.STAT2)
         stat3 = CCacheStats(self.STAT3)
         stats_diff = stat3 - stat2
         self.assertEqual(
             str(stat3),
-            "cache directory                   /Users/tlin/.ccache\n"
             "cache hit (direct)                   12004\n"
             "cache hit (preprocessed)              1786\n"
             "cache miss                           26348\n"
             "called for link                       2338\n"
             "called for preprocessing              6313\n"
             "compile failed                         399\n"
             "preprocessor error                     390\n"
             "bad compiler arguments                  86\n"
@@ -451,17 +449,16 @@ Uncacheable:
             "unsupported compiler option            187\n"
             "no input file                         1068\n"
             "files in cache                       18044\n"
             "cache size                             7.5 Gbytes\n"
             "max cache size                         8.6 Gbytes",
         )
         self.assertEqual(
             str(stats_diff),
-            "cache directory                   /Users/tlin/.ccache\n"
             "cache hit (direct)                   10104\n"
             "cache hit (preprocessed)              1486\n"
             "cache miss                           23748\n"
             "called for link                       1977\n"
             "called for preprocessing              6301\n"
             "compile failed                         377\n"
             "preprocessor error                     384\n"
             "bad compiler arguments                  74\n"
@@ -475,17 +472,16 @@ Uncacheable:
         )
 
     def test_cache_size_shrinking(self):
         stat4 = CCacheStats(self.STAT4)
         stat5 = CCacheStats(self.STAT5)
         stats_diff = stat5 - stat4
         self.assertEqual(
             str(stat4),
-            "cache directory                   /Users/tlin/.ccache\n"
             "cache hit (direct)                   21039\n"
             "cache hit (preprocessed)              2315\n"
             "cache miss                           39370\n"
             "called for link                       3651\n"
             "called for preprocessing              6693\n"
             "compile failed                         723\n"
             "ccache internal error                    1\n"
             "preprocessor error                     588\n"
@@ -495,17 +491,16 @@ Uncacheable:
             "unsupported compiler option            187\n"
             "no input file                         1711\n"
             "files in cache                       18313\n"
             "cache size                             6.3 Gbytes\n"
             "max cache size                         6.0 Gbytes",
         )
         self.assertEqual(
             str(stat5),
-            "cache directory                   /Users/tlin/.ccache\n"
             "cache hit (direct)                   21039\n"
             "cache hit (preprocessed)              2315\n"
             "cache miss                           39372\n"
             "called for link                       3653\n"
             "called for preprocessing              6693\n"
             "compile failed                         723\n"
             "ccache internal error                    1\n"
             "preprocessor error                     588\n"
@@ -515,17 +510,16 @@ Uncacheable:
             "unsupported compiler option            187\n"
             "no input file                         1711\n"
             "files in cache                       17411\n"
             "cache size                             6.0 Gbytes\n"
             "max cache size                         6.0 Gbytes",
         )
         self.assertEqual(
             str(stats_diff),
-            "cache directory                   /Users/tlin/.ccache\n"
             "cache hit (direct)                       0\n"
             "cache hit (preprocessed)                 0\n"
             "cache miss                               2\n"
             "called for link                          2\n"
             "called for preprocessing                 0\n"
             "compile failed                           0\n"
             "ccache internal error                    0\n"
             "preprocessor error                       0\n"
@@ -541,17 +535,16 @@ Uncacheable:
 
     def test_stats_version33(self):
         # Test stats for 3.3.2.
         stat3 = CCacheStats(self.STAT3)
         stat6 = CCacheStats(self.STAT6)
         stats_diff = stat6 - stat3
         self.assertEqual(
             str(stat6),
-            "cache directory                   /Users/tlin/.ccache\n"
             "cache hit (direct)                  319287\n"
             "cache hit (preprocessed)            125987\n"
             "cache hit rate                          37\n"
             "cache miss                          749959\n"
             "called for link                      87978\n"
             "called for preprocessing            418591\n"
             "multiple source files                 1861\n"
             "compiler produced no output            122\n"
@@ -567,17 +560,16 @@ Uncacheable:
             "no input file                       309538\n"
             "cleanups performed                       1\n"
             "files in cache                       17358\n"
             "cache size                            15.4 Gbytes\n"
             "max cache size                        17.2 Gbytes",
         )
         self.assertEqual(
             str(stat3),
-            "cache directory                   /Users/tlin/.ccache\n"
             "cache hit (direct)                   12004\n"
             "cache hit (preprocessed)              1786\n"
             "cache miss                           26348\n"
             "called for link                       2338\n"
             "called for preprocessing              6313\n"
             "compile failed                         399\n"
             "preprocessor error                     390\n"
             "bad compiler arguments                  86\n"
@@ -586,17 +578,16 @@ Uncacheable:
             "unsupported compiler option            187\n"
             "no input file                         1068\n"
             "files in cache                       18044\n"
             "cache size                             7.5 Gbytes\n"
             "max cache size                         8.6 Gbytes",
         )
         self.assertEqual(
             str(stats_diff),
-            "cache directory                   /Users/tlin/.ccache\n"
             "cache hit (direct)                  307283\n"
             "cache hit (preprocessed)            124201\n"
             "cache hit rate                          37\n"
             "cache miss                          723611\n"
             "called for link                      85640\n"
             "called for preprocessing            412278\n"
             "multiple source files                 1861\n"
             "compiler produced no output            122\n"
@@ -615,17 +606,16 @@ Uncacheable:
             "cache size                            15.4 Gbytes\n"
             "max cache size                        17.2 Gbytes",
         )
 
         # Test stats for 3.3.3.
         stat7 = CCacheStats(self.STAT7)
         self.assertEqual(
             str(stat7),
-            "cache directory                   /Users/tlin/.ccache\n"
             "cache hit (direct)                   27035\n"
             "cache hit (preprocessed)             13939\n"
             "cache hit rate                          39\n"
             "cache miss                           62630\n"
             "called for link                       1280\n"
             "called for preprocessing               736\n"
             "compile failed                         550\n"
             "preprocessor error                     638\n"
@@ -639,17 +629,16 @@ Uncacheable:
             "max cache size                         5.0 Gbytes",
         )
 
     def test_stats_version34(self):
         # Test parsing 3.4 output.
         stat8 = CCacheStats(self.STAT8)
         self.assertEqual(
             str(stat8),
-            "cache directory                   /home/psimonyi/.ccache\n"
             f"stats zero time                   {int(TIMESTAMP)}\n"
             f"stats zeroed                      {int(TIMESTAMP)}\n"
             "cache hit (direct)                     571\n"
             "cache hit (preprocessed)              1203\n"
             "cache hit rate                          13\n"
             "cache miss                           11747\n"
             "called for link                        623\n"
             "called for preprocessing              7194\n"
@@ -664,17 +653,16 @@ Uncacheable:
             "max cache size                         7.0 Gbytes",
         )
 
     def test_stats_version35(self):
         # Test parsing 3.5 output.
         stat9 = CCacheStats(self.STAT9)
         self.assertEqual(
             str(stat9),
-            "cache directory                   /Users/tlin/.ccache\n"
             f"stats zero time                   {int(TIMESTAMP)}\n"
             f"stats zeroed                      {int(TIMESTAMP)}\n"
             f"stats updated                     {int(TIMESTAMP2)}\n"
             "cache hit (direct)                   80147\n"
             "cache hit (preprocessed)             21413\n"
             "cache hit rate                          34\n"
             "cache miss                          191128\n"
             "called for link                       5194\n"
@@ -699,17 +687,16 @@ Uncacheable:
         self.assertTrue(CCacheStats._is_version_4_4_or_newer(self.VERSION_4_4))
         self.assertTrue(CCacheStats._is_version_4_4_or_newer(self.VERSION_4_4_2))
         self.assertTrue(CCacheStats._is_version_4_4_or_newer(self.VERSION_4_5))
 
         # Test parsing 4.4+ output.
         stat10 = CCacheStats(self.STAT10, True)
         self.assertEqual(
             str(stat10),
-            "cache directory                   /home/suer/.ccache\n"
             f"stats updated                     {int(TIMESTAMP)}\n"
             "cache hit (direct)                     197\n"
             "cache hit (preprocessed)               719\n"
             "cache hit rate                           9\n"
             "cache miss                            8427\n"
             "called for preprocessing               110\n"
             "compile failed                          49\n"
             "ccache internal error                    1\n"
@@ -723,47 +710,44 @@ Uncacheable:
             "files in cache                        4425\n"
             "cache size                             4.4 Gbytes\n"
             "max cache size                         5.0 Gbytes",
         )
 
         stat11 = CCacheStats(self.STAT11, True)
         self.assertEqual(
             str(stat11),
-            "cache directory                   /home/suer/.ccache\n"
             f"stats updated                     {int(TIMESTAMP)}\n"
             "cache hit (direct)                       0\n"
             "cache hit (preprocessed)                 0\n"
             "cache hit rate                           0\n"
             "cache miss                               0\n"
             "cleanups performed                      16\n"
             "files in cache                           0\n"
             "cache size                             0.0 Kbytes\n"
             "max cache size                         5.0 Gbytes",
         )
 
         stat12 = CCacheStats(self.STAT12, True)
         self.assertEqual(
             str(stat12),
-            "cache directory                   /home/suer/.ccache\n"
             "stats updated                            0\n"
             "cache hit (direct)                       0\n"
             "cache hit (preprocessed)                 0\n"
             "cache hit rate                           0\n"
             "cache miss                               0\n"
             "cleanups performed                      16\n"
             "files in cache                           0\n"
             "cache size                             0.0 Kbytes\n"
             "max cache size                         5.0 Gbytes",
         )
 
         stat13 = CCacheStats(self.STAT13, True)
         self.assertEqual(
             str(stat13),
-            "cache directory                   /Users/leebc/.ccache\n"
             f"stats updated                     {int(TIMESTAMP)}\n"
             "cache hit (direct)                  280542\n"
             "cache hit (preprocessed)                 0\n"
             "cache hit rate                          41\n"
             "cache miss                          387653\n"
             "compile failed                        1665\n"
             "ccache internal error                    1\n"
             "no input file                            2\n"
