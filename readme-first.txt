This is the patch queue repo for our SeaMonkey 2.53.xx prerelease base directory. It is used for building the next official beta or final version. The mercurial queues and patches in them are subject to change at any time because we are frequently porting back new stuff and now and then fix errors and omissions in the backported patches too.

If you want to build a previously released final or beta 2.53.x release please check out the latest source from our gitlab repositories instead. Instructions for this can be found in the release directoy in the file https://archive.mozilla.org/pub/seamonkey/releases/2.53.<version>/README.txt (replace <version> with the desired version).

The patches here are basically useless without checking out the base sources. We provide a copy of the base sources needed at foss.hepoapod.net. To check out the latest source compatible with the queues in here use the following commands:

hg clone https://foss.heptapod.net/seamonkey/mozilla-release comm-253
cd comm-253
hg clone https://foss.heptapod.net/seamonkey/comm-release comm

Afterwards you need to copy the patches in the comm-release/patches dir to .hg/patches in your comm directory. The mozilla-release/patches files go to the .hg/patches directory in your top source comm-253 directory.

To apply them use 'hg qpush --all --encodingmode replace' in both directories. You need the mercurial queues extension for this to work. You will not be able to build successfully without applying them first.

The documentation for the extensions is located here: https://www.mercurial-scm.org/wiki/MqExtension

While use is discouraged it is still the best and flexible way to apply and maintain a larger patch queue for stuff.

The l10n-release directory contains patches which you can apply against a clone of https://foss.heptapod.net/seamonkey/l10n-release. These are only needed if you want to build a language specific version.

If you want to help out with backporting and submit changes for inclusion please contact us at ircs://libera.chat/SeaMonkey . For new functionality and error fixes in SeaMonkey itself please file a bug in https://bugzilla.mozilla.org/ under product SeaMonkey, attach your patch and ask for review or feedback.

Some Naming conventions:

TOP- Patches which need to be applied at the end. This is usual stuff which is a stopgap solution only and will be replaced later.
PPPPPPP- Private patches for the prerelease versions only.
WIP- Work in progress patches for testing. Might have minor problems and will not be included in the official releases.

Patches with a 9999999 in the name still need to be filed in bugzilla and will change then later using the correct bug number. If not they will not be included in an official release.

If this all sounds messy then yes it is :) We would rather not backport and just use the current mozilla repositories but given how much was changed and how much functionality was removed we are still playing catch up there. Rest assured every change in the comm/suite directory whch applies will be put into the bleeding edge comm-central repo when reviewed.
