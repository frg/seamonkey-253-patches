cd ~/release/l10n-56/

omess="Bug 1684772 - move locale files from editor/ui/chrome to suite/chrome/editor for yz r=me"
opatch="1684772-l10n-yz-2537.patch"
for D in cs de el en-GB es-AR es-ES fi fr hu it ja ja-JP-mac ka nb-NO nl pl pt-BR pt-PT ru sk sv-SE zh-CN zh-TW
do
  cd ${D}
  mkdir -p suite/chrome/editor/prefs
  hg mv editor/ui/chrome/composer/editingOverlay.dtd suite/chrome/editor/
  hg mv editor/ui/chrome/composer/editor.dtd suite/chrome/editor/
  hg mv editor/ui/chrome/composer/editor.properties suite/chrome/editor/
  hg mv editor/ui/chrome/composer/editorOverlay.dtd suite/chrome/editor/
  hg mv editor/ui/chrome/composer/editorSmileyOverlay.dtd suite/chrome/editor/
  hg mv editor/ui/chrome/composer/editorPrefsOverlay.dtd suite/chrome/editor/prefs/
  hg mv editor/ui/chrome/composer/pref-composer.dtd suite/chrome/editor/prefs/
  hg mv editor/ui/chrome/composer/pref-editing.dtd suite/chrome/editor/prefs/
  hg mv editor/ui/chrome/dialogs/ suite/chrome/editor/
  patchname=${opatch/yz/$D}
  message=${omess/yz/$D}
  hg qnew -m "${message}" -f "${patchname}"
  cd ..
done
