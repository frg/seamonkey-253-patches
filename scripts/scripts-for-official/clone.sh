# Base directory
# pushd /e/builds/253
# git config --global user.email "xxxxxxx@xxxxnet"
# git config --global user.name "xxxxxxxxxxxxxxxxxx"

# Clone:
# add git path C:\Program Files\Git\cmd; to start-shell.bat under Windows if not there yet.
git.exe clone https://gitlab.com/seamonkey-project/seamonkey-2.53-mozilla.git comm-release
git.exe clone https://gitlab.com/seamonkey-project/seamonkey-2.53-comm.git comm-release/comm
git.exe clone https://gitlab.com/seamonkey-project/seamonkey-2.53-l10n.git l10n
pushd comm-release
git checkout 2_53_20_final
pushd comm
git checkout 2_53_20_final
popd
popd
pushd l10n
git checkout 2_53_20_final
popd

# New branch
# pushd /e/builds/beta-253/comm-release
# git.exe checkout -b foo-tmp-branch
# popd

# return from base directoy to current.
# popd
