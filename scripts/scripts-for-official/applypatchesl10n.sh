#!/bin/bash
# ./applypatchesl10n.sh /e/builds/beta-253/253-gitlab/l10n-release/patches
pushd /e/builds/beta-253/l10n-release
export IFS=

git checkout master

while read -r line || [[ -n "$line" ]]; do
  echo "apply patch: $line"
  patchfile=$1/$(echo $line)
  # if [[ "$line" != *-253171.patch ]] ;
  if [[ "$line" != *.patch ]] ;
  then
    echo "patch $line skipped"
  else
    if [ -f "$patchfile" ]; then
      echo "$patchfile exist"
      # git apply --check "$patchfile"
      # git apply --index --whitespace=nowarn "$patchfile"
      # git apply --stat "$patchfile"
      git am --patch-format=hg --whitespace=nowarn < "$patchfile"
      if [ $? -eq 0 ]
      then
        echo "ok"
      else
        echo "error"
        git am --abort
        exit 1
      fi
    fi
  fi 
 
done < "$1"/series
popd
