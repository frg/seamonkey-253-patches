#!/bin/bash
# ./applypatchesmr.sh /e/builds/beta-253/253-gitlab/mozilla-release/patches
cd /e/builds/beta-253/comm-release
export IFS=

git checkout 2_53_20_final
# git checkout 2_53_20_beta_01
# git checkout master

while read -r line || [[ -n "$line" ]]; do
  echo "apply patch: $line"
  patchfile=$1/$(echo $line)
  if [ "$line" = "1602257-gitignore-253.patch" ] ||
     [ "$line" = "1602257-2-gitignore-mr-2537.patch" ] ||
     [ "$line" = "1421012-59a1.patch" ] ||
     [ "$line" = "1635491-1only-killtup-PARTIAL-78a1.patch" ] ||
     [ "$line" = "1602261-retsulcksat.patch" ] ||
     [ "$line" = "1669850-killmobile-2535.patch" ] ||
     [ "$line" = "1464834-1-killservo-62a1.patch" ] ||
     [ "$line" = "1464834-1a-killservo-62a1.patch" ] ||
     [ "$line" = "1464834-2-killservo-62a1.patch" ] ||
     [ "$line" = "TOP-NOBUG-2-remove-web-platform-25320.patch" ] ||
     [[ "$line" == PPPP* ]] || [[ "$line" == WIP* ]] ||
     [[ "$line" == 9999999* ]] ;
  then
    echo "patch $line skipped"
  else
    if [ -f "$patchfile" ]; then
      echo "$patchfile exist"
      # git apply --check "$patchfile"
      # git apply --index --whitespace=nowarn "$patchfile"
      # git apply --stat "$patchfile"
      git am --patch-format=hg --whitespace=nowarn < "$patchfile"
      if [ $? -eq 0 ]
      then
        echo "ok"
      else
        echo "error"
        git am --abort
        exit 1
      fi
    fi
  fi 
 
done < "$1"/series
cd ..
