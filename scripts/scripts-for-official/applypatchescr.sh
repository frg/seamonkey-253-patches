#!/bin/bash
# ./applypatchescr.sh /e/builds/beta-253/253-gitlab/comm-release/patches
pushd /e/builds/beta-253/comm-release/comm
export IFS=

# git checkout master
git checkout 2_53_20_final
# git checkout 2_53_20_beta_01

while read -r line || [[ -n "$line" ]]; do
  echo "apply patch: $line"
  patchfile=$1/$(echo $line)
  if [ "$line" = "1602257-gitlab-253.patch" ] ||
    [ "$line" = "1602257-2-gitlab_V2-253.patch" ] ||
    [ "$line" = "1700003-domi_2_0_17_2-2538.patch" ] ||
    [[ "$line" == PPPP* ]] || [[ "$line" == WIP* ]] ||
    [[ "$line" == 9999999* ]] || [[ "$line" == 1551033-irc_* ]];
  then
    echo "patch $line skipped"
  else
    if [ -f "$patchfile" ]; then
      echo "$patchfile exist"
      # git apply --check "$patchfile"
      # git apply --index --whitespace=nowarn "$patchfile"
      # git apply --stat "$patchfile"
      git am --patch-format=hg --whitespace=nowarn < "$patchfile"
      if [ $? -eq 0 ]
      then
        echo "ok"
      else
        echo "error"
        git am --abort
        exit 1
      fi
    fi
  fi 
 
done < "$1"/series
popd
